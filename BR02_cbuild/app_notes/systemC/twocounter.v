// This demonstrates compiling and running a simple design with
// two counters driven by two clocks

// Note that reset2 and out2 are treated as an input and output
// due to the embedded directives

module twocounter(clk1, clk2, reset1, out1);
  input clk1, clk2, reset1;
  output [31:0] out1;
  reg [31:0]    out1;

  reg [31:0]    out2;                         // carbon scObserveSignal
  wire       reset2;                          // carbon scDepositSignal

  wire [31:0] internalSignal1 = out1 + out2;  // carbon forceSignal
                                              // carbon observeSignal
  reg [31:0]  internalSignal2;                // carbon observeSignal

  always @(posedge clk1)
    internalSignal2 <= internalSignal1 + 1; 

  always @(posedge clk1)
    if (reset1)
      out1 <= 32'b0;
    else
      out1 <= out1 + 32'd1;

  always @(posedge clk2)
    if (reset2)
      out2 <= 32'b0;
    else
      out2 <= out2 + 32'd3;
endmodule
