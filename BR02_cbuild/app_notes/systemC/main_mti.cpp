#include "libfifo.systemc.h"
#include "fifo_tb.h"
#include "systemc.h"

#ifndef MTI_SYSTEMC  //if using OSCI systemc simulator

int sc_main (int argc , char *argv[]) 
{

  sc_signal<bool>         rst;
  sc_signal<sc_uint<32> > data_in;
  sc_signal<bool>         push;
  sc_signal<sc_uint<32> > data_out;
  sc_signal<bool>         pop;
  sc_signal<bool>         empty;
  sc_signal<bool>         full;

  sc_clock push_clk("push_clk", 3, SC_NS);
  sc_clock pop_clk ("pop_clk",  2, SC_NS);

  fifo FIFO("FIFO");
  FIFO.rst(rst);
  FIFO.push_clk(push_clk);
  FIFO.data_in(data_in);
  FIFO.push(push);
  FIFO.pop_clk(pop_clk);
  FIFO.data_out(data_out);
  FIFO.pop(pop);
  FIFO.empty(empty);
  FIFO.full(full);

  fifo_tb FIFO_TB("FIFO_TB");
  FIFO_TB.rst(rst);
  FIFO_TB.push_clk(push_clk);
  FIFO_TB.data_in(data_in);
  FIFO_TB.push(push);
  FIFO_TB.pop_clk(pop_clk);
  FIFO_TB.data_out(data_out);
  FIFO_TB.pop(pop);
  FIFO_TB.empty(empty);
  FIFO_TB.full(full);

  sc_start(50000, SC_NS);
  cout << "Simulation exited at time " << sc_time_stamp().value() << endl;
  if (FIFO_TB.error_count)
  {
    cout << FIFO_TB.error_count << " errors found!!!" <<endl;
    return 1;
  } 
  else
  {
    cout << "No errors detected." << endl;
    return 0;
  }
}

#else  //if using Modelsim systemc simulator

SC_MODULE(mytop)
{
  sc_signal<bool>         rst;
  sc_signal<sc_uint<32> > data_in;
  sc_signal<bool>         push;
  sc_signal<sc_uint<32> > data_out;
  sc_signal<bool>         pop;
  sc_signal<bool>         empty;
  sc_signal<bool>         full;

  sc_clock                push_clk;
  sc_clock                pop_clk;

  fifo FIFO;
  fifo_tb FIFO_TB;

  void sc_main_body();

  SC_CTOR(mytop)
    : rst("rst"),
    data_in("data_in"),
    push("push"),
    data_out("data_out"),
    pop("pop"),
    empty("empty"),
    full("full)"),
    push_clk("push_clk", 3, 1.5, 0.0),
    pop_clk("pop_clk", 2, 1.0, 0.0),
    FIFO("FIFO"),
    FIFO_TB("FIFO_TB")
    {

      FIFO.rst(rst);
      FIFO.push_clk(push_clk);
      FIFO.data_in(data_in);
      FIFO.push(push);
      FIFO.pop_clk(pop_clk);
      FIFO.data_out(data_out);
      FIFO.pop(pop);
      FIFO.empty(empty);
      FIFO.full(full);

      FIFO_TB.rst(rst);
      FIFO_TB.push_clk(push_clk);
      FIFO_TB.data_in(data_in);
      FIFO_TB.push(push);
      FIFO_TB.pop_clk(pop_clk);
      FIFO_TB.data_out(data_out);
      FIFO_TB.pop(pop);
      FIFO_TB.empty(empty);
      FIFO_TB.full(full);
      
      SC_THREAD(sc_main_body);
    }
};

void
mytop::sc_main_body()
{
  wait(50000, SC_NS);
  cout << "Simulation exited at time " << sc_time_stamp().value() << endl;

  cout << "Fifo was popped " << FIFO.num_pops << " times" << endl;

  UInt32 val;
  CarbonMemoryID *fifo_mem = carbonFindMemory(FIFO.carbonModelHandle, "fifo.data_store");
  carbonExamineMemory(fifo_mem, 0, &val);
  cout << "Data at address 0 of internal storage is " << hex << val << endl;

  if (FIFO_TB.error_count)

    {
      cout << FIFO_TB.error_count << " errors found!!!" <<endl;
    }
  else
    {
      cout << "No errors detected." << endl;
      }
}

SC_MODULE_EXPORT(mytop);

#endif
