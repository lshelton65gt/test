include $(CARBON_HOME)/makefiles/Makefile.common

# Change this variable to point to your Cadence installation
NCSC_RUN_HOME = /software/cadence/IUS5.3/tools.lnx86

# To run in a different directory than where the
# application note files are located, copy this Makefile
# and set the following variable to the original location
APPNOTE_DIR = .

NCSC_RUN = ${NCSC_RUN_HOME}/bin/ncsc_run
NCSC_OPTIONS= -GCC_VERS 2.95.3 
CARBON_OPTIONS = -q  -systemCWrapper
CARBON_INCLUDES = -I$(CARBON_HOME)/include


run.x: ${APPNOTE_DIR}/fifo_tb.cpp  ${APPNOTE_DIR}/main.cpp libfifo.a
	$(NCSC_RUN) $(NCSC_OPTIONS)  $(CARBON_INCLUDES)  -VERBOSE -L. -lfifo  ${APPNOTE_DIR}/fifo_tb.cpp libfifo.systemc.cpp ${APPNOTE_DIR}/main.cpp ${CARBON_LIB_LIST}

libfifo.a: ${APPNOTE_DIR}/fifo.v ${APPNOTE_DIR}/cbuild.dir
	$(CARBON_CBUILD) $(CARBON_OPTIONS) ${APPNOTE_DIR}/fifo.v -directive ${APPNOTE_DIR}/cbuild.dir -o libfifo.a

clean:
	rm -rf INCA_libs ncsim_sc ncelab_nc *.log lib* *.o run.x
