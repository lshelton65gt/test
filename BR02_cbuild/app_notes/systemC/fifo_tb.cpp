#include "fifo_tb.h"
void fifo_tb::entry_tb_pop_clk()
{
  //The way the FIFO is modeled, pop means we're taking the data from
  //data_out so sample it now before we advance time and let it know we're
  //reading.  Otherwise the data will disappear
  if (rst)
  {
    pop.write(0);
    x_data_out = 1;
    error_count = 0;
  }
  else if (!empty && (rand() < pop_threshhold) && !rst)
  {
    pop.write(1);
//    cout << "Time: " << sc_time_stamp().value() << " Read: " << data_out.read() << endl;
    if (x_data_out != data_out.read())
    {
      cout << "Time: " << sc_time_stamp().value() << " Mismatch on data_out.  Expected: " << x_data_out<< " Actual: " << data_out.read() <<endl;
      error_count++;
    }
    x_data_out++;
  }
  else
    pop.write(0);
}

void fifo_tb::entry_tb_push_clk()
{

  if (sc_time_stamp().value() < 6000)
  {
    rst.write(1);
    push.write(0);
    data_in.write(0);
  }
  else if (!full && (rand() < push_threshhold))
  {
    rst.write(0);
    push.write(1);
//    cout << "Time: " << sc_time_stamp().value() << " Write: " << data_in.read() << endl;
    data_in.write(data_in.read() + 1);
  } 
  else
  {
    rst.write(0);
    push.write(0);
  }
}
