#include "libfifo.systemc.h"
#include "fifo_tb.h"

int sc_main (int argc , char *argv[]) 
{

  sc_signal<bool>         rst;
  sc_signal<sc_uint<32> > data_in;
  sc_signal<bool>         push;
  sc_signal<sc_uint<32> > data_out;
  sc_signal<bool>         pop;
  sc_signal<bool>         empty;
  sc_signal<bool>         full;

  sc_clock push_clk("push_clk", 3, SC_NS);
  sc_clock pop_clk ("pop_clk",  2, SC_NS);

  fifo FIFO("FIFO");
  FIFO.rst(rst);
  FIFO.push_clk(push_clk);
  FIFO.data_in(data_in);
  FIFO.push(push);
  FIFO.pop_clk(pop_clk);
  FIFO.data_out(data_out);
  FIFO.pop(pop);
  FIFO.empty(empty);
  FIFO.full(full);

  fifo_tb FIFO_TB("FIFO_TB");
  FIFO_TB.rst(rst);
  FIFO_TB.push_clk(push_clk);
  FIFO_TB.data_in(data_in);
  FIFO_TB.push(push);
  FIFO_TB.pop_clk(pop_clk);
  FIFO_TB.data_out(data_out);
  FIFO_TB.pop(pop);
  FIFO_TB.empty(empty);
  FIFO_TB.full(full);

  sc_start(50000, SC_NS);
  cout << "Simulation exited at time " << sc_time_stamp().value() << endl;

  cout << "Fifo was popped " << FIFO.num_pops << " times" << endl;

  UInt32 val;
  CarbonMemoryID *fifo_mem = carbonFindMemory(FIFO.carbonModelHandle, "fifo.data_store");
  carbonExamineMemory(fifo_mem, 0, &val);
  cout << "Data at address 0 of internal storage is " << hex << val << endl;

  if (FIFO_TB.error_count)
  {
    cout << FIFO_TB.error_count << " errors found!!!" <<endl;
    return 1;
  } 
  else
  {
    cout << "No errors detected." << endl;
    return 0;
  }
}
