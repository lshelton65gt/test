// Include the Carbon's automatically generated system C header file,
// which contains a System C SC_MODULE wrapper around the generated
// Carbon model.
//
// To add some functionality around that, we will make another wrapper
// around that, to define some member variables to hold CarbonNetIDs
// for internal signals

#define CARBON_NO_UINT_TYPES 1  // don't declare UInt32 et al in Carbon headers
#include "libtwocounter.systemc.h"
#include <fstream>
#include "systemc.h"

SC_MODULE( carbon_testbench ) {
public:
  // Member variable declaration for handles to internal Carbon signals
  CarbonNetID* mInternalSignal1; // marked as forcible in directive
  CarbonNetID* mInternalSignal2; // marked as observable in directive

  // System C Event structure that helps our testbench wake up every
  // cycle to change the inputs.  This event is sensitized in the
  // testbench constructor, and re-sensitized for every vector following
  sc_event carbon_apply_vector_event;

  // Counter to help us keep track of what we are doing
  unsigned int mVectorIndex;

  // Method that gets called every cycle to stimulate the design
  void carbon_apply_vector() {
    carbon_print_values();
    ++mVectorIndex;

    if (mVectorIndex == 1) {
      mReset1.write(1);
      mTwoCounter.reset2.write(1);    // internal signal accessed via scDepositSignal
    }
    else if (mVectorIndex == 16) {
      // De-assert reset after 10 cycles
      mReset1.write(0);
      mTwoCounter.reset2.write(0);
    }
    else if ((mVectorIndex % 4) == 1) {
      // Force/release every 4 ticks

      if ((mVectorIndex % 8) == 1) {
        std::cout << "[forcing]\n";
        carbonForce(mTwoCounter.carbonModelHandle, mInternalSignal1,
                    &mVectorIndex);
      }
      else {
        std::cout << "[releasing]\n";
        carbonRelease(mTwoCounter.carbonModelHandle, mInternalSignal1);
      }
    }

    // Toggle clocks
    mClk1.write(mVectorIndex & 1);
    if ((mVectorIndex % 3) == 0) {
      mClk2.write(!mClk2.read());
    }


    // Keep running vectors for 200 cycles
    if (mVectorIndex < 200 ) {
      carbon_apply_vector_event.notify( 100, SC_NS );
    }
  } // void carbon_apply_vector

  // // Print I/Os and observed signals
  void carbon_print_values() {
    std::cout << mVectorIndex << ":\t";
    std::cout << "clk1=" << mClk1.read();
    std::cout << " clk2=" << mClk2.read();
    std::cout << " reset1=" << mReset1.read();

    // reset2 is not a real input port.  It's an internal net that
    // was annotated with the embedded directive // carbon scDepositSignal
    // The "sc" prefix before "DepositSignal" tells cbuild to
    // create a shadow System C member variable in SC_MODULE "twocounter".
    // That SC_MODULE also includes code to automatically deposit to the
    // internal signal from the shadow variable.
    std::cout << " reset2=" << mTwoCounter.reset2.read(); 
    std::cout << " out1=" << mOut1.read();

    // Similarly, out2 is not a real input port.  It was declared in the
    // RTL with embedded directive // carbon scObserveSignal.  That
    // creates a shadow SystemC variable in SC_MODULE "twocounter",
    // and keeps it up-to-date with the DUT.
    std::cout << " out2=" << mTwoCounter.out2.read();

    // Access two internal signals via the Carbon C API.
    CarbonUInt32 v, drive;
    carbonExamine(mTwoCounter.carbonModelHandle, mInternalSignal1, &v, &drive);
    std::cout << " internalSignal1=" << v; // accessed via forceSignal

    // Note that internalSignal2 could have been declared with
    // scObserveSignal, rather than observeSignal.  That would
    // create a shadow System C variable in the "twocounter" model,
    // and that variable would be updated every cycle.  If you are not
    // going to examine the signal on every cycle, then that would
    // slow down the runtime, and it would be faster to use the C API
    // as shown here.
    carbonExamine(mTwoCounter.carbonModelHandle, mInternalSignal2, &v, &drive);
    std::cout << " internalSignal2=" << v; // accessed via observeSignal

    std::cout << std::endl;
  } // void carbon_print_values

  // System-C port member variables, hooked directly to the DUT instance
  // mTwoCounter
  sc_signal< bool > mClk1;
  sc_signal< bool > mClk2;
  sc_signal< bool > mReset1;
  sc_signal< sc_uint<32> > mOut1;

  // DUT instance
  twocounter mTwoCounter;

  // Constructor for the testbench
  SC_CTOR( carbon_testbench ) : mTwoCounter("inst") {

    // Connect the DUT to the testbench ports
    mTwoCounter.clk1(mClk1);
    mTwoCounter.clk2(mClk2);
    mTwoCounter.reset1(mReset1);
    mTwoCounter.out1(mOut1);

    // Member variable initialization for handles to internal Carbon signals
    mInternalSignal1 = carbonFindNet(mTwoCounter.carbonModelHandle,
                                     "twocounter.internalSignal1");
    assert(mInternalSignal1);
    mInternalSignal2 = carbonFindNet(mTwoCounter.carbonModelHandle,
                                     "twocounter.internalSignal2");
    assert(mInternalSignal2);

    mVectorIndex = 0;
    SC_METHOD( carbon_apply_vector );
    sensitive << carbon_apply_vector_event;
  }

};  // end carbon_testbench

int sc_main(int argc, char* argv[]) {
  carbon_testbench carbon_tb( "carbon_tb" );
  sc_start( -1 );
  std::cout << "Test complete" << std::endl;
  return 0;
}
