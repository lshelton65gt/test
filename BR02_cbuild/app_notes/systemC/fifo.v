/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


module fifo(pop_clk, push_clk, rst, pop, push, data_in, empty, full, data_out);
input pop_clk, push_clk, rst, pop, push;
input [31:0] data_in;
output empty, full;
output [31:0] data_out;
reg [31:0]data_store[31:0];
reg [4:0] push_ptr, pop_ptr;
reg [31:0] num_pops;

always @(posedge push_clk)
begin
  if (rst)
  begin
    push_ptr <= 0;
  end
  else if (push)
  begin
    data_store[push_ptr] <= data_in;
    push_ptr <= push_ptr + 1'b1;
  end
end

always @(posedge pop_clk)
begin
  if (rst)
  begin
    pop_ptr <= 0;
    num_pops <= 32'h0;
  end
  else if (pop)
  begin
    pop_ptr <= pop_ptr + 1'b1;
    num_pops <= num_pops + 32'h1;
  end
end

assign empty=(pop_ptr==push_ptr);
assign full=(push_ptr==(pop_ptr - 1'b1));
assign data_out=data_store[pop_ptr];

endmodule
