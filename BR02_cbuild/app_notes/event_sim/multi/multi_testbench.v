module top;

   integer a, b;  
   wire [31:0] sum;
   add adder(sum, a, b);

   integer minuend, subtrahend;
   wire [31:0] difference;
   sub subtracter(difference, minuend, subtrahend);

   initial begin
      a = 0;
      b = 0;
      minuend = 0;
      subtrahend = 0;

      #1 $monitor("add: %0d + %0d = %0d", a, b, sum);
      #1 $monitor("sub: %0d - %0d = %0d", minuend, subtrahend, difference);

      #1 a = 1;  // 1 + 0 = 1
      #1 b = 1;  // 1 + 1 = 2

      #1 minuend = 2;       // 2 - 0 = 2
      #1 subtrahend = 1;    // 2 - 1 = 1
   end
endmodule
