/* add_ $Revision: 1.1 $, $Date: 2007/10/03 17:35:43 $ Oct 03, 2007  09:11:06*/

/* veriusertfs definition for model */
#define PROTO_PARAMS(params) params
#define EXTERN extern
#define PLI_EXTRAS
#include "carbon/veriuser.h"
#undef PROTO_PARAMS
#undef EXTERN
#include "carbon/carbon_udtf.h"

#define GENERIC_PLI 1

extern int carbon_add_instance_checktf();
extern int carbon_add_instance_misctf();

/* Pulled from libsub.pli_tfcell.c */
extern int carbon_sub_instance_checktf();
extern int carbon_sub_instance_misctf();

/*
 * Aldec Riviera PLI interface
 */
#ifdef ALDEC_PLI
#undef GENERIC_PLI
#include "aldecpli.h"
s_tfcell veriusertfs[] =
{
#include "carbon/carbon_tfcell.h"
#include "libadd.pli_tfcell.h"
#include "libsub.pli_tfcell.h" /* Pulled from libsub.pli_tfcell.c */
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
};
#endif /* ALDEC_PLI */


/*
 * Cadence Verilog-XL and NC-Verilog interface
 */
#ifdef CADENCE_PLI
#undef GENERIC_PLI
#include "vxl_veriuser.h"
p_tfcell carbon_bootstrap()
{
  static s_tfcell carbon_tfs[] =
  {
#include "carbon/carbon_tfcell.h"
#include "libadd.pli_tfcell.h"
#include "libsub.pli_tfcell.h" /* Pulled from libsub.pli_tfcell.c */
    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
  };
  return carbon_tfs;
}
#endif /* CADENCE_PLI */


/*
 * If no simulator-specific PLI interface has been selected, 
 * assume a generic veriusertfs[] interface
 */
#ifdef GENERIC_PLI
#define usertask 1
#define userfunction 2
typedef struct t_tfcell
{
  PLI_INT16 type;
  PLI_INT16 data;
  PLI_INT32 (*checktf)();
  PLI_INT32 (*sizetf)();
  PLI_INT32 (*calltf)();
  PLI_INT32 (*misctf)();
  PLI_BYTE8 *tfname;
  PLI_INT32 forwref;
  PLI_BYTE8 *tfveritool;
  PLI_BYTE8 *tferrmessage;
  PLI_INT32 hash;
  struct t_tfcell *left_p;
  struct t_tfcell *right_p;
  PLI_BYTE8 *namecell_p;
  PLI_INT32 warning_printed;
} s_tfcell, *p_tfcell;
s_tfcell veriusertfs[] =
 {
#include "carbon/carbon_tfcell.h"
#include "libadd.pli_tfcell.h"
#include "libsub.pli_tfcell.h" /* Pulled from libsub.pli_tfcell.c */
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
};
#endif /* GENERIC_PLI */
