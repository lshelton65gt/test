#*****************************************************************************
#
# Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
#******************************************************************************

# NC-Sim specific make commands

include ${CARBON_HOME}/makefiles/Makefile.common

run : libmulti.pli.so multi_testbench.v libadd.pli.v libsub.pli.v
	ncvlog multi_testbench.v libadd.pli.v libsub.pli.v
	ncelab -access +rw top -loadpli1 libmulti.pli.so:carbon_bootstrap
	ncsim top

INCLUDE = -I${CDS_INST_DIR}/tools/include
CFLAGS = -DCADENCE_PLI
CC = ${CARBON_CC}

include Makefile.common
