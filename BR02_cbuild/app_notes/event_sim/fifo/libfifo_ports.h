/* fifo_ $Revision: 1.1 $, $Date: 2005/05/26 15:28:36 $ Feb 16, 2005  15:38:33*/

/* C/C++ interface to external code. */
#ifndef __fifo_PORTS_H_
#define __fifo_PORTS_H_

#ifndef __carbon_capi_h_
#include "carbon/carbon_capi.h"
#endif

struct carbon_fifo_ports {
  
  /* input change array */
  bool* mChangeArray;

  /* run combo */
  bool* mRunCombo;

  /* 'in' parameters */
  UInt1* m_rdclk; 	/* 0 */
  UInt1* m_rden;
  UInt1* m_rst; 	/* 1 */
  UInt1* m_wrclk; 	/* 2 */
  UInt16* m_wrdata;
  UInt1* m_wren;

  /* 'out' parameters */
  const UInt1* m_empty;
  const UInt16* m_rddata;

  /* 'tristate out' parameters */

  /* 'inout' parameters */
};

#ifdef __cplusplus
#define EXTERNDEF extern "C"
#else
#define EXTERNDEF
#endif
EXTERNDEF void carbon_fifo_portstruct_init(struct carbon_fifo_ports*, CarbonObjectID*);
#undef EXTERNDEF
#endif
