#*****************************************************************************
#
# Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
#******************************************************************************

# Edit this if $CDS_INST_DIR does not point to your
# Cadence install directory
CADENCE_TOOLS = ${CDS_INST_DIR}/tools

INCLUDE = -I${CADENCE_TOOLS}/include \
	  -I${CADENCE_TOOLS}/inca/include \

DEFINES = -DCADENCE_PLI

# Common makefile to build the fifo
include Makefile.common
