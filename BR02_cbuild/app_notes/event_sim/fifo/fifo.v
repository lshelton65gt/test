/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// 8 word, 16-bit FIFO

module fifo(rst, wrclk, wrdata, wren, rdclk, rddata, rden, empty);
   input rst;
   input wrclk;
   input [15:0] wrdata;
   input 	wren;
   input 	rdclk;
   output [15:0] rddata;
   input 	 rden;
   output 	 empty;

   reg [15:0] 	 mem[7:0];
   reg [2:0] 	 rdptr;
   reg [2:0] 	 wrptr;
   
   always @(posedge wrclk or posedge rst)
     if (rst)
       wrptr <= 3'b0;
     else begin
	if (wren) begin
	   mem[wrptr] <= wrdata;
	   wrptr <= wrptr + 3'b1;
	end
     end

   
   always @(posedge rdclk or posedge rst)
     if (rst)
       rdptr <= 3'b0;
     else begin
	if (rden & !empty)
	  rdptr <= rdptr + 3'b1;
     end

   assign rddata = mem[rdptr];
   assign empty = (rdptr == wrptr);
   
endmodule // fifo

   
