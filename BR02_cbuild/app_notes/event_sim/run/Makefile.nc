#*****************************************************************************
#
# Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
#******************************************************************************

# Makefile to build switch testbench

.PHONY: build clean veryclean

FIFO_DIR = ../fifo
TB_DIR = ../tb
SWITCH_DIR = ../switch

VFILES = ${TB_DIR}/tb.v ${SWITCH_DIR}/switch.v ${FIFO_DIR}/libfifo.pli.v

build: ${VFILES} libfifo.symtab.db ${TB_DIR}/libpli.so
	ncverilog ${VFILES} +loadpli1=${TB_DIR}/libpli.so:bootstrap +loadpli1=${FIFO_DIR}/libfifo_pli.so:carbon_bootstrap +access+r

libfifo.symtab.db: ${FIFO_DIR}/libfifo.symtab.db
	@rm -f $@
	@ln -s $< .

${FIFO_DIR}/libfifo.pli.v ${FIFO_DIR}/libfifo.symtab.db:
	$(error Please run make in ${FIFO_DIR} first!)

${TB_DIR}/libpli.so:
	$(error Please run make in ${TB_DIR} first!)

clean:
	rm -rf INCA_libs *.log ncverilog.key verilog.dump* libfifo.symtab.db

veryclean: clean
	make -C ${FIFO_DIR} -f Makefile.nc clean
	make -C ${TB_DIR} -f Makefile.nc clean
