/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* veriusertfs declaration for testbench */

#include "veriuser.h"

extern int tb_misc();
extern int tb_call();

s_tfcell veriusertfs[2] = {
    /* PLI call for testbench */
    { usertask, 1, 0, 0, tb_call, tb_misc, "$tb" },
    {0}
};
