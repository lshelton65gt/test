/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Output port BFM

class OutBFM
{
public:
    OutBFM(int port);
    ~OutBFM();

    void clk(int outdata,
	     bool req,
	     bool &ack);
    bool isIdle() const;

private:
    int mPort;		// Port number of this BFM
    bool mIdle;		// Is this BFM idle?
    bool mPrevAck;	// Was an ack issued the previous cycle?
};
