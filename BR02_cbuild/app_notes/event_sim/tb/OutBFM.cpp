/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Output port BFM

#include "OutBFM.h"
#include <iostream>

OutBFM::OutBFM(int port)
    : mPort(port),
      mIdle(true),
      mPrevAck(false)
{
}

OutBFM::~OutBFM()
{
}

void OutBFM::clk(int outdata,
		 bool req,
		 bool &ack)
{
    // Not idle if there's a req
    mIdle = !req;

    if (req) {
	if (mPrevAck) {
	    // If there was an ack the previous cycle, don't ack again
	    // in response to the req.  If the last entry in the FIFO
	    // was just acked, request won't be deasserted yet.
	    ack = false;
	} else {
	    // No ack the last cycle, so this must be a
	    // real request
	    ack = true;

	    // Print message as well
	    std::cout << "Port " << std::dec << mPort << " received data 0x"
		      << std::hex << outdata << std::endl;
	}
    } else
	ack = false;	// no request

    mPrevAck = ack;	// save last state of ack
}

bool OutBFM::isIdle() const
{
    return mIdle;
}

