/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Top-level testbench

`timescale 1ns/1ns

module tb;
   reg inclk, outclk, rst;
   reg [15:0] indata;
   reg [3:0]  ack;
   reg [1:0]  reason;
   reg allow_inclk, allow_outclk;
   
   // Generate clocks/reset
   initial begin
      inclk = 1'b0;
      outclk = 1'b0;
      rst = 1'b1;
      indata = 16'h0;
      ack = 4'h0;
      reason = 2'b00;
      $dumpvars;
      #100 rst = 1'b0;
      #1000;
      $display("Done at time %t", $time);
      $finish;
   end

   always forever
      #10 inclk = ~inclk;

   always forever
      #15 outclk = ~outclk;

   // Instantiate switch module
   switch switch(.rst(rst),
		 .inclk(inclk),
		 .outclk(outclk),
		 .indata(indata),
		 .ack(ack));

   // Call testbench code
   always @(posedge inclk or posedge outclk) begin
      if (rst) begin
	 indata = 16'h0;
	 ack = 4'h0;
      end
      else begin
	 reason = {allow_outclk, allow_inclk} & {outclk, inclk};
	 $tb(reason,
	     indata,
	     switch.outdata0,
	     switch.outdata1,
	     switch.outdata2,
	     switch.outdata3,
	     switch.req,
	     ack);
	 {allow_outclk, allow_inclk} = ~reason;
      end
   end

   always @(negedge inclk or posedge rst)
     if (rst)
       allow_inclk = 1'b0;
     else
       allow_inclk = 1'b1;
       
   always @(negedge outclk or posedge rst)
     if (rst)
       allow_outclk = 1'b0;
     else
       allow_outclk = 1'b1;
       
endmodule // tb
