/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* Bootstrap function for registering user routines */

#include "veriuser.h"
#include "vxl_veriuser.h"

extern int tb_misc();
extern int tb_call();

s_tfcell my_tfs[3] = {
    /* PLI call for testbench */
    { usertask, 1, 0, 0, tb_call, tb_misc, "$tb" },
    {0}
};

p_tfcell bootstrap ()
{
    return(my_tfs);
}
