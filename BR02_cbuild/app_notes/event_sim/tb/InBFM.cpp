/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Input port BFM

#include "InBFM.h"
#include <fstream>
#include <iostream>

InBFM::InBFM(const std::string &filename)
    : mFilename(filename)
{
    int data;

    // Open file
    std::ifstream pckfile(mFilename.c_str());
    if (!pckfile) {
	std::cout << "Couldn't open file " << mFilename << std::endl;
	return;
    }

    // Read packet info
    while (pckfile >> std::hex >> data)
	mPacket.push_back(data);

}

InBFM::~InBFM()
{
}

void InBFM::clk(int &indata)
{
    if (mPacket.size() > 0) {
	// If the packet isn't empty, send the next piece
	indata = mPacket.front();
	mPacket.pop_front();
    } else {
	// Packets are finished - drive 0
	indata = 0;
    }
}

bool InBFM::isDone() const
{
    return (mPacket.empty());
}
