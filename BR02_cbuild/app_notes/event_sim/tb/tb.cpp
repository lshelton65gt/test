/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Top-level testbench class

#include "tb.h"
#include "InBFM.h"
#include "OutBFM.h"
#include <iostream>

tb::tb(const std::string &filename)
    : mFilename(filename)
{
    // Allocate BFMs
    mIn = new InBFM(mFilename);
    mOut0 = new OutBFM(0);
    mOut1 = new OutBFM(1);
    mOut2 = new OutBFM(2);
    mOut3 = new OutBFM(3);
}

tb::~tb()
{
    // Cleanup
    delete mIn;
    delete mOut0;
    delete mOut1;
    delete mOut2;
    delete mOut3;
}

void tb::inclk(int &indata)
{
    // Call input port BFM
    mIn->clk(indata);
}

void tb::outclk(int outdata0,
		int outdata1,
		int outdata2,
		int outdata3,
		int req,
		int &ack)
{
    // Call output port BFMs
    // Each will drive a different bit of the
    // ack signal, so reassemble it at the end
    bool ack0, ack1, ack2, ack3;

    mOut0->clk(outdata0, req & 0x1, ack0);
    mOut1->clk(outdata1, (req >> 1) & 0x1, ack1);
    mOut2->clk(outdata2, (req >> 2) & 0x1, ack2);
    mOut3->clk(outdata3, (req >> 3) & 0x1, ack3);

    ack = (ack3 << 3) | (ack2 << 2) | (ack1 << 1) | ack0;
}

bool tb::isDone() const
{
    // Not done if any of the BFMs is busy
    if (!mIn->isDone()) return false;
    if (!mOut0->isIdle()) return false;
    if (!mOut1->isIdle()) return false;
    if (!mOut2->isIdle()) return false;
    if (!mOut3->isIdle()) return false;

    return true;
}

