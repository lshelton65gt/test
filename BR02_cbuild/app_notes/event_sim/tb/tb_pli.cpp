/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Switch testbench PLI calls

#include "tb.h"
#include <iostream>

#define PROTO_PARAMS(params) params
#define EXTERN extern "C"
#include "veriuser.h"
#undef PROTO_PARAMS
#undef EXTERN

extern "C" int tb_misc(int, int reason)
{
    tb* the_tb;
    if (reason == reason_endofcompile) {
	// Initialize
	std::string filename("packet.dat");
	char *arg;

	// Check for input file
	arg = mc_scan_plusargs("file=");
	if (arg)
	    filename = arg;

	// Create the testbench
	the_tb = new tb(filename);
	tf_setworkarea(reinterpret_cast<char *>(the_tb));
    } else if (reason == reason_finish) {
	// Cleanup
	the_tb = reinterpret_cast<tb *>(tf_getworkarea());
	delete the_tb;
    }
}

extern "C" int tb_call()
{
    int clock, indata, outdata0, outdata1, outdata2, outdata3, req, ack;

    clock = tf_getp(1);
    tb *the_tb = reinterpret_cast<tb *>(tf_getworkarea());
    if (clock & 0x1) {	// inclk
	// Read inputs from PLI
	// Call testbench
	the_tb->inclk(indata);
	// Copy outputs through PLI
	tf_putp(2, indata);
    }
    if (clock & 0x2) {		// outclk
	// Read inputs from PLI
	outdata0 = tf_getp(3);
	outdata1 = tf_getp(4);
	outdata2 = tf_getp(5);
	outdata3 = tf_getp(6);
	req = tf_getp(7);
	// Call testbench
	the_tb->outclk(outdata0,
		       outdata1,
		       outdata2,
		       outdata3,
		       req,
		       ack);
	// Copy outputs through PLI
	tf_putp(8, ack);
    }

    // End simulation when testbench is done
    if (the_tb->isDone())
	tf_dofinish();
}

