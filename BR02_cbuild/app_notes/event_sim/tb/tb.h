/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Top-level testbench class

#include <string>

class InBFM;
class OutBFM;

class tb
{
public:
    tb(const std::string &filename);
    ~tb();

    // Routines for each clock
    void inclk(int &indata);
    void outclk(int outdata0,
		int outdata1,
		int outdata2,
		int outdata3,
		int req,
		int &ack);
    bool isDone() const;

private:
    const std::string mFilename;		// File containing packets
    InBFM *mIn;					// Input port BFM
    OutBFM *mOut0, *mOut1, *mOut2, *mOut3;	// Output port BFMs
};
