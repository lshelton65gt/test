/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Input port BFM

#include <string>
#include <list>

class InBFM
{
public:
    InBFM(const std::string &filename);
    ~InBFM();

    void clk(int &indata);
    bool isDone() const;

private:
    const std::string mFilename;	// File holding packet data
    std::list<int> mPacket;		// List to hold the packet data
};

