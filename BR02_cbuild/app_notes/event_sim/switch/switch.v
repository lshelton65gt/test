/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// 1:4 packet decoder switch

module switch(rst, inclk, indata, outclk, outdata0, outdata1, outdata2, outdata3, req, ack);
   input rst;
   input inclk;
   input [15:0] indata;
   input 	outclk;
   output [15:0] outdata0;
   output [15:0] outdata1;
   output [15:0] outdata2;
   output [15:0] outdata3;
   output [3:0]  req;
   input [3:0] 	 ack;
   
   reg [15:0] 	 indata_q;
   wire 	 in_packet;
   reg [13:0] 	 data_count;
   reg [13:0] 	 next_data_count;
   reg [1:0] 	 dest_port;
   reg [1:0] 	 next_dest_port;
   reg [3:0] 	 fifo_select;
   wire [3:0] 	 empty;
   
   initial begin
      indata_q = 16'h0;
      fifo_select = 3'b0;
   end

   always @(posedge inclk or posedge rst) begin
      if (rst) begin
	 indata_q <= 16'h0;
	 data_count <= 14'h0;
	 dest_port <= 2'b0;
      end
      else begin
	 indata_q <= indata;
	 data_count <= next_data_count;
	 dest_port <= next_dest_port;
      end
   end
   
   // Decode incoming data
   always @(indata_q or in_packet or data_count) begin
      if (in_packet) begin
	 // A packet is in progress - just decrement the counter
	 next_data_count = data_count - 14'b1;
	 next_dest_port = dest_port;
      end
      else begin
	 // Look at incoming header:
	 // Bits [15:14] - destination port
	 // Bits [13:0] - data count
	 next_data_count = indata_q[13:0];
	 next_dest_port = indata_q[15:14];
      end
   end	 

   assign in_packet = (data_count != 14'h0);
   assign req = ~empty;
   
   // Fifo select/enable
   always @(in_packet or dest_port)
     case({in_packet, dest_port})
       3'b1_00: fifo_select = 4'b0001;
       3'b1_01: fifo_select = 4'b0010;
       3'b1_10: fifo_select = 4'b0100;
       3'b1_11: fifo_select = 4'b1000;
       default: fifo_select = 4'b0000;
     endcase // case({in_packet, dest_port})

   // Destination port fifos
   // These are wrapper modules that call the Carbon Models
   fifo fifo0(.rst(rst),
	      .wrclk(inclk),
	      .wrdata(indata_q),
	      .wren(fifo_select[0]),
	      .rdclk(outclk),
	      .rddata(outdata0),
	      .rden(ack[0]),
	      .empty(empty[0]));
   
   fifo fifo1(.rst(rst),
	      .wrclk(inclk),
	      .wrdata(indata_q),
	      .wren(fifo_select[1]),
	      .rdclk(outclk),
	      .rddata(outdata1),
	      .rden(ack[1]),
	      .empty(empty[1]));
   
   fifo fifo2(.rst(rst),
	      .wrclk(inclk),
	      .wrdata(indata_q),
	      .wren(fifo_select[2]),
	      .rdclk(outclk),
	      .rddata(outdata2),
	      .rden(ack[2]),
	      .empty(empty[2]));
   
   fifo fifo3(.rst(rst),
	      .wrclk(inclk),
	      .wrdata(indata_q),
	      .wren(fifo_select[3]),
	      .rdclk(outclk),
	      .rddata(outdata3),
	      .rden(ack[3]),
	      .empty(empty[3]));
   
   
   
endmodule // switch
