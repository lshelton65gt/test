// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#define TX_XTOR
#define RX_XTOR
#define RUN_TX
/*

  test environment to exercise the xGMII interface
  Author: John Johnson (john@carbondesignsystems.com)

*/

#include "libdesign.h"
#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimClock.h"
#include "carbonsim/CarbonSimStep.h"
#include "carbonsim/CarbonSimObjectInstance.h"
#include "carbonsim/CarbonSimNet.h"

#include <cassert>
// #include <cstdio>
#include <iostream>

#include "CarbonXGMIITransactor.h"
#include "CarbonXGMIITransaction.h"

void bindNet(CarbonSimMaster* m, CarbonSimFunctionGen* f, const char* name)
{
  CarbonSimNet* net = m->findNet(name);
  if(net) {
    f->bindSignal(net);
  } else {
    std::cerr << "ERROR: Could not find " <<  name << std::endl;
  }
  
}


int main(int argc, char*argv[]) {

  //instantiate simmaster and pci transactor
  CarbonSimMaster *sim             = CarbonSimMaster::create(); 

  CarbonSimObjectType *top_type = sim->addObjectType("xgmii", carbon_design_create); 
  CarbonSimObjectInstance *ganges  = sim->addInstance(top_type, "xgmii", eCarbonFullDB, eCarbon_EnableStats);

  //configure clocks and resets
  CarbonSimClock *xgmii_clk   = new CarbonSimClock(150, 75, 0); 
  CarbonSimStep  *rst_n     = new CarbonSimStep(300, 0); 

  sim->addClock(xgmii_clk);
  sim->addStep(rst_n);

  char net_name[270];

  bindNet(sim, xgmii_clk, "xgmii.RX_CLK");
  bindNet(sim, rst_n, "xgmii.reset_n");

  CarbonXGMIITxTransactor **gmii_tx = new CarbonXGMIITxTransactor*[3];

#ifdef TX_XTOR
  //configure the gmii tx transactor  
  // xtor tx -> dut rx
  for(int i = 0; i < 3; ++i) {
      const int NAMELEN = 128; 
      char netName[NAMELEN] = "";

      gmii_tx[i] =  new CarbonXGMIITxTransactor(sim);
      xgmii_clk->bindTransactor(gmii_tx[i]);
  
      snprintf(netName, NAMELEN, "xgmii.RXD_%d", i);
      gmii_tx[i]->bindData(netName);

      snprintf(netName, NAMELEN, "xgmii.RXC_%d", i);
      gmii_tx[i]->bindControl(netName);
  }
#endif

  CarbonXGMIIRxTransactor *gmii_rx = 0;
#ifdef RX_XTOR
  //configure the gmii rx transactor  
  // xtor rx -> dut tx

   gmii_rx = new CarbonXGMIIRxTransactor(sim);
  xgmii_clk->bindTransactor(gmii_rx);
  gmii_rx->setClock(xgmii_clk);

  gmii_rx->bindData("xgmii.TXD_0");
  gmii_rx->bindControl("xgmii.TXC_0");

#endif


  // Probably want to have a call bakc for when the rx
  // xtor gets a packet
  ////gmii_tx->onReadCompletion(gmii_rx_readcompletion_cb, (void *)sim);

  //enable waveform dumping
  CarbonWaveID* wave = carbonWaveInitFSDB(ganges->getCarbonObject(), "top.fsdb", e1ps);
  gmii_rx->setFsdbFile(wave);
  carbonDumpVars(wave, 0, "xgmii");
//  carbonDumpAll(wave);
  carbonDumpOn(wave);

  // run reset
  sim->run(600);

#ifdef RUN_TX
  //run the transactor - send 5 transactions
  // The transactions are totally bogus packets, but it really 
  // doesn't matter for these purposes
  for(int i=0; i<8; ++i) {
     
     int len = 8 + i;
     UInt8* data = new UInt8[len];


     for(int j = 0; j < len; ++j) {
	data[j] = j+ i;
     }

     CarbonXGMIITransaction *tran_tx = 
	new CarbonXGMIITransaction(data, len);     
     
     gmii_tx[0]->submit(tran_tx);
  }
#endif

  sim->run(7000);

  printf("Simulation completed\n");

  //clean up
  if(wave) {
    carbonDumpOff(wave);
  }

  delete ganges;
//  delete gmii_tx;
  delete gmii_rx;

  return (0);

}

// Local Variables:
// c-basic-offset: 3
// End:
