// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CarbonXGMIITransactor.h"
#include <cassert>

//#define STDOUT_DEBUG

#ifdef STDOUT_DEBUG
#include <iostream>
#endif


void CarbonXGMIITxTransactor::run()
{
    

   switch(mState)
   {
   case eIdle:
      mCtrl= 0xf;
      mData= 0x07070707; // 4 IDLE ctrl octets
      if (not xmtQueue.empty()) {
	 // get a packet to xmit
	 mCurPacket = xmtQueue.front();
	 xmtQueue.pop();
#ifdef STDOUT_DEBUG
	 std::cout << "Starting a packet of size: " 
		   << std::dec << mCurPacket->getLength()
		   << " Leaving IDLE State " << std::endl;
#endif
	 mState = ePreamble;
	    
      }
      break;
	
   case ePreamble:
//      std::cout << "PREAMBLE" << std::endl;
      mCtrl= 0x1;
      mData= 0x555555fb; // 3 PREAMBLE data octets, 1 START ctrl octet
      mState= eSFD;
      break;
	
   case eSFD:
//      std::cout << "SFD" << std::endl;
      mCtrl= 0x0;
      mData= 0xd5555555; // 1 SFD data octet, 3 PREAMBLE data octets
      mState= eData;
      break;
	
   case eData:
//      std::cout << "DATA" << std::endl;
      // Need to make sure this handles odd # words
      mData = mCurPacket->getNextWord();      
      mCtrl = 0;


      if( mCurPacket->endOfPacket() ) {
	 // If not all the bytes are data, 
	 // we need to add a terminate byte
	 // and maybe some idle bytes
	 switch (mCurPacket->getNumEmptyBytes()) {
	 case 0:
	    break;
	 case 1:
	    mCtrl = 0x8;
	    mData |= 0xfd000000;
	    break;
	 case 2:
	    mCtrl = 0xC;
	    mData |= 0x07fd0000;
	    break;
	 case 3:
	    mCtrl = 0xE;
	    mData |= 0x0707fd00;
	    break;
	 default:
	    assert("INTERNAL xGMII XTOR ERROR" == 0);
	 }

	 mCurPacket = 0;
	 if (mCtrl == 0)
	    mState= eEFD;
	 else {
	    mState= eIFG;
	 }
	    
	 // mCtrl= mLastCtrl;
      }
      break;

   case eEFD:
//      std::cout << "EFD" << std::endl;
      mCtrl= 0xf;
      mData= 0x070707fd; // 3 IDLE, 1 TERMINATE ctrl octets
      mState= eIFG;
      break;

   case eIFG:
//      std::cout << "IFG" << std::endl;
      mCtrl= 0xf;
      mData= 0x07070707; // 4 IDLE ctrl octets
      
      ++mIFGCount;
      // Using mIFG -1 because the 
      // next state will be idle, which contributes
      // one of the needed idle clocks
      if( not (mIFGCount < mIFG - 1)) {
	 mState= eIdle;
	 mIFGCount = 0;
      }
	
      break;

   default: // bogus
      assert ("HIT BAD xGMIITx STATE!!"==0);
      break;
   }
}

void CarbonXGMIITxTransactor::submit(CarbonXGMIITransaction * transaction)
{
#ifdef STDOUT_DEBUG
   std::cout << " Transaction submitted to TX xtor length " 
	     << std::dec << transaction->getLength()
	     << std::endl;
//    for (int x = 0; x < transaction->getLength(); ++x) {
//	std::cout << "D" << std::dec << x << ": "
//		  << std::hex << UInt32(transaction->getData(x)) << " " ;
//    }
//    std::cout << std::endl << std::endl;
#endif
   xmtQueue.push(transaction);
}



void CarbonXGMIITxTransactor::observeNets() {}     // nothing to observe for TX
void CarbonXGMIITxTransactor::depositNets() 
{
   if (mDataNet) mDataNet->deposit(&mData);
   if (mCtrlNet) mCtrlNet->deposit(&mCtrl);
}


// Local Variables:
// c-basic-offset: 3
// End:
//
