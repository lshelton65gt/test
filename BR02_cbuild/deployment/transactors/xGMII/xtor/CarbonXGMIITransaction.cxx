// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CarbonXGMIITransaction.h"
#include "carbonsim/CarbonSimTransactor.h"
#include <cassert>
//#include <cstring>
#include <iostream>

// -------------------- CarbonXGMIITransaction --------------------

CarbonXGMIITransaction::CarbonXGMIITransaction(UInt8 const * packet, 
					     int length)
   : aPacket(packet), mLength(length), mIdx(0)
{

}

CarbonXGMIITransaction::~CarbonXGMIITransaction()
{
   delete [] aPacket;
}

void
CarbonXGMIITransaction::step(
   CarbonSimTransactor * transactor, CarbonTime tick)
{
   transactor->step(tick); // 
}

bool CarbonXGMIITransaction::isDone(void)
{
   return true;
}

// read number of packet data bytes
int CarbonXGMIITransaction::getLength() 
{
   return mLength;
}

//int CarbonGMIITransaction::getIndex()
//{
//   return mIdx;
//}

/*
** This is a terrible name...
**
** What this function does is tell how many bytes
** of the last UInt32 returned by getNextWord 
** were empty.  So if you have a xaction with 
** 5 bytes of data, and call getNextWord() twice, then
** this will return 0 after the first getNextWord() call
** and 3 after the second.
**  
*/
int CarbonXGMIITransaction::getNumEmptyBytes()
{
   return mIdx - mLength;
}

UInt32 CarbonXGMIITransaction::getNextWord()
{
//   std::cout << "getNextWord idx=" << std::dec << mIdx
//	     << " l=" << mLength << std::endl;
   if (mIdx+4 > mLength) {
      int overrun = (mIdx+4) - mLength;
      assert(overrun < 4);

      // copy the remaining bytes
      unsigned char tc[4] = {0,0,0,0};
      for (int i = 0; i < 4 - overrun; ++i) {
	 tc[i] = aPacket[mIdx + i];
      }

      mIdx +=4;
      return *((UInt32*)tc);
   } 

   // I love pointer tricks!!!
   UInt32 *t = (UInt32*) (aPacket + mIdx);
   mIdx += 4;
    
   return *t;
    
}

UInt8 CarbonXGMIITransaction::getData(int index)
{
   if(index < mLength)
      return aPacket[index]; 
  
   else return 0;
}

bool CarbonXGMIITransaction::endOfPacket() 
{
//   std::cout << "eop: " << std::dec << mIdx << ' ' << mLength << std::endl;
   return  mIdx >= mLength; 
}


// Local Variables:
// c-basic-offset: 3
// End:
