// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CarbonXGMIITransactor.h"
#include "carbonsim/CarbonSimClock.h"

#include <cassert>


/*
** Transactor ID
*/ 
void CarbonXGMIITransactor::setId(int id)  { mId = id;   }
int CarbonXGMIITransactor::getId()  { return mId; }

/*
** DDR flag
*/
void CarbonXGMIITransactor::setDDR(bool val) { mDDR = val;  }
bool CarbonXGMIITransactor::getDDR() { return mDDR; }



void CarbonXGMIITransactor::step(CarbonTime tick)
{
   /*
   ** Ordering of the observes / deposits MIGHT
   ** be an issue.  However, I don't think it actually is
   ** because the interface is uni directional
   */
    
   observeNets();
    
   /*
   ** xGMII is supposed to be ddr according to 802.3
   ** However we've seen customer cases where it is just 
   ** a posedge thing.  So we make the DDR behavior configurable
   */
    
   if(mDDR) {
      run();
   } else if (mClk->getValue()) {
      run();
   }

   depositNets();
}

void CarbonXGMIITransactor::idle(UInt64 cycles)
{
   return;
}




void CarbonXGMIITransactor::setClock(CarbonSimClock *clk)
{
   std::cerr << "ERROR xGMII Transactor ID = " << std::dec 
	     << mId << " recieved null clock.  " << std::endl;
}


CarbonXGMIITransactor::CarbonXGMIITransactor():
   mState(eIdle)
{
}

/** data lines **/
bool CarbonXGMIITransactor::bindData(const char *signal_name) {       
   if(not mMaster) {
      assert("Carbon C-API interface not yet supported"==0);
   } else {
      bindData(mMaster->findNet(signal_name));
   }
   return true;
}

bool CarbonXGMIITransactor::bindData(CarbonNetID*) {
   assert("Carbon C-API Net Handles not yet supported" == 0);
   return true;
}
bool CarbonXGMIITransactor::bindData(CarbonSimNet*net)  {
   assert (net);  
   // Should probably check that the width
   mDataNet = net;
   return true;
}

/** control lines **/
bool CarbonXGMIITransactor::bindControl(const char *signal_name) {
   if(not mMaster) {
      assert("Carbon C-API interface not yet supported"==0);
   } else {
      bindControl( mMaster->findNet(signal_name));
   }
   return true;
}
bool CarbonXGMIITransactor::bindControl(CarbonNetID*) {
   assert("Carbon C-API Net Handles not yet supported" == 0);
   return true;
}
bool CarbonXGMIITransactor::bindControl(CarbonSimNet* net)  {
   assert(net);
   // Should probably check that it's 
   // an input, and that it's the right width (4 bits)
   mCtrlNet = net;
   return true;
}

// Local Variables:
// c-basic-offset: 3
// End:
