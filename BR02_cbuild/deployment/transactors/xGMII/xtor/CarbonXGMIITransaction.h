// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONXGMIITRANSACTION_H__
#define __CARBONXGMIITRANSACTION_H__

#include "carbonsim/CarbonSimTransaction.h"

typedef unsigned char    byte;
//typedef unsigned int uint;

class CarbonXGMIITransaction : public CarbonSimTransaction
{
public:
    CarbonXGMIITransaction(UInt8 const *packet, int length);
    ~CarbonXGMIITransaction();

    virtual void step(CarbonSimTransactor * transactor, CarbonTime tick);
    virtual bool isDone(void);
   
    UInt8 getData(int index); // read packet data byte
    int getLength(); // read number of packet data bytes
    int getNumEmptyBytes();
    bool endOfPacket();

    // maybe need some sort of reset to kick the index back to 0?
    // return one byte, advancing index
    UInt8 getNextByte();
    // return 4 bytes, advancing index
    UInt32 getNextWord();


private:
    /* not sure we want to have a defaul construtor... */
    CarbonXGMIITransaction();


    UInt8 const* aPacket;         // array of packet data bytes
    int mLength;  // number of packet bytes
    int mIdx;     // current byte being read...

};

#endif


// Local Variables: ***
// c-basic-offset: 3
// End: ***
