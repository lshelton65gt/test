// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CarbonXGMIITransactor.h"
#include "carbon/carbon_capi.h"
#include <cassert>

void CarbonXGMIIRxTransactor::gotPacket()
{
/*
** Build a transaction and hand the transaction class 
** back to the callback
*/

//   std::cout << " RX ID: " << std::dec << mId 
//	     << "GOT PACKET SIZE: " 
//	     << mByteCount << " data " << std::hex;
//
   // print all bytes that fit nicely in 32 bits
//   int last = 0;
//   for (int i = 0; i < mByteCount / 4; ++i) {
//      std::cout << ((aData[i] & 0x000000FF) >> 0) << " "
//		<< ((aData[i] & 0x0000FF00) >> 8) << " "
//		<< ((aData[i] & 0x00FF0000) >> 16) << " "
//		<< ((aData[i] & 0xFF000000) >> 24) << " ";
//      last = i;
//   }
//
//   for (int i = 0; i < mByteCount % 4; ++i) {
//      std::cout << ((aData[last+1] & (0xFF << (8*i) )) >> (i * 8)) << " ";
//   }
//   std::cout << std::endl;
//
}



void CarbonXGMIIRxTransactor::run()
{
   switch(mState)
   {
   case eIdle:
      if( mCtrl == 0xf && mData == 0x07070707 ) // 4 IDLE ctrl octets
	 mState= eIdle;
      else if( mCtrl == 0x1 && mData == 0x555555fb ) 
	 mState= eSFD; // 3 PREAMBLE data octets, 1 START ctrl octet
      else
	 assert("Invalid IDLE or PREAMBLE data/ctrl");
      break;

   case eSFD:
      if( mCtrl == 0x0 && mData == 0xd5555555 ) {
	 mState= eData; // 1 SFD data octet, 3 PREAMBLE data octets
	 mDataIndex=0;
      }
      else
	 assert("Invalid SFD data/ctrl");
      break;

   case eData:
      if (mCtrl == 0xf)  // if 0xf then there's no data here
      {
	 mByteCount = (mDataIndex * 4);  
	 // This is an EFD packet. 
	 mState = eIdle;

	 gotPacket();	   
      } else {

	 aData[mDataIndex++]= mData; // 4 data octets
	 
	 switch(mCtrl)
	 {
	 case 0x0: break; // continue reading data

	 case 0xe: // 3 ctrl, 1 data byte
	    if( (aData[mDataIndex-1] & 0xffffff00) == 0x0707fd00) { // IITD
	       mState= eIdle;  // ? not sure about that...
	       mByteCount = (mDataIndex * 4) - 3;  
	    } else {
	       std::cerr << "md " << std::hex << mData << " ad[i] "
			 << ( (aData[mDataIndex-1] & 0xffffff00 )  != 0x707fd00 )<< std::endl;
	       assert("Invalid ctrl while reading data" == 0);
	    }
	    break;

	 case 0xc: // 2 ctrl, 2 data bytes
	    if((aData[mDataIndex-1] & 0xffff0000) == 0x07fd0000) { // ITDD
	       mState= eIdle;
	       mByteCount = (mDataIndex * 4) - 2;  
	    } else {
	       assert("Invalid ctrl while reading data" == 0);
	    } 
	    break;

	 case 0x8: // 1 ctrl, 3 data bytes
	    if((aData[mDataIndex-1] & 0xff000000) == 0xfd000000) { // TDDD
	       mState= eIdle;
	       mByteCount = (mDataIndex * 4) - 1;  
	    } else {
	       assert("Invalid ctrl while reading data" == 0);
	    } 
	    break;
	 case 0xf:
	    break;
	 default:
	    assert("Invalid ctrl while reading data"== 0);
	 }
	 
	 if (mCtrl != 0) gotPacket();
      }
      break;
      
   default: // bogus
      assert("invalid state" == 0);
      break;
   }


}

/*
** This is experimental code and shouldn't be used yet...
*/
void CarbonXGMIIRxTransactor::setFsdbFile(CarbonWaveID* w)
{
   mWaveID = w;
#if 0
   carbonAddUserData( mWaveID, eCARBON_VT_VCD_INTEGER, 
		      eCarbonVarDirectionImplicit,
		      eCARBON_DT_VERILOG_INTEGER,
		      0,0,
		      &mFoo, 
		      "mFoo",
		      eCARBON_BYTES_PER_BIT_4B,
		      "xgmii",
		      ".");
		      
#endif		      
}

// Local Variables:
// c-basic-offset: 3
// End:
