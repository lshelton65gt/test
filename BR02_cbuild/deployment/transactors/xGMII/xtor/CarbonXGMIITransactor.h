// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONXGMIITRANSACTOR_H__
#define __CARBONXGMIITRANSACTOR_H__

#include "carbonsim/CarbonSimTransactor.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimMaster.h"
#include "CarbonXGMIITransaction.h"

#include <queue>
#include <cassert>
#include <iostream>

class CarbonSimMaster;
class CarbonSimClock;

class CarbonXGMIITransactor : public CarbonSimTransactor
{
public:
   virtual ~CarbonXGMIITransactor() {}

   void step(CarbonTime tick);
   virtual void idle(UInt64 cycles);

   void setId(int id);
   int getId();

   void setClock(CarbonSimClock *clk);

   void setDDR(bool);
   bool getDDR();


   /*
   ** NET BINDING
   */

   /** data lines **/
   bool bindData(const char *signal_name);
   bool bindData(CarbonNetID*); 
   bool bindData(CarbonSimNet*net);
   bool bindControl(const char *signal_name);
   bool bindControl(CarbonNetID*);
   bool bindControl(CarbonSimNet* net);


protected:
   CarbonXGMIITransactor();

   CarbonXGMIITransactor(CarbonSimMaster*sm) : 
      mState(eIdle),      
      mMaster(sm),
      mClk(0),
      mDDR(true)
      {}

   virtual void observeNets() = 0;
   virtual void depositNets() = 0;
   virtual void run() = 0;
    
   enum enumState {
      eIdle,
      ePreamble,
      eSFD,
      eData,
      eFCS,
      eEFD,
      eIFG
   };

   /* File to add xtor data to */
   CarbonWaveID* mWaveID;
  
   enumState mState;

   /*! Current Packet being transmitted */
   CarbonXGMIITransaction* mCurPacket;

   /** These are the VALUES of the nets **/
   UInt32 mData; // xGMII Data lines, 32 bits
   UInt32 mCtrl; // xGMII Control lines 4 bits

   int mId;

   /** These are the nets themselves **/
   CarbonSimNet *mDataNet;
   CarbonSimNet *mCtrlNet;

   /** CarbonSimMaster.  If 0 we are using the C-API */
   CarbonSimMaster* mMaster;

   /*! SimClock */
   CarbonSimClock* mClk;
   
   /*! True if xtor should operate on both edges */
   bool mDDR;

private:
};

// -------------------------------------------------
//                        TX
// -------------------------------------------------
class CarbonXGMIITxTransactor : public CarbonXGMIITransactor 
{
public:
   CarbonXGMIITxTransactor(CarbonSimMaster *sm)
      : CarbonXGMIITransactor(sm),
 	mIFG(5),
	mIFGCount(0)
      {}
   void submit(CarbonXGMIITransaction * transaction);  
    
private:
   void observeNets();
   void depositNets();
    
   void run(); // used to be transmit, renamed when refactored

   std::queue<CarbonXGMIITransaction*> xmtQueue;

   // Data

   //! InterFrame Gap
   int mIFG, mIFGCount;
};

// -------------------------------------------------
//                        RX
// -------------------------------------------------

class CarbonXGMIIRxTransactor : public CarbonXGMIITransactor 
{
public:
//   void submit(CarbonSimTransaction * transaction);
   CarbonXGMIIRxTransactor(CarbonSimMaster *sm)
      : CarbonXGMIITransactor(sm)
      {}
//   void step (CarbonTime tick);

   void setFsdbFile(CarbonWaveID* w);
   void gotPacket();
private:

   // These fields are temp until I work on the rx
   UInt32 aData[400]; // max packet length = 1548? bytes
   uint mDataLen, mDataIndex, mLen; // length/index of aData[]
   uint mLastCtrl; // last control word for aData[]
//   void receive(); // interface methods to DUT
   // nothing to observe for TX

   int mByteCount;

   void observeNets() { 
      mDataNet->examine(&mData);
      mCtrlNet->examine(&mCtrl);
   }
    
   // Put stuff onto bus...
   void depositNets() { return;    }
   void run(); // interface methods to DUT
};



#endif

// Local Variables:
// c-basic-offset: 3
// End:
