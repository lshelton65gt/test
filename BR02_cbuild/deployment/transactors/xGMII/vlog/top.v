module xgmii(TX_CLK,
	     TXD_0, TXC_0,
	     TXD_1, TXC_1,
	     TXD_2, TXC_2,
	     TXD_3, TXC_3, 
	     RXD_0, RXC_0,
	     	     RXD_3, RXC_3,
	     	     RXD_1, RXC_1,
	     	     RXD_2, RXC_2, 
	     RX_CLK, 
	     reset_n);


   output  [31:0] 	 TXD_0, TXD_1, TXD_2, TXD_3;
   output [3:0] 	 TXC_0, TXC_1, TXC_2, TXC_3;
   output 		 TX_CLK;
   
   input [31:0] 	 RXD_0, RXD_3, RXD_1, RXD_2;
   input [3:0] 		 RXC_0, RXC_3, RXC_1 ,RXC_2;
   
   input 		 RX_CLK;

   input 		 reset_n;
   
   assign 		 TXD_1 = RXD_1;
   assign 		 TXD_2 = RXD_2;
   assign 		 TXD_3 = RXD_3;
   assign 		 TXC_1 = RXC_1;
   assign 		 TXC_2 = RXC_2;
   assign 		 TXC_3 = RXC_3;
   
   assign 		 TX_CLK = RX_CLK;
   
   reg [31:0] 		 data_h;  // carbon observeSignal
   reg [31:0] 		 data_h1;  // carbon observeSignal
   reg [31:0] 		 data_l;  // carbon observeSignal
   reg [31:0] 		 data_l1; // carbon observeSignal
   
   reg [3:0] 		 ctrl_h;  // carbon observeSignal
   reg [3:0] 		 ctrl_h1;  // carbon observeSignal
   reg [3:0] 		 ctrl_l;  // carbon observeSignal
   reg [3:0] 		 ctrl_l1; // carbon observeSignal
   
   
   

   always @(posedge TX_CLK or negedge reset_n) begin
      if(~reset_n) begin
	 data_h <= 0;
	 data_h1 <= 0;

	 ctrl_h <= 0;
	 ctrl_h1 <= 0;
      end
      else begin
	 data_h <= RXD_0;
	 data_h1 <= data_h;      

	 ctrl_h <= RXC_0;
	 ctrl_h1 <= ctrl_h;      
      end

   end
   always @(negedge TX_CLK or negedge reset_n) begin
      if(~reset_n) begin 
	 data_l <= 0;
	 data_l1 <= 0;

	 ctrl_l <= 0;
	 ctrl_l1 <= 0;
      end
      else begin 
	 data_l <= RXD_0;      
	 data_l1 <= data_l;	 

	 ctrl_l <= RXC_0;      
	 ctrl_l1 <= ctrl_l;	 
      end
   end


   assign      TXD_0 = TX_CLK ? data_h1 : data_l1;
   assign      TXC_0 = TX_CLK ? ctrl_h1 : ctrl_l1;
   
   
endmodule



