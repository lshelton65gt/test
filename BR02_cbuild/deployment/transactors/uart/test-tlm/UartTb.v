//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2006 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// $Source: /cvs/repository/deployment/transactors/uart/test-tlm/UartTb.v,v $
// $Id: UartTb.v,v 1.1 2006/09/22 20:16:40 ronk Exp $

`timescale 1ns/100ps

module UartTb (
    clk
  , rst_l

  // modem signals
  , cts_i
  , dsr_i
  , ri_i
  , dcd_i
  , tx_dtr
  , tx_rts
  , tx_baud
  , rx_dtr
  , rx_rts
  , rx_baud
);

input   clk;
input   rst_l;

 input  cts_i;
 input  dcd_i;
 input  dsr_i;
 input   ri_i;
 wire   srx_i;
 wire   stx_o;
output  tx_baud, rx_baud; 
output  tx_dtr,  rx_dtr;
output  tx_rts,  rx_rts;

wire tx_intr, rx_intr;

carbonx_uart UartTx (
    .clk    ( clk    )
  , .rst_l  ( rst_l  )
  , .stx_o  ( stx_o  ) 
  , .srx_i  ( srx_i  ) 
  , .rts_o  ( tx_rts )
  , .cts_i  ( cts_i  )
  , .dtr_o  ( tx_dtr )
  , .dsr_i  ( dsr_i  )
  , .ri_i   ( ri_i   )
  , .dcd_i  ( dcd_i  )
  , .baud_o ( tx_baud )
  , .int_o  ( tx_intr )
);

carbonx_uart UartRx (
    .clk    ( clk    )
  , .rst_l  ( rst_l  )
  , .stx_o  ( srx_i  ) 
  , .srx_i  ( stx_o  ) 
  , .rts_o  ( rx_rts )
  , .cts_i  ( cts_i  )
  , .dtr_o  ( rx_dtr )
  , .dsr_i  ( dsr_i  )
  , .ri_i   ( ri_i   )
  , .dcd_i  ( dcd_i  )
  , .baud_o ( rx_baud )
  , .int_o  ( rx_intr )
);


endmodule 
