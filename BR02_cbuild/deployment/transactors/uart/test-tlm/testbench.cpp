/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// $Source: /cvs/repository/deployment/transactors/uart/test-tlm/testbench.cpp,v $
// $Id: testbench.cpp,v 1.1 2006/09/22 20:16:40 ronk Exp $

#include <cstdio>
#include <cstring>
#include <cerrno>
#include <string>

#include <unistd.h>
#include <fcntl.h>
#include <strings.h>

#include "testbench.h"

using namespace std;

enum { eXXX, eMemRdByte, eMemWrByte, eMemRdWord, eMemWrWord };

enum { DRLSB, DRMSB, FIFO=0, IER, ISR, FCR=2, LCR, MCR, LSR, MSR, SPR,
       DBG1=8, DBG2=12 
     };

enum { DRAB=0x80 }; // Divisor Register Access Bit

/* Debug Registers
 *
 * DBG1
 *
 *  3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 
 *  1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 
 * |---------------------------------------------------------------|
 * |      MCR      |      LCR      |  ISR  |  IER  |      LSR      |
 * |7 6 5 4 3 2 1 0|7 6 5 4 3 2 1 0|3 2 1 0|3 2 1 0|7 6 5 4 3 2 1 0|
 * |---------------------------------------------------------------|
 *
 * DBG2
 *
 *  3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 
 *  1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 
 * |---------------------------------------------------------------|
 * |   UNDEFINED   |FCR|   MCR   | RF_CNT  | RSTATE| TF_CNT  |TSTAT|
 * |7 6 5 4 3 2 1 0|7 6|4 3 2 1 0|4 3 2 1 0|3 2 1 0|4 3 2 1 0|2 1 0|
 * |---------------------------------------------------------------|
 *
 * Legend:
 *    RF_CNT : Rx FIFO Count
 *    RSTATE : Rx State
 *    TF_CNT : Tx FIFO Count
 *    TSTATE : Tx State
 */

Testbench::Testbench(sc_module_name name)
: sc_module(name)
, clk("clk", 68, SC_NS) // 14.7456 MHz = 67.816 nS
, rst_l("rst_l")
, cts_i("cts_i")
, dsr_i("dsr_i")
, ri_i("ri_i")
, dcd_i("dcd_i")
, tx_dtr("tx_dtr")
, tx_rts("tx_rts")
, tx_baud("tx_baud")
, rx_dtr("rx_dtr")
, rx_rts("rx_rts")
, rx_baud("rx_baud")
, uartTxChnl("uartTxChnl")
, uartRxChnl("uartRxChnl")
, uartTx("uartTx")
, uartRx("uartRx")
{

  // Create VHM
  vhm = new UartTb("UartTb");
  assert(vhm);

  vhm->carbonSCWaveInitFSDB();
  vhm->carbonSCDumpVars();
  
  // Connect Clock/Signals to VHM

  vhm->clk     ( clk    );
  vhm->rst_l   ( rst_l  );
  vhm->cts_i   ( cts_i  );
  vhm->dsr_i   ( dsr_i  );
  vhm->ri_i    ( ri_i   );
  vhm->dcd_i   ( dcd_i  );
  vhm->tx_rts  ( tx_rts  );
  vhm->tx_dtr  ( tx_dtr  );
  vhm->tx_baud ( tx_baud );
  vhm->rx_rts  ( rx_rts  );
  vhm->rx_dtr  ( rx_dtr  );
  vhm->rx_baud ( rx_baud );

  const char *tx_intr= "UartTb.tx_intr";
  const char *rx_intr= "UartTb.rx_intr";

  txnet= carbonFindNet(vhm->carbonModelHandle, tx_intr); assert(txnet); 
  rxnet= carbonFindNet(vhm->carbonModelHandle, rx_intr); assert(rxnet); 

  CarbonNetValueCBDataID *ptr;

  ptr=carbonAddNetValueChangeCB(vhm->carbonModelHandle, txIntrCB, this, txnet);
  assert(ptr);

  ptr=carbonAddNetValueChangeCB(vhm->carbonModelHandle, rxIntrCB, this, rxnet);
  assert(ptr);

  uartTxXtor= new CarbonXTransLoopSysC("uartTxXtor", "UartTx");
  uartRxXtor= new CarbonXTransLoopSysC("uartRxXtor", "UartRx");

  // Connect Ports to Channels
  uartTx(uartTxChnl.master_export);
  uartRx(uartRxChnl.master_export);

  // Connect Transactor Objects to Channels
  uartTxXtor->trans_port(uartTxChnl.slave_export);
  uartRxXtor->trans_port(uartRxChnl.slave_export);

  // Configure Process (e.g. reset chip)
  // Configure uarts
  SC_THREAD(configure);

  SC_THREAD(watchdog); // watchdog timer

  SC_THREAD(uartRXactions); // send uartRx xtor commands

  SC_THREAD(uartTXactions); // send uartTx xtor commands
  sensitive << configDone;
  dont_initialize();

  SC_THREAD(uartTXinterrupt); // uartTx interrupt handler

  SC_THREAD(uartRXinterrupt); // uartRx interrupt handler
};

void Testbench::txIntrCB(
     CarbonObjectID *, CarbonNetID *, void *ptr, UInt32 *val, UInt32 *)
{
  Testbench *tb= reinterpret_cast<Testbench *>(ptr);

  tb->txIntrEvent(val);
}

void Testbench::rxIntrCB(
     CarbonObjectID *, CarbonNetID *, void *ptr, UInt32 *val, UInt32 *)
{
  Testbench *tb= reinterpret_cast<Testbench *>(ptr);

  tb->rxIntrEvent(val);
}


// Destructor
Testbench::~Testbench()
{
  cout << "Destructing Testbench." << endl;

  vhm->carbonSCDumpFlush();

  delete vhm;
}

/** ST16C2550 PDF

 TABLE 5: TYPICAL DATA RATES WITH A 14.7456 MHZ CRYSTAL OR EXTERNAL CLOCK
|-------------|-----------------|-------------|-------------|-----------|
| OUTPUT      | DIVISOR FOR 16x | DLM PROGRAM | DLL PROGRAM | DATA RATE |
| Data  Rate  | Clock (Decimal) | VALUE (HEX) | VALUE (HEX) | ERROR (%) |
|-------------|-----------------|-------------|-------------|-----------|
|      400    |     2304        |     09      |     00      |     0     |
|     2400    |      384        |     01      |     80      |     0     |
|     4800    |      192        |     00      |     C0      |     0     |
|     9600    |       96        |     00      |     60      |     0     |
|     19.2k   |       48        |     00      |     30      |     0     |
|     38.4k   |       24        |     00      |     18      |     0     |
|     76.8k   |       12        |     00      |     0C      |     0     |
|    153.6k   |        6        |     00      |     06      |     0     |
|    230.4k   |        4        |     00      |     04      |     0     |
|    460.8k   |        2        |     00      |     02      |     0     |
|    921.6k   |        1        |     00      |     01      |     0     |
|-------------|-----------------|-------------|-------------|-----------|

**/

void Testbench::configure()
{
  CarbonStatus status;

  cout << "Reset chip." << endl;

  rst_l.write(true);  wait( clk.period() *  2 ); // release reset
  rst_l.write(false); wait( clk.period() * 10 ); // assert  reset
  rst_l.write(true);  wait( clk.period() * 10 ); // release reset

  cout << "Beginning Configuring Uarts." 
       << " : " << sc_time_stamp() << endl;

  cts_i = 1;
  dcd_i = 1;
  dsr_i = 1;
   ri_i = 1;

  uartTx.write8(LCR,   DRAB); // enable Divisor Regs
  uartTx.write8(DRLSB, 0x60); // 9600 baud
  uartTx.write8(LCR,   0x1b); // set parity, even par, 1 stop, 8-bits
  uartTx.write8(IER,   0x02); // enable tx interrupt

  uartRx.write8(LCR,   DRAB); // enable Divisor Regs
  uartRx.write8(DRLSB, 0x60); // 9600 baud
  uartRx.write8(LCR,   0x1b); // set parity, even par, 1 stop, 8-bits
  uartRx.write8(IER,   0x01); // enable rx interrupt
//uartRx.write8(FCR,   0x01); // rxfifo trig= 1, enable fifo 
  uartRx.write8(FCR,   0x41); // rxfifo trig= 4, enable fifo 
//uartRx.write8(FCR,   0x81); // rxfifo trig= 8, enable fifo 
//uartRx.write8(FCR,   0xc1); // rxfifo trig=14, enable fifo 

  cout << "Finished Configuring Uarts." 
       << " : " << sc_time_stamp() << endl;

  configDone.notify(SC_ZERO_TIME);
}

void Testbench::uartTXinterrupt()
{
  UInt8 isr, lsr, data;

  while(true) {
    wait( tx_event );

    cout << "TX INTR = " << *txvalue << " : " << sc_time_stamp() << endl;

    isr = uartTx.read8(ISR );
    lsr = uartTx.read8(LSR );
//  data= uartTx.read8(FIFO);

    cout << "TX INTR = " << hex << *txvalue
         << ", ISR= "           << int(isr)
         << ", LSR= "           << int(lsr)
         << " : " << sc_time_stamp() << endl;
  }
}

void Testbench::uartRXinterrupt()
{
  UInt8 isr, lsr, data;

  while(true) {
    wait( rx_event );

    cout << "RX INTR = " << *rxvalue << " : " << sc_time_stamp() << endl;

    isr = uartRx.read8(ISR );
    lsr = uartRx.read8(LSR );
//  data= uartRx.read8(FIFO);

    cout << "RX INTR = " << hex << *rxvalue
         << ", ISR= "           << int(isr)
         << ", LSR= "           << int(lsr)
         << " : " << sc_time_stamp() << endl;
  }
}


// sc_thread -- watchdog timer

void Testbench::watchdog()
{
//wait( clk.period() * 500000 );

  wait( 19, SC_MS );

  cout << "WatchDog Timer Expired: " << sc_time_stamp() << endl;

  sc_stop();
}

uint txcmds[][3] = {
//     cmd   addr data
/** 
  { eMemWrByte,   LCR, DRAB }, 
  { eMemRdByte, DRLSB, 0x00 },
  { eMemRdByte, DRMSB, 0x00 },
 **/
  { eMemRdWord,  DBG1, 0x00 },
  { eMemRdWord,  DBG2, 0x00 },

  { eMemWrByte,  FIFO, 0x81 }, // from uart_test.v
  { eMemWrByte,  FIFO, 0x42 },
  { eMemWrByte,  FIFO, 0xc3 },
  { eMemWrByte,  FIFO, 0x24 },
  { eMemWrByte,  FIFO, 0xa5 },
  { eMemWrByte,  FIFO, 0x66 },
  { eMemWrByte,  FIFO, 0xe7 },
  { eMemWrByte,  FIFO, 0x18 },

  { eMemWrByte,  FIFO, 0xa1 }, 
  { eMemWrByte,  FIFO, 0xa2 },
  { eMemWrByte,  FIFO, 0xa4 },
  { eMemWrByte,  FIFO, 0xa8 },
  { eMemWrByte,  FIFO, 0xae },
  { eMemWrByte,  FIFO, 0xad },
  { eMemWrByte,  FIFO, 0xab },
  { eMemWrByte,  FIFO, 0xa7 },
/**
  { eMemWrByte,  FIFO, 0xc1 }, // attempting to overflow tx fifo
  { eMemWrByte,  FIFO, 0xc2 },
  { eMemWrByte,  FIFO, 0xc4 },
  { eMemWrByte,  FIFO, 0xc8 },
  { eMemWrByte,  FIFO, 0xce },
  { eMemWrByte,  FIFO, 0xcd },
  { eMemWrByte,  FIFO, 0xcb },
  { eMemWrByte,  FIFO, 0xc7 },
**/
  { eMemRdWord,  DBG1, 0x00 },
  { eMemRdWord,  DBG2, 0x00 },

  { eMemRdByte,   IER, 0x0f }, // read status regs
  { eMemRdByte,   ISR, 0x00 },
  { eMemRdByte,   LCR, 0x00 },
  { eMemRdByte,   MCR, 0x00 },
  { eMemRdByte,   LSR, 0x00 },
  { eMemRdByte,   MSR, 0x00 },
  { eMemRdByte,   SPR, 0x00 },

};

uint rxcmds[][3] = {
  { eMemRdWord,  DBG1, 0x00 },
  { eMemRdWord,  DBG2, 0x00 },

  { eMemRdByte,   IER, 0x0f }, // read status regs
  { eMemRdByte,   ISR, 0x00 },
  { eMemRdByte,   LCR, 0x00 },
  { eMemRdByte,   MCR, 0x00 },
  { eMemRdByte,   LSR, 0x00 },
  { eMemRdByte,   MSR, 0x00 },
  { eMemRdByte,   SPR, 0x00 },

  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },
  { eMemRdByte,  FIFO, 0x00 },

  { eMemRdWord,  DBG1, 0x00 },
  { eMemRdWord,  DBG2, 0x00 },
};

static int txcmd_size= sizeof(txcmds) / sizeof(txcmds[0]);
static int rxcmd_size= sizeof(rxcmds) / sizeof(rxcmds[0]);

void Testbench::uartTXactions()
{
  UInt8 data;
  UInt32 word;

  for(int n=0 ; n < txcmd_size; n++ ) {
    switch(txcmds[n][0])
    {
      case eMemRdWord: // UInt8 read32(UInt64 address)
        word= uartTx.read32(txcmds[n][1]);

        cout <<  hex
             <<  "TX RD: addr= " << txcmds[n][1]
             <<        " data= " << word << endl;
        break;

      case eMemRdByte: // UInt8 read8(UInt64 address)
        data= uartTx.read8(txcmds[n][1]);

        cout <<  hex
             <<  "TX RD: addr= " << txcmds[n][1]
             <<        " data= " << int(data) << endl;
        break;

      case eMemWrByte: // void write8(UInt64 address, const UInt8 data)
        uartTx.write8(txcmds[n][1], txcmds[n][2]);

        cout <<  hex
             <<  "TX WR: addr= " << txcmds[n][1]
             <<        " data= " << txcmds[n][2] << endl;
        break;

      default: assert(!"invalid UART Command");
    }

  }
}

void Testbench::uartRXactions()
{
  UInt8 data;
  UInt32 word;

  wait( 18.5, SC_MS );

  for(int n=0; n < rxcmd_size; n++ ) {

    switch(rxcmds[n][0])
    {
      case eMemRdWord: // UInt8 read8(UInt64 address)
        word= uartRx.read32(rxcmds[n][1]);

        cout <<  hex
             <<  "RX RD: addr= " << rxcmds[n][1]
             <<        " data= " << word << endl;
        break;

      case eMemRdByte: // UInt8 read8(UInt64 address)
        data= uartRx.read8(rxcmds[n][1]);

        cout <<  hex
             <<  "RX RD: addr= " << rxcmds[n][1]
             <<        " data= " << int(data) << endl;
        break;

      case eMemWrByte: // void write8(UInt64 address, const UInt8 data)
        uartRx.write8(rxcmds[n][1], rxcmds[n][2]);

        cout <<  hex
             <<  "RX WR: addr= " << rxcmds[n][1]
             <<        " data= " << rxcmds[n][2] << endl;
        break;

      default: assert(!"invalid UART Command");
    }
  }
}

static const char *opcode[] = {
  "UNKNOWN",   //  0
  "NOP",       //  1
  "IDLE",      //  2
  "SLEEP",     //  3
  "READ",      //  4
  "WRITE",     //  5
  "CS_READ",   //  6
  "CS_WRITE",  //  7
  "NXTREQ",    //  8
  "DONE",      //  9
  "FREAD",     // 10
  "FWRITE",    // 11
  "WAKEUP",    // 12
  "CS_RMW",    // 13
  "UNKNOWN",   // 14
  "UNKNOWN",   // 15
};

extern "C" void tbp_print_error_status(void);

int sc_main (int argc , char *argv[]) 
{
  bool traceOn = true;

  CarbonStatus status= carbonSetFilePath(".:../vhm");

  assert(status == eCarbon_OK);

  Testbench * tb = new Testbench("Testbench");

  // Setup the Test
  cout << "--------------------" << endl;
  cout << "--- Running test ---" << endl;
  cout << "--------------------" << endl;
  
  // Run
  sc_start();
  
  // And Report Errors
//tbp_print_error_status();
//UInt32 numErrors = tbp_get_error_count();

  delete tb;
//return numErrors;
  return 0;
}
