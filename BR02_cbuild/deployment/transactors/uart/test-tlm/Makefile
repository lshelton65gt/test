# *****************************************************************************
# Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
# *****************************************************************************

# $Source: /cvs/repository/deployment/transactors/uart/test-tlm/Makefile,v $
# $Id: Makefile,v 1.2 2007/03/28 17:57:19 ronk Exp $

  export CARBON_HOME := /o/release/C2006_10_5065_p12
unexport CARBON_BIN

SBOX := /home/cds/ronk/sandbox

SVM_NAME   = uart

DEBUG ?= 1

# C files dirs & files

CFILES = testbench.cpp 

# default TB_NAME
TB_NAME = UartTb

# C Defines
ifeq ($(DEBUG),1)
LOCAL_CFLAGS = # -DCARBON_DUMP_FSDB 
endif

# Verilog defines
V_DEFS = 
VER_TB_TOP = $(TB_NAME)

# Verilog dirs & files
V_CODE_DIR = ../../v_code

#VER_DIR := $(CARBON_HOME)/lib/xactors/uart/verilog

ifeq ($(DEBUG),1)
VER_LIST := $(VER_TB_TOP).v 
#VER_TOP := ../../../src/xactors/uart/verilog
VER_TOP := ../src/verilog
VER_LIST += $(VER_TOP)/carbonx_uart.v 
VER_LIST += $(VER_TOP)/carbonx_uart_core.v 
VER_LIST += $(CARBON_HOME)/lib/xactors/common/verilog/XIF_core.vp
#VER_LIST += $(SBOX)/src/xactors/common/verilog/XIF_core.v

TEST_ARGS = -trace
else
VER_LIST := $(VER_TB_TOP).v 
#VER_DIR := $(CARBON_HOME)/lib/xactors/uart/verilog
VER_DIR := ../../../src/xactors/uart/verilog
VER_LIST += $(VER_DIR)/carbonx_uart.vp
VER_LIST += $(VER_DIR)/carbonx_uart_core.vp
endif

VER_SWITCH := +librescan +libext+.v +libext+.vp

SYSC  := /tools/systemc-2.1.v1-b/linux/gcc-3.4.3
TLM   := /tools/TLM-2005-04-08/tlm

include $(CARBON_HOME)/makefiles/Makefile.carbon.vsp

# Carbon Top Level Include Directory
CARBON_INC_DIR  = ${CARBON_HOME}/include

# VSPX Setup
VSPX_LIB      = carbonvspx
VSPX_LIB_DIR  = $(CARBON_HOME)/$(CARBON_TARGET_ARCH)/lib/$(CARBON_COMPILER)

ifeq ($(DEBUG),1)
VSPX_VINC_DIR = ../../../src/xactors/common/verilog
else
VSPX_VINC_DIR = ${CARBON_VSPX}/common/verilog
endif

VSPX_DIR_FILE = $(CARBON_HOME)/lib/xactors/common/carbonX.dir

# CBUILD Arguments
CFLAGS    = -DTBP_SIMULATION -DTBP_CDS $(LOCAL_CFLAGS) -g -O0
CBUILD_DEFS = +define+TBP_SIMULATION +define+TBP_CDS $(V_DEFS)
INCLUDES := -I. -I${SYSC}/include -I$(TLM) -I$(CARBON_INC_DIR) 
INCLUDES += -I$(CARBON_INC_DIR) -I$(CARBON_VSPX) -I$(CARBON_TARGET_ARCH)

# strip out c files
CPPFILES = ${CFILES:%.c=}
# strip out cc files
CCFILES = ${CFILES:%.cpp=}

CPP_OBJECT_FILES = ${CPPFILES:%.cpp=${CARBON_TARGET_ARCH}/%.o}
C_OBJECT_FILES   = ${CCFILES:%.c=${CARBON_TARGET_ARCH}/%.o}
TEST_OBJS        = ${C_OBJECT_FILES} ${CPP_OBJECT_FILES}

VER_SWITCH += +incdir+$(VSPX_VINC_DIR)
VER_SWITCH += -y $(VSPX_VINC_DIR)
VER_SWITCH += +incdir+$(CARBON_HOME)/lib/xactors/common/verilog
VER_SWITCH += +incdir+$(CARBON_HOME)/lib/xactors/uart/verilog


ifeq ($(CARBON_TARGET_ARCH),Win)
CARBON_RUN_TEST = $(CARBON_HOME)/scripts/runtest
else
CARBON_RUN_TEST =
endif


# ------------------------------------------------------------
# Makefile targets
#default : ${CARBON_TARGET_ARCH}/sim_run

default : run_testbench

run_% : ${CARBON_TARGET_ARCH}/sim_run
	cd ${CARBON_TARGET_ARCH};  $(CARBON_RUN_TEST) ./sim_run ${TEST_ARGS} $*

${CARBON_TARGET_ARCH}/sim_run : ${CARBON_TARGET_ARCH}/.dummy \
				${CARBON_TARGET_ARCH}/libdesign.a \
				${CARBON_HOST_ARCH}/libdesign.systemc.o \
				${TEST_OBJS} \
				$(VSPX_LIB_DIR)/lib$(VSPX_LIB).a
	$(CARBON_CXX) \
	-g -O0 $(TEST_OBJS) -o $@ ${CARBON_TARGET_ARCH}/libdesign.a \
	${CARBON_HOST_ARCH}/libdesign.systemc.o \
	-L$(SYSC)/lib-linux -lsystemc \
	$(CARBON_LIB_LIST)

${CARBON_TARGET_ARCH}/%.o: %.cpp ${CARBON_TARGET_ARCH}/.dummy
	$(CARBON_CC)  -g -c  $< $(INCLUDES) $(CFLAGS) -o $@

${CARBON_TARGET_ARCH}/%.o: %.c ${CARBON_TARGET_ARCH}/.dummy
	$(CARBON_CC)  -g -c  $< $(INCLUDES) $(CFLAGS) -o $@

${CARBON_TARGET_ARCH}/libdesign.a: ${CARBON_TARGET_ARCH}/.dummy $(VER_LIST) $(VSPX_DIR_FILE)
	$(CBUILD) $(VER_LIST) $(VER_SWITCH) $(CBUILD_DEFS) \
	-g -systemCWrapper -vlogTop $(VER_TB_TOP) -directive $(VSPX_DIR_FILE) -enableOutputSysTasks -o $@

${CARBON_HOST_ARCH}/libdesign.systemc.o : ${CARBON_HOST_ARCH}/libdesign.systemc.cpp 
	$(CARBON_CC) -c  $< ${INCLUDES} ${CFLAGS} -o $@ \
	 -DCARBON_SCHED_ALL_CLKEDGES=1

${CARBON_TARGET_ARCH}/.dummy : ; mkdir -p ${CARBON_TARGET_ARCH}; touch $@

# -------------- housekeeping functions ------------

debussy : ; /tools/debussy/bin/debussy -ssy -ssv $(VER_LIST) $(VER_SWITCH) &

rebuild : ; ../../../scripts/mkcds

clean_obj: ; rm -rf ${CARBON_TARGET_ARCH}/*.o $(ARB_SAT)/*.o

clean: clean_obj ; rm -rf ${CARBON_TARGET_ARCH} $(ARB_SAT) 

