// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// $Source: /cvs/repository/deployment/transactors/uart/test-tlm/testbench.h,v $
// $Id: testbench.h,v 1.1 2006/09/22 20:16:40 ronk Exp $

#ifndef __Testbench_h__
#define __Testbench_h__

#include "systemc.h"
#include "libdesign.systemc.h"
#include "xactors/systemc/CarbonXIfs.h"
#include "xactors/systemc/CarbonXTransLoopSysC.h"
#include "xactors/systemc/CarbonXTlmReqRspChannel.h"
#include "xactors/systemc/CarbonXTlmMasterPort.h"

SC_MODULE(Testbench)
{
  // Signals

  sc_clock          clk;    // UartTb.clk
  sc_signal<bool>   rst_l;  // UartTb.rst_l
  sc_signal<bool>   cts_i;  // UartTb.cts_i
  sc_signal<bool>   dsr_i;  // UartTb.dsr_i
  sc_signal<bool>   ri_i;   // UartTb.ri_i
  sc_signal<bool>   dcd_i;  // UartTb.dcd_i
  sc_signal<bool>   tx_dtr;  // UartTb.dtr_o
  sc_signal<bool>   tx_rts;  // UartTb.rts_o
  sc_signal<bool>   tx_baud; // UartTb.baud_o
  sc_signal<bool>   rx_dtr;  // UartTb.dtr_o
  sc_signal<bool>   rx_rts;  // UartTb.rts_o
  sc_signal<bool>   rx_baud; // UartTb.baud_o

  // Constructor
  Testbench(sc_module_name name);

  // Transaction Channels
  CarbonXTlmReqRspChannel<CarbonXTransReqT, CarbonXTransRespT>
    uartTxChnl, uartRxChnl;

  // Transaction Ports
  CarbonXTlmMasterPort uartTx, uartRx;

  // Transactor Modules
  CarbonXTransLoopSysC *uartTxXtor, *uartRxXtor;

  SC_HAS_PROCESS(Testbench);
  
  // Destructor
  ~Testbench();

  // VHM Object
  UartTb *vhm;

  // Processes
  void configure(void);
  void watchdog(void);
  void uartTXactions(void);
  void uartRXactions(void);
  void uartTXinterrupt(void);
  void uartRXinterrupt(void);

  void txIntrEvent(UInt32 *val) { txvalue= val; tx_event.notify(SC_ZERO_TIME); }
  void rxIntrEvent(UInt32 *val) { rxvalue= val; rx_event.notify(SC_ZERO_TIME); }

private:
  CarbonNetID *txnet, *rxnet;
  UInt32 *txvalue, *rxvalue;

  static void txIntrCB(
     CarbonObjectID *, CarbonNetID *, CarbonClientData, UInt32 *, UInt32 *);

  static void rxIntrCB(
     CarbonObjectID *, CarbonNetID *, CarbonClientData, UInt32 *, UInt32 *);

  sc_event tx_event, rx_event, configDone;

};

#endif
  

