//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2006 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Description
//
// UART 16550 transactor

`timescale 1 ns / 100 ps

module carbonx_uart (
    clk
  , rst_l

  // UART  signals
  , stx_o // serial output
  , srx_i // serial  input

  // modem signals
  , rts_o
  , cts_i
  , dtr_o
  , dsr_i
  , ri_i
  , dcd_i
  , baud_o

  // interrupt request
  , int_o
);

/**********************************
** List all Input/Output signals **
**********************************/

input   clk;
input   rst_l;

// UART  signals
 input  srx_i;
output  stx_o;
output  rts_o;
 input  cts_i;
output  dtr_o;
 input  dsr_i;
 input   ri_i;
 input  dcd_i;

output  baud_o; // optional 
output   int_o; // interrupt request

`protect
// car//bon license crbn_vsp_exec_xtor//_uart

// We need this handful of signals to properly configure the
// transactor interface.
parameter    
ClockId   =   1,
DataWidth =  32,
DataAdWid =   9, // Max is 15
GCSwid    = 128, // Minimum is 1.
PCSwid    =  32; // Minimum is 1.

//
// Include XIF.
//

`include "xif_core.vh"

//
// Internal registers and wires.
//

wire reset;

// WISHBONE interface
wire  [4:0] wb_adr_i = XIF_get_address;
wire [31:0] wb_dat_i = XIF_get_data;
wire [31:0] wb_dat_o;
reg         wb_we_i;  // write enable; rd=0, wr=1
reg         wb_stb_i;
reg         wb_cyc_i;
reg   [3:0] wb_sel_i; // byte selects 
wire        wb_ack_o;
wire        int_o;    // interrupt request

wire  [3:0] uart_size = XIF_get_size;
reg         uart_xrun, uart_dwe;
reg [2:0]   curState;

// .get_size(XIF_get_size),  
// .get_status(XIF_get_status),
// .get_csdata(XIF_get_csdata[63:0]),

// These are all outputs from the two state machines that get muxed
// on their way up to the C side
wire [31:0]     uart_status= 0;
wire [31:0]     uart_opcode;

/**********************************
** Internal wires/state
**********************************/

//
// Continuous assignments.
//

assign  BFM_clock = clk;
assign  BFM_reset = ~rst_l;

// Reset internal logic from external input or from SW
assign      reset     = ~rst_l | XIF_reset;

assign  BFM_put_operation = BFM_NXTREQ;
assign  BFM_put_data      = wb_dat_o;
assign  BFM_put_dwe       = wb_ack_o; // uart_dwe;
assign  BFM_put_status    = uart_status;
assign  BFM_xrun          = uart_xrun;
assign  BFM_gp_daddr      = 0; // word index ptr into get/put data array
assign  BFM_put_size      = 1; // byte size
assign  BFM_put_address   = 0;
assign  BFM_put_csdata    = 0;
assign  BFM_interrupt     = int_o; // 0;

assign  uart_opcode = XIF_get_operation;
   
/*************************************
 * State machine parameters         **
 *************************************/
parameter
  UART_IDLE       = 3'h0,
  UART_READ       = 3'h1,
  UART_READ_ACK   = 3'h2,
  UART_WRITE      = 3'h3,
  UART_WRITE_ACK  = 3'h4;

/**********************************
 ** State machine                 **
 **********************************/
always @(posedge clk or posedge reset)
  begin
    if (reset)
      begin
        wb_we_i   <=  1'b0;  // write enable; rd=0, wr=1
        wb_stb_i  <=  1'b0;
        wb_cyc_i  <=  1'b0;
        wb_sel_i  <=  4'h0; // byte selects 
        uart_xrun <=  1'b0;
        uart_dwe  <=  1'b0;

        curState  <= UART_IDLE;

//      pciStatus <= STATUS__NORMAL;
//      reading   <= 1'b0;
//      writing   <= 1'b0;
      end
    else
      begin
        case (curState)
    
          UART_IDLE : begin
            case (uart_opcode)
              BFM_NOP : begin
                curState  <= UART_IDLE;
                uart_xrun <= 1'b0;
              end

              BFM_READ : begin
                curState  <= UART_READ;
                uart_xrun <= 1'b1;
              end

              BFM_WRITE : begin
                curState  <= UART_WRITE;
                uart_xrun <= 1'b1;
              end

              default : begin
                curState  <= UART_IDLE;
                uart_xrun <= 1'b0;
              end
            endcase
          end

          UART_READ  : begin
            wb_we_i   <= 1'b0;  // write enable; rd=0, wr=1
            wb_stb_i  <= 1'b1;
            wb_cyc_i  <= 1'b1;
            wb_sel_i  <= uart_size == 1 ? 4'h1 : 4'hf; // byte selects 
            curState  <= UART_READ_ACK;
          end

          UART_READ_ACK  : begin
            if(wb_ack_o) begin
              wb_we_i   <= 1'b0;  // write enable; rd=0, wr=1
              wb_stb_i  <= 1'b0;
              wb_cyc_i  <= 1'b0;
              wb_sel_i  <= 4'h0; // byte selects 
              curState  <= UART_IDLE;
              uart_xrun <= 1'b0;
            end
          end

          UART_WRITE : begin
            wb_we_i   <= 1'b1;  // write enable; rd=0, wr=1
            wb_stb_i  <= 1'b1;
            wb_cyc_i  <= 1'b1;
            wb_sel_i  <= uart_size == 1 ? 4'h1 : 4'hf; // byte selects 
            curState  <= UART_READ_ACK;
            curState  <= UART_WRITE_ACK;
          end

          UART_WRITE_ACK  : begin
            if(wb_ack_o) begin
              wb_we_i   <= 1'b0;  // write enable; rd=0, wr=1
              wb_stb_i  <= 1'b0;
              wb_cyc_i  <= 1'b0;
              wb_sel_i  <= 4'h0; // byte selects 
              curState  <= UART_IDLE;
              uart_xrun <= 1'b0;
            end
          end
      
          default : curState <= UART_IDLE;
     
        endcase
      end
  end  // always


uart_top uart_core  (
    .wb_clk_i    ( clk   )
  , .wb_rst_i    ( reset )
  
  // Wishbone signals
  , .wb_adr_i    ( wb_adr_i )
  , .wb_dat_i    ( wb_dat_i )
  , .wb_dat_o    ( wb_dat_o )
  , .wb_we_i     ( wb_we_i  )
  , .wb_stb_i    ( wb_stb_i )
  , .wb_cyc_i    ( wb_cyc_i )
  , .wb_ack_o    ( wb_ack_o )
  , .wb_sel_i    ( wb_sel_i )

  // interrupt request
  , .int_o       ( int_o  )

  // UART  signals
  // serial input/output
  , .stx_pad_o   ( stx_o  )
  , .srx_pad_i   ( srx_i  )

  // modem signals
  , .rts_pad_o   ( rts_o  )
  , .cts_pad_i   ( cts_i  )
  , .dtr_pad_o   ( dtr_o  )
  , .dsr_pad_i   ( dsr_i  )
  , .ri_pad_i    ( ri_i   )
  , .dcd_pad_i   ( dcd_i  )
  , .baud_o      ( baud_o )
  );

`endprotect
endmodule
