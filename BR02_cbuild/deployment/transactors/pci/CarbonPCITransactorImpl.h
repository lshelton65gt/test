// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONPCITRANSACTORIMPL_H__
#define __CARBONPCITRANSACTORIMPL_H__

class CarbonSimMaster;
class CarbonSimTransaction;
class CarbonSimClock;
namespace CarbonDeployment {
class PinManager;
class PinWrapper;
class PCIMaster;
class PCITarget;
class PCIConfig;
}

#include "CarbonPCITransactor.h"  // for some enums
#include "PinWrapper.h"  // for some enums
#include "carbon/carbon_shelltypes.h"

class CarbonPCITransactorImpl
{
public:
    CarbonPCITransactorImpl(CarbonSimMaster *sm,
			    bool pullups_en = true,
			    bool use_force = false,
			    bool multi_xtor_mode = false);
    CarbonPCITransactorImpl(CarbonObjectID *id,
			    bool pullups_en = true,
			    bool use_force = false,
			    bool multi_xtor_mode = false);
    ~CarbonPCITransactorImpl();

    void step(CarbonTime tick);
    void idle(UInt64 cycles);
    void submit(CarbonSimTransaction * transaction);
    void sample();

    void setClock(CarbonSimClock *clk);
    bool bindRstL(const char *signal_name);
    bool bindAD(const char *signal_name);
    bool bindCBE(const char *signal_name);
    bool bindPar(const char *signal_name);
    bool bindPar64(const char *signal_name);
    bool bindFrameL(const char *signal_name);
    bool bindIrdyL(const char *signal_name);
    bool bindTrdyL(const char *signal_name);
    bool bindStopL(const char *signal_name);
    bool bindDevselL(const char *signal_name);
    bool bindIdsel(const char *signal_name);
    bool bindPerrL(const char *signal_name);
    bool bindSerrL(const char *signal_name);
    bool bindReqL(const char *signal_name);
    bool bindGntL(const char *signal_name);
    bool bindReq64L(const char *signal_name);
    bool bindAck64L(const char *signal_name);
    bool bindIntAL(const char *signal_name);
    bool bindIntBL(const char *signal_name);
    bool bindIntCL(const char *signal_name);
    bool bindIntDL(const char *signal_name);

    void setDecodeSpeed(CarbonPCITransactor::DecodeSpeed ds);
    void setTarget64Bit(bool en);
    void setFb2bMode(bool en);
    void setBarMask(UInt32 bar, UInt32 mask, bool mem, UInt32 func);
    void setBarMask64(UInt32 bar, UInt64 mask, UInt32 func);

    void writeConfigSpace(UInt32 func, UInt32 offset, UInt32 data, UInt32 mask);
    UInt32 readConfigSpace(UInt32 func, UInt32 offset);

    // Registering read/write callbacks
    void setReadHandler(UInt32 (*cb_func)(UInt32, UInt32, UInt32, void *), void *cb_data);
    void setWriteHandler(void (*cb_func)(UInt32, UInt32, UInt32, UInt32, UInt32, void *), void *cb_data);

    // Registers a callback function to be run when a transaction begins
    void onTransactionStart(void (*cb_func)(UInt64, UInt8, void*), void *cb_data);

    // Initiator special behavior
    void setSubsequentIRDYDelay(UInt32 value);
    void setInitialIRDYDelay(UInt32 value);
    void setFRAMEDelay(UInt32 value);
    void setReqDelay(UInt32 value);

    // Target special behavior
    void setSubsequentTRDYDelay(UInt32 value);
    void setInitialTRDYDelay(UInt32 value);
    void setTargetResponse(CarbonPCITransactor::TargetResponse type, UInt32 value);
    void setRepeatTargetResponses(UInt32 value);
    void setTargetBurstLength(UInt32 value);


protected:
    // Sim master
    CarbonSimMaster * const mSimMaster;

    // Carbon object pointer (for simmasterless use)
    CarbonObjectID * const mObj;

    // CarbonSimClock this transactor runs on
    CarbonSimClock *mClk;

    // Smart pin manager and wrappers for interface with object
    CarbonDeployment::PinManager *mPinMgr;
    CarbonDeployment::PinWrapper mRst;
    CarbonDeployment::PinWrapper mAD;
    CarbonDeployment::PinWrapper mCBE;
    CarbonDeployment::PinWrapper mPar;
    CarbonDeployment::PinWrapper mPar64;
    CarbonDeployment::PinWrapper mFrame;
    CarbonDeployment::PinWrapper mIrdy;
    CarbonDeployment::PinWrapper mTrdy;
    CarbonDeployment::PinWrapper mStop;
    CarbonDeployment::PinWrapper mDevsel;
    CarbonDeployment::PinWrapper mIdsel;
    CarbonDeployment::PinWrapper mPerr;
    CarbonDeployment::PinWrapper mSerr;
    CarbonDeployment::PinWrapper mReq;
    CarbonDeployment::PinWrapper mGnt;
    CarbonDeployment::PinWrapper mReq64;
    CarbonDeployment::PinWrapper mAck64;
    CarbonDeployment::PinWrapper mIntA;
    CarbonDeployment::PinWrapper mIntB;
    CarbonDeployment::PinWrapper mIntC;
    CarbonDeployment::PinWrapper mIntD;
    
    // Master and target components
    CarbonDeployment::PCIMaster *mMaster;
    CarbonDeployment::PCITarget *mTarget;
    CarbonDeployment::PCIConfig *mConfig;

    // Configuration options
    const bool mPullupsEn;
    const bool mUseForce;
    const bool mMultiXtorMode;

    // Public static utilities
public:
    static bool carbon_redxor(UInt32 x);
};

#endif
