// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "PCIMasterSM.h"
#include "PinWrapper.h"
#include <iostream>

using namespace CarbonDeployment;

PCIMasterSM::PCIMasterSM(const CarbonDeployment::PinWrapper &rst,
			 const CarbonDeployment::PinWrapper &gnt,
			 const CarbonDeployment::PinWrapper &trdy,
			 const CarbonDeployment::PinWrapper &frame,
			 const CarbonDeployment::PinWrapper &irdy,
			 const CarbonDeployment::PinWrapper &stop)
    : mRst(rst),
      mGnt(gnt),
      mTrdy(trdy),
      mFrame(frame),
      mIrdy(irdy),
      mStop(stop)
{
    mState = mState_d1 = mState_d2 = mState_d3 = IDLE;
}

PCIMasterSM::~PCIMasterSM()
{
}

PCIMasterSM::State PCIMasterSM::currState()
{
    return mState;
}

PCIMasterSM::State PCIMasterSM::stateD1()
{
    return mState_d1;
}

PCIMasterSM::State PCIMasterSM::stateD2()
{
    return mState_d2;
}

PCIMasterSM::State PCIMasterSM::stateD3()
{
    return mState_d3;
}

void PCIMasterSM::transition(bool lastCycWr,
			     bool sameTgt,
			     bool request,
			     bool step,
			     bool dev_to,
			     bool fb2b_en,
			     bool need_dac,
			     CarbonTime tick)
{
    State next_state = mState;

    switch (mState) {
    case IDLE:
	if(request && !step && !mGnt && mFrame && mIrdy)
	    // There is a request pending for us to put on the
	    // Put it out there
	    next_state = ADDR;
	else if (((request && step) || !request) && !mGnt && mFrame && mIrdy)
	       // There is no request to put out but we still have grant
	       // so we'll need to drive the bus
	    next_state = DR_BUS;
	break;
    case ADDR:
	if (need_dac)
	    next_state = DUAL_ADDR;
	else
	    next_state = M_DATA;
	break;
    case DUAL_ADDR:
	next_state = M_DATA;
	break;
    case M_DATA:
	if(!mFrame || (mFrame && mTrdy && mStop && !dev_to))
	       // !!!! not sure this transition makes tons of sense to me...
	       // Sure it does!  TRDY/STOP stall on the last data phase.
	    next_state = M_DATA;	
	else if ((request && !step) && !mGnt && mFrame && !mTrdy && mStop &&
		 lastCycWr && ( sameTgt || fb2b_en))
	       // for back 2 back transactions we can go right to ADDR
	    next_state = ADDR;
	else if ((mFrame && !mStop) || (mFrame && dev_to))
	    next_state = S_TAR;
	else 
	    next_state = TURN;
	break;
    case S_TAR:
	if (!mGnt)
	    next_state = DR_BUS;
	else
	    next_state = IDLE;	     
	break;
    case TURN:
	if (((request && step) || !request) && !mGnt)
	    next_state = DR_BUS;
	else if (request && !step && !mGnt)
	    next_state = ADDR;
	else if (mGnt)
	    next_state = IDLE;
	break;
    case DR_BUS:
	if (((request && step) || !request) && !mGnt)
	    next_state = DR_BUS;
	else if (request && !step && !mGnt)
	    next_state = ADDR;
	else if (mGnt)
	    next_state = IDLE;
	break;
    default:
	next_state = BAD_STATE;
    }

    if (!mRst)
	mState = mState_d1 = mState_d2 = mState_d3 = IDLE;
    else {
	if (scDebug && (next_state != mState))
	    std::cout << "PCIMasterSM @" << std::dec << tick << " : " << mState << " -> " << next_state << std::endl;
	mState_d3 = mState_d2;
	mState_d2 = mState_d1;
	mState_d1 = mState;
	mState = next_state;
    }
	
}

