// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __PINWRAPPER_H__
#define __PINWRAPPER_H__

#include "carbon/carbon_shelltypes.h"

class CarbonSimNet;

namespace CarbonDeployment {

class PinWrapper
{
    friend class PinManager;

protected:
    PinWrapper(CarbonSimNet *net);
    PinWrapper(CarbonNetID *net, CarbonObjectID *obj);

public:
    PinWrapper();
    ~PinWrapper();
    UInt32 read(UInt32 index = 0) const;
    void write(UInt32 val);
    const UInt32 *readBig() const;
    void writeBig(UInt32 *val);
    bool isDriven() const;
    void unDrive();
    void pullUp();
    void pullDown();
    void unPull();
    void useForce();

    // Cast to/assign from UInt32 for code simplicity
    operator UInt32() const;
    PinWrapper &operator=(UInt32 val);
    UInt32 operator[](UInt32 index) const;

protected:
    void examine();
    void depositorforce();

    void depositorforce(const UInt32 *val, bool drive);
    void deposit(const UInt32 *val, bool drive);
    void force(const UInt32 *val);
    void release();

    void init();

    // Is there a valid net?
    bool isValid() const;

    // Set the net pointer if the net is currently invalid
    bool setNet(CarbonSimNet *net);
    bool setNet(CarbonNetID *net, CarbonObjectID *obj);

    UInt32 *mReadVal, *mWriteVal;
    bool mReadDrive, mWriteDrive;
    UInt32 *mOne, *mZero, *mTemp;
    UInt32 mNumUInt;
    CarbonSimNet *mSimNet;
    CarbonObjectID *mObj;
    CarbonNetID *mNetID;

    typedef enum {
	NONE,
	UP,
	DOWN
    } PullType;
    PullType mPull;

    bool mIsInput, mIsTristate;
    bool mHasDriver;	// Has there been a request to drive this signal since the last depositorforce()?
    bool mPrevDriving;	// Did we drive this signal on the previous depositorforce()?

    bool mUseForce;
};

}

#endif
