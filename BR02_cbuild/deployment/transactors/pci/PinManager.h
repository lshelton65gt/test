// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __PINMANAGER_H__
#define __PINMANAGER_H__

#include <vector>

namespace CarbonDeployment {
class PinWrapper;
}
class CarbonSimMaster;
typedef struct carbon_model_descr CarbonObjectID;

namespace CarbonDeployment {

class PinManager
{
public:
    PinManager(CarbonSimMaster *sm);
    PinManager(CarbonObjectID *obj);
    ~PinManager();

    bool addPin(const char *name, PinWrapper &pin);
    void examine();
    void deposit();

protected:
    CarbonSimMaster *mSimMaster;
    CarbonObjectID *mObj;
    std::vector<CarbonDeployment::PinWrapper *> mPins;
};

}

#endif
