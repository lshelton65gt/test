// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __PCITARGETSM_H__
#define __PCITARGETSM_H__

namespace CarbonDeployment {
class PinWrapper;
}

#include "carbon/carbon_shelltypes.h"

namespace CarbonDeployment {

class PCITargetSM
{
public:
    PCITargetSM(const CarbonDeployment::PinWrapper &rst,
		const CarbonDeployment::PinWrapper &frame,
		const CarbonDeployment::PinWrapper &devsel,
		const CarbonDeployment::PinWrapper &irdy,
		const CarbonDeployment::PinWrapper &trdy,
		const CarbonDeployment::PinWrapper &stop);
    ~PCITargetSM();

    typedef enum {
	IDLE,		// bus idle
	B_BUSY,		// bus busy
	S_DATA,		// data
	BACKOFF,	// backoff after target term
	TURN,		// turn around cycle
	BAD_STATE
    } State;

    State currState();
    State stateD1();
    State stateD2();
    void transition(bool hit,
		    bool decode_done,
		    bool term,
		    bool ready,
		    CarbonTime tick);

protected:
    State mState, mState_d1, mState_d2;

    const CarbonDeployment::PinWrapper &mRst;
    const CarbonDeployment::PinWrapper &mFrame;
    const CarbonDeployment::PinWrapper &mDevsel;
    const CarbonDeployment::PinWrapper &mIrdy;
    const CarbonDeployment::PinWrapper &mTrdy;
    const CarbonDeployment::PinWrapper &mStop;

    static const bool scDebug = false;
};

}

#endif
