// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "PinManager.h"
#include "PinWrapper.h"
#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbon/carbon_capi.h"
#include <iostream>

using namespace CarbonDeployment;

PinManager::PinManager(CarbonSimMaster *sm)
    : mSimMaster(sm),
      mObj(0)
{
}

PinManager::PinManager(CarbonObjectID *obj)
    : mSimMaster(0),
      mObj(obj)
{
}

PinManager::~PinManager()
{
    for (std::vector<PinWrapper *>::iterator iter = mPins.begin(); iter != mPins.end(); ++iter)
	delete (*iter);
}

bool PinManager::addPin(const char *name, PinWrapper &pin)
{
    // Are we using CarbonSim or standard C api?
    if (mSimMaster) {
	CarbonSimNet *net = mSimMaster->findNet(name);
	if (!net)
	    return false;
	pin.setNet(net);
    } else {
	CarbonNetID *net = carbonFindNet(mObj, name);
	if (!net)
	    return false;
// 	std::cout << "addPin: " << name << std::endl;
	pin.setNet(net, mObj);
    }

    mPins.push_back(&pin);
    return pin.isValid();
}

void PinManager::examine()
{
    for (std::vector<PinWrapper *>::iterator iter = mPins.begin(); iter != mPins.end(); ++iter)
	(*iter)->examine();
}

void PinManager::deposit()
{
    for (std::vector<PinWrapper *>::iterator iter = mPins.begin(); iter != mPins.end(); ++iter)
	(*iter)->depositorforce();
}
