// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __PCIMASTERSM_H__
#define __PCIMASTERSM_H__

namespace CarbonDeployment {
class PinWrapper;
}

#include "carbon/carbon_shelltypes.h"

namespace CarbonDeployment {

class PCIMasterSM
{
public:
    PCIMasterSM(const CarbonDeployment::PinWrapper &rst,
		const CarbonDeployment::PinWrapper &gnt,
		const CarbonDeployment::PinWrapper &trdy,
		const CarbonDeployment::PinWrapper &frame,
		const CarbonDeployment::PinWrapper &irdy,
		const CarbonDeployment::PinWrapper &stop);
    ~PCIMasterSM();

    typedef enum {
	IDLE,		// bus idle
	ADDR,		// address phase
	M_DATA,		// data
	S_TAR,		// target asserted stop 
	TURN,		// turn around cycle
	DR_BUS,		// drive the bus, we have grant
	DUAL_ADDR,
	BAD_STATE
    } State;

    State currState();
    State stateD1();
    State stateD2();
    State stateD3();
    void transition(bool lastCycWr,	// Was the last cycle a write?
		    bool sameTgt,	// Using the same target as the last trans
		    bool request,	// Do we have a pending request to put onto the bus?
		    bool step,		// Address stepping?
		    bool dev_to,
		    bool fb2b_en,
		    bool need_dac,
		    CarbonTime tick);

protected:
    State mState, mState_d1, mState_d2, mState_d3;

    const CarbonDeployment::PinWrapper &mRst;
    const CarbonDeployment::PinWrapper &mGnt;
    const CarbonDeployment::PinWrapper &mTrdy;
    const CarbonDeployment::PinWrapper &mFrame;
    const CarbonDeployment::PinWrapper &mIrdy;
    const CarbonDeployment::PinWrapper &mStop;

    static const bool scDebug = false;
};

}

#endif
