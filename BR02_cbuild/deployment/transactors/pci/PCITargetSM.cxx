// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "PCITargetSM.h"
#include "PinWrapper.h"
#include <iostream>

using namespace CarbonDeployment;

PCITargetSM::PCITargetSM(const CarbonDeployment::PinWrapper &rst,
			 const CarbonDeployment::PinWrapper &frame,
			 const CarbonDeployment::PinWrapper &devsel,
			 const CarbonDeployment::PinWrapper &irdy,
			 const CarbonDeployment::PinWrapper &trdy,
			 const CarbonDeployment::PinWrapper &stop)
    : mRst(rst),
      mFrame(frame),
      mDevsel(devsel),
      mIrdy(irdy),
      mTrdy(trdy),
      mStop(stop)

{
    mState = mState_d1 = mState_d2 = IDLE;
}

PCITargetSM::~PCITargetSM()
{
}

PCITargetSM::State PCITargetSM::currState()
{
    return mState;
}

PCITargetSM::State PCITargetSM::stateD1()
{
    return mState_d1;
}

PCITargetSM::State PCITargetSM::stateD2()
{
    return mState_d2;
}

void PCITargetSM::transition(bool hit,
			     bool decode_done,
			     bool term,
			     bool ready,
			     CarbonTime tick)
{
    State next_state = mState;

    switch (mState) {
    case IDLE:
    case TURN:
	if(mFrame)
	    // No transaction starting
	    next_state = IDLE;
	else if (!mFrame && !decode_done)
	    // New transaction, waiting to decode
	    // (The spec uses hit instead of dec_done, but I currently have hit hardwired
	    // to 1, so it won't work.  I think this will accomplish the same thing).
	    next_state = B_BUSY;
	else if (!mFrame && decode_done && hit)
	    // OK - the actual next-state equations in the spec don't list this,
	    // but the discussion following it mentions the ability to go to S_DATA
	    // directly from IDLE/TURN.  This is required for fast decode devices,
	    // and I think the equation makes sense.
	    next_state = S_DATA;
	break;
    case B_BUSY:
	if ((!mFrame || !decode_done) && !hit)
	    // Still decoding the address
	    next_state = B_BUSY;
	else if ((!mFrame || !mIrdy) && decode_done && hit && (!term || term && ready))
	    // Accept the transaction
	    next_state = S_DATA;
	else if ((!mFrame || !mIrdy) && decode_done && hit && (term && !ready))
	    // Retry or some other termination with no data transfer
	    next_state = BACKOFF;
	else if (mFrame && decode_done || mFrame && !decode_done & !mDevsel)
	    // This branch is listed as the second 'if' test in the spec, but that
	    // doesn't appear to catch the case of decoding a request with a single
	    // data phase, where frame is asserted only during the address phase.
	    // By moving it to the end, I think the path to S_DATA will catch it properly.
	    next_state = IDLE;
	break;
    case S_DATA:
	if ((!mFrame && !mStop && !mTrdy && mIrdy) ||   // disconnect, with master stall
	    (!mFrame && mStop) ||                       // not terminating, not last data phase
	    (mFrame && mTrdy && mStop))                // last data phase, target stall
	    next_state = S_DATA;
	else if (!mFrame && !mStop && (mTrdy || !mIrdy))
	    // target termination
	    next_state = BACKOFF;
	else if (mFrame && (!mTrdy | !mStop))
	    // last data phase
	    next_state = TURN;
	break;
    case BACKOFF:
	if (!mFrame)
	    next_state = BACKOFF;
	else
	    next_state = TURN;     
	break;
    default:
	next_state = BAD_STATE;
    }

    if (!mRst)
	mState = mState_d1 = mState_d2 = IDLE;
    else {
	if (scDebug && (next_state != mState))
	    std::cout << "PCITargetSM @" << std::dec << tick << " : " << mState << " -> " << next_state << std::endl;
	mState_d2 = mState_d1;
	mState_d1 = mState;
	mState = next_state;
    }
	
}

