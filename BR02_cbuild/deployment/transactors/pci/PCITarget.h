// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __PCITARGET_H__
#define __PCITARGET_H__

namespace CarbonDeployment {
class PCITargetSM;
class PinWrapper;
class PCIConfig;
}

#include "CarbonPCITransactor.h"  // for some enums
#include "CarbonPCITransaction.h"  // for some enums
#include "carbon/carbon_shelltypes.h"

namespace CarbonDeployment {

class PCITarget
{
public:
    PCITarget(CarbonDeployment::PCIConfig * const config,
	      const CarbonDeployment::PinWrapper &rst,
	      const CarbonDeployment::PinWrapper &frame,
	      CarbonDeployment::PinWrapper &devsel,
	      const CarbonDeployment::PinWrapper &irdy,
	      CarbonDeployment::PinWrapper &trdy,
	      CarbonDeployment::PinWrapper &stop,
	      CarbonDeployment::PinWrapper &ad,
	      const CarbonDeployment::PinWrapper &cbe,
	      CarbonDeployment::PinWrapper &par,
	      const CarbonDeployment::PinWrapper &idsel,
	      CarbonDeployment::PinWrapper &perr,
	      const CarbonDeployment::PinWrapper &req64,
	      CarbonDeployment::PinWrapper &ack64,
	      CarbonDeployment::PinWrapper &par64);
    ~PCITarget();

    void step(CarbonTime tick);

    // Configuration functions
    void setDecodeSpeed(CarbonPCITransactor::DecodeSpeed ds);
    void set64BitMode(bool en);

    // Special behavior
    void setSubsequentTRDYDelay(UInt32 value);
    void setInitialTRDYDelay(UInt32 value);
    void setResponse(CarbonPCITransactor::TargetResponse type, UInt32 value);
    void setRepeatResponses(UInt32 value);
    void setBurstLength(UInt32 value);

    // Registering read/write callbacks
    void setReadHandler(UInt32 (*cb_func)(UInt32, UInt32, UInt32, void *), void *cb_data);
    void setWriteHandler(void (*cb_func)(UInt32, UInt32, UInt32, UInt32, UInt32, void *), void *cb_data);

    // Registers a callback function to be run when a transaction begins
    void onTransactionStart(void (*cb_func)(UInt64, UInt8, void*), void *cb_data);

protected:
    CarbonDeployment::PCITargetSM *mSM;
    CarbonDeployment::PCIConfig * const mConfig;

    // Pins
    const CarbonDeployment::PinWrapper &mRst;
    const CarbonDeployment::PinWrapper &mFrame;
    CarbonDeployment::PinWrapper &mDevsel;
    const CarbonDeployment::PinWrapper &mIrdy;
    CarbonDeployment::PinWrapper &mTrdy;
    CarbonDeployment::PinWrapper &mStop;
    CarbonDeployment::PinWrapper &mAd;
    const CarbonDeployment::PinWrapper &mCbe;
    CarbonDeployment::PinWrapper &mPar;
    const CarbonDeployment::PinWrapper &mIdsel;
    CarbonDeployment::PinWrapper &mPerr;
    const CarbonDeployment::PinWrapper &mReq64;
    CarbonDeployment::PinWrapper &mAck64;
    CarbonDeployment::PinWrapper &mPar64;

    // Delayed versions of signals
    bool mFrame_d1;
    UInt32 mAd_d1, mAd_d2;
    UInt32 mCbe_d1, mCbe_d2;
    UInt32 mAd64_d1;			// For 64-bit
    bool mPar_d1;

    // Configuration options
    CarbonPCITransactor::DecodeSpeed mDecodeSpeed;
    bool m64BitEn;		// Accept 64-bit transactions?

    // Other internal state
    UInt64 mCurrAddr;
    bool mWrite, mWrite_d1, mWrite_d2;		// last/current transaction is a write
    bool mDrivingAD, mDrivingAD_d1;
    bool mBadParity, mBadParity_d1;
    CarbonPCITransaction::Cmd mCmd;
    bool mHit;
    UInt32 mDecodeCount;
    UInt32 mBar, mFunc;				// keep track of the function and BAR when a decode hit occurs
    bool m64BitTrans;				// is this transaction 64-bit?
    bool mWaitDAC;				// are we waiting for the upper address in a DAC?

    // Not truly state, but needed in various places
    bool mDataXfer;
    bool mReady;
    bool mTerm;

    // Counters and settings for special behavior
    UInt32 mInitTrdyDelay;
    UInt32 mSubTrdyDelay;
    CarbonPCITransactor::TargetResponse mRespType;
    UInt32 mRespDataPhase;
    UInt32 mRespRepeat;
    UInt32 mTrdyDelayCount;
    UInt32 mRespDataPhaseCount;
    UInt32 mRespRepeatCount;

    // Utility functions
    void updateInternals();			// update misc internal state
    void updateOutputs();			// update output pins
    void processWriteData();
    void processWriteData64();
    UInt32 getReadData();
    UInt32 getReadData64();
    bool checkForHit();

    // Constants
    static const bool scTargetAbort = false;	// No target abort

    // We can't respond to transactions from the master side of our xtor,
    // but for now I'm gonna rely on the address windows being set up correctly
    static const bool scMasterActive = false;


    // Callback function and data pointers, to be called to read/write target memory
    UInt32 (*mReadCBFunc)(UInt32 /* addr */, UInt32 /* func */, UInt32 /* bar */, void * /* user data */);
    void *mReadCBData;
    void (*mWriteCBFunc)(UInt32 /* addr */, UInt32 /* write data */, UInt32 /* write mask */, UInt32 /* func */, UInt32 /* bar */, void * /* user data */);
    void *mWriteCBData;

    // Callback function and data to be run when a transaction starts (i.e. FRAME# asserts)
    void (*mStartCBFunc)(UInt64 /* AD bus */, UInt8 /* C/BE# bus */, void* /* user data */);
    void *mStartCBData;


    UInt32 mBurstLenBeforeDelay;
    UInt32 mXferCount;
};

}

#endif
