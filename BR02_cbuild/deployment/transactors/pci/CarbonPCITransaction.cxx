// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CarbonPCITransaction.h"
#include "carbonsim/CarbonSimTransactor.h"
#include <cstring>
#include <iostream>

CarbonPCITransaction::CarbonPCITransaction(Cmd cmd, UInt64 addr, UInt32 length)
    : mCmd(cmd),
      mAddr(addr),
      mLength(length),
      mCurrIndex(0),
      mCBFunc(0)
{
    mData = new UInt32[mLength];
    memset(mData, 0, mLength * sizeof(UInt32));

    mBE = new UInt8[mLength];
    memset(mBE, 0, mLength * sizeof(UInt8));
}

CarbonPCITransaction::~CarbonPCITransaction()
{
    delete [] mData;
    delete [] mBE;
}

void CarbonPCITransaction::step(CarbonSimTransactor * transactor, CarbonTime tick)
{
    // What on earth is this supposed to do?  I'll just pass the call onto the transactor,
    // in case that's what the end user expected to happen
    // MY transactor will never call this function, so there's no problem with an
    // infinite loop.

    transactor->step(tick);
}

bool CarbonPCITransaction::isDone(void)
{
    return (mCurrIndex == mLength);
}

UInt32 CarbonPCITransaction::getData(UInt32 index) const
{
    if (index >= mLength)
	return 0;

    return mData[index];
}

void CarbonPCITransaction::setData(UInt32 index, UInt32 data)
{
    if (index < mLength)
	mData[index] = data;
}

void CarbonPCITransaction::setBE(UInt32 index, UInt8 be)
{
    if (index < mLength)
	mBE[index] = be;
}

void CarbonPCITransaction::onCompletion(void (*cb_func)(const CarbonPCITransaction *, void *), void *cb_data)
{
    mCBFunc = cb_func;
    mCBData = cb_data;
}

bool CarbonPCITransaction::isRead() const
{
    return ((mCmd & 0x1) == 0);
}

bool CarbonPCITransaction::isWrite() const
{
    return ((mCmd & 0x1) && (mCmd != DAC));
}

UInt32 CarbonPCITransaction::getData()
{
    if (isDone())
	return 0;
    else
	return mData[mCurrIndex];
}

UInt8 CarbonPCITransaction::getBE()
{
    if (isDone() || !mBE)
	return 0;
    else
	return mBE[mCurrIndex];
}

void CarbonPCITransaction::writeData(UInt32 data)
{
    if (!isDone())
	mData[mCurrIndex] = data;
}

void CarbonPCITransaction::nextData()
{
    if (!isDone()) {
	++mCurrIndex;

	// If this completes the transaction, run callback function
	if (isDone() && mCBFunc)
	    (*mCBFunc)(this, mCBData);
    }
}

