// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "PCIConfig.h"
#include "PCIConfigRegs.h"
#include <string>
#include <iostream>

using namespace CarbonDeployment;

PCIConfig::PCIConfig()
{
    mRegs = new PCIConfigRegs*[scNumFuncs];
    memset(mRegs, 0, scNumFuncs * sizeof(PCIConfigRegs*));
}

PCIConfig::~PCIConfig()
{
    for (UInt32 i = 0; i < scNumFuncs; ++i)
	if (mRegs[i])
	    delete mRegs[i];

    delete [] mRegs;
}

void PCIConfig::write(UInt32 addr, UInt32 data, UInt32 mask, UInt32 func)
{
    if (func < scNumFuncs) {
	if (mRegs[func] == 0)	// need to create!
	    mRegs[func] = new PCIConfigRegs();
	mRegs[func]->write(addr, data, mask);
    }
}

UInt32 PCIConfig::read(UInt32 addr, UInt32 func) const
{
    if (func < scNumFuncs) {
	if (mRegs[func] == 0)	// need to create!
	    mRegs[func] = new PCIConfigRegs();
	return mRegs[func]->read(addr);
    }
    return 0;
}

void PCIConfig::setBarMask(UInt32 bar, UInt32 mask, bool mem, UInt32 func)
{
    if (func < scNumFuncs) {
	if (mRegs[func] == 0)	// need to create!
	    mRegs[func] = new PCIConfigRegs();
	return mRegs[func]->setBarMask(bar, mask, mem);
    }
}

void PCIConfig::setBarMask64(UInt32 bar, UInt64 mask, UInt32 func)
{
    if (func < scNumFuncs) {
	if (mRegs[func] == 0)	// need to create!
	    mRegs[func] = new PCIConfigRegs();
	return mRegs[func]->setBarMask64(bar, mask);
    }
}

bool PCIConfig::decode(UInt64 addr, bool mem, UInt32 &bar, UInt32 &func)
{
    // Try to decode 
    for (UInt32 i = 0; i < scNumFuncs; ++i)
	if (mRegs[i])
	    if (mRegs[i]->decode(addr, mem, bar)) {
		// hit!
		func = i;
		return true;
	    }

    return false;
}

