// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __PCICONFIG_H__
#define __PCICONFIG_H__

namespace CarbonDeployment {
class PCIConfigRegs;
}

#include "carbon/carbon_shelltypes.h"

namespace CarbonDeployment {

class PCIConfig
{
public:
    PCIConfig();
    ~PCIConfig();

    void write(UInt32 addr, UInt32 data, UInt32 mask, UInt32 func);
    UInt32 read(UInt32 addr, UInt32 func) const;
    void setBarMask(UInt32 bar, UInt32 mask, bool mem, UInt32 func);
    void setBarMask64(UInt32 bar, UInt64 mask, UInt32 func);
    bool decode(UInt64 addr, bool mem, UInt32 &bar, UInt32 &func);

protected:
    PCIConfigRegs **mRegs;	// one reg class per function

    static const UInt32 scNumFuncs = 8;	// from PCI spec - only 3 bits to specify

};

}

#endif
