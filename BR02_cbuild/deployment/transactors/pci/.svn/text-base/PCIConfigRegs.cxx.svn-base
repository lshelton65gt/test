// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "PCIConfigRegs.h"
#include <string>
#include <iostream>

using namespace CarbonDeployment;

PCIConfigRegs::PCIConfigRegs()
    : mCmdReg(0)
{
    memset(mBars, 0, scNumBars * sizeof(UInt32));
    memset(mBarAddrMask, 0, scNumBars * sizeof(UInt32));
    // Default to mem mode
    for (UInt32 i = 0; i < scNumBars; ++i)
	mBarROMask[i] = 0xf;
}

PCIConfigRegs::~PCIConfigRegs()
{
}

void PCIConfigRegs::write(UInt32 addr, UInt32 data, UInt32 mask)
{
    UInt32 bar = (addr >> 2) - 0x4;
    switch (addr) {
    case 0x04:	// Command/status
	// Update mask to reflect RO bits
	mask &= 0xffff;
	mCmdReg = (mCmdReg & ~mask) | (data & mask);
	break;
    case 0x10: // BAR 0
    case 0x14: // BAR 1
    case 0x18: // BAR 2
    case 0x1c: // BAR 3
    case 0x20: // BAR 4
    case 0x24: // BAR 5
	// Update mask to reflect RO bits
	mask &= (~mBarAddrMask[bar]);
	mask &= (~mBarROMask[bar]);
	mBars[bar] = (mBars[bar] & ~mask) | (data & mask);
	//	std::cout << "***MARK*** bar " << std::hex << bar << " has addr " << mBars[bar] << " and mask " << mBarAddrMask[bar] << std::endl;
	break;
    default:
	;
    }
}

UInt32 PCIConfigRegs::read(UInt32 addr) const
{
    UInt32 bar = (addr >> 2) - 0x4;
    switch (addr) {
    case 0x04:	// Command/status
	// Only do mem space & io space enable for now
	return mCmdReg;
    case 0x10: // BAR 0
    case 0x14: // BAR 1
    case 0x18: // BAR 2
    case 0x1c: // BAR 3
    case 0x20: // BAR 4
    case 0x24: // BAR 5
	return mBars[bar];
    default:
	return 0;
    }
}

void PCIConfigRegs::setBarMask(UInt32 bar, UInt32 mask, bool mem)
{
    if (bar < scNumBars) {
	mBarAddrMask[bar] = mask;
	// Set Mem/IO RO bits appropriately
	// Note that this completely wipes out whatever was in the BAR before
	if (mem) {
	    mBarROMask[bar] = 0xf;
	    mBars[bar] = 0x0;	// ignoring prefetch and other garbage for now
	} else {
	    mBarROMask[bar] = 0x3;
	    mBars[bar] = 0x1;
	}
    }
}

void PCIConfigRegs::setBarMask64(UInt32 bar, UInt64 mask)
{
    if (bar < (scNumBars - 1)) {	// Needs to be at least 1 less than the max bar, because it spans 2!
	mBarAddrMask[bar] = static_cast<UInt32>(mask & 0xffffffffULL);
	mBarAddrMask[bar + 1] = static_cast<UInt32>((mask >> 32) & 0xffffffffULL);
	// Set Mem/IO RO bits appropriately, and 64-bit field
	// Upper BAR becomes completely writable
	// Note that this completely wipes out whatever was in the BAR before
	mBarROMask[bar] = 0xf;
	mBars[bar] = 0x4;	// 64-bit BAR
	mBarROMask[bar + 1] = 0x0;
	mBars[bar + 1] = 0x0;
    }
}

// Attempt to decode the address.  If it hits one of the BARS,
// return true and set the bar by reference.  Otherwise, return
// false.
bool PCIConfigRegs::decode(UInt64 addr, bool mem, UInt32 &bar)
{
    // Make sure the spaces are enabled
    if ((mem && !(mCmdReg & 0x2)) || (!mem && !(mCmdReg & 0x1)))
	return false;

    for (UInt32 i = 0; i < scNumBars; ++i)
	if (mBarAddrMask[i]) {	// empty mask means this bar isn't enabled
	    bool baris64bit = ((mBars[i] & 0x7) == 0x4);
	    UInt64 base64 = static_cast<UInt64>(mBars[i]);
	    UInt64 mask64 = static_cast<UInt64>(mBarAddrMask[i]);
	    UInt64 romask64 = static_cast<UInt64>(mBarROMask[i]);
	    // If this is a 64-bit bar, add the upper 32 bits of these values
	    if (baris64bit) {
		base64 |= (static_cast<UInt64>(mBars[i + 1]) << 32);
		mask64 |= (static_cast<UInt64>(mBarAddrMask[i + 1]) << 32);
	    }
	    if ((mem && (mBarROMask[i] == 0xf)) || (!mem && (mBarROMask[i] == 0x3)))	// The proper kind of BAR (mem/io)
		if ((addr & ~mask64) == (base64 & ~romask64)) {	// hit!
		    bar = i;
		    //		    std::cout << "***MARK*** hit on bar " << std::hex << bar << " for addr " << addr << " with base " << base64 << " and mask " << mask64 << std::endl;
		    return true;
		}
	    if (baris64bit)
		// bar i + 1 is the upper 32 bits of bar i, so skip it
		++i;
	}

    return false;
}
