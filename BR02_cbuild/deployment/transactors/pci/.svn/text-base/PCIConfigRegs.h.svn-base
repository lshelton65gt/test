// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __PCICONFIGREGS_H__
#define __PCICONFIGREGS_H__

#include "carbon/carbon_shelltypes.h"

namespace CarbonDeployment {

class PCIConfigRegs
{
public:
    PCIConfigRegs();
    ~PCIConfigRegs();

    void write(UInt32 addr, UInt32 data, UInt32 mask);
    UInt32 read(UInt32 addr) const;
    void setBarMask(UInt32 bar, UInt32 mask, bool mem);
    void setBarMask64(UInt32 bar, UInt64 mask);
    bool decode(UInt64 addr, bool mem, UInt32 &bar);

protected:
    static const UInt32 scNumBars = 6;	// fixed by PCI spec

    UInt32 mCmdReg;
    UInt32 mBars[scNumBars];
    UInt32 mBarAddrMask[scNumBars];	// Mask for the number of decode address bits
    UInt32 mBarROMask[scNumBars];	// Mask for the RO bits at the low end of the register
};

}

#endif
