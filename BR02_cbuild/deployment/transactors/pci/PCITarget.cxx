// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "PCITarget.h"
#include "PCITargetSM.h"
#include "PinWrapper.h"
#include "PCIConfig.h"
#include "CarbonPCITransactorImpl.h"
#include <iostream>

using namespace CarbonDeployment;

PCITarget::PCITarget(CarbonDeployment::PCIConfig * const config,
		     const CarbonDeployment::PinWrapper &rst,
		     const CarbonDeployment::PinWrapper &frame,
		     CarbonDeployment::PinWrapper &devsel,
		     const CarbonDeployment::PinWrapper &irdy,
		     CarbonDeployment::PinWrapper &trdy,
		     CarbonDeployment::PinWrapper &stop,
		     CarbonDeployment::PinWrapper &ad,
		     const CarbonDeployment::PinWrapper &cbe,
		     CarbonDeployment::PinWrapper &par,
		     const CarbonDeployment::PinWrapper &idsel,
		     CarbonDeployment::PinWrapper &perr,
		     const CarbonDeployment::PinWrapper &req64,
		     CarbonDeployment::PinWrapper &ack64,
		     CarbonDeployment::PinWrapper &par64)
    : mConfig(config),
      mRst(rst),
      mFrame(frame),
      mDevsel(devsel),
      mIrdy(irdy),
      mTrdy(trdy),
      mStop(stop),
      mAd(ad),
      mCbe(cbe),
      mPar(par),
      mIdsel(idsel),
      mPerr(perr),
      mReq64(req64),
      mAck64(ack64),
      mPar64(par64),
      mFrame_d1(0),
      mAd_d1(0),
      mAd_d2(0),
      mCbe_d1(0),
      mCbe_d2(0),
      mAd64_d1(0),
      mPar_d1(0),
      mDecodeSpeed(CarbonPCITransactor::FastDecode),
      m64BitEn(true),
      mCurrAddr(0),
      mWrite(0),
      mWrite_d1(0),
      mWrite_d2(0),
      mDrivingAD(0),
      mDrivingAD_d1(0),
      mBadParity(0),
      mBadParity_d1(0),
      mHit(false),
      mDecodeCount(0),
      mInitTrdyDelay(0),
      mSubTrdyDelay(0),
      mRespType(CarbonPCITransactor::NormalResponse),
      mRespDataPhase(0),
      mRespRepeat(0),
      mTrdyDelayCount(0),
      mRespDataPhaseCount(0),
      mRespRepeatCount(0),
      mReadCBFunc(0),
      mReadCBData(0),
      mWriteCBFunc(0),
      mWriteCBData(0),
      mStartCBFunc(0),
      mStartCBData(0),
      mBurstLenBeforeDelay(0),
      mXferCount(0)
{
    mSM = new PCITargetSM(mRst,
			  mFrame,
			  mDevsel,
			  mIrdy,
			  mTrdy,
			  mStop);
}

PCITarget::~PCITarget()
{
}

void PCITarget::step(CarbonTime tick)
{
    /////////////////////////////////////////////
    // update SM and other inputs (combinational)
    /////////////////////////////////////////////

    // Do some things when a transaction is starting
    if (mFrame_d1 && !mFrame) {
	// call onStart callback if specified
	if (mStartCBFunc) {
	    UInt64 ad = (static_cast<UInt64>(mAd[1]) << 32) | static_cast<UInt64>(mAd[0]);
	    UInt8 cbe = static_cast<UInt8>(mCbe);
	    (*mStartCBFunc)(ad, cbe, mStartCBData);
	}

	// Set counters for special behaviors
	//
	// Initial TRDY delay
	// We do this when FRAME asserts because the delay is measured from the start of the
	// transaction, not from the assertion of DEVSEL.
	mTrdyDelayCount = mInitTrdyDelay;

	// no data xfer has happened yet, clear the counter
	mXferCount = 0;

	// Response data phase - only set this if it's not normal
	if (mRespType != CarbonPCITransactor::NormalResponse) {
	    // see if we need to actually do this
	    if (mRespRepeatCount) {
		mRespDataPhaseCount = mRespDataPhase - 1;		// we'll do this when the counter hits 0, so subtract 1 to start
		--mRespRepeatCount;
	    } else {
		mRespType = CarbonPCITransactor::NormalResponse;	// repeat count expired, so proceed normally
		mRespRepeatCount = mRespRepeat;				// reset counter for next time
	    }
	}

	mCmd = static_cast<CarbonPCITransaction::Cmd>(mCbe.read() & 0xf);	// use only lower 4 bits!
	mWaitDAC = (mCmd == CarbonPCITransaction::DAC);
	m64BitTrans = m64BitEn && !mReq64;
    } else if (mWaitDAC)	// flag will be cleared later
	// Update real command on a DAC
	mCmd = static_cast<CarbonPCITransaction::Cmd>(mCbe.read() & 0xf);	// use only lower 4 bits!

    mWrite = mCmd & 0x1;

    mDataXfer = (mSM->currState() == PCITargetSM::S_DATA) && !mTrdy && !mIrdy;

    // Update behavior counters
    if (mDataXfer) {
	mXferCount++;
	if (mXferCount >= mBurstLenBeforeDelay) {
	    mTrdyDelayCount = mSubTrdyDelay;	// prepare for next data phase	
	    mXferCount = 0;
	}
	--mRespDataPhaseCount;
    } else if (mTrdyDelayCount)
	--mTrdyDelayCount;			// decremement
	
    // Stupid enums are signed!
    bool decode_done = (mDecodeCount >= static_cast<UInt32>(mDecodeSpeed));

    mReady = (mTrdyDelayCount == 0) && !(mRespType == CarbonPCITransactor::Retry);
    mTerm = (mTrdyDelayCount == 0) && (mRespDataPhaseCount == 0) && ((mRespType == CarbonPCITransactor::Retry) || (mRespType == CarbonPCITransactor::DisconnectWithData));

    /////////////////////////////////////////////
    // update other internal state (sequential)
    /////////////////////////////////////////////
    updateInternals();

    /////////////////////////////////////////////
    // transition SM (sequential)
    /////////////////////////////////////////////
    mSM->transition(mHit,
		    decode_done,
		    mTerm,
		    mReady,
		    tick);


    /////////////////////////////////////////////
    // update outputs (combinational)
    /////////////////////////////////////////////
    updateOutputs();
}


void PCITarget::setDecodeSpeed(CarbonPCITransactor::DecodeSpeed ds)
{
    // TODO - sanity check
    mDecodeSpeed = ds;
}

void PCITarget::setBurstLength(UInt32 bl) 
{
    mBurstLenBeforeDelay = bl;
}

void PCITarget::set64BitMode(bool en)
{
    m64BitEn = en;
}

void PCITarget::setSubsequentTRDYDelay(UInt32 value)
{
    mSubTrdyDelay = value;
}

void PCITarget::setInitialTRDYDelay(UInt32 value)
{
    mInitTrdyDelay = value;
}

void PCITarget::setResponse(CarbonPCITransactor::TargetResponse type, UInt32 value)
{
    mRespType = type;
    // for testing only!
    //    mRespDataPhase = 1;
    mRespDataPhase = value;
}

void PCITarget::setRepeatResponses(UInt32 value)
{
    // Since this persist across transactions, only set if the counter is 0
    if (mRespRepeatCount == 0)
	mRespRepeat = value;
}

void PCITarget::setReadHandler(UInt32 (*cb_func)(UInt32, UInt32, UInt32, void *), void *cb_data)
{
    if (cb_func) {
	mReadCBFunc = cb_func;
	mReadCBData = cb_data;
    }
}

void PCITarget::setWriteHandler(void (*cb_func)(UInt32, UInt32, UInt32, UInt32, UInt32, void *), void *cb_data)
{
    if (cb_func) {
	mWriteCBFunc = cb_func;
	mWriteCBData = cb_data;
    }
}

void PCITarget::onTransactionStart(void (*cb_func)(UInt64, UInt8, void*), void *cb_data)
{
    if (cb_func) {
	mStartCBFunc = cb_func;
	mStartCBData = cb_data;
    }
}

void PCITarget::updateInternals()
{
    // Calculate bad parity
    mBadParity = ((CarbonPCITransactorImpl::carbon_redxor(mAd_d2) ^
		   CarbonPCITransactorImpl::carbon_redxor(mCbe_d2) ^
		   mPar_d1) &&						// bad parity
		  (mSM->stateD2() == PCITargetSM::S_DATA) &&            // in a data state
		  (mWrite_d2));                                   	// a write, so we need to check

    // Update current address
    if (mFrame_d1 && !mFrame) {
	if (mCmd == CarbonPCITransaction::DAC) {
//    mCurrAddr = static_cast<UInt64>(mAd[0]) << 32;
	    mCurrAddr = mAd[0];
	    mHit = false;
	} else {
	    mCurrAddr = mAd[0];
	    mHit = checkForHit();
	}
    } else if (mWaitDAC && (mCmd != CarbonPCITransaction::DAC)) {
	// waiting for the real command after the DAC, but we've seen it now
	// save the upper address - we'll clear the flag later
//mCurrAddr |= mAd[0];
  UInt64 hiAddr = static_cast<UInt64>(mAd[0]) << 32;
	mCurrAddr |= hiAddr;
	mHit = checkForHit();
    } else if (mDataXfer) {
	if (mWrite) {
	    processWriteData();
	    if (m64BitTrans)
		processWriteData64();
	}
	if (m64BitTrans) {
	    if (mCurrAddr & 0x4)
		mCurrAddr += 4;		// started on an odd dword, move to even one
	    else
		mCurrAddr += 8;		// move ahead 2 dwords
	} else
	    mCurrAddr += 4;		// move ahead 1 dword
    }

    // Track decode count
    if (!mDevsel)
	mDecodeCount = 0;
    else if ((mFrame_d1 && !mFrame && !mWaitDAC) |
	     (!mFrame_d1 && mWaitDAC))	 	// decode doesn't start until a DAC finishes
	mDecodeCount = 1;
    else if ((mDecodeCount != 0) && (mDecodeCount != 3))
	++mDecodeCount;
    else
	mDecodeCount = 0;

    if (mWaitDAC && (mCmd != CarbonPCITransaction::DAC))
	mWaitDAC = false;

    // Create some delayed version of signals
    mFrame_d1 = mFrame;
    mAd_d2 = mAd_d1;
    mAd_d1 = mAd;
    mCbe_d2 = mCbe_d1;
    mCbe_d1 = mCbe;
    mAd64_d1 = mAd[1];
    mPar_d1 = mPar;
    mDrivingAD_d1 = mDrivingAD;
    mBadParity_d1 = mBadParity;
    mWrite_d2 = mWrite_d1;
    mWrite_d1 = mWrite;
}

void PCITarget::updateOutputs()
{
    // first some combo signals that will be useful
    bool tar_dly = !((mDecodeSpeed == CarbonPCITransactor::FastDecode) && (mSM->currState() == PCITargetSM::S_DATA) &&
		     ((mSM->stateD1() == PCITargetSM::IDLE) || (mSM->stateD1() == PCITargetSM::TURN)));

    // Buffer for 64-bit data
    UInt32 data[2];


    // ad
    if (((mSM->currState() == PCITargetSM::S_DATA) || (mSM->currState() == PCITargetSM::BACKOFF)) && tar_dly && !mWrite) {
	mDrivingAD = true;
	data[0] = getReadData();
	if (m64BitTrans)
	    data[1] = getReadData64();
	else
	    data[1] = 0;	// no way to undrive only the upper bits!
	mAd.writeBig(data);
    } else {
	mDrivingAD = false;
	mAd.unDrive();
    }

    // trdy
    if ((mSM->currState() == PCITargetSM::BACKOFF) || (mSM->currState() == PCITargetSM::S_DATA) || (mSM->currState() == PCITargetSM::TURN))
	mTrdy = !(mReady && !scTargetAbort && (mSM->currState() == PCITargetSM::S_DATA) && (mWrite || !mWrite && tar_dly));
    else
	mTrdy.unDrive();

    // stop
    if ((mSM->currState() == PCITargetSM::BACKOFF) || (mSM->currState() == PCITargetSM::S_DATA) || (mSM->currState() == PCITargetSM::TURN))
	mStop = !((mSM->currState() == PCITargetSM::BACKOFF) || (mSM->currState() == PCITargetSM::S_DATA) && (scTargetAbort || mTerm) &&
		  (mWrite || !mWrite && tar_dly));
    else
	mStop.unDrive();

    // devsel
    if ((mSM->currState() == PCITargetSM::BACKOFF) || (mSM->currState() == PCITargetSM::S_DATA) || (mSM->currState() == PCITargetSM::TURN)) {
	mDevsel = !(((mSM->currState() == PCITargetSM::BACKOFF) || (mSM->currState() == PCITargetSM::S_DATA)) && !scTargetAbort);
	// ack64 mirrors devsel on 64-bit trans
	if (m64BitTrans)
	    mAck64 = !(((mSM->currState() == PCITargetSM::BACKOFF) || (mSM->currState() == PCITargetSM::S_DATA)) && !scTargetAbort);
	else
	    mAck64.unDrive();
    } else
	mDevsel.unDrive();

    // par
    if (mDrivingAD_d1) {
	mPar = CarbonPCITransactorImpl::carbon_redxor(mAd_d1) ^ CarbonPCITransactorImpl::carbon_redxor(mCbe_d1 & 0xf);
	// drive par64 on 64-bit trans
	if (m64BitTrans)
	    mPar64 = CarbonPCITransactorImpl::carbon_redxor(mAd64_d1) ^ CarbonPCITransactorImpl::carbon_redxor(mCbe_d1 & 0xf0);
	else
	    mPar64.unDrive();
    } else
	mPar.unDrive();

    // perr
    if (mBadParity || mBadParity_d1)
	mPerr = !mBadParity;
    else
	mPerr.unDrive();

}

void PCITarget::processWriteData()
{
    UInt32 config_addr = mCurrAddr & 0xff;
    UInt32 func = (mCurrAddr >> 8) & 0x7; 
    UInt32 mask = 0;
    for (UInt32 i = 0; i < 4; ++i)
	if (((mCbe >> i) & 0x1) == 0x0)
	    mask |= (0xff << (i * 8));
    if (mCmd == CarbonPCITransaction::CfgWrite)
	mConfig->write(config_addr, mAd, mask, func);
    else if (mWriteCBFunc)	// run callback function if it exists
	(*mWriteCBFunc)(mCurrAddr, mAd, mask, mFunc, mBar, mWriteCBData);
}

void PCITarget::processWriteData64()
{
    // Write data from upper 64 bits of bus
    if (mCurrAddr & 0x4)
	// We're on an odd dword, so we've already written this data from the lower half
	return;

    UInt32 mask = 0;
    for (UInt32 i = 4; i < 8; ++i)
	if (((mCbe >> i) & 0x1) == 0x0)
	    mask |= (0xff << (i * 8));

    // Need to write to the next dword
    if (mWriteCBFunc)
	(*mWriteCBFunc)(mCurrAddr + 4, mAd[1], mask, mFunc, mBar, mWriteCBData);
}

UInt32 PCITarget::getReadData()
{
    UInt32 config_addr = mCurrAddr & 0xff;
    UInt32 func = (mCurrAddr >> 8) & 0x7; 
    if (mCmd == CarbonPCITransaction::CfgRead)
	return mConfig->read(config_addr, func);

    if (mReadCBFunc)	// run callback function if it exists
	return (*mReadCBFunc)(mCurrAddr, mFunc, mBar, mReadCBData);

    return 0;
}

UInt32 PCITarget::getReadData64()
{
    // returns data for upper 32 bits of AD bus
    if (!mReadCBFunc)
	return 0;

    if (mCurrAddr & 0x4)
	// On on odd dword - Upper half of bus will be the same as the lower.
	// Just use this address
	return (*mReadCBFunc)(mCurrAddr, mFunc, mBar, mReadCBData);

    // Otherwise, we need to return data for the next dword address
    return (*mReadCBFunc)(mCurrAddr + 4, mFunc, mBar, mReadCBData);
}

bool PCITarget::checkForHit()
{
    bool bar_hit = false;
    if ((mCmd == CarbonPCITransaction::IORead) ||
	(mCmd == CarbonPCITransaction::IOWrite))
	bar_hit = mConfig->decode(mCurrAddr, false, mBar, mFunc);
    else if ((mCmd == CarbonPCITransaction::MemRead) ||
	     (mCmd == CarbonPCITransaction::MemWrite) ||
	     (mCmd == CarbonPCITransaction::MemReadMult) ||
	     (mCmd == CarbonPCITransaction::MemReadLine) ||
	     (mCmd == CarbonPCITransaction::MemWriteInval))
	bar_hit = mConfig->decode(mCurrAddr, true, mBar, mFunc);
 
    return (!scMasterActive &&
	    ((((mCmd == CarbonPCITransaction::CfgRead) ||
	       (mCmd == CarbonPCITransaction::CfgWrite)) && mIdsel) ||
	     bar_hit));
}
