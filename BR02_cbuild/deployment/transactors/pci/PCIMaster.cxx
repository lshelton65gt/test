// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "PCIMaster.h"
#include "PCIMasterSM.h"
#include "PinWrapper.h"
#include "PCIConfig.h"
#include "CarbonPCITransaction.h"
#include "CarbonPCITransactorImpl.h"
#include "carbonsim/CarbonSimTransaction.h"
#include <cassert>
#include <iostream>

using namespace CarbonDeployment;

PCIMaster::PCIMaster(CarbonDeployment::PCIConfig * const config,
		     const CarbonDeployment::PinWrapper &rst,
		     const CarbonDeployment::PinWrapper &gnt,
		     const CarbonDeployment::PinWrapper &trdy,
		     CarbonDeployment::PinWrapper &frame,
		     CarbonDeployment::PinWrapper &irdy,
		     const CarbonDeployment::PinWrapper &stop,
		     const CarbonDeployment::PinWrapper &devsel,
		     CarbonDeployment::PinWrapper &ad,
		     CarbonDeployment::PinWrapper &cbe,
		     CarbonDeployment::PinWrapper &par,
		     CarbonDeployment::PinWrapper &req,
		     CarbonDeployment::PinWrapper &perr)
    : mConfig(config),
      mRst(rst),
      mGnt(gnt),
      mTrdy(trdy),
      mFrame(frame),
      mIrdy(irdy),
      mStop(stop),
      mDevsel(devsel),
      mAd(ad),
      mCbe(cbe),
      mPar(par),
      mReq(req),
      mPerr(perr),
      mFrame_d1(1),
      mGnt_d1(1),
      mStop_d1(1),
      mAd_d1(0),
      mAd_d2(0),
      mCbe_d1(0),
      mCbe_d2(0),
      mPar_d1(0),
      mFb2bEn(false),
      mDataCount(0),
      mCurrAddr(0),
      mWrite(false),
      mWrite_d1(false),
      mWrite_d2(false),
      mDevselTOCount(0),
      mDrivingAD(0),
      mDrivingAD_d1(0),
      mBadParity(0),
      mBadParity_d1(0),
      mBackoffTimer(0),
      mDataXfer(false),
      mRequest(false),
      mReady(true),
      mReqDelay(0),
      mFrameDelay(0),
      mInitIrdyDelay(0),
      mSubIrdyDelay(0),
      mReqDelayCount(0),
      mFrameDelayCount(0),
      mIrdyDelayCount(0)
{
    mSM = new PCIMasterSM(mRst,
			  mGnt,
			  mTrdy,
			  mFrame,
			  mIrdy,
			  mStop);
}

PCIMaster::~PCIMaster()
{
    delete mSM;
}

void PCIMaster::submit(CarbonSimTransaction * transaction)
{
    // Make sure this is the right kind of transaction!
    CarbonPCITransaction *trans = dynamic_cast<CarbonPCITransaction *>(transaction);
    assert(trans);

    mTransList.push_back(trans);

    // Set delay counter
    // This one has to separate from the others, because it's possible
    // that a command is submitted while another one is in progress, and
    // we don't want to overwrite a counter that might be in use.
    mReqDelayCount = mReqDelay;
}

void PCIMaster::step(CarbonTime tick)
{
    /////////////////////////////////////////////
    // update SM and other inputs (combinational)
    /////////////////////////////////////////////

    // Calculate bad parity
    mBadParity = (CarbonPCITransactorImpl::carbon_redxor(mAd_d2) ^ CarbonPCITransactorImpl::carbon_redxor(mCbe_d2) ^ mPar_d1) &&	// bad parity
	(mSM->stateD2() == PCIMasterSM::M_DATA) &&				// in a data state
	!mWrite_d2 &&								// a read, so we need to check
	(mSM->stateD3() != PCIMasterSM::ADDR) &&				// but don't check during the AD turnaround cycle of the read
	(mSM->stateD3() != PCIMasterSM::DUAL_ADDR);

    if (mTransList.empty())
	mCurrTrans = 0;
    else
	mCurrTrans = mTransList.front();

    mDataXfer = (mSM->currState() == PCIMasterSM::M_DATA) && !mTrdy && !mIrdy;

    bool last_data = (mDataCount == 1);

    // We want to request a transaction if the queue isn't empty.
    // However, since the read pointer doesn't move until after the transaction
    // completes, if it's a write and we have fast b2b enabled, we'll try to
    // start a new transaction immediately following the last data phase
    // This is great if the next entry in the queue is valid, but we're finishing
    // the last command, we have to stop.
    if (!mTransList.empty() && !((mTransList.size() == 1) && last_data && mDataXfer)) {
	// See if we're delaying our request
	if (mReqDelayCount == 0)
	    mRequest = true;
	else {
	    mRequest = false;
	    --mReqDelayCount;
	}
    } else
	mRequest = false;

    // If we have a request and have received grant, determine whether we should
    // start the transaction.  Normally we would, but we have the option of
    // delaying the transaction for some number of cycles.
    // We can accomplish the delay by pretending we're doing address stepping.

    if (!mFrame)
	// reset frame delay counter whenever frame is asserted
	mFrameDelayCount = 0;
    else if (mRequest && (mFrameDelayCount == 0))
	// We have a request, but haven't set the delay count yet,
	// so do it.
	// Note: If the requested delay count is zero, we'll
	// do this regardless of whether we've done it already, but
	// it shouldn't matter
	mFrameDelayCount = mFrameDelay;

    // Only decrement the counter when grant is asserted
    if (mRequest && !mGnt && mFrame && mFrameDelayCount)
	--mFrameDelayCount;

    bool delay_frame = (mFrameDelayCount != 0);

//     if (mRequest)
// 	std::cout << "mRequest = " << mRequest << " mGnt = " << mGnt << " mFrame = " << mFrame << " mFrameDelayCount = " << mFrameDelayCount << std::endl;
    
//     if (mRequest)
// 	std::cout << "mIrdyDelayCount = " << mIrdyDelayCount << std::endl;

    mReady = (mIrdyDelayCount == 0);

    // Update IRDY delay counters
    if (mSM->currState() == PCIMasterSM::M_DATA) {
	if (mIrdyDelayCount)
	    --mIrdyDelayCount;
	else if (mDataXfer){
	    if (last_data)
		mIrdyDelayCount = mInitIrdyDelay;	// prepare for next transaction
	    else
		mIrdyDelayCount = mSubIrdyDelay;	// prepare for next data phase
	}
    }

    /////////////////////////////////////////////
    // update other internal state (sequential)
    /////////////////////////////////////////////
    updateInternals();

    /////////////////////////////////////////////
    // transition SM (sequential)
    /////////////////////////////////////////////
    mSM->transition(mWrite,
		    scSameTgt,
		    mRequest,
		    delay_frame,
		    devselTO(),
		    mFb2bEn,
		    needDAC(),
		    tick);


    /////////////////////////////////////////////
    // update outputs (combinational)
    /////////////////////////////////////////////
    updateOutputs();
}

void PCIMaster::setFb2bMode(bool en)
{
    mFb2bEn = en;
}

void PCIMaster::setSubsequentIRDYDelay(UInt32 value)
{
    if (value <= 8)	// max value according to PCI spec
	mSubIrdyDelay = value;
    else
	mSubIrdyDelay = 8;
}

void PCIMaster::setInitialIRDYDelay(UInt32 value)
{
    if (value <= 8)	// max value according to PCI spec
	mInitIrdyDelay = value;
    else
	mInitIrdyDelay = 8;
}

void PCIMaster::setFRAMEDelay(UInt32 value)
{
    mFrameDelay = value;
}

void PCIMaster::setReqDelay(UInt32 value)
{
    mReqDelay = value;
}

void PCIMaster::updateInternals()
{
   // DEVSEL# Timeouts (3.6.1)
   // A Target must assert devsel within 4 cycles,
   // if no one does, then we need to abort
    if (mFrame_d1 & !mFrame)
	mDevselTOCount = 1;
    else if ((!mFrame || !mIrdy) && mDevsel)
	++mDevselTOCount;

    // Set retry backoff timer
    if (!mRst)
	mBackoffTimer = 0;
    else if (mSM->currState() == PCIMasterSM::S_TAR)
	mBackoffTimer = scBackoffTimerVal;
    else if (mBackoffTimer != 0)
	--mBackoffTimer;

    // Keep track of whether the current (or just completed) transaction was
    // a write
    if ((mSM->currState() == PCIMasterSM::ADDR) || (mSM->currState() == PCIMasterSM::DUAL_ADDR)) {
	mWrite = mCurrTrans->isWrite();
	mDataCount = mCurrTrans->getLength();
	mCurrAddr = mCurrTrans->getAddr();
    } else if (mDataXfer) {
	// Update our state
	--mDataCount;
	mCurrAddr += 4;
	// ...as well as the transaction's state
	updateCurrTrans();
    }

    // Create some delayed version of signals
    mFrame_d1 = mFrame;
    mGnt_d1 = mGnt;
    mStop_d1 = mStop;
    mAd_d2 = mAd_d1;
    mAd_d1 = mAd;
    mCbe_d2 = mCbe_d1;
    mCbe_d1 = mCbe;
    mPar_d1 = mPar;
    mDrivingAD_d1 = mDrivingAD;
    mBadParity_d1 = mBadParity;
    mWrite_d2 = mWrite_d1;
    mWrite_d1 = mWrite;
}

void PCIMaster::updateOutputs()
{
    // frame
    if ((mSM->currState() == PCIMasterSM::ADDR) || (mSM->currState() == PCIMasterSM::DUAL_ADDR) || (mSM->currState() == PCIMasterSM::M_DATA))
	mFrame = !((mSM->currState() == PCIMasterSM::ADDR) || (mSM->currState() == PCIMasterSM::DUAL_ADDR) || (mSM->currState() == PCIMasterSM::M_DATA) && !devselTO() &&
		   (((mDataCount != 1) && (!scMasterTO || !mGnt_d1) && mStop_d1) || !mReady));
    else
	mFrame.unDrive();

    // cbe
    if ((mSM->currState() == PCIMasterSM::ADDR) || (mSM->currState() == PCIMasterSM::DUAL_ADDR) || (mSM->currState() == PCIMasterSM::M_DATA) ||
	(mSM->currState() == PCIMasterSM::DR_BUS)) {
	// Driving, but what value?
	UInt32 cbe_val = 0;
	switch (mSM->currState()) {
	case PCIMasterSM::ADDR:
	    cbe_val = needDAC() ? 0xD : mCurrTrans->getCmd();
	    break;
	case PCIMasterSM::DUAL_ADDR:
	    cbe_val = mCurrTrans->getCmd();
	    break;
	case PCIMasterSM::M_DATA:
	    cbe_val = mCurrTrans->getBE();
	    break;
	case PCIMasterSM::DR_BUS:
	    cbe_val = 0xf;
	    break;
	default:
	    ;
	}
	mCbe = cbe_val;
    } else
	mCbe.unDrive();

    // ad
    if ((mSM->currState() == PCIMasterSM::ADDR) || (mSM->currState() == PCIMasterSM::DUAL_ADDR) || ((mSM->currState() == PCIMasterSM::M_DATA) && mWrite) ||
	(mSM->currState() == PCIMasterSM::DR_BUS)) {
	// Driving, but what value?
	mDrivingAD = true;
	UInt32 ad_val = 0;
	switch (mSM->currState()) {
	case PCIMasterSM::ADDR:
	    ad_val = static_cast<UInt32>(mCurrTrans->getAddr() & 0xffffffff);
	    break;
	case PCIMasterSM::DUAL_ADDR:
	    ad_val = static_cast<UInt32>(mCurrTrans->getAddr() >> 32);
	    break;
	case PCIMasterSM::M_DATA:
	    ad_val = mCurrTrans->getData();
	    break;
	case PCIMasterSM::DR_BUS:
	    ad_val = 0xffffffff;
	    break;
	default:
	    ;
	}
	mAd = ad_val;
    } else {
	mDrivingAD = false;
	mAd.unDrive();
    }

    // irdy
    if ((mSM->stateD1() == PCIMasterSM::M_DATA) || (mSM->stateD1() == PCIMasterSM::ADDR) || (mSM->stateD1() == PCIMasterSM::DUAL_ADDR))
	mIrdy = !((mSM->currState() ==  PCIMasterSM::M_DATA) && (mReady || devselTO()));
    else
	mIrdy.unDrive();

    // par
    if (mDrivingAD_d1)
	mPar = CarbonPCITransactorImpl::carbon_redxor(mAd_d1) ^ CarbonPCITransactorImpl::carbon_redxor(mCbe_d1);
    else
	mPar.unDrive();

    // perr
    if (mBadParity || mBadParity_d1)
	mPerr = !mBadParity;
    else
	mPerr.unDrive();

    // req
    mReq = !(mRequest && !((mSM->currState() == PCIMasterSM::S_TAR) || (mSM->stateD1() == PCIMasterSM::S_TAR)) && (mBackoffTimer == 0));
}

bool PCIMaster::devselTO()
{
    return mDevselTOCount > 4;
}

bool PCIMaster::needDAC()
{
    return (mCurrTrans && (static_cast<UInt32>(mCurrTrans->getAddr() >> 32) != 0));
}

void PCIMaster::updateCurrTrans()
{
    // If the transaction is a read, write the data back into the transaction
    if (mCurrTrans->isRead())
	mCurrTrans->writeData(mAd);

    // Update the transaction, and remove it if it's done
    mCurrTrans->nextData();
    if (mCurrTrans->isDone()) {
	mTransList.pop_front();
	// Hmm... a lot of other places use mCurrTrans, but only in situations where it
	// should be valid.  Oh, well, I'll null it out anyway.  I'd actually rather
	// the seg fault that random incorrect behavior if the protocol goes awry.
	//
	// Oh, and where does the transaction get cleaned up?  We didn't create the
	// transaction, some outside entity did.  But, I don't think it can be deleted
	// there, because the only sensible way it could do that would be as part of	
	// the callback function.  That wouldn't work, because the callback function
	// is called from the object, and what happens when you return to a member function
	// (and try to access member data) of an object you just deleted?  I don't want
	// to find out...
	//
	// So, I'll just delete it here.  Maybe this will change in the future, but for
	// now it works.
	delete mCurrTrans;
	
	// For FBTB, we need to keep this valid!
	if (mTransList.empty())
	    mCurrTrans = 0;
	else
	    mCurrTrans = mTransList.front();
    }
}
