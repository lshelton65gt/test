// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CarbonPCITransactorImpl.h"
#include "PinManager.h"
#include "PCIMaster.h"
#include "PCITarget.h"
#include "PCIConfig.h"
#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimClock.h"
#include <cassert>

using namespace CarbonDeployment;

CarbonPCITransactorImpl::CarbonPCITransactorImpl(CarbonSimMaster *sm,
						 bool pullups_en,
						 bool use_force,
						 bool multi_xtor_mode)
    : mSimMaster(sm),
      mObj(0),
      mClk(0),
      mPullupsEn(pullups_en),
      mUseForce(use_force),
      mMultiXtorMode(multi_xtor_mode)
{
    assert(mSimMaster);
    mPinMgr = new PinManager(mSimMaster);
    mConfig = new PCIConfig();

    mMaster = new PCIMaster(mConfig,
			    mRst,
			    mGnt,
			    mTrdy,
			    mFrame,
			    mIrdy,
			    mStop,
			    mDevsel,
			    mAD,
			    mCBE,
			    mPar,
			    mReq,
			    mPerr);

    mTarget = new PCITarget(mConfig,
			    mRst,
			    mFrame,
			    mDevsel,
			    mIrdy,
			    mTrdy,
			    mStop,
			    mAD,
			    mCBE,
			    mPar,
			    mIdsel,
			    mPerr,
			    mReq64,
			    mAck64,
			    mPar64);

}

CarbonPCITransactorImpl::CarbonPCITransactorImpl(CarbonObjectID *obj,
						 bool pullups_en,
						 bool use_force,
						 bool multi_xtor_mode)
    : mSimMaster(0),
      mObj(obj),
      mClk(0),
      mPullupsEn(pullups_en),
      mUseForce(use_force),
      mMultiXtorMode(multi_xtor_mode)
{
    assert(mObj);
    mPinMgr = new PinManager(mObj);
    mConfig = new PCIConfig();

    mMaster = new PCIMaster(mConfig,
			    mRst,
			    mGnt,
			    mTrdy,
			    mFrame,
			    mIrdy,
			    mStop,
			    mDevsel,
			    mAD,
			    mCBE,
			    mPar,
			    mReq,
			    mPerr);
    mTarget = new PCITarget(mConfig,
			    mRst,
			    mFrame,
			    mDevsel,
			    mIrdy,
			    mTrdy,
			    mStop,
			    mAD,
			    mCBE,
			    mPar,
			    mIdsel,
			    mPerr,
			    mReq64,
			    mAck64,
			    mPar64);
}

CarbonPCITransactorImpl::~CarbonPCITransactorImpl()
{
    delete mPinMgr;
    if (mMaster)
	delete mMaster;
    if (mTarget)
	delete mTarget;
    delete mConfig;
}

void CarbonPCITransactorImpl::step(CarbonTime tick)
{
    // Only run on the posedge of the clock.
    // If the clock isn't set, assume we're always being called at the
    // appropriate time (e.g. we're in a SystemC environment instead of CarbonSim)
    if ((!mClk) || (mClk->getValue())) {

	// Transition state
	mMaster->step(tick);
	mTarget->step(tick);

	// Update outputs

	// Deposit outputs, and sample inputs for next tick
	mPinMgr->deposit();
	if (!mMultiXtorMode)
	    mPinMgr->examine();
    }
}

void CarbonPCITransactorImpl::idle(UInt64 cycles)
{
}

void CarbonPCITransactorImpl::submit(CarbonSimTransaction * transaction)
{
    mMaster->submit(transaction);
}

void CarbonPCITransactorImpl::sample()
{
    // If there are multiple transactors reading the same signals, the sample/output
    // calls for each have to be interleaved for all the values to propagate correctly.
    // If multi-xtor mode is enabled, this function will handle the sampling of the
    // current values.  In normal mode, this isn't necessary.
    //
    // This mode can't currently be used in a CarbonSim environment, because the
    // SimMaster isn't aware of it.  If you're manually calling step(), though,
    // you can use this mode by calling step() for all xtors, then calling sample()
    // for each.
    if (mMultiXtorMode)
	mPinMgr->examine();
}

void CarbonPCITransactorImpl::setClock(CarbonSimClock *clk)
{
    mClk = clk;
}

bool CarbonPCITransactorImpl::bindRstL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mRst);
    return valid;
}
bool CarbonPCITransactorImpl::bindAD(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mAD);
    if (mUseForce)
	mAD.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindCBE(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mCBE);
    if (mUseForce)
	mCBE.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindPar(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mPar);
    if (mUseForce)
	mPar.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindPar64(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mPar64);
    if (mUseForce)
	mPar64.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindFrameL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mFrame);
    if (mPullupsEn && valid)
	mFrame.pullUp();
    if (mUseForce)
	mFrame.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindIrdyL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mIrdy);
    if (mPullupsEn && valid)
	mIrdy.pullUp();
    if (mUseForce)
	mIrdy.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindTrdyL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mTrdy);
    if (mPullupsEn && valid)
	mTrdy.pullUp();
    if (mUseForce)
	mTrdy.useForce();
   return valid;
}
bool CarbonPCITransactorImpl::bindStopL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mStop);
    if (mPullupsEn && valid)
	mStop.pullUp();
    if (mUseForce)
	mStop.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindDevselL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mDevsel);
    if (mPullupsEn && valid)
	mDevsel.pullUp();
    if (mUseForce)
	mDevsel.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindIdsel(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mIdsel);
    return valid;
}
bool CarbonPCITransactorImpl::bindPerrL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mPerr);
    if (mPullupsEn && valid)
	mPerr.pullUp();
    if (mUseForce)
	mPerr.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindSerrL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mSerr);
    if (mPullupsEn && valid)
	mSerr.pullUp();
    if (mUseForce)
	mSerr.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindReqL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mReq);
    return valid;
}
bool CarbonPCITransactorImpl::bindGntL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mGnt);
    return valid;
}
bool CarbonPCITransactorImpl::bindReq64L(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mReq64);
    if (mPullupsEn && valid)
	mReq64.pullUp();
    if (mUseForce)
	mReq64.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindAck64L(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mAck64);
    if (mPullupsEn && valid)
	mAck64.pullUp();
    if (mUseForce)
	mAck64.useForce();
    return valid;
}
bool CarbonPCITransactorImpl::bindIntAL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mIntA);
    return valid;
}
bool CarbonPCITransactorImpl::bindIntBL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mIntB);
    return valid;
}
bool CarbonPCITransactorImpl::bindIntCL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mIntC);
    return valid;
}
bool CarbonPCITransactorImpl::bindIntDL(const char *signal_name)
{
    bool valid = mPinMgr->addPin(signal_name, mIntD);
    return valid;
}

void CarbonPCITransactorImpl::setDecodeSpeed(CarbonPCITransactor::DecodeSpeed ds)
{
    mTarget->setDecodeSpeed(ds);
}

void CarbonPCITransactorImpl::setTarget64Bit(bool en)
{
    mTarget->set64BitMode(en);
}

void CarbonPCITransactorImpl::setFb2bMode(bool en)
{
    mMaster->setFb2bMode(en);
}

void CarbonPCITransactorImpl::setBarMask(UInt32 bar, UInt32 mask, bool mem, UInt32 func)
{
    mConfig->setBarMask(bar, mask, mem, func);
}

void CarbonPCITransactorImpl::setBarMask64(UInt32 bar, UInt64 mask, UInt32 func)
{
    mConfig->setBarMask64(bar, mask, func);
}

void CarbonPCITransactorImpl::setReadHandler(UInt32 (*cb_func)(UInt32, UInt32, UInt32, void *), void *cb_data)
{
    mTarget->setReadHandler(cb_func, cb_data);
}

void CarbonPCITransactorImpl::setWriteHandler(void (*cb_func)(UInt32, UInt32, UInt32, UInt32, UInt32, void *), void *cb_data)
{
    mTarget->setWriteHandler(cb_func, cb_data);
}

void CarbonPCITransactorImpl::onTransactionStart(void (*cb_func)(UInt64, UInt8, void*), void *cb_data)
{
    mTarget->onTransactionStart(cb_func, cb_data);
}

void CarbonPCITransactorImpl::setSubsequentIRDYDelay(UInt32 value)
{
    mMaster->setSubsequentIRDYDelay(value);
}

void CarbonPCITransactorImpl::setInitialIRDYDelay(UInt32 value)
{
    mMaster->setInitialIRDYDelay(value);
}

void CarbonPCITransactorImpl::setFRAMEDelay(UInt32 value)
{
    mMaster->setFRAMEDelay(value);
}

void CarbonPCITransactorImpl::setReqDelay(UInt32 value)
{
    mMaster->setReqDelay(value);
}

void CarbonPCITransactorImpl::setSubsequentTRDYDelay(UInt32 value)
{
    mTarget->setSubsequentTRDYDelay(value);
}

void CarbonPCITransactorImpl::setInitialTRDYDelay(UInt32 value)
{
    mTarget->setInitialTRDYDelay(value);
}

void CarbonPCITransactorImpl::setTargetBurstLength(UInt32 bl)
{
    mTarget->setBurstLength(bl);
}

void CarbonPCITransactorImpl::setTargetResponse(CarbonPCITransactor::TargetResponse type, UInt32 value)
{
    mTarget->setResponse(type, value);
}

void CarbonPCITransactorImpl::setRepeatTargetResponses(UInt32 value)
{
    mTarget->setRepeatResponses(value);
}

void CarbonPCITransactorImpl::writeConfigSpace(UInt32 func, UInt32 offset, UInt32 data, UInt32 mask)
{
    mConfig->write(offset, data, mask, func);
}

UInt32 CarbonPCITransactorImpl::readConfigSpace(UInt32 func, UInt32 offset)
{
    return mConfig->read(offset, func);
}

// stolen from codegen/carbon_priv.h - I can't just extern it an resolve at link time because it's inlined
bool CarbonPCITransactorImpl::carbon_redxor(UInt32 x)
{
    x ^= (x >> 16);
    x ^= (x >> 8);
    x ^= (x >> 4);
    x &= 0xf;
    return ((0x6996 >> x) & 0x1);
}
