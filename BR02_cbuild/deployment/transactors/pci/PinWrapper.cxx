// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "PinWrapper.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbon/carbon_capi.h"
#include <cstring>
#include <iostream>

using namespace CarbonDeployment;

PinWrapper::PinWrapper(CarbonSimNet *net)
    : mSimNet(net),
      mObj(0),
      mNetID(0),
      mPull(NONE),
      mHasDriver(false),
      mPrevDriving(false),
      mUseForce(false)
{
    init();
}

PinWrapper::PinWrapper(CarbonNetID *net, CarbonObjectID *obj)
    : mSimNet(0),
      mObj(obj),
      mNetID(net),
      mPull(NONE),
      mHasDriver(false),
      mPrevDriving(false),
      mUseForce(false)
{
    init();
}

PinWrapper::PinWrapper()
    : mSimNet(0),
      mObj(0),
      mNetID(0),
      mPull(NONE),
      mHasDriver(false),
      mPrevDriving(false),
      mUseForce(false)
{
    // No net was given yet!

    mNumUInt = 0;
    mReadVal = 0;
    mWriteVal = 0;
    mOne = 0;
    mZero = 0;
    mTemp = 0;
    
    mReadDrive = false;
    mWriteDrive = false;
}

PinWrapper::~PinWrapper()
{
    delete [] mReadVal;
    delete [] mWriteVal;
    delete [] mOne;
    delete [] mZero;
}

UInt32 PinWrapper::read(UInt32 index) const
{
    if (mReadVal)
	return mReadVal[index];

    return 0;
}

void PinWrapper::write(UInt32 val)
{
    if (mWriteVal) {
	*mWriteVal = val;
	mWriteDrive = true;
	// so an unDrive() before the next deposit won't kill the drive
	mHasDriver = true;
    }
}

const UInt32 *PinWrapper::readBig() const
{
    return mReadVal;
}

void PinWrapper::writeBig(UInt32 *val)
{
    if (mWriteVal) {
	memcpy(mWriteVal, val, mNumUInt * sizeof(UInt32));
	mWriteDrive = true;
	// so an unDrive() before the next deposit won't kill the drive
	mHasDriver = true;
    }
}

bool PinWrapper::isDriven() const
{
    return mReadDrive;
}

void PinWrapper::unDrive()
{
    // only valid for tristates, or when forcing
    if (mIsTristate || mUseForce)
	// Since multiple objects may share a PinWrapper, we have to make sure
	// drive/undrive is resolved correctly.  If one object calls write and
	// the other calls unDrive(), the write value should take effect.
	// So, only clear the drive flag if no other object has asked to write
	// since the last deposit.
	if (!mHasDriver)
	    mWriteDrive = false;
}

void PinWrapper::pullUp()
{
    mPull = UP;
}

void PinWrapper::pullDown()
{
    mPull = DOWN;
}

void PinWrapper::unPull()
{
    mPull = NONE;
}

void PinWrapper::useForce()
{
    mUseForce = true;
}

PinWrapper::operator UInt32() const
{
    return read(0);
}

PinWrapper &PinWrapper::operator=(UInt32 val)
{
    write(val);
    return *this;
}

UInt32 PinWrapper::operator[](UInt32 index) const
{
    return read(index);
}

void PinWrapper::examine()
{
    if (mSimNet) {
	mSimNet->examine(mReadVal);
	mReadDrive = mSimNet->isDriving();
    } else if (mNetID) {
	carbonExamine(mObj, mNetID, mReadVal, mTemp);
	// Ugh... not quite correct...
	mReadDrive = (mTemp[0] == 0);
    }
}

void PinWrapper::depositorforce()
{
    // can't deposit to a xtor input
    if (isValid() && !mIsInput)
	if (mWriteDrive)
	    depositorforce(mWriteVal, true);
	else if (mIsTristate || mUseForce) {
	    if ((mPull == UP) && !mReadDrive) {
		examine();
		if (!mReadDrive)
		    depositorforce(mOne, true);
	    } else if ((mPull == DOWN) && !mReadDrive) {
		examine();
		if (!mReadDrive)
		    depositorforce(mZero, true);
	    } else	// mPull == NONE
		depositorforce(mZero, false);
	}

    // Keep track of whether we drove this or not
    mPrevDriving = mWriteDrive;

    // Reset flag to track drive requests
    mHasDriver = false;
}

void PinWrapper::depositorforce(const UInt32 *val, bool drive)
{
    if (mUseForce) {
	if (drive)
	    force(val);
	else
	    release();
    } else
	deposit(val, drive);
}

void PinWrapper::deposit(const UInt32 *val, bool drive)
{
    // Only do a release if we were previously driving this net
    // There may be other PinWrappers controlling this net, and
    // we don't want to cancel them out if they're trying to
    // drive it
    if (!mPrevDriving && !drive)
	return;

    if (mSimNet)
	mSimNet->deposit(val, drive ? mZero : mOne);
    else
	carbonDeposit(mObj, mNetID, val, drive ? mZero : mOne);
}

void PinWrapper::force(const UInt32 *val)
{
    if (mSimNet)
	mSimNet->force(val);
    else
	carbonForce(mObj, mNetID, val);
}

void PinWrapper::release()
{
    // Only do a release if we were previously driving this net
    // There may be other PinWrappers controlling this net, and
    // we don't want to cancel them out if they're trying to
    // drive it

    if (mPrevDriving) {
	if (mSimNet)
	    mSimNet->release();
	else
	    carbonRelease(mObj, mNetID);
    }
}

void PinWrapper::init()
{
    // some common init stuff
    if (mSimNet)
	mNumUInt = mSimNet->getNumUInt32s();
    else
	mNumUInt = carbonGetNumUInt32s(mNetID);

    mReadVal = new UInt32[mNumUInt];
    mWriteVal = new UInt32[mNumUInt];
    mOne = new UInt32[mNumUInt];
    mZero = new UInt32[mNumUInt];
    mTemp = new UInt32[mNumUInt];

    memset(mReadVal, 0, mNumUInt * sizeof(UInt32));
    memset(mWriteVal, 0, mNumUInt * sizeof(UInt32));
    memset(mOne, 0xff, mNumUInt * sizeof(UInt32));
    memset(mZero, 0, mNumUInt * sizeof(UInt32));

    // Output from the Carbon object is an input to
    // the xtor and cannot be driven
    if (mSimNet) {
	mIsInput = mSimNet->isOutput();
	mIsTristate = mSimNet->isTristate();
    } else {
	mIsInput = carbonIsOutput(mObj, mNetID);
	mIsTristate = carbonIsTristate(mNetID);
    }

//     char name[100];
//     carbonGetNetName(mObj, mNetID, name, 100);
//     std::cout << "***MARK*** net " << name << " - input: " << mIsInput << ", tristate: " << mIsTristate << std::endl;
}

bool PinWrapper::isValid() const
{
    return ((mSimNet != 0) || (mNetID != 0));
}

bool PinWrapper::setNet(CarbonSimNet *net)
{
    if (mSimNet || mNetID)
	return false;

    mSimNet = net;
    init();
    return true;
}

bool PinWrapper::setNet(CarbonNetID *net, CarbonObjectID *obj)
{
    if (mSimNet || mNetID)
	return false;

    mNetID = net;
    mObj = obj;
    init();
//     std::cout << "setNet: mUseForce is " << mUseForce << std::endl;
    return true;
}

