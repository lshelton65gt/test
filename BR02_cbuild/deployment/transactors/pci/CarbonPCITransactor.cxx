// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CarbonPCITransactor.h"
#include "CarbonPCITransactorImpl.h"

CarbonPCITransactor::CarbonPCITransactor(CarbonSimMaster *sm,
					 bool pullups_en,
					 bool use_force,
					 bool multi_xtor_mode)
{
    pImpl = new CarbonPCITransactorImpl(sm, pullups_en, use_force, multi_xtor_mode);
}

CarbonPCITransactor::CarbonPCITransactor(CarbonObjectID *obj,
					 bool pullups_en,
					 bool use_force,
					 bool multi_xtor_mode)
{
    pImpl = new CarbonPCITransactorImpl(obj, pullups_en, use_force, multi_xtor_mode);
}

CarbonPCITransactor::~CarbonPCITransactor()
{
    delete pImpl;
}

void CarbonPCITransactor::step(CarbonTime tick)
{
    pImpl->step(tick);
}

void CarbonPCITransactor::idle(UInt64 cycles)
{
    pImpl->idle(cycles);
}

void CarbonPCITransactor::submit(CarbonSimTransaction * transaction)
{
    pImpl->submit(transaction);
}

void CarbonPCITransactor::sample()
{
    pImpl->sample();
}

void CarbonPCITransactor::setClock(CarbonSimClock *clk)
{
    pImpl->setClock(clk);
}

bool CarbonPCITransactor::bindRstL(const char *signal_name)
{
    return pImpl->bindRstL(signal_name);
}

bool CarbonPCITransactor::bindAD(const char *signal_name)
{
    return pImpl->bindAD(signal_name);
}

bool CarbonPCITransactor::bindCBE(const char *signal_name)
{
    return pImpl->bindCBE(signal_name);
}

bool CarbonPCITransactor::bindPar(const char *signal_name)
{
    return pImpl->bindPar(signal_name);
}

bool CarbonPCITransactor::bindPar64(const char *signal_name)
{
    return pImpl->bindPar64(signal_name);
}

bool CarbonPCITransactor::bindFrameL(const char *signal_name)
{
    return pImpl->bindFrameL(signal_name);
}

bool CarbonPCITransactor::bindIrdyL(const char *signal_name)
{
    return pImpl->bindIrdyL(signal_name);
}

bool CarbonPCITransactor::bindTrdyL(const char *signal_name)
{
    return pImpl->bindTrdyL(signal_name);
}

bool CarbonPCITransactor::bindStopL(const char *signal_name)
{
    return pImpl->bindStopL(signal_name);
}

bool CarbonPCITransactor::bindDevselL(const char *signal_name)
{
    return pImpl->bindDevselL(signal_name);
}

bool CarbonPCITransactor::bindIdsel(const char *signal_name)
{
    return pImpl->bindIdsel(signal_name);
}

bool CarbonPCITransactor::bindPerrL(const char *signal_name)
{
    return pImpl->bindPerrL(signal_name);
}

bool CarbonPCITransactor::bindSerrL(const char *signal_name)
{
    return pImpl->bindSerrL(signal_name);
}

bool CarbonPCITransactor::bindReqL(const char *signal_name)
{
    return pImpl->bindReqL(signal_name);
}

bool CarbonPCITransactor::bindGntL(const char *signal_name)
{
    return pImpl->bindGntL(signal_name);
}

bool CarbonPCITransactor::bindReq64L(const char *signal_name)
{
    return pImpl->bindReq64L(signal_name);
}

bool CarbonPCITransactor::bindAck64L(const char *signal_name)
{
    return pImpl->bindAck64L(signal_name);
}

bool CarbonPCITransactor::bindIntAL(const char *signal_name)
{
    return pImpl->bindIntAL(signal_name);
}

bool CarbonPCITransactor::bindIntBL(const char *signal_name)
{
    return pImpl->bindIntBL(signal_name);
}

bool CarbonPCITransactor::bindIntCL(const char *signal_name)
{
    return pImpl->bindIntCL(signal_name);
}

bool CarbonPCITransactor::bindIntDL(const char *signal_name)
{
    return pImpl->bindIntDL(signal_name);
}

void CarbonPCITransactor::setDecodeSpeed(DecodeSpeed ds)
{
    pImpl->setDecodeSpeed(ds);
}

void CarbonPCITransactor::setTarget64Bit(bool en)
{
    pImpl->setTarget64Bit(en);
}

void CarbonPCITransactor::setFb2bMode(bool en)
{
    pImpl->setFb2bMode(en);
}

void CarbonPCITransactor::setBarMask(UInt32 bar, UInt32 mask, bool mem, UInt32 func)
{
    pImpl->setBarMask(bar, mask, mem, func);
}

void CarbonPCITransactor::setBarMask64(UInt32 bar, UInt64 mask, UInt32 func)
{
    pImpl->setBarMask64(bar, mask, func);
}

void CarbonPCITransactor::writeConfigSpace(UInt32 func, UInt32 offset, UInt32 data, UInt32 mask)
{
    pImpl->writeConfigSpace(func, offset, data, mask);
}

UInt32 CarbonPCITransactor::readConfigSpace(UInt32 func, UInt32 offset)
{
    return pImpl->readConfigSpace(func, offset);
}

void CarbonPCITransactor::setReadHandler(UInt32 (*cb_func)(UInt32, UInt32, UInt32, void *), void *cb_data)
{
    pImpl->setReadHandler(cb_func, cb_data);
}

void CarbonPCITransactor::setWriteHandler(void (*cb_func)(UInt32, UInt32, UInt32, UInt32, UInt32, void *), void *cb_data)
{
    pImpl->setWriteHandler(cb_func, cb_data);
}

void CarbonPCITransactor::onTransactionStart(void (*cb_func)(UInt64 /* AD bus */, UInt8 /* C/BE# bus */, void* /* user data */), void *cb_data)
{
    pImpl->onTransactionStart(cb_func, cb_data);
}

void CarbonPCITransactor::setSubsequentIRDYDelay(UInt32 value)
{
    pImpl->setSubsequentIRDYDelay(value);
}

void CarbonPCITransactor::setInitialIRDYDelay(UInt32 value)
{
    pImpl->setInitialIRDYDelay(value);
}

void CarbonPCITransactor::setFRAMEDelay(UInt32 value)
{
    pImpl->setFRAMEDelay(value);
}

void CarbonPCITransactor::setReqDelay(UInt32 value)
{
    pImpl->setReqDelay(value);
}

void CarbonPCITransactor::setSubsequentTRDYDelay(UInt32 value)
{
    pImpl->setSubsequentTRDYDelay(value);
}

void CarbonPCITransactor::setInitialTRDYDelay(UInt32 value)
{
    pImpl->setInitialTRDYDelay(value);
}

void CarbonPCITransactor::setTargetResponse(CarbonPCITransactor::TargetResponse type, UInt32 value)
{
    pImpl->setTargetResponse(type, value);
}

void CarbonPCITransactor::setRepeatTargetResponses(UInt32 value)
{
    pImpl->setRepeatTargetResponses(value);
}

void CarbonPCITransactor::setTargetBurstLength(UInt32 bl)
{
    pImpl->setTargetBurstLength(bl);
}
