// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONPCITRANSACTION_H__
#define __CARBONPCITRANSACTION_H__

#include "carbonsim/CarbonSimTransaction.h"

class CarbonPCITransaction : public CarbonSimTransaction
{
public:
    typedef enum {
	IORead = 0x2,
	IOWrite = 0x3,
	MemRead = 0x6,
	MemWrite = 0x7,
	CfgRead = 0xa,
	CfgWrite = 0xb,
	MemReadMult = 0xc,
	DAC = 0xd,
	MemReadLine = 0xe,
	MemWriteInval = 0xf
    } Cmd;

    CarbonPCITransaction(Cmd cmd, UInt64 addr, UInt32 length);
    ~CarbonPCITransaction();

    virtual void step(CarbonSimTransactor * transactor, CarbonTime tick);
    virtual bool isDone(void);

    Cmd getCmd() const { return mCmd; }
    UInt64 getAddr() const { return mAddr; }
    UInt32 getLength() const { return mLength; }

    UInt32 getData(UInt32 index) const;
    void setData(UInt32 index, UInt32 data);
    void setBE(UInt32 index, UInt8 be);
    void onCompletion(void (*cb_func)(const CarbonPCITransaction *, void *), void *cb_data);

    bool isRead() const;
    bool isWrite() const;

    // These are for the transactor to manage the transaction, not for the user
    // to add data
    UInt32 getData();
    UInt8 getBE();
    void writeData(UInt32 data);
    void nextData();

private:
    const Cmd mCmd;
    const UInt64 mAddr;
    const UInt32 mLength;
    UInt32 *mData;
    UInt8 *mBE;
    UInt32 mCurrIndex;
    void (*mCBFunc)(const CarbonPCITransaction *, void *);
    void *mCBData;

};

#endif
