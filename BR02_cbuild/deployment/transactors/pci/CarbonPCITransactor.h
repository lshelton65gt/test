// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBONPCITRANSACTOR_H__
#define __CARBONPCITRANSACTOR_H__

#include "carbonsim/CarbonSimTransactor.h"

class CarbonSimMaster;
class CarbonSimClock;
class CarbonPCITransactorImpl;

class CarbonPCITransactor : public CarbonSimTransactor
{
public:
    CarbonPCITransactor(CarbonSimMaster *sm,
			bool pullups_en = true,
			bool use_force = false,
			bool multi_xtor_mode = false);
    CarbonPCITransactor(CarbonObjectID *obj,
			bool pullups_en = true,
			bool use_force = false,
			bool multi_xtor_mode = false);
    virtual ~CarbonPCITransactor();

    // Overriding base class funcions
    virtual void step(CarbonTime tick);
    virtual void idle(UInt64 cycles);
    virtual void submit(CarbonSimTransaction * transaction);
    void sample();

    // Set the CarbonSimClock this transactor runs on
    void setClock(CarbonSimClock *clk);

    // Net binding functions
    bool bindRstL(const char *signal_name);
    bool bindAD(const char *signal_name);
    bool bindCBE(const char *signal_name);
    bool bindPar(const char *signal_name);
    bool bindPar64(const char *signal_name);
    bool bindFrameL(const char *signal_name);
    bool bindIrdyL(const char *signal_name);
    bool bindTrdyL(const char *signal_name);
    bool bindStopL(const char *signal_name);
    bool bindDevselL(const char *signal_name);
    bool bindIdsel(const char *signal_name);
    bool bindPerrL(const char *signal_name);
    bool bindSerrL(const char *signal_name);
    bool bindReqL(const char *signal_name);
    bool bindGntL(const char *signal_name);
    bool bindReq64L(const char *signal_name);
    bool bindAck64L(const char *signal_name);
    bool bindIntAL(const char *signal_name);
    bool bindIntBL(const char *signal_name);
    bool bindIntCL(const char *signal_name);
    bool bindIntDL(const char *signal_name);

    // Configuration functions
    typedef enum {
	FastDecode,
	MediumDecode,
	SlowDecode,
	SubtractiveDecode
    } DecodeSpeed;
    void setDecodeSpeed(DecodeSpeed ds);
    void setTarget64Bit(bool en);
    void setFb2bMode(bool en);
    void setBarMask(UInt32 bar, UInt32 mask, bool mem, UInt32 func);
    void setBarMask64(UInt32 bar, UInt64 mask, UInt32 func);

    // Backdoor config space reads and writes
    void writeConfigSpace(UInt32 func, UInt32 offset, UInt32 data, UInt32 mask);
    UInt32 readConfigSpace(UInt32 func, UInt32 offset);

    // Registering read/write callbacks
    void setReadHandler(UInt32 (*cb_func)(UInt32 /* addr */, UInt32 /* func */, UInt32 /* bar */, void * /* user data */), void *cb_data);
    void setWriteHandler(void (*cb_func)(UInt32 /* addr */, UInt32 /* write data */, UInt32 /* write mask */, UInt32 /* func */, UInt32 /* bar */, void * /* user data */), void *cb_data);

    // Registers a callback function to be run when a transaction begins
    void onTransactionStart(void (*cb_func)(UInt64 /* AD bus */, UInt8 /* C/BE# bus */, void* /* user data */), void *cb_data);

    // Initiator special behavior
    void setSubsequentIRDYDelay(UInt32 value);
    void setInitialIRDYDelay(UInt32 value);
    void setFRAMEDelay(UInt32 value);
    void setReqDelay(UInt32 value);
    
    // Target special behavior
    typedef enum {
	NormalResponse,
	Retry,
	DisconnectWithData /*,      Support these later?
	MasterAbort,
	TargetAbort */
    } TargetResponse;
    void setSubsequentTRDYDelay(UInt32 value);
    void setInitialTRDYDelay(UInt32 value);
    void setTargetResponse(TargetResponse type, UInt32 value);
    void setRepeatTargetResponses(UInt32 value);
    void setTargetBurstLength(UInt32 bl);
    
protected:
    CarbonPCITransactorImpl *pImpl;
};

#endif
