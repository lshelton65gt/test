// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __PCIMASTER_H__
#define __PCIMASTER_H__

class CarbonSimTransaction;
class CarbonPCITransaction;
namespace CarbonDeployment {
class PCIMasterSM;
class PinWrapper;
class PCIConfig;
}

#include "carbon/carbon_shelltypes.h"
#include <list>

namespace CarbonDeployment {

class PCIMaster
{
public:
    PCIMaster(CarbonDeployment::PCIConfig * const config,
	      const CarbonDeployment::PinWrapper &rst,
	      const CarbonDeployment::PinWrapper &gnt,
	      const CarbonDeployment::PinWrapper &trdy,
	      CarbonDeployment::PinWrapper &frame,
	      CarbonDeployment::PinWrapper &irdy,
	      const CarbonDeployment::PinWrapper &stop,
	      const CarbonDeployment::PinWrapper &devsel,
	      CarbonDeployment::PinWrapper &ad,
	      CarbonDeployment::PinWrapper &cbe,
	      CarbonDeployment::PinWrapper &par,
	      CarbonDeployment::PinWrapper &req,
	      CarbonDeployment::PinWrapper &perr);
    ~PCIMaster();

    void submit(CarbonSimTransaction * transaction);
    void step(CarbonTime tick);

    // Configuration functions
    void setFb2bMode(bool en);

    // Special behavior
    void setSubsequentIRDYDelay(UInt32 value);
    void setInitialIRDYDelay(UInt32 value);
    void setFRAMEDelay(UInt32 value);
    void setReqDelay(UInt32 value);

protected:
    std::list<CarbonPCITransaction*> mTransList;
    CarbonPCITransaction *mCurrTrans;
    CarbonDeployment::PCIMasterSM *mSM;
    CarbonDeployment::PCIConfig * const mConfig;

    // Pins
    const CarbonDeployment::PinWrapper &mRst;
    const CarbonDeployment::PinWrapper &mGnt;
    const CarbonDeployment::PinWrapper &mTrdy;
    CarbonDeployment::PinWrapper &mFrame;
    CarbonDeployment::PinWrapper &mIrdy;
    const CarbonDeployment::PinWrapper &mStop;
    const CarbonDeployment::PinWrapper &mDevsel;
    CarbonDeployment::PinWrapper &mAd;
    CarbonDeployment::PinWrapper &mCbe;
    CarbonDeployment::PinWrapper &mPar;
    CarbonDeployment::PinWrapper &mReq;
    CarbonDeployment::PinWrapper &mPerr;

    // Delayed versions of signals
    bool mFrame_d1;
    bool mGnt_d1;
    bool mStop_d1;
    UInt32 mAd_d1, mAd_d2;
    UInt32 mCbe_d1, mCbe_d2;
    bool mPar_d1;

    // Configuration options
    bool mFb2bEn;

    // Other internal state
    UInt32 mDataCount;				// data transfers remaining
    UInt64 mCurrAddr;				// current address for the transaction
    bool mWrite, mWrite_d1, mWrite_d2;		// last/current transaction is a write
    UInt32 mDevselTOCount;			// devsel timeout counter
    bool mDrivingAD, mDrivingAD_d1;
    bool mBadParity, mBadParity_d1;
    UInt32 mBackoffTimer;			// For time between retries

    // Not truly state, but needed in various places
    bool mDataXfer;
    bool mRequest;
    bool mReady;

    // Counters and settings for special behavior
    UInt32 mReqDelay;
    UInt32 mFrameDelay;
    UInt32 mInitIrdyDelay;
    UInt32 mSubIrdyDelay;
    UInt32 mReqDelayCount;
    UInt32 mFrameDelayCount;
    UInt32 mIrdyDelayCount;

    // Utility functions
    void updateInternals();			// update misc internal state
    void updateOutputs();			// update output pins
    bool devselTO();
    bool needDAC();
    void updateCurrTrans();			// update some state of the current transaction

    // Constants
    static const bool scStep = false;		// address stepping not supported
    static const bool scSameTgt = true;		// assume true - how would we know?
    static const bool scMasterTO = false;	// don't do master timout
    static const UInt32 scBackoffTimerVal = 20;	// How many cycles to wait after a retry
};

}

#endif
