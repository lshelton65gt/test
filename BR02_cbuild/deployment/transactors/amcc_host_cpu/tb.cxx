// Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.
//
// $Source: /cvs/repository/deployment/transactors/amcc_host_cpu/tb.cxx,v $
// $Id: tb.cxx,v 1.1 2006/09/22 20:16:40 ronk Exp $

#include <systemc>
#include "testbench.h"

int sc_main (int argc , char *argv[]) 
{
  try {
    // Module is an SC_MODULE that was defined somewhere else...
    testbench tb("np3450Testbench");

    sc_start();
  } catch (const sc_exception& x) {
    // Print the details of the error
    std::cerr << x.what() << std::endl;
  } catch (const char*s) {
    // This should be catching other exceptions...
    std::cerr << s << std::endl;
  } catch (...) {
    // '...' is a place holder meaning everything else
    std::cerr << "UKNOWN EXCEPTION OCCURRED" << std::endl;
  }

  return 0;
}
