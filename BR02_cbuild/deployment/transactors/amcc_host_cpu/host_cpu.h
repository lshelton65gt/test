// Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.
//
// $Source: /cvs/repository/deployment/transactors/amcc_host_cpu/host_cpu.h,v $
// $Id: host_cpu.h,v 1.1 2006/09/22 20:16:40 ronk Exp $

#ifndef _CPUXTOR_H_
#define _CPUXTOR_H_

#include <cassert>
#include <string>
#include <systemc>

#include "libnp3450vhm.systemc.h"
#include "carbon/carbon_shelltypes.h"

using namespace sc_core;
using namespace std;

extern __C__ void cds_cpuDone_run(void* hndl, const UInt32* cad);

// Interface

class host_if : virtual public sc_interface
{ 
public:
  virtual UInt32 read(UInt32 addr) = 0;
  virtual void  write(UInt32 addr, UInt32 data) = 0;
};

// Channel

class host_ch : public host_if, public sc_prim_channel
{
public:
   host_ch(CarbonObjectID *design, const char *hiername);
  ~host_ch();

  UInt32  read(UInt32 addr);
  void   write(UInt32 addr, UInt32 data);

protected:
  UInt32 mAddr, mData, mCAD;
  bool   mRWB;
  string mHdlName;
  CarbonNetID    * mNet;
  CarbonObjectID * mDesign;
  sc_event evDone;
  sc_mutex mutex;

  void cpuDone(const UInt32 &cad);

  static host_ch *gHost_ch;
  static const UInt32 ZERO;

  friend void cds_cpuDone_run(void*, const UInt32*); 
};

#endif // _CPUXTOR_H_
