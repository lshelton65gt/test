// Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.
//
// $Source: /cvs/repository/deployment/transactors/amcc_host_cpu/testbench.h,v $
// $Id: testbench.h,v 1.1 2006/09/22 20:16:40 ronk Exp $

#ifndef _TESTBENCH_H_
#define _TESTBENCH_H_

#include "np3450tb.h"

SC_MODULE(testbench)
{
   testbench(sc_module_name name);
  ~testbench();

private:
  // Clocks

  sc_clock clk50, clk62_5, clk100, clk125, clk200, clk333;

  // Signals

  sc_signal<bool>      mReset; 
  sc_signal<sc_logic>  i_pin_hcs_b; 
  sc_signal<sc_logic>  i_pin_hrd; 
  sc_signal<sc_logic>  i_pin_3450_mode; 
  sc_signal<sc_logic>  i_pin_hfba_b; 
  sc_signal<sc_logic>  i_pin_hoe_b; 
  sc_signal<sc_logic>  i_pin_s0rxv_b; 
  sc_signal<sc_logic>  i_pin_s0txfull_b; 
  sc_signal<sc_logic>  i_pin_s1rxv_b; 
  sc_signal<sc_logic>  i_pin_s1txfull_b; 
  sc_signal<sc_logic>  i_pin_s2rxv_b; 
  sc_signal<sc_logic>  i_pin_s2txfull_b; 
  sc_signal<sc_logic>  i_pin_s3rxv_b; 
  sc_signal<sc_logic>  i_pin_s3txfull_b; 

  // Testbench -- VHM and transactor channels/interfaces

  np3450tb *p3450tb;

  // Threads

  void tConfigure(void);
  void tWatchdog(void);
  void tReadWrite(void);

  // Functions

  void read(uint addr);
  void write(uint addr, uint data);
  void poll(void);

  sc_event evConfigDone; // reset/config complete

  SC_HAS_PROCESS(testbench);
};

#endif // _TESTBENCH_H_
