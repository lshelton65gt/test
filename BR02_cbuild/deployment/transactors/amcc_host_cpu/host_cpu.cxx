// Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.
//
// $Source: /cvs/repository/deployment/transactors/amcc_host_cpu/host_cpu.cxx,v $
// $Id: host_cpu.cxx,v 1.1 2006/09/22 20:16:40 ronk Exp $

#include "host_cpu.h"
#include "carbon/carbon_capi.h"
#include <iostream>

host_ch * host_ch::gHost_ch; // static global pointer

const UInt32 host_ch::ZERO = 0;

host_ch::host_ch(CarbonObjectID * design, const char *hiername)
: sc_prim_channel(sc_gen_unique_name("host_ch"))
, mDesign(design)
, mHdlName(hiername)
{
  assert(!gHost_ch); // support only 1 CPU XTOR : can be extended
  assert(  mDesign); // DesignPlayer object is required

  gHost_ch= this;

  mHdlName += ".cad";

  mNet= carbonFindNet(mDesign, mHdlName.c_str() );
  assert(mNet);
}

host_ch::~host_ch()
{
}

UInt32 host_ch::read(UInt32 addr)
{
  assert(! mutex.lock());

  mCAD  = 0x60000000; // run, read
  mCAD |= (addr & 0x1f) << 16;
  mCAD |=  0xbeef;

  carbonDeposit(mDesign, mNet, &mCAD, &ZERO);

  wait(evDone);

  assert(! mutex.unlock());

  return mData;
}

void host_ch::write(UInt32 addr, UInt32 data)
{
  assert(! mutex.lock());

  mCAD  = 0x40000000; // run, write
  mCAD |= (addr & 0x1f) << 16;
  mCAD |= (data & 0xffff);

  carbonDeposit(mDesign, mNet, &mCAD, &ZERO);

  wait(evDone);

  assert(! mutex.unlock());
}

void host_ch::cpuDone(const UInt32 &cad)
{
  mAddr = (cad & 0xf0000) >> 16;
  mData = (cad & 0xffff);
  mRWB  = (cad & 0x20000000);

//cout << ( mRWB  ? "read( " : "write( " )
//     << hex << mAddr << ", " << mData 
//     << " ) : " << sc_time_stamp() << endl;

  evDone.notify(SC_ZERO_TIME);
}

extern __C__ void
cds_cpuDone_run(void* hndl, const UInt32* cad) // Input, size = 1 word(s)
{
  assert(host_ch::gHost_ch);

  host_ch::gHost_ch->cpuDone(*cad);
}

extern __C__ void*
cds_cpuDone_create(int, CModelParam* cmodelParams, const char* ) {}

extern __C__ void cds_cpuDone_misc(void*, CarbonCModelReason, void*) {}

extern __C__ void cds_cpuDone_destroy(void* ) {}

