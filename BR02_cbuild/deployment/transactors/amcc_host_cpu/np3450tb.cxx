// Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.
//
// $Source: /cvs/repository/deployment/transactors/amcc_host_cpu/np3450tb.cxx,v $
// $Id: np3450tb.cxx,v 1.1 2006/09/22 20:16:40 ronk Exp $

#include "np3450tb.h"

np3450tb::np3450tb(sc_module_name name)
: sc_module(name)
, clk50  ("clk50")
, clk62_5("clk62_5")
, clk100 ("clk100")
, clk125 ("clk125")
, clk200 ("clk200")
, clk333 ("clk333")

, mReset           ("mReset")
, i_pin_hrd        ("i_pin_hrd")
, i_pin_hcs_b      ("i_pin_hcs_b")
, i_pin_hoe_b      ("i_pin_hoe_b")
, i_pin_hfba_b     ("i_pin_hfba_b")
, i_pin_s0rxv_b    ("i_pin_s0rxv_b")
, i_pin_s1rxv_b    ("i_pin_s1rxv_b")
, i_pin_s2rxv_b    ("i_pin_s2rxv_b")
, i_pin_s3rxv_b    ("i_pin_s3rxv_b")
, i_pin_s0txfull_b ("i_pin_s0txfull_b")
, i_pin_s1txfull_b ("i_pin_s1txfull_b")
, i_pin_s2txfull_b ("i_pin_s2txfull_b")
, i_pin_s3txfull_b ("i_pin_s3txfull_b")
, i_pin_3450_mode  ("i_pin_3450_mode")

{
  p3450vhm = new np3450vhm("np3450vhm");
  assert(p3450vhm);

  pHost_ch = new host_ch(p3450vhm->carbonModelHandle, "np3450vhm.cpu");
  assert(pHost_ch);

  host_cpu(*pHost_ch); // bind sc_export to channel

  p3450vhm->mClk50(clk50);
  p3450vhm->mClk62_5(clk62_5);
  p3450vhm->mClk100(clk100);
  p3450vhm->mClk125(clk125);
  p3450vhm->mClk200(clk200);
  p3450vhm->mClk333(clk333);

  p3450vhm->mReset(mReset); 
  p3450vhm->i_pin_hcs_b(i_pin_hcs_b); 
  p3450vhm->i_pin_hrd(i_pin_hrd); 
  p3450vhm->i_pin_3450_mode(i_pin_3450_mode); 
  p3450vhm->i_pin_hfba_b(i_pin_hfba_b); 
  p3450vhm->i_pin_hoe_b(i_pin_hoe_b); 
  p3450vhm->i_pin_s0rxv_b(i_pin_s0rxv_b); 
  p3450vhm->i_pin_s0txfull_b(i_pin_s0txfull_b); 
  p3450vhm->i_pin_s1rxv_b(i_pin_s1rxv_b); 
  p3450vhm->i_pin_s1txfull_b(i_pin_s1txfull_b); 
  p3450vhm->i_pin_s2rxv_b(i_pin_s2rxv_b); 
  p3450vhm->i_pin_s2txfull_b(i_pin_s2txfull_b); 
  p3450vhm->i_pin_s3rxv_b(i_pin_s3rxv_b); 
  p3450vhm->i_pin_s3txfull_b(i_pin_s3txfull_b); 
}

np3450tb::~np3450tb()
{
  p3450vhm->carbonSCDumpFlush();

  delete p3450vhm;
}

void np3450tb::dumpWaves(const char* fileName, sc_time_unit timescale)
{
  // wave dumping
  // ------------
  p3450vhm->carbonSCWaveInitFSDB(fileName, timescale);

  p3450vhm->carbonSCDumpVars();
}

