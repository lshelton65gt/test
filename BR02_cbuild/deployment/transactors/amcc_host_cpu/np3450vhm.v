// $Source: /cvs/repository/deployment/transactors/amcc_host_cpu/np3450vhm.v,v $
// $Id: np3450vhm.v,v 1.1 2006/09/22 20:16:40 ronk Exp $

// Instantiate np3450 and CPU xtor

`timescale  1ns /  10ps

module np3450vhm (
   mClk50   
 , mClk62_5 
 , mClk100  
 , mClk125  
 , mClk200  
 , mClk333  
 , mReset   

 , i_pin_3450_mode
 , i_pin_s0txfull_b
 , i_pin_s1txfull_b
 , i_pin_s2txfull_b
 , i_pin_s3txfull_b
 , i_pin_s0rxv_b
 , i_pin_s1rxv_b
 , i_pin_s2rxv_b
 , i_pin_s3rxv_b
 , i_pin_hrd
 , i_pin_hcs_b
 , i_pin_hoe_b
 , i_pin_hfba_b
);

  input mClk50, mClk62_5, mClk100, mClk125, mClk200, mClk333, mReset;

  wire i_pin_clk        = mClk50;
  wire i_pin_hclk       = mClk50;
  wire sclk50           = mClk50;
  wire i_pin_s0rxclk    = mClk62_5;
  wire i_pin_s1rxclk    = mClk62_5;
  wire i_pin_s2rxclk    = mClk62_5;
  wire i_pin_s3rxclk    = mClk62_5;
  wire sclk100          = mClk100;
  wire xclk100          = mClk100;
  wire i_pin_refclk     = mClk125;
  wire i_pin_s0txclk    = mClk125;
  wire i_pin_s1txclk    = mClk125;
  wire i_pin_s2txclk    = mClk125;
  wire i_pin_s3txclk    = mClk125;
  wire sclk200          = mClk200;
  wire xclk200          = mClk200;
  wire nguc_farm1_clk   = mClk333;
  wire i_pin_rst_b      = mReset;

  input i_pin_3450_mode;
  input i_pin_s0txfull_b;
  input i_pin_s1txfull_b;
  input i_pin_s2txfull_b;
  input i_pin_s3txfull_b;
  input i_pin_s0rxv_b;
  input i_pin_s1rxv_b;
  input i_pin_s2rxv_b;
  input i_pin_s3rxv_b;
  input i_pin_hrd;
  input i_pin_hcs_b;
  input i_pin_hoe_b;
  input i_pin_hfba_b;

  tri1 i_pin_3450_mode;
  tri1 i_pin_s0txfull_b;
  tri1 i_pin_s1txfull_b;
  tri1 i_pin_s2txfull_b;
  tri1 i_pin_s3txfull_b;
  tri1 i_pin_s0rxv_b;
  tri1 i_pin_s1rxv_b;
  tri1 i_pin_s2rxv_b;
  tri1 i_pin_s3rxv_b;
  tri1 i_pin_hrd;
  tri1 i_pin_hcs_b;
  tri1 i_pin_hoe_b;
  tri1 i_pin_hfba_b;

  wire          i_pin_ecs_b;
  wire          i_pin_ed;
  wire          i_pin_g0rbc1;
  wire          i_pin_g1rbc1;
  wire          i_pin_g2rbc1;
  wire          i_pin_g3rbc1;
  wire [7:0]    i_pin_gpio;
  wire [3:0]    i_pin_ha;
  wire [15:0]   i_pin_hd;
  wire [1:0]    i_pin_mbwe67b_b;
  wire          i_pin_mclk;
  wire [1:0]    i_pin_mcs34b_b;
  wire [63:0]   i_pin_md;
  wire          i_pin_mdio;
  wire          i_pin_notrdy_b;
  wire [95:0]   i_pin_pd;
  wire          i_pin_pllbp;
  wire [23:0]   i_pin_rxd;
  wire          i_pin_s0rxc;
  wire [15:0]   i_pin_s0rxd;
  wire          i_pin_s0rxp;
  wire          i_pin_s1rxc;
  wire [15:0]   i_pin_s1rxd;
  wire          i_pin_s1rxp;
  wire          i_pin_s2rxc;
  wire [15:0]   i_pin_s2rxd;
  wire          i_pin_s2rxp;
  wire          i_pin_s3rxc;
  wire [15:0]   i_pin_s3rxd;
  wire          i_pin_s3rxp;
  wire [3:0]    i_pin_spare;
  wire [4:0]    i_pin_ssem;
  wire          i_pin_sstrobe;
  wire          i_pin_xsc0_vld;
  wire          i_pin_xsc1_vld;
  wire [1:0]    i_pin_xsm_clk;
  wire [2:0]    o_pin_clko3450;
  wire [2:0]    o_pin_clko3454;
  wire          o_pin_eclk;
  wire          o_pin_ecs_b;
  wire          o_pin_ed;
  wire [7:0]    o_pin_gpio;
  wire [15:0]   o_pin_hd;
  wire          o_pin_hintr_b;
  wire          o_pin_hrfrq_b;
  wire          o_pin_hwfrq_b;
  wire [20:0]   o_pin_ma;
  wire [1:0]    o_pin_mbwe67b_b;
  wire [5:0]    o_pin_mbwe_b;
  wire [1:0]    o_pin_mclko;
  wire [1:0]    o_pin_mcs34b_b;
  wire [2:0]    o_pin_mcs_b;
  wire [63:0]   o_pin_md;
  wire          o_pin_mdc;
  wire          o_pin_mdio;
  wire          o_pin_moe_b;
  wire          o_pin_msz_b;
  wire [19:0]   o_pin_pa;
  wire [1:0]    o_pin_pce_b;
  wire [95:0]   o_pin_pd;
  wire [1:0]    o_pin_poe_b;
  wire          o_pin_pwe_b;
  wire          o_pin_rsto_b;
  wire          o_pin_s0rxfull_b;
  wire          o_pin_s0txc;
  wire          o_pin_s0txclko;
  wire [15:0]   o_pin_s0txd;
  wire          o_pin_s0txp;
  wire          o_pin_s0txv_b;
  wire          o_pin_s1rxfull_b;
  wire          o_pin_s1txc;
  wire          o_pin_s1txclko;
  wire [15:0]   o_pin_s1txd;
  wire          o_pin_s1txp;
  wire          o_pin_s1txv_b;
  wire          o_pin_s2rxfull_b;
  wire          o_pin_s2txc;
  wire          o_pin_s2txclko;
  wire [15:0]   o_pin_s2txd;
  wire          o_pin_s2txp;
  wire          o_pin_s2txv_b;
  wire          o_pin_s3rxfull_b;
  wire          o_pin_s3txc;
  wire          o_pin_s3txclko;
  wire [15:0]   o_pin_s3txd;
  wire          o_pin_s3txp;
  wire          o_pin_s3txv_b;
  wire [3:0]    o_pin_spare;
  wire [3:0]    o_pin_sync;
  wire [23:0]   o_pin_txd;
  wire          o_pin_xsc0_vld;
  wire          o_pin_xsc1_vld;
  wire          oe_pin_ecs_b;
  wire          oe_pin_ed;
  wire [7:0]    oe_pin_gpio;
  wire [1:0]    oe_pin_hd_b;
  wire [1:0]    oe_pin_mbwe67_en;
  wire [1:0]    oe_pin_mcs34_en;
  wire [63:0]   oe_pin_md_b;
  wire          oe_pin_mdio;
  wire          oe_pin_notest_b;
  wire [11:0]   oe_pin_pd_b;
  wire [3:0]    oe_pin_spare;
  wire          oe_pin_xsc0_vld;
  wire          oe_pin_xsc1_vld;

  host_cpu cpu (
      .rstb(i_pin_rst_b)
    , .clk(i_pin_hclk)
    , .addr(i_pin_ha)
    , .dout(i_pin_hd)
    , .din(o_pin_hd)
    , .cs(i_pin_hcs_b)
    , .rwb(i_pin_hrd)
    , .intr(o_pin_hintr_b)

    , .gpio(8'b0100) // hack to test rst/gpio logic
//  , .gpio(o_pin_gpio)

//  , .COE(i_pin_hoe_b)
//  , .CFBA(i_pin_hfba_b)
//  , .CWFRQ(o_pin_hwfrq_b)
//  , .CRFRQ(o_pin_hrfrq_b)
  );

  core_np345x np3450 (
      .i_pin_mcs34b_b(i_pin_mcs34b_b)
    , .o_pin_xsc0_vld(o_pin_xsc0_vld)
    , .o_pin_xsc1_vld(o_pin_xsc1_vld)
    , .i_pin_xsc0_vld(i_pin_xsc0_vld)
    , .i_pin_xsc1_vld(i_pin_xsc1_vld)
    , .oe_pin_xsc0_vld(oe_pin_xsc0_vld)
    , .oe_pin_xsc1_vld(oe_pin_xsc1_vld)
    , .i_pin_clk(i_pin_clk)
    , .i_pin_rst_b(i_pin_rst_b)
    , .o_pin_clko3450(o_pin_clko3450)
    , .o_pin_clko3454(o_pin_clko3454)
    , .o_pin_rsto_b(o_pin_rsto_b)
    , .o_pin_eclk(o_pin_eclk)
    , .i_pin_ecs_b(i_pin_ecs_b)
    , .o_pin_ecs_b(o_pin_ecs_b)
    , .oe_pin_ecs_b(oe_pin_ecs_b)
    , .i_pin_ed(i_pin_ed)
    , .o_pin_ed(o_pin_ed)
    , .oe_pin_ed(oe_pin_ed)
    , .i_pin_gpio(i_pin_gpio)
    , .o_pin_gpio(o_pin_gpio)
    , .oe_pin_gpio(oe_pin_gpio)
    , .i_pin_hclk(i_pin_hclk)
    , .i_pin_hd(i_pin_hd)
    , .o_pin_hd(o_pin_hd)
    , .oe_pin_hd_b(oe_pin_hd_b)
    , .i_pin_ha(i_pin_ha)
    , .i_pin_hrd(i_pin_hrd)
    , .i_pin_hcs_b(i_pin_hcs_b)
    , .i_pin_hoe_b(i_pin_hoe_b)
    , .i_pin_hfba_b(i_pin_hfba_b)
    , .o_pin_hwfrq_b(o_pin_hwfrq_b)
    , .o_pin_hrfrq_b(o_pin_hrfrq_b)
    , .o_pin_hintr_b(o_pin_hintr_b)
    , .i_pin_pd(i_pin_pd)
    , .o_pin_pd(o_pin_pd)
    , .oe_pin_pd_b(oe_pin_pd_b)
    , .o_pin_pa(o_pin_pa)
    , .o_pin_pwe_b(o_pin_pwe_b)
    , .o_pin_pce_b(o_pin_pce_b)
    , .o_pin_poe_b(o_pin_poe_b)
    , .i_pin_md(i_pin_md)
    , .o_pin_md(o_pin_md)
    , .oe_pin_md_b(oe_pin_md_b)
    , .o_pin_ma(o_pin_ma)
    , .o_pin_mcs_b(o_pin_mcs_b)
    , .o_pin_mcs34b_b(o_pin_mcs34b_b)
    , .oe_pin_mcs34_en(oe_pin_mcs34_en)
    , .i_pin_mbwe67b_b(i_pin_mbwe67b_b)
    , .oe_pin_mbwe67_en(oe_pin_mbwe67_en)
    , .o_pin_mbwe_b(o_pin_mbwe_b)
    , .o_pin_mbwe67b_b(o_pin_mbwe67b_b)
    , .o_pin_msz_b(o_pin_msz_b)
    , .o_pin_moe_b(o_pin_moe_b)
    , .i_pin_notrdy_b(i_pin_notrdy_b)
    , .i_pin_ssem(i_pin_ssem)
    , .i_pin_sstrobe(i_pin_sstrobe)
    , .i_pin_mclk(i_pin_mclk)
    , .o_pin_mclko(o_pin_mclko)
    , .i_pin_s2txclk(i_pin_s2txclk)
    , .o_pin_s2txclko(o_pin_s2txclko)
    , .o_pin_s2txd(o_pin_s2txd)
    , .o_pin_s2txv_b(o_pin_s2txv_b)
    , .o_pin_s2txp(o_pin_s2txp)
    , .o_pin_s2txc(o_pin_s2txc)
    , .i_pin_s2txfull_b(i_pin_s2txfull_b)
    , .i_pin_s2rxclk(i_pin_s2rxclk)
    , .i_pin_g2rbc1(i_pin_g2rbc1)
    , .i_pin_s2rxd(i_pin_s2rxd)
    , .i_pin_rxd(i_pin_rxd)
    , .o_pin_txd(o_pin_txd)
    , .i_pin_s2rxv_b(i_pin_s2rxv_b)
    , .i_pin_s2rxp(i_pin_s2rxp)
    , .i_pin_s2rxc(i_pin_s2rxc)
    , .o_pin_s2rxfull_b(o_pin_s2rxfull_b)
    , .i_pin_s3txclk(i_pin_s3txclk)
    , .o_pin_s3txclko(o_pin_s3txclko)
    , .o_pin_s3txd(o_pin_s3txd)
    , .o_pin_s3txv_b(o_pin_s3txv_b)
    , .o_pin_s3txp(o_pin_s3txp)
    , .o_pin_s3txc(o_pin_s3txc)
    , .i_pin_s3txfull_b(i_pin_s3txfull_b)
    , .i_pin_s3rxclk(i_pin_s3rxclk)
    , .i_pin_g3rbc1(i_pin_g3rbc1)
    , .i_pin_s3rxd(i_pin_s3rxd)
    , .i_pin_s3rxv_b(i_pin_s3rxv_b)
    , .i_pin_s3rxp(i_pin_s3rxp)
    , .i_pin_s3rxc(i_pin_s3rxc)
    , .o_pin_s3rxfull_b(o_pin_s3rxfull_b)
    , .i_pin_refclk(i_pin_refclk)
    , .o_pin_sync(o_pin_sync)
    , .i_pin_s0txclk(i_pin_s0txclk)
    , .o_pin_s0txclko(o_pin_s0txclko)
    , .o_pin_s0txd(o_pin_s0txd)
    , .o_pin_s0txv_b(o_pin_s0txv_b)
    , .o_pin_s0txp(o_pin_s0txp)
    , .o_pin_s0txc(o_pin_s0txc)
    , .i_pin_s0txfull_b(i_pin_s0txfull_b)
    , .i_pin_s0rxclk(i_pin_s0rxclk)
    , .i_pin_g0rbc1(i_pin_g0rbc1)
    , .i_pin_s0rxd(i_pin_s0rxd)
    , .i_pin_s0rxv_b(i_pin_s0rxv_b)
    , .i_pin_s0rxp(i_pin_s0rxp)
    , .i_pin_s0rxc(i_pin_s0rxc)
    , .o_pin_s0rxfull_b(o_pin_s0rxfull_b)
    , .i_pin_s1txclk(i_pin_s1txclk)
    , .o_pin_s1txclko(o_pin_s1txclko)
    , .o_pin_s1txd(o_pin_s1txd)
    , .o_pin_s1txv_b(o_pin_s1txv_b)
    , .o_pin_s1txp(o_pin_s1txp)
    , .o_pin_s1txc(o_pin_s1txc)
    , .i_pin_s1txfull_b(i_pin_s1txfull_b)
    , .i_pin_s1rxclk(i_pin_s1rxclk)
    , .i_pin_g1rbc1(i_pin_g1rbc1)
    , .i_pin_s1rxd(i_pin_s1rxd)
    , .i_pin_s1rxv_b(i_pin_s1rxv_b)
    , .i_pin_s1rxp(i_pin_s1rxp)
    , .i_pin_s1rxc(i_pin_s1rxc)
    , .o_pin_s1rxfull_b(o_pin_s1rxfull_b)
    , .o_pin_mdc(o_pin_mdc)
    , .i_pin_mdio(i_pin_mdio)
    , .o_pin_mdio(o_pin_mdio)
    , .oe_pin_mdio(oe_pin_mdio)
    , .oe_pin_notest_b(oe_pin_notest_b)
    , .i_pin_xsm_clk(i_pin_xsm_clk)
    , .i_pin_pllbp(i_pin_pllbp)
    , .i_pin_spare(i_pin_spare)
    , .o_pin_spare(o_pin_spare)
    , .oe_pin_spare(oe_pin_spare)
    , .i_pin_3450_mode(i_pin_3450_mode)
    , .xclk200(xclk200)
    , .xclk100(xclk100)
    , .sclk200(sclk200)
    , .sclk100(sclk100)
    , .sclk50(sclk50)
    , .nguc_farm1_clk(nguc_farm1_clk)
  );

endmodule
