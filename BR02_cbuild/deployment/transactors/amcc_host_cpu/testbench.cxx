// Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.
//
// $Source: /cvs/repository/deployment/transactors/amcc_host_cpu/testbench.cxx,v $
// $Id: testbench.cxx,v 1.1 2006/09/22 20:16:40 ronk Exp $

#include "testbench.h"

#include <iomanip>

testbench::testbench(sc_module_name name)
: sc_module(name)
, clk50  ("clk50",   20, 0.5, 0, false)
, clk62_5("clk62_5", 16, 0.5, 0, false)
, clk100 ("clk100",  10, 0.5, 0, false)
, clk125 ("clk125",   8, 0.5, 0, false)
, clk200 ("clk200",   5, 0.5, 0, false)
, clk333 ("clk333",   3, 0.5, 0, false)

, mReset           ("mReset")
, i_pin_hrd        ("i_pin_hrd")
, i_pin_hcs_b      ("i_pin_hcs_b")
, i_pin_hoe_b      ("i_pin_hoe_b")
, i_pin_hfba_b     ("i_pin_hfba_b")
, i_pin_s0rxv_b    ("i_pin_s0rxv_b")
, i_pin_s1rxv_b    ("i_pin_s1rxv_b")
, i_pin_s2rxv_b    ("i_pin_s2rxv_b")
, i_pin_s3rxv_b    ("i_pin_s3rxv_b")
, i_pin_s0txfull_b ("i_pin_s0txfull_b")
, i_pin_s1txfull_b ("i_pin_s1txfull_b")
, i_pin_s2txfull_b ("i_pin_s2txfull_b")
, i_pin_s3txfull_b ("i_pin_s3txfull_b")
, i_pin_3450_mode  ("i_pin_3450_mode")

{
  p3450tb = new np3450tb("np3450tb");
  assert(p3450tb);

  p3450tb->clk50(clk50);
  p3450tb->clk62_5(clk62_5);
  p3450tb->clk100(clk100);
  p3450tb->clk125(clk125);
  p3450tb->clk200(clk200);
  p3450tb->clk333(clk333);

  p3450tb->mReset(mReset); 
  p3450tb->i_pin_hcs_b(i_pin_hcs_b); 
  p3450tb->i_pin_hrd(i_pin_hrd); 
  p3450tb->i_pin_3450_mode(i_pin_3450_mode); 
  p3450tb->i_pin_hfba_b(i_pin_hfba_b); 
  p3450tb->i_pin_hoe_b(i_pin_hoe_b); 
  p3450tb->i_pin_s0rxv_b(i_pin_s0rxv_b); 
  p3450tb->i_pin_s0txfull_b(i_pin_s0txfull_b); 
  p3450tb->i_pin_s1rxv_b(i_pin_s1rxv_b); 
  p3450tb->i_pin_s1txfull_b(i_pin_s1txfull_b); 
  p3450tb->i_pin_s2rxv_b(i_pin_s2rxv_b); 
  p3450tb->i_pin_s2txfull_b(i_pin_s2txfull_b); 
  p3450tb->i_pin_s3rxv_b(i_pin_s3rxv_b); 
  p3450tb->i_pin_s3txfull_b(i_pin_s3txfull_b); 

  // wave dumping
  // ------------
  p3450tb->dumpWaves("carbon.fsdb", SC_PS);

  // Setup Threads
  // -------------

  SC_THREAD(tWatchdog);

  SC_THREAD(tConfigure);

  SC_THREAD(tReadWrite);
  sensitive << evConfigDone; // reset/config complete
  dont_initialize();
}

testbench::~testbench()
{
  delete p3450tb;
}

void testbench::tConfigure(void)
{
  cout << "start config : " << sc_time_stamp() << endl;

  sc_logic zero('0'), one('1');

  i_pin_hcs_b.write(one);
  i_pin_hrd.write(one);
  i_pin_3450_mode.write(one);
  i_pin_hfba_b.write(one);
  i_pin_hoe_b.write(one);
  i_pin_s0rxv_b.write(one);
  i_pin_s1rxv_b.write(one);
  i_pin_s2rxv_b.write(one);
  i_pin_s3rxv_b.write(one);
  i_pin_s0txfull_b.write(one);
  i_pin_s1txfull_b.write(one);
  i_pin_s2txfull_b.write(one);
  i_pin_s3txfull_b.write(one);

  mReset.write(false); // assert

  wait( clk100.period() * 10 ); // wait 10 clocks
  
  mReset.write(true); // release

  wait( clk100.period() * 900 ); // allow chip to reset

  cout << "config complete : " << sc_time_stamp() << endl;

  evConfigDone.notify(SC_ZERO_TIME);
}

void testbench::tWatchdog(void)
{
  wait( clk100.period() * 2000  ); // wait 2,000 clocks : 20 uS

  cout << "watchdog timeout at " << sc_time_stamp() << endl;

  sc_stop();
}

enum CpuAddr {
  GCR, GSRH, GSRL, IMRH, IMRL, CMR,  ADDR,  R0,
  VER, MBXR, MPR,  HFSR, HRFR, HWFR, HRHDR, HRPL
};

static char * cpuAddr[] = {
  "GCR", "GSRH", "GSRL", "IMRH", "IMRL", "CMR",  "ADDR",  "R0",
  "VER", "MBXR", "MPR",  "HFSR", "HRFR", "HWFR", "HRHDR", "HRPL"
};

void testbench::read(uint addr)
{
  uint data;

  data= p3450tb->host_cpu->read( addr  );

  cout << "The read data at "
       << cpuAddr[addr] << " (0x"
       << setw(4) << setfill('0') << hex 
       << addr << ") is: 0x"
       << setw(4) << setfill('0') << hex 
       << data << ".\n";
}

void testbench::write(uint addr, uint data)
{
  p3450tb->host_cpu->write( addr, data  );

  cout << "The write data at "
       << cpuAddr[addr] << " (0x"
       << setw(4) << setfill('0') << hex 
       << addr << ") is: 0x"
       << setw(4) << setfill('0') << hex 
       << data << ".\n";
}

void testbench::poll()
{
  //  # Write 32 bits into the ADDR register (msb, lsb)
  write( ADDR, 0x000e );
  write( ADDR, 0x0000 );

  // Write the command.
  write( CMR  , 0xe800 );

  // Poll. The CMR should get cleared.
  for(int n=0; n < 4; n++) {
    read( CMR  );
    read( GSRH );
    read( GSRL );
  }
  read( GSRL  );
}


void testbench::tReadWrite(void)
{
  cout << "start read/write : " << sc_time_stamp() << endl;

  cout << "proc simple_read_and_write { } " << endl;

  uint data= p3450tb->host_cpu->read( VER  );

  cout << "The version number is: 0x"
       << setw(4)
       << setfill('0')
       << hex 
       << data << ".\n";

   read( ADDR  );
  write( ADDR, 0xc0d0 );
   read( ADDR  );

  cout << "proc simple_command { } " << endl;

  poll();

  cout << "proc simple_command_with_intr { } " << endl;

  // Enable interrupts and provide a handler.
  write( IMRH, 0xffff );

//  on_cintr 

  poll();

  cout << "read/write complete : " << sc_time_stamp() << endl;

  sc_stop();
}
