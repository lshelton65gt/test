// Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.
//
// $Source: /cvs/repository/deployment/transactors/amcc_host_cpu/np3450tb.h,v $
// $Id: np3450tb.h,v 1.1 2006/09/22 20:16:40 ronk Exp $

#ifndef _NP3450TB_H_
#define _NP3450TB_H_

#include "host_cpu.h"

SC_MODULE(np3450tb)
{
   np3450tb(sc_module_name name);
  ~np3450tb();

  void dumpWaves(const char* fileName= NULL, sc_time_unit timescale= SC_PS);

  // Ports

  sc_in<bool> clk50, clk62_5, clk100, clk125, clk200, clk333;
  sc_in<bool> mReset; 

  sc_inout<sc_logic> i_pin_hcs_b; 
  sc_inout<sc_logic> i_pin_hrd; 
  sc_inout<sc_logic> i_pin_3450_mode; 
  sc_inout<sc_logic> i_pin_hfba_b; 
  sc_inout<sc_logic> i_pin_hoe_b; 
  sc_inout<sc_logic> i_pin_s0rxv_b; 
  sc_inout<sc_logic> i_pin_s0txfull_b; 
  sc_inout<sc_logic> i_pin_s1rxv_b; 
  sc_inout<sc_logic> i_pin_s1txfull_b; 
  sc_inout<sc_logic> i_pin_s2rxv_b; 
  sc_inout<sc_logic> i_pin_s2txfull_b; 
  sc_inout<sc_logic> i_pin_s3rxv_b; 
  sc_inout<sc_logic> i_pin_s3txfull_b; 

  sc_export<host_if> host_cpu;

private:
  // Testbench -- VHM and transactor channels/interfaces

  np3450vhm *p3450vhm;
  host_ch   *pHost_ch;
};

#endif // _NP3450TB_H_
