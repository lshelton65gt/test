// Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.
//
// $Source: /cvs/repository/deployment/transactors/amcc_host_cpu/host_cpu.v,v $
// $Id: host_cpu.v,v 1.1 2006/09/22 20:16:40 ronk Exp $

module host_cpu (
      rstb  // (i_pin_rst_b)
    , clk   // (i_pin_hclk)
    , addr  // (i_pin_ha)
    , dout  // (i_pin_hd)
    , din   // (o_pin_hd)
    , cs    // (i_pin_hcs_b)
    , rwb   // (i_pin_hrd)
    , intr  // (o_pin_hintr_b)
    , gpio  // (o_pin_gpio)
//  , COE   // (i_pin_hoe_b)
//  , CFBA  // (i_pin_hfba_b)
//  , CWFRQ // (o_pin_hwfrq_b)
//  , CRFRQ // (o_pin_hrfrq_b)
);

   input        rstb;
   input        clk;
   input        intr;
   input [15:0] din;
   input  [7:0] gpio;
  output  [3:0] addr;
  output [15:0] dout;
  output        cs;
  output        rwb;

  wire        rstb;
  wire        clk;
  wire        intr; // carbon observeSignal
  wire [15:0] din;
  wire  [7:0] gpio;
  reg   [4:0] addr;
  reg  [15:0] dout;
  reg         cs;
  reg         rwb;

  reg   [7:0] pio; // sampled on posedge rstb
  reg   [2:0] state;
  reg   [1:0] delay;
  reg  [31:0] cad; // control, addr, data
                   // carbon depositSignal

  wire        mDone = cad[31];
  wire        mRun  = cad[30];
  wire        mRW   = cad[29];
  wire  [3:0] mAddr = cad[19:16];
  wire [15:0] mData = cad[15:0];

  parameter // state machine
    IDLE    = 3'b000,
    READ    = 3'b001,
    RD_WAIT = 3'b010,
    RD_DONE = 3'b011,
    WRITE   = 3'b100,
    WR_WAIT = 3'b101,
    WR_DONE = 3'b110;

  parameter CPU_DONE = 16'h80000000;

  initial
    begin
       state <= IDLE;
       delay <=  2'h0;
       dout  <= 16'h0000;
       addr  <=  5'h0;
       pio   <=  8'h0;
       cs    <=  1'b1;
       rwb   <=  1'b1;
       cad   <= 32'h00000000;
    end

  always @(posedge rstb)
    pio  <=  gpio;

  always @(posedge clk)
    case(state)
       IDLE: begin
               if( mRun )
                 if( mRW )
                   state <= READ;
                 else
                   state <= WRITE;
             end

       READ: begin // A single read access lasts three or four clocks.
               rwb   <= 1'b1;
               cs    <= 1'b0;
               addr  <= mAddr;
               state <= RD_WAIT;
               delay <= pio[2];
             end

       RD_WAIT: begin // clocks = [2,3]
               cs    <=  1'b1;
               if(! delay)
                 state <= RD_DONE;
               else
                 delay <= delay - 1;
             end

       RD_DONE: begin // clocks = [3,4]
               state <= IDLE;
               cad   <= CPU_DONE;
               $cpuDone( { 11'h500, addr, din });
             end

       WRITE: begin // A single write access lasts one, two, or three clocks.
               rwb   <= 1'b0;
               cs    <= 1'b0;
               addr  <= mAddr;
               case( pio[1:0] )
                 0: state <= WR_DONE;

                 1,
                 2, 
                 3: // unspecified ?!?
                    begin
                      state <= WR_WAIT;
                      delay <= pio[1:0] - 1;
                    end

                 default: state <= WR_DONE;
               endcase
             end

       WR_WAIT: begin // clocks = [1,2,3]
                      // TODO -- check pio[3] == pipeline
               rwb   <= 1'b1;
               cs    <= 1'b1;
               if(! delay)
                 state <= WR_DONE;
               else
                 delay <= delay - 1;
             end

       WR_DONE: begin // clocks = [2,3,4]
               rwb   <= 1'b1;
               cs    <= 1'b1;
               dout  <= mData;
               state <= IDLE;
               cad   <= CPU_DONE;
               $cpuDone( { 11'h400, mAddr, mData });
             end

      default: state <= IDLE;
    endcase

endmodule
