// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* %PCIE_REVNUM% */

/*!
  \file
  PCI-Express Transactor for Carbon DesignPlayer models.
*/

#ifndef __PCIEXTOR_H__
#define __PCIEXTOR_H__

#include "carbon/carbon.h"

/*!
  \mainpage PCI-Express Transactor API Reference

  \htmlonly
  <center>
  \endhtmlonly

  \image html cds-logo.gif

  \htmlonly
  </center>
  \endhtmlonly

  \section intro Introduction to the PCI-Express Transactor

  The PCI-Express Transcator is a bus functional model used to connect
  a C/C++ testbench to a Carbon DesignPlayer.
*/

/*! \defgroup pciexpress PCI-Express Bus Functional Transactor
  @{
*/

// Declare implementation classes
class PCIExpressCommand;
class PCIExpressHandler;
class PCIExpressTransactor;
class PCIEXtor;

class CarbonSimMaster;
class CarbonSimClock;
class CarbonSimNet;
#include "carbonsim/CarbonSimTransactor.h"

/*
** Public Interfaces for the Carbon PCI-Express Transactor
*/

//! PCIECmd class
/*!
  Every transactor will consist of a number of transactions.  This
  class provides a uniform method of adding and calling transactions. 
  A transaction is a single operation. For example, the bytes of
  a configuration space read operation are combined to make \e one
  transaction.  This class provides stimulus to the PCIEXtor class.
*/
class PCIECmd {
 public:
   //!  Types of transactions available in the PCI-Express Base Specification Rev 1.0a.
   /*!
     The numeric values are 7-bits wide, corresponding to the concatination
     of the 2-bit format and 5-bit type.
   */
   typedef enum {
      MRd32 = 0x00,   //!< Memory Read Request, 32-bit address
      MRd64 = 0x20,   //!< Memory Read Request, 64-bit address
      MRdLk32 = 0x01, //!< Memory Read Request - Locked, 32-bit address
      MRdLk64 = 0x21, //!< Memory Read Request - Locked, 64-bit address
      MWr32 = 0x40,   //!< Memory Write Request, 32-bit address
      MWr64 = 0x60,   //!< Memory Write Request, 64-bit address
      IORd = 0x02,    //!< I/O Read Request
      IOWr = 0x42,    //!< I/O Write Request
      CfgRd0 = 0x04,  //!< Configuration Read Type 0
      CfgWr0 = 0x44,  //!< Configuration Write Type 0
      CfgRd1 = 0x05,  //!< Configuration Read Type 1
      CfgWr1 = 0x45,  //!< Configuration Write Type 1
      Msg = 0x30,     //!< Message Request
      MsgD = 0x70,    //!< Message Request with data payload
      Cpl = 0x0A,     //!< Completion without Data
      CplD = 0x4A,    //!< Completion with Data
      CplLk = 0x0B,   //!< Completion for Locked Memory Read without Data
      CplDLk = 0x4B   //!< Completion for Locked Memory Read with Data
   } PCIECmdType;


   //! Void Constructor.
   /*!
     All values will be initialized to zero and the command type will be Msg.
   */
   PCIECmd();
   //! Copy Constructor.
   /*!
     The contents of the PCIECmd object passed in will be copied to the
     referenced object.
     \param orig A pointer to the PCIECmd object to be copied.
   */
   PCIECmd(const PCIECmd& orig);
   //! Destructor.
   ~PCIECmd();

   //! Get c-style string for the PCIECmd::PCIECmdType value.
   /*!
     This function is provided to enhance printing messages.
     \param type The type to get the print string for.
     \return The c-style string for message printing.
   */
   static const char* getStrFromType (PCIECmdType type);

   //! Copy object function
   /*!
     The contents of the PCIECmd object passed in will be copied
     to the referenced object.
     \param orig A pointer to the PCIECmd object to be copied.
   */
   void   copy         (const PCIECmd& orig);

   //! Sets the command type for the transaction.
   /*!
     \param cmd The 7-bit value to be set.
   */
   void   setCmd       (UInt32 cmd);
   //! Gets the command type for the transaction.
   /*!
     \return The 7-bit command type value.
   */
   UInt32 getCmd       (void);
   //! Sets the command type for the transaction.
   /*!
     \param cmd The PCIECmdType value to be set.
   */
   void   setCmdByName (PCIECmd::PCIECmdType cmd);
   //! Gets the command type for the transaction.
   /*!
     \return The PCIECmd::PCIECmdType command type value.
   */
   PCIECmd::PCIECmdType getCmdByName (void);
   //! Sets the address for the transaction.
   /*!
     If only 32-bits are needed for the transaction then the upper 32-bits
     will be masked out when used.
     \param addr The 64-bit value to be set.
   */
   void   setAddr      (UInt64 addr);
   //! Gets the address for the transaction.
   /*!
     The value will be the same as passed into the setAddr() function,
     even if only 32-bits are used for this transaction.
     \return The 64-bit address value.
   */
   UInt64 getAddr      (void);
   //! Sets the requester ID for the transaction.
   /*!
     \param id The 16-bit value to be set.
   */
   void   setReqId     (UInt32 id);
   //! Gets the requester ID for the transaction.
   /*!
     \return The 16-bit requester ID.
   */
   UInt32 getReqId     (void);
   //! Sets the tag for the transaction.
   /*!
     \param tag The 8-bit value to be set.
   */
   void   setTag       (UInt32 tag);
   //! Gets the tag for the transaction.
   /*!
     \return The 8-bit tag value.
   */
   UInt32 getTag       (void);
   //! Sets the transaction register number and extended register number.
   /*!
     The numeric values are 10-bits wide, corresponding to the concatination
     of the 4-bit extended register number and 6-bit register number.
     \param reg The 10-bit value to be set.
   */
   void   setRegNum    (UInt32 reg);
   //! Gets the transaction register number and extended register number.
   /*!
     This value is 10-bits wide, corresponding to the concatination of
     the 4-bit extended register number and 6-bit register number.
     \return The 10-bits extended register number and register number.
   */
   UInt32 getRegNum    (void);
   //! Sets the completer ID for the transaction.
   /*!
     \param id The 16-bit value to be set.
   */
   void   setCompId    (UInt32 id);
   //! Gets the completer ID for the transaction.
   /*!
     \return The 16-bit completer ID value.
   */
   UInt32 getCompId    (void);
   //! Sets the message routing type for the transaction.
   /*!
     \param rout The 3-bit value to be set.
   */
   void   setMsgRout   (UInt32 rout);
   //! Gets the message routing type for the transaction.
   /*!
     \return The 3-bit message routing type.
   */
   UInt32 getMsgRout   (void);
   //! Sets the message code for the transaction.
   /*!
     \param code The 8-bit value to be set.
   */
   void   setMsgCode   (UInt32 code);
   //! Gets the message code for the transaction.
   /*!
     \return The 8-bit message code.
   */
   UInt32 getMsgCode   (void);
   //! Sets the lower address for the transaction.
   /*!
     \param addr The 7-bit value to be set.
   */
   void   setLowAddr   (UInt32 addr);
   //! Gets the lower address for the transaction.
   /*!
     \return The 7-bit lower address.
   */
   UInt32 getLowAddr   (void);
   //! Sets the byte count for the transaction.
   /*!
     The Byte Count field in the transaction, which equals the number
     of data bytes sent with a write or requested with a read.  In a
     Completion this may or may not equal the amount of data with the
     transaction.

     \param count The 12-bit value to be set.
   */
   void   setByteCount (UInt32 count);
   //! Gets the data byte count for the transaction.
   /*!
     \return The 12-bit byte count.
   */
   UInt32 getByteCount (void);
   //! Gets the double-word count for the 'Byte Count' value
   /*! 
     \deprecated
     This function has been replaced by getDataLength() and may be removed at any time.

     \return The number of DWords spanned by 'Byte Count' bytes.
     \sa getDataLength()
   */
   UInt32 getDWCount   (void);
   //! Sets the first double-word byte enable for the transaction.
   /*!
     \param be The 4-bit value for the FDWBE.
   */
   void   setFirstBE   (UInt32 be);
   //! Gets the first double-word byte enable value for the transaction.
   /*!
     \return The 4-bit value for the FDWBE.
   */
   UInt32 getFirstBE   (void);
   //! Sets the last double word byte enable for the transaction.
   /*!
     \param be The 4-bit value for the LDWBE.
   */
   void   setLastBE    (UInt32 be);
   //! Gets the last double-word byte enable value for the transaction.
   /*!
     \return The 4-bit value for the LDWBE.
   */
   UInt32 getLastBE    (void);
   //! Sets the completion status for the transaction.
   /*!
     \param status The 3-bit value to be set. 
   */
   void   setCplStatus (UInt32 status);
   //! Gets the completion status for the transaction.
   /*!
     \return The 3-bit completion status.
   */
   UInt32 getCplStatus (void);
   //! Sets the byte data for the transaction.
   //MJH - to be replaced, all data set as DWords, naturally aligned, written as 03_02_01_00.
   /*!
     \param index The index of the byte being set.
     \param data The value of the byte being set.
   */
   void   setDataByte  (UInt32 index, UInt8 data);
   //! Gets the data byte at the specified index for the transaction.
   //MJH - to be replaced, all data set as DWords, naturally aligned, written as 03_02_01_00.
   /*!
     If the byte \a index wasn't written then the return data will be 0x00.

     \param index The index of the byte being retrieved.
     \return The data byte at the specified index.
   */
   UInt8  getDataByte  (UInt32 index);
   //! Resizes the byte data buffer
   /*!
     Either extend or truncate the byte data buffer.  If \a start is greater
     than zero then all elements from zero to \a start will be removed.  If
     \a end is before the last element, then all after it will be removed,
     otherwise zero data will be appended to the byte data buffer.

     \param start The index of the first element to keep.
     \param end The index of the last element to keep or create.
   */
   void   resizeDataBytes  (UInt32 start, UInt32 end);
   //! Gets the number of bytes in the data queue
   /*!
     The length of the data queue is from byte 0 through the last byte written.  If only
     byte 11 is written, then the data queue length is 12 bytes.

     \return The number of bytes of data set for the tranaction
   */
   UInt32 getDataByteCount (void);
   //! Sets the data length value in double-words
   /*!
     \param length The 10-bit data length for the transaction
   */
   void   setDataLength (UInt32 length);
   //! Gets the data length in double-words
   /*!
     \return The 10-bit data length for the transaction
   */
   UInt32 getDataLength (void);

   //! Gets the transaction ID
   /*!
     The transaction ID is a concatination of the Requester ID and the Tag values.  On completion
     transactions this information links the completion to the original request.  For all other
     transactions the return value is undefined.
     \return The transaction ID of the original request on completions.
   */
   SInt32 getTransId   (void);

   // Ease of use functions
   //! Gets whether the transaction is a read
   /*!
     \retval true If the command is a memory, i/o, or config read
     \retval false If the command is not a read
   */
   bool   isRead       (void);
   //! Gets whether the transaction is a write
   /*!
     \retval true If the command is a memory, i/o, or config write
     \retval false If the command is not a write
   */
   bool   isWrite      (void);

 private:
   PCIExpressCommand *pimpl_;

   PCIECmd operator=(const PCIECmd&);                    // automatic assignment not defined, use 'copy' function
};


//! PCIEHandler class
/*!
  This class provides a way to easily control multiple PCIEXtor transactors
  in a single simulation.  It has the ability to control the calling of
  the transactors (instead of directly using a CarbonSimClock to call the
  transactors), and when CarbonSimMaster::interrupt() is called to halt
  the simulation.  It also provides a single function call to query whether
  all PCIEXtor transactors are currently idle.
*/
class PCIEHandler : public CarbonSimTransactor {
 public:
   //! Destructor.
   virtual ~PCIEHandler();

   //! Constructor.
   PCIEHandler();

   //! Bind the CarbonSimMaster to this object.
   /*!
     \param simMaster The CarbonSimMaster controlling the simulation.
     \retval true If the function was sucessful.
     \retval false If the function had an error.
   */
   bool bindSimMaster(CarbonSimMaster *simMaster); // bind the CarbonSimMaster
   //! Bind a PCIEXtor to this object.
   /*!
     \param xtor One of the PCIEXtor objects in the simulation.
     \retval true If the function was sucessful.
     \retval false If the function had an error.
   */
   bool bindTransactor(PCIEXtor *xtor);            // bind each PCIEXtor
   //! Query whether all PCIEXtor instances are idle.
   /*!
     \retval true If all instances are idle.
     \retval false If one or more instances are processing transations.
   */
   bool isDone(void);                              // are all transactors currently done
   //! Enable or disble this object from calling CarbonSimMaster::interrupt() when all PCIEXtor instances are idle.
   /*!
     \param tf The value TRUE will enable calling interrupt() while FALSE will disable it.
   */
   void setUseCarbonSimInterrupt(bool tf);         // Enable/Disable calling of CarbonSimMaster::interrupt() when isDone() becomes true, default is TRUE unless no CarbonSimMaster is given
   //! Query whether calling CarbonSimMaster::interrupt() is enabled or disabled.
   /*!
     \retval true If calling interrupt() is enabled.
     \retval false If calling interrupt() is disabled.
   */
   bool getUseCarbonSimInterrupt(void);            // Get the status of above
   //! Add a time to unconditionally call CarbonSimMaster::interrupt().
   /*!
     \retval true The interrupt time was added.
     \retval false The interrupt time was not added, possibly because that time has already passed.
   */
   bool addInterruptTime(CarbonTime);
   //! Enable or disble this object from driving the PCIEXtor instances when its called.  If disabled then the PCIEXtor instances will need to be driven directly by a CarbonSimClock object.
   /*!
     \param tf The value TRUE will enable driving the instances while FALSE will disable it.
   */
   void setDriveTransactors(bool tf);              // Call the transactors from here or allow CarbonSim to call them directly, default is TRUE
   //! Query whether driving the PCIEXtor instances is enabled or disabled.
   /*!
     \retval true If driving the instances is enabled.
     \retval false If driving the instances is disabled.
   */
   bool getDriveTransactors(void);                 // Get the status of above

#ifndef CARBON_EXTERNAL_DOC
   // CarbonSimTransactor items, not useful to users
   virtual void step (CarbonTime tick);
   virtual void idle (UInt64 cycles);
#endif // CARBON_EXTERNAL_DOC

 private:
   PCIExpressHandler *pimpl_;

   PCIEHandler(const PCIEHandler&);        // copy constructor not defined
};

//! PCIEXtor class
/*!
  This class provides the functional component of the Carbon PCI-Express
  transactor, based on the PCI-Express Base Specification, Rev 1.0a.
*/
class PCIEXtor : public CarbonSimTransactor {
 public:
   //! Destructor.
   virtual ~PCIEXtor();

   //! Supported interface types
   /*!
     The transactor currently supports two types of interfaces.  One takes 10-bit encoded and
     scrambled data as it should look just before going to the serializers, \a e10BitIntf.
     The other follows the Intel PIPE Specification interface, \a ePipeIntf.
   */
   typedef enum {
      e10BitIntf,  //!< 10-bit data interface
      ePipeIntf    //!< Phy Interface for PCI-Express (Intel PIPE interface)
   } PCIEIntfType;


   //! Reasons for callback function being called
   typedef enum {
      eReturnData,    //!< Received return data from a transaction
      eReceivedTrans  //!< Received transaction from DUT
   } PCIECallbackType;


   //! Signal type definitions for connecting CarbonSimNets
   /*!
     Not all signal types are available for all interface types.  The \a e10BitIntf
     interface uses the \a eClock, \a eRxData, \a eRxElecIdle, \a eTxData, and
     \a eTxElecIdle types.  The \a ePipeIntf interface uses the \a eClock, \a ePhyStatus,
     \a ePowerDown, \a eResetN, \a eRxData, \a eRxDataK, \a eRxElecIdle, \a eRxPolarity,
     \a eRxStatus, \a eRxValid, \a eTxCompliance, \a eTxData, \a eTxDataK, \a eTxDetectRx,
     and \a eTxElecIdle.

     All signal types, with the exception of \a eClock, \a ePhyStatus, \a ePowerDown,
     \a eResetN, and \a eTxDetectRx, are connected with one instance per lane of the link.
     Therefore if the link width is 4, there would be four signals of type \a eTxData
     connected, with the indecies 0 through 3.
   */
   typedef enum {
      eClock,        //!< Interface clock
      ePhyStatus,    //!< DUT input PHY status
      ePowerDown,    //!< DUT output power down state
      eResetN,       //!< DUT output active-low reset
      eRxData,       //!< DUT input receiver data
      eRxDataK,      //!< DUT input receiver symbol flag
      eRxElecIdle,   //!< DUT input receiver electrical idle
      eRxPolarity,   //!< DUT output receiver polarity inversion
      eRxStatus,     //!< DUT input receiver status
      eRxValid,      //!< DUT input receiver valid
      eTxCompliance, //!< DUT output transmitter compliance pattern
      eTxData,       //!< DUT output transmitter data
      eTxDataK,      //!< DUT output transmitter symbol flag
      eTxDetectRx,   //!< DUT output transmitter detects receiver or begin loopback
      eTxElecIdle    //!< DUT output transmitter electrical idle
   } PCIESignalType;

   //! Callback function to indicate that the transactor is done processing transactions.
   /*!
     This function will be called, if set, each time the transactor is called (rising edge of
     the signal connected with type \a eClock) when the transactor has no transactions to
     process, either on the transmitter or receiver.

     \param info Any information the user would like passed back to the callback function.
   */
   typedef void (PCIEDoneCBFn)(void *info);
   //! Callback function to indicate that the transactor has received a transaction from the DUT.
   /*!
     This function will be called each time the transactor receives a transaction (TLP packet)
     from the DUT.  The \a type will specify if the reason for calling is that read data is
     being returned or that a new transaction was received.

     \param pcie The calling transactor.
     \param type The reason for the function being called.
     \param info Any information the user would like passed back to the callback function.
   */
   typedef void (PCIETransCBFn)(PCIEXtor *pcie, PCIEXtor::PCIECallbackType type, void *info);


   //! Constructor.
   /*!
     \param intf       The interface type used for this transactor.
     \param link_width The number of lanes to be used.
     \param print_name The c-style string prepended to all transactor messages.
   */
   PCIEXtor(PCIEIntfType intf, UInt32 link_width, const char *print_name);

   //! Get c-style string for the PCIEXtor::PCIEIntfType value.
   /*!
     This function is provided to enhance printing messages.
     \param type The type to get the print string for.
     \return The c-style string for message printing.
   */
   static const char* getStrFromType (PCIEIntfType type);
   //! Get c-style string for the PCIEXtor::PCIECallbackType value.
   /*!
     This function is provided to enhance printing messages.
     \param type The type to get the print string for.
     \return The c-style string for message printing.
   */
   static const char* getStrFromType (PCIECallbackType type);
   //! Get c-style string for the PCIEXtor::PCIESignalType value.
   /*!
     This function is provided to enhance printing messages.
     \param type The type to get the print string for.
     \return The c-style string for message printing.
   */
   static const char* getStrFromType (PCIESignalType type);

   //! Set message printing level for transactor.
   /*!
     The available levels are:
     - 3+ : High, print copious amounts of data about every cycle.
     - 2  : Medium, print large amount of information during all transactions.
     - 1  : Low, print basic informational messages during simulations.
     - 0  : Off, print no message except for a few configuration error messages.

     The default value is '1' when the transactor is instanciated.

     \param level The print level to be used.
   */
   void   setPrintLevel      (UInt32 level);
   //! Get message printing level for transactor.
   /*!
     \retval 3 If the message printing is set to High.
     \retval 2 If the message printing is set to Medium.
     \retval 1 If the message printing is set to Low.
     \retval 0 If the message printing is set to Off.
   */
   UInt32 getPrintLevel      (void);
   void   setBusNum          (UInt32 num);	                 // Set PCI style Bus Number for config transactions
   UInt32 getBusNum          (void);                             // Get PCI style Bus Number for config transactions
   void   setDevNum          (UInt32 num);                       // Set PCI style Device Number for config transactions
   UInt32 getDevNum          (void);                             // Get PCI style Device Number for config transactions
   void   setFuncNum         (UInt32 num);                       // Set PCI style Function Number for config transactions
   UInt32 getFuncNum         (void);                             // Get PCI style Function Number for config transactions
   //! Sets whether data scrambling is enabled or disabled.
   /*!
     This option will effect both the transmitter and receiver functionality, ie. they must
     both or neither be sending scrambled data across the link.  This function should be called
     before the transactor starts training the link, and if it is called during the simulation
     then the transactor will need to retrain the link before it takes effect.

     \bug
     This function should be set before the simulation starts and not changed.  The
     transactor will immediately use this status and if changed during a simulation the DUT
     will most likely not be able to handle this change, since it is not allowed in the
     PCI-Express specification.

     \bug
     The only valid value is FALSE.  The scrambling functionality does not work properly and
     should be disabled.

     \param tf The value TRUE will enable data scrambling while FALSE will disable it.
   */
   void   setScrambleStatus  (bool tf);
   //! Gets whether data scrambling is enabled or disabled.
   /*!
     This function will return the latest value set with setScrambleStatus().
     \retval true If data scrambling is enabled.
     \retval false If data scrambling is disabled.
   */
   bool   getScrambleStatus  (void);
   //! Sets the maximum number of in-flight transactions
   /*!
     This option will set the maximum number of transactions from the transactor that can be
     in-flight at one time.  In-flight means that it is activly being transmitted, waiting for
     an acknowledgement, or waiting for a completion message.

     Once this limit is reached, all transactions will be held in the queue and idle will be
     transmitted on the bus until one or more of the in-flight transactions completes.

     The PCI-Express 1.0a specification states that the maximum number of in-flight transactions
     is 32.  If this function sets a value of 0 or more than 32, the transactor will respect the
     limit of 32 in the specification.  Any value between those two points will be used as is.

     \param max The maximum number of in-flight transactions.
   */
   void   setMaxTransInFlight (UInt32 max);
   //! Gets the maximum number of in-flight transactions
   /*!
     This option will return the maximum number of in-flight transactions that is allowed by
     the transactor.  This value is set by setMaxTransInFlight() and the default value is 32.

     \return The maximum number of in-flight transactions.
   */
   UInt32 getMaxTransInFlight (void);
   //! Start link training
   /*!
     Start training the link immediately if the link is currently in the electrical idle state.
     If the link is not in electrical idle then this function will have no effect.
     \retval true If the transactor will start link training.
     \retval false If the transactor will not start link training.
   */
   bool   startTraining      (void);
   //! Start link training
   /*!
     Start training the link after time \a tick if the link is currently in the electrical
     idle state.  If the link is not in electrical idle then this function will have no effect.
     The link will immediately transition from electrical idle to logical idle, stay in that
     state for time \a tick, and then start training.

     The value \a tick is in the same unit used for the CarbonSimClock objects.

     \param tick The length of time to spend in logical idle before training starts.
     \retval true If the transactor will start link training.
     \retval false If the transactor will not start link training.
   */
   bool   startTraining      (CarbonTime tick);
   //! Enable or disable link training bypass
   /*!
     Enabling the link training bypass will cause the transactor to go from an electrical
     idle to logical idle state with all lanes of the link trained.  The transactor will
     go into the flow control initialization state next.

     Disabling the link training bypass will cause the transactor to go through the full
     link training sequence when it is next necessary, upon transitioning either from the
     electrical idle state or to the recovery state.

     By default the training bypass is disabled.

     \param tf The value TRUE will bypass the link training while FALSE will disable the bypass.
   */
   void   setTrainingBypass  (bool tf);
   //! Get the current state of the training bypass feature.
   /*!
     Returns whether the transactor has the link training bypass enabled or disabled.

     \retval true If the link training is bypassed.
     \retval false If the link training is not bypassed.
   */
   bool   getTrainingBypass  (void);

   //! Sets the maximum payload size for transmitted transactions
   /*!
     Sets the Max_Payload_Size parameter in the PCI-Express Device
     Control Register (Offset 0x8).  See the PCI-Express Base
     Specification Rev 1.0a, Section 7.8.4 for a description of this
     configuration register.

     \param val The 3-bit value to be set.
   */
   void   setMaxPayloadSize(UInt32 val);
   //! Gets the maximum payload size for transmitted transactions
   /*!
     Gets the Max_Payload_Size parameter in the PCI-Express Device
     Control Register (Offset 0x8).  See the PCI-Express Base
     Specification Rev 1.0a, Section 7.8.4 for a description of this
     configuration register.

     \return The 3-bit parameter value.
   */
   UInt32 getMaxPayloadSize(void);

   //! Sets the callback function for received transactions and return data
   /*!
     This function must be set so the transactor can notify the calling program when it
     receives a transaction from the DUT.  The \a transCbInfo parameter is passed to the
     callback exactly as it was passed to this function.

     \param transCbFn The function to be called by the transactor.
     \param transCbInfo Any information the user would like passed back to the callback function.
   */
   void   setTransCallbackFn (PCIETransCBFn *transCbFn, void* transCbInfo);
   //! Returns the callback function pointer called for received transactions and return data
   /*!
     This function passes back the function pointer that was passed in as the first parameter
     to the setTransCallbackFn() function.

     \sa setTransCallbackFn(PCIETransCBFn *transCbFn, void* transCbInfo)
   */
   PCIETransCBFn* getTransCallbackFn (void);
   //! Returns the callback data pointer passed to the callback function for received transactions and return data
   /*!
     This function passes back the data pointer that was passed in as the second parameter
     to the setTransCallbackFn() function.

     \sa setTransCallbackFn(PCIETransCBFn *transCbFn, void* transCbInfo)
   */
   void*  getTransCallbackData (void);

   //! Gets a data return transaction in a PCIECmd object.
   /*!
     The user must pass in a pointer to an allocated PCIECmd object in which the transactor
     will populate the fields with the data from the received completion transaction.  If
     \a cmd is a null pointer that will be caught and an error message given.

     This command can be called as many times as necessary in order to get each of the stored
     PCIECmd objects.

     \param cmd A pointer to an allocated PCIECmd object.
     \retval true A valid transaction was placed into \a cmd.
     \retval false An error occurred or there are no new completion transactions to be read.
   */
   bool	  getReturnData      (PCIECmd *cmd);
   //! Gets the number of data return transactions which need to be processed.
   /*!
     Find the number of PCIECmd objects in the queue of data return transactions (completions).

     \retval length Number of transactions in queue
   */
   UInt32  getReturnDataQueueLength (void);
   //! Gets a received transaction in a PCIECmd object.
   /*!
     The user must pass in a pointer to an allocated PCIECmd object in which the transactor
     will populate the fields with the data from the received completion transaction.  If
     \a cmd is a null pointer that will be caught and an error message given.

     This command can be called as many times as necessary in order to get each of the stored
     PCIECmd objects.

     \param cmd A pointer to an allocated PCIECmd object.
     \retval true A valid transaction was placed into \a cmd.
     \retval false An error occurred or there are no new completion transactions to be read.
   */
   bool   getReceivedTrans   (PCIECmd *cmd);                     // Get PCIECmd object with received transaction information
   //! Gets the number of received transactions which need to be processed.
   /*!
     Find the number of PCIECmd objects in the queue of received transactions.

     \retval length Number of transactions in queue
   */
   UInt32  getReceivedTransQueueLength (void);

   //! Gets the transactor's link width
   /*!
     This function gets the link width, or number of lanes in the link, as specified when the
     transactor was instantiated.  This value can not change after the PCIEXtor object is
     created.

     \return The link width used by the transactor.
   */
   UInt32 getLinkWidth       (void);
   //! Gets the transactor's message print name.
   /*!
     This function gets the transactor's print name as specified when the transactor was
     instantiated.  This value can not change after the PCIEXtor object is created and is
     prepended to all messages printed from the transactor.

     \return The c-style string used in all printing messages.
   */
   char*  getPrintName       (void);
   //! Gets the transactor's interface type.
   /*!
     This function gets the transactor's interface type as specified when the transactor was
     instantiated.  This value can not change after the PCIEXtor object is created.

     \return The \a PCIEIntfType interface type used by this transactor instance.
   */
   PCIEIntfType getInterfaceType (void);

   //! Gets whether the transactor is currently finished processing transactions.
   /*!
     \deprecated
     This function has been replaced by isDone() and may be removed at any time.

     \retval true The transactor is done processing its transactions.
     \retval false The transactor has transactions to send out or is currently receiving
                   a new transaction.
     \sa isDone()
   */
   bool   getDoneForNow      (void);
   //! Sets the callback function for notifying that the transactor is finished.
   /*!
     This function may be set so the transactor can notify the calling program when it
     no longer has any transactions to process, either on the transmitter or receiver.
     The \a doneCbInfo parameter is passed to the callback exactly as it was passed to
     this function.

     This function will be called each time the transactor is called (rising edge of the
     signal connected with type \a eClock) as long as isDone() would return \a true.

     \param doneCbFn The function to be called by the transactor.
     \param doneCbInfo Any information the user would like passed back to the callback function.
   */
   void   setDoneCallbackFn  (PCIEDoneCBFn *doneCbFn, void* doneCbInfo);
   //! Returns the callback function pointer called for notification that the transactor is finished
   /*!
     This function passes back the function pointer that was passed in as the first parameter
     to the setDonCallbackFn() function.

     \sa setDoneCallbackFn(PCIEDoneCBFn *doneCbFn, void* doneCbInfo)
   */
   PCIEDoneCBFn* getDoneCallbackFn (void);
   //! Returns the callback data pointer passed to the callback function for notification that the transactor is finished
   /*!
     This function passes back the data pointer that was passed in as the second parameter
     to the setDoneCallbackFn() function.

     \sa setDoneCallbackFn(PCIEDoneCBFn *doneCbFn, void* doneCbInfo)
   */
   void*  getDoneCallbackData (void);

   //! Enqueue a new transaction
   /*!
     Information from the \a cmd object is used to create a new transaction, which is pushed
     onto the end of the queue.  The transation ID is returned upon success and is a unique
     identifier for this transaction.

     \param cmd The information for a new transaction.
     \retval -1 There was an error and no new transaction was added.
     \retval 0+ The new transaction was successfully enqueued and the transaction ID was returned.
   */
   SInt32 addTransaction     (PCIECmd *cmd);
   //! Binds a CarbonSimNet to the specified transactor signal
   /*!
     One of the transactor's signals will be bound to this object and remove the binding
     of any previous CarbonSimNets set to this signal.  If this function fails then the
     previous binding, if any, will remain intact.

     This function should be used to connect signals that are indexed per lane such that
     there is one of this signal type for each lane in the link, such as with the \a eRxData
     type.  Also, if the index is 0, then all of the non-indexed signals may be set with
     this function.

     \warning The CarbonSimNet object set with type \a eClock must be a net that is driven
     by the same CarbonSimClock that the transactor is bound to with
     CarbonSimClock::bindTransactor().  If this is not followed then the functionality is
     undefined.

     \param sigType The PCIESignalType this net represents.
     \param index The lane index for the specified PCIESignalType.
     \param net The CarbonSimNet to be connected.
     \retval true The signal was successfully set.
     \retval false There was an error setting the signal.

     \sa setSignal(CarbonSimClock *clock)
     \sa setSignal(PCIESignalType sigType, CarbonSimNet *net)
   */
   bool   setSignal          (PCIESignalType sigType, UInt32 index, CarbonSimNet *net);
   //! Binds a CarbonSimNet to the specified transactor signal
   /*!
     One of the transactor's signals will be bound to this object and remove the binding
     of any previous CarbonSimNets set to this signal.  If this function fails then the
     previous binding, if any, will remain intact.

     This function should be used to connect signals that are not indexed per lane such that
     there is only one of this signal type for the entire link, such as with the \a eClock
     type.  Also, if the link width is 1, then all of the indexed signals may be set with
     this function.

     \warning The CarbonSimNet object set with type \a eClock must be a net that is driven
     by the same CarbonSimClock that the transactor is bound to with
     CarbonSimClock::bindTransactor().  If this is not followed then the functionality is
     undefined.

     \param sigType The PCIESignalType this net represents.
     \param net The CarbonSimNet to be connected.
     \retval true The signal was successfully set.
     \retval false There was an error setting the signal.

     \sa setSignal(CarbonSimClock *clock)
     \sa setSignal(PCIESignalType sigType, UInt32 index, CarbonSimNet *net)
   */
   bool   setSignal          (PCIESignalType sigType, CarbonSimNet *net);
   //! Binds a CarbonSimClock to the transactor's clock signal
   /*!
     The transactors clock signal will be bound to this object and remove the binding
     of any previous signals set to the clock either with this function or the other
     overloaded functions with signal type \a eClock.  If this function fails then the
     previous binding, if any, will remain intact.

     \warning The CarbonSimClock object must be the same one that the transactor is
     bound to with CarbonSimClock::bindTransactor().  If this is not followed then the
     functionality is undefined.

     \param clock The CarbonSimClock to be used for the clock.
     \retval true The signal was successfully set.
     \retval false There was an error setting the signal.

     \sa setSignal(PCIESignalType sigType, CarbonSimNet *net)
     \sa setSignal(PCIESignalType sigType, UInt32 index, CarbonSimNet *net)
   */
   bool   setSignal          (CarbonSimClock *clock);            // Set CarbonSimClock for transactor's clock
   //!Sets the electrical idle nets as active high
   /*!
     This function determins whether the signals connected as \a eRxElecIdle and
     \a eTxElecIdle are active high or active low.  Both the transmitter and receiver
     will adhere to the calling of this function.  After this function is called, the
     specified signals will show that the receivers/transmitters are in the electrical
     idle state (powered down) if they have the value '1'.

     \note This is only valid for the \a e10BitIntf interface.  The \a ePipeIntf
     specifies that the signals be active high.
   */
   void   setElecIdleActiveHigh (void);
   //!Sets the electrical idle nets as active low
   /*!
     This function determins whether the signals connected as \a eRxElecIdle and
     \a eTxElecIdle are active high or active low.  Both the transmitter and receiver
     will adhere to the calling of this function.  After this function is called, the
     specified signals will show that the receivers/transmitters are in the electrical
     idle state (powered down) if they have the value '0'.

     \note This is only valid for the \a e10BitIntf interface.  The \a ePipeIntf
     specifies that the signals be active high.
   */
   void   setElecIdleActiveLow  (void);
   //! Gets whether the transactor fully configured and ready for use.
   /*!
     This function should be called after all of the necessary configuration
     functions have been called to determine if the transactor is ready to
     interact with the DUT.  If this function returns \c true then the transactor
     will function when its CarbonSimClock transitions.  If this function
     returns \c false then the transactor will not execute when its clock
     transitions and will print a warning message to the screen.

     To determine what caused this function to return \c false, call
     printConfiguration().

     \retval true The transactor is fully configured.
     \retval false The transactor has one or more necessary connections open.

     \sa printConfiguration()
   */
   bool   isConfigured       (void);
   //! Print out all configuration information.
   /*!
     This function can be called at any time to view the current configuration
     information.  A list of all DUT nets connected to transactor signals, the
     link width, etc., will be printed to STDOUT.

     For programability, the function isConfigured() is provided to return
     whether the transactor is fully configured or not.

     \sa isConfigured()
   */
   void   printConfiguration (void);
   //! Gets whether the transactor is currently finished processing transactions.
   /*!
     \retval true The transactor is done processing its transactions.
     \retval false The transactor has transactions to send out or is currently receiving
                   a new transaction.
   */
   bool   isDone             (void);                             // Is the transactor currently done doing work
   //! Determine if the link is in the active state.
   /*!
     This function will return whether the link has been trained and is ready for use
     or not.  A return value of \c true means that the link training state machine (LTSM)
     and data link layer state machine (DLLSM) have both completed training.

     \retval true The link is trained.
     \retval false The link has not been fully trained.

     \sa printLinkState()
   */
   bool   isLinkActive       (void);
   //! Print the current state of link training.
   /*!
     This function can be called at any time to view the current state of the link
     training while debugging.  The output will be printed to STDOUT for the current
     state of the link training state machine (LTSM) and data link layer state machine
     (DLLSM).

     For programability, the function isLinkActive() is provided to return whether the
     link is fully trained or not.

     \sa isLinkActive()
   */
   void   printLinkState     (void);

   //! Set the timescale for print messages.
   /*!
     This function relates the CarbonTime passed to the step() function to a
     user-specified CarbonTimescale.  For consistency this should match the
     CarbonTimescale value passed to the CarbonWaveID object if one exists. If this
     function is not called then the CarbonTime will be printed in the messages
     without any related scale.

     Since the PCI-Express 1.0a interface is controlled by a clock with a 4ns period,
     messages will be printed with the time scaled from the \a timescale specified
     here to \a e1ns.

     \param timescale The user-specified timescale to be used when printing the
     simulation time.
   */
   void   setTimescale       (CarbonTimescale timescale);

   //! Tell the transactor to run.
   /*!
     This function needs to be called when the pertinent clock changes value, at which
     time the transctor will process its stimulus and return any results.  If this
     transctor is used within a CarbonSim environment then this function will be called
     automatically when the transactor is bound to a CarbonSimClock.

     \param tick The current simulation time.
   */
   virtual void   step       (CarbonTime tick);
   //! Add an idle transaction.
   /*!
     Submit a logical idle transaction that will last \c cycles clock cycles.  This
     transaction will be appended to the transaction queue and can be used to space
     out transactions or keep the transactor running while waiting for the DUT (and 
     not have isDone() return true or related callback called).

     \param cycles The number of clock cycles to keep the transmitter in logical idle.
   */
   virtual void   idle       (UInt64 cycles);

 private:
   PCIExpressTransactor *pimpl_;

   PCIEXtor(const PCIEXtor&);                                    // copy constructor not defined
};

/*!
  @}
*/

#endif

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
