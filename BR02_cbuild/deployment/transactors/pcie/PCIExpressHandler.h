class CarbonSimMaster;
class PCIEXtor;

#include "carbon/carbon_shelltypes.h"
#include <vector>
#include <deque>

class PCIExpressHandler {
 public:
   PCIExpressHandler();
   ~PCIExpressHandler();

   bool bindSimMaster(CarbonSimMaster*);   // bind the CarbonSimMaster
   bool bindTransactor(PCIEXtor*);         // bind each PCIEXtor
   bool isDone(void);                      // are all transactors currently done
   void setUseCarbonSimInterrupt(bool);    // Enable/Disable calling of CarbonSimMaster::interrupt() when isDone() becomes true, default is TRUE unless no CarbonSimMaster is given
   bool getUseCarbonSimInterrupt(void);    // Get the status of above
   bool addInterruptTime(CarbonTime);      // Sets a time to call interrupt
   void setDriveTransactors(bool);         // Call the transactors from here or allow CarbonSim to call them directly, default is TRUE
   bool getDriveTransactors(void);         // Get the status of above

   // CarbonSimTransactor items, not useful to users
   void step (CarbonTime tick);
   void idle (UInt64 cycles);

 private:
   typedef std::vector<PCIEXtor *> PCIEXtorList;  // const access at end, linear access at front & middle, simplest sequence
   typedef std::deque<CarbonTime> CarbonTimeList; // const access at beginning & end, linear access at middle

   CarbonSimMaster *mSimMaster;   // pointer to current CarbonSimMaster
   PCIEXtorList mPcieList;        // list of pointers to PCIEXtor objects we'll handle
   CarbonTimeList mInterruptTime; // list of CarbonTime for when we will call mSimMaster->interrupt()
   bool mAreTheyDone;             // are all transactors currently idle?
   bool mUseCarbonSimInterrupt;   // are we going to call CarbonSimInterrupt when all are done?
   bool mDriveTransactors;        // are we going to call xtor->step() for each xtor?
};
