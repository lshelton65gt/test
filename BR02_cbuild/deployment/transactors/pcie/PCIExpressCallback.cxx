// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <iostream>
#include <iomanip>
#include "PCIExpressCallback.h"
#include "PCIExpressTransactor.h"

PCIExpressCallback::PCIExpressCallback(PCIExpressTransactor *inXtor, UInt32 *inBytes, 
				       UInt32 inLength)
{
    UInt32 i;
    xtor = inXtor;
    _length = inLength;
    //    bytes = new UInt32[length];

    for(i=0; i<_length; i++) {
      mBytes.push_back(static_cast<UInt8>(inBytes[i]));   // get a local copy of the data
    }

    printStr = xtor->getPrintName();
    printStr += "  ";

    if(xtor->getVerboseRxLow()) {
       if(_length < 4)   // need at least the header!
	  std::cout << printStr << "ERROR: transaction created with to few bytes." << std::endl;
    }

    if(xtor->getVerboseRxMed()) {
	std::cout << printStr << "Info: Received Transaction ( ";
	for(i=0; i<_length; i++)
	   std::cout << std::hex << std::setw(2) << std::setfill('0') << static_cast<unsigned>(mBytes[i]) << " ";
	std::cout << ")" << std::endl;
    }

    processHeader();

#if 0
    std::cout << " -- Got me some data for a " << std::hex << tType << std::endl
	      << "    inLength = " << std::dec << inLength << std::endl;
#endif

    switch(tType)
	{
	case PCIECmd::MRd32:
	case PCIECmd::MRd64:
	case PCIECmd::MRdLk32:
	case PCIECmd::MRdLk64:
	    processMemRead(); break;
	case PCIECmd::MWr32:
	case PCIECmd::MWr64:
	    processMemWrite(); break;
	case PCIECmd::IORd:
	    processIORead(); break;
	case PCIECmd::IOWr:
	    processIOWrite(); break;
	case PCIECmd::CfgRd0:
	case PCIECmd::CfgRd1:
	    processConfigRead(); break;
	case PCIECmd::CfgWr0:
	case PCIECmd::CfgWr1:
	    processConfigWrite(); break;
	case PCIECmd::Msg:
	case PCIECmd::MsgD:
	    processMessage(); break;
	case PCIECmd::Cpl:
	case PCIECmd::CplD:
	case PCIECmd::CplLk:
	case PCIECmd::CplDLk:
	    processCompletion(); break;
	default:
	    return; // done, dont' know what to do with this, should have printed an error message already
	}
    return;
}

void PCIExpressCallback::processHeader(void)
{
    // get feild information from first four bytes
    hdrFmt    = (mBytes[0] >> 5) & 0x03;               // byte0[6:5]
    hdrType   = mBytes[0] & 0x1F;                      // byte0[4:0]
    hdrTC     = (mBytes[1] >> 4) & 0x07;               // byte1[6:4]
    hdrTD     = (mBytes[2] & 0x80) && 0x1;             // byte2[7]
    hdrEP     = (mBytes[2] & 0x40) && 0x1;             // byte2[6]
    hdrAttr   = (mBytes[2] >> 4) & 0x03;               // byte2[5:4]
    hdrLength = ((mBytes[2] & 0x3) << 8) | mBytes[3];   // {byte2[1:0],byte3[7:0]}

    if(hdrEP)  // packet poisoned
	return;

    // tType = getTLPType((hdrFmt << 5) + hdrType);
    tType = getTLPType(mBytes[0] & 0x7F);

    if(xtor->getVerboseRxHigh())
	std::cout << "     tType=" << std::hex << tType << ", TC=" << hdrTC << ", TD=" << hdrTD << ", EP=" << hdrEP << ", Attr=" << hdrAttr << ", length=" << hdrLength << "(" << std::dec << hdrLength << ")" << std::endl;

    return;
}

PCIECmd::PCIECmdType PCIExpressCallback::getTLPType(UInt32 x)
{
    PCIECmd::PCIECmdType y;
    switch(x)
	{
	case 0x00:
	    y= PCIECmd::MRd32; break;
	case 0x20:
	    y= PCIECmd::MRd64; break;
	case 0x01:
	    y= PCIECmd::MRdLk32; break;
	case 0x21:
	    y= PCIECmd::MRdLk64; break;
	case 0x40:
	    y= PCIECmd::MWr32; break;
	case 0x60:
	    y= PCIECmd::MWr64; break;
	case 0x02:
	    y= PCIECmd::IORd; break;
	case 0x42:
	    y= PCIECmd::IOWr; break;
	case 0x04:
	    y= PCIECmd::CfgRd0; break;
	case 0x44:
	    y= PCIECmd::CfgWr0; break;
	case 0x05:
	    y= PCIECmd::CfgRd1; break;
	case 0x45:
	    y= PCIECmd::CfgWr1; break;
	case 0x30: case 0x31: case 0x32: case 0x33: case 0x34: case 0x35: case 0x36: case 0x37:
	    y= PCIECmd::Msg; break;
	case 0x70: case 0x71: case 0x72: case 0x73: case 0x74: case 0x75: case 0x76: case 0x77:
	    y= PCIECmd::MsgD; break;
	case 0x0A:
	    y= PCIECmd::Cpl; break;
	case 0x4A:
	    y= PCIECmd::CplD; break;
	case 0x0B:
	    y= PCIECmd::CplLk; break;
	case 0x4B:
	    y= PCIECmd::CplDLk; break;
	default:
	    if(xtor->getVerboseRxLow())
		std::cout << printStr << "ERROR: Bad TLP Type given." << std::endl;
	    y= PCIECmd::Msg;  // don't know what else to do with it...
	    break;
	}
    return y;
}

void PCIExpressCallback::processMemRead(void) {
    bool is32bit;
    UInt32 reqID;   // requester ID
    UInt32 tag;     // tag
    UInt32 fdwbe;   // first DW byte enable
    UInt32 ldwbe;   // last DW byte enable
    UInt32 addrHi;
    UInt32 addrLo;

#if 0
    std::cout << "MEMRD size: " << std::dec <<  (mBytes[3] | ((mBytes[2] & 0x3) << 8)) 
	      << " " << hdrLength << std::endl;
#endif

    if(hdrFmt == 0x00)
	is32bit = true;
    else
	is32bit = false;

    reqID = (mBytes[4] << 8) + mBytes[5];
    tag   = mBytes[6];
    ldwbe = (mBytes[7] >> 4) & 0x0F;
    fdwbe = mBytes[7] & 0x0F;
    if(is32bit) {
	addrHi = 0x0;
	addrLo = (mBytes[8] << 24) + (mBytes[9] << 16) + (mBytes[10] << 8) + mBytes[11];
    } else {
	addrHi = (mBytes[8]  << 24) + (mBytes[9]  << 16) + (mBytes[10] << 8) + mBytes[11];
	addrLo = (mBytes[12] << 24) + (mBytes[13] << 16) + (mBytes[14] << 8) + mBytes[15]; 
    }

    UInt64 addr = (UInt64(addrHi) << 32) | addrLo;

    PCIECmd data;
    data.setCmdByName(tType);
    data.setAddr(addr);
    data.setByteCount(hdrLength * 4);
    data.setDataLength(hdrLength);
    data.setFirstBE(fdwbe);
    data.setLastBE(ldwbe);
    data.setReqId(reqID);
    data.setTag(tag);

    xtor->callTransCallbackFn(PCIEXtor::eReceivedTrans, &data);
}

void PCIExpressCallback::processMemWrite(void) {
    bool is32bit;
    UInt32 reqID;   // requester ID
    UInt32 tag;     // tag
    UInt32 fdwbe;   // first DW byte enable
    UInt32 ldwbe;   // last DW byte enable
    UInt32 addrHi;
    UInt32 addrLo;
    UInt32 headerSize;
    
#if 0
    std::cout << "MEMWRITE" << std::endl;
#endif

    if(hdrFmt == 0x02) {
	is32bit = true;
	headerSize = 12; // 12-byte header
    } else {
	is32bit = false;
	headerSize = 16; // 16-byte header
    }

    reqID = (mBytes[4] << 8) + mBytes[5];
    tag   = mBytes[6];
    ldwbe = (mBytes[7] >> 4) & 0x0F;
    fdwbe = mBytes[7] & 0x0F;
    if(is32bit) {
	addrHi = 0x0;
	addrLo = (mBytes[8] << 24) + (mBytes[9] << 16) + (mBytes[10] << 8) + mBytes[11];
    } else {
	addrHi = (mBytes[8]  << 24) + (mBytes[9]  << 16) + (mBytes[10] << 8) + mBytes[11];
	addrLo = (mBytes[12] << 24) + (mBytes[13] << 16) + (mBytes[14] << 8) + mBytes[15]; 
    }

    UInt64 addr = (UInt64(addrHi) << 32) | addrLo;

    PCIECmd data;
    data.setCmdByName(tType);
    data.setAddr(addr);
    data.setByteCount(hdrLength * 4);
    data.setDataLength(hdrLength);
    data.setFirstBE(fdwbe);
    data.setLastBE(ldwbe);
    data.setReqId(reqID);
    data.setTag(tag);

    /*
    ** There are hdrLength * 4 bytes.  First data byte starts at index 12 on a memory write
    */
    const int firstDataByte = 12;
    for (unsigned int j = 0; j < hdrLength * 4; j++) {
       data.setDataByte(j, mBytes.at(firstDataByte + j) );
    }

    xtor->callTransCallbackFn(PCIEXtor::eReceivedTrans, &data);

    // Memory Writes are Posted, therefore no completions should be generated

    if(xtor->getVerboseRxMed()) {
	std::cout << printStr << "     tType=MWr" << ((is32bit)?"32":"64") << ", TrafficClass=" << std::hex << hdrTC << ", TlpDigest=" << hdrTD << ", ErrPoisoned=" << hdrEP << ", Attr=" << hdrAttr << ", Length=" << hdrLength << std::endl;
	std::cout << printStr << "       Req_ID=" << reqID << ", Tag=" << tag << ", Last_DW_Byte_Ena=" << ldwbe << ", First_DW_Byte_Ena=" << fdwbe << std::endl;
	std::cout << printStr << "       AddressHi=" << addrHi << ", AddressLo=" << addrLo << std::endl;
    }
}

void PCIExpressCallback::processIORead(void) {
   UInt64 addr;
   UInt32 reqID;   // requester ID
   UInt32 tag;     // tag
   UInt32 addrLo;
   UInt32 fdwbe;
   UInt32 ldwbe;

   addr = (mBytes[8] << 24) | (mBytes[9] << 16) | (mBytes[10] << 8) | (mBytes[11]);
   reqID = (mBytes[4] << 8) + mBytes[5];
   tag   = mBytes[6];
   addrLo = (mBytes[8] << 24) + (mBytes[9] << 16) + (mBytes[10] << 8) + mBytes[11];
   fdwbe = mBytes[7] & 0xF;
   ldwbe = (mBytes[7] >> 4) & 0xF;

   PCIECmd data;
   data.setCmdByName(tType);
   data.setAddr(addr);
   data.setReqId(reqID);
   data.setTag(tag);
   data.setLowAddr(addrLo);
   data.setByteCount(4);
   data.setDataLength(1);
   data.setFirstBE(fdwbe);
   data.setLastBE(ldwbe);

   xtor->callTransCallbackFn(PCIEXtor::eReceivedTrans, &data);
}

void PCIExpressCallback::processIOWrite(void) {
    UInt32 reqID;   // requester ID
    UInt32 tag;     // tag
    UInt32 fdwbe;   // first DW byte enable
    UInt32 addrLo;

    reqID = (mBytes[4] << 8) + mBytes[5];
    tag   = mBytes[6];
    fdwbe = mBytes[7] & 0x0F;
    addrLo = (mBytes[8] << 24) + (mBytes[9] << 16) + (mBytes[10] << 8) + mBytes[11];

    UInt64 addr = addrLo;
    /*
    ** IO Writes MUST have a length of 4? 
    ** Last DW BE must be 0 (PCIE Spec Fig 2-15)
    */
    PCIECmd data;
    data.setCmdByName(tType);
    data.setAddr(addr);
    data.setByteCount(4);
    data.setDataLength(1);
    data.setFirstBE(fdwbe);
    data.setLastBE(0);
    data.setReqId(reqID);
    data.setTag(tag);


    /*
    ** There are hdrLength * 4 bytes.  First data byte starts at index 12 on a memory write
    */
    const int firstDataByte = 12;
    for (unsigned int j = 0; j < 4; j++) {
       data.setDataByte(j, mBytes.at(firstDataByte + j) );
    }
    
    xtor->callTransCallbackFn(PCIEXtor::eReceivedTrans, &data);


//del:     // MIKE: Do we want to auto complete writes?
//del:     // MIKE: IOWrites need a completion, but the user may want to return error status
//del:     PCIECmd *cmd = new PCIECmd();
//del:     cmd->setCmdByName(PCIECmd::Cpl);
//del:     cmd->setAddr(addrLo);
//del:     cmd->setByteCount(hdrLength * 4);
//del:     cmd->setDataLength(0);
//del:     cmd->setTag(tag);
//del: 
//del:     xtor->addTransaction(cmd);
//del:     delete cmd;

    if(xtor->getVerboseRxMed()) {
	std::cout << printStr << "     tType=IOWr, TrafficClass=" << std::hex << hdrTC << ", TlpDigest=" << hdrTD << ", ErrPoisoned=" << hdrEP << ", Attr=" << hdrAttr << ", Length=" << hdrLength << std::endl;
	std::cout << printStr << "       Req_ID=" << reqID << ", Tag=" << tag << ", First_DW_Byte_Ena=" << fdwbe << std::endl;
	std::cout << printStr << "       AddressLo=" << addrLo << std::endl;
    }
}

void PCIExpressCallback::processConfigRead(void) {

   // UInt64 junk = (mBytes[8] << 20) | (mBytes[9] << 12) | ((mBytes[10] & 0xf) << 8) | (mBytes[11]);
   // std::cout << printStr << "Received a cfgrd to " << std::hex << junk << std::endl;
   if (hdrLength != 1) {
      std::cout << printStr << "ERROR: Received config read with a length not equal to 1 byte" << std::endl;
      hdrLength = 1;
   }

   // Config reads are always 1 DW / 4 BYTE.
   PCIECmd data;
   data.setCmdByName(tType);
   data.setByteCount(4);
   data.setDataLength(1);
   data.setFirstBE(mBytes[7] & 0xF);
   data.setLastBE(0);
   data.setReqId((mBytes[4] << 8) | mBytes[5]);   // {bus_num[7:0], dev_num[4:0], func_num[2:0]} of requester
   data.setTag(mBytes[6]);
   data.setCompId((mBytes[8] << 8) | mBytes[9]);  // {bus_num[7:0], dev_num[4:0], func_num[2:0]} of target
   data.setRegNum(((mBytes[10] & 0xF) << 6) | ((mBytes[11] >> 2) & 0x3F));   // {ext_reg_num[3:0], reg_num[6:0]}

   xtor->callTransCallbackFn(PCIEXtor::eReceivedTrans, &data);

}

void PCIExpressCallback::processConfigWrite(void) {
   UInt32 reqID, tag, compID;
   reqID = (mBytes[4] << 8) | mBytes[5];
   tag = mBytes[6];
   compID = (mBytes[8] << 8) | mBytes[9];

   UInt64 addr = (mBytes[8] << 20) | (mBytes[9] << 12) | ((mBytes[10] & 0xf) << 8) | (mBytes[11]);

   PCIECmd data;
   data.setCmdByName(tType);
   data.setAddr(addr);
   data.setByteCount(4);
   data.setDataLength(1);
   data.setFirstBE(mBytes[7] & 0xF);
   data.setLastBE(0);
   data.setReqId(reqID);
   data.setTag(tag);

   const int firstDataByte = 12;
   for (unsigned int j = 0; j < 4; j++) {
      data.setDataByte(j, mBytes.at(firstDataByte + j) );
   }

#if 0
  std::cout << std::hex << " byte data: " << ' ' << UInt32(mBytes[12]) << ' ' << UInt32(mBytes[13]) << ' ' 
	    << UInt32(mBytes[14]) << ' ' << UInt32(mBytes[15]) << std::endl;
#endif

  xtor->callTransCallbackFn(PCIEXtor::eReceivedTrans, &data);

//del:   /*
//del:   ** Put a completion in the transactor
//del:   */   
//del: 
//del:   // MIKE: Do we want to auto complete writes?
//del:   // MIKE: CfgWrites need a completion, but the user may want to return error status
//del:   PCIECmd *cmd = new PCIECmd();
//del:   cmd->setCmdByName(PCIECmd::Cpl);
//del:   cmd->setAddr(data.getAddr() & 0xFFFFFFFF);
//del:   cmd->setReqId(reqID);
//del:   cmd->setTag(tag);
//del:   cmd->setCompId(compID);
//del: 
//del:   xtor->addTransaction(cmd);
//del:   delete cmd;
}

void PCIExpressCallback::processMessage(void) {
    UInt32 reqID;   // requester ID
    UInt32 tag;     // tag
    UInt32 msgCode; // message code
    bool error = false;  // did we see an error?

    // first 4 bytes were the header
    reqID = (mBytes[4] << 8) + mBytes[5];
    tag = mBytes[6];
    msgCode = mBytes[7];

    switch(msgCode)
	{
	case 0x00:  // unlock (for lock transactions)
	    if(xtor->getVerboseRxMed())
		std::cout << printStr << "Info: Received message 'Unlock'." << std::endl;
	    break;
	case 0x50:  // set slot power limit
	    if(xtor->getVerboseRxMed())
		std::cout << printStr << "Info: Received message 'Set Slot Power Limit'." << std::endl;
	    break;
	default:
	    // Assert_INTx and Deassert_INTx are only allowed by end devices.  This receiver
	    //   should not see these messages from an IO chip.
	    if(xtor->getVerboseRxLow())
		std::cout << printStr << "ERROR: Message type 0x" << std::hex << msgCode << " not currently supported." << std::endl;
	    error = true;
	    break;
	}

//DEL:    if(not error) {
//DEL:	createCompletion();
//DEL:    }

    return;
}

void PCIExpressCallback::processCompletion(void) {
   UInt16 cplID   = (mBytes[4] << 8) | (mBytes[5]);
   UInt16 reqID   = (mBytes[8] << 8) | (mBytes[9]);
   UInt8  tag     = (mBytes[10]);
   UInt8  lowAddr = (mBytes[11] & 0x7F);
   UInt8 cplStat = ((mBytes[6] & 0xE0) >> 5);
   UInt32 numBytes = ((mBytes[6] & 0x0F) << 8) | (mBytes[7]);
   UInt32 transID;

   transID = UInt32(reqID << 8) | UInt32(tag);

   switch (cplStat)
      {
      case 0x0: // Successful Completion
	 if(xtor->getVerboseRxMed())
	    std::cout << printStr << "Info: Received Completion Type:  Successful Completion  (DW Length = 0x" << std::hex << hdrLength << ")" << std::endl;
	 break;
      case 0x1: // Unsupported Request
	 std::cout << printStr << "ERROR: Received Completion Type:  Unsupported Request" << std::endl;
	 break;
      case 0x2: // Configuration Request Retry Status
	 std::cout << printStr << "ERROR: Received Completion Type:  Config Request Retry Status" << std::endl;
	 break;
      case 0x4: // Completer Abort
	 std::cout << printStr << "ERROR: Received Completion Type:  Completer Abort" << std::endl;
	 break;
      default:
	 std::cout << printStr << "ERROR: Received Completion Type:  <Unknown Type>  (0x" << std::hex << cplStat << ")" << std::endl;
	 break;
      }

   if(xtor->getVerboseRxMed()) {
      std::cout << printStr << std::hex << "Info: Completion from DUT: byte data: ";
      if(hdrLength == 0)
	 std::cout << "(none)";
      else
	 for(UInt32 i=0; i < (hdrLength * 4); i++)
	    std::cout << std::hex << std::setw(2) << std::setfill('0') << static_cast<unsigned>(mBytes.at(12+i)) << " ";
      std::cout << std::endl;
   }

   // MIKE: Must check that these values are what I spec'd
   PCIECmd data;
   data.setCmdByName(tType);
   // data.setAddr(addr);
   data.setCplStatus(cplStat);
   data.setCompId(cplID);
   data.setReqId(reqID);
   data.setTag(tag);
   data.setLowAddr(lowAddr);
   data.setByteCount(numBytes);
   data.setDataLength(hdrLength);
   for (UInt32 j=0; j < (hdrLength * 4); j++) {
      data.setDataByte(j, mBytes.at(12 + j) );
   }

//c std::cout << "MIKE: rx cpl for transID 0x" << std::hex << ((UInt32)transID) << " (cplID=0x" << ((UInt32)cplID) << ", reqID=0x" << ((UInt32)reqID) << ", tag=0x" << ((UInt32)tag) << ", bytecount=0x" << numBytes << ") (mBytes[10]=0x" << ((UInt32)mBytes[10]) << ")" << std::endl;
//c std::cout << "MIKE: Transaction bytes: ";
//c for (UInt32 k=0; k < mBytes.size(); k++) {
//c    std::cout << std::hex << ((UInt32)mBytes[k]) << " ";
//c }
//c std::cout << std::endl;

   xtor->callTransCallbackFn(PCIEXtor::eReturnData, &data);

   return;
}

//DEL: void PCIExpressCallback::createCompletion(void) {
//DEL:     // PCIExpressCommand *cmd;
//DEL:     // UInt32 *data = NULL;
//DEL: 
//DEL:     switch(tType)
//DEL: 	{
//DEL: 	case PCIECmd::Msg:
//DEL: 	case PCIECmd::MsgD:
//DEL: 	    //                     tlp_type addrLo data d_len
//DEL: 	    // cmd = new PCIExpressCommand(Cpl, 0x00, data, 0x00);
//DEL: 	    // xtor->addTransaction(cmd);
//DEL: 	    break;
//DEL: 	default:
//DEL: 	    break;  // don't know what to do, so we'll do nothing
//DEL: 	}
//DEL: }


PCIExpressCallback::~PCIExpressCallback(void)
{
   mBytes.clear();
}


void PCIExpressCallback::onCompletion(CompletionFunc * func, void * user_arg)
{
   if (func == 0) {
      std::cout << printStr << "ERROR: onCompletion() called with a NULL function pointer" << std::endl;
   } else {
      mCompletionFunc = func;
      mCompletionFuncArg = user_arg;
   }
}


void PCIExpressCallback::doCompletion(void)
{
    UInt32 a;
    a = 0;
    if (mCompletionFunc)
    {
	(mCompletionFunc)(a,NULL,mCompletionFuncArg);
    }
}

int PCIExpressCallback::calcByteCount(UInt32 length, unsigned char lastBE, unsigned firstBE) {
  /*
  ** There's probably a more algorithmic approach to this...
  ** This is from table 2-21 (p86) of the PCIE spec
  */

  if (lastBE == 0) {
     if (length > 1) {
	std::cout << printStr << "ERROR: Found transaction to have a length > 1 and the last DW byte enable is 0.  Changing the length from " << std::dec << length << " to 1." << std::endl;
	length = 1;
     }

    switch(firstBE) {
    case 0x0:
    case 0x8:
    case 0x4:
    case 0x2:
    case 0x1:
      return 1;
    case 0xC:
    case 0x6:
    case 0x3:
      return 2;
    case 0xE:
    case 0xA:
    case 0x5:
    case 0x7:
      return 3;
    case 0x9:
    case 0xB:
    case 0xD:
    case 0xF:
      return 4;
    default:
       std::cout << printStr << "ERROR: Found transaction to have an invalid first DW byte enable (0x" << std::hex << firstBE << ")" << std::endl;
       return 4;
    }
  }

  int adjustment = 0;

  if (firstBE & 0x1) {
    adjustment -= 0;
  } else if (firstBE & 0x2) {
    adjustment -= 1;
  } else if (firstBE & 0x4) {
    adjustment -= 2;
  } else if (firstBE & 0x8) {
    adjustment -= 3;
  }

  if (lastBE & 0x8) {
    adjustment -= 0;
  } else if (lastBE & 0x4) {
    adjustment -= 1;
  } else if (lastBE & 0x2) {
    adjustment -= 2;
  } else if (lastBE & 0x1) {
    adjustment -= 3;
  }

  return (length * 4) + adjustment;

}


/*
** This routine adjusts the address bits of to point to the 
** starting byte based on the byte enables for the transaction
** This only needs to happen for memeory read requests.
** 
** See table 2-22 in the PCIE spec
*/
UInt32 PCIExpressCallback::fixAddrForBE(UInt32 addr, unsigned char be)
{
   UInt32 retAddr = addr & ~UInt32(0x3);

   if (be == 0 or (be & 0x1)) {
      retAddr |= 0;
   } else if (be & 0x2) {
      retAddr |= 1;
   } else if (be & 0x4) {
      retAddr |= 2;
   } else if (be & 0x8) {
      retAddr |= 3;
   } else {
      assert("Internal PCIExpress Transactor ERROR" == 0);
   }

   return retAddr & 0x7F;
}

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
