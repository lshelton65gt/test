// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#ifndef __PCIEXPRESSCOMMAND_H_
#define __PCIEXPRESSCOMMAND_H_

#include "PCIExpressTransaction.h"
#include <vector>

class PCIExpressCommand
{
public:
   //! Blank Constructor
   PCIExpressCommand(void);
   //! Copy Constructor
   PCIExpressCommand(const PCIExpressCommand&);

   void   copy         (const PCIExpressCommand& in);    // copy function (minus the constructor)

   void   setCmd       (UInt32 cmd);                     // Set 7-bit command {format[1:0],type[4:0]}
   UInt32 getCmd       (void);                           // Get 7-bit command {format[1:0],type[4:0]}
   void   setCmdByName (PCIECmd::PCIECmdType cmd);       // Set command by name (typedef)
   PCIECmd::PCIECmdType getCmdByName (void);             // Get command by name (typedef)
   void   setAddr      (UInt64 addr);                    // Set 64-bit address (only lower-32bits used for 32-bit transactions)
   UInt64 getAddr      (void);                           // Get 64-bit address (only lower-32bits used for 32-bit transactions)
   void   setReqId     (UInt32 id);                      // Set 16-bit requestor ID field
   UInt32 getReqId     (void);                           // Get 16-bit requestor ID field
   void   setTag       (UInt32 Tag);                     // Set 8-bit tag field
   UInt32 getTag       (void);                           // Get 8-bit tag field
   void   setRegNum    (UInt32 reg);                     // Set 10-bit register number and extended register number  {ext_reg[3:0], reg_num[5:0]}
   UInt32 getRegNum    (void);                           // Get 10-bit register number and extended register number  {ext_reg[3:0], reg_num[5:0]}
   void   setCompId    (UInt32 id);                      // Set 16-bit completer ID field
   UInt32 getCompId    (void);                           // Get 16-bit completer ID field
   void   setMsgRout   (UInt32 rout);                    // Set 3-bit message routing type
   UInt32 getMsgRout   (void);                           // Get 3-bit message routing type
   void   setMsgCode   (UInt32 code);                    // Set 8-bit message code
   UInt32 getMsgCode   (void);                           // Get 8-bit message code
   void   setLowAddr   (UInt32 addr);                    // Set 7-bit lower byte address for starting byte of completion
   UInt32 getLowAddr   (void);                           // Get 7-bit lower byte address for starting byte of completion
   void   setByteCount (UInt32 count);                   // Set byte count (may not equal amount of data in completion transaction)
   UInt32 getByteCount (void);                           // Get byte count (may not equal amount of data in completion transaction)
   UInt32 getDWCount   (void);                           // DEPRECATED: Get total number of double-words (DW) the data effects (does NOT take into account the address byte offset)
   void   setFirstBE   (UInt4 be);                       // Set 4-bit first DW byte enables
   UInt4  getFirstBE   (void);                           // Get 4-bit first DW byte enables
   void   setLastBE    (UInt4 be);                       // Set 4-bit last DW byte enables
   UInt4  getLastBE    (void);                           // Get 4-bit last DW byte enables
   void   setCplStatus (UInt32 status);                  // Set 3-bit status for a completion transaction
   UInt32 getCplStatus (void);                           // Get 3-bit status for a completion transaction
   void   setDataByte  (UInt32 index, UInt8 data);       // Set data byte at index 'index' [ 0 <= index < getByteCount() ]
   UInt8  getDataByte  (UInt32 index);                   // Get data byte at index 'index' [ 0 <= index < getByteCount() ]
   void   resizeDataBytes  (UInt32 start, UInt32 end);   // Resize the data byte vector, either extending or truncating in the process
   UInt32 getDataByteCount (void);                       // Get number of bytes of data present
   void   setDataLength    (UInt32 len);                 // Set number of DWs of data present (data length)
   UInt32 getDataLength    (void);                       // Get number of DWs of data present (data length)
   SInt32 getTransId   (void);                           // Get transaction ID for original transaction on completion transaction

   bool   isRead       (void);                           // is this any type of read
   bool   isWrite      (void);                           // is this any type of write

   //! allow copying of all data into transaction
   void assignData(std::vector<UInt8>::iterator begin, 
   		   std::vector<UInt8>::iterator end) {
      mData.assign(begin, end);
   }

   void pushData(UInt8 d) {mData.push_back(d);}


private:
   // WARNING WARNING WARNING WARNING WARNING WARNING WARNING
   //  Any new data elements must be added to the copy()
   //  function or that data will be lost *often* in the
   //  transactor.
   // WARNING WARNING WARNING WARNING WARNING WARNING WARNING

   PCIECmd::PCIECmdType type;
   UInt32 addrHi;           // upper half for 64-bit address
   UInt32 addrLo;           // 32-bit address or lower half for 64-bit address
   UInt32 reqID;            // 16-bit requester ID
   UInt32 tag;              //  8-bit tag
   UInt32 regNum;           // 10-bit register number & extended register number {ext[3:0],reg[6:0]}
   UInt32 compID;	    // 16-bit completer ID
   UInt32 msgRout;          //  3-bit msg routing
   UInt32 msgCode;          //  8-bit msg code
   UInt3  compStat;         //  3-bit completion status
   UInt32 byteCount;        // 12-bit byte count
   UInt32 lowAddr;          // lower address [7-bits in Cpl/CplD]
   UInt4 firstDWBE;         // First Double Word Byte Enable
   UInt4 lastDWBE;          // Last Double Word Byte Enable
   UInt12 seqNum;           // sequence number associated with transaction for return data or received transactions
   UInt32 length;           // 10-bit length field (num data DWs)

   std::vector<UInt8> mData;

};

#endif // __PCIEXPRESSCOMMAND_H_

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
