// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __PCIEXPRESSCALLBACK_H_
#define __PCIEXPRESSCALLBACK_H_


#include <string>
#include <vector>
// #include "carbonsim/CarbonSimTransaction.h"
// #include "carbonsim/CarbonSimTransactor.h"
#include "PCIExpressCommon.h"
#include "PCIExpressCommand.h"

#if 0
#define PCIE_DLLP_CRC_KEY  0x100B
#define PCIE_DLLP_CRC_SEED 0xFFFF
#define PCIE_DLLP_LCRC_KEY  0x04C11DB7
#define PCIE_DLLP_LCRC_SEED 0xFFFFFFFF
#endif

//! PCIExpressCallback class
/*!
  A Class that represents the callback transactions from the PCIE
  to the testbench/outside-world.
*/
class PCIExpressCallback
{
 public:
   //! Public Constructor by bytes
   PCIExpressCallback(PCIExpressTransactor *xtor, UInt32 *inBytes, UInt32 length);
   //! Destructor.
   ~PCIExpressCallback(void);

   typedef void (CompletionFunc)(UInt32 address, UInt32 ** data, void * user_arg);
   //! Is the transaction complete?
   // bool isDone(void) { return mDone; }
   //! Take this action when the transaction is completed.
   void onCompletion(CompletionFunc * func, void * user_arg);
   //! Take any actions needed at the end of the transaction.
   void doCompletion(void);

 protected:
   CompletionFunc * mCompletionFunc;
   void * mCompletionFuncArg;  

 private:
   PCIExpressTransactor *xtor;
   std::vector<UInt8> mBytes;

   UInt32 _length;   // total length of transaction
   std::string printStr; // string to use in all cout calls

   // Header information
   UInt32 hdrFmt;    // format
   UInt32 hdrType;   // type
   UInt32 hdrTC;     // traffic class
   bool   hdrTD;     // TLP digest present
   bool   hdrEP;     // TLP poisoned
   UInt32 hdrAttr;   // Attributes
   UInt32 hdrLength; // length of data in 4-byte DWs

   PCIECmd::PCIECmdType tType;   // created transaction type (= 7'h{hdrFmt,hdrType})

   void processHeader(void);
   PCIECmd::PCIECmdType getTLPType(UInt32 x);
   void processMemRead();
   void processMemWrite();
   void processIORead();
   void processIOWrite();
   void processConfigRead();
   void processConfigWrite();
   void processMessage();
   void processCompletion();
//DEL:   void createCompletion();
   int calcByteCount(UInt32 legth, unsigned char lastBE, unsigned firstBE);
   UInt32 fixAddrForBE(UInt32 addr, unsigned char be);
};

#endif

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
