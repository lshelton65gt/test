/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __PCIEXPRESSCOMMON_H__
#define __PCIEXPRESSCOMMON_H__

#ifdef SIMICS_API
// include this file because it redefines 'printf' to catch the output and put all output in the same buffer
#include "simics_api.h"
#endif

#include "PCIExtor.h"  // for user accessable typedefs

class CarbonModel;
class CarbonSimNet;
class CarbonSimTransaction;
class CarbonSimMaster;
class CarbonSimClock;
class PCIExpressTransactor;
class PCIExpressTransaction;
class PCIExpressCommand;

// transactor transmitter & receiver state
typedef enum {
    Electrical_Idle = 0,   // Electrical Idle, Zero on bus
    Logical_Idle = 1,      // Logical Idle
    Link_Training = 2,     // Link Training (TS1 & TS2)
    DLL_Setup = 3,	   // Data Link Layer Setup (InitFC1 & InitFC2)
    Clock_Tolerance = 4,   // Clock Tolerance Compensation
    Skip = 5,              // Skip Ordered Set
    Power_Save = 6,        // Power Saving Modes (L0s, L1, L2)
    Fast_Training = 7,     // Fast Training Set (FTS)
    Active = 98,           // Active, ready for use
    UNKNOWN = 99
} pcie_state_enum_t;

// transactor verbosity (active LO)  - use as (Verbose_Receiver & Verbosity_Low)
typedef enum {                   // hi me lo  tx rx
    Verbose_Receiver = 0x1E,     //  x  x  x   1  0
    Verbose_Transmitter = 0x1D,  //  x  x  x   0  1
    Verbose_All = 0x1C,          //  x  x  x   0  0
    Verbosity_Off = 0x1F,        //  1  1  1   x  x
    Verbosity_Low = 0x1B,        //  1  1  0   x  x
    Verbosity_Medium = 0x17,     //  1  0  1   x  x
    Verbosity_High = 0x0F        //  0  1  1   x  x
} pcie_verbosity_enum_t;

// transaction type
/*  FOUND IN PCIExtor.h
typedef enum {
    // Transaction Type Field Values
    // 7-bit wide values, {Format[1:0],Type[4:0]}
    MRd32 = 0x00,    // Memory Read Request, 32-bit address
    MRd64 = 0x20,    // Memory Read Request, 64-bit address
    MRdLk32 = 0x01,  // Memory Read Request - Locked, 32-bit address
    MRdLk64 = 0x21,  // Memory Read Request - Locked, 64-bit address
    MWr32 = 0x40,    // Memory Write Request, 32-bit address
    MWr64 = 0x60,    // Memory Write Request, 64-bit address
    IORd = 0x02,     // I/O Read Request
    IOWr = 0x42,     // I/O Write Request
    CfgRd0 = 0x04,   // Configuration Read Type 0
    CfgWr0 = 0x44,   // Configuration Write Type 0
    CfgRd1 = 0x05,   // Configuration Read Type 1
    CfgWr1 = 0x45,   // Configuration Write Type 1
    Msg = 0x30,      // Message Request
    MsgD = 0x70,     // Message Request with data payload
    Cpl = 0x0A,      // Completion without Data
    CplD = 0x4A,     // Completion with Data
    CplLk = 0x0B,    // Completion for Locked Memory Read without Data
    CplDLk = 0x4B    // Completion for Locked Memory Read with Data
} pcie_transaction_tlp_type_t;
*/


// State Machines
typedef enum {
    // Link Training State Machine states
    LTSM_Electrical_Idle = 0,	// Electrical Idle, zeros on bus
    LTSM_Logical_Idle = 1,	// Logical Idle
    LTSM_TS1_P_P = 2,		// TS1 {Link=PAD,Lane=PAD}
    LTSM_TS2_P_P = 3,		// TS2 {Link=PAD,Lane=PAD}
    LTSM_TS1_0_P = 4,    	// TS1 {Link=0,  Lane=PAD}
    LTSM_TS2_0_P = 5,		// TS2 {Link=0,  Lane=PAD}
    LTSM_TS1_0_x = 6,		// TS1 {Link=0,  Lane=laneNum}
    LTSM_TS2_0_x = 7,		// TS2 {Link=0,  Lane=laneNum}
    LTSM_Active = 8
} pcie_LTSM_state_t;
typedef enum {
    // Data Link Layer State Machine states
    DLLSM_Inactive = 0,	// DLL is not initialized
    DLLSM_Init1 = 1,	// Sent Stage 1 Init DLLPs
    DLLSM_Init2 = 2,	// Sent Stage 2 Init DLLPs
    DLLSM_Active = 3	// DLL is initialized
} pcie_DLLSM_state_t;

// Completion Status
typedef enum {
    PCIE_Cpl_SC = 0,	// Successful Completion
    PCIE_Cpl_UR = 1,	// Unsupported Request
    PCIE_Cpl_CRS = 2,	// Configuration Request Retry Status
    PCIE_Cpl_CA = 4,	// Completer Abort
    PCIE_Cpl_UKN = 8	// Unknown / Reserved
} pcie_cpl_status_t;

// transaction special characters
// Define the Data Bytes associated with the Special Character Symbol Codes
#define PCIE_COM	0xBC	// K28.5 // Used for Lane and Link Initialization
#define PCIE_STP	0xFB	// K27.7 // Marks the start of a Transaction Layer Packet
#define PCIE_SDP	0x5C	// K28.2 // Marks the start of a Data Link Layer Packet
#define PCIE_END	0xFD	// K29.7 // Marks the end of a TLP or DLLP
#define PCIE_EDB	0xFE	// K30.7 // Marks the end of a nullified TLP
#define PCIE_PAD	0xF7	// K23.7 // Used in Framing and Link Width and Lane ordering negotiations
#define PCIE_SKP	0x1C	// K28.0 // Used for compensating for different bit rates for two communicating ports
#define PCIE_FTS	0x3C	// K28.1 // Used within an ordered set to exit from L0s to L0 (standby to idle)
#define PCIE_IDL	0x7C	// K28.3 // Symbol used in the Electrical Idle ordered set
#define PCIE_RSVD1	0x9C	// K28.4 // RESERVED
#define PCIE_RSVD2	0xDC	// K28.6 // RESERVED
#define PCIE_RSVD3	0xFC	// K28.7 // RESERVED


#endif // __PCIEXPRESSCOMMON_H__
