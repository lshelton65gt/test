// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __PCIEXPRESSTRANSACTOR_H_
#define __PCIEXPRESSTRANSACTOR_H_


#include "carbonsim/CarbonSimNet.h"

#include "PCIExpressCommon.h"
#include "PCIExpressTransaction.h"

#include <vector>
#include <deque>
#include <list>
#include <cassert>


// PCIExpressTransactor class
class PCIExpressTransactor {
 public:
   // CarbonSim Constructor.
   PCIExpressTransactor(PCIEXtor *theXtor, PCIEXtor::PCIEIntfType intf, UInt32 linkwidth, char* inPrintName);
   // Void Constructor.
   PCIExpressTransactor(void);
   // Other Constructor.
   PCIExpressTransactor(char* cmdfile, UInt32 linkwidth, char* inPrintName);

   // Destructor.
   ~PCIExpressTransactor(void);

   // Submit an idle event to the the transactor.
   void idle(UInt64 cycles);

   // Tell the transactor its clock has incremented.
   void step(CarbonTime tick);

   // Just to keep the linker happy
   void print(bool verbose = true, UInt32 indent = 0) { }

   // Set the timescale factor for the CarbonTime sent to the step() function.
   void setTimescale(CarbonTimescale timescale) { mCurrTimescaleValid = true; mCurrTimescale = timescale; }

   // Set Verbosity of the transactor - Receiver, Transmitter, or Both; High, Low, or Off.
   void verbose(UInt32 flags);

   // Get Bus Number for this transactor instance
   inline void setBusNum(UInt32 x) { mBusNum = x; };
   // Get Device Number for this transactor instance
   inline void setDevNum(UInt32 x) { mDevNum = x; };
   // Get Function Number for this transactor instance
   inline void setFuncNum(UInt32 x) { mFuncNum = x; };

   // Get clock signal value
   UInt32 getClkValue(void);
   // Check if we're in reset and reset the SMs if we are
   void checkReset(void);
   // Check if we're seeing TxDetectRx assert and we'll respond
   void checkTxDetectRx(void);

   // Set data transmitted from PCIE Xtor, lanes 0 - 7
   void setTransmitPins(CarbonSimNet** pNet, const char* netPrintName, UInt32 index, UInt32 value);
   void setTransmitPins(CarbonSimNet* net, const char* netPrintName, UInt32 value);
   void setTransmitLos(UInt32 index, UInt32 value);
   inline void setTransmitData(UInt32 idx, UInt32 val)     { setTransmitPins(pSignalRxData, "RxData", idx, val); }
   inline void setTransmitKCode(UInt32 idx, UInt32 val)    { setTransmitPins(pSignalRxDataK, "RxDataK", idx, val); }
   inline void setTransmitRxStatus(UInt32 idx, UInt32 val) { setTransmitPins(pSignalRxStatus, "RxStatus", idx, val); }
   inline void setTransmitRxValid(UInt32 idx, UInt32 val)  { setTransmitPins(pSignalRxValid, "RxValid", idx, val); }
   inline void setTransmitPhyStatus(UInt32 val)            { setTransmitPins(mSignalPhyStatus, "PhyStatus", val); }

   // Get data transmitted to PCIE Xtor, lanes 0 - 7
   UInt32 getReceivePins(CarbonSimNet** pNet, const char* netPrintName, UInt32 index);
   UInt32 getReceivePins(CarbonSimNet* net, const char* netPrintName);
   UInt32 getReceiveLos(UInt32 index);
   inline UInt32 getReceiveData(UInt32 idx)         { return getReceivePins(pSignalTxData, "TxData", idx); }
   inline UInt32 getReceiveKCode(UInt32 idx)        { return getReceivePins(pSignalTxDataK, "TxDataK", idx); }
   inline UInt32 getReceiveTxCompliance(UInt32 idx) { return getReceivePins(pSignalTxCompliance, "TxCompliance", idx); }
   inline UInt32 getReceiveRxPolarity(UInt32 idx)   { return getReceivePins(pSignalRxPolarity, "RxPolarity", idx); }
   inline UInt32 getReceiveTxDetectRx(void)         { return getReceivePins(mSignalTxDetectRx, "TxDetectRx"); }
   inline UInt32 getReceiveResetN(void)             { return getReceivePins(mSignalResetN, "Reset#"); }
   inline UInt32 getReceivePowerDown(void)          { return getReceivePins(mSignalPowerDown, "PowerDown"); }


   // Get Bus Number for this transactor instance
   inline UInt32 getBusNum(void) { return mBusNum; };
   // Get Device Number for this transactor instance
   inline UInt32 getDevNum(void) { return mDevNum; };
   // Get Function Number for this transactor instance
   inline UInt32 getFuncNum(void) { return mFuncNum; };

   inline UInt32 getNewDLLPSeqNum(void) { return(mDLLPSeqNum++ % 4096); };
   inline UInt32 getLinkWidth(void) { return mLinkWidth; };
   inline char* getPrintName(void) { return printName; };
   bool isDoneForNow(void) { return mDoneForNow; }  // let outside world know if we're done working for now (& clear)
   PCIEXtor::PCIEIntfType getInterfaceType(void) { return mIntfType; }

   SInt32 addTransaction(PCIECmd*);   // return transaction ID, or -1 for error/invalid
   bool readCfgFile(char* cmdfile);
   UInt32 scramble_byte(UInt32 inbyte, bool kin, UInt32 lane)   { return scramble_descramble_byte(inbyte,kin,true,lane);  }
   UInt32 descramble_byte(UInt32 inbyte, bool kin, UInt32 lane) { return scramble_descramble_byte(inbyte,kin,false,lane); }
   UInt32 encoder_lut(UInt32, bool, UInt32);
   UInt32 decoder_lut(UInt32, bool*, UInt32);
   // Link Training State Machine processing
   void LTSM_process(void);
   // Data Link Layer State Machine processing
   void DLLSM_process(void);

   // Array of packets to be put on bus now, need one packet per link section (x4 link needs 4 packets)
   UInt32 *mLatestPackets;    // pointer to an array of packets, size of array defined by mLinkWidth
   UInt32 *mLatestPacketSym;    // pointer to an array of packets, size of array defined by mLinkWidth

   inline UInt32 getUniqueTag(void) { return(next_tag++); };

   bool getVerboseRxHigh(void) { return VerboseReceiverHigh; }
   bool getVerboseRxMed(void) { return VerboseReceiverMedium; }
   bool getVerboseRxLow(void) { return VerboseReceiverLow; }
   bool getVerboseTxHigh(void) { return VerboseTransmitterHigh; }
   bool getVerboseTxMed(void) { return VerboseTransmitterMedium; }
   bool getVerboseTxLow(void) { return VerboseTransmitterLow; }

   UInt32 calc16bCRC(UInt32*,UInt32);
   UInt32 calc32bCRC(UInt32*,UInt32);

   UInt8 getCplData(UInt32 idx) { return mCplData.at(idx); }
   void clrCplData(void) { mCplData.clear(); }
   void setCplData(UInt32 idx, UInt8 val) {
      if( not (idx >= mCplData.size()) ) {
	 mCplData[idx] = val;
      } else {
	 assert("Index out of range in PCIExpressTransactor::setCplData(UInt32, UInt8)" == 0);
      }
   }
   UInt32 getCplDataSize(void) { return mCplData.size(); }
   void setCplDataSize(UInt32 size) { mCplData.resize(size,0); }
   pcie_cpl_status_t getCplStatus(void) { return mCplStatus; }
   void setCplStatus(pcie_cpl_status_t s) { mCplStatus = s; }

   // Added for the pImpl interface...
   void   setPrintLevel(UInt32 flags);
   UInt32 getPrintLevel(void);

   void   setScrambleStatus(bool tf);
   bool   getScrambleStatus(void) { return mScrambleEnable; }

   bool   getDoneForNow(void) { return mDoneForNow; }
   void   setDoneCallbackFn(PCIEXtor::PCIEDoneCBFn *doneCbFn, void* doneCbInfo) { mDoneCBFn=doneCbFn; mDoneCBInfo=doneCbInfo; }
   PCIEXtor::PCIEDoneCBFn* getDoneCallbackFn(void) { return mDoneCBFn; }
   void*  getDoneCallbackData(void) { return mDoneCBInfo; }

   bool   setSignal(PCIEXtor::PCIESignalType sigType, UInt32 index, CarbonSimNet *net);
   bool   setSignal(PCIEXtor::PCIESignalType sigType, CarbonSimNet *net);
   bool   setSignal(CarbonSimClock *net);
   void   setElecIdleActiveHigh(void) { setElecIdleActiveState(true); }  // only valid for e10BitIntf;
   void   setElecIdleActiveLow(void) { setElecIdleActiveState(false); }  // only valid for e10BitIntf;

   bool   isConfigured(void);
   void   printConfiguration(void);
   bool	  isLinkActive(void);
   void   printLinkState(void);

   void   setMaxTransInFlight(UInt32 max) { if (max == 0 || max > 32) { mMaxTransInFlight = 32; } else { mMaxTransInFlight = max; } }
   UInt32 getMaxTransInFlight(void) { return mMaxTransInFlight; }

   // Allow the user to get their return data and received transactions
   void   setTransCallbackFn(PCIEXtor::PCIETransCBFn *transCbFn, void* transCbInfo) { mTransCBFn=transCbFn; mTransCBInfo=transCbInfo; }
   PCIEXtor::PCIETransCBFn* getTransCallbackFn(void) { return mTransCBFn; }
   void*  getTransCallbackData(void) { return mTransCBInfo; }

   // Return head of the return data queue
   bool   getReturnData     (PCIECmd *cmd);                        // Get PCIECmd object with return data from transaction referred to by seqNum in the PCIECmd object
   // Length of return data queue
   UInt32 getReturnDataQueueLength(void) { return mReturnDataList.size(); }
   // Return head of the received transaction queue
   bool   getReceivedTrans  (PCIECmd *cmd);                        // Get PCIECmd object with received transaction information
   // Length of received transaction queue
   UInt32 getReceivedTransQueueLength(void) { return mReceivedTransList.size(); }

   bool   startTraining(void);
   bool   startTraining(CarbonTime tick);

   void   setTrainingBypass(bool tf);
   bool   getTrainingBypass(void) { return mTrainingBypass; }

   void   setMaxPayloadSize(UInt32 val); // use encoded values from spec 7.8.4
   UInt32 getMaxPayloadSize(void);       // use encoded values from spec 7.8.4

   // Called from other Internal PCIE classes (class PCIExpressCallback)
   void   callTransCallbackFn(PCIEXtor::PCIECallbackType type, PCIECmd *data);   // call the callback function with a Pointer to this data object and reason for calling
   const char* getSymStringFromByte(UInt32);

   const char* getTimeString(void) { return mCurrTimeString; }
   const char* getLTSMStateString(pcie_LTSM_state_t sm);
   const char* getDLLSMStateString(pcie_DLLSM_state_t sm);

 private:
   typedef enum {
      eSuccess,
      eNoTransAvailable,
      eNoCreditsAvailable,
      eError = 99
   } GetTransactionReturnStatus;

   typedef std::list<PCIExpressTransaction *> PCIExpressTransactionList;

   PCIEXtor *mParent;              // pointer to the externally visible object, so it can be returned in callbacks

   // Give us the clock to work from.  We need either the CarbonSimNet or CarbonSimClock
   //  version.  The last to be set will be used and the other will be unset.
   CarbonSimNet *mSignalClk_SimNet;      // READ ONLY, net from the DesignPlayer
   CarbonSimClock *mSignalClk_SimClock;  // READ ONLY, clock attached to the CarbonSimMaster
   // Dynamic sizing of Tx/Rx data signal holders
   CarbonSimNet **pSignalRxData;    // alloc 'mLinkWidth' CarbonSimNet*'s for BFMs transmitter Data lines (dut transmitter lines)
   CarbonSimNet **pSignalTxData;    // alloc 'mLinkWidth' CarbonSimNet*'s for BFMs receiver Data lines (dut receiver lines)
   CarbonSimNet **pSignalRxEIdle;   // alloc 'mLinkWidth' CarbonSimNet*'s for BFMs transmitter elec idle lines
   CarbonSimNet **pSignalTxEIdle;   // alloc 'mLinkWidth' CarbonSimNet*'s for BFMs receiver elec idle lines

   //  ePipeIntf specific
   CarbonSimNet **pSignalRxDataK;      // alloc 'mLinkWidth' CarbonSimNet*'s for BFMs transmitter K code lines
   CarbonSimNet **pSignalTxCompliance; // alloc 'mLinkWidth' CarbonSimNet*'s for BFMs receiver tx_compliance lines (is dut sending compliance pattern)
   CarbonSimNet **pSignalRxPolarity;   // alloc 'mLinkWidth' CarbonSimNet*'s for BFMs receiver rx_polarity lines (invert data polarity for dut's receiver)
   CarbonSimNet **pSignalTxDataK;      // alloc 'mLinkWidth' CarbonSimNet*'s for BFMs receiver K code lines
   CarbonSimNet **pSignalRxStatus;     // alloc 'mLinkWidth' CarbonSimNet*'s for BFMs transmitter rx_status lines
   CarbonSimNet **pSignalRxValid;      // alloc 'mLinkWidth' CarbonSimNet*'s for BFMs transmitter rx_valid lines
   CarbonSimNet  *mSignalTxDetectRx;   // 1 CarbonSimNet* for BFMs receiver detect opereration or begin loopback
   CarbonSimNet  *mSignalResetN;       // 1 CarbonSimNet* for BFMs reset_low
   CarbonSimNet  *mSignalPowerDown;    // 1 CarbonSimNet* for BFMs power down status
   CarbonSimNet  *mSignalPhyStatus;    // 1 CarbonSimNet* for BFMs phy status

   bool mEIdleActiveHigh;           // are the Elec Idle nets active High (true) or Low (false)?

   UInt32 mBusNum;  // xtor Bus Number       //
   UInt32 mDevNum;  // xtor Device Number    // The Requestor ID
   UInt32 mFuncNum; // xtor Function Number  //
   UInt32 mDLLPSeqNum;  // xtor DLLP Sequence Number

   UInt32 mLinkWidth; // link width that we're connecting to (how many packets to send per cycle

   UInt32 mMaxTransInFlight;  // maximum number of transactions in flight at once, current spec max is 32 (default), use 1 to wait for one to complete before sending the next
   UInt32 mNumTransInFlight;  // current number of transactions in flight, value must be between 0 and mMaxTransInFlight

   // Character pointer containing the name for this device in print statements (default = "PCIE")
   char* printName;

   // Declare invalid constructors as private
   PCIExpressTransactor(const PCIExpressTransactor&);  
   PCIExpressTransactor& operator=(const PCIExpressTransactor&);
   UInt32 scramble_descramble_byte(UInt32,bool,bool,UInt32);

   void printTransactionQueue(void);
   void printTransactionQueueSingle(PCIExpressTransactionList&, const char*);
   void printReceivedPacketList(void);
   void printReceivedPacketList(UInt32 start, UInt32 end);

   UInt32 next_tag;

   // Adding Training Transactions, not user transactions
   void addTransactionSpecial(const char*);
   void addTransactionSpecialHead(const char*);
   void addTransactionSpecialAckNak(bool, UInt32);
   // Get pointer to transaction to be active currently or next
   GetTransactionReturnStatus getNewTransaction(void);
   // Check transaction for flow control credits
   bool checkForAvailFlowControlCredits(PCIExpressTransaction*);
   // See if completion or write can be split to go on the bus, and if so then do the split
   bool splitDataTransaction(bool, PCIExpressTransactionList::iterator);
   // Remove all transactions from queue
   void deleteTransactionQueue(void);
   // Process a completion message to clear the needsCpl flag from another transaction on the queue
   void processCompletion(PCIECmd*);
   // Process a ack message to clear the needsAck flags
   void processAck(UInt32);
   // Process a nak message to replay the transactions on the queue
   void processNak(UInt32);
   // Move transactions back onto 'new' queue to be replayed
   void updateTransactionsForNak(void);

   // Data Link Layer Protocol vars:
   bool *disparity_in;           // keep track of disparity for decoding each lane
   bool *disparity_out;          // keep track of disparity for encoding each lane
   bool *disparity_in_not_set;   // decide if we've figured out the correct input disparity yet
   //   Look Up Table for data bytes for 8b/10b encoding & decoding, with Positive Disparity
   UInt32 lut_data_10bit_p[256];
   //   Look Up Table for data bytes for 8b/10b encoding & decoding, with Negative Disparity
   UInt32 lut_data_10bit_n[256];
   //   Look Up Table for special bytes for 8b/10b encoding & decoding, with Positive Disparity
   UInt32 lut_spec_10bit_p[256];
   //   Look Up Table for special bytes for 8b/10b encoding & decoding, with Negative Disparity
   UInt32 lut_spec_10bit_n[256];
   void init_luts(void);

   // Data Scrabling (phys layer)
   unsigned short *lfsr_out;  // Linear Feedback Shift Register for scrabling outgoing bytes (one per lane)
   unsigned short *lfsr_in;   // Linear Feedback Shift Register for descrabling incoming bytes (one per lane)

   bool VerboseReceiverHigh;       // print out a LOT of Receiver info
   bool VerboseReceiverMedium;     // print out a bit of Receiver info
   bool VerboseReceiverLow;        // print out some Receiver info
   bool VerboseTransmitterHigh;    // print out a LOT of Transmitter info
   bool VerboseTransmitterMedium;  // print out a bit of Transmitter info
   bool VerboseTransmitterLow;     // print out some Transmitter info

   bool mScrambleEnable;           // Scramble/Descramble Enable for Receiver & Transmitter
   bool mDoneForNow;               // Are we done processing all of our commands / tasks? (IE, we don't need any more sim time right now)
   bool mInReset;                  // Are we in Reset currently?
   UInt32 mResetTimer;             // timer that counts when we leave reset
   UInt32 mDetectRxTimer;          // timer that counts ticks during TxDetectRx training session

   // Incoming Transaction Tracking and Usage...
   //  Called each step to watch input bus
   void readInputBus();
   //  Parse the full lane of packets coming in
   void parsePackets();
   // Check the packets we've gathered on the incoming port and see if we can decypher them
   void checkCurrentTransactionIncoming(void);
   void checkCurrentTransactionFTS(void);
   void checkCurrentTransactionPhysTraining(void);
   bool checkCurrentTransactionPhysTraining_idnumOnLanes(UInt32); // (helper function for above)
   void checkCurrentTransactionPadding(void);
   bool checkCurrentTransactionComplete(void);     // are there any complete transactions on the queue
   void checkCurrentTransactionDLLTraining(void);  // find & parse DLLPs on queue
   void checkCurrentTransactionTLPs(void);         // find & parse TLPs on queue
   void clearReceiverBuffer(UInt32, UInt32);
   // take starting id for packets held and length, return a buffer of packets with Ordered Sets expanded (in = 5 bytes including 1 ordered set, out = 12 bytes [linkwidth=8])
   UInt32 expandTransactionPackets(UInt32, UInt32, UInt32**);
   UInt32 expandTransactionPackets(UInt32, UInt32, UInt32**, bool**);  // also return list of symbol[T]/data[F] to match bytes of data
   // find the number of packets/packetsets in END of newTransactionPackets to get 'x' individual packets
   UInt32 findNumPacketsinPacketSets(UInt32);
   // Array of packets to be read off bus now, need one packet per link section (x4 link needs 4 packets)
   UInt32 *mIncomingPackets;    // pointer to an array of packets, size of array defined by mLinkWidth
   bool *mIncomingPacketsSym;   // pointer to an array of k codes, size of array defined by mLinkWidth
   UInt32 *mSpeculativeDescramBytes;  // pointer to array of packets, size of array defined by mLinkWidth - this stores the descrambled data when looking for the end of training on receiver
   // Array of DLLP Layer bytes to be processed once the entire transaction is received (only need 22: DLLP Header(2) + TLP(<=16) + DLLP Footer(4))
   UInt32 mIncomingDLLPBytes[22];
   UInt32 mIncomingDLLPBytesNext;    // Where to put the next byte (index var)
   // Array of packets for incoming transaction, hold packets until we see a useful transaction and act on it
   static const unsigned int TRANS_BUF_SIZE = 600;
   UInt32 newTransactionPackets[TRANS_BUF_SIZE];    // hold byte data
   bool newTransactionPacketsSym[TRANS_BUF_SIZE];   // hold k_sym bit
   bool newTransactionPacketsOSet[TRANS_BUF_SIZE];  // was it an ordered set of this data, or is this a single packet of data
   UInt32 newTransactionPacketsTotal;    // how many packets do we have now?
   UInt32 mCurrTransactionStart;
   UInt32 mCurrTransactionEnd;

   // Keep track of cycle count for adding SKP ordered sets
   UInt32 cycle_count_skp;

   // Are we in L0s power-saving state or not?
   UInt8 powerSave_L0s;   // 0 = Active; 1 = Power-Down; 2 = PowerSave; 3 = Returning/Power-Up

   // Are our transmitters currently powered down?
   UInt32 curPowerDnState;

   // Are our receivers seeing a power_down signal (current & delayed by 1 cycle)
   UInt32 txPowerDown;
   UInt32 txPowerDown_d1;

   // Receiver Link Training State Machine
   pcie_LTSM_state_t rxLTSM;
   // Transmitter Link Training State Machine
   pcie_LTSM_state_t txLTSM;
   // Receiver Data Link Layer State Machine
   pcie_DLLSM_state_t rxDLLSM;
   // Transmitter Data Link Layer State Machine
   pcie_DLLSM_state_t txDLLSM;
   // Are we reading in a transaction currently?
   bool mInTransaction;
   // Current status of the receiver
   pcie_state_enum_t curInputState;
   // Current status of the receiver - in text string
   char curInputStateStr[20];
   // Set state of receiver
   inline void setCurInputState(pcie_state_enum_t s) { setCurStateGeneric(s,true); };
   // Current status of the transmitter (xtor)
   pcie_state_enum_t curOutputState;
   // Current status of the transmitter - in text string
   char curOutputStateStr[20];
   // Set state of transmitter
   inline void setCurOutputState(pcie_state_enum_t s) { setCurStateGeneric(s,false); };
   // Do the actual setting of state for transmittor and receiver
   void setCurStateGeneric(pcie_state_enum_t newState, bool direction);

   // Put idle data onto the output wires
   void transmitIdleData(void);

   // Put data onto output wires
   void transmitData(bool encode);

   // Store transaction data for last Completion operation received
   std::vector<UInt8> mCplData;  // vector of bytes for Completion data
   // Store status for last Completion operation received
   pcie_cpl_status_t mCplStatus;

   // Flag that the transactor has been properly configured.  We check this before
   //  executing so that we don't end up with segfaults!
   bool mFullyConfigured;
   bool mHasBeenWarnedAboutConfig;

   // Flag that we're bypassing training or not.
   bool mTrainingBypass;
   // Flag that we're going to start training from Electrical Idle
   UInt32 mStartTraining;   // 0=no_action, 1=start_training, 2=in_progress, etc
   UInt64 mStartTrainingPrefaceIdleTime;  // number of cycles to run logical idle before we start sending training packets

   PCIEXtor::PCIEDoneCBFn *mDoneCBFn;
   void *mDoneCBInfo;

   PCIEXtor::PCIETransCBFn *mTransCBFn;
   void *mTransCBInfo;

   bool mFirstCall;   // have we run our first call to step()?

   bool mReplayTransactions;  // do we need to replay all transactions

   UInt64 mCurrTick;                // current tick (time)
   bool mCurrTimescaleValid;        // has the timescale been set
   CarbonTimescale mCurrTimescale;  // timescale for mCurrTick
   char* mCurrTimeString;           // c-string with the mCurrTick scaled to mCurrTimescale

   bool mForcedNaks; // set if we are forcing transactions to be Nak'd (until erroring transaction is retransmitted)
   UInt32 mLastTxAckSeqNum; // the last properly Ack'd sequence number we sent
   UInt32 mLastRxAckSeqNum; // the last properly Ack'd sequence number we received

   PCIEXtor::PCIEIntfType mIntfType;  // what is the interface type (10bit, pipe, other)

   bool isConfigured_10BitIntf(void);
   bool isConfigured_PipeIntf(void);

   void updateCurrTimeString(void);  // update the mCurrTimeString

   const char* getStringFromPCIESignalType(PCIEXtor::PCIESignalType);
   void setElecIdleActiveState(bool);

   PCIExpressTransaction *mTransactionInProgress1;     // Currently being transmitted, if any
   PCIExpressTransaction *mTransactionInProgress2;     // Currently being transmitted (Back-to-back non-padded), if any
   PCIExpressTransactionList mTransactionListNew;      // list of PCIExpressTransction objects for xtor to drive onto the bus (added by user, pop'd by xtor)
   PCIExpressTransactionList mTransactionListNeedAck;  // list of PCIExpressTransction objects which have been transmitted, waiting for Ack (moved by xtor from mTransactionListNew to here)
   PCIExpressTransactionList mTransactionListNeedCpl;  // list of PCIExpressTransction objects which have been Ack'd, waiting for completion (moved by xtor from mTransactionListNeedAck to here)

   typedef std::deque<PCIECmd *> PCIECmdList;  // const access to front & back, linear access to middle
   PCIECmdList mReceivedTransList;    // list of PCIECmd objects for received transactions (to be added by xtor, pop'd by user)
   PCIECmdList mReturnDataList;       // list of PCIECmd objects for return data (to be added by xtor, pop'd by user)


   // Flow Control Data
   //   Credits allocated by DUT for amt we can tx
   UInt32 mFcTxPostedHdrCreditLimit;      // P   HdrFC  Limit advertised
   UInt32 mFcTxPostedDataCreditLimit;     // P   DataFC Limit advertised
   UInt32 mFcTxNonPostedHdrCreditLimit;   // NP  HdrFC  Limit advertised
   UInt32 mFcTxNonPostedDataCreditLimit;  // NP  DataFC Limit advertised
   UInt32 mFcTxCompletionHdrCreditLimit;  // Cpl HdrFC  Limit advertised
   UInt32 mFcTxCompletionDataCreditLimit; // Cpl DataFC Limit advertised

   UInt32 mFcTxPostedHdrCreditsConsumed;      // P   HdrFC  Credits consumed
   UInt32 mFcTxPostedDataCreditsConsumed;     // P   DataFC Credits consumed
   UInt32 mFcTxNonPostedHdrCreditsConsumed;   // NP  HdrFC  Credits consumed
   UInt32 mFcTxNonPostedDataCreditsConsumed;  // NP  DataFC Credits consumed
   UInt32 mFcTxCompletionHdrCreditsConsumed;  // Cpl HdrFC  Credits consumed
   UInt32 mFcTxCompletionDataCreditsConsumed; // Cpl DataFC Credits consumed

   // Device Control Register (0x08) bits [7:5] (spec 1.0a sec 7.8.4)
   //   max data allowed to send in a single transaction
   //   Possible Values:   [default = 128B]
   //      128B =  32DW,  256B =  64DW,  512B =  128DW,
   //     1024B = 256DW, 2048B = 512DW, 4096B = 1024DW
   UInt32 mMaxPayloadSizeDW;
};

#endif

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
