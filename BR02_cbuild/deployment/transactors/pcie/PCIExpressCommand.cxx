// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "PCIExpressCommand.h"

PCIExpressCommand::PCIExpressCommand(void)
  : type(PCIECmd::Msg), addrHi(0), addrLo(0), reqID(0),
    tag(0), compID(0), msgCode(0), compStat(0),
    byteCount(0), lowAddr(0), seqNum(0)
{
   mData.clear();
}

PCIExpressCommand::PCIExpressCommand(const PCIExpressCommand& in) {
   copy(in);
}

void PCIExpressCommand::copy(const PCIExpressCommand& in) {
   type = in.type;
   addrHi = in.addrHi;
   addrLo = in.addrLo;
   reqID = in.reqID;
   tag = in.tag;
   regNum = in.regNum;
   compID = in.compID;
   msgRout = in.msgRout;
   msgCode = in.msgCode;
   compStat = in.compStat;
   byteCount = in.byteCount;
   lowAddr = in.lowAddr;
   firstDWBE = in.firstDWBE;
   lastDWBE = in.lastDWBE;
   seqNum = in.seqNum;
   length = in.length;
   mData = in.mData;
}

void PCIExpressCommand::setCmd(UInt32 cmd) {
   switch (cmd) {
   case 0x00:  type = PCIECmd::MRd32;   break;
   case 0x20:  type = PCIECmd::MRd64;   break;
   case 0x01:  type = PCIECmd::MRdLk32; break;
   case 0x21:  type = PCIECmd::MRdLk64; break;
   case 0x40:  type = PCIECmd::MWr32;   break;
   case 0x60:  type = PCIECmd::MWr64;   break;
   case 0x02:  type = PCIECmd::IORd;    break;
   case 0x42:  type = PCIECmd::IOWr;    break;
   case 0x04:  type = PCIECmd::CfgRd0;  break;
   case 0x44:  type = PCIECmd::CfgWr0;  break;
   case 0x05:  type = PCIECmd::CfgRd1;  break;
   case 0x45:  type = PCIECmd::CfgWr1;  break;
   case 0x30:  type = PCIECmd::Msg;     break;
   case 0x70:  type = PCIECmd::MsgD;    break;
   case 0x0A:  type = PCIECmd::Cpl;     break;
   case 0x4A:  type = PCIECmd::CplD;    break;
   case 0x0B:  type = PCIECmd::CplLk;   break;
   case 0x4B:  type = PCIECmd::CplDLk;  break;
   default:
      printf("ERROR: Attempting to set PCIExpressCommand to an invalid value: 0x%02X.\n", cmd);
      break;
   }
}

UInt32 PCIExpressCommand::getCmd(void) {
   return (UInt32)type;
}

void PCIExpressCommand::setCmdByName(PCIECmd::PCIECmdType cmd) {
   type = cmd;
}

PCIECmd::PCIECmdType PCIExpressCommand::getCmdByName(void) {
   return type;
}

void PCIExpressCommand::setAddr(UInt64 addr) {
   addrHi = (addr >> 32) & 0xFFFFFFFF;
   //   addrLo = addr & 0xFFFFFFFF;
   addrLo = addr & 0xFFFFFFFC;  // DW addressing
}

UInt64 PCIExpressCommand::getAddr(void) {
   UInt64 addr = (((UInt64)addrHi << 32) | addrLo);
   return addr;
}

void PCIExpressCommand::setReqId(UInt32 id) {
   reqID = id;
}

UInt32 PCIExpressCommand::getReqId(void) {
   return reqID;
}

void PCIExpressCommand::setTag(UInt32 Tag) {
   tag = Tag;
}

UInt32 PCIExpressCommand::getTag(void) {
   return tag;
}

void PCIExpressCommand::setRegNum(UInt32 reg) {
   regNum = (reg & 0x03FF);
}

UInt32 PCIExpressCommand::getRegNum(void) {
   return regNum;
}

void PCIExpressCommand::setCompId(UInt32 id) {
   compID = id;
}

UInt32 PCIExpressCommand::getCompId(void) {
   return compID;
}

void PCIExpressCommand::setMsgRout(UInt32 rout) {
   msgRout = rout;
}

UInt32 PCIExpressCommand::getMsgRout(void) {
   return msgRout;
}

void PCIExpressCommand::setMsgCode(UInt32 code) {
   msgCode = code;
}

UInt32 PCIExpressCommand::getMsgCode(void) {
   return msgCode;
}

void PCIExpressCommand::setLowAddr(UInt32 addr) {
   lowAddr = addr;
}

UInt32 PCIExpressCommand::getLowAddr(void) {
   return lowAddr;
}

void PCIExpressCommand::setByteCount(UInt32 count) {
   byteCount = count;
}

UInt32 PCIExpressCommand::getByteCount(void) {
   return byteCount;
}

// DEPRECATED
UInt32 PCIExpressCommand::getDWCount(void) {  // number of DWs taken up by the byte count
   return ((byteCount + 3) / 4);
}

void PCIExpressCommand::setFirstBE(UInt4 be) {
   firstDWBE = be;
}

UInt4 PCIExpressCommand::getFirstBE(void) {
   return firstDWBE;
}

void PCIExpressCommand::setLastBE(UInt4 be) {
   lastDWBE = be;
}

UInt4 PCIExpressCommand::getLastBE(void) {
   return lastDWBE;
}

void PCIExpressCommand::setCplStatus(UInt32 status) {
   compStat = (status & 0x7);
}

UInt32 PCIExpressCommand::getCplStatus(void) {
   return compStat;
}

void PCIExpressCommand::setDataByte(UInt32 index, UInt8 data) {
   if (index >= mData.size())
      mData.resize(index+1);
   mData[index] = data;
}

UInt8 PCIExpressCommand::getDataByte(UInt32 index) {
   if (index < mData.size())
      return mData.at(index);
   else
      return 0x0;
}

// Resize the data vector to (start,end) inclusive
void PCIExpressCommand::resizeDataBytes(UInt32 start, UInt32 end) {
   mData.resize(end + 1, 0); // remove all after 'end' or fill in with zeros
   if (start != 0)       // remove from begining and shift down
      mData.erase(mData.begin(), mData.begin() + start);
}

UInt32 PCIExpressCommand::getDataByteCount(void) {
   return mData.size();
}

void PCIExpressCommand::setDataLength(UInt32 len) {
   length = len & 0x3FF;
}

UInt32 PCIExpressCommand::getDataLength(void) {
   return length;
}

SInt32 PCIExpressCommand::getTransId(void) {
   return (((reqID & 0xFFFF) << 8) | (tag & 0x00FF));  // concatinate reqID[15:0] and tag[7:0]
}

bool PCIExpressCommand::isRead(void) {
   switch (type) {
   case PCIECmd::MRd32:
   case PCIECmd::MRd64:
   case PCIECmd::MRdLk32:
   case PCIECmd::MRdLk64:
   case PCIECmd::IORd:
   case PCIECmd::CfgRd0:
   case PCIECmd::CfgRd1:
      return true;
   default:
      return false;
   }
}

bool PCIExpressCommand::isWrite(void) {
   switch (type) {
   case PCIECmd::MWr32:
   case PCIECmd::MWr64:
   case PCIECmd::IOWr:
   case PCIECmd::CfgWr0:
   case PCIECmd::CfgWr1:
      return true;
   default:
      return false;
   }
}

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
