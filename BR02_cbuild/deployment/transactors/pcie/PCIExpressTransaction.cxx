// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include "PCIExpressTransaction.h"
#include "PCIExpressTransactor.h"
#include "carbonsim/CarbonSimNet.h"


PCIExpressTransaction::PCIExpressTransaction(PCIExpressTransactor *xtor, const char *cmdstr)
  : mXtor(xtor), mPcieCmd(0), mIsValid(true), mIsSpecial(true), mIsCpl(false), mIsAck(false), mIsNak(false), mIsTLP(false),
    mMustStartLane0(false), mDone(false), mNeedSeqNumAndLCRC(false), mBytesSent(0), mBytesTotal(0), phys_bytes_dat(NULL), phys_bytes_sym(NULL),
    runOutputState(Active), DLLP_SeqNum(0xDEADBEEF), expectAck(false),
    expectCpl(false), transID(0), canNotReplay(false), doReplay(false), numRetries(0),
    fcType(None), fcHeader(0), fcData(0), fcCreditsConsumed(true),
    mCompletionFunc(0)
{
   strcpy(printString, "");

   if((strncmp(cmdstr,"TS1 ",4) == 0) ||        // training set TS1
      (strncmp(cmdstr,"TS2 ",4) == 0) ||        // training set TS2
      (strcmp(cmdstr,"EIDLE") == 0) ||          // electrical idle (0)
      (strncmp(cmdstr,"IDLE ",5) == 0) ||       // logical idle for X cycles
      (strcmp(cmdstr,"CLOCK_TOL") == 0) ||      // clock tolerance compensation
      (strcmp(cmdstr,"InitFC1") == 0) ||        // DLLP Init Flow Control 1
      (strcmp(cmdstr,"InitFC2") == 0) ||        // DLLP Init Flow Control 2
      (strcmp(cmdstr,"UpdateFC") == 0) ||       // DLLP Update Flow Control
      (strncmp(cmdstr,"DLLP_Ack ",9) == 0) ||   // DLLP Ack Transaction
      (strncmp(cmdstr,"DLLP_Nak ",9) == 0) ||   // DLLP Nak Transaction
      (strcmp(cmdstr,"POWER_SAVE") == 0) ||     // L0->L0s IDLE Set
      (strcmp(cmdstr,"FTS") == 0)               // fast training set
     ) {
      // found a Special Command
      initializeTransactionSpecial(xtor,cmdstr);
      return;
   } else {
      mDone = true;
      mIsValid = false;
      if(xtor->getVerboseRxLow())
	 printf("%s  ERROR: Problem initializing PCIExpress transaction.  A bad command was submitted internally (%s).\n", xtor->getPrintName(), cmdstr);
   }

   return;
}

PCIExpressTransaction::PCIExpressTransaction(PCIExpressTransactor *xtor, PCIECmd *cmd)
  : mXtor(xtor), mPcieCmd(0), mIsValid(true), mIsSpecial(false), mIsCpl(false), mIsAck(false), mIsNak(false), mIsTLP(true),
    mMustStartLane0(false), mDone(false), mNeedSeqNumAndLCRC(true), mBytesSent(0), mBytesTotal(0), phys_bytes_dat(NULL), phys_bytes_sym(NULL),
    runOutputState(Active), DLLP_SeqNum(0xDEADBEEF), expectAck(false),
    expectCpl(false), transID(0), canNotReplay(false), doReplay(false), numRetries(0),
    fcType(None), fcHeader(0), fcData(0), fcCreditsConsumed(false),
    mCompletionFunc(0)
{
   if (cmd == 0) {  // passed in a Zero pointer for the command
      if(xtor->getVerboseRxLow())
	 printf("%s  ERROR: Problem initializing PCIExpress transaction.  Possibly a bad PCIECmd passed in.\n", xtor->getPrintName());
      mDone = true;
      mIsValid = false;
      return;
   }

   strcpy(printString, "");
   initializeTransaction(xtor, cmd);
   mPcieCmd = new PCIECmd(*cmd);  // make a local copy
   return;
}


PCIExpressTransaction::~PCIExpressTransaction(void) {
   delete[] phys_bytes_dat;
   delete[] phys_bytes_sym;
   if (mPcieCmd != 0) delete mPcieCmd;
}


void PCIExpressTransaction::step(PCIExpressTransactor *xtor, CarbonTime tick) {
   UInt32 i;

   if (mBytesSent == 0) {  // first cycle
      if (mNeedSeqNumAndLCRC == true)   // first time playing transaction
	 setSeqNumAndCRC(xtor);

      if(xtor->getVerboseRxMed()) {
	 printf("%s  Info: Starting Transaction '%s' ", xtor->getPrintName(), printString);
	 if (not mIsSpecial)
	    printf("(SeqNum=0x%03X) ", DLLP_SeqNum);
	 printf("(at %s)\n", xtor->getTimeString());
      }
      if(xtor->getVerboseRxHigh()) {
	 printf("%s           Transaction Bytes: ", xtor->getPrintName());
	 for (i=0; i < mBytesTotal; ++i) {
	    if (phys_bytes_sym[i])
	       printf("  %s", xtor->getSymStringFromByte(phys_bytes_dat[i]));
	    else
	       printf("  %02X", phys_bytes_dat[i]);
	 }
	 printf("\n");
      }
   }

   if (mDone && doReplay) {
      mBytesSent = 0;
      mDone = false;
      if (not mIsSpecial)
	 expectAck = true;
      numRetries++;
      doReplay = false;
   }
}


void PCIExpressTransaction::getTransmitPackets(UInt32 num, UInt32 * datHolder, UInt32 * symHolder) {
   UInt32 i;

   if ((datHolder == 0) || (symHolder == 0)) {
      printf("%s  ERROR: Received NULL pointer for internal packet transmission buffer.\n", mXtor->getPrintName());
      return;
   }

   // This shouldn't happen, but protect incase it does...
   if (mBytesSent == mBytesTotal) {
      // We must be waiting for something, so just output idle data
      //   The transactor has the ability of replaying this transaction if a Nak is received,
      //   so we'll just hang out until we are replayed or deleted.
      for(i=0; i < num; ++i) {
	 datHolder[i] = 0x00;
	 symHolder[i] = false;
      }
      return;
   }

   for (i = 0; i < num; ++i) {
      if ((i + mBytesSent) >= mBytesTotal) {
	 datHolder[i] = PCIE_PAD;   // fill with PADs
	 symHolder[i] = true;
      } else {
	 datHolder[i] = phys_bytes_dat[(i + mBytesSent)];
	 symHolder[i] = phys_bytes_sym[(i + mBytesSent)];
      }
   }

   mBytesSent += num;
   if (mBytesSent > mBytesTotal)
      mBytesSent = mBytesTotal;


   if (mBytesTotal == mBytesSent) {
      mDone = true;    // we put our last cycle out

      if (mXtor->getVerboseRxMed())
	 printf("%s  Info: Finishing Transaction '%s' (at %s)\n",mXtor->getPrintName(), printString, mXtor->getTimeString());
   }

   return;
}


void PCIExpressTransaction::onCompletion(CompletionFunc * func, void * user_arg) {
   assert(func);
   mCompletionFunc = func;
   mCompletionFuncArg = user_arg;
}


void PCIExpressTransaction::doCompletion(void) {
   UInt32 a;
   a = 0;
   if (mCompletionFunc) {
      (mCompletionFunc)(a,NULL,mCompletionFuncArg);
   }
}


void PCIExpressTransaction::initializeTransactionSpecial(PCIExpressTransactor *xtor, const char* cmdstr) {
    // setup packets for special internal transactions (not user transactions)
    UInt32 linkWidth;   // link width of xtor
    UInt32 totalSets;   // total # sets in TS1+SKP+TS2
    UInt32 temp=0;
    UInt32 i, j, k;
    UInt32 *single_bytes_dat; // hold the single packets that will be turned into ordered sets
    bool   *single_bytes_sym; // hold the single packets that will be turned into ordered sets
    UInt32 *temp_bytes_dat; 
    bool   *temp_bytes_sym; 
    // char *ptr;
    bool useScramble;
    bool this_is_ts1, this_is_fc1, this_is_updatefc, this_is_ack;
    UInt32 numTSx;
    UInt32 singleTx;   // # of sets to make up one atomic transaction
    bool tsxUseLinkPad, tsxUseLanePad;
    UInt32 tsxNumLinkVal, tsxNumLaneVal;
    UInt32 HdrFC, DataFC, CRC_16b;
    UInt32 padLength = 0;
    char str[8];

    // do not replay DLLP transactions
    canNotReplay = true;

    useScramble = xtor->getScrambleStatus();
    linkWidth = xtor->getLinkWidth();

    if(xtor->getVerboseRxMed())
	printf("%s  Info: Adding Special Transaction: %s\n",xtor->getPrintName(), cmdstr);

    i = (linkWidth > 4) ? linkWidth : 4;
    temp_bytes_dat = new UInt32[i];  // need 'linkWidth' bytes mostly, but one case need 4
    temp_bytes_sym = new bool[i];    // so we need to alloc the greater of the two

    if((strncmp(cmdstr,"TS1 ",4) == 0) || (strncmp(cmdstr,"TS2 ",4) == 0)) {  // ################ Training Set TS1 or TS2 (x24)
	// Training Sequences are composed of ordered sets.  They are never
	// scrambled but are always 8b/10b encoded.

	// Valid Values:  TS(1|2) (PAD|[0-8]) (PAD|[0-8])

	if(strncmp(cmdstr,"TS1 ",4) == 0)
	    this_is_ts1 = true;
	else
	    this_is_ts1 = false;
	numTSx = 20;

	if(strncmp(cmdstr+4,"P",1) == 0) {
	    tsxUseLinkPad = true;
	    tsxNumLinkVal = 0;
	    if(strncmp(cmdstr+8,"P",1) == 0) {
		tsxUseLanePad = true;
		tsxNumLaneVal = 0;
	    } else {
		tsxUseLanePad = false;
		tsxNumLaneVal = atoi(cmdstr + 8);
	    }
	} else {
	    tsxUseLinkPad = false;
	    tsxNumLinkVal = atoi(cmdstr+4);
	    // FOR SOME REASON THE BELOW GIVES A SEGFLT INSTEAD OF POINTING TO THE LAST ATTRIBUTE
	    // ptr = (char*) strtok(cmdstr+4, " ");
	    // ptr = (char*) strtok(NULL, " ");
	    // if(strncmp(ptr,"P",1) == 0) {
	    if(strncmp(cmdstr+6,"P",1) == 0) {
		tsxUseLanePad = true;
		tsxNumLaneVal = 0;
	    } else if(strncmp(cmdstr+6,"x",1) == 0) {
		tsxUseLanePad = false;
		tsxNumLaneVal = 999;   // flag that we're doing the lane number on each lane, not a standard value
	    } else {
		tsxUseLanePad = false;
		// tsxNumLaneVal = atoi(ptr);
		tsxNumLaneVal = atoi(cmdstr+6);
	    }
	}
// printf("%s  Special TX: TS%d ", xtor->getPrintName(), (this_is_ts1 ? 1:2));
// if(tsxUseLinkPad) printf("P ");
// else              printf("%d ",tsxNumLinkVal);
// if(tsxUseLanePad) printf("P\n");
// else              printf("%d\n",tsxNumLaneVal);

        if(tsxUseLinkPad)
	    if(tsxUseLanePad)
		sprintf(printString, "TS%d Pad Pad", (this_is_ts1 ? 1:2));
	    else
		sprintf(printString, "TS%d Pad %d", (this_is_ts1 ? 1:2), tsxNumLaneVal);
	else
	    if(tsxUseLanePad)
		sprintf(printString, "TS%d %d Pad", (this_is_ts1 ? 1:2), tsxNumLinkVal);
	    else
		sprintf(printString, "TS%d %d %d", (this_is_ts1 ? 1:2), tsxNumLinkVal, tsxNumLaneVal);


	// Total packets: TS1 = 16 (ordered set) x 20 TS1s in a row
	//               ((16*20) * xtor->getLinkWidth) = 320 * LinkWidth
	totalSets = (16*numTSx);
	single_bytes_dat = new UInt32[totalSets];
  	single_bytes_dat = reinterpret_cast<UInt32*>(memset(single_bytes_dat, 0, sizeof(UInt32) * totalSets));
	single_bytes_sym = new bool[totalSets];
	single_bytes_sym = reinterpret_cast<bool*>(memset(single_bytes_sym, 0, sizeof(bool) * totalSets));

	if(useScramble)
	    temp = 0x00;    // are we setting the Disable Scramble Bit or not?
	else
	    temp = 0x08;

	for (i=0; i<numTSx; i++) {
	    for(j=0; j < linkWidth; j++) {
		single_bytes_dat[((i*16)+0)]  = PCIE_COM;    // COMMA for symbol alignment
		single_bytes_sym[((i*16)+0)]  = true;
	    }
	    if(tsxUseLinkPad) {
		for(j=0; j < linkWidth; j++) {
		    single_bytes_dat[((i*16)+1)]  = PCIE_PAD;         // All Links
		    single_bytes_sym[((i*16)+1)]  = true;
		}
	    } else {
		for(j=0; j < linkWidth; j++) {
		    single_bytes_dat[((i*16)+1)]  = tsxNumLinkVal;    // Link Number within component
		    single_bytes_sym[((i*16)+1)]  = false;
		}
	    }
	    if(tsxUseLanePad) {
		for(j=0; j < linkWidth; j++) {
		    single_bytes_dat[((i*16)+2)]  = PCIE_PAD;         // All Lanes
		    single_bytes_sym[((i*16)+2)]  = true;
		}
	    } else {
		if(tsxNumLaneVal == 999) {        // special case, encode specific lane num into packet on each lane
		    single_bytes_dat[((i*16)+2)] = 0;
		    single_bytes_sym[((i*16)+2)] = false;
		    for(j=0; j < linkWidth; j++) {
			temp_bytes_dat[j]  = j;                // Lane Number within Link
			temp_bytes_sym[j]  = false;
		    }
		} else {
		    for(j=0; j < linkWidth; j++) {
			single_bytes_dat[((i*16)+2)]  = tsxNumLaneVal;    // Lane Number within Link
			single_bytes_sym[((i*16)+2)]  = false;
		    }
		}
	    }
	    for(j=0; j < linkWidth; j++) {
		single_bytes_dat[((i*16)+3)]  = 0x8C;        // Number of Fast Training Ordered Sets needed to recover from sleep mode
		single_bytes_sym[((i*16)+3)]  = false;
		single_bytes_dat[((i*16)+4)]  = 0x02;        // Data Rate (must be 2)
		single_bytes_sym[((i*16)+4)]  = false;
		single_bytes_dat[((i*16)+5)]  = temp;        // Training Control (just asserting Disable Scrambling)
		single_bytes_sym[((i*16)+5)]  = false;
	    }
	    if(this_is_ts1) {
		for(j=6; j < 16; j++) {
		    for(k=0; k < linkWidth; k++) {
			single_bytes_dat[((i*16)+j)] = 0x4A;	 // TS1 Identifier (D10.2)
			single_bytes_sym[((i*16)+j)] = false;
		    }
		}
	    } else {
		for(j=6; j < 16; j++) {
		    for(k=0; k < linkWidth; k++) {
			single_bytes_dat[((i*16)+j)] = 0x45;	 // TS2 Identifier (D5.2)
			single_bytes_sym[((i*16)+j)] = false;
		    }
		}
	    }
	}

	allocPhysBytes(totalSets * linkWidth);

	// take single packets and make them ordered sets in phys_bytes
	for(i=0; i < totalSets; i++) {
	    for(j=0; j < linkWidth; j++) {
		phys_bytes_dat[((i*linkWidth)+j)] = single_bytes_dat[i];
		phys_bytes_sym[((i*linkWidth)+j)] = single_bytes_sym[i];
	    }
	}
	if(tsxNumLaneVal == 999) {          // special case
	    for(i=0; i < totalSets; i++) {
		if( (i%16) == 2 ) {         // need the packets at this step to be different
		    for(j=0; j < linkWidth; j++) {
			phys_bytes_dat[((i*linkWidth)+j)] = temp_bytes_dat[j];
			phys_bytes_sym[((i*linkWidth)+j)] = temp_bytes_sym[j];
		    }
		}
	    }
	}
	delete[] single_bytes_dat;
	delete[] single_bytes_sym;

	runOutputState = Link_Training;
	mMustStartLane0 = true;

    } else if(strcmp(cmdstr,"EIDLE") == 0) {                       // ################ Electrical IDLE (0)
	totalSets = 1;    // 0
	allocPhysBytes(totalSets * linkWidth);
	strcpy(printString, "Electrical Idle");

	// take single packets and make them ordered sets in phys_bytes
	for(i=0; i < totalSets; i++) {
	   for(j=0; j < linkWidth; j++) {
	      phys_bytes_dat[((i*linkWidth)+j)] = 0;  // NOT 8b10b ENCODE EITHER
	      phys_bytes_sym[((i*linkWidth)+j)] = false;
	   }
	}

	runOutputState = Electrical_Idle;
	mMustStartLane0 = true;
    } else if(strncmp(cmdstr,"IDLE ",5) == 0) {                    // ################ IDLE data sets - Num presented in the cmdstr
	// Expect string to be "IDLE 5" or "IDLE 100" to specify number of idle cycles
	if(strlen(cmdstr) < 6) {
	    totalSets = 1;
	} else {
	    totalSets = (UInt32) atoi(cmdstr+5);
	    if (totalSets < 1)
		totalSets = 1;
	}

	allocPhysBytes(totalSets * linkWidth);
	strcpy(printString, "Logical Idle");

	for(i=0; i < (totalSets * linkWidth); i++) {
	    phys_bytes_dat[i] = 0x00;
	    phys_bytes_sym[i] = false;
	}

	runOutputState = Logical_Idle;
	mMustStartLane0 = true;

    } else if(strcmp(cmdstr,"CLOCK_TOL") == 0) {                   // ################ Clock Tolerance Compensation sets
	totalSets = 4;    // COM, SKP, SKP, SKP
	allocPhysBytes(totalSets * linkWidth);
	strcpy(printString, "Clock Tolerance");

	// take single packets and make them ordered sets in phys_bytes
	for(i=0; i < totalSets; i++) {
	    for(j=0; j < linkWidth; j++) {
		phys_bytes_dat[((i*linkWidth)+j)] = ((i==0) ? PCIE_COM : PCIE_SKP);
		phys_bytes_sym[((i*linkWidth)+j)] = true;
	    }
	}

	runOutputState = Clock_Tolerance;
	mMustStartLane0 = true;

    } else if((strcmp(cmdstr,"InitFC1") == 0)||(strcmp(cmdstr,"InitFC2") == 0)||(strcmp(cmdstr,"UpdateFC") == 0)) { // ################ DLL InitFC1 sets
	// total packets = 3 DLLPs * 8 Bytes each = 24 Bytes

       if(strcmp(cmdstr,"InitFC1") == 0) {
	  this_is_fc1 = true;
	  this_is_updatefc = false;
	  numTSx = 15;   // during Init we run 15 sets
       } else if(strcmp(cmdstr,"InitFC2") == 0) {
	  this_is_fc1 = false;
	  this_is_updatefc = false;
	  numTSx = 15;   // during Init we run 15 sets
       } else {
	  this_is_fc1 = false;
	  this_is_updatefc = true;
	  numTSx = 1;   // for incremental updates do 1 set
       }

	if (linkWidth > 8) {
	   // since only one SDP can be on the link in a single symbol time (clock cycle)
	   // we must pad each transaction if the link width is more than the length of
	   // the DLLP transactions.
	   padLength = linkWidth - 8;  // transaction is 8 symbols long, which fits evenly on 1,2,4,8 wide lanes
	                               // but needs padding on 12,16,32 wide lanes
	}
	totalSets = (((8+padLength)*3)*numTSx);  // 3 8-byte transactions plus padding, sent 15 times

	single_bytes_dat = new UInt32[8];  // single transaction = 8 bytes
	single_bytes_dat = reinterpret_cast<UInt32*>(memset(single_bytes_dat, 0, sizeof(UInt32) * 8));
	single_bytes_sym = new bool[8];
	single_bytes_sym = reinterpret_cast<bool*>(memset(single_bytes_sym, 0, sizeof(bool) * 8));

	allocPhysBytes(totalSets);

	for(i=0; i<3; i++) {
	    // Use Infinite Credit Flow Control (instead of sending UpdateFC's all the time
	    // switch(i) {
	    // case 0: HdrFC=0x20;
	    //         DataFC=0x0C0;
	    //         break;
	    // case 1: HdrFC=0x10;
	    //         DataFC=0x000;
	    // 	       break;
	    // case 2: HdrFC=0x00;
	    //         DataFC=0x000;
	    // 	       break;
	    // default: HdrFC = 0;
	    //         DataFC = 0;
	    // 	       break;
	    // }
	    HdrFC = 0x00;
	    DataFC = 0x000;


	    // Add SDP
	    single_bytes_dat[0] = PCIE_SDP;
	    single_bytes_sym[0] = true;
	    // Add DLLP Type
	    single_bytes_dat[1] = (((this_is_fc1) ? 0x40 : ((this_is_updatefc) ? 0x80 : 0xC0)) + (i*0x10));  // 40, 50, 60 / 80, 90, A0 / C0, D0, E0  // set cmd, virtual channel 0
	    single_bytes_sym[1] = false;
	    // Add HdrFC
	    single_bytes_dat[2] = (HdrFC >> 2) & 0x3F;  // {2'b00,HdrFC[7:2]}
	    single_bytes_sym[2] = false;
	    // Add DataFC
	    single_bytes_dat[3] = ((HdrFC << 6) & 0xC0) + ((DataFC >> 8) & 0x0F);  // {HdrFC[1:0],2'h0,DataFC[11:8]}
	    single_bytes_sym[3] = false;
	    single_bytes_dat[4] = (DataFC & 0xFF); // DataFC[8:0]
	    single_bytes_sym[4] = false;
	    // Add 16-bit CRC
	    for(j=0; j<4; j++)
		temp_bytes_dat[j] = single_bytes_dat[j+1];
	    CRC_16b = xtor->calc16bCRC(temp_bytes_dat,4);  // generate 16-bit CRC
//  printf("MIKE MIKE MIKE MIKE: InitFC%d: 0x%X 0x%X 0x%X 0x%X (CRC=0x%X)\n", (this_is_fc1)?1:2, temp_bytes_dat[0], temp_bytes_dat[1], temp_bytes_dat[2], temp_bytes_dat[3], CRC_16b);
	    single_bytes_dat[5] = ((CRC_16b >> 8) & 0xFF);  // CRC[15:8]
	    single_bytes_sym[5] = false;
	    single_bytes_dat[6] = (CRC_16b & 0xFF);         // CRC[7:0]
	    single_bytes_sym[6] = false;
	    // Add END
	    single_bytes_dat[7] = PCIE_END;
	    single_bytes_sym[7] = true;

	    for(j=0; j<8; j++) {
		phys_bytes_dat[((i*(8+padLength))+j)] = (single_bytes_dat[j] & 0xFF); // put first set of three
		phys_bytes_sym[((i*(8+padLength))+j)] = single_bytes_sym[j];          // transactions here
	    }
	    if (padLength > 0) {
	       for (j=8; j < (8+padLength); j++) {
		  phys_bytes_dat[((i*(8+padLength))+j)] = PCIE_PAD;
		  phys_bytes_sym[((i*(8+padLength))+j)] = true;
	       }
	    }
	}

	delete[] single_bytes_dat;
	delete[] single_bytes_sym;

	for(i=1; i<numTSx; i++) {
	    for(j=0; j < ((8+padLength)*3); j++) {
		phys_bytes_dat[((i*((8+padLength)*3))+j)] = phys_bytes_dat[j];
		phys_bytes_sym[((i*((8+padLength)*3))+j)] = phys_bytes_sym[j];
	    }
	}

	if(this_is_fc1) {
	   strcpy(printString, "DLLP InitFC1");
	} else if(this_is_updatefc) {
	   strcpy(printString, "DLLP UpdateFC");
	} else {
	   strcpy(printString, "DLLP InitFC2");
	}

	if (not this_is_updatefc)
	   runOutputState = DLL_Setup;
	mMustStartLane0 = true;

    } else if((strncmp(cmdstr,"DLLP_Ack ",9) == 0) || (strncmp(cmdstr,"DLLP_Nak ",9) == 0)) {  // ################ DLLP Ack or Nak of a TLP
	// total packets = 1 DLLP * 8 Bytes

	if(strncmp(cmdstr,"DLLP_Ack ",9) == 0) {
	    this_is_ack = true;
	    strcpy(str,cmdstr+9);
	    mIsAck = true;
	} else {
	    this_is_ack = false;
	    strcpy(str,cmdstr+10);
	    mIsNak = true;
	}
	temp = atoi(str);
//x  std::cout << "Creating a DLLP_" << ((this_is_ack)?"Ack":"Nak") <<  " with Sequence Number " << temp << ";  (cmd was '" << cmdstr << "')";

	totalSets = 8;  // 8 packets
	allocPhysBytes(totalSets);

	// Add SDP
	phys_bytes_dat[0] = PCIE_SDP;
	phys_bytes_sym[0] = true;
	// Add DLLP Type
	phys_bytes_dat[1] = ((this_is_ack) ? 0x00:0x10);
	phys_bytes_sym[1] = false;
	// Reserved
	phys_bytes_dat[2] = 0x00;
	phys_bytes_sym[2] = false;
	// AckNak_Seq_Num
	phys_bytes_dat[3] = ((temp >> 8) & 0x0F);  // seq_num[11:8]
	phys_bytes_sym[3] = false;
	phys_bytes_dat[4] = (temp & 0xFF);         // seq_num[7:0]
	phys_bytes_sym[4] = false;

	// Add 16-bit CRC
	for(j=0; j<4; j++)
	    temp_bytes_dat[j] = phys_bytes_dat[j+1];
	CRC_16b = xtor->calc16bCRC(temp_bytes_dat,4);  // generate 16-bit CRC

	phys_bytes_dat[5] = ((CRC_16b >> 8) & 0xFF);  // CRC[15:8]
	phys_bytes_sym[5] = false;
	phys_bytes_dat[6] = (CRC_16b & 0xFF);         // CRC[7:0]
	phys_bytes_sym[6] = false;
	// Add END
	phys_bytes_dat[7] = PCIE_END;
	phys_bytes_sym[7] = true;

	if(this_is_ack)
	    strcpy(printString, "DLLP Ack");
	else
	    strcpy(printString, "DLLP Nak");

    } else if(strcmp(cmdstr,"POWER_SAVE") == 0) {                       // ################ Power Save IDLE data set
	totalSets = 4;    // COM, IDL, IDL, IDL
	// totalSets++;      // Need 0's to end this special case

	allocPhysBytes(totalSets * linkWidth);
	strcpy(printString, "Power Save");

	// take single packets and make them ordered sets in phys_bytes
	for(i=0; i < totalSets; i++) {
	   // if(i != (totalSets - 1)) {
	      for(j=0; j < linkWidth; j++) {
		 phys_bytes_dat[((i*linkWidth)+j)] = ((i==0) ? PCIE_COM : PCIE_IDL);
		 phys_bytes_sym[((i*linkWidth)+j)] = true;
	      }
	   // } else {
	   //   for(j=0; j < linkWidth; j++) {
	   //	 phys_bytes_dat[((i*linkWidth)+j)] = 0x00;
	   //	 phys_bytes_sym[((i*linkWidth)+j)] = false;
	   //   }
	   // }
	}

	runOutputState = Power_Save;
	mMustStartLane0 = true;

    } else if(strcmp(cmdstr,"FTS") == 0) {                         // ################ Fast Training Set
	singleTx = 4;        // COM, FTS, FTS, FTS
        numTSx = 142;        // have to do them long enough for sync
	totalSets = (singleTx * numTSx);

	single_bytes_dat = new UInt32[singleTx];
	single_bytes_dat = reinterpret_cast<UInt32*>(memset(single_bytes_dat, 0, sizeof(UInt32) * singleTx));
	single_bytes_sym = new bool[singleTx];
	single_bytes_sym = reinterpret_cast<bool*>(memset(single_bytes_sym, 0, sizeof(bool) * singleTx));

	allocPhysBytes(totalSets * linkWidth);
	strcpy(printString, "Fast Training");

	// take single packets and make them ordered sets in phys_bytes
	for(i=0; i < singleTx; i++) {
	   single_bytes_dat[i] = ((i==0) ? PCIE_COM : PCIE_FTS);
	   single_bytes_sym[i] = true;
	}
	for(i=0; i < totalSets; i++) {
	   for(j=0; j < linkWidth; j++) {
	      phys_bytes_dat[((i*linkWidth)+j)] = single_bytes_dat[(i%singleTx)];
	      phys_bytes_sym[((i*linkWidth)+j)] = single_bytes_sym[(i%singleTx)];
	   }
	}

	delete[] single_bytes_dat;
	delete[] single_bytes_sym;

	runOutputState = Fast_Training;
	mMustStartLane0 = true;
    }

    delete[] temp_bytes_dat;
    delete[] temp_bytes_sym;
    return;
}

void PCIExpressTransaction::initializeTransaction(PCIExpressTransactor *xtor, PCIECmd *cmd) {
    // move stuff here, do all of the tlp, dllp, phys packet work here instead
    UInt32 tlp_bytes_hdr[16];
    UInt32 tlp_bytes_hdr_used;
    UInt32 tlp_bytes_total;   // # bytes total for the TLP (header + data) [no digest currently]
    UInt32 *dllp_bytes;       //  dynamic storage of the DLLP bytes
    UInt32 dllp_bytes_total;  // # bytes total for the DLLP (header, TLP, footer)
    UInt32 linkWidth;
    bool useScramble;
    UInt32 i;

    // PCIE 1.0a spec 2.2.9. Completion Rules:
    // All Read Requests and Non-Posted Write Requests require Completion.
    // (Mem Writes are posted, Cfg & IO Writes are not.)
    switch(cmd->getCmdByName()) {
    case PCIECmd::MRd32:   strcpy(printString, "Mem Read 32-bit");             fcType=NonPosted;  break;
    case PCIECmd::MRd64:   strcpy(printString, "Mem Read 64-bit");             fcType=NonPosted;  break;
    case PCIECmd::MRdLk32: strcpy(printString, "Mem Read Lock 32-bit");        fcType=NonPosted;  break;
    case PCIECmd::MRdLk64: strcpy(printString, "Mem Read Lock 64-bit");        fcType=NonPosted;  break;
    case PCIECmd::MWr32:   strcpy(printString, "Mem Write 32-bit");            fcType=Posted;     break;
    case PCIECmd::MWr64:   strcpy(printString, "Mem Write 64-bit");            fcType=Posted;     break;
    case PCIECmd::IORd:    strcpy(printString, "IO Read");                     fcType=NonPosted;  break;
    case PCIECmd::IOWr:    strcpy(printString, "IO Write");                    fcType=NonPosted;  break;
    case PCIECmd::CfgRd0:  strcpy(printString, "Config Read Type 0");          fcType=NonPosted;  break;
    case PCIECmd::CfgWr0:  strcpy(printString, "Config Write Type 0");         fcType=NonPosted;  break;
    case PCIECmd::CfgRd1:  strcpy(printString, "Config Read Type 1");          fcType=NonPosted;  break;
    case PCIECmd::CfgWr1:  strcpy(printString, "Config Write Type 1");         fcType=NonPosted;  break;
    case PCIECmd::Msg:     strcpy(printString, "Message");                     fcType=Posted;     break;
    case PCIECmd::MsgD:    strcpy(printString, "Message with Data");           fcType=Posted;     break;
    case PCIECmd::Cpl:     strcpy(printString, "Completion");                  fcType=Completion; break;
    case PCIECmd::CplD:    strcpy(printString, "Completion with Data");        fcType=Completion; break;
    case PCIECmd::CplLk:   strcpy(printString, "Completion Locked");           fcType=Completion; break;
    case PCIECmd::CplDLk:  strcpy(printString, "Completion Locked with Data"); fcType=Completion; break;
    default:               strcpy(printString, "Unknown");
    }

    // all 'regular' transactions need to be Acknowledged by the receiver.
    expectAck = true;
    // all TLP transactions take one flow control header unit
    fcHeader = 1;
    // posted transactions don't need completions, nor are they completions
    // nonposted transactions need completions
    // completions are completions
    switch(fcType) {
    case Posted:
       expectCpl = false;
       mIsCpl = false;
       break;
    case NonPosted:
       expectCpl = true;
       mIsCpl = false;
       break;
    case Completion:
       expectCpl = false;
       mIsCpl = true;
       break;
    default:
       break;
    }

    // Transaction Layer Protocol Packet Definition (Generic)
    //unused -  UInt32 mTLP_Fmt;    // Format[1:0]
    //unused -  UInt32 mTLP_Type;   // Type[4:0]
    UInt32 mTLP_TC;     // Traffic Class[2:0]
    UInt32 mTLP_TD;     // TLP Digest Present
    UInt32 mTLP_EP;     // TLP Error - Poisoned TLP
    UInt32 mTLP_Attr;   // Attributes[1:0]
    UInt32 mTLP_Len;    // Length[9:0] - length of data payload in 4-byte Double Words
    // Transaction Layer Protocol Packet Definition (Extended - Memory, I/O, & Configuration Request)
    UInt32 mTLP_ReqID;  // Requester ID[15:0]
    UInt32 mTLP_Tag;    // Tag[7:0]
    UInt32 mTLP_LDWBE;  // Last Double Word Byte Enable[3:0]
    UInt32 mTLP_FDWBE;  // First Double Word Byte Enable[3:0]
    UInt32 mTLP_AddrHi; // Address bits [63:32]
    UInt32 mTLP_AddrLo; // Address bits [31:00]
    // Transaction Layer Protocol Packet Definition (Extended - Completion)
    UInt32 mTLP_CompID; // Completer ID[15:0]
    UInt32 mTLP_CompSt; // Completion Status[2:0]
    UInt32 mTLP_BCM;    // Byte Count Modified (ONLY by PCIX Completers, not set by PCIExpress Completers)
    UInt32 mTLP_BC;     // Byte Count[11:0] - remaining byte count for the request
    UInt32 mTLP_LowAd;  // Lower Address[6:0] - lower byte address for starting byte of Completion
    // Transaction Layer Protocol Packet Definition (Extended - Messages)
    UInt32 mTLP_msgRout; // Message Routing Type[2:0]
    UInt32 mTLP_msgCode; // Message Code[7:0]
    ////////////////////////////////////////////

    //unused - mTLP_Fmt  = cmdParseTlpFmt(cmd->tlp_type);
    //unused - mTLP_Type = cmdParseTlpType(cmd->tlp_type);

// TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED 
    // we don't currently use multiple Traffic Classes      // TO BE COMPLETED     TO BE COMPLETED
    mTLP_TC = 0;    // TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED
    // we don't currently support sending a TLP Digest      // TO BE COMPLETED     TO BE COMPLETED
    mTLP_TD = 0;    // TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED
    // we don't currently support sending Poisoned Packets  // TO BE COMPLETED     TO BE COMPLETED
    mTLP_EP = 0;    // TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED
    // we don't currently support any Packet Attributes     // TO BE COMPLETED     TO BE COMPLETED
    mTLP_Attr = 0;  // TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED
// TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED 

    if((cmd->getCmdByName() == PCIECmd::IORd)   || (cmd->getCmdByName() == PCIECmd::IOWr)   ||
       (cmd->getCmdByName() == PCIECmd::CfgRd0) || (cmd->getCmdByName() == PCIECmd::CfgWr0) || 
       (cmd->getCmdByName() == PCIECmd::CfgRd1) || (cmd->getCmdByName() == PCIECmd::CfgWr1)) {
	mTLP_Len  = 0x01;  // Must be 1 DW
	mDataLength = 1;
    } else if((cmd->getCmdByName() == PCIECmd::Msg) || (cmd->getCmdByName() == PCIECmd::Cpl) ||
	      (cmd->getCmdByName() == PCIECmd::CplLk)) {
	mTLP_Len  = 0x00;  // No data
	mDataLength = 0;
    } else {
       //       if(cmd->getDWCount() >= 1024)  // max Data Payload = 1024 DWords
       if(cmd->getDataLength() >= 1024) {  // max Data Payload = 1024 DWords
	  mTLP_Len = 0;   // 0 = 1024 DWords
	  mDataLength = 1024;
       } else {
	  //	  mTLP_Len = cmd->getDWCount();  // set Length of Data in 4-byte Double Words
	  mTLP_Len = cmd->getDataLength();  // set Length of Data in 4-byte Double Words
	  mDataLength = mTLP_Len;
       }
    }

    // Requester ID[15:0] = {mBusNum[7:0],mDevNum[4:0],mFuncNum[2:0]}
    if((cmd->getCmdByName() == PCIECmd::Cpl)   || (cmd->getCmdByName() == PCIECmd::CplD) || 
       (cmd->getCmdByName() == PCIECmd::CplLk) || (cmd->getCmdByName() == PCIECmd::CplDLk) ||
       (cmd->getCmdByName() == PCIECmd::Msg)   || (cmd->getCmdByName() == PCIECmd::MsgD)) {
	mTLP_ReqID = cmd->getReqId();
	mTLP_Tag = cmd->getTag();
    } else {
	mTLP_ReqID = ((xtor->getBusNum() << 8) + (xtor->getDevNum() << 3) + xtor->getFuncNum());
	mTLP_Tag = xtor->getUniqueTag();   // xtor specific unique tag for this transaction
	//x std::cout << "MIKE, I'm Getting a new tag here!! (got seq #" << mTLP_Tag << " for a 0x" << cmd->getCmdByName() << " xaction)" << std::endl;
    }
    transID = ((mTLP_ReqID & 0xFFFF) << 8) | (mTLP_Tag & 0x00FF);  // Save the Transaction ID

    UInt64 tempAddr = cmd->getAddr();
    mTLP_AddrHi = (tempAddr >> 32) & 0xFFFFFFFF;
    mTLP_AddrLo = tempAddr & 0xFFFFFFFF;

    mTLP_FDWBE = cmd->getFirstBE();
    mTLP_LDWBE = cmd->getLastBE();

//     std::cout << "MIKE MIKE: AddrLo=0x" << std::hex << unsigned(mTLP_AddrLo)
// 	      << ", FDWBE=0x" << unsigned(mTLP_FDWBE)
// 	      << ", LDWBE=0x" << unsigned(mTLP_LDWBE)
// 	      << std::endl;

    if((cmd->getCmdByName() == PCIECmd::Cpl) || (cmd->getCmdByName() == PCIECmd::CplD) || 
       (cmd->getCmdByName() == PCIECmd::CplLk) || (cmd->getCmdByName() == PCIECmd::CplDLk)) {
	mTLP_CompID = cmd->getCompId();
    } else {
	mTLP_CompID = 0;
    }

    mTLP_CompSt = cmd->getCplStatus();

    mTLP_BCM = 0;  // MBZ for PCIExpress Completers (only used for PCIX Completers)
    
    /* 4 * len is not correct for completions of MEM Reads
    ** for the moment, I'm gonna let this get overriden by the callback routine,
    ** but the RIGHT thing to do would be to know what it is based on the request
    */
    mTLP_BC = cmd->getByteCount();
    mTLP_LowAd = cmd->getLowAddr();

    mTLP_msgRout = cmd->getMsgRout();
    mTLP_msgCode = cmd->getMsgCode();

    // first:
    //   create TLP packet bytes
    // second:
    //   update into DLLP packets (add header & CRC)
    // third:
    //   change into Phys packets (encode & scrable)
    // return:
    //   single 10bit packet of data to go onto bus

    ///////////////////////////////////////////////////////////////
    //          1)    Create TLP Packets                         //
    ///////////////////////////////////////////////////////////////

    for(i=0; i<16; i++)
	tlp_bytes_hdr[i] = 0;
                             // BYTE_0[7:0] = {1'b0, mTLP_Fmt[1:0], mTLP_Type[4:0]}
    if(   (cmd->getCmdByName() == PCIECmd::Msg)
       || (cmd->getCmdByName() == PCIECmd::MsgD)
       ) {
	// Command Type + Messge Routing
	tlp_bytes_hdr[0] = ((cmd->getCmdByName() & 0x78) + (cmd->getMsgRout()));
    } else {
	// Command Type
	tlp_bytes_hdr[0] = (cmd->getCmdByName() & 0x7F);
    }
                             // BYTE_1[7:0] = {1'b0, mTLP_TC[2:0], 4'b0000}
    tlp_bytes_hdr[1] = ((mTLP_TC & 0x07) << 4);
                             // BYTE_2[7:0] = {mTLP_TD, mTLP_EP, mTLP_Attr[1:0], 2'b00, mTLP_Len[9:8]}
    tlp_bytes_hdr[2] = (((mTLP_TD & 0x01) << 7) + ((mTLP_EP & 0x01) << 6) + ((mTLP_Attr & 0x03) << 4) + ((mTLP_Len >> 8) & 0x03));
                             // BYTE_3[7:0] = {mTLP_Len[7:0]}
    tlp_bytes_hdr[3] = (mTLP_Len & 0xFF);

    if(   (cmd->getCmdByName() == PCIECmd::Cpl)
       || (cmd->getCmdByName() == PCIECmd::CplD)
       || (cmd->getCmdByName() == PCIECmd::CplLk)
       || (cmd->getCmdByName() == PCIECmd::CplDLk)
       ) {            // Completions
	
      /*
      ** Mark all completions successful...
      */
	// Completer ID
	tlp_bytes_hdr[4] = (mTLP_CompID >> 8) & 0xFF;
	tlp_bytes_hdr[5] = mTLP_CompID & 0xFF;
	// Completion status, Byte Count Mask, Byte Count Remaining
	tlp_bytes_hdr[6] = ((mTLP_CompSt << 5) & 0xE0) | ((mTLP_BCM << 4) & 0x10) | ((mTLP_BC >> 8) & 0x0F);
	tlp_bytes_hdr[7] = mTLP_BC & 0xFF;
	// Requester ID 
	tlp_bytes_hdr[8] = (mTLP_ReqID >> 8) & 0xFF;
	tlp_bytes_hdr[9] = mTLP_ReqID & 0xFF;
	// TAG 
	tlp_bytes_hdr[10] = mTLP_Tag;
	tlp_bytes_hdr[11] = mTLP_LowAd & 0x7F;

	tlp_bytes_hdr_used = 12;
      
    } else {
	// Requester ID 
	tlp_bytes_hdr[4] = ((mTLP_ReqID >> 8) & 0xFF);
	tlp_bytes_hdr[5] = (mTLP_ReqID & 0xFF);
	// Tag
	tlp_bytes_hdr[6] = (mTLP_Tag & 0xFF);

	if(   (cmd->getCmdByName() == PCIECmd::Msg)
	   || (cmd->getCmdByName() == PCIECmd::MsgD)
	   ) {
	    // Message Code
	    tlp_bytes_hdr[7] = mTLP_msgCode;
	} else {
	    // Last & First Double Word Byte Enables
	    tlp_bytes_hdr[7] = (((mTLP_LDWBE & 0x0F) << 4) + (mTLP_FDWBE & 0x0F));
	}

	if(   (cmd->getCmdByName() == PCIECmd::MRd64)
	   || (cmd->getCmdByName() == PCIECmd::MRdLk64)
	   || (cmd->getCmdByName() == PCIECmd::MWr64)
	   ) {            // 64-bit address transaction
	                      // BYTE_8  = mAddressHi[31:24] = Address[63:56]
	    tlp_bytes_hdr[8]  = ((mTLP_AddrHi >> 24) & 0xFF);
	                      // BYTE_9  = mAddressHi[23:16] = Address[55:48]
	    tlp_bytes_hdr[9]  = ((mTLP_AddrHi >> 16) & 0xFF);
	                      // BYTE_10 = mAddressHi[15:08] = Address[47:40]
	    tlp_bytes_hdr[10] = ((mTLP_AddrHi >> 8) & 0xFF);
	                      // BYTE_11 = mAddressHi[07:02] = Address[39:32]
	    tlp_bytes_hdr[11] = (mTLP_AddrHi & 0xFF);
	                      // BYTE_12 = mAddressLo[31:24] = Address[31:24]
	    tlp_bytes_hdr[12] = ((mTLP_AddrLo >> 24) & 0xFF);
	                      // BYTE_13 = mAddressLo[23:16] = Address[23:16]
	    tlp_bytes_hdr[13] = ((mTLP_AddrLo >> 16) & 0xFF);
	                      // BYTE_14 = mAddressLo[15:08] = Address[16:08]
	    tlp_bytes_hdr[14] = ((mTLP_AddrLo >> 8) & 0xFF);
	                      // BYTE_15 = mAddressLo[07:02] = Address[07:02]
	    tlp_bytes_hdr[15] = (mTLP_AddrLo & 0xFC);

	    tlp_bytes_hdr_used = 16;
	} else if ( cmd->getCmdByName() == PCIECmd::Msg) {
	   tlp_bytes_hdr_used = 16;
	   tlp_bytes_hdr[8] = 0;
	   tlp_bytes_hdr[9] = 0;
	   tlp_bytes_hdr[10] = 0;
	   tlp_bytes_hdr[11] = 0;
	   tlp_bytes_hdr[12] = 0;
	   tlp_bytes_hdr[13] = 0;
	   tlp_bytes_hdr[14] = 0;
	   tlp_bytes_hdr[15] = 0;
	} else {          // 32-bit address transaction
	                      // BYTE_8  = mAddressLo[31:24] = Address[31:24]
	    tlp_bytes_hdr[8]  = ((mTLP_AddrLo >> 24) & 0xFF);
	                      // BYTE_9  = mAddressLo[23:16] = Address[23:16]
	    tlp_bytes_hdr[9]  = ((mTLP_AddrLo >> 16) & 0xFF);
	                      // BYTE_10 = mAddressLo[15:08] = Address[16:08]
	    tlp_bytes_hdr[10] = ((mTLP_AddrLo >> 8) & 0xFF);
	                      // BYTE_11 = mAddressLo[07:02] = Address[07:02]
	    tlp_bytes_hdr[11] = (mTLP_AddrLo & 0xFC);

	    tlp_bytes_hdr_used = 12;
	}
    }

    if ((cmd->getCmdByName() == PCIECmd::IOWr)   ||
	(cmd->getCmdByName() == PCIECmd::CfgWr0) ||
	(cmd->getCmdByName() == PCIECmd::CfgWr1)
       ) {
       fcData = 1;
    } else if ((cmd->getCmdByName() == PCIECmd::MWr32)  ||
	       (cmd->getCmdByName() == PCIECmd::MWr64)  ||
	       (cmd->getCmdByName() == PCIECmd::MsgD)   ||
	       (cmd->getCmdByName() == PCIECmd::CplD)   ||
	       (cmd->getCmdByName() == PCIECmd::CplDLk)
	      ) {
       // Flow Control Data Credit Unit is 4 DWs (PCIE rev1.0a, sec 2.6.1)
       fcData = ((mDataLength + 3) / 4);
    } else {
       fcData = 0;
    }

    // Add space for data payload, but only if it's a write or completion with data!
    if ((cmd->getCmdByName() == PCIECmd::MWr32)  || (cmd->getCmdByName() == PCIECmd::MWr64)  ||
	(cmd->getCmdByName() == PCIECmd::IOWr)   || (cmd->getCmdByName() == PCIECmd::CfgWr0) ||
	(cmd->getCmdByName() == PCIECmd::CfgWr1) || (cmd->getCmdByName() == PCIECmd::MsgD)   ||
	(cmd->getCmdByName() == PCIECmd::CplD)   || (cmd->getCmdByName() == PCIECmd::CplDLk)
       )
      tlp_bytes_total = tlp_bytes_hdr_used + (mTLP_Len * 4);
    else
      tlp_bytes_total = tlp_bytes_hdr_used;


    ///////////////////////////////////////////////////////////////
    //          2)    Create DLLP Packets                        //
    ///////////////////////////////////////////////////////////////
    dllp_bytes_total = tlp_bytes_total + 6;  // DLLP header = 2, DLLP footer (LCRC) = 4

    dllp_bytes = new UInt32[dllp_bytes_total];
    dllp_bytes = reinterpret_cast<UInt32*>(memset(dllp_bytes, 0, sizeof(UInt32) * dllp_bytes_total));

    // Can't add SeqNum until the tranasction is ready to be put across the bus, otherwise we can't
    //   ensure that the sequence numbers go in order.
    //m DLLP_SeqNum = xtor->getNewDLLPSeqNum();  // get a new Sequence Number from transactor
    //m dllp_bytes[0] = ((DLLP_SeqNum >> 8) & 0x0F);
    //m dllp_bytes[1] = (DLLP_SeqNum & 0x0FF);
    dllp_bytes[0] = 0;
    dllp_bytes[1] = 0;

    for(i=0; i < tlp_bytes_hdr_used; i++) {
	dllp_bytes[(i+2)] = tlp_bytes_hdr[i];
    }

    // Add data payload, but only if it's a write or completion with data!
    if ((cmd->getCmdByName() == PCIECmd::MWr32)  || (cmd->getCmdByName() == PCIECmd::MWr64)  ||
	(cmd->getCmdByName() == PCIECmd::IOWr)   || (cmd->getCmdByName() == PCIECmd::CfgWr0) ||
	(cmd->getCmdByName() == PCIECmd::CfgWr1) || (cmd->getCmdByName() == PCIECmd::MsgD)   ||
	(cmd->getCmdByName() == PCIECmd::CplD)   || (cmd->getCmdByName() == PCIECmd::CplDLk)
       ) {
       for (UInt32 j = 0; j < mDataLength; j++) {
	  dllp_bytes[tlp_bytes_hdr_used + 2 + (j*4) + 0] = cmd->getDataByte(j*4 + 0);
	  dllp_bytes[tlp_bytes_hdr_used + 2 + (j*4) + 1] = cmd->getDataByte(j*4 + 1);
	  dllp_bytes[tlp_bytes_hdr_used + 2 + (j*4) + 2] = cmd->getDataByte(j*4 + 2);
	  dllp_bytes[tlp_bytes_hdr_used + 2 + (j*4) + 3] = cmd->getDataByte(j*4 + 3);
       }
    }

    // Can't add CRC until the SeqNum is established.  Calculate & add it when SeqNum is written.
    //m temp = xtor->calc32bCRC(dllp_bytes,dllp_bytes_total-4);  // generate 32-bit LCRC
    //m dllp_bytes[dllp_bytes_total-4] = ((temp >> 24) & 0xFF);   // most significant bits
    //m dllp_bytes[dllp_bytes_total-3] = ((temp >> 16) & 0xFF);
    //m dllp_bytes[dllp_bytes_total-2] = ((temp >>  8) & 0xFF);
    //m dllp_bytes[dllp_bytes_total-1] = (temp & 0xFF);           // least significant bits
    dllp_bytes[dllp_bytes_total-4] = 0;
    dllp_bytes[dllp_bytes_total-3] = 0;
    dllp_bytes[dllp_bytes_total-2] = 0;
    dllp_bytes[dllp_bytes_total-1] = 0;

// for(i=0; i<dllp_bytes_total; i++) {
//     std::cout << i << ") " << dllp_bytes[i] << std::endl;
// }
    ///////////////////////////////////////////////////////////////
    //          3)    Create Phys Packets                        //
    ///////////////////////////////////////////////////////////////
    linkWidth = xtor->getLinkWidth();
    useScramble = xtor->getScrambleStatus();
    allocPhysBytes(dllp_bytes_total + 2);

    phys_bytes_dat[0] = PCIE_STP;
    phys_bytes_sym[0] = true;
    for(i=0; i < dllp_bytes_total; i++) {
	phys_bytes_dat[i+1] = dllp_bytes[i];
	phys_bytes_sym[i+1] = false;
    }
    phys_bytes_dat[dllp_bytes_total+1] = PCIE_END;
    phys_bytes_sym[dllp_bytes_total+1] = true;

    delete[] dllp_bytes;
    return;
}

void PCIExpressTransaction::replayTransaction(void) {
   if (canNotReplay) {
                  // don't replay DLLPs
      return;
   } else if ((isStarted()) && (not isDone())) {
                  // wait until transaction-in-progress is done before replaying
      doReplay = true;
   } else {
      mBytesSent = 0;
      mDone = false;
      if (not mIsSpecial)
	 expectAck = true;
      numRetries++;
      doReplay = false;
   }
   return;
}


PCIECmd::PCIECmdType PCIExpressTransaction::getTlpTypeFromStr(char *str)
{
    PCIECmd::PCIECmdType tlp_type;

    if (!strcmp(str, "MEMREAD32"))
	tlp_type = PCIECmd::MRd32;
    else if (!strcmp(str, "MEMREAD64"))
	tlp_type = PCIECmd::MRd64;
    else if (!strcmp(str, "MEMREADLOCK32"))
	tlp_type = PCIECmd::MRdLk32;
    else if (!strcmp(str, "MEMREADLOCK64"))
	tlp_type = PCIECmd::MRdLk64;
    else if (!strcmp(str, "MEMWRITE32"))
	tlp_type = PCIECmd::MWr32;
    else if (!strcmp(str, "MEMWRITE64"))
	tlp_type = PCIECmd::MWr64;
    else if (!strcmp(str, "IOREAD"))
	tlp_type = PCIECmd::IORd;
    else if (!strcmp(str, "IOWRITE"))
	tlp_type = PCIECmd::IOWr;
    else if (!strcmp(str, "CFGREAD0"))
	tlp_type = PCIECmd::CfgRd0;
    else if (!strcmp(str, "CFGWRITE0"))
	tlp_type = PCIECmd::CfgWr0;
    else if (!strcmp(str, "CFGREAD1"))
	tlp_type = PCIECmd::CfgRd1;
    else if (!strcmp(str, "CFGWRITE1"))
	tlp_type = PCIECmd::CfgWr1;
    else if (!strcmp(str, "MSG"))
	tlp_type = PCIECmd::Msg;
    else if (!strcmp(str, "MSGD"))
	tlp_type = PCIECmd::MsgD;
    else if (!strcmp(str, "COMPL"))
	tlp_type = PCIECmd::Cpl;
    else if (!strcmp(str, "COMPLD"))
	tlp_type = PCIECmd::CplD;
    else if (!strcmp(str, "COMPLLOCK"))
	tlp_type = PCIECmd::CplLk;
    else if (!strcmp(str, "COMPLDLOCK"))
	tlp_type = PCIECmd::CplDLk;
    else {
	printf("ERROR: Received a bad command for a transaction:\n  \"%s\"\n", str);
	tlp_type = PCIECmd::MRd32;
    }

    return tlp_type;
}

void PCIExpressTransaction::setSeqNumAndCRC(PCIExpressTransactor *xtor) {
   UInt32 lcrc = 0;
   SInt32 pos = 0;

   // Write correct Sequence Number
   DLLP_SeqNum = xtor->getNewDLLPSeqNum();  // get a new Sequence Number from transactor
   phys_bytes_dat[1] = ((DLLP_SeqNum >> 8) & 0x0F);
   phys_bytes_dat[2] = (DLLP_SeqNum & 0xFF);

   // find position to write the CRC (last 4 bytes are LCRC, then 'END', then 0+ 'PAD', so insert before 'END')
   for (pos = mBytesTotal-1; pos > 0; --pos) {
      if ((phys_bytes_dat[pos] == PCIE_END) && (phys_bytes_sym[pos] == true)) {
	 pos = pos - 4;  // move back 4 positions to the start of the 
	 break;
      }
   }

   if (pos > 0) {  // if pos <= 0 then we didn't find 'END' correctly and shouldn't try to write a CRC.
      // Calculate the proper LCRC now that we have all of the packets written
      // Don't use 'STP' or LCRC & 'END' in calc
      lcrc = xtor->calc32bCRC(&(phys_bytes_dat[1]),pos-1);  // generate 32-bit LCRC
      phys_bytes_dat[pos+0] = ((lcrc >> 24) & 0xFF);   // most significant bits
      phys_bytes_dat[pos+1] = ((lcrc >> 16) & 0xFF);
      phys_bytes_dat[pos+2] = ((lcrc >>  8) & 0xFF);
      phys_bytes_dat[pos+3] = (lcrc & 0xFF);           // least significant bits
   }

   mNeedSeqNumAndLCRC = false;
   return;
}

void PCIExpressTransaction::allocPhysBytes(UInt32 size) {
   mBytesTotal = size;
   phys_bytes_dat = new UInt32[mBytesTotal];
   phys_bytes_dat = reinterpret_cast<UInt32*>(memset(phys_bytes_dat, 0, sizeof(UInt32) * mBytesTotal));
   phys_bytes_sym = new bool[mBytesTotal];
   phys_bytes_sym = reinterpret_cast<bool*>(memset(phys_bytes_sym, 0, sizeof(bool) * mBytesTotal));
   return;
}


/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
