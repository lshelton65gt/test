// -*- C++ -*-
/*
  Carbon Design Systems - Copyright 2003 - 2005
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cassert>
#include <iostream>
#include <sstream>
#include "PCIExpressTransactor.h"
#include "PCIExpressTransaction.h"
#include "PCIExpressCallback.h"
#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimClock.h"
#include "crc.h"

const int PCIE_CMD_STR_MAX  = 256;


// Copied from $CARBON_HOME/src/inc/util/OSWrapper.h:
//! Printf format modifier for 64 bit arguments vs. 32 bit args
#if defined(__LP64__)
#  define Format64(x) "%l" #x
#  define Formatw64(w,x) "%" #w "l" #x
#  define FormatSize_t "%lu"
#else
#  if defined(__WIN32__)
#    define Format64(x) "%I64" #x
#    define Formatw64(w,x) "%" #w "I64" #x
#  else
#    define Format64(x) "%ll" #x
#    define Formatw64(w,x) "%" #w "ll" #x
#  endif
#  define FormatSize_t "%u"
#endif



PCIExpressTransactor::PCIExpressTransactor(PCIEXtor *theXtor, PCIEXtor::PCIEIntfType intf, UInt32 linkwidth, char* inPrintName)
   : mParent(theXtor), mSignalClk_SimNet(0), mSignalClk_SimClock(0),
     pSignalRxData(0), pSignalTxData(0), pSignalRxEIdle(0), pSignalTxEIdle(0),
     pSignalRxDataK(0), pSignalTxCompliance(0), pSignalRxPolarity(0),
     pSignalTxDataK(0), pSignalRxStatus(0), pSignalRxValid(0),
     mSignalTxDetectRx(0), mSignalResetN(0), mSignalPowerDown(0), mSignalPhyStatus(0),
     mEIdleActiveHigh(true), mBusNum(0), mDevNum(0), mFuncNum(0), 
     mDLLPSeqNum(0), mLinkWidth(linkwidth), mMaxTransInFlight(32), 
     mNumTransInFlight(0), next_tag(0), 
     VerboseReceiverHigh(false), VerboseReceiverMedium(false), VerboseReceiverLow(true), 
     VerboseTransmitterHigh(false), VerboseTransmitterMedium(false), 
     VerboseTransmitterLow(true), mScrambleEnable(true), mDoneForNow(false), mInReset(false),
     mResetTimer(0), mDetectRxTimer(0), mIncomingDLLPBytesNext(0), cycle_count_skp(0),
     powerSave_L0s(0), curPowerDnState(9999999),
     rxLTSM(LTSM_Electrical_Idle), txLTSM(LTSM_Electrical_Idle), rxDLLSM(DLLSM_Inactive), 
     txDLLSM(DLLSM_Inactive), mInTransaction(false), mFullyConfigured(false), mHasBeenWarnedAboutConfig(false),
     mTrainingBypass(false), mStartTraining(0), mStartTrainingPrefaceIdleTime(5),
     mDoneCBFn(0), mDoneCBInfo(0), mTransCBFn(0), mTransCBInfo(0),
     mFirstCall(true), mReplayTransactions(false), mCurrTick(0), mCurrTimescaleValid(false), mCurrTimescale(e1ns), mCurrTimeString(0),
     mForcedNaks(false), mLastTxAckSeqNum(0), mLastRxAckSeqNum(0), mIntfType(intf),
     mTransactionInProgress1(0), mTransactionInProgress2(0),
     mFcTxPostedHdrCreditLimit(0), mFcTxPostedDataCreditLimit(0), mFcTxNonPostedHdrCreditLimit(0),
     mFcTxNonPostedDataCreditLimit(0), mFcTxCompletionHdrCreditLimit(0), mFcTxCompletionDataCreditLimit(0),
     mFcTxPostedHdrCreditsConsumed(0), mFcTxPostedDataCreditsConsumed(0), mFcTxNonPostedHdrCreditsConsumed(0),
     mFcTxNonPostedDataCreditsConsumed(0), mFcTxCompletionHdrCreditsConsumed(0), mFcTxCompletionDataCreditsConsumed(0),
     mMaxPayloadSizeDW(32)
{
   UInt32 i;

   init_luts();
   mLatestPackets       = new UInt32[linkwidth];
   mLatestPacketSym     = new UInt32[linkwidth];
   mIncomingPackets     = new UInt32[linkwidth];
   mIncomingPacketsSym  = new bool[linkwidth];
   mSpeculativeDescramBytes = new UInt32[linkwidth];
   pSignalRxData        = new CarbonSimNet*[linkwidth];
   pSignalTxData        = new CarbonSimNet*[linkwidth];
   pSignalRxEIdle       = new CarbonSimNet*[linkwidth];
   pSignalTxEIdle       = new CarbonSimNet*[linkwidth];
   if (mIntfType == PCIEXtor::ePipeIntf) {
      pSignalRxDataK       = new CarbonSimNet*[linkwidth];
      pSignalTxCompliance  = new CarbonSimNet*[linkwidth];
      pSignalRxPolarity    = new CarbonSimNet*[linkwidth];
      pSignalTxDataK       = new CarbonSimNet*[linkwidth];
      pSignalRxStatus      = new CarbonSimNet*[linkwidth];
      pSignalRxValid       = new CarbonSimNet*[linkwidth];
   }
   disparity_in         = new bool[linkwidth];
   disparity_out        = new bool[linkwidth];
   disparity_in_not_set = new bool[linkwidth];
   lfsr_out             = new unsigned short[linkwidth];
   lfsr_in              = new unsigned short[linkwidth];
   printName            = new char[strlen(inPrintName)+1];
   mCurrTimeString      = new char[25];

   strcpy(printName,inPrintName);
   curInputState = Electrical_Idle;
   curOutputState = Electrical_Idle;
   strcpy(curInputStateStr,"Electrical Idle");
   strcpy(curOutputStateStr,"Electrical Idle");
   newTransactionPacketsTotal=0;
   mCurrTransactionStart=0;
   mCurrTransactionEnd=0;

   //   valid link widths are x1, x2, x4, x8, x12, x16, x32
   if((linkwidth != 1) && (linkwidth != 2) && (linkwidth != 4) && (linkwidth != 8)
      && (linkwidth != 12) && (linkwidth != 16) && (linkwidth != 32))
      printf("%s  ERROR: The linkwidth (%d) is invalid for PCI Express rev 1.0a.\n", printName, linkwidth);

   for(i=0; i < linkwidth; ++i) {
      mLatestPackets[i] = 0;
      mLatestPacketSym[i] = 0;
      mIncomingPackets[i] = 0;
      mIncomingPacketsSym[i] = false;
      mSpeculativeDescramBytes[i] = 0;
      pSignalRxData[i] = 0;
      pSignalTxData[i] = 0;
      pSignalRxEIdle[i] = 0;
      pSignalTxEIdle[i] = 0;
      disparity_in[i] = false;
      disparity_out[i] = false;
      disparity_in_not_set[i] = true;
      lfsr_out[i] = 0xFFFF;
      lfsr_in[i] = 0xFFFF; 
   }
   if (mIntfType == PCIEXtor::ePipeIntf) {
      for(i=0; i < linkwidth; ++i) {
	 pSignalRxDataK[i] = 0;
	 pSignalTxCompliance[i] = 0;
	 pSignalRxPolarity[i] = 0;
	 pSignalTxDataK[i] = 0;
	 pSignalRxStatus[i] = 0;
	 pSignalRxValid[i] = 0;
      }
   }

   for (i=0; i < TRANS_BUF_SIZE; ++i) {
      newTransactionPackets[i] = 0;
      newTransactionPacketsSym[i] = false;
      newTransactionPacketsOSet[i] = false;
   }

   txPowerDown = 0x0;
   txPowerDown_d1 = 0xDEADBEEF;

   updateCurrTimeString();

   mTransactionListNew.clear();
   mTransactionListNeedAck.clear();
   mTransactionListNeedCpl.clear();
   mReceivedTransList.clear();
   mReturnDataList.clear();
}


PCIExpressTransactor::~PCIExpressTransactor(void)
{
   PCIExpressTransactionList::const_iterator tx_iter;
   PCIECmdList::const_iterator cmd_iter;

   /*
   ** Clean up!
   */
   if(mLatestPackets)      delete[] mLatestPackets;
   if(mLatestPacketSym)    delete[] mLatestPacketSym;
   if(mIncomingPackets)    delete[] mIncomingPackets;
   if(mIncomingPacketsSym) delete[] mIncomingPacketsSym;
   if(mSpeculativeDescramBytes) delete[] mSpeculativeDescramBytes;
   if(pSignalRxData)       delete[] pSignalRxData;
   if(pSignalTxData)       delete[] pSignalTxData;
   if(pSignalRxEIdle)      delete[] pSignalRxEIdle;
   if(pSignalTxEIdle)      delete[] pSignalTxEIdle;
   if(pSignalRxDataK)      delete[] pSignalRxDataK;
   if(pSignalTxCompliance) delete[] pSignalTxCompliance;
   if(pSignalRxPolarity)   delete[] pSignalRxPolarity;
   if(pSignalTxDataK)      delete[] pSignalTxDataK;
   if(pSignalRxStatus)     delete[] pSignalRxStatus;
   if(pSignalRxValid)      delete[] pSignalRxValid;
   if(disparity_in)        delete[] disparity_in;
   if(disparity_out)       delete[] disparity_out;
   if(disparity_in_not_set) delete[] disparity_in_not_set;
   if(lfsr_out)            delete[] lfsr_out;
   if(lfsr_in)             delete[] lfsr_in;
   if(printName)           delete[] printName;
   if(mCurrTimeString)     delete[] mCurrTimeString;

   for (tx_iter = mTransactionListNew.begin(); tx_iter != mTransactionListNew.end(); ++tx_iter) {
      delete (*tx_iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNew.clear();                   // clear space in list

   for (tx_iter = mTransactionListNeedAck.begin(); tx_iter != mTransactionListNeedAck.end(); ++tx_iter) {
      delete (*tx_iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNeedAck.clear();               // clear space in list

   for (tx_iter = mTransactionListNeedCpl.begin(); tx_iter != mTransactionListNeedCpl.end(); ++tx_iter) {
      delete (*tx_iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNeedCpl.clear();               // clear space in list

   for (cmd_iter = mReceivedTransList.begin(); cmd_iter != mReceivedTransList.end(); ++cmd_iter) {
      delete (*cmd_iter);               // must explicitly delete alloced pointers
   }
   mReceivedTransList.clear();                    // clear space in list

   for (cmd_iter = mReturnDataList.begin(); cmd_iter != mReturnDataList.end(); ++cmd_iter) {
      delete (*cmd_iter);               // must explicitly delete alloced pointers
   }
   mReturnDataList.clear();                       // clear space in list
}


void PCIExpressTransactor::idle(UInt64 cycles) {
   std::ostringstream txname;
   txname.str(""); txname << "IDLE " << std::dec << cycles;
   addTransactionSpecial(txname.str().c_str());   // add IDLE transaction for 'cycles' length
}


void PCIExpressTransactor::step(CarbonTime tick) {
   mCurrTick = tick;  // keep track of the time
   updateCurrTimeString();

   // if we're configured then continue
   if (!mFullyConfigured) {
      // if not, then check if we are configured
      if (!mHasBeenWarnedAboutConfig && !isConfigured()) {
	 // if we're still not configured, then print a message
	 if (VerboseTransmitterLow || VerboseReceiverLow)
	    printf("%s  ERROR: Transactor not fully configured and will not operate.\n", printName);
	 if (VerboseTransmitterMedium || VerboseReceiverMedium)
	    printf("%s         Please use the printConfiguration() for details.\n", printName);
	 mHasBeenWarnedAboutConfig = true;
      }

      // since we're not configured, don't do anything but issue our Done callback and return

      // if a DoneForNow callback has been set then call it
      if(mDoneCBFn)
	 mDoneCBFn(mDoneCBInfo);
      return;
   }


   PCIExpressTransactor::GetTransactionReturnStatus transReturnStat=eError;
   UInt32 packetCount = 0;
   bool encode = true;  // by default we'll encode all out going data

   /*
   ** On our first call, we need to set our outputs to EIDLE
   */
   // NOTE: we are COUNTING on mLatestPackets starting off as Zero for EIDLE
   if (mFirstCall) {
      if (VerboseTransmitterHigh) {
	 printf("%s  Info: Setting the bus to EIDLE on first call.\n", printName);
      }
      transmitData(false);
      for (UInt32 i=0; i<mLinkWidth; ++i)
	 setTransmitLos(i, 0x1);
      mFirstCall=false;
   }

   /*
   ** We are only interested in rising edges.
   */
   if (getClkValue() == 0) {
      return;
   }

   /*
   ** Check our current state
   */
//  if(mCurrTick > 180000000ULL)
//    printf(" In: rxstate=%s, rxLTSM=%s, txstate=%s, txLTSM=%s (at %s)\n", curInputStateStr, getLTSMStateString(rxLTSM), curOutputStateStr, getLTSMStateString(txLTSM), mCurrTimeString);
   checkReset();

   if((curInputState == Electrical_Idle)
      && (curOutputState != Electrical_Idle)
      && (mStartTraining==0)
      ) {
      if(powerSave_L0s != 1) {
	 addTransactionSpecial("EIDLE");    // add transaction to zero the bus lines
      }
      if(powerSave_L0s == 0) {
	 rxLTSM = LTSM_Electrical_Idle;
	 txLTSM = LTSM_Electrical_Idle;
	 rxDLLSM = DLLSM_Inactive;
	 txDLLSM = DLLSM_Inactive;
	 if (mIntfType == PCIEXtor::ePipeIntf)  // txLTSM == LTSM_Electrical_Idle
	    for(UInt32 i=0; i<mLinkWidth; ++i)
	       setTransmitRxValid(i, 0x0);
      }
      mInTransaction = false;
   } else if(((curInputState == Logical_Idle) || (mStartTraining==1)) // Just saw receiver wake up (on last step)
	      && (curOutputState == Electrical_Idle)) {               // Or we're going to wake it up
      if (mStartTraining == 1) {
	 mStartTraining=2;
	 std::ostringstream txname;
	 txname.str("");
	 txname << "IDLE " << std::dec << mStartTrainingPrefaceIdleTime;
	 // addTransactionSpecial("IDLE 5");   // add 1 IDLE transaction (then bus stays in idle)
	 addTransactionSpecial(txname.str().c_str());   // add IDLE transactions
      }
      setCurOutputState(Logical_Idle);   // need this to be able to wake up from electrical idle
      if (mTrainingBypass) {
	 rxLTSM = LTSM_Active;
	 txLTSM = LTSM_Active;
	 for(UInt32 i=0; i<mLinkWidth; ++i)    // txLTSM != LTSM_Electrical_Idle
	    setTransmitLos(i, 0x0);
      } else {
	 rxLTSM = LTSM_Logical_Idle;
	 txLTSM = LTSM_Logical_Idle;
	 for(UInt32 i=0; i<mLinkWidth; ++i) {  // txLTSM != LTSM_Electrical_Idle
	    setTransmitLos(i, 0x0);
	    if (mIntfType == PCIEXtor::ePipeIntf)
	       setTransmitRxValid(i, 0x1);
	 }
      }
   } else if((curInputState == Power_Save)
	     && (curOutputState != Power_Save)
	     ) {
      addTransactionSpecial("POWER_SAVE");
      addTransactionSpecial("EIDLE");
   } else if((rxLTSM < LTSM_Active) || (txLTSM < LTSM_Active)) {      // Link Training to do here
      // *****
      // UNKNOWN IF THIS IS THE RIGHT PLACE, but it works
      checkTxDetectRx();
      // *****

      if (not mTrainingBypass) {
	 LTSM_process();
      }
   } else if((rxDLLSM < DLLSM_Active) || (txDLLSM < DLLSM_Active)) {  // Data Link Layer Training to do here
      DLLSM_process();
   }

   // Insert SKP Orderd Set at regular intervals for Clock Tolerance Compensation (PCIE Spec 1.0a sec 4.2.7.1)
   if((curOutputState == Electrical_Idle) || (powerSave_L0s != 0)
      || (txLTSM < LTSM_Active) || (txDLLSM < DLLSM_Active)
     ) {
      cycle_count_skp = 0;   // reset the active cycle counter for SKP sets
   } else { //  if(txLTSM == LTSM_Active){
      ++cycle_count_skp;
      if(cycle_count_skp >= 1200) {   // Per Spec: interval must be btwn 1180 & 1538 symbol times
	 addTransactionSpecial("CLOCK_TOL");
	 addTransactionSpecial("UpdateFC");
	 cycle_count_skp = 0;   // reset the active cycle counter for SKP sets
      }
   }


   // Read the incoming pins for a transaction at each tick
   //   and act on it.
   readInputBus();

   // Get pointer to transaction to be worked on.  The transaction may be new or in progress
   // at this point.  If there are no transactions to go across the bus then 0 is returned.
   transReturnStat = getNewTransaction();

   if (transReturnStat == eError)
      return;

   if ((curOutputState != Electrical_Idle) && (transReturnStat != eNoTransAvailable)) {
      mDoneForNow = false;

      if (transReturnStat == eNoCreditsAvailable) {
	 transmitIdleData();  // new tx being blocked by flow control, be sure bus is idle
      } else {  // eSuccess
	 if((mTransactionInProgress1->getRunningOutputState() == Power_Save) && (powerSave_L0s != 1)) {
	    powerSave_L0s = 1;
	 }
	 if(mTransactionInProgress1->getRunningOutputState() == Electrical_Idle) {
	    encode = false;   // special case, want true 0's.
	 } else {
	    setCurOutputState(mTransactionInProgress1->getRunningOutputState());
	 }
	 mTransactionInProgress1->step(this, tick);

	 packetCount = mTransactionInProgress1->getNumPacketsRemaining();
	 if ((packetCount >= mLinkWidth) ||  // first tx has enough packets to put on link
	     (mTransactionInProgress2 == 0) ||     // or we have no second tx
	     (mReplayTransactions)) {        // or this one needs to replay, don't start another
	    mTransactionInProgress1->getTransmitPackets(mLinkWidth, mLatestPackets, mLatestPacketSym);
	 } else {
	    mTransactionInProgress1->getTransmitPackets(packetCount, mLatestPackets, mLatestPacketSym);
	    mTransactionInProgress2->step(this, tick);
	    mTransactionInProgress2->getTransmitPackets((mLinkWidth-packetCount), &(mLatestPackets[packetCount]), &(mLatestPacketSym[packetCount]));
	 }

	 // Transmit Data
	 transmitData(encode);

	 if (mTransactionInProgress1->canDelete()) {
	    if(mTransactionInProgress1->getRunningOutputState() == Electrical_Idle) {
	       setCurOutputState(mTransactionInProgress1->getRunningOutputState());
	       if(powerSave_L0s == 1) {
		  powerSave_L0s = 2;
	       }
	    }
	 }

	 // If received a Nak then move transactions back into 'new' queue
	 if (mTransactionInProgress1->isDone()) {
	    if (mReplayTransactions) {
	       updateTransactionsForNak();
	    } else {
	       if (mTransactionInProgress1->canDelete())
		  delete mTransactionInProgress1;
	       else
		  mTransactionListNeedAck.push_back(mTransactionInProgress1);
	       mTransactionInProgress1 = 0;

	       if ((mTransactionInProgress2 != 0) &&
		   (mTransactionInProgress2->isDone())) {
		  if (mTransactionInProgress2->canDelete())
		     delete mTransactionInProgress2;
		  else
		     mTransactionListNeedAck.push_back(mTransactionInProgress2);
		  mTransactionInProgress2 = 0;
	       }

	       mTransactionInProgress1 = mTransactionInProgress2;
	       mTransactionInProgress2 = 0;
	    }
	 }
      }
   } else {  // either output in ElecIdle or there are NoTransAvailable
//      if((curOutputState != Logical_Idle) && (powerSave_L0s != 2) && (txLTSM == LTSM_Active)) {
      if(((curOutputState != Logical_Idle) || (mScrambleEnable)) &&
	 (powerSave_L0s != 2) &&
	 (txLTSM == LTSM_Active)) {
	 // Transmit Idle
	 transmitIdleData();
      }

      if ((transReturnStat == eNoTransAvailable) && (mReplayTransactions)) {
	 updateTransactionsForNak();
      }

      if(((txLTSM == LTSM_Active) || (rxLTSM == LTSM_Logical_Idle) || (rxLTSM == LTSM_Electrical_Idle))) {   // don't stop while we're in training.
	 mDoneForNow = true;  // set flag so some controlling watcher will know

	 // if a DoneForNow callback has been set then call it
	 if(mDoneCBFn)
	    mDoneCBFn(mDoneCBInfo);
      }
   }
}

void PCIExpressTransactor::checkReset(void) {
   UInt32 reset_n_value;

   // Nothing to do for the 10-bit interface
   if (mIntfType == PCIEXtor::e10BitIntf)
      return;

   reset_n_value = getReceiveResetN();

   // Pipe Intf has a ResetN signal so we know we're in reset
   if ((reset_n_value == 0) && (not mInReset)) {  // reset_l asserted
      rxLTSM = LTSM_Electrical_Idle;
      txLTSM = LTSM_Electrical_Idle;
      rxDLLSM = DLLSM_Inactive;
      txDLLSM = DLLSM_Inactive;
      if (mIntfType == PCIEXtor::ePipeIntf)  // txLTSM == LTSM_Electrical_Idle
	 for(UInt32 i=0; i<mLinkWidth; ++i)
	    setTransmitRxValid(i, 0x0);
      mInTransaction = false;
      mInReset = true;
      mResetTimer = 3;  // count 3 clocks after deassertion of reset pin before coming out of reset
      setTransmitPhyStatus(0x1);
   } else if ((reset_n_value != 0) && (mInReset)) {  // on deassertion of reset pin
      if (mResetTimer == 0) {
	 mInReset = false;
	 setTransmitPhyStatus(0x0);    // set PhyStatus low showing that we have clocks and are ready

	 if ((mStartTraining == 2) || (mTrainingBypass)) {
	    setCurOutputState(Logical_Idle);   // need this to be able to wake up from electrical idle
	    if (mTrainingBypass) {
	       rxLTSM = LTSM_Active;
	       txLTSM = LTSM_Active;
	       addTransactionSpecial("CLOCK_TOL");
	       mStartTraining=1;  // don't go back into eidle cause rx is in eidle
	    } else {
	       rxLTSM = LTSM_Logical_Idle;
	       txLTSM = LTSM_Logical_Idle;
	    }
	    for(UInt32 i=0; i<mLinkWidth; ++i) {
	       setTransmitLos(i, 0x0);
	       setTransmitRxValid(i, 0x1);
	    }
	 }
      } else {
	 --mResetTimer;
      }
   }
   return;
}

void PCIExpressTransactor::checkTxDetectRx(void) {
   UInt32 i;

   // Nothing to do for the 10-bit interface
   if (mIntfType == PCIEXtor::e10BitIntf)
      return;

   // Pipe Intf has a TxDetectRx signal so we know when the DUT is doing a receiver detect
   if (getReceiveTxDetectRx() == 1) {  // TxDetectRx asserted
      if (mDetectRxTimer == 0) {
	 mDetectRxTimer = 12;
      } else if (mDetectRxTimer == 10) {  // wait a few cycles to do the detect
	 setTransmitPhyStatus(0x1);      // signal done with detection by assert of PhyStatus
	 for (i=0; i<mLinkWidth; ++i) {
	    setTransmitRxStatus(i, 0x3); // set active lanes
	 }
      } else if (mDetectRxTimer == 9) {  // go 1 cycle & deassert PhyStatus
	 setTransmitPhyStatus(0x0);
	 for (i=0; i<mLinkWidth; ++i) {
	    setTransmitRxStatus(i, 0x0); // set active lanes
	 }
      }
      --mDetectRxTimer;
   } else if (mDetectRxTimer > 0) {
      if (mDetectRxTimer == 4) {  // go a few cycles and then say we're ready to progress (No, I don't know why)
	 setTransmitPhyStatus(0x1);
      } else if (mDetectRxTimer == 3) {  // go a few cycles and then say we're ready to progress (No, I don't know why)
	 setTransmitPhyStatus(0x0);
      }
      --mDetectRxTimer;
   }

   return;
}

void PCIExpressTransactor::LTSM_process() {
    if((txLTSM == LTSM_Logical_Idle) || (txLTSM == LTSM_Active)) {
       if((rxLTSM == LTSM_TS1_P_P) || (rxLTSM == LTSM_TS1_0_x) || (mStartTraining==2)) {
	  if(txLTSM == LTSM_Active) {    // link retrained, clear pending queue
	     deleteTransactionQueue();
	     txDLLSM = DLLSM_Inactive;  // always?
	  }
	  if((rxLTSM == LTSM_TS1_P_P) || (mStartTraining==2)) {           // Full Reset
	     addTransactionSpecial("CLOCK_TOL");
	     addTransactionSpecial("TS1 PAD PAD");      // add transaction TS1
	     txLTSM = LTSM_TS1_P_P;
	     mStartTraining=3;
	  } else {                               // Return from L1
	     addTransactionSpecial("TS1 0 x");
	     txLTSM = LTSM_TS1_0_x;
	  }
       }
    } else if((txLTSM == LTSM_TS1_P_P) && ((rxLTSM == LTSM_TS1_P_P) || (rxLTSM == LTSM_TS2_P_P))) {
	addTransactionSpecial("TS2 PAD PAD");
	txLTSM = LTSM_TS2_P_P;
    } else if((txLTSM == LTSM_TS2_P_P) && ((rxLTSM == LTSM_TS2_P_P) || (rxLTSM == LTSM_TS1_0_P))) {
	addTransactionSpecial("TS1 0 PAD");
	txLTSM = LTSM_TS1_0_P;
    } else if((txLTSM == LTSM_TS1_0_P) && ((rxLTSM == LTSM_TS1_0_P) || (rxLTSM == LTSM_TS1_0_x))) {
	addTransactionSpecial("TS1 0 x");  // 0 for link, laneNum for lane
	txLTSM = LTSM_TS1_0_x;
    } else if((txLTSM == LTSM_TS1_0_x) && ((rxLTSM == LTSM_TS1_0_x) || (rxLTSM == LTSM_TS2_0_x))) {
	addTransactionSpecial("TS2 0 x");
	txLTSM = LTSM_TS2_0_x;
    } else if((txLTSM == LTSM_TS2_0_x) && ((rxLTSM == LTSM_TS2_0_x) || (rxLTSM == LTSM_Active))) {  // Done training
	addTransactionSpecial("IDLE 1");
	txLTSM = LTSM_Active;
	mStartTraining=4;  // done with LTSM training...
    } else if((txLTSM != LTSM_Logical_Idle) && ((rxLTSM == LTSM_Logical_Idle) && (mStartTraining!=3))) {
	addTransactionSpecial("IDLE 1");
	txLTSM = LTSM_Logical_Idle;
    }
}

void PCIExpressTransactor::DLLSM_process()
{
    if((txDLLSM == DLLSM_Inactive) && ((rxDLLSM == DLLSM_Init1) || (mStartTraining==4))) {
	addTransactionSpecial("InitFC1");
	addTransactionSpecial("IDLE 3");
	txDLLSM = DLLSM_Init1;
	mStartTraining=5;
    } else if((txDLLSM == DLLSM_Init1) && ((rxDLLSM == DLLSM_Init1) || (rxDLLSM == DLLSM_Init2))) {
	addTransactionSpecial("InitFC2");
	addTransactionSpecial("IDLE 3");
	txDLLSM = DLLSM_Init2;
    } else if((txDLLSM == DLLSM_Init2) && ((rxDLLSM == DLLSM_Init2) || (rxDLLSM == DLLSM_Active))) {  // Done training
	addTransactionSpecial("IDLE 1");
	txDLLSM = DLLSM_Active;
	mStartTraining=0;  // reset our tracking flag
	if(VerboseReceiverLow)
	   printf("%s  Info: Link training complete (at %s).\n", printName, mCurrTimeString);
    } else if((txDLLSM != DLLSM_Inactive) && (rxDLLSM == DLLSM_Inactive) && (mStartTraining!=5)) {
	txDLLSM = DLLSM_Inactive;
    }
}


void PCIExpressTransactor::verbose(UInt32 flags)
{
    // Bits are Active LOW
    //  hi me lo tx rx
    //   x  x  x  1  0   = 0x1E = Verbose_Receiver
    //   x  x  x  0  1   = 0x1D = Verbose_Transmitter
    //   x  x  x  0  0   = 0x1C = Verbose_All
    //   1  1  1  x  x   = 0x1F = Verbosity_Off
    //   1  1  0  x  x   = 0x1B = Verbosity_Low
    //   1  0  x  x  x   = 0x17 = Verbosity_Medium
    //   0  x  x  x  x   = 0x0F = Verbosity_High

    if(!(flags & 0x01)) {    // receiver selected
	if(!(flags & 0x10)) {         // High
	    VerboseReceiverHigh = true;
	    VerboseReceiverMedium = true;
	    VerboseReceiverLow = true;
	    printf("%s  Info: Verbose Receiver = High\n", printName);
	} else if(!(flags & 0x08)) {  // Medium
	    VerboseReceiverHigh = false;
	    VerboseReceiverMedium = true;
	    VerboseReceiverLow = true;
	    printf("%s  Info: Verbose Receiver = Medium\n", printName);
	} else if(!(flags & 0x04)) {  // Low
	    VerboseReceiverHigh = false;
	    VerboseReceiverMedium = false;
	    VerboseReceiverLow = true;
	    printf("%s  Info: Verbose Receiver = Low\n", printName);
	} else {                     // Off
	    VerboseReceiverHigh = false;
	    VerboseReceiverMedium = false;
	    VerboseReceiverLow = false;
	    printf("%s  Info: Verbose Receiver = Off\n", printName);
	}
    }
    if(!(flags & 0x02)) {    // transmitter selected
	if(!(flags & 0x10)) {         // High
	    VerboseTransmitterHigh = true;
	    VerboseTransmitterMedium = true;
	    VerboseTransmitterLow = true;
	    printf("%s  Info: Verbose Transmitter = High\n", printName);
	} else if(!(flags & 0x08)) {  // Medium
	    VerboseTransmitterHigh = false;
	    VerboseTransmitterMedium = true;
	    VerboseTransmitterLow = true;
	    printf("%s  Info: Verbose Transmitter = Medium\n", printName);
	} else if(!(flags & 0x04)) {  // Low
	    VerboseTransmitterHigh = false;
	    VerboseTransmitterMedium = false;
	    VerboseTransmitterLow = true;
	    printf("%s  Info: Verbose Transmitter = Low\n", printName);
	} else {                     // Off
	    VerboseTransmitterHigh = false;
	    VerboseTransmitterMedium = false;
	    VerboseTransmitterLow = false;
	    printf("%s  Info: Verbose Transmitter = Off\n", printName);
	}
    }
}

void PCIExpressTransactor::setScrambleStatus(bool tf)
{
   mScrambleEnable = tf;
   if(VerboseTransmitterLow || VerboseReceiverLow)
      printf("%s  Info: Scramble/Descramble of packets %s\n",printName, (tf ? "Enabled" : "Disabled"));
}


// Data Scrambling / Descrambling one byte at a time.  This is taken directly from the
// PCI Express 1.0a spec, Appendix C.
/*
  this routine implements the serial descrabling algorithm in parallel form
  for the LSFR polynomial: x^16+x^5+x^4+x^3+1
  this advances the LSFR 8 bits every time it is called
*/

UInt32 PCIExpressTransactor::scramble_descramble_byte(UInt32 inbyte, bool kin, bool scramble_not_descramble, UInt32 laneNum)
{
    static int scrambit[16];   // scrabled/descrabled bits
    static int bit[16];
    static int bit_out[16];
    unsigned short lfsr;       // 16 bit short for polynomial
    int i, outbyte;
    bool inTraining = false;
    bool speculativeDescramble = false;

    for (i=0; i<16; ++i) {
       scrambit[i] = 0;
       bit[i] = 0;
       bit_out[i] = 0;
    }

    // Get Current value of the LFSR
    if(scramble_not_descramble) {
	lfsr = lfsr_out[laneNum];         // scrambling output data
	if ((txLTSM < LTSM_Active) ||
	    (curOutputState == Link_Training))
	   inTraining = true;
    } else {
	lfsr = lfsr_in[laneNum];          // descrambling input data
	if (rxLTSM == LTSM_TS2_0_x) {
	   speculativeDescramble = true;    // trying to find end of training packets...
	} else if (rxLTSM < LTSM_Active) {
	   inTraining = true;               // not scrambled during training
	}
    }

    if ((inbyte == PCIE_COM) && kin) {    // if this is a comma
       // reset the LFSR
       if (scramble_not_descramble)
	  lfsr_out[laneNum] = 0xFFFF;         // scrambling output data
       else
	  lfsr_in[laneNum] = 0xFFFF;          // descrambling input data
       return(PCIE_COM);        // and return the same data
    }
    if ((inbyte == PCIE_SKP) && kin)      // don't advance or encode/decode on skip
       return(PCIE_SKP);

    for (i=0; i<16; ++i)         // convert LFSR to bit array for legibility
       bit[i] = (lfsr >> i) & 1;

    // apply the xor to the data
    if ((not kin) &&                // if not a KCODE, scramble the data
	((speculativeDescramble) ||
	 (not inTraining))) {       // and if not in the middle of a training sequence

       for (i=0; i<8; ++i)          // convert byte to be scrambled/descrambled for legibility
	  scrambit[i] = (inbyte >> i) & 1;

	scrambit[0] ^= bit[15];
	scrambit[1] ^= bit[14];
	scrambit[2] ^= bit[13];
	scrambit[3] ^= bit[12];
	scrambit[4] ^= bit[11];
	scrambit[5] ^= bit[10];
	scrambit[6] ^= bit[9];
	scrambit[7] ^= bit[8];

	outbyte = 0;
	for (i=0; i<8; ++i)    // convert data back to an integer
	   outbyte += (scrambit[i] << i);
    } else {
       outbyte = inbyte;
    }

    if (speculativeDescramble) {                    // we are in training so don't descramble
       mSpeculativeDescramBytes[laneNum] = outbyte; // but save what it *would* have been so
       outbyte = inbyte;                            // we can find the end of training
    }

    // Now advance the LFSR 8 serial clocks
    bit_out[ 0] = bit[ 8];
    bit_out[ 1] = bit[ 9];
    bit_out[ 2] = bit[10];
    bit_out[ 3] = bit[11] ^ bit[ 8];
    bit_out[ 4] = bit[12] ^ bit[ 9] ^ bit[ 8];
    bit_out[ 5] = bit[13] ^ bit[10] ^ bit[ 9] ^ bit[ 8];
    bit_out[ 6] = bit[14] ^ bit[11] ^ bit[10] ^ bit[ 9];
    bit_out[ 7] = bit[15] ^ bit[12] ^ bit[11] ^ bit[10];
    bit_out[ 8] = bit[ 0] ^ bit[13] ^ bit[12] ^ bit[11];
    bit_out[ 9] = bit[ 1] ^ bit[14] ^ bit[13] ^ bit[12];
    bit_out[10] = bit[ 2] ^ bit[15] ^ bit[14] ^ bit[13];
    bit_out[11] = bit[ 3]           ^ bit[15] ^ bit[14];
    bit_out[12] = bit[ 4]                     ^ bit[15];
    bit_out[13] = bit[ 5];
    bit_out[14] = bit[ 6];
    bit_out[15] = bit[ 7];
    lfsr = 0;
    for (i=0; i<16; ++i)   // convert the LFSR back to an integer
	lfsr += (bit_out[i] << i);

    // Save off our current value of the LFSR
    if(scramble_not_descramble) {
	lfsr_out[laneNum] = lfsr;         // scrambling output data
    } else {
	lfsr_in[laneNum] = lfsr;          // descrambling input data
    }

    return(outbyte);
}

SInt32 PCIExpressTransactor::addTransaction(PCIECmd *cmd) {
   PCIExpressTransaction *tx;
   SInt32 retVal = -1;  // -1 = failure, other = transaction ID
   tx = new PCIExpressTransaction(this, cmd);
   if (tx->isValid()) {
      retVal = tx->getTxID();  // find the transaction ID assigned to this transaction
      mTransactionListNew.push_back(tx);
      if (VerboseTransmitterMedium)
	 printf("%s  Info: Adding transaction '%s' to the queue.\n", printName, tx->getPrintString());
   } else {
      printf("%s  ERROR: Invalid transaction sent to transactor.", printName);
      delete tx;
   }
   return retVal;
}

void PCIExpressTransactor::addTransactionSpecial(const char *cmdstr) {
   PCIExpressTransaction *tx;
   tx = new PCIExpressTransaction(this, cmdstr);
   if (tx->isValid()) {
      mTransactionListNew.push_back(tx);
   } else {
      printf("%s  ERROR: Invalid transaction sent to transactor internally. ('%s')", printName, cmdstr);
      delete tx;
   }
   return;
}

void PCIExpressTransactor::addTransactionSpecialHead(const char *cmdstr) {
   PCIExpressTransaction* tx = new PCIExpressTransaction(this, cmdstr);

   if (not tx->isValid()) {
      printf("%s  ERROR: Invalid transaction sent to transactor internally. ('%s')", printName, cmdstr);
      delete tx;
      return;
   }

   mTransactionListNew.push_front(tx);

   return;
}

void PCIExpressTransactor::addTransactionSpecialAckNak(bool isAck, UInt32 seqNum) {
   PCIExpressTransaction* tx;
   PCIExpressTransactionList::iterator iter;
   PCIExpressTransactionList::iterator pointer;
   char cmdstr[20];

   // I need the Ack/Nak to come before TLPs (so we don't hit deadlock with buffers
   // all taken up).  Need Ack/Nak to come after other Acks/Naks (or all DLLPs) so
   // we don't end up getting them in the wrong order (as was happening when just
   // using addTransactionSpecialHead()

   // Search forward to first transaction not yet on the bus & save insert pointer.
   // While searching check if any Ack or Nak are seen, each one saves insert pointer.
   // Continue forward to first TLP & break.
   // If inserting a Nak, then insert after pointer.
   // If inserting an Ack, if pointer points to Ack then replace it (ack collapsing),
   //   else insert after it (pointer will either be last Nak or head of to-do list).

   if (isAck)
      sprintf(cmdstr, "DLLP_Ack %d", seqNum);
   else
      sprintf(cmdstr, "DLLP_Nak %d", seqNum);

   tx = new PCIExpressTransaction(this, cmdstr);
   if (not tx->isValid()) {
      printf("%s  ERROR: Invalid transaction sent to transactor internally. ('%s')", printName, cmdstr);
      delete tx;
      return;
   }

   pointer = mTransactionListNew.end ();
   for (iter = mTransactionListNew.begin(); iter != mTransactionListNew.end(); ++iter) {
      if ((*iter)->isAck() || (*iter)->isNak()) {
	 pointer = iter;  // can put Ack/Nak after this
      }
   }

   if (pointer == mTransactionListNew.end ()) {
      mTransactionListNew.push_front(tx);
   } else {
      if (isAck && (*pointer)->isAck()) {     // have Ack & found Ack, collapse them
	 delete (PCIExpressTransaction*)(*pointer);  // must explicitly delete allocated space
	 // OVERKILL:
	 // pointer = mTransactionListNew.erase(pointer);  // remove old Ack, given pointer to next element
	 // mTransactionListNew.insert(pointer, tx);       // insert *before* pointer
	 // EASIER & FASTER:
	 (*pointer) = tx;
      } else {
	 ++pointer;                                // point to next
	 mTransactionListNew.insert(pointer, tx);  // insert *before* pointer
      }
   }

   return;
}

PCIExpressTransactor::GetTransactionReturnStatus
 PCIExpressTransactor::getNewTransaction(void) {
   PCIExpressTransactionList::iterator iter;
   GetTransactionReturnStatus retStatus = eNoTransAvailable;  // expect no transactions available, status only for first tx, not second
   bool failedCreditCheckPosted = false;
   bool failedCreditCheckNonPosted = false;
   bool failedCreditCheckCompletion = false;

   // Find first transaction to put on bus now, if that is almost done then
   // find a second transaction to put back-to-back
   typedef enum {
      eFindFirstTx,       // find first tx
      eFindSecondAsTLP,   // find second tx, must be TLP
      eFindSecondAsDLLP,  // find second tx, must be DLLP
      eFindSecondAsEither // find second tx, can be either
   } searchState;
   searchState state = eFindFirstTx;

   
   if (mTransactionInProgress1 != 0) {  // If already have a transaction to work on
      if ((mTransactionInProgress1->getNumPacketsRemaining() >= mLinkWidth) ||
	  (mTransactionListNew.empty())) {
	 return eSuccess;                  // can finish this up
      }	else {
	 state = eFindSecondAsEither;      // find a successor if possible
	 retStatus = eSuccess;
      }

   } else {                             // No current transactions to use
      if (mTransactionListNew.empty())
	 return eNoTransAvailable;         // nothing to do
      else
	 state = eFindFirstTx;
   }


   for (iter = mTransactionListNew.begin(); iter != mTransactionListNew.end(); ++iter) {

      if (((state == eFindSecondAsTLP) && ((*iter)->isDLLPTransaction())) ||
	  ((state == eFindSecondAsDLLP) && ((*iter)->isTLPTransaction()))) {
	 // this one is not valid as a follow-up transaction on same clock cycle
	 continue;
      }

      if ((state != eFindFirstTx) && ((*iter)->mustStartLane0())) {
	 // looking for a second tx, found a DLLP that must start on
	 // lane zero (OSet tx).  Don't let anything pass it, just
	 // quit an let the current one finish and then this can be
	 // sent.
	 break;
      }

      // try to find a new transaction to start
      if ((mNumTransInFlight >= mMaxTransInFlight) &&
	  (not (*iter)->isInternal()) &&
	  (not (*iter)->isCompletion())
	 ) {
	 if (state == eFindFirstTx) {
	    if (VerboseReceiverMedium) {
	       printf("%s  Info: Flow control credits unavailable: Maximum number of transactions currently pending (%d) (at %s).\n", printName, mMaxTransInFlight, mCurrTimeString);
	    }
	    retStatus = eNoCreditsAvailable;  // Found a trans not done and too many already in flight
	 }
	 continue;
      }

      // If we previously failed to allocate credits for a Posted transaction
      //    then nothing passes it (except DLLPs)
      if ((failedCreditCheckPosted) &&
	  ((*iter)->getFCType() != PCIExpressTransaction::None)) {
	 continue;
      }
      // If we previously failed to allocate credits for a NonPosted trans
      //    then we allow anything to pass it.
      //-
      // If we previously failed to allocate credits for a Completion trans
      //    then we don't allow any other Completions to pass it.
      if ((failedCreditCheckCompletion) &&
	  ((*iter)->getFCType() == PCIExpressTransaction::Completion)) {
	 continue;
      }

      if (checkForAvailFlowControlCredits(*iter)) {
	 if (state == eFindFirstTx) {
	    mTransactionInProgress1 = (*iter);
	    mTransactionInProgress2 = 0;  // just to ensure its clear
	    retStatus = eSuccess;
	 } else {
	    mTransactionInProgress2 = (*iter);
	 }

	 if ((not (*iter)->isInternal()) && (not (*iter)->isCompletion()))
	    ++mNumTransInFlight;  // increment in-flight count for user transactions only (except cpl)
	 if (VerboseTransmitterMedium) // only print when we start the transaction
	    printf("%s  Info: Starting transaction '%s' across the bus (at %s).\n", printName, (*iter)->getPrintString(), mCurrTimeString);
	 (*iter) = 0;  // tag for deletion

	 if (state == eFindFirstTx) {
	    if (mTransactionInProgress1->getNumPacketsRemaining() >= mLinkWidth) {
	       // this transaction won't leave extra room this clock cycle
	       // to start a second transaction
	       break;     // found all that we can handle
	    } else {
	       if (mTransactionInProgress1->isDLLPTransaction())
		  state = eFindSecondAsTLP;
	       else
		  state = eFindSecondAsDLLP;
	       continue; // found first, search for second
	    }
	 } else {
	    break;       // found all that we want
	 }
      } else {
	 // need to check what type failed to stay within transaction
	 // ordering rules (PCIE v1.0a sec 2.4.1 (table 2-23))
	 switch ((*iter)->getFCType()) {
	 case PCIExpressTransaction::Posted:
	    if (splitDataTransaction(false, iter)) {
	       // above function removed the current transaction and added
	       // two new ones after it if it succeeded
	    } else {
	       failedCreditCheckPosted = true;
	    }
	    break;
	 case PCIExpressTransaction::NonPosted:
	    failedCreditCheckNonPosted = true;
	    break;
	 case PCIExpressTransaction::Completion:
	    if (splitDataTransaction(true, iter)) {
	       // above function removed the current transaction and added
	       // two new ones after it if it succeeded
	    } else {
	       failedCreditCheckCompletion = true;
	    }
	    break;
	 default: break;
	 } // end switch
	 if (state == eFindFirstTx) {
	    retStatus = eNoCreditsAvailable;  // trans avail, but no credits for this one
	 }
      }
   } // end for loop

   // cleanup queue
   for (iter = mTransactionListNew.begin(); iter != mTransactionListNew.end(); ) {
      if ((*iter) == 0)
	 iter = mTransactionListNew.erase(iter); // remove, returns pointer to next item
      else
	 ++iter;
   }

   return retStatus;
}

bool PCIExpressTransactor::checkForAvailFlowControlCredits(PCIExpressTransaction *transaction) {
   bool flowControlAccepted = false;

   // Check Flow Control
   if ((transaction->getFCType() == PCIExpressTransaction::None) ||  // no flow control (DLLP transaction)
       (transaction->getFCCreditsConsumed())                         // credits already consumed for this
      ) {
      flowControlAccepted = true;
   } else {
      switch (transaction->getFCType()) {
      case PCIExpressTransaction::Posted:
	 if (((mFcTxPostedHdrCreditLimit == 0xFFFFFFFF) ||   // unlimited
	      (((mFcTxPostedHdrCreditLimit - mFcTxPostedHdrCreditsConsumed) & 0xFF) >= transaction->getFCHeaderNum())) &&
	     ((mFcTxPostedDataCreditLimit == 0xFFFFFFFF) ||  // unlimited
	      (((mFcTxPostedDataCreditLimit - mFcTxPostedDataCreditsConsumed) & 0xFFF) >= transaction->getFCDataNum())) &&
	     (transaction->getDataLength() <= mMaxPayloadSizeDW)
	     ) {                               // consume Posted TX credits
	    mFcTxPostedHdrCreditsConsumed += transaction->getFCHeaderNum();
	    mFcTxPostedHdrCreditsConsumed &= 0xFF;
	    mFcTxPostedDataCreditsConsumed += transaction->getFCDataNum();
	    mFcTxPostedDataCreditsConsumed &= 0xFFF;
	    transaction->setFCCreditsConsumed();
	    flowControlAccepted = true;
	    if (VerboseReceiverMedium) {
	       printf("%s  Info: Flow control credits consumed: Posted HeaderFC increased by 0x%02X to 0x%02X, DataFC increased by 0x%03X to 0x%03X.\n", printName, transaction->getFCHeaderNum(), mFcTxPostedHdrCreditsConsumed, transaction->getFCDataNum(), mFcTxPostedDataCreditsConsumed);
	       fflush(0);
	    }
	 } else {
	    flowControlAccepted = false;
	    if (transaction->getDataLength() > mMaxPayloadSizeDW) {
	       if (VerboseReceiverMedium)
		  printf("%s  Info: %s transaction has data payload (%d bytes) that is greater than the Max_Payload_Size parameter (%d bytes).  It will be split into multiple transactions.\n", printName, transaction->getPrintString(), (transaction->getDataLength() * 4), (mMaxPayloadSizeDW * 4));
	    } else {
	       if (VerboseReceiverMedium) {
		  printf("%s  Info: Flow control credits unavailable: Posted HeaderFC requested 0x%02X with 0x%02X available, DataFC requested 0x%03X with 0x%03X available.\n", printName, transaction->getFCHeaderNum(), ((mFcTxPostedHdrCreditLimit - mFcTxPostedHdrCreditsConsumed) & 0xFF), transaction->getFCDataNum(), ((mFcTxPostedDataCreditLimit - mFcTxPostedDataCreditsConsumed) & 0xFFF));
		  printf("%s                                          HeaderFC advertised 0x%02X, HeaderFC Consumed 0x%02X, DataFC advertised 0x%03X, DataFC consumed 0x%03X\n", printName, mFcTxPostedHdrCreditLimit, mFcTxPostedHdrCreditsConsumed, mFcTxPostedDataCreditLimit, mFcTxPostedDataCreditsConsumed);
		  fflush(0);
	       }
	    }
	 }
	 break;
      case PCIExpressTransaction::NonPosted:
	 if (((mFcTxNonPostedHdrCreditLimit == 0xFFFFFFFF) ||   // unlimited
	      (((mFcTxNonPostedHdrCreditLimit - mFcTxNonPostedHdrCreditsConsumed) & 0xFF) >= transaction->getFCHeaderNum())) &&
	     ((mFcTxNonPostedDataCreditLimit == 0xFFFFFFFF) ||  // unlimited
	      (((mFcTxNonPostedDataCreditLimit - mFcTxNonPostedDataCreditsConsumed) & 0xFFF) >= transaction->getFCDataNum()))
	     // [ No need to check the data length here, its only 0 or 1 DWs ]
	     ) {                               // consume NonPosted TX credits
	    mFcTxNonPostedHdrCreditsConsumed += transaction->getFCHeaderNum();
	    mFcTxNonPostedHdrCreditsConsumed &= 0xFF;
	    mFcTxNonPostedDataCreditsConsumed += transaction->getFCDataNum();
	    mFcTxNonPostedDataCreditsConsumed &= 0xFFF;
	    transaction->setFCCreditsConsumed();
	    flowControlAccepted = true;
	    if (VerboseReceiverMedium) {
	       printf("%s  Info: Flow control credits consumed: Non-Posted HeaderFC increased by 0x%02X to 0x%02X, DataFC increased by 0x%03X to 0x%03X.\n", printName, transaction->getFCHeaderNum(), mFcTxNonPostedHdrCreditsConsumed, transaction->getFCDataNum(), mFcTxNonPostedDataCreditsConsumed);
	       fflush(0);
	    }
	 } else {
	    flowControlAccepted = false;
	    if (VerboseReceiverMedium) {
	       printf("%s  Info: Flow control credits unavailable: Non-Posted HeaderFC requested 0x%02X with 0x%02X available, DataFC requested 0x%03X with 0x%03X available.\n", printName, transaction->getFCHeaderNum(), ((mFcTxNonPostedHdrCreditLimit - mFcTxNonPostedHdrCreditsConsumed) & 0xFF), transaction->getFCDataNum(), ((mFcTxNonPostedDataCreditLimit - mFcTxNonPostedDataCreditsConsumed) & 0xFFF));
	       printf("%s                                          HeaderFC advertised 0x%02X, HeaderFC Consumed 0x%02X, DataFC advertised 0x%03X, DataFC consumed 0x%03X\n", printName, mFcTxNonPostedHdrCreditLimit, mFcTxNonPostedHdrCreditsConsumed, mFcTxNonPostedDataCreditLimit, mFcTxNonPostedDataCreditsConsumed);
	       fflush(0);
	    }
	 }
	 break;
      case PCIExpressTransaction::Completion:
	 if (((mFcTxCompletionHdrCreditLimit == 0xFFFFFFFF) ||   // unlimited
	      (((mFcTxCompletionHdrCreditLimit - mFcTxCompletionHdrCreditsConsumed) & 0xFF) >= transaction->getFCHeaderNum())) &&
	     ((mFcTxCompletionDataCreditLimit == 0xFFFFFFFF) ||  // unlimited
	      (((mFcTxCompletionDataCreditLimit - mFcTxCompletionDataCreditsConsumed) & 0xFFF) >= transaction->getFCDataNum())) &&
	     (transaction->getDataLength() <= mMaxPayloadSizeDW)
	     ) {                               // consume Completion TX credits
	    mFcTxCompletionHdrCreditsConsumed += transaction->getFCHeaderNum();
	    mFcTxCompletionHdrCreditsConsumed &= 0xFF;
	    mFcTxCompletionDataCreditsConsumed += transaction->getFCDataNum();
	    mFcTxCompletionDataCreditsConsumed &= 0xFFF;
	    transaction->setFCCreditsConsumed();
	    flowControlAccepted = true;
	    if (VerboseReceiverMedium) {
	       printf("%s  Info: Flow control credits consumed: Completion HeaderFC increased by 0x%02X to 0x%02X, DataFC increased by 0x%03X to 0x%03X.\n", printName, transaction->getFCHeaderNum(), mFcTxCompletionHdrCreditsConsumed, transaction->getFCDataNum(), mFcTxCompletionDataCreditsConsumed);
	       fflush(0);
	    }
	 } else {
	    flowControlAccepted = false;
	    if (transaction->getDataLength() > mMaxPayloadSizeDW) {
	       if (VerboseReceiverMedium)
		  printf("%s  Info: %s transaction has data payload (%d bytes) that is greater than the Max_Payload_Size parameter (%d bytes).  It will be split into multiple transactions.\n", printName, transaction->getPrintString(), (transaction->getDataLength() * 4), (mMaxPayloadSizeDW * 4));
	    } else {
	       if (VerboseReceiverMedium) {
		  printf("%s  Info: Flow control credits unavailable: Completion HeaderFC requested 0x%02X with 0x%02X available, DataFC requested 0x%03X with 0x%03X available.\n", printName, transaction->getFCHeaderNum(), ((mFcTxCompletionHdrCreditLimit - mFcTxCompletionHdrCreditsConsumed) & 0xFF), transaction->getFCDataNum(), ((mFcTxCompletionDataCreditLimit - mFcTxCompletionDataCreditsConsumed) & 0xFFF));
		  printf("%s                                          HeaderFC advertised 0x%02X, HeaderFC Consumed 0x%02X, DataFC advertised 0x%03X, DataFC consumed 0x%03X\n", printName, mFcTxCompletionHdrCreditLimit, mFcTxCompletionHdrCreditsConsumed, mFcTxCompletionDataCreditLimit, mFcTxCompletionDataCreditsConsumed);
		  fflush(0);
	       }
	    }
	 }
	 break;
      default:
	 flowControlAccepted = true; // don't block all work because of this error
	 printf("%s  ERROR: Attempting to consume flow control credits of unknown type for transaction '%s'.\n", printName, transaction->getPrintString());
	 fflush(0);
	 break;
      }
   }
   return flowControlAccepted;
}

// Take the transaction pointed to by (**pIter) and see if there are enough credits to
// split the completion and put the first part on the bus now.  If so, remove the
// original transaction and replace it with two transactions, the part that fit on the
// bus now and the rest.
bool PCIExpressTransactor::splitDataTransaction(bool isCpl, PCIExpressTransactionList::iterator iter) {
   PCIECmd *pciecmd_orig, *pciecmd_new1, *pciecmd_new2;
   PCIExpressTransaction *pcietx_new1, *pcietx_new2;
   bool retStatus;
   UInt32 minDWsNeeded = 0;
   UInt32 minDataCreditsNeeded = 0;
   UInt32 maxDataPayloadSizeCredits = 0;
   UInt32 maxDataCreditsAvailable = 0;
   UInt32 fullChunkCreditsUsed = 0;
   UInt32 totalDWsUsed = 0;
   UInt32 totalDataCreditsUsed = 0;

   UInt32 txHeaderCreditLimit;
   UInt32 txHeaderCreditsConsumed;
   UInt32 txDataCreditLimit;
   UInt32 txDataCreditsConsumed;

   if (isCpl) {  // transaction is completion
      txHeaderCreditLimit = mFcTxCompletionHdrCreditLimit;
      txHeaderCreditsConsumed = mFcTxCompletionHdrCreditsConsumed;
      txDataCreditLimit = mFcTxCompletionDataCreditLimit;
      txDataCreditsConsumed = mFcTxCompletionDataCreditsConsumed;
   } else {      // transaction is write
      txHeaderCreditLimit = mFcTxPostedHdrCreditLimit;
      txHeaderCreditsConsumed = mFcTxPostedHdrCreditsConsumed;
      txDataCreditLimit = mFcTxPostedDataCreditLimit;
      txDataCreditsConsumed = mFcTxPostedDataCreditsConsumed;
   }

   // no header credits - fail
   if ((txHeaderCreditLimit != 0xFFFFFFFF) &&   // not unlimited
       (((txHeaderCreditLimit - txHeaderCreditsConsumed) & 0xFF) < (*iter)->getFCHeaderNum())
       ) {
      return false;
   }

   pciecmd_orig = (*iter)->getPCIECmd();

   // Can only split on 128 Byte boundaries for completions,
   // writes are just going to do the same
   if (isCpl) {  // transaction is completion
      minDWsNeeded = (0x80 - (pciecmd_orig->getLowAddr() & 0x7C)) / 4;
   } else {      // transaction is write
      minDWsNeeded = (0x80 - (pciecmd_orig->getAddr() & 0x7C)) / 4;
   }
   if (pciecmd_orig->getDataLength() < minDWsNeeded)
      minDWsNeeded = pciecmd_orig->getDataLength();
   minDataCreditsNeeded = (minDWsNeeded + 3) / 4;  // credits are in 4-DW units

   if (minDataCreditsNeeded == 0) {
      return false;   // this shouldn't happen, but it doesn't allow for splitting the transaction
   }
   
   maxDataPayloadSizeCredits = (mMaxPayloadSizeDW + 3) / 4;  // max credits allowed by Max_Payload_Size
   if (txDataCreditLimit == 0xFFFFFFFF) { // unlimited
      maxDataCreditsAvailable = maxDataPayloadSizeCredits;
   } else {
      maxDataCreditsAvailable = (txDataCreditLimit - txDataCreditsConsumed) & 0xFFF;
      if (maxDataCreditsAvailable > maxDataPayloadSizeCredits)
	 maxDataCreditsAvailable = maxDataPayloadSizeCredits;
   }

   // not enough data credits - fail
   if (maxDataCreditsAvailable < minDataCreditsNeeded) {
      return false;
   }

   // FOR COMPLETIONS:
   // Have enough to get something on the bus.
   // 1) Find the largest subsection that will fit and split there
   // 2) Modify 'Length' and data of each transaction
   // 3) Modify 'Byte Count' of second transaction to be 'Byte Count'
   //    of first transaction - number of bytes sent in first transaction
   // 4) Modify LowAddr() to be 0x00 in second transaction
   // FOR WRITES:
   // Have enough to get something on the bus.
   // 1) Find the largest subsection that will fit and split there
   // 2) Modify 'Length' and data of each transaction
   // 3) Modify 'Addr' of second transaction
   // 4) Modify 'ldwbe' of first be 0xF, 'fdwbe' of second be 0xF

   // 128 Byte chunks = 32 DW chunks = 8 credit chunks are the minimum allowed granularity
   // Don't have to check if we are going over the total credits needed for this
   //   transaction, we know that we don't have enough credits for the entire
   //   transaction.

   // How many credits available as full 128 Byte chunks (after the minimum needed)?
   fullChunkCreditsUsed = (maxDataCreditsAvailable - minDataCreditsNeeded) & 0xFFFFFFF8;
   // Total Credits Used
   totalDataCreditsUsed = fullChunkCreditsUsed + minDataCreditsNeeded;
   // Total DWs Used in that span of credits
   totalDWsUsed = (fullChunkCreditsUsed * 4) + minDWsNeeded;

   //----

   pciecmd_new1 = new PCIECmd(*pciecmd_orig);  // make copy for new smaller transaction
   pciecmd_new2 = new PCIECmd(*pciecmd_orig);  // make copy for the rest

   // Modify the PCIECmd objects
   //       changing the amount of data in the object will change the 'DataLength' also, not the byte count
   pciecmd_new1->resizeDataBytes(0, (totalDWsUsed * 4) - 1);                                   // keep only the first X bytes of data
   pciecmd_new1->setDataLength(totalDWsUsed);
   pciecmd_new2->resizeDataBytes((totalDWsUsed * 4), ((pciecmd_new2->getDataLength() * 4) - 1));  // keep only the rest of the data
   pciecmd_new2->setDataLength(pciecmd_new2->getDataLength() - totalDWsUsed);
   if (isCpl) {  // transaction is completion
      pciecmd_new2->setLowAddr(0x0);                                                    // second will always start on a 128B boundary
      pciecmd_new2->setByteCount( pciecmd_new2->getByteCount() - (totalDWsUsed * 4) );  // second has byte count of orig minus what is sent in first
   } else {      // transaction is write
      pciecmd_new1->setLastBE(0xF);
      pciecmd_new2->setFirstBE(0xF);
      pciecmd_new2->setAddr(pciecmd_new2->getAddr() + (totalDWsUsed * 4));
   }

   pcietx_new1 = new PCIExpressTransaction(this, pciecmd_new1);
   pcietx_new2 = new PCIExpressTransaction(this, pciecmd_new2);
   if (pcietx_new1->isValid() && pcietx_new2->isValid()) {
      retStatus = true;
      delete (PCIExpressTransaction*)(*iter);  // delete old transaction from queue
//      (*iter) = pcietx_new2;                   // put second new transaction in its place
      (*iter) = 0;  // tag for removal
      ++iter;  // insert After this one
      iter = mTransactionListNew.insert(iter, pcietx_new2); // insert second new tx before 'iter'
      iter = mTransactionListNew.insert(iter, pcietx_new1); // insert first new tx before 'iter'
   } else {
      retStatus = false;
      printf("%s  ERROR: Attempt to split %s transaction, to pass flow control, failed to produce valid transactions.\n", printName, (*iter)->getPrintString());
      // only delete if not putting on transaction queue
      if (pcietx_new1 != 0) delete pcietx_new1;
      if (pcietx_new2 != 0) delete pcietx_new2;
   }

   //----

   if (pciecmd_new1 != 0) delete pciecmd_new1;
   if (pciecmd_new2 != 0) delete pciecmd_new2;
   return retStatus;
}


void PCIExpressTransactor::deleteTransactionQueue(void) {
   PCIExpressTransactionList::const_iterator iter;
   for (iter = mTransactionListNew.begin(); iter != mTransactionListNew.end(); ++iter) {
      delete (PCIExpressTransaction*)(*iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNew.clear();

   for (iter = mTransactionListNeedAck.begin(); iter != mTransactionListNeedAck.end(); ++iter) {
      delete (PCIExpressTransaction*)(*iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNeedAck.clear();

   for (iter = mTransactionListNeedCpl.begin(); iter != mTransactionListNeedCpl.end(); ++iter) {
      delete (PCIExpressTransaction*)(*iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNeedCpl.clear();
   return;
}

void PCIExpressTransactor::processCompletion(PCIECmd *cmd) {
   PCIExpressTransactionList::iterator iter;
   bool foundIt = false;

   for (iter = mTransactionListNeedCpl.begin(); iter != mTransactionListNeedCpl.end(); ++iter) {
      if ((signed)(*iter)->getTxID() == cmd->getTransId()) {
	 foundIt = true;
	    if (VerboseTransmitterMedium)
	       printf("%s  Info: Matched completion transaction to '%s' from the transaction queue.\n", printName, (*iter)->getPrintString());
	    // Don't clear the flag if we receive a split completion.  The last
	    //  completion will have the byte count equal to the data length
	    //  (minus the masked off bytes), all else will have the data length
	    //  be less than the byte count [remaining].
	    // If completion has an error status then it will be the last
	    //  completion for this transaction
	    // This works for non-data completions as well because both the data
	    //  length and byte count are zero.
	    if (((cmd->getCmdByName() == PCIECmd::Cpl) ||              // is cpl without data
		 (cmd->getCmdByName() == PCIECmd::CplLk)) ||
		(cmd->getCplStatus() != 0x0) ||                        // or error completion
		(((cmd->getDataLength() * 4) - (cmd->getLowAddr() & 0x3))
		     >= cmd->getByteCount())) {                        // or last data cpl

	       if(VerboseTransmitterMedium)
		  printf("%s  Info: Deleting transaction '%s' from the transaction queue, sequence number 0x%03X.\n", printName, (*iter)->getPrintString(), (*iter)->getSeqNum());
	       --mNumTransInFlight;
	       delete (PCIExpressTransaction*)(*iter);     // must explicitly delete allocated space
	       // Here 'iter' may be invalid if we delete the first item, don't use 'iter' any more (hense the following break statement)
	       iter = mTransactionListNeedCpl.erase(iter); // return pointer to next element (or end)

	    }
	 break;
      }
   }

   if (not foundIt) {
      // This error should be printed unless the transaction was removed from the queue because
      // it didn't require a completion...
      // Again, don't print the message if we're getting the completion message only because it
      // indicates an error.
      if((cmd->getCplStatus() == 0x0) && (VerboseTransmitterLow))
	 printf("%s  ERROR: Received a completion with a transaction ID (0x%03X) that does not match any transaction left on the queue.\n", printName, cmd->getTransId());
   }

   return;
}

void PCIExpressTransactor::processAck(UInt32 seqNum) {
   PCIExpressTransactionList::iterator iter;
   bool foundIt = false;

   // error, can't have new Ack that is greater than 2048 more than
   // the last ack seq number
   if (((seqNum - mLastRxAckSeqNum) & 0xFFF) >= 0x7FF) {
      if (VerboseReceiverLow)
	 printf("%s  ERROR: Received an invalid Ack, new sequence number (0x%03X) is more than 2048 higher than the previous Ack sequence number (0x%03X).  Discarding Ack.\n", printName, seqNum, mLastRxAckSeqNum);
      return;
   }

   // We DEPEND on the transaction list staying in order
   // so sequence numbers are in order and replays happen in order
   for (iter = mTransactionListNeedAck.begin(); iter != mTransactionListNeedAck.end(); ++iter) {
      // Need to handle 12-bit register rollover with logical equation:
      //     ((*iter)->getSeqNum() < seqNum)
      if ((((*iter)->getSeqNum() - seqNum) & 0xFFF) > 0x7FF) {
	 if (VerboseTransmitterHigh)
	    printf("%s  Info: Received Ack for sequence number 0x%03X, setting Ack received for sequence number 0x%03X and all earlier.\n", printName, seqNum, (*iter)->getSeqNum());
	 (*iter)->clearNeedsAck();

	 if ((*iter)->needsCpl()) {
	    mTransactionListNeedCpl.push_back(*iter);
	 } else {
	    if (VerboseTransmitterMedium)
	       printf("%s  Info: Deleting transaction '%s' from the transaction queue, sequence number 0x%03X.\n", printName, (*iter)->getPrintString(), (*iter)->getSeqNum());
	    if (not (*iter)->isCompletion())
	       --mNumTransInFlight;
	    delete (PCIExpressTransaction*)(*iter);     // must explicitly delete allocated space
	 }
	 (*iter) = 0;  // tag for deletion

      } else if ((*iter)->getSeqNum() == seqNum) {
	 foundIt = true;

	 if (VerboseTransmitterHigh)
	    printf("%s  Info: Matched Ack to '%s' with sequence number 0x%03X from the transaction queue.\n", printName, (*iter)->getPrintString(), (*iter)->getSeqNum());
	 (*iter)->clearNeedsAck();
	 mLastRxAckSeqNum = seqNum;

	 if ((*iter)->needsCpl()) {
	    mTransactionListNeedCpl.push_back(*iter);
	 } else {
	    if (VerboseTransmitterMedium)
	       printf("%s  Info: Deleting transaction '%s' from the transaction queue, sequence number 0x%03X.\n", printName, (*iter)->getPrintString(), (*iter)->getSeqNum());
	    if (not (*iter)->isCompletion())
	       --mNumTransInFlight;
	    delete (PCIExpressTransaction*)(*iter);     // must explicitly delete allocated space
	 }
	 (*iter) = 0;  // tag for deletion

      } else {
	 break;
      }
   }

   if ((not foundIt) && (seqNum != mLastRxAckSeqNum)) {  // OK to repeatedly Ack mLastRxAckSeqNum (spec rev1.0a, section 3.5.2.1)
      if(VerboseTransmitterLow)
	 printf("%s  ERROR: Received an Ack for a sequence number (0x%03X) that does not match any transaction on the queue.  Last Ack'd sequence number is 0x%03X.\n", printName, seqNum, mLastRxAckSeqNum);
   }

   for (iter = mTransactionListNeedAck.begin(); iter != mTransactionListNeedAck.end(); ) {
      if ((*iter) == 0)
	 iter = mTransactionListNeedAck.erase(iter);
      else
	 ++iter;
   }

   return;
}

void PCIExpressTransactor::processNak(UInt32 seqNum) {
   PCIExpressTransactionList::const_iterator iter;

   // error, can't have new Ack that is greater than 2048 more than
   // the last ack seq number
   if (((seqNum - mLastRxAckSeqNum) & 0xFFF) >= 0x7FF) {
      if (VerboseReceiverLow)
	 printf("%s  ERROR: Received an invalid Nak, new sequence number (0x%03X) is more than 2048 higher than the previous Ack sequence number (0x%03X).  Discarding Nak.\n", printName, seqNum, mLastRxAckSeqNum);
      return;
   }

   // Nak should be with SeqNum of mLastRxAckSeqNum, but if its later...
   //   then does that mean Ack up to SeqNum and Nak afterwords?
                         // Need to handle 12-bit register rollover with logical equation:
   if (((mLastRxAckSeqNum - seqNum) & 0xFFF) > 0x7FF) {        // (seqNum > mLastRxAckSeqNum)
      processAck(seqNum);
   } else if (((seqNum - mLastRxAckSeqNum) & 0xFFF) > 0x7FF) { // (seqNum < mLastRxAckSeqNum)
      if (VerboseReceiverLow)
	 printf("%s  ERROR: Received a Nak for a transaction that has already been acknowledged. Last Ack sequence number is 0x%03X, this Nak sequence number is 0x%03X.\n", printName, mLastRxAckSeqNum, seqNum);
      return;  // do nothing
   }

   if (VerboseTransmitterLow)
      printf("%s  Info: Received Nak for sequence number 0x%03X, replaying all not acknowledged transactions (at %s).\n", printName, seqNum, mCurrTimeString);

   mReplayTransactions = true;
   return;
}

void PCIExpressTransactor::updateTransactionsForNak() {
   // we know that all transactions are done (not in progress) so we can
   // quickly put them back into the 'new' queue
   if (mTransactionInProgress2 != 0) {
      mTransactionListNew.push_front(mTransactionInProgress2);
      mTransactionInProgress2 = 0;
   }
   if (mTransactionInProgress1 != 0) {
      mTransactionListNew.push_front(mTransactionInProgress1);
      mTransactionInProgress1 = 0;
   }

   // move all of mTransactionListNeedAck into mTransactionListNew before begin()
   // mTransactionListNeedAck ends up clear
   mTransactionListNew.splice(mTransactionListNew.begin(), mTransactionListNeedAck);

   mReplayTransactions = false;
   return;
}


UInt32 PCIExpressTransactor::encoder_lut(UInt32 data_in, bool kin, UInt32 laneNum) {
    UInt32 data_out;
    bool rdisp;
    int sum, i;

    rdisp = disparity_out[laneNum];

    // printf("%s  Transmitter encountered data to be 8b/10b encoded: (%c)0x%X (lane %d)\n",printName,(kin?'K':'D'),data_in,laneNum);
    if(data_in > 0xFF) {
	if(VerboseTransmitterMedium)
	    printf("%s  ERROR: Transmitter encountered invalid data to be 8b/10b encoded: (%c)0x%X (lane %d)\n",printName,(kin?'K':'D'),data_in,laneNum);
	return(0xFFF); // invalid input data!
    }
    // data_in = data_in & 0xFF;

    if(kin) {        // have a special char symbol
	if(rdisp)
	    data_out = lut_spec_10bit_p[data_in];
	else
	    data_out = lut_spec_10bit_n[data_in];
    } else {        // have a data char symbol
	if(rdisp)
	    data_out = lut_data_10bit_p[data_in];
	else
	    data_out = lut_data_10bit_n[data_in];
    }
    // Generate new rdisp and put in *disp.
    // disparity of true = +1, false = -1
    // if data_out has six 1s & four 0s, its disparity = +2 and is added to the input disparity
    // if data_out has five 1s & five 0s, its disparity = 0 and is added to the input disparity
    // if data_out has four 1s & six 0s, its disparity = -2 and is added to the input disparity
    for(i=0,sum=0; i<10; ++i)
	sum += ((data_out >> i) & 1);

    // printf(":: sum equals %d\n",sum);
    if (sum == 5) ;                  // dont change
    else if ((sum == 4) && rdisp)    // set to -1
	rdisp = false;
    else if ((sum == 6) && !rdisp)   // set to +1
	rdisp = true;
    else
	if(VerboseTransmitterLow)
	    printf("%s  ERROR: Can't determine new disparity, Bad Symbol Sent Out. (disp=%s, K=%s, ByteIn=0x%02X, PacketOut=0x%03X)\n", printName, (rdisp ? "(+)" : "(-)"), (kin ? "true" : "false"), data_in, data_out);

    disparity_out[laneNum] = rdisp;  // save the new encoding disparity value
    return(data_out);
}

UInt32 PCIExpressTransactor::decoder_lut(UInt32 data_in, bool *kout, UInt32 laneNum)
{
    UInt32 data_out, i;
    bool rdisp, rk;
    bool rdisp_not_set;
    bool found_it;
    int sum,j;

    if(VerboseReceiverHigh)
	printf("          Decoder_Lut: data_in = 0x%03X, ",data_in);

    rdisp = disparity_in[laneNum];
    rdisp_not_set = disparity_in_not_set[laneNum];
    rk = true;
    found_it = false;

    if(data_in > 0x3FF)
	return(0xFF);  // invalid input data!
    data_out = 0xFF;


    // Generate new rdisp and put in *disp.
    // disparity of true = +1, false = -1
    // if data_in has six 1s & four 0s, its disparity = +2 and is added to the input disparity
    // if data_in has five 1s & five 0s, its disparity = 0 and is added to the input disparity
    // if data_in has four 1s & six 0s, its disparity = -2 and is added to the input disparity
    for(j=0,sum=0; j<10; ++j)
	sum += ((data_in >> j) & 1);

    // printf(":: sum equals %d\n",sum);
    if (sum == 5) ;                                         // dont change
    else if ((sum == 4) && ( rdisp || rdisp_not_set)) {     // set to -1
	disparity_in[laneNum] = false;
	disparity_in_not_set[laneNum] = false;
    } else if ((sum == 6) && (!rdisp || rdisp_not_set)) {   // set to +1
	disparity_in[laneNum] = true;
	disparity_in_not_set[laneNum] = false;
    } else
	if(VerboseReceiverLow)
	    printf("%s  ERROR: Can't determine new disparity, Bad Symbol Read In.\n", printName);

    if(VerboseReceiverHigh)
	printf(" rdisp = %d%s,",rdisp, (rdisp_not_set ? " (NOT SET)" : ""));
    if( (rdisp) || (rdisp_not_set) ) {
	for (i=0; i<=0xFF; ++i) {                       // look for special codes first
	    if(lut_spec_10bit_p[i] == data_in) {
		data_out = i;
		rk = true;
		found_it = true;
		break;
	    }
	}
	if(!found_it) {                                 // if not found
	    for (i=0; i<=0xFF; ++i) {                   // look for data translation
		if(lut_data_10bit_p[i] == data_in) {
		    data_out = i;
		    rk = false;
		    found_it = true;
		    break;
		}
	    }
	}
    }
    if( (!rdisp) || (rdisp_not_set && !found_it) ) {
	for (i=0; i<=0xFF; ++i) {                       // look for special codes first
	    if(lut_spec_10bit_n[i] == data_in) {
		data_out = i;
		rk = true;
		found_it = true;
		break;
	    }
	}
	if(!found_it) {                                 // if not found
	    for (i=0; i<=0xFF; ++i) {                   // look for data translation
		if(lut_data_10bit_n[i] == data_in) {
		    data_out = i;
		    rk = false;
		    found_it = true;
		    break;
		}
	    }
	}
    }
    if (!found_it) {
	disparity_in_not_set[laneNum] = true;
	if (VerboseReceiverLow)
	    printf("%s  ERROR: Bad Symbol Read In, Invalid 10-bit Value: 0x%03X.  (disparity=%d, lane=%d, time=%s)\n", printName, data_in, rdisp, laneNum, mCurrTimeString);
    }

    if(VerboseReceiverHigh)
	printf(" K = %d, DataOut = 0x%02X\n",rk, data_out);

    (*kout) = rk;
    return(data_out);
}

void PCIExpressTransactor::init_luts()
{
    int i;

    for (i=0; i<=0xFF; ++i) {
	lut_spec_10bit_p[i] = 0xFFF;
	lut_spec_10bit_n[i] = 0xFFF;
    }
    lut_spec_10bit_p[0x1C] = 0x30B;
    lut_spec_10bit_p[0x3C] = 0x306;
    lut_spec_10bit_p[0x5C] = 0x30A;
    lut_spec_10bit_p[0x7C] = 0x30C;
    lut_spec_10bit_p[0x9C] = 0x30D;
    lut_spec_10bit_p[0xBC] = 0x305;
    lut_spec_10bit_p[0xDC] = 0x309;
    lut_spec_10bit_p[0xFC] = 0x307;
    lut_spec_10bit_p[0xF7] = 0x057;
    lut_spec_10bit_p[0xFB] = 0x097;
    lut_spec_10bit_p[0xFD] = 0x117;
    lut_spec_10bit_p[0xFE] = 0x217;
    lut_spec_10bit_n[0x1C] = 0x0F4;
    lut_spec_10bit_n[0x3C] = 0x0F9;
    lut_spec_10bit_n[0x5C] = 0x0F5;
    lut_spec_10bit_n[0x7C] = 0x0F3;
    lut_spec_10bit_n[0x9C] = 0x0F2;
    lut_spec_10bit_n[0xBC] = 0x0FA;
    lut_spec_10bit_n[0xDC] = 0x0F6;
    lut_spec_10bit_n[0xFC] = 0x0F8;
    lut_spec_10bit_n[0xF7] = 0x3A8;
    lut_spec_10bit_n[0xFB] = 0x368;
    lut_spec_10bit_n[0xFD] = 0x2E8;
    lut_spec_10bit_n[0xFE] = 0x1E8;


    // INITIALIZE LOOK-UP TABLES
	lut_data_10bit_p[0x00] = 0x18B;
	lut_data_10bit_p[0x01] = 0x22B;
	lut_data_10bit_p[0x02] = 0x12B;
	lut_data_10bit_p[0x03] = 0x314;
	lut_data_10bit_p[0x04] = 0x0AB;
	lut_data_10bit_p[0x05] = 0x294;
	lut_data_10bit_p[0x06] = 0x194;
	lut_data_10bit_p[0x07] = 0x074;
	lut_data_10bit_p[0x08] = 0x06B;
	lut_data_10bit_p[0x09] = 0x254;
	lut_data_10bit_p[0x0A] = 0x154;
	lut_data_10bit_p[0x0B] = 0x344;
	lut_data_10bit_p[0x0C] = 0x0D4;
	lut_data_10bit_p[0x0D] = 0x2C4;
	lut_data_10bit_p[0x0E] = 0x1C4;
	lut_data_10bit_p[0x0F] = 0x28B;
	lut_data_10bit_p[0x10] = 0x24B;
	lut_data_10bit_p[0x11] = 0x234;
	lut_data_10bit_p[0x12] = 0x134;
	lut_data_10bit_p[0x13] = 0x324;
	lut_data_10bit_p[0x14] = 0x0B4;
	lut_data_10bit_p[0x15] = 0x2A4;
	lut_data_10bit_p[0x16] = 0x1A4;
	lut_data_10bit_p[0x17] = 0x05B;
	lut_data_10bit_p[0x18] = 0x0CB;
	lut_data_10bit_p[0x19] = 0x264;
	lut_data_10bit_p[0x1A] = 0x164;
	lut_data_10bit_p[0x1B] = 0x09B;
	lut_data_10bit_p[0x1C] = 0x0E4;
	lut_data_10bit_p[0x1D] = 0x11B;
	lut_data_10bit_p[0x1E] = 0x21B;
	lut_data_10bit_p[0x1F] = 0x14B;
	lut_data_10bit_p[0x20] = 0x189;
	lut_data_10bit_p[0x21] = 0x229;
	lut_data_10bit_p[0x22] = 0x129;
	lut_data_10bit_p[0x23] = 0x319;
	lut_data_10bit_p[0x24] = 0x0A9;
	lut_data_10bit_p[0x25] = 0x299;
	lut_data_10bit_p[0x26] = 0x199;
	lut_data_10bit_p[0x27] = 0x079;
	lut_data_10bit_p[0x28] = 0x069;
	lut_data_10bit_p[0x29] = 0x259;
	lut_data_10bit_p[0x2A] = 0x159;
	lut_data_10bit_p[0x2B] = 0x349;
	lut_data_10bit_p[0x2C] = 0x0D9;
	lut_data_10bit_p[0x2D] = 0x2C9;
	lut_data_10bit_p[0x2E] = 0x1C9;
	lut_data_10bit_p[0x2F] = 0x289;
	lut_data_10bit_p[0x30] = 0x249;
	lut_data_10bit_p[0x31] = 0x239;
	lut_data_10bit_p[0x32] = 0x139;
	lut_data_10bit_p[0x33] = 0x329;
	lut_data_10bit_p[0x34] = 0x0B9;
	lut_data_10bit_p[0x35] = 0x2A9;
	lut_data_10bit_p[0x36] = 0x1A9;
	lut_data_10bit_p[0x37] = 0x059;
	lut_data_10bit_p[0x38] = 0x0C9;
	lut_data_10bit_p[0x39] = 0x269;
	lut_data_10bit_p[0x3A] = 0x169;
	lut_data_10bit_p[0x3B] = 0x099;
	lut_data_10bit_p[0x3C] = 0x0E9;
	lut_data_10bit_p[0x3D] = 0x119;
	lut_data_10bit_p[0x3E] = 0x219;
	lut_data_10bit_p[0x3F] = 0x149;
	lut_data_10bit_p[0x40] = 0x185;
	lut_data_10bit_p[0x41] = 0x225;
	lut_data_10bit_p[0x42] = 0x125;
	lut_data_10bit_p[0x43] = 0x315;
	lut_data_10bit_p[0x44] = 0x0A5;
	lut_data_10bit_p[0x45] = 0x295;
	lut_data_10bit_p[0x46] = 0x195;
	lut_data_10bit_p[0x47] = 0x075;
	lut_data_10bit_p[0x48] = 0x065;
	lut_data_10bit_p[0x49] = 0x255;
	lut_data_10bit_p[0x4A] = 0x155;
	lut_data_10bit_p[0x4B] = 0x345;
	lut_data_10bit_p[0x4C] = 0x0D5;
	lut_data_10bit_p[0x4D] = 0x2C5;
	lut_data_10bit_p[0x4E] = 0x1C5;
	lut_data_10bit_p[0x4F] = 0x285;
	lut_data_10bit_p[0x50] = 0x245;
	lut_data_10bit_p[0x51] = 0x235;
	lut_data_10bit_p[0x52] = 0x135;
	lut_data_10bit_p[0x53] = 0x325;
	lut_data_10bit_p[0x54] = 0x0B5;
	lut_data_10bit_p[0x55] = 0x2A5;
	lut_data_10bit_p[0x56] = 0x1A5;
	lut_data_10bit_p[0x57] = 0x055;
	lut_data_10bit_p[0x58] = 0x0C5;
	lut_data_10bit_p[0x59] = 0x265;
	lut_data_10bit_p[0x5A] = 0x165;
	lut_data_10bit_p[0x5B] = 0x095;
	lut_data_10bit_p[0x5C] = 0x0E5;
	lut_data_10bit_p[0x5D] = 0x115;
	lut_data_10bit_p[0x5E] = 0x215;
	lut_data_10bit_p[0x5F] = 0x145;
	lut_data_10bit_p[0x60] = 0x18C;
	lut_data_10bit_p[0x61] = 0x22C;
	lut_data_10bit_p[0x62] = 0x12C;
	lut_data_10bit_p[0x63] = 0x313;
	lut_data_10bit_p[0x64] = 0x0AC;
	lut_data_10bit_p[0x65] = 0x293;
	lut_data_10bit_p[0x66] = 0x193;
	lut_data_10bit_p[0x67] = 0x073;
	lut_data_10bit_p[0x68] = 0x06C;
	lut_data_10bit_p[0x69] = 0x253;
	lut_data_10bit_p[0x6A] = 0x153;
	lut_data_10bit_p[0x6B] = 0x343;
	lut_data_10bit_p[0x6C] = 0x0D3;
	lut_data_10bit_p[0x6D] = 0x2C3;
	lut_data_10bit_p[0x6E] = 0x1C3;
	lut_data_10bit_p[0x6F] = 0x28C;
	lut_data_10bit_p[0x70] = 0x24C;
	lut_data_10bit_p[0x71] = 0x233;
	lut_data_10bit_p[0x72] = 0x133;
	lut_data_10bit_p[0x73] = 0x323;
	lut_data_10bit_p[0x74] = 0x0B3;
	lut_data_10bit_p[0x75] = 0x2A3;
	lut_data_10bit_p[0x76] = 0x1A3;
	lut_data_10bit_p[0x77] = 0x05C;
	lut_data_10bit_p[0x78] = 0x0CC;
	lut_data_10bit_p[0x79] = 0x263;
	lut_data_10bit_p[0x7A] = 0x163;
	lut_data_10bit_p[0x7B] = 0x09C;
	lut_data_10bit_p[0x7C] = 0x0E3;
	lut_data_10bit_p[0x7D] = 0x11C;
	lut_data_10bit_p[0x7E] = 0x21C;
	lut_data_10bit_p[0x7F] = 0x14C;
	lut_data_10bit_p[0x80] = 0x18D;
	lut_data_10bit_p[0x81] = 0x22D;
	lut_data_10bit_p[0x82] = 0x12D;
	lut_data_10bit_p[0x83] = 0x312;
	lut_data_10bit_p[0x84] = 0x0AD;
	lut_data_10bit_p[0x85] = 0x292;
	lut_data_10bit_p[0x86] = 0x192;
	lut_data_10bit_p[0x87] = 0x072;
	lut_data_10bit_p[0x88] = 0x06D;
	lut_data_10bit_p[0x89] = 0x252;
	lut_data_10bit_p[0x8A] = 0x152;
	lut_data_10bit_p[0x8B] = 0x342;
	lut_data_10bit_p[0x8C] = 0x0D2;
	lut_data_10bit_p[0x8D] = 0x2C2;
	lut_data_10bit_p[0x8E] = 0x1C2;
	lut_data_10bit_p[0x8F] = 0x28D;
	lut_data_10bit_p[0x90] = 0x24D;
	lut_data_10bit_p[0x91] = 0x232;
	lut_data_10bit_p[0x92] = 0x132;
	lut_data_10bit_p[0x93] = 0x322;
	lut_data_10bit_p[0x94] = 0x0B2;
	lut_data_10bit_p[0x95] = 0x2A2;
	lut_data_10bit_p[0x96] = 0x1A2;
	lut_data_10bit_p[0x97] = 0x05D;
	lut_data_10bit_p[0x98] = 0x0CD;
	lut_data_10bit_p[0x99] = 0x262;
	lut_data_10bit_p[0x9A] = 0x162;
	lut_data_10bit_p[0x9B] = 0x09D;
	lut_data_10bit_p[0x9C] = 0x0E2;
	lut_data_10bit_p[0x9D] = 0x11D;
	lut_data_10bit_p[0x9E] = 0x21D;
	lut_data_10bit_p[0x9F] = 0x14D;
	lut_data_10bit_p[0xA0] = 0x18A;
	lut_data_10bit_p[0xA1] = 0x22A;
	lut_data_10bit_p[0xA2] = 0x12A;
	lut_data_10bit_p[0xA3] = 0x31A;
	lut_data_10bit_p[0xA4] = 0x0AA;
	lut_data_10bit_p[0xA5] = 0x29A;
	lut_data_10bit_p[0xA6] = 0x19A;
	lut_data_10bit_p[0xA7] = 0x07A;
	lut_data_10bit_p[0xA8] = 0x06A;
	lut_data_10bit_p[0xA9] = 0x25A;
	lut_data_10bit_p[0xAA] = 0x15A;
	lut_data_10bit_p[0xAB] = 0x34A;
	lut_data_10bit_p[0xAC] = 0x0DA;
	lut_data_10bit_p[0xAD] = 0x2CA;
	lut_data_10bit_p[0xAE] = 0x1CA;
	lut_data_10bit_p[0xAF] = 0x28A;
	lut_data_10bit_p[0xB0] = 0x24A;
	lut_data_10bit_p[0xB1] = 0x23A;
	lut_data_10bit_p[0xB2] = 0x13A;
	lut_data_10bit_p[0xB3] = 0x32A;
	lut_data_10bit_p[0xB4] = 0x0BA;
	lut_data_10bit_p[0xB5] = 0x2AA;
	lut_data_10bit_p[0xB6] = 0x1AA;
	lut_data_10bit_p[0xB7] = 0x05A;
	lut_data_10bit_p[0xB8] = 0x0CA;
	lut_data_10bit_p[0xB9] = 0x26A;
	lut_data_10bit_p[0xBA] = 0x16A;
	lut_data_10bit_p[0xBB] = 0x09A;
	lut_data_10bit_p[0xBC] = 0x0EA;
	lut_data_10bit_p[0xBD] = 0x11A;
	lut_data_10bit_p[0xBE] = 0x21A;
	lut_data_10bit_p[0xBF] = 0x14A;
	lut_data_10bit_p[0xC0] = 0x186;
	lut_data_10bit_p[0xC1] = 0x226;
	lut_data_10bit_p[0xC2] = 0x126;
	lut_data_10bit_p[0xC3] = 0x316;
	lut_data_10bit_p[0xC4] = 0x0A6;
	lut_data_10bit_p[0xC5] = 0x296;
	lut_data_10bit_p[0xC6] = 0x196;
	lut_data_10bit_p[0xC7] = 0x076;
	lut_data_10bit_p[0xC8] = 0x066;
	lut_data_10bit_p[0xC9] = 0x256;
	lut_data_10bit_p[0xCA] = 0x156;
	lut_data_10bit_p[0xCB] = 0x346;
	lut_data_10bit_p[0xCC] = 0x0D6;
	lut_data_10bit_p[0xCD] = 0x2C6;
	lut_data_10bit_p[0xCE] = 0x1C6;
	lut_data_10bit_p[0xCF] = 0x286;
	lut_data_10bit_p[0xD0] = 0x246;
	lut_data_10bit_p[0xD1] = 0x236;
	lut_data_10bit_p[0xD2] = 0x136;
	lut_data_10bit_p[0xD3] = 0x326;
	lut_data_10bit_p[0xD4] = 0x0B6;
	lut_data_10bit_p[0xD5] = 0x2A6;
	lut_data_10bit_p[0xD6] = 0x1A6;
	lut_data_10bit_p[0xD7] = 0x056;
	lut_data_10bit_p[0xD8] = 0x0C6;
	lut_data_10bit_p[0xD9] = 0x266;
	lut_data_10bit_p[0xDA] = 0x166;
	lut_data_10bit_p[0xDB] = 0x096;
	lut_data_10bit_p[0xDC] = 0x0E6;
	lut_data_10bit_p[0xDD] = 0x116;
	lut_data_10bit_p[0xDE] = 0x216;
	lut_data_10bit_p[0xDF] = 0x146;
	lut_data_10bit_p[0xE0] = 0x18E;
	lut_data_10bit_p[0xE1] = 0x22E;
	lut_data_10bit_p[0xE2] = 0x12E;
	lut_data_10bit_p[0xE3] = 0x311;
	lut_data_10bit_p[0xE4] = 0x0AE;
	lut_data_10bit_p[0xE5] = 0x291;
	lut_data_10bit_p[0xE6] = 0x191;
	lut_data_10bit_p[0xE7] = 0x071;
	lut_data_10bit_p[0xE8] = 0x06E;
	lut_data_10bit_p[0xE9] = 0x251;
	lut_data_10bit_p[0xEA] = 0x151;
	lut_data_10bit_p[0xEB] = 0x348;
	lut_data_10bit_p[0xEC] = 0x0D1;
	lut_data_10bit_p[0xED] = 0x2C8;
	lut_data_10bit_p[0xEE] = 0x1C8;
	lut_data_10bit_p[0xEF] = 0x28E;
	lut_data_10bit_p[0xF0] = 0x24E;
	lut_data_10bit_p[0xF1] = 0x231;
	lut_data_10bit_p[0xF2] = 0x131;
	lut_data_10bit_p[0xF3] = 0x321;
	lut_data_10bit_p[0xF4] = 0x0B1;
	lut_data_10bit_p[0xF5] = 0x2A1;
	lut_data_10bit_p[0xF6] = 0x1A1;
	lut_data_10bit_p[0xF7] = 0x05E;
	lut_data_10bit_p[0xF8] = 0x0CE;
	lut_data_10bit_p[0xF9] = 0x261;
	lut_data_10bit_p[0xFA] = 0x161;
	lut_data_10bit_p[0xFB] = 0x09E;
	lut_data_10bit_p[0xFC] = 0x0E1;
	lut_data_10bit_p[0xFD] = 0x11E;
	lut_data_10bit_p[0xFE] = 0x21E;
	lut_data_10bit_p[0xFF] = 0x14E;

	lut_data_10bit_n[0x00] = 0x274;
	lut_data_10bit_n[0x01] = 0x1D4;
	lut_data_10bit_n[0x02] = 0x2D4;
	lut_data_10bit_n[0x03] = 0x31B;
	lut_data_10bit_n[0x04] = 0x354;
	lut_data_10bit_n[0x05] = 0x29B;
	lut_data_10bit_n[0x06] = 0x19B;
	lut_data_10bit_n[0x07] = 0x38B;
	lut_data_10bit_n[0x08] = 0x394;
	lut_data_10bit_n[0x09] = 0x25B;
	lut_data_10bit_n[0x0A] = 0x15B;
	lut_data_10bit_n[0x0B] = 0x34B;
	lut_data_10bit_n[0x0C] = 0x0DB;
	lut_data_10bit_n[0x0D] = 0x2CB;
	lut_data_10bit_n[0x0E] = 0x1CB;
	lut_data_10bit_n[0x0F] = 0x174;
	lut_data_10bit_n[0x10] = 0x1B4;
	lut_data_10bit_n[0x11] = 0x23B;
	lut_data_10bit_n[0x12] = 0x13B;
	lut_data_10bit_n[0x13] = 0x32B;
	lut_data_10bit_n[0x14] = 0x0BB;
	lut_data_10bit_n[0x15] = 0x2AB;
	lut_data_10bit_n[0x16] = 0x1AB;
	lut_data_10bit_n[0x17] = 0x3A4;
	lut_data_10bit_n[0x18] = 0x334;
	lut_data_10bit_n[0x19] = 0x26B;
	lut_data_10bit_n[0x1A] = 0x16B;
	lut_data_10bit_n[0x1B] = 0x364;
	lut_data_10bit_n[0x1C] = 0x0EB;
	lut_data_10bit_n[0x1D] = 0x2E4;
	lut_data_10bit_n[0x1E] = 0x1E4;
	lut_data_10bit_n[0x1F] = 0x2B4;
	lut_data_10bit_n[0x20] = 0x279;
	lut_data_10bit_n[0x21] = 0x1D9;
	lut_data_10bit_n[0x22] = 0x2D9;
	lut_data_10bit_n[0x23] = 0x319;
	lut_data_10bit_n[0x24] = 0x359;
	lut_data_10bit_n[0x25] = 0x299;
	lut_data_10bit_n[0x26] = 0x199;
	lut_data_10bit_n[0x27] = 0x389;
	lut_data_10bit_n[0x28] = 0x399;
	lut_data_10bit_n[0x29] = 0x259;
	lut_data_10bit_n[0x2A] = 0x159;
	lut_data_10bit_n[0x2B] = 0x349;
	lut_data_10bit_n[0x2C] = 0x0D9;
	lut_data_10bit_n[0x2D] = 0x2C9;
	lut_data_10bit_n[0x2E] = 0x1C9;
	lut_data_10bit_n[0x2F] = 0x179;
	lut_data_10bit_n[0x30] = 0x1B9;
	lut_data_10bit_n[0x31] = 0x239;
	lut_data_10bit_n[0x32] = 0x139;
	lut_data_10bit_n[0x33] = 0x329;
	lut_data_10bit_n[0x34] = 0x0B9;
	lut_data_10bit_n[0x35] = 0x2A9;
	lut_data_10bit_n[0x36] = 0x1A9;
	lut_data_10bit_n[0x37] = 0x3A9;
	lut_data_10bit_n[0x38] = 0x339;
	lut_data_10bit_n[0x39] = 0x269;
	lut_data_10bit_n[0x3A] = 0x169;
	lut_data_10bit_n[0x3B] = 0x369;
	lut_data_10bit_n[0x3C] = 0x0E9;
	lut_data_10bit_n[0x3D] = 0x2E9;
	lut_data_10bit_n[0x3E] = 0x1E9;
	lut_data_10bit_n[0x3F] = 0x2B9;
	lut_data_10bit_n[0x40] = 0x275;
	lut_data_10bit_n[0x41] = 0x1D5;
	lut_data_10bit_n[0x42] = 0x2D5;
	lut_data_10bit_n[0x43] = 0x315;
	lut_data_10bit_n[0x44] = 0x355;
	lut_data_10bit_n[0x45] = 0x295;
	lut_data_10bit_n[0x46] = 0x195;
	lut_data_10bit_n[0x47] = 0x385;
	lut_data_10bit_n[0x48] = 0x395;
	lut_data_10bit_n[0x49] = 0x255;
	lut_data_10bit_n[0x4A] = 0x155;
	lut_data_10bit_n[0x4B] = 0x345;
	lut_data_10bit_n[0x4C] = 0x0D5;
	lut_data_10bit_n[0x4D] = 0x2C5;
	lut_data_10bit_n[0x4E] = 0x1C5;
	lut_data_10bit_n[0x4F] = 0x175;
	lut_data_10bit_n[0x50] = 0x1B5;
	lut_data_10bit_n[0x51] = 0x235;
	lut_data_10bit_n[0x52] = 0x135;
	lut_data_10bit_n[0x53] = 0x325;
	lut_data_10bit_n[0x54] = 0x0B5;
	lut_data_10bit_n[0x55] = 0x2A5;
	lut_data_10bit_n[0x56] = 0x1A5;
	lut_data_10bit_n[0x57] = 0x3A5;
	lut_data_10bit_n[0x58] = 0x335;
	lut_data_10bit_n[0x59] = 0x265;
	lut_data_10bit_n[0x5A] = 0x165;
	lut_data_10bit_n[0x5B] = 0x365;
	lut_data_10bit_n[0x5C] = 0x0E5;
	lut_data_10bit_n[0x5D] = 0x2E5;
	lut_data_10bit_n[0x5E] = 0x1E5;
	lut_data_10bit_n[0x5F] = 0x2B5;
	lut_data_10bit_n[0x60] = 0x273;
	lut_data_10bit_n[0x61] = 0x1D3;
	lut_data_10bit_n[0x62] = 0x2D3;
	lut_data_10bit_n[0x63] = 0x31C;
	lut_data_10bit_n[0x64] = 0x353;
	lut_data_10bit_n[0x65] = 0x29C;
	lut_data_10bit_n[0x66] = 0x19C;
	lut_data_10bit_n[0x67] = 0x38C;
	lut_data_10bit_n[0x68] = 0x393;
	lut_data_10bit_n[0x69] = 0x25C;
	lut_data_10bit_n[0x6A] = 0x15C;
	lut_data_10bit_n[0x6B] = 0x34C;
	lut_data_10bit_n[0x6C] = 0x0DC;
	lut_data_10bit_n[0x6D] = 0x2CC;
	lut_data_10bit_n[0x6E] = 0x1CC;
	lut_data_10bit_n[0x6F] = 0x173;
	lut_data_10bit_n[0x70] = 0x1B3;
	lut_data_10bit_n[0x71] = 0x23C;
	lut_data_10bit_n[0x72] = 0x13C;
	lut_data_10bit_n[0x73] = 0x32C;
	lut_data_10bit_n[0x74] = 0x0BC;
	lut_data_10bit_n[0x75] = 0x2AC;
	lut_data_10bit_n[0x76] = 0x1AC;
	lut_data_10bit_n[0x77] = 0x3A3;
	lut_data_10bit_n[0x78] = 0x333;
	lut_data_10bit_n[0x79] = 0x26C;
	lut_data_10bit_n[0x7A] = 0x16C;
	lut_data_10bit_n[0x7B] = 0x363;
	lut_data_10bit_n[0x7C] = 0x0EC;
	lut_data_10bit_n[0x7D] = 0x2E3;
	lut_data_10bit_n[0x7E] = 0x1E3;
	lut_data_10bit_n[0x7F] = 0x2B3;
	lut_data_10bit_n[0x80] = 0x272;
	lut_data_10bit_n[0x81] = 0x1D2;
	lut_data_10bit_n[0x82] = 0x2D2;
	lut_data_10bit_n[0x83] = 0x31D;
	lut_data_10bit_n[0x84] = 0x352;
	lut_data_10bit_n[0x85] = 0x29D;
	lut_data_10bit_n[0x86] = 0x19D;
	lut_data_10bit_n[0x87] = 0x38D;
	lut_data_10bit_n[0x88] = 0x392;
	lut_data_10bit_n[0x89] = 0x25D;
	lut_data_10bit_n[0x8A] = 0x15D;
	lut_data_10bit_n[0x8B] = 0x34D;
	lut_data_10bit_n[0x8C] = 0x0DD;
	lut_data_10bit_n[0x8D] = 0x2CD;
	lut_data_10bit_n[0x8E] = 0x1CD;
	lut_data_10bit_n[0x8F] = 0x172;
	lut_data_10bit_n[0x90] = 0x1B2;
	lut_data_10bit_n[0x91] = 0x23D;
	lut_data_10bit_n[0x92] = 0x13D;
	lut_data_10bit_n[0x93] = 0x32D;
	lut_data_10bit_n[0x94] = 0x0BD;
	lut_data_10bit_n[0x95] = 0x2AD;
	lut_data_10bit_n[0x96] = 0x1AD;
	lut_data_10bit_n[0x97] = 0x3A2;
	lut_data_10bit_n[0x98] = 0x332;
	lut_data_10bit_n[0x99] = 0x26D;
	lut_data_10bit_n[0x9A] = 0x16D;
	lut_data_10bit_n[0x9B] = 0x362;
	lut_data_10bit_n[0x9C] = 0x0ED;
	lut_data_10bit_n[0x9D] = 0x2E2;
	lut_data_10bit_n[0x9E] = 0x1E2;
	lut_data_10bit_n[0x9F] = 0x2B2;
	lut_data_10bit_n[0xA0] = 0x27A;
	lut_data_10bit_n[0xA1] = 0x1DA;
	lut_data_10bit_n[0xA2] = 0x2DA;
	lut_data_10bit_n[0xA3] = 0x31A;
	lut_data_10bit_n[0xA4] = 0x35A;
	lut_data_10bit_n[0xA5] = 0x29A;
	lut_data_10bit_n[0xA6] = 0x19A;
	lut_data_10bit_n[0xA7] = 0x38A;
	lut_data_10bit_n[0xA8] = 0x39A;
	lut_data_10bit_n[0xA9] = 0x25A;
	lut_data_10bit_n[0xAA] = 0x15A;
	lut_data_10bit_n[0xAB] = 0x34A;
	lut_data_10bit_n[0xAC] = 0x0DA;
	lut_data_10bit_n[0xAD] = 0x2CA;
	lut_data_10bit_n[0xAE] = 0x1CA;
	lut_data_10bit_n[0xAF] = 0x17A;
	lut_data_10bit_n[0xB0] = 0x1BA;
	lut_data_10bit_n[0xB1] = 0x23A;
	lut_data_10bit_n[0xB2] = 0x13A;
	lut_data_10bit_n[0xB3] = 0x32A;
	lut_data_10bit_n[0xB4] = 0x0BA;
	lut_data_10bit_n[0xB5] = 0x2AA;
	lut_data_10bit_n[0xB6] = 0x1AA;
	lut_data_10bit_n[0xB7] = 0x3AA;
	lut_data_10bit_n[0xB8] = 0x33A;
	lut_data_10bit_n[0xB9] = 0x26A;
	lut_data_10bit_n[0xBA] = 0x16A;
	lut_data_10bit_n[0xBB] = 0x36A;
	lut_data_10bit_n[0xBC] = 0x0EA;
	lut_data_10bit_n[0xBD] = 0x2EA;
	lut_data_10bit_n[0xBE] = 0x1EA;
	lut_data_10bit_n[0xBF] = 0x2BA;
	lut_data_10bit_n[0xC0] = 0x276;
	lut_data_10bit_n[0xC1] = 0x1D6;
	lut_data_10bit_n[0xC2] = 0x2D6;
	lut_data_10bit_n[0xC3] = 0x316;
	lut_data_10bit_n[0xC4] = 0x356;
	lut_data_10bit_n[0xC5] = 0x296;
	lut_data_10bit_n[0xC6] = 0x196;
	lut_data_10bit_n[0xC7] = 0x386;
	lut_data_10bit_n[0xC8] = 0x396;
	lut_data_10bit_n[0xC9] = 0x256;
	lut_data_10bit_n[0xCA] = 0x156;
	lut_data_10bit_n[0xCB] = 0x346;
	lut_data_10bit_n[0xCC] = 0x0D6;
	lut_data_10bit_n[0xCD] = 0x2C6;
	lut_data_10bit_n[0xCE] = 0x1C6;
	lut_data_10bit_n[0xCF] = 0x176;
	lut_data_10bit_n[0xD0] = 0x1B6;
	lut_data_10bit_n[0xD1] = 0x236;
	lut_data_10bit_n[0xD2] = 0x136;
	lut_data_10bit_n[0xD3] = 0x326;
	lut_data_10bit_n[0xD4] = 0x0B6;
	lut_data_10bit_n[0xD5] = 0x2A6;
	lut_data_10bit_n[0xD6] = 0x1A6;
	lut_data_10bit_n[0xD7] = 0x3A6;
	lut_data_10bit_n[0xD8] = 0x336;
	lut_data_10bit_n[0xD9] = 0x266;
	lut_data_10bit_n[0xDA] = 0x166;
	lut_data_10bit_n[0xDB] = 0x366;
	lut_data_10bit_n[0xDC] = 0x0E6;
	lut_data_10bit_n[0xDD] = 0x2E6;
	lut_data_10bit_n[0xDE] = 0x1E6;
	lut_data_10bit_n[0xDF] = 0x2B6;
	lut_data_10bit_n[0xE0] = 0x271;
	lut_data_10bit_n[0xE1] = 0x1D1;
	lut_data_10bit_n[0xE2] = 0x2D1;
	lut_data_10bit_n[0xE3] = 0x31E;
	lut_data_10bit_n[0xE4] = 0x351;
	lut_data_10bit_n[0xE5] = 0x29E;
	lut_data_10bit_n[0xE6] = 0x19E;
	lut_data_10bit_n[0xE7] = 0x38E;
	lut_data_10bit_n[0xE8] = 0x391;
	lut_data_10bit_n[0xE9] = 0x25E;
	lut_data_10bit_n[0xEA] = 0x15E;
	lut_data_10bit_n[0xEB] = 0x34E;
	lut_data_10bit_n[0xEC] = 0x0DE;
	lut_data_10bit_n[0xED] = 0x2CE;
	lut_data_10bit_n[0xEE] = 0x1CE;
	lut_data_10bit_n[0xEF] = 0x171;
	lut_data_10bit_n[0xF0] = 0x1B1;
	lut_data_10bit_n[0xF1] = 0x237;
	lut_data_10bit_n[0xF2] = 0x137;
	lut_data_10bit_n[0xF3] = 0x32E;
	lut_data_10bit_n[0xF4] = 0x0B7;
	lut_data_10bit_n[0xF5] = 0x2AE;
	lut_data_10bit_n[0xF6] = 0x1AE;
	lut_data_10bit_n[0xF7] = 0x3A1;
	lut_data_10bit_n[0xF8] = 0x331;
	lut_data_10bit_n[0xF9] = 0x26E;
	lut_data_10bit_n[0xFA] = 0x16E;
	lut_data_10bit_n[0xFB] = 0x361;
	lut_data_10bit_n[0xFC] = 0x0EE;
	lut_data_10bit_n[0xFD] = 0x2E1;
	lut_data_10bit_n[0xFE] = 0x1E1;
	lut_data_10bit_n[0xFF] = 0x2B1;

}

void PCIExpressTransactor::readInputBus() {
   UInt32 i;

   // get packets out of the chip
   for(i=0; i < mLinkWidth; ++i)
      mIncomingPackets[i] = getReceiveData(i);

   if (mIntfType == PCIEXtor::ePipeIntf)
      for(i=0; i < mLinkWidth; ++i)
	 mIncomingPacketsSym[i] = (bool)(getReceiveKCode(i));


   if(VerboseReceiverHigh) {
      printf("%s  Info: Saw packets: ",printName);
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 for(i=0; i < mLinkWidth; ++i)
	    printf("0x%03X ", mIncomingPackets[i]);
      } else {
	 for(i=0; i < mLinkWidth; ++i) {
	    if (mIncomingPacketsSym[i])
	       printf(" %s ", getSymStringFromByte(mIncomingPackets[i]));
	    else
	       printf("0x%02X ", mIncomingPackets[i]);
	 }
      }
      printf(" (at %s)\n", mCurrTimeString);
   }

   parsePackets();
   return;
}

void PCIExpressTransactor::parsePackets() {
   UInt32 i;
   static UInt32 prev_byte;
   static bool prev_ksym;
   bool ordered_set;
   bool see_electrical_idle;
   bool electrical_idle_state_changed = false;


   // first time lets keep us in electrical idle, otherwise we miss it if the DUT hasn't clocked
   // the elec idle active yet
   if (txPowerDown_d1 == 0xDEADBEEF) {
      txPowerDown = 0;
      for (i=0; i < mLinkWidth; ++i)
	 txPowerDown |= 0x1 << i;
   } else {
      txPowerDown = 0;
      for (i=0; i < mLinkWidth; ++i)
	 if (getReceiveLos(i) != 0)
	    txPowerDown |= 0x1 << i;
   }

   if ((txPowerDown_d1 != 0) && (txPowerDown == 0)) {  // deasserting of power_down from chip
      if(VerboseReceiverLow) {
	 printf("%s  Info: Powerdown deasserting from DUT (at %s).",printName,mCurrTimeString);
	 if(VerboseReceiverMedium)
	    printf("  (pdn_new=0x%02X, pdn_old=0x%02X)",txPowerDown,txPowerDown_d1);
	 printf("\n");
      }
   } else if ((txPowerDown_d1 == 0) && (txPowerDown != 0)) {  // asserting of power_down from chip
      if(VerboseReceiverLow) {
	 printf("%s  Info: Powerdown asserting from DUT (at %s).",printName,mCurrTimeString);
	 if(VerboseReceiverMedium)
	    printf("  (Old value=0x%02X, New value=0x%02X)",txPowerDown_d1,txPowerDown);
	 printf("\n");
      }
   }

   if(txPowerDown != txPowerDown_d1) {
      electrical_idle_state_changed = true;
      if(VerboseReceiverHigh)
	 printf("%s  Info: TxPowerDown was 0x%02X", printName, txPowerDown_d1);
      txPowerDown_d1 = txPowerDown;      // copy signals that were early a cycle so we can use them now
      if(VerboseReceiverHigh)
	 printf(", now 0x%02X\n", txPowerDown_d1);
      // We handle the entire bus as powered up or powered down, not individual lanes.  Because of that
      //   I can simply clear out our current byte received queue since the DUT has now put the lane into
      //   electrical idle and shouldn't have anything new and meaningful in the process of being transmitted.
      clearReceiverBuffer(0,newTransactionPacketsTotal);
   }


   if (txPowerDown == 0)
      see_electrical_idle = false;
   else
      see_electrical_idle = true;

   if (electrical_idle_state_changed) {
      if (see_electrical_idle) {                   // Have Electrical idle
	 setCurInputState(Electrical_Idle);
	 // printf("%s  See electrical Idle input (at %s) pdn=0x%02X\n", printName, mCurrTimeString, txPowerDown_d1);
	 for (UInt32 i=0; i<mLinkWidth; ++i)
	    setTransmitLos(i, 0x1);
	 return;
      } else {
	 setCurInputState(Logical_Idle);

	 if (mTrainingBypass) {
	    txLTSM = LTSM_Active;
	    for(UInt32 i=0; i<mLinkWidth; ++i)    // txLTSM != LTSM_Electrical_Idle
	       setTransmitLos(i, 0x0);
	    addTransactionSpecial("CLOCK_TOL");

	 } else {

	    txLTSM = LTSM_Logical_Idle;
	    for (UInt32 i=0; i<mLinkWidth; ++i) {
	       setTransmitLos(i, 0x0);
	       if (mIntfType == PCIEXtor::ePipeIntf)
		  setTransmitRxValid(i, 0x1);      // txLTSM != LTSM_Electrical_Idle
	    }
	    transmitIdleData();
	    addTransactionSpecialHead("IDLE 1");

	 }

      }
   } else {
      if (see_electrical_idle)
	 return;
   }

    // Training Sequences
    //  Composed of ordered sets used for initialization and are never scrambled but
    //  always 8b/10b encoded.  TS1 & TS2 are transmitted consecutively and can only
    //  be interrupted by SKP ordered sets (PCI Express Base Spec r1.0a, sec 4.2.4.1)

    // DECODE & DESCRAMBLE PACKET TO BYTE
    for (i=0; i < mLinkWidth; ++i) {
       if (mIntfType != PCIEXtor::ePipeIntf)
	  mIncomingPackets[i] = decoder_lut(mIncomingPackets[i], &(mIncomingPacketsSym[i]), i);    // decode packet, get scrambled byte and whether its a KCODE or data
       if (mScrambleEnable)
	  mIncomingPackets[i] = descramble_byte(mIncomingPackets[i], mIncomingPacketsSym[i], i);
    }

    // data is now decoded & unscrambled
    ordered_set = true;
    for (i=1; i<mLinkWidth; ++i) {
       if ((mIncomingPackets[0] != mIncomingPackets[i]) || (mIncomingPacketsSym[0] != mIncomingPacketsSym[i])) {
	  ordered_set = false;
	  break;
       }
    }

    if(not mInTransaction) {
       if(ordered_set) {

	  // have to figure out when to enable descrambling on receiver
          if (mScrambleEnable && rxLTSM == LTSM_TS2_0_x) {
	     if ((mSpeculativeDescramBytes[0] == 0x00) && (mIncomingPacketsSym[0] == false)) { // lane 0 is scrambled 0x00 data, and its an ordered set
		// found scrambled logical idle at end of TS2_0_x on receiver!
// printf("MIKE: Speculative Find Idle Zero SUCCESSFUL!! at %s\n", mCurrTimeString);
		setCurInputState(Logical_Idle);
		rxLTSM = LTSM_Active;
		return;
	     }
// printf("MIKE: Speculative Find Idle Zero FAILED at %s\n", mCurrTimeString);

	  } else {

	     if((!mIncomingPacketsSym[0]) && (mIncomingPackets[0]==0x00)) {        // D00.0
		setCurInputState(Logical_Idle);
		if(rxLTSM == LTSM_TS2_0_x)
		   rxLTSM = LTSM_Active;
		else if(rxDLLSM == DLLSM_Init2)
		   rxDLLSM = DLLSM_Active;

		return;
	     }

	  }
       }

	if(curInputState == Logical_Idle)
	    setCurInputState(Active);
	mInTransaction = true;
#if 1
	for (i = 0; i<TRANS_BUF_SIZE; ++i) {
	    newTransactionPackets[i] = 0;
	    newTransactionPacketsOSet[i] = false;
	}
#endif
	if(ordered_set) {
	    newTransactionPackets[0] = mIncomingPackets[0];
	    newTransactionPacketsSym[0] = mIncomingPacketsSym[0];
	    newTransactionPacketsOSet[0] = true;
	    newTransactionPacketsTotal = 1;
	} else {
	    for(i=0; i<mLinkWidth; ++i) {
		newTransactionPackets[i] = mIncomingPackets[i];
		newTransactionPacketsSym[i] = mIncomingPacketsSym[i];
		newTransactionPacketsOSet[i] = false;
	    }
	    newTransactionPacketsTotal = mLinkWidth;
	}
    } else {
       // remove logical idle ordered sets if we just saw an END packet (for InitFC1/InitFC2 sets)
       if ((newTransactionPackets[(newTransactionPacketsTotal-1)] == PCIE_END) &&
	   (newTransactionPacketsSym[(newTransactionPacketsTotal-1)] == true) &&
	   (ordered_set && (mIncomingPackets[0]==0x0) && (mIncomingPacketsSym[0]==false))) {
	  ;
       } else {

	  if (ordered_set) {
	     newTransactionPackets[newTransactionPacketsTotal] = mIncomingPackets[0];
	     newTransactionPacketsSym[newTransactionPacketsTotal] = mIncomingPacketsSym[0];
	     newTransactionPacketsOSet[newTransactionPacketsTotal] = true;
	     ++newTransactionPacketsTotal;
	  } else {
	     for(i=0; i<mLinkWidth; ++i) {
		newTransactionPackets[newTransactionPacketsTotal] = mIncomingPackets[i];
		newTransactionPacketsSym[newTransactionPacketsTotal] = mIncomingPacketsSym[i];
		newTransactionPacketsOSet[newTransactionPacketsTotal] = false;
		++newTransactionPacketsTotal;
	     }
	  }

       }
    }
//  printf("%s   - NewTransactionPacketsTotal = %d, byte: 0x%02X, k=%d\n", printName, newTransactionPacketsTotal, newTransactionPackets[(newTransactionPacketsTotal-1)], newTransactionPacketsSym[(newTransactionPacketsTotal-1)]);
//  printReceivedPacketList();

    if(VerboseReceiverHigh) {
	if(ordered_set) {
	    if((mIncomingPackets[0] != prev_byte) || (mIncomingPacketsSym[0] != prev_ksym)) {
		if(mIncomingPacketsSym[0])
		    printf("%s  Info: Found ordered set symbol: %s (at %s)\n", printName, getSymStringFromByte(mIncomingPackets[0]), mCurrTimeString);
		else
		    printf("%s  Info: Found ordered set data:   0x%02X (at %s)\n", printName, mIncomingPackets[0], mCurrTimeString);
		prev_byte = mIncomingPackets[0];
		prev_ksym = mIncomingPacketsSym[0];
	    }
	} else {
	    printf("%s  Info: Found single data: ", printName);
	    for (i=0; i<mLinkWidth; ++i)
	       if (mIncomingPacketsSym[i])
		  printf(" %s ", getSymStringFromByte(mIncomingPackets[i]));
	       else
		  printf("0x%02X ", mIncomingPackets[i]);
	    printf("(at %s)\n", mCurrTimeString);
	    prev_byte = 0;
	    prev_ksym = true;
	}
    }

    checkCurrentTransactionIncoming();

    return;
}


void PCIExpressTransactor::checkCurrentTransactionIncoming(void)
{
    UInt32 i;

    UInt32 tpt = newTransactionPacketsTotal;

    // Check for CLOCK_TOL
    if ((tpt >= 4)
	&& (newTransactionPackets[(tpt-4)] == PCIE_COM)  && (newTransactionPacketsSym[(tpt-4)] == true) && (newTransactionPacketsOSet[(tpt-4)] == true)
	&& (newTransactionPackets[(tpt-3)] == PCIE_SKP)  && (newTransactionPacketsSym[(tpt-3)] == true) && (newTransactionPacketsOSet[(tpt-3)] == true)
	&& (newTransactionPackets[(tpt-2)] == PCIE_SKP)  && (newTransactionPacketsSym[(tpt-2)] == true) && (newTransactionPacketsOSet[(tpt-2)] == true)
	&& (newTransactionPackets[(tpt-1)] == PCIE_SKP)  && (newTransactionPacketsSym[(tpt-1)] == true) && (newTransactionPacketsOSet[(tpt-1)] == true)
       ) {
	clearReceiverBuffer((tpt-4),(tpt-1));  // remove packets 0 to 3 from buffer if we have 4 packets
	if(powerSave_L0s == 3) {        // must see SKP ordered set after FTS
	   powerSave_L0s = 0;           // to return to L0 state
//y printf("MIKE: PCIE %s Going to PowerSave State 0  from SKP set\n",printName);
//y printTransactionQueue();
	}
	if(VerboseReceiverMedium)
	    printf("%s  Info: Received transaction: Clock Tolerance Compensation\n", printName);
    // Check for Power_Save (switch to State L0s)
    } else if ((tpt >= 4)
	       && (newTransactionPackets[(tpt-4)] == PCIE_COM) && (newTransactionPacketsSym[(tpt-4)] == true) && (newTransactionPacketsOSet[(tpt-4)] == true)
	       && (newTransactionPackets[(tpt-3)] == PCIE_IDL) && (newTransactionPacketsSym[(tpt-3)] == true) && (newTransactionPacketsOSet[(tpt-3)] == true)
	       && (newTransactionPackets[(tpt-2)] == PCIE_IDL) && (newTransactionPacketsSym[(tpt-2)] == true) && (newTransactionPacketsOSet[(tpt-2)] == true)
	       && (newTransactionPackets[(tpt-1)] == PCIE_IDL) && (newTransactionPacketsSym[(tpt-1)] == true) && (newTransactionPacketsOSet[(tpt-1)] == true)
	      ) {
	clearReceiverBuffer((tpt-4),(tpt-1));   // remove packets 0 to 3 from buffer if we have 4 packets
	setCurInputState(Power_Save);
// printf("%s Found PowerSave Set (COM/IDL/IDL/IDL), going to PowerSave mode.\n", printName);
	if(VerboseReceiverMedium)
	    printf("%s  Info: Received Transaction: Entering Power Save Mode\n", printName);
    } else {
       // Check for Fast Traning Set (is an ordered set)
       checkCurrentTransactionFTS();
       // Check for Training Sets (are ordered sets)
       checkCurrentTransactionPhysTraining();
       // Remove end of transaction padding
       checkCurrentTransactionPadding();
       // Check for full transactions and parse if found until all are processed
       while (checkCurrentTransactionComplete()) {
	  if (newTransactionPackets[mCurrTransactionStart] == PCIE_SDP) {
	     checkCurrentTransactionDLLTraining();
	  } else {
	     checkCurrentTransactionTLPs();
	  }
       }
    }
    // don't complain if we haven't even trained yet
    if((rxLTSM < LTSM_TS1_P_P) && (tpt > (TRANS_BUF_SIZE - 50))) {
	clearReceiverBuffer(0,(tpt-1));
    }

    if(tpt > (TRANS_BUF_SIZE - 20)) {
	printf("%s  Info: Received a large number of packets I don't understand.  I'm going do delete them and hope for the best...\n  Bytes:", printName);
	for (i=0; i < tpt; ++i) {
	   if (newTransactionPacketsSym[i])  // have a symbol
	      printf("  %s%s", getSymStringFromByte(newTransactionPackets[i]), ((newTransactionPacketsOSet[i])?"[OSet]":""));
	   else                              // have a data byte
	      printf("  %02X%s", newTransactionPackets[i], ((newTransactionPacketsOSet[i])?"[OSet]":""));
	}
	printf("\n");
	clearReceiverBuffer(0,(tpt-1));
    }
}

void PCIExpressTransactor::checkCurrentTransactionFTS(void)
{
   UInt32 tpt = newTransactionPacketsTotal;
   if ((tpt >= 4)
       && (newTransactionPackets[(tpt-4)] == PCIE_COM)   && (newTransactionPacketsSym[(tpt-4)] == true) && (newTransactionPacketsOSet[(tpt-4)] == true)
       && (newTransactionPackets[(tpt-3)] == PCIE_FTS)   && (newTransactionPacketsSym[(tpt-3)] == true) && (newTransactionPacketsOSet[(tpt-3)] == true)
       && (newTransactionPackets[(tpt-2)] == PCIE_FTS)   && (newTransactionPacketsSym[(tpt-2)] == true) && (newTransactionPacketsOSet[(tpt-2)] == true)
       && (newTransactionPackets[(tpt-1)] == PCIE_FTS)   && (newTransactionPacketsSym[(tpt-1)] == true) && (newTransactionPacketsOSet[(tpt-1)] == true)
       ) {
      clearReceiverBuffer((tpt-4),(tpt-1));  // remove packets 0 to 3 from buffer
      if(powerSave_L0s == 2) {
	 addTransactionSpecialHead("IDLE 2");    // 3) then idle the bus
	 addTransactionSpecialHead("CLOCK_TOL"); // 4) then do a real clock tolerance
	 addTransactionSpecialHead("IDLE 2");    // 3) then pause
	 addTransactionSpecialHead("CLOCK_TOL"); // 2) must finish with a SKP ordered set
	 addTransactionSpecialHead("FTS");       // 1) add transaction to power-up chip
	 powerSave_L0s = 3;
//y printf("MIKE: PCIE %s Going to PowerSave State 3  (curOutputState=%d)\n",printName, curOutputState);
//y printTransactionQueue();
      }
      setCurInputState(Fast_Training);
      if(VerboseReceiverMedium)
	 printf("%s  Info: Received transaction: Fast Training Sequence\n", printName);
   }
}

// Check that each packet is data, not an ordered set, and the value is the same as the lane number
// starting with id 'index'
bool PCIExpressTransactor::checkCurrentTransactionPhysTraining_idnumOnLanes(UInt32 index) {
   UInt32 i;
   bool result = true;

   if (mLinkWidth == 1) {
      if (!((newTransactionPackets[index] == 0) && (newTransactionPacketsSym[index] == false)))
	 result = false;
   } else {
      for (i=0; i<mLinkWidth; ++i) {
	 if (!((newTransactionPackets[(index+i)] == i) && (newTransactionPacketsSym[(index+i)] == false) && (newTransactionPacketsOSet[(index+i)] == false))) {
	    result = false;
	    break;
	 }
      }
   }

   return result;
}

void PCIExpressTransactor::checkCurrentTransactionPhysTraining(void)
{
    bool trainHaveTS1, trainHaveLinkPad;
    UInt32 trainLinkNum;
    UInt32 i;
    bool foundOne = false;
    pcie_LTSM_state_t curRxLTSM = rxLTSM;
    UInt32 tpt = newTransactionPacketsTotal;

    if ((tpt >= 16)
	&& (newTransactionPackets[(tpt-16)]  == PCIE_COM)   && (newTransactionPacketsSym[(tpt-16)]  == true)  && (newTransactionPacketsOSet[(tpt-16)]  == true)
	//          This could PAD or any data value, must be an ordered set
	// && (newTransactionPackets[(tpt-15)]  == PCIE_PAD)   && (newTransactionPacketsSym[(tpt-15)]  == true)
	                                                                                                      && (newTransactionPacketsOSet[(tpt-15)]  == true)
	&& (newTransactionPackets[(tpt-14)]  == PCIE_PAD)   && (newTransactionPacketsSym[(tpt-14)]  == true)  && (newTransactionPacketsOSet[(tpt-14)]  == true)
	//          This could be any data value, must be data and an ordered set
	// && (newTransactionPackets[(tpt-13)]  == 0x8C)
	                                                    && (newTransactionPacketsSym[(tpt-13)]  == false) && (newTransactionPacketsOSet[(tpt-13)]  == true)
	&& (newTransactionPackets[(tpt-12)]  == 0x02)       && (newTransactionPacketsSym[(tpt-12)]  == false) && (newTransactionPacketsOSet[(tpt-12)]  == true)
	//          This could be any data value between 0 and 15, must be data and an ordered set
	// && (newTransactionPackets[(tpt-11)]  == 0x00)
	                                                    && (newTransactionPacketsSym[(tpt-11)]  == false) && (newTransactionPacketsOSet[(tpt-11)]  == true)
	&& ((
	       (newTransactionPackets[(tpt-10)] == 0x4A)    && (newTransactionPacketsSym[(tpt-10)] == false) && (newTransactionPacketsOSet[(tpt-10)] == true)
	    && (newTransactionPackets[(tpt-9)]  == 0x4A)    && (newTransactionPacketsSym[(tpt-9)]  == false) && (newTransactionPacketsOSet[(tpt-9)]  == true)
	    && (newTransactionPackets[(tpt-8)]  == 0x4A)    && (newTransactionPacketsSym[(tpt-8)]  == false) && (newTransactionPacketsOSet[(tpt-8)]  == true)
	    && (newTransactionPackets[(tpt-7)]  == 0x4A)    && (newTransactionPacketsSym[(tpt-7)]  == false) && (newTransactionPacketsOSet[(tpt-7)]  == true)
	    && (newTransactionPackets[(tpt-6)]  == 0x4A)    && (newTransactionPacketsSym[(tpt-6)]  == false) && (newTransactionPacketsOSet[(tpt-6)]  == true)
	    && (newTransactionPackets[(tpt-5)]  == 0x4A)    && (newTransactionPacketsSym[(tpt-5)]  == false) && (newTransactionPacketsOSet[(tpt-5)]  == true)
	    && (newTransactionPackets[(tpt-4)]  == 0x4A)    && (newTransactionPacketsSym[(tpt-4)]  == false) && (newTransactionPacketsOSet[(tpt-4)]  == true)
	    && (newTransactionPackets[(tpt-3)]  == 0x4A)    && (newTransactionPacketsSym[(tpt-3)]  == false) && (newTransactionPacketsOSet[(tpt-3)]  == true)
	    && (newTransactionPackets[(tpt-2)]  == 0x4A)    && (newTransactionPacketsSym[(tpt-2)]  == false) && (newTransactionPacketsOSet[(tpt-2)]  == true)
	    && (newTransactionPackets[(tpt-1)]  == 0x4A)    && (newTransactionPacketsSym[(tpt-1)]  == false) && (newTransactionPacketsOSet[(tpt-1)]  == true)
	   ) || (
	       (newTransactionPackets[(tpt-10)] == 0x45)    && (newTransactionPacketsSym[(tpt-10)] == false) && (newTransactionPacketsOSet[(tpt-10)] == true)
	    && (newTransactionPackets[(tpt-9)]  == 0x45)    && (newTransactionPacketsSym[(tpt-9)]  == false) && (newTransactionPacketsOSet[(tpt-9)]  == true)
	    && (newTransactionPackets[(tpt-8)]  == 0x45)    && (newTransactionPacketsSym[(tpt-8)]  == false) && (newTransactionPacketsOSet[(tpt-8)]  == true)
	    && (newTransactionPackets[(tpt-7)]  == 0x45)    && (newTransactionPacketsSym[(tpt-7)]  == false) && (newTransactionPacketsOSet[(tpt-7)]  == true)
	    && (newTransactionPackets[(tpt-6)]  == 0x45)    && (newTransactionPacketsSym[(tpt-6)]  == false) && (newTransactionPacketsOSet[(tpt-6)]  == true)
	    && (newTransactionPackets[(tpt-5)]  == 0x45)    && (newTransactionPacketsSym[(tpt-5)]  == false) && (newTransactionPacketsOSet[(tpt-5)]  == true)
	    && (newTransactionPackets[(tpt-4)]  == 0x45)    && (newTransactionPacketsSym[(tpt-4)]  == false) && (newTransactionPacketsOSet[(tpt-4)]  == true)
	    && (newTransactionPackets[(tpt-3)]  == 0x45)    && (newTransactionPacketsSym[(tpt-3)]  == false) && (newTransactionPacketsOSet[(tpt-3)]  == true)
	    && (newTransactionPackets[(tpt-2)]  == 0x45)    && (newTransactionPacketsSym[(tpt-2)]  == false) && (newTransactionPacketsOSet[(tpt-2)]  == true)
	    && (newTransactionPackets[(tpt-1)]  == 0x45)    && (newTransactionPacketsSym[(tpt-1)]  == false) && (newTransactionPacketsOSet[(tpt-1)]  == true)
           ))
	) {
	foundOne = true;
	setCurInputState(Link_Training);

	if(newTransactionPackets[(tpt-10)] == 0x4A)
	    trainHaveTS1 = true;
	else
	    trainHaveTS1 = false;
	if((newTransactionPackets[(tpt-15)] == PCIE_PAD) && (newTransactionPacketsSym[(tpt-15)] == true)) {
	    trainHaveLinkPad = true;
	    trainLinkNum = 99;
	} else {
	    trainHaveLinkPad = false;
	    trainLinkNum = newTransactionPackets[(tpt-15)];
	}
	if(trainHaveTS1) {
	    if(trainHaveLinkPad) {
		rxLTSM = LTSM_TS1_P_P;
	    } else if(trainLinkNum == 0) {
		rxLTSM = LTSM_TS1_0_P;
	    } else {
		rxLTSM = LTSM_Logical_Idle;   // bad link number
	    }
	} else {
	    if(trainHaveLinkPad) {
		rxLTSM = LTSM_TS2_P_P;
	    } else if(trainLinkNum == 0) {
		rxLTSM = LTSM_TS2_0_P;
	    } else {
		rxLTSM = LTSM_Logical_Idle;   // bad link number
	    }
	}

	if(VerboseReceiverMedium) {
	    printf("%s  Info: Received transaction: TS%d %c P (at %s)", printName,
		   (trainHaveTS1 ? 1 : 2),
		   (trainHaveLinkPad ? 'P' : '0'), mCurrTimeString);
	    if(VerboseReceiverHigh) {
		for (i=0; i < tpt; ++i)
		    printf("  %d=%02X", i, newTransactionPackets[i]);
	    }
	    printf("\n");
	}
	clearReceiverBuffer((tpt-16),(tpt-1));

    } else if ((tpt >= (15+mLinkWidth))
	&& (newTransactionPackets[(tpt-(15+mLinkWidth))]  == PCIE_COM)   && (newTransactionPacketsSym[(tpt-(15+mLinkWidth))]  == true)  && (newTransactionPacketsOSet[(tpt-(15+mLinkWidth))]  == true)
	&& (newTransactionPackets[(tpt-(14+mLinkWidth))]  == 0x00)       && (newTransactionPacketsSym[(tpt-(14+mLinkWidth))]  == false) && (newTransactionPacketsOSet[(tpt-(14+mLinkWidth))]  == true)
        && ( checkCurrentTransactionPhysTraining_idnumOnLanes(tpt-(13+mLinkWidth)) )
	//          This could be any data value, must be data and an ordered set
	// && (newTransactionPackets[(tpt-13)] == 0x8C)
	                                                   && (newTransactionPacketsSym[(tpt-13)] == false) && (newTransactionPacketsOSet[(tpt-13)] == true)
	&& (newTransactionPackets[(tpt-12)] == 0x02)       && (newTransactionPacketsSym[(tpt-12)] == false) && (newTransactionPacketsOSet[(tpt-12)] == true)
	//          This could be any data value between 0 and 15, must be data and an ordered set
	// && (newTransactionPackets[(tpt-11)] == 0x00)
	                                                   && (newTransactionPacketsSym[(tpt-11)] == false) && (newTransactionPacketsOSet[(tpt-11)] == true)
	&& ((
	       (newTransactionPackets[(tpt-10)] == 0x4A)   && (newTransactionPacketsSym[(tpt-10)] == false) && (newTransactionPacketsOSet[(tpt-10)] == true)
	    && (newTransactionPackets[(tpt-9)] == 0x4A)    && (newTransactionPacketsSym[(tpt-9)] == false)  && (newTransactionPacketsOSet[(tpt-9)] == true)
	    && (newTransactionPackets[(tpt-8)] == 0x4A)    && (newTransactionPacketsSym[(tpt-8)] == false)  && (newTransactionPacketsOSet[(tpt-8)] == true)
	    && (newTransactionPackets[(tpt-7)] == 0x4A)    && (newTransactionPacketsSym[(tpt-7)] == false)  && (newTransactionPacketsOSet[(tpt-7)] == true)
	    && (newTransactionPackets[(tpt-6)] == 0x4A)    && (newTransactionPacketsSym[(tpt-6)] == false)  && (newTransactionPacketsOSet[(tpt-6)] == true)
	    && (newTransactionPackets[(tpt-5)] == 0x4A)    && (newTransactionPacketsSym[(tpt-5)] == false)  && (newTransactionPacketsOSet[(tpt-5)] == true)
	    && (newTransactionPackets[(tpt-4)] == 0x4A)    && (newTransactionPacketsSym[(tpt-4)] == false)  && (newTransactionPacketsOSet[(tpt-4)] == true)
	    && (newTransactionPackets[(tpt-3)] == 0x4A)    && (newTransactionPacketsSym[(tpt-3)] == false)  && (newTransactionPacketsOSet[(tpt-3)] == true)
	    && (newTransactionPackets[(tpt-2)] == 0x4A)    && (newTransactionPacketsSym[(tpt-2)] == false)  && (newTransactionPacketsOSet[(tpt-2)] == true)
	    && (newTransactionPackets[(tpt-1)] == 0x4A)    && (newTransactionPacketsSym[(tpt-1)] == false)  && (newTransactionPacketsOSet[(tpt-1)] == true)
	   ) || (
	       (newTransactionPackets[(tpt-10)] == 0x45)   && (newTransactionPacketsSym[(tpt-10)] == false) && (newTransactionPacketsOSet[(tpt-10)] == true)
	    && (newTransactionPackets[(tpt-9)] == 0x45)    && (newTransactionPacketsSym[(tpt-9)] == false)  && (newTransactionPacketsOSet[(tpt-9)] == true)
	    && (newTransactionPackets[(tpt-8)] == 0x45)    && (newTransactionPacketsSym[(tpt-8)] == false)  && (newTransactionPacketsOSet[(tpt-8)] == true)
	    && (newTransactionPackets[(tpt-7)] == 0x45)    && (newTransactionPacketsSym[(tpt-7)] == false)  && (newTransactionPacketsOSet[(tpt-7)] == true)
	    && (newTransactionPackets[(tpt-6)] == 0x45)    && (newTransactionPacketsSym[(tpt-6)] == false)  && (newTransactionPacketsOSet[(tpt-6)] == true)
	    && (newTransactionPackets[(tpt-5)] == 0x45)    && (newTransactionPacketsSym[(tpt-5)] == false)  && (newTransactionPacketsOSet[(tpt-5)] == true)
	    && (newTransactionPackets[(tpt-4)] == 0x45)    && (newTransactionPacketsSym[(tpt-4)] == false)  && (newTransactionPacketsOSet[(tpt-4)] == true)
	    && (newTransactionPackets[(tpt-3)] == 0x45)    && (newTransactionPacketsSym[(tpt-3)] == false)  && (newTransactionPacketsOSet[(tpt-3)] == true)
	    && (newTransactionPackets[(tpt-2)] == 0x45)    && (newTransactionPacketsSym[(tpt-2)] == false)  && (newTransactionPacketsOSet[(tpt-2)] == true)
	    && (newTransactionPackets[(tpt-1)] == 0x45)    && (newTransactionPacketsSym[(tpt-1)] == false)  && (newTransactionPacketsOSet[(tpt-1)] == true)
	   ))
	) {
	foundOne = true;
	setCurInputState(Link_Training);

	if(newTransactionPackets[(tpt-10)] == 0x4A)
	    trainHaveTS1 = true;
	else
	    trainHaveTS1 = false;

	if(trainHaveTS1)
	    rxLTSM = LTSM_TS1_0_x;
        else
	    rxLTSM = LTSM_TS2_0_x;

	if(VerboseReceiverMedium) {
	    printf("%s  Info: Received transaction: TS%d 0 x (at %s)", printName, (trainHaveTS1 ? 1 : 2), mCurrTimeString);
	    if(VerboseReceiverHigh) {
		for (i=(tpt-(15+mLinkWidth)); i < tpt; ++i)
		    printf("  %d=%02X", i, newTransactionPackets[i]);
	    }
	    printf("\n");
	}
	clearReceiverBuffer((tpt-(15+mLinkWidth)),(tpt-1));
    }

    // FOUND a training packet when the link was already up!! (must be trying to retrain)
    if(foundOne && (curRxLTSM == LTSM_Active)) {
       printf("%s  WARNING: Link being retrained from a running state.\n", printName);
    }
    // FOUND a training packet when we were in power saving mode L0s, clear and start training
    if(foundOne && (powerSave_L0s != 0)) {
       powerSave_L0s = 0;
//y printf("MIKE: PCIE %s Going to PowerSave State 0 for training.\n",printName);
    }

    return;
}

// Find and remove PCIE_PAD packets (Not Ordered Sets) from the end of the queue
void PCIExpressTransactor::checkCurrentTransactionPadding(void) {
   while ((newTransactionPacketsTotal > 0) &&
	  (newTransactionPackets[(newTransactionPacketsTotal-1)] == PCIE_PAD) &&
	  (newTransactionPacketsSym[(newTransactionPacketsTotal-1)] == true) &&
	  (newTransactionPacketsOSet[(newTransactionPacketsTotal-1)] == false)) {
      --newTransactionPacketsTotal;
   }
}

// Find if there are any complete transactions on the queue
bool PCIExpressTransactor::checkCurrentTransactionComplete(void) {
   SInt32 start = -1;        // transaction start pointer (STP/SDP)
   SInt32 end = -1;          // transaction end pointer (END/EDB)

   for (UInt32 i=0; i < newTransactionPacketsTotal; i++) {
      if (start == -1) {
	 if (((newTransactionPackets[i] == PCIE_STP) ||
	      (newTransactionPackets[i] == PCIE_SDP)) &&
	     (newTransactionPacketsSym[i] == true) &&
	     ((newTransactionPacketsOSet[i] == false) ||
	      (mLinkWidth = 1))) {
	    start = i;
	 }
      } else {
	 if (((newTransactionPackets[i] == PCIE_END) ||
	      (newTransactionPackets[i] == PCIE_EDB)) &&
	     (newTransactionPacketsSym[i] == true) &&
	     ((newTransactionPacketsOSet[i] == false) ||
	      (mLinkWidth = 1))) {
	    end = i;

	    // Set pointers for this transactor
	    mCurrTransactionStart = start;
	    mCurrTransactionEnd = end;
	    return true;
	 }
      }
   }

   if (start != -1) {  // saw a transaction start, isn't done coming across bus
      mInTransaction = true;
   }

   // Clear pointers
   mCurrTransactionStart = 0;
   mCurrTransactionEnd = 0;
   return false;
}

void PCIExpressTransactor::checkCurrentTransactionDLLTraining(void)
{
   UInt32 VC, HeaderFC, DataFC;
   UInt32 SeqNum;
   char printStr[15];

   UInt32 *pkts = 0;
   bool *pktsSym = 0;
   UInt32 pktslen = 0;

   pktslen = expandTransactionPackets(mCurrTransactionStart, (mCurrTransactionEnd - mCurrTransactionStart) + 1, &pkts, &pktsSym);

   if (pktslen != 8) {
      printf("%s  ERROR: Found a DLLP Packet: %s (Don't know what to do with it)\n", printName, printStr);
      if(VerboseReceiverHigh)
	 printReceivedPacketList(mCurrTransactionStart, mCurrTransactionEnd);
      // drop this, we don't know what it is.
      clearReceiverBuffer(mCurrTransactionStart, mCurrTransactionEnd);
      delete[] pkts;
      delete[] pktsSym;
      return;
   }

    switch(pkts[1])
       {
       case 0x00:  strcpy(printStr,"Ack");          break;
       case 0x10:  strcpy(printStr,"Nak");          break;
       case 0x20:  strcpy(printStr,"PM_Enter_L1 "); break;
       case 0x21:  strcpy(printStr,"PM_Enter_L23"); break;
       case 0x23:  strcpy(printStr,"PM_Act_ReqL1"); break;
       case 0x24:  strcpy(printStr,"PM_Req_Ack");   break;
       case 0x30:  strcpy(printStr,"Vendor_Spec");  break;
       case 0x40: case 0x41: case 0x42: case 0x43: case 0x44: case 0x45: case 0x46: case 0x47:
	  strcpy(printStr,"InitFC1-P   "); break;
       case 0x50: case 0x51: case 0x52: case 0x53: case 0x54: case 0x55: case 0x56: case 0x57:
	  strcpy(printStr,"InitFC1-NP  "); break;
       case 0x60: case 0x61: case 0x62: case 0x63: case 0x64: case 0x65: case 0x66: case 0x67:
	  strcpy(printStr,"InitFC1-Cpl "); break;
       case 0xC0: case 0xC1: case 0xC2: case 0xC3: case 0xC4: case 0xC5: case 0xC6: case 0xC7:
	  strcpy(printStr,"InitFC2-P   "); break;
       case 0xD0: case 0xD1: case 0xD2: case 0xD3: case 0xD4: case 0xD5: case 0xD6: case 0xD7:
	  strcpy(printStr,"InitFC2-NP  "); break;
       case 0xE0: case 0xE1: case 0xE2: case 0xE3: case 0xE4: case 0xE5: case 0xE6: case 0xE7:
	  strcpy(printStr,"InitFC2-Cpl "); break;
       case 0x80: case 0x81: case 0x82: case 0x83: case 0x84: case 0x85: case 0x86: case 0x87:
	  strcpy(printStr,"UpdateFC-P  "); break;
       case 0x90: case 0x91: case 0x92: case 0x93: case 0x94: case 0x95: case 0x96: case 0x97:
	  strcpy(printStr,"UpdateFC-NP "); break;
       case 0xA0: case 0xA1: case 0xA2: case 0xA3: case 0xA4: case 0xA5: case 0xA6: case 0xA7:
	  strcpy(printStr,"UpdateFC-Cpl"); break;
       default:    strcpy(printStr,"RESERVED");     break;
       }

    switch(pkts[1])
       {
       case 0x40: case 0x41: case 0x42: case 0x43: case 0x44: case 0x45: case 0x46: case 0x47:
       case 0x50: case 0x51: case 0x52: case 0x53: case 0x54: case 0x55: case 0x56: case 0x57:
       case 0x60: case 0x61: case 0x62: case 0x63: case 0x64: case 0x65: case 0x66: case 0x67:
       case 0xC0: case 0xC1: case 0xC2: case 0xC3: case 0xC4: case 0xC5: case 0xC6: case 0xC7:
       case 0xD0: case 0xD1: case 0xD2: case 0xD3: case 0xD4: case 0xD5: case 0xD6: case 0xD7:
       case 0xE0: case 0xE1: case 0xE2: case 0xE3: case 0xE4: case 0xE5: case 0xE6: case 0xE7:
	  // Virtual Channel Initialiaztion - sent in groups of three - don't remove here
	  // Virtual Channel is lower 3 bits of DLLP_Type
	  VC = (pkts[1] & 0x07);
	  // 8 bits:   Packet    |   2    |   3    |
	  //           Bit       |76543210|76543210|
	  //           HdrFC     |xx765432|10xxxxxx|
	  HeaderFC = ((pkts[2] & 0x3F) << 2) + ((pkts[3] & 0xC0) >> 6);
	  // 12 bits:  Packet    |   3    |   4    |
	  //           Bit       |76543210|76543210|
	  //           HdrFC     |xxxxBA98|76543210|
	  DataFC = ((pkts[3] & 0x0F) << 8) + pkts[4];
	  if(VerboseReceiverHigh)
	     printf("%s  Info: Found a DLLP Packet: %s (VC=%01X) (HdrFC=0x%02X) (DataFC=0x%03X)\n", printName, printStr, VC, HeaderFC, DataFC);

	  if ((pkts[1] & 0x80) == 0) {
	     rxDLLSM = DLLSM_Init1;
	     setCurInputState(DLL_Setup);
	  } else {
	     rxDLLSM = DLLSM_Init2;
	     setCurInputState(DLL_Setup);
	  }

	  if (HeaderFC == 0x00) {
	     HeaderFC = 0xFFFFFFFF; // unlimited
	  }
	  if (DataFC == 0x00) {
	     DataFC = 0xFFFFFFFF; // unlimited
	  }
	  switch (pkts[1] & 0xF0)
	     {
	     case 0x40:
	     case 0xC0:
		mFcTxPostedHdrCreditLimit = HeaderFC;
		mFcTxPostedDataCreditLimit = DataFC;
		break;
	     case 0x50:
	     case 0xD0:
		mFcTxNonPostedHdrCreditLimit = HeaderFC;
		mFcTxNonPostedDataCreditLimit = DataFC;
		break;
	     case 0x60:
	     case 0xE0:
		mFcTxCompletionHdrCreditLimit = HeaderFC;
		mFcTxCompletionDataCreditLimit = DataFC;
		break;
	     }

	  clearReceiverBuffer(mCurrTransactionStart, mCurrTransactionEnd);
	  break;
       case 0x80: case 0x81: case 0x82: case 0x83: case 0x84: case 0x85: case 0x86: case 0x87:
       case 0x90: case 0x91: case 0x92: case 0x93: case 0x94: case 0x95: case 0x96: case 0x97:
       case 0xA0: case 0xA1: case 0xA2: case 0xA3: case 0xA4: case 0xA5: case 0xA6: case 0xA7:
	  // Virtual Channel Updating
	  VC = (pkts[1] & 0x07);
	  HeaderFC = ((pkts[2] & 0x3F) << 2) + ((pkts[3] & 0xC0) >> 6);
	  DataFC = ((pkts[3] & 0x0F) << 8) + pkts[4];
	  if(VerboseReceiverHigh)
	     printf("%s  Info: Found a DLLP Packet: %s (VC=%01X) (HdrFC=0x%02X) (DataFC=0x%03X)\n", printName, printStr, VC, HeaderFC, DataFC);
	  
	  switch (pkts[1] & 0xF0)
	     {
	     case 0x80:  // P
		if (VerboseReceiverMedium) {
		   printf("%s  Info: Flow control credit update: Posted ", printName);
		   if (mFcTxPostedHdrCreditLimit == 0xFFFFFFFF)
		      printf("HeaderFC unlimited, ");
		   else
		      printf("HeaderFC increased by 0x%02X to 0x%02X, ", (HeaderFC - mFcTxPostedHdrCreditLimit) & 0xFF, HeaderFC);
		   if (mFcTxPostedDataCreditLimit == 0xFFFFFFFF)
		      printf("DataFC unlimited.\n");
		   else
		      printf("DataFC increased by 0x%03X to 0x%03X.\n", (DataFC - mFcTxPostedDataCreditLimit) & 0xFFF, DataFC);
		   fflush(0);
		}

		if (mFcTxPostedHdrCreditLimit != 0xFFFFFFFF)
		   mFcTxPostedHdrCreditLimit = HeaderFC;
		if (mFcTxPostedDataCreditLimit != 0xFFFFFFFF)
		   mFcTxPostedDataCreditLimit = DataFC;
		break;
	     case 0x90:  // NP
		if (VerboseReceiverMedium) {
		   printf("%s  Info: Flow control credit update: Non-Posted ", printName);
		   if (mFcTxNonPostedHdrCreditLimit == 0xFFFFFFFF)
		      printf("HeaderFC unlimited, ");
		   else
		      printf("HeaderFC increased by 0x%02X to 0x%02X, ", ((HeaderFC - mFcTxNonPostedHdrCreditLimit) & 0xFF), HeaderFC);
		   if (mFcTxNonPostedDataCreditLimit == 0xFFFFFFFF)
		      printf("DataFC unlimited.\n");
		   else
		      printf("DataFC increased by 0x%03X to 0x%03X.\n", ((DataFC - mFcTxNonPostedDataCreditLimit) & 0xFFF), DataFC);
		   fflush(0);
		}

		if (mFcTxNonPostedHdrCreditLimit != 0xFFFFFFFF)
		   mFcTxNonPostedHdrCreditLimit = HeaderFC;
		if (mFcTxNonPostedDataCreditLimit != 0xFFFFFFFF)
		   mFcTxNonPostedDataCreditLimit = DataFC;
		break;
	     case 0xA0:  // Cpl
		if (VerboseReceiverMedium) {
		   printf("%s  Info: Flow control credit update: Completion ", printName);
		   if (mFcTxCompletionHdrCreditLimit == 0xFFFFFFFF)
		      printf("HeaderFC unlimited, ");
		   else
		      printf("HeaderFC increased by 0x%02X to 0x%02X, ", ((HeaderFC - mFcTxCompletionHdrCreditLimit) & 0xFF), HeaderFC);
		   if (mFcTxCompletionDataCreditLimit == 0xFFFFFFFF)
		      printf("DataFC unlimited.\n");
		   else
		      printf("DataFC increased by 0x%03X to 0x%03X.\n", ((DataFC - mFcTxCompletionDataCreditLimit) & 0xFFF), DataFC);
		   fflush(0);
		}

		if (mFcTxCompletionHdrCreditLimit != 0xFFFFFFFF)
		   mFcTxCompletionHdrCreditLimit = HeaderFC;
		if (mFcTxCompletionDataCreditLimit != 0xFFFFFFFF)
		   mFcTxCompletionDataCreditLimit = DataFC;
		break;
	     default:
		break;
	     }

	  clearReceiverBuffer(mCurrTransactionStart, mCurrTransactionEnd);
	  break;
       case 0x00:
       case 0x10:
	  SeqNum = ((pkts[3] & 0x0F) << 8) + pkts[4];
	  if(VerboseReceiverMedium)
	     printf("%s  Info: Found a DLLP Packet: %s (for SeqNum=0x%03X)\n", printName, printStr, SeqNum);

	  // register that we received an ack/nak so next step we'll process the associated transaction
	  if(pkts[1] == 0x00)         // Ack
	     processAck(SeqNum);
	  else                        // Nak
	     processNak(SeqNum);

	  clearReceiverBuffer(mCurrTransactionStart, mCurrTransactionEnd);
	  break;
       default:
	  printf("%s  ERROR: Found a DLLP Packet: %s (Don't know what to do with it)\n", printName, printStr);
	  if(VerboseReceiverHigh)
	     printReceivedPacketList(mCurrTransactionStart, mCurrTransactionEnd);
	  // drop this, we don't know what it is.
	  clearReceiverBuffer(mCurrTransactionStart, mCurrTransactionEnd);
	  break;
       }

    delete[] pkts;
    delete[] pktsSym;
    return;
}


void PCIExpressTransactor::checkCurrentTransactionTLPs(void) {
   UInt32 length=0;
   UInt32 j;
   UInt32 crc;
   UInt32 temp = 0;
   UInt32 *tmp_pkts = 0;
   UInt32 tmp_pkts_len;
   UInt32 trans_seq_num = 0xFFFFFFFF;

   length = (mCurrTransactionEnd - mCurrTransactionStart) + 1;

   if(newTransactionPackets[mCurrTransactionEnd] == PCIE_END) {
      // process TLP packets (don't include the 'STP', LCRC[4], & 'END' packets)
      //  need to expand Ordered Sets of packets!!  [ expand ALL from 'STP' through 'END' ]
      tmp_pkts_len = expandTransactionPackets(mCurrTransactionStart, length, &tmp_pkts);
      //  check the TLP LCRC value is correct       [ CRC on all but 'STP' on front and LCRC[4] & 'END' on back ]
      if (tmp_pkts_len > 6)
	 crc = calc32bCRC(&(tmp_pkts[1]), tmp_pkts_len-6);
      else
	 crc = 0x0;

      temp = ((tmp_pkts[tmp_pkts_len-5] & 0xFF) << 24) |
	 ((tmp_pkts[tmp_pkts_len-4] & 0xFF) << 16) |
	 ((tmp_pkts[tmp_pkts_len-3] & 0xFF) << 8)  |
	 (tmp_pkts[tmp_pkts_len-2] & 0xFF);

      trans_seq_num = (((tmp_pkts[1] << 8) + tmp_pkts[2]) & 0x0FFF);

      if (mForcedNaks) {
	 // We must Nak all transactions until receiving a new copy of the original
	 // transaction that had an issue.  If this transaction is not the proper
	 // sequence number then it gets a Nak.  If this is the proper sequence
	 // number then we'll clear this flag and process it as normal.
	 if (trans_seq_num <= (mLastTxAckSeqNum + 1))
	    mForcedNaks = false;
	 else
	    temp = 0x00000000;
      }
      // Back to processing as normal
      if (crc != temp) {
	 if ((VerboseReceiverLow) && (not mForcedNaks)) {
	    printf("%s  ERROR: Bad CRC found in received TLP Transaction.  Asking for a retry on SeqNum 0x%03X.\n", printName, trans_seq_num);
	    if (VerboseReceiverMedium) {
	       printf("         Received CRC=0x%08X; Our CRC=0x%08X\n",temp,crc);
	       printf("         TransactionPackets:(");
	       if (tmp_pkts_len > 6)
		  for(j=1; j < tmp_pkts_len-6; ++j)
		     printf("0x%02X ",tmp_pkts[j]);
	       printf(")\n");
#if 0
	       printf("         TransactionPacketsAll:(");
	       for(j=0; j < tmp_pkts_len; ++j)
		  printf("0x%02X ",tmp_pkts[j]);
	       printf(")\n");
#endif
	    }
	 }
	 // addTransactionSpecialAckNak(false, trans_seq_num);
	 addTransactionSpecialAckNak(false, mLastTxAckSeqNum);  // Nak always sends SeqNum last Ack'd until other device gets the hint and retransmits SeqNum+1
	 mForcedNaks = true;  // nak all future tx until this seq num comes back
      } else {
	 addTransactionSpecialAckNak(true, trans_seq_num);
	 mLastTxAckSeqNum = trans_seq_num;

	 PCIExpressCallback *tx;
	 // send in packets, minus 'STP' & SeqNum[2] on front, LCRC[4] & 'END' on back
	 if (tmp_pkts_len > 8) {
	    tx = new PCIExpressCallback(this, &(tmp_pkts[3]), tmp_pkts_len-8);  // initialization will process packets also
	    delete tx;
	 }
      }
      delete[] tmp_pkts;
   }

   clearReceiverBuffer(mCurrTransactionStart, mCurrTransactionEnd);
   return;
}


void PCIExpressTransactor::setCurStateGeneric(pcie_state_enum_t newState, bool direction)
{
    // DIRECTION:
    //	    true (1)  = input SM
    //	    false (0) = output SM

    pcie_state_enum_t curState;
    char stateStr[20];
    UInt32 i;

    if(direction)
	curState = curInputState;
    else
	curState = curOutputState;

    if(curState != newState) {
	switch(newState)
	    {
	    case Electrical_Idle:
		strcpy(stateStr,"Electrical Idle");
		break;
	    case Logical_Idle:
		strcpy(stateStr,"Logical Idle");
		break;
	    case Link_Training:
		strcpy(stateStr,"Link Training");
		break;
	    case DLL_Setup:
		strcpy(stateStr,"DLL Setup");
		break;
	    case Clock_Tolerance:
		strcpy(stateStr,"Clock Tolerance");
		break;
	    case Skip:
		strcpy(stateStr,"Skip Ordered Set");
		break;
	    case Power_Save:
	        strcpy(stateStr,"Power Save");
		break;
	    case Fast_Training:
		strcpy(stateStr,"Fast Training");
		break;
	    case Active:
		strcpy(stateStr,"Active");
		break;
	    default:
		strcpy(stateStr,"UNKNOWN");
		break;
	    }

	if(direction) {  // input
	    if((newState == Electrical_Idle) ||
	       (newState == Power_Save)
	       ){   // will have to figure out receiver disparity when link comes back up
// printf("HEY 4  - %s (oldState = %d, newState = %d(%s)\n", printName, curState, newState, stateStr);
	       for(i=0; i < mLinkWidth; ++i) {
		  disparity_in_not_set[i] = true;
	       }
	    }
// 	    if(VerboseReceiverMedium)
// 		printf("%s  Receiver changing from '%s' to '%s' (at %s)\n", printName, curInputStateStr, stateStr, mCurrTimeString);
	    curInputState = newState;
	    strcpy(curInputStateStr,stateStr);
	} else {         // output
// 	    if(VerboseTransmitterMedium)
// 		printf("%s  Transmitter changing from '%s' to '%s' (at %s)\n", printName, curInputStateStr, stateStr, mCurrTimeString);
	    curOutputState = newState;
	    strcpy(curOutputStateStr,stateStr);
	}
    }
    return;
}

void PCIExpressTransactor::transmitIdleData(void) {
   // put idle data on bus
   for (UInt32 i=0; i < mLinkWidth; ++i) {
      mLatestPackets[i] = 0x00;
      mLatestPacketSym[i] = false;
   }
   transmitData(true);
   setCurOutputState(Logical_Idle);
   return;
}

void PCIExpressTransactor::transmitData(bool encode) {
   UInt32 *tmp_pkts = new UInt32[mLinkWidth];
   UInt32 i=0;
   UInt32 temp=0;

   for (i=0; i < mLinkWidth; ++i) {
      if (encode) {
	 if (mScrambleEnable)
	    temp = scramble_byte(mLatestPackets[i],mLatestPacketSym[i],i);
	 else
	    temp = mLatestPackets[i];

	 if (mIntfType == PCIEXtor::ePipeIntf)  // pipe intf isn't encoded
	    tmp_pkts[i] = temp;
	 else
	    tmp_pkts[i] = encoder_lut(temp,mLatestPacketSym[i],i);

      } else {
	 // SPECIAL CASE ONLY: For putting real 0's on the bus (not driving the bus)
	 tmp_pkts[i] = mLatestPackets[i];
      }
   }

   if (VerboseTransmitterHigh) {
      printf("%s  Info: Transmitting data%s: ", printName, ((encode) ? "" : " (NOT 8b/10b ENCODED)"));
      for (i=0; i < mLinkWidth; ++i) {
	 if (mLatestPacketSym[i])
	    printf("  %s", getSymStringFromByte(mLatestPackets[i]));
	 else
	    printf("  %02X", mLatestPackets[i]);
      }
      printf("   (at %s)\n", mCurrTimeString);
   }

   // put packets on into the chip (transaction->step put data into mLatestPackets for us)
   //   only try to put into mLatestPackets[] enough to fill the lanes
   //   valid link widths are x1, x2, x4, x8, x12, x16, x32
   for (i=0; i < mLinkWidth; ++i) {
      setTransmitData(i, tmp_pkts[i]);
      if (mIntfType == PCIEXtor::ePipeIntf)
	 setTransmitKCode(i, mLatestPacketSym[i]);
   }

   delete[] tmp_pkts;

  return;
}

UInt32 PCIExpressTransactor::calc16bCRC(UInt32 *bytes, UInt32 length) {
    crcGen<16> crc(0x100B);

    // std::cout << "Bytes:";
    crc.clr();
    for(unsigned int i = 0; i < length; ++i) {
	// std::cout << std::hex << " " << bytes[i] << " ";
	crc.doByte(bytes[i]);
    }
    // std::cout << std::endl;

    return(crc.getResult());
}

UInt32 PCIExpressTransactor::calc32bCRC(UInt32 *bytes, UInt32 length) {
    crcGen<32, UInt64> crc(0x04C11DB7);
    UInt32 i;

    // std::cout << "calc32bCRC: Bytes:";
    crc.clr();
    for(i=0; i < length; ++i) {
	// std::cout << std::hex << " " << bytes[i] << " ";
	crc.doByte(bytes[i]);
    }
    // std::cout << "; result=" << crc.getResult() << std::endl;

    return(crc.getResult());
}

void PCIExpressTransactor::clearReceiverBuffer(UInt32 start, UInt32 end) {
    UInt32 length;
    UInt32 temp;
    UInt32 i;

    if (VerboseReceiverHigh) {
       printf("%s  Info: Entering 'clearReceiverBuffer(%u, %u);\n", printName, start, end);
       printReceivedPacketList();
    }

    // we want a range (inclusive), but don't actually care which is start & end
    if(end < start) {
	temp = start;
	start = end;
	end = temp;
    }

    if(start < newTransactionPacketsTotal) {
	// remove if start is within range
	if (end >= newTransactionPacketsTotal) {
	    // remove all after 'start'
	    newTransactionPacketsTotal = start;
	} else {
	    length = (end - start) + 1;  // (inclusive)
	    // remove section, bring following packets up
	    for(i=start; i < (newTransactionPacketsTotal - length); ++i) {
		newTransactionPackets[i]     = newTransactionPackets[i+length];
		newTransactionPacketsSym[i]  = newTransactionPacketsSym[i+length];
		newTransactionPacketsOSet[i] = newTransactionPacketsOSet[i+length];
	    }
	    newTransactionPacketsTotal -= length;
	}
    }
    // just make sure we don't think we're still in a transaction, we shouldn't be
    // clearing if we're not done with the transaction
    mInTransaction = false;

    if (VerboseReceiverHigh) {
       printf("%s  Info: Leaving 'clearReceiverBuffer(%u, %u);\n", printName, start, end);
       printReceivedPacketList();
    }

    return;
}

UInt32 PCIExpressTransactor::expandTransactionPackets(UInt32 start, UInt32 length, UInt32 **newpkts) {
   UInt32 *pkts;
   UInt32 pktslen;

   if ((start+length) > newTransactionPacketsTotal) {
      if (VerboseReceiverLow)
	 printf("%s  ERROR: Issue attempting to expand received packets.  Receive buffer contains (0 to %u).  Trying to expand %u packets starting with number %u.\n", printName, (newTransactionPacketsTotal-1), length, start);
      if (VerboseReceiverHigh)
         printReceivedPacketList();
      *newpkts = 0;
      return 0;
   }

   pktslen = length;
   for(unsigned int i = start; i < (start+length); ++i) {
      if(newTransactionPacketsOSet[i] == true)
	 pktslen += (mLinkWidth-1);
   }
   pkts = new UInt32[pktslen];
   for (UInt32 i = 0; i < pktslen; ++i)
      pkts[i] = 0xFFFF;

   for(unsigned int i = start, j = 0; i < (start+length); ++i) {
      if(newTransactionPacketsOSet[i] == true)
	 for(unsigned int k = 0; k < mLinkWidth; ++k)
	    pkts[j++] = newTransactionPackets[i];  // make mLinkWidth copies of the packet
      else
	 pkts[j++] = newTransactionPackets[i];     // just copy the packet
   }

   *newpkts = pkts;
   return pktslen;
}

UInt32 PCIExpressTransactor::expandTransactionPackets(UInt32 start, UInt32 length, UInt32 **newpkts, bool **newpktsSym) {
   bool *pktsSym;
   UInt32 pktslen = 0;

   pktslen = expandTransactionPackets(start,length,newpkts);

   // Don't need to print error here, above function call did it already
   if (pktslen == 0) {
      *newpktsSym = 0;
      return 0;
   }

   pktsSym = new bool[pktslen];
   for (UInt32 i = 0; i < pktslen; ++i)
      pktsSym[i] = false;

   for(unsigned int i = start, j = 0; i < (start+length); ++i) {
      if(newTransactionPacketsOSet[i] == true)
	 for(unsigned int k = 0; k < mLinkWidth; ++k)
	    pktsSym[j++] = newTransactionPacketsSym[i];  // make mLinkWidth copies of the bit
      else
	 pktsSym[j++] = newTransactionPacketsSym[i];     // just copy the bit
   }

   *newpktsSym = pktsSym;
   return pktslen;
}


void PCIExpressTransactor::printTransactionQueue() {
   printTransactionQueueSingle(mTransactionListNeedCpl, "Trans Need Cpl Queue   ");
   printTransactionQueueSingle(mTransactionListNeedAck, "Trans Need Ack Queue   ");

   PCIExpressTransactionList runList;
   if (mTransactionInProgress1 != 0)
      runList.push_back(mTransactionInProgress1);
   if (mTransactionInProgress2 != 0)
      runList.push_back(mTransactionInProgress2);
   printTransactionQueueSingle(runList,                 "Trans In Progress Queue");
   runList.clear();

   printTransactionQueueSingle(mTransactionListNew,     "Trans Pending Queue    ");
}

void PCIExpressTransactor::printTransactionQueueSingle(PCIExpressTransactionList &list, const char* name) {
   PCIExpressTransactionList::const_iterator iter;
   UInt32 i;

   if (list.empty()) {
      printf("%s  Info: %s is Empty.\n", printName, name);
   } else {
      // Print the transaction names
      printf("%s  Info: %s: ", printName, name);
      for (iter = list.begin(), i=1; iter != list.end(); ++iter, ++i)
	 if ((*iter) == 0)
	    printf("[%d: ---] ", i);
	 else if ((*iter)->isTLPTransaction())
	    printf("[%d: %s, SeqNum:0x%X] ", i, (*iter)->getPrintString(), (*iter)->getSeqNum());
	 else
	    printf("[%d: %s] ", i, (*iter)->getPrintString());
      printf("\n");
   }
   return;
}

void PCIExpressTransactor::printReceivedPacketList() {
   if (newTransactionPacketsTotal == 0) {
      printf("%s  Info: Received Transaction Packet List is Empty.\n", printName);
   } else {
      printReceivedPacketList(0, newTransactionPacketsTotal-1);
   }
}

void PCIExpressTransactor::printReceivedPacketList(UInt32 start, UInt32 end) {
   UInt32 i;
   UInt32 valid_end = end;

   if (start > end) {
      printf("%s  ERROR: Unable to print Received Transaction Packet List with invalid range: Start = %d, End = %d\n", printName, start, end);
      return;
   } else if (end >= newTransactionPacketsTotal) {
      valid_end = newTransactionPacketsTotal - 1;
   }

   if (start > valid_end) {
      printf("%s  Info: Received Transaction Packet List [%d:%d] is Empty.\n", printName, start, end);
   } else {
      // Print the transaction packets
      printf("%s  Info: Received Transaction Packet List: ", printName);
      for (i = start; i <= valid_end; ++i) {
	 if (newTransactionPacketsSym[i])  // symbol
	    printf("[%u: %s%s] ", i, getSymStringFromByte(newTransactionPackets[i]),
		   ((newTransactionPacketsOSet[i]) ? "(OSet)" : ""));
	 else                              // data byte
	    printf("[%u: %02X%s] ", i, newTransactionPackets[i],
		   ((newTransactionPacketsOSet[i]) ? "(OSet)" : ""));
      }
      for (i = valid_end+1; i <= end; ++i) {
	 printf("[%u: --] ", i);
      }
      printf("\n");
   }
   return;
}

void PCIExpressTransactor::setPrintLevel(UInt32 flags) {
    // Value:   3 = High
    //          2 = Medium
    //          1 = Low
    //          0 = Off

   if (flags > 3)
      flags = 3;
   switch (flags) {
   case 0x3:
      VerboseTransmitterHigh = true;
      VerboseTransmitterMedium = true;
      VerboseTransmitterLow = true;
      VerboseReceiverHigh = true;
      VerboseReceiverMedium = true;
      VerboseReceiverLow = true;
      break;
   case 0x2:
      VerboseTransmitterHigh = false;
      VerboseTransmitterMedium = true;
      VerboseTransmitterLow = true;
      VerboseReceiverHigh = false;
      VerboseReceiverMedium = true;
      VerboseReceiverLow = true;
      break;
   case 0x1:
      VerboseTransmitterHigh = false;
      VerboseTransmitterMedium = false;
      VerboseTransmitterLow = true;
      VerboseReceiverHigh = false;
      VerboseReceiverMedium = false;
      VerboseReceiverLow = true;
      break;
   case 0x0:
      VerboseTransmitterHigh = false;
      VerboseTransmitterMedium = false;
      VerboseTransmitterLow = false;
      VerboseReceiverHigh = false;
      VerboseReceiverMedium = false;
      VerboseReceiverLow = false;
      break;
   }
}

UInt32 PCIExpressTransactor::getPrintLevel(void) {
    // Value:   3 = High
    //          2 = Medium
    //          1 = Low
    //          0 = Off

    UInt32 flags;

    if (VerboseTransmitterHigh)
	flags = 0x3;
    else if (VerboseTransmitterMedium)
	flags = 0x2;
    else if (VerboseTransmitterLow)
	flags = 0x1;
    else
	flags = 0x0;

    return flags;
}

bool PCIExpressTransactor::setSignal(PCIEXtor::PCIESignalType sigType, UInt32 index, CarbonSimNet *net) {
   bool status = true;

   if (net == 0) {
      printf("%s  ERROR: Invalid CarbonSimNet* passed to setSignal(%s, %d, <net>).\n", printName, getStringFromPCIESignalType(sigType), index);
      return false;
   }
   if (index >= mLinkWidth) {
      printf("%s  ERROR: Invalid index %d passed to setSignal(), index is not between 0 and %d (inclusive).\n", printName, index, (mLinkWidth-1));
      return false;
   }

   switch(sigType) {
   case PCIEXtor::eClock:
   case PCIEXtor::ePhyStatus:
   case PCIEXtor::ePowerDown:
   case PCIEXtor::eResetN:
   case PCIEXtor::eTxDetectRx:
      if (index == 0) {
	 status = setSignal(sigType, net);
      } else {
	 printf("%s  ERROR: Signal type %s can only have one value, setting index %d is invalid.\n", printName, getStringFromPCIESignalType(sigType), index);
	 status = false;
      }
      break;
   case PCIEXtor::eRxData: // DUT RxData, BFM TxData
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 if (net->getBitWidth() == 10) {
	    pSignalRxData[index] = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a 10-bit vector.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	    status = false;
	 }
      } else if (mIntfType == PCIEXtor::ePipeIntf) {
	 if (net->getBitWidth() == 8) {
	    pSignalRxData[index] = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a 8-bit vector.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	    status = false;
	 }
      }
      break;
   case PCIEXtor::eRxElecIdle: // DUT RxElecIdle, BFM TxElecIdle
#ifdef CARBON_BUG4162
      if (net->getBitWidth() == 1) {
#else  // ifdef CARBON_BUG4162
      if (net->isScalar()) {
#endif // ifdef CARBON_BUG4162
	 pSignalRxEIdle[index] = net;
      } else {
	 printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	 status = false;
      }
      break;
   case PCIEXtor::eTxData: // DUT TxData, BFM RxData
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 if (net->getBitWidth() == 10) {
	    pSignalTxData[index] = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a 10-bit vector.     Net '%s' has width of %d.\n\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	    status = false;
	 }
      } else if (mIntfType == PCIEXtor::ePipeIntf) {
	 if (net->getBitWidth() == 8) {
	    pSignalTxData[index] = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a 8-bit vector.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	    status = false;
	 }
      }
      break;
   case PCIEXtor::eTxElecIdle: // DUT TxElecIdle, BFM RxElecIdle
#ifdef CARBON_BUG4162
      if (net->getBitWidth() == 1) {
#else  // ifdef CARBON_BUG4162
      if (net->isScalar()) {
#endif // ifdef CARBON_BUG4162
	 pSignalTxEIdle[index] = net;
      } else {
	 printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	 status = false;
      }
      break;
   case PCIEXtor::eRxDataK: // DUT RxDataK, BFM TxDataK
      if (net->isScalar()) {
	 pSignalRxDataK[index] = net;
      } else {
	 printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	 status = false;
      }
      break;
   case PCIEXtor::eRxValid: // DUT RxValid, BFM TxValid
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 printf("%s  ERROR: Signal type %s is not valid when using the PCIEXtor::e10BitIntf interface.\n", printName, getStringFromPCIESignalType(sigType));
	 status = false;
      } else if (mIntfType == PCIEXtor::ePipeIntf) {
	 if (net->isScalar()) {
	    pSignalRxValid[index] = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	    status = false;
	 }
      }
      break;
   case PCIEXtor::eRxStatus: // DUT RxStatus, BFM TxStatus
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 printf("%s  ERROR: Signal type %s is not valid when using the PCIEXtor::e10BitIntf interface.\n", printName, getStringFromPCIESignalType(sigType));
	 status = false;
      } else if (mIntfType == PCIEXtor::ePipeIntf) {
	 if (net->getBitWidth() == 3) {
	    pSignalRxStatus[index] = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a 3-bit vector.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	    status = false;
	 }
      }
      break;
   case PCIEXtor::eTxDataK: // DUT TxDataK, BFM RxDataK
      if(net->isScalar()) {
	 pSignalTxDataK[index] = net;
      } else {
	 printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	 status = false;
      }
      break;
   case PCIEXtor::eTxCompliance: // DUT TxCompliance, BFM RxCompliance
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 printf("%s  ERROR: Signal type %s is not valid when using the PCIEXtor::e10BitIntf interface.\n", printName, getStringFromPCIESignalType(sigType));
	 status = false;
      } else if (mIntfType == PCIEXtor::ePipeIntf) {
	 if (net->isScalar()) {
	    pSignalTxCompliance[index] = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	    status = false;
	 }
      }
      break;
   case PCIEXtor::eRxPolarity: // DUT RxPolarity, BFM RxPolarity
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 printf("%s  ERROR: Signal type %s is not valid when using the PCIEXtor::e10BitIntf interface.\n", printName, getStringFromPCIESignalType(sigType));
	 status = false;
      } else if (mIntfType == PCIEXtor::ePipeIntf) {
	 if (net->isScalar()) {
	    pSignalRxPolarity[index] = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, %d, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), index, net->getName(), net->getBitWidth());
	    status = false;
	 }
      }
      break;
   default:
      printf("%s  ERROR: Signal type %s is invalid in setSignal() call.\n", printName, getStringFromPCIESignalType(sigType));
      status = false;
   }
   return status;
}

bool PCIExpressTransactor::setSignal(PCIEXtor::PCIESignalType sigType, CarbonSimNet *net) {
   bool status = true;

   if (net == 0) {
      printf("%s  ERROR: Invalid CarbonSimNet* passed to setSignal(%s, <net>).\n", printName, getStringFromPCIESignalType(sigType));
      return false;
   }

   switch(sigType) {
   case PCIEXtor::eClock:
      if (net->isScalar()) {
	 mSignalClk_SimNet = net;
	 mSignalClk_SimClock = 0;  // make sure other pointer is clear
      } else {
	 printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), net->getName(), net->getBitWidth());
	 status = false;
      }
      break;
   case PCIEXtor::ePhyStatus:
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 printf("%s  ERROR: Signal type %s is not valid when using the PCIEXtor::e10BitIntf interface.\n", printName, getStringFromPCIESignalType(sigType));
	 status = false;
      } else if (mIntfType == PCIEXtor::ePipeIntf) {
	 if (net->isScalar()) {
	    mSignalPhyStatus = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), net->getName(), net->getBitWidth());
	    status = false;
	 }
      }
      break;
   case PCIEXtor::ePowerDown:
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 printf("%s  ERROR: Signal type %s is not valid when using the PCIEXtor::e10BitIntf interface.\n", printName, getStringFromPCIESignalType(sigType));
	 status = false;
      } else if (mIntfType == PCIEXtor::ePipeIntf) {
	 if (net->getBitWidth() == 2) {
	    mSignalPowerDown = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, <net>), the CarbonSimNet must be a 2-bit signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), net->getName(), net->getBitWidth());
	    status = false;
	 }
      }
      break;
   case PCIEXtor::eResetN:
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 printf("%s  ERROR: Signal type %s is not valid when using the PCIEXtor::e10BitIntf interface.\n", printName, getStringFromPCIESignalType(sigType));
	 status = false;
      } else if (mIntfType == PCIEXtor::ePipeIntf) {
	 if (net->isScalar()) {
	    mSignalResetN = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), net->getName(), net->getBitWidth());
	    status = false;
	 }
      }
      break;
   case PCIEXtor::eTxDetectRx:
      if (mIntfType == PCIEXtor::e10BitIntf) {
	 printf("%s  ERROR: Signal type %s is not valid when using the PCIEXtor::e10BitIntf interface.\n", printName, getStringFromPCIESignalType(sigType));
	 status = false;
      } else if (mIntfType == PCIEXtor::ePipeIntf) {
	 if (net->isScalar()) {
	    mSignalTxDetectRx = net;
	 } else {
	    printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", printName, getStringFromPCIESignalType(sigType), net->getName(), net->getBitWidth());
	    status = false;
	 }
      }
      break;
   case PCIEXtor::eRxData:
   case PCIEXtor::eRxElecIdle:
   case PCIEXtor::eTxData:
   case PCIEXtor::eTxElecIdle:
   case PCIEXtor::eRxDataK:
   case PCIEXtor::eRxValid:
   case PCIEXtor::eRxStatus:
   case PCIEXtor::eTxDataK:
   case PCIEXtor::eTxCompliance:
   case PCIEXtor::eRxPolarity:
      if (mLinkWidth == 1) {
	 status = setSignal(sigType, 0, net);
      } else {
	 printf("%s  ERROR: Signal type %s needs to be set for each individual lane.\n", printName, getStringFromPCIESignalType(sigType));
	 status = false;
      }
      break;
   default:
      printf("%s  ERROR: Signal type %s is invalid in setSignal() call.\n", printName, getStringFromPCIESignalType(sigType));
      status = false;
   }
   return status;
}

bool PCIExpressTransactor::setSignal(CarbonSimClock *net) {
   if (net == 0) {
      printf("%s  ERROR: Invalid CarbonSimClock* passed to setSignal(CarbonSimClock *<net>).\n", printName);
      return false;
   }

   mSignalClk_SimClock = net;
   mSignalClk_SimNet = 0;  // make sure other pointer is clear
   return true;
}

const char* PCIExpressTransactor::getStringFromPCIESignalType(PCIEXtor::PCIESignalType sigType) {
   switch(sigType) {
   case PCIEXtor::eClock:
      return "PCIEXtor::eClock";
   case PCIEXtor::ePhyStatus:
      return "PCIEXtor::ePhyStatus";
   case PCIEXtor::ePowerDown:
      return "PCIEXtor::ePowerDown";
   case PCIEXtor::eResetN:
      return "PCIEXtor::eResetN";
   case PCIEXtor::eRxData:
      return "PCIEXtor::eRxData";
   case PCIEXtor::eRxDataK:
      return "PCIEXtor::eRxDataK";
   case PCIEXtor::eRxElecIdle:
      return "PCIEXtor::eRxElecIdle";
   case PCIEXtor::eRxPolarity:
      return "PCIEXtor::eRxPolarity";
   case PCIEXtor::eRxStatus:
      return "PCIEXtor::eRxStatus";
   case PCIEXtor::eRxValid:
      return "PCIEXtor::eRxValid";
   case PCIEXtor::eTxCompliance:
      return "PCIEXtor::eTxCompliance";
   case PCIEXtor::eTxData:
      return "PCIEXtor::eTxData";
   case PCIEXtor::eTxDataK:
      return "PCIEXtor::eTxDataK";
   case PCIEXtor::eTxDetectRx:
      return "PCIEXtor::eTxDetectRx";
   case PCIEXtor::eTxElecIdle:
      return "PCIEXtor::eTxElecIdle";
   default:
      return "UNKNOWN";
   }
}

void PCIExpressTransactor::setElecIdleActiveState(bool tf) {
   if (mIntfType == PCIEXtor::e10BitIntf) {
      mEIdleActiveHigh = tf;
   } else if (mIntfType == PCIEXtor::ePipeIntf) {
      if(VerboseReceiverLow)
	 printf("%s  WARNING: Electrical idle signal's active level cannot be changed when using the PCIEXtor::ePipeIntf interface.\n", printName);
   }
}

bool PCIExpressTransactor::isConfigured(void) {
    bool status = true;

    if ((mSignalClk_SimNet == 0) && (mSignalClk_SimClock == 0))  // need to set one or the other
       status = false;
    if ((mLinkWidth != 1) && (mLinkWidth != 2) && (mLinkWidth != 4) && (mLinkWidth != 8)
        && (mLinkWidth != 12) && (mLinkWidth != 16) && (mLinkWidth != 32))
       status = false;

    if (mIntfType == PCIEXtor::e10BitIntf) {
       if (!(isConfigured_10BitIntf()))
	  status = false;
    } else if (mIntfType == PCIEXtor::ePipeIntf) {
       if (!(isConfigured_PipeIntf()))
	  status = false;
    } else {
       status = false;
    }

    if (not mTransCBFn) {    // need to tell us where to call when we
       status = false;       // receive a transaction
    }

    // save a copy of the latest return value so we don't need to call the function again
    mFullyConfigured = status;
    // clear flag so the user can be warned again if the config is still invalid and they try to call step()
    mHasBeenWarnedAboutConfig = false;
    return status;
}

bool PCIExpressTransactor::isConfigured_10BitIntf(void) {
   UInt32 i;
   // NOTE: Net size tests are taken care of when the user adds the CarbonSimNets
   for (i=0; i<mLinkWidth; ++i) {
      // make sure we have a value for each of the pointers
      if ((pSignalRxData[i] == 0) || (pSignalTxData[i] == 0) || (pSignalRxEIdle[i] == 0) || (pSignalTxEIdle[i] == 0))
	 return false;
   }
   return true;
}

bool PCIExpressTransactor::isConfigured_PipeIntf(void) {
   UInt32 i;
   // NOTE: Net size tests are taken care of when the user adds the CarbonSimNets
   for (i=0; i<mLinkWidth; ++i) {
      // make sure we have a value for each of the pointers
      if (   (pSignalRxData[i] == 0)
	  || (pSignalRxDataK[i] == 0)
	  || (pSignalTxCompliance[i] == 0)
	  || (pSignalRxPolarity[i] == 0)
	  || (pSignalTxData[i] == 0)
	  || (pSignalTxDataK[i] == 0)
	  || (pSignalRxStatus[i] == 0)
	  || (pSignalRxValid[i] == 0)
	  || (mSignalTxDetectRx == 0)
	  || (mSignalResetN == 0)
	  || (mSignalPowerDown == 0)
	  || (mSignalPhyStatus == 0)
	 )
	 return false;
   }
   return true;
}

void PCIExpressTransactor::printConfiguration(void) {
   UInt32 i, j, numNetTypes;
   CarbonSimNet **pNet=0;
   CarbonSimNet *net=0;
   char stringTxRx[45];

   printf("**** PRINTING CONFIGURATION INFO ****\n");
   printf("BFM Instance Name: %s\n", printName);
   printf("Number of lanes in the link: %d\n", mLinkWidth);

   printf("Clock: ");
   if (mSignalClk_SimClock) {
      printf("(CarbonSimClock)");
   } else if (mSignalClk_SimNet) {
      printf("%s", mSignalClk_SimNet->getName());
   } else {
      printf("Not Set");
   }
   printf("\n");

   if (mIntfType == PCIEXtor::e10BitIntf)
      numNetTypes = 4;
   else if (mIntfType == PCIEXtor::ePipeIntf)
      numNetTypes = 14;
   else
      numNetTypes = 0;

   for (j=0; j < numNetTypes; ++j) {
      switch(j) {
      case 0:  pNet = pSignalTxData;       strcpy(stringTxRx, "DUT Transmitter Data");                       break;
      case 1:  pNet = pSignalRxData;       strcpy(stringTxRx, "DUT Receiver Data");                          break;
      case 2:  pNet = pSignalTxEIdle;      strcpy(stringTxRx, "DUT Transmitter Elec Idle");                  break;
      case 3:  pNet = pSignalRxEIdle;      strcpy(stringTxRx, "DUT Receiver Elec Idle");                     break;
	 // only for ePipeIntf
      case 4:  pNet = pSignalTxDataK;      strcpy(stringTxRx, "DUT Transmitter Symbol Flag");                break;
      case 5:  pNet = pSignalRxDataK;      strcpy(stringTxRx, "DUT Receiver Symbol Flag");                   break;
      case 6:  pNet = pSignalTxCompliance; strcpy(stringTxRx, "DUT Transmitter Compliance Pattern");         break;
      case 7:  pNet = pSignalRxPolarity;   strcpy(stringTxRx, "DUT Receiver Polarity");                      break;
      case 8:  pNet = pSignalRxStatus;     strcpy(stringTxRx, "DUT Receiver Status");                        break;
      case 9:  pNet = pSignalRxValid;      strcpy(stringTxRx, "DUT Receiver Valid");                         break;

      case 10: net = mSignalTxDetectRx;   strcpy(stringTxRx, "DUT Transmitter Detect Receiver / Loopback"); break;
      case 11: net = mSignalResetN;       strcpy(stringTxRx, "DUT Phy Reset");                              break;
      case 12: net = mSignalPowerDown;    strcpy(stringTxRx, "DUT Phy Power Down");                         break;
      case 13: net = mSignalPhyStatus;    strcpy(stringTxRx, "DUT Phy Status");                             break;
      default:
	 continue;
      }
      for (i=0; i<mLinkWidth; ++i) {
	 if (j < 10) {
	    net = pNet[i];
	    if (mLinkWidth > 9)
	       printf("%s Lane %02d: ", stringTxRx, i);
	    else
	       printf("%s Lane %01d: ", stringTxRx, i);
	 } else {
	    printf("%s: ", stringTxRx);
	 }
	 if (net) {
	    printf("%s", net->getName());
	    if (net->isVector())
	       printf("[%d:%d]", net->getMSB(), net->getLSB());
	    if ((j==2 || j==3) && (mIntfType != PCIEXtor::ePipeIntf))  // ePipeIntf says these are always active HI
	       printf(" (Active %s)", ((mEIdleActiveHigh)?"High":"Low"));
	 } else {
	    printf("Not Set");
	 }
	 printf("\n");
	 if (j >= 10)
	    break;
      }
   }

   if (mTransCBFn)
      printf("Transaction callback is set.\n");
   else
      printf("Transaction callback is Not Set.\n");

   printf("\n");
}

bool PCIExpressTransactor::isLinkActive(void) {
   // return ((curInputState==Active) && (curOutputState==Active));
   if (((curInputState==Active) || (curInputState==Logical_Idle))
       && ((curOutputState==Active) || (curOutputState==Logical_Idle))
       && (txLTSM==LTSM_Active) && (rxLTSM==LTSM_Active)
       && (txDLLSM==DLLSM_Active) && (rxDLLSM==DLLSM_Active)) {
//       std::cout << "MIKE[isLinkActive]: Returning success." << std::endl;
      return true;
   } else {
//      printf("MIKE[isLinkActive]: curInputState(%c), curOutputState(%c), txLTSM(%c), rxLTSM(%c), txDLLSM(%c), rxDLLSM(%c) (at %s)\n"
//	     , (((curInputState==Active) || (curInputState==Logical_Idle))?'T':'F')
//	     , (((curOutputState==Active) || (curOutputState==Logical_Idle))?'T':'F')
//	     , ((txLTSM==LTSM_Active)?'T':'F')
//	     , ((rxLTSM==LTSM_Active)?'T':'F')
//	     , ((txDLLSM==DLLSM_Active)?'T':'F')
//	     , ((rxDLLSM==DLLSM_Active)?'T':'F')
//	     , mCurrTimeString);
      return false;
   }
}

void PCIExpressTransactor::printLinkState(void) {
   printf("**** PRINTING LINK STATE ****\n");
   printf("Transmitter for %s: %s\n", printName, curOutputStateStr);
   printf("Receiver for %s:    %s\n", printName, curInputStateStr);
   printf("Time: %s\n", mCurrTimeString);

   printf(  "Transmitter Link Training State Machine:   %s", getLTSMStateString(txLTSM));
   printf("\nReceiver Link Training State Machine:      %s", getLTSMStateString(rxLTSM));
   printf("\nTransmitter Data Link Layer State Machine: %s", getDLLSMStateString(txDLLSM));
   printf("\nReceiver Data Link Layer State Machine:    %s", getDLLSMStateString(rxDLLSM));
   printf("\n");
}

bool PCIExpressTransactor::getReturnData(PCIECmd *cmd) {
   bool retStatus = true;
   PCIECmd *our_copy = 0;

   // Copy in the next Return Data PCIECmd we have stored up
   // then delete our copy

   if (cmd == 0) {
      if (VerboseReceiverLow)
	 printf("%s  ERROR: PCIECmd object passed to getReturnData() is NULL.\n", printName);
      return false;
   }

   if (mReturnDataList.empty()) {
      retStatus = false;
   } else {
      our_copy = mReturnDataList.front();
      cmd->copy(*our_copy);
      mReturnDataList.pop_front();
      delete our_copy;
   }

   return retStatus;
}

bool PCIExpressTransactor::getReceivedTrans(PCIECmd *cmd) {
   bool retStatus = true;
   PCIECmd *our_copy = 0;

   // Copy in the next Received Data PCIECmd we have stored up
   // then delete our copy

   if (cmd == 0) {
      if (VerboseReceiverLow)
	 printf("%s  ERROR: PCIECmd object passed to getReceivedTrans() is NULL.\n", printName);
      return false;
   }

   if (mReceivedTransList.empty()) {
      retStatus = false;
   } else {
      our_copy = mReceivedTransList.front();
      cmd->copy(*our_copy);
      mReceivedTransList.pop_front();
      delete our_copy;
   }
   
   return retStatus;
}

const char* PCIExpressTransactor::getLTSMStateString(pcie_LTSM_state_t sm) {
   switch(sm) {
   case LTSM_Electrical_Idle: return "Electrical Idle";
   case LTSM_Logical_Idle:    return "Logical Idle";
   case LTSM_TS1_P_P:         return "TS1 (PAD,PAD)";
   case LTSM_TS2_P_P:         return "TS2 (PAD,PAD)";
   case LTSM_TS1_0_P:         return "TS1 (0, PAD)";
   case LTSM_TS2_0_P:         return "TS2 (0, PAD)";
   case LTSM_TS1_0_x:         return "TS1 (0, <lane>)";
   case LTSM_TS2_0_x:         return "TS2 (0, <lane>)";
   case LTSM_Active:          return "Active";
   default:                   return "ERROR";
   }
}

const char* PCIExpressTransactor::getDLLSMStateString(pcie_DLLSM_state_t sm) {
   switch(sm) {
   case DLLSM_Inactive: return "Inactive";
   case DLLSM_Init1:    return "Init1";
   case DLLSM_Init2:    return "Init2";
   case DLLSM_Active:   return "Active";
   default:             return "ERROR";
   }
}

bool PCIExpressTransactor::startTraining(void) {
   bool result;
   if (txLTSM == LTSM_Electrical_Idle) {
      mStartTraining = 1;
      result = true;
   } else {
      result = false;
   }
   return result;
}

bool PCIExpressTransactor::startTraining(CarbonTime tick) {
   bool result;
   if (startTraining()) {
      mStartTrainingPrefaceIdleTime = tick;
      result = true;
   } else {
      result = false;
   }
   return result;
}

void PCIExpressTransactor::setTrainingBypass(bool tf) {
   mTrainingBypass = tf;
   if(VerboseTransmitterLow || VerboseReceiverLow)
      printf("%s  Info: Link training bypass %s\n",printName, (tf ? "Enabled" : "Disabled"));
}

void PCIExpressTransactor::setMaxPayloadSize(UInt32 val) {
   // Use encoded values from Spec 1.0a Sec 7.8.4 (Device Control Register)
   // This sets the maximum number of bytes of data allowed to be sent
   // in a single transaction.  The default is value 0x0, 128B, 32DW.
   switch (val & 0x7) {
   case 0x0: mMaxPayloadSizeDW =   32; break;  //  128B
   case 0x1: mMaxPayloadSizeDW =   64; break;  //  256B
   case 0x2: mMaxPayloadSizeDW =  128; break;  //  512B
   case 0x3: mMaxPayloadSizeDW =  256; break;  // 1024B
   case 0x4: mMaxPayloadSizeDW =  512; break;  // 2048B
   case 0x5: mMaxPayloadSizeDW = 1024; break;  // 4096B
   default:                               // Reserved
      if(VerboseTransmitterLow || VerboseReceiverLow)
	 printf("%s  ERROR: Max_Payload_Size parameter written with an invalid value: 0x%01X\n", printName, (val & 0x7));
      break;
   }
   if(VerboseTransmitterLow || VerboseReceiverLow)
      printf("%s  Info: Max_Payload_Size parameter set to %d bytes\n", printName, (mMaxPayloadSizeDW * 4));
   return;
}

UInt32 PCIExpressTransactor::getMaxPayloadSize(void) {
   UInt32 val;
   switch (mMaxPayloadSizeDW) {
   case   32: val = 0x0; break;
   case   64: val = 0x1; break;
   case  128: val = 0x2; break;
   case  256: val = 0x3; break;
   case  512: val = 0x4; break;
   case 1024: val = 0x5; break;
   default: val = 0x0; break;
   }
   return val;
}

void PCIExpressTransactor::updateCurrTimeString(void) {
   if (mCurrTimescaleValid) {
      float tempTime;
      float factor;
      char format[10];
      switch(mCurrTimescale) {
      case e1fs:   strcpy(format,"%0.06fns"); factor=1000000; break;
      case e10fs:  strcpy(format,"%0.05fns"); factor=100000; break;
      case e100fs: strcpy(format,"%0.04fns"); factor=10000; break;
      case e1ps:   strcpy(format,"%0.03fns"); factor=1000; break;
      case e10ps:  strcpy(format,"%0.02fns"); factor=100; break;
      case e100ps: strcpy(format,"%0.01fns"); factor=10; break;
      case e1ns:   strcpy(format,"%0.0fns");    factor=1; break;
      case e10ns:  strcpy(format,"%0.0fns");    factor=0.1; break;
      case e100ns: strcpy(format,"%0.0fns");    factor=0.01; break;
      case e1us:   strcpy(format,"%0.0fns");    factor=0.001; break;
      case e10us:  strcpy(format,"%0.0fns");    factor=0.0001; break;
      case e100us: strcpy(format,"%0.0fns");    factor=0.00001; break;
      case e1ms:   strcpy(format,"%0.0fns");    factor=0.000001; break;
      case e10ms:  strcpy(format,"%0.0fns");    factor=0.0000001; break;
      case e100ms: strcpy(format,"%0.0fns");    factor=0.00000001; break;
      case e1s:    strcpy(format,"%0.0fns");    factor=0.000000001; break;
      case e10s:   strcpy(format,"%0.0fns");    factor=0.0000000001; break;
      case e100s:  strcpy(format,"%0.0fns");    factor=0.00000000001; break;
      default:     strcpy(format,"%0.0fns");    factor=1; break;
      }
      tempTime = ((float)(mCurrTick)) / factor;
      sprintf(mCurrTimeString, format, tempTime);
   } else {
      sprintf(mCurrTimeString, Format64(u), mCurrTick);
   }
}

void PCIExpressTransactor::callTransCallbackFn(PCIEXtor::PCIECallbackType type, PCIECmd *data) {
   // make Copy of 'data' object so it doesn't go out of scope or get deleted on us
   PCIECmd *data_copy = new PCIECmd(*data);  // deleted when pop'd off the queue
   if (type == PCIEXtor::eReceivedTrans) {
      mReceivedTransList.push_back(data_copy);
   } else if (type == PCIEXtor::eReturnData) {
      // clear completion flag from appropriate transaction
      processCompletion(data_copy);
      mReturnDataList.push_back(data_copy);
   } else {
      printf("%s  ERROR: Internally triggered transaction callback for unknown transaction category.  Type = '%s' (0x%0X).", printName, PCIEXtor::getStrFromType(type), type);
   }

   mTransCBFn(mParent, type, mTransCBInfo);
}

const char* PCIExpressTransactor::getSymStringFromByte(UInt32 byte) {
   switch(byte) {
   case PCIE_COM:   return "COM";
   case PCIE_STP:   return "STP";
   case PCIE_SDP:   return "SDP";
   case PCIE_END:   return "END";
   case PCIE_EDB:   return "EDB";
   case PCIE_PAD:   return "PAD";
   case PCIE_SKP:   return "SKP";
   case PCIE_FTS:   return "FTS";
   case PCIE_IDL:   return "IDL";
   case PCIE_RSVD1:
   case PCIE_RSVD2:
   case PCIE_RSVD3:
                    return "RSVD";
   default:
                    return "INVALID";
   }
}

UInt32 PCIExpressTransactor::getClkValue(void) {
   UInt32 val;
   if (mSignalClk_SimClock)
      val = mSignalClk_SimClock->getValue();
   else
      mSignalClk_SimNet->examine(&val);
   return(val);
}

void PCIExpressTransactor::setTransmitLos(UInt32 index, UInt32 value) {
   UInt32 temp = !(mEIdleActiveHigh ^ value);
   setTransmitPins(pSignalRxEIdle, "RxElecIdle", index, temp);
}

UInt32 PCIExpressTransactor::getReceiveLos(UInt32 index) {
   UInt32 temp;
   UInt32 value = getReceivePins(pSignalTxEIdle, "TxElecIdle", index);

   // Return value is active high, we need to account for the design's use of the signal
   temp = !(mEIdleActiveHigh ^ value);
   return temp;
}

void PCIExpressTransactor::setTransmitPins(CarbonSimNet* net, const char* netPrintName, UInt32 value) {
   net->deposit(&value);
}

void PCIExpressTransactor::setTransmitPins(CarbonSimNet** pNet, const char* netPrintName, UInt32 index, UInt32 value) {
   if (index < mLinkWidth)
      pNet[index]->deposit(&value);
   else
      printf("%s  ERROR: Invalid vector index trying to set DUT receiver %s value, index 0x%X is not between 0x0 and 0x%X (inclusive).\n", printName, netPrintName, index, (mLinkWidth-1));
}

UInt32 PCIExpressTransactor::getReceivePins(CarbonSimNet* net, const char* netPrintName) {
   UInt32 value = 0;
   net->examine(&value);
   return value;
}

UInt32 PCIExpressTransactor::getReceivePins(CarbonSimNet** pNet, const char* netPrintName, UInt32 index) {
   UInt32 value = 0;
   if (index < mLinkWidth)
      pNet[index]->examine(&value);
   else
      printf("%s  ERROR: Invalid vector index trying to get DUT transmitter %s value, index 0x%X is not between 0x0 and 0x%X (inclusive).\n", printName, netPrintName, index, (mLinkWidth-1));
   return value;
}

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
