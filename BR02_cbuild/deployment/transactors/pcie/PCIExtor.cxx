#include "PCIExtor.h"
#include "PCIExpressTransactor.h"
#include "PCIExpressHandler.h"
#include "PCIExpressCommand.h"
#include <sstream>

PCIECmd::PCIECmd()
    : pimpl_( new PCIExpressCommand() ) {
}

PCIECmd::PCIECmd(const PCIECmd& in)
    : pimpl_( new PCIExpressCommand(*(in.pimpl_)) ) {
}

PCIECmd::~PCIECmd() {
    delete pimpl_;
}

const char* PCIECmd::getStrFromType (PCIECmdType type) {
   std::ostringstream oss;

   switch (type) {
   case PCIECmd::MRd32:   return "MRd32";
   case PCIECmd::MRd64:   return "MRd64";
   case PCIECmd::MRdLk32: return "MRdLk32";
   case PCIECmd::MRdLk64: return "MRdLk64";
   case PCIECmd::MWr32:   return "MWr32";
   case PCIECmd::MWr64:   return "MWr64";
   case PCIECmd::IORd:    return "IORd";
   case PCIECmd::IOWr:    return "IOWr";
   case PCIECmd::CfgRd0:  return "CfgRd0";
   case PCIECmd::CfgWr0:  return "CfgWr0";
   case PCIECmd::CfgRd1:  return "CfgRd1";
   case PCIECmd::CfgWr1:  return "CfgWr1";
   case PCIECmd::Msg:     return "Msg";
   case PCIECmd::MsgD:    return "MsgD";
   case PCIECmd::Cpl:     return "Cpl";
   case PCIECmd::CplD:    return "CplD";
   case PCIECmd::CplLk:   return "CplLk";
   case PCIECmd::CplDLk:  return "CplDLk";
   default:               return "Invalid Type";
   }
}

void PCIECmd::copy(const PCIECmd& in) {
   pimpl_->copy(*(in.pimpl_));
}

void PCIECmd::setCmd(UInt32 cmd) {
    pimpl_->setCmd(cmd);
}

UInt32 PCIECmd::getCmd() {
    return pimpl_->getCmd();
}

void PCIECmd::setCmdByName(PCIECmd::PCIECmdType cmd) {
    pimpl_->setCmdByName(cmd);
}

PCIECmd::PCIECmdType PCIECmd::getCmdByName() {
    return pimpl_->getCmdByName();
}

void PCIECmd::setAddr(UInt64 addr) {
    pimpl_->setAddr(addr);
}

UInt64 PCIECmd::getAddr() {
    return pimpl_->getAddr();
}

void PCIECmd::setReqId(UInt32 id) {
    pimpl_->setReqId(id);
}

UInt32 PCIECmd::getReqId() {
    return pimpl_->getReqId();
}

void PCIECmd::setTag(UInt32 tag) {
    pimpl_->setTag(tag);
}

UInt32 PCIECmd::getTag() {
    return pimpl_->getTag();
}

void PCIECmd::setRegNum(UInt32 reg) {
    pimpl_->setRegNum(reg);
}

UInt32 PCIECmd::getRegNum() {
    return pimpl_->getRegNum();
}

void PCIECmd::setCompId(UInt32 id) {
    pimpl_->setCompId(id);
}

UInt32 PCIECmd::getCompId() {
    return pimpl_->getCompId();
}

void PCIECmd::setMsgRout(UInt32 rout) {
    pimpl_->setMsgRout(rout);
}

UInt32 PCIECmd::getMsgRout() {
    return pimpl_->getMsgRout();
}

void PCIECmd::setMsgCode(UInt32 code) {
    pimpl_->setMsgCode(code);
}

UInt32 PCIECmd::getMsgCode() {
    return pimpl_->getMsgCode();
}

void PCIECmd::setLowAddr(UInt32 addr) {
    pimpl_->setLowAddr(addr);
}

UInt32 PCIECmd::getLowAddr() {
    return pimpl_->getLowAddr();
}

void PCIECmd::setByteCount(UInt32 count) {
    pimpl_->setByteCount(count);
}

UInt32 PCIECmd::getByteCount() {
    return pimpl_->getByteCount();
}

UInt32 PCIECmd::getDWCount() {
    return pimpl_->getDWCount();
}

void PCIECmd::setFirstBE(UInt32 be) {
    pimpl_->setFirstBE(be);
}

UInt32 PCIECmd::getFirstBE() {
    return pimpl_->getFirstBE();
}

void PCIECmd::setLastBE(UInt32 be) {
    pimpl_->setLastBE(be);
}

UInt32 PCIECmd::getLastBE() {
    return pimpl_->getLastBE();
}

void PCIECmd::setCplStatus(UInt32 status) {
    pimpl_->setCplStatus(status);
}

UInt32 PCIECmd::getCplStatus() {
   return pimpl_->getCplStatus();
}

void PCIECmd::setDataByte(UInt32 index, UInt8 data) {
   pimpl_->setDataByte(index, data);
}

UInt8 PCIECmd::getDataByte(UInt32 index) {
   return pimpl_->getDataByte(index);
}

void PCIECmd::resizeDataBytes(UInt32 start, UInt32 end) {
   pimpl_->resizeDataBytes(start, end);
}

UInt32 PCIECmd::getDataByteCount(void) {
   return pimpl_->getDataByteCount();
}

void PCIECmd::setDataLength(UInt32 len) {
   pimpl_->setDataLength(len);
}

UInt32 PCIECmd::getDataLength(void) {
   return pimpl_->getDataLength();
}

SInt32 PCIECmd::getTransId() {
   return pimpl_->getTransId();
}

bool PCIECmd::isRead() {
   return pimpl_->isRead();
}

bool PCIECmd::isWrite() {
   return pimpl_->isWrite();
}

//#################################

PCIEHandler::PCIEHandler()
   : pimpl_(new PCIExpressHandler()) {
}

PCIEHandler::~PCIEHandler() {
   delete pimpl_;
}

bool PCIEHandler::bindSimMaster(CarbonSimMaster *sim) {
   return pimpl_->bindSimMaster(sim);
}

bool PCIEHandler::bindTransactor(PCIEXtor *xtor) {
   return pimpl_->bindTransactor(xtor);
}

bool PCIEHandler::isDone() {
   return pimpl_->isDone();
}

void PCIEHandler::setUseCarbonSimInterrupt(bool tf) {
   pimpl_->setUseCarbonSimInterrupt(tf);
}

bool PCIEHandler::getUseCarbonSimInterrupt() {
   return pimpl_->getUseCarbonSimInterrupt();
}

bool PCIEHandler::addInterruptTime(CarbonTime int_time) {
   return pimpl_->addInterruptTime(int_time);
}

void PCIEHandler::setDriveTransactors(bool tf) {
   pimpl_->setDriveTransactors(tf);
}

bool PCIEHandler::getDriveTransactors() {
   return pimpl_->getDriveTransactors();
}

void PCIEHandler::step(CarbonTime tick) {
   pimpl_->step(tick);
}

void PCIEHandler::idle(UInt64 cycles) {
   pimpl_->idle(cycles);
}

//#################################

PCIEXtor::PCIEXtor(PCIEXtor::PCIEIntfType intf, 
		   UInt32 link_width,
		   const char *print_name)
    : pimpl_( new PCIExpressTransactor(this, intf, link_width, const_cast<char *>(print_name)) ) {
}

PCIEXtor::~PCIEXtor() {
    delete pimpl_;
}

const char* PCIEXtor::getStrFromType (PCIEIntfType type) {
   switch (type) {
   case PCIEXtor::e10BitIntf: return "e10BitIntf";
   case PCIEXtor::ePipeIntf:  return "ePipeIntf";
   default:                   return "Invalid Type";
   }
}

const char* PCIEXtor::getStrFromType (PCIECallbackType type) {
   switch (type) {
   case PCIEXtor::eReturnData:    return "eReturnData";
   case PCIEXtor::eReceivedTrans: return "eReceivedTrans";
   default:                       return "Invalid Type";
   }
}

const char* PCIEXtor::getStrFromType (PCIESignalType type) {
   switch (type) {
   case PCIEXtor::eClock:        return "eClock";
   case PCIEXtor::eRxData:       return "eRxData";
   case PCIEXtor::eRxElecIdle:   return "eRxElecIdle";
   case PCIEXtor::eTxData:       return "eTxData";
   case PCIEXtor::eTxElecIdle:   return "eTxElecIdle";
   case PCIEXtor::eRxDataK:      return "eRxDataK";
   case PCIEXtor::eRxValid:      return "eRxValid";
   case PCIEXtor::eRxStatus:     return "eRxStatus";
   case PCIEXtor::eTxDataK:      return "eTxDataK";
   case PCIEXtor::eTxCompliance: return "eTxCompliance";
   case PCIEXtor::eRxPolarity:   return "eRxPolarity";
   default:                      return "Invalid Type";
   }
}

void PCIEXtor::setPrintLevel(UInt32 level) {
    pimpl_->setPrintLevel(level);
}

UInt32 PCIEXtor::getPrintLevel() {
    return pimpl_->getPrintLevel();
}

void PCIEXtor::setBusNum(UInt32 num) {
    pimpl_->setBusNum(num);
}

UInt32 PCIEXtor::getBusNum() {
    return pimpl_->getBusNum();
}

void PCIEXtor::setDevNum(UInt32 num) {
    pimpl_->setDevNum(num);
}

UInt32 PCIEXtor::getDevNum() {
    return pimpl_->getDevNum();
}

void PCIEXtor::setFuncNum(UInt32 num) {
    pimpl_->setFuncNum(num);
}

UInt32 PCIEXtor::getFuncNum() {
    return pimpl_->getFuncNum();
}

void PCIEXtor::setScrambleStatus(bool tf) {
    pimpl_->setScrambleStatus(tf);
}

bool PCIEXtor::getScrambleStatus() {
    return pimpl_->getScrambleStatus();
}

void PCIEXtor::setMaxTransInFlight(UInt32 max) {
   pimpl_->setMaxTransInFlight(max);
}

UInt32 PCIEXtor::getMaxTransInFlight() {
   return pimpl_->getMaxTransInFlight();
}

bool PCIEXtor::startTraining() {
   return pimpl_->startTraining();
}

bool PCIEXtor::startTraining(CarbonTime tick) {
   return pimpl_->startTraining(tick);
}

void PCIEXtor::setTrainingBypass(bool tf) {
   pimpl_->setTrainingBypass(tf);
}

bool PCIEXtor::getTrainingBypass() {
   return pimpl_->getTrainingBypass();
}

void PCIEXtor::setMaxPayloadSize(UInt32 val) {
   pimpl_->setMaxPayloadSize(val);
}

UInt32 PCIEXtor::getMaxPayloadSize() {
   return pimpl_->getMaxPayloadSize();
}

void PCIEXtor::setTransCallbackFn(PCIEXtor::PCIETransCBFn *transCbFn, void* transCbInfo) {
   pimpl_->setTransCallbackFn(transCbFn, transCbInfo);
}

PCIEXtor::PCIETransCBFn* PCIEXtor::getTransCallbackFn() {
   return pimpl_->getTransCallbackFn();
}

void* PCIEXtor::getTransCallbackData() {
   return pimpl_->getTransCallbackData();
}

bool PCIEXtor::getReturnData(PCIECmd *cmd) {
   return pimpl_->getReturnData(cmd);
}

UInt32 PCIEXtor::getReturnDataQueueLength(void) {
   return pimpl_->getReturnDataQueueLength();
}

bool PCIEXtor::getReceivedTrans(PCIECmd *cmd) {
   return pimpl_->getReceivedTrans(cmd);
}

UInt32 PCIEXtor::getReceivedTransQueueLength(void) {
   return pimpl_->getReceivedTransQueueLength();
}

UInt32 PCIEXtor::getLinkWidth() {
    return pimpl_->getLinkWidth();
}

char* PCIEXtor::getPrintName() {
    return pimpl_->getPrintName();
}

PCIEXtor::PCIEIntfType PCIEXtor::getInterfaceType() {
   return pimpl_->getInterfaceType();
}

bool PCIEXtor::getDoneForNow() {
   return pimpl_->getDoneForNow();
}

void PCIEXtor::setDoneCallbackFn(PCIEXtor::PCIEDoneCBFn *doneCbFn, void* doneCbInfo) {
   pimpl_->setDoneCallbackFn(doneCbFn, doneCbInfo);
}

PCIEXtor::PCIEDoneCBFn* PCIEXtor::getDoneCallbackFn() {
   return pimpl_->getDoneCallbackFn();
}

void* PCIEXtor::getDoneCallbackData() {
   return pimpl_->getDoneCallbackData();
}

SInt32 PCIEXtor::addTransaction(PCIECmd *cmd) {
   return pimpl_->addTransaction(cmd);
}

bool PCIEXtor::setSignal(PCIEXtor::PCIESignalType sigType, UInt32 index, CarbonSimNet *net) {
   return pimpl_->setSignal(sigType, index, net);
}

bool PCIEXtor::setSignal(PCIEXtor::PCIESignalType sigType, CarbonSimNet *net) {
   return pimpl_->setSignal(sigType, net);
}

bool PCIEXtor::setSignal(CarbonSimClock *net) {
   return pimpl_->setSignal(net);
}

void PCIEXtor::setElecIdleActiveHigh() {
   pimpl_->setElecIdleActiveHigh();
}

void PCIEXtor::setElecIdleActiveLow() {
   pimpl_->setElecIdleActiveLow();
}

bool PCIEXtor::isConfigured() {
   return pimpl_->isConfigured();
}

void PCIEXtor::printConfiguration() {
   pimpl_->printConfiguration();
}

bool PCIEXtor::isDone() {
   return pimpl_->isDoneForNow();
}

bool PCIEXtor::isLinkActive() {
   return pimpl_->isLinkActive();
}

void PCIEXtor::printLinkState() {
   pimpl_->printLinkState();
}

void PCIEXtor::setTimescale(CarbonTimescale timescale) {
   pimpl_->setTimescale(timescale);
}

void PCIEXtor::step(CarbonTime tick) {
   pimpl_->step(tick);
}

void PCIEXtor::idle(UInt64 cycles) {
   pimpl_->idle(cycles);
}

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
