// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __PCIEXPRESSTRANSACTION_H_
#define __PCIEXPRESSTRANSACTION_H_


#include <string.h>
#include "carbonsim/CarbonSimTransaction.h"
#include "PCIExpressCommon.h"

#define PCIE_DLLP_CRC_KEY  0x100B
#define PCIE_DLLP_CRC_SEED 0xFFFF
#define PCIE_DLLP_LCRC_KEY  0x04C11DB7
#define PCIE_DLLP_LCRC_SEED 0xFFFFFFFF


// PCIExpressTransaction class
class PCIExpressTransaction {
 public:
   // Public Constructor by string
   PCIExpressTransaction(PCIExpressTransactor *xtor, const char *cmdstr);
   // Public Constructor by PCIECmd
   PCIExpressTransaction(PCIExpressTransactor *xtor, PCIECmd *cmd);
   // Destructor.
   ~PCIExpressTransaction(void);

   typedef void (CompletionFunc)(UInt32 address, UInt32 ** data, void * user_arg);

   typedef enum {   // Flow control credit type used
      Posted,
      NonPosted,
      Completion,
      None
   } FlowControlType;

   // Get a pointer to the PCIECmd object which initialized this (if any)
   PCIECmd* getPCIECmd(void) { return mPcieCmd; }

   // Execute one step of the transaction.
   void step(PCIExpressTransactor * transactor, CarbonTime tick);
   // Is the transaction going across the bus?
   bool isStarted(void) { return (mBytesSent != 0);}
   // Is the transaction done going across the bus?
   bool isDone(void) { return mDone; }
   // Does this need to be replayed once finished being sent (only set if its in-progress on the bus)
   bool needsReplay(void) { return doReplay; }
   // Can this transaction be deleted?
   bool canDelete(void) { return ((not mIsValid) || ((mDone) && (not expectAck) && (not expectCpl))); }
   // Take this action when the transaction is completed.
   void onCompletion(CompletionFunc * func, void * user_arg);
   // Take any actions needed at the end of the transaction.
   void doCompletion(void);
   // Tell xtor what the transmitter state should be during this transaction
   pcie_state_enum_t getRunningOutputState(void) { return runOutputState; }
   // Retry this transaction (must have expectAck asserted and received a Nak from receiver)
   void replayTransaction(void);
   // Does this transaction require an Ack to be completed?
   bool needsAck(void) { return expectAck; }
   // Clears the 'expectAck' flag because there was an appropriate Ack received
   void clearNeedsAck() { expectAck = false; }
   // Does this transaction need a Completion to be finished?
   bool needsCpl() { return expectCpl; }
   // Clears the 'expectCpl' flag because there was an appropriate Completion received
   void clearNeedsCpl() { expectCpl = false; }
   // Return the Transaction ID
   UInt32 getTxID() { return transID; }
   // Return the Transaction Sequence Number
   UInt32 getSeqNum() { return DLLP_SeqNum; }
   // Return the printString to identify this transaction
   char* getPrintString() { return printString; }
   // Return whether the transacation is deemed valid or not
   bool isValid(void) { return mIsValid; }
   // Return whether the transaction is special (internal) or not
   bool isInternal(void) { return mIsSpecial; }
   // Return whether the transaction is a completion or not
   bool isCompletion(void) { return mIsCpl; }
   // Return whether the transaction is a DLLP Ack
   bool isAck(void) { return mIsAck; }
   // Return whether the transaction is a DLLP Nak
   bool isNak(void) { return mIsNak; }
   // Return whether the transaction is a TLP transaction
   bool isTLPTransaction(void) { return mIsTLP; }
   // Return whether the transaction is a DLLP transaction
   bool isDLLPTransaction(void) { return (not mIsTLP); }
   // Return whether the transaction must start on lane zero
   bool mustStartLane0(void) { return mMustStartLane0; }

   // Return the number of packets that still need to be transmitted
   UInt32 getNumPacketsRemaining(void) { return (mBytesTotal - mBytesSent); }
   // Return X packets on the UInt32* provided (must be big enough to hold data)
   void getTransmitPackets(UInt32 num, UInt32* datHolder, UInt32* symHolder);

   // Return type of Flow Control Credits consumed by this
   FlowControlType getFCType(void) { return fcType; }
   // Return number of Flow Control Header Units taken up
   UInt32 getFCHeaderNum(void) { return fcHeader; }
   // Return number of Flow Control Data Units taken up
   UInt32 getFCDataNum(void)   { return fcData; }
   // Has transactor consumed the Flow Control Credits for this yet?
   bool getFCCreditsConsumed(void) { return fcCreditsConsumed; }
   // Set that xtor has consumed the Flow Control Credits for this
   void setFCCreditsConsumed(void) { fcCreditsConsumed = true; }
   // Get the data length of the transaction (DWs)
   UInt32 getDataLength(void) { return mDataLength; }

 protected:
   // Save pointer to the transactor who owns this transaction
   PCIExpressTransactor *mXtor;
   // Save copy of the PCIECmd object used to initialize this object (if any)
   PCIECmd* mPcieCmd;

   bool mIsValid;     // is this transaction valid or was there a problem at constructor time?
   bool mIsSpecial;   // is this a special (internal) transaction [T] or a user transaction [F]?
   bool mIsCpl;       // is this a completion transaction?
   bool mIsAck;       // is this a DLLP Ack transaction?
   bool mIsNak;      // is this a DLLP Nak transaction?
   bool mIsTLP;       // is this a TLP transaction
   bool mMustStartLane0; // this must start on lane 0 (OSet tx)
   bool mDone;
   bool mNeedSeqNumAndLCRC;    // does the transaction still need a Sequence Number and LCRC to be established
   UInt32 mBytesSent;
   UInt32 mBytesTotal;
   // Physical Layer Packet Pointer
   UInt32 *phys_bytes_dat;
   bool *phys_bytes_sym;
   // What state should the xtor's transmitter be in when processing this transaction
   pcie_state_enum_t runOutputState;
   // Debug print string for command
   char printString[30];
   // Hold the DLLP Sequence Number for this transaction
   UInt32 DLLP_SeqNum;
   // Should we expect an Ack from the receiver before completion of the transaction?
   bool expectAck;
   // Should we expect a Completion from the receiver before completion of the transaction?
   bool expectCpl;
   UInt32 transID;  // save transaction ID (reqID + tag)
   // set if transaction should not be replayed, such as an ACK or NAK (any DLLP tx)
   bool canNotReplay;
   // set if the transaction should be replayed but is currently in progress (being put on the bus)
   bool doReplay;
   // number of retries started or to be started (incremented when replayTransaction() called)
   UInt32 numRetries;

   FlowControlType fcType;  // type of credits used
   UInt32 fcHeader;         // num of header credits used
   UInt32 fcData;           // num of data credits used
   bool fcCreditsConsumed;  // has xtor accounted for credits?
   UInt32 mDataLength;      // the data length, in DWs, needed for flow control

   CompletionFunc * mCompletionFunc;
   void * mCompletionFuncArg;  

 private:
   // Declare invalid constructors as private
   PCIExpressTransaction(void);
   PCIExpressTransaction(const PCIExpressTransaction&);
   PCIExpressTransaction& operator=(const PCIExpressTransaction&);

   void initializeTransaction(PCIExpressTransactor*, PCIECmd*);
   void initializeTransactionSpecial(PCIExpressTransactor*, const char*);
   PCIECmd::PCIECmdType getTlpTypeFromStr(char*);
   void setSeqNumAndCRC(PCIExpressTransactor*);
   void allocPhysBytes(UInt32);
};

#endif

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
