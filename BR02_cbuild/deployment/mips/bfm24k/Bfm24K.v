/*
 * Copyright (c) 1999-2001 MIPS Technologies, Inc.  All rights reserved.  
 * 
 */

`include "Bfm24KSigIntf.vh"

/*-------------------------------------------------------------------------*/
/* BFM Module Definition.                                                  */
/*-------------------------------------------------------------------------*/

module Bfm24K(
  OC_MCmd,
  OC_MTagID,
  OC_MAddr,
  OC_MReqInfo,
  OC_MAddrSpace,
  OC_MByteEn,
  OC_MBurstLength,
  OC_MBurstSeq,
  OC_MBurstPrecise,
  OC_MBurstSingleReq,
  OC_MDataValid,
  OC_MDataTagID,
  OC_MData,
  OC_MDataByteEn,
  OC_MDataLast,
  OC_MReset_n,
  OC_SResp,
  OC_STagID,
  OC_SData,
  OC_SRespLast,
  OC_SCmdAccept,
  OC_SDataAccept,
  SI_ClkIn,
  SI_OCPSync,
  SI_OCPReSyncReq,
  SI_Reset,
  SI_NMI,
  SI_ClkOut,
  SI_OCPRatioLock,
  SI_ERL,
  SI_EXL,
  SI_RP,
  SI_Sleep,
  SI_EICPresent,
  SI_EISS,
  SI_Int,
  SI_IPTI,
  SI_IPPCI,
  SI_IAck,
  SI_IPL,
  SI_SWInt,
  SI_TimerInt,
  SI_PCInt,
  SI_CPUNum,
  SI_Endian,
  SI_SimpleBE,
  SI_SBlock,
  L2_Sets,
  L2_LineSize,
  L2_Assoc,
  L2_PCWB,
  L2_PCAcc,
  L2_PCMiss,
  L2_PCMissCy,
  EJ_TRST_N,
  EJ_TCK,
  EJ_TMS,
  EJ_TDI,
  EJ_TDO,
  EJ_TDOzstate,
  EJ_DINTsup,
  EJ_DINT,
  EJ_DebugM,
  SI_Ibs,
  SI_Dbs,
  EJ_ManufID,
  EJ_PartNumber,
  EJ_Version,
  EJ_SRstE,
  EJ_PerRst,
  EJ_PrRst,
  gscanenable,
  gscanmode,
  gscanramaddr0,
  gscanramwr,
  gscanin,
  gscanout,
  SP_greset_pre,
  SP_gclk,
  SP_gfclk,
  SP_gscanenable,
  SP_parity_en,
  SP_present,
  SP_parity_present,
  SP_ram_busy,
  SP_busy_xx,
  SP_perfcnt_event,
  SP_wait_pd_xx,
  SP_sleep_req_xx,
  SP_tag_rdata_xx,
  SP_tag_msk_xx,
  SP_tag_rd_ag,
  SP_tag_wr_ag,
  SP_tag_sel_ag,
  SP_tag_wdata_ag,
  SP_data_rdata_xx,
  SP_data_rpar_xx,
  SP_data_wdata_ag,
  SP_data_wpar_ag,
  SP_datavld_nxt_xx,
  SP_data_addr_ag,
  SP_data_rd_ag,
  SP_dma_rd_ag,
  SP_data_wr_ag,
  SP_data_wren_ag,
  SP_core_reqpd_er,
  SP_req_id_ag,
  SP_data_id_xx,
  SP_dma_id_xx,
  SP_dma_addr_xx,
  SP_dma_wdata_xx,
  SP_dma_wren_xx,
  SP_dma_rd_xx,
  SP_dma_wr_xx,
  SP_dma_stallreq_xx,
  SP_resync_busy_xx,
  SP_mbsp_tosp_xx,
  SP_sp_tombsp_xx,
  ISP_greset_pre,
  ISP_gclk,
  ISP_gfclk,
  ISP_gscanenable,
  ISP_present,
  ISP_parity_present,
  ISP_ram_busy,
  ISP_busy_xx,
  ISP_perfcnt_event,
  ISP_tag_rdata_if,
  ISP_tag_msk_if,
  ISP_data_rdata_is,
  ISP_data_rpar_is,
  ISP_datavld_nxt_if,
  ISP_dma_addr_xx,
  ISP_dma_wdata_xx,
  ISP_dma_rdreq_xx,
  ISP_dma_wrreq_xx,
  ISP_dma_stallreq_xx,
  ISP_resync_busy_xx,
  ISP_parity_en,
  ISP_wait_pd_xx,
  ISP_sleep_req_xx,
  ISP_core_reqpd_xx,
  ISP_addr_ipf,
  ISP_tag_sel_ipf,
  ISP_rd_ipf,
  ISP_dma_rd_ipf,
  ISP_data_wr_ipf,
  ISP_data_wdata_ipf,
  ISP_data_wpar_ipf,
  ISP_tag_wr_ipf,
  ISP_tag_wdata_ipf,
  ISP_mbisp_toisp_xx,
  ISP_isp_tombisp_xx,
  MB_invoke,
  MB_ic_algorithm,
  MB_dc_algorithm,
  MB_sp_algorithm,
  MB_isp_algorithm,
  MB_tr_algorithm,
  MB_done,
  MB_dd_fail,
  MB_dt_fail,
  MB_dw_fail,
  MB_sp_fail,
  MB_isp_fail,
  MB_id_fail,
  MB_it_fail,
  MB_iw_fail,
  MB_tr_fail,
  MB_tombt,
  MB_frommbt,
  CP2_ir_0,
  CP2_irenable_0,
  CP2_inst32_0,
  CP2_endian_0,
  CP2_tds_0,
  CP2_torder_0,
  CP2_fordlim_0,
  CP2_tdata_0,
  CP2_fds_0,
  CP2_forder_0,
  CP2_tordlim_0,
  CP2_fdata_0,
  CP2_cccs_0,
  CP2_ccc_0,
  CP2_excs_0,
  CP2_exc_0,
  CP2_exccode_0,
  CP2_nulls_0,
  CP2_null_0,
  CP2_kills_0,
  CP2_kill_0,
  CP2_idle,
  CP2_as_0,
  CP2_abusy_0,
  CP2_ts_0,
  CP2_tbusy_0,
  CP2_fs_0,
  CP2_fbusy_0,
  CP2_present,
  CP2_kd_mode_0,
  CP2_tx32,
  CP2_reset,
  CP2_gclk,
  CP2_gfclk,
  CP2_gscanenable,
  CP2_perfcnt_event,
  UDI_present,
  UDI_ri_rf,
  UDI_use_rs_rf,
  UDI_use_rt_rf,
  UDI_wrreg_rf,
  UHL_use_hilo_rf,
  UHL_wr_hilo_rf,
  UDI_stallreq_ag,
  UHL_hi_wr_strobe_xx,
  UHL_lo_wr_strobe_xx,
  UHL_hi_wr_data_xx,
  UHL_lo_wr_data_xx,
  UDI_gpr_wr_strobe_ms,
  UDI_gpr_wr_data_ms,
  UDI_honor_cee,
  UDI_busy_xx,
  UDI_pend_gpr_wr_xx,
  UHL_pend_hilo_wr_xx,
  UHL_dsp_rd_hilo_rf,
  UHL_dsp_wr_state_rf,
  UHL_dsp_hilo_wr_xx,
  UDI_greset_pre,
  UDI_gclk,
  UDI_gfclk,
  UDI_gscanenable,
  UDI_ir_rf,
  UDI_kd_mode_rf,
  UDI_endianb_xx,
  UDI_nxt_opc_xx,
  UDI_run_ex,
  UDI_start_xx,
  UDI_rs_xx,
  UDI_rt_xx,
  UHL_hi_rd_data_xx,
  UHL_lo_rd_data_xx,
  UDI_age_xx,
  UDI_run_ms,
  UDI_gpr_wr_ack_ms,
  UDI_run_er,
  UDI_perfcnt_event,
  UDI_kill_er,
  TC_CRMax,
  TC_CRMin,
  TC_ProbeWidth,
  TC_DataBits,
  TC_Stall,
  TC_PibPresent,
  TC_ClockRatio,
  TC_Valid,
  TC_Data,
  TC_TrEnable,
  TC_Calibrate,
  TC_ProbeTrigIn,
  TC_ProbeTrigOut,
  TC_ChipTrigOut,
  TC_ChipTrigIn
);


/*-------------------------------------------------------------------------*/
/*  Inputs.                                                                */
/*-------------------------------------------------------------------------*/

    input [1:0]             OC_SResp;
    input [`TPZ_BIU_OCTAG_WIDTH-1:0] OC_STagID;
    input [63:0]            OC_SData;
    input                   OC_SRespLast;
    input                   OC_SCmdAccept;
    input                   OC_SDataAccept;
    input                   SI_ClkIn;
    input                   SI_OCPSync;
    input                   SI_OCPReSyncReq;
    input                   SI_Reset;
    input                   SI_NMI;
    input                   SI_EICPresent;
    input [3:0]             SI_EISS;
    input [5:0]             SI_Int;
    input [2:0]             SI_IPTI;
    input [2:0]             SI_IPPCI;
    input [9:0]             SI_CPUNum;
    input                   SI_Endian;
    input                   SI_SimpleBE;
    input                   SI_SBlock;
    input [3:0]             L2_Sets;
    input [3:0]             L2_LineSize;
    input [3:0]             L2_Assoc;
    input                   L2_PCWB;
    input                   L2_PCAcc;
    input                   L2_PCMiss;
    input                   L2_PCMissCy;
    input                   EJ_TRST_N;
    input                   EJ_TCK;
    input                   EJ_TMS;
    input                   EJ_TDI;
    input                   EJ_DINTsup;
    input                   EJ_DINT;
    input [10:0]            EJ_ManufID;
    input [15:0]            EJ_PartNumber;
    input [ 3:0]            EJ_Version;
    input                   gscanenable;
    input                   gscanmode;
    input                   gscanramaddr0;
    input                   gscanramwr;
    input [`TPZ_NUM_SCAN_CHAIN-1:0] gscanin;
    input                   SP_present;
    input                   SP_parity_present;
    input                   SP_ram_busy;
    input                   SP_busy_xx;
    input                   SP_perfcnt_event;
    input [31:11]           SP_tag_rdata_xx;
    input [31:12]           SP_tag_msk_xx;
    input [63:0]            SP_data_rdata_xx;
    input [7:0]             SP_data_rpar_xx;
    input                   SP_datavld_nxt_xx;
    input [2:0]             SP_data_id_xx;
    input [2:0]             SP_dma_id_xx;
    input [19:3]            SP_dma_addr_xx;
    input [63:0]            SP_dma_wdata_xx;
    input [7:0]             SP_dma_wren_xx;
    input                   SP_dma_rd_xx;
    input                   SP_dma_wr_xx;
    input                   SP_dma_stallreq_xx;
    input                   SP_resync_busy_xx;
    input [`TPZ_MBSP_TOMBSP_WIDTH-1:0] SP_sp_tombsp_xx;
    input                   ISP_present;
    input                   ISP_parity_present;
    input                   ISP_ram_busy;
    input                   ISP_busy_xx;
    input                   ISP_perfcnt_event;
    input [31:11]           ISP_tag_rdata_if;
    input [19:12]           ISP_tag_msk_if;
    input [69:0]            ISP_data_rdata_is;
    input [8:0]             ISP_data_rpar_is;
    input                   ISP_datavld_nxt_if;
    input [19:3]            ISP_dma_addr_xx;
    input [63:0]            ISP_dma_wdata_xx;
    input                   ISP_dma_rdreq_xx;
    input                   ISP_dma_wrreq_xx;
    input                   ISP_dma_stallreq_xx;
    input                   ISP_resync_busy_xx;
    input [`TPZ_MBISP_TOMBISP_WIDTH-1:0] ISP_isp_tombisp_xx;
    input                   MB_invoke;
    input [7:0]             MB_ic_algorithm;
    input [7:0]             MB_dc_algorithm;
    input [7:0]             MB_sp_algorithm;
    input [7:0]             MB_isp_algorithm;
    input [7:0]             MB_tr_algorithm;
    input [`TPZ_MB_TOMBT_WIDTH-1:0] MB_tombt;
    input                   CP2_fds_0;
    input [2:0]             CP2_forder_0;
    input [2:0]             CP2_tordlim_0;
    input [63:0]            CP2_fdata_0;
    input                   CP2_cccs_0;
    input                   CP2_ccc_0;
    input                   CP2_excs_0;
    input                   CP2_exc_0;
    input [4:0]             CP2_exccode_0;
    input                   CP2_idle;
    input                   CP2_abusy_0;
    input                   CP2_tbusy_0;
    input                   CP2_fbusy_0;
    input                   CP2_present;
    input                   CP2_tx32;
    input                   CP2_perfcnt_event;
    input                   UDI_present;
    input                   UDI_ri_rf;
    input                   UDI_use_rs_rf;
    input                   UDI_use_rt_rf;
    input [4:0]             UDI_wrreg_rf;
    input                   UHL_use_hilo_rf;
    input                   UHL_wr_hilo_rf;
    input                   UDI_stallreq_ag;
    input                   UHL_hi_wr_strobe_xx;
    input                   UHL_lo_wr_strobe_xx;
    input [31:0]            UHL_hi_wr_data_xx;
    input [31:0]            UHL_lo_wr_data_xx;
    input                   UDI_gpr_wr_strobe_ms;
    input [31:0]            UDI_gpr_wr_data_ms;
    input                   UDI_honor_cee;
    input                   UDI_busy_xx;
    input                   UDI_pend_gpr_wr_xx;
    input                   UHL_pend_hilo_wr_xx;
    input [1:0]             UHL_dsp_rd_hilo_rf;
    input                   UHL_dsp_wr_state_rf;
    input [1:0]             UHL_dsp_hilo_wr_xx;
    input                   UDI_perfcnt_event;
    input [2:0]             TC_CRMax;
    input [2:0]             TC_CRMin;
    input [1:0]             TC_ProbeWidth;
    input [2:0]             TC_DataBits;
    input                   TC_Stall;
    input                   TC_PibPresent;
    input                   TC_ProbeTrigIn;
    input                   TC_ChipTrigIn;

/*-------------------------------------------------------------------------*/
/*  Outputs.                                                               */
/*-------------------------------------------------------------------------*/

    output [2:0]            OC_MCmd;
    output [`TPZ_BIU_OCTAG_WIDTH-1:0] OC_MTagID;
    output [31:0]           OC_MAddr;
    output [3:0]            OC_MReqInfo;
    output [1:0]            OC_MAddrSpace;
    output [7:0]            OC_MByteEn;
    output [2:0]            OC_MBurstLength;
    output [2:0]            OC_MBurstSeq;
    output                  OC_MBurstPrecise;
    output                  OC_MBurstSingleReq;
    output                  OC_MDataValid;
    output [`TPZ_BIU_OCTAG_WIDTH-1:0] OC_MDataTagID;
    output [63:0]           OC_MData;
    output [7:0]            OC_MDataByteEn;
    output                  OC_MDataLast;
    output                  OC_MReset_n;
    output                  SI_ClkOut;
    output                  SI_OCPRatioLock;
    output                  SI_ERL;
    output                  SI_EXL;
    output                  SI_RP;
    output                  SI_Sleep;
    output                  SI_IAck;
    output [5:0]            SI_IPL;
    output [1:0]            SI_SWInt;
    output                  SI_TimerInt;
    output                  SI_PCInt;
    output                  EJ_TDO;
    output                  EJ_TDOzstate;
    output                  EJ_DebugM;
    output [3:0]            SI_Ibs;
    output [1:0]            SI_Dbs;
    output                  EJ_SRstE;
    output                  EJ_PerRst;
    output                  EJ_PrRst;
    output [`TPZ_NUM_SCAN_CHAIN-1:0] gscanout;
    output                  SP_greset_pre;
    output                  SP_gclk;
    output                  SP_gfclk;
    output                  SP_gscanenable;
    output                  SP_parity_en;
    output                  SP_wait_pd_xx;
    output                  SP_sleep_req_xx;
    output                  SP_tag_rd_ag;
    output                  SP_tag_wr_ag;
    output                  SP_tag_sel_ag;
    output [31:11]          SP_tag_wdata_ag;
    output [63:0]           SP_data_wdata_ag;
    output [7:0]            SP_data_wpar_ag;
    output [19:2]           SP_data_addr_ag;
    output                  SP_data_rd_ag;
    output                  SP_dma_rd_ag;
    output                  SP_data_wr_ag;
    output [7:0]            SP_data_wren_ag;
    output                  SP_core_reqpd_er;
    output [2:0]            SP_req_id_ag;
    output [`TPZ_MBSP_TOSP_WIDTH-1:0] SP_mbsp_tosp_xx;
    output                  ISP_greset_pre;
    output                  ISP_gclk;
    output                  ISP_gfclk;
    output                  ISP_gscanenable;
    output                  ISP_parity_en;
    output                  ISP_wait_pd_xx;
    output                  ISP_sleep_req_xx;
    output                  ISP_core_reqpd_xx;
    output [19:3]           ISP_addr_ipf;
    output                  ISP_tag_sel_ipf;
    output                  ISP_rd_ipf;
    output                  ISP_dma_rd_ipf;
    output                  ISP_data_wr_ipf;
    output [69:0]           ISP_data_wdata_ipf;
    output [8:0]            ISP_data_wpar_ipf;
    output                  ISP_tag_wr_ipf;
    output [31:11]          ISP_tag_wdata_ipf;
    output [`TPZ_MBISP_TOISP_WIDTH-1:0] ISP_mbisp_toisp_xx;
    output                  MB_done;
    output                  MB_dd_fail;
    output                  MB_dt_fail;
    output                  MB_dw_fail;
    output                  MB_sp_fail;
    output                  MB_isp_fail;
    output                  MB_id_fail;
    output                  MB_it_fail;
    output                  MB_iw_fail;
    output                  MB_tr_fail;
    output [`TPZ_MB_FROMMBT_WIDTH-1:0] MB_frommbt;
    output [31:0]           CP2_ir_0;
    output                  CP2_irenable_0;
    output                  CP2_inst32_0;
    output                  CP2_endian_0;
    output                  CP2_tds_0;
    output [2:0]            CP2_torder_0;
    output [2:0]            CP2_fordlim_0;
    output [63:0]           CP2_tdata_0;
    output                  CP2_nulls_0;
    output                  CP2_null_0;
    output                  CP2_kills_0;
    output [1:0]            CP2_kill_0;
    output                  CP2_as_0;
    output                  CP2_ts_0;
    output                  CP2_fs_0;
    output                  CP2_kd_mode_0;
    output                  CP2_reset;
    output                  CP2_gclk;
    output                  CP2_gfclk;
    output                  CP2_gscanenable;
    output                  UDI_greset_pre;
    output                  UDI_gclk;
    output                  UDI_gfclk;
    output                  UDI_gscanenable;
    output [31:0]           UDI_ir_rf;
    output                  UDI_kd_mode_rf;
    output                  UDI_endianb_xx;
    output [31:0]           UDI_nxt_opc_xx;
    output                  UDI_run_ex;
    output                  UDI_start_xx;
    output [31:0]           UDI_rs_xx;
    output [31:0]           UDI_rt_xx;
    output [31:0]           UHL_hi_rd_data_xx;
    output [31:0]           UHL_lo_rd_data_xx;
    output [1:0]            UDI_age_xx;
    output                  UDI_run_ms;
    output                  UDI_gpr_wr_ack_ms;
    output                  UDI_run_er;
    output                  UDI_kill_er;
    output [2:0]            TC_ClockRatio;
    output                  TC_Valid;
    output [63:0]           TC_Data;
    output                  TC_TrEnable;
    output                  TC_Calibrate;
    output                  TC_ProbeTrigOut;
    output                  TC_ChipTrigOut;


CarbonBfm24K Bfm24K_cds (
    .OC_MCmd                ( OC_MCmd                )
  , .OC_MTagID              ( OC_MTagID              )
  , .OC_MAddr               ( OC_MAddr               )
  , .OC_MReqInfo            ( OC_MReqInfo            )
  , .OC_MAddrSpace          ( OC_MAddrSpace          )
  , .OC_MByteEn             ( OC_MByteEn             )
  , .OC_MBurstLength        ( OC_MBurstLength        )
  , .OC_MBurstSeq           ( OC_MBurstSeq           )
  , .OC_MBurstPrecise       ( OC_MBurstPrecise       )
  , .OC_MBurstSingleReq     ( OC_MBurstSingleReq     )
  , .OC_MDataValid          ( OC_MDataValid          )
  , .OC_MDataTagID          ( OC_MDataTagID          )
  , .OC_MData               ( OC_MData               )
  , .OC_MDataByteEn         ( OC_MDataByteEn         )
  , .OC_MDataLast           ( OC_MDataLast           )
  , .OC_MReset_n            ( OC_MReset_n            )
  , .OC_SResp               ( OC_SResp               )
  , .OC_STagID              ( OC_STagID              )
  , .OC_SData               ( OC_SData               )
  , .OC_SRespLast           ( OC_SRespLast           )
  , .OC_SCmdAccept          ( OC_SCmdAccept          )
  , .OC_SDataAccept         ( OC_SDataAccept         )
  , .SI_ClkIn               ( SI_ClkIn               )
  , .SI_OCPSync             ( SI_OCPSync             )
  , .SI_OCPReSyncReq        ( SI_OCPReSyncReq        )
  , .SI_Reset               ( SI_Reset               )
  , .SI_NMI                 ( SI_NMI                 )
  , .SI_ClkOut              ( SI_ClkOut              )
  , .SI_OCPRatioLock        ( SI_OCPRatioLock        )
  , .SI_ERL                 ( SI_ERL                 )
  , .SI_EXL                 ( SI_EXL                 )
  , .SI_RP                  ( SI_RP                  )
  , .SI_Sleep               ( SI_Sleep               )
  , .SI_EICPresent          ( SI_EICPresent          )
  , .SI_EISS                ( SI_EISS                )
  , .SI_Int                 ( SI_Int                 )
  , .SI_IPTI                ( SI_IPTI                )
  , .SI_IPPCI               ( SI_IPPCI               )
  , .SI_IAck                ( SI_IAck                )
  , .SI_IPL                 ( SI_IPL                 )
  , .SI_SWInt               ( SI_SWInt               )
  , .SI_TimerInt            ( SI_TimerInt            )
  , .SI_PCInt               ( SI_PCInt               )
  , .SI_CPUNum              ( SI_CPUNum              )
  , .SI_Endian              ( SI_Endian              )
  , .SI_SimpleBE            ( SI_SimpleBE            )
  , .SI_SBlock              ( SI_SBlock              )
  , .L2_Sets                ( L2_Sets                )
  , .L2_LineSize            ( L2_LineSize            )
  , .L2_Assoc               ( L2_Assoc               )
  , .L2_PCWB                ( L2_PCWB                )
  , .L2_PCAcc               ( L2_PCAcc               )
  , .L2_PCMiss              ( L2_PCMiss              )
  , .L2_PCMissCy            ( L2_PCMissCy            )
  , .EJ_TRST_N              ( EJ_TRST_N              )
  , .EJ_TCK                 ( EJ_TCK                 )
  , .EJ_TMS                 ( EJ_TMS                 )
  , .EJ_TDI                 ( EJ_TDI                 )
  , .EJ_TDO                 ( EJ_TDO                 )
  , .EJ_TDOzstate           ( EJ_TDOzstate           )
  , .EJ_DINTsup             ( EJ_DINTsup             )
  , .EJ_DINT                ( EJ_DINT                )
  , .EJ_DebugM              ( EJ_DebugM              )
  , .SI_Ibs                 ( SI_Ibs                 )
  , .SI_Dbs                 ( SI_Dbs                 )
  , .EJ_ManufID             ( EJ_ManufID             )
  , .EJ_PartNumber          ( EJ_PartNumber          )
  , .EJ_Version             ( EJ_Version             )
  , .EJ_SRstE               ( EJ_SRstE               )
  , .EJ_PerRst              ( EJ_PerRst              )
  , .EJ_PrRst               ( EJ_PrRst               )
  , .gscanenable            ( gscanenable            )
  , .gscanmode              ( gscanmode              )
  , .gscanramaddr0          ( gscanramaddr0          )
  , .gscanramwr             ( gscanramwr             )
  , .gscanin                ( gscanin                )
  , .gscanout               ( gscanout               )
  , .SP_greset_pre          ( SP_greset_pre          )
  , .SP_gclk                ( SP_gclk                )
  , .SP_gfclk               ( SP_gfclk               )
  , .SP_gscanenable         ( SP_gscanenable         )
  , .SP_parity_en           ( SP_parity_en           )
  , .SP_present             ( SP_present             )
  , .SP_parity_present      ( SP_parity_present      )
  , .SP_ram_busy            ( SP_ram_busy            )
  , .SP_busy_xx             ( SP_busy_xx             )
  , .SP_perfcnt_event       ( SP_perfcnt_event       )
  , .SP_wait_pd_xx          ( SP_wait_pd_xx          )
  , .SP_sleep_req_xx        ( SP_sleep_req_xx        )
  , .SP_tag_rdata_xx        ( SP_tag_rdata_xx        )
  , .SP_tag_msk_xx          ( SP_tag_msk_xx          )
  , .SP_tag_rd_ag           ( SP_tag_rd_ag           )
  , .SP_tag_wr_ag           ( SP_tag_wr_ag           )
  , .SP_tag_sel_ag          ( SP_tag_sel_ag          )
  , .SP_tag_wdata_ag        ( SP_tag_wdata_ag        )
  , .SP_data_rdata_xx       ( SP_data_rdata_xx       )
  , .SP_data_rpar_xx        ( SP_data_rpar_xx        )
  , .SP_data_wdata_ag       ( SP_data_wdata_ag       )
  , .SP_data_wpar_ag        ( SP_data_wpar_ag        )
  , .SP_datavld_nxt_xx      ( SP_datavld_nxt_xx      )
  , .SP_data_addr_ag        ( SP_data_addr_ag        )
  , .SP_data_rd_ag          ( SP_data_rd_ag          )
  , .SP_dma_rd_ag           ( SP_dma_rd_ag           )
  , .SP_data_wr_ag          ( SP_data_wr_ag          )
  , .SP_data_wren_ag        ( SP_data_wren_ag        )
  , .SP_core_reqpd_er       ( SP_core_reqpd_er       )
  , .SP_req_id_ag           ( SP_req_id_ag           )
  , .SP_data_id_xx          ( SP_data_id_xx          )
  , .SP_dma_id_xx           ( SP_dma_id_xx           )
  , .SP_dma_addr_xx         ( SP_dma_addr_xx         )
  , .SP_dma_wdata_xx        ( SP_dma_wdata_xx        )
  , .SP_dma_wren_xx         ( SP_dma_wren_xx         )
  , .SP_dma_rd_xx           ( SP_dma_rd_xx           )
  , .SP_dma_wr_xx           ( SP_dma_wr_xx           )
  , .SP_dma_stallreq_xx     ( SP_dma_stallreq_xx     )
  , .SP_resync_busy_xx      ( SP_resync_busy_xx      )
  , .SP_mbsp_tosp_xx        ( SP_mbsp_tosp_xx        )
  , .SP_sp_tombsp_xx        ( SP_sp_tombsp_xx        )
  , .ISP_greset_pre         ( ISP_greset_pre         )
  , .ISP_gclk               ( ISP_gclk               )
  , .ISP_gfclk              ( ISP_gfclk              )
  , .ISP_gscanenable        ( ISP_gscanenable        )
  , .ISP_present            ( ISP_present            )
  , .ISP_parity_present     ( ISP_parity_present     )
  , .ISP_ram_busy           ( ISP_ram_busy           )
  , .ISP_busy_xx            ( ISP_busy_xx            )
  , .ISP_perfcnt_event      ( ISP_perfcnt_event      )
  , .ISP_tag_rdata_if       ( ISP_tag_rdata_if       )
  , .ISP_tag_msk_if         ( ISP_tag_msk_if         )
  , .ISP_data_rdata_is      ( ISP_data_rdata_is      )
  , .ISP_data_rpar_is       ( ISP_data_rpar_is       )
  , .ISP_datavld_nxt_if     ( ISP_datavld_nxt_if     )
  , .ISP_dma_addr_xx        ( ISP_dma_addr_xx        )
  , .ISP_dma_wdata_xx       ( ISP_dma_wdata_xx       )
  , .ISP_dma_rdreq_xx       ( ISP_dma_rdreq_xx       )
  , .ISP_dma_wrreq_xx       ( ISP_dma_wrreq_xx       )
  , .ISP_dma_stallreq_xx    ( ISP_dma_stallreq_xx    )
  , .ISP_resync_busy_xx     ( ISP_resync_busy_xx     )
  , .ISP_parity_en          ( ISP_parity_en          )
  , .ISP_wait_pd_xx         ( ISP_wait_pd_xx         )
  , .ISP_sleep_req_xx       ( ISP_sleep_req_xx       )
  , .ISP_core_reqpd_xx      ( ISP_core_reqpd_xx      )
  , .ISP_addr_ipf           ( ISP_addr_ipf           )
  , .ISP_tag_sel_ipf        ( ISP_tag_sel_ipf        )
  , .ISP_rd_ipf             ( ISP_rd_ipf             )
  , .ISP_dma_rd_ipf         ( ISP_dma_rd_ipf         )
  , .ISP_data_wr_ipf        ( ISP_data_wr_ipf        )
  , .ISP_data_wdata_ipf     ( ISP_data_wdata_ipf     )
  , .ISP_data_wpar_ipf      ( ISP_data_wpar_ipf      )
  , .ISP_tag_wr_ipf         ( ISP_tag_wr_ipf         )
  , .ISP_tag_wdata_ipf      ( ISP_tag_wdata_ipf      )
  , .ISP_mbisp_toisp_xx     ( ISP_mbisp_toisp_xx     )
  , .ISP_isp_tombisp_xx     ( ISP_isp_tombisp_xx     )
  , .MB_invoke              ( MB_invoke              )
  , .MB_ic_algorithm        ( MB_ic_algorithm        )
  , .MB_dc_algorithm        ( MB_dc_algorithm        )
  , .MB_sp_algorithm        ( MB_sp_algorithm        )
  , .MB_isp_algorithm       ( MB_isp_algorithm       )
  , .MB_tr_algorithm        ( MB_tr_algorithm        )
  , .MB_done                ( MB_done                )
  , .MB_dd_fail             ( MB_dd_fail             )
  , .MB_dt_fail             ( MB_dt_fail             )
  , .MB_dw_fail             ( MB_dw_fail             )
  , .MB_sp_fail             ( MB_sp_fail             )
  , .MB_isp_fail            ( MB_isp_fail            )
  , .MB_id_fail             ( MB_id_fail             )
  , .MB_it_fail             ( MB_it_fail             )
  , .MB_iw_fail             ( MB_iw_fail             )
  , .MB_tr_fail             ( MB_tr_fail             )
  , .MB_tombt               ( MB_tombt               )
  , .MB_frommbt             ( MB_frommbt             )
  , .CP2_ir_0               ( CP2_ir_0               )
  , .CP2_irenable_0         ( CP2_irenable_0         )
  , .CP2_inst32_0           ( CP2_inst32_0           )
  , .CP2_endian_0           ( CP2_endian_0           )
  , .CP2_tds_0              ( CP2_tds_0              )
  , .CP2_torder_0           ( CP2_torder_0           )
  , .CP2_fordlim_0          ( CP2_fordlim_0          )
  , .CP2_tdata_0            ( CP2_tdata_0            )
  , .CP2_fds_0              ( CP2_fds_0              )
  , .CP2_forder_0           ( CP2_forder_0           )
  , .CP2_tordlim_0          ( CP2_tordlim_0          )
  , .CP2_fdata_0            ( CP2_fdata_0            )
  , .CP2_cccs_0             ( CP2_cccs_0             )
  , .CP2_ccc_0              ( CP2_ccc_0              )
  , .CP2_excs_0             ( CP2_excs_0             )
  , .CP2_exc_0              ( CP2_exc_0              )
  , .CP2_exccode_0          ( CP2_exccode_0          )
  , .CP2_nulls_0            ( CP2_nulls_0            )
  , .CP2_null_0             ( CP2_null_0             )
  , .CP2_kills_0            ( CP2_kills_0            )
  , .CP2_kill_0             ( CP2_kill_0             )
  , .CP2_idle               ( CP2_idle               )
  , .CP2_as_0               ( CP2_as_0               )
  , .CP2_abusy_0            ( CP2_abusy_0            )
  , .CP2_ts_0               ( CP2_ts_0               )
  , .CP2_tbusy_0            ( CP2_tbusy_0            )
  , .CP2_fs_0               ( CP2_fs_0               )
  , .CP2_fbusy_0            ( CP2_fbusy_0            )
  , .CP2_present            ( CP2_present            )
  , .CP2_kd_mode_0          ( CP2_kd_mode_0          )
  , .CP2_tx32               ( CP2_tx32               )
  , .CP2_reset              ( CP2_reset              )
  , .CP2_gclk               ( CP2_gclk               )
  , .CP2_gfclk              ( CP2_gfclk              )
  , .CP2_gscanenable        ( CP2_gscanenable        )
  , .CP2_perfcnt_event      ( CP2_perfcnt_event      )
  , .UDI_present            ( UDI_present            )
  , .UDI_ri_rf              ( UDI_ri_rf              )
  , .UDI_use_rs_rf          ( UDI_use_rs_rf          )
  , .UDI_use_rt_rf          ( UDI_use_rt_rf          )
  , .UDI_wrreg_rf           ( UDI_wrreg_rf           )
  , .UHL_use_hilo_rf        ( UHL_use_hilo_rf        )
  , .UHL_wr_hilo_rf         ( UHL_wr_hilo_rf         )
  , .UDI_stallreq_ag        ( UDI_stallreq_ag        )
  , .UHL_hi_wr_strobe_xx    ( UHL_hi_wr_strobe_xx    )
  , .UHL_lo_wr_strobe_xx    ( UHL_lo_wr_strobe_xx    )
  , .UHL_hi_wr_data_xx      ( UHL_hi_wr_data_xx      )
  , .UHL_lo_wr_data_xx      ( UHL_lo_wr_data_xx      )
  , .UDI_gpr_wr_strobe_ms   ( UDI_gpr_wr_strobe_ms   )
  , .UDI_gpr_wr_data_ms     ( UDI_gpr_wr_data_ms     )
  , .UDI_honor_cee          ( UDI_honor_cee          )
  , .UDI_busy_xx            ( UDI_busy_xx            )
  , .UDI_pend_gpr_wr_xx     ( UDI_pend_gpr_wr_xx     )
  , .UHL_pend_hilo_wr_xx    ( UHL_pend_hilo_wr_xx    )
  , .UHL_dsp_rd_hilo_rf     ( UHL_dsp_rd_hilo_rf     )
  , .UHL_dsp_wr_state_rf    ( UHL_dsp_wr_state_rf    )
  , .UHL_dsp_hilo_wr_xx     ( UHL_dsp_hilo_wr_xx     )
  , .UDI_greset_pre         ( UDI_greset_pre         )
  , .UDI_gclk               ( UDI_gclk               )
  , .UDI_gfclk              ( UDI_gfclk              )
  , .UDI_gscanenable        ( UDI_gscanenable        )
  , .UDI_ir_rf              ( UDI_ir_rf              )
  , .UDI_kd_mode_rf         ( UDI_kd_mode_rf         )
  , .UDI_endianb_xx         ( UDI_endianb_xx         )
  , .UDI_nxt_opc_xx         ( UDI_nxt_opc_xx         )
  , .UDI_run_ex             ( UDI_run_ex             )
  , .UDI_start_xx           ( UDI_start_xx           )
  , .UDI_rs_xx              ( UDI_rs_xx              )
  , .UDI_rt_xx              ( UDI_rt_xx              )
  , .UHL_hi_rd_data_xx      ( UHL_hi_rd_data_xx      )
  , .UHL_lo_rd_data_xx      ( UHL_lo_rd_data_xx      )
  , .UDI_age_xx             ( UDI_age_xx             )
  , .UDI_run_ms             ( UDI_run_ms             )
  , .UDI_gpr_wr_ack_ms      ( UDI_gpr_wr_ack_ms      )
  , .UDI_run_er             ( UDI_run_er             )
  , .UDI_perfcnt_event      ( UDI_perfcnt_event      )
  , .UDI_kill_er            ( UDI_kill_er            )
  , .TC_CRMax               ( TC_CRMax               )
  , .TC_CRMin               ( TC_CRMin               )
  , .TC_ProbeWidth          ( TC_ProbeWidth          )
  , .TC_DataBits            ( TC_DataBits            )
  , .TC_Stall               ( TC_Stall               )
  , .TC_PibPresent          ( TC_PibPresent          )
  , .TC_ClockRatio          ( TC_ClockRatio          )
  , .TC_Valid               ( TC_Valid               )
  , .TC_Data                ( TC_Data                )
  , .TC_TrEnable            ( TC_TrEnable            )
  , .TC_Calibrate           ( TC_Calibrate           )
  , .TC_ProbeTrigIn         ( TC_ProbeTrigIn         )
  , .TC_ProbeTrigOut        ( TC_ProbeTrigOut        )
  , .TC_ChipTrigOut         ( TC_ChipTrigOut         )
  , .TC_ChipTrigIn          ( TC_ChipTrigIn          )
);

endmodule // Bfm24K


module CarbonBfm24K (
  OC_MCmd,
  OC_MTagID,
  OC_MAddr,
  OC_MReqInfo,
  OC_MAddrSpace,
  OC_MByteEn,
  OC_MBurstLength,
  OC_MBurstSeq,
  OC_MBurstPrecise,
  OC_MBurstSingleReq,
  OC_MDataValid,
  OC_MDataTagID,
  OC_MData,
  OC_MDataByteEn,
  OC_MDataLast,
  OC_MReset_n,
  OC_SResp,
  OC_STagID,
  OC_SData,
  OC_SRespLast,
  OC_SCmdAccept,
  OC_SDataAccept,
  SI_ClkIn,
  SI_OCPSync,
  SI_OCPReSyncReq,
  SI_Reset,
  SI_NMI,
  SI_ClkOut,
  SI_OCPRatioLock,
  SI_ERL,
  SI_EXL,
  SI_RP,
  SI_Sleep,
  SI_EICPresent,
  SI_EISS,
  SI_Int,
  SI_IPTI,
  SI_IPPCI,
  SI_IAck,
  SI_IPL,
  SI_SWInt,
  SI_TimerInt,
  SI_PCInt,
  SI_CPUNum,
  SI_Endian,
  SI_SimpleBE,
  SI_SBlock,
  L2_Sets,
  L2_LineSize,
  L2_Assoc,
  L2_PCWB,
  L2_PCAcc,
  L2_PCMiss,
  L2_PCMissCy,
  EJ_TRST_N,
  EJ_TCK,
  EJ_TMS,
  EJ_TDI,
  EJ_TDO,
  EJ_TDOzstate,
  EJ_DINTsup,
  EJ_DINT,
  EJ_DebugM,
  SI_Ibs,
  SI_Dbs,
  EJ_ManufID,
  EJ_PartNumber,
  EJ_Version,
  EJ_SRstE,
  EJ_PerRst,
  EJ_PrRst,
  gscanenable,
  gscanmode,
  gscanramaddr0,
  gscanramwr,
  gscanin,
  gscanout,
  SP_greset_pre,
  SP_gclk,
  SP_gfclk,
  SP_gscanenable,
  SP_parity_en,
  SP_present,
  SP_parity_present,
  SP_ram_busy,
  SP_busy_xx,
  SP_perfcnt_event,
  SP_wait_pd_xx,
  SP_sleep_req_xx,
  SP_tag_rdata_xx,
  SP_tag_msk_xx,
  SP_tag_rd_ag,
  SP_tag_wr_ag,
  SP_tag_sel_ag,
  SP_tag_wdata_ag,
  SP_data_rdata_xx,
  SP_data_rpar_xx,
  SP_data_wdata_ag,
  SP_data_wpar_ag,
  SP_datavld_nxt_xx,
  SP_data_addr_ag,
  SP_data_rd_ag,
  SP_dma_rd_ag,
  SP_data_wr_ag,
  SP_data_wren_ag,
  SP_core_reqpd_er,
  SP_req_id_ag,
  SP_data_id_xx,
  SP_dma_id_xx,
  SP_dma_addr_xx,
  SP_dma_wdata_xx,
  SP_dma_wren_xx,
  SP_dma_rd_xx,
  SP_dma_wr_xx,
  SP_dma_stallreq_xx,
  SP_resync_busy_xx,
  SP_mbsp_tosp_xx,
  SP_sp_tombsp_xx,
  ISP_greset_pre,
  ISP_gclk,
  ISP_gfclk,
  ISP_gscanenable,
  ISP_present,
  ISP_parity_present,
  ISP_ram_busy,
  ISP_busy_xx,
  ISP_perfcnt_event,
  ISP_tag_rdata_if,
  ISP_tag_msk_if,
  ISP_data_rdata_is,
  ISP_data_rpar_is,
  ISP_datavld_nxt_if,
  ISP_dma_addr_xx,
  ISP_dma_wdata_xx,
  ISP_dma_rdreq_xx,
  ISP_dma_wrreq_xx,
  ISP_dma_stallreq_xx,
  ISP_resync_busy_xx,
  ISP_parity_en,
  ISP_wait_pd_xx,
  ISP_sleep_req_xx,
  ISP_core_reqpd_xx,
  ISP_addr_ipf,
  ISP_tag_sel_ipf,
  ISP_rd_ipf,
  ISP_dma_rd_ipf,
  ISP_data_wr_ipf,
  ISP_data_wdata_ipf,
  ISP_data_wpar_ipf,
  ISP_tag_wr_ipf,
  ISP_tag_wdata_ipf,
  ISP_mbisp_toisp_xx,
  ISP_isp_tombisp_xx,
  MB_invoke,
  MB_ic_algorithm,
  MB_dc_algorithm,
  MB_sp_algorithm,
  MB_isp_algorithm,
  MB_tr_algorithm,
  MB_done,
  MB_dd_fail,
  MB_dt_fail,
  MB_dw_fail,
  MB_sp_fail,
  MB_isp_fail,
  MB_id_fail,
  MB_it_fail,
  MB_iw_fail,
  MB_tr_fail,
  MB_tombt,
  MB_frommbt,
  CP2_ir_0,
  CP2_irenable_0,
  CP2_inst32_0,
  CP2_endian_0,
  CP2_tds_0,
  CP2_torder_0,
  CP2_fordlim_0,
  CP2_tdata_0,
  CP2_fds_0,
  CP2_forder_0,
  CP2_tordlim_0,
  CP2_fdata_0,
  CP2_cccs_0,
  CP2_ccc_0,
  CP2_excs_0,
  CP2_exc_0,
  CP2_exccode_0,
  CP2_nulls_0,
  CP2_null_0,
  CP2_kills_0,
  CP2_kill_0,
  CP2_idle,
  CP2_as_0,
  CP2_abusy_0,
  CP2_ts_0,
  CP2_tbusy_0,
  CP2_fs_0,
  CP2_fbusy_0,
  CP2_present,
  CP2_kd_mode_0,
  CP2_tx32,
  CP2_reset,
  CP2_gclk,
  CP2_gfclk,
  CP2_gscanenable,
  CP2_perfcnt_event,
  UDI_present,
  UDI_ri_rf,
  UDI_use_rs_rf,
  UDI_use_rt_rf,
  UDI_wrreg_rf,
  UHL_use_hilo_rf,
  UHL_wr_hilo_rf,
  UDI_stallreq_ag,
  UHL_hi_wr_strobe_xx,
  UHL_lo_wr_strobe_xx,
  UHL_hi_wr_data_xx,
  UHL_lo_wr_data_xx,
  UDI_gpr_wr_strobe_ms,
  UDI_gpr_wr_data_ms,
  UDI_honor_cee,
  UDI_busy_xx,
  UDI_pend_gpr_wr_xx,
  UHL_pend_hilo_wr_xx,
  UHL_dsp_rd_hilo_rf,
  UHL_dsp_wr_state_rf,
  UHL_dsp_hilo_wr_xx,
  UDI_greset_pre,
  UDI_gclk,
  UDI_gfclk,
  UDI_gscanenable,
  UDI_ir_rf,
  UDI_kd_mode_rf,
  UDI_endianb_xx,
  UDI_nxt_opc_xx,
  UDI_run_ex,
  UDI_start_xx,
  UDI_rs_xx,
  UDI_rt_xx,
  UHL_hi_rd_data_xx,
  UHL_lo_rd_data_xx,
  UDI_age_xx,
  UDI_run_ms,
  UDI_gpr_wr_ack_ms,
  UDI_run_er,
  UDI_perfcnt_event,
  UDI_kill_er,
  TC_CRMax,
  TC_CRMin,
  TC_ProbeWidth,
  TC_DataBits,
  TC_Stall,
  TC_PibPresent,
  TC_ClockRatio,
  TC_Valid,
  TC_Data,
  TC_TrEnable,
  TC_Calibrate,
  TC_ProbeTrigIn,
  TC_ProbeTrigOut,
  TC_ChipTrigOut,
  TC_ChipTrigIn
);

/*-------------------------------------------------------------------------*/
/*  Inputs.                                                                */
/*-------------------------------------------------------------------------*/

    input [1:0]             OC_SResp;
    input [`TPZ_BIU_OCTAG_WIDTH-1:0] OC_STagID;
    input [63:0]            OC_SData;
    input                   OC_SRespLast;
    input                   OC_SCmdAccept;
    input                   OC_SDataAccept;
    input                   SI_ClkIn;
    input                   SI_OCPSync;
    input                   SI_OCPReSyncReq;
    input                   SI_Reset;
    input                   SI_NMI;
    input                   SI_EICPresent;
    input [3:0]             SI_EISS;
    input [5:0]             SI_Int;
    input [2:0]             SI_IPTI;
    input [2:0]             SI_IPPCI;
    input [9:0]             SI_CPUNum;
    input                   SI_Endian;
    input                   SI_SimpleBE;
    input                   SI_SBlock;
    input [3:0]             L2_Sets;
    input [3:0]             L2_LineSize;
    input [3:0]             L2_Assoc;
    input                   L2_PCWB;
    input                   L2_PCAcc;
    input                   L2_PCMiss;
    input                   L2_PCMissCy;
    input                   EJ_TRST_N;
    input                   EJ_TCK;
    input                   EJ_TMS;
    input                   EJ_TDI;
    input                   EJ_DINTsup;
    input                   EJ_DINT;
    input [10:0]            EJ_ManufID;
    input [15:0]            EJ_PartNumber;
    input [ 3:0]            EJ_Version;
    input                   gscanenable;
    input                   gscanmode;
    input                   gscanramaddr0;
    input                   gscanramwr;
    input [`TPZ_NUM_SCAN_CHAIN-1:0] gscanin;
    input                   SP_present;
    input                   SP_parity_present;
    input                   SP_ram_busy;
    input                   SP_busy_xx;
    input                   SP_perfcnt_event;
    input [31:11]           SP_tag_rdata_xx;
    input [31:12]           SP_tag_msk_xx;
    input [63:0]            SP_data_rdata_xx;
    input [7:0]             SP_data_rpar_xx;
    input                   SP_datavld_nxt_xx;
    input [2:0]             SP_data_id_xx;
    input [2:0]             SP_dma_id_xx;
    input [19:3]            SP_dma_addr_xx;
    input [63:0]            SP_dma_wdata_xx;
    input [7:0]             SP_dma_wren_xx;
    input                   SP_dma_rd_xx;
    input                   SP_dma_wr_xx;
    input                   SP_dma_stallreq_xx;
    input                   SP_resync_busy_xx;
    input [`TPZ_MBSP_TOMBSP_WIDTH-1:0] SP_sp_tombsp_xx;
    input                   ISP_present;
    input                   ISP_parity_present;
    input                   ISP_ram_busy;
    input                   ISP_busy_xx;
    input                   ISP_perfcnt_event;
    input [31:11]           ISP_tag_rdata_if;
    input [19:12]           ISP_tag_msk_if;
    input [69:0]            ISP_data_rdata_is;
    input [8:0]             ISP_data_rpar_is;
    input                   ISP_datavld_nxt_if;
    input [19:3]            ISP_dma_addr_xx;
    input [63:0]            ISP_dma_wdata_xx;
    input                   ISP_dma_rdreq_xx;
    input                   ISP_dma_wrreq_xx;
    input                   ISP_dma_stallreq_xx;
    input                   ISP_resync_busy_xx;
    input [`TPZ_MBISP_TOMBISP_WIDTH-1:0] ISP_isp_tombisp_xx;
    input                   MB_invoke;
    input [7:0]             MB_ic_algorithm;
    input [7:0]             MB_dc_algorithm;
    input [7:0]             MB_sp_algorithm;
    input [7:0]             MB_isp_algorithm;
    input [7:0]             MB_tr_algorithm;
    input [`TPZ_MB_TOMBT_WIDTH-1:0] MB_tombt;
    input                   CP2_fds_0;
    input [2:0]             CP2_forder_0;
    input [2:0]             CP2_tordlim_0;
    input [63:0]            CP2_fdata_0;
    input                   CP2_cccs_0;
    input                   CP2_ccc_0;
    input                   CP2_excs_0;
    input                   CP2_exc_0;
    input [4:0]             CP2_exccode_0;
    input                   CP2_idle;
    input                   CP2_abusy_0;
    input                   CP2_tbusy_0;
    input                   CP2_fbusy_0;
    input                   CP2_present;
    input                   CP2_tx32;
    input                   CP2_perfcnt_event;
    input                   UDI_present;
    input                   UDI_ri_rf;
    input                   UDI_use_rs_rf;
    input                   UDI_use_rt_rf;
    input [4:0]             UDI_wrreg_rf;
    input                   UHL_use_hilo_rf;
    input                   UHL_wr_hilo_rf;
    input                   UDI_stallreq_ag;
    input                   UHL_hi_wr_strobe_xx;
    input                   UHL_lo_wr_strobe_xx;
    input [31:0]            UHL_hi_wr_data_xx;
    input [31:0]            UHL_lo_wr_data_xx;
    input                   UDI_gpr_wr_strobe_ms;
    input [31:0]            UDI_gpr_wr_data_ms;
    input                   UDI_honor_cee;
    input                   UDI_busy_xx;
    input                   UDI_pend_gpr_wr_xx;
    input                   UHL_pend_hilo_wr_xx;
    input [1:0]             UHL_dsp_rd_hilo_rf;
    input                   UHL_dsp_wr_state_rf;
    input [1:0]             UHL_dsp_hilo_wr_xx;
    input                   UDI_perfcnt_event;
    input [2:0]             TC_CRMax;
    input [2:0]             TC_CRMin;
    input [1:0]             TC_ProbeWidth;
    input [2:0]             TC_DataBits;
    input                   TC_Stall;
    input                   TC_PibPresent;
    input                   TC_ProbeTrigIn;
    input                   TC_ChipTrigIn;

/*-------------------------------------------------------------------------*/
/*  Outputs.                                                               */
/*-------------------------------------------------------------------------*/

    output [2:0]            OC_MCmd;
    output [`TPZ_BIU_OCTAG_WIDTH-1:0] OC_MTagID;
    output [31:0]           OC_MAddr;
    output [3:0]            OC_MReqInfo;
    output [1:0]            OC_MAddrSpace;
    output [7:0]            OC_MByteEn;
    output [2:0]            OC_MBurstLength;
    output [2:0]            OC_MBurstSeq;
    output                  OC_MBurstPrecise;
    output                  OC_MBurstSingleReq;
    output                  OC_MDataValid;
    output [`TPZ_BIU_OCTAG_WIDTH-1:0] OC_MDataTagID;
    output [63:0]           OC_MData;
    output [7:0]            OC_MDataByteEn;
    output                  OC_MDataLast;
    output                  OC_MReset_n;
    output                  SI_ClkOut;
    output                  SI_OCPRatioLock;
    output                  SI_ERL;
    output                  SI_EXL;
    output                  SI_RP;
    output                  SI_Sleep;
    output                  SI_IAck;
    output [5:0]            SI_IPL;
    output [1:0]            SI_SWInt;
    output                  SI_TimerInt;
    output                  SI_PCInt;
    output                  EJ_TDO;
    output                  EJ_TDOzstate;
    output                  EJ_DebugM;
    output [3:0]            SI_Ibs;
    output [1:0]            SI_Dbs;
    output                  EJ_SRstE;
    output                  EJ_PerRst;
    output                  EJ_PrRst;
    output [`TPZ_NUM_SCAN_CHAIN-1:0] gscanout;
    output                  SP_greset_pre;
    output                  SP_gclk;
    output                  SP_gfclk;
    output                  SP_gscanenable;
    output                  SP_parity_en;
    output                  SP_wait_pd_xx;
    output                  SP_sleep_req_xx;
    output                  SP_tag_rd_ag;
    output                  SP_tag_wr_ag;
    output                  SP_tag_sel_ag;
    output [31:11]          SP_tag_wdata_ag;
    output [63:0]           SP_data_wdata_ag;
    output [7:0]            SP_data_wpar_ag;
    output [19:2]           SP_data_addr_ag;
    output                  SP_data_rd_ag;
    output                  SP_dma_rd_ag;
    output                  SP_data_wr_ag;
    output [7:0]            SP_data_wren_ag;
    output                  SP_core_reqpd_er;
    output [2:0]            SP_req_id_ag;
    output [`TPZ_MBSP_TOSP_WIDTH-1:0] SP_mbsp_tosp_xx;
    output                  ISP_greset_pre;
    output                  ISP_gclk;
    output                  ISP_gfclk;
    output                  ISP_gscanenable;
    output                  ISP_parity_en;
    output                  ISP_wait_pd_xx;
    output                  ISP_sleep_req_xx;
    output                  ISP_core_reqpd_xx;
    output [19:3]           ISP_addr_ipf;
    output                  ISP_tag_sel_ipf;
    output                  ISP_rd_ipf;
    output                  ISP_dma_rd_ipf;
    output                  ISP_data_wr_ipf;
    output [69:0]           ISP_data_wdata_ipf;
    output [8:0]            ISP_data_wpar_ipf;
    output                  ISP_tag_wr_ipf;
    output [31:11]          ISP_tag_wdata_ipf;
    output [`TPZ_MBISP_TOISP_WIDTH-1:0] ISP_mbisp_toisp_xx;
    output                  MB_done;
    output                  MB_dd_fail;
    output                  MB_dt_fail;
    output                  MB_dw_fail;
    output                  MB_sp_fail;
    output                  MB_isp_fail;
    output                  MB_id_fail;
    output                  MB_it_fail;
    output                  MB_iw_fail;
    output                  MB_tr_fail;
    output [`TPZ_MB_FROMMBT_WIDTH-1:0] MB_frommbt;
    output [31:0]           CP2_ir_0;
    output                  CP2_irenable_0;
    output                  CP2_inst32_0;
    output                  CP2_endian_0;
    output                  CP2_tds_0;
    output [2:0]            CP2_torder_0;
    output [2:0]            CP2_fordlim_0;
    output [63:0]           CP2_tdata_0;
    output                  CP2_nulls_0;
    output                  CP2_null_0;
    output                  CP2_kills_0;
    output [1:0]            CP2_kill_0;
    output                  CP2_as_0;
    output                  CP2_ts_0;
    output                  CP2_fs_0;
    output                  CP2_kd_mode_0;
    output                  CP2_reset;
    output                  CP2_gclk;
    output                  CP2_gfclk;
    output                  CP2_gscanenable;
    output                  UDI_greset_pre;
    output                  UDI_gclk;
    output                  UDI_gfclk;
    output                  UDI_gscanenable;
    output [31:0]           UDI_ir_rf;
    output                  UDI_kd_mode_rf;
    output                  UDI_endianb_xx;
    output [31:0]           UDI_nxt_opc_xx;
    output                  UDI_run_ex;
    output                  UDI_start_xx;
    output [31:0]           UDI_rs_xx;
    output [31:0]           UDI_rt_xx;
    output [31:0]           UHL_hi_rd_data_xx;
    output [31:0]           UHL_lo_rd_data_xx;
    output [1:0]            UDI_age_xx;
    output                  UDI_run_ms;
    output                  UDI_gpr_wr_ack_ms;
    output                  UDI_run_er;
    output                  UDI_kill_er;
    output [2:0]            TC_ClockRatio;
    output                  TC_Valid;
    output [63:0]           TC_Data;
    output                  TC_TrEnable;
    output                  TC_Calibrate;
    output                  TC_ProbeTrigOut;
    output                  TC_ChipTrigOut;

endmodule // CarbonBfm24K

