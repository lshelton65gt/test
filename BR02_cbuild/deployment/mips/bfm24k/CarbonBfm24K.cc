/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include <iostream>
#include <iomanip>
#include <cassert>

using namespace std;

#include "carbon/carbon_shelltypes.h"

//#include <inttypes.h>
//#include "MIPS_Types.h"
//#include <MIPS_Register.h>

// Begin - HACK
# define __PRI64_PREFIX "ll"
# define __PRIPTR_PREFIX
# define PRId16  "d"
# define PRId32  "d"
# define PRId64  __PRI64_PREFIX "d"
# define PRIx16  "x"
# define PRIx32  "x"
# define PRIx64  __PRI64_PREFIX "x"
# define UINT64_C(c) (c##ull)
// End - HACK

#include "MdiCoSimServer.h"
#include "MIPS_dlfcn.h"
#include <MIPS_Bfm24K.h>

class CarbonBfm24K
{
public:
    CarbonBfm24K();
   ~CarbonBfm24K();

   void GetSignal(CarbonUInt32 id,       CarbonUInt32* val);
   void SetSignal(CarbonUInt32 id, const CarbonUInt32* val);

   MIPS::Bfm24K   * getMipsBfm()   { return mMipsBfm; }

// CarbonObjectID * getCarbonObj() { return mCarbonObj; }

   void setCarbonObj(CarbonObjectID * obj) { mCarbonObj= obj; }

   void clock(bool edge);

protected:
   CarbonObjectID * mCarbonObj;
   MIPS::Bfm24K   * mMipsBfm;
   MIPS::REG32      mReg32;
   MIPS::REG64      mReg64;
   MIPS::REG128     mReg128;
   bool             mEdge; // current event is posedge or negedge ?
   bool             mInit; // initialize BFM?

   BfmTxID mAddrTrans;
   BfmTxID mRDataTrans;
   BfmTxID mWDataTrans;
   BfmTxID mIsTrans;
   BfmTxID mDsTrans;

   CarbonUInt32 ** mSigValue; // 32, 64 or 128 bits

   static const int
     mFirstOutput= Bfm24K_BFM_Cycle,
     mNumPins    = Bfm24K_LASTSIG;
};

extern MIPS::Bfm24K * gMipsBfm; // global pointer 

typedef enum CDSCarbonBfm24KContext{
  eCDSCarbonBfm24KFallSI_ClkIn,
  eCDSCarbonBfm24KRiseSI_ClkIn,
} CDSCarbonBfm24KContext;

extern __C__ void* cds_CarbonBfm24K_create(
    int numParams, CModelParam* cmodelParams, const char* inst);

extern __C__ void cds_CarbonBfm24K_misc(
    void* hndl, CarbonCModelReason reason, void* cmodelData);

extern __C__ void cds_CarbonBfm24K_destroy(void* hndl);

extern __C__ void cds_CarbonBfm24K_run(
    void* hndl, CDSCarbonBfm24KContext context
  ,       CarbonUInt32* OC_MCmd                // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MTagID              // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MAddr               // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MReqInfo            // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MAddrSpace          // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MByteEn             // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MBurstLength        // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MBurstSeq           // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MBurstPrecise       // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MBurstSingleReq     // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MDataValid          // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MDataTagID          // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MData               // Output, size = 2 word(s)
  ,       CarbonUInt32* OC_MDataByteEn         // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MDataLast           // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MReset_n            // Output, size = 1 word(s)
  , const CarbonUInt32* OC_SResp               //  Input, size = 1 word(s)
  , const CarbonUInt32* OC_STagID              //  Input, size = 1 word(s)
  , const CarbonUInt32* OC_SData               //  Input, size = 2 word(s)
  , const CarbonUInt32* OC_SRespLast           //  Input, size = 1 word(s)
  , const CarbonUInt32* OC_SCmdAccept          //  Input, size = 1 word(s)
  , const CarbonUInt32* OC_SDataAccept         //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_ClkIn               //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_OCPSync             //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_OCPReSyncReq        //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_Reset               //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_NMI                 //  Input, size = 1 word(s)
  ,       CarbonUInt32* SI_ClkOut              // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_OCPRatioLock        // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_ERL                 // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_EXL                 // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_RP                  // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_Sleep               // Output, size = 1 word(s)
  , const CarbonUInt32* SI_EICPresent          //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_EISS                //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_Int                 //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_IPTI                //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_IPPCI               //  Input, size = 1 word(s)
  ,       CarbonUInt32* SI_IAck                // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_IPL                 // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_SWInt               // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_TimerInt            // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_PCInt               // Output, size = 1 word(s)
  , const CarbonUInt32* SI_CPUNum              //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_Endian              //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_SimpleBE            //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_SBlock              //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_Sets                //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_LineSize            //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_Assoc               //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_PCWB                //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_PCAcc               //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_PCMiss              //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_PCMissCy            //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_TRST_N              //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_TCK                 //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_TMS                 //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_TDI                 //  Input, size = 1 word(s)
  ,       CarbonUInt32* EJ_TDO                 // Output, size = 1 word(s)
  ,       CarbonUInt32* EJ_TDOzstate           // Output, size = 1 word(s)
  , const CarbonUInt32* EJ_DINTsup             //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_DINT                //  Input, size = 1 word(s)
  ,       CarbonUInt32* EJ_DebugM              // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_Ibs                 // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_Dbs                 // Output, size = 1 word(s)
  , const CarbonUInt32* EJ_ManufID             //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_PartNumber          //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_Version             //  Input, size = 1 word(s)
  ,       CarbonUInt32* EJ_SRstE               // Output, size = 1 word(s)
  ,       CarbonUInt32* EJ_PerRst              // Output, size = 1 word(s)
  ,       CarbonUInt32* EJ_PrRst               // Output, size = 1 word(s)
  , const CarbonUInt32* gscanenable            //  Input, size = 1 word(s)
  , const CarbonUInt32* gscanmode              //  Input, size = 1 word(s)
  , const CarbonUInt32* gscanramaddr0          //  Input, size = 1 word(s)
  , const CarbonUInt32* gscanramwr             //  Input, size = 1 word(s)
  , const CarbonUInt32* gscanin                //  Input, size = 1 word(s)
  ,       CarbonUInt32* gscanout               // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_greset_pre          // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_gclk                // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_gfclk               // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_gscanenable         // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_parity_en           // Output, size = 1 word(s)
  , const CarbonUInt32* SP_present             //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_parity_present      //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_ram_busy            //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_busy_xx             //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_perfcnt_event       //  Input, size = 1 word(s)
  ,       CarbonUInt32* SP_wait_pd_xx          // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_sleep_req_xx        // Output, size = 1 word(s)
  , const CarbonUInt32* SP_tag_rdata_xx        //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_tag_msk_xx          //  Input, size = 1 word(s)
  ,       CarbonUInt32* SP_tag_rd_ag           // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_tag_wr_ag           // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_tag_sel_ag          // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_tag_wdata_ag        // Output, size = 1 word(s)
  , const CarbonUInt32* SP_data_rdata_xx       //  Input, size = 2 word(s)
  , const CarbonUInt32* SP_data_rpar_xx        //  Input, size = 1 word(s)
  ,       CarbonUInt32* SP_data_wdata_ag       // Output, size = 2 word(s)
  ,       CarbonUInt32* SP_data_wpar_ag        // Output, size = 1 word(s)
  , const CarbonUInt32* SP_datavld_nxt_xx      //  Input, size = 1 word(s)
  ,       CarbonUInt32* SP_data_addr_ag        // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_data_rd_ag          // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_dma_rd_ag           // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_data_wr_ag          // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_data_wren_ag        // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_core_reqpd_er       // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_req_id_ag           // Output, size = 1 word(s)
  , const CarbonUInt32* SP_data_id_xx          //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_id_xx           //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_addr_xx         //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_wdata_xx        //  Input, size = 2 word(s)
  , const CarbonUInt32* SP_dma_wren_xx         //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_rd_xx           //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_wr_xx           //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_stallreq_xx     //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_resync_busy_xx      //  Input, size = 1 word(s)
  ,       CarbonUInt32* SP_mbsp_tosp_xx        // Output, size = 1 word(s)
  , const CarbonUInt32* SP_sp_tombsp_xx        //  Input, size = 1 word(s)
  ,       CarbonUInt32* ISP_greset_pre         // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_gclk               // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_gfclk              // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_gscanenable        // Output, size = 1 word(s)
  , const CarbonUInt32* ISP_present            //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_parity_present     //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_ram_busy           //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_busy_xx            //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_perfcnt_event      //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_tag_rdata_if       //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_tag_msk_if         //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_data_rdata_is      //  Input, size = 3 word(s)
  , const CarbonUInt32* ISP_data_rpar_is       //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_datavld_nxt_if     //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_dma_addr_xx        //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_dma_wdata_xx       //  Input, size = 2 word(s)
  , const CarbonUInt32* ISP_dma_rdreq_xx       //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_dma_wrreq_xx       //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_dma_stallreq_xx    //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_resync_busy_xx     //  Input, size = 1 word(s)
  ,       CarbonUInt32* ISP_parity_en          // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_wait_pd_xx         // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_sleep_req_xx       // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_core_reqpd_xx      // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_addr_ipf           // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_tag_sel_ipf        // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_rd_ipf             // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_dma_rd_ipf         // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_data_wr_ipf        // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_data_wdata_ipf     // Output, size = 3 word(s)
  ,       CarbonUInt32* ISP_data_wpar_ipf      // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_tag_wr_ipf         // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_tag_wdata_ipf      // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_mbisp_toisp_xx     // Output, size = 1 word(s)
  , const CarbonUInt32* ISP_isp_tombisp_xx     //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_invoke              //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_ic_algorithm        //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_dc_algorithm        //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_sp_algorithm        //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_isp_algorithm       //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_tr_algorithm        //  Input, size = 1 word(s)
  ,       CarbonUInt32* MB_done                // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_dd_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_dt_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_dw_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_sp_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_isp_fail            // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_id_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_it_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_iw_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_tr_fail             // Output, size = 1 word(s)
  , const CarbonUInt32* MB_tombt               //  Input, size = 1 word(s)
  ,       CarbonUInt32* MB_frommbt             // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_ir_0               // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_irenable_0         // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_inst32_0           // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_endian_0           // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_tds_0              // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_torder_0           // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_fordlim_0          // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_tdata_0            // Output, size = 2 word(s)
  , const CarbonUInt32* CP2_fds_0              //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_forder_0           //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_tordlim_0          //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_fdata_0            //  Input, size = 2 word(s)
  , const CarbonUInt32* CP2_cccs_0             //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_ccc_0              //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_excs_0             //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_exc_0              //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_exccode_0          //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_nulls_0            // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_null_0             // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_kills_0            // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_kill_0             // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_idle               //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_as_0               // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_abusy_0            //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_ts_0               // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_tbusy_0            //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_fs_0               // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_fbusy_0            //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_present            //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_kd_mode_0          // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_tx32               //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_reset              // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_gclk               // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_gfclk              // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_gscanenable        // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_perfcnt_event      //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_present            //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_ri_rf              //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_use_rs_rf          //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_use_rt_rf          //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_wrreg_rf           //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_use_hilo_rf        //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_wr_hilo_rf         //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_stallreq_ag        //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_hi_wr_strobe_xx    //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_lo_wr_strobe_xx    //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_hi_wr_data_xx      //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_lo_wr_data_xx      //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_gpr_wr_strobe_ms   //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_gpr_wr_data_ms     //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_honor_cee          //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_busy_xx            //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_pend_gpr_wr_xx     //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_pend_hilo_wr_xx    //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_dsp_rd_hilo_rf     //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_dsp_wr_state_rf    //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_dsp_hilo_wr_xx     //  Input, size = 1 word(s)
  ,       CarbonUInt32* UDI_greset_pre         // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_gclk               // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_gfclk              // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_gscanenable        // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_ir_rf              // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_kd_mode_rf         // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_endianb_xx         // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_nxt_opc_xx         // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_run_ex             // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_start_xx           // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_rs_xx              // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_rt_xx              // Output, size = 1 word(s)
  ,       CarbonUInt32* UHL_hi_rd_data_xx      // Output, size = 1 word(s)
  ,       CarbonUInt32* UHL_lo_rd_data_xx      // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_age_xx             // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_run_ms             // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_gpr_wr_ack_ms      // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_run_er             // Output, size = 1 word(s)
  , const CarbonUInt32* UDI_perfcnt_event      //  Input, size = 1 word(s)
  ,       CarbonUInt32* UDI_kill_er            // Output, size = 1 word(s)
  , const CarbonUInt32* TC_CRMax               //  Input, size = 1 word(s)
  , const CarbonUInt32* TC_CRMin               //  Input, size = 1 word(s)
  , const CarbonUInt32* TC_ProbeWidth          //  Input, size = 1 word(s)
  , const CarbonUInt32* TC_DataBits            //  Input, size = 1 word(s)
  , const CarbonUInt32* TC_Stall               //  Input, size = 1 word(s)
  , const CarbonUInt32* TC_PibPresent          //  Input, size = 1 word(s)
  ,       CarbonUInt32* TC_ClockRatio          // Output, size = 1 word(s)
  ,       CarbonUInt32* TC_Valid               // Output, size = 1 word(s)
  ,       CarbonUInt32* TC_Data                // Output, size = 2 word(s)
  ,       CarbonUInt32* TC_TrEnable            // Output, size = 1 word(s)
  ,       CarbonUInt32* TC_Calibrate           // Output, size = 1 word(s)
  , const CarbonUInt32* TC_ProbeTrigIn         //  Input, size = 1 word(s)
  ,       CarbonUInt32* TC_ProbeTrigOut        // Output, size = 1 word(s)
  ,       CarbonUInt32* TC_ChipTrigOut         // Output, size = 1 word(s)
  , const CarbonUInt32* TC_ChipTrigIn          //  Input, size = 1 word(s) ,       CarbonUInt32* IT_gscanenable         // Output, size = 1 word(s)
);

MdiServer *gpMdiServer=0; // global pointer from MdiServer.h

static MdiCoSimServer * gsMdiServer;

MIPS::Bfm24K * gMipsBfm=0; // global pointer 

static void create_iss( MIPS::Bfm24K *bfm, int id, MDIHandleT *core_id);

CarbonBfm24K::CarbonBfm24K()
: mCarbonObj(0), mInit(true)
{
  int mId=1;
  MDIHandleT mCoreId=mId;

  MIPSServices_Init();
  MIPSServices_SetConsoleOutput(MIPS_C_CONSOLECOMMON);

  ::gMipsBfm= mMipsBfm= new MIPS::Bfm24K(mId);

  create_iss( ::gMipsBfm, mId, &mCoreId );

  /**
   * Mask for displaying debug information.
   *
   * bit0 set:    Show main external bus values.
   * bit1 set:    Show Bfm transation queue.
   * bit2 set:    Show miscellaneous messages.
   * bit3 set:    Dump bus log.
   * bit4 set:    Show Coprocessor bus values.
   * bit5 set:    Show Scratch Pad RAM bus values.
   */

//::gBfmDebugMask= 0x02; //0x0f;

//printf("GetName= %s\n", mMipsBfm->GetName() );

//for (int i=0; i < mMipsBfm->GetLastSignal(); i++)
//   printf("%-22s = %3d\n", mMipsBfm->GetSignalName(i), i);

  mSigValue= new CarbonUInt32* [ mNumPins ];

  for(int n=0; n < Bfm24K_LASTSIG; n++)
    switch(n)
    {
    case Bfm24K_ISP_data_rdata_is:  // 70 bits --  input
    case Bfm24K_ISP_data_wdata_ipf: // 70 bits -- output
      mSigValue[n] = new CarbonUInt32 [ 4 ];
      break;

    case Bfm24K_OC_SData:          // [33,64] bits -- input
    case Bfm24K_SP_data_rdata_xx:
    case Bfm24K_SP_dma_wdata_xx:
    case Bfm24K_ISP_dma_wdata_xx:
    case Bfm24K_CP2_fdata_0:
    case Bfm24K_OC_MData:          // [33,64] bits -- output
    case Bfm24K_SP_data_wdata_ag:
    case Bfm24K_CP2_tdata_0:
    case Bfm24K_TC_Data:
      mSigValue[n] = new CarbonUInt32 [ 2 ];
      break;

    default:
      mSigValue[n] = new CarbonUInt32 [ 1 ];
      break;
    }

  for(int n=0; n < mFirstOutput; n++) // init all inputs = 0
    SetSignal(n, mSigValue[n]);

  mInit = false;
}

CarbonBfm24K::~CarbonBfm24K()
{
  cout << "~CarbonBfm24K(): deleting signal values\n";

  for(int n=0; n < Bfm24K_LASTSIG; n++)
    delete [] mSigValue[n];

  cout << "~CarbonBfm24K(): ::gsMdiServer->Close(); \n";
  ::gsMdiServer->Close();

  delete [] mSigValue;
  delete    mMipsBfm;
  delete    ::gsMdiServer;

  cout << "~CarbonBfm24K(): ::gMipsBfm=0; ::gsMdiServer=0; \n";
  ::gMipsBfm=0;
  ::gsMdiServer=0;
}

void CarbonBfm24K::clock(bool edge)
{
  mEdge= edge;

  if(mEdge) { // posedge
    // Function to Check the Current Transactions on the bus
    // task GetCurTrans;

    mAddrTrans   = mMipsBfm->GetCurAddrTransaction();
    mRDataTrans  = mMipsBfm->GetCurRDataTransaction();
    mWDataTrans  = mMipsBfm->GetCurWDataTransaction();
    mIsTrans     = mMipsBfm->GetCurISTransaction();
    mDsTrans     = mMipsBfm->GetCurDSTransaction();

    mMipsBfm->Clock();
  } else // negedge
    mMipsBfm->NegClock();
}

// Convert between MIPS::REG32/64/128 and CarbonUInt32 *

void CarbonBfm24K::GetSignal(CarbonUInt32 id, CarbonUInt32* val)
{
  MIPS_T_UNS64 ulo, uhi;

  switch(id)
  {
    case Bfm24K_ISP_data_wdata_ipf: // 70 bits
      assert( mMipsBfm->GetSignal(id, mReg128) );
      ulo= mReg128.lo.getValue();
      uhi= mReg128.hi.getValue();
      mSigValue[id][3]= uhi >> 32;
      mSigValue[id][2]= uhi;
      mSigValue[id][1]= ulo >> 32;
      mSigValue[id][0]= ulo;
      val[0]= mSigValue[id][0];
      val[1]= mSigValue[id][1];
      val[2]= mSigValue[id][2];
      break;

    case Bfm24K_OC_MData:           // [33,64] bits
    case Bfm24K_SP_data_wdata_ag:
    case Bfm24K_CP2_tdata_0:
    case Bfm24K_TC_Data:
      assert( mMipsBfm->GetSignal(id, mReg64) );
      ulo= mReg64.getValue();
      mSigValue[id][1]= ulo >> 32;
      mSigValue[id][0]= ulo;
      val[0]= mSigValue[id][0];
      val[1]= mSigValue[id][1];
      break;

    case Bfm24K_SI_ClkOut:
      val[0]= mEdge ; // this output clock tracks the input clock
      break;

    default:
      assert( mMipsBfm->GetSignal(id, mReg32) );
      val[0]= mSigValue[id][0]= mReg32.getValue();
      break;
  }
}


// Convert 2 Carbon UInt32 ==> MIPS UInt64

MIPS_T_UNS64 u64(CarbonUInt32 v0, CarbonUInt32 v1)
{
  MIPS_T_UNS64 val64;

  val64   = v0;
  val64 <<= 32;
  val64  |= v1;

  return val64;
}

// Set Signals on Change

void CarbonBfm24K::SetSignal(CarbonUInt32 id, const CarbonUInt32* val)
{
  switch(id)
  {
    case Bfm24K_ISP_data_rdata_is: // 70 bits
      if(   mSigValue[id][0] != val[0]
         || mSigValue[id][1] != val[1]
         || mSigValue[id][2] != val[2] || mInit ) {
            mSigValue[id][0]  = val[0];
            mSigValue[id][1]  = val[1];
            mSigValue[id][2]  = val[2];
            mSigValue[id][3]  =     0 ;
            mReg128.hi= u64(mSigValue[id][3], mSigValue[id][2]);
            mReg128.lo= u64(mSigValue[id][1], mSigValue[id][0]);
/**
            cout << setw(22) << mMipsBfm->GetSignalName(id)
                 << " = " << hex << mReg128.hi.getValue() 
                 << " , " << hex << mReg128.lo.getValue() 
                 << " : " << hex << mReg128.hi.getValid() 
                 << " , " << hex << mReg128.lo.getValid() << endl;
**/
            assert( mMipsBfm->SetSignal(id, mReg128) );
      }
      break;

    case Bfm24K_OC_SData:          // [33,64] bits
    case Bfm24K_SP_data_rdata_xx:
    case Bfm24K_SP_dma_wdata_xx:
    case Bfm24K_ISP_dma_wdata_xx:
    case Bfm24K_CP2_fdata_0:
      if(   mSigValue[id][0] != val[0]
         || mSigValue[id][1] != val[1] || mInit ) {
            mSigValue[id][0]  = val[0];
            mSigValue[id][1]  = val[1];
            mReg64= u64(mSigValue[id][1], mSigValue[id][0]);
/**
            cout << setw(22) << mMipsBfm->GetSignalName(id)
                 << " = " << hex << mReg64.getValue() 
                 << " : " << hex << mReg64.getValid() << endl;
**/
            assert( mMipsBfm->SetSignal(id, mReg64) );
      }
      break;

    default:
      if( mSigValue[id][0] != val[0] || mInit ) {
//        mSigValue[id][0]= val[0];
//        mReg32          = val[0];
          mReg32 = mSigValue[id][0]= val[0];
/**
          cout << setw(22) << mMipsBfm->GetSignalName(id)
               << " = " << hex << mReg32.getValue() 
               << " : " << hex << mReg32.getValid() << endl;
**/
          assert( mMipsBfm->SetSignal(id, mReg32) );
      }
      break;
  }
}


void* cds_CarbonBfm24K_create(
    int numParams, CModelParam* cmodelParams, const char* inst)
{
  return new CarbonBfm24K;
}

void cds_CarbonBfm24K_misc(
    void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   if (reason == eCarbonCModelID && hndl != NULL)  {    
    CarbonBfm24K *model= reinterpret_cast<CarbonBfm24K *>(hndl);
    model->setCarbonObj( reinterpret_cast<CarbonObjectID *>( cmodelData ));  
  }
}

void cds_CarbonBfm24K_destroy(void* hndl)
{
  delete(reinterpret_cast<CarbonBfm24K *>(hndl));
}

void cds_CarbonBfm24K_run(
    void* hndl, CDSCarbonBfm24KContext context
  ,       CarbonUInt32* OC_MCmd                // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MTagID              // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MAddr               // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MReqInfo            // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MAddrSpace          // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MByteEn             // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MBurstLength        // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MBurstSeq           // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MBurstPrecise       // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MBurstSingleReq     // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MDataValid          // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MDataTagID          // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MData               // Output, size = 2 word(s)
  ,       CarbonUInt32* OC_MDataByteEn         // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MDataLast           // Output, size = 1 word(s)
  ,       CarbonUInt32* OC_MReset_n            // Output, size = 1 word(s)
  , const CarbonUInt32* OC_SResp               //  Input, size = 1 word(s)
  , const CarbonUInt32* OC_STagID              //  Input, size = 1 word(s)
  , const CarbonUInt32* OC_SData               //  Input, size = 2 word(s)
  , const CarbonUInt32* OC_SRespLast           //  Input, size = 1 word(s)
  , const CarbonUInt32* OC_SCmdAccept          //  Input, size = 1 word(s)
  , const CarbonUInt32* OC_SDataAccept         //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_ClkIn               //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_OCPSync             //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_OCPReSyncReq        //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_Reset               //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_NMI                 //  Input, size = 1 word(s)
  ,       CarbonUInt32* SI_ClkOut              // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_OCPRatioLock        // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_ERL                 // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_EXL                 // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_RP                  // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_Sleep               // Output, size = 1 word(s)
  , const CarbonUInt32* SI_EICPresent          //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_EISS                //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_Int                 //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_IPTI                //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_IPPCI               //  Input, size = 1 word(s)
  ,       CarbonUInt32* SI_IAck                // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_IPL                 // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_SWInt               // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_TimerInt            // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_PCInt               // Output, size = 1 word(s)
  , const CarbonUInt32* SI_CPUNum              //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_Endian              //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_SimpleBE            //  Input, size = 1 word(s)
  , const CarbonUInt32* SI_SBlock              //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_Sets                //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_LineSize            //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_Assoc               //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_PCWB                //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_PCAcc               //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_PCMiss              //  Input, size = 1 word(s)
  , const CarbonUInt32* L2_PCMissCy            //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_TRST_N              //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_TCK                 //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_TMS                 //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_TDI                 //  Input, size = 1 word(s)
  ,       CarbonUInt32* EJ_TDO                 // Output, size = 1 word(s)
  ,       CarbonUInt32* EJ_TDOzstate           // Output, size = 1 word(s)
  , const CarbonUInt32* EJ_DINTsup             //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_DINT                //  Input, size = 1 word(s)
  ,       CarbonUInt32* EJ_DebugM              // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_Ibs                 // Output, size = 1 word(s)
  ,       CarbonUInt32* SI_Dbs                 // Output, size = 1 word(s)
  , const CarbonUInt32* EJ_ManufID             //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_PartNumber          //  Input, size = 1 word(s)
  , const CarbonUInt32* EJ_Version             //  Input, size = 1 word(s)
  ,       CarbonUInt32* EJ_SRstE               // Output, size = 1 word(s)
  ,       CarbonUInt32* EJ_PerRst              // Output, size = 1 word(s)
  ,       CarbonUInt32* EJ_PrRst               // Output, size = 1 word(s)
  , const CarbonUInt32* gscanenable            //  Input, size = 1 word(s)
  , const CarbonUInt32* gscanmode              //  Input, size = 1 word(s)
  , const CarbonUInt32* gscanramaddr0          //  Input, size = 1 word(s)
  , const CarbonUInt32* gscanramwr             //  Input, size = 1 word(s)
  , const CarbonUInt32* gscanin                //  Input, size = 1 word(s)
  ,       CarbonUInt32* gscanout               // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_greset_pre          // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_gclk                // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_gfclk               // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_gscanenable         // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_parity_en           // Output, size = 1 word(s)
  , const CarbonUInt32* SP_present             //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_parity_present      //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_ram_busy            //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_busy_xx             //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_perfcnt_event       //  Input, size = 1 word(s)
  ,       CarbonUInt32* SP_wait_pd_xx          // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_sleep_req_xx        // Output, size = 1 word(s)
  , const CarbonUInt32* SP_tag_rdata_xx        //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_tag_msk_xx          //  Input, size = 1 word(s)
  ,       CarbonUInt32* SP_tag_rd_ag           // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_tag_wr_ag           // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_tag_sel_ag          // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_tag_wdata_ag        // Output, size = 1 word(s)
  , const CarbonUInt32* SP_data_rdata_xx       //  Input, size = 2 word(s)
  , const CarbonUInt32* SP_data_rpar_xx        //  Input, size = 1 word(s)
  ,       CarbonUInt32* SP_data_wdata_ag       // Output, size = 2 word(s)
  ,       CarbonUInt32* SP_data_wpar_ag        // Output, size = 1 word(s)
  , const CarbonUInt32* SP_datavld_nxt_xx      //  Input, size = 1 word(s)
  ,       CarbonUInt32* SP_data_addr_ag        // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_data_rd_ag          // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_dma_rd_ag           // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_data_wr_ag          // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_data_wren_ag        // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_core_reqpd_er       // Output, size = 1 word(s)
  ,       CarbonUInt32* SP_req_id_ag           // Output, size = 1 word(s)
  , const CarbonUInt32* SP_data_id_xx          //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_id_xx           //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_addr_xx         //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_wdata_xx        //  Input, size = 2 word(s)
  , const CarbonUInt32* SP_dma_wren_xx         //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_rd_xx           //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_wr_xx           //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_dma_stallreq_xx     //  Input, size = 1 word(s)
  , const CarbonUInt32* SP_resync_busy_xx      //  Input, size = 1 word(s)
  ,       CarbonUInt32* SP_mbsp_tosp_xx        // Output, size = 1 word(s)
  , const CarbonUInt32* SP_sp_tombsp_xx        //  Input, size = 1 word(s)
  ,       CarbonUInt32* ISP_greset_pre         // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_gclk               // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_gfclk              // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_gscanenable        // Output, size = 1 word(s)
  , const CarbonUInt32* ISP_present            //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_parity_present     //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_ram_busy           //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_busy_xx            //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_perfcnt_event      //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_tag_rdata_if       //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_tag_msk_if         //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_data_rdata_is      //  Input, size = 3 word(s)
  , const CarbonUInt32* ISP_data_rpar_is       //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_datavld_nxt_if     //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_dma_addr_xx        //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_dma_wdata_xx       //  Input, size = 2 word(s)
  , const CarbonUInt32* ISP_dma_rdreq_xx       //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_dma_wrreq_xx       //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_dma_stallreq_xx    //  Input, size = 1 word(s)
  , const CarbonUInt32* ISP_resync_busy_xx     //  Input, size = 1 word(s)
  ,       CarbonUInt32* ISP_parity_en          // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_wait_pd_xx         // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_sleep_req_xx       // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_core_reqpd_xx      // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_addr_ipf           // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_tag_sel_ipf        // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_rd_ipf             // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_dma_rd_ipf         // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_data_wr_ipf        // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_data_wdata_ipf     // Output, size = 3 word(s)
  ,       CarbonUInt32* ISP_data_wpar_ipf      // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_tag_wr_ipf         // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_tag_wdata_ipf      // Output, size = 1 word(s)
  ,       CarbonUInt32* ISP_mbisp_toisp_xx     // Output, size = 1 word(s)
  , const CarbonUInt32* ISP_isp_tombisp_xx     //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_invoke              //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_ic_algorithm        //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_dc_algorithm        //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_sp_algorithm        //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_isp_algorithm       //  Input, size = 1 word(s)
  , const CarbonUInt32* MB_tr_algorithm        //  Input, size = 1 word(s)
  ,       CarbonUInt32* MB_done                // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_dd_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_dt_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_dw_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_sp_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_isp_fail            // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_id_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_it_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_iw_fail             // Output, size = 1 word(s)
  ,       CarbonUInt32* MB_tr_fail             // Output, size = 1 word(s)
  , const CarbonUInt32* MB_tombt               //  Input, size = 1 word(s)
  ,       CarbonUInt32* MB_frommbt             // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_ir_0               // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_irenable_0         // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_inst32_0           // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_endian_0           // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_tds_0              // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_torder_0           // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_fordlim_0          // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_tdata_0            // Output, size = 2 word(s)
  , const CarbonUInt32* CP2_fds_0              //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_forder_0           //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_tordlim_0          //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_fdata_0            //  Input, size = 2 word(s)
  , const CarbonUInt32* CP2_cccs_0             //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_ccc_0              //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_excs_0             //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_exc_0              //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_exccode_0          //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_nulls_0            // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_null_0             // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_kills_0            // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_kill_0             // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_idle               //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_as_0               // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_abusy_0            //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_ts_0               // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_tbusy_0            //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_fs_0               // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_fbusy_0            //  Input, size = 1 word(s)
  , const CarbonUInt32* CP2_present            //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_kd_mode_0          // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_tx32               //  Input, size = 1 word(s)
  ,       CarbonUInt32* CP2_reset              // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_gclk               // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_gfclk              // Output, size = 1 word(s)
  ,       CarbonUInt32* CP2_gscanenable        // Output, size = 1 word(s)
  , const CarbonUInt32* CP2_perfcnt_event      //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_present            //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_ri_rf              //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_use_rs_rf          //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_use_rt_rf          //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_wrreg_rf           //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_use_hilo_rf        //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_wr_hilo_rf         //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_stallreq_ag        //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_hi_wr_strobe_xx    //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_lo_wr_strobe_xx    //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_hi_wr_data_xx      //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_lo_wr_data_xx      //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_gpr_wr_strobe_ms   //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_gpr_wr_data_ms     //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_honor_cee          //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_busy_xx            //  Input, size = 1 word(s)
  , const CarbonUInt32* UDI_pend_gpr_wr_xx     //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_pend_hilo_wr_xx    //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_dsp_rd_hilo_rf     //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_dsp_wr_state_rf    //  Input, size = 1 word(s)
  , const CarbonUInt32* UHL_dsp_hilo_wr_xx     //  Input, size = 1 word(s)
  ,       CarbonUInt32* UDI_greset_pre         // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_gclk               // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_gfclk              // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_gscanenable        // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_ir_rf              // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_kd_mode_rf         // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_endianb_xx         // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_nxt_opc_xx         // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_run_ex             // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_start_xx           // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_rs_xx              // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_rt_xx              // Output, size = 1 word(s)
  ,       CarbonUInt32* UHL_hi_rd_data_xx      // Output, size = 1 word(s)
  ,       CarbonUInt32* UHL_lo_rd_data_xx      // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_age_xx             // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_run_ms             // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_gpr_wr_ack_ms      // Output, size = 1 word(s)
  ,       CarbonUInt32* UDI_run_er             // Output, size = 1 word(s)
  , const CarbonUInt32* UDI_perfcnt_event      //  Input, size = 1 word(s)
  ,       CarbonUInt32* UDI_kill_er            // Output, size = 1 word(s)
  , const CarbonUInt32* TC_CRMax               //  Input, size = 1 word(s)
  , const CarbonUInt32* TC_CRMin               //  Input, size = 1 word(s)
  , const CarbonUInt32* TC_ProbeWidth          //  Input, size = 1 word(s)
  , const CarbonUInt32* TC_DataBits            //  Input, size = 1 word(s)
  , const CarbonUInt32* TC_Stall               //  Input, size = 1 word(s)
  , const CarbonUInt32* TC_PibPresent          //  Input, size = 1 word(s)
  ,       CarbonUInt32* TC_ClockRatio          // Output, size = 1 word(s)
  ,       CarbonUInt32* TC_Valid               // Output, size = 1 word(s)
  ,       CarbonUInt32* TC_Data                // Output, size = 2 word(s)
  ,       CarbonUInt32* TC_TrEnable            // Output, size = 1 word(s)
  ,       CarbonUInt32* TC_Calibrate           // Output, size = 1 word(s)
  , const CarbonUInt32* TC_ProbeTrigIn         //  Input, size = 1 word(s)
  ,       CarbonUInt32* TC_ProbeTrigOut        // Output, size = 1 word(s)
  ,       CarbonUInt32* TC_ChipTrigOut         // Output, size = 1 word(s)
  , const CarbonUInt32* TC_ChipTrigIn          //  Input, size = 1 word(s)
)
{
  CarbonBfm24K * bfm = reinterpret_cast<CarbonBfm24K *>(hndl);

  if(context == eCDSCarbonBfm24KRiseSI_ClkIn) bfm->clock(true);
  if(context == eCDSCarbonBfm24KFallSI_ClkIn) bfm->clock(false);

  // Send all inputs to MIPS::Bfm24K

  bfm->SetSignal( Bfm24K_CP2_abusy_0            , CP2_abusy_0            );
  bfm->SetSignal( Bfm24K_CP2_ccc_0              , CP2_ccc_0              );
  bfm->SetSignal( Bfm24K_CP2_cccs_0             , CP2_cccs_0             );
  bfm->SetSignal( Bfm24K_CP2_exc_0              , CP2_exc_0              );
  bfm->SetSignal( Bfm24K_CP2_exccode_0          , CP2_exccode_0          );
  bfm->SetSignal( Bfm24K_CP2_excs_0             , CP2_excs_0             );
  bfm->SetSignal( Bfm24K_CP2_fbusy_0            , CP2_fbusy_0            );
  bfm->SetSignal( Bfm24K_CP2_fdata_0            , CP2_fdata_0            );
  bfm->SetSignal( Bfm24K_CP2_fds_0              , CP2_fds_0              );
  bfm->SetSignal( Bfm24K_CP2_forder_0           , CP2_forder_0           );
  bfm->SetSignal( Bfm24K_CP2_idle               , CP2_idle               );
  bfm->SetSignal( Bfm24K_CP2_perfcnt_event      , CP2_perfcnt_event      );
  bfm->SetSignal( Bfm24K_CP2_present            , CP2_present            );
  bfm->SetSignal( Bfm24K_CP2_tbusy_0            , CP2_tbusy_0            );
  bfm->SetSignal( Bfm24K_CP2_tordlim_0          , CP2_tordlim_0          );
  bfm->SetSignal( Bfm24K_CP2_tx32               , CP2_tx32               );
  bfm->SetSignal( Bfm24K_EJ_DINT                , EJ_DINT                );
  bfm->SetSignal( Bfm24K_EJ_DINTsup             , EJ_DINTsup             );
  bfm->SetSignal( Bfm24K_EJ_ManufID             , EJ_ManufID             );
  bfm->SetSignal( Bfm24K_EJ_PartNumber          , EJ_PartNumber          );
  bfm->SetSignal( Bfm24K_EJ_TCK                 , EJ_TCK                 );
  bfm->SetSignal( Bfm24K_EJ_TDI                 , EJ_TDI                 );
  bfm->SetSignal( Bfm24K_EJ_TMS                 , EJ_TMS                 );
  bfm->SetSignal( Bfm24K_EJ_TRST_N              , EJ_TRST_N              );
  bfm->SetSignal( Bfm24K_EJ_Version             , EJ_Version             );
  bfm->SetSignal( Bfm24K_gscanenable            , gscanenable            );
  bfm->SetSignal( Bfm24K_gscanin                , gscanin                );
  bfm->SetSignal( Bfm24K_gscanmode              , gscanmode              );
  bfm->SetSignal( Bfm24K_gscanramaddr0          , gscanramaddr0          );
  bfm->SetSignal( Bfm24K_gscanramwr             , gscanramwr             );
  bfm->SetSignal( Bfm24K_ISP_busy_xx            , ISP_busy_xx            );
  bfm->SetSignal( Bfm24K_ISP_data_rdata_is      , ISP_data_rdata_is      );
  bfm->SetSignal( Bfm24K_ISP_data_rpar_is       , ISP_data_rpar_is       );
  bfm->SetSignal( Bfm24K_ISP_datavld_nxt_if     , ISP_datavld_nxt_if     );
  bfm->SetSignal( Bfm24K_ISP_dma_addr_xx        , ISP_dma_addr_xx        );
  bfm->SetSignal( Bfm24K_ISP_dma_rdreq_xx       , ISP_dma_rdreq_xx       );
  bfm->SetSignal( Bfm24K_ISP_dma_stallreq_xx    , ISP_dma_stallreq_xx    );
  bfm->SetSignal( Bfm24K_ISP_dma_wdata_xx       , ISP_dma_wdata_xx       );
  bfm->SetSignal( Bfm24K_ISP_dma_wrreq_xx       , ISP_dma_wrreq_xx       );
  bfm->SetSignal( Bfm24K_ISP_isp_tombisp_xx     , ISP_isp_tombisp_xx     );
  bfm->SetSignal( Bfm24K_ISP_parity_present     , ISP_parity_present     );
  bfm->SetSignal( Bfm24K_ISP_perfcnt_event      , ISP_perfcnt_event      );
  bfm->SetSignal( Bfm24K_ISP_present            , ISP_present            );
  bfm->SetSignal( Bfm24K_ISP_ram_busy           , ISP_ram_busy           );
  bfm->SetSignal( Bfm24K_ISP_resync_busy_xx     , ISP_resync_busy_xx     );
  bfm->SetSignal( Bfm24K_ISP_tag_msk_if         , ISP_tag_msk_if         );
  bfm->SetSignal( Bfm24K_ISP_tag_rdata_if       , ISP_tag_rdata_if       );
  bfm->SetSignal( Bfm24K_L2_Assoc               , L2_Assoc               );
  bfm->SetSignal( Bfm24K_L2_LineSize            , L2_LineSize            );
  bfm->SetSignal( Bfm24K_L2_PCAcc               , L2_PCAcc               );
  bfm->SetSignal( Bfm24K_L2_PCMiss              , L2_PCMiss              );
  bfm->SetSignal( Bfm24K_L2_PCMissCy            , L2_PCMissCy            );
  bfm->SetSignal( Bfm24K_L2_PCWB                , L2_PCWB                );
  bfm->SetSignal( Bfm24K_L2_Sets                , L2_Sets                );
  bfm->SetSignal( Bfm24K_MB_dc_algorithm        , MB_dc_algorithm        );
  bfm->SetSignal( Bfm24K_MB_ic_algorithm        , MB_ic_algorithm        );
  bfm->SetSignal( Bfm24K_MB_invoke              , MB_invoke              );
  bfm->SetSignal( Bfm24K_MB_isp_algorithm       , MB_isp_algorithm       );
  bfm->SetSignal( Bfm24K_MB_sp_algorithm        , MB_sp_algorithm        );
  bfm->SetSignal( Bfm24K_MB_tombt               , MB_tombt               );
  bfm->SetSignal( Bfm24K_MB_tr_algorithm        , MB_tr_algorithm        );
  bfm->SetSignal( Bfm24K_OC_SCmdAccept          , OC_SCmdAccept          );
  bfm->SetSignal( Bfm24K_OC_SData               , OC_SData               );
  bfm->SetSignal( Bfm24K_OC_SDataAccept         , OC_SDataAccept         );
  bfm->SetSignal( Bfm24K_OC_SResp               , OC_SResp               );
  bfm->SetSignal( Bfm24K_OC_SRespLast           , OC_SRespLast           );
  bfm->SetSignal( Bfm24K_OC_STagID              , OC_STagID              );
  bfm->SetSignal( Bfm24K_SI_ClkIn               , SI_ClkIn               );
  bfm->SetSignal( Bfm24K_SI_CPUNum              , SI_CPUNum              );
  bfm->SetSignal( Bfm24K_SI_EICPresent          , SI_EICPresent          );
  bfm->SetSignal( Bfm24K_SI_EISS                , SI_EISS                );
  bfm->SetSignal( Bfm24K_SI_Endian              , SI_Endian              );
  bfm->SetSignal( Bfm24K_SI_Int                 , SI_Int                 );
  bfm->SetSignal( Bfm24K_SI_IPPCI               , SI_IPPCI               );
  bfm->SetSignal( Bfm24K_SI_IPTI                , SI_IPTI                );
  bfm->SetSignal( Bfm24K_SI_NMI                 , SI_NMI                 );
  bfm->SetSignal( Bfm24K_SI_OCPReSyncReq        , SI_OCPReSyncReq        );
  bfm->SetSignal( Bfm24K_SI_OCPSync             , SI_OCPSync             );
  bfm->SetSignal( Bfm24K_SI_Reset               , SI_Reset               );
  bfm->SetSignal( Bfm24K_SI_SBlock              , SI_SBlock              );
  bfm->SetSignal( Bfm24K_SI_SimpleBE            , SI_SimpleBE            );
  bfm->SetSignal( Bfm24K_SP_busy_xx             , SP_busy_xx             );
  bfm->SetSignal( Bfm24K_SP_data_id_xx          , SP_data_id_xx          );
  bfm->SetSignal( Bfm24K_SP_data_rdata_xx       , SP_data_rdata_xx       );
  bfm->SetSignal( Bfm24K_SP_data_rpar_xx        , SP_data_rpar_xx        );
  bfm->SetSignal( Bfm24K_SP_datavld_nxt_xx      , SP_datavld_nxt_xx      );
  bfm->SetSignal( Bfm24K_SP_dma_addr_xx         , SP_dma_addr_xx         );
  bfm->SetSignal( Bfm24K_SP_dma_id_xx           , SP_dma_id_xx           );
  bfm->SetSignal( Bfm24K_SP_dma_rd_xx           , SP_dma_rd_xx           );
  bfm->SetSignal( Bfm24K_SP_dma_stallreq_xx     , SP_dma_stallreq_xx     );
  bfm->SetSignal( Bfm24K_SP_dma_wdata_xx        , SP_dma_wdata_xx        );
  bfm->SetSignal( Bfm24K_SP_dma_wren_xx         , SP_dma_wren_xx         );
  bfm->SetSignal( Bfm24K_SP_dma_wr_xx           , SP_dma_wr_xx           );
  bfm->SetSignal( Bfm24K_SP_parity_present      , SP_parity_present      );
  bfm->SetSignal( Bfm24K_SP_perfcnt_event       , SP_perfcnt_event       );
  bfm->SetSignal( Bfm24K_SP_present             , SP_present             );
  bfm->SetSignal( Bfm24K_SP_ram_busy            , SP_ram_busy            );
  bfm->SetSignal( Bfm24K_SP_resync_busy_xx      , SP_resync_busy_xx      );
  bfm->SetSignal( Bfm24K_SP_sp_tombsp_xx        , SP_sp_tombsp_xx        );
  bfm->SetSignal( Bfm24K_SP_tag_msk_xx          , SP_tag_msk_xx          );
  bfm->SetSignal( Bfm24K_SP_tag_rdata_xx        , SP_tag_rdata_xx        );
  bfm->SetSignal( Bfm24K_TC_ChipTrigIn          , TC_ChipTrigIn          );
  bfm->SetSignal( Bfm24K_TC_CRMax               , TC_CRMax               );
  bfm->SetSignal( Bfm24K_TC_CRMin               , TC_CRMin               );
  bfm->SetSignal( Bfm24K_TC_DataBits            , TC_DataBits            );
  bfm->SetSignal( Bfm24K_TC_PibPresent          , TC_PibPresent          );
  bfm->SetSignal( Bfm24K_TC_ProbeTrigIn         , TC_ProbeTrigIn         );
  bfm->SetSignal( Bfm24K_TC_ProbeWidth          , TC_ProbeWidth          );
  bfm->SetSignal( Bfm24K_TC_Stall               , TC_Stall               );
  bfm->SetSignal( Bfm24K_UDI_busy_xx            , UDI_busy_xx            );
  bfm->SetSignal( Bfm24K_UDI_gpr_wr_data_ms     , UDI_gpr_wr_data_ms     );
  bfm->SetSignal( Bfm24K_UDI_gpr_wr_strobe_ms   , UDI_gpr_wr_strobe_ms   );
  bfm->SetSignal( Bfm24K_UDI_honor_cee          , UDI_honor_cee          );
  bfm->SetSignal( Bfm24K_UDI_pend_gpr_wr_xx     , UDI_pend_gpr_wr_xx     );
  bfm->SetSignal( Bfm24K_UDI_perfcnt_event      , UDI_perfcnt_event      );
  bfm->SetSignal( Bfm24K_UDI_present            , UDI_present            );
  bfm->SetSignal( Bfm24K_UDI_ri_rf              , UDI_ri_rf              );
  bfm->SetSignal( Bfm24K_UDI_stallreq_ag        , UDI_stallreq_ag        );
  bfm->SetSignal( Bfm24K_UDI_use_rs_rf          , UDI_use_rs_rf          );
  bfm->SetSignal( Bfm24K_UDI_use_rt_rf          , UDI_use_rt_rf          );
  bfm->SetSignal( Bfm24K_UDI_wrreg_rf           , UDI_wrreg_rf           );
  bfm->SetSignal( Bfm24K_UHL_dsp_hilo_wr_xx     , UHL_dsp_hilo_wr_xx     );
  bfm->SetSignal( Bfm24K_UHL_dsp_rd_hilo_rf     , UHL_dsp_rd_hilo_rf     );
  bfm->SetSignal( Bfm24K_UHL_dsp_wr_state_rf    , UHL_dsp_wr_state_rf    );
  bfm->SetSignal( Bfm24K_UHL_hi_wr_data_xx      , UHL_hi_wr_data_xx      );
  bfm->SetSignal( Bfm24K_UHL_hi_wr_strobe_xx    , UHL_hi_wr_strobe_xx    );
  bfm->SetSignal( Bfm24K_UHL_lo_wr_data_xx      , UHL_lo_wr_data_xx      );
  bfm->SetSignal( Bfm24K_UHL_lo_wr_strobe_xx    , UHL_lo_wr_strobe_xx    );
  bfm->SetSignal( Bfm24K_UHL_pend_hilo_wr_xx    , UHL_pend_hilo_wr_xx    );
  bfm->SetSignal( Bfm24K_UHL_use_hilo_rf        , UHL_use_hilo_rf        );
  bfm->SetSignal( Bfm24K_UHL_wr_hilo_rf         , UHL_wr_hilo_rf         );

  // Get outputs from MIPS::Bfm24K

  // Update output clocks on both edges 

  bfm->GetSignal( Bfm24K_SI_ClkOut , SI_ClkOut );
  bfm->GetSignal( Bfm24K_CP2_gclk  , CP2_gclk  );
  bfm->GetSignal( Bfm24K_CP2_gfclk , CP2_gfclk );
  bfm->GetSignal( Bfm24K_ISP_gclk  , ISP_gclk  );
  bfm->GetSignal( Bfm24K_ISP_gfclk , ISP_gfclk );
  bfm->GetSignal( Bfm24K_SP_gclk   , SP_gclk   );
  bfm->GetSignal( Bfm24K_SP_gfclk  , SP_gfclk  );
  bfm->GetSignal( Bfm24K_UDI_gclk  , UDI_gclk  );
  bfm->GetSignal( Bfm24K_UDI_gfclk , UDI_gfclk );

  // Update on posedge

  if(context == eCDSCarbonBfm24KRiseSI_ClkIn) {

    bfm->GetSignal( Bfm24K_EJ_DebugM              , EJ_DebugM              );
    bfm->GetSignal( Bfm24K_EJ_PerRst              , EJ_PerRst              );
    bfm->GetSignal( Bfm24K_EJ_PrRst               , EJ_PrRst               );
    bfm->GetSignal( Bfm24K_EJ_SRstE               , EJ_SRstE               );
    bfm->GetSignal( Bfm24K_EJ_TDO                 , EJ_TDO                 );
    bfm->GetSignal( Bfm24K_EJ_TDOzstate           , EJ_TDOzstate           );
    bfm->GetSignal( Bfm24K_gscanout               , gscanout               );
    bfm->GetSignal( Bfm24K_MB_dd_fail             , MB_dd_fail             );
    bfm->GetSignal( Bfm24K_MB_done                , MB_done                );
    bfm->GetSignal( Bfm24K_MB_dt_fail             , MB_dt_fail             );
    bfm->GetSignal( Bfm24K_MB_dw_fail             , MB_dw_fail             );
    bfm->GetSignal( Bfm24K_MB_frommbt             , MB_frommbt             );
    bfm->GetSignal( Bfm24K_MB_id_fail             , MB_id_fail             );
    bfm->GetSignal( Bfm24K_MB_isp_fail            , MB_isp_fail            );
    bfm->GetSignal( Bfm24K_MB_it_fail             , MB_it_fail             );
    bfm->GetSignal( Bfm24K_MB_iw_fail             , MB_iw_fail             );
    bfm->GetSignal( Bfm24K_MB_sp_fail             , MB_sp_fail             );
    bfm->GetSignal( Bfm24K_MB_tr_fail             , MB_tr_fail             );
    bfm->GetSignal( Bfm24K_OC_MAddrSpace          , OC_MAddrSpace          );
    bfm->GetSignal( Bfm24K_OC_MAddr               , OC_MAddr               );
    bfm->GetSignal( Bfm24K_OC_MBurstLength        , OC_MBurstLength        );
    bfm->GetSignal( Bfm24K_OC_MBurstPrecise       , OC_MBurstPrecise       );
    bfm->GetSignal( Bfm24K_OC_MBurstSeq           , OC_MBurstSeq           );
    bfm->GetSignal( Bfm24K_OC_MBurstSingleReq     , OC_MBurstSingleReq     );
    bfm->GetSignal( Bfm24K_OC_MByteEn             , OC_MByteEn             );
    bfm->GetSignal( Bfm24K_OC_MCmd                , OC_MCmd                );
    bfm->GetSignal( Bfm24K_OC_MData               , OC_MData               );
    bfm->GetSignal( Bfm24K_OC_MDataByteEn         , OC_MDataByteEn         );
    bfm->GetSignal( Bfm24K_OC_MDataLast           , OC_MDataLast           );
    bfm->GetSignal( Bfm24K_OC_MDataTagID          , OC_MDataTagID          );
    bfm->GetSignal( Bfm24K_OC_MDataValid          , OC_MDataValid          );
    bfm->GetSignal( Bfm24K_OC_MReqInfo            , OC_MReqInfo            );
    bfm->GetSignal( Bfm24K_OC_MReset_n            , OC_MReset_n            );
    bfm->GetSignal( Bfm24K_OC_MTagID              , OC_MTagID              );
    bfm->GetSignal( Bfm24K_SI_Dbs                 , SI_Dbs                 );
    bfm->GetSignal( Bfm24K_SI_ERL                 , SI_ERL                 );
    bfm->GetSignal( Bfm24K_SI_EXL                 , SI_EXL                 );
    bfm->GetSignal( Bfm24K_SI_IAck                , SI_IAck                );
    bfm->GetSignal( Bfm24K_SI_Ibs                 , SI_Ibs                 );
    bfm->GetSignal( Bfm24K_SI_IPL                 , SI_IPL                 );
    bfm->GetSignal( Bfm24K_SI_OCPRatioLock        , SI_OCPRatioLock        );
    bfm->GetSignal( Bfm24K_SI_PCInt               , SI_PCInt               );
    bfm->GetSignal( Bfm24K_SI_RP                  , SI_RP                  );
    bfm->GetSignal( Bfm24K_SI_Sleep               , SI_Sleep               );
    bfm->GetSignal( Bfm24K_SI_SWInt               , SI_SWInt               );
    bfm->GetSignal( Bfm24K_SI_TimerInt            , SI_TimerInt            );
    bfm->GetSignal( Bfm24K_TC_Calibrate           , TC_Calibrate           );
    bfm->GetSignal( Bfm24K_TC_ChipTrigOut         , TC_ChipTrigOut         );
    bfm->GetSignal( Bfm24K_TC_ClockRatio          , TC_ClockRatio          );
    bfm->GetSignal( Bfm24K_TC_Data                , TC_Data                );
    bfm->GetSignal( Bfm24K_TC_ProbeTrigOut        , TC_ProbeTrigOut        );
    bfm->GetSignal( Bfm24K_TC_TrEnable            , TC_TrEnable            );
    bfm->GetSignal( Bfm24K_TC_Valid               , TC_Valid               );
    bfm->GetSignal( Bfm24K_UDI_age_xx             , UDI_age_xx             );
    bfm->GetSignal( Bfm24K_UDI_endianb_xx         , UDI_endianb_xx         );
    bfm->GetSignal( Bfm24K_UDI_gpr_wr_ack_ms      , UDI_gpr_wr_ack_ms      );
    bfm->GetSignal( Bfm24K_UDI_greset_pre         , UDI_greset_pre         );
    bfm->GetSignal( Bfm24K_UDI_gscanenable        , UDI_gscanenable        );
    bfm->GetSignal( Bfm24K_UDI_ir_rf              , UDI_ir_rf              );
    bfm->GetSignal( Bfm24K_UDI_kd_mode_rf         , UDI_kd_mode_rf         );
    bfm->GetSignal( Bfm24K_UDI_kill_er            , UDI_kill_er            );
    bfm->GetSignal( Bfm24K_UDI_nxt_opc_xx         , UDI_nxt_opc_xx         );
    bfm->GetSignal( Bfm24K_UDI_rs_xx              , UDI_rs_xx              );
    bfm->GetSignal( Bfm24K_UDI_rt_xx              , UDI_rt_xx              );
    bfm->GetSignal( Bfm24K_UDI_run_er             , UDI_run_er             );
    bfm->GetSignal( Bfm24K_UDI_run_ex             , UDI_run_ex             );
    bfm->GetSignal( Bfm24K_UDI_run_ms             , UDI_run_ms             );
    bfm->GetSignal( Bfm24K_UDI_start_xx           , UDI_start_xx           );
    bfm->GetSignal( Bfm24K_UHL_hi_rd_data_xx      , UHL_hi_rd_data_xx      );
    bfm->GetSignal( Bfm24K_UHL_lo_rd_data_xx      , UHL_lo_rd_data_xx      );

  }

  // Update on negedge

  if(context == eCDSCarbonBfm24KFallSI_ClkIn) {

    bfm->GetSignal( Bfm24K_CP2_as_0               , CP2_as_0               );
    bfm->GetSignal( Bfm24K_CP2_endian_0           , CP2_endian_0           );
    bfm->GetSignal( Bfm24K_CP2_fordlim_0          , CP2_fordlim_0          );
    bfm->GetSignal( Bfm24K_CP2_fs_0               , CP2_fs_0               );
    bfm->GetSignal( Bfm24K_CP2_gscanenable        , CP2_gscanenable        );
    bfm->GetSignal( Bfm24K_CP2_inst32_0           , CP2_inst32_0           );
    bfm->GetSignal( Bfm24K_CP2_ir_0               , CP2_ir_0               );
    bfm->GetSignal( Bfm24K_CP2_irenable_0         , CP2_irenable_0         );
    bfm->GetSignal( Bfm24K_CP2_kd_mode_0          , CP2_kd_mode_0          );
    bfm->GetSignal( Bfm24K_CP2_kill_0             , CP2_kill_0             );
    bfm->GetSignal( Bfm24K_CP2_kills_0            , CP2_kills_0            );
    bfm->GetSignal( Bfm24K_CP2_null_0             , CP2_null_0             );
    bfm->GetSignal( Bfm24K_CP2_nulls_0            , CP2_nulls_0            );
    bfm->GetSignal( Bfm24K_CP2_reset              , CP2_reset              );
    bfm->GetSignal( Bfm24K_CP2_tdata_0            , CP2_tdata_0            );
    bfm->GetSignal( Bfm24K_CP2_tds_0              , CP2_tds_0              );
    bfm->GetSignal( Bfm24K_CP2_torder_0           , CP2_torder_0           );
    bfm->GetSignal( Bfm24K_CP2_ts_0               , CP2_ts_0               );
    bfm->GetSignal( Bfm24K_ISP_addr_ipf           , ISP_addr_ipf           );
    bfm->GetSignal( Bfm24K_ISP_core_reqpd_xx      , ISP_core_reqpd_xx      );
    bfm->GetSignal( Bfm24K_ISP_data_wdata_ipf     , ISP_data_wdata_ipf     );
    bfm->GetSignal( Bfm24K_ISP_data_wpar_ipf      , ISP_data_wpar_ipf      );
    bfm->GetSignal( Bfm24K_ISP_data_wr_ipf        , ISP_data_wr_ipf        );
    bfm->GetSignal( Bfm24K_ISP_dma_rd_ipf         , ISP_dma_rd_ipf         );
    bfm->GetSignal( Bfm24K_ISP_greset_pre         , ISP_greset_pre         );
    bfm->GetSignal( Bfm24K_ISP_gscanenable        , ISP_gscanenable        );
    bfm->GetSignal( Bfm24K_ISP_mbisp_toisp_xx     , ISP_mbisp_toisp_xx     );
    bfm->GetSignal( Bfm24K_ISP_parity_en          , ISP_parity_en          );
    bfm->GetSignal( Bfm24K_ISP_rd_ipf             , ISP_rd_ipf             );
    bfm->GetSignal( Bfm24K_ISP_sleep_req_xx       , ISP_sleep_req_xx       );
    bfm->GetSignal( Bfm24K_ISP_tag_sel_ipf        , ISP_tag_sel_ipf        );
    bfm->GetSignal( Bfm24K_ISP_tag_wdata_ipf      , ISP_tag_wdata_ipf      );
    bfm->GetSignal( Bfm24K_ISP_tag_wr_ipf         , ISP_tag_wr_ipf         );
    bfm->GetSignal( Bfm24K_ISP_wait_pd_xx         , ISP_wait_pd_xx         );
    bfm->GetSignal( Bfm24K_SP_core_reqpd_er       , SP_core_reqpd_er       );
    bfm->GetSignal( Bfm24K_SP_data_addr_ag        , SP_data_addr_ag        );
    bfm->GetSignal( Bfm24K_SP_data_rd_ag          , SP_data_rd_ag          );
    bfm->GetSignal( Bfm24K_SP_data_wdata_ag       , SP_data_wdata_ag       );
    bfm->GetSignal( Bfm24K_SP_data_wpar_ag        , SP_data_wpar_ag        );
    bfm->GetSignal( Bfm24K_SP_data_wr_ag          , SP_data_wr_ag          );
    bfm->GetSignal( Bfm24K_SP_data_wren_ag        , SP_data_wren_ag        );
    bfm->GetSignal( Bfm24K_SP_dma_rd_ag           , SP_dma_rd_ag           );
    bfm->GetSignal( Bfm24K_SP_greset_pre          , SP_greset_pre          );
    bfm->GetSignal( Bfm24K_SP_gscanenable         , SP_gscanenable         );
    bfm->GetSignal( Bfm24K_SP_mbsp_tosp_xx        , SP_mbsp_tosp_xx        );
    bfm->GetSignal( Bfm24K_SP_parity_en           , SP_parity_en           );
    bfm->GetSignal( Bfm24K_SP_req_id_ag           , SP_req_id_ag           );
    bfm->GetSignal( Bfm24K_SP_sleep_req_xx        , SP_sleep_req_xx        );
    bfm->GetSignal( Bfm24K_SP_tag_rd_ag           , SP_tag_rd_ag           );
    bfm->GetSignal( Bfm24K_SP_tag_sel_ag          , SP_tag_sel_ag          );
    bfm->GetSignal( Bfm24K_SP_tag_wdata_ag        , SP_tag_wdata_ag        );
    bfm->GetSignal( Bfm24K_SP_tag_wr_ag           , SP_tag_wr_ag           );
    bfm->GetSignal( Bfm24K_SP_wait_pd_xx          , SP_wait_pd_xx          );

  }
}

static int bfm_print (int, void *, const char * buffer)
{
    cout << buffer << endl;

    return 0;
}

static void bfm_exit (int aSysID, void * apSysData) { exit(-1); }

static void bfm_mdiclock (void * aDevId)
{
  MDIInt32 return_status = gsMdiServer->NextClockEdge((MDIHandleT)aDevId);

  cout << "return_status= " << return_status << endl;
//assert(return_status == MDISuccess || return_status == MDIErrMore);
} 

static void create_iss( MIPS::Bfm24K *bfm, int id, MDIHandleT *core_id )
{
  MDIInt32 return_status;

  // global pointer from MdiServer.h
  gpMdiServer= gsMdiServer= new MdiCoSimServer;

  // Display all messages to console.
  gsMdiServer->SetDisplay(true);

  // Turn off messages to console display
  gsMdiServer->SetDisplayConsole(false); 

  // Do not reset core after debugger disconnects.
  gsMdiServer->SetResetOnDisconnect(false); 

  // Leave core open after debugger disconnects.
  gsMdiServer->SetCloseOnDisconnect(false); 

  // Leave simulation running after all debuggers disconnect.
  gsMdiServer->SetRunForever(true); 

  // Allow other cores to still run when a debugger halts a core.
  gsMdiServer->SetNonblockingHalt(true); 

  char * p_func_name = "MIPSsim_GetMdiCalls";
  char * mdi_libname = "libMIPSsim_MDIc.so";

  const MdiCalls * (*p_mipssim_getmdicalls) ();
  void ** p_func = (void**)&(p_mipssim_getmdicalls);

  void * gspMdiLib = dlopen(mdi_libname, RTLD_LOCAL|RTLD_LAZY);

  if (gspMdiLib == NULL) {
    printf("$MIPSBfmPli$Init: Could not open MDI Control "
           "Interface library %s, %s.", mdi_libname, dlerror());
    bfm_exit(0, NULL);

  }

  *p_func = dlsym(gspMdiLib, p_func_name);

  if (!*p_func) {
    printf("$MIPSBfmPli$Init: Could not find symbol %s "
           "in MDI Control Interface library %s.\n\t%s.",
           p_func_name, mdi_libname, dlerror());
    bfm_exit(0, NULL);
  }

  const MdiCalls * gspMdiCalls = p_mipssim_getmdicalls();
 
  // Connect to the MDI layer.
  
  return_status = 
    gsMdiServer->Open("mipsbfm", gspMdiCalls, bfm_print, bfm_exit);
  assert(return_status == MDISuccess);

  // Create CPU device and configure it.
  
  return_status = 
    gsMdiServer->OpenCore(bfm, core_id, "Carbon Bfm24K", "Bfm24K.cfg");
  assert(return_status == MDISuccess);

  if(bfm->MemIntfConnect() != 0) {
     MIPSServices_Fatal(*core_id,
         _("Could not connect BFM to Memory Interface."));

      bfm_exit(0, NULL);
  }

  // Install MDI clock callback in BFM.
  bfm->InstallRequestorClock(bfm_mdiclock, bfm_mdiclock, (void *) *core_id);

  // Startup ISS/CPU
  return_status = gsMdiServer->StartCore( *core_id );
  assert(return_status == MDISuccess);
}
