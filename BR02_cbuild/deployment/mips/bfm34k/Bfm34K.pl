
my $head = <<EOF;
cModuleBegin CarbonBfm34K

EOF

my $tail = <<EOF;
cModuleEnd

# Temp observability
# observeSignal *.* 
EOF

#observeSignal *.* *.*.* *.*.*.*

my $outfmt = <<EOF;
	cPortBegin %s
		cTiming %s SI_ClkIn
		cFanin *
	cPortEnd

EOF

my $clkfmt = <<EOF;
	cPortBegin %s
		cTiming rise SI_ClkIn
		cFanin *
		cTiming fall SI_ClkIn
		cFanin *
	cPortEnd

EOF

# SI_ClkOut <= 1;  // Drive Clock Out

my @clocks = qw(

  SI_ClkOut 
  CP2_gclk
  CP2_gfclk
  ISP_gclk
  ISP_gfclk
  IT_gclk
  IT_gfclk
  PM_gclk
  PM_gfclk
  SP_gclk
  SP_gfclk
  UDI_gclk
  UDI_gfclk

);

#   always @(posedge SI_ClkIn)
#
# internal signal
#  reg [63:0]              BFM_Cycle;
# $MIPSBfmPli$GetSignalChanged(BfmHandle, `Bfm34K_BFM_Cycle, BFM_Cycle);

my @posedge = qw(

  EJ_DebugM
  EJ_DebugM_1
  EJ_PerRst
  EJ_PrRst
  EJ_SRstE
  EJ_TDO
  EJ_TDOzstate
  gscanout
  IT_blk_grain
  IT_cmd
  IT_cmd_be
  IT_cmd_gsi
  IT_cmd_pa
  IT_cmd_tcid
  IT_cmd_wdata
  IT_greset_pre
  IT_gscanenable
  IT_perfcnt_tcen
  MB_dd_fail
  MB_done
  MB_dt_fail
  MB_dw_fail
  MB_frommbt
  MB_id_fail
  MB_isp_fail
  MB_it_fail
  MB_iw_fail
  MB_sp_fail
  MB_tr_fail
  OC_MAddrSpace
  OC_MAddr
  OC_MBurstLength
  OC_MBurstPrecise
  OC_MBurstSeq
  OC_MBurstSingleReq
  OC_MByteEn
  OC_MCmd
  OC_MData
  OC_MDataByteEn
  OC_MDataLast
  OC_MDataTagID
  OC_MDataValid
  OC_MReqInfo
  OC_MReset_n
  OC_MTagID
  PM_cp0_rd_ex
  PM_cp0_reg_er
  PM_cp0_reg_ex
  PM_cp0_rtc_ex
  PM_cp0_rvpe_ex
  PM_cp0_sel_er
  PM_cp0_sel_ex
  PM_cp0_wdata_er
  PM_cp0_wr_er
  PM_cp0_wtc_er
  PM_cp0_wvpe_er
  PM_greset_pre
  PM_gscanenable
  PM_perfcnt_tcen
  PM_relax_inst_issued
  PM_tc_fork
  PM_tc_inst_commited
  PM_tc_inst_issued
  PM_tc_ss
  PM_tc_state_0
  PM_tc_state_1
  PM_tc_state_2
  PM_tc_state_3
  PM_tc_state_4
  PM_tc_state_5
  PM_tc_state_6
  PM_tc_state_7
  PM_tc_state_8
  PM_vpe_dm
  PM_vpe_erl
  PM_vpe_exl
  PM_vpemap
  SI_Dbs
  SI_Dbs_1
  SI_ERL
  SI_ERL_1
  SI_EXL
  SI_EXL_1
  SI_IAck
  SI_IAck_1
  SI_Ibs
  SI_Ibs_1
  SI_IPL
  SI_IPL_1
  SI_OCPRatioLock
  SI_PCInt
  SI_PCInt_1
  SI_RP
  SI_RP_1
  SI_Sleep
  SI_SWInt
  SI_SWInt_1
  SI_TimerInt
  SI_TimerInt_1
  TC_Calibrate
  TC_ChipTrigOut
  TC_ClockRatio
  TC_Data
  TC_ProbeTrigOut
  TC_TrEnable
  TC_Valid
  UDI_age_xx
  UDI_endianb_xx
  UDI_gpr_wr_ack_ms
  UDI_greset_pre
  UDI_gscanenable
  UDI_ir_rf
  UDI_kd_mode_rf
  UDI_mt_tc_xx
  UDI_nullify_er
  UDI_nullify_ex
  UDI_nullify_ms
  UDI_nxt_opc_xx
  UDI_perfcnt_tcen
  UDI_rs_xx
  UDI_rt_xx
  UDI_run_er
  UDI_run_ex
  UDI_run_ms
  UDI_start_xx
  UHL_hilo_wren_xx
  UHL_hi_rd_data_xx
  UHL_lo_rd_data_xx

);

#   always @(negedge SI_ClkIn)

my @negedge = qw(

  CP2_as_0
  CP2_endian_0
  CP2_fordlim_0
  CP2_fs_0
  CP2_gscanenable
  CP2_inst32_0
  CP2_ir_0
  CP2_irenable_0
  CP2_kd_mode_0
  CP2_kill_0
  CP2_kills_0
  CP2_null_0
  CP2_nulls_0
  CP2_perfcnt_tcen
  CP2_reset
  CP2_targtcid_0
  CP2_tcid_0
  CP2_tdata_0
  CP2_tds_0
  CP2_torder_0
  CP2_ts_0
  CP2_vpeid_0
  CP2_vpemap
  ISP_addr_ipf
  ISP_core_reqpd_xx
  ISP_data_wdata_ipf
  ISP_data_wpar_ipf
  ISP_data_wr_ipf
  ISP_dma_rd_ipf
  ISP_greset_pre
  ISP_gscanenable
  ISP_mbisp_toisp_xx
  ISP_parity_en
  ISP_perfcnt_tcen
  ISP_rd_ipf
  ISP_sleep_req_xx
  ISP_tag_sel_ipf
  ISP_tag_wdata_ipf
  ISP_tag_wr_ipf
  ISP_tcid_ipf
  ISP_wait_pd_xx
  SP_core_reqpd_er
  SP_data_addr_ag
  SP_data_rd_ag
  SP_data_wdata_ag
  SP_data_wpar_ag
  SP_data_wr_ag
  SP_data_wren_ag
  SP_dma_rd_ag
  SP_greset_pre
  SP_gscanenable
  SP_mbsp_tosp_xx
  SP_parity_en
  SP_perfcnt_tcen
  SP_req_id_ag
  SP_sleep_req_xx
  SP_tag_rd_ag
  SP_tag_sel_ag
  SP_tag_wdata_ag
  SP_tag_wr_ag
  SP_wait_pd_xx

);

# -------------------- BEGIN : MAIN --------------------

print $head;
map( printf($clkfmt, $_),         @clocks);
map( printf($outfmt, $_, "rise"), @posedge);
map( printf($outfmt, $_, "fall"), @negedge);
print $tail;

# --------------------  END  : MAIN --------------------
