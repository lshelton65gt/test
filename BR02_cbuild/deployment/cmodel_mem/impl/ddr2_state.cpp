/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "ddr2_state.h"
#include "carbon_mem_core_hash.h"
#include <iostream>
#include <cassert>

carbon_ddr2_mem_state::carbon_ddr2_mem_state(UInt32 a_bits, UInt32 c_bits, UInt32 d_words, UInt32 d_bits, UInt32 b_bits, bool b_access)
    : addr_bits(a_bits),
      cols_bits(c_bits),
      data_words(d_words),
      data_bits(d_bits),
      bank_bits(b_bits),
      bank_access(b_access)
{
    if ((bank_bits != 2) && (bank_bits != 3))
	assert("Illegal number of banks in DDR2 model" == 0);

    cas_latency = 2;
    add_latency = 0;
    read_latency = 4;
    write_latency = 2;
    burst_length = 4;

    if (bank_access) {
	mem0 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
	mem1 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
	mem2 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
	mem3 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
	if (bank_bits == 3) {
	    mem4 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
	    mem5 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
	    mem6 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
	    mem7 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
	} else
	    mem4 = mem5 = mem6 = mem7 = 0;
    } else {
	mem0 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits + bank_bits), data_words, data_bits);
	mem1 = mem2 = mem3 = mem4 = mem5 = mem6 = mem7 = 0;
    }

    Dqs_out = Dqs_en = 0;

    Dq_out_saved = new UInt32[data_words];
    memset(Dq_out_saved, 0, data_words * sizeof(UInt32));
    
    row_addr0 = row_addr1 = row_addr2 = row_addr3 = row_addr4 = row_addr5 = row_addr6 = row_addr7 = 0;
    last_clk = last_strobe = 0;

    memset(pipe, 0, PIPE_LENGTH * sizeof(data_phase *));
}

carbon_ddr2_mem_state::~carbon_ddr2_mem_state()
{
    if (mem0)
	delete mem0;
    if (mem1)
	delete mem1;
    if (mem2)
	delete mem2;
    if (mem3)
	delete mem3;
    if (mem4)
	delete mem4;
    if (mem5)
	delete mem5;
    if (mem6)
	delete mem6;
    if (mem7)
	delete mem7;

    delete [] Dq_out_saved;
}

void carbon_ddr2_mem_state::posedge(const UInt32* Dq_in, UInt32* Dq_out, UInt32* Dq_en, const UInt32 Dqs_in,
				    const UInt32 Addr, const UInt32 Ba, const UInt32 Cs_n, const UInt32 Ras_n,
				    const UInt32 Cas_n, const UInt32 We_n, const UInt32 Dm)
{
    UInt32 full_addr, i;

    // decode command and add state to pipeline
    UInt32 command = (Cs_n << 3) | (Ras_n << 2) | (Cas_n << 1) | We_n;
    switch (command) {
    case LOAD_MODE:
	setConfig(Ba, Addr);
	break;
    case REFRESH:
	break;
    case PRECHARGE:
	break;
    case ACTIVE:
	switch (Ba) {
	case 0: row_addr0 = Addr; break;
	case 1: row_addr1 = Addr; break;
	case 2: row_addr2 = Addr; break;
	case 3: row_addr3 = Addr; break;
	case 4: row_addr4 = Addr; break;
	case 5: row_addr5 = Addr; break;
	case 6: row_addr6 = Addr; break;
	case 7: row_addr7 = Addr; break;
	}
	break;
    case WRITE:
	full_addr = buildFullAddr(Ba, Addr);
	for (i = 0; i < burst_length; ++i) {
	    addWrite(i + write_latency, !(i & 0x1), Ba, full_addr);
	    incrAddr(full_addr);
	}
	break;
    case READ:
	full_addr = buildFullAddr(Ba, Addr);
	for (i = 0; i < burst_length; ++i) {
	    addRead(i + read_latency, Ba, full_addr);
	    incrAddr(full_addr);
	}
	break;
    case TERMINATE:
	break;
    case NOP:
    default:
	break;
    }

    doData(Dq_in, Dq_out, Dq_en);
}

void carbon_ddr2_mem_state::negedge(const UInt32* Dq_in, UInt32* Dq_out, UInt32* Dq_en, const UInt32 Dqs_in,
				    const UInt32 Addr, const UInt32 Ba, const UInt32 Cs_n, const UInt32 Ras_n,
				    const UInt32 Cas_n, const UInt32 We_n, const UInt32 Dm)
{
    doData(Dq_in, Dq_out, Dq_en);
}

void carbon_ddr2_mem_state::addRead(UInt32 stage, UInt32 bank, UInt32 addr)
{
    data_phase *phase = new data_phase;
    phase->read = true;
    phase->edge = false;	// doesn't matter for reads
    phase->bank = bank;
    phase->addr = addr;

    // kill old entry
    if (pipe[stage])
	delete pipe[stage];
    pipe[stage] = phase;
}

void carbon_ddr2_mem_state::addWrite(UInt32 stage, bool edge, UInt32 bank, UInt32 addr)
{
    data_phase *phase = new data_phase;
    phase->read = false;
    phase->edge = edge;
    phase->bank = bank;
    phase->addr = addr;

    // kill old entry
    if (pipe[stage])
	delete pipe[stage];
    pipe[stage] = phase;
}

void carbon_ddr2_mem_state::doData(const UInt32 *Dq_in, UInt32* Dq_out, UInt32* Dq_en)
{
    // Drive read data or float bus, based on head of pipe
    if (pipe[0] && pipe[0]->read) {
	*Dq_en = 1;
	readMemory(pipe[0]->bank, pipe[0]->addr, Dq_out);
    } else {
	*Dq_en = 0;
	memset(Dq_out, 0, data_words * sizeof(UInt32));
    }

    // Save Dq for dummy output on strobe call
    memcpy(Dq_out_saved, Dq_out, data_words * sizeof(UInt32));

}

void carbon_ddr2_mem_state::genStrobes(UInt32 clk)
{
    // Set Dqs out/enables
    if (pipe[0]) {
	if (pipe[0]->read) {
	    Dqs_en = 1;
	    Dqs_out = clk;
	} else {
	    Dqs_en = 0;
	    Dqs_out = 0;
	}
    } else {
	if (pipe[2] && pipe[2]->read) {
	    Dqs_en = 1;
	    Dqs_out = 0;
	} else {
	    Dqs_en = 0;
	    Dqs_out = 0;
	}
    }
}

void carbon_ddr2_mem_state::doStrobes(const UInt32 *Dq_in, UInt32 Dqs_in, UInt32 Dm, UInt32 *Dq_out)
{
    // Copy the saved Dq to the dummy output
    memcpy(Dq_out, Dq_out_saved, data_words * sizeof(UInt32));

    // Maybe do a write...
    // We need to look at the current pipe stage, and
    // maybe the one after, since the write can start on different
    // data phases depending on strobe latencies
    UInt32 pipe_stage;
    if (pipe[0] && !(pipe[0]->read))		// late strobe
	pipe_stage = 0;
    else if (pipe[1] && !(pipe[1]->read)) {	// early strobe - haven't actually advanced to that phase yet
	// Only do this if the edge matches!
	if ((Dqs_in && pipe[1]->edge) ||
	    (!Dqs_in && !pipe[1]->edge))
	    pipe_stage = 1;
	else
	    return;
    } else
	return;					// no possible write pending - must've been a read

    // Actually do the write
    writeMemory(pipe[pipe_stage]->bank, pipe[pipe_stage]->addr, Dq_in, Dm);

    // Clear the pipe stage so we don't see it next time through,
    // and save the current strobe value
    delete pipe[pipe_stage];
    pipe[pipe_stage] = 0;
    last_strobe = Dqs_in;
}

void carbon_ddr2_mem_state::doPipe()
{
    UInt32 i;

    // Step the pipe
    if (pipe[0])
	delete pipe[0];

    for (i = 0; i < PIPE_LENGTH - 1; ++i)
	pipe[i] = pipe[i + 1];
    pipe[PIPE_LENGTH - 1] = 0;
}

UInt32 carbon_ddr2_mem_state::buildFullAddr(UInt32 bank, UInt32 col_addr)
{
    UInt32 row_addr, full_addr;
    switch (bank) {
    case 0: row_addr = row_addr0; break;
    case 1: row_addr = row_addr1; break;
    case 2: row_addr = row_addr2; break;
    case 3: row_addr = row_addr3; break;
    case 4: row_addr = row_addr4; break;
    case 5: row_addr = row_addr5; break;
    case 6: row_addr = row_addr6; break;
    case 7: row_addr = row_addr7; break;
    default: row_addr = 0;
    }

    // Build full address as {row_addr, col_addr}
    full_addr = (row_addr << cols_bits);

    // Need to manipulate column bits, since bit 10 is the precharge
    // bit and not actually part of the address.
    // Build the mask correctly based on number of bits
    UInt32 upper_col_mask = ((1 << cols_bits) - 1) & 0xfffffc00;	// bits [N:10]
    UInt32 lower_col_mask = ((1 << cols_bits) - 1) & 0x3ff;		// bits [9:0]
    full_addr |= ((col_addr >> 1) & upper_col_mask);
    full_addr |= (col_addr & lower_col_mask);
    
    return full_addr;
}

void carbon_ddr2_mem_state::incrAddr(UInt32 &addr)
{
    // Increment address, with proper wrapping
    // Only handles sequential ordering for now!
    UInt32 mask = burst_length - 1;
    if ((addr & mask) == mask)
	addr = addr & ~mask;	// wrap to beginning of block
    else
	++addr;
}

bool carbon_ddr2_mem_state::readMemory(UInt32 bank, UInt32 addr, UInt32 *data)
{
    carbon_mem_core *mem;

    if (bank_access) {
	switch (bank) {
	case 0: mem = mem0; break;
	case 1: mem = mem1; break;
	case 2: mem = mem2; break;
	case 3: mem = mem3; break;
	case 4: mem = mem4; break;
	case 5: mem = mem5; break;
	case 6: mem = mem6; break;
	case 7: mem = mem7; break;
	default: mem = 0;
	}
    } else {
	// one big memory - add the bank bits at the top of the memory
	mem = mem0;
	addr |= bank << (addr_bits + cols_bits);
    }
    
    if (mem == 0)
	return false;

    return (mem->read(addr, data));
}

bool carbon_ddr2_mem_state::writeMemory(UInt32 bank, UInt32 addr, const UInt32 *data, UInt32 mask)
{
    carbon_mem_core *mem;

    if (bank_access) {
	switch (bank) {
	case 0: mem = mem0; break;
	case 1: mem = mem1; break;
	case 2: mem = mem2; break;
	case 3: mem = mem3; break;
	case 4: mem = mem4; break;
	case 5: mem = mem5; break;
	case 6: mem = mem6; break;
	case 7: mem = mem7; break;
	default: mem = 0;
	}
    } else {
	// one big memory - add the bank bits at the top of the memory
	mem = mem0;
	addr |= bank << (addr_bits + cols_bits);
    }
    
    if (mem == 0)
	return false;
    
    if (mask == 0)// full write
	return (mem->write(addr, data));
    else {	// masked write
	UInt32 *temp = new UInt32[mem->getWordSize()];
	if (!(mem->read(addr, temp)))
	    return false;
	for (UInt32 i = 0; i < mem->getWordSize(); ++i) {
	    UInt32 byte_mask = 0;
	    for (int j = 0; j < 4; ++j)	// set 8 bits of byte_mask for every bit in original mask that is 0
		if (!(mask & (0x1 << (i * 4 + j))))
		    byte_mask |= (0xff << (j * 8));

	    // mask and save data
	    temp[i] = (temp[i] & ~byte_mask) | (data[i] & byte_mask);
	}
	bool retval = mem->write(addr, temp);
	delete [] temp;
	return retval;
    }
}

void carbon_ddr2_mem_state::setConfig(UInt32 bank, UInt32 addr)
{
    if (bank == 0) {
	// Mode Register
	switch (addr & 0x7) {		// Burst Length
	case 2: burst_length = 4; break;
	case 3: burst_length = 8; break;
	}

	if (addr & 0x8)			// Burst Type
	    assert("Interleaved burst type is not currently supported - contact Carbon Design Systems support." == 0);

	cas_latency = ((addr >> 4) & 7);	// CAS Latency
    }

    if (bank == 1) {
	// Extended Mode Register
	add_latency = ((addr >> 3) & 0x7);	// Additive Latency

// 	if ((addr & 0x400) == 0)		// Complement Query Strobe (0 == enable)
// 	    assert("Complement Query Strobe is not currently supported - contact Carbon Design Systems support." == 0);

	if (addr & 0x800)		// Read Data Strobe Output
	    assert("Read Data Strobe Output is not currently supported - contact Carbon Design Systems support." == 0);
    }

    // Recalculate R/W latency
    // These are counted in half cycles
    read_latency = (add_latency + cas_latency) * 2;
    write_latency = read_latency - 2;
}
