/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __DDR2_STATE_H_
#define __DDR2_STATE_H_

#include "carbon/carbon.h"

class carbon_mem_core;

// class for internals of c-models
class carbon_ddr2_mem_state
{
public:
    carbon_ddr2_mem_state(UInt32 a_bits, UInt32 c_bits, UInt32 d_words, UInt32 d_bits, UInt32 b_bits, bool b_access);
    ~carbon_ddr2_mem_state();

    void posedge(const UInt32* Dq_in, UInt32* Dq_out, UInt32* Dq_en, const UInt32 Dqs_in,
		 const UInt32 Addr, const UInt32 Ba, const UInt32 Cs_n, const UInt32 Ras_n,
		 const UInt32 Cas_n, const UInt32 We_n, const UInt32 Dm);

    void negedge(const UInt32* Dq_in, UInt32* Dq_out, UInt32* Dq_en, const UInt32 Dqs_in,
		 const UInt32 Addr, const UInt32 Ba, const UInt32 Cs_n, const UInt32 Ras_n,
		 const UInt32 Cas_n, const UInt32 We_n, const UInt32 Dm);


    struct data_phase
    {
	bool read;
	bool edge;
	UInt32 bank;
	UInt32 addr;
    };

    enum {
	LOAD_MODE,
	REFRESH,
	PRECHARGE,
	ACTIVE,
	WRITE,
	READ,
	TERMINATE,
	NOP
    };

    enum {
	ASYNC,
	POSEDGE,
	NEGEDGE
    };

    UInt32 addr_bits, cols_bits, data_words, data_bits, bank_bits, bank_access;
    carbon_mem_core *mem0, *mem1, *mem2, *mem3, *mem4, *mem5, *mem6, *mem7;
    UInt32 Dqs_out, Dqs_en;
    UInt32 *Dq_out_saved;
    UInt32 row_addr0, row_addr1, row_addr2, row_addr3, row_addr4, row_addr5, row_addr6, row_addr7;

    UInt32 cas_latency;
    UInt32 add_latency;
    UInt32 read_latency;
    UInt32 write_latency;
    UInt32 burst_length;
    //    UInt32 burst_type;
    UInt32 last_clk, last_strobe;

    // Number of pipe stages we can track
    // Should be max burst length (8) +
    // max read latency (in half cycles) (18)
    // 30 should be good
    static const UInt32 PIPE_LENGTH = 30;
    data_phase *pipe[PIPE_LENGTH];

    void addRead(UInt32 stage, UInt32 bank, UInt32 addr);
    void addWrite(UInt32 stage, bool edge, UInt32 bank, UInt32 addr);
    void doData(const UInt32 *Dq_in, UInt32* Dq_out, UInt32* Dq_en);
    void genStrobes(UInt32 clk);
    void doStrobes(const UInt32 *Dq_in, UInt32 Dqs_in, UInt32 Dm, UInt32 *Dq_out);
    void doPipe();
    UInt32 buildFullAddr(UInt32 bank, UInt32 col_addr);
    void incrAddr(UInt32 &addr);
    bool readMemory(UInt32 bank, UInt32 addr, UInt32 *data);
    bool writeMemory(UInt32 bank, UInt32 addr, const UInt32 *data, UInt32 mask = 0x0);
    void setConfig(UInt32 bank, UInt32 addr);
};

#endif
