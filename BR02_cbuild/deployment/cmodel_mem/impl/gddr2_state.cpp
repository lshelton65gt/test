/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "gddr2_state.h"
#include "carbon_mem_core_hash.h"
#include <iostream>
#include <cassert>

/*
** This gddr2 model is based on the spec for the samsung K4N26323AE-GC
** The implementation here may be specific to that part, but I don't know
** of any other to compare it to...
**
** While this part is called a GDDR2 part, it seems to be derived from DDR, NOT DDR2
*/
carbon_gddr2_mem_state::carbon_gddr2_mem_state(UInt32 a_bits, UInt32 c_bits, UInt32 d_words, UInt32 d_bits)
    : addr_bits(a_bits),
      cols_bits(c_bits),
      data_words(d_words),
      data_bits(d_bits)
{
    cas_latency = 5;
    burst_length = 2;

    mem0 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
    mem1 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
    mem2 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);
    mem3 = new carbon_mem_core_hash(1 << (addr_bits + cols_bits), data_words, data_bits);

    Dqs_out = Dqs_en = 0;

    Dq_out_saved = new UInt32[data_words];
    memset(Dq_out_saved, 0, data_words * sizeof(UInt32));
    
    row_addr0 = row_addr1 = row_addr2 = row_addr3 = 0;
    last_clk = last_strobe = 0;

    memset(pipe, 0, PIPE_LENGTH * sizeof(data_phase *));
}

carbon_gddr2_mem_state::~carbon_gddr2_mem_state()
{
    delete mem0;
    delete mem1;
    delete mem2;
    delete mem3;
    delete [] Dq_out_saved;
}

void carbon_gddr2_mem_state::posedge(const UInt32* Dq_in, UInt32* Dq_out, UInt32* Dq_en, const UInt32 Dqs_in,
				   const UInt32 Addr, const UInt32 Ba, const UInt32 Cs_n, const UInt32 Ras_n,
				   const UInt32 Cas_n, const UInt32 We_n, const UInt32 Dm)
{
    UInt32 full_addr, i;

    // decode command and add state to pipeline
    UInt32 command = (Cs_n << 3) | (Ras_n << 2) | (Cas_n << 1) | We_n;
    switch (command) {
    case LOAD_MODE:
	if (Ba == 0) {
	    setMRS(Addr);
	} else if (Ba == 1) {
	    setEMRS(Addr);
	} else {
	    assert("Carbon GDDR2: Invalid Ba1 == 1 on MRS / EMRS write" == 0);
	}

	break;
    case REFRESH:
	break;
    case PRECHARGE:
	break;
    case ACTIVE:
	switch (Ba) {
	case 0: row_addr0 = Addr; break;
	case 1: row_addr1 = Addr; break;
	case 2: row_addr2 = Addr; break;
	case 3: row_addr3 = Addr; break;
	}
	break;
    case WRITE:
	full_addr = buildFullAddr(Ba, Addr);
	for (i = 0; i < burst_length; ++i) {
	    // Two stages later is the latest the write strobes can occur,
	    // but it might actually be only one.  The write-processing function
	    // will take care of this.
	    addWrite(i + 2 +mAdditiveLatency, Ba, full_addr, (i % 2) == 0);
	    incrAddr(full_addr);
	}
	break;
    case READ:
	full_addr = buildFullAddr(Ba, Addr);
	for (i = 0; i < burst_length; ++i) {
	    addRead(i + cas_latency + mAdditiveLatency, Ba, full_addr, (i % 2) == 0);
	    incrAddr(full_addr);
	}
	break;
    case TERMINATE:
	break;
    case NOP:
    default:
	break;
    }

    doData(Dq_in, Dq_out, Dq_en);
}


void carbon_gddr2_mem_state::negedge(const UInt32* Dq_in, UInt32* Dq_out, UInt32* Dq_en, const UInt32 Dqs_in,
				   const UInt32 Addr, const UInt32 Ba, const UInt32 Cs_n, const UInt32 Ras_n,
				   const UInt32 Cas_n, const UInt32 We_n, const UInt32 Dm)
{
    doData(Dq_in, Dq_out, Dq_en);
}

void carbon_gddr2_mem_state::addRead(UInt32 stage, UInt32 bank, UInt32 addr, bool edge)
{
    data_phase *phase = new data_phase;
    phase->read = true;
    phase->bank = bank;
    phase->addr = addr;
    phase->posedge = edge;

    // kill old entry
    if (pipe[stage])
	delete pipe[stage];
    pipe[stage] = phase;
}

void carbon_gddr2_mem_state::addWrite(UInt32 stage, UInt32 bank, UInt32 addr, bool edge)
{
    data_phase *phase = new data_phase;
    phase->read = false;
    phase->bank = bank;
    phase->addr = addr;
    phase->posedge = edge;

    // kill old entry
    if (pipe[stage])
	delete pipe[stage];
    pipe[stage] = phase;
}

void carbon_gddr2_mem_state::doData(const UInt32 *Dq_in, UInt32* Dq_out, UInt32* Dq_en)
{
    // Drive read data or float bus, based on head of pipe
    if (pipe[0] && pipe[0]->read) {
	*Dq_en = 1;
	readMemory(pipe[0]->bank, pipe[0]->addr, Dq_out);
    } else {
	*Dq_en = 0;
	memset(Dq_out, 0, data_words * sizeof(UInt32));
    }

    // Save Dq for dummy output on strobe call
    memcpy(Dq_out_saved, Dq_out, data_words * sizeof(UInt32));

}

void carbon_gddr2_mem_state::genStrobes()
{
    // Set Dqs out/enables
    if (pipe[0]) {
	if (pipe[0]->read) {
	    Dqs_en = 1;
	    Dqs_out = pipe[0]->posedge;
	} else {
	    Dqs_en = 0;
	    Dqs_out = 0;
	}
    } else {
	if (pipe[2] && pipe[2]->read) {
	    Dqs_en = 1;
	    Dqs_out = 0;
	} else {
	    Dqs_en = 0;
	    Dqs_out = 0;
	}
    }
}

void carbon_gddr2_mem_state::doStrobes(const UInt32 *Dq_in, UInt32 Dqs_in, UInt32 Dm, UInt32 *Dq_out)
{
    // Copy the saved Dq to the dummy output
    memcpy(Dq_out, Dq_out_saved, data_words * sizeof(UInt32));

    // Maybe do a write...
    // We need to look at the current pipe stage, and
    // maybe the one after, since the write can start on different
    // data phases depending on strobe latencies
    UInt32 pipe_stage;
    if (pipe[0] && !(pipe[0]->read))		// late strobe
	pipe_stage = 0;
    else if (pipe[1] && !(pipe[1]->read))	// early strobe - haven't actually advanced to that phase yet
	pipe_stage = 1;
    else
	return;					// no possible write pending - must've been a read

    // Actually do the write
    writeMemory(pipe[pipe_stage]->bank, pipe[pipe_stage]->addr, Dq_in, Dm);

    // Clear the pipe stage so we don't see it next time through,
    // and save the current strobe value
    delete pipe[pipe_stage];
    pipe[pipe_stage] = 0;
    last_strobe = Dqs_in;
}

void carbon_gddr2_mem_state::doPipe()
{
    UInt32 i;

    // Step the pipe
    if (pipe[0])
	delete pipe[0];

    for (i = 0; i < PIPE_LENGTH - 1; ++i)
	pipe[i] = pipe[i + 1];
    pipe[PIPE_LENGTH - 1] = 0;
}

UInt32 carbon_gddr2_mem_state::buildFullAddr(UInt32 bank, UInt32 col_addr)
{
    UInt32 row_addr, full_addr;
    switch (bank) {
    case 0: row_addr = row_addr0; break;
    case 1: row_addr = row_addr1; break;
    case 2: row_addr = row_addr2; break;
    case 3: row_addr = row_addr3; break;
    default: row_addr = 0;
    }

    full_addr = (row_addr << cols_bits);	// bits 24:12
    full_addr |= ((col_addr >> 1) & 0xc00);	// bits 11:10
    full_addr |= (col_addr & 0x3ff);		// bits 9:0
    
    return full_addr;
}

void carbon_gddr2_mem_state::incrAddr(UInt32 &addr)
{
    // Increment address, with proper wrapping
    // Only handles sequential ordering for now!
    UInt32 mask = burst_length - 1;
    if ((addr & mask) == mask)
	addr = addr & ~mask;	// wrap to beginning of block
    else
	++addr;
}

bool carbon_gddr2_mem_state::readMemory(UInt32 bank, UInt32 addr, UInt32 *data)
{
    carbon_mem_core *mem;

    switch (bank) {
    case 0: mem = mem0; break;
    case 1: mem = mem1; break;
    case 2: mem = mem2; break;
    case 3: mem = mem3; break;
    default: mem = 0;
    }
    
    if (mem == 0)
	return false;

    return (mem->read(addr, data));
}

bool carbon_gddr2_mem_state::writeMemory(UInt32 bank, UInt32 addr, const UInt32 *data, UInt32 mask)
{
    carbon_mem_core *mem;

    switch (bank) {
    case 0: mem = mem0; break;
    case 1: mem = mem1; break;
    case 2: mem = mem2; break;
    case 3: mem = mem3; break;
    default: mem = 0;
    }
    
    if (mem == 0)
	return false;
    
    if (mask == 0)// full write
	return (mem->write(addr, data));
    else {	// masked write
	UInt32 *temp = new UInt32[mem->getWordSize()];
	if (!(mem->read(addr, temp)))
	    return false;
	for (UInt32 i = 0; i < mem->getWordSize(); ++i) {
	    UInt32 byte_mask = 0;
	    for (int j = 0; j < 4; ++j)	// set 8 bits of byte_mask for every bit in original mask that is 0
		if (!(mask & (0x1 << (i * 4 + j))))
		    byte_mask |= (0xff << (j * 8));

	    // mask and save data
	    temp[i] = (temp[i] & ~byte_mask) | (data[i] & byte_mask);
	}
	bool retval = mem->write(addr, temp);
	delete [] temp;
	return retval;
    }
}


/* Decode writes to the MRS register.  Ignore settings that are not useful, but
 * keep an eye out for things that are invalid.
 */
void carbon_gddr2_mem_state::setMRS(UInt32 Addr) 
{
    /* A[2:0] BURST LENGTH */
    switch (Addr & 0x7) {		// Burst Length
	/* Gddr2 require burst length = 4 */
	// case 1: burst_length = 2; break;
    case 2: burst_length = 4; break;
	// case 3: burst_length = 8; break;
    default:
	assert("Carbon GDDR2: Invalid burst length" == 0) ;
    }

    /* A[3] Burst type.  This part only supports 0 */
    if (Addr & 0x8)
	assert("Carbon GDDR2: invalid burst type 1 (interleaved)" == 0);

    /* A[6:4] */
    switch ((Addr >> 4) & 7) { 		// CAS Latency
	/* The samsung part supports 5,6,7
	** alos, the mapping shown here is different than the jedec spec
	*/		
    case 5: 
	cas_latency = 10; 
	break;
    case 6: 
	cas_latency = 12; 
	break;
    case 7: 
	cas_latency = 14; 
	break;
    default:
	assert("Carbon GDDR2: bad cas latency" == 0);
    }
	    
    /* A8 MBZ - diff than ddr spec where it controls dll */
    if(Addr & 0x100) 
	assert("Carbon GDDR2: A8 MBZ for MRS register" == 0);

    /* A[11:9] tWr.  these bits are reserved in DDR */
    switch((Addr >> 9) & 0x7) {
    case 2: mtWr = 3;
    case 3: mtWr = 4;
    case 4: mtWr = 5;
    default:
	assert("Carbon GDDR2: invalid tWR setting");
    }	    

}


/* Decode EMRS register write.  Ignore things that are electrical or 
** sub cycle timing except to check for validity.
**
** None of the samsung values match the DDR spec
*/
void carbon_gddr2_mem_state::setEMRS(UInt32 Addr) 
{
    /* Addr[1:0]  ODT option - doesn't affect a behavioral model... */    
    /* Addr[3:2]  ODT Control- doesn't affect behavioral models...  */
    /* Addr[4]    Additive latence */

    mAdditiveLatency = (Addr & 0x8) >> 4; // Needs to be 2x?
    
    /* Addr[5]    DQS */
    mDqsType = (Addr & (0x1 << 5)) >> 5;
    
    /* the rest of the bist do not effect behavioral models */

    /* Addr[6]    DLL */
    /* Addr[9:7]  Output Drive Strength */
    /* Addr[10]   ODT of DQs @ Rd */
}
