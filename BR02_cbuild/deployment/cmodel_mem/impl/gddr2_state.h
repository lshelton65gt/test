/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __GDDR_STATE_H_
#define __GDDR2_STATE_H_

#include "carbon/carbon.h"

class carbon_mem_core;

// class for internals of c-models
class carbon_gddr2_mem_state
{
public:
    carbon_gddr2_mem_state(UInt32 a_bits, UInt32 c_bits, UInt32 d_words, UInt32 d_bits);
    ~carbon_gddr2_mem_state();

    void posedge(const UInt32* Dq_in, UInt32* Dq_out, UInt32* Dq_en, const UInt32 Dqs_in,
		 const UInt32 Addr, const UInt32 Ba, const UInt32 Cs_n, const UInt32 Ras_n,
		 const UInt32 Cas_n, const UInt32 We_n, const UInt32 Dm);

    void negedge(const UInt32* Dq_in, UInt32* Dq_out, UInt32* Dq_en, const UInt32 Dqs_in,
		 const UInt32 Addr, const UInt32 Ba, const UInt32 Cs_n, const UInt32 Ras_n,
		 const UInt32 Cas_n, const UInt32 We_n, const UInt32 Dm);


    struct data_phase
    {
	bool read;
	bool posedge;
	UInt32 bank;
	UInt32 addr;
    };

#if 0
    enum {
	LOAD_MODE,
	REFRESH,
	PRECHARGE,
	ACTIVE,
	WRITE,
	READ,
	TERMINATE,
	NOP
    };
#else
    static const UInt32 LOAD_MODE = 0;
    static const UInt32 REFRESH   = 1;
    static const UInt32 PRECHARGE = 2;
    static const UInt32 ACTIVE    = 3;
    static const UInt32 WRITE     = 4;
    static const UInt32 READ      = 5;
    static const UInt32	TERMINATE = 6;
    static const UInt32 NOP       = 7;
#endif

    enum {
	ASYNC,
	POSEDGE,
	NEGEDGE
    };

    static const UInt32 DQS_DIFFERENTIAL = 0;
    static const UInt32 DQS_SINGLE = 1;

    UInt32 addr_bits, cols_bits, data_words, data_bits;
    carbon_mem_core *mem0, *mem1, *mem2, *mem3;
    UInt32 Dqs_out, Dqs_en;
    UInt32 *Dq_out_saved;
    UInt32 row_addr0, row_addr1, row_addr2, row_addr3;

    /* NOTE: latencies in this model are 2x the values that get programed in the spec
    ** this is because the spec is thinking in terms of the DDR clk cycles, where
    ** as we are just counting edges because integers are nice.
    */
    UInt32 cas_latency;        
    UInt32 burst_length;
    UInt32 last_clk, last_strobe;

    UInt32 mtWr;
    UInt32 mAdditiveLatency;
    UInt32 mDqsType;

    static const UInt32 PIPE_LENGTH = 15;
    data_phase *pipe[PIPE_LENGTH];

    void addRead(UInt32 stage, UInt32 bank, UInt32 addr, bool edge);
    void addWrite(UInt32 stage, UInt32 bank, UInt32 addr, bool edge);
    void doData(const UInt32 *Dq_in, UInt32* Dq_out, UInt32* Dq_en);
    void genStrobes();
    void doStrobes(const UInt32 *Dq_in, UInt32 Dqs_in, UInt32 Dm, UInt32 *Dq_out);
    void doPipe();
    UInt32 buildFullAddr(UInt32 bank, UInt32 col_addr);
    void incrAddr(UInt32 &addr);
    bool readMemory(UInt32 bank, UInt32 addr, UInt32 *data);
    bool writeMemory(UInt32 bank, UInt32 addr, const UInt32 *data, UInt32 mask = 0x0);

    /* Decode MRS register */
    void setMRS(UInt32 Addr);
    /* Decode EMRS register */
    void setEMRS(UInt32 Addr);

};

#endif
