/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "cds_libdesign_carbon_fifo_dc.h"
#include "cds_libdesign_carbon_fifo_sc.h"
#include "cds_libdesign_carbon_fifo_dc_sa.h"
#include "cds_libdesign_carbon_fifo_sc_sa.h"
#include <queue>	// yes, I'm that lazy...

//------------------------
// carbon_fifo_sc
//------------------------

class carbon_fifo_sc_state
{
public:
    carbon_fifo_sc_state(UInt32 depth, UInt32 data_width, UInt32 used_width, UInt32 almost_full, UInt32 almost_empty)
	: DEPTH(depth), DATA_WIDTH(data_width), USED_WIDTH(used_width), ALMOST_FULL(almost_full), ALMOST_EMPTY(almost_empty)
    {
	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;

	data_out = new UInt32[DATA_WIDTH_32];
	memset(data_out, 0, DATA_WIDTH_32 * sizeof(UInt32));
    }
    ~carbon_fifo_sc_state()
    {
	delete [] data_out;
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 USED_WIDTH;
    UInt32 ALMOST_FULL;
    UInt32 ALMOST_EMPTY;
    UInt32 DATA_WIDTH_32;
    std::queue<UInt32*> q;
    UInt32 *data_out;
};

extern __C__ void* cds_carbon_fifo_sc_create(int numParams, CModelParam* cmodelParams, const char* inst)
{
    carbon_fifo_sc_state *state;
    if (numParams == 0)
	state = new carbon_fifo_sc_state(8, 8, 3, 7, 1);
    else
	state = new carbon_fifo_sc_state(atoi(cmodelParams[0].paramValue),
					 atoi(cmodelParams[1].paramValue),
					 atoi(cmodelParams[2].paramValue),
					 atoi(cmodelParams[3].paramValue),
					 atoi(cmodelParams[4].paramValue));

    return static_cast<void*>(state);
}

extern __C__ void cds_carbon_fifo_sc_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
}

extern __C__ void cds_carbon_fifo_sc_run(void* hndl, CDScarbon_fifo_scContext context
		, const UInt32* clk // Input, size = 1 word(s)
		, const UInt32* rst // Input, size = 1 word(s)
		, const UInt32* data_in // Input, size = 1 word(s)
		, const UInt32* push // Input, size = 1 word(s)
		, const UInt32* pop // Input, size = 1 word(s)
		, UInt32* data_out // Output, size = 3 word(s)
		, UInt32* used // Output, size = 1 word(s)
		, UInt32* full // Output, size = 1 word(s)
		, UInt32* empty // Output, size = 1 word(s)
		, UInt32* a_full // Output, size = 1 word(s)
		, UInt32* a_empty // Output, size = 1 word(s)
	)
{
    carbon_fifo_sc_state *state = static_cast<carbon_fifo_sc_state*>(hndl);

    if (*rst) {
	while (!(state->q.empty())) {	// what, no erase() ?
	    UInt32 *data = state->q.front();
	    delete [] data;
	    state->q.pop();
	}
	memset(state->data_out, 0, state->DATA_WIDTH_32 * sizeof(UInt32));
    } else {
	if (*push) {
	    UInt32 *data = new UInt32[state->DATA_WIDTH_32];
	    memcpy(data, data_in, state->DATA_WIDTH_32 * sizeof(UInt32));
	    state->q.push(data);
	}
	if (*pop) {
	    if (!(state->q.empty())) {
		UInt32 *data = state->q.front();
		memcpy(state->data_out, data, state->DATA_WIDTH_32 * sizeof(UInt32));
		delete [] data;
		state->q.pop();
	    }
	}
    }

    memcpy(data_out, state->data_out, state->DATA_WIDTH_32 * sizeof(UInt32));
    *used = state->q.size();
    *full = (state->q.size() == state->DEPTH);
    *empty = state->q.empty();
    *a_full = (state->q.size() == state->ALMOST_FULL);
    *a_empty = (state->q.size() == state->ALMOST_EMPTY);
}

extern __C__ void cds_carbon_fifo_sc_destroy(void* hndl)
{
    carbon_fifo_sc_state *state = static_cast<carbon_fifo_sc_state*>(hndl);
    delete state;
}

//------------------------
// carbon_fifo_dc
//------------------------

class carbon_fifo_dc_state
{
public:
    carbon_fifo_dc_state(UInt32 depth, UInt32 data_width, UInt32 used_width)
	: DEPTH(depth), DATA_WIDTH(data_width), USED_WIDTH(used_width)
    {
	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;

	data_out = new UInt32[DATA_WIDTH_32];
	memset(data_out, 0, DATA_WIDTH_32 * sizeof(UInt32));
    }
    ~carbon_fifo_dc_state()
    {
	delete [] data_out;
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 USED_WIDTH;
    UInt32 DATA_WIDTH_32;
    std::queue<UInt32*> q;
    UInt32 *data_out;
};

extern __C__ void* cds_carbon_fifo_dc_create(int numParams, CModelParam* cmodelParams, const char* inst)
{
    carbon_fifo_dc_state *state;
    if (numParams == 0)
	state = new carbon_fifo_dc_state(8, 8, 3);
    else
	state = new carbon_fifo_dc_state(atoi(cmodelParams[0].paramValue),
					 atoi(cmodelParams[1].paramValue),
					 atoi(cmodelParams[2].paramValue));
    return static_cast<void*>(state);
}

extern __C__ void cds_carbon_fifo_dc_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
}

extern __C__ void cds_carbon_fifo_dc_run(void* hndl, CDScarbon_fifo_dcContext context
		, const UInt32* clkr // Input, size = 1 word(s)
		, const UInt32* clkw // Input, size = 1 word(s)
		, const UInt32* rst // Input, size = 1 word(s)
		, const UInt32* data_in // Input, size = 3 word(s)
		, const UInt32* push // Input, size = 1 word(s)
		, const UInt32* pop // Input, size = 1 word(s)
		, UInt32* data_out // Output, size = 3 word(s)
		, UInt32* used_r // Output, size = 1 word(s)
		, UInt32* used_w // Output, size = 1 word(s)
		, UInt32* full_r // Output, size = 1 word(s)
		, UInt32* full_w // Output, size = 1 word(s)
		, UInt32* empty_r // Output, size = 1 word(s)
		, UInt32* empty_w // Output, size = 1 word(s)
	)
{
    carbon_fifo_dc_state *state = static_cast<carbon_fifo_dc_state*>(hndl);

    if (*rst) {
	while (!(state->q.empty())) {	// what, no erase() ?
	    UInt32 *data = state->q.front();
	    delete [] data;
	    state->q.pop();
	}
	memset(state->data_out, 0, state->DATA_WIDTH_32 * sizeof(UInt32));
    }

    switch (context) {
    case eCDScarbon_fifo_dcRiseclkw:
	if (!(*rst) && *push) {
	    UInt32 *data = new UInt32[state->DATA_WIDTH_32];
	    memcpy(data, data_in, state->DATA_WIDTH_32 * sizeof(UInt32));
	    state->q.push(data);
	}

	*used_w = state->q.size();
	*full_w = (state->q.size() == state->DEPTH);
	*empty_w = state->q.empty();
	break;

    case eCDScarbon_fifo_dcRiseclkr:
	if (!(*rst) && *pop) {
	    if (!(state->q.empty())) {
		UInt32 *data = state->q.front();
		memcpy(state->data_out, data, state->DATA_WIDTH_32 * sizeof(UInt32));
		delete [] data;
		state->q.pop();
	    }
	}

	memcpy(data_out, state->data_out, state->DATA_WIDTH_32 * sizeof(UInt32));
	*used_r = state->q.size();
	*full_r = (state->q.size() == state->DEPTH);
	*empty_r = state->q.empty();
    }
}

extern __C__ void cds_carbon_fifo_dc_destroy(void* hndl)
{
    carbon_fifo_dc_state *state = static_cast<carbon_fifo_dc_state*>(hndl);
    delete state;
}

//------------------------
// carbon_fifo_sc_sa
//------------------------

class carbon_fifo_sc_sa_state
{
public:
    carbon_fifo_sc_sa_state(UInt32 depth, UInt32 data_width, UInt32 used_width, UInt32 almost_full, UInt32 almost_empty)
	: DEPTH(depth), DATA_WIDTH(data_width), USED_WIDTH(used_width), ALMOST_FULL(almost_full), ALMOST_EMPTY(almost_empty)
    {
	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;
    }
    ~carbon_fifo_sc_sa_state()
    {
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 USED_WIDTH;
    UInt32 ALMOST_FULL;
    UInt32 ALMOST_EMPTY;
    UInt32 DATA_WIDTH_32;
    std::queue<UInt32*> q;
};

extern __C__ void* cds_carbon_fifo_sc_sa_create(int numParams, CModelParam* cmodelParams, const char* inst)
{
    carbon_fifo_sc_sa_state *state;
    if (numParams == 0)
	state = new carbon_fifo_sc_sa_state(8, 8, 3, 7, 1);
    else
	state = new carbon_fifo_sc_sa_state(atoi(cmodelParams[0].paramValue),
					 atoi(cmodelParams[1].paramValue),
					 atoi(cmodelParams[2].paramValue),
					 atoi(cmodelParams[3].paramValue),
					 atoi(cmodelParams[4].paramValue));

    return static_cast<void*>(state);
}

extern __C__ void cds_carbon_fifo_sc_sa_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
}

extern __C__ void cds_carbon_fifo_sc_sa_run(void* hndl, CDScarbon_fifo_sc_saContext context
		, const UInt32* clk // Input, size = 1 word(s)
		, const UInt32* rst // Input, size = 1 word(s)
		, const UInt32* data_in // Input, size = 3 word(s)
		, const UInt32* push // Input, size = 1 word(s)
		, const UInt32* pop // Input, size = 1 word(s)
		, UInt32* data_out // Output, size = 3 word(s)
		, UInt32* used // Output, size = 1 word(s)
		, UInt32* full // Output, size = 1 word(s)
		, UInt32* empty // Output, size = 1 word(s)
		, UInt32* a_full // Output, size = 1 word(s)
		, UInt32* a_empty // Output, size = 1 word(s)
	)
{
    carbon_fifo_sc_sa_state *state = static_cast<carbon_fifo_sc_sa_state*>(hndl);

    if (*rst)
	while (!(state->q.empty())) {	// what, no erase() ?
	    UInt32 *data = state->q.front();
	    delete [] data;
	    state->q.pop();
	}
    else {
	if (*push) {
	    UInt32 *data = new UInt32[state->DATA_WIDTH_32];
	    memcpy(data, data_in, state->DATA_WIDTH_32 * sizeof(UInt32));
	    state->q.push(data);
	}
	if (*pop) {
	    if (!(state->q.empty())) {
		UInt32 *data = state->q.front();
		delete [] data;
		state->q.pop();
	    }
	}
    }

    if (state->q.empty())
	memset(data_out, 0, state->DATA_WIDTH_32 * sizeof(UInt32));
    else
	memcpy(data_out, state->q.front(), state->DATA_WIDTH_32 * sizeof(UInt32));

    *used = state->q.size();
    *full = (state->q.size() == state->DEPTH);
    *empty = state->q.empty();
    *a_full = (state->q.size() == state->ALMOST_FULL);
    *a_empty = (state->q.size() == state->ALMOST_EMPTY);
}

extern __C__ void cds_carbon_fifo_sc_sa_destroy(void* hndl)
{
    carbon_fifo_sc_sa_state *state = static_cast<carbon_fifo_sc_sa_state*>(hndl);
    delete state;
}

//------------------------
// carbon_fifo_dc_sa
//------------------------

class carbon_fifo_dc_sa_state
{
public:
    carbon_fifo_dc_sa_state(UInt32 depth, UInt32 data_width, UInt32 used_width)
	: DEPTH(depth), DATA_WIDTH(data_width), USED_WIDTH(used_width)
    {
	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;
    }
    ~carbon_fifo_dc_sa_state()
    {
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 USED_WIDTH;
    UInt32 DATA_WIDTH_32;
    std::queue<UInt32*> q;
};

extern __C__ void* cds_carbon_fifo_dc_sa_create(int numParams, CModelParam* cmodelParams, const char* inst)
{
    carbon_fifo_dc_sa_state *state;
    if (numParams == 0)
	state = new carbon_fifo_dc_sa_state(8, 8, 3);
    else
	state = new carbon_fifo_dc_sa_state(atoi(cmodelParams[0].paramValue),
					 atoi(cmodelParams[1].paramValue),
					 atoi(cmodelParams[2].paramValue));
    return static_cast<void*>(state);
}

extern __C__ void cds_carbon_fifo_dc_sa_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
}

extern __C__ void cds_carbon_fifo_dc_sa_run(void* hndl, CDScarbon_fifo_dc_saContext context
		, const UInt32* clkr // Input, size = 1 word(s)
		, const UInt32* clkw // Input, size = 1 word(s)
		, const UInt32* rst // Input, size = 1 word(s)
		, const UInt32* data_in // Input, size = 3 word(s)
		, const UInt32* push // Input, size = 1 word(s)
		, const UInt32* pop // Input, size = 1 word(s)
		, UInt32* data_out // Output, size = 3 word(s)
		, UInt32* used_r // Output, size = 1 word(s)
		, UInt32* used_w // Output, size = 1 word(s)
		, UInt32* full_r // Output, size = 1 word(s)
		, UInt32* full_w // Output, size = 1 word(s)
		, UInt32* empty_r // Output, size = 1 word(s)
		, UInt32* empty_w // Output, size = 1 word(s)
	)
{
    carbon_fifo_dc_sa_state *state = static_cast<carbon_fifo_dc_sa_state*>(hndl);

    if (*rst)
	while (!(state->q.empty())) {	// what, no erase() ?
	    UInt32 *data = state->q.front();
	    delete [] data;
	    state->q.pop();
	}

    switch (context) {
    case eCDScarbon_fifo_dc_saRiseclkw:
	if (!(*rst) && *push) {
	    UInt32 *data = new UInt32[state->DATA_WIDTH_32];
	    memcpy(data, data_in, state->DATA_WIDTH_32 * sizeof(UInt32));
	    state->q.push(data);
	}

	*used_w = state->q.size();
	*full_w = (state->q.size() == state->DEPTH);
	*empty_w = state->q.empty();
	break;

    case eCDScarbon_fifo_dc_saRiseclkr:
	if (!(*rst) && *pop) {
	    if (!(state->q.empty())) {
		UInt32 *data = state->q.front();
		delete [] data;
		state->q.pop();
	    }
	}

	if (state->q.empty())
	    memset(data_out, 0, state->DATA_WIDTH_32 * sizeof(UInt32));
	else
	    memcpy(data_out, state->q.front(), state->DATA_WIDTH_32 * sizeof(UInt32));

	*used_r = state->q.size();
	*full_r = (state->q.size() == state->DEPTH);
	*empty_r = state->q.empty();
    }
}

extern __C__ void cds_carbon_fifo_dc_sa_destroy(void* hndl)
{
    carbon_fifo_dc_sa_state *state = static_cast<carbon_fifo_dc_sa_state*>(hndl);
    delete state;
}

