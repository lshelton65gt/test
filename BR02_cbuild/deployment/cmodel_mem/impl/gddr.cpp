/*****************************************************************************

 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "cds_libdesign_carbon_gddr_mem.h"
#include "carbon_mem_internal.h"
#include "carbon_mem_intf.h"
#include "gddr_state.h"
#include "carbon_mem_core.h"
#include <iostream>
#include <string>

extern __C__ void* cds_carbon_gddr_mem_create(int numParams, CModelParam* cmodelParams, const char* inst)
{
    std::string base = inst;
    // Strip c-model instance name
    base.erase(base.rfind('.'));

    UInt32 addr_bits=0, data_bits=0, data_words=0, cols_bits=0;

    for (int i = 0; i < numParams; ++i) {
	if (strcmp(cmodelParams[i].paramName, "addr_bits") == 0)
	    addr_bits = atoi(cmodelParams[i].paramValue);
	if (strcmp(cmodelParams[i].paramName, "data_bits") == 0) {
	    data_bits = atoi(cmodelParams[i].paramValue);
	    data_words= data_bits / 32;
	    if (data_bits % 32)
		++data_words;
	}
	if (strcmp(cmodelParams[i].paramName, "cols_bits") == 0)
	    cols_bits = atoi(cmodelParams[i].paramValue);
    }

    carbon_gddr_mem_state *state = new carbon_gddr_mem_state(addr_bits, cols_bits, data_words, data_bits);

    // Register all 4 banks
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem((base + ".Bank0").c_str(), state->mem0);
    ptr->registerMem((base + ".Bank1").c_str(), state->mem1);
    ptr->registerMem((base + ".Bank2").c_str(), state->mem2);
    ptr->registerMem((base + ".Bank3").c_str(), state->mem3);

    return reinterpret_cast<void *>(state);
}

extern __C__ void cds_carbon_gddr_mem_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
}

extern __C__ void cds_carbon_gddr_mem_run(void* hndl, CDScarbon_gddr_memContext context
		, const UInt32* clk // Input, size = 1 word(s)
		, const UInt32* Dq_in // Input, size = 5 word(s)
		, UInt32* Dq_out // Output, size = 5 word(s)
		, UInt32* Dq_en // Output, size = 1 word(s)
		, const UInt32* Dqs_in // Input, size = 1 word(s)
		, UInt32* Dqs_out // Output, size = 1 word(s)
		, UInt32* Dqs_en // Output, size = 1 word(s)
		, const UInt32* Addr // Input, size = 1 word(s)
		, const UInt32* Ba // Input, size = 1 word(s)
		, const UInt32* Cs_n // Input, size = 1 word(s)
		, const UInt32* Ras_n // Input, size = 1 word(s)
		, const UInt32* Cas_n // Input, size = 1 word(s)
		, const UInt32* We_n // Input, size = 1 word(s)
		, const UInt32* Dm // Input, size = 1 word(s)
                , const UInt32* pre_sel // Input, size = 1 word(s)
	)
{
    carbon_gddr_mem_state *state = reinterpret_cast<carbon_gddr_mem_state *>(hndl);

    switch (context) {
    case eCDScarbon_gddr_memAsync:
	// Drive Dqs_en
	if (*clk != state->last_clk) {
	    state->doPipe();
	}
	    state->genStrobes();
	    *Dqs_out = state->Dqs_out;
	    *Dqs_en = state->Dqs_en;

	state->last_clk = *clk;
	break;
    case eCDScarbon_gddr_memFallclk:
	// Run negedge code
	if (state->last_clk == 1) {
	    state->doPipe();
	}
	// Drive Dq_out and Dq_en
	state->negedge(Dq_in, Dq_out, Dq_en, *Dqs_in, *Addr, *Ba, *Cs_n, *Ras_n, *Cas_n, *We_n, *Dm);
	state->last_clk = 0;
	break;
    case eCDScarbon_gddr_memRiseclk:
	// Run posedge code
	if (state->last_clk == 0) {
	    state->doPipe();
	}
	// Drive Dq_out and Dq_en
	state->posedge(Dq_in, Dq_out, Dq_en, *Dqs_in, *Addr, *Ba, *Cs_n, *Ras_n, *Cas_n, *We_n, *Dm, *pre_sel);
	state->last_clk = 1;
	break;

    case eCDScarbon_gddr_memRiseDqs_in:
    case eCDScarbon_gddr_memFallDqs_in:
	state->doStrobes(Dq_in, *Dqs_in, *Dm, Dq_out);
	break;
    }
}

extern __C__ void cds_carbon_gddr_mem_destroy(void* hndl)
{
}

