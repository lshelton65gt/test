/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// This file implements the EMC memory libraries as c-models

#include "cds_libdesign_carbon_mem1r1w_we.h"
#include "cds_libdesign_carbon_mem1r1w.h"
#include "cds_libdesign_carbon_mem1r1w_reg.h"
#include "cds_libdesign_carbon_mem1r1w_var.h"
#include "cds_libdesign_carbon_mem1rw_reg_var.h"
#include "cds_libdesign_carbon_mem1rw_var.h"
#include "cds_libdesign_carbon_mem2r2w.h"
#include "cds_libdesign_carbon_mem2r2w_be.h"
#include "cds_libdesign_carbon_mem2r2w_reg.h"
#include "cds_libdesign_carbon_mem2r2w_reg_be.h"
#include "carbon_mem_intf.h"
#include "carbon_mem_internal.h"
#include "carbon_mem_core_array.h"
#include "bitvector.h"
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <iostream>

//------------------------
// carbon_mem1r1w_we
//------------------------

class carbon_mem1r1w_we_state
{
public:
    carbon_mem1r1w_we_state(UInt32 depth, UInt32 data_width, UInt32 addr_width, const char* memory_file)
	: DEPTH(depth), DATA_WIDTH(data_width), ADDR_WIDTH(addr_width)
    {
	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;
	mem_array = new carbon_mem_core_array(DEPTH, DATA_WIDTH_32, DATA_WIDTH);
	doa = new UInt32[DATA_WIDTH_32];

	if ((memory_file != NULL) && (strlen(memory_file) > 0)) {
	   MEMORY_FILE = new char[strlen(memory_file)];
	   strcpy(MEMORY_FILE, memory_file);
	} else {
	   MEMORY_FILE = NULL;
	}

	memset(doa, 0, DATA_WIDTH_32 * sizeof(UInt32));
    }
    ~carbon_mem1r1w_we_state()
    {
	delete mem_array;
	delete [] doa;
	if (MEMORY_FILE) delete [] MEMORY_FILE;
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 ADDR_WIDTH;
    UInt32 DATA_WIDTH_32;
    char *MEMORY_FILE;  // Verilog parameter for readmemh() filename
    carbon_mem_core_array *mem_array;
    UInt32 *doa;
};

extern "C" void* cds_carbon_mem1r1w_we_create(int numParams, CModelParam* cmodelParams, const char* instName)
{
    carbon_mem1r1w_we_state *state;
    if (numParams != 4)
	state = new carbon_mem1r1w_we_state(8, 8, 3, "");
    else
	state = new carbon_mem1r1w_we_state(atoi(cmodelParams[0].paramValue),
					    atoi(cmodelParams[1].paramValue),
					    atoi(cmodelParams[2].paramValue),
					    cmodelParams[3].paramValue);

    // register for mem access functions
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem(instName, state->mem_array);

    return static_cast<void*>(state);
}

extern "C" void cds_carbon_mem1r1w_we_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   // Just after construction we are passed the CarbonObjectID* for reference
   if (reason == eCarbonCModelID && hndl != NULL) {
      carbon_mem1r1w_we_state *state = static_cast<carbon_mem1r1w_we_state*>(hndl);

      // save the CarbonObjectID*
      state->mem_array->setCarbonObject(static_cast<CarbonObjectID*>(cmodelData));

      // check if we need to call the readmemh() function to preload the memory
      if (state->MEMORY_FILE != NULL)
	 state->mem_array->readmemh(state->MEMORY_FILE, static_cast<CarbonObjectID*>(cmodelData));
   }
   return;
}


extern "C" void cds_carbon_mem1r1w_we_run(void* hndl, CDScarbon_mem1r1w_weContext context
		, const UInt32* clka // Input, size = 1 word(s)
		, const UInt32* clkb // Input, size = 1 word(s)
		, const UInt32* adra // Input, size = 1 word(s)
		, const UInt32* adrb // Input, size = 1 word(s)
		, const UInt32* web // Input, size = 1 word(s)
		, const UInt32* dib // Input, size = 2 word(s)
		, const UInt32* mea // Input, size = 1 word(s)
		, const UInt32* meb // Input, size = 1 word(s)
		, UInt32* doa // Output, size = 2 word(s)
	)
{
    carbon_mem1r1w_we_state *state = static_cast<carbon_mem1r1w_we_state*>(hndl);

    // Sequential code
    switch (context) {
    case (eCDScarbon_mem1r1w_weRiseclka):
	if (*mea)
	    state->mem_array->read(*adra, state->doa);
	break;
    case (eCDScarbon_mem1r1w_weRiseclkb):
	if (*meb) {
	    UInt32 *temp = new UInt32[state->DATA_WIDTH_32];
	    state->mem_array->read(*adrb, temp);
	    for (UInt32 i = 0; i < state->DATA_WIDTH_32; ++i) {
		// Each bit has a write enable
		temp[i] = (temp[i] & ~web[i]) | (dib[i] & web[i]);
	    }
	    state->mem_array->write(*adrb, temp);
	    delete [] temp;
	}
	break;
    }

    // Continuous assigns/outputs
    memcpy(doa, state->doa, state->DATA_WIDTH_32 * sizeof(UInt32));
}


extern "C" void cds_carbon_mem1r1w_we_destroy(void* hndl)
{
    carbon_mem1r1w_we_state *state = static_cast<carbon_mem1r1w_we_state*>(hndl);
    delete state;
}

//------------------------
// carbon_mem1r1w
//------------------------

class carbon_mem1r1w_state
{
public:
    carbon_mem1r1w_state(UInt32 depth, UInt32 data_width, UInt32 addr_width, const char* memory_file)
	: DEPTH(depth), DATA_WIDTH(data_width), ADDR_WIDTH(addr_width)
    {
	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;
	mem_array = new carbon_mem_core_array(DEPTH, DATA_WIDTH_32, DATA_WIDTH);
	dob_q = new UInt32[DATA_WIDTH_32];
	dob = new UInt32[DATA_WIDTH_32];

	if ((memory_file != NULL) && (strlen(memory_file) > 0)) {
	   MEMORY_FILE = new char[strlen(memory_file)];
	   strcpy(MEMORY_FILE, memory_file);
	} else {
	   MEMORY_FILE = NULL;
	}

	memset(dob_q, 0, DATA_WIDTH_32 * sizeof(UInt32));
	memset(dob, 0, DATA_WIDTH_32 * sizeof(UInt32));
	adrb_q = 0;
	reb_q = 0;
    }
    ~carbon_mem1r1w_state()
    {
	delete mem_array;
	delete [] dob_q;
	delete [] dob;
	if (MEMORY_FILE) delete [] MEMORY_FILE;
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 ADDR_WIDTH;
    UInt32 DATA_WIDTH_32;
    char *MEMORY_FILE;  // Verilog parameter for readmemh() filename
    carbon_mem_core_array *mem_array;
    UInt32 adrb_q;
    UInt32 reb_q;
    UInt32 *dob_q;
    UInt32 *dob;
};

extern "C" void* cds_carbon_mem1r1w_create(int numParams, CModelParam* cmodelParams,
					   const char* instName)
{
    carbon_mem1r1w_state *state;
    if (numParams != 4)
	state = new carbon_mem1r1w_state(8, 8, 3, "");
    else
	state = new carbon_mem1r1w_state(atoi(cmodelParams[0].paramValue),
					 atoi(cmodelParams[1].paramValue),
					 atoi(cmodelParams[2].paramValue),
					 cmodelParams[3].paramValue);

    // register for mem access functions
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem(instName, state->mem_array);

    return static_cast<void*>(state);
}

extern "C" void cds_carbon_mem1r1w_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   // Just after construction we are passed the CarbonObjectID* for reference
   if (reason == eCarbonCModelID && hndl != NULL) {
      carbon_mem1r1w_state *state = static_cast<carbon_mem1r1w_state*>(hndl);

      // save the CarbonObjectID*
      state->mem_array->setCarbonObject(static_cast<CarbonObjectID*>(cmodelData));

      // check if we need to call the readmemh() function to preload the memory
      if (state->MEMORY_FILE != NULL)
	 state->mem_array->readmemh(state->MEMORY_FILE, static_cast<CarbonObjectID*>(cmodelData));
   }
   return;
}


extern "C" void cds_carbon_mem1r1w_run(void* hndl, CDScarbon_mem1r1wContext context
		, const UInt32* clka // Input, size = 1 word(s)
		, const UInt32* adra // Input, size = 1 word(s)
		, const UInt32* dia // Input, size = 3 word(s)
		, const UInt32* wea // Input, size = 1 word(s)
		, const UInt32* clkb // Input, size = 1 word(s)
		, const UInt32* adrb // Input, size = 1 word(s)
		, UInt32* dob // Output, size = 3 word(s)
		, const UInt32* reb // Input, size = 1 word(s)
	)
{
    carbon_mem1r1w_state *state = static_cast<carbon_mem1r1w_state*>(hndl);

    // Sequential code
    switch (context) {
    case (eCDScarbon_mem1r1wRiseclka):
	if (*wea)
	    state->mem_array->write(*adra, dia);
	break;
    case (eCDScarbon_mem1r1wRiseclkb):
	if (state->reb_q)
	    memcpy(state->dob_q, state->dob, state->DATA_WIDTH_32 * sizeof(UInt32));
	state->adrb_q = *adrb;
	state->reb_q = *reb;
	break;
    }

    // Continuous assigns/outputs
    if (state->reb_q)
	state->mem_array->read(state->adrb_q, state->dob);
    else
	memcpy(state->dob, state->dob_q, state->DATA_WIDTH_32 * sizeof(UInt32));

    memcpy(dob, state->dob, state->DATA_WIDTH_32 * sizeof(UInt32));
}


extern "C" void cds_carbon_mem1r1w_destroy(void* hndl)
{
    carbon_mem1r1w_state *state = static_cast<carbon_mem1r1w_state*>(hndl);
    delete state;
}

//------------------------
// carbon_mem1r1w_reg
//------------------------

class carbon_mem1r1w_reg_state
{
public:
    carbon_mem1r1w_reg_state(UInt32 depth, UInt32 data_width, UInt32 addr_width, const char* memory_file)
	: DEPTH(depth), DATA_WIDTH(data_width), ADDR_WIDTH(addr_width)
    {
	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;
	mem_array = new carbon_mem_core_array(DEPTH, DATA_WIDTH_32, DATA_WIDTH);
	dob_q = new UInt32[DATA_WIDTH_32];
	dob = new UInt32[DATA_WIDTH_32];

	if ((memory_file != NULL) && (strlen(memory_file) > 0)) {
	   MEMORY_FILE = new char[strlen(memory_file)];
	   strcpy(MEMORY_FILE, memory_file);
	} else {
	   MEMORY_FILE = NULL;
	}

	memset(dob_q, 0, DATA_WIDTH_32 * sizeof(UInt32));
	memset(dob, 0, DATA_WIDTH_32 * sizeof(UInt32));
	adrb_q = 0;
	reb_q = 0;
    }
    ~carbon_mem1r1w_reg_state()
    {
	delete mem_array;
	delete [] dob_q;
	delete [] dob;
	if (MEMORY_FILE) delete [] MEMORY_FILE;
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 ADDR_WIDTH;
    UInt32 DATA_WIDTH_32;
    char *MEMORY_FILE;  // Verilog parameter for readmemh() filename
    carbon_mem_core_array *mem_array;
    UInt32 adrb_q;
    UInt32 reb_q;
    UInt32 *dob_q;
    UInt32 *dob;
};

extern "C" void* cds_carbon_mem1r1w_reg_create(int numParams, CModelParam* cmodelParams,
					   const char* instName)
{
    carbon_mem1r1w_reg_state *state;
    if (numParams != 4)
	state = new carbon_mem1r1w_reg_state(8, 8, 3, "");
    else
	state = new carbon_mem1r1w_reg_state(atoi(cmodelParams[0].paramValue),
					 atoi(cmodelParams[1].paramValue),
					 atoi(cmodelParams[2].paramValue),
					 cmodelParams[3].paramValue);

    // register for mem access functions
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem(instName, state->mem_array);

    return static_cast<void*>(state);
}

extern "C" void cds_carbon_mem1r1w_reg_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   // Just after construction we are passed the CarbonObjectID* for reference
   if (reason == eCarbonCModelID && hndl != NULL) {
      carbon_mem1r1w_reg_state *state = static_cast<carbon_mem1r1w_reg_state*>(hndl);

      // save the CarbonObjectID*
      state->mem_array->setCarbonObject(static_cast<CarbonObjectID*>(cmodelData));

      // check if we need to call the readmemh() function to preload the memory
      if (state->MEMORY_FILE != NULL)
	 state->mem_array->readmemh(state->MEMORY_FILE, static_cast<CarbonObjectID*>(cmodelData));
   }
   return;
}


extern "C" void cds_carbon_mem1r1w_reg_run(void* hndl, CDScarbon_mem1r1w_regContext context
		, const UInt32* clka // Input, size = 1 word(s)
		, const UInt32* adra // Input, size = 1 word(s)
		, const UInt32* dia // Input, size = 3 word(s)
		, const UInt32* wea // Input, size = 1 word(s)
		, const UInt32* clkb // Input, size = 1 word(s)
		, const UInt32* adrb // Input, size = 1 word(s)
		, UInt32* dob // Output, size = 3 word(s)
		, const UInt32* reb // Input, size = 1 word(s)
	)
{
    carbon_mem1r1w_reg_state *state = static_cast<carbon_mem1r1w_reg_state*>(hndl);

    // Sequential code
    switch (context) {
    case (eCDScarbon_mem1r1w_regRiseclka):
	if (*wea)
	    state->mem_array->write(*adra, dia);
	break;
    case (eCDScarbon_mem1r1w_regRiseclkb):
	if (state->reb_q)
	    memcpy(state->dob_q, state->dob, state->DATA_WIDTH_32 * sizeof(UInt32));

	if (*reb)
	    state->mem_array->read(state->adrb_q, state->dob);

	state->adrb_q = *adrb;
	state->reb_q = *reb;
	break;
    }

    // Continuous assigns/outputs
    memcpy(dob, state->dob, state->DATA_WIDTH_32 * sizeof(UInt32));
}


extern "C" void cds_carbon_mem1r1w_reg_destroy(void* hndl)
{
    carbon_mem1r1w_reg_state *state = static_cast<carbon_mem1r1w_reg_state*>(hndl);
    delete state;
}

//------------------------
// carbon_mem1r1w_var
//------------------------

class carbon_mem1r1w_var_state
{
public:
    carbon_mem1r1w_var_state(UInt32 write_depth, UInt32 write_data_width, UInt32 write_addr_width, UInt32 read_depth, UInt32 read_data_width, UInt32 read_addr_width, const char* memory_file)
	: WRITE_DEPTH(write_depth), WRITE_DATA_WIDTH(write_data_width), WRITE_ADDR_WIDTH(write_addr_width), READ_DEPTH(read_depth), READ_DATA_WIDTH(read_data_width), READ_ADDR_WIDTH(read_addr_width)
    {
	// Sanity checks
	// Perceived mem size must be the same on both ports
        if ((WRITE_DATA_WIDTH * WRITE_DEPTH) != (READ_DATA_WIDTH * READ_DEPTH))
	   std::cout << "ERROR: Perceived memory size must be the same on both ports. WRITE_DATA_WIDTH=" << std::dec << WRITE_DATA_WIDTH << ", WRITE_DEPTH=" << WRITE_DEPTH << ", READ_DATA_WIDTH=" << READ_DATA_WIDTH << ", READ_DEPTH=" << READ_DEPTH << "." << std::endl;
	assert((WRITE_DATA_WIDTH * WRITE_DEPTH) == (READ_DATA_WIDTH * READ_DEPTH));
	// Widths should be different - otherwise use the simpler model
	if (WRITE_DATA_WIDTH == READ_DATA_WIDTH)
	   std::cout << "ERROR: Read Data Width and Write Data Width must be different, otherwise a simpler model should be selected." << std::endl;
	assert(WRITE_DATA_WIDTH != READ_DATA_WIDTH);

	// We'll store things internally in the profile of the narrower port
	// Also, the wider port must be a multiple of the narrower one
	if (WRITE_DATA_WIDTH > READ_DATA_WIDTH) {
	    mem_format = READ;
	    assert ((WRITE_DATA_WIDTH % READ_DATA_WIDTH) == 0);
	} else {
	    mem_format = WRITE;
	    assert ((READ_DATA_WIDTH % WRITE_DATA_WIDTH) == 0);
	}

	WRITE_DATA_WIDTH_32 = WRITE_DATA_WIDTH / 32;
	if (WRITE_DATA_WIDTH % 32)
	    ++WRITE_DATA_WIDTH_32;
	READ_DATA_WIDTH_32 = READ_DATA_WIDTH / 32;
	if (READ_DATA_WIDTH % 32)
	    ++READ_DATA_WIDTH_32;

	if (mem_format == WRITE)
	    mem_array = new carbon_mem_core_array(WRITE_DEPTH, WRITE_DATA_WIDTH_32, WRITE_DATA_WIDTH);
	else
	    mem_array = new carbon_mem_core_array(READ_DEPTH, READ_DATA_WIDTH_32, READ_DATA_WIDTH);

	dob_q = new UInt32[READ_DATA_WIDTH_32];
	dob = new UInt32[READ_DATA_WIDTH_32];

	if ((memory_file != NULL) && (strlen(memory_file) > 0)) {
	   MEMORY_FILE = new char[strlen(memory_file)];
	   strcpy(MEMORY_FILE, memory_file);
	} else {
	   MEMORY_FILE = NULL;
	}

	memset(dob_q, 0, READ_DATA_WIDTH_32 * sizeof(UInt32));
	memset(dob, 0, READ_DATA_WIDTH_32 * sizeof(UInt32));
	adrb_q = 0;
	reb_q = 0;
    }
    ~carbon_mem1r1w_var_state()
    {
	delete mem_array;
	delete [] dob_q;
	delete [] dob;
	if (MEMORY_FILE) delete [] MEMORY_FILE;
    }

    // params
    UInt32 WRITE_DEPTH;
    UInt32 WRITE_DATA_WIDTH;
    UInt32 WRITE_ADDR_WIDTH;
    UInt32 WRITE_DATA_WIDTH_32;
    UInt32 READ_DEPTH;
    UInt32 READ_DATA_WIDTH;
    UInt32 READ_ADDR_WIDTH;
    UInt32 READ_DATA_WIDTH_32;
    char *MEMORY_FILE;  // Verilog parameter for readmemh() filename

    // conversion stuff
    UInt32 mem_format;

    // state
    carbon_mem_core_array *mem_array;
    UInt32 adrb_q;
    UInt32 reb_q;
    UInt32 *dob_q;
    UInt32 *dob;

    enum {
	READ,
	WRITE
    };
};

extern "C" void* cds_carbon_mem1r1w_var_create(int numParams, CModelParam* cmodelParams,
					   const char* instName)
{
    carbon_mem1r1w_var_state *state;
    if (numParams != 7)
	state = new carbon_mem1r1w_var_state(8, 8, 3, 16, 4, 4, "");
    else
	state = new carbon_mem1r1w_var_state(atoi(cmodelParams[0].paramValue),
					     atoi(cmodelParams[1].paramValue),
					     atoi(cmodelParams[2].paramValue),
					     atoi(cmodelParams[3].paramValue),
					     atoi(cmodelParams[4].paramValue),
					     atoi(cmodelParams[5].paramValue),
					     cmodelParams[6].paramValue);

    // register for mem access functions
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem(instName, state->mem_array);

    return static_cast<void*>(state);
}

extern "C" void cds_carbon_mem1r1w_var_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   // Just after construction we are passed the CarbonObjectID* for reference
   if (reason == eCarbonCModelID && hndl != NULL) {
      carbon_mem1r1w_var_state *state = static_cast<carbon_mem1r1w_var_state*>(hndl);

      // save the CarbonObjectID*
      state->mem_array->setCarbonObject(static_cast<CarbonObjectID*>(cmodelData));

      // check if we need to call the readmemh() function to preload the memory
      if (state->MEMORY_FILE != NULL)
	 state->mem_array->readmemh(state->MEMORY_FILE, static_cast<CarbonObjectID*>(cmodelData));
   }
   return;
}


extern "C" void cds_carbon_mem1r1w_var_run(void* hndl, CDScarbon_mem1r1w_varContext context
		, const UInt32* clka // Input, size = 1 word(s)
		, const UInt32* adra // Input, size = 1 word(s)
		, const UInt32* dia // Input, size = 3 word(s)
		, const UInt32* wea // Input, size = 1 word(s)
		, const UInt32* clkb // Input, size = 1 word(s)
		, const UInt32* adrb // Input, size = 1 word(s)
		, UInt32* dob // Output, size = 3 word(s)
		, const UInt32* reb // Input, size = 1 word(s)
	)
{
    carbon_mem1r1w_var_state *state = static_cast<carbon_mem1r1w_var_state*>(hndl);

    // Sequential code
    switch (context) {
    case (eCDScarbon_mem1r1w_varRiseclka):
	if (*wea) {
	    if (state->mem_format == carbon_mem1r1w_var_state::WRITE)	// simple
		state->mem_array->write(*adra, dia);
	    else {
		BitVector write_vec(state->WRITE_DATA_WIDTH, dia);
		UInt32 sub_words = state->WRITE_DATA_WIDTH / state->READ_DATA_WIDTH;
		for (UInt32 i = 0; i < sub_words; ++i)
		    state->mem_array->write(*adra * sub_words + i, write_vec.getRange(state->READ_DATA_WIDTH * i, state->READ_DATA_WIDTH));
	    }
			
	}
	break;
    case (eCDScarbon_mem1r1w_varRiseclkb):
	if (state->reb_q)
	    memcpy(state->dob_q, state->dob, state->READ_DATA_WIDTH_32 * sizeof(UInt32));

	state->adrb_q = *adrb;
	state->reb_q = *reb;
	break;
    }

    // Continuous assigns/outputs
    if ((state->mem_format == carbon_mem1r1w_var_state::READ) || !(state->reb_q)) {	// simple
	if (state->reb_q)
	    state->mem_array->read(state->adrb_q, state->dob);
	else
	    memcpy(state->dob, state->dob_q, state->READ_DATA_WIDTH_32 * sizeof(UInt32));
    } else {
	BitVector read_vec(state->READ_DATA_WIDTH);
	UInt32 sub_words = state->READ_DATA_WIDTH / state->WRITE_DATA_WIDTH;
	UInt32 temp[state->WRITE_DATA_WIDTH_32];
	for (UInt32 i = 0; i < sub_words; ++i) {
	    state->mem_array->read(state->adrb_q * sub_words + i, temp);
	    read_vec.fillRange(temp, state->WRITE_DATA_WIDTH * i, state->WRITE_DATA_WIDTH);
	}
	memcpy(state->dob, read_vec.getArray(), state->READ_DATA_WIDTH_32 * sizeof(UInt32));
    }
    memcpy(dob, state->dob, state->READ_DATA_WIDTH_32 * sizeof(UInt32));
}


extern "C" void cds_carbon_mem1r1w_var_destroy(void* hndl)
{
    carbon_mem1r1w_var_state *state = static_cast<carbon_mem1r1w_var_state*>(hndl);
    delete state;
}

//------------------------
// carbon_mem1rw_reg_var
//------------------------

class carbon_mem1rw_reg_var_state
{
public:
    carbon_mem1rw_reg_var_state(UInt32 write_depth, UInt32 write_data_width, UInt32 write_addr_width, UInt32 read_depth, UInt32 read_data_width, UInt32 read_addr_width, const char* memory_file)
	: WRITE_DEPTH(write_depth), WRITE_DATA_WIDTH(write_data_width), WRITE_ADDR_WIDTH(write_addr_width), READ_DEPTH(read_depth), READ_DATA_WIDTH(read_data_width), READ_ADDR_WIDTH(read_addr_width)
    {
	// Sanity checks
	// Perceived mem size must be the same on both ports
        if ((WRITE_DATA_WIDTH * WRITE_DEPTH) != (READ_DATA_WIDTH * READ_DEPTH))
	   std::cout << "ERROR: Perceived memory size must be the same on both ports." << std::endl;
	assert((WRITE_DATA_WIDTH * WRITE_DEPTH) == (READ_DATA_WIDTH * READ_DEPTH));
	// Widths should be different - otherwise use the simpler model
	if (WRITE_DATA_WIDTH == READ_DATA_WIDTH)
	   std::cout << "ERROR: Read Data Width and Write Data Width must be different, otherwise a simpler model should be selected." << std::endl;
	assert(WRITE_DATA_WIDTH != READ_DATA_WIDTH);

	// We'll store things internally in the profile of the narrower port
	// Also, the wider port must be a multiple of the narrower one
	if (WRITE_DATA_WIDTH > READ_DATA_WIDTH) {
	    mem_format = READ;
	    assert ((WRITE_DATA_WIDTH % READ_DATA_WIDTH) == 0);
	} else {
	    mem_format = WRITE;
	    assert ((READ_DATA_WIDTH % WRITE_DATA_WIDTH) == 0);
	}

	WRITE_DATA_WIDTH_32 = WRITE_DATA_WIDTH / 32;
	if (WRITE_DATA_WIDTH % 32)
	    ++WRITE_DATA_WIDTH_32;
	READ_DATA_WIDTH_32 = READ_DATA_WIDTH / 32;
	if (READ_DATA_WIDTH % 32)
	    ++READ_DATA_WIDTH_32;

	if (mem_format == WRITE)
	    mem_array = new carbon_mem_core_array(WRITE_DEPTH, WRITE_DATA_WIDTH_32, WRITE_DATA_WIDTH);
	else
	    mem_array = new carbon_mem_core_array(READ_DEPTH, READ_DATA_WIDTH_32, READ_DATA_WIDTH);

	if ((memory_file != NULL) && (strlen(memory_file) > 0)) {
	   MEMORY_FILE = new char[strlen(memory_file)];
	   strcpy(MEMORY_FILE, memory_file);
	} else {
	   MEMORY_FILE = NULL;
	}

	mem_out = new UInt32[READ_DATA_WIDTH_32];
	memset(mem_out, 0, READ_DATA_WIDTH_32 * sizeof(UInt32));
    }
    ~carbon_mem1rw_reg_var_state()
    {
	delete mem_array;
	delete [] mem_out;
	if (MEMORY_FILE) delete [] MEMORY_FILE;
    }

    // params
    UInt32 WRITE_DEPTH;
    UInt32 WRITE_DATA_WIDTH;
    UInt32 WRITE_ADDR_WIDTH;
    UInt32 WRITE_DATA_WIDTH_32;
    UInt32 READ_DEPTH;
    UInt32 READ_DATA_WIDTH;
    UInt32 READ_ADDR_WIDTH;
    UInt32 READ_DATA_WIDTH_32;
    char *MEMORY_FILE;  // Verilog parameter for readmemh() filename

    // conversion stuff
    UInt32 mem_format;

    // state
    carbon_mem_core_array *mem_array;
    UInt32 *mem_out;

    enum {
	READ,
	WRITE
    };
};

extern "C" void* cds_carbon_mem1rw_reg_var_create(int numParams, CModelParam* cmodelParams,
					   const char* instName)
{
    carbon_mem1rw_reg_var_state *state;
    if (numParams != 7)
	state = new carbon_mem1rw_reg_var_state(8, 8, 3, 16, 4, 4, "");
    else
	state = new carbon_mem1rw_reg_var_state(atoi(cmodelParams[0].paramValue),
					     atoi(cmodelParams[1].paramValue),
					     atoi(cmodelParams[2].paramValue),
					     atoi(cmodelParams[3].paramValue),
					     atoi(cmodelParams[4].paramValue),
					     atoi(cmodelParams[5].paramValue),
					     cmodelParams[6].paramValue);

    // register for mem access functions
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem(instName, state->mem_array);

    return static_cast<void*>(state);
}

extern "C" void cds_carbon_mem1rw_reg_var_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   // Just after construction we are passed the CarbonObjectID* for reference
   if (reason == eCarbonCModelID && hndl != NULL) {
      carbon_mem1rw_reg_var_state *state = static_cast<carbon_mem1rw_reg_var_state*>(hndl);

      // save the CarbonObjectID*
      state->mem_array->setCarbonObject(static_cast<CarbonObjectID*>(cmodelData));

      // check if we need to call the readmemh() function to preload the memory
      if (state->MEMORY_FILE != NULL)
	 state->mem_array->readmemh(state->MEMORY_FILE, static_cast<CarbonObjectID*>(cmodelData));
   }
   return;
}


extern "C" void cds_carbon_mem1rw_reg_var_run(void* hndl, CDScarbon_mem1rw_reg_varContext context
		, const UInt32* clk // Input, size = 1 word(s)
		, const UInt32* adrw // Input, size = 1 word(s)
		, const UInt32* adrr // Input, size = 1 word(s)
		, const UInt32* di // Input, size = 3 word(s)
		, const UInt32* we // Input, size = 1 word(s)
		, UInt32* _do // Output, size = 3 word(s)
	)
{
    UInt32 i;
    carbon_mem1rw_reg_var_state *state = static_cast<carbon_mem1rw_reg_var_state*>(hndl);

    // Sequential code
    // Do output first so data doesn't flow
    memcpy(_do, state->mem_out, state->READ_DATA_WIDTH_32 * sizeof(UInt32));

    // Now do the read data before the write, so *that* data doesn't flow, either
    if (state->mem_format == carbon_mem1rw_reg_var_state::READ)	// simple
	state->mem_array->read(*adrr, state->mem_out);
    else {
	BitVector read_vec(state->READ_DATA_WIDTH);
	assert (read_vec.getNumWords() == state->READ_DATA_WIDTH_32);
	UInt32 sub_words = state->READ_DATA_WIDTH / state->WRITE_DATA_WIDTH;
	UInt32 temp[state->WRITE_DATA_WIDTH_32];

	for (i = 0; i < sub_words; ++i) {
	    state->mem_array->read(*adrr * sub_words + i, temp);
 	    read_vec.fillRange(temp, state->WRITE_DATA_WIDTH * i, state->WRITE_DATA_WIDTH);
	}

	memcpy(state->mem_out, read_vec.getArray(), state->READ_DATA_WIDTH_32 * sizeof(UInt32));
    }

    // Now do the write
    if (*we) {
	if (state->mem_format == carbon_mem1rw_reg_var_state::WRITE)	// simple
	    state->mem_array->write(*adrw, di);
	else {
	    BitVector write_vec(state->WRITE_DATA_WIDTH, di);
	    UInt32 sub_words = state->WRITE_DATA_WIDTH / state->READ_DATA_WIDTH;
	    for (i = 0; i < sub_words; ++i)
		state->mem_array->write(*adrw * sub_words + i, write_vec.getRange(state->READ_DATA_WIDTH * i, state->READ_DATA_WIDTH));
	}
    }

}


extern "C" void cds_carbon_mem1rw_reg_var_destroy(void* hndl)
{
    carbon_mem1rw_reg_var_state *state = static_cast<carbon_mem1rw_reg_var_state*>(hndl);
    delete state;
}


//------------------------
// carbon_mem1rw_var
//------------------------

class carbon_mem1rw_var_state
{
public:
    carbon_mem1rw_var_state(UInt32 write_depth, UInt32 write_data_width, UInt32 write_addr_width, UInt32 read_depth, UInt32 read_data_width, UInt32 read_addr_width, const char* memory_file)
	: WRITE_DEPTH(write_depth), WRITE_DATA_WIDTH(write_data_width), WRITE_ADDR_WIDTH(write_addr_width), READ_DEPTH(read_depth), READ_DATA_WIDTH(read_data_width), READ_ADDR_WIDTH(read_addr_width)
    {
	// Sanity checks
	// Perceived mem size must be the same on both ports
        if ((WRITE_DATA_WIDTH * WRITE_DEPTH) != (READ_DATA_WIDTH * READ_DEPTH))
	   std::cout << "ERROR: Perceived memory size must be the same on both ports." << std::endl;
	assert((WRITE_DATA_WIDTH * WRITE_DEPTH) == (READ_DATA_WIDTH * READ_DEPTH));
	// Widths should be different - otherwise use the simpler model
	if (WRITE_DATA_WIDTH == READ_DATA_WIDTH)
	   std::cout << "ERROR: Read Data Width and Write Data Width must be different, otherwise a simpler model should be selected." << std::endl;
	assert(WRITE_DATA_WIDTH != READ_DATA_WIDTH);

	// We'll store things internally in the profile of the narrower port
	// Also, the wider port must be a multiple of the narrower one
	if (WRITE_DATA_WIDTH > READ_DATA_WIDTH) {
	    mem_format = READ;
	    assert ((WRITE_DATA_WIDTH % READ_DATA_WIDTH) == 0);
	} else {
	    mem_format = WRITE;
	    assert ((READ_DATA_WIDTH % WRITE_DATA_WIDTH) == 0);
	}

	WRITE_DATA_WIDTH_32 = WRITE_DATA_WIDTH / 32;
	if (WRITE_DATA_WIDTH % 32)
	    ++WRITE_DATA_WIDTH_32;
	READ_DATA_WIDTH_32 = READ_DATA_WIDTH / 32;
	if (READ_DATA_WIDTH % 32)
	    ++READ_DATA_WIDTH_32;

	if (mem_format == WRITE)
	    mem_array = new carbon_mem_core_array(WRITE_DEPTH, WRITE_DATA_WIDTH_32, WRITE_DATA_WIDTH);
	else
	    mem_array = new carbon_mem_core_array(READ_DEPTH, READ_DATA_WIDTH_32, READ_DATA_WIDTH);

	if ((memory_file != NULL) && (strlen(memory_file) > 0)) {
	   MEMORY_FILE = new char[strlen(memory_file)];
	   strcpy(MEMORY_FILE, memory_file);
	} else {
	   MEMORY_FILE = NULL;
	}
    }
    ~carbon_mem1rw_var_state()
    {
	delete mem_array;
	if (MEMORY_FILE) delete [] MEMORY_FILE;
    }

    // params
    UInt32 WRITE_DEPTH;
    UInt32 WRITE_DATA_WIDTH;
    UInt32 WRITE_ADDR_WIDTH;
    UInt32 WRITE_DATA_WIDTH_32;
    UInt32 READ_DEPTH;
    UInt32 READ_DATA_WIDTH;
    UInt32 READ_ADDR_WIDTH;
    UInt32 READ_DATA_WIDTH_32;
    char *MEMORY_FILE;  // Verilog parameter for readmemh() filename

    // conversion stuff
    UInt32 mem_format;

    // state
    carbon_mem_core_array *mem_array;

    enum {
	READ,
	WRITE
    };
};

extern "C" void* cds_carbon_mem1rw_var_create(int numParams, CModelParam* cmodelParams,
					   const char* instName)
{
    carbon_mem1rw_var_state *state;
    if (numParams != 7)
       state = new carbon_mem1rw_var_state(8, 8, 3, 16, 4, 4, "");
    else
       state = new carbon_mem1rw_var_state(atoi(cmodelParams[0].paramValue),
					   atoi(cmodelParams[1].paramValue),
					   atoi(cmodelParams[2].paramValue),
					   atoi(cmodelParams[3].paramValue),
					   atoi(cmodelParams[4].paramValue),
					   atoi(cmodelParams[5].paramValue),
					   cmodelParams[6].paramValue);

    // register for mem access functions
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem(instName, state->mem_array);

    return static_cast<void*>(state);
}

extern "C" void cds_carbon_mem1rw_var_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   // Just after construction we are passed the CarbonObjectID* for reference
   if (reason == eCarbonCModelID && hndl != NULL) {
      carbon_mem1rw_var_state *state = static_cast<carbon_mem1rw_var_state*>(hndl);

      // save the CarbonObjectID*
      state->mem_array->setCarbonObject(static_cast<CarbonObjectID*>(cmodelData));

      // check if we need to call the readmemh() function to preload the memory
      if (state->MEMORY_FILE != NULL)
	 state->mem_array->readmemh(state->MEMORY_FILE, static_cast<CarbonObjectID*>(cmodelData));
   }
   return;
}


extern "C" void cds_carbon_mem1rw_var_run(void* hndl, CDScarbon_mem1rw_varContext context
		, const UInt32* clk // Input, size = 1 word(s)
		, const UInt32* adrw // Input, size = 1 word(s)
		, const UInt32* adrr // Input, size = 1 word(s)
		, const UInt32* di // Input, size = 3 word(s)
		, const UInt32* we // Input, size = 1 word(s)
		, UInt32* _do // Output, size = 3 word(s)
	)
{
    UInt32 i;
    carbon_mem1rw_var_state *state = static_cast<carbon_mem1rw_var_state*>(hndl);

    // Now do the read data before the write, so *that* data doesn't flow, either
    if (state->mem_format == carbon_mem1rw_var_state::READ)	// simple
	state->mem_array->read(*adrr, _do);
    else {
	BitVector read_vec(state->READ_DATA_WIDTH);
	assert (read_vec.getNumWords() == state->READ_DATA_WIDTH_32);
	UInt32 sub_words = state->READ_DATA_WIDTH / state->WRITE_DATA_WIDTH;
	UInt32 temp[state->WRITE_DATA_WIDTH_32];

	for (i = 0; i < sub_words; ++i) {
	    state->mem_array->read(*adrr * sub_words + i, temp);
 	    read_vec.fillRange(temp, state->WRITE_DATA_WIDTH * i, state->WRITE_DATA_WIDTH);
	}

	memcpy(_do, read_vec.getArray(), state->READ_DATA_WIDTH_32 * sizeof(UInt32));
    }

    // Now do the write
    if (*we) {
	if (state->mem_format == carbon_mem1rw_var_state::WRITE)	// simple
	    state->mem_array->write(*adrw, di);
	else {
	    BitVector write_vec(state->WRITE_DATA_WIDTH, di);
	    UInt32 sub_words = state->WRITE_DATA_WIDTH / state->READ_DATA_WIDTH;
	    for (i = 0; i < sub_words; ++i)
		state->mem_array->write(*adrw * sub_words + i, write_vec.getRange(state->READ_DATA_WIDTH * i, state->READ_DATA_WIDTH));
	}
    }

}


extern "C" void cds_carbon_mem1rw_var_destroy(void* hndl)
{
    carbon_mem1rw_var_state *state = static_cast<carbon_mem1rw_var_state*>(hndl);
    delete state;
}


//------------------------
// carbon_mem2r2w
//------------------------

class carbon_mem2r2w_state
{
public:
    carbon_mem2r2w_state(UInt32 depth, UInt32 data_width, UInt32 addr_width, const char* memory_file)
	: DEPTH(depth), DATA_WIDTH(data_width), ADDR_WIDTH(addr_width)
    {
	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;
	mem_array = new carbon_mem_core_array(DEPTH, DATA_WIDTH_32, DATA_WIDTH);

	if ((memory_file != NULL) && (strlen(memory_file) > 0)) {
	   MEMORY_FILE = new char[strlen(memory_file)];
	   strcpy(MEMORY_FILE, memory_file);
	} else {
	   MEMORY_FILE = NULL;
	}

	adra_q = 0;
	adrb_q = 0;
    }
    ~carbon_mem2r2w_state()
    {
	delete mem_array;
	if (MEMORY_FILE) delete [] MEMORY_FILE;
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 ADDR_WIDTH;
    UInt32 DATA_WIDTH_32;
    char *MEMORY_FILE;  // Verilog parameter for readmemh() filename
    carbon_mem_core_array *mem_array;
    UInt32 adra_q;
    UInt32 adrb_q;
};

extern "C" void* cds_carbon_mem2r2w_create(int numParams, CModelParam* cmodelParams, const char* instName)
{
    carbon_mem2r2w_state *state;
    if (numParams != 4)
	state = new carbon_mem2r2w_state(8, 8, 3, "");
    else
	state = new carbon_mem2r2w_state(atoi(cmodelParams[0].paramValue),
					 atoi(cmodelParams[1].paramValue),
					 atoi(cmodelParams[2].paramValue),
					 cmodelParams[3].paramValue);

    // register for mem access functions
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem(instName, state->mem_array);

    return static_cast<void*>(state);
}

extern "C" void cds_carbon_mem2r2w_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   // Just after construction we are passed the CarbonObjectID* for reference
   if (reason == eCarbonCModelID && hndl != NULL) {
      carbon_mem2r2w_state *state = static_cast<carbon_mem2r2w_state*>(hndl);

      // save the CarbonObjectID*
      state->mem_array->setCarbonObject(static_cast<CarbonObjectID*>(cmodelData));

      // check if we need to call the readmemh() function to preload the memory
      if (state->MEMORY_FILE != NULL)
	 state->mem_array->readmemh(state->MEMORY_FILE, static_cast<CarbonObjectID*>(cmodelData));
   }
   return;
}


extern "C" void cds_carbon_mem2r2w_run(void* hndl, CDScarbon_mem2r2wContext context
		, const UInt32* clka // Input, size = 1 word(s)
		, const UInt32* adra // Input, size = 1 word(s)
		, const UInt32* dia // Input, size = 3 word(s)
		, const UInt32* wea // Input, size = 1 word(s)
		, UInt32* doa // Output, size = 3 word(s)
		, const UInt32* clkb // Input, size = 1 word(s)
		, const UInt32* adrb // Input, size = 1 word(s)
		, const UInt32* dib // Input, size = 3 word(s)
		, const UInt32* web // Input, size = 1 word(s)
		, UInt32* dob // Output, size = 3 word(s)
	)
{
    carbon_mem2r2w_state *state = static_cast<carbon_mem2r2w_state*>(hndl);

    // Sequential code
    switch (context) {
    case (eCDScarbon_mem2r2wRiseclka):
	if (*wea)
	    state->mem_array->write(*adra, dia);
	state->adra_q = *adra;
	break;
    case (eCDScarbon_mem2r2wRiseclkb):
	if (*web)
	    state->mem_array->write(*adrb, dib);
	state->adrb_q = *adrb;
	break;
    }

    // Continuous assigns/outputs
    if (doa)
	state->mem_array->read(state->adra_q, doa);
    if (dob)
	state->mem_array->read(state->adrb_q, dob);

}

extern "C" void cds_carbon_mem2r2w_destroy(void* hndl)
{
    carbon_mem2r2w_state *state = static_cast<carbon_mem2r2w_state*>(hndl);
    delete state;
}


//------------------------
// carbon_mem2r2w_be
//------------------------

class carbon_mem2r2w_be_state
{
public:
    carbon_mem2r2w_be_state(UInt32 depth, UInt32 data_width, UInt32 be_width, UInt32 addr_width, const char* memory_file)
	: DEPTH(depth), DATA_WIDTH(data_width), ADDR_WIDTH(addr_width)
    {
	// 32-bit max byte enable means 256-bit max data width
	assert(DATA_WIDTH <= 256);
	assert(be_width <= 32);

	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;
	mem_array = new carbon_mem_core_array(DEPTH, DATA_WIDTH_32, DATA_WIDTH);

	if ((memory_file != NULL) && (strlen(memory_file) > 0)) {
	   MEMORY_FILE = new char[strlen(memory_file)];
	   strcpy(MEMORY_FILE, memory_file);
	} else {
	   MEMORY_FILE = NULL;
	}

	adra_q = 0;
	adrb_q = 0;
    }
    ~carbon_mem2r2w_be_state()
    {
	delete mem_array;
	if (MEMORY_FILE) delete [] MEMORY_FILE;
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 ADDR_WIDTH;
    UInt32 DATA_WIDTH_32;
    char *MEMORY_FILE;  // Verilog parameter for readmemh() filename
    carbon_mem_core_array *mem_array;
    UInt32 adra_q;
    UInt32 adrb_q;
};

extern "C" void* cds_carbon_mem2r2w_be_create(int numParams, CModelParam* cmodelParams, const char* instName)
{
    carbon_mem2r2w_be_state *state;
    if (numParams != 5)
	state = new carbon_mem2r2w_be_state(8, 8, 1, 3, "");
    else
	state = new carbon_mem2r2w_be_state(atoi(cmodelParams[0].paramValue),
					    atoi(cmodelParams[1].paramValue),
					    atoi(cmodelParams[2].paramValue),
					    atoi(cmodelParams[3].paramValue),
					    cmodelParams[4].paramValue);

    // register for mem access functions
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem(instName, state->mem_array);

    return static_cast<void*>(state);
}

extern "C" void cds_carbon_mem2r2w_be_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   // Just after construction we are passed the CarbonObjectID* for reference
   if (reason == eCarbonCModelID && hndl != NULL) {
      carbon_mem2r2w_be_state *state = static_cast<carbon_mem2r2w_be_state*>(hndl);

      // save the CarbonObjectID*
      state->mem_array->setCarbonObject(static_cast<CarbonObjectID*>(cmodelData));

      // check if we need to call the readmemh() function to preload the memory
      if (state->MEMORY_FILE != NULL)
	 state->mem_array->readmemh(state->MEMORY_FILE, static_cast<CarbonObjectID*>(cmodelData));
   }
   return;
}


extern "C" void cds_carbon_mem2r2w_be_run(void* hndl, CDScarbon_mem2r2w_beContext context
		, const UInt32* clka // Input, size = 1 word(s)
		, const UInt32* adra // Input, size = 1 word(s)
		, const UInt32* dia // Input, size = 3 word(s)
		, const UInt32* wea // Input, size = 1 word(s)
		, const UInt32* bea // Input, size = 1 word(s)
		, UInt32* doa // Output, size = 3 word(s)
		, const UInt32* clkb // Input, size = 1 word(s)
		, const UInt32* adrb // Input, size = 1 word(s)
		, const UInt32* dib // Input, size = 3 word(s)
		, const UInt32* web // Input, size = 1 word(s)
		, const UInt32* beb // Input, size = 1 word(s)
		, UInt32* dob // Output, size = 3 word(s)
	)
{
    carbon_mem2r2w_be_state *state = static_cast<carbon_mem2r2w_be_state*>(hndl);
    UInt32 *temp = new UInt32[state->DATA_WIDTH_32];

    // Sequential code
    switch (context) {
    case (eCDScarbon_mem2r2w_beRiseclka):
	if (*wea) {
	    // Read current data and mask in new data
	    state->mem_array->read(*adra, temp);
	    for (UInt32 j = 0; j < state->DATA_WIDTH_32; ++j) {
		// Each byte has a write enable
		UInt32 mask = 0;
		for (int i = 0; i < 4; ++i)
		    if (*bea & (0x1 << (i + 4 * j)))
			mask |= (0xff << (i * 8));
		temp[j] = (temp[j] & ~mask) | (dia[j] & mask);
	    }
	    state->mem_array->write(*adra, temp);
	}
	state->adra_q = *adra;
	break;
    case (eCDScarbon_mem2r2w_beRiseclkb):
	if (*web) {
	    // Read current data and mask in new data
	    state->mem_array->read(*adrb, temp);
	    for (UInt32 j = 0; j < state->DATA_WIDTH_32; ++j) {
		// Each byte has a write enable
		UInt32 mask = 0;
		for (int i = 0; i < 4; ++i)
		    if (*beb & (0x1 << (i + 4 * j)))
			mask |= (0xff << (i * 8));
		temp[j] = (temp[j] & ~mask) | (dib[j] & mask);
	    }
	    state->mem_array->write(*adrb, temp);
	}
	state->adrb_q = *adrb;
	break;
    }

    // Continuous assigns/outputs
    if (doa)
	state->mem_array->read(state->adra_q, doa);
    if (dob)
	state->mem_array->read(state->adrb_q, dob);

    delete [] temp;
}

extern "C" void cds_carbon_mem2r2w_be_destroy(void* hndl)
{
    carbon_mem2r2w_be_state *state = static_cast<carbon_mem2r2w_be_state*>(hndl);
    delete state;
}


//------------------------
// carbon_mem2r2w_reg
//------------------------

class carbon_mem2r2w_reg_state
{
public:
    carbon_mem2r2w_reg_state(UInt32 depth, UInt32 data_width, UInt32 addr_width, const char* memory_file)
	: DEPTH(depth), DATA_WIDTH(data_width), ADDR_WIDTH(addr_width)
    {
	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;
	mem_array = new carbon_mem_core_array(DEPTH, DATA_WIDTH_32, DATA_WIDTH);
	doa = new UInt32[DATA_WIDTH_32];
	dob = new UInt32[DATA_WIDTH_32];

	if ((memory_file != NULL) && (strlen(memory_file) > 0)) {
	   MEMORY_FILE = new char[strlen(memory_file)];
	   strcpy(MEMORY_FILE, memory_file);
	} else {
	   MEMORY_FILE = NULL;
	}

	memset(doa, 0, DATA_WIDTH_32 * sizeof(UInt32));
	memset(dob, 0, DATA_WIDTH_32 * sizeof(UInt32));
	adra_q = 0;
	adrb_q = 0;
    }
    ~carbon_mem2r2w_reg_state()
    {
	delete mem_array;
	delete [] doa;
	delete [] dob;
	if (MEMORY_FILE) delete [] MEMORY_FILE;
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 ADDR_WIDTH;
    UInt32 DATA_WIDTH_32;
    char *MEMORY_FILE;  // Verilog parameter for readmemh() filename
    carbon_mem_core_array *mem_array;
    UInt32 adra_q;
    UInt32 adrb_q;
    UInt32 *doa;
    UInt32 *dob;
};

extern "C" void* cds_carbon_mem2r2w_reg_create(int numParams, CModelParam* cmodelParams, const char* instName)
{
    carbon_mem2r2w_reg_state *state;
    if (numParams != 4)
	state = new carbon_mem2r2w_reg_state(8, 8, 3, "");
    else
	state = new carbon_mem2r2w_reg_state(atoi(cmodelParams[0].paramValue),
					     atoi(cmodelParams[1].paramValue),
					     atoi(cmodelParams[2].paramValue),
					     cmodelParams[3].paramValue);
    // register so we can do readmems
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem(instName, state->mem_array);

    return static_cast<void*>(state);
}

extern "C" void cds_carbon_mem2r2w_reg_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   // Just after construction we are passed the CarbonObjectID* for reference
   if (reason == eCarbonCModelID && hndl != NULL) {
      carbon_mem2r2w_reg_state *state = static_cast<carbon_mem2r2w_reg_state*>(hndl);

      // save the CarbonObjectID*
      state->mem_array->setCarbonObject(static_cast<CarbonObjectID*>(cmodelData));

      // check if we need to call the readmemh() function to preload the memory
      if (state->MEMORY_FILE != NULL)
	 state->mem_array->readmemh(state->MEMORY_FILE, static_cast<CarbonObjectID*>(cmodelData));
   }
   return;
}


extern "C" void cds_carbon_mem2r2w_reg_run(void* hndl, CDScarbon_mem2r2w_regContext context
		, const UInt32* clka // Input, size = 1 word(s)
		, const UInt32* adra // Input, size = 1 word(s)
		, const UInt32* dia // Input, size = 3 word(s)
		, const UInt32* wea // Input, size = 1 word(s)
		, UInt32* doa // Output, size = 3 word(s)
		, const UInt32* clkb // Input, size = 1 word(s)
		, const UInt32* adrb // Input, size = 1 word(s)
		, const UInt32* dib // Input, size = 3 word(s)
		, const UInt32* web // Input, size = 1 word(s)
		, UInt32* dob // Output, size = 3 word(s)
	)
{
    carbon_mem2r2w_reg_state *state = static_cast<carbon_mem2r2w_reg_state*>(hndl);

    // Sequential code
    switch (context) {
    case (eCDScarbon_mem2r2w_regRiseclka):
	state->mem_array->read(state->adra_q, state->doa);
	if (*wea)
	    state->mem_array->write(*adra, dia);
	state->adra_q = *adra;
	break;
    case (eCDScarbon_mem2r2w_regRiseclkb):
	state->mem_array->read(state->adrb_q, state->dob);
	if (*web)
	    state->mem_array->write(*adrb, dib);
	state->adrb_q = *adrb;
	break;
    }

    // Continuous assigns/outputs
    if (doa)
	memcpy(doa, state->doa, state->DATA_WIDTH_32 * sizeof(UInt32));
    if (dob)
	memcpy(dob, state->dob, state->DATA_WIDTH_32 * sizeof(UInt32));
}

extern "C" void cds_carbon_mem2r2w_reg_destroy(void* hndl)
{
    carbon_mem2r2w_reg_state *state = static_cast<carbon_mem2r2w_reg_state*>(hndl);
    delete state;
}


//------------------------
// carbon_mem2r2w_reg_be
//------------------------

class carbon_mem2r2w_reg_be_state
{
public:
    carbon_mem2r2w_reg_be_state(UInt32 depth, UInt32 data_width, UInt32 be_width, UInt32 addr_width, const char* memory_file)
	: DEPTH(depth), DATA_WIDTH(data_width), ADDR_WIDTH(addr_width)
    {
	// 32-bit max byte enable means 256-bit max data width
	assert(DATA_WIDTH <= 256);
	assert(be_width <= 32);

	DATA_WIDTH_32 = DATA_WIDTH / 32;
	if (DATA_WIDTH % 32)
	    ++DATA_WIDTH_32;
	mem_array = new carbon_mem_core_array(DEPTH, DATA_WIDTH_32, DATA_WIDTH);
	doa = new UInt32[DATA_WIDTH_32];
	dob = new UInt32[DATA_WIDTH_32];

	if ((memory_file != NULL) && (strlen(memory_file) > 0)) {
	   MEMORY_FILE = new char[strlen(memory_file)];
	   strcpy(MEMORY_FILE, memory_file);
	} else {
	   MEMORY_FILE = NULL;
	}

	memset(doa, 0, DATA_WIDTH_32 * sizeof(UInt32));
	memset(dob, 0, DATA_WIDTH_32 * sizeof(UInt32));
	adra_q = 0;
	adrb_q = 0;
    }
    ~carbon_mem2r2w_reg_be_state()
    {
	delete mem_array;
	delete [] doa;
	delete [] dob;
	if (MEMORY_FILE) delete [] MEMORY_FILE;
    }

    UInt32 DEPTH;
    UInt32 DATA_WIDTH;
    UInt32 ADDR_WIDTH;
    UInt32 DATA_WIDTH_32;
    char *MEMORY_FILE;  // Verilog parameter for readmemh() filename
    carbon_mem_core_array *mem_array;
    UInt32 adra_q;
    UInt32 adrb_q;
    UInt32 *doa;
    UInt32 *dob;
};

extern "C" void* cds_carbon_mem2r2w_reg_be_create(int numParams, CModelParam* cmodelParams, const char* instName)
{
    carbon_mem2r2w_reg_be_state *state;
    if (numParams != 5)
	state = new carbon_mem2r2w_reg_be_state(8, 8, 1, 3, "");
    else
	state = new carbon_mem2r2w_reg_be_state(atoi(cmodelParams[0].paramValue),
						atoi(cmodelParams[1].paramValue),
						atoi(cmodelParams[2].paramValue),
						atoi(cmodelParams[3].paramValue),
						cmodelParams[4].paramValue);

    // register for mem access functions
    carbon_mem_internal *ptr = carbon_mem_intf::getPtr();
    ptr->registerMem(instName, state->mem_array);

    return static_cast<void*>(state);
}

extern "C" void cds_carbon_mem2r2w_reg_be_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
   // Just after construction we are passed the CarbonObjectID* for reference
   if (reason == eCarbonCModelID && hndl != NULL) {
      carbon_mem2r2w_reg_be_state *state = static_cast<carbon_mem2r2w_reg_be_state*>(hndl);

      // save the CarbonObjectID*
      state->mem_array->setCarbonObject(static_cast<CarbonObjectID*>(cmodelData));

      // check if we need to call the readmemh() function to preload the memory
      if (state->MEMORY_FILE != NULL)
	 state->mem_array->readmemh(state->MEMORY_FILE, static_cast<CarbonObjectID*>(cmodelData));
   }
   return;
}


extern "C" void cds_carbon_mem2r2w_reg_be_run(void* hndl, CDScarbon_mem2r2w_reg_beContext context
		, const UInt32* clka // Input, size = 1 word(s)
		, const UInt32* adra // Input, size = 1 word(s)
		, const UInt32* dia // Input, size = 3 word(s)
		, const UInt32* wea // Input, size = 1 word(s)
		, const UInt32* bea // Input, size = 1 word(s)
		, UInt32* doa // Output, size = 3 word(s)
		, const UInt32* clkb // Input, size = 1 word(s)
		, const UInt32* adrb // Input, size = 1 word(s)
		, const UInt32* dib // Input, size = 3 word(s)
		, const UInt32* web // Input, size = 1 word(s)
		, const UInt32* beb // Input, size = 1 word(s)
		, UInt32* dob // Output, size = 3 word(s)
	)
{
    carbon_mem2r2w_reg_be_state *state = static_cast<carbon_mem2r2w_reg_be_state*>(hndl);
    UInt32 *temp = new UInt32[state->DATA_WIDTH_32];

    // Sequential code
    switch (context) {
    case (eCDScarbon_mem2r2w_reg_beRiseclka):
	state->mem_array->read(state->adra_q, state->doa);
	if (*wea) {
	    // Read current data and mask in new data
	    state->mem_array->read(*adra, temp);
	    for (UInt32 j = 0; j < state->DATA_WIDTH_32; ++j) {
		// Each byte has a write enable
		UInt32 mask = 0;
		for (int i = 0; i < 4; ++i)
		    if (*bea & (0x1 << (i + 4 * j)))
			mask |= (0xff << (i * 8));
		temp[j] = (temp[j] & ~mask) | (dia[j] & mask);
	    }
	    state->mem_array->write(*adra, temp);
	}
	state->adra_q = *adra;
	break;
    case (eCDScarbon_mem2r2w_reg_beRiseclkb):
	state->mem_array->read(state->adrb_q, state->dob);
	if (*web) {
	    // Read current data and mask in new data
	    state->mem_array->read(*adrb, temp);
	    for (UInt32 j = 0; j < state->DATA_WIDTH_32; ++j) {
		// Each byte has a write enable
		UInt32 mask = 0;
		for (int i = 0; i < 4; ++i)
		    if (*beb & (0x1 << (i + 4 * j)))
			mask |= (0xff << (i * 8));
		temp[j] = (temp[j] & ~mask) | (dib[j] & mask);
	    }
	    state->mem_array->write(*adrb, temp);
	}
	state->adrb_q = *adrb;
	break;
    }

    // Continuous assigns/outputs
    if (doa)
	memcpy(doa, state->doa, state->DATA_WIDTH_32 * sizeof(UInt32));
    if (dob)
	memcpy(dob, state->dob, state->DATA_WIDTH_32 * sizeof(UInt32));

    delete [] temp;
}

extern "C" void cds_carbon_mem2r2w_reg_be_destroy(void* hndl)
{
    carbon_mem2r2w_reg_be_state *state = static_cast<carbon_mem2r2w_reg_be_state*>(hndl);
    delete state;
}


