module carbon_mem1r1w_we(clka,clkb,adra,adrb,web,dib,mea,meb,doa);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter ADDR_WIDTH = 3;
parameter MEMORY_FILE = "";

input clka,clkb;
input [ ADDR_WIDTH - 1:0 ] adra,adrb;
input [ DATA_WIDTH - 1:0 ] dib;
input mea,meb;
input [ DATA_WIDTH - 1:0 ] web;
output [ DATA_WIDTH - 1:0 ] doa;

endmodule

module carbon_mem1r1w(clka, adra, dia, wea, clkb, adrb, dob, reb);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter ADDR_WIDTH = 3;
parameter MEMORY_FILE = "";

input clka, clkb;
input [ ADDR_WIDTH - 1:0 ] adra, adrb;
input [ DATA_WIDTH - 1:0 ] dia;
input wea;
input reb;
output [ DATA_WIDTH - 1:0 ] dob;
endmodule

module carbon_mem1r1w_var(clka, adra, dia, wea, clkb, adrb, dob, reb);
parameter WRITE_DEPTH = 8;
parameter WRITE_DATA_WIDTH = 64;
parameter WRITE_ADDR_WIDTH = 3;
parameter READ_DEPTH = 8;
parameter READ_DATA_WIDTH = 64;
parameter READ_ADDR_WIDTH = 3;
parameter MEMORY_FILE = "";

input clka, clkb;
input [ WRITE_ADDR_WIDTH - 1:0 ] adra;
input [ READ_ADDR_WIDTH - 1:0 ] adrb;
input [ WRITE_DATA_WIDTH - 1:0 ] dia;
input wea;
input reb;
output [ READ_DATA_WIDTH - 1:0 ] dob;
endmodule

module carbon_mem1r1w_reg(clka, adra, dia, wea, clkb, adrb, dob, reb);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter ADDR_WIDTH = 3;
parameter MEMORY_FILE = "";

input clka, clkb;
input [ ADDR_WIDTH - 1:0 ] adra, adrb;
input [ DATA_WIDTH - 1:0 ] dia;
input wea;
input reb;
output [ DATA_WIDTH - 1:0 ] dob;
endmodule

module carbon_mem2r2w(clka, adra, dia, wea, doa, clkb, adrb, dib, web, dob);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter ADDR_WIDTH = 3;
parameter MEMORY_FILE = "";

input clka, clkb;
input [ ADDR_WIDTH - 1:0 ] adra, adrb;
input [ DATA_WIDTH - 1:0 ] dia, dib;
input wea, web;
output [ DATA_WIDTH - 1:0 ] doa, dob;
endmodule

module carbon_mem2r2w_reg(clka, adra, dia, wea, doa, clkb, adrb, dib, web, dob);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter ADDR_WIDTH = 3;
parameter MEMORY_FILE = "";

input clka, clkb;
input [ ADDR_WIDTH - 1:0 ] adra, adrb;
input [ DATA_WIDTH - 1:0 ] dia, dib;
input wea, web;
output [ DATA_WIDTH - 1:0 ] doa, dob;
endmodule

module carbon_mem2r2w_be(clka, adra, dia, wea, bea, doa, clkb, adrb, dib, web, beb, dob);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter BE_WIDTH = 1;  // Byte Enable width must be 32 bits or less
parameter ADDR_WIDTH = 3;
parameter MEMORY_FILE = "";

input clka, clkb;
input [ ADDR_WIDTH - 1:0 ] adra, adrb;
input [ DATA_WIDTH - 1:0 ] dia, dib;
input wea, web;
input [ BE_WIDTH - 1:0 ] bea, beb;
output [ DATA_WIDTH - 1:0 ] doa, dob;
endmodule

module carbon_mem2r2w_reg_be(clka, adra, dia, wea, bea, doa, clkb, adrb, dib, web, beb, dob);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter BE_WIDTH = 1;  // Byte Enable width must be 32 bits or less
parameter ADDR_WIDTH = 3;
parameter MEMORY_FILE = "";

input clka, clkb;
input [ ADDR_WIDTH - 1:0 ] adra, adrb;
input [ DATA_WIDTH - 1:0 ] dia, dib;
input wea, web;
input [ BE_WIDTH - 1:0 ] bea, beb;
output [ DATA_WIDTH - 1:0 ] doa, dob;
endmodule

module carbon_mem1rw_reg_var(clk, adrw, adrr, di, we, do);
parameter WRITE_DEPTH = 8;
parameter WRITE_DATA_WIDTH = 64;
parameter WRITE_ADDR_WIDTH = 3;
parameter READ_DEPTH = 8;
parameter READ_DATA_WIDTH = 64;
parameter READ_ADDR_WIDTH = 3;
parameter MEMORY_FILE = "";

input clk;
input [ WRITE_ADDR_WIDTH - 1:0 ] adrw;
input [ READ_ADDR_WIDTH - 1:0 ] adrr;
input [ WRITE_DATA_WIDTH - 1:0 ] di;
input we;
output [ READ_DATA_WIDTH - 1:0 ] do;
endmodule

module carbon_mem1rw_var(clk, adrw, adrr, di, we, do);
parameter WRITE_DEPTH = 8;
parameter WRITE_DATA_WIDTH = 64;
parameter WRITE_ADDR_WIDTH = 3;
parameter READ_DEPTH = 8;
parameter READ_DATA_WIDTH = 64;
parameter READ_ADDR_WIDTH = 3;
parameter MEMORY_FILE = "";

input clk;
input [ WRITE_ADDR_WIDTH - 1:0 ] adrw;
input [ READ_ADDR_WIDTH - 1:0 ] adrr;
input [ WRITE_DATA_WIDTH - 1:0 ] di;
input we;
output [ READ_DATA_WIDTH - 1:0 ] do;
endmodule

