module carbon_fifo_sc(clk, rst, data_in, push, pop, data_out, used, full, empty, a_full, a_empty);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter USED_WIDTH = 3;
parameter ALMOST_FULL = 7;
parameter ALMOST_EMPTY = 1;
   
input clk, rst;
input [ DATA_WIDTH - 1:0 ] data_in;
input push, pop;
output [ DATA_WIDTH - 1:0 ] data_out;
output [ USED_WIDTH - 1:0 ] used;
output full, empty, a_full, a_empty;
endmodule

module carbon_fifo_dc(clkr, clkw, rst, data_in, push, pop, data_out, used_r, used_w, full_r, full_w, empty_r, empty_w);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter USED_WIDTH = 3;
   
input clkr, clkw, rst;
input [ DATA_WIDTH - 1:0 ] data_in;
input push, pop;
output [ DATA_WIDTH - 1:0 ] data_out;
output [ USED_WIDTH - 1:0 ] used_r, used_w;
output full_r, full_w, empty_r, empty_w;
endmodule

module carbon_fifo_sc_sa(clk, rst, data_in, push, pop, data_out, used, full, empty, a_full, a_empty);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter USED_WIDTH = 3;
parameter ALMOST_FULL = 7;
parameter ALMOST_EMPTY = 1;
   
input clk, rst;
input [ DATA_WIDTH - 1:0 ] data_in;
input push, pop;
output [ DATA_WIDTH - 1:0 ] data_out;
output [ USED_WIDTH - 1:0 ] used;
output full, empty, a_full, a_empty;
endmodule

module carbon_fifo_dc_sa(clkr, clkw, rst, data_in, push, pop, data_out, used_r, used_w, full_r, full_w, empty_r, empty_w);
parameter DEPTH = 8;
parameter DATA_WIDTH = 64;
parameter USED_WIDTH = 3;
   
input clkr, clkw, rst;
input [ DATA_WIDTH - 1:0 ] data_in;
input push, pop;
output [ DATA_WIDTH - 1:0 ] data_out;
output [ USED_WIDTH - 1:0 ] used_r, used_w;
output full_r, full_w, empty_r, empty_w;
endmodule

