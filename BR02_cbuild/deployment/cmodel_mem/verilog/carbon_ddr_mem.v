/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

module carbon_ddr_mem(clk,
		      Dq_in,
		      Dq_out,
		      Dq_en,
		      Dqs_in,
		      Dqs_out,
		      Dqs_en,
		      Addr,
		      Ba,
		      Cs_n,
		      Ras_n,
		      Cas_n,
		      We_n,
		      Dm);

   
   parameter addr_bits =       13;
   parameter data_bits =      144;
   parameter cols_bits =       12;
   parameter mask_bits =       2;
   input     clk;
   input [data_bits - 1 : 0] Dq_in;
   output [data_bits - 1 : 0] Dq_out;
   output 		      Dq_en;
   input 		      Dqs_in;
   output 		      Dqs_out;
   output 		      Dqs_en;
   input [addr_bits - 1 : 0]  Addr;
   input [1 : 0] 	      Ba;
   input 		      Cs_n;
   input 		      Ras_n;
   input 		      Cas_n;
   input 		      We_n;
   input [mask_bits - 1 : 0]  Dm;
endmodule // carbon_ddr_mem

module carbon_ddr2_mem(clk,
		       Dq_in,
		       Dq_out,
		       Dq_en,
		       Dqs_in,
		       Dqs_out,
		       Dqs_en,
		       Addr,
		       Ba,
		       Cs_n,
		       Ras_n,
		       Cas_n,
		       We_n,
		       Dm);

   
   parameter addr_bits =       13;
   parameter data_bits =      144;
   parameter cols_bits =       12;
   parameter mask_bits =       2;
   parameter bank_bits =       2;
   parameter bank_access =     1;
   parameter mem_name = "mem";
   
   input     clk;
   input [data_bits - 1 : 0] Dq_in;
   output [data_bits - 1 : 0] Dq_out;
   output 		      Dq_en;
   input 		      Dqs_in;
   output 		      Dqs_out;
   output 		      Dqs_en;
   input [addr_bits - 1 : 0]  Addr;
   input [bank_bits - 1 : 0]  Ba;
   input 		      Cs_n;
   input 		      Ras_n;
   input 		      Cas_n;
   input 		      We_n;
   input [mask_bits - 1 : 0]  Dm;
endmodule // carbon_ddr2_mem
