/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __BITVECTOR_H__
#define __BITVECTOR_H__

#include "carbon/carbon.h"

class BitVector
{
public:
    BitVector(UInt32 bits, const UInt32 *data = 0);
    ~BitVector();

    UInt32 getNumWords() { return mNumWords; }
    UInt32 *getArray() { return mData; }
    UInt32 *getRange(UInt32 start, UInt32 bits);
    void fill(const UInt32 *data);
    void fillRange(UInt32 *data, UInt32 start, UInt32 bits);

private:
    const UInt32 mNumBits;
    const UInt32 mNumWords;
    UInt32 *mData, *mTmpData;
};

#endif
