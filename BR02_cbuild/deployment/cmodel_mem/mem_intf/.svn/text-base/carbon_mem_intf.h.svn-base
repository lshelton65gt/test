/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBON_MEM_INTF_H__
#define __CARBON_MEM_INTF_H__

// Interface to cmodel memories
#include "carbon/carbon.h"

typedef void* carbon_mem_handle;

class carbon_mem_internal;

class carbon_mem_intf
{
public:
    carbon_mem_intf() {}
    ~carbon_mem_intf() {}

    // name-based access
    static UInt32 getNumWords(const char *hdl_path);
    static bool read(const char *hdl_path, UInt32 addr, UInt32 *return_data);
    static bool write(const char *hdl_path, UInt32 addr, UInt32 *write_data);
    static bool readmemh(const char *hdl_path, const char *file, CarbonObjectID *obj);
    static bool setUninitializedValue(const char *hdl_path, const UInt32 *init_data);

    // handle-based access
    static carbon_mem_handle getHandle(const char *hdl_path);
    static UInt32 getNumWordsHandle(carbon_mem_handle h);
    static bool readHandle(carbon_mem_handle h, UInt32 addr, UInt32 *return_data);
    static bool writeHandle(carbon_mem_handle h, UInt32 addr, UInt32 *write_data);
    static bool readmemhHandle(carbon_mem_handle h, const char *file, CarbonObjectID *obj);
    static bool setUninitializedValueHandle(carbon_mem_handle h, const UInt32 *init_data);

    static carbon_mem_internal *getPtr();
    static void cleanup();

private:
    static carbon_mem_internal *mPtr;
};

#endif
