/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "bitvector.h"
#include <cstring>
#include <cassert>

BitVector::BitVector(UInt32 bits, const UInt32 *data)
    : mNumBits(bits), mNumWords((bits + 31) / 32)
{
    mData = new UInt32[mNumWords];
    mTmpData = new UInt32[mNumWords];

    if (data)
	fill(data);
    else
	memset(mData, 0, mNumWords * sizeof(UInt32));

    memset(mTmpData, 0, mNumWords * sizeof(UInt32));
}

BitVector::~BitVector()
{
    delete [] mData;
    delete [] mTmpData;
}

UInt32 *BitVector::getRange(UInt32 start, UInt32 bits)
{
    // Make sure it's legal
    assert(start + bits <= mNumBits);
    assert(bits != 0);

    // See if we're really lucky!
    if (((start % 32) == 0) && ((bits % 32) == 0))
	return &mData[start / 32];

    // Now we have to shift... here's the idea:
    // - start with the first word that has data we need
    // - combine it and the following word into a 64-bit number
    // - shift the required amount, and transfer the lower 32 bits into the temp array
    // - move to the next word and repeat

    UInt32 bits_left = bits;
    UInt32 src_word = start / 32;
    UInt32 dst_word = 0;
    UInt32 shift_amt = start % 32;

    // preload - the loop assumes the bottom 32 bits are ready when it enters
    UInt64 temp = mData[src_word];
    while (bits_left) {
	// Lower bits are already in place - only copy in the upper half if we need the data
	if (bits_left + shift_amt > 32)
	    temp |= static_cast<UInt64>(mData[src_word + 1]) << 32;
	// Shift and copy to dest
	mTmpData[dst_word] = static_cast<UInt32>(temp >> shift_amt);
	// Mask if this is the last iter and we need to
	if (bits_left < 32) {
	    mTmpData[dst_word] &= (0xffffffffU >> (32 - bits_left));
	    bits_left = 0;
	} else
	    bits_left -= 32;

	// Shift upper bits into place for next iter
	temp >>= 32;
	++src_word;
	++dst_word;
    }

    return mTmpData;
}

void BitVector::fill(const UInt32 *data)
{
    memcpy(mData, data, mNumWords * sizeof(UInt32));
}

void BitVector::fillRange(UInt32 *data, UInt32 start, UInt32 bits)
{
    // Make sure it's legal
    assert(start + bits <= mNumBits);
    assert(bits != 0);

    // Again, see if we're really lucky!
    if (((start % 32) == 0) && ((bits % 32) == 0)) {
	memcpy(&mData[start/32], data, (bits / 8));
	return;
    }

    // See if we're kinda lucky
    if ((start % 32) == 0) {
	UInt32 whole_words = bits / 32;
	UInt32 extra_bits = bits % 32;
	memcpy(&mData[start / 32], data, whole_words * sizeof(UInt32));
	UInt32 temp = data[whole_words];
	temp &= (0xffffffffU >> (32 - extra_bits));
	mData[start / 32 + whole_words] = temp;
	return;
    }

    // Crap, this is gonna be ugly
    // We'll do something like we did in getRange()
    UInt32 bits_left = bits;
    UInt32 src_word = 0;
    UInt32 dst_word = start / 32;
    UInt32 shift_amt = start % 32;

    // preload - the loop assumes the lower 32 bits are ready when it enters
    UInt64 temp = 0;
    while (bits_left) {
	// Lower bits are already in place - only copy in the upper half if we need the data
	if (bits_left > shift_amt)
	    temp |= static_cast<UInt64>(data[src_word]) << 32;
	// Shift and copy to dest
	UInt32 shifted_word = static_cast<UInt32>(temp >> (32 - shift_amt));	// lsh by amount, then grab upper 32 bits
	UInt32 bits_written = 32;
	if ((src_word != 0) && (bits_left >= 32))
	    mData[dst_word] = shifted_word;	// Full word, nothing to worry about
	else {
	    // We need to keep either the beginning or ending bits (or both!) of the destination region
	    UInt32 mask = 0;
	    if (bits_left < 32)	{	// If the last word, keep some high bits
 		mask = 0xffffffffU << bits_left;
	    }
	    if (src_word == 0) {	// If the first word, keep some low bits
		mask <<= shift_amt;	// move bits_left mask over to match
		mask |= (0xffffffffU >> (32 - shift_amt));
	    }
	    shifted_word &= ~mask;	// we never ensured that the upper unused bits were clean
	    // Set correct number of bits written
	    if ((shift_amt + bits_left) < 32)
		bits_written = bits_left;	// Fit everything in
	    else
		bits_written = 32 - shift_amt;

	    // Mask original data and copy ours in;
	    mData[dst_word] &= mask;
	    mData[dst_word] |= shifted_word;
	}

	bits_left -= bits_written;

	// Shift upper bits into place for next iter
	temp >>= 32;
	++src_word;
	++dst_word;
    }    
}

