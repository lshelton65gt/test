/*****************************************************************************

 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "carbon_mem_core_map.h"
#include <cstring>

carbon_mem_core_map::carbon_mem_core_map(UInt32 memsize, UInt32 wordsize, UInt32 numbits)
{
    mMemSize = memsize;
    mWordSize = wordsize;
    mNumBits = numbits;
    mInitData = new UInt32[mWordSize];
    memset(mInitData, 0, mWordSize * sizeof(UInt32));
}

carbon_mem_core_map::~carbon_mem_core_map()
{
    // clean up the memory
    for (carbon_mem_core_map_storage::iterator iter = mMap.begin(); iter != mMap.end(); ++iter)
	delete [] iter->second;
    delete [] mInitData;
}


UInt32 *carbon_mem_core_map::getMemWord(UInt32 addr)
{
    if (addr >= mMemSize)
	return 0;

    carbon_mem_core_map_storage::iterator iter = mMap.find(addr);
    if (iter == mMap.end()) {
	// not found, add entry
	UInt32 *temp = new UInt32[mWordSize];
	iter = (mMap.insert(std::pair<UInt32, UInt32*>(addr, temp))).first;
	// init word, since we don't know how it'll be used
// 	memset(iter->second, 0, mWordSize * sizeof(UInt32));
	memcpy(iter->second, mInitData, mWordSize * sizeof(UInt32));
    }

    return iter->second;
}

bool carbon_mem_core_map::setUninitializedValue(const UInt32 *init_data)
{
    memcpy(mInitData, init_data, mWordSize * sizeof(UInt32));

    return true;
}
