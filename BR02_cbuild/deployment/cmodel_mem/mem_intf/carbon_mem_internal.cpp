/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "carbon_mem_internal.h"
#include "carbon_mem_core.h"
#include <iostream>

carbon_mem_internal::carbon_mem_internal()
{
}

carbon_mem_internal::~carbon_mem_internal()
{
    for (UtMap<UtString, mem_info *>::iterator iter = mMems.begin(); iter != mMems.end(); ++iter)
	delete iter->second;
}

void carbon_mem_internal::registerMem(const char *hdl_path, carbon_mem_core *mem_core)
{
    mem_info *info = new mem_info(hdl_path, mem_core);
    mMems[hdl_path] = info;
}

UInt32 carbon_mem_internal::getNumWords(const char *hdl_path)
{
    carbon_mem_core *mem = getMem(hdl_path);
    if (!mem)
	return 0;

    return mem->getWordSize();
}

bool carbon_mem_internal::read(const char *hdl_path, UInt32 addr, UInt32 *return_data)
{
    carbon_mem_core *mem = getMem(hdl_path);
    if (!mem)
	return false;
 
    return mem->read(addr, return_data);
}

bool carbon_mem_internal::write(const char *hdl_path, UInt32 addr, UInt32 *write_data)
{
    carbon_mem_core *mem = getMem(hdl_path);
    if (!mem)
	return false;

    return mem->write(addr, write_data);
}

bool carbon_mem_internal::readmemh(const char *hdl_path, const char *file, CarbonObjectID *obj)
{
    carbon_mem_core *mem = getMem(hdl_path);
    if (!mem)
	return false;

    return mem->readmemh(file, obj);
}

bool carbon_mem_internal::setUninitializedValue(const char *hdl_path, const UInt32 *init_data)
{
    carbon_mem_core *mem = getMem(hdl_path);
    if (!mem)
	return false;

    return mem->setUninitializedValue(init_data);
}

carbon_mem_core *carbon_mem_internal::getMem(const char *hdl_path)
{
    // See if we can find the mem in our recently-used list
    UtList<mem_info *>::iterator cache_iter = mMemCache.begin();
    while ((cache_iter != mMemCache.end()) && 
	   ((*cache_iter)->name != hdl_path))
	++cache_iter;

    if (cache_iter != mMemCache.end()) {
	// Woo-hoo!
	carbon_mem_core *ret_mem = (*cache_iter)->mem;

	// Move to the front of the list
	if (cache_iter != mMemCache.begin()) {
	    // Is there a better way to do this?
	    mMemCache.push_front(*cache_iter);
	    mMemCache.erase(cache_iter);
	}

	return ret_mem;
    }

    UtMap<UtString, mem_info *>::iterator iter = mMems.find(hdl_path);

    if (iter == mMems.end())
	return 0;

    // Save this in our list, ditch the last one
    if (mMemCache.size() >= scCacheSize)
	mMemCache.pop_back();
    mMemCache.push_front(iter->second);

    return iter->second->mem;
}

UInt32 carbon_mem_internal::getNumWords(carbon_mem_core *mem)
{
    return mem->getWordSize();
}

bool carbon_mem_internal::read(carbon_mem_core *mem, UInt32 addr, UInt32 *return_data)
{
    return mem->read(addr, return_data);
}

bool carbon_mem_internal::write(carbon_mem_core *mem, UInt32 addr, UInt32 *write_data)
{
    return mem->write(addr, write_data);
}

bool carbon_mem_internal::readmemh(carbon_mem_core *mem, const char *file, CarbonObjectID *obj)
{
    return mem->readmemh(file, obj);
}

bool carbon_mem_internal::setUninitializedValue(carbon_mem_core *mem, const UInt32 *init_data)
{
    return mem->setUninitializedValue(init_data);
}

carbon_mem_internal::mem_info::mem_info(const char *n, carbon_mem_core *mem_core)
    : name(n), mem(mem_core)
{
}

carbon_mem_internal::mem_info::~mem_info()
{
}

