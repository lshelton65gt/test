/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBON_MEM_CORE_ARRAY_H__
#define __CARBON_MEM_CORE_ARRAY_H__

#include "carbon_mem_core.h"

class carbon_mem_core_array : public carbon_mem_core
{
public:
    carbon_mem_core_array(UInt32 memsize, UInt32 wordsize, UInt32 numbits);
    virtual ~carbon_mem_core_array();

    virtual UInt32 *getMemWord(UInt32 addr);
    virtual bool setUninitializedValue(const UInt32 *init_data);

protected:
    UInt32 *mArray;
};

#endif
