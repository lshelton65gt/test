/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBON_MEM_INTERNAL_H__
#define __CARBON_MEM_INTERNAL_H__

#include "carbon/carbon.h"
#include "util/UtMap.h"
#include "util/UtString.h"
#include "util/UtList.h"

class carbon_mem_core;

class carbon_mem_internal
{
public:
    carbon_mem_internal();
    ~carbon_mem_internal();

    class mem_info
    {
    public:
	mem_info(const char *n, carbon_mem_core *mem_core);
	~mem_info();
	UtString name;
	carbon_mem_core *mem;
    };

    void registerMem(const char *hdl_path, carbon_mem_core *mem_core);
    UInt32 getNumWords(const char *hdl_path);
    bool read(const char *hdl_path, UInt32 addr, UInt32 *return_data);
    bool write(const char *hdl_path, UInt32 addr, UInt32 *write_data);
    bool readmemh(const char *hdl_path, const char *file, CarbonObjectID *obj);
    bool setUninitializedValue(const char *hdl_path, const UInt32 *init_data);

    carbon_mem_core *getMem(const char *hdl_path);
    UInt32 getNumWords(carbon_mem_core *mem);
    bool read(carbon_mem_core *mem, UInt32 addr, UInt32 *return_data);
    bool write(carbon_mem_core *mem, UInt32 addr, UInt32 *write_data);
    bool readmemh(carbon_mem_core *mem, const char *file, CarbonObjectID *obj);
    bool setUninitializedValue(carbon_mem_core *mem, const UInt32 *init_data);

private:
    UtMap<UtString, mem_info *> mMems;
    static const UInt32 scCacheSize = 5;
    UtList<mem_info *> mMemCache;
};

#endif
