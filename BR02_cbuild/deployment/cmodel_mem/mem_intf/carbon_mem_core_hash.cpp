/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "carbon_mem_core_hash.h"
#include <iostream>

carbon_mem_core_hash::carbon_mem_core_hash(UInt32 memsize, UInt32 wordsize, UInt32 numbits)
{
    mMemSize = memsize;
    mWordSize = wordsize;
    mNumBits = numbits;
    mInitData = new UInt32[mWordSize];
    memset(mInitData, 0, mWordSize * sizeof(UInt32));
}

carbon_mem_core_hash::~carbon_mem_core_hash()
{
    // clean up the memory
    for (carbon_mem_core_hash_storage::iterator iter = mHash.begin(); iter != mHash.end(); ++iter)
	delete [] iter->second;

    delete [] mInitData;
}


UInt32 *carbon_mem_core_hash::getMemWord(UInt32 addr)
{
    if (addr >= mMemSize)
	return 0;

    carbon_mem_core_hash_storage::iterator iter = mHash.find(addr);
    if (iter == mHash.end()) {
	// not found, add entry
	UInt32 *temp = new UInt32[mWordSize];
	iter = (mHash.insert(std::pair<UInt32, UInt32*>(addr, temp))).first;
	// init word, since we don't know how it'll be used
// 	memset(iter->second, 0, mWordSize * sizeof(UInt32));
	memcpy(iter->second, mInitData, mWordSize * sizeof(UInt32));
    }

    return iter->second;
}

bool carbon_mem_core_hash::setUninitializedValue(const UInt32 *init_data)
{
    memcpy(mInitData, init_data, mWordSize * sizeof(UInt32));

    return true;
}
