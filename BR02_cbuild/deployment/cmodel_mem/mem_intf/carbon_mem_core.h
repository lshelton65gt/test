/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBON_MEM_CORE_H__
#define __CARBON_MEM_CORE_H__

#include "carbon/carbon.h"

class carbon_mem_core
{
public:
    carbon_mem_core() : mMemSize(0), mWordSize(0), mNumBits(0), mCarbonObj(0) {}
    virtual ~carbon_mem_core() {}

    UInt32 getMemSize() { return mMemSize; }
    UInt32 getWordSize() { return mWordSize; }
    virtual UInt32 *getMemWord(UInt32 addr) = 0;
    void setCarbonObject(CarbonObjectID *obj) { mCarbonObj = obj; }
    CarbonObjectID* getCarbonObject() { return mCarbonObj; }

    bool read(UInt32 addr, UInt32 *return_data);
    bool write(UInt32 addr, const UInt32 *write_data);
    bool readmemh(const char *file, CarbonObjectID *obj);

    // This needs to be implemented by the the derived class,
    // depending on implementation.
    // An array-based implementation will simply fill the array
    // with the data, but a map-based implementation will just
    // save the data and return it when an unmapped read occurs.
    virtual bool setUninitializedValue(const UInt32 *init_data) = 0;

protected:
    UInt32 mMemSize, mWordSize, mNumBits;
    CarbonObjectID* mCarbonObj;
};

#endif
