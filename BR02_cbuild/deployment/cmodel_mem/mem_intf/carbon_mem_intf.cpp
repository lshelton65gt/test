/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "carbon_mem_internal.h"
#include "carbon_mem_intf.h"

// Static member pointer
carbon_mem_internal *carbon_mem_intf::mPtr = NULL;

carbon_mem_internal *carbon_mem_intf::getPtr()
{
    if (!mPtr)
	mPtr = new carbon_mem_internal;
    return mPtr;
}

void carbon_mem_intf::cleanup()
{
    if (mPtr)
	delete mPtr;
}

UInt32 carbon_mem_intf::getNumWords(const char *hdl_path)
{
    if (!mPtr)
	return 0;

    return (mPtr->getNumWords(hdl_path));
}

bool carbon_mem_intf::read(const char *hdl_path, UInt32 addr, UInt32 *return_data)
{
    if (!mPtr)
	return false;

    return (mPtr->read(hdl_path, addr, return_data));
}

bool carbon_mem_intf::write(const char *hdl_path, UInt32 addr, UInt32 *write_data)
{
    if (!mPtr)
	return false;

    return (mPtr->write(hdl_path, addr, write_data));
}

bool carbon_mem_intf::readmemh(const char *hdl_path, const char *file, CarbonObjectID *obj)
{
    if (!mPtr)
	return false;

    return (mPtr->readmemh(hdl_path, file, obj));
}

bool carbon_mem_intf::setUninitializedValue(const char *hdl_path, const UInt32 *init_data)
{
    if (!mPtr)
	return false;

    return (mPtr->setUninitializedValue(hdl_path, init_data));
}

carbon_mem_handle carbon_mem_intf::getHandle(const char *hdl_path)
{
    if (!mPtr)
	return false;

    return reinterpret_cast<carbon_mem_handle>(mPtr->getMem(hdl_path));
}

UInt32 carbon_mem_intf::getNumWordsHandle(carbon_mem_handle h)
{
    if (!mPtr)
	return false;

    return (mPtr->getNumWords(reinterpret_cast<carbon_mem_core *>(h)));
}

bool carbon_mem_intf::readHandle(carbon_mem_handle h, UInt32 addr, UInt32 *return_data)
{
    if (!mPtr)
	return false;

    return (mPtr->read(reinterpret_cast<carbon_mem_core *>(h), addr, return_data));
}

bool carbon_mem_intf::writeHandle(carbon_mem_handle h, UInt32 addr, UInt32 *write_data)
{
    if (!mPtr)
	return false;

    return (mPtr->write(reinterpret_cast<carbon_mem_core *>(h), addr, write_data));
}

bool carbon_mem_intf::readmemhHandle(carbon_mem_handle h, const char *file, CarbonObjectID *obj)
{
    if (!mPtr)
	return false;

    return (mPtr->readmemh(reinterpret_cast<carbon_mem_core *>(h), file, obj));
}

bool carbon_mem_intf::setUninitializedValueHandle(carbon_mem_handle h, const UInt32 *init_data)
{
    if (!mPtr)
	return false;

    return (mPtr->setUninitializedValue(reinterpret_cast<carbon_mem_core *>(h), init_data));
}

