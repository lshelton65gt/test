/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "carbon_mem_core_array.h"
#include <cstring>

carbon_mem_core_array::carbon_mem_core_array(UInt32 memsize, UInt32 wordsize, UInt32 numbits)
{
    mMemSize = memsize;
    mWordSize = wordsize;
    mNumBits = numbits;
    mArray = new UInt32[mMemSize * mWordSize];
    memset(mArray, 0, mMemSize * mWordSize);
}

carbon_mem_core_array::~carbon_mem_core_array()
{
    // clean up the memory
    delete [] mArray;
}


UInt32 *carbon_mem_core_array::getMemWord(UInt32 addr)
{
    if (addr >= mMemSize)
	return 0;

    return &mArray[addr * mWordSize];
}

bool carbon_mem_core_array::setUninitializedValue(const UInt32 *init_data)
{
    // Fill 'er up!
    for (UInt32 i = 0; i < mMemSize; ++i)
	memcpy(&mArray[i * mWordSize], init_data, mWordSize * sizeof(UInt32));

    return true;
}
