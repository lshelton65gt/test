/*****************************************************************************

 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "carbon_mem_core.h"
//#if 0
#define __CARBON_SYS_INCLUDE_H__
//#include "hdl/ReadMemX.h"
//#include "shell/CarbonModel.h"
#include "carbon/carbon_capi.h"
//#endif
#include <cstring>
#include <cassert>

bool carbon_mem_core::read(UInt32 addr, UInt32 *return_data)
{
    if (addr >= mMemSize) 
	return false;

    UInt32 *word = getMemWord(addr);
    memcpy(return_data, word, mWordSize * sizeof(UInt32));

    return true;
}

bool carbon_mem_core::write(UInt32 addr, const UInt32 *write_data)
{
    if (addr >= mMemSize)
	return false;

    UInt32 *word = getMemWord(addr);
    memcpy(word, write_data, mWordSize * sizeof(UInt32));

    return true;
}

bool carbon_mem_core::readmemh(const char *file, CarbonObjectID *obj)
{
  CarbonMemFileID* rm = carbonReadMemFile(obj, file, eCarbonHex, mNumBits, false);
  if (rm == NULL)
    return false;
  
  CarbonSInt64 addr, lastAddr;
  carbonMemFileGetFirstAndLastAddrs(rm, &addr, &lastAddr);
  
  UInt32 *word;
  for (; addr <= lastAddr; ++addr)
  {
    const CarbonUInt32* row = carbonMemFileGetRow(rm, addr);
    word = getMemWord(addr);
    memcpy(word, row, mWordSize * sizeof(UInt32));
  }
  return true;
}

