/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// $Source: /cvs/repository/deployment/memories/amcc/qdr-k7r641884m/k7r641884m.v,v $
// $Id: k7r641884m.v,v 1.1 2006/09/22 20:16:40 ronk Exp $

module K7R641884M (D, Q, SA, R_N, W_N, BW_N, K, K_N, C, C_N, CQ, CQ_N, DOFF_N);

// ---------- input/output ----------

 input [17 : 0] D;
output [17 : 0] Q;
 input [19 : 0] SA;
 input          R_N;
 input          W_N;
 input [ 1 : 0] BW_N;
 input          K, K_N;
 input          C, C_N;
output          CQ, CQ_N;
 input          DOFF_N;

wire   [17 : 0] D;
wire   [19 : 0] SA;
wire   [ 1 : 0] BW_N;

wire R_N, W_N, DOFF_N; 
wire K  , K_N, C, C_N; // clocks

wire CQ   = (C & C_N) ? K   : C;
wire CQ_N = (C & C_N) ? K_N : C_N;

// ---------- internal ----------

reg [17:0]
  qout,
  mem0 [0 : (1 << 20) - 1 ], 
  mem1 [0 : (1 << 20) - 1 ], 
  mem2 [0 : (1 << 20) - 1 ], 
  mem3 [0 : (1 << 20) - 1 ]; // 4 : 1M x 18 RAMs

reg [19 : 0] rd_addr, rd_next;
reg [19 : 0] wr_addr, wr_next;

reg tristate, rd_cycle;

wire [17 : 0] Q = tristate ? 18'bz : qout; // data output

parameter // cmd state machine 
  RD_WR = 2'b00,
  READ  = 2'b01,
  WRITE = 2'b10,
  NOP   = 2'b11;

reg [1:0] cmd;

parameter // read state machine 
  RD0    = 3'b000,
  RD1    = 3'b001,
  RD2    = 3'b010,
  RD3    = 3'b011,
  RD_NOP = 3'b111;

reg [2:0] rstate;

parameter // write state machine 
  WR0    = 3'b000,
  WR1    = 3'b001,
  WR2    = 3'b010,
  WR3    = 3'b011,
  WR_NOP = 3'b111;

reg [2:0] wstate;

// initialize

initial
  begin
    cmd      <=    NOP;
    rstate   <= RD_NOP;
    wstate   <= WR_NOP;
    tristate <= 1;
  end

// decode read, write, nop commands

always @(posedge K)
  case( {R_N, W_N} )
    RD_WR: // special case 
      if( cmd == WRITE )
        begin
          cmd     <= READ;
          rd_next <= SA;
        end
      else
        begin
          cmd     <= WRITE;
          wr_next <= SA;
        end

    READ: 
      if( cmd == READ )
          cmd     <= NOP; // no back-to-back read cycles
      else
        begin
          cmd     <= READ;
          rd_next <= SA;
        end

    WRITE: 
      if( cmd == WRITE )
          cmd     <= NOP; // no back-to-back write cycles
      else
        begin
          cmd     <= WRITE;
          wr_next <= SA;
        end

    NOP     : cmd <= NOP;

    default : cmd <= NOP;

  endcase

// -------------------- Memory Read --------------------

// setup read state and negedge DDR cycles
// Need to check for READ on negedge C since
// K & C may occur at same time or
// C after K. But C must occur [0, 45%] * K
// thus, sampling on negedge C will always catch
// the proper cmd (READ, WRITE, NOP)

always @(posedge CQ_N)
  case( rstate )
    RD_NOP: 
      begin
      tristate <= 1;
        if( cmd == READ )
          begin
            rd_addr <= rd_next;
            rstate  <= RD0;
            rd_cycle <= 1;
          end
      end

    RD0:
      begin
        qout   <= mem0[rd_addr];
        rstate <= RD1;
        rd_cycle <= 0;
        tristate <= 0;
      end

    RD2:
      begin
        qout   <= mem2[rd_addr];
        rstate <= RD3;
        if( cmd == READ ) // next cycle is READ ?
          rd_cycle <= 1;
      end

    RD1,
    RD3: ; // nop -- handle on posedge CQ

    default : rstate <= RD_NOP;

  endcase

// posedge DDR cycles

always @(posedge CQ)
  case( rstate )
    RD1:
      begin
        qout   <= mem1[rd_addr];
        rstate <= RD2;
      end

    RD3:
      begin
        qout    <= mem3[rd_addr];
        if( rd_cycle ) // next cycle is READ ?
          begin
            rstate  <= RD0;
            rd_addr <= rd_next;
          end
        else
          begin
            rstate  <= RD_NOP;
            rd_addr <= ~rd_addr;
          end
      end

    RD_NOP,
    RD0,
    RD2: ; // nop -- handle on posedge CQ_N

    default : rstate <= RD_NOP;

  endcase

// -------------------- Memory Write --------------------

// setup write state and negedge DDR cycles

always @(posedge K_N)
  case( wstate )
    WR_NOP: 
      begin
        if( cmd == WRITE )
          begin
            wr_addr <= wr_next;
            wstate  <= WR0;
          end
      end

    WR1:
      begin
        mem1[wr_addr] <= writeMem( mem1[wr_addr], D, BW_N);
        wstate <= WR2;
      end

    WR3:
      begin
        mem3[wr_addr] <= writeMem( mem3[wr_addr], D, BW_N);
        if( cmd == WRITE )
          begin
            wstate  <= WR0;
            wr_addr <= wr_next;
          end
        else
          begin
            wstate  <= WR_NOP;
            wr_addr <= ~wr_addr;
          end
      end

    WR0,
    WR2: ; // nop -- handle on posedge CQ

    default : wstate <= WR_NOP;

  endcase

// posedge DDR cycles

always @(posedge K)
  case( wstate )
    WR0:
      begin
        mem0[wr_addr] <= writeMem( mem0[wr_addr], D, BW_N);
        wstate <= WR1;
      end

    WR2:
      begin
        mem2[wr_addr] <= writeMem( mem2[wr_addr], D, BW_N);
        wstate <= WR3;
      end

    WR_NOP,
    WR1,
    WR3: ; // nop -- handle on posedge CQ_N

    default : wstate <= WR_NOP;

  endcase

function [17:0] writeMem;
  input [17:0] mem, data;
  input  [1:0] enb;
  begin
    case( enb )
      0: writeMem = data;
      1: writeMem = (data & 18'h3fe00)
                  | (mem  & 18'h001ff);
      2: writeMem = (mem  & 18'h3fe00)
                  | (data & 18'h001ff);
      3: writeMem = mem;
    endcase
  end              
endfunction       

endmodule
