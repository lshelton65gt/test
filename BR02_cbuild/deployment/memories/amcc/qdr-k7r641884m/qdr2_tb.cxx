// $Source: /cvs/repository/deployment/memories/amcc/qdr-k7r641884m/qdr2_tb.cxx,v $
// $Id: qdr2_tb.cxx,v 1.1 2006/09/22 20:16:40 ronk Exp $

// -------------------- TestBench --------------------

//#include "systemc.h"
#include <systemc>
#include "libqdr2_tb.systemc.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <iostream>
#include <iomanip>

using namespace std;

SC_MODULE(testbench)
{
  sc_clock         	      K, Kb;
  sc_clock         	      C, Cb;
  sc_signal<sc_uint<2> > 	CQ, CQb, Rb, Wb;
  sc_signal<sc_uint<20> > XAddr, YAddr;
  sc_signal<sc_lv<18> >   XDout, YDout;

  sc_signal<bool> singleClock;
  bool mBurst;

  qdrTB *pTB;

  CarbonMemoryID * xmem[4], * ymem[4];
  CarbonObjectID * pVHM;

  void tRun(void);
  void dumpMem(void);
  void fNop(void);

   testbench(sc_module_name name, bool single=false, bool burst=false);
  ~testbench();

  SC_HAS_PROCESS(testbench);
};

testbench::testbench(sc_module_name name, bool single, bool burst)
: sc_module(name), mBurst(burst)
, K ("K" , 10, 0.5, 0, false)
, Kb("Kb", 10, 0.5, 0, true)
, C ("C" , 10, 0.5, 4.5, false)
, Cb("Cb", 10, 0.5, 4.5, true)
{
  pTB = new qdrTB("QDR_TB");
  assert(pTB);

  singleClock = true; // single clock mode

  if( single ) {
    pTB->C (singleClock);
    pTB->Cb(singleClock);
  } else {
    pTB->C(C);
    pTB->Cb(Cb);
  }

  pTB->K(K);
  pTB->Kb(Kb);
  pTB->Rb(Rb);
  pTB->Wb(Wb);
  pTB->CQ(CQ);
  pTB->CQb(CQb);
  pTB->XAddr(XAddr);
  pTB->YAddr(YAddr);
  pTB->XDout(XDout);
  pTB->YDout(YDout);

  // wave dumping
  // ------------
  pTB->carbonSCWaveInitFSDB("carbon.fsdb", SC_NS);

  pTB->carbonSCDumpVars();

  // dump memory
  // ------------

  pVHM= pTB->carbonModelHandle;
  assert(pVHM);

  char memName[32];

  for(int n=0; n < 4; n++) {
    sprintf(memName, "qdrTB.qdrX.mem%d", n);
    cout << "memName= " << memName << endl;
    xmem[n] = carbonFindMemory(pVHM, memName);
    assert(xmem[n]);

    sprintf(memName, "qdrTB.qdrY.mem%d", n);
    cout << "memName= " << memName << endl;
    ymem[n] = carbonFindMemory(pVHM, memName);
    assert(ymem[n]);
  }

  // Setup Threads
  // -------------

  SC_THREAD(tRun);
  sensitive << K.negedge_event(); // reset/config complete
  dont_initialize();
}

testbench::~testbench()
{
  pTB->carbonSCDumpFlush();

  delete pTB;
}

static UInt32 xaddr[] = { 0x0000, 0xcc00 };
static UInt32 yaddr[] = { 0xff00 };

static int xsize= sizeof(xaddr) / sizeof(int *);
static int ysize= sizeof(yaddr) / sizeof(int *);

void testbench::dumpMem(void)
{
  UInt32 xdata[3], ydata[3];

  for(int a=0x0; a < 0x10; a++) {
    for(int n=0; n < 4; n++) {

      cout << "mem[" << n << "]= " ;

      for(int x=0; x < xsize; x++) {
        if(carbonExamineMemory(xmem[n], xaddr[x]+a, &xdata[x]) != eCarbon_OK) {
          cerr << "Carbon could not read mem[" << n << "]\n";
          exit(1);
        }
        cout << setw(4) << setfill('0') << hex << (xaddr[x]+a) << " = "
             << setw(5) << setfill('0') << hex << xdata[x]   << " : " ;
      }

      for(int y=0; y < ysize; y++) {
        if(carbonExamineMemory(ymem[n], yaddr[y]+a, &ydata[y]) != eCarbon_OK) {
          cerr << "Carbon could not read mem[" << n << "]\n";
          exit(1);
        }
        cout << setw(4) << setfill('0') << hex << (yaddr[y]+a) << " = "
             << setw(5) << setfill('0') << hex << ydata[y]   << " : " ;
      }

      cout << endl;
    }
  }
}

void testbench::fNop(void)
{
  Rb = 3;    // nop
  Wb = 3;    // nop
  XAddr= 0x1234;
  YAddr= 0x5678;

  // period() appears to moves to next posedge!?!?
  wait( K.period() * 5 );

  wait(); // now sync on negedge
}

void testbench::tRun(void)
{
  fNop();

  // Read Xmem, Write Ymem

  for(int n=0; n < 16; n++) {
    Wb = 3;
    Rb = 2; // read xmem
    XAddr= 0x0000 + n;
    wait();

    Rb = 3;
    Wb = 1; // write ymem
    YAddr= 0xff00 + n;
    wait();

    if(mBurst) {
      Wb = 3;
      Rb = 1; // read ymem
      YAddr= 0xff00 + n;
      wait();

      Rb = 3;
      Wb = 2; // write xmem
      XAddr= 0xcc00 + n;
      wait();
    }

  }

  fNop();

  if(! mBurst) {
    // Read Ymem, Write Xmem

    for(int n=0; n < 16; n++) {
      Wb = 3;
      Rb = 1; // read ymem
      YAddr= 0xff00 + n;
      wait();

      Rb = 3;
      Wb = 2; // write xmem
      XAddr= 0xcc00 + n;
      wait();
    }

    fNop();
  }

  dumpMem();

  sc_stop();
}

#include <unistd.h>

int sc_main (int argc , char *argv[]) 
{
  bool single(false), burst(false);
  int c;

  while((c= getopt(argc, argv, "sb")) != EOF)
    switch(c) {
      case 's': single=true; break;
      case 'b': burst =true; break;
      case '?':  cout << "unknown switch:\n"; exit(1);
    }

  cout << "single= " << single << endl;
  cout << "burst = " << burst  << endl;

  testbench tb("QDR_TestBench",single,burst);

  sc_start();

  return 0;
}

