/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// $Source: /cvs/repository/deployment/memories/amcc/qdr-k7r321882m/k7r321882m.v,v $
// $Id: k7r321882m.v,v 1.1 2006/09/22 20:16:40 ronk Exp $

// 2Mx18-bit QDR II burst=2 SRAM

module K7R321882M (D, Q, SA, R_N, W_N, BW_N, K, K_N, C, C_N, CQ, CQ_N, DOFF_N);

// ---------- input/output ----------

 input [17 : 0] D;
output [17 : 0] Q;
 input [19 : 0] SA;
 input          R_N;
 input          W_N;
 input [ 1 : 0] BW_N;
 input          K, K_N;
 input          C, C_N;
output          CQ, CQ_N;
 input          DOFF_N;

wire   [17 : 0] D;
wire   [19 : 0] SA;
wire   [ 1 : 0] BW_N;

wire R_N, W_N, DOFF_N; 
wire K  , K_N, C, C_N; // clocks

wire CQ   = (C & C_N) ? K   : C;
wire CQ_N = (C & C_N) ? K_N : C_N;

// ---------- internal ----------

reg [17:0]
  qout,
  wr_data,
  mem [0 : (1 << 21) - 1 ]; // 2M x 18 RAM

reg [19 : 0] rd_addr [0:2]; // read address pipeline
reg [ 2 : 0] rd_cmdb;       // read command pipeline

reg          wr_cmdb;       // write command 
reg [ 1 : 0] wr_enb;        // save byte write enables
reg [19 : 0] wr_addr;

reg tristate, rd_cycle;

wire [17 : 0] Q = tristate ? 18'bz : qout; // data output

// initialize

initial
  begin
    rd_addr[0] <= 18'h01234;
    rd_addr[1] <= 18'h05678;
    rd_addr[2] <= 18'h09abc;
    rd_cmdb    <=  3'b111;
    wr_enb     <=  2'b11;
    wr_cmdb    <=  1'b1;
    tristate   <=  1'b1;
  end

// -------------------- Memory Read --------------------

// decode read command on posedge K

always @(posedge K)
  begin
    rd_cmdb[2] <= R_N;
    rd_addr[2] <= SA;
  end

// negedge DDR cycles

// Need to check for READ on negedge C since
// K & C may occur at same time or
// C after K. But C must occur [0, 45%] * K
// thus, sampling on negedge C will always decode
// the proper cmd (READ, NOP)

always @(posedge CQ_N)
  begin
    rd_cmdb    <= { 1'b1, rd_cmdb[2:1] }; // shift pipeline
    rd_addr[0] <= rd_addr[1];
    rd_addr[1] <= rd_addr[2];

    if( rd_cmdb[1] == 1'b0 ) // start read burst
      begin
        qout     <= mem[ { rd_addr[1], 1'b0 } ];
        tristate <= 1'b0;
      end
    else
      tristate <= 1'b1;
  end

// posedge DDR cycles

always @(posedge CQ)
  if( rd_cmdb[0] == 1'b0 ) // finish read burst
    qout <= mem[ { rd_addr[0], 1'b1 } ];


// -------------------- Memory Write --------------------

// decode write command on posedge K

always @(posedge K)
  begin
    wr_data <= D;    // grab "early write" data
    wr_cmdb <= W_N;
    wr_enb  <= BW_N;
  end

// write data into memory on negedge DDR cycles

always @(posedge K_N)
  if( wr_cmdb == 1'b0 )
    begin
      mem[ { SA, 1'b0 } ] <= writeMem( mem[ { SA, 1'b0 } ], wr_data, wr_enb);
      mem[ { SA, 1'b1 } ] <= writeMem( mem[ { SA, 1'b1 } ],       D, BW_N);
    end

// Decode Byte Write Enables

function [17:0] writeMem;
  input  [17:0] mem, data;
  input   [1:0] enb;
  begin
    case( enb )
      0: writeMem = data;
      1: writeMem = (data & 18'h3fe00)
                  | (mem  & 18'h001ff);
      2: writeMem = (mem  & 18'h3fe00)
                  | (data & 18'h001ff);
      3: writeMem = mem;
    endcase
  end              
endfunction       

endmodule
