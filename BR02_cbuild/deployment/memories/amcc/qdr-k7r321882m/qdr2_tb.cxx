// $Source: /cvs/repository/deployment/memories/amcc/qdr-k7r321882m/qdr2_tb.cxx,v $
// $Id: qdr2_tb.cxx,v 1.1 2006/09/22 20:16:40 ronk Exp $

// -------------------- TestBench --------------------

//#include "systemc.h"
#include <systemc>
#include "libqdr2_tb.systemc.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <iostream>
#include <iomanip>

using namespace std;

SC_MODULE(testbench)
{
  sc_clock         	      K, Kb;
  sc_clock         	      C, Cb;
  sc_signal<sc_uint<2> > 	CQ, CQb, Rb, Wb;
  sc_signal<sc_uint<20> > XAddr, YAddr;
  sc_signal<sc_lv<18> >   XDout, YDout;

  sc_signal<bool> singleClock;
  bool mBurst;

  qdrTB *pTB;

  CarbonMemoryID * xmem, * ymem;
  CarbonObjectID * pVHM;

  void tRun(void);
  void dumpMem(void);
  void fNop(void);

   testbench(sc_module_name name, bool single=false, bool burst=false);
  ~testbench();

  SC_HAS_PROCESS(testbench);
};

testbench::testbench(sc_module_name name, bool single, bool burst)
: sc_module(name), mBurst(burst)
, K ("K" , 10, 0.5, 0, false)
, Kb("Kb", 10, 0.5, 0, true)
, C ("C" , 10, 0.5, 4.5, false)
, Cb("Cb", 10, 0.5, 4.5, true)
{
  pTB = new qdrTB("QDR_TB");
  assert(pTB);

  singleClock = true; // single clock mode

  if( single ) {
    pTB->C (singleClock);
    pTB->Cb(singleClock);
  } else {
    pTB->C(C);
    pTB->Cb(Cb);
  }

  pTB->K(K);
  pTB->Kb(Kb);
  pTB->Rb(Rb);
  pTB->Wb(Wb);
  pTB->CQ(CQ);
  pTB->CQb(CQb);
  pTB->XAddr(XAddr);
  pTB->YAddr(YAddr);
  pTB->XDout(XDout);
  pTB->YDout(YDout);

  // wave dumping
  // ------------
  pTB->carbonSCWaveInitFSDB("carbon.fsdb", SC_NS);

  pTB->carbonSCDumpVars();

  // dump memory
  // ------------

  pVHM= pTB->carbonModelHandle;
  assert(pVHM);

  xmem = carbonFindMemory(pVHM, "qdrTB.qdrX.mem");
  ymem = carbonFindMemory(pVHM, "qdrTB.qdrY.mem");
  assert(xmem);
  assert(ymem);

  // Setup Threads
  // -------------

  SC_THREAD(tRun);
//sensitive << Kb.posedge_event(); 
  sensitive <<  K.posedge_event()  
            << Kb.posedge_event(); 
  dont_initialize();
}

testbench::~testbench()
{
  pTB->carbonSCDumpFlush();

  delete pTB;
}

// internal addresses are multiplied by 2
// addr[19:0] = 2^20 = 1M --> internal memory = 2m

static UInt32 xaddr[] = { 0x0000*2, 0xcc00*2 };
static UInt32 yaddr[] = { 0xff00*2 };

static int xsize= sizeof(xaddr) / sizeof(int *);
static int ysize= sizeof(yaddr) / sizeof(int *);

void testbench::dumpMem(void)
{
  UInt32 xdata[3], ydata[3];

  for(int a=0x0; a < 0x20; a++) {

    for(int x=0; x < xsize; x++) {
      if(carbonExamineMemory(xmem, xaddr[x]+a, &xdata[x]) != eCarbon_OK) {
        cerr << "Carbon could not read xmem\n";
        exit(1);
      }
      cout << setw(4) << setfill('0') << hex << (xaddr[x]/2+a) << " = "
           << setw(5) << setfill('0') << hex << xdata[x]   << " : " ;
    }

    for(int y=0; y < ysize; y++) {
      if(carbonExamineMemory(ymem, yaddr[y]+a, &ydata[y]) != eCarbon_OK) {
        cerr << "Carbon could not read ymem\n";
        exit(1);
      }
      cout << setw(4) << setfill('0') << hex << (yaddr[y]/2+a) << " = "
           << setw(5) << setfill('0') << hex << ydata[y]   << " : " ;
    }

    cout << endl;
  }
}

void testbench::fNop(void)
{
  Rb = 3;    // nop
  Wb = 3;    // nop
  XAddr= 0x1234;
  YAddr= 0x5678;

  wait( Kb.period() * 5 );
}

void testbench::tRun(void)
{
  fNop();

  // Do not understand it; but, will spin here until posedge Kb
  while(! Kb.posedge() ) {
    wait();
  }

  // Read Xmem, Write Ymem

  for(int n=0; n < 16 + 6; ) { // +6 : adjust for read/write latencies

    if( Kb.posedge() ) {
      Rb = 2; // read xmem
      Wb = 1; // write ymem
      if(mBurst) {
        if( n > 2 ) {
          Rb = 0; // read  xmem/ymem
          Wb = 0; // write xmem/ymem
        }
      }
      XAddr= 0x0000 + n; // read addr
      wait();
    } else {
      Rb = 3; // nop xmem
      Wb = 3; // nop ymem
      YAddr= 0xff00 - 2 + n++; // write addr -- adjust for "early write"
      if(mBurst) 
         XAddr= 0xcc00 - 6 + n;
      wait();
    }

/**
    if(mBurst) {
//    Wb = 3;
      Rb = 1; // read ymem
      YAddr= 0xff00 + n;
//    wait();

//    Rb = 3;
      Wb = 2; // write xmem
      XAddr= 0xcc00 + n;
      wait();
    }
**/

  }

  fNop();

  if(! mBurst) {
    // Read Ymem, Write Xmem

    for(int n=0; n < 16; n++) {
//    Wb = 3;
      Rb = 1; // read ymem
      YAddr= 0xff00 + n;
//    wait();

//    Rb = 3;
      Wb = 2; // write xmem
      XAddr= 0xcc00 + n;
      wait();
    }

    fNop();
  }

  dumpMem();

  sc_stop();
}

#include <unistd.h>

int sc_main (int argc , char *argv[]) 
{
  bool single(false), burst(false);
  int c;

  while((c= getopt(argc, argv, "sb")) != EOF)
    switch(c) {
      case 's': single=true; break;
      case 'b': burst =true; break;
      case '?':  cout << "unknown switch:\n"; exit(1);
    }

  cout << "single= " << single << endl;
  cout << "burst = " << burst  << endl;

  testbench tb("QDR_TestBench",single,burst);

  sc_start();

  return 0;
}

