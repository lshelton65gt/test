// $Source: /cvs/repository/deployment/memories/amcc/rldram-mt49h8m36/rldram_tb.v,v $
// $Id: rldram_tb.v,v 1.1 2006/09/22 20:16:40 ronk Exp $

`timescale  1 ns / 1 ns

module rldramTB(
    Ck, Ckb
  , Dk, Dkb
  , XAddr, YAddr
  , XBank, YBank
  , XCmd,  YCmd
  , Data
  , XQk, XQkb, YQk, YQkb
  , XDm, XVld, YDm, YVld
);

// ---------- inputs ----------

 input        Ck, Ckb, XDm, YDm;
 input [18:0] XAddr, YAddr;
 input  [2:0] XBank, YBank;
 input  [2:0] XCmd,  YCmd;
 input  [1:0] Dk, Dkb;

// ---------- inouts ----------

 inout [35:0] Data;

// ---------- outputs ----------

output        XVld, YVld;
output  [1:0] XQk,  XQkb, YQk, YQkb;

// ---------- wires ----------

wire        Ck, Ckb;
wire [18:0] XAddr, YAddr;
wire  [2:0] XBank, YBank, XCmd, YCmd;
tri1 [35:0] Data = 36'bz;
tri1        XVld, YVld;
wire  [1:0] Dk, Dkb, XQk, XQkb, YQk, YQkb;

// ---------- Xram/Yram ----------

//mt49h8m36 Xram (
mt49h16m18 Xram (
    .Dq(Data)
  , .Qvld(XVld)
  , .Qk(XQk)
  , .Qk_n(XQkb)
  , .Ck(Ck)
  , .Ck_n(Ckb)
  , .Dk(Dk)
  , .Dk_n(Dkb)
  , .Cs_n(XCmd[2])
  , .We_n(XCmd[1])
  , .Ref_n(XCmd[0])
  , .Addr(XAddr)
  , .Bank(XBank)
  , .Dm(XDm)
);

//mt49h8m36 Yram (
mt49h16m18 Yram (
    .Dq(Data)
  , .Qvld(YVld)
  , .Qk(YQk)
  , .Qk_n(YQkb)
  , .Ck(Ck)
  , .Ck_n(Ckb)
  , .Dk(Dk)
  , .Dk_n(Dkb)
  , .Cs_n(YCmd[2])
  , .We_n(YCmd[1])
  , .Ref_n(YCmd[0])
  , .Addr(YAddr)
  , .Bank(YBank)
  , .Dm(YDm)
);

// Initialize

initial
  begin
    $readmemh("xmem0.vecs", Xram.Bank0); // 1M x 36
    $readmemh("xmem1.vecs", Xram.Bank1); // 1M x 36
    $readmemh("xmem2.vecs", Xram.Bank2); // 1M x 36
    $readmemh("xmem3.vecs", Xram.Bank3); // 1M x 36
    $readmemh("xmem4.vecs", Xram.Bank4); // 1M x 36
    $readmemh("xmem5.vecs", Xram.Bank5); // 1M x 36
    $readmemh("xmem6.vecs", Xram.Bank6); // 1M x 36
    $readmemh("xmem7.vecs", Xram.Bank7); // 1M x 36
  end

initial
  begin
    $readmemh("xmem0.vecs", Yram.Bank0); // 1M x 36
    $readmemh("xmem1.vecs", Yram.Bank1); // 1M x 36
    $readmemh("xmem2.vecs", Yram.Bank2); // 1M x 36
    $readmemh("xmem3.vecs", Yram.Bank3); // 1M x 36
    $readmemh("xmem4.vecs", Yram.Bank4); // 1M x 36
    $readmemh("xmem5.vecs", Yram.Bank5); // 1M x 36
    $readmemh("xmem6.vecs", Yram.Bank6); // 1M x 36
    $readmemh("xmem7.vecs", Yram.Bank7); // 1M x 36
  end

endmodule

