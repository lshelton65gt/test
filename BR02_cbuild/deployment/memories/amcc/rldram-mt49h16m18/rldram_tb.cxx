// $Source: /cvs/repository/deployment/memories/amcc/rldram-mt49h16m18/rldram_tb.cxx,v $
// $Id: rldram_tb.cxx,v 1.1 2006/09/22 20:16:40 ronk Exp $

//#include <systemc>
#include "librldram_tb.systemc.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <iostream>
#include <iomanip>

using namespace std;

// -------------------- TestBench --------------------

SC_MODULE(testbench)
{
  sc_clock                Ck,  Ckb, Dk,   Dkb;
  sc_signal<bool>         XDm, YDm, XVld, YVld;
  sc_signal<sc_uint<2>  > XQk, XQkb, YQk, YQkb;
  sc_signal<sc_uint<20> > XAddr, YAddr;
  sc_signal<sc_uint<3>  > XBank, YBank, XCmd, YCmd;
  sc_signal<sc_lv<18>   > Data;

  enum enumCommand { eMRS=0, eWRITE=1, eAREF=2, eREAD=3, eNOP=7 };

  rldramTB *pTB;

  CarbonMemoryID * xmem[8], * ymem[8];
  CarbonObjectID * pVHM;

  bool MUX;
  uint BL, RL, WL, MRS;

  void tRun(void);
  void tInit(void);
  void dumpMem(void);
  void fNOP(bool x=true, bool y=true);
  void fMRS(uint opcode);
  void fMRS(bool mux, uint burst_len, uint config);

   testbench(sc_module_name name);
  ~testbench();

  SC_HAS_PROCESS(testbench);
};

testbench::testbench(sc_module_name name)
: sc_module(name)
, Ck ("Ck" , sc_time(2.5,SC_NS), 0.5, SC_ZERO_TIME, false)
, Ckb("Ckb", sc_time(2.5,SC_NS), 0.5, SC_ZERO_TIME, true)
, Dk ("Dk" , sc_time(2.5,SC_NS), 0.5, SC_ZERO_TIME, false)
, Dkb("Dkb", sc_time(2.5,SC_NS), 0.5, SC_ZERO_TIME, true)
{
  pTB = new rldramTB("RLDRAM_TB");
  assert(pTB);

  pTB->Ck(Ck);
  pTB->Ckb(Ckb);
  pTB->Dk(Dk);
  pTB->Dkb(Dkb);
  pTB->XQk(XQk);
  pTB->XQkb(XQkb);
  pTB->YQk(YQk);
  pTB->YQkb(YQkb);
  pTB->XAddr(XAddr);
  pTB->YAddr(YAddr);
  pTB->XBank(XBank);
  pTB->YBank(YBank);
  pTB->XCmd(XCmd);
  pTB->YCmd(YCmd);
  pTB->XVld(XVld);
  pTB->YVld(YVld);
  pTB->XDm(XDm);
  pTB->YDm(YDm);
  pTB->Data(Data);

  // wave dumping
  // ------------
  pTB->carbonSCWaveInitFSDB("carbon.fsdb", SC_PS);

  pTB->carbonSCDumpVars();

  // dump memory
  // ------------

  pVHM= pTB->carbonModelHandle;
  assert(pVHM);

  char memName[32];

  for(int n=0; n < 8; n++) {
    sprintf(memName, "rldramTB.Xram.Bank%d", n);
    cout << "memName= " << memName << endl;
    xmem[n] = carbonFindMemory(pVHM, memName);
    assert(xmem[n]);

    sprintf(memName, "rldramTB.Yram.Bank%d", n);
    cout << "memName= " << memName << endl;
    ymem[n] = carbonFindMemory(pVHM, memName);
    assert(ymem[n]);
  }

  // Setup Threads
  // -------------

  SC_THREAD(tRun);
  sensitive << Ckb.posedge_event();
  dont_initialize();

  SC_THREAD(tInit);
}

testbench::~testbench()
{
  pTB->carbonSCDumpFlush();

  delete pTB;
}

static UInt32 xaddr[] = { 0x0000, 0xcc00 };
static UInt32 yaddr[] = { 0x0000, 0xff00 };

static int xsize= sizeof(xaddr) / sizeof(int *);
static int ysize= sizeof(yaddr) / sizeof(int *);

void testbench::dumpMem(void)
{
  UInt32 xdata[3][2], ydata[3][2];

  for(int a=0x0; a < 0x10; a++) {
    for(int n=0; n < 8; n++) {

      cout << "mem[" << n << "]" ;

      for(int x=0; x < xsize; x++) {
        if(carbonExamineMemory(xmem[n], xaddr[x]+a, xdata[x]) != eCarbon_OK) {
          cerr << "Carbon could not read mem[" << n << "]\n";
          exit(1);
        }
        cout << " : " 
             << setw(4) << setfill('0') << hex << (xaddr[x]+a) << "="
             << setw(5) << setfill('0') << hex <<  xdata[x][0] ;
      }

      for(int y=0; y < ysize; y++) {
        if(carbonExamineMemory(ymem[n], yaddr[y]+a, ydata[y]) != eCarbon_OK) {
          cerr << "Carbon could not read mem[" << n << "]\n";
          exit(1);
        }
        cout << " : " 
             << setw(4) << setfill('0') << hex << (yaddr[y]+a) << "="
             << setw(5) << setfill('0') << hex <<  ydata[y][0] ;
      }

      cout << endl;
    }
  }
}

/**

Table 6: Address Widths at Different Burst Lengths

Burst Length |   Configuration     |
             |   x36 |  x18 |  x9  |
-------------|-------|------|------|
   BL = 2    |  18:0 | 19:0 | 20:0 |
   BL = 4    |  17:0 | 18:0 | 19:0 |
   BL = 8    |   NA  | 17:0 | 18:0 |

Table 7: Command Table

   Operation                 |   Code    | CS# | WE# | REF# | A[20:0] | B[2:0] 
-----------------------------|-----------|-----|-----|------|---------|--------
Device DESELECT/No Operation | DESEL/NOP | H   | X   | X    |    X    |    X
MRS: Mode Register Set       |   MRS     | L   | L   | L    | OPCODE  |    X 
READ                         |   READ    | L   | H   | H    |    A    |   BA 
WRITE                        |   WRITE   | L   | L   | H    |    A    |   BA 
AUTO REFRESH                 |   AREF    | L   | H   | L    |    X    |   BA

Table 8: Mode Register Bitmap

A5 : Addr Mux : 0= non-mux; 1=mux

A[4:3] : Burst Length
-------|--------------
  00   : BL = 2
  01   : BL = 4
  10   : BL = 8
  11   : Invalid

A[2:0] : Configuration
-------|--------------
  000  : Config #1
  001  : Config #1
  010  : Config #2
  011  : Config #3
  1XX  : Reserved

Table 11: Configuration Table

Config | 1 | 2 | 3 | Description
-------|---|---|---|---------------
  RC   | 4 | 6 | 8 | row cycle time
  RL   | 4 | 6 | 8 | read  latency
  WL   | 5 | 7 | 9 | write latency

 **/

void testbench::fMRS(bool mux, uint burst_len, uint config)
{
  uint opcode=0;

  opcode   = burst_len & 3;  // [4:3]= BL [2:0]=0:Config=1
  opcode <<= 3; 

  opcode  |= config    & 7;  // [2:0]= Config

  opcode  |= mux ? 0x20 : 0; //   [5]= mux

  fMRS(opcode);
}

void testbench::fMRS(uint opcode)
{
  MUX= opcode & 0x20;     // [5]

  switch(opcode & 0x18) { // [4:3]
    case 0x00: BL= 2; break;
    case 0x08: BL= 4; break;
    case 0x10: BL= 8; break;
    default:   BL= 2;
  }

  switch(opcode & 0x07) { // [2:0]
    case 0:  RL= 4; break;
    case 1:  RL= 4; break;
    case 2:  RL= 6; break;
    case 3:  RL= 8; break;
    default: RL= 4;
  }

  WL= RL + 1;

  XCmd = eMRS;
  YCmd = eMRS;
  XAddr= opcode;
  YAddr= opcode;

  cout << "BL=" << BL << " RL=" << RL << " WL=" << WL << endl;

  wait( Ckb.period() * 3 );

  fNOP();

  wait( Ckb.period() * 6 );
}

void testbench::fNOP(bool x, bool y)
{
  if(x) {
    XCmd = eNOP;
    XDm  = true;
    XAddr= 0x12345;
  }

  if(y) {
    YCmd = eNOP;
    YDm  = true;
    YAddr= 0x54321;
  }
}

void testbench::tInit(void) { fNOP(); }

void testbench::tRun(void)
{
//fMRS(false, 0, 0); // [5]=0:mux=0, [4:3]=0:BL=2, [2:0]=0:Config=1
//fMRS(false, 0, 2); // [5]=0:mux=0, [4:3]=0:BL=2, [2:0]=2:Config=2
//fMRS(false, 0, 3); // [5]=0:mux=0, [4:3]=0:BL=2, [2:0]=3:Config=3
//fMRS(false, 2, 3); // [5]=0:mux=0, [4:3]=2:BL=8, [2:0]=3:Config=3
  fMRS(false, 1, 3); // [5]=0:mux=0, [4:3]=1:BL=4, [2:0]=3:Config=3

  fNOP();

  // Read Xmem, Write Ymem

  uint xaddr=0x0000/BL, yaddr=0xff00/BL;

  YDm= false; 

  for(int n=0; n < 16/BL; n++, xaddr++, yaddr++)  
  for(int b=0; b <  8   ; b++) {
    XAddr= xaddr;
    YAddr= yaddr;
    XBank= b;
    YBank= b;

    switch(BL) {
      case 8: 
        YCmd = eWRITE; XCmd = eNOP; wait(); 
        XCmd = eREAD;  YCmd = eNOP; wait();
        XCmd = eNOP;   YCmd = eNOP; wait(); wait(); 
        break;

      case 4:
        YCmd = eWRITE; XCmd = eNOP; wait(); 
        XCmd = eREAD;  YCmd = eNOP; wait();
        break;

      case 2: 
      default: 
        YCmd = eWRITE; wait(); // WL - RL = 1 cycle difference
        XCmd = eREAD;
    }
  }

  fNOP();

  // Read Ymem, Write Xmem

  xaddr=0xcc00/BL, yaddr=0xff00/BL;

  XDm= false; 

  for(int n=0; n < 16/BL; n++, xaddr++, yaddr++)  
  for(int b=0; b <  8   ; b++) {
    XAddr= xaddr;
    YAddr= yaddr;
    XBank= b;
    YBank= b;

    switch(BL) {
      case 8: 
        XCmd = eWRITE; YCmd = eNOP; wait(); 
        YCmd = eREAD;  XCmd = eNOP; wait();
        XCmd = eNOP;   YCmd = eNOP; wait(); wait(); 
        break;

      case 4:
        XCmd = eWRITE; YCmd = eNOP; wait(); 
        YCmd = eREAD;  XCmd = eNOP; wait();
        break;

      case 2: 
      default: 
        XCmd = eWRITE; wait(); // WL - RL = 1 cycle difference
        YCmd = eREAD;
    }
  }


  fNOP();

  wait( Ckb.period() * 10); 

  dumpMem();

  sc_stop();
}

#include <unistd.h>

int sc_main (int argc , char *argv[]) 
{
  testbench tb("RLDRAM_TestBench");

  sc_start();

  return 0;
}

