module WRAPB2C_SRAM1DR04096X064D16S2M1
             ( ARYSEL,   CLOCK,    RDWRT,    SCAN_IN,  SCAN_OUT, A,        DIN,      DOUT );

parameter dataWidth = 64;
parameter addrWidth = 12;
parameter depth = 4096 ;

output          SCAN_OUT;
output  [dataWidth - 1:0] DOUT;

input          ARYSEL,  CLOCK,   RDWRT,   SCAN_IN;
input   [addrWidth - 1:0] A;
input   [dataWidth - 1:0] DIN;

reg   [dataWidth - 1:0] DOUT;
reg [dataWidth - 1:0] mem [0: depth - 1];

always @ (negedge CLOCK)
  if (~RDWRT)
    mem[A] <= DIN;

always @ (posedge CLOCK)
  if (RDWRT & ARYSEL)
    DOUT <= mem[A];

endmodule
