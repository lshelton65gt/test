//
// Mathces MarkK's WRAP2C_SRAM2B0512X052D04S1M1
//
module ibm_mem_C_SRAM_2_io ( ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  RDWRT_P1,
			  RDWRT_P2,  DIN_P1,    DIN_P2,    DOUT_P1,   DOUT_P2,   
			  A_P1,      A_P2 );
   parameter addrWidth = 9;
   parameter width = 52;
   parameter depth = 512;

   output   [width - 1:0]DOUT_P1, DOUT_P2;

   input          ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  RDWRT_P1,  RDWRT_P2;
   input [width - 1:0]   DIN_P1,    DIN_P2;
   input [addrWidth - 1 :0] 	  A_P1,      A_P2;


   carbon_mem_2rw_reg #(addrWidth, width, depth) m(.clka(CLOCK_P1),
						     .clkb(CLOCK_P2),
						     .adra(A_P1),
						     .adrb(A_P2),
						     .dia(DIN_P1),
						     .dib(DIN_P2),
						     .wea(~RDWRT_P1),
						     .web(~RDWRT_P2),
						     .doa(DOUT_P1),
						     .dob(DOUT_P2));

endmodule // ibm_mem_C_SRAM_2



module ibm_mem_C_SRAM_2 ( ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  RDWRT_P1,
			  RDWRT_P2,  DIN_P1,    DIN_P2,    DOUT_P1,   DOUT_P2,   
			  A_P1,      A_P2 );
   parameter addrWidth = 9;
   parameter width = 52;
   parameter depth = 512;

   output   [width - 1:0]DOUT_P1, DOUT_P2;

   input          ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  RDWRT_P1,  RDWRT_P2;
   input [width - 1:0]   DIN_P1,    DIN_P2;
   input [addrWidth - 1 :0] 	  A_P1,      A_P2;

   ibm_mem_C_SRAM_2_io #(addrWidth, width, depth) io
     ( ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  RDWRT_P1,
       RDWRT_P2,  DIN_P1,    DIN_P2,    DOUT_P1,   DOUT_P2,   
       A_P1,      A_P2 );


endmodule // ibm_mem_C_SRAM_2


