module WRAPB8C_RA032X75D2P2W1R1M1 (CLOCKP1, WEP1, A_RP1, A_WP1, DI_WP1, DO_RP1 );

parameter dataWidth = 75;
parameter addrWidth = 5;
parameter depth = 32;

input CLOCKP1, WEP1;
input [addrWidth - 1:0] A_RP1, A_WP1;
input [dataWidth - 1:0] DI_WP1;

output [dataWidth - 1:0] DO_RP1;

wire [dataWidth - 1:0] DO_RP1;
reg [dataWidth - 1:0] mem [0: depth - 1];


always @ (negedge CLOCKP1)
  if (WEP1)
      mem[A_WP1] <= DI_WP1;

`ifdef AXIS_CWTB
  wire write_through_p1 = WEP1 & CLOCKP1 & (A_WP1 == A_RP1);
  assign DO_RP1 = write_through_p1 ? DI_WP1 : mem[A_RP1];
`else
assign  DO_RP1 = mem[A_RP1];
`endif

endmodule

