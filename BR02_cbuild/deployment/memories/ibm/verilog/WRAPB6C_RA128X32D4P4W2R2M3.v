module WRAPB6C_RA128X32D4P4W2R2M3 (CLOCKP1, CLOCKP2, WEP1, WEP2, A_RP1, A_RP2, A_WP1, A_WP2, DI_WP1, DI_WP2, DO_RP1, DO_RP2);

parameter dataWidth = 32;
parameter addrWidth = 7;
parameter depth = 128;

input CLOCKP1, CLOCKP2, WEP1, WEP2;
input [addrWidth - 1:0] A_RP1, A_RP2, A_WP1, A_WP2;
input [dataWidth - 1:0] DI_WP1, DI_WP2;

output [dataWidth - 1:0] DO_RP1, DO_RP2;

wire [dataWidth - 1:0] DO_RP1, DO_RP2;
reg [dataWidth - 1:0] mem [0: depth - 1];


always @ (posedge CLOCKP1)
  if (WEP1)
      mem[A_WP1] <= DI_WP1;

assign  DO_RP1 = mem[A_RP1];


always @ (posedge CLOCKP2)
  if (WEP2)
      mem[A_WP2] <= DI_WP2;

assign  DO_RP2 = mem[A_RP2];

endmodule

