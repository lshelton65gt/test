//
// Mathces MarkK's WRAP2C_SRAM2B0512X052D04S1M1
//
module ibm_mem_C_SRAM_1 ( ARYSEL, CLOCK, RDWRT, DIN, DOUT, A ); 
   parameter addrWidth = 9;
   parameter width = 52;
   parameter depth = 512;

   output [width - 1:0] DOUT;

   input 		ARYSEL, CLOCK,  RDWRT;
   input [width - 1:0] 	DIN;
   input [addrWidth - 1 :0] A;


   ibm_mem_C_SRAM_1_io #(addrWidth, width, depth) io 
     ( ARYSEL, CLOCK, RDWRT, DIN, DOUT, A ); 

endmodule // ibm_mem_C_SRAM_1

module ibm_mem_C_SRAM_1_io ( ARYSEL, CLOCK, RDWRT, DIN, DOUT, A );
   parameter addrWidth = 9;
   parameter width = 52;
   parameter depth = 512;

   output [width - 1:0] DOUT;

   input 		ARYSEL, CLOCK,  RDWRT;
   input [width - 1:0] 	DIN;
   input [addrWidth - 1 :0] A;
   
   carbon_mem_1rw_reg #(addrWidth, width, depth) m (.clka(CLOCK),
						     .adra(A),
						     .dia(DIN),
						     .wea(~RDWRT),
						     .doa(DOUT));
   

endmodule // ibm_mem_C_SRAM_1



