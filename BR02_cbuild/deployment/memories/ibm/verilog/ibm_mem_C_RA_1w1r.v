//
// need to add hierarchy to have it mathc the waqy IBM does it
//
module ibm_mem_C_RA_1w1r ( CLOCKP1, WEP1, DI_WP1,DO_RP1, A_RP1, A_WP1 );
   parameter addrWidth = 8;
   parameter width = 32;
   parameter depth = 50;
   
   output [width-1:0] DO_RP1;

   input 	 CLOCKP1, WEP1;    
   input [width-1:0]  DI_WP1;  
   input [addrWidth-1:0] 	 A_RP1, A_WP1;

   ibm_mem_C_RA_1w1r_io  #(addrWidth, width, depth) io(.CLOCKP1 (CLOCKP1),
						       .A_WP1   (A_WP1), 
						       .DI_WP1  (DI_WP1),
						       .WEP1    (WEP1),	 
						       .A_RP1   (A_RP1), 
						       .DO_RP1  (DO_RP1));
   

endmodule

module ibm_mem_C_RA_1w1r_io ( CLOCKP1, WEP1, DI_WP1,DO_RP1, A_RP1, A_WP1 );
   parameter addrWidth = 2;
   parameter width = 4;
   parameter depth = 6;
   
   output [width-1:0] DO_RP1;

   input 	 CLOCKP1, WEP1;    
   input [width-1:0]  DI_WP1;  
   input [addrWidth-1:0] 	 A_RP1, A_WP1;

   carbon_mem_1r1w #(addrWidth, width, depth) m(.clk(CLOCKP1),
						  .wadr(A_WP1),
						  .di(DI_WP1),
						  .we(WEP1),
						  .radr(A_RP1),
						  .do(DO_RP1));
   

endmodule

