//
// Scripts generates wrapper that instantiates this module
// that instantation will be called "sram" in order to match
// the IBM hier.  This must then instantiate another laer called io
//
// 'B' rams have BYTE enables
//
module ibm_mem_B_SRAM_1 ( ARYSEL, CLOCK,  RDWRT,  DIN,    DOUT,   
			  A,      BW );
   parameter addrWidth = 9;
   parameter width = 52;
   parameter depth = 512;
   parameter bytes = (width / 8) + (width % 8) ? 1 : 0;

   output [width - 1:0] DOUT;

   input 		ARYSEL, CLOCK,  RDWRT;
   input [width - 1:0] 	DIN;
   input [addrWidth - 1 :0] A;
   input [bytes - 1 : 0]    BW;
   
   ibm_mem_B_SRAM_1_io #(addrWidth, width, depth) io (.ARYSEL(ARYSEL),
						      .CLOCK(CLOCK),
						      .RDWRT(RDWRT),
						      .DIN(DIN),
						      .DOUT(DOUT),
						      .A(A),
						      .BW(BW));
   
  
endmodule // ibm_mem_B_SRAM_2



//
// This layer instantiates the carbon memory.  That memory MUST be
// called m in order to match the way the IBM mems do it
//
module ibm_mem_B_SRAM_1_io ( ARYSEL, CLOCK,  RDWRT,  DIN,    DOUT,   
			  A,      BW );
   parameter addrWidth = 9;
   parameter width = 52;
   parameter depth = 512;

   parameter bytes = (width / 8) + (width % 8) ? 1 : 0;

   integer   i;
   
   output [width - 1:0] DOUT;

   input 		ARYSEL, CLOCK,  RDWRT;
   input [width - 1:0] 	DIN;
   input [addrWidth - 1 :0] A;
   input [bytes - 1 : 0]    BW;
   
   reg [width - 1 : 0] 	    bitmask1;
   
   always @(BW) begin
      bitmask1 = {width{1'b0}};      
      for (i = 0; i < bytes; i = i+1) begin
	 bitmask1 = bitmask1 << i;
	 bitmask1 = bitmask1 | {8{BW[bytes - i - 1]}};	 
      end
   end
   
   carbon_mem_1rw_reg_bitmask #(addrWidth, width, depth) m (.clka(CLOCK),
							    .adra(A),
							    .dia(DIN),
							    .wea(~RDWRT),
							    .doa(DOUT),
							    .bea(bitmask1));
endmodule // ibm_mem_B_SRAM_2





