module WRAPB8C_RA256X13D4P3W1R2M1 (CLOCKP1, WEP1, A_RP1, A_RP2, A_WP1, DI_WP1, DO_RP1, DO_RP2);

parameter dataWidth = 13;
parameter addrWidth = 8;
parameter depth = 256;

input CLOCKP1, WEP1;
input [addrWidth - 1:0] A_RP1, A_RP2, A_WP1;
input [dataWidth - 1:0] DI_WP1;

output [dataWidth - 1:0] DO_RP1, DO_RP2;

wire [dataWidth - 1:0] DO_RP1, DO_RP2;
reg [dataWidth - 1:0] mem [0: depth - 1];


always @ (negedge CLOCKP1)
  if (WEP1)
      mem[A_WP1] <= DI_WP1;

`ifdef AXIS_CWTB
  wire write_through_p1 = WEP1 & CLOCKP1 & (A_WP1 == A_RP1);
  wire write_through_p2 = WEP1 & CLOCKP1 & (A_WP1 == A_RP2);
  assign DO_RP1 = write_through_p1 ? DI_WP1 : mem[A_RP1];
  assign DO_RP2 = write_through_p2 ? DI_WP1 : mem[A_RP2];
`else
assign  DO_RP1 = mem[A_RP1];

assign  DO_RP2 = mem[A_RP2];
`endif

endmodule

