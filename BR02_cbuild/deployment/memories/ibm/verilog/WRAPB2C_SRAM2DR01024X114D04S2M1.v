module WRAPB2C_SRAM2DR01024X114D04S2M1
             ( ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  READ_P1, WRITE_P1,
               READ_P2, WRITE_P2,  SCAN_IN,   SCAN_OUT,  A_P1,      A_P2,      DIN_P1,    DIN_P2,    DOUT_P1,   DOUT_P2 );

parameter dataWidth = 11;
parameter addrWidth = 10;
parameter depth = 1024;

output          SCAN_OUT;
output  [dataWidth - 1 :0] DOUT_P1,  DOUT_P2;

input          ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  READ_P1, WRITE_P1,
               READ_P2, WRITE_P2,  SCAN_IN;
input    [addrWidth - 1 :0] A_P1,      A_P2;
input   [dataWidth - 1 :0] DIN_P1,    DIN_P2;

reg [dataWidth - 1 :0] DOUT_P1,  DOUT_P2;
reg [dataWidth - 1:0] mem [0: depth - 1];

always @ (negedge CLOCK_P1)
  if (WRITE_P1)
    mem[A_P1] <= DIN_P1;

always @ (posedge CLOCK_P1)
  if (READ_P1 & ARYSEL_P1) 
    DOUT_P1 <= mem[A_P1];

always @ (negedge CLOCK_P2)
  if (WRITE_P2)
    mem[A_P2] <= DIN_P2;

always @ (posedge CLOCK_P2)
  if (READ_P2 & ARYSEL_P2)
    DOUT_P2 <= mem[A_P2];

endmodule
