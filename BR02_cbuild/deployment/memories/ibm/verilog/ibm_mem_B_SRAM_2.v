//
// Scripts generates wrapper that instantiates this module
// that instantation will be called "sram" in order to match
// the IBM hier.  This must then instantiate another laer called io
//
// 'B' rams have BYTE enables
//
module ibm_mem_B_SRAM_2 ( ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  RDWRT_P1,
			  RDWRT_P2,  DIN_P1,    DIN_P2,    DOUT_P1,   DOUT_P2,   
			  A_P1,      A_P2,      BW_P1,     BW_P2 );
   parameter addrWidth = 9;
   parameter width = 52;
   parameter depth = 512;

   parameter bytes = (width / 8) + (width % 8) ? 1 : 0;
   
   output [width - 1:0] DOUT_P1, DOUT_P2;

   input 		ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  RDWRT_P1,  RDWRT_P2;
   input [width - 1:0] 	DIN_P1,    DIN_P2;
   input [addrWidth - 1 :0] A_P1,      A_P2;
   input [bytes - 1 : 0]    BW_P1, BW_P2;

   ibm_mem_B_SRAM_2_io #(addrWidth, width, depth) io
     ( .ARYSEL_P1 (ARYSEL_P1),
       .ARYSEL_P2 (ARYSEL_P2),
       .CLOCK_P1  (CLOCK_P1), 
       .CLOCK_P2  (CLOCK_P2), 
       .RDWRT_P1  (RDWRT_P1), 
       .RDWRT_P2  (RDWRT_P2), 
       .DIN_P1    (DIN_P1),   
       .DIN_P2    (DIN_P2),   
       .DOUT_P1   (DOUT_P1),  
       .DOUT_P2   (DOUT_P2),  
       .A_P1      (A_P1),	    
       .A_P2      (A_P2),	    
       .BW_P1     (BW_P1),    
       .BW_P2     (BW_P2)     
       );
   
endmodule // ibm_mem_B_SRAM_2

//
// Instantiate hierarchy.  carbon_mem model must be called 'm' to
// match the IBM hierarchies
module ibm_mem_B_SRAM_2_io ( ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  RDWRT_P1,
			     RDWRT_P2,  DIN_P1,    DIN_P2,    DOUT_P1,   DOUT_P2,   
			     A_P1,      A_P2,      BW_P1,     BW_P2 );
   parameter addrWidth = 9;
   parameter width = 52;
   parameter depth = 512;

   parameter bytes = (width / 8) + (width % 8) ? 1 : 0;
   
   output [width - 1:0] DOUT_P1, DOUT_P2;

   input 		ARYSEL_P1, ARYSEL_P2, CLOCK_P1,  CLOCK_P2,  RDWRT_P1,  RDWRT_P2;
   input [width - 1:0] 	DIN_P1,    DIN_P2;
   input [addrWidth - 1 :0] A_P1,      A_P2;
   input [bytes - 1 : 0]    BW_P1, BW_P2;
   
   reg [width - 1 : 0] 	    bitmask1, bitmask2;
   integer 		    i,j;
   
   always @(BW_P1) begin
      bitmask1 = {width{1'b0}};      
      for (i = 0; i < bytes; i = i+1) begin
	 bitmask1 = bitmask1 << i;
	 bitmask1 = bitmask1 | {8{BW_P1[bytes - i - 1]}};	 
      end
   end
   
   always @(BW_P2) begin
      bitmask2 = {width{1'b0}};      
      for (j = 0; j < bytes; j = j+1) begin
	 bitmask2 = bitmask2 << j;
	 bitmask2 = bitmask2 | {8{BW_P2[bytes - j - 1]}};	 
      end
   end
   
   carbon_mem_2rw_reg_bitmask #(addrWidth, width, depth) m (.clka(CLOCK_P1),
							     .clkb(CLOCK_P2),
							     .adra(A_P1),
							     .adrb(A_P2),
							     .dia(DIN_P1),
							     .dib(DIN_P2),
							     .wea(~RDWRT_P1),
							     .web(~RDWRT_P2),
							     .doa(DOUT_P1),
							     .dob(DOUT_P2),
							     .bea(bitmask1),
							     .beb(bitmask2));

endmodule // ibm_mem_B_SRAM_2





