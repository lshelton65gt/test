/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

module ts13e1p11shsb01 (Q,RSCOUT,ADR,D,WEM,WE,OE,ME,RSCIN,RSCEN,RSCRST,CLK,AWT,TADR,TD,TWEM,TWE,TOE,TME,BISTE,TEST1);
   parameter WIDTH = 32;
   parameter DEPTH = 8;
   parameter ADR_WIDTH = 3;
   
   output [WIDTH-1:0] Q;
   output 	      RSCOUT;
   input [ADR_WIDTH-1:0] ADR;
   input [WIDTH-1:0] 	 D;
   input [WIDTH-1:0] 	 WEM;
   input 		 WE;
   input 		 OE;
   input 		 ME;
   input 		 RSCIN;
   input 		 RSCEN;
   input 		 RSCRST;
   input 		 CLK;
   input 		 AWT;
   input [ADR_WIDTH-1:0] TADR;
   input [WIDTH-1:0] 	 TD;
   input [WIDTH-1:0] 	 TWEM;
   input 		 TWE;
   input 		 TOE;
   input 		 TME;
   input 		 BISTE;
   input 		 TEST1;

   reg [WIDTH-1:0] 	 mem [DEPTH-1:0];
   reg [WIDTH-1:0] 	 Q_buf;
   reg [WIDTH-1:0] 	 mem_tmp;
   
   
   assign Q = OE ? (AWT ? D : Q_buf) : {WIDTH{1'bz}};

   always @(posedge CLK) begin
      if(ME) begin
	 if (WE) begin
	    mem_tmp = mem[ADR] & (~WEM);	    
	    mem[ADR] <= (D & WEM) | mem_tmp;
	 end else begin
	    Q_buf <= mem[ADR];
	 end	 
      end      
   end
   
endmodule
