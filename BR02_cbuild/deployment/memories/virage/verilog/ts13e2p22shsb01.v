/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

///
///
// This is a synchronous 2-port 2r/w memory
//
//  Software           : Rev: 3.4.4 (build REL-3-4-4-2003-08-15)       
//  Library Format     : Rev: 1.05.00                                  
//  Compiler Name      : ts13e2p22shsb01                               
//  Date of Generation : Thu Feb 19 14:21:35 PST 2004                  
module ts13e2p22shsb01 (QA, QB, RSCOUTA, ADRA, DA, WEMA, WEA, OEA, MEA, 
			RSCINA, RSCENA, RSCRSTA, CLKA, AWTA, TADRA, TDA, 
			TWEMA, TWEA, TOEA, TMEA, BISTEA, TEST1A, TEST2A, 
			ADRB, DB, WEMB,    WEB, OEB, MEB, CLKB, AWTB, TADRB, 
			TDB, TWEMB, TWEB, TOEB, TMEB, BISTEB, TEST1B,
			TEST2B);
   

   
   parameter WIDTH = 32;
   parameter DEPTH = 8;
   parameter ADR_WIDTH = 3;
   
   //               Output Ports                                         
   output [WIDTH-1:0] QA;
   output [WIDTH-1:0] QB;
   output 	      RSCOUTA;
   
   //               Input Ports:                                         
   input [ADR_WIDTH-1:0] ADRA;
   input [WIDTH-1:0] 	 DA;
   input [WIDTH-1:0] 	 WEMA;   
   input 		 WEA;
   input 		 OEA;
   input 		 MEA;
   input 		 RSCINA;
   input 		 RSCENA;
   input 		 RSCRSTA;   
   input 		 CLKA;
   input 		 AWTA;
   input [ADR_WIDTH - 1:0] TADRA;
   input [WIDTH-1:0] 	   TDA;
   input [WIDTH-1:0] 	   TWEMA;
   input 		   TWEA;
   input 		   TOEA;
   input 		   TMEA;
   input 		   BISTEA;
   input 		   TEST1A;
   input 		   TEST2A;
   input [ADR_WIDTH-1:0]   ADRB;   
   input [WIDTH-1:0] 	   DB;
   input [WIDTH-1:0] 	   WEMB;   
   input 		   WEB;
   input 		   OEB;
   input 		   MEB;
   input 		   CLKB;
   input 		   AWTB;
   input [ADR_WIDTH-1:0]   TADRB;
   
   input [WIDTH-1:0] 	   TDB;  
   input [WIDTH-1:0] 	   TWEMB;
   input 		   TWEB;
   input 		   TOEB;
   input 		   TMEB;
   input 		   BISTEB;
   input 		   TEST1B;
   input 		   TEST2B;
   

   reg [WIDTH-1:0] 	   mem [0:DEPTH-1];

   reg [WIDTH-1:0] 	   QA_buf, QB_buf, QA_tmp, QB_tmp;   

   assign 		   QA = OEA ? QA_buf : {WIDTH{1'bz}};
   assign 		   QB = OEB ? QB_buf : {WIDTH{1'bz}};
   
   // port-A   
   always @(posedge CLKA) begin
      if (MEA) begin
	 if (WEA) begin
	    QA_tmp = mem[ADRA] & (~WEMA);
	    mem[ADRA] <= QA_tmp | (DA & WEMA);
	 end
	 QA_buf <= mem[ADRA];
      end
   end
   
   // port-B
   always @(posedge CLKB) begin
      if (MEB) begin
	 if (WEB) begin
	    QB_tmp = mem[ADRB] & (~WEMB);	    
	    mem[ADRB] <= QB_tmp | (DB & WEMB);
	 end
	 QB_buf <= mem[ADRB];
      end
   end
endmodule
