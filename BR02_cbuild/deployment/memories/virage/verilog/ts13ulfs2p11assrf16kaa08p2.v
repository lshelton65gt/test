/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

module ts13ulfs2p11assrf16kaa08p2 (QB, ADRA, DA, WEA, WEMA, MEA, CLKA, ADRB,
				   TADRA, TDA, TWEMA, TWEA, TMEA, BISTEA,  
				   TADRB, BISTEB);



   parameter WIDTH = 32;
   parameter DEPTH = 8;
   parameter ADR_WIDTH = 3;
   
   output [WIDTH-1:0] QB;

   input [ADR_WIDTH - 1:0] ADRA, ADRB;
   input [WIDTH-1:0] 	   DA;
   input 		   WEA;
   input [WIDTH-1:0] 	   WEMA;   
   input 		   MEA;
   input 		   CLKA;


   input [ADR_WIDTH - 1:0] TADRA;
   input [WIDTH-1:0] 	   TDA;
   input [WIDTH-1:0] 	   TWEMA;
   input 		   TWEA;
   input 		   TMEA;
   input 		   BISTEA;
   input [ADR_WIDTH - 1:0] TADRB;
   input 		   BISTEB;


   reg [WIDTH-1:0] 	   mem [0:DEPTH-1];
   reg [WIDTH-1:0] 	   mem_tmp;
   
   assign 		   QB = mem[ADRB];
   
   always@(posedge CLKA) begin
      if (MEA & WEA) begin
	 mem_tmp = mem[ADRA] & (~WEMA);	 
	 mem[ADRA] <= (DA & WEMA) | mem_tmp;	 
      end      
   end
endmodule // assrfalfs2p96x64cm2
