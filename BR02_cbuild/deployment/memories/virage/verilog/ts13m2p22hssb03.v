/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
module ts13m2p22hssb03 (QA, ADRA, DA, WEMA, WEA, OEA, MEA, CLKA,
			QB, ADRB, DB, WEMB, WEB, OEB, MEB, CLKB );
   
   parameter WIDTH = 32;
   parameter DEPTH = 8;
   parameter ADR_WIDTH = 3;
   
   output [WIDTH-1:0] QA, QB;

   input [ADR_WIDTH - 1:0] ADRA, ADRB;
   input [WIDTH-1:0] 	   DA, DB;
   input [WIDTH-1:0] 	   WEMA, WEMB;   
   input 		   WEA, WEB;
   input 		   MEA, MEB;
   input 		   CLKA, CLKB;
   input 		   OEA, OEB;

   reg [WIDTH-1:0] 	   mem [0:DEPTH-1];

   reg [WIDTH-1:0] 	   QA_buf, QB_buf, QA_tmp, QB_tmp;   

   assign 		   QA = OEA ? QA_buf : {WIDTH{1'bz}};
   assign 		   QB = OEB ? QB_buf : {WIDTH{1'bz}};
   
   // port-A   
   always @(posedge CLKA) begin
      if (MEA) begin
	 if (WEA) begin
	    QA_tmp = mem[ADRA] & (~WEMA);
	    mem[ADRA] <= QA_tmp | (DA & WEMA);
	 end
	 QA_buf <= mem[ADRA];
      end
   end
   
   // port-B
   always @(posedge CLKB) begin
      if (MEB) begin
	 if (WEB) begin
	    QB_tmp = mem[ADRB] & (~WEMB);	    
	    mem[ADRB] <= (DB & WEMB) | QB_tmp;
	 end
	 QB_buf <= mem[ADRB];
      end
   end
endmodule // ts13m2p22hssb03
