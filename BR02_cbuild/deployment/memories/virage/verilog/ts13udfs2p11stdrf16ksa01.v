/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

//
//                                                                     
//---------------------------------------------------------------------
//                                                                     
//  Software           : Rev: 3.4.4 (build REL-3-4-4-2003-08-15)       
//  Library Format     : Rev: 1.05.00                                  
//  Compiler Name      : ts13udfs2p11stdrf16ksa01                      
//  Date of Generation : Mon Feb 23 15:55:35 PST 2004                  
//                                                                     
//---------------------------------------------------------------------
//   --------------------------------------------------------------     
//                       Template Revision : 1.2.0                      
//   --------------------------------------------------------------     

//                      * Synchronous, 2-Port SRAM *                  
//                      * Verilog Behavioral Model *                  
//                THIS IS A SYNCHRONOUS 2-PORT MEMORY MODEL           
// 
module ts13udfs2p11stdrf16ksa01 (RSCOUT, CSCOUT, CURERR, QB, ADRA, WEMA, DA, WEA, MEA, CLKA, 
				 BISTE, RST, RSCLK, RSCIN, RSCEN, CSCIN, CSCEN,
				 RECEN, SCFORCE, CLRERR, CURERRIN, ADRB, MEB, CLKB, AWTB);
   

   parameter WIDTH = 32;
   parameter DEPTH = 8;
   parameter ADR_WIDTH = 3;

   output RSCOUT;
   output CSCOUT;
   output CURERR;
   output [WIDTH-1:0] QB;

   input [ADR_WIDTH-1:0] ADRA;
   input [WIDTH-1:0] 	 DA;
   input [WIDTH-1:0] 	 WEMA;
   input 		 WEA;
   input 		 MEA;
   input 		 CLKA;
   input 		 BISTE;
   input 		 RST;
   input 		 RSCLK;
   input 		 RSCIN;
   input 		 RSCEN;
   input 		 CSCIN;
   input 		 CSCEN;
   input 		 RECEN;
   input 		 SCFORCE;
   input 		 CLRERR;
   input 		 CURERRIN;
   input [ADR_WIDTH-1:0] ADRB;
   input 		 MEB;
   input 		 CLKB;
   input 		 AWTB;

   reg [WIDTH-1:0] 	 mem [0:DEPTH-1];
   reg [WIDTH-1:0] 	 QB_buf;
   reg [WIDTH-1:0] 	 mem_tmp;
   
   assign 		 QB = AWTB ? DA : QB_buf;

   always @(posedge CLKB) begin
      if (MEB)
	QB_buf <= mem[ADRB];      
   end
   
   always@(posedge CLKA) begin
      if (MEA & WEA) begin
	 mem_tmp = mem[ADRA] & (~WEMA);	 
	 mem[ADRA] <= (DA & WEMA) | mem_tmp;
      end      
   end
endmodule // assrfalfs2p96x64cm2
