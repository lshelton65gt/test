/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
module ts13ulfs2p11asdrf16ksa10 (ADRA, DA, WEMA, WEA, MEA, CLKA,
				 QB, ADRB, CLKB, OEB, MEB);
   parameter WIDTH = 32;
   parameter DEPTH = 8;
   parameter ADR_WIDTH = 3;
   
   output [WIDTH-1:0] QB;

   input [ADR_WIDTH - 1:0] ADRA;
   input [WIDTH-1:0] 	   DA;
   input [WIDTH-1:0] 	   WEMA;
   input 		   WEA;
   input 		   MEA;
   input 		   CLKA;
   input [ADR_WIDTH - 1:0] ADRB;
   input 		   CLKB;
   input 		   OEB;
   input 		   MEB;

   reg [WIDTH-1:0] 	   mem [0:DEPTH-1];
   reg [WIDTH-1:0] 	   mem_tmp;

   reg [WIDTH-1:0] 	   Q_buf;
   
   // Port A - write port
   always @(posedge CLKA) begin
      if (WEA & MEA) begin
	 mem_tmp = mem[ADRA] & (~WEMA);	 
	 mem[ADRA] <= (DA & WEMA) | mem_tmp;
      end
   end

   // Port B - read port
   always@(posedge CLKB) begin
      if (MEB) begin
	 Q_buf <= mem[ADRB];
      end
   end

   assign QB = OEB ? Q_buf : {WIDTH{1'bz}};
   
endmodule // ts13ulf2p11asdrf16ksa10
