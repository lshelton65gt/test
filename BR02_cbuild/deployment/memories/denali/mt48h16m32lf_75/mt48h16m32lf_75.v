// Module:                      mt48h16m32lf_75
// SOMA file:                   /home/bsm1/SystemModeling/Carbon/denali/mt48h16m32lf_75.soma
// Initial contents file:       

// PLEASE do not remove, modify or comment out the timescale declaration below.
// Doing so will cause the scheduling of the pins in Denali models to be
// inaccurate and cause simulation problems and possible undetected errors or
// erroneous errors.  It must remain `timescale 1ps/1ps for accurate simulation.   
`timescale 1ps/1ps

module mt48h16m32lf_75(
    a,
    rasbar,
    casbar,
    webar,
    csbar,
    dqm,
    clk,
    cke,
    dq,
    ba
);
    parameter memory_spec = "mt48h16m32lf_75.soma";
    input [12:0] a;
    input rasbar;
    input casbar;
    input webar;
    input csbar;
    input [3:0] dqm;
    input clk;
    input cke;
    inout [31:0] dq;
    input [1:0] ba;
    
    wire [31:0] dq_i= dq;
    wire [31:0] dq_o;

    assign dq[31:24] = dqm[3] ? dq_o[31:24] : 8'bz;
    assign dq[23:16] = dqm[2] ? dq_o[23:16] : 8'bz;
    assign dq[15: 8] = dqm[1] ? dq_o[15: 8] : 8'bz;
    assign dq[ 7: 0] = dqm[0] ? dq_o[ 7: 0] : 8'bz;

    mt48h16m32lf_75_cm #( memory_spec ) cmodel (
        .clk      ( clk      )
      , .cke      ( cke      )
      , .csbar    ( csbar    )
      , .rasbar   ( rasbar   )
      , .casbar   ( casbar   )
      , .webar    ( webar    )
      , .a        ( a        )
      , .ba       ( ba       )
      , .dq_i     ( dq_i     )
      , .dq_o     ( dq_o     )
      , .dqm      ( dqm      )
    );

endmodule

module mt48h16m32lf_75_cm(
    a,
    rasbar,
    casbar,
    webar,
    csbar,
    dqm,
    clk,
    cke,
    dq_i,
    dq_o,
    ba
);
    parameter memory_spec = "mt48h16m32lf_75.soma";
    input [12:0] a;
    input rasbar;
    input casbar;
    input webar;
    input csbar;
    input [3:0] dqm;
    input clk;
    input cke;
    input  [31:0] dq_i;
    output [31:0] dq_o;
    input [1:0] ba;
    
endmodule

