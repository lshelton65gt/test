#include <cstring>
#include <map>
#include "DenaliMemory.h"

#include "carbon/carbon_shelltypes.h"

#include <iostream>
using namespace std;

class Mt48h16m32lf_75 : public DenaliMemory
{
public:
   Mt48h16m32lf_75(string instance, string somaFile, string loadFile="")
   : DenaliMemory(instance, somaFile, loadFile) {}

   virtual void init() // need separate init routine.
   {
     DenaliMemory::init();

     clk    = getPin("clk");
     cke    = getPin("cke");
     csbar  = getPin("csbar");
     rasbar = getPin("rasbar");
     casbar = getPin("casbar");
     webar  = getPin("webar");
     ba     = getPin("ba");
     a      = getPin("a");
     dqm    = getPin("dqm");
     dq     = getPin("dq");

     // Initialize inputs

     clk->init(     DENALI_D0 );
     cke->init(     DENALI_D0 );
     csbar->init(   DENALI_D1 );
     rasbar->init(  DENALI_D1 );
     casbar->init(  DENALI_D1 );
     webar->init(   DENALI_D1 );
     ba->init(      DENALI_D0 );
     a->init(       DENALI_D0 );
     dqm->init(     DENALI_D1 );
     dq->init(      DENALI_Dz );
  }

//protected:

  DenaliPin * clk    ; 
  DenaliPin * cke    ; 
  DenaliPin * csbar  ; 
  DenaliPin * rasbar ; 
  DenaliPin * casbar ; 
  DenaliPin * webar  ; 
  DenaliPin * ba     ; 
  DenaliPin * a      ; 
  DenaliPin * dqm    ; 
  DenaliPin * dq     ; 
};

typedef enum CDSmt48h16m32lf_75_cmContext{
  eCDSmt48h16m32lf_75_cmFallck,
  eCDSmt48h16m32lf_75_cmRiseck,
}  CDSmt48h16m32lf_75_cmContext;

extern __C__ void* cds_mt48h16m32lf_75_cm_create(
    int numParams, CModelParam* cmodelParams, const char* inst);

extern __C__ void cds_mt48h16m32lf_75_cm_misc(
    void* hndl, CarbonCModelReason reason, void* cmodelData);

extern __C__ void cds_mt48h16m32lf_75_cm_destroy(void* hndl);

extern __C__ void cds_mt48h16m32lf_75_cm_run(
      void* hndl
    , CDSmt48h16m32lf_75_cmContext context
    , const CarbonUInt32* a      //  Input, size = 1 word(s)
    , const CarbonUInt32* rasbar //  Input, size = 1 word(s)
    , const CarbonUInt32* casbar //  Input, size = 1 word(s)
    , const CarbonUInt32* webar  //  Input, size = 1 word(s)
    , const CarbonUInt32* csbar  //  Input, size = 1 word(s)
    , const CarbonUInt32* dqm    //  Input, size = 1 word(s)
    , const CarbonUInt32* clk    //  Input, size = 1 word(s)
    , const CarbonUInt32* cke    //  Input, size = 1 word(s)
    , const CarbonUInt32* dq_i   //  Input, size = 1 word(s)
    ,       CarbonUInt32* dq_o   // Output, size = 1 word(s)
    , const CarbonUInt32* ba     //  Input, size = 1 word(s)
  );

// ============================================================

void* cds_mt48h16m32lf_75_cm_create(
  int numParams, CModelParam* cmodelParams, const char* inst )
{
  string somaFile; // Get SOMA file from parameters

  for(int i = 0; i < numParams; ++i) 
    if(strcmp(cmodelParams[i].paramName, "memory_spec") == 0) 
      somaFile = cmodelParams[i].paramValue;

  Mt48h16m32lf_75* memory = new Mt48h16m32lf_75(inst, somaFile);
   
  memory->init(); // Constructs Denali Pins.

  return reinterpret_cast<void*>(memory);
}

void cds_mt48h16m32lf_75_cm_misc(
  void* hndl, CarbonCModelReason reason, void* cmodelData)
{
  if (reason == eCarbonCModelID) {
    assert(hndl);
    Mt48h16m32lf_75* memory = reinterpret_cast<Mt48h16m32lf_75*>(hndl);
    memory->setCarbonObj( reinterpret_cast<CarbonObjectID*>(cmodelData) );
  }
}

void cds_mt48h16m32lf_75_cm_run(
      void* hndl
    , CDSmt48h16m32lf_75_cmContext context
    , const CarbonUInt32* a      //  Input, size = 1 word(s)
    , const CarbonUInt32* rasbar //  Input, size = 1 word(s)
    , const CarbonUInt32* casbar //  Input, size = 1 word(s)
    , const CarbonUInt32* webar  //  Input, size = 1 word(s)
    , const CarbonUInt32* csbar  //  Input, size = 1 word(s)
    , const CarbonUInt32* dqm    //  Input, size = 1 word(s)
    , const CarbonUInt32* clk    //  Input, size = 1 word(s)
    , const CarbonUInt32* cke    //  Input, size = 1 word(s)
    , const CarbonUInt32* dq_i   //  Input, size = 1 word(s)
    ,       CarbonUInt32* dq_o   // Output, size = 1 word(s)
    , const CarbonUInt32* ba     //  Input, size = 1 word(s)
)
{
  Mt48h16m32lf_75 * mem = reinterpret_cast<Mt48h16m32lf_75*>(hndl);
  CarbonTime   ctime = carbonGetSimulationTime(mem->getVHM());
  DENALItimeT  dtime = CarbonTimeToDenaliTime(ctime);
  
  setCurrentDenaliInstance(mem);  

  // copy carbon inputs to Denali model

  mem->clk->input(        clk,   dtime );
  mem->cke->input(        cke,   dtime );
  mem->csbar->input(    csbar,   dtime );
  mem->rasbar->input(  rasbar,   dtime );
  mem->casbar->input(  casbar,   dtime );
  mem->webar->input(    webar,   dtime );
  mem->ba->input(          ba,   dtime );
  mem->a->input(            a,   dtime );
  mem->dqm->input(        dqm,   dtime );
  mem->dq->input(          dq_i, dtime );

  DENALIeval( mem->getDenaliHdl() );
  
  // drive carbon outputs from Denali model

  if(dq_o) mem->dq->output(dq_o);
}  

void cds_mt48h16m32lf_75_cm_destroy(void* hndl)
{
  delete(reinterpret_cast<Mt48h16m32lf_75*>(hndl));
}
