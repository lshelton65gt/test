/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <cstdio>
#include <cstring>
#include <cerrno>
#include <string>

using namespace std;

#include <unistd.h>
#include <fcntl.h>
#include <strings.h>

#include "systemc.h"

#include "libmt48h16m32lf_75.systemc.h"

SC_MODULE(Testbench)
{
  sc_clock clk;

  // Signals

  sc_signal<sc_uint<13> >   a;  
  sc_signal<sc_uint<2> >    ba;  
  sc_signal<sc_lv<32> >     dq;  
  sc_signal<sc_uint<4> >    dqm;  
  sc_signal<bool>           csbar;  
  sc_signal<bool>           webar;  
  sc_signal<bool>           casbar;  
  sc_signal<bool>           rasbar;  
  sc_signal<bool>           cke;  

  // Constructor
  Testbench(sc_module_name name);

  SC_HAS_PROCESS(Testbench);
  
  // Destructor
  ~Testbench();

  // VHM Object
  mt48h16m32lf_75 *vhm;

  // Processes
  void configure(void);
  void watchdog(void);

private:
  sc_event configDone;

};

Testbench::Testbench(sc_module_name name)
: sc_module(name)
, clk("clk", 10, SC_US) // 100 KHz = 10 uS
, a("a")  
, ba("ba")  
, dq("dq")  
, dqm("dqm")  
, csbar("csbar")  
, webar("webar")  
, casbar("casbar")  
, rasbar("rasbar")  
, cke("cke")  
{

  // Create VHM
  vhm = new mt48h16m32lf_75("mt48h16m32lf_75");
  assert(vhm);

  vhm->carbonSCWaveInitFSDB();
  vhm->carbonSCDumpVars();
  
  // Connect Signals to VHM

  vhm->a      ( a      );
  vhm->ba     ( ba     );
  vhm->dq     ( dq     );
  vhm->dqm    ( dqm    );
  vhm->csbar  ( csbar  );
  vhm->webar  ( webar  );
  vhm->casbar ( casbar );
  vhm->rasbar ( rasbar );
  vhm->cke    ( cke    );
  vhm->clk    ( clk    );

  // Configure Process (e.g. reset chip)
  SC_THREAD(configure);

  SC_THREAD(watchdog); // watchdog timer
}

// Destructor
Testbench::~Testbench()
{
  cout << "Destructing Testbench." << endl;

  vhm->carbonSCDumpFlush();

  delete vhm;
}

// sc_thread -- watchdog timer

void Testbench::watchdog()
{
  wait( clk.period() * 50 );

  cout << "WatchDog Timer Expired: " << sc_time_stamp() << endl;

  sc_stop();
}

void Testbench::configure()
{
  CarbonStatus status;

  cout << "Reset chip." << endl;

  cke     = true ;
  csbar   = false ;
  webar   = true ;
  rasbar  = true ;
  casbar  = true ;

  configDone.notify(SC_ZERO_TIME);

//sc_stop();
}

int sc_main (int argc , char *argv[]) 
{
//bool traceOn = true;

//CarbonStatus status= carbonSetFilePath(".:../vhm");

//assert(status == eCarbon_OK);

  Testbench * tb = new Testbench("Testbench");

  // Setup the Test
  cout << "--------------------" << endl;
  cout << "--- Running test ---" << endl;
  cout << "--------------------" << endl;
  
  // Run
  sc_start();
  
  delete tb;

  return 0;
}
