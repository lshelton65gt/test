/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/*
This file provides the functions that connect Denali to carbon
*/

#include "DenaliCmodel.h"

DenaliCmodelData* gCurrentInstance;

#include <cassert>
#include <iostream>

// Callback Functions that Denali needs for integration into 
// Carbon.  See Denali's Yukon API documentation and examples
// for details
void DENALIEreportPinHandle(char *pinName, DENALIpinHandleT pinHandle, int busSize)
{

	/*
	** Allocate a new pin
	*/
	PinData *tmpPin = new PinData;
	tmpPin->busSize = busSize;
	tmpPin->pinHdl = pinHandle;
	tmpPin->driverHdl = 0;
	tmpPin->value = new DENALIdataT[busSize];
	tmpPin->updateMe = false;
	
	gPinNameMap[std::string(pinName)] = tmpPin;
}

void DENALIEgetDriver(char *pinName,
					DENALIdriverHandleT * driverHandle,
	       			int busSize)
{
	std::string pinNameStr(pinName);
	PinData* p = gPinNameMap[pinNameStr];
	*driverHandle = reinterpret_cast<DENALIdriverHandleT>(p);
}

void DENALIEscheduleDriver(
    DENALIdriverHandleT driverHandle
  , DENALIdataPT value
  , int busSize
  , DENALItimeT delay)
{
	PinData* p = reinterpret_cast<PinData*>(driverHandle);
	memcpy(p->value, value, busSize * sizeof(DENALIdataT));
	p->updateMe = true;
}

void DENALIEgetTimeResolution(DENALItimeT * resolution)
{
  resolution->high = 0;
  resolution->low = 1000000;
}

void DENALIEgetTimeNow(DENALItimeT * now)
{
	CarbonObjectID* vhm = getCurrentDenaliInstance()->vhmId;
	
	if(vhm) {
		CarbonTime t = carbonGetSimulationTime(vhm);
		now->high = (t >> 32) & 0xFFFFFFFF;
		now->low = t & 0xFFFFFFFF;
	} else {
		// This code gets called once before the VHM is done being constructed
		// so we'll assume that if the vhm handle is 0, it's because the
		// construction isn't done and this must be time 0
		now->high = now->low = 0;
	}
}

void DENALIEreportMessage(char *message,
           int msgCode,
           DENALIseverityT severity)
{

	std::cout << std::dec << "Denalie Mesage " << msgCode << " " << severity << " "<< message;
}

void DENALIEfatalError(void)
{
//  cout << "DENALIEfatalError!!!!!" << endl;
  assert(0);
}

/*
** The following routines are the ones that the CModel code
** can use to pass info to the denali data
*/

// Sets all bits of a pin to a given value and reports
// the value to denali.  Intended to be used at initialization
void initPin(PinData*p, DENALIdataT v)
{
	DENALItimeT t;
	t.low = t.high = 0;
	for(int i = 0; i < p->busSize; ++i) {
		p->value[i] = v;
	}	
	DENALIreportEvent( p->pinHdl, p->value, p->busSize, t);
}

// Used by CModel to indicate current denali model in case
// several are in the system
void setCurrentDenaliInstance(DenaliCmodelData *instance)
{
  gCurrentInstance = instance;
}

// Returns the current denali model
DenaliCmodelData *getCurrentDenaliInstance() { return gCurrentInstance; }

// Report a carbon formated value to a denali pin
void reportPinEvent (PinData* pin, const UInt32* val, DENALItimeT dtime) 
{
	setDenaliValueFromCarbonValue(pin, val);
    DENALIreportEvent(pin->pinHdl, pin->value, pin->busSize, dtime);
}

// Utility functions for use by DenaliCmodel.cpp routines
//
// Note: The denali Yukon docs say that the index into a value
// array matches the index into a vector. So if you have the 
// verilog vector foo[3:0] value[3] is equivalent to foo[3] and
// value[0] is equivalent to foo[0]
//
// However, in practice I've discoverd that the exact opposite is 
// true.  value [0] is mapping to foo[3] and vice versa

void setDenaliValueFromCarbonValue(PinData*pin, const UInt32* val)
{
	UInt32 tmp = 0xDEADBEEF;
	for(int i =0; i < pin->busSize; ++i) {
		if (i % 32 == 0) {
			tmp = val[i / 32];
		}
		pin->value[pin->busSize - 1 - i] = 
			(tmp & 0x1) ? DENALI_D1 : DENALI_D0;
		tmp >>= 1;	
	}
}

// See note above regarding value arrays.
void DenaliFourStateToCarbon(DENALIdataT* d, UInt32* out, UInt32* en, int bits)
{
	UInt32 tmpVal = 0, tmpDrv = 0;

	if(d[0] == DENALI_Dz) *en = 0;
	else *en = 1;
	
	for (int i = 0; i < bits; ++i) {
		if (d[bits - 1 - i] == DENALI_D1) tmpVal |= 0x1;
		
		// if last bit in word, copy everything over, else shift
		if((i % 32) == 31) {
			out[i / 32] = tmpVal;
			tmpVal = 0;			
		} else {
			tmpVal <<= 1;
		}
	}
	if ((bits -1) % 32 != 31) {
		out[(bits-1) / 32] = tmpVal;
	}
}

DENALItimeT CarbonTimeToDenaliTime (CarbonTime t) 
{
	DENALItimeT dt;
	/* on some platforms could this just be a cast? */
	dt.low = t & 0xFFFFFFFF;
	dt.high = (t >> 32) & 0xFFFFFFFF;
	return dt;
}
