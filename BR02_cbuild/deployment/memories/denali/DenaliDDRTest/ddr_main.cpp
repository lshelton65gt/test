#include "libddr.h"

#include <iostream>
#include <cassert>

void runCycle(CarbonObjectID*, CarbonNetID*, int n =1);

UInt32 gCommands[] = {
	// 1 - CKE_OFF wait for 10 cycles
	0x800000A0,   
	// 2 - CKE_ON
	0x80000001,
	// 3- PRECHARGE
	0x80000003,
//	0x80001003,
//	0x80002003,
//	0x80003003,
	// 4- NOP (10 cycles)
	0x800000A4,
	// 5 - MRS
   0x80021005,
	// 6 - MRS
   0x84000005,
   // 7- NOP (10 cycles)
   0x800000A4,
   // 8 - ACTIVATE
   0x80070608,
   // 9- NOP (3 cycles)
   0x80000034,
   // 10 - WRITE
   0x8005A506,
   // 11- NOP (10 cycles)
   0x800000A4,
   // 12 - READ
   0x80050007,

};

const int NUM_CMDS = 11;

struct CbData 
{
	int cmdCount;	
	CarbonNetID* cmdNet;
};

void nxtCmd(CarbonObjectID*vhm, CarbonNetID*net, void* hndl, UInt32* val, UInt32*) 
{
	CbData* cbData = reinterpret_cast<CbData*>(hndl);
	UInt32 deselCmd = 0x80000002;
	
	if (*val == 1) {
		if (cbData->cmdCount >= NUM_CMDS) {
			carbonDeposit(vhm, cbData->cmdNet, &deselCmd, 0);
		} else {
			carbonDeposit(vhm, cbData->cmdNet, gCommands + cbData->cmdCount, 0);
			cbData->cmdCount++;
		}
	}
	
}

int main(int argc, char *argv[])
{
	carbonSetFilePath(VHM_DIR_STR);
	CarbonObjectID* vhm = carbon_ddr_create(eCarbonFullDB, eCarbon_NoFlags)	;

	CarbonWaveID* waves = carbonWaveInitFSDB(vhm, "ddr.fsdb", e1ns);
	
	assert(waves);
	carbonDumpVars(waves, 0, "testtop");
	
	CarbonNetID* sysclk = carbonFindNet(vhm, "testtop.sysclk");
	CarbonNetID* cmd = carbonFindNet(vhm, "testtop.nextCmd");
	CarbonNetID* addr = carbonFindNet(vhm, "testtop.cmdAddr");
	CarbonNetID* wrData = carbonFindNet(vhm, "testtop.wrData");
	CarbonNetID* popCmd = carbonFindNet(vhm, "testtop.popCmd");
	
	CbData* cbData = new CbData;
	cbData->cmdCount = 0;
	cbData->cmdNet = cmd;
	
	carbonAddNetValueChangeCB(vhm, nxtCmd, (void*)cbData, popCmd);
	
	runCycle(vhm, sysclk, 200);
		
	carbonDestroy(&vhm);
}

void runCycle(CarbonObjectID* vhm, CarbonNetID* clk, int n) 
{
	static CarbonTime t = 0;
	const CarbonTime STEP = 7;
	
	assert(vhm);
	assert(clk);
	
	UInt32 clkVal = 0;
	for(int i = 0; i < n; ++i) {
		carbonDeposit(vhm, clk, &clkVal, 0);
		carbonSchedule(vhm, t);
		t += STEP;
		clkVal = !clkVal;
		
		carbonDeposit(vhm, clk, &clkVal, 0);
		carbonSchedule(vhm, t);
		t += STEP;
		clkVal = !clkVal;		
	}
		
}
