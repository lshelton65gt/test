module testtop();


   // TB interface
   wire [31:0] nextCmd; // carbon depositSignal
   wire [13:0] cmdAddr; // carbon depositSignal
   wire        sysclk; // carbon depositSignal
   wire [7:0]  rdData; // carbon observeSignal
   wire [7:0]  wrData; // carbon depositSignal

   wire [7:0]  c2d_data;
   wire        c2d_dqs;
   wire [1:0]  c2d_bank;
   wire [11:0] c2d_address;
   
   
   

   defparam    ddrCntrl.DATA_SIZE = 8;
   defparam    ddrCntrl.DQS_SIZE = 1;

   
   ddrCntrl ddrCntrl(
		     // tb interface
		     .sysclk(sysclk),
		     .CmdIn(nextCmd),
		     .popCmd(popCmd),
		     .wrData(wrData),
		     .rdData(rdData),
		     .cmdAddr(cmdAddr),
      
		     // ddr port
		     .clk(c2d_clk),
		     .clk_n(c2d_clk_n),
		     .address(c2d_address),
		     .data(c2d_data),
		     .dm(c2d_dm),
		     .dqs(c2d_dqs),
		     .bank(c2d_bank),
		     .cke(c2d_cke),
		     .cs_n(c2d_cs_n),
		     .cas_n(c2d_cas_n),
		     .ras_n(c2d_ras_n),
		     .we_n(c2d_we_n)
		     );
   
   
   denali_ddr ddr0(
    		   .clk(c2d_clk),
		   .clkbar(c2d_clk_n),
		   .cke(c2d_cke),
		   .csbar(c2d_cs_n),
		   .rasbar(c2d_ras_n),
		   .casbar(c2d_cas_n),
		   .webar(c2d_we_n),
		   .dqm(c2d_dm),
		   .ba(c2d_bank),
		   .address(c2d_address),
		   .data(c2d_data),
		   .dqs(c2d_dqs)
		   );

endmodule
