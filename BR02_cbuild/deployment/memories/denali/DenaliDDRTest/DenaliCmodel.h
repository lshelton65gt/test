#ifndef DENALICMODEL_H_
#define DENALICMODEL_H_

#include "carbon/carbon_capi.h"

#include <cstdio>  // yukon needs this...
#include "yukon/yukon.h"

#include <map>
#include <string>

// This structure can be defined in the cmodel code.  We need
// to have a forward declaration to it so data types can store
// a pointer to it
struct AllPins;

// Data that will get passed to all 
// Carbon CModel routines
struct DenaliCmodelData {
	CarbonObjectID* vhmId;
	DENALIinstanceHandleT denaliHdl;
	AllPins* pins;
};

// Data structure for a pin
struct PinData {
	int busSize;
	DENALIpinHandleT pinHdl;
	DENALIdriverHandleT driverHdl;
	DENALIdataT* value;
	bool updateMe;
};

// Map for associating pin names to data structure
typedef std::map<std::string, PinData*>  PinMap;
extern PinMap gPinNameMap;

// Set and get which Denali model is active
void setCurrentDenaliInstance(DenaliCmodelData* instance);
DenaliCmodelData* getCurrentDenaliInstance();

// Tell Denali about a change in a pin value
void reportPinEvent(PinData*, const UInt32*, DENALItimeT);

// Initialize all bits in a pin to a given value
void initPin(PinData*, DENALIdataT);

// Value conversions
void setDenaliValueFromCarbonValue(PinData*, const UInt32* val);
void DenaliFourStateToCarbon(DENALIdataT*, UInt32*, UInt32*, int);
DENALItimeT CarbonTimeToDenaliTime (CarbonTime t) ;

#endif /*DENALICMODEL_H_*/
