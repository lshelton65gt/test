// Module:                      denali_ddr
// SOMA file:                   /w/ae3/mattg/Work/denali_ddr.soma
// Initial contents file:       

// PLEASE do not remove, modify or comment out the timescale declaration below.
// Doing so will cause the scheduling of the pins in Denali models to be
// inaccurate and cause simulation problems and possible undetected errors or
// erroneous errors.  It must remain `timescale 1ps/1ps for accurate simulation.   
`timescale 1ps/1ps

module denali_ddr(
    clk,
    clkbar,
    cke,
    csbar,
    rasbar,
    casbar,
    webar,
    dqm,
    ba,
    address,
    data,
    dqs
);
    parameter memory_spec = "/w/ae3/mattg/Work/denali_ddr.soma";
    parameter init_file   = "";
    input clk;
    input clkbar;
    input cke;
    input csbar;
    input rasbar;
    input casbar;
    input webar;
    input dqm;
    input [1:0] ba;
    input [11:0] address;
    inout [7:0] data;
`ifdef CARBON
`else
      reg [7:0] den_data;
      assign data = den_data;
`endif
    inout dqs;
`ifdef CARBON
`else
      reg den_dqs;
      assign dqs = den_dqs;
`endif
      
`ifdef CARBON

	wire dqs_cmodel_en, dqs_cmodel_out, dqs_cmodel_in;
	
	assign dqs_cmodel_in = dqs;
	assign dqs = dqs_cmodel_en ? dqs_cmodel_out : 1'bz;
	
	wire data_cmodel_en;
	wire [7:0] data_cmodel_out, data_cmodel_in;
	assign data_cmodel_in = data;
	assign data = data_cmodel_en ? data_cmodel_out : {8{1'bz}};
	
	
	ddr_cmodel ddr_wrap (.clk(clk),
		.clkbar(clkbar),
		.cke(cke),
		.csbar(csbar),
		.rasbar(rasbar),
		.casbar(casbar),
		.webar(webar),
		.dqm(dqm),
		.ba(ba),
		.address(address),
		.data_in(data_cmodel_in),
		.data_out(data_cmodel_out),
		.data_en(data_cmodel_en),
		
		.dqs_in(dqs_cmodel_in),
		.dqs_out(dqs_cmodel_out),
		.dqs_en(dqs_cmodel_en)
		);

`else

initial
    $ddr_sdram_access(clk,clkbar,cke,csbar,rasbar,casbar,webar,dqm,ba,address,data,den_data,dqs,den_dqs);


`endif
endmodule

