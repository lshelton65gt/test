`define DDR_CMD_LOAD      4'b0000
`define DDR_CMD_REFRESH   4'b0001
`define DDR_CMD_PRECHARGE 4'b0010
`define DDR_CMD_ACTIVE    4'b0011
`define DDR_CMD_WRITE     4'b0100
`define DDR_CMD_READ      4'b0101
`define DDR_CMD_TERMINATE 4'b0110
`define DDR_CMD_NOP       4'b0111
`define DDR_CMD_DESELECT  4'b1111

`define CMD_CKE_OFF       4'b0000
`define CMD_CKE_ON        4'b0001
`define CMD_DESEL         4'b0010
`define CMD_PRECHARGE     4'b0011
`define CMD_NOP           4'b0100
`define CMD_MRS           4'b0101
`define CMD_WRITE         4'b0110
`define CMD_READ          4'b0111
`define CMD_ACTIVATE      4'b1000

`define WRITE  4'h0
`define READ   4'h1
`define IDLE   4'h2

module ddrCntrl(
		// tb interface
		sysclk,
		CmdIn,
		popCmd,
		wrData,
		rdData,
		cmdAddr,

		// ddr port
		clk,
		clk_n,
		address,
		data,
		dm,
		dqs,
		bank,
		cke,
		cs_n,
		cas_n,
		ras_n,
		we_n
		);

   parameter DM_SIZE = 2;
   parameter BA_SIZE = 2;
   parameter ADDR_SIZE = 12;
   parameter DATA_SIZE = 32;
   parameter DQS_SIZE = 2;
   parameter CMD_ADDR_SIZE = ADDR_SIZE + BA_SIZE;
   
   input     sysclk;
   input [31:0] CmdIn;
   output 	reg	popCmd;
   //      output 	popCmd;
   input [31:0] wrData;
   output [31:0] rdData;
   input [CMD_ADDR_SIZE - 1:0] cmdAddr;
   
   parameter 		       prechargeWaitMax = 10;   
   

   assign 		       clk = sysclk;
   assign 		       clk_n = ~sysclk;
   
   
   output 		       clk, clk_n, cke, cs_n, cas_n, ras_n, we_n;
   output [ADDR_SIZE-1:0]      address;
   output [DM_SIZE-1:0]        dm;
   output reg [BA_SIZE-1:0]        bank;
   inout [DQS_SIZE-1:0]        dqs;
   inout [DATA_SIZE-1:0]       data;

   parameter 		       STATE_WIDTH = 8;

   assign 		       dm = {DM_SIZE{1'b0}};   
   
   reg [STATE_WIDTH-1:0]       ddrState, nxtDdrState;
   reg [3:0] 		       initCnt;
   
   reg 			       cke;   
   reg [ADDR_SIZE-1:0] 	       address;
   
   
   
   reg [3:0] 		       ddrCmd;
   
   assign 		       {cs_n, ras_n, cas_n, we_n} = ddrCmd;

   wire [3:0] 		       cmd = CmdIn[	3:0];
   wire [7:0] 		       cmdWait = CmdIn[11:4];
   wire [7:0] 		       cmdArg1 = CmdIn[19:12];
   wire [7:0] 		       cmdArg2 = CmdIn[27:20];
   
   wire 		       cmdValid = CmdIn[31];	
   
   reg [7:0] 		       cmdArg1_r;
   reg [7:0] 		       cmdArg2_r;
   reg [3:0] 		       cmd_r;
   reg [4:0] 		       cmdWait_r;
   
   
   integer 		       nxtCmdState;
   integer 		       cmdWaitCnt;
   integer 		       cmdCycleCnt;
   
   //   wire [7:0] 		       nextArg1 = nextCmd[11:4];

   integer 		       cmdState;


   parameter 		       CMD_STATE_IDLE = 0;
   parameter 		       CMD_STATE_WAIT = 1;
   parameter 		       CMD_STATE_NEXT = 2;

   initial begin
      popCmd = 0;
      nxtCmdState = CMD_STATE_NEXT;
   end

	       
   
   always @(posedge sysclk) begin
      cmdCycleCnt = cmdCycleCnt + 1;      

      case (cmdState)
	CMD_STATE_IDLE: begin
	   popCmd = 0;
	   
	   if(cmdValid) begin
	      cmd_r <= cmd;
	      cmdWait_r <= cmdWait;
	      cmdArg1_r <= cmdArg1;
	      cmdArg2_r <= cmdArg2;
	      
	      nxtCmdState = CMD_STATE_WAIT;
	      cmdWaitCnt = 0;
	      cmdCycleCnt = 0;
	   end
	end
	CMD_STATE_WAIT: begin
	   if (cmdWaitCnt >= cmdWait_r) begin
	      nxtCmdState = CMD_STATE_NEXT;	    
	   end
	   else begin
	      nxtCmdState = CMD_STATE_WAIT;	    
	   end
	   
	   cmdWaitCnt = cmdWaitCnt + 1;	  
	end
	CMD_STATE_NEXT: begin
	   popCmd = 1;
	   nxtCmdState = CMD_STATE_IDLE;	  
	end       
      endcase // case(cmdState)

      cmdState <= nxtCmdState;
      
   end
   
   
   reg 			       writeEnable, writeEnable_e1;
   always @(negedge sysclk) begin
   	writeEnable <= (cmd_r == `CMD_WRITE);
   	writeEnable_e1 <= writeEnable_e1;
   	end
   

   /* CKE */
   always @(*) begin
      if (cmd_r == `CMD_CKE_OFF) begin
	 cke = 'b0;
      end 
      else begin
	 cke = 'b1;
      end
   end

   /* CMD (CS_N, RAS_N, CAS_N, WE_N) */
   always @(*) begin
      case (cmd_r) 
	`CMD_CKE_OFF:    ddrCmd = `DDR_CMD_DESELECT;
	`CMD_CKE_ON:     ddrCmd = `DDR_CMD_NOP;	
	`CMD_DESEL:      ddrCmd = `DDR_CMD_DESELECT;
	`CMD_PRECHARGE:  ddrCmd = `DDR_CMD_PRECHARGE;
	`CMD_WRITE: begin
	   if(cmdCycleCnt == 0) begin
	      ddrCmd = `DDR_CMD_WRITE;
	   end
	   else begin
	      ddrCmd = `DDR_CMD_NOP;
	   end
	end	
	`CMD_ACTIVATE: begin
	   if(cmdCycleCnt == 0) begin
	      ddrCmd = `DDR_CMD_ACTIVE;
	   end
	   else begin
	      ddrCmd = `DDR_CMD_NOP;
	   end
	end	
	`CMD_READ: begin
	   if(cmdCycleCnt == 0) begin
	      ddrCmd = `DDR_CMD_READ;
	   end
	   else begin
	      ddrCmd = `DDR_CMD_NOP;
	   end
	end	
	`CMD_MRS: begin
	   if(cmdCycleCnt == 0) begin
	      ddrCmd = `DDR_CMD_LOAD;
	   end
	   else begin
	      ddrCmd = `DDR_CMD_NOP;
	   end
	end
	default: ddrCmd = `DDR_CMD_DESELECT;	
      endcase
   end
   
   /* BA */
   always @(*) begin
      case (cmd_r)
	`CMD_PRECHARGE:   bank = cmdArg1_r[1:0];
	`CMD_MRS:         bank = cmdArg2_r[7:6];
	default: bank = 0;
      endcase
   end

   /* ADDR */
   always @(*) begin
      case (cmd_r)
	`CMD_PRECHARGE: address = 'h400;
	`CMD_MRS: address = {cmdArg2_r[5:0], cmdArg1_r};	
	default: address = 0;
	
   endcase
   end
   
   /// ---------------------------
   reg [DATA_SIZE-1:0] dataHi, dataLo;
   always @(posedge dqs) begin
      dataHi <= data;
   end
   always @(negedge dqs) begin
      dataLo <= data;
   end
   
   assign rdData = dataLo | dataHi;

   assign dqs = writeEnable ? sysclk : 1'bz;
   assign data = writeEnable ? wrData : {DATA_SIZE{1'bz}};
   
endmodule // ddrcntrl
