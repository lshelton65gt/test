#include "cds_libddr_ddr_cmodel.h"
#include "DenaliCmodel.h"



#include <cstring>
#include <map>

PinMap gPinNameMap;

/* EDIT ME:
** Keep all of the pins of the memory.  This structure 
** Will need to be edited to match the pins of the particular 
** memory model.  There should be a structure here for every pin
*/
struct AllPins {
	PinData* clk;
	PinData* clkbar;
	PinData* cke;
	PinData* csbar;
	PinData* rasbar;
	PinData* casbar;
	PinData* webar;
	PinData* dqm;
	PinData* ba;
	PinData* address;
	PinData* data; // tristate, maps to 3 of the cmodel pins
	PinData* dqs;  // tristate, maps to 3 of the cmodel pins
};
/* End Edits */

void* cds_ddr_cmodel_create(int numParams, CModelParam* cmodelParams, 
	const char* inst)
{
	DenaliCmodelData* data = new DenaliCmodelData;
	data->vhmId = 0;
	
	char* somaFile;
	/*
	** Get SOMA file from paramteres
	*/
	for(int i = 0; i < numParams; ++i) {
		if(strcmp(cmodelParams[i].paramName, "memory_spec") == 0) {
			somaFile = new char[strlen(cmodelParams[i].paramValue)+1];
			strcpy(somaFile, cmodelParams[i].paramValue);
		}
	}
	 
	/*
	** Instantiate Denali DDR memory
	*/
   	DENALIinstanceHandleT denaliHdl = 
		DENALIinstantiate (somaFile, (char*)inst, 0, 0, DENALI_TYP);
	setCurrentDenaliInstance(data);	
	data->denaliHdl = denaliHdl;
	
	/*
	** Verify all the Memory PIns
	** DENALI requires this
	*/
	char *n;
	int num = DENALIgetNumPins(denaliHdl); 
	int size;
	DENALImodeT mode;
	   
	for(int i = 1; i <= num; ++i) {
		DENALIgetPinInfo(denaliHdl, i, &n, &mode, &size );
	}
	
	/*
	** Save pin pointers for faster reference
	*/
	AllPins* pins = new AllPins;
	data->pins = pins;

	/*
	** EDIT ME:  This section should map PinData data structures
	** to denali pin names.  This will need to be customized for
	** every different memory type
	*/
	pins->clk = gPinNameMap["clk"];
	pins->clkbar = gPinNameMap["clkbar"];
	pins->cke = gPinNameMap["cke"];
	pins->csbar = gPinNameMap["csbar"];
	pins->rasbar = gPinNameMap["rasbar"];
	pins->casbar = gPinNameMap["casbar"];
	pins->webar = gPinNameMap["webar"];
	pins->dqm = gPinNameMap["dqm"];
	pins->ba = gPinNameMap["ba"];
	pins->address = gPinNameMap["address"];
	pins->data = gPinNameMap["data"];
	pins->dqs = gPinNameMap["dqs"];

	/*
	** EDIT ME:
	** This section needs to be edited to make sure that all pins
	** get initialized.
	*/
	initPin(pins->clk, DENALI_Dz);
	initPin(pins->clkbar, DENALI_Dz);
	initPin(pins->cke, DENALI_Dz);
	initPin(pins->casbar, DENALI_Dz);
	initPin(pins->rasbar, DENALI_Dz);
	initPin(pins->csbar, DENALI_Dz);
	initPin(pins->webar, DENALI_Dz);
	initPin(pins->dqm, DENALI_Dz);
	initPin(pins->ba, DENALI_Dz);
	initPin(pins->address, DENALI_Dz);
	initPin(pins->data, DENALI_Dz);
	initPin(pins->dqs, DENALI_Dz);
	
	/* End Edits */
	DENALIeval( denaliHdl );	
	return reinterpret_cast<void*>(data);
}

void cds_ddr_cmodel_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
	if (reason == eCarbonCModelID) {
		assert(hndl);
		DenaliCmodelData* data = reinterpret_cast<DenaliCmodelData*>(hndl);
	  	data->vhmId = reinterpret_cast<CarbonObjectID*>(cmodelData);
	}
}

void cds_ddr_cmodel_run(void* hndl, CDSddr_cmodelContext context
		, const UInt32* clk // Input, size = 1 word(s)
		, const UInt32* clkbar // Input, size = 1 word(s)
		, const UInt32* cke // Input, size = 1 word(s)
		, const UInt32* csbar // Input, size = 1 word(s)
		, const UInt32* rasbar // Input, size = 1 word(s)
		, const UInt32* casbar // Input, size = 1 word(s)
		, const UInt32* webar // Input, size = 1 word(s)
		, const UInt32* dqm // Input, size = 1 word(s)
		, const UInt32* ba // Input, size = 1 word(s)
		, const UInt32* address // Input, size = 1 word(s)
		, const UInt32* data_in // Input, size = 1 word(s)
		, UInt32* data_out // Output, size = 1 word(s)
		, UInt32* data_en // Output, size = 1 word(s)
		, const UInt32* dqs_in // Input, size = 1 word(s)
		, UInt32* dqs_out // Output, size = 1 word(s)
		, UInt32* dqs_en // Output, size = 1 word(s)
	)
{	

	// It might be an optimization to cache old values and only report
	// event when a pin actually changes...
	DenaliCmodelData* d = reinterpret_cast<DenaliCmodelData*>(hndl);
	AllPins* pins = d->pins;
	CarbonTime ctime = carbonGetSimulationTime(d->vhmId);
	DENALItimeT dtime = CarbonTimeToDenaliTime(ctime);
	
	/* 
	** When clocks are in phase, or 180 degrees out of phase, we only
	** to tell the compiler about one of the clocks when we created 
	** the directives.  This reduces the number of times the cmodel is 
	** called.  However, the clks that weren't marked as such in the 
	** directives will have the old values.  So we'll deduce their value
	** from the primary clock
	*/
	UInt32 tmpClkBar = !(*clk);
	
	/* EDIT ME:
	** In this section all valid input parameters should be reported to 
	** the denali model using reportPinEvent().  Tristates that have
	** been split shoud report their input component
	*/
	
	reportPinEvent(pins->clk,      clk,      dtime);
	reportPinEvent(pins->clkbar,   &tmpClkBar,   dtime);
	reportPinEvent(pins->cke,      cke,      dtime);
	reportPinEvent(pins->csbar,    csbar,    dtime);
	reportPinEvent(pins->rasbar,   rasbar,   dtime);
	reportPinEvent(pins->casbar,   casbar,   dtime);
	reportPinEvent(pins->webar,    webar,   dtime);
	reportPinEvent(pins->dqm,      dqm,   dtime);
	reportPinEvent(pins->ba,       ba,   dtime);
	reportPinEvent(pins->address,  address,   dtime);
	reportPinEvent(pins->data,     data_in,   dtime);
	reportPinEvent(pins->dqs,      dqs_in,   dtime);
	
	/* End Edits */
	
	// call denali
	DENALIeval( d->denaliHdl );
	
	/*
	** EDIT ME:
	** 
	** All outputs and inouts should be check to see if denali
	** wrote them.  If it did use the DenaliFourStateToCarbon()
	** routine to update the associate parameters.
	**
	** inouts should pass both the output and enable portions to
	** the routine.  For output only (no enable component), you'll need
	** to create a dummy UInt32 to pass as the enable.
	*/
	if(pins->data->updateMe) {
		pins->data->updateMe = false;
		DenaliFourStateToCarbon(pins->data->value, data_out, data_en,
							   pins->data->busSize);
	}
	
	if(pins->dqs->updateMe) {
		pins->dqs->updateMe = false;
		DenaliFourStateToCarbon(pins->dqs->value, dqs_out, dqs_en,
							   pins->dqs->busSize);
	}
	
}	/* End Edits */

void cds_ddr_cmodel_destroy(void* hndl)
{
	for(PinMap::iterator it = gPinNameMap.begin();
		it != gPinNameMap.end(); ++it) {
		
		PinData* p = it->second;
		delete[] p->value;
		delete p;
	}
	
	delete (reinterpret_cast<DenaliCmodelData*>(hndl));
	DENALIterminate();
}
