#include <cstring>
#include <map>
#include "DenaliMemory.h"

#include "carbon/carbon_shelltypes.h"

//#include <systemc>
#include <iostream>
using namespace std;

class K7n803601m_20 : public DenaliMemory
{
public:
   K7n803601m_20(string instance, string somaFile, string loadFile ="load.mem")
   : DenaliMemory(instance, somaFile, loadFile) {}

   virtual void init() // need separate init routine.
   {
     DenaliMemory::init();

     a       = getPin("a");
     dq      = getPin("dq");
     dqp     = getPin("dqp");
     adv     = getPin("adv");
     clk     = getPin("clk");
     cs2     = getPin("cs2");
     cs1bar  = getPin("cs1bar");
     cs2bar  = getPin("cs2bar");
     ckebar  = getPin("ckebar");
     lbobar  = getPin("lbobar");
     oebar   = getPin("oebar");
     webar   = getPin("webar");
     bwbar   = getPin("bwbar");

     // Initialize inputs

     a->init(    DENALI_D0);
     dq->init(   DENALI_D0);
     dqp->init(  DENALI_D0);
     adv->init(  DENALI_D0);
     clk->init(  DENALI_D0);
     cs2->init(  DENALI_D0);

     cs1bar->init(  DENALI_D1);
     cs2bar->init(  DENALI_D1);
     ckebar->init(  DENALI_D1);
     lbobar->init(  DENALI_D1);
     oebar->init(   DENALI_D1);
     webar->init(   DENALI_D1);
     bwbar->init(   DENALI_D1);
  
     DENALIeval( getDenaliHdl() );  
   }

//protected:
    DenaliPin * a ;
    DenaliPin * dq;
    DenaliPin * dqp;
    DenaliPin * adv ;
    DenaliPin * clk ;
    DenaliPin * cs2 ;
    DenaliPin * cs1bar ;
    DenaliPin * cs2bar ;
    DenaliPin * bwbar ;
    DenaliPin * ckebar ;
    DenaliPin * lbobar ;
    DenaliPin * oebar ;
    DenaliPin * webar ;
};


typedef enum CDSk7n803601m_20_cmContext{
  eCDSk7n803601m_20_cmRiseclk,
} CDSk7n803601m_20_cmContext;

extern __C__ void*
cds_k7n803601m_20_cm_create(
  int numParams, CModelParam* cmodelParams, const char* inst);

extern __C__ void
cds_k7n803601m_20_cm_misc(
  void* hndl, CarbonCModelReason reason, void* cmodelData);

extern __C__ void cds_k7n803601m_20_cm_destroy(void* hndl);

extern __C__ void
cds_k7n803601m_20_cm_run(
      void* hndl, CDSk7n803601m_20_cmContext context
    , const CarbonUInt32* a // Input, size = 1 word(s)
    , const CarbonUInt32* dq_i // Input, size = 1 word(s)
    , CarbonUInt32* dq_o // Output, size = 1 word(s)
		, CarbonUInt32* dq_en // Output, size = 1 word(s)
    , const CarbonUInt32* dqp_i // Input, size = 1 word(s)
    , CarbonUInt32* dqp_o // Output, size = 1 word(s)
		, CarbonUInt32* dqp_en // Output, size = 1 word(s)
    , const CarbonUInt32* bwbar // Input, size = 1 word(s)
    , const CarbonUInt32* webar // Input, size = 1 word(s)
    , const CarbonUInt32* cs1bar // Input, size = 1 word(s)
    , const CarbonUInt32* cs2bar // Input, size = 1 word(s)
    , const CarbonUInt32* cs2 // Input, size = 1 word(s)
    , const CarbonUInt32* clk // Input, size = 1 word(s)
    , const CarbonUInt32* ckebar // Input, size = 1 word(s)
    , const CarbonUInt32* oebar // Input, size = 1 word(s)
    , const CarbonUInt32* adv // Input, size = 1 word(s)
    , const CarbonUInt32* lbobar // Input, size = 1 word(s)
  );

// ============================================================

void*
cds_k7n803601m_20_cm_create(
  int numParams, CModelParam* cmodelParams, const char* inst)
{
  string somaFile; // Get SOMA file from paramteres

  for(int i = 0; i < numParams; ++i) 
    if(strcmp(cmodelParams[i].paramName, "memory_spec") == 0) 
      somaFile = cmodelParams[i].paramValue;

  K7n803601m_20* memory = new K7n803601m_20(inst, somaFile);
   
  memory->init(); // Constructs Denali Pins.

  return reinterpret_cast<void*>(memory);
}

void
cds_k7n803601m_20_cm_misc(
  void* hndl, CarbonCModelReason reason, void* cmodelData)
{
  if (reason == eCarbonCModelID) {
    assert(hndl);
    K7n803601m_20* memory = reinterpret_cast<K7n803601m_20*>(hndl);
    memory->setCarbonObj( reinterpret_cast<CarbonObjectID*>(cmodelData) );
  }
}

void cds_k7n803601m_20_cm_destroy(void* hndl)
{
  delete(reinterpret_cast<K7n803601m_20*>(hndl));
}

void
cds_k7n803601m_20_cm_run(
      void* hndl, CDSk7n803601m_20_cmContext context
    , const CarbonUInt32* a      // Input,  size = 1 word(s)
    , const CarbonUInt32* dq_i   // Input,  size = 1 word(s)
    ,       CarbonUInt32* dq_o   // Output, size = 1 word(s)
    ,       CarbonUInt32* dq_en  // Output, size = 1 word(s)
    , const CarbonUInt32* dqp_i  // Input,  size = 1 word(s)
    ,       CarbonUInt32* dqp_o  // Output, size = 1 word(s)
    ,       CarbonUInt32* dqp_en // Output, size = 1 word(s)
    , const CarbonUInt32* bwbar  // Input,  size = 1 word(s)
    , const CarbonUInt32* webar  // Input,  size = 1 word(s)
    , const CarbonUInt32* cs1bar // Input,  size = 1 word(s)
    , const CarbonUInt32* cs2bar // Input,  size = 1 word(s)
    , const CarbonUInt32* cs2    // Input,  size = 1 word(s)
    , const CarbonUInt32* clk    // Input,  size = 1 word(s)
    , const CarbonUInt32* ckebar // Input,  size = 1 word(s)
    , const CarbonUInt32* oebar  // Input,  size = 1 word(s)
    , const CarbonUInt32* adv    // Input,  size = 1 word(s)
    , const CarbonUInt32* lbobar // Input,  size = 1 word(s)
  )
{
  K7n803601m_20 * mem = reinterpret_cast<K7n803601m_20*>(hndl);
  CarbonTime   ctime = carbonGetSimulationTime(mem->getVHM());
  DENALItimeT  dtime = CarbonTimeToDenaliTime(ctime);
  
	setCurrentDenaliInstance(mem);	

  // copy carbon inputs to Denali model

  mem->a->input(        a,         dtime);
  mem->dq->input(       dq_i,      dtime);
  mem->dqp->input(      dqp_i,     dtime);
  mem->adv->input(      adv,       dtime);
  mem->clk->input(      clk,       dtime);
  mem->cs2->input(      cs2,       dtime);
  mem->cs1bar->input(   cs1bar,    dtime);
  mem->cs2bar->input(   cs2bar,    dtime);
  mem->ckebar->input(   ckebar,    dtime);
  mem->lbobar->input(   lbobar,    dtime);
  mem->oebar->input(    oebar,     dtime);
  mem->webar->input(    webar,     dtime);
  mem->bwbar->input(    bwbar,     dtime);
  
  DENALIeval( mem->getDenaliHdl() );
  
  // drive carbon outputs from Denali model

  if(dq_o)  mem->dq->output(dq_o);
  if(dqp_o) mem->dqp->output(dqp_o);
  if(dq_en)  mem->dq->enable(dq_en);
  if(dqp_en) mem->dqp->enable(dqp_en);
}

