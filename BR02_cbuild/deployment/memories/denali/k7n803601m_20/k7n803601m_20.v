// Module:      k7n803601m_20
// SOMA file:   k7n803601m_20.soma

module k7n803601m_20(
    a,
    dq,
    dqp,
    bwbar,
    webar,
    cs1bar,
    cs2bar,
    cs2,
    clk,
    ckebar,
    oebar,
    adv,
    lbobar
);
    parameter memory_spec = "../models/k7n803601m_20/k7n803601m_20.soma";
//  parameter memory_spec = "k7n803601m_20.soma";

    input [17:0] a; 
    inout [31:0] dq; 
    inout  [3:0] dqp; 
    input  [3:0] bwbar; 
    input webar; 
    input cs1bar; 
    input cs2bar; 
    input cs2; 
    input clk; 
    input ckebar; 
    input oebar; 
    input adv; 
    input lbobar; 

    reg [2:0] pipe, next; // read pipeline -- read cmd --> drive out @ clk+2
    reg       oe ; 

    wire clkb  = !clk;
    wire read  =  webar && !adv && !ckebar && !cs1bar && !cs2bar && cs2; 
    wire write = !webar && !adv && !ckebar && !cs1bar && !cs2bar && cs2; 

    wire [31:0]   dq_i= dq;
    wire [31:0]   dq_o;
    reg  [31:0] r_dq_o1;
    wire  [3:0]   dqp_i= dqp;
    wire  [3:0]   dqp_o;
    reg   [3:0] r_dqp_o1;

    // oebar active at current cycle
    assign dq  = oe ? r_dq_o1  : 32'bz ;
    assign dqp = oe ? r_dqp_o1 :  4'bz ;

    always @ (pipe or oebar or read)
    begin
      oe = pipe[1] && !oebar; 
      case (pipe) // state machine -- shift & load
        3'b000: if (read) next = 3'b100; else next = 3'b000;
        3'b001: if (read) next = 3'b100; else next = 3'b000;
        3'b010: if (read) next = 3'b101; else next = 3'b001;
        3'b011: if (read) next = 3'b101; else next = 3'b001;
        3'b100: if (read) next = 3'b110; else next = 3'b010;
        3'b101: if (read) next = 3'b110; else next = 3'b010;
        3'b110: if (read) next = 3'b111; else next = 3'b011;
        3'b111: if (read) next = 3'b111; else next = 3'b011;
      endcase
    end

    // need to latch memory outputs -- delay 1 cycle

    always @(posedge clk)
    begin
      pipe <= next; // state machine
      r_dq_o1  <= dq_o;
      r_dqp_o1 <= dqp_o;
    end

    initial pipe = 3'b000;
/***
    wire [31:0]   dq_i= dq;
    wire [31:0]   dq_o;
    wire  [3:0]   dqp_i= dqp;
    wire  [3:0]   dqp_o;
    wire          dq_en, dqp_en;

    assign dq  = dq_en  ? dq_o  : 32'bz;
    assign dqp = dqp_en ? dqp_o :  4'bz;

    // Quick Fix : iVivity

    wire          clkb = !clk;
***/ 
  k7n803601m_20_cm #( memory_spec ) cmodel (
      .a            ( a            )
    , .dq_i         ( dq_i         )
    , .dq_o         ( dq_o         )
    , .dq_en        ( dq_en        )
    , .dqp_i        ( dqp_i        )
    , .dqp_o        ( dqp_o        )
    , .dqp_en       ( dqp_en       )
    , .bwbar        ( bwbar        )
    , .webar        ( webar        )
    , .cs1bar       ( cs1bar       )
    , .cs2bar       ( cs2bar       )
    , .cs2          ( cs2          )
    , .clk          ( clkb         )
    , .ckebar       ( ckebar       )
    , .oebar        ( oebar        )
    , .adv          ( adv          )
    , .lbobar       ( lbobar       )
  );

endmodule


module k7n803601m_20_cm(
    a
  , dq_i
  , dq_o
  , dq_en
  , dqp_i
  , dqp_o
  , dqp_en
  , bwbar
  , webar
  , cs1bar
  , cs2bar
  , cs2
  , clk
  , ckebar
  , oebar
  , adv
  , lbobar
);

    parameter memory_spec = "k7n803601m_20.soma";

    input [17:0] a;
    input  [31:0] dq_i;
    output [31:0] dq_o;
    input  [3:0] dqp_i;
    output [3:0] dqp_o;
    input  [3:0] bwbar;
    output dq_en, dqp_en;
    input webar;
    input cs1bar;
    input cs2bar;
    input cs2;
    input clk;
    input ckebar;
    input oebar;
    input adv;
    input lbobar;

endmodule

