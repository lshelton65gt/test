// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// $Source: /cvs/repository/deployment/memories/denali/28f128j3a_150/testbench.h,v $
// $Id: testbench.h,v 1.2 2007/03/28 17:57:19 ronk Exp $

#ifndef __Testbench_h__
#define __Testbench_h__

#include "systemc.h"
#include "libintel_28f128j3a_150.systemc.h"

#include "DenaliMemory.h"

SC_MODULE(Testbench)
{
  sc_clock clk;

  // Signals

  sc_signal<sc_uint<24> >   a;  
  sc_signal<sc_lv<16> >     dq;  
  sc_signal<bool>           ce0;  
  sc_signal<bool>           ce1;  
  sc_signal<bool>           ce2;  
  sc_signal<bool>           oebar;  
  sc_signal<bool>           webar;  
  sc_signal<bool>           sts;  
  sc_signal<bool>           rpbar;  
  sc_signal<bool>           bytebar;  

  // Constructor
  Testbench(sc_module_name name);

  SC_HAS_PROCESS(Testbench);
  
  // Destructor
  ~Testbench();

  // VHM Object
  intel_28f128j3a_150 *vhm;

  // Processes
  void configure(void);
  void watchdog(void);

private:
  sc_event configDone;

};

#endif
  

