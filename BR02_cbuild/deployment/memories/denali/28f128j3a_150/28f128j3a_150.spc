-- ======================================================================
-- Copyright 1999-2003 by Denali Software, Inc.  All rights reserved.
-- ======================================================================
-- 
-- This SOMA file describes a memory model, using Denali Software's
-- proprietary SOMA language.  By using this SOMA file, you agree to the
-- following terms.  If you do not agree to these terms, you may not use
-- this SOMA file.
-- 
-- Subject to the restrictions set forth below, Denali Software grants
-- you a non-exclusive, non-transferable license only to use this SOMA
-- file to simulate the memory described in it using tools supplied by
-- Denali Software.
-- 
-- You may not:
-- 
--   (1)  Use this SOMA file to create software programs or tools that use
--        SOMA files as either input or output.
-- 
--   (2)  Modify this SOMA file or the SOMA language in any manner.
-- 
--   (3)  Use this SOMA file to create other languages for describing
--        memory models.
-- 
--   (4)  Distribute this SOMA file to others.
-- 
-- This SOMA file is based on information received by Denali Software
-- from third parties.  DENALI SOFTWARE PROVIDES THIS SOMA FILE "AS IS"
-- AND EXPRESSLY DISCLAIMS ALL REPRESENTATIONS, WARRANTIES AND
-- CONDITIONS, INCLUDING BUT NOT LIMITED TO WARRANTIES AND CONDITIONS OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND
-- NONINFRINGEMENT.  DENALI SOFTWARE'S AGGREGATE LIABILITY ARISING FROM
-- YOUR USE OF THIS SOMA FILE IS LIMITED TO ONE U.S. DOLLAR.
-- 
-- If you have any questions or if you would like to inquire about
-- obtaining additional or different rights in SOMA files or the SOMA
-- language, please contact Denali Software, at www.denali.com or at
-- info@denalisoft.com.
-- 
--********************************************************************************
-- Manufacturer    : INTEL
-- Memory Class    : FLASH
-- Part Name       : 28f128j3a-150
-- Revision        : Mon Mar 10 12:39:46 2003
-- Description   : 3 Volt Strata Flash Memory
-- Datasheet URL : ftp://download.intel.com/design/flcomp/datashts/29066713.pdf
-- Datasheet Info: February 2003
-- Author Name   : Denali Software SS, KK
-- Author Comment: SOMA updated to reflect memory vendor datasheet update
--********************************************************************************
Version 0.001
flash_intel

Manufacturer intel
Part "28F128J3A-150"
Sizes
manufacturerCode 0x89
deviceCode 0x18
sectorSizes "128x128kb"
bankCount 1
bankStarts ""
pageWords 4
burstStartMSB 13
burstStartLSB 11
burstStartLatencyMap "2=>2,3=>3,4=>4,5=>5,6=>6"
burstLengthMSB 2
burstLengthMap "1=>4,2=>8,7=>-1"
readCfgRgFirstDataInput 0x60
synchReadSectors ""
pageWriteBuffers 1
pageWriteWords 16
pageWriteMinWords 1
protectedSectors ""
cfiVendorSpecific "0x89"
cfiVendorID 0x01
cfiExtTableAddr 0x31
cfiExtQueryMajorV 0x31
cfiExtQueryMinorV 0x31
cfiExtQueryData "0xCE000000010100330001800003030300"
cfiAltVendorID 0
cfiAltExtTableAddr 0
cfiAltExtQueryMajorV 0
cfiAltExtQueryMinorV 0
cfiAltExtQueryData "0"
cfiVccMin 0x27
cfiVccMax 0x36
cfiVppMin 0
cfiVppMax 0
cfiTypTimeoutWrite 0x07
cfiTypTimeoutMaxWrite 0x07
cfiTypTimeoutSectorErase 0x0A
cfiTypTimeoutChipErase 0
cfiMaxTimeoutWrite 0x04
cfiMaxTimeoutMaxWrite 0x04
cfiMaxTimeoutSectorErase 0x04
cfiMaxTimeoutChipErase 0
cfiDeviceSize 0x18
cfiDeviceInterfaceID 0x02
cfiMaxBytesWrite 0x05
otpFactoryBaseAddress 0x81
otpFactorySize 4
otpUserBaseAddress 0x85
otpUserSize 4
otpLockByteAddress 0x80
otpByteSelectBit 0
otpAddrMSB 0
otpAddrLSB 0
paramBlockCount 0
otpNumberOfAdditPrlockRegisters 1
otpAdditLockByteAddresses ""
otpAdditRegCount ""
otpAdditRegSizes ""
otpAdditRegStartingAddresses ""
muxAddrSize 0
defaultCfgValue 0x0000
defaultECfgValue 0xffff
programCodeWordSize 16
codeWordsPerRegion 32
otpNumberOfBlockROMBits 5
blockLockConfigRegAddr 0x15a
extMemorySize 0
extMemorySectorSize 0

Features
configureSTS 1
bootSectored 0
topBoot 0
allowSimultaneousWrite 0
pageModeReads 1
burstModeReads 0
readConfigureCmd 0
delayAtPageBoundary 0
linearBurstOrderActiveLow 0
synchReadDeviceID 0
synchReadStatus 0
burstCrossBank 1
addressLatchPin 0
readyBusyPin 1
resetPin 1
powerSavePin 0
powerSaveCmd 0
selectByteWord 1
a0IsByteSelect 1
canAddSectors 0
singleWrites 1
pageModeWrites 1
noChipErase 1
eraseSuspendResume 1
eraseSuspendProgram 1
programSuspendResume 1
sectorProtection 1
writeProtectPin 0
writeProtectCmds 1
protectOnly 0
protectAllAvail 0
protectBurst 0
hasCfi 1
oneTimeProtect 1
otpFactoryArea 1
otpUserArea 1
otpUserPrevProtected 0
otpWithCmd 0
otpAddrBusRange 0
cfiSystemInterface 0
cfiDeviceGeometry 0
programControlVoltagePin 0
setPermanentLockBitComm 0
additionalChipEnablePins 1
setLockDownCommand 0
dataWaitPin 0
otpDeviceHasMultUserRegisters 0
enhancedFactoryProgramming 0
onlyDriveSR7IfBusy 1
hasExtendedStatusReg 1
muxDataAddr 0
enableRDWAITFunc 0
enableRDOEWTFunc 0
enableRDNAWTFunc 0
enhancedConfigReg 0
supportsSleepWake 0
cfgBit9ControlsDataHold 1
enableRDFLOWFunc 0
driveSR7AndSR0IfBusy 0
enableRDOETRIWTFunc 0
setUnLockDownCommand 0
lockCfgBit15 0
zeroOtpArea 0
cfgBit7SetsInterleavedBurst 0
cfgBit6SetsActiveClkEdge 1
cfgBit3SetsBurstWrap 1
onlyClkLatchesBurstAddr 0
defWaitPolarityLow 0
bit13SetsPageModeReadSize 0
deepPowerDownPin 0
ignoreConfirmAddr 0
supportsProgrammingModes 0
maintainReadMode 0
otpSupportsBlockROM 0
otpSuppBlockLockConfigReg 0
hasExtMemoryArea 0
enableRDHiAddrMuxFunc 0
supportsBlankCheck 0

Pins
address a 24
data dq 16
cebar ce0 1
oebar oebar 1
webar webar 1
wpbar wpbar 1
rybybar sts 1
reset rpbar 1
bytebar bytebar 1
clk clk 1
advbar advbar 1
waitbar waitbar 1
ps ps 1
cebar1 ce1 1
cebar2 ce2 1
vpp vpp 1
advbarMux advbarMux 1
dpd dpd 1

Timing
trc 150 ns
tprc 25 ns
taa 150 ns
tpacc 25 ns
tce 150 ns
toe 25 ns
tehqz 35 ns
tghqz 15 ns
tiacc disabled
tbacc disabled
tkhkh disabled
tkhkl disabled
tklkh disabled
tkhqx disabled
tavkh disabled
tkhax disabled
telkh disabled
tavlh disabled
tlhax disabled
tellh disabled
tllqv disabled
tlllh disabled
tlhll disabled
tllkh disabled
tkhlh disabled
tkhtl disabled
twc disabled
tas 55 ns
tah 0 ns
tds 50 ns
tdh 0 ns
toes disabled
toeh disabled
toehp disabled
tghwl disabled
tghll disabled
twhgl 35 ns
tlleh disabled
tcs 0 ns
tch 0 ns
twp 70 ns
twph 30 ns
tcp 70 ns
tcph 30 ns
tws 0 ns
twh 0 ns
tpxwh disabled
tqvpx disabled
twhwh1 210 us
twhwh1b 210 us
tpwhwh1 218 us
tpwhwh1b 218 us
twhwh2 1.0 s
twhwh3 disabled
teoe disabled
tseto disabled
tsusp 26 us
tpsusp 25 us
trl 35 us
tready 35.1 us
treadyr 200 ns
trh disabled
telfl 10 ns
telfh 10 ns
tflqz 1000 ns
tfhqv 1000 ns
tspw 250 ns
twhwh2b disabled
twhwh4 64 us
twhwh5 0.5 s
trp 35 us
teltl 1 s
tehtz 55 ns
tbusy 500 ns
toh 0 ns
tghel disabled
telqx 0 ns
tglqx 0 ns
tefp_w 1 s
tefp_setup 1 s
tefp_tran 1 s
tefp_verify 1 s
twhvh disabled
twhcv disabled
tphwl 1 us
twakeup 0 ns
tghtv 1 s
tgltv 1 s
twhqv disabled
tbefp_w 1 s
tgvch 0 ns
tphqv 210 ns
tghtz 1 s
tvhgl 0 ns
tvpwh 0 ns
tqvvl 0 ns
teltx disabled
tgltx disabled
tphvh disabled
tmhvl disabled
tvhwl_chwl disabled
tchtx disabled
tbcpb 1 s
tbcmb 1 s
tslsh 0 ns
tehsl 0 ns
tshel 0 ns
tphel 0 ns
tvlbh disabled
tbhax disabled
tchgl 0 ns

