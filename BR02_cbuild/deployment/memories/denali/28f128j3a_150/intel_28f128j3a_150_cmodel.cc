#include <cstring>
#include <map>
#include "DenaliMemory.h"

#include "carbon/carbon_shelltypes.h"

//#include <systemc>
#include <iostream>
using namespace std;

class Intel_28f128j3a_150 : public DenaliMemory
{
public:
   Intel_28f128j3a_150(string instance, string somaFile, string loadFile)
   : DenaliMemory(instance, somaFile, loadFile) {}

   virtual void init() // need separate init routine.
   {
     DenaliMemory::init();

     a       = getPin("a");
     dq      = getPin("dq");
     oebar   = getPin("oebar");
     webar   = getPin("webar");
     sts     = getPin("sts");
     rpbar   = getPin("rpbar");
     bytebar = getPin("bytebar");
     ce0     = getPin("ce0");
     ce1     = getPin("ce1");
     ce2     = getPin("ce2");

     // Initialize inputs

     a->init(       DENALI_D0);
     dq->init(      DENALI_D0);
     oebar->init(   DENALI_D1);
     webar->init(   DENALI_D1);
     sts->init(     DENALI_D0);
     rpbar->init(   DENALI_D1);
     bytebar->init( DENALI_D1);
     ce0->init(     DENALI_D1);
     ce1->init(     DENALI_D1);
     ce2->init(     DENALI_D1);
  
     DENALIeval( getDenaliHdl() );  
   }

//protected:
    DenaliPin * a ;
    DenaliPin * dq;
    DenaliPin * oebar ;
    DenaliPin * webar ;
    DenaliPin * sts ;
    DenaliPin * rpbar ;
    DenaliPin * bytebar ;
    DenaliPin * ce0 ;
    DenaliPin * ce1 ;
    DenaliPin * ce2 ;
};


typedef enum CDSintel_28f128j3a_150_cmContext{
  eCDSintel_28f128j3a_150_cmAsync,
} CDSintel_28f128j3a_150_cmContext;

extern __C__ void*
cds_intel_28f128j3a_150_cm_create(
    int numParams, CModelParam* cmodelParams, const char* inst);

extern __C__ void
cds_intel_28f128j3a_150_cm_misc(
    void* hndl, CarbonCModelReason reason, void* cmodelData);

extern __C__ void cds_intel_28f128j3a_150_cm_destroy(void* hndl);

extern __C__ void
cds_intel_28f128j3a_150_cm_run(
      void* hndl
    , CDSintel_28f128j3a_150_cmContext context
    , const CarbonUInt32* a // Input, size = 1 word(s)
    , const CarbonUInt32* dq_i // Input, size = 1 word(s)
    , CarbonUInt32* dq_o // Output, size = 1 word(s)
    , const CarbonUInt32* ce0 // Input, size = 1 word(s)
    , const CarbonUInt32* oebar // Input, size = 1 word(s)
    , const CarbonUInt32* webar // Input, size = 1 word(s)
    , CarbonUInt32* sts // Output, size = 1 word(s)
    , const CarbonUInt32* rpbar // Input, size = 1 word(s)
    , const CarbonUInt32* bytebar // Input, size = 1 word(s)
    , const CarbonUInt32* ce1 // Input, size = 1 word(s)
    , const CarbonUInt32* ce2 // Input, size = 1 word(s)
  );

// ============================================================


void* cds_intel_28f128j3a_150_cm_create(
  int numParams, CModelParam* cmodelParams, const char* inst )
{
  string somaFile; // Get SOMA file from parameters

  for(int i = 0; i < numParams; ++i) 
    if(strcmp(cmodelParams[i].paramName, "memory_spec") == 0) 
      somaFile = cmodelParams[i].paramValue;

  Intel_28f128j3a_150* memory
    = new Intel_28f128j3a_150(inst, somaFile, "flash.mem");
   
  memory->init(); // Constructs Denali Pins.

  return reinterpret_cast<void*>(memory);
}

void cds_intel_28f128j3a_150_cm_misc(
    void* hndl, CarbonCModelReason reason, void* cmodelData)
{
  if (reason == eCarbonCModelID) {
    assert(hndl);
    Intel_28f128j3a_150* memory = reinterpret_cast<Intel_28f128j3a_150*>(hndl);
    memory->setCarbonObj( reinterpret_cast<CarbonObjectID*>(cmodelData) );
  }
}

void
cds_intel_28f128j3a_150_cm_run(
      void* hndl
    , CDSintel_28f128j3a_150_cmContext context
    , const CarbonUInt32* a // Input, size = 1 word(s)
    , const CarbonUInt32* dq_i // Input, size = 1 word(s)
    , CarbonUInt32* dq_o // Output, size = 1 word(s)
    , const CarbonUInt32* ce0 // Input, size = 1 word(s)
    , const CarbonUInt32* oebar // Input, size = 1 word(s)
    , const CarbonUInt32* webar // Input, size = 1 word(s)
    , CarbonUInt32* sts // Output, size = 1 word(s)
    , const CarbonUInt32* rpbar // Input, size = 1 word(s)
    , const CarbonUInt32* bytebar // Input, size = 1 word(s)
    , const CarbonUInt32* ce1 // Input, size = 1 word(s)
    , const CarbonUInt32* ce2 // Input, size = 1 word(s)
)
{
  Intel_28f128j3a_150 * mem = reinterpret_cast<Intel_28f128j3a_150*>(hndl);
  CarbonTime   ctime = carbonGetSimulationTime(mem->getVHM());
  DENALItimeT  dtime = CarbonTimeToDenaliTime(ctime);
  
	setCurrentDenaliInstance(mem);	

  // copy carbon inputs to Denali model

  mem->a->input(        a,         dtime);
  mem->dq->input(       dq_i,      dtime);
  mem->ce0->input(      ce0,       dtime);
  mem->ce1->input(      ce1,       dtime);
  mem->ce2->input(      ce2,       dtime);
  mem->webar->input(    webar,     dtime);
  mem->oebar->input(    oebar,     dtime);
  mem->rpbar->input(    rpbar,     dtime);
  
  DENALIeval( mem->getDenaliHdl() );
  
  // drive carbon outputs from Denali model

  if(sts)  mem->sts->output(sts);
  if(dq_o) mem->dq->output(dq_o);
}  

void cds_intel_28f128j3a_150_cm_destroy(void* hndl)
{
  delete(reinterpret_cast<Intel_28f128j3a_150*>(hndl));
}
