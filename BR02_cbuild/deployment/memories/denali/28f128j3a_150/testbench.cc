/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// $Source: /cvs/repository/deployment/memories/denali/28f128j3a_150/testbench.cc,v $
// $Id: testbench.cc,v 1.2 2007/03/28 17:57:19 ronk Exp $

#include <cstdio>
#include <cstring>
#include <cerrno>
#include <string>

#include <unistd.h>
#include <fcntl.h>
#include <strings.h>

#include "testbench.h"

using namespace std;


Testbench::Testbench(sc_module_name name)
: sc_module(name)
, clk("clk", 10, SC_US) // 100 KHz = 10 uS
, a("a")  
, dq("dq")  
, ce0("ce0")  
, ce1("ce1")  
, ce2("ce2")  
, oebar("oebar")  
, webar("webar")  
, sts("sts")  
, rpbar("rpbar")  
, bytebar("bytebar")  
{

  // Create VHM
  vhm = new intel_28f128j3a_150("intel_28f128j3a_150");
  assert(vhm);

//vhm->carbonSCWaveInitFSDB();
//vhm->carbonSCDumpVars();
  
  // Connect Signals to VHM

  vhm->a       ( a       );  
  vhm->dq      ( dq      );  
  vhm->oebar   ( oebar   );  
  vhm->webar   ( webar   );  
  vhm->sts     ( sts     );  
  vhm->rpbar   ( rpbar   );  
  vhm->bytebar ( bytebar );  
  vhm->ce0     ( ce0     );  
  vhm->ce1     ( ce1     );  
  vhm->ce2     ( ce2     );  

  // Configure Process (e.g. reset chip)
  SC_THREAD(configure);

  SC_THREAD(watchdog); // watchdog timer

};

// Destructor
Testbench::~Testbench()
{
  cout << "Destructing Testbench." << endl;

  vhm->carbonSCDumpFlush();

  delete vhm;
}

// sc_thread -- watchdog timer

void Testbench::watchdog()
{
  wait( clk.period() * 200 );

  rpbar   = false  ; wait( clk.period() ); 
  rpbar   = true   ; wait( clk.period() ); 
  rpbar   = false  ; wait( clk.period() ); 
  rpbar   = true   ; wait( clk.period() ); 

  cout << "WatchDog Timer Expired: " << sc_time_stamp() << endl;

  sc_stop();
}

struct FData { // Flash Data
  uint         a;  
  const char * dq;  
  bool         oebar;  
  bool         webar;  
  bool         rpbar;  
  bool         ce0;  
  bool         ce1;  
  bool         ce2;  
  bool         bytebar;  

  FData(
    uint         _a,  
    const char * _dq,  
    bool         _oebar,  
    bool         _webar,  
    bool         _rpbar,  
    bool         _ce0,  
    bool         _ce1,  
    bool         _ce2,
    bool         _bytebar  
  )
  : a(_a), dq(_dq), oebar(_oebar), webar(_webar), rpbar(_rpbar)  
  , ce0(_ce0), ce1(_ce1), ce2(_ce2), bytebar(_bytebar)
  {}

} ;

static const FData d[] = {
       //     a,       dq, oe, we, rp, ce0, ce1, ce2, byte
  FData( 0x3210,      "Z",  0,  1,  1,   0,   0,   0,    1 ),
  FData( 0x7654,      "Z",  0,  1,  1,   0,   0,   0,    1 ),
  FData( 0xba98,      "Z",  0,  1,  1,   0,   0,   0,    1 ),
  FData( 0xfedc,      "Z",  0,  1,  1,   0,   0,   0,    1 ),

  FData( 0x3210, "0x0040",  1,  0,  1,   0,   0,   0,    1 ), // WR CMD
  FData( 0x3210, "0x0213",  1,  0,  1,   0,   0,   0,    1 ),
  FData( 0x3210, "0x0070",  1,  0,  1,   0,   0,   0,    1 ), // RD Status?
  FData( 0x3210, "0x0213",  0,  1,  1,   0,   0,   0,    1 ), // RD Status?
  FData( 0x7654, "0x0040",  1,  0,  1,   0,   0,   0,    1 ),
  FData( 0x7654, "0x4657",  1,  0,  1,   0,   0,   0,    1 ),
  FData( 0xba98, "0x0040",  1,  0,  1,   0,   0,   0,    1 ),
  FData( 0xba98, "0x8a9b",  1,  0,  1,   0,   0,   0,    1 ),
  FData( 0xfedc, "0x0040",  1,  0,  1,   0,   0,   0,    1 ),
  FData( 0xfedc, "0xcedf",  1,  0,  1,   0,   0,   0,    1 ),

  FData( 0x3210,      "Z",  0,  1,  1,   0,   0,   0,    1 ),
  FData( 0x7654,      "Z",  0,  1,  1,   0,   0,   0,    1 ),
  FData( 0xba98,      "Z",  0,  1,  1,   0,   0,   0,    1 ),
  FData( 0xfedc,      "Z",  0,  1,  1,   0,   0,   0,    1 ),

};

static const int d_size= sizeof(d) / sizeof(d[0]) ;

void Testbench::configure()
{
  CarbonStatus status;

  cout << "Reset chip." << endl;

  ce0     = true ;
  ce1     = true ;
  ce2     = true ;
  bytebar = true ;
  oebar   = true ;
  webar   = true ;

  rpbar.write(true);  wait( clk.period() * 2 ); // release reset
  rpbar.write(false); wait( clk.period() * 5 ); // assert  reset
  rpbar.write(true);  wait( clk.period() * 5 ); // release reset

  DenaliMemory * flashMem= getCurrentDenaliInstance();

//DENALIloadContent ( flashMem->denaliHdl, "flash.mem" ) ;
//DENALIsaveContent ( flashMem->denaliHdl, "flash.out" ) ;

  wait( clk.period() );

  configDone.notify(SC_ZERO_TIME);

  for(int n=0; n < d_size; n++) {
    a       = d[n].a;
    dq      = d[n].dq;
    bytebar = d[n].bytebar ;
    oebar   = d[n].oebar   ;
    webar   = d[n].webar   ;
    rpbar   = d[n].rpbar   ;
    ce0     = d[n].ce0     ;
    ce1     = d[n].ce1     ;
    ce2     = d[n].ce2     ;

    wait( clk.period() ); 

    webar   = true   ;
//  oebar   = true   ;

    wait( clk.period() ); 
  }
}

int sc_main (int argc , char *argv[]) 
{
  bool traceOn = true;

  CarbonStatus status= carbonSetFilePath(".:../vhm");

  assert(status == eCarbon_OK);

  Testbench * tb = new Testbench("Testbench");

  // Setup the Test
  cout << "--------------------" << endl;
  cout << "--- Running test ---" << endl;
  cout << "--------------------" << endl;
  
  // Run
  sc_start();
  
  delete tb;

  return 0;
}
