// Intel StrataFlash® Memory (J3) 28F128J3 (x8/x16)

module intel_28f128j3a_150 (
    a
  , dq
  , ce0
  , oebar
  , webar
  , sts
  , rpbar
  , bytebar
  , ce1
  , ce2
);

//  parameter memory_spec = "28f128j3a_150.soma";
    parameter memory_spec = "../models/28f128j3a_150/28f128j3a_150.soma";

     input [23:0] a;       // addr
     inout [15:0] dq;      // data
     input        ce0;     // chip enable
     input        oebar;   // output enable
     input        webar;   // write enable
    output        sts;     // status
     input        rpbar;   // reset/power down
     input        bytebar; // byte mode: 0=x8, 1=x16
     input        ce1;     // chip enable
     input        ce2;     // chip enable

    wire [15:0] dq, d, q;
    reg  [15:0] q_r, d_r;

    always @(oebar) if(!oebar) q_r = q; else d_r= dq;

    assign dq= oebar ? 16'bz : q_r;

    intel_28f128j3a_150_cm #( memory_spec ) cmodel (
        .a       ( a       )
//    , .dq_i    ( dq      )
//    , .dq_o    ( q       )
      , .dq_i    ( d_r     )
      , .dq_o    ( q       )
      , .ce0     ( ce0     )
      , .oebar   ( oebar   )
      , .webar   ( webar   )
      , .sts     ( sts     )
      , .rpbar   ( rpbar   )
      , .bytebar ( bytebar )
      , .ce1     ( ce1     )
      , .ce2     ( ce2     )
    );

endmodule

module intel_28f128j3a_150_cm (
    a
  , dq_i
  , dq_o
  , ce0
  , oebar
  , webar
  , sts
  , rpbar
  , bytebar
  , ce1
  , ce2
);

    parameter memory_spec = "28f128j3a_150.soma";

     input [23:0] a;
     input [15:0] dq_i;
    output [15:0] dq_o;
     input        ce0;
     input        oebar;
     input        webar;
    output        sts;
     input        rpbar;
     input        bytebar;
     input        ce1;
     input        ce2;

endmodule

