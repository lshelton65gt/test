
   setenv CARBON_HOME /o/release/C2006_10_5065_p27
   setenv MAXSIM_HOME /tools/linux/ARM/SoCDesigner_6.1
   setenv CXX $CARBON_HOME/Linux/gcc3/bin/g++
   setenv DENALI /tools/denali/3.2.027

   setenv LD_LIBRARY_PATH $CARBON_HOME/Linux/gcc345/lib:${LD_LIBRARY_PATH}

   set path = ( $MAXSIM_HOME/bin $path )
