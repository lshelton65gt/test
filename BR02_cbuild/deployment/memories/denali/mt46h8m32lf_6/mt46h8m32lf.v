// Micron - Mobile Double Data Rate (DDR) SDRAM
// MT46H8M32LF/LG - 2 Meg x 32 x 4 banks

// Input/Output Data busses are configured 4 x 8-byte lanes

module mt46h8m32lf (
    dqs0_i
  , dqs1_i
  , dqs2_i
  , dqs3_i
  , dqs0_o
  , dqs1_o
  , dqs2_o
  , dqs3_o
  , dqs_en
  , dq0_i
  , dq1_i
  , dq2_i
  , dq3_i
  , dq0_o
  , dq1_o
  , dq2_o
  , dq3_o
  , dq_en
  , a
  , ba
  , dm
  , webar
  , casbar
  , rasbar
  , csbar
  , cke
  , ckbar
  , ck
);

   parameter memory_spec = "mt46h8m32lf_6.soma";

    input         dqs0_i, dqs1_i, dqs2_i, dqs3_i;
   output         dqs0_o, dqs1_o, dqs2_o, dqs3_o;
   output [  3:0] dqs_en ; 
   output [ 31:0] dq_en   ; 
    input [  7:0] dq0_i, dq1_i, dq2_i, dq3_i;
   output [  7:0] dq0_o, dq1_o, dq2_o, dq3_o;
    input [ 12:0] a             ; 
    input [  1:0] ba            ; 
    input [  3:0] dm            ; 
    input         webar         ; 
    input         casbar        ; 
    input         rasbar        ; 
    input         csbar         ; 
    input         cke           ; 
    input         ckbar         ; 
    input         ck            ; 

mt46h8m32lf_cm  #( memory_spec ) cmodel (
    .dqs_i({dqs3_i, dqs2_i, dqs1_i, dqs0_i})
  , .dqs_o({dqs3_o, dqs2_o, dqs1_o, dqs0_o})
  , .dqs_en(dqs_en)
  , .dq_i({dq3_i, dq2_i, dq1_i, dq0_i})
  , .dq_o({dq3_o, dq2_o, dq1_o, dq0_o})
  , .dq_en(dq_en)
  , .a(a)
  , .ba(ba)
  , .dm(dm)
  , .webar(webar)
  , .casbar(casbar)
  , .rasbar(rasbar)
  , .csbar(csbar)
  , .cke(cke)
  , .ckbar(ckbar)
  , .ck(ck)
);

endmodule

module mt46h8m32lf_cm (
    dqs_i
  , dqs_o
  , dqs_en
  , dq_i
  , dq_o
  , dq_en
  , a
  , ba
  , dm
  , webar
  , casbar
  , rasbar
  , csbar
  , cke
  , ckbar
  , ck
);

   parameter memory_spec = "mt46h8m32lf_6.soma";

    input [  3:0] dqs_i         ; 
   output [  3:0] dqs_o, dqs_en ; 
    input [ 31:0] dq_i          ; 
   output [ 31:0] dq_o, dq_en   ; 
    input [ 12:0] a             ; 
    input [  1:0] ba            ; 
    input [  3:0] dm            ; 
    input         webar         ; 
    input         casbar        ; 
    input         rasbar        ; 
    input         csbar         ; 
    input         cke           ; 
    input         ckbar         ; 
    input         ck            ; 

endmodule
