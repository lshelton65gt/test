
#include <cstring>
#include <map>
#include "DenaliMemory.h"

#include "carbon/carbon_shelltypes.h"

//#include <systemc>
#include <iostream>
using namespace std;

class Mt46h8m32lf : public DenaliMemory
{
public:
   Mt46h8m32lf(string instance, string somaFile, string loadFile ="")
   : DenaliMemory(instance, somaFile, loadFile) {}

   virtual void init() // need separate init routine.
   {
     DenaliMemory::init();

     ck     = getPin("ck");
     ckbar  = getPin("ckbar");
     cke    = getPin("cke");
     csbar  = getPin("csbar");
     rasbar = getPin("rasbar");
     casbar = getPin("casbar");
     webar  = getPin("webar");
     ba     = getPin("ba");
     a      = getPin("a");
     dm     = getPin("dm");
     dq     = getPin("dq");
     dqs    = getPin("dqs");

     // Initialize inputs

     ck->init(      DENALI_D0 );
     ckbar->init(   DENALI_D1 );
     cke->init(     DENALI_D0 );
     csbar->init(   DENALI_D1 );
     rasbar->init(  DENALI_D1 );
     casbar->init(  DENALI_D1 );
     webar->init(   DENALI_D1 );
     ba->init(      DENALI_D0 );
     a->init(       DENALI_D0 );
     dm->init(      DENALI_D1 );
     dq->init(      DENALI_D0 );
     dqs->init(     DENALI_D0 );
  }

//protected:

  DenaliPin * ck     ; 
  DenaliPin * ckbar  ; 
  DenaliPin * cke    ; 
  DenaliPin * csbar  ; 
  DenaliPin * rasbar ; 
  DenaliPin * casbar ; 
  DenaliPin * webar  ; 
  DenaliPin * ba     ; 
  DenaliPin * a      ; 
  DenaliPin * dm     ; 
  DenaliPin * dq     ; 
  DenaliPin * dqs    ; 
};

typedef enum CDSmt46h8m32lf_cmContext{
  eCDSmt46h8m32lf_cmFallck,
  eCDSmt46h8m32lf_cmRiseck,
} CDSmt46h8m32lf_cmContext;

extern __C__ void* cds_mt46h8m32lf_cm_create(
    int numParams, CModelParam* cmodelParams, const char* inst);

extern __C__ void cds_mt46h8m32lf_cm_misc(
    void* hndl, CarbonCModelReason reason, void* cmodelData);

extern __C__ void cds_mt46h8m32lf_cm_run(
      void* hndl, CDSmt46h8m32lf_cmContext context
    , const UInt32* dqs_i // Input, size = 1 word(s)
    , UInt32* dqs_o // Output, size = 1 word(s)
    , UInt32* dqs_en // Output, size = 1 word(s)
    , const UInt32* dq_i // Input, size = 1 word(s)
    , UInt32* dq_o // Output, size = 1 word(s)
    , UInt32* dq_en // Output, size = 1 word(s)
    , const UInt32* a // Input, size = 1 word(s)
    , const UInt32* ba // Input, size = 1 word(s)
    , const UInt32* dm // Input, size = 1 word(s)
    , const UInt32* webar // Input, size = 1 word(s)
    , const UInt32* casbar // Input, size = 1 word(s)
    , const UInt32* rasbar // Input, size = 1 word(s)
    , const UInt32* csbar // Input, size = 1 word(s)
    , const UInt32* cke // Input, size = 1 word(s)
    , const UInt32* ckbar // Input, size = 1 word(s)
    , const UInt32* ck // Input, size = 1 word(s)
  );

extern __C__ void cds_mt46h8m32lf_cm_destroy(void* hndl);

void* cds_mt46h8m32lf_cm_create(
  int numParams, CModelParam* cmodelParams, const char* inst )
{
  string somaFile; // Get SOMA file from parameters

  for(int i = 0; i < numParams; ++i) 
    if(strcmp(cmodelParams[i].paramName, "memory_spec") == 0) 
      somaFile = cmodelParams[i].paramValue;

  Mt46h8m32lf* memory = new Mt46h8m32lf(inst, somaFile);
   
  memory->init(); // Constructs Denali Pins.

  return reinterpret_cast<void*>(memory);
}

void cds_mt46h8m32lf_cm_misc(
    void* hndl, CarbonCModelReason reason, void* cmodelData)
{
  if (reason == eCarbonCModelID) {
    assert(hndl);
    Mt46h8m32lf* memory = reinterpret_cast<Mt46h8m32lf*>(hndl);
    memory->setCarbonObj( reinterpret_cast<CarbonObjectID*>(cmodelData) );
  }
}

void cds_mt46h8m32lf_cm_run(
      void* hndl, CDSmt46h8m32lf_cmContext context
    , const UInt32* dqs_i // Input, size = 1 word(s)
    , UInt32* dqs_o // Output, size = 1 word(s)
    , UInt32* dqs_en // Output, size = 1 word(s)
    , const UInt32* dq_i // Input, size = 1 word(s)
    , UInt32* dq_o // Output, size = 1 word(s)
    , UInt32* dq_en // Output, size = 1 word(s)
    , const UInt32* a // Input, size = 1 word(s)
    , const UInt32* ba // Input, size = 1 word(s)
    , const UInt32* dm // Input, size = 1 word(s)
    , const UInt32* webar // Input, size = 1 word(s)
    , const UInt32* casbar // Input, size = 1 word(s)
    , const UInt32* rasbar // Input, size = 1 word(s)
    , const UInt32* csbar // Input, size = 1 word(s)
    , const UInt32* cke // Input, size = 1 word(s)
    , const UInt32* ckbar // Input, size = 1 word(s)
    , const UInt32* ck // Input, size = 1 word(s)
)
{
  Mt46h8m32lf  * mem = reinterpret_cast<Mt46h8m32lf*>(hndl);
  CarbonTime   ctime = carbonGetSimulationTime(mem->getVHM());
  DENALItimeT  dtime = CarbonTimeToDenaliTime(ctime);
  
	setCurrentDenaliInstance(mem);	

  // copy carbon inputs to Denali model

  mem->ck->input(          ck,   dtime );
  mem->ckbar->input(    ckbar,   dtime );
  mem->cke->input(        cke,   dtime );
  mem->csbar->input(    csbar,   dtime );
  mem->rasbar->input(  rasbar,   dtime );
  mem->casbar->input(  casbar,   dtime );
  mem->webar->input(    webar,   dtime );
  mem->ba->input(          ba,   dtime );
  mem->a->input(            a,   dtime );
  mem->dm->input(          dm,   dtime );
  mem->dq->input(        dq_i,   dtime );
  mem->dqs->input(      dqs_i,   dtime );

  DENALIeval( mem->getDenaliHdl() );
  
  // drive carbon outputs from Denali model

  if(dq_o)     mem->dq->output(dq_o);
  if(dqs_o)    mem->dqs->output(dqs_o);
}

void cds_mt46h8m32lf_cm_destroy(void* hndl)
{
  delete(reinterpret_cast<Mt46h8m32lf*>(hndl));
}
