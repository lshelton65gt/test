-- ======================================================================
-- Copyright 1999-2005 by Denali Software, Inc.  All rights reserved.
-- ======================================================================
-- 
-- This SOMA file describes a memory model, using Denali Software's
-- proprietary SOMA language.  By using this SOMA file, you agree to the
-- following terms.  If you do not agree to these terms, you may not use
-- this SOMA file.
-- 
-- Subject to the restrictions set forth below, Denali Software grants
-- you a non-exclusive, non-transferable license only to use this SOMA
-- file to simulate the memory described in it using tools supplied by
-- Denali Software.
-- 
-- You may not:
-- 
--   (1)  Use this SOMA file to create software programs or tools that use
--        SOMA files as either input or output.
-- 
--   (2)  Modify this SOMA file or the SOMA language in any manner.
-- 
--   (3)  Use this SOMA file to create other languages for describing
--        memory models.
-- 
--   (4)  Distribute this SOMA file to others.
-- 
-- This SOMA file is based on information received by Denali Software
-- from third parties.  DENALI SOFTWARE PROVIDES THIS SOMA FILE "AS IS"
-- AND EXPRESSLY DISCLAIMS ALL REPRESENTATIONS, WARRANTIES AND
-- CONDITIONS, INCLUDING BUT NOT LIMITED TO WARRANTIES AND CONDITIONS OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND
-- NONINFRINGEMENT.  DENALI SOFTWARE'S AGGREGATE LIABILITY ARISING FROM
-- YOUR USE OF THIS SOMA FILE IS LIMITED TO ONE U.S. DOLLAR.
-- 
-- If you have any questions or if you would like to inquire about
-- obtaining additional or different rights in SOMA files or the SOMA
-- language, please contact Denali Software, at www.denali.com or at
-- info@denalisoft.com.
--
--************************************************************************
-- Manufacturer    : MICRON
-- Memory Class    : DDR_SDRAM
-- Part Name       : mt46h8m32lf-6
-- Revision        : Thu Oct 13 17:09:38 2005
-- Description     : 256Mb (2M x 32Bit x 4 Banks) Mobile DDR SDRAM
-- Datasheet URL   : http://download.micron.com/pdf/datasheets/dram/mobile/MT46H16M16LF.pdf
-- Datasheet Info  : Rev. A, 08/05
-- Author Name     : Denali Software KK
-- Author Comment  : 
--************************************************************************
Version 0.001
ddr_sdram

Sizes
columnWidth 8
numBanks 4
prechargeBit 10
initRefresh 2
refreshBurstLimit 8
numRefreshCycle 0
writeReadMinDelay 2
temperatureRanges "-25:70,70:105"
highTempThreshold 0
driveStrengthBits "6,5"
driveStrengthValues "0,1,2,3"

Features
checkInitialization 1
enableCasLatency3 1
enableCasLatency15 0
fullPage 1
enableReadIntrptReadA 0
enableCasLatency4 0
enableCasLatency25 0
enableCasLatency2 0
checkEvenAddressFullPageRead 0
enableIntrptWriteA 0
enableIntrptAPBurst 0
concurrentAutoPrecharge 1
enableReadIntrptWrite 1
trasLockout 1
usetQHforDQoutputwindow 1
useSeparateTRCDforRW 0
enableCasLatency6 0
enableCasLatency5 0
enableBurstLength2 1
enableBurstLength8 1
enableWriteIntrptRead 1
tdqssCheckPreambleOnly 0
ddrIIWriteLatency 0
allowModeAtDLLReset 0
fixedBurstSequence 0
dllLockNopOnly 0
lowPower 1
PASRfullBankOnly 0
EMRSnotRequired 1
enableDeepPD 1
enableBurstLength16 1
checkEvenAddressAlways 0
disableCancelAP 0
concurrentAPwriteOnly 0
enablePreDuringAP 1
twpresNotRequired 0
dllLockNoExecutableCmd 0
DeepPDexitPause 1
initPauseCKEHigh 1
initPauseDQMHigh 0
hiZPin 0
temperatureReg 0
disableSrefAtHighTemp 0
enableClkStop 1
allowClkHighInClkStop 0
enableClkFreqChange 1
disablePDClkChecks 0
enableLowPowerPDMode 0
dynamicReadPreambleTime 0
dynamicAccessTime 0
partialConcurrentAP 0
enableDARF 0

Pins
clk ck 1
clkbar ckbar 1
cke cke 1
csbar csbar 1
rasbar rasbar 1
casbar casbar 1
webar webar 1
dqm dm 4
ba ba 2
address a 13
data dq 32
dqs dqs 4
ap ap 1
hiz hiz 1
tq tq 0

Timing
tck25 7.5 ns
tck25_max 13 ns
tck2 12 ns
tck2_max disabled
-- tck15 off
-- tck15_max off
tch 0.45 clk
tcl 0.45 clk
tds 0.6 ns
tdh 0.6 ns
tdipw 2.1 ns
tis 1.2 ns
tih 1.2 ns
trc 60 ns
tras 42 ns
tras_max 70000 ns
trp 18 ns
trcd 18 ns
trrd 12 ns
twr 12 ns
-- twtr1 off
twtr2 1 clk
tref 64 ms
tac 2 ns
tdqsck 2 ns
tdqsq 0.5 ns
thz 1 ns
tac_max 5.5 ns
tdqsck_max 5.5 ns
tdqsq_max 0.4 ns
thz_max 5.5 ns
tdv 0.375 ns
tdqss 0.75 clk
tdqss_max 1.25 clk
twpst 0.4 clk
twpst_max 0.6 clk
tinit 200 us
tdll 200 clk
tqh_thp 0.45 clk
tqh_sub 0.65 ns
tck4 0 ns
tck4_max 1 s
tck3 6 ns
tck3_max disabled
tck15 0 ns
tck15_max 1 s
trpre_min 0.9 clk
trpre_max 1.1 clk
trc2 0 clk
trc3 0 clk
tras2 0 clk
tras3 0 clk
tras_max2 0 clk
tras_max3 0 clk
trcd2 0 clk
trcd3 0 clk
trcdrd 0 ns
trcdwr 0 ns
trpa 0 ns
trpa2 0 clk
trpa3 0 clk
twtr1 0 clk
tdssk_min disabled
tdssk_max disabled
tdal disabled
trtw disabled
tck6 0 ns
tck6_max 1 s
tck5 0 ns
tck5_max 1 s
tpdex 25 ns
tdscs 0.2 clk
tdsch 0.2 clk
tccd disabled
twpres 0 ns
twpre 0.25 clk
tch_max 0.55 clk
tcl_max 0.55 clk
tlz 1 ns
tlz_max disabled
tipw 2.7 ns
trpst_min 0.4 clk
trpst_max 0.6 clk
trfc 70 ns
tmrd 2 clk
tdqsh 0.35 clk
tdqsl 0.35 clk
tcke 2 clk
tsrfc disabled
trefiCool 0 ns
trefiHot 0 ns
tac_maxCool 1 s
tac_maxHot 1 s
tdqsck_maxCool 1 s
tdqsck_maxHot 1 s
thz_maxCool 1 s
thz_maxHot 1 s
tlz_maxCool 1 s
tlz_maxHot 1 s
tdsCool 0 ns
tdsHot 0 ns
tdhCool 0 ns
tdhHot 0 ns
tisCool 0 ns
tisHot 0 ns
tihCool 0 ns
tihHot 0 ns
trasCool 0 ns
trasHot 0 ns
trcCool 0 ns
trcHot 0 ns
trfcCool 0 ns
trfcHot 0 ns
trcdCool 0 ns
trcdHot 0 ns
trpCool 0 ns
trpHot 0 ns
trrdCool 0 ns
trrdHot 0 ns
twrCool 0 clk
twrHot 0 clk
txsnrCool 0 ns
txsnrHot 0 ns
tlpthreshold 0 ns
txsnr1 0 ns
txsnr2 0 ns
txsrd1 0 ns
txsrd2 0 ns
tpdex1 0 ns
tpdex2 0 ns
dllEnblToRstDelay disabled
txsnr 120 ns
txsrd 120 ns
trpre_min2 0.5 clk
trpre_max2 1.1 clk
trpre_min3 0.9 clk
trpre_max3 1.1 clk
tac_min2 2 ns
tac_min3 2 ns
tac_max2 6.5 ns
tac_max3 6 ns

