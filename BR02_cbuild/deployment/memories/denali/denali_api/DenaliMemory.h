#ifndef DENALIMEMORY_H_
#define DENALIMEMORY_H_

#include "carbon/carbon_capi.h"
#include <cstdio>  // yukon needs this...
#include "yukon/yukon.h"
#include <map>
#include <string>

using namespace std;

class DenaliPin
{
public:
   DenaliPin(int busSize, DENALIpinHandleT pinHdl);
  ~DenaliPin();

   void init(DENALIdataT v); // initial value: 0, 1, x, z

   void update( // read Denali pinValue; update internal value/enable
          DENALIdataPT value
        , int busSize
        , DENALItimeT delay);

   void  input(const UInt32* val, DENALItimeT dtime);
   void enable(      UInt32* ptr) { *ptr= *mEnable; }

   void output(      UInt32* ptr)
   {
     for (UInt32 w=0; w < words; w++) ptr[w]= mData[w];  
   }

protected:
  int                   busSize;
  UInt32                msbits, words;
  DENALIpinHandleT      pinHdl;
  DENALIdriverHandleT   driverHdl;
  DENALIdataT         * value;
  UInt32              * mData;
  UInt32              * mEnable;
  bool                  updateMe;
  DENALItimeT           dtime; // , delay; // current time & delay

  static DENALItimeT    gtime; // global current time 

private:
  ostream & print( ostream &os ) const;

  friend ostream & operator <<(ostream &os, const DenaliPin &r)
  { return r.print(os); }

};

typedef map<string, DenaliPin *> PinMap;

class DenaliMemory
{
public:
   DenaliMemory(string instance, string somaFile, string loadFile);
   virtual ~DenaliMemory();

   virtual void init(); // need separate init routine.

   void initPin(const char *pinName, DENALIdataT);

   void addPin(
         const char *pinName
       , int busSize
       , DENALIpinHandleT pinHdl);

   DenaliPin * getPin(const char *pinName) { return mPin[pinName]; }

   DENALIinstanceHandleT   getDenaliHdl() { return denaliHdl; }
   CarbonObjectID        * getVHM()       { return mCarbonObj; }

   void setCarbonObj( CarbonObjectID * obj ) { mCarbonObj = obj; }

protected:
  string mInstance;
  string mSomaFile;
  string mLoadFile;
  bool   mInit;
  PinMap mPin; 
  CarbonObjectID        * mCarbonObj;
  DENALIinstanceHandleT   denaliHdl;

  static int gInstanceCount;
};

extern DenaliMemory* gCurrentInstance;

void setCurrentDenaliInstance(DenaliMemory* instance);
DenaliMemory* getCurrentDenaliInstance();

inline DENALItimeT CarbonTimeToDenaliTime (CarbonTime t) 
{
  DENALItimeT dt;
  /* on some platforms could this just be a cast? */
  dt.low  =  t        & 0xffffffff;
  dt.high = (t >> 32) & 0xffffffff;
  return dt;
}

#endif // DENALIMEMORY_H_
