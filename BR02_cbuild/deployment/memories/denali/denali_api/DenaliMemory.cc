// This file provides the functions that connect Denali to Carbon

#include "DenaliMemory.h"

DenaliMemory* gCurrentInstance;

int DenaliMemory::gInstanceCount= 0;

#include <cassert>
#include <iostream>

ostream & DenaliPin::print( ostream &os ) const
{
  return os << "data= " << *mData 
            << ", en= " << *mEnable << endl;
}

DenaliPin::DenaliPin(int _busSize, DENALIpinHandleT _pinHdl)
: busSize(_busSize), pinHdl(_pinHdl), driverHdl(0)
, msbits ( busSize%32)
,   words( (msbits?1:0) + busSize/32)
,  value ( new DENALIdataT[busSize])
, mData  ( new UInt32[words])
, mEnable( new UInt32[1])
{
  dtime.low= dtime.high= 0;
  gtime.low= gtime.high= 0;
}

DenaliPin::~DenaliPin()
{
  delete [] value  ;
  delete [] mData  ;
  delete [] mEnable;
}

void DenaliMemory::addPin(
  const char *pinName, int busSize, DENALIpinHandleT pinHandle)
{
  mPin[string(pinName)] = new DenaliPin(busSize, pinHandle);
}

void DENALIEreportPinHandle(
    char *pinName, DENALIpinHandleT pinHandle, int busSize)
{
  gCurrentInstance->addPin(pinName, busSize, pinHandle);
}

void DENALIEgetDriver(
    char *pinName, DENALIdriverHandleT * driverHandle, int busSize)
{
  DenaliPin *p = gCurrentInstance->getPin(pinName);

  if(p) {
    *driverHandle = reinterpret_cast<DENALIdriverHandleT>(p);
  } else {
    assert(0);
  }
}

void DENALIEscheduleDriver(
    DENALIdriverHandleT driverHandle
  , DENALIdataPT value
  , int busSize
  , DENALItimeT delay)
{
  DenaliPin * p = reinterpret_cast<DenaliPin *>(driverHandle);

  p->update(value, busSize, delay);
}

void DENALIEgetTimeResolution(DENALItimeT * resolution)
{
  resolution->high = 0;
  resolution->low = 1000; // fixed 1ps
}

void DENALIEgetTimeNow(DENALItimeT * now)
{
  CarbonObjectID* vhm = getCurrentDenaliInstance()->getVHM();
  
  if(vhm) {
    CarbonTime t = carbonGetSimulationTime(vhm);
    now->high = (t >> 32) & 0xFFFFFFFF;
    now->low = t & 0xFFFFFFFF;
  } else {
    // This code gets called once before the VHM is done being constructed
    // so we'll assume that if the vhm handle is 0, it's because the
    // construction isn't done and this must be time 0
    now->high = now->low = 0;
  }
}

void DENALIEreportMessage(
    char *message, int msgCode, DENALIseverityT severity)
{

  cout << dec << "Denali Message " << msgCode
            << " " << severity << " "<< message;
}

void DENALIEfatalError(void) { assert(0); }

/*
** Utility Funcs
*/

// was void initPin(PinData*p, DENALIdataT v)

void DenaliPin::init(DENALIdataT v)
{
  DENALItimeT t;
  t.low = t.high = 0;
  
  for(int i = 0; i < busSize; ++i) value[i] = v;
  
  DENALIreportEvent(pinHdl, value, busSize, t);
}

void setCurrentDenaliInstance(DenaliMemory *instance)
{
  gCurrentInstance = instance;
}

DenaliMemory *getCurrentDenaliInstance() { return gCurrentInstance; }

// was void
//    reportPinEvent (PinData* pin, const UInt32* val, DENALItimeT dtime) 
// and void
//    setDenaliValueFromCarbonValue(PinData*pin, const UInt32* val)

void DenaliPin::input(const UInt32* val, DENALItimeT dtime)
{
  // set global time
  gtime.low = dtime.low;
  gtime.high= dtime.high;

  UInt32 tmp = 0xDEADBEEF;

  for(int i =0; i < busSize; ++i) {
    if (i % 32 == 0) { tmp = val[i / 32]; }

    value[i] = (tmp & 0x1) ? DENALI_D1 : DENALI_D0;

    tmp >>= 1;  
  }

  // reorder values msb->lsb to lsb->msb

  DENALIdataT swap;

  for (int i=0, j=busSize-1; i < busSize / 2; i++, j--) {
      swap     = value[i];
      value[i] = value[j];
      value[j] = swap;
  }

  DENALIreportEvent(pinHdl, value, busSize, dtime);
}

/* read Denali pinValue; update internal value/enable
 *
 * merge with void DenaliFourStateToCarbon(
 *  DENALIdataT* d, UInt32* out, UInt32* en, int bits)
 */

DENALItimeT DenaliPin::gtime; // global current time 

void DenaliPin::update(
          DENALIdataPT _value
        , int busSize
        , DENALItimeT delay)
{
  /* Hack for DDR 
   * data=aaaa @ delay=2450 
   * data=xxxx @ delay=4060 
   */

  if(gtime.low || gtime.high)
  { // do not check @ t=0
    if( dtime.high == gtime.high && dtime.low == gtime.low ) return;
/**
    cout << "gtime= " << gtime.high << ":" << gtime.low << endl;
    cout << "dtime= " << dtime.high << ":" << dtime.low << endl;

    if(   dtime.high == gtime.high
       && dtime.low  == gtime.low )
    {
      cout << "ignoring 2nd event" << endl;
      return;
    }
**/
  }

  // set current time == global time
  dtime.low = gtime.low;
  dtime.high= gtime.high;

  // end-of-hack DDR

  value= _value;

  *mEnable= (value[0] == DENALI_Dz) ? 0 : 1;
  
  for (UInt32 n=0, w=0; w < words; w++) {
    UInt32 val = 0, bmask= w ? 1<<31 : 1<<(msbits-1);

    for (UInt32 b = bmask; b; b>>=1, n++) {

      val <<= 1; 
      val  |= value[n] == DENALI_D1 ? 1 : 0;
    }

    mData[w]= val;
  }
}

DenaliMemory::DenaliMemory(string instance, string somaFile, string loadFile)
: mInstance(instance), mSomaFile(somaFile), mLoadFile(loadFile)
{
  gInstanceCount++;

  setCurrentDenaliInstance(this);
}

// Must call init after constructor -- DenaliMemory instance
// needed by yukon: void DENALIEreportPinHandle(...) callback routine.

void DenaliMemory::init()
{
  denaliHdl= DENALIinstantiate(
          (char *) mSomaFile.c_str()
        , (char *) mInstance.c_str()
        , mLoadFile.empty() ? 0 : (char *) mLoadFile.c_str()
        , 0, DENALI_TYP);

  // Verify all the Memory Pins, DENALI requires this.

  int num = DENALIgetNumPins(denaliHdl); 
  char      * n;
  int         size;
  DENALImodeT mode;
     
  for(int i = 1; i <= num; ++i) 
    DENALIgetPinInfo(denaliHdl, i, &n, &mode, &size );
}

DenaliMemory::~DenaliMemory()
{
  if(!--gInstanceCount) DENALIterminate();

  for(PinMap::iterator it = mPin.begin(); it != mPin.end(); ++it) {
    DenaliPin * p = it->second;
    
    delete p;
  }
}
