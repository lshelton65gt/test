module carbon_mem_1rw_reg_bitmask(clka, adra, dia, wea, doa, bea);
   parameter ADDR_WIDTH = 3;
   parameter DATA_WIDTH = 8;
   parameter DEPTH = 8;

   input     clka;
   input [ADDR_WIDTH-1:0] adra;
   input [DATA_WIDTH-1:0] dia;
   input 		  wea;
   input [DATA_WIDTH-1:0] bea;
   output [DATA_WIDTH-1:0] doa;
   
   reg [DATA_WIDTH-1:0]    doa;
   reg [DATA_WIDTH-1:0]    mem[0:DEPTH-1];
   reg [DATA_WIDTH-1:0]    tmp;


   always @(posedge clka)
     if (wea) begin
	tmp = mem[adra];   
	mem[adra] <= (dia & bea) | (tmp & ~bea);	
     end 
     else begin
	doa <= mem[adra];
     end
endmodule // carbon_mem_1rw_reg

