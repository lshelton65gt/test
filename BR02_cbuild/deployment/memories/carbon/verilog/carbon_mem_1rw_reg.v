module carbon_mem_1rw_reg(clka, adra, dia, wea, doa);
   parameter ADDR_WIDTH = 3;
   parameter DATA_WIDTH = 8;
   parameter DEPTH = 8;

   input     clka;
   input [ADDR_WIDTH-1:0] adra;
   input [DATA_WIDTH-1:0] dia;
   input 		  wea;
   output [DATA_WIDTH-1:0] doa;
   
   reg [DATA_WIDTH-1:0] doa;
   reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];

   always @(posedge clka)
     if (wea)
       mem[adra] <= dia;
     else
       doa <= mem[adra];

endmodule // carbon_mem_1rw_reg

