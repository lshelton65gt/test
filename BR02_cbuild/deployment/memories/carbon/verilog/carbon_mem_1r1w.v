module carbon_mem_1r1w(clk, wadr, di, we, radr, do);
   parameter ADDR_WIDTH = 3;
   parameter DATA_WIDTH = 8;
   parameter DEPTH = 8;

   input     clk;
   input [ADDR_WIDTH-1:0] wadr;
   input [DATA_WIDTH-1:0] di;
   input 		  we;
   input [ADDR_WIDTH-1:0] radr;
   output [DATA_WIDTH-1:0] do;
   
   reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];

   always @(posedge clk)
     if (we)
       mem[wadr] <= di;
   
   assign 		do = mem[radr];
   
endmodule // carbon_mem_1r1w
