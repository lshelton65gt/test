module carbon_mem_2rw_reg_bitmask(clka, clkb, adra, adrb, dia, dib, wea, web, doa, dob, bea, beb);
   parameter ADDR_WIDTH = 3;
   parameter DATA_WIDTH = 8;
   parameter DEPTH = 8;

   input     clka, clkb;
   input [ADDR_WIDTH-1:0] adra, adrb;
   input [DATA_WIDTH-1:0] dia, dib;
   input 		  wea, web;
   input [DATA_WIDTH-1:0] bea, beb;

   output [DATA_WIDTH-1:0] doa, dob;
   
   reg [DATA_WIDTH-1:0] doa, dob;
   reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];
   reg [DATA_WIDTH-1:0] tmpa, tmpb;

   
   always @(posedge clka) begin
     if (wea) begin
       tmpa = mem[adra];   
       mem[adra] <= (dia & bea) | (tmpa & ~bea);
     end
     else begin
       doa <= mem[adra];
     end
   end
   
   always @(posedge clkb) begin
     if (web) begin
       tmpb = mem[adrb];	
       mem[adrb] <= (dib & beb) | (tmpb & ~beb);
     end
     else begin
       dob <= mem[adrb];
     end
   end
   
endmodule // carbon_mem_2rw_reg
