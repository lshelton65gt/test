/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Carbon implementation of basic (single data rate) SDRAM

module carbon_sdram(clk,
		    cke,
		    ba,
		    addr,
		    cs_n,
		    ras_n,
		    cas_n,
		    we_n,
		    dqi,
		    dqo,
		    dqm);

   parameter ROW_BITS = 12;
   parameter COL_BITS = 9;
   parameter DATA_BITS = 32;
   parameter MASK_BITS = 4;
   parameter USE_BANKS = 0;
   parameter ADDR_BITS = (ROW_BITS > COL_BITS) ? ROW_BITS : COL_BITS;
   parameter MEM_ADDR_BITS = ROW_BITS + COL_BITS + 2;
   parameter DEPTH = 1 << MEM_ADDR_BITS;
   parameter PAGE_SIZE = 1 << COL_BITS;
   parameter BITS_PER_MASK = DATA_BITS / MASK_BITS;
   
   input     clk;
   input     cke;
   input [1:0] ba;
   input [ADDR_BITS-1:0] addr;
   input 		 cs_n;
   input 		 ras_n;
   input 		 cas_n;
   input 		 we_n;
   input [DATA_BITS-1:0] dqi;
   output [DATA_BITS-1:0] dqo;
   input [MASK_BITS-1:0]  dqm;
   
   // Either the single mem or the 4 individual banks will be used
   // to store the data, depending on the USE_BANKS parameter
   reg [DATA_BITS-1:0] 	  mem[0:DEPTH-1];
   reg [DATA_BITS-1:0] 	  bank0[0:(DEPTH/4)-1];
   reg [DATA_BITS-1:0] 	  bank1[0:(DEPTH/4)-1];
   reg [DATA_BITS-1:0] 	  bank2[0:(DEPTH/4)-1];
   reg [DATA_BITS-1:0] 	  bank3[0:(DEPTH/4)-1];

   // The command queue
`define QUEUE_DEPTH 'd10

   // bits 1:0 are the command
`define COMMAND 1:0
`define Q_NOP 2'b00
`define Q_READ 2'b01
`define Q_WRITE 2'b10

   // bits (COL_BITS+1):2 are the number of addresses remaining in a burst
   // read/write of a full page.  For burst length = 2|4|8 this is 0
`define BURST_REMAINING (COL_BITS+1):2

   // bits (MEM_ADDR_BITS+COL_BITS+1):(COL_BITS+2) are the address
`define ADDRESS (MEM_ADDR_BITS+COL_BITS+1):(COL_BITS+2)
   // split up for when we're accessing individual banks
`define BANK_ADDRESS (MEM_ADDR_BITS+COL_BITS+1-2):(COL_BITS+2)
`define BANK (MEM_ADDR_BITS+COL_BITS+1):(MEM_ADDR_BITS+COL_BITS)

`define CMD_WIDTH (MEM_ADDR_BITS+COL_BITS+2)

//   reg [`CMD_WIDTH-1:0] command_q[0:QUEUE_DEPTH-1];
   reg [`CMD_WIDTH*`QUEUE_DEPTH-1:0] command_q;

   // The mode register fields
`define BURST_LENGTH 2:0
`define LENGTH_2 3'b001
`define LENGTH_4 3'b010
`define LENGTH_8 3'b011
`define LENGTH_PAGE 3'b111

`define BURST_TYPE 3

`define CAS_LATENCY 6:4

`define WRITE_BURST 9

   reg [9:0] 					    mode_reg;
   
   // Memory commands - concat of cs_n, ras_n, cas_n, we_n
`define ACTIVE 4'b0011
`define READ 4'b0101
`define WRITE 4'b0100
`define TERMINATE 4'b0110
`define PRECHARGE 4'b0010
`define LOAD_MODE_REGISTER 4'b0000
   
   // Saved row addresses for each bank
   reg [ROW_BITS-1:0] 				    row_addr_b0;
   reg [ROW_BITS-1:0] 				    row_addr_b1;
   reg [ROW_BITS-1:0] 				    row_addr_b2;
   reg [ROW_BITS-1:0] 				    row_addr_b3;

   // Temp variables
   wire [DATA_BITS-1:0] dqo_tmp;
   reg [ROW_BITS-1:0] 				    row_addr;
   integer 					    i, j;
   reg [`CMD_WIDTH*`QUEUE_DEPTH-1:0] next_command_q;
   integer 					     burst_length;
   reg [`CMD_WIDTH-1:0] 			     temp_cmd; 			     
   reg [MEM_ADDR_BITS-1:0] 			     mem_addr;
   reg [DATA_BITS-1:0] 				     write_mask; 				     
   reg [COL_BITS-1:0] 				     burst_remaining;
   integer 					     start_term, end_term;
   
   wire [COL_BITS-1:0] 				     col_addr = addr[COL_BITS-1:0];
   wire [3:0] 					     cmd = {cs_n, ras_n, cas_n, we_n};
   wire [`CMD_WIDTH-1:0] 			     last_cmd = command_q[`CMD_WIDTH*`QUEUE_DEPTH-1:`CMD_WIDTH*(`QUEUE_DEPTH-1)]; 			     
   
   // Determine the burst length
   always @(mode_reg)
     case (mode_reg[`BURST_LENGTH])
       `LENGTH_2: burst_length = 2;
       `LENGTH_4: burst_length = 4;
       `LENGTH_8: burst_length = 8;
       default: burst_length = 0;
     endcase

   // Determine the write mask
   always @(dqm)
     for (i = 0; i < MASK_BITS; i = i + 1)
       for (j = 0; j < BITS_PER_MASK; j = j + 1)
	 if (dqm[i])    // mask off
	   write_mask[i * BITS_PER_MASK + j] = 1'b0;
	 else
	   write_mask[i * BITS_PER_MASK + j] = 1'b1;
   
   always @(posedge clk) begin
      // default behavior is just to step the queue, but if the last entry is for a
      // burst transaction, we need to continue the burst by creating the next entry
      if ((last_cmd[`COMMAND] != `Q_NOP) &&   // valid
	  (last_cmd[`BURST_REMAINING] != 0)) begin// burst
	 temp_cmd[`COMMAND] = last_cmd[`COMMAND];  // same command
	 temp_cmd[`BURST_REMAINING] = last_cmd[`BURST_REMAINING] - 1;  // decrement burst count
	 temp_cmd[`ADDRESS] = last_cmd[`ADDRESS] + 1;  // increment address - TODO - What happens when you hit the end of a page?
	 next_command_q = {temp_cmd,command_q[`CMD_WIDTH*`QUEUE_DEPTH-1:`CMD_WIDTH]};
      end
      else
	next_command_q = {{`CMD_WIDTH{1'b0}},command_q[`CMD_WIDTH*`QUEUE_DEPTH-1:`CMD_WIDTH]};

      // decode the current command
      case (cmd)
	`ACTIVE: begin
	   // save the row address for this bank
	   case (ba)
	     2'b00: row_addr_b0 <= addr[ROW_BITS-1:0];
	     2'b01: row_addr_b1 <= addr[ROW_BITS-1:0];
	     2'b10: row_addr_b2 <= addr[ROW_BITS-1:0];
	     2'b11: row_addr_b3 <= addr[ROW_BITS-1:0];
	   endcase
	end
	`READ: begin
	   // choose the correct bank address
	   case (ba)
	     2'b00: row_addr = row_addr_b0;
	     2'b01: row_addr = row_addr_b1;
	     2'b10: row_addr = row_addr_b2;
	     2'b11: row_addr = row_addr_b3;
	   endcase

	   // If we're interrupting a write, clear the writes from the queue
	   // We only need to clear (at most) entries 0 and 1 of the next
	   // command queue
	   if (command_q[`CMD_WIDTH+1:`CMD_WIDTH] == `Q_WRITE)
	     next_command_q[`COMMAND] = `Q_NOP;
	   if (command_q[`CMD_WIDTH*2+1:`CMD_WIDTH*2] == `Q_WRITE)
	     next_command_q[`CMD_WIDTH+1:`CMD_WIDTH] = `Q_NOP;
	   
	   // populate the command queue
	   if (burst_length == 0) begin
	      for (i = 0; i < `QUEUE_DEPTH - (mode_reg[`CAS_LATENCY] - 1); i = i + 1) begin
		 mem_addr[COL_BITS-1:0] = col_addr + i;
		 mem_addr[ROW_BITS+COL_BITS-1:COL_BITS] = row_addr;
		 mem_addr[ROW_BITS+COL_BITS+1:ROW_BITS+COL_BITS] = ba;
		 burst_remaining = PAGE_SIZE - 1 - i;
		 temp_cmd = {mem_addr, burst_remaining, `Q_READ};
		 // I wish we could use Verilog 2001 here!
		 for (j = 0; j < `CMD_WIDTH; j = j + 1)
		   next_command_q[`CMD_WIDTH*(i + mode_reg[`CAS_LATENCY] - 1) + j] = temp_cmd[j];
	      end
	   end
	   else begin
	      // 2, 4, or 8 burst length
	      for (i = 0; i < burst_length; i = i + 1) begin
		 if (mode_reg[`BURST_TYPE]) begin
		    case (burst_length)
		      'd2: begin
			 mem_addr[COL_BITS-1:1] = col_addr[COL_BITS-1:1];
			 mem_addr[0] = col_addr[0] ^ i;
		      end
		      'd4: begin
			 mem_addr[COL_BITS-1:2] = col_addr[COL_BITS-1:2];
			 mem_addr[1:0] = col_addr[1:0] ^ i;
		      end
		      'd8: begin
			 mem_addr[COL_BITS-1:3] = col_addr[COL_BITS-1:3];
			 mem_addr[2:0] = col_addr[2:0] ^ i;
		      end
		      default: mem_addr[COL_BITS-1:0] = {COL_BITS{1'b0}};
		    endcase
		 end
		 else begin
		    // Sequential mode
		    case (burst_length)
		      'd2: begin
			 mem_addr[COL_BITS-1:1] = col_addr[COL_BITS-1:1];
			 mem_addr[0] = col_addr[0] + i;
		      end
		      'd4: begin
			 mem_addr[COL_BITS-1:2] = col_addr[COL_BITS-1:2];
			 mem_addr[1:0] = col_addr[1:0] + i;
		      end
		      'd8: begin
			 mem_addr[COL_BITS-1:3] = col_addr[COL_BITS-1:3];
			 mem_addr[2:0] = col_addr[2:0] + i;
		      end
		      default: mem_addr[COL_BITS-1:0] = {COL_BITS{1'b0}};
		    endcase
		 end
		 mem_addr[ROW_BITS+COL_BITS-1:COL_BITS] = row_addr;
		 mem_addr[ROW_BITS+COL_BITS+1:ROW_BITS+COL_BITS] = ba;
		 temp_cmd = {mem_addr, {COL_BITS{1'b0}}, `Q_READ};
		 // I wish we could use Verilog 2001 here!
		 for (j = 0; j < `CMD_WIDTH; j = j + 1)
		   next_command_q[`CMD_WIDTH*(i + mode_reg[`CAS_LATENCY] - 1) + j] = temp_cmd[j];
	      end
	   end
	end

	`WRITE: begin
	   // choose the correct bank address
	   case (ba)
	     2'b00: row_addr = row_addr_b0;
	     2'b01: row_addr = row_addr_b1;
	     2'b10: row_addr = row_addr_b2;
	     2'b11: row_addr = row_addr_b3;
	   endcase
	   
	   // The first piece of data will be written at the end of this block

	   // if we're in burst mode, store the rest of the write commands
	   if (!mode_reg[`WRITE_BURST]) begin
	      // 0 means burst mode
	      if (burst_length == 0) begin
		 // TODO - page burst
	      end
	      else begin
		 // 2, 4, or 8 burst length
		 for (i = 1; i < burst_length + mode_reg[`CAS_LATENCY] - 1; i = i + 1) begin
		    if (mode_reg[`BURST_TYPE]) begin
		       case (burst_length)
			 'd2: begin
			    mem_addr[COL_BITS-1:1] = col_addr[COL_BITS-1:1];
			    mem_addr[0] = col_addr[0] ^ i;
			 end
			 'd4: begin
			    mem_addr[COL_BITS-1:2] = col_addr[COL_BITS-1:2];
			    mem_addr[1:0] = col_addr[1:0] ^ i;
			 end
			 'd8: begin
			    mem_addr[COL_BITS-1:3] = col_addr[COL_BITS-1:3];
			    mem_addr[2:0] = col_addr[2:0] ^ i;
			 end
			 default: mem_addr[COL_BITS-1:0] = {COL_BITS{1'b0}};
		       endcase
		    end
		    else begin
		       // Sequential mode
		       case (burst_length)
			 'd2: begin
			    mem_addr[COL_BITS-1:1] = col_addr[COL_BITS-1:1];
			    mem_addr[0] = col_addr[0] + i;
			 end
			 'd4: begin
			    mem_addr[COL_BITS-1:2] = col_addr[COL_BITS-1:2];
			    mem_addr[1:0] = col_addr[1:0] + i;
			 end
			 'd8: begin
			    mem_addr[COL_BITS-1:3] = col_addr[COL_BITS-1:3];
			    mem_addr[2:0] = col_addr[2:0] + i;
			 end
			 default: mem_addr[COL_BITS-1:0] = {COL_BITS{1'b0}};
		       endcase
		    end
		    mem_addr[ROW_BITS+COL_BITS-1:COL_BITS] = row_addr;
		    mem_addr[ROW_BITS+COL_BITS+1:ROW_BITS+COL_BITS] = ba;
		    if (i < burst_length)
		      temp_cmd = {mem_addr, {COL_BITS{1'b0}}, `Q_WRITE};
		    else
		      temp_cmd = {{MEM_ADDR_BITS{1'b0}}, {COL_BITS{1'b0}}, `Q_NOP};
		    // I wish we could use Verilog 2001 here!
		    for (j = 1; j < `CMD_WIDTH; j = j + 1)
		      next_command_q[`CMD_WIDTH*(i-1) + j] = temp_cmd[j];
		 end
	      end
	   end
	end

	`TERMINATE,  // treat these two the same
	  `PRECHARGE: begin

	     // Writes terminate immediately, but reads have to take CAS latency into account
	     if (command_q[`COMMAND] == `Q_WRITE)
	       start_term = 0;
	     else
	       start_term = mode_reg[`CAS_LATENCY] - 1;
	     
	     if (burst_length == 0)
	       // Just clear out the queue for the rest of the burst
	       end_term = `QUEUE_DEPTH;
	     else
	      // 2, 4, or 8 burst length
	       end_term = burst_length + mode_reg[`CAS_LATENCY] - 2;
			     
	      for (i = start_term; i < end_term; i = i + 1) begin
		 temp_cmd = {{MEM_ADDR_BITS{1'b0}}, {COL_BITS{1'b0}}, `Q_NOP};
		 // I wish we could use Verilog 2001 here!
		 for (j = 0; j < `CMD_WIDTH; j = j + 1)
		   next_command_q[`CMD_WIDTH*i + j] = temp_cmd[j];
	      end
	  end

	`LOAD_MODE_REGISTER:
	  mode_reg <= addr[9:0];
	
      endcase

      // Do the first write of a burst
      if (cmd == `WRITE) begin
	 mem_addr[COL_BITS-1:0] = col_addr;
	 mem_addr[ROW_BITS+COL_BITS-1:COL_BITS] = row_addr;
	 mem_addr[ROW_BITS+COL_BITS+1:ROW_BITS+COL_BITS] = ba;
	 if (USE_BANKS == 1) begin
	    case (ba)
	      2'b00 : bank0[mem_addr[ROW_BITS+COL_BITS-1:0]] <= (dqi & write_mask) | (bank0[mem_addr[ROW_BITS+COL_BITS-1:0]] & ~write_mask);
	      2'b01 : bank1[mem_addr[ROW_BITS+COL_BITS-1:0]] <= (dqi & write_mask) | (bank1[mem_addr[ROW_BITS+COL_BITS-1:0]] & ~write_mask);
	      2'b10 : bank2[mem_addr[ROW_BITS+COL_BITS-1:0]] <= (dqi & write_mask) | (bank2[mem_addr[ROW_BITS+COL_BITS-1:0]] & ~write_mask);
	      2'b11 : bank3[mem_addr[ROW_BITS+COL_BITS-1:0]] <= (dqi & write_mask) | (bank3[mem_addr[ROW_BITS+COL_BITS-1:0]] & ~write_mask);
	    endcase
	 end
	 else
	   mem[mem_addr] <= (dqi & write_mask) | (mem[mem_addr] & ~write_mask);
      end
      // if there's a write at the head of the queue, and we didn't just
      // terminate it, execute it
      else if ((command_q[`COMMAND] == `Q_WRITE) && (cmd != `READ) && (cmd != `TERMINATE) && (cmd != `PRECHARGE)) begin
	 if (USE_BANKS == 1) begin
	    case (command_q[`BANK])
	      2'b00 : bank0[command_q[`BANK_ADDRESS]] <= (dqi & write_mask) | (bank0[command_q[`BANK_ADDRESS]] & ~write_mask);
	      2'b01 : bank1[command_q[`BANK_ADDRESS]] <= (dqi & write_mask) | (bank1[command_q[`BANK_ADDRESS]] & ~write_mask);
	      2'b10 : bank2[command_q[`BANK_ADDRESS]] <= (dqi & write_mask) | (bank2[command_q[`BANK_ADDRESS]] & ~write_mask);
	      2'b11 : bank3[command_q[`BANK_ADDRESS]] <= (dqi & write_mask) | (bank3[command_q[`BANK_ADDRESS]] & ~write_mask);
	    endcase
	 end
	 else
	    mem[command_q[`ADDRESS]] <= (dqi & write_mask) | (mem[command_q[`ADDRESS]] & ~write_mask);
      end
      
      // update the command queue
      command_q <= next_command_q;
   end

   // output read data if the head of the command queue is a read
   assign dqo_tmp = (USE_BANKS == 1) ? ((command_q[`BANK] == 2'b00) ? bank0[command_q[`BANK_ADDRESS]] :
					(command_q[`BANK] == 2'b01) ? bank1[command_q[`BANK_ADDRESS]] :
					(command_q[`BANK] == 2'b10) ? bank2[command_q[`BANK_ADDRESS]] :
					                              bank3[command_q[`BANK_ADDRESS]]) :
		    mem[command_q[`ADDRESS]];

   assign dqo = (command_q[`COMMAND] == `Q_READ) ? dqo_tmp : {DATA_BITS{1'bz}};
   
endmodule
