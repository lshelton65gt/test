/*
 Carbon implementation of LSI's 2 port (2 read, 2 write) synchronous memory
 */

module carbon_lsi_mem_222s(clka, clkb, adra, adrb, dia, dib, mea, meb, wea, web, wema, wemb, doa, dob);
   parameter ADDR_WIDTH = 3;
   parameter DATA_WIDTH = 8;
   parameter DEPTH = 8;

   input     clka, clkb;
   input [ADDR_WIDTH-1:0] adra, adrb;
   input [DATA_WIDTH-1:0] dia, dib;
   input 		  mea, meb;
   input 		  wea, web;
   input [DATA_WIDTH-1:0] wema, wemb;
   output [DATA_WIDTH-1:0] doa, dob;
   
   reg [ADDR_WIDTH-1:0] adra_r, adrb_r;
   reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];

   always @(posedge clka)
     if (mea) begin
	if (wea)
	  mem[adra] <= (mem[adra] & ~wema) | (dia & wema);
	else
	  adra_r <= adra;
     end

   always @(posedge clkb)
     if (meb) begin
	if (web)
	  mem[adrb] <= (mem[adrb] & ~wemb) | (dib & wemb);
	else
	  adrb_r <= adrb;
     end

   assign doa = mem[adra_r];
   assign dob = mem[adrb_r];

endmodule
