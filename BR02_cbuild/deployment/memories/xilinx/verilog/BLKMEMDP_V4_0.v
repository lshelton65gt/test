/*
 *************************************************************************
 * Filename:    BLKMEMDP_V4_0.v
 * 
 * Description: The Verilog functional model for the Dual Port Block Memory
 * 
 * ***********************************************************************
 */

`timescale 1ns/10ps
`celldefine


`define c_dp_rom 4
`define c_dp_ram 2
`define c_write_first 0
`define c_read_first  1
`define c_no_change   2

module BLKMEMDP_V4_0(DOUTA, DOUTB, ADDRA, CLKA, DINA, ENA, SINITA, WEA, NDA, RFDA, RDYA, ADDRB, CLKB, DINB, ENB, SINITB, WEB,NDB, RFDB, RDYB);

  parameter  c_addra_width         =  11 ;
  parameter  c_addrb_width         =  9 ;
  parameter  c_default_data        = "0"; // indicates string of hex characters used to initialize memory
  parameter  c_depth_a             = 2048 ;
  parameter  c_depth_b             = 512 ;
  parameter  c_enable_rlocs        = 0 ; //  core includes placement constraints
  parameter  c_has_default_data    =  1;
  parameter  c_has_dina            = 1  ;  // indicate port A has data input pins
  parameter  c_has_dinb            = 1  ;  // indicate port B has data input pins
  parameter  c_has_douta           = 1 ; // indicates port A has  output
  parameter  c_has_doutb           = 1 ; // indicates port B has  output
  parameter  c_has_ena             =  1 ; // indicates port A has a ENA pin
  parameter  c_has_enb             =  1 ; // indicates port B has a ENB pin
  parameter  c_has_limit_data_pitch       = 1 ;
  parameter  c_has_nda             = 0 ; //  Port A has a new data pin
  parameter  c_has_ndb             = 0 ; //  Port B has a new data pin
  parameter  c_has_rdya            = 0 ; //  Port A has result ready pin
  parameter  c_has_rdyb            = 0 ; //  Port B has result ready pin
  parameter  c_has_rfda            = 0 ; //  Port A has ready for data pin
  parameter  c_has_rfdb            = 0 ; //  Port B has ready for data pin
  parameter  c_has_sinita          =  1 ; // indicates port A has a SINITA pin
  parameter  c_has_sinitb          =  1 ; // indicates port B has a SINITB pin
  parameter  c_has_wea             =  1 ; // indicates port A has a WEA pin
  parameter  c_has_web             =  1 ; // indicates port B has a WEB pin
  parameter  c_limit_data_pitch           = 16 ;
  parameter  c_mem_init_file     =  "null.mif";  // controls which .mif file used to initialize memory
  parameter  c_pipe_stages_a       =  0 ; // indicates the number of pipe stages needed in port A
  parameter  c_pipe_stages_b       =  0 ; // indicates the number of pipe stages needed in port B
  parameter  c_reg_inputsa         = 0 ; // indicates we, addr, and din of port A are registered
  parameter  c_reg_inputsb         = 0 ; // indicates we, addr, and din of port B are registered
  parameter  c_sinita_value        = "0000"; // indicates string of hex used to initialize A output registers
  parameter  c_sinitb_value        = "0000"; // indicates string of hex used to initialize B output resisters
  parameter  c_width_a             =  8 ;
  parameter  c_width_b             = 32 ;
  parameter  c_write_modea        = 2; // controls which write modes shall be used
  parameter  c_write_modeb        = 2; // controls which write modes shall be used

  // New Generics for Primitive Selection and Pin Polarity
   parameter c_ybottom_addr        = "1024";
   parameter c_yclka_is_rising     = 1; // controls the active edge of the CLKA Pin
   parameter c_yclkb_is_rising     = 1; // controls the active edge of the CLKB Pin
   parameter c_yena_is_high        = 1; // controls the polarity of the ENA Pin
   parameter c_yenb_is_high        = 1; // controls the polarity of the ENB Pin
   parameter c_yhierarchy          = "hierarchy1";
   parameter c_ymake_bmm           = 0;
   parameter c_yprimitive_type     = "4kx4"; // Indicates which primitive should be used to build the
                                             // memory if c_yuse_single_primitive=1
   parameter c_ysinita_is_high     = 1; // controls the polarity of the SINITA Pin
   parameter c_ysinitb_is_high     = 1; // controls the polarity of the SINITB Pin
   parameter c_ytop_addr           = "0";
   parameter c_yuse_single_primitive = 0; // controls whether the Memory is build out of a
                                          // user selected primitive or is built from multiple
                                          // primitives with the "optimize for area" algorithm used
   parameter c_ywea_is_high        = 1; // controls the polarity of the WEA Pin
   parameter c_yweb_is_high        = 1; // controls the polarity of the WEB Pin


// IO ports

    output [c_width_a-1:0] DOUTA;

    input [c_addra_width-1:0] ADDRA;
    input [c_width_a-1:0] DINA;
    input ENA, CLKA, WEA, SINITA, NDA;
    output RFDA, RDYA;

    output [c_width_b-1:0] DOUTB;

    input [c_addrb_width-1:0] ADDRB;
    input [c_width_b-1:0] DINB;
    input ENB, CLKB, WEB, SINITB, NDB;
    output RFDB, RDYB;
    
    reg [c_width_a-1:0] ram_temp [0:c_depth_a-1] ;

    reg [c_width_a-1:0] DOUTA;
    reg [c_width_b-1:0] DOUTB;

    wire ena_int;
    wire clka_int;
    wire enb_int;
    wire clkb_int;
    wire wea_i;
    wire web_i;

    assign ena_int  = defval(ENA, c_has_ena, 1, c_yena_is_high);
    assign clka_int = defval(CLKA, 1, 1, c_yclka_is_rising);
    assign enb_int  = defval(ENB, c_has_enb, 1, c_yenb_is_high);
    assign clkb_int = defval(CLKB, 1, 1, c_yclkb_is_rising);
    assign wea_i = defval(WEA, c_has_wea, 0, c_ywea_is_high);
    assign web_i = defval(WEB, c_has_web, 0, c_yweb_is_high);

initial begin
  if (c_has_default_data == 0)
    $readmemb(c_mem_init_file, ram_temp) ;
end

always @ (posedge CLKA) begin
  if (wea_i)
   ram_temp[ADDRA] <= DINA;   
end

always @ (posedge CLKB) begin
  if (web_i)
   ram_temp[ADDRB] <= DINB;   
end

always @(posedge clka_int) begin
  if (c_has_douta)
    if (ena_int == 1'b1) 
      DOUTA <= ram_temp[ADDRA];
  else
    DOUTA <= {c_width_a{1'b0}};
end

always @(posedge clkb_int) begin
  if (c_has_doutb)
    if (enb_int == 1'b1) 
      DOUTB <= ram_temp[ADDRB];
  else
    DOUTB <= {c_width_b{1'b0}};
end

function defval;
  input i;
  input hassig;
  input val;  
  input active_high;
begin
  if(hassig == 1)
  begin
    if (active_high == 1)
      defval = i;
    else
      defval = ~i;
  end
  else
    defval = val;
  end
endfunction


endmodule
