// Carbon implementation of Xilinx ASYNC_FIFO_V6_1 module
// NOTE: not all ports/parameters are implemented

module ASYNC_FIFO_V6_1 (DIN, WR_EN, WR_CLK, RD_EN, RD_CLK, AINIT, DOUT, 
                        FULL, EMPTY, ALMOST_FULL, ALMOST_EMPTY, 
                        WR_COUNT, RD_COUNT, RD_ACK, RD_ERR, WR_ACK, WR_ERR);


/*************************************************************************
 * Definition of Ports
 * DIN         : Input data bus for the fifo.
 * DOUT        : Output data bus for the fifo.
 * AINIT       : Asynchronous Reset for the fifo.
 * WR_EN       : Write enable signal.
 * WR_CLK      : Write Clock.
 * FULL        : Indicates a full condition in the FIFO. Full is asserted 
 *               when no more words can be written into the FIFO.
 * ALMOST_FULL : Indicates that the FIFO only has room for one additional
 *               word to be written before it is full.
 * WR_ACK      : Acknowledgement of a successful write on the 
 *               previous clock edge
 * WR_ERR      : The write operation on the previous clock edge was
 *               unsuccessful due to an overflow condition
 * WR_COUNT    : Number of data words in fifo(synchronous to WR_CLK)
 *               This is only the MSBs of the count value.
 * RD_EN       : Read enable signal.
 * RD_CLK      : Read Clock.
 * EMPTY       : Indicates an empty condition in the FIFO. Empty is
 *               asserted when no more words can be read from the FIFO.
 * ALMOST_EMPTY: Indicates that the FIFO only has one word remaining 
 *               in the FIFO. One additional read will cause an 
 *               EMPTY condition.
 * RD_ACK      : Acknowledgement of a successful read on the 
 *               previous clock edge
 * RD_ERR      : The read operation on the previous clock edge was
 *               unsuccessful due to an underflow condition
 * RD_COUNT    : Number of data words in fifo(synchronous to RD_CLK)
 *               This is only the MSBs of the count value.
 *************************************************************************/


//Declare user parameters and their defaults
parameter C_DATA_WIDTH		= 8;
parameter C_ENABLE_RLOCS	= 0;  // NOT IMPLEMENTED
parameter C_FIFO_DEPTH 		= 511;
parameter C_HAS_ALMOST_EMPTY	= 1;
parameter C_HAS_ALMOST_FULL 	= 1;
parameter C_HAS_RD_ACK	        = 1;
parameter C_HAS_RD_COUNT        = 1;
parameter C_HAS_RD_ERR	        = 1;
parameter C_HAS_WR_ACK	        = 1;
parameter C_HAS_WR_COUNT        = 1;
parameter C_HAS_WR_ERR	        = 1;
parameter C_RD_ACK_LOW	        = 0;
parameter C_RD_COUNT_WIDTH      = 2;
parameter C_RD_ERR_LOW	        = 0;
parameter C_USE_BLOCKMEM        = 1;  // NOT IMPLEMENTED
parameter C_WR_ACK_LOW	        = 0;
parameter C_WR_COUNT_WIDTH      = 2;
parameter C_WR_ERR_LOW	        = 0;

//Declare input and output ports
input  [C_DATA_WIDTH-1 : 0] DIN;
input  WR_EN;
input  WR_CLK;
input  RD_EN;
input  RD_CLK;
input  AINIT;
output [C_DATA_WIDTH-1 : 0] DOUT;
output FULL;
output EMPTY;
output ALMOST_FULL;
output ALMOST_EMPTY;
output [C_WR_COUNT_WIDTH-1 : 0] WR_COUNT;
output [C_RD_COUNT_WIDTH-1 : 0] RD_COUNT;
output RD_ACK;
output RD_ERR;
output WR_ACK;
output WR_ERR;

`protect

   // Declare a memory to hold the fifo data.
   // This is actually one element larger than we need
   // because it will allow us to compare pointers to
   // determine the number of entries in the fifo.
   // Otherwise, empty and full would look the same.

   reg [C_DATA_WIDTH-1:0] fifo_mem [0:C_FIFO_DEPTH];

   // Read/write pointers
   // Unfortunately, there's no parameter for the number of
   // bits needed for the pointers, and there's no constant
   // expression I can think of to define a parameter with
   // the size, so we'll need to manually wrap the pointers
   reg [31:0] 		  read_ptr, write_ptr;

   // Determine the number of entries by comparing the pointers
   wire [31:0] 		  num_entries = (write_ptr >= read_ptr) ? (write_ptr - read_ptr) : (write_ptr + C_FIFO_DEPTH + 1 - read_ptr);

   // Status outputs can be active high or active low, so
   // declare some internal registers, and assign the outputs
   // as appropriate

   reg 			  rd_ack_int, rd_err_int, wr_ack_int, wr_err_int;

   assign 		  RD_ACK = C_RD_ACK_LOW ? ~rd_ack_int : rd_ack_int;
   assign 		  RD_ERR = C_RD_ERR_LOW ? ~rd_err_int : rd_err_int;
   assign 		  WR_ACK = C_WR_ACK_LOW ? ~wr_ack_int : wr_ack_int;
   assign 		  WR_ERR = C_WR_ERR_LOW ? ~wr_err_int : wr_err_int;

   // equations for full/empty outputs
   reg 			  in_reset;

   assign 		  FULL = in_reset | (num_entries == C_FIFO_DEPTH);
   assign 		  EMPTY = in_reset | (num_entries == 0);
   assign 		  ALMOST_FULL = in_reset | (num_entries == (C_FIFO_DEPTH - 1)) | FULL;
   assign 		  ALMOST_EMPTY = in_reset | (num_entries == 1) | EMPTY;

   // current count values

   parameter 		  WR_COUNT_SCALE = (C_WR_COUNT_WIDTH == 0) ? 1 : (C_FIFO_DEPTH + 1) / (1 << C_WR_COUNT_WIDTH);
   parameter 		  RD_COUNT_SCALE = (C_RD_COUNT_WIDTH == 0) ? 1 : (C_FIFO_DEPTH + 1) / (1 << C_RD_COUNT_WIDTH);

   assign 		  WR_COUNT = C_HAS_WR_COUNT ? (num_entries / WR_COUNT_SCALE) : 'h0;
   assign 		  RD_COUNT = C_HAS_RD_COUNT ? (num_entries / RD_COUNT_SCALE) : 'h0;

   // Output data register
   reg [C_DATA_WIDTH-1:0] DOUT;
   
   // Implement the write logic
   always @(posedge WR_CLK or posedge AINIT)
     if (AINIT) begin
	write_ptr <= 32'b0;
	wr_ack_int <= 1'b0;
	wr_err_int <= 1'b0;
	in_reset <= 1'b1;
     end
     else begin
	in_reset <= 1'b0;
	if (WR_EN) begin
	   if (FULL) begin
	      wr_ack_int <= 1'b0;
	      wr_err_int <= 1'b1;
	   end
	   else begin
	      fifo_mem[write_ptr] <= DIN;
	      if (write_ptr == C_FIFO_DEPTH)
		write_ptr <= 32'b0;
	      else
		write_ptr <= write_ptr + 32'b1;
	      wr_ack_int <= 1'b1;
	      wr_err_int <= 1'b0;
	   end
	end
	else begin
	   wr_ack_int <= 1'b0;
	   wr_err_int <= 1'b0;
	end
     end
   
   
   // Implement the read logic
   always @(posedge RD_CLK or posedge AINIT)
     if (AINIT) begin
	read_ptr <= 32'b0;
	DOUT <= {C_DATA_WIDTH{1'b0}};
	rd_ack_int <= 1'b0;
	rd_err_int <= 1'b0;
     end
     else begin
	if (RD_EN) begin
	   if (EMPTY) begin
	      rd_ack_int <= 1'b0;
	      rd_err_int <= 1'b1;
	   end
	   else begin
	      DOUT <= fifo_mem[read_ptr];
	      if (read_ptr == C_FIFO_DEPTH)
		read_ptr <= 32'b0;
	      else
		read_ptr <= read_ptr + 32'b1;
	      rd_ack_int <= 1'b1;
	      rd_err_int <= 1'b0;
	   end
	end
	else begin
	   rd_ack_int <= 1'b0;
	   rd_err_int <= 1'b0;
	end
     end

`endprotect

endmodule
