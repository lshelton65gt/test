// Carbon implementation of Xilinx BLKMEMDP_V6_1 memory
// NOTE: not all ports/parameters are implemented

`define c_write_first 0
`define c_read_first  1
`define c_no_change   2

module BLKMEMDP_V6_1(DOUTA, DOUTB, ADDRA, CLKA, DINA, ENA, SINITA, WEA, NDA, RFDA, RDYA, ADDRB, CLKB, DINB, ENB, SINITB, WEB,NDB, RFDB, RDYB);
  parameter  c_addra_width         =  11 ;
  parameter  c_addrb_width         =  9 ;
  parameter  c_default_data        = "0"; // indicates string of hex characters used to initialize memory - NOT IMPLEMENTED
  parameter  c_depth_a             = 2048 ;
  parameter  c_depth_b             = 512 ;
  parameter  c_enable_rlocs        = 0 ; //  core includes placement constraints - NOT IMPLEMENTED
  parameter  c_has_default_data    =  1; // NOT IMPLEMENTED
  parameter  c_has_dina            = 1  ;  // indicate port A has data input pins
  parameter  c_has_dinb            = 1  ;  // indicate port B has data input pins
  parameter  c_has_douta           = 1 ; // indicates port A has  output
  parameter  c_has_doutb           = 1 ; // indicates port B has  output
  parameter  c_has_ena             =  1 ; // indicates port A has a ENA pin
  parameter  c_has_enb             =  1 ; // indicates port B has a ENB pin
  parameter  c_has_limit_data_pitch       = 1 ;  // NOT IMPLEMENTED
  parameter  c_has_nda             = 0 ; //  Port A has a new data pin - NOT IMPLEMENTED
  parameter  c_has_ndb             = 0 ; //  Port B has a new data pin - NOT IMPLEMENTED
  parameter  c_has_rdya            = 0 ; //  Port A has result ready pin - NOT IMPLEMENTED
  parameter  c_has_rdyb            = 0 ; //  Port B has result ready pin - NOT IMPLEMENTED
  parameter  c_has_rfda            = 0 ; //  Port A has ready for data pin - NOT IMPLEMENTED
  parameter  c_has_rfdb            = 0 ; //  Port B has ready for data pin - NOT IMPLEMENTED
  parameter  c_has_sinita          =  1 ; // indicates port A has a SINITA pin - NOT IMPLEMENTED
  parameter  c_has_sinitb          =  1 ; // indicates port B has a SINITB pin - NOT IMPLEMENTED
  parameter  c_has_wea             =  1 ; // indicates port A has a WEA pin
  parameter  c_has_web             =  1 ; // indicates port B has a WEB pin
  parameter  c_limit_data_pitch           = 16 ; // NOT IMPLEMENTED
  parameter  c_mem_init_file     =  "null.mif";  // controls which .mif file used to initialize memory - NOTE: only works if c_addra_width >= c_addrb_width
  parameter  c_pipe_stages_a       =  0 ; // indicates the number of pipe stages needed in port A
  parameter  c_pipe_stages_b       =  0 ; // indicates the number of pipe stages needed in port B
  parameter  c_reg_inputsa         = 0 ; // indicates we, addr, and din of port A are registered
  parameter  c_reg_inputsb         = 0 ; // indicates we, addr, and din of port B are registered
  parameter  c_sinita_value        = "0000"; // indicates string of hex used to initialize A output registers - NOT IMPLEMENTED
  parameter  c_sinitb_value        = "0000"; // indicates string of hex used to initialize B output resisters - NOT IMPLEMENTED
  parameter  c_width_a             =  8 ;
  parameter  c_width_b             = 32 ;
  parameter  c_write_modea        = 2; // controls which write modes shall be used
  parameter  c_write_modeb        = 2; // controls which write modes shall be used

  // New Generics for Primitive Selection and Pin Polarity
   parameter c_ybottom_addr        = "1024"; // NOT IMPLEMENTED
   parameter c_yclka_is_rising     = 1; // controls the active edge of the CLKA Pin
   parameter c_yclkb_is_rising     = 1; // controls the active edge of the CLKB Pin
   parameter c_yena_is_high        = 1; // controls the polarity of the ENA Pin
   parameter c_yenb_is_high        = 1; // controls the polarity of the ENB Pin
   parameter c_yhierarchy          = "hierarchy1"; // NOT IMPLEMENTED
   parameter c_ymake_bmm           = 0; // NOT IMPLEMENTED
   parameter c_yprimitive_type     = "4kx4"; // Indicates which primitive should be used to build the
                                             // memory if c_yuse_single_primitive=1 - NOT IMPLEMENTED
   parameter c_ysinita_is_high     = 1; // controls the polarity of the SINITA Pin - NOT IMPLEMENTED
   parameter c_ysinitb_is_high     = 1; // controls the polarity of the SINITB Pin - NOT IMPLEMENTED
   parameter c_ytop_addr           = "0"; // NOT IMPLEMENTED
   parameter c_yuse_single_primitive = 0; // controls whether the Memory is build out of a
                                          // user selected primitive or is built from multiple
                                          // primitives with the "optimize for area" algorithm used - NOT IMPLEMENTED
   parameter c_ywea_is_high        = 1; // controls the polarity of the WEA Pin
   parameter c_yweb_is_high        = 1; // controls the polarity of the WEB Pin

   parameter c_yydisable_warnings  = 1; //1=no warnings, 0=print warnings - NOT IMPLEMENTED
 
// IO ports
    output [c_width_a-1:0] DOUTA;
    input [c_addra_width-1:0] ADDRA;
    input [c_width_a-1:0] DINA;
    input ENA, CLKA, WEA, SINITA, NDA;
    output RFDA, RDYA;
    output [c_width_b-1:0] DOUTB;
    input [c_addrb_width-1:0] ADDRB;
    input [c_width_b-1:0] DINB;
    input ENB, CLKB, WEB, SINITB, NDB;
    output RFDB, RDYB;

   // The memory cores
   // Only one of these will be used, depending on the variable widths of the
   // two ports.  If port B's data bus is wider, memb will be used, otherwise
   // mema will be used.
   
   reg [c_width_a-1:0] mema [0:c_depth_a-1];
   reg [c_width_b-1:0] memb [0:c_depth_b-1];

`protect

   // The idea here is to handle all the possible parameters, but in a way that Carbon's
   // optimization passes will eliminate all the unused stuff.  The uniquification of modules
   // based on parameters allows us to have fully-optimized modules even if there are different
   // instantiations with different configurations.

   // Some parameters for us to decide which memory we'll actually use, and in which
   // direction (if any) we need to munge data/address going in/out of the memory.

   parameter 	       USE_MEM_A = (c_width_a > c_width_b);
   parameter 	       USE_MEM_B = (c_width_a < c_width_b);
   parameter 	       USE_MEM_X = (c_width_a == c_width_b);   // no munging required
   
   // Enable readmemb
   initial
     if (c_mem_init_file != "")
       $readmemb(c_mem_init_file, mema);
   
   // Make internal versions of signals, which have taken into account the existance of
   // certain pins as well as their polarity

   wire 	       ena_int = c_has_ena ? (c_yena_is_high ? ENA : ~ENA) : 1'b1;
   wire 	       clka_int = c_yclka_is_rising ? CLKA : ~CLKA;
   wire 	       wea_i = c_has_wea ? (c_ywea_is_high ? WEA : ~WEA) : 1'b0;

   wire 	       enb_int = c_has_enb ? (c_yenb_is_high ? ENB : ~ENB) : 1'b1;
   wire 	       clkb_int = c_yclkb_is_rising ? CLKB : ~CLKB;
   wire 	       web_i = c_has_web ? (c_yweb_is_high ? WEB : ~WEB) : 1'b0;

   // Internal versions of address, data, and write enable can either be the values
   // on the input pins or registered versions.  Make the registered versions, and
   // select the correct signal based on the parameter

   reg [c_addra_width-1:0] addra_q;
   reg [c_width_a-1:0] 	   dina_q;
   reg 			   wea_q;
   reg [c_addrb_width-1:0] addrb_q;
   reg [c_width_b-1:0] 	   dinb_q;
   reg 			   web_q;

   always @(posedge clka_int)
     if (ena_int) begin
	addra_q <= ADDRA;
	dina_q <= DINA;
	wea_q <= wea_i;
     end

   always @(posedge clkb_int)
     if (enb_int) begin
	addrb_q <= ADDRB;
	dinb_q <= DINB;
	web_q <= web_i;
     end

   wire [c_addra_width-1:0] addra_int = c_reg_inputsa ? addra_q : ADDRA;
   wire [c_width_a-1:0]     dina_int = c_reg_inputsa ? dina_q : DINA;
   wire 		    wea_int = c_reg_inputsa ? wea_q : wea_i;

   wire [c_addrb_width-1:0] addrb_int = c_reg_inputsb ? addrb_q : ADDRB;
   wire [c_width_b-1:0]     dinb_int = c_reg_inputsb ? dinb_q : DINB;
   wire 		    web_int = c_reg_inputsb ? web_q : web_i;

   // Declare registers to hold output data

   reg [c_width_a-1:0] 	    douta_q;
   reg [c_width_b-1:0] 	    doutb_q;

   // Declare pipeline registers and fill

   reg [c_width_a-1:0] 	    douta_pipe[1:c_pipe_stages_a];
   reg [c_width_b-1:0] 	    doutb_pipe[1:c_pipe_stages_b];

   integer 		    i;

   always @(posedge clka_int)
     if (c_pipe_stages_a > 0) begin
	douta_pipe[1] <= douta_q;
	for (i = 2; i <= c_pipe_stages_a; i = i + 1)
	  douta_pipe[i] <= douta_pipe[i-1];
     end
   
   always @(posedge clkb_int)
     if (c_pipe_stages_b > 0) begin
	doutb_pipe[1] <= doutb_q;
	for (i = 2; i <= c_pipe_stages_b; i = i + 1)
	  doutb_pipe[i] <= doutb_pipe[i-1];
     end

   // Select the value to drive on the output ports
	
   assign 		    DOUTA = c_has_douta ? ((c_pipe_stages_a == 0) ? douta_q : douta_pipe[c_pipe_stages_a]) : {c_width_a{1'b0}};
   assign 		    DOUTB = c_has_doutb ? ((c_pipe_stages_b == 0) ? doutb_q : doutb_pipe[c_pipe_stages_b]) : {c_width_b{1'b0}};

   // Munged address/data for reading/writing internal memory when port widths
   // are different

   parameter 		    ADDRA_MEMB_BITS = USE_MEM_B ? (c_addra_width - c_addrb_width) : 1 /* unused - set to 1 to prevent bogus OOB warnings */ ;
   parameter 		    ADDRA_MEMA_BITS = USE_MEM_A ? (c_addrb_width - c_addra_width) : 1 /* unused - set to 1 to prevent bogus OOB warnings */ ;

   wire [c_addrb_width-1:0] addra_memb = USE_MEM_B ? (addra_int >> ADDRA_MEMB_BITS) : {c_addrb_width{1'b0}};
   wire [c_width_b-1:0]     dina_memb = USE_MEM_B ? (dina_int << (addra_int[ADDRA_MEMB_BITS - 1:0] * c_width_a)) : {c_width_b{1'b0}};
   wire [c_width_b-1:0]     mask_memb = USE_MEM_B ? ({c_width_a{1'b1}} << (addra_int[ADDRA_MEMB_BITS - 1:0] * c_width_a)) : {c_width_b{1'b0}};
   wire [c_width_a-1:0]     douta_memb = USE_MEM_B ? (memb[addra_memb] >> (addra_int[ADDRA_MEMB_BITS - 1:0] * c_width_a)) : {c_width_a{1'b0}};

   wire [c_addra_width-1:0] addrb_mema = USE_MEM_A ? (addrb_int >> (ADDRA_MEMA_BITS)) : {c_addra_width{1'b0}};
   wire [c_width_a-1:0]     dinb_mema = USE_MEM_A ? (dinb_int << (addrb_int[ADDRA_MEMA_BITS - 1:0] * c_width_b)) : {c_width_a{1'b0}};
   wire [c_width_a-1:0]     mask_mema = USE_MEM_A ? ({c_width_b{1'b1}} << (addrb_int[ADDRA_MEMA_BITS - 1:0] * c_width_b)) : {c_width_a{1'b0}};
   wire [c_width_b-1:0]     doutb_mema = USE_MEM_A ? (mema[addrb_mema] >> (addrb_int[ADDRA_MEMA_BITS - 1:0] * c_width_b)) : {c_width_b{1'b0}};

   // Implement Port A behavior
   
   always @(posedge clka_int)
     if (ena_int) begin
	if (wea_int) begin
	   // store the new read data
	   if (USE_MEM_B)
	     // munge the data/address
	     memb[addra_memb] <= (memb[addra_memb] & ~mask_memb) | (dina_memb & mask_memb);
	   else
	     // simple write
	     mema[addra_int] <= dina_int;

	   // update output
	   if (c_has_douta) begin
	      if (c_write_modea == `c_write_first)
		// propagate write data to output
		douta_q <= dina_int;
	      else if (c_write_modea == `c_read_first)
		// read old data - munge if necessary
		douta_q <= USE_MEM_B ? douta_memb : mema[addra_int];
	   end
	   
	end
	else if (c_has_douta)
	  // read data - munge if necessary
	  douta_q <= USE_MEM_B ? douta_memb : mema[addra_int];
     end
	  
   // Implement Port B behavior
   
   always @(posedge clkb_int)
     if (enb_int) begin
	if (web_int) begin
	   // store the new read data
	   if (USE_MEM_A)
	     // munge the data/address
	     mema[addrb_mema] <= (mema[addrb_mema] & ~mask_mema) | (dinb_mema & mask_mema);
	   else if (USE_MEM_B)
	     // simple write
	     memb[addrb_int] <= dinb_int;
	   else
	     // simple write
	     mema[addrb_int] <= dinb_int;

	   // update output
	   if (c_has_doutb) begin
	      if (c_write_modeb == `c_write_first)
		// propagate write data to output
		doutb_q <= dinb_int;
	      else if (c_write_modeb == `c_read_first)
		// read old data - munge if necessary
		doutb_q <= USE_MEM_A ? doutb_mema : (USE_MEM_B ? memb[addrb_int] : mema[addrb_int]);
	   end

	end
	else if (c_has_doutb)
	  // read data - munge if necessary
	  doutb_q <= USE_MEM_A ? doutb_mema : (USE_MEM_B ? memb[addrb_int] : mema[addrb_int]);
     end

   // Tie off unimplemented outputs
   assign RDYA = 1'b0;
   assign RDYB = 1'b0;
   assign RFDA = 1'b0;
   assign RFDB = 1'b0;
	  
`endprotect

endmodule
