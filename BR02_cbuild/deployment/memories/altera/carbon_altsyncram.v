// Copyright (C) 1988-2002 Altera Corporation
// Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
// support information,  device programming or simulation file,  and any other
// associated  documentation or information  provided by  Altera  or a partner
// under  Altera's   Megafunction   Partnership   Program  may  be  used  only
// to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
// other  use  of such  megafunction  design,  netlist,  support  information,
// device programming or simulation file,  or any other  related documentation
// or information  is prohibited  for  any  other purpose,  including, but not
// limited to  modification,  reverse engineering,  de-compiling, or use  with
// any other  silicon devices,  unless such use is  explicitly  licensed under
// a separate agreement with  Altera  or a megafunction partner.  Title to the
// intellectual property,  including patents,  copyrights,  trademarks,  trade
// secrets,  or maskworks,  embodied in any such megafunction design, netlist,
// support  information,  device programming or simulation file,  or any other
// related documentation or information provided by  Altera  or a megafunction
// partner, remains with Altera, the megafunction partner, or their respective
// licensors. No other licenses, including any licenses needed under any third
// party's intellectual property, are provided herein.


// Quartus II 4.1 Build 208 06/29/2004

// START_MODULE_NAME------------------------------------------------------------
//
// Module Name     : ALTSYNCRAM
//
// Description     : Synchronous ram model for Stratix series family
//
// Limitation      :
//
// END_MODULE_NAME--------------------------------------------------------------



// This is the Carbon implementation of the altsyncram memory
// Current limitations:
// 1. Read and write ports must be the same width
// 2. Clearing of registers isn't supported
// 3. All inputs on a given port must be registered on the same clock
// 4. rden_b is only used when port B output is registered
// 5. If byte enable width is 1, it is ignored (only wren is used)
//    (This is because the tri1/tri0 on unconnected inputs isn't implemented)

`ifdef USE_SUBSTITUTEMODULE
module carbon_altsyncram (
`else
module altsyncram (
`endif
                  wren_a,
                  wren_b,
                  rden_b,
                  data_a,
                  data_b,
                  address_a,
                  address_b,
                  clock0,
                  clock1,
                  clocken0,
                  clocken1,
                  aclr0,
                  aclr1,
                  byteena_a,
                  byteena_b,
                  addressstall_a,
                  addressstall_b,
                  q_a,
                  q_b
                  );

// GLOBAL PARAMETER DECLARATION

     // PORT A PARAMETERS
     parameter width_a          = 1;
     parameter widthad_a        = 1;
     parameter numwords_a       = 0;
     parameter outdata_reg_a    = "UNREGISTERED";
     parameter address_aclr_a   = "NONE";
     parameter outdata_aclr_a   = "NONE";
     parameter indata_aclr_a    = "NONE";
     parameter wrcontrol_aclr_a = "NONE";
     parameter byteena_aclr_a   = "NONE";
     parameter width_byteena_a  = 1;

     // PORT B PARAMETERS
     parameter width_b                   = 1;
     parameter widthad_b                 = 1;
     parameter numwords_b                = 0;
     parameter rdcontrol_reg_b           = "CLOCK1";
     parameter address_reg_b             = "CLOCK1";
     parameter outdata_reg_b             = "UNREGISTERED";
     parameter outdata_aclr_b            = "NONE";
     parameter rdcontrol_aclr_b          = "NONE";
     parameter indata_reg_b              = "CLOCK1";
     parameter wrcontrol_wraddress_reg_b = "CLOCK1";
     parameter byteena_reg_b             = "CLOCK1";
     parameter indata_aclr_b             = "NONE";
     parameter wrcontrol_aclr_b          = "NONE";
     parameter address_aclr_b            = "NONE";
     parameter byteena_aclr_b            = "NONE";
     parameter width_byteena_b           = 1;

     // STRATIX II RELATED PARAMETERS
     parameter clock_enable_input_a  = "NORMAL";
     parameter clock_enable_output_a = "NORMAL";
     parameter clock_enable_input_b  = "NORMAL";
     parameter clock_enable_output_b = "NORMAL";

     // GLOBAL PARAMETERS
     parameter operation_mode                     = "BIDIR_DUAL_PORT";
     parameter byte_size                          = 8;
     parameter read_during_write_mode_mixed_ports = "DONT_CARE";
     parameter ram_block_type                     = "AUTO";
     parameter init_file                          = "UNUSED";
     parameter init_file_layout                   = "UNUSED";
     parameter maximum_depth                      = 0;
     parameter intended_device_family             = "Stratix";

     parameter lpm_hint                           = "UNUSED";
     parameter lpm_type                           = "altsyncram";

     parameter cread_during_write_mode_mixed_ports =
               ((read_during_write_mode_mixed_ports == "DONT_CARE") ||
                (read_during_write_mode_mixed_ports == "UNUSED"))? "DONT_CARE":
                 read_during_write_mode_mixed_ports;


// INPUT PORT DECLARATION

    input  wren_a; // Port A write/read enable input
    input  wren_b; // Port B write enable input
    input  rden_b; // Port B read enable input
    input  [width_a-1:0] data_a; // Port A data input
    input  [width_b-1:0] data_b; // Port B data input
    input  [widthad_a-1:0] address_a; // Port A address input
    input  [widthad_b-1:0] address_b; // Port B address input

    // clock inputs on both ports and here are their usage
    // Port A -- 1. all input registers must be clocked by clock0.
    //           2. output register can be clocked by either clock0, clock1 or none.
    // Port B -- 1. all input registered must be clocked by either clock0 or clock1.
    //           2. output register can be clocked by either clock0, clock1 or none.
    input  clock0;
    input  clock1;

    // clock enable inputs and here are their usage
    // clocken0 -- can only be used for enabling clock0.
    // clocken1 -- can only be used for enabling clock1.
    input  clocken0;
    input  clocken1;

    // clear inputs on both ports and here are their usage
    // Port A -- 1. all input registers can only be cleared by clear0 or none.
    //           2. output register can be cleared by either clear0, clear1 or none.
    // Port B -- 1. all input registers can be cleared by clear0, clear1 or none.
    //           2. output register can be cleared by either clear0, clear1 or none.
    input  aclr0;
    input  aclr1;

    input [width_byteena_a-1:0] byteena_a; // Port A byte enable input
    input [width_byteena_b-1:0] byteena_b; // Port B byte enable input

    // Stratix II related ports
    input addressstall_a;
    input addressstall_b;



// OUTPUT PORT DECLARATION

    output [width_a-1:0] q_a; // Port A output
    output [width_b-1:0] q_b; // Port B output


// Carbon code follows...

   pullup(rden_b);
   pullup(clocken0);
   pullup(clocken1);
   
   reg [width_a-1:0] 	 mem_data [0:(1 << widthad_a) - 1];         // carbon exposeSignal

   // registered addresses and output data
   reg [widthad_a-1:0] address_a_r; // Port A address registered
   reg [widthad_b-1:0] address_b_r; // Port B address registered
   reg [width_a-1:0] q_a_r; // Port A output
   reg [width_b-1:0] q_b_r; // Port B output
   
   // outputs are either combinational or registered
   assign 	     q_a = (operation_mode == "DUAL_PORT") ?
			     0 : (((outdata_reg_a == "CLOCK0") ||
				   (outdata_reg_a == "CLOCK1")) ?
				  q_a_r : mem_data[address_a_r]);

   assign 	     q_b = ((operation_mode == "SINGLE_PORT") ||
			    (operation_mode == "ROM")) ?
			     0 : (((outdata_reg_b == "CLOCK0") ||
				   (outdata_reg_b == "CLOCK1")) ?
				  q_b_r : mem_data[address_b_r]);

   // write masks - only used if width_byteena_[ab] != 1
   reg [width_a-1:0] writemask_a;
   reg [width_b-1:0] writemask_b;
   integer 	     i;
   
   // clock 0 logic
   always @(posedge clock0) begin
      // Make sure clock 0 is enabled for port A inputs
      if ((clock_enable_input_a == "BYPASS") || clocken0) begin

	 // See if we're doing a write to port A
	 if ((operation_mode != "ROM") &&
	     wren_a) begin
	    // do we need a byte mask?
	    if (width_byteena_a == 1) begin
	       // no mask - unconditionally write
	       mem_data[address_a] <= data_a;
	    end
	    else if (width_byteena_a == width_a) begin
	       // use byteena input directly
	       mem_data[address_a] <= (data_a & byteena_a) | (mem_data[address_a] & ~byteena_a);
	    end
	    else begin
	       // need to build the full mask
	       for (i = 0; i < width_a; i = i + 1)
		 writemask_a[i] = byteena_a[i/byte_size];
	       mem_data[address_a] <= (data_a & writemask_a) | (mem_data[address_a] & ~writemask_a);
	    end
	 end
      end
      
      // Make sure clock 0 is enabled for port B inputs
      if ((clock_enable_input_b == "BYPASS") || clocken0) begin

	 // See if we're doing a write to port B
	 if ((operation_mode == "BIDIR_DUAL_PORT") &&
	     (wrcontrol_wraddress_reg_b == "CLOCK0") &&
	     wren_b) begin
	    // do we need a byte mask?
	    if (width_byteena_b == 1) begin
	       // no mask - unconditionally write
	       mem_data[address_b] <= data_b;
	    end
	    else if (width_byteena_b == width_b) begin
	       // use byteena input directly
	       mem_data[address_b] <= (data_b & byteena_b) | (mem_data[address_b] & ~byteena_b);
	    end
	    else begin
	       // need to build the full mask
	       for (i = 0; i < width_b; i = i + 1)
		 writemask_b[i] = byteena_b[i/byte_size];
	       mem_data[address_b] <= (data_b & writemask_b) | (mem_data[address_b] & ~writemask_b);
	    end
	 end
      end
      
      
      // See if we need to do a read from port A
      if (outdata_reg_a == "CLOCK0") begin
	 // Port A output is registered on this clock
	 if ((clock_enable_output_a == "BYPASS") || clocken0)
	   q_a_r <= mem_data[address_a];
      end
      else if ((outdata_reg_a == "UNREGISTERED") &&
	       ((clock_enable_input_a == "BYPASS") || clocken0))
	// Port A output is combinational, but we need to register
	// the address, and that happens on clock 0 (not configurable),
	// and clock 0 is enabled
	address_a_r <= address_a;

      // See if we need to do a read from port B
      if (outdata_reg_b == "CLOCK0") begin
	 // Port B output is registered on this clock
	 if (((clock_enable_output_b == "BYPASS") || clocken0) && rden_b)
	   q_b_r <= mem_data[address_b];
      end
      else if ((outdata_reg_b == "UNREGISTERED") &&
	       (address_reg_b == "CLOCK0") &&
	       ((clock_enable_input_b == "BYPASS") || clocken0))
	// Port B output is combinational, but we need to register
	// the address, and that happens on clock 0, and clock 0 is enabled
	address_b_r <= address_b;
   end
      
   // clock 1 logic
   always @(posedge clock1) begin

      // clock 1 can't be used for port A input

      // Make sure clock 1 is enabled for port B input
      if ((clock_enable_input_b == "BYPASS") || clocken1) begin
	 // See if we're doing a write to port B
	 if ((operation_mode == "BIDIR_DUAL_PORT") &&
	     (wrcontrol_wraddress_reg_b == "CLOCK1") &&
	     wren_b) begin
	    // do we need a byte mask?
	    if (width_byteena_b == 1) begin
	       // no mask - unconditionally write
	       mem_data[address_b] <= data_b;
	    end
	    else if (width_byteena_b == width_b) begin
	       // use byteena input directly
	       mem_data[address_b] <= (data_b & byteena_b) | (mem_data[address_b] & ~byteena_b);
	    end
	    else begin
	       // need to build the full mask
	       for (i = 0; i < width_b; i = i + 1)
		 writemask_b[i] = byteena_b[i/byte_size];
	       mem_data[address_b] <= (data_b & writemask_b) | (mem_data[address_b] & ~writemask_b);
	    end
	 end
      end

      // See if we need to do a read from port A
      if (outdata_reg_a == "CLOCK1") begin
	 // Port A output is registered on this clock
	 if ((clock_enable_output_a == "BYPASS") || clocken1)
	   q_a_r <= mem_data[address_a];
      end
      // Port A input can't be registered on clock 1
      
      // See if we need to do a read from port B
      if (outdata_reg_b == "CLOCK1") begin
	 // Port B output is registered on this clock
	 if (((clock_enable_output_b == "BYPASS") || clocken1) && rden_b)
	   q_b_r <= mem_data[address_b];
      end
      else if ((outdata_reg_b == "UNREGISTERED") &&
	       (address_reg_b == "CLOCK1") &&
	       ((clock_enable_input_b == "BYPASS") || clocken1))
	// Port B output is combinational, but we need to register
	// the address, and that happens on clock 1, and clock 1 is enabled
	address_b_r <= address_b;
   end

// dummy module to expose params (bug2724)
			  dummy dummy();
			  
endmodule


module dummy;
     wire width_a;                                  // carbon exposeSignal
     wire widthad_a;				    // carbon exposeSignal
     wire numwords_a;				    // carbon exposeSignal
     wire outdata_reg_a;			    // carbon exposeSignal
     wire address_aclr_a;			    // carbon exposeSignal
     wire outdata_aclr_a;			    // carbon exposeSignal
     wire indata_aclr_a;			    // carbon exposeSignal
     wire wrcontrol_aclr_a;			    // carbon exposeSignal
     wire byteena_aclr_a;			    // carbon exposeSignal
     wire width_byteena_a;			    // carbon exposeSignal
     wire width_b;				    // carbon exposeSignal
     wire widthad_b;				    // carbon exposeSignal
     wire numwords_b;				    // carbon exposeSignal
     wire rdcontrol_reg_b;			    // carbon exposeSignal
     wire address_reg_b;			    // carbon exposeSignal
     wire outdata_reg_b;			    // carbon exposeSignal
     wire outdata_aclr_b;			    // carbon exposeSignal
     wire rdcontrol_aclr_b;			    // carbon exposeSignal
     wire indata_reg_b;				    // carbon exposeSignal
     wire wrcontrol_wraddress_reg_b;		    // carbon exposeSignal
     wire byteena_reg_b;			    // carbon exposeSignal
     wire indata_aclr_b;			    // carbon exposeSignal
     wire wrcontrol_aclr_b;			    // carbon exposeSignal
     wire address_aclr_b;			    // carbon exposeSignal
     wire byteena_aclr_b;			    // carbon exposeSignal
     wire width_byteena_b;			    // carbon exposeSignal
     wire clock_enable_input_a;			    // carbon exposeSignal
     wire clock_enable_output_a;		    // carbon exposeSignal
     wire clock_enable_input_b;			    // carbon exposeSignal
     wire clock_enable_output_b;		    // carbon exposeSignal
     wire operation_mode;			    // carbon exposeSignal
     wire byte_size;				    // carbon exposeSignal
     wire read_during_write_mode_mixed_ports;	    // carbon exposeSignal
     wire ram_block_type;			    // carbon exposeSignal
     wire init_file;				    // carbon exposeSignal
     wire init_file_layout;			    // carbon exposeSignal
     wire maximum_depth;			    // carbon exposeSignal
     wire intended_device_family;		    // carbon exposeSignal
     wire lpm_hint;				    // carbon exposeSignal
     wire lpm_type;				    // carbon exposeSignal
     wire cread_during_write_mode_mixed_ports;      // carbon exposeSignal

endmodule
			  
