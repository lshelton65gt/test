#!/usr/bin/python2
#
#*****************************************************************************
#
# Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
#******************************************************************************

import os
import sys
import re

#
# Util function, needs to be added to a
# bit manipulation lib
def bitSize(x):
    assert x >= 0
    n = 0
    while x > 0:
        n = n+1
        x = x>>1
    return n



# TECHNOLOGY          : GFLXR
# MEMORY ARCHITECTURE : MF1R211RC : 2-PORT 1 READ 1 WRITE SYNCHRONOUS RAM.
# CONFIGURATION       : RCMEM20X72_1R1W_4WE_1CM -- 20 WORDS BY 72 BITS. BITS/BYTE = 18.  
# DESIGNER            : Memory Modeling Group


# MemoryModel defines the interface for manipulating vendor memory models
#
class MemoryModel:
    "Interface class for manipulating vendor memories"
    def readMemoryModel(self, fileName):
        raise "Called function on abstract base class"

class LSIMemoryArch:
    "Specific architecture base class"
    def mapPorts(self, ports):
        raise "Called function on abstract base class"

    def analyze(self, config):
        raise "Called function on abstract base class"

    def writeInstance(self, outfile):
        raise "Called function on abstract base class"
        

class LSIMemory211S(LSIMemoryArch):
    def __init__(self):
        "Initialize class"
        self.__addr_width = -1
        self.__data_width = -1
        self.__we_width = -1
        self.__group_width = -1
        self.__depth = -1
        self.__ports = []
        self.__instance_name = ""

    def analyze(self, config):
        "Determine memory size, etc. based on configuration string"
        configRE = re.compile(": (?P<instance>\w+) -- (?P<depth>\d+) WORDS BY (?P<width>\d+) BITS\. BITS/BYTE = (?P<group>\d+)")
        m = configRE.search(config)
        if m:
            self.__instance = m.group("instance")
            self.__depth = int(m.group("depth"))
            self.__data_width = int(m.group("width"))
            self.__group_width = int(m.group("group"))
            # determine required number of address bits
            self.__addr_width = bitSize(self.__depth)
            # determine we width, and make sure it's sane
            assert ((self.__data_width % self.__group_width) == 0)
            self.__we_width = self.__data_width / self.__group_width

    def mapPorts(self, ports):
        "Get list of port names for the original module declaration"
        # Must be 9 ports
        assert (len(ports) == 9)
        for p in ports:
            self.__ports.append(p)

    def writeInstance(self, outfile):
        "Write the instantiation of the Carbon memory to the output file"
        # TODO - correct instance name
        outfile.write("carbon_lsi_mem_211s #(%d, %d, %d) u0(" % (self.__addr_width, self.__data_width, self.__depth))
        # clka - from port 2
        outfile.write(".clka(%s),\n" % self.__ports[2][-1])
        # clkb - from port 3
        outfile.write("                                    .clkb(%s),\n" % self.__ports[3][-1])
        # adra - from port 0
        outfile.write("                                    .adra(%s),\n" % self.__ports[0][-1])
        # adrb - from port 1
        outfile.write("                                    .adrb(%s),\n" % self.__ports[1][-1])
        # dib - from port 4
        outfile.write("                                    .dib(%s),\n" % self.__ports[4][-1])
        # mea - from port 6
        outfile.write("                                    .mea(%s),\n" % self.__ports[6][-1])
        # meb - from port 7
        outfile.write("                                    .meb(%s),\n" % self.__ports[7][-1])
        # web/wemb - from port 5
        if (self.__we_width == 1):
            # If we_width is 1, connect it directly to enable and tie the mask to all 1's
            outfile.write("                                    .web(%s),\n" % self.__ports[5][-1])
            outfile.write("                                    .wemb({%d{1'b1}}),\n" % self.__data_width)
        else:
            # otherwise use reduction or to determine the enable
            outfile.write("                                    .web(|%s),\n" % self.__ports[5][-1])
            if (self.__we_width == self.__data_width):
                # if there's one bit per mask, we can connect the mask directly
                outfile.write("                                    .wemb(%s),\n" % self.__ports[5][-1])
            else:
                # here it gets tricky - we need to build the mask ourself from concats of the individual WE
                tmplist = []
                for i in range(0, self.__we_width):
                    tmplist.insert(0, "{%d{%s[%d]}}" % (self.__group_width, self.__ports[5][-1], i))
                outfile.write("                                    .wemb({%s}),\n" % ",".join(tmplist))
        # doa - from port 8
        outfile.write("                                    .doa(%s));\n" % self.__ports[8][-1])


# LSIMemory is the LSI-specific version of the memory model
class LSIMemory(MemoryModel):
    def __init__(self):
        "Initialize class"
        self.__name = ""
        self.__arch = LSIMemoryArch()
        self.__found_arch = 0
        self.__decl     = ""
        self.__ports    = []
        
    def readMemoryModel(self, fileName):
        "Read an LSI Verilog Memory model and get the data to map it to carbon"    
        memFile = open(fileName)
        print "Processing file ", fileName
        parsingComments = 1
        parsingModule = 0
        for line in memFile:
            if parsingComments:
                if line.find("MEMORY ARCHITECTURE") != -1:
                    # extract characteristics
                    archRE = re.compile("(?P<port>\d)-PORT (?P<read>\d) READ (?P<write>\d) WRITE (?P<sync>\w+) RAM")
                    m = archRE.search(line)
                    if m:
                        port = int(m.group("port"))
                        read = int(m.group("read"))
                        write = int(m.group("write"))
                        if (m.group("sync") == "SYNCHRONOUS"):
                            sync = 1
                        else:
                            sync = 0
                        # create the proper memory architecture
                        if ((port == 2) and (read == 1) and (write == 1) and (sync == 1)):
                            self.__arch = LSIMemory211S()
                            self.__found_arch = 1
                        else:
                            print "Unknown memory architecture in file", fileName
                            print line
                            memFile.close()
                            return 0
                    else:
                        print "Unknown memory architecture in file", fileName
                        print line
                        memFile.close()
                        return 0
                        
                elif line.find("CONFIGURATION") != -1:
                    # the newly-created architecture will know how to interpret this
                    self.__arch.analyze(line)
                elif line.find("module") != -1:
                    # make sure we found an architecture
                    if (not self.__found_arch):
                        print "No memory architecture found in file", fileName
                        memFile.close()
                        return 0
                    self.__name = "carbon_" + line.split()[1]
                    paren_pos = line.find("(")
                    self.__decl = "module " + self.__name + line[paren_pos:]
                    parsingComments = 0
                    parsingModule = 1
            elif parsingModule:
                if line.find("input") != -1 or line.find("output") != -1:
                    inPort = line.split()
                    if len(inPort) == 3:                        
                        self.__ports.append((inPort[0], inPort[1], inPort[2][:-1]))
                    else:
                        self.__ports.append((inPort[0], "", inPort[1][:-1]))
                elif line.find("endmodule") != -1:
                    parsingModule = 0

        # Tell architecture what ports we found
        self.__arch.mapPorts(self.__ports)
        return 1
                
    def writeFile(self, path):
        fileName = os.path.join(path, self.__name) + ".v"
        print "Writing file ", fileName
        outFile = file(fileName, "w")
        # Module header
        outFile.write(self.__decl)

        # Port declaration
        for port in self.__ports:
            outFile.write( "   " + " ".join(port) + ";\n")

        # instantiate Carbon Vendor Memory Lib
        self.__arch.writeInstance(outFile)

        #endmodlue
        outFile.write("endmodule\n")

    def getName(self):
        return self.__name


    
#
# Reads the memory model looking for key things that tell us the type
# of memory.  this routine is terribly inefficent as it reads much more of
# the file then it needs.
#
# the current implementation is looking for stuff tucked into the comments
#
# returns a map with the following Fields:
#
def scanMemory(fileName):
    foo = LSIMemory()
    if foo.readMemoryModel(fileName):
        return foo

    return None
 
def main():
    if (len(sys.argv) != 3):
        print "usage: %s <src dir> <dest dir>" % sys.argv[0]
        sys.exit(1)
        
    memDirFiles =  os.listdir(sys.argv[1])
    memories = []
    for file in memDirFiles:
        if file[-2:] == ".v":
            mem = scanMemory(sys.argv[1] + "/" + file)
            if (mem):
                memories.append(mem)

    # write a directives file with the proper substituteModule directives
    dirfile = open(os.path.join(sys.argv[2], "mem.dir"), "w")
    for mem in memories:
        mem.writeFile(sys.argv[2])
        dirfile.write("substituteModule %s %s\n" % (mem.getName()[7:], mem.getName()))

    dirfile.close()


##
## End main()
##
        
main()
