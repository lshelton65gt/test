#!/usr/bin/python2
#
# Copyright Carbon Design Systems 2004
import os
import sys

#
# Util function, needs to be added to a
# bit manipulation lib
def bitSize(x):
    assert x >= 0
    n = 0
    while x > 0:
        n = n+1
        x = x>>1
    return n



# //   --------------------------------------------------------------     
# //                       Template Revision : 2.2.2                      
# //   --------------------------------------------------------------     
# //                      * Synchronous, 2-Port SRAM *                  
# //                      * Verilog Behavioral Model *                  
# //                THIS IS A SYNCHRONOUS 2-PORT MEMORY MODEL           
# //                                                                    
# //   Memory Name:rf_b2_128x64                                         
# //   Memory Size:128 words x 64 bits                                  


# MemoryModel defines the interface for manipulating vendor memory models
#
class MemoryModel:
    "Interface class for manipulating vendor memories"
    def readMemoryModel(fileName):
        raise "Called function on abstract base class"

# VirageMemory is the Virage specific version of the memory model
class VirageMemory(MemoryModel):
    def __init__(self):
        "Initialize class"
        self.__compilerName = "UNKNOWN"
        self.__synchronous = 0
        self.__numPorts = -1
        self.__wordSize = -1
        self.__bitSize  = -1
        self.__name     = "UNKNOWN"
        self.__ports    = []
        
    def __str__(self):
        t = "Virage Memory Model %s %d-Port %sSYNCHRONOUS %dx%d.  Name: %s" % (self.__compilerName,
                                                                               self.__numPorts,
                                                                               self.__synchronous,
                                                                               self.__wordSize,
                                                                               self.__bitSize,
                                                                               self.__name)
        t = t + "\nPORTS:"
        for port in self.__ports:
            t = t + " ".join(port) + "\n"
        return t
    
    def readMemoryModel(self, fileName):
        "Read a Virage Verilog Memory model and get the data to map it to carbon"    
        memFile = open(fileName)
        parsingModule = 0
        for line in memFile:
            if parsingModule:
                if line.find("input") != -1 or line.find("output") != -1:
                    inPort = line.split()
                    if len(inPort) == 3:                        
                        self.__ports.append((inPort[0], inPort[1], inPort[2][:-1]))
                    else:
                        self.__ports.append((inPort[0], "", inPort[1][:-1]))
                elif line.find("endmodule") != -1:
                    parsingModule = 0
            elif line.find("module " + self.__name) != -1:
                parsingModule = 1
            elif line.find("Compiler Name") != -1:
                self.__compilerName = line.split()[4]
            elif line.find("THIS IS A") != -1:
                self.__synchronous = (line.split()[4] == "SYNCHRONOUS")
                self.__numPorts = int(line.split()[5][:-5])
            elif line.find("Memory Size") != -1:
                self.__wordSize = int(line.split()[2][5:])
                self.__bitSize  = int(line.split()[5])
            elif line.find("Memory Name:") != -1:
                self.__name = line.split()[2][5:]
                
    def writeFile(self, path):
        outFile = file(path + self.__name + ".v", "w")
        # Module header
        outFile.write( "module %s (" % (self.__name))
        portList = []
        shellHasMask = 0
        for port in self.__ports:
            portList.append(port[-1])
            # look for a WEM port 
            if port[-1].find("WEM") != -1:
                shellHasMask = 1
        
        outFile.write( ",".join(portList) + ");\n")

        # Port declaration
        for port in self.__ports:
            outFile.write( "   " + " ".join(port) + ";\n")

        # instantiate Carbon Vendor Memory Lib
        addrSize = bitSize(self.__wordSize - 1)
        param = " #(%d,%d,%d) " % (self.__bitSize, self.__wordSize, addrSize)
        outFile.write("\n   " + self.__compilerName + param + " mem(\n                ")
        portConnections = []
        for port in self.__ports:
            portConnections.append("." + port[-1] + "(" + port[-1] + ")")

        # all of the core libs have a bitmask, tie them off if this
        # wrapper does not
        if not shellHasMask:
            if self.__numPorts == 1:
                portConnections.append(".WEM( {%d{1'b1}} )" % self.__bitSize)
            else:
                # 2 ports, gotta find is there's a B write port
                portConnections.append(".WEMA( {%d{1'b1}} )" % self.__bitSize)
                if "DB" in portList:
                    portConnections.append(".WEMB( {%d{1'b1}} )" % self.__bitSize)
            

        outFile.write(",\n                ".join(portConnections) + " );\n")

        #endmodlue
        outFile.write("endmodule\n")

        

        
        
    
#
# Reads the memory model looking for key things that tell us the type
# of memory.  this routine is terribly inefficent as it reads much more of
# the file then it needs.
#
# the current implementation is looking for stuff tucked into the comments
#
# returns a map with the following Fields:
#
def scanMemory(fileName):
    foo = VirageMemory()
    foo.readMemoryModel(fileName)
    return foo
 
def main():
    print sys.argv[1], sys.argv[2]
    print "ok?"
    sys.stdin.readline()
    print "running"
    
    memDirFiles =  os.listdir(sys.argv[1])
    memories = []
    for file in memDirFiles:
        if file[-2:] == ".v":
            memories.append(scanMemory(sys.argv[1] + "/" + file))

    for mem in memories:
        mem.writeFile(sys.argv[2]);

##
## End main()
##
        
main()
