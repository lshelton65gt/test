#!/usr/bin/python2

# Util function, needs to be added to a
# bit manipulation lib
def bitSize(x):
    assert x >= 0
    n = 0
    while x > 0:
        n = n+1
        x = x>>1
    return n

class MemoryModel:
    "Interface class for manipulating vendor memories"
    def readMemoryModel(fileName):
        raise "Called function on abstract base class"
#
# This script parses out ibm memories and creates a
# wrapper for the carbon equivalent
#

import re
import os
import sys
# Ibm version of memory model class
class IbmMemory(MemoryModel):
    def __init__(self):
        "Initialize class"
        self.__memType = ""
        self.__width = -1
        self.__depth = -1
        self.__ports = -1
        self.__writePorts = -1
        self.__readPorts  = -1
        self.__timingMode = -1
        self.__decode = -1
        self.__enables = "none"
        self.__fileName = ""
        self.__name = ""
        self.__valid = 0
        self.__addrWidth = -1
        self.__enableChar = ''
        
    def __str__(self):
        t = "UNKNOWN MEMORY TYPE"
        if self.__valid:
            t = "IBM Memory Model %s %d-Port %dx%d %s." % (self.__fileName, self.__ports,
                                                           self.__depth, self.__width,
                                                           self.__memType)
        
        return t
 
    def readMemoryModel(self, fileName):
        "Read an IBM Verilog Memory model and get the data to map it to carbon"    
        # Just parse out the filename to get the memory type
        (path, self.__fileName) = os.path.split(fileName)
        # memFile must be of form
        # WRAP<num>[ABC]_RA<depth>X<width>D<decode>P<ports>W<write>R<read>M<mode>.v
        regArrayRe = re.compile("WRAP\d(?P<type>[ABC])_RA(?P<depth>\d+)X(?P<width>\d+)D(?P<decode>\d)P(?P<ports>\d)W(?P<write>\d)R(?P<read>\d)M(?P<timing>\d)\.v")
        sramRe = re.compile("WRAP\d(?P<type>[ABC])_SRAM(?P<ports>[12])\w(?P<depth>\d+)X(?P<width>\d+)D(?P<decode>\d+)S\d+M(?P<timing>\d)")
        m = regArrayRe.match(self.__fileName)        
        if m:
            self.__collectRegArrayParam(m)
            return

        m = sramRe.match(self.__fileName)
#        print m
        if m:
            self.__collectSramParam(m)
            return
        
        
    
    def writeFile(self, path):
        # outFile = file(path + self.__name + ".v", "w")
        # Module header

        # create a port list
        # Port list for RA wrappers
        ports = []
        if self.__memType == "RA":
            # port list is CLOCKPx, WEPx, BW_WPx, DI_WPx, DO_RPx, A_RPx, AWPx
            for x in range(0,self.__writePorts):
                ports.append(("input", "", "CLOCKP%d"%(x)))
            for x in range(0,self.__writePorts):
                ports.append(("input", "", "WEP%d" % (x)))
            if self.__enables == "bit":
                for x in range(0,self.__writePorts):
                    ports.append(("input", "[%d:0]" % (self.__width - 1), "BW_WP%d" % (x)))
            for x in range(0,self.__writePorts):
                ports.append(("input", "[%d:0]" % (self.__width - 1), "DI_WP%d" % (x)))
            for x in range(0,self.__readPorts):
                ports.append(("output", "[%d:0]" % (self.__width - 1), "DO_RP%d" % (x)))
            for x in range(0,self.__readPorts):
                ports.append(("output", "[%d:0]" % (self.__addrWidth - 1), "A_RP%d" % (x)))
            for x in range(0,self.__writePorts):
                ports.append(("output", "[%d:0]" % (self.__addrWidth - 1), "A_WP%d" % (x)))

            modelName = "ibm_mem_%s_%s_%dw%dr" % (self.__enableChar,
                                                  self.__memType,
                                                  self.__writePorts,
                                                  self.__readPorts)

        elif self.__memType == "SRAM":
            # port list is ARYSEL_Px, CLOCK_Px, RDWRT_Px, DIN_Px, DOUT_Px, A_Px
            for x in range(0,self.__ports):
                ports.append(("input", "", "ARYSEL_P%d" % x))
            for x in range(0,self.__ports):
                ports.append(("input", "", "CLOCK_P%d" % x))
            for x in range(0,self.__ports):
                ports.append(("input", "", "RDWRT_P%d" % x))
            for x in range(0,self.__ports):
                ports.append(("input",  "[%d:0]" % (self.__width - 1), "DIN_P%d" % x))
            for x in range(0,self.__ports):
                ports.append(("output", "[%d:0]" % (self.__width - 1), "DOUT_P%d" % x))
            for x in range(0,self.__ports):
                ports.append(("input", "[%d:0]" % (self.__addrWidth - 1), "A_P%d" % x))

            modelName = "ibm_mem_%s_%s_%d" % (self.__enableChar,
                                              self.__memType,
                                              self.__ports)

#        for p in ports:
#            print p

        outFile = open( os.path.join(path, self.__fileName), "w" )
        outFile.write("module %s (" % self.__name)
        # module declaration
        portNames = []
        for p in ports:
            portNames.append(p[2])
        outFile.write("%s);\n" % ",".join(portNames))
        # port declarations
        for p in ports:
            outFile.write("   %s   %s   %s;\n" % p)
        
        # instantiate carbon lib
        portInst = []        
        for p in ports:
            portInst.append( ".%s (%s)" % (p[2], p[2]) )

        parameters = "#(%d, %d, %d)" % (self.__addrWidth, self.__width, self.__depth)
        outFile.write("\n   %s %s io (" % (modelName, parameters))
        outFile.write(",\n      ".join(portInst))
        outFile.write(");\n")

        
        # end
        outFile.write("endmodule\n");
    def __collectRegArrayParam(self, m):
        "Uses regex match data to populate data structure"
        self.__memType = "RA"
        enType = m.group('type')
        if enType == 'A':
            self.__enables = "bit"
        elif enType == 'B':
            self.__enables = "byte"
        elif enType == 'C':
            self.__enables = "none"

        self.__enableChar = enType
        self.__depth      = int(m.group('depth'))
        self.__addrWidth  = int(bitSize(self.__depth - 1))
        self.__width      = int(m.group('width'))
        self.__decode     = int(m.group('decode'))
        self.__ports      = int(m.group('ports'))
        self.__writePorts = int(m.group('write'))
        self.__readPorts  = int(m.group('read'))
        self.__timingMode = int(m.group('timing'))
        self.__valid      = 1
        self.__name       = self.__fileName[:-2]    # remove ".v"
    def __collectSramParam(self, m):
        "Uses regex match data to populate data structure"
        self.__memType = "SRAM"
        enType = m.group('type')
        if enType == 'A':
            self.__enables = "bit"
        elif enType == 'B':
            self.__enables = "byte"
        elif enType == 'C':
            self.__enables = "none"

        self.__enableChar = enType
        self.__depth      = int(m.group('depth'))
        self.__addrWidth  = int(bitSize(self.__depth - 1))
        self.__width      = int(m.group('width'))
        self.__decode     = int(m.group('decode'))
        self.__ports      = int(m.group('ports'))
        self.__writePorts = int(m.group('ports'))
        self.__readPorts  = int(m.group('ports'))
        self.__timingMode = int(m.group('timing'))
        self.__valid      = 1
        self.__name       = self.__fileName[:-2]    # remove ".v"

def scanMemory(fileName):
    foo = IbmMemory()
    foo.readMemoryModel(fileName)
    return foo
 
def main():
    print " Creating IBM memory wrappers "
    print " Reading from:", sys.argv[1]
    print " Writing to:", sys.argv[2]
    print " Press <ENTER> key to continue or ^C to break..."
    sys.stdin.readline()
    print " Working... "
    
    memDirFiles =  os.listdir(sys.argv[1])

    memories = []
    for file in memDirFiles:
        if file[-2:] == ".v":
            memories.append(scanMemory(os.path.join(sys.argv[1], file)))

    for mem in memories:
        # print mem
        mem.writeFile(sys.argv[2]);

    
##
## End main()
##
        
main()
