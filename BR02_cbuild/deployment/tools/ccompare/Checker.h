// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CHECKER_H__
#define __CHECKER_H__

#include "util/AtomicCache.h"
#include "util/DynBitVector.h"
#include "util/UtUInt64Factory.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtList.h"

class RuleFile;
class Signal;
class SignalRule;
class WfAbsFileReader;
class UtOFStream;

// class to check rules (i.e. do the real work)
class Checker
{
public:
    Checker(RuleFile * const rf);
    ~Checker();

//     void setRules(RuleFile *rf);
    CarbonTimescale getTimescale() { return mTimeScale; }

    bool getSignals();
    bool loadValues();
    bool compare();
    UInt32 getNumErrors() const { return mNumErrors; }
    UInt32 getNumSignals() const { return mNumSignals; }
    
    void print() const;

protected:
    RuleFile * const mRuleFile;
    CarbonTimescale mTimeScale;

    WfAbsFileReader *mGoldenWf, *mCarbonWf;
    UtOFStream *mReport;

    // Map of all the signals we've found already, so we don't look anything up twice
    UtHashMap<UtString, Signal *> mSigMap;

    // List of all the SignalRules
    // This is actually just a list of the continuous compare rules (clocks).
    // Each continuous compare rule will have an internal list of its signal's
    // clocked compare rules
    UtList<SignalRule *> mContSigRules;

    UInt32 mNumErrors;
    UInt32 mNumSignals;

    // Don't know what these are, but the file reader needs 'em!
    AtomicCache mStringCache;
    DynBitVectorFactory mDynFactory;
    UtUInt64Factory mTimeFactory;
};

#endif
