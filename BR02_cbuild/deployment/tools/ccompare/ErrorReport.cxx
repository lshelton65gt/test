// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "ErrorReport.h"
#include "util/UtIOStream.h"
#include "util/UtString.h"
#include "util/UtPair.h"

UInt32 ErrorMsg::sGlobalIndex = 0;

ErrorReport::ErrorReport(UtOStream &out)
    : mOut(out)
{
}

ErrorReport::~ErrorReport()
{
}

void ErrorReport::addContError(const UtString &g_sig,
			       UInt64 g_time,
			       const UtString &g_val,
			       const UtString &c_sig,
			       UInt64 c_time,
			       const UtString &c_val)
{
    // For now, just dump immediately, but eventually we want to
    // save and sort these by time.
    //    mOut << "Error: Golden signal " << g_sig << " (@" << UtIO::dec << g_time << " - " << g_val << ") != Carbon signal " << c_sig << " (@" << c_time << " - " << c_val << ")" << UtIO::endl;

    UtString* err = new UtString;
    UtOStringStream stm(err);
    stm << "Error: Golden signal " << g_sig << " (@" << UtIO::dec << g_time << " - " << g_val << ") != Carbon signal " << c_sig << " (@" << c_time << " - " << c_val << ")" << UtIO::endl;
    ErrorMsg* msg = new ErrorMsg(std::min(g_time, c_time), err);
    mErrors.insert(msg);
}

void ErrorReport::addExtraClockError(const UtString &sig1,
				     UInt64 time1,
				     const UtString &sig2,
				     UInt64 time2)
{
    //    mOut << "Error: Clock " << sig1 << " (@" << UtIO::dec << time1 << ") has more edge than " << sig2 << " (@" << time2 << ")" << UtIO::endl;

    UtString* err = new UtString;
    UtOStringStream stm(err);
    stm << "Error: Clock " << sig1 << " (@" << UtIO::dec << time1 << ") has more transitions than " << sig2 << " (@" << time2 << ")" << UtIO::endl;
    ErrorMsg* msg = new ErrorMsg(std::max(time1, time2), err);	// clock error should be at the greater of the two times
    mErrors.insert(msg);
}

void ErrorReport::addTolError(const UtString &sig1,
			      UInt64 time1,
			      const UtString &sig2,
			      UInt64 time2)
{
    //    mOut << "Error: Clock edge of " << sig1 << " (@" << UtIO::dec << time1 << ") is too far from clock edge of " << sig2 << " (@" << time2 << ")" << UtIO::endl;
    UtString* err = new UtString;
    UtOStringStream stm(err);
    stm << "Error: Transition of " << sig1 << " (@" << UtIO::dec << time1 << ") is too far from transition of " << sig2 << " (@" << time2 << ")" << UtIO::endl;
    ErrorMsg* msg = new ErrorMsg(std::min(time1, time2), err);
    mErrors.insert(msg);
}

void ErrorReport::addClockedError()
{
    // Need to implement this!
}

void ErrorReport::writeReport()
{
    for (ErrorHashSet::SortedLoop iter = mErrors.loopSorted(); !iter.atEnd(); ++iter) {
	mOut << *(*iter)->mMsg;
	delete (*iter)->mMsg;
	delete (*iter);
    }
}
