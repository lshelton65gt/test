// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "SignalRule.h"
#include "Signal.h"
#include "CompareRule.h"
#include "ContCompareRule.h"
#include "ClockedCompareRule.h"
#include "ErrorReport.h"
#include "util/UtIOStream.h"

SignalRule::SignalRule(Signal *sig, const CompareRule *rule)
    : mSig(sig),
      mRule(rule)
{
}

SignalRule::~SignalRule()
{
    for (UtList<SignalRule *>::iterator iter = mClockedSigRules.begin(); iter != mClockedSigRules.end(); ++iter)
	delete *iter;
}

void SignalRule::addSignalRule(SignalRule *sr)
{
    mClockedSigRules.push_back(sr);
}

UInt32 SignalRule::compare(ErrorReport *report)
{
    UInt32 errors = 0;
    // Compare the clock values
    
    // Start at the start time
    mSig->gotoGTime(mRule->getStartTime());
    mSig->gotoCTime(mRule->getStartTime());
    if (!mSig->compareValues(report))
	// Always quit immediately on clock errors
	return 1;

    // If there are no transitions, we're done
    if (mSig->atMaxGTime() && mSig->atMaxCTime())
	return 0;

    bool done = false;
    while (!done) {
	// Advance to the next clock edge
	mSig->nextGEdge();
	bool posedge = mSig->nextCEdge();

	UInt64 gtime = mSig->getGTime();
	UInt64 ctime = mSig->getCTime();

	// Are we too far?
	if ((gtime > mRule->getEndTime()) ||(ctime > mRule->getEndTime())) {
	    // Go back to the end time, and just do a value comparison
	    mSig->gotoGTime(mRule->getEndTime());
	    mSig->gotoCTime(mRule->getEndTime());
	    if (!mSig->compareValues(report))
		return (errors + 1);
	    done = true;
	} else {
	    // Check for extra edges
	    if (mSig->atMaxGTime() && !mSig->atMaxCTime()) {
		report->addExtraClockError(mSig->getCarbonName(), ctime, mSig->getGoldenName(), gtime);
		return (errors + 1);
	    }
	    if (mSig->atMaxCTime() && !mSig->atMaxGTime()) {
		report->addExtraClockError(mSig->getGoldenName(), gtime, mSig->getCarbonName(), ctime);
		return (errors + 1);
	    }

	    // Check tolerance
	    if (abs(gtime - ctime) > (dynamic_cast<const ContCompareRule *>(mRule))->getTol()) {
		report->addTolError(mSig->getGoldenName(), gtime, mSig->getCarbonName(), ctime);
		return (errors + 1);
	    }

	    // Check value
	    if (!mSig->compareValues(report))
		return (errors + 1);

	    // Run clocked compares for all signals to be checked on this clock
	    // Only do this on the posedge, though (that's what the other comparison tools do)
	    if (posedge) {
		UtList<SignalRule *>::iterator iter = mClockedSigRules.begin();
		while (iter != mClockedSigRules.end()) {
		    // Make sure we're within range
		
		    if ((gtime >= (*iter)->getRule()->getStartTime()) &&
			(gtime <= (*iter)->getRule()->getEndTime()) &&
			(ctime >= (*iter)->getRule()->getStartTime()) &&
			(ctime <= (*iter)->getRule()->getEndTime())) {

			// compare, considering sample offset
			if (!(*iter)->getSignal()->compareValuesAtTime(report,
								       gtime + (dynamic_cast<const ClockedCompareRule*>((*iter)->getRule()))->getGoldenSample(),
								       ctime + (dynamic_cast<const ClockedCompareRule*>((*iter)->getRule()))->getCarbonSample())) {
			    // Update the error count, and stop checking this rule
			    ++errors;
			    iter = mClockedSigRules.erase(iter);
			} else
			    ++iter;
		    } else
			++iter;
		}
	    }
	    
	    // If we hit the end, we're done.
	    // We only have to check once, because we've already made sure that
	    // the end times aren't mismatched.
	    if (mSig->atMaxGTime())
		done = true;
	}
    }

    return errors;
}

UInt32 SignalRule::getNumSignals()
{
    // number of clocked compares, plus ourself
    return (mClockedSigRules.size() + 1);
}

void SignalRule::print() const
{
    UtIO::cout() << "SignalRule" << UtIO::endl;
    mRule->print();
    if (!mClockedSigRules.empty()) {
	UtIO::cout() << "Begin Clocked Signal Rules" << UtIO::endl;
	for (UtList<SignalRule *>::const_iterator iter = mClockedSigRules.begin(); iter != mClockedSigRules.end(); ++iter)
	    (*iter)->print();
	UtIO::cout() << "End Clocked Signal Rules" << UtIO::endl;
    }
}
