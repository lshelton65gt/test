// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "NCompareRuleFile.h"
#include "HierContCompareRule.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"

NCompareRuleFile::NCompareRuleFile(const char *filename)
    : RuleFile(filename)
{
}

NCompareRuleFile::~NCompareRuleFile()
{
}

bool NCompareRuleFile::readFile()
{
    mFile = fopen(mRuleFileName.c_str(), "r");
    if (!mFile) {
	UtIO::cerr() << "Couldn't open file " << mRuleFileName << UtIO::endl;
	return false;
    }

    // NOTE - only absolute compares are supported for now!
    // It should be easy to add support for clocked compares if necessary
    // (The rule structure is set up for it, and the comparescan interface
    // does it already), but I think we're moving away from that so I'm not
    // going to bother.

    // default values until overridden
    UInt64 start = 0;
    UInt64 end = CompareRule::scMaxTime;
    UInt32 tol = 0;

    // Start parsing, line by line
    // Each line will be split into whitespace-delimited tokens
    UtVector<UtString> tokens;
    while (getTokens(&tokens)) {
	if (!tokens.empty()) {
	    // Ignore comments
	    if (tokens[0][0] == '#')
		continue;

	    // Check for golden and Carbon wavefiles
	    if ((tokens.size() == 3) && (tokens[0] == "cmpOpenFsdb")) {
		mGoldenFileName = tokens[1];
		mCarbonFileName = tokens[2];
	    }

	    // Report file
	    if ((tokens.size() == 2) && (tokens[0] == "cmpSetReport"))
		mReportFileName = tokens[1];

	    // Get comparison options
	    if (tokens[0] == "cmpSetCmpOption") {
		for (unsigned int i = 1; i < tokens.size(); ++i) {
		    if (tokens[i] == "-Tolerance") {
			++i;
			if (i < tokens.size())
			    tol = convertTime(tokens[i]);
		    }
		    if (tokens[i] == "-Time") {
			++i;
			// This one's a pain.  The time values may be in one or two tokens, depending
			// on whether there's a space after the comma.  Also, either (both?) of the
			// values may be empty, meaning that the min/max time should be used
			// So, we can see things like
			// -Time (250ns,)
			// -Time (250ns, 26000ns)
			// -Time (250ns,26000ns)
			// -Time (,26000ns)
			UtString timestr = tokens[i];
			// if this ends in a comma, join with the next one
			if (timestr[timestr.length()-1] == ',')
			    timestr += tokens[++i];

			// find the delimeters
			UtString::size_type openparen = timestr.find('(');
			UtString::size_type comma = timestr.find(',');
			UtString::size_type closeparen = timestr.find(')');
			if (!((comma > openparen) && (closeparen > comma))) {
			    UtIO::cerr() << "Warning: unknown time format " << timestr << UtIO::endl;
			    continue;
			}

			// Set the times - empty means min/max
			if (comma == (openparen + 1))
			    start = 0;
			else
			    start = convertTime(timestr.substr(openparen + 1, comma - openparen - 1));
			if (closeparen == (comma + 1))
			    end = CompareRule::scMaxTime;
			else
			    end = convertTime(timestr.substr(comma + 1, closeparen - comma - 1));
		    }
		}
	    }

	    // Get compare signals
	    if (tokens[0] == "cmpSetSignalPair") {
		// For now, we only support the following format:
		// cmpSetSignalPair GoldenHier [SecondaryHier] -level N

		if ((tokens.size() < 4) || (tokens.size() > 5)) {
		    UtIO::cerr() << "Warning: unknown rule file syntax - tokens are" << UtIO::endl;
		    for (unsigned int i = 1; i < tokens.size(); ++i)
			UtIO::cerr() << "    " << tokens[i] << UtIO::endl;
		    continue;
		}

		// Need to do things a little differently depending on whether there's
		// a secondary hierarchy
		UtString goldhier, carbonhier;
		UInt32 level;

		if (tokens.size() == 4) {
		    if (tokens[2] != "-level") {
			UtIO::cerr() << "Warning: unknown rule file syntax - tokens are" << UtIO::endl;
			for (unsigned int i = 1; i < tokens.size(); ++i)
			    UtIO::cerr() << "    " << tokens[i] << UtIO::endl;
			continue;
		    }
		    goldhier = tokens[1];
		    carbonhier = tokens[1];
		    level = atoi(tokens[3].c_str());
		} else {
		    if (tokens[3] != "-level") {
			UtIO::cerr() << "Warning: unknown rule file syntax - tokens are" << UtIO::endl;
			for (unsigned int i = 1; i < tokens.size(); ++i)
			    UtIO::cerr() << "    " << tokens[i] << UtIO::endl;
			continue;
		    }
		    goldhier = tokens[1];
		    carbonhier = tokens[2];
		    level = atoi(tokens[4].c_str());
		}

		// Strip off the top module name to get the rest of the hierarchical name
		// If there's no lower hierarchy, set it to the empty string
		char empty = 0;
		char *name = strchr(carbonhier.c_str(), '.');
		if (!name)
		    name = &empty;

		// If this is the first rule, extract the golden and carbon prefixes
		if (mRuleList.empty()) {
		    if (*name == 0) {
			mGoldenPrefix = goldhier;
			mCarbonPrefix = carbonhier;
		    } else {
			mGoldenPrefix.assign(goldhier, 0, goldhier.find(name));
			mCarbonPrefix.assign(carbonhier, 0, carbonhier.find(name));
		    }
		}

		if (*name == '.')
		    ++name;		// skip the .

		mRuleList.push_back(new HierContCompareRule(name, level, start, end, tol));
	    }
	}
    }

    fclose(mFile);
    mFile = 0;

    // Some sanity checks
    if (mReportFileName == "") {
	mReportFileName = "ccompare.nce";
	UtIO::cerr() << "Warning: No report file found, using " << mReportFileName << UtIO::endl;
    }

    return true;
}
