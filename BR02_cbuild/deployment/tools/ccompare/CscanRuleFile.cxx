// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CscanRuleFile.h"
#include "ContCompareRule.h"
#include "ClockedCompareRule.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"

CscanRuleFile::CscanRuleFile(const char *filename)
    : RuleFile(filename)
{
}

CscanRuleFile::~CscanRuleFile()
{
}

bool CscanRuleFile::readFile()
{
    mFile = fopen(mRuleFileName.c_str(), "r");
    if (!mFile) {
	UtIO::cerr() << "Couldn't open file " << mRuleFileName << UtIO::endl;
	return false;
    }

    // Declare a vector to store our clock definitions
    UtVector<UtString> clock_defs;
    clock_defs.resize(10);	// will resize later if necessary

    // Start parsing, line by line
    // Each line will be split into whitespace-delimited tokens
    UtVector<UtString> tokens;
    while (getTokens(&tokens)) {
	if (!tokens.empty()) {
	    // Ignore comments
	    if (tokens[0][0] == '#')
		continue;

	    // Check for golden and Carbon wavefiles
	    if ((tokens.size() >= 2) && (tokens[0] == "datafile1"))
		mGoldenFileName = tokens[1];
	    if ((tokens.size() >= 2) && (tokens[0] == "datafile2"))
		mCarbonFileName = tokens[1];

	    // Report file
	    if ((tokens.size() == 4) && (tokens[0] == "report"))
		mReportFileName = tokens[3];

	    // Get compare rules
	    if (tokens[0] == "compare") {
		UInt64 start = 0;
		UInt64 end = CompareRule::scMaxTime;
		UInt32 tol = 0;

		// Carbon signal name should be third token
		if (tokens.size() < 3) {
		    UtIO::cerr() << "Warning: unknown rule file syntax - tokens are" << UtIO::endl;
		    for (unsigned int i = 1; i < tokens.size(); ++i)
			UtIO::cerr() << "    " << tokens[i] << UtIO::endl;
		    continue;
		}

		// Strip off the top module name to get the rest of the hierarchical name
		char *name = strchr(tokens[2].c_str(), '.');
		if (!name) {
		    UtIO::cerr() << "Warning: bad signal name: " << tokens[2] << UtIO::endl;
		    continue;
		}

		// If this is the first rule, extract the golden and carbon prefixes
		if (mRuleList.empty()) {
		    mGoldenPrefix.assign(tokens[1], 0, tokens[1].find(name));
		    mCarbonPrefix.assign(tokens[2], 0, tokens[2].find(name));
		}

		++name;		// skip the .

		// find the other options
		for (unsigned int i = 3; i < tokens.size(); ++i) {
		    if (tokens[i] == "-start") {
			++i;
			if (i < tokens.size())
			    start = convertTime(tokens[i]);
		    }
		    if (tokens[i] == "-end") {
			++i;
			if (i < tokens.size())
			    end = convertTime(tokens[i]);
		    }
		    if (tokens[i] == "-tol") {
			++i;
			if (i < tokens.size())
			    tol = convertTime(tokens[i]);
		    }
		}

		mRuleList.push_back(new ContCompareRule(name, start, end, tol));
	    }

	    // Look for clock definitions
	    if ((tokens.size() == 3) && (tokens[0] == "clockdef")) {
		if (tokens[1].find("clk") != 0) {
		    UtIO::cerr() << "Warning: bad clock name: " << tokens[1] << UtIO::endl;
		    continue;
		}
		unsigned int clock_num = atoi(tokens[1].c_str() + 3);
		if (clock_num & 0x1) {
		    // The clocks are really the same (just different hierarchy), so
		    // we'll only save one, and the Carbon one (odd number) is easier
		    // to extract
		    clock_num >>= 1;	// shift since low bit doesn't matter

		    // Strip off the top module name to get the rest of the hierarchical name
		    char *name = strchr(tokens[2].c_str(), '.');
		    if (!name) {
			UtIO::cerr() << "Warning: bad signal name: " << tokens[2] << UtIO::endl;
			continue;
		    }
		    ++name;		// skip the .

		    // Is our vector large enough?
		    if (clock_num >= clock_defs.size())
			// grow by at least 2x, but maybe more if the new number is large enough
			clock_defs.resize((clock_num > 2 * clock_defs.size()) ? clock_num + clock_defs.size() : 2 * clock_defs.size());

		    clock_defs[clock_num] = name;
		}
	    }
	    
	    // Get clock compare rules
	    if (tokens[0] == "clkcompare") {
		UInt64 start = 0;
		UInt64 end = CompareRule::scMaxTime;
		SInt32 sample1 = 0;
		SInt32 sample2 = 0;

		// Carbon clock and signal are 4th and 5th tokens
		if (tokens.size() < 5) {
		    UtIO::cerr() << "Warning: unknown rule file syntax - tokens are" << UtIO::endl;
		    for (unsigned int i = 1; i < tokens.size(); ++i)
			UtIO::cerr() << "    " << tokens[i] << UtIO::endl;
		    continue;
		}

		if (tokens[3].find("clk") != 0) {
		    UtIO::cerr() << "Warning: bad clock name: " << tokens[3] << UtIO::endl;
		    continue;
		}
		unsigned int clock_num = atoi(tokens[3].c_str() + 3) >> 1;
		if ((clock_num >= clock_defs.size()) || (clock_defs[clock_num] == "")) {
		    UtIO::cerr() << "Warning: unknown clock: " << tokens[3] << UtIO::endl;
		    continue;
		}

		const char *clock = clock_defs[clock_num].c_str();
		
		// Strip off the top module name to get the rest of the hierarchical name
		const char *name = strchr(tokens[4].c_str(), '.');
		if (!name) {
		    UtIO::cerr() << "Warning: bad signal name: " << tokens[4] << UtIO::endl;
		    continue;
		}
		++name;		// skip the .

		// find the other options
		for (unsigned int i = 5; i < tokens.size(); ++i) {
		    if (tokens[i] == "-start") {
			++i;
			if (i < tokens.size())
			    start = convertTime(tokens[i]);
		    }
		    if (tokens[i] == "-end") {
			++i;
			if (i < tokens.size())
			    end = convertTime(tokens[i]);
		    }
		    if (tokens[i] == "-sample1") {
			++i;
			if (i < tokens.size())
			    sample1 = convertTime(tokens[i]);
		    }
		    if (tokens[i] == "-sample2") {
			++i;
			if (i < tokens.size())
			    sample2 = convertTime(tokens[i]);
		    }
		}

		mRuleList.push_back(new ClockedCompareRule(name, clock, start, end, sample1, sample2));
	    }
	}
    }

    fclose(mFile);
    mFile = 0;

    // Some sanity checks
    if (mReportFileName == "") {
	mReportFileName = "ccompare.report";
	UtIO::cerr() << "Warning: No report file found, using " << mReportFileName << UtIO::endl;
    }

    return true;
}
