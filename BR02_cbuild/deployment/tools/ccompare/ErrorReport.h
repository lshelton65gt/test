// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __ERRORREPORT_H__
#define __ERRORREPORT_H__

#include "shell/carbon_shelltypes.h"
#include "util/UtHashSet.h"

class UtOStream;
class UtString;

class ErrorMsg
{
public:
    ErrorMsg(UInt64 t, UtString *m)
	: mTime(t),
	  mMsg(m)
    {
	mIndex = sGlobalIndex++;
    }

    UInt64 mTime;		// time when the error occurred
    UInt32 mIndex;		// unique index for deterministic sorting
    UtString *mMsg;		// the actual error message

protected:
    static UInt32 sGlobalIndex;
};

// hash comparison function class
template<typename _KeyType, typename _HashType>
class ErrorReportHash : public UtHashSmallWrapper<_KeyType, _HashType>
{
public:
    // Less than operator for sorting
    // Sort first by time (ascending), then by index (ascending)
    bool lessThan(const _KeyType& v1, const _KeyType& v2) const
    {
	return (v1->mTime < v2->mTime) || ((v1->mTime == v2->mTime) && (v1->mIndex < v2->mIndex));
    }
    
};

// class to keep track of errors and sort/display them
class ErrorReport
{
public:
    ErrorReport(UtOStream &out);
    ~ErrorReport();

    // What will the signatures for these look like?
    void addContError(const UtString &g_sig,
		      UInt64 g_time,
		      const UtString &g_val,
		      const UtString &c_sig,
		      UInt64 c_time,
		      const UtString &c_val);

    void addExtraClockError(const UtString &g_sig,
			    UInt64 g_time,
			    const UtString &c_sig,
			    UInt64 c_time);

    void addTolError(const UtString &g_sig,
		     UInt64 g_time,
		     const UtString &c_sig,
		     UInt64 c_time);

    void addClockedError();

    void writeReport();

protected:
    UtOStream &mOut;

    typedef UtHashSet<ErrorMsg*, HashHelper<ErrorMsg*>, HashMgr<ErrorMsg*>, ErrorReportHash<ErrorMsg*, HashHelper<ErrorMsg*> > > ErrorHashSet;
    ErrorHashSet mErrors;
};

#endif
