// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __SIGNAL_H__
#define __SIGNAL_H__

#include "shell/carbon_shelltypes.h"
#include "util/UtString.h"

class WfAbsSignal;
class WfAbsSignalVCIter;
class WfAbsFileReader;
class ErrorReport;

// Class that encapsulates the waveform signal interface
class Signal
{
public:
    Signal(WfAbsSignal *golden_sig, WfAbsSignal *carbon_sig);
    ~Signal();

    bool initIters(WfAbsFileReader *golden_wave, WfAbsFileReader *carbon_wave);

    void gotoGTime(UInt64 t);
    void gotoCTime(UInt64 t);
    UInt64 getGTime();
    UInt64 getCTime();

    // These two return whether the edge is a posedge
    bool nextGEdge();
    bool nextCEdge();

    bool atMaxGTime();
    bool atMaxCTime();

    bool compareValues(ErrorReport *report);
    bool compareValuesAtTime(ErrorReport *report, UInt64 gtime, UInt64 ctime);

    const UtString &getGoldenName() const { return mGoldenName; }
    const UtString &getCarbonName() const { return mCarbonName; }

protected:
    WfAbsSignal * const mGoldenSig;
    WfAbsSignal * const mCarbonSig;
    WfAbsSignalVCIter *mGoldenIter;
    WfAbsSignalVCIter *mCarbonIter;

    UtString mGoldenName, mCarbonName;

    // Storage for values
    UInt32 mNumWords;
    const UInt32 *mGVal, *mGMask, *mCVal, *mCMask;
    UtString mGValString, mCValString;

    // We're going to store our own time, as well
    UInt64 mGTime, mCTime, mNextGTime, mNextCTime;

    // If we're stepping through each transition, it's easier to keep the
    // iterator at the current value.  However, if we're going to specific
    // times, it's faster to look ahead to the next transition time.
    // This flag will tell us what mode we're in
    bool mGLookAhead, mCLookAhead;

    // Utility routine to get values
    void examineGValues();
    void examineCValues();
};

#endif
