// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "Checker.h"
#include "RuleFile.h"
#include "Signal.h"
#include "SignalRule.h"
#include "CompareRule.h"
#include "ContCompareRule.h"
#include "ClockedCompareRule.h"
#include "ErrorReport.h"
#include "waveform/WfVCDFileReader.h"
#include "waveform/WfFsdbFileReader.h"
#include "util/UtIOStream.h"
#include "util/UtPair.h"

Checker::Checker(RuleFile * const rf)
    : mRuleFile(rf),
      mDynFactory(true)
{
    // Prepare the waveform interfaces
    // This doesn't actually load anything yet
    char *ext;
    
    ext = strrchr(mRuleFile->getGoldenFileName(), '.');
    if (strcmp(ext, ".fsdb") == 0)	// Assume file ending in .fsdb is FSDB
	mGoldenWf = new WfFsdbFileReader(mRuleFile->getGoldenFileName(), &mStringCache, &mDynFactory, &mTimeFactory, 0);
    else 				// ... and everything else is VCD
	mGoldenWf = new WfVCDFileReader(mRuleFile->getGoldenFileName(), &mStringCache, &mDynFactory, &mTimeFactory);

    ext = strrchr(mRuleFile->getCarbonFileName(), '.');
    if (strcmp(ext, ".fsdb") == 0)	// Assume file ending in .fsdb is FSDB
	mCarbonWf = new WfFsdbFileReader(mRuleFile->getCarbonFileName(), &mStringCache, &mDynFactory, &mTimeFactory, 0);
    else 				// ... and everything else is VCD
	mCarbonWf = new WfVCDFileReader(mRuleFile->getCarbonFileName(), &mStringCache, &mDynFactory, &mTimeFactory);

    // Open the log file, in case we need to report any missing signals
    mReport = new UtOFStream(mRuleFile->getReportFileName());
    if (!mReport->is_open())
	UtIO::cerr() << "Warning: Couldn't open report file" << UtIO::endl;

    mTimeScale = static_cast<CarbonTimescale>(0);
    mNumErrors = 0;
    mNumSignals = 0;
}

Checker::~Checker()
{
    delete mGoldenWf;
    delete mCarbonWf;
    mReport->close();
    delete mReport;

    for (UtHashMap<UtString, Signal *>::iterator iter = mSigMap.begin(); iter != mSigMap.end(); ++iter)
	delete iter->second;
    for (UtList<SignalRule *>::iterator iter = mContSigRules.begin(); iter != mContSigRules.end(); ++iter)
	delete *iter;
}

bool Checker::getSignals()
{
    // First, load the signals from the two waveforms
    UtString reason;
    if (mGoldenWf->loadSignals(&reason) != WfAbsFileReader::eOK)
    {
	UtIO::cerr() << reason << UtIO::endl;
	return false;
    }

    if (mCarbonWf->loadSignals(&reason) != WfAbsFileReader::eOK)
    {
	UtIO::cerr() << reason << UtIO::endl;
	return false;
    }

    // Make sure the timescales match, and convert to API format
    if ((mGoldenWf->getTimescaleUnit() != mCarbonWf->getTimescaleUnit()) ||
	(mGoldenWf->getTimescaleValue() != mCarbonWf->getTimescaleValue()))
	UtIO::cerr() << "Warning: Timescales don't match!" << UtIO::endl;
	
    WfVCD::TimescaleUnit t_unit = mGoldenWf->getTimescaleUnit();
    WfVCD::TimescaleValue t_value = mGoldenWf->getTimescaleValue();

    switch (t_unit) {
    case (WfVCD::eWfVCDTimescaleUnitFS): mTimeScale = e1fs; break;
    case (WfVCD::eWfVCDTimescaleUnitMS): mTimeScale = e1ms; break;
    case (WfVCD::eWfVCDTimescaleUnitNS): mTimeScale = e1ns; break;
    case (WfVCD::eWfVCDTimescaleUnitPS): mTimeScale = e1ps; break;
    case (WfVCD::eWfVCDTimescaleUnitS): mTimeScale = e1s; break;
    case (WfVCD::eWfVCDTimescaleUnitUS): mTimeScale = e1us; break;
    default:
	UtIO::cerr() << "Warning: Invalid timescale" << UtIO::endl;
    }
    // Hey, YOU find a better way to do this!
    mTimeScale = static_cast<CarbonTimescale>(static_cast<int>(mTimeScale) + static_cast<int>(t_value));

    // Need to scale the rules to match the timescale
    mRuleFile->scaleRules(mTimeScale);
    // Also, expand any hierarchical rules to use the actual signals in the waveform
    // Use the carbon waveform because it will most likely have fewer signals
    mRuleFile->expandHierRules(mCarbonWf->getSymbolTable());


    // Start finding all the signals.
    // Multiple rules can reference the same signal, but we should only need
    // one interface per signal, so we'll save them as we find them in a map,
    // and reuse them.
    Signal *sig;
    CompareRule *rule;
    SignalRule *sr;
    UtHashMap<UtString, Signal *>::iterator sig_iter;
    UtString sig_name;
    UtList<SignalRule *>::iterator sr_iter = mContSigRules.begin();
    ClockedCompareRule *ccr;
    WfAbsSignal *g_sig, *c_sig;
    UtString g_pre = mRuleFile->getGoldenPrefix();
    UtString c_pre = mRuleFile->getCarbonPrefix();

    g_pre.append(".");
    c_pre.append(".");

    mRuleFile->resetRuleIter();
    while (mRuleFile->getRule(&rule)) {
	// See if we already have a signal for this net
	sig_name = rule->getName();
	sig_iter = mSigMap.find(sig_name);
	if (sig_iter == mSigMap.end()) {
	    // Don't have a signal yet...
	    // Look up the signal in both waveforms
	    g_sig = mGoldenWf->watchSignal(g_pre + sig_name);
	    if (!g_sig) {
		*mReport << "Warning: Could not find signal " << (g_pre + sig_name) << " in " << mRuleFile->getGoldenFileName() << UtIO::endl;
		continue;
	    }
	    
	    c_sig = mCarbonWf->watchSignal(c_pre + sig_name);
	    if (!c_sig) {
		*mReport << "Warning: Could not find signal " << (c_pre + sig_name) << " in " << mRuleFile->getCarbonFileName() << UtIO::endl;
		continue;
	    }

	    // Make sure the sizes match
	    if (g_sig->getWidth() != c_sig->getWidth()) {
		*mReport << "Warning: Signal " << sig_name << " has different widths in each waveform - skipping comparisons" << UtIO::endl;
		continue;
	    }

	    // Create the signal and save it
	    sig = new Signal(g_sig, c_sig);
	    mSigMap.insert(UtPair<UtString, Signal *> (sig_name, sig));
	} else {
	    // The signal already exists, so just use it
	    sig = sig_iter->second;
	}

	// Create a new SignalRule combo with this rule and the accompanying signal
	// Note that each rule has its own SignalRule, while each Signal may be shared
	// by several SignalRules.
	sr = new SignalRule(sig, rule);

	// Where do we put it?
	// If it's a continuous compare, we put it in our list
	if (dynamic_cast<ContCompareRule*>(rule))
	    mContSigRules.push_back(sr);
	// If it's a clocked compare, we add it to the continuous compare
	// for that clock
	else if ((ccr = dynamic_cast<ClockedCompareRule*>(rule))) {
	    // Look for the clock.
	    // Try the previous iterator value, because all the clocked compares
	    // are grouped together and it's likely we'll already have it.
	    if ((sr_iter != mContSigRules.end()) &&	// If we haven't done anything yet, the iter won't be valid
		(strcmp((*sr_iter)->getRule()->getName(), ccr->getClock()) == 0))
		(*sr_iter)->addSignalRule(sr);
	    else {
		// start from the beginning
		for (sr_iter = mContSigRules.begin(); sr_iter != mContSigRules.end(); ++sr_iter)
		    if (strcmp((*sr_iter)->getRule()->getName(), ccr->getClock()) == 0) {
			(*sr_iter)->addSignalRule(sr);
			break;
		    }
		if (sr_iter == mContSigRules.end()) {
		    // Somehow, we didn't find a rule for the clock that this
		    // clocked compare runs on!
		    *mReport << "Warning: Could not find continuous compare for clock " << ccr->getClock() << ", needed for signal " << ccr->getName() << UtIO::endl;
		    delete sr;
		}
	    }
	} else {
	    // WTF?  We should never get here!
	    *mReport << "Warning: Can't determine rule type for signal " << sig_name << UtIO::endl;
	    delete sr;
	}
    }
    
    // Determine how many signals we're actually going to compare
    for (UtList<SignalRule *>::const_iterator iter = mContSigRules.begin(); iter != mContSigRules.end(); ++iter)
	mNumSignals += (*iter)->getNumSignals();

    // Everything went well
    return true;
}

bool Checker::loadValues()
{
    // Unload the signals we don't need
    mGoldenWf->unloadSignals();
    mCarbonWf->unloadSignals();

#if 1
    // Load the value changes for all the signals we need
    UtString msg;
    WfAbsFileReader::Status stat;

    stat = mGoldenWf->loadValues(&msg);
    if (stat != WfAbsFileReader::eOK)
    {
	UtIO::cerr() << msg << UtIO::endl;
	if (stat == WfAbsFileReader::eError)
	    return false;
    }

    stat = mCarbonWf->loadValues(&msg);
    if (stat != WfAbsFileReader::eOK)
    {
	UtIO::cerr() << msg << UtIO::endl;
	if (stat == WfAbsFileReader::eError)
	    return false;
    }
#endif

    // Create signal iterators for all our signals
    for (UtHashMap<UtString, Signal *>::iterator iter = mSigMap.begin(); iter != mSigMap.end(); ++iter)
	if (!iter->second->initIters(mGoldenWf, mCarbonWf))
	    return false;

    return true;
}

bool Checker::compare()
{
    ErrorReport er(*mReport);
    for (UtList<SignalRule *>::const_iterator iter = mContSigRules.begin(); iter != mContSigRules.end(); ++iter)
	mNumErrors += (*iter)->compare(&er);

    er.writeReport();
    *mReport << UtIO::dec << mNumSignals << " signals were compared with " << mNumErrors << " errors found" << UtIO::endl;

    return (mNumErrors == 0);
}

void Checker::print() const
{
    for (UtList<SignalRule *>::const_iterator iter = mContSigRules.begin(); iter != mContSigRules.end(); ++iter)
	(*iter)->print();
}
