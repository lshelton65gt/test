// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CompareRule.h"
#include "util/UtIOStream.h"

CompareRule::CompareRule(const char *name, UInt64 start_time, UInt64 end_time)
    : mName(name),
      mStart(start_time),
      mEnd(end_time)
{
}

CompareRule::~CompareRule()
{
}

void CompareRule::print() const
{
    UtIO::cout() << "signal " << mName << ", start " << UtIO::dec << mStart << ", end " << mEnd << UtIO::endl;
}

void CompareRule::scaleRule(CarbonTimescale ts)
{
    scale(mStart, e1ps, ts);
    scale(mEnd, e1ps, ts);
}

