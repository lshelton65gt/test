// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CSCANRULEFILE_H__
#define __CSCANRULEFILE_H__

#include "RuleFile.h"

// Rules file interface for Comparescan rules files
class CscanRuleFile : public RuleFile
{
public:
    CscanRuleFile(const char *filename);
    virtual ~CscanRuleFile();

    virtual bool readFile();
};

#endif
