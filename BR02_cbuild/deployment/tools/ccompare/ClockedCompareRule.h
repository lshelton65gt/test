// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CLOCKEDCOMPARERULE_H__
#define __CLOCKEDCOMPARERULE_H__

#include "CompareRule.h"

// Rule definition class for clocked comparisons
class ClockedCompareRule : public CompareRule
{
public:
    ClockedCompareRule(const char *name, const char *clock, UInt64 start_time = 0, UInt64 end_time = scMaxTime, SInt32 golden_sample = 0, SInt32 carbon_sample = 0);
    virtual ~ClockedCompareRule();

    virtual void print() const;
    virtual void scaleRule(CarbonTimescale ts);

    const char *getClock() const { return mClk.c_str(); }
    SInt32 getGoldenSample() const { return mGoldenSample; }
    SInt32 getCarbonSample() const { return mCarbonSample; }

protected:
    const UtString mClk;
    SInt32 mGoldenSample, mCarbonSample;
};

#endif


