// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __HIERCONTCOMPARERULE_H__
#define __HIERCONTCOMPARERULE_H__

#include "ContCompareRule.h"
#include "util/UtList.h"

class STSymbolTable;

// Rule definition class for continuous comparisons
class HierContCompareRule : public ContCompareRule
{
public:
    HierContCompareRule(const char *name, UInt32 depth = 0, UInt64 start_time = 0, UInt64 end_time = scMaxTime, UInt32 tol = 0);
    virtual ~HierContCompareRule();

    UInt32 getDepth() const { return mDepth; }

    virtual void print() const;

    void expandHierarchy(const STSymbolTable *symtab);
    bool getNextHierRule(CompareRule **rule);

protected:
    const UInt32 mDepth;
    UtList<ContCompareRule*> mHierRuleList;	// list of all the individual signal rules after they've been expanded
    UtList<ContCompareRule*>::iterator mCurrHierRule;	// iterator for rule list
};

#endif


