// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CONTCOMPARERULE_H__
#define __CONTCOMPARERULE_H__

#include "CompareRule.h"

// Rule definition class for continuous comparisons
class ContCompareRule : public CompareRule
{
public:
    ContCompareRule(const char *name, UInt64 start_time = 0, UInt64 end_time = scMaxTime, UInt32 tol = 0);
    virtual ~ContCompareRule();

    UInt32 getTol() const { return mTol; }

    virtual void print() const;
    virtual void scaleRule(CarbonTimescale ts);

protected:
    UInt32 mTol;
};

#endif


