// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __SIGNALRULE_H__
#define __SIGNALRULE_H__

#include "util/UtList.h"

class Signal;
class CompareRule;
class ErrorReport;

// class to combine a rule with its signal interface
class SignalRule
{
public:
    SignalRule(Signal *sig, const CompareRule *rule);
    ~SignalRule();

    Signal *getSignal() const { return mSig; };
    const CompareRule *getRule() const { return mRule; }

    void addSignalRule(SignalRule *sr);

    UInt32 compare(ErrorReport *report);
    UInt32 getNumSignals();

    void print() const;

protected:
    Signal * const mSig;
    const CompareRule * const mRule;

    // If this SignalRule is for a continuous compare (i.e. a clock signal),
    // the SignalRules for all clocked compares based on this signal will
    // be kept in a list here.
    UtList<SignalRule *> mClockedSigRules;
};

#endif
