// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "RuleFile.h"
#include "CompareRule.h"
#include "HierContCompareRule.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"

RuleFile::RuleFile(const char *filename)
    : mRuleFileName(filename)
{
    mFile = 0;
    resetRuleIter();
}

RuleFile::~RuleFile()
{
    for (UtList<CompareRule *>::iterator iter = mRuleList.begin(); iter != mRuleList.end(); ++iter)
	delete *iter;
}

void RuleFile::print() const
{
    UtIO::cout() << "rule file " << mRuleFileName << ", golden wave " << mGoldenFileName << ", carbon wave " << mCarbonFileName << UtIO::endl;
    UtIO::cout() << "     golden prefix " << mGoldenPrefix << ", carbon prefix " << mCarbonPrefix << UtIO::endl;
    for (UtList<CompareRule *>::const_iterator iter = mRuleList.begin(); iter != mRuleList.end(); ++iter)
	(*iter)->print();
}

void RuleFile::resetRuleIter()
{
    mCurrRule = mRuleList.begin();
}

bool RuleFile::getRule(CompareRule **rule)
{
    if (mCurrRule == mRuleList.end())
	return false;

    *rule = *mCurrRule;
    ++mCurrRule;
    return true;
}

bool RuleFile::getTokens(UtVector<UtString> *vec)
{
    // No, this probably isn't the most efficient implementation, but we'll deal
    // with that if and when it becomes a problem.

    if (!mFile)
	return false;

    // clear from last iteration
    vec->clear();

    bool readline = true;
    static char line[1000];	// hope that's big enough...
    int tok_start = -1;
    while (readline) {
	if (fgets(line, 1000, mFile)) {
	    // Go through each character, finding the tokens
	    unsigned int len = strlen(line);
	    for (unsigned int i = 0; i < len; ++i) {
		if ((line[i] == ' ') ||
		    (line[i] == '\t') ||
		    (line[i] == '\n')) {	// fgets includes the \n in the char[]
		    // whitespace - if we're building a token, save it
		    if (tok_start != -1) {
			line[i] = '\0';
			vec->push_back(&line[tok_start]);
			tok_start = -1;
		    }
		} else {
		    // non-whitespace - if we're not building a token, start
		    if (tok_start == -1)
			tok_start = i;
		}
	    }
	    // done with this physical line - check the last token we saw to see if
	    // we need to continue this logical line
	    if (!vec->empty() && (vec->at(vec->size() - 1) == "\\"))
		vec->pop_back();	// remove from the vector
	    else
		readline = false;
	} else
	    // EOF, so we're done
	    return false;
    }
    
    return true;
}

SInt64 RuleFile::convertTime(const UtString &t)
{
    CarbonTimescale ts;
    // grab the last two characters of the string
    UtString ts_string;
    ts_string.assign(t, t.length() - 2, 2);

    if (ts_string == "fs")
	ts = e1fs;
    else if (ts_string == "ps")
	ts = e1ps;
    else if (ts_string == "ns")
	ts = e1ns;
    else if (ts_string == "us")
	ts = e1us;
    else if (ts_string == "ms")
	ts = e1ms;
    else if (ts_string[1] == 's') {
	// I think we've seen some waves that are in seconds!
	// Make sure the char before is a digit
	if ((ts_string[0] >= '0') && (ts_string[0] <= 9))
	    ts = e1s;
	else {
	    UtIO::cerr() << "Warning - bad time: " << t << UtIO::endl;
	    return 0;
	}
    } else {
	UtIO::cerr() << "Warning - bad time: " << t << UtIO::endl;
	return 0;
    }

    // Extract numeric value
    UtString val_string;
    if (ts == e1s)
	val_string.assign(t, 0, t.length() - 1);
    else
	val_string.assign(t, 0, t.length() - 2);

    SInt64 val = atoll(val_string.c_str());

    // Scale appropriately
    // Actually, at the time the rules file is parsed, we don't know the timescale
    // of the wave file, so we'll have to scale it later.  We'll scale to ps (I doubt
    // any of our rules with have finer granularity than that).

    CompareRule::scale(val, ts, e1ps);

    return val;
}

void RuleFile::scaleRules(CarbonTimescale ts)
{
    for (UtList<CompareRule *>::iterator iter = mRuleList.begin(); iter != mRuleList.end(); ++iter)
	(*iter)->scaleRule(ts);
}

void RuleFile::expandHierRules(const STSymbolTable *symtab)
{
    // Now that the waveform is loaded, we can expand any rules with implied hierarchy
    HierContCompareRule *rule;
    CompareRule *temp_rule;
    UtList<CompareRule *> temp_list;
    for (UtList<CompareRule *>::iterator iter = mRuleList.begin(); iter != mRuleList.end(); ) {
	rule = dynamic_cast<HierContCompareRule*>(*iter);
	if (rule) {
	    rule->expandHierarchy(symtab);

	    // Now, replace this rule with its hier rules in the list
	    // We'll put the hier rules in a temp list and merge them at the end
	    while (rule->getNextHierRule(&temp_rule))
		temp_list.push_back(temp_rule);

	    // newly-created rules will be cleaned up at the end, but delete the original now
	    delete rule;
	    iter = mRuleList.erase(iter);	// erase returns an iterator to the following element
	} else
	    ++iter;	// don't need to expand - move on
    }

    // Finally, append our temporary list of expanded rules to the main list
    mRuleList.insert(mRuleList.end(), temp_list.begin(), temp_list.end());
}
