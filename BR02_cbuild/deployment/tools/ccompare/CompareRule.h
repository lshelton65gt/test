// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __COMPARERULE_H__
#define __COMPARERULE_H__

#include "shell/carbon_shelltypes.h"
#include "util/UtString.h"

// Rule definition base class for all comparisons
class CompareRule
{
public:
    CompareRule(const char *name, UInt64 start_time = 0, UInt64 end_time = scMaxTime);
    virtual ~CompareRule();

    virtual void print() const;

    const char *getName() const { return mName.c_str(); }
    UInt64 getStartTime() const { return mStart; }
    UInt64 getEndTime() const { return mEnd; }
    
    static const UInt64 scMaxTime = 0xffffffffffffffffULL;

    virtual void scaleRule(CarbonTimescale ts);

    template<typename T>
    static void scale(T &val, CarbonTimescale from, CarbonTimescale to)
    {
	unsigned int scale = 1;
	int power = to - from;
	if (power != 0) {
	    // need to scale value (up or down) by 10 ** difference
	    for (unsigned int i = 0; i < abs(power); ++i)
		scale *= 10;
	    if (power > 0)
		// timescale is too small - need to divide to make it match
		val /= scale;
	    else
		// timescale is too large - need to multiply to make it match
		val *= scale;
	}
    }

protected:
    const UtString mName;
    UInt64 mStart, mEnd;

};

#endif


