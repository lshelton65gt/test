// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "HierContCompareRule.h"
#include "util/UtIOStream.h"
#include "symtab/STSymbolTable.h"
#include "symtab/STSymbolTableNode.h"
#include "symtab/STAliasedLeafNode.h"

HierContCompareRule::HierContCompareRule(const char *name, UInt32 depth, UInt64 start_time, UInt64 end_time, UInt32 tol)
    : ContCompareRule(name, start_time, end_time, tol),
      mDepth(depth)
{
    mCurrHierRule = mHierRuleList.end();
}

HierContCompareRule::~HierContCompareRule()
{
}

void HierContCompareRule::print() const
{
    ContCompareRule::print();
    UtIO::cout() << "depth " << UtIO::dec << mDepth << UtIO::endl;
}

void HierContCompareRule::expandHierarchy(const STSymbolTable *symtab)
{
    STAliasedLeafNode *node;
    
    for (STSymbolTable::CNodeLoop iter = symtab->getCNodeLoop(); !iter.atEnd(); ++iter) {
	// only add actual signals (leaves)
	node = (*iter)->castLeaf();
	if (node) {
	    // make sure it matches our hierarchy
	    // TODO - match depth
	    UtString namestr;
	    node->compose(&namestr, false);
	    // empty hier will always match!
// 	    UtIO::cout() << "Matching " << namestr << " to " << mName << UtIO::endl;
	    if (mName.empty() ||
		(namestr.find(const_cast<UtString&>(mName)) == 0)) {	// why isn't this const in UtString?
		// beginning hierarchy matches!
		// Strip the top module, if it's there
 		mHierRuleList.push_back(new ContCompareRule(namestr.c_str(), mStart, mEnd, mTol));
	    }
	}
    }

    // set iterator to front so we can walk the list
    mCurrHierRule = mHierRuleList.begin();
}

bool HierContCompareRule::getNextHierRule(CompareRule **rule)
{
    if (mCurrHierRule == mHierRuleList.end())
	return false;

    *rule = *mCurrHierRule;
    ++mCurrHierRule;
    return true;
}

