// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __RULEFILE_H__
#define __RULEFILE_H__

#include "util/UtList.h"
#include "util/UtVector.h"
#include "util/UtString.h"
#include "shell/carbon_shelltypes.h"

class CompareRule;
class STSymbolTable;

// Generic rule file interface
class RuleFile
{
public:
    RuleFile(const char *filename);
    virtual ~RuleFile();

    // Should this automatically be done at construction?
    // If so, the derived classes will have to explicity open/read
    // the file, and this function will go away.
    virtual bool readFile() = 0;

    void print() const;

    const char *getGoldenFileName() const { return mGoldenFileName.c_str(); }
    const char *getCarbonFileName() const { return mCarbonFileName.c_str(); }
    const char *getReportFileName() const { return mReportFileName.c_str(); }
    const char *getGoldenPrefix() const { return mGoldenPrefix.c_str(); }
    const char *getCarbonPrefix() const { return mCarbonPrefix.c_str(); }

    // For iterating over the rules in this file
    void resetRuleIter();
    bool getRule(CompareRule **rule);

    void scaleRules(CarbonTimescale ts);
    void expandHierRules(const STSymbolTable *symtab);

protected:
    FILE *mFile;

    // Save all the rules in a list.
    // Also, allow the user to iterate over them
    UtList<CompareRule *> mRuleList;
    UtList<CompareRule *>::iterator mCurrRule;    


    // Various rule file info
    const UtString mRuleFileName;
    UtString mGoldenFileName, mCarbonFileName, mReportFileName;
    UtString mGoldenPrefix, mCarbonPrefix;

    // utility function to parse a logical line (including backslash continuation)
    // into a vector of tokens
    bool getTokens(UtVector<UtString> *vec);
    // scales a time value to the timescale of this object
    SInt64 convertTime(const UtString &t);

};

#endif
