// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "Signal.h"
#include "ErrorReport.h"
#include "waveform/WfAbsSignal.h"
#include "waveform/WfAbsSignalVCIter.h"
#include "waveform/WfAbsFileReader.h"
#include "util/UtIOStream.h"

Signal::Signal(WfAbsSignal *golden_sig, WfAbsSignal *carbon_sig)
    : mGoldenSig(golden_sig),
      mCarbonSig(carbon_sig)
{
    mGoldenIter = 0;
    mCarbonIter = 0;

    // Allocate storage for values
    // We've already ensured that they're the same size
    mNumWords = (mGoldenSig->getWidth() % 32) ? (mGoldenSig->getWidth() / 32 + 1) : (mGoldenSig->getWidth() / 32);

    /*
    New waveform interface means I don't have to allocate these
     
    mGVal = new UInt32[mNumWords];
    mGMask = new UInt32[mNumWords];
    mCVal = new UInt32[mNumWords];
    mCMask = new UInt32[mNumWords];

    memset(mGVal, 0, mNumWords * sizeof(UInt32));
    memset(mGMask, 0, mNumWords * sizeof(UInt32));
    memset(mCVal, 0, mNumWords * sizeof(UInt32));
    memset(mCMask, 0, mNumWords * sizeof(UInt32));
    */

    mGTime = 0;
    mCTime = 0;
    mNextGTime = 0;
    mNextCTime = 0;
    mGLookAhead = false;
    mCLookAhead = false;
}

Signal::~Signal()
{
    // The waveform API allocates these, so we need to delete them
    /*
    Actually, it looks like it cleans these up now

    delete mGoldenSig;
    delete mCarbonSig;
    if (mGoldenIter)
	delete mGoldenIter;
    if (mCarbonIter)
	delete mCarbonIter;
    */

    /*
    New waveform interface means I don't have to allocate these

    delete [] mGVal;
    delete [] mGMask;
    delete [] mCVal;
    delete [] mCMask;
    */
}

bool Signal::initIters(WfAbsFileReader *golden_wave, WfAbsFileReader *carbon_wave)
{
    UtString buf;

    mGoldenIter = golden_wave->createSignalVCIter(mGoldenSig, &buf);
    if (!buf.empty())
	UtIO::cerr() << buf << UtIO::endl;
    if (!mGoldenIter)
	return false;
    
    mCarbonIter = carbon_wave->createSignalVCIter(mCarbonSig, &buf);
    if (!buf.empty())
	UtIO::cerr() << buf << UtIO::endl;
    if (!mCarbonIter)
	return false;
    
    mGoldenIter->getName(&mGoldenName);
    mCarbonIter->getName(&mCarbonName);

    return true;
}

void Signal::gotoGTime(UInt64 t)
{
    if (mGLookAhead) {
	if ((t >= mGTime) && (t < mNextGTime))
	    // This is easy!  The current value we have is correct.
	    // Just update the time
	    mGTime = t;
	else if (t >= mNextGTime) {
	    // Need to move forward in time.
	    // Hopefully, we only need to move ahead one transition,
	    // so I'll try that first.
	    if (mGoldenIter->atLastTransition()) {
		// This is the last transition, so update our values.
		mGTime = t;
		examineGValues();
		mNextGTime = UtUINT64_MAX;
	    } else {
		// examine the current values and advance one edge
		examineGValues();
		mGoldenIter->gotoNextTime();
		if (t < mGoldenIter->getTime()) {
		    // One edge was enough
		    mGTime = t;
		    mNextGTime = mGoldenIter->getTime();
		} else {
		    // Give up, and go directly to the time
		    mGTime = t;
		    mGoldenIter->gotoTime(mGTime);
		    examineGValues();
		    if (mGoldenIter->atLastTransition())
			mNextGTime = UtUINT64_MAX;
		    else {
			mGoldenIter->gotoNextTime();
			mNextGTime = mGoldenIter->getTime();
		    }
		}
	    }
	}
    } else {
	// Save the requested time and values
	mGTime = t;
	mGoldenIter->gotoTime(mGTime);
	examineGValues();
	// Move to the next change and save its time
	if (mGoldenIter->atLastTransition())
	    mNextGTime = UtUINT64_MAX;
	else {
	    mGoldenIter->gotoNextTime();
	    mNextGTime = mGoldenIter->getTime();
	}
	// Now we're in lookahead mode
	mGLookAhead = true;
    }
}

void Signal::gotoCTime(UInt64 t)
{
    if (mCLookAhead) {
	if ((t >= mCTime) && (t < mNextCTime))
	    // This is easy!  The current value we have is correct.
	    // Just update the time
	    mCTime = t;
	else if (t >= mNextCTime) {
	    // Need to move forward in time.
	    // Hopefully, we only need to move ahead one transition,
	    // so I'll try that first.
	    if (mCarbonIter->atLastTransition()) {
		// This is the last transition, so update our values.
		mCTime = t;
		examineCValues();
		mNextCTime = UtUINT64_MAX;
	    } else {
		// examine the current values and advance one edge
		examineCValues();
		mCarbonIter->gotoNextTime();
		if (t < mCarbonIter->getTime()) {
		    // One edge was enough
		    mCTime = t;
		    mNextCTime = mCarbonIter->getTime();
		} else {
		    // Give up, and go directly to the time
		    mCTime = t;
		    mCarbonIter->gotoTime(mCTime);
		    examineCValues();
		    if (mCarbonIter->atLastTransition())
			mNextCTime = UtUINT64_MAX;
		    else {
			mCarbonIter->gotoNextTime();
			mNextCTime = mCarbonIter->getTime();
		    }
		}
	    }
	}
    } else {
	// Save the requested time and values
	mCTime = t;
	mCarbonIter->gotoTime(mCTime);
	examineCValues();
	// Move to the next change and save its time
	if (mCarbonIter->atLastTransition())
	    mNextCTime = UtUINT64_MAX;
	else {
	    mCarbonIter->gotoNextTime();
	    mNextCTime = mCarbonIter->getTime();
	}
	// Now we're in lookahead mode
	mCLookAhead = true;
    }
}

UInt64 Signal::getGTime()
{
    return mGTime;
}

UInt64 Signal::getCTime()
{
    return mCTime;
}

bool Signal::nextGEdge()
{
    if (mGLookAhead) {
	// Our iterator is already at the next edge, so just examine the value
	// and update the time.
	// Note - we actually query the time again, because we might already be
	// at the last edge, and our saved next value would be UtUINT64_MAX.
	mGLookAhead = false;
    } else {
	mGoldenIter->gotoNextTime();
    }
    mGTime = mGoldenIter->getTime();
    examineGValues();
    return (mGVal[0] & 0x1);
}

bool Signal::nextCEdge()
{
    if (mCLookAhead) {
	// Our iterator is already at the next edge, so just examine the value
	// and update the time.
	// Note - we actually query the time again, because we might already be
	// at the last edge, and our saved next value would be UtUINT64_MAX.
	mCLookAhead = false;
    } else {
	mCarbonIter->gotoNextTime();
    }
    mCTime = mCarbonIter->getTime();
    examineCValues();
    return (mCVal[0] & 0x1);
}

bool Signal::atMaxGTime()
{
    if (mGLookAhead)
	return (mNextGTime == UtUINT64_MAX);

    return mGoldenIter->atLastTransition();
}

bool Signal::atMaxCTime()
{
    if (mCLookAhead)
	return (mNextCTime == UtUINT64_MAX);

    return mCarbonIter->atLastTransition();
}

bool Signal::compareValues(ErrorReport *report)
{
    //    UtIO::cout() << "Comparing sig " << mGoldenName << " (@" << UtIO::dec << getGTime() << ") with sig " << mCarbonName << " (@" << getCTime() << ")" << UtIO::endl;

    for (unsigned int i = 0; i < mNumWords; ++i)
	// Use the golden mask for both, so Z or X will match anything Carbon dumps
	// Note that the getValue() documentation is wrong - a 0, not a 1, in the mask
	// means the corresponding value bit is 0/1 vs. X/Z, just like the API.
	// Also, if golden mask is null, just compare values
	if ((mGMask && ((mGVal[i] & ~mGMask[i]) != (mCVal[i] & ~mGMask[i]))) ||
	    (!mGMask && (mGVal[i] != mCVal[i]))) {
	    report->addContError(mGoldenName, mGTime, mGValString, mCarbonName, mCTime, mCValString);
	    return false;
	}

    return true;
}

bool Signal::compareValuesAtTime(ErrorReport *report, UInt64 gtime, UInt64 ctime)
{
    // Move to the required time and get the current values
    gotoGTime(gtime);
    gotoCTime(ctime);

    //    UtIO::cout() << "Clock comparing sig " << mGoldenName << " (@" << UtIO::dec << gtime << ") with sig " << mCarbonName << " (@" << ctime << ")" << UtIO::endl;

    for (unsigned int i = 0; i < mNumWords; ++i)
	// Use the golden mask for both, so Z or X will match anything
	// Carbon dumps
	// Also, if golden mask is null, just compare values
	if ((mGMask && ((mGVal[i] & ~mGMask[i]) != (mCVal[i] & ~mGMask[i]))) ||
	    (!mGMask && (mGVal[i] != mCVal[i]))) {
	    report->addContError(mGoldenName, mGTime, mGValString, mCarbonName, mCTime, mCValString);
	    return false;
	}

    return true;
}

void Signal::examineGValues()
{
    mGoldenIter->getValueDrive(&mGVal, &mGMask);
    mGoldenIter->getValueStr(&mGValString);
}

void Signal::examineCValues()
{
    mCarbonIter->getValueDrive(&mCVal, &mCMask);	// mCMask is unused
    mCarbonIter->getValueStr(&mCValString);
}
