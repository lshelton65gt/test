// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "ContCompareRule.h"
#include "util/UtIOStream.h"

ContCompareRule::ContCompareRule(const char *name, UInt64 start_time, UInt64 end_time, UInt32 tol)
    : CompareRule(name, start_time, end_time),
      mTol(tol)
{
}

ContCompareRule::~ContCompareRule()
{
}

void ContCompareRule::print() const
{
    CompareRule::print();
    UtIO::cout() << "tolerance " << UtIO::dec << mTol << UtIO::endl;
}

void ContCompareRule::scaleRule(CarbonTimescale ts)
{
    CompareRule::scaleRule(ts);
    scale(mTol, e1ps, ts);
}
