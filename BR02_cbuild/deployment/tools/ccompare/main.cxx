// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Carbon waveform comparison tool

#include "CscanRuleFile.h"
#include "NCompareRuleFile.h"
#include "Checker.h"
#include "util/UtIOStream.h"
#include "util/Stats.h"

int main(int argc, char **argv)
{
    
    Stats *stats = new Stats(stdout);

    if (argc != 2) {
	UtIO::cerr() << "No file specified " << UtIO::endl;
	return 1;
    }

    RuleFile *rf;
    // check file extension
    if (strcmp(argv[1] + (strlen(argv[1]) - 4), ".ncr") == 0)
	rf = new NCompareRuleFile(argv[1]);
    else
	rf = new CscanRuleFile(argv[1]);

    if (!rf->readFile()) {
	UtIO::cerr() << "Error reading rules file " << UtIO::endl;
	return 1;
    }
    stats->printIntervalStatistics("Load Rules");

    Checker *check = new Checker(rf);
    stats->printIntervalStatistics("Open Waveforms");

    check->getSignals();
    stats->printIntervalStatistics("Get Signals");
    
//     check->print();

    check->loadValues();
    stats->printIntervalStatistics("Load Values");

    check->compare();
    stats->printIntervalStatistics("Compare");
    stats->printTotalStatistics();

    UInt32 num_errors = check->getNumErrors();

    delete check;
    delete rf;

    UtIO::cerr() << UtIO::dec << num_errors << " errors found." << UtIO::endl;
    return num_errors;
}
