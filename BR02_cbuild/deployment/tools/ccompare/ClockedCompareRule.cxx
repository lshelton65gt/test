// -*-c++-*-

/*****************************************************************************

 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "ClockedCompareRule.h"
#include "util/UtIOStream.h"

ClockedCompareRule::ClockedCompareRule(const char *name, const char *clock, UInt64 start_time, UInt64 end_time, SInt32 golden_sample, SInt32 carbon_sample)
    : CompareRule(name, start_time, end_time),
      mClk(clock),
      mGoldenSample(golden_sample),
      mCarbonSample(carbon_sample)
{
}

ClockedCompareRule::~ClockedCompareRule()
{
}

void ClockedCompareRule::print() const
{
    CompareRule::print();
    UtIO::cout() << "clock " << mClk << ", golden sample " << UtIO::dec << mGoldenSample << ", carbon sample " << mCarbonSample << UtIO::endl;
}

void ClockedCompareRule::scaleRule(CarbonTimescale ts)
{
    CompareRule::scaleRule(ts);
    scale(mGoldenSample, e1ps, ts);
    scale(mCarbonSample, e1ps, ts);
}
