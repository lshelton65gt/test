library ieee;
use ieee.std_logic_1164.all;

entity myreg is
  
  port (
    clk1 : in  std_logic;
    clk2 : in  std_logic;
    i   : in  std_logic_vector (7 downto 0);
    o   : out std_logic_vector (7 downto 0);
    obar: out std_logic_vector (7 downto 0);
    comp: out std_logic);

end myreg;

architecture rtl of myreg is

  signal otemp : std_logic_vector (7 downto 0) := (others => '0');
  signal obartemp : std_logic_vector (7 downto 0) := (others => '0');
  signal oreg : std_logic_vector (7 downto 0) := (others => '0');
  signal obarreg : std_logic_vector (7 downto 0) := (others => '0');

begin  -- arch

  process (clk1)
  begin
    if clk1'event and clk1 = '1' then
      otemp <= i;
      obarreg <= not (obartemp);
    end if;
  end process;

  process (clk2)
  begin
    if clk2'event and clk2 = '1' then
      oreg <= otemp;
      obartemp <= i;
    end if;
  end process;
  
  o <= oreg;
  obar <= obarreg;
  comp <= '0' when (oreg > obarreg) else '1';

end rtl;
