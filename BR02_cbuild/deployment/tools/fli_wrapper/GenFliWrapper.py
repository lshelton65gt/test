import sys
import Carbon.DB

def printIncludes(design, stream = sys.stdout):
    "Print necessary includes"

    print >> stream, "// Carbon FLI wrapper"
    print >> stream, """
#include "mti.h"
#include "lib%s.h"
#include <iostream> """ % design

def printEnums(stream = sys.stdout):
    "Print enumerated types"

    print >> stream, """
enum {
    FLI_STD_LOGIC_U,
    FLI_STD_LOGIC_X,
    FLI_STD_LOGIC_0,
    FLI_STD_LOGIC_1,
    FLI_STD_LOGIC_Z,
    FLI_STD_LOGIC_W,
    FLI_STD_LOGIC_L,
    FLI_STD_LOGIC_H,
    FLI_STD_LOGIC_DASH
};

enum FliSigType {
    FLI_TYPE_BIT,
    FLI_TYPE_BIT_VECTOR,
    FLI_TYPE_STD_LOGIC,
    FLI_TYPE_STD_LOGIC_VECTOR,
    FLI_TYPE_INTEGER,
    FLI_TYPE_UNSUPPORTED
};
"""    

def printStructs(top, inputs, outputs, bidis, stream = sys.stdout):
    "Print necessary structs"

    print >> stream, """struct %s_fli_struct
{
    // Carbon data
    CarbonObjectID *obj;""" % top
    for x in inputs + outputs + bidis:
        print >> stream, "    CarbonNetID *carbon_%s;" % x.leafName()

    print >> stream, """
    // MTI data"""
    for x in inputs + outputs + bidis:
        print >> stream, "    mtiSignalIdT mti_%s_sig;" % x.leafName()
    for x in outputs + bidis:
        print >> stream, "    mtiDriverIdT mti_%s_drv;" % x.leafName()
    
    print >> stream, """    mtiProcessIdT sched_func;
};

struct %s_input_struct
{
    CarbonObjectID *obj;
    CarbonNetID *carbon_sig;
    mtiSignalIdT mti_sig;
    void (*val_func)(mtiSignalIdT, UInt32*, UInt32*);
    mtiProcessIdT sched_func;

    %s_input_struct(CarbonObjectID *o, CarbonNetID *c, mtiSignalIdT m, void (*f)(mtiSignalIdT, UInt32*, UInt32*), mtiProcessIdT s)
        : obj(o), carbon_sig(c), mti_sig(m), val_func(f), sched_func(s)
    {}
};
""" % (top, top)

def printProtos(inputs, bidis, stream = sys.stdout):
    "Print prototypes for functions to be defined later"

    # first, the standard ones
    print >> stream, """
static FliSigType getSigType(mtiSignalIdT sig);
static void get_val_bit(mtiSignalIdT sig, UInt32 *val, UInt32 *drive);
template <int W>
static void get_val_bit_vector(mtiSignalIdT sig, UInt32 *val, UInt32 *drive);
static void get_val_std_logic(mtiSignalIdT sig, UInt32 *val, UInt32 *drive);
template <int W>
static void get_val_std_logic_vector(mtiSignalIdT sig, UInt32 *val, UInt32 *drive);
static void get_val_integer(mtiSignalIdT sig, UInt32 *val, UInt32 *drive);
static void output_func_bit(CarbonObjectID*, CarbonNetID*, void *data, UInt32 *val, UInt32*);
template <int W>
static void output_func_bit_vector(CarbonObjectID*, CarbonNetID*, void *data, UInt32 *val, UInt32*);
static void output_func_std_logic(CarbonObjectID*, CarbonNetID*, void *data, UInt32 *val, UInt32 *drive);
template <int W>
static void output_func_std_logic_vector(CarbonObjectID*, CarbonNetID*, void *data, UInt32 *val, UInt32 *drive);
static void output_func_integer(CarbonObjectID*, CarbonNetID*, void *data, UInt32 *val, UInt32*);
static void schedule_func(void *data);
static void exit_func(void *data);
static void delete_input_func(void *data);
"""

    # now, the sensitivity functions for each input/inout
    for x in inputs + bidis:
        print >> stream, "static void input_func_%s(void *data);" % x.leafName()
        
    
def printInit(design, top, inputs, outputs, bidis, stream = sys.stdout):
    "Print the main init function"

    print >> stream, """
extern "C" {
    void %s_init(mtiRegionIdT region,
                 char *param,
                 mtiInterfaceListT *generics,
                 mtiInterfaceListT *ports)
    {
        // allocate/initialize
        %s_fli_struct *p = new %s_fli_struct;
        p->obj = carbon_%s_create(eCarbonFullDB, eCarbon_NoFlags);""" % (top, top, top, design)

    for x in inputs + outputs + bidis:
        print >> stream, "        p->carbon_%s = carbonFindNet(p->obj, \"%s.%s\");" % (x.leafName(), top, x.leafName())

    print >> stream, """
        CarbonWaveID *vcd = carbonWaveInitVCD(p->obj, "%s.vcd", e1ns);
        carbonDumpVars(vcd, 0, "%s");
""" % (top, top)

    for x in inputs + outputs + bidis:
        print >> stream, "        p->mti_%s_sig = mti_FindPort(ports, \"%s\");" % (x.leafName(), x.leafName())
    for x in outputs + bidis:
        print >> stream, "        p->mti_%s_drv = mti_CreateDriver(p->mti_%s_sig);" % (x.leafName(), x.leafName())

    print >> stream, """
        // register exit function to cleanup
        mti_AddRestartCB(exit_func, p);
        mti_AddQuitCB(exit_func, p);
        
        // register schedule function
        p->sched_func = mti_CreateProcessWithPriority(0, schedule_func, p->obj, MTI_PROC_IMMEDIATE);

        mtiProcessIdT proc;
        %s_input_struct *in;
        FliSigType type;
""" % top

    # determine the type of each input, setting up the MTI->Carbon transfer functionality
    for x in inputs + bidis:
        sig = x.leafName()
        print >> stream, "        // register function to respond to changes on %s" % sig
        print >> stream, "        type = getSigType(p->mti_%s_sig);" % sig
        # determine what type of signal Carbon thinks this is
        if x.isScalar():
            # print code to detect bit/std_logic and use the appropriate function
            print >> stream, """        if (type == FLI_TYPE_BIT)
            in = new myreg_input_struct(p->obj, p->carbon_%s, p->mti_%s_sig, get_val_bit, p->sched_func);
        else if (type == FLI_TYPE_STD_LOGIC)
            in = new myreg_input_struct(p->obj, p->carbon_%s, p->mti_%s_sig, get_val_std_logic, p->sched_func);""" % (sig, sig, sig, sig)
        elif x.isVector():
            # print code to detect bit/std_logic and use the appropriate function
            width = x.getWidth()
            print >> stream, """        if (type == FLI_TYPE_BIT_VECTOR)
            in = new myreg_input_struct(p->obj, p->carbon_%s, p->mti_%s_sig, get_val_bit_vector<%d>, p->sched_func);
        else if (type == FLI_TYPE_STD_LOGIC_VECTOR)
            in = new myreg_input_struct(p->obj, p->carbon_%s, p->mti_%s_sig, get_val_std_logic_vector<%d>, p->sched_func);
        else if (type == FLI_TYPE_INTEGER)
            in = new myreg_input_struct(p->obj, p->carbon_%s, p->mti_%s_sig, get_val_integer, p->sched_func);""" % (sig, sig, width, sig, sig, width, sig, sig)
        else:
            print "Unsupported type for input", sig
            sys.exit(1)
        # the rest is common
        print >> stream, """        else {
            in = 0;
            std::cout << "Unsupported/unknown type for input %s" << std::endl;
            mti_FatalError();
        }
        proc = mti_CreateProcessWithPriority(0, input_func_%s, in, MTI_PROC_NORMAL);
        mti_Sensitize(proc, p->mti_%s_sig, MTI_EVENT);
        mti_AddRestartCB(delete_input_func, in);
        mti_AddQuitCB(delete_input_func, in);
""" % (sig, sig, sig)

    # time for outputs - similar to inputs
    for x in outputs + bidis:
        sig = x.leafName()
        numwords = int((x.getWidth() + 31) / 32)
        print >> stream, "        // register callback function for output %s" % sig
        print >> stream, "        UInt32 %s_val[%d], %s_drive[%d];" % (sig, numwords, sig, numwords)
        print >> stream, "        carbonExamine(p->obj, p->carbon_%s, %s_val, %s_drive);" % (sig, sig, sig)
        print >> stream, "        type = getSigType(p->mti_%s_sig);" % sig
        # determine what type of signal Carbon thinks this is
        if x.isScalar():
            # print code to detect bit/std_logic and use the appropriate function
            print >> stream, """        if (type == FLI_TYPE_BIT) {
            output_func_bit(p->obj, p->carbon_%s, p->mti_%s_drv, %s_val, 0);
            carbonAddNetValueChangeCB(p->obj, output_func_bit, p->mti_%s_drv, p->carbon_%s);
        } else if (type == FLI_TYPE_STD_LOGIC) {
            output_func_std_logic(p->obj, p->carbon_%s, p->mti_%s_drv, %s_val, %s_drive);
            carbonAddNetValueChangeCB(p->obj, output_func_std_logic, p->mti_%s_drv, p->carbon_%s);""" % (sig, sig, sig, sig, sig, sig, sig, sig, sig, sig, sig)
        elif x.isVector():
            # print code to detect bit/std_logic and use the appropriate function
            width = x.getWidth()
            print >> stream, """        if (type == FLI_TYPE_BIT_VECTOR) {
            output_func_bit_vector<%d>(p->obj, p->carbon_%s, p->mti_%s_drv, %s_val, 0);
            carbonAddNetValueChangeCB(p->obj, output_func_bit_vector<%d>, p->mti_%s_drv, p->carbon_%s);
        } else if (type == FLI_TYPE_STD_LOGIC_VECTOR) {
            output_func_std_logic_vector<%d>(p->obj, p->carbon_%s, p->mti_%s_drv, %s_val, %s_drive);
            carbonAddNetValueChangeCB(p->obj, output_func_std_logic_vector<%d>, p->mti_%s_drv, p->carbon_%s);
        } else if (type == FLI_TYPE_INTEGER) {
            output_func_integer(p->obj, p->carbon_%s, p->mti_%s_drv, %s_val, 0);
            carbonAddNetValueChangeCB(p->obj, output_func_integer, p->mti_%s_drv, p->carbon_%s);""" % (width, sig, sig, sig, width, sig, sig, width, sig, sig, sig, sig, width, sig, sig, sig, sig, sig, sig, sig)
        else:
            print "Unsupported type for output", sig
            sys.exit(1)
        # the rest is common
        print >> stream, """        } else {
            std::cout << "Unsupported/unknown type for output %s" << std::endl;
            mti_FatalError();
        }
""" % sig

    print >> stream, """
    }
}"""

def printFuncs(top, inputs, bidis, stream = sys.stdout):
    "Print body of all the functions"

    # first, the standard ones
    print >> stream, """
static FliSigType getSigType(mtiSignalIdT sig)
{
    mtiTypeIdT type = mti_GetSignalType(sig);
    char **enum_values;

    switch (mti_GetTypeKind(type)) {
    case MTI_TYPE_ENUM:
        // This can be either bit or std_logic
        enum_values = mti_GetEnumValues(type);
        if ((strcmp(enum_values[0], "'0'") == 0) && (strcmp(enum_values[1], "'1'") == 0))
            return FLI_TYPE_BIT;
        if ((strcmp(enum_values[FLI_STD_LOGIC_0], "'0'") == 0) && (strcmp(enum_values[FLI_STD_LOGIC_1], "'1'") == 0))
            return FLI_TYPE_STD_LOGIC;
        return FLI_TYPE_UNSUPPORTED;

    case MTI_TYPE_ARRAY:
        // This can be either bit_vector or std_logic_vector
        // Get the subtype and compare as with the ENUM type above
        type = mti_GetArrayElementType(type);
        if (mti_GetTypeKind(type) != MTI_TYPE_ENUM)
            return FLI_TYPE_UNSUPPORTED;

        enum_values = mti_GetEnumValues(type);
        if ((strcmp(enum_values[0], "'0'") == 0) && (strcmp(enum_values[1], "'1'") == 0))
            return FLI_TYPE_BIT_VECTOR;
        if ((strcmp(enum_values[FLI_STD_LOGIC_0], "'0'") == 0) && (strcmp(enum_values[FLI_STD_LOGIC_1], "'1'") == 0))
            return FLI_TYPE_STD_LOGIC_VECTOR;
        return FLI_TYPE_UNSUPPORTED;

    case MTI_TYPE_SCALAR:
        return FLI_TYPE_INTEGER;
        
    default:
        return FLI_TYPE_UNSUPPORTED;
    }
}

static void get_val_bit(mtiSignalIdT sig, UInt32 *val, UInt32 *drive)
{
    char mti_val;
    mti_GetSignalValueIndirect(sig, &mti_val);
    *val = mti_val;
    *drive = 0;
}

template <int W>
static void get_val_bit_vector(mtiSignalIdT sig, UInt32 *val, UInt32 *drive)
{
    char mti_val[W];
    mti_GetSignalValueIndirect(sig, mti_val);

    if (W > 32) {
        UInt32 temp_val[(W+31)/32] = {0};
        for (int i = 0; i < W; ++i)
            if (mti_val[(W-1)-i] == 1)
                temp_val[i/32] |= (1 << (i%%32));

        memcpy(val, temp_val, ((W+31)/32) * sizeof(UInt32));
        memset(drive, 0, ((W+31)/32) * sizeof(UInt32));
    } else {
        UInt32 temp_val = 0;
        for (int i = 0; i < W; ++i)
            if (mti_val[(W-1)-i] == 1)
                temp_val |= (1 << i);

        *val = temp_val;
        *drive = 0;
    }
}

static void get_val_std_logic(mtiSignalIdT sig, UInt32 *val, UInt32 *drive)
{
    char mti_val;
    mti_GetSignalValueIndirect(sig, &mti_val);
    *val = (mti_val == FLI_STD_LOGIC_1) || (mti_val == FLI_STD_LOGIC_H);
    *drive = (mti_val == FLI_STD_LOGIC_Z) || (mti_val == FLI_STD_LOGIC_W);
}

template <int W>
static void get_val_std_logic_vector(mtiSignalIdT sig, UInt32 *val, UInt32 *drive)
{
    char mti_val[W];
    mti_GetSignalValueIndirect(sig, mti_val);

    if (W > 32) {
        UInt32 temp_val[(W+31)/32] = {0};
        UInt32 temp_drive[(W+31)/32] = {0};
        for (int i = 0; i < W; ++i) {
            if ((mti_val[(W-1)-i] == FLI_STD_LOGIC_1) || (mti_val[(W-1)-i] == FLI_STD_LOGIC_H))
                temp_val[i/32] |= (1 << (i%%32));
            if ((mti_val[(W-1)-i] == FLI_STD_LOGIC_Z) || (mti_val[(W-1)-i] == FLI_STD_LOGIC_W))
                temp_drive[i/32] |= (1 << (i%%32));
        }

        memcpy(val, temp_val, ((W+31)/32) * sizeof(UInt32));
        memcpy(drive, temp_drive, ((W+31)/32) * sizeof(UInt32));
    } else {
        UInt32 temp_val = 0;
        UInt32 temp_drive = 0;
        for (int i = 0; i < W; ++i) {
            if ((mti_val[(W-1)-i] == FLI_STD_LOGIC_1) || (mti_val[(W-1)-i] == FLI_STD_LOGIC_H))
                temp_val |= (1 << i);
            if ((mti_val[(W-1)-i] == FLI_STD_LOGIC_Z) || (mti_val[(W-1)-i] == FLI_STD_LOGIC_W))
                temp_drive |= (1 << i);
        }

        *val = temp_val;
        *drive = temp_drive;
    }
}

static void get_val_integer(mtiSignalIdT sig, UInt32 *val, UInt32 *drive)
{
    *val = static_cast<UInt32>(mti_GetSignalValue(sig));
    *drive = 0;
}

static void output_func_bit(CarbonObjectID*, CarbonNetID*, void *data, UInt32 *val, UInt32*)
{
    // apply the new output
    mtiDriverIdT id = reinterpret_cast<mtiDriverIdT>(data);
    char mti_val = *val;
    mti_ScheduleDriver(id, mti_val, 0, MTI_INERTIAL);
}

template <int W>
static void output_func_bit_vector(CarbonObjectID*, CarbonNetID*, void *data, UInt32 *val, UInt32*)
{
    // apply the new output
    mtiDriverIdT id = reinterpret_cast<mtiDriverIdT>(data);
    char mti_val[W];
    for (int i = 0; i < W; ++i)
        if (val[i/32] & (1 << (i %% 32)))
            mti_val[(W-1)-i] = 1;
        else
            mti_val[(W-1)-i] = 0;

    mti_ScheduleDriver(id, reinterpret_cast<long>(mti_val), 0, MTI_INERTIAL);
}

static void output_func_std_logic(CarbonObjectID*, CarbonNetID*, void *data, UInt32 *val, UInt32 *drive)
{
    // apply the new output
    mtiDriverIdT id = reinterpret_cast<mtiDriverIdT>(data);
    char mti_val = (drive && *drive) ? FLI_STD_LOGIC_Z : ((*val) ? FLI_STD_LOGIC_1 : FLI_STD_LOGIC_0);
    mti_ScheduleDriver(id, mti_val, 0, MTI_INERTIAL);
}

template <int W>
static void output_func_std_logic_vector(CarbonObjectID*, CarbonNetID*, void *data, UInt32 *val, UInt32 *drive)
{
    // apply the new output
    mtiDriverIdT id = reinterpret_cast<mtiDriverIdT>(data);
    char mti_val[W];
    for (int i = 0; i < W; ++i)
        if (drive && (drive[i/32] & (1 << (i %% 32))))
            mti_val[(W-1)-i] = FLI_STD_LOGIC_Z;
        else if (val[i/32] & (1 << (i %% 32)))
            mti_val[(W-1)-i] = FLI_STD_LOGIC_1;
        else
            mti_val[(W-1)-i] = FLI_STD_LOGIC_0;

    mti_ScheduleDriver(id, reinterpret_cast<long>(mti_val), 0, MTI_INERTIAL);
}

static void output_func_integer(CarbonObjectID*, CarbonNetID*, void *data, UInt32 *val, UInt32*)
{
    // apply the new output
    mtiDriverIdT id = reinterpret_cast<mtiDriverIdT>(data);
    long mti_val = static_cast<long>(*val);
    mti_ScheduleDriver(id, mti_val, 0, MTI_INERTIAL);
}

static void schedule_func(void *data)
{
    // Run model
    CarbonObjectID *obj = reinterpret_cast<CarbonObjectID*>(data);
    CarbonTime t = (static_cast<CarbonTime>(mti_NowUpper()) << 32) | static_cast<CarbonTime>(mti_Now());
    carbonSchedule(obj, t);
}

void exit_func(void *data)
{
    // cleanup
    %s_fli_struct *p = reinterpret_cast<%s_fli_struct*>(data);
    if (p->obj)
        carbonDestroy(&p->obj);
    delete p;
}

static void delete_input_func(void *data)
{
    // cleanup
    %s_input_struct *p = reinterpret_cast<%s_input_struct*>(data);
    if (p)
        delete p;
}"""% (top, top, top, top)

    # now, the sensitivity functions for each input
    for x in inputs + bidis:
        numwords = int((x.getWidth() + 31) / 32)
        print >> stream, """
static void input_func_%s(void *data)
{
    // apply the new input data to the model
    %s_input_struct *p = reinterpret_cast<%s_input_struct*>(data);

    UInt32 val[%d], drive[%d];""" % (x.leafName(), top, top, numwords, numwords)

        # If this is a bidi, this function will be called as a result of Carbon driving
        # an output value onto this signal.  We need to detect this and not re-deposit.
        # NOTE: This checks that all bits are driven, so if the net is partially driven,
        # this won't work.

        if x.isPrimaryBidi():
            print >> stream, """    // don't feed back the value we're driving out
    carbonExamine(p->obj, p->carbon_sig, val, drive);
    for (int i = 0; i < %d; ++i)
        if (drive[i] == 0)
            return;""" % numwords

        print >> stream, """    p->val_func(p->mti_sig, val, drive);
    carbonDeposit(p->obj, p->carbon_sig, val, drive);"""

        # if this is a clock, we need to schedule the model
        if (x.isClk()):
            print >> stream, """
    // schedule the model to be run
    mti_ScheduleWakeup(p->sched_func, 0);"""
        print >> stream, "}"


def printArch(top, stream = sys.stdout):
    "Print FLI architecture"

    print >> stream, """-- Carbon-generated fli architecture
architecture fli of %s is
  attribute foreign : string;
  attribute foreign of fli : architecture is "%s_init %s_fli.so";
begin
end fli;
""" % (top, top, top)

# main
def script_functionality(args):
    "main functionality"

    if len(args) != 2:
        print "usage: GenFliWrapper database"
        sys.exit(1)

    db = Carbon.DB.DBContext(args[1])

    if db:
        design = db.getInterfaceName()
        top = db.getTopLevelModuleName()
        inputs = []
        for x in db.iterInputs():
            inputs.append(x)
        outputs = []
        for x in db.iterOutputs():
            outputs.append(x)
        bidis = []
        for x in db.iterBidis():
            bidis.append(x)

        # create the C++ file
        cxxfile = open("%s_fli.cxx" % top, "w")

        printIncludes(design, cxxfile)
        printEnums(cxxfile)
        printStructs(top, inputs, outputs, bidis, cxxfile)
        printProtos(inputs, bidis, cxxfile)
        printInit(design, top, inputs, outputs, bidis, cxxfile)
        printFuncs(top, inputs, bidis, cxxfile)

        cxxfile.close()

        # create a VHDL file with the FLI architecture
        archfile = open ("%s_fli_arch.vhd" % top, "w")
        printArch(top, archfile)
        archfile.close()
    
    else:
        print "Error reading design database"
        sys.exit(1)


if __name__ == '__main__':
    script_functionality(sys.argv)
