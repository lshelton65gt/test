entity myreg is
  
  port (
    clk1 : in  bit;
    clk2 : in  bit;
    i   : in  integer;
    o   : out integer;
    obar: out integer;
    comp: out bit);

end myreg;

architecture rtl of myreg is

  signal otemp : integer := 0;
  signal obartemp : integer := 0;
  signal oreg : integer := 0;
  signal obarreg : integer := 0;

begin  -- arch

  process (clk1)
  begin
    if clk1'event and clk1 = '1' then
      otemp <= i;
      obarreg <= 0 - (obartemp + 1);
    end if;
  end process;

  process (clk2)
  begin
    if clk2'event and clk2 = '1' then
      oreg <= otemp;
      obartemp <= i;
    end if;
  end process;
  
  o <= oreg;
  obar <= obarreg;
  comp <= '0' when (oreg > obarreg) else '1';

end rtl;
