library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity test is
  
end test;

architecture arch of test is

  component myreg
    port (
      clk1 : in  std_logic;
      clk2 : in  std_logic;
      i   : in  std_logic_vector (63 downto 0);
      o   : out std_logic_vector (63 downto 0);
      obar: out std_logic_vector (63 downto 0);
      comp: out std_logic);
  end component;

  for u1 : myreg use entity work.myreg(rtl);
--  for u2 : myreg use entity work.myreg(rtl);
  for u2 : myreg use entity work.myreg(fli);
  
signal clk : std_logic := '0';
signal num : std_logic_vector (63 downto 0) := (others => '0');
signal o1 : std_logic_vector (63 downto 0) := (others => '0');
signal o2 : std_logic_vector (63 downto 0) := (others => '0');
signal obar1 : std_logic_vector (63 downto 0) := (others => '0');
signal obar2 : std_logic_vector (63 downto 0) := (others => '0');
signal comp1 : std_logic;
signal comp2 : std_logic;

begin  -- arch

  u1 : myreg
    port map (
      clk1 => clk,
      clk2 => clk,
      i   => num,
      o   => o1,
      obar   => obar1,
      comp => comp1);

  u2 : myreg
    port map (
      clk1 => clk,
      clk2 => clk,
      i   => num,
      o   => o2,
      obar   => obar2,
      comp => comp2);

  process
   begin
     wait for 1 ns;
     clk <= '1';
     wait for 1 ns;
     clk <= '0';
   end process;

   process (clk)
     variable myline : line;
   begin
     if clk'event and clk = '1' then
       num <= num + 1;
       write(myline, string'("o1 = "));
       write(myline, o1);
       write(myline, string'(", o2 = "));
       write(myline, o2);
       if o1 /= o2 then
         write (myline, string'("  ***DIFF***"));
       end if;
       writeline(OUTPUT, myline);

       write(myline, string'("obar1 = "));
       write(myline, obar1);
       write(myline, string'(", obar2 = "));
       write(myline, obar2);
       if obar1 /= obar2 then
         write (myline, string'("  ***DIFF***"));
       end if;
       writeline(OUTPUT, myline);

       write(myline, string'("comp1 = "));
       write(myline, comp1);
       write(myline, string'(", comp2 = "));
       write(myline, comp2);
       if comp1 /= comp2 then
         write (myline, string'("  ***DIFF***"));
       end if;
       writeline(OUTPUT, myline);
     end if;
   end process;
  
end arch;
