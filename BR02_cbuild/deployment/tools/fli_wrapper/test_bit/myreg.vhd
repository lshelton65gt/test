entity myreg is
  
  port (
    clk1 : in  bit;
    clk2 : in  bit;
    i   : in  bit_vector (7 downto 0);
    o   : out bit_vector (7 downto 0);
    obar: out bit_vector (7 downto 0);
    comp: out bit);

end myreg;

architecture rtl of myreg is

  signal otemp : bit_vector (7 downto 0) := (others => '0');
  signal obartemp : bit_vector (7 downto 0) := (others => '0');
  signal oreg : bit_vector (7 downto 0) := (others => '0');
  signal obarreg : bit_vector (7 downto 0) := (others => '0');

begin  -- arch

  process (clk1)
  begin
    if clk1'event and clk1 = '1' then
      otemp <= i;
      obarreg <= not (obartemp);
    end if;
  end process;

  process (clk2)
  begin
    if clk2'event and clk2 = '1' then
      oreg <= otemp;
      obartemp <= i;
    end if;
  end process;
  
  o <= oreg;
  obar <= obarreg;
  comp <= '0' when (oreg > obarreg) else '1';

end rtl;
