library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity test is
  
end test;

architecture arch of test is

  component myreg
    port (
      clk : in  std_logic;
      io1 : inout std_logic_vector (7 downto 0);
      io2 : inout std_logic_vector (7 downto 0);
      dir1 : in std_logic;
      dir2 : in std_logic);
  end component;

  for u1 : myreg use entity work.myreg(rtl);
--  for u2 : myreg use entity work.myreg(rtl);
  for u2 : myreg use entity work.myreg(fli);
  
signal clk : std_logic := '0';
signal num : std_logic_vector (7 downto 0) := (others => '0');
signal io11  : std_logic_vector (7 downto 0) := (others => '0');
signal io21  : std_logic_vector (7 downto 0) := (others => '0');
signal io12  : std_logic_vector (7 downto 0) := (others => '0');
signal io22  : std_logic_vector (7 downto 0) := (others => '0');
signal dir1 : std_logic := '0';
signal dir2 : std_logic := '0';
  
begin  -- arch

  dir1 <= not(num(0)) and not(num(1));
  dir2 <= not(num(0)) and num(1);
  io11 <= num when dir1 = '1' else (others => 'Z');
  io12 <= num when dir1 = '1' else (others => 'Z');
  io21 <= num when dir2 = '1' else (others => 'Z');
  io22 <= num when dir2 = '1' else (others => 'Z');

  u1 : myreg
    port map (
      clk => clk,
      io1 => io11,
      io2 => io21,
      dir1 => dir1,
      dir2 => dir2);

  u2 : myreg
    port map (
      clk => clk,
      io1 => io12,
      io2 => io22,
      dir1 => dir1,
      dir2 => dir2);

  process
   begin
     wait for 1 ns;
     clk <= '1';
     wait for 1 ns;
     clk <= '0';
   end process;

   process (clk)
     variable myline : line;
     variable i : integer;
   begin
     if clk'event and clk = '1' then
       num <= num + 1;
       write(myline, string'("io11 = "));
       write(myline, io11);
       write(myline, string'(", io12 = "));
       write(myline, io12);
       for i in io11'range loop
         if io11(i) /= io12(i) then
           write (myline, string'("  ***DIFF***"));
           exit;
         end if;
       end loop;
       writeline(OUTPUT, myline);

       write(myline, string'("io21 = "));
       write(myline, io21);
       write(myline, string'(", io22 = "));
       write(myline, io22);
       for i in io21'range loop
         if io21(i) /= io22(i) then
           write (myline, string'("  ***DIFF***"));
           exit;
         end if;
       end loop;
       writeline(OUTPUT, myline);
     end if;
   end process;
  
end arch;
