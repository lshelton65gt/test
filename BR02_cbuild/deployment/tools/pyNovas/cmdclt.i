%include cpointer.i
%include cmalloc.i

%module cmdclt
%{
/* Includes the header in the wrapper code */
#include "/tools/novas/verdi/share/libcmd/cmdclt.h"
%}

/* the cmdclt lib passes pointers into the library 
   functions to get return values.  We need to allocate those
   pointers (char**) */
%malloc(char*, charp)

/* Parse the header file to generate wrappers */
%include "/tools/novas/verdi/share/libcmd/cmdclt.h"