#!/usr/bin/perl

# Parse Vera files with "hdl_node", extract the external portion and generate
#  cbuild mvObserveSignal directives

# 5/16/08 ehm Initial release.

# Usages:

# parse_vera [-I<dir>] [-D<macro>] [-D<macro>=<value>] filename ...

# This script preprocess #define, #ifdef, #ifndef, #else, #endif, #include
# so that only the pertinant hdl_nodes are read.  So, you can
# set one or more include paths and one or more macro definitions on the
# command line.

# REQUIRES Perl 5.6 or higher


# Way-cool balanced parens regex
# (Modified version of http://perl.plover.com/yak/regex/samples/slide083.html)
# (The multiline version doesn't work)
$BALPAREN = qr/\((?{local$d=1})(?:\((?{$d++})|\)(?{$d--})(?(?{$d<0})(?!))|(?>[^()]*))*?(?(?{$d!=0})(?!))/;

# Parse SVA bind statement
#                      inst name              optional params              args
$BINDSTMT = qr/bind\s+(\w[\w\.]*\w)\s+\w+\s*(?:\#\s*$BALPAREN\s+)?\w+\s*($BALPAREN)\s*;/s;

# Parse Vera "hdl_node" statement
#  [...]
$BALBRACKETS = qr/\[[^\]]+\]/s;
# input [31:0] name type another_name hdl_node "path";
#  (We want to extract "input" and the path)
#$HDL_NODE = qr/(input|output)\s*(?:$BALBACKETS)?\s*\w+\s+\w+\s+hdl_node\s+\"([^"]+)\";/s;
$HDL_NODE = qr/(input|output)[^;]+hdl_node\s+\"([^"]+)\";/s;

# Quick-and-dirty way of reading files given on the command line
$text = "";
$IS_EATING = 0;
$CURRENT_NESTING_LEVEL = 0;
$HANDLE=0;
push (@INCLUDES, ".");

foreach (@ARGV) {
  if (/^-D(\w+)$/) {
    $MACROS{$1} = "";
  }
  elsif (/^-D(\w+)=(.+)$/) {
    $MACROS{$1} = $2;
  }
  elsif (/^-I(.+)$/) {
    if (-d $1) {
      push (@INCLUDES, $1);
    }
    else {
      print ("Warning: -I path \"$1\" does not exist\n");
    }
  }
  else {
    if (-f) {
      push (@FILELIST, $_);
    }
    else {
      die "File \"$_\" does not exist\n";
    }
  }
}

foreach (@FILELIST) {
  parse_file ($_);
}


sub parse_file {
  my $filename = shift;
  my $infile_handle = "HANDLE$HANDLE";
  $HANDLE++;
  open ($infile_handle, "<$filename");

#  $infile_handle = INFILE;

  &parse_handle ($infile_handle);
  close INFILE;
}

sub parse_handle {
  my $handle = shift;
  my $text = "";
  while (<$handle>){
    $text .= &preprocess($_);		# Assumes #define, etc are on a single line
  }

  # NOTE: these things will fool this simple parser:
  # - A comment sequence // or /* inside of quotes, because
  #   the comments are stripped first without regard for
  #   looking inside quote marks


  # Strip line commands
  $text =~ s://.*$::gm;

  # Strip global comments
  $text =~ s:/\*.*?\*/::gs;

  # Preprocess, stripping undefined ifdefs
  #(Later)

  # Strip everything in quotes (and handle escaped quotes)
  #$text =~ s:\"[^"\\]*(\\.[^"\\]*)*\":"":gs

  # Do the parsing
  &parse_vera_text($text);

  # %OBS contains the observable signals.  The hash key is the entity name,
  # the value is an array of the signal names it found.
  foreach $path (keys (%OBS)) {
    print "mvObserveSignal $path\n";
  }

}

# Given a text stream containing one or more bind statements,
#  extract the bind statements and get out of them an instance/entity
#  name, and an list of signals
sub parse_vera_text {
  my $text = shift;

# Find all the bind statements, and eat them when we see them
  while ($text =~ s/$HDL_NODE//s) {
    my $inputoutput = $1;
    my $path = $2;

    if ($inputoutput eq "input" && !exists ($OBS{$path})) {
      $OBS{$path} = 1;
    }
  }
}

sub preprocess {
  my $text = shift;
  my $returnval = "";
  my $newtext;

  if ($IN_BLOCK_COMMENT) {
    if ($text =~ s:^.*\*/::) {	# end of multi-line comment
      $IN_BLOCK_COMMENT = 0;
    }
    else {
      return "";
    }
  }

  $text =~ s://.*$::;		# Remove line comments
  $text =~ s:/\*.*?\*/::g;	# One-line block comment
  if ($text =~ s:/\*.*$::) {	# Start of multi-line comment
    $IN_BLOCK_COMMENT = 1;
  }

  # Is this a #something?
  if ($text =~ s/^\s*#//) {
    # yes...
    if ($text =~ /^if(n)?def\s+(\w+)/) {
      $macroname = $2;
      $n = $1;

      $CURRENT_NESTING_LEVEL++;

      &debug_print ("In if${n}def, macro is $macroname, ");
      $macro_defined = exists $MACROS{$macroname};
      if (($macro_defined && $n eq "n") || (!$macro_defined && $n ne "n")) {
	&debug_print ("eating\n");
	$IS_EATING = $CURRENT_NESTING_LEVEL;
	$ELSE_EAT[$CURRENT_NESTING_LEVEL] = 0;
      }
      elsif (IS_EATING == 0) {
	&debug_print ("processing\n");
	$ELSE_EAT[$CURRENT_NESTING_LEVEL] = 1;
      }
      else {
	&debug_print ("still eating\n");
	$ELSE_EAT[$CURRENT_NESTING_LEVEL] = 0;
      }
    }
    elsif ($text =~ /^endif/) {
      $ELSE_EAT[$CURRENT_NESTING_LEVEL] = 0;
      $CURRENT_NESTING_LEVEL--;
      debug_print ("endif, now at level $CURRENT_NESTING_LEVEL, eating is at $IS_EATING\n");
    }
    elsif ($text =~ /^else/) {
      debug_print ("in else, ");
      if ($ELSE_EAT[$CURRENT_NESTING_LEVEL]) {
	&debug_print ("eating\n");
	$IS_EATING = $CURRENT_NESTING_LEVEL;
	$ELSE_EAT[$CURRENT_NESTING_LEVEL] = 0;
      }
      else {
	&debug_print ("processing\n");
	$IS_EATING = 0;
      }
    }
    # These directives are ignored if eating
    if ($IS_EATING == 0) {
      if ($text =~ /^define\s+(\w+)\s+?(.+?)$/) {
	$MACROS{$1} = &macro_expand($2);
      }
      elsif ($text =~ /^define\s+(\w+)$/) {
	$MACROS{$1} = "";
      }
      elsif ($text =~ /^include\s+\"([^"]+)\"/) {
	&push_include ($1);
      }
      else {
	$returnval = $text;
      }
    }
    # See if it is time to stop eating
    elsif ($IS_EATING > $CURRENT_NESTING_LEVEL) {
      $IS_EATING = 0;
    }
  }
  # Normal case: no "#"
  elsif ($IS_EATING == 0) {
    $newtext = &macro_expand($text);
    $returnval = $newtext;
  }
  elsif ($IS_EATING > $CURRENT_NESTING_LEVEL) {
    $IS_EATING = 0;
  }
  &debug_print ("$returnval");
  return $returnval;
}

sub macro_expand {
  my $text = shift;
  my $newtext = $text;
  foreach $macroname (keys (%MACROS)) {
    $newtext =~ s/\b$macroname\b/$MACROS{$macroname}/g;
  }
  return $newtext;
}

sub debug_print {
  if ($DEBUG) {
    print (@_);
  }
}

sub push_include {
  my $path = shift;
  my $processed = 0;
  foreach my $incpath (@INCLUDES) {
    if (-f "$incpath/$path") {
      $processed = 1;
      &parse_file ("$incpath/$path");
      last;			# Do just one
    }
  }
  if ($processed == 0) {
    print ("Warning: include file \"$path\" not found in:\n");
    foreach my $incpath (@INCLUDES) {
      print ("  \"$incpath\"\n");
    }
  }
}

