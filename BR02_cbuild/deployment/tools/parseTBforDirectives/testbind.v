// This is a Verilog-like file with some test binds

/* This is
 * a
 * global
 * comment
 */

  bind vqdec_mc reset_checker #(.BIT_WIDTH(4), .ASSERT_SEVERITY (2) ,
. err_msg ("vdec reset check failed"))
       vdec_mc_reset_sram_pred_buf_csb (.clk(clk), .reset(reset) , .reset_state(0)) ;

// This is a comment

  bind vdec_mcyy1 reset_checker #(.BIT_WIDTH(4), .ASSERT_SEVERITY (2) ,
. err_msg ("vdec reset check failed"))
       vydec_mc_reset_sram_pred_buf (clk, .reset(reset) , .reset_state(0)) ;

  bind vdec_mcxx2 reset_checker #(.BIT_WIDTH(4), .ASSERT_SEVERITY (2) ,
. err_msg ("vdec reset check failed"))
       vxdec_mc_reset_sram_pred_buf (foo, bar, .reset(reset) , .reset_state(0)) ;



