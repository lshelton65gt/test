from qt import QListViewItem
from qt import QColorGroup

class ColorListItem(QListViewItem):
    def __init__(self):
        self.textColor = None

    def __init__(self, parent, text):
        QListViewItem.__init__(self,parent,text)
        self.textColor = None

    def paintCell(self, p, cg, c, w, a):
        grp = QColorGroup(cg)
        grp.setColor(QColorGroup.Text, self.textColor)
        QListViewItem.paintCell(self, p, grp,c,w,a)
