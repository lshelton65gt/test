#
# -*- Python -*-
#
"""Provide functions that parse various reports from cbuild"""
from tree import Node
from csv import DictReader
import re

def readCosts(costsFile, nameMapper):
    """Read a costs file.  Depends on having access to a function
    that can map names from uniquified version to original name"""
    costs = {}
    costReport = DictReader(costsFile)
    for moduleCost in costReport:
        moduleName = moduleCost['']

        # For the moment ignore the top leve and tasks??
        if moduleName == 'DESIGN' or '.' in moduleName:
            continue

        moduleName = nameMapper(moduleName)
        del moduleCost['']

        # turn all of the text values into integers
        for (key,val) in moduleCost.iteritems():
            moduleCost[key] = int(val)
            
        # save the cost data for the module
        costs[moduleName] = moduleCost

    return costs

def countLeadingSpaces(s):
    """ Count the number of spaces at the begining of a line of text,
    utility function for readHierarchy()"""
    return len(s) - len(s.lstrip())

def readHierarchy(hierFile):
    """reads a libdesign.hierarchy file"""

    sources = {}
    hierFile.readline()
    hierFile.readline()

    unelabMods = []        
    curIndent = 0

    #first line is exceptional...
    topLine = hierFile.readline().split()    
    topNode = Node((topLine[0], topLine[0]))
    unelabMods.append(topLine[0])
    sources[topLine[0]] = topLine[1]
    
    curNode = topNode


    for l in hierFile:
        indent = countLeadingSpaces(l)
        (mod, inst, source) = l.split()
        if indent > curIndent:
            # Greater means this is a child
            n = Node((mod,inst), curNode)
            curNode.children.append(n)
            curNode = n
        elif indent < curIndent:
            # less means we've reached a sibling of an ancestor
            # figure out home many levels up to jump up
            # the indent diff gets us to our last sibling,
            # but we'll be looking for our parent so we need to go one more
            levelsToParent = (prevIndent - indent) / 2 + 1
            node = curNode
            for i in range(levelsToParent):
                node = node.parent

            n = Node((mod,inst), node)
            sources[mod] = source
            node.children.append(n)
            curNode = n
        else:
            # sibling of curNode
            n = Node(l.split(), curNode.parent)
            curNode.parent.children.append(n)
            curNode = n
            sources[mod] = source
        if curNode.moduleName not in unelabMods:
            unelabMods.append(curNode.moduleName)

        curIndent = indent
        prevIndent = indent
        
    sourceLocator = {} # Placeholder...
    return (topNode, unelabMods, sources)
    #self.tree = topNode
    #self.modules = unelabMods

def readFlattenData(flattenReport, nameMapper):
    """Reads flattening file, adds data to provided design info"""
    pat = re.compile("instance (?P<childElab>([0-9A-Za-z_]+)) of module (?P<childUnelab>[0-9A-Za-z_#]+) into module (?P<parent>[0-9A-Za-z_#]+) \((?P<status>(SUCCESS)|(FAILURE))\)")
    data = {}
    for l in flattenReport:
        m = pat.search(l)
        if m:
            childName = nameMapper(m.group('childUnelab'))
            parentName = nameMapper(m.group('parent'))
            
            rec = (parentName, childName, m.group('childElab'))

            if m.group('status') == "SUCCESS":
                data[rec] = True
            else:
                data[rec] = False
        else:
            pass
    return data

def readParameterMap(mapFile):
    """Loads the mapping of paramterized modules to uniquified module
    name and returns a map where the uniquified name is the key and
    original module name is the value."""
    paramMap = {}
    for line in mapFile:
        tmp = line.strip().split()
        if len(tmp) > 3 and tmp[1] == "->":
            paramMap[tmp[0] ] = tmp[2]

    return paramMap


