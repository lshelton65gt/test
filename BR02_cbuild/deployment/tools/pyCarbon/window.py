from Form1 import Form1
from qt import *
from qtcanvas import *
from carbon import *
from qtAddons import ColorListItem

def addChildNode(parentItem, childNode, designInfo):
    itemText = "%s (%s)" % (childNode.instanceName, childNode.moduleName)

    if designInfo.isFlattened(childNode):
        childNode.listItem = ColorListItem(parentItem, itemText)
        childNode.listItem.textColor = QColor("red")
    else:
        childNode.listItem = QListViewItem(parentItem, itemText)

    for c in childNode.children:
        addChildNode(childNode.listItem, c, designInfo)
    
def addDesignTreeToListView(listView, topNode, designInfo):
    topNode.listItem = QListViewItem(listView, topNode.moduleName)
            
    for c in topNode.children:
        addChildNode(topNode.listItem, c, designInfo)


def updateCosts(listView, topNode):
    topNode.listItem = QListViewItem(listView, topNode.moduleName)

    for c in topNode.children:
        addChildNode(topNode.listItem, c)


def getName(qstr):
    return str(qstr).split()[0]

def getItemHierName(item):
    # Gets called with None also...
    if item:                
        hierName = getName(item.text(0))
        curItem = item
        while curItem.parent():
            curItem = curItem.parent()
            hierName = "%s.%s" % (getName(curItem.text(0)), hierName)

        return hierName

def mapToListView(view, costs):
    """ Display the cost info"""
    view.clear()
    for c in costs:
        QListViewItem(view, c, str(costs[c]))

def loadDesignInfo(info, baseName):
    """Loads design information out of various SPEEDCompiler reports
    and into a DesignInfo class"""
    costFile = open(baseName + '.costs')

    print "Load Hierarchy..."
    hierFile = open(baseName + '.hierarchy')
    info.readHierarchy(hierFile)

    print "Load Parameters..."
    paramFile = open(baseName + '.parameters')
    info.readParameterMap(paramFile)

    print "Load Flattening..."
    flattenReport = open('libdesign.flattening')
    info.readFlattenData(flattenReport)

    print "Load Costs..." 
    info.readCosts(costFile)


class Window(QMainWindow):
    def __init__(self,parent = None,name = None,fl = 0):
        QMainWindow.__init__(self,parent,name,fl)

        self.setCaption("Carbon Cost Viewer")

        if not name:
            self.setName("CostView")

        self.setSizePolicy(QSizePolicy(5,5,0,0,self.sizePolicy().hasHeightForWidth()))
        self.setCentralWidget(QWidget(self,"qt_central_widget"))

        self.designInfo = DesignInfo()
        loadDesignInfo(self.designInfo, 'libdesign')
        
        self.addTreeView()
        
        #docking window for reports
        self.reportDock = QDockWindow(self)
        self.reportDock.setResizeEnabled(True)
        self.reportDock.setOpaqueMoving(True)
        self.reportDock.setCloseMode(QDockWindow.Always)


        self.reportTabs = QTabWidget(self.reportDock,"reportTabs")
        self.reportDock.setWidget(self.reportTabs)
        
        self.moduleCostView = QListView(self.reportTabs, "moduleCostView")
        self.moduleCostView.addColumn("Metric")
        self.moduleCostView.addColumn("Value")
        self.moduleCostView.setGeometry(QRect(0,0,210,370))

        self.reportTabs.insertTab(self.moduleCostView, "Costs")


        # addDesignTreeToListView(self.designView, self.designInfo.tree, self.designInfo)
        addDesignTreeToListView(self.treeView, self.designInfo.tree, self.designInfo)

        self.connect(self.treeView,
                     SIGNAL("mouseButtonClicked(int, QListViewItem *, const QPoint&, int)"),
                     self.designTreeItemClicked)

        # self.graphCanvas = QCanvas(100,100)
        #self.graphView = QCanvasView(self.graphCanvas, self.splitter1);
        
        
        fileMenu = self.menuBar().insertItem("&File")
        # WRONG - fileMenu.insertItem("Open...")
        
        self.menuBar().insertItem("bar")
        self.statusBar().hide()
        

        self.resize(QSize(600,420).expandedTo(self.minimumSizeHint()))
        self.clearWState(Qt.WState_Polished)

    def designTreeItemClicked (self, button, item, pos, col ):
        if item and button == 1:
            name = getItemHierName(item)
            node = self.designInfo.tree.findNodeByHierName(name)
            # print node
            #print self.designInfo.sources[node.moduleName]
            if node.moduleName in self.designInfo.costs:
                mapToListView(self.moduleCostView,
                              self.designInfo.costs[node.moduleName])
            else:
                if self.designInfo.isFlattened(node):
                    self.moduleCostView.clear()
                    QListViewItem(self.moduleCostView, "FLATTENED")
                else:
                    raise "Error finding cost data for %s" % (node,)
                                
            
    def addTreeView(self):
        # Docking Window for design tree
        self.treeDock = QDockWindow(self)
        self.treeDock.setResizeEnabled(True)
        self.treeDock.setOpaqueMoving(True)
        self.treeDock.setCloseMode(QDockWindow.Always)

        self.treeView = QListView(self.treeDock, 'treeView')
        self.treeView.addColumn('Design Tree')
        self.treeView.setRootIsDecorated(True)
        self.treeDock.setWidget(self.treeView)

