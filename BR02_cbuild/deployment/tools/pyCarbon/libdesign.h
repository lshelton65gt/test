/* r2d2_ $Revision: 1.1 $, $Date: 2005/08/31 16:05:11 $ Jun 23, 2005  21:19:27*/

/* C/C++ interface to external code. */
#ifndef __design_H_
#define __design_H_

#ifndef __carbon_capi_h_
#include "carbon/carbon_capi.h"
#endif

#ifdef __cplusplus
#define EXTERNDEF extern "C"
#else
#define EXTERNDEF
#endif
EXTERNDEF CarbonObjectID* carbon_design_create(CarbonDBType dbType, CarbonInitFlags flags);
#endif
