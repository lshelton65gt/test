#
# -*- Python -*-
#

import parsers

class DesignInfo:
    """Class consolidates all design info into one place and provides
    an interface for retrieving any of the data"""
    def __init__(self):
        self.flattenData = {}
        self.costs = {}
        self.tree = None
        self.modules = []
        self.paramMap = {}
        self.sources = {}  # map unelab module to source file
        
    def isFlattened(self, node):
        """ returns true if a module has been flattened """
        if not node.parent:
            #top levels can't be flattened
            return True
        key = (node.parent.moduleName, node.moduleName, node.instanceName)
        return self.flattenData[key]

    def isModule(self, modName):
        """Returns true if modName is the unelaborated name of a module"""
        return modName in self.modules
    
    def getOrigModuleName(self, name):
        """ returns an un-uniquified module name """
        if self.isModule(name):
            return name
        elif name in self.paramMap:
            return self.paramMap[name]
        else:
            raise "Can't find a good name for %s" %(name,)
        
    def readCosts(self, costsFile):
        self.costs = parsers.readCosts(costsFile, self.getOrigModuleName)
            
    def readHierarchy(self, hierFile):
        (self.tree, self.modules, self.sources) = parsers.readHierarchy(hierFile)

    def readFlattenData(self, file):
        self.flattenData = parsers.readFlattenData(file,
                                                   self.getOrigModuleName)

    def readParameterMap(self, mapFile):
        self.paramMap = parsers.readParameterMap(mapFile)
    

    
if __name__=="__main__":
    f = open('libdesign.hierarchy')
    di = DesignInfo()
    di.readHierarchy(f)

    di.tree.printTree()
    
