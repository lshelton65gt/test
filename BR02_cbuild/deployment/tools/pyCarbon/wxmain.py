from wxPython.wx import *
from carbon import *
import wx
import wx.py as py
import sys

def addChildNode(treeView, parentItem, childNode, designInfo):
    itemText = "%s (%s)" % (childNode.instanceName, childNode.moduleName)

    thisItem = treeView.AppendItem(parentItem, itemText)
    if designInfo.isFlattened(childNode):
        pass
    else:
        treeView.SetItemBold(thisItem, True)

    for c in childNode.children:
        addChildNode(treeView, thisItem, c, designInfo)
    
def addDesignTreeToListView(treeView, topNode, designInfo):
    root = treeView.AddRoot("DESIGN")
    treeView.SetItemBold(root, True)
    addChildNode(treeView, root, topNode, designInfo)
    #for c in topNode.children:
    #    addChildNode(treeView, root, c, designInfo)
    treeView.Expand(root)

def loadDesignInfo(info, baseName):
    """Loads design information out of various SPEEDCompiler reports
    and into a DesignInfo class"""
    costFile = open(baseName + '.costs')

    print "Load Hierarchy..."
    hierFile = open(baseName + '.hierarchy')
    info.readHierarchy(hierFile)

    print "Load Parameters..."
    paramFile = open(baseName + '.parameters')
    info.readParameterMap(paramFile)

    print "Load Flattening..."
    flattenReport = open('libdesign.flattening')
    info.readFlattenData(flattenReport)

    print "Load Costs..." 
    info.readCosts(costFile)

ID_ABOUT = 101
ID_EXIT  = 102

class MyFrame(wxFrame):
    def __init__(self, parent, ID, title):
        wxFrame.__init__(self, parent, ID, title,
                         wxDefaultPosition, wxSize(200, 150))
        self.CreateStatusBar()
        self.SetStatusText("This is the statusbar")

        # - --------------- MENU
        menu = wxMenu()
        menu.Append(ID_ABOUT, "&About",
                    "More information about this program")
        menu.AppendSeparator()
        menu.Append(ID_EXIT, "E&xit", "Terminate the program")

        menuBar = wxMenuBar()
        menuBar.Append(menu, "&File");

        self.SetMenuBar(menuBar)


        EVT_MENU(self, ID_ABOUT, self.OnAbout)
        EVT_MENU(self, ID_EXIT,  self.TimeToQuit)

        # -------- Splitter Window
        self.splitH = wxSplitterWindow(self, -1, style = wx.SP_LIVE_UPDATE)
        self.splitV = wxSplitterWindow(self.splitH, -1, style = wx.SP_LIVE_UPDATE)

        #----------- Design Tree
        self.tree = wxTreeCtrl (self.splitV, -1, style=wx.TR_DEFAULT_STYLE)
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnSelChanged, self.tree)

        #----
        self.crust = py.shell.Shell(self.splitH)
        
        #----------- List control
        self.costView = wx.ListCtrl(self.splitV, style = wx.LC_REPORT | wx.LC_EDIT_LABELS | wx.LC_SORT_ASCENDING)
        self.costView.DeleteAllColumns()
        self.costView.InsertColumn(0,'Metric')
        self.costView.InsertColumn(1,'Cost')
        self.costView.SetColumnWidth(0, wx.LIST_AUTOSIZE)
        self.costView.SetColumnWidth(1, wx.LIST_AUTOSIZE)

        self.splitV.SplitVertically(self.tree, self.costView, -100)
        self.splitH.SplitHorizontally(self.splitV, self.crust, -100)

        #------ Load Design Reports
        self.designInfo = DesignInfo()
        loadDesignInfo(self.designInfo, 'libdesign')

        addDesignTreeToListView(self.tree, self.designInfo.tree, self.designInfo)
        
    def OnAbout(self, event):
        dlg = wxMessageDialog(self, "Blah",
                              "About Me", wxOK | wxICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()


    def TimeToQuit(self, event):
        self.Close(true)

    def OnSelChanged(self, event):
        item = event.GetItem()
        name = getItemHierName(self.tree, item)
        print "*** name:", name
        if not name:
            return

        node = self.designInfo.tree.findNodeByHierName(name)

        self.costView.DeleteAllItems()
        if node.moduleName in self.designInfo.costs:
            mapToListView(self.costView,
                          self.designInfo.costs[node.moduleName])
        else:
            if self.designInfo.isFlattened(node):
                print "FLAT"
                #        self.moduleCostView.clear()
             
            else:
                raise "Error finding cost data for %s" % (node,)

        event.Skip()

def mapToListView(view, data):
    for i in data:
        idx = view.InsertStringItem(0, i)
        idx = view.SetStringItem(idx, 1, str(data[i]))
    view.SetColumnWidth(0, wx.LIST_AUTOSIZE)
    view.SetColumnWidth(1, wx.LIST_AUTOSIZE)

def getItemHierName(view, item):
    hierName = view.GetItemText(item).split()[0]    
    curItem = item
    while curItem != view.GetRootItem():
        curItem = view.GetItemParent(curItem)
        hierName = "%s.%s" % (view.GetItemText(curItem).split()[0],
                              hierName)
    #return hierName
    return hierName[len("DESIGN."):]


class MyApp(wxApp):
    def OnInit(self):
        self.frame = MyFrame(NULL, -1, "Carbon Cost Viewer")
        self.frame.Show(true)
        self.SetTopWindow(self.frame)
        return true

app = MyApp(0)
app.MainLoop()


