import dtree

h = open("libdesign.hierarchy")
hier = dtree.readHier(h)

c = open("libdesign.costs")
dtree.addCostsToTree(c, hier)

#hier.depthWalk()

instanceList = hier.findAllInstances('ln_sdr2ddr')

print "----"


for i in instanceList:
    print i, ' flops:', i.attributes['Flops']
    
