import dtree


def FilterFn(node):
    if node.flattened == True:
        return True
    if not node.costs:
        # This module has no cost data
        return True
    if node.costs['Flops'] == 0:
        return True
    else:
        return False
    

def AttrFn(curNode, childNode = None):
    if childNode:
        return ""
        #        if curNode.moduleName == "osc" and childNode.moduleName == "carbon_osc":            
        #            return "[weight = 1]"
        #        else:
        #            return ""
    else:
        if curNode.moduleName == "m9xb_wrapper":
            return "   \"%s\"" % curNode.getNames(), "[ color = blue, shape = square ]"

        return "[]" (curNode.costs['Flops'],)

        
    return ""


class MaxAttr:
    def __init__(self, attr):
        self.maxVal = 0
        self.maxNode = []
        self.attr = attr

    def __call__(self, n):
        if len(n.costs) == 0:
            # print n, 'has no cost data'
            val = 0
        else:
            val = n.costs[self.attr]

        if  val > self.maxVal:
            self.maxVal = val
            self.maxNode = [n]
        elif val == self.maxVal:
            self.maxVal = val
            self.maxNode.append(n)


# def moduleFlattened(designInfo, parent, child):
#     """Takes a parent and child node and returns true if the node was flattened"""
#     key = (parent.moduleName, child.moduleName, child.instanceName)
# 
#     if not designInfo.flattenData.has_key(key):
#         print "not found", key
#         return False
#     
#     if designInfo.flattenData[key] == True:
#         print "found true"
#         return True
#     else:
#         print "found false", key
#         return False
    
class UnelabGraphMaker:
    def __init__(self, info):
        self.edges = {}
        self.nodes = []
        self.info = info
        self.maxSize = 0
    def __call__(self, n):

        if n.moduleName in self.info.costs and self.info.costs[n.moduleName]['Flops'] > self.maxSize:            
            self.maxSize = self.info.costs[n.moduleName]['Flops']
            
        if n.moduleName not in self.nodes and not self.info.isFlattened(n):
            self.nodes.append(n.moduleName)

        for c in n.children:
            # print c,  c.parent
            if not self.info.isFlattened(c):
                if c.moduleName not in self.nodes:
                    self.nodes.append(c.moduleName)
                
                    edge = (n.moduleName, c.moduleName)
                    if not self.edges.has_key(edge):
                        self.edges[edge] = 1
                    else:
                        self.edges[edge] = self.edges[edge] + 1
        

di = dtree.DesignInfo()

print 'Reading Hierarchy...'
h = open("libdesign.hierarchy")
di.readHierarchy(h)

print 'Reading parameters...'
p = open('libdesign.parameters')
di.paramMap = dtree.readParameterMap(p)
# print di.paramMap

print 'Reading Cost Data...'
c = open('libdesign.costs')
di.readCosts(c)
hier = di.tree
#dtree.addCostsToTree(c, hier)

print 'Looking for max flops'
maxObj = hier.walkTree(MaxAttr('Flops'))
maxFlops = maxObj.maxVal

# print "Max number of flops:", maxObj.maxVal
# print "in modules:", maxObj.maxNode


print "getting flattening info"
flattenReport = open('libdesign.flattening')
dtree.addFlattenDataToTree(flattenReport, di)
# print di.flattenData
# dotFile = open('test1.dot', 'w')
# dtree.TreeToDotFile(dotFile, hier, 2, AttrFn, FilterFn)

ug = UnelabGraphMaker(di)
hier.walkTree(ug)

# print ug.nodes
df = open ('test1.dot', 'w')

df.write("digraph DESIGN {\n")

for node in ug.nodes:
    size = di.costs[node]['Flops']
    if size > 0:
        size = 4 / (ug.maxSize / size)
    df.write('%s [label = "%s",fixedsize=true,width=%d, height=%d] \n' % (node, node, size, size))

for e in ug.edges:

    (source, sink) = e
    df.write(" %s -> %s [weight = %d]\n" % (source, sink, ug.edges[e]))

df.write("\n}\n")
