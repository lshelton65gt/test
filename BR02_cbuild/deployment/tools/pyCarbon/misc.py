""" Garbage pot for stuff """

def printDotNode(dotFile, parentNode, level, maxLevel, attrFn = None,
                 filterFn = None):
    if filterFn and filterFn(parentNode):
        return    

    if attrFn:
        dotFile.write(attrFn(parentNode))
        dotFile.write('\n')
        
    for c in parentNode.children:
        attrString = ""
        if attrFn:
            attrString = attrFn(parentNode, c)            
        dotFile.write("  \"%s\" -> \"%s\" %s\n" % (parentNode.getNames(), c.getNames(), attrString))
        
    if maxLevel and level >= maxLevel:
        return
    
    for c in parentNode.children:
        printDotNode(dotFile, c, level + 1, maxLevel, attrFn, filterFn)

    
def TreeToDotFile(dotFile, tree, levels = 0, attrFn = None, filterFn = None):
    dotFile.write("digraph DESIGN {\n")
    printDotNode(dotFile, tree, 0, levels, attrFn, filterFn)
    dotFile.write('}\n')
    

