
    
class Node:
    def __init__(self):
        self.children = []
        self.moduleName = ""
        self.instanceName = ""
        self.parent = None
        
    def __init__(self, valList, parent = None):
        self.children = []
        self.moduleName = valList[0]
        self.instanceName = valList[1]
        self.parent = parent
        self.flattenCost = -1
        
    def __str__(self):
        if self.moduleName:
            return "%s (%s)" % (self.instanceName, self.moduleName)
        else:
            return self.instanceName        

    def __repr__(self):
        return self.instanceName

    def printTree(self, indent = 0):
        name = self.instanceName
        if not name:
            name = self.moduleName
        print indent * ' ', name
        for c in self.children:
            c.printTree(indent + 2)

    def getNames(self):
        if self.instanceName:
            names = "%s (%s)" % (self.instanceName, self.moduleName)
        else:
            names = self.moduleName

        return names

        
    def findAllInstances(self, unelabName):
        instances = []
        for c in self.children:
            instances = instances + c.findAllInstances(unelabName)
        if self.moduleName == unelabName:
            instances.append(self)
        return instances

    def walkTree(self, fn):
        """Invokes a callable class passed as argument on every node
        of the design tree.  Walk is depth first, invoking the class on
        the way down the tree"""
        
        fn(self)
        for c in self.children:
            c.walkTree(fn)

        return fn

    
    def findNodeByHierName(self, hierName, seperator = '.'):
        """Accepts an elaborated name for a module and returns that
        module's node in the design tree.  Optional seperator parameter
        specificies the hierarchy seperator charactor."""

        print hierName
        
        names = hierName.split(seperator, 1)
        # print names, self.instanceName
        if self.instanceName != names[0]:
            return None

        if len(names) == 1:
            # this is the end!
            return self

        # wasn't the end, pass along to children.  If the name matches
        # one of our children, they'll return the correct node which this
        # routine will then return
        for c in self.children:
            match = c.findNodeByHierName(names[1], seperator)
            if match:
                return match

        # if we fell through to hear, then the sub names didn't match...
        return None
         
