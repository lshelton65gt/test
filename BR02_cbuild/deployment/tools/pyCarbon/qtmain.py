import sys
from qt import *
from window import Window
#from optik import OptionParser


def main(args):
    args = sys.argv
    app=QApplication(args)
    win=Window()
    app.setMainWidget(win)
    win.show()
    app.connect(app, SIGNAL("lastWindowClosed()")
                , app
                , SLOT("quit()"))
    app.exec_loop()

if __name__=="__main__":
        main(sys.argv)



#        parser=OptionParser()
#        (options,args)=parser.parse_args(sys.argv)
