// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003, 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

// Smart wrapper around __gnu_cxx::hash_map that improves iteration access time
// NOTE : not all funcions of __gnu_cxx::hash_map are implemented yet
// Basic idea:
// Store the (key, value) pairs in a vector, instead of a hash.  The
// hash will store (key, index) pairs, where index is the index into the
// vector where the real data is.
// find/insert operate on the hash, while begin() and end() operate on the vector

#include <ext/hash_map>
#include <vector>

template <class _Key, class _Value>
class CarbonHashMap
{
public:
    CarbonHashMap() { }
    ~CarbonHashMap() { }

    typedef std::pair<_Key, _Value> value_type;
    typedef typename std::vector<value_type>::iterator iterator;

    iterator begin()
    {
	return mVector.begin();
    }

    iterator end()
    {
	return mVector.end();
    }

    iterator find(const _Key k)
    {
	hash_iter_type iter = mHash.find(k);
	if (iter == mHash.end())
	    return mVector.end();
	return (mVector.begin() + iter->second);	// STL vector does this... hope it's safe!
    }

    std::pair<iterator, bool> insert(value_type val)
    {
	index_type index = mVector.size();
	hash_pair_type h_p(val.first, index);
	mHash.insert(h_p);
	
	mVector.push_back(val);

	std::pair<iterator, bool> ret_iter(mVector.begin() + index, true);
	return ret_iter;
    }

    void clear()
    {
	mHash.clear();
	mVector.clear();
    }

private:
    typedef typename std::vector<value_type>::size_type index_type;
    typedef typename __gnu_cxx::hash_map<_Key, index_type>::iterator hash_iter_type;
    typedef std::pair<_Key, index_type> hash_pair_type;

    __gnu_cxx::hash_map<_Key, index_type> mHash;
    std::vector<value_type> mVector;
};
