
#include "vpi_user.h"
#include "carbon/carbon_capi.h"
#include "CarbonVpiUtil.h"
#include <cassert>
#include <cstring>


/****************************** OBJECT TYPES ******************************/
const char* vpiTypeString[] = { "",
"vpiAlways", //             1   /* always block */
"vpiAssignStmt", //         2   /* quasi-continuous assignment */
"vpiAssignment", //         3   /* procedural assignment */
"vpiBegin", //              4   /* block statement */
"vpiCase", //               5   /* case statement */
"vpiCaseItem", //           6   /* case statement item */
"vpiConstant", //           7   /* numerical constant or literal string */
"vpiContAssign", //         8   /* continuous assignment */
"vpiDeassign", //           9   /* deassignment statement */
"vpiDefParam", //          10   /* defparam */
"vpiDelayControl", //      11   /* delay statement (e.g. #10) */
"vpiDisable", //           12   /* named block disable statement */
"vpiEventControl", //      13   /* wait on event, e.g. @e */
"vpiEventStmt", //         14   /* event trigger, e.g. ->e */
"vpiFor", //               15   /* for statement */
"vpiForce", //             16   /* force statement */
"vpiForever", //           17   /* forever statement */
"vpiFork", //              18   /* fork-join block */
"vpiFuncCall", //          19   /* HDL function call */
"vpiFunction", //          20   /* HDL function */
"vpiGate", //              21   /* primitive gate */
"vpiIf", //                22   /* if statement */
"vpiIfElse", //            23   /* if-else statement */
"vpiInitial", //           24   /* initial block */
"vpiIntegerVar", //        25   /* integer variable */
"vpiInterModPath", //      26   /* intermodule wire delay */
"vpiIterator", //          27   /* iterator */
"vpiIODecl", //            28   /* input/output declaration */
"vpiMemory", //            29   /* behavioral memory */
"vpiMemoryWord", //        30   /* single word of memory */
"vpiModPath", //           31   /* module path for path delays */
"vpiModule", //            32   /* module instance */
"vpiNamedBegin", //        33   /* named block statement */
"vpiNamedEvent", //        34   /* event variable */
"vpiNamedFork", //         35   /* named fork-join block */
"vpiNet", //               36   /* scalar or vector net */
"vpiNetBit", //            37   /* bit of vector net */
"vpiNullStmt", //          38   /* a semicolon. Ie. #10 ; */
"vpiOperation", //         39   /* behavioral operation */
"vpiParamAssign", //       40   /* module parameter assignment */
"vpiParameter", //         41   /* module parameter */
"vpiPartSelect", //        42   /* part select */
"vpiPathTerm", //          43   /* terminal of module path */
"vpiPort", //              44   /* module port */
"vpiPortBit", //           45   /* bit of vector module port */
"vpiPrimTerm", //          46   /* primitive terminal */
"vpiRealVar", //           47   /* real variable */
"vpiReg", //               48   /* scalar or vector reg */
"vpiRegBit", //            49   /* bit of vector reg */
"vpiRelease", //           50   /* release statement */
"vpiRepeat", //            51   /* repeat statement */
"vpiRepeatControl", //     52   /* repeat control in an assign stmt */
"vpiSchedEvent", //        53   /* vpi_put_value() event */
"vpiSpecParam", //         54   /* specparam */
"vpiSwitch", //            55   /* transistor switch */
"vpiSysFuncCall", //       56   /* system function call */
"vpiSysTaskCall", //       57   /* system task call */
"vpiTableEntry", //        58   /* UDP state table entry */
"vpiTask", //              59   /* HDL task */
"vpiTaskCall", //          60   /* HDL task call */
"vpiTchk", //              61   /* timing check */
"vpiTchkTerm", //          62   /* terminal of timing check */
"vpiTimeVar", //           63   /* time variable */
"vpiTimeQueue", //         64   /* simulation event queue */
"vpiUdp", //               65   /* user-defined primitive */
"vpiUdpDefn", //           66   /* UDP definition */
"vpiUserSystf", //         67   /* user defined system task or function */
"vpiVarSelect", //         68   /* variable array selection */
"vpiWait", //              69   /* wait statement */
"vpiWhile", //             70   /* while statement */
};

void carbonValueToVpiVecVal(t_vpi_vecval* vpiVal, const UInt32* vhmVal,
                            const UInt32* vhmDrive, int bits)
{
   int words = (bits / 32) + (bits % 32 ? 1 : 0);   

#if DEBUG
   {
      // I can't imagine a situation where this isn't true, but I'm paranoid
      // and want to make sure everything is nicely typedef'd
      assert(sizeof(PLI_INT32) == sizeof(UInt32));
   }
#endif
 
   if (vhmDrive) {
      for (int i = 0; i < words; ++i) {
         // need to clear data bits for 'z'
         vpiVal[i].aval = vhmVal[i] & ~vhmDrive[i];
         vpiVal[i].bval = vhmDrive[i];
      }
   } else {
      for (int i = 0; i < words; ++i) {
         vpiVal[i].aval = vhmVal[i];
      }
   }

}



void carbonValueToVpiIntVal(t_vpi_value* vpiVal, const UInt32* vhmVal,
                            const UInt32* vhmDrive, int bits)
{
#if DEBUG
   {
      assert(sizeof(PLI_INT32) == sizeof(UInt32));
   }
#endif

   if(bits > sizeof(PLI_INT32)) {
      setVpiError(vpiError, "Can't convert vectors larger than 32bits to integer value", "VPI_FOO"); 
   }
   vpiVal->value.integer = *vhmVal;

}

void carbonValueToVpiScalarVal(t_vpi_value* vpiVal, const UInt32* vhmVal,
                               const UInt32* vhmDrive, int bits)
{
   // the Z stuff needs to be beefed up...
   if(bits != 1) {
      setVpiError(vpiError, "Can'tuse vpiScalarVal for nets wider than 1 bit", "VPI_FOO"); 
      return;
   }

   if (!vhmDrive) {
      vpiVal->value.scalar = vpiZ;
   } else if (vhmVal) {
      vpiVal->value.scalar = vpi1;
   } else {
      vpiVal->value.scalar = vpi0;
   }

}

void carbonValueToVpiValue(t_vpi_value* vpiVal, const UInt32* vhmVal,
                      const UInt32* vhmDrive, int bits)
{
   switch(vpiVal->format) {
   case vpiVectorVal:
      carbonValueToVpiVecVal(vpiVal->value.vector, vhmVal, vhmDrive, bits);
      break;
   case vpiIntVal:
      carbonValueToVpiIntVal(vpiVal, vhmVal, vhmDrive, bits);
      break;
   case vpiScalarVal:
      carbonValueToVpiScalarVal(vpiVal, vhmVal, vhmDrive, bits);
      break;
   default:
      UNSUPPORTED("Unsupported VPI Value Format");
      return;
   };
}



void vpiTimeToCarbonTime(CarbonTime *vhmTime, const t_vpi_time* vpiTime)
{
   switch(vpiTime->type) {
      case vpiSuppressTime:
         break;
      case vpiScaledRealTime:
         *vhmTime = CarbonTime(vpiTime->real);
         break;
      case  vpiSimTime:
         *vhmTime = ((CarbonTime(vpiTime->high) << 32) | vpiTime->low);
         break;
      default:
         UNSUPPORTED("OOOPS");
   };
}

void copyCallbackData(s_cb_data*dest, const s_cb_data *src)
{

   
   memcpy(dest, src, sizeof(s_cb_data));

   dest->time = new s_vpi_time;
   memcpy(dest->time, src->time, sizeof(s_vpi_time));

   dest->value = new s_vpi_value;
   dest->value->format = src->value->format;

int width;
   switch(dest->value->format)
   {
   case vpiSuppressVal:
	   break;
   case vpiVectorVal:
	   assert(dest->obj);
	   width = vpi_get(vpiSize, dest->obj);
	   dest->value->value.vector = new t_vpi_vecval[width];
	   break;
   case vpiIntVal:
      break;
   default:
	   setVpiError(vpiError,"Bad value format", "PLI_FOO");
   }

}

CarbonTime vpiTimeToCarbonTime(const t_vpi_time* vpiTime)
{
   switch(vpiTime->type) {
      case vpiSuppressTime:
         return 0;
         break;
      case vpiScaledRealTime:
         return CarbonTime(vpiTime->real);
         break;
      case  vpiSimTime:
         return((CarbonTime(vpiTime->high) << 32) | vpiTime->low);
         break;
      default:
         UNSUPPORTED("OOOPS");
   };
}

void carbonTimeToVpiTime(t_vpi_time* vpiTime, 
                         const CarbonTime vhmTime)
{
   if(vpiTime->type ==  vpiSuppressTime) {
      // time doesn't matter
      return;
   }

   vpiTime->high = UInt32((vhmTime >> 32) & 0xFFFFFFFF);
   vpiTime->low  = UInt32((vhmTime) & 0xFFFFFFFF);

   // Doesn't actually scale...  This is the 
   // same as VMoD support I think.
   vpiTime->real = double(vhmTime);
}
