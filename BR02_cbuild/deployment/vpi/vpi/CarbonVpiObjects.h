// -*- C++ -*-
/*
**
** Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.
**
** THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
** DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
** THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
** APPEARS IN ALL COPIES OF THIS SOFTWARE.
**
*/

#ifndef __CARBONVPIOBJECTS_H__
#define __CARBONVPIOBJECTS_H__

#include "carbon/carbon_capi.h"
#include "carbon/carbon_dbapi.h"
#include "vpi_user.h"
#include "CarbonVpiUtil.h"

#include <functional>
#include <cassert>


extern CarbonDB* gDB;
class CarbonVpiHandle 
{
public:
	/* Object specific implementation of vpi call */
	virtual int vpi_get(PLI_INT32 property);
	virtual void vpi_get_str(PLI_INT32 property, char* buf, int bufSize);
	virtual void vpi_get_value(s_vpi_value* val) const { UNSUPPORTED("object does not support vpi_get_value()");  }
	virtual vpiHandle vpi_iterate(PLI_INT32 type){ UNSUPPORTED ("vpi_iterate error"); return 0;}
	virtual vpiHandle vpi_scan(){ UNSUPPORTED ("Only vpiInterator type supports vpi_scan"); return 0;}
	virtual vpiHandle vpi_handle(PLI_INT32) { UNSUPPORTED (""); return 0; }
	virtual vpiHandle vpi_handle_by_index(PLI_INT32) { UNSUPPORTED (""); return 0; }

private:
protected:
	CarbonVpiHandle() {}
	CarbonVpiHandle(PLI_INT32 _type): mType(_type) {}
	
	/*
	** Each of these is a property that an object might have.  By default
	** all objects return some sort of bad status, error code, or invalid 
	** result.  Objects that have a given property need to implement the 
	** appropriate routine
	*/
	virtual PLI_INT32 getType() const { return mType; }
	virtual PLI_INT32 getSize() const { return vpiUndefined; }
	virtual void getName(char* buf, int bufSize) const { buf[0] = '\0'; }
	virtual void getFullName(char* buf, int bufSize) const { buf[0] = '\0'; }
	virtual bool getIsScalar() const {return false;}
	virtual bool getIsVector() const {return false; }	
	virtual PLI_INT32 getDirection() const {return vpiNoDirection; }

	PLI_INT32 mType;
};

class CarbonVpiConstant : public CarbonVpiHandle
{
public:
   CarbonVpiConstant (int _val): CarbonVpiHandle(vpiConstant), mVal(_val) {}
public:
   //virtual PLI_INT32 getType() const {return vpiConstant;}
   // need think about it the call below is safe?  What is a constant's type and format?
   virtual void vpi_get_value(s_vpi_value* val) const { carbonValueToVpiValue(val, &mVal, 0, 32); }
private:
   UInt32 mVal;
private:
   CarbonVpiConstant();
};

class CarbonVpiModule : public CarbonVpiHandle
{
public:
   CarbonVpiModule(CarbonObjectID* _vhm, CarbonDB* _db, const CarbonDBNode* _node): mNode(_node), mDB (_db), mVhm(_vhm) {}
   void getName(char* buf, int bufSize) const;
   void getFullName(char* buf, int bufSize) const;
   virtual vpiHandle vpi_iterate(PLI_INT32 type);
private:
   const CarbonDBNode* mNode;
   CarbonDB* mDB;
   CarbonObjectID* mVhm;
private:
   virtual PLI_INT32 getType() const {
      return vpiModule;
   }
private:
    CarbonVpiModule();

};

class CarbonVpiMemWordIterator : public CarbonVpiHandle
{
public:
//    CarbonVpiMemWordIterator(CarbonObjectID* _obj, CarbonMemoryID* _mem) {}
    CarbonVpiMemWordIterator(CarbonObjectID* _obj, CarbonMemoryID* _mem): CarbonVpiHandle(vpiIterator),
		mVhm(_obj), mMem(_mem) 
	{
	    mHiAddr = carbonGetHighAddr(mMem);
	    mNextAddr = mLoAddr = carbonGetLowAddr(mMem);
	    mAddrRising = mHiAddr > mLoAddr;
	}

private:

    CarbonVpiMemWordIterator(const CarbonVpiMemWordIterator&);
    CarbonVpiMemWordIterator();

public:
vpiHandle vpi_scan();

private:
    CarbonObjectID* mVhm;
    CarbonMemoryID* mMem;
    SInt32 mLoAddr;
    SInt32 mHiAddr;
    SInt32 mNextAddr;
    bool mAddrRising;
};



class CarbonVpiIterator : public CarbonVpiHandle
{
public:
	CarbonVpiIterator(CarbonObjectID* _obj, CarbonDB* _db, CarbonDBNodeIter* _it,
		PLI_INT32 _type): mVhm(_obj), mIter(_it), mDB(_db), mType(_type) {}
	//private:
	CarbonVpiIterator(const CarbonVpiIterator&);
	CarbonVpiIterator();
public:
 vpiHandle vpi_scan();

private:
   CarbonDBNodeIter* mIter;
   CarbonDB* mDB;
   CarbonObjectID* mVhm;
   PLI_INT32 mType;
private:
   bool isPort(const CarbonDBNode*);


};


/*
** scratch pads
*/
extern s_vpi_value valueBuf;
extern s_vpi_vecval vecValBuf;


class CarbonVpiNet : public CarbonVpiHandle {
public:
   CarbonVpiNet(CarbonObjectID*, CarbonNetID*);
   CarbonNetID* carbonNet() { return mNet; }
public:
   /*
   ** This stuff maps 1-to-1 to vpi calls
   */
   virtual void vpi_get_value(s_vpi_value* val) const ;
   virtual vpiHandle vpi_iterate(PLI_INT32 type);
   virtual vpiHandle vpi_handle(PLI_INT32);

protected:
   /*
   ** routines that support vpi calls
   */
    virtual PLI_INT32 getSize() const {return mWidth; }
   virtual void getName(char*, int) const;
   virtual void getFullName(char*, int) const;
   virtual bool getIsScalar() const {return carbonIsScalar(mNet); }
   virtual bool getIsVector() const {return carbonIsVector(mNet); }

protected:
   CarbonNetID* mNet;
   CarbonObjectID* mObj;
   UInt32 *mDriveBuf;
   UInt32 *mValBuf;
   int mWidth;

private:
   CarbonVpiNet(CarbonNetID* _net): mNet(_net) {}
   CarbonVpiNet();
   CarbonVpiNet(const CarbonVpiNet&);
};

class CarbonVpiPort : public CarbonVpiNet
{
public:
	CarbonVpiPort(CarbonObjectID*, CarbonDB*, const CarbonDBNode*);
protected:

	virtual PLI_INT32 getDirection() const;

private:
   const CarbonDBNode* mNode;
   CarbonDB* mDB;

private:
   CarbonVpiPort();
   CarbonVpiPort(const CarbonVpiPort &);
};

class CarbonVpiMemoryWord : public CarbonVpiHandle
{
public:
   CarbonVpiMemoryWord(CarbonObjectID* _vhm, CarbonMemoryID* _mem, SInt64 _index);
public:
   void vpi_get_value(s_vpi_value*);
private:
   CarbonObjectID* mVhm;
   CarbonMemoryID* mMem;
   SInt64 address;
   int mWidth, mWords;
   UInt32 *mDataBuf;
private:
   CarbonVpiMemoryWord();
   CarbonVpiMemoryWord(const CarbonVpiMemoryWord&);
};

class CarbonVpiMem : public CarbonVpiHandle {
public:
    CarbonVpiMem(CarbonObjectID* _vhm, CarbonMemoryID* _mem): CarbonVpiHandle(vpiMemory), mVhm (_vhm), mMem(_mem) {}
    CarbonMemoryID* carbonMem() { return mMem; }
public:
    // virtual int vpi_get(PLI_INT32 property);
    // virtual void vpi_get_str(PLI_INT32 property, char* buf, int bufSize);
    // virtual void vpi_get_value(s_vpi_value* val) const { UNSUPPORTED("object does not support vpi_get_value()");  }
    virtual vpiHandle vpi_iterate(PLI_INT32 type);
    // virtual vpiHandle vpi_scan(){ UNSUPPORTED ("Only vpiInterator type supports vpi_scan"); return 0;}
    virtual vpiHandle vpi_handle(PLI_INT32);
    virtual vpiHandle vpi_handle_by_index(PLI_INT32);
private:
    CarbonMemoryID* mMem;
    CarbonObjectID* mVhm;
private:
    bool isValidMemAddress(SInt64);
    CarbonVpiMem();
    CarbonVpiMem(const CarbonVpiMem&);
};


class CarbonVpiCB : public CarbonVpiHandle {
public:
   CarbonVpiCB(t_cb_data* _vpiCbData);
   PLI_INT32 getType() {return vpiCallback;}
protected:
   t_cb_data *mVpiCbData;
};

class CarbonVpiValueCB : public CarbonVpiCB {
public:
   CarbonVpiValueCB(t_cb_data* _vpiCbData);
   CarbonNetValueCBDataID* carbonCB() { return cb; }
private:
   CarbonNetValueCBDataID* cb;
};


#endif
