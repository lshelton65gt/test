// -*- C++ -*-
/*
**
** Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.
**
** THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
** DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
** THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
** APPEARS IN ALL COPIES OF THIS SOFTWARE.
**
*/

#include "vpi_user.h"
#include "carbon/carbon_capi.h"
#include "carbon/carbon_dbapi.h"
#include "CarbonVpiUtil.h"
#include "CarbonVpiObjects.h"
#include "VpiMaster.h"

#include <cstring>
#include <cassert>
#include <cstdarg>
#include <cstdio>
#define VPI_UNSUPPORTED_ASSERT

/*
** Convert from vpi formated vactors to Carbon's format
** aval goes straight into val.  This means if the vec
** has an 'x' the value in carbon will be '1'.
** if a pointer for drive is provided, Z values will be calculated,
** otherwise we will ignore the drive bits in the vecval and just 
** make all things driven
*/
/*
**
** The following need to be supported.  As I add things
** to the implementation, I'll remove them from this list
*
* vpi_get_cb_info()
* vpi_get_systf_info()
* vpi_remove_cb()
*
* The following are all printing / logging:
* vpi_flush()
* vpi_mcd_close()
* vpi_mcd_flush()
* vpi_mcd_name()
* vpi_mcd_open()
* vpi_mcd_printf()
* vpi_mcd_vprintf()
* vpi_printf()
* vpi_vprintf()
*/

// VPI status variables
s_vpi_error_info lastErrInfo;

void setVpiError(PLI_INT32 level, PLI_BYTE8* msg,
                 PLI_BYTE8* code) 
{
   lastErrInfo.message = msg;
   lastErrInfo.code = code;
   lastErrInfo.level = level;
}



vpiHandle vpiHandleFromMem(CarbonMemoryID*mem) 
{
   /*
   ** For the moment I'm just casting stuff.
   ** Need to add real book keeping later...
   */

   return (vpiHandle)mem;
}

//vpiHandle vpiHandleFromNet(CarbonNetID*net) 
//{
//   /*
//   ** For the moment I'm just casting stuff.
//   ** Need to add real book keeping later...
//   */
//   return (vpiHandle)net;
//}

/*
** Utility Functions
*/
void clearVpiErrors()
{
   lastErrInfo.state = vpiRun;           /* vpi[Compile,PLI,Run] */
   lastErrInfo.level = 0;           /* vpi[Notice,Warning,Error,System,Internal] */
   lastErrInfo.message = 0;
   lastErrInfo.product = 0;
   lastErrInfo.code = 0;
   lastErrInfo.file = 0;
   lastErrInfo.line = 0;
}


extern "C" 
{

/*
** VPI Functions
*/
#define PROTO_PARAMS(params) params

PLI_INT32 vpi_chk_error PROTO_PARAMS((p_vpi_error_info error_info_p))
{
   if(lastErrInfo.level && error_info_p) {
      /* I think that the user is allowed to deallocate the error_info_p
      ** but not the pointers inside it...
      */
      memcpy(error_info_p, &lastErrInfo, sizeof(s_vpi_error_info));
   }

   return lastErrInfo.level;
}


/*
** Currently free_object does nothing.  We could make it 
** clean up stuff later, but for now I don't really care...
*/
PLI_INT32 vpi_free_object PROTO_PARAMS((vpiHandle object))
{
   return 1;
}


/* utility routines */
PLI_INT32  vpi_compare_objects PROTO_PARAMS((vpiHandle object1, vpiHandle object2))
{
   /* The VLOG spec suggests that a C compare isn't applicable here, but it's
   ** really implementation specific.  We need to be careful that if we introduce a 
   ** scheme for vpiHandles where diff handles can reference the same object, this function
   ** will need to change 
   */
   return object1 == object2;
}


vpiHandle vpi_handle_by_name PROTO_PARAMS((PLI_BYTE8 *name,
   vpiHandle scope))
{
   clearVpiErrors();

   // Try the db
   const CarbonDBNode* dbNode = carbonDBFindNode(gDB, name);
   if(dbNode){
      
      // vpi_printf("name lookup of %s found a db node\n", name);
   
      if (carbonDBIsBranch(gDB, dbNode)) {
         // vpi_printf("%s is a module\n", name);
         return (vpiHandle)new CarbonVpiModule(gObj, gDB, dbNode);
      }

	  bool isNet = carbonDBIsScalar(gDB, dbNode) ||
		  carbonDBIsVector(gDB, dbNode);
	  if(isNet) {
		  // Try nets
		  CarbonNetID* net = carbonFindNet(gObj, name);
		  // CarbonVpiNet* vpiNetObj = new CarbonVpiNet(gObj, net);

		  //assert(vpiNetObj);
		  if(net) return ((vpiHandle)(new CarbonVpiNet(gObj, net)));
		          return 0;
	  }

	  if(carbonDBIs2DArray(gDB, dbNode)) {
         // Try Memories
         CarbonMemoryID* mem = carbonFindMemory(gObj, name);
         if (mem) return (vpiHandle)(new CarbonVpiMem(gObj, mem));
         return 0;
      }

   }

   // There's nothing else.. create an error    
   setVpiError(vpiWarning, "Unable to find vpi handle by name",
      "VPI_FOO");

   return 0;
}



struct VpiCallbackData 
{
   VpiCallbackData(PLI_BYTE8* _userData, PLI_INT32(*_cb)(struct t_cb_data*)):userData(_userData), cb(_cb) {}
   PLI_INT32    (*cb)(struct t_cb_data *); /* call routine */
   PLI_BYTE8   *userData;
};



vpiHandle vpi_register_cb PROTO_PARAMS((p_cb_data cb_data_p))
{    
   clearVpiErrors();

   if (cb_data_p->reason == cbValueChange) {
      /*
      ** Register the callback
      */
      s_cb_data* cbData = new s_cb_data();

      // I don't think the vpi owns the s_cb_data parameter so need to copy it.
      // If I'm wrong, then this probably introduces a memory leak since we 
      // aren't going to clean up s_cb_data
      //memcpy(cbData, cb_data_p, sizeof(s_cb_data));
      copyCallbackData(cbData, cb_data_p  );
      //CarbonVpiNet* net = dynamic_cast<CarbonVpiNet*>(reinterpret_cast<CarbonVpiHandle*>(cb_data_p->obj));
      // CarbonNetValueCBDataID* retVal = carbonAddNetValueChangeCB(gObj, vpiCB, (CarbonClientData)d, net->carbonNet());

      /*
      ** CODE BELOW NEEDS ERROR CHECKS!
      */
      return reinterpret_cast<vpiHandle>(new CarbonVpiValueCB(cbData));
   } else if (cb_data_p->reason == cbEndOfCompile) {
       s_cb_data* cbData = new s_cb_data();

      // I don't think the vpi owns the s_cb_data parameter so need to copy it.
      // If I'm wrong, then this probably introduces a memory leak since we 
      // aren't going to clean up s_cb_data
//      memcpy(cbData, cb_data_p, sizeof(s_cb_data));
copyCallbackData(cbData, cb_data_p  );
      gMaster->registerVpiEndCompCb(cbData);
   } else if (cb_data_p->reason == cbReadOnlySynch) {
      s_cb_data* cbData = new s_cb_data();

      // I don't think the vpi owns the s_cb_data parameter so need to copy it.
      // If I'm wrong, then this probably introduces a memory leak since we 
      // aren't going to clean up s_cb_data
      memcpy(cbData, cb_data_p, sizeof(s_cb_data));
      gMaster->registerVpiCb(cbData);
   } else {
      // value change is the only currently supported reason
      unsupported("Only cbValueChange is currently supported by vpi_register_cb");
      return 0;
   }
}




PLI_INT32  vpi_remove_cb       PROTO_PARAMS((vpiHandle cb_obj))
{
   UNSUPPORTED("vpi_remove_cb - Unsupported VPI Call");    
   return 0;
}

//-----

void vpi_get_cb_info PROTO_PARAMS((vpiHandle object, p_cb_data cb_data_p))
{
   UNSUPPORTED("Unsupported VPI Call VPI_GET_CB_INFO");
}



vpiHandle  vpi_register_systf  PROTO_PARAMS((p_vpi_systf_data
   systf_data_p))
{
   UNSUPPORTED("Unsupported VPI Call, vpi_register_systf");
   return 0;
}

void       vpi_get_systf_info  PROTO_PARAMS((vpiHandle object,
   p_vpi_systf_data
   systf_data_p))
{
   UNSUPPORTED("Unsupported VPI Call");    
}

/* for obtaining handles */
vpiHandle  vpi_handle_by_index PROTO_PARAMS((vpiHandle object,
                                            PLI_INT32 index))
{
   CarbonVpiHandle* handle = reinterpret_cast<CarbonVpiHandle*>(object);
   return handle->vpi_handle_by_index(index);
}

/* for traversing relationships */
vpiHandle  vpi_handle          PROTO_PARAMS((PLI_INT32 type,
   vpiHandle refHandle))

{
   UNSUPPORTED("Unsupported VPI Call, vpi_handle");
   return 0;
}


vpiHandle  vpi_handle_multi    PROTO_PARAMS((PLI_INT32 type,
   vpiHandle refHandle1,
   vpiHandle refHandle2,
   ... ))
{
   UNSUPPORTED ("Unsupported VPI Call, vpi_handle_multi");
      return 0;
}


vpiHandle  vpi_iterate         PROTO_PARAMS((PLI_INT32 type,
   vpiHandle refHandle))
{
   CarbonVpiHandle* handle = reinterpret_cast<CarbonVpiHandle*>(refHandle);
   return handle->vpi_iterate(type);
}

vpiHandle  vpi_scan            PROTO_PARAMS((vpiHandle iterator))
{
   CarbonVpiHandle* handle = reinterpret_cast<CarbonVpiHandle*>(iterator);
   return handle->vpi_scan();
}

/* 
**    
*/
PLI_INT32  vpi_get PROTO_PARAMS((PLI_INT32 property,
                                vpiHandle object))
{
   clearVpiErrors();

   if (object == 0) {
      setVpiError(vpiError, "NULL object passed to vpi_get", "VPI_FOO");
      return vpiUndefined;
   }

   // pass the call to the object
      

   CarbonVpiHandle* hdl = reinterpret_cast<CarbonVpiHandle*>(object);
   return hdl->vpi_get(property);
}

const int STR_BUF_SIZE = 512;
char vpiGetStrBuf[STR_BUF_SIZE] = "";
PLI_BYTE8 *vpi_get_str PROTO_PARAMS((PLI_INT32 property,
                                    vpiHandle object))
{
   CarbonVpiHandle* hdl = reinterpret_cast<CarbonVpiHandle*>(object);
   hdl->vpi_get_str(property, vpiGetStrBuf, STR_BUF_SIZE);

   return vpiGetStrBuf;
}

/* delay processing */
void       vpi_get_delays      PROTO_PARAMS((vpiHandle object,
                                            p_vpi_delay delay_p))
{
   UNSUPPORTED("Unsupported VPI Call, vpi_get_delays");
}

void       vpi_put_delays      PROTO_PARAMS((vpiHandle object,
                                            p_vpi_delay delay_p))

{
   UNSUPPORTED("call is unsupported\n");
}

/* value processing */
void       vpi_get_value       PROTO_PARAMS((vpiHandle expr,
                                            p_vpi_value value_p))
{
      clearVpiErrors();

   if (expr == 0) {
      setVpiError(vpiError, "NULL object passed to vpi_get", "VPI_FOO");
   }
   // pass the call to the object      
   CarbonVpiHandle* hdl = reinterpret_cast<CarbonVpiHandle*>(expr);
   hdl->vpi_get_value(value_p);

//   UNSUPPORTED("call is unsupported\n");
}


vpiHandle  vpi_put_value       PROTO_PARAMS((vpiHandle object,
                                            p_vpi_value value_p,
                                            p_vpi_time time_p,
                                            PLI_INT32 flags))
{
   clearVpiErrors();

   // ToDo:
   //    - Check value types
   //    - chec times
   //    - check flags for appropriate delays
   if(value_p->format != vpiIntVal) {
      // currently only int is supported.  
      // vector should be simple

   }
   assert(value_p->format == vpiIntVal);
   CarbonVpiNet* net = dynamic_cast<CarbonVpiNet*>((CarbonVpiHandle*)object);
   // should do error check on net
   PLI_INT32* i = &(value_p->value.integer);
   UInt32 *p = (UInt32*)i;
   carbonDeposit(gObj, net->carbonNet(), p, 0);

   // FIXME - What does this actually return???
   return 0;
}

/*
** Does not support scaling and ignores
** the timescale of the object
*/
void vpi_get_time PROTO_PARAMS((vpiHandle object,
                               p_vpi_time time_p))

{
   carbonTimeToVpiTime(time_p, carbonGetSimulationTime(gObj));
}

/* I/O routines */
PLI_UINT32 vpi_mcd_open        PROTO_PARAMS((PLI_BYTE8 *fileName))
{
   UNSUPPORTED("call is unsupported, vpi_mcd_open\n");
   return 0;
}

PLI_UINT32 vpi_mcd_close       PROTO_PARAMS((PLI_UINT32 mcd))
{
   UNSUPPORTED("call is unsupported\n");
   return 0;
}
PLI_BYTE8 *vpi_mcd_name        PROTO_PARAMS((PLI_UINT32 cd))
{
   UNSUPPORTED("call is unsupported\n");
   return 0;
}
PLI_INT32  vpi_mcd_printf      PROTO_PARAMS((PLI_UINT32 mcd,
   PLI_BYTE8 *format,
   ...))
{
   unsupported("call is unsupported\n");
   return 0;
}

/*
** vpi_printf is going to go to stdout for the moment
*/
PLI_INT32 vpi_printf PROTO_PARAMS((PLI_BYTE8 *format, ...))
{
   int retval = 0;
   va_list ap;

   va_start(ap, format);       /* Initialize the va_list */
   retval = vprintf(format, ap);  /* Call vprintf */
   va_end(ap); /* Cleanup the va_list */

   return retval;
}



PLI_INT32  vpi_get_vlog_info   PROTO_PARAMS((p_vpi_vlog_info
   vlog_info_p))
{
   UNSUPPORTED("call is unsupported\n");
   return 0;
}


/* routines added with 1364-2000 */
PLI_INT32  vpi_get_data        PROTO_PARAMS((PLI_INT32 id,
   PLI_BYTE8 *dataLoc,
   PLI_INT32 numOfBytes))
{
   UNSUPPORTED("call is unsupported\n");
   return 0;
}
PLI_INT32  vpi_put_data        PROTO_PARAMS((PLI_INT32 id,
   PLI_BYTE8 *dataLoc,
   PLI_INT32 numOfBytes))
{
   UNSUPPORTED("call is unsupported\n");
   return 0;
}
void      *vpi_get_userdata    PROTO_PARAMS((vpiHandle obj))
{
   unsupported("call is unsupported\n");
   return 0;
}
PLI_INT32  vpi_put_userdata    PROTO_PARAMS((vpiHandle obj,
   void *userdata))
{
   UNSUPPORTED("call is unsupported\n");
   return 0;
}

/*
** For the moment this goes to STDOUT
*/
PLI_INT32  vpi_vprintf PROTO_PARAMS((PLI_BYTE8 *format,
   va_list ap))
{
   return vprintf (format, ap);
}



PLI_INT32  vpi_mcd_vprintf     PROTO_PARAMS((PLI_UINT32 mcd,
   PLI_BYTE8 *format,
   va_list ap))
{
   unsupported("call is unsupported\n");
   return 0;
}
PLI_INT32  vpi_flush           PROTO_PARAMS((void))
{
   unsupported("call is unsupported\n");
   return 0;
}
PLI_INT32  
   vpi_mcd_flush       PROTO_PARAMS((PLI_UINT32 mcd))
{
   unsupported("call is unsupported\n");
   return 0;
}


PLI_INT32 vpi_control PROTO_PARAMS((PLI_INT32 operation,
   ...))
{
   clearVpiErrors();
   switch (operation) {
   case vpiStop:
      gMaster->stop();
      return 1;
   case vpiFinish:
      gMaster->finish();
      return 1;
   default:
      setVpiError(vpiError, "make me log an error", "");
      return 0;
   }
}

vpiHandle  
   vpi_handle_by_multi_index PROTO_PARAMS((vpiHandle obj,
   PLI_INT32 num_index,
   PLI_INT32 *index_array))
{
   unsupported("call is unsupported\n");
   return 0;
}



}





//---------------------------------
