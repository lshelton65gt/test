// -*- C++ -*-
/*
**
** Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.
**
** THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
** DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
** THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
** APPEARS IN ALL COPIES OF THIS SOFTWARE.
**
*/

#ifdef _DEBUG
#define ASSERT(x) assert((x))
#else
#define ASSERT(X)
#endif

#include "CarbonVpiObjects.h"
#include "CarbonVpiUtil.h"
#include "VpiMaster.h"

extern CarbonObjectID* gObj;

eCarbonMsgCBStatus msgHandler(CarbonClientData clientData, 
							  CarbonMsgSeverity severity, 
							  int number, const char *text, unsigned int len)
{
	vpi_printf("CARBON_MESSAGE: %d %s\n", number, text);
	return eCarbonMsgStop;
}


/*
** VpiSimMaster Constructor
**
** Will instantiate the VHM and need book keeping
*/
VpiSimMaster::VpiSimMaster(createFn fn)
{
   mStopped = mFinished = false;

   carbonSetFilePath("winobj");
   CarbonMsgCBDataID* msgCb1 = carbonAddMsgCB(0, msgHandler, 0);  

	mVhm = fn(eCarbonFullDB, eCarbon_NoFlags);
   gObj = mVhm;

   CarbonMsgCBDataID* msgCb2 = carbonAddMsgCB(mVhm, msgHandler, 0);  

// This definitely needs to be adjusted...
   gDB = carbonDBCreate("winobj/libdesign.symtab.db", false);

   // CarbonStatus sts = carbonChangeMsgSeverity(mVhm, 5055, eCarbonMsgSuppress);
   // ASSERT(sts == eCarbon_OK);

 // attach a messaging callback.
   ASSERT(mVhm);
   ASSERT(gObj);   

   /* For the moment dump entire DUT.  But this needs
   ** to be cleaned up appropriately
   */
   CarbonWaveID* vcd = carbonWaveInitVCD(gObj, "out.vcd", e1ns);
   ASSERT(vcd);

   carbonDumpAll(vcd);
   carbonDumpVars(vcd, 0, "test");

   gMaster = this;
}

void VpiSimMaster::createClock(const char* ckNet, CarbonTime p)
{
   assert(mVhm);
   VpiMasterClock* ck = new VpiMasterClock(mVhm, ckNet, p);
   mEventQ.push(VpiMasterEvent(ck->nextEventTime(), 
      CLOCK_SLOT, new VpiSignalAction(ck) ));
}
/*
** VpiSimMaster Destructor.
** Just cleans up.  Has to destroy the object to make
** sure that waves get flushed.
*/
VpiSimMaster::~VpiSimMaster()
{
   carbonDestroy(&mVhm);
   gObj = 0;
}



void VpiSimMaster::registerVpiCb(s_cb_data* cbData)
{
   VpiCallbackAction* cbAct = new VpiCallbackAction(cbData);

   /*  switch(cbData->reason) {
   case cbReadOnlySynch: */ 
   if (cbData->reason ==cbReadOnlySynch) {
      VpiMasterEvent evCB = VpiMasterEvent(vpiTimeToCarbonTime(cbData->time), READ_ONLY_SYNCH_SLOT, cbAct);
      mEventQ.push(evCB);
   }
   /*     break;
   default:
   UNSUPPORTED("Unsupported Callback reason");
   }*/
}


void VpiSimMaster::registerVpiEndCompCb(s_cb_data* cbData)
{
   mEndOfCompCbList.push_back(cbData);
}


void invokeVpiCb(s_cb_data* cbData, CarbonTime t)
{
   carbonTimeToVpiTime(cbData->time, t);
   (cbData->cb_rtn)(cbData);
}

/*
** This code is what serves as a sim kernel
** it's obviously very simple.  It generates clocks,
** calls vpi callbacks, and runs the device
*/
void VpiSimMaster::run()
{
   // maybe some command line book keeping before starting?

   // I'm using NoInit so that cbEndOfCompile callbacks can be called
   // before the init blocks are run
   //gObj = carbon_design_create(eCarbon_NoInit, eCarbon_NoFlags);

   // Call end of compile CBs
   for(std::vector<s_cb_data*>::iterator i = mEndOfCompCbList.begin();
      i != mEndOfCompCbList.end(); ++i)
   {
      invokeVpiCb(*i, 0);  
   }

   //carbonInitialize(gObj, 0, 0, 0);
   CarbonTime simTime = 0;
   bool simulate = true;


   while(simulate) {
      VpiMasterEvent event = mEventQ.top();
      simTime = event.time;

      bool beforeSchedule = true;
      bool runSchedule = false;
      while(event.time == simTime) {
         if (beforeSchedule && runSchedule && event.slot > CARBON_SCHEDULE_SLOT) {
            carbonSchedule(mVhm, simTime);
            beforeSchedule = false;
         }

         event.action->execute(simTime);

         if (event.action->needsSchedule()) {
            // the event is indicating that Carbon Schedule should be 
            // run in this time stamp
            runSchedule = true;
         }

         if(event.action->needsRequeue()) {
            //VpiMasterEvent tmpEvent = event.nextEvent();
            event.time = event.action->nextEventTime();
            // if the new event time is the current simTime,
            // we'll get stuck in a loop....
            ASSERT(event.time() != simTime);
            mEventQ.push(event);
         }

         mEventQ.pop();
         event = mEventQ.top();
      }

      carbonSchedule(gObj, simTime);
      //if (simTime > 5000) simulate = false; 
      // call cbReadOnly CBs
      if (mStopped || mFinished) simulate = false;
   } // end sim loop
   // Call end of sim CBs

   // clean up

   // exit
}

/*
** VPI SIGNAL ACTION
*/
void VpiSignalAction::execute(CarbonTime t)

{
   mSignal->update(t);
   mSignal->applyValue();
}

bool VpiSignalAction::needsRequeue() const {return true;}
bool VpiSignalAction::needsSchedule() const {return true;}
CarbonTime VpiSignalAction::nextEventTime() const {
   return mSignal->nextEventTime();
}

/*
** Callback action
*/

void VpiCallbackAction::execute(CarbonTime t)
{
   invokeVpiCb(mCbData, t);
   //  (mCbData->rtn)(mCbData);
}


