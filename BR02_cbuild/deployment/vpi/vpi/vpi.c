#include "vpi_user.h"
#include <assert.h>
#include <stdlib.h>

/*
** This file tests:
**  - looking up a net by name
**  - registering a callback
**     o cbValueChange
**     o cbIntVal
**  - vpi_printf()
**  - vpi_chk_error() for successes, no negative tests
*/

PLI_INT32 ckCB(s_cb_data* data)
{
  vpiHandle *input;
  s_vpi_value inVal;
  s_vpi_time inTime;


  vpi_printf("called for ckCB %d\n", data->value->value.integer);
 
  if(data->value->value.integer) { // rising edge
    inVal.value.integer = rand();
    inVal.format = vpiIntVal;

    input = (vpiHandle*) data->user_data;

    vpi_put_value(*input, &inVal, &inTime, vpiNoDelay);
  }

  return 0;

}

void registerCkCB()
{
  s_cb_data cbData;
  vpiHandle hdlCk;
  s_vpi_error_info err;

  vpiHandle* input;

  // look up the clock
  hdlCk = vpi_handle_by_name("test.ck", 0);
  assert(vpi_chk_error(0) == 0);

  // look up input pin (test.i)
  input = (void*)malloc(sizeof(vpiHandle));
  *input = vpi_handle_by_name("test.i", 0);

  // attach a cb
  cbData.reason = cbValueChange;
  cbData.cb_rtn = ckCB;
  cbData.obj = hdlCk;
  cbData.time = (void*)malloc(sizeof(s_vpi_time));
  cbData.time->type = vpiSimTime;
  cbData.value = (void*)malloc(sizeof(s_vpi_value));
  cbData.value->format = vpiIntVal;
  cbData.index = 0;
  cbData.user_data = (PLI_BYTE8*)input;

  vpi_register_cb(&cbData);
   
  if (vpi_chk_error(&err)) {
    vpi_printf("%s\n", err.message);
  }
  
}

PLI_INT32 vpit_setup( PLI_BYTE8 *user_data )
{

    vpi_printf( "\n===========================\n" );
    vpi_printf( "      VPI TEST THINGY\n"   );
    vpi_printf( "===========================\n"   );

    // put a callback on the clock
    registerCkCB();
    assert( vpi_chk_error(0) == 0 );

    return 0;
}
// 
/*****************************************************************************
 *
 * vpit_RegisterTfs
 *
 * Registers test functions with the simulator.
 *
 *****************************************************************************/

void vpit_RegisterTfs( void )
{
    s_vpi_systf_data systf_data;
    vpiHandle        systf_handle;

    systf_data.type        = vpiSysTask;
    systf_data.sysfunctype = 0;
    systf_data.tfname      = "$initVpi";
    systf_data.calltf      = vpit_setup;
    systf_data.compiletf   = 0;
    systf_data.sizetf      = 0;
    systf_data.user_data   = 0;
    systf_handle = vpi_register_systf( &systf_data );
    vpi_free_object( systf_handle );

    srand(5150);
}
#ifndef CARBON
void (*vlog_startup_routines[])() = {
    vpit_RegisterTfs,
    0
};
#endif
