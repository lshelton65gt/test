module test();
   wire [31:0] i;        // carbon depositSignal
   wire [31:0] o;        // carbon observeSignal
   reg 	       ck;   // carbon depositSignal
   reg 	       r;    // carbon depositSignal

   
   initial begin
      $initVpi;
      ck = 0;
      r = 0;      
      #50;
      r = 1;
      

   end

`ifdef CARBON
`else
   always #10 ck = ~ck;   
`endif
   
   dut dut1(ck, i, o, r);

   // the tb will drive from here...
   bfm bfm1(i, o);
   
endmodule // test

module bfm(i,o);
   input [31:0] i;  // carbon depositSignal
   output [31:0] o; // carbon examineSingal
   
endmodule

module dut(ck, i, o, r,a);
   input [31:0] i;
   input 	ck, r;
   output [31:0] o;

   wire [31:0] 	 w;

   ff s1(ck, i, w, r);
   ff s2(ck, w, o, r);
   
endmodule // dut


module ff(ck, i, o, r);
   input [31:0] i;
   input 	ck, r;
   output [31:0] o;

   reg [31:0] 	 o;
   
   always @(posedge ck or negedge r) begin
      if(!r) o <= 0;
      else o <= i;           
   end

endmodule
   

module concat(ck, hi, lo, o);
parameter IN_WIDTH = 32;
input ck;
input [IN_WIDTH - 1:0], hi, lo;
output reg [(IN_WIDTH*2) -1:0] o;

always @(posedge ck) begin
o <= {hi, lo};
end

endmodule

module mem(i, addr, o), we, ck;
input [127:0] i;
input [1:0] addr;
ouput reg [127:0] o;
input we, ck;

reg [127:0] m [3:0];

always @(posedge ck) begin

if (we) m [addr] <= i;

o <= m[addr];
end


endmodule