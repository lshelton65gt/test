#include "libdesign.h"
#include "CarbonVpiUtil.h"
#include "CarbonVpiObjects.h"
#include "VpiMaster.h"

#ifdef DEBUG
#define ASSERT(x) (assert((x)))
#else
#define ASSERT(x)
#endif

void tbSetup();
void errChkPrint(bool errAssert = false)
{
   s_vpi_error_info err;
   int status = vpi_chk_error(&err);
   if (status) {
      //printf("%s\n" , err.message);
      vpi_printf("%s\n" , err.message);
      assert(!errAssert);
   }
}



void testVpiGet() 
{
   /*
    The following support is provided:
    o vpiPort objects support vpiType, vpiSize, vpiDirection, 
      vpiPortIndex, vpiScalar, and vpiVector requests
    o vpiNet objects support vpiType, vpiSize, vpiArray, vpiNetType, 
      vpiScalar, vpiSigned, and vpiVector requests
    o vpiReg objects support vpiType, vpiSize, vpiArray, vpiScalar, 
      vpiSigned, and vpiVector requests
    o vpiMemory objects support vpiType and vpiSize requests
    o vpiMemoryWord objects support vpiType and vpiSize requests
    o vpiParameter objects support vpiType, vpiSize, vpiConstType, 
      vpiLocalParam, and vpiSigned requests
    o vpiConstant objects support vpiType, vpiSize, and vpiConstType 
      requests
    o vpiIterator objects support vpiType and vpiIteratorType 
      requests
    o vpiCallback objects support vpiType requests*/

/* VMOD Support for vpiModule:
       vpiType, vpiArray, vpiCellInstance, 
      vpiDefNetType, vpiTimePrecision, vpiTimeUint, and vpiTopModule requests
   CARBON Support:
      vpiType
 */
   vpi_printf("=== vpi_get() test for vpiModule type ===\n");
   vpiHandle module = vpi_handle_by_name("test.dut1",0);
   errChkPrint(true);

   PLI_INT32 val = vpi_get(vpiType, module);
   errChkPrint(true);

   assert(val == vpiModule);
   vpi_free_object(module);
   vpi_printf("=== PASSED ===\n");



   /*
VMOD
   vpiPort objects support vpiType, vpiSize, vpiDirection, 
      vpiPortIndex, vpiScalar, and vpiVector requests
Carbon:
vpiType, vpiSize, vpiDirection, vpiScalar, and vpiVector
*/
   vpi_printf("=== vpi_get() test for vpiPort type ===\n");
   module = vpi_handle_by_name("test.dut1",0);
   vpiHandle portIt = vpi_iterate(vpiPort, module);



   // Get a handle to the .ck input port
   vpiHandle port = vpi_scan(portIt);
   errChkPrint(true);

   while(port) {
	   const char* tmpStr = vpi_get_str(vpiName, port);
	   if(strcmp(tmpStr, "ck") == 0) 
		   break;

	   port = vpi_scan(portIt);
	   errChkPrint(true);
   }
   assert(port);
   vpi_printf("Got port %s \n",vpi_get_str(vpiName, port));

   // TODO: need to try some different sizes.  say 1, 32, 63, 64, 65
   //       many of the tests (scalar, vector etc) would benefit
   PLI_INT32 size = vpi_get(vpiSize, port);
   assert(size == 1);

   val = vpi_get(vpiType, port);
   assert(val == vpiPort);

   val = vpi_get(vpiScalar, port);
   assert(val == 1);

   val = vpi_get(vpiVector, port);
   assert(val == 0);

   val = vpi_get(vpiDirection, port);
   assert(val == vpiInput);

   const char* portstr = vpi_get_str(vpiType, port);
   assert (strcmp(portstr, "vpiPort") == 0);

   portstr = vpi_get_str(vpiName, port);
   assert (strcmp(portstr, "ck") == 0);

   portstr = vpi_get_str(vpiFullName, port);
   assert (strcmp(portstr, "test.dut1.ck") == 0);

   vpi_printf("=== PASSED ===\n");

}

void testVpiMem()
{
   vpi_printf("=== vpi_get() test for vpiMemory type ===\n");
    vpiHandle mem = vpi_handle_by_name("test.dut1.m1.m",0);
    assert(mem);
    errChkPrint();

    PLI_INT32 val;
    val = vpi_get(vpiType, mem);
    assert(val == vpiMemory);
    errChkPrint();

    // try and iterate over the words
    vpiHandle wordIt = vpi_iterate(vpiMemoryWord, mem);
    assert(wordIt);
    errChkPrint();

    // Pull a word from the iterator
    vpiHandle memWord = vpi_scan(wordIt);
    assert(memWord);
    val = vpi_get(vpiType, memWord);
    assert(val == vpiMemoryWord);
    errChkPrint();


	assert(strcmp(vpi_get_str(vpiType, memWord), "vpiMemoryWord") == 0);
	errChkPrint();

	
	assert(strcmp(vpi_get_str(vpiType, wordIt), "vpiIterator") == 0);
	errChkPrint();


   vpi_printf("=== PASSED ===\n");

}

int main()
{
   /*
   ** Do command line and stuff
   */      
   VpiSimMaster sim(carbon_design_create);
   sim.createClock("test.ck", 75);

   tbSetup();

   sim.run();

   return 0;
}


// cbReadOnlySynch callback
PLI_INT32 readOnlyCallback(s_cb_data* cbData)
{
   vpi_printf("cbReadOnlySynch callback @ %d\n", cbData->time->low);
   return 1;
}

void writeData()
{
	/*
	** It wouldn't actually be very performance friendly
	** to be constantly creating and destroying handles like this
	*/
	vpiHandle dutIn = vpi_handle_by_name("test.bfm1.duti", 0);
	assert(dutIn);

	assert(vpi_get(vpiSize, dutIn) == 32);

	t_vpi_value val;
	val.format = vpiVector;

	vpi_get_value(dutIn, &val);

	

}
void testCbData(s_cb_data* cbData, int expVal) 
{

	PLI_INT32 size, type;

	s_vpi_error_info err;

	size = vpi_get(vpiSize, cbData->obj);
	//assert(vpi_chk_error(&err) == 0);
	errChkPrint();

	assert(size == 1);

	type = vpi_get(vpiType, cbData->obj);
	assert(vpi_chk_error(&err) == 0);
	assert(type == vpiNet);

	s_vpi_value val;
	val.format = vpiVectorVal;
	vpi_get_value(cbData->obj, &val);
	assert(vpi_chk_error(&err) == 0);

	assert(val.value.vector->aval == expVal);

	UInt64 calltime = (UInt64(cbData->time->high) << 32) | cbData->time->low;
	vpi_printf("%ld clock callback -  size: %d, type: %d", calltime, size, type); 
	vpi_printf(" value: %d\n", val.value.vector->aval);
	assert(vpi_chk_error(&err) == 0);

}
// value change callback that we'll attack to clocks
PLI_INT32 clockCallback(s_cb_data* cbData)
{

   static bool testsDone = false;
   
   UInt64 calltime = (UInt64(cbData->time->high) << 32) | cbData->time->low;

	static int expVal = 1;
	testCbData(cbData, expVal);


   if (calltime > 1100 && !testsDone) {
      testVpiGet();
      testVpiMem();
      testsDone = true;
   }


	// get_value with an integer
	s_vpi_value valInt;
	valInt.format = vpiIntVal;
	vpi_get_value(cbData->obj, &valInt);
	errChkPrint();
	assert(valInt.value.integer == expVal);

	expVal = !expVal;

	writeData();

	// try to register a callback with ReadOnlySynch
	s_cb_data cb2;


	cb2.reason = cbReadOnlySynch;
	cb2.cb_rtn = readOnlyCallback;
	cb2.obj = 0;
	cb2.time = new s_vpi_time;
	cb2.value = new s_vpi_value;
	cb2.index = 0;
	cb2.user_data = 0;


	cb2.time->type = vpiSimTime;
	cb2.time->low = 2100;
	cb2.time->high = 0;

	cb2.value->format = vpiIntVal;

	if(calltime == 1050) {

		vpiHandle rdOnlyCb = vpi_register_cb(&cb2);
      errChkPrint();
   }

   if (calltime > 5000) vpi_control(vpiFinish);

   return 1;
}



/*
** This funciton looks up a module by name and iterates on it's ports
*/ 
void testIteratePorts()
{
   vpi_printf("=== vpi port iteration test ===");
   vpiHandle dutTop = vpi_handle_by_name("test.dut1",0);
   errChkPrint();

   vpi_printf("  vpiName = %s\n", vpi_get_str(vpiName, dutTop));
   vpi_printf("  vpiFullName = %s\n", vpi_get_str(vpiFullName, dutTop));

   vpiHandle it = vpi_iterate(vpiPort, dutTop); 
   assert(it);
   vpiHandle port = vpi_scan(it);
   while (port)
   {
      vpi_printf("  port = %s\n", vpi_get_str(vpiName, port));
      port = vpi_scan(it);
   }
}

/*
** The end of compile callback is going to register the value change callback 
** for the clock.
*/
PLI_INT32 endCompCallback(s_cb_data* d)
{
   vpi_printf("End Of Compile Callback\n");

   // Set up a value change callback
   vpiHandle hdlCk = vpi_handle_by_name("test.ck", 0);

   // test vpiName and vpiFullName for the net just for kicks
   vpi_printf("  vpiName = %s\n", vpi_get_str(vpiName, hdlCk));
   vpi_printf("  vpiFullName = %s\n", vpi_get_str(vpiFullName, hdlCk));

   s_cb_data cbData;

   cbData.reason = cbValueChange;
   cbData.cb_rtn = clockCallback;
   cbData.obj = hdlCk;
   cbData.time = new s_vpi_time;
   cbData.value = new s_vpi_value;
   cbData.index = 0;
   cbData.user_data = 0;

   cbData.time->type = vpiSimTime;
   cbData.value->format = vpiVectorVal;

   vpiHandle cbClock = vpi_register_cb(&cbData);
   errChkPrint();

   testIteratePorts();
   return 0;
}



// routine to set up test

/*
** Registers a Callback for clock changes.
** the carious tests will be setup in that callback and tried out there.
**
** This routine also ends up test vpi_register_cb for cbEndOfCompile
**
*/
void tbSetup()
{
   s_cb_data cbData;

   cbData.reason = cbEndOfCompile;
   cbData.cb_rtn = endCompCallback;
   cbData.obj = 0;
   cbData.time = new s_vpi_time;
   cbData.value = new s_vpi_value;
   cbData.index = 0;
   cbData.user_data = 0;

   cbData.time->type = vpiSuppressTime;
   cbData.value->format = vpiSuppressVal;

   vpiHandle cbEndComp = vpi_register_cb(&cbData);
   ASSERT(vpiHandle);

   errChkPrint();
   delete cbData.time;
   delete cbData.value;
}
