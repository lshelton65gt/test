// -*- C++ -*-
/*
**
** Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.
**
** THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
** DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
** THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
** APPEARS IN ALL COPIES OF THIS SOFTWARE.
**
*/

#include "carbon/carbon_capi.h"
#include <vector>
#include <queue>
#include <cassert>



const int NEXT_TIME_CB_SLOT = 8;
const int CLOCK_SLOT    =  9;
const int CARBON_SCHEDULE_SLOT = 10;
const int READ_ONLY_SYNCH_SLOT = 12;


typedef CarbonObjectID*(*createFn)(CarbonDBType, CarbonInitFlags);
class EventAction;

/*
** Following struction keeps track of all the Actions 
** That get queued up 
*/
struct VpiMasterEvent
{
   VpiMasterEvent(CarbonTime t, int slot, EventAction* ea): time(t), slot(slot), action(ea) {}
   CarbonTime time;
   int slot;
   EventAction *action;
};

/*
** Actions get stored in a prioritiy queue so we need
** to be able to compare them
*/
inline bool operator>  (const VpiMasterEvent& lhs,
                        const VpiMasterEvent& rhs)
{
   if(lhs.time > rhs.time) return true;
   else if (lhs.time == rhs.time) return lhs.slot > rhs.slot;
   else return false;

}


/*
** The master keeps track of time and runs the simulation
** In the case of a VPI integration, the master needs to 
**  1. Keep track of simulation objects (the CarbonObjectID,
**    Waveforms, etc)
**  2. keep track of clock / reset generation
**  3. track and call time base callbacks
**  4. run the design schedule ( call carbonSchedule )
**  5. Other things???
*/
class VpiSimMaster
{
public:
   VpiSimMaster( createFn );
   ~VpiSimMaster();
   CarbonObjectID* getVHM();
   void createClock(const char*, CarbonTime);
   void registerVpiTimeCb(s_cb_data* cbData);
   void registerVpiEndCompCb(s_cb_data* cbData);   
   void registerVpiCb(s_cb_data* cbData);   
   void run();
   void stop() {mStopped = true;}
   void finish() {mFinished = true;}
private:
   //! Compiled RTL Object
   CarbonObjectID* mVhm;  
   bool mStopped, mFinished;
   //! sorted queue of simulation events
   std::priority_queue<VpiMasterEvent, std::vector<VpiMasterEvent>, std::greater<VpiMasterEvent> > mEventQ;
   std::vector<s_cb_data*> mEndOfCompCbList;
};

/*
** All simulation events have an associated action
** That action might be:
**   - Calling a callback
**   - toggling a clock
**   - calling carbon schedule
**
** The EventAction class just defines an interface.  That 
** the master can use so that it can ignore the details of
** the action
*/
class EventAction 
{
public:
   //! Go do this event
   virtual void execute(CarbonTime t) = 0;
   //! Does this event lead to others that we need to queue?
   virtual bool needsRequeue() const = 0;
   //! Does this event need the design schedule to be run?
   virtual bool needsSchedule() const = 0;
   //! When should following events be queued for
   virtual CarbonTime nextEventTime() const = 0;
};


/*
** Class for representing VPI Callbacks in the event queue.
** These are callbacks based on sim time, not value change 
** callbacks.  The value change callbacks are registered with 
** the VHM API
**
** Currently this assumes that callbacks are gone once they're 
** executed.  It may be the case that some need to be requeued
*/
class VpiCallbackAction : public EventAction
{
public:
   VpiCallbackAction(s_cb_data* cbd): mCbData(cbd) {}
   //! Go do this event
   void execute(CarbonTime t);
   //! Does this event lead to others that we need to queue?
   bool needsRequeue() const {return false;}
   //! Does this event need the design schedule to be run?
   bool needsSchedule() const {return false;}
   //! When should following events be queued for
   CarbonTime nextEventTime() const {return 0;}
private:
   s_cb_data* mCbData;
private:
   VpiCallbackAction();

};

class VpiMasterSignal;
class VpiSignalAction : public EventAction
{
public:
   VpiSignalAction(CarbonTime t, VpiMasterSignal* s) :
       mSignal(s) {}
   VpiSignalAction(VpiMasterSignal*s): mSignal(s) {}
   void execute(CarbonTime t);
   bool needsRequeue() const;
   bool needsSchedule() const;
   VpiMasterEvent nextEvent();
   CarbonTime nextEventTime() const;
protected:
   VpiMasterSignal* mSignal;

};

class VpiMasterSignal 
{
protected:
   int mWidthm, mWords;
   UInt32 *mVal;
   CarbonNetID* mNet;
   CarbonObjectID* mObj;
public:
   VpiMasterSignal(CarbonObjectID* obj, CarbonNetID* net):
      mVal(0),  mNet(net),mObj(obj) {
         if (mNet) 
         {
            int width = carbonGetBitWidth(net);
            assert(width <= 32);
            mVal = new UInt32;
         }
      }
      virtual void applyValue();  
      virtual bool update(CarbonTime t) = 0;
      //virtual bool hasMoreEvents();// = 0;
      virtual CarbonTime nextEventTime() const = 0;
};




/*
** Represents a clock that changes on a given period.
*/
class VpiMasterClock : public VpiMasterSignal
{
private:
   const CarbonTime mPeriod;
   CarbonTime mNextChange;
public:
   VpiMasterClock(CarbonObjectID*, const char*, CarbonTime);
   CarbonTime nextEventTime() const {return mNextChange;}
   bool update(CarbonTime t);
   bool hasMoreEvents() { return true; }
private:
   VpiMasterClock();
   VpiMasterClock(const VpiMasterClock&);
};

extern VpiSimMaster* gMaster;
