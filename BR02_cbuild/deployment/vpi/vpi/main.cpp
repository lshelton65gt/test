#include "libdesign.h"
#include "carbon/vpi_user.h"

//extern "C" void vpit_RegisterTfs( void );
extern "C" PLI_INT32 vpit_setup( PLI_BYTE8 *user_data );


CarbonObjectID* gObj;

int main()
{
    UInt32 ZERO = 0;
    UInt32 ONE  = 1;
    CarbonTime t = 0;

    gObj = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
    CarbonWaveID* w = carbonWaveInitFSDB(gObj, "out.fsdb", e10ns);
    carbonDumpVars(w, 0, "test");

    vpit_setup(0);


    CarbonNetID* ck = carbonFindNet(gObj, "test.ck");

    carbonDeposit(gObj, ck, &ONE, 0);
    carbonSchedule(gObj, t++);
    carbonDeposit(gObj, ck, &ZERO, 0);
    carbonSchedule(gObj, t++);

    carbonDeposit(gObj, ck, &ONE, 0);
    carbonSchedule(gObj, t++);
    carbonDeposit(gObj, ck, &ZERO, 0);
    carbonSchedule(gObj, t++);

    carbonDeposit(gObj, ck, &ONE, 0);
    carbonSchedule(gObj, t++);
    carbonDeposit(gObj, ck, &ZERO, 0);
    carbonSchedule(gObj, t++);

    carbonDeposit(gObj, ck, &ONE, 0);
    carbonSchedule(gObj, t++);
    carbonDeposit(gObj, ck, &ZERO, 0);
    carbonSchedule(gObj, t++);

    carbonDeposit(gObj, ck, &ONE, 0);
    carbonSchedule(gObj, t++);
    carbonDeposit(gObj, ck, &ZERO, 0);
    carbonSchedule(gObj, t++);

    carbonDeposit(gObj, ck, &ONE, 0);
    carbonSchedule(gObj, t++);
    carbonDeposit(gObj, ck, &ZERO, 0);
    carbonSchedule(gObj, t++);

    carbonDeposit(gObj, ck, &ONE, 0);
    carbonSchedule(gObj, t++);
    carbonDeposit(gObj, ck, &ZERO, 0);
    carbonSchedule(gObj, t++);

    carbonDeposit(gObj, ck, &ONE, 0);
    carbonSchedule(gObj, t++);
    carbonDeposit(gObj, ck, &ZERO, 0);
    carbonSchedule(gObj, t++);


    carbonDestroy(&gObj);
}
