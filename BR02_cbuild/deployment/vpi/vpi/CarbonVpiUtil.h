// -*- C++ -*-
/*
**
** Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.
**
** THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
** DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
** THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
** APPEARS IN ALL COPIES OF THIS SOFTWARE.
**
*/
#ifndef __CARBONVPIUTIL_H__
#define __CARBONVPIUTIL_H__

#include "vpi_user.h"
#include "carbon/carbon_capi.h"
void setVpiError(PLI_INT32 level, PLI_BYTE8* msg,
                 PLI_BYTE8* code) ;


extern const char * vpiTypeString[];

/*
** Global Status
*/
#ifdef VPI_UNSUPPORTED_ASSERT
#define UNSUPPORTED(x) (assert( (x) == 0));
#else
#define UNSUPPORTED(x) unsupported((x));
#endif 

void copyCallbackData(s_cb_data*dest, const s_cb_data *src);

/*
** This routine is just for calling out unsupported functions when they 
** are hit.  A VPI error is set when this routine is called
*/
inline void unsupported(PLI_BYTE8* msg) 
{
   void ClearVpiErrors();
   setVpiError(vpiError, msg, "VPI_FOO");    
}

void vpiTimeToCarbonTime(CarbonTime *vhmTime, const t_vpi_time*);
CarbonTime vpiTimeToCarbonTime(const t_vpi_time*);

void carbonTimeToVpiTime(t_vpi_time* vpiTime, 
                         const CarbonTime vhmTime);

void carbonValueToVpiValue(t_vpi_value* vpiVal, const UInt32* vhmVal,
                      const UInt32* vhmDrive, int bits);
void vpiVecValToCarbon(t_vpi_vecval *vpiVal, UInt32 *vhmVal, UInt32 *vhmDrive, int bits);


// Global Object Handle
extern CarbonObjectID* gObj;
extern CarbonObjectID* gSim;

#endif

