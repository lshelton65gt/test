// -*- C++ -*-
/*
**
** Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.
**
** THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
** DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
** THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
** APPEARS IN ALL COPIES OF THIS SOFTWARE.
**
*/

#include "vpi_user.h"
#include "carbon/carbon_capi.h"
#include "carbon/carbon_dbapi.h"
#include "CarbonVpiUtil.h"
#include "CarbonVpiObjects.h"
#include "VpiMaster.h"

#include <cassert>


s_vpi_value valueBuf;
s_vpi_vecval vecValBuf;

VpiSimMaster *gMaster;
CarbonObjectID* gObj;
CarbonDB* gDB;

vpiHandle CarbonVpiModule::vpi_iterate(PLI_INT32 type)
{
   CarbonDBNodeIter* nodeIt = 0;
   CarbonVpiIterator* vpiIt = 0;

   switch(type)
   {
   case vpiPort:
      nodeIt = carbonDBLoopChildren(mDB, mNode);
      assert(nodeIt);
      vpiIt = new CarbonVpiIterator(gObj, mDB, nodeIt, type);
      return reinterpret_cast<vpiHandle>(vpiIt);
      break;
   default:
      setVpiError(vpiError, "Unsupported iteration type", "vpifoo");
   }
}

/*
** Hackishly find out if this is a port.  This ought to be 
** possible using the DB API, but the functionality isn't
** exposed yet.  So we have to go through the VHM API 
*/
bool CarbonVpiIterator::isPort(const CarbonDBNode* node)
{
   if (!carbonDBIsLeaf(mDB, node) ) return false;
   const char* name = carbonDBNodeGetFullName(mDB, node);
   CarbonNetID* net = carbonFindNet(mVhm, name);
   if (net) {
	   return carbonIsInput(mVhm, net) ||
		   carbonIsOutput(mVhm, net) ||
		   carbonIsBidirect(mVhm, net);
   } else {
	   return false;
   }
}	

vpiHandle CarbonVpiIterator::vpi_scan()
{
   assert(mType == vpiPort);  


   const CarbonDBNode* node = carbonDBNodeIterNext(mIter);
   while(node && !isPort(node) ) {
      node = carbonDBNodeIterNext(mIter);
   }
   if (!node) {
      carbonDBFreeNodeIter(mIter);
      return 0;
   }

   /*
   ** maybe ports should work with the CarbonNetID*?
   */
   CarbonVpiPort* port = new CarbonVpiPort(mVhm, mDB, node);

   return reinterpret_cast<vpiHandle>(port);
}

CarbonVpiPort::CarbonVpiPort(CarbonObjectID* _obj, CarbonDB* _db, const 
                             CarbonDBNode* _node):
mNode(_node), 
mDB(_db),  
CarbonVpiNet(_obj, carbonFindNet(_obj, carbonDBNodeGetFullName(_db, _node)))
{
	mType = vpiPort;
}

void CarbonVpiModule::getName(char* buf, int bufSize) const
{
   
   const char* n = carbonDBNodeGetLeafName(mDB, mNode);

   if (strlen(n) >= bufSize) {
      setVpiError(vpiWarning, "Buffer to small for object name, truncating", "VPI_FOO");
   }

   strncpy(buf, n, bufSize - 1);
}


vpiHandle CarbonVpiNet::vpi_handle(PLI_INT32 type)
{
   switch(type) {
   case vpiLeftRange:
      return reinterpret_cast<vpiHandle>(new CarbonVpiConstant(carbonGetLSB(mNet)));
      break;
   case vpiRightRange:
      return reinterpret_cast<vpiHandle>(new CarbonVpiConstant(carbonGetMSB(mNet)));
      break;
   default:
      setVpiError(vpiError, "Unsupported type for vpi_handle()", "");
      return 0;
   }

}

CarbonVpiMemoryWord::CarbonVpiMemoryWord(CarbonObjectID* _vhm, CarbonMemoryID* _mem, SInt64 _index) : 
CarbonVpiHandle(vpiMemoryWord),
mVhm(_vhm), 
mMem(_mem),
address(_index),
mWidth(carbonMemoryRowWidth(mMem) ),
mWords(carbonMemoryRowNumUInt32s(mMem) )
{
	mDataBuf = new UInt32[mWords];
}



void CarbonVpiMemoryWord::vpi_get_value(s_vpi_value* vpiVal)
{
   carbonValueToVpiValue(vpiVal, mDataBuf, 0, mWidth);
}

vpiHandle CarbonVpiMem::vpi_iterate(PLI_INT32 type)
{ 
    if (type == vpiMemoryWord) {
	return reinterpret_cast<vpiHandle> ( new CarbonVpiMemWordIterator(mVhm, mMem) );
    } else {
	UNSUPPORTED ("vpi_iterate error"); return 0;
    }
}

vpiHandle CarbonVpiMemWordIterator::vpi_scan()
{
    if(mNextAddr > mHiAddr) return 0;

	return reinterpret_cast<vpiHandle> (new    CarbonVpiMemoryWord(mVhm, mMem, mNextAddr++));
}

vpiHandle CarbonVpiMem::vpi_handle(PLI_INT32) { UNSUPPORTED ("support me "); return 0; }

bool CarbonVpiMem::isValidMemAddress(SInt64 addr)
{
   SInt64 hiAddr = carbonGetHighAddr(mMem);
   SInt64 loAddr = carbonGetLowAddr(mMem);

   // swap addresses if hi < low ie 'reg [31:0] mem [10:1];'
   if(hiAddr < loAddr) {
        SInt64 temp = hiAddr;
        hiAddr = loAddr;
        loAddr = temp;
   } 

   if (addr < loAddr) {
      return false; 
   } else if (addr > hiAddr) {
      return false;
   } else {
      return true;
   }
}
vpiHandle CarbonVpiMem::vpi_handle_by_index(PLI_INT32 index) 
{
   if(!isValidMemAddress(index)) {
      setVpiError(vpiError, "Attempted to read invalid array index using vpi_handle_by_index", "");
      return 0;
   }
   // return a handle to the word
   return reinterpret_cast<vpiHandle> (new CarbonVpiMemoryWord(mVhm, mMem, index));
}

void CarbonVpiModule::getFullName(char* buf, int bufSize) const
{
   
   const char* n = carbonDBNodeGetFullName(mDB, mNode);

   if (strlen(n) >= bufSize) {
      setVpiError(vpiWarning, "Buffer to small for object name, truncating", "VPI_FOO");
   }

   strncpy(buf, n, bufSize - 1);
}

int CarbonVpiHandle::vpi_get(PLI_INT32 property) {
   /*
   ** Each sub class for a type can override the 
   ** various property calls to return something more appropriate.
   */
   switch (property) {
      case vpiType: return this->getType();
      case vpiSize: return this->getSize();
	  case vpiScalar: return this->getIsScalar();
	  case vpiVector: return this->getIsVector();	
	  case vpiDirection: return this->getDirection();
      default: 
         UNSUPPORTED("Unsupported VPI Call, vpi_get_str");
         return vpiUndefined;
   };
}

void CarbonVpiHandle::vpi_get_str(PLI_INT32 property, char* buf, int bufSize)
{
	const char*tmp;
   switch(property) {
      case vpiName:
         this->getName(buf, bufSize);
         break;
      case vpiFullName: 
         this->getFullName(buf, bufSize);
         break;
	  case vpiType:
		 tmp = vpiTypeString[this->getType()];
		  strncpy(buf, tmp, bufSize - 1);
		  break;
      default: 
         UNSUPPORTED("Unsupported VPI Call, vpi_get_str");
         buf[0] = '\0';
         break;
   };
}
vpiHandle CarbonVpiNet::vpi_iterate(PLI_INT32 type)
{
   setVpiError(vpiError, "vpi Iterate on vpiNet unsupported", "VPI_FOO");
   return 0;
}

/*
** returns the name of a net into a buffer.  Needed for vpi_get_str()
*/
void CarbonVpiNet::getFullName(char* buf, int bufSize) const
{

   carbonGetNetName(gObj, mNet, buf, bufSize);
}


/*
** Tries to return just the local name.  Not entirely robust...
** This routine assumes that '.' is the seperator and that the net 
** name is not escaped (in other words that the only reason '.' is in 
** the net is as a seperator)
*/
void CarbonVpiNet::getName(char* buf, int bufSize) const
{
	char* tmp = new char[bufSize];

	CarbonStatus sts = carbonGetNetName(gObj, mNet, tmp, bufSize);
	if(sts == eCarbon_ERROR) {
		setVpiError(vpiError, "Error when reading net name", "");
	}

	int len = strlen(tmp);
	int sepPos = 0;
	// assume that the last character (index len -1) is not a seperator
	for	(sepPos = len - 2;  sepPos >= 0; --sepPos) {
		if (tmp[sepPos] == '.' )	{break;}
	}
	strcpy(buf,&(tmp[sepPos + 1]) );


}




/*
** Callback used for value changes.
** just calls the callback that has the vpi callback
** function signature
*/
void vpiCB(CarbonObjectID* obj, CarbonNetID* net, CarbonClientData data,
           UInt32* val, UInt32* drive)
{
   s_cb_data *d = (s_cb_data*)data;

   carbonTimeToVpiTime(d->time, carbonGetSimulationTime(obj));
   carbonValueToVpiValue(d->value, val, drive, 
      carbonGetBitWidth(net));

   d->index = 0;

   (d->cb_rtn)(d);
}
CarbonVpiCB::CarbonVpiCB(t_cb_data* _vpiCbData):
 mVpiCbData(_vpiCbData)
{
}

CarbonVpiValueCB::CarbonVpiValueCB(t_cb_data* _vpiCbData):
CarbonVpiCB (_vpiCbData)
{
   CarbonVpiHandle *vpiHandleObj = reinterpret_cast<CarbonVpiHandle*>(mVpiCbData->obj);
   if (vpiHandleObj->vpi_get(vpiType) != vpiNet) {
      UNSUPPORTED("Value callbacks only supported on vpiNet type");
   }

   CarbonVpiNet* vpiNetObj = dynamic_cast<CarbonVpiNet*>(vpiHandleObj);
   CarbonNetValueCBDataID* retVal = 
      carbonAddNetValueChangeCB(gObj, vpiCB, 
      (CarbonClientData)mVpiCbData, vpiNetObj->carbonNet());
}


PLI_INT32 CarbonVpiPort::getDirection() const
{
	if (carbonIsInput(mObj, mNet)) return vpiInput;
	if (carbonIsOutput(mObj, mNet)) return vpiOutput;
	if (carbonIsBidirect(mObj, mNet)) return vpiInout;
}

void VpiMasterSignal::applyValue()
{
   assert(mObj);
   assert(mNet);
   carbonDeposit(mObj, mNet, mVal, 0);
};




VpiMasterClock::VpiMasterClock(CarbonObjectID* obj, const char* netName, CarbonTime per):
VpiMasterSignal(obj,0), 
mPeriod(per),
mNextChange(per)
{
   assert(obj);
   assert(mObj); 
   mNet = carbonFindNet(mObj, netName);
   assert(mNet);

   int width = carbonGetBitWidth(mNet);
   assert(width <= 32);
   mVal = new UInt32;

}

/*
** Move forward to time t.  If value changed, 
** update the next change time
*/
bool VpiMasterClock::update(CarbonTime t)
{
   if (t >= mNextChange) {
      mNextChange += mPeriod;
      *mVal = ~(*mVal);
      return true;
   }
   return false;
}

void CarbonVpiNet::vpi_get_value(s_vpi_value* val) const 
{
 
   if (val->format == vpiVectorVal ) {
      val->value.vector = &vecValBuf;
   } else if (val->format == vpiIntVal) {
      // pass
   } else {
      UNSUPPORTED("requested unsupported value type from net");  
   }
   carbonExamine(mObj, mNet, mValBuf, mDriveBuf);
   carbonValueToVpiValue(val, mValBuf, 0, mWidth);

}

CarbonVpiNet::CarbonVpiNet(CarbonObjectID* _obj, 
             CarbonNetID* _net): CarbonVpiHandle(vpiNet), mObj(_obj), mNet(_net) 
{
   mWidth = carbonGetBitWidth(mNet);
   int words = carbonGetNumUInt32s(mNet);
   mDriveBuf = new UInt32[words];
   mValBuf = new UInt32[words];

}
