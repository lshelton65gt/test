;;; Description: 
;;; Reads an operation, operands, and address from the ports
;;; Performs the operation on the opearnds, write the result to 
;;; the external data RAM @ adrress and address + 1.
;;; 
;;; While the calculation is being performed, '1' is put on 
;;; port 0 outputs to indicate that chip is busy.  When the
;;; s/w is about to sample new vaules, '0' is put on that port
;;; 
;;; Inpt port usage:
;;; 
;;; p0  operation
;;; p1 operand A
;;; p2 operand B
;;; p3 address
;;; 
;;; Operand Values: 0 = add, 1 = mul 

;;; Register usage
;;;    r0   Address to write result to
;;;    r1   Operation 0 = add, 1 = mul
;;;    r2   Operand B
;;;    r3   Operand A
	
loop:	mov r1, p0 		; get the op
	mov p0, #1		; indicate we're thinking
	
	mov r3, p1   		; get A
	mov r2, p2		; get B
	mov r0, p3	       	; get address

	mov a, r1
	jnb acc.0, addops
	ljmp mulops

	nop

addops:	mov a, r3
	add a, r2
	mov b, #0
	mov b.0, C
	ljmp wrres

mulops:	mov a, r3
	mov b, r2
	mul ab
	ljmp wrres
	nop

wrres:	movx @r0, a
	nop
	mov a,b
	inc r0
	movx @r0, a
	nop

	;; A is now corrupt!
clean:	mov p0, #0
	ljmp loop


	
	