//
// This pad implements a scan chain that sorta kinda
// looks like the IEEE scan standard, but I've grossly
// simplified it...
//
module pad(di, do, si, so, tck, shift, ten);
   
   input di;
   output do;

   input  si, tck, shift, ten;
   output so;

   wire   shiftCk = tck & shift;
   wire   updateCk = tck & !shift;   

   wire   shiftCkB, updateCkB;

   //
   // Clock Buffering
   //
   clkBuf sb (shiftCkB, shiftCk);
   clkBuf ub (updateCkB, updateCk);

   //
   // Test muxes
   //
   mux smux (z, di, si, shift);
   mux outmux(do, di, uq, ten);

   //
   // test flops
   //
   flop s(so, z, shiftCkB);
   flop u(uq, so, updateCkB);

endmodule

module mux(z, a, b, sel);
   input a,b,sel;
   output z;

   wire   z = sel ? b : a;
endmodule // mux

module flop(q,d,ck);
   input d,ck;
   output q;
   reg 	  q;
   
   always @(posedge ck) q <= d;
endmodule // flop
