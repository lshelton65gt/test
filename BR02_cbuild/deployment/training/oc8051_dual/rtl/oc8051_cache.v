// - * - Verilog - * -
//
// This module is the cache used for the icahce and dcache in the "subsystem" of the
// dual 8051 training system.
//
// Right not it's not so much of a cache as it is a memory...
//
// $Id: oc8051_cache.v,v 1.3 2005/01/08 18:27:57 mattg Exp $
//

/// ---- TEMPORAY
/// Data width is the width of the data bus
/// The internal memory widht is going to be 8 bits
/// regardless of the bus width
///
module   oc8051_cache (rst,
		      clk,
		      addr_i,
		      data_i,
		      data_o,
		      stb_i,
		      cyc_i,
		      ack_o,
		      we_i,
		      err_o,
		      );
   parameter DELAY = 0;
   parameter DWIDTH = 8;
   parameter AWIDTH = 16;

   parameter BYTES_PER_ENTRY = (DWIDTH / 8) + ((DWIDTH % 8) ? 1 : 0);
   
   parameter DEPTH = (1 << (AWIDTH -1));
   
   
   input [DWIDTH - 1:0] data_i;   
   input 		rst, clk;
   input [AWIDTH - 1:0] addr_i;
   
   input 		stb_i, we_i;
   input 		cyc_i; 
		
   
   output 		ack_o;
   output [DWIDTH - 1:0] data_o;

   output 		 err_o;
   wire 		 err_o = 1'b0;
   
   
   reg [7:0] 		 mem [0: (DEPTH * BYTES_PER_ENTRY) - 1];  

   reg 			 ackOut;  
   reg [DWIDTH - 1:0] 	 dataOut;
   reg [DWIDTH - 1:0] 	 data_tmp;
   
   integer 		 i;   

   reg [DWIDTH-1:0] 	 d0;
   reg 			 a0;

   wire [DWIDTH -1:0] 	 data_o = dataOut;
//   wire [DWIDTH -1:0] 	 data_o = d0;   
   wire 		 ack_o = ackOut;   
//   wire 		 ack_o = a0;

   
//   always @(posedge clk) begin
//      d0 <= dataOut;
//      a0 <= ackOut;
//   end

   always @(posedge clk) begin
     if (rst) begin
	ackOut <= 0;
	dataOut <= {DWIDTH{1'b0}};	
     end
     else begin
	if (stb_i) begin
	   ackOut <= 1'b1;	      
	   if (we_i) begin
	      // I'm gonna assume all
	      // writes ar 8 bits for now...
	      // that's safe in this model because the dbus is 8 bits
	      // and the ibus never gets a write.
	      mem[addr_i] <= data_i;
	      
	   end
	   else begin
	      // Need to generalize this code...
	      dataOut <= {mem[addr_i+3], mem[addr_i+2], mem[addr_i+1], mem[addr_i]};	      
	      
	   end
	end
	else begin
	   ackOut <= 1'b0;
	end // else: !if(stb_i)	
     end // else: !if(rst)
   end

       
endmodule // oc8051_xrom

