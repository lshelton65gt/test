module oc8051_cla (a,b,cin,cout,sum);
   input [7:0] a,b;
   input       cin;
   output      cout;
   output [7:0] sum;

   wire [8:0] 	carryBits;
   wire [7:0] 	carryProp, carryGen, sum;
      
   assign 	carryBits[0] = cin;
   assign 	cout = carryBits[8];
   
      
   pfa p0 (a[0], b[0], carryBits[0], sum[0], carryProp[0], carryGen[0]);
   pfa p1 (a[1], b[1], carryBits[1], sum[1], carryProp[1], carryGen[1]);
   pfa p2 (a[2], b[2], carryBits[2], sum[2], carryProp[2], carryGen[2]);
   pfa p3 (a[3], b[3], carryBits[3], sum[3], carryProp[3], carryGen[3]);
   pfa p4 (a[4], b[4], carryBits[4], sum[4], carryProp[4], carryGen[4]);
   pfa p5 (a[5], b[5], carryBits[5], sum[5], carryProp[5], carryGen[5]);
   pfa p6 (a[6], b[6], carryBits[6], sum[6], carryProp[6], carryGen[6]);
   pfa p7 (a[7], b[7], carryBits[7], sum[7], carryProp[7], carryGen[7]);

   carryGen cg (carryBits, carryProp, carryGen);      
   
endmodule

module carryGen(c, p, g);
   input [7:0] p, g;
   inout [8:0] c;

   carryBitCalc cbc1 (c[1], c[0], p[0], g[0] );
   carryBitCalc cbc2 (c[2], c[1], p[1], g[1] );
   carryBitCalc cbc3 (c[3], c[2], p[2], g[2] );
   carryBitCalc cbc4 (c[4], c[3], p[3], g[3] );
   carryBitCalc cbc5 (c[5], c[4], p[4], g[4] );
   carryBitCalc cbc6 (c[6], c[5], p[5], g[5] );
   carryBitCalc cbc7 (c[7], c[6], p[6], g[6] );
   carryBitCalc cbc8 (c[8], c[7], p[7], g[7] );

   

endmodule // carryGen


module carryBitCalc (cout, c, p, g);
   input c, g, p;
   output cout;
   
   ocAnd  i1 (z, p, c);
   ocOr   i2 (cout, z, g);   
   
endmodule

module pfa (a,b,c, s,p,g);
   input a,b,c;
   output s,p,g;


   ocXor xor1(p, a, b);
   ocXor xor2(s, c, p);
   ocAnd and1(g, a, b);

endmodule
   
module ocOr(z,a,b);
   output z;
   input a, b;

   or(z,a,b);
endmodule // ocOr

module ocXor(z, a, b);
   input a, b;
   output z;

   xor(z,a,b);
endmodule // ocXor

module ocAnd(z,a,b);
   input a,b;
   output z;

   and(z,a,b);   
   
endmodule