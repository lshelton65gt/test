//
// 8051 Dual Core Systems - Subsytem block
//
// Contains 1 processor core + icache & dcache
//
//


module oc8051_subsys(core_rst_i, mem_rst_i, core_clk_i, mem_clk_i, int0_i, int1_i,
		     p0_i, p0_o,
		     p1_i, p1_o,
		     p2_i, p2_o,
		     p3_i, p3_o,
		     rxd_i, 
		     txd_o,
		     t0_i, 
		     t1_i,
		     t2_i, 
		     t2ex_i
		     );
   
   // Clocks & Resets
   input    core_rst_i;
   input    mem_rst_i;
   input    core_clk_i;
   input    mem_clk_i;

   // interrupt interface
   input    int0_i; 
   input    int1_i;
   
   // port interface
   input [7:0]  p0_i;
   input [7:0] 	p1_i;
   input [7:0] 	p2_i;
   input [7:0] 	p3_i;

   output [7:0] p0_o;
   output [7:0] p1_o;
   output [7:0] p2_o;
   output [7:0] p3_o;
   
   // serial interface
   input 	rxd_i; 
   output 	txd_o;
   
   // counter interface
   input 	t0_i; 
   input 	t1_i;
   input 	t2_i; 
   input 	t2ex_i;
   

   // Instruction Cache interface
   wire [15:0] 	instrAddr;
   wire [31:0] 	instrRdData;   
   wire 	instrStrobe;   
   wire 	instrAck;   
   wire 	instrCyc;
   wire 	instrErr;
   
   // Data Cache interface
   wire [7:0] 	dataRdData;
   wire [7:0] 	dataWrData;
   wire [15:0] 	dataAddr;
   wire 	dataWrEn;
   wire 	dataAck; 
   wire 	dataStrobe; 
   wire 	dataCyc;
   wire 	dataErr;
   
   
   oc8051_top oc8051 (.wb_rst_i  ( core_rst_i),
		      .wb_clk_i   (core_clk_i),
      
		      //interface to instruction rom
		      .wbi_adr_o        (instrAddr), 
		      .wbi_dat_i        (instrRdData),
		      .wbi_stb_o        (instrStrobe), 
		      .wbi_ack_i        (instrAck), 
		      .wbi_cyc_o        (instrCyc),
		      .wbi_err_i        (instrErr),
		      .ea_in            (1'b0),
		      //interface to data ram
		      .wbd_dat_i        (dataRdData), 
		      .wbd_dat_o        (dataWrData),
		      .wbd_adr_o        (dataAddr), 
		      .wbd_we_o         (dataWrEn), 
		      .wbd_ack_i        (dataAck), 
		      .wbd_stb_o        (dataStrobe), 
		      .wbd_cyc_o        (dataCyc),
		      .wbd_err_i        (dataErr),

		      // interrupt interface
		      .int0_i       (int0_i), 
		      .int1_i       (int1_i),

		      // port interface
		      .p0_i      (p0_i),
		      .p0_o      (p0_o),
		      .p1_i      (p1_i),
		      .p1_o      (p1_o),
		      .p2_i      (p2_i),
		      .p2_o      (p2_o),
		      .p3_i      (p3_i),
		      .p3_o      (p3_o),

		      // serial interface
		      .rxd_i      (rxd_i), 
		      .txd_o      (txd_o),

		      // counter interface
		      .t0_i      (t0_i), 
		      .t1_i      (t1_i),
		      .t2_i      (t2_i),
		      .t2ex_i    (t2ex_i)
		      );

   oc8051_cache #(1,32) icache (.rst      (mem_rst_i),
				.clk      (mem_clk_i),
				.addr_i   (instrAddr),
				.data_i   (),
				.data_o   (instrRdData),
				.stb_i    (instrStrobe),
				.cyc_i    (instrCyc),  
				.ack_o    (instrAck),
				.we_i     (1'b0),
				.err_o    (instErr)
			    );

   oc8051_cache #(1,8) dcache (.rst      (mem_rst_i),
			       .clk      (mem_clk_i),
			       .addr_i   (dataAddr),
			       .data_i   (dataWrData),
			       .data_o   (dataRdData),
			       .stb_i    (dataStrobe),
			       .cyc_i    (dataCyc),  
			       .ack_o    (dataAck),
			       .we_i     (dataWrEn),
			       .err_o    (dataErr)
			    );

   
endmodule // dualCoreSys

