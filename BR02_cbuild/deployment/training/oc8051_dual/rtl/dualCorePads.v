module dualCorePads(core_rst_i, mem_rst_i, core_clk_i, mem_clk_i, 
		    scanin, tck, shift, ten, scanout, int0a_i, int1a_i, int0b_i, 
		    int1b_i, p0a_i, p0b_i, p1a_i, p1b_i, p2a_i, p2b_i, p3a_i, p3b_i, rxda_i, 
		    rxdb_i, t0a_i, t1a_i, t0b_i, t1b_i, t2a_i, t2b_i, t2exa_i, t2exb_i,
		    p0a_o, p0b_o, p1a_o, p1b_o, p2a_o, p2b_o, p3a_o, p3b_o, txda_o, txdb_o,
		    c_rst_A, m_rst_A, c_ck_A, m_ck_A, int0_A, int1_A, port0_in_A, port1_in_A,
		    port2_in_A, port3_in_A, rx_A, t0_A, t1_A, t2_A, t2ex_A, port0_out_A,
		    port1_out_A, port2_out_A, port3_out_A, tx_A, c_rst_B, m_rst_B, c_ck_B, 
		    m_ck_B, int0_B, int1_B, port0_in_B, port1_in_B, port2_in_B, port3_in_B,
		    rx_B, t0_B, t1_B, t2_B, t2ex_B, port0_out_B, port1_out_B, port2_out_B, 
		    port3_out_B, tx_B    
		    );

   input scanin, tck, shift, ten;
   output scanout;
   
   // Primary Input Pins
   input core_rst_i;   
   input mem_rst_i;    
   input core_clk_i;   
   input mem_clk_i;    
   input int0a_i;   	    
   input int1a_i;   	    
   input int0b_i;   	    
   input int1b_i;   	    

   input [7:0] p0a_i;     	    
   input [7:0] p0b_i;     	    
   input [7:0] p1a_i;     	    
   input [7:0] p1b_i;     	    
   input [7:0] p2a_i;     	    
   input [7:0] p2b_i;     	    
   input [7:0] p3a_i;     	    
   input [7:0] p3b_i;     	    

   input rxda_i;    	    
   input rxdb_i;    	    
   input t0a_i;     	    
   input t1a_i;     	    
   input t0b_i;     	    
   input t1b_i;     	      
   input t2a_i;     	    
   input t2b_i;     	    
   input t2exa_i;   	    
   input t2exb_i;
   
   // Primary Outputs
   output [7:0] p0a_o;
   output [7:0] p0b_o;
   output [7:0] p1a_o;
   output [7:0] p1b_o;
   output [7:0] p2a_o;
   output [7:0] p2b_o;
   output [7:0] p3a_o;
   output [7:0] p3b_o;
   
   output txda_o;
   output txdb_o;

   // To Proc A
   output c_rst_A;
   output m_rst_A;
   output c_ck_A;
   output m_ck_A;
   output int0_A;
   output int1_A;
   output [7:0] port0_in_A;
   output [7:0] port1_in_A;
   output [7:0] port2_in_A;
   output [7:0] port3_in_A;
   output rx_A;
   output t0_A;
   output t1_A;       
   output t2_A;
   output t2ex_A;

   
   // From Proc A
   input  [7:0] port0_out_A;
   input  [7:0] port1_out_A;
   input  [7:0] port2_out_A;   
   input  [7:0] port3_out_A;
   input  tx_A       ;

   // To Proc B
   output c_rst_B;
   output m_rst_B;
   output c_ck_B;
   output m_ck_B;
   output int0_B;
   output int1_B;
   output [7:0] port0_in_B;
   output [7:0] port1_in_B;
   output [7:0] port2_in_B;
   output [7:0] port3_in_B;
   output rx_B;
   output t0_B;
   output t1_B;      
   output t2_B;
   output t2ex_B;
   
   // From Proc B
   input  [7:0] port0_out_B;
   input  [7:0] port1_out_B;
   input  [7:0] port2_out_B;   
   input  [7:0] port3_out_B;
   input  tx_B;

   //
   // PAD instantiations for ProcA
   //


   // To Proc A
   clkBuf crstabuf (c_rst_A, core_rst_i);
   clkBuf mrstabuf (m_rst_A, mem_rst_i);
   clkBuf cclkabuf ( c_ck_A, core_clk_i);   
   clkBuf mclkabuf ( m_ck_A, mem_clk_i);

   
   pad p00 (int0a_i, int0_A,     scanin,      s0,   tck, shift, ten);
   pad p01 (int0a_i, int1_A,	    s0,          s1,   tck, shift, ten);

//   pad p02 (p0a_i,   port0_in_A, s1,          s2,   tck, shift, ten);
   pad p02_0 (p0a_i[0],   port0_in_A[0], s1,     s2a,   tck, shift, ten);
   pad p02_1 (p0a_i[1],   port0_in_A[1], s2a,    s2b,   tck, shift, ten);
   pad p02_2 (p0a_i[2],   port0_in_A[2], s2b,    s2c,   tck, shift, ten);
   pad p02_3 (p0a_i[3],   port0_in_A[3], s2c,    s2d,   tck, shift, ten);
   pad p02_4 (p0a_i[4],   port0_in_A[4], s2d,    s2e,   tck, shift, ten);
   pad p02_5 (p0a_i[5],   port0_in_A[5], s2e,    s2f,   tck, shift, ten);
   pad p02_6 (p0a_i[6],   port0_in_A[6], s2f,    s2g,   tck, shift, ten);
   pad p02_7 (p0a_i[7],   port0_in_A[7], s2g,    s2,    tck, shift, ten);
   
//   pad p03 (p1a_i,   port1_in_A, s2,          s3,   tck, shift, ten);
   pad p03_0 (p1a_i[0],   port1_in_A[0], s2,     s3a,   tck, shift, ten);
   pad p03_1 (p1a_i[1],   port1_in_A[1], s3a,    s3b,   tck, shift, ten);
   pad p03_2 (p1a_i[2],   port1_in_A[2], s3b,    s3c,   tck, shift, ten);
   pad p03_3 (p1a_i[3],   port1_in_A[3], s3c,    s3d,   tck, shift, ten);
   pad p03_4 (p1a_i[4],   port1_in_A[4], s3d,    s3e,   tck, shift, ten);
   pad p03_5 (p1a_i[5],   port1_in_A[5], s3e,    s3f,   tck, shift, ten);
   pad p03_6 (p1a_i[6],   port1_in_A[6], s3f,    s3g,   tck, shift, ten);
   pad p03_7 (p1a_i[7],   port1_in_A[7], s3g,    s3,    tck, shift, ten);

//   pad p04 (p2a_i,   port2_in_A, s3,          s4,   tck, shift, ten);
   pad p04_0 (p2a_i[0],   port2_in_A[0], s3,     s4a,   tck, shift, ten);
   pad p04_1 (p2a_i[1],   port2_in_A[1], s4a,    s4b,   tck, shift, ten);
   pad p04_2 (p2a_i[2],   port2_in_A[2], s4b,    s4c,   tck, shift, ten);
   pad p04_3 (p2a_i[3],   port2_in_A[3], s4c,    s4d,   tck, shift, ten);
   pad p04_4 (p2a_i[4],   port2_in_A[4], s4d,    s4e,   tck, shift, ten);
   pad p04_5 (p2a_i[5],   port2_in_A[5], s4e,    s4f,   tck, shift, ten);
   pad p04_6 (p2a_i[6],   port2_in_A[6], s4f,    s4g,   tck, shift, ten);
   pad p04_7 (p2a_i[7],   port2_in_A[7], s4g,    s4,    tck, shift, ten);

//   pad p05 (p3a_i,   port3_in_A, s4,          s5,   tck, shift, ten);
   pad p05_0 (p3a_i[0],   port3_in_A[0], s4,     s5a,   tck, shift, ten);
   pad p05_1 (p3a_i[1],   port3_in_A[1], s5a,    s5b,   tck, shift, ten);
   pad p05_2 (p3a_i[2],   port3_in_A[2], s5b,    s5c,   tck, shift, ten);
   pad p05_3 (p3a_i[3],   port3_in_A[3], s5c,    s5d,   tck, shift, ten);
   pad p05_4 (p3a_i[4],   port3_in_A[4], s5d,    s5e,   tck, shift, ten);
   pad p05_5 (p3a_i[5],   port3_in_A[5], s5e,    s5f,   tck, shift, ten);
   pad p05_6 (p3a_i[6],   port3_in_A[6], s5f,    s5g,   tck, shift, ten);
   pad p05_7 (p3a_i[7],   port3_in_A[7], s5g,    s5,    tck, shift, ten);


   pad p06 (rxda_i,  rx_A,	    s5,          s6,   tck, shift, ten);
   pad p07 (t0a_i,   t0_A,	    s6,          s7,   tck, shift, ten);
   pad p08 (t1a_i,   t1_A,       s7,          s8,   tck, shift, ten);
   pad p09 (t2a_i,   t2_A,	    s8,          s9,   tck, shift, ten);
   pad p10 (t2exa_i, t2ex_A,     s9,          s10,  tck, shift, ten);
   
   
   // From Proc A
   pad p11_0 (port0_out_A[0], p0a_o[0],   s10,    s11a,    tck, shift, ten);
   pad p11_1 (port0_out_A[1], p0a_o[1],   s11a,   s11b,    tck, shift, ten);
   pad p11_2 (port0_out_A[2], p0a_o[2],   s11b,   s11c,    tck, shift, ten);
   pad p11_3 (port0_out_A[3], p0a_o[3],   s11c,   s11d,    tck, shift, ten);
   pad p11_4 (port0_out_A[4], p0a_o[4],   s11d,   s11e,    tck, shift, ten);
   pad p11_5 (port0_out_A[5], p0a_o[5],   s11e,   s11f,    tck, shift, ten);
   pad p11_6 (port0_out_A[6], p0a_o[6],   s11f,   s11g,    tck, shift, ten);
   pad p11_7 (port0_out_A[7], p0a_o[7],   s11g,   s11,     tck, shift, ten);


   pad p12_0 (port1_out_A[0], p1a_o[0],   s11,    s12a,    tck, shift, ten);
   pad p12_1 (port1_out_A[1], p1a_o[1],   s12a,   s12b,    tck, shift, ten);
   pad p12_2 (port1_out_A[2], p1a_o[2],   s12b,   s12c,    tck, shift, ten);
   pad p12_3 (port1_out_A[3], p1a_o[3],   s12c,   s12d,    tck, shift, ten);
   pad p12_4 (port1_out_A[4], p1a_o[4],   s12d,   s12e,    tck, shift, ten);
   pad p12_5 (port1_out_A[5], p1a_o[5],   s12e,   s12f,    tck, shift, ten);
   pad p12_6 (port1_out_A[6], p1a_o[6],   s12f,   s12g,    tck, shift, ten);
   pad p12_7 (port1_out_A[7], p1a_o[7],   s12g,   s12,     tck, shift, ten);


   pad p13_0 (port2_out_A[0], p2a_o[0],   s12,    s13a,    tck, shift, ten);   
   pad p13_1 (port2_out_A[1], p2a_o[1],   s13a,   s13b,    tck, shift, ten);   
   pad p13_2 (port2_out_A[2], p2a_o[2],   s13b,   s13c,    tck, shift, ten);   
   pad p13_3 (port2_out_A[3], p2a_o[3],   s13c,   s13d,    tck, shift, ten);   
   pad p13_4 (port2_out_A[4], p2a_o[4],   s13d,   s13e,    tck, shift, ten);   
   pad p13_5 (port2_out_A[5], p2a_o[5],   s13e,   s13f,    tck, shift, ten);   
   pad p13_6 (port2_out_A[6], p2a_o[6],   s13f,   s13g,    tck, shift, ten);   
   pad p13_7 (port2_out_A[7], p2a_o[7],   s13g,   s13,    tck, shift, ten);   

   pad p14_0 (port3_out_A[0], p3a_o[0],   s13,    s14a,    tck, shift, ten);   
   pad p14_1 (port3_out_A[1], p3a_o[1],   s14a,   s14b,    tck, shift, ten);   
   pad p14_2 (port3_out_A[2], p3a_o[2],   s14b,   s14c,    tck, shift, ten);   
   pad p14_3 (port3_out_A[3], p3a_o[3],   s14c,   s14d,    tck, shift, ten);   
   pad p14_4 (port3_out_A[4], p3a_o[4],   s14d,   s14e,    tck, shift, ten);   
   pad p14_5 (port3_out_A[5], p3a_o[5],   s14e,   s14f,    tck, shift, ten);   
   pad p14_6 (port3_out_A[6], p3a_o[6],   s14f,   s14g,    tck, shift, ten);   
   pad p14_7 (port3_out_A[7], p3a_o[7],   s14g,   s14,    tck, shift, ten);   

   pad p15 (tx_A,        txda_o,  s14,   s15,    tck, shift, ten);
   


   // To Proc B
   clkBuf crstbbuf (c_rst_B, core_rst_i);
   clkBuf mrstbbuf (m_rst_B, mem_rst_i);
   clkBuf cclkbbuf ( c_ck_B, core_clk_i);   
   clkBuf mclkbbuf ( m_ck_B, mem_clk_i);
   
   pad p16 (int0b_i, int0_B,     s14,        s15,   tck, shift, ten);
   pad p17 (int0b_i, int1_B,	    s15,        s16,   tck, shift, ten);
   
//   pad p18 (p0b_i,   port0_in_B, s16,          s17,   tck, shift, ten);
   pad p18_0 (p0b_i[0],   port0_in_B[0], s16,     s17a,   tck, shift, ten);
   pad p18_1 (p0b_i[1],   port0_in_B[1], s17a,    s17b,   tck, shift, ten);
   pad p18_2 (p0b_i[2],   port0_in_B[2], s17b,    s17c,   tck, shift, ten);
   pad p18_3 (p0b_i[3],   port0_in_B[3], s17c,    s17d,   tck, shift, ten);
   pad p18_4 (p0b_i[4],   port0_in_B[4], s17d,    s17e,   tck, shift, ten);
   pad p18_5 (p0b_i[5],   port0_in_B[5], s17e,    s17f,   tck, shift, ten);
   pad p18_6 (p0b_i[6],   port0_in_B[6], s17f,    s17g,   tck, shift, ten);
   pad p18_7 (p0b_i[7],   port0_in_B[7], s17g,    s17,    tck, shift, ten);
   
//   pad p03 (p1b_i,   port1_in_B, s17,          s18,   tck, shift, ten);
   pad p19_0 (p1b_i[0],   port1_in_B[0], s17,    s18a,   tck, shift, ten);
   pad p19_1 (p1b_i[1],   port1_in_B[1], s18a,   s18b,   tck, shift, ten);
   pad p19_2 (p1b_i[2],   port1_in_B[2], s18b,   s18c,   tck, shift, ten);
   pad p19_3 (p1b_i[3],   port1_in_B[3], s18c,   s18d,   tck, shift, ten);
   pad p19_4 (p1b_i[4],   port1_in_B[4], s18d,   s18e,   tck, shift, ten);
   pad p19_5 (p1b_i[5],   port1_in_B[5], s18e,   s18f,   tck, shift, ten);
   pad p19_6 (p1b_i[6],   port1_in_B[6], s18f,   s18g,   tck, shift, ten);
   pad p19_7 (p1b_i[7],   port1_in_B[7], s18g,   s18,    tck, shift, ten);

//   pad p04 (p2b_i,   port2_in_B, s18,          s19,   tck, shift, ten);
   pad p20_0 (p2b_i[0],   port2_in_B[0], s18,     s19a,   tck, shift, ten);
   pad p20_1 (p2b_i[1],   port2_in_B[1], s19a,    s19b,   tck, shift, ten);
   pad p20_2 (p2b_i[2],   port2_in_B[2], s19b,    s19c,   tck, shift, ten);
   pad p20_3 (p2b_i[3],   port2_in_B[3], s19c,    s19d,   tck, shift, ten);
   pad p20_4 (p2b_i[4],   port2_in_B[4], s19d,    s19e,   tck, shift, ten);
   pad p20_5 (p2b_i[5],   port2_in_B[5], s19e,    s19f,   tck, shift, ten);
   pad p20_6 (p2b_i[6],   port2_in_B[6], s19f,    s19g,   tck, shift, ten);
   pad p20_7 (p2b_i[7],   port2_in_B[7], s19g,    s19,    tck, shift, ten);

//   pad p05 (p3a_i,   port3_in_A, s19,          s20,   tck, shift, ten);
   pad p21_0 (p3b_i[0],   port3_in_B[0], s19,     s20a,   tck, shift, ten);
   pad p21_1 (p3b_i[1],   port3_in_B[1], s20a,    s20b,   tck, shift, ten);
   pad p21_2 (p3b_i[2],   port3_in_B[2], s20b,    s20c,   tck, shift, ten);
   pad p21_3 (p3b_i[3],   port3_in_B[3], s20c,    s20d,   tck, shift, ten);
   pad p21_4 (p3b_i[4],   port3_in_B[4], s20d,    s20e,   tck, shift, ten);
   pad p21_5 (p3b_i[5],   port3_in_B[5], s20e,    s20f,   tck, shift, ten);
   pad p21_6 (p3b_i[6],   port3_in_B[6], s20f,    s20g,   tck, shift, ten);
   pad p21_7 (p3b_i[7],   port3_in_B[7], s20g,    s20,    tck, shift, ten);

   pad p22 (rxdb_i,  rx_B,	    s20,        s21,   tck, shift, ten);
   pad p23 (t0b_i,   t0_B,	    s21,        s22,   tck, shift, ten);
   pad p24 (t1b_i,   t1_B,       s22,        s23,   tck, shift, ten);
   pad p25 (t2b_i,   t2_B,	    s23,        s24,   tck, shift, ten);
   pad p26 (t2exb_i, t2ex_B,     s24,        s25,  tck, shift, ten);

   // From Proc B
   // From Proc A

//   pad p27 (port0_out_B, p0b_o,   s25,   s26,     tck, shift, ten);
   pad p27_0 (port0_out_B[0], p0b_o[0],   s25,    s26a,    tck, shift, ten);
   pad p27_1 (port0_out_B[1], p0b_o[1],   s26a,   s26b,    tck, shift, ten);
   pad p27_2 (port0_out_B[2], p0b_o[2],   s26b,   s26c,    tck, shift, ten);
   pad p27_3 (port0_out_B[3], p0b_o[3],   s26c,   s26d,    tck, shift, ten);
   pad p27_4 (port0_out_B[4], p0b_o[4],   s26d,   s26e,    tck, shift, ten);
   pad p27_5 (port0_out_B[5], p0b_o[5],   s26e,   s26f,    tck, shift, ten);
   pad p27_6 (port0_out_B[6], p0b_o[6],   s26f,   s26g,    tck, shift, ten);
   pad p27_7 (port0_out_B[7], p0b_o[7],   s26g,   s26,     tck, shift, ten);

//   pad p28 (port1_out_B, p1b_o,   s26,   s27,     tck, shift, ten);
   pad p28_0 (port1_out_B[0], p1b_o[0],   s26,    s27a,    tck, shift, ten);
   pad p28_1 (port1_out_B[1], p1b_o[1],   s27a,   s27b,    tck, shift, ten);
   pad p28_2 (port1_out_B[2], p1b_o[2],   s27b,   s27c,    tck, shift, ten);
   pad p28_3 (port1_out_B[3], p1b_o[3],   s27c,   s27d,    tck, shift, ten);
   pad p28_4 (port1_out_B[4], p1b_o[4],   s27d,   s27e,    tck, shift, ten);
   pad p28_5 (port1_out_B[5], p1b_o[5],   s27e,   s27f,    tck, shift, ten);
   pad p28_6 (port1_out_B[6], p1b_o[6],   s27f,   s27g,    tck, shift, ten);
   pad p28_7 (port1_out_B[7], p1b_o[7],   s27g,   s27,     tck, shift, ten);

//   pad p29 (port2_out_B, p2b_o,   s27,   s28,     tck, shift, ten);   
   pad p29_0 (port2_out_B[0], p2b_o[0],   s27,    s28a,    tck, shift, ten);   
   pad p29_1 (port2_out_B[1], p2b_o[1],   s28a,   s28b,    tck, shift, ten);   
   pad p29_2 (port2_out_B[2], p2b_o[2],   s28b,   s28c,    tck, shift, ten);   
   pad p29_3 (port2_out_B[3], p2b_o[3],   s28c,   s28d,    tck, shift, ten);   
   pad p29_4 (port2_out_B[4], p2b_o[4],   s28d,   s28e,    tck, shift, ten);   
   pad p29_5 (port2_out_B[5], p2b_o[5],   s28e,   s28f,    tck, shift, ten);   
   pad p29_6 (port2_out_B[6], p2b_o[6],   s28f,   s28g,    tck, shift, ten);   
   pad p29_7 (port2_out_B[7], p2b_o[7],   s28g,   s28,    tck, shift, ten);   

//   pad p30 (port3_out_B, p3b_o,   s28,   s29,     tck, shift, ten);
   pad p30_0 (port3_out_B[0], p3b_o[0],   s28,    s29a,    tck, shift, ten);   
   pad p30_1 (port3_out_B[1], p3b_o[1],   s29a,   s29b,    tck, shift, ten);   
   pad p30_2 (port3_out_B[2], p3b_o[2],   s29b,   s29c,    tck, shift, ten);   
   pad p30_3 (port3_out_B[3], p3b_o[3],   s29c,   s29d,    tck, shift, ten);   
   pad p30_4 (port3_out_B[4], p3b_o[4],   s29d,   s29e,    tck, shift, ten);   
   pad p30_5 (port3_out_B[5], p3b_o[5],   s29e,   s29f,    tck, shift, ten);   
   pad p30_6 (port3_out_B[6], p3b_o[6],   s29f,   s29g,    tck, shift, ten);   
   pad p30_7 (port3_out_B[7], p3b_o[7],   s29g,   s29,    tck, shift, ten);   

   pad p31 (tx_B,        txdb_o,  s29,   scanout, tck, shift, ten);


endmodule // dualCorePads
