//
// Behavioral clock buffer circuit
//
module clkBuf(z,a);
   input a;
   output z;

   assign z = a + 0;
endmodule // clkBuf
