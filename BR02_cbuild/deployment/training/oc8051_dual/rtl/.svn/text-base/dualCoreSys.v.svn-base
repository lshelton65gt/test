// - * - Verilog - * -
//
// Dual Core 8051 System
//
// Instantiate PAD Ring and 2 subsytems.  Each Subsystem conatins an 8051 and the icache
// and dcache
//

`include "oc8051_timescale.v"

module dualCoreSys(core_rst_i, mem_rst_i, core_clk_i, mem_clk_i, 
		   int0a_i, int1a_i, int0b_i, int1b_i,
		   p0a_i, p0a_o, p0b_i, p0b_o,
		   p1a_i, p1a_o, p1b_i, p1b_o,
		   p2a_i, p2a_o, p2b_i, p2b_o,
		   p3a_i, p3a_o, p3b_i, p3b_o,
		   rxda_i, txda_o,  rxdb_i, txdb_o,
		   t0a_i, t1a_i, t0b_i, t1b_i,
		   t2a_i, t2exa_i, t2b_i, t2exb_i,
		   scanin, scanout, tck, test_en, load_scan
		   );

   input scanin, tck, test_en, load_scan;
   output scanout;

   input   core_rst_i, mem_rst_i, core_clk_i, mem_clk_i;
   input   int0a_i, int1a_i;
   input   int0b_i, int1b_i;
   

   // --- Port Interface ---
   input   [7:0] p0a_i, p0b_i;
   input   [7:0] p1a_i, p1b_i;
   input   [7:0] p2a_i, p2b_i;   
   input   [7:0] p3a_i, p3b_i;

   output  [7:0] p1a_o, p1b_o;
   output  [7:0] p0a_o, p0b_o;
   output  [7:0] p2a_o, p2b_o;   
   output  [7:0] p3a_o, p3b_o;
 
   input   rxda_i, rxdb_i;
   output  txda_o, txdb_o;

   input   t0a_i, t1a_i;
   input   t0b_i, t1b_i;

   input   t2a_i, t2exa_i;   
   input   t2b_i, t2exb_i;   

   wire [7:0] port0_in_A, port0_out_A, port0_in_B, port0_out_B;
   wire [7:0] port1_in_A, port1_out_A, port1_in_B, port1_out_B;
   wire [7:0] port2_in_A, port2_out_A, port2_in_B, port2_out_B;
   wire [7:0] port3_in_A, port3_out_A, port3_in_B, port3_out_B;
   

   oc8051_subsys procA (.core_rst_i  (c_rst_A), 
			.mem_rst_i   (m_rst_A), 
			.core_clk_i  (c_ck_A), 
			.mem_clk_i   (m_ck_A), 
			.int0_i      (int0_A), 
			.int1_i      (int1_A),
			.p0_i        (port0_in_A), 
			.p0_o        (port0_out_A),
			.p1_i        (port1_in_A), 
			.p1_o        (port1_out_A),
			.p2_i        (port2_in_A),
			.p2_o        (port2_out_A),   
			.p3_i        (port3_in_A), 
			.p3_o        (port3_out_A),
			.rxd_i       (rx_A), 
			.txd_o       (tx_A),
			.t0_i        (t0_A), 
			.t1_i        (t1_A),
			.t2_i        (t2_A), 
			.t2ex_i      (t2ex_A)
			);

   oc8051_subsys procB (.core_rst_i  (c_rst_B), 
			.mem_rst_i   (m_rst_B), 
			.core_clk_i  (c_ck_B), 
			.mem_clk_i   (m_ck_B), 
			.int0_i      (int0_B), 
			.int1_i      (int1_B),
			.p0_i        (port0_in_B), 
			.p0_o        (port0_out_B),
			.p1_i        (port1_in_B), 
			.p1_o        (port1_out_B),
			.p2_i        (port2_in_B),
			.p2_o        (port2_out_B),   
			.p3_i        (port3_in_B), 
			.p3_o        (port3_out_B),
			.rxd_i       (rx_B), 
			.txd_o       (tx_B),
			.t0_i        (t0_B), 
			.t1_i        (t1_B),
			.t2_i        (t2_B), 
			.t2ex_i      (t2ex_B)
			);

   dualCorePads pads (
		      // test
		      .scanin      (scanin),
		      .scanout     (scanout),
		      .ten         (test_en),
		      .shift       (load_scan),
		      .tck         (tck),
		      // Primary Input Pins
		      .core_rst_i  (core_rst_i),   
		      .mem_rst_i   (mem_rst_i),    
		      .core_clk_i  (core_clk_i),   
		      .mem_clk_i   (mem_clk_i),    
		      .int0a_i     (int0a_i), 	    
		      .int1a_i     (int1a_i),	    
		      .int0b_i     (int0b_i), 	    
		      .int1b_i     (int1b_i),	    
		      .p0a_i       (p0a_i),	    
		      .p0b_i       (p0b_i),	    
		      .p1a_i       (p1a_i), 	    
		      .p1b_i       (p1b_i), 	    
		      .p2a_i       (p2a_i), 	    
		      .p2b_i       (p2b_i), 	    
		      .p3a_i       (p3a_i), 	    
		      .p3b_i       (p3b_i), 	    
		      .rxda_i      (rxda_i), 	    
		      .rxdb_i      (rxdb_i), 	    
		      .t0a_i       (t0a_i), 	    
		      .t1a_i       (t1a_i), 	    
		      .t0b_i       (t0b_i), 	    
		      .t1b_i       (t1b_i),	      
		      .t2a_i       (t2a_i), 	    
		      .t2exa_i     (t2exa_i), 	    
		      .t2b_i 	   (t2b_i), 	    
		      .t2exb_i	   (t2exb_i),

		      // Primary Outputs
		      .p0a_o       (p0a_o),	    
		      .p0b_o       (p0b_o),	      
		      .p1a_o       (p1a_o), 	    
		      .p1b_o       (p1b_o),	      
		      .p2a_o       (p2a_o), 	    
		      .p2b_o       (p2b_o),	      
		      .p3a_o       (p3a_o), 	    
		      .p3b_o       (p3b_o),	      
		      .txda_o      (txda_o),  	    
		      .txdb_o      (txdb_o),	      

		      // To Proc A
                      .c_rst_A      (c_rst_A),     
		      .m_rst_A      (m_rst_A),     
		      .c_ck_A       (c_ck_A),      
		      .m_ck_A       (m_ck_A),      
		      .int0_A       (int0_A),      
		      .int1_A       (int1_A),      
		      .port0_in_A   (port0_in_A),  
		      .port1_in_A   (port1_in_A),  
		      .port2_in_A   (port2_in_A),  
		      .port3_in_A   (port3_in_A),  
		      .rx_A         (rx_A),        
		      .t0_A         (t0_A),        
		      .t1_A         (t1_A),	       
		      .t2_A         (t2_A),        
		      .t2ex_A       (t2ex_A),
		      
		      // From Proc A
		      .port0_out_A   (port0_out_A),
		      .port1_out_A   (port1_out_A),
		      .port2_out_A   (port2_out_A),   
		      .port3_out_A   (port3_out_A),
		      .tx_A          (tx_A),      

		      // To Proc B
                      .c_rst_B      (c_rst_B),     
		      .m_rst_B      (m_rst_B),     
		      .c_ck_B       (c_ck_B),      
		      .m_ck_B       (m_ck_B),      
		      .int0_B       (int0_B),      
		      .int1_B       (int1_B),      
		      .port0_in_B   (port0_in_B),  
		      .port1_in_B   (port1_in_B),  
		      .port2_in_B   (port2_in_B),  
		      .port3_in_B   (port3_in_B),  
		      .rx_B         (rx_B),        
		      .t0_B         (t0_B),        
		      .t1_B         (t1_B),	       
		      .t2_B         (t2_B),        
		      .t2ex_B       (t2ex_B),
		      
		      // From Proc B
		      .port0_out_B   (port0_out_B),
		      .port1_out_B   (port1_out_B),
		      .port2_out_B   (port2_out_B),   
		      .port3_out_B   (port3_out_B),
		      .tx_B          (tx_B)
		      );
   
   
endmodule // dualCoreSys
