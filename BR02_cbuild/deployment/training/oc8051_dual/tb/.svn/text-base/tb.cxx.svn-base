/*
** The DualCore oc8051 platform has a the following interface
**		   
*/

#include <iostream> 
#include <string>
#include <cstdlib>

#include "libdualCore.h"
#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimStep.h"
#include "carbonsim/CarbonSimClock.h"
#include "carbonsim/CarbonSimMemory.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimObjectInstance.h"

/*
** This testbench is inteded to work with the portCalc software
** it montiors the p0 output waiting for it to be 0 (ports go to
** 0xff in reset).  Once it is 0, we will send the next data to 
** be calculated to the input ports. 
**
** The output of the operation is written to the external ram.
** this tb will track what it expects the ram to be, then
** compare at the end.
**
** --- BUG ---
** Something is wrong with this TB.  It seems to me that the 
** test should be creating the same stream of random numbers for
** a given seed, but in actuality it's not.  I'm not sure why but I'd
** guess that either my callbacks aren't getting called deterministicly
** from run to run, or something else is calling rand and I'm overlooking
** it.
**
*/


const int MemorySize = 257;
/*
** Some simple data structures follow 
*/

// configuration info from command line
struct Config {
    std::string* icacheAFile;
    std::string* icacheBFile;
    std::string* dcacheAFile;
    std::string* dcacheBFile;

    int randSeed;
    Config() : icacheAFile(0), icacheBFile(0), dcacheAFile(0), dcacheBFile(0),
	randSeed(0) {}
};

// Container for objects that the port 0 callback will need
struct PortCBData {
    CarbonSimNet* p0in;
    CarbonSimNet* p1in;
    CarbonSimNet* p2in;
    CarbonSimNet* p3in;
 
    // because the address is specifed on the ports (p3) which
    // is only 8 bits, we know that we only need to track 257 memory
    // locations.  It's 257 because if we use the address 255, then 
    // the addr 256 will also be written
    UInt32 mem[MemorySize];
    UInt32 pendResult, pendAddr;
   
    PortCBData() : p0in(0), p1in(0), p2in(0), p3in(0),
	pendResult(0), pendAddr(0) {}
};


/*
** Function declarations
*/

// utilities
bool bindFunc(CarbonSimMaster* m, CarbonSimFunctionGen* f, const char* netName);
Config* processArgs(int, char*[]);
void dumpMem(CarbonSimMemory* mem);

// 
void checkMem(PortCBData* d, CarbonSimMemory* m);

// callbacks
void port0CB(CarbonObjectID*, CarbonNetID*, CarbonClientData, UInt32*, UInt32*);


/*
**
** MAIN
**
*/
int main(int argc, char* argv[])
{
    Config* cfg = processArgs(argc, argv);
    srand(cfg->randSeed);

//    CarbonSimMaster* master = createEnv();
    /*
    ** The DesignPlayer for the System
    */
    CarbonSimMaster *master = CarbonSimMaster::create();

    CarbonSimObjectType *sysType = master->addObjectType("dualCoreSys", carbon_dualCore_create);
    CarbonSimObjectInstance *sys = master->addInstance(sysType, "system", eCarbonFullDB, eCarbon_NoFlags);    

    /*
    ** Clocks and Resets
    */
    CarbonSimClock* clock = new CarbonSimClock(20,10);
    bindFunc(master, clock, "system.core_clk_i" );
    bindFunc(master, clock, "system.mem_clk_i" );
    master->addClock(clock);

    CarbonSimStep* reset = new CarbonSimStep(40,1);
    bindFunc(master, reset, "system.core_rst_i");
    bindFunc(master, reset, "system.mem_rst_i");
    master->addStep(reset);

    /*
    ** Waves
    */
    CarbonWaveID* wave = carbonWaveInitFSDB(sys->getCarbonObject(), "out.fsdb", e1ns);
    carbonDumpVars(wave, 0, "dualCoreSys");

    /*
    ** get some handles
    */

    /// memories 
    CarbonSimMemory* icacheA = master->findMemory("system.procA.icache.mem");
    CarbonSimMemory* icacheB = master->findMemory("system.procB.icache.mem");

    /// ports
    CarbonSimNet* port0ai = master->findNet("system.p0a_i");
    CarbonSimNet* port1ai = master->findNet("system.p1a_i");
    CarbonSimNet* port2ai = master->findNet("system.p2a_i");
    CarbonSimNet* port3ai = master->findNet("system.p3a_i");
    //
    CarbonSimNet* port0bi = master->findNet("system.p0b_i");
    CarbonSimNet* port1bi = master->findNet("system.p1b_i");
    CarbonSimNet* port2bi = master->findNet("system.p2b_i");
    CarbonSimNet* port3bi = master->findNet("system.p3b_i");
    //
    CarbonSimNet* port0ao = master->findNet("system.p0a_o");
    CarbonSimNet* port1ao = master->findNet("system.p1a_o");
    CarbonSimNet* port2ao = master->findNet("system.p2a_o");
    CarbonSimNet* port3ao = master->findNet("system.p3a_o");
    //	 
    CarbonSimNet* port0bo = master->findNet("system.p0b_o");
    CarbonSimNet* port1bo = master->findNet("system.p1b_o");
    CarbonSimNet* port2bo = master->findNet("system.p2b_o");
    CarbonSimNet* port3bo = master->findNet("system.p3b_o");
    
    // Add a callback to port 0
    PortCBData* portCBDataA = new PortCBData;
    portCBDataA->p0in = port0ai;
    portCBDataA->p1in = port1ai;
    portCBDataA->p2in = port2ai;
    portCBDataA->p3in = port3ai;
    memset(portCBDataA->mem, 0, MemorySize * sizeof(UInt32) );

    CarbonNetID* p0a = carbonFindNet(sys->getCarbonObject(), "dualCoreSys.p0a_o");    
    carbonAddNetValueChangeCB(sys->getCarbonObject(), port0CB, 
			      (CarbonClientData)portCBDataA,  p0a);

    // Add a callback to port 0, processor B
    PortCBData* portCBDataB = new PortCBData;
    portCBDataB->p0in = port0bi;
    portCBDataB->p1in = port1bi;
    portCBDataB->p2in = port2bi;
    portCBDataB->p3in = port3bi;
    memset(portCBDataB->mem, 0, MemorySize * sizeof (UInt32) );

    CarbonNetID* p0b = carbonFindNet(sys->getCarbonObject(), "dualCoreSys.p0b_o");    
    carbonAddNetValueChangeCB(sys->getCarbonObject(), port0CB, 
			      (CarbonClientData)portCBDataB,  p0b);


    /*
    ** Load the icaches    
    */
    if (cfg->icacheAFile) {
	icacheA->readmemh(cfg->icacheAFile->c_str() );	
    }
    
    if (cfg->icacheBFile) {
	icacheB->readmemh(cfg->icacheBFile->c_str() );	
    }



    /*
    ** Execute
    */
    master->run(20 * 100000);

    /*
    ** Dump dmem contents
    */
    UInt32 buf[1] = {0};
    CarbonSimMemory* dcacheA = master->findMemory("system.procA.dcache.mem");
    CarbonSimMemory* dcacheB = master->findMemory("system.procB.dcache.mem");

//    dumpMem(dcacheA);
    checkMem(portCBDataA, dcacheA);
    checkMem(portCBDataB, dcacheB);

    delete master;
    delete cfg;
}

Config* processArgs(int argc, char*argv[])
{
    Config* cfg = new Config;

    /* start at 1 to ignore executable name */
    for(int i = 1; i < argc; ++i) {
	std::string arg(argv[i]);

	if(arg == "-iA" and ++i < argc) {
	    cfg->icacheAFile = new std::string(argv[i]);
	} else if(arg == "-iB" and ++i < argc) {
	    cfg->icacheBFile = new std::string(argv[i]);
	} else if (arg == "-seed" and ++i < argc) {
	    cfg->randSeed = atoi(argv[i]);
	    std::cout << "SEED: 0x" << std::hex << cfg->randSeed << std::endl;
			
	} else {
	    /*
	    ** Default case, argument was not understood
	    */
	    std::cout << " Did not understand argument " << argv[i] << std::endl;
	}
    }

    return cfg;

}

/*
** Helper for finding a net and binding it to a function generator (clock or reset)
*/
bool bindFunc(CarbonSimMaster* m, CarbonSimFunctionGen* f, const char* netName)
{
    CarbonSimNet* n = m->findNet(netName);

    if (not n) {
	std::cerr << "Could not find net " << netName << std::endl;
	return false;
    }

    if( not f->bindSignal(n) ) {
	std::cerr << "WTF?!" << std::endl;
    }

    return true;
}


void dumpMem(CarbonSimMemory* mem)
{
    UInt32 buf[1];
    for(int i = 0; i < 257; ++i) {
	buf[0] = 0;
	mem->examine(i, buf);
	std::cout << std::dec << i << ": 0x" << std::hex << buf[0] << std::endl;
    }
}

//
// This routine is the brains of the tb
//
void applyStimulus(PortCBData*d)
{
    // we need to come up with:
    //  - new operands (applied to p1 & p2)
    //  - new operation (applied to p0)
    //  - new address (applied to p3)
    //
    // we'll then save the expected result in a memory that
    // we can check against what the oc8051 did


    //
    // We won't save the results until the next call
    // this is to try and avoid cases where we stop the
    // sim before the mem was written and thus we miscompare
    //
    // This hack still lets that happen, but the window of 
    // opportunity is much shorter.
    //
    // Save the previous expected results
    d->mem[d->pendAddr]   = d->pendResult & 0xFF;
    d->mem[d->pendAddr+1] = (d->pendResult & 0xFF00) >> 8;


    UInt32 a, b, op, addr, result;

    a    = rand() & 0xFF;
    b    = rand() & 0xFF;
    op   = rand() & 0x1;

    // I've decided to ignore the corner case of addr = 0xFF
    do {
	addr = rand() & 0xFF;
    } while (addr == 0xFF);

    if(op) {
	// mult
	result = a * b;
    } else {
	// add
	result = a + b;
    }
    
    d->pendAddr = addr;
    d->pendResult = result;

    // deposit the values
    d->p0in->deposit(&op);
    d->p1in->deposit(&a);
    d->p2in->deposit(&b);
    d->p3in->deposit(&addr);
}

//
// This function just turns around and calls the pattern generation routine applyStimulus
// 
void port0CB(CarbonObjectID*o, CarbonNetID*n, CarbonClientData data, UInt32* val, UInt32*)
{
    PortCBData* d = (PortCBData*) data;

    if (*val == 0) {
	applyStimulus(d);
	std::cout << "Applied new Stimulus" << std::endl;
    } else if (*val == 1) {
	std::cout << "Waiting for result..." << std::endl;
    } else {
	char buf[128];
	carbonGetNetName(o,n, buf, 128);
	std::cerr << "ERROR- unexpect " << buf << "  value: 0x" 
		  << std::hex << *val << std::endl;
    }

}


//
// Check the contents of the data memory vs expect values
//
void checkMem(PortCBData* d, CarbonSimMemory* m)
{
    UInt32* gold = d->mem;
    
    UInt32 buf;

    std::cout << "Checking Memory: " << m->getName() << std::endl;

    for (int i = 0; i < MemorySize; ++i) {
	m->examine(i, &buf);
	if (buf != gold[i]) {
	    std::cerr << "ERROR: MISMATCHED RESULTS addr = 0x" << std::hex << i
		      << " expected 0x" << gold[i] << " Actual 0x" << buf << std::endl;
	} else {
//	    std::cerr << "Matched results addr = 0x" << std::hex << i
//		      << " expected 0x" << gold[i] << " Actual 0x" << buf << std::endl;
	}	    

    }
}
