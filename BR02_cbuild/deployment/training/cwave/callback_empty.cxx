#include "libdesign.h"
#include <cassert>
#include <iostream>
#include <cstdio>


extern void userInitFn(CarbonModel * model, void ** client_data)
{
}

extern void userDestroyFn(CarbonModel * model, void * client_data)
{
}

extern void userPreScheduleFn(CarbonModel * model , void *  client_data, CarbonTime time)
{
}


extern void userPostScheduleFn(CarbonModel* model, void* client_data, CarbonTime time)
{
}

