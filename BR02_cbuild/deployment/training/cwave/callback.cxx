#include "libdesign.h"
#include <cassert>
#include <iostream>
#include <cstdio>

void callForceWord(CarbonModel*, CarbonNetID*, uint32, CarbonTime);
void callDepositWord(CarbonModel*, CarbonNetID*, uint32, CarbonTime);
void callDeposit(CarbonModel*, CarbonNetID*, uint32*, CarbonTime);

#if 1
extern void userInitFn(CarbonModel * model, void ** client_data)
{
    CarbonNetID ** net_array = new CarbonNetID * [3];

    net_array[0] = carbonFindNet(model, "oc8051_top.oc8051_sfr1.oc8051_uatr1.tx_done");
    assert(net_array[0]);
    net_array[1] = carbonFindNet(model, "oc8051_top.oc8051_sfr1.oc8051_uatr1.t1_ow_buf");
    assert(net_array[1]);
    net_array[2] = carbonFindNet(model, "oc8051_top.oc8051_sfr1.oc8051_uatr1.sbuf_txd");
    assert(net_array[2]);

    *client_data = net_array;
}
#else
extern void userInitFn(CarbonModel *, void **) { }
#endif

#if 1
extern void userDestroyFn(CarbonModel * model, void * client_data)
{
  //  CarbonTime time = model->getSimulationTime();
  // printf("Simulation Ending at time %d\n", (int) time);
  CarbonNet ** net_array = static_cast<CarbonNet **>(client_data);
  delete [] net_array;
}
#else
extern void userDestroyFn(CarbonModel *, void *) { }
#endif



#if 1
extern void userPreScheduleFn(CarbonModel * model , void *  client_data, CarbonTime time)
{
    CarbonNetID ** net_array = static_cast<CarbonNetID **>(client_data);

    if (time == 0)
      {
	callForceWord(model, net_array[0], 0x1, time);
      }
    else if (time == 270)
      {
	callForceWord(model, net_array[1], 0x0, time);
	callForceWord(model, net_array[2], 0x5a5, time);
      }
    else if (time == 330)
      carbonRelease(model, net_array[1]);
}
#else
extern void userPreScheduleFn(CarbonModel *, void *, CarbonTime) { }
#endif



#if 0
extern void userPostScheduleFn(CarbonModel* model, void* client_data, CarbonTime time)
{
    CarbonNet ** net_array = static_cast<CarbonNet **>(client_data);
    
    if (time == 0)
    {
      model->depositWord(net_array[0], 0x1, 0);
    }
}
#else
extern void userPostScheduleFn(CarbonModel*, void*, CarbonTime) { }
#endif


void callForceWord(CarbonModel *model, CarbonNetID *net, uint32 val, CarbonTime time)
{
#if DEBUG
    char name[200];
    carbonGetNetName(model, net, name, 200);
    printf("Forcing signal at time %d;  %s = 0x%x\n", (int)time, name, val);
#endif
    if(carbonForceWord(model, net, val, 0) != eCarbon_OK) {
	char name[200];
	carbonGetNetName(model, net, name, 200);
	printf("ERROR: problem forcing %s at time %d\n", name, (int)time);
    }
    return;
}

void callDepositWord(CarbonModel *model, CarbonNetID *net, uint32 val, CarbonTime time)
{
    char name[200];
    carbonGetNetName(model, net, name, 200);

#if DEBUG
    printf("Depositing signal at time %d;  %s = 0x%x\n", (int)time, name, val);
#endif
    if(carbonDepositWord(model, net, val, 0, 0) != eCarbon_OK)
	printf("ERROR: problem depositing %s at time %d\n", name, (int)time);
    return;
}

void callDeposit(CarbonModel *model, CarbonNetID *net, uint32 *val, CarbonTime time)
{
    char name[200];
    carbonGetNetName(model, net, name, 200);

#if DEBUG
    printf("Depositing signal at time %d;  %s = 0x{?}_%x\n", (int)time, name, val[0]);
#endif
    if(carbonDeposit(model, net, val, 0) != eCarbon_OK)
	printf("ERROR: problem depositing %s at time %d \n", name, (int)time);
    return;
}
