/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// repeat condition having part-select 
// Section 5.12.7 of the test plan
module REPEAT7(clk,cond,index,in1,out1,out2);
input clk,in1;
input [3:0] cond;
input [1:0] index;
output [31:0] out1,out2;
reg [31:0] out1,out2;
reg [31:0] temp;

  always
  begin
    @(posedge clk);
        temp=0;
                out1=0;
                out2=0;
    repeat(cond[1:0]==2'b11)
                begin
     @(posedge clk);
     temp = temp + in1;
     out1 = temp;
                end
    @(posedge clk);
    while(cond[in1])
    begin
     @(posedge clk);
     temp = temp + in1;
    end
    out2 = temp;
 end

endmodule
