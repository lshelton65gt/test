/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// repeat condition having registers 
// Section 5.12.3 of the test plan
module REPEAT3(cond1,cond2,clk,in1,out1);
input clk;
input [1:0] cond1,cond2,in1;
output [31:0] out1;
reg [31:0] out1;

  always
  begin
    @(posedge clk)
    begin : block
      reg [1:0] cond11,cond22;
      reg [31:0] temp;
      temp =0;
      cond11=cond1;
      cond22=cond2;
      repeat(cond11 > cond22)
      begin
       @(posedge clk);
        temp = temp + in1;
      end
      out1 = temp;
    end
  end
endmodule

