/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// repeat condition having concatenation 
// Section 5.12.9 of the test plan
module REPEAT9 (in1,in2,cond1,cond2,clk,out1);
input [2:0] in1,in2;
input cond1,cond2,clk;
output [31:0] out1;
reg [31:0] out1;


  always 
  begin
    @(posedge clk)
    out1 = in1;
    repeat({cond1,cond2})
    begin
      @(posedge clk);
      out1 = out1 + in2;
    end
 end
endmodule

