/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// repeat condition having numbers 
// Section 5.12.1 of the test plan
module REPEAT1(clk,in1,in2,out1);
input [3:0] in1,in2;
input clk;
output [3:0]out1;
reg [3:0]out1;
 
  always
  begin
     @(posedge clk)
     repeat(1'b1)
     begin : b1
     @(posedge clk);
      if(in1 > 7 ) disable b1;
      out1 = in1 ^ in2;
     end
  end
endmodule
