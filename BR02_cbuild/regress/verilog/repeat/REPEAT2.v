/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// repeat condition having parameters 
// Section 5.12.2 of the test plan
module REPEAT2(clk,in1,in2,out1);
input [3:0] in1,in2;
input clk;
output [3:0] out1;
reg [3:0] out1;
parameter p=1'b1;
 
  always
  begin
    @(posedge clk)
     repeat(p)
     begin : b1
     @(posedge clk);
      if(in1 > 7 ) disable b1;
      out1 = in1 ~^ in2;
     end
  end
endmodule
