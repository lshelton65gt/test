/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// repeat condition having wires 
// Section 5.12.4 of the test plan
//cond1 and cond2 inpout ports treated as wires.
module REPEAT4(cond1,cond2,clk,in1,out1);
input clk;
input [1:0] cond1,cond2,in1;
output [31:0] out1;
reg [31:0] out1;

  always
  begin
    @(posedge clk)
    begin : block
      reg [31:0] temp;
      temp =0;
      repeat(cond1 > cond2)
      begin
       @(posedge clk);
        temp = temp + in1;
      end
      out1 = temp;
    end
  end
endmodule

