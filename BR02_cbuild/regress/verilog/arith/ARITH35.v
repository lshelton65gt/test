/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Modulus operator having concatenation as operands
// Section 2.1.1.4 of the testplan

module ARITH35(clk,out1,out2);
input clk;
output [31:0] out1,out2;
reg [31:0] out1,out2;
parameter p1=2'b11,p2=2'b10,p3=2'b01;

  always @(posedge clk)
  begin
    out1 = {p1,p2,p3,2'b11} % 4'b1000;
    out2 = { {p2,1'b1},{p1,3'o4},{p3,4'hf}} % 3'o4;
  end
endmodule
























  

