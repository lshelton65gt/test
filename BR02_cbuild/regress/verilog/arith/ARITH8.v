/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

//Subtracting two negative integers
// Section 2.1.1.2 of test plan

module ARITH8 (clk,out1,out2);
input clk;
output [31:0] out1,out2;
reg [31:0] out1,out2;
integer int1,int2,int3,int4;

  always @(posedge clk)
  begin
     int1=-100;
     int2=-200 + ( -300)+( -400);
     int3=500 +( - 900);
     int4=-1000;
     out1=int1 - int2;
     out2=int3 - int4;
  end
endmodule

    

