/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Section 2.1.1.1 of test plan

module ARITH7(clk,out1,out2);
input clk;
output [6:0] out1,out2;
reg [6:0] out1,out2;
reg [4:0] reg1,reg2,reg3,reg4;



  always @(posedge clk)
  begin
    reg1 = -20;
    reg2 = -30;
    reg3 = -31;
    reg4 = -15;
    out1 = reg1 + reg2;
    out2 = reg3 + reg4;
  end
endmodule
    
