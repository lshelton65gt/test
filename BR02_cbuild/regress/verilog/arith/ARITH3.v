/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// / and % operators
// Section 2.1.1.4 and 2.1.1.5 of the test plan

module ARITH3(clk,out1,out2);
input clk;
output [10:0] out1,out2;
reg [10:0] out1,out2;

  always @(posedge clk)
  begin: block
   reg [10:0] r1,r2;
   integer i1,i2;
   parameter p1=1000,p2=16;

   r1 = 16'd12345 / 32;
   r2 = p1 /p2;
   i1 = 300987 /2'b10;
   i2 = 16'hffff/3'o4;
   out1 = r1 + r2 + i1 + i2;

   r1 = 16'd12345 % 32;
   r2 = p1 % p2;
   i1 = 300987 % 2'b10;
   i2 = 16'hffff % 3'o4;
   out2 = r1 + r2 + i1 + i2;
  end
 endmodule


