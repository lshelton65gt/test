/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// Modulus  ( % ) by nonstatic expression 
// Section 2.1.1.5.1 of the test plan
module ARITH47(clk, opa, opb, quo, rem);
input           clk;
input   [49:0]  opa;
input   [23:0]  opb;
output  [49:0]  quo, rem;
reg     [49:0]  quo, rem, quo1, remainder;
always @(posedge clk)
        quo1 <= #1 opa / opb;
always @(posedge clk)
        quo <= #1 quo1;
always @(posedge clk)
        remainder <= #1 opa % opb;
always @(posedge clk)
        rem <= #1 remainder;
endmodule
