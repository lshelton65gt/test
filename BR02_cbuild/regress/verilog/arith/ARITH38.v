/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Division operator having bit_select as operand
// Section 2.1.1.4 of test plan

module ARITH38 (clk,out1,out2);
input clk;
output [7:0] out1,out2;
reg [7:0] out1,out2;
parameter [1:0]  p1=2'b11,p2=2'b10;

  always @(posedge clk)
  begin
    out1 = { p1[0],p2[1],p1[1] } / 2; 
    out2 = { p1[0],p2[0],p1[1],p2[0] } /4;
  end
endmodule


