/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// - operator having bit_selects as operands
// Section 2.1.1.2 of test plan

module ARITH22 (in1,in2,in3,out1,out2);
input [1:0] in1,in2,in3;
output [1:0] out1,out2;

 assign out1 = in1[0] - in2[0] - in3[0];
 assign out2 = in1[1] - in2[1] - in3[1];

endmodule

