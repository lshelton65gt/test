/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// + operator having numbers
// Section 2.1.1.1 of test plan

module ARITH19 (clk,out1,out2);
input clk;
output [14:0] out1,out2;
reg [14:0] out1,out2;

  always@(posedge clk)
  begin
    out1 = 12'o765 +12'habc + 12'd1234 +12'b111111110000;
    out2 = 78654 + 4'h1 + 8'b10101010 + 6'o45 + 3'd3;
  end
endmodule
