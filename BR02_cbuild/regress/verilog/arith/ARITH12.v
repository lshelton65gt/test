/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Multiplication
// Section 2.1.1.3 of test plan

module ARITH12(clk,in1,in2,in3,in4,out1,out2);
input clk;
input [4:0] in1,in2,in3,in4;
output[7:0] out1;
output[10:0] out2;

reg [7:0] reg1,reg2,out1;
reg [10:0] reg3,out2;
  always @(posedge clk)
  begin
     reg1 = in1 * in3;
     reg2 = in3 * in4;
     reg3=reg1 * reg2;
     out1 = reg3;
     out2 = reg3;
  end
endmodule
    
