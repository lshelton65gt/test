/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// - operator having part-select as operands
// Section 2.1.1.2 of test plan

module ARITH26(in1,in2,in3,out1,out2);
input [4:1] in1,in2,in3;
output [4:2] out1,out2;

 assign out1 = in1 [4:3] - in2 [4:3] - in3[4:3];
 assign out2 = in1 [2:1] - in2 [2:1] - in3[2:1];
endmodule
