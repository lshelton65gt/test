/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Division
// Section 2.1.1.4 of test plan

module ARITH14(clk,in1,in2,out1,out2);
parameter p1 = 2,
	  p2 = 4;
input clk;
input [15:0] in1,in2;
output [31:0] out1,out2;
reg [31:0] out1,out2;
reg [15:0] reg1,reg2;
integer int1,int2;
reg [31:0]reg3,reg4;

   always @(posedge clk)
   begin
     reg1 = in1;
     reg2 = in2;
     reg3 = 1/p1;
     reg4 = 1/p2;
     int1 = (-reg1*reg3);
     int2 = (-reg2*reg4);
     out1 = int1;
     out2 = int2;
   end
endmodule
     
