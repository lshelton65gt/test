/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// + operator having multiple concatenation
// Section 2.1.1.1 of test plan

module ARITH17 (in1,in2,in3,out1,out2);
input in1,in2,in3;
output [4:0] out1,out2;
  assign out1 = {2{in1}} + {3{in2}} + {4{in3}};
  assign out2 = {3{in1}} + {3{in2}} + {3{in3}};
endmodule
