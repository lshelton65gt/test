/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// - operator having concatenation as operands
// Section 2.1.1.1 of test plan

module ARITH16(in1,in2,in3,in4,in5,in6,out1,out2);
input in1,in2,in3,in4,in5,in6;
output [2:0] out1,out2;

 assign out1 = {in1,in2} - {in3,in4};
 assign out2 = {in1,in2} - {in3,in4} - {in5,in6};
endmodule
