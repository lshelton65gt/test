/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// subtraction in funccall
// section 2.1.1.7 of the test plan

module ARITH43 (in1,out1);
input[7:0] in1;
output[3:0] out1;
reg[3:0] temp;

function[3:0] myfunc;
input[7:0] in1;
reg[3:0] temp1;
integer sigval;
begin
  sigval = in1 - 15;
  if (sigval < 0) temp1 = 7 - sigval;
  else temp1 = sigval;
  myfunc = temp1;
end
endfunction

always @ (myfunc (in1))
  temp = myfunc (in1);

assign out1 = temp;

endmodule
