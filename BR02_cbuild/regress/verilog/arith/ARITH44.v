/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// subtraction using concat
// section 2.1.1.8 of the test plan

module ARITH44 (in1,out1);
input[2:0] in1;
output[5:0] out1;

assign out1 = (6'b000111 - {3'b000,in1}) ;

endmodule
