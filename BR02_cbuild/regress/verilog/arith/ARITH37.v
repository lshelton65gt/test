/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Modulus operator having multiple concatenation as operand
// Section 2.1.1.5 of test plan

module ARITH37 (clk,out1,out2);
input clk;
output [31:0] out1,out2;
reg [31:0] out1,out2;
parameter p1=1'b1,p2=2'b10,p3=3'b111;

  always @(posedge clk)
  begin
    out1 = {2{{p1,p2,p3}}}%5'b10000;
    out2 = {3{p3}}%64;
  end
endmodule
