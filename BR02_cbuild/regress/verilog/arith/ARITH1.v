/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// + and - operators
// Section 2.1.1.1 and 2.1.1.2 of test plan

module ARITH1(in1,in2,in3,in4,out1,out2);
input [8:0] in1,in2,in3,in4;
output [31:0] out1,out2;
reg [31:0] out1,out2;

  always @(in1 or in2 or in3 or in4)
  begin: block
   reg [31:0] r1,r2;
   reg r3;
   integer i1,i2;
   parameter p1=10,p2=20;

    r1 = in1 + in2;
    r2 = in3[7:0] + in4[7:0];
    r3 = in1[0] + in2[0];
    i1 = in3 + in4;
    i2 = r1 + r2 + r3;
    out1 = i1 + i2+p1+p2;
    r2 = in3[7:0] - in4[7:0];
    r3 = in1[0] - in2[0];
    i1 = in3 - in4;
    i2 = r1 - r2 - r3;
    out2 = i1 - i2- p1 - p2;
  end
endmodule



    
