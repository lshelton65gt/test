/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// * operator having multiple concatenation as operand
// Section 2.1.1.3 of test plan 

module ARITH29 (in1,in2,in3,out1,out2);
input in1,in2,in3;
output [7:0] out1,out2;
 assign out1 = {2{in1}} * {3{in2}} * {4{in3}};
 assign out2 = {2{{in1,in2}}} * {2{{in2,in3}}};
endmodule
