/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// * operator having concatenation as operands
// Section 2.1.1.3 of test plan

module ARITH28 (in1,in2,in3,in4,in5,in6,out1,out2);
input in1,in2,in3,in4,in5,in6;
output [4:0] out1,out2;
  assign out1 = {in1,in2} * {in3,in4} * {in5,in6};
  assign out2 = {in1,in2,in5} * {in3,in4};
endmodule
