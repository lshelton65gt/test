/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Modulus operator having part_select as operand
// Section 2.1.1.5 of test plan

module ARITH41 (clk,out1,out2);
input clk;
output [7:0] out1,out2;
reg [7:0] out1,out2;
parameter [3:0]  p1=2'b11,p2=2'b10;

  always @(posedge clk)
  begin
    out1 = { p1[1:0],p2[1:0],p1[3:2] } % 2; 
    out2 = { p1[1:0],p2[1:0],p1[3:2],p2[3:2] } % 4;
  end
endmodule


