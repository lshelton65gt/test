/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While Condition having bit_select
// Section 5.5.6 of the test plan

module WHILE18(clk,cond,index,in1,out1,out2);
input clk,in1;
input [3:0] cond;
input [1:0] index;
output [31:0] out1,out2;
reg [31:0] out1,out2;
reg [31:0] temp;

  always
  begin
    @(posedge clk)
    temp=0;
		out1=0;
		out2=0;
    while(cond[in1])
    begin
     @(posedge clk);
     temp = temp + in1;
    end
    out1 = temp;
		@(posedge clk)
    while(cond[1:0]==2'b11)
		begin
		 @(posedge clk)
     temp = temp + in1;
		end
    out2 = temp;
 end

endmodule


