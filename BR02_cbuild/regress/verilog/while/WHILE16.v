/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While having task
// Section 5.5.18 of the test plan

module WHILE16(clk,in1,in2,out1,out2);
input [7:0] in1,in2;
input clk;
output [7:0] out1,out2;
reg [7:0] out1,out2;
integer i;
task task1;
input in1,in2;
output out;
 out = in1 & in2;
endtask

task task2;
input in1,in2;
output out;
 out = in1 | in2;
endtask
  
   always 
   begin
     @(posedge clk)
     begin
       i = 0;
       while(i <=7)
       begin
         @(posedge clk);
         task1(in1[i],in2[i],out1[i]);
         task2(~in1[i],~in2[i],out2[i]);
         i = i +1;
       end
     end
  end
endmodule
