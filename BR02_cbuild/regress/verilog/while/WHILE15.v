/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While having procedural assignment
// Section 5.5.17 of the test plan

module WHILE15(clk,in1,in2,out1,out2);
input [7:0] in1,in2;
input clk;
output [7:0] out1,out2;
reg [7:0] out1,out2;
integer i;
  
   always 
   begin
     @(posedge clk)
     begin
       i = 0;
	   out1=0;
	   out2=0;
       while(i <=7)
       begin
         @(posedge clk);
         {out1,out2} = {in1,in2};
         i = i +1;
       end
     end
  end
endmodule
