/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While Statement having if statement
 // Section 5.5.10 of the test plan

 module WHILE11(cond,cond1,cond2,in1,out1,clk);
 input clk;
 input [7:0] cond,cond1,cond2,in1;
 output [31:0] out1;
 reg [31:0] out1;

  always
  begin
   @(posedge clk)
   begin : block
   reg [31:0] temp;
   reg [7:0] c;
   c= cond;
   temp =0;
     while(c)
     begin
      @(posedge clk);
      if(cond1)
        if(cond2)
          temp = temp + in1;
        else
          temp = temp + 2;
      else
        if(cond2)
          temp = temp + in1;
        else
          temp = temp +3;
      c = c<<1;
     end
     out1 = temp;
     end
   end
  endmodule


