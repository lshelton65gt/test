/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// While loop without an event control 
// Section 5.5.20 of the test plan
         module WHILE(in1,in2,cond,out1);
         input in1,in2;
         input [1:0] cond;
         output [31:0] out1;
         reg [31:0]  out1;
         reg [7:0] i;
         
           always @(in1 or in2 or cond)
           begin : block1
            out1 = in1;
            i = 0;
            while (i <7) 
            begin : block
              if(cond >1) disable block1;
              out1 = out1 + in2;
                  i = i + 1;
           end
           end
         endmodule
