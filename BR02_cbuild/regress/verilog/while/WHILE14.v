/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While statement having forever loop, named block and disable statement
// Section 5.5.14 and 5.5.15  of the test plan
// Section 5.5.15 
// Section 5.5.16 

module WHILE14(cond,cond1,clk,out1);
input clk;
input [1:0] cond1;
input [7:0] cond;
output [31:0] out1;
reg [31:0] out1;
integer i;

  always
  begin
    @(posedge clk)
    begin : block1
     reg [31:0] temp;
    reg [7:0] c;
   temp = 0;
    c = cond;
     while (c)
     begin : block
     i =0;
      @(posedge clk);
      forever
      begin
      @(posedge clk);
       if(i > 7) disable block;
        temp = temp + cond1;
        out1 = ~temp;
        i = i + 1;
       end
      c=c>>1;
     end
     out1 = temp;
    end
  end
endmodule
