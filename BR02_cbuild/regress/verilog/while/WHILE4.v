/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While condition having function call
// Section 5.5.9 of test plan

module WHILE4(in1,in2,clk,out1);
input [1:0] in1,in2;
input clk;
output [31:0] out1;
reg [31:0] out1;
function max;
input [1:0] in1,in2;
 if(in1> in2)
   max = 1'b1;
 else
   max =1'b0;
endfunction

always
begin
//   out1 = 0;
	 @(posedge clk)
   out1 = 0;
   while(max(in1,in2))
   begin
     @(posedge clk);
      out1 = out1 + 1;
   end
end
endmodule

  
