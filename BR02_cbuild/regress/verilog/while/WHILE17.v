/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While Condition having expression
// Section 5.5.19 of the test plan

module WHILE17(clk,cond,index,in1,out1);
input clk,in1;
input [3:0] cond;
input [1:0] index;
output [31:0] out1;
reg [31:0] out1;
reg [31:0] temp;

  always
  begin
    @(posedge clk);
        temp=0;
		out1=0;
    while(cond[in1])
    begin
     @(posedge clk);
     temp = temp + in1;
    end
    out1 = temp;
 end
endmodule


