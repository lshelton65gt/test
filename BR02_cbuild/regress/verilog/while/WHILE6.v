/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While condition having number and disable statement
// Section 5.5.1 and 5.5.16  of the test plan
 
module WHILE6(clk,in1,in2,out1);
input [3:0] in1,in2;
input clk;
output [3:0]out1;
reg [3:0]out1;
 
  always
  begin
     @(posedge clk)
     while(1'b1)
     begin : b1
     @(posedge clk);
      if(in1 > 7 ) disable b1;
      out1 = in1 ^ in2;
     end
  end
endmodule

