/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While condition having part-select
// Section 5.5.7 of test plan 
// always statement having while loop
// Section 5.10.7 of test plan 

module WHILE2 (in1,in2,cond1,clk,out1);
input [1:0] in1,in2;
input[2:0] cond1;
input clk;
output [31:0] out1;
reg [31:0] out1;
parameter p1 = 2'b10;

  always 
  begin
	@(posedge clk)
    out1 = 0;
    while( cond1[1:0]!=p1 || cond1[2:1]!=p1)
    begin
      @(posedge clk)
      out1 = out1 + in2;
    end
 end
endmodule
