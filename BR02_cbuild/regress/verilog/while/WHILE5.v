/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Nested while loop
// Section 5.5.12 of test plan

module WHILE5 (in1,in2,in3,cond1,cond2,cond3,clk,out1);
input [1:0] in1,in2,in3,cond1,cond2,cond3;
input clk;
output [31:0] out1;
reg [31:0] out1;

  always
  begin
   // out1 = 0;
	 @(posedge clk)
   out1 = 0;
   while(cond1!=2'b11)
   begin
     @(posedge clk);
     out1 = out1 + in1;
     while(cond2!=2'b11)
     begin
       @(posedge clk);
       out1 = out1 + in2;
       while(cond3!=2'b11)
       begin
         @(posedge clk);
         out1 = out1 + in3;
       end
    end
  end
 end
endmodule



