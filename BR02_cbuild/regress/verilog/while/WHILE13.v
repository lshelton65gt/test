/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While statement having for statement
// Section 5.5.13 of the test plan

module WHILE13(cond,cond1,clk,out1);
input clk;
input [1:0] cond1;
input [7:0] cond;
output [31:0] out1;
reg [31:0] out1;
integer i;
reg [31:0] temp;
reg [7:0] c;

  always
  begin
    @(posedge clk)
    begin : block
    temp = 0;
    c = cond;
     while (c)
     begin
      @(posedge clk);
      out1 = temp;
      for(i=0;i<=7;i=i+1)
        temp = temp +cond1;
      c=c>>1;
     end
     out1 = temp;
    end
  end
endmodule
