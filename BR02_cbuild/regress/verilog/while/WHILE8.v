/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// While Condition having wires
// Section 5.5.4 of the test plan

module WHILE8(cond1,cond2,clk,in1,out1);
input clk;
input [1:0] cond1,cond2,in1;
output [31:0] out1;
reg [31:0] out1;

  always
  begin
    @(posedge clk)
    begin : block
      reg [31:0] temp;
      temp =0;
      while(cond1 > cond2)
      begin
       @(posedge clk);
        temp = temp + in1;
      end
      out1 = temp;
    end
  end
endmodule
