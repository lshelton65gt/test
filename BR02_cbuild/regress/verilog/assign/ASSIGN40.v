/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Blocking assignment having register on lhs
// Section 3.2.2 of the test plan

module ASSIGN40(in1,in2,in3,in4,out1);
input in1,in2;
input [1:0] in3,in4;
output [2 : 0] out1;
reg [2 : 0] out1;
reg reg1;
      reg [1:0] reg2;
      reg [2:0] reg3;
    always @(in1 or in2 or in3 or in4) 
    begin : block
        reg1 = in1 & in2;
        reg2 = in3 | in4;
        reg3 = { reg1,reg2};
        out1 = reg3;
    end
endmodule

