/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// nonblocking assignment inside for loop
// section 5.1.5 of test plan

module ASSIGN71(in1,in2,in3,out1);
input  in1,in2;
input [1:0] in3;
output [1:0] out1;
reg [1:0] out1;

integer i;

always @(in1 or in2 or in3)
  begin
	for (i = 0; i < 2; i = i+1)
	begin
		if (in3[i])
			out1[i] <= in2;
		else
			out1[i] <= in1;
	end
  end
endmodule
