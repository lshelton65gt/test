/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having expression on rhs
// Section 3.1.19 of the test plan

module ASSIGN34(in1,in2,out1,out2);
input [7:0] in1,in2;
output [7:0] out1,out2;

  assign out1 = (in1 << 4) >> 2;
  assign out2 = (in2 >> 4) >> 2;
endmodule
