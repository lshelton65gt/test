/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having conditional operator on RHS
// Section 3.1.18 of test plan

module ASSIGN7(in1,in2,in3,in4,out1,out2);
input [1:0] in1,in2,in3,in4;
output [1:0] out1,out2;

  assign {{out1[0],out2[1]},{out1[1],out2[0]}} =
        in1 > in2 ? ( in3 > in4 ? {in1,in3} : {in2,in4})
                  : ( in3 > in4 ? {in2,in3} : {in1,in4});
endmodule

