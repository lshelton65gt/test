/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Non blocking assignments having expr on rhs
// Section 3.2.16 of the test plan

module ASSIGN59(in1,in2,in3,in4,in5,in6,out1,out2,out3,out4,out5,out6,out7);
input [1:0] in1,in2,in3,in4,in5,in6;
output out1,out2,out3,out4,out5,out6,out7;
reg out1,out2,out3,out4,out5,out6,out7;

  always @( in1 or in2 or in3 or in4 or in5 or in6)
  begin
    out1 <= in1 > in2;
    out2 <= in3 >= in4;
    out3 <= in5 < in6;
    out4 <= in1 <= in6;
    out5 <= in1 == in5;
    out6 <= in2 != in4;
    out7 <= !in3;
  end
endmodule

    
