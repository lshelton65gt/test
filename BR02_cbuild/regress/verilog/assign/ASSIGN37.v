/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having expression on rhs
// Section 3.1.19 of the test plan

module ASSIGN37(in1,in2,in3,in4,in5,in6,out1,out2,out3,out4,out5,out6);
input [31:0] in1,in2,in3,in4,in5,in6;
output [31:0] out1,out2,out3,out4,out5,out6;

   assign out1 = &in1;
   assign out2 = ~&in2;
   assign out3 = |in3;
   assign out4 = ~|in4;
   assign out5 = ^in5;
   assign out6 = ~^in5;

endmodule

