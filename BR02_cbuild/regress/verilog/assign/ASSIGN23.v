/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having wor on LHS
// Section 3.1.4 of the test plan

module ASSIGN23(in1,in2,in3,in4,in5,in6,out1,out2,out3);
input in1,in2;
input [1:0] in3,in4;
input [2:0] in5,in6;
output out1;
output [1:0] out2;
output [2:0] out3;
wor out1;
wor [1:0] out2;
wor [2:0] out3;

  assign out1 = in1 | in2;
  assign out1 = in1 ^ in2;
  assign out2 = ~in3;
  assign out2 = ~in4;
  assign out3 = in5 << 1;
  assign out3 = in6 >> 1;
endmodule
  
