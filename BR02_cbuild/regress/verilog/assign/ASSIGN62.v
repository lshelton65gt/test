/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having nested concatenation on lhs
// Section 3.1.8 of test plan 

module ASSIGN62 (in1,out1,out2);
input [7:0] in1;
output [3:0] out1,out2;

  assign { { {out1[0],out1[1]} ,out2[1:0]} , {out1[3:2],out2[3:2]}} = in1;

endmodule
