/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Blocking assignment having function call on rhs
// Section 3.2.13 of test plan

module ASSIGN14(in1,in2,in3,in4,clk,out1);
input [1:0] in1,in2,in3,in4;
input clk;
output [6:0] out1;
reg [6:0] out1;

 function [3:0] mult;
 input [1:0] in1,in2;
   mult = in1 * in2;
 endfunction
 always @(posedge clk)
 begin
   out1[6:0] = mult(in1,in2) * mult(in3,in4);
 end
endmodule


   
   


