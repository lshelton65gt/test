/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Non Blocking assignment having conditional op on rhs
// Section 3.2.15 of test plan

module ASSIGN19(in1,in2,in3,in4,out1,out2);
input [1:0] in1,in2,in3,in4;
output [1:0] out1,out2;
reg [1:0] out1,out2;

  always @(in1 or in2 or in3 or in4)
  begin
    out1 <= (in1 + in2 ) >= (in3 + in4) 
           ? (in1 > in2 ) ? in1 - in2 : in2 - in1
           : (in3 > in4 ) ? in3 - in4 : in4 - in3;
    out2 <= in1 > in2 ? in3 > in4 ? in3 : in4 
                       : in2 - in1;

  end
endmodule
