/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// Continuous assign statement with inverted ranges 
// Section 7.4.2.1 of the test plan
        module ASSIGN81 (in1, in2, in3, in4, sel1, sel2, sel3, sel4, z1, z2, z3, z4);
        input   [4:0]   in1;
        input   [2:0]   sel1;
        output          z1;
        input   [0:4]   in3;
        input   [0:2]   sel3;
        output          z3;
        input   [2:0]   in2;
        input   [1:0]   sel2;
        output          z2;
        input   [0:2]   in4;
        input   [0:1]   sel4;
        output          z4;
        assign z1 = in1[sel1];
        assign z2 = in2[sel2];
        assign z3 = in3[sel3];
        assign z4 = in4[sel4];
        endmodule
