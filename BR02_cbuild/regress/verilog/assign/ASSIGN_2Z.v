/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

//Same net driven by two cont assign having z 
// Section 3.1.21 of the test plan

module ASSIGN_2Z(ctl,in1,in2,out1,out2);
input ctl;
input [3:0] in1,in2;
output [3:0] out1,out2;

  assign  out1 = ctl ? in1 : 4'bz;
  assign  out2 = !ctl ? in2 : 4'bz;

endmodule
