/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Blocking assignment having integer on rhs
// Section 3.2.10 of the test plan
// always block having procedural assignment
// Section 5.10.3 of the test plan


module ASSIGN52 (in1,in2,in3,in4,in5,in6,out1);
input [15:0] in1,in2,in3,in4,in5,in6;
output [31:0] out1;
reg [32:0] out1;
  always @( in1 or in2 or in3 or in4 or in5 or in6)
  begin: block
    integer int1,int2,int3;
      int1 = {in1,in2};
      int2 = {in3,in4};
      int3 = {in5,in6};
      out1 = int1 + int2 + int3;
  end
endmodule
