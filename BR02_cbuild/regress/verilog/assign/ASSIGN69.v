/* Copyright (c) 1998 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Procedural Assignment with multiple concatenation
// section 3.2.14 of test  plan

module ASSIGN69(in1,in2,out1);
input [3:0] in1,in2;
output [15:0] out1;
reg [15:0] out1;


always @(in1 or in2)
begin
  out1 = {2{in1,in2}};
end
endmodule



