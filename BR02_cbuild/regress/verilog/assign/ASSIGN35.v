/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having expression on rhs
// Section 3.1.19 of the test plan

module ASSIGN35(in1,in2,out1,out2,out3,out4,out5,out6);
input [7:0] in1,in2;
output  out1,out2,out3,out4,out5,out6;

  assign out1 = in1 > in2;
  assign out2 = in1 >= ~in2;
  assign out3 = ~in1 < in2;
  assign out4 = ~in1 <= 3;
  assign out5 = in1 !=3;
  assign out6 = in1 == in2;
endmodule
