/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Non Blocking assignment having numbers on rhs
// Section 3.2.6 of the test plan

module ASSIGN45(clk,out1,out2,out3,out4,out5);
input clk;
output [12:0] out1,out2,out3,out4;
reg [12:0] out1,out2,out3,out4;
output [4:0] out5;
reg [4:0] out5;

   always @(posedge clk)
   begin
     out1 <= -12'b111111000000;
     out2 <= -12'o1234;
     out3 <= -12'habc;
     out4 <= -12'd1234;
     out5 <= -12;
   end
endmodule

