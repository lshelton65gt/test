/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

//Non  Blocking assignment having function call on rhs
// Section 3.2.13 of test plan

module ASSIGN15(in1,in2,in3,in4,clk,out1);
input [1:0] in1,in2,in3,in4;
input clk;
output [4:0] out1;
reg [4:0] out1;

 function [3:0] mult;
 input [1:0] in1,in2;
   mult = in1 * in2;
 endfunction
 function [2:0] add;
 input [1:0] in1,in2;
   add = in1 + in2;
 endfunction

 always @(posedge clk)
 begin
   out1[4:0] <= mult(in1,in2) +  add(in3,in4);
 end
endmodule


   
   


