/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having integer on rhs
// Section 3.1.13 of the test plan

module ASSIGN30(in1,in2,in3,in4,in5,in6,out1,out2,out3);
input in1,in2;
input [1:0] in3,in4;
input [2:0] in5,in6;
output out1;
output [1:0] out2;
output [2:0] out3;
integer reg1;
integer  reg2;
integer  reg3;

  assign out1 = reg1;
  assign out2 = reg2;
  assign out3 = reg3;
     always @( in1 or in2 or in3 or in4 or in5 or in6)
     begin: block
         reg1 = in1 ^ in2;
         reg2 = in3 ~^ in4;
         reg3 = in5 & in6;
     end

endmodule
