/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Blocking assignment having conditional op on rhs
// Section 3.2.15 of test plan

module ASSIGN18(in1,in2,in3,in4,out1);
input [1:0] in1,in2,in3,in4;
output [1:0] out1;
reg [1:0] out1;

  always @(in1 or in2 or in3 or in4)
  begin
    out1 = (in1 + in2 ) >= (in3 + in4) 
           ? (in1 > in2 ) ? in1 - in2 : in2 - in1
           : (in3 > in4 ) ? in3 - in4 : in4 - in3;
  end
endmodule
