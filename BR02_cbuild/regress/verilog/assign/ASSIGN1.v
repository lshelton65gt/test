/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Wire declaration having constinuous assignment
// Section 3.1.1 of the test plan
// module having  constinuous assignment
// Section 7.4.2 of the test plan

module ASSIGN1 (in1,in2,in3,out1);
input [1:0] in1,in2,in3; 
output [1:0] out1;
wire [1:0] w = in1 > in2 ? ( in1 > in3 ? in1 : (in3 > in2 ? in3 : in2)) 
                         : (in2 > in3  ? in2 : (in3 > in1 ? in3 : in1));

assign out1 = w;
endmodule
