/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// explicit part select on a signal & reg
// section 3.1.25 of test plan

module ASSIGN78(clk, in1, in2, out1, out2);
input   clk, in1, in2;
output  [1:0] out1;
output [0:0] out2;
reg [0:0] out2;

assign out1[1:0] = in1 & in2;


always @(posedge clk)
begin
	out2[0:0] = in1 | in2;
end

endmodule
