/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Blocking assignment having integer on lhs
// Section 3.2.1 of the test plan

module ASSIGN38(in1,in2,in3,in4,out1);
input [30:0] in1,in2,in3,in4;
output [31:0] out1;
reg [31:0] out1;
integer int1,int2;

   always @( in1 or in2 or in3 or in4)
   begin : block
      int1 = in1 + in2;
      int2 = in3 * 2;
      int1 = int1 + int2 - in4;
      out1 = int1;
   end
endmodule
