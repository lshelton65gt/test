/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continous assignment having part-select on lhs
// Section 3.1.7 of the test plan

module ASSIGN3 (in1,in2,out1);
parameter a=2'b00,b=2'b01,c=2'b10,d=2'b11;
input [d:a] in1,in2;
output [d:a] out1;

  assign out1[b : a] = in1 [b:a] & in2 [b:a];
  assign out1[d : c] = in1 [d:c] | in2[d:c];

endmodule

