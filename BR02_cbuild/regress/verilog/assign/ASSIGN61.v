/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Blocking assignment having integer on lhs
// Section 3.2.1 of the test plan
 
module ASSIGN61(in1,in2,in3,in4,out1);
input [31:0] in1,in2,in3,in4;
output [31:0]out1;
reg [31:0] out1;
integer int1,int2; // 32 bit reg instead of integer
 
   always @( in1 or in2 or in3 or in4 or int1 or int2)
   begin : block
      int1 = in1 + in2;
      int2 = in3 + in4;
      out1 = int1 + int2 ;
   end
endmodule

