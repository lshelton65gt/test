/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Blocking assignment having bit-select on lhs
// Section 3.2.3 of test plan

module ASSIGN8 (in1,in2,clk,out1);
parameter a = 0,b = 1;
input [b:a] in1,in2;
input clk;
output [b:a] out1;
reg [b:a] out1;

  always @(posedge clk)
  begin
     out1[a] = in1[a] & in1[b];
     out1[b] = in1[b] | in1[a];
 end
endmodule


