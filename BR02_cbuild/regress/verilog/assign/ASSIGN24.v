/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having tri on LHS
// Section 3.1.5 of the test plan

module ASSIGN24(in1,in2,out1);
input in1,in2;
output out1;
wire a=1'bz;
tri out1;

   assign  out1 = in1;
   assign out1 = in2;
   assign out1 = a;
endmodule
  
