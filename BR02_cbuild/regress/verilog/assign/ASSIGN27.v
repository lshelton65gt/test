/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having parameters on rhs
// Section 3.1.10 of the test plan

module ASSIGN27(in1,out1,out2,out3,out4,out5,out6);
input in1;
output out1;
output [11:0] out2,out3,out4,out5;
output [3:0] out6;
 parameter p1 = 12'b111111000000,
           p2 = 12'habc,
           p3 = 12'o3456,
           p4 = 12'd1234,
           p5 = 15;
  assign out1 = in1;
  assign out2 = p1;
  assign out3 = p2;
  assign out4 = p3;
  assign out5 = p4;
  assign out6 = p5;
endmodule

