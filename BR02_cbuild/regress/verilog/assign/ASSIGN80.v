/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Unary plus operation
// Section 3.1.27 of the test plan 

module ASSIGN80 (cin,in1,out1,cout);
input  in1, cin;
output out1,cout;

  assign {cout,out1}  = in1 + +cin;

endmodule
