
// Assignments with output port on RHS 
// section 3.1.23 of test plan

module ASSIGN66(in1, in2, out1, out2);
input [3:0] in1, in2;
output [3:0] out1,out2;

assign out1 = in1 & in2;

assign out2 = ~out1;

endmodule
