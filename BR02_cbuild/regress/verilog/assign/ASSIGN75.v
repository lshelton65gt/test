/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// wire declaration with concat
// section 3.1.24 of test plan

module ASSIGN75(in0, in1, in2, out1);
input   in0, in1, in2;
output  [2:0] out1;

wire [2:0] w1 = {in0, in2, ~in1};

assign out1 = w1;

endmodule
