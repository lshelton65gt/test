/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Non Blocking assignment having part-select on rhs
// Section 3.2.12 of the test plan

module ASSIGN57(in1,in2,in3,in4,out,out2);
input [3:0] in1,in2,in3,in4;
output [3:0] out,out2;
reg [3:0] out,out2;

  always @( in1 or in2 or in3 or in4 )
  begin 
    out[1:0] <= in1[1:0] & in2[1:0];
    out[3:2] <= in1[3:2] & in2[3:2];
    out2[1:0] <= in3[1:0] & in4[1:0];
    out2[3:2] <= in3[3:2] & in4[3:2];
  end
endmodule
