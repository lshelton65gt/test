/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having concatenation on lhs
// Section 3.1.8 of test plan 

module ASSIGN4 (in1,in2,out1,out2);
input [1:0] in1,in2;
output [1:0] out1,out2;

  assign { {out1[0],out2[0]},{out1[1],out2[1]}} = {in1,in2};

endmodule
