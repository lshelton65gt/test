/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Condtinuous assignment having wire on lhs
// Section 3.1.2 of the test plan

module ASSIGN21(in1,in2,in3,in4,in5,in6,in7,in8,out1,out2,out3,out4);
input in1,in2;
input [1:0] in3,in4;
input [2:0] in5,in6;
input [3:0] in7,in8;
output out1;
wire out1;
output [1:0] out2;
output [2:0] out3;
output [3:0] out4;
wire [1:0] out2;
wire [2:0] out3;
wire [3:0] out4;

  assign out1 = in1 & in2;
  assign out2 = in3 | in4;
  assign out3 = in5 ^ in6;
  assign out4 = in7 ~^ in8;

endmodule

