/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Non Blocking assignment having parameters on rhs
// Section 3.2.7 of the test plan

module ASSIGN47(clk,out1,out2,out3,out4,out5);
input clk;
output [11:0] out1,out2,out3,out4;
reg [11:0] out1,out2,out3,out4;
output [3:0] out5;
reg [3:0] out5;
parameter p1=12'b101010111000,
          p2=12'o1234,
          p3=12'h123,
          p4 =12'd1000,
          p5 = 12;

   always @(posedge clk)
   begin
     out1 <= p1;
     out2 <= p2;
     out3 <= p3;
     out4 <= p4;
     out5 <= p5;
   end
endmodule

