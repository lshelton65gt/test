/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Wire assigned to a constant of lesser width
// Section 3.1.26 of the test plan 

module ASSIGN79 (in1,out1);
input [2:0] in1;
output [7:0] out1;
wire [7:0] wire1 = 'b1000000;

  assign out1 = wire1 >> in1;

endmodule
