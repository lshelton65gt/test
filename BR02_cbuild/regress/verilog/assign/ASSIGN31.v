/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having bit-select on rhs
// Section 3.1.14 of the test plan

module ASSIGN31(in1,in2,in3,in4,out1,out2);
input [3:0] in1,in2,in3,in4;
output [3:0] out1,out2;

  assign out1[0] = in1[0] ^ in2[0];
  assign out1[1] = in1[0] ^ in2[0]; 
  assign out1[2] = in1[0] ^ in2[0]; 
  assign out1[3] = in1[0] ^ in2[0]; 
  assign out2[0] = in1[0] ~^ in2[0]; 
  assign out2[1] = in1[0] ~^ in2[0]; 
  assign out2[2] = in1[0] ~^ in2[0]; 
  assign out2[3] = in1[0] ~^ in2[0]; 
endmodule
