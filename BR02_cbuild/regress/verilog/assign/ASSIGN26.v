/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having numbers on rhs
// Section 3.1.9 of the test plan

module ASSIGN26(in1,out1,out2,out3,out4,out5,out6);
input in1;
output out1;
output [12:0] out2,out3,out4,out5;
output [4:0] out6;

  assign out1 = in1;
  assign out2 = -12'b101010111111;
  assign out3 = -12'hfed;
  assign out4 = -12'o7654;
  assign out5 = -12'd1234;
  assign out6 = -15;
endmodule

