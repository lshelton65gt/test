// Procedural statement with blocking and non_blocking statements

// section 3.2.16 of test plan

module ASSIGN64(a1,b1,c1,m1,y1,m2,y2);
input a1,b1,c1;
output m1,y1,m2,y2;
reg m1,m2;
reg y1,y2;


always @(a1 or b1 or c1 or m1)
  begin : Block_comb
     m1 = (a1 & b1) + (a1 | b1) + (a1 ^ b1) + (a1 ^~ b1);
     y1 = m1 | c1;
  end

always @(a1 or b1 or c1 or m2)
  begin : Non_Block_comb
     m2 <= (a1 & b1) + (a1 | b1) + (a1 ^ b1) + (a1 ^~ b1);
     y2 <= m2 | c1;
  end

endmodule

