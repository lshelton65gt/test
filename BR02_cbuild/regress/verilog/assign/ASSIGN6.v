/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having concatenation on rhs
// Section 3.1.17 of the test plan

module ASSIGN6 (in1,in2,out1);
input [1:0] in1,in2;
output [7:0] out1;

  assign out1 = { {2{in1[0]}},{2{in2[1:0]}},{in1[1],1'b1}};

endmodule
