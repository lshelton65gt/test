/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Non Blocking assignment having concatenation on rhs
// Section 3.2.14 of test plan

module ASSIGN17 (in1,in2,in3,in4,out1);
input[1:0] in1,in2,in3,in4; 
output [13:0] out1;
reg [13:0] out1;

  always @(in1 or in2 or in3 or in4)
  begin
    out1 <= { {in1[0],in2[0]},{in3,1'b1},{2{in4[1:0],in1[1:0]}}};
  end
endmodule
