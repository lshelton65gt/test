// Always statement having blocking and non blocking statements

// section 3.2.16 of test plan

module ASSIGN67 ( clock, data, pipea,pipeb);
input clock,data;
output [3:0] pipea,pipeb;
reg [3:0] pipea,pipeb;

always @(posedge clock)
begin
    pipea[3] <= pipea[2];
    pipea[2] <= pipea[1];
    pipea[1] <= pipea[0];
    pipea[0] <= data;
end

always @(posedge clock)
begin
    pipeb[3] = pipeb[2];
    pipeb[2] = pipeb[1];
    pipeb[1] = pipeb[0];
    pipeb[0] = data;
end
endmodule
