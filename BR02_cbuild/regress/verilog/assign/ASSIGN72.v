/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// reading nonblocking value in synchronous part
// section 5.1.6 of test plan

module ASSIGN72(clk, reset, enable, out1, out2);
input   clk, reset, enable;
output  out1, out2;
reg out1, out2;

always @(posedge clk or posedge reset)
begin
	if (reset) begin
		out2 <= 1'b0;
		out1 <= 1'b0;
	end
	else begin
	if (enable)
		out1 <= 1'b1;
		out2 <= ~out1 ;
	end
end

endmodule
