/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Blocking assigment having register on rhs
// Section 3.2.9 of the test plan

module ASSIGN50(in1,in2,in3,in4,in5,in6,out1);
input in1,in2;
input [1:0] in4,in3;
input [3:0] in5,in6;
output [11:0] out1;
reg [11:0] out1;

   always @( in1 or in2 or in3 or in4 or in5 or in6)
   begin: block
   reg reg1;
   reg [1:0] reg2;
   reg [3:0] reg3;
     reg1 = &(in1 & in2);
     reg2 = ({in2,in3}) << 2;
     reg3 = in5 + in6;
     out1 = {{reg1,~reg1},{reg2,~reg2},{reg3,~reg3}};
   end
endmodule

     
