/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Non blocking assignment having expression on rhs
// Section 3.2.16 of the test plan

module ASSIGN58(in1,in2,in3,in4,in5,in6,out1,out2,out3,out4,out5,out6);
input [7:0] in1,in2,in3,in4,in5,in6;
output [7:0] out1,out2,out3,out4,out5,out6;
reg [7:0]  out1,out2,out3,out4,out5,out6;

  always @( in1 or in2 or in3 or in5 or in6)
  begin 
    out1 <= & in1;
    out2 <= ~&in2;
    out3 <= |in3;
    out4 <= ~|in4;
    out5 <= ^in5;
    out6 <= ~^in6;
 end
endmodule
