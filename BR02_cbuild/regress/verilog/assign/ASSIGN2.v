/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// continuous assignment having bit-select on lhs
// Section 3.1.6 of the test plan 

module ASSIGN2 (in1,in2,out1);
input [1:0] in1,in2;
output [1:0] out1;
parameter zero = 1'b0;
parameter one = 1'b1;
  assign out1[zero] = in1[zero] + in2[zero];
  assign out1[one]  = in1[1'b1] + in2[1'b1];

endmodule
