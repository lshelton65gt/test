/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Overlapping bit-slices of cont assign
// Section 3.1.22 of the test plan

module ASSIGN_OVERLAP(ctl,in1,in2,out1,out2);
input ctl;
input [3:0] in1,in2;
output [3:0] out1,out2;

  assign  out1[3:2] = ctl ? in1 : 4'bz;
  assign  out1[2:0] = !ctl ? in2 : 4'bz;
  assign  out2[3:2] = ctl ? in1 : 4'bz;
  assign  out2[2:0] = !ctl ? in2 : 4'bz;

endmodule
