/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

//Non Blocking assignment having concatenation on lhs
// Section 3.2.5 of test plan 

module ASSIGN13 (in1,in2,clk,out1,out2);
input [1:0] in1,in2;
input clk;
output [1:0] out1,out2;
reg [1:0] out1,out2;

  always @(posedge clk)
  begin
    {out1[0],out1[1],out2[1:0]} <= {2{in1+in2}};
  end
endmodule
 



