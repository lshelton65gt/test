/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// different bits driven by combinational & sequential block
// section 3.2.18 of test plan

module ASSIGN77(clk, in1, in2, out1);
input   clk, in1, in2;
output  [1:0] out1;
reg  [1:0] out1;

always @(in2)
begin
	out1[1] = in2;
end

always @(posedge clk)
begin
	out1[0] = in1;
end

endmodule
