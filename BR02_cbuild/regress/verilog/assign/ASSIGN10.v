/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Blocking assignment having part select on lhs
// Section 3.2.4 of test plan

module ASSIGN10 (in1,in2,clk,out1);
parameter a = 1,b=2,c=3,d=4;
input [a : d] in1,in2;
input clk;
output [a:d] out1;
reg [a:d] out1;

  always @(posedge clk)
  begin
    out1[a:b] = in1 [a:b] & in2[a:b];
    out1[c:d] = in1[c:d] | in2[c:d];
  end
endmodule
