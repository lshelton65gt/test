/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Procedural assignment having wires on rhs
// Section 3.2.8 of the test plan

module ASSIGN48(in1,in2,in3,in4,in5,in6,out1,out2,out3);
input in1,in2,in3,in4,in5,in6;
output out1;
reg out1;
output [3:0] out2;
reg [3:0] out2;
output [1:0] out3;
reg [1:0] out3;

    always @(in1 or in2 or in3 or in4 or in5 or in6)
    begin
      out1 = in1 | in2 ^ in3 & in4 ~^ in5 | in6;
      out2 = { {in1,in2},{in3,in4}};
      out3 = { in1 & in2,in3 | in4};
    end
endmodule


