/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Non Blocking assignment having bit-select on rhs
// Section 3.2.11 of the test plan

module ASSIGN55(in1,in2,in3,in4,out,out2);
input [3:0] in1,in2,in3,in4;
output [3:0] out,out2;
reg [3:0] out,out2;

  always @( in1 or in2 or in3 or in4 )
  begin 
    out[0] <= in1[0] & in2[0];
    out[1] <= in1[0] & in2[0];
    out[2] <= in1[0] & in2[0];
    out[3] <= in1[0] & in2[0];
    out2[0] <= in3[0] & in4[0];
    out2[1] <= in3[0] & in4[0];
    out2[2] <= in3[0] & in4[0];
    out2[3] <= in3[0] & in4[0];
  end
endmodule
