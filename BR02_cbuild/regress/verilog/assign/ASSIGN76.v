/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// partial bitselect in synchronous & asynchronous block
// section 3.2.17 of test plan

module ASSIGN76(clk, reset, enable, out1, out2);
input   clk, reset, enable;
output  [1:0] out1, out2;
reg  [1:0] out1, out2;

always @(posedge clk or posedge reset)
begin
	if (reset)
	begin
		out2[0] = 1'b1;
		out1 = 2'b01;
	end
	else 
	begin
		if (enable)
		begin
			out1[0] = 1'b1;
			out2 = out1 ;
		end
		else
			out1[0] = 1'b0;
	end
end

endmodule
