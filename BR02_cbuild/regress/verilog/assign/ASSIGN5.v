/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having function call on rhs
// Section 3.1.16 of test plan

module ASSIGN5 (in1,in2,in3,in4,out1,out2); 
input [1:0] in1,in2,in3,in4;
output [1:0] out1,out2;

function [2:0] add;
input [1:0] in1,in2;
   add = in1 + in2;
endfunction

  assign {out1,out2} = add(in1,in2) + add(in3,in4);
endmodule

