/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Procedural assignment with concatenation of complex expressions
// section 3.2.14 of test plan

module ASSIGN70(in1,in2,in3,in4,in5,in6,out1);
input [3:0] in1,in2;
input [1:0] in3,in4,in5,in6;
output [15:0] out1;
reg [15:0] out1;


always @(in1 or in2 or in3 or in4 or in5 or in6)
  begin
   out1 = { {(in1+in2),{in3,in4}}  , { {(in3^in4),(in3&in4)},{in5,in6}}};
  end
endmodule



