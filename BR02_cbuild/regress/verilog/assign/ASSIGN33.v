/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Continuous assignment having expression on RHS
// Section 3.1.19 of the test plan

module ASSIGN33(in1,in2,in3,in4,in5,out1,out2,out3,out4,out5);
input [1:0] in1,in2,in3,in4,in5;
output [3:0] out1,out2,out3,out4,out5;

  assign out1 = in1 + in2 + (~in1) + (~in2);
  assign out2 = in3 - in2 - (~in3) -(~in4); 
  assign out3 = in5 * 4;
  assign out4 = 31/4;
  assign out5 = 201/8;
endmodule
