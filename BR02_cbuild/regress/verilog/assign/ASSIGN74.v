/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// nonblocking assignment with shift operator
// section 5.1.8 of test plan

module ASSIGN74(clk, cond1, in1, out1);
input   clk, cond1, in1;
output  [3:0] out1;
reg  [3:0] out1;

always @(posedge clk)
begin
	if (cond1)
		out1 <= in1 << 3;
end

endmodule
