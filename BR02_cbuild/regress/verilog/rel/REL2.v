/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// > and >= operators
// Section 2.1.2.3 and 2.1.2.4 of test plan

module REL2(in1,in2,in3,in4,out1,out2);
input [3:0] in1,in2,in3,in4;
output out1,out2;
reg out1,out2;

  always @( in1 or in2 or in3 or in4)
  begin: block
  reg r1,r2,r3,r4;
  integer i1,i2;

   r1 = in1 > in2;
   r2 = in3[3:2] > in4[1:0];
   i1 = in1[0] > in2[3];
   i2 = in3 > in4;
   r3 = r1 > r2;
   r4 = i1 > i2;
   out1 = r3 > r4;
   r1 = in2 >= in1;
   r2 = in3[2:1] >= in4[1:0];
   i1 = in1[1] >= in2[3];
   i2 = in4 >= in3;
   r3 = r2 >= r1;
   r4 = i1 >= i2;
   out2 = r3 >= r4;
  end
endmodule



