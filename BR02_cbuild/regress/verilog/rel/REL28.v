/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// <= operator having bit_select
// Section 2.1.2.2 of test plan

module REL28 (in1,in2,in3,out1,out2,out3);
input [2:0] in1,in2,in3;
output out1,out2,out3;
  assign out1 = (in1[0] <= in2[0]) <= in3[0];
  assign out2 = (in1[1] <= in2[1]) <= in3[1];
  assign out3 = (in1[2] <= in2[2]) <= in3[2];
endmodule

