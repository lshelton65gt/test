/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// <= operator having concatenation operands
// Section 2.1.2.2 of test plan

module REL20(in1,in2,in3,in4,in5,in6,out1,out2,out3);
input in1,in2,in3,in4,in5,in6;
output out1,out2,out3;

  assign out1 = {in1,in2,in3} <= {in4,in5,in6};
  assign out2 = {{in1,in3},{in2,in4}} <= {{in3,in4,in5},in6};
  assign out3 = {in1,in2,in3,in4,in5,in6} <= {{in1,in3},{in2,in5},{in4,in6}};
endmodule
