/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// >= operator having integer operands
// Section 2.1.2.4 of test plan

module REL6(clk,out1,out2,in1,in2,out3);
input clk;
input [31:0] in1,in2;
output out1,out2,out3;
reg out1,out2,out3;
integer int1,int2,int3;
   always @(posedge clk)
   begin
      int1 = -5;
      int2 = 3;
      int3 = -6;
      out1 = int1 >= int2;
      out2 = int3 >= int1;
      int1 = in1;
      int2 = in2;
      out3 = int1 >= int2;
   end
endmodule
