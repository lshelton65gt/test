/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// < operator having register operands
// Section 2.1.2.1 of test plan

module REL7 ( clk,in1,in2,out1,out2,out3);
input clk;
input [3:0] in1,in2;
output out1,out2,out3;
reg out1,out2,out3;
reg [3:0] reg1,reg2,reg3;
  always @(posedge clk)
  begin
    reg1 = -4;
    reg2 = -2;
    reg3 = 3;
    out1 = reg1 < reg2;
    out2 = reg1  < reg3;
    reg1 = in1;
    reg2 = in2;
    out3 = reg1 < reg2;
  end
endmodule


