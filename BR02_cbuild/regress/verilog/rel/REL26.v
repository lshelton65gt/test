/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// >= operator having multiple concatenation
// Section 2.1.2.4 of test plan

module REL26 (in1,in2,in3,in4,out1,out2,out3);
input in1,in2,in3,in4;
output out1,out2,out3;

  assign out1 = {3{in1}} >= {3{in2}};
  assign out2 = {4{in3}} >= {4{in4}};
  assign out3 = {2{{in1,in2}}} >= {2{{in3,in4}}};
endmodule
