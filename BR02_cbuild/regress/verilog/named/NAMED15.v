/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Two named block having same name in nested scope
// Section 5.9.16 of the test plan

module NAMED15(in1,in2,cond1,cond2,out1);
input [7:0] in1,in2,cond1,cond2;
output [7:0] out1;
reg [7:0] out1;

  always @( cond1 or cond2 or in1 or in2)
  begin : b1
  reg [7:0] cond;
    cond = cond1 ^ cond2;
    if(cond)
    begin : b1
    integer i;
     for(i=0;i<=7;i=i+1)
     begin
       out1[i]=in1[i] ^ in2[1];
     end
    end
    else
    begin : b3
    integer i;
     for(i=0;i<=7;i=i+1)
     begin
       out1[i]=in1[i] ~^ in2[1];
     end
    end
  end
endmodule



