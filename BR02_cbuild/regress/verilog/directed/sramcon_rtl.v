/*********************************************************/
// MODULE:		SRAM/ROM Controller
//
// FILE NAME:	sramcon_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module implements a controller for an
// SRAM or ROM.
//
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.
`define WR_COUNT 1	// Number of write cycles needed
`define RD_COUNT 4	// Number of read cycles needed
`define CNT_BITS 2	// Number of bits needed for the
					// counter to count the cycles

// TOP MODULE
module sram_control(
		clock,
		reset_n,
		as_n,
		rw,
		out_en,
		write_en,
		ack);

// INPUTS
input		clock;	   	// State machine clock
input		reset_n;   	// Active low, synchronous reset
input		as_n;		// Active low address strobe
input		rw;			// Read/write command
						// = 1 to read
						// = 0 to write

// OUTPUTS
output		out_en;		// Output enable to memory
output		write_en;	// Write enable to memory
output		ack;		// Acknowledge signal to processor

// INOUTS

// SIGNAL DECLARATIONS
wire		clock;
wire		reset_n;
wire		as_n;
wire		rw;
wire		out_en;
wire		write_en;
wire		ack;

reg  [1:0]				mem_state;	// Synthesis state_machine
reg  [`CNT_BITS-1:0]	cnt;		// Cycle counter

// PARAMETERS
parameter[1:0]			// State machine states
	IDLE	= 0,
	WRITE	= 1,
	READ	= 2;

// ASSIGN STATEMENTS
						// Create the outputs from the states
assign out_en = mem_state[1];
assign write_en = mem_state[0];

						// Create the acknowledge combinatorially
assign #`DEL ack = ~as_n && ((~rw && (cnt == `WR_COUNT-1)) ||
				  	   ( rw && (cnt == `RD_COUNT-1)));

// MAIN CODE

// Look at the rising edge of clock for state transitions
always @(posedge clock or negedge reset_n) begin
	if (~reset_n) begin
		mem_state <= #`DEL IDLE;
		cnt <= #`DEL `CNT_BITS'h0;
	end
	else begin
						   	// Use parallel_case directive
						   	// to show that all states are
						   	// mutually exclusive
							// Use full_case directove
							// to show that any other states
							// are don't cares

		case (mem_state)  	// synthesis parallel_case full_case
			IDLE:	begin
				// Look for address strobe to begin the access
				if (~as_n) begin
					if (rw) begin
						// This is a read access
						mem_state <= #`DEL READ;
					end
					else begin
						// This is a write access
						mem_state <= #`DEL WRITE;
					end
				end
			end
			WRITE:	begin
				// If we have reached the final cycle count
				// for the access, the access is finished.
				// If the address strobe has been deasserted,
				// the access is aborted
				if ((cnt == `WR_COUNT-1) || as_n) begin
					mem_state <= #`DEL IDLE;
					cnt <= #`DEL `CNT_BITS'h0;
				end
				else
					cnt <= #`DEL cnt + 1;
			end
			READ:	begin
				// If we have reached the final cycle count
				// for the access, the access is finished.
				// If the address strobe has been deasserted,
				// the access is aborted
				if ((cnt == `RD_COUNT-1) || as_n) begin
					mem_state <= #`DEL IDLE;
					cnt <= #`DEL `CNT_BITS'h0;
				end
				else
					cnt <= #`DEL cnt + 1;
			end
		endcase
	end
end
endmodule		// sram_control