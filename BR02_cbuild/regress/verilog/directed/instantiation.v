module instantiation(a0, o0);

input a0;
output o0;

wire local;

assign local = a0;

child c1(local, o0);
child c2(a0, o0);

endmodule

module child(ca0, co0);

input ca0;
output co0;

gchild gc1(ca0, co0);
gchild gc2(ca0, co0);

endmodule

module gchild(gca0, gco0);

input gca0;
output gco0;

wire incr;
assign incr = gca0 + 1;

assign gco0 = incr;

endmodule
