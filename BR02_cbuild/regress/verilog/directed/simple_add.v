/* 
* Simple nwk to test the abc interface.
*/

module simple_add (aIn, bIn, cIn, op);
   input aIn,bIn, cIn;
   output op;

   wire tmp1, tmp2;
   wire abOp, Op1;
   assign abOp = aIn & bIn;
   assign Op1 = cIn ^ abOp;
   assign tmp1 = Op1 + bIn;
   assign tmp2 = tmp1 + aIn;
   assign op = tmp1 | ! tmp2;

endmodule // simple
