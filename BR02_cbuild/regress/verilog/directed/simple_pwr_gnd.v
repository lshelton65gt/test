/* 
* Simple nwk to test the abc interface.
*/

module simple_pwr_gnd (aIn, bIn, op);
   input aIn,bIn;
   output op;

   wire tmp1, tmp2, tmp3;
   assign tmp1 = aIn ?  1'b1 : bIn;
   assign tmp2 = bIn ? 1'b0 : tmp1;
   assign tmp3 = tmp1 ? tmp2 : 1'b1;
   assign op = tmp3 ? bIn : 1'b0;

endmodule // simple
