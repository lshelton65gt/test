module cutTopOrder (a, b, c, d, e, out);
    input a;  
    input b;  
    input c;  
    input d;  
    input e;
    output out;
    
   wire wire1, wire2, wire3, wire4, wire5, wire6, wire7, wire8, wire10, wire11;
   
   and wire1_inst(wire1, a, b);

   or wire2_inst(wire2, c, d);

   xor wire3_inst(wire3, wire1, d);

   and wire4_inst(wire4, wire3, wire2);

   xor wire5_inst(wire5, e, wire2);

   and wire6_inst(wire6, level21, wire5);

   or wire7_inst(wire7, wire6, wire1);

   and wire8_inst(wire8, wire4, wire6);

   xor wire9_inst(wire9, wire4, wire8);

   assign wire10 = e ? wire4 : wire7;

   assign wire11 = wire9 ? 1'b0 : wire9;

   or (out, wire10, wire11);
    
endmodule

// --- Cut volume size is : 17

//        Cut volume                                                                        | Longest path value
//                                                                                          | from cut out
// -----------------------------------------------------------------------------------------|-----
//cut volume : a_rushc_in[RushcOut] instance type : RUSHC_IO net : n16 dir : DIR_OUT LPId : | 7
//cut volume : b_rushc_in[RushcOut] instance type : RUSHC_IO net : n17 dir : DIR_OUT LPId : | 7
//cut volume : wire1_inst[o] instance type : PRIM_AND net : wire1 dir : DIR_OUT LPId :      | 6
//cut volume : d_rushc_in[RushcOut] instance type : RUSHC_IO net : n19 dir : DIR_OUT LPId : | 7
//cut volume : wire3_inst[o] instance type : PRIM_XOR net : wire3 dir : DIR_OUT LPId :      | 5
//cut volume : c_rushc_in[RushcOut] instance type : RUSHC_IO net : n18 dir : DIR_OUT LPId : | 7
//cut volume : wire2_inst[o] instance type : PRIM_OR net : wire2 dir : DIR_OUT LPId :       | 6
//cut volume : e_rushc_in[RushcOut] instance type : RUSHC_IO net : n20 dir : DIR_OUT LPId : | 6
//cut volume : wire4_inst[o] instance type : PRIM_AND net : wire4 dir : DIR_OUT LPId :      | 4
//cut volume : wire5_inst[o] instance type : PRIM_XOR net : wire5 dir : DIR_OUT LPId :      | 5
//cut volume : wire6_inst[o] instance type : PRIM_AND net : wire6 dir : DIR_OUT LPId :      | 4
//cut volume : wire7_inst[o] instance type : PRIM_OR net : wire7 dir : DIR_OUT LPId :       | 2
//cut volume : wire8_inst[o] instance type : PRIM_AND net : wire8 dir : DIR_OUT LPId :      | 3
//cut volume : wire9_inst[o] instance type : PRIM_XOR net : wire9 dir : DIR_OUT LPId :      | 2
//cut volume : i13[o] instance type : PRIM_MUX net : wire10 dir : DIR_OUT LPId :            | 1
//cut volume : i14[o] instance type : PRIM_MUX net : wire11 dir : DIR_OUT LPId :            | 1
//cut volume : i16[o] instance type : PRIM_OR net : out dir : DIR_OUT LPId :                | 0
