module Reachability2 (clk, rst, outCount1, outCount2);

input clk, rst;

reg [1 : 0] cFirst;
reg [2 : 0] cSecond;
output outCount1;
output outCount2;

always @(posedge clk or negedge rst)
begin
	if (~rst)
	  begin
	     cFirst <= 2'b0;
	     cSecond <= 3'b0;
	  end
	else
	  begin
	     cFirst <= (cFirst == 2'b11) ? 2'b00 : cFirst + 2'b01;
	     cSecond <= (cSecond == 3'b111) ? 3'b000 : cSecond + 3'b001;
	  end
end // always @ (posedge clk or rst)

assign outCount1 = cFirst;
assign outCount2 = cSecond;
   
endmodule

