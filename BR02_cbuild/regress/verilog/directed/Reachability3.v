/**
* Test for reachability.
* Unreachable state is :
* The counter cSecond value 100 assert the counter cFirst value to 111.
* So for the next cycle when cSecond value is 101, the cFirst only possible
* value is 00.
* Unreachable states: 
* cFirst[0] 0 1 1
* cFirst[1] 1 0 1
* cSecond[0] 1 1 1
* cSecond[1] 0 0 0
* cSecond[2] 1 1 1
*
* The intention of using cFirstInput input is not to have Mealy state machine boundary model and
* pass the reachability check to the solver.
*/

module Reachability3 (cFirstInput, clk, rst, outCount1, outCount2);

input clk, rst;

input [1 : 0] cFirstInput;

reg [1 : 0] cFirst;
reg [2 : 0] cSecond;
output outCount1;
output outCount2;

always @(posedge clk or negedge rst)
begin
	if (~rst)
	  begin
	     cFirst <= 2'b0;
	     cSecond <= 3'b0;
	  end
	else
	  begin
	     cFirst <= (cSecond == 3'b100) ? (cFirstInput | 2'b11) : cFirst + 2'b01;
	     //cFirst <= (cSecond == 3'b100) ? (cFirstInput | 8'b10101011) : cFirst + 1;
	     cSecond <= (cSecond == 3'b111) ? 3'b000 : cSecond + 3'b001;
	  end
end // always @ (posedge clk or rst)

assign outCount1 = cFirst;
assign outCount2 = cSecond;
   
endmodule

