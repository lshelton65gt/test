/*********************************************************/
// MODULE:		Moore state machine
//
// FILE NAME:	moore_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module shows a state machine
// implementation.
//
// NOTES: This is a Moore model of a state machine, since
// the outputs are defined by the current state.
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.

// TOP MODULE
module state_machine(
		clock,
		reset_n,
		wr,
		rd,
		ready,
		out_en,
		write_en,
		ack);

// INPUTS
input		clock;	   	// State machine clock
input		reset_n;   	// Active low, synchronous reset
input		wr;	   		// Write command from processor
input		rd;   		// Read command from processor
input		ready;		// Ready signal from memory device

// OUTPUTS
output		out_en;		// Output enable to memory
output		write_en;	// Write enable to memory
output		ack;		// Acknowledge signal to processor

// INOUTS

// SIGNAL DECLARATIONS
wire		clock;
wire		reset_n;
wire		wr;
wire		rd;
wire		ready;
reg			out_en;
reg			write_en;
reg			ack;

reg  [1:0]	mem_state;	// synthesis state_machine

// PARAMETERS
parameter[1:0]			// State machine states
	IDLE	= 0,
	WRITE	= 1,
	READ1	= 2,
	READ2	= 3;

// ASSIGN STATEMENTS

// MAIN CODE

// Look at the rising edge of clock for state transitions
always @(posedge clock or negedge reset_n) begin : fsm
	if (~reset_n)
		mem_state <= #`DEL IDLE;
	else begin
						   	// use parallel_case directive
						   	// to show that all states are
						   	// mutually exclusive

		case (mem_state)  	// synthesis parallel_case
			IDLE:	begin
				if (wr == 1'b1)
					mem_state <= #`DEL WRITE;
				else if (rd == 1'b1)
					mem_state <= #`DEL READ1;
			end
			WRITE:	begin
				mem_state <= #`DEL IDLE;
			end
			READ1:	begin
				if (ready == 1'b1)
					mem_state <= #`DEL READ2;
			end
			READ2:	begin
				mem_state <= #`DEL IDLE;
			end
		endcase
	end
end			// fsm

// Look at changes in the state to determine outputs
always @(mem_state) begin : outputs

						   	// use parallel_case directive to
						   	// to show that all states are
						   	// mutually exclusive
							// use full_case directive to
							// show that any undefineds
							// states are don't cares

	case (mem_state)		// synthesis parallel_case full_case
		IDLE:	begin
			out_en = 1'b0;
			write_en = 1'b0;
			ack = 1'b0;
		end
		WRITE:	begin
			out_en = 1'b0;
			write_en = 1'b1;
			ack = 1'b1;
		end
		READ1:	begin
			out_en = 1'b1;
			write_en = 1'b0;
			ack = 1'b0;
		end
		READ2:	begin
			out_en = 1'b1;
			write_en = 1'b0;
			ack = 1'b1;
		end
	endcase
end			// outputs
endmodule		// state_machine
