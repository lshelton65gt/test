module test_1 (a, b, op);

input a, b;
output op;

assign op = a & b;

endmodule

