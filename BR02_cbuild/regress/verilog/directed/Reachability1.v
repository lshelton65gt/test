
module Reachability1(clk, rst, ip, counter_out_0, counter_out_1, counter_out, mOutStage1, mOutStage2);
    parameter N = 1;

    input clk,rst;

    input [8 : 0] ip;

    output [N : 0] mOutStage1;
    output mOutStage2;
    output [1 : 0] counter_out;
    output counter_out_0;
    output counter_out_1;

    reg [1 : 0] counter_q;
    reg [N : 0] stage_1_q;
    reg stage_2_q;
    wire [N : 0] mux1;
    wire mux2;
   
 	     
   //the counter: asynchronous reset.
   always  @(posedge clk or negedge rst)
     begin
	if (~rst)
	  counter_q <= 2'b00;
	else
	  begin
	     counter_q <= (counter_q == 2'b11) ? 2'b00 : counter_q + 2'b01;
	  end
     end

   //the state elements
   always @(posedge clk or negedge rst)
     begin
	if (~rst)
	  begin
	     stage_1_q <= 2'b10;
	     stage_2_q <= 1'b0;
	  end
	else
	  begin
	     stage_1_q <= mux1;
	     stage_2_q <= mux2;
	  end
     end // always @ (posedge clk or rst)

   //combinational assignments
   
   //The muxes
   assign mux1 = (counter_q == 2'b00) ? ip[3] : stage_1_q;
   assign  mux2 = (counter_q == 2'b11) ? stage_1_q[0] : stage_2_q;
   //The output assignment
   assign counter_out = counter_q;
   assign counter_out_0 = counter_q[0];
   assign counter_out_1 = counter_q[1];
   assign mOutStage1 = stage_1_q;
   assign mOutStage2 = stage_2_q;

endmodule // Reachability1

module test_bench();

parameter N = 1;

reg clock, reset;
reg [8:0] ip_input;
wire [N:0] stage_1_out;
wire stage_2_out;
wire [1:0] counterQ;
wire counterQ0;
wire counterQ1;

Reachability1 u_Reachability1(clock, reset, ip_input, counterQ0, counterQ1, counterQ, stage_1_out, stage_2_out);

initial begin
    clock = 1;
    reset = 1;
end

initial
begin
    $dumpfile("Vcd");
    $dumpvars();
end

always #1 clock = ~clock;

initial
begin
    ip_input = 8'b00000000;
    #3 reset = 1'b0;
    #2 reset = 1'b1;
    #200 $finish;
end

always @(clock)
begin
    if (~clock)
        ip_input = ip_input + 1;
    $monitor("TIME=%g Reset=%b clk=%b counterQ=%b stage_1_out=%b", $time, reset, clock, counterQ, stage_1_out);
end


endmodule
