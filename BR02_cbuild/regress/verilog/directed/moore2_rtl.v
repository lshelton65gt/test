/*********************************************************/
// MODULE:		Moore state machine
//
// FILE NAME:	moore2_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module shows a state machine
// implementation. The reset signal is asynchronous and is
// active low.
//
// NOTES: This is a Moore model of a state machine, since
// the outputs are defined by the current state.  This
// implementation uses the outputs to represent the states,
// saving logic.
/*********************************************************/

// DEFINES
`define DEL	1				// Clock-to-output delay. Zero
							// time delays can be confusing
							// and sometimes cause problems.

`define MEM_STATE {out_en, write_en, ack}

// TOP MODULE
module state_machine(
		clock,
		reset_n,
		wr,
		rd,
		ready,
		out_en,
		write_en,
		ack);

// INPUTS
input		clock;	   	// State machine clock
input		reset_n;   	// Active low, synchronous reset
input		wr;	   		// Write command from processor
input		rd;   		// Read command from processor
input		ready;		// Ready signal from memory device

// OUTPUTS
output		out_en;		// Output enable to memory
output		write_en;	// Write enable to memory
output		ack;		// Acknowledge signal to processor

// INOUTS

// SIGNAL DECLARATIONS
wire		clock;
wire		reset_n;
wire		wr;
wire		rd;
wire		ready;
reg			out_en;
reg			write_en;
reg			ack;

// PARAMETERS
parameter[2:0]			// State machine states
	IDLE	= 0,
	WRITE	= 3,
	READ1	= 4,
	READ2	= 5;

// ASSIGN STATEMENTS

// MAIN CODE

// Look at the rising edge of clock for state transitions
always @(posedge clock or negedge reset_n) begin : fsm
	if (~reset_n)
		`MEM_STATE <= #`DEL IDLE;
	else begin
						   	// use parallel_case directive
						   	// to show that all states are
						   	// mutally exclusive

		case (`MEM_STATE)  	// synthesis parallel_case
			IDLE:	begin
				if (wr == 1'b1)
					`MEM_STATE <= #`DEL WRITE;
				else if (rd == 1'b1)
					`MEM_STATE <= #`DEL READ1;
			end
			WRITE:	begin
				`MEM_STATE <= #`DEL IDLE;
			end
			READ1:	begin
				if (ready == 1'b1)
					`MEM_STATE <= #`DEL READ2;
			end
			READ2:	begin
				`MEM_STATE <= #`DEL IDLE;
			end
			default: begin	// Since not all states are defined,
							// include a default state to optimize
							// synthesis
				`MEM_STATE <= 3'bxxx;
			end
		endcase
	end
end		// fsm
endmodule		// state_machine
