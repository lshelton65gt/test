/*
 Small network to test cnf generation.
 */

module cnf1(a, b, c, d, out);
   input a, b, c, d;
   output out;

   assign out = a & ((a & b) | (c & d));
   //assign out = a | c;

endmodule // cut1

   
