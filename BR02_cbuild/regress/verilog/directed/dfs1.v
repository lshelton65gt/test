module dfs1 (a, b, c, d, e, f,g,h,clk, rst, op);   // cut1.v(5)
    input a;  
    input b;  
    input c;  
    input d;  
    input e;
   input  f;
   input  g;
   input  h;
    input clk;
    input rst;
    output op;
    
   wire    level1,level2,level3,level4,level51,level52,level6,level7;
   reg 	   level8_q;
   
   
   and level1_inst(level1,a,b);
   xor level2_inst(level2,level1,c);
   and level3_inst(level3,level2,d);
   xor level4_inst(level4,level3,e);
   and level51_inst(level51,level4,f);
   nand level52_inst(level52,g,h);
   xor level6_inst(level6,level51,level52);
   
   always  @(posedge clk or negedge rst)
     begin
	if (~rst)
	  level8_q <= 1'b0;
	else
	  level8_q <= level6;
     end
   
   and (op,level8_q,level52);
    
endmodule
