/*
 Small sat sweep example: 
 Common logic (a&b) should be found...
 Constants should be propagated (f should reduce to 1).
 */

module top (a,b,c,d,e,op1,op2);
   input a,b,c,d,e;
   output op1,op2;

   wire   or1 = ~a | ~b;
   wire   and1 = a& b;
   wire   and2 = ~(~b & and1); //and2 should reduce to 1
   //[reconvergent fanout ]
   wire   f = and2 | c;      //f should be constant 1.
   wire   g = ~(or1);       //g should match and1, after DeMorgan
   assign op2 = g ^ and2; //op2 should reduce to g/.
   

   
endmodule // top

   
	  
