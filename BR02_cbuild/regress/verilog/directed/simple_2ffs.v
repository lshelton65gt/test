/* 
* Simple nwk to test the abc interface.
*/

module simple_ff (clk, rst, aIn, bIn, op1, op2, op3);
    input clk, rst;
    input aIn, bIn;
    output op1, op2, op3;

    wire Tmp1;
    reg RegFirst;
    reg RegSecond;
    
    assign Tmp1 = aIn & bIn;
    
    always  @(posedge clk or negedge rst)
        begin
            if (~rst)
                begin
                RegFirst <= 1'b0;
                RegSecond <= 1'b0;
                end
            else
                begin
                RegFirst <= Tmp1;
                RegSecond <= Tmp1;
                end
        end

    
    assign op1 = RegFirst | aIn;
    assign op2 = Tmp1;
    assign op3 = RegSecond;

endmodule // simple_ff
