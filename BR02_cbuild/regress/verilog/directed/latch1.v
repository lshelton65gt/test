/* 
* Simple nwk to test the abc interface.
*/

module latch1 (d, q);

input d;
output q;

reg q_int;

always  @(d)
begin
    q_int <= d;
end

assign q = q_int;

endmodule // simple_ff
