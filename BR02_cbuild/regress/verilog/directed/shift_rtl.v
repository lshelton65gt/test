/*********************************************************/
// MODULE:		barrel shifter
//
// FILE NAME:	shift_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module defines an 8-bit barrel
// shifter.
//
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.

// TOP MODULE
module Shifter(
		clk,
		load,
		rshift,
		lshift,
		shiftnum,
		inbit,
		in,
		out);

// INPUTS
input			clk;		// Clock
input			load;		// Synchronous load
input			rshift;		// Synchronous right shift control
input			lshift;		// Synchronous right shift control
input [2:0]		shiftnum;  	// Number of bit places to shift
input			inbit;	   	// Bit to shift into empty places
input [7:0]		in;			// input word

// OUTPUTS
output [7:0]	out;		// output word

// INOUTS

// SIGNAL DECLARATIONS
wire			clk;
wire			load;
wire			rshift;
wire			lshift;
wire [2:0]		shiftnum;
wire			inbit;
wire [7:0]		in;
reg  [7:0]		out;

// PARAMETERS

// ASSIGN STATEMENTS

// MAIN CODE

// Look at the rising edge of the clock
always @(posedge clk) begin
	if (load) begin
		// In this implementation, load has highest priority
		out <= #`DEL in;
	end
	else if (rshift) begin
		// In this implementation, rshift has priority
		// over lshift
		case (shiftnum)
			3'h0: out <= #`DEL out;
			3'h1: out <= #`DEL {inbit,out[7:1]};
			3'h2: out <= #`DEL {inbit,inbit,out[7:2]};
			3'h3: out <= #`DEL {inbit,inbit,inbit,out[7:3]};
			3'h4: out <= #`DEL {inbit,inbit,inbit,inbit,
							out[7:4]};
			3'h5: out <= #`DEL {inbit,inbit,inbit,inbit,
							inbit,out[7:5]};
			3'h6: out <= #`DEL {inbit,inbit,inbit,inbit,
							inbit,inbit,out[7:6]};
			3'h7: out <= #`DEL {inbit,inbit,inbit,inbit,
							inbit,inbit,inbit,out[7]};
		endcase
	end
	else if (lshift) begin
		// In this implementation, lshift has lowest priority
		case (shiftnum)
			3'h0: out <= #`DEL out;
			3'h1: out <= #`DEL {out[6:0],inbit};
			3'h2: out <= #`DEL {out[5:0],inbit,inbit};
			3'h3: out <= #`DEL {out[4:0],inbit,inbit,
							inbit};
			3'h4: out <= #`DEL {out[3:0],inbit,inbit,
							inbit,inbit};
			3'h5: out <= #`DEL {out[2:0],inbit,inbit,
							inbit,inbit,inbit};
			3'h6: out <= #`DEL {out[1:0],inbit,inbit,
							inbit,inbit,inbit,inbit};
			3'h7: out <= #`DEL {out[0],inbit,inbit,inbit,
							inbit,inbit,inbit,inbit};
		endcase
	end
end
endmodule		// Shifter
