// filename: test_2.v
// Description:  This is simple sequentianl case


module test_2(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule
