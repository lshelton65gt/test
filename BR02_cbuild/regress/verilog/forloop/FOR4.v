/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

//  for loop having > operator 
// Section 5.4.6 of the test plan

module FOR4(in1,in2,out1,out2);
input [7:0] in1,in2;
output [7:0] out1,out2;
reg [7:0] out1,out2;

  always @(in1 or in2)
  begin : block
    integer i;
    for(i=7;i>-1;i=i-1)
    begin
      out1[i] = in1[i] & in2[i];
      out2[i] = in1[i] | in2[i];
    end
  end
endmodule
