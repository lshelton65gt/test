/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// For loop having register index
//section 5.4.2 of test plan

module FOR1 (in1,in2,out1,out2);
input [7:0] in1,in2;
output [7:0] out1,out2;
reg [7:0] out1,out2;
reg [3:0] i;


  always @(in1 or in2)
  begin : block
    out1 = in1;
    out2 = in2;
    for (i=0;i<4;i = i + 1)
    begin
      out1[i] = 1'b1;
      out2[i] = 1'b0;
    end
  end
endmodule

    
