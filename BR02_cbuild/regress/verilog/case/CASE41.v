/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case statement having for loop
// Section 5.3.29 of the test plan

module CASE41(cond,in1,in2,out);
input [1:0] cond;
input [7:0] in1,in2;
output [7:0] out;
reg [7:0] out;

  always @( cond or in1 or in2)
  begin : block
  integer i;

    case(cond)
      2'b00 : for(i=0;i<=7;i=i+1)
                 out[i]=in1[i] & in2[i];
      2'b01 : for(i=0;i<=7;i=i+1)
                 out[i]=in1[i] | in2[i];
      2'b10 : for(i=0;i<=7;i=i+1)
                 out[i]=in1[i] ^ in2[i];
      2'b11 : for(i=0;i<=7;i=i+1)
                 out[i]=in1[i] ~^ in2[i];
   endcase
  end
endmodule
