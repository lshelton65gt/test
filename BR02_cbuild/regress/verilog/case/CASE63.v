/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression variable and case items variable
// Section 5.3.38 of the test plan

module CASE63(cond1,cond2,i1,i2,i3,i4,in1,in2,out1);
input [3:0] cond1,cond2,i1,i2,i3,i4,in1,in2;
output [3:0] out1;
reg [3:0] out1;

  always @(cond1 or cond2 or i1 or i2 or i3 or i4 or in1 or in2)
  begin
    case(cond1)
      i1 : case(cond2)
            i1 : out1 = in1 ;
            i2 : out1 = in2;
            i3 : out1 = in1 | in2;
            i4 : out1 = in1 << 2;
            default : out1 = in2<<2;
           endcase

      i2 : case(cond2)
            i1 : out1 = ~in1 ;
            i2 : out1 = ~in2;
            i3 : out1 = ~in1 | in2;
            i4 : out1 = ~in1 << 2;
            default : out1 = ~in2<<2;
           endcase

      i3 : case(cond2)
            i1 : out1 = in1 & in2;
            i2 : out1 = in2 ^ in1;
            i3 : out1 = ~in1 ~^ in2;
            i4 : out1 = in1 << 2;
            default : out1 = in2<<2;
           endcase

      i4 : case(cond2)
            i1 : out1 = in1 >>2;
            i2 : out1 = in2>>2;
            i3 : out1 = {&in1,&in2,~&in1,~&in2};
            i4 : out1 = {^in1,^in2,~^in1,~^in2};
            default : out1 = in2<<2;
           endcase
     default : case(cond2)
            i1 : out1 = {in1<in2,in1>in2,in1!=in2,!in1} ;
            i2 : out1 = in2;
            i3 : out1 = in1 | in2;
            i4 : out1 = in1 << 2;
            default : out1 = in2<<2;
           endcase
    endcase 
   end
endmodule

