/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case items having parameters
// Section 5.3.12 of the test plan

module CASE30(cond,in1,in2,out1);
input [1:0] in1,in2,cond;
output [1:0] out1;
reg [1:0] out1;
parameter p0=0,p1=1,p2=2,p3=3;

  always @(in1 or in2 or cond)
  begin
    case(cond)
     p0 : out1 = in1 & in2;
     p1 : out1 = in1 | in2;
     p2 : out1 = in1 ~^ in2; 
     p3 : out1 = in1 ^ in2;
   endcase
  end
endmodule
 
