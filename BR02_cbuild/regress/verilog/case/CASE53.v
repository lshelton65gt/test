/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case items having expression
// Section 5.3.43 of the test plan

module CASE53(in1,in2,out1);
input [1:0] in1,in2;
output [2:0] out1;
reg [2:0] out1;

  always @( in1 or in2)
  begin
    case(1'b1)
      in1 > in2 : out1 =1;
      in1 >= 2 : out1 =2;
      in1  < in2 : out1 = 3;
      in1 <= 3   : out1 =4;
      in2 == in2 : out1 =5;
      in1 !=in2  : out1 = 6;
      default : out1 = 7;
    endcase
  end
endmodule

