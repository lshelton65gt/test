/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// Bitwise negation in a case statement
// Section 5.3.46 of the test plan
        module CASE73(in1,in2,in3,in4,out);
        input in1, in2;
        input in3,in4;
        output out;
        reg out;
        always @(in1 or in3 or in4)
            case(1)
                in1: out = in3;
                ~in1: out = in4;
                default: out = 1'b0;
            endcase
        endmodule
