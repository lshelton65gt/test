/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case Expression having bit_select
// Section 5.3.3 of the test plan

module CASE55(in1,in2,in3,in4,out1);
input [3:0] in1,in2,in3;
input [1:0] in4;
output [3:0] out1;
reg [3:0] out1;

  always @(in1 or in2 or in3 or in4)
  begin 
    case(in1[in4])
      1'b1 : out1 = in2;
      1'b0 : out1 = in3;
    endcase
  end
endmodule

    
