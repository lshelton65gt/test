/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression having function call
// Section 5.3.6 of test plan 

module CASE4(in1,in2,out1);
input [1:0] in1,in2;
output [1:0] out1;
reg [1:0] out1;

function max ;
input [1:0] in1,in2;
  max = in1 >= in2 ? in1 : in2;
endfunction

  always @(in1 or in2)
  begin : block
    reg [1:0] t1,t2;
    
     t1 = in1;
     t2 = in2;
     case (max(in1,in2))
      1'b1 : out1 = t1;
      1'b0 : out1 = t2;
     endcase
  end
endmodule






