/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case statement without default
// Section 5.3.23 of the test plan

module CASE37(cond,in1,in2,out1);
input [1:0] in1,in2,cond;
output [1:0] out1;
reg [1:0] out1;

  always @(in1 or in2 or cond)
  begin
    case(cond)
     2'b00 : out1 = in1 & in2;
     2'b01 : out1 = in1 | in2;
     2'b10 : out1 = in1 ~^ in2; 
     2'b11 : out1 = in1 ^ in2;
   endcase
  end
endmodule
 
