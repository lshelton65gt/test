/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression having exoression
// Section 5.3.7 of the test plan

module CASE22(cond1,cond2,cond3,cond4,in1,in2,out1);
input [1:0] cond1,cond2,cond3,cond4,in1,in2;
output [1:0] out1;
reg [1:0] out1;

   always @(cond1 or cond2 or cond3 or cond4 or in1 or in2)
   begin
     case(((&cond1 <( ~&cond2)) >(|cond3 >=(~|cond4))) <= ((^cond1== 2'b10) !=1'b1))
       1'b1 : out1 = in1;
       1'b0 : out1 = in2;
     endcase
  end
endmodule
