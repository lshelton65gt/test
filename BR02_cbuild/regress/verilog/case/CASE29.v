/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case items having hex numbers
// Section 5.3.11 of the test plan

module CASE29(cond,in1,in2,cond1,in3,in4,out2,out1);
input [3:0] in1,in2,cond,cond1,in3,in4;
output [3:0] out1,out2;
reg [3:0] out1,out2;

  always @(in1 or in2 or cond or cond1 or in3 or in4)
  begin
    case(cond)
     4'h0,4'h1,4'h2,4'h3 : out1 = in1 & in2;
     4'h4,4'h5,4'h6,4'h7 : out1 = in1 | in2;
     4'h8,4'hb,4'h9,4'ha : out1 = in1 ~^ in2; 
     4'hf,4'hc,4'hd,4'he : out1 = in1 ^ in2;
   endcase
    case(cond1)
     4'h0,4'h1,4'h2,4'h3 : out2 = in3 & in4;
     4'h4,4'h5,4'h6,4'h7 : out2 = in3 | in4;
     4'h8,4'hb,4'h9,4'ha : out2 = in3 ~^ in4; 
     4'hf,4'hc,4'hd,4'he : out2 = in3 ^ in4;
   endcase

  end
endmodule
 
