/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case item having bit_select
// Section 5.3.16 of test plan

module CASE5 (in1,in2,cond1,out1);
input [1:0]  in1,in2;
input [1:0] cond1;
output [2:0] out1;
reg [2:0] out1;
  always @( in1 or in2 or cond1)
  begin
    out1 = 0;
    if(cond1==2'b11)
       out1 = 3'b111;
    else
    case(1'b1)
      cond1[0] : out1 = in1 + in2;
      cond1[1] : out1 = in1 - in2;
      default : out1  = in1 + 1;
    endcase
  end
endmodule
                   

