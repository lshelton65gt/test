/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case statement having while loop
// Section 5.3.30 of Test Plan

module CASE70(clk,in1,in2,cond1,cond2,out1);
input clk;
input [7:0] in1,in2,cond1,cond2;
output [7:0] out1;
reg [7:0] out1;
integer i;

  always begin 
  @(posedge clk)
  begin : block
    out1 =0;
   case (cond1 <=  cond2)
   0: 
    begin
      i=0;
      while(i<=7)
      begin
       @(posedge clk);
       out1[i]=in1[i] | in2[i];
       i=i+1;
      end
    end
  1: 
    begin
     i=8;
     while(i >0)
     begin
     @(posedge clk);
      out1[i-1] = in1[i-1] ^ in2[i-1];
      i=i-1;
    end
  end
  endcase
 end
 end
endmodule


