/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case itemp of size smaller than case expression
// Section 5.3.37 of the test plan

module CASE62(in1,in2,cond,out1);
input [3:0] in1,in2,cond;
output [3:0] out1;
reg [3:0] out1;

   always @(in1 or in2 or cond)
   begin
     case(cond)
       2'b00 : out1 = in1 & in2;
       2'b01 : out1 = in1 | in2;
       2'b10 : out1 = in1 ^ in2;
       2'b11 : out1 = in1 ~^ in2;
     endcase
  end
endmodule
