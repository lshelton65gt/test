/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// case expression having part-select
// Section 5.3.4 of test plan

module CASE2 (cond1,cond2,out1);
input [2:0] cond1,cond2;
output [11:0] out1;
reg [11:0] out1;

 always @(cond1 or cond2)
 begin
   out1 = 0;
   case ( cond1[2:1] )
     2'b00 : case(cond2[1:0])
               2'b00 : out1 = 12'b0101010101 | 12'habc;
               2'b10 : out1 = 12'd34567 & 12'o4563;
               2'b11 : out1 = 12'hfed ^ 12'o7162;
               2'b01 : out1 = 12'd12345 ~^ 12'h123;
             endcase
     2'b10 : case(cond2[2:1])
               2'b00 : out1 = 12'b111111000000 << 2;
               2'b01 : out1 = 12'd123456 << 2;
               2'b11 : out1 = 12'o7654 >> 2;
               2'b10 : out1 = 12'hfda >> 2;
            endcase
     2'b11 : case (cond2[1:0] & cond2[2:1])
              2'b00 : out1 = 12'b000000111111 + 12'd12345;
              2'b01 : out1 = 12'o1234 - 12'h123;
              2'b11 : out1 = 12'hfed /2'b10;
              2'b10 : out1 = 12'o3456 % 2;
            endcase
     2'b01 : out1 = 12'hfff;
   endcase
 end
endmodule

