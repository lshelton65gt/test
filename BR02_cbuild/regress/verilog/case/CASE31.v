/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case item having wires
// Section 5.3.13 of the test plan

module CASE31(cond,in1,in2,in3,in4,out1);
input [1:0] cond,in1,in2,in3,in4;
output [2:0] out1;
reg [2:0] out1;

  always @( cond or in1 or in2 or in3 or in4)
  begin
    case(cond)
      in1 : out1 = 1;
      in2 : out1 = 2;
      in3 : out1 = 3;
      in4 : out1 = 4;
      default : out1 = 7;
    endcase
  end
endmodule
