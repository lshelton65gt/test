/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// case statement having task statement
// Section 5.3.32 of the test plan

module CASE44(cond,in1,in2,in3,in4,out1,out2);
input [1:0] in1,in2,in3,in4,cond;
output [1:0] out1,out2;
reg [1:0] out1,out2;

task task1;
input [1:0] in1,in2;
output [1:0] out1;
  out1 = in1 ^ in2;
endtask

task task2;
input [1:0] in1,in2;
output [1:0] out1;
  out1 = in1 ~^ in2;
endtask

    always @( cond or in1 or in2 or in3 or in4)
    begin
      case(cond)
       2'b00 : begin
                 task1(in1,in2,out1);
                 task2(in1,in2,out2);
               end
      2'b01 : begin
                 task1(in3,in4,out1);
                 task2(in3,in4,out2);
              end
       2'b10 : begin
                 task1(~in1,~in2,out1);
                 task2(~in1,~in2,out2);
               end
      2'b11 : begin
                 task1(~in3,~in4,out1);
                 task2(~in3,~in4,out2);
              end
       endcase
    end
endmodule


