/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression having wires
// Section 5.3.7 of the test plan

module CASE25(cond,in1,in2,out1);
input [31:0] in1,in2,cond;
output [31:0] out1;
reg [31:0] out1;

  always @(in1 or in2 or cond)
  begin: block
    integer i;
    i = cond;
    case(i)
     10,20,30,40,50,60,70,80,90 : out1 = in1 & in2;
     100,200,300,400,500,600,700,800,900 : out1 = in1 | in2;
     1000,2000,3000,4000,5000,6000,7000,8000,9000 : out1 = in1 ~^ in2; 
     default : out1 = in1 ^ in2;
   endcase
  end
endmodule
 
