/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case item having part select
// Section 5.3.17 of test plan

module CASE6 (in1,in2,cond1,out1);
input [1:0] in1,in2;
input [3:0] cond1;
output [1:0] out1;
reg [1:0] out1;
parameter p1 = 2'b10;

  always @ (in1 or in2 or cond1)
  begin
    out1 =0;
    case (p1)
     cond1[3:2] : out1 = in1 & in2; 
     cond1[2:1] : out1 = in1 | in2;
     cond1[1:0] : out1 = in1 ^ in2;
     default    : out1 = 2'b11;
   endcase
  end
endmodule


