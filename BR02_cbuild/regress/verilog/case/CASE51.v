/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case items having expression
// Section 5.3.43 of the test plan

module CASE51(in1,in2,cond,out1);
input [1:0] cond;
input [1:0] in1,in2;
output [1:0] out1;
reg [1:0] out1;

  always @(cond or in1 or in2)
  begin
    case(cond)
      in1 << 1 : out1 =1;
       in2 >>1 : out1 =2;
      default : out1 = 3;
    endcase
  end
endmodule

