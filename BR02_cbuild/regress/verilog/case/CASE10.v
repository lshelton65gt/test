/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Casez statement
// Section 5.3.26 of test plan

module CASE10 (in1,in2,cond,out1);
input [1:0] in1,in2;
input [3:0] cond;
output [1:0] out1;
reg [1:0] out1;
 
  always @( in1 or in2 or cond)
  begin
    out1 =0;
    casez(cond)
     4'b111? : out1 = in1;
     4'b10z1 : out1 = in2;
     4'b0z0z : out1 = in1 & in2;
     4'b1z?z : out1 = in1 | in2;
     default : out1 = in1 ^ in2;
    endcase
  end
endmodule
