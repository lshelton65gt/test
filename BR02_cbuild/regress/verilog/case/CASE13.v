/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression having binary numbers
// Section 5.3.1 of the test plan

module CASE13(cond,in1,in2,in3,in4,out1);
input [1:0] in1,in2,in3,in4,cond;
output [1:0] out1;
reg [1:0] out1;

  always @(cond or in1 or in2 or in3 or in4)
  begin
    case(2'b10)
      in1 : out1 = 2'b01;
      in2 : out1 = 2'b10;
      in3 : out1 = 2'b11;
      in4 : out1 = 2'b00;
      default : out1 =2'b00;
    endcase
  end
endmodule
