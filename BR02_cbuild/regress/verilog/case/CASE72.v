/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// casex statement with byte alignment 
// Section 5.3.25.1 of the test plan
        module CASE72 (clk,in1,out1,out2);
        input clk;
        input [84:17] in1;
        output [1:0] out2;
        reg [1:0] out2;
        output [55:0] out1;
        reg [55:0] out1;
           always @(posedge clk)
             begin
                out2       <= 2'b00; 
             end
           always @(in1 or out2)
             begin
                casex(out2) 
                  0: out1 = 56'h40000000000000  / in1[80:52];
                  default: out1 = 56'h00000000000000;
                endcase 
             end 
        endmodule
