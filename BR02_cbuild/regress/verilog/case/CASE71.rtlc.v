module CASE71(in1, out1, in2);
input	in1;
output	out1;
input	in2;
supply0	VSS;
supply1	VCC;
wire	rtlc_N1;
wire	rtlc_G6;
wire	rtlc_N7;

	M_RTL_MUX_2_2	rtlc_I5 (.Z(rtlc_G6), .in1({VSS, VCC}), .sel1({rtlc_N1, in2}));
	INT_NOT	rtlc_I8 (.Z(rtlc_N7), .A(rtlc_G6));
	INT_FDPCE	rtlc_I9 (.Q(out1), .D(VSS), .CP(in1), .PRE(VSS), .CLR(VSS), .EN(rtlc_N7));
	INT_NOT	rtlc_I10 (.Z(rtlc_N1), .A(in2));
endmodule

module M_RTL_MUX_2_2(Z, in1, sel1);
output	Z;
input	[1:0]	in1;
input	[1:0]	sel1;
wire	and_0out;
wire	and_1out;

	INT_AND2	rtlc_I11 (.Z(and_0out), .in1(in1[0]), .in2(sel1[0]));
	INT_AND2	rtlc_I12 (.Z(and_1out), .in1(in1[1]), .in2(sel1[1]));
	INT_OR2	rtlc_I13 (.Z(Z), .in1(and_0out), .in2(and_1out));
endmodule

