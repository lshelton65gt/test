/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// case expression having bit_select
// Section 5.3.3 of test plan 

module CASE1 (cond1,cond2,out1);
input [1:0] cond1,cond2;
output [11:0] out1;
reg [11:0] out1;
  always @( cond1 or cond2)
  begin
    out1 = 0;
    case(cond1[0])
      1'b1 : case (cond2[1])
               1'b1 : out1 =12'b101010101010; 
               1'b0 : out1 = 12'd101010;
            endcase
      1'b0 : case(cond2[0])
               1'b1 : out1=12'hfed;
               1'b0 : out1=12'o7654;
             endcase
    endcase
 end
endmodule
    


