/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Completely specified case statement with no statements in one branch
// Section 5.3.45 of Test Plan

module CASE71 (in1, out1, in2);
input in1;
output out1;
input in2;
reg out1;
always @(posedge in1) begin
        case (in2)
              1'b0: out1 <= 0;
              1'b1: begin
                    end 
            default: out1 <= 0;
        endcase
    end
endmodule
