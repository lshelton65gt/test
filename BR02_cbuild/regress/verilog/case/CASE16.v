/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression having parameters
// Section 5.3.2 of the test plan

module CASE16(cond,in1,in2,in3,in4,out1,out2);
input [2:0] in1,in2,in3,in4,cond;
output [2:0] out1,out2;
reg [2:0] out1,out2;
parameter p1  = 3'd2,p2=3'o7;

  always @(cond or in1 or in2 or in3 or in4)
  begin
    case(p1)
      in1 : out1 = 1;
      in2 : out1 = 2;
      in3 : out1 = 3;
      in4 : out1 = 4;
      default : out1 =5;
    endcase
    case(p2)
      in1 : out2 = 1;
      in2 : out2 = 2;
      in3 : out2 = 3;
      in4 : out2 = 4;
      default : out2 =5;
    endcase
  end
endmodule
