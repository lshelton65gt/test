/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case having only default
// Section 5.3.41 of the test plan

module CASE66(cond1,cond2,cond3,in1,in2,in3,out1,out2,out3);
input [3:0] cond1,cond2,cond3,in1,in2,in3;
output [3:0] out1,out2,out3;
reg [3:0] out1,out2,out3;

  always @(cond1 or cond2 or cond3 or in1 or in2 or in3)
  begin
    case(cond1)
      default : out1 = in1;
    endcase

    casex(cond2)
      default : out2 = in2;
    endcase

    casez(cond3)
      default : out3 = in3;
    endcase
 end
endmodule
