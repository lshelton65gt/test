/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression having register
// Section 5.3.9 of the test plan

module CASE24(cond,cond1,in1,in2,out1);
input [1:0] in1,in2,cond,cond1;
output [1:0] out1;
reg [1:0] out1;

  always @(in1 or in2 or cond or cond1)
  begin: block
  reg [1:0] cond2;
    cond2 = cond | cond1;
    case(cond2)
     2'b00 : out1 = in1 & in2;
     2'b01 : out1 = in1 | in2;
     2'b10 : out1 = in1 ~^ in2; 
     2'b11 : out1 = in1 ^ in2;
   endcase
  end
endmodule
 
