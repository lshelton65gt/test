/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case stament having forever statment, disable statement and named block
// Sections 5.3.31,5.3.33,5.3.34  of the test plan

module CASE43(cond,in1,in2,clk,out1);
input clk;
input [1:0] cond;
input [3:0] in1,in2;
output [3:0] out1;
reg [3:0] out1;

  always begin
	@(posedge clk);
	   out1 = 0;
       case (cond)
         0 : begin : b1
             forever
             begin
	           @(posedge clk);
               if(in1 > 3) disable b1;
               out1 = in1 & in2;
             end
             end
         1: begin : b2
             forever
             begin
	          @(posedge clk);
               if(in1 > 3) disable b2;
               out1 = in1 | in2;
             end
             end
         2 : begin : b3
             forever
             begin
	       @(posedge clk);
               if(in1 > 3) disable b3;
               out1 = in1 ^ in2;
             end
             end

         3 : begin : b4
             forever
             begin
	       @(posedge clk);
               if(in1 > 3) disable b4;
               out1 = in1 ~^ in2;
             end
             end
       endcase
  end
endmodule


