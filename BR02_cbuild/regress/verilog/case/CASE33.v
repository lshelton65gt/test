/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case item having integer
// Section 5.3.15 of the test plan

module CASE33(cond,in1,in2,in3,in4,out1);
input [31:0] cond,in1,in2,in3,in4;
output [31:0] out1;
reg [31:0] out1;

  always @( cond or in1 or in2 or in3 or in4)
  begin: block
  integer int1,int2,int3,int4;
  int1 = in1 + in2;
  int2 = in3 - in4;
  int3 = in1 * 2;
  int4 = in4;
    case(cond)
      int1 : out1 = 1;
      int2 : out1 = 2;
      int3 : out1 = 3;
      int4 : out1 = 4;
      default : out1 = 7;
    endcase
  end
endmodule
