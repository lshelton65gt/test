/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// case items having bit_select
// Section 5.3.16 of the test plan

module CASE58(in1,in2,in3,in4,in5,out1);
input [3:0] in1,in2,in3,in4; 
input [1:0] in5;
output [3:0] out1;
reg [3:0] out1;

   always @( in1 or in2 or in3 or in4 or in5)
   begin
     case(1'b1)
       in1[in5] : out1 = in3;
       in2[in5] : out1 = in4;
       default : out1 = in3 | in4;
     endcase
   end
endmodule
