/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// case items having bit_select
// Section 5.3.16 of the test plan

module CASE60(in1,in2,in3,in4,in5,out1,out2);
input [3:0] in1,in2,in3,in4; 
input [1:0] in5;
output [3:0] out1,out2;
reg [3:0] out1,out2;

   always @( in1 or in2 or in3 or in4 or in5)
   begin

     case(2'b11)
      in1[1:0] : out2 = in3 & in4;
      in2[1:0] : out2 = in3 ^ in4;
      default  : out2 = in3 | in4;
    endcase

     case(1'b1)
       in1[in5] : out1 = in3;
       in2[in5] : out1 = in4;
       default : out1 = 4'b1100;
     endcase
   end
endmodule
