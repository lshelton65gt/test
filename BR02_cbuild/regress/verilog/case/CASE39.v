/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case statement having if statement
//Section 5.3.27 of the test plan

module CASE39 (cond,cond1,cond2,cond3,out1,out2);
input [1:0] cond1,cond2,cond3,cond;
output [3:0] out1,out2;
reg [3:0] out1,out2;

  always @( cond or cond1 or cond2 or cond3)
  begin
    case(cond)
     0 : if(cond1)
            if(cond2)
              if(cond3)
               begin               
                  out1 = 1;
                  out2  = 2;
               end
              else
              begin
                out1 = 2;
                out2 =3;
              end
            else
             begin
               out1=3;
               out2 =4;
             end
        else
         if(cond3)
           if(cond2)
            begin
              out1 = 4;
              out2 = 5;
            end
           else
           begin
             out1 = 5;
             out2 = 6;
           end
        else
        begin
          out1 = 6;
          out2 =7;
        end
     1: if(cond1)
            if(cond2)
              if(cond3)
               begin               
                  out1 = 1;
                  out2  = 2;
               end
              else
              begin
                out1 = 2;
                out2 =3;
              end
            else
             begin
               out1=3;
               out2 =4;
             end
        else
         if(cond3)
           if(cond2)
            begin
              out1 = 4;
              out2 = 5;
            end
           else
           begin
             out1 = 5;
             out2 = 6;
           end
        else
        begin
          out1 = 6;
          out2 =7;
        end
     2 : if(cond1)
            if(cond2)
              if(cond3)
               begin               
                  out1 = 1;
                  out2  = 2;
               end
              else
              begin
                out1 = 2;
                out2 =3;
              end
            else
             begin
               out1=3;
               out2 =4;
             end
        else
         if(cond3)
           if(cond2)
            begin
              out1 = 4;
              out2 = 5;
            end
           else
           begin
             out1 = 5;
             out2 = 6;
           end
        else
        begin
          out1 = 6;
          out2 =7;
        end

    default : begin
               out1 = 15;
               out2 = 15;
              end
   endcase
  end
endmodule

