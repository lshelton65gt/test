/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Nested case
// Section 5.3.28 of the test plan

module CASE40(cond1,cond2,out1);
input [1:0] cond1,cond2;
output [3:0] out1;
reg [3:0] out1;

  always @(cond1 or cond2)
  begin
    case(cond1)
     2'b00 : case(cond2)
               2'b00 : out1 =0;    
               2'b01 : out1 =1;
               2'b10 : out1 =2;
               2'b11 : out1 =3;
             endcase
     2'b01 : case(cond2)
               2'b00 : out1 =4;    
               2'b01 : out1 =5;
               2'b10 : out1 =6;
               2'b11 : out1 =7;
             endcase
     2'b10 : case(cond2)
               2'b00 : out1 =8;    
               2'b01 : out1 =9;
               2'b10 : out1 =10;
               2'b11 : out1 =11;
             endcase
     2'b11 : case(cond2)
               2'b00 : out1 =12;    
               2'b01 : out1 =13;
               2'b10 : out1 =14;
               2'b11 : out1 =15;
             endcase
    endcase
  end
endmodule



