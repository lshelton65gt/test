/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression having concatenation
// Section 5.3.18 of test plan

module CASE7(in1,in2,cond,out1);
input [1:0] in1,in2,cond;
output [1:0] out1;
reg [1:0] out1;
parameter p1 = 1'b1,p0=1'b0;

 always @ (in1 or in2 or cond)
 begin
   out1 = 0;
   case(cond)
    {p0,p0} : out1 = in1 + 1;
    {p0,p1} : out1 = in1 - 1;
    {p1,p0} : out1 = in2 + 1;
    {p1,p1} : out1 = in2 -1;
   endcase
 end
endmodule
