/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case item having registers
// Section 5.3.14 of the test plan

module CASE32(cond,in1,in2,in3,in4,out1);
input [1:0] cond,in1,in2,in3,in4;
output [2:0] out1;
reg [2:0] out1;

  always @( cond or in1 or in2 or in3 or in4)
  begin: block
  reg [1:0] reg1,reg2,reg3,reg4;
  reg1 = ~in1;reg2=~in2;reg3=~in3;reg4=~in4;
    case(cond)
      reg1 : out1 = 1;
      reg2 : out1 = 2;
      reg3 : out1 = 3;
      reg4: out1 = 4;
      default : out1 = 7;
    endcase
  end
endmodule
