/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case items having octal numbers
// Section 5.3.11 of the test plan

module CASE28(cond,in1,in2,cond1,in3,in4,out2,out1);
input [2:0] in1,in2,cond,in3,in4,cond1;
output [2:0] out1,out2;
reg [2:0] out1,out2;

  always @(in1 or in2 or cond or in3 or in4 or cond1)
  begin
    case(cond)
    3'o1,3'o0 : out1 = in1 & in2;
     3'o2,3'o3 : out1 = in1 | in2;
     3'o4,3'o5 : out1 = in1 ~^ in2; 
     3'o6,3'o7 : out1 = in1 ^ in2;
   endcase
    case(cond1)
    3'o1,3'o0 : out2 = in3 & in4;
     3'o2,3'o3 : out2 = in3 | in4;
     3'o4,3'o5 : out2 = in3 ~^ in4; 
     3'o6,3'o7 : out2 = in3 ^ in4;
   endcase

  end
endmodule
 
