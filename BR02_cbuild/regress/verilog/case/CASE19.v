/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expresiion having complex expression
// Section 5.3.7 of the test plan 

module CASE19(cond1,cond2,cond3,cond4,in1,in2,in3,in4,out1);
input [3:0] cond1,cond2,cond3,cond4,in1,in2,in3,in4;
output [3:0] out1;
reg [3:0] out1;

  always @(cond1 or cond2 or cond3 or cond4 or in1 or in2 or in3 or in4)
  begin
    case ((cond1 << 2) | ( cond3>>2) ~^((cond2 &cond4)<<2'b11))
       0,1,2,3,4 : out1 = in1;
       5,6,7,8,9 : out1 = in2;
       10,11,12 : out1 = in3;
       13,14,15 : out1 = in4;
    endcase
  end
endmodule
