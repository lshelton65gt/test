/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case items having decimal numbers
// Section 5.3.11 of the test plan

module CASE27(cond,in1,in2,cond2,in3,in4,out2,out1);
input [1:0] in1,in2,cond,cond2,in3,in4;
output [1:0] out1,out2;
reg [1:0] out1,out2;

  always @(in1 or in2 or cond or cond2 or in3 or in4)
  begin
    case(cond)
     2'd0 : out1 = in1 & in2;
     2'd1 : out1 = in1 | in2;
     2'd2 : out1 = in1 ~^ in2; 
     2'd3 : out1 = in1 ^ in2;
   endcase
    case(cond2)
     2'd0 : out2 = in3 & in4;
     2'd1 : out2 = in3 | in4;
     2'd2 : out2 = in3 ~^ in4; 
     2'd3 : out2 = in3 ^ in4;
   endcase
  end

endmodule
 
