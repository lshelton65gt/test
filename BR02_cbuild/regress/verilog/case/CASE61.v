/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression of size 64
// Section 5.3.36 of the test plan

module CASE61(cond,in1,in2,out1);
input [3:0] in1,in2;
input [63:0] cond;
output [3:0] out1;
reg [3:0] out1;

  always @(cond or in1 or in2)
  begin
    case(cond)
     64'habcdef1234567890 : out1 = in1 & in2;
     64'h1234567887654321 : out1 = in1 | in2;
     64'h0000000000000001 : out1 = in1 ^ in2;
     64'h0000000000000002 : out1 = in1 ~^ in2;
     64'h0000000000000003 : out1 = ~in1 | in2;
     64'h0101010101010101 : out1 = ~in1 ^ in2;
                  default : out1 = ~in1;
   endcase
 end
endmodule


