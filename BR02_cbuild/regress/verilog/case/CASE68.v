/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Overlapping case item
// Section 5.3.42 of the test plan

module CASE68(in1,in2,cond,out1);
input [3:0] in1,in2,cond;
output [3:0] out1;
reg [3:0] out1;

  always @(in1 or in2 or cond)
  begin : block
    casez (cond)
      4'b0000 : out1 = in1 & in2;
      4'b00z? : out1 = in1 | in2;
      4'b1?z1 : out1 = in1;
      4'b?11? : out1 = in2;
      4'b0z?0 : out1 = ~in1;
      4'b111? : out1 = in1 ^ in2;
      4'bzz?1 : out1 = in1 ~^ in2;
      default : out1 = 4'b1010;
    endcase
  end
endmodule


