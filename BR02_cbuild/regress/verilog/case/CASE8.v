/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case item having function call
// Section 5.3.19 of test plan

module CASE8(in1,in2,out1);
input [1:0] in1,in2;
output [1:0] out1;
reg [1:0] out1;
function max;
input [1:0] in1,in2;
  max = in1 > in2 ? 1'b1 : 1'b0;
endfunction

always @ (in1 or in2)
begin
 out1 = 0;
 case (1'b1)
   max(in1,in2) : out1 = in1;
   max(in2,in1) : out1 = in2;
 endcase
end
endmodule

