/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Combination of case, casex and casez
// Section 5.3.44 of the test plan


module CASE11 (in1,in2,cond1,cond2,cond3,out1);
input [1:0] in1,in2;
input cond1;
input [2:0] cond3;
input [3:0] cond2;
output [3:0] out1;
reg [3:0] out1;

  always @(in1 or in2 or cond1 or cond2 or cond3)
  begin
   out1 =0;
     case (cond1)
      1'b0 : casex(cond2)
             4'b101x : out1 = in1;
             4'b00x? : out1 = in1 + 1;
             4'b111z : casez(cond3)
                        3'b10z : out1 = in1 + 2;
                        3'bz1? : out1 = in1 + in2;
                        default : out1 = in2 + 2;
                       endcase
             default : out1 = in1 - in2;
             endcase
     1'b1 : casez(cond3)
              3'b?z1: casex(cond2) 
                       4'b11xz: out1 = in2 + 3;
                       4'b00?z : out1 = in2 + 1;
                       default : out1 = in1 * in2;
                       endcase
              default : out1 = 4'b1010;
           endcase
     endcase
  end
endmodule

