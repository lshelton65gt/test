/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression having concatenation
// Section 5.3.5 of test plan

module CASE3 (in1,in2,cond1,cond2,cond3,cond4,out1);
input [1:0] in1,in2;
input cond1,cond2,cond3,cond4;
output [4:0] out1;
reg [4:0] out1;

  always @(in1 or in2 or cond1 or cond2 or cond3 or cond4)
  begin
     case ({cond1,cond2})
       2'b00 : case ({cond3,cond4})
                   2'b00 : out1 = {in1,in2} + {in2,in1};
                   2'b10 : out1 = in1[1:0] + in2[1:0] ;
                   2'b11 : out1 = in1[0] + in1[1] + in2[0] + in2[1];
                   2'b01 : out1 = in1 + in2;
               endcase
      2'b10 : case ({cond4,cond3})
                 2'b00 : out1 = in1 - in2;
                 2'b10 : out1 = in1[1:0] - in2[1:0];
                 default : out1 = {in1[1],in2[1]} - {in1[0],in2[0]};
              endcase
      2'b11 : case ({2{cond3}})
               2'b00 : out1 = in1 * in2 ;
               2'b11 : out1 = in1[1:0] * in2[1:0];
               2'b10,2'b01 : out1 = {in1[1],in2[1]} * {in1[0],in2[0]};
              endcase
      2'b01 :begin 
              out1[4:3] = in1 & in2; 
              out1[3:2] = in1 | in2;
              out1[2:1] = in1 ^ in2;
              out1[0] = in1[0] ~^ in2[0];
            end
  endcase
 end
 endmodule
