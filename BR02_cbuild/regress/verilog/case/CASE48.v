/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case statement having procedural assignments
// Section 5.3.35 of the test plan

module CASE48(cond,in1,in2,in3,in4,out1,out2);
input [1:0] in1,in2,in3,in4,cond;
output [3:0] out1,out2;
reg [3:0] out1,out2;

  always @(cond or in1 or in2 or in3 or in4)
  begin
    case(cond)
      2'b00 : begin
              {out1[1:0],out1[3:2]} = {in1,in2};
              {out2[1:0],out2[3:2]} <= {in3,in4};
              end
      2'b01 : begin
              {out1[1:0],out1[3:2]} = {~in1,in2};
              {out2[1:0],out2[3:2]} <= {~in3,in4};
              end
      2'b10 : begin
              {out1[1:0],out1[3:2]} = {in1,~in2};
              {out2[1:0],out2[3:2]} <= {in3,~in4};
              end
      2'b11 : begin
              {out1[1:0],out1[3:2]} = {~in1,~in2};
              {out2[1:0],out2[3:2]} <= {~in3,~in4};
              end
    endcase
  end
endmodule
