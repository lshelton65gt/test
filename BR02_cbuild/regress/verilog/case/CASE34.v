/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case having duplicate items
// Section 5.3.20 of the test plan

module CASE34(cond,in1,in2,out1);
input [11:0] cond,in1,in2;
output [11:0] out1;
reg [11:0] out1;

  always @(cond or in1 or in2)
  begin
   case(cond)
    12'b000000000001,12'habc,12'o0001 : out1 = in1 & in2;
    12'd10,12'h00a,12'd15 : out1 =in1 | in2; 
    12'h00f : out1 = in1 ^ in2;
    default : out1 = in1 ~^ in2;
   endcase
  end
endmodule
