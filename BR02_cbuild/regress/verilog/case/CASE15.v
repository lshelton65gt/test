/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case expression having hex numbers
// Section 5.3.1 of the test plan

module CASE15(cond,in1,in2,in3,in4,out1);
input [3:0] in1,in2,in3,in4,cond;
output [3:0] out1;
reg [3:0] out1;

  always @(cond or in1 or in2 or in3 or in4)
  begin
    case(4'hf)
      in1 : out1 = 1;
      in2 : out1 = 2;
      in3 : out1 = 3;
      in4 : out1 = 4;
      default : out1 =7;
    endcase
  end
endmodule
