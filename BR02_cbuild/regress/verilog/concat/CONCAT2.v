/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Concatenation having part_select
// Section 2.1.9.4 of test plan

module CONCAT2 (in1,in2,in3,in4,out1,out2);
input [3:0] in1,in2,in3,in4;
output [7:0] out1,out2;

  assign out1 = { in1[1:0],in2[1:0],in1[3:2],in2[3:2] };
  assign out2 = { in3[1:0],in3[3:2],in4[1:0],in4[3:2] };
endmodule

