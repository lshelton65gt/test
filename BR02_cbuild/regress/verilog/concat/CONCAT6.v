/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Nested Concatenation
// Section 2.1.9.6 2.1.9.12  of test plan

module CONCAT6 (in1,in2,in3,in4,out1);
input [1:0] in1,in2,in3,in4;
output [19:0] out1;

assign out1 = { {1'b1,4'hb,3'o7,4'd15},{in1[0],in2[0],in1[1],in2[1]},
                {in3[1:0],in4[1:0]}};
endmodule
