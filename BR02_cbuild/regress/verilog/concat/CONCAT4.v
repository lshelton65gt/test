/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Multiple Concatenation having part_select
// Section 2.1.9.10 of test plan

module CONCAT4 (in1,in2,in3,in4,out1,out2);
input [3:0] in1,in2,in3,in4;
output [26:0] out1,out2;

  assign out1 = { {2{in1[1:0]}},{3{in2[1:0]}},{5{in1[3:2]}},{4{in2[3:2]}} };
  assign out2 = { {5{in3[1:0]}},{4{in3[3:2]}},{3{in4[1:0]}},{2{in4[3:2]}} };
endmodule

