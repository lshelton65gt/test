/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// Concatenation in continuous assign statement with padding 
// Section 2.1.9.15 of the test plan
module CONCAT33(in1,in2,out1);
input  [31:0] in1, in2;
output [1023:0] out1;
assign out1 =  {in2, in1};
endmodule
