/*** multiple  concatenation in while statement **/
// section 2.1.9.14 of the test plan

module CONCAT28(clk,reset,Ain, Bin, Cin, Dout);
input clk,reset;
input [3:0] Ain,Bin; 
input Cin;
output [3:0] Dout;
reg [3:0] Dout;


always 
  begin @ (posedge clk)
    if (reset) Dout = 4'b0000;
    while (Ain >= {2{Cin,~Cin}})
      begin
	@ (posedge clk);
	Dout = Dout + Bin;
      end
  end

endmodule
	

