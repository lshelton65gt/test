//Nested Concatenation in 'case' statement
// section 2.1.9.14 of the test plan

module CONCAT24(inp1, inp2, sel1, sel2, out1);
input [2:0] inp1,inp2;
input [1:0] sel1,sel2;
output [2:0] out1;

reg [2:0] out1;


always @(inp1 or inp2 or sel1 or sel2)
	begin
	case ({sel1,{sel2[0],sel2[1]}})
		4'b0000: out1 = inp1 + inp2;
		4'b0101: out1 = inp1 - inp2;
		4'b1010: out1 = inp1 & inp2;
		4'b1110: out1 = inp1 | inp2;
		default: out1 = 3'b111;
	endcase
	end


endmodule

