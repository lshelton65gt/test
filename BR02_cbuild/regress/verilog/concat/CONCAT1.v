/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Concatenation having bit_select
// Section 2.1.9.3 of test plan

module CONCAT1(in1,in2,in3,in4,out1,out2);
input [1:0] in1,in2,in3,in4;
output [3:0] out1,out2;

  assign out1 = { in1[0],in2[0],in1[1],in2[1] };
  assign out2 = { in3[0],in3[1],in4[0],in4[1] };
endmodule

