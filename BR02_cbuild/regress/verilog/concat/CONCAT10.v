/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Multiple concatenation having numbers
// Section 2.1.9.6 of the test plan

module CONCAT10(clk,out1,out2,out3,out4,out5);
input clk;
output [7:0]out1;
output [17:0] out2;
output [31:0] out3;
output [49:0] out4;
output [19:0] out5;

reg [7:0] out1;
reg [17:0] out2;
reg [31:0] out3;
reg [49:0] out4;
reg [19:0] out5;
   always @(posedge clk)
   begin
     out1 = {2{4'b1100}};
     out2 = {3{6'o76}};
     out3 = {4{8'hfe}};
     out4 = {10{5'd16}};
     out5 ={2{{1'b1,2'd3,3'o3,4'ha}}};
   end
endmodule
