/*** nested concatenation in if statement **/
//section 2.1.9.14 of the test plan

module CONCAT22(Ain, Bin, Cin, Dout);
input [3:0] Ain, Bin;
input Cin;
output [3:0] Dout;
reg [3:0] Dout;


always @(Ain or Bin or Cin)
	begin
		if (Ain >= {{{Cin,~Cin},Cin},~Cin})
			Dout = Ain - Bin + Cin;
		else
			Dout = Ain - Bin - Cin;
	end

endmodule

