/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Concatenation having expression
// Section 2.1.9.13 of the test plan

module CONCAT17(in1,in2,in3,in4,in5,in6,out1,out2);
input [7:0] in1,in2,in3,in4,in5,in6;
output [23:0] out1,out2;

  assign out1 = {in1 << 2,in2 >> 2'b10,in3 <<4'h8};
  assign out2 = {in4 >> 6'o10,in5<<2'd2,in6>>4};
endmodule
