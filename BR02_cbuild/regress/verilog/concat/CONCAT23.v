/* Test case for multiple concatenation in case statement */
// section 2.1.9.14 of the test plan


module CONCAT23(inp1, inp2, sel, out1);
input [2:0] inp1,inp2;
input [1:0] sel;
output [2:0] out1;

reg [2:0] out1;


always @(inp1 or inp2 or sel)
	begin
	case ({2{sel}})
		4'b0000: out1 = inp1 + inp2;
		4'b0101: out1 = inp1 - inp2;
		4'b1010: out1 = inp1 & inp2;
		4'b1111: out1 = inp1 | inp2;
		default: out1 = 4'b0000;
	endcase
	end


endmodule

