/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Concatenation having numbers
// Section 2.1.9.1 of the test plan

module CONCAT7(clk,out1,out2,out3);
input clk;
output [8:0] out1;
reg [8:0] out1;
output [13:0] out2;
reg [13:0] out2;
output [12:0] out3;
reg [12:0] out3;

  always @(posedge clk)
  begin
    out1 = {2'b11,3'o7,4'ha};
    out2 = {4'b1010,4'd10,6'o12};
    out3 = {8'hab,5'd31};
  end
endmodule

