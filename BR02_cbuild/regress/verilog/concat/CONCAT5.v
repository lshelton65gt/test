/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Multiple Concatenation having concatenation
// Section 2.1.9.11 of test plan

module CONCAT5(in1,in2,in3,in4,out1,out2);
input [1:0] in1,in2,in3,in4;
output [ 9: 0] out1;
output [7:0] out2;

 assign out1 = { {2{{in1[0],in2[0]}}},{3{{in1[1],in2[1]}}}};
 assign out2 = { {2{{in3[1:0],in4[1:0]}}}};
endmodule
