/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Multiple concatenation having wires
// Section 2.1.9.7 of the test plan

module CONCAT11(in1,in2,in3,out1,out2,out3,out4);
input in1;
input [1:0] in2;
input [2:0] in3;
output [11:0] out1,out2,out3,out4;

  assign out1 = {12{in1}};
  assign out2 = {6{in2}};
  assign out3 = {4{in3}};
  assign out4 = {2{{in1,in2,in3}}};
endmodule
