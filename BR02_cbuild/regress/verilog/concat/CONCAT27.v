/*** nested concatenation in while statement **/

// section 2.1.9.14 of the test plan

module CONCAT27(clk,Ain, Bin, Cin, Dout);
input clk;
input [3:0] Ain,Bin; 
input Cin;
output [3:0] Dout;
reg [3:0] Dout;


always 
  begin @ (posedge clk)
    Dout = 4'b0000;
    while (Ain >= {{{Cin,~Cin},Cin},~Cin})
      begin
	@ (posedge clk);
	Dout = Dout + Bin;
      end
  end

endmodule
	

