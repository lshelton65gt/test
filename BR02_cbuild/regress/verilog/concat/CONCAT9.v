/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Concatenation having registers
// Section 2.1.9.5 of the test plan

module CONCAT9(clk,in1,in2,in3,in4,in5,in6,out1,out2);
input clk,in1,in2,in3;
input [1:0] in4,in5,in6;
output [8:0] out1,out2;
reg [8:0] out1,out2,out3;
reg reg1,reg2,reg3;
reg [1:0] reg4,reg5,reg6;

  always @(posedge clk)                                 
  begin
    reg1 = in1;reg2=in2;reg3=in3;reg4=in4;reg5=in5;reg6=in6;
    out1 = {reg1,reg2,reg3,reg4,reg5,reg6};
    out2 = {{reg1,reg6},{reg2,reg5,reg3},reg4};
   end
endmodule
