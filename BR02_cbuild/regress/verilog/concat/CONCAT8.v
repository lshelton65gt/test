/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Concatenation having wires
// Section 2.1.9.2 of the test plan

module CONCAT8(in1,in2,in3,in4,in5,in6,out1,out2);
input in1,in2,in3;
input [1:0] in4,in5,in6;
output [8:0] out1,out2;

  assign out1 = {in1,in6,in2,in5,in3,in4};
  assign out2 = {{in1,in6},{in2,in5,in3},in4};
endmodule
