/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Multiple Concatenation having bit_select
// Section 2.1.9.9 of test plan

module CONCAT3(in1,in2,in3,in4,out1,out2);
input [1:0] in1,in2,in3,in4;
output [13:0] out1,out2;

  assign out1 = { {2{in1[0]}},{3{in2[0]}},{4{in1[1]}},{5{in2[1]}} };
  assign out2 = { {5{in3[0]}},{4{in3[1]}},{3{in4[0]}},{2{in4[1]}} };
endmodule

