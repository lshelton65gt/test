/* Copyright (c) 1998 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Concatenation having expression with
//  multiple and nested  concatenations
// Section 2.1.9.13.1 of the test plan


module CONCAT18(in1,in2,in3,in4,in5,in6,out1,out2);
input [7:0] in1,in2,in3,in4,in5,in6;
output [23:0] out1,out2;



assign out1 = {{in1 << 2,in2 >> 2'b10},{in3[3:0],{in3[5:4],in3[7:6]}}};
assign out2 =   { {2{(in4[7:4] & in4[3:0])}}, {2{(in5[7:4] | in5[3:0])}}, {2{(in6[7:4]^in6[3:0])}}};


endmodule
