/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Concatenation having expression
// Section 2.1.9.13 of the test plan

module CONCAT15(in1,in2,in3,in4,in5,in6,out1,out2);
input [1:0] in1,in2,in3,in4,in5,in6;
output [2:0] out1,out2;

  assign out1 = { in1 && in2 ,in3 || in4 ,!(in5 || in6)};
  assign out2 = { in1 && in2 || in3 ,!(in1) && (!in5),in1 && in2 || in3 && in4 || in5 && in6};
endmodule
