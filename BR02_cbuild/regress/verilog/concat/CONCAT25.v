// Concatenation of multiple and complex expressions
// in the if statement

// section 2.1.9.14 of the test plan


module CONCAT25(Ain, Bin, Cin, Din, Dout);
input [3:0] Bin,Cin;
input [7:0] Din;
input [15 : 0] Ain;
output [4:0] Dout;
reg [4:0] Dout;



always @(Ain or Bin or Cin)
	begin
		if (Ain == {{Bin << 2, Cin >> 2'b10},{2{(Din[7:4] & Din[3:0])}}}
)
			Dout = Bin + Cin;
		else
			Dout = Bin - Cin;
	end
endmodule

