/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Multiple concatenation having registers
// Section 2.1.9.8 of the test plan

module CONCAT12(clk,in1,in2,in3,out1,out2,out3,out4);
input clk,in1;
input [1:0] in2;
input [2:0] in3;
output [11:0] out1,out2,out3,out4;
reg [11:0] out1,out2,out3,out4;
reg reg1;
reg [1:0] reg2;
reg [2:0] reg3;

  always @(posedge clk)
  begin
    reg1 = in1;reg2=in2;reg3=in3;
    out1 = {12{reg1}};
    out2 = {6{reg2}};
    out3 = {4{reg3}};
    out4 = {2{{reg1,reg2,reg3}}};
 end
endmodule
