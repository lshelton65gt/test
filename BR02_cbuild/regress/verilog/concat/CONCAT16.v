/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Concatenation having expression
// Section 2.1.9.13 of the test plan

module CONCAT16(in1,in2,in3,in4,in5,in6,out1);
input [7:0] in1,in2,in3,in4,in5,in6;
output [5:0] out1;

  assign out1 = { &in1,|in2,~&in3,~|in4,^in5,~^in6};
endmodule
