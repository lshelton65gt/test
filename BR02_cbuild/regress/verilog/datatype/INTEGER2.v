/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// Array of integers 
// Section 1.6 of the test plan
module INTEGER2(in1,in2,out1);
input [3:0] in1,in2;
output [3:0] out1;
integer mem[1:0];

  always @(in1 or in2)
  begin
    mem[0] = in1;
    mem[1] = in2;
  end

  assign out1 = mem[0] & mem[1];
endmodule
