/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Integer Declaration
// Section 1.4 of the test plan

module INTEGER1 (in1,in2,clk,out1);
input clk;
input [30:0] in1,in2;
output [31:0] out1;
reg [31:0] out1;
integer int1,int2;

always @(posedge clk)
begin
   int1 = in1 - in2;
   int2 = in1 + in2;
   out1 = int1 + int2;
end
endmodule

