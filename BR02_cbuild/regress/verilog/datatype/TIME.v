/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// Time type 
// Section 1.7 of the test plan
module TIME(in1,in2,clk,out1);
input [31:0] in1,in2;
input clk;
output [63:0] out1;
reg [63:0] out1;
 always @(posedge clk)
 begin : block
   time time_var;
   time_var = { in1,in2};
   out1 = time_var;
 end
endmodule
