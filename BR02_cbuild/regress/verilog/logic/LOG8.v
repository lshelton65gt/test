/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// || operator having concatenation
// Section 2.1.4.3 of test plan

module LOG8(in1,in2,in3,in4,in5,in6,out1,out2);
input in1,in2,in3,in4,in5,in6;
output out1,out2;

  assign out1 = {in1,in2,in3} || {in4,in5,in6};
  assign out2 = {{in1,in2},{in3,in4}} || {{in1,in5},{in2,in6}};
endmodule
