/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// ! operator having bit_select
// Section 2.1.4.1 of test plan

module LOG18(in1,out1,out2,out3);
input [2:0] in1;
output out1,out2,out3;
 assign out1 = !in1[0];
 assign out2 = !in1[1];
 assign out3 = !in1[2];
endmodule

