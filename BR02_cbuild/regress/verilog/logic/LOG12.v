/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// || operator having part_select
// Section 2.1.4.3 of test plan

module LOG12(in1,in2,in3,out1,out2,out3);
input [5:0] in1,in2,in3;
output out1,out2,out3;

  assign out1 = (in1[1:0] || in2[1:0] ) || in3[1:0];
  assign out2 = (in1[3:2] || in2[3:2] ) || in3[3:2];
  assign out3 = (in1[5:4] || in2[5:4] ) || in3[5:4];
endmodule

