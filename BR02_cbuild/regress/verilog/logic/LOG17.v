/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// ! operator having multiple concatenation
// Section 2.1.4.1 of test plan

module LOG17(in1,in2,in3,out1,out2);
input in1,in2,in3;
output out1,out2;

  assign out1 = ! {2{in1}};
  assign out2 = !{2{{in1,in2,in3}}};
endmodule
