#!/bin/tcsh -f

set testdir=""
if ! $?TESTDIR then
    echo "Setting default testdir to ./verilog"
    set testdir="./"
else
    set testdir="${TESTDIR}"
endif
set directed_tests="test_1.v,test_2.v,test_3.v,function2.v,sfifo_rtl.v,adder_implicit.v,dramcon_rtl.v,task1.v,param_instantiation.v,instantiation.v,dff1.v,dff2.v,dff3.v,dff4.v,dff5.v,dff6.v,latch1.v"
set case_tests="CASE10.v,CASE11.v,CASE12.v,CASE13.v,CASE14.v,CASE15.v,CASE16.v,CASE17.v,CASE18.v,CASE19.v,CASE1.v,CASE20.v,CASE21.v,CASE22.v,CASE23.v,CASE24.v,CASE25.v,CASE26.v,CASE27.v,CASE28.v,CASE29.v,CASE2.v,CASE30.v,CASE31.v,CASE32.v,CASE33.v,CASE34.v,CASE35.v,CASE36.v,CASE37.v,CASE38.v,CASE39.v,CASE3.v,CASE40.v,CASE41.v,CASE43.v,CASE44.v,CASE46.v,CASE47.v,CASE48.v,CASE49.v,CASE4.v,CASE50.v,CASE51.v,CASE52.v,CASE53.v,CASE54.v,CASE55.v,CASE56.v,CASE57.v,CASE58.v,CASE59.v,CASE5.v,CASE60.v,CASE61.v,CASE62.v,CASE63.v,CASE64.v,CASE65.v,CASE66.v,CASE67.v,CASE68.v,CASE69.v,CASE6.v,CASE70.v,CASE71.rtlc.v,CASE71.v,CASE72.v,CASE73.v,CASE7.v,CASE8.v,CASE9.v"
set assign_tests="ASSIGN10.v,ASSIGN11.v,ASSIGN12.v,ASSIGN13.v,ASSIGN14.v,ASSIGN15.v,ASSIGN16.v,ASSIGN17.v,ASSIGN18.v,ASSIGN19.v,ASSIGN1.v,ASSIGN20.v,ASSIGN21.v,ASSIGN22.v,ASSIGN23.v,ASSIGN24.v,ASSIGN25.v,ASSIGN26.v,ASSIGN27.v,ASSIGN28.v,ASSIGN29.v,ASSIGN2.v,ASSIGN_2Z.v,ASSIGN30.v,ASSIGN31.v,ASSIGN32.v,ASSIGN33.v,ASSIGN34.v,ASSIGN35.v,ASSIGN36.v,ASSIGN37.v,ASSIGN38.v,ASSIGN39.v,ASSIGN3.v,ASSIGN40.v,ASSIGN41.v,ASSIGN42.v,ASSIGN43.v,ASSIGN44.v,ASSIGN45.v,ASSIGN46.v,ASSIGN47.v,ASSIGN48.v,ASSIGN49.v,ASSIGN4.v,ASSIGN50.v,ASSIGN52.v,ASSIGN54.v,ASSIGN55.v,ASSIGN56.v,ASSIGN57.v,ASSIGN58.v,ASSIGN59.v,ASSIGN5.v,ASSIGN60.v,ASSIGN61.v,ASSIGN62.v,ASSIGN64.v,ASSIGN66.v,ASSIGN67.v,ASSIGN68.v,ASSIGN69.v,ASSIGN6.v,ASSIGN70.v,ASSIGN71.v,ASSIGN72.v,ASSIGN73.v,ASSIGN74.v,ASSIGN75.v,ASSIGN76.v,ASSIGN77.v,ASSIGN78.v,ASSIGN79.v,ASSIGN7.v,ASSIGN80.v,ASSIGN81.v,ASSIGN8.v,ASSIGN9.v,ASSIGN_OVERLAP.v"
set arith_tests="ARITH10.v,ARITH11.v,ARITH12.v,ARITH13.v,ARITH14.v,ARITH15.v,ARITH16.v,ARITH17.v,ARITH18.v,ARITH19.v,ARITH1.v,ARITH20.v,ARITH21.v,ARITH22.v,ARITH23.v,ARITH24.v,ARITH25.v,ARITH26.v,ARITH28.v,ARITH29.v,ARITH2.v,ARITH31.v,ARITH32.v,ARITH33.v,ARITH34.v,ARITH35.v,ARITH36.v,ARITH37.v,ARITH38.v,ARITH39.v,ARITH3.v,ARITH41.v,ARITH42.v,ARITH43.v,ARITH44.v,ARITH45.v,ARITH46.v,ARITH47.v,ARITH48.v,ARITH4.v,ARITH6.v,ARITH7.v,ARITH8.v"
set concat_tests="CONCAT10.v,CONCAT11.v,CONCAT12.v,CONCAT13.v,CONCAT14.v,CONCAT15.v,CONCAT16.v,CONCAT17.v,CONCAT18.v,CONCAT1.v,CONCAT21.v,CONCAT22.v,CONCAT23.v,CONCAT24.v,CONCAT25.v,CONCAT26.v,CONCAT27.v,CONCAT28.v,CONCAT29.v,CONCAT2.v,CONCAT30.v,CONCAT31.v,CONCAT32.v,CONCAT33.v,CONCAT3.v,CONCAT4.v,CONCAT5.v,CONCAT6.v,CONCAT7.v,CONCAT8.v,CONCAT9.v"
set logic_tests="LOG10.v,LOG11.v,LOG12.v,LOG13.v,LOG14.v,LOG15.v,LOG16.v,LOG17.v,LOG18.v,LOG19.v,LOG1.v,LOG20.v,LOG2.v,LOG7.v,LOG8.v,LOG9.v"
set reduc_tests="REDUC10.v,REDUC11.v,REDUC12.v,REDUC13.v,REDUC14.v,REDUC15.v,REDUC16.v,REDUC17.v,REDUC18.v,REDUC19.v,REDUC1.v,REDUC20.v,REDUC21.v,REDUC22.v,REDUC23.v,REDUC24.v,REDUC25.v,REDUC26.v,REDUC27.v,REDUC2.v,REDUC3.v,REDUC4.v,REDUC5.v,REDUC6.v,REDUC7.v,REDUC8.v,REDUC9.v"
set rel_tests="REL10.v,REL19.v,REL1.v,REL20.v,REL21.v,REL22.v,REL23.v,REL24.v,REL25.v,REL26.v,REL27.v,REL28.v,REL29.v,REL2.v,REL30.v,REL31.v,REL32.v,REL33.v,REL34.v,REL35.v,REL36.v,REL37.v,REL38.v,REL3.v,REL4.v,REL5.v,REL6.v,REL7.v,REL8.v,REL9.v"
set shift_tests="SHIFT10.v,SHIFT12.v,SHIFT13.v,SHIFT14.v,SHIFT15.v,SHIFT16.v,SHIFT1.v,SHIFT2.v,SHIFT3.v,SHIFT4.v,SHIFT5.v,SHIFT6.v,SHIFT7.v,SHIFT8.v,SHIFT9.v"
set static_tests="STATIC1.v"
set datatype_tests="INTEGER1.v,INTEGER2.v,TIME.v"
set forloop_tests="FOR1.v,FOR2.v,FOR3.v,FOR4.v"
set repeat_tests="REPEAT1.v,REPEAT2.v,REPEAT3.v,REPEAT4.v,REPEAT5.v,REPEAT6.v,REPEAT7.v,REPEAT8.v,REPEAT9.v"
set while_tests="WHILE10.v,WHILE11.v,WHILE12.v,WHILE13.v,WHILE14.v,WHILE15.v,WHILE16.v,WHILE17.v,WHILE18.v,WHILE19.v,WHILE20.v,WHILE2.v,WHILE3.v,WHILE4.v,WHILE5.v,WHILE6.v,WHILE7.v,WHILE8.v,WHILE9.v"
set ifelse_tests="IF1.v,IF2.v,IF3.v,IF4.v,IF5.v,IF6.v,IF7.v,IF8.v"
set forever_tests="FOREVER1.v,FOREVER2.v"
set named_tests="NAMED15.v"
set tests="directed/{${directed_tests}} case/{${case_tests}} assign/{${assign_tests}} arith/{${arith_tests}} concat/{${concat_tests}} logic/{${logic_tests}} reduc/{${reduc_tests}} rel/{${rel_tests}} shift/{${shift_tests}} static/{${static_tests}} datatype/{${datatype_tests}} forloop/{${forloop_tests}} ifelse/{${ifelse_tests}} forever/{${forever_tests}} named/{${named_tests}}"

#repeat/{${repeat_tests}} while/{${while_tests}}"

#set tests="directed/{${directed_tests}}"
#set tests="case/{${case_tests}}"
#set tests="assign/{${assign_tests}}"
#set tests="arith/{${arith_tests}}"
#set tests="concat/{${concat_tests}}"
#set tests="logic/{${logic_tests}}"
#set tests="reduc/{${reduc_tests}}"
#set tests="rel/{${rel_tests}}"
#set tests="shift/{${shift_tests}}"
#set tests="static/{${static_tests}}"
#set tests="datatype/{${datatype_tests}}"
#set tests="forloop/{${forloop_tests}}"
#set tests="ifelse/{${ifelse_tests}}"
#set tests="forever/{${forever_tests}}"
#set tests="repeat/{${repeat_tests}}"
#set tests="while/{${while_tests}}"
#set tests="while/WHILE20.v"

setenv CARBON_BIN product
foreach i (${tests})
    set ctest="${testdir}/${i}"
    rm -f design.exe design_verific.exe design_interra.exe interra.txt verific.txt 
    cbuild -q ${ctest} -useVerific -testdriverDumpVCD >& /dev/null
    mv design.exe design_verific.exe
    ./design_verific.exe >& verific.txt
    rm design.exe -f
    cbuild -q ${ctest} -testdriverDumpVCD >& /dev/null
    mv design.exe design_interra.exe
    ./design_interra.exe >& interra.txt
    diff -q ./verific.txt ./interra.txt >& /dev/null
    if ($status != 0) then
        echo "${ctest} - FAIL"
    else
        echo "${ctest} - PASS"
    endif
end
