/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// << operator having part-select and bit_select
// Section 2.1.7.1 of the test plan

module SHIFT6(in1,out1,out2,out3);
input [3:0] in1;
output out1;
output [1:0] out2;
output [2:0] out3;
    assign out1 = in1[0] << 1'b1;
    assign out2 = in1 [1:0] << 2'b10;
    assign out3 = in1[3:1] <<3'o2;
endmodule
