/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// Shift left and shift right on the same output variable 
// Section 2.1.7.6 of the test plan
        module SHIFT14(clk,sel,rst,in1,in2,in3,out1);
        input clk,rst,sel;
        input [3:0] in2,in3;
        input [1:0] in1;
        output [3:0] out1;
        reg [3:0] temp,out1;
        always @(posedge clk or posedge rst)
          if (rst)
            out1 = 4'h0;
          else if (sel) begin
            temp = in2 + in3;
            out1 = temp << in1;
          end
          else begin
            temp = in2 - in3;
            out1 = temp >> in1;
          end
        endmodule
