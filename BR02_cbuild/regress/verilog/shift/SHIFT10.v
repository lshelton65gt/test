/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited
   without prior written permission of Interra Inc.
*/
 
// shift operator
// Section 2.1.7.3 of test plan

module SHIFT10(in1,in2,out1,out2);
input [7:0] in1,in2;
output [7:0] out1,out2;
reg [7:0] out1,out2;

  always @(in1 or in2)
  begin : block
  integer r1,r2;
    
    r1 = in1 ;
    out1 = r1 << 4;
    r2 = in2;
    out2 = r2 >> 4;

   end
endmodule


