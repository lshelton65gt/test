/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// left shift with variable shift in continuous assignment
// section 2.1.7.4 of the test plan

module SHIFT12 (in1,out1);
input[2:0] in1;
output[7:0] out1;
wire[7:0] out1;

assign out1 = 1 << in1;

endmodule
