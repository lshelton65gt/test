/* Copyright (c) 2001 Interra Technology, Inc.
Use, disclosure or distribution is prohibited
without prior written permission of Interra Technology, Inc.
*/
// Bit-wise shift right operation using a for-loop 
// Section 2.1.7.5 of the test plan
        module SHIFT13 (in1, out1);
           parameter p1 = 2;
           input[p1-1:0]    in1;
           output[p1-1:0]   out1;
           reg[p1-1:0]      out1;
           integer i;
        always @ (in1)
        begin
           for (i = 0; i < p1; i = i + 1)
           begin
              out1[i] = in1 >>i ;
           end
        end
        endmodule
