/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Shift operators << and >>
// Section 2.1.7.1 of test plan
// Section 2.1.7.2 of test plan

module SHIFT1(in1,in2,out1);
input [7:0] in1,in2;
output [7:0] out1;
reg [7:0] out1;

  always @(in1 or in2)
  begin : block
  reg [7:0] r1;
  reg r2;
  reg [6:0] r3;
    
    r1 = in1 << 2;
    r2 = in2[0] <<1;
    r3 = in1[7:1] << 4;
    out1 = ({r1,r2,r3})<<4;

    r1 = in1 >> 2;
    r2 = in2[0] >> 1;
    r3 = in1[7:1] >> 4;
    out1 = ({r1,r2,r3}) >> 4;
   end
endmodule


