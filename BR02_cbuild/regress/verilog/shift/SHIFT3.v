/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// >> operator having registers and integers
// Section 2.1.7.2 of the test plan

module SHIFT3(clk,in1,in2,in3,out1,out2,out3,out4,out5);
input clk;
input [31:0] in1,in2;
input [2:0] in3;
output [31:0] out1,out2,out3,out4;
reg [31:0] reg1,out1,out2,out3,out4;
integer int1;
output out5;
reg out5;
   always @(posedge clk)
   begin
     reg1 = in1;
     int1 = in2;
     out1 = reg1 >> 4;
     out2 = reg1 >> in3;
     out3 = int1 >> 8;
     out4 = int1 >> in3; 
     out5 =(1'b1 <<15) >>15;
   end
endmodule
