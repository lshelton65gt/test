/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

//>> operator having concatenation and multiple conacatenation
// Section 2.1.7.2 of test plan

module SHIFT5(in1,in2,in3,in4,out1,out2,out3,out4);
input in1,in2,in3,in4;
output [7:0] out1,out2,out3,out4;

  assign out1 = {in1,in2,in3,in4} >> 2;
  assign out2 = {{in1,in2},{in2,in3},{in3,in4},{in4,in1}} >> {1'b1,1'b0};
  assign out3 = {8{in1}} >> {2{1'b1}};
  assign out4 = {2{{in1,in2,in3,{in4,1'b1}}}} >> {2{{1'b1,1'b0}}};
endmodule
