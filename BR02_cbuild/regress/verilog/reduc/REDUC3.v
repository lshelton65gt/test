/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Reduction ^ and ~^ operators
// Section 2.1.6.5 and 2.1.6.6 of test plan

module REDUC3(in1,in2,out1,out2);
input [3:0] in1,in2;
output out1,out2;
reg out1,out2;

  always @( in1 or in2 )
  begin: block
  reg r1,r2,r3;

   r1 = ^in1 ;
   r2 = ^in2[3:2] ;
   r3 = ^in2[0];
   out1= ^({r1,r2,r3});
   
   r1 = ~^in1 ;
   r2 = ~^in2[3:2] ;
   r3 = ~^in2[0];
   out2= ~^({r1,r2,r3});
  end
endmodule



