/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// reduction ^  operator having concatenation  and multiple concatenation
// Section 2.1.6.5 of test plan

module REDUC14(in1,in2,in3,in4,out1,out2,out3,out4);
input in1,in2,in3,in4;
output  out1,out2,out3,out4;

  assign out1 = ^({8{in1}});
  assign out2 = ^({4{{in1,in2}}});
  assign out3 = ^({in1,in2,in3,in4,in1,in2,in3,in4});
  assign out4 =^({{in1,in2},{in3,in4},{in1,in3},{in2,in4}});
endmodule


