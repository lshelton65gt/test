/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// reduction  operator ^ having function call
// Section 2.1.6.5 of test plan

module REDUC26(in1,in2,in3,in4,out1,out2);
input [1:0] in1,in2;
input [2:0] in3,in4;
output out1,out2;

function [1:0] func1;
input [1:0] in1,in2;
  func1 = in1 & in2;
endfunction

function [2:0] func2;
input [2:0] in1,in2;
  func2 = in1 ^ in2;
endfunction

  assign out1 = ^func1(in1,in2);
  assign out2 = ^func2(in3,in4);
endmodule
  
