/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Reduction operator & having bit-select and part-select
// Section 2.1.6.1 of test plan

module REDUC16(in1,out1,out2,out3);
input [3:0] in1;
output out1,out2,out3;

  assign out1 = &in1[3];
  assign out2 = &in1[2:0];
  assign out3 = &in1[3:2];
endmodule
