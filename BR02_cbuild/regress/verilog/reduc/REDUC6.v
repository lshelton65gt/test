/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// reduction ~&  operator having register and integer operand
// Section 2.1.6.3 of test plan

module REDUC6(clk,in1,in2,in3,in4,out1,out2,out3,out4);
input clk;
input [7:0] in1,in2;
reg [7:0] reg1,reg2; 
output out1,out2,out3,out4; 
reg  out1,out2,out3,out4;
integer int1,int2;
input [31:0] in3,in4;




   always@(posedge clk)
   begin
      reg1 = in1;
      reg2 = in2;
      int1 = in3;
      int2 = in3;
      out1 = ~&reg1;
      out2 = ~&reg2;
      out3 = ~&int1;
      out4 = ~&int2;
   end
endmodule
