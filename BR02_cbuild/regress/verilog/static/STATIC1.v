/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Static Expression having arithmetic operators
// Section 2.4.1 of test plan

module STATIC1(clk,out1,out2);
input clk;
output [31:0] out1,out2;
reg [31:0] out1,out2;
parameter p1 = 1000,p2=3000,p3=16;

  always @(posedge clk)
  begin
     out1 = (16'hfeda + 'o76531) - (p1 + 100/p3) * ( p2%p3 + p1 * p3);
     out2 = (16'b1010101010101010 +16'd5675438) /(2*p3) -(16'hafdc -
             16'b0000000011111111) % (p3/4);
  end
endmodule
