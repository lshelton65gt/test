/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Forever statement having if
// always statement having named forever statement
// Section 5.6.1 of the test plan
// Section 5.10.8 of the test plan
// Section 5.9.8 of the test plan

module FOREVER1 (in1,in2,in3,clk,out1);
input [1:0] in1,in2,in3;
input clk;
output [31:0] out1;
reg [31:0] out1;

always 
  begin
   @(posedge clk)
   out1 = in1;
   forever
   begin : block
   @(posedge clk);
    if(in1>in2) disable block;
    if(in2 == 2'b00)
       out1 = out1 + in3;
    else
      if(in2 == 2'b01)
         out1 = out1+ out1 * in3;
      else
        if(in2 == 2'b11)
          out1 = out1 + out1 ^ in3;
        else
          out1 = out1 + in3 | in1;
  end
 end
endmodule

