/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Forever having case
// Section 5.6.2 of test plan 

module FOREVER2 (in1,in2,cond1,cond2,clk,out1);
input [1:0] in1,in2,cond1,cond2;
input clk;
output [1:0] out1;
reg [1:0] out1;

  always 
  begin
		@(posedge clk)
    out1 = 0;
    forever
    begin: block
     @(posedge clk);
     if(cond1==2'b11) disable block;
     case(cond2)
       2'b00 : out1 = in1 | in2;
       2'b01 : out1 = in1 ^ in2;
       2'b10 : out1 = in1 & in2;
       2'b11 : out1 = in1 ~^ in2;
     endcase
    end
 end
endmodule
    
      
