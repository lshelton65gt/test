/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// if condition having function call
// Section 5.2.9 of test plan

module IF3(in1,in2,in3,out1);
input [1:0] in1,in2,in3;
output [1:0] out1;
reg [1:0] out1;

function max;
input [1:0] in1,in2;
  max = in1 >= in2 ? 1'b1 : 1'b0;
endfunction

  always @(in1 or in2 or in3 )
  begin
    out1 = 0;
    if(max(in1,in2))
       if(max(in1,in3))
          out1 = in1;
        else
          if(max(in2,in3))
             out1 = in2;
          else
            out1 = in3;
  else
    if(max(in2,in3))
       out1 = in2;
    else
      if(max(in1,in3))
        out1 = in1;
      else
       out1 = in3;
  end
endmodule

