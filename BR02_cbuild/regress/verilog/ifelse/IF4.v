/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// if condtion having concatenation
// Section 5.2.10 of test plan

module IF4(in1,in2,cond1,cond2,out1);
input [1:0] in1,in2;
input cond1,cond2;
output [2:0] out1;
reg [2:0] out1;

parameter p1=2'b00,p2=2'b01,p3=2'b10,p4=2'b11;

  always @(in1 or in2 or cond1 or cond2)
  begin
    out1 = 0;
    if({cond1,cond2}==p1 || {cond1,cond2}==p2)
       out1 = in1 + in2;
    else
      if({2{cond1,cond2}}=={p2,p3})
           out1 = in1 - in2;
      else
         if(({cond1,cond2} &{cond1,cond2})==p4)
           out1 = in1 + 1;
         else
           out1 = in1 - 1;
  end
endmodule
  

