/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// if condition having parameters
// Section 5.2.4 of the test plan

module IF8(clk,out1,out2,out3,out4,out5);
input clk;
output [11:0] out1,out2,out3,out4,out5;
reg [11:0] out1,out2,out3,out4,out5;
parameter p1=1,p2=1'b1,p3=1'd1,p4=3'o1,p5=4'h1;
  always @(posedge clk)
  begin
    if(p1 && p2)
       out1 = 1234;
    if(p3 && p4)
       out2 = 12'b101010101010;
    if(p1 || p5)
      out3 = 12'o1234;
    if(p2 || p4)
      out4 = 12'habc;
    if(p3 && p5 )
      out5 = 12'd1244;
  end
endmodule
