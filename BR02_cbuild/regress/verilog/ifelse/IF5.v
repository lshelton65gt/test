/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Simple if statement
// Section 5.2.1 of the test plan

module IF5(in1,in2,cond,out1);
input [1:0] in1,in2;
input cond;
output [1:0] out1;
reg [1:0] out1;
   always @(in1 or in2 or cond)
   begin
     out1 = in1;
     if(cond)
       out1 = in2;
   end
endmodule
