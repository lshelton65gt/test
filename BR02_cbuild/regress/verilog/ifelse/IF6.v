/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// if-else  statement
// Section 5.2.2 of the test plan

module IF6(in1,in2,cond,in3,in4,out1,out2);
input [1:0] in1,in2,in3,in4;
input cond;
output [1:0] out1,out2;
reg [1:0] out1,out2;
   always @(in1 or in2 or cond or in3 or in4)
   begin
     if(cond)
       out1 = in1;
     else
      out1 = in2;
     if(cond)
       out2 = in3;
     else
      out2 = in4;
   end
endmodule
