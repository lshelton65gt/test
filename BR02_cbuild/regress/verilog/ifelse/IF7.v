/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// if condition having numbers
// Section 5.2.3 of the test plan

module IF7(clk,out1,out2,out3,out4,out5);
input clk;
output [11:0] out1,out2,out3,out4,out5;
reg [11:0] out1,out2,out3,out4,out5;

  always @(posedge clk)
  begin
    if(1 && 1'b1)
       out1 = 1234;
    if(1'b1 && 3'o1)
       out2 = 12'b101010101010;
    if(3'o1 || 1'd1)
      out3 = 12'o1234;
    if(4'h1 || 1'd1)
      out4 = 12'habc;
    if(1'd1 )
      out5 = 12'd1244;
  end
endmodule
