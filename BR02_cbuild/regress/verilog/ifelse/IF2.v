/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// if condition having part select
// Section 5.2.8 of the test plan
// module having always statement
// Section 7.4.1 of the test plan

module IF2(in1,in2,cond1,cond2,out1);
input [1:0] in1,in2;
input [2:0] cond1,cond2;
output [1:0] out1;
reg [1:0] out1;
parameter p1 = 2'b00,p2=2'b11,
          p3 = 2'b10,p4=2'b01;

  always @(in1 or in2 or cond1 or cond2)
  begin
    out1 = 0;
    if((cond1[1:0]==p1) || (cond2[1:0] == p2) 
      || (cond1[2:1]==p3) || (cond2[2:1] ==p4))
      out1 = in1 << 2;
    else
      if((cond1[1:0]&cond2[1:0]) != (cond1[2:1] | cond2[2:1]))
         out1 = in2 >> 2;
      else
       if(cond1[1:0])
          out1 = in1 >> 2;
      else
         out1 = in2 << 2;
 end
endmodule

