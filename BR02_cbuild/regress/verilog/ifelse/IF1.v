/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// if condition having bit_select
// Section 5.2.7 of test plan
// always block having if statement
// Section 5.10.4 of Test Plan

module IF1(in1,in2,cond1,cond2,out1);
input [1:0] in1,in2;
input [1:0] cond1,cond2;
output [1:0] out1;
reg [1:0] out1;

  always @(in1 or in2 or cond1 or cond2)
  begin
    out1 =0;
    if((cond1[0]==cond2[0]) && (cond1[1]==cond2[1]))
        out1 = in1 & in2;
    else
      if((cond1[0]&cond2[0])>(cond1[1]&cond2[1]))
          out1 = in1 | in2;
      else
          out1 = in1 ^ in2; 
  end
endmodule

