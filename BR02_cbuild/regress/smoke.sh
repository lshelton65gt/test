#!/bin/tcsh -f

set testdir=""
if ! $?TESTDIR then
    echo "Setting default testdir to ./verilog"
    set testdir="./verilog"
else
    set testdir="${TESTDIR}"
endif
set tests="test_1.v test_2.v test_3.v function2.v sfifo_rtl.v"


setenv CARBON_BIN product
foreach i (${tests})
    set ctest="${testdir}/${i}"
    rm -f design.exe design_verific.exe design_interra.exe interra.txt verific.txt 
    cbuild -q ${ctest} -useVerific -testdriverDumpVCD >& /dev/null
    mv design.exe design_verific.exe
    ./design_verific.exe >& verific.txt
    rm design.exe -f
    cbuild -q ${ctest} -testdriverDumpVCD >& /dev/null
    mv design.exe design_interra.exe
    ./design_interra.exe >& interra.txt
    diff -q ./verific.txt ./interra.txt >& /dev/null
    if ($status != 0) then
        echo "${ctest} - FAIL"
    else
        echo "${ctest} - PASS"
    endif
end
