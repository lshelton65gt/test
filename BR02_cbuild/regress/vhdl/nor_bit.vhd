-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

--6.1.1 of testplan version 2.0
--nor operator with STD_LOGIC type
	
library IEEE;
use IEEE.std_logic_1164.all;

	entity nor_bit is 
		port(in1,in2 : STD_LOGIC;
				output : out STD_LOGIC);
end;

architecture nor_bit of nor_bit is
signal temp : STD_LOGIC; -- carbon observeSignal
begin
	temp <= in1 and in2;
	output <= temp and (in1 and in2);
end;
