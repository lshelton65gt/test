#!/bin/tcsh -f

set testdir=""
#echo "Setting default testdir to ./vhdl"
#set testdir="./vhdl"
set testdir="./"
set tests="sig_var.vhd dff.vhd"


setenv CARBON_BIN debug
foreach i (${tests})
    set ctest="${testdir}/${i}"
    set topname=$i:r
    rm -f design.exe design_verific.exe design_interra.exe interra.txt verific.txt 
    cbuild -q ${ctest} -useVerific -testdriverDumpVCD >& /dev/null
    mv design.exe design_verific.exe
    ./design_verific.exe >& verific.txt
    rm design.exe -f
    cbuild -q ${ctest} -testdriverDumpVCD -vhdlTop ${topname} >& /dev/null
    mv design.exe design_interra.exe
    ./design_interra.exe >& interra.txt
    diff -q ./verific.txt ./interra.txt >& /dev/null
    if ($status != 0) then
        echo "${ctest} - FAIL"
    else
        echo "${ctest} - PASS"
    endif
end
