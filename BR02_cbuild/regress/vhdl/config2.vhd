-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- 2 of test plan version 2.0
-- Configuration

entity config22 is
  port ( in1,in2 : integer;
         output : out integer
       );
end config22;

architecture sub of config22 is
begin
  process(in1,in2)
  begin
    if (in1>=in2) then
      output<= in1 + in2;
    else
      output<= in1 - in2;
    end if;
  end process;
end;

architecture add of config22 is
begin
  process (in1,in2) 
  begin
    output<= in1 + in2;
  end process;
end;

       

library work;
use work.all;
entity config2 is
  port ( in1,in2: integer;
         output : out integer
       );
end config2;

configuration my_config of config22 is
  --for add
  for sub
   end for;
end;

architecture config2 of config2 is
  component config22
   port ( in1,in2 : integer;
           output : out integer
        );
   end component;
   -- ragma synthesis_off
   for add1: config22 use configuration my_config;
   -- ragma synthesis_on
begin
  add1 : config22 port map (in1,in2,output);
end;


