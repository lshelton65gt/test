-- stall_down.vhdl    demonstrate stalling a pipeline where the registers
--                  in the pipeline accept data on a falling clock.

library IEEE;
use IEEE.std_logic_1164.all;

entity register_32 is                   -- accept data on falling clock
    port(clk    : in  std_logic;
                clear  : in  std_logic;
                       input  : in  std_logic_vector (31 downto 0);
                              output : out std_logic_vector (31 downto 0) );
    end entity register_32;

    architecture behavior of register_32 is
      begin  -- behavior
          reg_32: process(clk, clear)
                      begin
                                    if clear='1' then
                                                    output <= (others=>'0');
                                                                elsif clk='0' then
                                                                                output <= input after 1 ns;
                                                                                            end if;
                                                                                          end process reg_32;
                                                                                end architecture behavior;
-- of register_32

                                                                                library IEEE;
                                                                                use IEEE.std_logic_1164.all;

                                                                                entity mux_32 is
                                                                                    port(in0    : in  std_logic_vector (31 downto 0);
                                                                                                in1    : in  std_logic_vector (31 downto 0);
                                                                                                       ctl    : in  std_logic;
                                                                                                              result : out std_logic_vector (31 downto 0));
                                                                                    end entity mux_32;

                                                                                    architecture behavior of mux_32 is
                                                                                      begin
                                                                                          result <= in1 when ctl='1' else in0 after 1 ns;
                                                                                          end architecture behavior;
-- of mux_32

                                                                                          use STD.textio.all;
                                                                                          library IEEE;
                                                                                          use IEEE.std_logic_1164.all;
                                                                                          use IEEE.std_logic_textio.all;
                                                                                          use IEEE.std_logic_arith.all;

                                                                                          entity stall_down is
                                                                                            end entity stall_down;

                                                                                            architecture circuits of stall_down is
                                                                                                subtype word_32 is std_logic_vector(31 downto 0);
                                                                                                  signal zero_32   : word_32 := (others=>'0');
-- 32 bits of zero
                                                                                                  signal stall     : std_logic := '0';
-- stall signal
                                                                                                  signal sclk      : std_logic := '1';
-- stall clock

                                                                                                  signal clear     : std_logic := '1';
-- one shot clear
                                                                                                  signal clk       : std_logic := '1';
-- master clock

                                                                                                  signal R1        : word_32;
-- register 1
                                                                                                  signal R1_a      : word_32;
-- R1 after logic
                                                                                                  signal R2        : word_32;
-- register 2
                                                                                                  signal R3        : word_32;
-- register 3
                                                                                                  signal R2_mux    : word_32;
-- mux output

                                                                                                  signal cntr      : word_32 := x"00000000";
-- counter
                                                                                                begin
-- circuits of stall_down, top level architecture and test bench

                                                                                                    clock_gen: process(clk)
-- clock generator and one shot clear signal
                                                                                                                   begin
                                                                                                                                    if clear='1' then
-- happens only once
                                                                                                                                                       clear <= '0' after 500 ps;
                                                                                                                                                                      end if;
                                                                                                                                                                      clk <= not clk after 5 ns;
-- 10 ns period
                                                                                                                                                                    end process clock_gen;

                                                                                                                                                         cntr  <= unsigned(cntr)+unsigned'(x"00000001") after 1 ns
                                                                                                                                                                             when sclk'event and sclk='0';
-- counter run by same clock edge

                                                                                                                                                         stall <= '1' after 1 ns when R2=x"00000002" and R3=x"00000001"
                                                                                                                                                                             else '0' after 1 ns;

                                                                                                                                                         sclk  <= clk and not stall after 1 ns;


--  pipeline stages
                                                                                                                                                         R1_reg: entity work.register_32 port map(sclk, clear, cntr, R1);
                                                                                                                                                                 R1_a <= R1 and x"0FFFFFFF" after 1 ns;
-- any logic
                                                                                                                                                         R2_reg: entity work.register_32 port map(sclk, clear, R1_a, R2);
                                                                                                                                                         R3_reg: entity work.register_32 port map(clk,  clear, R2_mux, R3);
                                                                                                                                                         A2_mux: entity work.mux_32 port map(R2, zero_32, stall, R2_mux);


-- print processes for signals
                                                                                                                                                         stallpr : postponed process (stall)
-- not part of working circuit
                                                                                                                                                                                             variable my_line : line;
                                                                                                                                                                                                         begin
                                                                                                                                                                                                                         write(my_line, string'("stall="));
                                                                                                                                                                                                                                       write(my_line, stall);
                                                                                                                                                                                                                                       write(my_line, string'(" at "));
                                                                                                                                                                                                                                       write(my_line, now);
                                                                                                                                                                                                                                       writeline(output, my_line);
                                                                                                                                                                                                                                     end process stallpr;
                                                                                                                                                                                                                           clkpr   : postponed process (clk)
-- not part of working circuit
                                                                                                                                                                                                                                                               variable my_line : line;
                                                                                                                                                                                                                                                                           begin
                                                                                                                                                                                                                                                                                           write(my_line, string'("clk  ="));
                                                                                                                                                                                                                                                                                                         write(my_line, clk);
                                                                                                                                                                                                                                                                                                         write(my_line, string'(" at "));
                                                                                                                                                                                                                                                                                                         write(my_line, now);
                                                                                                                                                                                                                                                                                                         writeline(output, my_line);
                                                                                                                                                                                                                                                                                                       end process clkpr;
                                                                                                                                                                                                                                                                                             sclkpr  : postponed process (sclk)
-- not part of working circuit
                                                                                                                                                                                                                                                                                                                                 variable my_line : line;
                                                                                                                                                                                                                                                                                                                                             begin
                                                                                                                                                                                                                                                                                                                                                             write(my_line, string'("sclk ="));
                                                                                                                                                                                                                                                                                                                                                                           write(my_line, sclk);
                                                                                                                                                                                                                                                                                                                                                                           write(my_line, string'(" at "));
                                                                                                                                                                                                                                                                                                                                                                           write(my_line, now);
                                                                                                                                                                                                                                                                                                                                                                           writeline(output, my_line);
                                                                                                                                                                                                                                                                                                                                                                         end process sclkpr;
                                                                                                                                                                                                                                                                                                                                                               R1pr    : postponed process (R1)
-- not part of working circuit
                                                                                                                                                                                                                                                                                                                                                                                                   variable my_line : line;
                                                                                                                                                                                                                                                                                                                                                                                                               begin
                                                                                                                                                                                                                                                                                                                                                                                                                               write(my_line, string'("R1   ="));
                                                                                                                                                                                                                                                                                                                                                                                                                                             hwrite(my_line, R1);
                                                                                                                                                                                                                                                                                                                                                                                                                                             write(my_line, string'(" at "));
                                                                                                                                                                                                                                                                                                                                                                                                                                             write(my_line, now);
                                                                                                                                                                                                                                                                                                                                                                                                                                             writeline(output, my_line);
                                                                                                                                                                                                                                                                                                                                                                                                                                           end process R1pr;
                                                                                                                                                                                                                                                                                                                                                                                                                                 R2pr    : postponed process (R2)
-- not part of working circuit
                                                                                                                                                                                                                                                                                                                                                                                                                                                                     variable my_line : line;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 begin
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 write(my_line, string'("R2   ="));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               hwrite(my_line, R2);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               write(my_line, string'(" at "));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               write(my_line, now);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               writeline(output, my_line);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             end process R2pr;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   R3pr    : postponed process (R3)
-- not part of working circuit
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       variable my_line : line;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   begin
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   write(my_line, string'("R3   ="));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 hwrite(my_line, R3);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 write(my_line, string'(" at "));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 write(my_line, now);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 writeline(output, my_line);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               end process R3pr;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   end architecture circuits;
-- of stall_down
