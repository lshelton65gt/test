library ieee;
use ieee.std_logic_1164.all;

entity min_max IS
  generic (limit: INTEGER:= 255);
  PORT(ena: in BIT;
       inp1, inp2: in INTEGER range 0 to limit;
       min_out, max_out: out INTEGER range 0 to limit);
end min_max;
  

architecture my_architecture of min_max is

  procedure sort (signal in1, in2: in integer range 0 to limit;
                  signal min,max: out integer range 0 to limit) is
    begin
      if (in1 > in2) then
        max <= in1;
        min <= in2;
        else
          max <= in2;
          min <= in1;
        end if;
      end sort;
          

  begin
   process (ena)
     begin
       if (ena = '1')
         then sort (inp1,inp2,min_out, max_out);
       end if;
       end process;
  end my_architecture;
       
