-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.
-- Section 2 of the test plan
-- Configuration binding 
-- Configuration uses port map and generic map
         
entity config61_comp is
  generic( n: integer:=10);
  port(in11: integer;
       in12 : bit_vector(0 to 1);
       output1 : out integer;
       output2 : out bit_vector(0 to 1));
end;
architecture arch1 of config61_comp is
begin
  output1 <= in11 + n;
  output2 <= in12 ;
  
end;

architecture arch2 of config61_comp is
begin
  output1 <= in11 - n;
  output2 <= in12 ;
end;

configuration conf1_config61_comp  of config61_comp is
  for arch1
end for;
end;

configuration conf1_config61_comp of config61_comp  is
  for arch2
end for;
end;

entity config6_comp is
  generic( n: integer:=10);
  port(in11: integer:=10;
       in12 : bit_vector(0 to 1);
       output1 : out integer;
       output2 : out bit_vector(0 to 1));
end;
architecture arch of config6_comp is
  component comp1
    port(
      in11 : integer;
      in12: bit_vector(0 to 1);
      output1 : out integer;
      output2 : out bit_vector(0 to 1));
  end component;
begin
  inst1 : comp1 port map(in11,in12,output1,output2);
end;


entity config6 is
  port(in1: integer;
       in2 : bit_vector (0 to 1);
       output1 : out integer;
       output2 : out bit_vector(0 to 1));
end;
architecture arch of config6 is
  component comp
    generic ( n : integer:=40);
    port(
      in11 : integer :=30;
      in12: bit_vector(0 to 1);
      output1 : out integer;
      output2 : out bit_vector(0 to 1));
  end component;
  for inst1 : comp use entity work.config6_comp port map(in11=>in11,in12=>in12,output1=>output1,output2=>output2);
begin
  inst1 : comp generic map ( 50) port map (in1,in2,output1,output2);
end;
configuration conf1_config6 of config6 is
  for arch
    for inst1 : comp generic map ( 20);
  for arch
    for inst1 : comp1 use entity work.config6_comp port map(in11,in12=>in12,output1=>output1,output2=>output2);
end for;
end for;
end for;
end for;
end;
