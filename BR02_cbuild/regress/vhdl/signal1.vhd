-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- 3.5 of test plan version 1.0
-- This is to test signal declaration

use std.standard.all;

entity signal1 is
  port ( in1 : bit_vector ( 3 downto 0 ) ;
         in2 : bit_vector ( 3 downto 0 ) ;
         output : out bit_vector ( 3 downto 0 ) 
       ) ;
end ;

architecture signal1 of signal1 is
signal sum : bit_vector ( 3 downto 0 ) ;
begin
  process (in1, in2, sum)
  begin
     sum <= in1 and in2 ;
     output <= sum ;
  end process; 
end ;


