-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- 3.5.6 of test plan version 3.0
-- Signal Declaration of array of array 


package CONV_PACK_signal_decl6 is
 type row is array ( 0 to 3 ) of bit;
 type matrix is array ( 0 to 3 ) of row;
end CONV_PACK_signal_decl6;

package body CONV_PACK_signal_decl6 is
end CONV_PACK_signal_decl6;

use work.CONV_PACK_signal_decl6.all;
entity signal_decl6 is
port( in0, in1 , in2 , in3 : row;
      control              : integer range 0 to 3;
      out_mux              : out row);
end;

architecture signal_decl6 of signal_decl6 is
 signal in_matrix : matrix;
begin
 process(in0, in1, in2, in3, control,in_matrix)
 begin
  in_matrix <=  (0 => in0 , 1 => in1 , 2 => in2 , 3 => in3);
  out_mux   <=  in_matrix( control );
 end process;
end;
