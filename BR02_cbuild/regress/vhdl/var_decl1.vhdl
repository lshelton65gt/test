-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- 3.6.1 of test plan version 3.0
-- variable Declaration of integer types 

entity var_decl1 is
 port( signal in1  :  in integer ;
			  in2  :  in integer ;
       signal out1 : out integer ;
       signal out2 : out integer );
end;
 
architecture var_decl1 of var_decl1 is
begin
 process(in1, in2)
  variable var1, var2 : integer;
 begin
   var1 := in2 + 5;
   var2 := in2 + 6;
   out1  <=  in1 + var1 ;
   out2  <=  in1 + var2 ;
 end process;
end;
