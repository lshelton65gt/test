#!/bin/bash

if [ "${1}" == "" ];
then
  echo "You must provide an argument that is the path of the CMS installation to be updated."
  exit
else
  if [ ! -d $1 ];
  then
    echo "You must provide an argument that is the path of the CMS installation to be updated."
    echo "${1} is not a directory"
    exit
  else
    export CMS_INSTALL_PATH="${1}"
  fi
fi

#####################
# derive paths based on where this script resides
#####################
mypath=${0}
mypath_dirname=`dirname ${mypath}`
pushd ${mypath_dirname} > /dev/null
export CBUILD_BUILD_DIR=`pwd`
popd > /dev/null

# source a script for setup common to both build script, and this "update" script
source ${CBUILD_BUILD_DIR}/CMS-nightly-setup.sh
# ${CBUILD_BUILD_DIR}/CMS-nightly-setup-inspect.sh

logname=${CMS_nightly_log_dir}/CMS-update-build.log

# If we're about to update an installation of CMS under /o/release, then we're going to
# be extra paranoid.
let len=${#CMS_INSTALL_PATH}
if [ $len -gt 10 ];
then
  pathBegin="${1:0:10}"
  if [ "${pathBegin}" == "/o/release" ];
  then
    if [ "${HOSTNAME}" != "farm223" ];
    then
      echo "Updating an installtion under /o/release should only be run on farm223"
      exit
    fi
    if [ "${USER}" != "qa" ];
    then
      echo "Updating an installtion under /o/release should only be run by the qa account"
      exit
    fi
  fi
fi


if [ -e ${CARBON_HOME}/BUILD_IN_PROGRESS ];
then
  #
  # This script should be running as part of a cron job that is scheduled for 1:05am
  # The requirement is that the target directory to be updated under /o/release/CMS
  # NOT be updated EXCEPT between 1:00am and 3:00am.  So, if the build is still in 
  # progress, we'll check two more times at 45 minute intervals (1:50am & 2:35am)
  # 
  now=`date`
  echo "The nightly build was still in progress at ${now}.  Sleeping 45 minutes." >> ${logname} 
  sleep 2700
  if [ -e ${CARBON_HOME}/BUILD_IN_PROGRESS ];
  then
    now=`date`
    echo "The nightly build was still in progress at ${now}.  Sleeping another 45 minutes." >> ${logname} 
    sleep 2700
  fi
fi

if [ -e ${CARBON_HOME}/BUILD_IN_PROGRESS ];
then
  now=`date`
  echo "The nightly build was still in progress at ${now}, so the update to /o/release did not happen." >> ${logname} 
else
  if [ -e ${CARBON_HOME}/BUILD_PASSED ];
  then
    echo "UPDATING ${CMS_INSTALL_PATH}" >> ${logname}
    echo "Starting at:" >> ${logname}
    echo `date` >> ${logname}
    echo ""  >> ${logname}
  
    export CMS_INSTALL_PATH_new="${CMS_INSTALL_PATH}-new"
    export CMS_INSTALL_PATH_old="${CMS_INSTALL_PATH}-old"
  
    mkdir $CMS_INSTALL_PATH_new
    tar xzf ${CARBON_HOME}/carbon-release-Linux-${SANDBOX_DIRNAME}.tgz -C ${CMS_INSTALL_PATH_new}/
    tar xzf ${CARBON_HOME}/carbon-release-Windows-${SANDBOX_DIRNAME}.tgz -C ${CMS_INSTALL_PATH_new}/
    cp -R ${CBUILD_CHECKOUT_DIR}/scripts ${CMS_INSTALL_PATH_new}/
  
    mv $CMS_INSTALL_PATH $CMS_INSTALL_PATH_old
    mv $CMS_INSTALL_PATH_new $CMS_INSTALL_PATH
  
    echo "New build available at:" >> ${logname}
    echo `date` >> ${logname}
    echo ""  >> ${logname}
  
    rm -rf $CMS_INSTALL_PATH_old
    echo "Old build done being removed at:" >> ${logname}
    echo `date` >> ${logname}
    echo ""  >> ${logname}

    mail -s "PATH UPDATED: ${CMS_INSTALL_PATH}" build@carbondesignsystems.com
    mail -s "${CMS_INSTALL_PATH} is updated" gnovak@carbondesignsystems.com < ${logname}

  else
  
    mail -s "PATH **NOT** UPDATED: ${CMS_INSTALL_PATH}" build@carbondesignsystems.com
    echo "The nightly build failed, so the update to /o/release did not happen." >> ${logname} 
    mail -s "${CMS_INSTALL_PATH} is updated" gnovak@carbondesignsystems.com < ${logname}
  
  fi
fi
