#!/bin/bash -e

# Validate a published distribution by comparing to a previous release.
# $1 is where release directories can be found, probably /o/release/CMS
# CARBON_RELEASE_NAME is assumed to be set by hudson (probably via the setup.sh file)
# CARBON_OLD_RELEASE_NAME is assumed to be set by hudson (probably via the setup.sh file)

# If there is a setup script in the CARBON_SANDBOX, use that. It is only
# there for newer jobs that use the new style. This will
# set up the above env variables
if [ "$CARBON_SANDBOX" != "" -a -f $CARBON_SANDBOX/setup.sh ]; then
    source $CARBON_SANDBOX/setup.sh
fi

if [ x$CARBON_RELEASE_NAME = 'x' ]; then
  echo "CARBON_RELEASE_NAME not set"
  exit 1
fi

if [ x$CARBON_OLD_RELEASE_NAME = 'x' ]; then
  echo "CARBON_OLD_RELEASE_NAME not set"
  exit 1
fi


old=$CARBON_OLD_RELEASE_NAME
oldsuffix=$CARBON_OLD_RELEASE_NAME
new=$CARBON_RELEASE_NAME
newsuffix=$CARBON_RELEASE_NAME

echo "CMS-hudson-validate-distro: compares contents of tar files in distributions:"
echo "  old distribution:   $1/$old/INSTALL"
echo "  new distribution:   $1/$new/INSTALL"

rm -fr $old
mkdir -p $old

rm -fr $new
mkdir -p $new

rm -fr diffs
mkdir -p diffs

rm -fr FNF.txt

havefnf=0
for file in $1/$old/INSTALL/* ; do
  thisnewbasename=`basename $file | sed "s/$oldsuffix/$newsuffix/g"`
  thisnew="$1/$new/INSTALL/$thisnewbasename"
  if [ ! -f $thisnew ]; then
    echo "FILE NOT FOUND: " $thisnew >> FNF.txt
    havefnf=1
  fi
done

havediff=0
for file in $1/$old/INSTALL/*tgz ; do
  thisbasename=`basename $file $oldsuffix.tgz`
  thisold="$1/$old/INSTALL/$thisbasename$oldsuffix.tgz"
  thisnew="$1/$new/INSTALL/$thisbasename$newsuffix.tgz"
  tar tzf $thisold | sort | grep -v ".pyc" >& $old/$thisbasename.out
  if [ -f $thisnew ]; then
    tar tzf $thisnew | sort | grep -v ".pyc" >& $new/$thisbasename.out
    # Allow diff to fail
    set +e
    diff $old/$thisbasename.out $new/$thisbasename.out >& diffs/$thisbasename.diff
    if [ $? -ne 0 ]; then
      havediff=1
    fi
    set -e
  fi
done

retstatus=0
if [ $havefnf -ne 0 ]; then
  echo "FAILED: New distribution missing files:"
  cat FNF.txt
  retstatus=1
else
  echo "PASSED: No files missing from distribution."
fi

if [ $havediff -ne 0 ]; then
  echo "FAILED: Diffs found:"
  for file in diffs/*.diff ; do
    if [ -s $file ]; then
      echo "DIFF: " $file
      cat $file
    fi
  done
  retstatus=1
else
  echo "PASSED: No diffs in tgz file contents."
fi

exit $retstatus
