#!/bin/bash

for arg in $*;
do
   case $arg in

        CLEAN_CARBON_HOME )
            export CLEAN_CARBON_HOME="yes" ;;

        USE_NETAPP_CH )
            export USE_NETAPP_CH="yes" ;;

        NONOTIFY )
	    export NOTIFY="no" ;;

        NOOP )
	    export noop="yes" ;;

        *   )
            echo "unknown argument, ${arg}, exitting!"
            exit ;;
    esac
done

if [ "${USE_NETAPP_CH}" != "yes" ];
then
  export USE_NETAPP_CH="no"
fi

#####################
# derive paths based on where this script resides
#####################
mypath=${0}
mypath_dirname=`dirname ${mypath}`
pushd ${mypath_dirname} > /dev/null
export CBUILD_BUILD_DIR=`pwd`
popd > /dev/null

# source a script for setup common to both build script, and this "update" script
source ${CBUILD_BUILD_DIR}/CMS-nightly-setup.sh $USE_NETAPP_CH

#####################
# clean out CARBON_HOME if requested via command line switch
#####################
if [ -d $CARBON_HOME ];
then
  if [ "$CLEAN_CARBON_HOME" == "yes" ];
  then
    rm -rf ${CARBON_HOME}/*
  fi
fi

# create the log directory if its not there already
if [ ! -d $CMS_nightly_log_dir ];
then
  mkdir -p $CMS_nightly_log_dir
fi
logname=${CMS_nightly_log_dir}/CMS-nightly-build.log
antlogname=${CMS_nightly_log_dir}/ant-BuildAndTestLinux.log

echo "Starting script at:" > ${logname}
echo `date` >> ${logname}
echo ""  >> ${logname}

touch ${CARBON_HOME}/BUILD_IN_PROGRESS

#####################
# run the build with the ant script
#####################
cd $CBUILD_BUILD_DIR
if [ "${noop}" == "yes" ];
then
  echo "ant BuildAndTestLinux -listener net.sf.antcontrib.perf.AntPerformanceListener" >& ${antlogname}
else
  ant BuildAndTestLinux -listener net.sf.antcontrib.perf.AntPerformanceListener >& ${antlogname}
fi

if [ $? -ne 0 ];
then
  touch ${CARBON_HOME}/BUILD_FAILED
else
  touch ${CARBON_HOME}/BUILD_PASSED
fi
rm ${CARBON_HOME}/BUILD_IN_PROGRESS

echo "Finished script at:" >> ${logname}
echo `date` >> ${logname}

if [ ! "${NOTIFY}" == "no" ];
then
  if [ -e ${CARBON_HOME}/BUILD_PASSED ];
  then
    `mail -s CMS_build_wrapper_log_from_${HOSTNAME} gnovak@carbondesignsystems.com < ${logname}`
    `mail -s CMS_ant_build_log_from_${HOSTNAME} gnovak@carbondesignsystems.com < ${antlogname}`
    `mail -s CMS_NIGHTLY_BUILD_PASSED build@carbondesignsystems.com < ${antlogname}`
  else
    `mail -s CMS_build_wrapper_log_from_${HOSTNAME} gnovak@carbondesignsystems.com < ${logname}`
    echo "For individual ant target log files, look in ${CMS_nightly_log_dir}" > ${antlogname}-new
    cat ${antlogname} >> ${antlogname}-new
    mv ${antlogname}-new ${antlogname}
    `mail -s CMS_NIGHTLY_BUILD_FAILED eng@carbondesignsystems.com < ${antlogname}`
  fi
fi

# drop off the user at the same place they started.
cd ${cwd}


#    commenting out the rest of this script because we'll push everynight, regardless of whether
#    or not the source tree in cvs has had any updates.  Something on /o/tools may be different.
#    
#    #####################
#    # install the build
#    #####################
#    export CMS_NIGHTLY_INSTALLATION_DIR=${CBUILD_CHECKOUT_DIR}/installation
#    mkdir $CMS_NIGHTLY_INSTALLATION_DIR
#    cd $CMS_NIGHTLY_INSTALLATION_DIR
#    tar xzf ${CARBON_HOME}/carbon-release-Linux-${SANDBOX_DIRNAME}.tgz
#    tar xzf ${CARBON_HOME}/carbon-release-Windows-${SANDBOX_DIRNAME}.tgz
#    
#    #####################
#    # get the build version
#    #####################
#    unset CARBON_BIN
#    export CARBON_HOME=${CMS_NIGHTLY_INSTALLATION_DIR}
#    $CARBON_HOME/bin/cbuild -verboseVersion >> ${logname}
#    

