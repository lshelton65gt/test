#!/bin/bash

# setup the common environment variables that will be used by
# both the nightly build and the nightly update scripts.  There
# are two separate scripts because the update can only happen 
# between 1:00am and 3:00am

# this script should get run via the "source" command

export USE_NETAPP_CH=$1

#####################
# derive paths based on where this script resides
#####################
mypath=${0}
mypath_dirname=`dirname ${mypath}`
cd ${mypath_dirname}
export CBUILD_BUILD_DIR=`pwd`
cd ..
export CBUILD_CHECKOUT_DIR=`pwd`
cd ${CBUILD_BUILD_DIR}

#####################
# generate CARBON_HOME to be on a local disk or on the NetApp disk (/home/cds/<user>/...)
#####################
export SANDBOX_DIRNAME=`basename ${CBUILD_CHECKOUT_DIR}`
if [ "${USE_NETAPP_CH}" == "yes" ];
then
  export CARBON_HOME=${CBUILD_CHECKOUT_DIR}/CARBON_HOME
else
  export CARBON_HOME=/w/${HOSTNAME}/${USER}/${SANDBOX_DIRNAME}
fi
echo "CARBON_HOME=${CARBON_HOME}"
export CARBON_BIN=product

export CMS_nightly_log_dir=${CARBON_HOME}/LOGS
