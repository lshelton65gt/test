import sys
import Carbon.DB

def compute(argv):
    db = Carbon.DB.DBContext(argv[1])
    if db:
        if db.isReplayable() == 1:
            print "-DCARBON_REPLAY"

if __name__ == "__main__":
    compute(sys.argv)
