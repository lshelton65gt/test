entity procedure2 is
  port (in1, in2, in3: in bit;  out1: out bit);
end;

architecture a_top of procedure2 is

        procedure arc (signal a1:in bit; signal a2:in bit;signal  a3: in bit; variable b: out bit) is 
        begin
                b :=  a1 and a2 and a3;
        end;

begin
   process (in1)
   variable tmp:bit;
   begin
      arc(in1, in2, in3, tmp);
      out1 <= tmp;
   end process;
  
end;

