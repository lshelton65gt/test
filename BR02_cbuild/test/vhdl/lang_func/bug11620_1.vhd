-- Testcase 1 for codegen error caused by passing an integer as actual
-- to a procedure whose formal is a positive

library ieee;
use ieee.std_logic_1164.all;

entity bug11620_1 is
  port (
    i : in  positive;
    o : out integer);
end bug11620_1;

architecture arch of bug11620_1 is

  procedure plus1 (
    variable i : positive;
    variable o : out positive) is
  begin
    o := i + 1;
  end;

begin

  process (i)
    variable temp_i : positive;
    variable temp_o : integer;
  begin

    temp_i := i;
    plus1(temp_i, temp_o);
    o <= temp_o;
  end process;
  
end arch;
