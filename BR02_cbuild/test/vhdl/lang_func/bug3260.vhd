
entity bottom is
  port (i1, i2 : in bit_vector(0 to 7); o1: out bit_vector(0 to 7));
end bottom;

architecture arch of bottom is
begin
  o1 <= i1 and i2;
end;

  
entity bug3260 is
  port (in1, in2 : in bit_vector(0 to 7); out1: out bit_vector(0 to 7));
end;

architecture arch of bug3260 is
component bottom is
  port (i1, i2 : in bit_vector(0 to 7); o1: out bit_vector(0 to 7));
end component;
    
function f1( a : bit_vector) return bit_vector is
begin 
  return a;
end;

function f( a : bit_vector) return bit_vector is
begin
  return a;
end;
signal temp : bit_vector(0 to 7);
begin
  temp <= f(f1(in2));
  mid_inst : bottom port map (f1(in1), temp, out1);
end;

