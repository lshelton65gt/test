library ieee;
use ieee.numeric_bit.all;

entity multidim_unop is
  port (
    din1, din2   : in  bit_vector(7 downto 0);
    dout1, dout2 : out bit_vector(7 downto 0));
end multidim_unop;

architecture arch of multidim_unop is
  type twodee is array (0 to 1) of bit_vector(7 downto 0);
  signal sig1, sig3 : twodee;

  function "-" ( L: twodee) return twodee is
    variable retval : twodee;
  begin
    retval(0) := bit_vector( -signed(L(0)));
    retval(1) := bit_vector( -signed(L(1)));
    return retval;
  end "-";
begin

  sig1 <= ( din1, din2 );
  sig3 <= -sig1;
  dout1 <= sig3(0);
  dout2 <= sig3(1);
end arch;
