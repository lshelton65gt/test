-- testcase to demonstrate that extracting a single bit from a function with no
-- arguments is confusing to cbuild.  (inspired by  matrox/phoenix/carbon/cwtb/ps_top)
-- see test/langcov/TaskFunction/reset_split.v for a verilog version of this problem.
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity bug3843 is
  port (
    clkbuf  : in     std_logic;
    rsmode  : in     std_logic;
    rsrdy   : in     std_logic;
    rstN    : in     std_logic;
    outp    : out    std_logic
    );
end bug3843;


architecture functional of bug3843 is

  function initrs return std_logic_vector is
    variable resetval : std_logic_vector(31 downto 0);

  begin
    resetval := (others => '1');
    return resetval;

  end initrs;

  signal wkregld      : std_logic;
  signal dbregrstpdst : std_logic;
  
begin
  
  rsdwgcmdrstpdstreg : process (clkbuf, rstN)
  begin
    if (rstN = '0') then
      dbregrstpdst   <= initrs(0);
    elsif (clkbuf'event and clkbuf = '1') then
        dbregrstpdst <= '1';
    end if;
  end process rsdwgcmdrstpdstreg;

  outp <= dbregrstpdst;
end functional;
