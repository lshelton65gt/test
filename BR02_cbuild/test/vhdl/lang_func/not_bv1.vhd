 entity not_bv1 is
                port(in1 : bit_vector(0 to 7);
                                output : out bit_vector(0 to 7));
end;

architecture not_bv1 of not_bv1 is
signal temp : bit_vector(0 to 7);
begin
                output <= not (in1);
end;

