-- inspired by bug3317, here we have a function call with single bit selection
-- (a index?)
library ieee;
use ieee.numeric_bit.all;

entity func_with_index_1 is
  port (in1,in2: in unsigned(0 to 7);
        out1: out unsigned(0 to 1));
end;

architecture arch of func_with_index_1 is
  function foo (a,b : unsigned) return unsigned is
  begin
    return a + b;
  end;
  
begin
  out1 <= foo(in1,in2)(1) & foo(in1,in2)(2);
end;

