library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity bug5168 is
  
  port (
    i : in  integer;
    o : out std_logic_vector (5 downto 0));

end bug5168;

architecture rtl of bug5168 is

begin  -- rtl

  o <= conv_std_logic_vector(i, 6) (5 downto 0);
  
end rtl;
