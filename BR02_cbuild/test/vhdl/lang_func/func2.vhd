entity func2 is
  port (in1: in bit; out2, out1: out bit);
end;

architecture a_top of func2 is

        function ent (a : bit) return bit is
        begin
                return a;
        end;

        function arc (a : bit) return bit is
        begin
                return a;
        end;

begin
                out1 <= arc(in1);
     --           out2 <= ent(in1 and in1);
end;

