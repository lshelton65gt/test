library ieee;
use ieee.numeric_bit.all;

entity func_max_return is
  port (in1,in2: in unsigned(0 to 7);
        out1: out unsigned(0 to 7));
end;

architecture arch of func_max_return is
  function foo (a,b : unsigned) return unsigned is
  begin
    return a + b;
  end;
begin
  out1 <= foo(in1,in2);
end;

