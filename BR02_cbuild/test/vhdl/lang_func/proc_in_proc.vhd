use std.standard.all;

entity proc_in_proc is
  port ( in1 : bit  ;
         in2 : bit ;
         in3 : bit ;
         output : out bit
       );
end ; 

architecture proc_in_proc of proc_in_proc is
 procedure sum 
    ( signal in1 : in bit ;
      signal in2 : in bit ;
      signal in3 : in bit ;
      signal output : out bit
    ) is
    -- a procedure inside a procedure
    procedure in_sum
        ( signal in1 : in bit ;
          signal in2 : in bit ;
          signal in3 : in bit ;
          signal output : out bit
        ) is
     begin
       output <= in1 and in2 and in3;
    end ;

 begin
  in_sum(in1, in2, in3, output);
end ;

begin
 process ( in1, in2, in3)
 begin
    sum(in1,in2,in3, output);
 end process;
end ;
  
