use std.standard.all ;

package p_function_scope2 is
  function sum
    ( signal in1 : bit ;
      signal in2 : bit
    ) return bit ;
end ;
package body p_function_scope2 is
function sum
  ( signal in1 : bit ;
    signal in2 : bit
  ) return bit is
begin
  return  in1 or in2;
end ;
end;
 
use work.p_function_scope2.all;

entity function_scope2 is
  port ( in1 : bit ;
         in2 : bit ;
         output1 : out bit;
         output2 : out bit;
         output3 : out bit
       ) ;
end ;

architecture function_scope2 of function_scope2 is
   function sum
     ( signal in1 : bit ;
       signal in2 : bit
     ) return bit is
   begin
     return  in1 and in2;
   end ;

begin
   output1 <= sum ( in1, in2 ); -- architecture scope


   process (in1, in2)
      function sum
        ( signal in1 : bit ;
          signal in2 : bit
        ) return bit is
      begin
        return  in1 xor in2;
      end ;
   begin
      output3 <= sum(in1, in2); -- process scope
   end process;

   process (in1, in2)
   begin
     output2 <= sum(in1, in2); -- architecture scope
   end process;

end; 
