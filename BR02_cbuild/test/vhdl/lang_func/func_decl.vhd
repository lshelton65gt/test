entity func_decl is
  port (in1,in2: in bit; out1, out2: out bit);
end;

architecture a_top of func_decl is

        function ent (a : bit) return bit; -- function declaration
        function ent (a : bit) return bit is
        begin
                return a;
        end;

        function arc (a : bit) return bit is
        begin
                return a;
        end;

begin
                out1 <= arc(in1);
                out2 <= ent(in1 and in2);
end;

