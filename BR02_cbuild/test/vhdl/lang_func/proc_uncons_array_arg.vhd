entity proc_uncons_array_arg is
  port (in1: in bit_vector(3 downto 0); 
        in2 : in bit_vector(4 downto 0); 
        in3 : in bit_vector(3 downto 0); 
        out1: out bit_vector(3 downto 0);
        out2: out bit_vector(5 downto 0);
        out3: out bit_vector(3 downto 0));
end;

architecture arch of proc_uncons_array_arg is
  procedure foo (a : in bit_vector; signal o : out bit_vector) is
  begin
    o <= a;
  end;
  procedure foo (a : bit_vector; signal o : out bit) is
  begin
    o <= a(a'left);
  end;
  procedure foo  is
  begin
    assert FALSE
           report "This is just a check for assert"
           severity NOTE;
  end;

begin
  blk: block
  begin
    foo(in1,out1);  
    foo(in2,out2(4 downto 0));
  end block blk;

  foo(in2,out2(5)); 
  process(in3)
  begin
    foo(in3,out3);  
  end process;
end;

