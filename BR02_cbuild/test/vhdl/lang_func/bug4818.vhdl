library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package bug4818_pkg is

 function mylog2 (A : integer) return integer;
 function extunsigned(Arg : unsigned; Size : integer) return unsigned;

 constant msize : integer := 1024;
 constant mwidth	: integer := mylog2 (msize);

end package bug4818_pkg;

package body bug4818_pkg is


  function mylog2 (A : integer) return integer is
    variable vLog2 : integer;
  begin
    vLog2 := 0;
    for I in 0 to 100 loop
      if 2**vLog2<A then
        vLog2 := I+1;
      end if;
    end loop;
    return vLog2;
  end function mylog2 ;

  function extunsigned(Arg : unsigned; Size : integer) return unsigned is
  begin
    return unsigned(ext(std_logic_vector(Arg), Size));
  end function extunsigned;


end package body bug4818_pkg;

-- -----------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;


library work;
use work.bug4818_pkg.all;


entity bug4818 is
  port (nRst		: in std_ulogic;
        Clk 		: in std_ulogic;
        inbus1	: in std_logic_vector(mwidth-1 downto 0);
  	inbus2		: in integer range 0 to 2**11-1;
	outbus1	: out std_logic_vector(mwidth-1 downto 0)

);
end entity bug4818;


architecture rtl of bug4818 is


begin

  tcproc : process (nRst, Clk)
    variable inbus2uns : unsigned(10 downto 0);
  begin

    if nRst='0' then
      outbus1 <= (others => '0');

    elsif Clk'event and Clk='1' then
            inbus2uns := conv_unsigned(inbus2, 11);
            outbus1 <= extunsigned(inbus2uns(10 downto 2), mwidth)+ unsigned(inbus1);
    end if;

  end process tcproc;

end architecture rtl;

