library ieee;
use ieee.std_logic_1164.all;
package p1 is
 subtype mm is std_logic_vector(0 to 4);
 function func ( var1 : mm ) return mm;
end p1;
package body p1 is
 function func (var1 : mm) return mm is
   variable Result : std_logic_vector(0 to 4) := "11101";
 begin
   return Result;
 end;
end p1;

library IEEE;
use IEEE.std_logic_1164.all;
entity init_func_var1 is
   port( input : in std_logic_vector(0 to 4);
         output : out std_logic_vector(0 to 4));
end ;
library work;
use work.p1.all;
architecture init_func_var1 of init_func_var1 is
begin
   process(input)
   begin
       output <= func(input);
   end process;
end ;

