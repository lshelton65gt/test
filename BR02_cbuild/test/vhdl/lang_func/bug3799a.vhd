entity bug3799a is
  port(in1, in2 : in bit_vector(0 to 1); out1, out2 :  out bit_vector(0 to 1) );
end;

architecture arch of bug3799a is
  type bool_vector is array (natural range <> ) of boolean;
  function foo (in1, in2 : bool_vector) return bool_vector is
  variable v : bool_vector(in1'range);
  begin
    for i in in1'range loop
      v(i) := in1(i) or in2(i);
    end loop;
    return v;
  end;
  function foo (in1, in2 : bit_vector) return bit_vector is
  variable v : bit_vector(in1'range);
  begin
    v := in1 and in2;
    return v;
  end;
  function bit2bool (in1 : bit_vector) return bool_vector is
  variable v : bool_vector(in1'range);
  begin
    for i in in1'range loop
      if (in1(i) = '0' ) then
        v(i) := FALSE;
      else
        v(i) := TRUE;
      end if;
    end loop;
    return v;
  end;
  function bool2bit (in1 : bool_vector) return bit_vector is
  variable v : bit_vector(in1'range);
  begin
    for i in in1'range loop
      if (in1(i) = FALSE ) then
        v(i) := '0';
      else
        v(i) := '1';
      end if;
    end loop;
    return v;
  end;
  begin
    out1 <= foo(in1,in2);
    out2 <= bool2bit(foo(bit2bool(in1), bit2bool(in2)));
  end;
