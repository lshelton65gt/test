-- This tests for correct scoping of a procedure declared in a process, with
-- the net name in question, doffset, having multiple instances.
library IEEE;
use IEEE.std_logic_1164.all;

entity bug5395_1 is
  port ( CLOCK, din: in STD_LOGIC;
         dout : out std_logic_vector(1 downto 0) );
end;


architecture Arch_top of bug5395_1 is
  signal  OFFSET_REG: std_logic;
  signal dOFFSET : std_logic;
begin

  offset_reg <= din;

  exec_all_process  :  process
    variable dOFFSET : std_logic;

    procedure P_OFFSET is
    begin
      dOFFSET := OFFSET_REG;
      dout(0) <= doffset;
    end P_OFFSET;
  begin
    wait until CLOCK'event and CLOCK='1';
    P_OFFSET;
  end process exec_all_process;

  doffset <= '0';
  dout(1) <= doffset;

end Arch_top;
