-- last mod: Wed Feb  1 16:35:33 2006
-- filename: test/vhdl/lang_func/named_not.vhd
-- Description:  This test checks the support for calling functions by
-- name. like  "not"(i1)

library ieee;
use ieee.std_logic_1164.all;

entity named_not is
  port (
    in1      : in  std_logic_vector(7 downto 0);
    out1     : out std_logic_vector(7 downto 0));
end;

architecture arch of named_not is
begin
  out1 <= "not"(in1);
end;
