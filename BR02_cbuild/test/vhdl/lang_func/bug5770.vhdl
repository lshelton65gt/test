-- This test came from Matrox's use of DesignWare.  We were not expecting to
-- encounter a unary op when determining if the return value of DWF_absval was
-- an integer subrange or not.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug5770 is
  port (
    din      : in  signed(7 downto 0);
    dout     : out signed(7 downto 0);
    clk, rst : in  std_logic);
end bug5770;

architecture arch of bug5770 is

    function DWF_absval(A: SIGNED) return SIGNED is
      -- pragma map_to_operator ABS_OP
      -- pragma type_function SIGNED_ARG
      -- pragma return_port_name Z
    begin
        return(ABS(A));
    end;
begin

  p1: process (clk, rst)
  begin
    if rst = '0' then
      dout <= ( others => '1' );
    elsif clk'event and clk = '1' then
      dout <= DWF_absval( din );
    end if;
  end process p1;
end arch;
