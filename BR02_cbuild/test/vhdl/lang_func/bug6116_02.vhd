-- last mod: Wed Jul  5 08:13:12 2006
-- filename: test/vhdl/lang_func/bug6116_02.vhd
-- Description:  This test is a variation on the original from bug6116, modified
-- to duplicate a new internal error reported from the customer

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6116_02 is
port (
	AddrInt : in integer range 0 to 2;
	MemPtr1 : in std_logic_vector (0 to 17);
	MemPtr2 : in std_logic_vector (17 downto 0);
	RdData1 : out std_logic_vector(15 downto 0);
	RdData2 : out std_logic_vector(15 downto 0)
);
end entity;

architecture rev1 of bug6116_02 is

  -- Read back register bits starting from RightIdx. Len is used to define the
  -- bits of the vector that are used by this capability class. This prevents
  -- accessing unused high-bits in lower capability classes.
  function RdBits1(constant Reg : in std_logic_vector;
                  constant Len : integer;
                  constant RightIdx : in integer)
    return std_logic_vector is
  constant cZero : std_logic_vector(31 downto 0) := "00000000000000000000000000000000";
  begin
    if Len < RightIdx then
      return ext(Reg(Len-1 downto RightIdx), 16);
    else
      return cZero(15 downto 0);
    end if;
  end function RdBits1;

  function RdBits2(constant Reg : in std_logic_vector;
                  constant Len : integer;
                  constant LeftIdx : in integer)
    return std_logic_vector is
  constant cZero : std_logic_vector(31 downto 0) := "00000000000000000000000000000000";
  begin
    if Len < LeftIdx then
      return ext(Reg(LeftIdx to Len-1), 16);  -- this line must not execute!
    else
      return cZero(15 downto 0);
    end if;
  end function RdBits2;

begin
  RdData1 <= RdBits1(MemPtr1, 16, 0);
  RdData2 <= RdBits2(MemPtr2, 16, 0);
end architecture;
