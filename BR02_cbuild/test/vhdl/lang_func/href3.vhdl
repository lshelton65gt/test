-- Copyright (c) 2002, 2005 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- Section 1.8.3 of the test plan
-- Hierarchical reference to function inside architecture

       library ieee;
       use ieee.std_logic_1164.all;
       package pack1 is
         function func ( var1 : std_logic ) return std_logic;
       end pack1;
       package body pack1 is
         function func (var1 : std_logic) return std_logic is
           variable Result : std_logic := '0';
         begin
           Result := var1;
           return Result;
         end;
       end pack1;
         library IEEE;
         use IEEE.std_logic_1164.all;
         entity href3 is
             port( input : in std_logic;
                   output : out std_logic);
         end href3;
         library work;
         use work.pack1.all;
         architecture arch of href3 is
         begin
             process(input)
             variable var1 : std_logic;
             begin
                        var1 := input;
                 output <= work.pack1.func(var1);
             end process;
         end arch;
