-- Enum type access


package enum1_pack is
        type etype is (idle, active);
end enum1_pack;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.enum1_pack.all;

entity enum1 is
        port(
                clk: in std_logic;
                out2 : out etype;
                out1 : out etype
            );
end enum1;

architecture arch_enum1 of enum1 is
begin
  process (clk)
  begin
        if (clk = '1' and clk'event) then
            out1 <= idle;
            out2 <= active;
         end if;
  end process;
end arch_enum1;

