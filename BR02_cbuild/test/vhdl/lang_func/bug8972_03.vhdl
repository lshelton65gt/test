-- June 2008
-- This test case was created based on customer's code.
-- The problem was with a slice of a vector in a loop, the bound of which is
-- calculated with function calls.

library ieee;
use ieee.std_logic_1164.all;

package A_func_pkg is
  constant PEL_W              : natural := 8;
  constant SEC_W              : natural := 4;


  function get_msb(x : integer; y : integer) return natural;
  function get_lsb(x : integer; y : integer) return natural;

end package A_func_pkg;

package body A_func_pkg is

  function get_lsb(x : integer; y : integer) return natural is
    variable lsb : natural;
    variable coord : natural;
  begin
 
      coord := 8*x+y; -- calculate coordinate as decimal number for easy reading
      case (coord) is
      when 00   => lsb := 0;
      when 01   => lsb := 4;
      when 08   => lsb := 8;
      when 09   => lsb := 12;
      when 16   => lsb := 16;
      when 17   => lsb := 20;
      when others => null; -- may not occur
      end case;

    return lsb;
  end function get_lsb;

 
  function get_msb(x : integer; y : integer) return natural is
  begin
    return get_lsb(x,y)+SEC_W-1;
  end function get_msb;

end package body;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
library work;
use work.A_func_pkg.all;

entity bug8972_03 is
  port (
       clk   : in  std_logic;
       rst   : in  std_logic;
       in1   : in  std_logic_vector(PEL_W*3-1 downto 0);
       out1  : out std_logic_vector(PEL_W*3-1 downto 0)
  );
end;

architecture rtl of bug8972_03 is
  signal temp         : std_logic_vector(PEL_W*3-1 downto 0);
begin

  
  p1: process (clk, rst)
  begin  -- process p1
    if rst = '0' then                   
      temp    <= (others => '0');
    elsif clk'event and clk = '1' then  
      for x in 0 to 2 loop
        for y in 0 to 1 loop
          -- the line below is tested
          temp(get_msb(x,y) downto get_lsb(x,y)) <= in1(get_msb(x,y) downto get_lsb(x,y)); 
        end loop;
      end loop;
    end if;
  end process p1;

  out1 <= temp;
  
end architecture rtl;
