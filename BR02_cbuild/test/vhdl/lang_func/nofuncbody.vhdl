package pack is
  function func (constant inp : bit) return bit;
end pack;

use work.pack.all;

entity nofuncbody is
  port (
    in1  : in  bit_vector(3 downto 0);
    out1 : out bit_vector(3 downto 0));
end nofuncbody;

architecture arch of nofuncbody is

begin

  out1 <= in1(2 downto 0) & func(in1(3));

end arch;
