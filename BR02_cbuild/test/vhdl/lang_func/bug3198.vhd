library ieee;
use ieee.std_logic_1164.all;

entity bug3198 is
  port(short  : in std_logic_vector(3 downto 0);
       long   : in std_logic_vector(4 downto 0);
       out1   : out std_logic;
       out2   : out std_logic);
end;

architecture synth of bug3198 is
  signal result : std_logic;

  function func ( data: std_logic_vector ) return std_logic is
    variable retval: std_logic;
  begin
    retval := '0';
    for loopvar in data'range loop
      retval := retval xor data(loopvar);
    end loop;
    return retval;
  end;

begin
  outregs: process (short, long)
  begin
    result <= func(short);
    out1 <= result;
    result <= func(long);
    out2 <= result;
  end process outregs;
end synth;


