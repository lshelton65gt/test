entity var1 is
  port(in1: integer;
       output1 : out integer;
       output2 : out integer
      );
end var1;

architecture var1 of var1 is
  constant con : integer := 7;
  signal sig : integer := 0;
begin
  process(in1, sig)
    variable var : integer := 5;
  begin
    output1<= in1 + var;
    output2<= in1 + sig;
  end process;
end;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_bit.all;

entity initialvar1 is
  port ( in1 : std_logic_vector(0 to 31);
         out1 : out std_logic_vector(0 to 31);
         out2 : out std_logic_vector(0 to 31));
end;

architecture arch of initialvar1 is
 component var1
   port(in1 : in integer; output1, output2 : out integer);
 end component;
 signal t1 , t2, t3 : integer;
begin
  inst1 : var1
     port map(t1, t2, t3);

  process (in1, t2, t3)
  begin
     t1 <= CONV_INTEGER(in1);
     out1 <= CONV_STD_LOGIC_VECTOR(t2, 32);
     out2 <= CONV_STD_LOGIC_VECTOR(t3, 32);
  end process;
end;

