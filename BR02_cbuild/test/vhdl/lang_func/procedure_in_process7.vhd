-- this testcase was inspired by testing for a bug at skyworks

-- the interesting part of this test is that the procedure writes to signals
-- declared outside fo the procedure (hierrefs) so the procedure is flattened.
-- Also there is some sort of port size mismatch when the procedure is rewritten

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity procedure_in_process7 is
  port (
    in1, in2 : in integer range -2**15 to 2**15-1;
    outp : out signed(31 downto 0)
  );
end;

architecture arch of procedure_in_process7 is

  constant cPhaseErrorMax : integer := 2949;
  constant cPhaseErrorMin : integer := -2949;
  constant cAluAdd16 : integer := 1;
  constant cNumMul : integer := 4;
  constant cNumAlu16 : integer := 4;

  type tArrayVector32 is array(integer range <>) of signed(31 downto 0);
  type tArrayVector16 is array (integer range <>) of signed(15 downto 0);
  type tArrayInteger15 is array (integer range <>) of integer range 0 to 15;

  signal MulResult : tArrayVector32(cNumMul downto 1);
  signal AluPortA: tArrayVector16 (cNumMul downto 1);
  signal AluPortB: tArrayVector16 (cNumMul downto 1);
  signal AluOp:    tArrayInteger15 (cNumMul downto 1);


  
begin

  p1: process(in1, in2)

    procedure Alu16( constant AluNum : in integer range 1 to cNumAlu16;
                     constant PortA : in signed(15 downto 0);
                     constant PortB : in integer range -2**15 to 2**15-1;
                     constant Op : in integer range 0 to 15
                     ) is

  begin

    AluPortA(AluNum) <= PortA;
    AluPortA(1) <= PortA;

    AluPortB(AluNum) <= conv_signed(PortB, 16);

    AluOp(AluNum) <= Op;

  end procedure Alu16;

  begin
  Alu16( 1, MulResult(1)(15 downto 0), cPhaseErrorMax, cAluAdd16); -- eeqCore.vhd line 612

--  Alu16(2, MulResult(2)(15 downto 0), cPhaseErrorMin, cAluAdd16); -- eeqCore.vhd line 613


  end process p1;

  outp <= (AluPortA(1) & AluPortA(2));

end arch;


