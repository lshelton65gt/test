-- testcase test/vhdl/lang_func/bug3336.vhd, found during the analysis of 3336
-- in this test we once asserted on the release of a NUNetRef without a refcount of 0

library ieee;
use ieee.std_logic_1164.all;

entity bug3336 is
  port(short  : in std_logic_vector(3 downto 0);
       long   : in std_logic_vector(4 downto 0);
       out1   : out std_logic);
end;

architecture synth of bug3336 is
  signal result : std_logic;

  function func ( data: std_logic_vector )
    return boolean is
  begin
    if (data(0) = '0') then
      return FALSE;
    else
      return TRUE;
    end if;

  end;

begin
  outregs: process (short, long, result)
  begin

    if (func(short)) then
      result <= '1';
      out1 <= result;
    elsif (func(long)) then
      result <= '0';
      out1 <= result;
    end if;
  end process outregs;
end synth;


