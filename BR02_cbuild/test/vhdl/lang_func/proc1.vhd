entity proc1 is
  port (in1: in bit;  out1: out bit);
end;

architecture a_top of proc1 is

        procedure arc (signal a : bit; signal b: out bit) is 
        begin
                b <=  a;
        end;

begin
                arc(in1, out1);
end;

