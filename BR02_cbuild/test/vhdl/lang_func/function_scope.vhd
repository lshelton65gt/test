entity function_scope is
  port (in1, in2, in3: in bit;  out1, out2: out bit);
end;

architecture function_scope of function_scope is
        function anding (signal a1:in bit; 
                       signal a2:in bit;
                       signal a3: in bit) return bit  is 
        begin
                return a1 or a2 or a3;
        end;
begin
   process (in1, in2, in3)
        function anding (signal a1:in bit; 
                       signal a2:in bit;
                       signal a3: in bit) return bit is 
        begin
                return a1 and a2 and a3;
        end;
   begin
      out1 <= anding(in1, in2, in3); 
      out2 <= function_scope.anding(in1, in2, in3);
   end process;
  
end;

