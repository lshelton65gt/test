entity function_scope1 is
  port (in1, in2, in3: in bit;  out1, out2: out bit);
end;

architecture function_scope1 of function_scope1 is
        function anding (signal a1:in bit; 
                       signal a2:in bit;
                       signal a3: in bit) return bit  is 
        begin
                return a1 or a2 or a3;
        end;
begin
   process (in1, in2, in3)
        function anding (signal a1:in bit; 
                       signal a2:in bit;
                       signal a3: in bit) return bit is 
        begin
                return a1 and a2 and a3;
        end;
   begin
      out1 <= anding(in1, in2, in3); -- process scope 
   end process;

   process (in1, in2, in3)
   begin
      out2 <= anding(in1, in2, in3); -- architecture scope
   end process;

  
end;

