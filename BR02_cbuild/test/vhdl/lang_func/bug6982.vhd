-- Testcase that reproduces bug6982. The overloaded "+" and "-" operator
-- functions are called with variable bound, but constant width constrained
-- arguments which results in a Jaguar error. We workaround this by
-- compiling with -vhdlLoopUnroll argument.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package pkg is

    FUNCTION "+"        (L: STD_LOGIC_VECTOR;   R: STD_LOGIC)           RETURN STD_LOGIC_VECTOR;
    FUNCTION "-"        (L: STD_LOGIC_VECTOR;   R: STD_LOGIC)           RETURN STD_LOGIC_VECTOR;

end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package body pkg is

    FUNCTION "+"(L: STD_LOGIC_VECTOR;	R: STD_LOGIC)			RETURN STD_LOGIC_VECTOR IS
    BEGIN
	RETURN UNSIGNED (L) + (R);
    END;

    FUNCTION "-"(L: STD_LOGIC_VECTOR;	R: STD_LOGIC)			RETURN STD_LOGIC_VECTOR IS
    BEGIN
	RETURN UNSIGNED (L) - (R);
    END;

end pkg;

library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bug6982 is
  
  port (
    clk   : in  std_logic;
    reset : in  std_logic;
    in2   : in  std_logic_vector(3 downto 0);
    in3   : in  std_logic_vector(3 downto 0);
    in1   : in  std_logic_vector(15 downto 0);
    out1  : out std_logic_vector(15 downto 0));

end bug6982;

architecture arch of bug6982 is

begin  -- arch

  process (clk, reset)
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge
      if reset = '1' then                 -- asynchronous reset (active low)
        out1 <= (others => '0');
      else
        for i in 0 to 3 loop
          out1(i*4+3 downto i*4) <= in1(i*4+3 downto i*4) - in2(i) + in3(i);
        end loop;  -- i
      end if;
    end if;
  end process;
  
end arch;
