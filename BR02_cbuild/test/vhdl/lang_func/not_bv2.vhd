-- last mod: Wed Feb  1 17:20:06 2006
-- filename: test/vhdl/lang_func/not_bv2.vhd
-- Description:  This test is like not_bv1.vhd but uses "not" instead of not
-- currently C2005.12 SP1(4191), this is not supported, bug 4854


entity not_bv2 is
  port(in1 : bit_vector(0 to 7);
       output : out bit_vector(0 to 7));
end;

architecture not_bv2 of not_bv2 is
  signal temp : bit_vector(0 to 7);
begin
  output <= "not"(in1);
--  output <= not in1;
end;

