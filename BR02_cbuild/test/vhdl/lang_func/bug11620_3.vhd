-- Testcase 3 for codegen error caused by passing an integer as actual
-- to a procedure whose formal is a natural.  This testcase is based
-- on the actual customer RTL.

library ieee;
use ieee.std_logic_1164.all;

entity bug11620_3 is
  port (
    i : in  positive range 1 to 29;
    o : out integer range 0 to 28);
end bug11620_3;

architecture arch of bug11620_3 is

  procedure minus1 (
    variable i : positive;
    variable o : out natural) is
  begin
    o := i - 1;
  end;

begin

  process (i)
    variable temp_i : positive range 1 to 29;
    variable temp_o : integer range 0 to 28;
  begin

    temp_i := i;
    minus1(temp_i, temp_o);
    o <= temp_o;
  end process;
  
end arch;
