use std.standard.all ;

package func_decl_in_pack_decl1_pack is
  function sum
    ( signal in1 : bit ;
      signal in2 : bit
    ) return bit ;
end ;
package body func_decl_in_pack_decl1_pack is
function sum
  ( signal in1 : bit ;
    signal in2 : bit
  ) return bit is
begin
  return  in1 and in2;
end ;
end;
 
use work.func_decl_in_pack_decl1_pack.all;

entity func_decl_in_pack_decl is
  port ( in1 : bit ;
         in2 : bit ;
         output : out bit
       ) ;
end ;

architecture func_decl_in_pack_decl of func_decl_in_pack_decl is

begin
  output <= sum ( in1, in2 );
end; 
