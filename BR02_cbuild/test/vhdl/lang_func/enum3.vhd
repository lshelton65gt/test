-- Enum type access


package enum3_pack is
        attribute enum_encoding: string;
        type etype is (idle, active);
        attribute enum_encoding of etype : type is "00010 11000";
end enum3_pack;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.enum3_pack.all;

entity enum3 is
        port(
                clk: in std_logic;
                out2 : out etype;
                out1 : out etype
            );
end enum3;

architecture arch_enum3 of enum3 is
begin
  process (clk)
  begin
        if (clk = '1' and clk'event) then
            out1 <= idle;
            out2 <= active;
         end if;
  end process;
end arch_enum3;

