library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug3288 is
  port (Op               : in            std_logic_vector(12 downto 0);
        shiftsel   : out std_logic_vector(3 downto 0));
end;

architecture arch of bug3288 is
   function prienc (A: std_logic_vector) return INTEGER is
      variable INDEX: integer;
    begin
      for i in A'length-1 downto 0 loop
   if (A(i) = '1' ) then
     return(i+1);
  elsif (A(i) /= '0') then
      return(-(i+1));
  end if;
      end loop;
      return(0);
    end prienc;
    function DWF_prienc(A: std_logic_vector; INDEX_width: NATURAL)
        return std_logic_vector is
      -- pragma map_to_operator PRIENC_STD_LOGIC_OP
      -- pragma type_function PRIENC_STD_LOGIC_ARG
      -- pragma return_port_name Z
      constant A_width: NATURAL := A'length;
      variable AV: std_logic_vector(A_width-1 downto 0);
      variable Z: std_logic_vector(INDEX_width-1 downto 0);
      variable Y:  std_logic_vector(INDEX_width-1 downto 0);
      variable index_int: integer;
    begin
      AV := TO_UX01(std_logic_vector(A));
      index_int := prienc(A);
      if (index_int < 0 ) then
        return(Y);
      else
        return(std_logic_vector(CONV_UNSIGNED(index_int, INDEX_width)));
      end if;
    end;
begin
  priencode : process (Op)
  begin

    shiftSel <= DWF_prienc(Op, 4);

  end process priencode;

end;

