library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package pack_var_func_bit is
  function buildmask(nbits : integer) return std_logic_vector ;
end;

package body pack_var_func_bit is
  function buildmask(nbits : integer ) return std_logic_vector is
    variable var : std_logic_vector(nbits - 1 downto 0);
  begin
    var(var'length - 1 downto 0) := not(conv_std_logic_vector(0, nbits - 1 + 1));
    return var;
  end;
end;

use work.pack_var_func_bit.all;
library IEEE;
use IEEE.std_logic_1164.all;

entity  func_sizing is
  port(clk : in std_logic;
       in1 : in std_logic;
       out1 : out std_logic_vector(31 downto 0));
end;

architecture func_sizing of func_sizing is
begin
  process(clk)
  begin
    if (clk = '1' and clk'event) then
      out1 <= buildmask(32);
      out1(0) <= in1;
    end if;
  end process;
end;
