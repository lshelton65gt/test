entity func_with_multiple_return is
  port (in1,in2: in bit; out1: out bit);
end;

architecture a_top of func_with_multiple_return is

  function foo (a,b : bit) return bit is
  variable v : bit;
  begin
    v := a xor b;
    if (a = '1' ) then
      return a and b;
    elsif a = '0' and b = '0' then
      return '1';
    elsif a = b then
      return '0';
    end if;
    return v;
  end;

begin
  out1 <= foo(in1, in2);
end;

