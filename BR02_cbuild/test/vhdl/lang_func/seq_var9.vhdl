-- Copyright (c) 2002, 2006 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- TestPlan version 3.0
-- 7.3.9 : Variable assignment having qualified expression the rhs

-- This variation was added because testdriver is unlikely to
-- give us in1==in2 when they are both 32-bit integers.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity seq_var9 is
  port ( in1 : std_logic_vector(1 downto 0);
         in2 : std_logic_vector(1 downto 0);
         out1 : out STD_LOGIC;
         out2 : out boolean);
end seq_var9;

architecture seq_var9 of seq_var9 is
  
  function equality(a,b : std_logic_vector(1 downto 0)) return Boolean is
    variable var1 : Boolean;
  begin
    if (a = b) then
      var1 := TRUE;
    else var1 := FALSE;
    end if;
    return var1;
  end equality;
  
  function equality(a,b : std_logic_vector(1 downto 0)) return STD_LOGIC is
    variable var1 : STD_LOGIC;
  begin
    if (a=b) then var1 := '1';
    else var1 := '0';
    end if;
    return var1;
  end equality;
  
  function fn2(a : Boolean) return boolean is
    variable var1 : Boolean ;
  begin
    var1 := TRUE;
    if (a = FALSE) then
      var1 := FALSE;
    end if;
    return var1;
  end fn2;
  
  function fn2(a : STD_LOGIC) return STD_LOGIC is
    variable var1 : STD_LOGIC := '1';
  begin
    var1 := '1';
    if (a = '0') then var1 := '1';
    end if;
    return var1;
  end fn2;
  
  
begin -- start of the architecture
  
  process(in1,in2)
    variable sig1 : STD_LOGIC;
    variable  sig2 : boolean;
  begin
    sig1 := STD_LOGIC'(fn2(STD_LOGIC'(equality(in1,in2))));
    out1 <= sig1 xor '1';
    sig2 := boolean'(fn2(boolean'(equality(in1,in2))));
    out2<= sig2;
  end process;
  
end seq_var9 ;
