
LIBRARY ieee;
USE ieee.STD_LOGIC_1164.ALL;

ENTITY bug3799 IS
  PORT (
    msm_wr_data    : IN  STD_LOGIC_VECTOR(32-1 DOWNTO 0);
    msm_addr       : IN  STD_LOGIC_VECTOR(14-1 DOWNTO 0);
    core_hw_reset  : IN  STD_LOGIC;
    msm_ack_loc    : IN STD_LOGIC;
    core_req       : IN STD_LOGIC;
    core_rd_data_lat : IN STD_LOGIC_VECTOR(32-1 DOWNTO 0);
    msm_rd_data    : OUT STD_LOGIC_VECTOR(32-1 DOWNTO 0);
    core_addr      : OUT STD_LOGIC_VECTOR(14-1 DOWNTO 0);
    core_wr_data   : OUT STD_LOGIC_VECTOR(32-1 DOWNTO 0)
    );

END ;


ARCHITECTURE synth OF bug3799 IS

  FUNCTION "and"  (L: std_logic_vector; R: std_logic) return std_logic_vector is
      variable TEMP : std_logic_vector (L'range);
  BEGIN
      TEMP := (others => R);
      return (L and TEMP);
  END;

BEGIN
  msm_rd_data <= core_rd_data_lat AND msm_ack_loc AND NOT core_hw_reset;
  core_addr <= msm_addr(14-1 DOWNTO 0) AND core_req;
  core_wr_data <= msm_wr_data AND (core_req AND NOT core_hw_reset);
END synth;
