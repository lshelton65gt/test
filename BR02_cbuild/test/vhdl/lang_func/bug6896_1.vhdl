-- A test for bug6896, nets declared inside package body ..which are local to
-- the package, result in cbuild error. This was because the nets were not being
-- populated and the procedure scope was wrong. This has been fixed.

library ieee;
use ieee.std_logic_1164.all;

package pkg is

  procedure proc (
    idx     : in  integer;
    out_val : out integer);

end pkg;

package body pkg is

  type vec is array (15 downto 0) of integer;
  constant c1 : vec := (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, others => 0);

  procedure proc (
    idx     : in  integer;
    out_val : out integer) is
  begin  -- proc
    out_val := c1(idx);
  end proc;
  
end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkg.all;

entity bug6896_1 is
  
  port (
    in1  : in  std_logic_vector(15 downto 0);
    idx1 : in  std_logic_vector(3 downto 0);
    out1 : out std_logic);

end bug6896_1;

architecture arch of bug6896_1 is

begin  -- arch

  process (in1, idx1)
    variable var : integer := 0;
    variable out_var : integer := 0;
  begin  -- process
    var := to_integer(unsigned(idx1));
    proc(var, out_var);
    out1 <= in1(out_var);
  end process;

end arch;
