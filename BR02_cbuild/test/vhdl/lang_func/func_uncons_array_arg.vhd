entity func_uncons_array_arg is
  port (in1: in bit_vector(3 downto 0); 
        in2 : in bit_vector(4 downto 0); 
        in3 : in bit_vector(3 downto 0); 
        out1: out bit_vector(3 downto 0);
        out2: out bit_vector(5 downto 0);
        out3: out bit_vector(3 downto 0));
end;

architecture arch of func_uncons_array_arg is
  function foo (a : bit_vector) return bit_vector is
  begin
    return a;
  end;
  function foo (a : bit_vector) return bit is
  begin
    return a(a'left);
  end;
begin
  out1 <= foo(in1);  
  out2(4 downto 0) <= foo(in2);  
  out2(5) <= foo(in2); 
  out3 <= foo(in3);  
end;

