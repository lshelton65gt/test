-- This tests for correct scoping of a procedure declared in a process
library IEEE;
use IEEE.std_logic_1164.all;

entity bug5395 is
  port ( CLOCK, din: in STD_LOGIC;
         dout : out std_logic );
end;


architecture Arch_top of bug5395 is
  signal  OFFSET_REG: std_logic;
begin

  offset_reg <= din;
  
  exec_all_process  :  process
    -- this variable is placed in the exec_all_process scope
    variable dOFFSET : std_logic;

    -- this procedure is placed in the top scope
    procedure P_OFFSET is
    begin
      -- the variable dOFFSET cannot be found in the P_OFFSET scope or it's
      -- parent (top)
      dOFFSET := OFFSET_REG;
      dout <= doffset;
    end P_OFFSET;
  begin
    wait until CLOCK'event and CLOCK='1';
    P_OFFSET;
  end process exec_all_process;

end Arch_top;
