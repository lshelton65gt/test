-- testcase showing that we correctly inline a procedure
-- with memories as input parameters.
--
-- inlining is forced due to the non- blocking assigns.
package proc is
  type mem is array (0 to 3) of bit_vector(7 downto 0);
  procedure mine(input : bit_vector(7 downto 0);
                 idx   : integer;
                 signal io_x : in mem;
                 signal io_y : out mem);
end;

package body proc is
  procedure mine(input : bit_vector(7 downto 0);
                 idx   : integer;
                 signal io_x : in mem;
                 signal io_y : out mem) is
    variable l_mem : mem;
    begin
      l_mem(idx) := input and io_x(idx);
      io_y(idx) <= l_mem(idx);
    end;
end;

use work.proc.all;
  
entity bug4393_in is
  port (clk, rst, sel : in bit;
        i,j,k,l: in integer;
        outvec: out bit_vector(7 downto 0));
end bug4393_in;

architecture a of bug4393_in is
  signal a,b,y,z : mem;
  signal false_selector : bit;
begin
  process (clk, rst)
  begin
    if clk'event and clk = '1' then
      z <= y;
      mine(z(i),j,y,z);
    end if;
  end process;
  a <= y;
  b <= z;
  outvec <= a(0) and a(1) and b(0) and b(1);

end a;
