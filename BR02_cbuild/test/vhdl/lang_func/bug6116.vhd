-- last mod: Tue Jun 20 15:52:17 2006
-- filename: test/vhdl/lang_func/bug6116.vhd
-- Description:  This test is the original from bug6116, modified to duplicate
-- the internal error reported in that bug.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6116 is
port (
	AddrInt : in integer range 0 to 2;
	MemPtr : in std_logic_vector (17 downto 0);
	RdData : out std_logic_vector(15 downto 0)
);
end entity;

architecture rev1 of bug6116 is

  -- Read back register bits starting from RightIdx. Len is used to define the
  -- bits of the vector that are used by this capability class. This prevents
  -- accessing unused high-bits in lower capability classes.
  function RdBits(constant Reg : in std_logic_vector;
                  constant Len : integer;
                  constant RightIdx : in integer)
    return std_logic_vector is
  constant cZero : std_logic_vector(31 downto 0) := "00000000000000000000000000000000";
  begin
    if Len>RightIdx then
      return ext(Reg(Len-1 downto RightIdx), 16);
    else
      return cZero(15 downto 0);
    end if;
  end function RdBits;

begin
  with AddrInt select
    RdData <= RdBits(MemPtr, 16, 0) when 0,
              RdBits(MemPtr, 16, 16) when 1,
	      "1111111111111111" when 2;

end architecture;
