-- A test for bug6896, nets declared inside package body ..which are local to
-- the package, result in cbuild error. This was because the nets were not being
-- populated and the function scope was wrong. This has been fixed.

library ieee;
use ieee.std_logic_1164.all;

package pkg is

  function fun (idx : integer) return integer;

end pkg;

package body pkg is

  type vec is array (15 downto 0) of integer;
  constant c1 : vec := (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, others => 0);

  function fun (idx : integer) return integer is
    variable var : integer := 0;
  begin
    var := c1(idx);
    return var;
  end fun;
  
end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkg.all;

entity bug6896 is
  
  port (
    in1  : in  std_logic_vector(15 downto 0);
    idx1 : in  std_logic_vector(3 downto 0);
    out1 : out std_logic);

end bug6896;

architecture arch of bug6896 is

begin  -- arch

  process (in1, idx1)
    variable var : integer := 0;
  begin  -- process
    var := to_integer(unsigned(idx1));
    out1 <= in1(fun(var));
  end process;

end arch;
