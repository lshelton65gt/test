-- A simple test that reproduces bug6807. An function call is used
-- to initialize a constant. The constant is used to define the range
-- of a type declaration. To be able to populate the type correctly,
-- the function needs to be evaluated statically. Pure functions
-- are evaluated statically by Jaguar, while impure functions are
-- not ..since they may return different values for function calls
-- with same arguments.

-- In this testcase, the function is incorrectly called impure by
-- the user. Jaguar helps evaluate impure functions if they
-- have pure function properties. 

library ieee;
use ieee.std_logic_1164.all;

entity bug6807 is
  
  port (
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0));

end bug6807;

architecture rtl of bug6807 is

  impure function compute_left_bound return natural is
    variable left_bound : natural;
  begin
    left_bound := 4;
    return left_bound;
  end compute_left_bound;

  constant LEFT_BOUND : natural := compute_left_bound;

  signal sig : std_logic_vector(LEFT_BOUND-1 downto 0);
  
begin  -- rtl

  sig <= in1;
  out1 <= sig;
  
end rtl;
