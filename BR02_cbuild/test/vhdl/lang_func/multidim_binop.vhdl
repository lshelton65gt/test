library ieee;
use ieee.numeric_bit.all;

entity multidim_binop is
  port (
    din1, din2   : in  bit_vector(7 downto 0);
    din3, din4   : in  bit_vector(7 downto 0);
    dout1, dout2 : out bit_vector(7 downto 0));
end multidim_binop;

architecture arch of multidim_binop is
  type twodee is array (0 to 1) of bit_vector(7 downto 0);
  signal sig1, sig2, sig3 : twodee;

  function "+" ( L, R : twodee) return twodee is
    variable retval : twodee;
  begin
    retval(0) := bit_vector( unsigned(L(0)) + unsigned(R(0)));
    retval(1) := bit_vector( unsigned(L(1)) + unsigned(R(1)));
    return retval;
  end "+";
begin

  sig1 <= ( din1, din2 );
  sig2 <= ( din3, din4 );

  p1: process (sig1, sig2)
  begin
    sig3 <= sig1 + sig2;
  end process p1;

  dout1 <= sig3(0);
  dout2 <= sig3(1);
end arch;
