-- this testcase was inspired by testing for a bug at skyworks

-- the interesting part of this test is that the procedure writes to signals
-- declared outside fo the procedure (hierrefs) so the procedure is flattened.
-- Also there is some sort of port size mismatch when the procedure is rewritten

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity procedure_in_process6 is
  port (
    in1, in2 : in integer range -2**15 to 2**15-1;
    outp : out signed(15 downto 0)
  );
end;

architecture arch of procedure_in_process6 is
  constant cNumMul: integer := 4;
  type tArrayVector16 is array (integer range <>) of signed(15 downto 0);
  type tArrayInteger15 is array (integer range <>) of integer range 0 to 15;
  signal MulPortB: tArrayVector16 (cNumMul downto 1);
  signal MulOp:    tArrayInteger15 (cNumMul downto 1);

begin

  p1: process(in1, in2)
    procedure Mul(constant MulNum : in integer range 1 to cNumMul;
                  constant PortB : in integer range -2**15 to 2**15-1;
                  constant Op : in integer range 0 to 15) is
    begin
      MulPortB(MulNum) <= conv_signed(PortB, 16);
      MulOp(MulNum) <= Op;
    end procedure Mul;

  begin
    Mul(1, in2, 2);
  end process p1;
  
  outp <= MulPortB(1);

end arch;
