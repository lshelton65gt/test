use std.standard.all ;

package pack_proc_decl_in_pack_decl is
  procedure sum
    ( signal in1 : bit ;
      signal in2 : bit ;
      signal output : out bit 
    ) ;
end ;

package body pack_proc_decl_in_pack_decl is
procedure sum
  ( signal in1 : bit ;
    signal in2 : bit ;
    signal output : out bit 
  )  is
begin
  output <= in1 or in2;
end ;
end ;
use work.pack_proc_decl_in_pack_decl.all;
entity proc_decl_in_pack_decl is
  port (  in1 : bit ;
          in2 : bit ;
          output : out bit
       ) ;
end ;

architecture proc_decl_in_pack_decl of proc_decl_in_pack_decl is

begin
   sum ( in1, in2, output );
end; 
