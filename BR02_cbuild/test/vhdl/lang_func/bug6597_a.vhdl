-- Another version of bug6597.vhdl testcase where inputs to recursive
-- function is directly an input, and not a variable initialized to
-- a constant value.
library ieee;
use ieee.std_logic_1164.all;

entity bug6597_a is
  
  port (
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0));

end bug6597_a;

architecture arch of bug6597_a is

  function next_cnt(grayval : std_logic_vector) return std_logic_vector is
    variable tmp   : std_logic_vector(grayval'length-1 downto 0);
    variable tmp2  : std_logic_vector(1 downto 0);
    variable zeros : std_logic_vector(grayval'length-2 downto 0);
    variable onezeros : std_logic_vector(grayval'length-2 downto 0);
  begin
    tmp := grayval;
      
    if (tmp'length = 1) then
      return not(grayval);
    elsif (tmp'length = 2) then
      tmp2 := grayval(1 downto 0);
      case tmp2 is
        when "00"   => 
          return "01";
        when "01"   => 
          return "11";
        when "11"   => 
          return "10";
        when "10"   => 
          return "00";
        when others => 
          return "XX";
      end case;
    else
      zeros    := (others => '0');
      onezeros := (others => '0');
      onezeros(onezeros'left) := '1';
      if (tmp(tmp'left) = '0') then
        if (tmp(tmp'left-1 downto 0) = onezeros) then
          tmp(tmp'left) := '1';
          return tmp;
        else
          tmp(tmp'left-1 downto 0) := next_cnt(tmp(tmp'left-1 downto 0));
          return tmp;
        end if ;
      else
        if (tmp(tmp'left-1 downto 0) = zeros) then
          tmp(tmp'left) := '0';
          return tmp;
        else
          tmp(tmp'left-1 downto 0) := next_cnt(tmp(tmp'left-1 downto 0));
          return tmp;
        end if ;
      end if ;
    end if ;
  end next_cnt ;

begin  -- arch

  out1 <= next_cnt(in1);
  
end arch;
