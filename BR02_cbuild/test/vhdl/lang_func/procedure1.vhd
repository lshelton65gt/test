entity procedure1 is
  port (in1,in2,in3: in bit;  out1: out bit);
end;

architecture a_top of procedure1 is

        procedure arc (signal a1, a2,a3: in bit; signal b: out bit) is 
        begin
                b <=  a1 and a2 and a3;
        end;

begin
                arc(in1, in2, in3, out1);
end;

