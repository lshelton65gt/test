-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- 4.2  of test plan version 1.0
-- This is to test nested procedure

use std.standard.all;

entity proc_call_in_proc1 is
  port ( in1 : bit  ;
         in2 : bit ;
         in3 : bit ;
         output : out bit
       );
end ; 

architecture proc_call_in_proc1 of proc_call_in_proc1 is
 procedure sum 
    ( signal in1 : in bit ;
      signal in2 : in bit ;
      signal in3 : in bit ;
      signal output : out bit
    ) is
 begin
   output <= in1 and in2 and in3;
end ;

procedure sum1 
  ( signal in1 : bit ;
    signal in2 : bit ; 
    signal in3 : bit ; 
    signal add : out bit 
  ) is
begin
  sum ( in1, in2, in3, add);
end ;

begin
 process ( in1, in2, in3)
 begin
    sum1(in1,in2,in3, output);
 end process;
end ;
  
