-- Test for recursive function derived from matrox/phoenix
-- testcase.
library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity bug6597 is
  generic (DP : integer := 8);
  port (
    rclkbuf    :  in      std_logic;
    rclkrstN   :  in      std_logic;
    rhd        :  in      std_logic;
    empty      :  buffer  std_logic;
    out1       :  out std_logic_vector(3 downto 0)
    );
end bug6597;

architecture arch of bug6597 is


      function next_cnt(grayval : std_logic_vector)
      return std_logic_vector;
      function prev_cnt(grayval : std_logic_vector)
      return std_logic_vector;

      function prev_cnt(grayval : std_logic_vector)
      return std_logic_vector is
         variable tmp   : std_logic_vector(grayval'length-1 downto 0);
         variable tmp2  : std_logic_vector(1 downto 0);
         variable zeros : std_logic_vector(grayval'length-2 downto 0);
         variable onezeros : std_logic_vector(grayval'length-2 downto 0);
      begin
         tmp := grayval;
      
         if (tmp'length = 1) then
               return not(grayval);
         elsif (tmp'length = 2) then
            tmp2 := grayval(1 downto 0);
            case tmp2 is
               when "00"   => 
                  return "10";
               when "01"   => 
                  return "00";
               when "11"   => 
                  return "01";
               when "10"   => 
                  return "11";
               when others => 
                  return "XX";
            end case;
         else
            zeros    := (others => '0');
            onezeros := (others => '0');
            onezeros(onezeros'left) := '1';
            if (tmp(tmp'left) = '0') then
               if (tmp(tmp'left-1 downto 0) = zeros) then
                  tmp(tmp'left) := '1';
                  return tmp;
               else
                  tmp(tmp'left-1 downto 0) := prev_cnt(tmp(tmp'left-1 downto 0));
                  return tmp;
               end if ;
            else
               if (tmp(tmp'left-1 downto 0) = onezeros) then
                  tmp(tmp'left) := '0';
                  return tmp;
               else
                  tmp(tmp'left-1 downto 0) := next_cnt(tmp(tmp'left-1 downto 0));
                  return tmp;
               end if ;
            end if ;
         end if ;
      end prev_cnt ;

      function next_cnt(grayval : std_logic_vector)
      return std_logic_vector is
         variable tmp   : std_logic_vector(grayval'length-1 downto 0);
         variable tmp2  : std_logic_vector(1 downto 0);
         variable zeros : std_logic_vector(grayval'length-2 downto 0);
         variable onezeros : std_logic_vector(grayval'length-2 downto 0);
      begin
         tmp := grayval;
      
         if (tmp'length = 1) then
               return not(grayval);
         elsif (tmp'length = 2) then
            tmp2 := grayval(1 downto 0);
            case tmp2 is
               when "00"   => 
                  return "01";
               when "01"   => 
                  return "11";
               when "11"   => 
                  return "10";
               when "10"   => 
                  return "00";
               when others => 
                  return "XX";
            end case;
         else
            zeros    := (others => '0');
            onezeros := (others => '0');
            onezeros(onezeros'left) := '1';
            if (tmp(tmp'left) = '0') then
               if (tmp(tmp'left-1 downto 0) = onezeros) then
                  tmp(tmp'left) := '1';
                  return tmp;
               else
                  tmp(tmp'left-1 downto 0) := next_cnt(tmp(tmp'left-1 downto 0));
                  return tmp;
               end if ;
            else
               if (tmp(tmp'left-1 downto 0) = zeros) then
                  tmp(tmp'left) := '0';
                  return tmp;
               else
                  tmp(tmp'left-1 downto 0) := prev_cnt(tmp(tmp'left-1 downto 0));
                  return tmp;
               end if ;
            end if ;
         end if ;
      end next_cnt ;


      function BOOL_TO_INTEGER (
        in1 : boolean)
        return integer is
      begin  -- BOOL_TO_INTEGER
        if in1 then
          return 1;
        end if;
        return 0;
      end BOOL_TO_INTEGER;
      
      constant AW            : integer := 3; -- log2(DP);
 constant WRAP_VALUE    : std_logic_vector(AW-1 downto 0) := conv_std_logic_vector(DP-1,AW);
 constant POWER_OF_TWO  : boolean := ((2**AW) = DP);
 constant PW            : integer := AW + BOOL_TO_INTEGER(POWER_OF_TWO);
 signal rptr     : std_logic_vector(PW-1 downto 0);
 signal nrptr    : std_logic_vector(PW-1 downto 0);
 signal nrunld   : std_logic;
 signal nempty   : std_logic;
  
begin  -- arch

  nrunld <= not (rhd or empty);
  nempty <= '1' WHEN (rhd = empty) else '0';
  nrptr  <= rptr WHEN nrunld='0' else next_cnt(rptr);
  out1 <= nrptr;
  
  reg_rptr : process (rclkbuf, rclkrstN)
  begin 
  if(rclkrstN = '0')then
    rptr  <= (others => '0');
    empty <= '1';
  elsif (rclkbuf'event and rclkbuf = '1') then
    rptr  <= nrptr;
    empty <= nempty;
  end if;
end process reg_rptr;


end arch;
