entity func_uncons_array_arg_size is
  port (in1,in2: in bit_vector(0 to 179);
        out1: out bit_vector(0 to 179));
end;

architecture arch of func_uncons_array_arg_size is
  function foo (a,b : bit_vector) return bit_vector is
  begin
    return a and b;
  end;
begin
  out1(0 to 7) <= foo(in1(0 to 7),in2(0 to 7));
  out1(8 to 23) <= foo(in1(8 to 23),in2(8 to 23));
  out1(24 to 55) <= foo(in1(24 to 55),in2(24 to 55));
  out1(56 to 119) <= foo(in1(56 to 119),in2(56 to 119));
  out1(120 to 179) <= foo(in1(120 to 179),in2(120 to 179));
end;

