-- Testcase 2 for codegen error caused by passing an integer as actual
-- to a procedure whose formal is a positive

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity bug11620_2 is
  port (
    i : in  positive;
    o : out STD_LOGIC_VECTOR(31 downto 0));
end bug11620_2;

architecture arch of bug11620_2 is

  procedure plus1 (
    variable i : positive;
    variable o : out positive) is
  begin
    o := i + 1;
  end;

begin

  process (i)
    variable temp_i : positive;
    variable temp_o : integer;
  begin

    temp_i := i;
    plus1(temp_i, temp_o);
    o <= STD_LOGIC_VECTOR(TO_UNSIGNED(temp_o,32));
  end process;
  
end arch;
