library ieee;
use ieee.std_logic_1164.all;

entity bug6806 is
  
  port (
    in1  : in  std_logic;
    in2  : in  std_logic;
    out1 : out std_logic);

end bug6806;

architecture arch of bug6806 is

  function func (
    in1 : std_logic;
    in2 : std_logic := '1')
    return std_logic is
  begin  -- func
    return in1 and in2;
  end func;

  procedure proc (
    in1  : in  std_logic;
    out1 : out std_logic;
    in2  : in  std_logic := '1') is
  begin  -- proc
    out1 := in1 and in2;
  end proc;

begin  -- arch

  process (in1, in2)
    variable var : std_logic := '0';
  begin  -- process
    proc(in1, var);
    out1 <= func(in2) and var;
  end process;

end arch;
