-- testcase test/vhdl/lang_func/func_in_while.vhd
-- in this test a function call is used as the condition in a while loop construct.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity func_in_while is
  port(short  : in std_logic_vector(3 downto 0);
       out1   : out std_logic_vector(0 to 31));
end;

architecture synth of func_in_while is

  function func ( data: std_logic_vector(3 downto 0); i: integer )
    return boolean is
  begin
    if (data = "0000") then
      return false;
    end if;
    if (data(i) = '0') then
      return FALSE;
    else
      return TRUE;
    end if;

  end;

  
begin
  regProc: process (short)
    variable position: integer;
  begin
    position := 0;
    while (func(short, position )) loop
--      out1<= conv_std_logic_vector(position, 32);  -- out'length is better (once it is supported)
      out1<= conv_std_logic_vector(position, out1'length);
      if ( position >= 3 ) then
        exit;
      else
        position := position + 1;
      end if;
    end loop;
  end process regProc;
  
end synth;


