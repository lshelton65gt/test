entity procedure_in_process5 is
  port (in1, in2, in3: in bit;  out1: out bit);
end;

architecture a_top of procedure_in_process5 is
begin
   process (in1, in2, in3)
        procedure arc (signal a1:in bit; signal a2:in bit;signal  a3: in bit; 
                      variable b: out bit) is 
        begin
                b :=  a1 and a2 and a3;
        end;
       variable temp:bit;
   begin
      arc(in1, in2, in3, temp);
      out1 <= temp;
   end process;

end;

