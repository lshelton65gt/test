-- Test that signal parameters are pass-by-reference

library IEEE;
use IEEE.STD_LOGIC_1164.all;
--use STD.TEXTIO.all;
--use IEEE.STD_LOGIC_TEXTIO.all;

entity call_by_ref1 is
  port(clk: in std_logic;
       ena: in std_logic;
       in1: in std_logic;
       out1: inout std_logic := '0';
       err1: out std_logic);
end;

architecture call_by_ref1 of call_by_ref1 is
  procedure proc_a(signal in1 : in std_logic;
                   signal out1: inout std_logic;
                   signal err1 : out std_logic) is
  begin  -- proc_a
    out1 <= not in1;

    -- if out1 and in1 are in fact passed by reference, they will be
    -- aliased to the same object, and after complementing, 'out1' and 'in1'
    -- will have the same value.
    
    err1 <= (out1 and in1) or ((not out1) and (not in1)) ;
  end proc_a;

begin  -- call_by_ref1
  process(clk)
  begin
    if clk'event and clk = '1' then
      out1 <= in1;
      proc_a(out1, out1, err1);
    end if;
  end process;
end call_by_ref1;
