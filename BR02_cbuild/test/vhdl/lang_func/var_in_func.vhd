package pack_var_func_bit is
          function toggle(input : bit) return bit ;
end;

package body pack_var_func_bit is
         function toggle(input : bit ) return bit is
          variable var :bit :='0' ;
         begin
         var := '0';
         if input = '1' then 
            return var;
         else 
            return  '1';
         end if;
    end;
end;

use work.pack_var_func_bit.all;
entity  var_in_func is
                port(input : in bit;
                     output : out bit);
end;

architecture var_in_func of var_in_func is
begin
                process(input)
                begin
                   output<=toggle(input);
                end process;
end;

