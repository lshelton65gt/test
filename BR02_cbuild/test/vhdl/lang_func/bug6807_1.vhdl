-- A negative test for checking that impure functions that are correctly
-- indicated by user as impure aren't evaluated.

library ieee;
use ieee.std_logic_1164.all;

entity bug6807_1 is
  
  port (
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0));

end bug6807_1;

architecture rtl of bug6807_1 is

  shared variable shar_var : natural := 1; -- by setting this to 1, the default
                                           -- value for shar_var will be such
                                           -- that the calculation of LEFT_BOUND below will be 5
                                           -- (which is incompatible with the width of in1 above). 
                                           -- Thus this test will fail if compute_left_bound is
                                           -- actually used to set the width of 'sig'

  -- This function is really impure since it uses a
  -- variable outside it's scope.
  impure function compute_left_bound return natural is
    variable left_bound : natural;
  begin
    left_bound := shar_var + 4;
    return left_bound;
  end compute_left_bound;

  -- This constant is not being initialized with a pure
  -- function that can be statically evaluated.
  constant LEFT_BOUND : natural := compute_left_bound;

  signal sig : std_logic_vector(LEFT_BOUND-1 downto 0);

begin  -- rtl

process (in1, sig)
begin  -- process
  sig <= in1;
    if sig /= "0" then                  -- here is a bit of logic to make sure that shar_var is always a shared variable
      shar_var := shar_var + 1;
    end if;
  out1 <= sig;
end process;

end rtl;
