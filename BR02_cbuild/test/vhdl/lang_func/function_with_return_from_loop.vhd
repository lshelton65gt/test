library ieee;
use ieee.numeric_bit.all;

entity function_with_return_from_loop is
  port(in1,in2: in unsigned(0 to 3); out1: out unsigned(0 to 7));
end;

architecture arch of function_with_return_from_loop is
  function foo(in1,in2 : unsigned) return unsigned is
  variable v : integer;
  variable j : integer;
  variable temp : unsigned(7 downto 0);
  begin
    temp := (others => '0');
    v := 0;
    temp(3 downto 0) := in1 + in2;
    l1: for i in 0 to 3 loop
      j := 0;
      l2: while j <= 3 loop
        if (i = 3) then
          exit;
        else
          v := v + j;
        end if;
        j := j + 1;
        if (j = 3 ) then
          return temp;
        end if;
        temp := temp + 1;               -- the offending line
      end loop l2;
      v := v + i;
      if (v = 15) then
        return temp;
      end if;
    end loop l1;
  end;

begin
  process (in1,in2)
  begin
  out1 <= foo(in1,in2);
  end process;
end;
