--function "+"(L: SIGNED; R: UNSIGNED) return SIGNED
library ieee;
use ieee.std_logic_arith.all;

entity add17 is
  port (
    in1 : in signed(65 downto 0);
    in2 : in unsigned(6 downto 0);
    out1: out signed(65 downto 0));
end;

architecture arch of add17 is

begin
  process(in1, in2)
  begin
    out1 <= in1 + in2;
  end process;

end arch;
