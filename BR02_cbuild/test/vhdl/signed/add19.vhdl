--    function "+"(L: UNSIGNED; R: INTEGER) return UNSIGNED 
library ieee;
use ieee.std_logic_arith.all;

entity add19 is
  port (
    in1 : in unsigned(33 downto 0);
    in2 : in integer;
    out1: out unsigned(33 downto 0));
end;

architecture arch of add19 is

begin
  process(in1, in2)
  begin
    out1 <= in1 + in2;
  end process;

end arch;
