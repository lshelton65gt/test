--    function "+"(L: UNSIGNED; R: INTEGER) return STD_LOGIC_VECTOR
--    function "+"(L: INTEGER; R: UNSIGNED) return STD_LOGIC_VECTOR
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity add33 is
  port (
    in1 : in unsigned(1 downto 0);
    in2 : in integer;
    match : out std_logic;
    out1: out std_logic_vector(1 downto 0));
end;

architecture arch of add33 is
  signal sum1, sum2 : std_logic_vector(out1'range);
begin
  process(in1, in2)
  begin
    sum1 <= in1 + in2;
    sum2 <= in2 + in1;
  end process;

  match <= '1' when sum1 = sum2 else '0';
  out1 <= sum1;
end arch;
