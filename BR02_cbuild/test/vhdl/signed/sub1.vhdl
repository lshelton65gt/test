-- test builtin operator - for full integers
entity sub1 is
  port (
    in1, in2 : in integer;
    out1: out integer);
end sub1;

architecture arch of sub1 is

begin
  process(in1, in2)
  begin
    out1 <= in1 - in2;
  end process;

end arch;
