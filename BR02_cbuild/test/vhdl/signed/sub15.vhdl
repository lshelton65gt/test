--    function "-"(L: UNSIGNED; R: SIGNED) return SIGNED
library ieee;
use ieee.std_logic_arith.all;

entity sub15 is
  port (
    in1 : in unsigned(12 downto 0);
    in2 : in signed(67 downto 0);
    out1: out signed(67 downto 0));
end;

architecture arch of sub15 is

begin
  process(in1, in2)
  begin
    out1 <= in1 - in2;
  end process;

end arch;
