library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity signed_subrange is
  port(a : in integer;
       b : out integer range 0 to 255);
end signed_subrange;

architecture fun of signed_subrange is

begin  -- fun

  b <= 133;                             -- don't do m_b = -122

end fun;
