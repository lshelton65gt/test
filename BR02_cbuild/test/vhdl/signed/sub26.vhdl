--    function "-"(L: UNSIGNED; R: STD_ULOGIC) return UNSIGNED
--    function "-"(L: STD_ULOGIC; R: UNSIGNED) return UNSIGNED
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity sub26 is
  port (
    in1 : in unsigned(21 downto 0);
    in2 : in std_ulogic;
    match : out std_logic;
    out1: out unsigned(21 downto 0));
end;

architecture arch of sub26 is
  signal buf, reverseargs : unsigned(out1'range);
begin
  process(in1, in2)
  begin
    buf <= in1 - in2;
    reverseargs <= in2 - in1;
  end process;

  match <= '1' when buf = reverseargs else '0';
  out1 <= buf;
end arch;
