-- test builtin operator - for a signed subrange of integers
package pack is
  subtype myint is integer range -742 to 742;
end pack;

use work.pack.all;

entity sub2 is
  port (
    in1, in2 : in integer;
    out1: out integer);
end sub2;

architecture arch of sub2 is

begin
  process(in1, in2)
    variable s1, s2 : myint;
  begin
    s1 := in1 rem 743;
    s2 := in2 rem 743;
    out1 <= s1 - s2;
  end process;

end arch;
