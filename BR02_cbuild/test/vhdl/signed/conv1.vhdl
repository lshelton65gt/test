library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity conv1 is
  port (
    a : in  std_logic_vector(32 downto 0);
    c : out std_logic_vector(65 downto 0)
    );                    
end;


architecture functional of conv1 is
  
begin
  c <= sxt(a, 66);
end functional;
