-- filename: test/vhdl/signed/bug12130.vhd
-- Description:  This test duplicates the problem seen in bug 12131 where
-- a simulation mismatch was due to signed comparisons not being done

-- this is not a vhdl specific problem, see a simplified testcase with the same
-- problem in test/langcov/Signed/bug12130.v


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug12130 is
  
  port (
    curnt1_metric, curnt0_metric : in  std_logic_vector(159 downto 0);
    level1_7sel                  : out std_logic_vector(16 downto 1);
    level1_metric                : out std_logic_vector(159 downto 0)
    );

  constant ACS : integer := 16;
  constant SM_BITS : integer := 10;

end bug12130;

architecture rtl of bug12130 is

begin  -- rtl

    gsixteen: if (ACS=16) generate
    -- Compare 32 to generate 16
    cmp32x16: process (curnt1_metric, curnt0_metric)
    begin
      if (signed(curnt1_metric(SM_BITS-1 downto 0)) > signed(curnt0_metric(SM_BITS-1 downto 0))) then
        level1_7sel(1) <= '1';
        level1_metric((SM_BITS)-1 downto 0) <= curnt1_metric(SM_BITS-1 downto 0);
      else
        level1_7sel(1) <= '0';
        level1_metric((SM_BITS)-1 downto 0) <= curnt0_metric(SM_BITS-1 downto 0);
      end if;

      if (signed(curnt1_metric((2*SM_BITS)-1 downto SM_BITS)) > signed(curnt0_metric((2*SM_BITS)-1 downto SM_BITS))) then
        level1_7sel(2) <= '1';
        level1_metric((2*SM_BITS)-1 downto SM_BITS) <= curnt1_metric((2*SM_BITS)-1 downto SM_BITS);
      else
        level1_7sel(2) <= '0';
        level1_metric((2*SM_BITS)-1 downto SM_BITS) <= curnt0_metric ((2*SM_BITS)-1 downto SM_BITS);
      end if;

      if (signed(curnt1_metric((3*SM_BITS)-1 downto 2*SM_BITS)) > signed(curnt0_metric((3*SM_BITS)-1 downto 2*SM_BITS))) then
        level1_7sel(3) <= '1';
        level1_metric((3*SM_BITS)-1 downto 2*SM_BITS) <= curnt1_metric((3*SM_BITS)-1 downto 2*SM_BITS);
      else
        level1_7sel(3) <= '0';
        level1_metric((3*SM_BITS)-1 downto 2*SM_BITS) <= curnt0_metric ((3*SM_BITS)-1 downto 2*SM_BITS);
      end if;

      if (signed(curnt1_metric((4*SM_BITS)-1 downto 3*SM_BITS)) > signed(curnt0_metric((4*SM_BITS)-1 downto 3*SM_BITS))) then
        level1_7sel(4) <= '1';
        level1_metric((4*SM_BITS)-1 downto 3*SM_BITS) <= curnt1_metric((4*SM_BITS)-1 downto 3*SM_BITS);
      else
        level1_7sel(4) <= '0';
        level1_metric((4*SM_BITS)-1 downto 3*SM_BITS) <= curnt0_metric ((4*SM_BITS)-1 downto 3*SM_BITS);
      end if;

      if (signed(curnt1_metric((5*SM_BITS)-1 downto 4*SM_BITS)) > signed(curnt0_metric((5*SM_BITS)-1 downto 4*SM_BITS))) then
        level1_7sel(5) <= '1';
        level1_metric((5*SM_BITS)-1 downto 4*SM_BITS) <= curnt1_metric((5*SM_BITS)-1 downto 4*SM_BITS);
      else
        level1_7sel(5) <= '0';
        level1_metric((5*SM_BITS)-1 downto 4*SM_BITS) <= curnt0_metric ((5*SM_BITS)-1 downto 4*SM_BITS);
      end if;

      if (signed(curnt1_metric((6*SM_BITS)-1 downto 5*SM_BITS)) > signed(curnt0_metric((6*SM_BITS)-1 downto 5*SM_BITS))) then
        level1_7sel(6) <= '1';
        level1_metric((6*SM_BITS)-1 downto 5*SM_BITS) <= curnt1_metric((6*SM_BITS)-1 downto 5*SM_BITS);
      else
        level1_7sel(6) <= '0';
        level1_metric((6*SM_BITS)-1 downto 5*SM_BITS) <= curnt0_metric ((6*SM_BITS)-1 downto 5*SM_BITS);
      end if;

      if (signed(curnt1_metric((7*SM_BITS)-1 downto 6*SM_BITS)) > signed(curnt0_metric((7*SM_BITS)-1 downto 6*SM_BITS))) then
        level1_7sel(7) <= '1';
        level1_metric((7*SM_BITS)-1 downto 6*SM_BITS) <= curnt1_metric((7*SM_BITS)-1 downto 6*SM_BITS);
      else
        level1_7sel(7) <= '0';
        level1_metric((7*SM_BITS)-1 downto 6*SM_BITS) <= curnt0_metric ((7*SM_BITS)-1 downto 6*SM_BITS);
      end if;

      if (signed(curnt1_metric((8*SM_BITS)-1 downto 7*SM_BITS)) > signed(curnt0_metric((8*SM_BITS)-1 downto 7*SM_BITS))) then
        level1_7sel(8) <= '1';
        level1_metric((8*SM_BITS)-1 downto 7*SM_BITS) <= curnt1_metric((8*SM_BITS)-1 downto 7*SM_BITS);
      else
        level1_7sel(8) <= '0';
        level1_metric((8*SM_BITS)-1 downto 7*SM_BITS) <= curnt0_metric ((8*SM_BITS)-1 downto 7*SM_BITS);
      end if;

      if (signed(curnt1_metric((9*SM_BITS)-1 downto 8*SM_BITS)) > signed(curnt0_metric((9*SM_BITS)-1 downto 8*SM_BITS))) then
        level1_7sel(9) <= '1';
        level1_metric((9*SM_BITS)-1 downto 8*SM_BITS) <= curnt1_metric((9*SM_BITS)-1 downto 8*SM_BITS);
      else
        level1_7sel(9) <= '0';
        level1_metric((9*SM_BITS)-1 downto 8*SM_BITS) <= curnt0_metric ((9*SM_BITS)-1 downto 8*SM_BITS);
      end if;

      if (signed(curnt1_metric((10*SM_BITS)-1 downto 9*SM_BITS)) > signed(curnt0_metric((10*SM_BITS)-1 downto 9*SM_BITS))) then
        level1_7sel(10) <= '1';
        level1_metric((10*SM_BITS)-1 downto 9*SM_BITS) <= curnt1_metric((10*SM_BITS)-1 downto 9*SM_BITS);
      else
        level1_7sel(10) <= '0';
        level1_metric((10*SM_BITS)-1 downto 9*SM_BITS) <= curnt0_metric ((10*SM_BITS)-1 downto 9*SM_BITS);
      end if;

      if (signed(curnt1_metric((11*SM_BITS)-1 downto 10*SM_BITS)) > signed(curnt0_metric((11*SM_BITS)-1 downto 10*SM_BITS))) then
        level1_7sel(11) <= '1';
        level1_metric((11*SM_BITS)-1 downto 10*SM_BITS) <= curnt1_metric((11*SM_BITS)-1 downto 10*SM_BITS);
      else
        level1_7sel(11) <= '0';
        level1_metric((11*SM_BITS)-1 downto 10*SM_BITS) <= curnt0_metric ((11*SM_BITS)-1 downto 10*SM_BITS);
      end if;

      if (signed(curnt1_metric((12*SM_BITS)-1 downto 11*SM_BITS)) > signed(curnt0_metric((12*SM_BITS)-1 downto 11*SM_BITS))) then
        level1_7sel(12) <= '1';
        level1_metric((12*SM_BITS)-1 downto 11*SM_BITS) <= curnt1_metric((12*SM_BITS)-1 downto 11*SM_BITS);
      else
        level1_7sel(12) <= '0';
        level1_metric((12*SM_BITS)-1 downto 11*SM_BITS) <= curnt0_metric ((12*SM_BITS)-1 downto 11*SM_BITS);
      end if;

      if (signed(curnt1_metric((13*SM_BITS)-1 downto 12*SM_BITS)) > signed(curnt0_metric((13*SM_BITS)-1 downto 12*SM_BITS))) then
        level1_7sel(13) <= '1';
        level1_metric((13*SM_BITS)-1 downto 12*SM_BITS) <= curnt1_metric((13*SM_BITS)-1 downto 12*SM_BITS);
      else
        level1_7sel(13) <= '0';
        level1_metric((13*SM_BITS)-1 downto 12*SM_BITS) <= curnt0_metric ((13*SM_BITS)-1 downto 12*SM_BITS);
      end if;

      if (signed(curnt1_metric((14*SM_BITS)-1 downto 13*SM_BITS)) > signed(curnt0_metric((14*SM_BITS)-1 downto 13*SM_BITS))) then
        level1_7sel(14) <= '1';
        level1_metric((14*SM_BITS)-1 downto 13*SM_BITS) <= curnt1_metric((14*SM_BITS)-1 downto 13*SM_BITS);
      else
        level1_7sel(14) <= '0';
        level1_metric((14*SM_BITS)-1 downto 13*SM_BITS) <= curnt0_metric ((14*SM_BITS)-1 downto 13*SM_BITS);
      end if;

      if (signed(curnt1_metric((15*SM_BITS)-1 downto 14*SM_BITS)) > signed(curnt0_metric((15*SM_BITS)-1 downto 14*SM_BITS))) then
        level1_7sel(15) <= '1';
        level1_metric((15*SM_BITS)-1 downto 14*SM_BITS) <= curnt1_metric((15*SM_BITS)-1 downto 14*SM_BITS);
      else
        level1_7sel(15) <= '0';
        level1_metric((15*SM_BITS)-1 downto 14*SM_BITS) <= curnt0_metric ((15*SM_BITS)-1 downto 14*SM_BITS);
      end if;

      if (signed(curnt1_metric((16*SM_BITS)-1 downto 15*SM_BITS)) > signed(curnt0_metric((16*SM_BITS)-1 downto 15*SM_BITS))) then
        level1_7sel(16) <= '1';
        level1_metric((16*SM_BITS)-1 downto 15*SM_BITS) <= curnt1_metric((16*SM_BITS)-1 downto 15*SM_BITS);
      else
        level1_7sel(16) <= '0';
        level1_metric((16*SM_BITS)-1 downto 15*SM_BITS) <= curnt0_metric ((16*SM_BITS)-1 downto 15*SM_BITS);
      end if;
    end process cmp32x16;
  end generate;


end rtl;

