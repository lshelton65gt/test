-- filename: test/vhdl/signed/mult.vhdl
-- This is modelled after test/langcov/Signed/binary.v. VHDL will require
-- multiple copies of this file, because there are multiple different packages
-- defining multiplication and we need to test them all.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mult7 is
  port (
    in1 : in std_logic_vector(31 downto 0);
    in2 : in std_logic_vector(31 downto 0);
    out_a : out integer;
    out_b : out integer;
    out_c : out integer
    );
end mult7;

architecture arch of mult7 is
  subtype sint8 is integer range -(2**7)+1 to 2**7-1;
  signal temp1, temp2 : sint8;
  signal int1, int2 : integer;
begin
  int1 <= to_integer(signed(in1));
  int2 <= to_integer(signed(in2));
  out_a <= int1 * int2;

  temp1 <= int1 rem 2**7;
  temp2 <= int2 rem 2**7;
  out_b <= temp1 * temp2;

  out_c <= to_integer(signed(in1(24 downto 16))) * to_integer(signed(in2(24 downto 16)));
end arch;
