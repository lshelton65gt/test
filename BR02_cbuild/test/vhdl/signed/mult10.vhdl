library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity mult10 is
  port (
    v_operanda        : in std_logic_vector(32 downto 0);
    v_operandb        : in std_logic_vector(32 downto 0);
    v_localresult     : out std_logic_vector(65 downto 0)
    );                    
end mult10;


architecture functional of mult10 is
  
begin
  v_localresult <= unsigned(v_operanda) * unsigned(v_operandb);
end functional;
