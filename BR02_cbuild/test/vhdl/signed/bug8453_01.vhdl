-- filename: test/vhdl/signed/bug8453_01.vhdl
-- Description:  This test duplicates the simulation mismatch seen in bug 8453,
-- the issue is that in the backend phase we lose the comparison with -1 for
-- some reason

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_FftCo is
  type     t_dataArray is array(natural range <>) OF std_logic_vector(17 downto 0);
--  type     t_TransIdxArray is array(natural range <>) OF natural range 0 to 31;
end pkg_FftCo;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkg_FftCo.all;

entity bug8453_01 is
  port (
    clock    : in  std_logic;
    in1, in2, in3, in4 : in  std_logic_vector(17 downto 0);
    ctl : in std_logic_vector(2 downto 0);
    out1     : out integer);
end;

architecture arch of bug8453_01 is

begin
  main: process (clock)
    variable temp  : natural range 0 to 4;
   variable  regs : t_dataArray(0 to 3);
    
  begin
    if clock'event and clock = '1' then 
      temp        := 0;
      regs(0) := in1;
      regs(1) := in2;
      regs(2) := in3;
      regs(3) := in4;
      case ctl is
        when "001"  =>
          for i in 0 to 3 loop
            if (signed(regs((i))(17 downto 16)) /= - 1) -- has a simulation mismatch
--            if (to_integer(signed(regs((i))(17 downto 16))) /= - 1)  -- works ok
            then
              temp:= temp+ 1;
            end if;
          end loop;
        when others =>
          temp    := 0;
      end case;
      out1 <= temp;
    end if;
  end process;
end;
