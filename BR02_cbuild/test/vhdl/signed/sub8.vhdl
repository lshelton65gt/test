--A.6 function "-" (L: NATURAL; R: UNSIGNED) return UNSIGNED
library ieee;
use ieee.numeric_std.all;

entity sub8 is
  port (
    in1 : in integer;
    in2 : in unsigned(66 downto 0);
    out1: out unsigned(66 downto 0));
end sub8;

architecture arch of sub8 is

begin
  process(in1, in2)
    variable nat : natural range 0 to 314159;
  begin
    nat := in1 mod 314160;
    out1 <= nat - in2;
  end process;

end arch;
