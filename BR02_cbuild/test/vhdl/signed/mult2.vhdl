-- filename: test/vhdl/signed/mult.vhdl
-- This is modelled after test/langcov/Signed/binary.v. VHDL will require
-- multiple copies of this file, because there are multiple different packages
-- defining multiplication and we need to test them all.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mult2 is
  port (
    in1 : in std_logic_vector(2 - 1 downto 0);
    in2 : in std_logic_vector(3 - 1 downto 0);
    in3 : in std_logic_vector(4 - 1 downto 0);
    Sout_a: out signed(3 + 3 - 1 downto 0)
    );
end mult2;

architecture arch of mult2 is
  signal s2 : signed(3 - 1 downto 0);
  signal u3 : unsigned(4 - 1 downto 0);

begin
  p1: process (in1, in2, in3, u3, s2)
  begin
    S2 <= signed(in2);
    U3 <= unsigned(in3);
    
    Sout_a <= S2 * to_integer(U3); -- signed * int
  end process p1;
end arch;
