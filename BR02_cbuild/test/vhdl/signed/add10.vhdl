--A.8 function "+" (L: INTEGER; R: SIGNED) return SIGNED
library ieee;
use ieee.numeric_std.all;

entity add10 is
  port (
    in1 : in signed(30 downto 0);
    in2 : in integer;
    out1: out signed(30 downto 0));
end add10;

architecture arch of add10 is

begin
  process(in1, in2)
  begin
    out1 <= in2 + in1;
  end process;

end arch;
