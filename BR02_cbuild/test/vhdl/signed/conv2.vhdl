library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity conv2 is
  port (
    a : in  std_logic_vector(32 downto 0);
    c : out std_logic_vector(65 downto 0)
    );                    
end;


architecture functional of conv2 is
  
begin
  c <= ext(a, 66);
end functional;
