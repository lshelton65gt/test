-- test builtin operator + for full integers
entity add1 is
  port (
    in1, in2 : in integer;
    out1: out integer);
end add1;

architecture arch of add1 is

begin
  process(in1, in2)
  begin
    out1 <= in1 + in2;
  end process;

end arch;
