--    function "-"(L: UNSIGNED; R: STD_ULOGIC) return STD_LOGIC_VECTOR
--    function "-"(L: STD_ULOGIC; R: UNSIGNED) return STD_LOGIC_VECTOR
--    function "-"(L: SIGNED; R: STD_ULOGIC) return STD_LOGIC_VECTOR
--    function "-"(L: STD_ULOGIC; R: SIGNED) return STD_LOGIC_VECTOR
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity sub37 is
  port (
    in1 : in unsigned(21 downto 0);
    in2 : in std_ulogic;
    match : out std_logic;
    out1: out std_logic_vector(21 downto 0));
end;

architecture arch of sub37 is
  signal sum1, sum2, sum3, sum4 : std_logic_vector(out1'range);
begin
  process(in1, in2)
  begin
    sum1 <= in1 - in2;
    sum2 <= in2 - in1;
    sum3 <= signed(in1) - in2;
    sum4 <= in2 - signed(in1);
  end process;

  match <= '1' when sum1 = sum2 and sum2 = sum3 and sum3 = sum4 else '0';
  out1 <= sum1;
end arch;
