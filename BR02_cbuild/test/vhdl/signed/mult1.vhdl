-- filename: test/vhdl/signed/mult.vhdl
-- This is modelled after test/langcov/Signed/binary.v. VHDL will require
-- multiple copies of this file, because there are multiple different packages
-- defining multiplication and we need to test them all.  This test tests all
-- the various forms of multiplication defined in package numeric_std.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mult1 is
  generic (
    size1 : integer := 2;
    size2 : integer := 3;
    size3 : integer := 4);
  port (
    in1 : in std_logic_vector(size1 - 1 downto 0);
    in2 : in std_logic_vector(size2 - 1 downto 0);
    in3 : in std_logic_vector(size3 - 1 downto 0);
    Sout_a: out signed(size2 + size2 - 1 downto 0);
    Sout_a2: out unsigned(size3 + size3 -1 downto 0);
    Sout_b: out signed(size2 + size3 - 1 downto 0);
    Sout_c, Sout_d, Sout_e : out signed(size1 + size2 + size3 - 1 downto 0);
    Sout_f: out signed(size1 + size2 * 2 - 1 downto 0);
    Sout_g, Sout_h : out signed((size1 + size2) * 2 - 1 downto 0);
    Sout_i : out signed(size1 + size3 * 2 - 1 downto 0);
    Sout_j, Sout_k : out signed(size1 * 2 + size3 - 1 downto 0);
    Sout_l : out signed(size1 * 2 - 1 downto 0);
    Sout_m, Sout_n : out signed(size1 * 4 - 1 downto 0);
    Sout_o : out signed((size2 + size3) * 2 - 1 downto 0);
    Sout_p, Sout_q : out signed(size2 * 2 + size3 - 1 downto 0);
    Sout_r, Sout_s, Sout_t : out signed(size2 * 4 - 1 downto 0);
    Sout_u : out signed(size3 * 4 - 1 downto 0);
    Sout_v, Sout_w : out signed(size3 * 2 - 1 downto 0);
    Uout_x, Uout_y, Uout_z : out unsigned(size1 + size2 + size3 - 1 downto 0)
    );
end mult1;

architecture arch of mult1 is
  signal s1 : signed(SIZE1 - 1 downto 0);
  signal u1 : unsigned(SIZE1 - 1 downto 0);
  signal s2 : signed(SIZE2 - 1 downto 0);
  signal u2 : unsigned(SIZE2 - 1 downto 0);
  signal s3 : signed(SIZE3 - 1 downto 0);
  signal u3 : unsigned(SIZE3 - 1 downto 0);

begin
  p1: process (in1, in2, in3, u1, u2, u3, s1, s2, s3)
  begin
    S1 <= signed(in1);
    U1 <= unsigned(in1);
    S2 <= signed(in2);
    U2 <= unsigned(in2);
    S3 <= signed(in3);
    U3 <= unsigned(in3);
    
    Sout_a <= S2 * to_integer(U3); -- signed * int
    Sout_a2 <= to_integer(S2) * U3; -- int * unsigned
    Sout_b <= S2 * S3; -- signed * signed

    Sout_c <= (S1 * S2) * S3; -- (signed * signed) * signed
    Sout_d <= S1 * (S2 * S3);-- signed * (signed * signed)
    Sout_e <= S1 * S2 * S3; -- signed * signed * signed

    Sout_f <= S1 * (S2 * to_integer(U3));-- signed * (signed * unsigned)
    Sout_g <= (S1 * S2) * to_integer(U3); -- (signed * signed) * unsigned
    Sout_h <= S1 * S2 * to_integer(U3); -- signed * signed * unsigned

    Sout_i <= S1 * (to_integer(U2) * S3);-- signed * (unsigned * signed)
    Sout_j <= (S1 * to_integer(U2)) * S3; -- (signed * unsigned) * signed
    Sout_k <= S1 * to_integer(U2) * S3; -- signed * unsigned * signed

    Sout_l <= S1 * to_integer(U2 * U3);-- signed * (unsigned * unsigned)
    Sout_m <= (S1 * to_integer(U2)) * to_integer(U3); -- (signed * unsigned) * unsigned
    Sout_n <= S1 * to_integer(U2) * to_integer(U3); -- signed * unsigned * unsigned

    Sout_o <= to_integer(U1) * (S2 * S3);-- unsigned * (signed * signed)
    Sout_p <= (to_integer(U1) * S2) * S3; -- (unsigned * signed) * signed
    Sout_q <= to_integer(U1) * S2 * S3; -- unsigned * signed * signed

    Sout_r <= to_integer(U1) * (S2 * to_integer(U3));-- unsigned * (signed * unsigned)
    Sout_s <= (to_integer(U1) * S2) * to_integer(U3); -- (unsigned * signed) * unsigned
    Sout_t <= to_integer(U1) * S2 * to_integer(U3); -- unsigned * signed * unsigned
    
    Sout_u <= to_integer(U1) * (to_integer(U2) * S3);-- unsigned * (unsigned * signed)
    Sout_v <= to_integer(U1 * U2) * S3; -- (unsigned * unsigned) * signed
    Sout_w <= to_integer(U1) * to_integer(U2) * S3; -- unsigned * unsigned * signed

    Uout_x <= U1 * (U2 * U3);-- unsigned * (unsigned * unsigned)
    Uout_y <= (U1 * U2) * U3; -- (unsigned * unsigned) * unsigned
    Uout_z <= U1 * U2 * U3; -- unsigned * unsigned * unsigned

  end process p1;
end arch;
