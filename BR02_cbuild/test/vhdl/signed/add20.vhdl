--    function "+"(L: UNSIGNED; R: INTEGER) return UNSIGNED 
library ieee;
use ieee.std_logic_arith.all;

entity add20 is
  port (
    in1 : in unsigned(4 downto 0);
    in2 : in integer;
    out1: out unsigned(4 downto 0));
end;

architecture arch of add20 is

begin
  process(in1, in2)
  begin
    out1 <= in1 + in2;
  end process;

end arch;
