-- test builtin operator - for an unsigned subrange of integers.  This involves
-- integer->natural conversion.
package pack is
  subtype myint is natural range 0 to 742;
end pack;

use work.pack.all;

entity sub3 is
  port (
    in1, in2 : in integer;
    out1: out integer);
end sub3;

architecture arch of sub3 is

begin
  process(in1, in2)
    variable s1, s2 : myint;
  begin
--    s1 := abs(in1) rem 743;
--    s2 := abs(in2) rem 743;
    s1 := in1 mod 743;
    s2 := in2 mod 743;
    out1 <= s1 - s2;
  end process;

end arch;
