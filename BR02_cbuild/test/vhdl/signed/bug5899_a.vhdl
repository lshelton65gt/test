-- This test came from ST.  The problem was that we were always
-- considering the ABS overloaded operator to return the same type and
-- signedness as its result. This is not the case for one version of
-- IEEE.STD_LOGIC_ARITH."ABS".  This test checks that we get the correct
-- behavior with IEEE.STD_LOGIC_SIGNED."ABS".

library IEEE ;
use IEEE.std_logic_1164.all;
--use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;

entity bug5899_a is
  port (
    i : in  std_logic_vector (7 downto 0);
    o : out std_logic_vector (15 downto 0));
end;

architecture arch of bug5899_a is
  signal j : std_logic_vector(7 downto 0) := "10000000";
begin
  o <= ieee.std_logic_arith.ext(abs(j), 16);
end arch;
