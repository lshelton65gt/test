-- filename: test/vhdl/signed/mult.vhdl
-- This is modelled after test/langcov/Signed/binary.v. VHDL will require
-- multiple copies of this file, because there are multiple different packages
-- defining multiplication and we need to test them all.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mult6 is
  generic (
    size1 : integer := 2;
    size2 : integer := 3;
    size3 : integer := 4);
  port (
    in1 : in std_logic_vector(1 downto 0);
    in2 : in std_logic_vector(2 downto 0);
    in3 : in std_logic_vector(3 downto 0);
    Sout_w : out signed(8 - 1 downto 0)
    );
end mult6;

architecture arch of mult6 is
  signal u1 : unsigned(1 downto 0);
  signal u2 : unsigned(2 downto 0);
  signal s3 : signed(3 downto 0);

begin
  p1: process (in1, in2, in3, u1, u2, s3)
  begin
    U1 <= unsigned(in1);
    U2 <= unsigned(in2);
    S3 <= signed(in3);
    
    Sout_w <= to_integer(U1) * to_integer(U2) * S3; -- unsigned * unsigned * signed
  end process p1;
end arch;
