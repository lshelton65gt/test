--function "-"(L: UNSIGNED; R: UNSIGNED) return STD_LOGIC_VECTOR
--function "-"(L: SIGNED; R: SIGNED) return STD_LOGIC_VECTOR
--function "-"(L: UNSIGNED; R: SIGNED) return STD_LOGIC_VECTOR
--function "-"(L: SIGNED; R: UNSIGNED) return STD_LOGIC_VECTOR
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity sub31 is
  port (
    in1 : in unsigned(4 downto 0);
    in2 : in unsigned(70 downto 0);
    match : out std_logic;
    out1: out std_logic_vector(70 downto 0));
end;

architecture arch of sub31 is
  signal sum1, sum2, sum3, sum4 : std_logic_vector(out1'range);
begin
  process(in1, in2)
    variable s1 : signed(in1'range);
    variable s2 : signed(in2'range);
  begin
    sum1 <= in1 - in2;
    s1 := signed(in1);
    s2 := signed(in2);
    sum2 <= s1 - s2;
    sum3 <= s1 - signed(s2);
    sum4 <= signed(s1) - s2;
  end process;

  match <= '1' when sum1 = sum2 and sum2 = sum3 and sum3 = sum4 else '0';
  out1 <= sum1;
end arch;
