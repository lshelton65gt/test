-- filename: test/vhdl/signed/mult.vhdl
-- This is modelled after test/langcov/Signed/binary.v. VHDL will require
-- multiple copies of this file, because there are multiple different packages
-- defining multiplication and we need to test them all.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mult5 is
  generic (
    size1 : integer := 2;
    size2 : integer := 3;
    size3 : integer := 4);
  port (
    in1 : in std_logic_vector(1 downto 0);
    in2 : in std_logic_vector(2 downto 0);
    in3 : in std_logic_vector(3 downto 0);
    Sout_g, Sout_h : out signed(9 downto 0)
    );
end mult5;

architecture arch of mult5 is
  signal s1 : signed(1 downto 0);
  signal s2 : signed(2 downto 0);
  signal u3 : unsigned(3 downto 0);

begin
  p1: process (in1, in2, in3, u3, s1, s2)
  begin
    S1 <= signed(in1);
    S2 <= signed(in2);
    U3 <= unsigned(in3);
    
    Sout_g <= (S1 * S2) * to_integer(U3); -- (signed * signed) * unsigned
    Sout_h <= S1 * S2 * to_integer(U3); -- signed * signed * unsigned
  end process p1;
end arch;
