--function "-"(L: SIGNED; R: UNSIGNED) return SIGNED
library ieee;
use ieee.std_logic_arith.all;

entity sub16 is
  port (
    in1 : in signed(12 downto 0);
    in2 : in unsigned(13 downto 0);
    out1: out signed(14 downto 0));
end;

architecture arch of sub16 is

begin
  process(in1, in2)
  begin
    out1 <= in1 - in2;
  end process;

end arch;
