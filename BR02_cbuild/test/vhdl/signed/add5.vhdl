--A.3  function "+" (L, R: UNSIGNED) return UNSIGNED;
library ieee;
use ieee.numeric_std.all;

entity add5 is
  port (
    in1 : in unsigned(72 downto 0);
    in2 : in unsigned(2 downto 0);
    out1: out unsigned(72 downto 0));
end add5;

architecture arch of add5 is

begin
  process(in1, in2)
  begin
    out1 <= in1 + in2;
  end process;

end arch;
