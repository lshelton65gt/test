--A.7 function "-" (L: SIGNED; R: INTEGER) return SIGNED
--This generates incorrect results-probably in extending the natural to 75 bits?
--The simulation is OK with lengths <= 64
library ieee;
use ieee.numeric_std.all;

entity sub9 is
  port (
    in1 : in signed(64 downto 0);
    in2 : in integer;
    out1: out signed(64 downto 0));
end sub9;

architecture arch of sub9 is

begin
  process(in1, in2)
  begin
    out1 <= in1 - in2;
  end process;

end arch;
