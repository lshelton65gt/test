--A.4  function "+" (L, R: SIGNED) return SIGNED;
library ieee;
use ieee.numeric_std.all;

entity add6 is
  port (
    in1 : in signed(13 downto 0);
    in2 : in signed(72 downto 0);
    out1: out signed(72 downto 0));
end add6;

architecture arch of add6 is

begin
  process(in1, in2)
  begin
    out1 <= in1 + in2;
  end process;

end arch;
