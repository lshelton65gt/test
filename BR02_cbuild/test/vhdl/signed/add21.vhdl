--    function "+"(L: INTEGER; R: UNSIGNED) return UNSIGNED
library ieee;
use ieee.std_logic_arith.all;

entity add21 is
  port (
    in1 : in integer;
    in2 : in unsigned(64 downto 0);
    out1: out unsigned(64 downto 0));
end;

architecture arch of add21 is

begin
  process(in1, in2)
  begin
    out1 <= in1 + in2;
  end process;

end arch;
