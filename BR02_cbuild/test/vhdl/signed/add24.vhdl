--    function "+"(L: SIGNED; R: INTEGER) return SIGNED
--    function "+"(L: INTEGER; R: SIGNED) return SIGNED
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity add24 is
  port (
    in1 : in signed(63 downto 0);
    in2 : in integer;
    match : out std_logic;
    out1: out signed(63 downto 0));
end;

architecture arch of add24 is
  signal buf, reverseargs : signed(out1'range);
begin
  process(in1, in2)
  begin
    buf <= in1 + in2;
    reverseargs <= in2 + in1;
  end process;

  match <= '1' when buf = reverseargs else '0';
  out1 <= buf;
end arch;
