-- filename: test/vhdl/signed/mult.vhdl
-- This is modelled after test/langcov/Signed/binary.v. VHDL will require
-- multiple copies of this file, because there are multiple different packages
-- defining multiplication and we need to test them all.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mult4 is
  port (
    in1 : in std_logic_vector(1 downto 0);
    in2 : in std_logic_vector(2 downto 0);
    in3 : in std_logic_vector(3 downto 0);
    uout_a2: out unsigned(7 downto 0)
    );
end mult4;

architecture arch of mult4 is
  signal s2 : signed(2 downto 0);
  signal u3 : unsigned(3 downto 0);

begin
  p1: process (in1, in2, in3, u3, s2)
  begin
    S2 <= signed(in2);
    U3 <= unsigned(in3);
    
    uout_a2 <= to_integer(S2) * U3; -- int * unsigned
  end process p1;
end arch;
