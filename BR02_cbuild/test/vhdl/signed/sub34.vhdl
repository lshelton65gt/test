--    function "-"(L: SIGNED; R: INTEGER) return STD_LOGIC_VECTOR
--    function "-"(L: INTEGER; R: SIGNED) return STD_LOGIC_VECTOR
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity sub34 is
  port (
    in1 : in signed(65 downto 0);
    in2 : in integer;
    match : out std_logic;
    out1: out std_logic_vector(65 downto 0));
end;

architecture arch of sub34 is
  signal sum1, sum2 : std_logic_vector(out1'range);
begin
  process(in1, in2)
  begin
    sum1 <= in1 - in2;
    sum2 <= in2 - in1;
  end process;

  match <= '1' when sum1 = sum2 else '0';
  out1 <= sum1;
end arch;
