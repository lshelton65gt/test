--A.4  function "+" (L, R: SIGNED) return SIGNED;
-- This segvs in codegen, and the problem goes away if signed is changed to unsigned.
library ieee;
use ieee.numeric_std.all;

entity add4 is
  port (
    in1, in2 : in signed(65 downto 0);
    out1: out signed(65 downto 0));
end add4;

architecture arch of add4 is

begin
  process(in1, in2)
  begin
    out1 <= in1 + in2;
  end process;

end arch;
