--function "+"(L: UNSIGNED; R: UNSIGNED) return UNSIGNED
library ieee;
use ieee.std_logic_arith.all;

entity add11 is
  port (
    in1 : in unsigned(95 downto 0);
    in2 : in unsigned(9 downto 0);
    out1: out unsigned(95 downto 0));
end add11;

architecture arch of add11 is

begin
  process(in1, in2)
  begin
    out1 <= in1 + in2;
  end process;

end arch;
