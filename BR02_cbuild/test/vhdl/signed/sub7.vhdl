--A.5 function "-" (L: UNSIGNED; R: NATURAL) return UNSIGNED
library ieee;
use ieee.numeric_std.all;

entity sub7 is
  port (
    in1 : in unsigned(13 downto 0);
    in2 : in natural;
    out1: out unsigned(13 downto 0));
end sub7;

architecture arch of sub7 is

begin
  process(in1, in2)
  begin
    out1 <= in1 - in2;
  end process;

end arch;
