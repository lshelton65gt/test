--A.7 function "+" (L: SIGNED; R: INTEGER) return SIGNED
--This generates incorrect results-probably in extending the natural to 75 bits?
--The simulation is OK with lengths <= 64
--The order of operands doesn't matter (same results for in1+in2 and in2+in1)
library ieee;
use ieee.numeric_std.all;

entity add9 is
  port (
    in1 : in signed(64 downto 0);
    in2 : in integer;
    out1: out signed(64 downto 0));
end add9;

architecture arch of add9 is

begin
  process(in1, in2)
  begin
    out1 <= in1 + in2;
  end process;

end arch;
