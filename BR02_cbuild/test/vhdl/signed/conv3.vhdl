library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity conv3 is
  port (
    a : in  signed(32 downto 0);
    c : out std_logic_vector(65 downto 0)
    );                    
end;


architecture functional of conv3 is
  
begin
  c <= sxt(sxt(std_logic_vector(a), 33), 66);
end functional;
