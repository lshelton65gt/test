-- filename: test/vhdl/signed/mult.vhdl
-- This is modelled after test/langcov/Signed/binary.v. VHDL will require
-- multiple copies of this file, because there are multiple different packages
-- defining multiplication and we need to test them all.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mult3 is
  port (
    in2 : in std_logic_vector(3 downto 0);
    in3 : in std_logic_vector(4 downto 0);
    Sout_a: out signed(7 downto 0);
    Sout_b: out signed(7 downto 0);
    uout_c: out unsigned(7 downto 0);
    uout_d: out unsigned(7 downto 0)
    );
end mult3;

architecture arch of mult3 is
  signal s2 : signed(3 downto 0);
  signal u2 : unsigned(3 downto 0);
  signal u3 : unsigned(4 downto 0);
  signal s3 : signed(4 downto 0);

begin
  p1: process (in2, in3, u3, s2, s3, u2)
  begin
    S2 <= signed(in2);
    U2 <= unsigned(in2);
    S3 <= signed(in3);
    U3 <= unsigned(in3);
    
    Sout_a <= S2 * to_integer(U3);
    Sout_b <= S2 * to_integer(s3);
    Uout_c <= U2 * to_integer(u3);
    Uout_d <= U2 * to_integer(s3);
  end process p1;

end arch;
