-- Heavily extracted from fujitsu/device/carbon/cwtb/rtl069.  This test ensures
-- we get (i = var1 + 1) right.  It populates to ( var1 = i - 1).  After loop
-- unrolling, we get ( var1 = -1 ) for loop index 0.  This is out of range,
-- and GCC will detect this and warn on it if not folded away.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity bug5900_1 is
  port(
    port3   : in    std_logic_vector(5 downto 0);
    outport : out   std_logic_vector(1 downto 0)
    );
end bug5900_1;

architecture rtl of bug5900_1 is
begin

  DeQuantization : process(port3)
    variable var1 : integer range 0 to 17;
  begin
    var1 := conv_integer(unsigned(port3)) mod 18;
    for i in 0 to 1 loop
      if (i <= (var1 - 4)) then
      elsif (i = (var1 + 1)) then
        outport(i) <= port3(4);         --we only get here when var1 == 0 and i == 1, thus outport(0) is never writtern
      end if;
    end loop;
  end process DeQuantization;
end rtl ;
