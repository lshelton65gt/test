--A.6 function "+" (L: NATURAL; R: UNSIGNED) return UNSIGNED
--The order of operands doesn't matter (same results for in1+in2 and in2+in1)
library ieee;
use ieee.numeric_std.all;

entity add8 is
  port (
    in1 : in integer;
    in2 : in unsigned(66 downto 0);
    out1: out unsigned(66 downto 0));
end add8;

architecture arch of add8 is
begin
  process(in1, in2)
    variable nat : natural range 0 to 314159;
  begin
    nat := in1 mod 314160;
    out1 <= nat + in2;
  end process;

end arch;
