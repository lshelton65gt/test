library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity mult9 is
  port (
    v_operanda        : in std_logic_vector(32 downto 0);
    v_operandb        : in std_logic_vector(32 downto 0);
    v_localresult     : out std_logic_vector(65 downto 0)
    );                    
end mult9;


architecture functional of mult9 is
  
begin
  v_localresult <= signed(v_operanda) * signed(v_operandb);
end functional;
