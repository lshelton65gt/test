--    function "-"(L: INTEGER; R: UNSIGNED) return UNSIGNED
library ieee;
use ieee.std_logic_arith.all;

entity sub23 is
  port (
    in1 : in integer;
    in2 : in unsigned(63 downto 0);
    out1: out unsigned(63 downto 0));
end;

architecture arch of sub23 is

begin
  process(in1, in2)
  begin
    out1 <= in1 - in2;
  end process;

end arch;
