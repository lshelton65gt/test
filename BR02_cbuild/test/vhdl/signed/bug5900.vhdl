library IEEE ;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;

entity sub is

  port (
    i0 : in  integer range -128 to 127;
    i1 : in  integer range -128 to 127;
    i2 : in  integer range 0 to 1;
    i3 : in  std_logic;
    o : out std_logic_vector (16 downto 0));

end sub;

architecture arch of sub is

  signal x : std_logic_vector (16 downto 0);

begin

  x <= conv_std_logic_vector(i0 + i1 + i2, 17);
  o <= x when i3 = '0' else x(16) & x(16 downto 1);

end arch;


library IEEE ;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
      
entity bug5900 is
 port(
  mode : in std_logic;
  o : out std_logic_vector(16 downto 0));
end;

architecture arch of bug5900 is

component sub
  port (
    i0       : in  integer range -128 to 127;
    i1       : in  integer range -128 to 127;
    i2       : in  integer range 0 to 1;
    i3       : in  std_logic;
    o : out std_logic_vector(16 downto 0));

end component;

  signal i0       : integer range -128 to 127;
  signal i1       : integer range -128 to 127;
  signal i2       : integer range 0 to 1;
  signal i3 : std_logic;

begin

  process (mode)
  begin
    if mode = '0' then
      i0 <= 0;
      i1 <= 0;
      i2 <= 0;
      i3 <= '0';
    else
      i0 <= 0;
      i1 <= 0;
      i2 <= 1;
      i3 <= '1';
    end if;
  end process;

  i_sub : sub
    port map (
      i0 => i0,
      i1 => i1,
      i2 => i2,
      i3 => i3,
      o  => o);

end arch;
