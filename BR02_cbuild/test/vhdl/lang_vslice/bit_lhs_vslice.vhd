-- test for part select a bit vector with variable width slice on LHS

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;


entity bit_lhs_vslice is 
  port (
    clk       : in  std_logic;
    val       : in  integer ;
    format    : in  integer ;
    stringout : out bit_vector(10 downto 1)
  ) ;
end bit_lhs_vslice;

architecture TEST of bit_lhs_vslice is

begin

  process( clk, val, format )
    
    variable buf            : bit_vector( 10 downto 1) ;
    variable str_index      : integer := 0 ;
    variable ival           : integer ;
  
  begin

    if (clk'event and clk = '1') then
      buf(10 downto 1) := "1010101010";
      str_index := format mod 10;
      ival := val mod 10;
      if (str_index > ival) then
        stringout (str_index downto ival+1)<= buf(str_index downto ival+1);
      else
        stringout (ival downto str_index+1)<= buf(ival downto str_index+1);
      end if;
    end if;
  
  end process ;

end TEST ;

