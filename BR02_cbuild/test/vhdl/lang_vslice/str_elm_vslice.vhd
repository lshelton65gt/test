-- LHS & RHS variable indexed slice in a memory element
package  p is
  type str is array (4 downto 1) of bit_vector(7 downto 0); 
end  p;
library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use work.p.all;

entity str_elm_vslice is 
  port (
    format   : in  integer ;
    vec_out  : out bit_vector(7 downto 0)
  ) ;
end str_elm_vslice;

architecture TEST of str_elm_vslice is

begin
  process( format )
  
    variable buf            : str;
    variable vec : str;
    variable ival : integer;
    variable idx : integer;
  begin

    for i  in 4 downto 1 loop
      buf( i ) := "10101010";      
    end loop;
    ival := format mod 8;
    idx := (format mod 4)+1;
    if (ival > 1) then
      vec(idx)(ival downto ival-1) := buf(idx)(ival downto ival-1);
    else
      vec(idx)(ival+1 downto ival) := buf(idx)(ival+1 downto ival);
    end if;

    vec_out <= vec(idx);
  end process ;

end TEST ;
