-- memory to memory assignment (string)
library IEEE;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity str1 is 
  port (
    format    : in  integer ;
    sout      : out std_logic_vector(10 downto 1)
  ) ;
end str1;

architecture TEST of str1 is

begin

  process( format )

    variable vec : string (5 downto 1);
    variable buf : string (10 downto 1);
    variable ival : integer;
  begin

    -- load buf with the string "aaaaaaaaaa"
    for i  in 10 downto 1 loop
      buf( i ) := 'a';      
    end loop;

    -- load vec with the string "xxxxx"
    for i  in 5 downto 1 loop
      vec( i ) := 'x';      
    end loop;

    ival := (format mod 10)+1;
    if (ival > 2) then
      buf(ival-1) := 'b';
    else      
      buf(ival) := 'c';
    end if;

    ival := (ival mod 5) +1;
    vec := buf( ival downto 1 ) ;    -- replace vec with ival chars from buf,
                                     -- ival must be 5 or this is a runtime error

    for i  in 10 downto 1 loop
      if (vec(i) = buf(i)) then
        sout(i) <= '1';
      else
        sout(i) <= '0';
      end if;
    end loop;
      
  end process ;

end TEST ;

