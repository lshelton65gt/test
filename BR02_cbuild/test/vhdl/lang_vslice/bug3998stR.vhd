-- last mod: Wed Feb 14 2007
-- filename: test/vhdl/lang_vslice/bug3998stR.vhd
-- Description:  1) This test includes a slice of a slice of a std_logic_vector (temp1). 
--		 2) This test includes a slice of a slice of a slice of a std_logic_vector (temp1). 
--               As of Feb 2007 this is not supported.

LIBRARY IEEE ;
USE IEEE.std_logic_1164.ALL ;
USE IEEE.std_logic_unsigned.ALL ;

entity bug3998stR is
        port( clk : in std_logic;
              j : in integer;
              k : in integer;
              in1 : in std_logic_vector(7 downto 0);
              out1 : out std_logic_vector(7 downto 0)
            );
end bug3998stR;

architecture arch_logic1 of bug3998stR is
signal temp1 : std_logic_vector(31 downto 0);  
signal temp2 : std_logic_vector(7 downto 0);
begin
        out1(7 downto 0) <= temp2(7 downto 0);
        temp1(7 downto 0) <= in1(7 downto 0);
        
        P1:process(in1, j, k, temp1)
        begin

	    temp2(7 downto 0) <= temp1(10 downto 0)(10 downto 3);            
	    temp2(7 downto 0) <= temp1(11 downto 1)(10 downto 3);
	    temp2(7 downto 0) <= temp1(12 downto 2)(10 downto 3);
	    temp2(7 downto 0) <= temp1(13 downto 3)(10 downto 3);
	    temp2(7 downto 0) <= temp1(10 downto 0)(10 downto 0)(10 downto 3);

        end process;

end arch_logic1;
