-- both R/L HS, variable width slice of a whole memory, 
-- currently messaged with unsupported, otherwise seg fault.
package  p is
  type str is array (10 downto 1) of bit_vector(7 downto 0); 
end  p;
library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use work.p.all;

entity str_vslice_2 is 
  port (
    format    : in  integer ;
    vec : out str
  ) ;
end str_vslice_2;

architecture TEST of str_vslice_2 is

begin

  process( format )
  
    variable buf            : str;
  
  begin

    for i  in 10 downto 1 loop
      buf( i ) := "00000000";      
    end loop;

    if ( format < 10 ) then
      buf( format ) := "10101010";
    end if;

    vec(format downto 1) <= buf( format downto 1 ) ;
  
  end process ;

end TEST ;

