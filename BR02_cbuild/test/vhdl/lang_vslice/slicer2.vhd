-- filename: test/vhdl/lang_vslice/slicer2.vhd
-- Description:  This test shows that cbuild incorrectly populates the if
-- condition here.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; -- for to_integer()

entity slicer2 is
  port (
    clock    : in  std_logic;
    in0      : in  std_logic_vector(3 downto 0);
    in1      : in  std_logic_vector(3 downto 0);
    in2      : in  std_logic_vector(127 downto 0);
    out1     : out std_logic);
end;

architecture arch of slicer2 is
begin
  main: process (clock)
    variable pl : std_logic_vector(3 downto 0);
    variable pr : std_logic_vector(3 downto 0);
    variable in3 : std_logic_vector(255 downto 0);
  begin
    if clock'event and clock = '1' then 
      out1 <= '0';
      -- the following line makes in2 and in3 match for the low 128 bits
      in3 := ("00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" & in2);
      -- now make it so that p1 and p2 can be used as (p1 downto p2) without a
      -- chance of a runtime error
      if in0 > in1 then
        pl := in0;
        pr := in1;
      else
        pl := in1;
        pr := in0;
      end if;
      -- now compare a dynamic slice from in2 and in3 (somewhere within the
      -- range (127:0) so we expect them to be idential, and this to always be
      -- TRUE!
      if in2(to_integer(unsigned(pl)) downto to_integer(unsigned(pr))) = in3(to_integer(unsigned(pl)) downto to_integer(unsigned(pr))) then
        out1 <= '1';
      end if;
    end if;
  end process;
end;
