-- RHS memory variable indexed slice (variable width)
-- NOT YET SUPPORTED.
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;


entity str_rhs_v_2 is 
  port (
    val       : in  integer ;
    format    : in  integer ;
    stringout : out string (10 downto 1)
  ) ;
end str_rhs_v_2;

architecture TEST of str_rhs_v_2 is

begin

  process( val, format )
  
    variable buf            : string( 10 downto 1) ;
    variable str_index      : integer := 0 ;
    variable ival           : integer ;
  
  begin
  
    ival := ABS( val ) ;
    
    if( ival = 0 ) then
      str_index := str_index + 1 ;
      buf(str_index) := '0' ;
    else
      int_loop : loop
        str_index := str_index + 1 ;
        ival := ival / 2 ;
        exit int_loop when ival=0 ;
      end loop ;
    end if ;

    for i  in 10 downto 1 loop
      buf( i ) := '0';      
    end loop;

    if (format < 10) then
      buf( format ) := 'a';
    end if;

    stringout <= buf( str_index downto 1 ) ;
  
  end process ;

end TEST ;

