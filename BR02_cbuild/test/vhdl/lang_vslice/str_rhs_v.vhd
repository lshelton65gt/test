-- RHS memory variable indexed slice (fixed width)
-- NOT YET SUPPORTED.
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity str_rhs_v is 
  port (
    format    : in  integer ;
    stringout : out string (10 downto 1)
  ) ;
end str_rhs_v;

architecture TEST of str_rhs_v is

begin

  process( format )
  
    variable buf            : string( 10 downto 1) ;
  
  begin

    for i  in 10 downto 1 loop
      buf( i ) := '0';      
    end loop;

    if ( format < 10 ) then
      buf( format ) := 'a';
    end if;

    stringout <= buf( format downto format-2 ) ;
  
  end process ;

end TEST ;

