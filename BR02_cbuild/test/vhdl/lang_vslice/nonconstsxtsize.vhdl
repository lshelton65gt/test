-- Originally extracted from Matrox.  This affects Phoenix ps_top and
-- te_top.  We didn't have a tiny test that covered this case; that is,
-- a nonconstant size expression to an extension operator.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity nonconstsxtsize is
  port (
    WEMA : out std_logic_vector(127 downto 0);
    WBEN : in std_logic_vector(3 downto 0));
end nonconstsxtsize;

architecture arch of nonconstsxtsize is

begin

  p1: process (WBEN)
    variable temp : std_logic_vector(127 downto 0);
    variable tempbit : std_logic_vector(0 downto 0);
  begin
    for i in WBEN'range loop
      tempbit(0) := not(WBEN(i));
      temp(i*32+31 downto i*32) := SXT(tempbit, (i*32+31-i*32)+1);
--      temp(i*32+31 downto i*32) := SXT(tempbit, 32);
    end loop;
    WEMA <= temp;
  end process;
end arch;
