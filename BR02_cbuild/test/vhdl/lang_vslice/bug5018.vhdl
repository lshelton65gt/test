-- This test came out of Matrox Phoenix, where we were not properly constructing
-- a eVhBiDownto operator for the variable slice.  THe commented-out workaround
-- below was created by Todd.
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity bug5018 is
  port (
    a : in std_logic_vector(31 downto 0);
    b : in std_logic_vector(1 downto 0);
    z : out std_logic_vector(7 downto 0)
    );
end bug5018;

architecture functional of bug5018 is

begin

  process (a, b)
--    variable b_high, b_low : integer;
  begin
-- Workaround
--    b_high := (8*(conv_integer(unsigned(b))+1))-1;
--    b_low := (8*conv_integer(unsigned(b)));
--    z <= a(b_high downto b_low);

    -- causes core dump
    z <= a((8*(conv_integer(unsigned(b))+1))-1 downto
           (8*conv_integer(unsigned(b)))); 
  end process;

end functional;
