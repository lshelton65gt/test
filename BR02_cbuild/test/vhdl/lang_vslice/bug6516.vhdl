-- This tests that we do not error out inadvertently on the
-- sometimes-NULL range in the if condition.  The error comes when determining
-- that this is a variable-width object; at that point we don't want to error
-- out if a null range is found.  We just record a null range as having a width
-- of 0.
entity bug6516 is
  port (
    inp  : in  bit_vector(7 downto 0);
    outp : out bit_vector(7 downto 0));
end bug6516;

architecture arch of bug6516 is
begin
  p1: process (inp)
  begin
    for i in 0 to 6 loop
      if i = 0 then
        outp <= "00000000";
      else
        -- The LHS of the comparison is NULL for i==0.
        if inp(i-1 downto 0) = inp(i+1 downto 2) then
          outp <= "11111111";
        end if;
      end if;
    end loop;
  end process p1;
end arch;
