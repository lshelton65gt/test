-- filename: test/vhdl/lang_vslice/slicer.vhd
-- Description:  This test checks that a dynamic slice of a vector (on LHS and
-- RHS) are supported


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; -- for to_integer()

entity slicer is

  port (
    clock    : in  std_logic;
    in0      : in  std_logic_vector(3 downto 0);
    in1      : in  std_logic_vector(3 downto 0);
    in2      : in  std_logic_vector(127 downto 0);
    out1     : out std_logic_vector(127 downto 0));
end;

architecture arch of slicer is
begin
  main: process (clock)
    variable pl : std_logic_vector(3 downto 0);
    variable pr : std_logic_vector(3 downto 0);
  begin
    if clock'event and clock = '1' then 
      out1 <= (others => '0');
      if in0 > in1 then
        pl := in0;
        pr := in1;
      else
        pl := in1;
        pr := in0;
      end if;
      out1(to_integer(unsigned(pl)) downto to_integer(unsigned(pr))) <= in2(to_integer(unsigned(pl)) downto to_integer(unsigned(pr))); 
    end if;
  end process;
end;
