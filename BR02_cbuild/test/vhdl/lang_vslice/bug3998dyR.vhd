-- last mod: Wed Feb 14 2007
-- filename: test/vhdl/lang_vslice/bug3998dyR.vhd
-- Description:  1) This test includes a slice of a slice of a std_logic_vector (temp1)
--		 2) This test includes a slice of a slice of a slice of a std_logic_vector (temp1). 
--               As of Feb 2007 this is not supported.
    
LIBRARY IEEE ;
USE IEEE.std_logic_1164.ALL ;
USE IEEE.std_logic_unsigned.ALL ;

entity bug3998dyR is
        port( clk : in std_logic;
              j : in integer;
              k : in integer;
              m : in integer;
              in1 : in std_logic_vector(7 downto 0);
              out1 : out std_logic_vector(7 downto 0)
            );
end bug3998dyR;

architecture arch_logic1 of bug3998dyR is
signal temp1 : std_logic_vector(31 downto 0);  
signal temp2 : std_logic_vector(7 downto 0);
begin
--        out1 <= temp2;
  out1(7 downto 0) <= temp2(7 downto 0);
  temp1(7 downto 0) <= in1(7 downto 0);

  P1:process(in1, j, k, m, temp1)
  begin
    
    temp2(7 downto 0) <= temp1(j downto j-20)(k downto k-7);
    temp2(7 downto 0) <= temp1(j+1 downto j-19)(k downto k-7);
    temp2(7 downto 0) <= temp1(j+2 downto j-18)(k downto k-7);
    temp2(7 downto 0) <= temp1(j+3 downto j-17)(k downto k-7);
    temp2(7 downto 0) <= temp1(j downto j-20)(m downto m-10)(k downto k-7);
  end process;

end arch_logic1;
