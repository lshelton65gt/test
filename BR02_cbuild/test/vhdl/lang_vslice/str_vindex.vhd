-- test for variable index to memory (string)

entity str_vindex is
	port( 
                 x    : in integer;
		 out1 : out boolean);
end str_vindex;

architecture arch_STR1 of str_vindex is
begin

  process ( x )
    variable idx : integer;
    variable in1 : STRING(1 to 3);
    constant CCC : string := "bab";     --undeclared width, must be calculated from size of literal string

  begin
    idx := (x mod 2) + 1;
    in1 := CCC;
    if (x > 100000) then
      in1 := "bbb";
    else
      if ( x > 1000) then
        in1 := "aaa";
      end if;
    end if;
    out1  <=  (in1(idx)  /= in1(idx+1) );

  end process;

end arch_STR1;
