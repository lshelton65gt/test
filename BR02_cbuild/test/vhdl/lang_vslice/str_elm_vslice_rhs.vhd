-- variable indexed slice in a memory element on RHS
package  p is
  type str is array (2 downto 1) of bit_vector(7 downto 0); 
end  p;
library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use work.p.all;

entity str_elm_vslice_rhs is 
  port (
    format   : in integer ;
    vec      : out bit_vector(3 downto 0)
  ) ;
end str_elm_vslice_rhs;

architecture TEST of str_elm_vslice_rhs is

begin
  process( format )

    variable ival : integer;
    variable j : integer;
    variable buf  : str;
  
  begin
      
      if ((format mod 2) = 0) then
        j := 1;
        buf(2) := "01010101";
      else
        j := 2;
        buf(1) := "00000000";
      end if;
      
      buf( j ) := "11111111";

      ival := format mod 8;

      if (ival > 3) then
        vec <= buf(j)( ival downto ival-3 ) ;
      else
        vec <= buf(1)( ival+3 downto ival ) ;
      end if;

  end process ;

end TEST ;
