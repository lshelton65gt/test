-- May 2008
-- This file reproduces the bug 8562.
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

entity bug8562_01 is
   port( 
      CHATIM           : in     std_logic_vector (17 downto 0);
      CHANA_DIVIDE     : in     std_logic_vector (3 downto 0);
      D_OUT            : out    std_logic_vector (17 downto 0)
   );

-- Declarations

end bug8562_01;



architecture RTL of bug8562_01 is
    signal CHANA_DIVIDE_INT : std_logic_vector(4 downto 0) := (others => '0');
    signal PSF_DIVISOR      : std_logic_vector(4 downto 0) := (others => '0');
  
    signal PSF_DVDND1       : std_logic_vector(17 downto 0) := (others => '0');
    signal PSF_QUOTIENT     : std_logic_vector(17 downto 6) := (others => '0');

    
begin
  
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------

  PSF_DIVIDE_COMB1 : process (CHATIM, PSF_DIVISOR, CHANA_DIVIDE, CHANA_DIVIDE_INT)
  variable DVDND : std_logic_vector(17 downto 0) ;  
  variable QUOTIENT : std_logic_vector(17 downto 0) ;  
  -- Do only the first 9 of 18 iterations in this stage, and then register its intermediate result
  -- for input into the second stage.
    begin
        CHANA_DIVIDE_INT <= '0' & CHANA_DIVIDE ;
        PSF_DIVISOR <= CHANA_DIVIDE_INT + "01" ;
        DVDND := CHATIM(17 downto 0) ; 
        QUOTIENT := (others => '0') ;    -- Initialize quotient to all zeros.
        for i in 1 to 6 loop
          if (DVDND(17 downto 18-i) >= PSF_DIVISOR) then
            QUOTIENT(18-i) := '1' ;
            if (i <= 5) then
              DVDND(17 downto 18-i) := DVDND(17 downto 18-i) - PSF_DIVISOR(i-1 downto 0);--this line is tested
            else
              DVDND(22-i downto 18-i) := DVDND(22-i downto 18-i) - PSF_DIVISOR(4 downto 0) ;
            end if ;
          end if ;
        end loop ;

      PSF_QUOTIENT(17 downto 12) <= QUOTIENT(17 downto 12) ;
      PSF_DVDND1 <= DVDND ;

    end process PSF_DIVIDE_COMB1 ;
    
     D_OUT <= PSF_DVDND1;
end RTL ;
    
