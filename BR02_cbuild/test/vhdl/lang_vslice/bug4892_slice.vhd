-- a test case derived from bug4892 for variable indexed slice
-- currently, populate error handling the range.
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity bottom is
port (
      clk : in std_logic;
      rst : in std_logic;
      enable  : in  std_logic_vector(  7 downto 0);
      cmpsurf : in  std_logic_vector(  9 downto 0);
      perf    : out std_logic_vector(187 downto 140)
);
end bottom;

architecture functional of bottom is
  begin
    IMPORTANT_WORK : process(clk,rst,enable,cmpsurf)
    begin
      if (rst = '1') then
        perf <= (others => '0');
      elsif (clk'event and clk = '1') then
        if (enable(1)='1') then
            perf(conv_integer(unsigned(cmpsurf(8 downto 6)))+152 downto conv_integer(unsigned(cmpsurf(8 downto 6)))+150) <= "111";
        elsif (enable(2)='1') then
            perf(conv_integer(unsigned(cmpsurf(8 downto 6)))+140 downto conv_integer(unsigned(cmpsurf(8 downto 6)))+139) <= "11";
        elsif (enable(3)='1') then
            perf(conv_integer(unsigned(cmpsurf(8 downto 6)))+160 downto conv_integer(unsigned(cmpsurf(8 downto 6)))+156) <= "11111";
        elsif (enable(4)='1') then
            perf(conv_integer(unsigned(cmpsurf(8 downto 6)))+168) <= '1';
        elsif (enable(5)='1') then
            perf(conv_integer(unsigned(cmpsurf(8 downto 6)))+176) <= '1';
        end if;
      end if;
    end process;
end functional;

library IEEE;
use IEEE.std_logic_1164.all;

entity bug4892_slice is
  port (
    clk : in std_logic;
    rst : in std_logic;
    enable  : in     std_logic_vector(  7 downto 0);
    cmpsurf : in     std_logic_vector(  9 downto 0);
    perf    : out    std_logic_vector(187 downto 128)
    );
end bug4892_slice;

architecture functional of bug4892_slice is

  component bottom is
  port (
      clk : in std_logic;
      rst : in std_logic;
      enable  : in     std_logic_vector(  7 downto 0);
      cmpsurf : in     std_logic_vector(  9 downto 0);
      perf    : out    std_logic_vector(187 downto 140)
      );
  end component;

  begin
    IMPORTANT_WORK : process(enable,cmpsurf)
    begin
        perf(139 downto 128) <= (others => '0');
    end process;
    BOTTOM_INSTANCE : bottom
    port map (
      clk => clk,
      rst => rst,
      enable => enable,
      cmpsurf => cmpsurf,
      perf => perf(187 downto 140)
    );
  end functional;
