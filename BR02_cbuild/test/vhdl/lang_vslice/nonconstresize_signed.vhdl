-- Modified from nonconstsxtsize.vhdl to check the unsigned resize operator, too.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nonconstresize_signed is
  port (
    WEMA : out std_logic_vector(127 downto 0);
    WBEN : in std_logic_vector(3 downto 0));
end;

architecture arch of nonconstresize_signed is

begin

  p1: process (WBEN)
    variable temp : std_logic_vector(127 downto 0);
    variable tempbit : signed(0 downto 0);
  begin
    for i in WBEN'range loop
      tempbit(0) := not(WBEN(i));
      temp(i*32+31 downto i*32) := std_logic_vector(RESIZE(tempbit, (i*32+31-i*32)+1));
--      temp(i*32+31 downto i*32) := RESIZE(tempbit, 32);
    end loop;
    WEMA <= temp;
  end process;
end arch;
