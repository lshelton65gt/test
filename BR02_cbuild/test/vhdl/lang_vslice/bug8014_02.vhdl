-- November 2007
-- This file tests (others =>) for variable  slice.
-- The temp1 and temp2 have downto range, where temp1 is not 0 based.
-- The temp3 and temp4 have to range, where temp3 is not 0 based.

entity bug8014_02 is
  port (
    clk, rst   :  in  bit;
    data_out1  :  out bit_vector(9 downto 1);
    data_out2  :  out bit_vector(8 downto 0);
    data_out3  :  out bit_vector(1 to 9);
    data_out4  :  out bit_vector(0 to 8)
    );
end;

architecture arch of bug8014_02 is
  signal temp1 : bit_vector(9 downto 1);
  signal temp2 : bit_vector(8 downto 0);
  signal temp3 : bit_vector(1 to 9);
  signal temp4 : bit_vector(0 to 8);
begin  -- arch


  p1: process (clk, rst)
    variable bit_number : integer := 3;
  begin  -- process p1
    if rst = '0' then                   -- asynchronous reset (active low)
      temp1 <= (others => '0');  
      temp2 <= (others => '0');  
      temp3 <= (others => '0');  
      temp4 <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      bit_number := bit_number + 1;
      if(bit_number = 7) then
        bit_number := 3;
      end if;

      

    temp1(9 downto bit_number) <= (others => '0');        -- this line is tested
    temp1(bit_number-1 downto 1 ) <= (others => '1');     -- this line is tested
--    temp1 <= (others => '0');

    temp2(8 downto bit_number) <= (others => '0');        -- this line is tested
    temp2(bit_number-1 downto 0 ) <= (others => '1');     -- this line is tested

--      temp2 <= (others => '0');

    temp3(1 to bit_number-1 ) <= (others => '0');         -- this line is tested
    temp3(bit_number to 9) <= (others => '1');            -- this line is tested

    temp4(0 to bit_number-1 ) <= (others => '0');         -- this line is tested
    temp4(bit_number to 8) <= (others => '1');            -- this line is tested
       
      
    end if;
  end process p1;

  data_out1 <= temp1;
  data_out2 <= temp2;
  data_out3 <= temp3;
  data_out4 <= temp4;

end arch;


