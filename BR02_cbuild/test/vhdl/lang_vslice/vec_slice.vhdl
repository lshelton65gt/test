-- Reproduces bug 7304. The slice is incorrectly bounded based
-- on initial value of vec_index. This leads to simulation error.
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;


entity vec_slice is 
  port (
    in1  : in std_logic_vector(7 downto 0);
    idx  : in std_logic_vector(2 downto 0) ;
    out1 : out std_logic_vector(7 downto 0)
    ) ;
end vec_slice;

architecture TEST of vec_slice is

begin

  process( idx, in1 )
    variable vec_index      : integer range 0 to 7 := 1 ;
  begin

    out1(vec_index downto 0) <= in1(vec_index downto 0) ;
    vec_index := conv_integer(unsigned(idx));
    
  end process ;

end TEST ;

