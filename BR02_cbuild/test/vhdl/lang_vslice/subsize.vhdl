--testcase that tests our determining how wide the addition is.
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
entity subsize is
  port(INPUT : in std_logic_vector(34 downto 0);  
       SUM : out std_logic_vector(4 downto 0));
end subsize;

architecture sim of subsize is
 
begin
  sum_up : process (INPUT)
  variable tmp_sum : UNSIGNED (4 downto 0);
  begin
     tmp_sum := UNSIGNED(INPUT(4 downto 0));
     for i in 1 to 6 loop
       tmp_sum := tmp_sum 
                  - UNSIGNED(INPUT((i+1)*5-1 downto i*5));
     end loop;
     SUM <= std_logic_vector(tmp_sum);
  end process sum_up;
end sim;
