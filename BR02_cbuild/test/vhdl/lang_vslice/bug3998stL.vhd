-- last mod: Wed Feb 14 2007
-- filename: test/vhdl/lang_vslice/bug3998stL.vhd
-- Description:  1) This test includes a slice of a slice of a std_logic_vector (temp2). 
--		 2) This test includes a slice of a slice of a slice of a std_logic_vector (temp2).
--               As of Feb 2007 this is not supported.

LIBRARY IEEE ;
USE IEEE.std_logic_1164.ALL ;
USE IEEE.std_logic_unsigned.ALL ;

entity bug3998stL is
        port( clk : in std_logic;
              j : in integer;
              k : in integer;
              in1 : in std_logic_vector(7 downto 0);
              out1 : out std_logic_vector(63 downto 0)
            );
end bug3998stL;

architecture arch_logic1 of bug3998stL is
signal temp2 : std_logic_vector(63 downto 0);
begin
        out1 <= temp2;

        P1:process(in1, j, k)
        begin
          temp2(20 downto 0)(10 downto 3) <= in1;
          temp2(21 downto 1)(10 downto 3) <= in1;
          temp2(22 downto 2)(10 downto 3) <= in1;
          temp2(23 downto 3)(10 downto 3) <= in1;
          temp2(20 downto 0)(20 downto 0)(10 downto 3) <= in1;
        end process;

end arch_logic1;
