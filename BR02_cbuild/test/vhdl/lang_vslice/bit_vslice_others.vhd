-- variable slice for bit_vector, currently gets populate error
-- due to the problem finding  range/width (others ==>'0')

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;


entity bit_vslice_others is 
  port (
    val       : in  integer ;
    format    : in  integer ;
    stringout : out bit_vector( 10 downto 1)
  ) ;
end bit_vslice_others;

architecture TEST of bit_vslice_others is

begin

  process( val, format )
  
    variable buf            : bit_vector( 10 downto 1) ;
    variable str_index      : integer := 0 ;
    variable ival           : integer ;
  
  begin
  
    ival := ABS( val ) ;
    
    if( ival = 0 ) then
      str_index := str_index + 1 ;
      buf(str_index) := '0' ;
    else
      int_loop : loop
        str_index := str_index + 1 ;
        case ( ival mod 10 ) is
          when 0      => buf(str_index) := '0' ;
          when 1      => buf(str_index) := '1' ;
          when 2      => buf(str_index) := '0' ;
          when 3      => buf(str_index) := '1' ;
          when 4      => buf(str_index) := '1' ;
          when 5      => buf(str_index) := '0' ;
          when 6      => buf(str_index) := '1' ;
          when 7      => buf(str_index) := '1' ;
          when 8      => buf(str_index) := '0' ;
          when 9      => buf(str_index) := '0' ;
          when others => null ;
        end case ;
        ival := ival / 2 ;
        exit int_loop when ival=0 ;
      end loop ;
    end if ;
    
    if( format > str_index ) then
      buf( format downto str_index + 1 ) := ( others => '0' ) ;
      str_index := format ;
    end if ;
    
    stringout <= buf( str_index downto 1 ) ;
  
  end process ;

end TEST ;

