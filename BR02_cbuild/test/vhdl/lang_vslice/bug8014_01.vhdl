-- November 2007
-- This file was created based on customers code.
-- The problem was with (others =>) assigned to a variable slice.

entity bug8014_01 is
  port (
    frac_ratio :  bit_vector(9 downto 0));
end;

architecture arch of bug8014_01 is
  CONSTANT IEEEF19MANTSIZE : INTEGER := 10;
  signal expadjust : integer;
  signal fracnew : bit_vector(IEEEF19MANTSIZE-1 downto 0);
begin  -- arch

  process(frac_ratio)
  begin
    expadjust <= 1;
    fracnew   <= (others => '0');
    for i in IEEEF19MANTSIZE-1 downto 0 loop
      if (frac_ratio(i) = '1') then
        expadjust <= IEEEF19MANTSIZE-i;
        if (i /= 0) then
          fracnew(IEEEF19MANTSIZE-1 downto IEEEF19MANTSIZE - i) <= frac_ratio(i-1 downto 0);
          fracnew(IEEEF19MANTSIZE-i-1 downto 0)                 <= (others => '0');
          fracnew(i downto 0)                 <= (others => '0');
        end if;
        exit;
      end if;
    end loop;
 end process;

end arch;
