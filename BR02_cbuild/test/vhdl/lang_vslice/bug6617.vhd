-- last mod: Sun Sep 24 20:45:03 2006
-- filename: test/vhdl/lang_vslice/bug6617.vhd
-- Description:  This test is a simple version of the test from bug6617, the
-- issue was that a 36 bit constant (with the most significant 4 bits being
-- zero) was being partselected incorrectly because with -no-OOB the constant
-- was being stored in a 32 bit value, not a 36 bit value.  This was a mistake.
-- MUST BE RUN WITH -no-OOB to see this problem


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6617 is
  port (
    clock    : in  std_logic;
    psflushsent : in std_logic;
    idx      : in integer range 8 downto 0;
    in1, in2 : in  std_logic_vector(7 downto 0);
    ikdbg    : out std_logic_vector( 15 downto   0));
end;

architecture arch of bug6617 is 
type smwlkaatype is (IDLE, NORM, CMD, NORMAA, FREEFRAG, KILLFRAG,
                     AAPAL123, AA4PAL123, AA4PAL4);
type smwlkaatbltyp is array (smwlkaatype range IDLE to AA4PAL4) of
      std_logic_vector(3 downto 0);

type idxtotyp is array (integer range 8 downto 0) of
      std_logic_vector(3 downto 0);

constant filler1 : smwlkaatype := AAPAL123;

signal smwlkaastate      : smwlkaatype;

   constant SMWLKAA2SLV : smwlkaatbltyp :=
      (IDLE      => "0000",
       NORM      => "0001",
       CMD       => "0010",
       NORMAA    => "0011",
       FREEFRAG  => "0100",
       KILLFRAG  => "0101",
       AAPAL123  => "0110",
       AA4PAL123 => "0111",
       AA4PAL4   => "1000");
constant idx2slv : idxtotyp :=
      (8 => "0000",
       7 => "0001",
       6 => "0010",
       5 => "0011",
       4 => "0100",
       3 => "0101",
       2 => "0110",
       1 => "0111",
       0 => "1000");
  
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
--     if in1 > "00000100" then
--       smwlkaastate := filler1;
--     else
--       smwlkaastate := AA4PAL4;
--     end if;
     if smwlkaastate = IDLE then
       smwlkaastate <= NORM;
     elsif smwlkaastate = NORM then
       smwlkaastate <= CMD;
     elsif smwlkaastate = CMD then
       smwlkaastate <= NORMAA;
     elsif smwlkaastate = NORMAA then
       smwlkaastate <= FREEFRAG;
     elsif smwlkaastate = FREEFRAG then
       smwlkaastate <= KILLFRAG;
     elsif smwlkaastate = KILLFRAG then
       smwlkaastate <= AAPAL123;
     elsif smwlkaastate = AAPAL123 then
       smwlkaastate <= AA4PAL123;
     elsif smwlkaastate = AA4PAL123 then
       smwlkaastate <= AA4PAL4;
     elsif smwlkaastate = AA4PAL4 then
       smwlkaastate <= IDLE;
     end if;
      ikdbg <= psflushsent &
--            SMWLKAA2SLV(smwlkaastate) &
               idx2slv(integer(idx)) &
            in1(1) &                   
            in1(4) &                   
            in1(2) &
            in1(7) &
            in1(6) &
            in1(5) &
            in2(0) &
            in2(3) &
            in1(3) &
            in2(1) &
            in2(3); 

    end if;
  end process;
end;
