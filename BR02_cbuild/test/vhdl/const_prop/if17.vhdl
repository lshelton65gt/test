-- October 2008
-- Tests if-the-else block statement, which doesn't have
-- niether else if nor else blocks. There are four possible
-- options. See bellow for details.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if17 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 3);
    out1 : out std_logic;
    out2 : out std_logic;
    out3 : out std_logic;
    out4 : out std_logic
    );
end if17;


architecture if17 of if17 is

begin

  p1: process ( sel, in1 )
    variable temp : integer := 0;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 1;
    if (temp > 0) then  -- this is statically true
      temp := 3;
    end if;
    out1<= in1(temp);   -- constant propagation should make temp = 3
  end process;

  p2: process ( sel, in1 )
    variable temp : integer := 0;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (temp < 2) then  -- this is statically false
      temp := 1;
    end if;
    out2<= in1(temp);   -- constant propagation should make temp = 3
  end process;
  
  p3: process ( sel, in1 )
    variable temp : integer := 0;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel < 2) then -- the condition is not static
      temp := 1;
    end if;
    out3<= in1(temp);     -- constant propagation should do nothing
  end process;

  p4: process ( sel, in1 )
    variable temp : integer := 0;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 2;
    if (int_sel < 2) then  -- the condition is not static
      temp := 2;
    end if;
    out4<= in1(temp);      -- constant propagation should make temp = 2  
  end process;

end;
