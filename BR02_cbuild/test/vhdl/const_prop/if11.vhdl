-- The temp is a shared variable. Shared variables are not constant propagated
-- since they could be aliased to a net in the instantiated 
-- sub entity or the parent entity, and we don't resolve such
-- aliases at population time. 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if11 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(5 downto 0);
    out1 : out std_logic_vector(5 downto 0);
    out2 : out std_logic
    );
end if11;


architecture if11 of if11 is

  shared variable temp : integer := 2;
  
  impure function get_pattern (in1 : std_logic_vector) return std_logic_vector is
    variable in1_cpy : std_logic_vector(in1'range) := (others => '0');
  begin  -- get_pattern
    temp := 3;
    in1_cpy := in1;
    in1_cpy(temp) := '1';
    return in1_cpy;
  end get_pattern;

begin

  process ( sel, in1 )
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel = 0) then
      out1 <= get_pattern(in1) and in1;
    elsif (int_sel = 1) then
      out1 <= not in1;
    elsif (int_sel = 2) then
      out1 <= "11001" & in1(temp);
    else
      out1 <= in1;
    end if;
    out2 <= in1(temp);
  end process;

end if11;
