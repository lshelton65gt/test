-- Demonstrates a flaw in population. Even though the elsif condition
-- is always true and if-then condition always false, population
-- populates both if-then and elsif blocks. This causes temp to be
-- invalidated by a missing def in the if-then block. The assign
-- statement that follows does not see temp as constant.
-- It does demonstrate though that if statement condition is
-- evaluated with propagated constants prior to those constants being
-- invalidated. Bug7386
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if5 is
  port (
    clk : in std_logic;
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end if5;


architecture if5 of if5 is

begin

  process ( in1 )
    variable temp : integer := 2;
  begin
    temp := 3;
    if (temp = 2) then
      out1 <= "110110" and in1;
      temp := 3;
    elsif (temp = 3) then
      out1 <= not in1;
      temp := 4;
    elsif (temp = 4) then
      out1 <= "011110" xor in1;
      temp := 5;
    else
      out1 <= in1;
      temp := 6;
    end if;
    out2 <= in1(temp);
  end process;

end if5;
