-- Tests override of initial value of a variable with an assignment. The assigned
-- value should propagate.
library ieee;
use ieee.std_logic_1164.all;

entity func2 is
  
  port (
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0));

end func2;

architecture func2 of func2 is

  function get_vec (in1 : std_logic_vector) return std_logic_vector is
    variable temp : integer := 0;
    variable cpy : std_logic_vector(in1'range) := (others => '0');
  begin  -- get_vec
    temp := 2;
    cpy := in1;
    cpy(temp) := in1(temp) xor '1';
    return cpy;
  end get_vec;
  
begin  -- func2

  out1 <= get_vec(in1);

end func2;
