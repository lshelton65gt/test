-- A case statement with select that is constant propagated.
-- The select is def'ed within the case statement, but that
-- should not stop the constant propagation to select.
-- ADDITIONAL WORK: The case statement population should
-- avoid populating unnecessary case items if select is constant. Bug7385
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity case5 is
  port (
    clk : in std_logic;
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end case5;


architecture case5 of case5 is

begin

  process ( in1 )
    variable int_sel : integer := 0;
  begin
    int_sel := 2;
    case int_sel is
      when 0 =>
        out1 <= "110110" and in1;
        int_sel := 1;
      when 1 =>
        out1 <= not in1;
        int_sel := 2;
      when 2 =>
        out1 <= "011110" xor in1;
        int_sel := 3;
      when others =>
        out1 <= in1;
        int_sel := 0;
    end case;
    out2 <= in1(int_sel);
  end process;

end case5;
