-- Tests a recursive function call from a for loop where the arguments to the
-- function and the lvalue slice vary from iteration to iteration. This wasn't
-- populatable prior to local constant propagation. This for loop is automatically
-- unrolled.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;


entity for2 is
  port (
    in1 : in std_logic_vector(7 downto 0);
    out1 : out std_logic_vector(63 downto 0)
    );
end for2;


architecture for2 of for2 is

  function recur(param1 : std_logic_vector) return std_logic_vector is
    variable return_vec : std_logic_vector(param1'range);
    variable len : integer;
    variable mid : integer;
  begin
    len := param1'length;
    mid := (param1'low + param1'high )/2 + 1;
    if ( len = 1) then
      return_vec := param1;
      return return_vec;
    else
      return_vec(param1'high downto mid) := 
        recur(param1(param1'high downto mid));
      return_vec(mid - 1 downto param1'low) :=
        recur(param1(mid - 1 downto param1'low));
      return return_vec;
    end if;
  end;

begin

  process ( in1 )
    variable bound : integer;
    variable zeros : std_logic_vector(7 downto 0);
  begin
    bound := 8;
    zeros := (others => '0');
    for i in 0 to bound-1 loop
      out1(i*8+7 downto i*8) <= zeros(bound-1 downto bound-i) & recur(in1(bound-i-1 downto 0));
    end loop;  -- i
  end process;

end for2;
