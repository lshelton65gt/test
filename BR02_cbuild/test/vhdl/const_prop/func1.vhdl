-- Tests propagation of variable's constant initial value.
library ieee;
use ieee.std_logic_1164.all;

entity func1 is
  
  port (
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0));

end func1;

architecture func1 of func1 is

  function get_vec (in1 : std_logic_vector) return std_logic_vector is
    variable temp : integer := 2;
    variable cpy : std_logic_vector(in1'range) := (others => '0');
  begin  -- get_vec
    cpy := in1;
    cpy(temp) := in1(temp) xor '1';
    return cpy;
  end get_vec;
  
begin  -- func1

  out1 <= get_vec(in1);

end func1;
