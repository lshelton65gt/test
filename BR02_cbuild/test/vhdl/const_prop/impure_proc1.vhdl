library ieee;
use ieee.std_logic_1164.all;

entity impure_proc1 is
  
  port (
    in1  : in  std_logic_vector(31 downto 0);
    out1 : out std_logic_vector(3 downto 0);
    out2 : out std_logic);

end impure_proc1;

architecture arch of impure_proc1 is

  shared variable var : integer := 28;

  procedure impure_proc (in1 : in std_logic_vector; out1 : out std_logic_vector) is
    variable incr : integer := 4;
  begin  -- impure_proc
    var := var + incr;
    out1 := in1(var-1 downto var-4);
    if (var = 32) then
      var := 4;
    end if;
  end impure_proc;
  
begin  -- arch

  process (in1)
    variable temp : std_logic_vector(3 downto 0) := (others => '0');
  begin  -- process
    var := 8;
    impure_proc(in1, temp);
    out1 <= temp;
    out2 <= in1(var-1);
  end process;

end arch;
