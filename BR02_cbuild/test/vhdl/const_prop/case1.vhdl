-- A fully specified case statement with variable temp not
-- def'ed in any case items. The statement RHS should
-- convert from in1(temp) to in1(3)
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity case1 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end case1;


architecture case1 of case1 is

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    case int_sel is
      when 0 =>
        out1 <= "110110" and in1;
      when 1 =>
        out1 <= not in1;
      when 2 =>
        out1 <= "011110" xor in1;
      when others =>
        out1 <= in1;
    end case;
    out2 <= in1(temp);
  end process;

end case1;
