-- Tests value based recursion termination. Compiler fails
-- to determine the termination of value based recursion,
-- which results in infinite recursion check to fail.
-- This can be made to work with little more effort. Josh's
-- idea is to populate them as they are, without attempting
-- to unroll recursion, and let it execute at runtime.
library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity recur_func2 is
  port (
    in1    :  in      integer;
    out1   :  out     integer);
end recur_func2;

architecture arch of recur_func2 is

  function recur (in1 : integer) return integer is
    constant mult : integer := 8;
  begin  -- recur
    if in1 = 0 then
      return 0;
    else
      return recur(in1-1) + mult;
    end if;
  end recur;

begin  -- arch

  process (in1)
    variable cnt : integer;
  begin  -- process
    cnt := 4;
    out1 <= in1 + recur(cnt);
  end process;
end arch;
