-- Populates variable ranged slices from variable ranged slices. The
-- variable ranges are computed into variables before used as slice
-- bounds. This loop works without the need for unrolling at population time.
-- Also verifies that a variable def'ed in the loop is invalidates
-- for constant propagation.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;


entity for4 is
  port (
    in1 : in std_logic_vector(7 downto 0);
    out1 : out std_logic_vector(63 downto 0);
    out2 : out std_logic
    );
end for4;


architecture for4 of for4 is

begin

  process ( in1 )
    variable bound : integer;
    variable zeros : std_logic_vector(7 downto 0);
    variable temp  : integer;
    variable temp1 : integer;
  begin
    bound := 8;
    zeros := (others => '0');
    temp  := 0;
    for i in bound-1 downto 0 loop
      temp := i*8;
      temp1 := bound - i;
      out1(temp+7 downto temp) <= zeros(bound-1 downto temp1) & in1(temp1-1 downto 0);
    end loop;  -- i
    out2 <= in1(temp);                  -- temp is not constant.
  end process;

end for4;
