-- Tests constant propagation in a function invoked
-- from within as well as outside the process.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity func_init is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(5 downto 0);
    out1 : out std_logic_vector(5 downto 0)
    );
end func_init;


architecture func_init of func_init is

  function get_pattern(sz : in integer) return std_logic_vector is
    variable pattern : std_logic_vector(sz-1 downto 0);
    variable toggle  : std_logic := '0';
  begin
    for i in sz-1 downto 0 loop
      pattern(i) := toggle;
      toggle := not toggle;
    end loop;
    return pattern;
  end get_pattern;

  impure function get_vec(sel : in std_logic_vector(1 downto 0);
                          in1 : in std_logic_vector(5 downto 0))
    return std_logic_vector is
    variable temp : integer := 2;
    variable int_sel : integer := 0;
    variable out1 : std_logic_vector(5 downto 0);
  begin
    temp := 3;
    case int_sel is
      when 0 =>
        temp := 2;
        out1 := get_pattern(6-temp) & in1(temp-1 downto 0);
      when 1 =>
        temp := 3;
        out1 := get_pattern(6-temp) & (not in1(temp-1 downto 0));
      when 2 =>
        temp := 6;
        out1 := get_pattern(temp) xor in1;
      when others =>
        out1 := in1;
    end case;
    out1(temp-1) := '1';
    return out1;
  end get_vec;

begin

  process ( sel, in1 )
    variable pattern : std_logic_vector(5 downto 0) := get_vec("11", "000000");
  begin
    out1 <= pattern xor get_vec(sel, in1);
  end process;

end func_init;
