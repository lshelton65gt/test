-- Tests if-the-else block statement. Although the
-- temp is def'ed in some if statement blocks, the
-- constant value in parent context should be available
-- to each if block, unless it's def'ed prior to use in
-- it's context. This constant value should be propagated
-- into function calls to assist with recursion termination.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if9 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(5 downto 0);
    out1 : out std_logic_vector(5 downto 0)
    );
end if9;


architecture if9 of if9 is

  function recur (in1 : in std_logic_vector; sz : in integer) return std_logic_vector is
    variable retvec : std_logic_vector(sz-1 downto 0) := (others => '0');
  begin  -- recur
    if (sz = 2) then
      retvec := not in1(sz-1 downto 0);
    else
      retvec := in1(sz-1 downto 0) xor ('0' & recur(in1(sz-2 downto 0), sz-1));
  end if;
  return retvec;
  end recur;

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel = 0) then
      temp := 4;
      out1 <= "10" & recur(in1(temp-1 downto 0), temp);  -- temp is 4.
    elsif (int_sel = 1) then
      out1 <= "101" & recur(in1(temp-1 downto 0), temp); -- temp is 3.
    elsif (int_sel = 2) then
      out1 <= "110" & recur(in1(temp-1 downto 0), temp); -- temp is 3.
    else
      temp := 2;
      out1 <= "1011" & recur(in1(temp-1 downto 0), temp); -- temp is 2.
    end if;
  end process;

end if9;
