-- A fully specified case statement with variable temp is
-- def'ed in one case item by the same value as it's current
-- value, but conditionally. The RHS of assign statement after
-- case statement should convert from in1(temp) to in1(3), since
-- the temp value is always the same.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity case9 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end case9;


architecture case9 of case9 is
begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    case int_sel is
      when 0 =>
        if (in1(temp) = '1') then
          temp := 3;
        end if;
        out1 <= "110111" and in1;
      when 1 =>
        out1 <= not in1;
      when 2 =>
        out1 <= "011110" xor in1;
      when others =>
        out1 <= in1;
    end case;
    out2 <= in1(temp);
  end process;

end case9;
