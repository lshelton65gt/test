-- Tests if-the-else block statement. Variable temp ought
-- not be invalidated since it's not def'ed in any if-the-else
-- blocks.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if1 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end if1;


architecture if1 of if1 is

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel = 0) then
      out1 <= "110110" and in1;
    elsif (int_sel = 1) then
      out1 <= not in1;
    elsif (int_sel = 2) then
      out1 <= "011110" xor in1;
    else
      out1 <= in1;
    end if;
    out2 <= in1(temp);
  end process;

end if1;
