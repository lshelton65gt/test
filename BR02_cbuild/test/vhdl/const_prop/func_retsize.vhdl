-- Demonstrates a Jaguar bug. The size of get_vec function found
-- using vhGetExprSize(get_vec) return 2:0 instead of 3:0. This
-- results in simulation mismatch. Bug7387.
library ieee;
use ieee.std_logic_1164.all;

entity func_retsize is

 port (
   in1  : in  std_logic_vector(3 downto 0);
   out1 : out std_logic_vector(3 downto 0));

end func_retsize;

architecture func_retsize of func_retsize is

 function get_vec (in1 : std_logic_vector) return std_logic_vector is
   variable temp : integer := 2;
 begin  -- get_vec
   temp := temp + 1;
   return in1(temp downto 0);
 end get_vec;

begin  -- func_retsize

 process (in1)
 begin  -- process
   out1 <= get_vec(in1);
 end process;

end func_retsize;

