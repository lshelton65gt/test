-- Simpler version of for2.vhdl. Simply populates variable ranged slices
-- from variable ranged slices. This loop does not need unrolling.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;


entity for3 is
  port (
    in1 : in std_logic_vector(7 downto 0);
    out1 : out std_logic_vector(63 downto 0)
    );
end for3;


architecture for3 of for3 is

begin

  process ( in1 )
    variable bound : integer;
    variable zeros : std_logic_vector(7 downto 0);
  begin
    bound := 8;
    zeros := (others => '0');
    for i in 0 to bound-1 loop
      out1(i*8+7 downto i*8) <= zeros(bound-1 downto bound-i) & in1(bound-i-1 downto 0);
    end loop;  -- i
  end process;

end for3;
