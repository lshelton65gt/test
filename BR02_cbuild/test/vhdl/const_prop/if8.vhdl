-- Tests if-the-else block statement. Although the
-- temp is def'ed in some if statement blocks, the
-- constant value in parent context should be available
-- to each if block, unless it's def'ed prior to use in
-- it's context.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if8 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end if8;


architecture if8 of if8 is

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel = 0) then
      temp := 4;
      out1 <= "110110" and in1;
      out2 <= in1(temp);        -- temp is 4.
    elsif (int_sel = 1) then
      out1 <= not in1;
      out2 <= in1(temp);        -- temp is 3.
    elsif (int_sel = 2) then
      out1 <= "011110" xor in1;
      out2 <= in1(temp);        -- temp is 3.
    else
      temp := 0;
      out1 <= in1;
      out2 <= in1(temp);        -- temp is 0.
    end if;
  end process;

end if8;
