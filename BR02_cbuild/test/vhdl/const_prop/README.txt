
This document describes the test plan for population time local
constant propagation.

General tests:

Constant Invalidation          Case         If          For

Not Def'ed                     case1        if1         for1

Def'ed in some but
not all mutex blocks           case2        if2         n/a

Def'ed in all mutex
blocks with same value         case3        if6         n/a

Def'ed in all mutex
blocks with diff values        case4        if7         n/a

Condition constant prop'ed
but def'ed in statement        case5        if5         for5

Parent constant value
invalidated and re-def'ed
before use                     case6        if8         for4

Constant value invalidated
by impure function call        case7        if11        for7

Parent constant value
def'ed by same value in one
but not all mutex contexts     case8        if12        n/a

Parent constant value
conditionally def'ed by same
value in one mutex context     case9                    n/a

Parent constant value
conditionally def'ed by diff
value in one mutex context     case10       if13        n/a

Parent constant value def'ed                if3         n/a
in a block that never
would execute

Recursive function calls
from statement blocks                       if9         for2

Parent constant value def'ed
by diff value but restored
prior to block exit                         if14        for1

Constant def'ed and invalidated                   
by procedure output            case12                   for8

Special for loop tests:


1. Automatic unroll of loop when a variable bound slice is specified as actual to a 
   function call within the loop.

   for2.vhdl

2. No unrolling needed with variable bound slice is in context other than function call

   for3.vhdl, for4.vhdl

Miscalleneous tests:

1. Function call from outside the process context

   func_init.vhdl

2. How far can a constant be propagated?
   a. Initial value:    func1.vhdl
   b. Initial value, then change by simple assignment: func2.vhdl
   c. Propagation of constant, temp1 used to define temp2, temp2 used to define temp3: func3.vhdl
   d. Constant is inited by function call with static args: func4.vhdl


3. How to invalidate a constant (2 cases each, one of new constant, other of non-constant)?
   a. A second assignment: func5.vhdl
   b. Assign new expression to variable: func5.vhdl


Issues and tests:

1. Function return type is populated prior to function body. This fails in cases where
   return type size can only be computed by constant propagation within the body.

   for6.vhdl

2. Consider an if-then-elsif-else chain where if-then condition is always false and elsif
   condition is always true. The elsif block is executed all the time. Defs in that
   block should be valid. However since population populates empty blocks for if-then block,
   the def invalidates variables. The fix is to populate just those blocks which will actually
   be executed.

   if5.vhdl

3. Constant propagation assumes that assign to shared variables by impure functions always
   invalidates them. However if the assigned value is the same as the current constant
   propagated value, this is pessimistic. This may lead to population issues in rare cases. 

   case7.vhdl
