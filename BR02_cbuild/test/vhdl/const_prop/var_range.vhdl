-- Most of the tests we have for slices with for loop index bounds
-- are constant width slice tests. This one has slice widths that
-- are different for every iteration. This should generate an
-- elaborated function for every iteration.
library ieee;
use ieee.std_logic_1164.all;

entity var_range is
  
  port (
    in1  : in  std_logic_vector(15 downto 0);
    out1 : out std_logic_vector(15 downto 0));

end var_range;

architecture arch of var_range is

  function func (v : std_logic_vector) return std_logic_vector is
  begin  -- func
    return v;
  end func;
  
begin  -- arch

  process (in1)
  begin  -- process
    for i in 3 downto 0 loop
      out1(3+4*i downto i) <= func(in1(3+4*i downto i));
    end loop;  -- i
  end process;

end arch;
