-- Although case item def's temp, it still has a statically
-- known constant value at the end of case statement, since
-- the case item is unconditionally executed.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity case11 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end case11;


architecture case11 of case11 is
begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    case int_sel is
      when others =>
        temp := 4;
        out1 <= in1;
    end case;
    out2 <= in1(temp);
  end process;

end case11;
