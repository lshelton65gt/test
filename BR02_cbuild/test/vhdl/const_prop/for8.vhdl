-- A procedure output def's a propagated constant invalidating it.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;


entity for8 is
  port (
    in1 : in std_logic_vector(7 downto 0);
    out1 : out std_logic_vector(63 downto 0);
    out2 : out std_logic
    );
end for8;


architecture for8 of for8 is

  procedure get_int (val : inout integer) is
  begin  -- get_int
    val := val + 1;
  end get_int;

begin

  process ( in1 )
    variable bound : integer;
    variable zeros : std_logic_vector(7 downto 0);
    variable temp  : integer;
    variable temp1 : integer;
    variable temp2  : integer;
  begin
    bound := 8;
    zeros := (others => '0');
    temp  := 2;
    temp2 := -1;
    for i in bound-1 downto 0 loop -- bound1 is constant propagated.
      temp := i*8;
      temp1 := bound - i;
      get_int(temp2);
      out1(temp+7 downto temp) <= zeros(bound-1 downto temp1) & in1(temp1-1 downto 0);
    end loop;  -- i
    out2 <= in1(temp2);                  -- temp2 is not constant.
  end process;

end for8;
