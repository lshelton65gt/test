-- Tests if-the-else block statement. Variable temp ought
-- NOT have constant value since it's NOT assigned the same
-- value in all if statement blocks.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if7 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end if7;


architecture if7 of if7 is

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel = 0) then
      temp := 4;
      out1 <= "110110" and in1;
    elsif (int_sel = 1) then
      temp := 4;
      out1 <= not in1;
    elsif (int_sel = 2) then
      temp := 5;
      out1 <= "011110" xor in1;
    else
      temp := 4;
      out1 <= in1;
    end if;
    out2 <= in1(temp); -- temp is not statically known.
  end process;

end if7;
