-- A fully specified case statement with variable temp
-- def'ed in all case items with different values. The last
-- statement RHS in1(temp) does not know it's constant value.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity case4 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end case4;


architecture case4 of case4 is

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    case int_sel is
      when 0 =>
        out1 <= "110110" and in1;
        temp := 4;
      when 1 =>
        out1 <= not in1;
        temp := 5;
      when 2 =>
        out1 <= "011110" xor in1;
        temp := 0;
      when others =>
        out1 <= in1;
        temp := 1;
    end case;
    out2 <= in1(temp);
  end process;

end case4;
