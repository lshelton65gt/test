-- A test for recursive functions where the recursion termination is
-- size based. The swapper_works function uses constants and compiles
-- successfully without constant propagation. The swapper_fails
-- uses variables and needs constant propagation to work. Bug7324.
library ieee;
use ieee.std_logic_1164.all;

entity recur_func1 is
  
  port (
    clk  : in  std_logic;
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0);
    out2 : out std_logic_vector(3 downto 0);
    mismatch : out std_logic);

end recur_func1;

architecture arch of recur_func1 is

  function swapper_fails (in1 : std_logic_vector) return std_logic_vector is
    variable len : integer;
    variable mid : integer;
    variable ret : std_logic_vector(in1'range);
  begin  -- swapper_fails
    len := in1'length;
    mid := (in1'left + in1'right)/2 + 1;

    if len = 1 then
      ret := in1;
    else
      ret(in1'left downto mid) := swapper_fails(in1(mid-1 downto in1'right));
      ret(mid-1 downto in1'right)  := swapper_fails(in1(in1'left downto mid));
    end if;
    return ret;
  end swapper_fails;

  function swapper_works (in1 : std_logic_vector) return std_logic_vector is
    constant len : integer := in1'length;
    constant mid : integer := (in1'left + in1'right)/2 + 1;
    variable ret : std_logic_vector(in1'range);
  begin  -- swapper_works
    if len = 1 then
      ret := in1;
    else
      ret(in1'left downto mid) := swapper_works(in1(mid-1 downto in1'right));
      ret(mid-1 downto in1'right)  := swapper_works(in1(in1'left downto mid));
    end if;
    return ret;
  end swapper_works;

  signal sig1 : std_logic_vector(3 downto 0) := (others => '0');
  
begin  -- arch

  process (clk, in1)
    variable var1 : std_logic_vector(3 downto 0);
    variable var2 : std_logic_vector(3 downto 0);
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge
      sig1 <= in1;
      var1 := swapper_fails(sig1);
      var2 := swapper_works(sig1);
      out1 <= var1;
      out2 <= var2;
      if (var1 = var2) then
        mismatch <= '0';
      else
        mismatch <= '1';
      end if;
    end if;
  end process;

end arch;
