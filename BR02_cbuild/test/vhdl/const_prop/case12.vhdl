-- A fully specified case statement with variable temp 
-- def'ed in case items by procedure outputs. The statement RHS
-- should NOT convert from in1(temp) to in1(3).
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity case12 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end case12;


architecture case12 of case12 is

  procedure get_int_bid (val : inout integer) is
  begin  -- get_int_bid
    val := val + 1;
  end get_int_bid;

  procedure get_int_out (val_in : in integer; val_out : out integer) is
  begin  -- get_int_out
    val_out := val_in + 1;
  end get_int_out;


begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    case int_sel is
      when 0 =>
        get_int_bid(temp);
        out1 <= "110110" and in1;
      when 1 =>
        get_int_out(temp, temp);
        out1 <= not in1;
      when 2 =>
        out1 <= "011110" xor in1;
      when others =>
        out1 <= in1;
    end case;
    out2 <= in1(temp);
  end process;

end case12;
