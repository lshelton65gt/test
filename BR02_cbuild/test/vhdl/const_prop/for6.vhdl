-- Impure function should invalidate the constant temp2. The
-- loop does not need unrolling. This fails to populate since
-- function result is populated prior to function. Constant
-- propagation can't help in that case. Bug7384
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;


entity for6 is
  port (
    in1 : in std_logic_vector(7 downto 0);
    out1 : out std_logic_vector(63 downto 0);
    out2 : out std_logic
    );
end for6;


architecture for6 of for6 is
  shared variable temp2  : integer;

  impure function get_pattern (bound : in integer) return std_logic_vector is
    variable pattern : std_logic_vector(7 downto 0) := "11010100";
  begin  -- get_pattern
    temp2 := bound - 1;
    return pattern(temp2 downto 0);
  end get_pattern;

begin

  process ( in1 )
    variable bound : integer;
    variable zeros : std_logic_vector(7 downto 0);
    variable temp  : integer;
    variable temp1 : integer;
  begin
    bound := 8;
    zeros := (others => '0');
    temp  := 2;
    temp2 := 3;
    for i in bound-1 downto 0 loop
      temp := i*8;
      temp1 := bound - i;
      zeros(bound-1 downto 0) := get_pattern(bound);
      out1(temp+7 downto temp) <= zeros(bound-1 downto temp1) & in1(temp1-1 downto 0);
    end loop;  -- i
    out2 <= in1(temp2);                  -- temp2 is not constant.
  end process;

end for6;
