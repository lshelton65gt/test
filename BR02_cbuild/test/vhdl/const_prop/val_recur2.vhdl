-- Demonstrates another jaguar bug. The variable declared in
-- function recur loses is initial value when vhOpenDeclarativeRegion()
-- is called on the elaborated function.
library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity val_recur2 is
  port (
    in1    :  in      integer;
    out1   :  out     integer);
end val_recur2;

architecture arch of val_recur2 is

  function recur (in1 : integer) return integer is
    variable mult : integer := 8;
  begin  -- recur
    if in1 = 0 then
      return 0;
    else
      return recur(in1-1) + mult;
    end if;
  end recur;

begin  -- arch

  process (in1)
    variable cnt : integer := 4;
  begin  -- process
    cnt := 5;
    out1 <= in1 + recur(cnt);
  end process;
end arch;
