-- Tests if-then-else block statement. Variable temp ought
-- not be invalidated by the elsif block that def's it
-- since the elsif condition is never true. 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if3 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end if3;


architecture if3 of if3 is

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable temp1 : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    temp1 := 3;
    if (int_sel = 0) then
      out1 <= "110110" and in1;
    elsif ((temp1 + 2) = 4) then    -- never executed
      temp := 4;
      out1 <= not in1;
    elsif (int_sel = 2) then
      out1 <= "011110" xor in1;
    else
      out1 <= in1;
    end if;
    out2 <= in1(temp); -- temp is constant 3
  end process;

end if3;
