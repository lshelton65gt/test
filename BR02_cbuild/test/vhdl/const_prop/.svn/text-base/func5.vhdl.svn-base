-- Tests propagation of initial value invalidated by a def of
-- a non-constant variable.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity func5 is
  
  port (
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0));

end func5;

architecture func5 of func5 is

  function get_vec (in1 : std_logic_vector) return std_logic_vector is
    variable temp : integer := 2; -- inited to 2
    variable temp1 : integer := 0; -- inited to 0
    variable cpy : std_logic_vector(in1'range) := (others => '0');
  begin  -- get_vec
    temp := 3; -- initial value overridden by 3
    temp1 := 2; -- initial value overridden by 2
    temp := conv_integer(unsigned(in1(1 downto 0))); -- assigned value overridden by non-constant expression
    temp1 := temp; -- assigned value overridden by non-constant identifier
    cpy := in1;
    cpy(temp1) := in1(temp) xor '1'; -- temp is not a constant here.
    return cpy;
  end get_vec;
  
begin  -- func5

  out1 <= get_vec(in1);

end func5;
