-- bug7773 : Sanity check fails when loop range contains
-- function call in the right range bound.
library ieee;
use ieee.std_logic_1164.all;

entity for9 is
  
  port (
    in1  : in  std_logic_vector(39 downto 0);
    in2  : in  std_logic_vector(39 downto 0);
    in3  : in  std_logic_vector(39 downto 0);
    en   : in  std_logic_vector(26 downto 0);
    out1 : out std_logic_vector(47 downto 0));

end for9;


architecture arch of for9 is

  function fun (idx : integer) return integer is
  begin  -- fun
    return (idx+1)*16;
  end fun;
  
begin  -- arch

  proc1: process (in1, in2, in3, en)
    type vecary is array (2 downto 0) of std_logic_vector(39 downto 0);
    variable a0 : vecary := (others => (others => '0'));
    variable a1 : std_logic_vector(47 downto 0) := (others => '0');
    variable a2 : std_logic_vector(15 downto 0) := (others => '0');
    variable a3 : std_logic_vector(47 downto 0) := (others => '0');
    type enary is array (2 downto 0) of std_logic_vector(2 downto 0);
    type enaryary is array (2 downto 0) of enary;
    variable varen : enaryary := (others => (others => (others => '0')));
  begin  -- process proc1
    a0(0) := in1;
    a0(1) := in2;
    a0(2) := in3;

    for i in 0 to 2 loop
      for j in 0 to 2 loop
        for k in 0 to 2 loop
          varen(i)(j)(k) := en(i*9+j*3+k);
        end loop;  -- k
      end loop;  -- j
    end loop;  -- i
    
    for i in 0 to 2 loop
      for j in 0 to 2 loop -- loop over write ports
        a1 := "00000000" & a0(j) ; --AND mask; -- extend to 48 bits
        for k in 0 to (fun(j) + 15)/ 16 - 1 loop     ----> line no 252
          a2 := a1(k*16+15 downto k*16);
          if varen(j)(i)(k) = '1' then
            a3(i*16+15 downto i*16) := a3(i*16+15 downto i*16) or a2;       
          end if;
        end loop;
      end loop;
    end loop;
    out1 <= a3;
  end process proc1;

end arch;
