-- Reproduces a problem seen with testcase bug4825 with this smaller
-- testcase. The loop unrolling causes case select to be evaluated
-- to a constant. The select is a subrange integer and is sized to 5.
-- The choice for case item is a constant integer sized to 32. The
-- sizes don't match. To avoid this problem, all subrange integer constants are
-- being sized to 32 if their context size from LHS is not available.
-- This test fails now with simulation differences which need to be debugged.
-- I've verified this failure with 5319 and 5481 releases.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity subrangeint_const is
  port (clk : in std_logic;
        reset : in std_logic;
        di              : in    std_logic_vector(7 downto 0);
        dq              : in    std_logic_vector(7 downto 0);
        phase           : in    std_logic_vector(1 downto 0);
        sum             : out   std_logic_vector(15 downto 0));
end subrangeint_const;

architecture arch of subrangeint_const is
  subtype coefficient is integer range -10 to 10;
  type coeff_type  is array (NATURAL range <>) of coefficient;
  type  data_type  is array (NATURAL range <>) of std_logic_vector(7 downto 0);

  constant aw     : integer := 16;
  constant ntaps  : integer := 16; 
  constant cs     : integer := ntaps*2;

  function zxt (oper : std_logic_vector; n : integer) return std_logic_vector is
    variable outv: std_logic_vector(n-1 downto 0);
  begin
    outv                         := (others => '0');
    outv(oper'length-1 downto 0) := oper;
    return outv;
  end zxt;

  function asl (oper : std_logic_vector; n : integer)
    return std_logic_vector is
    variable outv: std_logic_vector(oper'length-1 downto 0);
  begin
    outv                     := (others => '0');
    outv(outv'high downto n) := oper(oper'high-n downto 0);
    return (outv);
  end asl;

  function csd (oper : std_logic_vector; coeff : coefficient)
    return std_logic_vector is
    variable zero, op, o1, o2: std_logic_vector(oper'length+2 downto 0);
    variable outv : std_logic_vector(aw*2-1 downto 0);
  begin
    op   := sxt(oper,op'length);
    zero := (others => '0');
    case coeff is                       -- evaluated to a constant of size 5 due to loop unrolling.
      when  10 => o2 :=     asl(op,3) ; o1 :=     asl(op,1);  -- select 10 is constant of size 32
      when   9 => o2 :=     asl(op,3) ; o1 :=         op;
      when   8 => o2 :=     asl(op,3) ; o1 :=       zero;
      when   7 => o2 :=     asl(op,3) ; o1 :=     not op;
      when   6 => o2 :=     asl(op,2) ; o1 :=     asl(op,1);
      when   5 => o2 :=     asl(op,2) ; o1 :=         op;
      when   4 => o2 :=     asl(op,2) ; o1 :=       zero;
      when   3 => o2 :=     asl(op,1) ; o1 :=         op;
      when   2 => o2 :=     asl(op,1) ; o1 :=       zero;
      when   1 => o2 :=         op    ; o1 :=       zero;
      when   0 => o2 :=       zero    ; o1 :=       zero;
      when - 1 => o2 := not(    op)   ; o1 :=       zero;
      when - 2 => o2 := not(asl(op,1)); o1 :=       zero;
      when - 3 => o2 := not(asl(op,1)); o1 :=     not(op);
      when - 4 => o2 := not(asl(op,2)); o1 :=       zero;
      when - 5 => o2 := not(asl(op,2)); o1 :=     not(op);
      when - 6 => o2 := not(asl(op,2)); o1 := not(asl(op,1));
      when - 7 => o2 := not(asl(op,3)); o1 :=         op;
      when - 8 => o2 := not(asl(op,3)); o1 :=       zero;
      when - 9 => o2 := not(asl(op,3)); o1 :=     not(op);
      when -10 => o2 := not(asl(op,3)); o1 := not(asl(op,1));
    end case;

    -- don't sign-extend, add a correction to the summation
    o1(o1'high) := not o1(o1'high);
    o2(o2'high) := not o2(o2'high);
    
    -- concatenate 2 CSD digits
    outv := zxt(o2,aw) & zxt(o1,aw);

    return (outv);
  end csd;

  constant coeffi : coeff_type (0 to ntaps-1) := (0,-6,- 1, 0,- 1,-6, 0, 3,-9,-1,10, 7,10,-1,-9, 3);
  constant coeffq : coeff_type (0 to ntaps-1) := (9, 1,-10,-7,-10, 1, 9,-3, 0, 6, 1, 0, 1, 6, 0,-3);

  signal yi :  data_type (ntaps-1 downto 0) := (others => (others => '0'));
  signal yq :  data_type (ntaps-1 downto 0) := (others => (others => '0'));

begin


  process (clk, reset, di, dq)
  begin  -- process
    if reset = '0' then                 -- asynchronous reset (active low)
      yi <= (others => (others => '0'));
      yq <= (others => (others => '0'));
    elsif clk'event and clk = '1' then  -- rising clock edge
      yi <= yi(ntaps-2 downto 0) & di;
      yq <= yq(ntaps-2 downto 0) & dq;
    end if;
  end process;
  
  mpys: process (phase, yi, yq)
    variable summation : std_logic_vector(  aw-1 downto 0) := (others => '0');
    variable operands  : std_logic_vector(2*aw-1 downto 0) := (others => '0');
    variable temp : std_logic_vector(3 downto 0);
  begin

    for k in 0 to ntaps-1 loop          -- loop unrolled.
    case phase  is
        when "00"   => operands := csd( yq(ntaps-k-1), coeffq(k));
        when "01"   => operands := csd( yi(ntaps-k-1), coeffi(k));
        when "10"   => operands := csd( yi(ntaps-k-1), coeffq(k));
        when "11"   => operands := csd( yq(ntaps-k-1), coeffi(k));
        when others => operands := (others => '0');
      end case;

      summation := summation + operands(2*aw-1 downto aw) + operands(aw-1 downto 0);
    end loop;
    sum <= summation;
  end process mpys;

end arch;
