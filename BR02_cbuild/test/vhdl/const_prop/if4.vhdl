-- Tests if-then-else block statement. This demonstrates a
-- weakness in constant propagation.  It fails to realize that
-- although temp1 is a variable, it's never modified after
-- initialization and is really a constant. The elsif
-- block that would never be executed is populated anyway,
-- resulting in temp variable being invalidated for propagation. Bug7383
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if4 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end if4;


architecture if4 of if4 is

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable temp1 : integer := 3;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel = 0) then
      out1 <= "110110" and in1;
    elsif ((temp1 + 2) = 4) then    -- never executed
      temp := 4;
      out1 <= not in1;
    elsif (int_sel = 2) then
      out1 <= "011110" xor in1;
    else
      out1 <= in1;
    end if;
    out2 <= in1(temp); -- temp is NOT constant
  end process;

end if4;
