-- A version of if9 test with no size available to the function.
-- It computes the size based on the length of input vector.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if10 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(5 downto 0);
    out1 : out std_logic_vector(5 downto 0)
    );
end if10;


architecture if10 of if10 is

  function recur (in1 : in std_logic_vector) return std_logic_vector is
    variable sz : integer := 0;
    variable retvec : std_logic_vector(in1'length-1 downto 0) := (others => '0');
  begin  -- recur
    sz := in1'length;
    if (sz = 2) then
      retvec := in1(sz-1 downto 0);
    else
      retvec := in1(sz-1 downto 0) xor ('0' & recur(in1(sz-2 downto 0)));
    end if;
    return retvec;
  end recur;

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel = 0) then
      temp := 4;
      out1 <= "10" & recur(in1(temp-1 downto 0));  -- temp is 4.
    elsif (int_sel = 1) then
      out1 <= "101" & recur(in1(temp-1 downto 0)); -- temp is 3.
    elsif (int_sel = 2) then
      out1 <= "110" & recur(in1(temp-1 downto 0)); -- temp is 3.
    else
      temp := 2;
      out1 <= "1011" & recur(in1(temp-1 downto 0)); -- temp is 2.
    end if;
  end process;

end if10;
