-- Tests if-then-else block statement. This demonstrates a
-- weakness in constant propagation.  It fails to realize that
-- although temp1 is a variable, it's never modified after
-- initialization and is really a constant. The elsif
-- block that would never be executed is populated anyway,
-- resulting in temp variable being invalidated for propagation.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if16 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(5 downto 0);
    out1 : out std_logic_vector(5 downto 0);
    out2 : out std_logic;
    out3 : out std_logic_vector(2 downto 0)
    );
end if16;


architecture if16 of if16 is

  function get_bit_set (in1 : std_logic_vector) return std_logic_vector is
  begin  -- get_bit_set
    return in1;
  end get_bit_set;

  shared variable temp2 : integer := 2;
  shared variable temp3 : integer := 0;

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable temp1 : integer := 3;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel = 0) then
      out1 <= "110110" and in1;
    elsif ((temp1 + 2) = 4) then    -- never executed
      temp := 4;
      out1 <= not in1;
    elsif (int_sel = 2) then
      out1 <= "011110" xor in1;
    else
      out1 <= in1;
    end if;
    out2 <= in1(temp); -- temp is NOT constant
    out3 <= get_bit_set(in1(temp2 downto temp3));
  end process;

end if16;
