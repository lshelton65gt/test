-- Tests if-the-else block statement. Variable temp ought
-- to be invalidated since it's def'ed in the if-the-else
-- block.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if2 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end if2;


architecture if2 of if2 is

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel = 0) then
      out1 <= "110110" and in1;
    elsif (int_sel = 1) then
      temp := 4;
      out1 <= not in1;
    elsif (int_sel = 2) then
      out1 <= "011110" xor in1;
    else
      out1 <= in1;
    end if;
    out2 <= in1(temp);
  end process;

end if2;
