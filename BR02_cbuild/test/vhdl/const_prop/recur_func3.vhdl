-- Similar to recur_func2.vhdl, this is a value based recursion
-- termination test. This test passes however since function
-- arguments are constants instead of variables.
library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity recur_func3 is
  port (
    in1    :  in      integer;
    out1   :  out     integer);
end recur_func3;

architecture arch of recur_func3 is

  function recur (in1 : integer) return integer is
    constant mult : integer := 8;
  begin  -- recur
    if in1 = 0 then
      return 0;
    else
      return recur(in1-1) + mult;
    end if;
  end recur;

begin  -- arch

  process (in1)
    constant cnt : integer := 4;
  begin  -- process
    out1 <= in1 + recur(cnt);
  end process;
end arch;
