-- October 2008
-- was extracted from customer test case
-- The problem was in the function sum_tree, which has double loop, with
-- if/else after it. Because the loops have function call inside. CBUILD
-- forces them to be unrolled. Then requires the if/else to be constant
-- propagated. Before my fix, the constant propagation was failing.
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

package func_pkg is
  function max (a : natural; b : natural) return natural;
  function extend(a : std_logic_vector; w : natural) return std_logic_vector;
  function log2 (N : natural) return positive;
  function sum (a : std_logic_vector; b : std_logic_vector) return std_logic_vector;
  function sum_tree (a : std_logic_vector; w : natural) return std_logic_vector;
end func_pkg;

package body func_pkg is
  function log2 (n: natural) RETURN positive is
    VARIABLE result : natural ;
    VARIABLE pwr2 : natural   ;
  BEGIN
    result := 0;
    pwr2 := 1;
    WHILE pwr2 < n LOOP
      result := result + 1;
      pwr2 := pwr2 * 2;
    END LOOP;
    RETURN result;
  END log2;
  
 
  function max(a : natural; b : natural) return natural is
  begin
    if a > b then
      return a;
    else
      return b;
    end if;
  end;
  
  function extend(a : std_logic_vector; w : natural) return std_logic_vector is
    constant a_len : natural := a'length;
    constant a_left : natural := a'left;
    constant a_right : natural := a'right;
    CONSTANT bits : natural := max(a'LENGTH, w);
    variable temp : std_logic_vector(a_right+bits-1 downto a_right) := (others => '0');
  begin
    temp(a_left downto a_right) := a;
    return temp;
  end;

  
  FUNCTION sum (a : std_logic_vector; b : std_logic_vector)
  RETURN std_logic_vector IS
      CONSTANT w : natural := max(a'LENGTH, b'LENGTH);
      VARIABLE result_int : unsigned(w-1 DOWNTO 0);
      VARIABLE result : std_logic_vector(w-1 DOWNTO 0);
  BEGIN
      result_int := unsigned(extend(a, w)) + unsigned(extend(b, w));
      result := std_logic_vector(result_int);
      RETURN result;
  END sum;
  
  FUNCTION sum_tree (a : std_logic_vector; w : natural)
    RETURN std_logic_vector IS
        VARIABLE tmp_result : std_logic_vector(a'length-1 DOWNTO 0);
        VARIABLE old_num_args, curr_num_args : natural;
        variable temp : natural;
	
    BEGIN
        tmp_result := a;
        old_num_args  := a'LENGTH / w;
        IF old_num_args > 1 THEN
            FOR i IN 1 TO log2(old_num_args) LOOP
                curr_num_args := old_num_args / 2;  
                FOR j IN 0 TO curr_num_args-1 LOOP
                    tmp_result((j+1)*w-1 DOWNTO j*w) :=
                       sum(tmp_result((2*j+1)*w-1 DOWNTO 2*j*w),
                            tmp_result((2*j+2)*w-1 DOWNTO (2*j+1)*w));
                END LOOP;

                IF old_num_args MOD 2 = 1 THEN
                    tmp_result((curr_num_args+1)*w-1 DOWNTO (curr_num_args)*w) :=
                        tmp_result(old_num_args*w-1 DOWNTO (old_num_args-1)*w);
                    old_num_args := curr_num_args+1;
                ELSE
                    old_num_args := curr_num_args;
                END IF;
            END LOOP;
        END IF;        
        RETURN tmp_result(w-1 DOWNTO 0);
    END sum_tree;

        
end func_pkg;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use work.func_pkg.all;

entity bug9122_01 is
  generic ( WIDTH :natural := 16);
    
  port (
    clk, rst : in  std_logic;
    in1      : in std_logic_vector(WIDTH-1 downto 0);
    out1     : out std_logic_vector(WIDTH-1 downto 0));

end bug9122_01;

architecture arch of bug9122_01 is

begin  -- arch

  p1: process (clk, rst)
  begin  -- process p1
    if rst = '0' then 
      out1 <= (others => '0');
    elsif clk'event and clk = '1' then 
   out1(3 downto 0) <= sum_tree(in1, 4);
    end if;
  end process p1;

end arch;
