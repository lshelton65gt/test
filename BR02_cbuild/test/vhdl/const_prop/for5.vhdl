-- The temp2 is a shared variable. Shared variables are not constant propagated
-- since they could be aliased to a net in the instantiated 
-- sub entity or the parent entity, and we don't resolve such
-- aliases at population time. The loop should be automatically unrolled
-- to enable successful population of impure function.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;


entity for5 is
  port (
    in1 : in std_logic_vector(7 downto 0);
    out1 : out std_logic_vector(63 downto 0);
    out2 : out std_logic
    );
end for5;


architecture for5 of for5 is
  shared variable temp2  : integer;

  impure function get_pattern (in1 : std_logic_vector) return std_logic_vector is
    variable in1_cpy : std_logic_vector(in1'range) := (others => '0');
  begin  -- get_pattern
    temp2 := 0;
    in1_cpy := in1;
    in1_cpy(temp2) := '1';
    return in1_cpy;
  end get_pattern;

begin

  process ( in1 )
    variable bound : integer;
    variable zeros : std_logic_vector(7 downto 0);
    variable temp  : integer;
    variable temp1 : integer;
  begin
    bound := 8;
    zeros := (others => '0');
    temp  := 2;
    temp2 := 3;
    for i in bound-1 downto 0 loop
      temp := i*8;
      temp1 := bound - i;
      out1(temp+7 downto temp) <= zeros(bound-1 downto temp1) & get_pattern(in1(temp1-1 downto 0));
    end loop;  -- i
    out2 <= in1(temp2);                  -- temp2 is not constant.
  end process;

end for5;
