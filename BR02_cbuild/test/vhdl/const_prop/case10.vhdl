-- A version of case9.vhdl where the variable temp is
-- def'ed conditionally in one case item with a different
-- value than it's current value. The RHS of assign statement after
-- case statement should NOT convert from in1(temp) to in1(3), since
-- the temp value is NOT always the same.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity case10 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end case10;


architecture case10 of case10 is
begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    case int_sel is
      when 0 =>
        if (in1(temp) = '1') then
          temp := 4;
        end if;
        out1 <= "110111" and in1;
      when 1 =>
        out1 <= not in1;
      when 2 =>
        out1 <= "011110" xor in1;
      when others =>
        out1 <= in1;
    end case;
    out2 <= in1(temp);
  end process;

end case10;
