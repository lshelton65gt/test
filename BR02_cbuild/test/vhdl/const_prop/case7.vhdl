-- A fully specified case statement with variable temp discreetly
-- def'ed in one case item by an impure function call. The RHS of
-- assign statement after case statement should NOT convert
-- from in1(temp) to in1(3), even though the impure function
-- def's it to the same value. This is because the temp is
-- a shared variable. Shared variables are not constant propagated
-- since they could be aliased to a net in the instantiated 
-- sub entity or the parent entity, and we don't resolve such
-- aliases at population time.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity case7 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end case7;


architecture case7 of case7 is
  shared variable temp : integer := 2;

  impure function get_pattern (in1 : std_logic_vector) return std_logic_vector is
    variable in1_cpy : std_logic_vector(in1'range) := (others => '0');
  begin  -- get_pattern
    temp := 3;
    in1_cpy := in1;
    in1_cpy(temp) := '1';
    return in1_cpy;
  end get_pattern;

begin

  process ( sel, in1 )
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    case int_sel is
      when 0 =>
        out1 <= get_pattern(in1) and in1;
      when 1 =>
        out1 <= not in1;
      when 2 =>
        out1 <= "011110" xor in1;
      when others =>
        out1 <= in1;
    end case;
    out2 <= in1(temp);
  end process;

end case7;
