-- In this case, cbuild fails to populate because it cannot
-- figure out the size of the return for function get_vec()
-- because the size depends on temp, which is a variable
-- declared inside the body of the function. This makes
-- constant propagation irrelevant. See bug 8523.
library ieee;
use ieee.std_logic_1164.all;

entity func_retsize_2 is

 port (
   in1  : in  std_logic_vector(3 downto 0);
   out1 : out std_logic_vector(3 downto 0));

end func_retsize_2;

architecture func_retsize_2 of func_retsize_2 is

 function get_vec (in1 : std_logic_vector) return std_logic_vector is
   variable temp : integer;
 begin  -- get_vec
   temp := in1'length;
   return in1(temp-1 downto 0);  -- Cannot determine size statically
 end get_vec;

begin  -- func_retsize_2

 process (in1)
 begin  -- process
   out1 <= get_vec(in1);
 end process;

end func_retsize_2;

