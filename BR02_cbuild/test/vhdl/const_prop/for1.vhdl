-- Tests a recursive function call from a for loop where the arguments to the
-- function vary from iteration to iteration. This wasn't populatable prior to
-- local constant propagation. This for loop is automatically unrolled.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;


entity for1 is
  port (
    in1 : in std_logic_vector(5 downto 0);
    out1 : out std_logic_vector(35 downto 0)
    );
end for1;


architecture for1 of for1 is

  function recur (in1 : in std_logic_vector; sz : in integer) return std_logic_vector is
    variable retvec : std_logic_vector(sz-1 downto 0) := (others => '0');
  begin  -- recur
    if (sz = 2) then
      retvec := not in1(sz-1 downto 0);
    else
      retvec := in1(sz-1 downto 0) xor ('0' & recur(in1(sz-2 downto 0), sz-1));
  end if;
  return retvec;
  end recur;

begin

  process ( in1 )
    variable bound : integer;
    variable temp : integer := 2;
    variable zeros : std_logic_vector(5 downto 0);
  begin
    bound := 6;
    zeros := (others => '0');
    for i in bound-1 downto 1 loop
      temp := bound - i;
      out1(i*6+5 downto i*6) <= (zeros(bound-1 downto bound-i+1) & recur(in1(bound-i downto 0), temp+1));
    end loop;  -- i
    out1(bound-1 downto 0) <= in1(bound-1 downto 0);  -- bound not def'ed in loop.
  end process;

end for1;
