-- A fully specified case statement with variable temp's
-- constant value propagating into case items. Although
-- the temp is def'ed in some case items, the constant
-- value in parent context should be available to each
-- case item, unless it's def'ed prior to use in the
-- case item context.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity case6 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(0 to 5);
    out1 : out std_logic_vector(0 to 5);
    out2 : out std_logic
    );
end case6;


architecture case6 of case6 is

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    case int_sel is
      when 0 =>
        out1 <= "110110" and in1;
        out2 <= in1(temp);
        temp := 4;
      when 1 =>
        out1 <= not in1;
        temp := 5;
        out2 <= in1(temp);
      when 2 =>
        out1 <= "011110" xor in1;
        out2 <= in1(temp);
      when others =>
        out1 <= in1;
        temp := 1;
        out2 <= in1(temp);
    end case;
  end process;

end case6;
