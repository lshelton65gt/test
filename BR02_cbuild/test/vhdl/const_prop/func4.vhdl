-- Tests propagation of initial value computed by a function with static
-- argumnts across a bunch of assign statements.
library ieee;
use ieee.std_logic_1164.all;

entity func4 is
  
  port (
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0));

end func4;

architecture func4 of func4 is

  function get_num (in1 : integer) return integer is
  begin
    return in1 + 1;
  end get_num;

  function get_vec (in1 : std_logic_vector) return std_logic_vector is
    variable temp : integer := get_num(1);
    variable temp1 : integer := 0;
    variable temp2 : integer := 0;
    variable cpy : std_logic_vector(in1'range) := (others => '0');
  begin  -- get_vec
    temp1 := temp;
    temp2 := temp1;
    cpy := in1;
    cpy(temp2) := in1(temp1) xor '1';
    return cpy;
  end get_vec;
  
begin  -- func4

  out1 <= get_vec(in1);

end func4;
