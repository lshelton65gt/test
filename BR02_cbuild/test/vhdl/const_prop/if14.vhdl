-- The temp should be invalidated for propagation. However after
-- it has been restored to original value it ought to be made valid again.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity if14 is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(5 downto 0);
    out1 : out std_logic_vector(5 downto 0);
    out2 : out std_logic
    );
end if14;


architecture if14 of if14 is

begin

  process ( sel, in1 )
    variable temp : integer := 2;
    variable int_sel : integer := 0;
    variable temp1 : std_logic_vector(5 downto 0);
  begin
    int_sel := conv_integer(unsigned(sel));
    temp := 3;
    if (int_sel = 0) then
      out1 <= "101011" and in1;
    elsif (int_sel = 1) then
      temp1 := not in1;
      if (temp1(temp) = '1') then
        temp := 4;                      -- def with value different from current
                                        -- propagated value from parent context
        temp1(temp) := '1';
      else
        temp1(temp) := '1';
      end if;
      temp := 3;                        -- value restore to original.
      out1 <= temp1;
    elsif (int_sel = 2) then
      out1 <= "11001" & in1(temp);
    else
      out1 <= in1;
    end if;
    out2 <= in1(temp);                  -- still a constant.
  end process;

end if14;
