library ieee;
use ieee.std_logic_1164.all;

entity bug5110 is
  port (
    d : in  std_logic;
    q : out std_logic);
end bug5110;

architecture arch of bug5110 is
  type zwpmsktype is array (0 to 3) of std_logic_vector( 1 downto 0);
  constant ALLUNTOUCHED : zwpmsktype := (others => (others => '0'));
  constant ALLTOUCHED : zwpmsktype := (others => (others => '1'));
  signal tmpzwpmsk : zwpmsktype := ALLTOUCHED;
begin
  p1: process (d, tmpzwpmsk)
  begin
    if tmpzwpmsk /= ALLUNTOUCHED then
      q <= d;
    end if;
  end process p1;
end arch;
