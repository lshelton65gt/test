-- This testcase simplifies the memory usage in Matrox Phoenix rtchz.vhdl.  Its
-- primary purpose is to test the correct handling and population of a nested
-- others statement initializing an entire memory to '0'.

library ieee;
use ieee.std_logic_1164.all;

package pack is
  type array_of_4_std_logic_vector4 is array (3 downto 0) of std_logic_vector(3 downto 0);
  type array_of_8_array_of_4_std_logic_vector4 is array (7 downto 0) of array_of_4_std_logic_vector4;
end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity matrox5 is
  port (
    clk  : in  std_logic;
    data : in  std_logic;
    outp : out std_logic);
end matrox5;

architecture arch of matrox5 is
   signal hzdwenarraya   : array_of_8_array_of_4_std_logic_vector4;
begin

process (clk)
  begin
    if clk'event and clk = '1' then
      hzdwenarraya <=(others=>(others=>(others=>'0')));      
    end if;
  end process;  
  process (hzdwenarraya, data)
    variable v : std_logic;
  begin
    v := '1';
    for i in hzdwenarraya'RANGE loop
      for j in hzdwenarraya(i)'RANGE loop
        for k in hzdwenarraya(i)(j)'RANGE loop
          if hzdwenarraya(i)(j)(k) /= data then
            v := '0';
          end if;
        end loop;  -- k
      end loop;  -- j
    end loop;  -- i
    outp <= v;
  end process;

end arch;
