entity conc_sig_asgn is
  port (in1, in2,in3 : in bit; out1: out bit);
end conc_sig_asgn;

architecture arch of conc_sig_asgn is
begin
  out1 <= in1 xor in2 xor in3;
end;
