-- last mod: Wed Oct 18 15:49:36 2006
-- filename: test/vhdl/lang_memory/memory3.vhd
-- Description:  This test duplicates the problem seen in bug6687, where there
-- is an instance of a module that has a multi d memory as an output port, but
-- there is no actual connection to that port.  This resulted in a segfault.

library ieee;
use ieee.std_logic_1164.all;

package pack is
  type recarray is array(0 to 1) of std_logic_vector(7 downto 0);
  type mem2 is array (1 downto 0) of std_logic_vector (1 downto 0);
  type TMEMORY is array (1 downto 0) of mem2;

end pack;


-- bottom
library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bot is
  port (
    bp1, bp2 : in     std_logic_vector(7 downto 0);
    brec     : buffer recarray);
end bot;

architecture barch of bot is
begin
  brec(0) <= bp1;
  brec(1) <= bp2;
end barch;


-- middle

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity mid is
  port ( 
    mp1, mp2 : in     std_logic_vector(7 downto 0);
    mem1     : buffer TMEMORY;          --leave this disconnected
    mrec     : buffer recarray);
end mid;

architecture march of mid is
  component bot
    port (
      bp1, bp2           : in     std_logic_vector(7 downto 0);
      brec               : buffer recarray);
  end component;
  
begin
  b1: bot port map ( bp1 => mp1, bp2 => mp2, brec => mrec );

  mem1 <= (others => (others => (others => '0')));

end march;



-- top


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity memory3a is
  port (
    p1, p2 : in     std_logic_vector(7 downto 0);
    outp   : buffer std_logic_vector(15 downto 0));
end memory3a;

architecture arch of memory3a is

  signal toprec : recarray;

  component mid
    port (
      mp1, mp2 : in     std_logic_vector(7 downto 0);
  --    mem1 : buffer TMEMORY;          -- disconnected
      mrec     : buffer recarray);
  end component;
  
begin
  m1 : mid port map (mp1  => p1, mp2  => p2, mrec => toprec);
  outp <= toprec(0) & toprec(1);
end arch;
