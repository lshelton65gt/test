-- This testcase is abstracted from Matrox's te_tm_mapaluctl.vhdl.  It tests
-- assigning a 2-d memory from a 2-d slice of a 3-d memory.
library ieee;
use ieee.std_logic_1164.all;

package pack is
  type ALUCONFIGVAL is (ANONE, A2D, A2DL, A3DL, A2DP, ACUBIC, ABMP, A3D, A3DP, A3DCB, 
                        A2DCBL, A3DCBP);
  subtype ALUCOUNTER is integer range 0 to 4;

  type MAPALUSEQOP is (SINT, TINT, RINT, QINT, SOP1, TOP1, ROP1, STP,
                       SOP2, TOP2, ROP2, NONE);
  type MAPALUSEQCYCLE is array (0 to 2) of MAPALUSEQOP;
  
  type MAPALUSEQ2 is array (0 to 1) of MAPALUSEQCYCLE;
  type MAPALUSEQ3 is array (0 to 2) of MAPALUSEQCYCLE;
  type MAPALUSEQ4 is array (0 to 3) of MAPALUSEQCYCLE;
  type MAPALUSEQ5 is array (0 to 4) of MAPALUSEQCYCLE;

  constant CBMPSEQ   : MAPALUSEQ4 := ( (SOP1 , TOP1 , SINT),
                                       (SOP1 , TOP1 , TINT),
                                       (SINT , TOP1 , TINT),
                                       (SOP1 , TINT , SINT));
  constant C2DPSEQ   : MAPALUSEQ4 := ( (SINT , STP  , QINT),
                                       (STP  , TINT , QINT),
                                       (SINT , TINT , STP),
                                       (SINT , TINT , QINT));
  constant CCUBICSEQ : MAPALUSEQ5 := ( (SOP1 , TOP1 , SINT),
                                       (SINT , TINT , RINT),
                                       (SOP1 , TOP1 , TINT),
                                       (SINT , TINT , RINT),
                                       (SOP1 , TOP1 , RINT));
  constant C3DLSEQ   : MAPALUSEQ2 := ( (SINT , TINT , RINT),
                                       (SOP1 , TOP1 , QINT));
  constant C2DCBLSEQ : MAPALUSEQ2 := ( (SINT , TINT , RINT),
                                       (SOP1 , TOP1 , QINT));
  constant C3DPSEQ   : MAPALUSEQ2 := ( (SINT , STP  , QINT),
                                       (ROP1 , TINT , RINT));
  constant C3DCBSEQ  : MAPALUSEQ2 := ( (SINT , TINT , RINT),
                                       (SOP1 , TOP1 , ROP1));
  constant C3DCBPSEQ : MAPALUSEQ3 := ( (SINT , TINT , ROP1),
                                       (STP  , QINT , RINT),
                                       (SOP2 , TOP2 , ROP2));

end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity matrox10 is
  port (
    in1        : in integer;
    alucnt_int : in integer;
    outp0: out std_logic_vector(3 downto 0);
    outp1: out std_logic_vector(3 downto 0);
    outp2: out std_logic_vector(3 downto 0));
end matrox10;

architecture arch of matrox10 is
  signal mapaluseq        : MAPALUSEQCYCLE;
  signal aluconfig        : ALUCONFIGVAL;
  signal alucnt           : ALUCOUNTER;
  signal inmod : integer range 0 to 11;
  
  function MAPALUSEQOP_to_slv (constant inp : MAPALUSEQOP) return std_logic_vector is
    variable retval : std_logic_vector(3 downto 0);
  begin
    case inp is
      when  SINT => retval := "0000";
      when  TINT => retval := "0001";
      when  RINT => retval := "0010";
      when  QINT => retval := "0011";
      when  SOP1 => retval := "0100";
      when  TOP1 => retval := "0101";
      when  ROP1 => retval := "0110";
      when  STP  => retval := "0111";
      when  SOP2 => retval := "1000";
      when  TOP2 => retval := "1001";
      when  ROP2 => retval := "1010";
      when  NONE => retval := "1011";
      when others => retval := "1111";
    end case;
    return retval;
  end MAPALUSEQOP_to_slv;
  
begin

  -- map from integer to the Matrox range
  alucnt <= alucnt_int mod 2;
  -- map from integer to the Matrox range
  inmod <= in1 mod 12;
  
  -- map from SLV to the Matrox data type
  aluconfig <= ANONE when inmod = 0 else
               a2d when inmod = 1 else
                a2dl when inmod = 2 else
                a3dl when inmod = 3 else
                a2dp when inmod = 4 else
                acubic when inmod = 5 else
                abmp when inmod = 6 else
                a3d when inmod = 7 else
                a3dp when inmod = 8 else
                a3dcb when inmod = 9 else
                a2dcbl when inmod = 10 else
                a3dcbp;

  mapaluseq <= (SINT, TINT, RINT) when (aluconfig = ANONE or aluconfig = A2D or 
                                        aluconfig = A3D) else
               (SINT, TINT, QINT) when (aluconfig = A2DL) else
               C3DLSEQ(alucnt)    when (aluconfig = A3DL) else
               CBMPSEQ(alucnt)    when (aluconfig = ABMP) else
               C2DPSEQ(alucnt)    when (aluconfig = A2DP) else
               CCUBICSEQ(alucnt)  when (aluconfig = ACUBIC) else
               C3DPSEQ(alucnt)    when (aluconfig = A3DP) else
               C3DCBSEQ(alucnt)   when (aluconfig = A3DCB) else
               C2DCBLSEQ(alucnt)  when (aluconfig = A2DCBL) else
               C3DCBPSEQ(alucnt);

  -- map from Matrox data type to SLV
  outp0 <= MAPALUSEQOP_to_slv(mapaluseq(0));
  outp1 <= MAPALUSEQOP_to_slv(mapaluseq(1));
  outp2 <= MAPALUSEQOP_to_slv(mapaluseq(2));
end arch;
