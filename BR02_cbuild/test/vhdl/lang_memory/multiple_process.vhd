entity multiple_process is
  port (d,clk,rst : in bit; q: out bit);
end;

architecture arch of multiple_process is
signal bool_data1, bool_data2 : bit;
begin
  p1: process (clk, rst)
  begin
    if (rst = '1') then
      bool_data1 <= '0';
    elsif (clk'event and clk = '1') then
      bool_data1 <= d;
    end if;
  end process p1;
  -- Process with arbitary if/elsif cond
  p2: process (bool_data1, clk)
  begin
    if ((bool_data1 = '1') and (rst = '0')) then
      q <= bool_data1;
    elsif (clk = '1') then
      q <= bool_data1;
    else
      q <= '0';
    end if;
  end process p2;
end;
