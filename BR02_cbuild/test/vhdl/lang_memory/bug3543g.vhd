library ieee;
use ieee.std_logic_1164.all;


package pack is
	type memory is array ( natural range 0 to 1 ) of std_logic_vector(0 to 3);
	subtype memarray_2 is memory;
	subtype memarray_1 is memarray_2;
	type memarray is array ( natural range <>) of memarray_1;
	constant m1 : memarray_1 := ( others => (others => '0'));
	constant m2 : memarray_2 := ( others => (others => '1'));
	constant mem2 : memarray(0 to 1 ) := ( 0 => m1 , 1 =>m2 );
end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bug3543g is
	port(address : in std_logic; data : out std_logic_vector(0 to 3));
end;

architecture arch of bug3543g is 
signal int_address : natural := 0;
begin
  int_address <= 1 when address = '1' else 0;
  process (int_address)
  begin
    data <= mem2(int_address)(int_address);
  end process;
end;

