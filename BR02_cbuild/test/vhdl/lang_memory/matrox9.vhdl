-- Test assigning a 2-d slice of a 3-d memory from a 2-d memory.
library ieee;
use ieee.std_logic_1164.all;

package pack is
  type ALUCONFIGVAL is (ANONE, A2D, A2DL, A3DL, A2DP, ACUBIC, ABMP, A3D, A3DP, A3DCB, 
                        A2DCBL, A3DCBP);
  subtype ALUCOUNTER is integer range 0 to 4;

  type MAPALUSEQOP is (SINT, TINT, RINT, QINT, SOP1, TOP1, ROP1, STP,
                       SOP2, TOP2, ROP2, NONE);
  type MAPALUSEQCYCLE is array (0 to 2) of MAPALUSEQOP;
  
  type MAPALUSEQ2 is array (0 to 1) of MAPALUSEQCYCLE;
  type MAPALUSEQ3 is array (0 to 2) of MAPALUSEQCYCLE;
  type MAPALUSEQ4 is array (0 to 3) of MAPALUSEQCYCLE;
  type MAPALUSEQ5 is array (0 to 4) of MAPALUSEQCYCLE;

end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity matrox9 is
  port (
    alucnt : in integer range 0 to 3;
    outp0: out std_logic_vector(3 downto 0);
    outp1: out std_logic_vector(3 downto 0);
    outp2: out std_logic_vector(3 downto 0));
end matrox9;

architecture arch of matrox9 is
  signal mapaluseq : MAPALUSEQCYCLE := (TOP2, ROP2, NONE);
begin

  process (alucnt, mapaluseq)
    function MAPALUSEQOP_to_slv (constant inp : MAPALUSEQOP) return std_logic_vector is
      variable retval : std_logic_vector(3 downto 0);
    begin
      case inp is
        when  SINT => retval := "0000";
        when  TINT => retval := "0001";
        when  RINT => retval := "0010";
        when  QINT => retval := "0011";
        when  SOP1 => retval := "0100";
        when  TOP1 => retval := "0101";
        when  ROP1 => retval := "0110";
        when  STP  => retval := "0111";
        when  SOP2 => retval := "1000";
        when  TOP2 => retval := "1001";
        when  ROP2 => retval := "1010";
        when  NONE => retval := "1011";
        when others => retval := "1111";
      end case;
      return retval;
    end MAPALUSEQOP_to_slv;
    
    variable CBMPSEQ : MAPALUSEQ4 := ((SINT , TINT , RINT),
                                      (QINT , SOP1 , TOP1),
                                      (ROP1 , STP , SOP2),
                                      (TOP2 , ROP2 , NONE));

  begin
    CBMPSEQ(alucnt) := mapaluseq;
    
    outp0 <= MAPALUSEQOP_to_slv(CBMPSEQ(alucnt)(0));
    outp1 <= MAPALUSEQOP_to_slv(CBMPSEQ(alucnt)(1));
    outp2 <= MAPALUSEQOP_to_slv(CBMPSEQ(alucnt)(2));
    
  end process;

end arch;
