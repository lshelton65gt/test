-- This test tests an aggregate assigned to a memory
entity bug4194_bug4 is
  port (
    clk : in bit;
    rst : in bit;
    din : in  bit_vector(15 downto 0);
    dout : out bit_vector(15 downto 0));
end bug4194_bug4;

architecture rtl of bug4194_bug4 is

  type mem_type is array (5 downto 0) of bit_vector(15 downto 0);
  signal mem : mem_type;

begin  -- rtl

  process (clk, rst)
  begin  -- process
    if rst = '0' then
      mem <= (others => (others => '0'));
      dout <= "0000000000000000";
    elsif clk'event and clk = '1' then
      mem <= ( 5 => mem(4), 0 => din, 3 => mem(2),
               2 => mem(1), 1 => mem(0), 4 => mem(3) );
      dout <= mem(5);
    end if;
  end process;
end rtl;

