LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.std_logic_arith.all;

ENTITY bug5450 IS
   GENERIC
   (
        A_SZ            : integer := 9;
        D_SZ            : integer := 128;
        NUM_LOC         : integer := 512
   );
	PORT
	(
 		ACLK	: IN     std_ulogic;
		AME	: IN     std_ulogic;
		ARE	: IN     std_ulogic;
		AWWE	: IN     std_ulogic;
		ABE	: IN     std_logic_vector((D_SZ/8)-1 downto 0);
 		ARADR  	: IN     std_logic_vector(A_SZ-1     downto 0);
 		AWADR  	: IN     std_logic_vector(A_SZ-1     downto 0);
		ADI	: IN     std_logic_vector(D_SZ-1     downto 0);	
		ADO	: BUFFER std_logic_vector(D_SZ-1     downto 0);

		BCLK	: IN     std_ulogic;
		BME	: IN     std_ulogic;
		BRE	: IN     std_ulogic;
		BWWE	: IN     std_ulogic;
		BBE	: IN     std_logic_vector((D_SZ/8)-1 downto 0);
 		BRADR   : IN     std_logic_vector(A_SZ-1     downto 0);
 		BWADR   : IN     std_logic_vector(A_SZ-1     downto 0);
		BDI	: IN     std_logic_vector(D_SZ-1     downto 0);	
		BDO	: BUFFER std_logic_vector(D_SZ-1     downto 0)
	
	); -- end PORT
END bug5450;

ARCHITECTURE  bug5450_ARCH OF bug5450 IS

TYPE   MEM IS ARRAY (NUM_LOC-1 downto 0) of std_logic_vector(ADI'range);
SIGNAL memcore : MEM;

BEGIN

process(ACLK)

VARIABLE iadr              : integer;

begin 

-- PORT A
  if(ACLK'event and ACLK='1')then
      if(AME='1' and AWWE='1')then
         iadr := conv_integer(unsigned(AWADR));
         FOR lb IN 0 to (ADI'LENGTH-1)/8 LOOP
            IF (ABE(lb)='1') THEN
              memcore(iadr)(lb*8+7 downto lb*8) <= ADI(lb*8+7 downto lb*8);
            END IF;
         END LOOP;
      end if;
      if(AME='1' and ARE='1')then
         iadr := conv_integer(unsigned(ARADR));
         ADO <= memcore(iadr);
      end if;
  end if;  
end process;

process(BCLK)

VARIABLE iadr              : integer;

begin 

-- PORT B
  if(BCLK'event and BCLK='1')then
      if(BME='1' and BWWE='1')then
         iadr := conv_integer(unsigned(BWADR));
         FOR lb IN 0 to (BDI'LENGTH-1)/8 LOOP
            IF (BBE(lb)='1') THEN
              memcore(iadr)(lb*8+7 downto lb*8) <= BDI(lb*8+7 downto lb*8);
            END IF;
         END LOOP;
      end if;
      if(BME='1' and BRE='1')then
         iadr := conv_integer(unsigned(BRADR));
         BDO <= memcore(iadr);
      end if;
  end if;  
end process;

END  bug5450_ARCH;


