library ieee;
use ieee.std_logic_1164.all;

entity bug5985 is
  port (
    db_clk      : in  std_logic;
    filter_mode : in  std_logic_vector(1 downto 0);
    y_4x4_set1  : out natural;
    y_4x4_set2  : out natural;
    y_4x4_set3  : out natural;
    y_4x4_set4  : out natural;
    y_4x4_set5  : out natural);
end;

architecture arch of bug5985 is
   constant num_0 : natural := 0;
   constant num_1 : natural := 1;
   constant num_2 : natural := 2;
   constant num_3 : natural := 3;
   constant num_4 : natural := 4;
   constant num_5 : natural := 5;
   constant num_6 : natural := 6;
   constant num_7 : natural := 7;
   constant num_9 : natural := 9;
   constant num_10  : natural := 10;
   constant num_11 : natural := 11;
   constant num_12 : natural := 12;
   constant num_14 : natural := 14;
   constant num_15 : natural := 15;
   constant num_16 : natural := 16;
   constant num_19 : natural := 19;
   constant num_20 : natural := 20;
   constant num_21 : natural := 21;
   constant num_22 : natural := 22;
   constant num_23 : natural := 23;
   constant num_24 : natural := 24;

   TYPE     Y_4X4_VECTOR       IS ARRAY (INTEGER RANGE <>) OF NATURAL;
   CONSTANT UF_Y_4X4_SET : Y_4X4_VECTOR (0 to 4) :=  (NUM_6, NUM_11, NUM_16, NUM_21, NUM_0);
   CONSTANT xx_Y_4X4_SET : Y_4X4_VECTOR (0 to 4) :=  (NUM_0, NUM_1,  NUM_2,  NUM_3,  NUM_4);
   CONSTANT yy_Y_4X4_SET : Y_4X4_VECTOR (0 to 4) :=  (NUM_24,NUM_24, NUM_24, NUM_24, NUM_24);
   signal y_4x4_set : y_4x4_vector(0 to 4);
begin

  y_4x4_set1 <= y_4x4_set(0);
  y_4x4_set2 <= y_4x4_set(1);
  y_4x4_set3 <= y_4x4_set(2);
  y_4x4_set4 <= y_4x4_set(3);
  y_4x4_set5 <= y_4x4_set(4);

  p1: process
  begin
    wait on db_clk until db_clk'event and db_clk = '1';
    case filter_mode is
      when "00" => y_4x4_set <= UF_Y_4X4_SET;
      when "01" => y_4x4_set <= xx_Y_4X4_SET;
      when "10" => y_4x4_set <= yy_Y_4X4_SET;
      when others => y_4x4_set <= (num_0, num_0, num_0, num_0, num_0);
    end case;
  end process p1;

end arch;
