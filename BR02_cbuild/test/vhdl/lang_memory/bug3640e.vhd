library ieee;
use ieee.std_logic_1164.all;

entity bug3640e is
  port (
    enp          : in     std_logic;
    inp          : in     std_logic_vector(1 downto 0);
    out1, out2   : out std_logic
    );
end ;

architecture arch of bug3640e is
begin
  (out1 , out2 ) <= inp when enp = '1' else "ZZ";
end;

