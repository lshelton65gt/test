entity cond_sig_asgn is
  port (in1, in2,in3 : in bit; out1: out bit);
end cond_sig_asgn;

architecture arch of cond_sig_asgn is
begin
  out1 <= in1 xor in2 when in1 = in3 else
          in2 xor in3 when in1 = in2;
end;
