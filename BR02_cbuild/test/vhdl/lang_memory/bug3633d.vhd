library ieee;
use ieee.std_logic_1164.all;

entity bug3633d is
  port( in1, in2, ena : in std_logic; 
        out1 : out std_logic);
end;

architecture arch of bug3633d is
begin
  out1 <= in1 and in2 when (ena = '1') else 'Z';
end;
