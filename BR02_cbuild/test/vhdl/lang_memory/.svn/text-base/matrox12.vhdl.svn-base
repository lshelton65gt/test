-- This testcase is abstracted from Matrox's te_tm_mapaluctl.vhdl. It tests
-- assigning a 2-d memory from a 2-d slice of a 3-d memory.
library ieee;
use ieee.std_logic_1164.all;

package pack is
  constant MCDATASZ : integer := 256;
  constant BITWIDTH : integer := 8; -- NUMBITS
  constant PIPEWIDTH : integer := 8; -- NUMBYTES
  constant NUMPIPES : integer := 4; -- NUMPIPES
  type bytebank is array (PIPEWIDTH-1 downto 0) of std_logic_vector(BITWIDTH-1 downto 0);
  type databank is array (NUMPIPES-1 downto 0) of bytebank;
end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity matrox12 is
  port (
    indata : in std_logic_vector(255 downto 0);
    outdata : out std_logic_vector(255 downto 0)
    );
end matrox12;

architecture arch of matrox12 is
  signal indatamod : databank;
  
begin
  XMAPDATA : process (indata)
    variable lowerrange : integer range 0 to 255;
    variable upperrange : integer range 0 to 255;
  begin
    for pipenum in 0 to NUMPIPES-1 loop
      for bytenum in 0 to PIPEWIDTH-1 loop 
        lowerrange := bytenum*32+pipenum*BITWIDTH;
        upperrange := bytenum*32+(pipenum+1)*BITWIDTH-1;
        indatamod(pipenum)(bytenum) <= indata(upperrange downto lowerrange);
      end loop;
    end loop;
  end process;

  outdata <=
    indatamod(3)(7) & indatamod(3)(6) & indatamod(3)(5) & indatamod(3)(4) & indatamod(3)(3) & indatamod(3)(2) & indatamod(3)(1) & indatamod(3)(0) & 
    indatamod(2)(7) & indatamod(2)(6) & indatamod(2)(5) & indatamod(2)(4) & indatamod(2)(3) & indatamod(2)(2) & indatamod(2)(1) & indatamod(2)(0) & 
    indatamod(1)(7) & indatamod(1)(6) & indatamod(1)(5) & indatamod(1)(4) & indatamod(1)(3) & indatamod(1)(2) & indatamod(1)(1) & indatamod(1)(0) & 
    indatamod(0)(7) & indatamod(0)(6) & indatamod(0)(5) & indatamod(0)(4) & indatamod(0)(3) & indatamod(0)(2) & indatamod(0)(1) & indatamod(0)(0);
  
end arch;
