library ieee;
use ieee.std_logic_1164.all;

package pkg is

  subtype rsdatatype is std_logic_vector(127 downto 0);      
  type rsdatatbltype is array (natural range <>) of rsdatatype;
  
end pkg;


library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bottom1 is

  generic (nbrd : integer := 1);
  
  port (bin    : in  rsdatatbltype(nbrd-1 downto 0);
        clkbuf : in std_logic;
        bout   : inout rsdatatbltype(nbrd-1 downto 0));

end bottom1;

architecture bottomarch of bottom1 is

  signal datasig   : rsdatatbltype(nbrd-1 downto 0);
  signal sourcesig : rsdatatbltype(nbrd-1 downto 0);
  
begin  -- bottomarch

  sourcesig <= bin;
  
  gen: for i in nbrd-1 to 0 generate    
    data: process (clkbuf)
    begin  -- process data
      if clkbuf'event and clkbuf = '1' then  -- rising clock edge
        datasig(i) <= sourcesig(i);
      end if;
    end process data;
  end generate gen;

  bout <= datasig;

end bottomarch;

library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bug11624_bidir_2 is

  generic (nbrd : integer := 1);
  
  
  port (tin1  : in rsdatatbltype(nbrd-1 downto 0);
        clk   : in std_logic;
        tout1 : inout rsdatatbltype(nbrd-1 downto 0));

end bug11624_bidir_2;

architecture toparch of bug11624_bidir_2 is

begin  -- toparch

  -- This is the case of a mem select being assigned to another mem select when
  -- both memories have only one row.
  u2 : entity work.bottom1
    generic map (nbrd => 1)
    port map (bin(0)  => tin1(0),
              clkbuf  => clk,
              bout(0) => tout1(0));
  
end toparch;
