library ieee;
use ieee.std_logic_1164.all;

package pkg is

  subtype rsdatatype is std_logic_vector(127 downto 0);      
  type rsdatatbltype is array (natural range <>) of rsdatatype;
  
end pkg;

library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bottom2 is

  generic (nbrd : integer := 1);
  
  port (bin1   : in  rsdatatype;
        bin2   : in rsdatatype;
        clkbuf : in std_logic;
        bout1  : inout rsdatatype;
        bout2  : inout rsdatatype);

end bottom2;

architecture bottom2arch of bottom2 is

  signal datasig1   : rsdatatype;
  signal datasig2   : rsdatatype;
  signal sourcesig1 : rsdatatype;
  signal sourcesig2 : rsdatatype;
  
begin  -- bottom2arch

  sourcesig1 <= bin1;
  sourcesig2 <= bin2;
  
  data: process (clkbuf)
  begin  -- process data
    if clkbuf'event and clkbuf = '1' then  -- rising clock edge
      datasig1 <= sourcesig1;
      datasig2 <= sourcesig2;
    end if;
  end process data;

  bout1 <= datasig1;
  bout2 <= datasig2;

end bottom2arch;

library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bug11624_bidir_5 is

  generic (nbrd : integer := 1);
  
  
  port (tin4  : in rsdatatbltype(1 downto 0);
        clk   : in std_logic;
        tout4 : inout rsdatatbltype(1 downto 0));

end bug11624_bidir_5;

architecture toparch of bug11624_bidir_5 is

begin  -- toparch

  -- This is the case of a vector formal being assigned to an actual which is a memory select
  u5 : entity work.bottom2
    port map (bin1   => tin4(0),
              bin2   => tin4(1),
              clkbuf => clk,
              bout1  => tout4(0),
              bout2  => tout4(1));
    
  
end toparch;
