-- test 4-D arrays
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity fourdim1 is
  port (
    din   : in  std_logic_vector(0 to 37);
    dout1 : out std_logic_vector(32 downto 0);
    dout2 : out std_logic_vector(31 downto 0));
end fourdim1;

architecture arch of fourdim1 is
  type twodee is array (0 to 1) of std_logic_vector(100 to 107);
  type threedee is array (16 downto 15) of twodee;
  -- fourdee is 2x2x2x8 bits = 64 bits total
  type fourdee is array (5 downto 2) of threedee;

  constant table : fourdee := (
    (("00000000", "00000001"), ("00000010", "00000011")),
    (("00000100", "00000101"), ("00000110", "00000111")),
    (("00001000", "00001001"), ("00001010", "00001011")),
    (("00001100", "00001101"), ("00001110", "00001111"))
    );

begin
  approx: process (din)
    variable v4 : fourdee;
    variable v3 : threedee;
    variable v2 : twodee;
    variable v1 : std_logic_vector(7 downto 0);
  begin

    -- read and write entire memory
    v4 := table;
    -- write 1 bit
    v4(2 + conv_integer(unsigned(din(0 to 1))))(15 + conv_integer(din(1)))(conv_integer(din(2)))(100 + conv_integer(unsigned(din(3 to 5)))) := din(6);
    -- write 1 word (1-d vector)
    v4(2 + conv_integer(unsigned(din(7 to 8))))(15 + conv_integer(din(8)))(conv_integer(din(9))) := din(10 to 17);
    -- write a 2-d slice
    v2 := (din(18 to 25), din(26 to 33));
    v4(2+conv_integer(unsigned(din(34 to 35))))(15 + conv_integer(din(35))) := v2;
    v4(2+conv_integer(unsigned(din(36 to 37))))(15 + conv_integer(din(37))) := (v2(1), v2(0));
    -- write a 3-d slice
    v3 := ((v2(1), v2(0)), (din(22 to 29), din(19 to 26)));
    v4(2 + conv_integer(din(23))) := v3;

    -- drive undriven bits to '0'; any subsequent assigns will override
    dout1 <= (others => '0');
    -- read one bit
    dout1(32) <= v4(2 + conv_integer(unsigned(din(1 to 2))))(15 + conv_integer(din(2)))(conv_integer(din(3)))(100 + conv_integer(unsigned(din(4 to 6))));
    -- read one word (1-d vector)
    dout1(31 downto 24) <= v4(2 + conv_integer(unsigned(din(18 to 19))))(15 + conv_integer(din(19)))(conv_integer(din(20)));
    -- read a 2-d slice
    v2 := v4(2 + conv_integer(unsigned(din(21 to 22))))(15 + conv_integer(din(22)));
    dout1(23 downto 8) <= v2(1) & v2(0);

    -- read a 3-d slice
    v3 := v4(2 + conv_integer(unsigned(din(7 to 8))));
    dout2 <= v3(16)(0) & v3(15)(1) & v3(16)(1) & v3(15)(0);
    
  end process approx;
  
end arch;
