library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity reg_memsel is
  
  port (
    clk  : in  std_logic;
    addr : in  std_logic_vector(3 downto 0);
    in1  : in  std_logic_vector(15 downto 0);
    out1 : out std_logic_vector(15 downto 0));

end reg_memsel;

architecture arch of reg_memsel is

  constant width : integer := 16;
  constant groups : integer := 4;
  constant group_width : integer := 4;
  constant addr_width : integer := 4;

  type mem_typ is array (natural range <>) of std_logic_vector(width-1 downto 0);

  signal var1 : mem_typ(width-1 downto 0) := (others => (others => '0'));
  signal var2 : mem_typ(width-1 downto 0) := (others => (others => '0'));
  shared variable temp_var : integer := 0;
  
begin  -- arch

  process (clk, in1, addr)
    variable int_addr : integer := 0;
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge
      int_addr := to_integer(unsigned(addr));
      var1(int_addr) <= in1;

      for i in 0 to groups-1 loop
        temp_var := i;
        var2(temp_var*group_width+group_width-1 downto temp_var*group_width)
          <= var1(temp_var*group_width+group_width-1 downto temp_var*group_width);
      end loop;  -- i

      out1 <= var2(int_addr);
    end if;
  end process;

end arch;
