-- November 2008
-- this file tests assignment of a slice of array of arrays of a vector.
library ieee;
use ieee.std_logic_1164.all;

package defs is

  type array2X2 is array (integer range <>) of std_logic_vector(3 downto 0);
  type arraysOf2x2 is array (integer range <>) of array2x2(1 downto 0);

end defs;

library ieee;
use ieee.std_logic_1164.all;
use work.defs.all;

entity bug8799_02 is
  
  port (
    clk : in  std_logic;
    in1 : in  std_logic_vector(3 downto 0);
    in2 : in  std_logic_vector(3 downto 0);
    in3 : in  std_logic_vector(3 downto 0);
    in4 : in  std_logic_vector(3 downto 0);
    out1: out std_logic_vector(3 downto 0);
    out2: out std_logic_vector(3 downto 0);
    out3: out std_logic_vector(3 downto 0);
    out4: out std_logic_vector(3 downto 0)
    );

end bug8799_02;
  
architecture arch_1 of bug8799_02 is
  signal t1 : arraysOf2x2(5 downto 2) := (others => (others => (others => '0')));
  signal t2 : arraysOf2x2(5 downto 2) := (others => (others => (others => '0')));
begin  -- arch_1

  t1(5)(1) <= in1;
  t1(5)(0) <= in2;
  t1(4)(1) <= in3;
  t1(4)(0) <= in4;
  t1(3)(1) <= not in1;
  t1(3)(0) <= not in2;
  t1(2)(1) <= not in3;
  t1(2)(0) <= not in4;

  p1: process (clk)
  begin
    if clk'event and clk = '1' then  
      t2(5 downto 2) <= t1;                         -- this line is tested
    end if;
  end process p1;

  out1 <= t2(5)(1) or t2(5)(0);
  out2 <= t2(4)(1) or t2(4)(0);
  out3 <= t2(3)(1) or t2(3)(0);
  out4 <= t2(2)(1) or t2(2)(0);

end arch_1;
