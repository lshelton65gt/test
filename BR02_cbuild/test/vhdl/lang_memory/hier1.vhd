-- this testcase has a two word array passed between blocks

library ieee;
use ieee.std_logic_1164.all;

package pack is
  type recarray is array(0 to 1) of std_logic_vector(7 downto 0);
end pack;


-- bottom
library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bot is
  port (
    bp1, bp2 : in     std_logic_vector(7 downto 0);
    brec     : buffer recarray);
end bot;

architecture barch of bot is
begin
  brec(0) <= bp1;
  brec(1) <= bp2;
end barch;


-- middle

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity mid is
  port (
    mp1, mp2 : in     std_logic_vector(7 downto 0);
    mrec     : buffer recarray);
end mid;

architecture march of mid is
  component bot
    port (
      bp1, bp2           : in     std_logic_vector(7 downto 0);
      brec               : buffer recarray);
  end component;
  
begin
  b1: bot port map ( bp1 => mp1, bp2 => mp2, brec => mrec );
end march;



-- top


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity hier1 is
  port (
    p1, p2 : in     std_logic_vector(7 downto 0);
    outp   : buffer std_logic_vector(15 downto 0));
end hier1;

architecture arch of hier1 is

  signal toprec : recarray;

  component mid
    port (
      mp1, mp2 : in     std_logic_vector(7 downto 0);
      mrec     : buffer recarray);
  end component;
  
begin
  m1 : mid port map (mp1  => p1, mp2  => p2, mrec => toprec);
  outp <= toprec(0) & toprec(1);
end arch;
