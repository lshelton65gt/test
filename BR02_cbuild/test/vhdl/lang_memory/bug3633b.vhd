
entity bug3633b is
  port( ena1,ena2 : in bit;  a,b:in character; out1:out character);
end;

architecture arch of bug3633b is
begin
  out1 <= b when ena1 = '0' else a when ena2 /= '1' else 'Z' ;
  out1 <= a when ena2 = '1' else b when ena1 /= '0' else 'Z' ;
end;

