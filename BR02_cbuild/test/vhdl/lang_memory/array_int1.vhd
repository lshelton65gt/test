library ieee;
use ieee.std_logic_1164.all;
entity array_int1 is
        port(
                 in1  : in  std_logic;
                 int1 : in integer;
                 int2 : in integer;
                 out1 : out integer
                 );
end array_int1;

architecture array_int1 of array_int1 is
begin
  process (in1, int1, int2)
   type int_arr is array(2 downto 1) of integer ;
   variable my_int_arr : int_arr;
        begin
            my_int_arr(1) := int1;
            my_int_arr(2) := int2;
        if in1 = '1' then  
           out1 <= my_int_arr(2);
        else
            out1 <= my_int_arr(1);
        end if;
        end process;

end array_int1;

