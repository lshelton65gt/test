-- July 2007
-- This file tests a concatenation of a vector with an array.
-- 
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
 
entity bug7520_01 is
  port (
  in0 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
  in1 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
  in2 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
  in3 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
  in4 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
  out0 : out STD_LOGIC_VECTOR(3 DOWNTO 0);
  out1 : out STD_LOGIC_VECTOR(3 DOWNTO 0);
  out2 : out STD_LOGIC_VECTOR(3 DOWNTO 0);
  out3 : out STD_LOGIC_VECTOR(3 DOWNTO 0);
  out4 : out STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
  
end bug7520_01;

architecture rtl of bug7520_01 is
 
TYPE MYBUS IS ARRAY (INTEGER RANGE <>) OF STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal A              :  MYBUS(5 DOWNTO 4);
 signal b              :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 SIGNAL C              :  MYBUS(3 DOWNTO 2);
 SIGNAL D              :  MYBUS(7 DOWNTO 3);
 
begin
  A(5) <= in4;
  A(4) <= in3;
  b <= in2;
  C(3) <= in1;
  C(2) <= in0;
  
  D <= A & b & C; -- this line is tested      
  
  out4 <= D(7);
  out3 <= D(6);
  out2 <= D(5);  
  out1 <= D(4);
  out0 <= D(3);
    
end rtl;
