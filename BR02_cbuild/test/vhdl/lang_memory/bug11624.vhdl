library ieee;
use ieee.std_logic_1164.all;

package pkg is

  subtype rsdatatype is std_logic_vector(127 downto 0);      
  type rsdatatbltype is array (natural range <>) of rsdatatype;
  
end pkg;


library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bottom1 is

  generic (nbrd : integer := 1);
  
  port (bin    : in  rsdatatbltype(nbrd-1 downto 0);
        clkbuf : in std_logic;
        bout   : buffer rsdatatbltype(nbrd-1 downto 0));

end bottom1;

architecture bottomarch of bottom1 is

  signal datasig   : rsdatatbltype(nbrd-1 downto 0);
  signal sourcesig : rsdatatbltype(nbrd-1 downto 0);
  
begin  -- bottomarch

  sourcesig <= bin;
  
  gen: for i in nbrd-1 to 0 generate    
    data: process (clkbuf)
    begin  -- process data
      if clkbuf'event and clkbuf = '1' then  -- rising clock edge
        datasig(i) <= sourcesig(i);
      end if;
    end process data;
  end generate gen;

  bout <= datasig;

end bottomarch;

library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bottom2 is

  generic (nbrd : integer := 1);
  
  port (bin1   : in  rsdatatype;
        bin2   : in rsdatatype;
        clkbuf : in std_logic;
        bout1  : buffer rsdatatype;
        bout2  : buffer rsdatatype);

end bottom2;

architecture bottom2arch of bottom2 is

  signal datasig1   : rsdatatype;
  signal datasig2   : rsdatatype;
  signal sourcesig1 : rsdatatype;
  signal sourcesig2 : rsdatatype;
  
begin  -- bottom2arch

  sourcesig1 <= bin1;
  sourcesig2 <= bin2;
  
  data: process (clkbuf)
  begin  -- process data
    if clkbuf'event and clkbuf = '1' then  -- rising clock edge
      datasig1 <= sourcesig1;
      datasig2 <= sourcesig2;
    end if;
  end process data;

  bout1 <= datasig1;
  bout2 <= datasig2;

end bottom2arch;

library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bottom3 is

  generic (nbrd : integer := 1);
  
  port (bin  : in  rsdatatbltype(nbrd-1 downto 0);
        clkbuf : in std_logic;
        bout : buffer rsdatatbltype(nbrd-1 downto 0));

end bottom3;

architecture bottom3arch of bottom3 is

  signal datasig  : rsdatatbltype(nbrd-1 downto 0);
  signal sourcesig : rsdatatbltype(nbrd-1 downto 0);

  
begin  -- bottom3arch

  sourcesig <= bin;
  
  data: process (clkbuf)
  begin  -- process data
    if clkbuf'event and clkbuf = '1' then  -- rising clock edge
      datasig <= sourcesig;
    end if;
  end process data;

  bout <= datasig;

end bottom3arch;

library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bug11624 is

  generic (nbrd : integer := 1);
  
  
  port (tin0  : in rsdatatype;
        tin1  : in rsdatatbltype(nbrd-1 downto 0);
        tin2  : in rsdatatbltype(1 downto 0);
        tin3  : in rsdatatbltype(1 downto 0);
        tin4  : in rsdatatbltype(1 downto 0);
        clk   : in std_logic;
        tout0 : buffer rsdatatype;
        tout1 : buffer rsdatatbltype(nbrd-1 downto 0);
        tout2 : buffer rsdatatbltype(1 downto 0);
        tout3 : buffer rsdatatbltype(1 downto 0);
        tout4 : buffer rsdatatbltype(1 downto 0));

end bug11624;

architecture toparch of bug11624 is

begin  -- toparch

  -- This is the original problem. A mem select of a memory that only has one
  -- row is being assigned to a vector.
  u1 : entity work.bottom1
    generic map (nbrd => 1)
    port map (bin(0)  => tin0,
              clkbuf  => clk,
              bout(0) => tout0);

  -- This is the case of a mem select being assigned to another mem select when
  -- both memories have only one row.
  u2 : entity work.bottom1
    generic map (nbrd => 1)
    port map (bin(0)  => tin1(0),
              clkbuf  => clk,
              bout(0) => tout1(0));
  
  -- This is the case of mem selects being assigned to mem selects when the
  -- memory has more than one row.
  u3 : entity work.bottom3
    generic map (nbrd => 2)
    port map (bin(0)  => tin2(0),
              bin(1)  => tin2(1),
              clkbuf  => clk,
              bout(0) => tout2(0),
              bout(1) => tout2(1));

  -- This is the case of one memory being assigned to another memory
  u4 : entity work.bottom3
    generic map (nbrd => 2)
    port map (bin     => tin3,
              clkbuf  => clk,
              bout    => tout3);

  -- This is the case of a vector formal being assigned to an actual which is a memory select
  u5 : entity work.bottom2
    port map (bin1   => tin4(0),
              bin2   => tin4(1),
              clkbuf => clk,
              bout1  => tout4(0),
              bout2  => tout4(1));
    
  
end toparch;
