library ieee;
use ieee.std_logic_1164.all;
--use IEEE.std_logic_arith.all;

entity bug5110_2 is
  port (
    rtclkbuf : in std_logic;
    czkillpix : in std_logic_vector(3 downto 0);
    czdr, pfzwpexthd : in std_logic; 
    d : in  std_logic;
    q : out std_logic);
end bug5110_2;

architecture arch of bug5110_2 is
  type zwpmsktype is array (0 to 3) of std_logic_vector( 1 downto 0);
  constant ALLUNTOUCHED : zwpmsktype := (others => (others => '0'));
  constant ALLTOUCHED : zwpmsktype := (others => (others => '1'));
  type smpftype is (IDLE, CMD, NORMDATA,
                    NORMAA, NEWAA, FLUSHAA, FREEAA, KILLAA,
                    SENDFLFREEDCMD, WAITFLFREEDACK);

  signal tmpzwpmsk : zwpmsktype := ALLTOUCHED;
  signal rsaafb, rssmallprimopten : std_logic := '1';
  signal wkhd : std_logic := '0';
  signal nsmpfstate        : smpftype := NORMDATA;
  
begin
  p1: process (rtclkbuf)
  begin
    if czdr = '0' then
      q <= '0';
    elsif (rtclkbuf'event and rtclkbuf = '1') then
-- From /cust/data/matrox/phoenix/10_7_2006/vhdl/rtupreffrag.vhdl:827
      if (czdr = '1' and (pfzwpexthd or wkhd) = '0' and rsaafb = '1' and
          nsmpfstate = NORMDATA and rssmallprimopten = '1' and
          (tmpzwpmsk /= ALLUNTOUCHED or czkillpix /= X"0")) then
        q <= d;
      end if;
    end if;
  end process p1;
end arch;
