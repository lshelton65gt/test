-- This testcase fails.  We need to resynthesize this into
--    always @(*)
--      if (grd)
--        out1 = ena1? (a & b) : 1'bz;
-- But instead we synthesize this into
--   assign out1 = ((!grd) || (!ena1)) ? (grd ? (a & b) : out1) : 1'bz;
--
-- and we get a mismatch in xz-diff:
--   line 39, col 7:  00110010  ::  00110011 
--
-- I haven't analyzed this enough yet, and LFTristate does not look
-- at blocking assigns.
--
-- For the moment, I am committing the gold file as a .sim.gold and
-- removing the .xz.gold.  When this testcase is functional again,
-- the gold file should be renamed back

library ieee;
use ieee.std_logic_1164.all;

entity bug3633c is
  port(grd, ena1,ena2 : in std_logic;  a,b:in std_logic; out1:out std_logic);
end;

architecture arch of bug3633c is
begin
  a1:block (grd = '1') is
  begin
  out1 <= GUARDED a and b when ena1 = '0' else 'Z' ;
  end block;
end;

