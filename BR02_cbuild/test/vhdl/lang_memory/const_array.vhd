
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity const_array is
   port( input : in std_logic_vector(0 to 2);
         output1 : out std_logic_vector(5 downto 0);
         output2 : out std_logic_vector(5 downto 0);
         output3 : out std_logic_vector(5 downto 0));
end ;

architecture const_array of const_array is

 subtype ROM_DELTA_WORD is std_logic_vector(5 downto 0);
 type DELTA_TABLE_TYPE is array (0 to 7) of ROM_DELTA_WORD;
 constant DELTA_TABLE: DELTA_TABLE_TYPE := DELTA_TABLE_TYPE'(
 "000111",
 "000011",
 "000110",
 "001100",
 "011000",
 "000100",
 "001000",
 "011000"
 );
 constant D_VAL : ROM_DELTA_WORD := "000110";

 type reverse_TABLE_TYPE is array (7 downto 0) of ROM_DELTA_WORD;
 constant reverse_TABLE: reverse_TABLE_TYPE := reverse_TABLE_TYPE'(
 "010011",
 "010011",
 "010110",
 "011100",
 "011010",
 "000110",
 "001010",
 "011011"
 );

begin
   process(input)
   begin
       output1 <= D_VAL;
       output2 <= DELTA_TABLE(conv_integer(unsigned(input)));
       output3 <= reverse_TABLE(conv_integer(unsigned(input)));
   end process;
end ;

