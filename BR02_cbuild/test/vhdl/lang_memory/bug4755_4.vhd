-- This test tests assigning a memory to a lefthand-concat in a process.  This
-- is not allowed.
entity bug4755_4 is
  port (
    clk : in bit;
    rst : in bit;
    din : in  bit_vector(7 downto 0);
    dout : out bit_vector(5 downto 0));
end bug4755_4;

architecture rtl of bug4755_4 is

  type mem_type is array (integer range <>) of bit_vector(7 downto 0);
  signal mem : mem_type(4 downto 0);
  signal bigmem : mem_type(5 downto 0);

begin

  fillmem: process (clk, rst)
  begin
    if rst = '0' then
      mem <= (others => "00000000");
    elsif clk'event and clk = '1' then
      mem <= mem(3 downto 0) & din;
    end if;
  end process fillmem;

  memcopy: process (clk, rst)
  begin
    if clk'event and clk = '1' then
      bigmem(2 downto 1) & bigmem(5 downto 3) := mem;
      bigmem(0) := din ror 4;
    end if;
  end process;

  outdrive: dout <= bigmem(0)(7) & bigmem(1)(7) & bigmem(2)(7) &
                    bigmem(3)(7) & bigmem(4)(7) & bigmem(5)(7);
  
end rtl;

