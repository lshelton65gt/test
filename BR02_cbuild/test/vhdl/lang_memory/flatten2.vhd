-- this testcase was extracted from a customer test, is used to test for the flattening of small memories


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity sub_one is
  port (
    count   : in     integer;
    qa      : buffer std_logic_vector(3 downto 0);
    qb      : buffer std_logic_vector(3 downto 0)
    );
end sub_one;

architecture functional of sub_one is

begin  -- functional

  process (count)
  begin  -- process

    qa <= CONV_STD_LOGIC_VECTOR(count,4);
    qb <= CONV_STD_LOGIC_VECTOR(count,4);
  end process;

end functional;


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity flatten2 is
  port (
    out1        : out    std_logic_vector(3 downto 0)
    );
end flatten2;


architecture functional of flatten2 is
component sub_one is
  port (
    count   : in     integer;
    qa      : buffer std_logic_vector(3 downto 0);
    qb      : buffer std_logic_vector(3 downto 0)
    );
end component sub_one;
  
   type ARRAYSMALL    is array (0 to 1) of std_logic_vector( 3 downto 0);

   signal count                    : integer;
   signal mem2                     : ARRAYSMALL;
   signal mem3                     : ARRAYSMALL;
  
begin

      xSUB_ONE: sub_one
         port map (
            count      => count,
            qa         => mem3(0),
            qb         => mem3(1)
         );

  
    process(mem3, mem2)
      begin
         count <= 1;

         mem2 <= mem3;

         out1 <= mem2(1);               -- a live output
      end process;


end functional;

