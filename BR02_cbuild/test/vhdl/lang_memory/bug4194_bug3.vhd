-- This test tests an concat of a slice and an element assigned to a slice
-- This test uses concurrent assignments
entity bug4194_bug3 is
  port (
    clk : in bit;
    rst : in bit;
    din : in  bit_vector(15 downto 0);
    dout : out bit_vector(15 downto 0));
end bug4194_bug3;

architecture rtl of bug4194_bug3 is

  type mem_type is array (6 downto 0) of bit_vector(15 downto 0);
  signal mem : mem_type;

begin  -- rtl
  mem(5 downto 0) <= mem(4 downto 0) & din;
  dout <= mem(5);
end rtl;

