-- This test is not strictly a matrox test but it is derived from matrox1.vhdl.
-- This test is not simulated correctly with either Aldec or MTI.  Carbon
-- agrees with NCVHDL that the value fed into the memory should feed all the
-- way through to the output.  Aldec never does this, and MTI does only
-- sometimes.
entity matrox3 is
  port (
    a: in integer;
    idx1, idx2: in integer;
    outint: out integer);
end matrox3;

architecture arch of matrox3 is
  type t_data  is array (0 to 1) of integer;
  type t_lut   is array (0 to 31) of t_data;

  -------------------------------------------------------------------------
  -- The following is the LUT that contains the values of (2**13) * (1/x) and
  -- of (2**13)*((1/x)-(1/y))
  -- where x = 1+q/31, y = 1+((q+1)/31), q = 0,1,...31.
  -------------------------------------------------------------------------
  signal c_rcp_lut : t_lut := (
    (0, 1), (2, 3), (4, 5), (6, 7),
    (8, 9), (10, 11), (12, 13), (14, 15),
    (16, 17), (18, 19), (20, 21), (22, 23),
    (24, 25), (26, 27), (28, 29), (30, 31),
    (32, 33), (34, 35), (36, 37), (38, 39),
    (40, 41),  (42, 43),  (44, 45),  (46, 47),
    (48, 49),  (50, 51),  (52, 53),  (54, 55),
    (56, 57),  (58, 59),  (60, 61),  (62, 63)
    );

  signal idx1mod, idx2mod : natural;
begin
  idx1mod <= idx1 mod 32;
  idx2mod <= idx2 mod 2;
  c_rcp_lut(idx1mod)(idx2mod) <= a;
  outint <= c_rcp_lut(idx1mod)(idx2mod);
  
end arch;
