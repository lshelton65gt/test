library ieee;
use ieee.std_logic_1164.all;

entity bug3640c is
  port (
    enp          : in     std_logic;
    inp          : in     std_logic_vector(5 downto 0);
    outp         : out std_logic_vector(5 downto 0)
    );
end ;

architecture arch of bug3640c is
begin
  outp(5 downto 0) <= inp when enp = '1' else "ZZZZZZ";
end;

