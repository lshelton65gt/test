-- test constant 2-D array of integers declared in a package
-- abstracted from matrox Phoenix ce_fpurcp_carbon.vhdl
-- total data size: 2 x 32 x 32 bits: (0:1)(0:31)(31:0)
-- should get resynthesized into 64 x 32 bits (63:0)(31:0)
-- This tests reading from individual words and writing to a 2-D slice of a memory.
package pack is

  type t_data  is array (0 to 1) of integer;
  type t_lut   is array (0 to 31) of t_data;

  -------------------------------------------------------------------------
  -- The following is the LUT that contains the values of (2**13) * (1/x) and
  -- of (2**13)*((1/x)-(1/y))
  -- where x = 1+q/31, y = 1+((q+1)/31), q = 0,1,...31.
  -------------------------------------------------------------------------
  constant c_rcp_lut : t_lut := (
    (8191, 249), (7942, 233), (7709, 220), (7489, 209),
    (7281, 197), (7084, 187), (6898, 178), (6721, 168),
    (6553, 160), (6393, 152), (6241, 146), (6096, 139),
    (5957, 133), (5825, 127), (5698, 121), (5577, 116),
    (5461, 112), (5349, 107), (5242, 103), (5139, 98),
    (5041, 96),  (4945, 91),  (4854, 88),  (4766, 86),
    (4681, 83),  (4599, 80),  (4519, 76),  (4443, 75),
    (4369, 72),  (4297, 69),  (4228, 68),  (4161, 66)
    );
  

end pack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.pack.all;

entity matrox4 is
  port (
    a: in std_logic_vector(32 downto 1);
    pvalue0  : out std_logic_vector(13 downto 1);
    pdeltay0 : out std_logic_vector(8 downto 1));  
end matrox4;

architecture arch of matrox4 is
begin
  approx: process (a)
    variable value0      : std_logic_vector( 13 downto 1 ); -- base value used in linear interpolation
    variable deltay0     : std_logic_vector( 8 downto 1 );  -- delta y used in linear interpolation
  begin
    value0  := conv_std_logic_vector(c_rcp_lut( conv_integer(
      unsigned(a(23 downto 19))))(0), value0'left);
    deltay0 := conv_std_logic_vector(c_rcp_lut(conv_integer(
      unsigned(a(23 downto 19))))(1),deltay0'left);
    
    -- drive the buffered values onto the output ports
    pvalue0 <= value0;
    pdeltay0 <= deltay0;
    
  end process approx;
  
end arch;
