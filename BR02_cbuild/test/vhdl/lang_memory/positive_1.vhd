
entity positive_1 is
  port (in1, in2 : in positive ; out1: out positive);
end;

architecture arch of positive_1 is
begin
  out1 <= in1 + in2 ;
end;
