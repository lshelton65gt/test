-- filename: test/vhdl/lang_memory/bug8345_04.vhd
-- Description:  This test checks for proper operation of the creation of an
-- individual memselRvalue.  it was inspired by the test bug8345_03.vhd


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug8345_04 is
  port (
    clock    : in  std_logic;
    int1 : in natural range 0 to 3;
    in1 : in  std_logic;
    in2 : in  std_logic_vector(1 downto 0);
    out1  : out std_logic_vector(4 downto 0));
end;

architecture arch of bug8345_04 is
begin
  main: process (clock)
    TYPE   natural_array_type2    is array (NATURAL range <>) of natural range 0 to 3; 
    TYPE   natural_array_type10    is array (NATURAL range <>) of natural range 0 to 1023; 
    variable array_r : natural_array_type2(2 downto 0) := (others => 3);
    variable array_l : natural_array_type10(2 downto 0) := (others => 1000);
    VARIABLE count_r         : unsigned(1 DOWNTO 0);



  begin
    if clock'event and clock = '1' then 
      if in1 = '1' then 
        array_r(0)  :=  0;
        array_r(1) := int1;
        array_l(1) := array_r(0);
        array_l(2) := array_r(1);
        -- note that the previous two lines CANNOT be rewritten as:
        --   array_l(2 downto 1) := array_r(1 downto 0);
        -- because that is invalid VHDL (because the types do not match).  It
        -- is good that vhdl catches this because we populate:
        --   array_l(1) := array_r(0);
        -- as
        --   main.array_l[1] = {{30{1'b0}},main.array_r[0]}[9:0]; 
        -- Note the necessary padding on the RHS so that you can assign ranged
        -- integers to other ranged integers that might have a different
        -- range/size.
        -- If we followed the same pattern (to pad all ranged integers) then
        -- when we- built a concat for the rhs of:
        --   array_l(2 downto 1) := array_r(1 downto 0);
        -- we might try to create
        --   {{{30{1'b0}},main.array_r[1]},{{30{1'b0}},main.array_r[0]}}[9:0]; 
        -- which would be wrong.  We did this at one time and is the source of
        -- bug 8345.
        
        out1   <= STD_LOGIC_VECTOR(to_unsigned(array_l(2), 5)); 
      end if;
    end if;
  end process;
end;

