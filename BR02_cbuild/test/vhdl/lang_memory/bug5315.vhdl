
library ieee; 
USE IEEE.STD_LOGIC_1164.ALL;

package pkgx is

  type a18x32 is array (17 downto 0) of std_logic_vector(31 downto 0);       

end pkgx;
library ieee; 

USE IEEE.STD_LOGIC_1164.ALL;

USE WORK.pkgx.ALL;


entity bug5315 is
  port (
    H : in  std_logic_vector (5 downto 0);
    M0 : out std_logic_vector(31 downto 0);
    M1 : out std_logic_vector(31 downto 0);
    M2 : out std_logic_vector(31 downto 0);
    M3 : out std_logic_vector(31 downto 0);
    M4 : out std_logic_vector(31 downto 0);
    M5 : out std_logic_vector(31 downto 0);
    M6 : out std_logic_vector(31 downto 0);
    M7 : out std_logic_vector(31 downto 0);
    M8 : out std_logic_vector(31 downto 0);
    M9 : out std_logic_vector(31 downto 0);
    M10 : out std_logic_vector(31 downto 0);
    M11 : out std_logic_vector(31 downto 0);
    M12 : out std_logic_vector(31 downto 0);
    M13 : out std_logic_vector(31 downto 0);
    M14 : out std_logic_vector(31 downto 0);
    M15 : out std_logic_vector(31 downto 0);
    M16 : out std_logic_vector(31 downto 0);
    M17 : out std_logic_vector(31 downto 0)
    );
end;

architecture arch of bug5315 is
  TYPE IA18 IS ARRAY (0 TO 17) OF INTEGER;
  CONSTANT X                                                       
    : IA18 := (0, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8);
  CONSTANT Y                                                       
    : IA18 := (3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9, 3, 2, 3);

  signal M : a18x32;
begin
  
  p : PROCESS(H)
  begin
    FOR i IN 0 TO 17 LOOP
        M(X(i))(Y(i) + 5 DOWNTO Y(i)) <= H;
    end loop;
  end process;

  M0 <= M(0);
  M1 <= M(1);
  M2 <= M(2);
  M3 <= M(3);
  M4 <= M(4);
  M5 <= M(5);
  M6 <= M(6);
  M7 <= M(7);
  M8 <= M(8);
  M9 <= M(9);
  M10 <= M(10);
  M11 <= M(11);
  M12 <= M(12);
  M13 <= M(13);
  M14 <= M(14);
  M15 <= M(15);
  M16 <= M(16);
  M17 <= M(17);
end;
