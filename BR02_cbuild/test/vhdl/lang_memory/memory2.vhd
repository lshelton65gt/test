library IEEE;
use IEEE.std_logic_1164.all;
entity memory2 is
   generic (
           DATAWIDTH : INTEGER := 8;
           ADDRWIDTH : INTEGER := 2
           );
    port (
         clk : in std_logic;
         din : in std_logic_vector(DATAWIDTH-1 downto 0);
         addr : in integer range 0 to 3;
         dout : out std_logic_vector(DATAWIDTH-1 downto 0)
         );
end memory2;

architecture memory2 of memory2 is
   type TMEMORY is array (2**ADDRWIDTH-1 downto 0) of std_logic_vector (DATAWIDTH-1 downto 0);

   signal mem       : TMEMORY;
 begin

   process (clk)
      begin
        if(clk = '1' and clk'event) then
           mem(addr) <= din;
        end if;
      end process;

   process (clk)
      begin
        if(clk = '0' and clk'event) then
           dout <= mem(addr);
        end if;
      end process;
end memory2;
