library ieee;
use ieee.std_logic_1164.all;

entity bug3717 is
  port (
    clk     : in std_logic;
    reset   : in std_logic;
    datain  : in std_logic_vector(9 downto 0);
    dataout : out std_logic_vector(7 downto 0));
end bug3717;

architecture arch of bug3717 is
  subtype address is std_logic_vector(7 downto 0);

  -- This is the original failing code.  Before about cbuild 1.2751,
  -- this construct would error out with a "unconstrained array not
  -- supported" when determining the bounds of the memory.
  TYPE array_of_addresses IS ARRAY (NATURAL RANGE <>) OF address;
  SIGNAL mem : array_of_addresses(0 TO 10);
  
  -- the original workaround for carbon
--  TYPE array_of_addresses IS ARRAY (0 TO 10) OF address;  
--  SIGNAL mem : array_of_addresses;

  signal addr : integer range 0 to 10 := 0;
begin

  data_in: process (clk, reset)
  begin
    if reset = '0' then
      mem <= (others => (others => '0'));
    elsif clk'event and clk = '1' then
      mem(addr) <= datain(7 downto 0) xor mem((addr + 1) mod 10);
      addr <= (addr + 1) mod 10;
    end if;
  end process data_in;

  dataout <= mem(0)(0) & mem(1)(1) & mem(2)(2) & mem(3)(3) &
             mem(4)(4) & mem(5)(5) & mem(6)(6) & mem(7)(7);
end arch;
