-- This test tests assigning a memory slice to a memory slice in a process
-- using variables, not signals.
entity bug4755_6 is
  port (
    clk : in bit;
    rst : in bit;
    din : in  bit_vector(7 downto 0);
    dout : out bit_vector(5 downto 0));
end bug4755_6;

architecture rtl of bug4755_6 is

  type mem_type is array (integer range <>) of bit_vector(7 downto 0);
  signal mem : mem_type(0 to 4);
  signal bigmem : mem_type(5 downto 0);

begin

  fillmem: process (clk, rst)
  begin
    if rst = '0' then
      mem <= (others => "00000000");
    elsif clk'event and clk = '1' then
      mem <= mem(1 to 4) & din;
    end if;
  end process fillmem;

  memcopy: process (clk, rst)
    variable varmem : mem_type(6 downto 1);
  begin
    if clk'event and clk = '1' then
      varmem(5 downto 4) := mem(3 to 4);
      varmem(3 downto 2) := mem(1 to 2);
      varmem(1) := mem(0);
      varmem(6) := din ror 1;
      bigmem <= varmem;
    end if;
  end process;

  outdrive: dout <= bigmem(0)(7) & bigmem(1)(7) & bigmem(2)(7) &
                    bigmem(3)(7) & bigmem(4)(7) & bigmem(5)(7);
  
end rtl;

