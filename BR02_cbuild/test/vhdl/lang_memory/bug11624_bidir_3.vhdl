library ieee;
use ieee.std_logic_1164.all;

package pkg is

  subtype rsdatatype is std_logic_vector(127 downto 0);      
  type rsdatatbltype is array (natural range <>) of rsdatatype;
  
end pkg;


library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bottom3 is

  generic (nbrd : integer := 1);
  
  port (bin  : in  rsdatatbltype(nbrd-1 downto 0);
        clkbuf : in std_logic;
        bout : inout rsdatatbltype(nbrd-1 downto 0));

end bottom3;

architecture bottom3arch of bottom3 is

  signal datasig  : rsdatatbltype(nbrd-1 downto 0);
  signal sourcesig : rsdatatbltype(nbrd-1 downto 0);

  
begin  -- bottom3arch

  sourcesig <= bin;
  
  data: process (clkbuf)
  begin  -- process data
    if clkbuf'event and clkbuf = '1' then  -- rising clock edge
      datasig <= sourcesig;
    end if;
  end process data;

  bout <= datasig;

end bottom3arch;

library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity bug11624_bidir_3 is

  generic (nbrd : integer := 1);
  
  
  port (tin2  : in rsdatatbltype(1 downto 0);
        clk   : in std_logic;
        tout2 : inout rsdatatbltype(1 downto 0));

end bug11624_bidir_3;

architecture toparch of bug11624_bidir_3 is

begin  -- toparch

  -- This is the case of mem selects being assigned to mem selects when the
  -- memory has more than one row.
  u3 : entity work.bottom3
    generic map (nbrd => 2)
    port map (bin(0)  => tin2(0),
              bin(1)  => tin2(1),
              clkbuf  => clk,
              bout(0) => tout2(0),
              bout(1) => tout2(1));

end toparch;
