library ieee;
use ieee.std_logic_1164.all;


package pack is
	type memory is array ( natural range 0 to 1 ) of std_logic_vector(0 to 3);
	type memarray is array ( natural range <>) of memory;
	constant m1 : memory := ( others => (others => '0'));
	constant m2 : memory := ( others => (others => '1'));
	constant mem2 : memarray(0 to 1 ) := ( 0 => m1 , 1 =>m2 );
end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bug3543a is
	port(address : in std_logic; data : out std_logic_vector(0 to 3));
end;

architecture arch of bug3543a is 
signal int_address : natural := 0;
begin
  int_address <= 1 when address = '1' else 0;
  process (int_address)
  begin
    data <= mem2(int_address)(int_address);
  end process;
end;

