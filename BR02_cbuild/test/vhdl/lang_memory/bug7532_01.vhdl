-- August 2007
-- This file tests a concatenation of a indexed two dimensional array with a
-- single dimensional array.
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
 
entity bug7532_01 is
  port (
    in1 : in  std_logic_vector(7 downto 4);
    in2 : in  std_logic_vector(7 downto 4);
    in3 : in  std_logic_vector(7 downto 4);
    in4 : in  std_logic_vector(7 downto 4);
    in5 : in  std_logic_vector(7 downto 4);
    in6 : in  std_logic_vector(7 downto 4);
    in7 : in  std_logic_vector(7 downto 4);
    in8 : in  std_logic_vector(7 downto 4);
    out1: out std_logic_vector(7 downto 4);
    out2: out std_logic_vector(7 downto 4);
    out3: out std_logic_vector(7 downto 4);
    out4: out std_logic_vector(7 downto 4);
    out5: out std_logic_vector(7 downto 4);
    out6: out std_logic_vector(7 downto 4);
    out7: out std_logic_vector(7 downto 4);
    out8: out std_logic_vector(7 downto 4)
    );

end bug7532_01;

architecture rtl of bug7532_01 is
 
TYPE MYBUS IS ARRAY (1 downto 0) OF STD_LOGIC_VECTOR(7 DOWNTO 4);
type arr_arr is array (integer range <>) of MYBUS;

 signal a              :  MYBUS;
 SIGNAL B              :  arr_arr(2 DOWNTO 0);
 SIGNAL C              :  arr_arr(3 DOWNTO 0);
 
begin

  a(1) <= in1;
  a(0) <= in2;
  B(2)(1) <= in3;
  B(2)(0) <= in4;
  B(1)(1) <= in5;
  B(1)(0) <= in6;
  B(0)(1) <= in7;
  B(0)(0) <= in8;
  
--  C <=   B(2) & B(1) & B(0) & a; -- it's interesting that MTI fails to
                                   -- compile this line. It issues an error
                                   -- "2 subprograms named '&' are valid in this context".
                                   -- But the line below it compiles.
  C <=   B(2 downto 1) & B(0)  & a; -- this line is tested

  out1 <= C(3)(1);
  out2 <= C(3)(0);
  out3 <= C(2)(1);
  out4 <= C(2)(0);
  out5 <= C(1)(1);
  out6 <= C(1)(0);
  out7 <= C(0)(1);
  out8 <= C(0)(0);
    
end rtl;
