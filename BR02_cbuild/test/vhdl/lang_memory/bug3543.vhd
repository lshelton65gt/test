library ieee;
use ieee.std_logic_1164.all;


package pack is
	type memory is array ( natural range <> , natural range <> ) of std_logic;
	constant rom : memory(3 downto 0, 3 downto 0) := ( (others => '0'), 
                                             (2 downto 0 => '0', others => '1'),
                                             (1 downto 0 => '0', others => '1'),
                                             (0 => '0', others => '1'));
end pack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pack.all;

entity bug3543 is
	port(address : in unsigned(0 to 1); 
       read : in std_logic;
       ramdata : inout std_logic_vector(3 downto 0);
       romdata : out std_logic_vector(3 downto 0));
end;

architecture arch of bug3543 is 
	signal ram : memory(3 downto 0, 3 downto 0) := ( (others => '0'), 
                                             (2 downto 0 => '0', others => '1'),
                                             (1 downto 0 => '0', others => '1'),
                                             (0 => '0', others => '1'));
begin
  romaccess: for i in 3 downto 0 generate
    romdata(i) <= rom(TO_INTEGER(address), i);
  end generate romaccess;

  ramaccess: for i in 3 downto 0 generate
  process (address, read, ram)
	variable ram2 : memory(3 downto 0, 3 downto 0) := ( (others => '0'), 
                                             (1 downto 0 => '0', others => '1'),
                                             (2 downto 0 => '0', others => '1'),
                                             (0 => '1', others => '0'));
  begin
    if (read = '1') then
      ramdata(i) <= ram(TO_INTEGER(address), i) xor ram2(TO_INTEGER(address), i);
    end if;
  end process;
  end generate ramaccess;
end;

