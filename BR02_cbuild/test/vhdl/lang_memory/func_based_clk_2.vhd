library ieee;
use ieee.std_logic_1164.all;
entity func_based_clk_2 is
  port (d,clk,rst : in std_logic; q: out std_logic);
end;

architecture arch of func_based_clk_2 is
begin
  process (clk, rst)
  begin
    if (rst = '0')  then
      q <= '0';
    elsif (FALLING_EDGE(clk)) then
      q <= d;
    end if;
  end process;
end;
