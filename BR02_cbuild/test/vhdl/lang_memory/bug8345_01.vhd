-- filename: test/vhdl/lang_memory/bug8345_01.vhd
-- Description:  This test duplicates the the problem seen in bug 8345,

-- see test/vhdl/lang_memory/bug8345_04.vhd for details about what was done wrong


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug8345_01 is
  port (
    clock    : in  std_logic;
    int1 : in natural range 0 to 3;
    in1 : in  std_logic;
    in2 : in  std_logic_vector(1 downto 0);
    trans_type  : out std_logic_vector(1 downto 0));
end;

architecture arch of bug8345_01 is
begin
  main: process (clock)
    TYPE   natural_array_type    is array (NATURAL range <>) of natural range 0 to 3; 
    variable array_r : natural_array_type(6 downto 0) := (others => 3);
    VARIABLE count_r         : unsigned(1 DOWNTO 0);


    function shift_left_natural(arg: natural_array_type; count: natural) return natural_array_type is 
      CONSTANT arg_l: integer := arg'length-1; 
      VARIABLE result  : natural_array_type(arg_l DOWNTO 0); 
    begin 
      if count <= arg_l then 
        result(arg_l DOWNTO count) := arg(arg_l-count DOWNTO 0); 
      end if; 
      return result; 
    end shift_left_natural; 

  begin
    if clock'event and clock = '1' then 
      if in1 = '1' then 
        array_r(0)  :=  0;
        array_r(1)  := TO_INTEGER(count_r);
        array_r(1) := int1;
        array_r  := shift_left_natural(array_r, 1);
        
        trans_type   <= STD_LOGIC_VECTOR(to_unsigned(array_r(6), 2)); 
      end if;
    end if;
  end process;
end;

