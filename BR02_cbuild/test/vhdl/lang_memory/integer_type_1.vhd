
entity integer_type_1 is
  port (in1, in2 : in integer range 0 to 18; out1: out integer range 0 to 36);
end;

architecture arch of integer_type_1 is
begin
  out1 <= in1 + in2 ;
end;
