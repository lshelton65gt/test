-- this is a trimmed down version of the test in bug5409, with a few layers of typedefs to make it difficult
PACKAGE apkg IS
  TYPE IntArray3     IS ARRAY ( 3 DOWNTO 0 ) OF INTEGER RANGE 0 TO 34;
  TYPE IntArray3_2 IS ARRAY ( 0  TO 1 )    OF IntArray3;
  TYPE IntArray3_1 IS ARRAY ( 0  TO 1 )    OF IntArray3_2;
  TYPE IntArray10x3 IS ARRAY ( 0  TO 7 )    OF IntArray3_1;

  CONSTANT TBL : IntArray10x3 :=
    (
      ((( 0, 1, 2, 3),( 0, 1, 2, 3)),(( 0, 1, 2, 3),( 0, 1, 2, 3))),
      ((( 1, 2, 3, 4),( 1, 2, 3, 4)),(( 1, 2, 3, 4),( 1, 2, 3, 4))),
      ((( 2, 3, 4, 5),( 2, 3, 4, 5)),(( 2, 3, 4, 5),( 2, 3, 4, 5))),
      ((( 3, 4, 5, 6),( 3, 4, 5, 6)),(( 3, 4, 5, 6),( 3, 4, 5, 6))),
      ((( 4, 5, 6, 7),( 4, 5, 6, 7)),(( 4, 5, 6, 7),( 4, 5, 6, 7))),
      ((( 5, 6, 7, 8),( 5, 6, 7, 8)),(( 5, 6, 7, 8),( 5, 6, 7, 8))),
      ((( 6, 7, 8, 9),( 6, 7, 8, 9)),(( 6, 7, 8, 9),( 6, 7, 8, 9))),
      ((( 7, 8, 9,10),( 7, 8, 9,10)),(( 7, 8, 9,10),( 7, 8, 9,10)))
      );
END apkg;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_arith.ALL;
USE work.apkg.ALL;


ENTITY bug5409_b IS
  PORT (
    d1 : in std_logic_vector(0 downto 0);
    d2 : in std_logic_vector(0 downto 0);
    d3 : in std_logic_vector(2 downto 0);
    osel : out integer;
    o0  : OUT integer;
    o1  : OUT integer;
    o2  : OUT integer
    );
END bug5409_b;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_arith.ALL;
USE IEEE.STD_LOGIC_unsigned.ALL;
USE work.apkg.ALL;

ARCHITECTURE arch OF bug5409_b IS
BEGIN

  proc: PROCESS ( d1, d2, d3 )
    VARIABLE sel1     : INTEGER RANGE 0 TO 1;
    VARIABLE sel2     : INTEGER RANGE 0 TO 1;
    VARIABLE sel3     : INTEGER RANGE 0 TO 127;
    VARIABLE index   : IntArray3;

  BEGIN
    sel1               := conv_integer(d1);
    sel2               := conv_integer(d2);
    sel3               := conv_integer(d3);
    index             := TBL(sel3)(sel2)(sel1);

    osel <= sel2;
    o0 <= index(0);
    o1 <= index(1);
    o2 <= index(2);
  END PROCESS proc;

END arch;


