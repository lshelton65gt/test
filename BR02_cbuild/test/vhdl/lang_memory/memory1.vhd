library IEEE;
use IEEE.std_logic_1164.all;
entity memory1 is  
   generic (
           DATAWIDTH : INTEGER := 8;
           ADDRWIDTH : INTEGER := 2
           );
    port (
         clk : in std_logic;
         din : in std_logic_vector(DATAWIDTH-1 downto 0);
         addr : in integer range 0 to 3;
         dout : out std_logic_vector(DATAWIDTH-1 downto 0)
         );
end memory1;

architecture memory1 of memory1 is
   type TMEMORY is array (2**ADDRWIDTH-1 downto 0) of std_logic_vector (DATAWIDTH-1 downto 0);

   signal mem       : TMEMORY;
 begin

   process (addr, din, mem)
      begin
        mem(addr) <= din;
        dout <= mem(addr);
      end process;
end memory1;


