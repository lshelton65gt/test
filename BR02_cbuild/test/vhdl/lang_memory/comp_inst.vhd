
entity bottom is
  port (i1, i2 : in bit; o1: out bit);
end bottom;

architecture arch of bottom is
begin
  o1 <= i1 and i2;
end;

entity middle is
  port (n1, n2 : in bit; t1: out bit);
end middle;

architecture arch of middle is
component bottom is
  port (i1, i2 : in bit; o1: out bit);
end component;
begin
  bot_inst : bottom port map (i2 => n2, i1 => n1, o1 => t1);
end;

entity comp_inst is
  port (in1, in2 : in bit; out1: out bit);
end comp_inst;

architecture arch of comp_inst is
component middle is
  port (n1, n2 : in bit; t1: out bit);
end component;
begin
  mid_inst : middle port map (in1, in2, out1);
end;
