package mypack is
subtype short is integer range 0 to 15;
end mypack;

use work.mypack.all;

entity sub_type_int is
  port (in1, in2 : in short; out1: out short);
end;

architecture arch of sub_type_int is
begin
  out1 <= in1 + in2 ;
end;
