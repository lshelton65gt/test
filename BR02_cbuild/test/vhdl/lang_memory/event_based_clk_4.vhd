entity event_based_clk_4 is
  port (d,clk,rst : in bit; q: out bit);
end;

architecture arch of event_based_clk_4 is
begin
  process (clk, rst)
  begin
    if (rst = '0')  then
      q <= '0';
    elsif (clk = '1' and not clk'STABLE) then
      q <= d;
    end if;
  end process;
end;
