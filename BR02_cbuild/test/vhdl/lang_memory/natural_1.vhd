
entity natural_1 is
  port (in1, in2 : in natural; out1: out natural);
end;

architecture arch of natural_1 is
begin
  out1 <= in1 + in2 ;
end;
