library ieee;
use ieee.std_logic_1164.all;


package pack is
	type memory is array ( natural range <> , natural range <> ) of std_logic;
	constant rom : memory(3 downto 0, 3 downto 0) := ( (others => '0'), 
                                             (2 downto 0 => '0', others => '1'),
                                             (1 downto 0 => '0', others => '1'),
                                             (0 => '0', others => '1'));
end pack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pack.all;

entity bug3543f is
	port(address : in unsigned(0 to 1); 
       romdata : out std_logic_vector(3 downto 0));
end;

architecture arch of bug3543f is 
alias ALIAS_ROM : memory(3 downto 0, 3 downto 0) is rom;
begin
  romaccess: for i in 3 downto 0 generate
    romdata(i) <= ALIAS_ROM(TO_INTEGER(address), i);
  end generate romaccess;
end;

