library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity bug4792_2 is
  port (
    in1, in2 : in  std_logic_vector(3 downto 0);
    outp     : out std_logic_vector(31 downto 0));
end bug4792_2;

architecture arch of bug4792_2 is
  type word_4 is array (0 to 3) of std_logic_vector(7 downto 0);
-- Makes a mask that is a string of ones starting at the lsb, which is bit 0 of byte 3.
-- The length is in nibbles (4 bits).  This function is used by ldb and dpb.
  function mask_4b(length: std_logic_vector(3 downto 0))
    return word_4 is
    variable mask:      word_4;
  begin
    case length is
      when "0000" =>
        mask(0) := X"00";  mask(1) := X"00";  mask(2) := X"00";  mask(3) := X"00";
      when "0001" =>
        mask(0) := X"00";  mask(1) := X"00";  mask(2) := X"00";  mask(3) := X"0F";
      when "0010" =>
        mask(0) := X"00";  mask(1) := X"00";  mask(2) := X"00";  mask(3) := X"FF";
      when "0011" =>
        mask(0) := X"00";  mask(1) := X"00";  mask(2) := X"0F";  mask(3) := X"FF";
      when "0100" =>
        mask(0) := X"00";  mask(1) := X"00";  mask(2) := X"FF";  mask(3) := X"FF";
      when "0101" =>
        mask(0) := X"00";  mask(1) := X"0F";  mask(2) := X"FF";  mask(3) := X"FF";
      when "0110" =>
        mask(0) := X"00";  mask(1) := X"FF";  mask(2) := X"FF";  mask(3) := X"FF";
      when "0111" =>
        mask(0) := X"0F";  mask(1) := X"FF";  mask(2) := X"FF";  mask(3) := X"FF";
      when "1000" =>
        mask(0) := X"FF";  mask(1) := X"FF";  mask(2) := X"FF";  mask(3) := X"FF";
      when others =>
        mask(0) := X"00";  mask(1) := X"00";  mask(2) := X"00";  mask(3) := X"00";
    end case;
    return mask;
  end mask_4b;

begin
  process( in1, in2 )
    variable mask_s:    word_4;
    variable mask_ps:   word_4;
    variable mask_p:    word_4;
    variable size:      std_logic_vector(3 downto 0);
    variable posn:      std_logic_vector(3 downto 0);
begin
  size := in1;
  posn := in2;
  mask_s := mask_4b(size);
  mask_ps := mask_4b(posn + size);
  mask_p := mask_4b(posn);
  outp <= mask_p(3) & mask_p(2) & mask_p(1) & mask_p(0);
end process;
end arch;
