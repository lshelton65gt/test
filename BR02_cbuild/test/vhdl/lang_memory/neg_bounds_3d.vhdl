-- This test is derived from matrox3.vhdl to test negative bounds in 3d obj.
-- sim gold file from nc. carbon results mismatch with aldec.
entity neg_bounds_3d is
  port (
    a: in integer;
    idx1, idx2: in integer;
    outint: out integer);
end neg_bounds_3d;

architecture arch of neg_bounds_3d is
  type t_data  is array (-1 to 1) of integer;
  type t_lut   is array (-16 to 15) of t_data;

  signal c_rcp_lut : t_lut := (
    (0, 1,1), (2, 3,4), (4, 5,6), (6, 7, 8),
    (8, 9, 0), (10, 11, 12), (12, 13, 14), (14, 15,16),
    (16, 17, 4), (18, 19, 3), (20, 21, 2), (22, 23, 1),
    (24, 25, 5), (26, 27, 6), (28, 29, 7), (30, 31, 8),
    (32, 33, 4), (34, 35, 3), (36, 37, 2), (38, 39,1),
    (40, 41, 1),  (42, 43, 2),  (44, 45, 3),  (46, 47, 4),
    (48, 49, 4),  (50, 51, 3),  (52, 53, 2),  (54, 55, 1),
    (56, 57, 1),  (58, 59, 2),  (60, 61, 3),  (62, 63, 4)
    );
 signal idx1mod, idx2mod : integer := 0;

begin

  idx1mod <= (idx1 mod 32) -16 ;
  idx2mod <= (idx2 mod 3 ) - 1;
  c_rcp_lut(idx1mod)(idx2mod) <= a;
  outint <= c_rcp_lut(idx1mod)(idx2mod);

end arch;
