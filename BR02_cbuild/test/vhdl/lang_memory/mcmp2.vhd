-- last modified: Mar 25 15:21:15 EST 2005
-- file name mcmp2.vhd
-- description: derived from beacon11/logic9,
-- memory assignment and comparison with equal
-- resuls, out1 = TRUE.
-- sim.log.gold file is cbuild generated.

LIBRARY IEEE ;
USE IEEE.std_logic_1164.ALL ;
USE IEEE.std_logic_unsigned.ALL ;

package package_mcmp2 is
	type int_array is array(natural range <>) of integer;
end;

LIBRARY IEEE ;
USE IEEE.std_logic_1164.ALL ;
USE IEEE.std_logic_unsigned.ALL ;
USE WORK.package_mcmp2.all;

entity mcmp2 is
	port( 
		  in1 : in integer;
		  in2 : in integer;
		  out1 : out boolean
	    );
end mcmp2;

architecture arch_mcmp2 of mcmp2 is
begin

	P1:process(in1, in2)
	variable short_array : int_array(0 to 1);
	variable short_array2 : int_array(2 to 3);
	variable long_array : int_array(2 to 5);
	begin
        short_array := (in1, others => in2);
        long_array := (in1, others => in2);
        short_array2 := long_array(2 to 3);
        if (short_array = short_array2) then
          out1 <= TRUE;
        else
          out1 <= FALSE;
		end if;
	end process;

end arch_mcmp2;
