-- last modified: Tue May  9 10:13:06 EDT 2006
-- file name mcmp.vhd
-- description: derived from beacon11/logic9,
-- memory assignment and comparison with not
-- equal results, out1 = FALSE.
-- sim.log.gold file is cbuild generated.


LIBRARY IEEE ;
USE IEEE.std_logic_1164.ALL ;
USE IEEE.std_logic_unsigned.ALL ;

package package_mcmp is
  type int_array is array(natural range <>) of integer;
end;

LIBRARY IEEE ;
USE IEEE.std_logic_1164.ALL ;
USE IEEE.std_logic_unsigned.ALL ;
USE WORK.package_mcmp.all;

entity mcmp is
  port( 
    in1 : in integer;
    in2 : in integer;
    out1 : out boolean;
    out2 : out boolean;
    out3 : out boolean
    );
end mcmp;

architecture arch_mcmp of mcmp is
begin

  P1:process(in1, in2)
    variable short_array : int_array(0 to 1);
    variable short_array2 : int_array(2 to 3);
    variable long_array : int_array(2 to 5);
    variable another_array : int_array(1000 downto 997);
  begin
    short_array := (in1, others => in2);
    long_array := (in2, others => in1);
    short_array2 := long_array(2 to 3);
    another_Array := long_array;
    
    if (short_array = short_array2) then
      out1 <= TRUE;
    else
      out1 <= FALSE;
    end if;

    -- Make sure the full memory assignment worked when the memories have
    -- different indicies, without using memory compare.
    if another_array(1000) /= long_array(2) or
      another_array(999)   /= long_array(3) or
      another_array(998)   /= long_array(4) or
      another_array(997)   /= long_array(5) then
      out2 <= false;
    else
      out2 <= true;
    end if;

    -- Make sure the full memory assignment worked when the memories have
    -- different indicies, using memory compare.
    if another_array /= long_array then
      out3 <= false;
    else
      out3 <= true;
    end if;    
  end process;

end arch_mcmp;
