library ieee;
use ieee.std_logic_1164.all;


package pack is
	type memory is array ( natural range <> , natural range <> ) of std_logic;
	constant rom : memory(0 to 3, 0 to 3) := ( (others => '0'), 
                                             (0 to 2 => '0', others => '1'),
                                             (0 to 1 => '0', others => '1'),
                                             (0 => '0', others => '1'));
end pack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pack.all;

entity bug3543d is
	port(address : in unsigned(0 to 1); 
       ramdata : out std_logic_vector(0 to 3));
end;

architecture arch of bug3543d is 
	signal ram : memory(0 to 3, 0 to 3) := ( (others => '0'), 
                                             (0 to 2 => '0', others => '1'),
                                             (0 to 1 => '0', others => '1'),
                                             (0 => '0', others => '1'));
  signal foo : memory(0 to 3, 0 to 3);
  shared variable bar : memory(0 to 3, 0 to 3);
begin
  process(ram)
  begin
    foo <= ram;
    bar := rom;
  end process;

  ramaccess: for i in 0 to 3 generate
    ramdata(i) <= foo(TO_INTEGER(address), i) or bar(TO_INTEGER(address), i);
  end generate ramaccess;
end;

