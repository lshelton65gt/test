library ieee;
use ieee.std_logic_1164.all;

entity bug3640d is
  port (
    enp          : in     std_logic;
    inp          : in     std_logic_vector(1 downto 0);
    outp         : out std_logic_vector(1 downto 0)
    );
end ;

architecture arch of bug3640d is
begin
  outp(1) <= inp(1) when enp = '1' else 'Z';
  outp(0) <= inp(0) when enp = '1' else 'Z';
end;

