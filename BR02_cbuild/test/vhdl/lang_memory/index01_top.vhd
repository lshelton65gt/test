 --This testcase checks whether cbuild properly shows the error for out of
 -- index memory access.
library IEEE;
use IEEE.std_logic_1164.all;

entity index01_top is
    port ( port1 : in bit;
           port2 : in bit_vector(0 to 1);
           port3 : out bit_vector(1 downto 0)
         );
end entity index01_top;

architecture arch of index01_top is

subtype array1 is bit_vector(0 to 1);
type array2 is array (4 to 6) of array1;
type array3 is array (0 to 1) of array2;

signal nk : array3;

begin
     nk(0)(5)(2) <= port1;
end architecture arch;

