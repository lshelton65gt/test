library ieee;
use ieee.std_logic_1164.all;
entity array_bool1 is
        port(
                 in1  : in  std_logic;
                 b1 : in boolean;
                 b2 : in boolean;
                 b3 : in boolean;
                 out1 : out boolean
                 );
end array_bool1;

architecture array_bool1 of array_bool1 is
begin
  process (in1, b1, b2, b3)
   type bool_arr is array(3 downto 1) of boolean ;
   variable my_bool_arr : bool_arr;
        begin
            my_bool_arr(1) := b1;
            my_bool_arr(2) := b2;
            my_bool_arr(3) := b3;
        if in1 = '1' then  
           out1 <= my_bool_arr(2);
        else
            out1 <= my_bool_arr(3);
        end if;
        end process;

end array_bool1;

