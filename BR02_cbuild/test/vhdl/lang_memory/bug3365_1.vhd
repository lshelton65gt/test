library ieee;
use ieee.std_logic_1164.all;


entity bug3365_1 is
  port(clk : in std_logic ;
       in1: in std_logic_vector(174 downto 0); 
       junk4: out std_logic_vector(5 downto 0);
       junk5: out std_logic_vector(5 downto 0);
       junk6: out std_logic_vector(5 downto 0));
end;

architecture bug3365_1 of bug3365_1 is
 type array_of_64_std_logic_vector6  is array (10 to 73) of std_logic_vector(15 downto 10);
 type r_array_of_64_std_logic_vector6  is array (163 downto 100) of std_logic_vector(15 downto 10);
 signal count , ncount: array_of_64_std_logic_vector6;
 signal r_count, r_ncount : r_array_of_64_std_logic_vector6;
begin
  process (clk, in1)
  begin
     if (clk'event and clk = '1') then
      ncount <= (others => "011100");
      count <= ncount;
      junk4 <= count(11);
  
      r_ncount <= ( "111111" , others => "000000");
      r_count <= r_ncount;
      junk5 <= r_count(163);
      junk6 <= r_count(100);
     end if;
  end process;
end;

