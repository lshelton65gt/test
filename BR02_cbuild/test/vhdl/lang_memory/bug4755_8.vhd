-- This test tests assigning a memory to a variable slice in a process
entity bug4755_8 is
  port (
    clk : in bit;
    rst : in bit;
    din : in  bit_vector(7 downto 0);
    dout : out bit_vector(5 downto 0));
end bug4755_8;

architecture rtl of bug4755_8 is

  type mem_type is array (integer range <>) of bit_vector(7 downto 0);
  signal mem : mem_type(4 downto 0);
  signal bigmem : mem_type(5 downto 0);

begin

  fillmem: process (clk, rst)
  begin
    if rst = '0' then
      mem <= (others => "00000000");
    elsif clk'event and clk = '1' then
      mem <= mem(3 downto 0) & din;
    end if;
  end process fillmem;

  memcopy: process (clk, rst)
    variable varmem : mem_type(5 downto 0);
    variable varmem2 : mem_type(4 downto 0);
  begin
    if clk'event and clk = '1' then
      varmem2 := mem(2) & mem(1) & mem(4) & mem(0) & mem(3);
      varmem(5 downto 0) := varmem;
      varmem(0) := din ror 4;
      bigmem(5 downto 0) <= varmem(5 downto 0);
    end if;
  end process;

  outdrive: dout <= bigmem(0)(7) & bigmem(1)(7) & bigmem(2)(7) &
                    bigmem(3)(7) & bigmem(4)(7) & bigmem(5)(7);
  
end rtl;

