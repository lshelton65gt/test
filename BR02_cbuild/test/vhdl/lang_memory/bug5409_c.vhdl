-- this is a trimmed down version of the test in bug5409
PACKAGE apkg IS
  TYPE IntArray3     IS ARRAY ( 3 DOWNTO 0 ) OF INTEGER RANGE 0 TO 34;
  TYPE IntArray10x3 IS ARRAY ( 0  TO 9 )    OF IntArray3;

  CONSTANT TBL : IntArray10x3 :=
    (
      ( 0,1,2,3),
      ( 1,2,3,4),
      ( 2,3,4,5),
      ( 3,4,5,6),
      ( 4,5,6,7),
      ( 5,6,7,8),
      ( 6,7,8,9),
      ( 7,8,9,10),
      ( 8,9,10,11),
      ( 9,10,11,12)
      );
END apkg;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_arith.ALL;
USE work.apkg.ALL;


ENTITY bug5409_c IS
  PORT (
    d2 : in std_logic_vector(1 downto 0);
    osel : out integer;
    o0  : OUT integer;
    o1  : OUT integer;
    o2  : OUT integer;
    o3  : OUT integer
    );
END bug5409_c;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_arith.ALL;
USE IEEE.STD_LOGIC_unsigned.ALL;
USE work.apkg.ALL;

ARCHITECTURE arch OF bug5409_c IS
BEGIN

  proc: PROCESS ( d2)
    VARIABLE sel     : INTEGER RANGE 0 TO 127;
    VARIABLE index   : IntArray3;

  BEGIN
    sel               := conv_integer(d2);
    index             := TBL(sel);

    osel <= sel;
    o0 <= index(0);
    o1 <= index(1);
    o2 <= index(2);
    o3 <= TBL(3)(0);
  END PROCESS proc;

END arch;


