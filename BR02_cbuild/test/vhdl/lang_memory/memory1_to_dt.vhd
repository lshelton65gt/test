-- test all declaration order variations of a memory (with respect to to and downto).
library IEEE;
use IEEE.std_logic_1164.all;
entity memory1_to_dt is  
   generic (
           DATAWIDTH : INTEGER := 8;
           ADDRWIDTH : INTEGER := 2
           );
    port (
         clk : in std_logic;
         din : in std_logic_vector(7 downto 0);
         addr_r : in integer range 0 to 3;
         addr_r2 : in integer range 0 to 3;
         addr_w : in integer range 0 to 3;
         bitpos : in integer range 0 to 7;
         dout1 : out std_logic_vector(7 downto 0);
         dout2 : out std_logic_vector(7 downto 0);
         dout3 : out std_logic_vector(7 downto 0);
         dout4 : out std_logic_vector(7 downto 0);
         bout1 : out std_logic;
         bout2 : out std_logic;
         bout3 : out std_logic;
         bout4 : out std_logic
         );
end memory1_to_dt;

architecture memory1_to_dt of memory1_to_dt is 
   subtype vecdt is std_logic_vector (7 downto 0);
   subtype vecto is std_logic_vector (0 to 7);
   
   type TMEMORY_DT_DT   is array (2**ADDRWIDTH-1 downto 0) of vecdt;
   type TMEMORY_DT_TO   is array (2**ADDRWIDTH-1 downto 0) of vecto;
   type TMEMORY_TO_DT   is array (0 to 2**ADDRWIDTH-1)     of vecdt;
   type TMEMORY_TO_TO   is array (0 to 2**ADDRWIDTH-1)     of vecto;

   signal mem1       : TMEMORY_DT_DT := ("00000000","00000001","00000010","00000011");
   signal mem2       : TMEMORY_DT_TO := ("00000100","00000101","00000110","00000111");
   signal mem3       : TMEMORY_TO_DT := ("00001100","00001101","00001110","00001111");
   signal mem4       : TMEMORY_TO_TO := ("00011100","00011101","00011110","00011111");
 begin

   process (addr_r, addr_r2, addr_w, din, mem1, mem2, mem3, mem4)
      begin
        mem1(addr_w) <= din; 
        mem2(addr_w) <= din; 
        mem3(addr_w) <= din; 
        mem4(addr_w) <= din; 
        dout1 <= mem1(addr_r);
        dout2 <= mem2(addr_r);
        dout3 <= mem3(addr_r);
        dout4 <= mem4(addr_r);
        bout1 <= mem1(addr_r2)(bitpos);
        bout2 <= mem2(addr_r2)(bitpos);
        bout3 <= mem3(addr_r2)(bitpos);
        bout4 <= mem4(addr_r2)(bitpos);
      end process;
end memory1_to_dt;


