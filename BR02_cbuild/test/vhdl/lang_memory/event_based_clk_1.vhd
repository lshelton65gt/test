entity event_based_clk_1 is
  port (d,clk,rst1, rst2: in bit; q: out bit);
end;

architecture arch of event_based_clk_1 is
begin
  process (clk, rst1, rst2)
  begin
    if (rst1 = '0')  then
      q <= '0';
    elsif (rst2 = '1') then
      q <= '0';
    elsif (clk = '0' and clk'EVENT) then
      q <= d;
    end if;
  end process;
end;
