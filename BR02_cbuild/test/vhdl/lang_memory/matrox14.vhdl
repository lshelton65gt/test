library IEEE;
use IEEE.std_logic_1164.all;

entity matrox14 is
  port (
    datain : in std_logic_vector(31 downto 0);
    onecrnr  : out std_logic
    );
end matrox14;

architecture functional of matrox14 is
  type eqge_t is array (1 downto 0) of std_logic_vector(3 downto 0);
  type snglcrnr_t is array (3 downto 0) of eqge_t;

  signal snglcrnr   : snglcrnr_t;       -- (3:0)(1:0)(3:0)
begin  -- functional

  loaddata: process (datain)
    variable base : integer;
  begin
    for i in 3 downto 0 loop
      for j in 1 downto 0 loop
        base := ((i * 8) + (j + 1) * 4) - 1;
        snglcrnr(i)(j) <= datain(base downto base - 3);
      end loop;  -- j
    end loop;  -- i
  end process loaddata;
  
  Xdpvsbldta : process (snglcrnr)
    function chk1crnr (c0 : eqge_t; c1 : eqge_t) return std_logic is
      variable x : std_logic;
    begin  -- chk1crnr
      x := '0';
      for e in 3 downto 0 loop
        if (c0(1)(e) & c1(0)(e)) = std_logic_vector'("10") then
          x := '1';
        end if;
      end loop;
      return x;
    end chk1crnr;
  begin

    onecrnr <= chk1crnr(snglcrnr(0),  snglcrnr(1));

  end process Xdpvsbldta;
end functional;
