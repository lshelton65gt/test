-- last mod: Mon Oct 30 07:36:27 2006
-- filename: test/vhdl/lang_memory/memory4.vhd
-- Description:  This design is just like memory3 with a minor difference.
-- the port mem1 on mid is left disconnected but it is listed in the port list
-- where mid is instantiated.  in memory3 it was not listed.


library ieee;
use ieee.std_logic_1164.all;

package pack is
  type recarray is array(0 to 1) of std_logic_vector(7 downto 0);
  type mem2 is array (1 downto 0) of std_logic_vector (1 downto 0);
  type TMEMORY is array (1 downto 0) of mem2;

end pack;


-- bottom
library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bot is
  port (
    bp1, bp2 : in     std_logic_vector(7 downto 0);
    brec     : buffer recarray);
end bot;

architecture barch of bot is
begin
  brec(0) <= bp1;
  brec(1) <= bp2;
end barch;


-- middle

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity mid is
  port ( 
    mp1, mp2 : in     std_logic_vector(7 downto 0);
    mem1     : buffer TMEMORY;          --leave this disconnected
    mrec     : buffer recarray);
end mid;

architecture march of mid is
  component bot
    port (
      bp1, bp2           : in     std_logic_vector(7 downto 0);
      brec               : buffer recarray);
  end component;
  
begin
  b1: bot port map ( bp1 => mp1, bp2 => mp2, brec => mrec );

  mem1 <= (others => (others => (others => '0')));

end march;



-- top


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity memory4a is
  port (
    p1, p2 : in     std_logic_vector(7 downto 0);
    outp   : buffer std_logic_vector(15 downto 0));
end memory4a;

architecture arch of memory4a is

  signal toprec : recarray;

  component mid
    port (
      mp1, mp2 : in     std_logic_vector(7 downto 0);
      mem1 : buffer TMEMORY;
      mrec     : buffer recarray);
  end component;
  
begin
  m1 : mid port map (mp1  => p1, mp2  => p2, mrec => toprec); -- mem1 is left disconnected
  outp <= toprec(0) & toprec(1);
end arch;
