-- variable index to memory
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
entity dynIndex is  
    port (
         clk : in std_logic;
         din : in std_logic_vector(255 downto 0);
         addr : in unsigned(7 downto 0);
         dout : out std_logic_vector(255 downto 0)
         );
end dynIndex;

architecture dynIndex of dynIndex is
   type TMEMORY is array (255 downto 0) of std_logic_vector (255 downto 0);

   signal mem       : TMEMORY;
 begin

   process (addr, din, mem)
     variable addr_int : integer;
      begin
        addr_int :=  To_INTEGER(addr);
        mem(addr_int) <= din;
        dout <= mem(addr_int)(addr_int-1 downto 0)  &
                mem(addr_int)(255 downto addr_int);
      end process;
end dynIndex;


