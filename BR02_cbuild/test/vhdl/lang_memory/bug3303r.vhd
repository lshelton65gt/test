library ieee;
--use ieee.numeric_bit.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_1164.all;

entity bug3303r is
  port(in1: in std_logic_vector(1 downto 0);
       in2: in std_logic_vector(1 downto 0);
       out1: out std_logic_vector(1 downto 0);
       out2: out std_logic_vector(1 downto 0);
       revout1: out std_logic_vector(1 downto 0);
       revout2: out std_logic_vector(1 downto 0);
       revout3: out std_logic_vector(1 downto 0));
end;

architecture bug3303r of bug3303r is
 type   dataarray    is array(0 to 6) of std_logic_vector(1 downto 0);
 type   dataarrayreverse    is array(6 downto 0) of std_logic_vector(1 downto 0);
 signal data2align   : dataarray;
 signal data2align2   : dataarray;
 signal data2align_rev   : dataarrayreverse;
 signal data2align_rev2   : dataarrayreverse;
begin
  process (in1, in2, data2align, data2align2,data2align_rev,data2align_rev2)
  begin
     data2align(1 to 5) <= (in1, in2, in1, in2, in1);
     data2align2(1 to 2) <= data2align(1 to 2);
     out1 <= data2align2(1); -- in1
     out2 <= data2align2(2); -- in2
     data2align_rev(6 downto 3) <= (in2, in2, in1, in1);
     data2align_rev2(2 downto 0) <= data2align_rev(6 downto 4);
     revout1 <= data2align_rev2(2);  -- in2
     revout2 <= data2align_rev2(1);  -- in2 
     revout3 <= data2align_rev2(0);  -- in1 
  end process;

end;
