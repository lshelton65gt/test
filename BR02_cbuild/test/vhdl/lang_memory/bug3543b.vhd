library ieee;
use ieee.std_logic_1164.all;


package pack is
	type memory is array ( natural range <> , natural range <> ) of std_logic;
end pack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pack.all;

entity bug3543b is
	port(address : in unsigned(0 to 1); 
       read : in std_logic;
       datain : in std_logic_vector(0 to 3);
       ramdata : out std_logic_vector(0 to 3));
end;

architecture arch of bug3543b is 
	signal ram : memory(0 to 3, 0 to 3);
begin
  ramaccess: for i in 0 to 3 generate
  rw: process (address, read)
  begin
    if (read = '1') then
      ramdata(i) <= ram(TO_INTEGER(address), i);
    else
      ram(TO_INTEGER(address), i) <= datain(i);
    end if;
  end process;
  end generate ramaccess;
end;

