entity process_with_var is
  port (d,clk,rst: in bit; q: out bit);
end;

architecture arch of process_with_var is
begin
  process (d, rst)
  variable temp : bit;
  begin
    if (rst = '0')  then
      temp := '0';
    else
      temp := d;
    end if;
    q <= temp;
  end process ;
end;
