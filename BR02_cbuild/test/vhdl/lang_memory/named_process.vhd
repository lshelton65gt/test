entity named_process is
  port (d,clk,rst: in bit; q: out bit);
end;

architecture arch of named_process is
begin
  p1: process (rst,d)
  begin
    if (rst = '0')  then
      q <= '0';
    else
      q <= d;
    end if;
  end process p1;
end;
