-- Cbuild testdriver doesnt yet support memory in interface

package p is
  type int_arr is array(2 downto 1) of integer ;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity dut is
  port(
    in1  : in  std_logic;
    int1 : in int_arr;
    out1 : out integer
    );
end dut;

architecture array_int2 of dut is
begin
  process (in1, int1)
    variable my_int_arr : int_arr;
  begin
    my_int_arr := int1;
    if in1 = '1' then  
      out1 <= my_int_arr(2);
    else
      out1 <= my_int_arr(1);
    end if;
  end process;

end array_int2;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity array_int2 is
  port (
    in1    : in  std_logic;
    int1_2 : in  integer;
    int1_1 : in  integer;
    out1   : out integer);
end array_int2;

architecture arch of array_int2 is
  component dut is
                  port(
                    in1  : in  std_logic;
                    int1 : in int_arr;
                    out1 : out integer
                    );
  end component dut;

  signal actual_int1 : int_arr;
begin  -- arch

  actual_int1 <= (int1_2, int1_1);
  
  u1 : dut port map (
    in1  => in1,
    int1 => actual_int1,
    out1 => out1);

end arch;
