-- This test tests an concat of a slice and an element in a blocking assign
entity bug4194_bugvar is
  port (
    clk : in bit;
    rst : in bit;
    din : in  bit_vector(15 downto 0);
    dout : out bit_vector(15 downto 0));
end bug4194_bugvar;

architecture rtl of bug4194_bugvar is

  type mem_type is array (5 downto 0) of bit_vector(15 downto 0);

begin  -- rtl

  process (clk, rst)
  variable mem : mem_type;
  begin  -- process
    if rst = '0' then
      mem := (others => (others => '0'));
      dout <= "0000000000000000";
    elsif clk'event and clk = '1' then
      mem := din & mem(5 downto 1);
      dout <= mem(0);
    end if;
  end process;
end rtl;

