library IEEE;
use IEEE.std_logic_1164.all;


package pack is
  type     mem is array(0 to 1) of std_logic_vector(2 downto 0);     
end pack;


library IEEE;
use IEEE.std_logic_1164.all;
use work.pack.all;

entity bug3640 is
  port (
    enp          : in     std_logic;          
    inp          : in     std_logic_vector(5 downto 0);
    outs1        : out character;
    outs2        : out character;
    outp         : out std_logic_vector(5 downto 0)
    );
end bug3640;

architecture functional of bug3640 is
signal outs : string(1 to 2);
signal temp : mem;
begin
  
  temp <= ("000", "000") when enp = '0' else (inp(2 downto 0 ), inp(5 downto 3));
  outs <= "XY" when enp = '1' else "ZZ";
  outp(2 downto 0 ) <= temp(0);
  outp(5 downto 3) <= temp(1);
  outs1 <= outs(1);
  outs2 <= outs(2);
end functional;
