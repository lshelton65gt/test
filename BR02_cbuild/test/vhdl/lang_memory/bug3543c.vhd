library ieee;
use ieee.std_logic_1164.all;


package pack is
	type memory is array ( natural range <> , natural range <> ) of std_logic;
	constant rom : memory(3 downto 0, 3 downto 0) := ( (others => '0'), 
                                             (2 downto 0 => '0', others => '1'),
                                             (1 downto 0 => '0', others => '1'),
                                             (0 => '0', others => '1'));
end pack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pack.all;

entity bug3543c is
	port(address : in unsigned(1 downto 0); 
       ramdata : out std_logic_vector(3 downto 0));
end;

architecture arch of bug3543c is 
	signal ram : memory(3 downto 0, 3 downto 0) := ( (others => '0'), 
                                             (2 downto 0 => '0', others => '1'),
                                             (1 downto 0 => '0', others => '1'),
                                             (0 => '0', others => '1'));
  signal foo : memory(3 downto 0, 3 downto 0);
  shared variable bar : memory(3 downto 0, 3 downto 0);
begin
  process(address)
  begin
    foo <= ram;
    bar := rom;
  end process;

  ramaccess: for i in 3 downto 0 generate
    ramdata(i) <= foo(TO_INTEGER(address), i) or bar(TO_INTEGER(address), i);
  end generate ramaccess;
end;

