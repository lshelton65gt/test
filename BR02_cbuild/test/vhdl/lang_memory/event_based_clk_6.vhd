entity event_based_clk_6 is
  port (d,clk,rst : in bit; q: out bit);
end;

architecture arch of event_based_clk_6 is
signal arbitary_signal1, arbitary_signal2, arbitary_signal3 : bit;
begin
  p1: process (clk, rst)
  begin
    if (rst = '1') then
      arbitary_signal1 <= '0';
    elsif (clk'event and clk = '1') then
      arbitary_signal1 <= d;
    end if;
  end process p1;

  p2: process (arbitary_signal1)
  begin
    arbitary_signal2 <= arbitary_signal1;
  end process p2;

  p3: process (arbitary_signal2)
  begin
    arbitary_signal3 <= arbitary_signal2;
  end process p3;

  q <= arbitary_signal3;

end;
