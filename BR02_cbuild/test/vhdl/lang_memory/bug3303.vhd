library ieee;
--use ieee.numeric_bit.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_1164.all;

entity bug3303 is
  port(in1: in std_logic_vector(1 downto 0);
       in2: in std_logic_vector(1 downto 0);
       out1: out std_logic_vector(1 downto 0);
       out2: out std_logic_vector(1 downto 0);
       revout1: out std_logic_vector(1 downto 0);
       revout2: out std_logic_vector(1 downto 0);
       junk1: out std_logic_vector(1 downto 0);
       junk2: out std_logic_vector(1 downto 0));
end;

architecture bug3303 of bug3303 is
 type   dataarray    is array(0 to 6) of std_logic_vector(1 downto 0);
 type   dataarrayreverse    is array(6 downto 0) of std_logic_vector(1 downto 0);
 signal data2align   : dataarray;
 signal data2align_rev   : dataarrayreverse;
begin
  process (in1, in2, data2align)
  begin
--     data2align(0)(1 downto 0) <= in1;
     data2align(4 to 5) <= (in1, in2);
     out1 <= data2align(4);   -- in1
     out2 <= data2align(5);   -- in2
     data2align_rev(6 downto 3) <= (in2, in2, in1, in1);
     revout1 <= data2align_rev(5);  -- in2
     revout2 <= data2align_rev(0);  -- 00
     data2align_rev(2 downto 1) <= ((others => '0') , (others => '1'));
     junk1 <= data2align_rev(1);  -- 11
     junk2 <= data2align_rev(2);  -- 00
  end process;

end;
