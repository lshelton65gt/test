-- filename: test/vhdl/lang_memory/issue36.vhd
-- Description:  This test duplicates issue 36 from Customer 2.
-- it just has a 3 d array and uses the 'length qualifier
-- on a slice of that memory.


library ieee;
use ieee.std_logic_1164.all;

entity issue36 is
  generic (
    WIDTH : integer := 4);

  port (
    clock    : in  std_logic;
    out1     : out integer);
end;

architecture arch of issue36 is
  type twodee is array (0 to 1) of std_logic_vector(100 to 107);
  type threedee is array (0 to 1) of twodee;

  signal threedee_a : threedee;
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1 <= threedee_a(0)(0)'length;  --expecting the value 8
    end if;
  end process;
end;
