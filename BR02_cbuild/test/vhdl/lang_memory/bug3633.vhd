library ieee;
use ieee.std_logic_1164.all;

entity bug3633 is
  port( in1, in2, in3, in4, ena1, ena2, ena3 : in std_logic; 
        out1, out2 : out std_logic);
end;

architecture arch of bug3633 is
begin
  -- this is a good interesting testcase for Z conflict alerts.
  -- out1 is driven by a tristate and a latch.  They are not in
  -- conflict, and we don't give a message about it because
  -- out1 is *not* killed in the second assign to it.
  out1 <= in1 and in2 when in1 = '1' else 'Z';
  out1 <= '1' when in1 /= '1';

  out2 <= in1 when ena1 = '1' else
          in2 when ena2 = '1' else
          in3 when ena3 = '1' else
          in4 ;
end;
