-- test passing resynthesized memories through component ports
library ieee;
use ieee.std_logic_1164.all;

package pack is
  constant BITWIDTH : integer := 8; -- NUMBITS
  constant PIPEWIDTH : integer := 4; -- NUMBYTES
  constant NUMPIPES : integer := 2;
  type bytebank is array (PIPEWIDTH-1 downto 0) of std_logic_vector(BITWIDTH-1 downto 0);
  type databank is array (NUMPIPES-1 downto 0) of bytebank;
end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bottom is
  port (
    indata  : in    bytebank;
    outdata : inout bytebank);
end bottom;

architecture arch of bottom is

begin  -- arch

  p1: process (indata)
  begin  -- process indata
    for i in 0 to 1 loop
      outdata(i) <= indata(1 - i);
    end loop;  -- i
    
  end process;

end arch;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity matrox13 is
  port (
    in00, in01, in02, in03 : in std_logic_vector(7 downto 0);
    in10, in11, in12, in13 : in std_logic_vector(7 downto 0);
    out00, out01, out02, out03 : out std_logic_vector(7 downto 0);
    out10, out11, out12, out13 : out std_logic_vector(7 downto 0)
    );
end matrox13;

architecture arch of matrox13 is
component bottom is
  port (
    indata  : in     bytebank;
    outdata : inout bytebank);
end component bottom;
signal aindata, aoutdata : databank;
begin  -- arch

  aindata <= ((in13, in12, in11, in10), (in03, in02, in01, in00));

  u1 : bottom port map (
    indata  => aindata(0),
    outdata => aoutdata(0));

  out13 <= aoutdata(1)(3);
  out03 <= aoutdata(0)(3);
  out12 <= aoutdata(1)(2);
  out02 <= aoutdata(0)(2);
  out11 <= aoutdata(1)(1);
  out01 <= aoutdata(0)(1);
  out10 <= aoutdata(1)(0);
  out00 <= aoutdata(0)(0);
  
end arch;
