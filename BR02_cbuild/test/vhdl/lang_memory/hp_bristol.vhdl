LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

PACKAGE aes_package IS

   constant BLOCK_SIZE : integer := 4;  -- Nb - four words per block always

   constant NUM_ROUNDS_KEY_4   : integer := 10;
   constant NUM_ROUNDS_KEY_8   : integer := 14;
   constant MAX_NUM_ROUNDS     : integer := NUM_ROUNDS_KEY_8;
   
   -- NOTE: we only support key lengths 4, 8
   constant MAX_KEY_EXP        : integer := 3;  -- 8 x 4 byte words = 256 bits
   constant MAX_KEY_LENGTH     : integer := 2**MAX_KEY_EXP;

   -- Pre-calculate maximum i loops for key expansion
   constant MAX_ROUND_KEY_WORDS_4      : integer := BLOCK_SIZE * (NUM_ROUNDS_KEY_4 + 1);
   constant MAX_ROUND_KEY_WORDS_8      : integer := BLOCK_SIZE * (NUM_ROUNDS_KEY_8 + 1);
   constant MAX_ROUND_KEY_WORDS_BITS   : integer := 6;  -- Number of bits in a vector representation
   constant MAX_ROUND_KEY_WORDS        : integer := MAX_ROUND_KEY_WORDS_8;

   type int_array_t is array (natural range <>) of integer range 0 to MAX_NUM_ROUNDS;

   subtype byte_t      is std_logic_vector(7 downto 0);

   type word_t         is array (0 to 3)                of byte_t;  -- "column" in a 4x4 block matrix

   type word_array_t   is array (natural range <>)      of word_t;

   type block_t        is array (0 to BLOCK_SIZE-1)     of word_t;  -- each block is a 4x4 byte matrix

   subtype block_vector_t is std_logic_vector(127 downto 0);

   type aes_key_t      is array (0 to MAX_KEY_LENGTH-1) of word_t;  -- Full key (256 bit)

   subtype round_key_t is block_t;

   subtype num_rounds_t is integer range 0 to MAX_NUM_ROUNDS;

   type key_t is record
                    value      : aes_key_t;
                    length     : integer range 0 to MAX_KEY_LENGTH;
                    length_exp : integer range 0 to MAX_KEY_EXP;
                    num_rounds : num_rounds_t;
                 end record;

   type round_keys_t  is array (0 to MAX_NUM_ROUNDS) of round_key_t;

   type round_block_t is array (natural range <>)    of block_t;

   type sbox_word_t   is array (0 to 1)              of byte_t;  -- for input to sbox calculating block

   type s_box_t       is array (0 to 255)            of bit_vector(7 downto 0);  -- 2-D 16x16 array

   constant sbox : s_box_t :=
      (X"63", X"7c", X"77", X"7b", X"f2", X"6b", X"6f", X"c5", X"30", X"01", X"67", X"2b", X"fe", X"d7", X"ab", X"76",
       X"ca", X"82", X"c9", X"7d", X"fa", X"59", X"47", X"f0", X"ad", X"d4", X"a2", X"af", X"9c", X"a4", X"72", X"c0",
       X"b7", X"fd", X"93", X"26", X"36", X"3f", X"f7", X"cc", X"34", X"a5", X"e5", X"f1", X"71", X"d8", X"31", X"15",
       X"04", X"c7", X"23", X"c3", X"18", X"96", X"05", X"9a", X"07", X"12", X"80", X"e2", X"eb", X"27", X"b2", X"75",
       X"09", X"83", X"2c", X"1a", X"1b", X"6e", X"5a", X"a0", X"52", X"3b", X"d6", X"b3", X"29", X"e3", X"2f", X"84",
       X"53", X"d1", X"00", X"ed", X"20", X"fc", X"b1", X"5b", X"6a", X"cb", X"be", X"39", X"4a", X"4c", X"58", X"cf",
       X"d0", X"ef", X"aa", X"fb", X"43", X"4d", X"33", X"85", X"45", X"f9", X"02", X"7f", X"50", X"3c", X"9f", X"a8",
       X"51", X"a3", X"40", X"8f", X"92", X"9d", X"38", X"f5", X"bc", X"b6", X"da", X"21", X"10", X"ff", X"f3", X"d2",
       X"cd", X"0c", X"13", X"ec", X"5f", X"97", X"44", X"17", X"c4", X"a7", X"7e", X"3d", X"64", X"5d", X"19", X"73",
       X"60", X"81", X"4f", X"dc", X"22", X"2a", X"90", X"88", X"46", X"ee", X"b8", X"14", X"de", X"5e", X"0b", X"db",
       X"e0", X"32", X"3a", X"0a", X"49", X"06", X"24", X"5c", X"c2", X"d3", X"ac", X"62", X"91", X"95", X"e4", X"79",
       X"e7", X"c8", X"37", X"6d", X"8d", X"d5", X"4e", X"a9", X"6c", X"56", X"f4", X"ea", X"65", X"7a", X"ae", X"08",
       X"ba", X"78", X"25", X"2e", X"1c", X"a6", X"b4", X"c6", X"e8", X"dd", X"74", X"1f", X"4b", X"bd", X"8b", X"8a",
       X"70", X"3e", X"b5", X"66", X"48", X"03", X"f6", X"0e", X"61", X"35", X"57", X"b9", X"86", X"c1", X"1d", X"9e",
       X"e1", X"f8", X"98", X"11", X"69", X"d9", X"8e", X"94", X"9b", X"1e", X"87", X"e9", X"ce", X"55", X"28", X"df",
       X"8c", X"a1", X"89", X"0d", X"bf", X"e6", X"42", X"68", X"41", X"99", X"2d", X"0f", X"b0", X"54", X"bb", X"16"
      );

   constant inv_sbox : s_box_t :=
      (X"52", X"09", X"6a", X"d5", X"30", X"36", X"a5", X"38", X"bf", X"40", X"a3", X"9e", X"81", X"f3", X"d7", X"fb",
       X"7c", X"e3", X"39", X"82", X"9b", X"2f", X"ff", X"87", X"34", X"8e", X"43", X"44", X"c4", X"de", X"e9", X"cb",
       X"54", X"7b", X"94", X"32", X"a6", X"c2", X"23", X"3d", X"ee", X"4c", X"95", X"0b", X"42", X"fa", X"c3", X"4e",
       X"08", X"2e", X"a1", X"66", X"28", X"d9", X"24", X"b2", X"76", X"5b", X"a2", X"49", X"6d", X"8b", X"d1", X"25",
       X"72", X"f8", X"f6", X"64", X"86", X"68", X"98", X"16", X"d4", X"a4", X"5c", X"cc", X"5d", X"65", X"b6", X"92",
       X"6c", X"70", X"48", X"50", X"fd", X"ed", X"b9", X"da", X"5e", X"15", X"46", X"57", X"a7", X"8d", X"9d", X"84",
       X"90", X"d8", X"ab", X"00", X"8c", X"bc", X"d3", X"0a", X"f7", X"e4", X"58", X"05", X"b8", X"b3", X"45", X"06",
       X"d0", X"2c", X"1e", X"8f", X"ca", X"3f", X"0f", X"02", X"c1", X"af", X"bd", X"03", X"01", X"13", X"8a", X"6b",
       X"3a", X"91", X"11", X"41", X"4f", X"67", X"dc", X"ea", X"97", X"f2", X"cf", X"ce", X"f0", X"b4", X"e6", X"73",
       X"96", X"ac", X"74", X"22", X"e7", X"ad", X"35", X"85", X"e2", X"f9", X"37", X"e8", X"1c", X"75", X"df", X"6e",
       X"47", X"f1", X"1a", X"71", X"1d", X"29", X"c5", X"89", X"6f", X"b7", X"62", X"0e", X"aa", X"18", X"be", X"1b",
       X"fc", X"56", X"3e", X"4b", X"c6", X"d2", X"79", X"20", X"9a", X"db", X"c0", X"fe", X"78", X"cd", X"5a", X"f4",
       X"1f", X"dd", X"a8", X"33", X"88", X"07", X"c7", X"31", X"b1", X"12", X"10", X"59", X"27", X"80", X"ec", X"5f",
       X"60", X"51", X"7f", X"a9", X"19", X"b5", X"4a", X"0d", X"2d", X"e5", X"7a", X"9f", X"93", X"c9", X"9c", X"ef",
       X"a0", X"e0", X"3b", X"4d", X"ae", X"2a", X"f5", X"b0", X"c8", X"eb", X"bb", X"3c", X"83", X"53", X"99", X"61",
       X"17", X"2b", X"04", X"7e", X"ba", X"77", X"d6", X"26", X"e1", X"69", X"14", X"63", X"55", X"21", X"0c", X"7d"
      );

   ----------------------------------------------------------------------------
   -- Galois multiplication GF(2^8) functions
   ----------------------------------------------------------------------------
   constant irreducible_polynomial : unsigned(byte_t'range) := to_unsigned(16#1b#,byte_t'length);  --ignore bit 8
   
   function gf_mult_2 (
      signal in_byte : byte_t)
      return byte_t;

   function gf_mult_3 (
      signal in_byte : byte_t)
      return byte_t;

   ----------------------------------------------------------------------------
   -- Block to vector conversions
   ----------------------------------------------------------------------------
   function block_to_vec (
      signal block_in : block_t)
      return block_vector_t;
   
   function vec_to_block (
      signal vec_in : block_vector_t)
      return block_t;
   
   ----------------------------------------------------------------------------
   -- Function to do xor of aes blocks
   ----------------------------------------------------------------------------
   function xor_aes_block (
      signal block1 : block_t;
      signal block2  : block_t
      )
      return block_t;
   
END aes_package;
--######################################################################--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

PACKAGE body aes_package IS

   ----------------------------------------------------------------------------
   -- Galois multiplication GF(2^8) functions
   ----------------------------------------------------------------------------
   function gf_mult_2 (
      signal in_byte : byte_t)
      return byte_t is

      variable result : unsigned(byte_t'range);
      
   begin
      -------------------------------------------------------------------------
      -- Multiply by 2 is:
      --    shift left
      --    if msb is '1',
      --       xor with irreducial_polynomial
      -------------------------------------------------------------------------
      result := shift_left(unsigned(in_byte), 1);
      
      if in_byte(in_byte'high) = '1' then
         result := result xor irreducible_polynomial;
      end if;

      return(std_logic_vector(result));
   end;

   function gf_mult_3 (
      signal in_byte : byte_t)
      return byte_t is
      
   begin
      -------------------------------------------------------------------------
      -- Multiply by 3 is:
      --    in_byte xor 2 * in_byte
      -------------------------------------------------------------------------
      return(in_byte xor gf_mult_2(in_byte));
      
   end;

   ----------------------------------------------------------------------------
   -- Block to vector conversions
   -- As per our LTO GCM endianism spec
   ----------------------------------------------------------------------------
   function block_to_vec (
      signal block_in : block_t)
      return block_vector_t is
      
      variable vec_out : block_vector_t;
   begin
      vec_out(127 downto 120) := block_in(0)(0);
      vec_out(119 downto 112) := block_in(0)(1);
      vec_out(111 downto 104) := block_in(0)(2);
      vec_out(103 downto  96) := block_in(0)(3);
      vec_out( 95 downto  88) := block_in(1)(0);
      vec_out( 87 downto  80) := block_in(1)(1);
      vec_out( 79 downto  72) := block_in(1)(2);
      vec_out( 71 downto  64) := block_in(1)(3);
      vec_out( 63 downto  56) := block_in(2)(0);
      vec_out( 55 downto  48) := block_in(2)(1);
      vec_out( 47 downto  40) := block_in(2)(2);
      vec_out( 39 downto  32) := block_in(2)(3);
      vec_out( 31 downto  24) := block_in(3)(0);
      vec_out( 23 downto  16) := block_in(3)(1);
      vec_out( 15 downto   8) := block_in(3)(2);
      vec_out(  7 downto   0) := block_in(3)(3);

      return(vec_out);
   end;      
   
   function vec_to_block (
      signal vec_in : block_vector_t)
      return block_t is

      variable block_out : block_t;
   begin
      block_out(0)(0) := vec_in(127 downto 120);
      block_out(0)(1) := vec_in(119 downto 112);
      block_out(0)(2) := vec_in(111 downto 104);
      block_out(0)(3) := vec_in(103 downto  96);
      block_out(1)(0) := vec_in( 95 downto  88);
      block_out(1)(1) := vec_in( 87 downto  80);
      block_out(1)(2) := vec_in( 79 downto  72);
      block_out(1)(3) := vec_in( 71 downto  64);
      block_out(2)(0) := vec_in( 63 downto  56);
      block_out(2)(1) := vec_in( 55 downto  48);
      block_out(2)(2) := vec_in( 47 downto  40);
      block_out(2)(3) := vec_in( 39 downto  32);
      block_out(3)(0) := vec_in( 31 downto  24);
      block_out(3)(1) := vec_in( 23 downto  16);
      block_out(3)(2) := vec_in( 15 downto   8);
      block_out(3)(3) := vec_in(  7 downto   0);

      return(block_out);
   end;
   
   ----------------------------------------------------------------------------
   -- Function to do xor of aes blocks
   ----------------------------------------------------------------------------
   function xor_aes_block (
      signal block1 : block_t;
      signal block2  : block_t
      )
      return block_t is

      variable retval : block_t;
      
   begin
      for w in block_t'range loop
         for b in word_t'range loop
            retval(w)(b) := block1(w)(b) xor block2(w)(b);
         end loop;
      end loop;

      return(retval);
      
   end;
         
                           
END;
--######################################################################--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

LIBRARY WORK;
use WORK.aes_package.all;

PACKAGE crypt_package IS

   ----------------------------------------------------------------------------
   -- General AES block type
   ----------------------------------------------------------------------------
   subtype block_bytes_t is unsigned(4 downto 0);  -- allows upto 16!
   constant AES_BLOCK_BYTES : block_bytes_t := to_unsigned(16, block_bytes_t'length);
   
   ----------------------------------------------------------------------------
   -- Counter field types
   ----------------------------------------------------------------------------
   subtype ctr_iv_t        is std_logic_vector(95 downto 0);
   subtype ctr_record_t    is std_logic_vector(47 downto 0);
   subtype ctr_aes_block_t is std_logic_vector(31 downto 0);
   subtype ctr_bit_index_t is integer range 0 to block_vector_t'high;

   ----------------------------------------------------------------------------
   -- Special array type to aid synthesis of ghash_mult
   ----------------------------------------------------------------------------
   type ghash_block_array_t is array (0 to 127) of block_vector_t;

   ----------------------------------------------------------------------------
   -- Type for number of blocks in a record
   -- Max record len is 24 bits. But compressor can expand this, and of course
   -- CRC is added and EOR is padde to 32 bits. So add an extra bit (double size).
   -- Then subtract 4 bits cos there are 16 bytes per block
   ----------------------------------------------------------------------------
   subtype record_blocks_t is unsigned(23+1-4 downto 0);
   
   ----------------------------------------------------------------------------
   -- Extra Data types
   ----------------------------------------------------------------------------
   constant MAX_ED_LEN : integer := 4;  -- Number of 128 bit ED words
   subtype ed_num_t is integer range 0 to MAX_ED_LEN;
   type    ed_t     is array (0 to MAX_ED_LEN-1) of block_vector_t;  -- array of ED

   ----------------------------------------------------------------------------
   -- Function to swap bytes: leftmost goes to right most etc
   ----------------------------------------------------------------------------
   function byte_swap_slv (
      signal bytes_in : std_logic_vector)
      return std_logic_vector;
   
   function byte_swap_uns (
      signal bytes_in : unsigned)
      return unsigned;

   ----------------------------------------------------------------------------
   -- Function to assign a particular bit slice from a signal msb.
   --
   -- This has to be done in a for loop since we cannot synthesise
   -- somthing like slice <= vec_in(msb_sig downto lsb_sig.
   -- 
   -- ASSUMPTION: vec_in'length is integer multiple of slice_len.
   -- ASSUMPTION: slice is aligned at slice_len intervals, i.e. msb is one of
   --                slice_len-1, slice_len*2-1, ... vec_in'high
   ----------------------------------------------------------------------------
   subtype msb_t is integer range 0 to block_vector_t'high;

   function get_slice (
      signal   vec_in    : std_logic_vector;  -- obtain slice from this vector
      signal   msb       : msb_t;           -- required msb
      constant slice_len : natural)           -- length of slice
      return std_logic_vector;
   
   ----------------------------------------------------------------------------
   -- Procedure to assign a particular bit slice from a signal msb.
   --
   -- This has to be done in a for loop since we cannot synthesise
   -- somthing like vec_out(msb_sig downto lsb_sig) <= slice.
   -- 
   -- ASSUMPTION: vec_out'length is integer multiple of slice_len.
   -- ASSUMPTION: slice is aligned at slice_len intervals, i.e. msb is one of
   --                slice_len-1, slice_len*2-1, ... vec_in'high
   ----------------------------------------------------------------------------
   procedure assign_slice (
      signal   vec_out : inout std_logic_vector;  -- write slice to this vector
      variable msb     : in    natural;           -- required msb
      signal   slice   : in    std_logic_vector); -- input slice to copy from

   ----------------------------------------------------------------------------
   -- General purpose function to swap bytes in a bit slice
   -- Bytes are swapped MSB<->LSB etc
   ----------------------------------------------------------------------------
   function swap_bytes (
      signal in_slice : std_logic_vector)
      return std_logic_vector;
   

END crypt_package;
--######################################################################--
PACKAGE body crypt_package IS

   
   ----------------------------------------------------------------------------
   -- Function to swap bytes: leftmost goes to right most etc
   ----------------------------------------------------------------------------
   function byte_swap_slv (
      signal bytes_in : std_logic_vector)
      return std_logic_vector is

      constant VECLEN    : integer := bytes_in'length;  -- this is how std_numeric does this sort of thing
      constant NUMBYTES  : integer := (bytes_in'length)/8;
      variable j         : natural;
      variable b_out, b_in : std_logic_vector(bytes_in'length-1 downto 0);
   begin

      b_in := bytes_in;
      
      for i in 0 to NUMBYTES-1 loop
         j := NUMBYTES -1 -i;
         b_out(7+i*8 downto i*8) := b_in(7+j*8 downto j*8);
      end loop;  -- i

      return(b_out);
      
   end;

   function byte_swap_uns(
      signal bytes_in : unsigned)
      return unsigned is

      constant VECLEN    : integer := bytes_in'length;  -- this is how std_numeric does this sort of thing
      constant NUMBYTES  : integer := (bytes_in'length)/8;
      variable j         : natural;
      variable b_out, b_in : unsigned(bytes_in'length-1 downto 0);
   begin

      b_in := bytes_in;
      
      for i in 0 to NUMBYTES-1 loop
         j := NUMBYTES -1 -i;
         b_out(7+i*8 downto i*8) := b_in(7+j*8 downto j*8);
      end loop;  -- i

      return(b_out);
      
   end;

   ----------------------------------------------------------------------------
   -- Function to assign a particular bit slice from a signal msb.
   --
   -- This has to be done in a for loop since we cannot synthesise
   -- somthing like slice <= vec_in(msb_sig downto lsb_sig.
   -- 
   -- ASSUMPTION: vec_in'length is integer multiple of slice_len.
   -- ASSUMPTION: slice is aligned at slice_len intervals, i.e. msb is one of
   --                slice_len-1, slice_len*2-1, ... vec_in'high
   ----------------------------------------------------------------------------
   function get_slice (
      signal   vec_in    : std_logic_vector;  -- obtain slice from this vector
      signal   msb       : msb_t;           -- required msb
      constant slice_len : natural)           -- length of slice
      return std_logic_vector is

      variable slice_v   : std_logic_vector(slice_len-1 downto 0);
      variable msb_v     : natural;
      variable lsb_v     : natural;
      -- coverage off
      -- synopsys translate_off
      variable found_msb : boolean := false;
      -- synopsys translate_on
      -- coverage on
         
   begin  -- get_slice
      
      -- coverage off
      -- synopsys translate_off
      assert msb <= vec_in'high report "ERROR: get_slice: msb too big" severity FAILURE;
      -- synopsys translate_on
      -- coverage on
      
      for i in 1 to (vec_in'length)/slice_len loop
         msb_v :=  i    * slice_len - 1;
         lsb_v := (i-1) * slice_len;
         if msb_v = msb then
            slice_v   := vec_in(msb_v downto lsb_v);
            -- coverage off
            -- synopsys translate_off
            found_msb := true;
            -- synopsys translate_on
            -- coverage on
         end if;
      end loop;

      -- coverage off
      -- synopsys translate_off
      assert found_msb report "ERROR: get_slice: cannot find matching msb" severity FAILURE;
      -- synopsys translate_on
      -- coverage on
      
      return slice_v;
      
   end get_slice;
   
   ----------------------------------------------------------------------------
   -- Procedure to assign a particular bit slice from a signal msb.
   --
   -- This has to be done in a for loop since we cannot synthesise
   -- somthing like vec_out(msb_sig downto lsb_sig) <= slice.
   -- 
   -- ASSUMPTION: vec_out'length is integer multiple of slice_len.
   -- ASSUMPTION: slice is aligned at slice_len intervals, i.e. msb is one of
   --                slice_len-1, slice_len*2-1, ... vec_in'high
   ----------------------------------------------------------------------------
   procedure assign_slice (
      signal vec_out : inout std_logic_vector;  -- write slice to this vector
      variable msb   : in    natural;           -- required msb
      signal slice   : in    std_logic_vector)  -- input slice to copy from
      is

      constant VEC_LEN   : natural := vec_out'length;
      constant SLICE_LEN : natural :=   slice'length;
      variable msb_v     : natural;
      variable lsb_v     : natural;
      -- coverage off
      -- synopsys translate_off
      variable found_msb : boolean := false;
      -- synopsys translate_on
      -- coverage on
         
   begin  -- get_slice
      
      -- coverage off
      -- synopsys translate_off
      assert msb < VEC_LEN report "ERROR: assign_slice: msb too big" severity FAILURE;
      -- synopsys translate_on
      -- coverage on
      
      for i in 1 to (VEC_LEN)/SLICE_LEN loop
         msb_v :=  i    * SLICE_LEN - 1;
         lsb_v := (i-1) * SLICE_LEN;
         if msb_v = msb then
            vec_out(msb_v downto lsb_v) <= slice;
            -- coverage off
            -- synopsys translate_off
            found_msb := true;
            -- synopsys translate_on
            -- coverage on
         end if;
      end loop;

      -- coverage off
      -- synopsys translate_off
      assert found_msb report "ERROR: assign_slice: cannot find matching msb" severity FAILURE;
      -- synopsys translate_on
      -- coverage on
      
   end assign_slice;

   ----------------------------------------------------------------------------
   -- General purpose function to swap bytes in a bit slice
   -- Bytes are swapped MSB<->LSB etc
   ----------------------------------------------------------------------------
   function swap_bytes (
      signal in_slice : std_logic_vector)
      return std_logic_vector is

      constant SLICE_LENGTH: integer := in_slice'length;
      constant SLICE_HIGH  : integer := SLICE_LENGTH-1;
      variable out_slice_v : std_logic_vector(SLICE_HIGH downto 0);
      variable in_msb_v    : integer range 0 to SLICE_HIGH;
      variable out_msb_v   : integer range 0 to SLICE_HIGH;
      
   begin  -- swap_bytes
      
      for byte_in in 0 to (SLICE_LENGTH/8) -1 loop
         
         in_msb_v  := byte_in*8 +7;           -- checked OK for Formality (FMR_ELAB-149)
         out_msb_v := SLICE_HIGH - byte_in*8; -- checked OK for Formality (FMR_ELAB-149)
         
         out_slice_v(out_msb_v downto out_msb_v-7) := in_slice(in_msb_v downto in_msb_v-7);
         
      end loop;  -- byte_in

      return out_slice_v;
      
   end swap_bytes;

END;
-- VHDL Entity crypt_lib.aes.symbol
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:37 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;

ENTITY hp_bristol IS
   PORT( 
      Nsysrst          : IN     std_logic;
      cipher_ready     : IN     std_logic;
      clear_pipeline   : IN     std_logic;
      go               : IN     std_logic;
      key              : IN     key_t;
      plaintext_valid  : IN     std_logic;
      plaintext_vec_in : IN     block_vector_t;
      sysclk           : IN     std_logic;
      cipher_valid     : BUFFER std_logic;
      cipher_vec_out   : BUFFER block_vector_t;
      key_valid        : BUFFER std_logic;
      plaintext_ready  : BUFFER std_logic
   );

-- Declarations

END hp_bristol ;

-- VHDL Entity crypt_lib.aes_round.symbol
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:38 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;

ENTITY aes_round IS
   PORT( 
      Nsysrst         : IN     std_logic;
      cipher_ready    : IN     std_logic;
      clear_pipeline  : IN     std_logic;
      go              : IN     std_logic;
      key_valid       : IN     std_logic;
      num_rounds      : IN     num_rounds_t;
      plaintext       : IN     block_t;
      plaintext_valid : IN     std_logic;
      round           : IN     integer RANGE 0 TO MAX_NUM_ROUNDS;
      round_key       : IN     round_key_t;
      sysclk          : IN     std_logic;
      cipher          : BUFFER block_t;
      cipher_valid    : BUFFER std_logic;
      plaintext_ready : BUFFER std_logic
   );

-- Declarations

END aes_round ;

--
-- VHDL Architecture crypt_lib.aes_round.struct
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:38 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;


ARCHITECTURE struct OF aes_round IS

   -- Architecture declarations

   -- Internal signal declarations
   SIGNAL sbox_input  : sbox_word_t;
   SIGNAL sbox_result : sbox_word_t;


   -- Component Declarations
   COMPONENT encrypt_sm
   PORT (
      Nsysrst         : IN     std_logic ;
      cipher_ready    : IN     std_logic ;
      clear_pipeline  : IN     std_logic ;
      go              : IN     std_logic ;
      key_valid       : IN     std_logic ;
      num_rounds      : IN     num_rounds_t ;
      plaintext       : IN     block_t ;
      plaintext_valid : IN     std_logic ;
      round           : IN     integer RANGE 0 TO MAX_NUM_ROUNDS;
      round_key       : IN     block_t ;
      sbox_result     : IN     sbox_word_t ;
      sysclk          : IN     std_logic ;
      cipher          : BUFFER block_t ;
      cipher_valid    : BUFFER std_logic ;
      plaintext_ready : BUFFER std_logic ;
      sbox_input      : BUFFER sbox_word_t 
   );
   END COMPONENT;
   COMPONENT s_box
   PORT (
      sbox_input  : IN     sbox_word_t ;
      sbox_result : BUFFER sbox_word_t 
   );
   END COMPONENT;


BEGIN

   -- Instance port mappings.
   encrypt_sm_inst : encrypt_sm
      PORT MAP (
         Nsysrst         => Nsysrst,
         cipher_ready    => cipher_ready,
         clear_pipeline  => clear_pipeline,
         go              => go,
         key_valid       => key_valid,
         num_rounds      => num_rounds,
         plaintext       => plaintext,
         plaintext_valid => plaintext_valid,
         round           => round,
         round_key       => round_key,
         sbox_result     => sbox_result,
         sysclk          => sysclk,
         cipher          => cipher,
         cipher_valid    => cipher_valid,
         plaintext_ready => plaintext_ready,
         sbox_input      => sbox_input
      );
   s_box_inst : s_box
      PORT MAP (
         sbox_input  => sbox_input,
         sbox_result => sbox_result
      );

END struct;
--
-- VHDL Architecture crypt_lib.aes.struct
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:38 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;


ARCHITECTURE struct OF hp_bristol IS

   -- Architecture declarations

   -- Internal signal declarations
   SIGNAL num_rounds         : num_rounds_t;
   SIGNAL plaintext_block_in : block_t;
   SIGNAL round              : int_array_t(1 TO MAX_NUM_ROUNDS);
   SIGNAL round_key4_blocks  : round_block_t(1 TO NUM_ROUNDS_KEY_4 +1);
   SIGNAL round_key4_ready   : std_logic_vector(1 TO NUM_ROUNDS_KEY_4 +1);
   SIGNAL round_key4_valid   : std_logic_vector(1 TO NUM_ROUNDS_KEY_4 +1);
   SIGNAL round_key8_blocks  : round_block_t(NUM_ROUNDS_KEY_4+1 TO NUM_ROUNDS_KEY_8+1);
   SIGNAL round_key8_ready   : std_logic_vector(NUM_ROUNDS_KEY_4+1 TO NUM_ROUNDS_KEY_8+1);
   SIGNAL round_key8_valid   : std_logic_vector(NUM_ROUNDS_KEY_4+1 TO NUM_ROUNDS_KEY_8+1);
   SIGNAL round_keys         : round_keys_t;


   -- Component Declarations
   COMPONENT aes_round
   PORT (
      Nsysrst         : IN     std_logic ;
      cipher_ready    : IN     std_logic ;
      clear_pipeline  : IN     std_logic ;
      go              : IN     std_logic ;
      key_valid       : IN     std_logic ;
      num_rounds      : IN     num_rounds_t ;
      plaintext       : IN     block_t ;
      plaintext_valid : IN     std_logic ;
      round           : IN     integer RANGE 0 TO MAX_NUM_ROUNDS;
      round_key       : IN     round_key_t ;
      sysclk          : IN     std_logic ;
      cipher          : BUFFER block_t ;
      cipher_valid    : BUFFER std_logic ;
      plaintext_ready : BUFFER std_logic 
   );
   END COMPONENT;
   COMPONENT encrypt_round_0_sm
   PORT (
      Nsysrst         : IN     std_logic ;
      cipher_ready    : IN     std_logic ;
      clear_pipeline  : IN     std_logic ;
      go              : IN     std_logic ;
      key_valid       : IN     std_logic ;
      plaintext       : IN     block_t ;
      plaintext_valid : IN     std_logic ;
      round_key       : IN     round_key_t ;
      sysclk          : IN     std_logic ;
      cipher          : BUFFER block_t ;
      cipher_valid    : BUFFER std_logic ;
      plaintext_ready : BUFFER std_logic 
   );
   END COMPONENT;
   COMPONENT key_scheduler
   PORT (
      Nsysrst    : IN     std_logic ;
      go         : IN     std_logic ;
      key        : IN     key_t ;
      sysclk     : IN     std_logic ;
      key_valid  : BUFFER std_logic ;
      round_keys : BUFFER round_keys_t 
   );
   END COMPONENT;


BEGIN
   -- Architecture concurrent statements
   -- HDL Embedded Text Block 1 eb1
   -- eb1 1     
   plaintext_block_in <= vec_to_block(plaintext_vec_in);

   -- HDL Embedded Text Block 2 eb2
   -- cipher outputs
   cipher_valid   <= round_key4_valid(round_key4_valid'high)                           when num_rounds = NUM_ROUNDS_KEY_4 else
                     round_key8_valid(round_key8_valid'high);
   cipher_vec_out <= block_to_vec(round_key4_blocks(round_key4_blocks'high))           when num_rounds = NUM_ROUNDS_KEY_4 else
                     block_to_vec(round_key8_blocks(round_key8_blocks'high));
   
   -- signals between key4 and key8 blocks
   round_key4_ready(round_key4_ready'high)  <= cipher_ready                            when num_rounds = NUM_ROUNDS_KEY_4 else
                                               round_key8_ready(round_key8_ready'low);
   round_key8_valid(round_key8_valid'low)   <= round_key4_valid(round_key4_valid'high) when num_rounds = NUM_ROUNDS_KEY_8 else
                                               '0';
   round_key8_blocks(round_key8_blocks'low) <= round_key4_blocks(round_key4_blocks'high);
   
   -- topmost key8 ready input
   round_key8_ready(round_key8_ready'high)  <= cipher_ready;

   -- HDL Embedded Text Block 3 eb4
   -- eb4 3
   -- Have to do this cos synopsys
   -- demands a signal or port
   -- for connections to components
   round( 1) <=  1;
   round( 2) <=  2;
   round( 3) <=  3;
   round( 4) <=  4;
   round( 5) <=  5;
   round( 6) <=  6;
   round( 7) <=  7;
   round( 8) <=  8;
   round( 9) <=  9;
   round(10) <= 10;
   round(11) <= 11;
   round(12) <= 12;
   round(13) <= 13;
   round(14) <= 14;
   

   -- HDL Embedded Text Block 4 eb3
   -- eb3 4      
   num_rounds <= key.num_rounds;


   -- Instance port mappings.
   round_0_sm_inst : encrypt_round_0_sm
      PORT MAP (
         Nsysrst         => Nsysrst,
         cipher_ready    => round_key4_ready(1),
         clear_pipeline  => clear_pipeline,
         go              => go,
         key_valid       => key_valid,
         plaintext       => plaintext_block_in,
         plaintext_valid => plaintext_valid,
         round_key       => round_keys(0),
         sysclk          => sysclk,
         cipher          => round_key4_blocks(1),
         cipher_valid    => round_key4_valid(1),
         plaintext_ready => plaintext_ready
      );
   key_scheduler_inst : key_scheduler
      PORT MAP (
         Nsysrst    => Nsysrst,
         go         => go,
         key        => key,
         sysclk     => sysclk,
         key_valid  => key_valid,
         round_keys => round_keys
      );

   -- THIS IS THE GENERATE THAT TRIGGERED BUG15153
   -- Both inputs and outputs are tested
   g0: FOR i IN 1 TO NUM_ROUNDS_KEY_4 GENERATE
      round_128_inst : aes_round
         PORT MAP (
            Nsysrst => Nsysrst,
            sysclk => sysclk,
            go => go,
            clear_pipeline => clear_pipeline,
            round => round(i),
            key_valid => key_valid,
            round_key => round_keys(i),
            num_rounds => num_rounds,
            plaintext_ready => round_key4_ready(i),
            plaintext_valid => round_key4_valid(i),
            plaintext => round_key4_blocks(i), -- INPUT
            cipher_ready => round_key4_ready(i+1),
            cipher_valid => round_key4_valid(i+1),
            cipher => round_key4_blocks(i+1) -- OUTPUT
         );
   END GENERATE g0;

   g1: FOR i IN NUM_ROUNDS_KEY_4+1 TO NUM_ROUNDS_KEY_8 GENERATE
      round_256_inst : aes_round
         PORT MAP (
            Nsysrst => Nsysrst,
            sysclk => sysclk,
            clear_pipeline => clear_pipeline,
            go => go,
            round => round(i),
            round_key => round_keys(i),
            key_valid => key_valid,
            num_rounds => num_rounds,
            plaintext_ready => round_key8_ready(i),
            plaintext_valid => round_key8_valid(i),
            plaintext => round_key8_blocks(i),
            cipher_ready => round_key8_ready(i+1),
            cipher_valid => round_key8_valid(i+1),
            cipher => round_key8_blocks(i+1)
         );
   END GENERATE g1;

END struct;
-- VHDL Entity crypt_lib.crypt.symbol
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 16:01:17 02/07/11
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;
USE WORK.crypt_package.ALL;

ENTITY crypt IS
   PORT( 
      Nsysrst          : IN     std_logic ;
      cipher_ready     : IN     std_logic ;
      clear_pipeline   : IN     std_logic ;
      go               : IN     std_logic ;
      key              : IN     key_t ;
      plaintext_valid  : IN     std_logic ;
      plaintext_vec_in : IN     block_vector_t ;
      sysclk           : IN     std_logic ;
      cipher_valid     : BUFFER std_logic ;
      cipher_vec_out   : BUFFER block_vector_t ;
      key_valid        : BUFFER std_logic ;
      plaintext_ready  : BUFFER std_logic
   );

END crypt ;

--
-- VHDL Architecture crypt_lib.crypt.struct
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 16:01:18 02/07/11
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;
USE WORK.crypt_package.ALL;


ARCHITECTURE struct OF crypt IS

   -- Component Declarations
   COMPONENT aes
   PORT (
      Nsysrst          : IN     std_logic ;
      cipher_ready     : IN     std_logic ;
      clear_pipeline   : IN     std_logic ;
      go               : IN     std_logic ;
      key              : IN     key_t ;
      plaintext_valid  : IN     std_logic ;
      plaintext_vec_in : IN     block_vector_t ;
      sysclk           : IN     std_logic ;
      cipher_valid     : BUFFER std_logic ;
      cipher_vec_out   : BUFFER block_vector_t ;
      key_valid        : BUFFER std_logic ;
      plaintext_ready  : BUFFER std_logic 
   );
   END COMPONENT;

BEGIN
   -- Instance port mappings.
   aes_inst : aes
      PORT MAP (
         Nsysrst          => Nsysrst,
         cipher_ready     => cipher_ready,
         clear_pipeline   => clear_pipeline,
         go               => go,
         key              => key,
         plaintext_valid  => plaintext_valid,
         plaintext_vec_in => plaintext_vec_in,
         sysclk           => sysclk,
         cipher_valid     => cipher_valid,
         cipher_vec_out   => cipher_vec_out,
         key_valid        => key_valid,
         plaintext_ready  => plaintext_ready
      );

END struct;
-- VHDL Entity crypt_lib.encrypt_round_0_sm.symbol
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:38 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;

ENTITY encrypt_round_0_sm IS
   PORT( 
      Nsysrst         : IN     std_logic;
      cipher_ready    : IN     std_logic;
      clear_pipeline  : IN     std_logic;
      go              : IN     std_logic;
      key_valid       : IN     std_logic;
      plaintext       : IN     block_t;
      plaintext_valid : IN     std_logic;
      round_key       : IN     round_key_t;
      sysclk          : IN     std_logic;
      cipher          : BUFFER block_t;
      cipher_valid    : BUFFER std_logic;
      plaintext_ready : BUFFER std_logic
   );

-- Declarations

END encrypt_round_0_sm ;

--
-- VHDL Architecture crypt_lib.encrypt_round_0_sm.fsm
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:38 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;
 
ARCHITECTURE fsm OF encrypt_round_0_sm IS

   TYPE STATE_TYPE IS (
      idle,
      do_addRoundKey,
      ready,
      cipher_done
   );
 
   -- State vector declaration
   ATTRIBUTE state_vector : string;
   ATTRIBUTE state_vector OF fsm : ARCHITECTURE IS "current_state";

   -- Declare current and next state signals
   SIGNAL current_state : STATE_TYPE;
   SIGNAL next_state : STATE_TYPE;

   -- Declare any pre-registered internal signals
   SIGNAL cipher_cld : block_t ;
   SIGNAL cipher_valid_cld : std_logic ;
   SIGNAL plaintext_ready_cld : std_logic ;

BEGIN

   -----------------------------------------------------------------
   clocked_proc : PROCESS ( 
      sysclk,
      Nsysrst
   )
   -----------------------------------------------------------------
   BEGIN
      IF (Nsysrst = '0') THEN
         current_state <= idle;
         -- Default Reset Values
         cipher_cld <= (others => (others => (others => '1')));
         cipher_valid_cld <= '0';
         plaintext_ready_cld <= '0';
      ELSIF (sysclk'EVENT AND sysclk = '1') THEN
         current_state <= next_state;

         -- Combined Actions
         CASE current_state IS
            WHEN idle => 
               IF ((go = '1') and (key_valid = '1')) THEN 
                  plaintext_ready_cld <= '1';
               END IF;
            WHEN do_addRoundKey => 
               for word in block_t'range loop
                  for byte in 0 to 3 loop
                     cipher_cld(word)(byte) <= cipher_cld(word)(byte) xor round_key(word)(byte);
                  end loop;
               end loop;
               cipher_valid_cld <= '1';
            WHEN ready => 
               IF (plaintext_valid = '1') THEN 
                  plaintext_ready_cld <= '0';
                  cipher_cld          <= plaintext;
               END IF;
            WHEN cipher_done => 
               IF (cipher_ready = '1') THEN 
                  cipher_valid_cld    <= '0';
                  plaintext_ready_cld <= '1';
               END IF;
            WHEN OTHERS =>
               NULL;
         END CASE;

         -- Interrupts
         IF ((go = '0') or (clear_pipeline = '1')) THEN
            plaintext_ready_cld <= '0';
            cipher_valid_cld    <= '0';
         END IF;
      END IF;
   END PROCESS clocked_proc;
 
   -----------------------------------------------------------------
   nextstate_proc : PROCESS ( 
      cipher_ready,
      clear_pipeline,
      current_state,
      go,
      key_valid,
      plaintext_valid
   )
   -----------------------------------------------------------------
   BEGIN

      -- Combined Actions
      CASE current_state IS
         WHEN idle => 
            IF ((go = '1') and (key_valid = '1')) THEN 
               next_state <= ready;
            ELSE
               next_state <= idle;
            END IF;
         WHEN do_addRoundKey => 
            next_state <= cipher_done;
         WHEN ready => 
            IF (plaintext_valid = '1') THEN 
               next_state <= do_addRoundKey;
            ELSE
               next_state <= ready;
            END IF;
         WHEN cipher_done => 
            IF (cipher_ready = '1') THEN 
               next_state <= ready;
            ELSE
               next_state <= cipher_done;
            END IF;
         WHEN OTHERS =>
            next_state <= idle;
      END CASE;

      -- Interrupts
      IF ((go = '0') or (clear_pipeline = '1')) THEN
         next_state <= idle;
      END IF;
   END PROCESS nextstate_proc;
 
   -- Concurrent Statements
   -- Clocked output assignments
   cipher <= cipher_cld;
   cipher_valid <= cipher_valid_cld;
   plaintext_ready <= plaintext_ready_cld;
END fsm;
-- VHDL Entity crypt_lib.encrypt_sm.symbol
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:38 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;

ENTITY encrypt_sm IS
   PORT( 
      Nsysrst         : IN     std_logic;
      cipher_ready    : IN     std_logic;
      clear_pipeline  : IN     std_logic;
      go              : IN     std_logic;
      key_valid       : IN     std_logic;
      num_rounds      : IN     num_rounds_t;
      plaintext       : IN     block_t;
      plaintext_valid : IN     std_logic;
      round           : IN     integer RANGE 0 TO MAX_NUM_ROUNDS;
      round_key       : IN     block_t;
      sbox_result     : IN     sbox_word_t;
      sysclk          : IN     std_logic;
      cipher          : BUFFER block_t;
      cipher_valid    : BUFFER std_logic;
      plaintext_ready : BUFFER std_logic;
      sbox_input      : BUFFER sbox_word_t
   );

-- Declarations

END encrypt_sm ;

--
-- VHDL Architecture crypt_lib.encrypt_sm.fsm
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:38 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;
 
ARCHITECTURE fsm OF encrypt_sm IS

   -- Architecture Declarations
   signal word_count : integer range block_t'range;
   signal byte_count : integer range word_t'range;

   TYPE STATE_TYPE IS (
      idle,
      do_subbytes,
      do_shift,
      do_mixColumns,
      do_addRoundKey,
      ready,
      cipher_done
   );
 
   -- State vector declaration
   ATTRIBUTE state_vector : string;
   ATTRIBUTE state_vector OF fsm : ARCHITECTURE IS "current_state";

   -- Declare current and next state signals
   SIGNAL current_state : STATE_TYPE;
   SIGNAL next_state : STATE_TYPE;

   -- Declare any pre-registered internal signals
   SIGNAL cipher_cld : block_t ;
   SIGNAL cipher_valid_cld : std_logic ;
   SIGNAL plaintext_ready_cld : std_logic ;

BEGIN

   -----------------------------------------------------------------
   clocked_proc : PROCESS ( 
      sysclk,
      Nsysrst
   )
   -----------------------------------------------------------------
   BEGIN
      IF (Nsysrst = '0') THEN
         current_state <= idle;
         -- Default Reset Values
         cipher_cld <= (others => (others => (others => '1')));
         cipher_valid_cld <= '0';
         plaintext_ready_cld <= '0';
         byte_count <= 0;
         word_count <= 0;
      ELSIF (sysclk'EVENT AND sysclk = '1') THEN
         current_state <= next_state;

         -- Combined Actions
         CASE current_state IS
            WHEN idle => 
               IF ((go = '1') and (key_valid = '1')) THEN 
                  plaintext_ready_cld <= '1';
               END IF;
            WHEN do_subbytes => 
               IF ((word_count                    = block_t'high) and
                   (byte_count + sbox_word_t'high = word_t'high)) THEN 
                  for i in sbox_word_t'range loop
                     cipher_cld(word_count)(byte_count + i) <= sbox_result(i);
                  end loop;
               ELSE
                  for i in sbox_word_t'range loop
                     cipher_cld(word_count)(byte_count + i) <= sbox_result(i);
                  end loop;
                  if (byte_count + sbox_word_t'high = word_t'high) then
                     word_count <= word_count + 1;
                     byte_count <= 0;
                  else
                     byte_count <= byte_count + sbox_word_t'length;
                  end if;
               END IF;
            WHEN do_shift => 
               IF (round = num_rounds) THEN 
                  cipher_cld(0)(1) <= cipher_cld(1)(1);
                  cipher_cld(1)(1) <= cipher_cld(2)(1);
                  cipher_cld(2)(1) <= cipher_cld(3)(1);
                  cipher_cld(3)(1) <= cipher_cld(0)(1);
                  
                  cipher_cld(0)(2) <= cipher_cld(2)(2);
                  cipher_cld(1)(2) <= cipher_cld(3)(2);
                  cipher_cld(2)(2) <= cipher_cld(0)(2);
                  cipher_cld(3)(2) <= cipher_cld(1)(2);
                  
                  cipher_cld(0)(3) <= cipher_cld(3)(3);
                  cipher_cld(1)(3) <= cipher_cld(0)(3);
                  cipher_cld(2)(3) <= cipher_cld(1)(3);
                  cipher_cld(3)(3) <= cipher_cld(2)(3);
               ELSE
                  cipher_cld(0)(1) <= cipher_cld(1)(1);
                  cipher_cld(1)(1) <= cipher_cld(2)(1);
                  cipher_cld(2)(1) <= cipher_cld(3)(1);
                  cipher_cld(3)(1) <= cipher_cld(0)(1);
                  
                  cipher_cld(0)(2) <= cipher_cld(2)(2);
                  cipher_cld(1)(2) <= cipher_cld(3)(2);
                  cipher_cld(2)(2) <= cipher_cld(0)(2);
                  cipher_cld(3)(2) <= cipher_cld(1)(2);
                  
                  cipher_cld(0)(3) <= cipher_cld(3)(3);
                  cipher_cld(1)(3) <= cipher_cld(0)(3);
                  cipher_cld(2)(3) <= cipher_cld(1)(3);
                  cipher_cld(3)(3) <= cipher_cld(2)(3);
               END IF;
            WHEN do_mixColumns => 
               -- Sadly, don't seem to be able to do this as a for loop
               -- since function signal params have to be static names.
               cipher_cld(0)(0) <= gf_mult_2(cipher_cld(0)(0)) xor gf_mult_3(cipher_cld(0)(1)) xor           cipher_cld(0)(2)  xor           cipher_cld(0)(3) ;
               cipher_cld(0)(1) <=           cipher_cld(0)(0)  xor gf_mult_2(cipher_cld(0)(1)) xor gf_mult_3(cipher_cld(0)(2)) xor           cipher_cld(0)(3) ;
               cipher_cld(0)(2) <=           cipher_cld(0)(0)  xor           cipher_cld(0)(1)  xor gf_mult_2(cipher_cld(0)(2)) xor gf_mult_3(cipher_cld(0)(3));
               cipher_cld(0)(3) <= gf_mult_3(cipher_cld(0)(0)) xor           cipher_cld(0)(1)  xor           cipher_cld(0)(2)  xor gf_mult_2(cipher_cld(0)(3));
               cipher_cld(1)(0) <= gf_mult_2(cipher_cld(1)(0)) xor gf_mult_3(cipher_cld(1)(1)) xor           cipher_cld(1)(2)  xor           cipher_cld(1)(3) ;
               cipher_cld(1)(1) <=           cipher_cld(1)(0)  xor gf_mult_2(cipher_cld(1)(1)) xor gf_mult_3(cipher_cld(1)(2)) xor           cipher_cld(1)(3) ;
               cipher_cld(1)(2) <=           cipher_cld(1)(0)  xor           cipher_cld(1)(1)  xor gf_mult_2(cipher_cld(1)(2)) xor gf_mult_3(cipher_cld(1)(3));
               cipher_cld(1)(3) <= gf_mult_3(cipher_cld(1)(0)) xor           cipher_cld(1)(1)  xor           cipher_cld(1)(2)  xor gf_mult_2(cipher_cld(1)(3));
               cipher_cld(2)(0) <= gf_mult_2(cipher_cld(2)(0)) xor gf_mult_3(cipher_cld(2)(1)) xor           cipher_cld(2)(2)  xor           cipher_cld(2)(3) ;
               cipher_cld(2)(1) <=           cipher_cld(2)(0)  xor gf_mult_2(cipher_cld(2)(1)) xor gf_mult_3(cipher_cld(2)(2)) xor           cipher_cld(2)(3) ;
               cipher_cld(2)(2) <=           cipher_cld(2)(0)  xor           cipher_cld(2)(1)  xor gf_mult_2(cipher_cld(2)(2)) xor gf_mult_3(cipher_cld(2)(3));
               cipher_cld(2)(3) <= gf_mult_3(cipher_cld(2)(0)) xor           cipher_cld(2)(1)  xor           cipher_cld(2)(2)  xor gf_mult_2(cipher_cld(2)(3));
               cipher_cld(3)(0) <= gf_mult_2(cipher_cld(3)(0)) xor gf_mult_3(cipher_cld(3)(1)) xor           cipher_cld(3)(2)  xor           cipher_cld(3)(3) ;
               cipher_cld(3)(1) <=           cipher_cld(3)(0)  xor gf_mult_2(cipher_cld(3)(1)) xor gf_mult_3(cipher_cld(3)(2)) xor           cipher_cld(3)(3) ;
               cipher_cld(3)(2) <=           cipher_cld(3)(0)  xor           cipher_cld(3)(1)  xor gf_mult_2(cipher_cld(3)(2)) xor gf_mult_3(cipher_cld(3)(3));
               cipher_cld(3)(3) <= gf_mult_3(cipher_cld(3)(0)) xor           cipher_cld(3)(1)  xor           cipher_cld(3)(2)  xor gf_mult_2(cipher_cld(3)(3));
            WHEN do_addRoundKey => 
               for word in block_t'range loop
                  for byte in 0 to 3 loop
                     cipher_cld(word)(byte) <= cipher_cld(word)(byte) xor round_key(word)(byte);
                  end loop;
               end loop;
               cipher_valid_cld <= '1';
            WHEN ready => 
               IF (plaintext_valid = '1') THEN 
                  plaintext_ready_cld <= '0';
                  cipher_cld <= plaintext;
                  word_count <= 0;
                  byte_count <= 0;
               END IF;
            WHEN cipher_done => 
               IF (cipher_ready = '1') THEN 
                  cipher_valid_cld    <= '0';
                  plaintext_ready_cld <= '1';
               END IF;
            WHEN OTHERS =>
               NULL;
         END CASE;

         -- Interrupts
         IF ((go = '0') or (clear_pipeline = '1')) THEN
            plaintext_ready_cld <= '0';
            cipher_valid_cld    <= '0';
         END IF;
      END IF;
   END PROCESS clocked_proc;
 
   -----------------------------------------------------------------
   nextstate_proc : PROCESS ( 
      byte_count,
      cipher_ready,
      clear_pipeline,
      current_state,
      go,
      key_valid,
      num_rounds,
      plaintext_valid,
      round,
      word_count
   )
   -----------------------------------------------------------------
   BEGIN

      -- Combined Actions
      CASE current_state IS
         WHEN idle => 
            IF ((go = '1') and (key_valid = '1')) THEN 
               next_state <= ready;
            ELSE
               next_state <= idle;
            END IF;
         WHEN do_subbytes => 
            IF ((word_count                    = block_t'high) and
                (byte_count + sbox_word_t'high = word_t'high)) THEN 
               next_state <= do_shift;
            ELSE
               next_state <= do_subbytes;
            END IF;
         WHEN do_shift => 
            IF (round = num_rounds) THEN 
               next_state <= do_addRoundKey;
            ELSE
               next_state <= do_mixColumns;
            END IF;
         WHEN do_mixColumns => 
            next_state <= do_addRoundKey;
         WHEN do_addRoundKey => 
            next_state <= cipher_done;
         WHEN ready => 
            IF (plaintext_valid = '1') THEN 
               next_state <= do_subbytes;
            ELSE
               next_state <= ready;
            END IF;
         WHEN cipher_done => 
            IF (cipher_ready = '1') THEN 
               next_state <= ready;
            ELSE
               next_state <= cipher_done;
            END IF;
         WHEN OTHERS =>
            next_state <= idle;
      END CASE;

      -- Interrupts
      IF ((go = '0') or (clear_pipeline = '1')) THEN
         next_state <= idle;
      END IF;
   END PROCESS nextstate_proc;
 
   -- Concurrent Statements
   -- Clocked output assignments
   cipher <= cipher_cld;
   cipher_valid <= cipher_valid_cld;
   plaintext_ready <= plaintext_ready_cld;
   -------------------------------------------------------------------------------
   -- Generate comb outputs to sbox component
   -------------------------------------------------------------------------------
   sbox_proc: process(cipher_cld, word_count, byte_count)
   begin  -- process sbox_proc
      for i  in sbox_word_t'range loop
         sbox_input(i) <= cipher_cld(word_count)(byte_count+i); 
      end loop;  -- i 
   end process sbox_proc;
END fsm;
-- VHDL Entity crypt_lib.key_scheduler.interface
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:37 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;

ENTITY key_scheduler IS
   PORT( 
      Nsysrst    : IN     std_logic;
      go         : IN     std_logic;
      key        : IN     key_t;
      sysclk     : IN     std_logic;
      key_valid  : BUFFER std_logic;
      round_keys : BUFFER round_keys_t
   );

-- Declarations

END key_scheduler ;

--
-- VHDL Architecture crypt_lib.key_scheduler.fsm
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:37 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;
 
ARCHITECTURE fsm OF key_scheduler IS

   -- Architecture Declarations
   subtype i_counter_t is integer range 0 to MAX_ROUND_KEY_WORDS                                      ;
   signal i_counter, i_minus_1, i_minus_Nk        : i_counter_t                                       ;
   signal word_minus_1, word_minus_Nk             : word_t                                            ;
   signal RotWord, Rcon, SubWord_RotWord_xor_Rcon : word_t                                            ;
   signal i_div_Nk                                : i_counter_t                                       ;
   signal temp                                    : word_t                                            ;
   signal i_counter_mod_key_length                : unsigned(MAX_KEY_EXP-1 downto 0)                  ;
   signal max_i_counter                           : i_counter_t                                       ;
   signal i_counter_vec                           : unsigned(MAX_ROUND_KEY_WORDS_BITS-1 downto 0)     ;
   constant BYTE_EQ_1                             : unsigned(7 downto 0) := (0 => '1', others => '0') ;
   signal word_to_sbox                            : word_t                                            ;
   signal sbox_word                               : word_t                                            ;
   signal byte_count                              : integer range 0 to word_t'high+1                  ;
   signal word_byte_index                         : integer range word_t'range                        ;
   signal sbox_byte_in                            : integer range 0 to 255                            ;
   signal do_special_word                         : std_logic                                         ;
   -- Signal for large array of round key words (as per aes spec psuedo code)
   signal round_key_words                         : word_array_t(0 to MAX_ROUND_KEY_WORDS-1)          ;
   signal expand_key                              : std_logic                                         ;
   signal prev_go                                 : std_logic                                         ;

   TYPE STATE_TYPE IS (
      idle,
      expanding_byte,
      done_key
   );
 
   -- State vector declaration
   ATTRIBUTE state_vector : string;
   ATTRIBUTE state_vector OF fsm : ARCHITECTURE IS "current_key_state";

   -- Declare current and next state signals
   SIGNAL current_key_state : STATE_TYPE;
   SIGNAL next_key_state : STATE_TYPE;

   -- Declare any pre-registered internal signals
   SIGNAL key_valid_cld : std_logic ;

BEGIN

   -----------------------------------------------------------------
   clocked_proc : PROCESS ( 
      sysclk,
      Nsysrst
   )
   -----------------------------------------------------------------
   BEGIN
      IF (Nsysrst = '0') THEN
         current_key_state <= idle;
         -- Default Reset Values
         key_valid_cld <= '0';
         byte_count <= 0;
         i_counter <= 0;
         prev_go <= '0';
         round_key_words <= (others => (others => (others => '0')));
         sbox_word <= (others => (others => '0'));
      ELSIF (sysclk'EVENT AND sysclk = '1') THEN
         current_key_state <= next_key_state;
         -- Default Assignment To Internals
         prev_go <= go;

         -- Combined Actions
         CASE current_key_state IS
            WHEN idle => 
               IF (expand_key = '1') THEN 
                  for i in 0 to MAX_KEY_LENGTH-1 loop
                     round_key_words(i) <= key.value(i);
                  end loop;
                  i_counter <= key.length;
                  key_valid_cld <= '0';
                  byte_count   <= 0;
               END IF;
            WHEN expanding_byte => 
               IF (((byte_count = 4) or (do_special_word = '0')) AND (i_counter = max_i_counter)) THEN 
                  key_valid_cld <= '1';
               ELSIF ((byte_count = 4) or (do_special_word = '0')) THEN 
                  round_key_words(i_counter)(0) <= word_minus_Nk(0) xor temp(0);
                  round_key_words(i_counter)(1) <= word_minus_Nk(1) xor temp(1);
                  round_key_words(i_counter)(2) <= word_minus_Nk(2) xor temp(2);
                  round_key_words(i_counter)(3) <= word_minus_Nk(3) xor temp(3);
                  i_counter                   <= i_counter + 1;
                  byte_count   <= 0;
               ELSE
                  sbox_word(word_byte_index) <= to_stdlogicvector(    sbox(sbox_byte_in));
                  byte_count                 <= byte_count + 1;
               END IF;
            WHEN OTHERS =>
               NULL;
         END CASE;

         -- Interrupts
         IF (go = '0') THEN
            key_valid_cld <= '0';
         END IF;
      END IF;
   END PROCESS clocked_proc;
 
   -----------------------------------------------------------------
   nextstate_proc : PROCESS ( 
      byte_count,
      current_key_state,
      do_special_word,
      expand_key,
      go,
      i_counter,
      max_i_counter
   )
   -----------------------------------------------------------------
   BEGIN

      -- Combined Actions
      CASE current_key_state IS
         WHEN idle => 
            IF (expand_key = '1') THEN 
               next_key_state <= expanding_byte;
            ELSE
               next_key_state <= idle;
            END IF;
         WHEN expanding_byte => 
            IF (((byte_count = 4) or (do_special_word = '0')) AND (i_counter = max_i_counter)) THEN 
               next_key_state <= done_key;
            ELSIF ((byte_count = 4) or (do_special_word = '0')) THEN 
               next_key_state <= expanding_byte;
            ELSE
               next_key_state <= expanding_byte;
            END IF;
         WHEN done_key => 
            next_key_state <= done_key;
         WHEN OTHERS =>
            next_key_state <= idle;
      END CASE;

      -- Interrupts
      IF (go = '0') THEN
         next_key_state <= idle;
      END IF;
   END PROCESS nextstate_proc;
 
   -- Concurrent Statements
   -- Clocked output assignments
   key_valid <= key_valid_cld;
   -------------------------------------------------------------------------------
   -- Generate expand_key when go changes from 0->1
   -------------------------------------------------------------------------------
   expand_key <= go and not prev_go;
   
   -------------------------------------------------------------------------------
   -- Work out intermediate values for Key Expansion
   -------------------------------------------------------------------------------
   i_minus_1        <= i_counter - 1          when i_counter > 0          else
                       0;
   i_minus_Nk       <= i_counter - key.length when i_counter > key.length else
                       0;
   word_minus_1     <= round_key_words(i_minus_1);
   word_minus_Nk    <= round_key_words(i_minus_Nk);
   
   RotWord          <= (word_minus_1(1), word_minus_1(2), word_minus_1(3), word_minus_1(0));
   
   -------------------------------------------------------------------------------
   -- RCON: Since these are round constants, just look them up
   -------------------------------------------------------------------------------
   -- only 2 values expected for key length                    
   i_div_Nk <= to_integer(SHIFT_RIGHT(i_counter_vec, 2)) when key.length_exp = 2 else
               to_integer(SHIFT_RIGHT(i_counter_vec, 3));
   
   -- Rcon only has byte(0) set to anything
   Rcon(1 to word_t'high) <= (others => (others => '0'));
   
   with i_div_Nk select
         Rcon(0) <= "00000001" when 1,
                    "00000010" when 2,
                    "00000100" when 3,
                    "00001000" when 4,
                    "00010000" when 5,
                    "00100000" when 6,
                    "01000000" when 7,
                    "10000000" when 8,
                    "00011011" when 9,
                    "00110110" when 10,
                    "00000000" when others;
   
   -------------------------------------------------------------------------------
   -- Work out SubWord(RotWord(temp))
   -------------------------------------------------------------------------------
   -- Set output to sbox according to round i_counter
   word_to_sbox    <= RotWord when i_counter_mod_key_length = 0 else
                      word_minus_1;
   word_byte_index <= byte_count when byte_count < word_t'length else
                       0;
   sbox_byte_in    <= to_integer(unsigned(word_to_sbox(word_byte_index)));
   
   -------------------------------------------------------------------------------
   -- SubWord() xor Rcon
   -------------------------------------------------------------------------------
   SubWord_RotWord_xor_Rcon <= (sbox_word(0) xor Rcon(0),
                                sbox_word(1) xor Rcon(1),
                                sbox_word(2) xor Rcon(2),
                                sbox_word(3) xor Rcon(3));
   
   -------------------------------------------------------------------------------
   -- Calculate 'temp' word as per AES spec
   -------------------------------------------------------------------------------
   temp <= SubWord_RotWord_xor_Rcon when i_counter_mod_key_length = 0                        else -- SubWord(RotWord(temp)) xor Rcon[i/Nk]TQ
           sbox_word                when (key.length = 8) and (i_counter_mod_key_length = 4) else -- SubWord(w[i-1])
           word_minus_1;
   do_special_word <= '1' when (i_counter_mod_key_length = 0) or ((key.length = 8) and (i_counter_mod_key_length = 4)) else
                      '0';
   
   -------------------------------------------------------------------------------
   -- Quick calculation of "i_counter mod key.length", using the knowledge
   -- that key.length will be 4 or 8 only
   -------------------------------------------------------------------------------
   i_counter_vec            <= to_unsigned(i_counter, i_counter_vec'length);                   
   i_counter_mod_key_length <=       i_counter_vec(2 downto 0) when key.length_exp = 3 else
                               '0' & i_counter_vec(1 downto 0);
   
   -------------------------------------------------------------------------------
   -- Maximum 'i' loop value for key expansion loops
   -------------------------------------------------------------------------------
   max_i_counter <= MAX_ROUND_KEY_WORDS_8 when key.length = 8 else
                    MAX_ROUND_KEY_WORDS_4;
   
                  
   -------------------------------------------------------------------------------
   -- Assign output (for simple type conversion really)
   -------------------------------------------------------------------------------
   round_key_proc: process (round_key_words)
   begin  -- process round_key_proc
      for b in round_keys_t'range loop
         for w in block_t'range loop
            round_keys(b)(w) <= round_key_words(b*block_t'length + w);
         end loop;
      end loop;
   end process round_key_proc;
END fsm;
-- VHDL Entity crypt_lib.s_box.symbol
--
-- Created:
--          by - gregt.rand (merry.gbr.hp.com)
--          at - 11:30:38 10/13/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY WORK;
USE WORK.aes_package.ALL;

ENTITY s_box IS
   PORT( 
      sbox_input  : IN     sbox_word_t;
      sbox_result : BUFFER sbox_word_t
   );

-- Declarations

END s_box ;

--
--######################################################################--
ARCHITECTURE rtl OF s_box IS
BEGIN

   sbox_proc: process (sbox_input)
   begin  -- process sbox_proc
      for i in sbox_word_t'range loop
         sbox_result(i) <= to_stdlogicvector(sbox(to_integer(unsigned(sbox_input(i)))));
      end loop;  -- i
   end process sbox_proc;
   
END ARCHITECTURE rtl;

