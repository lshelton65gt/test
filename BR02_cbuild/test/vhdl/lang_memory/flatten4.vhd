-- this testcase was extracted from a customer test, is used to test for the flattening of small memories
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity sub_one is
  port (
    count   : in     integer;
    qa      : out std_logic_vector(3 downto 0);
    qb      : out std_logic_vector(3 downto 0);
    qc      : out std_logic_vector(3 downto 0);
    qd      : out std_logic_vector(3 downto 0)
    );
end sub_one;

architecture functional of sub_one is

begin  -- functional

  process (count)
  begin  -- process
    qa <= CONV_STD_LOGIC_VECTOR(count,4);
    qb <= CONV_STD_LOGIC_VECTOR(count,4); -- (4 to 7);
    qc <= CONV_STD_LOGIC_VECTOR(count,4); --(11 downto 8);
    qd <= CONV_STD_LOGIC_VECTOR(count,4); --(15 downto 12);
    if (count = 0) then
      qa <= CONV_STD_LOGIC_VECTOR(count,4); --(19 downto 16);
      qb <= CONV_STD_LOGIC_VECTOR(count,4); --(23 downto 20);
      qc <= CONV_STD_LOGIC_VECTOR(count,4); --(27 downto 24);
      qd <= CONV_STD_LOGIC_VECTOR(count,4); --(31 downto 28);
    end if;
  end process;

end functional;


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity flatten4 is
  port (
    clock1      : in     std_logic;
    enable1     : in     std_logic;
    out1        : out    std_logic_vector(3 downto 0);
    rstN        : in     std_logic
    );
end flatten4;


architecture functional of flatten4 is
component sub_one is
  port (
    count   : in     integer;
    qa      : out std_logic_vector(3 downto 0);
    qb      : out std_logic_vector(3 downto 0);
    qc      : out std_logic_vector(3 downto 0);
    qd      : out std_logic_vector(3 downto 0)
    );
end component sub_one;
  
   type ARRAYSMALL    is array (0 to 3) of std_logic_vector( 3 downto 0);
   type ARRAYLARGE    is array (0 to 3) of std_logic_vector(15 downto 0);

   signal p3_tag                   : ARRAYLARGE;
   signal p4_mem1                  : ARRAYSMALL;
   signal p4_count                 : integer;
   signal  mem1                     : ARRAYSMALL;

   signal mem2                     : ARRAYSMALL;
  
begin

      xSUB_ONE: sub_one
         port map (
            count      => p4_count,
   
            qa         => p4_mem1(0),
            qb         => p4_mem1(1),
            qc         => p4_mem1(2),
            qd         => p4_mem1(3)
         );

  
    process(enable1, p4_mem1, p3_tag, mem2)
      begin

        
         p4_count <= 1;

         mem1 <= p4_mem1;
         
         mem2 <= p4_mem1;
         p4_mem1 <= mem1;
         out1 <= mem1(1);
      end process;


end functional;

