entity wait_based_clk_1 is
  port (d,clk,rst : in bit; q: out bit);
end;

architecture arch of wait_based_clk_1 is
begin
  process 
  begin
	wait until clk'EVENT and clk = '0';
    if (rst = '0')  then
      q <= '0';
    else
      q <= d;
    end if;
  end process;
end;
