-- filename: test/vhdl/lang_memory/bug8345_03.vhd
-- Description:  This test duplicates the problem seen in bug 8345.  The
-- problem is that the memory slice assignment:
--   array_r(2:1) = array_r(1:0)
-- is not properly populated.  At one time the RHS terms were incorrectly
-- extended to 32 bits.
-- This is like bug8345_01 but the array is smaller and there is no function
-- used to do the shift of array_r(2:1) = array_r(1:0) 

-- see test/vhdl/lang_memory/bug8345_04.vhd for details about what was done wrong

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug8345_03 is
  port (
    clock    : in  std_logic;
    int1 : in natural range 0 to 3;
    in1 : in  std_logic;
    in2 : in  std_logic_vector(1 downto 0);
    out1  : out std_logic_vector(1 downto 0));
end;

architecture arch of bug8345_03 is
begin
  main: process (clock)
    TYPE   natural_array_type    is array (NATURAL range <>) of natural range 0 to 3; 
    variable array_r : natural_array_type(2 downto 0) := (others => 3);
    VARIABLE count_r         : unsigned(1 DOWNTO 0);



  begin
    if clock'event and clock = '1' then 
      if in1 = '1' then 
        array_r(0)  :=  0;
        array_r(1) := int1;
        array_r(2 downto 1) := array_r(1 downto 0);

        out1   <= STD_LOGIC_VECTOR(to_unsigned(array_r(2), 2)); 
      end if;
    end if;
  end process;
end;

