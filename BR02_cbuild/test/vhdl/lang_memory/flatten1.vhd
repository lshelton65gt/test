-- this testcase was extracted from a customer test, is used to test for the flattening of small memories

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity flatten1 is
  port (
    teclkbuf              : in     std_logic;
    holdpipe              : in     std_logic;
    p4_seqmiss            : out    std_logic_vector(15 downto 0);
    p4_tagmux_0           : out    std_logic_vector(3 downto 0);
    p4_seqmux_0           : out    std_logic_vector(3 downto 0);
    rstN    : in     std_logic
    );
end flatten1;


architecture functional of flatten1 is
   type ARRAY_OF_TAGMUX    is array (0 to 23) of std_logic_vector( 3 downto 0);
   type ARRAY_OF_16TAGS    is array (0 to 23) of std_logic_vector(15 downto 0);

   signal p3_tag                     : ARRAY_OF_16TAGS;
   signal p3_tagmux                  : ARRAY_OF_TAGMUX;
   signal p4_tagmux                  : ARRAY_OF_TAGMUX;
   signal p3_seqmiss                 : std_logic_vector(15 downto 0);
   signal p3_seqmux                  : ARRAY_OF_TAGMUX;
   signal p4_seqmux                  : ARRAY_OF_TAGMUX;
   signal p3_ldreg                   : std_logic;
   signal p4_count                   : integer := 0;

  
begin
  
    process(p3_tag)
         variable tagmiss : std_logic_vector(15 downto 0);
         variable seqmiss : std_logic_vector(15 downto 0);
         variable tagmux  : ARRAY_OF_TAGMUX;
         variable seqmux  : ARRAY_OF_TAGMUX;
         variable seq     : integer;
      begin

        
         tagmiss(0) := '1';
         tagmux(0)  := (OTHERS => '0');
         
         for i in 1 to 15 loop

            tagmiss(i) := '1';
            tagmux(i)  := CONV_STD_LOGIC_VECTOR(i,tagmux(i)'length);

            for j in 0 to i-1 loop
               if (p3_tag(i) = p3_tag(j) and tagmiss(j) = '1') then
                  tagmux(i)  := CONV_STD_LOGIC_VECTOR(j,tagmux(i)'length);
                  tagmiss(i) := '0';
               end if;
            end loop;
         end loop;

         -- creates the sequence
         
         seq := 0;
         seqmiss := (OTHERS => '0');
         seqmux  := (OTHERS => (OTHERS => '0'));
         
         for k in 0 to 15 loop
            if (tagmiss(k) = '1') then
               seqmiss(seq) := '1';
               seqmux(seq)  := tagmux(k);
               seq := seq + 1;
            end if;
         end loop;

         p3_tagmux  <= tagmux;
         p3_seqmiss <= seqmiss;
         p3_seqmux  <= seqmux;

      end process;

      process (teclkbuf)
      begin
         if (teclkbuf'event and teclkbuf = '1') then
            if ( holdpipe = '0' ) then
               p4_seqmiss        <= p3_seqmiss;
               p4_tagmux         <= p3_tagmux;
               p4_seqmux         <= p3_seqmux;

               if(p3_ldreg = '1') then
                 p4_count <= 0;
               end if;
               if (p4_count < 23 ) then
                 p4_count <= p4_count+1;
               else
                 p4_count <= 0;
               end if;
               p3_tag(p4_count) <= "0000000000000001"; 
               p4_tagmux_0 <= p4_tagmux(0);
               p4_seqmux_0 <= p4_seqmux(0);

            end if;
         end if;
      end process;

end functional;
