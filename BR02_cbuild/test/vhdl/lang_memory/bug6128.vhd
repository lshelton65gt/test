-- last mod: Fri May 26 09:06:39 2006
-- filename: test/vhdl/signed/bug6128.vhd
-- Description:  This test is from bug6128, it reported a signed/unsigned
-- mismatch on a port.  the original design is transpose, there is a wrapper
-- surrounding it so that -testdriver will work with it



library ieee;
use ieee.std_logic_1164.all;

package baggage is
constant cPhBitW : integer := 16;
constant cDiMemW : integer := 16;
type tVecArrayPhBitW is array (natural range <>) of  std_logic_vector(cPhBitW-1 downto 0);
type tVecArrayDiMemW is array (natural range <>) of std_logic_vector(cDiMemW-1 downto 0);
end package;



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.baggage.all;

entity sub_block is
port (
    iTrChOffset   :  tVecArrayDiMemW(0 to 7)   ;
    iFirstBitTrCh : tVecArrayPhBitW(0 to 7)   ;
    iLastBitTrCh  : tVecArrayPhBitW(0 to 7)   ;
    oTrChOffset   : out  tVecArrayDiMemW(0 to 7)   ;
    oFirstBitTrCh : out tVecArrayPhBitW(0 to 7)   ;
    oLastBitTrCh  : out tVecArrayPhBitW(0 to 7)
);
end entity;

architecture rev1 of sub_block is
begin
   process (iTrChOffset) is
   begin
    for I in iTrChOffset'low to iTrChOffset'high loop
  	OTrChOffset(i)   <= not iTrChOffset(i);
    end loop;
   end process;

  OFirstBitTrCh <=  iFirstBitTrCh;
  OLastBitTrCh  <=  iLastBitTrCh;
end architecture;


library ieee;
use ieee.std_logic_1164.all;
use work.baggage.all;


entity transpose is

port (

    iTrChOffset   :  tVecArrayDiMemW(0 to 7)   ;
    iFirstBitTrCh : tVecArrayPhBitW(0 to 7)   ;
    iLastBitTrCh  : tVecArrayPhBitW(0 to 7)   ;
    oTrChOffset   : out tVecArrayDiMemW(0 to 7)   ;
    oFirstBitTrCh : out tVecArrayPhBitW(0 to 7)   ;
    oLastBitTrCh  : out tVecArrayPhBitW(0 to 7)
);
end entity;

architecture rev1 of transpose is

  function Transp (X : tVecArrayPhBitW) return tVecArrayPhBitW is
    variable Y : tVecArrayPhBitW(X'range);
  begin
    -- transpose the array X
    for I in X'low to X'high loop
      Y(X'high+X'low-I) := X(I);
    end loop;
    return Y;
  end function Transp;

  function Transp (X : tVecArrayDiMemW) return tVecArrayDiMemW is
    variable Y : tVecArrayDiMemW(X'range);
  begin
    -- transpose the array X
    for I in X'low to X'high loop
      Y(X'high+X'low-I) := X(I);
    end loop;
    return Y;
  end function Transp;

component sub_block is
port (
    iTrChOffset   : tVecArrayDiMemW(0 to 7)   ;
    iFirstBitTrCh : tVecArrayPhBitW(0 to 7)   ;
    iLastBitTrCh  : tVecArrayPhBitW(0 to 7)   ;
    oTrChOffset   : out  tVecArrayDiMemW(0 to 7)   ;
    oFirstBitTrCh : out tVecArrayPhBitW(0 to 7)   ;
    oLastBitTrCh  : out tVecArrayPhBitW(0 to 7)
);
end component;

begin

  u1 : sub_block port map (iTrChOffset,        iFirstBitTrCh,iLastBitTrCh,OTrChOffset,OFirstBitTrCh,OLastBitTrCh);
  u2 : sub_block port map (Transp(iTrChOffset),iFirstBitTrCh,iLastBitTrCh,OTrChOffset,OFirstBitTrCh,OLastBitTrCh);

end architecture;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.baggage.all;

entity bug6128 is
  port (
    iTrChOffset_0 : in std_logic_vector(15 downto 0);
    iTrChOffset_1 : in std_logic_vector(15 downto 0);
    iTrChOffset_2 : in std_logic_vector(15 downto 0);
    iTrChOffset_3 : in std_logic_vector(15 downto 0);
    iTrChOffset_4 : in std_logic_vector(15 downto 0);
    iTrChOffset_5 : in std_logic_vector(15 downto 0);
    iTrChOffset_6 : in std_logic_vector(15 downto 0);
    iTrChOffset_7 : in std_logic_vector(15 downto 0);
    iFirstBitTrCh_0 : in std_logic_vector(15 downto 0);
    iFirstBitTrCh_1 : in std_logic_vector(15 downto 0);
    iFirstBitTrCh_2 : in std_logic_vector(15 downto 0);
    iFirstBitTrCh_3 : in std_logic_vector(15 downto 0);
    iFirstBitTrCh_4 : in std_logic_vector(15 downto 0);
    iFirstBitTrCh_5 : in std_logic_vector(15 downto 0);
    iFirstBitTrCh_6 : in std_logic_vector(15 downto 0);
    iFirstBitTrCh_7 : in std_logic_vector(15 downto 0);
    iLastBitTrCh_0 : in std_logic_vector(15 downto 0);
    iLastBitTrCh_1 : in std_logic_vector(15 downto 0);
    iLastBitTrCh_2 : in std_logic_vector(15 downto 0);
    iLastBitTrCh_3 : in std_logic_vector(15 downto 0);
    iLastBitTrCh_4 : in std_logic_vector(15 downto 0);
    iLastBitTrCh_5 : in std_logic_vector(15 downto 0);
    iLastBitTrCh_6 : in std_logic_vector(15 downto 0);
    iLastBitTrCh_7 : in std_logic_vector(15 downto 0);

    oTrChOffset_0 : out std_logic_vector(15 downto 0);
    oTrChOffset_1 : out std_logic_vector(15 downto 0);
    oTrChOffset_2 : out std_logic_vector(15 downto 0);
    oTrChOffset_3 : out std_logic_vector(15 downto 0);
    oTrChOffset_4 : out std_logic_vector(15 downto 0);
    oTrChOffset_5 : out std_logic_vector(15 downto 0);
    oTrChOffset_6 : out std_logic_vector(15 downto 0);
    oTrChOffset_7 : out std_logic_vector(15 downto 0);
    oFirstBitTrCh_0 : out std_logic_vector(15 downto 0);
    oFirstBitTrCh_1 : out std_logic_vector(15 downto 0);
    oFirstBitTrCh_2 : out std_logic_vector(15 downto 0);
    oFirstBitTrCh_3 : out std_logic_vector(15 downto 0);
    oFirstBitTrCh_4 : out std_logic_vector(15 downto 0);
    oFirstBitTrCh_5 : out std_logic_vector(15 downto 0);
    oFirstBitTrCh_6 : out std_logic_vector(15 downto 0);
    oFirstBitTrCh_7 : out std_logic_vector(15 downto 0);
    oLastBitTrCh_0 : out std_logic_vector(15 downto 0);
    oLastBitTrCh_1 : out std_logic_vector(15 downto 0);
    oLastBitTrCh_2 : out std_logic_vector(15 downto 0);
    oLastBitTrCh_3 : out std_logic_vector(15 downto 0);
    oLastBitTrCh_4 : out std_logic_vector(15 downto 0);
    oLastBitTrCh_5 : out std_logic_vector(15 downto 0);
    oLastBitTrCh_6 : out std_logic_vector(15 downto 0);
    oLastBitTrCh_7 : out std_logic_vector(15 downto 0));

end entity;

architecture arch of bug6128 is
  component transpose is
    port (
      iTrChOffset   :  tVecArrayDiMemW(0 to 7)   ;
      iFirstBitTrCh : tVecArrayPhBitW(0 to 7)   ;
      iLastBitTrCh  : tVecArrayPhBitW(0 to 7)   ;
      oTrChOffset   : out tVecArrayDiMemW(0 to 7)   ;
      oFirstBitTrCh : out tVecArrayPhBitW(0 to 7)   ;
      oLastBitTrCh  : out tVecArrayPhBitW(0 to 7)
      );
  end component transpose;

begin  -- arch

  u1 : transpose port map (
    iTrChOffset(0) => iTrChOffset_0,
    iTrChOffset(1) => iTrChOffset_1,
    iTrChOffset(2) => iTrChOffset_2,
    iTrChOffset(3) => iTrChOffset_3,
    iTrChOffset(4) => iTrChOffset_4,
    iTrChOffset(5) => iTrChOffset_5,
    iTrChOffset(6) => iTrChOffset_6,
    iTrChOffset(7) => iTrChOffset_7,
    iFirstBitTrCh(0) => iFirstBitTrCh_0,
    iFirstBitTrCh(1) => iFirstBitTrCh_1,
    iFirstBitTrCh(2) => iFirstBitTrCh_2,
    iFirstBitTrCh(3) => iFirstBitTrCh_3,
    iFirstBitTrCh(4) => iFirstBitTrCh_4,
    iFirstBitTrCh(5) => iFirstBitTrCh_5,
    iFirstBitTrCh(6) => iFirstBitTrCh_6,
    iFirstBitTrCh(7) => iFirstBitTrCh_7,
    iLastBitTrCh(0) => iLastBitTrCh_0,
    iLastBitTrCh(1) => iLastBitTrCh_1,
    iLastBitTrCh(2) => iLastBitTrCh_2,
    iLastBitTrCh(3) => iLastBitTrCh_3,
    iLastBitTrCh(4) => iLastBitTrCh_4,
    iLastBitTrCh(5) => iLastBitTrCh_5,
    iLastBitTrCh(6) => iLastBitTrCh_6,
    iLastBitTrCh(7) => iLastBitTrCh_7,
    oTrChOffset(0) => oTrChOffset_0,
    oTrChOffset(1) => oTrChOffset_1,
    oTrChOffset(2) => oTrChOffset_2,
    oTrChOffset(3) => oTrChOffset_3,
    oTrChOffset(4) => oTrChOffset_4,
    oTrChOffset(5) => oTrChOffset_5,
    oTrChOffset(6) => oTrChOffset_6,
    oTrChOffset(7) => oTrChOffset_7,
    oFirstBitTrCh(0) => oFirstBitTrCh_0,
    oFirstBitTrCh(1) => oFirstBitTrCh_1,
    oFirstBitTrCh(2) => oFirstBitTrCh_2,
    oFirstBitTrCh(3) => oFirstBitTrCh_3,
    oFirstBitTrCh(4) => oFirstBitTrCh_4,
    oFirstBitTrCh(5) => oFirstBitTrCh_5,
    oFirstBitTrCh(6) => oFirstBitTrCh_6,
    oFirstBitTrCh(7) => oFirstBitTrCh_7,
    oLastBitTrCh(0) => oLastBitTrCh_0,
    oLastBitTrCh(1) => oLastBitTrCh_1,
    oLastBitTrCh(2) => oLastBitTrCh_2,
    oLastBitTrCh(3) => oLastBitTrCh_3,
    oLastBitTrCh(4) => oLastBitTrCh_4,
    oLastBitTrCh(5) => oLastBitTrCh_5,
    oLastBitTrCh(6) => oLastBitTrCh_6,
    oLastBitTrCh(7) => oLastBitTrCh_7
    );


end arch;

