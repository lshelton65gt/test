-- Reproduces the error reported by bug6778, incorrect number of elements in
-- array aggregate, expected 3, but found 6.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug6778 is
  
  port (
    shift   : in std_logic;
    addr_in : in std_logic_vector(2 downto 0);
    data_in : in std_logic_vector(2 downto 0);
    out1    : out std_logic_vector(2 downto 0));

end bug6778;

architecture synth of bug6778 is
  constant width : integer := 8;
  constant width_bits : integer := 3;
  type int_vector is array (natural range <>) of integer range 0 to width-1;
  signal mem  : int_vector(width-1 downto 0) := (others => 0);
  shared variable addr : integer range 0 to width-1 := 0;
  shared variable data : integer range 0 to width-1 := 0;
  shared variable data_out : integer range 0 to width-1 := 0;
begin  -- synth

  process (shift, addr_in, data_in, mem)
  begin  -- process
    addr := to_integer(unsigned(addr_in));
    data := to_integer(unsigned(data_in));
    mem(addr) <= data;
    if (shift = '1') then
      mem <= mem(width-1) & mem(width-1 downto 1);  -- error: incorrect number of elements in array aggregate
    end if;
    out1 <= std_logic_vector(to_unsigned(mem(addr), width_bits));
  end process;

end synth;
