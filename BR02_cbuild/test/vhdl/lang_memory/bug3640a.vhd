library ieee;
use ieee.std_logic_1164.all;

entity bug3640a is
  port (
    enp          : in     std_logic;
    inp          : in     std_logic_vector(5 downto 0);
    outp         : out std_logic_vector(5 downto 0)
    );
end ;

architecture arch of bug3640a is

type rec is record
  sig1 : std_logic_vector(2 downto 0);
  sig2 : std_logic_vector(2 downto 0);
end record;

signal temp : rec;

begin

  temp.sig1 <= inp(2 downto 0 ) when enp = '1' else "ZZZ";
  temp.sig2 <= inp(5 downto 3 ) when enp = '1' else "ZZZ";

  outp(2 downto 0 ) <= temp.sig1;
  outp(5 downto 3) <= temp.sig2;
end;

