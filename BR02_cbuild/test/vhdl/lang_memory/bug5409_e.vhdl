-- this is a trimmed down version of the test in bug5409, used to check see if
-- array to vector assignments work
PACKAGE apkg IS
  TYPE IntArray3     IS ARRAY ( 1 DOWNTO 0 ) OF INTEGER RANGE 0 TO 34;
  TYPE IntArray10x3 IS ARRAY ( 0  TO 9 )    OF IntArray3;

  shared variable TBL : IntArray10x3;
END apkg;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_arith.ALL;
USE work.apkg.ALL;


ENTITY bug5409_e IS
  PORT (
    clk : in bit;
    sel_in : in std_logic_vector(2 downto 0);
    d1 : in INTEGER RANGE 0 TO 34;
    osel : out integer;
    o1  : OUT integer;
    o0  : OUT integer
    );
END bug5409_e;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_arith.ALL;
USE IEEE.STD_LOGIC_unsigned.ALL;
USE work.apkg.ALL;

ARCHITECTURE arch OF bug5409_e IS
BEGIN

  proc: PROCESS ( clk)
    VARIABLE prev_sel, sel     : INTEGER RANGE 0 TO 9;
    VARIABLE index   : IntArray3;
    variable d1_l : integer range 0 to 34 := 0;

  BEGIN
    if clk'event and clk = '1' then
      sel               := conv_integer(sel_in);
      index             := TBL(prev_sel); -- read the value written last cycle
      d1_l := d1;
      if (d1_l > 34) then
        d1_l := 0;
      end if;

      TBL(sel) := ( d1_l, d1_l ); --write 2 copies of a value into location sel

      prev_sel := sel;

      osel <= sel;
      o0 <= index(0);
      o1 <= index(1);
      
    end if;
  END PROCESS proc;

END arch;


