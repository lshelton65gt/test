entity event_based_clk_5 is
  port (d,clk : in bit; q: out bit);
end;

architecture arch of event_based_clk_5 is
begin
  process (clk)
  begin
    if (clk'EVENT and clk = '1') then
      q <= d;
    end if;
  end process;
end;
