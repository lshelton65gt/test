entity sel_sig_asgn2 is
  port (in1, in2,in3 : in bit; out1: out bit);
end sel_sig_asgn2;

architecture arch of sel_sig_asgn2 is
begin
  with in1 select
		out1 <= in2 and in3 when '0' ,
		        in2 xor in3 when others;

end;
