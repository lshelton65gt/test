entity simple_process is
  port (d,clk,rst: in bit; q: out bit);
end;

architecture arch of simple_process is
begin
  process (d, rst)
  begin
    if (rst = '0')  then
      q <= '0';
    else
      q <= d;
    end if;
  end process;
end;
