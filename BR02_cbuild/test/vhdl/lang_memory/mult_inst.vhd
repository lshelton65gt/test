library ieee;
use ieee.std_logic_1164.all;

entity flop is
	port (
			D   : IN  std_ulogic;
			CLK : IN  std_ulogic;
			CLR : IN  std_ulogic;
			Q   : OUT std_ulogic
		 );
end;

architecture flop_arc of flop is
begin
	process(CLK, CLR)
	begin
		if (CLR = '1') then
			Q <= '0';
		elsif (CLK = '1' and CLK'EVENT) then
			Q <= D;
		end if;
	end process;
end;


library ieee;
use ieee.std_logic_1164.all;

entity mult_inst is
	port (
			data1 : IN     std_ulogic;
			data2 : IN     std_ulogic;
			aclk : IN     std_ulogic;
			rst  : IN     std_ulogic;
			dout1 : out std_ulogic;
			dout2 : out std_ulogic
		 );
end;

architecture arch of mult_inst is

	COMPONENT flop
		PORT (
			D   : IN  std_ulogic;
			CLK : IN  std_ulogic;
			CLR : IN  std_ulogic;
			Q   : OUT std_ulogic
		);
	END COMPONENT;

begin
		flop0 : flop
			PORT MAP (
				D   =>    data1,
				CLK =>    aclk,
				CLR =>    rst,
				Q   =>    dout1
			);
		flop1 : flop
			PORT MAP (
				D   =>    data2,
				CLK =>    aclk,
				CLR =>    rst,
				Q   =>    dout2
			);
end;

