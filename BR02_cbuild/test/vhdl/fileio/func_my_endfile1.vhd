-- last mod: Mon Feb  6 14:18:19 2006
-- filename: test/vhdl/fileio/func_my_endfile1.vhd
-- Description:  This test is designed to check that we pick up the user
-- defined function called endfile 

-- gold simulation results are by inspection (aldec says the function endfile
-- is ambiguous) (nc cannot handle endfile without a return stmt)

use std.standard.all ;
package func_my_endfile1_pack is 
  type my_file is file of integer; 
  function endfile
    ( file in1 : my_file) return boolean ;
end ;
package body func_my_endfile1_pack is 
function endfile
  ( file in1 : my_file) return boolean is
begin 
  return  true;       -- always return true which is opposite of std endfile 
end;
end;
 
use work.func_my_endfile1_pack.all;



entity func_my_endfile1 is 
  port (in1 : bit; out1 : out integer);
end;
    
architecture func_my_endfile1 of func_my_endfile1 is 
--  type my_file is file of integer;    -- why does uncommenting this line cause
                                        --  us to pick up the std.textio.endfile
                                        --  instead of user defined one? 
  file jet : my_file open READ_MODE is "func_my_endfile1.vhd"; --read self
begin
  process (in1)
      variable int1 : integer;
  begin
    for i in 0 to 10
     loop
       if (endfile(jet)) then           --the std endfile will return false
         out1 <= 10;
       else
         out1 <= 255;
       end if;
    end loop;
  end process;
end;

