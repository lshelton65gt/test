entity file_rw1 is
    port (in1 : integer range 0 to 31; out1 : out integer range 0 to 31);
end;

architecture file_rw1 of file_rw1 is
  type FType is file of integer;
  file out_file : FType;
  file out_file2 : FType;
begin

  process (in1)
    variable in8 : integer ;
    variable in9 : integer ;
  begin
    file_open(out_file, "file1.in", READ_MODE);
    read(out_file, in8);
    read(out_file, in9);
    file_close(out_file);

    file_open(out_file2, "RW1", APPEND_MODE);
    write(out_file2, in8);
    write(out_file2, in9);
    file_close(out_file2);

    out1 <= in8; 
  end process;

end;

