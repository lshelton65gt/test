-- Read all std_logic_vector from a file, one line at a time using the LINE variable.
library STD;
use STD.textio.all;
library ieee;
use ieee.std_logic_textio.all;
use IEEE.std_logic_1164.all;

entity readline_stdlogic1 is
  port ( in1 : boolean;
         int1, int2, int3, int4 : out std_logic_vector(9 downto 0));
end;

architecture readline_stdlogic1 of readline_stdlogic1 is
  subtype bit10 is std_logic_vector(9 downto 0);
  type my_file is file of string;
  file rd : text open READ_MODE is "readline_stdlogic1.in";
begin
  process 
    variable lyne : line;
    variable vint1, vint2, vint3, vint4 : bit10 := "0000000000";
  begin
    while (NOT endfile(rd))
    loop
      readline(rd, lyne);
      read(lyne, vint1);  -- 1st std_logic_vector of line
      read(lyne, vint2);  -- 2nd std_logic_vector of line
      read(lyne, vint3);  -- 3rd std_logic_vector of line
      read(lyne, vint4);  -- 4th std_logic_vector of line.
     int1 <= vint1;
     int2 <= vint2;
     int3 <= vint3;
     int4 <= vint4; -- evenually gets the last std_logic_vector of last line.
    end loop;
  end process;
end;

