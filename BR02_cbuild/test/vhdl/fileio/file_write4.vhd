-- Writing bit_vector to a file
-- bit_vector size < 8 as well as a bit_vector of size > 64.
entity file_write4 is
  port ( in1 : bit_vector(5 downto 0); in2 : bit_vector(75 downto 0);
         out1 : out bit_vector(5 downto 0); out2 : out bit_vector(75 downto 0));
end file_write4;

architecture file_write4 of file_write4 is
  subtype bit6 is bit_vector(5 downto 0);
  subtype bit76 is bit_vector(75 downto 0);
  type bit6File is file of bit6;
  type bit76File is file of bit76;
 
  file my1stFile : bit6File open WRITE_MODE is "file_write4.wr1.out";
  file my2ndFile : bit76File open WRITE_MODE is "file_write4.wr2.out";
  begin
    process (in1, in2) 
      begin
        write(my1stFile, in1); 
        write(my2ndFile, in2); 
        out1 <= in1;
        out2 <= in2;
    end process;

end file_write4;
