-- Testing if the proper include files are written to the generated code or not.
-- If not. compilation will fail.
use std.textio.all;

entity line1 is
  port ( in1 : bit;
         out1 : out integer);
end;

architecture line1 of line1 is
begin
  process (in1)
    variable lyne : line;
    variable vint1 : integer:= 0;
  begin
    out1 <= vint1;
  end process;
end;

