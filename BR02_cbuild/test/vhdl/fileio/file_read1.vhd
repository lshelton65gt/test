
-- Testing reading of a bit_vector of size > 8 by implicit read() procedure.
entity file_read1 is
  port ( in1 : bit;
         out1 : out bit_vector(9 downto 0));
end file_read1;

architecture file_read1 of file_read1 is
  subtype bit10 is bit_vector(9 downto 0);
  type bit_vecFile is file of bit10;
 
  file myFile : bit_vecFile  open READ_MODE is "file_read1.in";

  begin
    process (in1) 
      variable value : bit10 := "0000000000";
      begin
        if not(ENDFILE(myFile)) then 
           READ(myFile, value);
           out1 <= value; -- reads the first value from the file.
        end if;
    end process;

end file_read1;
