-- Writing integers to a file.
-- Using declaration to open a file, so all values of in1 will eventually go into file.
entity file_write2 is 
  port (in1 : integer; out1 : out integer);
end;
    
architecture file_write2 of file_write2 is 
  type my_file is file of integer;
  file junk : my_file;
  file jet : my_file open WRITE_MODE is "file_write2.wr1.out";
begin
  process (in1)
  begin
    write(jet, in1);
    out1 <= in1;
  end process;
end;

