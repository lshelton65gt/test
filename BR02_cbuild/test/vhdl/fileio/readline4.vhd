use std.textio.all;

entity readline4 is
  port ( in1 : integer;
         int1, int2, int3, int4 : out integer);
end;

architecture readline4 of readline4 is
  type my_file is file of string;
  file rd : text open READ_MODE is "integerlines.in";
  file wrt : text open WRITE_MODE is "integerlines.out";
begin
  process (in1)
    variable lyne : line;
    variable vint1, vint2, vint3, vint4 : integer := 0;
  begin
    if (NOT endfile(rd)) then
      readline(rd, lyne);
      read(lyne, vint1);  -- 1st int of line
      read(lyne, vint2);  -- 2nd int of line
      read(lyne, vint3);  -- 3rd int of line
      read(lyne, vint4);  -- 4th int of line.
    end if;
    int1 <= in1;
    int2 <= vint2;
    int3 <= vint3;
    int4 <= vint4;
  end process;
end;

