-- Writing a bit and a boolean value.
-- Currently we will output boolean as a 1 bit binary. But it shud be 'true' and 'false' identifiers. 
entity file_write5 is
  port ( in1 : bit; in2 : boolean;
         out1 : out bit; out2 : out boolean);
end file_write5;

architecture file_write5 of file_write5 is
  type bitFile is file of bit;
  type boolFile is file of boolean;
  
  file mybitFile : bitFile open WRITE_MODE is "file_write5.wr1.out";
  file myboolFile : boolFile open WRITE_MODE is "file_write5.wr2.out"; 
  begin
    process (in1, in2) 
      begin
        write(mybitFile, in1); 
        write(myboolFile, in2); 
        out1 <= in1;
        out2 <= in2;
    end process;

end file_write5;
