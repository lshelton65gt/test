library std;
use std.textio.all;
entity read_stdin is
  port ( clk : bit;
         out1 : out bit_vector(9 downto 0));
end read_stdin;

architecture read_stdin of read_stdin is
  subtype bit10 is bit_vector(9 downto 0);
  begin
    process (clk)
      variable value : bit10 := "0000000000";
      variable lyne : line;
      begin
        if clk'event and clk='1' then
              readline(STD.TEXTIO.INPUT, lyne);
              READ(lyne, value);

              out1 <= value;

       end if;
    end process;

end read_stdin;

