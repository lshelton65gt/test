use std.textio.all;

entity writeline1 is
  port (
         int1, int2, int3, int4 : in integer;
         out1, out2, out3, out4 : out integer);
end;

architecture writeline1 of writeline1 is
  type my_file is file of string;
  file wrt : text open WRITE_MODE is "writeline1.wr1.out";
begin
  process (int1, int2, int3, int4)
    variable lyne : line;
    variable vint : integer;
  begin
      write(lyne, int1);
      write(lyne, int2);
      write(lyne, int3);
      write(lyne, int4);
      writeline(wrt, lyne); -- insert a line containing the 4 int value.
      write(lyne, 0);
      writeline(wrt, lyne); -- insert a line containing just a "0"
    out1 <= int1;
    out2 <= int2;
    out3 <= int3;
    out4 <= int4;
  end process;
end;

