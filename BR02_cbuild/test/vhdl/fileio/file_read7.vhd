-- Reading a boolean from a file.
entity file_read7 is
  port ( in1 : bit_vector(0 to 5);
         out1 : out bit_vector(0 to 5));
end file_read7;

architecture file_read7 of file_read7 is
  type boolFile is file of boolean;
 
  file myFile : boolFile open READ_MODE is "file_read7.in";
  begin
    process (in1) 
      variable in2 : boolean := true;
      begin
        if not(ENDFILE(myFile)) then 
           READ(myFile, in2); 
           if (in2 = true) then
              out1 <= "110011";
           else
              out1 <= "111111";
           end if;
        end if;
    end process;

end file_read7;
