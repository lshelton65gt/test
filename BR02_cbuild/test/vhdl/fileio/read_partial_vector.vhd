library std;
use std.textio.all;
entity read_partial_vector is
  port ( clk : bit;
         out1 : out bit_vector(79 downto 0));
end read_partial_vector;

architecture read_partial_vector of read_partial_vector is
  subtype bit10 is bit_vector(89 downto 0);
  file myFile : text open READ_MODE is "read_partial_vector.in";
  begin
    process (clk)
      variable value : bit10 := "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
      variable lyne : line;
      begin
        if clk'event and clk='1' then
           if not(ENDFILE(myFile)) then
              readline(myFile, lyne);
              READ(lyne, value(88 downto 9));

              out1 <= value(88 downto 9);

         end if;
       end if;
    end process;

end read_partial_vector;

