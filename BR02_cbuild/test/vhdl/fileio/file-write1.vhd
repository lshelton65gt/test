library STD;
use STD.textio.all;

entity file1 is
  port ( INPUT1, INPUT2 : bit_vector(7 downto 0));
end file1;

architecture file1 of file1 is
   begin
   process(INPUT1, INPUT2)
   variable THE_LINE : LINE;
   variable TEMP : bit_vector(7 downto 0);
   file THEFILE : TEXT open write_mode is "file1.out";
   begin
   for i in INPUT1'range loop
   TEMP(i) := INPUT1(i) and INPUT2(i);
   end loop;
   WRITE(THE_LINE,TEMP);
   WRITELINE(THEFILE,THE_LINE);
   end process;
end architecture file1;

