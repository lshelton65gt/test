
-- Testing the return status of READ().
-- This test is similar to read_status1. The difference being, 
--    this one has some illegal entries in input file.
-- The legal value in the input file should be a 10-bit binary vector.
-- For ex the following lines in the input file, READ() return the status as
----------------------------------------------
-- Lines in input file          :     Status 
----------------------------------------------
-- 1110000001                   :     TRUE
--  1110000010                  :     TRUE   
-- asdfghjklm                   :     FALSE
-- true                         :     FALSE
-- 1111101H00                   :     TRUE      -- It is read as 0001111101
-- 111                          :     TRUE      -- It is read as 0000000111

library std;
use std.textio.all;
entity read_status2 is
  port ( clk : bit;
         out1 : out bit_vector(9 downto 0);
         outEof, outStatus : out boolean);
end read_status2;

architecture read_status2 of read_status2 is
  subtype bit10 is bit_vector(9 downto 0);
  file myFile : text open READ_MODE is "read_status2.in";

  begin
    process (clk)
      variable value : bit10 := "0000000000";
      variable boolval : boolean := false;
      variable lyne : line;
      begin
        if clk'event and clk='1' then
         if not(ENDFILE(myFile)) then
            outEof <= false;
            readline(myFile, lyne);
            READ(lyne, value, boolval);
            outStatus <= boolval;
            if (boolval) then
              out1 <= value; -- reads the last value read from the file.
            end if;
         else
           outEof <= true;
         end if;
       end if;
    end process;

end read_status2;

