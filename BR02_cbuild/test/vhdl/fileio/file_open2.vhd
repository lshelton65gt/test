-- Implicit opening of a file.
-- open a file during declaration
entity file_open2 is 
  port (in1 : bit; out1 : out integer);
end;
    
architecture file_open2 of file_open2 is 
  type my_file is file of integer;
  file jet : my_file open READ_MODE is "file_open2.in";
begin
  process (in1)
      variable int1 : integer;
  begin
    for i in 0 to 10
     loop
       if (NOT endfile(jet)) then
         read(jet, int1);
         out1 <= int1;   -- process is scheduled only once ?? 
       end if;
    end loop;
  end process;
end;

