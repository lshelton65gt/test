-- Direct dumping of lines from input-file to output-file using only LINE variable

use std.textio.all;

entity readwriteline2 is
  port ( in1 : bit;
         out1 : out integer);
end;

architecture readwriteline2 of readwriteline2 is
  type my_file is file of string;
  file rd : text open READ_MODE is "readwriteline2.in";
  file wrt : text open WRITE_MODE is "readwriteline2.wr1.out";
begin
  process (in1)
    variable lyne : line;
    variable vint1 : integer:= 0;
  begin
    while (NOT endfile(rd))
    loop
      readline(rd, lyne);
      writeline(wrt, lyne); -- dumping a line from input file 
    end loop;
    out1 <= vint1;
  end process;
end;

