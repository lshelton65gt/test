-- This test is here to test a file descriptor declared in process scope.  It
-- is not intended to be simulated.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.TEXTIO.all;

entity bug4563 is
  port (
    clk         : in     std_logic;
    readI       : in     std_logic;
    writeI      : in     std_logic;
    addressWRI  : in     std_logic_vector( 28 downto 0);
    addressRDI  : in     std_logic_vector( 28 downto 0);
    maskI       : in     std_logic_vector( 15 downto 0);
    dataI       : in     std_logic_vector(127 downto 0);
    dataO       : buffer std_logic_vector(127 downto 0)
    );
end bug4563;


architecture functional of bug4563 is
  type   mem_4BitWide_t is array (1 downto 0) of std_logic_vector(3 downto 0);
  signal mem0 : mem_4BitWide_t;
  signal memory_initialized : boolean := false;
begin   -- architecture

  init_memory: process( memory_initialized )
    variable L: line;
    file F: text open read_mode is "mch0";
--    variable c : character;
--    variable add: integer range 0 to 2**29;
--    variable dat: std_logic_vector(127 downto 0);
  begin
    if memory_initialized = false then
      while not ENDFILE(F) loop
--        readline(F, L);
--        read(L, c);   -- consume the '@' at the beginning of the line
--        read(L, add); -- read the address
--        read(L, dat); -- read the data
--        mem0(add) <= dat(3 downto 0); 
      end loop;
    end if;
    memory_initialized <= true;
  end process init_memory;
end functional;
