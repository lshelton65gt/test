entity file_read9 is
  port ( in1 : string(1 to 9);
         out1 : out string(1 to 9));
end file_read9;

architecture file_read9 of file_read9 is
  type strFile is file of string(1 to 9);
 
  file myFile : strFile open READ_MODE is "file_read9";
  begin
    process (in1) 
      variable in2 : string(1 to 9) := "carboncds";
      begin
        while not(ENDFILE(myFile)) loop 
           READ(myFile, in2); 
           out1 <= in2;
        end loop;
    end process;

end file_read9;
