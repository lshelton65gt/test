-- Interleaved read/write/readline/writeline operations.
use std.textio.all;

entity readwriteline3 is
  port ( in1 : bit;
         out1 : out integer);
end;

architecture readwriteline3 of readwriteline3 is
  type my_file is file of string;
  file rd : text open READ_MODE is "readwriteline3.in";
  file wrt : text open WRITE_MODE is "readwriteline3.wr1.out";
  file wrt1 : text open WRITE_MODE is "readwriteline3.wr2.out";
begin
  process
    variable lyne : line;
    variable vint1 : integer:= 0;
  begin
    while (NOT endfile(rd))
    loop
      readline(rd, lyne);
      read(lyne, vint1);  -- 1st int of line (removed from lyne)
      writeline(wrt, lyne); -- dumping a line from input file 
                            -- (with one less intger) to ouput file
      write(lyne, vint1);
      write(lyne, vint1);
      writeline(wrt1, lyne); -- dump two ints per line. 
      out1 <= vint1;
    end loop;
  end process;
end;

