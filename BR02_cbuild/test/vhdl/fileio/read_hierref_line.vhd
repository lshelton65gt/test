library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
package p1 is
  shared variable lynnnnn : line;
end p1;

use std.textio.all;
use work.p1.all;
entity read_hierref_line is
  port ( in1 : bit;
         int1, int2, int3, int4 : out integer);
end;

architecture read_hierref_line of read_hierref_line is
  type my_file is file of string;
  file rd : text open READ_MODE is "readline1.in";
begin
  process( in1 )
    variable vint1, vint2, vint3, vint4 : integer := 0;
  begin
    if in1'event and in1 = '1' then
      
      if (NOT endfile(rd)) then 
        readline(rd, lynnnnn);
        read(lynnnnn, vint1);  -- 1st int of line
        read(lynnnnn, vint2);  -- 2nd int of line
        read(lynnnnn, vint3);  -- 3rd int of line
        read(lynnnnn, vint4);  -- 4th int of line.
      end if;
      int1 <= vint1;
      int2 <= vint2;
      int3 <= vint3;
      int4 <= vint4;
    end if;
  end process;
end;

