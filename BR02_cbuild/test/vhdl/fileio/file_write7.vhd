
-- Write characters to a file.
-- There isnt any support yet for format string %c in cbuild. We are using %h instead.

entity file_write7 is
  port ( in1 : character;
         out1 : out character);
end ;

architecture file_write6 of file_write7 is
  type charFile is file of character;
 
  file myFile : charFile open WRITE_MODE is "file_write7.wr1.out";
  begin
    process (in1) 
      begin
        write(myFile, in1); 
        out1 <= in1;
    end process;

end ;
