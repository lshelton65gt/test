-- In bug7535 codegen asserted out when an empty string was used a the filename
-- argument in an NUFSOpenSysTask
--
-- The customer RTL that first discovered the bug obtained the filename via a
-- generic parameter.
--
-- Note: this test requires -vhdlEnableFileIO
-- this test is expected to have an error at runtime because it tries to open a
-- file with an empty filename.

entity sub is
  generic ( filename : string := "defaultname"); 
  port (readfile : in bit);
end sub;

architecture arch of sub is
  type myIntFile is file of integer;
  file myfile : myIntFile;
begin
  process (readfile)
    variable stat : file_open_status;
  begin
    file_open (stat, myfile, filename, read_mode);
  end process;
end;

entity file_open8 is
  port (in1: in bit);
end ;

architecture arch of file_open8 is
component sub is
  generic ( filename : string); 
  port (readfile : in bit);
end component;
begin
  mid_inst : sub generic map (filename => "") port map (in1);
end;
