-- last mod: Mon Feb  6 14:18:24 2006
-- filename: test/vhdl/fileio/func_my_endfile.vhd
-- Description:  This test is designed to check that we pick up the user
-- defined function called endfile 

-- gold simulation results are by inspection (aldec says the function endfile
-- is ambiguous) (nc cannot handle endfile without a return stmt)

use std.textio.all ;
 package func_my_endfile31_pack is 
   type my_file is file of integer; 
   function endfile
     ( file in1 : text) return boolean ;
 end ;
 package body func_my_endfile31_pack is 
 function endfile
   ( file in1 : text) return boolean is
 begin 
    return not std.textio.endfile(in1);             --return the opposite of real endfile
 end;
 end;
 
use work.func_my_endfile31_pack.all;
use std.textio.all ;

entity func_my_endfile3 is 
  port (in1 : bit; out1 : out integer);
end;
    
architecture func_my_endfile3 of func_my_endfile3 is 
  file jet : text open READ_MODE is "func_my_endfile3.vhd"; --read self
begin
  process (in1)
      variable int1 : integer;
  begin
    for i in 0 to 10
     loop
       if (endfile(jet)) then           --the std endfile will return false
         out1 <= 10;
       else
         out1 <= 255;              -- in hex this is BADBAD
       end if;
    end loop;
  end process;
end;

