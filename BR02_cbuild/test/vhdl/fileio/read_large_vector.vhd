library std;
use std.textio.all;
entity read_large_vector is
  port ( clk : bit;
         out1 : out bit_vector(79 downto 0));
end read_large_vector;

architecture read_large_vector of read_large_vector is
  subtype bit10 is bit_vector(79 downto 0);
  file myFile : text open READ_MODE is "read_large_vector.in";
  begin
    process (clk)
      variable value : bit10 := "00000000000000000000000000000000000000000000000000000000000000000000000000000000";
      variable lyne : line;
      begin
        if clk'event and clk='1' then
           if not(ENDFILE(myFile)) then
              readline(myFile, lyne);
              READ(lyne, value);

              out1 <= value;

         end if;
       end if;
    end process;

end read_large_vector;

