-- Writing integer with constraints
-- Also using file_open n file_close within the process block 
--      so only 2 integers will be written
entity file_write1 is
  port (in1 : integer range 0 to 31; out1 : out integer range 0 to 31);
end;

architecture file_write1 of file_write1 is
  type FType is file of integer;
  file out_file : FType;
begin

  process (in1)
  begin
    file_open(out_file, "file_write1.wr1.out", WRITE_MODE);
    write(out_file, in1);  -- eventually only the last in1 will remain in the file.
    write(out_file, 89);
    out1 <= in1;
    file_close(out_file);
  end process;

end;

