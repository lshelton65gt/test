-- Reading characters from a file.
entity file_read8 is
  port ( in1 : character;
         out1 : out character);
end file_read8;

architecture file_read8 of file_read8 is
  type charFile is file of character;
 
  file myFile : charFile open READ_MODE is "file_read8.in";
  begin
    process (in1) 
      variable in2 : character := 'a';
      begin
        while not(ENDFILE(myFile)) loop 
           READ(myFile, in2); 
           out1 <= in2;
        end loop;
    end process;

end file_read8;
