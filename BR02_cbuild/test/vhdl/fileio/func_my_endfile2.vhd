-- last mod: Wed Nov 27 11:48:32 2013
-- filename: test/vhdl/fileio/func_my_endfile2.vhd
-- Description:  This test is designed to check that we pick up the user
-- defined function called endfile 
--
-- gold simulation results are by inspection (aldec says the function endfile
-- is ambiguous) (nc cannot handle endfile without a return stmt)

use std.standard.all ;
package func_my_endfile21_pack is 
  type my_file is file of integer; 
  function endfile (
    signal in1 : boolean)
    return boolean;
end ;
package body func_my_endfile21_pack is 
function endfile
  ( signal in1 : boolean) return boolean is
begin
  return  true;       -- always return true 
end ;
end;
 
use work.func_my_endfile21_pack.all;



entity func_my_endfile2 is 
  port (in1 : bit; out1 : out integer);
end;
    
architecture func_my_endfile2 of func_my_endfile2 is 
 signal dumy : boolean := true;
begin
  process (in1, dumy)
      variable int1 : integer;
  begin
    for i in 0 to 10
     loop
       if (endfile(dumy)) then           --the std endfile does not work with a signal as an arg, so this should geneate an error if std endfile is used
         out1 <= 10;
       else
         out1 <= 255;
       end if;
    end loop;
  end process;
end;

