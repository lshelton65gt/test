-- this testcase is the test from bug 5119, with version 1.3778 this testcase
-- got a backend compile error related to a missing operator>> for a memory
-- generated for the code  read(file_line, file_addr, ok);


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use std.textio.all;
-- was sram128kx8_ent
entity bug5119_0 is
   generic
      (ram_chip_ld : string (1 to 13) := "ram_chip_load");
   port
      (cs_n : in    std_logic;
       ce   : in    std_logic;
       we_n : in    std_logic;
       oe_n : in    std_logic;
       addr : in    std_logic_vector(16 downto 0);
       dq   : inout std_logic_vector(7 downto 0));


  constant word_size  : integer :=    8; -- number of bits in a data word
  constant block_size : integer := 1024; -- number of data words in a memory block

  constant max_rows   : integer :=  128; -- number of memory blocks in the whole memory

end bug5119_0;

-------------------------------------------------------------------------------------
-- Architecture Body
-------------------------------------------------------------------------------------
architecture sram128kx8_arch of bug5119_0 is

  -----------------------------------------------------------------------------
  -- SIGNAL DECLARATIONS
  -----------------------------------------------------------------------------

   signal load            : integer := 0;
   signal write_data      : std_logic_vector(word_size -1 downto 0);      

   type  mem_type IS array ((block_size*max_rows)-1 downto 0) of std_logic_vector(word_size-1 downto 0);

   signal memcore           : mem_type;



   function To_string(input_integer : integer) return string is
       -- to convert larger integer values, it's necessary to change only the length of output_string variable on the next line
       variable output_string : string (1 to 5);
      	variable first_nonzero_char : integer := 1;
      	variable subtract_total : integer := 0;
      	variable pwr_o_ten : integer;
      	variable char_integer : integer;
   begin
      	-- if (input_integer > 10**output_string'length - 1) then
      	if (input_integer > 10**5 - 1) then
      		assert false
      		    report "Integer exceeded conversion function's range. (Macro file size has exceeded maximum number of lines.)"
      		    severity failure;
           for i in output_string'low to output_string'high loop
               output_string(i) := '?';
   	    end loop;
      	else
           -- pwr_o_ten := (output_string'length - 1);
           pwr_o_ten := (4);
      	    for i in output_string'low to output_string'high loop
      			char_integer := (input_integer - subtract_total)/10**pwr_o_ten;
      			case char_integer is 
      				when 0 => output_string(i) := '0';
      				when 1 => output_string(i) := '1';
      				when 2 => output_string(i) := '2';
      				when 3 => output_string(i) := '3';
      				when 4 => output_string(i) := '4';
      				when 5 => output_string(i) := '5';
      				when 6 => output_string(i) := '6';
      				when 7 => output_string(i) := '7';
      				when 8 => output_string(i) := '8';
      				when 9 => output_string(i) := '9';
      				when others => output_string(i) := '0';
      							   assert false
      								   report "Unexpected integer value in function to_string"
      								   severity failure;
      			end case;
      		    subtract_total := subtract_total + char_integer*10**pwr_o_ten;
      		    pwr_o_ten := pwr_o_ten - 1;
           end loop;
   		-- delete leading zeros from string to be returned (comment this loop out if leading zeros are desired)
   		for i in output_string'low to output_string'high loop
               if output_string(i) /= '0' then 
                   first_nonzero_char := i;
                   exit;
               elsif output_string(i) = '0' and (i = output_string'high) then
                   first_nonzero_char := output_string'high;
               end if;
           end loop;
      	end if;
       return output_string(first_nonzero_char to output_string'high);
   end To_string;


   function HEX_TO_INTGR(constant hex_string : in string) return integer is
      variable sum  : integer := 0;
      variable temp : integer := 0;
      variable i    : integer := 0;
      variable char : character;
   begin
       for index in hex_string'reverse_range loop
           char := hex_string(index);
           case char is
               when '0' => temp := 0;
               when '1' => temp := 1;
               when '2' => temp := 2;
               when '3' => temp := 3;
               when '4' => temp := 4;
               when '5' => temp := 5;
               when '6' => temp := 6;
               when '7' => temp := 7;
               when '8' => temp := 8;
               when '9' => temp := 9;
               when 'A' => temp := 10;
               when 'B' => temp := 11;
               when 'C' => temp := 12;
               when 'D' => temp := 13;
               when 'E' => temp := 14;
               when 'F' => temp := 15;
               when others => assert false
                               report "Invalid hex character in input file"
                                severity failure;
           end case;
           sum := sum + (temp * (16 ** i));
           i   := i + 1;
       end loop;
       return sum;
   end HEX_TO_INTGR;



   function HEX_TO_STD_LOGIC_VECTOR(arg         : in string;
                                    size        : in integer := 9;
                                    upper_bound : in integer := 35) return std_logic_vector is

       variable temp  : std_logic_vector (0 to upper_bound);
       variable temp2 : std_logic_vector (0 to 3);
       variable hex   : string (1 to size);    
       variable bin   : std_logic_vector (1 to size*4);
       variable char  : character;  
       variable i     : integer;
   begin
       hex := arg;
           for index in size downto 1 loop                -- size argument is chosen to be
               char := hex(index);                        -- able to handle an even multiple
               case char is                               -- of 4 bits for all hex nibbles
                   when '0' => temp2 := "0000";           -- being converted.  For hex
                   when '1' => temp2 := "0001";           -- representations of binary
                   when '2' => temp2 := "0010";           -- numbers where the most signifigant
                   when '3' => temp2 := "0011";           -- nibble represents less than 4
                   when '4' => temp2 := "0100";           -- bits a hard-coded exception
                   when '5' => temp2 := "0101";           -- case must be coded at the
                   when '6' => temp2 := "0110";           -- bottom of the function to
                   when '7' => temp2 := "0111";           -- truncate the excess bits from
                   when '8' => temp2 := "1000";           -- result being returned to the
                   when '9' => temp2 := "1001";           -- VHDL statement calling the
                   when 'A' => temp2 := "1010";           -- function.
                   when 'B' => temp2 := "1011";
                   when 'C' => temp2 := "1100";
                   when 'D' => temp2 := "1101";
                   when 'E' => temp2 := "1110";
                   when 'F' => temp2 := "1111";
                   when 'X' => temp2 := "XXXX";
                   when 'Z' => temp2 := "ZZZZ";
                   when 'U' => temp2 := "UUUU";
                   when 'a' => temp2 := "1010";
                   when 'b' => temp2 := "1011";
                   when 'c' => temp2 := "1100";
                   when 'd' => temp2 := "1101";
                   when 'e' => temp2 := "1110";
                   when 'f' => temp2 := "1111";
                   when 'x' => temp2 := "0000";
                   when 'z' => temp2 := "0000";
                   when 'u' => temp2 := "0000";
   -- additional 9-state cases
                   when 'W' => temp2 := "0000";
                   when 'w' => temp2 := "0000";
                   when 'L' => temp2 := "0000";
                   when 'l' => temp2 := "0000";
                   when 'H' => temp2 := "1111";
                   when 'h' => temp2 := "1111";
                   when '-' => temp2 := "0000";
                   when others => assert false
                                       report "Invalid hex character in input file"
                                       severity failure;
               end case;

               bin((index*4)-3 to (index*4)) := temp2;

           end loop;
       
       i := 0;

       while (i <= upper_bound) loop
          temp(upper_bound - i) := bin(size*4 - i);
          i := i + 1;
       end loop;

       return temp;

   end HEX_TO_STD_LOGIC_VECTOR;

   procedure load_mem_proc(file_ptr : in string; signal mem_core : inout mem_type) is
        file mem_load_file : text is in file_ptr;
        variable file_line       : line;
        variable file_addr       : string (1 to 5);
        variable file_data       : string (1 to 2);
        variable delimiter_space : string (1 to 1);
        variable line_number     : integer := 0;
        variable ok              : boolean := false;
        variable nibbles         : integer;
        variable i_addr          : integer;
   begin

         if ((word_size mod 4) /= 0) then
           nibbles := (word_size/4) + 1;  -- Add an extra nibble for a word size not evenly divisible by 4.
         else
           nibbles := word_size/4;
         end if;

         -- read file
         readline(mem_load_file, file_line);            -- Read line from mem_load_file file.  Don't use first line.
         ram_load_loop : while (not endfile(mem_load_file)) loop
            line_number := line_number + 1;
            readline(mem_load_file, file_line);         -- Read line from mem_load_file file.
            -- if (file_line'length >= 8) then               -- process only valid data lines
               read(file_line, file_addr, ok);             -- Read address field from file_line.
               if (not ok) then
                  assert false
                   report "Error reading address field from ram_chip_ld file!  Line: " & To_string(line_number)
                    severity failure;
               end if;
               read(file_line, delimiter_space);           -- Trash unused characters.
               read(file_line, file_data, ok);             -- Read data field from file_line.
               if (not ok) then
                  assert false
                   report "Error reading data field from ram_chip_ld file!"
                    severity failure;
               end if;
               i_addr := HEX_TO_INTGR(file_addr);    
               mem_core(i_addr) <= HEX_TO_STD_LOGIC_VECTOR(file_data,nibbles,word_size - 1);
            -- end if;
         end loop ram_load_loop;
   end load_mem_proc;

begin -- architecture


    dq <= memcore(conv_integer(unsigned(addr))) 
          when (cs_n='0' and ce='1' and we_n = '1' and oe_n = '0') 
          else (others => 'Z');

       write_process: process(we_n)
         VARIABLE iadr              : integer;
       begin
         if(we_n'event and we_n='1')then
            if(cs_n='0' and ce='1' )then
                  iadr := conv_integer(unsigned(addr));
                  memcore(iadr) <= dq;
            end if;
         end if;
      end process;

       init_process : process(load)
         VARIABLE init_ram : integer := 0 ;
       begin
         if(load=0)then
                load_mem_proc(ram_chip_ld, memcore);
		load <= 1;
         end if;
      end process;
end sram128kx8_arch;


