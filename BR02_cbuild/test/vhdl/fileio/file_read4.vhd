-- Testing reading of bit_vector of size < 8.
-- For this UtIStream::operator>> (char & ) has been added.

entity file_read4 is
  port ( in1 : bit;
         out1 : out bit);
end file_read4;

architecture file_read4 of file_read4 is
  type bitFile is file of bit;
 
  file myFile : bitFile open READ_MODE is "file_read4.in";
  begin
    process (in1) 
      variable in2 : bit := '0';
      begin
        if not(ENDFILE(myFile)) then 
           READ(myFile, in2); 
           out1 <= in2;
        end if;
    end process;

end file_read4;
