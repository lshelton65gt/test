

-- Testing explicit opening of a file which is not writeable
-- a status other than OPEN_OK is to be returned.

entity file_open4 is
 port ( INPUT1 : bit_vector(7 downto 0);
        output1 : out bit_vector(7 downto 0));
end file_open4;

architecture file_open4 of file_open4 is

    type myIntFile is file of integer;
    file myfile : myIntFile;
    constant filename : string := "nosuchdir/nosuchfile"; -- a file that cannot be opened
  begin
    process (INPUT1)
      variable stat : FILE_OPEN_STATUS;
      begin
        FILE_OPEN(stat, myfile, filename, WRITE_MODE);
        if (stat = OPEN_OK) then
           output1 <= "11111111";
        elsif ((stat = STATUS_ERROR) or (stat = MODE_ERROR) or (stat = NAME_ERROR) ) then
           output1 <= "00111100";
        end if;
        FILE_CLOSE(myfile);
      end process;
end file_open4;
