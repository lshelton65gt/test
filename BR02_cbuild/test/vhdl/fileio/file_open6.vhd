-- last mod: Mon Oct 21 09:42:14 2013
-- filename: test/vhdl/fileio/file_open6.vhd
-- Description:  This test checks the handling of file open when the filename
-- is a non-const variable.  
-- This testcase should fail with message about non-constant filename.

entity file_open6 is
 port ( INPUT1 : bit_vector(7 downto 0);
        output1 : out bit_vector(7 downto 0);
        output2 : out bit_vector(7 downto 0)
        );
end file_open6;

architecture file_open6 of file_open6 is

    type myIntFile is file of integer;
    file myfile : myIntFile;
  begin
    process (INPUT1)
      variable filename : string(1 to 14) := "does_not_exist"; -- filename string that is not a constant, and the file does not exist
      variable filename2 : string(1 to 14) := "file_open6.vhd"; -- a constant string
      variable stat : FILE_OPEN_STATUS;
      begin
        FILE_OPEN(stat, myfile, filename, READ_MODE);
--        FILE_OPEN(stat, myfile, "does_not_exist", READ_MODE);
        if (stat = OPEN_OK) then
           output1 <= "11111111";
        elsif ((stat = STATUS_ERROR) or (stat = MODE_ERROR) or (stat = NAME_ERROR) ) then
           output1 <= "00111100";
        end if;
        FILE_CLOSE(myfile);
        -- now try to read a file that we know exists.  This also assures that filename variable is not a constant
        filename := filename2;
        FILE_OPEN(stat, myfile, filename, READ_MODE);
--        FILE_OPEN(stat, myfile, "file_open6.vhd", READ_MODE);
        if (stat = OPEN_OK) then
           output2 <= "11111111";
        elsif ((stat = STATUS_ERROR) or (stat = MODE_ERROR) or (stat = NAME_ERROR) ) then
           output2 <= "00111100";
        end if;
        FILE_CLOSE(myfile);
      end process;
end file_open6;
