-- There are two bitvector per line of file.
-- Read them and populate an array of bitvectors.
-- access the populated array by the input index.

-- The array/memory is populated/initialized only once at the first posedge clock event.
-- The 'index' input randomly tests the memory initialized by the input file.
 
use std.textio.all;

entity readwritememory2 is
  port ( clk : bit; index : integer range 0 to 25;
         out1, out2 : out bit_vector(9 downto 0));
end;

architecture readwritememory2 of readwritememory2 is
  subtype word is bit_vector(9 downto 0);

  type storage_array is
         array(natural range 0 to 25) of word;

  signal storage : storage_array;

  file rd : text open READ_MODE is "readwritememory2.in";

  signal init : boolean := false;
begin
  process (clk, init)
    variable lyne : line;
    variable i : natural := 0; 
    variable vint1, vint2 : word := "0000000000";
  begin
      if clk'event and clk='1' then 
        if (init = false) then
          while (NOT endfile(rd))
          loop
            readline(rd, lyne);
            read(lyne, vint1);  -- 1st bit_vector of line
            read(lyne, vint2);  -- 2nd bit_vector of line
     
            storage(i) <= vint1;
            storage(i + 1) <= vint2;
            i := i + 2;
          end loop;
  
          init <= true;
        end if;

        out1 <= storage(index);
        out2 <= storage((index + 1) mod 26);

      end if;

  end process;

end;

