-- write a bit_vector using LINE variable
use std.textio.all;

entity writeline2 is
  port (
         int1, int2 : in bit_vector( 0 to 69);
         out1, out2 : out bit_vector( 0 to 69));
end;

architecture writeline2 of writeline2 is
  type my_file is file of string;
  file wrt : text open WRITE_MODE is "writeline2.wr1.out";
begin
  process (int1, int2)
    variable lyne : line;
  begin
      write(lyne, int1);
      write(lyne, int2);
      writeline(wrt, lyne); -- insert a line containing the 2 bit_vectors.
    out1 <= int1;
    out2 <= int2;
  end process;
end;

