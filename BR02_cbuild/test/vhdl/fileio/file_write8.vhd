-- Writing strings to a file

entity file_write8 is
  port ( in1 : character;
         out1 : out character);
end ;

architecture file_write8 of file_write8 is
  type strFile is file of string;
 
  file myFile : strFile open WRITE_MODE is "file_write8.wr1.out";
  constant name : string(1 to 9) := "Carbonzds";
  begin
    process (in1) 
      begin
        write(myFile, name); 
        write(myFile, "checking"); 
        write(myFile, "works adsjalsdjasd asdasdasdfafsfhlasdf  adasdasdad"); 
        write(myFile, "%%%%%"); 
        write(myFile, "101011"); 
        out1 <= in1;
    end process;

end ;
