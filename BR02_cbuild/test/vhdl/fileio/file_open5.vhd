-- last mod: Thu Aug 23 16:45:19 2007
-- filename: test/vhdl/fileio/file_open5.vhd
-- Description:  This test checks the handling of file open when the filename
-- is an empty string.  This testcase should generate a runtime error because of
-- the empty filename string
-- this test is similar to the test in comment 8 of bug 7535
entity file_open5 is
 port ( INPUT1 : bit_vector(7 downto 0);
        output1 : out bit_vector(7 downto 0));
end file_open5;

architecture file_open5 of file_open5 is

    type myIntFile is file of integer;
    file myfile : myIntFile;
    constant filename : string := ""; -- empty filename string
  begin
    process (INPUT1)
      variable stat : FILE_OPEN_STATUS;
      begin
        FILE_OPEN(stat, myfile, filename, READ_MODE);
        if (stat = OPEN_OK) then
           output1 <= "11111111";
        elsif ((stat = STATUS_ERROR) or (stat = MODE_ERROR) or (stat = NAME_ERROR) ) then
           output1 <= "00111100";
        end if;
        FILE_CLOSE(myfile);
      end process;
end file_open5;
