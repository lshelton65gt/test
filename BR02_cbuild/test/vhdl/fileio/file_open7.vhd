-- Implicit opening of a file.
-- open a file during declaration, with empty string as filename
-- this is expected to have a runtime error because of the empty string
entity file_open7 is 
  port (in1 : bit; out1 : out integer);
end;
    
architecture file_open7 of file_open7 is 
  type my_file is file of integer;
  file jet : my_file open READ_MODE is "";  -- note empty string used here
begin
  process (in1)
      variable int1 : integer;
  begin
    for i in 0 to 10
     loop
       if (NOT endfile(jet)) then
         read(jet, int1);
         out1 <= int1;   -- process is scheduled only once ?? 
       end if;
    end loop;
  end process;
end;

