-- Read all bit_vectors from a file, one line at a time using the LINE variable.
use std.textio.all;

entity readline2 is
  port ( in1 : bit;
         int1, int2, int3, int4 : out bit_vector(9 downto 0));
end;

architecture readline2 of readline2 is
  subtype bit10 is bit_vector(9 downto 0);
  type my_file is file of string;
  file rd : text open READ_MODE is "readline2.in";
begin
  process 
    variable lyne : line;
    variable vint1, vint2, vint3, vint4 : bit10 := "0000000000";
  begin
    while (NOT endfile(rd))
    loop
      readline(rd, lyne);
      read(lyne, vint1);  -- 1st bit_vector of line
      read(lyne, vint2);  -- 2nd bit_vector of line
      read(lyne, vint3);  -- 3rd bit_vector of line
      read(lyne, vint4);  -- 4th bit_vector of line.
     int1 <= vint1;
     int2 <= vint2;
     int3 <= vint3;
     int4 <= vint4; -- evenually gets the last bit_vector of last line.
    end loop;
  end process;
end;

