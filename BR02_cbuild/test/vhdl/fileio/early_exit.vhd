-- here a task has a disable that causes early exit from the task.
-- This test tests the use of STD.TEXTIO.OUTPUT file object in 
-- file -I/O procedures such as write and writeline.

library IEEE;
use STD.TEXTIO.all;
use IEEE.STD_LOGIC_TEXTIO.all;
use IEEE.STD_LOGIC_1164.all;

entity early_exit is
  port(clk: in std_logic;
       in1: in std_logic;
       out1: out std_logic);
end;

architecture early_exit of early_exit is
  procedure proc_a(out1: out std_logic) is
    variable out_line: LINE;
  begin  -- proc_a
    write( STD.TEXTIO.OUTPUT, "start task: " );
    if in1 = '0' then
      return;
    end if;
    write( OUTPUT, "in task, this line should only be seen if in1 is 1, here a is: ");
    write( out_line, in1);
    writeline( STD.TEXTIO.OUTPUT, out_line );
    out1 := in1;
  end proc_a;

begin  -- early_exit
  process(clk)
    variable  temp_out: std_logic;
  begin
    if clk'event and clk = '1' then
      proc_a(temp_out);
      out1 <= temp_out;
    end if;
  end process;
end early_exit;
