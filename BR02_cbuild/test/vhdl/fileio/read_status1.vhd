-- Testing The return status of the READ() procedure.
-- All entries in the input file are legal.
-- The test read_status2 has some illegal entries too.

library std;
use std.textio.all;
entity read_status1 is
  port ( clk : bit;
         out1 : out bit_vector(9 downto 0);
         outEof, outStatus : out boolean);
end read_status1;

architecture read_status1 of read_status1 is
  subtype bit10 is bit_vector(9 downto 0);
  file myFile : text open READ_MODE is "read_status1.in";

  begin
    process (clk)
      variable value : bit10 := "0000000000";
      variable boolval : boolean := false;
      variable lyne : line;
      begin
        if clk'event and clk='1' then
         if not(ENDFILE(myFile)) then
            outEof <= false;
            readline(myFile, lyne);
            READ(lyne, value, boolval);
            outStatus <= boolval;
            if (boolval) then
              out1 <= value; -- reads the last value read from the file.
            end if;
         else
           outEof <= true;
         end if;
       end if;
    end process;

end read_status1;

