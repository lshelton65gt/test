entity file_rw2 is
  port ( in1 : integer;
         out1 : out integer);
end file_rw2;

architecture file_rw2 of file_rw2 is
  type intFile is file of integer;
 
  file myRFile : intFile open READ_MODE is "Read2";
  file myWFile : intFile open WRITE_MODE is "RW2";

  begin
    process (in1) 
     variable value : integer := 5;
      begin
        while (not ENDFILE(myRFile)) loop 
           READ(myRFile, value);
           WRITE(myWFile, value);
        end loop;
        out1 <= value;
    end process;

end file_rw2;
