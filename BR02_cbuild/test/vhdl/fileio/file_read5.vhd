--- Reading integer using implicit read() procedure, inside a while loop

entity file_read5 is
  port ( in1 : integer;
         out1 : out integer);
end file_read5;

architecture file_read5 of file_read5 is
  type intFile is file of integer;
 
  file myFile : intFile open READ_MODE is "file_read5.in";

  begin
    process (in1) 
     variable value : integer := 5;
      begin
        while (not ENDFILE(myFile)) loop 
           READ(myFile, value); -- reads one int value at a time.
           out1 <= value; -- gets the last value read from file eventually.
        end loop;
 
    end process;

end file_read5;
