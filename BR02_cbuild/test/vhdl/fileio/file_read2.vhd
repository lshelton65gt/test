-- Reading an integer from an implicitly defined read procedure
entity file_read2 is
  port ( in1 : integer;
         out1 : out integer);
end file_read2;

architecture file_read2 of file_read2 is
  type intFile is file of integer;
 
  file myFile : intFile open READ_MODE is "file_read2.in";

  begin
    process (in1) 
     variable value : integer := 5;
      begin
        if not(ENDFILE(myFile)) then 
           READ(myFile, value);
           out1 <= value;
        end if;
 
    end process;

end file_read2;
