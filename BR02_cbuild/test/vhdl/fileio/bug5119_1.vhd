-- this testcase is an attempt to resolve the problem from bug5119_0
-- with 1.3694  this would cause an assert in cbuild
-- test is from Northrop,

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use std.textio.all;
-- was sram128kx8_ent
entity bug5119_1 is
   generic
      (ram_chip_ld : string (1 to 18) := "bug5119_1.mem_data");
                                          
   port
      (cs_n : in    std_logic;
       ce   : in    std_logic;
       we_n : in    std_logic;
       oe_n : in    std_logic;
       addr : in    std_logic_vector(16 downto 0);
       dq   : inout std_logic_vector(7 downto 0));


  constant word_size  : integer :=    8; -- number of bits in a data word
  constant block_size : integer := 1024; -- number of data words in a memory block

  constant max_rows   : integer :=  128; -- number of memory blocks in the whole memory
  constant addr_size  : integer :=    17; -- number of bits in a data word

end bug5119_1;

-------------------------------------------------------------------------------------
-- Architecture Body
-------------------------------------------------------------------------------------
architecture bug5119_1_arch of bug5119_1 is

  -----------------------------------------------------------------------------
  -- SIGNAL DECLARATIONS
  -----------------------------------------------------------------------------

   signal load            : integer := 0;
   signal write_data      : std_logic_vector(word_size -1 downto 0);      

   type  mem_type IS array ((block_size*max_rows)-1 downto 0) of std_logic_vector(word_size-1 downto 0);

   signal memcore           : mem_type;



   function To_string(input_integer : integer) return string is
       -- to convert larger integer values, it's necessary to change only the length of output_string variable on the next line
       variable output_string : string (1 to 5);
      	variable first_nonzero_char : integer := 1;
      	variable subtract_total : integer := 0;
      	variable pwr_o_ten : integer;
      	variable char_integer : integer;
   begin
      	-- if (input_integer > 10**output_string'length - 1) then
      	if (input_integer > 10**5 - 1) then
      		assert false
      		    report "Integer exceeded conversion function's range. (Macro file size has exceeded maximum number of lines.)"
      		    severity failure;
           for i in output_string'low to output_string'high loop
               output_string(i) := '?';
   	    end loop;
      	else
           -- pwr_o_ten := (output_string'length - 1);
           pwr_o_ten := (4);
      	    for i in output_string'low to output_string'high loop
      			char_integer := (input_integer - subtract_total)/10**pwr_o_ten;
      			case char_integer is 
      				when 0 => output_string(i) := '0';
      				when 1 => output_string(i) := '1';
      				when 2 => output_string(i) := '2';
      				when 3 => output_string(i) := '3';
      				when 4 => output_string(i) := '4';
      				when 5 => output_string(i) := '5';
      				when 6 => output_string(i) := '6';
      				when 7 => output_string(i) := '7';
      				when 8 => output_string(i) := '8';
      				when 9 => output_string(i) := '9';
      				when others => output_string(i) := '0';
      							   assert false
      								   report "Unexpected integer value in function to_string"
      								   severity failure;
      			end case;
      		    subtract_total := subtract_total + char_integer*10**pwr_o_ten;
      		    pwr_o_ten := pwr_o_ten - 1;
           end loop;
   		-- delete leading zeros from string to be returned (comment this loop out if leading zeros are desired)
   		for i in output_string'low to output_string'high loop
               if output_string(i) /= '0' then 
                   first_nonzero_char := i;
                   exit;
               elsif output_string(i) = '0' and (i = output_string'high) then
                   first_nonzero_char := output_string'high;
               end if;
           end loop;
      	end if;
       return output_string(first_nonzero_char to output_string'high);
   end To_string;


   procedure load_mem_proc(file_ptr : in string; signal mem_core : inout mem_type) is
        file mem_load_file : text is in file_ptr;
        variable file_line       : line;
        variable file_addr       : std_logic_vector (addr_size-1 downto 0);
        variable file_data       : std_logic_vector (word_size-1 downto 0);
        variable line_number     : integer := 0;
        variable ok              : boolean := false;
        variable i_addr          : integer;
   begin

         -- read file
         readline(mem_load_file, file_line);            -- Read line from mem_load_file file.  Don't use first line.
         ram_load_loop : while (not endfile(mem_load_file)) loop
            line_number := line_number + 1;
            readline(mem_load_file, file_line);         -- Read line from mem_load_file file.
            -- if (file_line'length >= 8) then               -- process only valid data lines
               hread(file_line, file_addr, ok);             -- Read address field from file_line.
               if (not ok) then
                  assert false
                   report "Error reading address field from ram_chip_ld file!  Line: " & To_string(line_number)
                    severity failure;
               end if;
               hread(file_line, file_data, ok);             -- Read data field from file_line.
               if (not ok) then
                  assert false
                   report "Error reading data field from ram_chip_ld file!"
                    severity failure;
               end if;
               i_addr := conv_integer(unsigned(file_addr));    
               mem_core(i_addr) <= file_data;
            -- end if;
         end loop ram_load_loop;
   end load_mem_proc;

begin -- architecture


    dq <= memcore(conv_integer(unsigned(addr))) 
          when (cs_n='0' and ce='1' and we_n = '1' and oe_n = '0') 
          else (others => 'Z');

       write_process: process(we_n)
         VARIABLE iadr              : integer;
       begin
         if(we_n'event and we_n='1')then
            if(cs_n='0' and ce='1' )then
                  iadr := conv_integer(unsigned(addr));
                  memcore(iadr) <= dq;
            end if;
         end if;
      end process;

       init_process : process(load)
         VARIABLE init_ram : integer := 0 ;
       begin
         if(load=0)then
                load_mem_proc(ram_chip_ld, memcore);
		load <= 1;
         end if;
      end process;
end bug5119_1_arch;


