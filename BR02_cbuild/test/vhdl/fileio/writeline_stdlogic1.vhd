-- write a std_logic_vector using LINE variable
library STD;
use STD.textio.all;
library ieee;
use ieee.std_logic_textio.all;
use IEEE.std_logic_1164.all;

entity writeline_stdlogic1 is
  port (
         int1, int2 : in std_logic_vector( 0 to 69);
         out1, out2 : out std_logic_vector( 0 to 69));
end;

architecture writeline_stdlogic1 of writeline_stdlogic1 is
  type my_file is file of string;
  file wrt : text open WRITE_MODE is "writeline_stdlogic1.wr1.out";
begin
  process (int1, int2)
    variable lyne : line;
  begin
      owrite(lyne, int1);
      hwrite(lyne, int2);
      writeline(wrt, lyne); -- insert a line containing the 2 std_logic_vectors in diff format.
    out1 <= int1;
    out2 <= int2;
  end process;
end;

