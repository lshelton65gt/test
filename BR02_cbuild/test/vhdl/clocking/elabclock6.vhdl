-- This test must be elaborated before the clock can be properly validated
library ieee;
use ieee.std_logic_1164.all;

entity dut is
  generic (
    GENVAL : integer );
  port (
    clk, datain : in  std_logic;
    dataout     : out std_logic);
end dut;

architecture arch of dut is

  constant CONSTVAL : integer := 4;
begin

  the_process: process (clk, datain)
    variable temp : std_logic;
  begin
    temp := datain;
    if GENVAL = CONSTVAL then
      if clk'event and clk = '1' then
        dataout <= temp;
      end if;
    else
      dataout <= temp;
    end if;
  end process the_process;
end arch;


library ieee;
use ieee.std_logic_1164.all;

entity elabclock6 is
  port (
    clk, datain : in  std_logic;
    dataout     : out std_logic);
end elabclock6;

architecture arch of elabclock6 is

  component dut
    generic ( GENVAL : integer);
    port ( clk, datain : in  std_logic;
           dataout     : out std_logic);
  end component;
  
begin

  the_component : dut
    generic map (GENVAL => 4)
    port map (clk     => clk,
              datain  => datain,
              dataout => dataout);

end arch;
