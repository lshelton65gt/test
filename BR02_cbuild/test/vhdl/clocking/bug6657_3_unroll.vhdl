-- this  is almost the same test as bug6657_3 but run with -vhdlLoopUnroll to show
-- that with loop unrolling the constant can be recognized.  However the test
-- is wrong because the line vec(var-1) <= '0' will be an out-of-range selection
-- when i is 1.  Which is a runtime error. when run with -vhdlLoopUnroll this
-- test checks that the out-of-range message is printed.

-- Although the variable can be computed to a constant statically at
-- compile time, cbuild throws the alert indicating reset block has
-- dynamic select.
library ieee;
use ieee.std_logic_1164.all;

entity bug6657_3_unroll is
  
  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0);
    out2 : out std_logic_vector(3 downto 0));

end bug6657_3_unroll;

architecture arch of bug6657_3_unroll is

  signal vec : std_logic_vector(3 downto 0);
  signal vec1 : std_logic_vector(3 downto 0);
  constant c1 : integer := 1;
  constant c2 : integer := 1;
  constant c3 : integer := c1 + c2;
  
begin  -- arch

  seqproc: process (clk, rst)
   variable var : integer;
  begin  -- process seqproc
    if clk'event and clk = '1' then  -- rising clock edge
      for i in 3 downto 0 loop
        vec(i) <= in1(i);
      end loop;  -- i
      vec1 <= in1;
    end if;
    if rst = '0' then                   -- asynchronous reset (active low)
      for i in 4 downto 1 loop
        var := i - c3 + 1;
        vec(var-1) <= '0';
      end loop;  -- i
    end if;
  end process seqproc;

  out1 <= vec;
  out2 <= vec1;
  
end arch;
