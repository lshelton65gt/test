-- Test using a wait statement as the sensitivity list instead of the
-- actual process sensitivity list.  This, um, "interesting" coding
-- style comes from Intel Hudson.
library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity bug5907 is
    port ( clk0, din : in std_logic;
           dout : out std_logic);
end bug5907;

architecture bug5907_arch of bug5907 is
begin
  P0: process
  begin
    wait on clk0;
    if (clk0'event and clk0 = '1') then
      dout <= din;
    end if;
  end process P0;
end bug5907_arch;
