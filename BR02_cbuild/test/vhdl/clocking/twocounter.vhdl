-- This demonstrates using a variable for a flop instead of a signal.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity twocounter is
  port (clk1, clk2, reset1, reset2 : in std_logic;
        out1, out2 : out std_logic_vector(31 downto 0));
end twocounter;

architecture arch_twocounter of twocounter is

begin
  p1: process(clk1)
    variable out_tmp : std_logic_vector(31 downto 0);
  begin
    if (clk1'event and clk1 = '1') then
      if (reset1 = '1') then
        out_tmp := (others => '0');
      else
        out_tmp := out_tmp + 1;
      end if;
    end if;
    out1 <= out_tmp;
  end process;

  p2: process(clk2)
    variable out_tmp : std_logic_vector(31 downto 0);
  begin
    if (clk2'event and clk2 = '1') then
      if (reset2 = '1') then
        out_tmp := (others => '0');
      else
        out_tmp := out_tmp + 3;
      end if;
    end if;
    out2 <= out_tmp;
  end process;
  
end arch_twocounter;
