-- last mod: Thu Mar 20 2007
-- filename: test/vhdl/clocking/bug6373.vhdl
-- Description:  This test is trimmed from an example from TTPCom.
-- The issue was in the text of the following message:
-- bug6373.vhdl:55: Warning 3560: Process contains no clock statements
-- but this points to the line
--     if clk'event and clk='1' then
-- which confused the user because the indicated line appears to be a clock
-- statement.
-- Before the bug 6373 fix, there was no differentiation between empty process
-- and process with no clock statement. Now, cbuild reports different errors for
-- these two cases. So, the process pDataPipe3 reports Warning 3575.
-- Process p1 was added. The p1 is an empty process. So, it gets Warning 3560.
-- Warning 3560 was modified. Now the message is "Process contains an empty clock block".


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug6373 is
port (
clk,en,l : in std_logic;
mi,ei : unsigned (7 downto 0);
mo,eo : out unsigned (7 downto 0)
);
end entity;

architecture rev1 of bug6373 is

-- Record types used for complex math.
     type FpUnsigned is record
       Mantissa : unsigned(7 downto 0);
       Exponent : unsigned(7 downto 0);
     end record FpUnsigned;

    signal CorrISquared, CorrQSquared, FloatCorrI, FloatCorrQ : FpUnsigned;

begin

  loader : process (clk)
  begin
  	if clk'event and clk='1' then
  		if l = '1' then
  			FloatCorrI.Mantissa <= mi;
  			FloatCorrI.Exponent <= ei;
  			FloatCorrQ.Mantissa <= mi;
  			FloatCorrQ.Exponent <= ei;
  		end if;
  	end if;
  end process;

  pDataPipe3 : process (clk)
  begin
    if clk'event and clk='1' then                 --
--      if en='1' then 
--       no statements within this clock block
    end if;
  end process pDataPipe3;

 
  p1 : process (clk)
  begin
  end process p1;

  CorrISquared <= (mi,ei);
  mo <= CorrISquared.Mantissa;
  eo <= CorrISquared.Exponent;

end rev1;
