library ieee;
use ieee.std_logic_1164.all;

entity waitclock is
  port (
    clk, din : in  std_logic;
    dout     : out std_logic);
end waitclock;

architecture arch of waitclock is

begin

  process
  begin
    wait on clk until clk = '1';
    dout <= not din;
  end process;
end arch;
