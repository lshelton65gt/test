-- This tests that the process can be normalized into a format supported for
-- clock analysis.
library ieee;
use ieee.std_logic_1164.all;

entity bug4408 is
  port (
    en, clk, din : in  std_logic;
    dout         : out std_logic);
end bug4408;

architecture arch of bug4408 is

begin  -- arch

  p1: process( en, clk )
  begin
    if en = '1' then
      if clk'event and clk = '1' then
        dout <= din;
      end if;
    end if;
  end process p1;

end arch;
