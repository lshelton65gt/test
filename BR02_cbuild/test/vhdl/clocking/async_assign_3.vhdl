-- last mod: Wed Oct  23 13:22:04 2007
-- filename: test/vhdl/clocking/async_assign_3.vhdl
-- Description:  This test was inspired by bug 7679
-- Sequential statements above if/else clock block in a process are
-- allowed and synthesizable with some limitations;
-- 1) All rvalues on the right side of assignment should appear in the sensitivity
-- list.
-- 2) No signal should appear on left side of assignment statements in both
-- sequential statement and if/else block.
--
-- VSP compiler breaks this type of processes into two separate always block.
-- First one is combinational (with sequential statements), and second one
-- sequential with if/else block.



library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity async_assign_3 is
  port (
    clock    : in  std_logic;
    in_data  : in  std_logic;
    out1     : out std_logic;
    out2     : out std_logic;
    out3     : out std_logic;
    out4     : out std_logic_vector(3 downto 0));
end;

architecture arch of async_assign_3 is 
 signal counter         : std_logic_vector(3 downto 0) := "0000";
 signal strobe          : std_logic;
 signal cnt_str         : std_logic;
 signal in_data_d       : std_logic;
begin

  count1: process (clock)
  begin  -- process count1
    if clock'event and clock = '1' then  -- rising clock edge
      if cnt_str = '1' then
        counter <= "0000";
      else
        counter <= counter + 1;
      end if;
    end if;
  end process count1;

  
  main: process (clock, in_data, in_data_d, counter)
  begin 

    -- two lines below are tested
    strobe <= in_data_d and (not in_data);  
    cnt_str <= counter(3) and (not counter(2));
    
    if clock'event and clock = '1' then 
      in_data_d <= in_data;
    end if;
  end process;

  out1 <= in_data_d;
  out2 <= strobe;
  out3 <= cnt_str;
  out4 <= counter;
  
end;


