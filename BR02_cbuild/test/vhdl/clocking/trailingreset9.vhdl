-- bug5075.vhdl tests a single trailing reset and bug5000.vhdl tests multiple
-- trailing resets.
-- This tests a process driving multiple signals when not all the signals are
-- reset all the time.
library ieee;
use ieee.std_logic_1164.all;

entity trailingreset9 is
    port (
    d1, d2, d3    : in  std_logic_vector(3 downto 0);
    q1, q2, q3, q4 : out std_logic_vector(3 downto 0);
    clk, reset1 : in  std_logic;
    reset2, reset3 : in std_logic);
end;

architecture arch of trailingreset9 is
begin
  backwards: process (clk, reset1, reset2, reset3)
  begin
    if clk'event and clk = '1' then
      q1 <= d1;
      q2 <= d2;
      q3 <= d3;
    end if;
    if reset1 = '0' then 
      q1 <= "0000";
      q3 <= "1010";
    end if;
    if '0' = reset2 then 
      q1 <= "0001";
      q4 <= "1111";
    end if;
    if reset3 = '0' then 
      q3 <= "0110";
    end if;
  end process backwards;
end arch;
