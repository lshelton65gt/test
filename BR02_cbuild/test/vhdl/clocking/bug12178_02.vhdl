-- June 2008
-- This test uses not syntesizable clock construct.
-- By customer request CBUILD now supports such construct.
-- This file uses the same construct as in file bug12178_01.
-- The differences are: the clock and reset are of type bit and
-- the reset is active high.

library ieee;
use ieee.std_logic_1164.all;

entity bug12178_02 is
  
  port (
    rst  : in  bit;
    clk  : in  bit;
    in1  : in  std_logic;
    out1 : out std_logic);

end bug12178_02;

architecture arch2 of bug12178_02 is

begin
  
  p1 : PROCESS (rst, clk)
  BEGIN
    IF (rst = '1') THEN
        out1 <= '0';
    ELSIF (rst = '0') THEN
      IF (clk = '1' AND clk'EVENT) THEN
        out1 <= in1;
      END IF;
    ELSE
        out1 <= 'X';
    END IF;
  END PROCESS;

end;
