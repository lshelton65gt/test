-- bug5075.vhdl tests a single trailing reset and bug5000.vhdl tests multiple
-- trailing resets.
-- This tests support for an elsif on a reset block
library ieee;
use ieee.std_logic_1164.all;

entity trailingreset6 is
    port (
    d           : in  std_logic_vector(3 downto 0);
    q           : out std_logic_vector(3 downto 0);
    clk, reset1 : in  std_logic;
    reset2, reset3 : in std_logic);
end;

architecture arch of trailingreset6 is
begin
  backwards: process (clk, reset1, reset2, reset3)
    variable v : std_logic_vector(3 downto 0);
  begin
    if clk'event and clk = '1' then
      q <= d;
    end if;
    if reset1 = '0' then 
      q <= "0000";
    elsif reset1 = reset2 then
      q <= "1111";
    end if;
    if reset2 = '0' then 
      q <= "0001";
    end if;
    if reset3 = '0' then 
      q <= "0010";
    end if;
  end process backwards;
end arch;
