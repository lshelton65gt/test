-- This tests for the ability to not detect a clock when other
-- conditions are ORed with the clock in the same if condition.
entity bug4198_1 is
  port (
    clk : in bit;
    en : in bit;
    din : in bit;
    dout : out bit);
end;

architecture rtl of bug4198_1 is
begin
  process (clk, en, din)
  begin
    if ( clk'event and clk='1' ) or en='1' then
      dout <= din;
    end if;
  end process;
end rtl;

-- Is this what needs to be populated for the above process?
--replacement: process (clk, en, din)
--begin
--  if en = '1' and din = '1' then
--    dout <= '1';
--  elsif en = '1' and din = '0' then
--    dout <= '0';
--  elsif clk'event and clk = '1' then
--    dout <= din;
--  end if;
--end process replacement;

-- or, is it this:
--replacement2: process(clk, en, din)
--  begin
--    if en = '1' then
--      dout <= din;
--    elsif clk = '1' and clock'event then
--      dout <= din;
--    end if;
--  end;
