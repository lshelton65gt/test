-- This test checks the ability to correctly populate unconditional statements
-- that follow the clock block when there are also trailing resets in the process.
entity bug5597_1 is
  port(clk, pause_any : in bit;
       rst, rst2 : in bit;
       pc : out bit_vector(3 downto 0));
end;
architecture logic of bug5597_1 is
  -- pc_reg must be initialized to mimic VHDL simulator initialization in
  -- cbuild.  Simulation runs each process once at time 0 to initialize; this
  -- will cause the reset block to fire at simulation.  cbuild does not do
  -- this, so we need to force the value that would be computed by simulation
  -- initialization onto pc_reg.  
  signal pc_reg : bit_vector(3 downto 2) := "01";
begin

  pc_next: process(clk, pause_any, pc_reg, rst, rst2)
    variable pc_next : bit_vector(3 downto 2);
  begin
    pc_next := "00";
    if pause_any = '1' then
      pc_next := pc_reg;
    end if;

    if clk'EVENT and clk = '1' then
      pc_reg <= pc_next;
    end if;

    if rst = '0' then
      pc_reg <= "01";
    end if;

    pc <= pc_reg & "00";

    if rst2 = '0' then
      pc_reg <= "11";
    end if;
  end process;
end;
