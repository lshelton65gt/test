-- This tests having more resets than are supported
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity trailingreset22 is
    port (
    d1 : in  std_logic_vector(7 downto 0);
    q1 : out std_logic_vector(7 downto 0);
    reset : in std_logic_vector(30 downto 0);
    clk: in  std_logic);
end;

architecture arch of trailingreset22 is
begin
  backwards: process (clk, reset)
  begin
    if clk'event and clk = '1' then
      q1 <= d1;
    end if;
    if reset(30) = '0' then 
      q1 <= "00011110";
    end if;
    if reset(29) = '0' then 
      q1 <= "00011101";
    end if;
    if reset(28) = '0' then 
      q1 <= "00011100";
    end if;
    if reset(27) = '0' then 
      q1 <= "00011011";
    end if;
    if reset(26) = '0' then 
      q1 <= "00011010";
    end if;
    if reset(25) = '0' then 
      q1 <= "00011001";
    end if;
    if reset(24) = '0' then 
      q1 <= "00011000";
    end if;
    if reset(23) = '0' then 
      q1 <= "00010111";
    end if;
    if reset(22) = '0' then 
      q1 <= "00010110";
    end if;
    if reset(21) = '0' then 
      q1 <= "00010101";
    end if;
    if reset(20) = '0' then 
      q1 <= "00010100";
    end if;
    if reset(19) = '0' then 
      q1 <= "00010011";
    end if;
    if reset(18) = '0' then 
      q1 <= "00010010";
    end if;
    if reset(17) = '0' then 
      q1 <= "00010001";
    end if;
    if reset(16) = '0' then 
      q1 <= "00010000";
    end if;
    if reset(15) = '0' then 
      q1 <= "00001111";
    end if;
    if reset(14) = '0' then 
      q1 <= "00001110";
    end if;
    if reset(13) = '0' then 
      q1 <= "00001101";
    end if;
    if reset(12) = '0' then 
      q1 <= "00001100";
    end if;
    if reset(11) = '0' then 
      q1 <= "00001011";
    end if;
    if reset(10) = '0' then 
      q1 <= "00001010";
    end if;
    if reset(9) = '0' then 
      q1 <= "00001001";
    end if;
    if reset(8) = '0' then 
      q1 <= "00001000";
    end if;
    if reset(7) = '0' then 
      q1 <= "00000111";
    end if;
    if reset(6) = '0' then 
      q1 <= "00000110";
    end if;
    if reset(5) = '0' then 
      q1 <= "00000101";
    end if;
    if reset(4) = '0' then 
      q1 <= "00000100";
    end if;
    if reset(3) = '0' then 
      q1 <= "00000011";
    end if;
    if reset(2) = '0' then 
      q1 <= "00000010";
    end if;
    if reset(1) = '0' then 
      q1 <= "00000001";
    end if;
    if reset(0) = '0' then 
      q1 <= "00000000";
    end if;
  end process backwards;

end arch;
