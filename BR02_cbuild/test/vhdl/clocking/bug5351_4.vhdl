-- This tests that multiple statically elaboratable if statements in a
-- combinational process are correctly elaborated in the Jaguar domain.
library ieee;
use ieee.std_logic_1164.all;

entity bug5351_4 is
  generic (
    NUMBER_OF_UARTS : integer := 2);
  port (
    din   : in  std_logic_vector(7 downto 0);
    dout  : out std_logic_vector(7 downto 0);
    din2  : in  std_logic;
    dout2 : out std_logic;
    sel   : in std_logic );

end;

architecture arch of bug5351_4 is

begin
  p1: process (din, din2, sel)
  begin
    dout <= din;
    dout2 <= din2;
    if (NUMBER_OF_UARTS > 0) then
      if sel = '1' then
        dout(7 downto 5) <= din(2 downto 0);
        dout2 <= din2;        
      end if;
    end if;
    if (NUMBER_OF_UARTS > 2) then
      if sel = '0' then
        dout(2 downto 0) <= din(7 downto 5);
        dout2 <= not din2;
      end if;
    end if;
  end process p1;
end arch;
