--make sure we handle the priority of multiple leading resets and a
--trailing reset properly
library ieee;
use ieee.std_logic_1164.all;
entity trailingreset23 is
  port (
    clk  : in std_logic;
    rst1, rst2, rst3 : in std_logic;
    in1  : in std_logic;
    out1  : out std_logic);
end trailingreset23;

architecture trailingreset23 of trailingreset23 is
begin
  process (clk, rst1, rst2, rst3)
  begin
    if (rst1 = '1') then
      out1 <= '0';
    elsif (rst2 = '1') then
      null ;
    elsif (clk'event and clk = '1') then
      out1 <= in1;
    end if;

    if rst3 = '1' then
      out1 <= '1';
    end if;
  end process;

end trailingreset23;

