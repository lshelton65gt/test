-- This tests multiple instances of the same object requiring static
-- elaboration.
library ieee;
use ieee.std_logic_1164.all;

entity flop is
  generic (
    edge   : integer := 0;
    invert : integer := 0;
    set    : integer := 0);
  port (
    d   : in std_logic;
    q   : out std_logic;
    rst : in std_logic;
    ck  : in std_logic);
end;

architecture behav of flop is
  signal q1 : std_logic;
begin  -- behav

  p: process (ck, rst)
  begin  -- process p
    if set /= 1 then   
      if rst = '0' then                   -- asynchronous reset (active low)
        q1  <= '0';
      else
        if edge = 0 then
          if ck'event and ck = '0' then 
            q1 <= d;
          end if;  
        else
          if ck'event and ck = '1' then    -- rising clock edge
            q1 <= d;
          end if;
        end if;
      end if;
    else
      if rst = '0' then                   -- asynchronous reset (active low)
         q1  <= '1';
      else
        if edge = 0 then
          if ck'event and ck = '0' then 
            q1 <= d;
          end if;  
        else
          if ck'event and ck = '1' then    -- rising clock edge
            q1 <= d;
          end if;
        end if;
      end if;
    end if;
  end process p;
  
  q <= q1 when invert = 0
       else not q1;

end behav;

library ieee;
use ieee.std_logic_1164.all;

entity bug5351_6 is
  port (
    clk, rst : in std_logic;
    d : in  std_logic_vector(7 downto 0);
    q : out std_logic_vector(7 downto 0));
end bug5351_6;

architecture arch of bug5351_6 is
component flop is
  generic (
    edge   : integer;
    invert : integer;
    set    : integer);
  port (
    d   : in std_logic;
    q   : out std_logic;
    rst : in std_logic;
    ck  : in std_logic);
end component;
begin

  inst0 : flop generic map (
    edge   => 0,
    invert => 0,
    set    => 0)
    port map (
      ck  => clk,
      rst => rst,
      d   => d(0),
      q   => q(0));
  
  inst1 : flop generic map (
    edge   => 0,
    invert => 0,
    set    => 1)
    port map (
      ck  => clk,
      rst => rst,
      d   => d(1),
      q   => q(1));
  
  inst2 : flop generic map (
    edge   => 0,
    invert => 1,
    set    => 0)
    port map (
      ck  => clk,
      rst => rst,
      d   => d(2),
      q   => q(2));
  
  inst3 : flop generic map (
    edge   => 0,
    invert => 1,
    set    => 1)
    port map (
      ck  => clk,
      rst => rst,
      d   => d(3),
      q   => q(3));
  
  inst4 : flop generic map (
    edge   => 1,
    invert => 0,
    set    => 0)
    port map (
      ck  => clk,
      rst => rst,
      d   => d(4),
      q   => q(4));
  
  inst5 : flop generic map (
    edge   => 1,
    invert => 0,
    set    => 1)
    port map (
      ck  => clk,
      rst => rst,
      d   => d(5),
      q   => q(5));
  
  inst6 : flop generic map (
    edge   => 1,
    invert => 1,
    set    => 0)
    port map (
      ck  => clk,
      rst => rst,
      d   => d(6),
      q   => q(6));
  
  inst7 : flop generic map (
    edge   => 1,
    invert => 1,
    set    => 1)
    port map (
      ck  => clk,
      rst => rst,
      d   => d(7),
      q   => q(7));

end arch;
