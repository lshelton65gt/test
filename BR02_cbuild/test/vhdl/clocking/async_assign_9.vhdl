-- last mod: Wed Oct  23 13:22:04 2007
-- Description:  This test was inspired by bug 7679
-- See comment in test/vhdl/clocking/async_assign_3.vhdl
-- The signal temp1 is a vector. The  asynchronous and
-- synchronous assignment is done on parts of the vector,
-- which are overlapping. This is not synthesizable.

library ieee;
use ieee.std_logic_1164.all;

entity async_assign_9 is
  port (
    clock    : in  std_logic;
    rst      : in  std_logic;
    in1, in2 : in  std_logic_vector(3 downto 0);
    out1     : out std_logic_vector(7 downto 0));
end;

architecture arch of async_assign_9 is
 signal temp1 : std_logic_vector(7 downto 0);
begin
  main: process (clock, rst, in1, in2)
  begin 

    temp1(4 downto 1) <= in1;                  
    if rst = '1' then
      temp1(7 downto 4) <= "0000";
    elsif clock'event and clock = '1' then
      temp1(7 downto 4) <= in2;
    end if;

  end process;

  out1 <= temp1;
  
end;


