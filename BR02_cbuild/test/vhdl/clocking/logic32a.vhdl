-- Copyright (c) 2002, 2005 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- Flip-flop with synchronous reset.

-- This is a copy of the beacon11 test misc.logic32a.vhd.  It was copied
-- in here because it is a more complex case of extra statements before
-- the clock block.  The if statement can confuse the clock validation
-- routine.
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;

entity logic32a is
  port( clk : in std_logic;
        reset : in std_logic;
        in1 : in std_logic_vector(3 downto 0);
        out1 : out std_logic_vector(3 downto 0)
        );
end logic32a;

architecture arch_logic32a of logic32a is
begin

  P1:process(clk, reset, in1)
    variable temp1 : std_logic_vector(3 downto 0);
    variable temp2 : std_logic_vector(3 downto 0);
    variable temp3 : std_logic_vector(3 downto 0);
  begin
    temp3 := temp1;
    if (reset = '1') then
      temp2 := in1;
    else
      temp2 := temp3;
    end if;

    if (clk'event and clk = '1') then
      temp1 := temp2;
      out1 <= temp1;
    end if;
  end process;

end arch_logic32a;
