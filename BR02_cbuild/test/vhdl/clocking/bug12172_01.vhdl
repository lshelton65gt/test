entity bug12172_01 is
  
  port (
    reset : in  bit;
    clk   : in  bit;
    in1   : in  bit_vector(3 downto 0);
    out1  : out bit_vector(3 downto 0));

end bug12172_01;

architecture arch of bug12172_01 is
  signal t1 : bit_vector(3 downto 0) := "0000";
begin  -- arch

  p1 : process 
    variable  temp1     : bit_vector(3 downto 0);
    variable  temp2    : bit_vector(3 downto 0);
    
  begin
--    t1 <= in1;
    temp1 := in1;
    temp2 := temp1;
    wait until clk = '1';
--    temp := in1;
    if (reset = '1') then
      out1 <= "0000";
    else
      out1 <= temp2;
    end if;
  end process;
  

end arch;

