-- June 2008
-- This test uses not syntesizable clock construct.
-- By customer request CBUILD now supports such construct.

library ieee;
use ieee.std_logic_1164.all;

entity bug12178_01 is
  
  port (
    rst  : in  std_logic;
    clk  : in  std_logic;
    in1  : in  std_logic;
    out1 : out std_logic);

end bug12178_01;

architecture arch1 of bug12178_01 is

begin
  
  p2 : PROCESS (rst, clk)
  BEGIN
    IF (rst = '0') THEN
        out1 <= '0';
    ELSIF (rst = '1') THEN
      IF (clk = '1' AND clk'EVENT) THEN
        out1 <= in1;
      END IF;
    ELSE
        out1 <= 'X';
    END IF;
  END PROCESS;
 

end;
