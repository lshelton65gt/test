LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY bug5379 IS
   PORT(
      cclk   : IN  std_logic;
      din    : IN  std_logic;
      rstb   : in  std_logic;
      din_d1 : out std_logic
   );

END bug5379;

ARCHITECTURE arch OF bug5379 IS
BEGIN
   process(cclk, rstb)
     variable jall_ones : std_logic_vector(15 downto 0);
     variable resetval : std_logic := '1';
   begin
--jlnk_crc.vhd:19: Error 3006: Always block cannot reset
     jall_ones := "1111111111111111";
     resetval := '0';
--jlnk_crc.vhd:21: Error 3007: Always block is not synthesizable
--     jall_ones := (others => '1');
--     jall_ones := "11111111" & "00000000";
     if (rstb ='0') then
       din_d1 <= resetval;
     elsif (cclk'event and cclk='1') then
       din_d1 <= din;
     end if;
   end process;

END arch;
