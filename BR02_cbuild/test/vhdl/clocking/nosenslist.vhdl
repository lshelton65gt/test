library ieee;
use ieee.std_logic_1164.all;

entity nosenslist is
  port (
    clk, din : in std_logic;
    dout : out std_logic);
end nosenslist;

architecture arch of nosenslist is

begin  -- arch

p1: process
begin  -- process p1
  if clk'event and clk = '1' then
    dout <= din;
  end if;
end process p1;

end arch;
