-- Make a really stupid test mixing the use of variables and signals in a
-- single process.
library ieee;
use ieee.std_logic_1164.all;

entity sigandvar is
  port (
    clk, rst, d : in  std_logic;
    q           : out std_logic_vector(1 downto 0));
end sigandvar;

architecture arch of sigandvar is

begin

  stupid: process (clk, rst)
    variable qv, temp : std_logic;
  begin
    if rst = '0' then
      q(0) <= '0';
    elsif clk'event and clk = '1' then
      temp := not d;
      q(0) <= temp;
      qv := temp;
    end if;

    if rst = '0' then
      qv := '0';
    end if;

    q(1) <= qv;

  end process stupid;

  

end arch;
