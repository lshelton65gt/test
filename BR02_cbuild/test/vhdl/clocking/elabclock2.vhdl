-- This test must be elaborated before the clock can be properly validated
library ieee;
use ieee.std_logic_1164.all;

entity dut is
  generic (
    GENVAL : integer);
  port (
    clk, datain : in  std_logic;
    dataout     : out std_logic);
end dut;

architecture arch of dut is

  constant CONSTVAL : integer := 4;
begin

  the_process: process (clk, datain)
  begin
    if GENVAL = CONSTVAL then
      if clk'event and clk = '1' then
        dataout <= datain;
      end if;
    else
      dataout <= datain;
    end if;
  end process the_process;
end arch;


library ieee;
use ieee.std_logic_1164.all;

entity elabclock2 is
  port (
    clk, datain : in  std_logic;
    dataout     : out std_logic);
end elabclock2;

architecture arch of elabclock2 is

  component dut
    generic ( GENVAL : integer);
    port ( clk, datain : in  std_logic;
           dataout     : out std_logic);
  end component;
  
begin

  the_component : dut
    generic map (GENVAL => 3)
    port map (clk     => clk,
              datain  => datain,
              dataout => dataout);

end arch;
