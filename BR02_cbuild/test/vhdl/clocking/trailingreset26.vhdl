-- If the trailing reset block for loop is unrolled and the clock block for
-- loop is not unrolled, then cbuild generated model fails functionally.
-- The problem is during population when the non-overlapping def'ed signals
-- are removed and placed into a separate always block without reset.
-- The overlapping set is incorrectly determined.
library ieee;
use ieee.std_logic_1164.all;

entity trailingreset26 is
  
  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0));

end trailingreset26;

architecture arch of trailingreset26 is

  signal vec : std_logic_vector(3 downto 0);
  
begin  -- arch

  seqproc: process (clk, rst)
  begin  -- process seqproc
    if clk'event and clk = '1' then  -- rising clock edge
      for i in 3 downto 0 loop
        vec(i) <= in1(i);
      end loop;  -- i
    end if;
    if rst = '0' then                   -- asynchronous reset (active low)
      vec(2) <= '0';
      vec(1) <= '0';
      vec(0) <= '0';
-- Use of for loop generates alert which indicates that non-constant
-- select is used in reset block. That check doesn't catch the problem
-- of non-overlapping def'ed signals in clock and reset block. So unrolling
-- it to avoid the alert.
--       for i in 2 downto 0 loop
--         vec(i) <= '0';
--       end loop;  -- i
    end if;
  end process seqproc;

  out1 <= vec;
  
end arch;
