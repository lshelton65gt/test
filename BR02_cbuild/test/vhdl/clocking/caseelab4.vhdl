-- Test Jaguar-domain static elaboration of a case statement with integer select.
-- Choose a DOWNTO range.
library ieee;
use ieee.std_logic_1164.all;

entity caseelab4 is
  generic (
    sel : integer := 6);
  port (
    inp  : in  std_logic_vector(4 downto 0);
    outp : out std_logic);
end;

architecture arch of caseelab4 is
begin
  p1: process (inp)
  begin
    case sel is
      when 1 => outp <= inp(0);
      when 2 to 3 => outp <= inp(1);
      when 4 downto 5 => outp <= '1';   -- null range: supported, never matched
      when 4 | 7 => outp <= inp(2);
      when 6 downto 5 | 8 to 8 | 12 => outp <= inp(3);
      when others => outp <= inp(4);
    end case;
  end process p1;
end arch;
