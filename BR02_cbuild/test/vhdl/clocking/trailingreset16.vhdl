-- This tests the separation of a memory that is reset and a vector that is not.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity trailingreset16 is
    port (
    d1 : in  std_logic_vector(3 downto 0);
    q1 : out std_logic_vector(3 downto 0);
    m0, m1, m2, m3 : out std_logic_vector(3 downto 0);
    clk, reset1, reset2 : in  std_logic);
end;

architecture arch of trailingreset16 is
  type memtype is array (3 downto 0) of std_logic_vector(3 downto 0);
  signal mem : memtype := (others => "0000" );
begin
  backwards: process (clk, reset1)
  begin
    if clk'event and clk = '1' then
      if reset2 = '0' then
        for i in 3 downto 0 loop
          mem(i) <= "0001";
        end loop;
        q1 <= "0001";
      end if;
      for i in 3 downto 0 loop
        mem(i) <= d1;
      end loop;
      q1 <= not d1;
    end if;

    if reset1 = '0' then 
      for i in 3 downto 0 loop
        mem(i) <= "0000";
      end loop;
    end if;
  end process backwards;

  m0 <= mem(0);
  m1 <= mem(1);
  m2 <= mem(2);
  m3 <= mem(3);
  
end arch;
