-- This tests a process driving a data-driven varsel of a vector with
-- different clock/reset patterns.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity trailingreset18 is
    port (
    d1 : in  std_logic_vector(3 downto 0);
    q1 : out std_logic_vector(3 downto 0);
    clk, reset1 : in  std_logic);
end;

architecture arch of trailingreset18 is
begin
  backwards: process (clk, reset1, d1)
  begin
    if clk'event and clk = '1' then
      q1(0) <= d1(3);
    end if;
    if reset1 = '0' then 
      q1(to_integer(unsigned(d1(1 downto 0)))) <= '0';
    end if;
  end process backwards;

end arch;
