-- This tests a process driving multiple slices of a multidimensional
-- signal with different clock/reset patterns.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity trailingreset13 is
    port (
    d1 : in  std_logic_vector(3 downto 0);
    q1 : out std_logic_vector(3 downto 0);
    m0, m1, m2, m3 : out std_logic_vector(3 downto 0);
    clk, reset1 : in  std_logic);
end;

architecture arch of trailingreset13 is
  type memtype is array (3 downto 0) of std_logic_vector(3 downto 0);
  signal mem : memtype := (others => "0000" );
begin
  backwards: process (clk, reset1)
  begin
    if clk'event and clk = '1' then
      q1(2 downto 1) <= d1(1 downto 0);
      mem(to_integer(unsigned(d1(1 downto 0)))) <= d1;
    end if;
    if reset1 = '0' then 
      q1 <= "0000";
    end if;
  end process backwards;

  m0 <= mem(0);
  m1 <= mem(1);
  m2 <= mem(2);
  m3 <= mem(3);
  
end arch;
