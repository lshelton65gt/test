-- last mod: Wed Oct  23 13:22:04 2007
-- Description:  This test was inspired by bug 7679
-- See comment in test/vhdl/clocking/async_assign_3.vhdl
-- The signal temp1 is an array. The  asynchronous and
-- synchronous assignment is done on parts of the array,
-- which are not overlapping.

library ieee;
use ieee.std_logic_1164.all;

entity async_assign_6 is
  port (
    clock    : in  std_logic;
    rst      : in  std_logic;
    in1, in2 : in  integer;
    out1     : out integer;
    out2     : out integer);
end;

architecture arch of async_assign_6 is
 type arr is array (0 to 1) of integer; 
 signal temp1 : arr;
begin
  main: process (clock, rst, in1, in2)
  begin 

    temp1(0) <= in1;                    -- this line is tested
    if rst = '1' then
      temp1(1) <= 0;
    elsif clock'event and clock = '1' then
      temp1(1) <= in2;
    end if;

  end process;

  out1 <= temp1(0);
  out2 <= temp1(1);
  
end;


