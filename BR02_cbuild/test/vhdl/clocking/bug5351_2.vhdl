-- This is an abstract of Intel's generalized flop.  It tests that
-- nested constant if expressions can be located and the process
-- correctly statically elaborated in the Jaguar domain.  bug5351,
-- bug5351_1, bug5351_2 and bug5351_3 are the same code with different
-- generics.
library ieee;
use ieee.std_logic_1164.all;

entity bug5351_2 is
  generic (
    edge   : integer := 1;
    invert : integer := 1;
    set    : integer := 0);
  port (
    d   : in std_logic;
    q   : out std_logic;
    rst : in std_logic;
    ck  : in std_logic);
end;

architecture behav of bug5351_2 is
  signal q1 : std_logic;
begin  -- behav

  p: process (ck, rst)
  begin  -- process p
    if set /= 1 then   
      if rst = '0' then                   -- asynchronous reset (active low)
        q1  <= '0';
      else
        if edge = 0 then
          if ck'event and ck = '0' then 
            q1 <= d;
          end if;  
        else
          if ck'event and ck = '1' then    -- rising clock edge
            q1 <= d;
          end if;
        end if;
      end if;
    else
      if rst = '0' then                   -- asynchronous reset (active low)
         q1  <= '1';
      else
        if edge = 0 then
          if ck'event and ck = '0' then 
            q1 <= d;
          end if;  
        else
          if ck'event and ck = '1' then    -- rising clock edge
            q1 <= d;
          end if;
        end if;
      end if;
    end if;
  end process p;
  
  q <= q1 when invert = 0
       else not q1;

end behav;
