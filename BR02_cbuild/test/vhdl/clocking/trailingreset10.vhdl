-- bug5075.vhdl tests a single trailing reset and bug5000.vhdl tests multiple
-- trailing resets.
-- This tests a process driving multiple fixed slices of one signal
library ieee;
use ieee.std_logic_1164.all;

entity trailingreset10 is
    port (
    d1 : in  std_logic_vector(2 downto 0);
    q1 : out std_logic_vector(3 downto 0);
    clk, reset1 : in  std_logic;
    reset2, reset3 : in std_logic);
end;

architecture arch of trailingreset10 is
begin
  backwards: process (clk, reset1, reset2, reset3)
  begin
    if clk'event and clk = '1' then
      q1(2 downto 0) <= d1;
    end if;
    if reset1 = '0' then 
      q1(0) <= '0';
    end if;
    if '0' = reset2 then 
      q1(2 downto 1) <= "11";
    end if;
    if reset3 = '0' then 
      q1 <= "0000";
    end if;
  end process backwards;
end arch;
