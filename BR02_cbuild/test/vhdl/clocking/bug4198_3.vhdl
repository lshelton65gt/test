-- This tests for the ability to detect a clock when other conditions
-- are ANDed with the clock in the same if condition.  This test does not have
-- the clock condition specified in a way that Jaguar will create a binary op
-- of the two halves, so we don't detect the clock and generate a combo block
-- for the process.  We should detect the clock in the middle, but we don't,
-- because Jaguar left-associates the terms of the boolean expression, so we
-- never see clk'event and clk='1' as a binary expr.
entity bug4198_3 is
  port (
    clk : in bit;
    en1, en2 : in bit;
    din : in bit;
    dout : out bit);
end;

architecture rtl of bug4198_3 is
begin
  process (clk, en1, en2)
  begin
    if en1='1' and clk'event and clk='1' and en2='1' then
      dout <= din;
    end if;
  end process;
end rtl;
