-- last mod: Thu Oct 19 13:40:38 2006
-- filename: test/bugs/bug6131/bug6131_a.vhdl
-- Description:  This test duplicates the problem seen in 6131, of leaked
-- memory when a complex if elif elif stmt is processed, and the middle one is
-- always true.  It is a changed version of the part of design in
-- test/bugs/bug6131 so that it demonstrates a simulation difference as well as
-- the memory leak.


library ieee;
use ieee.std_logic_1164.all;

entity bug6131_a is 
 generic (
   c_a_width         : integer := 16;
   c_mult_type       : integer := 0;
      c_has_q           : integer := 1;
      c_reg_a_b_inputs  : integer := 1;

   c_baat : integer := 16);
  port (
    clk_i    : in  std_logic;
    in1, in2 : in  std_logic_vector(7 downto 0);
    out1     : out std_logic_vector(7 downto 0));
end;

architecture arch of bug6131_a is 
     type     multiplier_stages is array (0 to 3) of std_logic_vector(7 downto 0);
    signal sub_product   : multiplier_stages           := (others => "01010101");
   signal c_latency        : integer := 0;
   signal product_i     : std_logic_vector(7 downto 0) := (others => '0');
   signal product_j     : std_logic_vector(7 downto 0) := (others => '1');

begin
   pipeline_output_async : process (clk_i,  product_i, product_j, c_latency, sub_product)
   begin  -- process register_output
    if (c_baat = c_a_width) then                                         -- always true
      if c_mult_type > 2 and c_latency > 0 and c_reg_a_b_inputs = 0 then -- always false
        sub_product(0) <= product_i;
      elsif c_reg_a_b_inputs = 1 then                                    -- always true
        if (c_has_q = 1) and c_latency = 0 then                          -- always true
          sub_product(0) <= not product_i;
        end if;
      else                                                               -- this is never executed
        if c_latency = 0 and c_has_q = 0 then
          sub_product(0) <= product_j;
        end if;
      end if;
    end if;
    out1  <= sub_product(0);

   end process pipeline_output_async;
end;
