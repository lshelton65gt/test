-- Test a trailing reset with a range and a bitsel to cover the entire range of
-- the vector.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity trailingreset21 is
    port (
    d1 : in  std_logic_vector(3 downto 0);
    q1 : out std_logic_vector(3 downto 0);
    clk, reset1 : in  std_logic);
end;

architecture arch of trailingreset21 is
begin
  backwards: process (clk, reset1)
  begin
    if clk'event and clk = '1' then
      q1(to_integer(unsigned(d1(1 downto 0)))) <= d1(3);
    end if;
    if reset1 = '0' then 
      q1(3 downto 1) <= "000";
      q1(0) <= '1';
    end if;
  end process backwards;

end arch;
