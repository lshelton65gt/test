-- Testcase for bug7775 to test that compiler exists with "not supported"
-- error message for the non-synthesizable block instead of "Carbon Internal Error".
library ieee;
use ieee.std_logic_1164.all;

entity bug7775 is
  port (
    CLKA, CLKB, AA, AB    : in  std_logic;
    DOA, DOB       : out std_logic);
end;


architecture RTL of bug7775 is

begin

  process (CLKA, CLKB)
  begin

    if (CLKA'event and CLKA = '1') then
      DOA <= AA;
    end if;

    if (CLKB'event and CLKB = '1') then
      DOB <= AB;   
    end if;
  end process;

end RTL;
