-- last mod: Wed Oct  30, 2007
-- Description:  This test was inspired by bug 7679
-- This test used to cause an Alert 3549. Now, it doesn't.

library ieee;
use ieee.std_logic_1164.all;

entity async_assign_1 is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector(7 downto 0);
    asyncout1: out std_logic_vector(7 downto 0);
    out1     : out std_logic_vector(7 downto 0));
end;

architecture arch of async_assign_1 is 
 signal temp1 : std_logic_vector(7 downto 0) := "00000000";
begin
  main: process (clock, in1, temp1)
  begin 
   asyncout1 <= temp1 and in1;   -- this line used to cause alert 3549.
                                 -- with last changes, it doesn't anymore.
   
    if clock'event and clock = '1' then 
     temp1 <= in1 and in2;
      out1 <= temp1;
    end if;
  end process;
end;


