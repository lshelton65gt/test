-- This test checks the ability to correctly populate unconditional statements
-- that follow the clock block.
entity bug5597 is
  port(clk, pause_any : in bit;
       pc : out bit_vector(3 downto 0));
end;
architecture logic of bug5597 is
  signal pc_reg : bit_vector(3 downto 2);
begin

  pc_next: process(clk, pause_any, pc_reg)
    variable pc_next : bit_vector(3 downto 2);
  begin
    if pause_any = '1' then
      pc_next := pc_reg;
    end if;

    if clk'EVENT and clk = '1' then
      pc_reg <= pc_next;
    end if;

    pc <= pc_reg & "00";
  end process;
end;
