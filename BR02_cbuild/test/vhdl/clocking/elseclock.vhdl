library ieee;
use ieee.std_logic_1164.all;

entity elseclock is
  port (
    clk, rst1, rst2, din : in  std_logic;
    dout                 : out std_logic);
end elseclock;

architecture arch of elseclock is

begin
  p1: process (clk, rst1, rst2)
  begin
    if rst1 = '0' then
      dout <= '0';
    elsif rst2 = '0' then
      dout <= '1';
    else
      if clk'event and clk = '1' then
        dout <= din;
      end if;
    end if;
  end process p1;
end arch;
