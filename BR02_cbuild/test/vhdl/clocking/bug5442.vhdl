-- Test that we can correctly extract the clock edge from an elsif clause in a
-- process requiring the elaboration of generics.
entity bug5442 is
  generic (
    en : integer := 1);
  port (
    clk : in  bit;
    rst : in  bit;
    i   : in  bit;
    o   : out bit);
end;

architecture rtl of bug5442 is
begin
  process (clk, rst)
  begin
    if en = 1 then
      if rst = '0' then
        o <= '0';
      elsif clk'event and clk = '1' then
        o <= i;
      end if;
    end if;
  end process;
end rtl;
