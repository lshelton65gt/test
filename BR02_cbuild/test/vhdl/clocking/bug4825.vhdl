-- This is abstracted from the broadcom code in bug4825.  Here a memory is
-- created during record population; we need to be able to support such
-- memories that are clocked and reset identically.
library ieee;
use ieee.std_logic_1164.all;

package localDefs is
  type bundle_pcmciaConfig is
    record
      pcmciaAddrBase0		: std_logic_vector(3 downto 0);
      pcmciaAddrBase1		: std_logic_vector(7 downto 0);
      pcmciaAddrBase2		: std_logic_vector(7 downto 0);
      spromInfo                 : std_logic_vector(2 downto 0);
    end record;
  type configType is array(3 downto 0) of bundle_pcmciaConfig;
end localDefs;

library ieee;
use ieee.std_logic_1164.all;
use work.localDefs.all;

entity bug4825 is
  port (
    m0            : out std_logic_vector(3 downto 0);
    m10           : out std_logic_vector(7 downto 0);
    m20           : out std_logic_vector(7 downto 0);
    m1            : out std_logic_vector(3 downto 0);
    m2            : out std_logic_vector(3 downto 0);
    m3            : out std_logic_vector(3 downto 0);
    Size          : in std_logic_vector(1 downto 0);
    dinSync       : in std_logic_vector(7 downto 0);
    powerOnReset  : in std_logic;
    pcmciaOcpClk  : in std_logic
    );
end bug4825;

architecture synth of bug4825 is
  signal configs		: configType;
begin

  regsPcmcia : process (pcmciaOcpClk, powerOnReset)
  begin
    if pcmciaOcpClk'event and pcmciaOcpClk = '1' then
      configs(2).spromInfo(1 downto 0) <= Size;
      configs(0).pcmciaAddrBase0 <= dinSync(3 downto 0);
      configs(0).pcmciaAddrBase1 <= dinSync(7 downto 0);
      configs(0).pcmciaAddrBase2 <= dinSync(7 downto 0);
    end if;
    
    if powerOnReset = '1' then
      configs(0).pcmciaAddrBase0 <= "0001";
      configs(0).pcmciaAddrBase1 <= (others => '0');
      configs(0).pcmciaAddrBase2 <= "00011000";
    end if;
  end process regsPcmcia; 

  m0 <= configs(0).pcmciaAddrBase0;
  m10 <= configs(0).pcmciaAddrBase1;
  m20 <= configs(0).pcmciaAddrBase2;
  m1 <= configs(1).pcmciaAddrBase0;
  m2 <= configs(2).pcmciaAddrBase0;
  m3 <= configs(3).pcmciaAddrBase0;
  
end synth;
