-- This test requires both static elaboration and process normalization to
-- occur in order to pass.  First, REGTYPE=true is evaluated as true, and the
-- then statements for that if statement are elaborated.  Then, normalization
-- occurs, changing the reset condition to "not rst = '0'" and rearranging the
-- outer if statement.  Finally, the clock is detected and what is left of the
-- process's statement list is populated.
entity bug5282 is
  generic ( REGTYPE : boolean := true);
  port( clk, rst : in bit;
        din : in bit_vector(3 downto 0);
        dout : out bit_vector(3 downto 0));
end;

architecture arch of bug5282 is
begin
  p1: process (clk, din, rst)
  begin
    if rst = '0' then
      if REGTYPE = true then
        if clk'event and clk = '1' then
          dout <= din;
        end if;
      else
        dout <= din;
      end if;
    else
      dout <= (others => '0');
    end if;
  end process;
end arch;
