-- Checks that constant are propagated to statically compute that select
-- in reset block is not dynamic.
library ieee;
use ieee.std_logic_1164.all;

entity bug6657_2 is
  
  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0);
    out2 : out std_logic_vector(3 downto 0));

end bug6657_2;

architecture arch of bug6657_2 is

  signal vec : std_logic_vector(3 downto 0);
  signal vec1 : std_logic_vector(3 downto 0);
  constant c1 : integer := 1;
  constant c2 : integer := 1;
  constant c3 : integer := c1 + c2;
  
begin  -- arch

  seqproc: process (clk, rst)
  begin  -- process seqproc
    if clk'event and clk = '1' then  -- rising clock edge
      for i in 3 downto 0 loop
        vec(i) <= in1(i);
      end loop;  -- i
      vec1 <= in1;
    end if;
    if rst = '0' then                   -- asynchronous reset (active low)
      for i in 4 downto 1 loop
        vec(i-c3+1) <= '0';
      end loop;  -- i
    end if;
  end process seqproc;

  out1 <= vec;
  out2 <= vec1;
  
end arch;
