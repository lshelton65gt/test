library ieee;
use ieee.std_logic_1164.all;

entity bug5075_1 is
  port (
    pclk : in std_logic;
    combinedReset : in std_logic;
    bothresets : in std_logic;
    clearPme : out std_logic;
    regConfigResponse: out std_logic);

end bug5075_1;

architecture synth of bug5075_1 is
begin 
  writeConfig: process (pclk, combinedReset, bothResets)
  begin
    if (combinedReset = '1') then
      clearPme <= '1';
    elsif (pclk'event and pclk = '1') then
      clearPme <= '0';
      regConfigResponse <= '0';
    end if;
    if bothResets = '1' then
      regConfigResponse <= '1';
    end if;
  end process writeConfig;
end synth;
