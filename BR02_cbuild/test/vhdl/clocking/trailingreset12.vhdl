-- This tests a process driving multiple slices of one signal with different clock/
-- reset patterns.
library ieee;
use ieee.std_logic_1164.all;

entity trailingreset12 is
    port (
    d1 : in  std_logic_vector(1 downto 0);
    q1 : out std_logic_vector(3 downto 0);
    clk, reset1 : in  std_logic);
end;

architecture arch of trailingreset12 is
begin
  backwards: process (clk, reset1)
  begin
    if clk'event and clk = '1' then
      q1(2 downto 1) <= d1;
    end if;
    if reset1 = '0' then 
      q1 <= "0000";
    end if;
  end process backwards;
end arch;
