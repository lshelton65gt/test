-- This tests for the ability to detect a clock when other conditions
-- are ANDed with the clock in the same if condition.
entity bug4198 is
  port (
    clk : in bit;
    en : in bit;
    din : in bit;
    dout : out bit);
end;

architecture rtl of bug4198 is
begin
  process (clk)
  begin
    if clk'event and clk='1' and en='1' then
      dout <= din;
    end if;
  end process;
end rtl;
