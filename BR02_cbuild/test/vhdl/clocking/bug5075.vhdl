-- This tests a single trailing reset clause
library ieee;
use ieee.std_logic_1164.all;

entity bug5075 is
    port (
    d          : in  std_logic;
    q          : out std_logic;
    clk, reset : in  std_logic);
end bug5075;

architecture arch of bug5075 is
begin
  backwards: process (clk, reset)
  begin
    if clk'event and clk = '1' then
      q <= d;
    end if;
    if reset = '0' then 
      q <= '0';
    end if;
  end process backwards;
end arch;
