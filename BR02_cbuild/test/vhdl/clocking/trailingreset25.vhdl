--make sure we handle both edges of a reset properly
library ieee;
use ieee.std_logic_1164.all;
entity trailingreset25 is
  port (
    clk  : in std_logic;
    rst1 : in std_logic;
    in1  : in std_logic;
    out1  : out std_logic);
end trailingreset25;

architecture trailingreset25 of trailingreset25 is
begin
  process (clk, rst1)
  begin
    if (rst1 = '1') then
      out1 <= '0';
    elsif (rst1 = '0') then
      out1 <= '1';
    elsif (clk'event and clk = '1') then
      out1 <= in1;
    end if;
  end process;

end trailingreset25;

