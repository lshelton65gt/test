-- last mod: Tue Oct  9 18:01:16 2007
-- Description:  This test was inspired by bug 7679
-- this testcase is NOT synthesizable because temp1 is
-- assigned synchronously and asynchronously.

library ieee;
use ieee.std_logic_1164.all;

entity async_assign_2 is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector(7 downto 0);
    asyncout1: out std_logic_vector(7 downto 0);
    out1     : out std_logic_vector(7 downto 0));
end;

architecture arch of async_assign_2 is 
 signal temp1 : std_logic_vector(7 downto 0) := "00000000";
begin
  main: process (clock, in1, in2, temp1)
  begin 
   temp1 <= in2 or in1;          -- this line makes it unsynthesizable
   asyncout1 <= temp1;           
   
   if clock'event and clock = '1' then 
     temp1 <= in1 and in2;
      out1 <= temp1;
   end if;
  end process;
end;


