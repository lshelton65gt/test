-- bug5075.vhdl tests a single trailing reset and bug5000.vhdl tests multiple
-- trailing resets.
-- This tests keeping variable assignments in the clock and reset blocks out of
-- the internal clocked signal list.
library ieee;
use ieee.std_logic_1164.all;

entity trailingreset11 is
    port (
    d, d2       : in  std_logic;
    q, q2       : out std_logic;
    clk, reset, r2 : in  std_logic);
end;

architecture arch of trailingreset11 is
begin
  backwards: process (clk, reset, r2)
    variable v : std_logic;
  begin
    if clk'event and clk = '1' then
      v := d;
      q <= v;
      q2 <= not v and d2;
    end if;
    if reset = '0' then
      v := '0';
      q <= v;
      q2 <= '1';
    end if;
    if r2 = '0' then
      q <= not v;
    end if;
  end process backwards;
end arch;
