-- Reproduces bug 6657 (Raw Broadcom WLAN). An alert is generated since
-- the select in reset block of process is non-constant. This alert can
-- be bypassed and this test works.
library ieee;
use ieee.std_logic_1164.all;

entity bug6657 is
  
  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0);
    out2 : out std_logic_vector(3 downto 0));

end bug6657;

architecture arch of bug6657 is

  signal vec : std_logic_vector(3 downto 0);
  signal vec1 : std_logic_vector(3 downto 0);
  
begin  -- arch

  seqproc: process (clk, rst)
  begin  -- process seqproc
    if clk'event and clk = '1' then  -- rising clock edge
      for i in 3 downto 0 loop
        vec(i) <= in1(i);
      end loop;  -- i
      vec1 <= in1;
    end if;
    if rst = '0' then                   -- asynchronous reset (active low)
      for i in 3 downto 0 loop
        vec(i) <= '0';
      end loop;  -- i
    end if;
  end process seqproc;

  out1 <= vec;
  out2 <= vec1;
  
end arch;
