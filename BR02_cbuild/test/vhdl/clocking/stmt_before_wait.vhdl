-- This test is designed to run with the Alert due to the wait clock not being
-- the first statement in process p1 demoted to a warning.  The goal here is
-- that the process should be populated with the statement preceding the
-- process treated as a clocked statement.
library ieee;
use ieee.std_logic_1164.all;

entity stmt_before_wait is
  port (
    clk, din : in  std_logic;
    dout     : out std_logic);
end stmt_before_wait;

architecture arch of stmt_before_wait is
begin

  p1: process
    variable v : std_logic;
    variable dummy : integer := 0;
  begin
    assert dummy = 0 report "this statement is not populated" severity failure;
    report "neither is this one" severity error;
    null;                               -- nor this one
    wait until clk'EVENT and clk = '1';
    v := din;
    dout <= v;
  end process p1;

end arch;
