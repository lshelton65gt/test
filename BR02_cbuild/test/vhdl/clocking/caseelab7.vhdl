-- Test Jaguar-domain static elaboration of a case statement with integer select.
-- The choice comes down through hierarchy and is different from the default
-- value. This test is abstracted from Matrox Phoenix dmatop dmaxchan.vhdl,
-- around line 225.
package pack is
  constant genval : integer := 2;
end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity dut is
  generic (
    sel : integer := 0);
  port (
    clk, rst : in std_logic;
    inp  : in  std_logic_vector(5 downto 0);
    outp : out std_logic_vector(2 downto 0));
end;

architecture arch of dut is
begin
  p1: process (clk, rst, inp)
  begin
    if rst = '0' then
      case sel is
        when 1 => outp <= "000";
        when genval to 3 => outp <= "001";
        when 4 downto 5 => outp <= "010";
        when 4 | 7 => outp <= "011";
        when 6 downto 5 | 8 to 8 | 12 => outp <= "100";
        when others => outp <= "101";
      end case;
    elsif clk'event and clk = '1' then
      outp <= inp(3 downto 1);
    end if;
  end process p1;
end arch;


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity caseelab7 is
  port (
    clk, rst : in std_logic;
    inp  : in  std_logic_vector(5 downto 0);
    outp : out std_logic_vector(2 downto 0));
end;


architecture arch of caseelab7 is
  component dut
    is
      generic (
        sel : integer := 0);
    port (
      clk, rst : in std_logic;
      inp  : in  std_logic_vector(5 downto 0);
      outp : out std_logic_vector(2 downto 0));
  end component;

begin

  u1 : dut generic map (sel => genval)
    port map (inp  => inp,
              outp => outp,
              clk => clk,
              rst => rst);

end arch;
