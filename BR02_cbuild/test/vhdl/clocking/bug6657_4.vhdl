-- Tests to ensure that non-reset statements with dynamic selects
-- don't throw an alert.
library ieee;
use ieee.std_logic_1164.all;

entity bug6657_4 is
  
  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    in1  : in  std_logic_vector(3 downto 0);
    idx  : in  integer range 0 to 3;
    out1 : out std_logic_vector(3 downto 0);
    out2 : out std_logic_vector(3 downto 0));

end bug6657_4;

architecture arch of bug6657_4 is

  signal vec : std_logic_vector(3 downto 0);
  signal vec1 : std_logic_vector(3 downto 0);
  
begin  -- arch

  seqproc: process (clk, rst, in1)
  begin  -- process seqproc
    if clk'event and clk = '1' then  -- rising clock edge
      for i in 3 downto 0 loop
        vec(i) <= in1(i);
      end loop;  -- i
    end if;
    if rst = '0' then                   -- asynchronous reset (active low)
      for i in 3 downto 0 loop
        vec1(i) <= in1(i);
      end loop;
    end if;
  end process seqproc;

  out1 <= vec;
  out2 <= vec1;
  
end arch;
