library IEEE; 
use IEEE.std_logic_1164.all; 

entity gatedwaitclock is 
  port( clock, stall, data : in std_logic; 
        out1 : out std_logic); 
end gatedwaitclock; 

architecture arch of gatedwaitclock is 
begin 
  process 
  begin 
    wait until CLOCK'event and CLOCK = '1' and stall = '0'; 
    out1 <= data; 
  end process; 
end arch;


