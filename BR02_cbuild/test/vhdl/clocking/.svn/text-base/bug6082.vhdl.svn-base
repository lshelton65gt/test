-- This tests that Jaguar-domain static elaboration can successfully choose an
-- elsif clause as the elaborated branch when there is no else clause.  This is
-- the original testcase.
library ieee;
use ieee.std_logic_1164.all;

entity bug6082 is
  generic (
    disable_block : integer := 0;
    do_work       : integer := 1);
  port (
    clk : in  std_logic;
    i   : in  std_logic;
    o   : out std_logic);
end bug6082;

architecture functional of bug6082 is
begin

  clock_process: process (clk)
  begin
    if disable_block = 1 then
      o <= '1';
    elsif do_work = 1 then
      if (clk'event and clk = '1') then
        o <= i;
      end if;
    end if;
  end process clock_process;

end functional;
