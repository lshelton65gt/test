-- This test requires both static elaboration and process normalization to
-- occur in order to pass.  First, REGTYPE=true is evaluated as false, and the
-- then statements for that if statement are elaborated.  Process normalization
-- does not occur here because the static elaboration removes the clock.  This
-- turns the process into a combinational block.
entity bug5282_1 is
  generic ( REGTYPE : boolean := false);
  port( clk, rst : in bit;
        din : in bit_vector(3 downto 0);
        dout : out bit_vector(3 downto 0));
end;

architecture arch of bug5282_1 is
begin
  p1: process (clk, din, rst)
  begin
    if rst = '0' then
      if REGTYPE = true then
        if clk'event and clk = '1' then
          dout <= din;
        end if;
      else
        dout <= din;
      end if;
    else
      dout <= (others => '0');
    end if;
  end process;
end arch;
