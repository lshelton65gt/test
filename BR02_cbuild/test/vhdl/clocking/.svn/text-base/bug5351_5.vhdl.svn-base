-- This is another example of static elaboration in the Jaguar domain, this one
-- abstracted from Matrox.  This one pointed out issues with cbuild's use of Jaguar
-- lists that were causing memory to be corrupted.
library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity bug5351_5 is
  generic (
    indx : integer := 7);
  port (
    edgfnc   : in  std_logic_vector(38 downto 0);
    edgfinc  : in  std_logic_vector(26 downto 0);
    eincxtra : in  std_logic_vector(27 downto 0);
    gtzero   : out std_logic_vector( 1 downto 0)
    );
end;

architecture arch of bug5351_5 is
  function zeropad (x : std_logic_vector; z : natural)
    return std_logic_vector is
    variable y : std_logic_vector(x'left-x'right+z downto 0);
    variable j : natural;
  begin  -- zeropad
    j := x'left+1;
    for i in y'left downto z loop
      j := j-1;
      y(i) := x(j);
    end loop;
    for i in z-1 downto 0 loop
      y(i) := '0';
    end loop;
    return y;
  end zeropad;
  
begin
  Xcmp : process (edgfinc, edgfnc, eincxtra)
  begin  -- process Xcmp
    gtzero <= (others => '0');
    if signed(edgfnc) > signed(zeropad(edgfinc, 1)) then
      gtzero(1) <= '1';
    end if;
    if indx /= 2 then
      if signed(edgfnc) > signed(zeropad(eincxtra, 1)) then
        gtzero(0) <= '1';
      end if;
    end if;
  end process Xcmp;
end arch;
