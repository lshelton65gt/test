library ieee;
use ieee.std_logic_1164.all;

entity unconnected_reset is
  port (
    e1, e2, clk, din : in std_logic;
    dout : out std_logic_vector(2 downto 0));
end unconnected_reset;

architecture arch of unconnected_reset is

begin  -- arch

p1: process (clk)
begin  -- process p1
  if e1 = '1' then
    dout <= "000";
  elsif e2 = '1' then
    dout <= "001";
  else
    dout <= "010";
  end if;
  if clk'event and clk = '1' then
    dout <= din & "11";
  end if;
end process p1;

end arch;
