-- This tests for the ability to detect a clock when other conditions
-- are ANDed with the clock in the same if condition.
entity bug4198_2 is
  port (
    clk : in bit;
    en1, en2 : in bit;
    din : in bit;
    dout : out bit);
end;

architecture rtl of bug4198_2 is
begin
  process (clk)
  begin
    if en1='1' and (clk'event and clk='1') and en2='1' then
      dout <= din;
    end if;
  end process;
end rtl;
