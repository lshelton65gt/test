-- This tests a process driving a data-driven varsel of a vector with
-- different clock/reset patterns.  The reset for loop could be supported, but
-- right now we don't recognize that all bits are actually driven.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity trailingreset20 is
    port (
    d1 : in  std_logic_vector(3 downto 0);
    q1 : out std_logic_vector(3 downto 0);
    clk, reset1 : in  std_logic);
end;

architecture arch of trailingreset20 is
begin
  backwards: process (clk, reset1, d1)
  begin
    if clk'event and clk = '1' then
      q1(0) <= d1(3);
    end if;
    if reset1 = '0' then
      for i in q1'range loop
        q1(i) <= '0';
      end loop;
    end if;
  end process backwards;

end arch;
