library ieee;
use ieee.std_logic_1164.all;

entity wait_not_first is
  port (
    clk, din : in  std_logic;
    dout     : out std_logic);
end wait_not_first;

architecture arch of wait_not_first is
begin

  p1: process
    variable v : std_logic;
  begin
    v := din;
    wait until clk'EVENT and clk = '1';
    dout <= v;
  end process p1;

end arch;
