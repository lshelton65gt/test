library ieee;
use ieee.std_logic_1164.all;

entity bothedges is
  port (
    rst, clk, d : in  std_logic;
    q           : out std_logic);
end bothedges;

architecture arch of bothedges is

begin

  p1: process (clk, rst)
  begin
    if rst = '0' then
      q <= '0';
    elsif rst = '1' then
      q <= '1';
    elsif clk'event and clk = '1' then
      q <= d;
    end if;
  end process p1;

end arch;
