library ieee;
use ieee.std_logic_1164.all;

entity bit_sel_clock is
  port (d : in std_logic_vector(2 downto 0); q: out std_logic_vector(1 downto 0));
end;

architecture arch of bit_sel_clock is
begin
  process (d(0))
  begin
    if (d(0) = '1' and d(0)'event) then
      q <= d(2 downto 1);
    end if;
  end process;
end;
