-- November 2007
-- This file tests an incomplete clock condition. The clock condition misses
-- the second part (clock = '1'). This is not syntesizable and it's in
-- PEXIT.

library ieee;
use ieee.std_logic_1164.all;

entity bug7759_01 is
  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector(7 downto 0);
    out1     : out std_logic_vector(7 downto 0));
end;

architecture arch of bug7759_01 is 
begin
  main: process (clock, in1)
  begin 
    if clock'event then                 -- this line is tested
      out1 <= in1;
    end if;
  end process;
end;


