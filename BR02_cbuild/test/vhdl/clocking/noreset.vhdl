library ieee;
use ieee.std_logic_1164.all;

entity noreset is
  port (
    clk, din : in  std_logic;
    dout     : out std_logic);
end noreset;

architecture arch of noreset is

begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      dout <= not din;
    end if;
  end process;
end arch;
