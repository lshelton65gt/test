library ieee;
use ieee.std_logic_1164.all;

entity asyncreset is
  port (
    clk, rst, din : in  std_logic;
    dout          : out std_logic);
end asyncreset;

architecture arch of asyncreset is

begin
  p1: process (clk, rst)
  begin
    if rst = '0' then
      dout <= '0';
    elsif clk'event and clk = '1' then
      dout <= din;
    end if;
  end process p1;
end arch;
