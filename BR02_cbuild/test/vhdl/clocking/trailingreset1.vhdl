-- bug5075.vhdl tests a single trailing reset and bug5000.vhdl tests multiple
-- trailing resets.
-- This tests prologue statements preceding the clock block.  This is a
-- negative testcase, as the alert is not demoted.
library ieee;
use ieee.std_logic_1164.all;

entity trailingreset1 is
    port (
    d          : in  std_logic;
    q          : out std_logic;
    clk, reset : in  std_logic);
end;

architecture arch of trailingreset1 is
begin
  backwards: process (clk, reset)
    variable v : std_logic;
  begin
    v := d;
    if clk'event and clk = '1' then
      q <= v;
    end if;
    if reset = '0' then 
      q <= '0';
    end if;
  end process backwards;
end arch;
