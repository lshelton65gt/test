entity for22 is
  port(in1, in2 : in bit_vector( 0 to 3);
       out1     : out bit_vector( 0 to 3)) ;
end for22;

architecture for22 of for22 is

begin

      process(in1,in2)
        begin
            for i in in1'range loop
              for i in in1'range loop
                out1( i ) <= in1(i) and in2(i);
              end loop;
            end loop;
      end process;

end for22;
