-- Testing use of : slice of an alias object
library IEEE;
use IEEE.std_logic_1164.all;

entity partsel_alias2 is
  port ( IN1, IN2 : std_logic_vector ( 0 to 3 ); 
           OUTPUT : out std_logic_vector(0 to 3));
end entity partsel_alias2;

architecture partsel_alias2 of partsel_alias2 is
  begin
      process ( IN1, IN2 )
      alias AL_OUTPUT : std_logic_vector ( 7 to 10) is OUTPUT;
      alias ALIN1 : std_logic_vector(1 to 4) is IN1(0 to 3); 
      alias ALIN2 : std_logic_vector(0 to 3) is IN2(0 to 3); 
      begin 
        for i in 0 to 3 loop
          if (i = 0)  then
            AL_OUTPUT(7 to 8 ) <= ALIN1(i+1 to i + 2) and
                                              ALIN2(i to i + 1);
          elsif ( i = 2 ) then
            AL_OUTPUT(9 to 10) <= ALIN1(i+1 to i + 2) and
                                              ALIN2(i to i + 1);
          end if;
        end loop;
      end process;
end architecture partsel_alias2;
