library ieee;
use ieee.std_logic_1164.all;

entity for_gen4 is
  port (
      D    : IN     std_ulogic_vector(1 DOWNTO 0);
      clk  : IN     std_ulogic;
      rst  : IN     std_ulogic;
      Q    : OUT    std_ulogic_vector(1 DOWNTO 0)
     );
end;

architecture arch of for_gen4 is
begin
  gen1 : for i in 0 to 1 generate
  p1: process(clk,rst)
  begin
  if (rst = '1') then
    Q(i) <= '0';
  elsif CLK = '1' and CLK'event then
    Q(i) <= D(i);
  end if;
  end process p1;
  end generate;
end;

