-- filename: test/vhdl/lang_flowctrl/bug8249_01.vhd
-- Description:  This test shows how we handle the use of a primary input with
-- a name "guard".  Note that guard is not a reserved name, but it is an
-- implicit signal created for a guarded block (see section 9.1 of lrm)
-- currently (04/21/08) this test incorrectly fails with a codegen error.

library ieee;
use ieee.std_logic_1164.all;

entity bug8249_01 is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    guard    : in  std_logic_vector((WIDTH-1) downto 0);
    in2      : in  std_logic_vector((WIDTH-1) downto 0);
    out1     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of bug8249_01 is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1 <= guard and in2;
    end if;
  end process;
end;
