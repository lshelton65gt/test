-- The lvalue of an assignment is a variable width bus  
library ieee;
use ieee.std_logic_1164.all;

entity variable_bus_lvalue is
  port(in1,in2 : std_logic_vector(0 to 3);
       out1: out std_logic_vector(0 to 3));
end;

architecture arch of variable_bus_lvalue is
begin
  p1 : process(in1,in2)
  begin
    for i in 2 downto 0 loop
      for j in i to i+1 loop
            out1(i to j)   <= in1(i to j) and in2(i to j);
      end loop;
    end loop;
  end process;
end;

