library ieee;
use ieee.std_logic_1164.all;

entity flop is
  port (
      D   : IN  std_ulogic;
      CLK : IN  std_ulogic;
      CLR : IN  std_ulogic;
      Q   : OUT std_ulogic
     );
end;

architecture flop_arc of flop is
begin
  process(CLK, CLR)
  begin
    if (CLR = '1') then
      Q <= '0';
    elsif (CLK = '1' and CLK'EVENT) then
      Q <= D;
    end if;
  end process;
end;


library ieee;
use ieee.std_logic_1164.all;

entity for_gen3 is
  port (
      D    : IN     std_ulogic_vector(1 DOWNTO 0);
      aclk : IN     std_ulogic;
      rst  : IN     std_ulogic;
      Q    : OUT    std_ulogic_vector(1 DOWNTO 0)
     );
end;

architecture arch of for_gen3 is

  COMPONENT flop
    PORT (
      D   : IN  std_ulogic;
      CLK : IN  std_ulogic;
      CLR : IN  std_ulogic;
      Q   : OUT std_ulogic
    );
  END COMPONENT;
begin
  gena : for i in D'range generate
    flop1 : flop
      PORT MAP (
        D   =>    D(i),
        CLK =>    aclk,
        CLR =>    rst,
        Q   =>    Q(i)
      );
  end generate;
end;

