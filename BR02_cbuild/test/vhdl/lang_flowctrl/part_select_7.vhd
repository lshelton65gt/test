library ieee;
use ieee.std_logic_1164.all;

entity part_select_7 is
  port(in1,in2 : std_logic_vector(0 to 3);
       out1: out std_logic_vector(0 to 3));
end;

architecture arch of part_select_7 is
begin
  p1 : process(in1,in2)
  variable v : integer;
  begin
    v := 2;
    out1(v to 3)   <= in1(v to 3);
  end process;
end;
