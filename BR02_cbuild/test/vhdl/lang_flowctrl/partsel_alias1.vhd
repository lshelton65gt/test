-- Testing use of : slice of an alias object, range direction 'TO'
library IEEE;
use IEEE.std_logic_1164.all;

entity partsel_alias1 is
  port ( IN1, IN2 : std_logic_vector ( 3 downto 0 ); 
           OUTPUT : out std_logic_vector(3 downto 0));
end entity partsel_alias1;

architecture partsel_alias1 of partsel_alias1 is
  begin
      process ( IN1, IN2 )
      alias AL_OUTPUT : std_logic_vector ( 10 downto 7) is OUTPUT;
      alias ALIN1 : std_logic_vector(1 to 4) is IN1(3 downto 0); 
      alias ALIN2 : std_logic_vector(0 to 3) is IN2(3 downto 0); 
      begin 
        for i in 0 to 3 loop
          if (i = 0)  then
            AL_OUTPUT(10 downto 9 ) <= ALIN1(i+1 to i + 2) and
                                              ALIN2(i to i + 1);
          elsif ( i = 2 ) then
            AL_OUTPUT(8 downto 7) <= ALIN1(i+1 to i + 2) and
                                              ALIN2(i to i + 1);
          end if;
        end loop;
      end process;
end architecture partsel_alias1;
