-- Test for recursive function that uses variables with constant
-- values to terminate recursion. Compiler fails to determine
-- terminating condition since it assumes variables to not be constant.
library ieee;
use ieee.std_logic_1164.all;

package tmp is
    FUNCTION  OR_RED  (ARG: STD_LOGIC_VECTOR) return STD_LOGIC ;
end tmp;
   
package body tmp is

    FUNCTION  OR_RED  (ARG: STD_LOGIC_VECTOR) return STD_LOGIC is
      variable UPPER_TREE, LOWER_TREE: std_logic;
      variable RESULT: STD_LOGIC;
      variable MID, LEN: natural;
      variable TEMP_ARG: STD_LOGIC_VECTOR(ARG'LENGTH-1 downto 0);
    BEGIN
 TEMP_ARG := ARG;
 LEN := TEMP_ARG'length;
 if LEN = 1 then
    RESULT := TEMP_ARG(TEMP_ARG'LEFT);
 elsif LEN = 2 then
    RESULT := TEMP_ARG(TEMP_ARG'LEFT) or TEMP_ARG(TEMP_ARG'RIGHT);
 else
    MID := (LEN +1)/2 + TEMP_ARG'RIGHT;
    UPPER_TREE := OR_RED(TEMP_ARG(TEMP_ARG'LEFT downto MID)); 
    LOWER_TREE := OR_RED(TEMP_ARG(MID -1 downto TEMP_ARG'RIGHT));
    RESULT := UPPER_TREE or LOWER_TREE;
 end if;
 return RESULT;
    END;
end tmp;
library ieee;
use ieee.std_logic_1164.all;

library work;
use work.tmp.all;

entity bug3544_4 is
    port(clk : in std_logic;
         tmp : out std_logic;
         reg2txs : in std_logic_vector(7 downto 0)
    );
end bug3544_4;

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.tmp.all;

architecture top of bug3544_4 is
begin
proc: process(clk, reg2txs) begin
      tmp <= OR_RED(reg2txs);
end process proc;
end top;
