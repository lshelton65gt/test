entity nested_if is
  port(in1, in2 : bit; out1 : out bit);
end;

architecture arch of nested_if is
begin
  process(in1,in2)
  begin
    if (in1 = '1') then
      if (in2 = '0') then
        out1 <= '1';
      else
        out1 <= '0';
      end if;
    else
      if (in2 = '1') then
        out1 <= '1';
      else
        out1 <= '0';
      end if;
    end if;
  end process;
end;
