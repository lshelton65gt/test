-- The slices of in1 and in2 taken in the loops below are NULL ranges,
-- and are also out of range indicies when i==3.  This is a negative
-- test, we're looking for error messages.
-- This test passes with warnings because we don't try to determine
-- statically if the null range code will be executed, and issue an
-- error if it will be.
library ieee;
use ieee.std_logic_1164.all;

entity part_select_6 is
  port(in1,in2 : std_logic_vector(0 to 3);
       out1: out std_logic_vector(0 to 3));
end;

architecture arch of part_select_6 is
begin
  p1 : process(in1,in2)
  begin
    for i in 3 downto 0 loop
      for j in 3 downto 0 loop
        if ( i = j) then
          if (i = 2) then
            out1(2 to 3)   <= in1(i+1 to i) and in2(i+1 to i);
          elsif (i = 0) then
            out1(0 to 1)   <= in1(i+1 to i) and in2(i+1 to i);
          end if;
        end if;
      end loop;
    end loop;
  end process;
end;

