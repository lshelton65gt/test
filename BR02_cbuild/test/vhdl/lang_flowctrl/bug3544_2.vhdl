-- compile error; impure function is not marked as such
library ieee;
use ieee.std_logic_1164.all;

entity bug3544_2 is
  port(clk : in std_logic;
       tmp : out std_logic;
       reg2txs : in std_logic_vector(7 downto 0)
       );
end bug3544_2;

architecture top of bug3544_2 is

  signal sig : natural := 17;
  
  FUNCTION  OR_RED  (ARG: STD_LOGIC_VECTOR) return STD_LOGIC is
    variable UPPER_TREE, LOWER_TREE: std_logic;
    variable RESULT: STD_LOGIC;
    variable MID, LEN: natural;
    variable TEMP_ARG: STD_LOGIC_VECTOR(ARG'LENGTH-1 downto 0);
    variable extra : natural;
  BEGIN
    TEMP_ARG := ARG;
    LEN := TEMP_ARG'length;
    extra := sig;
    if LEN = 1 then
      RESULT := TEMP_ARG(TEMP_ARG'LEFT);
    elsif LEN = 2 then
      RESULT := TEMP_ARG(TEMP_ARG'LEFT) or TEMP_ARG(TEMP_ARG'RIGHT);
    else
      MID := (LEN +1)/2 + TEMP_ARG'RIGHT;
      -- MID := (LEN +1)/2 + TEMP_ARG'RIGHT + sig;  --can't determine MID
      -- sig <= 99; --OK
      UPPER_TREE := OR_RED(TEMP_ARG(TEMP_ARG'LEFT downto MID)); 
      LOWER_TREE := OR_RED(TEMP_ARG(MID -1 downto TEMP_ARG'RIGHT));
      RESULT := UPPER_TREE or LOWER_TREE;
      if extra = 16 then
        result := not result;
      end if;
    end if;
    return RESULT;
  END;

begin
  
  proc: process(clk, reg2txs)
  begin
    tmp <= OR_RED(reg2txs);
  end process proc;

  subsig: process (clk)
  begin
    if clk'event and clk = '1' then
      if sig = 0 then
        sig <= 17;
      else
        sig <= sig - 1;
      end if;
    end if;
  end process subsig;
  
end top;
