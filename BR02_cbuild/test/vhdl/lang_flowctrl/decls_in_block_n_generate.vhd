library ieee;
use ieee.std_logic_1164.all;

entity decls_in_block_n_generate is
  port (
      D    : IN     std_ulogic_vector(3 DOWNTO 0);
      Q    : OUT    std_ulogic_vector(3 DOWNTO 0)
     );
end;

architecture arch of decls_in_block_n_generate is
begin
  forgen : for i in 3 downto 0 generate
    signal temp : std_ulogic;
    constant const : std_ulogic := '1';
    type unused_type is (a, b, c, e);
    subtype unused_subtype is unused_type range a to c;
    begin
      temp <= const when i = 0 else
              D(i);
      blk: block
        function foo( in1 : std_ulogic; in2 : integer range 0 to 3) return
std_ulogic is
        begin
          if (in2 < 3) then
            return in1;
          else
            return '1';
          end if;
        end;
        begin
        ifgen1: if i = 3 generate
          shared variable var : std_ulogic;
          begin
            process(D)
            begin
              var := D(i);
            end process;
            Q(i) <= var;
        end generate; -- ifgen1
        ifgen2: if i < 3 generate
          Q(i) <= foo(temp,i);
        end generate; --ifgen2
      end block blk;
  end generate; -- forgen
end;

