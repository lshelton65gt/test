-- Reproduces bug7673. Functions that take variable arguments and
-- return a constant value based on the argument may need to be
-- statically computed at compile time. One such case is when
-- the value is used to determine bounds of a slice or function
-- return type size. This test used to fail because the numbits()
-- function's constant return value could not be determined at compile time.
-- The code has been fixed to do that and this test works now.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package p is

  function pickbits(invec: STD_LOGIC_VECTOR; nbits: integer) return STD_LOGIC_VECTOR;

  function numbits (invec: STD_LOGIC_VECTOR ) return integer;

  function pickbits(invec: signed; nbits: integer) return signed;

  function makesigned(invec: std_logic_vector ) return signed;

end p;

package body p is

  function pickbits (invec: STD_LOGIC_VECTOR; nbits: integer) return STD_LOGIC_VECTOR is
    variable ret : std_logic_vector(2*nbits-1 downto 0);
  begin
    ret := invec(2*nbits-1 downto 0);
    return ret;
  end pickbits;

  function numbits (invec: STD_LOGIC_VECTOR ) return integer is
    constant lrange      : integer := invec'high - invec'low;
    constant nbits       : integer := (lrange+1)/2;
  begin
    return nbits;
  end numbits;

  function pickbits (invec: signed; nbits: integer) return signed is
    variable ret : signed(2*nbits-1 downto 0);
  begin
    ret(2*nbits-1 downto 0) := invec(2*nbits-1 downto 0);
    return ret;
  end pickbits;

  function makesigned(invec: std_logic_vector) return signed is
    constant lrange      : integer := invec'high - invec'low;
    variable x : signed(lrange downto 0);
  begin
    x := signed(invec(lrange downto 0));
    return x;
  end makesigned;

end p;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.p.all;

entity bug7673 is
  
  port (
    clk   : in std_logic;
    reset : in std_logic;
    wr    : in std_logic;
    invec : in std_logic_vector(39 downto 0);
    outvec : out std_logic_vector(31 downto 0));

end bug7673;

architecture arch of bug7673 is

  constant cInWidth   : natural := 40;
  constant cOutWidth    : natural := 16;

  signal sigout : std_logic_vector(2*cOutWidth-1 downto 0);
  signal sigin : std_logic_vector(cInWidth-1 downto 0);

  procedure getbits(signal outvec:	inout std_logic_vector;
                    signal invec:	in    std_logic_vector;
                    signal wr   :       in    std_logic) is
  begin
    if wr = '1' then
      outvec <= std_logic_vector(pickbits(makesigned(invec), numbits(outvec)));
    else
      outvec <= pickbits(invec, numbits(outvec));
    end if;
  end getbits;
  
begin  -- arch

  outvec <= sigout;

  proc: process (clk, reset, invec, sigout)
  begin  -- process proc
    if reset = '0' then                 -- asynchronous reset (active low)
      sigout <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      sigin <= invec;
      getbits(sigout, sigin, wr);
    end if;
  end process proc;

end arch;
