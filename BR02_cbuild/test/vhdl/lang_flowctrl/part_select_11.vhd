-- part select with enum type index constraint.  Since the type is std_logic,
-- we will alert on each metalogical value.  Demoting the Alert to a Warning
-- _will_ generate incorrect simulation results, because we don't represent the
-- values for std_logic as 9 states, but 4 states maximum.
library ieee;
use ieee.std_logic_1164.all;

entity part_select_11 is
  port(in1,in2 : in std_ulogic_vector(0 to 8);
       out1: out std_ulogic_vector(0 to 8));
end;

architecture arch of part_select_11 is

type my_array_type is array (std_logic) of std_logic;
signal my_array1 : my_array_type;
signal my_array2 : my_array_type;

begin
  p1 : process(in1,in2, my_array1('U' to '1'), my_array2('U' to '1'))
  begin
    my_array1('U') <= in1(0);
    my_array2('U') <= in2(0);
    my_array1('X') <= in1(1);
    my_array2('X') <= in2(1);
    my_array1('0') <= in1(2);
    my_array2('0') <= in2(2);
    my_array1('1') <= in1(3);
    my_array2('1') <= in2(3);
    my_array1('Z') <= in1(4);
    my_array2('Z') <= in2(4);
    my_array1('W') <= in1(5);
    my_array2('W') <= in2(5);
    my_array1('L') <= in1(6);
    my_array2('L') <= in2(6);
    my_array1('H') <= in1(7);
    my_array2('H') <= in2(7);
    my_array1('-') <= in1(8);
    my_array2('-') <= in2(8);
    if ( my_array1('U' to '1') = my_array2('U' to '1') ) then
      out1 <= in1;
    else
      out1 <= in2;
    end if;
    end process;
end;

