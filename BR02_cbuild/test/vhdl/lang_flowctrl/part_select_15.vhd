-- variable width part select lavalue
library ieee;
use ieee.std_logic_1164.all;

entity part_select_15 is
  port(in1,in2 : std_logic_vector(0 to 3);
       out1: out std_logic_vector(0 to 3));
end;

architecture arch of part_select_15 is
begin
  p1 : process(in1,in2)
  begin
    for i in 3 downto 0 loop
      for j in 3 downto 0 loop
      if ( j > i) then
        out1(i to j)   <= in1(i to j) or in2(i to j);
      end if;
      end loop;
    end loop;
  end process;
end;

