-- Variable index, variable width part select where the index is  signal/var
library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity part_select_10 is
  port(in1, in2 : in unsigned(0 to 3);
       in3      : in unsigned(0 to 1);
       out1     : out unsigned(0 to 3));
end;

architecture arch of part_select_10 is
signal i : integer range 0 to 3 := 0;
signal j : integer range 0 to 3 := 1;
begin
  i <= to_integer(in3);
  j <= (to_integer(in3) + 1) mod 4;
  p1 : process(in1,in2, i)
  begin
    if (i = 2) then
      out1(2 to 3)   <= in1(i to j) and in2(i to j);
    elsif (i = 0) then
      out1(0 to 1)   <= in1(i to j) and in2(i to j);
    end if;
  end process;
end;
