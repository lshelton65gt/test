-- last mod: Mon May  7 09:01:56 2007
-- filename: test/vhdl/lang_flowctrl/dynamic_part_select_01.vhd
-- Description:  This test has variable index, fixed width part select where the index is  signal/var
-- we expect that out1 and out2 will always have the same values.
library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity dynamic_part_select_01 is
  port(in1, in2 : in unsigned(6 downto 0);
       in3      : in unsigned(1 downto 0);
       out1     : out unsigned(3 downto 0); -- declared as downto
       out2     : out unsigned(0 to 3));    -- declared as to
end;

architecture arch of dynamic_part_select_01 is
signal i : integer range 0 to 3 := 0;
begin
  i <= to_integer(in3);
  p1 : process(in1,in2, i)
  begin
    out1 <= in1(i+3 downto i) and in2(i+3 downto i);
    out2 <= in1(i+3 downto i) and in2(i+3 downto i);
  end process;
end;
