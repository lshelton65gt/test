use std.standard.all;

entity for_in_proc1 is
  port ( in1 : bit_vector ( 0 to 7 );
         in2 : bit_vector ( 0 to 7 );
         output : out  bit_vector ( 0 to 7 )
       );

end ;

architecture for_in_proc1 of for_in_proc1 is
procedure proc
   ( signal in1 : bit_vector ( 0 to 7 );
     signal in2 : bit_vector ( 0 to 7 ) ;
     signal output : out bit_vector ( 0 to 7 )
   )  is
begin
   for i in 0 to 7 loop
    output(i) <= in1(i) and in2(i);
   end loop;
end ;
begin
   proc ( in1 ,in2, output );
end ;
