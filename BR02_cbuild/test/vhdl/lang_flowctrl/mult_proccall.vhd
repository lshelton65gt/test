entity mult_proccall is
  port (in1,in2: in bit_vector(0 to 1);  out1: out bit_vector(0 to 1));
end;

architecture a_top of mult_proccall is
procedure proc (signal a1, a2 : in bit; signal b: out bit) is 
begin
  b <=  a1 and a1;
end;

begin
  proc(in1(0), in2(0), out1(0));
  proc(in1(1), in2(1), out1(1));
end;

