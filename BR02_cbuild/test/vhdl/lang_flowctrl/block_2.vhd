entity block_2 is
  port(grd : in bit;  a,b:in bit; c1,c2,c3:out bit);
end;

architecture arch of block_2 is
begin
  a1:block (grd = '1') is
  begin
  c1 <= GUARDED a;
  with a select
  c2 <= GUARDED b when '0' , '1' when others;
  c3 <= GUARDED a and b when a = b else a or b ;
  end block;
end;

