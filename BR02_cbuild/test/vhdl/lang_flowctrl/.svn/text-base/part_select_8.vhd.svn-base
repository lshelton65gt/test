-- Note: this test used to have in1,in2,out1 defined with range 0 to 3.  That
-- resulted in out-of-bounds writes in Aldec and MTI, resulting in
-- SEGV and error message, respectively.
--
-- I changed the output declaration to "0 to 4".  With input declarations
-- at "0 to 3", an out-of-bound read can occur, which results in mismatches
-- with Aldec.  I don't think that's what we are trying to test, so I changed
-- the test to avoid that.

library ieee;
use ieee.std_logic_1164.all;

entity part_select_8 is
  port(in1,in2 : std_logic_vector(0 to 4);
       out1: out std_logic_vector(0 to 4));  -- foo(3)==4, don't assign out-of-bounds
end;

architecture arch of part_select_8 is
function foo(arg : integer) return integer is
begin
  if ( arg < 2 ) then
    return arg;
  else
    return arg + 1;
  end if;
end;

begin
  p1 : process(in1,in2)
  begin
  for i in 0 to 3 loop
    out1(i to foo(i))   <= in1(i to foo(i));
  end loop;
  end process;
end;
