-- This is a complicated nested generate statement that uses
-- non-synthesizable MOD calls both in generate loop elaboration and in
-- component instantiation.  This testcase checks to ensure that we a) accept
-- these non-synthesizable uses of MOD, since they are globally static, and b)
-- that we can evaluate the static expression.

library	ieee;
use ieee.std_logic_1164.all;

entity dregibit is
    port (
      clock_dr          : in	std_logic;
      par_data_i	: in	std_logic;
      tck		: out	std_logic
    );
end dregibit;

architecture a of dregibit is
begin
  tck <= clock_dr and par_data_i;
end a;

library	ieee;
use ieee.std_logic_1164.all;

entity globallystaticmod is
  port (data_par_i, clock_dr_o : std_logic_vector(0 to 280);
        tck_o : out std_logic_vector(0 to 280) );
  constant TOTSIG : integer := 281;
end globallystaticmod;

architecture behavioral of globallystaticmod is

  component dregibit
    port (
      clock_dr          : in	std_logic;
      par_data_i	: in	std_logic;
      tck		: out	std_logic
    );
  end component;	-- dregibit

begin
  
  -- for loop 0 to 280
  L0: for sigcnt in 0 to TOTSIG-1 GENERATE
    -- if (sigcnt < 270) and (sigcnt mod 10 == 0)
    L1: if (sigcnt < (TOTSIG-(TOTSIG mod 10)-10) and (sigcnt mod 10 = 0)) GENERATE
      -- if (sigcnt == 20)
      L2: if (sigcnt = 20) GENERATE
        drb : dregibit
          port map (
            clock_dr	=> clock_dr_o(sigcnt-(sigcnt mod 10)-10),
            par_data_i	=> data_par_i(sigcnt),
            tck		=> tck_o(sigcnt-(sigcnt mod 10))
          );
      end GENERATE;	-- L2
    end GENERATE;	-- L1
  end GENERATE;		-- L0
end;
