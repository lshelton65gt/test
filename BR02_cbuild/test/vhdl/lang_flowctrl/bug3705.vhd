LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.STD_LOGIC_ARITH.all;

ENTITY bug3705 IS
    PORT (
       data        : IN std_logic_vector(12 downto 0);
       target      : OUT std_logic_vector(15 downto 0)
    );
END bug3705;

ARCHITECTURE synth OF bug3705 IS
    FUNCTION max (A, B: INTEGER) return INTEGER is
    BEGIN
        IF A > B then   return A;
        ELSE            return B;
        END IF;
    END;

  FUNCTION func1     (a   : std_logic_vector;
                        b   : std_logic_vector) RETURN STD_LOGIC_VECTOR IS
    CONSTANT length     : INTEGER := max(a'LENGTH, b'LENGTH);
    VARIABLE sat_val    : STD_LOGIC_VECTOR(length-1 DOWNTO 0);
  BEGIN
      sat_val := (others => '1');
      sat_val := a;
      RETURN (sat_val);
  END func1;

    FUNCTION odr4 (a : STD_LOGIC_VECTOR) RETURN STD_LOGIC_VECTOR IS
        VARIABLE n : NATURAL;
    BEGIN
        n := a'LENGTH;

         RETURN func1 ( a(n-1 DOWNTO 4), conv_std_logic_vector(0, n-4));
    END odr4;

BEGIN
  target(8 DOWNTO 0) <= odr4(data);
END synth;
