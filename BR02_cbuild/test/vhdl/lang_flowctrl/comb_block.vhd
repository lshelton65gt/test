entity comb_block is
  port(in1, in2 : bit; out1 : out bit);
end;

architecture arch of comb_block is
begin
  process(in1,in2)
  begin
    if (in1 = '1') then
      out1 <= in1 xor in2;
    else
      out1 <= in2;
    end if;
  end process;
end;
