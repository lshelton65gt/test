-- The alias has opposite direction of the range of the actual object
-- The offset is 0, negative and positive . So, this is overall a complex
-- test with alias and part-select .
library IEEE;
use IEEE.std_logic_1164.all;

entity partsel_alias3 is
  port ( IN1, IN2 : std_logic_vector ( 3 downto 0 ); 
         OUTPUT : out std_logic_vector(3 downto 0));
end entity partsel_alias3;

architecture partsel_alias3 of partsel_alias3 is
begin
  process ( IN1, IN2 )
    alias AL_OUTPUT : std_logic_vector ( 10 downto 7) is OUTPUT;
    alias ALIN1 : std_logic_vector(1 to 4) is IN1(3 downto 0); 
    alias ALIN2 : std_logic_vector(0 to 3) is IN2(3 downto 0); 
  begin 
    for i in 0 to 3 loop
      if (i = 0)  or ( i = 2 ) then
        AL_OUTPUT(10 - i downto 9 - i) <= ALIN1(i+1 to i + 2) and
                                          ALIN2(i to i + 1);
      end if;
    end loop;
  end process;
end architecture partsel_alias3;
