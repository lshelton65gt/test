package package_for is
 subtype vector8 is bit_vector(0 to 7);
end package_for;

use std.standard.all;
use work.package_for.all;

entity for_in_func1 is
  port ( in1 : bit_vector ( 0 to 7 );
         in2 : bit_vector ( 0 to 7 );
         output : out vector8
       );

end ;

architecture for_in_func1 of for_in_func1 is
function func
   ( signal  in1 : bit_vector ( 0 to 7 );
     signal in2 : bit_vector ( 0 to 7 )
   ) return vector8 is
 variable var : vector8;
begin
   for i in 0 to 7 loop
    var(i) := in1(i) and in2(i);
   end loop;
   return var;
end;

begin
  output <= func ( in1 ,in2 );
end ;

