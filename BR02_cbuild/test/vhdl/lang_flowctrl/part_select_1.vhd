library ieee;
use ieee.std_logic_1164.all;

entity part_select_1 is
  port (d : in std_logic_vector(2 downto 0); q: out std_logic_vector(2 downto 0));
end;

architecture arch of part_select_1 is
begin
  process (d(0))
  begin
	if (d(0) = '1' and d(0)'event) then
      q(2 downto 1) <= d(2 downto 1);
	end if;
  end process;
end;
