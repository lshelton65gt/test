library ieee;
use ieee.std_logic_1164.all;

entity block_in_for_gen is
  port (
      D    : IN     std_ulogic_vector(1 DOWNTO 0);
      Q    : OUT    std_ulogic_vector(1 DOWNTO 0)
     );
end;

architecture arch of block_in_for_gen is
begin
  gen1 : for i in 1 downto 0 generate
    blk: block
    begin 
      Q(i) <= D(i);
    end block blk;
  end generate;
end;

