-- Variable index, variable width part select of alias where the 
-- index is  signal/var
library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity part_select_13 is
  port(in1,in2 : unsigned(0 to 3);
       in3 : in unsigned(0 to  1);
       out1: out unsigned(0 to 3));
end;

architecture arch of part_select_13 is
signal i : integer range 0 to 5;
alias tmp1 : unsigned(1 to 4) is in1;
alias tmp2 : unsigned(1 to 4) is in2;
alias alin1 : unsigned(2 to 5) is tmp1;
alias alin2 : unsigned(2 to 5) is tmp1;
begin
  i <= to_integer(in3) + 2;
  p1 : process(in1,in2)
  begin
    for i in 3 downto 0 loop
      if (i = 2) then
        out1(2 to 3)   <= alin1(2+i to i*2 + 1) and alin2(2+i to i*2 + 1);
      elsif (i = 0) then
        out1(0 to 1)   <= alin1(2+i to i+3) and alin2(2+i to i+3);
      end if;
    end loop;
  end process;
end;
