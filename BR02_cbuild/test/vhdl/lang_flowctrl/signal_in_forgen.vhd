entity leaf is port (a, b, c: in bit; y: out bit); end;
architecture rtl of leaf is
   signal tmp: bit;
begin
   y <= tmp and c;
   tmp <= a and b;
end;

entity signal_in_forgen 
   is port (a, b, c: in bit_vector (3 downto 0); 
            y: out bit_vector (3 downto 0));
end;

architecture rtl of signal_in_forgen is
   component leaf 
       port (a, b, c: in bit; y: out bit); 
   end component;
begin
   inst0 : leaf port map (a=>a(0), b=>b(0), c=>c(0), y=>y(0));
   for_label : for i in 1 to 3 generate
   signal output_tmp : bit_vector (3 downto 0);
   begin
      forgen_inst : leaf port map (a=>a(i), b=>b(i), c=>c(i), y=>output_tmp(i));
    y(i) <= output_tmp(i) ;
   end generate;
end;

