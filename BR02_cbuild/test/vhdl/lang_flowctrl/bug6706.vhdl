-- Reproduces bug6706 where a part select assigned an
-- aggregate results in an error. This has been fixed.
library ieee;
use ieee.std_logic_1164.all;

package pkg is

  constant cWidth : integer := 4;

end pkg;

library ieee;
use ieee.std_logic_1164.all;

use work.pkg.all;

entity bug6706 is
  
  generic (
    WIDTH : integer := 4);

  port (
    en   : in std_logic;
    in1  : in std_logic_vector(1 downto 0);
    out1 : out std_logic_vector(cWidth-1 downto 0));

end bug6706;

architecture synth of bug6706 is

begin  -- synth

  testProc: process (en, in1)
  begin  -- process testProc
    if (en = '1') then
      out1(3 downto 2) <= in1;
      out1(1 downto 0) <= "01";
    end if;

    if ((WIDTH /= 4) and (en = '1')) then
      out1(3 downto WIDTH) <= (others => '0'); -- cbuild used to error here before the fix!
    end if;
  end process testProc;

end synth;
