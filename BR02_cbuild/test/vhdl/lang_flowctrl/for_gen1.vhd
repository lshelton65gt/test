library ieee;
use ieee.std_logic_1164.all;

entity flop is
  port (
      D   : IN  std_ulogic;
      CLK : IN  std_ulogic;
      CLR : IN  std_ulogic;
      Q   : OUT std_ulogic
     );
end;

architecture flop_arc of flop is
begin
  process(CLK, CLR)
  begin
    if (CLR = '1') then
      Q <= '0';
    elsif (CLK = '1' and CLK'EVENT) then
      Q <= D;
    end if;
  end process;
end;


library ieee;
use ieee.std_logic_1164.all;

entity for_gen1 is
  port (
      D    : IN     std_ulogic_vector(31 DOWNTO 0);
      aclk : IN     std_ulogic;
      rst  : IN     std_ulogic;
      Q    : OUT     std_ulogic_vector(31 DOWNTO 0)
     );
end;

architecture arch of for_gen1 is

  COMPONENT flop
    PORT (
      D   : IN  std_ulogic;
      CLK : IN  std_ulogic;
      CLR : IN  std_ulogic;
      Q   : OUT std_ulogic
    );
  END COMPONENT;

begin
  gena : for j in 0 TO 31 generate
    flop1 : flop
      PORT MAP (
        D   =>    D(j),
        CLK =>    aclk,
        CLR =>    rst,
        Q   =>    Q(j)
      );
  end generate;
end;

