-- Negative tests, it's array index is not integer. Only
-- Integer array index is synthesizable.

library ieee;
use ieee.std_logic_1164.all;

package p1 is
type my_vector is array (boolean range FALSE to TRUE) of std_logic;
end p1;

use work.p1.all;

entity for_gen5 is
  port (
      input     : IN     my_vector;
      output    : OUT    my_vector
     );
end;

architecture arch of for_gen5 is
begin
  gen1 : for i in FALSE to TRUE generate
    output(i) <= input(i);
  end generate;
end;

