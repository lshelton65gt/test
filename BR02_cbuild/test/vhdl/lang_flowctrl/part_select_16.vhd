library ieee;
use ieee.std_logic_1164.all;

entity part_select_16 is
  port (d : in std_logic_vector(2 downto 0); q: out std_logic_vector(2 downto 0));
end;

architecture arch of part_select_16 is
signal foo : std_logic_vector(2 downto 1);
signal bar : std_logic_vector(1 to 2);
begin
  process (d(0))
  begin
	if (d(0) = '1' and d(0)'event) then
      q(foo'range) <= d(bar'reverse_range);
	end if;
  end process;
end;
