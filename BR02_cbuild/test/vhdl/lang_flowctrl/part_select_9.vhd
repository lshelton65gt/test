-- Variable index, variable width part select
library ieee;
use ieee.std_logic_1164.all;

entity part_select_9 is
  port(in1,in2 : std_logic_vector(0 to 3);
       out1: out std_logic_vector(0 to 3));
end;

architecture arch of part_select_9 is
begin
  p1 : process(in1,in2)
  begin
    for i in 3 downto 0 loop
      if (i = 2) then
        out1(2 to 3)   <= in1(i to i*2 - 1) and in2(i to i*2 - 1);
      elsif (i = 0) then
        out1(0 to 1)   <= in1(i to i+1) and in2(i to i+1);
      end if;
    end loop;
  end process;
end;

