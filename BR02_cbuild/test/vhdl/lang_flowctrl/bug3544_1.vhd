-- Tests recursive function with terminating condition which is
-- a constant expression. Compiler successfully populates this.
library ieee;
use ieee.std_logic_1164.all;

package tmp is
    FUNCTION  OR_RED  (ARG: STD_LOGIC_VECTOR) return STD_LOGIC ;
end tmp;
   
package body tmp is

    FUNCTION  OR_RED  (ARG: STD_LOGIC_VECTOR) return STD_LOGIC is
      variable UPPER_TREE, LOWER_TREE: std_logic;
      variable RESULT: STD_LOGIC;
      variable TEMP_ARG: STD_LOGIC_VECTOR(ARG'LENGTH-1 downto 0);
      constant LEN : natural := TEMP_ARG'length;
      constant MID : natural := (LEN +1)/2 + TEMP_ARG'RIGHT;
    BEGIN
 TEMP_ARG := ARG;
 if LEN = 1 then
    RESULT := TEMP_ARG(TEMP_ARG'LEFT);
 elsif LEN = 2 then
    RESULT := TEMP_ARG(TEMP_ARG'LEFT) or TEMP_ARG(TEMP_ARG'RIGHT);
 else
    UPPER_TREE := OR_RED(TEMP_ARG(TEMP_ARG'LEFT downto MID)); 
    LOWER_TREE := OR_RED(TEMP_ARG(MID -1 downto TEMP_ARG'RIGHT));
    RESULT := UPPER_TREE or LOWER_TREE;
 end if;
 return RESULT;
    END;
end tmp;
library ieee;
use ieee.std_logic_1164.all;

library work;
use work.tmp.all;

entity bug3544_1 is
    port(clk : in std_logic;
         tmp : out std_logic;
         reg2txs : in std_logic_vector(7 downto 0)
    );
end bug3544_1;

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.tmp.all;

architecture top of bug3544_1 is
begin
proc: process(clk, reg2txs) begin
      tmp <= OR_RED(reg2txs);
end process proc;
end top;
