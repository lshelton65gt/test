-- The variable index part-select has range direction 'DOWNTO'
library ieee;
use ieee.std_logic_1164.all;

entity part_select_3 is
  port(in1,in2 : std_logic_vector(3 downto 0);
       out1: out std_logic_vector(3 downto 0));
end;

architecture arch of part_select_3 is
begin
  p1 : process(in1,in2)
  begin
    for i in 3 downto 0 loop
      for j in 3 downto 0 loop
        if ( i = j) then
          if (i = 2) or (i = 0) then
            out1(i+1 downto i)   <= in1(i+1 downto i) and in2(i+1 downto i);
          end if;
        end if;
      end loop;
    end loop;
  end process;
end;
