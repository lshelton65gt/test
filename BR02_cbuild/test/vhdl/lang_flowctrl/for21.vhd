entity for21 is
  port(in1, in2 : in bit_vector( 0 to 3);
       out1     : out bit_vector( 0 to 3)) ;
end for21;

architecture for21 of for21 is

begin

      process(in1,in2)
        begin
            for i in in1'range loop
              for j in in1'range loop
                out1( j ) <= in1(i) and in2(i);
              end loop;
            end loop;
      end process;

end for21;
