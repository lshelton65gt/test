entity block_1 is
	port(a,b:in bit; c:out bit);
end;
architecture block_1 of block_1 is
begin
	a1:block (a = b) is
	signal i1:bit;
	signal tmp:boolean;
	begin
		i1 <= '0';
		tmp <= GUARD;
		a2:block
		signal i1:bit;
		begin
			i1 <= a;
			c <= i1 and b;
		end block;
	end block;
end;
