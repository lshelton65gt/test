-- Variable index, fixed width part select where the index is while loop index
library ieee;
use ieee.std_logic_1164.all;

entity part_select_12 is
  port(in1,in2 : std_logic_vector(0 to 3);
       out1: out std_logic_vector(0 to 3));
end;

architecture arch of part_select_12 is
begin
  p1 : process(in1,in2)
  variable i : integer range 0 to 4;
  begin
    i := 0;
    while ( i < 4 ) loop
      if (i = 2) then
        out1(2 to 3)   <= in1(i to i*2 - 1) and in2(i to i*2 - 1);
      elsif (i = 0) then
        out1(0 to 1)   <= in1(i to i+1) and in2(i to i+1);
      end if;
      i := i + 1;
    end loop;
  end process;
end;

