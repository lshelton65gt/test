entity for20 is
  generic( N : integer := 5 );
  port(in1, in2 : in bit_vector( 0 to N);
       out1     : out bit_vector( 0 to N) ;
       out2     : out bit_vector( 0 to N) ) ;
end for20;

architecture for20 of for20 is

begin 
        
      process(in1,in2)
        begin 
            for i in in1'reverse_range loop
              out1(i) <= in1(i) and in2(i);
            end loop;
            for i in in1'reverse_range loop
              out2(i) <= in1(i) and in2(i);
            end loop;
      end process;

end for20;
