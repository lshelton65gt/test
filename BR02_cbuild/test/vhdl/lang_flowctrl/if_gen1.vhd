library ieee;
use ieee.std_logic_1164.all;

entity flop is
  port (
      D   : IN  std_ulogic;
      CLK : IN  std_ulogic;
      CLR : IN  std_ulogic;
      Q   : OUT std_ulogic
     );
end;

architecture flop_arc of flop is
begin
  process(CLK, CLR)
  begin
    if (CLR = '1') then
      Q <= '0';
    elsif (CLK = '1' and CLK'EVENT) then
      Q <= D;
    end if;
  end process;
end;


library ieee;
use ieee.std_logic_1164.all;

entity if_gen1 is
  generic (INSTANTIATE : integer := 1);
  port (
      D    : IN     std_ulogic;
      clk : IN     std_ulogic;
      rst  : IN     std_ulogic;
      Q    : OUT     std_ulogic
     );
end;

architecture arch of if_gen1 is

  COMPONENT flop
    PORT (
      D   : IN  std_ulogic;
      CLK : IN  std_ulogic;
      CLR : IN  std_ulogic;
      Q   : OUT std_ulogic
    );
  END COMPONENT;

begin
  ifgen_label : if INSTANTIATE = 1 generate
    flop1 : flop
      PORT MAP (
        D   =>    D,
        CLK =>    clk,
        CLR =>    rst,
        Q   =>    Q
      );
  end generate;
end;

