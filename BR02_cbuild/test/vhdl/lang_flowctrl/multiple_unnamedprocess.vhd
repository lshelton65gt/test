entity multiple_unnamedprocess is
  port (d,clk,rst : in bit; q: out bit);
end;

architecture arch of multiple_unnamedprocess is
signal bool_data1, bool_data2 : bit;
begin
  process (clk, rst)
  variable tmp: bit;
  begin
    if (rst = '1') then
      tmp := '0';
      bool_data1 <= tmp;
    elsif (clk'event and clk = '1') then
      tmp := d;
      bool_data1 <= tmp;
    end if;
  end process ;
  -- Process with arbitary if/elsif cond
  process (bool_data1, clk)
  variable tmp: bit;
  begin
    if ((bool_data1 = '1') and (rst = '0')) then
      tmp := bool_data1;
    elsif (clk = '1') then
      tmp := bool_data1;
    else
      tmp := '0';
    end if;
    q <= tmp;
  end process ;
end;
