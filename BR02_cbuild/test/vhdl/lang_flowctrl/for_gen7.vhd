-- Negative test- NULL range
library ieee;
use ieee.std_logic_1164.all;

entity for_gen7 is
  port (
      D    : IN     std_ulogic_vector(1 DOWNTO 0);
      Q    : OUT    std_ulogic_vector(1 DOWNTO 0)
     );
end;

architecture arch of for_gen7 is
begin
  gen1 : for i in 1 to 0 generate
    Q(i) <= D(i);
  end generate;
end;

