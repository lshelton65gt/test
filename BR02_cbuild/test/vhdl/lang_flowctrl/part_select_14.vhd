-- The lvalue is not variable width ... it is dynamic indexed but
-- fixe width slice
library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity part_select_14 is
  port(in1,in2 : unsigned(0 to 3);
       in3 : in unsigned(0 to  1);
       out1: out unsigned(0 to 3));
end;

architecture arch of part_select_14 is
signal i : integer range 0 to 3;
signal j : integer range 0 to 3;
begin
  i <= to_integer(in3);
  j <= i + 1;
  p1 : process(in1,in2,i,j)
  begin
    if (i = 2) then
      out1(i to j)   <= in1(2 to 3) and in2(2 to 3);
    elsif (i = 0) then
      out1(i to j)   <= in1(0 to 1) and in2(0 to 1);
    end if;
  end process;
end;
