-- The variable index part-select has range direction 'TO'
library ieee;
use ieee.std_logic_1164.all;

entity part_select_4 is
  port(in1,in2 : std_logic_vector(0 to 3);
       out1: out std_logic_vector(0 to 3));
end;

architecture arch of part_select_4 is
begin
  p1 : process(in1,in2)
  begin
    for i in 3 downto 0 loop
      for j in 3 downto 0 loop
        if ( i = j) then
          if (i = 2) then
            out1(2 to 3)   <= in1(i to i+1) and in2(i to i+1);
          elsif (i = 0) then
            out1(0 to 1)   <= in1(i to i+1) and in2(i to i+1);
          end if;
        end if;
      end loop;
    end loop;
  end process;
end;

