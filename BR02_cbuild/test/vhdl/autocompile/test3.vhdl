library ieee;
use ieee.std_logic_1164.all;

use work.packB.all;

entity test3 is
  
  port (in1  : in  typeB;
        out1 : out typeB);

end test3;

architecture toparch of test3 is

  component test1
    port (in1  : in  typeB;
          out1 : out typeB);
  end component;
  
begin  -- toparch

  u1 : test1
  port map ( in1  => in1,
             out1 => out1);

end toparch;
