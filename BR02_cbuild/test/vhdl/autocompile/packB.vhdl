library ieee;
use ieee.std_logic_1164.all;

use work.packA.all;

package packB is

  subtype typeB is typeA range 0 to 10 ;

end packB;
