-- this file is used to test a mixture of vhdl 87 and 93 RTL files.
-- This file can only be successfully parsed in -87 mode because it uses the
-- name 'xnor' as a port name
library IEEE;
use IEEE.std_logic_1164.all;
package conv_pack_vhdl_top_93 is
  type arr is array ( integer range<> ) of STD_LOGIC;
end ;
   

use work.conv_pack_vhdl_top_93.all;

library IEEE;
use IEEE.std_logic_1164.all;

use work.conv_pack_vhdl_top_93.all;
library IEEE;
use IEEE.std_logic_1164.all;

entity vhdl_top_93 is
  port ( clock    : in  std_logic;
         in1,in2 : in std_logic_vector(3 downto 0);
         output  : out std_logic_vector( 3 downto 0)
       );
end;


architecture vhdl_top_93 of vhdl_top_93 is
  signal sig1 : STD_LOGIC;
  signal sig2 : STD_LOGIC;
  signal sig3 : STD_LOGIC;
 component vhdl_87aa 
    port (
        xnor1    : in  std_logic;            --this line only works in -87 (because 'xnor' is a reserved word in -93
        in1, in2 : in  std_logic_vector(3 downto 0);
        out1     : out std_logic_vector(3 downto 0));
 end component;
 component vhdl_87bb 
    port (
        xnor1    : in  std_logic;            --this line only works in -87 (because 'xnor' is a reserved word in -93
        in1, in2 : in  std_logic_vector(3 downto 0);
        out1     : out std_logic_vector(3 downto 0));
 end component;
 component vhdl_93a 
    port (
            clock    : in  std_logic;
            in1, in2 : in  std_logic_vector(3 downto 0);
            out1     : out std_logic_vector(3 downto 0));
 end component;
 component vhdl_93b
    port (
            clock    : in  std_logic;
            in1, in2 : in  std_logic_vector(3 downto 0);
            out1     : out std_logic_vector(3 downto 0));
 end component;

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      sig1 <= sig2 xnor sig3;    -- this line works in -93 only (because 'xnor' operator was only introduced in -93
    end if;
  end process;
  inst1 : vhdl_87aa port map (clock,in1,in2,output);
  inst2 : vhdl_87bb port map (clock,in1,in2,output);
  inst3 : vhdl_93a port map (clock,in1,in2,output);
  inst4 : vhdl_93b port map (clock,in1,in2,output);
end;
  
 

