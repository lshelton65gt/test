library ieee;
use ieee.std_logic_1164.all;

use work.packB.all;

entity test1a is
  
  port (in1  : in  typeB;
        out1 : out typeB);

end test1a;

architecture toparch of test1a is

begin  -- toparch

  out1 <= in1;

end toparch;
