library ieee;
use ieee.std_logic_1164.all;

use work.packB.all;

entity test1 is
  
  port (in1  : in  typeB;
        out1 : out typeB);

end test1;

architecture toparch of test1 is

begin  -- toparch

  out1 <= in1;

end toparch;
