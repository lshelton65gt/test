-- filename: test/directives/vhdl_87aa.vhdl
-- Description:  This file can only be parsed without error in -87 mode


library ieee;
use ieee.std_logic_1164.all;
use STD.TEXTIO.all;

entity vhdl_87aa is

    port (
        xnor1    : in  std_logic;            --this line only works in -87 (because 'xnor' is a reserved word in -93
        in1, in2 : in  std_logic_vector(3 downto 0);
        out1     : out std_logic_vector(3 downto 0));
end;

        architecture arch of vhdl_87aa is
            signal xnor : STD_LOGIC;
        begin
        main: process (xnor1)
        begin
        if xnor1'event and xnor1 = '1' then
        out1 <= in1 and in2;
        end if;
        end process;
        end;
