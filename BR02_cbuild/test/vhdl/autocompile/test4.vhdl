library ieee;
use ieee.std_logic_1164.all;

use work.packB.all;

entity test4 is
  
  port (in1  : in  typeB;
        out1 : out typeB);

end test4;

architecture toparch of test4 is

  component test3
    port (in1  : in  typeB;
          out1 : out typeB);
  end component;
  
begin  -- toparch

  u1 : test3
  port map ( in1  => in1,
             out1 => out1);

end toparch;
