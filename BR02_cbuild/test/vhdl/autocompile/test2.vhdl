library ieee;
use ieee.std_logic_1164.all;

use work.packC.all;

entity test2 is
  
  port (in1  : in  typeC;
        out1 : out typeC);

end test2;

architecture toparch of test2 is

begin  -- toparch

  out1 <= in1;

end toparch;
