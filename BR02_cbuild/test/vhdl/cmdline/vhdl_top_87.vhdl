-- this file is used to test a mixture of vhdl 87 and 93 RTL files.
-- This file can only be successfully parsed in -87 mode because it uses the
-- name 'xnor' as a port name
library IEEE;
use IEEE.std_logic_1164.all;
package conv_pack_vhdl_top_87 is
  type arr is array ( integer range<> ) of STD_LOGIC;
end ;
   

use work.conv_pack_vhdl_top_87.all;

library IEEE;
use IEEE.std_logic_1164.all;

use work.conv_pack_vhdl_top_87.all;
library IEEE;
use IEEE.std_logic_1164.all;

entity vhdl_top_87 is
  port ( clock    : in  std_logic;
         in1,in2 : in std_logic_vector(3 downto 0);
         output  : out std_logic_vector( 3 downto 0)
       );
end;


architecture vhdl_top_87 of vhdl_top_87 is
 component vhdl_87a 
    port (
        xnor    : in  std_logic;            --this line only works in -87 (because 'xnor' is a reserved word in -93
        in1, in2 : in  std_logic_vector(3 downto 0);
        out1     : out std_logic_vector(3 downto 0));
 end component;
 component vhdl_87b 
    port (
        xnor    : in  std_logic;            --this line only works in -87 (because 'xnor' is a reserved word in -93
        in1, in2 : in  std_logic_vector(3 downto 0);
        out1     : out std_logic_vector(3 downto 0));
 end component;
 component vhdl_93a 
    port (
            clock    : in  std_logic;
            in1, in2 : in  std_logic_vector(3 downto 0);
            out1     : out std_logic_vector(3 downto 0));
 end component;
 component vhdl_93b 
    port (
            clock    : in  std_logic;
            in1, in2 : in  std_logic_vector(3 downto 0);
            out1     : out std_logic_vector(3 downto 0));
 end component;

begin
  inst1 : vhdl_87a port map (clock,in1,in2,output);
  inst2 : vhdl_87b port map (clock,in1,in2,output);
  inst3 : vhdl_93a port map (clock,in1,in2,output);
  inst4 : vhdl_93b port map (clock,in1,in2,output);
end;
  
 

