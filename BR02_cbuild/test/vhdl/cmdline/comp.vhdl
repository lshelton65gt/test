 entity comp is
   port (
      ca : in  bit_vector(1 downto 0);
      cb : out bit_vector(0 to 1));
 end entity comp;

 architecture arch of comp is

 begin  -- arch

   cb <= ca;

 end arch;
