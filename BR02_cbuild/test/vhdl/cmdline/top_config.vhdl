entity test is
  port (d,clk,rst: in bit; q: out bit);
end;

architecture arch of test is
begin
  process (d, rst)
  begin
    if (rst = '0')  then
      q <= '0';
    else
      q <= d;
    end if;
  end process;
end;

configuration top_config of test is
	for arch
	end for;
end;
