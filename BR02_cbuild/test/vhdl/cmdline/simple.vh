entity top is
  port (
    in1  : in  integer;
    in2  : in  integer;
    outp : out integer);
end;

architecture a of top is

begin

  outp <= in1 + in2;

end a;
