entity multiple_archs is
  port (in1,in2: in bit; out1: out bit);
end;

architecture behav_and of multiple_archs is
begin
  out1 <= in1 and in2;
end;

architecture behav_or of multiple_archs is
begin
  out1 <= in1 or in2;
end;
