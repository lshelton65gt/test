-- filename: test/directives/vhdl_93.vhdl
-- Description:  This test 


library ieee;
use ieee.std_logic_1164.all;
use STD.TEXTIO.all;

entity vhdl_93 is
generic (
        WIDTH : integer := 4;
DEPTH : integer := 4);


port (
        clock    : in  std_logic;
        in1, in2 : in  std_logic_vector((WIDTH-1) downto 0);
out1     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of vhdl_93 is
    begin
main: process (clock)
    --file file_87_style : TEXT is in "filename.txt";  -- this line works in -87 only 
    file file_93_style : TEXT open READ_MODE is "filename.txt";  -- this line works in -93 only 
    begin
    if clock'event and clock = '1' then
    out1 <= in1 and in2;
    end if;
    end process;
    end;
