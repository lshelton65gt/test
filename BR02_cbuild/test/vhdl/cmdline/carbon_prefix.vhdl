--Test the default translate_off/on pragmas

library IEEE;
use IEEE.std_logic_1164.all;

entity e is
  port( s: out std_logic );
end e;

architecture a of e is

   function To_Stdlogic(s    : character) return std_logic
   is
-- carbon translate_off
     xyzzy
     type Table is array(character) of std_logic;
-- carbon translate_on
-- carbon synthesis_off
     plugh
     constant stdlogic_Table : Table
       := ('U'=> 'U', 'X'=> 'X', '0'=> '0',
           'u'=> 'U', 'x'=> 'X', 'l'=> 'L',
           '1'=> '1', 'Z'=> 'Z', 'W'=> 'W',
           'z'=> 'Z', 'w'=> 'W', 'h'=> 'H',
           'L'=> 'L', 'H'=> 'H', '-'=> '-',
           others => 'X');
-- carbon synthesis_on
      variable retval : std_logic;
   begin
-- carbon translate_off
      zork was a fun game
-- carbon translate_on
-- carbon synthesis_off
      but it was based on adventure
      return stdlogic_Table(s);
-- carbon synthesis_on
-- pragma translate_off
      -- make sure that pragma is NOT a valid prefix
      case s is
-- pragma translate_on
        when '1' | 'H' | 'h' =>
          retval := '1';
        when '0' | 'L' | 'l' =>
          retval := '0';
        when 'Z' | 'z' =>
          retval := 'Z';
        when others =>
          retval := 'X';
-- synopsys translate_off
      -- make sure that synopsys is NOT a valid prefix
      end case;
-- synopsys translate_on
      return retval;
   end;

   signal c : character;
begin

  s <= To_StdLogic( c );
  
end a;
