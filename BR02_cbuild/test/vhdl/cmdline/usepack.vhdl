library util;
use util.libpack.all;

entity e is
  port (
    in1  : in  fourbitint;
    in2  : in  fourbitint;
    outp : out eightbitint);
end e;

architecture a of e is

begin

  outp <= in1 + in2;

end a;
