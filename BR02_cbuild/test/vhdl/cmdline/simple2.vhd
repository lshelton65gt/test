entity e is
  port (
    in1  : in  integer;
    in2  : in  integer;
    outp : out integer);
end;

architecture a of e is

begin

  outp <= in1 + in2;

end a;
