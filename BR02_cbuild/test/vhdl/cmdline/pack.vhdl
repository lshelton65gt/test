library ieee;
use ieee.std_logic_1164.all;

package libpack is

  subtype fourbitint is integer range 0 to 15;
  subtype eightbitint is integer range 0 to 255;
  subtype fourbits is std_logic_vector(3 downto 0);
  subtype eightbits is std_logic_vector(7 downto 0);

  constant fourbitint_0 : fourbitint := 0;
  constant eightbitint_1 : eightbitint := 1;
  constant eightbitint_16 : eightbitint := 16;
  constant fourbit_0 : fourbits := (others => '0');
  constant eightbit_16 : eightbits := "00010000";
end libpack;
