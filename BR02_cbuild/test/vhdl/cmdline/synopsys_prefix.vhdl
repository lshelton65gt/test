--Test the default translate_off/on synopsyss

library IEEE;
use IEEE.std_logic_1164.all;

entity e is
  port( s: out std_logic );
end e;

architecture a of e is

   function To_Stdlogic(s    : character) return std_logic
   is
-- synopsys translate_off
     xyzzy
     type Table is array(character) of std_logic;
-- synopsys translate_on
-- synopsys synthesis_off
     plugh
     constant stdlogic_Table : Table
       := ('U'=> 'U', 'X'=> 'X', '0'=> '0',
           'u'=> 'U', 'x'=> 'X', 'l'=> 'L',
           '1'=> '1', 'Z'=> 'Z', 'W'=> 'W',
           'z'=> 'Z', 'w'=> 'W', 'h'=> 'H',
           'L'=> 'L', 'H'=> 'H', '-'=> '-',
           others => 'X');
-- synopsys synthesis_on
      variable retval : std_logic;
   begin
-- synopsys translate_off
      zork was a fun game
-- synopsys translate_on
-- synopsys synthesis_off
      but it was based on adventure
      return stdlogic_Table(s);
-- synopsys synthesis_on
      case s is
        when '1' | 'H' | 'h' =>
          retval := '1';
        when '0' | 'L' | 'l' =>
          retval := '0';
        when 'Z' | 'z' =>
          retval := 'Z';
        when others =>
          retval := 'X';
      end case;
      return retval;
   end;

   signal c : character;
begin

  s <= To_StdLogic( c );
  
end a;
