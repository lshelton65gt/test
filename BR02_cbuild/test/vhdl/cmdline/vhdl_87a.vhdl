-- filename: test/directives/vhdl_87a.vhdl
-- Description:  This file can only be parsed without error in -87 mode


library ieee;
use ieee.std_logic_1164.all;
use STD.TEXTIO.all;

entity vhdl_87a is

    port (
        xnor    : in  std_logic;            --this line only works in -87 (because 'xnor' is a reserved word in -93
        in1, in2 : in  std_logic_vector(3 downto 0);
        out1     : out std_logic_vector(3 downto 0));
end;

        architecture arch of vhdl_87a is
        begin
        main: process (xnor)
        begin
        if xnor'event and xnor = '1' then
        out1 <= in1 and in2;
        end if;
        end process;
        end;
