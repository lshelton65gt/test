entity top is
  
  port (
    a : in  bit_vector(1 downto 0);
    b : out bit_vector(0 to 1));

end;

architecture arch of top is

  component comp
    port (
      ca : in  bit_vector(1 downto 0);
      cb : out bit_vector(0 to 1));
  end component;

begin  -- arch

  u1: comp
    port map ( ca => a, cb => b );

end arch;
