entity top_ent_arch is
  port (d,clk,rst: in bit; q: out bit);
end;

architecture arch of top_ent_arch is
begin
  process (d, rst)
  begin
    if (rst = '0')  then
      q <= '0';
    else
      q <= d;
    end if;
  end process;
end;
