-- sim gold file generated with MTI.  Aldec gives all Z

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.std_logic_arith.all;

entity dyn_slice8 is  
    port (
      clk : in std_logic;
      rst : in std_logic;
      ldena : in std_logic;
      idx : in unsigned(3 downto 0);
      ldval : in std_logic_vector(31 downto 0);
      result : out std_logic_vector(31 downto 0)
      );
end dyn_slice8;

architecture dyn_slice8 of dyn_slice8 is

 begin
   process (clk, rst, ldena, ldval)
     variable idx_var : integer;
     variable result_var : std_logic_vector(31 downto 0);
     constant zeros : std_logic_vector(15 downto 0) := "0000000000000000";
   begin
     if (rst = '1') then
       result_var := "00000000000000000000000000000000";
       result <= result_var;
     else
       if (clk = '1' and clk'event) then
         if ldena = '1' then
           result_var := ldval;
         else
           idx_var := to_integer(idx);
           result_var(31 - idx_var downto 16) := result_var(31 downto 16 + idx_var);
           result_var(31 downto 31 - idx_var) := zeros(15 downto 15 - idx_var);
         end if;
         result <= result_var;
       end if;
     end if;
   end process;
end dyn_slice8;

