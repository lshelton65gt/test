-- variant on dyn_slice where there are three variable width slices
-- in the concatenation, and two indices.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity dyn_slice3 is  
    port (
         a : in std_logic_vector(63 downto 0);
         b : in std_logic_vector(63 downto 0);
         c : in std_logic_vector(63 downto 0);
         i : in unsigned(4 downto 0);
         j : in unsigned(3 downto 0);
         dout : out std_logic_vector(63 downto 0)
         );
end dyn_slice3;

architecture dyn_slice3 of dyn_slice3 is
 begin

   process (a, b, c, i, j)
     variable ii : integer;
     variable jj : integer;
      begin
        ii := to_integer(i);            -- i >= 0, i <= 31
        jj := to_integer(j) + ii + 2;   -- jj > ii, jj > 1, jj <= 48
        dout <= a(63 downto jj) &
                b(jj - 1 downto ii + 1) &
                c(ii downto 0);
      end process;
end dyn_slice3;
