-- last mod: Fri Sep 15 12:05:54 2006
-- filename: test/vhdl/lang_misc/bug6585_i.vhd
-- Description:  This test is a variation on bug6585_a.vhd

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6585_i is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector(15 downto 0);
    out1     : out std_logic_vector(19 downto 0));
end;

architecture arch of bug6585_i is 
function level2 (v : std_logic_vector; len1 : integer; len2: integer) return std_logic_vector;
function level1 (v : std_logic_vector; pad : integer) return std_logic_vector;

function level2 (v : std_logic_vector; len1 : integer; len2 : integer) return std_logic_vector is 
 constant argsize : integer := v'length + len1;
 constant outsize1 : integer := v'length + len2;
 subtype rtype is std_logic_vector(outsize1-1 downto 0);
 variable retval : rtype;
begin
 retval := level1(v,1);
 return retval;
end level2;


function level1 (v : std_logic_vector; pad : integer) return std_logic_vector is
 constant outsize : integer := v'length + pad;
 constant shift : integer := v'length - pad;
 subtype rtype is std_logic_vector(outsize-1 downto 0);
 variable retval : rtype;
begin 
 retval := conv_std_logic_vector(shl(unsigned(conv_std_logic_vector(unsigned(v), outsize)), conv_unsigned(shift,3)), outsize);
 return retval;
end level1;
  
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
     for i in 3 downto 0 loop
      out1(4+5*i downto 5*i) <= level2(in1(3+4*i downto 4*i), 1, 1);
     end loop;  -- i
    end if;
  end process;
end;
