-- Reproducer for issue 74 customer 2 testcase B
--
-- Segfault in Verific::VhdlScope::PopStack, during
-- VhdlDeadBranchRewriter.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity issue74 is
  port (
         clk : in std_logic;
         in1 : in std_logic_vector(7 downto 0);
         in2 : in std_logic_vector(7 downto 0);
         out1 : out std_logic_vector(7 downto 0)
       );
end issue74;

architecture rtl of issue74 is

   function func1(constant N : integer; in1 : std_logic_vector; in2 : std_logic_vector) return std_logic_vector is
      variable result : std_logic_vector(in1'range);
   begin
      if (N > 1) then
        -- The existence of this loop triggers the failure.
        -- Remove the loop, the failure goes away.
        for i in in1'range loop
          for j in result'range loop
            result(j) := not in1(i);
          end loop;  -- j
        end loop;
      else
         result := in2;
      end if;
      return result;
   end;

   constant SIZE  : integer := 1;

begin
   out1 <= func1(SIZE, in1, in2);  
end rtl;
