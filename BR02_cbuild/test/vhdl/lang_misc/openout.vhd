-- This tests to ensure that an open output port is accepted by cbuild.
-- Since the port is open in instance u2, out2 should always remain
-- undriven, and hence 'z'.  out1 will follow "in1 and in2".

entity bottom is
  port (
    in1, in2 : in  bit;
    out1     : out bit);
end bottom;

architecture arch of bottom is

begin

  out1 <= in1 and in2;

end arch;

entity openout is
  port (
    in1, in2   : in  bit;
    out1, out2 : out bit);
end openout;

architecture arch of openout is

  component bottom
    port (
      in1, in2 : in  bit;
      out1     : out bit);
  end component;
  
begin

  u1: bottom
    port map (
      in1  => in1,
      in2  => in2,
      out1 => out1);

  u2: bottom
    port map (
      in1  => in1,
      in2  => in2,
      out1 => open);

end arch;
