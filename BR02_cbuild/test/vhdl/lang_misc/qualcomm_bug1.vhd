library ieee;
use ieee.std_logic_1164.all;

package pack is
  subtype foo is natural range 11 downto 0;
end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity qualcomm_bug1 is
  port (target : out std_logic_vector(11 downto 0);
    ram_port1, ram_port2 : in std_logic_vector(15 downto 0));
end qualcomm_bug1;

architecture a of qualcomm_bug1 is
  begin  -- a
    target <= ram_port1 ( foo ) and ram_port2 ( foo );
end a;
