-- July 2007
-- The signal mem_data is an array.
-- It is  defined in the package declaration. The entity (not top one) uses the
-- signal in the left and right side assignment. 
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is
  subtype fixed4_type is SIGNED (3 downto 0);
  type arr_sig is array (0 to 1) of fixed4_type;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
package FFT_CORE_SIG_PKG is
  signal mem_data      : arr_sig;
end FFT_CORE_SIG_PKG;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity sub is
  port (
    out1      : out fixed4_type);
end sub;

architecture arch of sub is
begin
    mem_data(1) <= mem_data(0);           -- this is tested
    out1  <= mem_data(1);                 -- this is tested
end;




library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity arr_signals_package is
  port (
    in1       : in fixed4_type;
    out1      : out fixed4_type);
end arr_signals_package;

architecture arch_top of arr_signals_package is
  signal temp       : fixed4_type;
begin

  mem_data(0) <= in1;
  i_sub : entity sub 
     port map (
         out1   => temp);

  out1 <=  temp; 
end;
