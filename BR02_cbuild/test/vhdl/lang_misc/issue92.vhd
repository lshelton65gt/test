-- filename: test/vhdl/lang_misc/issue92.vhd
-- Description:  This test duplicates a population error seen in Customer2A design


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;


entity issue92 is
  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector(15 downto 0);
    out1     : out std_logic);
end;

architecture arch of issue92 is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
      out1 <= or_reduce(in1 and (15 downto 0 => '1'));
    end if;
  end process;
end;
