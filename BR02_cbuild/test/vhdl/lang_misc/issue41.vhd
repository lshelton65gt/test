-- Reproduces issue 41 exposed by customer 2 testcase
-- This design causes a CARBON INTERNAL ERROR during nucleus sanity checking:
-- NucleusSanity.cxx:144 NU_ASSERT(mod == mModule) failed
-- Both entity instantiations are required to reproduce the failure.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
entity child is
  generic (
      n : integer
  );
  port (
      in1 : in std_logic_vector(n-1 downto 0);
      stat : out std_logic
  );
end child;

architecture rtl of child is
begin

   stat <= or_reduce(in1);
end rtl;

-----------------------------------------

library ieee;
use ieee.std_logic_1164.all;
entity issue41 is
  generic (
      n1 : integer := 2;
      n2 : integer := 3
  );
  port (
      in1 : in std_logic_vector(n1-1 downto 0);
      in2 : in std_logic_vector(n2-1 downto 0);
      stat1 : out std_logic;
      stat2 : out std_logic
  );
end issue41;

architecture rtl of issue41 is
begin

inst1: entity work.child(rtl) generic map(n1) port map (in1, stat1);
inst2: entity work.child(rtl) generic map(n2) port map (in2, stat2);

end rtl;
