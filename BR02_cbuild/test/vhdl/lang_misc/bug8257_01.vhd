-- filename: test/vhdl/lang_misc/bug8257_01.vhd
-- Description:  This test duplicates the fold/codegen problem seen in bug 8257


-- 
-- TLV1010 Viterbi Decoder
-- Copyright: (c) 2004 - 2006 Tolv Inc / All Rights Reserved.
-- Licensed to Edgewater Computer Systems Inc.
-- 
-- Author: Tolv Inc. / www.tolv.ca
-- Release Date: August 2006
-- 

-- Description:  This package contains a number of useful types and
-- constants used throughout the design.

library ieee;
use ieee.std_logic_1164.all;

package vd_types is

-- Parameters defining the rate 1/N convolutional code.
constant N : integer := 2;
constant M : integer := 6;
--1 constant K : integer := 7;
-- Polynomials defining the code.  Note: these are only used by the encoder.
constant poly0 : std_logic_vector(M downto 0) := B"1_011_011";
constant poly1 : std_logic_vector(M downto 0) := B"1_111_001";

-- The maximum number of encoder states.
constant NUM_STATES : integer := 2**M;

-- A type to hold the binary representation of a state number.
subtype state_t is std_logic_vector(M-1 downto 0);

-- A type to hold an array of state_t.
type state_vector_t is array (natural range <>) of state_t;

-- This constant defines the bit width used for state metrics inside the 
-- decoder.
constant METRIC_BITS : integer := 6;

-- A type to hold the binary representation of a state metric.
subtype state_metric_t is std_logic_vector(METRIC_BITS-1 downto 0);

-- A type to hold an array of state_metric_t.
type state_metric_vector_t is array (natural range <>) of state_metric_t;


subtype hd_vector_t is std_logic_vector(NUM_STATES-1 downto 0);
type hd_vector_array_t is array (natural range <>) of hd_vector_t;

-- A constrained array type to hold an array of state_metric_t.
type metric_vector_t is array (0 to (2**N)-1) of state_metric_t;

-- A type to hold an array of integers.
type int_vector_t is array (natural range <>) of integer;

-- A constant to define the maximum traceback length supported by the decoder.
-- This is used to size the path memories.
constant MAX_TRACEBACK : integer := 128;

-- This is log2(MAX_TRACEBACK).
constant TB_BITS : integer := 7;

-- A constant to define the polarity of the asynchronous reset.
constant RSTVAL : std_logic := '1';

end package vd_types;






-- 
-- TLV1010 Viterbi Decoder
-- Copyright: (c) 2004 - 2006 Tolv Inc / All Rights Reserved.
-- Licensed to Edgewater Computer Systems Inc.
-- 
-- Author: Tolv Inc. / www.tolv.ca
-- Release Date: August 2006
-- 

-- Description: This is a single Add-Compare-Select unit for the Viterbi
-- decoder.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;

use work.vd_types.all;

entity vd_acsu_single is
port (
    arst : in std_logic;
    clk : in std_logic;
    init : in std_logic;
    en : in std_logic;
    normalize : in std_logic;

    mm0 : in state_metric_t;
    mm1 : in state_metric_t;
    sm0 : in state_metric_t;
    sm1 : in state_metric_t;

    hd : out std_logic;
    smout : out state_metric_t
);
end entity vd_acsu_single;

architecture rtl of vd_acsu_single is

begin

process (arst, clk)
    variable s0, s1, m0, m1 : std_logic_vector(METRIC_BITS downto 0);
    variable tm0, tm1, res : std_logic_vector(METRIC_BITS downto 0);
    variable hdres : std_logic;
begin
    if arst=RSTVAL then
        smout <= (others => '0');
        hd <= '0';

    elsif rising_edge(clk) then
        m0 := sxt(mm0, m0'length);
        m1 := sxt(mm1, m1'length);
        s0 := sxt(sm0, s0'length);
        s1 := sxt(sm1, s1'length);
        tm0 := m0 + s0;
        tm1 := m1 + s1;
        if (tm0>tm1) then
            res := tm0;
            hdres := '0';
        else
            res := tm1;
            hdres := '1';
        end if;
        if init='1' then
            smout <= (others => '0');
            hd <= '0';
        elsif en='1' then
            if normalize='1' then
                smout <= res(METRIC_BITS-1 downto 0) - 16;
            else
                smout <= res(METRIC_BITS-1 downto 0);
            end if;
            hd <= hdres;
        end if;
    end if;
end process;

end architecture rtl;


-- 
-- TLV1010 Viterbi Decoder
-- Copyright: (c) 2004 - 2006 Tolv Inc / All Rights Reserved.
-- Licensed to Edgewater Computer Systems Inc.
-- 
-- Author: Tolv Inc. / www.tolv.ca
-- Release Date: August 2006
-- 

-- Description:  This module contains the Viterbi decoder trellis.
--
-- The trellis consists of 2**M ACS units.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;

use work.vd_types.all;

entity bug8257_01 is
port (
    arst : in std_logic;
    clk : in std_logic;
    init : in std_logic;
    en : in std_logic;
    metrics : in metric_vector_t;
    hd : out hd_vector_t;
    current_states : out state_metric_vector_t(0 to NUM_STATES-1);
    states_valid : out std_logic;

    -- SOF pass-through
    sof_in : in std_logic;
    sof_out : out std_logic
);
end entity bug8257_01;

architecture rtl of bug8257_01 is

--<< Signals
signal states : state_metric_vector_t(0 to NUM_STATES-1);
signal metrics_reg : metric_vector_t;
signal en_reg : std_logic;
signal sof_reg : std_logic;
signal normalize : std_logic_vector(NUM_STATES-1 downto 0);
-->>

--<< Code tables
-- These tables are configured to hold the convolutional encoder output given
-- a 0 or 1 input bit and an original encoder state.
constant metric_idx0 : int_vector_t(0 to NUM_STATES-1) := (
    0, 1, 0, 1, 3, 2, 3, 2, 3, 2, 3, 2, 0, 1, 0, 1,
    2, 3, 2, 3, 1, 0, 1, 0, 1, 0, 1, 0, 2, 3, 2, 3,
    3, 2, 3, 2, 0, 1, 0, 1, 0, 1, 0, 1, 3, 2, 3, 2,
    1, 0, 1, 0, 2, 3, 2, 3, 2, 3, 2, 3, 1, 0, 1, 0
);

constant metric_idx1 : int_vector_t(0 to NUM_STATES-1) := (
    3, 2, 3, 2, 0, 1, 0, 1, 0, 1, 0, 1, 3, 2, 3, 2,
    1, 0, 1, 0, 2, 3, 2, 3, 2, 3, 2, 3, 1, 0, 1, 0,
    0, 1, 0, 1, 3, 2, 3, 2, 3, 2, 3, 2, 0, 1, 0, 1,
    2, 3, 2, 3, 1, 0, 1, 0, 1, 0, 1, 0, 2, 3, 2, 3
);
-->>

begin

--<< Build the trellis
-- Instantiate all ACS units for the decoder.  The connection of state metrics
-- from outputs to inputs is trivial due to the non-systematic nature of the
-- code.  The connection of branch metrics to inputs is determined by the
-- tables above.
nodes: for i in 0 to NUM_STATES-1 generate
    unit: entity work.vd_acsu_single
        port map (
            arst => arst,
            clk => clk,
            init => init,
            en => en_reg,
            normalize => normalize(i),

            mm0 => metrics_reg( metric_idx0(i) ),
            mm1 => metrics_reg( metric_idx1(i) ),
            sm0 => states((2*i) mod NUM_STATES),
            sm1 => states(((2*i) mod NUM_STATES)+1),
            hd => hd(i),
            smout => states(i)
        );
end generate nodes;
-->>

--<< Detect normalization condition
-- State metrics are normalized by subtracting a fixed amount from all state
-- metrics simultaneously.  This is done if all state metrics are greater than
-- or equal to zero (ie. non-negative).
detect_normalization: process (states)
begin 

  -- the problem in bug 8257 is the way the following loop is optimized, and in
  -- particular the fact that the if condition is via a memory, not a vector.
  
    normalize <= (others => '1');
    for i in 0 to NUM_STATES-1 loop
        -- if any is negative, then do not normalize yet
        if states(i)(states(i)'high)='1' then
            normalize <= (others => '0');
        end if;
    end loop;
end process;
-->>

current_states <= states;

process (arst, clk)
begin
    if arst=RSTVAL then
        metrics_reg <= (others => (others => '0'));
        en_reg <= '0';
        states_valid <= '0';
        sof_reg <= '0';
        sof_out <= '0';

    elsif rising_edge(clk) then
        -- register inputs for speed
        metrics_reg <= metrics;
        en_reg <= en;
        -- output valid signal
        states_valid <= en_reg;
        -- SOF pass-through
        sof_reg <= sof_in;
        sof_out <= sof_reg;
    end if;
end process;

end architecture rtl;

