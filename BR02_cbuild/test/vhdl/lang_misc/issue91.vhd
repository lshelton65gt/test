-- filename: test/vhdl/lang_misc/issue91.vhd
-- Description:  This test duplicates a sim mismatch seen in Customer 2A design
-- file. somehow related to conditional waveforms e.g.
--   lhs <= val1 when cond1 else
--          val2 when cond2
--
--


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;

library ieee;
use ieee.std_logic_1164.all;
entity issue91 is
  port (
      in256 : in std_logic_vector(255 downto 0);
      sel1 : in signed(3 downto 0);
      out32 : out std_logic_vector(31 downto 0)
  );
end issue91;

architecture rtl of issue91 is
begin

   out32 <= in256(127 downto 96) when sel1(3 downto 2) = 3 else 
            in256( 95 downto 64) when sel1(3 downto 2) = 2 else 
            in256( 63 downto 32) when sel1(3 downto 2) = 1 else 
            in256( 31 downto 0);
   
end rtl;
