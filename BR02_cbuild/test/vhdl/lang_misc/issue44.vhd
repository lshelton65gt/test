-- Reproduces issue 44 exposed by customer 2 testcase.
-- The initialization of ALL_ONES does not work correctly.
-- Only the lowest 64 bits are initialized. The upper 64
-- bits are 0.
library ieee;
use ieee.std_logic_1164.all;
package constants is
   constant ALL_ONES   : std_logic_vector(127 downto 0) := (others => '1');
end package;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use work.constants.all;
entity child is
  port (
      sel : in std_logic;
      in1 : in std_logic_vector(7 downto 0);
      out1 : out std_logic_vector(7 downto 0)
  );
end child;

architecture rtl of child is
begin

   out1 <= in1 when sel = '0' else ALL_ONES(127 downto 120);

end rtl;

-----------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.constants.all;
entity issue44 is
  port (
      in1 : in std_logic_vector(7 downto 0);
      sel1 : in std_logic;
      out1 : out std_logic_vector(7 downto 0)
  );
end issue44;

architecture rtl of issue44 is
begin

inst1: entity work.child(rtl) port map (sel=>sel1, in1=>in1, out1=>out1);
end rtl;
