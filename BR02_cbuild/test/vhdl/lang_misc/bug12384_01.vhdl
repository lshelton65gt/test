-- June 2009
-- The issue was with constant NDX. CBUILD was failing to populate
-- correctly that net, because it's declared as constant.
library ieee;
use ieee.std_logic_1164.all;


entity bug12384_01 is
  port (
    clk  : in  std_logic;
    in1  : in  std_logic_vector(9 downto 0);
    rot  : in  integer range 0 to 12;
    out1 : out std_logic_vector(9 downto 0);
    out2 : out std_logic_vector(9 downto 0));
end bug12384_01;


architecture arch1 of bug12384_01 is
  FUNCTION ROTR       (L: STD_LOGIC_VECTOR; R: INTEGER)       RETURN STD_LOGIC_VECTOR IS
    CONSTANT MX:    integer := L'length-1;          
    VARIABLE RSLT:  STD_LOGIC_VECTOR(MX DOWNTO 0);
    VARIABLE TEMP:  STD_LOGIC_VECTOR(MX DOWNTO 0);
    CONSTANT NDX:   integer := R mod L'length;      
  BEGIN
    TEMP := L;                                     
    RSLT (MX-NDX DOWNTO 0) := TEMP (MX DOWNTO NDX);
    IF (NDX /= 0) THEN
      RSLT(MX DOWNTO MX-NDX+1) := TEMP (NDX-1 DOWNTO 0);
    END IF;
    RETURN RSLT;
  END;

  signal c1 : integer range 0 to 12 := 9;

begin 

    
  out1 <=  ROTR(in1, rot); 
  out2 <=  ROTR(in1, c1); 

end arch1;

