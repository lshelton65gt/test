
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity foo is
      generic( w : integer := 2);
      port(
           a : in  std_logic_vector(w downto 0); -- Input a
           b : in  std_logic_vector(w downto 0); -- Input a
           c : out std_logic_vector(w downto 0)  -- Output c
          );
end ;

architecture behav of foo is
begin
c <= a + b;
end behav;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity bug2140_2 is
      generic( g : integer );
      port(
           a : in  std_logic_vector(1 downto 0); -- Input a
           b : in  std_logic_vector(1 downto 0); -- Input a
           c : out std_logic_vector(1 downto 0)  -- Output c
          );
end bug2140_2;
-----------------------------------------------------------------
architecture behav of bug2140_2 is
component foo 
      generic( w : integer);
      port(
           a : in  std_logic_vector(w downto 0); -- Input a
           b : in  std_logic_vector(w downto 0); -- Input a
           c : out std_logic_vector(w downto 0)  -- Output c
          );
end component;
begin
  inst : foo generic map(w=>g)
             port map(a,b,c);
end behav;

