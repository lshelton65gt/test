-- July 2008
-- This file tests negative index range for for loop.
-- The only difference between this file and bug9124_01.vhdl is
-- that the start of the loop is changed to -1-H_VAL.

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;

entity bug9124_02 is
  
  generic (
    PEL_W : natural := 1;
    H_VAL : natural := 0);

  port (
    clk, rst : in    std_logic;
    in1      : in    std_logic_vector(13*PEL_W-1 downto 0);
    out1     : out   std_logic_vector(13*PEL_W-1 downto 0));

end bug9124_02;

architecture arch of bug9124_02 is
  signal temp  : std_logic_vector(13*PEL_W-1 downto 0);
begin  -- arch


  p1: process (clk, rst)
  begin  -- process p1
    if rst = '0' then 
      temp <= (others => '0');
    elsif clk'event and clk = '1' then 
--      for x in -H_VAL-1 to 9+H_VAL loop
      for x in -1-H_VAL to 9+H_VAL loop -- this line is tested
        temp((11-x)*PEL_W-1 downto (10-x)*PEL_W) <= in1((11-x)*PEL_W-1 downto (10-x)*PEL_W);
      end loop;
      
    end if;
  end process p1;

  out1 <= temp;

end arch;
