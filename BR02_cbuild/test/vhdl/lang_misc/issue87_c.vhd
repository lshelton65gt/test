-- filename: test/vhdl/lang_misc/issue87_c.vhd


library ieee;
use ieee.std_logic_1164.all;

entity issue87_c is
  port (
    clock    : in  std_logic;
    out1     : out std_logic_vector(3 downto 0));
end;

architecture arch of issue87_c is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1 <= (others => '1');
    end if;
  end process;
end;
