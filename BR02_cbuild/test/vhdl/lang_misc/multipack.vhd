-- This file tests for proper resolution of members of identically-named
-- packages residing in different libraries.
library ieee;
use ieee.std_logic_1164.all;
library stdstd;
use stdstd.pack.all;
library lib2;
use lib2.pack.all;
use work.pack.all;

entity multipack is
  port (
    clk, rst1, rst2, rst3 : in  std_logic;
    din : in std_logic_vector(1 downto 0);
    dout : out std_logic_vector(1 downto 0));
end multipack;

architecture arch of multipack is
begin

p1: process (clk, rst1, rst2, rst3)
  begin
    if rst1 = '1' then
      dout <= "00";
    elsif rst2 = '1' then
      dout <= "01";
    elsif rst3 = '1' then
      dout <= "10";
    elsif clk'event and clk = '1' then
      dout <= din;
    end if;
  end process p1;  

end arch;
