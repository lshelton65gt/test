library ieee;
use ieee.std_logic_1164.all;

entity generate1 is
  port (
    in1  : in  std_logic_vector(2 downto 0);
    out1 : out std_logic_vector(2 downto 0));
end generate1;

architecture arch of generate1 is

begin

  nullrange: for i in 0 to -1 generate 
    out1(i) <= in1(i);
  end generate nullrange;

  out1(2 downto 1) <= in1(2 downto 1);

end arch;
