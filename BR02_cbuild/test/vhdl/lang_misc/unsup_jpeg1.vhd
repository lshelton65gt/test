package p is
  constant N_FILTER_BITS : integer := 15;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity unsup_jpeg1 is
  port (input : in std_logic_vector(N_FILTER_BITS-1 downto 0);
        dct_trmem_din : out std_logic_vector(N_FILTER_BITS-1 downto 0));
end ;

architecture arch of unsup_jpeg1 is
   signal dct : std_logic_vector(13+N_FILTER_BITS downto 0);
   signal s : std_logic;
begin

    s <= dct(N_FILTER_BITS-1);          -- OK
    dct_trmem_din <= dct(14 downto 0);  --OK
    dct_trmem_din <= dct(15-1 downto 0);  --Error 3500: Unsupported data type
    dct_trmem_din <= dct(N_FILTER_BITS-1 downto 0); --Error 3500: Unsupported data type
    dct_trmem_din <= input;
end ;
