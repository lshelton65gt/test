-- filename: test/vhdl/lang_misc/issue51.vhd
-- Description:  This test duplicates the problem seen in Customer2 issue51
-- the issue was that the population of case statements was not making copies
-- of the select expression properly.

library ieee;
use ieee.std_logic_1164.all;

entity issue51 is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector((WIDTH-1) downto 0);
    sel      : in integer range 0 to 255;
    out1     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of issue51 is 
begin
  main: process (clock)
  variable sel_limited : integer range 0 to 255;
  begin
    if clock'event and clock = '1' then 
     if (0 < sel) or (sel < 255) then
       sel_limited := sel;
     else
       sel_limited := 0;
     end if;
      case sel is
        when 0 to 31 => out1 <= in1 and in2;
        when 32 to 128 => out1 <= in1 or in2;
        when 129 | 130 | 131  => out1 <= in1;
        when 132 to 135 | 140 | 142 to 200  => out1 <= in2;
        when others => out1 <= "0000";
      end case;
    end if;
  end process;
end;
