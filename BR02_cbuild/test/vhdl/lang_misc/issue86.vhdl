-- issue86: incorrect handling of sign extension of first operand to conv_unsigned.
-- The first argument should be truncated, not sign extended.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
entity issue86 is

  port (
   i1 : in std_logic_vector(6 downto 0);
   o1 : out std_logic;
   o2 : out std_logic
    );

end issue86;

architecture rtl of issue86 is

   constant BL    : natural := 4;
   constant MO : natural := 96;

begin  -- rtl

   o1 <= '1' when unsigned(i1) <= conv_unsigned(MO - BL, i1'length) else '0';
   o2 <= '1' when unsigned(i1) <= conv_unsigned(BL - MO, i1'length) else '0';

end rtl;
