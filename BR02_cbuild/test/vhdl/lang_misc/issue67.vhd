-- filename: test/vhdl/lang_misc/issue67.vhd
-- Description:  This test duplicates the issue 67 seen in a test derived from issue65
-- with r2411 gets CARBON INTERNAL ERROR , see issue65.vhd for a similar test
-- that does not have this CARBON INTERNAL ERROR 

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

package issue67_pkg is
  function function1(constant arg: std_logic_vector; constant size: natural) return std_logic_vector;
  function function2(input_vector : std_logic_vector) return std_logic;
end;

package body issue67_pkg is

  function function1(constant arg : std_logic_vector;
                     constant size: natural) return std_logic_vector is
  begin
    return std_logic_vector(ieee.numeric_std.unsigned(arg));
  end function1; 

  function function2(input_vector : std_logic_vector) return std_logic is
    variable v_in_vec_log2 : std_logic_vector(3 downto 0) := function1(input_vector, 4);
  begin
    return (v_in_vec_log2(1));
  end function2;
end;

library ieee;
use ieee.std_logic_1164.all;
use work.issue67_pkg.all;

entity issue67 is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector((WIDTH-1) downto 0);
    out1     : out std_logic_vector(0 downto 0));
end;

architecture arch of issue67 is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1(0) <= function2(in1);
    end if;
  end process;
end;
