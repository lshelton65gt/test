-- July 2008
-- This file tests negative index range for for loop.
-- This file compiles fine. But the bug9124_02 fails.

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity bug9124_01 is
  
  generic (
    PEL_W : natural := 1;
    H_VAL : natural := 0);

  port (
    clk, rst : in    std_logic;
    in1      : in    std_logic_vector(13*PEL_W-1 downto 0);
    out1     : out   std_logic_vector(13*PEL_W-1 downto 0));

end bug9124_01;

architecture arch of bug9124_01 is
  signal temp  : std_logic_vector(13*PEL_W-1 downto 0);
begin  -- arch


  p1: process (clk, rst)
  begin  -- process p1
    if rst = '0' then 
      temp <= (others => '0');
    elsif clk'event and clk = '1' then 
      for x in -H_VAL-1 to 9+H_VAL loop  -- This line is tested
        temp((11-x)*PEL_W-1 downto (10-x)*PEL_W) <= in1((11-x)*PEL_W-1 downto (10-x)*PEL_W);
      end loop;
      
    end if;
  end process p1;

  out1 <= temp;

end arch;
