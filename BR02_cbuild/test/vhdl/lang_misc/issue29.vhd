-- Reproducer for customer 2 issue29
-- (others=>'0') on the RHS of a selected
-- signal assignment.
entity issue29 is
  port (
    op : in bit_vector(1 downto 0);
    a  : in bit_vector(7 downto 0);
    b  : in bit_vector(7 downto 0);
    result  : out bit_vector(7  downto 0)
  );
end entity issue29;

architecture rtl of issue29 is

begin

   with op select
      result <= (others => '0') when "00",
                a and b         when "01",
                a or  b         when "10",
                (others => '1') when "11";

end rtl;

   
