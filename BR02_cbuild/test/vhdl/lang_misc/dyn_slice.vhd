library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity dyn_slice is  
    port (
         a : in std_logic_vector(63 downto 0);
         b : in std_logic_vector(63 downto 0);
         idx : in unsigned(5 downto 0);
         dout : out std_logic_vector(63 downto 0)
         );
end dyn_slice;

architecture dyn_slice of dyn_slice is
 begin

   process (a, b, idx)
     variable idx_int : integer;
      begin
        idx_int := to_integer(idx);
        dout <= a(63 downto idx_int) &
                b(idx_int - 1 downto 0);
      end process;
end dyn_slice;
