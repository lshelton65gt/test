-- This test used to cause the following error from the Verific flow:
-- Alert 61275: type of aggregate cannot be determined without context ; 0 visible types match here
-- This error renders subsequent elaboration calls to the Verific API to fail.
library ieee;
use ieee.std_logic_1164.all;
entity tickrange is
port( a : in std_logic_vector(15 downto 0);
      b : in std_logic_vector(15 downto 0);
      c : out std_logic_vector(31 downto 0)
     );
end tickrange;

architecture rtl of tickrange is
begin

process(a,b) is
begin
  if (a = (a'range => '1')) then
     c <= a & b;
  else
     c <= b & a;
  end if;     
end process;

end architecture;

