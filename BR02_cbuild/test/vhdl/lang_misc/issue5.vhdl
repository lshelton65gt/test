-- filename: test/vhdl/lang_misc/issue5.vhdl
-- Description:  This test duplicates Issue 5 (as seen the second time it appeared)
-- the problem is the use of the std_match function that uses an array initialized with TRUE/FALSE values

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity issue5 is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector((WIDTH-1) downto 0);
    out1     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of issue5 is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
     if std_match(in1, in2) then
      out1 <= in1 and in2;
     end if;
    end if;
  end process;
end;
