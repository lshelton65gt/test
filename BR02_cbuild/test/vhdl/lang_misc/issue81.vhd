-- filename: test/vhdl/lang_misc/issue81.vhd
-- Description:  This test duplicates problem seen in Cust2A, where read from
-- an array of records is not working.  In the Customer design the array is not
-- a constant. The fact that it is a constant created an additional problem.

library ieee;
use ieee.std_logic_1164.all;

entity issue81 is

  port (
    clock    : in  std_logic;
    out0, out1,out2 : out std_logic_vector(39 downto 12);
    out4     : out std_logic
    );
end;

architecture arch of issue81 is 

  type record1_element_type is record
                                 field1 : std_logic;
                                 field2  : std_logic_vector(39 downto 12);
                                 field3 : std_logic;
                               end record;
  
  type record1_type is array (0 to 1) of record1_element_type;
  signal arrayOfRecord1 : record1_type := (
    ('0', "1000000000000110000000000000", '1'),
    ('0', "1111111111111001111111111111", '0')
    );

  signal record1 : record1_element_type := ('0', "1000000000000110000000000000", '1');


begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 

      out0 <= arrayOfRecord1(0).field2(39 downto 12);
      out1 <= arrayOfRecord1(1).field2(39 downto 12);
      out2 <= record1.field2(39 downto 12);
      out4 <= arrayOfRecord1(0).field3;

    end if;
  end process;
end;
