-- Customer 2 Issue 19 on wiki. Slicing the return value of a typeconversion function that has
-- an expression as an argument has not worked correctly in the Verific flow
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
entity issue19 is
port (
        clk : in std_logic;
        din_lo : in std_logic_vector(15 downto 0);
        din_hi : in std_logic_vector(15 downto 0);
        dout_lo: out bit_vector(15 downto 0);
        dout_hi: out bit_vector(15 downto 0)
);
end issue19;

architecture rtl of issue19 is
begin
   process (clk)
   begin
      if clk'event and clk = '1' then
          dout_lo <= to_bitvector(din_hi & din_lo)(15 downto 0);
          dout_hi <= to_bitvector(din_hi & din_lo)(31 downto 16);
      end if;
   end process;
end architecture rtl;
