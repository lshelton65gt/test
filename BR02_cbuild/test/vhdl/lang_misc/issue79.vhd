library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
package r_pack is
   constant SIZE_N         : integer := 12;
   type a44_t is array (3 downto 0) of std_logic_vector(3 downto 0);
   type a431_t is array(3 downto 0) of std_logic_vector(30 downto 0);
   function c_std_match(input : std_logic_vector; test : std_logic_vector) return boolean;
   function calc(cur : std_logic_vector(30 downto 0)) return std_logic_vector;

end r_pack;

package body r_pack is

   function c_std_match(input : std_logic_vector; test : std_logic_vector) return boolean is
   begin
      return std_match(input, test);
   end;
   function calc(cur : std_logic_vector(30 downto 0)) return std_logic_vector is
      type p_t is array (0 to 5) of std_logic_vector(14 downto 0);
      variable test : p_t;
      variable tru  : std_logic_vector(14 downto 0);
      variable can  : std_logic_vector(3 downto 0);
   begin
      test(0) := "00000" & "----" & "---" & "--" & "-";
      test(1) := "1----" & "0000" & "---" & "--" & "-";
      test(2) := "-1---" & "1---" & "111" & "--" & "-";
      test(3) := "--1--" & "-1--" & "0--" & "11" & "-";
      test(4) := "---1-" & "--1-" & "-0-" & "0-" & "1";
      if cur(30) = '0' then
         tru := cur(14 downto 0);
         can := "0101";
         for w in 0 to 4 loop
            if c_std_match(tru, test(w)) then
               can := conv_std_logic_vector(w, 4);
            end if;
         end loop;
      else
         tru := cur(29 downto 15);
         can := "1011";
         for w in 0 to 4 loop
            if c_std_match(tru, test(w)) then
               can := conv_std_logic_vector(6+w, 4);
            end if;
         end loop;
      end if;
      return can;
   end function;
end r_pack;


library work;
use work.r_pack.all;
entity r_ent is
  port(
   o       : out a44_t;
   in1    : in a431_t
    );
end r_ent;

architecture rtl of r_ent is

begin
   g : for r in 0 to 3 generate
      o(r) <= calc(in1(r));
   end generate;
end rtl;
