use work.all;

entity bottom2 is
   port (verilogin2  :  in bit;
         verilogout2 : out bit);
end bottom2 ;

architecture arch_bottom2 of bottom2 is
begin
    verilogout2 <= verilogin2;
end arch_bottom2;

entity bottom3 is
   port (verilogin3  :  in bit;
         verilogout3 : out bit);
end bottom3 ;

architecture arch_bottom3 of bottom3 is
begin
    verilogout3 <= verilogin3;
end arch_bottom3;

entity bottom4 is
   port (verilogin4  :  in bit;
         verilogout4 : out bit);
end bottom4 ;

architecture arch_bottom4 of bottom4 is
begin
    verilogout4 <= verilogin4;
end arch_bottom4;


entity forGen03_top is
   port (vhdlin  :  in bit_vector(0 to 2);
         vhdlout : out bit_vector(0 to 2));
end forGen03_top ;

architecture arch_top of forGen03_top is

   Component bottom2
      port (verilogin2  :  in bit;
            verilogout2 : out bit);
   end Component;
   Component bottom3
      port (verilogin3  :  in bit;
            verilogout3 : out bit);
   end Component;
   Component bottom4
      port (verilogin4  :  in bit;
            verilogout4 : out bit);
   end Component;

   constant  loopVar1 : integer := 0;
   constant  loopVar2 : integer := 1;
   constant  loopVar3 : integer := 2;
begin

       Label2 : if loopVar1 = 0 generate
       begin
           inst : bottom2
              port map(verilogin2 => vhdlin(loopVar1), verilogout2 => vhdlout(loopVar1));
       end generate Label2;
       Label3 : if loopVar2 = 1 generate
       begin
           inst : bottom3
              port map(verilogin3 => vhdlin(loopVar2), verilogout3 => vhdlout(loopVar2));
       end generate Label3;
       Label4 : if loopVar3 = 2 generate
       begin
           inst : bottom4
              port map(verilogin4 => vhdlin(loopVar3), verilogout4 => vhdlout(loopVar3));
       end generate Label4;

end arch_top;

