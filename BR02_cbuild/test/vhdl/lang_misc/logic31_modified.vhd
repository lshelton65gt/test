-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- Back to back latch configuration.
-- This testcase was modified to remove an unstable cycle

LIBRARY IEEE ;
USE IEEE.std_logic_1164.ALL ;
USE IEEE.std_logic_unsigned.ALL ;

entity logic31_modified is
	port( 
		  in1 : in std_logic;
		  in2 : in std_logic;
		  in3 : in std_logic;
		  out1 : out std_logic
	    );
end logic31_modified;

architecture arch_logic31_modified of logic31_modified is
	signal temp1 : std_logic := '0';
	signal temp2 : std_logic := '0';
begin

	process(in1, in2, in3, temp1, temp2)
	begin
		if (in1 = '1') then
			temp1 <= in3 and temp2;
		end if;
		if (in2 = '1') then
			temp2 <= in3 or temp1;
		end if;
		out1 <= temp2;
	end process;
end arch_logic31_modified;
