-- Reproduce an issue found in customer 2 test 2b.
-- Issue 66
--
-- VerificVhdlIdRef.cpp:394 INFO_ASSERT(false) failed

library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity issue66 is
port (
   clk : in std_logic;
   a1 : in std_logic_vector(2 downto 0);
   out1 : out std_logic
);
end issue66;

architecture rtl of issue66 is

   constant CONST1 : std_logic_vector(2 downto 0) := "010";

   procedure comp (in0          : in  std_logic_vector;
                          in1          : in  std_logic_vector;
                          result       : out std_logic) is
      constant CONST2 : integer := conv_integer (unsigned (in0));
      variable in1_val : integer;
   begin
      in1_val := conv_integer(unsigned(in1));
      if (in1_val > CONST2) then
         result := '1';
      else
         result := '0';
      end if;
   end procedure comp;

begin


   process (clk) is
     variable temp : std_logic;
   begin
     if (clk'event and clk = '1') then
        comp(CONST1, a1, temp);
        out1 <= temp;
     end if;    
   end process;

end rtl;
