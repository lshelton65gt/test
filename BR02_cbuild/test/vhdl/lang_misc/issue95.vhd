-- filename: test/vhdl/lang_misc/issue95.vhd
-- Description:  This test demonstrates a problem seen in Imagination Rogue test
-- The first argument to asll was incorrectly sized and would not be fully
-- extended by resize function.  However the assignment to out2 was correct so
-- somehow the problem is related to the function call to asll


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all; -- for to_integer()

entity issue95 is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    size      : in  std_logic_vector(7 downto 0);
    out1     : out std_logic_vector(127 downto 0)
    );
end;

architecture arch of issue95 is

   function asll(a : signed; s : unsigned) return std_logic_vector is
   begin
      return(std_logic_vector((unsigned(a) sll to_integer(s))));
   end function asll;

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1 <= not asll( resize(signed'("1"),128),     unsigned(size));
    end if;
  end process;
end;
