-- May 2007
-- This testcase incorrectly reports that a net is undriven:
--  bug7352.vhdl:15 bug7352.i_mux4_1_pass_b2.i1: Warning 4020: Net is undriven.
-- The problem is related to the assignment of a generic (ZEROED_SLICE) to
-- port I1.

library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_std.all;
use IEEE.std_logic_arith.all;


-------------------------------------------------------------------------------  
Entity mux4_1_x_n is                    -- carbon disallowFlattening
   generic (
      C_WIDTH : Integer := 8
      ); 
   port ( 
       Sel    : In  std_logic_vector(0 to 1);
       I0     : In  std_logic_vector(0 to C_WIDTH-1);
       I1     : In  std_logic_vector(0 to C_WIDTH-1);
       I2     : In  std_logic_vector(0 to C_WIDTH-1);
       I3     : In  std_logic_vector(0 to C_WIDTH-1);
       Y      : Out std_logic_vector(0 to C_WIDTH-1)
      );
end entity mux4_1_x_n; --  

Architecture implementation of mux4_1_x_n is
    -- declarations

begin
    
   -------------------------------------------------------------
   -- Combinational Process
   --
   -- Label: SELECT4_1
   --
   -- Process Description:
   --   This process implements an 4 to 1 mux.
   --
   -------------------------------------------------------------
   SELECT4_1 : process (Sel, I0, I1, I2, I3)
      begin
   
         case Sel is

           when "00" =>
               Y <= I0;
               
           when "01" =>
               Y <= I1;
               
           when "10" =>
               Y <= I2;
               
           when "11" =>
               Y <= I3;
               
           when others =>
               Y <= I0;
               
         end case;
         
      end process SELECT4_1; 

end implementation; -- mux4_1_x_n


library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_std.all;
use IEEE.std_logic_arith.all;


entity bug7352 is
   generic (
       BYTE_WIDTH   : integer := 8;
 -- This fails to compile, we  should review it later     
--       SLICE_WIDTH  : integer := BYTE_WIDTH+1; 
--       ZEROED_SLICE : std_logic_vector(0 to SLICE_WIDTH-1) := (others => '0');
       SLICE_WIDTH  : integer := 9;
       ZEROED_SLICE : std_logic_vector(0 to 8) := (others => '0')
   );
   port ( 
       sel    : In  std_logic_vector(0 to 1);
       in1    : In  std_logic_vector(0 to SLICE_WIDTH-1);
       out1   : Out std_logic_vector(0 to SLICE_WIDTH-1)
      );
end bug7352;


architecture  dre_arch of bug7352 is
begin
  I_MUX4_1_PASS_B2 : entity work.mux4_1_x_n
          generic map(
             C_WIDTH =>  SLICE_WIDTH  --  : Integer := 8
             )
          port map(
              Sel    =>  sel,                   --  : In  std_logic_vector(0 to 1);
              I0     =>  in1,                   --  : In  std_logic_vector(0 to C_WIDTH-1);
              I1     =>  ZEROED_SLICE,          --  : In  std_logic_vector(0 to C_WIDTH-1);
              I2     =>  in1,                   --  : In  std_logic_vector(0 to C_WIDTH-1);
              I3     =>  in1,                   --  : In  std_logic_vector(0 to C_WIDTH-1);
              Y      =>  out1                   --  : Out std_logic_vector(0 to C_WIDTH-1)
             );

end dre_arch;
