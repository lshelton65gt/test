-- May 2007
-- Tests the attributes value and image both for static and dynamic arguments.
-- The static ones pass, the dynamic ones fail.


entity attr4 is
port (in1 : in integer;
      in2 : in string(1 to 4);
      out1 : out integer;
      out2 : out integer
     );
end attr4;

architecture arch_attr4 of attr4 is
  signal temp : integer := 100;
  signal strVar1 : string(1 to 4);
  signal strVar2 : string(1 to 11);
begin

  out1 <= integer'value("2000");
  out2 <= integer'value(in2);           -- fails
  
  strVar1 <= integer'image(2000);
  strVar2 <= integer'image(in1);        -- fails
  
end arch_attr4;
