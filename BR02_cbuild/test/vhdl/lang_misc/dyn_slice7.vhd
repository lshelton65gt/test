-- this testcase demonstrates a problem related to the sizing of a
-- NUVarselLvalue seen with -newSizing
-- this testcase was inspired by the test cust/matrox/phoenix/carbon/cwtb/mctl 


library IEEE;
   use IEEE.std_logic_1164.all;
   use IEEE.std_logic_arith.all;
library WORK;

package slice7pak is

      constant MCDATASZ     : integer := 256;
      constant MCBENSZ      : integer := MCDATASZ/8;
      constant MCTIDSZ      : integer := 5;
      constant MCRIDSZ      : integer := 10;
      constant MCCOMPINFOSZ : integer := 2;
   -----------------------------------------------------------------------
   -- Pipe Constants
   -----------------------------------------------------------------------
   constant BITWIDTH            : integer := 8;                               -- NUMBITS
   constant PIPEWIDTH           : integer := 8;                               -- NUMBYTES
   constant PIPEWIDTHDECOMP     : integer := 8;
   constant NUMPIPES            : integer := 4;                               -- NUMPIPES
   constant TOTALBITS           : integer := NUMPIPES * PIPEWIDTH * BITWIDTH; -- TOTALBITS
   constant TOTALBITSDECOMP     : integer := NUMPIPES * PIPEWIDTHDECOMP * BITWIDTH; -- TOTALBITS




   type bytebank is array (PIPEWIDTH-1 downto 0) of std_logic_vector(BITWIDTH-1 downto 0);
   type bytebankdecomp is array (PIPEWIDTHDECOMP-1 downto 0) of std_logic_vector(BITWIDTH-1 downto 0);
   type databank is array (NUMPIPES-1 downto 0) of bytebank;
   type databankdecomp is array (NUMPIPES-1 downto 0) of bytebankdecomp;

   type TXINFO is record
      tid      : std_logic_vector(MCTIDSZ-1 downto 0);
      compinfo : std_logic_vector(MCCOMPINFOSZ-1 downto 0);
      rid      : std_logic_vector(MCRIDSZ-1 downto 0);
      burstid  : std_logic_vector(1 downto 0);
   end record;


end slice7pak;

package body slice7pak is

end slice7pak;




library IEEE;
   use IEEE.std_logic_1164.all;
   use IEEE.std_logic_arith.all;
   use IEEE.std_logic_unsigned.all;
library WORK;
   use work.slice7pak.all;

entity dyn_slice7 is  
    port (
         a          : in std_logic_vector(8-1 downto 0);
         b          : in std_logic_vector(0 downto 0);
         mfeclkbuf  : in     std_logic;
         inhold     : in     std_logic;
         outdata    : buffer std_logic_vector(256-1 downto 0) := (others => '0')
         );
end dyn_slice7;



architecture dyn_slice7 of dyn_slice7 is 
signal rowdata           : databankdecomp := (others=>(others=>(others=>'0')));
signal outdataserial     : std_logic_vector(256-1 downto 0);
signal writereg3         : std_logic;

begin


   LOADROW : process (mfeclkbuf)
   begin 
     if (mfeclkbuf'event and mfeclkbuf = '1') then 
      writereg3 <= b(0);
      
       for pipenum in 0 to NUMPIPES-1 loop
         for bytenum in 0 to PIPEWIDTHDECOMP-1 loop
            rowdata(pipenum)(bytenum) <= a;
         end loop;
      end loop;

     end if;
   end process;
  
   XMAPDATAOUT : process(rowdata)
   variable lowerrange : integer range 0 to TOTALBITSDECOMP-1;
   variable upperrange : integer range 0 to TOTALBITSDECOMP-1;
   begin
      for pipenum in 0 to NUMPIPES-1 loop
         for bytenum in 0 to PIPEWIDTHDECOMP-1 loop
            lowerrange := bytenum*32+pipenum*BITWIDTH;
            upperrange := bytenum*32+(pipenum+1)*BITWIDTH-1;
            outdataserial(upperrange downto lowerrange) <= rowdata(pipenum)(bytenum);
         end loop;
      end loop;
   end process;

   XREGISTEROUT : process(mfeclkbuf)
   begin
      if (mfeclkbuf'event and mfeclkbuf = '1') then
         if (inhold = '0') then
            if (writereg3 = '1') then
               outdata <= outdataserial;
            end if;
         end if;
      end if;
   end process;   

   
end dyn_slice7;
