-- May 2007
-- Test the attributes val, succ, pred, leftof and rightof
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity attr3 is
port (clk : in std_logic;
      in1 : in integer range 1 to 2;
      out1 : out integer;
      out2 : out integer;
      out3 : out integer;
      out4 : out integer
     );
end attr3;

architecture arch_attr3 of attr3 is
  constant intR : integer := 1;
  constant intY : integer := 2;
  constant intB : integer := 4;
  constant intG : integer := 8;
  constant intE : integer := 256;
  type color_typ is ('R', 'Y', 'B', 'G' );
  signal temp  : color_typ := 'Y';
  signal temp1 : color_typ;
  signal temp2 : color_typ;
  signal temp3 : color_typ;
  signal temp4 : color_typ;
  
begin

  process (clk, in1)
  begin

   if (clk'event and clk = '1') then
     temp <= color_typ'val(in1);       
     temp1 <= color_typ'succ(temp);     
     if(temp1 = 'Y') then
       out1 <= intY;
     elsif (temp1 = 'B') then
       out1 <= intB;
     elsif(temp1 = 'G') then
       out1 <= intG;
     else
       out1 <= intE;
     end if;
       
     temp2 <= color_typ'pred(temp);    
     if(temp2 = 'R') then
       out2 <= intR;
     elsif (temp1 = 'Y') then
       out2 <= intY;
     elsif(temp1 = 'B') then
       out2 <= intB;
     else
       out2 <= intE;
     end if;

     temp3 <= color_typ'leftof(temp);     
     if(temp3 = 'R') then
       out3 <= intR;
     elsif (temp1 = 'Y') then
       out3 <= intY;
     elsif(temp1 = 'B') then
       out3 <= intB;
     else
       out3 <= intE;
     end if;

     temp4 <= color_typ'rightof(temp);   
     if(temp4 = 'R') then
       out4 <= intR;
     elsif (temp1 = 'Y') then
       out4 <= intY;
     elsif(temp1 = 'B') then
       out4 <= intB;
     else
       out4 <= intE;
     end if;
     
   end if;
   
  end process;
end arch_attr3;

         
