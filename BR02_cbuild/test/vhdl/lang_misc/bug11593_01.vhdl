--  October, 2008
-- This file tests the dynamic succ,pred, leftof and rightof attributes for scalars.
library ieee;
use ieee.std_logic_1164.all;

package pkg_attr is
  type t_colors is ('r','g','b','y','o','v');
  subtype t_my_char   is character range 'A' to 'z';
  subtype t_my_digits is integer range -20 to 30;
  function f_succ(pos : integer) return character;
  function f_pred(pos : integer) return character;
  function f_leftof(pos : integer) return character;
  function f_rightof(pos : integer) return character;
  function f_succ(pos : integer) return t_my_digits;
  function f_pred(pos : integer) return t_my_digits;
  function f_succ(pos : integer) return t_colors;
  function f_pred(pos : integer) return t_colors;

end pkg_attr;

package body pkg_attr is

  function f_succ(pos : integer) return character is
    variable temp1 : character;
    variable temp2 : character;
  begin
    temp1 := character'val(pos);
    temp2 := character'succ(temp1);
    return temp2;
  end f_succ;

  function f_succ(pos : integer) return t_my_digits is
    variable temp1 : t_my_digits;
    variable temp2 : t_my_digits;
  begin
    temp1 := t_my_digits'val(pos);
    temp2 := t_my_digits'succ(temp1);
    return temp2;
  end f_succ;

  function f_succ(pos : integer) return t_colors is
    variable temp1 : t_colors;
    variable temp2 : t_colors;
  begin
    temp1 := t_colors'val(pos);
    temp2 := t_colors'succ(temp1);
    return temp2;
  end f_succ;

  function f_pred(pos : integer) return character is
    variable temp1 : character;
    variable temp2 : character;
  begin
    temp1 := character'val(pos);
    temp2 := character'pred(temp1);
    return temp2;
  end f_pred;

  function f_pred(pos : integer) return t_my_digits is
    variable temp1 : t_my_digits;
    variable temp2 : t_my_digits;
  begin
    temp1 := t_my_digits'val(pos);
    temp2 := t_my_digits'pred(temp1);
    return temp2;
  end f_pred;

  function f_pred(pos : integer) return t_colors is
    variable temp1 : t_colors;
    variable temp2 : t_colors;
  begin
    temp1 := t_colors'val(pos);
    temp2 := t_colors'pred(temp1);
    return temp2;
  end f_pred;

  function f_leftof(pos : integer) return character is
    variable temp1 : character;
    variable temp2 : character;
  begin
    temp1 := character'val(pos);
    temp2 := character'leftof(temp1);
    return temp2;
  end f_leftof;
  
  function f_rightof(pos : integer) return character is
    variable temp1 : character;
    variable temp2 : character;
  begin
    temp1 := character'val(pos);
    temp2 := character'rightof(temp1);
    return temp2;
  end f_rightof;
end pkg_attr;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.pkg_attr.all;

entity bug11593_01 is
  
  port (
    rst  : in  std_logic;
    clk  : in  std_logic;
    sel1 : in  integer range 66 to 120;
    sel2 : in  integer range -19 to 29;
    sel3 : in  integer range 1 to 4;
    out1 : out character;
    out2 : out character;
    out3 : out t_my_digits;
    out4 : out t_my_digits;
    out5 : out integer range 0 to 5;
    out6 : out integer range 0 to 5;
    out7 : out character;
    out8 : out character
    );
end bug11593_01;

architecture bug11593_01_arch of bug11593_01 is
begin 


  P1: process (rst, clk)
    variable temp5 : t_colors;
    variable temp6 : t_colors;
  begin  
    if rst = '0' then
      out1 <= 'A';
      out2 <= 'A';
      out3 <= 0;
      out4 <= 0;
      out5 <= 0;
      out6 <= 0;
    elsif clk'event and clk = '1' then  
      out1 <= f_succ(sel1);
      out2 <= f_pred(sel1);
      out3 <= f_succ(sel2);
      out4 <= f_pred(sel2);
      temp5 := f_succ(sel3);
      temp6 := f_pred(sel3);
      out5 <= t_colors'pos(temp5);
      out6 <= t_colors'pos(temp6);
      out7 <= f_leftof(sel1);
      out8 <= f_rightof(sel1);
    end if;
  end process P1;

end bug11593_01_arch;

