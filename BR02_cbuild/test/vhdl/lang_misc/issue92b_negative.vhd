-- filename: test/vhdl/lang_misc/issue92b_negative.vhd
-- Description:  This test shows some usages of others that should not be supported


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;


entity issue92b_negative is
  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector(31 downto 16);
    in2      : in  std_logic_vector(3 downto 0);
    out0, out1,out2,out3,out4 : out std_logic_vector(31 downto 16);
    out5     : out std_logic_vector(15 downto 0)
    );
end;

architecture arch of issue92b_negative is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
      -- each of the following assigments should not be supported and should genearate an error mesage
      -- 
      out0(31 downto 16) <= in1 and (14 downto 7 => in2(0), 6 downto 3 => in2(1), 0=> in2(3), 2 downto 1 => in2(2)); -- bit 15 is not specified, it should not be supported 
      -- out1(31 downto 16) <= in1 and (15 downto 0 => in2(0)); -- this is valid VHDL, so see issue92b.vhd
      -- out2(31 downto 16) <= in1 and (32 downto 17 => in2(1));-- this is valid VHDL, so see issue92b.vhd
      out3(31 downto 16) <= in1 and (14 downto 7 => in2(0), 6 downto 3 => in2(1), 0=> in2(3), others => '0'); -- others should cover bits 15, 2, 1, it should not be supported 
      out4(31 downto 16) <= in1 and (others => in2(2)); -- others should cover 16 bits wide, it should not be supported 
      out5(15 downto 0) <= in1 and (others => in2(3));  -- others is intended to be 16 bits, but constraint is missing it should not be supported 
    end if;
  end process;
end;
