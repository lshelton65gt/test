-- Reproduces issue 45 exposed by customer 2 testcase
-- Segfault during PostPopulateResynth, on the RHS of the
-- VhdlSignalAssignment in the combinational process.
--
-- see also issue38.vhd
library ieee;
use ieee.std_logic_1164.all;

package types is
   constant RANGE_1_dt_0         : std_logic_vector(1 downto 0)  := (others => '0');
   constant RANGE_15_dt_0        : std_logic_vector(15 downto 0) := (others => '0');
   constant RANGE_13_dt_4        : std_logic_vector(13 downto 4) := (others => '0');
   constant RANGE_3_dt_2         : std_logic_vector(3 downto 2)  := (others => '0');
   constant CONST_7              : natural := 7;
   
   type array15 is array (natural range <>) of std_logic_vector(14 downto 0);
   type array16x15 is array (natural range <>) of array15(15 downto 0);
   type array12 is array (natural range <>) of std_logic_vector(11 downto 0);
   type array16x12 is array (natural range <>) of array12(15 downto 0);

   type array2x16x15 is array (natural range <>) of array16x15(RANGE_1_dt_0'range);

end package;

-----------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;
use work.types.all;
entity issue45 is
  port (
      clk : in std_logic;
      addr : in std_logic_vector(3 downto 0);
      sel : in std_logic;
      result : out std_logic_vector(15 downto 0)
  );
end issue45;

architecture rtl of issue45 is

   signal   sig_array_8x2x16x15     : array2x16x15(CONST_7 downto 0):= (others=>(others=>(others=>"010101010101010")));
   signal   sig_array_2x16x12       : array16x12(RANGE_1_dt_0'range) := (others=>(others=>"010101010101"));

begin

process (clk) is
                variable addr1InRange, addr2InRange : integer;

begin
  if clk'event and clk = '1' then

    addr1InRange := CONV_INTEGER(addr);  --  when ( CONV_INTEGER(addr) < 1 ) --  else 1;
    if addr1InRange > 1 then
      addr1InRange := 1;
    end if;
    if addr1InRange < 0 then
      addr1InRange := 0;
    end if;
    addr2InRange := CONV_INTEGER(addr);  --  when ( CONV_INTEGER(addr) < 15 ) else 15;
    if addr2InRange > 15 then
      addr2InRange := 15;
    end if;
    if addr2InRange < 0 then
      addr2InRange := 0;
    end if;

    sig_array_8x2x16x15(7)(addr1InRange)(addr2InRange) <= "111111111111110";
    
    for II in RANGE_1_dt_0'range loop
      for JJ in RANGE_15_dt_0'range loop
        if sel = '0' then
          sig_array_2x16x12(II)(JJ) <= sig_array_8x2x16x15(7)(II)(JJ)(RANGE_13_dt_4'high downto RANGE_3_dt_2'low);
        end if;
      end loop;
    end loop;

    result <= ('1' & '0' & '1' & '0' & sig_array_2x16x12(addr1InRange)(addr2InRange)(11 downto 0));
  end if;
end process;

end rtl;

