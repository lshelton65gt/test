
entity bottom2 is
   port (verilogin2  :  in bit;
         verilogout2 : out bit);
end bottom2 ;

architecture arch_bottom2 of bottom2 is
begin
    verilogout2 <= verilogin2;
end arch_bottom2;

entity bottom3 is
   port (verilogin3  :  in bit;
         verilogout3 : out bit);
end bottom3 ;

architecture arch_bottom3 of bottom3 is
begin
    verilogout3 <= verilogin3;
end arch_bottom3;

entity bottom4 is
   port (verilogin4  :  in bit;
         verilogout4 : out bit);
end bottom4 ;

architecture arch_bottom4 of bottom4 is
begin
    verilogout4 <= verilogin4;
end arch_bottom4;


entity forGen02_top is
   port (vhdlin  :  in bit_vector(0 to 2);
         vhdlout : out bit_vector(0 to 2));
end forGen02_top ;

architecture arch_top of forGen02_top is

   Component comp_bottom
      port (comp_verilogin  :  in bit;
            comp_verilogout : out bit);
   end Component;

   constant  loopVar1 : integer := 0;
   constant  loopVar2 : integer := 1;
   constant  loopVar3 : integer := 2;
begin

   --Label1: for loopVar1 in 0 to 2 generate
   -- begin
       Label2 : if loopVar1 = 0 generate
       begin
           inst : comp_bottom
              port map(comp_verilogin => vhdlin(loopVar1), comp_verilogout => vhdlout(loopVar1));
       end generate Label2;
       Label3 : if loopVar2 = 1 generate
       begin
           inst : comp_bottom
              port map(comp_verilogin => vhdlin(loopVar2), comp_verilogout => vhdlout(loopVar2));
       end generate Label3;
       Label4 : if loopVar3 = 2 generate
       begin
           inst : comp_bottom
              port map(comp_verilogin => vhdlin(loopVar3), comp_verilogout => vhdlout(loopVar3));
       end generate Label4;
   --end generate Label1;

end arch_top;

configuration config_forGen02_top of forGen02_top is
    for arch_top
        -- for Label1
            for Label3
                for inst : comp_bottom use entity work.bottom3
                       port map (verilogin3 => comp_verilogin, verilogout3 => comp_verilogout);
                end for;
            end for;
            for Label4
                for inst : comp_bottom use entity work.bottom4
                       port map (verilogin4 => comp_verilogin, verilogout4 => comp_verilogout);
                end for;
            end for;
            for Label2
                for inst : comp_bottom use entity work.bottom2
                       port map (verilogin2 => comp_verilogin, verilogout2 => comp_verilogout);
                end for;
            end for;
       -- end for;
    end for;
end config_forGen02_top;


