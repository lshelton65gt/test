-- May 2009
-- The issue was with dquant_int_tmp. CBuild was allocating the net with wrong
-- size.
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;


package pkg_functions  is
  function func1 (signal qp    : std_logic_vector(5 downto 0);
                  signal quant : std_logic_vector(5 downto 0))
  return std_logic_vector;
end pkg_functions ;

package body pkg_functions is 

  function func1 (signal qp    : std_logic_vector(5 downto 0);
                  signal quant : std_logic_vector(5 downto 0))
  return std_logic_vector is
    variable dquant_int     : integer range -51 to 51;  
    variable dquant_int_tmp : integer range -26 to 25;

    variable next_dquant : std_logic_vector(5 downto 0);
  begin

    dquant_int := TO_INTEGER(UNSIGNED(quant)) - TO_INTEGER(UNSIGNED(qp));
               
    IF dquant_int > 25 THEN                --dquant >  25
      dquant_int_tmp := dquant_int - 52;   --dquant -  52
    ELSIF dquant_int > (-27) THEN          --dquant > -27 
      dquant_int_tmp := dquant_int;
    ELSE                                   --dquant < -26 
      dquant_int_tmp := dquant_int + 52;   --dquant +  52
    END IF;

    next_dquant := STD_LOGIC_VECTOR(TO_SIGNED(dquant_int_tmp, 6));

  return next_dquant;
  end func1;

  
end pkg_functions ;


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY work;
use work.pkg_functions.all;   

entity bug12602_01 is
  
  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    in1  : in  std_logic_vector(5 downto 0);
    out1 : out std_logic_vector(5 downto 0));

end bug12602_01;


architecture arch1 of bug12602_01 is
  signal temp     : std_logic_vector(5 downto 0) := "000000";
begin  -- arch1

  temp(4 downto 0) <= in1(4 downto 0);
  
  p2: process (clk, rst)
  begin  -- process
    if rst = '0' then                   -- asynchronous reset (active low)
      out1 <= (others => '0');
    elsif clk'event and clk = '1' then  
      out1 <= func1(in1, temp); 
    end if;
  end process;

end arch1;
