-- This triggers the following error in the Carbon VHDL analyzer (Verific flow):
-- nor_reduce.vhd:26: Error 3791: Failed to get constraint for  nor_reduce's subprogram argument.
-- The root cause is a NULL return from VhdlOperator::EvaluateConstraintInternal when given the nor_reduce argument

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
entity nor_reduce2 is
port (
        din1 : in std_logic;
        din2 : in std_logic_vector(1 downto 0);
        din4 : in std_logic_vector(1 downto 0);
        din3 : in std_logic;
        dout1: out std_logic;
        dout2: out std_logic
);
end nor_reduce2;

architecture rtl of nor_reduce2 is
begin
      -- This call to nor_reduce triggers the error
      dout1 <= NOR_REDUCE((din1 or din3) & din2);
      dout2 <= NOR_REDUCE((din1 or din3) & din2 & din4);
      -- This call to nor_reduce does not trigger an error
      -- dout <= NOR_REDUCE((din2 or not din4) & din1);
end architecture rtl;
