library ieee;
use ieee.std_logic_1164.all;

library sub2_lib;
use sub2_lib.all;

package subPack is

  component sub2only
    port (
     clk  : in  std_logic;
     inS  : in  std_logic;                           
     outs : out std_logic);     
  end component;  

end subPack;
