-- this is dyn_slice10.vhd, where reset is clearing dout, which is
-- not what I meant to do.  But it allows the test to run fast.
--
-- In this case, I successfully diffed the results against MTI after
-- I globally replaced U -> 0 in the MTI results.  If I clear mem 
-- on reset then I think it will all compare, but then I run into
-- scalability issues.  That's covered in dyn_slice12.vhd

-- The purpose of this test is to prove that we can handle the
-- part-select-write to the huge vector "mem", whose pessimistic
-- size is larger than our implementation limit of 65535 bits.
-- We should be able to handle this because the RHS tells us that
-- the size of the assign must be 32 bits.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity dyn_slice11 is  
    port (
      clk : in std_logic;
      rst : in std_logic;
      a : in std_logic_vector(31 downto 0);
      idx_in : in unsigned(18 downto 0);
      idx_out : in unsigned(18 downto 0);
      dout : out std_logic_vector(31 downto 0)
      );
end dyn_slice11;

architecture dyn_slice11 of dyn_slice11 is
  signal mem : std_logic_vector(524287+32 downto 0);
begin
  process (clk, rst, a, idx_in, idx_out)
    variable low : integer;
    variable high : integer;
  begin
    if rst = '1' then
      dout <= (others=>'0');
    else
      if (clk = '1' and clk'event) then
        low := to_integer(idx_in);
        high := low + 31;
        mem(high downto low) <= a;
        low := to_integer(idx_out);
        high := low + 31;
        dout <= mem(high downto low);
      end if;
    end if;
  end process;
end dyn_slice11;
