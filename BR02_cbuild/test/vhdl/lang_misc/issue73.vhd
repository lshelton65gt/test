-- Reproducer for issue73, customer 2 b testcase
-- TODO: Enhance this test to make a good simulation
-- testcase.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity issue73 is
  port (
         clk : in std_logic;
         in1 : in std_logic;
         out1 : out unsigned(0 downto 0)
       );
end issue73;

architecture rtl of issue73 is
begin

   process (clk) is
     -- Failure occurs while processing this declaration.
     constant foo : unsigned := conv_unsigned(1,1);
   begin 
     if (clk'event and clk = '1') then
        if (in1 = '1') then
           out1 <= "0";
        else
           out1 <= foo;
        end if;
     end if;
   end process;

end rtl;

