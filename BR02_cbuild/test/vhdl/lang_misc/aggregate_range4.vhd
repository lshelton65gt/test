-- test for aggregate range negative to positive (-4 to 4)

library IEEE;
use IEEE.std_logic_1164.all;

package pack is
    subtype idx is integer range -4 to 4;
    subtype value is integer range 0 to 3;
    type ttype is array(idx range <>) of value;
    constant table: ttype(-4 to 4) :=
                (0, 1, 1, 2, 2, 2, 3, 3, 3);
end;
package body pack is
end;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.pack.all;

entity aggregate_range4 is
    port(in1 : in  integer;
         out1: out integer);
end aggregate_range4;
architecture a of aggregate_range4 is
begin
    process(in1)
    variable x  : integer;
    begin
        x := in1 mod 8;
        x := x - 4;
        out1 <= table(x);
    end process;
end;


