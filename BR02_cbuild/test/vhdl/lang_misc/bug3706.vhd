library ieee;
use ieee.std_logic_1164.all;

entity bug3706 is

  port (
    clk : in std_logic;
    d   : in std_logic_vector(31 downto 0);
    q   : out std_logic_vector(31 downto 0));

end bug3706;

architecture arch of bug3706 is
  function funky ( constant in1 : integer )  return integer is
  begin  -- funky
    return in1;
  end funky;
  subtype address is std_logic_vector(funky(31) downto 0);
  type array_of_addresses is array (natural range <>) of address;
  signal s : array_of_addresses(0 to 1);

begin  

  p1: process (clk)
  begin  -- process p1
    if rising_edge(clk) then
        s(0) <= d;
        s(1) <= d;
    end if;
  end process p1;

  q <= s(0) xor s(1);
end ;

