-- This is a reproducer for customer 2 issue 60 memory leak
--  #Bytes %Tot #Allocs StackTrace
--     40 100%       1 NUConst::createKnownIntConst(unsigned long long, unsigned int, bool, SourceLocator const&)
--                     verific2nucleus::VerificVhdlDesignWalker::VisitVhdlInteger(Verific::VhdlInteger&)
--                     verific2nucleus::VerificVhdlDesignWalker::arrayIndex(Verific::VhdlIndexedName&)
--                     verific2nucleus::VerificVhdlDesignWalker::VisitVhdlIndexedName(Verific::VhdlIndexedName&)
--                     verific2nucleus::VerificVhdlDesignWalker::VisitVhdlConditionalWaveform(Verific::VhdlConditionalWaveform&)
--                     verific2nucleus::VerificVhdlDesignWalker::VisitVhdlConditionalSignalAssignment(Verific::VhdlConditionalSignalAssignment&)
--                     verific2nucleus::VerificVhdlDesignWalker::VisitVhdlArchitectureBody(Verific::VhdlArchitectureBody&)
--                     verific2nucleus::VerificVhdlDesignWalker::VisitVhdlEntityDecl(Verific::VhdlEntityDecl&)
--                     CarbonContext::runVerificPopulateNucleus()
--                     CarbonContext::runPopulateNucleus()
--            0xb5c401e0

library ieee;
use ieee.std_logic_1164.all;

package typedecls is

  type matrix_type is array (0 to 1, 0 to 1) of std_logic_vector(3 downto 0);
  type matrix_array_type is array  (1 downto 0) of matrix_type;

end package;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;
use work.typedecls.all;
entity issue60 is
port( clk : in std_logic;
      a : in std_logic_vector(3 downto 0);
      c : out std_logic_vector(3 downto 0)
     );
end issue60;

architecture rtl of issue60 is
 
 signal buf     : matrix_array_type;

begin

 -- Each of these assignments cause a memory leak
 buf(0)(0,0) <= a;
 c <= buf(0)(0, 0);

end rtl;
