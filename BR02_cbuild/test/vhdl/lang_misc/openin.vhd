-- This tests to ensure that an open input port is not accepted by cbuild.
-- VHDL does not allow unconnected input ports unless they have a default
-- value. Since cbuild does not support default values, unconnected input ports
-- are not supported.

entity bottom is
  port (
    in1, in2 : in  bit;
    out1     : out bit);
end bottom;

architecture arch of bottom is

begin

  out1 <= in1 and in2;

end arch;

entity openin is
  port (
    in1, in2   : in  bit;
    out1, out2 : out bit);
end openin;

architecture arch of openin is

  component bottom
    port (
      in1, in2 : in  bit;
      out1     : out bit);
  end component;
  
begin

  u1: bottom
    port map (
      in1  => in1,
      in2  => in2,
      out1 => out1);

  u2: bottom
    port map (
      in1  => in1,
      in2  => open,
      out1 => out2);

end arch;
