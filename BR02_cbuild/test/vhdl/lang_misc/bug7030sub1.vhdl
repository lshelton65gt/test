library ieee;
use ieee.std_logic_1164.all;

entity sub is
  port (
    clk  : in  std_logic;
    inS  : in  std_logic;
    outS : out std_logic);
end sub;

architecture archSub of sub is
  signal temp  : std_logic  := '0';
begin  -- archSub

  outS <= temp;
  
  p1: process (clk, inS)
  begin  -- process p1
    if(clk = '1' and clk'event) then 
      temp <= inS;
    end if;
  end process p1;

end archSub;

library ieee;
use ieee.std_logic_1164.all;

entity sub1only is
  port (
    clk  : in  std_logic;
    inS  : in  std_logic;
    outS : out std_logic);

end sub1only;

architecture archSub1only of sub1only is
  signal temp  : std_logic  := '0';
begin  -- archSub1only

  outS <= temp;
  
  p1: process (clk, inS)
  begin  -- process p1
    if(clk = '1' and clk'event) then 
      temp <= inS;
    end if;
  end process p1;
 
end archSub1only;
