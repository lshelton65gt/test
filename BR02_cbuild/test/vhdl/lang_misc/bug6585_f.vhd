-- last mod: Wed Sep 13 15:41:40 2006
-- filename: test/vhdl/lang_misc/bug6585_f.vhd
-- Description:  Same as bug6585_e.vhd, except that direct constants are used instead 
-- of constant aggregates to compute variable range vector range.


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6585_f is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector(15 downto 0);
    out1     : out std_logic_vector(15 downto 0));
end;

architecture arch of bug6585_f is 

function level2 (v : std_logic_vector; argsize_left : integer) return std_logic_vector;
function level1 (v : std_logic_vector) return std_logic_vector;

function level2 (v : std_logic_vector; argsize_left : integer) return std_logic_vector is
-- constant argsize_left : integer := v'left;
  constant argsize_right : integer := v'right;
  constant argsize : integer := argsize_left - argsize_right + 1;
  constant argsize_minus_1 : integer := argsize - 1;
  subtype rtype is std_logic_vector(argsize_minus_1 downto 0);
  variable retval : rtype;
begin
  retval := level1(v);
  return retval;
end level2;


function level1 (v : std_logic_vector) return std_logic_vector is
  constant argsize : integer := v'length;
  constant argsize_minus_1 : integer := argsize - 1;
  subtype rtype is std_logic_vector(argsize_minus_1 downto 0);
  variable retval : rtype;
begin 
  retval := sxt(v, argsize);
  return retval;
end level1;

constant vec_off : integer := 3;
constant vec_width : integer := 4;

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
     for i in vec_off downto 0 loop
      out1(vec_off+vec_width*i downto vec_width*i) <= level2(in1(vec_off+vec_width*i downto vec_width*i), vec_off+vec_width*i);
     end loop;  -- i
    end if;
  end process;
end;
