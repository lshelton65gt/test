-- Reproduces bug7030, where a use clause like <library>.entity results
-- in a Jaguar error.

library ieee;
use ieee.std_logic_1164.all;

entity sub is
  
  port (
    in1  : in  std_logic;
    out1 : out std_logic);

end sub;

architecture arch of sub is

begin  -- arch

  out1 <= in1;

end arch;


library ieee;
use ieee.std_logic_1164.all;

library work;
use work.sub; -- <library>.<entity>, this line caused a crash (bug7030)

entity bug7030 is
  
  port (
    in1  : in  std_logic;
    out1 : out std_logic);

end bug7030;


architecture arch of bug7030 is

  component sub
    port (
      in1  : in  std_logic;
      out1 : out std_logic);
  end component;

begin  -- arch

  s1 : sub port map (
    in1  => in1,
    out1 => out1);

end arch;

