entity bug8654_01 is
  
  port (
    in1  : in  bit_vector(7 downto 0);
    out1 : out bit_vector(7 downto 0);
    out2 : out bit_vector(7 downto 0));

end bug8654_01;

architecture arch of bug8654_01 is

begin  -- arch

  out1 <= in1(7 downto 2) & "00";
  out2 <= "00" & in1(7 downto 2);

end arch;
