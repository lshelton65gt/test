-- June 2008
-- This file tests partially open port map.
-- This file should be compiled with -87 switch.

library ieee;
use ieee.std_logic_1164.all;

package pack is
  type mem is array ( integer range 0 to 2) of std_logic_vector(0 to 1);
end;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bottom is
  port(in1, in2, in3 : in std_logic_vector(0 to 1); 
       out1 : buffer mem);
end;


architecture arch of bottom is
begin
  out1 <= (in1, in2, in3);
end;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bug8906_01 is
  port(in1, in2, in3 : in std_logic_vector(0 to 1); 
       outa: buffer std_logic_vector(0 to 1));
end;


architecture arch of bug8906_01 is
component bottom 
  port(in1, in2, in3 : in std_logic_vector(0 to 1); 
       out1 : buffer mem);
end component;

signal tmpmem : mem;

begin

  
  inst1 : bottom 
          port map(in1 => in1, in2 => in2, in3 => in3,
                  out1(0 to 1) => open,  -- this line is tested
                  out1(2) => outa);
end;
