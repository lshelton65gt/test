-- This testcase reproduces sub-issue of customer 2 b issue 71.
--
-- issue71c.vhd issue with record constant assignment 

package destypes is

type coord is
  record
    x : integer range 5 downto 0;
    y : integer range 6 downto 0;
    z : integer range 7 downto 0;
  end record;


constant val : coord := (4,3,0);


end package;


library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.destypes.all;

entity issue71c is
  port (
        in_x : in integer range 5 downto 0;
        in_y : in integer range 6 downto 0;
        in_z : in integer range 7 downto 0;
        x : out integer range 13 downto 0;
        y : out integer range 14 downto 0;
        z : out integer range 15 downto 0
       );
end issue71c;

architecture rtl of issue71c is
   signal trio : coord;
begin
   -- The incorrect population of constant results to simulation diff (if left hand-side is record we should populate concat and not try to convert it to integer, resynth gets confused with integer population)
   trio <= val;
   x <= in_x + trio.x;
   y <= in_y + trio.y;
   z <= in_z + trio.z;
end rtl;

