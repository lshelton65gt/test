-- This test reproduces two errors noticed during compile of Matrox design.
-- First error had to do with use of constant expression instead of a constant
-- as second argument of conv_std_logic_vector, a library call.
-- Second error had to do with use of a user defined function ..that returns
-- unconstrained array inside an expression. Typically unconstrained array
-- bounds are derived from lvalue. In an expression no lvalue is available.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6585_m is
  
  port (
    in1 : in std_logic_vector(68 downto 0);
    in2 : in std_logic_vector(155 downto 0);
    out1 : out std_logic_vector(155 downto 0));

end bug6585_m;

architecture arch of bug6585_m is

  function zxt (arg          : std_logic_vector; size : integer ) return std_logic_vector is
    constant shft : integer := size - arg'length;
    subtype t1 is std_logic_vector(size-1 downto 0);
    variable rslt : t1;
  begin
    rslt := conv_std_logic_vector(shl(unsigned(conv_std_logic_vector(unsigned(arg), size)), conv_unsigned(shft, 3)), size);
    return rslt;
  end function zxt;

  function szxt ( arg : std_logic_vector;  sgn : integer; pad : integer)
    return std_logic_vector is
    constant isize : integer := arg'length + pad;
    constant fsize : integer := isize + sgn;
    subtype rtype is std_logic_vector(fsize-1 downto 0);
    variable result : rtype;
  begin
    result := sxt(zxt(arg, isize), fsize);
    return result;
  end function szxt;

  constant ubound : integer := 2;
  constant lbound : integer := 0;
  
begin  -- arch

   Xout1: process (in1, in2)
   begin  -- process Xout1
     for i in ubound downto lbound loop
       out1(38+i*39 downto i*39) <=
         unsigned(in2(38+i*39 downto i*39)) +
         unsigned(not szxt(in1(22+i*23 downto i*23), 11, 5)) + '1';
     end loop;  -- i
     out1(155 downto 117) <=
       unsigned(in2(155 downto 117)) +
       unsigned(not szxt(in1(22 downto 0), 11, 5)) + '1';
   end process Xout1;
   
end arch;
