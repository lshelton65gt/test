-- bug7030_5 - In the top VHDL file we use library specifications sub1_lib, sub2_lib
-- and pack_lib. The pack_lib library uses sub2_lib library specification.
-- So, the parser should be able to find the sub2only entity, which is compiled
-- into the sub2_lib. (Look in the cmdsOpts file for command line options).
-- 

library ieee;
use ieee.std_logic_1164.all;

library sub1_lib;
--use sub1_lib.all;
library sub2_lib;
--use sub2_lib.all;
library pack_lib;
use pack_lib.subPack.all;


entity bug7030_5 is
  
  port (
    clk  : in  std_logic;
    in1  : in  std_logic;
    out1 : out std_logic;
    out2 : out std_logic);
end bug7030_5;

architecture arch of bug7030_5 is


begin  -- arch

  s1 : entity sub1_lib.sub  port map (
    clk  => clk,
    inS  => in1,
    outS => out1);

  s3 : sub2only  port map (
    clk  => clk,
    inS  => in1,
    outS => out2);

end arch;
