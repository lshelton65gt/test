-- filename: test/vhdl/lang_misc/issue59.vhd
-- Description:  This test duplicates the issue 59 problem seen in Customer 2


library ieee;
use ieee.std_logic_1164.all;

package issue59_package is
  type byte_record_type is
      record
         valid : std_logic;
         data  : std_logic_vector(7 downto 0);
      end record;

  type word_record_type is array (0 to 15) of byte_record_type;
  
  function slv_to_word_record(vector : std_logic_vector(143 downto 0)) return word_record_type;
end package;
package body issue59_package is

function slv_to_word_record(vector : std_logic_vector(143 downto 0)) return word_record_type is
      variable result : word_record_type;
      variable low_bit  : integer;
      variable high_bit : integer;
   begin
      for byte in 0 to 15 loop
         -- Data
         low_bit  := byte*9;
         high_bit := low_bit+result(byte).data'high;
         result(byte).data  := vector(high_bit downto low_bit);
         -- Valid
         high_bit := high_bit + 1;
         result(byte).valid := vector(high_bit);
      end loop;
      return(result);
   end function;

end issue59_package;


library ieee;
use ieee.std_logic_1164.all;
use work.issue59_package.all;

entity issue59 is
  generic (
    WIDTH : integer := 8;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1, in2 : integer;
    in_wide  : in std_logic_vector(143 downto 0);
    out1     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of issue59 is
   signal loc_signal         : word_record_type;


begin

main: process (clock)
  begin
    if clock'event and clock = '1' then
      loc_signal <= slv_to_word_record(in_wide);
    end if;
  end process;
  out1 <= loc_signal(in1).data when (loc_signal(in1).valid = '1') else loc_signal(in2).data;
end;
