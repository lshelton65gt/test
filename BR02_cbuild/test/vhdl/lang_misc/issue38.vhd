-- This reproduces issue 38 with the customer 2 testcase.
-- Declaration of certain types of multi-dimensional arrays.
-- This test was also modified to show issue_45.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity issue38 is
  port (
    clk : in std_logic;
    in1 : in integer;
    in2 : in integer;
    in3 : in integer;
    in4 : in integer;
    din : in std_logic_vector(31 downto 0);
    dout : out std_logic_vector(31 downto 0)
  );
end issue38;

architecture rtl of issue38 is

type word_array is array (natural range <>) of std_logic_vector(31 downto 0);
type a_word_array is array (natural range <>) of word_array(15 downto 0);
type a_a_word_array is array (natural range<>) of a_word_array(7 downto 0);

-- Issue_38 Failure occurs during processing of this declaration.
signal mem : a_a_word_array(3 downto 0);
signal addr1 : std_logic_vector(0 to 1);
signal addr2 : std_logic_vector(0 to 2);
signal addr3 : std_logic_vector(0 to 3);

begin
  main: process (clk)
  begin
    if clk'event and clk = '1' then 

      
     addr1 <= std_logic_vector(to_unsigned(in1, 2));
     addr2 <= std_logic_vector(to_unsigned(in2, 3));
     addr3 <= std_logic_vector(to_unsigned(in3, 4));
     mem(to_integer('0' & unsigned(addr1)))(to_integer('0' & unsigned(addr2)))(to_integer('0' & unsigned(addr3))) <= din;         -- this is issue_45 (multi dim memory used on LHS)
     dout <= mem(to_integer('0' & unsigned(addr1)))(to_integer('0' &
                                                               unsigned(addr2)))(to_integer('0' & unsigned(addr3)));        -- this is issue_38 (multi dim memory used on RHS)
    end if;
  end process;

end rtl;
