-- This is a simplified version of the kind of recursion used by one of our customers.
-- The Interra flow has had problems with this, but the Verific flow handles it correctly.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.or_reduce;
use ieee.std_logic_misc.and_reduce;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

entity recursive_test1 is
  
  port (
    inp  : in  std_logic_vector(7 downto 0);
    outp : out std_logic_vector(2 downto 0);
    clk  : in  std_logic);

end recursive_test1;

architecture test_behavior of recursive_test1 is
  
  function extract_recursive (a : std_logic_vector) return std_logic_vector is
      variable temp : std_logic_vector(a'length-1 downto 0);
   begin
      temp := a;
      if (temp'length = 1) then
         return(temp);
      elsif (temp'length = 2) then
         if (temp(0) = '1') then
            return("0");
         else
            return("1");
         end if;
      else
         if (or_reduce(temp((temp'length/2)-1 downto 0)) = '1') then
            return('0' & extract_recursive(temp((temp'length/2)-1 downto 0)));
         else
            return('1' & extract_recursive(temp(temp'length-1 downto (temp'length/2))));
         end if;
      end if;
   end function extract_recursive;

begin

  process(clk)
  begin
    if(clk = '1' and clk'event) then
      outp <= extract_recursive(inp);
    end if;
  end process;
   
end test_behavior;

  
