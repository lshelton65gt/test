-- This reproduces issue 37 with the customer 2 testcase
-- (derived from issue 17)
library ieee;
use ieee.std_logic_1164.all;
package destypes is

   type halfword is
     record
       hi : std_logic_vector(7 downto 0);
       lo : std_logic_vector(7 downto 0);
     end record;

   type halfword_array is array(3 downto 0) of halfword;

end package;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;
use work.destypes.all;
entity issue37 is
  port (
    clk : in bit;
    rst : in bit;
    din : in std_logic_vector(7 downto 0);
    dout: out std_logic_vector(7  downto 0)
  );
end entity issue37;

architecture rtl of issue37 is

signal cache : halfword_array;     

begin

   process (clk, rst) is
   begin
      if (rst = '1') then
         for i in 3 downto 0 loop
           cache(i).hi <= "00000000";
           cache(i).lo <= "00000000";
         end loop;
      elsif (clk'event and clk = '1') then
         for i in 3 downto 0 loop
           for j in 7 downto 0 loop
             cache(i).hi(j) <= din(j);
             cache(i).lo(j) <= not din(j);
           end loop;
         end loop;
         dout <= cache(3).hi;
      end if;
    end process;

end rtl;

   
