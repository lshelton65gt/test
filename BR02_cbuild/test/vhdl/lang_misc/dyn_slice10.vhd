-- this is from test/cust/qualcomm/prevoltron/carbon/projects/xilinx_libs,
-- where a downto write to a huge vector stopped working with new sizing.
-- But we should be able to accurately size the LHS based on the fixed-size
-- RHS.

-- This compiles slowly in Carbon due to port coercion, and this needs to
-- be investigated.  dyn_slice11.vhd captures the intended issue, which
-- is pessimistically sized partsels exceeding implementation limits, without
-- introducing compile-time slowdowns

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity dyn_slice10 is  
    port (
      clk : in std_logic;
      rst : in std_logic;
      a : in std_logic_vector(31 downto 0);
      idx : in unsigned(18 downto 0);
      dout : out std_logic_vector(524287+32 downto 0)
      );
end dyn_slice10;

architecture dyn_slice10 of dyn_slice10 is
begin
  
  process (clk, rst, a, idx)
    variable low : integer;
    variable high : integer;
  begin
    if rst = '1' then
      dout <= (others=>'0');
    else
      if (clk = '1' and clk'event) then
        low := to_integer(idx);
        high := low + 31;
        dout(high downto low) <= a;
      end if;
    end if;
  end process;
end dyn_slice10;
