entity decl_in_entity is
  port (in1, in2 : bit; out1 : out bit);
  signal temp : bit;
end;
architecture arch of decl_in_entity is
begin
  temp <= in1 and in2;
  out1 <= temp;
end;
