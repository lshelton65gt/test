-- This reproduces issue 34 with the customer 2 testcase
--
-- This is happening in the VhdlSelectedName visitor, on a signal assignment of the form:
-- array_of_records(index).field <= value.
-- The failure is during processing of the LHS, which is a VhdlSelectedName

library IEEE;
use IEEE.std_logic_1164.all;

package destypes is

   type rec is
     record
       field1 : std_logic;
       field2 : std_logic;
     end record;

   type rec_array is array(integer range<>) of rec;

end package;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.destypes.all;

entity issue32 is
  port (
    clk : in bit;
    rst : in bit;
    value1 : in std_logic; 
    value2 : in std_logic;
    value3 : in std_logic;
    value4 : in std_logic;
    value5 : in std_logic;
    value6 : in std_logic;
    value7 : in std_logic;
    value8 : in std_logic;
    outp  : out std_logic_vector(7  downto 0)
  );
end entity issue32;

architecture rtl of issue32 is

signal foo : rec_array(3 downto 0);     

begin
   process (clk, rst) is
   begin
      if (rst = '1') then
         foo(3).field1 <= '0';
         foo(3).field2 <= '0';
         foo(2).field1 <= '0';
         foo(2).field2 <= '0';
         foo(1).field1 <= '0';
         foo(1).field2 <= '0';
         foo(0).field1 <= '0';
         foo(0).field2 <= '0';
      elsif (clk'event and clk = '1') then
         foo(3).field1 <= value1;
         foo(3).field2 <= value2;
         foo(2).field1 <= value3;
         foo(2).field2 <= value4;
         foo(1).field1 <= value5;
         foo(1).field2 <= value6;
         foo(0).field1 <= value7;
         foo(0).field2 <= value8;
         outp(0) <= foo(3).field1;
         outp(1) <= foo(3).field2;
         outp(2) <= foo(2).field1;
         outp(3) <= foo(2).field2;
         outp(4) <= foo(1).field1;
         outp(5) <= foo(1).field2;
         outp(6) <= foo(0).field1;
         outp(7) <= foo(0).field2;
      end if;
    end process;
end rtl;

   
