-- September 2009
-- This file tests the if/elsif/else statement, where the condition of
-- it is a binary expression with selected name of integer with constraint.
-- This if statement gets replaced with switch statement and the condition
-- gets replaced with LHS of the expression.
-- Before September 2009, the sign of the thread_nat was changed to signed,
-- which was causing simulation mismatch.

package pkg_types is
  constant WIDTH   : integer := 4;
  constant R1 : integer := 6;
  constant S1 : integer := 3;
  subtype range_nat_t is  integer range 0 to R1-1;
  subtype range_int_t is  integer range 0-S1 to R1-1-S1;
end pkg_types;


library work;
use work.pkg_types.all;


entity bug12901_01 is
  
  port (
    sel  : in  range_nat_t;
    in0  : in  bit_vector(WIDTH-1 downto 0);
    in1  : in  bit_vector(WIDTH-1 downto 0);
    in2  : in  bit_vector(WIDTH-1 downto 0);
    in3  : in  bit_vector(WIDTH-1 downto 0);
    in4  : in  bit_vector(WIDTH-1 downto 0);
    in5  : in  bit_vector(WIDTH-1 downto 0);
    out1 : out bit_vector(WIDTH-1 downto 0);
    out2 : out bit_vector(WIDTH-1 downto 0));

end bug12901_01;


architecture arch of bug12901_01 is
  type status_t is record
    thread_nat : range_nat_t;
    thread_int : range_int_t;
  end record;

  type ar_status_t is array (0 to R1 - 1) of status_t;

  signal status : ar_status_t;
begin  -- arch

  p1: process (sel)
  begin  -- process p2
    for i in status'range loop
      status(i).thread_nat <= sel;
      status(i).thread_int <= sel-S1;
    end loop;  
  end process p1;
  
  p2: process (sel, status, in0, in1, in2, in3, in4, in5)
  begin  -- process p1
    if status(sel).thread_nat = 1 then
      out1 <= in1;
    elsif status(sel).thread_nat = 2 then
      out1 <= in2;
    elsif  status(sel).thread_nat = 3 then
      out1 <= in3;
    elsif  status(sel).thread_nat= 4 then
      out1 <= in4;
    elsif  status(sel).thread_nat= 5 then
      out1 <= in5;
    else
       out1 <= in0;
      
    end if;
    
  end process p2;
  
  p3: process (sel, status, in0, in1, in2, in3, in4, in5)
  begin  -- process p1
    if status(sel).thread_int = -2 then
      out2 <= in1;
    elsif status(sel).thread_int = -1 then
      out2 <= in2;
    elsif  status(sel).thread_int = 0 then
      out2 <= in3;
    elsif  status(sel).thread_int = 1 then
      out2 <= in4;
    elsif  status(sel).thread_int = 2 then
      out2 <= in5;
    else
       out2 <= in0;
      
    end if;
    
  end process p3;
      

  
end arch;
