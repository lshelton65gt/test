-- Test reproduces the problem reported with bug7566. There was a hack in vhdl
-- population code to force the constant values for 'low,'high,'left,'right
-- attributes to be signed. Since ranged value expressions are sized to minimum
-- bits needed, this incorrectly changes a positive number to negative or
-- vice-versa. The hack was removed and this testcase generates functionally
-- correct results.
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY bug7566_1 IS

  PORT (
    in2               : in std_logic;
    out2              : out std_logic;
    out1		   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
    );

END bug7566_1;

ARCHITECTURE synth OF bug7566_1 IS

  SUBTYPE  subtype1 is NATURAL RANGE 31 DOWNTO 16;
  CONSTANT const4 : INTEGER := subtype1'LOW; -- Expected value 16. Incorrectly sign-forced
                                             -- to be -16.

  FUNCTION LSL	(L: STD_LOGIC_VECTOR; R: INTEGER)	RETURN STD_LOGIC_VECTOR IS
    VARIABLE TEMP: STD_LOGIC_VECTOR(L'length-1 DOWNTO 0);
    VARIABLE RSLT: STD_LOGIC_VECTOR(L'length-1 DOWNTO 0);
  BEGIN
    TEMP := L;
    RSLT := (others => '0');
    for i in R to L'length-1 loop
      RSLT(i) := TEMP(i-R);
    END loop;
    RETURN RSLT;
  END;

  SIGNAL sig1   : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";

BEGIN
  out1 <= LSL (sig1, const4);
  out2 <= in2;

END synth;
