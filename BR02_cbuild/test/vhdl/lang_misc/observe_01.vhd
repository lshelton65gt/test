-- filename: test/vhdl/lang_misc/observe_01.vhd
-- Description:  This test attempted to duplicate an issue from Customer 2 test
-- but was never successful at it, so instead this test has been used to check
-- if observeSignal is working for designs with multiple levels of hierarchy.
-- run with -directive observeAll.dir
-- to see if the signal defined within the observe_01_sub is marked as observable,
-- 


library ieee;
use ieee.std_logic_1164.all;

package observe_01_package is 
  type int_array is array (integer range <>) of integer;

  type halfbyte_valid_type is record
                                valid : std_logic;
                                blk_id : int_array(1 downto 0);
                              end record;
  
  type halfbyte_type is record
                          data_hi  : std_logic_vector(7 downto 4);
                          data_low : std_logic_vector(3 downto 0);
                        end record;
  
  
  type record_144_type is
      record
         valid : halfbyte_valid_type;
         data  : halfbyte_type;
         valid2 : halfbyte_valid_type;
      end record;

  
  function slv_to_record(vector_r : std_logic_vector) return record_144_type;
end package;
package body observe_01_package is

function slv_to_record(vector_r : std_logic_vector) return record_144_type is
      variable vector : std_logic_vector(vector_r'length-1 downto 0);
      variable result : record_144_type;
      variable low_bit  : integer;
      variable high_bit : integer;
  begin 
         vector := vector_r;

         low_bit  := 0;
         high_bit := low_bit+result.data.data_low'high;
         result.data.data_low  := vector(high_bit downto low_bit);
         low_bit  := high_bit + 1;
         high_bit := low_bit+3;
         result.data.data_hi  := vector(high_bit downto low_bit);         
         -- Valid
         high_bit := high_bit + 1;
         result.valid.valid := vector(high_bit);
         result.valid2.valid := vector(high_bit-1);
         result.valid.blk_id(0) := 3;
         result.valid2.blk_id(1) := 2;

      return(result);
   end function;

end observe_01_package;


library ieee;
use ieee.std_logic_1164.all;
use work.observe_01_package.all;

entity observe_01_sub is
  generic (
    WIDTH : integer := 8;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in_wide1  : in std_logic_vector(143 downto 0);
    out1     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of observe_01_sub is
   signal \(loc_signal1\         : record_144_type;


begin

main: process (clock)
  begin
    if clock'event and clock = '1' then
      \(loc_signal1\ <= slv_to_record(in_wide1);
    end if;
  end process;
  out1 <= (\(loc_signal1\.data.data_hi & \(loc_signal1\.data.data_low) when (\(loc_signal1\.valid.valid = '1') else (\(loc_signal1\.data.data_hi & \(loc_signal1\.data.data_low);
end;


library ieee;
use ieee.std_logic_1164.all;
use work.observe_01_package.all;

entity observe_01 is
  generic (
    WIDTH : integer := 8;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1  : in std_logic_vector(143 downto 0);
    in2  : in std_logic_vector(143 downto 0);
    out1     : out std_logic_vector((WIDTH-1) downto 0);
    out2     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of observe_01 is

   component observe_01_sub
     port (
       clock : in  std_logic;
       in_wide1   : in  std_logic_vector(143 downto 0);
       out1  : out std_logic_vector(7 downto 0));
   end component;

begin

  g0: for i in 0 to 0 generate
  begin
    sub1 : observe_01_sub port map (
      clock => clock,
      in_wide1   => in1,
      out1  => out1);
  
    sub2 : observe_01_sub port map (
      clock => clock,
      in_wide1   => in2,
      out1  => out2);
  end generate g0;  
end;
