-- This reproduces issue 34 with the customer 2 testcase
-- It needs to be beefed up to make a good simulation test case. Right
-- now, the output is constant.
library ieee;
use ieee.std_logic_1164.all;
package destypes is

   type descriptor is
     record
       status : std_logic_vector(47 downto 0);
       data   : std_logic_vector(47 downto 0);
     end record;

   constant null_descriptor_entry : descriptor := ((others=>'0'), (others=>'0'));
   type descriptor_array is array(47 downto 0) of descriptor;

end package;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;
use work.destypes.all;
entity issue34 is
  port (
    clk : in bit;
    rst : in bit;
    dout : out std_logic_vector(47 downto 0)
  );
end entity issue34;

architecture rtl of issue34 is

signal queue : descriptor_array;

begin

   process (clk, rst) is
   begin
      if (rst = '1') then
         queue(47 downto 0) <= (others => null_descriptor_entry);
      elsif (clk'event and clk = '1') then
         dout <= queue(3).data;
      end if;
    end process;

end rtl;

   
