-- This reproduces issue 17 with the customer 2 testcase
library ieee;
use ieee.std_logic_1164.all;
package destypes is

   type halfword is
     record
       hi : std_logic_vector(7 downto 0);
       lo : std_logic_vector(7 downto 0);
     end record;

   type halfword_array is array(3 downto 0) of halfword;
   type halfword_2darray is array(3 downto 0) of halfword_array;

end package;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;
use work.destypes.all;
entity issue17 is
  port (
    clk : in bit;
    rst : in bit;
    din : in std_logic_vector(7 downto 0);
    x1  : in std_logic_vector(1 downto 0);
    x2  : in std_logic_vector(1 downto 0);
    hw  : out std_logic_vector(7  downto 0)
  );
end entity issue17;

architecture rtl of issue17 is

signal cache : halfword_2darray;     

begin

   process (clk, rst) is
      variable x1_temp : integer range 0 to 3;
      variable x2_temp : integer range 0 to 3;
   begin
      if (rst = '1') then
         for i in 3 downto 0 loop
            for j in 3 downto 0 loop
              -- The error occurs here
              cache(i)(j).hi <= "00000000";
            end loop;
         end loop;
      elsif (clk'event and clk = '1') then
         x1_temp := conv_integer(unsigned(x1));
         x2_temp := conv_integer(unsigned(x2));
         cache(x1_temp)(x2_temp).hi <= din;
         cache(x1_temp)(x2_temp).lo <= not din;
         hw <= cache(x2_temp)(x1_temp).hi(7 downto 4) & cache(x2_temp)(x1_temp).lo(3 downto 0);
      end if;
    end process;

end rtl;

   
