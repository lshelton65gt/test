-- filename: issue87.vhd
-- Description:  This test duplicates a sim mismatch seen in Customer2A design,
-- seems to be related to initial value assignment to an array of slv


library ieee;
use ieee.std_logic_1164.all;

entity issue87 is
  generic (
    WIDTH : integer := 16);

  port (
    clock,resetn    : in  std_logic;
    in0, in1 : in  std_logic_vector((WIDTH-1) downto 0);
    out0, out1     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of issue87 is 
  constant WORD_RANGE       : std_logic_vector(15 downto 0)  := (others => '0');
  constant NUM_WORD_RANGE   : std_logic_vector(1 downto 0)   := (others => '0');
  type t_word_array is array (natural range <>) of std_logic_vector(WORD_RANGE'range);   

  signal done  : t_word_array(NUM_WORD_RANGE'range);

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
      -- send both words to outputs to keep them live
      out1 <= in1 and done(1);
      out0 <= in0 and done(0);
    end if;
  end process;

  p_done : process(clock, resetn)
  begin
    if (resetn = '0') then
      done <= (others => (others => '1')); --this line is populated incorrectly
    elsif (rising_edge(clock)) then
      done <= ("0000111100001111", "1111000011110000");
    end if;
  end process;
end;
