-- This reproduces issue 35 with the customer 2 testcase.
-- This issue shows up in 'signed' typemark.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;

entity issue35 is
  generic (
    N : integer := 4
  );
  port (
    clk : in bit;
    rst : in bit;
    din : in signed(N-1 downto 0);
    dout  : out signed(N-1  downto 0)
  );
end entity issue35;

architecture rtl of issue35 is

begin

   process (clk, rst) is
   begin
      if (rst = '1') then
         dout <= signed'(N-1 downto 0 => '0');
      elsif (clk'event and clk = '1') then
         dout <= din;
      end if;
    end process;


end rtl;
