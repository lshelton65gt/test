-- filename: test/vhdl/lang_misc/issue75.vhd
-- Description:  This test duplicates the simulation mismatch seen in Customer
-- 2A design file.  probably due to use of unsupported function std_match,
-- see also issue76.vhd


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

package issue75_package is
  function selector(choices : std_logic_vector(1 downto 0); previously_selected : std_logic) return std_logic;
  function selector(choices : std_logic_vector; previously_selected : std_logic_vector) return std_logic_vector;
  function selector5(choices : std_logic_vector(4 downto 0); previously_selected : std_logic_vector(2 downto 0)) return std_logic_vector;  
end package;
package body issue75_package is

  function selector(choices : std_logic_vector(1 downto 0); previously_selected : std_logic) return std_logic is
    variable selected_choice : std_logic_vector(0 downto 0);
  begin
    selected_choice := selector(choices, conv_std_logic_vector(previously_selected, 1));
    return selected_choice(0);
  end;

  function selector(choices : std_logic_vector; previously_selected : std_logic_vector) return std_logic_vector is
    constant NUM_CHOICES         : integer := choices'length;
    variable bit_to_check    : integer;
    function create_mask(position : integer) return ieee.numeric_std.unsigned;
    function create_mask(position : integer) return ieee.numeric_std.unsigned is
      variable vector : ieee.numeric_std.unsigned(NUM_CHOICES-1 downto 0);
    begin
      vector := (others => '-');
      vector(position) := '1';
      return vector;
    end;
    
    variable mask : std_logic_vector(NUM_CHOICES-1 downto 0);
    variable selected_choice    : integer;
  begin

    selected_choice := conv_integer(previously_selected);
    
    for outer_choice in 0 to NUM_CHOICES-1 loop
      if outer_choice = conv_integer(previously_selected) then
        for inner_choice in NUM_CHOICES-1 downto 1 loop
          bit_to_check := (inner_choice+outer_choice) mod NUM_CHOICES;
          mask := std_logic_vector(create_mask(bit_to_check));
          
          if std_match(choices, mask) then
            selected_choice := bit_to_check;
          end if;
        end loop;
        
      end if;
    end loop;
    return conv_std_logic_vector(selected_choice, previously_selected'length);
  end;

  function selector5(choices : std_logic_vector(4 downto 0); previously_selected : std_logic_vector(2 downto 0)) return std_logic_vector is
    variable selected_choice : std_logic_vector(previously_selected'range);
  begin
    selected_choice := selector(choices, previously_selected);
    return selected_choice;
  end;

end issue75_package;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.issue75_package.all;

entity issue75 is
  generic (
    WIDTH : integer := 8;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1      : in std_logic_vector(4 downto 0);
    in3      : in integer;
    out1     : out integer);
end;

architecture arch of issue75 is


begin

  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1 <=  conv_integer(unsigned(selector5(in1, conv_std_logic_vector(in3,3))));
    end if;
  end process;

end;
