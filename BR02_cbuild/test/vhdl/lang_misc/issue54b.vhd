-- This is a reproducer for customer 2 issue 54
-- NUExpr.cxx:1172 NU_ASSERT(!net->is2DAnything()) failed
--
library ieee;
library work;
use ieee.std_logic_1164.all;

package destypes is
   type vec_array is array (natural range <>) of std_logic_vector(1 downto 0);
   type vec_matrix is array (natural range <>) of vec_array(1 downto 0);
   type rec is record
      valid : bit;
      data :  bit_vector(1 downto 0);
   end record;
   type rec_array is array (natural range <>) of rec;
end package;



library ieee;
library work;
use ieee.std_logic_1164.all;
use work.destypes.all;

entity issue54b is
   port(sel    : in integer range 0 to 3;
        vin  : in  vec_array(1 downto 0);
        min  : in vec_matrix(1 downto 0);
        -- Records cause incorrect simulation results
        -- as of r2379, so are not included in the test
        -- rin  : in rec_array(1 downto 0);
        bin  : in bit;
        bout : out bit;
        bout2 : out bit;
        bout3 : out bit;
        mout : out vec_matrix(1 downto 0);
        -- rout1 : out rec_array(1 downto 0);
        -- rout2 : out rec_array(1 downto 0);
        vout : out  vec_array(1 downto 0)
        );
end issue54b;
architecture rtl of issue54b is
  signal const_zero : vec_array(1 downto 0) := (others =>(others => '0'));
  signal const_matrix_zero : vec_matrix(1 downto 0) := (others=> (others =>(others => '0')));
  signal const_matrix_one : vec_matrix(1 downto 0) := (others=> (others =>(others => '1')));
  signal const_one : vec_array(1 downto 0) := (others =>(others => '1'));
  -- signal rec_zero : rec_array(1 downto 0) := (others => ('1', "10")); 
begin

   -- This used to cause: NUExpr.cxx:1172 NU_ASSERT(!net->is2DAnything()) failed
   vout <= vin when (sel = 0) else const_zero;

   -- Verify that >2 d memories are populated as nested if's.
   mout <= min when (sel = 0) else const_matrix_zero when (sel = 1) else const_matrix_one when (sel = 2);

   -- Verify that records and record arrays are populated as nested if's
   -- rout1(0) <= rin(1) when (sel = 0) else rin(1) when (sel = 1) else rin(0);
   -- rout1(1) <= rin(0) when (sel = 0);
   -- rout2 <= rin when (sel = 0) else rec_zero;

   -- This used to cause this error from the Verific nucleus populator: 
   -- issue54b.vhd:38: Error 3256: should be one
   -- It shold be populated as nested ifs, with no final else.
   vout <= vin when (sel = 0) else const_zero when (sel = 1) else const_one when (sel = 2);


   -- This simple form (without memories) should be populated as an if with no else.
   bout <= bin when (sel = 0) else '0';

   -- This should be populated as a nested ternary expression
   bout2 <= bin when (sel = 0) else '0' when (sel = 1) else '1';

   -- Verific produces this error: issue54b.vhd:38: Error 3256: should be one
   -- Should be populated as a nested if 
   bout3 <= bin when (sel = 0) else '0' when (sel = 1) else '1' when (sel = 2);

end rtl;
