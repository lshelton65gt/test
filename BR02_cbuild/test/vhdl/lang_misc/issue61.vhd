-- Customer 2 issue 61 memory leak reproducer
--
-- This produces a variety of memory leaks related to the 
-- selected signal assignment (VhdlSelectedSignalAssignment visitor)
library ieee;
use ieee.std_logic_1164.all;
entity issue61 is
port( 
      a : in std_logic_vector(3 downto 0);
      c : out std_logic
     );
end issue61;

architecture rtl of issue61 is
 
begin

 with a select c <= '1' when "0001" | "0010" | "0100" | "1000", '0' when others;

end rtl;
