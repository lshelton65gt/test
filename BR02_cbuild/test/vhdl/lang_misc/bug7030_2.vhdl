-- bug7030_2
-- Neither the library nor the use clause are present for the pack_lib
-- (they are commented out). The full path to sub2only for s3 is defined.
-- The cbuild options direct to compile vhdl file into pack_lib
-- "-vhdlLib pack_lib bug7030package.vhdl".
-- The library clause "library pack_lib" is commented out intentionally.
-- The right behavior for the Parser is to fail compiling with error "undefined
-- pack_lib".

-- Modelsim - doesn't compile, complains about unknown component for s3.
library ieee;
use ieee.std_logic_1164.all;


library sub1_lib;
--use sub1_lib.all;
library sub2_lib;
--use sub2_lib.all;
--library pack_lib;
--use pack_lib.subPack.all;


entity bug7030_2 is
  
  port (
    clk  : in  std_logic;
    in1  : in  std_logic;
    out1 : out std_logic;
    out2 : out std_logic;
    out3 : out std_logic);
  

end bug7030_2;


architecture arch of bug7030_2 is

--  component sub
--    port (
--      in1  : in  std_logic;
--      out1 : out std_logic);
--  end component;
 
begin  -- arch

  s1 : entity sub1_lib.sub port map (
    clk  => clk,
    inS  => in1,
    outS => out1);

  s2 : entity sub2_lib.sub  port map (
    clk  => clk,
    inS  => in1,
    outS => out2);

--  s3 : sub2only  port map (
    s3 : pack_lib.subPack.sub2only  port map (
    clk  => clk,
    inS  => in1,
    outS => out3);

end arch;

