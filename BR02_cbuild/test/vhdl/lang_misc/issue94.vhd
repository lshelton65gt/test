-- filename: test/vhdl/lang_misc/issue94.vhd
-- Description:  This test duplicates an error seen in Imagination Rogue design
-- at simulation time related to a signal not being a net:
-- Error 5055: issue94.current_address[3] is a valid design path, but not a net.
-- run this with -testdriverDumpFSDB, error is seen during simulation step


library ieee;
use ieee.std_logic_1164.all;

entity issue94 is
  generic (
    WIDTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector((WIDTH-1) downto 0);
    out1     : out integer);
end;

architecture arch of issue94 is
  type integer14_array is array (integer range <>) of integer range 0 to 13;
  type integer14_array2 is array (integer range <>) of integer14_array(1 downto 0);

  signal n_current_address, current_address : integer14_array2(WIDTH-1 downto 0);
  constant i0 : integer := 0;
  constant i1     : integer := 1;

begin
  main: process (clock)
  begin 

    if clock'event and clock = '1' then
      out1 <= current_address(i0)(i1);
    end if;
  end process;

  c: process (current_address, n_current_address, in1)
    variable val : integer range 0 to 3;
  begin
    if (in1(0) = '1') then
       val := 1;
    else 
       val := 2;
    end if;
    n_current_address(i0)(i1) <= val;
  end process;

  current_address <= n_current_address;  
end;
