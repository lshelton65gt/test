-- Reproducer for customer 2 issue 63
-- Assertion: CARBON_INTERNAL_ERROR
-- Inconsistency between isArrayEmpty and branchIsLive
-- VerificVhdlStatementWalker.cpp:1298 INFO_ASSERT(!short_circuited) failed
--
library ieee;
use ieee.std_logic_1164.all;

package typedecls is

  constant N1 : integer := 2;
  constant V1 : std_logic_vector(3 downto 0) := X"1";

  function func1(i : integer; v1 : std_logic_vector(3 downto 0); v2 : std_logic_vector(3 downto 0)) return std_logic_vector;

end;

package body typedecls is

  function func1(i : integer; v1 : std_logic_vector(3 downto 0); v2 : std_logic_vector(3 downto 0)) return std_logic_vector is
    variable result : std_logic_vector(3 downto 0);
  begin
    if (i = 1) then
      result := "0001";
    elsif (v1 = "0001") then
      result := v2;
    else
      result := "0000";
    end if;
    return result;
  end;

end package body;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;
use work.typedecls.all;
entity issue63 is
port( clk : in std_logic;
      a : in std_logic_vector(3 downto 0);
      c : out unsigned(3 downto 0)
     );
end issue63;

architecture rtl of issue63 is
begin

  process (a) is
     constant foo : unsigned(3 downto 0) := "0001";
  begin
     if (foo = unsigned(func1(N1, V1, a))) then
        c <= unsigned(a);
     end if;
  end process;

end rtl;
