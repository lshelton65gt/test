-- File is a copy of test/vhdl/records-new/nestedrecord4a.vhd and can be removed 
-- as soon as test/vhdl/records-new will be on regular run of regressions

-- Test whole nested record assignment, both concurrent and sequential.
-- Also split the record defs between a package and the architecture.
-- This tests whole sub-record assignment, not whole top-level record assignment.
package pack is
  type bottomrec is record
                      b1     : bit;
                      t2     : bit_vector(3 downto 0);  -- reuse field name
                    end record;

end pack;

use work.pack.bottomrec;

package pack1 is
  type midrec is record
                   mid_nest1 : bottomrec;
                   m2        : bit;
                   mid_nest2 : bottomrec;  -- multiple instance of the same
                                           -- record type
                 end record;
end pack1;

use work.pack1.midrec;

entity issue58 is
  port (
    clk: in bit;
    reset: in bit;
    inp : in bit_vector(15 downto 0);
    outp : out bit_vector(15 downto 0));
end issue58;

architecture a of issue58 is
  type toprec is record
                   t1        : bit_vector(1 to 2);
                   top_nest1 : midrec;
                   t4        : bit_vector(2 to 4);
                 end record;

  signal s1, s2, s3 : toprec;
  
begin

  p2: process (s1.t4(3),s3)
  begin
    if s1.t4(3) = '1' then
      s2.t1 <= s3.t1;
      s2.top_nest1 <= s3.top_nest1;
      s2.t4 <= s3.t4;
    end if;
  end process p2;

  s3.top_nest1 <= s1.top_nest1;
  s3.t1 <= s1.t1;
  s3.t4 <= s1.t4;
  
  p1: process (clk, reset)
  begin
    if reset = '0' then
      s1.t1 <= "00";
      s1.t4 <= (others => '1');
      s1.top_nest1.m2 <= '0';
      s1.top_nest1.mid_nest1.b1 <= '0';
      s1.top_nest1.mid_nest2.b1 <= '0';
      s1.top_nest1.mid_nest1.t2 <= X"F";
      s1.top_nest1.mid_nest2.t2 <= B"1_1_1_1";
    elsif clk'event and clk = '1' then
      s1.t1 <= inp(15 downto 14);
      s1.t4 <= inp(13 downto 11);
      s1.top_nest1.m2 <= inp(10);
      s1.top_nest1.mid_nest1.b1 <= inp(9);
      s1.top_nest1.mid_nest2.b1 <= inp(8);
      s1.top_nest1.mid_nest1.t2 <= inp(6 downto 3);
      s1.top_nest1.mid_nest2.t2 <= inp(2 downto 0) & inp(7);
    end if;
  end process p1;

  outp <= s2.t4 &
          s2.top_nest1.m2 &
          s2.top_nest1.mid_nest1.b1 &
          s2.top_nest1.mid_nest2.t2(2) &
          s2.top_nest1.mid_nest2.b1 &
          s2.top_nest1.mid_nest2.t2(1 downto 0) &
          s2.top_nest1.mid_nest2.t2(3) &
          s2.top_nest1.mid_nest1.t2 &
          s2.t1;
end a;
