library ieee;
--use ieee.numeric_bit.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_1164.all;

entity conv_func is
  port(in1: in unsigned(0 to 7); out1: out std_logic_vector(0 to 7));
end;

architecture arch of conv_func is

begin
  out1 <= conv_std_logic_vector(abs(signed(in1)),8);
end;

