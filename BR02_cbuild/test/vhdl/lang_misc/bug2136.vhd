entity sub is
  
  port (
    goingin   : in  bit;
    comingout : out bit);

end sub;

architecture a of sub is

begin

  comingout <= goingin;

end a;

entity bug2136 is
  port(topin1 : in bit;topout1 : out bit);
end bug2136;

architecture arch of bug2136 is
  component sub
    port (
      comingout : out bit;
      goingin : in  bit);
  end component;
  
begin  -- a

  u_sub1: sub
    port map (
      comingout => topout1,
      goingin => topin1);

 
end arch;

