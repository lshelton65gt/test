-- May 2007
-- Tests the attributes simple_name, path_name and instance_name of named items
-- These attributes are not supported.

entity attr5 is
port (in1 : in integer;
      out1 : out integer;
      out2 : out integer
     );
end attr5;

architecture arch_attr5 of attr5 is
  signal temp : integer := 100;
  signal strVar1 : string(1 to 4);
  signal strVar2 : string(1 to 11);
  signal strVar3 : string(1 to 23);
begin

  strVar1 <= temp'simple_name;          -- fails
  strVar2 <= temp'path_name;            -- fails
  strVar3 <= temp'instance_name;        -- fails
  
end arch_attr5;
