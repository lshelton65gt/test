-- last mod: Wed Sep 13 15:41:40 2006
-- filename: test/vhdl/lang_misc/bug6585_h.vhd
-- Description:  Same as bug6585_g.vhd, except that the function now computes the constant length.


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6585_h is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector(15 downto 0);
    out1     : out std_logic_vector(15 downto 0));
end;

architecture arch of bug6585_h is 

function level2 (v : std_logic_vector; argsize_left : integer) return std_logic_vector;
function level1 (v : std_logic_vector) return std_logic_vector;

function level2 (v : std_logic_vector; argsize_left : integer) return std_logic_vector is
  constant argsize : integer := argsize_left + 1;
  constant argsize_minus_1 : integer := argsize - 1;
  subtype rtype is std_logic_vector(argsize_minus_1 downto 0);
  variable retval : rtype;
begin
  retval := level1(v);
  return retval;
end level2;


function level1 (v : std_logic_vector) return std_logic_vector is
  constant argsize : integer := v'length;
  constant argsize_minus_1 : integer := argsize - 1;
  subtype rtype is std_logic_vector(argsize_minus_1 downto 0);
  variable retval : rtype;
begin 
  retval := sxt(v, argsize);
  return retval;
end level1;

-- purpose: Returns length of vector argument
function getLength ( v : std_logic_vector) return integer is
begin  -- getLength
  return v'length;
end getLeft;

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
     for i in 3 downto 0 loop
       out1(3+4*i downto 4*i) <= level2(in1(3+4*i downto 4*i), getLength(in1(3+4*i downto 4*i)) - 1);
     end loop;  -- i
    end if;
  end process;
end;
