library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug3519 is
  port (in1,in2 : in signed( 0 to 1); out1 : out signed(0 to 1));
end;

architecture arch of bug3519 is
begin
  process(in1,in2)
  begin
    case in1 > in2 is
      when true => out1 <= "11";
      when others => out1 <= "00";
    end case;
  end process;
end;

