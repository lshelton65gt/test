-- This testcase reproduces issue 55 seen in customer 2 design
-- Failure mode: CARBON INTERNAL ERROR, fixme, 
-- VerificVhdlExpressionWalker.cpp:1026 INFO_ASSERT(false)

package destypes is

   type fullword is
     record
       valid : bit;
       up : bit_vector(15 downto 0);
       down : bit_vector(15 downto 0);
     end record;

   function make_fullword(v : bit_vector(31 downto 0)) return fullword;

end package;

package body destypes is

   function make_fullword(v : bit_vector(31 downto 0)) return fullword is
     variable q : fullword;
   begin
     q.valid := '1';
     q.up := v(31 downto 16);
     q.down := v(15 downto 0);
     return q;
   end;

end destypes;

use work.destypes.all;
entity issue55 is
  port (
    in1 : in bit_vector(31 downto 0);
    out1 : out bit_vector(31 downto 0);
    out2 : out bit
  );
end issue55;

architecture rtl of issue55 is
begin
   process (in1) is
   begin 
      out1 <= (make_fullword(in1).up & make_fullword(in1).down);
      out2 <= make_fullword(in1).valid;
   end process;

end rtl;

