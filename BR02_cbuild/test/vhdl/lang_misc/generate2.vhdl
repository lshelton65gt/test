library ieee;
use ieee.std_logic_1164.all;

entity bottom is
  generic (
    g1 : integer;
    g2 : integer);
  port (
    in1  : in  std_logic_vector(2 downto 0);
    out1 : out std_logic_vector(2 downto 0));
end bottom;

architecture arch of bottom is

begin

  gen1: for i in 0 to g1/g2 - 1 generate
    out1(i) <= in1(i);
  end generate gen1;

  out1(2 downto 1) <= in1(2 downto 1);
  
end arch;


library ieee;
use ieee.std_logic_1164.all;

entity generate2 is
  port (
    in1  : in  std_logic_vector(2 downto 0);
    out1 : out std_logic_vector(2 downto 0));
end generate2;

architecture arch of generate2 is

  component bottom
    generic (
      g1 : integer;
      g2 : integer);
    port (
      in1  : in  std_logic_vector(2 downto 0);
      out1 : out std_logic_vector(2 downto 0));
  end component;
  
begin

  u1 : bottom 
  generic map (
    g1 => 8,
    g2 => 9)
  port map (
    in1  => in1,
    out1 => out1);
end arch;
