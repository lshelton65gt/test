-- Similar to bug7566_1 testcase. Tests to ensure that values of attributes on 
-- signed ranges are correctly marked as signed.
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY bug7566_2 IS

  PORT (
    in2               : in std_logic;
    out2              : out std_logic;
    out1		   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
    );

END bug7566_2;

ARCHITECTURE synth OF bug7566_2 IS

  SUBTYPE  subtype1 is integer RANGE 16 DOWNTO -15;
  CONSTANT const4 : INTEGER := subtype1'HIGH;

  FUNCTION LSL	(L: STD_LOGIC_VECTOR; R: INTEGER)	RETURN STD_LOGIC_VECTOR IS
    VARIABLE TEMP: STD_LOGIC_VECTOR(L'length-1 DOWNTO 0);
    VARIABLE RSLT: STD_LOGIC_VECTOR(L'length-1 DOWNTO 0);
  BEGIN
    TEMP := L;
    RSLT := (others => '0');
    for i in R to L'length-1 loop
      RSLT(i) := TEMP(i-R);
    END loop;
    RETURN RSLT;
  END;

  SIGNAL sig1   : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";

BEGIN
  out1 <= LSL (sig1, const4);
  out2 <= in2;

END synth;
