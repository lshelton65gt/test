-- This test exposed a segfault on exit statements inside an unrolled loop 
-- in the Verific flow.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
entity exit1 is
port (
        clk : in std_logic;
        din : in std_logic_vector(7 downto 0);
        dout: out std_logic_vector(7 downto 0)
);
end exit1;

architecture rtl of exit1 is
function foobar(n : integer) return integer is
begin
   if n > 5 then
     return n + 1;
   else
     return n - 1;
   end if;
end foobar;
begin
   process (clk)
   begin
   if clk'event and clk = '1' then
      for i in 0 to 7 loop
        if foobar(i) = 8 then
           dout(i) <= din(0);
           exit;
        end if;
        dout(i) <= din(i);
      end loop;
   end if;
   end process;
end architecture rtl;
