-- bug7301
-- Test that we do not get a codegen error due to generating
-- "x op= b" for "x = x op b" when x is generated as Extender<...>
-- on the rhs

library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;
entity bug7301_01 is
  port (
    clock    : in  std_logic;
    in1, in2 : in  SIGNED(30 downto 0);
    out1     : out SIGNED(61 downto 0));
end;

architecture arch of bug7301_01 is
    function do_shift(A,B: SIGNED) return SIGNED is
      variable result: SIGNED((A'length+B'length-1) downto 0);
      constant one : UNSIGNED(1 downto 0) := "01";
    begin
        result := CONV_SIGNED(('0' & ABS(B)),(A'length+B'length));
        for i in 0 to A'length-1 loop
          result := SHL(result,one);
        end loop;
        return(result);
    end do_shift;

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
      out1 <= do_shift(in1, in2);
    end if;
  end process;
end;
