-- This reproduces issue 9 with the customer 2 testcase
-- ******CARBON INTERNAL ERROR*******
--
-- NUCompositeFieldExpr not yet supported in arraySliceWithConstIndexes
-- /home/cds/ricks/dev/br02_cust/src/verific2nucleus/VerificVhdlUtil.cpp:1520 INFO_ASSERT(false) failed

package destypes is

   type halfword is
     record
       hi : bit_vector(7 downto 0);
       lo : bit_vector(7 downto 0);
     end record;

   type halfword_array is array(3 downto 0) of halfword;

end package;

use work.destypes.all;
entity issue9 is
  port (
    clk : in bit;
    rst : in bit;
    hi  : in bit_vector(7 downto 0);
    lo  : in bit_vector(7 downto 0);
    hw  : out bit_vector(7  downto 0)
  );
end entity issue9;

architecture rtl of issue9 is

signal mem : halfword_array;     

begin

   process (clk, rst) is
     variable x : natural range 3 downto 0 := 0;
   begin
      if (rst = '1') then
         x := 0;
         mem(3).hi <= "00001111";
         mem(3).lo <= "10110101";
         mem(2).hi <= "00001111";
         mem(2).lo <= "11010101";
         mem(1).hi <= "00001111";
         mem(1).lo <= "01111010";
         mem(0).hi <= "00001101";
         mem(0).lo <= "11110001";
      elsif (clk'event and clk = '1') then
         if (x = 3) then
            x := 0;
         else
            x := x + 1;
         end if;
         hw <= mem(x).hi(3 downto 0) & mem(x).lo(7 downto 4);
      end if;
    end process;

end rtl;

   
