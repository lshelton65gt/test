-- June 2008
-- This file tests initialization of signal/constant, which is of type of array
-- of enumeration.
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

PACKAGE ex_pkg IS
    TYPE mbit_t IS (
        '0', -- compare with constant '0'
        '1', -- compare with constant '1'
        '-'  -- don't care (skip bit for comparison)
    );

    TYPE mvec_t IS ARRAY (natural RANGE <>) OF mbit_t;
   CONSTANT cme_i_nop : mvec_t(7 DOWNTO 0) := "-01-00-1";  -- this line is tested
END ex_pkg;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use work.ex_pkg.all;

entity bug8966_01 is
  
  port (
    clk    : in bit;
    in1    : in  integer range 0 to 7;
    out1   : out integer range 0 to 2);

end bug8966_01;

architecture arch of bug8966_01 is
  signal s1 : mvec_t(7 DOWNTO 0) := cme_i_nop;
  signal s2 : mbit_t := '0';
  signal t1 : integer range 0 to 2 := 0;
begin  -- arch

  p1: process (clk, s2)
  begin  -- process p1
    if clk'event and clk = '1' then 
      s2 <= s1(in1);
    end if;

    case s2 is
      when '0' => t1 <= 0 ;
      when '1' => t1 <= 1 ;
      when '-' => t1 <= 2 ;
      when others => t1 <= 0;
    end case;
  end process p1;

  out1 <= t1;

end arch;
