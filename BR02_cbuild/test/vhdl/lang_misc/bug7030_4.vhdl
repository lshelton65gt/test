-- bug7030_4 - In this design we use library specifications sub1_lib and sub2_lib.
-- In addition there are use clauses sub1_lib.all and sub2_lib.all Each of these
-- libraries contain an entity called sub, so there is a name conflict and
-- potential ambiguity about which entity, named sub, should be used. The parser
-- should not be able to resolve a reference to "sub". In valid VHDL design a
-- full path to resolve this ambiguity is required.


library ieee;
use ieee.std_logic_1164.all;


library sub1_lib;
use sub1_lib.all;
library sub2_lib;
use sub2_lib.all;

entity bug7030_4 is
  
  port (
    clk  : in  std_logic;
    in1  : in  std_logic;
    out1 : out std_logic;
    out2 : out std_logic);
end bug7030_4;

architecture arch of bug7030_4 is

begin  -- arch

  s1 : entity sub port map (
    clk  => clk,
    inS  => in1,
    outS => out1);

  s2 : entity sub  port map (
    clk  => clk,
    inS  => in1,
    outS => out2);

end arch;
