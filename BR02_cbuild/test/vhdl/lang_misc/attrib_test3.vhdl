-- Tests use of 'left, 'right attributes on a variable range ..but constant
-- width, input vector to the function. The 'left-'right ..i.e width-1, is used
-- to declare a vector with that left bound.
entity attrib_test3 is
  port (
    inp  : in  bit_vector(15 downto 0);
    outp : out bit_vector(3 downto 0));
end attrib_test3;

architecture arch of attrib_test3 is

function foo (v : bit_vector) return bit is
  variable foo_vect : bit_vector(v'left - v'right downto 0);
  variable foo_int : integer;
begin
  foo_vect := v;
  foo_int := v'length;
  return foo_vect(foo_int - 1 );
end foo;
  
begin
  process (inp)
  begin
    for i in 3 downto 0 loop
      outp(i) <= not FOO( inp(4*i+3 downto 4*i) );
    end loop;
  end process;
end arch;
