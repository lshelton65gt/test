-- Reproduces bug6787, where VHRANGE of 'RANGE attribute type results in
-- an error. The population covered VHRANGE of non-attribute type, however
-- missed the attribute type.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sub_test is
  
  generic (
    words : integer;
    abits : integer;
    bits  : integer);

  port (
    datain   : in std_logic_vector(bits-1 downto 0);
    wrenable : in std_logic_vector(words-1 downto 0);
    addr     : in std_logic_vector(abits-1 downto 0);
    dataout  : out std_logic_vector(bits-1 downto 0));

end sub_test;

architecture arch of sub_test is
  type memtype is array (wrenable'range) of std_logic_vector(datain'range);
-- Following is the workaround:
--  type memtype is array (words-1 downto 0) of std_logic_vector(bits-1 downto 0);
  signal mem : memtype := (others => (others => '0'));
  shared variable intAddr : integer := 0;
  
begin  -- arch

  -- purpose: memory write
  proc: process (datain, addr, wrenable, mem)
  begin  -- process proc
    intAddr := to_integer(unsigned(addr));
    if (wrenable(intAddr) = '1') then
      mem(intAddr) <= datain;
    end if;
    -- memory read
    dataout <= mem(intAddr);
  end process proc;
  
end arch;

library ieee;
use ieee.std_logic_1164.all;

entity attrib_test5 is
  
  port (
    datain   : in std_logic_vector(3 downto 0);
    wrenable : in std_logic_vector(31 downto 0);
    addr     : in std_logic_vector(4 downto 0);
    dataout  : out std_logic_vector(3 downto 0));

end attrib_test5;

architecture arch of attrib_test5 is
component sub_test
  generic (
    words : integer;
    abits : integer;
    bits  : integer);
  port (
    datain   : in std_logic_vector(bits-1 downto 0);
    wrenable : in std_logic_vector(words-1 downto 0);
    addr     : in std_logic_vector(abits-1 downto 0);
    dataout  : out std_logic_vector(bits-1 downto 0));
end component;
  
begin  -- arch

  sub1 : sub_test
    generic map (words => 32, abits => 5, bits  => 4)
    port map (datain   => datain, wrenable => wrenable, addr => addr, dataout => dataout);

end arch;
