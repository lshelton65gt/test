-- filename: test/vhdl/lang_misc/issue65.vhd
-- Description:  This test duplicates the issue 65 seen in customer2A design file
-- issue is a backend compile failure about a variable not being declared in
-- this scope.


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

package issue65_pkg is
  function function1(constant arg: std_logic_vector; constant size: natural) return std_logic_vector;
  function function2(input_vector : std_logic_vector) return std_logic;
end;

package body issue65_pkg is

  function function1(constant arg : std_logic_vector;
                     constant size: natural) return std_logic_vector is
  begin
    return std_logic_vector(resize(ieee.numeric_std.unsigned(arg), size));
  end function1; 

  function function2(input_vector : std_logic_vector) return std_logic is
    variable v_in_vec_log2 : std_logic_vector(3 downto 0) := function1(input_vector, 4);
  begin
    return (v_in_vec_log2(1));
  end function2;
end;

library ieee;
use ieee.std_logic_1164.all;
use work.issue65_pkg.all;

entity issue65 is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector((WIDTH-1) downto 0);
    out1     : out std_logic_vector(0 downto 0));
end;

architecture arch of issue65 is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1(0) <= function2(in1);
    end if;
  end process;
end;
