library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity bug2140 is
      generic(
              w : integer -- w-bits adder
             );
      port(
           a : in  std_logic_vector(w-1 downto 0); -- Input a
           b : in  std_logic_vector(w-1 downto 0); -- Input a
           c : out std_logic_vector(w-1 downto 0)  -- Output c
          );
end bug2140;
-----------------------------------------------------------------
architecture behav of bug2140 is
begin
c <= a + b;
end behav;

