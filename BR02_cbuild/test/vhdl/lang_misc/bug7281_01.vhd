-- last mod: $Date: 2007/05/21 19:23:00 $
-- filename: test/vhdl/lang_misc/bug7281_01.vhd
-- Description:  This test duplicates the problem seen in bug 7281
-- Check that we do not generate an invalid static_cast for the WHEN (a = b) construct
-- where both a and b are 2d memories. In this test, one operand is a constant, the
-- other is a signal

library ieee;
use ieee.std_logic_1164.all;

package pack_7281_01 is
  subtype array1 is std_logic_vector (1 downto 0);
  type array_array1 is array (natural range <>) of array1;
  constant zero  : array1 := (others => '0');
end pack_7281_01;

library ieee;
use ieee.std_logic_1164.all;
use work.pack_7281_01.all;

entity bug7281_01 is
  port (
    clock    : in std_logic;
    in1_1    : in work.pack_7281_01.array1;
    in1_0    : in work.pack_7281_01.array1;
    out3     : out std_logic;
    out2     : out std_logic;
    out1     : out std_logic;
    out0     : out std_logic);
end;

architecture arch of bug7281_01 is 
  signal signal1  : work.pack_7281_01.array_array1(1 downto 0);
  constant zero1  : work.pack_7281_01.array_array1(1 downto 0) := (others => zero);
begin
  out3 <= '0' when (zero1 = signal1) else '1';
  out2 <= '0' when (zero1 /= signal1) else '1';
  out1 <= '0' when (signal1 = zero1) else '1';
  out0 <= '0' when (signal1 /= zero1) else '1';
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      signal1 (0) <= in1_0;
      signal1 (1) <= in1_1;
    end if;
  end process;
end;

