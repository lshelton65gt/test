-- last mod: $Date: 2007/05/21 19:23:00 $
-- filename: test/vhdl/lang_misc/bug7281_02.vhd
-- Check that we do not generate an invalid static_cast for the WHEN (a = b) construct
-- where both a and b are 2d memories. In this test, both operands are signals.

library ieee;
use ieee.std_logic_1164.all;

package pack_7281_02 is
  SUBTYPE array1 IS std_logic_vector(1 DOWNTO 0);
  TYPE array_array1 IS ARRAY (NATURAL RANGE <>) OF array1;
  CONSTANT zero  : array1 := (OTHERS => '0');
end pack_7281_02;

library ieee;
use ieee.std_logic_1164.all;
use work.pack_7281_02.all;

entity bug7281_02 is
  port (
    clock    : in  std_logic;
    in1_0    : in work.pack_7281_02.array1;
    in1_1    : in work.pack_7281_02.array1;
    in0_0    : in work.pack_7281_02.array1;
    in0_1    : in work.pack_7281_02.array1;
    out3     : out std_logic;
    out2     : out std_logic;
    out1     : out std_logic;
    out0     : out std_logic);
end;

architecture arch of bug7281_02 is 
  SIGNAL signal1  : work.pack_7281_02.array_array1(1 DOWNTO 0);
  SIGNAL signal0  : work.pack_7281_02.array_array1(1 DOWNTO 0);
begin
  out3 <= '0' WHEN (signal0 /= signal1) ELSE '1';
  out2 <= '0' WHEN (signal0 = signal1) ELSE '1';
  out1 <= '0' WHEN (signal1 /= signal0) ELSE '1';
  out0 <= '0' WHEN (signal1 = signal0) ELSE '1';
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      signal1 (0) <= in1_0;
      signal1 (1) <= in1_1;
      signal0 (0) <= in0_0;
      signal0 (1) <= in0_1;
    end if;
  end process;
end;
