-- sim gold file generated with MTI.  Aldec gives all Z

-- this testcase is derived from test/cust/matrox/sunex/carbon/cwtb/demtop,
-- which fails in g++ with -newSizing due to left-shifting a 32-bit constant
-- by 32.  Without -newSizing, the constant is sized to 128 bits, which
-- is pessimistic.  We just need a 64 bit constant.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity dyn_slice9 is  
    port (
         data : in std_logic_vector(4*32 - 1 downto 0);
         control : in std_logic_vector(4 - 1 downto 0);
         dout : out std_logic_vector(32 - 1 downto 0)
         );
end dyn_slice9;

architecture dyn_slice9 of dyn_slice9 is
 begin

   process (data, control)
     variable index : integer := 0;
     variable low : integer;
     variable high : integer;

      begin
        -- Loop over the values of the control inputs
        for_loop : for i in control'range loop

          if ( control(i) = '1' ) then

            index := i;
            exit for_loop;

          end if;

        end loop;

        -- Store the corresponding data lines into the output
        low := 32 * index;
        high := low + 32 - 1;
        dout <= data( high downto low );
      end process;
end dyn_slice9;


