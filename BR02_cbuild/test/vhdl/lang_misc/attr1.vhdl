-- May 2007
-- Tests the attributes base and pos
-- 'base is supported.
-- 'pos is supported here, because parameters are static.


entity attr1 is
port (clk : bit;
      out1, out2 : out integer
     );
end attr1;

architecture arch_attr1 of attr1 is
  type color_typ is (Red, Yellow, Blue, Green );
  subtype mycolor is color_typ;
begin
  process (clk)
  begin
   if (clk'event and clk = '1') then
     out1 <= mycolor'base'pos(Yellow);
     out2 <= mycolor'base'pos(Green);
     
   end if;
  end process;
end arch_attr1;

         
