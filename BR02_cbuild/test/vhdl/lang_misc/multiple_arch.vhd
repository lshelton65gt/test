
entity foo is
  port(in1, in2 : in bit; out1 : out bit);
end;

architecture arch1 of foo is
begin
  out1 <= in1 xor in2;
end;

architecture arch2 of foo is
begin
  out1 <= in1 and in2;
end;

entity multiple_arch is
  port(in1, in2 : in bit_vector(0 to 1); out1 : out bit_vector(0 to 1) );
end;

architecture arch of multiple_arch is
component foo is
  port(in1, in2 : in bit; out1 : out bit);
end component;

begin
  inst1 : entity work.foo(arch1) port map(in1(0), in2(0), out1(0));
  inst2 : entity work.foo(arch2) port map(in1(1), in2(1), out1(1));
end;

