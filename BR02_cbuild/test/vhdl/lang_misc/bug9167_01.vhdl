-- August 2008
-- This file tests the function return, which is a slice of a vector. The
-- bounds of the slice are calculated based on the variable.
library ieee;
use ieee.std_logic_1164.all;

package func_pkg is
  constant PEL_W : natural  := 2;
  function ipb_pel(ipb : std_logic_vector; x : integer) return std_logic_vector;
end func_pkg;

package body func_pkg is

  function ipb_pel(ipb : std_logic_vector; x : integer)
  return std_logic_vector is
    variable origin : natural;
    variable lsb : natural;
  begin
    origin := 3;
    lsb := PEL_W*(origin-x);
    return ipb(lsb+PEL_W-1 downto lsb);  -- this line is tested
  end;
end func_pkg;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.func_pkg.all;

entity bug9167_01 is
  generic (
    WIDTH     : natural := 16);

  port (
    clk, rst     : in  std_logic;
    in1          : in  std_logic_vector(WIDTH-1 downto 0);
    out1         : out std_logic_vector(WIDTH-1 downto 0));
end bug9167_01;

architecture arch of bug9167_01 is
  signal temp : std_logic_vector(WIDTH-1 downto 0);
begin  -- arch

  p1: process (clk, rst)
  begin  -- process p1
    if rst = '0' then     
      temp <= (others => '1');
    elsif clk'event and clk = '1' then
      temp(1 downto 0) <= ipb_pel(in1, -2);
    end if;
  end process p1;
  out1 <= temp;

end arch;
