-- August 2008
-- This file was created based on the NXP design.
-- The issue was with concat of a vector with a bit.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity bug9164_01 is
  generic (
    PEL_W : positive := 4);

  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    in1  : in  std_logic_vector(PEL_W*2-1 downto 0);
    in2  : in  std_logic_vector(PEL_W*2-1 downto 0);
    out1 : out std_logic_vector(PEL_W*2-1 downto 0));

end;

architecture arch of bug9164_01 is
    signal round_vec, add_out : unsigned(PEL_W*2-1 downto 0);
begin  -- arch

  p1: process (clk, rst)
  begin  -- process p1
    if rst = '0' then         
      round_vec <= (others => '1');
      add_out <= (others => '0');
    elsif clk'event and clk = '1' then 
      for i in 0 to 1 loop
        -- the line below is tested
        add_out <= round_vec
                   + unsigned('0' & in1((i+1)*PEL_W-1 downto i*PEL_W))
                   + unsigned('0' & in2((i+1)*PEL_W-1 downto i*PEL_W)); 
                                                                         
      end loop;
    end if;
  end process p1;
  out1 <= std_logic_vector(add_out);

end arch;
