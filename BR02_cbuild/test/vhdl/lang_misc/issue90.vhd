-- filename: test/vhdl/lang_misc/issue90.vhd
-- Description:  This test duplicates issue90 of Customer2A a CARBON INTERNAL
-- ERROR related to creating a extendedExpr for resize(signed'("1"))


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity issue90 is
  generic (
    WIDTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector((WIDTH-1) downto 0);
    out1     : out std_logic_vector(1 downto 0);
    out5     : out std_logic_vector(4 downto 0);
    out3     : out std_logic_vector(3 downto 0));
end;

architecture arch of issue90 is 
   function asll(a : signed; s : integer) return std_logic_vector is
   begin
      return std_logic_vector((unsigned(a) sll s));
   end function asll;

begin 
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
     out1 <= "00";
     out5 <= "00000";
     out3 <= "0000";
     if SIGNED(in1) < SIGNED'("0000") then 
       out1 <= asll(resize(signed'("1"), 2), 1);
       out5 <= asll(resize(signed'("101010"), 5), 0); --should produce "11010"
       out3 <= asll(resize(signed'("10"), 4), 1);    --should produce "1100"
     end if;
    end if;
  end process;
end;
