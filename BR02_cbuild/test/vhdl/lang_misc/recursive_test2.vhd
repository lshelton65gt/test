-- This recursive testcase is derived from bug8924.vhd, which is a Customer 2 testcase. The Interra flow
-- does not support it, but the Verific flow handles it ok.
-- This is a reduced version of the testcase.
library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.ext;
use ieee.std_logic_arith.sxt;
use ieee.std_logic_misc.all;

entity recursive_test2 is
   port (
     input1  : in  std_logic_vector(5 downto 0);
     output1  : out std_logic_vector(3 downto 0)
     );
end recursive_test2;

architecture rtl of recursive_test2 is

   function extract_recursive(a : std_logic_vector; v : integer) return std_logic_vector is
      constant w     : integer := 2**v;
      variable a_temp   : std_logic_vector(w-1 downto 0);
      variable b_temp   : std_logic_vector((w/2)-1 downto 0);
      variable shift : std_logic_vector(0 downto 0);
   begin
      a_temp := ext(a,w);
      if or_reduce(a_temp(w-1 downto w/2)) = '1' then
         b_temp      := a_temp(w-1 downto w/2);
         shift(0) := '1';
      else
         b_temp      := a_temp(w/2-1 downto 0);
         shift(0) := '0';
      end if;
      if (w = 2) then
         return (shift);                -- Return size is (0 to 0) or 1
      else
         return (shift & extract_recursive(b_temp, v-1));
      end if;
   end extract_recursive;

begin

  output1 <= extract_recursive(input1, 4);

end rtl;
