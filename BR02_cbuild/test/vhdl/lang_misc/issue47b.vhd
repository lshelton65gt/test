-- Test out multi-level record arrays.
-- This demonstrates a simulation difference between Interra/MTI and
-- Verific. Interra and MTI agree.
-- I think the issue is that we are not distinguishing
-- record array elements correctly. E.G.
--
-- a(0).up.hi
-- 
-- is probably being treated the same as
--
-- a(1).up.hi
--
-- This is not confirmed, though, and more investigation is
-- required.

package destypes is

   type halfword is
     record
       hi : bit_vector(7 downto 0);
       lo : bit_vector(7 downto 0);
     end record;

   type fullword is
     record
       valid : bit;
       up : halfword;
       down : halfword;
     end record;

   type fullwordarray is array(1 downto 0) of fullword;

end package;

use work.destypes.all;
entity issue47b is
  port (
    in1 : in bit_vector(31 downto 0);
    out1 : out bit_vector(31 downto 0)
  );
end issue47b;

architecture rtl of issue47b is
begin
   process (in1) is
     variable a : fullwordarray;
   begin
      a(1).up.hi := in1(31 downto 24);
      a(1).up.lo := in1(23 downto 16);
      a(1).down.hi := in1(15 downto 8);
      a(1).down.lo := in1(7 downto 0);
      a(0).up.hi := in1(7 downto 0);
      a(0).up.lo := in1(15 downto 8);
      a(0).down.hi := in1(23 downto 16);
      a(0).down.lo := in1(31 downto 24);
      out1 <= (a(1).up.hi or a(0).up.hi) & (a(1).up.lo or a(0).up.lo) & (a(1).down.hi or a(0).down.hi) & (a(1).down.lo or a(0).down.lo);
   end process;

end rtl;

