-- last mod: Fri May  4 08:26:44 2007
-- filename: test/vhdl/lang_misc/bug7300.vhd
-- Description:  This test duplicates the problem seen in bug 7300, the expected
-- output is that outerr is always zero.

library IEEE;
use IEEE.std_logic_1164.all;

entity bug7300 is 
  port(clk		: in  STD_LOGIC;
       resetn		: in  STD_LOGIC;
       ctl1		: in  STD_LOGIC_VECTOR (0 to 7);
       ctl2		: in  STD_LOGIC_VECTOR (0 to 7);
--	inbus0		: in  STD_LOGIC_VECTOR (0 to 11);
--	inbus1		: in  STD_LOGIC_VECTOR (0 to 11);
--	inbus2		: in  STD_LOGIC_VECTOR (0 to 11);
--	inbus3		: in  STD_LOGIC_VECTOR (0 to 11);
--	inbus4		: in  STD_LOGIC_VECTOR (0 to 11);
--	inbus5		: in  STD_LOGIC_VECTOR (0 to 11);
       outbus0		: out  STD_LOGIC_VECTOR (0 to 11);
       outbus1		: out  STD_LOGIC_VECTOR (0 to 11);
       outbus2		: out  STD_LOGIC_VECTOR (0 to 11);
       outbus3		: out  STD_LOGIC_VECTOR (0 to 11);
       outbus4		: out  STD_LOGIC_VECTOR (0 to 11);
       outbus5		: out  STD_LOGIC_VECTOR (0 to 11);
       outbus6		: out  STD_LOGIC_VECTOR (0 to 11);
       outbus7		: out  STD_LOGIC_VECTOR (0 to 11);
       outbus0_wa	: out  STD_LOGIC_VECTOR (0 to 11);
       outbus1_wa	: out  STD_LOGIC_VECTOR (0 to 11);
       outbus2_wa	: out  STD_LOGIC_VECTOR (0 to 11);
       outbus3_wa	: out  STD_LOGIC_VECTOR (0 to 11);
       outbus4_wa	: out  STD_LOGIC_VECTOR (0 to 11);
       outbus5_wa	: out  STD_LOGIC_VECTOR (0 to 11);
       outbus6_wa	: out  STD_LOGIC_VECTOR (0 to 11);
       outbus7_wa	: out  STD_LOGIC_VECTOR (0 to 11);
       outerr		: out  STD_LOGIC_VECTOR (0 to 7) );
end bug7300;


architecture rtl of  bug7300 is

  SUBTYPE  my_array       IS STD_LOGIC_VECTOR (0 TO 11);
  TYPE my_array_array IS ARRAY (NATURAL RANGE <>) OF my_array;

  SIGNAL signal1     : my_array_array(0 TO 7);

  -- workaround
  SIGNAL signal1_wa     : my_array_array(0 TO 7);
begin

  if_bug_proc: PROCESS(clk, resetn)
  BEGIN
    IF (resetn = '0' or resetn = 'L') THEN
      signal1(0) <= "000000000000";
      signal1(1) <= "000100010001";
      signal1(2) <= "001000100010";
      signal1(3) <= "001100110011";
      signal1(4) <= "010001000100";
      signal1(5) <= "010001000101";
      signal1(6) <= "010001000110";
      signal1(7) <= "111111111111";
    ELSIF (clk'EVENT AND clk = '1') THEN
      FOR I IN 0 TO 6 LOOP
        IF (ctl1(I) = '1') THEN
          signal1(I) <= signal1(I+1);
          IF(ctl2(I) = '1') THEN
            signal1(I) <= signal1(0);   -- incorrectly does not executes this line when condition was true
          END IF;
        END IF;
      END LOOP; 
    END IF;
  END PROCESS if_bug_proc;



  if_workaround_proc: PROCESS(clk, resetn)
  BEGIN
    IF (resetn = '0' or resetn = 'L') THEN
      signal1_wa(0) <= "000000000000";
      signal1_wa(1) <= "000100010001";
      signal1_wa(2) <= "001000100010";
      signal1_wa(3) <= "001100110011";
      signal1_wa(4) <= "010001000100";
      signal1_wa(5) <= "010001000101";
      signal1_wa(6) <= "010001000110";
      signal1_wa(7) <= "111111111111";
--	         signal1_wa(0) <= inbus0;
--	         signal1_wa(1) <= inbus1;
--	         signal1_wa(2) <= inbus2;
--	         signal1_wa(3) <= inbus3;
--	         signal1_wa(4) <= inbus4;
--	         signal1_wa(5) <= inbus5;
    ELSIF (clk'EVENT AND clk = '1') THEN
      FOR I IN 0 TO 6 LOOP
        IF (ctl1(I) = '1') THEN --{
          IF(ctl2(I) = '0') THEN -- {
            signal1_wa(I) <= signal1_wa(I+1);
          ELSE
            signal1_wa(I) <= signal1_wa(0);
          END IF; -- }
        END IF; -- }
      END LOOP; 
    END IF;
  END PROCESS if_workaround_proc ;

  outbus0      <= signal1(0); 
  outbus1      <= signal1(1); 
  outbus2      <= signal1(2); 
  outbus3      <= signal1(3); 
  outbus4      <= signal1(4); 
  outbus5      <= signal1(5); 
  outbus6      <= signal1(6); 
  outbus7      <= signal1(7); 

  outbus0_wa      <= signal1_wa(0); 
  outbus1_wa      <= signal1_wa(1); 
  outbus2_wa      <= signal1_wa(2); 
  outbus3_wa      <= signal1_wa(3); 
  outbus4_wa      <= signal1_wa(4); 
  outbus5_wa      <= signal1_wa(5); 
  outbus6_wa      <= signal1_wa(6); 
  outbus7_wa      <= signal1_wa(7); 


  PROCESS(clk)
  BEGIN
    IF clk'EVENT AND clk = '0' THEN
      FOR I IN 0 TO 7 LOOP
        if (signal1(I) = signal1_wa(I)) THEN
          outerr(I) <= '0';
        ELSE
          outerr(I) <= '1';
        END IF; 
      END LOOP; 
    END IF; 
  END PROCESS;

end rtl;

