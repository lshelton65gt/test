-- Reproducer for issue 69, customer 2 b.
--
-- VerificVhdlIdRef.cpp:394 INFO_ASSERT(false) failed

library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity issue69 is
port (
   clk : in std_logic;
   sel : in integer range 0 to 2;
   a1 : in std_logic_vector(3 downto 0);
   out1 : out std_logic;
   out2 : out std_logic
);
end issue69;

architecture rtl of issue69 is

-- Constant array of vectors
type t1 is array(0 to 3) of std_logic_vector(5 downto 3);
constant const1 : t1 := (0=>"000", 1=>"001", 2=>"010", 3=>"100");

begin


   process (clk) is
     variable temp : integer range 5 downto 3;
   begin
     if (clk'event and clk = '1') then
       -- original failure occurs on this line.
       out1 <= a1(a1'length-1 downto 1)(1);       

       -- but this works ok
       -- out1 <= a1(a1'length-1 downto 1)(sel);       

       -- related failure occurs here
       temp := sel + 3; -- Adjust temp to be within range
       out2 <= const1(3)(temp);
     end if;    
   end process;

end rtl;
