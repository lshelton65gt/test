-- Tests assignment of signed and unsigned ranged integers to each other.
library ieee;
use ieee.std_logic_1164.all;

entity attribute_sign is
  
  port (
    s : in  integer range 0 to 3;
    o0 : out integer;
    o1 : out integer;
    o2 : out integer;
    o3 : out integer);

end attribute_sign;

architecture arch of attribute_sign is

  subtype sr_int0 is integer range 8 to 12;
  signal s0 : sr_int0 := sr_int0'high;

  subtype sr_int1 is integer range 0 to 15;
  signal s1 : sr_int1 := sr_int1'high;
  
  subtype sr_int2 is integer range -12 to 12;
  signal s2 : sr_int2 := sr_int2'low;
  
  subtype sr_int3 is integer range -15 to 15;
  signal s3 : sr_int3 := sr_int3'low;
  
begin  -- arch

  process (s, s0, s1, s2, s3)
  begin  -- process
    if s = 0 then
      -- Narrow ranges assign to larger ranges
      s1 <= sr_int0'low;                -- 8

      s2 <= sr_int2'low;                -- -12
    elsif s = 1 then
      -- Large ranges assign to narrow ranges with legal values
      s0 <= sr_int1'high - 3;             -- 12

      s2 <= sr_int3'low - sr_int2'low;  -- -15 - (-12) = -3
    elsif s = 2 then
      -- unsigned range assigns to signed range
      s2 <= sr_int0'low;                -- 8

      s3 <= sr_int1'low + sr_int3'high;  -- 15
    elsif s = 3 then
      -- signed range assigns to unsigned range
      s0 <= sr_int2'high;               -- 12

      s1 <= sr_int3'high;               -- 15
    end if;
  end process;

  o0 <= s0;
  o1 <= s1;
  o2 <= s2;
  o3 <= s3;
    
end arch;
