-- this version uses a Downto outside the scope of a concat

-- note, Aldec gives bogus results for this (all Zs).  The gold
-- file was created with MTI.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity dyn_slice5 is  
    port (
         a : in std_logic_vector(63 downto 0);
         idx : in unsigned(4 downto 0);
         dout : out std_logic_vector(31 downto 0)
         );
end dyn_slice5;

architecture dyn_slice5 of dyn_slice5 is
 begin

   process (a, idx)
     variable idx_int : integer;
      begin
        idx_int := to_integer(idx);
        dout <= a((idx_int + 31) downto idx_int);
      end process;
end dyn_slice5;
