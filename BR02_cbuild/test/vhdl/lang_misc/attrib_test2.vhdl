-- Tests use of 'length attribute in functions (sxt,zxt) similar to the ones
-- found in matrox design. The functions use the 'length attribute to compute
-- the length of input vector, and use it to declare a vector of that length.
library IEEE;
use  IEEE.STD_LOGIC_1164.all;
use  IEEE.STD_LOGIC_ARITH.all;
use  IEEE.STD_LOGIC_UNSIGNED.all;

entity attrib_test2 is
  port (
    in1  : in std_logic_vector(3 downto 0);
    in2  : in std_logic_vector(7 downto 0);
    out1 : out std_logic_vector(7 downto 0);
    out2 : out std_logic_vector(11 downto 0);
    out3 : out std_logic_vector(8 downto 0);
    out4 : out std_logic_vector(12 downto 0));
end attrib_test2;

architecture arch of attrib_test2 is

function zxt (arg : std_logic_vector; size : integer)
  return std_logic_vector is
  constant shft : integer := size - arg'length;
  subtype t1 is std_logic_vector(size-1 downto 0);
  variable rslt: t1;
begin
  rslt := conv_std_logic_vector(shl(unsigned(conv_std_logic_vector(unsigned(arg), size)), conv_unsigned(shft, 3)), size);
  return rslt;
end function zxt;


function szxt (arg : std_logic_vector; sgn : integer; pad: integer)
  return std_logic_vector is
  constant isize : integer := arg'length + pad;
  constant fsize : integer := isize + sgn;
  subtype rtype is std_logic_vector(fsize-1 downto 0);
  variable result : rtype;
begin -- function extndvctr
  result := sxt(zxt(arg,isize),fsize);
  return result;
end function szxt;

begin  -- arch

  out1 <= zxt(in1, out1'length);
  out2 <= zxt(in2, out2'length);

  out3 <= szxt(in1, 1, 4);
  out4 <= szxt(in2, 1, 4);
  
end arch;
