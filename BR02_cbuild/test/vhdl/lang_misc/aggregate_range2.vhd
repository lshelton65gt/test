-- test for aggregate range negative downto negative (-1 downto -11)

library IEEE;
use IEEE.std_logic_1164.all;

package pack is
    subtype idx is integer range -1 downto -11;
    subtype value is integer range 0 to 3;
    type ttype is array(idx range <>) of value;
    constant table: ttype(-1 downto -11) :=
                (0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3);
end;
package body pack is
end;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.pack.all;

entity aggregate_range2 is
    port(in1 : in  integer;
         out1: out integer);
end aggregate_range2;
architecture a of aggregate_range2 is
begin
    process(in1)
    variable x  : integer;
    begin
        x := in1 mod 10;
        x := x - 11;
        out1 <= table(x);
    end process;
end;


