-- variant on dyn_slice where there are interposed fixed-with slices
-- in the concatenation

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity dyn_slice2 is  
    port (
         a : in std_logic_vector(63 downto 0);
         b : in std_logic_vector(63 downto 0);
         v : in std_logic_vector(7 downto 0);
         x : in std_logic_vector(7 downto 0);
         y : in std_logic_vector(7 downto 0);
         idx : in unsigned(5 downto 0);
         dout : out std_logic_vector(87 downto 0)
         );
end dyn_slice2;

architecture dyn_slice2 of dyn_slice2 is
 begin

   process (a, b, v, x, y, idx)
     variable idx_int : integer;
      begin
        idx_int := to_integer(idx);
        dout <= v &
                a(63 downto idx_int) &
                x &
                b(idx_int - 1 downto 0) &
                y;
      end process;
end dyn_slice2;
