-- Tests use of 'length attribute on a variable range ..but constant width,
-- vector within a for loop. The width is passed to the function which uses
-- it to declare a vector of that length.
entity attrib_test4 is
  port (
    inp  : in  bit_vector(15 downto 0);
    outp : out bit_vector(3 downto 0));
end attrib_test4;

architecture arch of attrib_test4 is

function foo (v : bit_vector; size : integer) return bit is
  variable foo_vect : bit_vector(size downto 0);
  variable foo_int : integer;
begin
  foo_vect := v;
  foo_int := size;
  return foo_vect(foo_int - 1 );
end foo;
  
begin
  process (inp)
  begin
    for i in 3 downto 0 loop
      outp(i) <= not FOO( inp(4*i+3 downto 4*i), inp(4*i+3 downto 4*i)'length - 1);
    end loop;
  end process;
end arch;
