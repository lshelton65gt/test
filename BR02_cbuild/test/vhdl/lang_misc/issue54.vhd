-- This is a reproducer for customer 2 issue 54
-- NUExpr.cxx:1172 NU_ASSERT(!net->is2DAnything()) failed
--
-- A fix for this might also fix issue 53
library ieee;
library work;
use ieee.std_logic_1164.all;

package destypes is
   type vec_array is array (natural range <>) of std_logic_vector(31 downto 0);
end package;



library ieee;
library work;
use ieee.std_logic_1164.all;
use work.destypes.all;

entity issue54 is
   port(clk    : in std_logic;
        resetn : in std_logic;
        sel    : in std_logic; 
        zin  : in  vec_array(7 downto 0);
        zout : out  vec_array(7 downto 0)
        );
end issue54;
architecture rtl of issue54 is
   signal hld_zin     : vec_array(7 downto 0);
begin

   process(clk, resetn) is
   begin
      if (resetn = '0') then
         hld_zin <= (others => "00000000000000000000000000000000");
      elsif (clk'event and clk = '1') then
         hld_zin <= zin;
      end if;
   end process;

   zout <= zin when (sel = '0') else hld_zin;

end rtl;
