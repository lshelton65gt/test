-- filename: test/vhdl/lang_misc/issue87_b.vhd
-- Description:  This test is a simplified version of issue87, the assignment
-- to out1 is populated incorrectly.

library ieee;
use ieee.std_logic_1164.all;

package my_pkg is
  type arr2 is array (0 to 1) of std_logic_vector(3 downto 0);
end my_pkg;



library ieee;
use ieee.std_logic_1164.all;
use work.my_pkg.all;

entity issue87_b is
  port (
    clock    : in  std_logic;
    out1     : out arr2);
end;

architecture arch of issue87_b is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1 <= (others => (others => '1')); --this was populated incorrectly as out1 <= ( '0' & '0' & '1' & '1')
    end if;
  end process;
end;
