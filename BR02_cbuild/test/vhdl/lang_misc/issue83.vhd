-- filename: test/vhdl/lang_misc/issue83.vhd
-- Description:  This test duplicates a simulation mismatch seen in the
-- Immagination Rogue design file.
-- To duplicate the problem, the indicated comparsion of record1.field1 and
-- ENUMERATED_THREADS must occur within a for LOOP even if there is only one
-- iteration of the loop!


library ieee;
use ieee.std_logic_1164.all;

entity issue83 is
  port (
    clock    : in  std_logic;
    in4 : in  std_logic_vector(3 downto 0);
    in1 : in  std_logic;
    out1     : out std_logic);
end;

architecture arch of issue83 is 
  type p_record1 is record
                      field4 : std_logic_vector(3 downto 0);
                      field1 : std_logic_vector(1 downto 0);
                    end record;

  constant ENUMERATED_THREADS     : std_logic_vector(7 downto 0) := "11" & "10" & "01" & "00";
begin 
  main: process (clock)
    variable record1      : p_record1;
    variable temp : std_logic := '0';
  begin
    if clock'event and clock = '1' then 
      record1.field1 := ('1','0');
      record1.field4 := in4;

      for i in 2 to 2 loop
        -- record1.field1 is always "10", and ENUMERATED_THREADS((i*2) downto i*2) is
        -- always "10" so the following if should always be true
        if record1.field1 = ENUMERATED_THREADS((i*2)+1 downto i*2) then
          temp := in1;
        end if;
      end loop;


      out1 <= temp;
    end if;
  end process;
end;
