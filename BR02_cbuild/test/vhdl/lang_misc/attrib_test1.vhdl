-- Tests use of 'length attribute in the body of the function.
library ieee;
use ieee.std_logic_1164.all;

entity attrib_test1 is
  port (
    idx  : in integer;
    out1 : out std_logic;
    out2 : out std_logic);
end attrib_test1;

architecture arch of attrib_test1 is

  function checkidx (
    idx : in integer;
    upper_bound : in integer := 8) return std_logic is

    variable vec : std_logic_vector(0 to upper_bound);
  begin
    if (idx < vec(0 to upper_bound-2)'length) then
      return '1';
    end if;
    return '0';
  end checkidx;

  constant upper_bound1 : integer := 8;
  constant upper_bound2 : integer := 12;
  
begin  -- arch

  check: process (idx)
  begin  -- process check
    out1 <= checkidx(idx, upper_bound1);
    out2 <= checkidx(idx, upper_bound2);
  end process check;
  
end arch;
