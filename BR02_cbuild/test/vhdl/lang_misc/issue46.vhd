library ieee;
use ieee.std_logic_1164.all;
package mine is

    type recordWithVector is
    record
      vectorA           : std_logic_vector(7 downto 0);
    end record;


end mine;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_bit.all;
use work.mine.all;

entity issue46 is
  port(    clock    : in  std_logic;
           in1,in2: in unsigned(0 to 3);
           out1: out unsigned(0 to 7));
end;

architecture arch of issue46 is 

  signal sigRecord   : recordWithVector;
  signal temp : std_logic_vector(1 downto 0);


  
  function foo(in1,in2 : unsigned) return unsigned is
  variable v : integer;
  variable j : integer;
  variable temp : unsigned(7 downto 0);
  begin
    temp := (others => '0');
    v := 0;
    temp(3 downto 0) := in1 + in2;
    l1: for i in 0 to 1 loop
      j := 0;
      l2: while j <= 3 loop
        if (i = 3) then
          exit;
        else
          v := v + j;
        end if;
        j := j + 1;
        if (j = 3 ) then
          return temp;
        end if;
        temp := temp + 1;               -- the offending line
      end loop l2;
      v := v + i;
      if (v = 15) then
        return temp;
      end if;
    end loop l1;
  end;

begin
  process (clock)
  begin 
    if clock'event and clock = '1' then
      l1: for i in 0 to 3 loop 
        temp <= sigRecord.vectorA(i*2+1 downto i*2);
        if temp = "01" then
          exit;
        end if;
        out1 <= foo(in1,in2);
      end loop;
    end if;
      
  end process;
end;
