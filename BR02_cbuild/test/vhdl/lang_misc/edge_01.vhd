-- filename: test/vhdl/lang_misc/edge_01.vhd
-- Description:  This test is used to show that
--     rising_edge(clk)
-- is not equivalent to
--     clk'event and clk = '1'
-- we expect q1 and q2 to always be equivalent, and carbon handles both types of
-- clock expressiosn as equivalent
-- but other simulators do not; in particular at the start of the simulation,
-- before clk has a value.*

-- gold file is from aldec and mti
library ieee;
use ieee.std_logic_1164.all;

entity edge_01 is

  port (
    clk : in std_logic;
    din     : in std_logic_vector(1 downto 0);
    q1,q2   : out std_logic_vector(1 downto 0));

end edge_01;

architecture arch of edge_01 is
begin  

  p1: process (clk)
  begin  -- process p1
    if rising_edge(clk) then
        q1 <= din;
    end if;
  end process p1;


  p2: process (clk)
  begin  -- process p2
    if (clk = '1' and clk'event) then
        q2 <= din;
    end if;
  end process p2;
end ;

