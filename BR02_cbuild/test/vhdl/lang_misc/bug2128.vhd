library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity bug2128 is
  port(rval_signal : in STD_LOGIC_VECTOR(7 downto 0);
       lval : out STD_LOGIC_VECTOR(2 downto 0));
end;

architecture arch of bug2128 is
  constant rval_constant : std_logic_vector(7 downto 0) := "00000001";
begin

  process (rval_signal)
    variable rval_variable : std_logic_vector(7 downto 0);
  begin
      lval <= "000";
      lval <= rval_signal(6 downto 4);
      lval <= rval_variable(6 downto 4);
      lval <= rval_constant(6 downto 4);
--cbuild: /home/cds/tmcbraye/dev/vhdl/src/localflow/VhPopulateExpr.cxx:1134:
--VhdlPopulate::ErrorCode VhdlPopulate::aggregate(vhExprStruct*, LFContext*,
--NUModule*, int, NUExpr**):
--Assertion `0' failed.
  end process;

end arch;
