-- May 2007
-- Tests the attribute val for enum.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity attr2 is
port (clk : in std_logic;
      in1 : integer range 0 to 3;
      out1 : out integer
     );
end attr2;

architecture arch_attr2 of attr2 is
  constant intR : integer := 1;
  constant intY : integer := 2;
  constant intB : integer := 4;
  constant intG : integer := 8;
  constant intE : integer := 256;
  
  type color_typ is ('R', 'Y', 'B', 'G' );
  signal temp : color_typ;
begin

  process (clk, in1)
  begin

   if (clk'event and clk = '1') then
     temp <= color_typ'val(in1);       
     if(temp = 'R') then                
       out1 <= intR;
     elsif (temp = 'Y') then
       out1 <= intY;
     elsif (temp = 'B') then
       out1 <= intB;
     elsif(temp = 'G') then
       out1 <= intG;
     else
       out1 <= intE;
     end if;
   end if;
   
  end process;
end arch_attr2;

         
