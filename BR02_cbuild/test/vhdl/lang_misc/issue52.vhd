-- This testcase reproduces issue 52 seen in customer 2 design
--
-- The failure mode is:
-- CARBON INTERNAL ERROR, should has NULvalue base, 
-- VerificVhdlStatementWalker.cpp:1831 
-- INFO_ASSERT(dynamic_cast<NULvalue*>(lBase)) failed

package destypes is

   type halfword is
     record
       hi : bit_vector(7 downto 0);
       lo : bit_vector(7 downto 0);
     end record;

   type fullword is
     record
       valid : bit;
       up : halfword;
       down : halfword;
     end record;

   function to_record(v : bit_vector) return halfword;
   function to_record(v : bit_vector) return fullword;

end package;

package body destypes is

   function to_record(v : bit_vector) return halfword is
     variable q : halfword;
     variable len : integer := v'length;            -- 16
     variable hi_l : integer := v'left;             -- 15
     variable hi_r : integer := hi_l - (len/2) + 1; -- 15 - 8 + 1= 8
     variable lo_l : integer := hi_r - 1;           -- 7
     variable lo_r : integer := lo_l - (len/2) + 1; -- 7 - 8 + 1 = 0
   begin
     q.hi := v(hi_l downto hi_r);
     q.lo := v(lo_l downto lo_r);
     return q;
   end;

   function to_record(v : bit_vector) return fullword is
     variable q : fullword;
   begin
     q.valid := '1';
     q.up := to_record(v(31 downto 16));
     q.down := to_record(v(15 downto 0));
     return q;
   end;

end destypes;

use work.destypes.all;
entity issue52 is
  port (
    in1 : in bit_vector(31 downto 0);
    out1 : out bit_vector(31 downto 0)
  );
end issue52;

architecture rtl of issue52 is
begin
   process (in1) is
     variable a : fullword;
   begin
      a := to_record(in1);
      out1 <= a.up.hi & a.up.lo & a.down.hi & a.down.lo;
   end process;

end rtl;

