-- This demonstrates a problem assigning (others => '0')
-- when the LHS doesn't have statically determinable range.
-- This used to caause:
-- VerificVhdlExpressionWalker.cpp:1262 INFO_ASSERT(mTargetConstraint) failed
-- in getMissingIndexes

library ieee;
use ieee.std_logic_1164.all;
entity others2 is
port( clk : in std_logic;
      a : in std_logic_vector(3 downto 0);
      b : out std_logic_vector(3 downto 0);
      c : out std_logic_vector(3 downto 0)
     );
end others2;

architecture rtl of others2 is
begin
   process is
   begin
      wait until clk'event and clk = '1';
      for i in 3 downto 2 loop
         if i = 3 then
            b(i downto i-1) <= (others => a(0));
         elsif i = 2 then
            b(i-1 downto 0) <= (others => a(1));
         end if;
      end loop;
      c <= not a;
   end process;
end architecture;

