-- This testcase demonstrates an issue with the
-- Verific flow. Range subtypes used to cause
-- a segfault.
library ieee;
use ieee.std_logic_1164.all;
entity rang1 is
port( a : in std_logic_vector(15 downto 0);
      b : in std_logic_vector(15 downto 0);
      c : out std_logic_vector(31 downto 0)
     );
end rang1;

architecture rtl of rang1 is
subtype  BASE_ADDR_RNG is natural range 15 downto 0;
subtype  TOP_ADDR_RNG is natural range 31 downto 16;
begin

process(a,b) is
begin
  c(BASE_ADDR_RNG) <= a;
  c(TOP_ADDR_RNG) <= b;
end process;

end architecture;

