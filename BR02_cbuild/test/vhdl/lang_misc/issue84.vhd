-- Reproducer for issue 84: functions returning arrays of records not populated
-- properly (result mostly 0).

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;

package pack is
   constant NW : INTEGER := 16;
   type trrt is
      record
         v      : std_logic;
         ss     : std_logic_vector(3 downto 0);
         ti     : std_logic_vector(3 downto 0);
         dm     : std_logic_vector(4 downto 0);
         di     : std_logic_vector(4 downto 0);
         ta     : std_logic_vector(40-6-6-1 downto 0);
      end record;
   type trrta is array (0 to NW-1) of trrt;
   function conv_trrta(v : std_logic_vector(NW*48-1 downto 0)) return trrta;
end pack;
package body pack is
  function conv_trrta(v : std_logic_vector(NW*48-1 downto 0)) return trrta is
      variable r : trrta;
      variable lb  : integer;
      variable hb : integer;
   begin
      for w in 0 to NW-1 loop
         lb  := w*48;
         hb := lb+r(w).ta'high;
         r(w).ta := v(hb downto lb);

         lb  := hb + 1;
         hb := lb + r(w).di'high;
         r(w).di := v(hb downto lb);

         lb  := hb + 1;
         hb := lb + r(w).dm'high;
         r(w).dm := v(hb downto lb);

         lb  := hb + 1;
         hb := lb + r(w).ti'high;
         r(w).ti := v(hb downto lb);

         lb  := hb + 1;
         hb := lb + r(w).ss'high;
         r(w).ss := v(hb downto lb);

         hb := hb + 1;
         r(w).v  := v(hb);
      end loop;
      return(r);
   end function;
end pack;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library work;
use work.pack.all;
entity issue84 is
port (
   o         : out trrta;
   i         : in std_logic_vector(NW*48-1 downto 0)
  );
end issue84;

architecture rtl of issue84 is

begin  -- rtl

   o <= conv_trrta(i);

end rtl;
