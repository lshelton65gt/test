-- filename: test/vhdl/lang_misc/test_is_x.vhd
-- Description:  This test duplicates a problem seen in Customer2A design (issue80)
-- where parts of the design seem to be disabled.  Caused by the fact that the
-- std is_x () function was returning true on values that had zero because
-- carbon folds some values into zero.


library ieee;
use ieee.std_logic_1164.all;

entity test_is_x is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector((WIDTH-1) downto 0);
    out1     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of test_is_x is 
 signal sul : std_ulogic := 'X';
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
     if is_x(in1) then 
       -- this branch should never be executed because in1 will never have x in
       -- it so is_x(in1) should always return false
       out1 <= (others => '0');
     else
      if is_x('X') then                --should always be true
        if is_x('0') then                --should always be false
          out1 <= "0011";   --this line should never be executed because is_x('0') should always be false
        else
          out1 <= "111" & in2(0);
        end if;
      else
        out1 <= "0001";   --this line should never be executed because is_x('X') should always be true
      end if;
     end if;
    end if;
  end process;
end;
