-- November 2008
-- The issue is with aggregate, which is defined by name. With previous version
-- of CBUILD, the indexes were populated incorrectly.

library ieee;
use ieee.std_logic_1164.all;

entity bug11709_01 is
  
  port (
    in1  : in  std_logic_vector(7 downto 0);
    out1 : out std_logic_vector(10 downto 1);
    out2 : out std_logic_vector(5 to 14));

end bug11709_01;

architecture arch of bug11709_01 is
begin  -- arch

  out1 <= (5 to 8 => '1', 3 downto 1 => not(in1(3)), 9  => in1(7), 4 => in1(5), 10 => in1(2));
  out2 <= (10 to 13 => '1', 9 downto 7 => not(in1(3)), 6  => in1(7), 5 => in1(5), 14 => in1(2));

end arch;
