-- May 2009
-- The issue was with dquant_int_tmp. CBuild was allocating the net with wrong
-- size.
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

entity bug12602_02 is
  
  port (
    in1  : in  std_logic_vector(2 downto 0);
    in2  : in  std_logic_vector(2 downto 0);
    in3  : in  std_logic_vector(2 downto 0);
    in4  : in  std_logic_vector(2 downto 0);
    in5  : in  std_logic_vector(3 downto 0);
    in6  : in  std_logic_vector(4 downto 0);
    in7  : in  std_logic_vector(5 downto 0);
    in8  : in  std_logic_vector(5 downto 0);
    in9  : in  std_logic_vector(6 downto 0);
    
    out1 : out  std_logic_vector(2 downto 0);
    out2 : out  std_logic_vector(2 downto 0);
    out3 : out  std_logic_vector(2 downto 0);
    out4 : out  std_logic_vector(2 downto 0);
    out5 : out  std_logic_vector(3 downto 0);
    out6 : out  std_logic_vector(4 downto 0);
    out7 : out  std_logic_vector(5 downto 0);
    out8 : out  std_logic_vector(5 downto 0);
    out9 : out  std_logic_vector(6 downto 0));

end bug12602_02;


architecture arch1 of bug12602_02 is
    signal t1 : integer range  -4 to 0;  -- carbon1 observeSignal
    signal t2 : integer range  -4 to 3;  -- carbon1 observeSignal
    signal t3 : integer range  -4 to 1;  -- carbon1 observeSignal
    signal t4 : integer range  1 to 4;   -- carbon1 observeSignal
    signal t5 : integer range  0 to 11;  -- carbon1 observeSignal
    signal t6 : integer range  0 to 26;  -- carbon1 observeSignal
    signal t7 : integer range -26 to 25; -- carbon1 observeSignal
    signal t8 : integer range -26 to 0;  -- carbon1 observeSignal
    signal t9 : integer range -51 to 51; -- carbon1 observeSignal

begin

  p1: process (in1, in2, in3, in4, in5, in6, in7, in8, in9)
    variable temp1 : std_logic_vector(2 downto 0) := (others => '0');
    variable temp2 : std_logic_vector(2 downto 0) := (others => '0');
    variable temp3 : std_logic_vector(2 downto 0) := (others => '0');
    variable temp4 : std_logic_vector(2 downto 0) := (others => '0');
    variable temp5 : std_logic_vector(3 downto 0) := (others => '0');
    variable temp6 : std_logic_vector(4 downto 0) := (others => '0');
    variable temp7 : std_logic_vector(5 downto 0) := (others => '0');
    variable temp8 : std_logic_vector(5 downto 0) := (others => '0');
    variable temp9 : std_logic_vector(6 downto 0) := (others => '0');
  begin
    temp1(2) := '1';
    temp1(1 downto 0) := in1(1 downto 0);
    t1 <= TO_INTEGER(SIGNED(temp1));

    temp2 := in2;
    t2 <= TO_INTEGER(SIGNED(temp2));

    if(in3(2) = '1') then
      temp3 := in3;
    else
      temp3(2 downto 1) := "00";
      temp3(0) := in3(0);
    end if;
    t3 <= TO_INTEGER(SIGNED(temp3));

    if(in4(2) = '1') then
      temp4(2) := in4(2);
      temp4(1 downto 0) := "00";
    elsif in4(0) = '1' then
      temp4 := in4;
    else
      temp4(2) := '0';
      temp4(1) := in4(1);
      temp4(0) := '1';
    end if;
    t4 <= TO_INTEGER(UNSIGNED(temp4));

    temp5(2 downto 0) := in5(2 downto 0);
    t5 <= TO_INTEGER(UNSIGNED(temp5));
    
    temp6(3 downto 0) := in6(3 downto 0);
    t6 <= TO_INTEGER(UNSIGNED(temp6));


    if(in7(5 downto 3) = "100") then
      temp7(5 downto 3) := in7(5 downto 3);
      temp7(2 downto 0) := "110";
    elsif(in7(5 downto 3) = "011") then
      temp7(5 downto 3) := in7(5 downto 3);
      temp7(2 downto 0) := "001";
    else
      temp7 := in7;
    end if;
    t7 <= TO_INTEGER(SIGNED(temp7));
    
    if(in8(5 downto 3) = "100") then
      temp8(5 downto 3) := in8(5 downto 3);
      temp8(2 downto 0) := "110";
    elsif(in8(5) = '1') then
      temp8 := in8;
    else
      temp8 := "000000";
    end if;
    t8 <= TO_INTEGER(SIGNED(temp8));

    temp9(4 downto 0) := in7(4 downto 0);
    t9 <= TO_INTEGER(SIGNED(temp9));
  end process;
    
  out1 <= STD_LOGIC_VECTOR(TO_SIGNED(t1, 3));  
  out2 <= STD_LOGIC_VECTOR(TO_SIGNED(t2, 3));  
  out3 <= STD_LOGIC_VECTOR(TO_SIGNED(t3, 3));  
  out4 <= STD_LOGIC_VECTOR(TO_UNSIGNED(t4, 3));  
  out5 <= STD_LOGIC_VECTOR(TO_UNSIGNED(t5, 4));  
  out6 <= STD_LOGIC_VECTOR(TO_UNSIGNED(t6, 5));  
  out7 <= STD_LOGIC_VECTOR(TO_SIGNED(t7, 6));  
  out8 <= STD_LOGIC_VECTOR(TO_SIGNED(t8, 6));  
  out9 <= STD_LOGIC_VECTOR(TO_SIGNED(t9, 7));

end arch1;
