-- filename: test/vhdl/lang_misc/bug7875_02.vhd
-- Description:  This test is a trimmed down version of bug7875_01.vhd
-- it duplicates the problem seen in bug 7875 is that during post pop resynth
-- an array operation on a record element is incorrectly modified.

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY bug7875_02 IS
PORT
(
 clk_dms    : IN STD_LOGIC;
 out1       : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
);

END bug7875_02;

ARCHITECTURE rtl OF bug7875_02 IS
 CONSTANT Tcq       : TIME := 300 ps;
 constant loc : integer := 1;
 TYPE mswb_entry_type IS
 RECORD
  data     : STD_LOGIC_VECTOR(15 DOWNTO 0);
 END RECORD;

 TYPE mswb_entry_array_type IS ARRAY (NATURAL RANGE<>) OF mswb_entry_type;

 SIGNAL next_mswb_fifo         : mswb_entry_array_type(0 TO 2) := ((others => "0100110001110000"), (others => "1011001110001111"),(others => "0000111000110010")); --must be an array
 SIGNAL mswb_fifo              : mswb_entry_type := (others => (others => '1'));

BEGIN

  mswb_fifo_s_process: PROCESS(clk_dms)
 variable var1 : STD_LOGIC_VECTOR(15 downto 0) := (others =>  '0');
  BEGIN

--    IF RISING_EDGE(clk_dms) THEN
    if (clk_dms = '1' and clk_dms'event) then
      FOR j IN 0 TO 1 LOOP
        mswb_fifo.data((j*8)+7 DOWNTO (j*8))  <= next_mswb_fifo(loc).data((j*8)+7 DOWNTO (j*8)) AFTER Tcq;
      END LOOP;
      out1 <= mswb_fifo.data;
    END IF;

  END PROCESS;

END rtl;
