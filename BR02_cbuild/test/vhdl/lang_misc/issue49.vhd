-- Reproducer for customer 2 issue 49.
-- This causes a crash in runGlobalOptimizations, assertion failure
-- NU_ASSERT(idx < (int) mPortConnections.size()) failure in NUModuleInstance.cxx, getPortConnectionByIndex.
-- this test is a bit strange for simulation, to make sure that we always see
-- disconnected ports some of the primary outputs are partially undriven, so we
-- expect to see z's in the sim.log file

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
entity child is
   generic(w : integer);
   port(clk    : in  std_logic;
        resetn : in  std_logic;
        eout2   : out std_logic;
        din_later    : in  std_logic_vector(w-1 downto 0); --note out of order alphabetically 
        din_early    : in  std_logic_vector((2*w)-1 downto 0); --note out of order alphabetically 
        doutA   : out std_logic_vector((w*2)-1 downto 0);
        vout   : out std_logic;
        ein    : in  std_logic;
        vin    : in  std_logic;
        eout   : out std_logic;
        doutB   : out std_logic_vector((w*2)-1 downto 0);
        eout3   : out std_logic
        );
end child;
architecture rtl of child is
   signal hld_din     : std_logic_vector(w-1 downto 0)  := (others => '1');
   signal hld_vin     : std_logic;
   signal nxt_hld_vin : std_logic;
   signal eout_i      : std_logic;
   signal nxt_eout    : std_logic;
begin
   rctrl : process (clk, resetn)
   begin
      if (resetn = '0') then
         hld_vin <= '0';
         eout_i  <= '0';
      elsif (clk'event and clk = '1') then
         hld_vin <= nxt_hld_vin;
         eout_i  <= nxt_eout;
      end if;
   end process;
   eout <= eout_i;
   eout2 <= eout_i;
   eout3 <= eout_i;

   ctrl : process (clk)
   begin
      if (clk'event and clk = '1') then
         if (ein = '0' and eout_i = '1' and vin = '1') then
            hld_din <= din_early(32 downto 0) or hld_din;
         end if;
      end if;
   end process;

   nxt_hld_vin <= '1' when (ein = '0' and vin = '1' and eout_i = '1') else
                  '0' when (ein = '1') else
                  hld_vin;

   nxt_eout <= '1' when ((ein = '1') or (hld_vin = '0' and vin = '0')) else '0';
   doutA <= (din_later & hld_din) when (hld_vin = '0') else (din_early);
   doutB <= (din_early) when (hld_vin = '0') else (hld_din & din_later);

   vout <= (eout_i and vin) when (hld_vin = '0') else '1';

end rtl;

-----------------------------------------------------------------------------------------------------------------

library ieee;
library work;
use ieee.std_logic_1164.all;
entity issue49 is
  port (
      clk : in std_logic;
      resetn : in std_logic;
      dout1 : out std_logic_vector(65 downto 0);
      dout2 : out std_logic_vector(65 downto 0)
  );
end issue49;

architecture rtl of issue49 is

   component child
      generic (
         w : integer);
      port (
         clk    : in  std_logic;
         resetn : in  std_logic;
         doutA   : out std_logic_vector((w*2)-1 downto 0);
         din_early    : in  std_logic_vector((2*w)-1 downto 0);
         din_later    : in  std_logic_vector(w-1 downto 0);
         vin    : in  std_logic;
         eout   : out std_logic;
         doutB   : out std_logic_vector((w*2)-1 downto 0);
         vout   : out std_logic;
         ein    : in  std_logic);
   end component;


   signal vec1 : std_logic_vector(65 downto 0) := (others => '1');
   signal p1   : std_logic;
   signal vec2 : std_logic_vector(32 downto 0) := (others => '0');
   signal p2   : std_logic;
   signal p3   : std_logic;
begin


   i_child : child
      generic map(
         w => 33
      )
      port map(
         clk    => clk,
         resetn => resetn,
         din_early    => vec1,
         din_later    => vec2,
         vin    => p1,
         doutA   => dout1,                --vec2
         -- Crash occurs if following line is commented
         -- eout   => open,
         vout   => p2,
--         doutB => dout2
         ein    => p3
         
         );

end rtl;
