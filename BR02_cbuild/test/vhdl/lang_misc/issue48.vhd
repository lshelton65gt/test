-- Description:  This test duplicates customer 2 issue48


library ieee;
use ieee.std_logic_1164.all;

entity issue48 is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 0);

  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector((WIDTH-1) downto 0);
    TOUT     : out std_logic_vector(DEPTH-1 downto 0); --note this is a null range!!!
    out1     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of issue48 is
begin
  main: process (clock)
  begin 
   
    TOUT <= (others => '0');            -- the others=> applied to a null range
                                        -- produces a concat with no elements
                                        -- which is poorly handled and the
                                        -- essence of issue 48

    if clock'event and clock = '1' then
      out1 <= in1 and in2;
    end if;
  end process;
end;
