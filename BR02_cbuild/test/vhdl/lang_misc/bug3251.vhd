library ieee;
use ieee.std_logic_1164.all;

entity bug3251 is
port(
     scsffdatao   :        in       std_logic_vector(2 downto 0);
     xlength      : out std_logic_vector(0 downto 0)
     );
end bug3251;

--******************************************************************
--  Architecture Header Declarations 
--******************************************************************

architecture functional of bug3251 is
begin
   xlength(0 downto 0)  <= scsffdatao(2 downto 2);
end functional;

