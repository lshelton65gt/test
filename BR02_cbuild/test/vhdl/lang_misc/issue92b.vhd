-- filename: test/vhdl/lang_misc/issue92b.vhd
-- Description:  This test duplicates a population error seen in Customer2A design


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;


entity issue92b is
  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector(31 downto 16);
    in2      : in  std_logic_vector(3 downto 0);
    out0, out1,out2,out3,out4 : out std_logic_vector(31 downto 16);
    out5     : out std_logic_vector(15 downto 0)
    );
end;

architecture arch of issue92b is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
--       out0(31 downto 16) <= in1 and (14 downto 7 => in2(0), 6 downto 3 => in2(1), 0=> in2(3), 2 downto 1 => in2(2)); -- bit 15 is not specified, it should not be supported (moved to issue92b_negative.vhd)
       out1(31 downto 16) <= in1 and (15 downto 0 => in2(0)); -- this is valid VHDL 
       out2(31 downto 16) <= in1 and (32 downto 17 => in2(1)); -- this is valid VHDL 
--       out3(31 downto 16) <= in1 and (14 downto 7 => in2(0), 6 downto 3 => in2(1), 0=> in2(3), others => '0'); -- others should cover bits 15, 2, 1, it should not be supported (moved to issue92b_negative.vhd) 
--       out4(31 downto 16) <= in1 and (others => in2(2)); -- others should cover 16 bits wide
--       out5(15 downto 0) <= in1 and (others => in2(3)); -- others is intended to be 16 bits, but constraint is missing it should not be supported 
    end if;
  end process;
end;
