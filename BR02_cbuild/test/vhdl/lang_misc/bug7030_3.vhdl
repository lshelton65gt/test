-- bug7030_3 - all library clauses are present. No use clause is defined intentionally.
-- The full path to sub for s1 is not defined.
-- The Parser should fail to find the "sub" entity.
-- With MVV version 1.21.d that was fixed. The previous version was compiling
-- without complaining about missing definition for sub.

-- CBuild works correctly with MVV version 1.21.d. 

-- ModelSim - doesn't compile, complains about sub not defined for s1.

library ieee;
use ieee.std_logic_1164.all;

library sub1_lib;
--use sub1_lib.all;
library sub2_lib;
--use sub2_lib.all;

entity bug7030_3 is
  
  port (
    clk  : in  std_logic;
    in1  : in  std_logic;
    out1 : out std_logic;
    out2 : out std_logic;
    out3 : out std_logic);
end bug7030_3;

architecture arch of bug7030_3 is

 
begin  -- arch

  s1 : entity sub port map (
    clk  => clk,
    inS  => in1,
    outS => out1);

  s2 : entity sub2_lib.sub  port map (
    clk  => clk,
    inS  => in1,
    outS => out2);

end arch;
