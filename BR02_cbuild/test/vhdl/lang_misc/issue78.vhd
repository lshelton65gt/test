-- filename: test/vhdl/lang_misc/issue78.vhd
-- Description:  This test checks that constant BOOELAN tables are populated
-- correctly. with r2428 the verific flow creates a
--  reg          [0:0]          and_table[0:0];
--  reg          [0:0]          or_table[0:0];
-- while the interra flow creates:
--  reg          [0:8]          and_table[0:8];
--  reg          [0:8]          or_table[0:8];


library ieee;
use ieee.std_logic_1164.all;

entity issue78 is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector((WIDTH-1) downto 0);
    out1,out2: out boolean);
end;

architecture arch of issue78 is
  type BOOLEAN_TABLE is array(STD_ULOGIC, STD_ULOGIC) of BOOLEAN;

  constant AND_TABLE: BOOLEAN_TABLE := (
      --------------------------------------------------------------------------
      -- U      X      0      1      Z      W      L      H      -
      --------------------------------------------------------------------------
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | U |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | X |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | 0 |
      (FALSE, FALSE, FALSE,  TRUE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | 1 |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | Z |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | W |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | L |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | H |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)  -- | - |
      );
  constant OR_TABLE: BOOLEAN_TABLE := (
      --------------------------------------------------------------------------
      -- U      X      0      1      Z      W      L      H      -
      --------------------------------------------------------------------------
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | U |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | X |
      (FALSE, FALSE, FALSE,  TRUE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | 0 |
      (FALSE, FALSE,  TRUE,  TRUE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | 1 |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | Z |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | W |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | L |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE), -- | H |
      (FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)  -- | - |
      );

   -- or the two operands together and then do a reduction OR on the result
   function OR_OR_REDUCTION(L,R: STD_LOGIC_VECTOR) return BOOLEAN;

   function OR_OR_REDUCTION(L,R: STD_LOGIC_VECTOR) return BOOLEAN is
    alias LV: STD_LOGIC_VECTOR(1 to L'LENGTH) is L;
    alias RV: STD_LOGIC_VECTOR(1 to R'LENGTH) is R;
   begin  -- OR1
      for I in LV'LOW to LV'HIGH loop
        if (OR_TABLE(LV(I), RV(I))) then
          return TRUE;
        end if;
      end loop;
      return FALSE;

   end OR_OR_REDUCTION;

   -- AND the two operands together and then do a reduction AND on the result
   function AND_AND_REDUCTION(L,R: STD_LOGIC_VECTOR) return BOOLEAN;

   function AND_AND_REDUCTION(L,R: STD_LOGIC_VECTOR) return BOOLEAN is
    alias LV: STD_LOGIC_VECTOR(1 to L'LENGTH) is L;
    alias RV: STD_LOGIC_VECTOR(1 to R'LENGTH) is R;
   begin  -- AND1
      for I in LV'LOW to LV'HIGH loop
        if not (AND_TABLE(LV(I), RV(I))) then
          return FALSE;
        end if;
      end loop;
      return TRUE;
     
   end AND_AND_REDUCTION;

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1 <= AND_AND_REDUCTION (in1, in2);
      out2 <= OR_OR_REDUCTION (in1, in2);
    end if;
  end process;
end;
