-- This test checks that an error message is issued for instance i1 of entity
-- comp which does not have a component declaration. The component declaration
-- should exist either in architecture declaration section or in the package
-- included using the USE clause.
library ieee;
use ieee.std_logic_1164.all;

library decls;
use decls.comp_decls.all;

-- The library that contains the component declaration.
-- library defs;

entity top is
  
  port (
    in1  : in  std_logic;
    out1 : out std_logic);

end top;

architecture synth of top is

begin  -- synth

  i1 : comp port map (
    in1  => in1,
    out1 => out1);

end synth;
