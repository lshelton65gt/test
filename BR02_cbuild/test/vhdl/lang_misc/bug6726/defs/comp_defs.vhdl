library ieee;
use ieee.std_logic_1164.all;

entity comp is
  
  port (
    in1  : in  std_logic;
    out1 : out std_logic);

end comp;

architecture synth of comp is

begin  -- synth

  out1 <= in1;

end synth;
