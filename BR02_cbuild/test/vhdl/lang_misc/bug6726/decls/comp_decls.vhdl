library ieee;
use ieee.std_logic_1164.all;

package comp_decls is

  component comp
    port (
      in1  : in  std_logic;
      out1 : out std_logic);
  end component;

end comp_decls;
