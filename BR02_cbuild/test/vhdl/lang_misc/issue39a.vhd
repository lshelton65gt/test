-- Customer 2 issue 39
-- This is bug 16453
-- Using "ext" function (in std_logic_arith package) with a non-constant
-- second argument (which determines the overall size of the result)
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
entity issue39a is
  port (
    clk : in std_logic;
    x1  : in integer range 0 to 15;
    din : in std_logic_vector(15 downto 0);
    dout : out std_logic_vector(31 downto 0)
  );
end issue39a;

architecture rtl of issue39a is
begin
   process(clk, x1) is
   begin
      if (clk'event and clk = '1') then
        -- Interra population flow fails to populate this correctly, which results to simulation diff with MTI
        dout <= (ext(din, (32-x1)) & ext(din, x1));
      end if;
   end process;
end rtl;
