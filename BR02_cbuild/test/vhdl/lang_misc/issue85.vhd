-- Reproducer for issue 84 - problem with dead code removal not using the
-- correct data flow object, constant values computed in if/else branches were
-- being seen by the conditional for the next elseif.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;

entity issue85 is
port (  
   in1               : in std_logic_vector(3 downto 0);
   in2             : in std_logic_vector(3 downto 0);
   in3                 : in std_logic_vector(3 downto 0);
   in4                 : in std_logic_vector(3 downto 0);
   in5             : in std_logic_vector(3 downto 0);

    out1             : out std_logic_vector(1 downto 0)
    );
end issue85;

   architecture rtl of issue85 is

   begin  -- rtl

     
  p_comb : process(in2,
                      in4,
                      in1,
                      in5)
      variable pm1 : std_logic;
      variable pm2  : std_logic;
   begin
      out1  <= "11";
      pm1 := not in5(3) and in4(3) and in1(3);
      pm2  := not in5(3) and in2(3);
      for inst in 2 downto 0 loop
         if in5(inst) = '0' and in4(inst) = '1' and in1(inst) = '1' then
            out1  <= conv_std_logic_vector(inst, 2);
            pm1 := '1';
         elsif in5(inst) = '0' and in2(inst) = '1' and pm1 = '0' then
            out1  <= conv_std_logic_vector(inst, 2);
            pm2  := '1';
         elsif in5(inst) = '0' and in4(inst) = '1' and pm1 = '0' and pm2 = '0' then
            out1  <= conv_std_logic_vector(inst, 2);
         end if;
               end loop;
   end process;

   end rtl;
