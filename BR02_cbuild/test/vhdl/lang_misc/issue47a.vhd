-- Reproduces issue 47 exposed by customer 2 testcase
-- Segfault during PostPopulateResynth, on the RHS of the
-- VhdlSignalAssignment in the combinational process.
--
-- Notes: both processes are required to reproduce the problem.
-- This testcase should be enhanced for simulation. As it stands
-- now, it doesn't produce meaningful simulation output.
library ieee;
use ieee.std_logic_1164.all;

package types is
   constant NCOUNT : integer := 2;

   type     rec_nested is
      record
         rec_nested_item : std_logic;
      end record;

   type rec_top is
      record
         rec_top_item              : rec_nested;
      end record;

  type rec_top_array is array (NCOUNT-1 downto 0) of rec_top;

end package;

-----------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.types.all;
entity issue47a is
  port (
      inport : in std_logic;
      result : out std_logic
  );
end issue47a;

architecture rtl of issue47a is
   signal rec_top_array1           : rec_top_array;
   signal t_rec_top_item              : rec_nested;
begin

process (rec_top_array1, t_rec_top_item, inport) is
begin 
        t_rec_top_item.rec_nested_item  <= inport;
        rec_top_array1(0).rec_top_item <= t_rec_top_item; --write whole rec_top_item
        rec_top_array1(1).rec_top_item <= t_rec_top_item; --write whole rec_top_item
        result                         <= rec_top_array1(0).rec_top_item.rec_nested_item or rec_top_array1(1).rec_top_item.rec_nested_item ; --read from rec_nested
end process;


end rtl;

