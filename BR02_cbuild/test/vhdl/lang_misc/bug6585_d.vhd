-- last mod: Wed Sep 13 15:41:40 2006
-- filename: test/vhdl/lang_misc/bug6585_c.vhd
-- Description:  A variation of bug6585_b.vhd. Instead of 'left, 'length attribute is used
-- to compute the length of vector passed to the function. The length is constant, although
-- the left/right bounds vary.


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6585_d is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector(15 downto 0);
    out1     : out std_logic_vector(15 downto 0));
end;

architecture arch of bug6585_d is 

function level2 (v : std_logic_vector; argsize_left : integer) return std_logic_vector;
function level1 (v : std_logic_vector) return std_logic_vector;

function level2 (v : std_logic_vector; argsize_left : integer) return std_logic_vector is
  constant argsize_right : integer := 0;
  constant argsize : integer := argsize_left - argsize_right + 1;
  constant argsize_minus_1 : integer := argsize - 1;
  subtype rtype is std_logic_vector(argsize_minus_1 downto 0);
  variable retval : rtype;
begin
  retval := level1(v);
  return retval;
end level2;


function level1 (v : std_logic_vector) return std_logic_vector is
  constant argsize : integer := v'length;
  constant argsize_minus_1 : integer := argsize - 1;
  subtype rtype is std_logic_vector(argsize_minus_1 downto 0);
  variable retval : rtype;
begin 
  retval := sxt(v, argsize);
  return retval;
end level1;

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
     for i in 3 downto 0 loop
      out1(3+4*i downto 4*i) <= level2(in1(3+4*i downto 4*i), in1(3+4*i downto 4*i)'length-1);
     end loop;  -- i
    end if;
  end process;
end;
