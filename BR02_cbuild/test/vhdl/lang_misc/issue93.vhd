-- filename: test/vhdl/lang_misc/issue93.vhd
-- Description:  This test duplicates an unsupported message (from Immagination
-- rogue test)


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; -- for to_integer()

entity issue93 is
  port (
    clock    : in  std_logic;
    in1      : in integer range 0 to 3;
    hi       : out std_logic_vector(3 downto 0);
    lo       : out std_logic_vector(3 downto 0));
end;

architecture arch of issue93 is 
   type array_of_std_logic_vector8  is array (0 to 3) of std_logic_vector(7 downto 0);

   constant c_array : array_of_std_logic_vector8 := ("11111111", "11110000", "10101010", "00001111");

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
       case in1 is
          when 0 => 
            hi <= c_array(0)(7 downto 4);
            lo <= c_array(0)(3 downto 0);
          when 1 => 
            hi <= c_array(1)(7 downto 4);
            lo <= c_array(1)(3 downto 0);
          when 2 =>             
            hi <= c_array(2)(7 downto 4);
            lo <= c_array(2)(3 downto 0);
          when 3 =>             
            hi <= c_array(3)(7 downto 4);
            lo <= c_array(3)(3 downto 0);
         end case;
    end if;
  end process;
end;
