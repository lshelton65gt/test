-- This testcase reproduces sub-issue of customer 2 b issue 71.
--
-- issue71a.vhd issue with record constant assignment 

package destypes is

type coord is
  record
    x : integer range 3 downto 0;
    y : integer range 3 downto 0;
  end record;


constant val : coord := (1,0);


end package;


library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.destypes.all;

entity issue71a is
  port (
        in_x : in integer range 3 downto 0;
        in_y : in integer range 3 downto 0;
        x : out integer range 6 downto 0;
        y : out integer range 6 downto 0
       );
end issue71a;

architecture rtl of issue71a is
   signal pair : coord;
begin
   -- The incorrect population of constant results to simulation diff (if left hand-side is record we should populate concat and not try to convert it to integer, resynth gets confused with integer population)
   pair <= val;
   x <= in_x + pair.x;
   y <= in_y + pair.y;
end rtl;

