-- filename: test/vhdl/lang_misc/issue75b.vhd
-- Description:  This test checks for proper operation of the std_match function


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;


entity issue75b is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector((WIDTH-1) downto 0);
    out1,out2,out3     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of issue75b is
   signal p2   : std_logic;
   signal p3   : std_logic;
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then

      if std_match(in1, "-10-") then
        out1 <= in1 and in2;
      end if;

      if std_match("1-0-", in2) then
        out2 <= in1 and in2;
      end if;

      if std_match(in1, "0100" ) then  -- multiple match of function std_match Alert 61146
        out3 <= in1 and in2;
      end if;
    end if;
  end process;
end;

