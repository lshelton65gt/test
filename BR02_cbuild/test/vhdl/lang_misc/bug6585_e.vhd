-- last mod: Wed Sep 13 15:41:40 2006
-- filename: test/vhdl/lang_misc/bug6585_e.vhd
-- Description:  A variation of bug6585_a.vhd. Constant aggregates combined with for
-- loop index are used to compute the range of variable range vector. The left range
-- expression is passed as left range to the function.


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6585_e is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector(15 downto 0);
    out1     : out std_logic_vector(15 downto 0));
end;

architecture arch of bug6585_e is 

function level2 (v : std_logic_vector; argsize_left : integer) return std_logic_vector;
function level1 (v : std_logic_vector) return std_logic_vector;

function level2 (v : std_logic_vector; argsize_left : integer) return std_logic_vector is
-- constant argsize_left : integer := v'left;
  constant argsize_right : integer := v'right;
  constant argsize : integer := argsize_left - argsize_right + 1;
  constant argsize_minus_1 : integer := argsize - 1;
  subtype rtype is std_logic_vector(argsize_minus_1 downto 0);
  variable retval : rtype;
begin
  retval := level1(v);
  return retval;
end level2;


function level1 (v : std_logic_vector) return std_logic_vector is
  constant argsize : integer := v'length;
  constant argsize_minus_1 : integer := argsize - 1;
  subtype rtype is std_logic_vector(argsize_minus_1 downto 0);
  variable retval : rtype;
begin 
  retval := sxt(v, argsize);
  return retval;
end level1;

type argary is array (Natural Range <> ) of integer;

constant off_idx : integer := 1;
constant width_idx : integer := 2;
constant args : argary(3 downto 0) := (off_idx => 3, width_idx => 4, others => 0);

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
     for i in args(off_idx) downto 0 loop
      out1(args(off_idx)+args(width_idx)*i downto args(width_idx)*i) <= level2(in1(args(off_idx)+args(width_idx)*i downto args(width_idx)*i),
                                                                               args(off_idx)+args(width_idx)*i);
     end loop;  -- i
    end if;
  end process;
end;
