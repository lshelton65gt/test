-- filename: test/vhdl/lang_misc/issue88.vhd
-- Description:  This test duplicates a simmismatch problem seen in cust 2A.
-- the problem is that population executes the call to function1 before the
-- loop, which means that it uses the wrong value within the function.

library ieee;
use ieee.std_logic_1164.all;

entity issue88 is
  generic (
    WIDTH : integer := 66);

  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector(3 downto 0);
    out0     : out std_logic_vector(9 downto 0);
    out1     : out std_logic_vector(9 downto 0);
    out3    : out  std_logic
    );
end;

architecture arch of issue88 is 


   type rec_t is
      record
         field_0     : std_logic_vector(3 downto 0);
         field_1     : std_logic_vector(1 downto 0);
      end record;

   constant rec_null_entry : rec_t := ((others => '0'),
                                       (others => '0'));

   type rec_array_t is array (9 downto 0) of rec_t;

   
  signal rec_array : rec_array_t := (others => rec_null_entry );


   signal sig_0 : std_logic_vector(9 downto 0);
   signal sig_1 : std_logic_vector(9 downto 0);

   
   function function1(input_vector : std_logic_vector) return std_logic_vector is
      variable in_vec : std_logic_vector(input_vector'length-1 downto 0);
      variable result : std_logic_vector(input_vector'length-1 downto 0);
      constant MIDDLE : integer := input_vector'length/2;
   begin
      in_vec := input_vector;
      if (in_vec'length >= 2) then
         result := function1(in_vec(input_vector'length-1 downto MIDDLE)) & function1(in_vec(MIDDLE-1 downto 0));
         if result(MIDDLE-1) = '1' then
            result(input_vector'length-1 downto MIDDLE) := (others => '1');
         end if;
      else
        result(0) := in_vec(0);
      end if;
      return result;
   end function1;


   

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
     out0 <= sig_0;
     out1 <= sig_1;
    end if;
  end process;


  p_0: process (in1, rec_array)
      variable local_1 : std_logic_vector(9 downto 0);
   begin
      local_1 := (others => '0');
      for i in 0 to 9 loop
         if rec_array(i).field_0 = in1 then
           local_1(i) := '1';
         end if;
      end loop;

      -- check to see if local_1 is all 1's, if so set the last output bit
      if local_1 = "1111111111" then 
        out3 <= '1';
      else
        out3 <= '0';
      end if;

      -- although we know that when local_1 is all 1's that we get a sim
      -- mismatch, but if we uncomment the following line then there is NO sim mismatch
      -- local_1 := "1111111111";

      
      sig_0 <= local_1; -- make this value of local_1 visible

      
      local_1 := function1(local_1);

      sig_1 <= local_1;
   end process p_0;
  
end;
