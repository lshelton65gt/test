entity reverse_range_attrib is
  port(in1, in2 : in bit_vector(1 downto 0); out1 : out bit_vector(0 to 1) );
end;

architecture arch of reverse_range_attrib is
signal temp : bit_vector(in1'reverse_range);
begin
  temp <= in1 and in2;
  out1 <= temp;
end;

