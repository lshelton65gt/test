-- This testcase demonstrates that the Verific VHDL flow does not
-- populate the size of the integers in multi-d integer arrays
-- correctly. Verific does not seem to respect the integer 'range'
-- information.

package destypes is

   type intarray1 is array(integer range <>) of integer range 7 downto 0;
   type intarray2 is array(0 to 1) of intarray1(0 to 1);

end package;

use work.destypes.all;
entity top is
  port (
    clk : in bit;
    rst : in bit;
    din : in intarray2;
    dout: out intarray2
  );
end entity top;

architecture rtl of top is

begin

   process (clk, rst) is
   begin
      if (rst = '1') then
         dout <= ((0,0),(0,0));
      elsif (clk'event and clk = '1') then
         -- Unfortunately, these statements can cause
         -- runtime simulation errors in Modelsim 
         -- (value out of range). But the DO cause
         -- testdriver to produce different results
         -- between Interra and Verific flows.
         -- TODO: Come up with a better testcase that
         -- is legal VHDL, that shows sim time diffs.
         dout(0)(0) <= din(0)(0) + 15;
         dout(0)(1) <= din(0)(1) + 15;
         dout(1)(0) <= din(0)(0) mod 7;
         dout(1)(1) <= din(0)(1) mod 7;
      end if;
    end process;

end rtl;

   
