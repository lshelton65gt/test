-- This test our handling of default function argument values.
-- This previously caused a crash in the Verific flow because
-- comma separated inputs with a common type were not being counted
-- correctly.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
entity default_func_arg_value is
port( a : in integer;
      b : in integer;
      c : out integer
     );
end default_func_arg_value;

architecture rtl of default_func_arg_value is

function add(a,b,c,d,e : integer; f : integer := 15) return integer is
variable result : integer;
begin
     result := a + b + c + d + e + f;
     return result;
end ;

begin

 c <= add (a,b,a,b,a);

end architecture;




