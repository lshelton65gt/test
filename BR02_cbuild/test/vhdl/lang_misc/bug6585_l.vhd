-- last mod: Fri Sep 15 15:22:58 2006
-- filename: test/vhdl/lang_misc/bug6585_l.vhd
-- Description:  This test is a variation on bug6585_a.vhd,
-- it exits with error bug6585_l.vhd:46: Error 3505: Data type `NULL' is not supported 

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6585_l is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector(15 downto 0);
    out1     : out std_logic_vector(15 downto 0));
end;

architecture arch of bug6585_l is 
function level2 (v : std_logic_vector; len1 : integer; len2: integer) return std_logic_vector;
function level1 (v : std_logic_vector; pad : integer) return std_logic_vector;

function level2 (v : std_logic_vector; len1 : integer; len2 : integer) return std_logic_vector is 
 constant argsize : integer := v'length + len1;
 constant outsize1 : integer := v'length + len1 + len2 - 1;
 subtype rtype is std_logic_vector(outsize1-5 downto 0);
 variable retval : rtype;
begin
 retval := level1(v,1);
 return retval;
end level2;


function level1 (v : std_logic_vector; pad : integer) return std_logic_vector is
 constant outsize : integer := v'length + pad - 1;
 constant shift : integer := v'length - pad;
 subtype rtype is std_logic_vector(outsize-1 downto 0);
 variable retval : rtype;
begin 
 retval := conv_std_logic_vector(shl(unsigned(conv_std_logic_vector(unsigned(v), outsize)), conv_unsigned(shift,3)), outsize);
 return retval;
end level1;
  
begin
  main: process (in1, in2) is
  begin
     for i in 3 downto 0 loop
      out1(3+4*i downto 4*i) <=   unsigned(in2(3+4*i downto 4*i)) + unsigned( not level2(in1(3+4*i downto 4*i), 2, 3));
     end loop;  -- i
  end process;
end;
