-- This testcase reproduces customer 2 b issue 71.
--
-- issue71.vhd:28 Internal compiler error: Unsupported constant value - '(1,1)'

package destypes is

type coord is
  record
    x : integer range -1 to 1;
    y : integer range -1 to 1;
  end record;

type coord_array is array(natural range <>) of coord;
type coord_matrix is array(natural range<>) of coord_array(0 to 3);

constant VAL1 : coord_array := (
   (1,1), (1,0), (1,1), (1,0)
);

constant VAL2 : coord_array := (
   (-1,1), (-1,0), (1,1), (-1,0)
);

constant VAL3 : coord_array := (
   (1,1), (-1,0), (1,1), (-1,0)
);

constant VAL4 : coord_array := (
   (1,1), (1,0), (1,1), (-1,0)
);

constant table : coord_matrix(0 to 3) := (
   VAL1,
   VAL2,
   VAL3,
   VAL4
   );

end package;


library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.destypes.all;

entity issue71 is
  port (
        row : in integer range 0 to 3;
        col : in integer range 0 to 3;
        x : out integer range -1 to 1;
        y : out integer range -1 to 1
       );
end issue71;

architecture rtl of issue71 is
   signal pair : coord;
begin
   -- The failure occurs on this assignment
   pair <= table(row)(col);
   x <= pair.x;
   y <= pair.y;
end rtl;

