-- filename: test/vhdl/lang_misc/bug8257_02.vhd
-- Description:  This test is an extremely trimmed down version of the testcase
-- of the customer. This test probably does nothing resonable, it just
-- exercises the bug of 8257


library ieee;
use ieee.std_logic_1164.all;
package bug8257_pak is
constant M : integer := 6;
constant NUM_STATES : integer := 2**M;
constant METRIC_BITS : integer := 6;
subtype state_metric_t is std_logic_vector(METRIC_BITS-1 downto 0);
type state_metric_vector_t is array (natural range <>) of state_metric_t;
end bug8257_pak;

library ieee;
use ieee.std_logic_1164.all;
use work.bug8257_pak.all;

entity bug8257_02 is
  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector(5 downto 0);
    in2      : in  integer;
    in3      : in  std_logic_vector(5 downto 0);
    out1     : out std_logic_vector(5 downto 0));
end;

architecture arch of bug8257_02 is
signal states : state_metric_vector_t(0 to NUM_STATES-1);
signal normalize : std_logic_vector(NUM_STATES-1 downto 0);
signal in2_64 : integer := 0;           -- value of in2 limited to 0  -- 63
begin 


    
detect_normalization: process (states)
begin 

  -- the problem in bug 8257 is the way the following loop is optimized, and in
  -- particular the fact that the if condition is a memory, not a vector.
  
    normalize <= (others => '1');
    for i in 0 to NUM_STATES-1 loop
        if states(i)(states(i)'high)='1' then
            normalize <= (others => '0');
        end if;
    end loop;
end process;


  main: process (clock)
  begin 
    if clock'event and clock = '1' then 
      in2_64 <= in2;
      if (in2 < 0) or ( in2 > 63 ) then
        in2_64 <= 0;
      end if;
      states(in2_64) <= in1;
      out1 <= in3 and normalize(5 downto 0);
    end if;
  end process;
  end;
