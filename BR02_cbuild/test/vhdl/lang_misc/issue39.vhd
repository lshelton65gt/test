-- Customer 2 issue 39
-- Using "ext" function (in std_logic_arith package) with a non-constant
-- second argument (which determines the overall size of the result)
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
entity issue39 is
  port (
    clk : in std_logic;
    x1  : in integer range 0 to 15;
    din : in std_logic_vector(15 downto 0);
    dout : out std_logic_vector(31 downto 0)
  );
end issue39;

architecture rtl of issue39 is

begin

   process(clk, x1) is
      variable temp : integer;
   begin
      if (clk'event and clk = '1') then
        if (x1 < 15) then
           temp := 32;
        else
           temp := x1 + x1 + 2;
        end if;
        -- temp will always be 32 here
        dout <= ext(din, temp);
      end if;
   end process;
end rtl;
