--  April, 2007
-- This file tests the dynamic POS and VAL attributes for scalars.
-- Currently these attributes work only for character type.
-- In the future the support for other types will be added.
library ieee;
use ieee.std_logic_1164.all;

package pkgString is
  constant StrPosBeg  : integer  := 1;
  constant MaxStrLen  : integer  := 16;
  
  function f_UpperCase(c : character) return character;
  function f_Ascii2Int (c : character) return integer;
  function f_Int2Asci (pos : integer) return character;
  
  
end pkgString;

package body pkgString is

  function f_UpperCase(c : character) return character is
    variable v_a2A : integer := character'Pos('a')-character'Pos('A');
  begin
    if c >= 'a' and c <= 'z' then
      return character'val( character'Pos(c) - v_a2A );
    end if;

    return c;
  end f_UpperCase;

  function f_Ascii2Int (c : character) return integer is
  begin
    return character'Pos(c);
  end f_Ascii2Int;

  function f_Int2Asci (pos : integer) return character is
    variable temp1 : integer; 
  begin
    temp1 := pos;
    return character'val(temp1);
  end f_Int2Asci;
  
  
end pkgString;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.pkgString.all;

entity bug7190 is
  
  port (
    rst  : in  std_logic;
    clk  : in  std_logic;
    in1  : in  std_logic_vector(0 to 7);
    out1 : out std_logic_vector(0 to 7);
    out2 : out std_logic_vector(0 to 7));
end bug7190;

architecture bug7190arch of bug7190 is
  signal temp1  : std_logic_vector(0 to 7);
  signal temp2  : std_logic_vector(0 to 7);
--  subtype digit1 is integer range -5 to 8;
begin  -- bug7190arch


  P1: process (rst, clk)

  variable charInt : integer := 0;
    variable char1    : character := 'H';
    variable char2    : character := 'G';
    variable count   : integer := 0;
    
  begin  -- process P1
    if rst = '0' then
     count := StrPosBeg;
     temp1 <= "00000000";
     temp2 <= "00000000";
--     temp3 <= "00000000";

    elsif clk'event and clk = '1' then  -- rising clock edge
     char1 := f_UpperCase('c');
     temp1 <= conv_std_logic_vector(f_Ascii2Int(char1), 8);
     count := count + 1;

     char2 := f_Int2Asci(count);
     temp2 <= conv_std_logic_vector(f_Ascii2Int(char2), 8);
--     temp3 <= conv_std_logic_vector(digit1'val(count), 32);

     if count = MaxStrLen then
       count := StrPosBeg;
     end if;
     
    end if;

    
  end process P1;

  out1 <= temp1;
  out2 <= temp2;
--  out3 <= temp3;


end bug7190arch;

