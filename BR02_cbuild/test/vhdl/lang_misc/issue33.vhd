-- This reproduces issue 33 with the customer 2 testcase
library ieee;
use ieee.std_logic_1164.all;
package destypes is
   type grparray is array (0 to 4) of std_logic_vector(3 downto 0);
   constant ba : grparray := ((others => '0'), "0011", "1011", "1000", (others => '1'));
   constant wa : grparray := ((others => '0'), "0100", "1100", "1001", (others => '1'));
end package;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;
use work.destypes.all;
entity issue33 is
  port (
     a : in std_logic_vector(1 downto 0);
     out1 : out std_logic_vector(3 downto 0)
  );
end issue33;

architecture rtl of issue33 is
begin
   out1 <= ba(conv_integer(a)) when wa(conv_integer(a)) = ba(conv_integer(a)+1) else wa(conv_integer(a));
end rtl;
