-- variant on dyn_slice3 where there are three variable width slices
-- in the concatenation, and two indices, but also interleave four
-- fixed pieces into the concat

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity dyn_slice4 is  
    port (
         a : in std_logic_vector(63 downto 0);
         b : in std_logic_vector(63 downto 0);
         c : in std_logic_vector(63 downto 0);
         v : in std_logic_vector(7 downto 0);
         w : in std_logic_vector(7 downto 0);
         x : in std_logic_vector(7 downto 0);
         y : in std_logic_vector(7 downto 0);
         i : in unsigned(4 downto 0);
         j : in unsigned(3 downto 0);
         dout : out std_logic_vector(95 downto 0)
         );
end dyn_slice4;

architecture dyn_slice4 of dyn_slice4 is
 begin

   process (a, b, c, i, j, v, w, x, y)
     variable ii : integer;
     variable jj : integer;
      begin
        ii := to_integer(i);            -- i >= 0, i <= 31
        jj := to_integer(j) + ii + 2;   -- jj > ii, jj > 1, jj <= 48
        dout <= v &
                a(63 downto jj) &
                w &
                b(jj - 1 downto ii + 1) &
                x &
                c(ii downto 0) &
                y;
      end process;
end dyn_slice4;
