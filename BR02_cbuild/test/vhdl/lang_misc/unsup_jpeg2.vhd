library ieee;
use ieee.std_logic_1164.all;

entity unsup_jpeg2 is
  port (out1 : out std_logic_vector(17 downto 0));
end;

architecture arch of unsup_jpeg2 is
   signal en_bus             : std_logic_vector(15 downto 0);
   signal cu_enable          : std_logic;
   signal cs1_cld            : std_logic_vector (1 downto 0);
begin

  en_bus <= (others=>cu_enable);
--/home/cds/tmcbraye/dev/clean/src/localflow/VhPopulateModule.cxx:114: Error 30435:  Unexpected Object Type VH_ERROR_OBJECT_TYPE in vhGetFlatSignalList()
--unsup_jpeg2.vhdl:12: Error 3500: Unsupported expression

  cs1_cld <= (others=>'0');
--/home/cds/tmcbraye/dev/clean/src/localflow/VhPopulateModule.cxx:114: Error 30435:  Unexpected Object Type VH_ERROR_OBJECT_TYPE in vhGetFlatSignalList()
--unsup_jpeg2.vhdl:17: Error 3500: Unsupported expression
  out1 <= en_bus & cs1_cld;
end;
