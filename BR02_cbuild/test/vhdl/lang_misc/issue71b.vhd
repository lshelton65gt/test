-- This testcase reproduces sub-issue for customer 2b issue 71.
--
-- problem seems to be related that we don't recognize and populate NUCompositeExpr (nucleus type for composite concat expressions) as well as we are getting wrong size for concat sub-elements 

package destypes is

type coord is
  record
    x : integer range -1 to 1;
    y : integer range -1 to 1;
  end record;

type coord_array is array(0 to 3) of coord;

end package;


library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.destypes.all;

entity issue71b is
  port (
        row : in integer range 0 to 3;
        col : in integer range 0 to 3;
        x : out integer range -1 to 1;
        y : out integer range -1 to 1
       );
end issue71b;

architecture rtl of issue71b is
   signal pair_list : coord_array;
   signal pair : coord;
begin
   pair_list <= ((row,col), (row,0), (1,col), (-1,0));
   pair <= pair_list(0);
   x <= pair.x;
   y <= pair.y;
end rtl;

