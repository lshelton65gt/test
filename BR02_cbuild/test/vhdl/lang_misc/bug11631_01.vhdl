-- October, 2008
-- This test case was created based on the customer design.
-- The issue was with function call for initialization of record
-- fields.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity bug11631_01 is
  port (clk1, clk2, reset1, reset2 : in std_logic;
        out1, out2 : out std_logic_vector(31 downto 0));
end bug11631_01;

architecture arch of bug11631_01 is

constant PEL_W : natural := 1;
constant SUPPORT_MPEG4 : natural := 0;
constant SUPPORT_H264 : natural := 1;

  constant VERT_IN_PEL_W : natural := PEL_W;

  constant IPB_NUM_PEL_OUT : natural := 11    
        + (1-SUPPORT_MPEG4)*SUPPORT_H264*2   
        + SUPPORT_MPEG4*4;                    

  type vert_in_t is record
         ip0, ip1_h264, ip1_mpeg4,
         ip2, ip3, ip4, ip5, ip6, ip7,
         ip3_t1, ip4_t1  : std_ulogic_vector(VERT_IN_PEL_W-1 downto 0);
         ip_add : std_ulogic_vector(2*VERT_IN_PEL_W-1 downto 0);
  end record;
  type vert_in_array_t is array(0 to 12) of vert_in_t;

  signal vert_in    : vert_in_array_t;
 signal DONTCARE : std_ulogic_vector(31 downto 0);

 function ipb_pel(ipb : std_ulogic_vector; x : integer)
  return std_ulogic_vector is
    variable origin : natural;
    variable lsb : natural;
  begin
    if SUPPORT_MPEG4=1 then  
      origin := 11;
    elsif SUPPORT_H264=1 then 
      origin := 10;
    else  
      origin := 9;
    end if;
    lsb := PEL_W*(origin-x);
    return ipb(lsb+PEL_W-1 downto lsb);
  end;

signal ipb_pel_out              : std_ulogic_vector(IPB_NUM_PEL_OUT*PEL_W-1 downto 0);
signal ipb_add_out              : std_ulogic_vector(2*IPB_NUM_PEL_OUT*PEL_W-1 downto 0);

begin
  p1: process(clk1)
    variable out_tmp : std_logic_vector(31 downto 0);
  begin
    if (clk1'event and clk1 = '1') then
      if (reset1 = '1') then
        out_tmp := (others => '0');
      else
        out_tmp := out_tmp + 1;
      end if;
    end if;



    out1 <= out_tmp;
  end process;

VERT_H264 : if SUPPORT_H264=1 generate


  vert_in(0) <= ( ip0       => DONTCARE(VERT_IN_PEL_W-1 downto 0),
                  ip1_mpeg4 => DONTCARE(VERT_IN_PEL_W-1 downto 0),
                  ip1_h264  => ipb_pel (ipb_pel_out,  -2), -- this line is tested
                  ip2       => DONTCARE(VERT_IN_PEL_W-1 downto 0),
                  ip3       => DONTCARE(VERT_IN_PEL_W-1 downto 0),
                  ip4       => DONTCARE(VERT_IN_PEL_W-1 downto 0),
                  ip5       => DONTCARE(VERT_IN_PEL_W-1 downto 0),
                  ip6       => DONTCARE(VERT_IN_PEL_W-1 downto 0),
                  ip7       => DONTCARE(VERT_IN_PEL_W-1 downto 0),
                  ip3_t1    => DONTCARE(VERT_IN_PEL_W-1 downto 0),
                  ip4_t1    => DONTCARE(VERT_IN_PEL_W-1 downto 0),
                  ip_add    => ipb_pel (ipb_add_out,  -2) -- this line is tested
 );

 end generate VERT_H264;

  p2: process(clk2)
    variable out_tmp : std_logic_vector(31 downto 0);
  begin
    if (clk2'event and clk2 = '1') then
      if (reset2 = '1') then
        out_tmp := (others => '0');
      else
        out_tmp := out_tmp + 3;
      end if;
    end if;
    out2 <= out_tmp;
  end process;
  
end arch;
