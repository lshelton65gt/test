-- Customer 2 issue 39, extended test b
-- Using "ext" function (in std_logic_arith package) with a non-constant
-- second argument (which determines the overall size of the result)
-- extended version of issue39a, with expected output

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
--use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
entity issue39b is
  generic (
    LHS_WIDTH : integer := 32 
  );
  port (
    clk : in std_logic;
    x1  : in integer range 0 to 2;
    x2  : in integer range 0 to 15;
    din2 : in std_logic_vector(1 downto 0);
    din5 : in std_logic_vector(4 downto 0);
    din9 : in std_logic_vector(8 downto 0);
    dout1 : out std_logic_vector(LHS_WIDTH - 1 downto 0);
    dout2 : out std_logic_vector(LHS_WIDTH - 1 downto 0);
    dout_diff : out std_logic
  );
end issue39b;

architecture rtl of issue39b is
   signal mask1 : std_logic_vector(LHS_WIDTH - 1 downto 0);
   signal mask2 : std_logic_vector(LHS_WIDTH - 1 downto 0);
   signal mask3 : std_logic_vector(LHS_WIDTH - 1 downto 0);
   signal dout1_temp : std_logic_vector(LHS_WIDTH - 1 downto 0);
   signal dout2_temp : std_logic_vector(LHS_WIDTH - 1 downto 0);
begin
   process(clk, x1) is
        variable U1: unsigned (LHS_WIDTH - 1 downto 0);
        variable COUNT1, COUNT2, COUNT3, COUNT4, COUNT5: unsigned (LHS_WIDTH - 1 downto 0);
        variable TEMP1, TEMP2, TEMP3: unsigned (LHS_WIDTH - 1 downto 0);
        variable VALUE1, VALUE2, VALUE3: unsigned (LHS_WIDTH - 1 downto 0);
   begin
      if (clk'event and clk = '1') then
        dout1_temp <= (ext(din9, (LHS_WIDTH-(x1+x2))) & ext(din5, (x2)) & ext(din2, x1));
        -- the expressions we want to create here are: 
        -- `define LHS_WIDTH = dout'width;  // this must be a constant
        -- reg [(LHS_WIDTH-1)0] mask1 = (1<<(32-(x1+x2)))-1;  // mask* values defined at runtime based on x1 and x2
        -- reg [(LHS_WIDTH-1)0] mask2 = (1<<(x2))-1;
        -- reg [(LHS_WIDTH-1)0] mask3 = (1<<(x1))-1;
        -- shift amounts defined at runtime.
        --  dout <=  ((ext(din9,LHS_WIDTH) & mask1) << (x2+x1)) ||
        --           ((ext(din5,LHS_WIDTH) & mask2) << (x1)) ||
        --           ((ext(din2,LHS_WIDTH) & mask3) << (0)))
        -- Construct Masks
        U1 := "00000000000000000000000000000001";
        -- Converting from integer to unsigned
        COUNT1 := CONV_UNSIGNED(ARG => (LHS_WIDTH - (x1 + x2)), SIZE => LHS_WIDTH);
        TEMP1 := SHL(U1, COUNT1) - 1;
        -- Converting unsigned to std_logic_vector
        mask1 <= std_logic_vector(TEMP1);
        COUNT2 := CONV_UNSIGNED(ARG => x1, SIZE => LHS_WIDTH);
        TEMP2 := SHL(U1, COUNT2) - 1;
        mask2 <= std_logic_vector(TEMP2);
        COUNT3 := CONV_UNSIGNED(ARG => x2, SIZE => LHS_WIDTH);
        TEMP3 := SHL(U1, COUNT3) - 1;
        mask3 <= std_logic_vector(TEMP3);
        COUNT4 := CONV_UNSIGNED(ARG => (x1 + x2), SIZE => LHS_WIDTH); 
        COUNT5 := CONV_UNSIGNED(ARG => x1, SIZE => LHS_WIDTH);
        -- Converting std_logic_vector expression to unsigned
        VALUE1 := unsigned(ext(din9,LHS_WIDTH) and mask1);
        VALUE2 := unsigned(ext(din5,LHS_WIDTH) and mask2);
        VALUE3 := unsigned(ext(din2,LHS_WIDTH) and mask3);
        -- Create final expression
        dout2_temp <= (std_logic_vector(SHL(VALUE1, COUNT4)) or std_logic_vector(SHL(VALUE2, COUNT5)) or std_logic_vector(VALUE3));
        dout1 <= dout1_temp;
        dout2 <= dout2_temp;
        if (dout1_temp = dout2_temp) then
           dout_diff <= '0';
        else
           dout_diff <= '1';
        end if;
        
      end if;
   end process;
end rtl;
