-- filename: test/vhdl/lang_misc/issue70.vhd
-- Description:  This test demonstrates an issue observed that verific flow
-- prints a message of the form:
-- issue70.vhd:28: Warning 3080: Statement in an initial block uses a non-constant value, it will be eliminated
-- but the statement is a constant and it is still populated.


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity issue70 is
  generic (
    WIDTH : integer := 8;
    HALF_WIDTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector((WIDTH-1) downto 0);
    out1     : out std_logic_vector((WIDTH-1) downto 0));
end;

architecture arch of issue70 is 
    type e_lut_col  is array(0 to 1) of std_logic_vector(HALF_WIDTH - 1 downto 0);
    type e_lut_type is array(0 to 2) of e_lut_col;

  
 constant temp1 : e_lut_type := ( (conv_std_logic_vector(-2, HALF_WIDTH), conv_std_logic_vector(1, HALF_WIDTH)),
                                  (conv_std_logic_vector(-4, HALF_WIDTH), conv_std_logic_vector(3, HALF_WIDTH)),
                                  (conv_std_logic_vector(-8, HALF_WIDTH), conv_std_logic_vector(5, HALF_WIDTH))
                                   );
   signal p2   : std_logic;
   signal p3   : std_logic;
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1 <= in1 and (temp1(0)(0) & temp1(1)(1));
    end if;
  end process;
end;
