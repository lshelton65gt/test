-- This testcase reproduces customer 2 b issue 72
--
-- NUExpr.cxx:1172 NU_ASSERT(!net->is2DAnything()) failed

library ieee;
use ieee.std_logic_1164.all;

entity issue72 is
  port (
         clk : in std_logic;
         in1 : in std_logic;
         out1 : out integer range 0 to 128
       );
end issue72;

architecture rtl of issue72 is
  type array_type is array (2 downto 0) of integer range 0 to 128;

  function f1(i1 : std_logic) return array_type is
    variable temp : array_type;
  begin
    temp := (others=>0);
    if i1 = '1' then
      temp(0) := 0;
      temp(1) := 57;
      temp(2) := 31;
    else
      temp(0) := 21;
      temp(1) := 88;
      temp(2) := 77;
    end if;
    return temp;      
   end;
begin

   process (clk) is
     variable temp : array_type;
   begin 
     if (clk'event and clk = '1') then
       temp := f1(in1);
       out1 <= temp(0);
     end if;
   end process;

end rtl;

