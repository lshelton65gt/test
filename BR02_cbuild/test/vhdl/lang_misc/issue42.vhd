-- Reproduces issue 42 exposed by customer 2 testcase.
-- 'or_reduce' produces a 2-bit return value. It should 
-- produce a 1-bit return value.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
entity child is
  generic (
      n : integer
  );
  port (
      in1 : in std_logic_vector(n-1 downto 0);
      out1 : out std_logic_vector(n downto 0)
  );
end child;

architecture rtl of child is
begin

   out1 <= in1 & or_reduce(in1);

end rtl;

-----------------------------------------

library ieee;
use ieee.std_logic_1164.all;
entity issue42 is
  port (
      in1 : in std_logic_vector(15 downto 0);
      in2 : in std_logic_vector(7 downto 0);
      out1 : out std_logic_vector(16 downto 0);
      out2 : out std_logic_vector(8 downto 0)
  );
end issue42;

architecture rtl of issue42 is
begin

inst1: entity work.child(rtl) generic map(16) port map (in1, out1);
inst2: entity work.child(rtl) generic map(8)  port map  (in2, out2);

end rtl;
