-- this variation adds another assignment to other bits in dout,
-- so tha pack-assigns creates a concat in the backend.  This fails
-- unless we remove all the 'downto' constructs up front, not just
-- the ones that were initially populated inside concats.


-- note, Aldec gives bogus results for this (all Zs).  The gold
-- file was created with MTI.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity dyn_slice6 is  
    port (
         a : in std_logic_vector(31 downto 0);
         b : in std_logic_vector(15 downto 0);
         idx : in unsigned(3 downto 0);
         dout : out std_logic_vector(31 downto 0)
         );
end dyn_slice6;

architecture dyn_slice6 of dyn_slice6 is
 begin

   process (a, b, idx)
     variable idx_int : integer;
      begin
        idx_int := to_integer(idx);
        dout(15 downto 0) <= a((idx_int + 15) downto idx_int);
        dout(31 downto 16) <= b;
      end process;
end dyn_slice6;
