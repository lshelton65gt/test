-- filename: test/vhdl/lang_misc/bug8606_01.vhd
-- Description:  This test duplicates the issue seen in bug 8606,
-- when run with observeSignal *.* this generated an internal error.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use IEEE.std_logic_signed.all;

entity flop is 
  port (
    D : in std_logic_vector(3 downto 0);
    CLK : IN  std_ulogic;
    CLR : IN  std_ulogic;
    Q   : OUT std_logic_vector(3 downto 0)
    );
end;

architecture flop_arc of flop is
   procedure  b2i (signal S:std_logic_vector; R:out integer) is
     variable result : integer := 0;
   begin
     R := ieee.std_logic_unsigned.conv_integer(S);
   end b2i;
begin

  named1 : process(CLK, CLR)
  variable A_VAL : integer := -1;
  begin
    if (CLR = '1') then
      Q <= "0000";
    elsif (CLK = '1' and CLK'EVENT) then 
      b2i(D,A_VAL);
      
      Q <= std_logic_vector(CONV_UNSIGNED(A_VAL, 4));
    end if;
  end process;
end;


library ieee;
use ieee.std_logic_1164.all;

entity bug8606_01 is
  port (
      D    : IN     std_logic_vector(15 DOWNTO 0);
      aclk : IN     std_ulogic;
      rst  : IN     std_ulogic;
      Q    : OUT    std_logic_vector(15 DOWNTO 0)
     );
end;

architecture arch of bug8606_01 is

  COMPONENT flop
    PORT (
      D : in std_logic_vector(3 downto 0);
      CLK : IN  std_ulogic;
      CLR : IN  std_ulogic;
      Q   : OUT std_logic_vector(3 downto 0)
      );
  END COMPONENT;
begin
  gena : for i in 0 to 3 generate
    flop1 : flop
      PORT MAP (
        D   =>    D((i*4) + 3 downto (i*4)),
        CLK =>    aclk,
        CLR =>    rst,
        Q   =>    Q((i*4) + 3 downto (i*4))
      );
  end generate;
end;

