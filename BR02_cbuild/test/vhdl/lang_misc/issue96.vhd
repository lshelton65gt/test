-- filename: test/vhdl/lang_misc/issue96.vhd
-- Description:  This test duplicates a problem seen in Imagination Rogue
-- ******CARBON INTERNAL ERROR*******
--
-- Detected an unconstrained formal on a subprogram call
-- related to conv_integer


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; -- for to_integer()
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;

entity issue96 is
  generic (
    MAX_TAG     : integer := 3;
    WIDTH_TAG   : integer := 2;
    DEPTH_LOG2  : integer := 5;
    DEPTH       : integer := 32
);

  port (
    clock    : in  std_logic;
    rd_tag        : in  std_logic_vector(WIDTH_TAG-1 downto 0);
    next_ptr_in_linked_list  : out std_logic_vector(DEPTH_LOG2-1 downto 0));
end;

architecture arch of issue96 is
   type ram_ptr_array_type is array (natural range <>) of std_logic_vector(DEPTH_LOG2-1 downto 0);
   type tag_ptr_type is array (natural range <>) of std_logic_vector(DEPTH_LOG2-1 downto 0);
   signal ram_ptr_array : ram_ptr_array_type(DEPTH-1 downto 0) := ("00000","00001","00010","00011","00100","00101","00110","00111",
                                                                   "01000","01001","01010","01011","01100","01101","01110","01111",
                                                                   "10000","10001","10010","10011","10100","10101","10110","10111",
                                                                   "11000","11001","11010","11011","11100","11101","11110","11111");
   signal rd_ptr_for_tag : tag_ptr_type(MAX_TAG downto 0) := ("00000","00001","00010","00011");

begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then 
      next_ptr_in_linked_list  <= ram_ptr_array( conv_integer(rd_ptr_for_tag(conv_integer(rd_tag))) );
    end if;
  end process;
end;
