-- bug7030_1
-- The use clauses are commented out and the full paths to all 3 entities are defined.
-- This test compiles successfully.
-- ModelSim compiles fine as well.

library ieee;
use ieee.std_logic_1164.all;


library sub1_lib;
--use sub1_lib.all;
library sub2_lib;
--use sub2_lib.all;
library pack_lib;
--use pack_lib.subPack.all;


entity bug7030_1 is
  
  port (
    clk  : in  std_logic;
    in1  : in  std_logic;
    out1 : out std_logic;
    out2 : out std_logic;
    out3 : out std_logic);
  

end bug7030_1;


architecture arch of bug7030_1 is
begin  -- arch

  s1 : entity sub1_lib.sub port map (
    clk  => clk,
    inS  => in1,
    outS => out1);

  s2 : entity sub2_lib.sub  port map (
    clk  => clk,
    inS  => in1,
    outS => out2);

--  s3 : sub2only  port map (
    s3 : pack_lib.subPack.sub2only  port map (
    clk  => clk,
    inS  => in1,
    outS => out3);

end arch;

