-- Tests to confirm that temp variable is not constant
-- propagated since it's protected mutable (depositable).
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity deposit is
  port (
    clk : in std_logic;
    sel : in std_logic_vector(1 downto 0);
    in1 : in std_logic_vector(5 downto 0);
    out1 : out std_logic_vector(5 downto 0);
    out2 : out std_logic
    );
end deposit;


architecture deposit of deposit is
begin

  proc1: process (in1)
    variable temp : integer := 3;
  begin
    if (temp = 3) then -- temp can be constant propagated as 3 here so that the
                       -- condition is always true. However since temp is
                       -- depositable, it should not be constant propagated.
      out2 <= '1';
    else
      out2 <= in1(0);
    end if;
  end process;

  proc2: process (clk, sel, in1)
    variable int_sel : integer := 0; -- carbon observeSignal
  begin
    if (clk'event and clk = '1') then
      int_sel := conv_integer(unsigned(sel));
      if (int_sel = 0) then
        out1 <= "110110" and in1;
      elsif (int_sel = 1) then
        out1 <= not in1;
      elsif (int_sel = 2) then
        out1 <= "011110" xor in1;
      else
        out1 <= in1;
      end if;
    end if;
  end process;

end deposit;
