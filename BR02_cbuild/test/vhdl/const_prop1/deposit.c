/* Hand-written C testbench to drive deposit example */

#define CARBON_NO_UINT_TYPES 1  /* avoid typedefing for UInt32, etc */
#include "libdeposit.h"   /* generated header file */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* Convenience routine to examine one 32 bit value */
static unsigned long examine1(CarbonObjectID *model,
                              CarbonNetID* net)
{
  CarbonUInt32 val;
  (void) carbonExamine(model, net, &val, 0);
  return val;
}

static unsigned long examine1Input(CarbonObjectID *model,
                                   CarbonNetID* net)
{
  CarbonUInt32 val;
  (void) carbonExamine(model, net, &val, 0);
  return val;
}

int main()
{
  CarbonTime t = 0;
  int clk1tick = 0;
  int clk2tick = 0;
  CarbonNetID *clk, *sel, *in1, *temp, *out1, *out2;
  CarbonWaveID *fsdb;
  CarbonUInt32 one = 0;
  CarbonUInt32 value = 0;

  /* Instantiate a two counter model */
  CarbonObjectID *deposit =
    carbon_deposit_create(eCarbonIODB, eCarbon_NoFlags);

  if (deposit == NULL)
  {
    printf("Unable to create Carbon model\n");
    exit(EXIT_FAILURE);
  }

  /* Initialize wave-form dumper for FSDB format output */
  fsdb = carbonWaveInitFSDB (deposit, "deposit.fsdb", e1us);

  /* Get handles to all the I/O nets */
  clk = carbonFindNet(deposit, "deposit.clk");
  assert(clk);
  sel = carbonFindNet(deposit, "deposit.sel");
  assert(sel);
  in1 = carbonFindNet(deposit, "deposit.in1");
  assert(in1);
  temp = carbonFindNet(deposit, "deposit.proc1.temp");
  assert(temp);
  out1 = carbonFindNet(deposit, "deposit.out1");
  assert(out1);
  out2 = carbonFindNet(deposit, "deposit.out2");
  assert(out2);

  CarbonWaveUserDataID* mysig =
    carbonAddUserData(fsdb, eCARBON_VT_VHDL_SIGNAL,
                      eCarbonVarDirectionInput, 
                      eCARBON_DT_VHDL_STD_ULOGIC,
                      0, 0, NULL, "mysig", eCARBON_BYTES_PER_BIT_1B,
                      "deposit", "/");

  /* Dump all primary i/o's */
  carbonDumpVars (fsdb, 1, "deposit");

  carbonDeposit(deposit, temp, &one, NULL);

  for (t = 0; t < 100000; t += 100)
  {
    /*
     * Advance clk1 every 700 ticks
     */
    if ((t % 700) == 0)
    {
      CarbonUInt32 val;
      ++clk1tick;
      val = clk1tick & 1;
      carbonDeposit(deposit, clk, &val, NULL);

      /* After 5 edges deposit a 0 on temp */ 
/*       if (clk1tick > 5) */
/*         carbonDeposit(deposit, temp, &one, NULL); */

      ++value;
      carbonDeposit(deposit, in1, &value, NULL);

      //carbonWaveUserDataChange(mysig);
    }

    /*
     * Run a schedule, print out results
     */
    carbonSchedule(deposit, t);
    fprintf(stdout,
            "%lu:\clk=%ld sel=%ld in1=%ld temp=%ld out1=%ld out2=%ld\n",
            (unsigned long) t,
            examine1Input(deposit, clk),
            examine1Input(deposit, sel),
            examine1Input(deposit, in1),
            examine1Input(deposit, temp),
            examine1(deposit, out1),
            examine1(deposit, out2));
  }

  assert (carbonDumpFlush (fsdb) == eCarbon_OK);
  carbonDestroy(&deposit);
  return 0;
}
