-- Test port associations of record parts through the mixed-language interface.
-- Gold sim file generated with Aldec
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(7 downto 0);
    end record;
  type bundlerec is array(0 to 1) of rec;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity mixed1 is
  port (
    en : in std_logic;
    recinport_r1  : in  std_logic_vector(15 downto 0);
    recoutport_r1 : out std_logic_vector(7 downto 0));
end;

architecture arch of mixed1 is
  component dut
    is
      port ( en : in std_logic;
             in1 : in std_logic_vector(7 downto 0);
             out1 : out std_logic_vector(7 downto 0);
             inout1 : inout std_logic_vector(7 downto 0)
             );
  end component;
  signal s1,s2 : rec := ("10101010", "01010101");
begin

  s1.r1 <= recinport_r1(15 downto 8);
  s1.r2 <= recinport_r1(7 downto 0);
  
  c1 : dut
    port map (en, s1.r1, s1.r2, s2.r1);

  recoutport_r1 <= s1.r2(4 downto 1) & s2.r1(6 downto 3);
end arch;
