-- use a record on the LHS of a selected signal assignment statment
-- at one time this resulted in an internal error related to new/old record
-- conflict because an old record routine was called when new-records were
-- enabled.
-- this issue is reported in bug 7672


package p is

  type rec1 is record 
                 f0  : integer;
                 f1  : integer;
                 f2  : integer;
                 f3  : integer;
                 f4  : integer;
                 f5  : integer;
                 f6  : integer;
                 f7  : integer;
                 f8  : integer;
                 f9  : integer;
               end record;

  type two_outs is record
                     out1 : integer;
                     out2 : integer;
                   end record;
  
end p;

use work.p.all;

entity selected_signal_assign1 is
  port ( clk  : in  bit;
         sel1 : bit_vector( 0 to 1);
         sel2 : bit_vector( 1 downto 0);
         output1 : out integer;
         output2 : out integer
         );
end selected_signal_assign1;

architecture selected_signal_assign_arch of selected_signal_assign1 is 
  signal pattern : rec1 := (1,2,3,4,5,6,7,8,9,10);
  signal out_rec : two_outs;
begin 
  output1 <= out_rec.out1;
  output2 <= out_rec.out2;

  with sel1 select
    out_rec <=
    (pattern.f1,pattern.f6) when "00",
    (pattern.f2,pattern.f7) when "01",
    (pattern.f3,pattern.f8) when "10",
    (pattern.f4,pattern.f9) when "11";
end;
