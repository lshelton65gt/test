-- last mod: Wed Jan 18 09:17:06 2006
-- filename: test/vhdl/records/vec_of_record_1.vhd
-- Description:  This test has a vector of records on the portlist of the top
-- entity. It tests support for OTHERS in a record aggregate, and for records
-- in packages.
library IEEE;
use IEEE.std_logic_1164.all;

package target is

subtype vec_type is std_logic_vector(3 downto 0);


type rec_type is record
  firstaddr	: vec_type;
  lastaddr	: vec_type;
end record;

type vec_of_rec is array (Natural Range <> ) of rec_type;
constant empty_vec_of_rec : rec_type :=
  ((others => '0'), (others => '0'));

constant const_vec_of_rec : vec_of_rec(0 to 3) := (
  ("0000", "0010"),
  ("0100", "1000"),
   others => empty_vec_of_rec);


end;

library ieee;
use ieee.std_logic_1164.all;
use work.target.all;

entity dut is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector(7 downto 0);
    out1     : out std_logic_vector(7 downto 0);
    out2     : out vec_of_rec(0 to 3));
end;

architecture arch of dut is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1 <= in1 and in2;
      out2 <= const_vec_of_rec;
    end if;
  end process;
end;


library ieee;
use ieee.std_logic_1164.all;
use work.target.all;

entity vec_of_record_1 is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector(7 downto 0);
    out1     : out std_logic_vector(7 downto 0);
    out2_0_firstaddr : out std_logic_vector(3 downto 0);
    out2_0_lastaddr : out std_logic_vector(3 downto 0);
    out2_1_firstaddr : out std_logic_vector(3 downto 0);
    out2_1_lastaddr : out std_logic_vector(3 downto 0);
    out2_2_firstaddr : out std_logic_vector(3 downto 0);
    out2_2_lastaddr : out std_logic_vector(3 downto 0);
    out2_3_firstaddr : out std_logic_vector(3 downto 0);
    out2_3_lastaddr : out std_logic_vector(3 downto 0));
end;

architecture arch of vec_of_record_1 is
  component dut is
    port (
      clock    : in  std_logic;
      in1, in2 : in  std_logic_vector(7 downto 0);
      out1     : out std_logic_vector(7 downto 0);
      out2     : out vec_of_rec(0 to 3));
  end component;

begin

  u1 : dut port map (
    clock             => clock,
    in1               => in1,
    in2               => in2,
    out1              => out1,
    out2(0).firstaddr => out2_0_firstaddr,
    out2(0).lastaddr  => out2_0_lastaddr,
    out2(1).firstaddr => out2_1_firstaddr,
    out2(1).lastaddr  => out2_1_lastaddr,
    out2(2).firstaddr => out2_2_firstaddr,
    out2(2).lastaddr  => out2_2_lastaddr,
    out2(3).firstaddr => out2_3_firstaddr,
    out2(3).lastaddr  => out2_3_lastaddr
    );

end arch;
