-- This tests the handling of an unassociated record port
library IEEE;
use IEEE.std_logic_1164.all;

package merpack is
type MY_RECORD_TYPE is record
  ch0_select : std_logic;
  ch1_select : std_logic;
end record;
end merpack;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.merpack.all;

entity mer_regs is
port (
  data : in std_logic_vector(1 downto 0);
  MY_REC : buffer MY_RECORD_TYPE;
  UNUSED : buffer MY_RECORD_TYPE
);
end mer_regs;

architecture functional of mer_regs is
  begin
  MY_REC.ch0_select <= data(0);
  MY_REC.ch1_select <= data(1);
end functional;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.merpack.all;

entity mer is
port (
  data : in std_logic_vector(1 downto 0);
  MY_REC0 : buffer MY_RECORD_TYPE;
  MY_REC1 : buffer MY_RECORD_TYPE
  );
end mer;

architecture functional of mer is

  component mer_regs
  port (
    data : in std_logic_vector(1 downto 0);
    MY_REC : buffer MY_RECORD_TYPE;
    UNUSED : buffer MY_RECORD_TYPE
  );
  end component;

  begin
    REGS_0 : mer_regs
    port map (
      data => data,
      MY_REC => MY_REC0,
      UNUSED => MY_REC1
    );
  end functional;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.merpack.all;

entity bug4891small is
port (
  data : in std_logic_vector(1 downto 0);
  MY_REC : buffer MY_RECORD_TYPE
  );
end bug4891small;

architecture functional of bug4891small is
  component mer
  port (
    data : in std_logic_vector(1 downto 0);
    MY_REC0 : buffer MY_RECORD_TYPE;
    MY_REC1 : buffer MY_RECORD_TYPE
  );
  end component;
begin

  MER0 : mer
    port map (
      data => data,
                        -- note that MY_REC0 is unassociated in this port map
      MY_REC1 => MY_REC
    );

end functional;

