file: test/vhdl/records/testplan.ol

record (composite type) test coverage matrix:
* record defined as variable (bug3940_1.vhd)
 ** assignment to single element (bug3940_1.vhd)
 ** read from element 
 ** assignment of whole record to whole record (record4.vhd)
* record defined as signal
 ** assignment to single element (record1.vhd) (bug3820.vhd)
 ** read from element (record1.vhd)
 ** assignment of whole record to whole record (record4.vhd) (bug3820.vhd)
* record used as argument to function (bug3820_3.vhd)
* function returns a record (bug3820_2.vhd  bug3307)
* record declared as a local in a function
* record used in sequentaial statement
 ** record used in signal assignment statement (LHS, RHS)
 ** variable assignment statement (LHS, RHS)
 ** record used in case expression
 ** record used in procedure call
 ** record used in if statement condition
 ** record used in loop statement
 ** record used in next statement
 ** record used in exit statement
 ** record used in return statement
 ** record used in wait statement
 ** record used in assertion statement
* aggregate support
 ** assign of a record from an aggregate
 ** assign to an aggregate from a record
* record used in formal arg of procedure
 ** input 
 ** inout
 ** output
* record used in actual arg of procedure
* record declared as a local in a procedure
* record appears in port of top module 
 ** input (record5.vhd)
 ** output (record5.vhd)
 ** inout
* Record defined in package (record2.vhd)
 ** assignment to record field
 ** read of record field
 ** assignment to full record from full record
* select bits from a record field (record3.vhd)
* array of records
 ** on formal and actual of ports simultaneously (test/bugs/bug4001)
 ** on primary ports
 ** signal assignment to whole record within an array (record9.vhd)
 ** read from whole record within array (record9.vhd)
 ** signal assignment to field of a record from array (record11.vhd)
 ** read from field of record from array(record11.vhd)
 ** read from partsel of field of array defined in an array (bug3331clks.vhd downto range) (bug3331clks_to.vhd to range)
 ** write to partsel of field of array defined in an array (bug3331clks.vhd downto range) (bug3331clks_to.vhd to range)
 ** 
* nested records
 ** as signals
 ** as variable
* 
