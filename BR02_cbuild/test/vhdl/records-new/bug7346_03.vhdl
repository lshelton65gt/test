-- May 2007
-- The  signal rec_arr is of record type. It is  instantiated in the
-- package declaration. The record has an array as it's element.
-- The entity uses the signal in the left and right
-- side assignment.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is

  type test is array (1 to 4) of integer;
  type complex_fixed_type is
    record
      test1 : test;
    end record;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;


package FFT_CORE_SIG_PKG is
  signal rec_arr    : complex_fixed_type;
end FFT_CORE_SIG_PKG;
library ieee;
use ieee.std_logic_1164.all;
use work.FFT_CORE_SIG_PKG.all;
entity bug7346_03 is
  port (
    clock      : in  std_logic;
    out1       : out integer;
    out2       : out integer;
    out3       : out integer;
    out4       : out integer);
end bug7346_03;

architecture arch of bug7346_03 is
  signal temp1 : integer := 10;
  signal temp2 : integer := 20;
  signal temp3 : integer := 30;
  signal temp4 : integer := 40;
begin
  main: process (clock, rec_arr )
  begin 
    if clock'event and clock = '1' then
      rec_arr.test1(1) <= temp1;
      rec_arr.test1(2) <= temp2;
      rec_arr.test1(3) <= temp3;
      rec_arr.test1(4) <= temp4;
    end if;

    out1 <= rec_arr.test1(1);
    out2 <= rec_arr.test1(2);
    out3 <= rec_arr.test1(3);
    out4 <= rec_arr.test1(4);
    
  end process;
end;
