-- May 2007
-- The signal mem_data is of record type. It is  defined in the
-- package declaration. The entity uses the signal in the left and right
-- side assignment. The major difference of this test case is that there is
-- another module, not top one, which uses the signal as well.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library ieee;
use ieee.std_logic_1164.all;
library lib1;
use lib1.FFT_CORE_SIG_PKG.all;
entity bug7346_04 is
  port (
    in1        : in integer;
    in2        : in integer;
    out1       : out integer;
    out2       : out integer);
end bug7346_04;

architecture arch_top of bug7346_04 is
begin

  mem_data.real1 <= in1;
  mem_data.imag1 <= in2;
  i_sub : entity sub 
     port map (
         out1  => out1,
         out2  => out2);
end;
