-- Test non-port unconstrained type arrayed nested records concurrent
-- assignment.  Also test others association with a nested record
-- subelement.
library ieee;
use ieee.std_logic_1164.all;

package pack is
  -- bottomrec is 5 bits total
  type bottomrec is record
                      b1     : std_logic;
                      t2     : std_logic_vector(3 downto 0);  -- reuse field name
                    end record;

  -- midrec is 11 bits total
  type midrec is record
                   mid_nest1 : bottomrec;
                   m2        : std_logic;
                   mid_nest2 : bottomrec;  -- multiple instance of the same
                                           -- record type
                 end record;

  type bundlerec is array(natural range<>) of midrec;
end pack;


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity nestedrecord6bdut is
  port ( clk, reset: in std_logic;
         recinport : in midrec;
         recoutport1: out midrec;
         recoutport2: out midrec );
end nestedrecord6bdut;

architecture a of nestedrecord6bdut is
  signal sigrec : bundlerec(0 to 1);
begin
  sigrec(1) <= ( m2 => sigrec(0).mid_nest2.b1,
                 others => sigrec(0).mid_nest1 );
  sigrec(0) <= recinport;
  recoutport1 <= sigrec(0);
  recoutport2 <= sigrec(1);
end a;


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity nestedrecord6b is
  port (
    clk, reset     : in  std_logic;
    recinport_m2   : in  std_logic;
    recinport_1_b1 : in  std_logic;
    recinport_1_t2 : in  std_logic_vector(3 downto 0);
    recinport_2_b1 : in  std_logic;
    recinport_2_t2 : in  std_logic_vector(3 downto 0);
    
    recoutport1_m2   : out  std_logic;
    recoutport1_1_b1 : out  std_logic;
    recoutport1_1_t2 : out  std_logic_vector(3 downto 0);
    recoutport1_2_b1 : out  std_logic;
    recoutport1_2_t2 : out  std_logic_vector(3 downto 0);

    recoutport2_m2   : out  std_logic;
    recoutport2_1_b1 : out  std_logic;
    recoutport2_1_t2 : out  std_logic_vector(3 downto 0);
    recoutport2_2_b1 : out  std_logic;
    recoutport2_2_t2 : out  std_logic_vector(3 downto 0)
    );
end nestedrecord6b;

architecture a of nestedrecord6b is
  component nestedrecord6bdut
    is
      port ( clk, reset: in std_logic;
             recinport : in midrec;
             recoutport1: out midrec;
             recoutport2: out midrec );
  end component nestedrecord6bdut;

begin

  dut : nestedrecord6bdut port map (
    clk            => clk,
    reset          => reset,
    recinport.m2 => recinport_m2,
    recinport.mid_nest1.b1 => recinport_1_b1,
    recinport.mid_nest1.t2 => recinport_1_t2,
    recinport.mid_nest2.b1 => recinport_2_b1,
    recinport.mid_nest2.t2 => recinport_2_t2,
    
    recoutport1.m2 => recoutport1_m2,
    recoutport1.mid_nest1.b1 => recoutport1_1_b1,
    recoutport1.mid_nest1.t2 => recoutport1_1_t2,
    recoutport1.mid_nest2.b1 => recoutport1_2_b1,
    recoutport1.mid_nest2.t2 => recoutport1_2_t2,
    
    recoutport2.m2 => recoutport2_m2,
    recoutport2.mid_nest1.b1 => recoutport2_1_b1,
    recoutport2.mid_nest1.t2 => recoutport2_1_t2,
    recoutport2.mid_nest2.b1 => recoutport2_2_b1,
    recoutport2.mid_nest2.t2 => recoutport2_2_t2 );
  
end a;
