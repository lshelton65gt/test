-- Test passing a record with array of slv to a function (input param only)
-- Parse error : type mis-match on line 51 ??
library ieee;
use ieee.std_logic_1164.all;

package p is
  type slv_arr is array(2 downto 1) of std_logic_vector(3 downto 0) ;
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;
  type rec2 is
    record
      r2 : slv_arr;
      r1 : std_logic;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_slv_arrdut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end rec_func_i_slv_arrdut;

architecture a of rec_func_i_slv_arrdut is
  signal sigrec : rec;

  function func2 ( inrec : rec2; b1: std_logic ) return std_logic_vector is
    variable outdata : std_logic_vector(7 downto 0);
  begin
    if b1 = inrec.r1 then
      outdata := inrec.r2(1) & inrec.r2(2);
    else
      outdata := inrec.r2(2) & inrec.r2(1);
    end if;
    return outdata;
  end func2;
begin

  p1: process (clk)
    variable result : std_logic_vector(7 downto 0);
    variable r2 : std_logic_vector(7 downto 0);
    variable va : slv_arr := ("0010","0100");  -- constant
  begin
    if clk'event and clk = '1' then
      result := func2 ((r1=>recinport.r1(3),
                        r2=>(recinport.r2, recinport.r1(7 downto 4))),
                        recinport.r1(5));
      r2 := func2 ((va, result(0)), recinport.r1(4));
      r2 := func2 (((result(7 downto 4), "1101"), r2(0)),recinport.r1(6));
      
      sigrec.r1 <= r2;
      sigrec.r2 <= result(4 downto 1);
    end if;
  end process p1;

  p3: process( sigrec )
  begin
    recoutport1 <= sigrec;
  end process;

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_slv_arr is
  port (
    clk            : in  std_logic;
    num            : in  integer;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0));
end rec_func_i_slv_arr;

architecture a of rec_func_i_slv_arr is
component rec_func_i_slv_arrdut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end component rec_func_i_slv_arrdut;

begin

  dut : rec_func_i_slv_arrdut port map (
    clk            => clk,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2);
end a;
