-- June 2007
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is
  subtype fixed4_type is SIGNED (3 downto 0);
  type array_t is array (0 to 1) of fixed4_type;
  type rec_arr is
    record
      mem1    : array_t;
    end record;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
package FFT_CORE_SIG_PKG is
  signal mem_data       : rec_arr;
end FFT_CORE_SIG_PKG;

