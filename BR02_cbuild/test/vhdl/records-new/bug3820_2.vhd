-- like bug3820.vhd but more complex, here we have a function that returns a record.

library ieee;
use ieee.std_logic_1164.all;

package bug3820_2_pak is

  type myrec is record
                  f1 : std_logic_vector(1 downto 0);
                  f2 : std_logic;
                end record;

  -- Conversion function for myrec_record
  function to_myrec(
    ftc_list : std_logic_vector(2 downto 0)
    ) return myrec;
end bug3820_2_pak;

package body bug3820_2_pak is
  
  function to_myrec(
    ftc_list : std_logic_vector(2 downto 0)
    ) return myrec is 
    variable command           : myrec;
    alias part1    : std_logic_vector(1 downto 0) is ftc_list(1 downto 0);
    alias part2    : std_logic                    is ftc_list(2);
  begin
    command.f1    := part1;
    command.f2    := part2;
    return command;
  end to_myrec;
end bug3820_2_pak;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library work;
use work.bug3820_2_pak.all;

entity bug3820_2 is
  port(in1, in2 : std_logic_vector(2 downto 0); out1 : out std_logic);
end;

architecture arch of bug3820_2 is

  signal sig1 : myrec; -- := ( others => '1');
  signal sig2 : myrec; --  := ( others => '0');
  signal temp : myrec; --  := ( others => '0');
begin
  sig1 <= to_myrec(in1);
  sig2.f1(0) <= in2(0);
  sig2.f1(1) <= in2(1);
  sig2.f2 <= in2(2);
  temp <= sig1 when in1(0)  = '1' else sig2;
  out1 <= temp.f1(0) and temp.f2;
end;
