-- August 2007
-- The ports in1 and out1 are type of record of array of records.
-- With the out1 <= in1 line commented out this test was failing in composite
-- resynthesis because a composite nets in1 and out1 appear only in port list,
-- and the resynthesis of the nets in1 and out1 was being missed by the
-- composite resynthesis walker.
-- No simulation is needed.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is
  subtype fixed4_type is SIGNED (15 downto 0);
  type complex_fixed_type is
    record
      real1    : fixed4_type;
      imag1    : fixed4_type;
    end record;

  type arr_rec is array (0 to 1) of complex_fixed_type;

  type t_rec_arr_rec is record
    arr_rec1 : arr_rec;
  end record;
  
end FIXED_POINT_PKG;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;

entity bug7529_01 is
  port (
    in1        : in  arr_rec;
    out1       : out arr_rec);
end bug7529_01;

architecture arch_top of bug7529_01 is
begin
--  out1 <= in1;  -- (Attention - keep it commented out)

end;
