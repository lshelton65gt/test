-- like rec_arr3.vhd but the array is not defined as part of a record

-- Test array of records with a collection of data types in the record.
-- This test was inspired by the broadcom testcase for bug4739(top: dot11aphy)

-- could not get aldec to simulate this so checked the results manually

library ieee;
use ieee.std_logic_1164.all;

package p is
  
  type tableArray is array(NATURAL range <>) of std_logic_vector(7 downto 0);

  type recType is record
     oneTableArray:              tableArray(7 downto 0); 
  end record;

end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

-- this is the outermost level 
entity rec_arr4 is
  port ( inport_r3 : in std_logic
         ; inport_i1 : in natural range 7 downto 0
         ; inport_slv8 : in std_logic_vector(7 downto 0)
         ; recoutport1_slv8: out std_logic_vector(7 downto 0)
         ; clk: in std_logic
         );
end rec_arr4;

architecture a of rec_arr4 is

begin

  p1: process (clk)
    variable tableA:          tableArray(7 downto 0);
    variable oneTableArray:     tableArray(7 downto 0);

  begin
    if clk'event and clk = '1' then

      tableA(inport_i1) := inport_slv8;  -- put a 8bit slv into one word of a tableArray

      oneTableArray := tableA;  -- put a tableArray into a tableArray

      recoutport1_slv8 <= tableA(inport_i1);

      tableA := oneTableArray;  -- put a tableArray from within a record into a tableArray

    end if;
  end process p1;
end a;



