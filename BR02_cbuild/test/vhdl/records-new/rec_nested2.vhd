-- Test for nested record 2.
library ieee;

use ieee.std_logic_1164.all;

package p is

  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;

  type rec_sl is
    record
      r2 : std_logic_vector(3 downto 0);
      r3 : std_logic;
    end record;

  type rec_int is
    record
      sl : rec_sl;
      i1 : integer;
      i2 : integer;
    end record;

  type rec_cb is
    record
      ch1,ch2 : character;
      b1 : bit;
      bv : bit_vector(3 downto 0);
    end record;

  type rec_all is
    record
      r1 : rec_int;
      r2 : rec_cb;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_nested2dut is
  port ( recinport : in rec
         ; intinport : in  integer
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end rec_nested2dut;

architecture a of rec_nested2dut is

  function func ( temp : rec_all ) return std_logic_vector is
    variable xslv : std_logic_vector(3 downto 0);
  begin
    if temp.r2.ch1 = temp.r2.ch2 then
      xslv(0) := '1';
    else
      xslv(0) := '0';
    end if;
    if temp.r1.i1 > temp.r1.i2 then
      xslv(1) := '1';
    else
      xslv(1) := '0';
    end if;
    if temp.r2.ch1 > temp.r2.ch2 then
      xslv(2) := '1';
    else
      xslv(2) := '0';
    end if;
    if temp.r1.i1 = temp.r1.i2 then
      xslv(3) := '1';
    else
      xslv(3) := '0';
    end if;
    return xslv;
  end func;

  procedure proc ( variable iorec : inout rec_all;
                   variable io1 : inout std_logic;
                   b1 : in std_logic ) is
    variable xi : integer;
    variable xch : character;
  begin
    io1 := b1 or io1;
    iorec.r1.sl.r2 := iorec.r1.sl.r3 & iorec.r1.sl.r2(2 downto 1) & io1;
    iorec.r1.sl.r3 := iorec.r1.sl.r2(2);
    xi := iorec.r1.i1;
    iorec.r1.i1 := iorec.r1.i2;
    iorec.r1.i2 := xi;
    xch := iorec.r2.ch2;
    iorec.r2.ch2 := iorec.r2.ch1;
    iorec.r2.ch1 := xch;
    if iorec.r2.b1 = '1' then
      iorec.r2.b1 := '0';
    else
      iorec.r2.b1 := '1';
    end if;
    iorec.r2.bv := iorec.r2.bv(2 downto 1) &
                   iorec.r2.bv(0) &
                   iorec.r2.bv(3);
  end proc;
  
  function func1 (xi : integer) return character is
    variable c : character ;
  begin
    case xi mod 26 is
      when  0 =>       c := 'g';
      when  1 =>       c := 'x';
      when  2 =>       c := 'y';
      when  3 =>       c := 'z';
      when  4 =>       c := 'a';
      when  5 =>       c := 'b';
      when  6 =>       c := 'c';
      when  7 =>       c := 'd';
      when  8 =>       c := 'e';
      when  9 =>       c := 'f';
      when  10 =>      c := 'Q';
      when  11 =>      c := 'H';
      when  12 =>      c := 'I';
      when  13 =>      c := 'J';
      when  14 =>      c := 'K';
      when  15 =>      c := 'L';
      when  16 =>      c := 'M';
      when  17 =>      c := 'N';
      when  18 =>      c := 'O';
      when  19 =>      c := 'P';
      when others =>   c:= 'A';
    end case;
    return c;
  end func1;

begin
  p1: process (clk, intinport)
    variable result, result2 : rec;
    variable xcb : rec_cb;
    variable sl,sl2  : std_logic;
    variable temp, temp2 : rec_all;
    variable xslv : std_logic_vector(3 downto 0);
    variable c1,c2 : character;
  begin
    if clk'event and clk = '1' then
      c1 := func1(intinport);
      c2 := func1(intinport+1200);
      xcb := (c1, c2, to_bit(recinport.r1(1), '0'),
              to_bitvector(recinport.r1(7 downto 4), '0'));
      temp := (r2=>xcb, r1=>(i2=>intinport+1, sl=>(recinport.r2, recinport.r1(0)),
                             others=>intinport));
      sl := recinport.r1(2);
      proc(temp, sl, recinport.r1(3));
      xslv := func(temp);
      result.r1 := temp.r1.sl.r2 & xslv;
      result.r2 := std_logic_vector(to_stdulogicvector(temp.r2.bv));
      recoutport1 <= result;
      temp.r2.ch1 := func1(temp.r1.i1);
      temp.r2.ch2 := func1(temp.r1.i2);
      temp2 := temp;
      proc(temp2, sl, temp.r1.sl.r3);
      xslv := func(temp2);
      result2.r1 := temp2.r1.sl.r2 & xslv;
      result2.r2 := std_logic_vector(to_stdulogicvector(temp2.r2.bv));
      recoutport2 <= result2;
    end if;
    
  end process p1;
end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_nested2 is
  port (
    clk            : in  std_logic;
    num            : in  integer;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0);
    recoutport2_r1 : out std_logic_vector(7 downto 0);
    recoutport2_r2 : out std_logic_vector(3 downto 0));
end rec_nested2;

architecture a of rec_nested2 is
component rec_nested2dut is
  port ( recinport : in rec
         ; intinport : in integer
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end component rec_nested2dut;

begin

  dut : rec_nested2dut port map (
    clk            => clk,
    intinport      => num,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2,
    recoutport2.r1 => recoutport2_r1,
    recoutport2.r2 => recoutport2_r2);
  
end a;
