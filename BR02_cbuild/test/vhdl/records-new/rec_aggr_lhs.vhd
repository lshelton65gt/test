-- Test LHS record aggregate with a collection of types. 
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec_all is
    record
      r1 : std_logic_vector(64 downto 0);
      r2 : std_logic_vector(3 downto 0);
      r3 : std_logic;
      i1,i2 : integer;
      ch1,ch2 : character;
      b1 : bit;
      bv : bit_vector(7 downto 0) ;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_aggr_lhsdut is
  port ( recinport : in rec_all
         ; recoutport1: out rec_all
         ; recoutport2: out rec_all
         ; clk: in std_logic
         );
end rec_aggr_lhsdut;

architecture a of rec_aggr_lhsdut is

  begin

  p1: process (clk)
    variable vr1 : std_logic_vector(64 downto 0);
    variable vr2 : std_logic_vector(3 downto 0);
    variable vr3 : std_logic;
    variable vi1,vi2 : integer;
    variable vch1,vch2 : character;
    variable vb1 : bit;
    variable vbv : bit_vector(7 downto 0) ;
    variable temp, temp2: rec_all;
  begin
    if clk'event and clk = '1' then
      -- lhs aggregate position association
      (vr1,vr2,vr3,vi1,vi2,vch1,vch2,vb1,vbv) := recinport;
      vr1(0) := vr2(3);
      vr2 := vr1(7 downto 4);
      vch1 := vch2;
      vbv(7) := vb1;
      vb1 := vbv(3);
      temp := (vr1, vr2, vr3, i1=>vi2+10, i2=>vi2-2,
               ch1=>vch1, ch2=>vch2, b1=>vb1, bv=>vbv);
      -- lhs aggregate name association
      (temp2.r1, temp2.r2, vr3, vi1, vi2, ch2=>temp2.ch1,
       ch1=>temp2.ch2, b1=>temp2.b1, bv=>temp2.bv) := temp;
      temp2.i1 := vi2 + (vi2 - vi1);
      temp2.i2 := vi2;
      temp2.r3 := temp.r3 and recinport.r3 and recinport.r1(0);
      recoutport1<=temp;
      recoutport2<=temp2; 
    end if;
  end process p1;
end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_aggr_lhs is
  port (
    clk            : in  std_logic;
    recinport_r1   : in  std_logic_vector(64 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recinport_r3   : in  std_logic;
    recinport_i1   : in  integer;
    recinport_i2   : in  integer;
    recinport_ch1  : in  character;
    recinport_ch2  : in  character;
    recinport_b1   : in  bit;
    recinport_bv   : in  bit_vector(7 downto 0);
    recoutport1_r1   : out  std_logic_vector(64 downto 0);
    recoutport1_r2   : out  std_logic_vector(3 downto 0);
    recoutport1_r3   : out  std_logic;
    recoutport1_i1   : out  integer;
    recoutport1_i2   : out  integer;
    recoutport1_ch1  : out  character;
    recoutport1_ch2  : out  character;
    recoutport1_b1   : out  bit;
    recoutport1_bv   : out  bit_vector(7 downto 0);
    recoutport2_r1   : out  std_logic_vector(64 downto 0);
    recoutport2_r2   : out  std_logic_vector(3 downto 0);
    recoutport2_r3   : out  std_logic;
    recoutport2_i1   : out  integer;
    recoutport2_i2   : out  integer;
    recoutport2_ch1  : out  character;
    recoutport2_ch2  : out  character;
    recoutport2_b1   : out  bit;
    recoutport2_bv   : out  bit_vector(7 downto 0));
end rec_aggr_lhs;

architecture a of rec_aggr_lhs is
  component rec_aggr_lhsdut is
                          port ( recinport : in rec_all
                                 ; recoutport1: out rec_all
                                 ; recoutport2: out rec_all
                                 ; clk: in std_logic
                                 );
  end component rec_aggr_lhsdut;

begin

  dut : rec_aggr_lhsdut port map (
    clk              => clk,
    recinport.r1     => recinport_r1,
    recinport.r2     => recinport_r2,
    recinport.r3     => recinport_r3,
    recinport.i1     => recinport_i1,
    recinport.i2     => recinport_i2,
    recinport.ch1    => recinport_ch1,
    recinport.ch2    => recinport_ch2,
    recinport.b1     => recinport_b1,
    recinport.bv     => recinport_bv,
    recoutport1.r1   => recoutport1_r1,
    recoutport1.r2   => recoutport1_r2,
    recoutport1.r3   => recoutport1_r3,
    recoutport1.i1   => recoutport1_i1,
    recoutport1.i2   => recoutport1_i2,
    recoutport1.ch1  => recoutport1_ch1,
    recoutport1.ch2  => recoutport1_ch2,
    recoutport1.b1   => recoutport1_b1,
    recoutport1.bv   => recoutport1_bv,
    recoutport2.r1   => recoutport2_r1,
    recoutport2.r2   => recoutport2_r2,
    recoutport2.r3   => recoutport2_r3,
    recoutport2.i1   => recoutport2_i1,
    recoutport2.i2   => recoutport2_i2,
    recoutport2.ch1  => recoutport2_ch1,
    recoutport2.ch2  => recoutport2_ch2,
    recoutport2.b1   => recoutport2_b1,
    recoutport2.bv   => recoutport2_bv);
end a;
