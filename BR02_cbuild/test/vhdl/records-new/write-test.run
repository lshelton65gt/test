#!/bin/bash
  
# NOTE- A test needs to be put into the PASS list when it is removed from the 
#       FAIL list.
#
#   This is the script that writes a new 'test.run' command
#    file for this testsuite
#
#   This is run by the test harness *every* time this testsuite is
#   run.  It is invoked like this:
#		./write-test.run  >  test.run
#
#

# This testsuite has a limited lifetime.  It is only here during the
# development of the NUComposite* classes and the population of records
# into them.  This suite consists of copies of a subset of the tests in
# test/vhdl/records, and is run with the -tempNewRecord switch thrown.
# Once all the current tests are working, the switch will be deleted,
# the new population method will be the default, and this testsuite will
# be removed.

# Tests with arrayed records:
# aggregate_init aggregate_init1 aggregate_init2 aggregate_init3 aggregate_init4 
# aggregate_init5 aggregate_init6  bug3331clks  bug3331clks_to  bug4456  bug4456a
# bug4792  bug4792_3  nestedrecord6  nestedrecord6a  nestedrecord6b  nestedrecord7
# rec_arr  rec_arr2  record10  record11 record12  record7 record8 record9 
# record_others vec_of_record_1 

# bug6374 : The vhdl is not acceptable to MTI/Aldec, cbuild issues value out of range error correctly. It needs fixing.
# rec_memsel : A test for variable ranged slice of record, where record fields are vectors, leading to memory variable ranged selects after resynthesis. Fails in codegen since cbuild fails to synthesize away downto during post population resynthesis. We display "unsupported message" right now. Bug 6937
# rec_varsel : A test for variable ranged slice of record, where record fields are bit fields, leading to vector variable ranged selects after resynthesis. Works, but we display "unsupported message" right now.
# rec_varsel_agg : A test for aggregate that assigns to variable ranged record slice, where record has bit fields. Fails in population. Bug 6938
# rec_memsel_agg : A test for aggregate that assigns to variable ranged record slice, where record has bit array fields. Fails in population. Bug 6938
XTEXIT=" \
bug6374 \
rec_varsel \
rec_memsel \
rec_varsel_agg \
rec_memsel_agg \
"

XTEXITBACKEND=" \
"

# bug4212 : Has inout port which confuses UD and results in simulation mismatch according to test comments.
XSIMTDIFF=" \
bug4212 \
"

# mixed1  : Has real simulation mismatches.
# rec_func_* : All have arrays on ports. Test driver cannot drive them.
# bug7529 has no meaningful simulation because output is not driven. At one time this test failed with internal error.
XPASSNOSIM=" \
mixed1 \
rec_func_o_arr \
rec_proc_io_arr \
rec_proc_o_arr \
bug7529_01 \
"

# bug4792_3 : Expected to fail since function does not return anything, while there's a statement that
#             invokes it expects it to return something.
# concat10  : Concat of aggregates that haven't been typecasted. This is expected to fail.
XPEXIT=" \
bug4792_3 \
concat10 \
"

XPASS=" \
aggregate_init \
aggregate_init1 \
aggregate_init10 \
aggregate_init11 \
aggregate_init12 \
aggregate_init13 \
aggregate_init14 \
aggregate_init15 \
aggregate_init16 \
aggregate_init17 \
aggregate_init18 \
aggregate_init19 \
aggregate_init2 \
aggregate_init20 \
aggregate_init3 \
aggregate_init4 \
aggregate_init5 \
aggregate_init6 \
aggregate_init7 \
aggregate_init8 \
aggregate_init9 \
aggregate_inita \
bug3331clks \
bug3331clks_to \
bug3820 \
bug3820_2 \
bug3820_3 \
bug3820_4 \
bug3940_1 \
bug4456 \
bug4456a \
bug4691 \
bug4792 \
bug4891 \
bug4891small \
bug5343 \
bug6135 \
bug6374_1 \
bug6374_2 \
bug7346_01 \
bug7346_02 \
bug7346_03 \
bug7346_04 \
bug7346_05 \
bug7346_06 \
bug7346_07 \
bug7346_08 \
bug7346_09 \
bug7346_10 \
bug7346_11 \
bug7346_12 \
bug7380_01 \
bug7380_02 \
bug7380_03 \
bug7380_04 \
bug7493_01 \
bug7493_02 \
bug7555_01 \
bug7555_02 \
bug7555_03 \
bug7555_04 \
bug7555_05 \
bug7555_06 \
bug7555_07 \
bug7555_08 \
bug7669 \
bug7672_01 \
bug7672_02 \
bug7676_01 \
bug7771 \
bug7803 \
bug7878 \
bug7878_1 \
bug7878_2 \
bug8155_01 \
bug8867_01 \
bug12169_01 \
bug12542 \
bug12542_1 \
bug12542_2 \
bug12542_3 \
bug12542_4 \
bug12744_01 \
bug12746_01 \
bug12902_01 \
bug12902_02 \
concat1 \
concat2 \
concat3 \
concat4 \
concat5 \
concat6 \
concat7 \
concat8 \
concat9 \
inout1 \
multidim1 \
multidim2 \
multidim3 \
multidim4 \
multidim5 \
multidim6 \
multidim7 \
nestedrecord1 \
nestedrecord10 \
nestedrecord11 \
nestedrecord12 \
nestedrecord13 \
nestedrecord3 \
nestedrecord4 \
nestedrecord4a \
nestedrecord5 \
nestedrecord6 \
nestedrecord6a \
nestedrecord6b \
nestedrecord7 \
nestedrecord8 \
nestedrecord9 \
overloadedoperator \
rec_agg_port_actual \
rec_aggr_lhs \
rec_arr \
rec_arr2 \
rec_arr3 \
rec_arr4 \
rec_arr5 \
rec_field_uses \
rec_func_i_bit \
rec_func_i_bv \
rec_func_i_ch \
rec_func_i_ch_arr \
rec_func_i_int \
rec_func_i_int_arr \
rec_func_i_sl \
rec_func_i_slv \
rec_func_i_slv65 \
rec_func_i_slv_arr \
rec_func_o \
rec_nested \
rec_nested2 \
rec_nested2a \
rec_nested3 \
rec_proc_io \
rec_proc_o \
rec_slice_port_actual \
record1 \
record10 \
record11 \
record12 \
record13 \
record14 \
record15 \
record2 \
record3 \
record4 \
record4a \
record5 \
record5a \
record6 \
record7 \
record7a \
record8 \
record9 \
record_compare \
record_lslice \
record_others \
record_rslice \
recordclock \
recordclock2 \
ret_func3 \
selected_signal_assign1 \
testArRec \
trailrstrecord1 \
trailrstrecord2 \
vec_of_record_1 \
"

                ###
                ###   Optionally define any extra command line options for the
                ###   cbuild command, if undefined then none will be used.
                ###

TRAILINGCBUILDOPTS="-userTypePopulate"

                ###
                ###   Optionally define the cbuild command line to be used for
                ###   all builds, if undefined then "cbuild" will be used
                ###

CBUILD="cbuild -q"

                ###
                ###   Extra command arguments passed to the nCompare script
                ###

DEFAULTNCOMPAREOPTS="-strict"

                ### also check that libdesign.hierarchy files are correct when
		### a <test>.hierarchy.gold file exists
CHECKEXTRACBUILDEXTENSIONS="hierarchy"

# set TESTOUTPUT on windows so cbuilds don't use the same exec and lib names
if [ "$WINX" == 1 ]; then
  TESTOUTPUT=1
fi

############# below this line you should not need to modify this file

# pick up common routines, and write tests for standard variable lists
source $CARBON_HOME/test/write-test.common
f_writeTestsForStandardLists "$@ $TRAILINGCBUILDOPTS"

