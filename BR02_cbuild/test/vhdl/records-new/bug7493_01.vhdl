-- July 2007
-- A temp1 is of array of nested record type.
-- It's defined as a variable in the process.
-- It's used in LHS and RHS assignment.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is

  type complex_fixed_type is
    record
      real1    : integer;
      imag1    : integer;
    end record;

  type entry_type is
    record
      entry   : complex_fixed_type;
    end record;

  type arr_nested_rec is array (0 to 1) of entry_type;
  
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;


library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity bug7493_01 is
  port (
    in1        : in integer;
    in2        : in integer;
    out1       : out integer;
    out2       : out integer);
end bug7493_01;

architecture arch of bug7493_01 is
begin
  p1: process (in1, in2 )
    variable temp1 : arr_nested_rec;   
  begin  -- process p1
    temp1(0).entry.real1 := in1;        -- this line is tested
    temp1(0).entry.imag1 := in2;        -- this line is tested
    out1 <= temp1(0).entry.real1;       -- this line is tested
    out2 <= temp1(0).entry.imag1;       -- this line is tested 
     
  end process p1;

end;
