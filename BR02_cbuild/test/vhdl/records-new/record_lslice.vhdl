library ieee;
use ieee.std_logic_1164.all;

entity record_lslice is
  
  port (
    clk : in std_logic;
    inIdx : in integer range 0 to 7;
    outIdx : in integer range 0 to 7;
    f1 : in std_logic_vector(6 downto 0);
    f2 : in std_logic_vector(6 downto 0);
    out1 : out std_logic_vector(6 downto 0));
end record_lslice;

architecture simple of record_lslice is

    type rec is record
                     f1 : std_logic_vector(6 downto 0);
                     f2 : std_logic_vector(6 downto 0);
                   end record;

    type recvec is array (natural range <>) of rec;
    shared variable var1 : recvec(0 to 7) := (others => (others => (others => '0')));
begin  -- simple

  process (clk, f1, f2)
  variable var2 : recvec(0 to 15) := (others => (others => (others => '0')));
  begin  -- process
    var1(inIdx) := (f1, f2);
    var2(2 to 9) := var1;
    out1 <= var2(outIdx).f1 and var2(outIdx).f2;
  end process;
  
end simple;
