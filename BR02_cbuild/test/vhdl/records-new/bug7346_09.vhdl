-- July 2007
-- The signal mem_data is of nested record type.
-- It is  defined in the package declaration. The entity (not top one) uses the
-- signal in the right and left side whole record assignment.
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is
  subtype fixed4_type is SIGNED (3 downto 0);
  type complex_fixed_type is
    record
      real1    : fixed4_type;
      imag1    : fixed4_type;
    end record;

  type rec_rec is
    record
      a1 : complex_fixed_type;
      b1 : complex_fixed_type;
    end record;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
package FFT_CORE_SIG_PKG is
  signal mem_data      : rec_rec;
  signal mem_data2     : rec_rec;
end FFT_CORE_SIG_PKG;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity sub is
  port (
    out1      : out rec_rec);
end sub;

architecture arch of sub is
begin
    mem_data2 <= mem_data;      -- this is tested
    out1  <= mem_data2;         -- this is tested
end;




library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity bug7346_09 is
  port (
    in11       : in fixed4_type;
    in12       : in fixed4_type;
    in21       : in fixed4_type;
    in22       : in fixed4_type;
    out11      : out fixed4_type;
    out12      : out fixed4_type;
    out21      : out fixed4_type;
    out22      : out fixed4_type);
end bug7346_09;

architecture arch_top of bug7346_09 is
  signal mem_data_top       : rec_rec;
begin

  mem_data.a1.real1 <= in11;
  mem_data.a1.imag1 <= in12;
  mem_data.b1.real1 <= in21;
  mem_data.b1.imag1 <= in22;
  i_sub : entity sub 
     port map (
         out1   => mem_data_top);

  out11 <= mem_data_top.a1.real1;
  out12 <= mem_data_top.a1.imag1; 
  out21 <= mem_data_top.b1.real1;
  out22 <= mem_data_top.b1.imag1; 
end;
