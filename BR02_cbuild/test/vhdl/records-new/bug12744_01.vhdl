-- June 2009
-- Created based on the customer test case.
-- The issue was with const instr_c, which is of type of array of records,
-- and is created in a package body. CBUILD used to assert on it.
package bug_pkg is

  function getInstr (in1 : bit_vector(3 downto 0)) return bit_vector;
    
  
end bug_pkg;

package body bug_pkg is
  
  type instr_t is record
    elm1 : bit_vector(3 downto 0);
    elm2 : bit_vector(3 downto 0);
    mask : bit_vector(3 downto 0);
  end record;
  
  type instr_array_t is array (natural range <>) of instr_t;
  constant instr_c : instr_array_t :=
		(
                  ("0000", "0000", (others => '1')),
                  ("0001", "0001", (others => '1')),
                  ("0010", "0010", (others => '1')),
                  ("0011", "0011", (others => '1')),
                  ("0100", "0100", (others => '1')),
                  ("0101", "0101", (others => '1')),
                  ("0110", "0110", (others => '1')),
                  ("0111", "0111", (others => '1')),
                  ("1000", "1000", (others => '1')),
                  ("1001", "1001", (others => '1')),
                  ("1010", "1010", (others => '1')),
                  ("1011", "1011", (others => '1')),
                  ("1100", "1100", (others => '1')),
                  ("1101", "1101", (others => '1')),
                  ("1110", "1110", (others => '1')),
                  ("1111", "1111", (others => '1'))
                );

  function getInstr (in1 : bit_vector(3 downto 0)) return bit_vector is
    variable temp : bit_vector(3 downto 0) := "0000";
  begin
    for i in instr_c'RANGE loop
      if (instr_c(i).mask and in1) = instr_c(i).elm1 then
        temp := instr_c(i).elm2;
        exit;
      end if;
    end loop;

    return temp;
    
  end;
end bug_pkg;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library work;
use work.bug_pkg.all;

entity sub is
  port (
    clk  : in  bit;
    rst  : in  bit;
    in1  : in  bit_vector(3 downto 0);
    out1 : out bit_vector(3 downto 0));
end sub;

architecture arch_sub of sub is
begin 

  p1: process (clk, rst)
  begin 
    if rst = '0' then 
       out1 <= "0000";
    elsif clk'event and clk = '1' then  -- rising clock edge
       out1 <= getInstr(in1);
    end if;
  end process p1;

end arch_sub;


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library work;
use work.bug_pkg.all;


entity bug12744_01 is
  
  port (
    clk  : in  bit;
    rst  : in  bit;
    in1  : in  bit_vector(3 downto 0);
    out1 : out bit_vector(3 downto 0));

end bug12744_01;


architecture arch1 of bug12744_01 is
  component sub
    port (
      clk  : in  bit;
      rst  : in  bit;
      in1 :  in  bit_vector(3 downto 0);
      out1 : out bit_vector(3 downto 0));
  end component;
begin  -- arch1

  s1 : sub port map (
    clk  => clk,
    rst  => rst,
    in1  => in1,
    out1 => out1);
end arch1;
