-- July 2007
-- The signal mem_data is of record type that has an array as it's field.It is  defined in the
-- package declaration. The entity (not top one) uses the signal in the right
-- and left side assignment. 
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is
  subtype fixed4_type is SIGNED (3 downto 0);
  type array_t is array (0 to 1) of fixed4_type;
  type rec_arr is
    record
      mem1    : array_t;
    end record;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
package FFT_CORE_SIG_PKG is
  signal mem_data      : rec_arr;
  signal mem_data2     : rec_arr;
end FFT_CORE_SIG_PKG;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity sub is
  port (
    out1      : out rec_arr);
end sub;

architecture arch of sub is
begin
  mem_data2 <= mem_data;        -- this is tested
  out1  <= mem_data2;           -- this is tested
end;





library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity bug7346_10 is
  port (
    in11       : in fixed4_type;
    in12       : in fixed4_type;
    out11      : out fixed4_type;
    out12      : out fixed4_type);
end bug7346_10;

architecture arch_top of bug7346_10 is
  signal mem_data_top       : rec_arr;
begin

  mem_data.mem1(0) <= in11;
  mem_data.mem1(1) <= in12;
  i_sub : entity sub 
     port map (
         out1   => mem_data_top);

  out11 <= mem_data_top.mem1(0);
  out12 <= mem_data_top.mem1(1); 
end;
