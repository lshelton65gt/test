-- Test for array of array of record which contains a record field
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity aggregate_init9 is
  port (sel_i : in std_logic_vector(0 downto 0);
        sel_j : in std_logic_vector(0 downto 0);
        fsel : in std_logic_vector(1 downto 0);
        out1: out std_logic_vector(5 downto 0));
end aggregate_init9;

architecture rtl of aggregate_init9 is
  constant const1 : integer := 64;   
  constant const2 : integer := 2; 

  subtype fint is integer range 0 to const1-1;

  type dtype is record
                  f11 : fint;
                  f12 : fint;
                end record;

  type ctype is record
                  f1 : fint;
                  f2 : fint;
                  f3 : dtype;
                end record;

  type sconfig is array (Natural Range <> ) of ctype;

  type tconfig is array (Natural Range <>) of sconfig(const2-1 downto 0);
  
  constant c1 : ctype := (2, 6, (10, 14));
  constant c2 : sconfig(const2-1 downto 0) := (1 => (1, 5, (9, 13)), others => c1);
  constant c3 : tconfig(const2-1 downto 0) := (1 => c2, 0 => ((3, 7, (11, 15)), (4, 8, (12, 16))));
  
  signal sig1 : tconfig(const2-1 downto 0);
begin
  sig1 <= c3;

  process (sel_i, sel_j, fsel, sig1)
    variable lfsel : integer;
    variable lsel_i : integer;
    variable lsel_j : integer;
    variable lout : fint;
  begin
    lfsel := to_integer(unsigned(fsel));
    lsel_i := to_integer(unsigned(sel_i));
    lsel_j := to_integer(unsigned(sel_j));
    case lfsel is
      when 0 => lout := sig1(lsel_i)(lsel_j).f1;
      when 1 => lout := sig1(lsel_i)(lsel_j).f2;
      when 2 => lout := sig1(lsel_i)(lsel_j).f3.f11;
      when 3 => lout := sig1(lsel_i)(lsel_j).f3.f12;
      when others => lout := 0;
    end case;
    out1 <= std_logic_vector(to_unsigned(lout, out1'length));
  end process;
end rtl;
