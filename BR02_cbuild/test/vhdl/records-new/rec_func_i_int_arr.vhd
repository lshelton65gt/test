-- Test passing a record to a function (input param only)
-- test a memory (int_arr) in record.  Untested.
library ieee;
use ieee.std_logic_1164.all;

package p is
  type int_arr is array(2 downto 1) of integer ;
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;

  type rec2 is
    record
      r1 : int_arr;
      r2 : std_logic_vector(11 downto 0);
    end record;

end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_int_arrdut is
  port ( recinport : in rec
         ; intinport : in  integer
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end rec_func_i_int_arrdut;

architecture a of rec_func_i_int_arrdut is
  signal sigrec : rec;

  function func2 ( inrec : rec2; b1 : std_logic ) return std_logic_vector is
    variable outdata : std_logic_vector(11 downto 0);
  begin
    if inrec.r1(1) = inrec.r1(2) and b1 = '1'  then
      outdata := inrec.r2 and "010101010101";
    else
      outdata := inrec.r2 and "101010101010";
    end if;
    return outdata;
  end func2;
begin

  p1: process (clk, intinport)
    variable result : std_logic_vector(11 downto 0);
  begin
    if clk'event and clk = '1' then
      result := func2 ((r2=>recinport.r2&recinport.r2&recinport.r2,
                        r1=>(intinport, intinport+1)),
                       recinport.r1(7) );
      result := func2 (((intinport, 100),result), result(11) );
      result := func2 (((10, intinport),r2=>result), result(11) );
      sigrec.r1 <= result(11 downto 4);
      sigrec.r2 <= result(3 downto 0);
    end if;
  end process p1;

  p3: process( sigrec )
  begin
    recoutport1 <= sigrec;
    recoutport2.r1 <= sigrec.r1;
    recoutport2.r2 <= sigrec.r2;
  end process;

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_int_arr is
  port (
    clk            : in  std_logic;
    num            : in  integer;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0);
    recoutport2_r1 : out std_logic_vector(7 downto 0);
    recoutport2_r2 : out std_logic_vector(3 downto 0)); end rec_func_i_int_arr;

architecture a of rec_func_i_int_arr is
component rec_func_i_int_arrdut is
  port ( recinport : in rec
         ; intinport : in integer
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end component rec_func_i_int_arrdut;

begin

  dut : rec_func_i_int_arrdut port map (
    clk            => clk,
    intinport      => num,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2,
    recoutport2.r1 => recoutport2_r1,
    recoutport2.r2 => recoutport2_r2);
  
end a;
