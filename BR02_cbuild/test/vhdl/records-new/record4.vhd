-- Test whole record assignment, both concurrent and sequential.
library ieee;
use ieee.std_logic_1164.all;

entity record4 is
  port (
    clk: in std_logic
    ;reset: in std_logic
    ;in1  : in  std_logic_vector(7 downto 0)
    ;in2  : in  std_logic_vector(7 downto 0)
    ;outp : out std_logic_vector(7 downto 0)
    );
end record4;

architecture a of record4 is

type rec is record
    r1 : std_logic_vector(7 downto 0);
    r2 : std_logic_vector(3 downto 0);
end record;

signal s1, s2: rec;

begin

  s2 <= s1;
  
  p1: process (clk, reset)
  begin  -- process p1
    if reset = '0' then                   -- asynchronous reset (active low)
      s1.r1 <= (others => '0');
      s1.r2 <= (others => '1');
      s2.r1 <= (others => '1');
      s2.r2 <= X"9";
    elsif clk'event and clk = '1' then    -- rising clock edge
      s1.r1 <= in2(6 downto 4) & in1(3 downto 0) & in1(7);
      s1.r2 <= in2(4 downto 2) & s1.r1(5);
    end if;
  end process p1;

  p2: process (s1)
  begin
    if s1.r1(3) = '1' then
      s2 <= s1;
    end if;
  end process p2;

  outp <= s2.r1(7) & s2.r2 & s2.r1(2 downto 0);

end a;
