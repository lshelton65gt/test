-- Test basic population and manipulation of record fields
-- others, positional, and named associations in aggregates
library ieee;
use ieee.std_logic_1164.all;

entity record3 is
  port (
    clk: in std_logic
    ;reset: in std_logic
    ;in1  : in  std_logic_vector(7 downto 0)
    ;in2  : in  std_logic_vector(7 downto 0)
    ;outp : out std_logic_vector(7 downto 0)
    );
end record3;

architecture a of record3 is

type rec is record
    r1 : std_logic_vector(7 downto 0);
    r2 : std_logic_vector(3 downto 0);
end record;

signal s1, s2: rec;

begin

p: process (clk, reset)
begin  -- process p
  if reset = '0' then                   -- asynchronous reset (active low)
    s1.r1 <= "00000000";
    s1.r2 <= (2 => '1', others => '0');
    s2.r1 <= ('1', others => '0');
    s2.r2 <= (others => '1');
  elsif clk'event and clk = '1' then    -- rising clock edge
    if s1.r1(0) = '1' then
      s1.r1 <= s1.r2(2 downto 0) & in2(5) & s1.r1(2 downto 0) & s1.r2(3);
      s2.r1 <= in1;
    else
      s1.r2 <= in2(2 downto 0) & s1.r1(3);
      s2.r2 <= s1.r2;
    end if;
  end if;
end process p;
  
  outp <= ( s1.r2(3), s1.r2(2), s1.r2(1), s1.r2(0),
            s2.r2(3), s2.r2(1), s2.r2(2), s2.r2(0) );
end a;
