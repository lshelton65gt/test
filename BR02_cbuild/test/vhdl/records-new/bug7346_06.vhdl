-- June 2007
-- The signal mem_data is of nested record type. It is  defined in the
-- package declaration. The sub entity gets instantiated and the signal 
-- mem_data is used as the port of the entity.
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library ieee;
use ieee.std_logic_1164.all;
library lib1;
use lib1.FIXED_POINT_PKG.all;
use lib1.FFT_CORE_SIG_PKG.all;
entity bug7346_06 is
  port (
    in11       : in  complex_fixed_type;
    in12       : in  complex_fixed_type;
    out11      : out complex_fixed_type;
    out12      : out complex_fixed_type);
end bug7346_06;

architecture arch_top of bug7346_06 is
  signal mem_data1       : rec_rec;
begin

  mem_data.a1 <= in11;
  mem_data.b1 <= in12;

  i_sub : entity sub 
  port map (
         in1  => mem_data,
         out1 => mem_data1  );

  out11 <= mem_data1.a1;
  out12 <= mem_data1.b1;
  
end;
