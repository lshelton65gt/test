-- Test non-port unconstrained type arrayed records whole-record assignment

package p is

type tim_logic_vector is array (integer range <>) of bit;
  
type rec is record
    r1 : tim_logic_vector(7 downto 0);
    r2 : tim_logic_vector(3 downto 0);
end record;

type bundlerec is array(natural range<>) of rec;

end p;

use work.p.all;

entity record8 is
  port ( recinport : in rec
       ; recoutport: out rec
       ; clk: in bit
       );
end record8;

architecture a of record8 is
  signal arrayrec : bundlerec(0 to 1);
begin

  p1: process (clk)
  begin  -- process p1
    if clk'event and clk = '1' then  -- rising clock edge
      arrayrec(0) <= recinport;
    end if;
  end process p1;
  
  p2: process (clk)
  begin  -- process p1
    if clk'event and clk = '1' then  -- rising clock edge
      arrayrec(1) <= arrayrec(0);
    end if;
  end process p2;
  
  p3: process (clk)
  begin  -- process p1
    if clk'event and clk = '1' then  -- rising clock edge
      recoutport <= arrayrec(1);
    end if;
  end process p3;
  
end a;
