-- Test for part-select of a 1 dimensional record array.
-- This gives us varsels on field bit nets that become
-- vector nets after composite resynthesis. This gives us
-- varsels on vector nets.
library ieee;
use ieee.std_logic_1164.all;

entity bug7878 is

  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    ivec : in  std_logic_vector(4 downto 0);
    ovec : out std_logic_vector(4 downto 0));
  
end bug7878;

architecture arch of bug7878 is

  type rec is record
                f1             : std_ulogic;
              end record;

  type recArray is array (natural range <>) of rec;

  signal sig1to5              :  recArray(1 to 5) := (others => (f1 => '0'));
  signal sig1to8           : recArray(1 to 8) := (others => (f1 => '1'));
  signal sig5dt1           : recArray(5 downto 1) := (others => (f1 => '1'));
  signal sig1to9           : recArray(1 to 9) := (others => (f1 => '0'));
  
begin  -- arch

  proc1: process (clk, rst)
  begin  -- process proc1
    if rst = '0' then                   -- asynchronous reset (active low)
      ovec <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- Constant index varsel on lvalue
      sig1to5(1) <= (f1 => ivec(0));
      sig1to5(2) <= (f1 => ivec(1));
      -- Variable index varsel on lvalue
      for j in 3 to 5 loop
        sig1to5(j) <= (f1 => ivec(j-1));
      end loop;  -- j

      -- Constant range part-select on lvalue and rvalue
      sig1to8(1 to 5) <= sig1to5;
      sig5dt1 <= sig1to8(1 to 5);

      -- Variable range part-select on lvalue and rvalue
      for i in 1 to 2 loop
         -- 2:3 <= 2:1, 4:5 <= 4:3
        sig1to9(i*2 to i*2+1) <= sig5dt1((i-1)*2+2 downto (i-1)*2+1);
      end loop;  -- i

      -- Constant index varsel on rvalue
      sig1to9(1) <= sig5dt1(5);

      ovec(0) <= sig1to9(5).f1;
      ovec(1) <= sig1to9(4).f1;
      ovec(2) <= sig1to9(3).f1;
      ovec(3) <= sig1to9(2).f1;
      ovec(4) <= sig1to9(1).f1;
    end if;
  end process proc1;

end arch;
