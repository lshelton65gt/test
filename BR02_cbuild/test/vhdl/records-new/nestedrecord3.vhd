-- Test basic population and manipulation of nested record fields:
-- others, positional, and named associations in aggregates, and also
-- test concats of record elements
entity nestedrecord3 is
  port (
    clk: in bit;
    reset: in bit;
    inp : in bit_vector(15 downto 0);
    outp : out bit_vector(15 downto 0));
end nestedrecord3;

architecture a of nestedrecord3 is
  type bottomrec is record
                      b1     : bit;
                      t2     : bit_vector(3 downto 0);  -- reuse field name
                    end record;

  type midrec is record
                   mid_nest1 : bottomrec;
                   m2        : bit;
                   mid_nest2 : bottomrec;  -- multiple instance of the same
                                           -- record type
                 end record;

  type toprec is record
                   t1        : bit_vector(1 to 2);
                   top_nest1 : midrec;
                   t4        : bit_vector(2 to 4);
                 end record;

  signal nrc : toprec;
  
begin

  p1: process (clk, reset )
  begin
    if reset = '0' then
      nrc.t1 <= "00";
      nrc.t4 <= (others => '1');
      nrc.top_nest1.m2 <= '0';
      nrc.top_nest1.mid_nest1.b1 <= '0';
      nrc.top_nest1.mid_nest2.b1 <= '0';
      nrc.top_nest1.mid_nest1.t2 <= (2 => '1', others => '0');
      nrc.top_nest1.mid_nest2.t2 <= (3 => '0', 0 => '1', 2 => '0', others => '1');
    elsif clk'event and clk = '1' then
      nrc.t1 <= inp(15 downto 14);
      nrc.t4 <= inp(13 downto 11);
      nrc.top_nest1.m2 <= inp(10);
      nrc.top_nest1.mid_nest1.b1 <= inp(9);
      nrc.top_nest1.mid_nest2.b1 <= inp(8);
      nrc.top_nest1.mid_nest1.t2 <= inp(6 downto 3);
      nrc.top_nest1.mid_nest2.t2 <= inp(2 downto 0) & inp(7);
    end if;
  end process p1;

  outp <= nrc.t4 &
          nrc.top_nest1.m2 &
          nrc.top_nest1.mid_nest1.b1 &
          nrc.top_nest1.mid_nest2.t2(2) &
          nrc.top_nest1.mid_nest2.b1 &
          nrc.top_nest1.mid_nest2.t2(1 downto 0) &
          nrc.top_nest1.mid_nest2.t2(3) &
          nrc.top_nest1.mid_nest1.t2 &
          nrc.t1;
end a;
