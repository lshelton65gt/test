-- Reproduces bug7771 where compiler failed to populate subtype
-- record declaration.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug7771 is
  
  port (
    clk  : in  std_logic;
    in1  : in  std_logic_vector(8 downto 0);
    idx1 : in  std_logic_vector(1 downto 0);
    idx2 : in  std_logic_vector(2 downto 0);
    out1 : out std_logic_vector(8 downto 0));
  
end bug7771;

architecture arch of bug7771 is
  type      BYTE_TYPE      is record
                                DATA            : std_logic_vector(7 downto 0);
                                BEN             : std_logic;
                              end record;

  type      BYTE_ARRAY     is array (INTEGER range <>) of BYTE_TYPE;
  subtype  B_TYPE   is BYTE_ARRAY(7 downto 0); -- record subtype declaration that caused compiler failure.
  type     A_TYPE   is array (3 downto 0) of B_TYPE;

  subtype C_TYPE is BYTE_ARRAY;
  type D_TYPE is array (3 downto 0) of C_TYPE(7 downto 0);

  signal tmp  : A_TYPE := (others => (others => (data => "00100101", ben => '0')));
  signal tmp1 : D_TYPE := (others => (others => (data => "11010110", ben => '1')));

  signal tmpidx1 : std_logic_vector(1 downto 0) := "00";
  signal tmpidx2 : std_logic_vector(2 downto 0) := "000";
  signal tmp1idx1 : std_logic_vector(1 downto 0) := "00";
  signal tmp1idx2 : std_logic_vector(2 downto 0) := "000";

  function conv_to_int (idx : std_logic_vector) return integer is
  begin  -- conv_to_int
    return conv_integer(unsigned(idx));
  end conv_to_int;
  
begin  -- arch

  proc1: process (clk, in1, idx1, idx2)
    variable tmpvidx1 : integer range 0 to 3 := 0;
    variable tmpvidx2 : integer range 0 to 7 := 0;
    variable tmp1vidx1 : integer range 0 to 3 := 0;
    variable tmp1vidx2 : integer range 0 to 7 := 0;
    variable tmp2vidx1 : integer range 0 to 3 := 0;
    variable tmp2vidx2 : integer range 0 to 7 := 0;
  begin
    if (clk'event and clk = '1') then
      tmpvidx1 := conv_to_int(idx1);
      tmpvidx2 := conv_to_int(idx2);
      tmp(tmpvidx1)(tmpvidx2) <= (data => in1(7 downto 0), ben => in1(8));

      tmp1vidx1 := conv_to_int(tmpidx1);
      tmp1vidx2 := conv_to_int(tmpidx2);
      tmp1(tmp1vidx1)(tmp1vidx2) <= tmp(tmpvidx1)(tmpvidx2);
      
      tmp2vidx1 := conv_to_int(tmp1idx1);
      tmp2vidx2 := conv_to_int(tmp1idx2);
      out1 <= tmp1(tmp2vidx1)(tmp2vidx2).ben & tmp1(tmp2vidx1)(tmp2vidx2).data;
    end if;
  end process;

  proc2: process (clk, idx1, idx2)
  begin
    if (clk'event and clk = '1') then
      tmpidx1 <= idx1;
      tmpidx2 <= idx2;
      tmp1idx1 <= tmpidx1;
      tmp1idx2 <= tmpidx2;
    end if;
  end process;
      
end arch;
