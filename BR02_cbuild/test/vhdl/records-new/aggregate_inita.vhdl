library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;

entity aggregate_inita is
  port (in1 : in std_logic; 
        out1 : out integer range 0 to 16
       );
end aggregate_inita;

architecture rtl of aggregate_inita is
  constant const1 : integer := 17;   
  constant const2 : integer := 4; 
  constant const3 : integer := 3; 
  subtype addr1 is std_logic_vector(const2-1 downto 0);
  subtype addr2 is std_logic_vector(const3-1 downto 0);
  type ctype is record
                  faddr : addr1;
                  laddr : addr1;
                  ind   : integer range 0 to const1-1;
                  spl   : boolean;
                  en    : boolean;
                end record;
  type sconfig is array (Natural Range <> ) of ctype;
  constant configv : ctype :=
    ((others => '1'), (others => '0'), 16, false, false);
begin

  process (in1) is
  begin
    if (in1 = '0') then
      out1 <= configv.ind;
    else
      out1 <= 13;
    end if;
  end process;

end rtl;
