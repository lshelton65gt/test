-- Reproduces bug7803 where a composite field select on case select
-- causes an assert in 5481_0927 release. This bug has been fixed
-- in 5481_1001 release by implementing the missing compareHelper()
-- methods for NUCompositeExprs/Lvalues.
library ieee;
use ieee.std_logic_1164.all;

entity bug7803 is

  port (
    ivec : in  std_logic_vector(3 downto 0);
    sel  : in  std_logic_vector(3 downto 0);
    clk  : in  std_logic;
    ovec : out std_logic_vector(3 downto 0));
  
end bug7803;

architecture arch of bug7803 is
  type rec is record
                f0 : std_logic;
              end record;
  type recary is array (3 downto 0) of rec;

  signal recsig : recary := (others => (f0 => '0'));
  signal vecsig : std_logic_vector(3 downto 0) := (others => '0');
begin  -- arch

  proc1: process (clk)
  begin  -- process proc1
    if (clk'event and clk = '1') then
      for i in recary'range loop
        vecsig(i) <= ivec(i);
        recsig(i).f0 <= sel(i);
        case recsig(i).f0 is
          when '0' => ovec(i) <= '0';
          when others => ovec(i) <= vecsig(i);
        end case;
      end loop;  -- i
    end if;
  end process proc1;
  
end arch;
