-- Test passing a record with memory to a proc (output variable test)
-- a collection of data types in the record.  Untested.
library ieee;
use ieee.std_logic_1164.all;

package p is
  type int_arr is array(2 downto 1) of integer ;
  type slv_arr_l is array(0 to 1) of std_logic_vector(64 downto 0);
  type slv_arr_s is array(1 downto 0) of std_logic_vector(3 downto 0);
  type ch_arr is array(3 downto 2) of character ;
  type rec_all is
    record
      r1 : slv_arr_l;
      r2 : slv_arr_s;
      r3 : std_logic;
      i1 : int_arr;
      ch1 : ch_arr;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_proc_o_arrdut is
  port ( recinport : in rec_all
         ; recoutport1: out rec_all
         ; recoutport2: out rec_all
         ; clk: in std_logic
         );
end rec_proc_o_arrdut;

architecture a of rec_proc_o_arrdut is

  procedure proc ( variable outrec : out rec_all;
                   variable o1 : out std_logic;
                   inrec : rec_all;
                   b1, b2 : in std_logic ) is
  begin
    o1 := b1 or b2;
    outrec.r1(1) := inrec.r1(1)(64 downto 32 ) & inrec.r1(0) (31 downto 0);
    outrec.r1(0) := inrec.r1(0)(64 downto 32 ) & inrec.r1(1) (31 downto 0);
    outrec.r2(1) := inrec.r2(0)(2 downto 0) & inrec.r2(1)(3);
    outrec.r2(0) := inrec.r2(1)(2 downto 0) & inrec.r2(0)(3);
    outrec.r3 := inrec.r3 and b1;
    outrec.i1(2) := inrec.i1(1);
    outrec.i1(1) := inrec.i1(2);
    outrec.ch1(3) := inrec.ch1(2);
    outrec.ch1(2) := inrec.ch1(3);
  end proc;


  begin

  p1: process (clk)
    -- note: the temps are here because we can't use signals in
    -- procedure output ports yet; this test was converted from using
    -- signals to variables.
    variable temp1, temp2 : rec_all;
    variable to1, to2 : std_logic;
    variable temp_slv_l : slv_arr_l;
    variable temp_slv_s : slv_arr_s;
    variable temp_int : int_arr;
    variable temp_ch : ch_arr;
  begin
    if clk'event and clk = '1' then
      proc( temp1, to1, recinport, b2 => recinport.r3, b1 => recinport.r2(0)(0) );
      recoutport1 <= temp1;
      recoutport1.r3 <= to1;
      temp_slv_l(1) := temp1.r2(1)&temp1.r1(1)(64 downto 8)&temp1.r1(0)(3 downto 0);
      temp_slv_l(0) := temp1.r2(0)&temp1.r1(0)(64 downto 8)&temp1.r1(1)(3 downto 0);
      temp_slv_s(1) := temp1.r1(0)(7 downto 4);
      temp_slv_s(0) := temp1.r1(1)(7 downto 4);
      temp_int(2) := recinport.i1(2)+recinport.i1(1);
      temp_int(1) := 200;
      
      proc( temp2, to2,  (r2 => temp_slv_s,
                         r1 => temp_slv_l,
                         r3 => recinport.r3 and temp1.r3,
                         i1 => temp_int,
                         ch1=>(others => temp1.ch1(3))),
            temp1.r3,
            recinport.r3);
      to2 := to2 or temp2.r3;
      recoutport2<=temp2; 
      recoutport2.r3 <= to2;
    end if;
  end process p1;
end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_proc_o_arr is
  port (
    clk            : in  std_logic;
    recinport_r1   : in  slv_arr_l;
    recinport_r2   : in  slv_arr_s;
    recinport_r3   : in  std_logic;
    recinport_i1   : in  int_arr;
    recinport_ch1  : in  ch_arr;
    recoutport1_r1   : out  slv_arr_l;
    recoutport1_r2   : out  slv_arr_s;
    recoutport1_r3   : out  std_logic;
    recoutport1_i1   : out  int_arr;
    recoutport1_ch1  : out  ch_arr;
    recoutport2_r1   : out  slv_arr_l;
    recoutport2_r2   : out  slv_arr_s;
    recoutport2_r3   : out  std_logic;
    recoutport2_i1   : out  int_arr;
    recoutport2_ch1  : out  ch_arr);
end rec_proc_o_arr;

architecture a of rec_proc_o_arr is
  component rec_proc_o_arrdut is
                          port ( recinport : in rec_all
                                 ; recoutport1: out rec_all
                                 ; recoutport2: out rec_all
                                 ; clk: in std_logic
                                 );
  end component rec_proc_o_arrdut;

begin

  dut : rec_proc_o_arrdut port map (
    clk              => clk,
    recinport.r1     => recinport_r1,
    recinport.r2     => recinport_r2,
    recinport.r3     => recinport_r3,
    recinport.i1     => recinport_i1,
    recinport.ch1    => recinport_ch1,
    recoutport1.r1   => recoutport1_r1,
    recoutport1.r2   => recoutport1_r2,
    recoutport1.r3   => recoutport1_r3,
    recoutport1.i1   => recoutport1_i1,
    recoutport1.ch1  => recoutport1_ch1,
    recoutport2.r1   => recoutport2_r1,
    recoutport2.r2   => recoutport2_r2,
    recoutport2.r3   => recoutport2_r3,
    recoutport2.i1   => recoutport2_i1,
    recoutport2.ch1  => recoutport2_ch1);
end a;
