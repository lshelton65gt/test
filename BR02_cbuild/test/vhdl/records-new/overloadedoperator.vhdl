library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity overloadedoperator is
  port (
    in1, in2, in3, in4 : in  std_logic_vector(3 downto 0);
    out1, out2     : out std_logic_vector(7 downto 0));
end;

architecture arch of overloadedoperator is
    type rec1 is record
      f1 : unsigned(3 downto 0);
      f2 : unsigned(3 downto 0);
    end record rec1;

    type rec2 is record
      f3 : unsigned(7 downto 0);
      f4 : unsigned(7 downto 0);
    end record rec2;

    function "*" ( i1 : rec1; i2 : rec1) return rec2 is
    begin
      return (i1.f1*i2.f1, i1.f2*i2.f2);
    end "*";
    
    shared variable s2, s3 : rec1;
    shared variable s1 : rec2;
begin

  process (in1, in2, in3, in4)
  begin  -- process
    s2 := (unsigned(in1), unsigned(in2));
    s3 := (unsigned(in3), unsigned(in4));
    s1 := s2 * s3;
    out1 <= std_logic_vector(s1.f3);
    out2 <= std_logic_vector(s1.f4);
  end process;

end arch;
