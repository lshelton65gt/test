-- September 2007
-- The function max_packet returns an array of records.
-- It is called like this: function_name(0 to i). 
-- 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use std.textio.all;

package pkg_test is

    type rec is
       record 
         a      : integer;
         b      : integer;
       end record;
       
    type arr_rec is array (5 to 6) of rec;
    function max_packet(a : integer; b : integer;
                        c : integer; d : integer) return arr_rec;
    
end pkg_test;

package body pkg_test is 
    
    function max_packet(a : integer; b : integer;
                        c : integer; d : integer) return arr_rec
    is
      variable Ret : arr_rec;
    begin
          Ret(5).a := a;
          Ret(5).b := b;
          Ret(6).a := c;
          Ret(6).b := d;
       return Ret;
    end;
    
end pkg_test;    



library ieee;
use ieee.std_logic_1164.all;
use work.pkg_test.all;

entity bug7555_05 is
    port (clk	   : in	std_logic;
	  input_1  : in	integer;
	  input_2  : in	integer;
	  input_3  : in	integer;
	  input_4  : in	integer;
	  output_1 : out integer;
          output_2 : out integer;
	  output_3 : out integer;
          output_4 : out integer);
end bug7555_05;

architecture behav of bug7555_05 is
  signal temp : arr_rec;
begin

  process(clk)
  begin
   if (clk = '1' and clk'event) then
     temp(5 to 6) <=  max_packet(input_1, input_2, input_3, input_4)(5 to 6); -- this line is tested
   end if;
  end process;

  output_1 <= temp(5).a;  
  output_2 <= temp(5).b;  
  output_3 <= temp(6).a;   
  output_4 <= temp(6).b;   
 
end behav;
