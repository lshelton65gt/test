-- September 2007
-- The function max_packet returns a nested record.
-- It is called like this: function_name.record_field, where record field is
-- nested record or simple net.
-- 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use std.textio.all;

package pkg_test is
    type  nested_rec is
      record
        b: integer;  
      end record; 

    type packet is
       record 
         a      : integer;
         b_inst : nested_rec;
       end record;
       
    function max_packet(a : integer; b : integer) return packet;
    
end pkg_test;

package body pkg_test is 
    
    function max_packet(a : integer; b : integer) return packet
    is
      variable Ret : packet;
    begin
          Ret.a := a;
          Ret.b_inst.b := b;
       return Ret;
    end;
    
end pkg_test;    



library ieee;
use ieee.std_logic_1164.all;
use work.pkg_test.all;

entity bug7555_02 is
    port (clk	   : in	 std_logic;
	  input_a  : in	 integer;
	  input_b  : in	 integer;
	  output_a : out integer;
          output_b : out integer);
end bug7555_02;

architecture behav of bug7555_02 is
  signal result  : packet;
begin

  process(clk)
  begin
    if (clk = '1' and clk'event) then
       result.a <= max_packet(input_a, input_b).a;                -- this line is tested
       result.b_inst.b <= max_packet(input_a, input_b).b_inst.b;  -- this line is tested
    end if;
  end process;

  output_a <= result.a;
  output_b <= result.b_inst.b;
              
end behav;
