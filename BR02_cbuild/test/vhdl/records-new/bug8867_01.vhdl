-- May 2008
-- This text case was created from customer design.
-- It used to have simulation mismatch due to the incorrect population of
-- constant record with a field with constraint integer. 
library ieee;
use ieee.std_logic_1164.all;

package pkg_dnova is
  constant c_RtBusAddrNrBits     : integer := 17;
  constant c_RtBusDataNrBits     : integer := 24;
  constant c_RtBusIregDataNrBits : integer := 16;

  type t_RtBusMaster2All is
  record
    Req     : std_ulogic;
    IregReq : std_ulogic;
    Addr    : std_ulogic_vector(c_RtBusAddrNrBits-1 downto 0);
    WrEna   : std_ulogic;
    WrData  : std_ulogic_vector(c_RtBusDataNrBits-1 downto 0);
  end record;

  constant c_RegMaxWidth          : natural := 16;
  type t_RegisterDescriptor is
  record
    order   : natural;                            -- order of the register in the register map
    size    : natural range 1 to c_RegMaxWidth;   -- number of bits in register
    address : natural range 0 to 255;             -- address of IREG
  end record;

  constant c_VdEnbPos     : natural := 14;
  constant c_OutErrCntPos : natural := 13;
  constant c_Vd_DecCtl    : t_RegisterDescriptor := ( 0,      16,        1);
  constant c_Vd_Dcoef0    : t_RegisterDescriptor := ( 1,      16,        2);
  constant c_Vd_Dcoef1    : t_RegisterDescriptor := ( 2,      14,        3);
--  constant c_Vd_ErrCnt    : t_RegisterDescriptor := ( 0,      16,        8);
  type      t_CtlRegisterMap is array(natural range <>) OF t_RegisterDescriptor;
end;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkg_dnova.all;


entity bug8867_01 is
  
  port (
    VdClk             : in std_logic;
    inInt             : in std_ulogic;
    inAddr            : in std_ulogic_vector(1 downto 0);
    out1              : out std_ulogic);

end bug8867_01;

architecture RTL of bug8867_01 is
  constant c_VdRegisterMap : t_CtlRegisterMap := (c_Vd_DecCtl,c_Vd_Dcoef0,c_Vd_Dcoef1);
  signal RtBusMaster2All : t_RtBusMaster2All := ('0', '0', (others => '0'), '0', (others => '0'));
begin

  RtBusMaster2All.IregReq <= inInt;
  RtBusMaster2All.Addr(1 downto 0) <= inAddr;
  
  process(VdClk, RtBusMaster2All)
    VARIABLE v_match       : std_ulogic;
    variable v_IregMuxSel  : natural range 0 to c_VdRegisterMap'length - 1;
    variable v_SapLowByte  : integer range 0 to 255;
  begin
    v_SapLowByte  := TO_INTEGER(UNSIGNED(RtBusMaster2All.Addr(7 downto 0)));
    v_match := '0';
    v_IregMuxSel := 0;
    if RtBusMaster2All.IregReq = '1' then
       for i in c_VdRegisterMap'range loop
         if v_SapLowByte = c_VdRegisterMap(i).address then
            v_match := '1';
            v_IregMuxSel := i;
         end if;
       end loop;
    end if;
  out1 <= v_match;
  end process;

end RTL;
