-- Test blocking sequential whole record assignment, and record aggregates.
-- This also tests a record aggregate with multiple record fields chosen.
library ieee;
use ieee.std_logic_1164.all;

entity record4a is
  port (
    clk: in std_logic
    ;reset: in std_logic
    ;in1  : in  std_logic_vector(7 downto 0)
    ;in2  : in  std_logic_vector(7 downto 0)
    ;outp : out std_logic_vector(7 downto 0)
    );
end record4a;

architecture a of record4a is

type rec is record
    r1 : std_logic_vector(7 downto 0);
    r2 : std_logic_vector(2 downto 0);
    r3 : std_logic_vector(2 downto 0);
end record;

begin

  p1: process (clk, reset)
    variable v1, v2, v3 : rec;
  begin
    if reset = '0' then
      v1 := v3;
      v1 := ("00000000", "000", "000");
      v2 := (r1 => "11111111", r2 => "111", r3 => "011" );
      v3 := (r1 => "01010101", others => (others => '0'));
    elsif clk'event and clk = '1' then
      v1 := (in1, in2(2 downto 0), in2(7 downto 5));
      v2 := (r2 | r3 => in1(7 downto 5),r1 => v1.r1);
      v3 := (r1 => (in1(6 downto 3) & in1(4 downto 1)), others => v1.r1(5 downto 3));
      outp <= v1.r1(0) & v1.r2(1) & v2.r3(2) & v2.r2(0) & v3.r1(5 downto 4) & v3.r2(1 downto 0);
    end if;
  end process p1;
  
end a;
