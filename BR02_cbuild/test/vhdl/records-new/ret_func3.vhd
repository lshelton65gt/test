-- Copyright (c) 2002, 2005 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- 4.11 of test plan version 2.0
-- Function returning record type

package pack_ret_func3 is
  type rec is
    record
       field1 : bit_vector ( 0 to 3);
       field2 : integer range 0 to 1000;
    end record;
end pack_ret_func3;

use work.pack_ret_func3.all;

entity ret_func3 is
  port ( in1,in2 : integer;
         in3,in4 : bit_vector(0 to 1);
         out1: out integer;
         out2: out bit_vector( 0 to 3)
       );
end ret_func3;


architecture ret_func3 of ret_func3 is
  function func ( in1,in2: integer;
                  in3,in4: bit_vector( 0 to 1)
                ) return rec is
   variable my_var : rec;
  begin
   -- The original code from Interra would assign to my_var.field2 when the
   -- result was negative, causing a runtime error.
   if ((in1+in2) <= 1000 and (in1+in2) >= 0) then
    my_var.field2:= in1 + in2;
   else
    my_var.field2 := 999;
   end if;
   my_var.field1 := in3 & in4;
   return my_var;
 end;
 begin
   process ( in1,in2,in3,in4)
    variable my_var : rec;
   begin
      my_var := func(in1,in2,in3,in4);
      out1 <= my_var.field2;
      out2 <= my_var.field1;
   end process;
end;
    

