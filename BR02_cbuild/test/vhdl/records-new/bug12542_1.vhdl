library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package pkg is

  subtype small is integer range 0 to 7;
  
  type rec_t is record
                  f1 : integer;
                  f2 : small;
  end record;

end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.pkg.all;

entity bug12542_1 is
  
  port (
    tout1 : out rec_t;
    tout2 : out integer;
    tout3 : out small;
    tin1 : in  integer;
    tin2 : in  small);

end bug12542_1;

architecture toparch of bug12542_1 is

begin  -- toparch

  tout1 <= (tin1, tin2);
  tout2 <= tin1;
  tout3 <= tin2;

end toparch;
