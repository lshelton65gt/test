-- September 2007
-- The function max_packet returns an array of records with array.
-- It is called like this: function_name(i).arr(j). 
-- 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use std.textio.all;

package pkg_test is

    type arr_int is array (0 to 1) of integer;
    type rec is
       record 
         arr      : arr_int;
       end record;

    type arr_rec is array (0 to 1) of rec;
    
       
    function max_packet(a : integer; b : integer) return arr_rec;
    
end pkg_test;

package body pkg_test is 
    
    function max_packet(a : integer; b : integer) return arr_rec
    is
      variable Ret : arr_rec;
    begin
          Ret(0).arr(0) := a;
          Ret(0).arr(1) := b;
          Ret(1).arr(0) := a;
          Ret(1).arr(1) := b;
       return Ret;
    end;
    
end pkg_test;    



library ieee;
use ieee.std_logic_1164.all;
use work.pkg_test.all;

entity bug7555_08 is
    port (clk	   : in	std_logic;
	  input_1  : in	integer;
	  input_2  : in	integer;
	  output_1 : out integer;
          output_2 : out integer);
end bug7555_08;

architecture behav of bug7555_08 is
  signal temp : arr_int;
begin

process(clk)
begin
   if (clk = '1' and clk'event) then
     temp(0) <= max_packet(input_1, input_2 )(0).arr(0);   -- this line is tested
     temp(1) <= max_packet(input_1, input_2 )(0).arr(1);   -- this line is tested
   end if;
end process;

  output_1 <= temp(0);
  output_2 <= temp(1);
  
  

end behav;
