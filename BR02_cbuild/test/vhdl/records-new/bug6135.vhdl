-- This tests our ability to populate an aggregate for an array of an array of
-- records.
entity bug6135 is
  port (
    clk : in bit;
    o1, o2 : out integer
    );
end entity;

architecture rev1 of bug6135 is
  constant cNumClkDomain : integer := 8;


  type tRange is record
                   high : integer;
                   low : integer;
                 end record;

  type tClk is array (cNumClkDomain-1 downto 0) of integer;
  type tClkArr is array (natural range <>) of tClk;


  type tClkRange is array (cNumClkDomain-1 downto 0) of tRange;
  type tClkRangeArr is array (natural range <>) of tClkRange;

  -- Calculate clock ranges for a clock array
  function CalcRanges (ClkArr : tClkArr) return tClkRangeArr is
    variable result : tClkRangeArr(ClkArr'range);
    variable accum : integer;
  begin
    -- Accumulate number of clocks in each element of ClkArr. Repeat for all
    -- clock domains.
    for DomainIndex in ClkArr(ClkArr'low)'range loop
      accum := 0;
      for ClkIndex in ClkArr'low to ClkArr'high loop
        if ClkArr(ClkIndex)(DomainIndex) /= 0 then
          result(ClkIndex)(DomainIndex).low := accum;
          accum := accum + ClkArr(ClkIndex)(DomainIndex);
          result(ClkIndex)(DomainIndex).high := accum - 1;
        end if;
      end loop;
    end loop;
    return result;
  end function CalcRanges;

  -- Defines different clock domains
  constant cAbp    : integer := 0;
  constant cPrc    : integer := 2;

  -- Gated clock definitions for each sub-block
  constant cTcuNumAbpVClks : integer := 31;
  constant cSckNumAbpVClks : integer := 6;
  constant cHslNumAbpVClks : integer := 5;
  constant cIrqNumAbpVClks : integer := 8; -- etc etc...


  constant cAcbNumPrcVClks : integer := 1;
  constant cAcmNumAbpVClks : integer := 1;
  constant cSimNumAbpVClks : integer := 1;
  constant cCipNumAbpVClks : integer := 1;
  constant cCikNumAbpVClks : integer := 1;
  constant cSemNumAbpVClks : integer := 1;
  constant cAcbNumAbpVClks : integer := 1;



  constant cPhcClks : tClkArr := (
    -- PhyCtrl Blocks
    0   => (cAbp => cTcuNumAbpVClks, others => 5),
    1   => (cAbp => cSckNumAbpVClks, others => 0),
    2   => (cAbp => cHslNumAbpVClks, others => 0),
    3   => (cAbp => cHslNumAbpVClks, others => 0),
    4   => (cAbp => cHslNumAbpVClks, others => 0),
    5   => (cAbp => cIrqNumAbpVClks, others => 0),
    6   => (cAbp => cIrqNumAbpVClks, others => 0),
    7   => (cAbp => cIrqNumAbpVClks, others => 0),
    8   => (cAbp => cIrqNumAbpVClks, others => 0),
    9   => (cAbp => cSimNumAbpVClks, others => 0),
    10  => (cAbp => cCipNumAbpVClks, others => 0),
    11  => (cAbp => cCikNumAbpVClks, others => 0),
    12  => (cAbp => cSemNumAbpVClks, others => 0),
    13  => (cAbp => cAcmNumAbpVClks, others => 0),
    14  => (cAbp => cAcbNumAbpVClks, cPrc => cAcbNumPrcVClks,others => 0),
    15  => (cAbp => cAcbNumAbpVClks, cPrc => cAcbNumPrcVClks,others => 0),
    16  => (cAbp => cAcbNumAbpVClks, cPrc => cAcbNumPrcVClks,others => 0)
    );

  ------------------------------------------------------------------------------
  -- Extract clock fields
  ------------------------------------------------------------------------------
  constant cPhcClkRanges    : tClkRangeArr := CalcRanges(cPhcClks);

  signal i : integer := cPhcClkRanges'left;
  signal j : integer := tClk'left;

begin
  p1: process (clk)
  begin
    if clk'event and clk = '1' then
      o1 <= cPhcClkRanges(i)(j).low;
      o2 <= cPhcClkRanges(i)(j).high;
      if i = cPhcClkRanges'right then
        i <= cPhcClkRanges'left;
      else
        i <= i + 1;
      end if;
      if j = tClk'right then
        j <= tClk'left;
      else
        j <= j - 1;
      end if;
    end if;
  end process p1;
--  o <= cPhcClkRanges(0)(0).low;

end architecture;

