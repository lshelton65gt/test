-- June 2007
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is
  subtype fixed16_type is SIGNED (7 downto 0);
  type complex_fixed_type is
    record
      real1    : fixed16_type;
      imag1    : fixed16_type;
    end record;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
package FFT_CORE_SIG_PKG is
  signal mem_data       : complex_fixed_type;
end FFT_CORE_SIG_PKG;

