-- May 2007
-- This file is used with bug7346_04.vhdl.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is

  type complex_fixed_type is
    record
      real1    : integer;
      imag1    : integer;
    end record;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
package FFT_CORE_SIG_PKG is
  signal simple         : integer;
  signal mem_data       : complex_fixed_type;
end FFT_CORE_SIG_PKG;

