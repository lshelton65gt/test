library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rec_memsel_agg is
  
  port (
    clk  : in  std_logic;
    addr : in  std_logic_vector(3 downto 0);
    in1  : in  std_logic_vector(15 downto 0);
    in2  : in  std_logic_vector(15 downto 0);
    out1 : out std_logic_vector(15 downto 0));

end rec_memsel_agg;

architecture arch of rec_memsel_agg is

  constant width : integer := 16;
  constant groups : integer := 4;
  constant addr_width : integer := 4;
  constant group_width : integer := 4;
  
  type rec is record
                f1 : std_logic_vector(width-1 downto 0);
                f2 : std_logic_vector(width-1 downto 0);
              end record;

  type recary is array (natural range <>) of rec;

  signal var1 : recary(width-1 downto 0) := (others => (others => (others => '0')));
  shared variable temp_var : integer := 0;
  
begin  -- arch

  process (clk, in1, in2, addr)
    variable int_addr : integer := 0;
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge
      int_addr := to_integer(unsigned(addr));

      for i in 0 to groups-1 loop
        temp_var := i;
        var1(temp_var*group_width+group_width-1 downto temp_var*group_width)
          <= (others => (f1 => in1, f2 => in2));
      end loop;  -- i

      out1 <= var1(int_addr).f1 and var1(int_addr).f2;
    end if;
  end process;

end arch;
