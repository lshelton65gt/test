-- September 2009
-- This file tests the sxt function on a composite field expression.
-- Before September 2009 the composite re-synthesis phase was replacing
-- the NUCompositeFieldExpr with NUMemselRvalue without transferring the
-- sign of the expression. That was causing simulation mismatch.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug12902_02 is
  
  port (
    in1  : in  std_logic_vector(7 downto 0);
    sel : integer range 0 to 3;
    out1 : out unsigned(11 downto 0);
    out2 : out unsigned(11 downto 0)
    );

end bug12902_02;


architecture arch of bug12902_02 is
  type rec_t is record
    instr  : std_logic_vector(7 downto 0);
  end record;

  type arr_rec_t is array (0 to 3) of rec_t;

  signal temp : arr_rec_t := (others => (others => (others => '0')));
begin  -- arch


  p1: process (in1)
  begin
    for i in temp'range loop
      temp(i).instr  <= in1;
    end loop; 
    
  end process p1;
  

  p2: process (sel, temp, in1)
  begin  
    out1 <= unsigned(sxt(in1, 12));
    out2 <= unsigned(sxt(temp(sel).instr, 12));  -- this line is tested
  end process p2;

end arch;
