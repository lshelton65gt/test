-- This tests arrayed records, which makes memories of the record elements,
-- with all interior array directions being "to".

library ieee;
--use ieee.numeric_bit.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_1164.all;


entity bug3331clks_to is
  port(clk : in std_logic ;
       in1: in std_logic_vector(0 to 174); 
       --waddr_int : in integer range 0 to 1;
       waddr_int : in std_logic;
       junk1: out std_logic_vector(0 to 12);
       junk2: out std_logic_vector(0 to 40);
       junk3: out std_logic_vector(0 to 63);
       junk4: out std_logic_vector(0 to 0));
end;

architecture bug3331clks_to of bug3331clks_to is
 type h025risefiforecord is
         record
            addr                   : std_logic_vector( 44 to 194);
            cmd                    : std_logic_vector(  10 to 93);
            fmt                    : std_logic_vector(  0 to 14);
         end record;
 type   h025risememtype is array(1 downto 0) of h025risefiforecord;
 signal data2align   : h025risememtype;
begin
  process (clk, in1, data2align)
  begin
     if (clk'event and clk = '1') then
       data2align(CONV_INTEGER(waddr_int)).addr(44 to 69) <= in1(74 to 99);
       data2align(CONV_INTEGER(waddr_int)).addr(70 to 170) <= in1(10 to 110);
       data2align(CONV_INTEGER(waddr_int)).addr(171 to 194) <= in1(0 to 23);
       data2align(CONV_INTEGER(waddr_int)).cmd(11 to 80) <= in1(11 to 80);
     junk1 <= data2align(CONV_INTEGER(waddr_int)).cmd(12 to 24);
     junk2 <= data2align(CONV_INTEGER(waddr_int)).addr(75 to 115); 
     junk3 <= data2align(CONV_INTEGER(waddr_int)).addr(44 to 107); 
     junk4 <= data2align(CONV_INTEGER(waddr_int)).addr(55 to 55); 
     end if;
  end process;


end;

