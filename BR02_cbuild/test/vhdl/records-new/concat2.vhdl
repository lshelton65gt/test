-- Tests concat of std_logic_vector
library ieee;
use ieee.std_logic_1164.all;

entity concat2 is
  
  port (
    in1  : in  std_logic;
    in2  : in  std_logic;
    in3  : in  std_logic;
    in4  : in  std_logic;
    out1 : out std_logic;
    out2 : out std_logic);

end concat2;

architecture arch of concat2 is

  type arrvec is array (natural range<>) of std_logic_vector(1 downto 0);
  
  signal r1 : std_logic_vector(1 downto 0);
  signal r2 : std_logic_vector(1 downto 0);
  signal rv : arrvec(1 downto 0);
  
begin  -- arch

  r1 <= (in1, in2);
  r2 <= (in3, in4);

  rv <= r1 & r2;

  out1 <= rv(0)(0) xor rv(1)(0);
  out2 <= rv(0)(1) xor rv(1)(1);

end arch;
