library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package pkg is

  -- Op-codes
  type grad_opcode_t is (GRAD_OPCODE_DOT_E, GRAD_OPCODE_RCP_E, GRAD_OPCODE_SQRTMUL_E, GRAD_OPCODE_SQRTADD_E);

  -- Number of parallel ALUs
  constant GRAD_NUM_ALUS_C : integer := 2;
  
  -- Number of concurrently executed program threads and thread index type
  constant GRAD_NUM_THREADS_C : integer := 4*GRAD_NUM_ALUS_C;               -- compensate 4 cycles of latency
  subtype grad_thread_idx_t is integer range 0 to GRAD_NUM_THREADS_C-1;     -- for 2 parallel ALUs
  
  -- Gradient unit internal pixel ID, used for identifying consequtive pixels
  subtype grad_pixid_t is integer range 0 to 2*GRAD_NUM_THREADS_C-1; -- theoretically at most

  constant V0_NUM_TXTS_C : integer := 4; -- max number of textures per pixel  
  subtype v0_texidx_t is integer range 0 to V0_NUM_TXTS_C-1;

  -- Number of exponent and mantissa bits in gradient unit floating-point type
  constant GRAD_EXP_SIZE_C : integer := 7;
  constant GRAD_MAN_SIZE_C : integer := 16;
  subtype grad_float_t is std_logic_vector(GRAD_EXP_SIZE_C+GRAD_MAN_SIZE_C downto 0);
  
  type pipe0_t is record                
    valid   : std_logic;                -- valid operation
    opcode  : grad_opcode_t;            -- opcode
    dest    : unsigned(3 downto 0);     -- destination register
    thread  : grad_thread_idx_t;        -- thread index
    pixid   : grad_pixid_t;             -- internal pixel ID
    texid   : v0_texidx_t;              -- texture index    
    last    : std_logic;                -- last operation
    mux1    : grad_float_t;
    mux2    : grad_float_t;
    c       : grad_float_t;
    mux3    : grad_float_t;
    d       : grad_float_t;
    e       : grad_float_t;
  end record;  

end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.pkg.all;

entity bug12542 is
  port (
    pipe0_r     : out pipe0_t;
    op_ena_i    : in  std_logic;
    op_code_i   : in  grad_opcode_t;
    op_dest_i   : in  unsigned (3 downto 0);
    op_thread_i : in  grad_thread_idx_t;
    op_pixid_i  : in  grad_pixid_t;
    op_texid_i  : in  v0_texidx_t;
    op_last_i   : in  std_logic;
    mux1_s      : in  grad_float_t;
    mux2_s      : in  grad_float_t;
    c_i         : in  grad_float_t;
    mux3_s      : in  grad_float_t;
    d_i         : in  grad_float_t;
    e_i         : in  grad_float_t);
 
end bug12542;

architecture toparch of bug12542 is
  
begin  -- toparch

  pipe0_r <= (valid => op_ena_i,
              opcode => op_code_i,
              dest => op_dest_i,
              thread => op_thread_i,
              pixid => op_pixid_i,
              texid => op_texid_i,
              last => op_last_i,
              mux1 => mux1_s,
              mux2 => mux2_s,
              c => c_i,
              mux3 => mux3_s,
              d => d_i,
              e => e_i);

end toparch;
