library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package pkg is

  type rec_t is record
                  f1 : integer range -9 to -3;
                  f2 : integer range -3 to 3;
                  f3 : integer range 3 to 9;
  end record;

end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.pkg.all;

entity bug12542_3 is
  
  port (
    tout1 : out rec_t;
    tout2 : out integer range -9 to -3;
    tout3 : out integer range -3 to 3;
    tout4 : out integer range 3 to 9;
    tin1 : in  integer range -9 to -3;
    tin2 : in  integer range -3 to 3;
    tin3 : in  integer range 3 to 9);

end bug12542_3;

architecture toparch of bug12542_3 is

begin  -- toparch

  tout1 <= (tin1, tin2, tin3);
  tout2 <= tin1;
  tout3 <= tin2;
  tout4 <= tin3;

end toparch;
