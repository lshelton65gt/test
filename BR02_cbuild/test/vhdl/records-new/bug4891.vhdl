library IEEE;
use IEEE.std_logic_1164.all;

package merpack is
type MY_RECORD_TYPE is record
  ch0_select : std_logic;
  ch1_select : std_logic;
  ch2_select : std_logic;
  ch3_select : std_logic;
  addr       : std_logic_vector(11 downto 0);
end record;
end merpack;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.merpack.all;

entity mer_regs is
port (
  clk : in std_logic;
  rst : in std_logic;
  data : in std_logic_vector(15 downto 0);
  MY_REC : buffer MY_RECORD_TYPE
);
end mer_regs;

architecture functional of mer_regs is
  begin
  MY_REC.ch0_select <= data(0);
  MY_REC.ch1_select <= data(1);
  MY_REC.ch2_select <= data(2);
  MY_REC.ch3_select <= data(3);
  MY_REC.addr       <= data(15 downto 4);
end functional;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.merpack.all;

entity mer is
port (
  clk : in std_logic;
  rst : in std_logic;
  data : in std_logic_vector(15 downto 0);
  MY_REC0 : buffer MY_RECORD_TYPE;
  MY_REC1 : buffer MY_RECORD_TYPE
  );
end mer;

architecture functional of mer is

  component mer_regs
  port (
    clk : in std_logic;
    rst : in std_logic;
    data : in std_logic_vector(15 downto 0);
    MY_REC : buffer MY_RECORD_TYPE
  );
  end component;

  begin
    REGS_0 : mer_regs
    port map (
      clk => clk,
      rst => rst,
      data => data,
      MY_REC => MY_REC0
    );
    REGS_1 : mer_regs
    port map (
      clk => clk,
      rst => rst,
      data => data,
      MY_REC => MY_REC1
    );
  end functional;

library IEEE;
use IEEE.std_logic_1164.all;
use WORK.merpack.all;

entity bug4891 is
port (
  clk : in std_logic;
  rst : in std_logic;
  data : in std_logic_vector(15 downto 0);
  MY_REC1 : buffer MY_RECORD_TYPE
  );
end bug4891;

architecture functional of bug4891 is
  component mer
  port (
    clk : in std_logic;
    rst : in std_logic;
    data : in std_logic_vector(15 downto 0);
    -- MY_REC0 : buffer MY_RECORD_TYPE;
    MY_REC1 : buffer MY_RECORD_TYPE
  );
  end component;
begin

  MER0 : mer
    port map (
      clk => clk,
      rst => rst,
      data => data,
      MY_REC1 => MY_REC1
    );

end functional;

