-- Test passing a record with character fields to a function (input param only)
library ieee;
use ieee.std_logic_1164.all;

package p is
  type ch_arr is array(3 downto 2) of character ;
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;

  type rec2 is
    record
      r2 : std_logic_vector(3 downto 0);
      ch1 : ch_arr;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_ch_arrdut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end rec_func_i_ch_arrdut;

architecture a of rec_func_i_ch_arrdut is
  signal sigrec : rec;

  function func2 ( inrec : rec2 ) return std_logic_vector is
    variable outdata : std_logic_vector(3 downto 0);
  begin
    if (inrec.ch1(3)= inrec.ch1(2)) then
      outdata := "00" & inrec.r2(1 downto 0);
    else
      outdata := "11" & inrec.r2(3 downto 2);
    end if;
    return outdata;
  end func2;
begin

  p1: process (clk)
    variable result : std_logic_vector(7 downto 0);
    variable chArr : ch_arr := ('X', 'G');  -- constant
    variable r1 : std_logic_vector(3 downto 0);
    variable r2 : std_logic_vector(3 downto 0);
  begin
    if clk'event and clk = '1' then
      r1 := func2 ((recinport.r2, ('x', 'c')));
      r2 := func2 ((r1, (others=>'x')));
      result := r1 & r2;
      r1 := func2 ((ch1=>chArr, r2=>recinport.r1(7 downto 4)));
      sigrec.r1 <= result;
      sigrec.r2 <= r1;
    end if;
  end process p1;

  p3: process( sigrec )
  begin
    recoutport1 <= sigrec;
  end process;

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_ch_arr is
  port (
    clk            : in  std_logic;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0));
end rec_func_i_ch_arr;

architecture a of rec_func_i_ch_arr is
component rec_func_i_ch_arrdut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end component rec_func_i_ch_arrdut;

begin

  dut : rec_func_i_ch_arrdut port map (
    clk            => clk,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2);

end a;
