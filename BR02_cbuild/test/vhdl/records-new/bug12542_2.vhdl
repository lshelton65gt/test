library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package pkg is

  type rec_t is record
                  f1 : integer;
                  f2 : natural;
  end record;

end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.pkg.all;

entity bug12542_2 is
  
  port (
    tout1 : out rec_t;
    tout2 : out integer;
    tout3 : out natural;
    tin1 : in  integer;
    tin2 : in  natural);

end bug12542_2;

architecture toparch of bug12542_2 is

begin  -- toparch

  tout1 <= (tin1, tin2);
  tout2 <= tin1;
  tout3 <= tin2;

end toparch;
