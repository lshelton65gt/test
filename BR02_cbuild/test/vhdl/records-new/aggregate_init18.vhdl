-- Test array aggregate initialization of 33 downto 2 array of records.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity aggregate_init18 is
  port (d0 : in std_logic;
        d1 : in std_logic;
        d2 : in std_logic;
        d3 : in std_logic;
        addr : in std_logic_vector(4 downto 0);
        out1: out std_logic);
end aggregate_init18;

architecture rtl of aggregate_init18 is
  type ctype is record
    data : std_logic;
    en : std_logic;
  end record;
  type sconfig is array (Natural Range <> ) of ctype;
  constant c0 : ctype := ('1', '0');
  constant c1 : ctype := ('0', '1');
  constant c2 : ctype := ('1', '0');
  constant c3 : ctype := ('0', '1');
  signal mem : sconfig(33 downto 2) := (4 => c2, 3 => c1, 2 => c0, others => c3);
  signal rec : ctype;
begin

  rec <= (d3, '1');
  mem <= ( 4 to 5 => (d2, '0'), 12 downto 7 => (d1, '1'), 16#20# => (d0, '0'), others => rec);

  process(addr, mem)
    variable idx : integer;
  begin
    idx := to_integer(unsigned(addr));
    out1 <= mem(idx+2).data;
  end process;
end rtl;
