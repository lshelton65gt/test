-- Test nested 3-d unconstrained array of records
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

package pack is
  type T1 is                            -- 3 bits
    record
      bot1 : std_logic_vector(0 to 1);
      bot2 : std_logic;
    end record;
  type T2 is array (natural range <>) of T1;  -- array of 3N bits
  type T22 is array (natural range <>) of T2(3 downto 0);  -- 12N bits
  type T222 is array (natural range <>) of T22(7 downto 4);  -- 48N bits
  type T3 is                            -- 196 bits
    record
      top1 : T222(11 downto 8);         -- 192 bits
      top2 : std_logic_vector(0 to 3);  -- 4 bits
    end record;
end pack;

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use work.pack.all;

entity nestedrecord9 is
  port( d : in std_logic_vector(15 downto 0);
        in2, in3, in4 : integer range 0 to 3;
        out1 : out std_logic_vector(0 to 1);
        out2 : out std_logic_vector(0 to 1);
        out3 : out std_logic
        );
end;

architecture arch of nestedrecord9 is
  constant czero : T1 := ("00", '0');
  signal temp : T1 := czero;
  signal in1 : T3;
  
begin
  in1.top2 <= d(15 downto 12);

  p1: process (d, in2, in3, in4)
    variable temp2 : T2(3 downto 0) := (others => czero);
    variable temp3 : T22(7 downto 4) := ( others => (others => czero));
    variable temp4 : T222(11 downto 8) := (others => ( others => (others => czero)));
  begin
    temp2(in2) := (d(10 downto 9), d(0));
    temp3(in3+4) := ((d(9 downto 8), d(7)), others => temp2(in4));
    for i in temp4'range loop
      temp4(i) := temp3;
    end loop;
    temp4(in2+8)(in3+4)(in4).bot1 := "11";
    temp4(in3+8)(in3+4)(in4).bot2 := '1';
    temp4(in2+8)(6)(in4).bot1 := "00";
    temp4(in3+8)(4)(in4).bot2 := '0';
    in1.top1 <= temp4;
  end process p1;
  
  out1 <= in1.top1(8+in2)(4+in3)(in4).bot1;
  temp <= in1.top1(4+in2+4)(1+1+1+1+in4)(in3);
  out2 <= temp.bot1;
  out3 <= temp.bot2;
end arch;
