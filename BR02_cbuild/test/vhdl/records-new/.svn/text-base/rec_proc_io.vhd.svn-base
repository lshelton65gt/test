-- Test passing a record to a proc (inout variable test)
-- a collection of data types in the record.
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec_all is
    record
      r1 : std_logic_vector(64 downto 0);
      r2 : std_logic_vector(3 downto 0);
      r3 : std_logic;
      i1,i2 : integer;
      ch1,ch2 : character;
      b1 : bit;
      bv : bit_vector(7 downto 0) ;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_proc_iodut is
  port ( recinport : in rec_all
         ; recoutport1: out rec_all
         ; recoutport2: out rec_all
         ; clk: in std_logic
         );
end rec_proc_iodut;

architecture a of rec_proc_iodut is

  procedure proc ( variable iorec : inout rec_all;
                   variable io1 : inout std_logic;
                   b1 : in std_logic ) is
  begin
    io1 := b1 or io1;
    iorec.r1 := iorec.r2 & iorec.r1(3 downto 0)&
                 iorec.r1(32 downto 8) & iorec.r1(64 downto 33);
    iorec.r2 := iorec.r1(7 downto 4);
    iorec.r3 := iorec.r3 and io1;
    iorec.i1 := iorec.i2;
    iorec.i2 := iorec.i1;
    iorec.ch2 := iorec.ch1;
    iorec.ch1 := iorec.ch2;
    if iorec.b1 = '1' then
      iorec.b1 := '0';
    else
      iorec.b1 := '1';
    end if;
    iorec.bv := iorec.bv(2 downto 1) & iorec.bv(0) & iorec.bv(7 downto 3);
  end proc;


  begin

  p1: process (clk)
    -- note: the temps are here because we can't use signals in
    -- procedure output ports yet; this test was converted from using
    -- signals to variables.
    variable temp1: rec_all;
    variable to1, to2 : std_logic;
  begin
    if clk'event and clk = '1' then
      temp1 := recinport;
      to1 := recinport.r3;
      proc( temp1, to1, recinport.r2(0) );
      recoutport1 <= temp1;
      recoutport1.r3 <= to1;
      proc( temp1, to1, recinport.r3);
      recoutport2<=temp1; 
      recoutport2.r3 <= to1;
    end if;
  end process p1;
end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_proc_io is
  port (
    clk            : in  std_logic;
    recinport_r1   : in  std_logic_vector(64 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recinport_r3   : in  std_logic;
    recinport_i1   : in  integer;
    recinport_i2   : in  integer;
    recinport_ch1  : in  character;
    recinport_ch2  : in  character;
    recinport_b1   : in  bit;
    recinport_bv   : in  bit_vector(7 downto 0);
    recoutport1_r1   : out  std_logic_vector(64 downto 0);
    recoutport1_r2   : out  std_logic_vector(3 downto 0);
    recoutport1_r3   : out  std_logic;
    recoutport1_i1   : out  integer;
    recoutport1_i2   : out  integer;
    recoutport1_ch1  : out  character;
    recoutport1_ch2  : out  character;
    recoutport1_b1   : out  bit;
    recoutport1_bv   : out  bit_vector(7 downto 0);
    recoutport2_r1   : out  std_logic_vector(64 downto 0);
    recoutport2_r2   : out  std_logic_vector(3 downto 0);
    recoutport2_r3   : out  std_logic;
    recoutport2_i1   : out  integer;
    recoutport2_i2   : out  integer;
    recoutport2_ch1  : out  character;
    recoutport2_ch2  : out  character;
    recoutport2_b1   : out  bit;
    recoutport2_bv   : out  bit_vector(7 downto 0));
end rec_proc_io;

architecture a of rec_proc_io is
  component rec_proc_iodut is
                          port ( recinport : in rec_all
                                 ; recoutport1: out rec_all
                                 ; recoutport2: out rec_all
                                 ; clk: in std_logic
                                 );
  end component rec_proc_iodut;

begin

  dut : rec_proc_iodut port map (
    clk              => clk,
    recinport.r1     => recinport_r1,
    recinport.r2     => recinport_r2,
    recinport.r3     => recinport_r3,
    recinport.i1     => recinport_i1,
    recinport.i2     => recinport_i2,
    recinport.ch1    => recinport_ch1,
    recinport.ch2    => recinport_ch2,
    recinport.b1     => recinport_b1,
    recinport.bv     => recinport_bv,
    recoutport1.r1   => recoutport1_r1,
    recoutport1.r2   => recoutport1_r2,
    recoutport1.r3   => recoutport1_r3,
    recoutport1.i1   => recoutport1_i1,
    recoutport1.i2   => recoutport1_i2,
    recoutport1.ch1  => recoutport1_ch1,
    recoutport1.ch2  => recoutport1_ch2,
    recoutport1.b1   => recoutport1_b1,
    recoutport1.bv   => recoutport1_bv,
    recoutport2.r1   => recoutport2_r1,
    recoutport2.r2   => recoutport2_r2,
    recoutport2.r3   => recoutport2_r3,
    recoutport2.i1   => recoutport2_i1,
    recoutport2.i2   => recoutport2_i2,
    recoutport2.ch1  => recoutport2_ch1,
    recoutport2.ch2  => recoutport2_ch2,
    recoutport2.b1   => recoutport2_b1,
    recoutport2.bv   => recoutport2_bv);
end a;
