-- Test passing a record as inout parameter to  a proc (inout variable test)
-- for bug4212 -- UD confused with the inout parameter.
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
      r3: std_logic;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity bug4212dut is
  port ( recinport: in rec
         ; recoutport: out rec
         ; clk: in std_logic
         );
end bug4212dut;

architecture a of bug4212dut is
  procedure proc ( variable recin : inout rec) is
    begin
      recin.r1 := recin.r2 & recin.r1(7 downto 4);
      recin.r2 := recin.r1(3 downto 0);
      recin.r3 := recin.r3;
    end proc;
begin

  p1: process (clk)
    variable tmp : rec;
  begin
    if clk'event and clk = '1' then
      tmp := recinport;
      proc( tmp);
      recoutport <= tmp;
    end if;
  end process p1;
end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity bug4212 is
  port (
    clk      : in  std_logic;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recinport_r3   : in  std_logic;
    recoutport_r1 : out std_logic_vector(7 downto 0);
    recoutport_r2 : out std_logic_vector(3 downto 0);
    recoutport_r3 : out std_logic);
end bug4212;

architecture a of bug4212 is
  component bug4212dut is
                          port ( recinport: in rec
                                 ; recoutport: out rec
                                 ; clk: in std_logic
                                 );
  end component bug4212dut;

begin

  dut : bug4212dut port map (
    clk            => clk,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recinport.r3   => recinport_r3,
    recoutport.r1 => recoutport_r1,
    recoutport.r2 => recoutport_r2,
    recoutport.r3 => recoutport_r3);
end a;
