-- This tests the ability to populate a multidimensional array of
-- records and to do element assigns and array indexing with them.

entity multidim7 is
  port ( clk : in bit;
         in1 : in bit_vector(3 downto 0);
         o1, o2 : out bit_vector(3 downto 0) );
end entity;

architecture arch of multidim7 is
  type tRange is record
                   high : bit_vector(3 downto 0);
                   low : bit_vector(3 downto 0);
                 end record;
--  type tClkRangeArr is array (7 downto 0, 1 to 2) of tRange;
  type tClkRangeArr is array (natural range <>, natural range<>) of tRange;

  signal s : tClkRangeArr(7 downto 0, 1 to 2);
begin

  p1 : process ( in1 )
  begin
    for i in s'range(1) loop
      for j in s'range(2) loop
        s(i, j).high <= in1;
        s(i, j).low <= not in1;
      end loop;
    end loop;
  end process;

  p2: process (s)
    variable r1 : integer range s'range(1) := s'left(1);
    variable r2 : integer range s'range(2) := s'left(2);
  begin
    o1 <= s(r1, r2).high;
    o2 <= s(r1, r2).low;
    if r1 = 0 then
      r1 := 7;
    else
      r1 := r1 - 1;
    end if;
    if r2 = 1 then
      r2 := 2;
    else
      r2 := 1;
    end if;
  end process p2;
  
end architecture;
