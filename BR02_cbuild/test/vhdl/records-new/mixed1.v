module dut(en, in1, out1, inout1);
   input en;
   input [7:0] in1;
   output [7:0] out1;
   inout [7:0]  inout1;

   assign out1 = inout1;

   assign inout1 = en?in1:8'bzzzzzzzz;

endmodule