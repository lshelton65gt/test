library ieee;
use ieee.std_logic_1164.all;
entity aggregate_init is
  port (in1 : in std_logic; 
        out1: out std_logic;
        out2: out std_logic_vector(15 downto 0));
end aggregate_init;

architecture rtl of aggregate_init is
  constant const1 : integer := 7;   
  constant const2 : integer := 4; 
  constant const3 : integer := 3; 
  subtype addr1 is std_logic_vector(const2-1 downto 0);
  subtype addr2 is std_logic_vector(const3-1 downto 0);
  type ctype is record
                  faddr : addr1;
                  laddr : addr1;
                  ind   : integer range 0 to const1-1;
                  spl   : boolean;
                  en    : boolean;
                end record;
  type sconfig is array (Natural Range <> ) of ctype;
  constant configv : ctype :=
    ((others => '0'), (others => '0'), 0, false, false);
  constant cfg_std : sconfig(0 to const1-1) := (
    ("0000", "0111",  0,   false, configv.en), 
    ("1000", "0100",  11#1#,   false, true),
    others => configv);
  constant cfg_std1 : sconfig(0 to const1-1) := (
    2 to 4 => ("1100", "0110", 1, false, true),
    16#0# => ("0001", "0101",  0, false, true), 
    others => configv);
  signal sig1 : addr1;
begin
  sig1 <= cfg_std(5).faddr when in1 = '1' else cfg_std1(2).laddr;
  
  out1 <= '1' when sig1(2) = '1' else sig1(1);
  out2 <= cfg_std(6).laddr & cfg_std1(1).faddr & cfg_std(0).faddr & cfg_std1(5).laddr;
end rtl;
