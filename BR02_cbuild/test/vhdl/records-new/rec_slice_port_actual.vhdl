-- Tests use of record slice on port actuals.
library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

package tcpkg is


    type bundle_bistStatus is record
        bf:             std_logic;
        bistDone:       std_logic;
    end record;
    type bistStatusArray is array(natural range<>) of bundle_bistStatus;


end tcpkg;


library ieee;
use ieee.std_logic_1164.all;

use work.tcpkg.all;

entity tc_ff is
  port (
  clk:            in  std_logic;
  bistStatus_in:  in  bistStatusArray(2 downto 0);
  bistStatus_out: out bistStatusArray(2 downto 0)
  );
end tc_ff;



architecture synth of tc_ff is

begin

  dff:process (clk)
  begin
    if clk'event and clk = '1' then
	bistStatus_out <= bistStatus_in;
    end if;
  end process;

end synth;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.tcpkg.all;

entity rec_slice_port_actual is
  port (
  clk:              in      std_logic;
  bf_in_bus:        in      std_logic_vector(11 downto 0);
  bistDone_in_bus:  in      std_logic_vector(11 downto 0);
  bf_out_bus:       out     std_logic_vector(11 downto 0);
  bistDone_out_bus: out     std_logic_vector(11 downto 0)
  );
end rec_slice_port_actual;


architecture synth of rec_slice_port_actual is

  component tc_ff 
    port (
    clk:            in  std_logic;
    bistStatus_in:  in  bistStatusArray(2 downto 0);
    bistStatus_out: out bistStatusArray(2 downto 0)
    );
  end component;

  signal  bistStatus_in: bistStatusArray(11 downto 0);
  signal  bistStatus_out: bistStatusArray(11 downto 0);

begin


  busToRecGen: for i in 11 downto 0 generate
  begin
      bistStatus_in(i)  <= (bf_in_bus(i),  bistDone_in_bus(i));
      bf_out_bus(i)       <= bistStatus_out(i).bf;
      bistDone_out_bus(i) <= bistStatus_out(i).bistDone;
  end generate;


  tc_gen: for chn in 3 downto 0 generate
  begin   
      itc_ff: tc_ff 
        port map (
  	clk => clk,
          bistStatus_in              => bistStatus_in((chn*3)+2 downto chn*3),
          bistStatus_out             => bistStatus_out((chn*3)+2 downto chn*3)
        );
  end generate;

end synth;
