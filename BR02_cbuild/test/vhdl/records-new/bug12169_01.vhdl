-- March 2009
-- This file tests constant records declared in a package.

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.numeric_std.all;

package mypack is
  type natural_array_type is array (integer range <>) of natural;
  TYPE dft_shift_type IS
      RECORD
         shift_2     : natural_array_type(0 TO 7);
         shift_3     : natural_array_type(0 TO 4);
         shift_5     : natural_array_type(0 TO 2);
      END RECORD;

  TYPE dft_shift_array_type IS ARRAY (NATURAL RANGE <>) OF dft_shift_type;

  CONSTANT dft_shift_n_lut : dft_shift_array_type(0 TO 3) := (
       0 => ( ( 0, 10, 20, 30, 40, 50, 60, 70 ), ( 100, 200, 300, 400, 500 ), ( 1005, 2005, 3005 ) ),
       1 => ( ( 1, 11, 21, 31, 41, 51, 61, 71 ), ( 101, 201, 301, 401, 501 ), ( 1006, 2006, 3006 ) ),
       2 => ( ( 2, 12, 22, 32, 42, 52, 92, 72 ), ( 102, 202, 302, 402, 502 ), ( 1007, 2007, 3007 ) ),
       3 => ( ( 3, 13, 23, 33, 43, 53, 63, 73 ), ( 103, 203, 303, 403, 503 ), ( 1008, 2008, 3008 ) )
   );

  
end mypack;

use work.mypack.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug12169_01 is
  port(
    index  : in natural range 0 to 3;
    stage2 : in natural range 0 to 7;
    stage3 : in natural range 0 to 4;
    stage5 : in natural range 0 to 2;
    out1   : out natural;
    out2   : out natural;
    out3   : out natural;
    out4   : out natural
    );
end bug12169_01;
architecture arch  of bug12169_01 is
begin  -- a 
  out1 <=  dft_shift_n_lut(index).shift_2(stage2);
  out2 <=  dft_shift_n_lut(index).shift_3(stage3);
  out3 <=  dft_shift_n_lut(index).shift_5(stage5);

  out4 <=  dft_shift_n_lut(1).shift_2(3);
  
end arch ;

