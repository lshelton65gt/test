-- July 2007
-- The signal mem_data is of record type. It is  defined in the
-- package declaration. The entity (not top one) uses the signal in the right
-- and left side assignment. 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is
  subtype fixed4_type is SIGNED (3 downto 0);
  type complex_fixed_type is
    record
      real1    : fixed4_type;
      imag1    : fixed4_type;
    end record;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
package FFT_CORE_SIG_PKG is
  signal mem_data       : complex_fixed_type;
  signal mem_data2      : complex_fixed_type;
end FFT_CORE_SIG_PKG;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity sub is
  port (
    outSubC    : out complex_fixed_type);
end sub;

architecture arch of sub is
begin
--  mem_data2 <= mem_data;       -- this is tested 
  mem_data2.real1 <= mem_data.real1;       -- this is tested 
  mem_data2.imag1 <= mem_data.imag1;       -- this is tested 
  outSubC <= mem_data2;        -- this is tested
end;




library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity bug7346_08 is
  port (
    in1        : in fixed4_type;
    in2        : in fixed4_type;
    out1       : out fixed4_type;
    out2       : out fixed4_type);
end bug7346_08;

architecture arch_top of bug7346_08 is
  signal tempTop : complex_fixed_type;
begin

  mem_data.real1 <= in1;
  mem_data.imag1 <= in2;
  i_sub : entity sub 
     port map (
         outSubC  => tempTop);

  out1 <= tempTop.real1;
  out2 <= tempTop.imag1;
end;
