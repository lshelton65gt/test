-- Test nested 3-d unconstrained array of records.  Gold file generated
-- with Aldec.
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

package pack is
  type T1 is                            -- 2 bits
    record
      bot1 : std_logic_vector(0 to 1);
    end record;
  type T2 is array (1 to 2, 1 to 2) of T1;  -- 8 bits
  type T22 is array (natural range <>) of T2;  -- 8N bits
  type T3 is                            -- 16 bits
    record
      top1 : T22(77 downto 76);
    end record;
  type T4 is array (-3 to -2) of T3;    -- 32 bits
  type T5 is array (integer range <>, natural range <>) of T4;  -- 32NM bits
end pack;

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use work.pack.all;

entity nestedrecord13 is
  port( clk, rst : in std_logic;
        d : in std_logic_vector(1 to 23);
        o : out std_logic_vector(1 to 64)
        );
end;

architecture arch of nestedrecord13 is
  signal in1 : T5(0 downto 0, 101 to 102);  -- 64 bits
  
begin
  p1: process (clk, rst)
    variable temp : T22(78 to 79);
    variable temp2 : T2;
    variable zero : integer := 0;
  begin
    if rst = '0' then
      for a in 0 downto 0 loop
        for b in 101 to 102 loop
          for c in -3 to -2 loop
            for d in 77 downto 76 loop
              for e in 1 to 2 loop
                for f in 1 to 2 loop
                  in1(a,b)(c).top1(d)(e,f).bot1 <= "11";
                end loop;  -- f
              end loop;  -- e
            end loop;  -- d
          end loop;  -- c
        end loop;  -- b
      end loop;  -- a
      in1(0,101)(-3).top1(77)(1,1).bot1 <= "00";
      in1(0,101)(-3).top1(77)(2,1).bot1 <= "00";
      in1(0,101)(-3).top1(77)(1,2).bot1 <= "00";
      in1(0,101)(-3).top1(77)(2,2).bot1 <= "00";
    elsif clk'event and clk = '1' then
      temp(78)(1,1) := (bot1 => d(1 to 2));
      temp(78)(1,2) := (others => d(3 to 4));
      temp(78)(2,1).bot1 := d(5 to 6);
      temp(78)(2,2).bot1 := d(7 to 8);
      temp(79)(1,1).bot1 := d(9 to 10);
      temp(79)(1,2).bot1 := d(11 to 12);
      temp(79)(2,1).bot1 := d(13 to 14);
      temp(79)(2,2).bot1 := d(15 to 16);

      temp2(1,1) := (bot1=>d(16 to 17));
      temp2(2,1) := (others=>d(18 to 19));
      temp2(1,2).bot1 := d(20 to 21);
      temp2(2,2).bot1(1) := d(22);
      temp2(2,2).bot1(0) := d(23);

      in1(zero,101)(-2).top1 <= temp;
      in1(zero,102)(-3).top1 <= temp;
      in1(zero,101)(-2).top1 <= temp;
      in1(zero,102)(-3).top1 <= temp;
      in1(zero,102)(-3).top1(77) <= temp2;
    end if;
  end process p1;

  p2: process (in1)                     -- read out the record into the output
    variable i : integer range 1 to 64 := 1;
  begin
    i := 1;
    for a in 0 downto 0 loop
      for b in 101 to 102 loop
        for c in -3 to -2 loop
          for d in 77 downto 76 loop
            for e in 1 to 2 loop
              for f in 1 to 2 loop
                if i = 63 then
                  i := 1;
                else
                  i := i + 2;
                end if;
                o(i to i+1) <= in1(a,b)(c).top1(d)(e,f).bot1;
              end loop;  -- f
            end loop;  -- e
          end loop;  -- d
        end loop;  -- c
      end loop;  -- b
    end loop;  -- a    
  end process p2;
  
end arch;
