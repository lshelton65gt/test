-- Test for nested record 3.
library ieee;

use ieee.std_logic_1164.all;

package p is

  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;

  type rec_sl is
    record
      r2 : std_logic_vector(3 downto 0);
      r3 : std_logic;
    end record;

  type rec_int is
    record
      sl : rec_sl;
      i1 : integer;
      i2 : integer;
    end record;

  type rec_cb is
    record
      ch1,ch2 : character;
      b1 : bit;
      bv : bit_vector(3 downto 0);
    end record;

  type rec_all is
    record
      r1 : rec_int;
      r2 : rec_cb;
    end record;

  type rec_x is
    record
      x : rec_all;
      y : rec_int;
    end record  ;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_nested3dut is
  port ( recinport : in rec_all
         ; recoutport1: out rec_all
         ; recoutport2: out rec_all
         ; clk: in std_logic
         );
end rec_nested3dut;

architecture a of rec_nested3dut is

  procedure proc ( variable iorec : inout rec_all;
                   variable io1 : inout std_logic;
                   b1 : in std_logic ) is
    variable xi : integer;
    variable xch : character;
  begin
    io1 := b1 or io1;
    iorec.r1.sl.r2 := iorec.r1.sl.r3 & iorec.r1.sl.r2(2 downto 1) & io1;
    iorec.r1.sl.r3 := iorec.r1.sl.r2(2);
    xi := iorec.r1.i1;
    iorec.r1.i1 := iorec.r1.i2;
    iorec.r1.i2 := xi;
    xch := iorec.r2.ch2;
    iorec.r2.ch2 := iorec.r2.ch1;
    iorec.r2.ch1 := xch;
    if iorec.r2.b1 = '1' then
      iorec.r2.b1 := '0';
    else
      iorec.r2.b1 := '1';
    end if;
    iorec.r2.bv := iorec.r2.bv(2 downto 1) &
                   iorec.r2.bv(0) &
                   iorec.r2.bv(3);
  end proc;

  function func (  inrec : rec_all;
                   b1, b2 : in std_logic ) return rec_x is
    variable outrec : rec_x;
  begin
    outrec.x := inrec;
    outrec.y := inrec.r1;
    outrec.x.r1.sl.r3 := b1;
    outrec.x.r1.sl.r2(1) := b2;
    outrec.y.i1 := inrec.r1.i2;
    outrec.y.i2 := inrec.r1.i2;
    outrec.x.r2.ch1 := inrec.r2.ch2;
    if inrec.r2.b1 = '1' then
      outrec.x.r2.b1 := '0';
    else
      outrec.x.r2.b1 := '1';
    end if;
    outrec.x.r2.bv := inrec.r2.bv(2 downto 1) & inrec.r2.bv(0) & inrec.r2.bv(3);
    return outrec;
  end func;


begin
  p1: process (clk)
    variable temp, temp2 : rec_all;
    variable Sl : std_logic;
    variable xx : rec_x;
  begin
    if clk'event and clk = '1' then
      sl := recinport.r1.sl.r2(2);
      xx := func (recinport, recinport.r1.sl.r3, sl);
      proc(xx.x, xx.y.sl.r3, xx.x.r1.sl.r3);
      recoutport1 <= xx.x;
      xx.x.r1 := xx.y;
      proc(xx.x, xx.y.sl.r3, recinport.r1.sl.r3);
      recoutport2 <= xx.x;
    end if;
  end process p1;
end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_nested3 is
  port (
    clk                   : in  std_logic;
    recinport_r1_sl_r2    : in  std_logic_vector(3 downto 0);
    recinport_r1_sl_r3    : in  std_logic;
    recinport_r1_i1       : in  integer;
    recinport_r1_i2       : in  integer;
    recinport_r2_ch1      : in  character;
    recinport_r2_ch2      : in  character;
    recinport_r2_b1       : in  bit;
    recinport_r2_bv       : in  bit_vector(3 downto 0);
    recoutport1_r1_sl_r2   : out  std_logic_vector(3 downto 0);
    recoutport1_r1_sl_r3   : out  std_logic;
    recoutport1_r1_i1      : out  integer;
    recoutport1_r1_i2      : out  integer;
    recoutport1_r2_ch1     : out  character;
    recoutport1_r2_ch2     : out  character;
    recoutport1_r2_b1      : out  bit;
    recoutport1_r2_bv      : out  bit_vector(3 downto 0);
    recoutport2_r1_sl_r2  : out  std_logic_vector(3 downto 0);
    recoutport2_r1_sl_r3  : out  std_logic;
    recoutport2_r1_i1     : out  integer;
    recoutport2_r1_i2     : out  integer;
    recoutport2_r2_ch1    : out  character;
    recoutport2_r2_ch2    : out  character;
    recoutport2_r2_b1     : out  bit;
    recoutport2_r2_bv     : out  bit_vector(3 downto 0));
end rec_nested3;

architecture a of rec_nested3 is
component rec_nested3dut is
  port ( recinport : in rec_all
         ; recoutport1 : out rec_all
         ; recoutport2: out rec_all
         ; clk: in std_logic
         );
end component rec_nested3dut;

begin

  dut : rec_nested3dut port map (
    clk                    => clk,
    recinport.r1.sl.r2     => recinport_r1_sl_r2,
    recinport.r1.sl.r3     => recinport_r1_sl_r3,
    recinport.r1.i1        => recinport_r1_i1,
    recinport.r1.i2        => recinport_r1_i2,
    recinport.r2.ch1       => recinport_r2_ch1,
    recinport.r2.ch2       => recinport_r2_ch2,
    recinport.r2.b1        => recinport_r2_b1,
    recinport.r2.bv        => recinport_r2_bv,
    recoutport1.r1.sl.r2     => recoutport1_r1_sl_r2,
    recoutport1.r1.sl.r3     => recoutport1_r1_sl_r3,
    recoutport1.r1.i1        => recoutport1_r1_i1,
    recoutport1.r1.i2        => recoutport1_r1_i2,
    recoutport1.r2.ch1       => recoutport1_r2_ch1,
    recoutport1.r2.ch2       => recoutport1_r2_ch2,
    recoutport1.r2.b1        => recoutport1_r2_b1,
    recoutport1.r2.bv        => recoutport1_r2_bv,
    recoutport2.r1.sl.r2     => recoutport2_r1_sl_r2,
    recoutport2.r1.sl.r3     => recoutport2_r1_sl_r3,
    recoutport2.r1.i1        => recoutport2_r1_i1,
    recoutport2.r1.i2        => recoutport2_r1_i2,
    recoutport2.r2.ch1       => recoutport2_r2_ch1,
    recoutport2.r2.ch2       => recoutport2_r2_ch2,
    recoutport2.r2.b1        => recoutport2_r2_b1,
    recoutport2.r2.bv        => recoutport2_r2_bv);
end a;
