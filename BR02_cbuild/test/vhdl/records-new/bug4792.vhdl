library ieee;
use ieee.std_logic_1164.all;

entity bug4792 is
  port (
    coeffLBankRdData : in std_logic_vector(127 downto 0);
    outp : out std_logic_vector(127 downto 0));
end bug4792;

architecture arch of bug4792 is
  subtype LmsCoeffData is std_logic_vector (15 downto 0);
  type LmsCoeffComplexData is
    record
      I: LmsCoeffData;
      Q: LmsCoeffData;
    end record;
  type LmsCoeffArray is array (natural range <>) of LmsCoeffComplexData;
  signal loCoeffs : LmsCoeffArray(0 to 3); 

  function unpackLmsCoeffArray (slv : std_logic_vector)
    return LmsCoeffArray is
    variable j : natural;
    variable a : LmsCoeffArray(0 to slv'LENGTH/32-1);
  begin
    for i in a'RANGE loop
      j := slv'LOW + i * 32;
      a(i).I := slv(j+31 downto j+16);
      a(i).Q := slv(j+15 downto j);
    end loop;
    return a;
  end unpackLmsCoeffArray;

begin
  loCoeffs <= unpackLmsCoeffArray(coeffLBankRdData);
  outp <= locoeffs(3).i & locoeffs(2).i & locoeffs(1).i & locoeffs(0).i &
          locoeffs(0).q & locoeffs(1).q & locoeffs(2).q & locoeffs(3).q;
end arch;
