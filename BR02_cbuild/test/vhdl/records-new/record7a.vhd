-- Test non-port unconstrained type arrayed records, referencing individual
-- array members
library ieee;
use ieee.std_logic_1164.all;

package p is
  type umem is array (integer range <>) of std_logic_vector(7 downto 0);
  type cmem is array (7 to 9) of std_logic_vector(3 downto 0);
  type rec is
    record
      r1 : std_logic;
      r2 : std_logic_vector(3 downto 0);
      r3 : umem(17 downto 15);
      r4 : cmem;
    end record;
  type bundlerec is array(natural range<>) of rec;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity record7a_dut is
  port ( recinport : in rec
         ; recoutport: out rec
         ; clk: in bit
         );
end;

architecture a of record7a_dut is
  signal arrayrec : bundlerec(0 to 1);
begin

  arrayrec(0) <= recinport;
  arrayrec(1) <= arrayrec(0);
  recoutport <= arrayrec(1);

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity record7a is
  port (
    clk : in bit;
    ri_r1    : in std_logic;
    ri_r2    : in std_logic_vector(3 downto 0);
    ri_r4_7  : in std_logic_vector(3 downto 0);
    ri_r4_8  : in std_logic_vector(3 downto 0);
    ri_r4_9  : in std_logic_vector(3 downto 0);
    ri_r3_17 : in std_logic_vector(7 downto 0);
    ri_r3_16 : in std_logic_vector(7 downto 0);
    ri_r3_15 : in std_logic_vector(7 downto 0);
    ro_r1    : out std_logic;
    ro_r2    : out std_logic_vector(3 downto 0);
    ro_r4_7  : out std_logic_vector(3 downto 0);
    ro_r4_8  : out std_logic_vector(3 downto 0);
    ro_r4_9  : out std_logic_vector(3 downto 0);
    ro_r3_17 : out std_logic_vector(7 downto 0);
    ro_r3_16 : out std_logic_vector(7 downto 0);
    ro_r3_15 : out std_logic_vector(7 downto 0)
    );
end record7a;

architecture arch of record7a is
  component record7a_dut
    is port ( recinport : in rec
              ; recoutport: out rec
              ; clk: in bit
              );
  end component;
begin

  u1 : record7a_dut port map (
    clk => clk,
    recinport.r1 => ri_r1,
    recinport.r2 => ri_r2,
    recinport.r3(17) => ri_r3_17,
    recinport.r3(16) => ri_r3_16,
    recinport.r3(15) => ri_r3_15,
    recinport.r4(9) => ri_r4_9,
    recinport.r4(8) => ri_r4_8,
    recinport.r4(7) => ri_r4_7,
    recoutport.r1 => ro_r1,
    recoutport.r2 => ro_r2,
    recoutport.r3(17) => ro_r3_17,
    recoutport.r3(16) => ro_r3_16,
    recoutport.r3(15) => ro_r3_15,
    recoutport.r4(9) => ro_r4_9,
    recoutport.r4(8) => ro_r4_8,
    recoutport.r4(7) => ro_r4_7
    );

end arch;
