-- Test for nested record.
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;

    type rec2 is
    record
      r1 : integer;
      r2 : rec;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_nesteddut is
  port ( recinport : in rec
         ; intinport : in  integer
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end rec_nesteddut;

architecture a of rec_nesteddut is

begin

  p1: process (clk, intinport)
    variable result : rec;
    variable x2 : rec;
    variable x3 : std_logic;
    variable xi : integer;
    variable temp : rec2;
  begin
    if clk'event and clk = '1' then
      temp := (intinport, recinport);
      (xi, x2):= temp;
      x2.r1 := recinport.r1(3 downto 0) & recinport.r2(3 downto 0);
      x2.r2 := recinport.r1(7 downto 4);
      if (xi = 15) then
        result := x2;
      else
        result := (x2.r2 & x2.r1(5 downto 2),
                   x2.r1(7 downto 6) & x2.r1(1 downto 0));
      end if;
      recoutport1 <= result;
      recoutport2 <= temp.r2;
    end if;
    
  end process p1;
end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_nested is
  port (
    clk            : in  std_logic;
    num            : in  integer;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0);
    recoutport2_r1 : out std_logic_vector(7 downto 0);
    recoutport2_r2 : out std_logic_vector(3 downto 0));
end rec_nested;

architecture a of rec_nested is
component rec_nesteddut is
  port ( recinport : in rec
         ; intinport : in integer
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end component rec_nesteddut;

begin

  dut : rec_nesteddut port map (
    clk            => clk,
    intinport      => num,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2,
    recoutport2.r1 => recoutport2_r1,
    recoutport2.r2 => recoutport2_r2);
  
end a;
