-- Test passing a record to a function (input param only)
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity record13dut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end record13dut;

architecture a of record13dut is
  signal sigrec : rec;

  function func ( inrec : rec; b1, b2 : std_logic ) return std_logic_vector is
    variable outdata : std_logic_vector(11 downto 0);
  begin
    outdata := (inrec.r1 or "00001111") & (inrec.r2 and "0101");
    outdata(7) := outdata(7) xor b1;
    outdata(1) := outdata(11) xor b2;
    return outdata;
  end func;
begin

  p1: process (clk)
    variable result : std_logic_vector(11 downto 0);
  begin
    if clk'event and clk = '1' then
      result := func( recinport, b2 => clk, b1 => not clk );
      result := func( inrec => (r2 => result(3 downto 0), r1 => result(11 downto 4)),
                      b2 => result(4), b1 => result(9) );
      sigrec.r1 <= result(10 downto 3);
      sigrec.r2 <= result(11) & result(2 downto 0);
    end if;
  end process p1;

  p3: process( sigrec )
  begin
    recoutport1 <= sigrec;
    recoutport2.r1 <= sigrec.r1;
    recoutport2.r2 <= sigrec.r2;
  end process;

end a;


library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity record13 is
  port (
    clk            : in  std_logic;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0);
    recoutport2_r1 : out std_logic_vector(7 downto 0);
    recoutport2_r2 : out std_logic_vector(3 downto 0));
end record13;

architecture a of record13 is
component record13dut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end component record13dut;

begin

  dut : record13dut port map (
    clk            => clk,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2,
    recoutport2.r1 => recoutport2_r1,
    recoutport2.r2 => recoutport2_r2);
  
end a;
