-- June 2007
-- The signal mem_data is of record type. It is  defined in the
-- package declaration. The sub entity gets instantiated and the signal mem_
-- data is used as the port of the entity.
-- 
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library ieee;
use ieee.std_logic_1164.all;
library lib1;
use lib1.FIXED_POINT_PKG.all;
use lib1.FFT_CORE_SIG_PKG.all;
entity bug7346_05 is
  port (
    in11        : in  complex_fixed_type;
    out11       : out complex_fixed_type);
end bug7346_05;

architecture arch_top of bug7346_05 is
begin

  mem_data <= in11;
  i_sub : entity sub 
     port map (
         in1 => mem_data,
         out1  => out11);

  
end;
