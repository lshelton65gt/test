-- September 2009
-- This file tests the sxt function on a composite selected expression.
-- Before September 2009 the composite re-synthesis phase was replacing
-- the NUCompositeSelExpr with NUVarselRvalue without transferring the
-- sign of the expression. That was causing simulation mismatch.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug12902_01 is
  
  generic (
    WIDTH : integer := 32);

  port (
    in1  : in  unsigned(WIDTH - 3 downto 0);
    in2  : in  std_logic_vector(WIDTH - 1 downto 0);
    sel : integer range 0 to 3;
    out1 : out unsigned(WIDTH - 3 downto 0);
    out2 : out unsigned(WIDTH - 3 downto 0);
    out3 : out unsigned(WIDTH - 3 downto 0);
    out4 : out unsigned(WIDTH - 3 downto 0)
    );

end bug12902_01;


architecture arch of bug12902_01 is
  type rec_t is record
    pc_val : unsigned(WIDTH-3 downto 0);
    instr  : std_logic_vector(WIDTH-1 downto 0);
  end record;

  type arr_rec_t is array (0 to 3) of rec_t;

  signal temp : arr_rec_t := (others => ((others => '0'), (others => '0')));
begin  -- arch


  p1: process (in1, in2)
  begin
    for i in temp'range loop
      temp(i).pc_val <= in1;
      temp(i).instr  <= in2;
    end loop; 
    
  end process p1;
  

  p2: process (sel, temp, in1, in2)
  begin  
    out1 <= unsigned(sxt(in2(16 downto 0), WIDTH-2));
    out2 <= unsigned(sxt(temp(sel).instr(16 downto 0), WIDTH-2));  -- this line is tested
    
    out3 <= in1 + unsigned(sxt(in2(16 downto 0), WIDTH-2));
    out4 <= temp(sel).pc_val + unsigned(sxt(temp(sel).instr(16 downto 0), WIDTH-2));
  end process p2;

end arch;
