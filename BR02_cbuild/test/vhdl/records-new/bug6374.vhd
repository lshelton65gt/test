-- last mod: Thu Jul 20 08:05:10 2006
-- filename: test/vhdl/records/bug6374.vhd
-- Description:  This test is from TTPCom and the issue here is the support for
-- the overloaded function for "*" that returns a record


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug6374 is
port (
clk,en,l : in std_logic;
mi,ei : unsigned (7 downto 0);
mo,eo : out unsigned (7 downto 0)
);
end entity;

architecture rev1 of bug6374 is

-- Record types used for complex math.
     type FpUnsigned is record
       Mantissa : unsigned(7 downto 0);
       Exponent : unsigned(7 downto 0);
     end record FpUnsigned;

    signal CorrISquared, CorrQSquared, FloatCorrI, FloatCorrQ : FpUnsigned;

  function Maximum(n1,n2 : integer) return integer is
  begin
    if (n1>n2) then return n1;
    else return n2;
    end if;
  end function Maximum;

  -- This function takes an argument of type unsigned, finds its left-most '1' bit, and
  -- returns the index of that bit.
  function FindMs1(Arg : unsigned) return integer is
    variable vReturnVal : integer range 0 to Arg'length;
  begin
    for I in Arg'reverse_range loop
      if Arg(I)='1' then
        vReturnVal := I;
      end if;
    end loop;
    return vReturnVal;
  end function FindMs1;

  function "*"(L : FpUnsigned; R : FpUnsigned) return FpUnsigned is
    variable vNewExp : unsigned(7 downto 0);
    variable vMantProd : unsigned(7*2 downto 0);
    variable vReturnVal : FpUnsigned;
    variable vMantMs1 : integer range 0 to 7*2;
    variable vMantToRound : unsigned (7 downto 0);
  begin
    -- Calculate the raw new exponent and mantissa
    vNewExp := ('0'&L.exponent) + ('0'&R.exponent);
    vMantProd := (L.Mantissa * R.Mantissa) & '0';
    -- Determine the most significant relevant bit of the new mantissa
    vMantMs1 := Maximum(FindMs1(vMantProd), 7);
    -- Calculate the final new mantissa and exponent
    -- (Need rounding and clipping?)
    for I in vMantToRound'range loop
      vMantToRound(I) := vMantProd(I + vMantMs1-7);
    end loop;
    if vMantToRound = to_unsigned(-1, 7+1) then
      vReturnVal.Mantissa := (vReturnVal.Mantissa'left => '1', others => '0');
      vNewExp := vNewExp+(vMantMs1-7)+1;
    else
      -- vReturnVal.Mantissa := Round(vMantToRound, 7);
      vReturnVal.Mantissa := vMantToRound;
      vNewExp := vNewExp+(vMantMs1-7);
    end if;
    vReturnVal.Exponent := vNewExp(7 downto 0);
    return vReturnVal;
  end function "*";


begin

  CorrISquared <= FloatCorrI*FloatCorrI;
  CorrQSquared <= FloatCorrQ*FloatCorrQ;

  loader : process (clk)
  begin
  	if clk'event and clk='1' then
  		if l = '1' then
  			FloatCorrI.Mantissa <= mi;
  			FloatCorrI.Exponent <= ei;
  			FloatCorrQ.Mantissa <= mi;
  			FloatCorrQ.Exponent <= ei;
  		end if;
  	end if;
  end process;


  pDataPipe3 : process (clk)
  begin
    if clk'event and clk='1' then                 -- #### LINE 219
      if en='1' then
          CorrISquared <= FloatCorrI*FloatCorrI;
          CorrQSquared <= FloatCorrQ*FloatCorrQ;
      end if;
    end if;
  end process pDataPipe3;

  mo <= CorrISquared.Mantissa;
  eo <= CorrISquared.Exponent;

end rev1;
