-- Test port arrayed record field access in ports

library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is record
                r1 : std_logic_vector(7 downto 0);
                r2 : std_logic_vector(3 downto 0);
              end record;

  type bundlerec is array(natural range<>) of rec;
  type conbundlerec is array(34 to 35) of rec;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity dut is
  port ( recinport : in bundlerec(3 downto 2)
         ; recoutport1: out conbundlerec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end dut;

architecture a of dut is
  signal arrayrec : bundlerec(0 to 1);
begin

  p1: process (clk)
  begin  -- process p1
    if clk'event and clk = '1' then  -- rising clock edge
      arrayrec(0).r1 <= recinport(3).r1;
      arrayrec(0).r2 <= recinport(3).r2;
    end if;
  end process p1;

  p2: process (clk)
  begin  -- process p1
    if clk'event and clk = '1' then  -- rising clock edge
      arrayrec(1).r1 <= recinport(2).r1;
      arrayrec(1).r2 <= recinport(2).r2;
    end if;
  end process p2;

  p3: process( arrayrec )
  begin  -- process
    recoutport1(35) <= arrayrec(0);
    recoutport1(34) <= arrayrec(1);
    recoutport2.r1 <= arrayrec(1).r1;
    recoutport2.r2 <= arrayrec(1).r2;
  end process;

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity record12 is
  port (
    rip3_r1    : in  std_logic_vector(7 downto 0);
    rip3_r2    : in  std_logic_vector(3 downto 0);
    rip2_r1    : in  std_logic_vector(7 downto 0);
    rip2_r2    : in  std_logic_vector(3 downto 0);
    rop1_34_r1 : out std_logic_vector(7 downto 0);
    rop1_34_r2 : out std_logic_vector(3 downto 0);
    rop1_35_r1 : out std_logic_vector(7 downto 0);
    rop1_35_r2 : out std_logic_vector(3 downto 0);
    rop2_r1    : out std_logic_vector(7 downto 0);
    rop2_r2    : out std_logic_vector(3 downto 0);
    clk        : in  std_logic);
end record12;

architecture a of record12 is
component dut is
  port ( recinport : in bundlerec(3 downto 2)
         ; recoutport1: out conbundlerec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end component dut;

begin

u1: dut port map (
  recinport(3).r1 => rip3_r1,
  recinport(3).r2 => rip3_r2,
  recinport(2).r1 => rip2_r1,
  recinport(2).r2 => rip2_r2,
  recoutport1(34).r1 => rop1_34_r1,
  recoutport1(34).r2 => rop1_34_r2,
  recoutport1(35).r1 => rop1_35_r1,
  recoutport1(35).r2 => rop1_35_r2,
  recoutport2.r1 => rop2_r1,
  recoutport2.r2 => rop2_r2,
  clk => clk);

end a;
