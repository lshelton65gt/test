-- Test inout ports of record type.  One field is coerced to input, one to output.

library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(7 downto 0);
    end record;
  type bundlerec is array(0 to 1) of rec;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity dut is
  port ( recport : inout rec
         ; clk: in bit
         );
end;

architecture a of dut is
  signal temprec : std_logic_vector(7 downto 0);
begin

  p1: process (clk)
  begin  -- process p1
    if clk'event and clk = '1' then  -- rising clock edge
      temprec <= recport.r1;
    end if;
  end process p1;
  
  p3: process (clk)
  begin  -- process p3
    if clk'event and clk = '1' then  -- rising clock edge
      recport.r2 <= temprec;
    end if;
  end process p3;
  
end a;


library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity inout1 is
  port (
    clk, en : in bit;
    recinport_r1  : in  std_logic_vector(7 downto 0);
    recoutport_r1 : out std_logic_vector(7 downto 0));
end;

architecture arch of inout1 is
  component dut
    is
      port ( recport : inout rec
             ; clk: in bit
             );
  end component;
  signal s1 : rec := ("10101010", "01010101");
begin

  s1.r1 <= recinport_r1;
  
  c1 : dut
    port map (
      clk     => clk,
      recport => s1);

  recoutport_r1 <= s1.r2 when en = '1'
                   else
                   (others => 'Z');
                    
end arch;
