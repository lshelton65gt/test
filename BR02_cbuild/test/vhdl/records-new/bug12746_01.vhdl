-- June 2009
-- Created based on the customer test case.
-- The issue was with const instr_c, which is of type of array of records,
-- The range of the array is defined with subtype range_t.
-- CBUILD was failing to populate correct size of the signal temp with
-- a subtype range.

package bug_pkg is
  
  type instr_t is record
    elm1 : bit_vector(3 downto 0);
    elm2 : bit_vector(3 downto 0);
  end record;
  
  subtype range_t is integer range 0 to 15;
  type instr_array_t is array (range_t) of instr_t;
end bug_pkg;

package body bug_pkg is
end bug_pkg;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

library work;
use work.bug_pkg.all;


entity bug12746_01 is
  
  port (
    clk  : in  bit;
    rst  : in  bit;
    idx  : in  range_t;
    in1  : in  bit_vector(3 downto 0);
    in2  : in  bit_vector(3 downto 0);
    out1 : out bit_vector(3 downto 0);    
    out2 : out bit_vector(3 downto 0));

end bug12746_01;


architecture arch1 of bug12746_01 is 
  signal temp     : instr_array_t;     
  signal idx_temp : integer;          
    
begin  -- arch1

  p1: process (clk, rst)
  begin  
    if rst = '0' then                   
      temp <= (others => ((others => '0'), (others => '0')));
      idx_temp <= 0;
    elsif clk'event and clk = '1' then
      idx_temp <= idx;
      temp(idx).elm1 <= in1;
      temp(idx).elm2 <= in2;
    end if;
  end process p1;

  
  p2: process (clk, rst)
  begin  
    if clk'event and clk = '1' then
      out1 <= temp(idx_temp).elm1;
      out2 <= temp(idx_temp).elm2;
    end if;
  end process p2;

end arch1;
