-- Test for part-select of a 2 dimensional record array.
-- This gives us varsels on field bit nets that become
-- memory nets after composite resynthesis. This design tests
-- varsels on memsels of fields.

-- this is a trimmed down version of bug7878_1, that was used during debugging
-- of bug 7875
library ieee;
use ieee.std_logic_1164.all;

entity bug7878_2 is

  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    ivec : in  std_logic_vector(2 downto 2);
    ovec : out std_logic_vector(6 downto 6));
  
end bug7878_2;

architecture arch of bug7878_2 is

  type rec is record
                f1             : std_ulogic;
              end record;

  type recArray is array (natural range <>) of rec;

  type rec3Array is array (1 to 2) of recArray(5 downto 1);
  type rec4Array is array (1 to 2) of recArray(1 to 9);

  signal sig5dt1           : rec3Array := (others => (others => (f1 => '1')));
  signal sig1to9           : rec4Array := (others => (others => (f1 => '0')));
  
begin  -- arch

  proc1: process (clk, rst)
  begin  -- process proc1
    if rst = '0' then                   -- asynchronous reset (active low)
      ovec <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      sig5dt1(1)(5) <= (f1 => ivec(2));

      for i in 2 to 2 loop
          sig1to9(1)(4 to 5) <= sig5dt1(1)(5 downto (i*2));
      end loop;  -- i

      ovec(6) <= sig1to9(1)(4).f1;
    end if;
  end process proc1;

end arch;
