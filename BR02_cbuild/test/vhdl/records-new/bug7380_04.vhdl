-- August 2007
-- The signal mem_data is type of array of records with the field of array of records.
-- It is defined in a package declaration. The entity (not top one) uses the
-- signal in the left and right side assignment. 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is
  subtype fixed4_type is SIGNED (3 downto 0);
  type t_rec is
    record
      last   : fixed4_type;
    end record;
  type t_arr_rec is array (0 to 3) of t_rec;  
  type t_rec_arr_rec is
    record
      a1   : t_arr_rec;
    end record;

  type t_arr_rec_arr_rec is array (0 to 1) of t_rec_arr_rec;  
  
end FIXED_POINT_PKG;


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
use work.FIXED_POINT_PKG.all;
package FFT_CORE_SIG_PKG is
  signal mem_data     : t_arr_rec_arr_rec;
end FFT_CORE_SIG_PKG;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity sub is
end sub;

architecture arch of sub is
begin
  mem_data(1) <= mem_data(0);           -- this line is tested
end;



library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity bug7380_04 is
  port (
    in1        : in  fixed4_type;
    in2        : in  fixed4_type;
    in3        : in  fixed4_type;
    in4        : in  fixed4_type;
    out1       : out fixed4_type;
    out2       : out fixed4_type;
    out3       : out fixed4_type;
    out4       : out fixed4_type);
end bug7380_04;

architecture arch_top of bug7380_04 is
begin

  mem_data(0).a1(0).last <= in1;             
  mem_data(0).a1(1).last <= in2;             
  mem_data(0).a1(2).last <= in3;             
  mem_data(0).a1(3).last <= in4;             

  i_sub : entity sub;

  out1 <= mem_data(1).a1(0).last;              
  out2 <= mem_data(1).a1(1).last;
  out3 <= mem_data(1).a1(2).last;              
  out4 <= mem_data(1).a1(3).last;              
end;

