-- July 2007
-- A temp1 is of record type with array.
-- It's defined as a variable in the process.
-- It's used in LHS and RHS assignment.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is
  type t_array is array (0 to 1) of integer;

  type rec_arr is
    record
      entry   : t_array;
    end record;
  
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;


library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity bug7493_02 is
  port (
    in1        : in integer;
    in2        : in integer;
    out1       : out integer;
    out2       : out integer);
end bug7493_02;

architecture arch of bug7493_02 is
begin
  p1: process (in1, in2)
    variable temp1 : rec_arr;
  begin  -- process p1
    temp1.entry(0) := in1;               -- this line is tested
    temp1.entry(1) := in2;               -- this line is tested
    out1 <= temp1.entry(0);              -- this line is tested
    out2 <= temp1.entry(1);              -- this line is tested
    
  end process p1;

end;
