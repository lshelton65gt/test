-- Test passing a record with a scalar to a function (input param only)
-- Test for  65-bit slv in record field.
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(8 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;
  type rec2 is
    record
      r2 : std_logic_vector(64 downto 0);
      r1 : std_logic_vector(3 downto 0);
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_slv65dut is
  port ( recinport : in rec2
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end rec_func_i_slv65dut;

architecture a of rec_func_i_slv65dut is
  signal sigrec : rec;

  function func2 ( inrec : rec2; b1,b2 : std_logic ) return std_logic_vector is
    variable outdata : std_logic_vector(8 downto 0);
  begin
    outdata(7) := inrec.r2(6);
    outdata(6) := inrec.r2(7);
    outdata(5) := inrec.r2(17);
    outdata(4) := inrec.r2(5);
    outdata(3) := inrec.r2(22);
    outdata(8) := inrec.r2(61);
    outdata(0) := b1;
    outdata(1) := b2;
    outdata(2) := inrec.r1(0);
    return outdata;
  end func2;
begin

  p1: process (clk)
    variable result : std_logic_vector(8 downto 0);
    variable r2 : std_logic_vector(8 downto 0);
  begin
    if clk'event and clk = '1' then
      result := func2 ((recinport.r2, recinport.r1),
                       recinport.r2(0),
                       recinport.r2(3));
      r2 := func2 ((r1=>"0010",
                    r2=>recinport.r2(63 downto 0)&recinport.r1(3)),
                   result(0),
                   result(8));
      sigrec.r1 <= r2;
      sigrec.r2 <= result(7 downto 4);
    end if;
  end process p1;

  p3: process( sigrec )
  begin
    recoutport1 <= sigrec;
  end process;

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_slv65 is
  port (
    clk            : in  std_logic;
    recinport_r1   : in  std_logic_vector(3 downto 0);
    recinport_r2   : in  std_logic_vector(64 downto 0);
    recoutport1_r1 : out std_logic_vector(3 downto 0);
    recoutport1_r2 : out std_logic_vector(8 downto 0));
end rec_func_i_slv65;

architecture a of rec_func_i_slv65 is
component rec_func_i_slv65dut is
  port ( recinport : in rec2
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end component rec_func_i_slv65dut;

begin

  dut : rec_func_i_slv65dut port map (
    clk            => clk,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r2 => recoutport1_r1,
    recoutport1.r1 => recoutport1_r2);

end a;
