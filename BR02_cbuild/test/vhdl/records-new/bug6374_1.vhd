library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug6374_1 is
  
  port (
    in1  : in  std_logic_vector(3 downto 0);
    in2  : in  std_logic_vector(3 downto 0);
    in3  : in  std_logic_vector(3 downto 0);
    in4  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(7 downto 0);
    out2 : out std_logic_vector(7 downto 0));

end bug6374_1;

architecture arch of bug6374_1 is

  type rec is record
                f1 : integer range 0 to 255;
                f2 : integer range 0 to 255;
              end record;

  function "*"(arg1 : rec; arg2 : rec) return rec is
    variable ret : rec := (others => 0);
  begin  -- *
    ret := (arg1.f1*arg2.f1, arg1.f2*arg2.f2);
    return ret;
  end "*";

begin  -- arch

  process (in1, in2, in3, in4)
    variable arg1 : rec := (others => 0);
    variable arg2 : rec := (others => 0);
    variable res  : rec := (others => 0);
  begin  -- process

    arg1 := (to_integer(unsigned(in1)), to_integer(unsigned(in2)));
    arg2 := (to_integer(unsigned(in3)), to_integer(unsigned(in4)));

    res := arg1 * arg2;

    out1 <= std_logic_vector(to_unsigned(res.f1, 8));
    out2 <= std_logic_vector(to_unsigned(res.f2, 8));
    
  end process;

end arch;
