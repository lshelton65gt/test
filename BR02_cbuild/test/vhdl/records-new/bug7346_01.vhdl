-- May 2007
-- The signal mem_data is of record type. It is  instantiated in the
-- package declaration. The entity uses the signal in the left and right
-- side assignment. 
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is

  type complex_fixed_type is
    record
      real1    : integer;
      imag1    : integer;
    end record;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;


package FFT_CORE_SIG_PKG is
  signal mem_data       : complex_fixed_type;
  signal simple         : integer;
end FFT_CORE_SIG_PKG;

library ieee;
use ieee.std_logic_1164.all;
use work.FFT_CORE_SIG_PKG.all;
entity bug7346_01 is
  port (
    clock      : in  std_logic;
    out1       : out integer;
    out2       : out integer;
    out3       : out integer);
end bug7346_01;

architecture arch of bug7346_01 is
  signal temp1 : integer := 10;
  signal temp2 : integer := 20;
  signal temp3 : integer := 30;
begin
  main: process (clock, mem_data, simple)
  begin 
    if clock'event and clock = '1' then
      mem_data.real1 <= temp1;
      mem_data.imag1 <= temp2;
      simple <= temp3;
    end if;

    out1  <= mem_data.real1;
    out2  <= mem_data.imag1;
    out3  <= simple;
  end process;
end;
