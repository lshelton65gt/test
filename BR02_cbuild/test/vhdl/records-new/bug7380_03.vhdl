-- August 2007
-- The signals mem_data1, mem_data2 and mem_data3 are of record type.
-- They are defined in a package declaration. Two entities (not top one) use the
-- signals in the left and right side assignment. 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is
  type complex_fixed_type is
    record
      real1    : integer;
      imag1    : integer;
    end record;
end FIXED_POINT_PKG;


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
use work.FIXED_POINT_PKG.all;
package FFT_CORE_SIG_PKG is
  signal mem_data1     : complex_fixed_type;
  signal mem_data2     : complex_fixed_type;
  signal mem_data3     : complex_fixed_type;
end FFT_CORE_SIG_PKG;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.FFT_CORE_SIG_PKG.all;
entity sub1 is
end sub1;

architecture arch1 of sub1 is
begin
  mem_data2 <= mem_data1;               -- this line is tested
end;


library ieee;
use ieee.std_logic_1164.all;
use work.FFT_CORE_SIG_PKG.all;
entity sub2 is
end sub2;

architecture arch2 of sub2 is
begin
  mem_data3 <= mem_data2;               -- this line is tested
end;

library ieee;
use ieee.std_logic_1164.all;
use work.FFT_CORE_SIG_PKG.all;
entity bug7380_03 is
  port (
    in1        : in  integer;
    in2        : in  integer;
    out1       : out integer;
    out2       : out integer);
end bug7380_03;

architecture arch_top of bug7380_03 is
begin

  mem_data1.real1 <= in1;             
  mem_data1.imag1 <= in2;             

  i_sub1 : entity sub1; 
  i_sub2 : entity sub2; 

  out1 <= mem_data3.real1;              
  out2 <= mem_data3.imag1;              
end;

