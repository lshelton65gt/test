-- Test nested record instantiation and reference of each individual
-- leaf individually.  Include field names repeated at different levels of
-- hierarchy and multiple instances of the same record type. 

library ieee;
use ieee.numeric_bit.all;

entity nestedrecord1 is
  
  port (
    clk: in bit;
    inp : in bit_vector(50 downto 0);
    outp : out bit_vector(3 downto 0));

end nestedrecord1;

architecture a of nestedrecord1 is
  type bottomrec is record
                      b1     : bit;
                      b2, b3 : bit;
                      t2     : bit_vector(3 downto 0);  -- reuse field name
                    end record;

  type midrec is record
                   mid_nest1 : bottomrec;
                   m2        : bit;
                   m3        : integer;
                   mid_nest2 : bottomrec;  -- multiple instance of the same
                                           -- record type
                 end record;

  type toprec is record
                   t1        : bit_vector(1 to 2);
                   t2        : bit_vector(3 to 4);
                   top_nest1 : midrec;
                   t4        : bit;
                 end record;

  signal nrc : toprec;
  
begin

  p1: process (clk )
  begin
    if clk'event and clk = '1' then
      nrc.t1 <= inp(49 downto 48);
      nrc.t2 <= inp(47 downto 46);
      nrc.top_nest1.mid_nest1.b1 <= inp(45);
      nrc.top_nest1.mid_nest1.b2 <= inp(44);
      nrc.top_nest1.mid_nest1.b3 <= inp(43);
      nrc.top_nest1.mid_nest1.t2 <= inp(42 downto 39);
      nrc.top_nest1.m2 <= inp(38);
      nrc.top_nest1.m3 <= to_integer( signed(inp(38 downto 7)));
      nrc.top_nest1.mid_nest2.b1 <= inp(6);
      nrc.top_nest1.mid_nest2.b2 <= inp(5);
      nrc.top_nest1.mid_nest2.b3 <= inp(4);
      nrc.top_nest1.mid_nest2.t2 <= inp(3 downto 0);
      nrc.t4 <= inp(50);
    end if;
  end process p1;

  outp <= nrc.t4 &
          nrc.top_nest1.m2 &
          nrc.top_nest1.mid_nest1.b2 &
          nrc.top_nest1.mid_nest2.t2(2);
end a;
