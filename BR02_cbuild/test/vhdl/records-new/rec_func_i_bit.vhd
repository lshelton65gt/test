-- Test passing a record with a scalar bit to a function (input param only)
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;

  type rec2 is
    record
      r2 : std_logic_vector(3 downto 0);
      b1 : bit;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_bitdut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end rec_func_i_bitdut;

architecture a of rec_func_i_bitdut is
  signal sigrec : rec;

  function func2 ( inrec : rec2; b1 :  bit ) return std_logic_vector is
    variable outdata : std_logic_vector(7 downto 0);
  begin
    outdata := inrec.r2 & "1010";
    if (inrec.b1 = b1) then
      outdata(4) := inrec.r2(2);
      outdata(7) := inrec.r2(3);
      outdata(6) := inrec.r2(1);
      outdata(5) := inrec.r2(0);
    else
      outdata(4) := inrec.r2(1);
      outdata(7) := inrec.r2(0); 
      outdata(6) := inrec.r2(2);
      outdata(5) := inrec.r2(3);
    end if;
    return outdata;
  end func2;
begin

  p1: process (clk)
    variable result : std_logic_vector(7 downto 0);
    variable r2 : std_logic_vector(7 downto 0);
    variable b  : bit;
  begin
    if clk'event and clk = '1' then
      if recinport.r1(0) = '1' then
        b := '1';
      else
        b := '0';
      end if;
      result := func2 ((recinport.r2, b1=>b), '0');
      r2 := func2 ((b1=>b, r2=>result(7 downto 4)), '1');
      sigrec.r1 <= r2;
      sigrec.r2 <= result(3 downto 0);
    end if;
  end process p1;

  p3: process( sigrec )
  begin
    recoutport1 <= sigrec;
  end process;

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_bit is
  port (
    clk            : in  std_logic;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0)); end rec_func_i_bit;

architecture a of rec_func_i_bit is
component rec_func_i_bitdut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end component rec_func_i_bitdut;

begin

  dut : rec_func_i_bitdut port map (
    clk            => clk,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2);

end a;
