-- June 2007
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is
  subtype fixed4_type is SIGNED (3 downto 0);
  type complex_fixed_type is
    record
      real1    : fixed4_type;
      imag1    : fixed4_type;
    end record;

  type rec_rec is
    record
      a1 : complex_fixed_type;
      b1 : complex_fixed_type;
    end record;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
package FFT_CORE_SIG_PKG is
  signal mem_data       : rec_rec;
end FFT_CORE_SIG_PKG;

