-- Has a problem with parsing record aggregates being assigned to record array part selects with dynamic index, but constant width.
library ieee;
use ieee.std_logic_1164.all;

package pkg is

  type vec1 is array (8 downto 0) of std_logic_vector(7 downto 0);
  type vec2 is array (9 downto 0) of vec1;
  type vec3 is array (10 downto 0) of vec2;

  type rec is record
                f1 : std_logic_vector(7 downto 0);
                f2 : std_logic_vector(7 downto 0);
              end record;
  type rec1vec is array (8 downto 0) of rec;
  type rec2vec is array (9 downto 0) of rec1vec;
  type rec3vec is array (10 downto 0) of rec2vec;
end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkg.all;

entity rec_var_slice_agg is

  port (
    in1  : in  vec3;
    in2  : in  vec3;
    sel0 : in  std_logic_vector(1 downto 0);
    sel1 : in  std_logic_vector(1 downto 0);
    sel2 : in  std_logic_vector(1 downto 0);
    sel3 : in  std_logic_vector(1 downto 0);
    out1 : out vec3);
  
end rec_var_slice_agg;

architecture arch of rec_var_slice_agg is
  signal sig : rec3vec := (others => (others => (others => (others => (others => '0')))));
  shared variable int_sel0 : integer range 0 to 3 := 0;
  shared variable int_sel1 : integer range 0 to 3 := 0;
  shared variable int_sel2 : integer range 0 to 3 := 0;
  shared variable int_sel3 : integer range 0 to 3 := 0;
begin  -- arch

  process (in1, in2, sel3, sel2, sel1, sel0, sig)
  begin  -- process
    int_sel0 := to_integer(unsigned(sel0));
    int_sel1 := to_integer(unsigned(sel1));
    int_sel2 := to_integer(unsigned(sel2));
    int_sel3 := to_integer(unsigned(sel3));

    sig(int_sel3)(int_sel2)(int_sel1).f1(1+int_sel0*2 downto int_sel0*2) <= in1(int_sel3)(int_sel2)(int_sel1)(1+int_sel0*2 downto int_sel0*2);
    sig(int_sel3)(int_sel2)(1+int_sel1*2 downto int_sel1*2) <= (others => (f1 => in1(int_sel3)(int_sel2)(int_sel1), others => in2(int_sel3)(int_sel2)(int_sel1)));
    sig(int_sel3)(1+int_sel2*2 downto int_sel2*2) <= (others => (others => (f1 => in1(int_sel3)(int_sel2)(int_sel1), others => in2(int_sel3)(int_sel2)(int_sel1))));
    sig(1+int_sel3*2 downto int_sel3*2) <= (others => (others => (others => (f1 => in1(int_sel3)(int_sel2)(int_sel1), others => in2(int_sel3)(int_sel2)(int_sel1)))));

    out1(int_sel3)(int_sel2)(int_sel1)(1+int_sel0*2 downto int_sel0*2) <= sig(int_sel3)(int_sel2)(int_sel1).f1(1+int_sel0*2 downto int_sel0*2) and sig(int_sel3)(int_sel2)(int_sel1).f2(1+int_sel0*2 downto int_sel0*2);
    out1(int_sel3)(int_sel2)(1+int_sel1*2 downto int_sel1*2) <= (others => sig(int_sel3)(int_sel2)(int_sel1).f1(1+int_sel0*2 downto int_sel0*2) and sig(int_sel3)(int_sel2)(int_sel1).f2(1+int_sel0*2 downto int_sel0*2));
    out1(int_sel3)(1+int_sel2*2 downto int_sel2*2) <= (others => (others => sig(int_sel3)(int_sel2)(int_sel1).f1(1+int_sel0*2 downto int_sel0*2) and sig(int_sel3)(int_sel2)(int_sel1).f2(1+int_sel0*2 downto int_sel0*2)));
    out1(1+int_sel3*2 downto int_sel3*2) <= (others => (others => (others => sig(int_sel3)(int_sel2)(int_sel1).f1(1+int_sel0*2 downto int_sel0*2) and sig(int_sel3)(int_sel2)(int_sel1).f2(1+int_sel0*2 downto int_sel0*2))));
  end process;

end arch;
