-- June 2007
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library ieee;
use ieee.std_logic_1164.all;
library lib1;
use lib1.FIXED_POINT_PKG.all;
use lib1.FFT_CORE_SIG_PKG.all;
entity sub is
  port (
    in1       : in  complex_fixed_type;
    out1      : out complex_fixed_type);
end sub;

architecture arch of sub is
begin
    out1  <= in1;
end;
