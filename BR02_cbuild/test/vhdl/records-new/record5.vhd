-- Test record ports

package p is

type rec is record
    r1 : bit_vector(7 downto 0);
    r2 : bit_vector(3 downto 0);
end record;

end p;

use work.p.all;

entity dut is
  port ( recinport : in rec
       ; recoutport: out rec
       );
end dut;

architecture a of dut is
begin
  recoutport.r1 <= recinport.r1;
  recoutport.r2 <= recinport.r2;
end a;


use work.p.all;

entity record5 is
  port (
    rir1 : in  bit_vector(7 downto 0);
    rir2 : in  bit_vector(3 downto 0);
    ror1 : out bit_vector(7 downto 0);
    ror2 : out bit_vector(3 downto 0));
end record5;

architecture arch of record5 is
component dut is
  port ( recinport : in rec
       ; recoutport: out rec
       );
end component dut;

begin

  u1 : dut port map (
    recinport.r1 => rir1,
    recinport.r2 => rir2,
    recoutport.r1 => ror1,
    recoutport.r2 => ror2);

end arch;
