-- Test passing a record with a scalar bit to a function (input param only)
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;

  type rec2 is
    record
      b  : bit;
      bv : bit_vector(7 downto 0) ;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_bvdut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end rec_func_i_bvdut;

architecture a of rec_func_i_bvdut is
  signal sigrec : rec;

  function func2 ( inrec : rec2; b1:  bit ) return std_logic_vector is
    variable outdata : std_logic_vector(7 downto 0);
  begin
    for i in 0 to 7 loop
      if inrec.bv(i) = b1 and inrec.b = b1 then
        outdata(i) := '1';
      else
        outdata(i) := '0';
      end if;
    end loop; 
    return outdata;
  end func2;
begin

  p1: process (clk)
    variable result : std_logic_vector(7 downto 0);
    variable r1 : std_logic_vector(7 downto 0);
    variable bvVar : bit_vector(7 downto 0);
  begin
    if clk'event and clk = '1' then
      for i in 0 to 7 loop
        if (recinport.r1(i)='1') then
          bvVar(i) := '1';
        else
          bvVar(i) := '0';
        end if;
      end loop;  -- i
      r1 := func2 ((bvVar(0), "10101010"), bvVar(7));
      result := func2 ((bv=>bvVar, b=>bvVar(4)), bvVar(5));
      sigrec.r1 <= result;
      sigrec.r2 <= r1(3 downto 0);
    end if;
  end process p1;

  p3: process( sigrec )
  begin
    recoutport1 <= sigrec;
  end process;

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_bv is
  port (
    clk            : in  std_logic;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0)); end rec_func_i_bv;

architecture a of rec_func_i_bv is
component rec_func_i_bvdut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end component rec_func_i_bvdut;

begin

  dut : rec_func_i_bvdut port map (
    clk            => clk,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2);

end a;
