-- June 2007
-- The signal mem_data is of record type, which has an array as its field.
-- It is  defined in the package declaration. The sub entity gets instantiated
-- and the signal mem_data is used as the port of the entity.
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library ieee;
use ieee.std_logic_1164.all;
library lib1;
use lib1.FIXED_POINT_PKG.all;
use lib1.FFT_CORE_SIG_PKG.all;
entity bug7346_07 is
  port (
    in11       : in  fixed4_type;
    in12       : in  fixed4_type;
    out11      : out fixed4_type;
    out12      : out fixed4_type);
end bug7346_07;

architecture arch_top of bug7346_07 is
  signal mem_data1       : rec_arr;
begin

  mem_data.mem1(0) <= in11;
  mem_data.mem1(1) <= in12;
  
  i_sub : entity sub 
     port map (
         in1 => mem_data,
         out1  => mem_data1  );

  out11 <= mem_data1.mem1(0);
  out12 <= mem_data1.mem1(1);
end;
