-- This is adapted from the beacon6 test misc.rec2.
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

package pack is
  type T1 is
    record
      bot1 : std_logic_vector(0 to 1);
      bot2 : std_logic;
    end record;
  type T2 is array (3 downto 0) of T1;
  type T3 is
    record
      top1 : T2;
      top2 : std_logic_vector(0 to 3);
    end record;
end pack;

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use work.pack.all;

entity nestedrecord7 is
  port( d : in std_logic_vector(15 downto 0);
        in2 : integer range 0 to 3;
        out1 : out std_logic_vector(0 to 1);
        out2 : out std_logic_vector(0 to 1);
        out3 : out std_logic
        );
end;

architecture arch of nestedrecord7 is
  signal in1 : T3;
  signal temp : T1;
begin
  -- unpack the input vector into in1
  in1.top2 <= d(15 downto 12);
  in1.top1(3) <= (d(11 downto 10), d(9));
  in1.top1(2) <= (bot2 => d(8), bot1 => d(7 downto 6));
  in1.top1(1) <= (d(5 downto 4), d(3));
  in1.top1(0) <= (d(2 downto 1), d(0));
  
  out1 <= in1.top1(in2).bot1;
  temp <= in1.top1(in2);
  out2 <= temp.bot1;
  out3 <= temp.bot2;
end arch;
