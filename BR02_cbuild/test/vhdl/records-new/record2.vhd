--Test using a record type defined in a package
package p is

type rec is record
    r1 : bit_vector(7 downto 0);
    r2 : bit_vector(7 downto 0);
end record;

end p;

entity record2 is
  port (
    in1  : in  bit_vector(7 downto 0)
    ;in2  : in  bit_vector(7 downto 0)
    ;outp : out bit_vector(7 downto 0)
    );
end record2;

architecture a of record2 is

signal s1: work.p.rec;

begin
  a.s1.r1 <= record2.in1;
  s1.r2 <= in2;
  outp <= s1.r1 and s1.r2;
end a;
