-- Test nested 3-d unconstrained array of records
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

package pack is
  type T1 is                            -- 3 bits
    record
      bot1 : std_logic_vector(0 to 1);
      bot2 : std_logic;
    end record;
  type T2 is array (natural range <>, natural range <>, natural range <>) of T1;
  type T3 is                            -- 196 bits
    record
      top1 : T2(11 downto 8, 7 downto 4, 3 downto 0);         -- 192 bits
      top2 : std_logic_vector(0 to 3);  -- 4 bits
    end record;
end pack;

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use work.pack.all;

entity nestedrecord11 is
  port( clk, rst : in std_logic;
        d : in std_logic_vector(15 downto 0);
        in2, in3, in4 : integer range 0 to 3;
        out1 : out std_logic_vector(0 to 1);
        out2 : out std_logic_vector(0 to 1);
        out3 : out std_logic
        );
end;

architecture arch of nestedrecord11 is
  constant czero : T1 := ("00", '0');
  signal temp : T1 := czero;
  signal in1 : T3;
  
begin
  in1.top2 <= d(15 downto 12);

  p1: process (clk,rst)
    variable temp2 : T2(11 downto 8, 7 downto 4, 3 downto 0);
  begin
    if rst = '0' then
      for i in temp2'range(1) loop
        for j in temp2'range(2) loop
          for k in temp2'range(3) loop
            temp2(i,j,k) := ("00",'0');
          end loop;  -- k
        end loop;  -- j
      end loop;  -- i
      temp2(11,7,3).bot1 := "11";
      temp2(11,7,3).bot2 := '1';
    elsif clk'event and clk = '1' then
      temp2(in2+8,in3+4,in4) := ((d(in2),d(in3)),d(4));
    end if;
    in1.top1 <= temp2;
  end process p1;
  
  out1 <= in1.top1(8+in2,4+in3,in4).bot1;
  temp <= in1.top1(4+in2+4,6,in3);
  out2 <= temp.bot1;
  out3 <= temp.bot2;
end arch;
