-- Test lotsw of different forms of array aggregate initialization.
-- Gold file generated with MTI, as Aldec doesn't get the direction
-- correct for config5.
library ieee;
use ieee.std_logic_1164.all;
entity aggregate_init1 is
  port (in1 : in integer range 0 to 9;
        out1: out boolean;
        out2: out std_logic_vector(3 downto 0));
end aggregate_init1;

architecture rtl of aggregate_init1 is
  type ctype is record
    faddr : std_logic_vector(3 downto 0);
    en    : boolean;
  end record;

  
  constant config0 : ctype := ((others => '0'), false);
  constant config1 : ctype := ("0001", en => false);
  constant config2 : ctype := ("0010", true);
  constant config3 : ctype := (en => true, faddr => "0100");
  constant config4 : ctype := (faddr => "1000", en => false);
  -- Aldec gets this one backwards.
  constant config5 : ctype := (std_logic_vector'(1 downto 0 => '0', 3 downto 2 => '1' ), true);
  constant config6 : ctype := ((1 => '1', 2 => '0', 3 => '1', 0 => '1'), false);
  constant config7 : ctype := ((1 downto 0 => std_logic'('0'), 3 downto 2 => std_logic'('1') ), true);
  constant config8 : ctype := (std_logic_vector'(3 downto 2 => '1', 1 downto 0 => '0'), true);
  constant config9 : ctype := (std_logic_vector'(0 to 1 => '0', 2 to 3 => '1' ), true);

  type sconfig is array (Natural Range <> ) of ctype;
  constant cfg_std : sconfig(0 to 9) := (
    config0, config1, config2, config3, config4,
    config5, config6, config7, config8, config9
    );
  
begin
  out1 <= cfg_std(in1).en;
  out2 <= cfg_std(in1).faddr;
end rtl;
