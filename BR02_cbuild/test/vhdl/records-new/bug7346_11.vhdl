-- July 2007
-- The signal mem_data is array of records.
-- It is  defined in the package declaration. The entity (not top one) uses the
-- signal in the left and right side assignment. 
 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is
  subtype fixed4_type is SIGNED (15 downto 0);
  type complex_fixed_type is
    record
      real1    : fixed4_type;
      imag1    : fixed4_type;
    end record;

  type arr_rec is array (0 to 1) of complex_fixed_type;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
package FFT_CORE_SIG_PKG is
  signal mem_data      : arr_rec;
end FFT_CORE_SIG_PKG;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity sub is
  port (
    out1      : out complex_fixed_type);
end sub;

architecture arch of sub is
begin
  mem_data(1) <=  mem_data(0);           -- this is tested
  out1  <= mem_data(1);                  -- this is tested
end;




library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity bug7346_11 is
  port (
    in1        : in fixed4_type;
    in2        : in fixed4_type;
    out1       : out fixed4_type;
    out2       : out fixed4_type);
end bug7346_11;

architecture arch_top of bug7346_11 is
  signal top_rec       : complex_fixed_type;
begin

  mem_data(0).real1 <= in1;
  mem_data(0).imag1 <= in2;
  i_sub : entity sub 
     port map (
         out1   => top_rec);

  out1 <= top_rec.real1;
  out2 <= top_rec.imag1;
end;
