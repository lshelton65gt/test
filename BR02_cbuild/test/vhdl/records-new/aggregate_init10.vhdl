-- Test for array of array of record which contains a record array field
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity aggregate_init10 is
  port (sel_i : in std_logic_vector(0 downto 0);
        sel_j : in std_logic_vector(0 downto 0);
        fsel : in std_logic_vector(2 downto 0);
        out1: out std_logic_vector(5 downto 0));
end aggregate_init10;

architecture rtl of aggregate_init10 is
  constant const1 : integer := 64;   
  constant const2 : integer := 2; 
  constant const3 : integer := 3; 

  subtype fint is integer range 0 to const1-1;

  type dtype is record
                  f11 : fint;
                  f12 : fint;
                end record;

  type dconfig is array (Natural Range <>) of dtype;

  type ctype is record
                  f1 : fint;
                  f2 : fint;
                  f3 : dconfig(const2-1 downto 0);
                  f4 : fint;
                end record;

  type sconfig is array (Natural Range <> ) of ctype;

  type tconfig is array (Natural range <>) of sconfig(const3-1 downto 0);
  
  constant c1 : ctype := (2, 8, ((15, 27),(16, 28)), 38);
  constant c2 : ctype := (3, 9, ((17, 29),(18, 30)), 39);
  constant c3 : sconfig(const3-1 downto 0) := (2 => (1, 7, ((13, 25),(14, 26)), 37), 1 => c1, others => c2);
  constant c4 : tconfig(const2-1 downto 0) := (1 => c3, 0 => ((4, 10, ((19, 31),(20, 32)), 40), (5, 11, ((21, 33), (22, 34)), 41), (6, 12, ((23, 35), (24, 36)), 42)));
  
  signal sig1 : tconfig(const2-1 downto 0);
begin
  sig1 <= c4;

  process (sel_i, sel_j, sig1, fsel)
    variable lsel_i : integer;
    variable lsel_j : integer;
    variable lfsel : integer;
    variable lout  : fint;
  begin
    lsel_i := to_integer(unsigned(sel_i));
    lsel_j := to_integer(unsigned(sel_j));
    lfsel := to_integer(unsigned(fsel));
    case lfsel is
      when 0 => lout := sig1(lsel_i)(lsel_j).f1;
      when 1 => lout := sig1(lsel_i)(lsel_j).f2;
      when 2 => lout := sig1(lsel_i)(lsel_j).f3(0).f11;
      when 3 => lout := sig1(lsel_i)(lsel_j).f3(0).f12;
      when 4 => lout := sig1(lsel_i)(lsel_j).f3(1).f11;
      when 5 => lout := sig1(lsel_i)(lsel_j).f3(1).f12;
      when 6 => lout := sig1(lsel_i)(lsel_j).f4;
      when others => lout := 0;
    end case;
    out1 <= std_logic_vector(to_unsigned(lout, out1'length));
  end process;
end rtl;
