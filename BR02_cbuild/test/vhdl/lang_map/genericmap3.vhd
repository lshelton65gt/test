package mypack is
type myint is range 5 to 10;
end mypack;

use work.mypack.all;
entity bottom is
  generic ( g1 : myint := 5); 
  port (i1, i2 : in bit; o1: out bit);
end bottom;

architecture arch of bottom is
begin
process (i1, i2)
  begin
  if (g1 = 10) then
    o1 <= i1 and i2;
  end if;
  end process;
end;

use work.mypack.all;
entity middle is
  generic ( g1 : myint := 7); 
  port (n1, n2 : in bit; t1: out bit);
end middle;

architecture arch of middle is
component bottom is
  generic ( g1 : myint := 7); 
  port (i1, i2 : in bit; o1: out bit);
end component;
begin
  bot_inst : bottom generic map(g1 => g1)
                    port map (i2 => n2, i1 => n1, o1 => t1);
end;

use work.mypack.all;
entity genericmap3 is
  port (in1, in2 : in bit; out1: out bit);
end ;

architecture arch of genericmap3 is
component middle is
  generic ( g1 : myint := 9); 
  port (n1, n2 : in bit; t1: out bit);
end component;
begin
  mid_inst : middle generic map(g1 => 15) 
                    port map (in1, in2, out1);
end;
