entity based_literal5 is
  port(in1 : in integer range 0 to 5; out1: out integer range 0 to 100);
end;

architecture arch of based_literal5 is
begin
  out1 <= in1 + integer(10#50#E-1);
end;

