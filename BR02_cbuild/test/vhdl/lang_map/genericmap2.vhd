entity middle is
  generic ( g1 : boolean := TRUE);
  port (n1, n2 : in bit; t1: out bit);
end ;

architecture arch of middle is
begin
  t1 <= n1 and n2;
end;

entity genericmap2 is
  port (in1, in2 : in bit; out1: out bit);
end ;

architecture arch of genericmap2 is
component middle is
  generic ( g1 : boolean := TRUE); 
  port (n1, n2 : in bit; t1: out bit);
end component;
begin
  mid_inst : middle generic map(g1 => 10) 
                    port map (in1, in2, out1);
end;
