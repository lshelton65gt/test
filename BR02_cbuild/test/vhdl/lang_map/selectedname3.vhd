library ieee;
use ieee.std_logic_1164.all;
package p1 is
 function func ( var1 : std_logic ) return std_logic;
end p1;
package body p1 is
 function func (var1 : std_logic) return std_logic is
   variable Result : std_logic := '0';
 begin
   Result := var1;
   return Result;
 end;
end p1;
library IEEE;
use IEEE.std_logic_1164.all;
entity selectedname3 is
   port( input : in std_logic;
         output : out std_logic);
end ;

library work;
use work.p1.all;
architecture arch of selectedname3 is
begin
   process(input)
   begin
       output <= work.p1.func(input);
   end process;
end ;
