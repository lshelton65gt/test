library ieee;
use ieee.std_logic_1164.all;

entity aggregate3 is
  generic( g: integer := 1);
  port (in1 : in std_logic;
        out1 : out std_logic_vector(0 to 1));
end;

architecture arch of aggregate3 is
begin
--  out1 <= (g => '1' , others => in1);
  out1 <= (1 => '1' , others => in1);
end;
