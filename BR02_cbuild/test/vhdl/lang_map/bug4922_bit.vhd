library ieee;
use ieee.std_logic_1164.all;
                                                                                
library ieee;
use ieee.std_logic_1164.all;

entity bug4922_bit is
  generic
  (
     C_KIND_OF_INTR : bit := '1';
     C_KIND_OF_EDGE : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
     C_KIND_OF_LVL  : std_logic_vector(31 downto 0) := "11111111111111111111111111111111"
   );
  port (
     sw_ints  : out std_logic_vector( 1 downto 0);
     reg_ints : in std_logic_vector( 1 downto 0)
);
end bug4922_bit;

architecture rtl of bug4922_bit is

begin

                                                                               
  tc_proc1:
  process(reg_ints)
  begin
    if C_KIND_OF_INTR = '1' then
      for i in reg_ints'range
      loop
        sw_ints(i) <= reg_ints(i) xnor C_KIND_OF_EDGE(i);
      end loop;
    else
      for i in reg_ints'range
      loop
        sw_ints(i) <= reg_ints(i) xnor C_KIND_OF_LVL(i);
      end loop;
    end if;
  end process tc_proc1;

end rtl;
