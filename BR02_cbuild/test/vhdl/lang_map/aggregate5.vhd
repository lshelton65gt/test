library ieee;
use ieee.std_logic_1164.all;

entity aggregate5 is
  port (in1 : in std_logic;
        out1 : out std_logic_vector(0 to 3));
end;

architecture arch of aggregate5 is
constant cnst : std_logic_vector(0 to 1) := ( others => '1');
begin
  out1 <= (1 => cnst(0), 0 => cnst(1), 2 => '0' , others => in1);
end;
