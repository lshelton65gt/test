package mypack is
type myint is range 5 to 10;
end mypack;

use work.mypack.all;
entity bottom is
  generic ( g1 : myint := 5); 
  port (i1, i2 : in bit; o1: out bit);
end bottom;

architecture arch of bottom is
begin
process (i1, i2)
  begin
  if (g1 = 10) then
    o1 <= i1 and i2;
  else
    o1 <= i1 xor i2;
  end if;
  end process;
end;

use work.mypack.all;
entity middle is
  generic ( g1 : myint := 7); 
  port (n1, n2 : in bit; t1: out bit);
end middle;

architecture arch of middle is
component bottom is
  generic ( g1 : myint := 7); 
  port (i1, i2 : in bit; o1: out bit);
end component;
begin
  bot_inst : bottom generic map(g1 => g1)
                    port map (i2 => n2, i1 => n1, o1 => t1);
end;

use work.mypack.all;
entity genericmap4 is
  port (in1, in2 : in bit; out1,out2: out bit);
end ;

architecture arch of genericmap4 is
component middle is
  generic ( g1 : myint := 10); 
  port (n1, n2 : in bit; t1: out bit);
end component;
begin
  ins1  : middle generic map(g1 => 8) 
                    port map (in1, in2, out1);
  inst2 : middle port map (in1, in2, out2);
end;
