entity based_literal2 is
  port(in1 : in integer; out1,out2: out integer);
end;

architecture arch of based_literal2 is
begin
  out1 <= in1 + 10#5#E1;
  out2 <= in1 + 2#110010#;
end;

