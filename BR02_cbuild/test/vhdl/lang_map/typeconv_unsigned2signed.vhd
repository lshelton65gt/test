library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity typeconv_unsigned2signed is
  port(in1,in2 : in UNSIGNED(7 downto 0);
       out1 : out SIGNED(7 downto 0));
end;

architecture arch of typeconv_unsigned2signed is
begin
  out1 <= SIGNED(in1) + SIGNED(in2);
end;

