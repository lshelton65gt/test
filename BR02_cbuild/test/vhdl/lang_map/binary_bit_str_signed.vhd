library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity binary_bit_str_signed is
  port(in1 : in SIGNED(2 downto 0);
       out1 : out SIGNED(2 downto 0));
end;

architecture arch of binary_bit_str_signed is
begin
  out1 <= in1 + "111"; -- decimal -3, expected out1 is in1 - 3
end;

