library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity qualified_expr_signed is
  port(in1 : in SIGNED(7 downto 0);
       out1 : out SIGNED(7 downto 0));
end;

architecture arch of qualified_expr_signed is
begin
  out1 <= in1 + SIGNED'('0','0','0','0','0','0','0','1');
end;

