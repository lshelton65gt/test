entity bottom is
  generic ( g1 : integer := 5); 
  port (i1, i2 : in bit_vector(0 to g1+1); o1: out bit_vector(0 to g1+1));
end bottom;

architecture arch of bottom is
begin
  forgen: for i in 0 to g1+1 generate
    o1(i) <= i1(i) and i2(i); 
  end generate;
end;

entity genericmap7 is
  port (in1, in2 : in bit_vector(0 to 2); out1: out bit_vector(0 to 2));
end ;

architecture arch of genericmap7 is
component bottom is
  generic ( g1 : integer := 7); 
  port (i1, i2 : in bit_vector(0 to g1+1); o1: out bit_vector(0 to g1+1));
end component;
begin
  bot_inst : bottom generic map(g1 => 1)
                    port map (in1, in2,out1);
end;
