entity leaf is
  generic (foo : integer := 5);
  port(in1, in2 : bit; out1 : out bit);
end;

architecture arch of leaf is
begin
process(in1,in2)
begin
  if (foo = 10#5#E1) then
    out1 <= in1 and in2;
  else
    out1 <= in1 xor in2;
  end if;
  end process;
end;

entity based_literal1 is
  port(in1,in2 : in bit_vector(0 to 1); out1 : out bit_vector(0 to 1));
end;

architecture arch of based_literal1 is
component leaf 
  generic (foo : integer := 5);
  port(in1, in2 : bit; out1 : out bit);
end component;
begin
  inst1 : leaf
          generic map( foo => 50) port map(in1(0),in2(0),out1(0));
  inst2 : leaf
          generic map( foo => 4#1_1#E1) port map(in1(1),in2(1),out1(1));
end;

