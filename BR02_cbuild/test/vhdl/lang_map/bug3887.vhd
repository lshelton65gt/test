library ieee;
use ieee.std_logic_1164.all;

entity bug3887 is
  port(in1, in2 : in std_logic_vector(10 downto 0); 
       out1 : out std_logic_vector(10 downto 0));
end;

architecture arch of bug3887 is
type mant11type is array (0 to 3) of std_logic_vector(10 downto 0);
signal mant11      : mant11type;
begin
  mant11(0) <= in1;
  mant11(1) <= in2;
  mant11(2) <= (mant11(2)'right => in1(0), others => in1(1));
  mant11(3) <= (mant11(3)'left => in2(0), others => '0');
  out1 <= (mant11(0) and mant11(1) ) or (mant11(2) or mant11(3));
end;
