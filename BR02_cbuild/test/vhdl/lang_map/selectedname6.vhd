entity foo is
  constant c : integer := 5;
end;

library IEEE;
use IEEE.std_logic_1164.all;
entity selectedname6 is
    port( input : in std_logic;
          output : out integer
        );
end ;

library work;
use work.foo;
architecture arch of selectedname6 is
begin
    P1 : process(input)
    variable v : integer;
    begin
        P1.v := 1;
        output <= P1.v + work.foo.c;
    end process;
end ;
