library ieee;
use ieee.std_logic_1164.all;
entity selectedname2 is
   port( input : in std_logic_vector(1 downto 0);
         output : out std_logic_vector(1 downto 0)
       );
   constant c : integer := 10;
end ;

library work;
use work.selectedname2;

architecture arch of selectedname2 is
begin
   process(input)
   begin
       if (work.selectedname2.c = 10) then
          output <= input;
       end if;
   end process;
end ;
