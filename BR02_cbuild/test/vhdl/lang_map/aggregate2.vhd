library ieee;
use ieee.std_logic_1164.all;

entity aggregate2 is
  port (in1 : in integer range 0 to 1; out1 : out std_logic_vector(0 to 1));
end;

architecture arch of aggregate2 is
begin
  out1 <= (in1 => '1' , others => '0');
end;
