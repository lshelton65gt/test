library IEEE;
use IEEE.std_logic_1164.all;
entity selectedname5 is
    port( input : in std_logic;
          output : out integer
        );
end ;

library work;
use work.selectedname5;
architecture arch of selectedname5 is
begin
    P1 : process(input)
    variable v : integer;
    begin
        P1.v := 1;
        output <= P1.v;
    end process;
end ;
