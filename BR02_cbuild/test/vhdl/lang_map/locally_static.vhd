entity locally_static is
    port(in1 :bit;
        flag : boolean;
        output : out boolean);
end;

architecture arch of locally_static is
begin
    process(flag, in1)
    variable pnat1 : bit;
    begin
      pnat1 := '1'; 
      if flag then
        output <= pnat1 = '0' ;
      end if;
    end process ;
end;

