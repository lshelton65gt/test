library ieee;
use ieee.std_logic_1164.all;

entity aggregate1 is
  port (in1 : in std_logic; out1 : out std_logic_vector(0 to 3));
end;

architecture arch of aggregate1 is
begin
  out1 <= (0 to 1 => '1' , others => in1);
--  out1(1) <= '1';
--  out1(0) <= in1;
end;
