library ieee;
use ieee.std_logic_1164.all;
                                                                                
package tc_pkg is

  subtype QUADLET_TYPE is std_logic_vector(0 to 31);
  subtype data_word_type is quadlet_type;
  constant WORD_SIZE : positive := data_word_type'high + 1;

end package tc_pkg;

library ieee;
use ieee.std_logic_1164.all;
use work.tc_pkg.all;

entity bug4922 is
  generic
  (
     REG_WIDTH : positive range 1 to WORD_SIZE := 2;
     C_KIND_OF_INTR : std_logic_vector(WORD_SIZE - 1 downto 0) := "11111111111111111111111111111111";
     C_KIND_OF_EDGE : std_logic_vector(WORD_SIZE - 1 downto 0) := "11111111111111111111111111111111";
     C_KIND_OF_LVL  : std_logic_vector(WORD_SIZE - 1 downto 0) := "11111111111111111111111111111111"
   );
  port (
     sw_ints  : out std_logic_vector(REG_WIDTH - 1 downto 0);
     reg_ints : in std_logic_vector(REG_WIDTH - 1 downto 0)
);
end bug4922;

architecture rtl of bug4922 is

begin

                                                                               
  tc_proc1:
  process(reg_ints)
  begin
    for i in reg_ints'range
    loop
      if C_KIND_OF_INTR(i) = '1' then
        sw_ints(i) <= reg_ints(i) xnor C_KIND_OF_EDGE(i);
      else
        sw_ints(i) <= reg_ints(i) xnor C_KIND_OF_LVL(i);
      end if;
    end loop;
  end process tc_proc1;

end rtl;
