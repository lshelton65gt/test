library IEEE;
use IEEE.std_logic_1164.all;
entity selectedname4 is
    port( input : in std_logic;
          output : out integer
        );
end ;

library work;
use work.selectedname4;
architecture arch of selectedname4 is
constant c : integer := 16;
begin
    process(input)
    begin
        output <= arch.c;
    end process;
end ;
