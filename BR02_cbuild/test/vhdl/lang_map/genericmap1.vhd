
entity bottom is
  generic ( g1 : integer := 5); 
  port (i1, i2 : in bit; o1: out bit);
end bottom;

architecture arch of bottom is
begin
process (i1, i2)
  begin
  if (g1 = 10) then
    o1 <= i1 and i2;
  end if;
  end process;
end;

entity middle is
  generic ( g1 : integer := 7); 
  port (n1, n2 : in bit; t1: out bit);
end middle;

architecture arch of middle is
component bottom is
  generic ( g1 : integer := 7); 
  port (i1, i2 : in bit; o1: out bit);
end component;
begin
  bot_inst : bottom generic map(g1 => g1)
                    port map (i2 => n2, i1 => n1, o1 => t1);
end;

entity genericmap1 is
  port (in1, in2 : in bit; out1: out bit);
end ;

architecture arch of genericmap1 is
component middle is
  generic ( g1 : integer := 9); 
  port (n1, n2 : in bit; t1: out bit);
end component;
begin
  mid_inst : middle generic map(g1 => 10) 
                    port map (in1, in2, out1);
end;
