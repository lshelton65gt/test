entity bottom is
end bottom;

architecture arch of bottom is
begin
end;

entity emptyportmap is
  port (in1: in bit; out1: out bit);
end emptyportmap;

architecture arch of emptyportmap is
component bottom is
end component;
begin
  bot_inst : bottom; 
  out1 <= in1;
end;
