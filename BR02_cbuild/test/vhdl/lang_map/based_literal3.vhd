entity based_literal3 is
  port(in1 : in integer range 0 to 5; out1,out2: out integer range 0 to 100);
end;

architecture arch of based_literal3 is
begin
  out1 <= in1 + 10#5.9#;
  out2 <= in1 + 2#110010#;
end;

