entity based_literal4 is
  port(in1 : in integer; out1: out integer);
end;

architecture arch of based_literal4 is
begin
  out1 <= in1 + integer(10#5.1#);
end;

