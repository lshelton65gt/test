package pack1 is
  constant c1 : integer := 2;
  constant c2 : integer := 3;
end pack1;
package pack2 is
  signal c1 : integer := 1;
end pack2;
package pack3 is
  shared variable c1 : integer := 3;
end pack3;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;
entity selectedname1 is
  port (in1 : in std_logic_vector(7 downto 0);
        out1: out std_logic_vector(15 downto 0));
end ;
architecture arch of selectedname1 is
begin
  svar: process (work.pack2.c1)
  begin
    work.pack3.c1 := work.pack2.c1 - 2;   
  end process svar;


   work.pack2.c1 <= conv_integer(signed(in1)) + 1;
   selectedname1.out1 <= conv_std_logic_vector( work.pack1.c1 + work.pack1.c2 + work.pack2.c1 + work.pack3.c1 + conv_integer( signed( in1 )), 16);

end ;
