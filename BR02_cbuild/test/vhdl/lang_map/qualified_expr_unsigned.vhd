library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity qualified_expr_unsigned is
  port(in1 : in UNSIGNED(7 downto 0);
       out1 : out UNSIGNED(7 downto 0));
end;

architecture arch of qualified_expr_unsigned is
begin
  out1 <= in1 + UNSIGNED'('0','0','0','0','0','0','0','1');
end;

