library ieee;
use ieee.std_logic_1164.all;
                                                                                
library ieee;
use ieee.std_logic_1164.all;

entity bug4922_bv is
  generic
  (
     C_KIND_OF_INTR : bit_vector(31 downto 0) := "11111111111111111111111111111111";
     C_KIND_OF_EDGE : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
     C_KIND_OF_LVL  : std_logic_vector(31 downto 0) := "11111111111111111111111111111111"
   );
  port (
     sw_ints  : out std_logic_vector( 1 downto 0);
     reg_ints : in std_logic_vector( 1 downto 0)
);
end bug4922_bv;

architecture rtl of bug4922_bv is

begin

                                                                               
  tc_proc1:
  process(reg_ints)
  begin
    for i in reg_ints'range
    loop
      if C_KIND_OF_INTR(i) = '1' then
        sw_ints(i) <= reg_ints(i) xnor C_KIND_OF_EDGE(i);
      else
        sw_ints(i) <= reg_ints(i) xnor C_KIND_OF_LVL(i);
      end if;
    end loop;
  end process tc_proc1;

end rtl;
