library ieee;
use ieee.std_logic_1164.all;
entity z_drive is
  port (in1, ena1, in2, ena2: in std_logic; out1: out std_logic);
end;

architecture arch of z_drive is
begin
  process(in1,in2,ena1,ena2)
  begin
    if (ena1 = '1') then
      out1 <= in1;
    elsif (ena2 = '1') then
      out1 <= in2;
    else
      out1 <= 'Z';
    end if;
  end process;
end;

