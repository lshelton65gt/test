-- test the ability to fold constant expressions during population and
-- to use nucleus data to evaluate certain predefined attributes
library IEEE;
use IEEE.std_logic_1164.all;

entity bug3961_3 is 
  port (
    spmskxinc : out std_logic_vector(65 downto 0);
    dlty      : in  std_logic_vector(59 downto 0)
    );
end bug3961_3;

architecture functional of bug3961_3 is
  constant E20  : integer := 2;
  constant E01  : integer := 0;
  function sgnxtnd (sx : std_logic_vector; s : natural
                    ) return std_logic_vector is
    variable y : std_logic_vector(sx'high-sx'low+s downto 0);
    variable j : natural;
  begin
    for i in y'high downto y'high-s+1 loop
      y(i) := sx(sx'high);
    end loop;
    j := sx'high+1;
    for i in y'high-s downto y'low loop
      j := j-1;
      y(i) := sx(j);
    end loop;
    return y;
  end sgnxtnd;

  function zeropad (zx : std_logic_vector; z : natural
                    ) return std_logic_vector is
    variable y : std_logic_vector(zx'left-zx'right+z downto 0);
    variable j : natural;
  begin
    j := zx'left+1;
    for i in y'left downto z loop
      j := j-1;
      y(i) := zx(j);
    end loop;
    for i in z-1 downto 0 loop
      y(i) := '0';
    end loop;
    return y;
  end zeropad;

  function extndvctr (ex : std_logic_vector;
                      s : natural;
                      z : natural
                      ) return std_logic_vector is
  begin
    -- we can't figure out how wide the first formal argument to sgnxtnd is
    -- here.  It gets populated as a scalar. 
    return sgnxtnd(zeropad(ex, z), s);
  end extndvctr;
begin
  process (dlty)
  begin
    for e in E01 to E20 loop
      spmskxinc(21+e*22 downto e*22) <= extndvctr(dlty(19+e*20 downto e*20), 1, 1);
    end loop;
  end process;
end functional;
