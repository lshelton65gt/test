library ieee;
use ieee.std_logic_1164.all;

entity std_logic_vector_1 is
  port (clk: in std_logic; d : in std_logic_vector(2 downto 0); q: out std_logic_vector(2 downto 0));
end;

architecture arch of std_logic_vector_1 is
begin
  process (clk)
  begin
	if (clk = '1' and clk'event) then
      q <= d;
	end if;
  end process;
end;
