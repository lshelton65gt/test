-- filename: test/vhdl/lang_types/type_conv1.vhd
-- Description:  This test checks that type conversion of user defined types works


library ieee;
use ieee.std_logic_1164.all;

entity type_conv1 is
  port (
    clock    : in  std_logic;
    in1 : in std_logic;
    out1     : out std_logic);
end;

architecture arch of type_conv1 is
  type T1 is array (0 to 3) of std_logic ;
  type T2 is array (3 downto 0) of std_logic ;


begin

  main: process (clock)
  variable s1 : T1;
  variable s2 : T2 := ('1','0','1','0');
  begin
    if clock'event and clock = '1' then 
     if in1 = '1' then 
       s1 := T1(s2) ;                     --note that range is reversed for s1 and s2, so that s1(n) is never equal to s2(n)
     else
       s1 := (others => '0') ;
     end if;
     if ( (s1(0) = s2(0)) or (s1(1) = s2(1)) or (s1(2) = s2(2)) or (s1(3) = s2(3)) ) then
       out1 <= '1';                     -- thus this should never be executed if in1 is asserted
     else
       out1 <= '0';
     end if;
    end if;
  end process;
end;


