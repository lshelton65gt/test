library ieee;
use ieee.std_logic_1164.all;

package tmp is
  subtype regFileAddr is std_logic_vector(8 downto 0);
  subtype regFileAddrInc is regFileAddr;
end tmp;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.tmp.all;

entity bug4737 is port (
  IR : in std_logic_vector(57 downto 0);
  outp : out std_logic_vector(8 downto 0));
end bug4737;

architecture top of bug4737 is
  alias IR_inc1 : regFileAddrInc is IR(35 downto 27);
  signal xrdptr0inc : regFileAddrInc;
begin
  xrdptr0inc <= CONV_STD_LOGIC_VECTOR(345, regFileAddrInc'LENGTH);
  outp <= xrdptr0inc;
end top;
