-- This test is for bug6108.  It was extracted and simplified from Broadcom WLAN
-- bcm4318.  The problem was in the calls to rdptrinc; we were not handling subtypes
-- correctly when passing an unconstrained argument to a function.  Unconstrained types
-- worked; unconstrained subtypes did not.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity pprocAGen is
  port (rdcmd0in      : in     bit_vector(2 downto 0);
        opaddrctlin   : in     bit_vector(3 downto 0);
	lastOp1Inc    : in     std_logic;
        lastOp2Inc    : in     std_logic;
        xrd_out       : out    std_logic_vector(8 downto 0)
        );
end pprocagen;


architecture synth of pprocAGen is
  type opAddrCtl is (immInc0, immInc1, indP0, indP1);
  type aGenRdCmd is (rdOp1, rdOp2, rdOp2I, rdOp2Q, nop);
  subtype regFileAddrXtn is std_logic_vector(8 downto 0);
  subtype regFileAddr    is std_logic_vector(8 downto 0);
  -- This was the troublemaker
  subtype regFileAddrInc is regFileAddr;

  type regPtr is record
    addrX       : regFileAddrXtn;
    addrWrapped : std_logic;
    addr        : regFileAddr;
    inc         : regFileAddr;
  end record;
  constant op1 : integer := 0;
  constant op2 : integer := 1;
  constant p0  : integer := 2;
  constant p1  : integer := 3;
  subtype rdPtrIndex is NATURAL range op1 to p1;
  type rdPtrArray is array(rdPtrIndex) of regPtr;

  function rdPtrInc(rdCmd: aGenRdCmd; ctl: opAddrCtl; lastInc: std_logic;
		    rdPtr: rdPtrArray)
    return regFileAddrInc is
    variable inc : regFileAddrInc;
  begin
    if rdCmd = rdOp2I then
      inc := CONV_STD_LOGIC_VECTOR(1, regFileAddrInc'LENGTH);
    else
      case ctl is
	when immInc0 =>
	  inc := CONV_STD_LOGIC_VECTOR(0, regFileAddrInc'LENGTH);
	when immInc1 =>
	  if rdCmd = rdOp2Q then
	    inc := CONV_STD_LOGIC_VECTOR(2, regFileAddrInc'LENGTH);
	  else
	    inc := CONV_STD_LOGIC_VECTOR(1, regFileAddrInc'LENGTH);
	  end if;
	when indP0 | indP1 =>
	  if lastInc = '1' then
	    if rdCmd = rdOp1 then
	      inc := rdPtr(op1).addr; -- loaded from ROM, not incremented
	    else
	      inc := rdPtr(op2).addr; -- ditto
	    end if;
	  else
	    if ctl = indP0 then
	      inc := rdPtr(p0).inc;
	    else
	      inc := rdPtr(p1).inc;
	    end if;
	  end if;
      end case;
    end if;
    if rdCmd = rdOp2Q then
      inc := inc - 1;
    end if;
    return inc;
  end rdPtrInc;

  signal op1ACtl     : opAddrCtl;
  signal op2ACtl     : opAddrCtl;
  signal rdptr       : rdPtrArray;
  signal xrdptr0Inc  : regFileAddrInc;
  signal rdcmd0      : aGenRdCmd;

begin
  -- convert bit_vector to enum
  with opaddrctlin(1 downto 0) select
    op1ACtl <=
    immInc0 when "00",
    immInc1 when "01",
    indP0   when "10",
    indP1   when "11";
  
  -- convert bit_vector to enum
  with opaddrctlin(3 downto 2) select
    op2ACtl <=
    immInc0 when "00",
    immInc1 when "01",
    indP0   when "10",
    indP1   when "11";
  
  -- convert bit_vector to enum
  with rdcmd0in select
    rdcmd0 <=
    rdop1  when "000",
    rdop2  when "001",
    rdop2i when "010",
    rdop2q when "011",
    nop    when others;

  -- drive the output
  xrd_out <= xrdptr0inc;
  
  with rdcmd0 select
    xrdptr0Inc <=
      CONV_STD_LOGIC_VECTOR(0, regFileAddrInc'LENGTH) when nop,
      rdptrInc(rdcmd0, op1ACtl, lastOp1Inc, rdptr)     when rdOp1,
      rdPtrInc(rdcmd0, op2ACtl, lastOp2Inc, rdptr)     when rdOp2 | rdOp2I | rdOp2Q;
end synth;
