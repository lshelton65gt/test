--This test is a modification of type_conv6 in this directory.  It uses a
--subtype of a user integer type.
package CONV_PACK_type_conv7 is
  type int2 is range 0 to 40000;
  subtype int1 is int2 range 0 to 20000;
end CONV_PACK_type_conv7;

use work.CONV_PACK_type_conv7.all;

entity type_conv7 is
  port ( in1 : integer;
         out1 : out integer;
         out2 : out integer);
end type_conv7;

architecture type_conv7 of type_conv7 is

  signal var1 : int1; 
  signal int : integer;
begin
  process(in1,var1)

  begin

    var1 <= (10 + int1(in1 mod 19991));
    out1 <= integer(var1);
    out2 <= integer(var1) + in1;

  end process;

end type_conv7;

