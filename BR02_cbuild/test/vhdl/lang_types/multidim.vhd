-- This doesn't generate output; we should check that it gets populated
-- correctly, though. 
library ieee;
use ieee.std_logic_1164.all;

entity multidim is
end multidim;

architecture arch of multidim is
  -- one dimensional constrained user-defined type
  type onedarray is array (1 to 10) of std_logic;
  -- one dimensional unconstrained user-defined type
  type onedarray_un is array (integer range <>) of std_logic;
  -- 2-D constrained array
  type twodarray is array (11 to 20, 21 to 30) of std_logic;
  -- 3-D constrained array
  type threedarray is array (31 to 40, 41 to 50, 51 to 60) of std_logic;
  -- 3-D unconstrained array
  type threedarray_un is array (natural range <>,
                                natural range <>,
                                natural range <>) of std_logic;

  -- subtype constraining the unconstrained type 'std_logic_vector'
  subtype word is std_logic_vector (31 downto 0);
  -- unconstrained array of 'word'
  type memory is array (natural range <>) of word;
  -- array of type memory; note that memory's size must be constrained
  type bankedmemory is array (positive range <>) of memory(1023 downto 0);

  type nasty is array (natural range <>) of threedarray_un(1 to 2, 1 to 2, 1 to 2);
  type indexingthisishard is array (natural range<>, natural range <>) of nasty(0 to 7);

  signal s1 : onedarray;
  signal s2 : onedarray_un(0 to 10000000);  -- must have constraint supplied
  signal s3 : twodarray;
  signal s4 : threedarray;
  -- this is the same size and bounds as s4 but not the same type
  signal s5 : threedarray_un(31 to 40, 41 to 50, 51 to 60);
  -- same size as s4 and s5 but different bounds
  signal s6 : threedarray_un(1 to 10, 1 to 10, 1 to 10);
  -- different size than s4, s5, s6
  signal s7 : threedarray_un(7 downto 2, 1 to 2, 437 downto 181);
  signal s8 : word;                     -- 32 bits
  signal s9 : memory(1023 downto 0);    -- the size of a bank in type bankedmemory
  signal s10 : bankedmemory(1 to 4);    -- 4 x 1024 x 32 3-d array of std_logic
  signal s11 : indexingthisishard(20 downto 5, 6 downto 4);
  
  
begin
  -- one bit extracted from s11, using the left bound of each range
  s11(20, 6)(0)(1, 1, 1) <= '0';
  -- one bit extracted from s11, using the right bound of each range
  s11(5, 4)(7)(2, 2, 2) <= '1';
  -- the LHS and RHS are of type threedarray_un(1 to 2, 1 to 2, 1 to 2)
  s11(5, 4)(7) <= s11(6,5)(3);

end arch;
