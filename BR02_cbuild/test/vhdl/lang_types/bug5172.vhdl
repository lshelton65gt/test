library std;
use std.textio.all;
-- changed name from timenow to bug5172 to simplifiy running from runlist
entity bug5172 is
  port (
    clk : in  bit;
    i0  : in  bit_vector (31 downto 0);
    i1  : in  bit_vector (31 downto 0);
    sel : in  bit;
    o   : out bit_vector (31 downto 0));
end;

architecture rtl of bug5172 is
  file outf : text open write_mode is "sim.log";
begin  -- rtl

  p0: process (clk)
    variable l0 : line;
    variable t : time;
  begin
    if clk'event and clk = '1' then
      t := now;
      if sel='0' then
        write(l0, string'("T="));
        write(l0, t);
        write(l0, string'(",S=0"));
        writeline(outf, l0);
        o <= i0;
      else
        write(l0, string'("T="));
        write(l0, t);
        write(l0, string'(",S=1"));
        writeline(outf, l0);
        o <= i1;
      end if;
    end if;
  end process p0;
  
end rtl;
