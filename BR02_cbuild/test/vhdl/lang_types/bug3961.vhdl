-- test the ability to fold constant expressions during population and
-- to use nucleus data to evaluate certain predefined attributes
library IEEE;
use IEEE.std_logic_1164.all;

entity bug3961 is 
    port (
	z	: out std_logic_vector(65 downto 0);
	a	: in  std_logic_vector(59 downto 0)
	);
end bug3961;

architecture functional of bug3961 is
  constant E20  : integer := 2;
  constant E01  : integer := 0;
    function sgnxtnd (x : std_logic_vector; s : natural
                      ) return std_logic_vector is
        variable y : std_logic_vector(x'left-x'right+s downto 0);
        variable j : natural;
    begin  -- sgnxtnd
        for i in y'left downto y'left-s+1 loop
            y(i) := x(x'left);
        end loop;
        j := x'left+1;
        for i in y'left-s downto 0 loop
            j := j-1;
            y(i) := x(j);
        end loop;
        return y;
    end sgnxtnd;
begin

    process (a)
    begin
      for e in E01 to E20 loop
	z(21+e*22 downto e*22) <= sgnxtnd(a(19+e*20 downto e*20), 2);
      end loop;
    end process;

end functional;


