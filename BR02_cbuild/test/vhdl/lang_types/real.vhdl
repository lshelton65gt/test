-- declare reals in any way I can think of.  We won't process with them, but we'll
-- declare them.
package pack is
  signal s : real;
  shared variable v : real;
end pack;

entity real is
  generic (
    g1 : real := 1.1);
  port (
    p1 : in  real);
end;

architecture a of real is
  subtype rsub is real range 2.71828 to 3.14159;
  constant c1 : rsub := 3.11;
  constant c2 : real := c1;
  signal s1 : real := 2.1;
  signal s2 : rsub;
  signal s3 : rsub range 3.0 to 3.1;
  signal s4 : rsub := c1;
  signal i1 : integer;
  type rec is
    record
      r1 : real;
      r2 : rsub;
    end record;
  signal rec1 : rec := (1.1, 3.14158);
  
  function func (i1 : integer) return integer is
    variable r2 : real;
  begin
    return 0;
  end func;
begin

  i1 <= func(7) after c1 * 1 ns;

  real1: process (p1, s1)
    variable v1 : real range 1.0 to 2.9;
  begin
    
  end process real1;

end a;
