entity bit_vector1 is
  port (clk: in bit; d : in bit_vector(2 downto 0); q: out bit_vector(2 downto 0));
end;

architecture arch of bit_vector1 is
begin
  process (clk)
  begin
	if (clk = '1' and clk'event) then
      q <= d;
	end if;
  end process;
end;
