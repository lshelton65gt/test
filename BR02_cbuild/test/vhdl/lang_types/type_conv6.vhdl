--This test is a modification of beacon9/expression.type_conv6.  Without the
--mod statement in place, it couldn't create a gold simulation run in NC.  With
--the mod in place, NC and aldec agree with cbuild.
  package CONV_PACK_type_conv6 is
    type int1 is range 0 to 20000;
  end CONV_PACK_type_conv6;

use work.CONV_PACK_type_conv6.all;

entity type_conv6 is
  port ( in1 : integer;
         out1 : out integer;
         out2 : out integer);
end type_conv6;

architecture type_conv6 of type_conv6 is

  signal var1 : int1; 
  signal int : integer;
begin
  process(in1,var1)

  begin

    var1 <= (10 + int1(in1 mod 19991));
    out1 <= integer(var1);
    out2 <= integer(var1) + in1;

  end process;

end type_conv6;

