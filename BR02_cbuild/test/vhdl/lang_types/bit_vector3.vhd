-- Testing slice-index normalization

entity bit_vector3 is
  port (in1 : in bit_vector(1 to 4);
        out1: out bit_vector(1 downto 0);
        out2: out bit_vector(1 downto 0);
        out3: out bit_vector(1 downto 0));
end;

architecture bit_vector3 of bit_vector3 is
begin
  out1 <= in1(3 to 4)  ;
  out2 <= in1(1 to 2)  ;
  out3 <= in1(2 to 3)  ;
end;
