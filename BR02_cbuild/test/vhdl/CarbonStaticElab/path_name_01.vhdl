-- filename: ../test/vhdl/CarbonStaticElab/path_name_01.vhdl
-- Description:  This test duplicated a problem seen where Verific StaticElab
-- would crash if the _path_name array was not properly setup properly.


library ieee;
use ieee.std_logic_1164.all;




entity mid is
  port (
    in1, in2      : in  integer;
    out1     : out std_logic_vector(3 downto 0)
    );
end;

architecture a_mid of mid is 
 
procedure bottom (
  win1  : in  integer;
  wout1 : out std_logic) is
begin  -- bottom
  if win1 > 50 then
    wout1 := '1';
  else
    wout1 := '0';
  end if;
end bottom;

begin 

 g_mid: if 3 /= 4 generate
  
  l_mid: process (in1, in2)
      variable out1_v : std_logic_vector (3 downto 0);

  begin
   if in2 > 0 then 
     if in2 < in1 then
       for ii in 3 downto 0 loop
         bottom(in1+ii, out1_v(ii));
       end loop;  -- ii
     end if;
    end if;
     out1 <= out1_v;
  end process;
end generate g_mid;
end;


library ieee;
use ieee.std_logic_1164.all;

entity path_name_01 is

  port (
    in1, in2 : in  integer;
    out1     : out std_logic_vector(3 downto 0));
end;

architecture a_path_name_01 of path_name_01 is
 component mid
   port (
     in1,in2  : in  integer;
     out1 : out std_logic_vector(3 downto 0));
 end component;
 
begin 
 i_mid : mid
   port map (
     in1  => in1,
     in2  => in2,
     out1 => out1);
end;
