-- This tests case statement pruning with 'range' choices
-- It shows that the VhdlCSEVisitor and the VhdlDeadBranchRewriter
-- don't handle case statements with ranges correctly.
entity case_range is
generic (
      N : natural := 4
      );
port( op : in bit_vector(2 downto 0);
       a : in bit_vector(N-1 downto 0);
       b : in bit_vector((N/2)-1 downto 0);
      o1 : out bit_vector(N-1 downto 0);
      o2 : out bit_vector((N/2)-1 downto 0)
     );
end case_range;

architecture rtl of case_range is

-- Perform operation based on op code
function perform_op(op : integer range 7 downto 0; v : bit_vector) return bit_vector is
      variable result : bit_vector(v'range);
   begin
      -- The case statement has to have a constant selector
      -- and the choices have to use ranges
      case op is
      when 7 downto 6 | 1 downto 0 => return not v;
      when 5 downto 4 | 3  => return v;
      when others => 
         result := (others => '0');
         return result;
      end case;
   end;


begin

process (op, a, b) is
  variable selector : integer range 7 downto 0;
begin
   if (op = "000") then
     o1 <= perform_op(0, a);
     o2 <= perform_op(0, b);
   elsif (op = "001") then
     o1 <= perform_op(1, a);
     o2 <= perform_op(1, b);
   elsif (op = "010") then
     o1 <= perform_op(2, a);
     o2 <= perform_op(2, b);
   elsif (op = "011") then
     o1 <= perform_op(3, a);
     o2 <= perform_op(3, b);
   elsif (op = "100") then
     o1 <= perform_op(4, a);
     o2 <= perform_op(4, b);
   elsif (op = "101") then
     o1 <= perform_op(5, a);
     o2 <= perform_op(5, b);
   elsif (op = "110") then
     o1 <= perform_op(6, a);
     o2 <= perform_op(6, b);
   else
     o1 <= perform_op(7, a);
     o2 <= perform_op(7, b);
   end if;
end process;

end architecture;

