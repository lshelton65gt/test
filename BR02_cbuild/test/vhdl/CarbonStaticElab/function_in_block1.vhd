-- Declare a function in a block, and in nested blocks
--
library ieee;
use ieee.std_logic_1164.all;
entity function_in_block1 is
port( a : in std_logic_vector(15 downto 0);
      b : in std_logic_vector(15 downto 0);
      c : out std_logic_vector(31 downto 0);
      d : out std_logic_vector(7 downto 0)
     );
end function_in_block1;

architecture rtl of function_in_block1 is
begin

block1: block is

begin -- block1

block1a: block is


function swap_and(a,b : std_logic_vector) return std_logic_vector is
begin
   return b & a;
end;

begin -- block1a

     c <= swap_and(a,b);

end block ; -- 1a
end block ; -- 1

block2: block is

function swap_and(a,b : std_logic_vector) return std_logic_vector is
constant width : integer := a'length;
variable result : std_logic_vector((width/2)-1 downto 0);
begin
   result := a((width/2)-1 downto 0) and b((width/2)-1 downto 0);
   return result;
end;

begin

   d <= swap_and(a,b);

end block;

end architecture;

