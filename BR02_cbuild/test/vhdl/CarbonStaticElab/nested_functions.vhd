-- This test case has function declarations nested inside of
-- others. It exposed a number of issues with CarbonStaticElaboration
-- function uniquification.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
package funcpack is
   function prio(vec : std_logic_vector) return std_logic_vector;
end funcpack;

package body funcpack is

-- this function has a nested function declared inside
function prio(vec : std_logic_vector) return std_logic_vector is 
    constant NUM_BITS : integer := vec'length;

    -- This function finds the highest '1' bit in the bit vector
    function findpos(vec : std_logic_vector) return integer;
    function findpos(vec : std_logic_vector) return integer is
        variable result : integer := 0;
        variable found : boolean := false;
    begin
        for i in vec'range loop
          if (not found) then
             if (vec(i) = '1') then
                found := true;
                result := i;
             end if;
          end if;
        end loop;
        return result;
    end function findpos;

    -- This nested function sets a bit in a bit vector.
    -- This is the one with the problematic signature.
    function setbit(pos : integer) return std_logic_vector;
    function setbit(pos : integer) return std_logic_vector is
        variable result : std_logic_vector(NUM_BITS-1 downto 0);
    begin
        result := (others => '0');
        result(pos) := '1';
        return result;
    end function setbit;
begin
    return setbit(findpos(vec));
end function prio;

end funcpack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
use work.funcpack.all;

entity nested_functions is
port( a : in std_logic_vector(15 downto 0);
      b : in std_logic_vector(7 downto 0);
      c : out std_logic_vector(15 downto 0);
      d : out std_logic_vector(7 downto 0)
     );
end nested_functions;

architecture rtl of nested_functions is


begin

 c <= prio(a);
 d <= prio(b);

end architecture;




