-- This testcase INVALIDATES a variable in
-- an if.
--
-- The CarbonStaticElaborator used to get this wrong. It
-- didn't invlidate the value of 'temp' after the 'if'
-- statement.
--
-- Note that the Interra flow populates and simulates
-- this incorrectly. It mistakenly thinks the loop
-- range is 0 to 0.

library ieee;
use ieee.std_logic_1164.all;
entity dataflow2b is
port (
        din   : in integer;
        din2  : in std_logic;
        dout  : out integer;
        dout2 : out std_logic
);
end dataflow2b;

architecture rtl of dataflow2b is

begin

   dout2 <= din2;
     
   process (din)
     -- NOTE: These vars are initialized at start of sim, but
     -- not everytime 'din' changes.
     variable temp : integer := 0;
     variable acc : integer := 0;
     variable din_temp : integer;
   begin
     -- Restrict the size of 'din' so the test doesn't take
     -- forever to run.
     din_temp := din mod 15;
     temp := 0;
     -- The dataflow value for 'temp' should be 0 here
     if acc > 0 then
        temp := din_temp;
     elsif acc = 0 then
        temp := 2;
     else
        temp := din_temp-1;
     end if; 
     -- The dataflow value for 'temp' should be UNKNOWN here.
     -- The loop cannot be unrolled.
     for j in 0 to temp loop
         acc := acc + j;
     end loop;
     dout <= acc;
   end process;

end architecture rtl;
