-- Expose another dataflow bug that will arise inside
-- loops that can't be unrolled. 
--
-- In this testcase, variable 'foobar' is
-- known to have value '3' inside the loop.

entity dataflow9 is
	port (
		in1 : in bit_vector(7 downto 0);
		count : out integer;
		error : out boolean
	);
end;

architecture rtl of dataflow9 is

function increment (b : bit_vector; v : integer) return integer is
  variable result : integer;
begin
   result := 0;
   for j in b'range loop
     if (b(1) = '1') then
        result := result + 1;
     end if;
   end loop;
   return v + result;
end;

begin
	process(in1)
		variable tcount : integer ;
		variable seen_first_zero, seen_last_zero : boolean ;
                variable foobar : integer;
	begin
		error <= false;
                seen_first_zero := false;
                seen_last_zero := false;
                tcount := 0;
                foobar := 3;
                -- 'foobar' has value 3 in the parent dataflow, which is
                -- not invalidated before entering the loop, because no assignment 
                -- to 'foobar' takes place inside the loop.
		for i in 7 downto 0
		loop
			if (seen_last_zero and in1(i) = '0') then
				tcount := 0;
				error <= true;
				exit;
			elsif (seen_first_zero and in1(i) = '1') then
				seen_last_zero := true;
			elsif (in1(i) = '0') then
				seen_first_zero := true;
				tcount := tcount + 1;
			end if;
                        -- We should be smart enough to know that
                        -- foobar is a constant 3 here.
                        -- If we are NOT, then the constraint elaborator
                        -- will complain that it can figure out the size
                        -- of the argument to 'increment'.
                        if (foobar < 3) then
                           tcount := increment(in1(foobar downto 0), tcount);
                        end if;
		end loop;
                for j in 0 to foobar loop
                  tcount := tcount + 1;
                end loop;
		count <= tcount;
        end process;
end;
							

