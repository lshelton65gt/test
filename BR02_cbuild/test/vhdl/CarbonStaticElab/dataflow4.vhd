-- All branches assign the same value to a dataflow variable
--
-- Interra flow: Works.
-- Verific flow (w/o CSE): Error: cannot determine size of 'invert' function argument
-- Verific flow (w CSE): Error: cannot determine size of 'invert' function argument

library ieee;
use ieee.std_logic_1164.all;
entity dataflow4 is
port (  
        din         : in std_logic_vector(31 downto 0);
        sel_input   : in std_logic;
        dout        : out std_logic_vector(31 downto 0);
        sel_out     : out std_logic_vector(1 downto 0)
);
end dataflow4;

architecture rtl of dataflow4 is

function invert (din: std_logic_vector) return std_logic_vector is
begin
  return not din;
end;

begin

   process (din, sel_input)
     variable size : integer := 1;
   begin
     size := 0;
     -- The dataflow value for 'size' should be 0 here
     if (sel_input = '1') then
        size := 1;
        dout <= not din;
     else
        size := 1;
        dout <= din;
     end if;
     -- The dataflow value for 'size' should be 1 here
     sel_out <= invert(din(size downto 0));

   end process;

end architecture rtl;
