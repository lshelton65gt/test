-- Test using an overloaded operator in a range
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package pkg is

  type bit_array is array (natural range<>) of bit;
  type bit_matrix is array(natural range<>, natural range<>) of bit;

  function "+" (left_matrix : bit_matrix; right_matrix : bit_matrix) return natural;

end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package body pkg is
    -- Overloaded operator counts number of '1' bits in both matrices
    function "+" (left_matrix : bit_matrix; right_matrix : bit_matrix) return natural is
       variable result : natural := 0;
    begin
       for i in left_matrix'range(1) loop
           for j in left_matrix'range(2) loop
               if (left_matrix(i,j) = '1') then
                  result := result + 1;
               end if; 
               if (right_matrix(i,j) = '1') then
                  result := result + 1;
               end if; 
           end loop;
       end loop;
       return result;
    end;

end pkg;

library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity op_ovload_range is
  
  port (
    clk   : in  std_logic;
    reset : in  std_logic;
    in1   : in  bit_matrix(2 downto 0, 2 downto 0);
    in2   : in  bit_matrix(2 downto 0, 2 downto 0);
    in3   : in  bit_vector(18 downto 0);
    out1  : out bit_vector(18 downto 0));

end op_ovload_range;

architecture arch of op_ovload_range is

begin  

  process (clk, reset)
  begin  
    if clk'event and clk = '1' then
      if reset = '1' then          
        out1 <= (others => '0');
       else
        out1 (in1 + in2 downto 0) <= in3 (in1 + in2 downto 0);
      end if;
    end if;
  end process;
  
end arch;
