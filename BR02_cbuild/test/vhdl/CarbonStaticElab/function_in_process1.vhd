-- Declare a function in a process
--
library ieee;
use ieee.std_logic_1164.all;
entity function_in_process1 is
port( a : in std_logic_vector(15 downto 0);
      b : in std_logic_vector(15 downto 0);
      c : out std_logic_vector(31 downto 0);
      d : out std_logic_vector(7 downto 0)
     );
end function_in_process1;

architecture rtl of function_in_process1 is
begin

process(a,b) is

function swap_and(a,b : std_logic_vector) return std_logic_vector is
begin
   return b & a;
end;

begin
  if (a = (a'range => '1')) then
     c <= a & b;
  else
     c <= swap_and(a,b);
  end if;     
end process;

process(a,b) is

function swap_and(a,b : std_logic_vector) return std_logic_vector is
constant width : integer := a'length;
variable result : std_logic_vector((width/2)-1 downto 0);
begin
   result := a((width/2)-1 downto 0) and b((width/2)-1 downto 0);       
   return result;
end;

begin

   d <= swap_and(a,b);

end process;

end architecture;

