-- This test case causes a "ranges do not overlap" CARBON
-- INTERNAL ERROR in the Verific flow, using the
-- CarbonStaticElaborator with loops forced to be
-- unrolled.
--
-- The problem is that the nucleus populator
-- 'if' dead branch pruning is not smart enough
-- to prune cases in which the following statement
-- will not be executed:
--
--    dout(i+3 downto 0) <= (others=>'1');
package mytypes is
   type integers is array(integer range<>) of integer;   
   constant NUM : integer := 8;
end package mytypes;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use work.mytypes.all;
entity loop_in_elsif2 is
port (
        clk   : in std_logic;
        rst   : in std_logic;
        op    : in std_logic;
        din   : in std_logic_vector(NUM-1 downto 0);
        dout  : out std_logic_vector(NUM-1 downto 0)
);
end loop_in_elsif2;

architecture rtl of loop_in_elsif2 is

begin

   process (clk, rst)
   begin
   if rst = '1' then
      dout <= (others => '0'); 
   elsif clk'event and clk = '1' then
      if op = '1' then
         dout <= (others => '0'); 
      elsif op = '0' then 
         for i in NUM-1 downto 0 loop
           if i <= (NUM/2) then
              -- This doesn't get pruned properly   
              dout(i+3 downto 0) <= (others=>'1');
           -- because this condition is not constant.
           elsif unsigned(din) /= 0 then
              dout <= din;
           else
              dout <= "11110000"; 
           end if;
         end loop;
      end if;
   end if;
   end process;
end architecture rtl;
