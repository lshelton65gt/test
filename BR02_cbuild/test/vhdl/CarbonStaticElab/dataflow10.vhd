-- Test that subprogram calls that assign a value
-- to a dataflow variable inside an unrolled loop
-- are properly detected, and invalidated in the
-- parent dataflow.
--
-- This test required -vhdlLoopUnroll

entity dataflow10 is
	port (
		in1 : in bit_vector(7 downto 0);
		count : out integer;
		error : out boolean
	);
end;

architecture rtl of dataflow10 is

function increment (b : bit_vector; v : integer) return integer is
  variable result : integer;
begin
   result := 0;
   for j in b'range loop
     if (b(1) = '1') then
        result := result + 1;
     end if;
   end loop;
   return v + result;
end;

procedure do_assign(variable i : in integer; variable o : out integer) is
begin
      o := i + 7;         
end;

begin
	process(in1)
		variable tcount : integer ;
		variable seen_first_zero, seen_last_zero : boolean ;
                variable foobar : integer;
                variable dead : integer;
	begin
		error <= false;
                seen_first_zero := false;
                seen_last_zero := false;
                tcount := 0;
                foobar := 3;
		for i in 7 downto 0
		loop
			if (seen_last_zero and in1(i) = '0') then
				tcount := 0;
				error <= true;
				exit;
			elsif (seen_first_zero and in1(i) = '1') then
				seen_last_zero := true;
			elsif (in1(i) = '0') then
				seen_first_zero := true;
				tcount := tcount + 1;
                                -- Assign to 'foobar' here should
                                -- should invalidate id in parent dataflow
                                do_assign(o=>foobar, i=>tcount);
			end if;
		end loop;
                -- We should be smart enough to know that foobar is not 3
                -- here, and therefore this loop cannot be unrolled.
                for j in 0 to foobar loop
                  tcount := tcount + 1;
                end loop;
		count <= tcount;
        end process;
end;
							

