-- This tests calls to functions whose arguments and
-- return values are fully constrained, but make
-- calls to functions (setcount, in this case)
-- whose arguments are unconstrained,
-- and whose widths are a function of the value passed
-- in to the parent function.
--
-- The Interra and Verific flows both
-- handle this correctly. Two copies of 'parent'
-- are made, and two copies of 'setcount' are made.
--

 

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;

entity for_loop_inside_function is
port (
        clk   : in std_logic;
        din   : in std_logic_vector(31 downto 0);
        dout1 : out integer;
        dout2 : out integer
);
end for_loop_inside_function;

architecture rtl of for_loop_inside_function is

-- This function counts the number of bits that are '1' in the supplied
-- vector.
function setcount(vec : std_logic_vector) return integer is
variable count : integer := 0;
begin
    l1: for i in vec'range loop
       if vec(i) = '1' then
          count := count + 1;
       end if;
    end loop l1;
    return count;
end setcount;

-- This function counts the number of bits that are '1' in supplied vector,
-- starting at supplied position.
function parent(vec : std_logic_vector(31 downto 0); pos : integer) return integer is
begin
   return setcount(vec(pos downto 0));
end parent;

begin
   process (clk)
   begin
   if clk'event and clk = '1' then
      -- for i in 0 to 0 loop
      dout1 <= parent(din, 3);
      dout2 <= parent(din, 7);
      -- end loop;
   end if;
   end process;
end architecture rtl;
