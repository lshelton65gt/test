-- This tests id invalidation for case statements

entity dataflow6 is
port( 
      sel : in bit;
      o1 : out integer
     );
end dataflow6;

architecture rtl of dataflow6 is
begin

   process (sel) is
   constant incrementer : integer := 32;
   variable temp: integer;
   variable acc : integer;
   begin
      temp := 3;
      case (sel) is
        when '1' => temp := 5;
        when '0' => temp := 2;
        when others => temp := 1;
      end case;
      acc := 0;
      for i in 0 to temp loop
         acc := acc + temp;
      end loop;
      o1 <= acc;
   end process;

end architecture;

