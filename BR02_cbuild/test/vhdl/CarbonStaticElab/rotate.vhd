-- Verify constraint expression processing for
-- rol builtin operator.

library ieee;
use ieee.std_logic_1164.all;
-- use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

entity rotate is
port( a : in signed(7 downto 0);
      o : out signed(7 downto 0)
     );
end rotate;

architecture rtl of rotate is

function get_val(v1 : signed) return signed is
   begin
        return v1 rol 3;
   end;


begin

o <= get_val(a);

end architecture;

