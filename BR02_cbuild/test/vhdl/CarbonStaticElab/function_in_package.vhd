-- This tests CarbonStaticElaboration function uniquification for
-- functions declared inside a package.

library ieee;
use ieee.std_logic_1164.all;

-- Package header
package func_pack is

   function swap(i : std_logic_vector) return std_logic_vector;

end func_pack;

-- Package body
package body func_pack is

   function swap(i : std_logic_vector) return std_logic_vector is
      constant len : integer := i'length;
   begin
      return i((len/2)-1 downto 0) & i(len-1 downto (len/2));
   end swap;
 
end func_pack;

library ieee;
use ieee.std_logic_1164.all;
use work.func_pack.all;
entity function_in_package is
port( a : in std_logic_vector(15 downto 0);
      b : in std_logic_vector(7 downto 0);
      c : out std_logic_vector(15 downto 0);
      d : out std_logic_vector(7 downto 0)
     );
end function_in_package;

architecture rtl of function_in_package is

begin

c <= swap(a);
d <= swap(b);

end architecture;
