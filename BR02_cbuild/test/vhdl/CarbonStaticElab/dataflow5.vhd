-- 'if' condition uses and 'id' that is on rhs in branch
--
-- The interra flow segfaults
-- The Verific flow (w/o CSE): Error: unable to determine size of arg to invert
-- The Verific flow (w CSE): Similar error to above.

library ieee;
use ieee.std_logic_1164.all;
entity dataflow5 is
port (  
        size        : in integer;
        din         : in std_logic_vector(31 downto 0);
        dout        : out std_logic_vector(31 downto 0)
);
end dataflow5;

architecture rtl of dataflow5 is

function invert (din: std_logic_vector) return std_logic_vector is
begin
  return not din;
end;

begin

   process (din, size)
   begin
     -- The dataflow value for 'size' should be 0 here
     if (size = 32) then
        -- The dataflow value for 'size' should be 32 here
        dout <= invert(din(size-1 downto 0));
     else
        dout <= din;
     end if;

   end process;

end architecture rtl;
