-- This tests loop unrolling when the loop
-- has a function call, that value of whose argument
-- determines the width of an argument to a nested function
-- call.
--
-- Unless we unroll the loop, we can't possibly figure out
-- the width of the nested function call.
--
-- Currently, both the Verific and the Interra flow
-- unroll the loop, and handle this correctly.
-- However, the test fails when -enableCSElab is
-- given.

package mytypes is
   type integers is array(integer range<>) of integer;   
end package mytypes;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use work.mytypes.all;
entity function_inside_for_loop is
port (
        clk   : in std_logic;
        din   : in std_logic_vector(31 downto 0);
        dout  : out integers(1 downto 0)
);
end function_inside_for_loop;

architecture rtl of function_inside_for_loop is

-- This function counts the number of bits that are '1' in the supplied
-- vector.
function setcount(vec : std_logic_vector) return integer is
variable count : integer := 0;
begin
    for i in vec'range loop
       if vec(i) = '1' then
          count := count + 1;
       end if;
    end loop;
    return count;
end setcount;

-- This function counts the number of bits that are '1' in supplied vector,
-- starting at supplied position.
function foobar(vec : std_logic_vector(31 downto 0); pos : integer) return integer is
begin
   return setcount(vec(pos downto 0));
end foobar;

begin
   process (clk)
   begin
   if clk'event and clk = '1' then
      for i in 0 to 1 loop
        dout(i) <= foobar(din, i);
      end loop;
   end if;
   end process;
end architecture rtl;
