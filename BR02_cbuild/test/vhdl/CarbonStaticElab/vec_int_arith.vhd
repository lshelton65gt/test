-- Reproduce an issue found in customer 2 test 2b.
-- "Error 3266: Failed in Carbon Static 
-- Elaboration: VhdlConstraintElaborator. Details: Function 'foo' 
-- returns different size results for the same call signature.. 
-- Unable to recover."


library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity vec_int_arith is
port (
   a1 : in std_logic_vector(4 downto 0);
   a2 : in std_logic_vector(4 downto 0);
   a3 : in std_logic_vector(4 downto 0);
   out1 : out std_logic_vector(4 downto 0)
);
end vec_int_arith;

architecture rtl of vec_int_arith is

  function dr_func(in1 : std_logic_vector) return std_logic_vector is
      variable res : std_logic_vector(in1'range);
  begin
      res := in1 + '1';
      return '0' & res(in1'LEFT downto 1);
  end dr_func;

  function clip (in1 : std_logic_vector; in2 : std_logic_vector) return std_logic_vector is
  begin
     if (in1 > in2) then
        return in1;
     else
        return in2;
     end if;
  end;

begin

-- Prior to the fix, we computed the range of expression "a1-1+a2" as "0 to 4", not "4 downto 0",
-- causing a NULL range warning in dr_func, and a caluclated return size/range
-- for dr_func of "0 to 0".
out1 <= clip(dr_func(a1 - 1 + a2), a3);

end rtl;
