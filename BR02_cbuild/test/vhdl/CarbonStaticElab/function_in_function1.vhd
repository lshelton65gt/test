-- This testcase explores the processing of functions declared inside a function
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity function_in_function1 is
port( a : in std_logic_vector(15 downto 0);
      b : in std_logic_vector(15 downto 0);
      c : out unsigned(15 downto 0)
     );
end function_in_function1;

architecture rtl of function_in_function1 is
   function round_robin(valids : std_logic_vector; prev_req : std_logic_vector) return ieee.std_logic_arith.unsigned is 
   -- Function declared inside a function
   function doand(in1, in2: std_logic_vector) return unsigned is
      variable result : std_logic_vector(in1'range);  
   begin
      result := in1 and in2;
      return unsigned(result);
   end;
begin
   return doand(prev_req, valids);
end ;

begin

 c <= round_robin(a,b);

end architecture;




