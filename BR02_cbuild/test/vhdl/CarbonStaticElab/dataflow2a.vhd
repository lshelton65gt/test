-- This tests dataflow management. temp must not be equal
-- to '0' after the loop is processed.
library ieee;
use ieee.std_logic_1164.all;
entity dataflow2a is
port (
        din   : in integer;
        din2  : in std_logic;
        dout  : out integer;
        dout2 : out std_logic
);
end dataflow2a;

architecture rtl of dataflow2a is

function inc (v : integer) return integer is
begin
  return v + 1;
end;

begin

   dout2 <= din2;
     
   process (din)
     variable temp : integer := 0;
     variable acc : integer := 0;
   begin
     temp := 0;
     -- The dataflow value for 'temp' should be 0 here
     for i in 0 to 2 loop
         if (i > 0) then
            temp := i;
         end if;
     end loop;
     -- The dataflow value for 'temp' should be 2 here.
     -- Or, it could be 'unknown', depending on how
     -- the dataflow works. But it should not be '0'.
     for j in 0 to temp loop
         acc := acc + j;
     end loop;
     dout <= acc;
   end process;

end architecture rtl;
