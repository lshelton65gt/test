-- This test case exposes a case that
-- is not handled in the Verific flow
-- by VerificVhdlDesignWalker::arraySliceWithConstIndices().
-- As a consequence, it asserts.
-- Exposed by customer 2 testcase, Issue 22
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;

entity typeconv6 is
generic (
      NO_OF_BURSTS : integer := 16;
      BEATS_PER_BURSTS : integer := 8
      );
port( a : in bit_vector(NO_OF_BURSTS-1 downto 0);
      b : in bit_vector(NO_OF_BURSTS-1 downto 0);
      ram_addr : out std_logic_vector(11 downto 0)
     );
end typeconv6;

architecture rtl of typeconv6 is

begin

ram_addr <= To_StdLogicVector(a and b)(11 downto 0);

end architecture;

