-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

--6.1.1 of testplan version 2.0
--nand operator with bit array type

package pack_for_with_next is
		type arr is array(7 downto 0) of bit;
end;

use work.pack_for_with_next.all;

entity for_with_next is 
		port(in1,in2 : bit_vector(7 downto 0);
				output : out bit_vector(7 downto 0));
end;

architecture for_with_next of for_with_next is
begin
    p1: process(in1,in2)
        variable temp1, temp2, temp3, temp4 : arr;
    begin
        l1: for i in 0 to 7 loop 
            temp1(i) := in1(i);
            temp2(i) := in2(i);
        end loop l1;
        temp3 := temp1 nand temp2;
        z2: for i in 0 to 1 loop
          l3: for j in 0 to 3 loop
            if (j = 2) then
              output(3*i+j) <= '1';
              next z2;
            end if;
            if (in1(i) = in2(i)) then
                output(3*i+j) <= '0';
            else
                output(3*i+j) <= temp3(3*i+j);
            end if;
          end loop l3;
        end loop z2;
        temp4 := "00000001";
        if ( temp4 = "00000001") then
            temp4 := "00000010";
        else
            temp4 := "00000111";
        end if;
    end process p1;
end;


