-- This is a simple minded recursive function that
-- reverses a bit vector.
-- Note: The Interra flow cannot compile this, due
-- to the recursion.
entity recursive1 is
generic (
      N : natural := 4
      );
port( a : in bit_vector(N-1 downto 0);
      b : in bit_vector((N/2)-1 downto 0);
      o1 : out bit_vector(N-1 downto 0);
      o2 : out bit_vector((N/2)-1 downto 0)
     );
end recursive1;

architecture rtl of recursive1 is

-- Recursive function to reverse a slv
function reverse(v : bit_vector) return bit_vector is
   constant W : integer := v'length;
   begin
      if (W = 1) then
         return v;
      else
         return v(v'right) & reverse(v(v'left downto v'right+1));
      end if;
   end;


begin

o1 <= reverse(a);
o2 <= reverse(b);

end architecture;

