-- This tests 'parent statement tracking' for operator
-- overload processing. 
--
-- The VhdlSubprogCallUniquifier class replaces overloaded
-- operators with function calls. To do that, it needs
-- to keep track of the "parent" statment in which
-- the overloaded operator expression appears.
--
-- This test case has overloaded operators in
-- the conditional expressions of an 'if' statement,
-- as well as in the then/else statements. It has
-- expressions with multiple overloaded operators.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package pkg is

  type bit_array is array (natural range<>) of bit;
  type bit_matrix is array(natural range<>, natural range<>) of bit;

  function "+" (left_matrix : bit_matrix; right_matrix : bit_matrix) return bit_matrix;
  function "-" (left_matrix : bit_matrix; right_matrix : bit_matrix) return bit_matrix;
  function "=" (left_matrix : bit_matrix; right_matrix : bit_matrix) return boolean;
  function invert(matrix : bit_matrix) return bit_matrix;
  function matrix_xor(left_matrix : bit_matrix; right_matrix : bit_matrix) return bit_matrix;

end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package body pkg is

    -- Matrix addition
    function "+" (left_matrix : bit_matrix; right_matrix : bit_matrix) return bit_matrix is
       variable result : bit_matrix(left_matrix'range(1), left_matrix'range(2));
    begin
       for i in left_matrix'range(1) loop
           for j in left_matrix'range(2) loop
               result(i,j) := left_matrix(i,j) or right_matrix(i,j);
           end loop;
       end loop;
       return result;
    end;

    -- Matrix subtraction
    function "-" (left_matrix : bit_matrix; right_matrix : bit_matrix) return bit_matrix is
       variable result : bit_matrix(left_matrix'range(1), left_matrix'range(2));
    begin
       for i in left_matrix'range(1) loop
           for j in left_matrix'range(2) loop
               result(i,j) := left_matrix(i,j) and right_matrix(i,j);
           end loop;
       end loop;
       return result;
    end;
        
    -- Matrix comparison
    function "=" (left_matrix : bit_matrix; right_matrix : bit_matrix) return boolean is
       variable is_equal : boolean := true;
    begin
       for i in left_matrix'range(1) loop
           for j in left_matrix'range(2) loop
               if (left_matrix(i,j) /=  right_matrix(i,j)) then
                  is_equal := false;
               end if;
           end loop;
       end loop;
       return is_equal;
    end;

    -- Invert all bits in a matrix
    function invert(matrix : bit_matrix) return bit_matrix is
       variable result : bit_matrix(matrix'range(1), matrix'range(2));
    begin
       for i in matrix'range(1) loop
           for j in matrix'range(2) loop
               result(i,j) := not matrix(i,j);
           end loop;
       end loop;
       return result;
    end;

    -- Perform an exclusive or operation, implemented with a function call
    function matrix_xor(left_matrix : bit_matrix; right_matrix : bit_matrix) return bit_matrix is
       variable result : bit_matrix(left_matrix'range(1), left_matrix'range(2));
    begin
       for i in left_matrix'range(1) loop
           for j in left_matrix'range(2) loop
               result(i,j) := left_matrix(i,j) xor right_matrix(i,j);
           end loop;
       end loop;
       return result;
    end;

end pkg;

library ieee;
use ieee.std_logic_1164.all;
use work.pkg.all;

entity op_ovload is
  
  port (
    clk   : in  std_logic;
    reset : in  std_logic;
    in1   : in  bit_matrix(2 downto 0, 2 downto 0);
    in2   : in  bit_matrix(2 downto 0, 2 downto 0);
    out1  : out bit_matrix(2 downto 0, 2 downto 0));

end op_ovload;

architecture arch of op_ovload is

begin  -- arch

  process (clk, reset)
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge
      if reset = '1' then                 -- asynchronous reset (active low)
        out1 <= (others => (others => '0'));
       elsif in1 = in2 then
        out1 <= (in1 + in1) - (in2 + in2);
       elsif (in1(0,0) = '1') then
        out1 <= invert(in1 + in2 + in1);
       elsif (in1(0,1) = '0') then
        out1 <= matrix_xor(in1 + in2, in1 - in2);
       elsif (in1(1,0) = '1') then
        out1 <= matrix_xor(in1, in2) - invert(in1);
       else
        out1 <= in1 + in2;
      end if;
    end if;
  end process;
  
end arch;

