-- This is a reproducer for customer 2 issue 27
-- std_logic const evaluation is not handled
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity std_logic_const is
port (a: in std_logic;
      b : in std_logic;
      c : out std_logic
     );
end std_logic_const;

architecture rtl of std_logic_const is

function func (i1 : std_logic; i2 : std_logic) return std_logic is
variable temp : std_logic;
begin
   temp := i2;
   return temp and i1;
end;
 
begin

 c <= func(a and b, '0' or '1');

end rtl;
