-- This tests branch pruning for 'case' statements.

-- Unfortunately, it crashes in the nucleus populator

entity case_recurse is
generic (
      N : natural := 4
      );
port( a : in bit_vector(N-1 downto 0);
      b : in bit_vector((N/2)-1 downto 0);
      o1 : out bit_vector(N-1 downto 0);
      o2 : out bit_vector((N/2)-1 downto 0)
     );
end case_recurse;

architecture rtl of case_recurse is

-- Recursive function to reverse a slv
function reverse(v : bit_vector) return bit_vector is
   constant W : integer := v'length;
   begin
      case W is
         when 1 => return v;
         when others => return v(v'right) & reverse(v(v'left downto v'right+1));  
      end case;
   end;


begin

o1 <= reverse(a);
o2 <= reverse(b);

end architecture;

