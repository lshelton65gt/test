-- This tests dataflow management - invalidating a
-- variable value in an if condition that depends
-- on an input - inside a loop.
library ieee;
use ieee.std_logic_1164.all;
entity dataflow1 is
port (
        din   : in integer;
        din2  : in std_logic;
        dout  : out integer;
        dout2 : out std_logic
);
end dataflow1;

architecture rtl of dataflow1 is

function inc (v : integer) return integer is
begin
  return v + 1;
end;

begin

   dout2 <= din2;
     
   process (din)
     variable temp : integer := 0;
   begin
     temp := 0;
     -- The dataflow value for 'temp' should be 0 here
     for i in 0 to 2 loop
       if din > 5 then
          temp := temp + i;
       end if;
     end loop;
     -- The dataflow value for 'temp' should be unknown here.
     -- The CarbonStaticElab used to have temp '0' here.
     dout <= inc(temp); 
   end process;

end architecture rtl;
