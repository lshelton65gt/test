
package pack_RewriteLoop is
		type arr is array(7 downto 0) of bit;
end;

use work.pack_RewriteLoop.all;

entity RewriteLoop is 
		port(in1,in2 : bit_vector(7 downto 0);
				output : out bit_vector(7 downto 0));
end;

architecture RewriteLoop of RewriteLoop is
begin
    p1: process(in1,in2)
        variable temp1, temp2, temp3, temp4 : arr;
    begin
        l1: for i in 0 to 7 loop 
            temp1(i) := in1(i) ;
            temp2(i) := in2(i) ;
        end loop l1;
        temp3 := temp1 nand temp2;
        l2: for i in 0 to 7 loop
            if (i = 0) then
                output(i) <= '0';
            else
                output(i) <= temp3(i);
            end if;
        end loop l2;
        temp4 := "00000001";
        if ( temp4 = "00000001") then
            temp4 := "00000010";
        else
            temp4 := "00000111";
        end if;
    end process p1;
end;


