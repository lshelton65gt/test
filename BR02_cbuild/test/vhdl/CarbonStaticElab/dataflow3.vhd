-- This test case demonstrates a problem
-- with data flow management in the CarbonStaticElaborator.
-- NOTE that Interra gets the wrong simulation result for
-- this test - but the Verific flow (with CarbonStaticElaboration)
-- matches Modelsim output.
library ieee;
use ieee.std_logic_1164.all;
entity dataflow3 is
port (  
        din         : in std_logic_vector(1 downto 0);
        sel_input   : in std_logic;
        dout        : out std_logic_vector(1 downto 0)
);
end dataflow3;

architecture rtl of dataflow3 is

begin

   process (din, sel_input)
     variable temp : integer := 1;
   begin
     temp := 0;
     -- The dataflow value for 'temp' should be 0 here
     for i in 0 to 2 loop
       if sel_input = '1' then
          temp := 1;
       end if;
     end loop;
     -- The dataflow value for 'temp' should be unknown here.
     -- And the loop cannot be unrolled, even with -vhdlLoopUnroll
     for j in 0 to temp loop
        dout(j) <= not din(j);
     end loop;
   end process;

end architecture rtl;
