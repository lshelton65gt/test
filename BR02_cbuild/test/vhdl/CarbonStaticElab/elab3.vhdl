-- Derived from beacon_misc/misc.elab3.vhdl
-- This is a torture test for CarbonStaticElaboration
-- and in particular dataflow analysis.
-- Interra cannot elaborate this.
-- CSE should elaborate it ok, without error.
-- Unfortunately, it encounters backend compile issues.
LIBRARY ieee                    ;
USE ieee.std_logic_1164.all     ;
ENTITY elab3 IS
  PORT(    in1         : in  std_logic_vector(88 downto 0) ;
           in2         : in  std_logic_vector(42 downto 0) ;
		   in3         : in  std_logic;
		   in4         : in  std_logic_vector(15 downto 0);
		   out1        : out std_logic_vector(88 downto 0)) ;
END elab3 ;
ARCHITECTURE elab3 OF elab3 IS

constant const1     : integer :=  30 ;
constant const2   : integer :=  39 ;
constant const3     : integer :=  40 ;
constant const4     : integer :=  41 ;
constant zbusMsb       : integer := 88 ;
constant zExpSize      : integer := 16 ;
constant zExpLSB       : integer := zbusMsb-zExpSize ;
constant zManMSB       : integer := zExpLSB-1 ;
constant zExpMSB       : integer := zbusMsb-1 ;

 function imin( L : integer; R : integer ) return integer is
 variable minimum : integer;
 begin
        if l > r then minimum := R;
        else minimum := L;
        end if;
        return(minimum);
 end;

 function vec_0_not( invec : std_logic_vector  ) return std_logic_vector IS
 VARIABLE outvec: std_logic_vector( invec'high DOWNTO invec'low ) ;
 BEGIN
  outvec := NOT(invec) ;
  return( outvec ) ;
 END ;

 function vec_not( invec : std_logic_vector ; fsize : integer := 0; 
                        intleav : integer := 0 ) return std_logic_vector IS
 VARIABLE outvec: std_logic_vector( invec'range ) ;
 variable fsize1, nfolds, h, l : integer;
 BEGIN
   -- ignore carbon translate_off
   if fsize = 0 then fsize1 := outvec'length;
   else fsize1 := fsize;
   end if;
   -- ignore carbon translate_on
   -- fsize1 := outvec'length;
   nfolds := (invec'length-1)/fsize1+1;
   for i in 1 to nfolds loop
        h := imin(invec'high, (invec'low + i*fsize1-1) );
        l := invec'low + (i-1)*fsize1;
        if intleav = 0 then
           outvec(h downto l) := vec_0_not(invec(h downto l)); 
        end if;
   end loop;
  return( outvec ) ;
 END ;
function zero( length: integer ) return std_logic_vector IS
  variable vector: std_logic_vector( length-1 downto 0 ) ;
BEGIN
  vector := (others => '0') ;
  return vector ;
END zero ;
 function func1    ( control,controlx  : std_logic_vector(3 downto 0) ;
                        invec0,invec1,invec2,invec3 : std_logic_vector) return std_logic_vector IS
 VARIABLE outvec: std_logic_vector( invec0'range ) ;
 BEGIN
  CASE control IS
   WHEN "0001"  => outvec := invec0 ;
   WHEN "0010"  => outvec := invec1 ;
   WHEN "0100"  => outvec := invec2 ;
   WHEN "1000"  => outvec := invec3 ;
   WHEN OTHERS  => FOR i IN invec0'range LOOP outvec(i) := 'X' ;
                   END LOOP ;
  END CASE ;
  return(outvec) ;
 END ;
 function func2( control      : std_logic_vector(3 downto 0) ;
                        invec0,invec1,invec2,invec3 : std_logic_vector) return std_logic_vector IS
 VARIABLE outvec: std_logic_vector( invec0'range ) ;
 VARIABLE  controlx   : std_logic_vector(control'range);
 BEGIN
  controlx := not control ;
  outvec := func1 (control, controlx,invec0,invec1,invec2,invec3); 
  return(outvec) ;
 END ;

 function func3( control      : std_logic_vector(3 downto 0) ;
                        invec0,invec1,invec2,invec3       : std_logic_vector ;
                        fsize : integer := 0; 
                        intleav : integer := 0 ) return std_logic_vector IS
 variable outvec: std_logic_vector( invec0'length-1 downto 0 ) ;
 variable v0: std_logic_vector( invec0'length-1 downto 0 ) ;
 variable v1: std_logic_vector( invec0'length-1 downto 0 ) ;
 variable v2: std_logic_vector( invec0'length-1 downto 0 ) ;
 variable v3: std_logic_vector( invec0'length-1 downto 0 ) ;
 variable fsize1, nfolds, h, l : integer;
 begin
   -- ignore carbon translate_off
   if fsize = 0 then fsize1 := outvec'length;
   else fsize1 := fsize;
   end if;
   -- ignore carbon translate_on
   -- fsize1 := outvec'length;
   v0 := invec0;
   v1 := invec1;
   v2 := invec2;
   v3 := invec3;
   nfolds := (invec0'length-1)/fsize1+1;
   for i in 1 to nfolds loop
      h := imin(v0'high, i*fsize1-1 );
      l := (i-1)*fsize1;
      if intleav = 0 then
         outvec(h downto l) := func2(control, v0(h downto l), 
                v1(h downto l), v2(h downto l), v3(h downto l) ); 
      end if;
   end loop;
  return( outvec ) ;
 end;

BEGIN
  out1  <= func3( in2(const1)   & in2(const2) &
                          in2(const3)   & in2(const4)   ,
              (in3 & in4 & in1(zManMSB downto 0) ),
              ("01000000000111110" & zero(48) & 
                  in1(zExpMSB downto zExpLSB) & zero(8)),
              ("00111111111111111" & '1' & vec_not(in1(zManMSB downto 1)) ) ,
              ( in3 & in1(zbusMsb-1 downto 0) ) ) ;        
END elab3 ;

 
