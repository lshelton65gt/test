entity function_prefix is
port (
   a : in bit_vector(15 downto 0);
   b : out bit_vector(7 downto 0)
);
end function_prefix;

architecture rtl of function_prefix is

-- Recursive function to reverse a slv
function reverse(v : bit_vector) return bit_vector is
   constant W : integer := v'length;
   begin
      if (W = 1) then
         return v;
      else
         return v(v'right) & reverse(v(v'left downto v'right+1));
      end if;
   end;

begin

  b <= reverse(a)(0 to 7);

end rtl;

