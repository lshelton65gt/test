-- Expose another dataflow bug that will arise inside
-- loops that can't be unrolled. 
--
-- Scenario: Dataflow incorrectly thinks
-- a variable has the wrong value (in this case,
-- 'foobar').
--
-- Requires -vhdlLoopUnroll

entity dataflow8 is
	port (
		in1 : in bit_vector(7 downto 0);
		count : out integer;
		error : out boolean
	);
end;

architecture rtl of dataflow8 is
begin
	process(in1)
		variable tcount : integer ;
		variable seen_first_zero, seen_last_zero : boolean ;
                variable foobar : integer;
	begin
		error <= false;
                seen_first_zero := false;
                seen_last_zero := false;
                tcount := 0;
                -- dataflow has foobar = 3 here
                foobar := 3;
                -- loop cannot be unrolled, due to 'exit'
		for i in 7 downto 0
		loop
			if (seen_last_zero and in1(i) = '0') then
				tcount := 0;
                                -- this assignment to foobar should
                                -- inavlidate foobar id in dataflow.
                                foobar := 7;
				error <= true;
				exit;
			elsif (seen_first_zero and in1(i) = '1') then
				seen_last_zero := true;
			elsif (in1(i) = '0') then
				seen_first_zero := true;
				tcount := tcount + 1;
			end if;
		end loop;
                -- value of foobar should not be known, so loop
                -- cannot be unrolled.
                for j in 0 to foobar loop
                  tcount := tcount + 1;
                end loop;
		count <= tcount;
        end process;
end;
							

