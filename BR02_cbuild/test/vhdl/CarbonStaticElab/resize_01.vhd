-- This test reproduces ISSUE 24 in customer 2 design.
-- The resize seems to work OK if the argument is
-- of 'unsigned' type.

-- testcase extended to try all variations of extend and truncate
-- resize of a signed number when the requested size (arg 2) is smaller than
-- the width of the input (arg 1) is slightly strange e.g. 
-- {in1_s(msb) & in1_s(requested_size-2 downto 0)}  -- for signed (conbine signed bit with a slice that is 1 bit smaller than the requested size)
-- {             in1_u(requested-size-1 downto 0)}  -- for unsigned (normal truncation to requested size)


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity resize_01 is
generic (
      WIDTH : integer := 16
      );
port( in1_s : in signed(WIDTH-1 downto 0);
      in2_u : in unsigned(WIDTH-1 downto 0);
      out5_s: out signed(4 downto 0);
      out20_s: out signed(19 downto 0);
      out16_s: out signed(15 downto 0);
      out5_u: out unsigned(4 downto 0);
      out16_u: out unsigned(15 downto 0);
      out20_u: out unsigned(19 downto 0)
     );
end resize_01;

architecture rtl of resize_01 is

function invert_s (local_in1 : signed) return signed is
begin
  return not local_in1;
end;

function invert_u (local_in1 : unsigned) return unsigned is
begin
  return not local_in1;
end;


begin

process(in1_s, in2_u) is
begin

   out5_s<= invert_s(resize(in1_s, 5));       --truncate
   out5_u<= invert_u(resize(in2_u, 5));       --truncate
   out16_s<= invert_s(resize(in1_s, 16));      --no change to size
   out16_u<= invert_u(resize(in2_u, 16));      --no change to size
   out20_s<= invert_s(resize(in1_s, 20));      --extend
   out20_u<= invert_u(resize(in2_u, 20));      --extend
 
end process;
end architecture;

