-- This test case demonstrates a problem
-- with data flow management in the nucleus populator,
-- introduced by the CSE 'if' statement pruning.

library ieee;
use ieee.std_logic_1164.all;
entity dataflow12 is
generic (
        N     : integer := 3
        );
port (
        din   : in std_logic_vector(N-1 downto 0);
        dout : out integer
);
end dataflow12;

architecture rtl of dataflow12 is

-- Recursive function to compute factorial.
function fact(in1 : integer) return integer is
begin
   if (in1 > 0) then
      return in1 * fact(in1-1);
   else 
      return 1;
   end if;
end;

begin

   process (din)
     variable temp : integer := 0;
     variable val : integer := 0;
   begin
     temp := 0;
     val := 0;
     -- The dataflow value for 'val' should be 0 here
     if (N = 0) then
        val := 2;
     elsif (N = 3) then
        val := 3;
     end if;
     -- The dataflow value for 'val' should be 3 here
     -- The call to the recursive function should evaluate to
     -- a constant, since fact(3) can be evaluated during
     -- nucleus population.
     dout <= fact(val);
   end process;

end architecture rtl;
