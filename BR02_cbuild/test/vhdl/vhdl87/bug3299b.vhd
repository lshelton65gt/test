-- This design compiles in cbuild but not in Aldec--VHDL rules (Clause
-- 1.1.1.2) don't support the partial association of port out1.
library ieee;
use ieee.std_logic_1164.all;

package pack is
  type mem is array ( integer range 0 to 2) of std_logic_vector(0 to 1);
end;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bottom is
  port(in1, in2, in3 : in std_logic_vector(0 to 1); 
       out1 : buffer mem);
end;


architecture arch of bottom is
begin
  out1 <= (in1, in2, in3);
end;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bug3299b is
  port(in1, in2, in3 : in std_logic_vector(0 to 1); 
       out1: buffer std_logic_vector(0 to 1));
end;


architecture arch of bug3299b is
component bottom 
  port(in1, in2, in3 : in std_logic_vector(0 to 1); 
       out1 : buffer mem);
end component;
begin
  inst1 : bottom 
          port map(in1 => in1, in2 => in2, in3 => in3,
                  out1(0 to 1) => OPEN, 
                  out1(2) => out1);
end;

