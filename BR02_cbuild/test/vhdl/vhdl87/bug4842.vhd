use STD.textio.all;

entity bug4842 is
  
  port (
    clk : in  bit;
    i   : in  bit_vector (31 downto 0);
    o   : out bit_vector (31 downto 0));

end;

architecture foo of bug4842 is

  -- VHDL 87
  file OutFile : TEXT is out "out_data.txt";
  -- VHDL 93
--  file OutFile : TEXT open WRITE_MODE is "out_data.txt";
  
begin  -- foo

  main: process (clk)
  variable outline : LINE;
  begin  -- process main
    if clk'event and clk = '1' then  -- rising clock edge
      o <= i;
      write(outline, i);
      writeline(OutFile, outline);
    end if;
  end process main;

end foo;
