library ieee;
use ieee.std_logic_1164.all;

entity bottom is
  port(in1, in2 : in std_logic_vector(0 to 3); 
       out1 : buffer std_logic_vector(0 to 3));
end;


architecture arch of bottom is
begin
  out1 <= in1 and in2;
end;

library ieee;
use ieee.std_logic_1164.all;

entity bug3299 is
  port(in1, in2 : in std_logic_vector(0 to 3); 
       out1,out2 : buffer std_logic_vector(0 to 2));
end;


architecture arch of bug3299 is
component bottom 
  port(in1, in2 : in std_logic_vector(0 to 3); 
       out1 : buffer std_logic_vector(0 to 3));
end component;
begin
  inst1 : bottom 
         port map(in1 => in1, in2 => in2,
                  out1(0 to 1) => OPEN, 
                  out1(2 to 3) => out1(0 to 1));
  inst2 : bottom 
         port map(in1 => in1, in2 => in2,
                  out1(0) => OPEN, 
                  out1(1 to 3) => out2(0 to 2));
end;

