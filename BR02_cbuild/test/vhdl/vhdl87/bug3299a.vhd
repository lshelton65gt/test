
library ieee;
use ieee.std_logic_1164.all;

package pack is
  type mem is array ( integer range 0 to 2) of std_logic_vector(0 to 1);
end;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bottom is
  port(in1, in2, in3 : in std_logic_vector(0 to 1); 
       out1 : buffer mem);
end;


architecture arch of bottom is
begin
  out1 <= (in1, in2, in3);
end;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bug3299a is
  port(in1, in2, in3 : in std_logic_vector(0 to 1); 
       out1,out2: buffer std_logic_vector(0 to 1));
end;


architecture arch of bug3299a is
component bottom 
  port(in1, in2, in3 : in std_logic_vector(0 to 1); 
       out1 : buffer mem);
end component;
begin
  inst1 : bottom 
          port map(in1 => in1, in2 => in2, in3 => in3,
                  out1(0) => OPEN, 
                  out1(1) => out1, out1(2) => out2);
end;

