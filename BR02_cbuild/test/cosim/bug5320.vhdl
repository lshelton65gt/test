entity sub is
  
  port (
    a : in  bit_vector (7 downto 0);
    b : in  bit_vector (15 downto 0);
    c : in  bit_vector (31 downto 0);
    d : out bit_vector (23 downto 0));

end sub;

architecture rtl of sub is

begin  -- rtl

  d <= a & b;

end rtl;
