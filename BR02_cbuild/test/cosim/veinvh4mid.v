// no timescale

module veinvh4_L1_L(outp, inp, en );
   output [3:0] outp;
   input  [3:0] inp, en;

   veinvh4_L2_L u1(outp[3:2], inp[3:2], en[3:2]);
   veinvh4_L2_R u2(outp[1:0], inp[1:0], en[1:0]);

endmodule
