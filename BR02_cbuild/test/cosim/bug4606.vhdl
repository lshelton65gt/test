library ieee;
use ieee.std_logic_1164.all;

entity bug4606 is
  port ( clk: in  std_logic );
end bug4606;

architecture synth of bug4606 is
  signal	xwaddrMuxed	: std_logic_vector(5 downto 0);
  signal	xdatainMuxed	: std_logic_vector(31 downto 0);
  signal	xwrenMuxed	: std_logic;
  signal	xraddrMuxed	: std_logic_vector(5 downto 0);
  signal	tmpData         : std_logic_vector(31 downto 0);
  signal	memBypass	: std_logic;
  signal	v_lo            : std_logic;

  component h04_brfd64x32ar1sb 
    generic(
      words           : integer := 64;
      bits            : integer := 32;
      addresses       : integer := 6
      );
    port(
      do              : out   std_logic_vector(31 downto 0);
      so              : out   std_logic;
      di              : in    std_logic_vector(31 downto 0);
      web             : in    std_logic;
      wa              : in    std_logic_vector(5 downto 0);
      ra              : in    std_logic_vector(5 downto 0);
      wclk            : in    std_logic;
      se              : in    std_logic;
      si              : in    std_logic;
      byp             : in    std_logic
      );
  end component;

begin
  iR64X32: h04_BRFD64X32AR1SB
    port map (	
      wa	=> xwaddrMuxed,
      di	=> xdatainMuxed,
      web	=> xwrenMuxed,
      ra	=> xraddrmuxed,
      do	=> tmpData,
      byp	=> memBypass,
      se	=> v_lo,
      si	=> v_lo,
      so	=> open,
      wclk	=> clk
      );
end synth;
