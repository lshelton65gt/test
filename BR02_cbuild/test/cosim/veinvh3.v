module sub(outp, inp, en );
  output outp;
  input  inp, en;

  assign outp = inp & en;

endmodule

module sub2(outp, inp, en );
  output outp;
  input  inp, en;

   assign outp = en & inp;

   always @(en)
     $display("this has a side effect", en);

endmodule

