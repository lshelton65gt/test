-- last mod: Tue Sep 26 12:04:32 2006
-- filename: test/cosim/veinvh7.vhd
-- Description:  This test checks what errors/warnings are reported when ports
-- are left unconnected when instantiating verilog from vhdl.  BUG 6634


entity veinvh7 is
  
  port (
    out1     : out bit_vector(1 downto 0);
    in1, en1 : in  bit);

end veinvh7;

architecture arch of veinvh7 is

  component sub
    port (
      outp    : out bit;
--      inp  : in bit;
      en : in  bit
      );
  end component;

  component sub2
    port (
      outp    : out bit;
--      inp : in bit;
      en : in  bit);
  end component;

  signal sbus : bit_vector(1 downto 0);
  
begin  -- arch

  S1 : sub port map (
    en => in1,
    outp => sbus(1)
--    inp  => en1
    );

  S2 : sub2 port map (
    sbus(0),
--    in1,
    en1 );

  out1 <= sbus;
  
end arch;
