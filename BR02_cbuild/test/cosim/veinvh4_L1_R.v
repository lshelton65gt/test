`timescale 1ns/1ns

module veinvh4_L1_R(outp, inp, en );
   output [3:0] outp;
   input [3:0] 	inp;
   input 	en;

   veinvh4_L2_L u1(outp[3:2], inp[3:2], en);
   veinvh4_L2_R u2(outp[1:0], inp[1:0], en);

endmodule
