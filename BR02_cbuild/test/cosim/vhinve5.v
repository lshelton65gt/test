// last mod: Wed Oct  5 08:09:09 2005
// filename: test/cosim/vhinve5.v
// Description:  This test shows how ports that are left disconnected are handled by mvv


module vhinve5 (out, in1, en1);
   output [1:0] out;
   input  in1, en1;

   wire [1:0] bus;
   // named association, explicit specification of no connection to the ports
   // called 'out2' and 'unused_in'
   sub u1 (.en(in1), .out1(bus[0]), .inp(en1), .out2(), .unused_in());

   // named association, implicit no connection to the ports 'out2' and 'unused_in'
   sub u2 (.en(in1), .out1(bus[1]), .inp(en1));

   assign out = ~bus;

endmodule // top
