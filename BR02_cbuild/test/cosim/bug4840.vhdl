library ieee;
use ieee.std_logic_1164.all;

entity dff is
  port (
    clk  : in  std_logic;
    rstn : in  std_logic;
    d    : in  std_logic;
    q    : out std_logic
    );
end dff;

architecture rtl of dff is
begin
  dproc : process(clk, rstn)
  begin
    if(rstn='0')then
      q <= '0';
    elsif(clk'event and clk='1')then
      q <= d;
    end if;
  end process;
end rtl;  
