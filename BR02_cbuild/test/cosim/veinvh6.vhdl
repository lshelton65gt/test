-- in this test there is a name conflict between the verilog sub2 and the vhdl
-- sub2, make sure we pick up the correct one.

entity veinvh6 is
  
  port (
    out1     : out bit_vector(2 downto 0);
    in1, en1 : in  bit);

end veinvh6;

architecture arch of veinvh6 is

  component sub3
    port (
      outp    : out bit;
      inp, en : in  bit);
  end component;

  component sub
    port (
      outp    : out bit;
      inp, en : in  bit);
  end component;

  component sub2
    port (
      outp    : out bit;
      inp, en : in  bit);
  end component;

  signal sbus : bit_vector(2 downto 0);
  
begin  -- arch

  -- reference a verilog module first to make sure it gets picked up
  S3 : sub3 port map ( sbus(0), in1, en1 );

  S1 : sub port map (
    outp => sbus(2),
    inp  => en1,
    en   => in1);

  S2 : sub2 port map ( sbus(1), in1, en1 );

  out1 <= sbus;
  
end arch;



library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity sub2 is
  port (
    OUTP    : out bit;
    INp, eN : in  bit);
end sub2;

architecture a of sub2 is
begin
  ouTp <= iNP and EN;
end a;

