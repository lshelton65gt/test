module veinvh4bot(outp, inp, en );
   output outp;
   input  inp, en;

   assign outp = en & inp;

   always @(en)
     $display("this has a side effect", en);

endmodule
