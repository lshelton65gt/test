module top(i,o,c);
   input [7:0] i;
   output [7:0] o;
   input c;

   byte b[3:0] (.i(i),.o({o[3:0],o[7:4]}),.c(c));
endmodule

module byte(i,o,c);
   input [1:0] i;
   output [1:0] o;
   input c;
   test f[1:0] (i,o,c);
endmodule

