entity sub is
  port (
    outp    : out bit;
    inp, en : in  bit);
end sub;

architecture a of sub is
begin
  outp <= inp and en;
end a;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity sub2 is
  port (
    outp    : out std_logic;
    inp, en : in  std_logic);
end sub2;

--Make sure missing arch is detected
--architecture a of sub2 is
--begin
--  outp <= inp and en;
--end a;
