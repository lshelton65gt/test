// test VHDL instances inside a Verilog generate.  This is the simple
// test case for bug4840 found at Skyworks.  This was verified with MTI,
// because both NC and Aldec can't do this correctly.

module tc_gen (clk, rstn, din, qout);

   parameter width = 4;

   input     clk;
   input     rstn;
   input [width-1:0] din;
   output [width-1:0] qout;

   genvar  index;

   generate
   for (index = 0; index < width; index = index + 1)
     begin: xxxx
        dff dff_gen
          (
           .D (din[index]),   
           .Q (qout[index]),    
           .CLK (clk),   
           .RSTN (rstn)  
           );
     end
   endgenerate

endmodule
