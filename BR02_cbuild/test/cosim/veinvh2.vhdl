entity veinvh2 is
  
  port (
    out1     : out bit_vector(1 downto 0);
    in1, en1 : in  bit);

end veinvh2;

architecture arch of veinvh2 is

  component sub
    port (
      outp    : out bit;
      inp, en : in  bit);
  end component;

  component sub2
    port (
      outp    : out bit;
      inp, en : in  bit);
  end component;

  signal sbus : bit_vector(1 downto 0);
  
begin  -- arch

  S1 : sub port map (
    en => in1,
    outp => sbus(1),
    inp  => en1);

  S2 : sub2 port map ( sbus(0), in1, en1 );

  out1 <= sbus;
  
end arch;
