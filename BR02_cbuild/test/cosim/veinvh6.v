// last mod: Wed Jan  4 09:01:07 2006
// filename: test/cosim/veinvh6.v
// Description:  This test is to test case where there is a name conflict
// between verilog and vhdl for the module sub2


module sub(outp, inp, en );
  output outp;
  input  inp, en;

  assign outp = inp & en;

endmodule

module sub2(outp, inp, en );
  output outp;
  input  inp, en;

   initial
     $display("%m if this line is printed then there is an error.");
   
  assign outp = (!en) & inp;

endmodule


module sub3(outp, inp, en );
  output outp;
  input  inp, en;

   initial
     $display("%m This line must always be printed");
   
  assign outp = (!en) & inp;

endmodule

