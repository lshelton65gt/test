entity sub is
  port (
    OUTP    : out bit;
    INP, EN : in  bit);
end sub;

architecture a of sub is
begin
  OUTP <= INP and EN;
end a;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity sub2 is
  port (
    OUTP    : out std_logic;
    INp, eN : in  std_logic);
end sub2;

architecture a of sub2 is
begin
  ouTp <= iNP and EN;
end a;

architecture b of sub2 is
begin 
end;
