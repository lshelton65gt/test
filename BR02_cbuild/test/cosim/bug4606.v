module H04_BRFD64X32AR1SB( do, so, di, web, wa, ra, wclk , se ,si, byp );
   // RAM PARAMETERS
   parameter WORDS = 64,
             BITS = 32,
             ADDRESSES  = 6;

   // RAM I/O
   output [31:0] do;
   output        so;
   input [31:0]  di;
   input         web;
   input [5:0]   wa;
   input [5:0]   ra;
   input         wclk;
   input         se;
   input         si;
   input         byp;
endmodule
