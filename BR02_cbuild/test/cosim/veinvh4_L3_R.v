`timescale 1ns/1ns

module veinvh4_L3_R(outp, inp, en );
   output  outp;
   input   inp;
   input en;

   assign outp = en & inp;

   always @(en)
     $display("in %m, this has a side effect", en);


endmodule
