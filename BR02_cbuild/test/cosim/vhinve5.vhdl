entity sub is
  port (
    OUT1    : out bit;
    OUT2 : out bit;
    inp, en, unused_in : in  bit);
end sub;

architecture a of sub is
begin
  OUT2 <= inp xor en;
  out1 <= inp and en;
end a;
