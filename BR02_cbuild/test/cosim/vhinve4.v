// Test named and positional association for net connections in a VHDL in Verilog design

module top (out, in1, en1);
   output [1:0] out;
   input  in1, en1;

   wire [1:0] bus;
// named association
   sub S1 (.en(in1), .outp(bus[1]), .inp(en1), .missing());

// positional association
   sub2 S2 (bus[0], in1, en1);

   assign out = ~bus;

endmodule // top
