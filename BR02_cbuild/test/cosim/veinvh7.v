module sub(outp, inp, en );
  output outp;
  input  inp, en;

  assign outp = inp & en;

endmodule

module sub2(inp, en, outp );
  output outp;
  input  inp, en;

  assign outp = en & inp;

endmodule

