// last mod: Wed Jan  4 09:28:03 2006
// filename: test/cosim/vhinve6.v
// Description:  This test has a name conflict between the verilog sub2 and the
// vhdl sub2, make sure we pick up the correct one.


module vhinve6 (out, in1, en1);
   output [1:0] out;
   input  in1, en1;

   wire [1:0] bus;
// named association
   sub S1 (.en(in1), .outp(bus[1]), .inp(en1));

// positional association
   sub2 S2 (bus[0], in1, en1);

   assign out = ~bus;

endmodule // top


module printanerror(out1);
   output out1;

   reg 	  out1;
   initial
     begin
	$display("if you see this line then there has been an error");
	out1 = 0;
     end
endmodule


module sub2(out, in1, en1);
   output out;
   input  in1, en1;

   assign out = in1 & en1;
endmodule
