
// this file is the top layer of verilog
// several layers of verilog below this point (defined in veinvh4mid.v veinvh4bot.v)
module sub(outp, inp, en );
  output outp;
  input  inp, en;

  assign outp = inp & en;

endmodule

module sub2(outp, inp, en );
  output outp;
  input  inp, en;

   veinvh4mid u1(outp, inp, en );

endmodule

