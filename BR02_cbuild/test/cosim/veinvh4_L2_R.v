`timescale 1ns/1ns

module veinvh4_L2_R(outp, inp, en );
   output [1:0] outp;
   input [1:0] 	inp;
   input en;

   veinvh4_L3_L u1(outp[1], inp[1], en);
   veinvh4_L3_R u2(outp[0], inp[0], en);

endmodule
