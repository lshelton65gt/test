// last mod: Wed Dec 14 20:20:06 2005
// filename: test/cosim/bug5320.v
// Description:  This test is from bug 5320, at one time mvv would generate a bad
// error message about the explicitly disconnected port c of sub.

module bug5320(a, b, d);
   input [7:0] a;
   input [15:0] b;
   output [23:0] d;

   sub sub(.a(a),
	   .b(b),
	   .c(),
	   .d(d));
endmodule

