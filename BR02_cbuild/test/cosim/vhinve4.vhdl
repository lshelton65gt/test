entity sub is
  port (
    OUTP    : out bit;
    missing : out bit;
    inp, en : in  bit);
end sub;

architecture a of sub is
begin
  missing <= inp xor en;
  outP <= inp and en;
end a;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity sub2 is
  port (
    ouTP    : out std_logic;
    inp, en : in  std_logic);
end sub2;

architecture a of sub2 is
begin
  Outp <= inp and en;
end a;
