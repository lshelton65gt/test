library IEEE;
use IEEE.std_logic_1164.all;

entity test is
    port (i    : in bit;
          o    : out bit;
          c    : in bit);
end;

architecture arch of test is

begin
    process (c,i)
    begin
      o <= i;
    end process;

end;


