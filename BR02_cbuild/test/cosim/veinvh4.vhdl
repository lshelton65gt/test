-- test vhdl at top, several layers of verilog below, testing different
-- combinations of inclusion or missing timescale directives
-- the hierarhchy is:
-- all left modules (_L) have no timescale directive
-- all right modules (_R) have a timescale directive
--  veinvh4  (vhdl)
--    veinvh4_L1_L
--     veinvh4_L2_L
--      veinvh4_L3_L
--      veinvh4_L3_R
--     veinvh4_L2_R
--      veinvh4_L3_L
--      veinvh4_L3_R
--    veinvh4_L1_R
--     veinvh4_L2_L
--      veinvh4_L3_L
--      veinvh4_L3_R
--     veinvh4_L2_R
--      veinvh4_L3_L
--      veinvh4_L3_R


entity veinvh4 is

  port (
    out1     : out bit_vector(7 downto 0);
    in1      : in bit_vector(7 downto 0);
    en1      : in  bit);

end veinvh4;

architecture arch of veinvh4 is

  component veinvh4_L1_L
    port (
      outp    : out bit_vector(3 downto 0);
      inp     : in bit_vector(3 downto 0);
      en      : in  bit);
  end component;

  component veinvh4_L1_R
    port (
      outp    : out bit_vector(3 downto 0);
      inp     : in bit_vector(3 downto 0);
      en      : in  bit);
  end component;

  signal sbus : bit_vector(7 downto 0);

begin  -- arch

  S1 : veinvh4_L1_L port map (
    outp => sbus(7 downto 4),
    inp  => in1(7 downto 4),
    en   => en1);

  S2 : veinvh4_L1_R port map ( sbus(3 downto 0), in1(3 downto 0), en1 );

  out1 <= sbus;

end arch;
