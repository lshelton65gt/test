
module mid(outp, inp, en );
  output outp;
  input  inp, en;
   
  lower U1(outp, inp, en );
   
endmodule


module lower(outp, inp, en );
  output outp;
  input  inp, en;

   functions F1();
   
   // use a hierref to a function in a differen module
   assign outp = F1.andsub(en, inp);
//  assign outp = en & inp;

endmodule
module sub(outp, inp, en );
  output outp;
  input  inp, en;
   
  assign outp = inp & en;

endmodule

module functions();
   function andsub;
      input in, en;
      begin
	 andsub =  in | en;
      end
   endfunction
endmodule
   
