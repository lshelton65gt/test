-- this example is used to check that hierrefs to functions work when the top
-- level module is vhdl, inspired by bug 7592

entity veinvh8 is
  
  port (
    out1     : out bit_vector(1 downto 0);
    in1, en1 : in  bit);

end veinvh8;

architecture arch of veinvh8 is

  component sub
    port (
      outp    : out bit;
      inp, en : in  bit);
  end component;

  component mid
    port (
      outp    : out bit;
      inp, en : in  bit);
  end component;

  signal sbus : bit_vector(1 downto 0);
  
begin  -- arch

  S1 : sub port map (
    outp => sbus(1),
    inp  => en1,
    en   => in1);

  S2 : mid port map ( sbus(0), in1, en1 );

  out1 <= sbus;
  
end arch;
