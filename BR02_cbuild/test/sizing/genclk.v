
`timescale 1ns/1fs

module genclk(CLK);

   // faster (smaller period) - start w/ 100fs variance in period for now

   parameter HI_TIME = 0.320;
   parameter LO_TIME = 0.320;

   output CLK;

   reg 	  CLK;     // carbon depositSignal
   
`ifdef CARBON

   reg   dummy_in; // carbon depositSignal
   
   genclk_carbon genclk1 (.dummy_in (dummy_in));

   defparam genclk1.HI_TIME = HI_TIME;
   defparam genclk1.LO_TIME = LO_TIME;
//   defparam genclk1.HI_TIME = (HI_TIME * 10000);
//   defparam genclk1.LO_TIME = (LO_TIME * 10000);

`else

initial
  forever 
    begin: clk_gen
       CLK = 1'b0;
       #HI_TIME  CLK = 1'b1;
       #LO_TIME  CLK = 1'b0;         
    end
   
`endif // !ifdef CARBON
   
   
endmodule // genclk

`ifdef CARBON

module genclk_carbon (dummy_in);

   input dummy_in;

   parameter HI_TIME = 0.320;
   parameter LO_TIME = 0.320;

   // This module is implemented as a Carbon C-model.

endmodule // genclk_carbon

`endif //  `ifdef CARBON
 `timescale 1ns/1fs

 `define CLK200_HPERIOD   2.5
 `define SYS_HPERIOD      5
 `define RIO_HPERIOD      3.2
 `define FAST_HPERIOD     0.2
 `define SRIO_HPERIOD       0.3200
 `define SRIO_HPERIOD_SLOW  0.3201
 `define SRIO_HPERIOD_FAST  0.3199

 `define I2C_HPERIOD   31.25
 `define UART_HPERIOD  130
   
module sys;

   wire SYS_CLK;     // carbon observeSignal
   wire CLK200;      // carbon observeSignal
   wire RIO_CLK;     // carbon observeSignal
   wire I2C_CLK;     // carbon observeSignal
   wire UART_CLK;    // carbon observeSignal
   wire FAST_SYS_CLK;// carbon observeSignal
   wire SRIORX_CLK;  // carbon observeSignal
   wire SRIOTX_CLK1; // carbon observeSignal
   wire SRIOTX_CLK2; // carbon observeSignal
   wire SRIOTX_CLK3; // carbon observeSignal

   //====================================
   // CLK generator
   //====================================

   // SYS_CLK
   genclk
//     #(`SYS_HPERIOD,
//      `SYS_HPERIOD)
     #(`SYS_HPERIOD * 10000,
      `SYS_HPERIOD * 10000)
     clk_gen0 (.CLK(SYS_CLK));
  
   // CLK200
   genclk
     #(`CLK200_HPERIOD,
      `CLK200_HPERIOD)
     clk_gen (.CLK(CLK200));
   
//DEL   // RIO_CLK
//DEL   genclk
//DEL     #(`RIO_HPERIOD,
//DEL       `RIO_HPERIOD)
//DEL     clk_gen1 (.CLK(RIO_CLK));
   
   // I2C_CLK
   genclk
     #(`I2C_HPERIOD,
      `I2C_HPERIOD)
     i2c_gen (.CLK(I2C_CLK));
   
   // UART_CLK
   genclk
     #(`UART_HPERIOD,
      `UART_HPERIOD)
     uart_gen (.CLK(UART_CLK));
   
   // FAST_SYS_CLK
   genclk
     #(`FAST_HPERIOD,
      `FAST_HPERIOD)
     fast_clk (.CLK(FAST_SYS_CLK));
   
//DEL   // SRIORX_CLK - srio rx at nominal
//DEL   genclk
//DEL     #(`SRIO_HPERIOD,
//DEL       `SRIO_HPERIOD)
//DEL     clk_gen2b (.CLK(SRIORX_CLK));
//DEL
//DEL
//DEL   // SRIOTX_CLK - srio tx at nominal
//DEL   genclk
//DEL     #(`SRIO_HPERIOD,
//DEL       `SRIO_HPERIOD)
//DEL     clk_gen2a1 (.CLK(SRIOTX_CLK1));
//DEL   
//DEL   // SRIOTX_CLK - fast
//DEL   genclk
//DEL     #(`SRIO_HPERIOD,
//DEL      `SRIO_HPERIOD_FAST)
//DEL     clk_gen2a2 (.CLK(SRIOTX_CLK2));
//DEL   
//DEL   // SRIOTX_CLK - slow
//DEL   genclk
//DEL     #(`SRIO_HPERIOD,
//DEL       `SRIO_HPERIOD_SLOW)
//DEL     clk_gen2a3 (.CLK(SRIOTX_CLK3));

   sub1 #(`SYS_HPERIOD * 1000) s1 (UART_CLK);
   sub2 #(`SYS_HPERIOD) s2 (UART_CLK);

endmodule // sys

module sub1(clk);
   parameter NUM = 1;
   input clk;
   subx #(NUM) s (clk);
endmodule

module sub2(clk);
   parameter NUM = 1;
   input clk;
   subx #(NUM * 1000) s (clk);
endmodule

module subx(clk);
   parameter NUM = 1;
   input clk;
   reg 	 out; // carbon observeSignal

   initial
     $display("%m: NUM=%d", NUM);

endmodule
