/* C/C++ interface to cmodel. */
#include "cds_libbug5184_2_genclk_carbon.h"
#include "carbonsim/CarbonSimClock.h"
#include "carbonsim/CarbonSimMaster.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

struct genclk_carbon_inst {
  char *inst;          // instance name
  const char *hi_time;
  const char *lo_time;
};

extern __C__ void* cds_genclk_carbon_create(int numParams, CModelParam* cmodelParams, const char* inst)
{
  genclk_carbon_inst *hndl = new genclk_carbon_inst;
  hndl->inst = new char [strlen (inst) + 1];
  strcpy (hndl->inst, inst);
  hndl->hi_time = hndl->lo_time = "<none>";

  // Parse the parameters.
  for (int i = 0; i < numParams; i++) {
    if (!strcmp (cmodelParams[i].paramName, "HI_TIME")) {
      hndl->hi_time = cmodelParams[i].paramValue;
    }
    else
      if (!strcmp (cmodelParams[i].paramName, "LO_TIME")) {
	hndl->lo_time = cmodelParams[i].paramValue;
      }
  }


  printf ("genclk: %s => %s, %s\n", inst, hndl->hi_time, hndl->lo_time);

  return (void *) hndl;
}

extern __C__ void cds_genclk_carbon_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
{
}

extern __C__ void cds_genclk_carbon_run(void* hndl, CDSgenclk_carbonContext context
		, const UInt32* dummy_in // Input, size = 1 word(s)
	)
{
}

extern __C__ void cds_genclk_carbon_destroy(void* hndl)
{
}


