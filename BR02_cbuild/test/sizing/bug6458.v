
// Use of a localparam - bug 6458
module bug(o, i);
   output o;
   input [1:0] i;
   localparam foo = 4'b1010;

   assign      o = foo[i];
endmodule
