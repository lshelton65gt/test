// Generated with: ./rangen -s 60 -n 0x5baa9244 -o /work/cloutier/Wmaint2/obj/Linux.product/test/rangen/rangen/testcases.072606/cbld/assert2.v
// Using seed: 0x5baa9244
// Writing cyclic circuit of size 60 to file '/work/cloutier/Wmaint2/obj/Linux.product/test/rangen/rangen/testcases.072606/cbld/assert2.v'
// Module count = 8

// module EQIt
// size = 5 with 2 ports
module EQIt(jyYS, qorlT);
input jyYS;
input qorlT;

wire [1:6] jyYS;
wire qorlT;
reg EOR;
reg [0:1] Zh;
reg [5:42] YxT;
wire [10:25] PeQsq;
reg [22:25] tqdF;
Njg EFp (/* unconnected */, {qorlT,qorlT,jyYS[5:6],jyYS[5:6]});

always @(qorlT)
if (~qorlT)
begin
tqdF[23:24] = ~qorlT&qorlT;
end

BbYLX SLtQS ({qorlT,qorlT,qorlT,jyYS[5:6],qorlT}, qorlT, {qorlT,qorlT,qorlT}, qorlT, qorlT, {tqdF[23:24],jyYS[5:6],qorlT,qorlT,tqdF[23:24],tqdF[23:24]}, {tqdF[23:24],qorlT,tqdF[23:24],qorlT});

Njg THKz (/* unconnected */, {qorlT,tqdF[23:24],qorlT,tqdF[23:24]});

S U (qorlT, /* unconnected */, jyYS[5:6]);

BbYLX pLUPB ({jyYS[5:6],tqdF[23:24],jyYS[5:6]}, qorlT, {tqdF[23:24],1'b1}, qorlT, qorlT, {jyYS[5:6],qorlT,qorlT,qorlT,tqdF[23:24],qorlT,jyYS[5:6]}, {tqdF[23:24],tqdF[23:24],qorlT,qorlT});

BbYLX FsW ({jyYS[5:6],jyYS[5:6],qorlT,qorlT}, qorlT, {tqdF[23:24],qorlT}, qorlT, qorlT, {jyYS[5:6],qorlT,jyYS[5:6],qorlT,qorlT,qorlT,jyYS[5:6]}, {jyYS[5:6],qorlT,tqdF[23:24],qorlT});

gemjX KhmB (qorlT, {jyYS[5:6],qorlT,qorlT,jyYS[5:6],qorlT,tqdF[23:24],tqdF[23:24],qorlT,tqdF[23:24],qorlT,20'b01000001101101011111}, qorlT);

eMa Gh ({qorlT,tqdF[23:24],tqdF[23:24],qorlT}, qorlT, {jyYS[5:6],jyYS[5:6],qorlT,qorlT,qorlT});

assign PeQsq[12:19] = ~qorlT^tqdF[23:24];

always @(&tqdF[23:24])
if (tqdF[23:24])
begin
YxT[5:38] = jyYS[5:6];
end
else
begin
Zh[0] = ~jyYS[5:6];
end

Njg J (/* unconnected */, {tqdF[23:24],qorlT,jyYS[5:6],Zh[0]});

OPjjN tN ({Zh[0],jyYS[5:6],qorlT}, {PeQsq[12:19]}, {qorlT,jyYS[5:6]}, {Zh[0],jyYS[5:6],tqdF[23:24],jyYS[5:6],Zh[0],tqdF[23:24],qorlT,qorlT,jyYS[5:6]});

endmodule

// module BbYLX
// size = 33 with 7 ports
module BbYLX(on, sCY, ZTYaT, XPrJ, ZBAnx, npGBu, JuGzE);
input on;
input sCY;
input ZTYaT;
input XPrJ;
input ZBAnx;
input npGBu;
input JuGzE;

wire [2:7] on;
wire sCY;
wire [0:2] ZTYaT;
wire XPrJ;
wire ZBAnx;
wire [18:27] npGBu;
wire [1:6] JuGzE;
reg lD;
wire x;
reg [8:8] lut;
reg [5:7] A;
wire [35:84] BOp; // carbon observeSignal

reg [2:6] qvEr; // carbon observeSignal

wire [10:28] UzfS;
reg [2:5] FFUhJ;
reg VnrH;
reg [24:86] HxeAG;
reg [0:2] Va;
reg [7:23] Gji; // carbon observeSignal

reg [111:119] K_lQM [3:3]; // memory
wire P;
reg [1:4] t_Qz;
reg [3:29] SUQ; // carbon observeSignal

reg [17:19] FN;
reg XOTy;
reg [19:30] zC;
reg [19:24] UyYWZ;
reg [0:24] StZU;
reg [11:13] ttqt; // carbon observeSignal

reg [0:1] p [14:25]; // memory
reg [12:15] zJ;
reg rRZtv;
reg [3:3] j;
reg Ift;
always @(negedge &StZU[0:16])
begin :u
reg [0:7] xYYo; // carbon observeSignal

reg [5:6] jzo [0:3]; // memory
reg [1:2] Fzr;
reg AP_O;
reg TnImI; // carbon observeSignal


begin
TnImI = ~8'b01110110;
end
end

S O (Ift, UzfS[23:28], {2'b11});

assign x = 4'b1111;

assign x = on[6:7]|StZU[0:16];

always @(posedge &on[6:7])
begin :Z
reg [4:6] pcG;

begin
zJ[12:13] = {XPrJ,on[6:7]};
end
end

always @(posedge XPrJ)
begin
begin
end
end

always @(negedge &SUQ[14:20])
begin
begin
StZU[19:23] = ~StZU[3:9];
end
end

S L (sCY, UzfS[23:28], on[6:7]);

always @(posedge &SUQ[14:20])
begin
begin
zC[28:29] = StZU[3:9];
Va[1:2] = 7'b1111011;
end
end

cS Gc ({SUQ[14:20],sCY,1'b1}, {StZU[19:23],Ift});

always @(negedge &Va[1:2])
begin
begin
FFUhJ[2:3] = HxeAG[35:55];
end
end

always @(&SUQ[7:8])
if (~SUQ[7:8])
begin
StZU[11:16] = (sCY);
p[19]/* memuse */ = ~(sCY);
end
else
begin
StZU[22:24] = ~(ZTYaT[0:2]&on[6:7]);
end

always @(negedge &Gji[8:21])
begin
begin
StZU[22:24] = ZTYaT[0:2];
end
end

assign x = 4'b1100;

always @(posedge &FFUhJ[2:3])
begin
begin
ttqt[12] = ~StZU[22:24];
end
end

always @(negedge &UyYWZ[19:22])
begin
begin
VnrH = (~(~(StZU[0:16])));
end
end

always @(negedge on[3])
begin
begin
qvEr[3:4] = ~(~UyYWZ[19:22]);
end
end

always @(posedge &ZTYaT[0:2])
begin
begin
SUQ[14:19] = JuGzE[1:3]|zJ[12:13];
end
end

assign P = ~(~XPrJ&Gji[8:21]);

cS yo ({XPrJ,JuGzE[1:3],JuGzE[1:3],on[3],1'b0}, SUQ[14:19]);

always @(posedge &HxeAG[26:33])
begin :adF
reg [53:58] CnGd;
reg XO;
reg s;
reg [17:19] BY [1:2]; // memory
reg [4:122] KGLD;

begin
t_Qz[2:3] = ttqt[12];
end
end

always @(&StZU[9:22])
if (~StZU[9:22])
begin
StZU[11:16] = SUQ[7:8];
end

always @(&HxeAG[35:57])
if (HxeAG[35:57])
begin
p[19]/* memuse */ = StZU[22:24]^VnrH;
end
else
begin
p[19]/* memuse */ = {zJ[12:13],StZU[3:9]};
end

always @(&SUQ[14:20])
if (SUQ[14:20])
begin
FFUhJ[3:5] = SUQ[14:19]&Ift;
end

always @(posedge &UyYWZ[19:22])
begin
begin
ttqt[12] = JuGzE[1:3];
end
end

always @(&StZU[9:22])
if (~StZU[9:22])
begin
zC[28:29] = ~ttqt[12];
Gji[12:14] = ~zC[28:29];
end

always @(negedge ttqt[12])
begin :lVIZ
reg [14:30] YhEff;
reg [6:66] vT [2:6]; // memory
reg [1:4] KOroe;
reg [0:3] xfzq;

begin
Va[1:2] = ~t_Qz[2:3];
end
end

always @(posedge &p[19]/* memuse */)
begin
begin
lD = ~SUQ[14:19]|ZTYaT[0:2];
end
end

always @(posedge &npGBu[26:27])
begin
begin
StZU[11:16] = ~Gji[8:21];
end
end

always @(&StZU[3:9])
if (StZU[3:9])
begin
qvEr[5] = ~FFUhJ[3:5];
end

cS hN (K_lQM[3]/* memuse */, StZU[11:16]);

wire ozp = &on[6:7];
always @(posedge &FFUhJ[3:5] or negedge ozp)
if (~ozp)
begin
begin
if (StZU[3:9] == 7'b1101101)
begin
t_Qz[2:3] = ~K_lQM[3]/* memuse */;
end
else
begin
ttqt[12] = ~(~2'b01);
end
end
end
else
begin
begin
end
end

endmodule

// module Njg
// size = 22 with 2 ports
module Njg(kKx, lqFX);
output kKx;
input lqFX;

reg kKx;
wire [0:5] lqFX; // carbon observeSignal

reg [25:28] Q;
wire [86:126] maD; // carbon observeSignal

reg v;
reg [34:75] QYsf;
reg [0:1] gCaQ;
reg [21:31] g;
reg [6:12] XcADe;
reg [3:6] h_kOp;
reg [2:7] fB;
reg [4:7] mF;
reg [0:20] FuZv; // carbon observeSignal

reg [10:20] f;
reg [0:3] _kJup;
always @(posedge &QYsf[40:63])
begin :EOv

begin
_kJup[0:3] = maD[114]&QYsf[38:63];
end
end

always @(posedge &lqFX[1:2])
begin
begin
Q[27:28] = ~QYsf[40:63];
end
end

always @(posedge &_kJup[0:3])
begin
begin
end
end

S lx (maD[114], /* unconnected */, lqFX[1:2]);

eMa krYZG ({lqFX[1:2],lqFX[1:2],maD[114],1'b0}, maD[114], QYsf[66:72]);

eMa yHr ({maD[114],_kJup[0:3],1'b0}, maD[114], {maD[114],lqFX[1:2],maD[114],maD[114],2'b10});

OPjjN _bG_x ({lqFX[1:2],maD[114],maD[114]}, maD[86:95], {_kJup[1:2],maD[114]}, {maD[114],maD[114],_kJup[1:2],maD[117:123],maD[114],_kJup[1:2]});

always @(posedge maD[114])
begin :qW
reg [13:31] MUKt; // carbon observeSignal


begin
v = ~(maD[114]&lqFX[1:2]);
end
end

always @(&maD[90:116])
if (~maD[90:116])
begin
end

eMa JC ({_kJup[1:2],Q[27:28],maD[114],1'b0}, maD[114], {v,_kJup[1:2],Q[27:28],lqFX[1:2]});

assign maD[95:101] = 2'b00;

always @(posedge &_kJup[0:3])
begin
begin
XcADe[9:10] = QYsf[57:67];
end
end

assign maD[107:118] = 1'b1;

always @(posedge &QYsf[40:63])
begin
begin
kKx = maD[90:116];
end
end

gemjX io (v, {kKx,maD[117:123],QYsf[36:44],maD[117:123],maD[114],lqFX[1:2],_kJup[1:2],6'b110111}, kKx);

always @(posedge v)
begin
begin
end
end

always @(negedge v)
begin :_Cx
reg [2:7] hiaMt;
reg tLp;
reg [2:25] prlK;
reg lDEz;

begin
if (QYsf[36:44] == 9'b101100111)
begin
QYsf[67:69] = (~(_kJup[1:2]&maD[90:118]));
end
end
end

cS Jdml (QYsf[36:44], {v,maD[114],maD[114],3'b001});

always @(posedge &maD[90:118])
begin
begin
if (maD[117:123] == 7'b0010000)
begin
QYsf[37:68] = QYsf[36:44];
end
else
begin
end
end
end

wire tlelJ = &XcADe[9:10];
always @(negedge &QYsf[36:44] or posedge tlelJ)
if (tlelJ)
begin :LXzez
reg rYfoM;
reg [2:2] ISUDz;
reg [2:6] x;

begin
QYsf[50:55] = ~4'b0001;
end
end
else
begin
begin
mF[4:6] = ~QYsf[50:55]^_kJup[1:2];
end
end

always @(&QYsf[66:72])
if (~QYsf[66:72])
begin
_kJup[2:3] = maD[86:109]^Q[27:28];
end

wire tfi = &mF[4:6];
always @(posedge &QYsf[38:63] or negedge tfi)
if (~tfi)
begin
begin
gCaQ[0:1] = (~maD[90:116]^maD[90:118]) + (~9'b000100101);
end
end
else
begin
begin
QYsf[37:68] = ~lqFX[1:2];
end
end

always @(&maD[90:116])
if (maD[90:116])
begin
end

eMa Jm (QYsf[50:55], maD[114], QYsf[66:72]);

S qD (maD[114], /* unconnected */, _kJup[2:3]);

always @(&QYsf[40:63])
if (QYsf[40:63])
begin
QYsf[37:68] = Q[27:28];
end

assign maD[89:97] = ~maD[86:109];

assign maD[103:119] = 9'b010101101;

always @(&QYsf[38:63])
if (QYsf[38:63])
begin
_kJup[2:3] = QYsf[66:72]&maD[86:109];
end

assign maD[103:119] = ~(QYsf[50:55]^kKx);

endmodule

// module eMa
// size = 2 with 3 ports
module eMa(qGPYd, voRQt, HVWE);
input qGPYd;
input voRQt;
input HVWE;

wire [1:6] qGPYd;
wire voRQt;
wire [0:6] HVWE;
reg WAA;
reg [0:1] q;
reg [9:16] z;
reg VQAh;
reg S;
reg [1:29] ax;
reg [0:1] bWSO;
reg [18:36] JQq;
reg U;
reg [7:25] CPWjO; // carbon observeSignal

wire [4:4] BQx;
always @(qGPYd[4])
if (qGPYd[4])
begin
end
else
begin
case (VQAh)
1'd0:
begin
ax[19:25] = {qGPYd[4],CPWjO[7:17]};
end
1'd1:
begin
JQq[24:28] = BQx[4];
end
default:
begin
end
endcase
end

endmodule

// module OPjjN
// size = 5 with 4 ports
module OPjjN(j_yH, eT, MS, Bt);
input j_yH;
output eT;
input MS;
input Bt;

wire [1:4] j_yH;
reg [12:21] eT;
wire [3:5] MS;
wire [6:19] Bt;
reg [0:0] jZ [18:26]; // memory
reg [2:3] dAqp;
wire [3:6] g;
assign g[3:4] = j_yH[1:2]^MS[4];

assign g[3:4] = Bt[6:13]|MS[4];

always @(&Bt[6:13])
if (~Bt[6:13])
begin
jZ[26]/* memuse */ = ~eT[14:21]^j_yH[1:2];
end
else
begin
eT[15:16] = ~3'b001;
end

wire gkER = MS[4];
always @(negedge &j_yH[1:2] or negedge gkER)
if (~gkER)
begin
begin
dAqp[3] = eT[15:16]^MS[4];
end
end
else
begin
begin
dAqp[3] = ~eT[15:16];
end
end

endmodule

// module gemjX
// size = 2 with 3 ports
module gemjX(Y, UWtj, Q);
input Y;
input UWtj;
input Q;

wire Y;
wire [14:48] UWtj;
wire Q;
reg hEQt;
reg [1:5] YBk;
reg [1:23] Zi;
reg PcEuh;
reg [5:19] N;
reg [18:28] XsHMN;
reg [4:5] t;
reg g;
reg F;
reg [0:5] s; // carbon observeSignal

reg fSKM;
reg [0:7] OoZRg;
reg cG;
reg [24:25] FgG;
reg bLeeM;
reg [0:1] jpGBz; // carbon observeSignal

reg [6:99] GllRD;
reg [3:9] J;
reg TmIDC;
reg LTIp;
wire Rvo; // carbon observeSignal

always @(posedge Y)
begin
begin
XsHMN[18:27] = (~GllRD[19:97]);
end
end

always @(posedge &XsHMN[21:26])
begin
begin
GllRD[9:71] = ~Q&F;
end
end

endmodule

// module cS
// size = 2 with 2 ports
module cS(qMa, Hu);
input qMa;
input Hu;

wire [10:18] qMa;
wire [13:18] Hu;
wire aLesL;
wire [4:7] cAh;
reg [2:3] Hb;
assign cAh[5] = (~(Hu[15:16]|qMa[11:16]) + (10'b1001100010));

assign cAh[5] = qMa[11:16];

endmodule

// module S
// size = 2 with 3 ports
module S(X, oht, o);
input X;
output oht;
input o;

wire X;
reg [2:7] oht;
wire [3:4] o;
reg [1:6] INSxV;
reg [0:0] MXqn [2:5]; // memory
reg [29:30] LPyy;
wire [8:31] Qs;
always @(&Qs[30:31])
if (~Qs[30:31])
begin
oht[4] = (1'b0);
end

always @(posedge &Qs[30:31])
begin
begin
oht[2:4] = ~oht[4]^Qs[12:25];
end
end

endmodule
