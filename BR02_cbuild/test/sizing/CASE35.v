/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Case items having list of identifiers or numbers
// Section 5.3.21 of the test plan
module CASE35(cond,in1,in2,cond1,cond2,cond3,out1);
input [2:0] in1,in2,cond,cond1,cond2,cond3;
output [2:0] out1;
reg [2:0] out1;

  always @(cond or cond1 or cond2 or cond3 or in1 or in2)
  begin
    case(cond)
      0,1,2 : out1= in1 & in2;
      cond1,~cond2 : out1 = in1 | in2;
      4,5,cond2,cond3 : out1 = in1 ^ in2;
      default : out1 = ~in1;
    endcase
  end
endmodule
