module top (clk);
   input clk;
   
   sub #("dv") sub(clk);
endmodule // top

module sub(clk);
   input clk;

   parameter [9:0] myParam = 1;
   
   always @(posedge clk)
     $display("%b", myParam);
endmodule // sub

`ifdef SIMULATE
module carbon_test_driver;
   reg clk;

   top top(clk);

   initial
     begin
        clk = 0;

        #10
          clk = 1;

        #10 clk = 0;
     end
endmodule // test
`endif //  `ifdef SIMULATE
