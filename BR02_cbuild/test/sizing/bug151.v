module bug (clk,rst,in,out);
  input clk;
  input rst;
  input [35:0] in;
  output [35:0] out;
  reg [35:0] out;
   
  always @(posedge clk or posedge rst) 
  begin
    if (rst)
      out <= {35{1'b0}};
    else
      out <= in;
  end

endmodule //Bug

