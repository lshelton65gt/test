module top(bar);
   output [31:0] bar;
   
  sub #(5) sub(bar);
endmodule

module sub(bar);
   output [31:0] bar;
   reg [31:0]    bar;
   
   parameter foo = 9;
   integer   i;
    
   initial begin
     bar = 0;
     for (i = 0; i < foo; i = i + 1)
	bar = bar + foo[i];
   end
endmodule

