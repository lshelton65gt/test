#include "carbon/carbon.h"
#include "carbon/carbon_capi.h"

#include "carbonsim/CarbonSimClock.h"
#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimStep.h"
#include <cassert>
#include <cstdio>
#include <csignal>

extern "C" CarbonObjectID* carbon_bug5184_2_create (CarbonDBType dbType,
                                                    CarbonInitFlags flags);

int main (int argc, char **argv)
{
  CarbonSimMaster *SimMaster = CarbonSimMaster::create();

  CarbonSimObjectType *type_sys =
    SimMaster->addObjectType ("sys", carbon_bug5184_2_create);
  CarbonSimObjectInstance *sys = SimMaster->addInstance (type_sys, "sys");

  SimMaster->run ();
}
