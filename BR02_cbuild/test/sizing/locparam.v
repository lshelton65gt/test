`define DEPTH 13

module dut(o, i, clk);
   output o;
   input  i;
   input  clk;

   localparam [2:0] DEPTH = `DEPTH;

   sub #(DEPTH) s (o, i, clk);
endmodule

module sub(o, i, clk);
   parameter DEPTH = 2;
   output o;
   input  i;
   input  clk;

   reg    o;
   reg [DEPTH-1:0] queue;

   initial queue = 0;
   

   
   always @(negedge clk)
     queue = queue <<< 1;
   
   always @(posedge clk) begin
      o <= queue[DEPTH - 1];
      queue[0] <= i;
   end
endmodule
