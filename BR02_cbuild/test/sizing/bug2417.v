/*****************************************************************************
(c) 2002-2003, Conexant Systems, Inc., all rights reserved
$Project: Mojo_AV_Core$
$Header: /cvs/repository/test/sizing/bug2417.v,v 1.1 2004/07/10 02:15:33 mark Exp $
Description:
Copied from E4
*****************************************************************************/

module cordic_top(
		// inputs
		clk27,
		resetn,
		en_in,
		I_in,
		Q_in,
		cordic_shift,
		// outputs
		dt_out,
		en_out
		);

parameter COORD_WIDTH = 14;
parameter COORD_Q_WIDTH = 3;
parameter COORD_F_WIDTH = 11;
parameter ANGIN_WIDTH = 16;
parameter ANGOUT_WIDTH = 22;
parameter LOOP_WIDTH = 4;
parameter PREC_WIDTH = 5;

parameter MAX_COUNT = 12;

parameter QUAD0_OFF = 16'h0000;
parameter QUAD1_OFF = 16'h1680;
parameter QUAD2_OFF = 16'h2d00;
parameter QUAD3_OFF = 16'h4380;

parameter NEG_ROT = - 22'h002d00;
parameter POS_ROT =   22'h002d00;
/*
parameter ATAN0 = 16'h1680;
parameter ATAN1 = 16'h0d48;
parameter ATAN2 = 16'h0704;
parameter ATAN3 = 16'h0390;
parameter ATAN4 = 16'h01c9;
parameter ATAN5 = 16'h00e5;
parameter ATAN6 = 16'h0072;
parameter ATAN7 = 16'h0039;
parameter ATAN8 = 16'h001c;
parameter ATAN9 = 16'h000e;
parameter ATAN10 = 16'h0007;
parameter ATAN11 = 16'h0003;
*/

parameter ATAN0 = 16'h0b40;
parameter ATAN1 = 16'h06a4;
parameter ATAN2 = 16'h0382;
parameter ATAN3 = 16'h01c8;
parameter ATAN4 = 16'h00e4;
parameter ATAN5 = 16'h0072;
parameter ATAN6 = 16'h0039;
parameter ATAN7 = 16'h001c;
parameter ATAN8 = 16'h000e;
parameter ATAN9 = 16'h0007;
parameter ATAN10 = 16'h0003;
parameter ATAN11 = 16'h0001;

parameter DDT_DATA_WIDTH = 22;

parameter DDT_Q_WIDTH = 17;
parameter DDT_F_WIDTH = 5;

parameter DDT_NUM_COEFF   = 8;
parameter DDT_COEFF_WIDTH = 22;
//parameter DDT_COEFF_WIDTH = 13;
parameter DDT_NUM_TAPS = 15;
parameter DDT_ACCUM_CNT_WIDTH = 4;
parameter DDT_MULT_WIDTH = DDT_COEFF_WIDTH + DDT_DATA_WIDTH;
//parameter DDT_ACCUM_WIDTH = DDT_COEFF_WIDTH + DDT_DATA_WIDTH + DDT_NUM_COEFF;
parameter DDT_ACCUM_WIDTH = DDT_DATA_WIDTH;

/*
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF0 = 13'h002f;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF1 = - 13'h0056;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF2 = 13'h00cf;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF3 = - 13'h01c0;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF4 = 13'h036d;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF5 = - 13'h069d;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF6 = 13'h0f45;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF7 = 13'h0000;
*/

parameter    DDT_CF0 = 22'h000000;
parameter    DDT_CF1 = 22'h000001; // 1
parameter    DDT_CF2 = 22'h3fffd5; //-43
parameter    DDT_CF3 = 22'h00012d; //301
parameter    DDT_CF4 = 22'h3ffb51; //-1199
parameter    DDT_CF5 = 22'h000fff; //4095

/*
parameter    DDT_CF0 = 22'h000000;
parameter    DDT_CF1 = 22'h3fffff;
parameter    DDT_CF2 = 22'h000002;
parameter    DDT_CF3 = 22'h3ffffc;
parameter    DDT_CF4 = 22'h000007;
parameter    DDT_CF5 = 22'h3ffff3;
parameter    DDT_CF6 = 22'h00001f;
parameter    DDT_CF7 = 22'h000000;
*/



parameter IIRFX_DATA_WIDTH = 16;
parameter IIRFX_COEFF_WIDTH = 16;
parameter IIRFX_DATA_SEG_WIDTH = 8;
parameter IIRFX_COEFF_SEG_WIDTH = 8;
parameter IIRFX_TMP_NUM = 2;
parameter IIRFX_ADD_WIDTH = 16;
parameter IIRFX_MULT_WIDTH = IIRFX_COEFF_SEG_WIDTH + IIRFX_DATA_SEG_WIDTH;
//parameter IIRFX_MULT_WIDTH = IIRFX_COEFF_SEG_WIDTH + IIRFX_DATA_SEG_WIDTH;
parameter IIRFX_MULT_SEG_WIDTH = IIRFX_COEFF_WIDTH + IIRFX_DATA_WIDTH;
parameter IIRFX_AP_COEFF_CNT_WIDTH = 4;
parameter IIRFX_AP_DATA_CNT_WIDTH = 4;
parameter IIRFX_AP_ADD_CNT_WIDTH = 2;
parameter IIRFX_AP_MULT_CNT_WIDTH = 4;
parameter IIRFX_COEFF_SEG = 4;
//parameter IIRFX_DATA_SEG = 8;
parameter  [IIRFX_AP_DATA_CNT_WIDTH-1:0] IIRFX_DATA_SEG = 4;
parameter  [IIRFX_AP_DATA_CNT_WIDTH-1:0] IIRFX_TOTAL_CNT = 7;
parameter IIRFX_ADD_SEG = 4;
parameter IIRFX_MULT_SEG = 4;
parameter IIRFX_SUB_CYC = IIRFX_COEFF_SEG + 3*IIRFX_DATA_SEG;

parameter IIRFX1_STR_NUM = 6;
parameter IIRFX1_AP_NUM = 9;
parameter IIRFX1_AP_PATH_CNT_WIDTH = 5;
parameter [IIRFX1_AP_PATH_CNT_WIDTH-1:0] IIRFX1_PATH_CYC = 16;


parameter IIRFX1_CF0 = 16'h0c7c;
parameter IIRFX1_CF1 = 16'h023f;
parameter IIRFX1_CF2 = 16'h06ed;
parameter IIRFX1_CF3 = 16'h01ad;
parameter IIRFX1_CF4 = - 16'h02a7;
parameter IIRFX1_CF5 = 16'h0186;
parameter IIRFX1_CF6 = - 16'h08f1;

parameter IIRFX2_STR_NUM = 3;
parameter IIRFX2_AP_NUM = 6;
parameter IIRFX2_AP_PATH_CNT_WIDTH = 4;
parameter [IIRFX2_AP_PATH_CNT_WIDTH-1:0] IIRFX2_PATH_CYC = 10;

parameter IIRFX2_CF0 = 16'h0a97;
parameter IIRFX2_CF1 =  - 16'h02b1;
parameter IIRFX2_CF2 = 16'h0a00;
parameter IIRFX2_CF3 = - 16'h0063;

parameter IIRFX4_STR_NUM = 8;
parameter IIRFX4_AP_NUM = 10;
parameter IIRFX4_AP_PATH_CNT_WIDTH = 6;
parameter [IIRFX4_AP_PATH_CNT_WIDTH-1:0] IIRFX4_PATH_CYC = 10;


parameter IIRFX4_AP_CF0 = 16'h0c69;
parameter IIRFX4_AP_CF1 = 16'h0de4;
parameter IIRFX4_AP_CF2 = 16'h01cf;

parameter IIRFX4_STR_CF0 = 16'h0aca;
parameter IIRFX4_STR_CF1 = 16'h019f;


parameter IIRFX3_STR_NUM = 21;
parameter IIRFX3_AP_NUM = 26;
parameter IIRFX3_AP_PATH_CNT_WIDTH = 5;
parameter IIRFX3_PATH_CYC = 26;

parameter   IIRFX3_AP_CF0 = 16'h0dba;
parameter   IIRFX3_AP_CF1 = 16'h04c0;
parameter   IIRFX3_AP_CF2 = 16'h0e65;
parameter   IIRFX3_AP_CF3 = 16'h03f5;
parameter   IIRFX3_AP_CF4 = 16'h0667;
parameter   IIRFX3_AP_CF5 = 16'h03b2;
parameter   IIRFX3_AP_CF6 = 16'hfdc3;    // - 16'h023d;
parameter   IIRFX3_AP_CF7 = 16'h0396;
// parameter   [IIRFX_COEFF_WIDTH-1:0] IIRFX3_AP_CF8 = 16'hf617;    // - 16'h09e9;
parameter   IIRFX3_AP_CF8 = 16'hf617;    // - 16'h09e9;
parameter   IIRFX3_AP_CF9 = 16'h038b;
parameter   IIRFX3_AP_CF10 = 16'hf18e;   // - 16'h0e72;


parameter   MUX_DATA_WIDTH = 14;

input	            	clk27;
input	            	resetn;
input	            	en_in;
input [COORD_WIDTH-1:0]   	I_in;
input [COORD_WIDTH-1:0]   	Q_in;
input [2:0]		   	cordic_shift;

output [COORD_WIDTH-1:0]       	dt_out;
output	            		en_out;


wire cordic_en_out;
wire [ANGOUT_WIDTH-1:0]        fix_out;
wire [ANGOUT_WIDTH-1:0]       	a_out;
wire [COORD_WIDTH-1:0]		ddt_out;

cordic cordic_inst(
		// inputs
		.clk27(clk27),
		.resetn(resetn),
		.en_in(en_in),
		.a_in({COORD_WIDTH{1'b0}}),
		.x_in(I_in),
		.y_in(Q_in),
		// outputs
		.a_out(a_out),
		.fix_out(fix_out),
		.en_out(cordic_en_out)
		);

ddt  ddt_inst(
	 	// inputs
		.clk27(clk27),
		.resetn(resetn),
		.en_in(cordic_en_out),
		.ddt_in(a_out),
		.fix_in(fix_out),
		.cordic_shift(cordic_shift),
		// outputs
		.ddt_out(ddt_out),
		.en_out(en_out)
		);


//assign dt_out = ddt_out[DDT_DATA_WIDTH-1 : DDT_DATA_WIDTH-COORD_WIDTH];
assign dt_out = ddt_out;


endmodule

module ddt(
	 	// inputs
		clk27,
		resetn,
		en_in,
		ddt_in,
		fix_in,
		cordic_shift,
		// outputs
		ddt_out,
		en_out
		);

parameter COORD_WIDTH = 14;
parameter COORD_Q_WIDTH = 3;
parameter COORD_F_WIDTH = 11;
parameter ANGIN_WIDTH = 16;
parameter ANGOUT_WIDTH = 22;
parameter LOOP_WIDTH = 4;
parameter PREC_WIDTH = 5;

parameter MAX_COUNT = 12;

parameter QUAD0_OFF = 16'h0000;
parameter QUAD1_OFF = 16'h1680;
parameter QUAD2_OFF = 16'h2d00;
parameter QUAD3_OFF = 16'h4380;

parameter NEG_ROT = - 22'h002d00;
parameter POS_ROT =   22'h002d00;
/*
parameter ATAN0 = 16'h1680;
parameter ATAN1 = 16'h0d48;
parameter ATAN2 = 16'h0704;
parameter ATAN3 = 16'h0390;
parameter ATAN4 = 16'h01c9;
parameter ATAN5 = 16'h00e5;
parameter ATAN6 = 16'h0072;
parameter ATAN7 = 16'h0039;
parameter ATAN8 = 16'h001c;
parameter ATAN9 = 16'h000e;
parameter ATAN10 = 16'h0007;
parameter ATAN11 = 16'h0003;
*/

parameter ATAN0 = 16'h0b40;
parameter ATAN1 = 16'h06a4;
parameter ATAN2 = 16'h0382;
parameter ATAN3 = 16'h01c8;
parameter ATAN4 = 16'h00e4;
parameter ATAN5 = 16'h0072;
parameter ATAN6 = 16'h0039;
parameter ATAN7 = 16'h001c;
parameter ATAN8 = 16'h000e;
parameter ATAN9 = 16'h0007;
parameter ATAN10 = 16'h0003;
parameter ATAN11 = 16'h0001;

parameter DDT_DATA_WIDTH = 22;

parameter DDT_Q_WIDTH = 17;
parameter DDT_F_WIDTH = 5;

parameter DDT_NUM_COEFF   = 8;
parameter DDT_COEFF_WIDTH = 22;
//parameter DDT_COEFF_WIDTH = 13;
parameter DDT_NUM_TAPS = 15;
parameter DDT_ACCUM_CNT_WIDTH = 4;
parameter DDT_MULT_WIDTH = DDT_COEFF_WIDTH + DDT_DATA_WIDTH;
//parameter DDT_ACCUM_WIDTH = DDT_COEFF_WIDTH + DDT_DATA_WIDTH + DDT_NUM_COEFF;
parameter DDT_ACCUM_WIDTH = DDT_DATA_WIDTH;

/*
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF0 = 13'h002f;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF1 = - 13'h0056;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF2 = 13'h00cf;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF3 = - 13'h01c0;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF4 = 13'h036d;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF5 = - 13'h069d;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF6 = 13'h0f45;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF7 = 13'h0000;
*/

parameter    DDT_CF0 = 22'h000000;
parameter    DDT_CF1 = 22'h000001; // 1
parameter    DDT_CF2 = 22'h3fffd5; //-43
parameter    DDT_CF3 = 22'h00012d; //301
parameter    DDT_CF4 = 22'h3ffb51; //-1199
parameter    DDT_CF5 = 22'h000fff; //4095

/*
parameter    DDT_CF0 = 22'h000000;
parameter    DDT_CF1 = 22'h3fffff;
parameter    DDT_CF2 = 22'h000002;
parameter    DDT_CF3 = 22'h3ffffc;
parameter    DDT_CF4 = 22'h000007;
parameter    DDT_CF5 = 22'h3ffff3;
parameter    DDT_CF6 = 22'h00001f;
parameter    DDT_CF7 = 22'h000000;
*/



parameter IIRFX_DATA_WIDTH = 16;
parameter IIRFX_COEFF_WIDTH = 16;
parameter IIRFX_DATA_SEG_WIDTH = 8;
parameter IIRFX_COEFF_SEG_WIDTH = 8;
parameter IIRFX_TMP_NUM = 2;
parameter IIRFX_ADD_WIDTH = 16;
parameter IIRFX_MULT_WIDTH = IIRFX_COEFF_SEG_WIDTH + IIRFX_DATA_SEG_WIDTH;
//parameter IIRFX_MULT_WIDTH = IIRFX_COEFF_SEG_WIDTH + IIRFX_DATA_SEG_WIDTH;
parameter IIRFX_MULT_SEG_WIDTH = IIRFX_COEFF_WIDTH + IIRFX_DATA_WIDTH;
parameter IIRFX_AP_COEFF_CNT_WIDTH = 4;
parameter IIRFX_AP_DATA_CNT_WIDTH = 4;
parameter IIRFX_AP_ADD_CNT_WIDTH = 2;
parameter IIRFX_AP_MULT_CNT_WIDTH = 4;
parameter IIRFX_COEFF_SEG = 4;
//parameter IIRFX_DATA_SEG = 8;
parameter  [IIRFX_AP_DATA_CNT_WIDTH-1:0] IIRFX_DATA_SEG = 4;
parameter  [IIRFX_AP_DATA_CNT_WIDTH-1:0] IIRFX_TOTAL_CNT = 7;
parameter IIRFX_ADD_SEG = 4;
parameter IIRFX_MULT_SEG = 4;
parameter IIRFX_SUB_CYC = IIRFX_COEFF_SEG + 3*IIRFX_DATA_SEG;

parameter IIRFX1_STR_NUM = 6;
parameter IIRFX1_AP_NUM = 9;
parameter IIRFX1_AP_PATH_CNT_WIDTH = 5;
parameter [IIRFX1_AP_PATH_CNT_WIDTH-1:0] IIRFX1_PATH_CYC = 16;


parameter IIRFX1_CF0 = 16'h0c7c;
parameter IIRFX1_CF1 = 16'h023f;
parameter IIRFX1_CF2 = 16'h06ed;
parameter IIRFX1_CF3 = 16'h01ad;
parameter IIRFX1_CF4 = - 16'h02a7;
parameter IIRFX1_CF5 = 16'h0186;
parameter IIRFX1_CF6 = - 16'h08f1;

parameter IIRFX2_STR_NUM = 3;
parameter IIRFX2_AP_NUM = 6;
parameter IIRFX2_AP_PATH_CNT_WIDTH = 4;
parameter [IIRFX2_AP_PATH_CNT_WIDTH-1:0] IIRFX2_PATH_CYC = 10;

parameter IIRFX2_CF0 = 16'h0a97;
parameter IIRFX2_CF1 =  - 16'h02b1;
parameter IIRFX2_CF2 = 16'h0a00;
parameter IIRFX2_CF3 = - 16'h0063;

parameter IIRFX4_STR_NUM = 8;
parameter IIRFX4_AP_NUM = 10;
parameter IIRFX4_AP_PATH_CNT_WIDTH = 6;
parameter [IIRFX4_AP_PATH_CNT_WIDTH-1:0] IIRFX4_PATH_CYC = 10;


parameter IIRFX4_AP_CF0 = 16'h0c69;
parameter IIRFX4_AP_CF1 = 16'h0de4;
parameter IIRFX4_AP_CF2 = 16'h01cf;

parameter IIRFX4_STR_CF0 = 16'h0aca;
parameter IIRFX4_STR_CF1 = 16'h019f;


parameter IIRFX3_STR_NUM = 21;
parameter IIRFX3_AP_NUM = 26;
parameter IIRFX3_AP_PATH_CNT_WIDTH = 5;
parameter IIRFX3_PATH_CYC = 26;

parameter   IIRFX3_AP_CF0 = 16'h0dba;
parameter   IIRFX3_AP_CF1 = 16'h04c0;
parameter   IIRFX3_AP_CF2 = 16'h0e65;
parameter   IIRFX3_AP_CF3 = 16'h03f5;
parameter   IIRFX3_AP_CF4 = 16'h0667;
parameter   IIRFX3_AP_CF5 = 16'h03b2;
parameter   IIRFX3_AP_CF6 = 16'hfdc3;    // - 16'h023d;
parameter   IIRFX3_AP_CF7 = 16'h0396;
// parameter   [IIRFX_COEFF_WIDTH-1:0] IIRFX3_AP_CF8 = 16'hf617;    // - 16'h09e9;
parameter   IIRFX3_AP_CF8 = 16'hf617;    // - 16'h09e9;
parameter   IIRFX3_AP_CF9 = 16'h038b;
parameter   IIRFX3_AP_CF10 = 16'hf18e;   // - 16'h0e72;


parameter   MUX_DATA_WIDTH = 14;


input				clk27;
input				resetn;
input				en_in;
input [DDT_DATA_WIDTH-1:0]	ddt_in;
input [DDT_DATA_WIDTH-1:0]	fix_in;
input [2:0]		   	cordic_shift;

output [COORD_WIDTH-1:0]	ddt_out;
output				en_out;

reg [DDT_COEFF_WIDTH-1:0] 		curr_coeff;

reg [DDT_DATA_WIDTH-1:0]		samp_reg [0:DDT_NUM_TAPS-1];
reg [DDT_DATA_WIDTH-1:0]		hold_reg;

//reg [DDT_ACCUM_WIDTH - 1 :0]		tap_accum;
reg [DDT_ACCUM_WIDTH  :0]		tap_accum;

reg [DDT_ACCUM_CNT_WIDTH-1:0] 		accum_cnt;
reg [DDT_ACCUM_CNT_WIDTH-1:0] 		accum_cnt_d;
reg en_in_d;

//reg [DDT_DATA_WIDTH-1:0]		ddt_out_int;
//wire [DDT_DATA_WIDTH-1:0]		ddt_out_int_x;
reg [DDT_DATA_WIDTH:0]		ddt_out_int;
wire [DDT_DATA_WIDTH:0]		ddt_out_int_x;

reg  [COORD_WIDTH-1:0]			ddt_out;
reg  [COORD_WIDTH-1:0]			ddt_out_x;
reg  [DDT_DATA_WIDTH-1:0] 		curr_tap;

wire 					coeff_sign;
reg 					coeff_sign_d;
wire [DDT_COEFF_WIDTH-1:0]              coeff_mag;
wire [DDT_COEFF_WIDTH-1:0]              coeff_mag_wsb;
wire 					data_sign;
reg 					data_sign_d;
wire [DDT_DATA_WIDTH-1:0]              	data_mag;
wire [DDT_DATA_WIDTH-1:0]              	sum_tap_fix;
wire [DDT_DATA_WIDTH-1:0]              	fix_tap;
wire [DDT_DATA_WIDTH-1:0]              	fix_tap_curr;
wire [DDT_DATA_WIDTH-1:0]              	data_mag_wsb;
//wire [DDT_MULT_WIDTH-1:0]		tap_mult;
//reg [DDT_MULT_WIDTH-1:0]		tap_mult_d;
//wire [DDT_DATA_WIDTH-1:0]		tap_mult_signed;
//wire [DDT_DATA_WIDTH-1:0]		pre_mult_signed;
wire [DDT_MULT_WIDTH:0]		tap_mult;
reg [DDT_MULT_WIDTH:0]		tap_mult_d;
wire [DDT_DATA_WIDTH:0]		tap_mult_signed;
wire [DDT_DATA_WIDTH:0]		pre_mult_signed;




always @(posedge clk27)
  begin
  if(~resetn)
    begin
    samp_reg [0] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [1] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [2] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [3] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [4] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [5] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [6] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [7] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [8] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [9] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [10] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [11] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [12] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [13] <= {DDT_DATA_WIDTH{1'b0}}; 
    samp_reg [14] <= {DDT_DATA_WIDTH{1'b0}}; 
    hold_reg <= {DDT_DATA_WIDTH{1'b0}};
    end
  else if(en_in)
    begin
    samp_reg[0] <= ddt_in; 
    hold_reg <= samp_reg[0];
    end
  else
    begin
    case(accum_cnt)
      
    
      4'h1 : 
        begin
        samp_reg[1] <= fix_tap; 
        hold_reg <= samp_reg[1];
        end
    
    
      4'h2 : 
        begin
        samp_reg[2] <= fix_tap; 
        hold_reg <= samp_reg[2];
        end
    
      4'h3 : 
        begin
        samp_reg[3] <= fix_tap; 
        hold_reg <= samp_reg[3];
        end
    
    
      4'h4 : 
        begin
        samp_reg[4] <= fix_tap; 
        hold_reg <= samp_reg[4];
        end
    
    
      4'h5 : 
        begin
        samp_reg[5] <= fix_tap; 
        hold_reg <= samp_reg[5];
        end
      
      4'h6 : 
        begin
        samp_reg[6] <= fix_tap; 
        hold_reg <= samp_reg[6];
        end
    
    
      4'h7 : 
        begin
        samp_reg[7] <= fix_tap; 
        hold_reg <= samp_reg[7];
        end
    
     
      4'h8 : 
        begin
        samp_reg[8] <= fix_tap; 
        hold_reg <= samp_reg[8];
        end
    
    
      4'h9 : 
        begin
        samp_reg[9] <= fix_tap; 
        hold_reg <= samp_reg[9];
        end
    
    
      4'ha : 
        begin
        samp_reg[10] <= fix_tap; 
        hold_reg <= samp_reg[10];
        end
    
      4'hb : 
        begin
        samp_reg[11] <= fix_tap; 
        hold_reg <= samp_reg[11];
        end
    
      4'hc : 
        begin
        samp_reg[12] <= fix_tap; 
        hold_reg <= samp_reg[12];
        end
    
      4'hd : 
        begin
        samp_reg[13] <= fix_tap; 
        hold_reg <= samp_reg[13];
        end
    
      4'he : 
        begin
        samp_reg[14] <= fix_tap; 
        hold_reg <= samp_reg[14];
        end

	default : 
	begin
	end
    

    endcase
  end
end
  

always @(posedge clk27)
  begin
  if(~resetn)
    accum_cnt <= {DDT_ACCUM_CNT_WIDTH{1'b0}};
  else 
    begin
      if(en_in)
        accum_cnt <=  {{(DDT_ACCUM_CNT_WIDTH-1){1'b0}},1'b1};
      else if(accum_cnt == DDT_NUM_TAPS)
        accum_cnt <= {DDT_ACCUM_CNT_WIDTH{1'b0}};
      else if(accum_cnt !=0)
        accum_cnt <= accum_cnt + 1'b1;
    end
  end    

always @(posedge clk27)
  if(~resetn) begin
     en_in_d <= 0;
     accum_cnt_d <= 0;
  end
  else begin
     en_in_d <= en_in;
     accum_cnt_d <= accum_cnt;
  end
  

always @(posedge clk27)
  begin
  if(~resetn)
    tap_accum <= 0;
  else 
    begin
      if(en_in | en_in_d)
        tap_accum <= 0;
      else if(|accum_cnt_d)
        tap_accum <= tap_accum + tap_mult_signed;
    end
  end



always @(accum_cnt or samp_reg[0] or 
	samp_reg[1] or
	samp_reg[2] or
	samp_reg[3] or
	samp_reg[4] or
	samp_reg[5] or
	samp_reg[6] or
	samp_reg[7] or
	samp_reg[8] or
	samp_reg[9] or
	samp_reg[10] or
	samp_reg[11] or
	samp_reg[12] or
	samp_reg[13] or
	samp_reg[14]
	)
  begin
  
     
  curr_coeff = {DDT_COEFF_WIDTH{1'b0}};
  
  case(accum_cnt)
    
    
      4'h1 : 
        begin
  	curr_coeff = DDT_CF0;
	curr_tap   = samp_reg[0];
        end
    
      4'h2 : 
        begin
  	curr_coeff = DDT_CF1;
	curr_tap   = samp_reg[1];
        end
    
    
      4'h3 : 
        begin
  	curr_coeff = DDT_CF2;
	curr_tap   = samp_reg[2];
        end
    
      4'h4 : 
        begin
  	curr_coeff = DDT_CF3;
	curr_tap   = samp_reg[3];
        end
    
    
      4'h5 : 
        begin
  	curr_coeff = DDT_CF4;
	curr_tap   = samp_reg[4];
        end
    
    
      4'h6 : 
        begin
  	curr_coeff = DDT_CF5;
	curr_tap   = samp_reg[5];
        end
      
      4'h7 : 
        begin
  	curr_coeff = DDT_CF0;
	curr_tap   = samp_reg[6];
        end
    
    
      4'h8 : 
        begin
   	curr_coeff = - DDT_CF5;
	curr_tap   = samp_reg[7];
        end
    
     
      4'h9 : 
        begin
  	curr_coeff = - DDT_CF4;
	curr_tap   = samp_reg[8];
        end
    
    
      4'ha : 
        begin
  	curr_coeff = - DDT_CF3;
	curr_tap   = samp_reg[9];
        end
    
    
      4'hb : 
        begin
  	curr_coeff = - DDT_CF2;
	curr_tap   = samp_reg[10];
        end
    
      4'hc : 
        begin
  	curr_coeff = - DDT_CF1;
	curr_tap   = samp_reg[11];
        end
    
      4'hd : 
        begin
  	curr_coeff =  DDT_CF0;
	curr_tap   = samp_reg[12];
        end
    
      4'he : 
        begin
  	curr_coeff =  DDT_CF0;
	curr_tap   = samp_reg[13];
        end
    
      4'hf : 
        begin
  	curr_coeff =  DDT_CF0;
	curr_tap   = samp_reg[14];
        end

      default : 
        begin
  	curr_coeff = DDT_CF0;
	curr_tap   = samp_reg[0];
        end
     
    endcase

  end

assign coeff_sign = curr_coeff[DDT_COEFF_WIDTH-1];
assign coeff_mag_wsb =  (coeff_sign ? - curr_coeff : curr_coeff);
assign coeff_mag =  coeff_mag_wsb[DDT_COEFF_WIDTH-1:0];
assign fix_tap = hold_reg + fix_in;
assign fix_tap_curr = curr_tap[DDT_DATA_WIDTH-1:0] + fix_in;
assign sum_tap_fix = ((accum_cnt == 1) ? curr_tap[DDT_DATA_WIDTH-1:0] : fix_tap_curr);
assign data_sign = sum_tap_fix[DDT_DATA_WIDTH-1];
assign data_mag_wsb =  (data_sign ? - sum_tap_fix : sum_tap_fix);
assign data_mag =   data_mag_wsb[DDT_DATA_WIDTH-1:0];


assign tap_mult = data_mag * coeff_mag;

always @(posedge clk27)
  if (~resetn) begin
    tap_mult_d <= 0;
    coeff_sign_d <= 0;
    data_sign_d <= 0;
  end
  else begin
    tap_mult_d <= tap_mult;
    coeff_sign_d <= coeff_sign;
    data_sign_d <= data_sign;
  end

//assign pre_mult_signed = tap_mult_d[DDT_MULT_WIDTH - 1 - DDT_Q_WIDTH : (DDT_MULT_WIDTH - DDT_Q_WIDTH - DDT_DATA_WIDTH)];
assign pre_mult_signed = tap_mult_d[27:5];
assign tap_mult_signed = ((coeff_sign_d ^ data_sign_d) ? - pre_mult_signed : pre_mult_signed);


//assign ddt_out_int_x = tap_accum[DDT_ACCUM_WIDTH - 1 : DDT_ACCUM_WIDTH - DDT_DATA_WIDTH];
assign ddt_out_int_x = tap_accum[22:0];

always @(posedge clk27)
  if (~resetn) 
    ddt_out_int <= 0;
  else if (en_out)
    ddt_out_int <= ddt_out_int_x;
  else
    ddt_out_int <= ddt_out_int;

assign en_out = en_in;

always @(cordic_shift or ddt_out_int)
 /* 
  case(cordic_shift)
    3'h0: ddt_out_x = {ddt_out_int[22:10],4'b0}; // x16
    3'h1: ddt_out_x = {ddt_out_int[22:9],3'b0}; // x8
    3'h2: ddt_out_x = {ddt_out_int[22:8],2'b0}; // x4
    3'h3: ddt_out_x = {ddt_out_int[22:7],1'b0}; // x2
    3'h4: ddt_out_x = ddt_out_int[22:6]; // x1
    3'h5: ddt_out_x = ddt_out_int[21:5]; // x1/2
    3'h6: ddt_out_x = ddt_out_int[20:4]; // x1/4
    3'h7: ddt_out_x = ddt_out_int[19:3]; // x1/8
  endcase
*/
  case(cordic_shift)
    3'h0: ddt_out_x = {ddt_out_int[22:10],21'b0} ; //x1/16
    3'h1: ddt_out_x = ddt_out_int[22:9] ; //x1/8
    3'h2: ddt_out_x = ddt_out_int[22] ? //x1/4
                       ((ddt_out_int[21] == 1'b1) ? ddt_out_int[21:8] : 14'h2000) :
                       ((ddt_out_int[21] == 1'b0) ? ddt_out_int[21:8] : 14'h1fff) ;
    3'h3: ddt_out_x = ddt_out_int[22] ? //x1/2
                       ((ddt_out_int[21:20] == 2'b11) ? ddt_out_int[20:7] : 14'h2000) :
                       ((ddt_out_int[21:20] == 2'b00) ? ddt_out_int[20:7] : 14'h1fff) ;
    3'h4: ddt_out_x = ddt_out_int[22] ? //x1
                       ((ddt_out_int[21:19] == 3'b111) ? ddt_out_int[19:6] : 14'h2000) :
                       ((ddt_out_int[21:19] == 3'b000) ? ddt_out_int[19:6] : 14'h1fff) ;
    3'h5: ddt_out_x = ddt_out_int[22] ? //x2
                       ((ddt_out_int[21:18] == 4'hf) ? ddt_out_int[18:5] : 14'h2000) :
                       ((ddt_out_int[21:18] == 4'h0) ? ddt_out_int[18:5] : 14'h1fff) ;
    3'h6: ddt_out_x = ddt_out_int[22] ? //x4
                       ((ddt_out_int[21:17] == 5'h1f) ? ddt_out_int[17:4] : 14'h2000) :
                       ((ddt_out_int[21:17] == 5'h00) ? ddt_out_int[17:4] : 14'h1fff) ;
    3'h7: ddt_out_x = ddt_out_int[22] ? //x8
                       ((ddt_out_int[21:16] == 6'h3f) ? ddt_out_int[16:3] : 14'h2000) :
                       ((ddt_out_int[21:16] == 6'h00) ? ddt_out_int[16:3] : 14'h1fff) ;
  endcase

     
    
// register output
always @(posedge clk27)
  if (~resetn)
    ddt_out <= 0;
  else
    ddt_out <= ddt_out_x;


endmodule

module cordic(
		// inputs
		clk27,
		resetn,
		en_in,
		a_in,
		x_in,
		y_in,
		// outputs
		a_out,
		fix_out,
		en_out
		);

parameter COORD_WIDTH = 14;
parameter COORD_Q_WIDTH = 3;
parameter COORD_F_WIDTH = 11;
parameter ANGIN_WIDTH = 16;
parameter ANGOUT_WIDTH = 22;
parameter LOOP_WIDTH = 4;
parameter PREC_WIDTH = 5;

parameter MAX_COUNT = 12;

parameter QUAD0_OFF = 16'h0000;
parameter QUAD1_OFF = 16'h1680;
parameter QUAD2_OFF = 16'h2d00;
parameter QUAD3_OFF = 16'h4380;

parameter NEG_ROT = - 22'h002d00;
parameter POS_ROT =   22'h002d00;
/*
parameter ATAN0 = 16'h1680;
parameter ATAN1 = 16'h0d48;
parameter ATAN2 = 16'h0704;
parameter ATAN3 = 16'h0390;
parameter ATAN4 = 16'h01c9;
parameter ATAN5 = 16'h00e5;
parameter ATAN6 = 16'h0072;
parameter ATAN7 = 16'h0039;
parameter ATAN8 = 16'h001c;
parameter ATAN9 = 16'h000e;
parameter ATAN10 = 16'h0007;
parameter ATAN11 = 16'h0003;
*/

parameter ATAN0 = 16'h0b40;
parameter ATAN1 = 16'h06a4;
parameter ATAN2 = 16'h0382;
parameter ATAN3 = 16'h01c8;
parameter ATAN4 = 16'h00e4;
parameter ATAN5 = 16'h0072;
parameter ATAN6 = 16'h0039;
parameter ATAN7 = 16'h001c;
parameter ATAN8 = 16'h000e;
parameter ATAN9 = 16'h0007;
parameter ATAN10 = 16'h0003;
parameter ATAN11 = 16'h0001;

parameter DDT_DATA_WIDTH = 22;

parameter DDT_Q_WIDTH = 17;
parameter DDT_F_WIDTH = 5;

parameter DDT_NUM_COEFF   = 8;
parameter DDT_COEFF_WIDTH = 22;
//parameter DDT_COEFF_WIDTH = 13;
parameter DDT_NUM_TAPS = 15;
parameter DDT_ACCUM_CNT_WIDTH = 4;
parameter DDT_MULT_WIDTH = DDT_COEFF_WIDTH + DDT_DATA_WIDTH;
//parameter DDT_ACCUM_WIDTH = DDT_COEFF_WIDTH + DDT_DATA_WIDTH + DDT_NUM_COEFF;
parameter DDT_ACCUM_WIDTH = DDT_DATA_WIDTH;

/*
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF0 = 13'h002f;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF1 = - 13'h0056;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF2 = 13'h00cf;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF3 = - 13'h01c0;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF4 = 13'h036d;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF5 = - 13'h069d;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF6 = 13'h0f45;
parameter    [DDT_COEFF_WIDTH-1:0] DDT_CF7 = 13'h0000;
*/

parameter    DDT_CF0 = 22'h000000;
parameter    DDT_CF1 = 22'h000001; // 1
parameter    DDT_CF2 = 22'h3fffd5; //-43
parameter    DDT_CF3 = 22'h00012d; //301
parameter    DDT_CF4 = 22'h3ffb51; //-1199
parameter    DDT_CF5 = 22'h000fff; //4095

/*
parameter    DDT_CF0 = 22'h000000;
parameter    DDT_CF1 = 22'h3fffff;
parameter    DDT_CF2 = 22'h000002;
parameter    DDT_CF3 = 22'h3ffffc;
parameter    DDT_CF4 = 22'h000007;
parameter    DDT_CF5 = 22'h3ffff3;
parameter    DDT_CF6 = 22'h00001f;
parameter    DDT_CF7 = 22'h000000;
*/



parameter IIRFX_DATA_WIDTH = 16;
parameter IIRFX_COEFF_WIDTH = 16;
parameter IIRFX_DATA_SEG_WIDTH = 8;
parameter IIRFX_COEFF_SEG_WIDTH = 8;
parameter IIRFX_TMP_NUM = 2;
parameter IIRFX_ADD_WIDTH = 16;
parameter IIRFX_MULT_WIDTH = IIRFX_COEFF_SEG_WIDTH + IIRFX_DATA_SEG_WIDTH;
//parameter IIRFX_MULT_WIDTH = IIRFX_COEFF_SEG_WIDTH + IIRFX_DATA_SEG_WIDTH;
parameter IIRFX_MULT_SEG_WIDTH = IIRFX_COEFF_WIDTH + IIRFX_DATA_WIDTH;
parameter IIRFX_AP_COEFF_CNT_WIDTH = 4;
parameter IIRFX_AP_DATA_CNT_WIDTH = 4;
parameter IIRFX_AP_ADD_CNT_WIDTH = 2;
parameter IIRFX_AP_MULT_CNT_WIDTH = 4;
parameter IIRFX_COEFF_SEG = 4;
//parameter IIRFX_DATA_SEG = 8;
parameter  [IIRFX_AP_DATA_CNT_WIDTH-1:0] IIRFX_DATA_SEG = 4;
parameter  [IIRFX_AP_DATA_CNT_WIDTH-1:0] IIRFX_TOTAL_CNT = 7;
parameter IIRFX_ADD_SEG = 4;
parameter IIRFX_MULT_SEG = 4;
parameter IIRFX_SUB_CYC = IIRFX_COEFF_SEG + 3*IIRFX_DATA_SEG;

parameter IIRFX1_STR_NUM = 6;
parameter IIRFX1_AP_NUM = 9;
parameter IIRFX1_AP_PATH_CNT_WIDTH = 5;
parameter [IIRFX1_AP_PATH_CNT_WIDTH-1:0] IIRFX1_PATH_CYC = 16;


parameter IIRFX1_CF0 = 16'h0c7c;
parameter IIRFX1_CF1 = 16'h023f;
parameter IIRFX1_CF2 = 16'h06ed;
parameter IIRFX1_CF3 = 16'h01ad;
parameter IIRFX1_CF4 = - 16'h02a7;
parameter IIRFX1_CF5 = 16'h0186;
parameter IIRFX1_CF6 = - 16'h08f1;

parameter IIRFX2_STR_NUM = 3;
parameter IIRFX2_AP_NUM = 6;
parameter IIRFX2_AP_PATH_CNT_WIDTH = 4;
parameter [IIRFX2_AP_PATH_CNT_WIDTH-1:0] IIRFX2_PATH_CYC = 10;

parameter IIRFX2_CF0 = 16'h0a97;
parameter IIRFX2_CF1 =  - 16'h02b1;
parameter IIRFX2_CF2 = 16'h0a00;
parameter IIRFX2_CF3 = - 16'h0063;

parameter IIRFX4_STR_NUM = 8;
parameter IIRFX4_AP_NUM = 10;
parameter IIRFX4_AP_PATH_CNT_WIDTH = 6;
parameter [IIRFX4_AP_PATH_CNT_WIDTH-1:0] IIRFX4_PATH_CYC = 10;


parameter IIRFX4_AP_CF0 = 16'h0c69;
parameter IIRFX4_AP_CF1 = 16'h0de4;
parameter IIRFX4_AP_CF2 = 16'h01cf;

parameter IIRFX4_STR_CF0 = 16'h0aca;
parameter IIRFX4_STR_CF1 = 16'h019f;


parameter IIRFX3_STR_NUM = 21;
parameter IIRFX3_AP_NUM = 26;
parameter IIRFX3_AP_PATH_CNT_WIDTH = 5;
parameter IIRFX3_PATH_CYC = 26;

parameter   IIRFX3_AP_CF0 = 16'h0dba;
parameter   IIRFX3_AP_CF1 = 16'h04c0;
parameter   IIRFX3_AP_CF2 = 16'h0e65;
parameter   IIRFX3_AP_CF3 = 16'h03f5;
parameter   IIRFX3_AP_CF4 = 16'h0667;
parameter   IIRFX3_AP_CF5 = 16'h03b2;
parameter   IIRFX3_AP_CF6 = 16'hfdc3;    // - 16'h023d;
parameter   IIRFX3_AP_CF7 = 16'h0396;
// parameter   [IIRFX_COEFF_WIDTH-1:0] IIRFX3_AP_CF8 = 16'hf617;    // - 16'h09e9;
parameter   IIRFX3_AP_CF8 = 16'hf617;    // - 16'h09e9;
parameter   IIRFX3_AP_CF9 = 16'h038b;
parameter   IIRFX3_AP_CF10 = 16'hf18e;   // - 16'h0e72;


parameter   MUX_DATA_WIDTH = 14;

input	            	clk27;
input	            	resetn;
input	            	en_in;
input [COORD_WIDTH-1:0]   	a_in;
input [COORD_WIDTH-1:0]   	x_in;
input [COORD_WIDTH-1:0]   	y_in;

output [ANGOUT_WIDTH-1:0]        a_out;
output [ANGOUT_WIDTH-1:0]        fix_out;
output	            		en_out;

reg [ANGIN_WIDTH-1:0]      	a_start;
reg [14:0]     	x_start;
reg [14:0]     	y_start;
reg [PREC_WIDTH-1:0]        	prec_st;
reg [1:0]           		quadrant;

reg [LOOP_WIDTH-1:0] 		loop_cnt;

wire sign_x;
wire sign_y;
wire x_start_sign;
wire y_start_sign;
reg [ANGIN_WIDTH-1:0] a_add;
wire [ANGIN_WIDTH-1:0] a_add_signed;

reg [14:0] x_add;
wire [14:0] x_add_signed;
reg [14:0] y_add;
wire [14:0] y_add_signed;

reg [1:0] quad_prev;
reg [ANGOUT_WIDTH-1:0] a_accum;
reg [ANGOUT_WIDTH-1:0] a_rot;
reg loop_dn;
wire [ANGOUT_WIDTH-1:0]       a_out_int;

reg [ANGOUT_WIDTH-1:0]        a_out;
reg [ANGOUT_WIDTH-1:0]        fix_out;
reg	            		en_out;


assign sign_x = x_in[COORD_WIDTH-1];
assign sign_y = y_in[COORD_WIDTH-1];

always @(posedge clk27)
  begin
  if(~resetn)
    begin
          x_start <= {15{1'b0}};
          y_start <= {15{1'b0}};
	  quadrant <= 2'b00;
	  a_start <= {ANGOUT_WIDTH{1'b0}};
    end
  else  if(en_in)
    begin 
      case({sign_x,sign_y})

        2'b00 :
          begin
          x_start <= {x_in[13],x_in};
          y_start <= {y_in[13],y_in};
	  quadrant <= 2'b00;
	  a_start <= {1'b0,a_in} + QUAD0_OFF;
          end

        2'b10 :
          begin
          x_start <= {y_in[13],y_in};
          y_start <= -{x_in[13],x_in};
	  quadrant <= 2'b01;
	  a_start <= {1'b0,a_in} + QUAD1_OFF;
          end

        2'b11 :
          begin
          x_start <= -{x_in[13],x_in};
          y_start <= -{y_in[13],y_in};
	  quadrant <= 2'b10;
	  a_start <= {1'b0,a_in} + QUAD2_OFF;
          end

        2'b01 :
          begin
          x_start <= -{y_in[13],y_in};
          y_start <= {x_in[13],x_in};
	  quadrant <= 2'b11;
	  a_start <= {1'b0,a_in} + QUAD3_OFF;
          end

      endcase
    end  
  else if((|loop_cnt) & ~(y_start == {COORD_WIDTH{1'b0}}))
    begin
    x_start <= x_start + x_add_signed;
    y_start <= y_start + y_add_signed;
    a_start <= a_start + a_add_signed;
    end
  end


always @(posedge clk27)
  begin
  if(~resetn)
    begin
    loop_cnt <= {LOOP_WIDTH{1'b0}};
    end
  else if(en_in)
    begin
    loop_cnt <= {{(LOOP_WIDTH-1){1'b0}},1'b1};
    end
  else if(loop_cnt == MAX_COUNT)
    begin
    loop_cnt <= {LOOP_WIDTH{1'b0}};
    end
  else if(loop_cnt !=0)
    begin
    loop_cnt <= loop_cnt + 1'b1;
    end
  end    


always @(posedge clk27)
  begin
  if(~resetn)
    begin
    prec_st <= {PREC_WIDTH{1'b0}};
    end
  else if(en_in)
    begin
    prec_st <= {PREC_WIDTH{1'b0}};
    end
  else
    begin
    prec_st <= loop_cnt;
    end
  end    



assign x_start_sign = x_start[14];
assign y_start_sign = y_start[14];

always @(prec_st or x_start or y_start)
  begin
  
        y_add = {15{1'b0}};	
        x_add = {15{1'b0}};

    case(prec_st)
    
      5'h00 : 
        begin
        y_add = x_start;
        x_add = y_start;
        end
    
      5'h01 : 
        begin
        y_add = {x_start[14],x_start[14:1]};	
        x_add = {y_start[14],y_start[14:1]};	
        end
    
      5'h02 : 
        begin
        y_add = {{2{x_start[14]}},x_start[14:2]};	
        x_add = {{2{y_start[14]}},y_start[14:2]};	
        end
    
    
      5'h03 : 
        begin
        y_add = {{3{x_start[14]}},x_start[14:3]};	
        x_add = {{3{y_start[14]}},y_start[14:3]};	
        end
    
      5'h04 : 
        begin
        y_add = {{4{x_start[14]}},x_start[14:4]};	
        x_add = {{4{y_start[14]}},y_start[14:4]};	
        end
    
    
      5'h05 : 
        begin
        y_add = {{5{x_start[14]}},x_start[14:5]};	
        x_add = {{5{y_start[14]}},y_start[14:5]};	
        end
    
    
      5'h06 : 
        begin
        y_add = {{6{x_start[14]}},x_start[14:6]};	
        x_add = {{6{y_start[14]}},y_start[14:6]};	
        end
      
      5'h07 : 
        begin
        y_add = {{7{x_start[14]}},x_start[14:7]};	
        x_add = {{7{y_start[14]}},y_start[14:7]};	
        end
    
    
      5'h08 : 
        begin
        y_add = {{8{x_start[14]}},x_start[14:8]};	
        x_add = {{8{y_start[14]}},y_start[14:8]};	
        end
    
     
      5'h09 : 
        begin
        y_add = {{9{x_start[14]}},x_start[14:9]};	
        x_add = {{9{y_start[14]}},y_start[14:9]};	
        end
    
    
      5'h0a : 
        begin
        y_add = {{10{x_start[14]}},x_start[14:10]};	
        x_add = {{10{y_start[14]}},y_start[14:10]};	
        end
    
    
      5'h0b : 
        begin
        y_add = {{11{x_start[14]}},x_start[14:11]};	
        x_add = {{11{y_start[14]}},y_start[14:11]};	
        end
    
      5'h0c : 
        begin
        y_add = {{12{x_start[14]}},x_start[14:12]};	
        x_add = {{12{y_start[14]}},y_start[14:12]};	
        end
    
    endcase
  end


assign x_add_signed = (y_start_sign? -x_add : x_add);

assign y_add_signed = (y_start_sign? y_add : -y_add);



always @(posedge clk27)
  begin
  if(~resetn)
    begin
    quad_prev <= 2'b00;
    end
  else if(en_in)
    begin
    quad_prev <= quadrant;
    end
  end    

// calculate diff between adjacent angles
reg [15:0] a_start_d;
always @(posedge clk27)
  if(~resetn) 
     a_start_d <= 0;
  else if(en_in)
     a_start_d <= a_start;

//  absolute (old angle - new angle)
wire [15:0] a_diff = a_start_d - a_start; 
wire [15:0] abs_a_diff = a_diff[15]? (~a_diff + 1) : a_diff;

//             	|
//	    01  |  00
//            	|
//	  --------------
//            	|
//          10 	|  11
//		|

always @(posedge clk27)
  begin
  if(~resetn)
    begin
    a_rot <= 22'h0;
    end
  else if(loop_cnt == MAX_COUNT)
    begin
      case ({quad_prev,quadrant})

        4'b11_00 : a_rot <= NEG_ROT;	//-2pi
        4'b00_11 : a_rot <= POS_ROT;	//+2pi

        4'b10_00 : a_rot <= (abs_a_diff < POS_ROT) ? 0 : NEG_ROT ;
        4'b00_10 : a_rot <= (abs_a_diff < POS_ROT) ? 0 : POS_ROT ; 

        4'b11_01 : a_rot <= (abs_a_diff < POS_ROT) ? 0 : NEG_ROT ;
        4'b01_11 : a_rot <= (abs_a_diff < POS_ROT) ? 0 : POS_ROT ;


        default  : a_rot <= {ANGOUT_WIDTH{1'b0}};
      endcase
    end
  end    


always @(loop_cnt)
  begin
  
        a_add = {ANGOUT_WIDTH{1'b0}};	

    case(loop_cnt)
    
      4'h0 : 
        begin
        a_add = {ANGOUT_WIDTH{1'b0}};
        end
    
      4'h1 : 
        begin
        a_add = ATAN0;
        end
    
      4'h2 : 
        begin
        a_add = ATAN1;
        end
    
    
      4'h3 : 
        begin
        a_add = ATAN2;
        end
    
      4'h4 : 
        begin
        a_add = ATAN3;
        end
    
    
      4'h5 : 
        begin
        a_add = ATAN4;
        end
    
    
      4'h6 : 
        begin
        a_add = ATAN5;
        end
      
      4'h7 : 
        begin
        a_add = ATAN6;
        end
    
    
      4'h8 : 
        begin
        a_add = ATAN7;
        end
    
     
      4'h9 : 
        begin
        a_add = ATAN8;
        end
    
    
      4'ha : 
        begin
        a_add = ATAN9;
        end
    
    
      4'hb : 
        begin
        a_add = ATAN10;
        end

       4'hc : 
        begin
        a_add = ATAN11;
        end
    
    endcase
  end

assign a_add_signed = (y_start_sign? -a_add : a_add);

always @(posedge clk27)
  begin
  if(~resetn)
    begin
 en_out <= 1'b0;
 fix_out <=  {ANGOUT_WIDTH{1'b0}};
 a_out <= {ANGOUT_WIDTH{1'b0}};
    end
  else if(en_in)
    begin
 en_out <= 1'b1;
 fix_out <=  a_rot;
 a_out <= {{7{a_start[ANGIN_WIDTH-1]}},a_start[ANGIN_WIDTH-1:1]};
    end
  else
    begin
 en_out <= 1'b0;
    end
  end    


/*
assign fix_out =  a_rot;
assign a_out = {{7{a_start[ANGIN_WIDTH-1]}},a_start[ANGIN_WIDTH-1:1]};
assign en_out = en_in;
*/
endmodule


