module top(bar);
   parameter foo = 32'd9;
   integer   i;
   output [31:0] bar;
   reg [31:0]    bar;
   
   always @* begin
     bar = 0;
     for (i = 0; i < foo; i = i + 1)
	bar = bar + foo[i];
   end
endmodule

