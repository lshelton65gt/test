#!/bin/csh -f

## Ensure the given library won't require the C++ runtime library to link.

## Pain is caused when our runtime code links with the C++ runtime
## library, because the library is tied to the particular (version of
## the) compiler, and linking with two different C++ runtime libraries
## causes conflicts.  This script attempts to ensure that our runtime
## code doesn't reference any symbols that is resolved by the
## compiler's C++ runtime library.  This frees user code to use
## whatever C++ runtime library (and therefore whatever compiler (and
## version)) he prefers.

if ($?CARBON_HOST_ARCH) then
  set arch = $CARBON_HOST_ARCH
else
  set arch = `$CARBON_HOME/scripts/carbon_arch`
endif

# Choose the correct libcarbon filename based on the current library version
set libcarbonversion = `$CARBON_HOME/bin/carbon libversion`
set libcarbonfilename = "libcarbon$libcarbonversion"

# Check libraries with which users link.
set libs = (gcc{2,3,4}/{$libcarbonfilename,libcarbonmem,libcarbonpli,libpliwrapper,libcarbonvspx}.a)

cd $CARBON_HOME
set liblist = ""
foreach lib ($libs)
  if (-e $arch/lib/$lib) set liblist = ($liblist $arch/lib/$lib)
end

if ("x$liblist" == x) then
  echo "$0 is broken:" '$liblist is empty.'
  exit 1
endif

# Symbols that would get resolved from the C++ runtime library
set disallowed_symbols = 'operator new|operator delete|std::|dynamic_cast|cxxabi'
# Allow disallowed symbols if the line also contains an exempt symbol.
# This isn't ideal -- it would be better if grep could match "'std::'
# only when not followed by 'pair'," but it can't.
set exempt_symbols = 'std::pair'
# Objects that are in the runtime library but not linked into user programs
set exempt_objects = '_ArgProc.o|_UtProfile.o'

host_exec nm -ACu $liblist | egrep "$disallowed_symbols" \
                           | egrep -v "$exempt_symbols" \
                           | egrep -v "$exempt_objects"

if (! $status) then
  # I suppose we could customize this output and only report the relevant ones.
  echo ""
  echo "Likely causes of failure:"
  echo "For new/delete:"
  echo " You added a class without the CARBONMEM_OVERRIDES macro."
  echo " You're using new or delete on a non-Carbon class rather than"
  echo "  CARBON_NEW/DELETE, CARBON_ALLOC/FREE_VEC, or StringUtil::alloc/free."
  echo "For std::__throw_length_error:"
  echo " You're using STL's Vector template, or something that uses it."
  echo "For __dynamic_cast:"
  echo " There's a dynamic_cast<>() in the code."
  echo "For vtable for __cxxabiv1::"
  echo " Module wasn't compiled with -fno-rtti."
  echo "Other std:: symbols:"
  echo " You've found some STL class that has a runtime dependency."
  echo "Asking the compiler for preprocessor output (-E) may be helpful"
  echo " in finding the the source of the problematic symbol."
  exit 1
endif
exit 0
