# Makefile for dumping Make variables not starting with CARBON
#
# Idea from http://www.cmcrossroads.com/content/view/6521/

.PHONY: printvars
printvars:
	@:$(foreach V,$(sort $(filter-out CARBON%,$(.VARIABLES))),$(if $(filter file,$(origin $V)),$(warning $V)))
