#!/usr/bin/perl -w

# Find the source tree by groveling thru the top-level makefile.
# This all assumes that CARBON_HOME points to the dist directory
#
# (code stolen from src/test/runlist)
#
$ts=`grep "^srcdir = " $ENV{CARBON_HOME}/../Makefile`;

 chomp $ts;
 $ts=~ s#/src$##;
 $ts=~ s#^srcdir = ##;
 print "${ts}/src";
