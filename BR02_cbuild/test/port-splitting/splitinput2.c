/* c testbench for splitinput.v */

#include "libdesign.h"
#include <stdio.h>
#include <string.h>

static const int fail = 3;

#define CONST1 0xf0f0f0ff

static int sIsZero(const UInt32* val, int numWords)
{
  int i;
  int isZero;
  
  isZero = 1;
  for (i = 0; i < numWords && isZero; ++i)
    isZero = (val[i] == 0) ? 1 : 0;

  return isZero;
}

int main()
{
  UInt32 splitA[9];
  UInt32 splitAd[9];
  UInt32 en;
  CarbonNetID* splitAN;
  CarbonNetID* enN;
  CarbonObjectID* hdl;
  CarbonWaveID* wave;
  const char* waveFile;

  char str[265];

  int i;  
  int stat = 0; 
  CarbonTime time = 0;
  
  en = 1;
  waveFile = "splitinput2.vcd";

  /* just initialize these internal variables */
  memset(splitA, 0, 9 * sizeof(UInt32));

  memset(splitAd, 0, 9 * sizeof(UInt32));
  
  hdl = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  if (! hdl)
  {
    fprintf(stderr, "design create failed.\n");
    return fail;
  }

  wave = carbonWaveInitVCD(hdl, waveFile, e1ns);
  if (! wave)
  {
    fprintf(stderr, "Unable to create wave file.\n");
    return fail;
  }

  if (carbonDumpVars(wave, 0, "top") != eCarbon_OK)
  {
    fprintf(stderr, "Unable to dump top level.\n");
    return fail;
  }
  
  splitAN = carbonFindNet(hdl, "top.splitA");
  enN= carbonFindNet(hdl, "top.en");
  

  if (! splitAN)
  {
    fprintf(stderr, "splitA findNet failed.\n");
    stat = fail;
  }
  if (! enN)
  {
    fprintf(stderr, "en findNet failed.\n");
    stat = fail;
  }



  if (stat == 0)
  {
    carbonDeposit(hdl, enN, &en, NULL);
    carbonSchedule(hdl, time);

    time += 10;


    // this will spew out error messages. But that's ok for this test.
    carbonDeposit(hdl, splitAN, splitA, 0);


    carbonSchedule(hdl, time);    

    splitA[0] = 1;

    time += 10;


    if (carbonExamine(hdl, splitAN, splitA, splitAd) != eCarbon_OK)
    {
      fprintf(stderr, "Examining splitA failed.\n");
      stat = fail;
    }


    if (! sIsZero(splitA, 9))
    {
      fprintf(stderr, "examine splitA failed.\n");
      stat = fail;
    }
    
  }
  
  // We should have an x in the 66th bit (splitA[65]).
  carbonFormat(hdl, splitAN, str, 265, eCarbonBin);
  printf("%s\n", str);
  carbonDestroy(&hdl);  
  return stat;
}
