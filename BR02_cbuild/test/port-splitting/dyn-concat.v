module top(a,b,c,d,idx,in);
   output a,b,c,d;
   input [1:0] idx;
   input       in;

   assign      {a,b} = {c,d};

   sub s0(.out({a,b,c,d}),.idx(idx),.in(in));
endmodule

module sub(out,idx,in);
   output [3:0] out;
   input [1:0] idx;
   input       in;
   reg [3:0] out;

   // rewriting for port-splitting needs to handle replacing 'out'
   // with a LHS concat where the bitsel is dynamic.
   always out[idx] = in | (|out);
endmodule
