// Hier-refs writes are not strong drivers, so they could result in 
// port-splitting
module top(clk, a, b, out, out2);
  input clk;
  input [3:0] a, b;
  output [31:0] out;
  output        out2;

  always @(posedge clk) begin
    sub.writeone;
    sub.writezero;
  end

  sub sub(a, b, out[17:0], out2);

  assign out[16] = b[0];
  assign out[31:18] = 14'b0;
endmodule

module sub(a, b, out, out2);
  input [3:0] a, b;
  output [17:0] out;
  output        out2;
  reg [17:0]    out;

  initial out = 32'b0;

  task writeone;
    out[{1'b0,a}] = 1'b1;
  endtask

  task writezero;
    out[{1'b0,b}] = 1'b0;
  endtask

  always @(a)
    out[17] = a[0];
  assign out2 = out[16];        // forces splitting of 'out'
endmodule
