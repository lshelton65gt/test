module top (out1, out2, clk, in1, in2, in3);
   output out1, out2;
   input  clk, in1, in2, in3;

   wire   r1, r2;
   wire [1:0] bus;
   sub S1 (r1, bus[0], clk, in1 & in2 & in3, in2);
   sub S2 (r2, bus[1], clk, in1 & in2 & in3, in2);

   reg        out1, out2;
   always @ (posedge clk)
     out1 <= bus[0] ^ bus[1];
   always @ (posedge clk)
     out2 <= r1 ^ r2;

   pull P1 (bus);

endmodule // top

module sub (r, io, clk, i1, i2);
   output r;
   inout  io;
   input  clk, i1, i2;

   assign io = i1 ? i2 : 1'bz;

   reg    r;
   always @ (posedge clk)
     r <= io;

endmodule // sub1

module pull (io);
   inout [1:0] io;

   pullup pio [1:0] (io);

endmodule // pull

   
