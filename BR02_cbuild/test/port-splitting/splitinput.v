
module top(split1, split2, split4, split8, splitA, testout1, testout2, en, split1_2);
   inout [32:1] split1;
   inout [39:0] split2;
   inout [67:0] split4;
   inout [131:0] split8;
   inout [263:0] splitA;
   inout [31:0]  split1_2;
   
   output testout1;
   output testout2;
   input en;

   wire [31:0] split1dead;
   wire [39:0] split2dead;
   wire [67:0] split4dead;
   wire [131:0] split8dead;
   wire [263:0] splitAdead;
   wire dummy1;
   wire dummy2;

`ifdef DEADSPLIT   
   one t1 (split1, split2, split4, split8, splitAdead, testout1, testout2, en);
`else
   
   one t1 (split1, split2, split4, split8, splitA, testout1, testout2, en);
   
 `ifndef BUG2023
   one t2 (split1_2, split2dead, split4dead, split8dead, splitAdead, dummy1, dummy2, en);
 `endif
   
`endif
endmodule // top

module one(split1, split2, split4, split8, splitA, testout1, testout2, en);
   inout [31:0] split1;
   inout [39:0] split2;
   inout [67:0] split4;
   inout [131:0] split8;
   inout [263:0] splitA;
   output testout1;
   output testout2;
   input en;

   test test1(split1[7:0],
              split1[15:8],
              split2[9:0],
              split2[19:10],
              split4[16:0],
              split4[33:17],
              split8[32:0],
              split8[65:33],
              splitA[65:0],
              splitA[131:66],
              testout1, en);
   
   test test2(split1[23:16],
              split1[31:24],
              split2[29:20],
              split2[39:30],
              split4[50:34],
              split4[67:51],
              split8[98:66],
              split8[131:99],
              splitA[197:132],
              splitA[263:198],
              testout2, en);
   
endmodule // top

module test(in1, out1,
            in2, out2,
            in4, out4,
            in8, out8,
            inA, outA,
            testout, en);
   
   input [7:0] in1;
   inout [7:0] out1;
   input [9:0]  in2;
   inout [9:0] out2;
   input [16:0] in4;
   inout [16:0] out4;
   input [32:0]  in8;
   inout [32:0] out8;
   input [65:0]  inA;
   inout [65:0] outA;
   output       testout;
   input        en;

   assign        outA = en ? 66'hz : inA;
   assign        out8 = en ? 33'hz : 8;
`ifdef BUG1963
   assign        out4 = en ? 17'hz : 17'd4;
   assign        out2 = en ? 10'hz : 10'd2;
`else
   assign        out4 = en ? 17'hz : 4;
   assign        out2 = en ? 10'hz : 2;
`endif
   assign        out1 = en ? 8'hz : 1;
   assign        testout = (in1 | in2 | in4 | in8) ? 1 : 0;
endmodule // test

