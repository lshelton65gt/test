module dembuf(out,io,en,d);
   output [1:0] out;
   inout [1:0] io;
   input  en;
   input [1:0] d;
   wire [1:0]  local;
   pad p0(local[0],io[0],en,d[0]);
   pad p1(local[1],io[1],en,d[1]);

   wrap w0(local,out);
endmodule

module pad(o,io,en,d);
   output o;
   inout  io;
   input  en;
   input  d;
   assign o = io;
   assign io = en ? d : 1'bz;
endmodule

module wrap(in,out); // carbon disallowFlattening
   input [1:0] in;
   output [1:0] out;
   assign 	out = in;
endmodule
