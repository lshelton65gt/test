module top (out1, out2, clk, in1, in2, in3);
   output out1, out2;
   input  clk, in1, in2, in3;

   wire   r1, r2;
   wire [1:0] bus;
   wire [1:0] bus_in;
   sub S1 (r1, bus[0], clk, bus_in[0], in1 & in2 & in3, in2);
   sub S2 (r2, bus[1], clk, bus_in[1], in1 & in2 & in3, in2);

   reg        out1, out2;
   always @ (posedge clk)
     out1 <= bus[0] ^ bus[1];
   always @ (posedge clk)
     out2 <= r1 ^ r2;

   pull P1 (bus);

   flop F1 (bus_in, clk);

endmodule // top

module sub (r, io, clk, i0, i1, i2);
   output r;
   inout  io;
   input  clk, i0, i1, i2;

   assign io = i1 ? i2 : 1'bz;
   assign io = ~i1 ? i0 : 1'bz;

   reg    r;
   always @ (posedge clk)
     r <= io;

endmodule // sub1

module pull (io);
   inout [1:0] io;

   pullup pio [1:0] (io);

endmodule // pull

   
module flop (q, clk);
   output [1:0] q;
   input        clk;
   
   reg    [1:0] q;
   always @ (posedge clk)
     q[1] <= q[0];

   always @ (posedge clk)
     q[0] <= q[1];

endmodule // flop
