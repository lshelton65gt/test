-- Reproduces bug 7677. NUScope::gensym() fails with an assert
-- indicating that same name is created more than once during port splitting.
-- A name can appear only once in a scope. This failure was a result
-- of port splitting using just the field name and not the record
-- signal name to construct port split net names.
library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

package bistDefs is

  constant ABSMaxAddressBits:	 integer := 20;

  type bundle_bistStatus is record
                              bf:             std_logic;
                              bistDone:       std_logic;
                            end record;
  type bistStatusArray is array(natural range<>) of bundle_bistStatus;

end bistDefs;


library ieee;
use ieee.std_logic_1164.all;
use work.bistDefs.all;

entity tc_bot is port (
  clk			: in    std_logic;
  din                 : in    std_logic_vector (1 downto 0);
  dout                : out   std_logic_vector (1 downto 0);
  
  bistStatus          : inout bundle_bistStatus
  );
end tc_bot;

architecture synth of tc_bot is
begin

  biststatusprocess : process (clk, din)
  begin
    if clk'Event and clk = '1' then
      bistStatus<= (din(0), din(1));
    end if;
  end process;

  tc_proc: process(clk, din)
  begin
    if clk'Event and clk = '1' then
      dout <= din or (bistStatus.bf & bistStatus.bistDone);
    end if;
  end process;

end synth;

library ieee;
use ieee.std_logic_1164.all;
use work.bistDefs.all;

entity tc_mid is port (
  clk			: in    std_logic;
  din0                : in    std_logic_vector (3 downto 0);
  dout0               : out   std_logic_vector (3 downto 0);
  
  bistStatus_arr          : inout bistStatusArray(1 downto 0)
  
  
  );
end tc_mid;

architecture synth of tc_mid is

  component tc_bot 
    port (
      clk			: in    std_logic;
      din                 : in    std_logic_vector (1 downto 0);
      dout                : out   std_logic_vector (1 downto 0);
      
      bistStatus          : inout bundle_bistStatus
      );
  end component;

--  alias bistStatus0 : bundle_bistStatus is bistStatus_arr(0);
--  alias bistStatus1 : bundle_bistStatus is bistStatus_arr(1);

begin

  u0_tc_bot: tc_bot port map (
    clk => clk,
    din => din0(1 downto 0),
    dout => dout0(1 downto 0),
    bistStatus => bistStatus_arr(0)
    );

  u1_tc_bot: tc_bot port map (
    clk => clk,
    din => din0(3 downto 2),
    dout => dout0(3 downto 2),
    bistStatus => bistStatus_arr(1)
    );

end synth;

library ieee;
use ieee.std_logic_1164.all;
use work.bistDefs.all;

entity bug7677 is port (
  clk			: in    std_logic;
  din                : in    std_logic_vector (7 downto 0);
  dout               : out    std_logic_vector (11 downto 0)
  );
end bug7677;


architecture synth of bug7677 is

  component tc_mid 
    port (
      clk			: in    std_logic;
      din0                : in    std_logic_vector (3 downto 0);
      dout0               : out    std_logic_vector (3 downto 0);
      
      bistStatus_arr          : inout bistStatusArray(1 downto 0)
      );
  end component; 

  signal bistStatus_arr    : bistStatusArray(1 downto 0);
  signal bistStatus_arr1 : bistStatusArray(1 downto 0);
  
begin

  process (bistStatus_arr, din)
  begin  -- process

    if (din(1) = '1') then
      dout(11) <= bistStatus_arr(0).bf;
      dout(10) <= bistStatus_arr(1).bf;
      dout(9) <= bistStatus_arr(0).bistdone;
      dout(8) <= bistStatus_arr(1).bistdone;
    else
      bistStatus_arr(0).bf <= din(7);
      bistStatus_arr(1).bf <= din(6);
      bistStatus_arr(0).bistdone <= din(5);
      bistStatus_arr(1).bistdone <= din(4);
    end if;

    bistStatus_arr1 <= bistStatus_arr;
    
  end process;


  u_tc_mid_0: tc_mid port map (
    clk => clk,
    din0 => din(3 downto 0),
    dout0 => dout(3 downto 0),
    bistStatus_arr => bistStatus_arr
    );

  u_tc_mid_1: tc_mid port map (
    clk => clk,
    din0 => din(7 downto 4),
    dout0 => dout(7 downto 4),
    bistStatus_arr => bistStatus_arr1
    );


end synth;
