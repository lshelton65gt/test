module top(o,io,en);
   output [31:0] o;
   inout [31:0]  io;
   input 	 en;

   one t0(o,io,en);
endmodule

module one(o,io,en);
   output [31:0] o;
   inout [31:0]  io;
   input 	 en;

`ifdef LEVEL_ONE
   two t0(o[31:16],io[31:16],en);
   two t1(o[15:0],io[15:0],en);
`else
   assign o = io;
   assign io = en ? 32'hffffffff : 32'bz;
`endif
   
endmodule

module two(o,io,en);
   output [15:0] o;
   inout [15:0]  io;
   input 	 en;

`ifdef LEVEL_TWO
   three t0(o[15:8],io[15:8],en);
   three t1(o[7:0],io[7:0],en);
`else
   assign o = io;
   assign io = en ? 16'hffff : 16'bz;
`endif
endmodule

module three(o,io,en);
   output [7:0] o;
   inout [7:0]  io;
   input 	 en;

`ifdef LEVEL_THREE
   four t0(o[7:4],io[7:4],en);
   four t1(o[3:0],io[3:0],en);
`else
   assign o = io;
   assign io = en ? 8'hff : 8'bz;
`endif
endmodule

module four(o,io,en);
   output [3:0] o;
   inout [3:0]  io;
   input 	 en;

`ifdef LEVEL_FOUR
   five t0(o[3:2],io[3:2],en);
   five t1(o[1:0],io[1:0],en);
`else
   assign o = io;
   assign io = en ? 4'hf : 4'bz;
`endif
endmodule

module five(o,io,en);
   output [1:0] o;
   inout [1:0]  io;
   input 	 en;

`ifdef LEVEL_FIVE
   six t0(o[1],io[1],en);
   six t1(o[0],io[0],en);
`else
   assign o = io;
   assign io = en ? 2'b11 : 2'bz;
`endif
endmodule

module six(o,io,en);
   output o;
   inout  io;
   input  en;

   assign o = io;
   assign io = en ? 1'b1 : 1'bz;
   
endmodule
