// Hier-refs writes are not strong drivers, so they could result in 
// port-splitting
module top(clk, a, b, out, out2, out3);
  input clk;
  input [3:0] a, b;
  output [31:0] out;
  output        out2, out3;

  always @(posedge clk) begin
    sub.writeone;
    sub.writezero;
  end

  sub sub(a, b, out[19:0], out2, out3);

  assign out[16] = b[0];
endmodule

module sub(a, b, out, out2, out3);
  input [3:0] a, b;
  output [19:0] out;
  output        out2, out3;
  reg [19:0]    out;

  initial out = 32'b0;

  task writeone;
    out[{1'b0,a}] = 1'b1;
  endtask

  task writezero;
    out[{1'b0,b}] = 1'b0;
  endtask

  always @(a)
    out[17] = a[0];
  assign out2 = out[16];        // forces splitting of 'out'

  leaf leaf(out[19:18], a, out3);
endmodule

module leaf(out, a, out3);
  input [1:0] out; // coerced to inout.  Cheetah error if specified as inout
  input [3:0] a;
  output      out3;

  assign      out[0] = a[1];
  assign      out3 = out[1];
endmodule
