
module top(testout1, testout2, en);
   
   output testout1, testout2;
   input en;

   wire [263:0] splitA; // carbon observeSignal

   wire         ja; // carbon depositSignal
   assign       splitA[63] = ja;
   
   one t1 (splitA, testout1, testout2, en);
   
endmodule // top

module one(splitA, testout1, testout2, en);
   inout [263:0] splitA;
   output testout1, testout2;
   input en;

   test test1(splitA[65:0],
              splitA[131:66],
              testout1, en);
   
   test test2(splitA[197:132],
              splitA[263:198],
              testout2, en);
   
endmodule // top

module test(inA, outA,
            testout, en);
   
   input [65:0]  inA;
   inout[65:0] outA;
   output       testout;
   input        en;
   assign       outA[65:1] = inA;
   
   assign        outA[0] = en ? 1'hz : 0;
   assign        testout = inA[63] ? 1 : 0;
endmodule // test

