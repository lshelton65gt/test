# test for duplicate symbol creation error.
ITSKIP echo 'VTEXIT/VHDL this test because it uses VHDL and currently has a CARBON INTERNAL ERROR'
VTEXIT cbuild -q -vhdlTop bug7677 bug7677.vhdl -testdriver -o libbug7677.a >& bug7677.cbld.log
VTEXIT VTDIFF runtest ./bug7677.exe >& bug7677.sim.log

cbuild -f coerce.cmd split.v >& zero.cbld.log

# just splitting and coercion
cbuild -f coerce.cmd split.v +define+LEVEL_ONE >& one.cbld.log 
cbuild -f coerce.cmd split.v +define+LEVEL_ONE+LEVEL_TWO >& two.cbld.log 
cbuild -f coerce.cmd split.v +define+LEVEL_ONE+LEVEL_TWO+LEVEL_THREE >& three.cbld.log 
cbuild -f coerce.cmd split.v +define+LEVEL_ONE+LEVEL_TWO+LEVEL_THREE+LEVEL_FOUR >& four.cbld.log 
cbuild -f coerce.cmd split.v +define+LEVEL_ONE+LEVEL_TWO+LEVEL_THREE+LEVEL_FOUR+LEVEL_FIVE >& five.cbld.log 

# flattening versions for everything.
cbuild -f flatten.cmd split.v +define+LEVEL_ONE >& one-flat.cbld.log 
cbuild -f flatten.cmd split.v +define+LEVEL_ONE+LEVEL_TWO >& two-flat.cbld.log 
cbuild -f flatten.cmd split.v +define+LEVEL_ONE+LEVEL_TWO+LEVEL_THREE >& three-flat.cbld.log 
cbuild -f flatten.cmd split.v +define+LEVEL_ONE+LEVEL_TWO+LEVEL_THREE+LEVEL_FOUR >& four-flat.cbld.log 
cbuild -f flatten.cmd split.v +define+LEVEL_ONE+LEVEL_TWO+LEVEL_THREE+LEVEL_FOUR+LEVEL_FIVE >& five-flat.cbld.log 

# ati/dc found this bug. check that folding does the right thing to a 
# partselect of a partselect. Eg, bus[22:21][0:0], should return bus[21]
cbuild nested_partsel.v -f coerce.cmd -testdriver -o libnested_partsel.$STATEXT >& nested_partsel.cbld.log
runtest ./nested_partsel.exe >& nested_partsel.sim.log

# simple split of an inout to input and output
# deposit test
make clean DRIVER=splitinput >& splitinput.cleandeposit.log
make splitinput DRIVER=splitinput >& splitinput.shell.make.deposit.log

runtest ./splitinput >& splitinput.rund.log

carbon_exec wavetestbenchgen/wavequery -mode dumpnames -t vcd -in splitinput.deposit.vcd >& splitinput.wave.name.dump

# force test
make clean DRIVER=splitinput >& splitinput.cleanforce.log
make splitinput DRIVER=splitinput FLAGS=-DFORCE XTRA_CBFLAGS="-directive splitinput.dir" >& splitinput.shell.make.force.log 

runtest ./splitinput >& splitinput.runf.log

# force test with -noInputFlow
#make clean DRIVER=splitinput >& splitinput.cleannf.log
#make splitinput DRIVER=splitinput FLAGS=-DFORCE XTRA_CBFLAGS="-noInputFlow -directive splitinput.dir" >& splitinput.shell.make.forcenf.log 

#runtest ./splitinput >& splitinput.runnf.log

# force test with dead input. 
#The runtime should fail, but the vcd files should match
make clean DRIVER=splitinput >& splitinput.cleandead.log
make splitinput DRIVER=splitinput FLAGS="-DFORCE" XTRA_CBFLAGS="+define+DEADSPLIT -directive splitinput.dir" >& splitinput.shell.make.dead.log

PEXIT runtest ./splitinput >& splitinput.dead.log 
carbon_exec wavetestbenchgen/wavequery -mode diff -t vcd -in splitinput.deadsplit.vcd.gold -in splitinput.force.vcd >& splitinput.wavequery.dead.diff

# bug 3063
make clean DRIVER=splitinput2
make splitinput2 DRIVER=splitinput2 >& splitinput2.bld.log
runtest ./splitinput2 >& splitinput2.sim.log

cbuild -f coerce.cmd dyn-concat.v >& dyn-concat.cbld.log 

cbuild -f coerce.cmd concat.v -testdriver -o libconcat.$STATEXT >& concat.cbld.log
runtest ./concat.exe >& concat.sim.log

cbuild -f coerce.cmd -flatten concat.v -testdriver -o libconcat_flat.$STATEXT >& concat_flat.cbld.log
runtest ./concat_flat.exe >& concat_flat.sim.log

# the following fails with an Alert
TEXIT cbuild -f coerce.cmd concat2.v -testdriver -o libconcat2.$STATEXT >& concat2.cbld.log
# runtest ./concat2.exe >& concat.sim.log

cbuild -q -noFlatten -directive splitobs.dir -coercePorts -dumpSchedule splitobs.v >& splitobs.cbld.log
cbuild -q -flatten -directive splitobs.dir -coercePorts -dumpSchedule splitobs.v >& splitobs.flat.cbld.log

cbuild -q -noFlatten -directive splitdep.dir -coercePorts -dumpSchedule splitdep.v >& splitdep.cbld.log
cbuild -q -flatten -directive splitdep.dir -coercePorts -dumpSchedule splitdep.v >& splitdep.flat.cbld.log

cbuild -q -testdriver -coercePorts bug4011.v -o libbug4011.$STATEXT >& bug4011.cbld.log
runtest bug4011.exe >& bug4011.td.log

cbuild -q -testdriver -coercePorts -verboseSplitPorts bug4260.v -o libbug4260.$STATEXT >& bug4260.cbld.log
runtest bug4260.exe >& bug4260.sim.log

# these tests all assert-fail during cbuild prior to 5/26/05.  I can't
# seem to get test results I believe with Aldec, and Finsim crashes with
# no error message.
cbuild -q hiersplit.v -testdriver >& hiersplit.cbld.log
cbuild -q hiersplit2.v -promoteAllTasks -promoteHierTasks >& hiersplit2.cbld.log
cbuild -q hiersplit3.v >& hiersplit3.cbld.log

# Add a test where concat rewrite and port splitting create the wrong
# simulation results
cbuild -q -verboseSplitPorts concat3.v -testdriver -dumpVerilog -verboseConcatRewrite >& concat3.cbld.log
runtest design.exe >& concat3.sim.log

# Add a test for bug13019
# Currently protected regions initial support added
# testcase cbld log has diffs with Interra version, which needs to be investigated more thoroughly  
VTDIFF cbuild -q -f bug13019.f bug13019.v -testdriver >& bug13019.cbld.log
runtest design.exe >& bug13019.sim.log
