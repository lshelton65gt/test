
module top(o1,o2,en,in);
   output [1:0] o1, o2;
   input  en;
   input [1:0] in;
   wire   io_0, io_1;

   pullwrap pullwrap(o1, {io_0,io_1});
   padwrap  padwrap (o2, {io_0,io_1}, en, in);
   
endmodule

module pullwrap(o,io);
   output [1:0] o;
   inout [1:0] 	io;

   mypull mypull0(o[0],io[0]);
   mypull mypull1(o[1],io[1]);
endmodule

module mypull(o,io);
   output o;
   inout  io;
   pullup (io);
   assign o = io;
endmodule

module padwrap(o,io,en,in);
   output [1:0] o;
   inout [1:0]  io;
   input 	 en;
   input [1:0] 	 in;

   pad pad0(o[0],io[0],en,in[0]);
   pad pad1(o[1],io[1],en,in[1]);
endmodule


module pad(o,io,en,in);
   output o;
   inout  io;
   input  en;
   input  in;

   assign o = io;
   assign io = en ? in : 1'bz;
   
endmodule

