module leaf(out, a, out3);
  input [1:0] out;
  input [3:0] a;
  output out3;

  // Continuous Assigns
  assign out[0] = a[1];                            // hiersplit2.v:47
  assign out3 = out[1];                            // hiersplit2.v:48
endmodule // leaf

module sub(a, b, out, out2, out3);
  input [3:0] a;
  input [3:0] b;
  output [19:0] out;
  output out2;
  output out3;

  // Output regs (1)
  reg          [19:0]         out;

  // Initial Blocks
  initial                                          // hiersplit2.v:25
    // $block_hiersplit2_v_L25;1
    begin                                          // hiersplit2.v:25
      // Declarations
      // Statements
      out = 0;                                     // hiersplit2.v:25
    end

  // Continuous Assigns
  assign out2 = out[16];                           // hiersplit2.v:37

  // Always Blocks
  always                                           // hiersplit2.v:35
    // $block_hiersplit2_v_L35;1
    begin                                          // hiersplit2.v:35
      // Declarations
      // Statements
      out[17] = a[0];                              // hiersplit2.v:36
    end

  // Instances
  leaf                                             // hiersplit2.v:42
    leaf(                                          // hiersplit2.v:39
      .out(out[19:18]),                            // input
      .a(a),                                       // input
      .out3(out3));                                // output
endmodule // sub

module top(clk, a, b, out, out2, out3);
  input clk;
  input [3:0] a;
  input [3:0] b;
  output [31:0] out;
  output out2;
  output out3;

  // Continuous Assigns
  assign out[16] = b[0];                           // hiersplit2.v:16

  // Always Blocks
  always @(posedge clk)                            // hiersplit2.v:9
    // $block_hiersplit2_v_L9;1
    begin                                          // hiersplit2.v:9
      // Declarations
      // Statements
      writeone;
      writezero;
    end

  // Tasks
  task writeone;
    top.sub.out[{1'b0,top.sub.a}] = 1'b1;            // hiersplit2.v:28
  endtask
  task writezero;
    top.sub.out[{1'b0,top.sub.b}] = 1'b0;            // hiersplit2.v:32
  endtask

  // Instances
  sub                                              // hiersplit2.v:19
    sub(                                           // hiersplit2.v:14
      .a(a),                                       // input
      .b(b),                                       // input
      .out(out[19:0]),                             // output
      .out2(out2),                                 // output
      .out3(out3));                                // output
endmodule // top
