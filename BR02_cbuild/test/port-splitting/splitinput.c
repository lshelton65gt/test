/* c testbench for splitinput.v */

#include "libdesign.h"
#include <stdio.h>
#include <string.h>

static const int fail = 3;

#define CONST1 0xf0f0f0ff

static int sIsZero(const UInt32* val, int numWords)
{
  int i;
  int isZero;
  
  isZero = 1;
  for (i = 0; i < numWords && isZero; ++i)
    isZero = (val[i] == 0) ? 1 : 0;

  return isZero;
}

int main()
{
  UInt32 split1;
  UInt32 split1d;
  UInt32 split2[2];
  UInt32 split2d[2];
  UInt32 split4[3];
  UInt32 split4d[3];
  UInt32 split8[5];
  UInt32 split8d[5];
  UInt32 splitA[9];
  UInt32 splitAd[9];
  UInt32 en;
  CarbonNetID* split1N;
  CarbonNetID* split2N;
  CarbonNetID* split4N;
  CarbonNetID* split8N;
  CarbonNetID* splitAN;
  CarbonNetID* enN;
  CarbonObjectID* hdl;
  CarbonWaveID* wave;
  const char* waveFile;
  UInt32 split4Ex[3];

  int i;  
  int stat = 0; 
  CarbonTime time = 0;
  
#ifdef FORCE
  en = 0;
  waveFile = "splitinput.force.vcd";
#else
  en = 1;
  waveFile = "splitinput.deposit.vcd";
#endif

  split1 = 0;
  /* just initialize these internal variables */
  memset(splitA, 0, 9 * sizeof(UInt32));
  memset(split8, 0, 5 * sizeof(UInt32));
  memset(split4, 0, 3 * sizeof(UInt32));
  memset(split2, 0, 2 * sizeof(UInt32));

  memset(splitAd, 0, 9 * sizeof(UInt32));
  memset(split8d, 0, 5 * sizeof(UInt32));
  memset(split4d, 0, 3 * sizeof(UInt32));
  memset(split2d, 0, 2 * sizeof(UInt32));
  
  hdl = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  if (! hdl)
  {
    fprintf(stderr, "design create failed.\n");
    return fail;
  }

  wave = carbonWaveInitVCD(hdl, waveFile, e1ns);
  if (! wave)
  {
    fprintf(stderr, "Unable to create wave file.\n");
    return fail;
  }

  if (carbonDumpVars(wave, 0, "top") != eCarbon_OK)
  {
    fprintf(stderr, "Unable to dump top level.\n");
    return fail;
  }
  
  split1N = carbonFindNet(hdl, "top.split1");
  split2N = carbonFindNet(hdl, "top.split2");
  split4N = carbonFindNet(hdl, "top.split4");
  split8N = carbonFindNet(hdl, "top.split8");
  splitAN = carbonFindNet(hdl, "top.splitA");
  enN= carbonFindNet(hdl, "top.en");
  
  if (! split1N)
  {
    fprintf(stderr, "split1 findNet failed.\n");
    stat = fail;
  }

  if (! split2N)
  {
    fprintf(stderr, "split2 findNet failed.\n");
    stat = fail;
  }

  if (! split4N)
  {
    fprintf(stderr, "split4 findNet failed.\n");
    stat = fail;
  }

  if (! split8N)
  {
    fprintf(stderr, "split8 findNet failed.\n");
    stat = fail;
  }

  if (! splitAN)
  {
    fprintf(stderr, "splitA findNet failed.\n");
    stat = fail;
  }
  if (! enN)
  {
    fprintf(stderr, "en findNet failed.\n");
    stat = fail;
  }


  /* this tests both force when FORCE defined and exprnet */
  carbonGetExternalDrive(hdl, split4N, split4Ex);
  if ((split4Ex[0] != 0xffffffff) ||
      (split4Ex[1] != 0xffffffff) ||
      (split4Ex[2] != 0xf))
  {
    fprintf(stderr, "carbonGetExternalDrive on undriven expr net failed.\n");
    stat = fail;
  }

  if (stat == 0)
  {
    carbonDeposit(hdl, enN, &en, NULL);
    carbonSchedule(hdl, time);

    time += 10;

#ifdef FORCE
    if (carbonForce(hdl, split1N, &split1) != eCarbon_OK)
    {
      fprintf(stderr, "Forcing split1 failed.\n");
      stat = fail;
    }

    if (carbonForce(hdl, split2N, split2) != eCarbon_OK)
    {
      fprintf(stderr, "Forcing split2 failed.\n");
      stat = fail;
    }

    if (carbonForce(hdl, split4N, split4) != eCarbon_OK)
    {
      fprintf(stderr, "Forcing split4 failed.\n");
      stat = fail;
    }

    if (carbonForce(hdl, split8N, split8) != eCarbon_OK)
    {
      fprintf(stderr, "Forcing split8 failed.\n");
      stat = fail;
    }

    if (carbonForce(hdl, splitAN, splitA) != eCarbon_OK)
    {
      fprintf(stderr, "Forcing splitA failed.\n");
      stat = fail;
    }

    split1 = 1;
    split2[0] = 1;
    split2[1] = 1;
    memset(splitA, 1, 9 * sizeof(UInt32));
    memset(split8, 1, 5 * sizeof(UInt32));
    memset(split4, 1, 3 * sizeof(UInt32));

    if (carbonDeposit(hdl, split1N, &split1, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit split1 failed.\n");
      stat = fail;
    }

    if (carbonDeposit(hdl, split2N, split2, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit split2 failed.\n");
      stat = fail;
    }

    if (carbonDeposit(hdl, split4N, split4, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit split4 failed.\n");
      stat = fail;
    }

    if (carbonDeposit(hdl, split8N, split8, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit split8 failed.\n");
      stat = fail;
    }

    if (carbonDeposit(hdl, splitAN, splitA, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit splitA failed.\n");
      stat = fail;
    }

    
#else
    if (carbonDeposit(hdl, split1N, &split1, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit split1 failed.\n");
      stat = fail;
    }

    if (carbonDeposit(hdl, split2N, split2, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit split2 failed.\n");
      stat = fail;
    }

    if (carbonDeposit(hdl, split4N, split4, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit split4 failed.\n");
      stat = fail;
    }

    if (carbonDeposit(hdl, split8N, split8, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit split8 failed.\n");
      stat = fail;
    }

    if (carbonDeposit(hdl, splitAN, splitA, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit splitA failed.\n");
      stat = fail;
    }


    carbonGetExternalDrive(hdl, split4N, split4Ex);
    if ((split4Ex[0] != 0) ||
        (split4Ex[1] != 0) ||
        (split4Ex[2] != 0))
    {
      fprintf(stderr, "carbonGetExternalDrive on driven expr net failed.\n");
      stat = fail;
    }

#endif

    carbonSchedule(hdl, time);    

    split1 = 1;
    split2[0] = 1;
    split4[0] = 1;
    split8[0] = 1;
    splitA[0] = 1;

    time += 10;


    if (carbonExamine(hdl, split1N, &split1, &split1d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining split1 failed.\n");
      stat = fail;
    }

    if (carbonExamine(hdl, split2N, split2, split2d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining split2 failed.\n");
      stat = fail;
    }

    if (carbonExamine(hdl, split4N, split4, split4d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining split4 failed.\n");
      stat = fail;
    }

    if (carbonExamine(hdl, split8N, split8, split8d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining split8 failed.\n");
      stat = fail;
    }

    if (carbonExamine(hdl, splitAN, splitA, splitAd) != eCarbon_OK)
    {
      fprintf(stderr, "Examining splitA failed.\n");
      stat = fail;
    }

    if (! sIsZero(&split1, 1))
    {
      fprintf(stderr, "examine split1 failed.\n");
      stat = fail;
    }
    if (! sIsZero(split2, 2))
    {
      fprintf(stderr, "examine split2 failed.\n");
      stat = fail;
    }

    if (! sIsZero(split4, 3))
    {
      fprintf(stderr, "examine split4 failed.\n");
      stat = fail;
    }

    if (! sIsZero(split8, 5))
    {
      fprintf(stderr, "examine split8 failed.\n");
      stat = fail;
    }


    if (! sIsZero(splitA, 9))
    {
      fprintf(stderr, "examine splitA failed.\n");
      stat = fail;
    }
    
    
#ifdef FORCE
    if (carbonRelease(hdl, split1N) != eCarbon_OK)
    {
      fprintf(stderr, "Releasing split1 failed.\n");
      stat = fail;
    }

    if (carbonRelease(hdl, split2N) != eCarbon_OK)
    {
      fprintf(stderr, "Releasing split2 failed.\n");
      stat = fail;
    }

    if (carbonRelease(hdl, split4N) != eCarbon_OK)
    {
      fprintf(stderr, "Releasing split4 failed.\n");
      stat = fail;
    }

    if (carbonRelease(hdl, split8N) != eCarbon_OK)
    {
      fprintf(stderr, "Releasing split8 failed.\n");
      stat = fail;
    }

    if (carbonRelease(hdl, splitAN) != eCarbon_OK)
    {
      fprintf(stderr, "Releasing splitA failed.\n");
      stat = fail;
    }
    
    if (carbonDepositWord(hdl, splitAN, 3, 0, 0) != eCarbon_OK)
    {
      fprintf(stderr, "Deposit splitA failed.\n");
      stat = fail;
    }

    carbonSchedule(hdl, time);    
    time += 10;

    if (carbonExamine(hdl, split1N, &split1, &split1d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining 2 split1 failed.\n");
      stat = fail;
    }

    if (carbonExamine(hdl, split2N, split2, split2d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining 2 split2 failed.\n");
      stat = fail;
    }

    if (carbonExamine(hdl, split4N, split4, split4d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining 2 split4 failed.\n");
      stat = fail;
    }

    if (carbonExamine(hdl, split8N, split8, split8d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining 2 split8 failed.\n");
      stat = fail;
    }

    if (carbonExamine(hdl, splitAN, splitA, splitAd) != eCarbon_OK)
    {
      fprintf(stderr, "Examining 2 splitA failed.\n");
      stat = fail;
    }

#ifndef BUG2014
    if (sIsZero(&split1, 1))
    {
      fprintf(stderr, "examine 2 split1 failed.\n");
      stat = fail;
    }
    if (sIsZero(split2, 2))
    {
      fprintf(stderr, "examine 2 split2 failed.\n");
      stat = fail;
    }

    if (sIsZero(split4, 3))
    {
      fprintf(stderr, "examine 2 split4 failed.\n");
      stat = fail;
    }

    if (sIsZero(split8, 5))
    {
      fprintf(stderr, "examine 2 split8 failed.\n");
      stat = fail;
    }
    

    if (sIsZero(splitA, 9))
    {
      fprintf(stderr, "examine 2 splitA failed.\n");
      stat = fail;
    }

#endif

    /* Now do a force word and range on the big net. */
    split1 = CONST1;
    if (carbonForceWord(hdl, splitAN, split1, 4) != eCarbon_OK)
    {
      fprintf(stderr, "Forcing splitA word, index 4 failed.\n");
      stat = fail;
    }

    split4[0] = 0xffffffff;
    split4[1] = 0xffffffff;
    split4[2] = 0x12;

    if (carbonForceRange(hdl, splitAN, split4, 87, 15) != eCarbon_OK)
    {
      fprintf(stderr, "Forcing splitA range, index 1 failed.\n");
      stat = fail;
    }
    
    carbonSchedule(hdl, time);    
    time += 10;

    split1 = 0;
    if (carbonExamineWord(hdl, splitAN, &split1, 4, &split1d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining Word 4 of splitA failed.\n");
      stat = fail;
    }

    if ((split1 != CONST1) && (split1d != 0))
    {
      fprintf(stderr, "examine Word 4 of splitA failed.\n");
      stat = fail;
    }
    
    split4[0] = 0;
    split4[1] = 0;
    split4[2] = 0;
    split4d[0] = 0xffffffff;
    split4d[1] = 0xffffffff;
    split4d[2] = 0x1f;
    if (carbonExamineRange(hdl, splitAN, split4, 87, 15, split4d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining Range of splitA failed.\n");
      stat = fail;
    }

    if ((split4[0] != 0xffffffff) ||
        (split4[1] != 0xffffffff) ||
        (split4[2] != 0x12) ||
        (split4d[0] != 0) ||
        (split4d[1] != 0) ||
        (split4d[2] != 0))
    {
      fprintf(stderr, "examine Range of splitA failed.\n");
      stat = fail;
    }

    /* Release. */
    if (carbonReleaseWord(hdl, splitAN, 4) != eCarbon_OK)
    {
      fprintf(stderr, "Releasing splitA word, index 4 failed.\n");
      stat = fail;
    }

    if (carbonReleaseRange(hdl, splitAN, 87, 15) != eCarbon_OK)
    {
      fprintf(stderr, "Releasing splitA range, index 1 failed.\n");
      stat = fail;
    }
    
    carbonSchedule(hdl, time);    
    time += 10;

    en = 1;
    carbonDeposit(hdl, enN, &en, NULL);

    carbonSchedule(hdl, time);    
    time += 10;

    en = 0;
    carbonDeposit(hdl, enN, &en, NULL);

    carbonSchedule(hdl, time);    
    time += 10;

    if (carbonExamineWord(hdl, splitAN, &split1, 4, &split1d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining 2 Word 4 of splitA failed.\n");
      stat = fail;
    }

    /* First 4 bits are output */
    if ((split1 != 0) || (split1d != 0xfffffff0))
    {
      fprintf(stderr, "examine 2 Word 4 of splitA failed.\n");
      stat = fail;
    }
    
    if (carbonExamineRange(hdl, splitAN, split4, 87, 15, split4d) != eCarbon_OK)
    {
      fprintf(stderr, "Examining 2 Range of splitA failed.\n");
      stat = fail;
    }

    /*
      Bit 66 and 67 are ones in splitA, which translates to 0x180000
      in the examined range
    */
    if ((split4[0] != 0) ||
        (split4[1] != 0x180000) ||
        (split4[2] != 0) ||
        (split4d[0] != 0xffffffff) ||
        (split4d[1] != 0x7ffff) ||
        (split4d[2] != 0))
      
    {
      fprintf(stderr, "examine 2 Range of splitA failed.\n");
      stat = fail;
    }

    /* test carbonResolveBidi on the expr net */
    carbonDepositWord(hdl, splitAN, 0, 3, 0);
    carbonResolveBidi(hdl, splitAN);
    carbonGetExternalDrive(hdl, splitAN, splitAd);
    if (splitAd[3] != 0xffffffff)
    {
      fprintf(stderr, "resolveBidi failed on exprnet\n");
      stat = fail;
    }

    
#endif
  }
  
  
  carbonDestroy(&hdl);  
  return stat;
}
