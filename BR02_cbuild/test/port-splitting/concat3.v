// This test found a problem at Panasonic with a combination of
// port splitting and ConcatRewrite. The problem is that concat
// rewrite cannot be done blindly if some of the LHS and RHS nets
// are actually the same elaborated net.
//
// In this case:
//  top.o1[1] is aliased to top.F1.q[1]
//        and
//  top.F1.d[1] is aliased to top.o1[0]
//        and
//  top.o2[0] is aliased to top.F1.q[0]
//        and
//  top.F1.d[0] is aliased to top.o2[1]
//
// This is bug7178.

module top(o1, o2, en, in, clk); // carbon disallowFlattening
   output [1:0] o1, o2;
   input  en;
   input [1:0] in;
   input       clk;

   pullwrap pullwrap1(o1);
   pullwrap pullwrap2(o2);
   flop F1 (o1, clk, {o1[0], in[0]});
   flop F2 (o2, clk, {in[0], o2[1]});
   
endmodule

module pullwrap(io);
   inout [1:0] 	io;

   mypull mypull0(io[0]);
   mypull mypull1(io[1]);
endmodule

module mypull(io);
   inout  io;
   pulldown (io);
endmodule

module flop(q, clk, d); // carbon flattenModule
   output [1:0] q;
   input        clk;
   input [1:0]  d;

   reg [1:0]    q;
   initial q = 0;
   always @ (posedge clk)
     begin
        q <= d;
     end

endmodule
