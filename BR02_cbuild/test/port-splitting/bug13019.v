`timescale 1ns / 1ps
module model_sdram128MB_Row8kBank4Col512 (Dq, Addr, Ba, Clk, Cke, Cs_n, Ras_n, Cas_n, We_n, Dqm);


    parameter addr_bits =      13;
    parameter data_bits =      16*4;
    parameter col_bits  =       9;
    parameter mem_sizes = 4194303;

    inout     [data_bits - 1 : 0] Dq;
    input     [addr_bits - 1 : 0] Addr;
    input                 [1 : 0] Ba;
    input                         Clk;
    input                         Cke;
    input                         Cs_n;
    input                         Ras_n;
    input                         Cas_n;
    input                         We_n;
    input                 [7 : 0] Dqm;

 mt48lc16m16a2 mt48lc16m16a2_0(	
		.Dq(Dq[15:0]), 
		.Addr(Addr), 
		.Ba(Ba), 
		.Clk(Clk), 
		.Cke(Cke), 
		.Cs_n(Cs_n), 
		.Ras_n(Ras_n), 
		.Cas_n(Cas_n), 
		.We_n(We_n), 
		.Dqm(Dqm[1:0])
	);

 mt48lc16m16a2 mt48lc16m16a2_1(	
		.Dq(Dq[31:16]), 
		.Addr(Addr), 
		.Ba(Ba), 
		.Clk(Clk), 
		.Cke(Cke), 
		.Cs_n(Cs_n), 
		.Ras_n(Ras_n), 
		.Cas_n(Cas_n), 
		.We_n(We_n), 
		.Dqm(Dqm[3:2])
	);

 mt48lc16m16a2 mt48lc16m16a2_2(	
		.Dq(Dq[47:32]), 
		.Addr(Addr), 
		.Ba(Ba), 
		.Clk(Clk), 
		.Cke(Cke), 
		.Cs_n(Cs_n), 
		.Ras_n(Ras_n), 
		.Cas_n(Cas_n), 
		.We_n(We_n), 
		.Dqm(Dqm[5:4])
	);

 mt48lc16m16a2 mt48lc16m16a2_3(	
		.Dq(Dq[63:48]), 
		.Addr(Addr), 
		.Ba(Ba), 
		.Clk(Clk), 
		.Cke(Cke), 
		.Cs_n(Cs_n), 
		.Ras_n(Ras_n), 
		.Cas_n(Cas_n), 
		.We_n(We_n), 
		.Dqm(Dqm[7:6])
	);



 endmodule	//model_sdram128MB_Row8kBank4Col512.(Data Width==64bit.Address Width==13bit.)



`timescale 1ns / 1ps

module mt48lc16m16a2 (Dq, Addr, Ba, Clk, Cke, Cs_n, Ras_n, Cas_n, We_n, Dqm);

    parameter addr_bits =      13;
    parameter data_bits =      16;
    parameter col_bits  =       9;
    parameter mem_sizes = 4194303;

    inout     [data_bits - 1 : 0] Dq;
    input     [addr_bits - 1 : 0] Addr;
    input                 [1 : 0] Ba;
    input                         Clk;
    input                         Cke;
    input                         Cs_n;
    input                         Ras_n;
    input                         Cas_n;
    input                         We_n;
    input                 [1 : 0] Dqm;

carbonx_ddrsdram #(256, 16) ram_inst
( .dq(Dq),
.dqs(Dqs),
.addr(Addr),
.ba(Ba),
.clk(Clk),
.clk_n(Clk_n),
.cke(Cke),
.cs_n(Cs_n),
.cas_n(Cas_n),
.ras_n(Ras_n),
.we_n(We_n),
.dm(Dm)
);

endmodule

`timescale 1ns / 1ps

module top (
           Dq_0
         , Dq_1
         , Dq_2
         , Dq_3
         , Dq_4
         , Dq_5
         , Dq_6
         , Dq_7
         , Dq_8
         , Dq_9
         , Dq_10
         , Dq_11
         , Dq_12
         , Dq_13
         , Dq_14
         , Dq_15
         , Dq_16
         , Dq_17
         , Dq_18
         , Dq_19
         , Dq_20
         , Dq_21
         , Dq_22
         , Dq_23
         , Dq_24
         , Dq_25
         , Dq_26
         , Dq_27
         , Dq_28
         , Dq_29
         , Dq_30
         , Dq_31
         , Addr, Ba, Clk, Cke, Cs_n, Ras_n, Cas_n, We_n, Dqm);

    parameter addr_bits =      13;
    parameter data_bits =      8;
    parameter col_bits  =       9;
    parameter mem_sizes = 4194303;

    parameter n0 = 0;
    parameter n1 = n0 + 64;
    parameter n2 = n1 + 64;
    parameter n3 = n2 + 64;
    parameter n4 = n3 + 64;
    parameter n5 = n4 + 64;
    parameter n6 = n5 + 64;
    parameter n7 = n6 + 64;
    parameter n8 = n7 + 64;

    inout     [data_bits - 1 : 0] Dq_0;
    inout     [data_bits - 1 : 0] Dq_1;
    inout     [data_bits - 1 : 0] Dq_2;
    inout     [data_bits - 1 : 0] Dq_3;
    inout     [data_bits - 1 : 0] Dq_4;
    inout     [data_bits - 1 : 0] Dq_5;
    inout     [data_bits - 1 : 0] Dq_6;
    inout     [data_bits - 1 : 0] Dq_7;
    inout     [data_bits - 1 : 0] Dq_8;
    inout     [data_bits - 1 : 0] Dq_9;
    inout     [data_bits - 1 : 0] Dq_10;
    inout     [data_bits - 1 : 0] Dq_11;
    inout     [data_bits - 1 : 0] Dq_12;
    inout     [data_bits - 1 : 0] Dq_13;
    inout     [data_bits - 1 : 0] Dq_14;
    inout     [data_bits - 1 : 0] Dq_15;
    inout     [data_bits - 1 : 0] Dq_16;
    inout     [data_bits - 1 : 0] Dq_17;
    inout     [data_bits - 1 : 0] Dq_18;
    inout     [data_bits - 1 : 0] Dq_19;
    inout     [data_bits - 1 : 0] Dq_20;
    inout     [data_bits - 1 : 0] Dq_21;
    inout     [data_bits - 1 : 0] Dq_22;
    inout     [data_bits - 1 : 0] Dq_23;
    inout     [data_bits - 1 : 0] Dq_24;
    inout     [data_bits - 1 : 0] Dq_25;
    inout     [data_bits - 1 : 0] Dq_26;
    inout     [data_bits - 1 : 0] Dq_27;
    inout     [data_bits - 1 : 0] Dq_28;
    inout     [data_bits - 1 : 0] Dq_29;
    inout     [data_bits - 1 : 0] Dq_30;
    inout     [data_bits - 1 : 0] Dq_31;
    input     [addr_bits - 1 : 0] Addr;
    input                 [1 : 0] Ba;
    input                         Clk;
    input                         Cke;
    input                         Cs_n;
    input                         Ras_n;
    input                         Cas_n;
    input                         We_n;
    input                 [7 : 0] Dqm;

  model_sdram128MB_Row8kBank4Col512 model_sdram128MB_Row8kBank4Col512_0 (
      .Dq    ( {Dq_0, Dq_1, Dq_2, Dq_3} )
    , .Addr  ( Addr )
    , .Ba    ( Ba )
    , .Clk   ( Clk )
    , .Cke   ( Cke )
    , .Cs_n  ( Cs_n )
    , .Ras_n ( Ras_n )
    , .Cas_n ( Cas_n )
    , .We_n  ( We_n )
    , .Dqm   ( Dqm )
  );

  model_sdram128MB_Row8kBank4Col512 model_sdram128MB_Row8kBank4Col512_1 (
      .Dq    ( {Dq_4, Dq_5, Dq_6, Dq_7} )
    , .Addr  ( Addr )
    , .Ba    ( Ba )
    , .Clk   ( Clk )
    , .Cke   ( Cke )
    , .Cs_n  ( Cs_n )
    , .Ras_n ( Ras_n )
    , .Cas_n ( Cas_n )
    , .We_n  ( We_n )
    , .Dqm   ( Dqm )
  );

  model_sdram128MB_Row8kBank4Col512 model_sdram128MB_Row8kBank4Col512_2 (
      .Dq    ( {Dq_8, Dq_9, Dq_10, Dq_11} )
    , .Addr  ( Addr )
    , .Ba    ( Ba )
    , .Clk   ( Clk )
    , .Cke   ( Cke )
    , .Cs_n  ( Cs_n )
    , .Ras_n ( Ras_n )
    , .Cas_n ( Cas_n )
    , .We_n  ( We_n )
    , .Dqm   ( Dqm )
  );

  model_sdram128MB_Row8kBank4Col512 model_sdram128MB_Row8kBank4Col512_3 (
      .Dq    ( {Dq_12, Dq_13, Dq_14, Dq_15} )
    , .Addr  ( Addr )
    , .Ba    ( Ba )
    , .Clk   ( Clk )
    , .Cke   ( Cke )
    , .Cs_n  ( Cs_n )
    , .Ras_n ( Ras_n )
    , .Cas_n ( Cas_n )
    , .We_n  ( We_n )
    , .Dqm   ( Dqm )
  );

  model_sdram128MB_Row8kBank4Col512 model_sdram128MB_Row8kBank4Col512_4 (
      .Dq    ( {Dq_16, Dq_17, Dq_18, Dq_19} )
    , .Addr  ( Addr )
    , .Ba    ( Ba )
    , .Clk   ( Clk )
    , .Cke   ( Cke )
    , .Cs_n  ( Cs_n )
    , .Ras_n ( Ras_n )
    , .Cas_n ( Cas_n )
    , .We_n  ( We_n )
    , .Dqm   ( Dqm )
  );

  model_sdram128MB_Row8kBank4Col512 model_sdram128MB_Row8kBank4Col512_5 (
      .Dq    ( {Dq_20, Dq_21, Dq_22, Dq_23} )
    , .Addr  ( Addr )
    , .Ba    ( Ba )
    , .Clk   ( Clk )
    , .Cke   ( Cke )
    , .Cs_n  ( Cs_n )
    , .Ras_n ( Ras_n )
    , .Cas_n ( Cas_n )
    , .We_n  ( We_n )
    , .Dqm   ( Dqm )
  );

  model_sdram128MB_Row8kBank4Col512 model_sdram128MB_Row8kBank4Col512_6 (
      .Dq    ( {Dq_24, Dq_25, Dq_26, Dq_27} )
    , .Addr  ( Addr )
    , .Ba    ( Ba )
    , .Clk   ( Clk )
    , .Cke   ( Cke )
    , .Cs_n  ( Cs_n )
    , .Ras_n ( Ras_n )
    , .Cas_n ( Cas_n )
    , .We_n  ( We_n )
    , .Dqm   ( Dqm )
  );

  model_sdram128MB_Row8kBank4Col512 model_sdram128MB_Row8kBank4Col512_7 (
      .Dq    ( {Dq_28, Dq_29, Dq_30, Dq_31} )
    , .Addr  ( Addr )
    , .Ba    ( Ba )
    , .Clk   ( Clk )
    , .Cke   ( Cke )
    , .Cs_n  ( Cs_n )
    , .Ras_n ( Ras_n )
    , .Cas_n ( Cas_n )
    , .We_n  ( We_n )
    , .Dqm   ( Dqm )
  );

endmodule

