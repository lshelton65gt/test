// testcase which makes sure that part-selects properly resolve after
// port splitting/port coercion.

module top(clk,in,out1,out2,out3,out4);
   input 	clk;
   input [3:0] 	in;
   output [0:0] out1,out2,out3,out4;

   wire [8:0] 	bus;

   setup setup(bus,out1,out2,out3,out4,in,clk);
   flop_wrap flop(bus);

endmodule

module setup (bus,out1,out2,out3,out4,in,clk);
   inout [8:0] bus;
   output [0:0] out1,out2,out3,out4;
   input [3:0] 	in;
   input 	clk;

   // read from inputs
   assign 	bus[0] = clk;
   assign 	bus[4:1] = in;

   // write to outputs.
   assign 	out1 = bus[5];
   assign 	out2 = bus[6];
   assign 	out3 = bus[7];
   assign 	out4 = bus[8];
endmodule

module flop_wrap(bus);
   inout [8:0] bus;

   flop2 f2( .out(bus[8:7]),
	     .in(bus[4:3]),
	     .clk(bus[0]) );

   flop2 f1( .out(bus[6:5]), 
	     .in(bus[2:1]),
	     .clk(bus[0]) );
   
endmodule

module flop2(out,in,clk);
   output [1:0] out;
   input [1:0] 	in;
   input 	clk;
   flop f2(out[1],in[1],clk);
   flop f1(out[0],in[0],clk);
endmodule

module flop(out,in,clk);
   output out;
   input  in;
   input  clk;
   reg 	  out;
   initial out = 0;
   always @(posedge clk)
     out <= in;
endmodule
