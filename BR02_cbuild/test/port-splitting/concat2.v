// last mod: Tue Oct 25 09:58:33 2005
// filename: test/port-splitting/concat2.v
// Description:  This test (provided by Aron) gets an assert in ConcatRewrite
// bug5211

module top(clk,a,b,en,d,i,io,out,o2); // carbon disallowFlattening
   input clk,a,b,en,d;
   input i;
   inout io;
   output [1:0] out;
   output 	o2;
   
   wire [1:0] 	vec;
   
   pad p0(.o(o2),.io(io),.en(en),.d(d));
   pad p1(.o(o2),.io(io),.en(~en),.d(d));

   pad p2(.o(o2),.io(vec[0]),.en(en),.d(d));
   pad p3(.o(o2),.io(vec[1]),.en(en),.d(d));

   flop f0(clk,a,b,i,vec);
   assign out = ~vec;
endmodule

module flop(clk,a,b,i,vec);
   input clk,a,b,i;
   output [1:0] vec;
   
   reg [1:0] 	vec;
   always @(posedge clk) begin
      vec[i] = a+b;
   end

endmodule

module pad(o,io,en,d);
   output o;
   output io;
   input  en,d;
   assign o = io;
   assign io = en ? d : 1'bz;
endmodule
