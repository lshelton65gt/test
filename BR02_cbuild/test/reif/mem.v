// if-to-case conversion with a memory
module top(c, a, o);
   input c;
   input [1:0] a;
   output [7:0] o;
   reg [7:0]    o;

   reg [7:0]    mem [3:0];
   initial
     begin
        mem[0] = 1;
        mem[1] = 2;
        mem[2] = 3;
        mem[3] = 4;
     end
   

   always @(posedge c)
     begin
        if (mem[a] == 0)
          o = 1;
        else if (mem[a] == 1)
          o = 2;
        else if (mem[a] == 2)
          o = 3;
        else if (mem[a] == 3)
          o = 4;
        else
          o = 255;
     end

endmodule

