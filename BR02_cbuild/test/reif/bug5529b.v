module top (clk, sel, a,b,c,o);
   output [7:0] o;
   input [7:0] a,b,c;
   input       clk;
   input [1:0] sel;
   reg   [7:0]      o;
   
   always @(posedge clk)
     begin
        if (!sel)
          o = a;
        else if (sel==1)
          o = b;
        else if (sel==2)
          o = c;
        else if (sel)           // must be 3...
          o = 0;
        else
          o = -1;
     end

   always @(negedge clk)
     begin
        if (sel)
          if (sel!=1)
            if (sel!=2)
              if (sel!=3)
                o=-1;
              else
                o = 0;          // sel==3
            else
              o = b;            // sel==2
          else
            o = a;              // sel==1
        else
          o = 0;                // sel==0
     end
endmodule
