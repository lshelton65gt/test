// Examples of valid clock gating
module top (q1, q2, q3, q4, clk, en0i, d1, d2, d3, d4);
   output q1, q2, q3, q4;
   input  clk, en0i, d1, d2, d3, d4;

   reg    q1, q2, q3, q4;
   reg    en0, en1;

   // Flop the enable inputs to help match with Aldec
   always @ (posedge clk)
     begin
        en0 <= en0i;
     end

   // Example 2
   always @ (posedge clk)
     en1 <= en0;

   wire   gclk1;
   assign gclk1 = en1 | clk;

   always @ (negedge gclk1)
     begin
        q1 <= d1;
        q2 <= d2;
        q3 <= d3;
        q4 <= d4;
     end

endmodule
