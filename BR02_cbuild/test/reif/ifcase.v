// simple tests of if-to-case-conversion

module top(i,o);
   input [31:0] i;
   output [7:0] o;

   modA a(i[7:0] ,o[0]);
   modB b(i,o[2:1]);
   modC c(i,o[3]);
   modD d(i,o[7:4]);
endmodule

module modA(i,o);
   input [7:0] i;
   output       o;
   reg [7:0]    t;
   
   assign       o = ^t;

   always @(i)
     begin
        if (i == 0)
          t = 4;
        else if (i == 1)
          t = 3;
        else if (i == 2)
          t = 5;
        else if (i == 3)
          t = 2;
        else if (i == 4)
          t = 6;
        else if (i == 5)
          t = 1;
        else if (i == 6)
          t = 7;
        else if (i == 7)
          t = 0;
        else
          // can't happen - fully specified in above cases - should warn?
          t = 9;
     end
endmodule

module modB(i,o);
   input [31:0] i;
   output[1:0]  o;
   reg [1:0]    t;
   
   assign o = t;

   always@(i)
     begin  // select a small piece
        if (i[1:0] == 0)
          t = 1;
        else if (i[1:0] == 1 || i[1:0] == 2) // fold interferes with reif here...
          t = 2;
        else
          t = 3;
     end
endmodule


module modC(i,o);               // Folded to boolean logic
   input [31:0]i;
   output      o;

   assign o = (i==0) ? 1
          : (i == 1) ? 0
          : (i == 2) ? 1
          : (i == 3) ? 1 
          : (i == 4) ? 0
          : (!i) ? 1               // equivalent to (i == 0)
          : 0;

endmodule

module modD(i,o);
   input [31:0] i;
   output [3:0] o;
   reg [3:0]    t;
   assign       o = t;
   
   // A case we should fold, but dont
   always @(i)
     if (i >= 0 && i < 7)
       t = 0;
     else if (i == 8)
       t = 1;
     else if (i >= 9 && i <= 32)
       t = 2;
     else
       t = 3;
endmodule

