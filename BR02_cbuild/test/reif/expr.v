// if-to-case conversion with an expression
module top(c, a, b, o);
   input c;
   input [7:0] a,b;
   output [7:0] o;

   reg [7:0]    o;
   always @(posedge c)
     begin
        if ((a+b) == 0)
          o = 1;
        else if ((a+b) == 1)
          o = 2;
        else if ((a+b) == 2)
          o = 3;
        else if ((a+b) == 3)
          o = 4;
        else
          o = 255;
     end

endmodule

