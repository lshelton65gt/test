// if-to-case conversions that shouldn't happen

module top(c,i,o);
   input c;
   input [63:0] i;
   output [15:0] o;
   reg [15:0]    o;

   always @(posedge c)
     begin                      // only allow 32 bit selectors
        if (i == 0)
          o = 1;
        else if (i == 1)
          o = 3;
        else if (i == 2)
          o = 6;
        else if (i == 3)
          o = 9;
        else if (i == 4)
          o = 12;
        else if (i == 5)
          o = 13;
        else
          o = 0;
     end

   always @(negedge c)          // Testing separate bits
     begin
        if (i[7:0] == 0)
          o = 32;
        else if (i[7:0] == 1)
          o = 31;
        else if (i[7:0] == 2)
          o = 32;
        else if (i[7:0] == 3)
          o = 33;
        else if (i[7:0] == 4)
          o = 34;
        else if (i[7:0] == 5)
          o = 35;
        else if (i[6:0] == 6)   // this one should be rejected.
          o = 36;
     end
endmodule

          
