module bug(clk,en,d,q);
  input clk;
  input [1:0] en;
  input d;
  output q;
  reg q;
  always @(posedge clk) begin
     if (en[0] | en[0])         // complain about duplicate test here
       q <= d;
     else if (en[1])            // no duplicate here
       q <= d;
  end
endmodule

