// This version of nonblock1 has a use of crq1 and crq0 after they are def'd.
// But since they are def'd with non-blocking assigns, there is really a use
// before kill, and I think this will be problematic for us.

module cds_asp2(chp_nmbr,cr_out,crq1,crq0,crq_a,crq_b); // carbon disallowFlattening
   input [1:0] chp_nmbr;
   input cr_out;
   output crq1;
   output crq0;
   output [1:0] crq_a, crq_b;
   chp_mux mst_tvg1(chp_nmbr,cr_out,crq1,crq0,crq_a);
   chp_mux mst_tvg2(~chp_nmbr,~cr_out,crq1,crq0,crq_b);
endmodule

module chp_mux(chp_nmbr,cr_out,crq1,crq0,crq);
   input [1:0] chp_nmbr;
   input cr_out;
   output crq1;
   output crq0;
   output [1:0] crq;
   reg 	 crq1;
   reg 	 crq0;
   reg [1:0] crq;
   
   always @(chp_nmbr or cr_out or crq1 or crq0) begin
      case(chp_nmbr)
	2'b00: begin
          crq1 = 1'bz;
          crq0 = cr_out;
        end
	2'b01: begin
          crq1 = cr_out;
          crq0 = 1'bz;
        end
	2'b10: begin
          crq1 = 1'bz;
          crq0 = 1'bz;
        end
	2'b11: begin
          crq1 = 1'bz;
          crq0 = 1'bz;
        end
      endcase
      crq = {crq1, crq0};
   end
endmodule
