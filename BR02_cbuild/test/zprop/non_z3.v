// In this variation, we have a second, non-flattened tristate buffer
// with a simple enable that is locally variable, but is based on
// an expression with a net that is elaboratedly constant.

module top(out1, out2, in1, en1, in2);
  output out1, out2;
  input  in1, en1, in2;

  // Instantiate a tristate buffer with a variable enable
  tribuf u1(out1, in1, en1);

  // Instantiate another tristate buffer that can be simplified
  tribuf u2(out2, in2, 1'b1);

  // Also drive the out2 with a non-z driver
  assign out2 = ~in2;
endmodule

module tribuf(out, in, ena);
  // carbon disallowFlattening
  output out;
  input  in, ena;
  assign out = !ena ? in : 1'bz;
endmodule
