module no_lost_z(a, b, out);
  input a, b;
  output out;

  assign out = a & b;
  assign out = a & b;           // redundant driver, no harm no foul
endmodule
