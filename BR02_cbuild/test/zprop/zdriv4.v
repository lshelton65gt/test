module zdriv(out, in1, in2, in3, ena1, ena2);
  output out;
  input  in1, in2, in3;
  input  ena1, ena2;
  wire   ena2_masked = ena2 & ~ena1;
  wire   ena3_masked = ~(ena1 | ena2_masked);
  assign out = ena1? in1: 1'bz;
  sub sub(out, ena2_masked, in2);
  assign out = ena3_masked? in3: 1'bz;
endmodule // zdriv

module sub(out, ena2, in2);
  output out;
  input  ena2, in2;
  wire   tmp = ena2? in2: 1'bz;
  assign out = tmp;
endmodule // sub

