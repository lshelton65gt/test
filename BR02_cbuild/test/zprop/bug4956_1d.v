// variation on bug4956_1c.v where the always-block is
// directly attached to the output, via a level of hierarchy
// that will be flattened

module top(in, en1, in2, en2_in, in3, xout);
  input in, in2, in3, en1, en2_in;
  output xout;
  
  wire   out_tmp;
  
  wire   en2 = en2_in & ~en1;
  wire   en3 = ~(en2 | en1);

  assign out_tmp = en1 ? ~in : 1'bz;

  assign xout = out_tmp;
  assign xout = en2 ?  in2 : 1'bz;

  sub sub(en3, in, xout);
endmodule

module sub(en3, in, xout);
  input en3, in;
  output xout;
  reg    xout;

  // carbon flattenModule

  // In this variation, we have another tristate driver,
  // so that there is always exactly one driver.  There
  // should be no Zs here, and we should get the right
  // answer all the time.
  
  always @(en3 or in)
    if (en3)
      xout = in;
    else
      xout = 1'bz;
endmodule
