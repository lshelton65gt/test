// this is a version of nonblock1.v, where I have temped the
// non-blocking assigns manually.  This fails in simulation due to
// our failure to propagate Zs through assigns.

module cds_asp2(chp_nmbr,cr_out,crq1,crq0); // carbon disallowFlattening
   input [1:0] chp_nmbr;
   input cr_out;
   output crq1;
   output crq0;

  // instantiate chp_mux twice with complementary selects, so
  // that we get 0 vs 1 mismatches when we don't implement Z correctly

   chp_mux mst_tvg1(chp_nmbr,~cr_out,crq1,crq0);
   chp_mux mst_tvg2(~chp_nmbr,cr_out,crq1,crq0);
endmodule

module chp_mux(chp_nmbr,cr_out,crq1,crq0);
   input [1:0] chp_nmbr;
   input cr_out;
   output crq1;
   output crq0;
   reg 	 crq1;
   reg 	 crq0;
   
   always @(chp_nmbr or cr_out) begin : scope
     reg reorder_crq1;
     reorder_crq1 = 1'b0;

     case(chp_nmbr)
       2'b00: begin
         reorder_crq1  = 1'bz;
         crq0  = cr_out;
       end
       2'b01: begin
         reorder_crq1  = cr_out;
         crq0  = 1'bz;
       end
       2'b10: begin
         reorder_crq1  = 1'bz;
         crq0  = 1'bz;
       end
       2'b11: begin
         reorder_crq1  = 1'bz;
         crq0  = 1'bz;
       end
     endcase
     crq1 = reorder_crq1;
   end
endmodule
