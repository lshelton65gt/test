module top(a, b, en, out);
  output out;
  input a, b;
  input  en;
  trireg tr;
  
  assign tr = en? a : 1'bz;
  assign out = ~tr;
endmodule
