module zdriv(out, in1, in2a, in2b, sel, in3, ena1, ena2);
  output out;
  input  in1, in3, sel;
  input [1:0] in2a, in2b;
  input  ena1, ena2;
  wire   ena2_masked = ena2 & ~ena1;      // exactly one driver always active
  wire   ena3_masked = ~(ena1 | ena2_masked);  
  wire   bus;
  assign   out = ~bus;

  assign bus = ena1? in1: 1'bz;
  wire [1:0] tmp1, tmp2, tmp;
  assign tmp1 = ena2_masked? in2a: 2'bzz;
  assign tmp2 = ena2_masked? in2b: 2'bzz;
  assign tmp = sel ? tmp1 : tmp2;
  assign bus = tmp[0];
  assign bus = ena3_masked? in3: 1'bz;
endmodule // zdriv
