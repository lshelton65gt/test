module top(a, b, en1, en2, out);
  output out;
  input a, b;
  input  en1, en2;
  wire   en2_masked = en2 & !en1;
  
  wire   tmp;
  assign tmp = en1? a : 1'bz;
  assign tmp = en2_masked? b : 1'bz;
  assign out = ~tmp;
endmodule
