// This testcase came from Pat and was inspired by Astute.  After
// constant propagation there should be no z/non-z conflicts, but
// we had to make constant propagation a bit more aggressive to
// realize this goal.

// Although this verilog code is really messy, it is not very
// difficult to deal with because constant propagation should
// completely eliminate the always-off tristate buffers.

// But there are situations where different constants going into
// a multiply instantiated tristate buffer prevent constant propagation
// from doing that job, and those are in non_z2.v, non_z3.v, non_z4.v.
// In the code, this is handled by RENetWarning, which looks up the
// elaborated enable values in the constant database, and uses those
// to simplify the enable expression.

module tb (chip_trst_L, clk, din, dout);
input clk, din;
output dout;
output chip_trst_L;

   wire trst;
   reg p_trst_;
   reg dout;

   assign trst=p_trst_;
   parameter simulation_mmc_cycle = 10.0;

   initial begin
/* JDM: This initial set of cycles before the delay kills our ability
   to match sim results, and is not related to the issue with this
   testcase

     p_trst_ <= 0;
     #simulation_mmc_cycle
*/
     p_trst_ <= 1;
   end

   always @(posedge clk or negedge chip_trst_L)
   begin
        if (chip_trst_L == 0)
            dout <= 0;
        else
            dout <= din;
   end

   tc_io tc_io (.trst_L(trst), .chip_trst_L(chip_trst_L));

endmodule


module tc_io (trst_L, chip_trst_L);

   input trst_L;
   output chip_trst_L;

   wire treset_L;

   assign chip_trst_L          =  treset_L;

  BD33LV_12_PU_A  trst_L_PAD (.PAD(trst_L),       .DOUT(treset_L),        .DIN(1'b0),     .OEN(1'b1));


endmodule

module LVIOOUT_PU (
	din,
	oen,
	pad
);

input  din;
input  oen;
inout  pad;
	
reg    pad_driver;
tri1   pad = pad_driver;
	

always @( din or oen )
begin
		if ( !oen )
			pad_driver = din;
		else
			pad_driver = 1'bz;
end

endmodule




module LVIOIN (
	pad,
	dout
);

inout  pad;
output dout;
			
reg    dout;

always @(pad )
begin
		dout = pad;
end
			
endmodule


`celldefine
module BD33LV_12_PU_A (PAD, DIN, DOUT, OEN);
   inout  PAD;
   input  DIN;
   output DOUT;
   input  OEN;
   
   LVIOOUT_PU LVIOOUT_PU  ( 
			    .din     ( DIN ),
			    .oen     ( OEN ),
			    .pad     ( PAD )
			    );
   
   LVIOIN LVIOIN (
		  .pad     ( PAD ),
		  .dout    ( DOUT )
		  );
   
   specify
      // Pin-to-pin timing.
      (DIN => PAD) = (0,0);
      (OEN => PAD) = (0,0,0,0,0,0);
      (PAD => DOUT) = (0,0);
   endspecify
   
endmodule
`endcelldefine


