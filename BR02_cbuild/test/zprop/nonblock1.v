module cds_asp2(chp_nmbr,cr_out,crq1,crq0); // carbon disallowFlattening
   input [1:0] chp_nmbr;
   input cr_out;
   output crq1;
   output crq0;
   chp_mux mst_tvg(chp_nmbr,cr_out,crq1,crq0);
endmodule

module chp_mux(chp_nmbr,cr_out,crq1,crq0);
   input [1:0] chp_nmbr;
   input cr_out;
   output crq1;
   output crq0;
   reg 	 crq1;
   reg 	 crq0;
   
   always @(chp_nmbr or cr_out) begin
      case(chp_nmbr)
	2'b00: begin
          crq1 <= 1'bz;
          crq0 <= cr_out;
        end
	2'b01: begin
          crq1 <= cr_out;
          crq0 <= 1'bz;
        end
	2'b10: begin
          crq1 <= 1'bz;
          crq0 <= 1'bz;
        end
	2'b11: begin
          crq1 <= 1'bz;
          crq0 <= 1'bz;
        end
      endcase
   end
endmodule
