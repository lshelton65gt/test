module zdriv(out, in1, in2, in3, ena1, ena2);
  output out;
  input  in1, in2, in3;
  input  ena1, ena2;
  wire   ena2_masked = ena2 & ~ena1;      // exactly one driver always active
  wire   ena3_masked = ~(ena1 | ena2_masked);  
  wire   bus;
  assign   out = ~bus;

  assign bus = ena1? in1: 1'bz;
  assign bus = ena2_masked? in2: 1'bz;
  assign bus = ena3_masked? in3: 1'bz;
endmodule // zdriv
