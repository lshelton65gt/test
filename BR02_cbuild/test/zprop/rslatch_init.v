module top(clk, r, s, out);
  input clk;
  input r, s;
  output out;
  reg    out, q, q_;

  // gate off s to disallow SET and RESET at the same time,
  // which could cause an unstable combinational cycle and
  // (I think) make Aldec hang or error out
  wire   s_gated = s & !r;

  integer    count;

  initial begin
    // start off in a legal state
    q = 1'b1;
    q_ = 1'b0;

    // start 'out' off in a different state than q.
    // if out&q are aliased, that will zero q when it
    // should stay 1 until count gets to 10, at which
    // point it will follow the r & s control lines.
    //
    // Actually I was never able to get the simulations to
    // differ, and we are relying only on verboseAliasing
    // to make sure we don't start aliasing out & q again
    out = 1'b0;

    count = 0;
  end

  // this counter is to enforce that testdriver doesn't immediately
  // reset the outputs, as we want to show a diff based on the initial
  // driver conflict.  In the simulation, you will see the first 40
  // lines mismatch, and thereafter everything matches.  It's only
  // the initial state that's broken due to the unfortunate assignment
  // aliasing
  always @(posedge clk)
    count <= count + 1;

  always @(q or clk) // 'clk' condition to make aldec evaluate this at time 0
    out = q;    // if we assignment-alias out&q, there will be sim mismatches
                // due to conflicting initial values

  always @(r or q_)
    if (count > 10)
      q = !(r | q_);

  always @(s_gated or q)
    if (count > 10)
      q_ = !(s_gated | q);
endmodule // top
