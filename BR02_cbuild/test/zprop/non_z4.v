// In this variation, we need two variable subsitutions followed
// by a Fold simplification to discover that the elaborated enable
// is constant

module top(out1, out2, in1, en1, en2, in2);
  output out1, out2;
  input  in1, en1, en2, in2;

  // Instantiate a tristate buffer with a variable enable
  tribuf u1(out1, in1, en1, en2);

  // Instantiate another tristate buffer that can be simplified
  tribuf u2(out2, in2, 1'b1, 1'b0);

  // Also drive the out2 with a non-z driver
  assign out2 = ~in2;
endmodule

module tribuf(out, in, ena1, ena2);
  // carbon disallowFlattening
  output out;
  input  in, ena1, ena2;
  assign out = !(ena1 ^ ena2) ? in : 1'bz;
endmodule
