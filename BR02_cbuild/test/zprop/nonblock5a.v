// This is a modified version of nonblock5.v where the assignments
// broken out into continous assigns causes z loss/conflict alerts.
// This failed before my change to fix a bug in concat rewrite. We
// found it because my change to fix concat rewrite (bug7178)
// causes the assignment to crq to get turned into individual
// cont assigns.

module cds_asp2(chp_nmbr,cr_out,crq1,crq0,crq); // carbon disallowFlattening
   input [1:0] chp_nmbr;
   input cr_out;
   output crq1;
   output crq0;
   output [1:0] crq;
   chp_mux mst_tvg1(chp_nmbr,~cr_out,crq1,crq0,crq);
   chp_mux mst_tvg2(~chp_nmbr,cr_out,crq1,crq0,crq);
endmodule

module chp_mux(chp_nmbr,cr_out,crq1,crq0,crq);
   input [1:0] chp_nmbr;
   input cr_out;
   output crq1;
   output crq0;
   output [1:0] crq;
   reg 	 crq1;
   reg 	 crq0;
   //reg [1:0] crq;
   
`ifdef CARBON
`else
   initial begin
     crq1 = 1'b0;
     crq0 = 1'b0;
     crq = 2'b0;
   end
`endif

   always @(chp_nmbr or cr_out or crq1 or crq0) begin
      case(chp_nmbr)
	2'b00: begin
          crq1 <= 1'bz;
          crq0 <= cr_out;
        end
	2'b01: begin
          crq1 <= cr_out;
          crq0 <= 1'bz;
        end
	2'b10: begin
          crq1 <= 1'bz;
          crq0 <= 1'bz;
        end
	2'b11: begin
          crq1 <= 1'bz;
          crq0 <= 1'bz;
        end
      endcase
   end

  assign crq[1] = crq1;
  assign crq[0] = crq0; // use before kill
endmodule
