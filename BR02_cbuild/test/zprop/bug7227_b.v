// last mod: Tue Jun  5 10:12:13 2007
// filename: test/zprop/bug7227_b.v
// Description:  This test duplicates the problem seen at Panasonic in bug 7227.
// Somehow we get a net that is elaborated live, but unelaborated dead
// cbuild exits with an elaborated sanity check failure


module bug7227_b(out1, out2, clk);
   output out1, out2;
   input  clk;

   wire   temp1, temp2;
   wire   ou1, ou2;
   wire   locz;
   pulldown P2(temp1);

   assign out1 = S1.reg1;
   assign out2 = ou2;

   sub S1 (clk, temp1, ou1);
   sub S2 (clk, temp2, ou2);

endmodule

module sub(clk, d, out1);
   input clk;
   
   inout d; // carbon depositSignal
   output out1;
   
   pulldown P0 (d);

   wire   d2;
   assign d2 = d;

   reg 	  reg1;
   assign out1 = reg1;
   always @ (posedge clk)
     begin
	reg1 = d2;
     end

endmodule

