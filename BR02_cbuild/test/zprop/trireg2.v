// Note -- Aldec gets the wrong answer in this case, use Finsim

module top(a, b, en1, en2, out);
  output out;
  input a, b;
  input  en1, en2;
  wire   en2_masked = en2 & !en1;
  trireg tr;

  assign tr = en1? a : 1'bz;
  assign tr = en2_masked? b : 1'bz;
  assign out = ~tr;
endmodule
