// last mod: Tue Jun  5 10:18:16 2007
// filename: test/zprop/bug7227_c.v
// Description:  This test is a more complex version of bug7227, it includes more hierrefs


module bug7227_c(clock, in1, in2, outA, outB, outC, outD, outE);
   input clock;
   input [7:0] in1, in2;
   output [7:0] outA, outB;
   output [3:0] outC;
   output [7:0] outD, outE;

   wire [7:0] 	unused1;

   assign 	outC = u_left.u_midl.u_up.w1;
   assign       outD = u_left.u_midl.out_hr;
   assign       outE = u_right.u_midr.out_hr;

   left  u_left(clock, in1, in2, outA);
   right u_right(clock, in1, in2, outB);
	       
endmodule


module left(clock, in1, in2, out_left);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out_left;

   midl u_midl(clock, in1, in2, out_left);
endmodule

module right(clock, in1, in2, out_right);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out_right;

   midr u_midr(clock, in1, in2, out_right);
endmodule


module midl(clock, in1, in2, out_midl);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out_midl;

   wire   [7:0] out_hr;

   assign 	out_hr = u_right.u_midr.u_dn.w1;
   
   up u_up(clock, in1, in2, out_midl[7:4]);
   dn u_dn(clock, in1, in2, out_midl[3:0]);
endmodule

module midr(clock, in1, in2, out_midr);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out_midr;
   wire   [7:0] out_hr;

   assign 	out_hr = u_left.u_midl.u_dn.w1;

   up u_up(clock, in1, in2, out_midr[7:4]);
   dn u_dn(clock, in1, in2, out_midr[3:0]);
endmodule

module up(clock, in1, in2, out_up);
   input clock;
   input [7:0] in1, in2;
   output [7:4] out_up;
   reg [7:4] out_up;
   wire [3:0] w1;

   assign w1 = in1[7:4] | in2[7:4];

   always @(posedge clock)
     out_up = in1[7:4] & in2[7:4];
endmodule

module dn(clock, in1, in2, out_dn);
   input clock;
   input [7:0] in1, in2;
   output [3:0] out_dn;
   reg [3:0] out_dn;
   wire [7:0] w1;

   assign w1 = {in1[3:0], in2[3:0]};

   always @(posedge clock)
     out_dn = in1[3:0] & in2[3:0];
endmodule
