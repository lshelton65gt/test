

module tribuf32(out, in, ena);
   output [31:0] out;
   input [31:0] in;
   input ena;

   assign out = ena? in: 1'bz;
   // Meant 32'bz, result is multi-driver conflict,
   // but the multi-driver conflict does not produce an alert because there
   // are no lost Z values in the fanin for out[31:1], and we get the
   // right answer because all the 'else' values are 0
 endmodule

module top(a1, a2, b1, b2, ena1, ena2, sum1, sum2, sum3);
   input [31:0] a1, a2, b1, b2;
   input ena1, ena2;
   output [31:0] sum1, sum2, sum3;
   wire [31:0] a, b;                // a and b are marked tristate

   tribuf32 u1(a, a1, ena1);
   tribuf32 u2(a, a2, ena2);

   tribuf32 u3(b, b1, ena1);
   tribuf32 u4(b, b2, ena2);

   add32 u5(sum1, a, b);
   add32 u6(sum2, a1, b1);
   add32 u7(sum3, a2, b2);
 endmodule

 module add32(sum, a, b);
   input [31:0] a, b;
   output [31:0] sum;
   assign sum = a + b;              // a and b are *not* marked tristate
 endmodule
