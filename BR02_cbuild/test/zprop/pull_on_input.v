module top(in, en1, in2, en2_in, en3_in, in3, xout);
  input in, in2, in3, en1, en2_in, en3_in;
  output xout;
  
  wire   out_tmp;
  reg    out_r;
  
  wire   en2 = en2_in & ~en1;
  wire   en3 = en3_in & ~(en1 | en2);

  assign out_tmp = en1 ? ~in : 1'bz;

  // This pulldown will prevent the aliasing of out_tmp, and
  // Carbon will not be able to get the right answer, and should
  // give an alert.
  pulldown(out_tmp);

  assign xout = out_tmp;
  assign xout = out_r;
  assign xout = en2 ?  in2 : 1'bz;

  // In this variation, we have another tristate driver,
  // so that there is always exactly one driver.  There
  // should be no Zs here, and we should get the right
  // answer all the time.
  
  always @(en3 or in)
    if (en3)
      out_r = in;
    else
      out_r = 1'bz;
endmodule
