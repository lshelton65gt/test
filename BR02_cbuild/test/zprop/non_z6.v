// In this example we finally run out of gas.  Constant Propagation
// could conceivably see that elaboratedly u2.ena is 0 because it's
// driven from (top.en1 ^ top.en1).  I'm not sure why that doesn't
// happen.  But I'm going to turn that over to Richard.

module top(out1, out2, in1, en1, en2, in2);
  output out1, out2;
  input  in1, en1, en2, in2;

  // Instantiate a tristate buffer with a variable enable
  tribuf u1(out1, in1, en1, en2);

  // Instantiate another tristate buffer that can be simplified
  tribuf u2(out2, in2, en1, en1);

  // Also drive the out2 with a non-z driver
  assign out2 = ~in2;
endmodule

module tribuf(out, in, ena1, ena2);
  // carbon disallowFlattening
  output out;
  input  in, ena1, ena2;
  wire ena = (ena1 ^ ena2);
  assign out = ena ? in : 1'bz;
endmodule
