module cds_asp2(chp_nmbr,en,cr_out,crq); // carbon disallowFlattening
   input [1:0] chp_nmbr;
   input [3:0] en;
   input       cr_out;
   output [3:0] crq;
   
   scanner scan1(chp_nmbr,cr_out,crq);
   scanner stare1(chp_nmbr,cr_out,crq);
   spc spc(en,cr_out,crq);
endmodule

module spc(en,s_o_crq1,crq);
   input [3:0] en;
   input s_o_crq1;
   inout [3:0] crq;
   assign crq[0] = en[0] ? s_o_crq1 : 1'bz;
   assign crq[1] = en[1] ? s_o_crq1 : 1'bz;
   assign crq[2] = en[2] ? s_o_crq1 : 1'bz;
   assign crq[3] = en[3] ? s_o_crq1 : 1'bz;
endmodule
   
module scanner(chp_nmbr,cr_out,crq);
   input [1:0] chp_nmbr;
   input       cr_out;
   output [3:0] crq;
   chp_mux mst_tvg(chp_nmbr,cr_out,crq[3],crq[2],crq[1],crq[0]);
endmodule

// DefNet: cds_asp2.scan1.mst_tvg.crq0
// Scope: cds_asp2.scan1.mst_tvg
module chp_mux(chp_nmbr,cr_out,crq3,crq2,crq1,crq0);
   input [1:0] chp_nmbr;
   input cr_out;
   output crq3;
   output crq2;
   output crq1;
   output crq0;
   reg 	 crq3;
   reg 	 crq2;
   reg 	 crq1;
   reg 	 crq0;
   
   // $splat_$splatlocal_$rescope_$drive_$reorder_crq0;2
   // 1. reorder
   // 2. z-rewrite
   // 3. rescope
   // 4. cycle-splitting
   always @(cr_out or chp_nmbr) begin
      // how do we get $drive in here?
      case(chp_nmbr)
	2'b00: {crq3,crq2,crq1,crq0} <= {1'bz,1'bz,1'bz,cr_out};
	2'b01: {crq3,crq2,crq1,crq0} <= {1'bz,1'bz,cr_out,1'bz};
	2'b10: {crq3,crq2,crq1,crq0} <= {1'bz,cr_out,1'bz,1'bz};
	2'b11: {crq3,crq2,crq1,crq0} <= {cr_out,1'bz,1'bz,1'bz};
      endcase
   end
endmodule
