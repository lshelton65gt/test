module cds_asp2(chp_nmbr,en,cr_out,crq1,crq0);
   input [0:0] chp_nmbr;
   input [1:0] en;
   input       cr_out;
   output crq1,crq0;
   
   assign crq0 = en[0] ? cr_out : 1'bz;
   assign crq1 = en[1] ? cr_out : 1'bz;
   scanner scan1(chp_nmbr,cr_out,crq1,crq0);
endmodule

module scanner(chp_nmbr,cr_out,crq1,crq0);
   input [0:0] chp_nmbr;
   input cr_out;
   output crq1;
   output crq0;
   reg 	 crq1;
   reg 	 crq0;

`ifdef CARBON
`else
   initial begin
      crq1 = 1'b0;
      crq0 = 1'b0;
   end
`endif
   
   // $splat_$splatlocal_$rescope_$drive_$reorder_crq0;2
   // 1. reorder
   // 2. z-rewrite
   // 3. rescope
   // 4. cycle-splitting
   always @(cr_out or chp_nmbr) begin
      // how do we get $drive in here?
      case(chp_nmbr)
	1'b0: begin crq1<=1'bz;crq0<=cr_out; end
	1'b1: begin crq1<=cr_out;crq0<=1'bz; end
	default: begin crq1<=1'bz;crq0<=1'bz; end
      endcase
   end
endmodule
