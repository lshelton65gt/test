module top(out, in, in0, in1, e1, e2);
  output [1:0] out;
  input [1:0]  in;
  input  in0, in1, e1, e2;

  assign out = e1 ? in : 2'bz;     // z-driver covering 2 bits (in conflict)
  assign out[0] = e2 ? in0 : 1'bz; // z-driver covering 1 bit (not in conflict)
  assign out[1] = ~in1;            // non-z driver covreing 1 bit (in conflict)
endmodule
