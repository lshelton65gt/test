// last mod: Tue Jun  5 10:10:41 2007
// filename: test/zprop/bug7227_a.v
// Description:  This test is a variation of the test that duplicates the
// problem seen at Panasonic in bug 7227.
// here there is no hierref assign to out1
// see bug7227.v for the test that caused a problem


module bug7227_a(out1, out2, clk);
   output out1, out2;
   input  clk;

   wire   temp1, temp2;
   wire   ou1, ou2;
   wire   locz;
   pulldown P2(temp1);

   assign out1 = ou1;
   assign out2 = ou2;

   sub S1 (clk, temp1, ou1);
   sub S2 (clk, temp2, ou2);

endmodule

module sub(clk, d, out1);
   input clk;
   
   inout d; // carbon depositSignal
   output out1;
   
   pulldown P0 (d);

   wire   d2;
   assign d2 = d;

   reg 	  reg1;
   assign out1 = reg1;
   always @ (posedge clk)
     begin
	reg1 = d2;
     end

endmodule

