// last mod: Wed May 30 13:49:33 2007
// filename: test/zprop/bug7227.v
// Description:  This test duplicates the problem seen at Panasonic in bug 7227.
// Somehow we get a net that is elaborated live, but unelaborated dead
// cbuild exits with an elaborated sanity check failure
// to duplicate the problem run with the -noFlatten switch


module bug7227(out1, out2, clk, d);
   output out1, out2;
   input  clk, d;

   wire   temp1, temp2;
   wire   ou1, ou2;
   wire   locz;
   pulldown P2(temp1);

   assign out1 = S1.reg1;
   assign out2 = ou2;

   sub S1 (clk, temp1, ou1);
   sub S2 (clk, temp2, ou2);

endmodule

module sub(clk, d, out1);
   input clk;
   
   inout d; // carbon depositSignal
   output out1;
   
   pulldown P0 (d);

   wire   d2;
   assign d2 = d;

   reg 	  reg1;
   assign out1 = reg1;
   always @ (posedge clk)
     begin
	reg1 = d2;
     end

endmodule

