module bug(clk,en1,en2);
   input clk,en1,en2;

   wire [3:0] data;

   wrap wrap(clk,en1,en2&~en1);
   always @(posedge clk)
     $display("data = %x", data);
endmodule

module wrap(clk,en1,en2);
   input clk,en1,en2;
   mover mov1(clk,en1,bug.data[1:0],bug.data[3:2]);
   mover mov2(clk,en2,bug.data[3:2],bug.data[1:0]);
endmodule
   
module mover(clk,en,a,b);
   input clk,en;
   output [1:0] a,b;
   assign a = en ? 2'b10 : 2'bz;
   assign b = en ? 2'b01 : 2'bz;

`ifdef WORKAROUND
   wire   dummy = a & b;
`endif
endmodule
