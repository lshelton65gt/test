module lost_z(in1, in2, ena1, ena2, out1, out2);
  input [1:0] in1, in2;
  input       ena1, ena2;
  output out1, out2;

  wire [1:0] v = ena1 ? in1[1:0] : 2'bzz;
  wire [1:0] w = ena2 ? in2[1:0] : 2'bzz;
  assign out1    = v[0]; // Carbon loses Z-state on the bit-select
  assign out1    = w[0];
  assign out2    = v[1];
  assign out2    = w[1];
endmodule

