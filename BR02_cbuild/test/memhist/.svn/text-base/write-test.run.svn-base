#!/bin/csh -f

# Make sure that we histogram alloc/realloc/free, both the for the
# overhead versions (carbonmem_malloc,carbonmem_realloc,carbonmem_free),
# and the non-overhead versions (carbon_alloc,carbonmem_reallocate,
# carbonmem_dealloc).

set linux64 = 0
if ($?CARBON_HOST_ARCH == 1) then
  if ($CARBON_HOST_ARCH == Linux64) set linux64 = 1
endif


foreach i (no_overhead overhead)
  echo '$CARBON_HOME/bin/carbon_exec util/testsuite/utiltest memhist' $i '-dumpMemory mem.dump >& memhist.log'
  echo 'carbondb -memDebug mem.dump carbondb.log -memDetail 1 >& carbondb.log'
  foreach j (start small_block small_block_freed large_block \
             large_block_freed realloc_same_block \
             realloc_bigger_block realloc_smaller_block \
             realloc_large_block realloc_larger_block \
             realloc_freed deep5 deep6 deep7 deep8 deep9 \
             deep10 deep11 deep12 deep13 deep14)

    # the gold files have 'unsigned' in them, which matches the Linux32 and
    # Solaris results, but the Linux64 results say 'unsigned long'.  Use
    # sed to convert the memhist log files to 32-bit format
    if ($linux64 == 1) then
      echo 'sed -e "s/unsigned long/unsigned/g"' "<memhist.$j >& memhist.$j.$$"
      echo "mv memhist.$j.$$ memhist.$j"
    else
      echo 'sed -e "s/unsigned int/unsigned/g"' "<memhist.$j >& memhist.$j.$$"
      echo "mv memhist.$j.$$ memhist.$j"
    endif
    echo "diff -b memhist.$j.gold memhist.$j >& memhist.$j.diff"
  end    
end

# assocred.v was borrowed from test/fold.  It does enough BDD-folding
# that the BDD package calls realloc, which was a source of trouble
# for memory histogramming.  There is also some targeting memory histogram
# testing in test/unit-util, using "utiltest memhist".

echo 'cbuild -q assocred.v -2001 -dumpMemory mem.dump -nocc >& cbld.log'
echo 'carbondb -memDebug mem.dump -memDetail 0 >& carbondb0.log'
echo 'carbondb -memDebug mem.dump -memDetail 1 >& carbondb1.log'
echo 'carbondb -memDebug mem.dump -memDetail 2 >& carbondb2.log'
echo 'carbondb -memDebug mem.dump -memDetail 3 >& carbondb3.log'
echo 'carbondb -memDebug mem.dump -memDetail 0 -memPointers >& carbondb1p.log'
echo 'carbondb -memDebug mem.dump -memDetail 1 -memPointers >& carbondb2p.log'
echo 'carbondb -memDebug mem.dump -memDetail 2 -memPointers >& carbondb3p.log'
echo 'carbondb -memDebug mem.dump -memDetail 3 -memPointers >& carbondb4p.log'


# a test to check that -dumpMemory fails as expected when -dumpMemory
# switch is specified but not on the command line

#XFAIL cbuild -q -nocc  rand1.v -f dumpMemory.cmd >& test-dumpMemory2.cbld.log
