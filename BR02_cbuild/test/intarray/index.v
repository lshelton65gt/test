module top(clk, index, data, err);
input clk, index, data;
output err;

wire clk;
wire [3:0] index;
wire [31:0] data;

integer arr1 [0:15];
integer arr2 [16:31];
integer arr3 [15:0];
integer arr4 [31:16];

integer i;

initial
begin
  for (i = 0; i < 16; i=i+1)
  begin
    arr1[i] = i;
    arr2[i | 5'b10000] = i;
    arr3[15-i] = ~i; 
    arr4[i + 16] = ~i;
  end  
end

reg err;
initial err = 1'b0;

integer sum;
always @(posedge clk)
begin
  arr1[index] = data;
  arr2[index | 5'b10000] = data;
  arr3[15-index] = ~data; 
  arr4[index + 16] = ~data;

  err = 1'b0;
  for (i = 0; i < 16; i=i+1)
  begin
    sum = arr1[i] + arr2[i+16] + arr3[15-i] + arr4[5'b10000 | i];
    if (sum != -2)
    begin
      err = 1'b1;
      $display("mismatch at index = %d sum = %d", i, sum);
      $display("arr1[%d] = %d", i, arr1[i]);
      $display("arr2[%d] = %d", i+16, arr2[i+16]);
      $display("arr3[%d] = %d", 15-i, arr3[15-i]);
      $display("arr4[%d] = %d", 5'b10000 | i, arr4[5'b10000 | i]);
    end
  end
end

endmodule
