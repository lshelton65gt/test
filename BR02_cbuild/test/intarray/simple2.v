// This test behaves differently with -2001 and without.
// The signedness of 'arr[i]' is always UNSIGNED without -2001,
// while with -2001, we print a signed number.

module top(clk, out1, out2, out3);
input clk;
output out1, out2, out3;

   wire clk;
   wire [31:0] out1;
   wire [31:0] out2;
   wire [31:0] out3;

   integer arr [-16:15];
   integer a;
   initial a = 7;
   integer b;
   initial b = -8;

   integer i;

   always @(posedge clk)
   begin
     arr[a-b] = a+b;
     a = a - 1;
     b = b + 1;
     if (b == 8)
     begin
       for (i=-16; i < 16; i = i+1)
         $display("arr[%d] = %d", i, arr[i]);
     end
   end

   assign out1 = a;
   assign out2 = b;
   assign out3 = arr[a-b+2];

endmodule
