module top(clk,a,b,result);
input clk, a, b;
output result;

wire clk;
wire [7:0] a;
wire [7:0] b;
wire [7:0] result;

wire en;
wire done;
wire [7:0] q;
wire [7:0] r;
divmod DM (clk, en, a, b, q, r, done);

assign en = done & (b != 0);
assign result = done ? r : 0;

endmodule

module divmod(clk,en,n,d,q,r,done);
input clk,en,n,d;
output q,r,done;

   wire clk;
   wire en;
   wire [30:0] n;
   wire [30:0] d;
   wire [30:0] q;
   wire [30:0] r;
   reg done;

   initial done = 1'b1;

   integer arr [0:2];

   always @(posedge clk)
   begin
     if (en)
     begin
       arr[0] = n;
       arr[1] = d;
       arr[2] = 0;
       done = 1'b0;
       $display("starting %d / %d", arr[0], arr[1]);
     end
     if (arr[0] < arr[1])
     begin
       $display("q = %d r = %d", arr[2], arr[0]);
       done = 1'b1;
     end
     else
     begin
       arr[0] = arr[0] - arr[1];
       arr[2] = arr[2] + 1;
     end
   end

   assign q = arr[2];
   assign r = arr[0];

endmodule
