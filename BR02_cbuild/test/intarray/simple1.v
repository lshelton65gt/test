module top(clk, index, incr, out);
input clk, index, incr;
output out;

   wire clk;
   wire [2:0] index;
   wire [7:0] incr;   
   wire [31:0] out;

   integer arr [0:7];

   always @(posedge clk)
      arr[index] = arr[index] + incr;

   assign out = arr[index];

endmodule
