module top(clk,a,b,result);
input clk, a, b;
output result;

wire clk;
wire [7:0] a;
wire [7:0] b;
wire [7:0] result;

wire en;
wire done;
wire [7:0] q;
wire [7:0] r;

gcd GCD(clk, en, a, b, r, done);

assign en = done;
assign result = done ? r : 0;

endmodule

module gcd(clk,en,a,b,r,done);
input clk,en,a,b;
output r,done;

   wire clk;
   wire en;
   wire [30:0] a;
   wire [30:0] b;
   reg [30:0] r;
   reg done;

   initial done = 1'b1;

   integer arr [0:1];
   integer saved_b;
   wire [30:0] a_mod_b;
   reg div_en;
   initial div_en = 1'b0;
   reg div_in_progress;
   initial div_in_progress = 1'b0;
   wire div_done;

   /* STATES:
      0 - initial state
      1 - start modulo
      2 - waiting for module to finish
      3 - module done
    */
   reg [1:0] state;
   initial state = 0;

   always @(posedge clk)
   begin
     case (state)
     0: if (en)
        begin
          if (a > b)
          begin
            arr[0] = a;
            arr[1] = b;
          end
          else
          begin
            arr[0] = b;
            arr[1] = a;
          end
          r = 0;
          done = 0;
          state = 1;
          $display("starting GCD(%d,%d)", arr[0], arr[1]);
        end
     1: begin
          saved_b = arr[1];
          state = 2;
          div_en = 1'b1;
        end
     2: begin
          if (!div_en && div_done)
          begin
            arr[0] = saved_b;
            state = 3;
          end
          div_en = 1'b0;
        end
     3: if (a_mod_b == 0)
        begin
          $display("result  = %d", arr[0]);
          r = arr[0];
          done = 1'b1;
          state = 0;
        end
        else
        begin
         arr[1] = a_mod_b;
         $display("intermediate result, a = %d b = %d", arr[0], arr[1]);
         state = 1;
        end
     endcase
   end

   wire unused;
   divmod DM (clk, div_en, arr[0], arr[1], unused, a_mod_b, div_done);

endmodule

module divmod(clk,en,n,d,q,r,done);
input clk,en,n,d;
output q,r,done;

   wire clk;
   wire en;
   wire [30:0] n;
   wire [30:0] d;
   wire [30:0] q;
   wire [30:0] r;
   reg done;

   initial done = 1'b1;

   integer arr [0:2];

   always @(posedge clk)
   begin
     if (en)
     begin
       arr[0] = n;
       arr[1] = d;
       arr[2] = 0;
       done = 1'b0;
     end
     if (arr[0] < arr[1])
     begin
       done = 1'b1;
     end
     else
     begin
       arr[0] = arr[0] - arr[1];
       arr[2] = arr[2] + 1;
     end
   end

   assign q = arr[2];
   assign r = arr[0];

endmodule
