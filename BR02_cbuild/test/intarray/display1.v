module top(clk);
input clk;

   wire clk;
   integer arr [0:7];
   integer val;
   initial val = 3;
   integer idx;
   initial idx = 0;

   integer i;

   always @(posedge clk)
   begin
     if (idx < 8)
     begin
       arr[idx] = val;
       val = val - 1;
       idx = idx + 1;
     end
     else
     begin
       for (i = 0; i < 8; i=i+1)
         $display("arr[%d] = %d", i, arr[i]);
       idx = 0;
       val = -val;
     end
   end

endmodule
