library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity bug8331_01 is
  port (
    clk,rst  : in  std_logic;
    in1      : in  std_logic_vector(5 downto 2);
    out1     : out std_logic_vector(5 downto 2));
end;

architecture arch of bug8331_01 is
  signal temp1: std_logic_vector(5 downto 2);  -- carbon1 observeSignal
begin

  out1 <= temp1;

  p1: process(rst, clk, in1)
  begin
    if(rst = '1') then
      temp1 <= (others => '0');
    elsif (clk'event and clk = '1') then 
      temp1 <= in1;
    end if;
  end process;
  
end arch;

