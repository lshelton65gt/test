// This demonstrates compiling and running a simple design with
// two counters driven by two clocks

module twocounter(clk1, clk2, reset1, reset2, out1, out2);
  input clk1, clk2, reset1, reset2;
  output [31:0] out1, out2;
  reg [31:0] out1, out2;

  always @(posedge clk1)
    if (reset1)
      out1 <= 32'b0;
    else
      out1 <= out1 + 32'd1;

  always @(posedge clk2)
    if (reset2)
      out2 <= 32'b0;
    else
      out2 <= out2 + 32'd3;
endmodule
