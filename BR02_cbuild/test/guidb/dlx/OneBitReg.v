`include "sdlx.h"

module OneBitReg (RegOut, RegIn, RegRW, Clk);
   output  RegOut;
   input   RegIn;
   input   RegRW;
   input   Clk;

   reg 	   RegBuf;
   wire    RegOut = RegBuf;

   initial RegBuf = 0;

   always @(negedge Clk) 
     begin
	if (RegRW) #0 RegBuf = RegIn;
     end

endmodule /* OneBitReg */
