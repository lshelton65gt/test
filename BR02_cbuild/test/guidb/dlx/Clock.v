`include "sdlx.h"

`ifdef CARBON
`else
module Clock (clk);
   output clk;
   reg 	  clk;

   initial
      clk = 0;

   always
      #(`CLOCKPERIOD / 2)     clk = ~clk;

endmodule /* Clock */
`endif





