`include "alu.h"
`include "sdlx.h"

module ALUControl(ALUop, OPcode, Func, ALUCtrl);

   input  [`ALUO] ALUop;
   input  [5:0]   OPcode;
   input  [10:0]  Func;
   output [`ALUC] ALUCtrl;
   
   reg    [`ALUC] ALUCtrl;

   initial
     ALUCtrl = 3'b000;

   always @(ALUop or OPcode or Func) 
     begin
	if (ALUop == `ITYPE)
	  begin
	     casex (OPcode)
               `ADDI, `ADDUI:  ALUCtrl = `ALUADD;
               `SUBI, `SUBUI:  ALUCtrl = `ALUSUB;
	       `ANDI:  ALUCtrl = `ALUAND;
	       `ORI:   ALUCtrl = `ALUOR;
	       `XORI:  ALUCtrl = `ALUXOR;
	       `SLLI:  ALUCtrl = `ALUSLL;
	       `SRLI:  ALUCtrl = `ALUSRL;
	       `SRAI:  ALUCtrl = `ALUSRA;
	       `SLTI:  ALUCtrl = `ALUSLT;
	       `SGTI:  ALUCtrl = `ALUSGT;
	       `SLEI:  ALUCtrl = `ALUSLE;
	       `SGEI:  ALUCtrl = `ALUSGE;
	       `SEQI:  ALUCtrl = `ALUSEQ;
	       `SNEI:  ALUCtrl = `ALUSNE;
	       `LHI:   ALUCtrl = `ALULH;
               default: 
		 begin
		 end
	     endcase // casex(OPcode)
	  end // if (ALUop == `ITYPE)
	else if (ALUop == `BRANCH)
	  ALUCtrl = `ALUADD;
	else if (ALUop == `MEMACC)
	  ALUCtrl = `ALUADD;
	else if (ALUop == `RTYPE)
	  begin
	     casex (Func)
               `AND:  ALUCtrl = `ALUAND;
               `OR:   ALUCtrl = `ALUOR;
               `XOR:  ALUCtrl = `ALUXOR;
               `SLL:  ALUCtrl = `ALUSLL;
               `SRL:  ALUCtrl = `ALUSRL;
               `SRA:  ALUCtrl = `ALUSRA;
               `ADD, `ADDU: ALUCtrl = `ALUADD;
               `SUB, `SUBU: ALUCtrl = `ALUSUB;
               `MUL:  ALUCtrl = `ALUMUL;
               `SLT:  ALUCtrl = `ALUSLT;
               `SGT:  ALUCtrl = `ALUSGT;
               `SLE:  ALUCtrl = `ALUSLE;
               `SGE:  ALUCtrl = `ALUSGE;
               `SEQ:  ALUCtrl = `ALUSEQ;
               `SNE:  ALUCtrl = `ALUSNE;
               default: 
		 begin
		 end
	     endcase // casex(Func)
	  end // if (ALUop == `RTYPE)
     end // always @ (ALUop or OPcode or Func)
endmodule  /* ALUControl */



