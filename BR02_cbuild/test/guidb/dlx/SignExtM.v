`include "sdlx.h"

module SignExtM (S, I, O);

   input  [2:0]   S;     // Select Offset or Imm
   input  [`DATA] I;
   output [`DATA] O;

   wire [`DATA]   O = (S==1) ? {{16 {I[15]}}, I[15:0]} : 
                      (S==2) ? {16'b0, I[15:0]} :
                      (S==3) ? {{24 {I[7]}}, I[7:0]} :
                      (S==4) ? {24'b0, I[7:0]} :
		      I[31:0];

endmodule  /* SignExtM */
