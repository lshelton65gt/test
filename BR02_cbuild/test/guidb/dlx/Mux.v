`include "sdlx.h"

module Mux (S, A, B, Z);

   input          S;      // Select line
   input  [`DATA] A;
   input  [`DATA] B;
   output [`DATA] Z;

   wire   [`DATA] Z = S ? B : A;

endmodule /* Mux */
