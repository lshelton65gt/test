`include "sdlx.h"

module RegFile (RdReg1, RdReg2, WrtReg, WrtData, R1Out, R2Out, RegWrite, Clk,
		r0, r1, r2, r3, r4, r5, r6, r7, 
		r8, r9, r10, r11, r12, r13, r14, r15,
		r16, r17, r18, r19, r20, r21, r22, r23,
		r24, r25, r26, r27, r28, r29, r30, r31);
   input  [`RDDR] RdReg1, RdReg2, WrtReg;  
   input  [`DATA] WrtData; 
   output [`DATA] R1Out; 
   output [`DATA] R2Out; 
   input          RegWrite; 
   input          Clk;
   output [`DATA] r0, r1, r2, r3, r4, r5, r6, r7, 
		  r8, r9, r10, r11, r12, r13, r14, r15,
		  r16, r17, r18, r19, r20, r21, r22, r23,
		  r24, r25, r26, r27, r28, r29, r30, r31;

   wire   [`DATA] R1Out;
   wire   [`DATA] R2Out;

   wire rw0  = WrtReg ==  0 && RegWrite,
	rw1  = WrtReg ==  1 && RegWrite,
	rw2  = WrtReg ==  2 && RegWrite,
	rw3  = WrtReg ==  3 && RegWrite,
	rw4  = WrtReg ==  4 && RegWrite,
	rw5  = WrtReg ==  5 && RegWrite,
	rw6  = WrtReg ==  6 && RegWrite,
	rw7  = WrtReg ==  7 && RegWrite,
	rw8  = WrtReg ==  8 && RegWrite,
	rw9  = WrtReg ==  9 && RegWrite,
	rw10 = WrtReg == 10 && RegWrite,
	rw11 = WrtReg == 11 && RegWrite,
	rw12 = WrtReg == 12 && RegWrite,
	rw13 = WrtReg == 13 && RegWrite,
	rw14 = WrtReg == 14 && RegWrite,
	rw15 = WrtReg == 15 && RegWrite,
	rw16 = WrtReg == 16 && RegWrite,
	rw17 = WrtReg == 17 && RegWrite,
	rw18 = WrtReg == 18 && RegWrite,
	rw19 = WrtReg == 19 && RegWrite,
	rw20 = WrtReg == 20 && RegWrite,
	rw21 = WrtReg == 21 && RegWrite,
	rw22 = WrtReg == 22 && RegWrite,
	rw23 = WrtReg == 23 && RegWrite,
	rw24 = WrtReg == 24 && RegWrite,
	rw25 = WrtReg == 25 && RegWrite,
	rw26 = WrtReg == 26 && RegWrite,
	rw27 = WrtReg == 27 && RegWrite,
	rw28 = WrtReg == 28 && RegWrite,
	rw29 = WrtReg == 29 && RegWrite,
	rw30 = WrtReg == 30 && RegWrite,
	rw31 = WrtReg == 31 && RegWrite;

   Register R0  (r0,  32'b0000, rw0,  Clk);
   Register R1  (r1,  WrtData, rw1,  Clk);
   Register R2  (r2,  WrtData, rw2,  Clk);
   Register R3  (r3,  WrtData, rw3,  Clk);
   Register R4  (r4,  WrtData, rw4,  Clk);
   Register R5  (r5,  WrtData, rw5,  Clk);
   Register R6  (r6,  WrtData, rw6,  Clk);
   Register R7  (r7,  WrtData, rw7,  Clk);
   Register R8  (r8,  WrtData, rw8,  Clk);
   Register R9  (r9,  WrtData, rw9,  Clk);
   Register R10 (r10, WrtData, rw10, Clk);
   Register R11 (r11, WrtData, rw11, Clk);
   Register R12 (r12, WrtData, rw12, Clk);
   Register R13 (r13, WrtData, rw13, Clk);
   Register R14 (r14, WrtData, rw14, Clk);
   Register R15 (r15, WrtData, rw15, Clk);
   Register R16 (r16, WrtData, rw16, Clk);
   Register R17 (r17, WrtData, rw17, Clk);
   Register R18 (r18, WrtData, rw18, Clk);
   Register R19 (r19, WrtData, rw19, Clk);
   Register R20 (r20, WrtData, rw20, Clk);
   Register R21 (r21, WrtData, rw21, Clk);
   Register R22 (r22, WrtData, rw22, Clk);
   Register R23 (r23, WrtData, rw23, Clk);
   Register R24 (r24, WrtData, rw24, Clk);
   Register R25 (r25, WrtData, rw25, Clk);
   Register R26 (r26, WrtData, rw26, Clk);
   Register R27 (r27, WrtData, rw27, Clk);
   Register R28 (r28, WrtData, rw28, Clk);
   Register R29 (r29, WrtData, rw29, Clk); 
   Register R30 (r30, WrtData, rw30, Clk);
   Register R31 (r31, WrtData, rw31, Clk); 

RegMux R1_1Mux(RdReg1, R1Out,
        r0, r1, r2, r3, r4, r5, r6, r7,
        r8, r9, r10, r11, r12, r13, r14, r15,
        r16, r17, r18, r19, r20, r21, r22, r23,
        r24, r25, r26, r27, r28, r29, r30, r31);

RegMux R2_1Mux(RdReg2, R2Out,
        r0, r1, r2, r3, r4, r5, r6, r7,
        r8, r9, r10, r11, r12, r13, r14, r15,
        r16, r17, r18, r19, r20, r21, r22, r23,
        r24, r25, r26, r27, r28, r29, r30, r31);

endmodule /* RegFile */
