// ALUOp defines

`define ALUOPWIDTH      2
`define ALUO            `ALUOPWIDTH-1:0

`define MEMACC          2'b00
`define BRANCH          2'b01
`define RTYPE           2'b10
`define ITYPE           2'b11

// ALU actions

`define ALUCTRLWIDTH    4  // was 3
`define ALUC            `ALUCTRLWIDTH-1:0

`define ALUAND          4'b0000
`define ALUOR           4'b0001
`define ALUADD          4'b0010
`define ALUSUB          4'b0011
`define ALUMUL          4'b0100
`define ALUSLL          4'b0101
`define ALUSRL          4'b0110
`define ALUXOR          4'b0111
`define ALUSRA          4'b1000
`define ALUSLT          4'b1001
`define ALUSGT          4'b1010
`define ALUSLE          4'b1011
`define ALUSGE          4'b1100
`define ALUSEQ          4'b1101
`define ALUSNE          4'b1110
`define ALULH           4'b1111
