`include "sdlx.h"
`include "alu.h"

module Control(IR,
	       IRRW,
	       PCRW,
	       NPCRW,
	       ARW,
	       BRW,
	       IRW,
	       ALUoutRW,
	       MDRW,
	       BCRW,
	       ZSel,
	       BraE,
	       JmpE,
	       RegDst,
	       ALUInA,
	       ALUInB,
	       SESel,
	       WBSel,
	       ALUOp,
	       RegRW,
	       MemRW,
	       BMask,
	       MSESel,
	       Reset,
	       Clk);

   input   [`DATA1] IR;
   
   // Internal Registers
   output 	    IRRW;
   output 	    PCRW;
   output 	    NPCRW;
   output 	    ARW;
   output 	    BRW;
   output 	    IRW;
   output 	    ALUoutRW;
   output 	    MDRW;
   output 	    BCRW;
   
   // MUXs
   output 	    ZSel;
   output 	    BraE;
   output 	    JmpE;
   output [1:0]     RegDst;
   output 	    ALUInA;
   output 	    ALUInB;
   output [1:0]     WBSel;
   
   // Sign Extension
   output [1:0]     SESel;
   
   // ALU
   output [`ALUO]   ALUOp;
   
   // Register File
   output 	    RegRW;
   
   // Data memory
   output 	    MemRW;
   output [3:0]     BMask;
   output [2:0]     MSESel;

   input 	    Reset, Clk;
   
   wire [5:0] 	    OPcode;
   reg 		    IRRW, PCRW, NPCRW, ARW, BRW, IRW, ALUoutRW, MDRW, BCRW;
   reg 		    ZSel, BraE, JmpE, ALUInA, ALUInB;
   reg	[1:0]	    RegDst;
   reg	[1:0]	    SESel;
   reg  [1:0] 	    WBSel;
   reg 		    RegRW, MemRW;
   reg [3:0] 	    BMask;
   reg [2:0] 	    MSESel; 
   reg [`ALUO] 	    ALUOp;
   
   reg [2:0] 	    State, NextState;

   reg 		    monitor_reset ;
   reg [5:0] 	    monitor_opcode;
 		  
   
   initial 
     begin
	State     = `IDLE;
	NextState = `IDLE;
	ALUOp    = 2'b00;
	IRRW     = 0;
	PCRW     = 0;
	NPCRW    = 0;
	ARW      = 0;
	BRW      = 0;
	IRW      = 0;
	ALUoutRW = 0;
	MDRW     = 0;
	BCRW     = 0;
	ZSel     = 0;
	BraE     = 0;
	JmpE     = 0;
	RegDst   = 2'b00;
	ALUInA   = 0;
	ALUInB   = 0;
	SESel    = 2'b00;
	WBSel    = 2'b00;
	RegRW    = 0;
	MemRW    = 0;
	BMask    = 4'b0000;
	MSESel   = 3'b000;
	monitor_reset = 0;
	monitor_opcode =0;
     end
   
   
   always @(posedge Clk) begin
      monitor_reset=Reset;
      monitor_opcode = `OPCODE;
      
      State = NextState;
      
      case (State)
	`IDLE: 
	  begin
	     //             Outputvec(21'b000000000000000000000);
	     ALUOp    = 2'b00;
	     IRRW     = 0;
	     PCRW     = 0;
	     NPCRW    = 0;
	     ARW      = 0;
	     BRW      = 0;
	     IRW      = 0;
	     ALUoutRW = 0;
	     MDRW     = 0;
	     BCRW     = 0;
	     ZSel     = 0;
	     BraE     = 0;
	     JmpE     = 0;
	     RegDst   = 2'b00;
	     ALUInA   = 0;
	     ALUInB   = 0;
	     SESel    = 2'b00;
	     WBSel    = 2'b00;
	     RegRW    = 0;
	     MemRW    = 0;
	     BMask    = 4'b0000;
	     MSESel   = 3'b000;
	     NextState = Reset ? `IDLE : `IF;
	  end // case: `IDLE
        `IF:   
	  begin
	     // IR  <- IMEM[PC]
	     // NPC <- [PC] + 4
	     //           OutputVec(21'b001010000000000000000);
	     ALUOp    = 2'b00;
	     IRRW     = 1;
	     PCRW     = 0;
	     NPCRW    = 1;
	     ARW      = 0;
	     BRW      = 0;
	     IRW      = 0;
	     ALUoutRW = 0;
	     MDRW     = 0;
	     BCRW     = 0;
	     ZSel     = 0;
	     BraE     = 0;
	     JmpE     = 0;
	     RegDst   = 2'b00;
	     ALUInA   = 0;
	     ALUInB   = 0;
	     SESel    = 2'b00;
	     WBSel    = 2'b00;
	     RegRW    = 0;
	     MemRW    = 0;
	     BMask    = 4'b0000;
	     MSESel   = 3'b000;
	     NextState = Reset ? `IDLE : `ID;
	  end
	`ID:   
	  begin
	     // A  <- Reg[IRS]
	     // B  <- Reg[IRD]
	     // I  <- signext ## IMM
	     // PC <- NPC
	     if ((`OPCODE == `J) || (`OPCODE == `JAL))  // I <- signext ## IR[25:0]
	       begin
                  // OutputVec(21'b000101110000000001000);
		  ALUOp    = 2'b00;
		  IRRW     = 0;
		  PCRW     = 1;
		  NPCRW    = 0;
		  ARW      = 1;
		  BRW      = 1;
		  IRW      = 1;
		  ALUoutRW = 0;
		  MDRW     = 0;
		  BCRW     = 0;
		  ZSel     = 0;
		  BraE     = 0;
		  JmpE     = 0;
		  RegDst   = 2'b00;
		  ALUInA   = 0;
		  ALUInB   = 0;
		  SESel    = 2'b01;
		  WBSel    = 2'b00;
		  RegRW    = 0;
		  MemRW    = 0;
		  BMask    = 4'b0000;
		  MSESel   = 3'b000;
	       end
	     if ((`OPCODE == `ADDUI) || (`OPCODE == `SUBUI))  // No Sign ext
	       begin
		  ALUOp    = 2'b00;
		  IRRW     = 0;
		  PCRW     = 1;
		  NPCRW    = 0;
		  ARW      = 1;
		  BRW      = 1;
		  IRW      = 1;
		  ALUoutRW = 0;
		  MDRW     = 0;
		  BCRW     = 0;
		  ZSel     = 0;
		  BraE     = 0;
		  JmpE     = 0;
		  RegDst   = 2'b00;
		  ALUInA   = 0;
		  ALUInB   = 0;
		  SESel    = 2'b10;
		  WBSel    = 2'b00;
		  RegRW    = 0;
		  MemRW    = 0;
		  BMask    = 4'b0000;
		  MSESel   = 3'b000;
	       end
	     else                   // I <- signext ## IR[15:0]
	       //   OutputVec(21'b000101110000000000000);
	       begin
		  ALUOp    = 2'b00;
		  IRRW     = 0;
		  PCRW     = 1;
		  NPCRW    = 0;
		  ARW      = 1;
		  BRW      = 1;
		  IRW      = 1;
		  ALUoutRW = 0;
		  MDRW     = 0;
		  BCRW     = 0;
		  ZSel     = 0;
		  BraE     = 0;
		  JmpE     = 0;
		  RegDst   = 2'b00;
		  ALUInA   = 0;
		  ALUInB   = 0;
		  SESel    = 2'b00;
		  WBSel    = 2'b00;
		  RegRW    = 0;
		  MemRW    = 0;
		  BMask    = 4'b0000;
		  MSESel   = 3'b000;
	       end
	     NextState = Reset ? `IDLE : `EX;
	  end // case: `ID
	`EX:   
	  begin
	     if (IR === `NOP) 
	       //   OutputVec(21'b000000000000000000000);
	       begin
		  ALUOp    = 2'b00;
		  IRRW     = 0;
		  PCRW     = 0;
		  NPCRW    = 0;
		  ARW      = 0;
		  BRW      = 0;
		  IRW      = 0;
		  ALUoutRW = 0;
		  MDRW     = 0;
		  BCRW     = 0;
		  ZSel     = 0;
		  BraE     = 0;
		  JmpE     = 0;
		  RegDst   = 2'b00;
		  ALUInA   = 0;
		  ALUInB   = 0;
		  SESel    = 2'b00;
		  WBSel    = 2'b00;
		  RegRW    = 0;
		  MemRW    = 0;  	
		  BMask    = 4'b0000;
		  MSESel   = 3'b000;
		  NextState = Reset ? `IDLE : `IF;
	       end
	     else 
	       begin
		  case (`OPCODE)
		    `FSEL: 
		      // ALUout <- [A] op [B]
		      begin
			 ALUOp    = 2'b10;
			 IRRW     = 0;
			 PCRW     = 0;
			 NPCRW    = 0;
			 ARW      = 0;
			 BRW      = 0;
			 IRW      = 0;
			 ALUoutRW = 1;
			 MDRW     = 0;
			 BCRW     = 0;
			 ZSel     = 0;
			 BraE     = 0;
			 JmpE     = 0;
			 RegDst   = 2'b00;
			 ALUInA   = 1;
			 ALUInB   = 0;
			 SESel    = 2'b00;
			 WBSel    = 2'b00;
			 RegRW    = 0;
			 MemRW    = 0;
			 BMask    = 4'b0000;
			 MSESel   = 3'b000;
			 NextState = Reset ? `IDLE : `WB;
		      end
		    `J, `JAL: 
		      begin
			 // ALUout <- [NPC] + [Imm]
			 // OutputVec({`BRANCH,19'b0000001000000010000});
			 ALUOp    = 2'b01;
			 IRRW     = 0;
			 PCRW     = 0;
			 NPCRW    = 0;
			 ARW      = 0;
			 BRW      = 0;
			 IRW      = 0;
			 ALUoutRW = 1;
			 MDRW     = 0;
			 BCRW     = 0;
			 ZSel     = 0;
			 BraE     = 0;
			 JmpE     = 0;
			 RegDst   = 2'b00;
			 ALUInA   = 0;
			 ALUInB   = 1;
			 SESel    = 2'b00;
			 WBSel    = 2'b00;
			 RegRW    = 0;
			 MemRW    = 0;
			 BMask    = 4'b0000;
			 MSESel   = 3'b000;
			 NextState = Reset ? `IDLE : `WB;
		      end
		    `JR, `JALR: 
		      begin
			 // ALUout <- [R] + [Imm]
			 // OutputVec({`BRANCH,19'b0000001000000010000});
			 ALUOp    = 2'b11;
			 IRRW     = 0;
			 PCRW     = 0;
			 NPCRW    = 0;
			 ARW      = 0;
			 BRW      = 0;
			 IRW      = 0;
			 ALUoutRW = 1;
			 MDRW     = 0;
			 BCRW     = 0;
			 ZSel     = 0;
			 BraE     = 0;
			 JmpE     = 0;
			 RegDst   = 2'b00;
			 ALUInA   = 1;
			 ALUInB   = 1;
			 SESel    = 2'b00;
			 WBSel    = 2'b00;
			 RegRW    = 0;
			 MemRW    = 0;
			 BMask    = 4'b0000;
			 MSESel   = 3'b000;
			 NextState = Reset ? `IDLE : `WB;
		      end
		    `BEQZ: 
		      begin
			 // ALUout <- ([A] == 0) ? [NPC] + [Imm] : [NPC]
			 //putVec({`BRANCH,19'b0000001010000010000});
			 ALUOp    = 2'b01;
			 IRRW     = 0;
			 PCRW     = 0;
			 NPCRW    = 0;
			 ARW      = 0;
			 BRW      = 0;
			 IRW      = 0;
			 ALUoutRW = 1;
			 MDRW     = 0;
			 BCRW     = 1;
			 ZSel     = 0;
			 BraE     = 0;
			 JmpE     = 0;
			 RegDst   = 2'b00;
			 ALUInA   = 0;
			 ALUInB   = 1;
			 SESel    = 2'b00;
			 WBSel    = 2'b00;
			 RegRW    = 0;
			 MemRW    = 0;
			 BMask    = 4'b0000;
			 MSESel   = 3'b000;
			 NextState = Reset ? `IDLE : `WB;
		      end
		    `BNEZ: 
		      begin
			 // ALUout <- ([A] != 0) ? [NPC] + [Imm] : [NPC]
			 //  OutputVec({`BRANCH,19'b0000001011000010000});
			 ALUOp    = 2'b01;
			 IRRW     = 0;
			 PCRW     = 0;
			 NPCRW    = 0;
			 ARW      = 0;
			 BRW      = 0;
			 IRW      = 0;
			 ALUoutRW = 1;
			 MDRW     = 0;
			 BCRW     = 1;
			 ZSel     = 1;
			 BraE     = 0;
			 JmpE     = 0;
			 RegDst   = 2'b00;
			 ALUInA   = 0;
			 ALUInB   = 1;
			 SESel    = 2'b00;
			 WBSel    = 2'b00;
			 RegRW    = 0;
			 MemRW    = 0;
			 BMask    = 4'b0000;
			 MSESel   = 3'b000;
			 NextState = Reset ? `IDLE : `WB;
		      end
		    `ADDI, `ADDUI,
		    `SUBI, `SUBUI,
		    `ANDI,
		    `ORI,
		    `XORI,
		    `SLLI,
		    `SRLI,
		    `SRAI,
		    `SLTI,
		    `SGTI,
		    `SLEI,
		    `SGEI,
		    `SEQI,
		    `SNEI,
		    `LHI: 
		      begin
			 // ALUout <- [RS] + [Imm]
			 // OutputVec({`ITYPE,19'b0000001000000110000});
			 ALUOp    = 2'b11;
			 IRRW     = 0;
			 PCRW     = 0;
			 NPCRW    = 0;
			 ARW      = 0;
			 BRW      = 0;
			 IRW      = 0;
			 ALUoutRW = 1;
			 MDRW     = 0;
			 BCRW     = 0;
			 ZSel     = 0;
			 BraE     = 0;
			 JmpE     = 0;
			 RegDst   = 2'b00;
			 ALUInA   = 1;
			 ALUInB   = 1;
			 SESel    = 2'b00;
			 WBSel    = 2'b00;
			 RegRW    = 0;
			 MemRW    = 0;
			 BMask    = 4'b0000;
			 MSESel   = 3'b000;
			 NextState = Reset ? `IDLE : `WB;
		      end
		    `LW, `LH, `LHU, `LB, `LBU: 
		      begin
			 // ALUout <- [RS]+[Imm]
			 //   OutputVec({`MEMACC,19'b0000001000000110000});
			 ALUOp    = 2'b00;
			 IRRW     = 0;
			 PCRW     = 0;
			 NPCRW    = 0;
			 ARW      = 0;
			 BRW      = 0;
			 IRW      = 0;
			 ALUoutRW = 1;
			 MDRW     = 0;
			 BCRW     = 0;
			 ZSel     = 0;
			 BraE     = 0;
			 JmpE     = 0;
			 RegDst   = 2'b00;
			 ALUInA   = 1;
			 ALUInB   = 1;
			 SESel    = 2'b00;
			 WBSel    = 2'b00;
			 RegRW    = 0;
			 MemRW    = 0;
			 BMask    = 4'b0000;
			 MSESel   = 3'b000;
			 NextState = Reset ? `IDLE : `MEM;
		      end
		    `SW, `SH, `SB: 
		      begin
			 // ALUout <- [RS]+[Imm]
			 //   OutputVec({`MEMACC,19'b0000001000000110000});
			 ALUOp    = 2'b00;
			 IRRW     = 0;
			 PCRW     = 0;
			 NPCRW    = 0;
			 ARW      = 0;
			 BRW      = 0;
			 IRW      = 0;
			 ALUoutRW = 1;
			 MDRW     = 0;
			 BCRW     = 0;
			 ZSel     = 0;
			 BraE     = 0;
			 JmpE     = 0;
			 RegDst   = 2'b00;
			 ALUInA   = 1;
			 ALUInB   = 1;
			 SESel    = 2'b00;
			 WBSel    = 2'b00;
			 RegRW    = 0;
			 MemRW    = 0;
			 BMask    = 4'b0000;
			 MSESel   = 3'b000;
			 NextState = Reset ? `IDLE : `MEM;
		      end
		    `TRAP0: 
		      begin
		      end
		    default: 
		      begin
		      end
		  endcase // case(`OPCODE)
	       end // else: !if(IR === `NOP)
	  end // case: `EX
	`MEM:  
	  begin
	     case (`OPCODE)
	       `LW: 
		 begin
		    //  OutputVec(21'b000000000100000000000);
		    // MD <- DMEM[ALUout]
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 0;
		    NPCRW    = 0;
		    ARW      = 0;
		    BRW      = 0;
		    IRW      = 0;
		    ALUoutRW = 0;
		    MDRW     = 1;
		    BCRW     = 0;
		    ZSel     = 0;
		    BraE     = 0;
		    JmpE     = 0;
		    RegDst   = 2'b00;
		    ALUInA   = 0;
		    ALUInB   = 0;
		    SESel    = 2'b00;
		    WBSel    = 2'b00;
		    RegRW    = 0;
		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b000;
		    NextState = Reset ? `IDLE : `WB;
		 end
	       `LH: 
		 begin
		    //  OutputVec(21'b000000000100000000000);
		    // MD <- DMEM[ALUout]
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 0;
		    NPCRW    = 0;
		    ARW      = 0;
		    BRW      = 0;
		    IRW      = 0;
		    ALUoutRW = 0;
		    MDRW     = 1;
		    BCRW     = 0;
		    ZSel     = 0;
		    BraE     = 0;
		    JmpE     = 0;
		    RegDst   = 2'b00;
		    ALUInA   = 0;
		    ALUInB   = 0;
		    SESel    = 2'b00;
		    WBSel    = 2'b00;
		    RegRW    = 0;
		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b001;
		    NextState = Reset ? `IDLE : `WB;
		 end
	       `LHU: 
		 begin
		    //  OutputVec(21'b000000000100000000000);
		    // MD <- DMEM[ALUout]
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 0;
		    NPCRW    = 0;
		    ARW      = 0;
		    BRW      = 0;
		    IRW      = 0;
		    ALUoutRW = 0;
		    MDRW     = 1;
		    BCRW     = 0;
		    ZSel     = 0;
		    BraE     = 0;
		    JmpE     = 0;
		    RegDst   = 2'b00;
		    ALUInA   = 0;
		    ALUInB   = 0;
		    SESel    = 2'b00;
		    WBSel    = 2'b00;
		    RegRW    = 0;
		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b010;
		    NextState = Reset ? `IDLE : `WB;
		 end
	       `LB: 
		 begin
		    //  OutputVec(21'b000000000100000000000);
		    // MD <- DMEM[ALUout]
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 0;
		    NPCRW    = 0;
		    ARW      = 0;
		    BRW      = 0;
		    IRW      = 0;
		    ALUoutRW = 0;
		    MDRW     = 1;
		    BCRW     = 0;
		    ZSel     = 0;
		    BraE     = 0;
		    JmpE     = 0;
		    RegDst   = 2'b00;
		    ALUInA   = 0;
		    ALUInB   = 0;
		    SESel    = 2'b00;
		    WBSel    = 2'b00;
		    RegRW    = 0;
		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b011;
		    NextState = Reset ? `IDLE : `WB;
		 end
	       `LBU: 
		 begin
		    //  OutputVec(21'b000000000100000000000);
		    // MD <- DMEM[ALUout]
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 0;
		    NPCRW    = 0;
		    ARW      = 0;
		    BRW      = 0;
		    IRW      = 0;
		    ALUoutRW = 0;
		    MDRW     = 1;
		    BCRW     = 0;
		    ZSel     = 0;
		    BraE     = 0;
		    JmpE     = 0;
		    RegDst   = 2'b00;
		    ALUInA   = 0;
		    ALUInB   = 0;
		    SESel    = 2'b00;
		    WBSel    = 2'b00;
		    RegRW    = 0;
		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b100;
		    NextState = Reset ? `IDLE : `WB;
		 end
	       `SW: 
		 begin
		    // OutputVec(21'b000000000000000000001);
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 0;
		    NPCRW    = 0;
		    ARW      = 0;
		    BRW      = 0;
		    IRW      = 0;
		    ALUoutRW = 0;
		    MDRW     = 0;
		    BCRW     = 0;
		    ZSel     = 0;
		    BraE     = 0;
		    JmpE     = 0;
		    RegDst   = 2'b00;
		    ALUInA   = 0;
		    ALUInB   = 0;
   		    SESel    = 2'b00;
		    WBSel    = 2'b00;
		    RegRW    = 0;
   		    MemRW    = 1;
		    BMask    = 4'b1111;
		    MSESel   = 3'b000;
                    // DMEM[ALUout] <- [RD]
		    NextState = Reset ? `IDLE : `IF;
		 end
	       `SH: 
		 begin
		    // OutputVec(21'b000000000000000000001);
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 0;
		    NPCRW    = 0;
		    ARW      = 0;
		    BRW      = 0;
		    IRW      = 0;
		    ALUoutRW = 0;
		    MDRW     = 0;
		    BCRW     = 0;
		    ZSel     = 0;
		    BraE     = 0;
		    JmpE     = 0;
		    RegDst   = 2'b00;
		    ALUInA   = 0;
		    ALUInB   = 0;
   		    SESel    = 2'b00;
		    WBSel    = 2'b00;
		    RegRW    = 0;
   		    MemRW    = 1;
		    BMask    = 4'b0011;
		    MSESel   = 3'b000;
                    // DMEM[ALUout] <- [RD]
		    NextState = Reset ? `IDLE : `IF;
		 end
	       `SB: 
		 begin
		    // OutputVec(21'b000000000000000000001);
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 0;
		    NPCRW    = 0;
		    ARW      = 0;
		    BRW      = 0;
		    IRW      = 0;
		    ALUoutRW = 0;
		    MDRW     = 0;
		    BCRW     = 0;
		    ZSel     = 0;
		    BraE     = 0;
		    JmpE     = 0;
		    RegDst   = 2'b00;
		    ALUInA   = 0;
		    ALUInB   = 0;
   		    SESel    = 2'b00;
		    WBSel    = 2'b00;
		    RegRW    = 0;
   		    MemRW    = 1;
		    BMask    = 4'b0001;
		    MSESel   = 3'b000;
                    // DMEM[ALUout] <- [RD]
		    NextState = Reset ? `IDLE : `IF;
		 end
	     endcase // case(`OPCODE)
	  end // case: `MEM
	`WB:   
	  begin
	     case (`OPCODE)
	       `FSEL: 
		 begin
		    // RD <- [ALUout]
		    //OutputVec(21'b000000000000001000110);
		    ALUOp    = 2'b00;
		    IRRW     = 0;
   		    PCRW     = 0;
   		    NPCRW    = 0;
   		    ARW      = 0;
   		    BRW      = 0;
   		    IRW      = 0;
   		    ALUoutRW = 0;
   		    MDRW     = 0;
   		    BCRW     = 0;
   		    ZSel     = 0;
   		    BraE     = 0;
   		    JmpE     = 0;
   		    RegDst   = 2'b01;
   		    ALUInA   = 0;
   		    ALUInB   = 0;
   		    SESel    = 2'b00;
   		    WBSel    = 2'b01;
   		    RegRW    = 1;
   		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b000;
		 end
	       `J, `JR: 
		 begin
		    // Jump completion: could be in MEM too
		    // PC <- [ALUout]
		    //  OutputVec(21'b000100000000010000000);
		    ALUOp    = 2'b00;
		    IRRW     = 0;
   		    PCRW     = 1;
   		    NPCRW    = 0;
   		    ARW      = 0;
   		    BRW      = 0;
   		    IRW      = 0;
   		    ALUoutRW = 0;
   		    MDRW     = 0;
   		    BCRW     = 0;
   		    ZSel     = 0;
   		    BraE     = 0;
   		    JmpE     = 1;
   		    RegDst   = 2'b00;
   		    ALUInA   = 0;
   		    ALUInB   = 0;
   		    SESel    = 2'b00;
   		    WBSel    = 2'b00;
   		    RegRW    = 0;
		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b000;
		 end
	       `JAL, `JALR: 
		 begin
		    // Jump completion: could be in MEM too
		    // PC <- [ALUout]
		    //  OutputVec(21'b000100000000010000000);
		    ALUOp    = 2'b00;
		    IRRW     = 0;
   		    PCRW     = 1;
   		    NPCRW    = 0;
   		    ARW      = 0;
   		    BRW      = 0;
   		    IRW      = 0;
   		    ALUoutRW = 0;
   		    MDRW     = 0;
   		    BCRW     = 0;
   		    ZSel     = 0;
   		    BraE     = 0;
   		    JmpE     = 1;
   		    RegDst   = 2'b10;
   		    ALUInA   = 0;
   		    ALUInB   = 0;
   		    SESel    = 2'b00;
   		    WBSel    = 2'b10;
   		    RegRW    = 1;
		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b000;
		 end
	       `BEQZ: 
		 begin
		    // Branch completion: could be in MEM too
		    // PC <- [ALUout]
		    //  OutputVec(21'b000100000000100000000);
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 1;
		    NPCRW    = 0;
		    ARW      = 0;
   		    BRW      = 0;
   		    IRW      = 0;
   		    ALUoutRW = 0;
   		    MDRW     = 0;
   		    BCRW     = 0;
   		    ZSel     = 0;
   		    BraE     = 1;
   		    JmpE     = 0;
   		    RegDst   = 2'b00;
   		    ALUInA   = 0;
   		    ALUInB   = 0;
   		    SESel    = 2'b00;
   		    WBSel    = 2'b00;
   		    RegRW    = 0;
   		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b000;
		 end
	       `BNEZ: 
		 begin
		    // Branch completion: could be in MEM too
		    // PC <- [ALUout]
		    //  OutputVec(21'b000100000000100000000);
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 1;
		    NPCRW    = 0;
		    ARW      = 0;
		    BRW      = 0;
		    IRW      = 0;
   		    ALUoutRW = 0;
   		    MDRW     = 0;
   		    BCRW     = 0;
   		    ZSel     = 0;
   		    BraE     = 1;
   		    JmpE     = 0;
   		    RegDst   = 2'b00;
   		    ALUInA   = 0;
   		    ALUInB   = 0;
   		    SESel    = 2'b00;
   		    WBSel    = 2'b00;
   		    RegRW    = 0;
   		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b000;
		 end
	       `ADDI, `ADDUI,
	       `SUBI, `SUBUI,
	       `ANDI,
	       `ORI,
	       `XORI,
	       `SLLI,
	       `SRLI,
	       `SRAI,
	       `SLTI,
	       `SGTI,
	       `SLEI,
	       `SGEI,
	       `SEQI,
	       `SNEI,
	       `LHI: 
		 begin
		    // RD <- [ALUout]
		    // OutputVec(21'b000000000000000000110);
		    ALUOp    = 2'b00;
		    IRRW     = 0;
   		    PCRW     = 0;
   		    NPCRW    = 0;
   		    ARW      = 0;
   		    BRW      = 0;
   		    IRW      = 0;
   		    ALUoutRW = 0;
   		    MDRW     = 0;
   		    BCRW     = 0;
   		    ZSel     = 0;
   		    BraE     = 0;
   		    JmpE     = 0;
   		    RegDst   = 2'b00;
   		    ALUInA   = 0;
   		    ALUInB   = 0;
   		    SESel    = 2'b00;
   		    WBSel    = 2'b01;
   		    RegRW    = 1;
   		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b000;
		 end
	       `LW, `LHU, `LH, `LB, `LBU: 
		 begin
		    // RD <- [MD]
		    // OutputVec(21'b000000000000000000010);
		    ALUOp    = 2'b00;
		    IRRW     = 0;
		    PCRW     = 0;
   		    NPCRW    = 0;
   		    ARW      = 0;
   		    BRW      = 0;
   		    IRW      = 0;
   		    ALUoutRW = 0;
   		    MDRW     = 0;
   		    BCRW     = 0;
   		    ZSel     = 0;
   		    BraE     = 0;
   		    JmpE     = 0;
   		    RegDst   = 2'b00;
   		    ALUInA   = 0;
   		    ALUInB   = 0;
   		    SESel    = 2'b00;
   		    WBSel    = 2'b00;
   		    RegRW    = 1;
   		    MemRW    = 0;
		    BMask    = 4'b0000;
		    MSESel   = 3'b000;	
		 end
	     endcase // case(`OPCODE)
	     NextState = Reset ? `IDLE : `IF;
	  end // case: `WB
	default: 
	  begin
	  end
      endcase // case(State)
   end // always @ (posedge Clk)
endmodule /* Control */
