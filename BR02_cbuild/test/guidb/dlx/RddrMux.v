`include "sdlx.h"

module RddrMux (S, A, B, Z);

   input  [1:0]   S;     // Select line
   input  [`RDDR] A;
   input  [`RDDR] B;
   output [`RDDR] Z;

   wire   [`RDDR] Z = (S == 2'b01) ? B : ((S == 2'b00) ? A : 5'd31);

endmodule /* RddrMux */

