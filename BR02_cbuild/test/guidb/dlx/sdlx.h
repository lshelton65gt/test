
// Parameter declaration

//`define BYTE            3:0
`define DATAWIDTH       32  // was 4
`define ADDRWIDTH       32  // was 4
`define DATA            `DATAWIDTH-1:0
`define ADDR            `ADDRWIDTH-1:0
`define REGWIDTH        5   // was 3
`define RDDR            `REGWIDTH-1:0
`define MAXREGS         1<<`REGWIDTH
`define MEMSIZE         256
`define DATAWIDTH1      32
`define DATA1           `DATAWIDTH1-1:0
`define CLOCKPERIOD     20

// Instruction fields

`define OPCODE          IR[31:26]

// I-Type Instruction
`define IRS             IR[25:21]
`define IRD             IR[20:16]
`define IMM             IR[15:0]

// R-Type Instruction
`define RRS1            IR[25:21]
`define RRS2            IR[20:16]
`define RRD             IR[15:11]
//`define SHAMT           IR[7:6]
`define FUNC            IR[10:0]

// J-Type Instruction
`define OFFSET          IR[25:0]

// Address modes

//`define DSPLTYPE        0
//`define IMMTYPE         1

// OPcode map

// OPcodes
`define NOP     00000000000000000000000000000000

`define FSEL    6'b000000
`define J       6'b000010
`define JAL     6'b000011
`define BEQZ    6'b000100
`define BNEZ    6'b000101
`define ADDI    6'b001000
`define ADDUI   6'b001001
`define SUBI    6'b001010
`define SUBUI   6'b001011
`define ANDI    6'b001100
`define ORI     6'b001101
`define XORI    6'b001110
`define LHI     6'b001111
`define TRAP0   6'b010001
`define JR      6'b010010
`define JALR    6'b010011
`define SLLI    6'b010100
`define SRLI    6'b010110
`define SRAI    6'b010111
`define SEQI    6'b011000
`define SNEI    6'b011001
`define SLTI    6'b011010
`define SGTI    6'b011011
`define SLEI    6'b011100
`define SGEI    6'b011101
`define LB      6'b100000
`define LH      6'b100001
`define LW      6'b100011
`define LBU     6'b100100
`define LHU     6'b100101
`define SB      6'b101000
`define SH      6'b101001
`define SW      6'b101011
//`define HLT     6'b001101

// func
`define SLL     6'b000100
`define SRL     6'b000110
`define SRA     6'b000111
`define ADD     6'b100000
`define ADDU    6'b100001
`define SUB     6'b100010
`define SUBU    6'b100011
`define AND     6'b100100
`define OR      6'b100101
`define XOR     6'b100110
`define SEQ     6'b101000
`define SNE     6'b101001
`define SLT     6'b101010
`define SGT     6'b101011
`define SLE     6'b101100
`define SGE     6'b101101
`define MUL     6'b011000

// Execution steps

`define IDLE    0
`define IF      1
`define ID      2
`define EX      3
`define MEM     4
`define WB      5

// Miscellaneous

`define TRUE            1
`define FALSE           0

`define LEFT            1
`define RIGHT           0

`define DB_ON           debug = 1
`define DB_OFF          debug = 0
