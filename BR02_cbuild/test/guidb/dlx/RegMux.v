`include "sdlx.h"

module RegMux (S, RegOut,
               r0, r1, r2, r3, r4, r5, r6, r7,
               r8, r9, r10, r11, r12, r13, r14, r15,
               r16, r17, r18, r19, r20, r21, r22, r23,
               r24, r25, r26, r27, r28, r29, r30, r31);
   input  [`RDDR] S;        // Select Line
   output [`DATA] RegOut;
   input  [`DATA] r0, r1, r2, r3, r4, r5, r6, r7,
		  r8, r9, r10, r11, r12, r13, r14, r15,
		  r16, r17, r18, r19, r20, r21, r22, r23,
		  r24, r25, r26, r27, r28, r29, r30, r31;

   // ** if the S is equal to i, then assign ri to RegOut
   assign RegOut = 
	  S == 0  ? r0  :
          S == 1  ? r1  :
          S == 2  ? r2  :
          S == 3  ? r3  :
          S == 4  ? r4  :
          S == 5  ? r5  :
          S == 6  ? r6  :
          S == 7  ? r7  :
          S == 8  ? r8  :
          S == 9  ? r9  :
          S == 10 ? r10 :
          S == 11 ? r11 :
          S == 12 ? r12 :
          S == 13 ? r13 :
          S == 14 ? r14 :
          S == 15 ? r15 :
          S == 16 ? r16 :
          S == 17 ? r17 :
          S == 18 ? r18 :
          S == 19 ? r19 :
          S == 20 ? r20 :
          S == 21 ? r21 :
          S == 22 ? r22 :
          S == 23 ? r23 :
          S == 24 ? r24 :
          S == 25 ? r25 :
          S == 26 ? r26 :
          S == 27 ? r27 :
          S == 28 ? r28 :
          S == 29 ? r29 :
          S == 30 ? r30 :r31;
                
endmodule /* RegMux */
