`include "sdlx.h"

module WBMux (S, A, B, C, Z);

   input  [1:0]   S;      // Select line
   input  [`DATA] A;
   input  [`DATA] B;
   input  [`DATA] C;
   output [`DATA] Z;

   wire   [`DATA] Z = (S == 2'b01) ? B : ((S == 2'b00) ? A : C);

endmodule /* Mux */
