`include "alu.h"
`include "sdlx.h"

module ALU(AIn, BIn, Out, ALUCtrl);
   input  [`DATA] AIn, BIn;
   output [`DATA] Out;
   input  [`ALUC] ALUCtrl;

   reg [`DATA] 	  Out;
   reg 		  tmp;
   integer 	  i;

   initial 
     begin
	Out = 0;
	tmp = 0;
     end

   always @(ALUCtrl or AIn or BIn) 
     begin
	case (ALUCtrl)
	  `ALUAND: 
	    begin
               Out = AIn & BIn;
            end
	  `ALUOR: 
	    begin
               Out = AIn | BIn;
            end
	  `ALUXOR: 
	    begin
               Out = AIn ^ BIn;
            end
	  `ALUSLL: 
	    begin
	       Out = AIn << BIn;
	    end
	  `ALUSRL: 
	    begin
	       Out = AIn >> BIn;
	    end
	  `ALUSRA: 
	    begin
	       tmp = AIn[31];
	       Out = AIn >> BIn;
`ifdef CARBON_BM
	       if (0 < BIn)
		 Out[31] = AIn[31];
	       if (1 < BIn)
		 Out[30] = AIn[31];
	       if (2 < BIn)
		 Out[29] = AIn[31];
	       if (3 < BIn)
		 Out[28] = AIn[31];
	       if (4 < BIn)
		 Out[27] = AIn[31];
	       if (5 < BIn)
		 Out[26] = AIn[31];
	       if (6 < BIn)
		 Out[25] = AIn[31];
	       if (7 < BIn)
		 Out[24] = AIn[31];
	       if (8 < BIn)
		 Out[23] = AIn[31];
	       if (9 < BIn)
		 Out[22] = AIn[31];
	       if (10 < BIn)
		 Out[21] = AIn[31];
	       if (11 < BIn)
		 Out[20] = AIn[31];
	       if (12 < BIn)
		 Out[19] = AIn[31];
	       if (13 < BIn)
		 Out[18] = AIn[31];
	       if (14 < BIn)
		 Out[17] = AIn[31];
	       if (15 < BIn)
		 Out[16] = AIn[31];
	       if (16 < BIn)
		 Out[15] = AIn[31];
	       if (17 < BIn)
		 Out[14] = AIn[31];
	       if (18 < BIn)
		 Out[13] = AIn[31];
	       if (19 < BIn)
		 Out[12] = AIn[31];
	       if (20 < BIn)
		 Out[11] = AIn[31];
	       if (21 < BIn)
		 Out[10] = AIn[31];
	       if (22 < BIn)
		 Out[9] = AIn[31];
	       if (23 < BIn)
		 Out[8] = AIn[31];
	       if (24 < BIn)
		 Out[7] = AIn[31];
	       if (25 < BIn)
		 Out[6] = AIn[31];
	       if (26 < BIn)
		 Out[5] = AIn[31];
	       if (27 < BIn)
		 Out[4] = AIn[31];
	       if (28 < BIn)
		 Out[3] = AIn[31];
	       if (29 < BIn)
		 Out[2] = AIn[31];
	       if (30 < BIn)
		 Out[1] = AIn[31];
	       if (31 < BIn)
		 Out[0] = AIn[31];
`else
	       for (i = 0; i < 32; i = i + 1)
		 if (i < BIn) 
		   Out[31-i] = tmp;
`endif
	    end
	  `ALUADD: 
	    begin
               Out = AIn + BIn;
            end
	  `ALUSUB: 
	    begin
               Out = AIn - BIn;
            end
	  `ALUSLT:
	    begin
	       if (AIn < BIn)
		 Out = 1;
	       else
		 Out = 0;
	    end
	  `ALUSGT:
	    begin
	       if (AIn > BIn)
		 Out = 1;
	       else
		 Out = 0;
	    end
	  `ALUSLE:
	    begin
	       if (AIn <= BIn)
		 Out = 1;
	       else
		 Out = 0;
	    end
	  `ALUSGE:
	    begin
	       if (AIn >= BIn)
		 Out = 1;
	       else
		 Out = 0;
	    end
	  `ALUSEQ:
	    begin
	       if (AIn === BIn)
		 Out = 1;
	       else
		 Out = 0;
	    end
	  `ALUSNE:
	    begin
	       if (AIn !== BIn)
		 Out = 1;
	       else
		 Out = 0;
	    end
	  `ALULH:
	    begin
	       Out[31:16] = BIn[15:0];
	       Out[15:0] = 16'b0;
	    end
	endcase
     end
   
endmodule /* ALU */
