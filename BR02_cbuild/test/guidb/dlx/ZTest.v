`include "sdlx.h"

module ZTest (S, I, Out);

   input          S;      // S ? nez : eqz
   input  [`DATA] I;
   output 	  Out;

//   wire 	  Out = S ? ((I != 0) ? 1 : 0) : ((I == 0) ? 1 : 0);
   wire 	  Out = S ? (|I) : (~(|I));

endmodule /* ZTest */
