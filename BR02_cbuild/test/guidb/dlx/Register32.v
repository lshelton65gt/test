`include "sdlx.h"

module Register32 (RegOut, RegIn, RegRW, Clk);
   output [`DATA1] RegOut;
   input  [`DATA1] RegIn;
   input 	   RegRW;
   input 	   Clk;

   reg    [`DATA1] RegBuf;
   wire   [`DATA1] RegOut = RegBuf;


   initial
     begin
	RegBuf = 0;
     end

   always @(negedge Clk) 
     begin
	if (RegRW) #0 RegBuf = RegIn;
     end

endmodule /* Register32 */
