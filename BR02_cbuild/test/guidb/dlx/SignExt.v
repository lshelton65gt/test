`include "sdlx.h"

module SignExt (S, I, O);

   input  [1:0]   S;     // Select Offset or Imm
   input  [25:0]  I;
   output [`DATA] O;

   wire [`DATA]   O = (S==1) ? {{6 {I[25]}}, I} : 
                  (S==0) ? {{16 {I[15]}}, I[15:0]} :
		  {{16'b0}, I[15:0]};

endmodule  /* SignExt */
