`include "sdlx.h"

module Register (RegOut, RegIn, RegRW, Clk);
   output [`DATA] RegOut;
   input  [`DATA] RegIn;
   input 	  RegRW;
   input 	  Clk;


   reg    [`DATA] RegBuf;

   wire   [`DATA] RegOut = RegBuf;

   initial
     begin
	RegBuf = 0;
     end

   always @(negedge Clk) 
     begin
	if (RegRW) #0 RegBuf = RegIn;
     end

endmodule /* Register */
