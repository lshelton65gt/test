// sdlx.v

`include "sdlx.h"
`include "alu.h"
`include "dbgflags.h"

module sdlx (clk, Reset, PC, IR, 
	     R00, R01, R02, R03, R04, R05, R06, R07,
	     R08, R09, R10, R11, R12, R13, R14, R15,
	     R16, R17, R18, R19, R20, R21, R22, R23,
	     R24, R25, R26, R27, R28, R29, R30, R31,
	     DMEM0, DMEM1, DMEM2, DMEM3, DMEM4, DMEM5
	     );
   input clk, Reset;
   output [`ADDR] PC;
   output [`DATA1] IR;
   output [`DATA]  R00, R01, R02, R03, R04, R05, R06, R07,
		   R08, R09, R10, R11, R12, R13, R14, R15,
		   R16, R17, R18, R19, R20, R21, R22, R23,
		   R24, R25, R26, R27, R28, R29, R30, R31,
		   DMEM0, DMEM1, DMEM2, DMEM3, DMEM4, DMEM5;

   reg  [`DATA] IMEM [0:`MEMSIZE-1];
   integer 	i;
   
   wire [`ADDR] pcin, npcin;
   wire [`DATA] aluain, alubin, aluout;
   wire [`ALUO] ALUOp;
   wire [`ALUC] ALUCtrl;
   wire [1:0] 	SESel;
   wire [`DATA] wb2rf, rf2a, rf2b;
   wire [`RDDR] rd;
   wire [`DATA] dmemout;
   wire [`ADDR] PC,NPC;
   wire [`DATA1] IR;
   wire [`DATA]  I,iin;
   wire [`DATA]  A, B, ALUOut,
		 MD, mdin;
   wire 	 BC, bcin;
   wire [3:0] 	 BMask;
   wire [2:0] 	 MSESel;
   wire [1:0] 	 WBSel;
   wire [1:0] 	 RegDst;
   
`ifdef CARBON
   initial $readmemb("loop.s.bin", IMEM);
`else   
   Clock       Clock      (clk);
`endif
   
   Register32  IRReg      (IR, IMEM[PC >> 2],IRRW,clk);
   Register    PCReg      (PC, pcin, PCRW, clk);
   Register    NPCReg     (NPC, npcin, NPCRW, clk);
   Register    AReg       (A, rf2a, ARW, clk);
   Register    BReg       (B, rf2b, BRW, clk);
   Register    IReg       (I, iin, IRW, clk);
   Register    ALUOutReg  (ALUOut, aluout, ALUoutRW, clk);
   Register    MDReg      (MD, mdin, MDRW, clk);
   OneBitReg   BCReg      (BC, bcin, BCRW, clk);

   RegFile     RegFile    (`RRS1, `RRS2, rd, wb2rf, rf2a, rf2b, RegRW, clk,
			   R00, R01, R02, R03, R04, R05, R06, R07,
			   R08, R09, R10, R11, R12, R13, R14, R15,
			   R16, R17, R18, R19, R20, R21, R22, R23,
			   R24, R25, R26, R27, R28, R29, R30, R31);
   DataMem     DataMem    (BMask, ALUOut, B, dmemout, MemRW, clk,
			   DMEM0, DMEM1, DMEM2, DMEM3, DMEM4, DMEM5);
   Control     Control    (IR,
			   // Register control
			   IRRW,
			   PCRW,
			   NPCRW,
			   ARW,
			   BRW,
			   IRW,
			   ALUoutRW,
			   MDRW,
			   BCRW,
			   // Functional Units and MUXs
			   ZSel,
			   BraE,
			   JmpE,
			   RegDst,
			   ALUInA,
			   ALUInB,
			   SESel,
			   WBSel,
			   ALUOp,
			   RegRW,
			   MemRW,
			   BMask,
			   MSESel,
			   
			   Reset,
			   clk) ;
   
   ALUControl  ALUControl (ALUOp, `OPCODE, `FUNC, ALUCtrl);
   ALU         ALU        (aluain, alubin, aluout, ALUCtrl);

   SignExtM    SignExtM   (MSESel, dmemout, mdin);
   SignExt     SignExt    (SESel, `OFFSET, iin);
   Add4        Add4       (PC, npcin);
   ZTest       ZTest      (ZSel, A, bcin);

   Mux         PCMux      ((BC & BraE) | JmpE, NPC, ALUOut, pcin);
   RddrMux     RDMux      (RegDst, `IRD, `RRD, rd);
   Mux         ALUAInMux  (ALUInA, NPC, A, aluain);
   Mux         ALUBInMux  (ALUInB, B, I, alubin);
   WBMux       WBMux      (WBSel, MD, ALUOut, PC, wb2rf);
/*
   wire [`DATA]  DMEM0 = sdlx.DataMem.DMEM[0];
   wire [`DATA]  DMEM1 = sdlx.DataMem.DMEM[1];
   wire [`DATA]  DMEM2 = sdlx.DataMem.DMEM[2];
   wire [`DATA]  DMEM3 = sdlx.DataMem.DMEM[3];
   wire [`DATA]  DMEM4 = sdlx.DataMem.DMEM[4];
   wire [`DATA]  DMEM5 = sdlx.DataMem.DMEM[5];
*/ 
`ifdef CARBON
`else
   
   // Reset
   task apply_reset;
      begin
	 Reset = 1;
	 #`CLOCKPERIOD ;
	 Reset = 0;
//	 PCReg.RegBuf = 0;
//	 BCReg.RegBuf = 0;
      end
   endtask
   
   initial begin : load_prog
      integer i;
      
      // addition with operands from data memory
      //$readmemb("../../TEST/programs/addi.s.bin", IMEM);
      // $readmemb("../../TEST/programs/andy.s.bin", IMEM);
      $readmemb("../../TEST/programs/loop.s.bin", IMEM);

      if (`dbg) 
	begin
	   $display(" Loaded program instructions:");
	   for(i=0; i<10; i=i+1)
	     $display(" %d> %b", i, IMEM[i]);
	end

      if (`dbg) 
	begin
	   $display(" Loaded program data:");
	   for(i=0; i<10; i=i+1)
	     $display(" %d> %b", i, sdlx.DataMem.DMEM[i]);
	end
      
      if (`dbg)
	begin
	   $dumpfile("verilog.dump");
	   $dumpvars(0, sdlx);
	   $display($time, ":%b> PC=%h IR=%h   R1=%h R2=%h R3=%h R31=%h   DMEM[0]=%h DMEM[1]=%h DMEM[2]=%h",
		    clk, PC, IR, RegFile.R1.RegBuf, 
		    RegFile.R2.RegBuf, RegFile.R3.RegBuf,
		    RegFile.R31.RegBuf,
		    DMEM0,
		    DMEM1,
		    DMEM2);
	   $monitor($time, ":%b> PC=%h IR=%h   R1=%h R2=%h R3=%h R31=%h   DMEM[0]=%h DMEM[1]=%h DMEM[2]=%h",
		    clk, PC, IR, RegFile.R1.RegBuf, 
		    RegFile.R2.RegBuf, RegFile.R3.RegBuf,
		    RegFile.R31.RegBuf,
		    DMEM0,
		    DMEM1,
		    DMEM2);
	end
      else if (`vbs)
	$monitor($time, ":%b> %h %h %h %h %h %h %h %h %h",
		 clk, PC, NPC, IR, aluain, alubin, ALUOut, RegFile.R1.RegBuf,
		 RegFile.R2.RegBuf, RegFile.R3.RegBuf);

      apply_reset;
   end

   initial
     begin
	//#2700 $stop;
	#900000;
	$display($time, ":%b> PC=%h IR=%h",
		 clk, PC, IR);
	$display("                        R00=%h R01=%h R02=%h R03=%h R04=%h R05=%h R06=%h R07=%h",
		 RegFile.R0.RegBuf, RegFile.R1.RegBuf, RegFile.R2.RegBuf, RegFile.R3.RegBuf,
		 RegFile.R4.RegBuf, RegFile.R5.RegBuf, RegFile.R6.RegBuf, RegFile.R7.RegBuf);
	$display("                        R08=%h R09=%h R10=%h R11=%h R12=%h R13=%h R14=%h R15=%h",
		 RegFile.R8.RegBuf, RegFile.R9.RegBuf, RegFile.R10.RegBuf, RegFile.R11.RegBuf,
		 RegFile.R12.RegBuf, RegFile.R13.RegBuf, RegFile.R14.RegBuf, RegFile.R15.RegBuf);
	$display("                        R16=%h R17=%h R18=%h R19=%h R20=%h R21=%h R22=%h R23=%h",
		 RegFile.R16.RegBuf, RegFile.R17.RegBuf, RegFile.R18.RegBuf, RegFile.R19.RegBuf,
		 RegFile.R20.RegBuf, RegFile.R21.RegBuf, RegFile.R22.RegBuf, RegFile.R23.RegBuf);
	$display("                        R24=%h R25=%h R26=%h R27=%h R28=%h R29=%h R30=%h R31=%h",
		 RegFile.R24.RegBuf, RegFile.R25.RegBuf, RegFile.R26.RegBuf, RegFile.R27.RegBuf,
		 RegFile.R28.RegBuf, RegFile.R29.RegBuf, RegFile.R30.RegBuf, RegFile.R31.RegBuf);
	$display("                        DMEM[0]=%h DMEM[1]=%h DMEM[2]=%h DMEM[3]=%h DMEM[4]=%h DMEM[5]=%h",
		 DMEM0,
		 DMEM1,
		 DMEM2,
		 DMEM3,
		 DMEM4,
		 DMEM5);
	$stop;
     end
`endif	
endmodule /* sdlx */
