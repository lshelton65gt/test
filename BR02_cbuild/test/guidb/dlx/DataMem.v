
`include "sdlx.h"

module DataMem (Mask, Addr, DataIn, DataOut, MemRW, Clk,
		dmem0, dmem1, dmem2, dmem3, dmem4, dmem5);
   input  [3:0]   Mask;
   input  [`ADDR] Addr;
   input  [`DATA] DataIn;
   output [`DATA] DataOut;
   input 	  MemRW;
   input 	  Clk;
   output [`DATA] dmem0, dmem1, dmem2, dmem3, dmem4, dmem5;
   
   reg    [`DATA] DMEM[0:`MEMSIZE-1];
   wire   [`DATA] DataOut = DMEM[(Addr >> 2) & (`MEMSIZE-1)];
   integer 	  i;
   reg    [`DATA] temp;

   assign 	  dmem0 = DMEM[0];
   assign 	  dmem1 = DMEM[1];
   assign 	  dmem2 = DMEM[2];
   assign 	  dmem3 = DMEM[3];
   assign 	  dmem4 = DMEM[4];
   assign 	  dmem5 = DMEM[5];
   
`ifdef CARBON_BM
`else
   initial
     begin
	for(i=0;i<`MEMSIZE;i=i+1)
	  begin
	     DMEM[i]=0;
	  end
     end
`endif
   
   always @(negedge Clk) begin
      if (MemRW)
	begin
	   temp = DMEM[Addr >> 2];
	   if (Mask[3]) temp[31:24] = DataIn[31:24];
	   if (Mask[2]) temp[23:16] = DataIn[23:16];
	   if (Mask[1]) temp[15: 8] = DataIn[15: 8];
	   if (Mask[0]) temp[ 7: 0] = DataIn[ 7: 0];
	   DMEM[Addr >> 2] = temp;
	end
   end

   wire [31:0] DMEM0 = DMEM[0];
   wire [31:0] DMEM1 = DMEM[1];
   wire [31:0] DMEM2 = DMEM[2];
   wire [31:0] DMEM3 = DMEM[3];
   
   
endmodule /* DataMem */
