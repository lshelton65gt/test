`include "sdlx.h"

module Add4 (I, O);
   
   input  [`DATA] I;
   output [`DATA] O;

   wire   [`DATA] O = I + 4;

endmodule /* Add4 */

