library ieee;
use ieee.std_logic_1164.all;

package pack is
  type rec is
    record
      f1 : std_logic;
      f2 : std_logic;
      f3 : std_logic;
      f4 : std_logic;
    end record;
end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bot is
  port (
    bp1, bp2, bp3, bp4 : in     std_logic;
    brec               : buffer rec);
end bot;

architecture barch of bot is
begin
  brec.f1 <= bp1;
  brec.f2 <= bp3;
  brec.f3 <= bp2 and bp4;
  brec.f4 <= bp1 or bp2 or bp3 or bp4;
end barch;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity mid is
  port (
    mp1, mp2, mp3, mp4 : in     std_logic;
    mrec               : buffer rec);
end mid;

architecture march of mid is
  component bot
    port (
      bp1, bp2, bp3, bp4 : in     std_logic;
      brec               : buffer rec);
  end component;
  
begin
  b1: bot port map
    ( bp1 => mp1, bp2 => mp2, bp3 => mp3, bp4 => mp4, brec => mrec );
end march;


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity top is
  port (
    p1, p2, p3, p4 : in     std_logic;
    o1, o2, o3, o4 : buffer std_logic);
end top;

architecture arch of top is

  signal toprec : rec;

  component mid
    port (
      mp1, mp2, mp3, mp4 : in     std_logic;
      mrec               : buffer rec);
  end component;
  
begin

  m1 : mid port map (
    mp1  => p1,
    mp2  => p2,
    mp3  => p3,
    mp4  => p4,
    mrec => toprec);

  o1 <= toprec.f1;
  o2 <= toprec.f2;
  o3 <= toprec.f3;
  o4 <= toprec.f4;

end arch;
