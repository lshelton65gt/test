# Tests over twocounter.

# check that a gui database is written and both the elaborated
# and unelaborated symbol tables work
cbuild -enableCompositeTypeDump -q -nocc -writeGuiDB -o libtwocounter1.a twocounter.v >& twocounter1.cbld.log
readiodb -hier -all libtwocounter1.gui.db >& twocounter1.elab.log
readiodb -hier -all -unelab libtwocounter1.gui.db >& twocounter1.unelab.log

# check that the -testGuiDB switch works (this writes the gui.db file
# from a subprocess)
PSKIP-Wine PSKIP-Windows cbuild -enableCompositeTypeDump -q -nocc -testGuiDB -o libtwocounter2.a twocounter.v >& twocounter2.cbld.log

# After -testGuiDB there should be both an IO database and a GUI database.
PSKIP-Wine PSKIP-Windows readiodb -hier -all -noLoc -noUnelab libtwocounter2_child.gui.db >& twocounter2.gui.log
PSKIP-Wine PSKIP-Windows readiodb -hier -all -noLoc -noUnelab libtwocounter2.io.db >& twocounter2.io.log

# For the simple, single-module twocounter, io.db and gui.db should be identical.
# Check that the two outputs really are the same. This test guards against
# someone changing the gold files.
PSKIP-Wine PSKIP-Windows diff twocounter2.gui.log twocounter2.io.log >& twocounter2.diff.log

# Check that when the child process forked by -testGuiDB returns with a non-zero
# exit status the parent process raises a fatal error. The -failGuiDB switch
# tells the child to just exit...
PEXIT PSKIP-Wine PSKIP-Windows cbuild -enableCompositeTypeDump -q -nocc -testGuiDB -failGuiDB twocounter.v >& twocounter3.cbld.log

# Tests over dlx - a slightly richer design

# This looks for the dlx verilog in test/dlx. Hopefully, <dep>test/dlx in
# alltests.xml will make sure that it is there...

cbuild -enableCompositeTypeDump -q -nocc ./dlx/sdlx.v -noFlatten -y ./dlx +libext+.v+ +incdir+./dlx/+ +define+CARBON+CARBON_BM+ -writeGuiDB -o libdlx1.a >& dlx1.cbld.log
readiodb -hier -all libdlx1.gui.db >& dlx1.elab.log
readiodb -hier -all -unelab libdlx1.gui.db >& dlx1.unelab.log

# A vhdl design with records

cbuild -enableCompositeTypeDump -q -nocc hierrecord.vhdl -vhdlTop top -writeGuiDB -o libhierrecord1.a >& hierrecord1.cbld.log
readiodb -hier -all libhierrecord1.gui.db >& libhierrecord1.gui.log

# same design, no flattening
cbuild -enableCompositeTypeDump -q -nocc hierrecord.vhdl -vhdlTop top -writeGuiDB -o libhierrecord2.a -noFlatten >& hierrecord2.cbld.log
readiodb -hier -all libhierrecord2.gui.db >& libhierrecord2.gui.log

# Check that user type information is captured in the guidb
cbuild -enableCompositeTypeDump -q -nocc -enableCompositeTypeDump -writeGuiDB -vhdlTop bug8331_01 bug8331_01.vhd >& bug8331_01.cbld.log
carbondb -text libdesign.gui.db >& bug8331_01.db.log

# Check that a symbol that could be optimised away by MemoryBVResynth is preserved when dumping
# the GUI database
cbuild -q -nocc -writeGuiDB -vhdlTop bug -o libbug8664_01.a bug8664_01.vhd >& bug8664_01.cbld.log
readiodb libbug8664_01.gui.db >& bug8664_01.gui.log
grep bug.arr bug8664_01.gui.log >& bug8664_01.grep.log
