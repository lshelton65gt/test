-- This test checks that MemoryBVResynth is disabled in post-population resynth
-- by -writeGuiDB. bug.arr is removed from the symbol table by memory BV resynth
-- and replaced with a $bvmem_... net.

library ieee;
use ieee.std_logic_1164.all;

entity bug is
  
  port (
    clk  : in  std_logic;
    we   : in  std_logic;
    sel  : in  std_logic;
    din  : in  std_logic_vector (1 downto 0);
    dout : out std_logic_vector (1 downto 0));

end bug;

architecture arch of bug is
  type myarray is array (1 downto 0) of std_logic_vector(1 downto 0);
  signal arr: myarray;

begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        if sel = '1' then
          arr(1) <= din;
        else
          arr(0) <= din;
        end if;
      else
        if sel = '1' then
          dout <= arr(1);
        else
          dout <= arr(0);
        end if;
      end if;
    end if;
  end process;
  
end arch;

