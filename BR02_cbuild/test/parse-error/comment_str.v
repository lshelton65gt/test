
module top(in, clk, out);
   input in;
   input clk;
   output out;
   reg    out;
   
   initial begin
      
   end

   always @(posedge clk)
     begin
        out <= in;
        $display("%s", `COMMENTSTR1);
        $display("%s", `COMMENTSTR2);
        $display("%s", `COMMENTSTR3);
     end
   
endmodule // top
