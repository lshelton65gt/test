module dut(i,o);
   
   input i;
   output o;
   assign o=i;
endmodule
