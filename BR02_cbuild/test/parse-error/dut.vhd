library ieee;
use     ieee.std_logic_1164.all;
use ieee.std_logic_1164.all;

Entity dut is port (
	i: in std_logic;
	o: out std_logic);
end dut;

Architecture str of dut is 
begin 
  o <= i;
end str; -- dut
