// filename: test/parse-error/undefined_systask.v
// Description:  This test checks that the user can turn warnings to errors for
// undefined sysTasks/functions.
// try this with -warnForSysTask


module undefined_systask(clock, in1, in2, out1) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & $no_such_function(in2); // the sys task $no_such_function does not exist
     end
endmodule
