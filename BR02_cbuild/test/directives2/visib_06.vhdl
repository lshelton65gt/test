-- September 2007
-- This file tests the visibility of signal temp, which is of nested record
-- type.It's marked observable with inline directive.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is
  type t_rec is
    record
      a : integer;
      b : integer;
    end record;

  type top_rec is
    record
      top_a : t_rec;
      top_b : t_rec;
    end record;
end FIXED_POINT_PKG;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity visib_06 is
  port (
    in1  : in  integer;
    in2  : in  integer;
    out1 : out integer;
    out2 : out integer);
end visib_06;

architecture arch of visib_06 is
  signal temp : top_rec;                -- carbon observeSignal
begin
  temp.top_a.a <= in1;
  temp.top_a.b <= in2;
  temp.top_b.a <= in1;
  temp.top_b.b <= in2;

  out1 <= temp.top_a.a + temp.top_a.b - temp.top_b.b;
  out2 <= temp.top_b.a + temp.top_b.b - temp.top_a.a;
 
end;
