// last mod: Thu Jun  8 12:18:50 2006
// filename: test/directives2/bug6185.v
// Description:  This test duplicates the problem found in bug6185 in a tiny
// testcase.  The issue was that when this was run with the directives:
// observeSignal bug6185.i1.out1
// observeSignal bug6185.i1
// an assert would be generated for the second observeSignal  this directive is
// actually invalid because i1 is a module but it should not have asserted.
// note you must have both directives so that the first causes the creation of
// the elaborated i1 in the directive processing code.


module bug6185(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   mid i1(clock, in1, in2, out1);
endmodule

module mid(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] 	out1;
   always @(posedge clock)
     begin: main
	out1 = in1 & in2;
     end
endmodule

