
module sub3( out, clk, data );
parameter PORT_SIZE = 7;
output [PORT_SIZE:0] out;
input clk;
input [PORT_SIZE:0] data;

  defparam i1.IN_SIZE = PORT_SIZE;
  defparam i1.OUT_SIZE = PORT_SIZE;
  m1 i1( out, clk, data );
endmodule

module m1( out, clk, data );
  parameter BOGUS1 = 59;
  parameter IN_SIZE = 1;
  parameter OUT_SIZE = 2;
  parameter BOGUS2 = 60;

output [OUT_SIZE:0] out;
input clk;
input [IN_SIZE:0] data;
reg [OUT_SIZE:0] out;
  always@( posedge clk )
  begin
    out = data;
  end
endmodule


module m2( data, clk, out );
  parameter BOGUS3 = 61;
  parameter IN_SIZE = 3;
  parameter BOGUS4 = 62;
  parameter OUT_SIZE = 4;

input [IN_SIZE:0] data;
input clk;
output [OUT_SIZE:0] out;
reg [OUT_SIZE:0] out;
  always@( negedge clk )
  begin
    out = ~data;
  end
endmodule
