-- September 2007
-- This file tests the visibility of signal temp, which is of record type.
-- The inline observeSignal directive is used here.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is
  type t_rec is
    record
      a : integer;
      b : integer;
    end record;
end FIXED_POINT_PKG;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity visib_02 is
  port (
    clk  : in  std_logic;
    in1  : in  integer;
    in2  : in  integer;
    out1 : out integer;
    out2 : out integer);
end visib_02;

architecture arch of visib_02 is
  signal temp  : t_rec;                 -- carbon observeSignal
begin

  P1: process (clk, in1, in2)
  begin  -- process P1
    if clk'event and clk = '1' then    -- rising clock edge
     temp.a <= in1;
     temp.b <= in2;
    end if;
  end process P1;
  
  out1  <= temp.a;
  out2  <= temp.b;

end;
