-- September 2007
-- This file tests the visibility of the signal temp.c. Only simple subnet c of
-- nested record is marked observable in the directive file.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is
  type t_rec is
    record
      a : integer;
      b : integer;
    end record;

  type top_rec is
    record
      top_a : t_rec;
      top_b : t_rec;
      c     : integer;
    end record;
end FIXED_POINT_PKG;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity visib_10 is
  port (
    in1  : in  integer;
    in2  : in  integer;
    out1 : out integer;
    out2 : out integer);
end visib_10;

architecture arch of visib_10 is
  signal temp : top_rec;
begin
  temp.top_a.a <= in1;
  temp.top_a.b <= in1;
  temp.top_b.a <= in1;
  temp.top_b.b <= in1;
  temp.c       <= in2;
  

  out1 <= temp.top_a.a + temp.top_a.b - temp.top_b.a - temp.top_b.b + temp.c;
  out2 <= temp.top_a.a + temp.top_a.b - temp.top_b.a - temp.top_b.b + temp.c;
end;
