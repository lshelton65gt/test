module sub_mod_carbon(clk_carbon, in_carbon, out_carbon);
  input clk_carbon, in_carbon;
  output out_carbon;

  reg    out_carbon;

  always @(posedge clk_carbon)
    out_carbon <= in_carbon;

endmodule

