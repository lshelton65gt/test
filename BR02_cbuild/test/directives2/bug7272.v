// last mod: Thu Apr 19 14:27:10 2007
// filename: test/directives2/bug7272.v
// Description:  This test checks that you can apply an in-line observeSignal
// directive to integers, and memories (bugs 7272 and 4702)
// this test should be run with (and without) the -2001 switch because memories
// are handled differently by cheetah

module bug7272(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] 	out1; // carbon observeSignal
   integer 	int1; // carbon observeSignal
   reg [31:0] 	temp1; // carbon observeSignal
   reg [31:0] 	mem1 [7:0]; // carbon observeSignal
   real 	real1; // carbon observeSignal
   time         time1; // carbon observeSignal
   always @(posedge clock)
     begin: main
	int1 = in1 & in2;
	temp1 = int1;
	real1 = int1;
	mem1[0] = temp1;
	time1 = real1;
	out1 = mem1[0];
     end
endmodule
