// Error with hierarchical references into a substituted module.
module sub5( out, clk, data );
output out;
input clk;
input data;
  assign out = i1.hier_net;
  m1 i1( out, clk, data );
endmodule

module m1( out, clk, data );
output out;
input clk;
input data;
wire hier_net;
reg out;
  always@( posedge clk )
  begin
    out = data;
  end
endmodule

module M2( CLK, DATA, OUT );
input CLK;
input DATA;
output OUT;
wire hier_net;
reg OUT;
  always@( negedge CLK )
  begin
    OUT = ~DATA;
  end
endmodule

