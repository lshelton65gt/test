module top(o1, o2, c1, c2, d1, d2);
  output o1, o2;
  input  c1, c2, d1, d2;

  flop u1(o1, c1, d1);
  andgate u2(o2, c2, d2);
endmodule

module flop(q, cp, d);
  output q;
  input  cp, d;
  reg    q;

  always @(posedge cp)
    q <= d;
endmodule

module andgate(out, a, b);
  output out;
  input  a, b;

  assign out = a & b;
endmodule
