
module sub2( out, clk, data );
output out;
input clk;
input data;
  m1 i1( out, clk, data );
endmodule

module m1( out, clk, data );
output out;
input clk;
input data;
reg out;
  always@( posedge clk )
  begin
    out = data;
  end
endmodule


module m2( data, clk, out );
input data;
input clk;
output out;
reg out;
  always@( negedge clk )
  begin
    out = ~data;
  end
endmodule
