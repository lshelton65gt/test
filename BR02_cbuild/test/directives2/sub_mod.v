module sub_mod(clk, in, out);
  input clk, in;
  output out;

  assign out = in;

endmodule

