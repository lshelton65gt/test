// Check that hierrefs within a substituted tre work.
module sub4( out, clk, data );
output out;
input clk;
input data;
  m1 i1( out, clk, data );
endmodule

module m1( out, clk, data );
output out;
input clk;
input data;
reg out;
  always@( posedge clk )
  begin
    out = data;
  end
endmodule

module M2( CLK, DATA, OUT );
input CLK;
input DATA;
output OUT;
  assign OUT = g1.OUT;
  grandchild g1( CLK, DATA, );

endmodule

module grandchild( CLK, DATA, OUT );
input CLK;
input DATA;
output OUT;
reg OUT;
  always@( negedge CLK )
  begin
    OUT = ~DATA;
  end
endmodule
