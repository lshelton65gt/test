// last mod: Thu Jun  1 10:09:26 2006
// filename: test/directives2/bug6151_3.v
// Description:  This is a trimmed down version of the test from bug 6151.
// This would get an internal error when run with the directive: 
// 
// note it does not use a generate at all
// this is variation on bug6151_2.v, run it with observeSignal *.* *.*.*

module bug6151_3(in,out);
   input in;
   output out;
   assign out = fn(in);

   function fn;
      input  din;
      reg temp;          // << note the variable local to the function
      begin: local
	 temp = din;
         fn = ~temp;
      end
   endfunction
endmodule
