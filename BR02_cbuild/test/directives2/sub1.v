
module sub1( out, clk, data );
output out;
input clk;
input data;
  m1 i1( out, clk, data );
endmodule

module m1( out, clk, data );
output out;
input clk;
input data;
reg out;
  always@( posedge clk )
  begin
    out = data;
  end
endmodule

module M2( CLK, DATA, OUT );
input CLK;
input DATA;
output OUT;
reg OUT;
  always@( negedge CLK )
  begin
    OUT = ~DATA;
  end
endmodule

// Port size mismatch
module m3( data, clk, out );
output out;
input [1:0] clk;
input data;
reg out;

endmodule

// Port direction mismatch
module m4( clk, data, out );
input out;
input clk;
input data;

endmodule
