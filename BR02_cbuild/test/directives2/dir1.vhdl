-- April, 2007
-- This file tests inline directives for VHDL
-- There are three inline directives in this file.
-- Two of them are correct (diallowFlattening and observeSignal on in2),
-- and the third one is incorrect.
-- CBuild finds and processes them.
library ieee;
use ieee.std_logic_1164.all;


entity sub is -- carbon disallowFlattening
  port (
    out1, out2 : out std_logic;
    in1, in2  : in  std_logic);          -- carbon observeSignal
end sub;

architecture sub_arch of sub is
  signal temp  : std_logic := '0';       -- carbon depositSignal
begin  -- sub_arch

  temp <= in1 or in2;
  out1 <= temp;
  out2 <= in1 and in2;

end sub_arch;

library ieee;
use ieee.std_logic_1164.all;


entity flop is
  port (
    clk, rst, d : in  std_logic;
    q   : out std_logic);
end flop;

architecture flop_acrh of flop is
begin  -- flop_acrh

  p1: process (clk, rst)
  begin  -- process p1
    if rst = '1' then               -- asynchronous reset (active high)
      q <= '0';                        
    elsif clk'event and clk = '1' then  -- rising clock edge
      q <= d;
    end if;
  end process p1;

end flop_acrh;

  

library ieee;
use ieee.std_logic_1164.all;


entity dir1 is 
  port (
    out1, out2, q  : out std_logic;
    clk,rst        : in std_logic;
    in1, in2       : in std_logic;
    d              : in std_logic); 
end dir1;

architecture dir1arc of dir1 is

begin  -- dir1arc
  s1 : entity work.sub port map (
    in1  => in1,
    in2  => in2,
    out1 => out1,
    out2 => out2);

  f1 : entity work.flop port map (
    clk => clk,
    rst => rst,
    d   => d,
    q   => q);
    
end dir1arc;
