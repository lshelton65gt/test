module flop(q, cp, d);
  output q;
  input  cp, d;
  reg    q;

  always @(posedge cp)
    q <= d;
endmodule

module andgate(out, a, b);
  output out;
  input  a, b;

  assign out = a & b;
endmodule
