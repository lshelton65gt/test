-- September 2007
-- This file tests the visibility of signal temp2, which is of record type and
-- it's in the sub entity. There are two signals of record type temp1 and temp2,
-- but only temp1 is marked with observeSignal inline directive.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is
  type t_rec is
    record
      a : integer;
      b : integer;
    end record;
end FIXED_POINT_PKG;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity sub is
  port (
    insub1  : in  integer;
    insub2  : in  integer;
    outsub  : out integer);
end sub;

architecture sub_arch of sub is
  signal temp1 : t_rec;                 -- carbon observeSignal
  signal temp2 : t_rec;                
begin  -- sub_arch

  temp1.a <= insub1;
  temp1.b <= insub2;

  temp2 <= temp1;
  
  outsub <= temp2.a + temp2.b;
end sub_arch;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity visib_04 is
  port (
    clk  : in  std_logic;
    in1  : in  integer;
    in2  : in  integer;
    out1 : out integer;
    out2 : out integer);
end visib_04;

architecture arch of visib_04 is
begin

  s1 : entity work.sub 
    port map(
      insub1 => in1,
      insub2 => in2,
      outsub => out1 );

  s2 : entity work.sub 
    port map(
      insub1 => in1,
      insub2 => in2,
      outsub => out2 );
end;
