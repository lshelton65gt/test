module flop(q, cp, d);
  output q;
  input  cp, d;
  reg    q;

  always @(posedge cp)
    q <= d;
endmodule
