module top(in1, in2, sel, o);
   input in1, in2;
   input sel; // carbon ignoreSynthCheck
   output o;
   reg    o;

   always @(in1 or in2)
     begin: b
        reg f; // carbon ignoreSynthCheck
        if (sel) f = in1;
        if (sel) o = f & in2;
     end
endmodule

