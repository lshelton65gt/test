-- September 2007
-- This file tests the visibility of signal temp, which is of nested record type and
-- it's defined in the sub entity. The temp is marked as observeble with inline
-- directive.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is
  type t_rec is
    record
      a : integer;
      b : integer;
    end record;

  type top_rec is
    record
      top_a : t_rec;                   
      top_b : t_rec;
    end record;
end FIXED_POINT_PKG;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity sub is
  port (
    insub1  : in  integer;
    insub2  : in  integer;
    outsub  : out integer);
end sub;

architecture sub_arch of sub is
  signal temp : top_rec;        -- carbon observeSignal
begin  -- sub_arch

  temp.top_a.a <= insub1;
  temp.top_a.b <= insub2;
  temp.top_b.a <= insub1;
  temp.top_b.b <= insub2;

  outsub <= temp.top_a.a + temp.top_a.b - temp.top_b.a - temp.top_b.b;
end sub_arch;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity visib_08 is
  port (
    clk  : in  std_logic;
    in1  : in  integer;
    in2  : in  integer;
    out1 : out integer;
    out2 : out integer);
end visib_08;

architecture arch of visib_08 is
  signal top_temp1 : integer;
  signal top_temp2 : integer;
begin

  top_temp1 <= in1;
  top_temp2 <= in2;
  
  s1 : entity work.sub 
    port map(
      insub1 => in1,
      insub2 => in2,
      outsub => top_temp1 );

  s2 : entity work.sub 
    port map(
      insub1 => in1,
      insub2 => in2,
      outsub => top_temp2 );

  out1 <= top_temp1;
  out2 <= top_temp2;
  
  
end;
