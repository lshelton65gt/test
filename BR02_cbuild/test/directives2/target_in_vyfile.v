module top(o1, o2, c1, c2, d1, d2);
  output o1, o2;
  input  c1, c2, d1, d2;

  flop u1(o1, c1, d1);
  andgate u2(o2, c2, d2);
endmodule
