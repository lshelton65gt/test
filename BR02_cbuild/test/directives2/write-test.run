#!/bin/bash

#
#   test.run for   additional directives tests
#
USING_VERIFIC=`echo $CARBON_CBUILD_ARGS | grep -c -e "-useVerific"`
PREFIX=""
if [[ $USING_VERIFIC == 1 ]]; then
## currently Verific flow does not support substituteModule directive (D6.1.1)
PREFIX="VTSKIP "
fi
#echo "USING_VERIFIC: $USING_VERIFIC:, PREFIX $PREFIX"


### sustituteModule
# directive errors
for i in 1 2 3 4 5 6 7 8 9 10
do 
  echo "${PREFIX}PEXIT cbuild -q -nocc -noEncrypt -vlogTop sub1 -directive sub1e$i.dir sub1.v" ' >& ' "sub1e$i.log"
done


echo "${PREFIX}PEXIT cbuild -q -nocc -noEncrypt -vlogTop sub5 -directive sub1.dir sub5e.v >& sub5e.log"

# basic tests
for i in 1 2 3
do 
  echo "${PREFIX}cbuild -q -noEncrypt -vlogTop sub$i -directive sub$i.dir -o libsub$i.$SOEXT sub$i.v -testdriver" ' >& ' "sub$i.cbld.log"
  echo "${PREFIX}runtest ./sub$i.exe " ' >& ' " sub$i.run.log"
done

cat <<EOF
${PREFIX}cbuild -q -vlogTop sub6 -directive sub6.dir sub6.v sub_mod.v sub_mod_carbon.v -testdriver -o libsub6.$SOEXT >& sub6.cbld.log
${PREFIX}runtest ./sub6.exe >& sub6.run.log

${PREFIX}cbuild -q -vlogTop top -directive target_in_design.dir target_in_design.v -o libtarget_in_design.$STATEXT >& target_in_design.cbld.log
${PREFIX}diff libtarget_in_design.hierarchy target_in_design.hierarchy.gold

${PREFIX}cbuild -q -vlogTop top -directive target_in_design.dir target_in_vyfile.v -o libtarget_in_vfile.$STATEXT -v vfile.v >& target_in_vfile.cbld.log
${PREFIX}diff libtarget_in_vfile.hierarchy target_in_vfile.hierarchy.gold

${PREFIX}cbuild -q -vlogTop top -directive target_in_design.dir target_in_vyfile.v -o libtarget_in_yfile.$STATEXT -y ydir +libext+.v >& target_in_yfile.cbld.log
${PREFIX}diff libtarget_in_yfile.hierarchy target_in_yfile.hierarchy.gold

### test some directives you cannot apply to nets within a function
PEXIT cbuild -q -2001 -directive bug6151_2.directives  bug6151_2.v  -testdriver >& bug6151_2.cbld.log
PEXIT cbuild -q -2001 -directive bug6151_3.directives  bug6151_3.v  -testdriver >& bug6151_3.cbld.log

# if you try to apply an observeSignal to a module you should get an Warning
PEXIT cbuild -q -2001 -directive bug6185.directives  bug6185.v  -testdriver >& bug6185.cbld.log

# bug 7272, in-line observeSignal was not being applied to integers
/bin/rm -rf libdesign.* 
cbuild -q bug7272.v      -nocc >& bug7272.cbld.log
cat libdesign.dir | grep -v '/work/' | grep -v '/w/' >& bug7272.directive.log
/bin/rm -rf libdesign.* 
cbuild -q bug7272.v -v2k -nocc  >& bug7272_2k.cbld.log
cat libdesign.dir | grep -v '/work/' | grep -v '/w/' >& bug7272_2k.directive.log


/bin/rm -rf libdesign.* 
PEXIT cbuild -q -vhdlTop dir1 dir1.vhdl >& dir1.cbld.log
ITSKIP echo 'VTSKIP/VHDL this test because it uses VHDL which doesnt yet handle directives correctly'
VTSKIP cat libdesign.dir | grep -v '/work/' | grep -v '/w/' >& dir1.directive.log

# bug 7632, msg 30505 should be a warning, not an error. Also, replaced 'suppressMsg' with 'silentMsg'
# for Warning 136, which is issued when an user tries to change the severity of an error.
/bin/rm -rf libdesign.*
cbuild -q -vhdl_synth_prefix pragma -directive dir2.dir -vhdlTop dff dir2.vhdl >& dir2.cbld.log
cat libdesign.dir | grep -v '/work/' |  grep -v '/w/' >& dir2.directive.log

# test the inline directive on the nets declared inside the alwas block
cbuild -q bug11617_01.v -nocc  >&  bug11617_01.cbld.log
cat libdesign.dir | grep -v '/work/' | grep -v '/w/' >& bug11617_01.directive.log


# VISBILITY
rm -rf lib*
cbuild -q -vhdlTop visib_01 -directive visib_01.dir visib_01.vhdl -testdriver >& visib_01.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_01.symtab

# visib_02 seems to have infinite loop in verific flow
rm -rf lib*
VTSKIP cbuild -q -vhdlTop visib_02 visib_02.vhdl -testdriver >& visib_02.cbld.log
VTSKIP carbondb libdesign.symtab.db | grep 'Observed' >& visib_02.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_03 -directive visib_03.dir visib_03.vhdl -testdriver >& visib_03.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_03.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_04 visib_04.vhdl -testdriver >& visib_04.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_04.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_05 -directive visib_05.dir visib_05.vhdl -testdriver >& visib_05.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_05.symtab


# visib_06 seems to have infinite loop in verific flow
rm -rf lib*
VTSKIP cbuild -q -vhdlTop visib_06 visib_06.vhdl -testdriver >& visib_06.cbld.log
VTSKIP carbondb libdesign.symtab.db | grep 'Observed' >& visib_06.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_07 -directive visib_07.dir visib_07.vhdl -testdriver >& visib_07.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_07.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_08 visib_08.vhdl -testdriver >& visib_08.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_08.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_09 -directive visib_09.dir visib_09.vhdl -testdriver >& visib_09.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_09.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_10 -directive visib_10.dir visib_10.vhdl -testdriver >& visib_10.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_10.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_01 -directive visib_11.dir visib_01.vhdl -testdriver >& visib_11.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_11.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_03 -directive visib_12.dir visib_03.vhdl -testdriver >& visib_12.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_12.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_05 -directive visib_13.dir visib_05.vhdl -testdriver >& visib_13.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_13.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_07 -directive visib_14.dir visib_07.vhdl -testdriver >& visib_14.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_14.symtab


rm -rf lib*
cbuild -q -vhdlTop visib_10 -directive visib_15.dir visib_10.vhdl -testdriver >& visib_15.cbld.log
carbondb libdesign.symtab.db | grep 'Observed' >& visib_15.symtab

EOF


