// last mod: Thu Jun  1 11:32:59 2006
// filename: test/directives2/bug6151_2.v
// Description:  This is a trimmed down version of the test from bug 6151.
// This would get an internal error when run with the directive: 
// observeSignal *.* *.*.*.*
// 


module bug6151_2(in,out);
   input in;
   output out;
   assign out = fn(in);

   function fn;
      input  din;
      begin: local
	 reg temp;   // <<< note a local variable
	 temp = din;
         fn = ~temp;
      end
   endfunction
endmodule
