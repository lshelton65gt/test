// Tests substitution of specific instances, instead of all instances of
// module using substituteModule <instance name> <module name> directive.
// The instances are at various levels of hierarchy, and the port names
// also have to be substituted.

module sub6(clk, in, out);
  input clk;
  input [5:0] in;
  output [5:0] out;

  wire [5:0]    out;

  sub_mod t0(.clk(clk), .in(in[0]), .out(out[0]));
  sub_mod t1(.clk(clk), .in(in[1]), .out(out[1]));

  sub6_level1 tl1(.clk(clk), .in(in[5:2]), .out(out[5:2]));
endmodule

module sub6_level1(clk, in, out);
  input clk;
  input [3:0] in;
  output [3:0] out;

  wire [3:0]   out;

  sub_mod t0(.clk(clk), .in(in[0]), .out(out[0]));
  sub_mod t1(.clk(clk), .in(in[1]), .out(out[1]));

  sub6_level2 tl2(.clk(clk), .in(in[3:2]), .out(out[3:2]));
endmodule  

module sub6_level2(clk, in, out);
  input clk;
  input [1:0] in;
  output [1:0] out;

  sub_mod t0(.clk(clk), .in(in[0]), .out(out[0]));
  sub_mod t1(.clk(clk), .in(in[1]), .out(out[1]));

endmodule  
