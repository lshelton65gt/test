-- Test memory->bv conversion with task/function args.
library ieee;
use ieee.std_logic_1164.all;

package pack is
  constant BITWIDTH : integer := 8; -- NUMBITS
  constant PIPEWIDTH : integer := 4; -- NUMBYTES
  constant NUMPIPES : integer := 2;
  type bytebank is array (PIPEWIDTH-1 downto 0) of std_logic_vector(BITWIDTH-1 downto 0);
  type databank is array (NUMPIPES-1 downto 0) of bytebank;

  procedure proc1 (
    signal in1  : in  bytebank;
    signal out1 : out bytebank);
end pack;

package body pack is
  procedure proc1 (
    signal in1  : in  bytebank;
    signal out1 : out bytebank) is
  begin
    out1 <= in1;
  end;
end pack;


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity top is
  port(
    in1: in bytebank;
    out1: out bytebank
  );
end top;

architecture top of top is
  component bottom is
  port (
    indata  : in bytebank;
    outdata : out bytebank);
  end component bottom;
  signal localin : bytebank;
  signal localout : bytebank;
begin  -- top
  process (in1)
  begin  -- process
    for i in 0 to PIPEWIDTH-1 loop
      localin(i) <= in1(i);
    end loop;  -- i
  end process;

  process (localout)
  begin  -- process
    for i in 0 to PIPEWIDTH-1 loop
      out1(i) <= localout(i);
    end loop;  -- i
  end process;

  process (localin)
  begin  -- process
    proc1(localin, localout);
  end process;

end top;
