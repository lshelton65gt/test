// Test of memory to BV conversion.
// Dynamic bit accesses and requires a state update variable.
`define WIDTH 4
`define DEPTH 4
`define ADDRBITS 2 // log2(DEPTH)
module top(clka, clkb, clkc, di, do, wea, wec, adra, adrb, adrc);
   input clka, clkb, clkc, wea, wec;
   input [`WIDTH - 1:0] di;
   output [`WIDTH - 1:0] do;
   reg [`WIDTH - 1:0]    do;
   reg [`WIDTH - 1:0]    mem_array [`DEPTH - 1:0];
   input [`ADDRBITS - 1:0]   adra;
   input [`ADDRBITS - 1:0]   adrb;
   input [`ADDRBITS - 1:0]   adrc;

   always @(posedge clkb)
     begin
        mem_array[adrb] = di;
     end

   always @(posedge clka)
     begin
        if (wea)
          mem_array[adra] = di;
        do = mem_array[adra];
     end

   always @(posedge clkc)
     begin
        if (wec)
          mem_array[adrc] = di;
        do = mem_array[adrc];
     end

endmodule
