library IEEE;
use IEEE.std_logic_1164.all;

package pack is
   constant NBACC         : integer := 4;
   type ARRAY_OF_ACCEN    is array (0 to NBACC-1) of std_logic_vector(3 downto 0);
end pack;

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_arith.all;
library work;
use work.pack.all;

entity top is
port (
  accloc : in std_logic_vector(3 downto 0);
  o : out std_logic_vector(3 downto 0)
  );
end top;

architecture top of top is
begin  -- top

process (accloc)
  variable loc16 : integer range 0 to 3;
  variable naccen : ARRAY_OF_ACCEN;
begin  -- process
  loc16 := CONV_INTEGER(UNSIGNED(accloc(1 downto 0)));
  naccen(0)(loc16  ) := '1';
  o <= naccen(0);
end process;

end top;
