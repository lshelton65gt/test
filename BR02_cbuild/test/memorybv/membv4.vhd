-- Test memory->bv conversion with port args.
-- This testcase should have bottom.indata be converted.
library ieee;
use ieee.std_logic_1164.all;

package pack is
  constant BITWIDTH : integer := 8; -- NUMBITS
  constant PIPEWIDTH : integer := 4; -- NUMBYTES
  constant NUMPIPES : integer := 2;
  type bytebank is array (PIPEWIDTH-1 downto 0) of std_logic_vector(BITWIDTH-1 downto 0);
  type databank is array (NUMPIPES-1 downto 0) of bytebank;
end pack;



library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity bottom is
  port (
    indata  : in     bytebank;
    outdata : out bytebank);
end bottom;

architecture arch of bottom is

begin  -- arch
  process (indata)
  begin  -- process
    for i in 0 to PIPEWIDTH-1 loop
      outdata(i) <= indata(i);
    end loop;  -- i
  end process;
end arch;



library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity top is
  port(
    in1: in bytebank;
    out1: out bytebank
  );
end top;

architecture top of top is
  component bottom is
  port (
    indata  : in bytebank;
    outdata : out bytebank);
  end component bottom;
  signal localin : bytebank;
begin  -- top
  process (in1)
  begin  -- process
    for i in 0 to PIPEWIDTH-1 loop
      localin(i) <= in1(i);
    end loop;  -- i
  end process;
  u1 : bottom port map (localin, out1);
end top;
