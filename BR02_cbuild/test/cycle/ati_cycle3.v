module top (out, in1);
   output [6:0] out;
   input [32:0] in1;
 
   wire [6:0]  net_;
 
   wire [33:0]  _net_6;
   assign      _net_6 = in1[32:0]<<0;
   assign      net_[6] = ~(| in1[32:0]);
   wire [33:0]  _net_5;
   assign      _net_5 = _net_6<<(64&(-net_[6]));
   assign      net_[5] = ~(| _net_5[32:1]);
   wire [33:0]  _net_4;
   assign      _net_4 = _net_5<<(32&(-net_[5]));
   assign      net_[4] = ~(| _net_4[32:17]);
   wire [33:0]  _net_3;
   assign      _net_3 = _net_4<<(16&(-net_[4]));
   assign      net_[3] = ~(| _net_3[32:25]);
   wire [33:0]  _net_2;
   assign      _net_2 = _net_3<<(8&(-net_[3]));
   assign      net_[2] = ~(| _net_2[32:29]);
   wire [33:0]  _net_1;
   assign      _net_1 = _net_2<<(4&(-net_[2]));
   assign      net_[1] = ~(| _net_1[32:31]);
   wire [33:0]  _net_0;
   assign      _net_0 = _net_1<<(2&(-net_[1]));
   assign      net_[0] = ~(| _net_0[32:32]);
 
   
   assign      out = ~net_;
endmodule // top
