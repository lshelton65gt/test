module cycle(in, clk1, clk2, out);
input in, clk1, clk2;
output out;

wire in, out, clk1, clk2;

reg [1:0] x;
reg [1:0] y;

always @(clk1 or clk2 or x or y)
begin
  if (clk1)
    x[0] = in;
  if (clk2)
    y[1] = ~x[0];
  if (clk1)
    x[1] = ~y[1];
  if (clk2)
    y[0] = ~x[1];
end

assign out = ~y[0];

endmodule
