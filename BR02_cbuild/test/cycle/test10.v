// The for loop creates cyclic flow that is completely internal to the always block
module DW_ecc(gen, dataout);
        input  gen;
        output dataout;

        reg dataout;

        integer c;
        integer val;
        integer idx;

        reg n;

        always@(gen)
        begin
          c = 0;
          for (idx = 0; c < 0; idx = (idx + 1))
          begin
            val = 0;
            if (val > 1)
            begin
              n = c ^ 1;
              c = c + 1;
            end
          end
        end

        always @(gen) begin : PROC2
          dataout = n;
        end
endmodule
