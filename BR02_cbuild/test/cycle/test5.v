// a cycle with two memories but no nets

module top(addr, out);
input addr;
output out;

wire [7:0] addr;
reg [7:0] mema [0:100];
reg [7:0] memb [0:100];

// there is a long-period cycle when addr = 50
reg incr;
always @(addr)
begin : block
  reg [7:0] tmp [0:100];
  tmp[addr] = ~memb[addr];
  incr = (tmp[100-addr] != 8'hff);
  mema[addr] = memb[100-addr] + incr;
end

always @(addr)
  memb[addr] = mema[100-addr];

// out should be 0xfe for addr=50, if the cycle settles fully
assign out = mema[addr] + memb[addr];

endmodule

