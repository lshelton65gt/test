// This test case comes from EMC
module bug(a, b, par, z);
   input [3:0] a;
   input [3:0] b;
   input       par;
   output [8:0] z;

   assign z = {^z[7:0]^par,
	       a,
	       b};

endmodule // bug
