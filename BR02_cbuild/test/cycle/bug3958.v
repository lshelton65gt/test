module top(sel, d, o, o1);
   input [1:0] sel;
   input d;
   output [1:0] o;
   output [1:0] o1;

   assign       o[0] = sel[0] ? o1[0] : 'bz;
   assign       o[1] = sel[1] ? o1[1] : 'bz;

   assign o1 = sel ^ o;

endmodule
