module top(clk, in1, in2, in3, out);
input clk, in1, in2, in3;
output out;

reg a, b, c, d, e, f, g, h;

always @(clk)
begin
  if (clk)
  begin
    a = ~b ^ in1;
    c = d ^ g ^ in2;
    e = ~f ^ in3;
  end
end

always @(clk)
begin
  if (~clk)
  begin
    b = a ^ c;
    d = ~a;
  end
end

always f = ~e;

always @(clk)
begin
  if (~clk)
    g = ~h;
  else
    h = g ^ b;
end

assign out = a ^ f;

endmodule
