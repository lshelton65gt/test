module top (out1, out2, clk, en1, en2, in);
   output out1, out2;
   input  clk, en1, en2, in;

   wire   gclk1;
   reg    ren1;
   reg    ren2;
   wire   gclk2;
   
   assign gclk1 = clk & ren1;
   always @ (gclk1 or en1)
     if (~gclk1)
       ren1 <= en1;

   reg    out1;
   always @ (posedge gclk1)
     out1 <= in;

   always @ (gclk2 or en2)
     if (~gclk2)
       ren2 <= en2;
   assign gclk2 = clk & ren2;

   reg    out2;
   always @ (posedge gclk2)
     out2 <= in;

endmodule

