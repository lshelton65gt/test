module cycle(out);
output out;

wire out;

wire [3:0] bus;
wire clk;
reg dclk;

assign out = ~dclk;

assign bus[3:2] = dclk ? 0 : 2'bz;

assign bus[2:0] = ~bus[3:1];

assign clk = dclk & bus[1];

always @(negedge clk)
  dclk = ~dclk;

endmodule
