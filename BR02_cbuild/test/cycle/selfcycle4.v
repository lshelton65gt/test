// Test for the various equiality and logical operators when breaking
// net cycles.
module top(out, in1, in2);
   output [17:0] out;
   input [3:0]  in1, in2;

   assign out = { out[3:0] == out[7:4],
                  out[3:0] != out[7:4],
                  out[3:0] === out[7:4],
                  out[3:0] !== out[7:4],
                  out[3:0] && out[7:4],
                  out[3:0] || out[7:4],
                  out[3:0] < out[7:4],
                  out[3:0] <= out[7:4],
                  out[3:0] > out[7:4],
                  out[3:0] >= out[7:3],
	          in1,
	          in2};

endmodule // bug
