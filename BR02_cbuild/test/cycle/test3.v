module top(reset, out);
input reset;
output out;

wire reset;
wire [31:0] out;

reg [31:0] counter;
wire [31:0] temp;

always@(reset or temp or counter)
begin
  if (reset)
    counter = 0;
  else
    counter = temp + 1;
end

assign temp = counter;

assign out = counter;

endmodule

