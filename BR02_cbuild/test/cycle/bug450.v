module top(in1, in2, vect);
   input [11:0] in1;
   input [1:0] in2;
   output [15:0] vect;
   reg [15:0] 	vect;
   reg 		foo;
   
   always
     begin
	vect[11:0] = in1;
	vect[13:12] = foo;
	vect[15:14] = in2;
     end

   always
     begin
	foo = vect[3:1] == 3'b0;
     end
   
endmodule // top
