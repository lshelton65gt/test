// Test of multiple levels, multiple drivers, and port connections.
// Must run with -noFlatten.
module top(o, sel, d, e);
   output [3:0] o;
   input [3:0]  sel;
   input [3:0]  d;
   input [3:0]  e;

   wire [3:0]   w;
   
   middle middle1 (o, w, e);
   assign       o[2] = w[2] | e[2];
   driver driver (w, sel, d);
   assign       w[0] = sel[0] ? d[0] : 'bz;
endmodule

module driver(o, sel, d);
   output [4:1] o;
   input [4:1]  sel;
   input [4:1]  d;

   driverbottom  #(2) driverbottom2 (o, sel, d);
   assign       o[3] = sel[3] ? d[3] : 'bz;
   driverbottom #(4) driverbottom4 (o, sel, d);
endmodule

module driverbottom(o, sel, d);
   parameter    bitnum = 0;
   output [5:2] o;
   input [5:2]  sel;
   input [5:2]  d;
   assign       o[bitnum+1] = sel[bitnum+1] ? d[bitnum+1] : 'bz;
endmodule

module middle(o, w, e);
   output [6:3] o;
   input [6:3]  w;
   input [6:3]  e;

   assign       o[4] = w[4] | e[4];

   middlebottom #(3) middlebottom3 (o, w, e);
   middlebottom #(6) middlebottom6 (o, w, e);
endmodule

module middlebottom(o, w, e);
   parameter bitnum = 0;
   output [7:4] o;
   input [7:4]  w;
   input [7:4]  e;
   assign       o[bitnum+1] = w[bitnum+1] | e[bitnum+1];
endmodule
