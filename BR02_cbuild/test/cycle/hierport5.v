// Test of bidirect expression synthesis when
// some bits of the enable are not used and are
// elaboratedly dead.
//
// This is a small testcase which shows a problem
// that amd/pogo uncovered in expr synth.
//
// Run with -noFlatten
module top(io0, data, env1, env2);
   inout [1:0] io0;
   input data;
   input [31:0] env1, env2;

   wire [31:0]  env;

   pads  pads0 (io0, env, data);
   enablegen enablegen (env1, env2, env);
endmodule

module pads(io, enables, d);
   inout [1:0] io;
   input [31:0] enables;
   input d;
   assign io[0] = enables[0] ? d : 1'bz;
endmodule

module enablegen(env1, env2, env);
   input [31:0] env1, env2;
   output [31:0] env;

   assign        env[15:0] = env1[15:0] & env2[15:0];
   assign        env[31:16] = ~env1[31:16];
endmodule
