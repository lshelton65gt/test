module mybuf(in,out);
  input in;
  output out;

  assign out = in;
endmodule

module sched(clk,in,out);
  input clk, in;
  output out;

  wire [5:0] net;
  reg out;

  assign net[0] = in;

  mybuf u0 (net[0], net[1]);
  mybuf u1 (net[2], net[3]);
  mybuf u2 (net[4], net[5]);

  assign net[2] = net[1];
  assign net[4] = net[3];
  
  always @(posedge clk)
    out = net[1] & in;

endmodule

