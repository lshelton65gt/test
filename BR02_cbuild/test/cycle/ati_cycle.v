module top(abs, out);
  output [6:0] out;
  wire [2:0] shift_vec;        // sc_bc_w.ulc_dx_urc_dy_shift_
  wire [3:0] shift1_vec;        // __ulc_dx_urc_dy_shift__1
  input [3:0] abs;              // ulc_dx_urc_dy_abs_

  assign1 u1(shift_vec[2:1], shift1_vec, abs);
  assign2 u2(abs, shift1_vec, shift_vec);
  assign3 u3(shift1_vec, shift_vec[0], shift_vec);

  assign out = {shift_vec, shift1_vec};
endmodule

module assign1(shift_vec, shift1_vec, abs);
  output [2:1] shift_vec;
  input [3:0]  shift1_vec, abs;
  reg [2:1]    shift_vec;

  initial shift_vec = 2'b00;

  always @(shift1_vec or abs) begin
    // ../../../customer/rtl/parts_lib/src/gfx/sc/sc_bc_w.bvrl:3301
    shift_vec[1] = ! (| shift1_vec[3:2]);

    // ../../../customer/rtl/parts_lib/src/gfx/sc/sc_bc_w.bvrl:3298
    shift_vec[2] = ! (| abs[3:0]);
  end
endmodule

module assign2(abs, shift1_vec, shift_vec);
  output [3:0] shift1_vec;
  input [3:0]  abs;
  input [2:0]  shift_vec;

  reg [3:0]    shift_2;
  reg [3:0]    shift1_vec;

  initial shift1_vec = 0;
  initial shift_2 = 0;

  always @(abs or shift_vec or shift_2) begin
    // ../../../customer/rtl/parts_lib/src/gfx/sc/sc_bc_w.bvrl:3297
    shift_2 = abs[3:0] << 0;

    // ../../../customer/rtl/parts_lib/src/gfx/sc/sc_bc_w.bvrl:3300  
    shift1_vec = shift_2 << shift_vec[2];
  end
endmodule

module assign3(shift1_vec, shift_vec0, shift_vec);
  output shift_vec0;
  input [3:0]  shift1_vec;
  input [2:0]  shift_vec;

  reg [4:0] shift_0;
  reg  shift_vec0;

  initial shift_0 = 0;
  initial shift_vec0 = 1'b0;

  always @(shift1_vec or shift_vec or shift_0) begin
    // ../../../customer/rtl/parts_lib/src/gfx/sc/sc_bc_w.bvrl:3303
    shift_0 = shift1_vec << shift_vec[1];

    // ../../../customer/rtl/parts_lib/src/gfx/sc/sc_bc_w.bvrl:3304  
    shift_vec0 = ! (shift_0[3]);
  end
endmodule
