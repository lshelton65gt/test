// This should not produce a cycle, although it did at one
// point because of a bug in handling of non-continuous
// nested flow.  MarkDesign::fillFaninSet was including
// the continuous driver of b as a fanin of c, when
// it should not have.

module test13(sel, in, out, out2);
input sel, in;
output out, out2;

wire sel, in, out, out2;
reg [1:0] a,b,c,d;

always
begin
    a = {1'b0,in};
end

always
begin
  b = ~a;
  if (!sel)
    c[0] = ~b[1];
  else
    c[1] = b[1];
  b[0] = &c;
  b[1] = d[0];
end

always
begin
  d = ~a;
end

assign out = b[0] ^ b[1];
assign out2 = b[1] ^ d ^ a;

endmodule
