module bug(in, out);
input in;
output out;

wire in;
wire [0:3] out2;
wire [0:3] out;
wire en;

assign out2[1] = ~out2[0];

assign out2[3] = in;

assign out2[2] = en ? ~out2[3] : 1'bz;

assign out = ~out2;

endmodule
