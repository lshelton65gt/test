module top(a, f, out);
  input a, f;
  output out;
  reg    out;
  wire   b, c, d, e;

  initial out = 0;
  always @(e)
    out = e;

  assign b = a & c;
  assign c = b & d;
  assign d = c & e;
  assign e = d & f;
endmodule // top
