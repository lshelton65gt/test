module top(b, out);
  input b;
  output out;
  reg    out;
  wire   c, d, e, f, g, h, i, j;

  // having a reg output allows us to initialize it
  // so that verilog XL gives the same answer as
  // Carbon at the first timestep
  initial out = 0;
  always @(j)
    out = j;

  assign c = b & d;
  assign d = c & e;
  assign e = d & f;
  assign f = e & g;
  assign g = f & h;
  assign h = g & i;
  assign i = h & j;
  assign j = i & b;
endmodule // top
