// This is from bug 133

module cycle5 (in, out, clk);
   input clk;
   input in;
   output [1:0] out;
   reg 	  t1;
   reg [1:0] out, data;
   
   always @(in or t1)
     begin
	data = {t1, in};
     end

   always @(in or data)
     begin
	t1 = data[0];
     end

   always @(posedge clk)
     begin
	out = data;
     end

endmodule


