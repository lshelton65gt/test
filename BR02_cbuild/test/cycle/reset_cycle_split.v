module bug(clk,in,in2,out,rst);
  input clk,in,in2,rst;
  output out;
  reg q1,q1p,q2,q2p,reset,q3p,out;

  wire anded_rst = reset & rst;

  always @(posedge clk)
    q1p <= in & anded_rst;
  always @(anded_rst or q1p)
    q1 = q1p & anded_rst;

  always @(posedge clk)
    q2p <= ~q1;
  always @(q1 or q2p)
    q2 <= q2p | ~q1;

  always @(posedge clk)
    q3p <= q2;
  always @(q2 or q3p)
    reset = q2 & q3p;

  always @(posedge clk or negedge reset)
    if (~reset)
      out = 0;
    else
      out = in2;
endmodule
