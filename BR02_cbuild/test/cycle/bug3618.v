module cycle(in, clk1, clk2, out);
input in, clk1, clk2;
output out;

wire in, out, clk1, clk2;

reg [1:0] x;
reg [1:0] y;

always @(clk1 or in)
  if (clk1)
    x[0] = in;

always @(clk1 or y)
  if (clk1)
    x[1] = ~y[1];

always @(clk2 or x)
  if (clk2)
    y[0] = ~x[1];

always @(clk2 or x)
  if (clk2)
    y[1] = ~x[0];

assign out = ~y[0];

endmodule
