module top (out, in0, in1, in2, in3, in4, in5, in6);
   output [35:0] out;
   input         in0;
   input [7:0]   in1, in2, in3, in4, in5, in6;
   

   assign 	  out = {1'b0, // no parity, let LM provide it
			   ^out[23:16]^in0,
			   ^out[15:8]^in0,
			   ^out[7:0]^in0,
			   8'h0, // LM command bits and spares
			   1'h0, //23
			   in1[7], //no_done_int
			   2'h0, // 21:20
			   in2[4:0], // 19:15 = Done, OVFs, Gross error
			   in1[6], // 14 = MM diag
			   1'b0, // 13 = UM register, only in DSA
			   in3[4], // 12 = Address overflow
			   in1[3:2], // 11:10 = Always return status bits
			   1'b0, // 9 = Stop/kill bit not in MM
			   in1[0], // 8 = uCode bit
			   in4[7], // 7 = hi priority
			   in5[6], // 6 = started
			   in4[5], // 5 = DW disable
			   in6[0], // 4 = not used
			   2'b00, // 3:2 = sub_dir, only on DMA
			   in4[1:0] // 1:0 = Direction, Enable
			   };

endmodule // top
