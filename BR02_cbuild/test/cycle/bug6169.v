module bug6169(clk,cin,qin,out);
   input clk;
   input cin,qin;
   output out;

   reg 	  s1,s2;
   reg [2:0] blah_output;
   reg 	  comb_output;
   reg 	  flop_output;

   function [2:0] blah;
      input a;
      input b;
      input c;
      blah = {a,b,c};
   endfunction

   initial s1=1'b0;
   initial s2=1'b0;
   always @(cin or s1 or s2) begin
      blah_output = blah(cin,s1,s2);
      {s1,s2,comb_output} = blah_output;
   end

   initial flop_output = 1'b0;
   always @(posedge clk) begin
      flop_output = qin;
   end

   assign out = flop_output & (comb_output == blah_output[2]);
endmodule
