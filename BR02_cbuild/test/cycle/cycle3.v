module cycle3(in, out);
  input in;
  output out;
  wire [2:0] shift;

  assign out = shift[0];
  assign shift[0] = ~shift[1];
  assign shift[1] = ~shift[2];
  assign shift[2] = ~in;
endmodule // cycle3
