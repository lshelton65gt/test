module top (out1, out2, out3, out4, out5, out6, in1, in2, clk);
   output out1, out2, out3, out4, out5, out6;
   input  in1, in2, clk;

   // register the inputs
   wire   r1, r2;
   flop F1 (r1, clk, in1);
   flop F2 (r2, clk, in2);

   // d1 is depositable but d2 is undriven. We hope to make a pull
   // driver down below
   wire   d1; // carbon depositSignal
   wire   d2;
   sub1 S1_1 (out1, out2, r1, d1, clk);
   sub1 S1_2 (out3, out4, r2, d2, clk);

   // Add initials to the undriven net so that we match an event based
   // 4-state simulator.
   init I1 (d2);

   // Also add a cycle so that we redo elaborated flow
   reg    out5, out6;
   initial out5 = 0;
   initial out6 = 0;
   always @ (r1 or r2 or clk)
     if (~clk)
       begin
          out5 = (out5 & r1) | r2;
          out6 = r1 ^ r2;
       end

endmodule

module sub1 (c1, q1, i1, i2, clk);
   output c1, q1;
   input  i1, i2, clk;

   flop F1 (q1, clk, i1);
   sub2 S2 (c1, i2);

endmodule

module flop (q, clk, d);
   output q;
   input  clk, d;
   reg    q;

   initial q = 0;

   always @ (posedge clk)
     q <= d;

endmodule

module sub2 (o, i);
   output o;
   input  i;

   reg    o;
   initial o = 1;
   always @ (i)
     begin
        o = ~i;
     end

endmodule

module init(o);
   output o;
   reg    o;

`ifdef EVENTSIM
   initial begin
      o = 0;
   end
`endif

endmodule
