// last mod: Mon Jan  3 09:42:56 2005
// filename: test/cycle/cycle10.v
// Description:  This test was trimmed down from a testcase found during
// verilog generate_for test development that had a simulation mismatch.
// even without the use of the generate statement it has a mismatch due to
// incorrect calculation of cycle settling.
// This testcase must be run with the -2001 option.

// with 1.2653 this generates a simulation mismatch

module top(co, a, b, ci);
   output co;
   input [3:0] a;
   input [3:0] b;
   input       ci;

   // Local nets (2)
   wire        temp1, temp2, temp3;
   wire [3:0]  t[1:3];

   // Continuous Assigns
   assign      t[3][0] = 1'b1;             //  required
   assign      temp1 =  t[3][0];           //  required
   assign      t[1][1] = 1'b0;
   assign      t[2][1] = 1'b1;
   assign      t[3][1] = (t[1][1] & temp1);  //  required, but this evals to 0
   assign      temp2 = (t[2][1] | t[3][1]);  //  required, but this evals to 1
   assign      t[1][2] = (a[2] ^ b[2]);      //  required
   assign      t[3][2] = (t[1][2] & temp2);
   assign      temp3 =  t[3][2];           //  required
   assign      t[3][3] =  temp3;           //  required but t[3][3] not used
   assign      co =  temp3;
endmodule
