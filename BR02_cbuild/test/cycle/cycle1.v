module top (out, clk, in);
   output out;
   input  clk, in;

   reg 	  tmp1;
   reg 	  tmp2;
   always @ (in or tmp2)
     tmp1 = in & tmp2;

   always @ (in or tmp1)
     tmp2 = in | tmp1;

   reg 	  out;
   always @ (posedge clk)
     out = tmp2;

endmodule // top
