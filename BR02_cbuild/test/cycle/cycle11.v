module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   reg    r1, r2;
   initial begin
      r1 = 0;
      r2 = 0;
   end
   always @ (posedge clk)
     begin
        r1 <= in1;
        r2 <= in2;
     end

   reg a, b, d, dummy;
   initial begin
      a = 0;
      b = 1;
      d = 0;
      dummy = 0;
   end
   wire c;

   task one;
      begin
         b = ~a;
         dummy = d;
         if (r1 && !r2)
           d = dummy ^ c;
      end
   endtask

   task two;
      begin
         dummy = d;
         if (r2 && !r1)
           d = dummy ^ c;
      end
   endtask

   always @ (a or r1 or c)
     begin
        one;
     end

   always @ (r2 or c)
     begin
        two;
     end

   assign c = ~b;

   reg    out1, out2;
   initial begin
      out1 = 0;
      out2 = 0;
   end
   always @ (posedge clk)
     begin
        out1 <= d;
        out2 <= dummy;
     end

endmodule
