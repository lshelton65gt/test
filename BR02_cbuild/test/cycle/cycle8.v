module top(RX_CTL_DET, RX_SYNC_ARM, rx_ctlcad_uni, PWRDWN);
  output RX_CTL_DET;
  input  RX_SYNC_ARM, PWRDWN;
  input [8:0] rx_ctlcad_uni;
  
  assign      RX_CTL_DET = ((RX_SYNC_ARM ? 1'b0 :
                             (rx_ctlcad_uni[8] | RX_CTL_DET)) &
                            (~PWRDWN));
endmodule // top
