// compile with -noFlatten
// there is no cycle here, unless the ports are pessimistic
// a[1] and b[1] should be dead bits and bar is entirely dead
module top(foo,blah);
input foo;
output blah;

  wire bar;
  wire [1:0] a;
  wire [1:0] b;

  my_buf buf1 (b[0], foo);
  my_buf buf2 (b[1], bar);
  my_buf buf3 (a[0], b[0]);
  my_buf buf4 (a[1], b[1]);
  my_buf buf5 (blah, a[0]);
  my_buf buf6 (bar,  blah);

endmodule

module my_buf(out, in);
output out;
input in;

  assign out = in;
  
endmodule
