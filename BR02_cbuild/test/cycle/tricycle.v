// Combinational Cycle through tristate drivers

module tricycle(out, i1, i2, e1, e2);
  output out;
  input  i1, i2, e1, e2;
  reg    keep_val;

  wire   e2_safe = e2 & ~e1;
  assign out = e1? i1: 1'bz;
  assign out = e2_safe? i2: 1'bz;
  pulldown(out);
  nor(keep_ena, e1, e2);
  assign out = keep_ena? keep_val: 1'bz;

  initial keep_val = 0;
  always @(out)
    #1 keep_val = out;
endmodule // tricycle


