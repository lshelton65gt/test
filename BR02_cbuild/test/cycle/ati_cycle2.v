module top (out, in1, clk);
  input clk;
  output [6:0] out;
  input [32:0] in1;
  reg [6:0]    out;
  
  wire [6:0]   net;
  
  wire [33:0]  n6 = in1[32:0]<<0;
  assign       net[6] = in1[0];
  wire [33:0]  n5 = n6<<(64&(net[6]));
  assign       net[5] = n5[1];
  wire [33:0]  n4 = n5<<(32&(net[5]));
  assign       net[4] = n4[17];
  wire [33:0]  n3 = n4<<(16&(net[4]));
  assign       net[3] = n3[25];
  wire [33:0]  n2 = n3<<(8&(net[3]));
  assign       net[2] = n2[29];
  wire [33:0]  n1 = n2<<(4&(net[2]));
  assign       net[1] = n1[31];
  wire [33:0]  n0 = n1<<(2&(net[1]));
  assign       net[0] = n0[32];
  
  
  initial
    out = 0;
  always @(posedge clk)
    out <= ~net;
endmodule // top
