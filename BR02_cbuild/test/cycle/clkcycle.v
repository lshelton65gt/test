module top (
    hclk,
    test_en,
    i_rst_,
    pllclk0_reset,
    cmd_vld,
    rst_cmd,
    rc165_sync,
    clk165,
    clk220
  );
  input hclk,test_en,i_rst_,pllclk0_reset,cmd_vld,rst_cmd,clk165,clk220;
  output rc165_sync;

  wire reset__int;
  wire anded_resets;
  reg [7:0] pll_count_time;
  reg [7:0] sys_count_time;
  reg [13:0] clkdiv_count_time;
  reg sys_reset__int;
  wire soft_reset_;

  assign anded_resets = i_rst_ & soft_reset_;
  wire sys_count_clk = sys_count_time[2];

  always @ (posedge pllclk0_reset or negedge anded_resets)
  begin
    if (~anded_resets)
      sys_count_time <= 8'h00; 
    else
    begin
      if (~sys_count_time[2])
	sys_count_time <= (sys_count_time + 1'b1);
    end
  end

  always @(posedge sys_count_clk or negedge anded_resets)
  begin
    if (~anded_resets)
      sys_reset__int = 1'b0;
    else
      sys_reset__int = 1'b1;
  end

  reset_nedge_sync reset_clk165_sync (.i_clk(clk165), 
    .i_reset_(sys_reset__int), 
    .i_test_en(test_en), 
    .o_reset_(rc165_sync));
  reset_nedge_sync rc220_sync (.i_clk(clk220), 
    .i_reset_(sys_reset__int), 
    .i_test_en(test_en), 
    .o_reset_(rc220_synced));


reset_sync hrst_gen (.i_clk(hclk), .i_reset_(rc220_synced), .i_test_en(test_en), .o_reset_(hrst_b));

 reg sw_rst_s0;
 reg sw_rst_s1;
 parameter TP = 1;
 reg soft_reset__reg;
 always @(posedge hclk or negedge hrst_b)
 if (~hrst_b) soft_reset__reg <= #TP 1'b1;
 else if (cmd_vld & rst_cmd) soft_reset__reg <= #TP 1'b0;
 always @(posedge hclk or negedge soft_reset__reg)
 if (~soft_reset__reg) sw_rst_s0 <= #TP 1'b0;
 else sw_rst_s0 <= #TP 1'b1;
 always @(posedge hclk or negedge soft_reset__reg)
 if (~soft_reset__reg) sw_rst_s1 <= #TP 1'b0;
 else sw_rst_s1 <= #TP sw_rst_s0;
 assign soft_reset_ = sw_rst_s1;

endmodule

module DFFNX1 (Q, QN, D, CKN);
output Q, QN;
input D, CKN;
reg Q,QN;
always @(negedge CKN)
begin
  Q <= D;
  QN <= ~D;
end
endmodule 
module DLY2X1 (A, Y);
 input A;
 output Y;
 assign Y = A;
endmodule 
module reset_nedge_sync (i_clk, i_reset_, i_test_en, o_reset_);
 input i_clk; 
 input i_reset_;
 input i_test_en;
 output o_reset_;
 wire n0, n1, reset_;
DFFNX1 sync0 (.CKN(i_clk), .D(i_reset_), .Q(n0), .QN(dummy1));
DLY2X1 del0 (.A(n0), .Y(n1));
DFFNX1 sync1 (.CKN(i_clk), .D(n1), .Q(reset_), .QN(dummy2));
//AND2X4 reset_synced (.A(i_reset_), .B(reset_), .Y(reset_synced_));
//OR2X4 reset_masked (.A(reset_synced_), .B(i_test_en), .Y(reset_masked_)); 
//BUFX12 reset_buf (.A(reset_masked_), .Y(o_reset_));

wire reset_synced_ = i_reset_ & reset_;
assign o_reset_ = reset_synced_ | i_test_en;
endmodule 
module reset_sync (i_clk, i_reset_, i_test_en, o_reset_);
 input i_clk; 
 input i_reset_;
 input i_test_en;
 output o_reset_;
 wire n0, n1, reset_;
DFFHQX4 sync0 (.CK(i_clk), .D(i_reset_), .Q(n0));
DLY2X1 del0 (.A(n0), .Y(n1));
DFFHQX4 sync1 (.CK(i_clk), .D(n1), .Q(reset_));
AND2X4 reset_synced (.A(i_reset_), .B(reset_), .Y(reset_synced_));
OR2X4 reset_masked (.A(reset_synced_), .B(i_test_en), .Y(reset_masked_)); 
BUFX12 reset_buf (.A(reset_masked_), .Y(o_reset_));
endmodule 
module AND2X4 (A, B, Y);
 input A, B;
 output Y;
 assign Y = A & B;
endmodule 
module BUFX12 (A, Y);
 input A;
 output Y;
 assign Y = A;
endmodule 
module DFFHQX4 (Q, D, CK);
 input CK;
 input D;
 output Q;
reg Q;
always @ (posedge CK) Q <= #0.5 D;
endmodule 
module OR2X4 (Y, A, B);
input A;
input B;
output Y;
assign Y = A | B;
endmodule
