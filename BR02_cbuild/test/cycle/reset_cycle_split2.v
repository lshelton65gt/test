module bug(clk,in,out,rst);
  input clk,in,rst;
  output out;
  reg q1,q1p,q2,q2p,q3,q3p;

  wire anded_rst = q3 & rst;

  always @(posedge clk)
    q1p <= in & anded_rst;
  always @(anded_rst or q1p)
    if (q1p)
      q1 = anded_rst;
    else
      q1 = 1'b0;

  always @(posedge clk)
    q2p <= ~q1;
  always @(q1 or q2p)
    if (q2p)
      q2 = 1'b1;
    else
      q2 = ~q1;

  always @(posedge clk)
    q3p <= q2;
  always @(q2 or q3p)
    if (q3p)
      q3 = q2;
    else
      q3 = 1'b0;

  assign out = q3;

endmodule
