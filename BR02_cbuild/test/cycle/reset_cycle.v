module bug(clk,in,out,rst);
  input clk,in,rst;
  output out;
  reg q1,q2,q3;

  wire anded_rst = q3 & rst;

  always @(posedge clk or negedge anded_rst)
    if (~anded_rst)
      q1 <= 0;
    else 
      q1 <= in;

  always @(posedge clk or negedge q1)
    if (~q1)
      q2 <= 1;
    else
      q2 <= 0;

  always @(posedge clk or negedge q2)
    if (~q2)
      q3 <= 0;
    else
      q3 <= q2;

  assign out = q3;

endmodule
