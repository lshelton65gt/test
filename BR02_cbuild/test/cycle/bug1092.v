module top(clk,a,b,out);
input clk,a,b;
output out;

wire clk, a, b;

reg [1:0] vec;
reg c, d, out;

always
  begin
    vec[0] = a;
    vec[1] = b;
    c = vec[0];
    d = vec[1];
  end

assign b = c;

always @(posedge clk)
  out = d;

endmodule
