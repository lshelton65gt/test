// This test case comes from Sun
module top(out, in);
   output [3:0] out;
   input [2:0]  in;

   assign       out[0] = 1'b0;
   assign       out[3:1] = out[2:0] | in[2:0];

endmodule // top
