// Test of false loop when port aggregation occurs.
// Must run with -noFlatten.
module top(sel, d, o, o1);
   input [1:0] sel;
   input d;
   output [1:0] o;
   output [1:0] o1;

   assign       o[0] = sel[0] ? o1[0] : 'bz;
   assign       o[1] = sel[1] ? o1[1] : 'bz;

   middle0 middle0 (o1, d);
   middle1 middle1 (o1, o);
endmodule

module middle0(o, i);
   output [1:0] o;
   input   i;

   assign       o[0] = i;
endmodule

module middle1(o, i);
   output [1:0] o;
   input [1:0]  i;

   assign       o[1] = i[0];
endmodule
