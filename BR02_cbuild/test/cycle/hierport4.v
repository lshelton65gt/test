// Test of block merging being able to handle
// dead bits of a net.
module top(i1, i2, clk1, clk2, rst, o1, o2);
   input [1:0] i1, i2;
   input       clk1, clk2, rst;
   output [1:0] o1, o2;
   reg [1:0]    o1, o2;
   wire [1:0]   r;
   wire [1:0]   s;
   wire         p, q;
   
   bottom bottom1 (i1, i2, clk1, rst, r, p);
   bottom bottom2 (i1, i2, clk1, rst, s, q);
   
   always @(posedge clk2)
     o1[1] = r[1] & p & q;
   always @(posedge clk2)
     o2[0] = s[0];
   always @(posedge clk2)
     o2[1] = s[1];
endmodule


module bottom(i1, i2, clk, rst, o, o2);
   input [1:0] i1, i2;
   output [1:0] o;
   input        clk, rst;
   reg [1:0]    o;
   output       o2;
   reg          o2;

   always @(posedge clk or posedge rst)
     begin
     if (rst)
       begin
          o[0] = 1'b1;
          o2 = 1'b0;
       end
     else
       begin
          o[0] = i1[0] & i2[1];
          o2 =i1[0];
       end
     end
   
   always @(posedge clk or posedge rst)
     if (rst)
       o[1] = 1'b0;
     else
       o[1] = i1[1] & i2[0];

endmodule
