module cycle(in, out);
input in;
output out;
 
wire [1:0] x;
wire [1:0] y;
 
assign x[0] = in;
assign x[1] = y[1];
assign out = y[0];
 
sub S1 (x,y);
 
endmodule
 
module sub(a, b);
input a;
output b;
 
wire [1:0] a;
reg [1:0] b;

always @(a)  
begin
  b[0] = ~a[1];
  b[1] = ~a[0];
end

endmodule
