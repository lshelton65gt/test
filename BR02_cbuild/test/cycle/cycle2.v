module top (out, clk, in);
   output out;
   input  clk, in;

   reg 	  tmp1;
   initial tmp1 = 0;

   always @ (in or tmp1)
     tmp1 = in & tmp1;

   reg 	  out;
   initial out = 0;
   always @ (posedge clk)
     out = tmp1;

endmodule // top
