// Test of dead drivers through port elaboration.
// Must run with -noFlatten.
module top(o, a, b, c, d);
   output  o;
   input   a, b, c, d;

   wire [1:0]   w;

   assign       w[0] = a & b;
   assign       w[1] = c & d;

   middle middle (o, w);
endmodule


module middle (o, i);
   output o;
   input [1:0] i;
   assign      o = i[1];
endmodule
