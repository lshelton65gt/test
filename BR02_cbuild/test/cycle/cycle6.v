// This is from bug 132

//The following code produces a false asynchronous loop through the casez
//statement.  Logically the case output t5 is not dependent, logically, on
//the input t4

module cycle6(t1, t2, t3, clk, out);
   input t1, t2, t3, clk;
   output out;
   reg 	  t4, t5, out;
   
   always @(t1 or t2 or t3 or t4)
     begin
	casez ({t1,t2,t3,t4})
	  4'b1???: t5 = 0;
	  4'b01??: t5 = 0;
	  4'b0000: t5 = 1;
	  4'b0001: t5 = 1;
	  4'b001?: t5 = 1;
	endcase // casez({t1,t2,t3,t4})
     end
   
   always @(t5)
     begin
	t4 = t5;
     end

   always @(posedge clk)
     begin
	out <= t5;
     end

endmodule
	      