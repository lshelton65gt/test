module cycle4(in, out);
  input in;
  output out;
  wire [3:0] shift;

  assign out = shift[0];
  assign shift[0] = ~shift[1];
  assign shift[1] = ~shift[2];
  assign shift[2] = ~shift[3];
  assign shift[3] = ~in;
endmodule // cycle3
