module top(en, in, out);
input en;
input in;
output out;

wire en;
wire [7:0] in;
wire [7:0] out;
wire [7:0] a;

assign a = en ? out : in;
assign out = a + en;

endmodule
