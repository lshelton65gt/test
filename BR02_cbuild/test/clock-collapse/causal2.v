module clkbuf(in, out, clk, iclk);
  input in, clk, iclk;
  output out;

  reg    q1, q2, q3, q4, out, clk1;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
    out = 0;
  end

  always @(posedge iclk)
    clk1 <= clk;

  wire    clk2, clk_;

  assign clk_ = ~clk1;
  assign clk2 = ~clk_;

  always @(posedge clk)
    q1 <= in;

  always @(posedge clk_)
    q2 <= q1;

  always @(negedge clk2)
    q3 <= q2;

  always @(negedge clk_)
    q4 <= q3;

  always @(posedge clk2)
    out <= q4;
endmodule // clkbuf
