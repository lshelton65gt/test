module top (out1, out2, clk, d, en);
   output out1, out2;
   input  clk;
   input [3:0] d;
   input [7:0] en;

   wire         null1, null2;
   sub S1 (out1, out2, clk, d[1:0], en[7:0]);
   sub S2 (null1, null2, clk, d[3:2], en[7:0]);

endmodule


module sub (out1, out2, clk, d, en);
   output out1, out2;
   input  clk;
   input [1:0] d;
   input [8:1] en;

   wire   gclk1, gclk2, gclk3, gclk4, gclk5, gclk6, gclk7, gclk8;
   wire   gclk9, gclk10, gclk11, gclk12, gclk13, gclk14, gclk15, gclk16;
   gatedClk C1 (gclk1, clk, en[1]);
   gatedClk C2 (gclk2, gclk1, en[2]);
   gatedClk C3 (gclk3, gclk2, en[3]);
   gatedClk C4 (gclk4, gclk3, en[4]);
   gatedClk C5 (gclk5, gclk4, en[5]);
   gatedClk C6 (gclk6, gclk5, en[6]);
   gatedClk C7 (gclk7, gclk6, en[7]);
   gatedClk C8 (gclk8, gclk7, en[8]);
   gatedClk C9 (gclk9, gclk8, ~en[1]);
   gatedClk C10 (gclk10, gclk9, ~en[2]);
   gatedClk C11 (gclk11, gclk10, ~en[3]);
   gatedClk C12 (gclk12, gclk11, ~en[4]);
   gatedClk C13 (gclk13, gclk12, ~en[5]);
   gatedClk C14 (gclk14, gclk13, ~en[6]);
   gatedClk C15 (gclk15, gclk14, ~en[7]);
   gatedClk C16 (gclk16, gclk15, ~en[8]);

   reg    out1, out2;
   initial out1 = 0;
   initial out2 = 0;
   always @ (posedge gclk16)
     out1 <= d[0];
   always @ (posedge gclk15)
     out2 <= d[1];

endmodule

module gatedClk(gclk, clk, eni);
   output gclk;
   input  clk, eni;

   reg    eno;
   initial eno = 0;
   always @ (clk or eni)
     if (clk)
       eno = eni;
   assign gclk = ~clk & eno;

endmodule
