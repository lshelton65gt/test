// In this test, clk1 and clk2 are derived from a common input
// through a construct that clock equivalence cannot handle.
// We prefer this to making two separate inputs so that Verilog XL
// gets the same answer.

module clkdiv(in, in2, in3, out, dclk1_, dclk2_, clks);
  input in, in2, in3;
  output out, dclk1_, dclk2_;
  input [1:0] clks;

  reg    q1, q2, q3, q4, out, out2, out3;
  reg    dclk1, dclk2;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
    out = 0;
    dclk1 = 0;
    dclk2 = 0;
    out2 = 0;
    out3 = 0;
  end

  // We're going to collapse clk1i and clk2i together, so make
  // sure Verilog-XL gives the same answers as we do
  wire clk1 = clks[0];
`ifdef VERILOG_XL
  wire clk2 = clks[0];            // ignore clks[1];
`else
  wire clk2 = clks[1];            // rely on collapseClock
`endif
  assign dclk1_ = ~dclk1;
  assign dclk2_ = ~dclk2;

  always @(posedge clk1)
    dclk1 <= dclk1_;

  always @(posedge clk2)
    dclk2 <= dclk2_;

  always @(posedge dclk1)
    q1 <= in;

  always @(posedge dclk2)
    q2 <= q1;

  always @(posedge dclk1)
    q3 <= q2;

  always @(posedge dclk2)
    q4 <= q3;

  always @(posedge dclk1)
    out <= q4;
endmodule // clkdiv

