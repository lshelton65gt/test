module top(clk1, in, out);
  input clk1;
  input [4:0] in;
  output [4:0] out;
  reg [4:0] out;
  reg       clk2, clk3;        // deposit directive, no driver
  reg       clk4, clk5;        // deposit directive, ignored equivalent drivers

  always @(posedge clk1)
    out[0] <= in[0];
  
  always @(posedge clk2)
    out[1] <= in[1];
  
  always @(posedge clk3)
    out[2] <= in[2];

  always @(posedge clk4)
    out[3] <= in[3];
  
  always @(posedge clk5)
    out[4] <= in[4];

  // clk3 & clk4 are equivalent as far as the design logic is concerned,
 // but we should respect deposit directive and not mark them as such.
  initial begin
    out = 0;
    clk4 = 0;
    clk5 = 0;
  end

  always @(posedge clk1)
    clk4 <= ~clk4;

  always @(posedge clk1)
    clk5 <= ~clk5;
endmodule
 
  
  