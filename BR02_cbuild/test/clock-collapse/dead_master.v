module top(clks, i1, i2, q1, q2);
  input [1:0] clks;
  input i1, i2;
  output q1, q2;

  reg    q1, q2;
  initial begin
    q1 = 0;
    q2 = 0;
  end

  wire clk1, clk2;

  always @(posedge clk1)
    q1 <= i1;
  always @(posedge clk2)
    q2 <= i2;
endmodule
