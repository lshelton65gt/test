// last mod: Wed Dec 28 17:44:28 2005
// filename: test/clock-collapse/bug5486_2.v
// Description:  This test is an attempt to create the NU_ASSERT found in bug 5486.
// here we have a loop of divided clocks that are collapse_clocked together
// this test is nonsense, but it gets a NU_ASSERT
// cbuild -vlogTop bug5486_2 -noFlatten  -2001 -dumpClockAliases -verboseClockAliases -q -directive bug5486_2.directives  -nocc  bug5486_2.v

module bug5486_2(in1, in2, out, reset1, reset2, clk1, clk2);
   input in1, in2, clk1, clk2, reset1, reset2;
   output out;

   wire   dc_1, dc_2;
   wire   dv_c_1, dv_c_2, dv_c_3;
   wire   out1, out2, out3;

   assign out = out1 ^ out2 ^ out3;

   divided_clock u1(out3, dv_c_3);
   divided_clock u2(dv_c_3, dv_c_2);
   divided_clock u3(dv_c_2, dv_c_1);
   divided_clock u4(dv_c_1, out3);

   clock_user_1 u5 (dv_c_1, in1, in2, out1);
   clock_user_1 u6 (dv_c_3, in1, in2, out2);
endmodule

module divided_clock(in_clk, out_clk);
   input in_clk;
   output out_clk;

   reg 	  out_clk;
   initial
     out_clk = 0;

   always @(posedge in_clk)
     out_clk = ~out_clk;
endmodule

module clock_user_1(clk, in1, in2, out);
   input clk, in1, in2;
   output out;

   reg 	  out;
   initial
     out = 0;

   always @(posedge clk)
      begin
	 out = in1 ^ !in2;
      end
endmodule
