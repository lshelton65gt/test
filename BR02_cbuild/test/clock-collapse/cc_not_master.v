// In this testcase, we have collapsed clk1 and clk2, but neither
// of those should be labelled the master, because clk is upstream
// of those.  Less logic will need to run if we choose 'clk' as
// the master, and 'clk1' and 'clk2' will be Aliased.  Whereas if
// we choose the collapse-master, clk1, then 'clk' will need to
// be marked Equiv, because it is needed to compute clk1.

module top (out1, out2, out3, clk, d1, d2, d3);
   output out1, out2, out3;
   input  clk, d1, d2, d3;

   wire   clk0, clk1, clk2;
   assign clk0 = ~clk;
   cnot N1 (clk1, clk0);
   cnot N2 (clk2, clk0);

   // Use all the clocks that can be collapsed
   reg    out1, out2, out3;
   always @ (posedge clk)
     out1 <= d1;
   always @ (posedge clk1)
     out2 <= d2;
   always @ (posedge clk2)
     out3 <= d3;

endmodule

module cnot (o, i);
   output o;
   input  i;

   assign o = ~i;

endmodule
