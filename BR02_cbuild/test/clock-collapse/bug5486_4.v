// last mod: Wed Dec 28 17:44:28 2005
// filename: test/clock-collapse/bug5486_4.v
// Description:  Modified version of bug5486_3.v, where flattening
//               reveals a local optimization that eliminates the cycle
// cbuild -vlogTop bug5486_3 -noFlatten  -2001 -dumpClockAliases -verboseClockAliases -q -directive bug5486_2.directives  -nocc  bug5486_2.v

module top(input clk, input in1, input in2, output out1, output out2);
  wire   clk1;                  // carbon collapseClock top.clk2
  wire   clk2;
  divided_clock u1(clk, clk1, 1'b1, clk2);
  divided_clock u2(clk, clk2, 1'b0, clk1); // cycle!

  clock_user_1 u5 (clk1, in1, out1);
  clock_user_1 u6 (clk2, in2, out2);
endmodule

module divided_clock(input clk1, input clk2, input sel, output reg out_clk);
  wire clk = sel ? clk1 : clk2; // simplifies if sel is a constant
  always @(posedge clk)
     out_clk = ~out_clk;
endmodule

module clock_user_1(input clk, input in1, output reg out1);
  always @(posedge clk)
    out1 <= ~in1;
endmodule
