module top(clks, i1, i2, q1, q2);
  input [1:0] clks;
  input i1, i2;
  output q1, q2;

  reg    q1, q2, clk1, clkb;
  initial begin
    q1 = 1;
    q2 = 0;
    clk1 = 0;
    clkb = 0;
  end

  always @(clks) begin
    clk1 = clks[0];
    #1 clkb = ~clk1;
  end

`ifdef VERILOG_XL
  wire clk2 = clks[0];            // ignore clks[1];
`else
  wire clk2; // dead master -- guard against assert
`endif

  // clk2 is collapsed into clk1, which will be the master.

  // clka will be the master, but it's derived off clk2, so
  // clk1 will be a dead master with a live alias
  buf #(1)(clka, ~clk2);

  always @(posedge clk2 or posedge clka)
    if (clka)
      q1 <= 0;
    else
      q1 <= i1;
  always @(posedge clk2 or posedge clkb)
    if (clkb)
      q2 <= 0;
    else
      q2 <= i2;
endmodule
