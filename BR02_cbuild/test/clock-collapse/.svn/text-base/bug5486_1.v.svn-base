// last mod: Wed Dec 28 16:04:35 2005
// filename: test/clock-collapse/bug5486_1.v
// Description:  This test is an attempt to create the NU_ASSERT found in bug 5486.
// here we have derived clocks that are collapse_clocked together
// currently this does not produce an assert.


module bug5486_1(in1, in2, out, reset1, reset2, clk1, clk2);
   input in1, in2, clk1, clk2, reset1, reset2;
   output out;

   wire   dc_1, dc_2;
   wire   dv_c_1, dv_c_2;
   wire   out1, out2, out3;

`ifndef CARBON
       // we will be using collapse clock to force this
   assign in2 = in1;
`endif
   
   assign out = out1 | out2 | out3;

   derived_clock1 u1(clk1, reset1, reset2, dc_1);
   derived_clock2 u2(clk2, reset1, reset2, dc_2);

   clock_user_1 u3(dv_c_1, in1, in2, out1);
   clock_user_2 u4(dv_c_2, in2, in1, out2);
   clock_user_2 u7(dv_c_2, dv_c_1, in1, out3);

   divided_clock u5(out3 /* dc_1 */, dv_c_1);
   divided_clock u6(dc_2, dv_c_2);
   
endmodule

module divided_clock(in_clk, out_clk);
   input in_clk;
   output out_clk;

   reg 	  out_clk;
   initial
     out_clk = 0;

   always @(posedge in_clk)
     out_clk = ~out_clk;
endmodule

module derived_clock1(in_clk, reset1, reset2, out_clk);
   input in_clk;
   input reset1, reset2;
   output out_clk;

   reg 	  out_clk;

   initial
     out_clk = 0;
   
   always @(posedge in_clk)
     begin
	out_clk = ~(reset1 | reset2);
     end
endmodule


    // module derived_clock2 is functionally equivalent to derived_clock1 but
    // has many excess assignments to make it flatten differently
module derived_clock2(in_clk, reset1, reset2, out_clk);
   input in_clk;
   input reset1, reset2;
   output out_clk;

   wire   internal_clock, internal_reset1, internal_reset2;

   not u0A(out_clk, internal_clock);
   not u0B(internal_reset1, reset1);
   not u0C(internal_reset2, reset2);

   derived_clock2_level_1 u1 (in_clk, internal_reset1, internal_reset2, internal_clock);
endmodule

module derived_clock2_level_1(in_clk, reset1, reset2, out_clk);
   input in_clk;
   input reset1, reset2;
   output out_clk;

   wire   internal_clock, internal_reset1, internal_reset2;
   not u0A(out_clk, internal_clock);
   not u0B(internal_reset1, reset1);
   not u0C(internal_reset2, reset2);

   derived_clock2_level_2 u1 (in_clk, internal_reset1, internal_reset2, internal_clock);
endmodule

module derived_clock2_level_2(in_clk, reset1, reset2, out_clk);
   input in_clk;
   input reset1, reset2;
   output out_clk;

   wire   internal_clock, internal_reset1, internal_reset2;
   not u0A(out_clk, internal_clock);
   not u0B(internal_reset1, reset1);
   not u0C(internal_reset2, reset2);

   derived_clock2_level_3 u1 (in_clk, internal_reset1, internal_reset2, internal_clock);

endmodule

module derived_clock2_level_3(in_clk, reset1, reset2, out_clk);
   input in_clk;
   input reset1, reset2;
   output out_clk;

   wire   internal_clock, internal_reset1, internal_reset2;
   not u0A(out_clk, internal_clock);
   not u0B(internal_reset1, reset1);
   not u0C(internal_reset2, reset2);

   derived_clock2_level_4 u1 (in_clk, internal_reset1, internal_reset2, internal_clock);

endmodule

module derived_clock2_level_4(in_clk, reset1, reset2, out_clk);
   input in_clk;
   input reset1, reset2;
   output out_clk;

   reg 	  out_clk;

   initial
     out_clk = 0;

   always @(posedge in_clk)
     begin
	out_clk = ~(reset1 | reset2);
     end
endmodule

module clock_user_1(clk, in1, in2, out);
   input clk, in1, in2;
   output out;

   reg 	  out;
   initial
     out = 0;

   always @(posedge clk)
      begin
	 out = in1 ^ !in2;
      end
endmodule

module clock_user_2(clk, in1, in2, out);
   input clk, in1, in2;
   output out;

   reg 	  out;
   initial
     out = 0;

   always @(posedge clk)
      begin
	 out = in1 ^ !in2;
      end
endmodule
