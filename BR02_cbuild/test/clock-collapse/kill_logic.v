module clkdiv(in, in2, in3, out, dclk1_, dclk2_, clk1, clk2);
  input in, in2, in3, clk1, clk2;
  output out, dclk1_, dclk2_;

  reg    q1, q2, q3, q4, out, out2, out3;
  reg    dclk1, dclk2;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
    out = 0;
    dclk1 = 0;
    dclk2 = 0;
    out2 = 0;
    out3 = 0;
  end

  assign dclk1_ = ~dclk1;
  assign dclk2_ = ~dclk2;

  always @(posedge clk1)
    dclk1 <= dclk1_;

  always @(posedge clk2)
    dclk2 <= dclk2_;

  always @(posedge dclk1)
    q1 <= in;

  always @(posedge dclk2)
    q2 <= q1;

  always @(posedge dclk1)
    q3 <= q2;

  always @(posedge dclk2)
    q4 <= q3;

  always @(posedge dclk1)
    out <= q4;
endmodule // clkdiv

