// last mod: Wed Dec 28 17:44:28 2005
// filename: test/clock-collapse/bug5486_3.v
// Description:  Simplified version of bug5486_2.v
// cbuild -vlogTop bug5486_3 -noFlatten  -2001 -dumpClockAliases -verboseClockAliases -q -directive bug5486_2.directives  -nocc  bug5486_2.v

module top(input clk, input in1, input in2, output out1, output out2);
  wire   clk1;                  // carbon collapseClock top.clk2
  wire   clk2;
  wire   clk3, clk4;
  divided_clock u1(clk1, clk2);
  divided_clock u4(clk2, clk1); // cycle!

  clock_user_1 u5 (clk1, in1, out1);
  clock_user_1 u6 (clk2, in2, out2);
endmodule

module divided_clock(input in_clk, output reg out_clk);
   always @(posedge in_clk)
     out_clk = ~out_clk;
endmodule

module clock_user_1(input clk, input in1, output reg out1);
  always @(posedge clk)
    out1 <= ~in1;
endmodule
