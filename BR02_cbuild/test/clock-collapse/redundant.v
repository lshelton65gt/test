module top(in1, in2, in3, in4, clks,
           out1, out2, out3, out4);
  input in1, in2, in3, in4;
  input [4:1] clks;
  output out1, out2, out3, out4;
  reg    out1, out2, out3, out4;

  initial begin
    out1 = 0;
    out2 = 0;
    out3 = 0;
    out4 = 0;
  end

  // We're going to collapse everything to clk3, so make
  // sure Verilog-XL gives the same answers as we do
`ifdef VERILOG_XL
  wire clk1 = clks[3];            // ignore clks[1];
  wire clk2 = clks[3];            // ignore clks[2];
  wire clk3 = clks[3];
  wire clk4 = clks[3];            // ignore clks[4];
`else
  wire clk1 = clks[1];            // rely on collapseClock
  wire clk2 = clks[2];            // rely on collapseClock
  wire clk3 = clks[3];
  wire clk4 = clks[4];            // rely on collapseClock
`endif
  
  always @(posedge clk1)
    out1 <= in1;
  always @(posedge clk2)
    out2 <= in2;
  always @(posedge clk3)
    out3 <= in3;
  always @(posedge clk4)
    out4 <= in4;
endmodule

  