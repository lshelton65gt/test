module bug (in, iclk, out);
   input [3:0] in;
   input [1:0] iclk;
   output [3:0] out;

   wire   [1:0] clk1, clk2;

   wire   en;

  assign  clk1[0] = iclk[0];
`ifdef VERILOG_XL
  assign  clk1[1] = iclk[0];  // manually do collapse clocks equiv
`else
  assign  clk1[1] = iclk[1];  // manually do collapse clocks equiv
`endif
  
   assign en = 1'b1;
   assign clk2 = clk1;
   
   
   flop flop1 (in[0], clk1[0], out[0], en);
   flop flop2 (in[1], clk1[1], out[1], en);
   flop flop3 (in[2], clk2[0], out[2], en);
   flop flop4 (in[3], clk2[1], out[3], en);

endmodule // bug

module flop (in, clk, out, en);
   input in, clk, en;
   output out;
   reg 	  out;

   wire   clki = clk & en;
   initial out = 1'b0;
   
   always @(posedge clki)
     out <= in;

endmodule // flop
