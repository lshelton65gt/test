module clkbuf(in1, out1, in2, out2, in3, out3, clks);
  input [1:0] clks;
  input in1, in2, in3;
  output out1, out2, out3;

  reg    out1, out2, out3;

  initial begin
    out1 = 0;
    out2 = 0;
    out3 = 0;
  end

  wire clk1 = clks[0];
`ifdef VERILOG_XL
  wire clk2 = clks[0];            // ignore clks[1];
`else
  wire clk2 = clks[1];            // rely on collapseClock
`endif

  always @(posedge clk1)
    out1 <= in1;
  always @(posedge clk2)
    out2 <= in2;
  buf(clk3, clk2);
  always @(posedge clk3)
    out3 <= in3;
endmodule

