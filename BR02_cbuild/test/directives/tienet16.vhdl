library ieee;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
  
  port ( pin1  : in  bit;
         pin2  : in  std_logic;
         pin4  : in  std_logic_vector(0 to 7);
         pin5  : in  signed(0 to 7);
         pin6  : in  unsigned(0 to 7);
         pout1 : out bit;
         pout2 : out std_logic;
         pout4 : out std_logic_vector(0 to 7);
         pout5 : out signed(0 to 7);
         pout6 : out unsigned(0 to 7) );

end top;

architecture toparch of top is

begin  -- toparch

  pout1 <= pin1;
  pout2 <= pin2;
  pout4 <= pin4;
  pout5 <= pin5;
  pout6 <= pin6;

end toparch;
