library ieee;
use ieee.std_logic_1164.all;

package defs is

  subtype MyArray is std_logic_vector(0 to 7);
  type MyArrayOfSLV is array (natural range <>) of MyArray;

end defs;

library ieee;
use ieee.std_logic_1164.all;
use work.defs.all;

entity top is
  
  port ( pin  : in  MyArrayOfSLV(0 to 7);
         pout : out MyArrayOfSLV(0 to 7) );

end top;

architecture toparch of top is

begin  -- toparch

  pout <= pin;

end toparch;
