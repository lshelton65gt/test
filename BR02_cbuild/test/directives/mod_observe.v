module bug(clk, in1, in2, out1, out2);
   input clk, in1, in2;
   output out1, out2;

   sub sub1(.clk(clk), .in(in1), .out(out1));
   sub sub2(.clk(clk), .in(in2), .out(out2));
endmodule


module sub(clk, in, out);
   input clk, in;
   output out;
   reg 	  out;

   wire   in_n = ~in;
   
   always @(posedge clk) begin : foo
     reg tmp;
     tmp <= in_n;
     out <= tmp;
   end
endmodule


