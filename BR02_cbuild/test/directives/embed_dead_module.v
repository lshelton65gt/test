module top(ck,d,q);
   input ck, d;
   output q;
   reg    q;
   
   always @(posedge ck) q <= d;

endmodule // top

module foo (a,b);
   input a;
   output b;

   wire   c;  // carbon forceSignal
    
   assign c = a;
   assign b = c;
endmodule // foo

