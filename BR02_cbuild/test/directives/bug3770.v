module bug(clk, in, out);
   input clk, in;
   output out;

   sub sub(.clk(clk), .in(in), .out(out));
endmodule


module sub(clk, in, out);
   input clk, in;
   output out;
   reg 	  out;

   wire   in_n = ~in;
   
   always @(posedge clk) begin : foo
      out <= in_n;
   end
endmodule


