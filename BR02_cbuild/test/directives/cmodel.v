module top (out1, out2, out3, clk, in1, in2);
   output out1, out2, out3;
   input  clk, in1, in2;

   wire   c1, c2, o1, o2, o3;
   assign c1 = in1;
   assign c2 = in2;
   assign out1 = o1;
   assign out2 = o2;
   assign out3 = o3;

   sub S1 (o1, o2, o3, clk, c1, c2);

endmodule // top

module sub (out1, out2, out3, clk, a, b);
   output out1, out2, out3;
   input  clk, a, b;

   reg 	  out1;
   always @ (posedge clk)
     out1 = a & b;

   assign out2 = a;

   reg 	  r1;
   always @ (posedge clk)
     r1 = ~a;
   assign out3 = b & r1;

endmodule // sub
