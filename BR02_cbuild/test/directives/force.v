// This netlist shows the terrors of not bottom-flattening a gate
// level netlist and then forcing one of them.

module flop(q, clk, d);
  output q;
  input  clk, d;
  reg    q;

  always @(posedge clk)
    q <= d;
endmodule

module top(q1, q2, clk, d1, d2);
  output q1, q2;
  input  clk, d1, d2;

  flop u1(q1, clk, d1);
  flop u2(q2, clk, d2);
endmodule
