module top (out, clk, in, en);
   output out;
   input  clk, in;
   input [31:0] en;

   wire         c1;
   sub S1 (c1, in);

   reg    out;
   always @ (posedge clk)
     if (en)
       out <= c1;

endmodule // top

module sub (y, a);
   output y;
   input  a;

   assign y = a;

endmodule // sub
