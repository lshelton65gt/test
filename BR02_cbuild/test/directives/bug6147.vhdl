-- May 2007.
-- This file tests the 'inline' directive located both in directive file
-- and in the vhdl file. One of the 'inline' directives is in bug6147.dir
-- file and the other oneis in this file.
library ieee;
use ieee.std_logic_1164.all;

entity bug6147 is
  
  port (
    a : in  bit_vector(31 downto 0);
    b : out bit_vector(31 downto 0);
    c : out bit_vector(31 downto 0)); 
end bug6147;

architecture rtl of bug6147 is

  function invert (i : bit_vector) return bit_vector is  
    variable o : bit_vector(i'range);
  begin
    for J in i'low to i'high loop
      o(J) := not i(J);
    end loop; 
    return o;
  end function invert;

  function convert (i : bit_vector) return bit_vector is  -- carbon inline
    variable o : bit_vector(i'range);
  begin
    for J in i'low to i'high loop
      o(J) := not i(J);
    end loop; 
    return o;
  end function convert;
  
begin  -- rtl

  b <= invert(a);
  c <= convert(a);

end rtl;
