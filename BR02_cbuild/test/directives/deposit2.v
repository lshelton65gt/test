module top (out, clk, i1, i2);
   output out;
   input  clk, i1, i2;

   wire   c1, c2;
   sub S1 (c1, c2, clk, i1, i2);

   flop F1 (out, clk, c1, c2);

endmodule

module sub (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;
   reg    out1, out2;

   always @ (clk or in1)
     if (clk)
       out1 <= in1;
   always @ (clk or in2)
     if (clk)
       out2 <= in2;

endmodule

module flop (q, clk, d1, d2);
   output q;
   input  clk, d1, d2;
   reg    q;

   always @ (posedge clk)
     q <= d1 ^ d2;

endmodule
