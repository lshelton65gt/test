library ieee;
use ieee.std_logic_1164.all;

package defs is

  type week is (monday, tuesday, wednesday, thursday, friday, saturday, sunday);

end defs;

library ieee;
use ieee.std_logic_1164.all;
use work.defs.all;

entity top is
  
  port ( pin  : in  week;
         pout : out week );

end top;

architecture toparch of top is

begin  -- toparch

  pout <= pin;

end toparch;
