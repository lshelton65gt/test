module dut(i,o1,o2);
   input  i;
   output o1,o2;

   sub1 a(i,o1);
   sub1 b(i,o2);

endmodule

module sub1(i,o);
   input i;
   output o;
   reg 	  ena;
   initial ena = 1;
   assign o = (ena) ? i : 1'b0;
endmodule
