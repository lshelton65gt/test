-- May 2007.
-- This file tests the 'inline' directive in the package.
-- Inline directives in the package are currently not supported.



library ieee;
use ieee.std_logic_1164.all;

package pack is
  function convert (i : bit_vector) return bit_vector;  -- carbon inline
end pack;

package body pack is
  function convert (i : bit_vector) return bit_vector is  
    variable o : bit_vector(i'range);
  begin
    for J in i'low to i'high loop
      o(J) := not i(J);
    end loop; 
    return o;
  end function convert;
end pack;

 
library ieee;
use ieee.std_logic_1164.all;

library work;
use work.pack.all;

entity bug6147pack is
  
  port (
    a : in  bit_vector(31 downto 0);
    b : out bit_vector(31 downto 0);
    c : out bit_vector(31 downto 0)); 
end bug6147pack;

architecture rtl of bug6147pack is

  function invert (i : bit_vector) return bit_vector is  
    variable o : bit_vector(i'range);
  begin
    for J in i'low to i'high loop
      o(J) := not i(J);
    end loop; 
    return o;
  end function invert;

  
begin  -- rtl

  b <= invert(a);
  c <= convert(a);      

end rtl;
