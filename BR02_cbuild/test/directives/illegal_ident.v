`define CARBON_V2 1

module HDDFFPQ1(D, CK, Q);

	input			D;
	input			CK;
	output			Q;

	reg			notifier;
	wire			SMC_IQ;

	dff_p0 SMC_I0(
		.q				(SMC_IQ), 
		.d				(D), 
		.clk				(CK), 
		.clear				(1'b1), 
		.preset				(1'b1), 
		.notifier			(notifier));

	buf SMC_I1(Q, SMC_IQ);
endmodule

module dff_p0(q, d, clk, clear, preset, notifier);

	output			q;
	input			clk;
	input			clear;
	input			d;
	input			preset;
	input			notifier;

`ifdef CARBON_V2
	udp_dff_p0 D1(q, d, clk, clear, preset);
`else
	udp_dff_p0 D1(q, d, clk, clear, preset, notifier);
`endif

endmodule

`ifdef CARBON_V2

module udp_dff_p0(q, d, clk, clear, preset);
  output q;
  input  d;
  input  clk;
  input  clear;
  input  preset;
  reg    q;

  always @(posedge clk or negedge clear or negedge preset)
    if (~clear)
      q <= 1'b0;
    else if (~preset)
      q <= 1'b1;
    else
      q <= d;

endmodule

`else

primitive udp_dff_p0(q, d, clk, clear, preset, notifier);
output				q;
input				d;
input				clk;
input				clear;
input				preset;
input				notifier;
reg				q;
table
?    ?    0    ?    ?    : ?: 0;
?    ?    1    0    ?    : ?: 1;
0    r    1    1    ?    : ?: 0;
1    r    1    1    ?    : ?: 1;
1    *    1    ?    ?    : 1: 1;
0    *    ?    1    ?    : 0: 0;
?    n    1    1    ?    : ?: -;
*    ?    1    1    ?    : ?: -;
?    ?    p    1    ?    : ?: -;
?    b    *    1    ?    : 0: 0;
0    x    *    1    ?    : 0: 0;
?    ?    1    p    ?    : ?: -;
?    b    1    *    ?    : 1: 1;
0    x    1    *    ?    : 1: 1;
?    ?    ?    ?    *    : ?: x;
endtable
endprimitive

`endif
