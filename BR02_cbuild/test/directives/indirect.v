module top(a, b, c, d);
  input a, b;
  output c, d;

  mid u1(a, c);
  mid u2(b, d);
endmodule

module mid(in, out);
  input in;
  output out;

  inv u1(in, tmp);
  inv u2(tmp, out);
endmodule

module inv(i, o);
  input i;
  output o;
  wire   t, q;

  assign t = ~i;
  assign q = ~t;
  assign o = ~q;
endmodule
