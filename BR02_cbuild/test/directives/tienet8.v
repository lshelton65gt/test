// Test that assigns a constant to the same net twice.
module top (out, clk, in, en);
   output out;
   input  clk, in;
   input [1:0] en;

   wire [1:0]  dis;
   assign      dis = ~en;
   reg         out;
   always @ (posedge clk)
     if (dis == 2'b10)
       out <= in;

endmodule // top
