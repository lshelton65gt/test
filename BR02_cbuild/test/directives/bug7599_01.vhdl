-- August 2007
-- This test case demonstrates that directive forceSignal is not supported for
-- memory nets


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is

  type arr is array (0 to 3) of integer;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity bug7599_01 is
  port (
    clk  : std_logic;
    in1  : in  integer;
    in2  : in  integer;
    out1 : out integer;
    out2 : out integer);
end bug7599_01;

architecture arch of bug7599_01 is
  signal temp : integer;   
  signal arr_loc  :  arr;  -- carbon forceSignal
 begin
  P1: process (clk)
  begin  -- process P1
    if clk'event and clk = '1' then  -- rising clock edge
      arr_loc(0) <= in1;
      arr_loc(1) <= in2;
      arr_loc(2) <= in1;
      arr_loc(3) <= in2;
      temp <= in1;
    end if;
  end process P1;

  out1  <= arr_loc(0) + arr_loc(1) + temp;
  out2  <= arr_loc(2) + arr_loc(3);

end;
