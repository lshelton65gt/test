// filename: test/directives/embedded_01.v
// Description:  This test checks to see if embedded enableOutputSysTasks
// directives work if specified to enable and disable


module embedded_01(clock, in1, in2, out1) ; // carbon disableOutputSysTasks
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   sub1 u1(clock, in1, in2, out1) ;
   
   always @(posedge clock)
     begin: main
	$display("This line should never appear in the output");
     end
endmodule


module sub1(clock, in1, in2, out1) ; // carbon enableOutputSysTasks
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
	$display("sub1 out1: %d", out1);
     end
endmodule
