library ieee;
use ieee.std_logic_1164.all;

entity top is
  
  port ( pin  : in  string(1 to 8);
         pout : out string(1 to 8) );

end top;

architecture toparch of top is

begin  -- toparch

  pout <= pin;

end toparch;
