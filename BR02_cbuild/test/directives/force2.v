// This netlist shows even more terrors of not bottom-flattening a gate
// level netlist and then forcing one of them.  Here we define several
// different gate-level symbols, and watch the forced-ness propagate
// to almost every design net

module flop(q, clk, d);
  output q;
  input  clk, d;
  reg    q;

  always @(posedge clk)
    q <= d;
endmodule

module and2(z, a, b);
  output z;
  input  a, b;
  assign z = a & b;
endmodule

module or2(z, a, b);
  output z;
  input  a, b;
  assign z = a | b;
endmodule

module xor2(z, a, b);
  output z;
  input  a, b;
  assign z = a ^ b;
endmodule

module top(z5, clk, d1);
  output z5;
  input  clk, d1;

  flop u1(q1, clk, d1);   // q1 is marked forced, propagates to flop.q
  flop u2(q2, clk, q1);   // now flop.d is marked forced, so all flop
                          // inputs and outputs are forcible.
  and2 u3(z1, q1, q2);    // now and2.a and and2.b are marked forcible
  flop u4(q3, clk, z1);   // now and2.z is marked forcible
  or2  u5(z2, z1, q3);    // now or2.a and or2.b are  marked forcible
  and2 u6(z3, z1, z2);    // now or2.z is marked forcible
  xor2 u7(z4, z2, z3);    // now xor.a and xor.b are marked forcible
  xor2 u8(z5, z4, z1);    // now xor's output is marked forcible!
endmodule
