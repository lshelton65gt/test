module top (out1, out2, out3, out4);
   output out1, out2, out3, out4;

   wire   a1, a2, b1, b2;
   assign out1 = ~a1;
   assign out2 = ~a2;
   assign out3 = ~b1;
   assign out4 = ~b2;

endmodule
