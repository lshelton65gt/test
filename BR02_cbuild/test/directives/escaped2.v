// run this with the directive
// a variation on the test test/langcov/TaskFunction/tf2_f.v
// inline top.\tf2_an*
// it will correctly not in-line top.tf2_and
module top(clock, in1, in2, in3, out);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   input [7:0] in3;
   output [7:0] out;
   reg [7:0] 	out;
   reg [7:0] 	tmp1;
   reg [7:0] 	tmp2;

   initial
     begin
	out = 0;
     end

   task tf2_and ;
      output [7:0] o1;
      input [7:0] ia1;
      input [7:0] ia2;
      begin
	 o1 = ia1 & ia2;
      end
   endtask

   task tf2_or;
      output [7:0] o1;
      input [7:0] io1;
      input [7:0] io2;
      begin
	 o1 = io1 | io2;
      end
   endtask
   
      
   always @(posedge clock)
     begin: main
	tf2_and (tmp1, in1, in2);
	tf2_and (tmp2, in2, in3);
	tf2_or(out, tmp1, tmp2);
     end
endmodule // top
