module top (out1, out2, clk, bisten, bistin, in1, in2);
   output out1, out2;
   input  clk, bisten, bistin, in1, in2;

   assign out1 = bisten ? bistin : in1;

   sub S1 (out2, in2);

endmodule

module sub (o, i);
   output o;
   input  i;

   assign o = ~i;

endmodule

