module top(clk, ena, in, out);
  input clk, in, ena;
  output out;
  reg out;
  and(clk_ena, ena, clk);
  always @(posedge clk_ena)
    out <= in;
endmodule
