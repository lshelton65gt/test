module bug(clk1, clk2, d1, d2, q1, q2, ena, other);
   input clk1;
   input clk2;
   input  ena;
   input [3:0] other;
   input d1, d2;
   output q1, q2;

   subflp f1 (.clk(clk1), .d(d1), .q(q1), .ena(ena));
   subflp f2 (.clk(clk2), .d(d2), .q(q2), .ena(other[0]));
   
endmodule // bug

module subflp(clk, d, q, ena);
   input d;
   input clk;
   input  ena;
   output q;
   reg    q;

   wire    eclk = clk & ena;
   always @(posedge eclk)
     q <= d;
endmodule

