library ieee;
use ieee.std_logic_1164.all;

package defs is

  subtype MyInt is integer range 10 to 1000;

end defs;

library ieee;
use ieee.std_logic_1164.all;
use work.defs.all;

entity top is
  
  port ( pin  : in  MyInt;
         pout : out MyInt );

end top;

architecture toparch of top is

begin  -- toparch

  pout <= pin;

end toparch;
