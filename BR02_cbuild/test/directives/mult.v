module mult(clk, reset_, a, b, sum, product);
  input clk, reset_;
  output [6:0] a, b, sum;
  reg [6:0] sum;
  output [13:0] product;
  reg [13:0] product;

  counter c1(clk, reset_, 7'd1, a);
  counter c2(clk, reset_, 7'd7, b);

  always @(a or b) begin
    sum = a + b;
    product = a * b;
  end
endmodule // mult

module counter(clk, reset_, increment, q);
  input clk, reset_;
  input [6:0] increment;
  output [6:0] q;
  reg [6:0]    q;

  always @(posedge clk or negedge reset_) begin
    if (!reset_)
      q = 0;
    else
      q = q + increment;
  end
endmodule // counter
