module top(in1, in2, clk1, clk2, out, ena);
  input in1, in2;
  input ena;   // carbon tieNet 1'b1
  output out;
  input  clk1;
  input  clk2; // carbon collapseClock top.clk1

  reg a;   // carbon observeSignal
           // carbon depositSignal
  always @(posedge clk1)
    if (ena)
      a <= ~in1;

  
  wire out;  // carbon depositSignal
  flop u2(out, in2, clk2, ena);
endmodule

module flop(q, d, clk, ena);
  output q;
  input  d, clk, ena;
  reg    q;

  // carbon disallowFlattening
  always @(posedge clk)
    if (ena)
      q <= d;
endmodule
