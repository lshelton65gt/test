module top (out, in1, in2);
   output out;
   input  in1, in2;

   wire   c1, c2;
   reg 	  o1;
   assign c1 = in1;
   assign c2 = in2;
   assign out = o1;

   always @ (c1 or c2)
     $doand(o1, c1, c2);

endmodule // top
