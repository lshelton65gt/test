module top(in1, in2, in3, out1, out2, out3, clk1, clk2, clk3);
  input in1, in2, in3, clk1, clk2, clk3;
  output out1, out2, out3;
  reg out1, out2, out3;

  always @(posedge clk1) 
    out1 = in1;

  always @(posedge clk2) 
    out2 = in2;

  always @(posedge clk3) 
    out3 = in3;
endmodule
