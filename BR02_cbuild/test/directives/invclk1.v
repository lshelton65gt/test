
module fun(clkp, clkn, d1, d2, out1, out2);
   input clkp;
   input clkn;
   input d1, d2;
   output out1;
   output out2;
   reg    out1, out2;
   reg    tmp;

   wire   invclkp;
   assign invclkp = ~clkp;

   always @(posedge clkp)
     out1 <= d1;

   always @(posedge clkn)
     out2 <= d2;

   always @(posedge invclkp)
     tmp <= 0;
endmodule // fun

         