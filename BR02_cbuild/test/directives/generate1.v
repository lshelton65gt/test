// filename: test/directives/generate1.v
// Description:  This test is used to check that in-line directives can be
// applied to items defined within generate blocks (bug 13828)

module generate1(in1, in2, clk, out1, out2);
   // carbon enableOutputSysTasks
   input in1, in2, clk;
   output out1, out2;
   reg    out1; // carbon observeSignal

   middle middle (out2);

   always @(posedge clk)
     begin
        middle.r = in1 & in2;
        out1 = middle.bottom.r;
     end
endmodule

module middle(out2);
   output out2;
   reg 	  r;
   
   bottom bottom (r, out2);
endmodule


module bottom(r, out2);
   input r;
   output out2; // carbon observeSignal
                // ccarbon depositSignal

// here we have 2 generate loops (each has a single iteration,
//   so the result is a single definition of the memory 'temp'   

genvar i;
genvar j;
generate
      // carbon enableOutputSysTasks
   for ( i = 0; i < 1; i = i + 1)
     begin: blk
     for ( j = 0; j <= i; j = j + 1)
	begin: blk2
	   reg   [0:0] temp [0:1]; // carbon observeSignal
	   always @(*)
	     begin
		temp[1] <= temp[0];
		temp[0] = r;
	     end
	   assign out2 = temp[1];
	end
   end
endgenerate
endmodule
