library ieee;
use ieee.std_logic_1164.all;

package defs is

  type rec is record
    f1 : std_logic_vector(0 to 7);
    f2 : std_logic_vector(0 to 7);
  end record;

end defs;

library ieee;
use ieee.std_logic_1164.all;
use work.defs.all;

entity top is
  
  port ( pin  : in  rec;
         pout : out rec );

end top;

architecture toparch of top is

begin  -- toparch

  pout <= pin;

end toparch;
