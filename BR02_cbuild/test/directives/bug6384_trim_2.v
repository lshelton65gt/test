
// The directive file for this test contains "depositSignal TOP.mid_i.bot2_i1.pl". This net is
// eliminated before directive processing. Check that a NetHasGone warning is written for this
// explicit directive rather than an alert.

// Generated with: ./rangen -s 96 -n 0xf3d6b679 -o /w/bohrium/cloutier/Wmaint2/obj/Linux.product/test/rangen/rangen/testcases.072406/cbld/non_zero_exit6.v
// Using seed: 0xf3d6b679
// Writing cyclic circuit of size 96 to file '/w/bohrium/cloutier/Wmaint2/obj/Linux.product/test/rangen/rangen/testcases.072406/cbld/non_zero_exit6.v'
// Module count = 6

// module TOP
// size = 6 with 2 ports

// trimmed down version

module TOP(i, out1);
   input i;
   output out1;

   mid mid_i ( , );		// two unconnected inout ports

endmodule

module mid(INOUT1 , INOUT2);
   inout  INOUT1;
   inout  INOUT2;


   wire 	 WE;
   wire 	 WF;
   reg 		 R1;
   wire [24:0] 	 WD;


   bot1 bot1_i (WF, WE);

   bot2 bot2_i1 ( R1);

   bot3 bot3_i1 (1'b1, WD, INOUT1);

endmodule

module bot1(out3, in1);
   output out3;
   input  in1;

   wire [22:0]  WA;

   bot2 bot2_i2 (in1);

   bot3 bot3_i2 (1'b1, {WA,out3,out3}, );

endmodule


module bot2(pl);
   input pl;

   wire  pl;

endmodule

module bot3(in1, out1, out2);
   input   in1;
   output  out1;
   output  out2;

   reg [24:0] out1;
   reg 	      out2;
   
   always @(negedge in1)
     begin
	out1 = 0;
     end

   always @(in1)
     begin
	out1 = 0;
     end

   always @(negedge out2)
     begin
	out2 = 0;
     end

endmodule

