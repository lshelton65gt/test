module flopinv(in1, in2, in3, out1, out3, clk1, clk2);
  input in1, in2, clk1, clk2, in3;
  output out1, out3;
  reg q1, q2, t1, t2, out1, out3;

  always @(posedge clk1) 
    q1 = in1;

  always @(posedge clk2) 
    q2 = in2;

  always @(q1)
    t1 = q1;
    
  always @(q2)
    t2 = q2;
    
  always @(t1 or t2)
  begin
    out1 = t1 + t2;
    out3 = in3;
  end
   
endmodule
