module top(a, b, sum);
  input [7:0] a, b;
  output [7:0] sum;
  wire [7:0]   diff;            // dead logic
  assign       sum = a + b;
  assign       diff = a - b;
endmodule // top
