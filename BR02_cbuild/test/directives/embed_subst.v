module top(ck,d,q);
   input ck, d;
   output q;
   
  flop u1(ck, d, q);

endmodule // top

module flop(ck,d,q);
   input ck, d;
   output q;
   reg    q;  // carbon forceSignal
    
   always @(posedge ck)
     q <= d;
endmodule

module andgate(ck,d,q);
   input ck, d;
   output q;  // carbon tieNet 1'b0

  assign  q = ck & d;
endmodule

