module top(out, a, b);
  input a, b;
  output out;

  sub sub1(a_, a);
  sub sub2(b_, b);
  assign out = a_ ^ b_;
endmodule

module sub(out, in);
  output out;
  input  in;

  assign out = !in;
endmodule
