module top(clk1, clk2, in, out);
  input clk1, clk2;
  input [5:0] in;
  output [5:0] out;
  reg [5:0]    out;

  initial out = 5'b0;

  // clk1, equivalent, and inversion
  always @(posedge clk1)
    out[0] <= in[0];
  and(clk1a, clk1, clk1);
  always @(posedge clk1a)
    out[1] <= in[1];
  not(clk1_, clk1);
  always @(posedge clk1_)
    out[2] <= in[2];


  // clk2, and a combinational with clk1
  always @(posedge clk2)
    out[3] <= in[3];

  or(clk3, clk1, clk2);

  always @(posedge clk3)
    out[4] <= in[4];

  // clk4, a divided clock
  reg          clk4;
  initial clk4 = 0;
  always @(posedge clk1)
    clk4 = ~clk4;

  always @(posedge clk4)
    out[5] <= in[5];
endmodule
