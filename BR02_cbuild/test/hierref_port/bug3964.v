module top(a,b,o1,o2);
   input a,b;
   output o1,o2;

   ander ander(a, b, o1);
   storage storage(ander.a, ander.b, o2);
endmodule

module ander(a,b,o);
   input a,b;
   output o;
   assign o = a & b;
endmodule

module storage(a,b,o);
   input a,b;
   output o;
   assign o = a & b;
endmodule
