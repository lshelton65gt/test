// Testcase to check that we emit some hierrefs as pointers and others
// as direct reverences. The direct references should not increase the
// size of the model.
module top(a,b,o1,o2);
   input a,b;
   output o1,o2;

   ywrap y(o1);

   xwrap x(a,b,o2);

   // Hierarchical reference separate from a port connection. 
   // Code as a direct reference.
   assign x.storage.o = a & b;
endmodule

module ywrap(o1);
   output o1;
   
   // hierarchical reference port connections
   ander ander(x.storage.a, // Code as ptr.
	       x.storage.b, // Code as ptr.
	       o1);

endmodule

module xwrap(a,b,o2);
   input a,b;
   output o2;

   storage storage(a,b,o2);

endmodule

module ander(a,b,o);
   input a,b;
   output o;
   assign o = a & b;
endmodule

module storage(a,b,o);
   input a,b;
   output o;
endmodule
