// testcase that shows a gcc-compilation error due to missing member variables.
// % cbuild -q emc-gcc.v -enableOutputSysTasks
// emc-gcc.v:33: Note 3028: Input monitor.OK has been coerced to inout.
// Error 2023: Compilation errors: Examine ./libdesign.codegen.errors, If not a computer resource problem (such as file permissions or quota) contact Carbon Design Customer Support.

module sys(clk,ik); // carbon disallowFlattening
   input clk;
   input ik;
   wire [7:0] IOP_OK; // carbon observeSignal 
   assign     IOP_OK = sys.bosco.RM.IOPOK;
   monitor mon(clk,sys.bosco.IOP_OK);
   BOSCO_UCM bosco(clk,ik);
endmodule

module BOSCO_UCM(clk,ik); // carbon disallowFlattening
   input clk;
   input ik;
   wire [7:0] IOP_OK;
   RM RM(.IOPOK(IOP_OK));
   IOP_GEN gen(.clk(clk),.ik(ik),.ok(IOP_OK));
endmodule

module RM(IOPOK); // carbon flattenModule
   input [7:0] IOPOK;
endmodule

module IOP_GEN(clk,ik,ok); // carbon flattenModule
   input clk;
   input ik;
   output [7:0] ok;
   reg [7:0] ok;
   always @(posedge clk)
     ok = ~{8{ik}};
endmodule

module monitor(clk,OK); // carbon disallowFlattening
   input clk;
   input [7:0] OK;
   monitor_carbon cmodel(clk,OK);
endmodule

module monitor_carbon(clk,OK);
   input clk;
   inout [7:0] OK;
   assign OK = ~{8{clk}};

   always @(posedge clk) $display(OK);
endmodule
