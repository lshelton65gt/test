// locally relative hierarchical reference initialization testcase
// similar to the one in the design doc.
module top(data,out,dummy_out);
   input data;
   output out;
   output dummy_out;

   wire   real_location;

   // When manufacturing the constructor for c_top.m_s0, we
   // incorrectly referenced s1.out through its m_s1.a_out name
   // instead of m_s1.m_out or m_s1.get_out()
   sub s0(out,s1.out);
   sub s1(real_location,data);

   // Two drivers to the same net preventing allocation at sub.out.
   // This causes our s1.out to be a reference and real_location to be
   // its storage.
   sub dum0(dummy,data);
   sub dum1(dummy,data);
   assign dummy_out = (real_location | dummy);
endmodule

module sub(out,in);
   output out;
   input  in;
   assign out = ~in;
endmodule
