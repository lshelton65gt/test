// simple testcase showing a cyclic hierarchical reference.
module top(out);
  output out;
  sub sub(.data(sub.data));
endmodule

module sub(data);
  output data;
endmodule
