// simple testcase showing cyclic hierarchical references.
module top();

   // the sub hierarchical reference is self-referencial through
   // several levels of hierarchy.
   sub sub( .data(sub.basement.data) );

   // The cross hierarchy uses two hierarchical references to build a cycle.
   sub cross0( .data(cross1.basement.data) );
   sub cross1( .data(cross0.basement.data) );

   // The chain hierarchy uses a number of hierarchical references to
   // build a cycle. Not all of the hierrefs refer to the lowest
   // level, either.
   sub chain0( .data(chain1.basement.data) );
   sub chain1( .data(chain2.data) );
   sub chain2( .data(chain3.basement.data) );
   sub chain3( .data(chain4.data) );
   sub chain4( .data(chain5.basement.data) );
   sub chain5( .data(chain6.data) );
   sub chain6( .data(chain7.basement.data) );
   sub chain7( .data(chain8.data) );
   sub chain8( .data(chain0.basement.data) );

   // The hidden hierarchy builds a cycle through hierarchical
   // references which do not appear at the same level of hierarchy.
   sub visible( .data(hidden.sub.basement.data) );
   hidden hidden();
endmodule

module hidden();
   sub sub( .data(top.visible.data) );
endmodule

module sub(data);
   inout data;
   basement basement( .data(data) );
   // other hierarchy unrelated to the hierref.
   misc misc0( .data(data) );
   misc misc1( .data(data) );
endmodule

module basement(data);
   inout data;
   // other hierarchy unrelated to the hierref.
   misc misc0( .data(data) );
   misc misc1( .data(data) );
endmodule

module misc(data);
   inout data; // carbon observeSignal
               // carbon depositSignal
   pullup(data);
endmodule
