// example with port hierarchical references at different points in the hierarchy
module top(en,data,out);
   input en;
   input [127:0] data;
   output [31:0] out;
   
   enabled low0(en,storage.data_low[31:0],data[31:0]);
   wrap_low low1(en,data);

   enabled high0(en,storage.data_high[63:32],data[95:64]);
   wrap_high high1(en,data);
   
   storage storage(out);
endmodule

module wrap_low(en,data);
   input en;
   input [127:0] data;
   wire en_n = ~en;
   enabled low0(en_n,storage.data_low[31:0],data[63:32]);
endmodule

module wrap_high(en,data);
   input en;
   input [127:0] data;
   wire en_n = ~en;
   enabled high0(en_n,storage.data_high[63:32],data[127:96]);
endmodule

module enabled(en,io,data);
   input en;
   inout [31:0] io;
   input [31:0] data;
   assign 	io = en ? data : 32'bz;
endmodule

module storage(out);
   output [31:0] out;
   wire [31:0] data_low;
   wire [63:32] data_high;
   assign 	out = data_low | data_high;
endmodule
