// Testcase where port hierarchical reference is only referenced in
// the port lists. Codegen initially was performing an incorrect mark
// pass; the 'cross' data member was seen as unreferenced.
module top(data,out);
   input data;
   output out;

   sub s0(storage.cross,data);
   sub s1(out,storage.cross);
   storage storage();
endmodule

module sub(out,in);
   output out;
   input  in;
   assign out = ~in;
endmodule

module storage();
   wire cross;
endmodule

