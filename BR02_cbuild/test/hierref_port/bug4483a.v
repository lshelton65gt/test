module bug4483(in,a,b,c,d);
   input in;
   output a,b,c,d;

   reader r0(a,in);
   reader r1(b,r0.in);
   reader r2(c,r1.in);
   reader r3(d,r2.in);
endmodule

module reader(out,in);
   output out;
   input  in;
   assign out = ~in;
endmodule
