// example with four different port hierarchical references, all at the same level.
module top(en,data,out);
   input en;
   input [127:0] data;
   output [15:0] out;
   
   wire en_n = ~en;
   enabled low0(en,storage.data_low0[15:0],data[15:0]);
   enabled low1(en,storage.data_low1[31:16],data[31:16]);
   enabled low2(en_n,storage.data_low0[15:0],data[47:32]);
   enabled low3(en_n,storage.data_low1[31:16],data[63:48]);

   enabled high0(en,storage.data_high0[47:32],data[79:64]);
   enabled high1(en,storage.data_high1[63:48],data[95:80]);
   enabled high2(en_n,storage.data_high0[47:32],data[111:96]);
   enabled high3(en_n,storage.data_high1[63:48],data[127:112]);

   storage storage(out);
endmodule

module enabled(en,io,data);
   input en;
   inout [15:0] io;
   input [15:0] data;
   assign 	io = en ? data : 16'bz;
endmodule

module storage(out);
   output [15:0] out;
   wire [15:0] data_low0;
   wire [31:16] data_low1;
   wire [47:32] data_high0;
   wire [63:48] data_high1;
   assign      out = data_low0 | data_low1 | data_high0 | data_high1;
endmodule
