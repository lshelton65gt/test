module top(in, out);
   input in;
   output out;

   storage storage();   
   sub sub(top.storage.thisclk, in, out);

   
endmodule // top

module storage;
   wire thisclk; 
endmodule // storage

module sub(clk, in, out);
   input in, clk;
   output out;
   reg    out;

   wire   clk; // carbon forceSignal

   assign top.storage.thisclk = clk;
   
   always @(posedge clk)
     out <= in;
endmodule // sub

