module top(o,en);
   output o;
   input en;

   // The wrap.o net is an illegal allocation since it appears in the
   // alias ring twice. The only legal allocation for this ring is
   // top.o.
   wrap w0(o,en);
   wrap w1(o,~en);
   
   // The hierref at the port forces the allocation down to wrap.o.
   // Unfortunately, the w0 and w1 instances prevent wrap.o from being
   // an allocation point.
   wrap wrap(wrap.storage.data,en);

   // There are two important alias rings in this testcase:
   // top.o
   //     top.w0.o
   //     top.w1.o
   // top.wrap.o
   //     top.wrap.storage.data
   //     top."wrap.storage.data" (hierref)
   //
   // A valid allocation strategy would have been to choose top.o and
   // top.wrap.storage.data as allocation points. Unfortunately,
   // REAlloc did not uncover this as a possibility.

 endmodule

module wrap(o,en);
   output o;
   input en;

   assign o = en ? 1'b1 : 1'bz;

   storage storage();
endmodule

module storage();
   // There is nothing explicitly preventing storage.data from being
   // an allocatable net.
   wire data; // carbon observeSignal
   pulldown(data);
endmodule
