module top(o,en);
   output o;
   input en;

   // these two instances prevent wrap.o from being an allocation
   // candidate (since it will appear twice in the alias ring).
   wrap w0(o,en);
   wrap w1(o,en);
   
   // this instance will end up being unallocatable.
   wrap wrap(wrap.b1.storage.data,en);

endmodule

module wrap(o,en);
   output o;
   input en;
   wire en_n = ~en;

   // these two instances prevent box.o from being an allocation
   // candidate.
   box b1(o,en);
   box b2(o,en_n);

endmodule

module box(o,en);
   output o;
   input  en;
   assign o = en ? 1'b1 : 1'bz;

   storage storage();
endmodule

module storage();
   // How do we prevent 'data' from being an allocation point?
   wire data; // carbon observeSignal
   pulldown(data);
endmodule

