module bug4483(in,out);
   input in;
   output out;

   left left(in);
   pass pass(left.in);
   right right(out);
endmodule

module left(in); // carbon disallowFlattening
   input in;
endmodule

module pass(in);
   input in;
endmodule

module right(out); // carbon disallowFlattening
   output out;
   assign out = pass.in;
endmodule
   
