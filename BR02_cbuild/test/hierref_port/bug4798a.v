// last mod: Mon Aug  1 12:02:29 2005
// filename: test/hierref_port/bug4798a.v
// Description:  This test was inspired by bug 4798, where cheetah had problems
// with a hierref used on a port of a module instance.  We were unable to
// duplicate the problems seen in that bug but this testcase was the first to
// use parameterized modules with hierarchical references on ports


module bug4498a(clk, in1, in2, in4, out1, out2, out3, out4);
   parameter width = 1;
   input     clk;
   input [width-1:0] in1, in2, in4;
   output [width-1:0] out1,out2,out3, out4;

   bug4498a_intermeidate I1(clk, in1, in2, in4, out1, out2, out3, out4);
   
endmodule


module bug4498a_intermeidate(clk, in1, in2, in4, out1, out2, out3, out4);
   parameter width = 1;
   input clk;
   input [width-1:0] in1, in2, in4;
   output [width-1:0] out1,out2,out3, out4;

   
   left  Ileft1(clk, in4, out4);
   left #2 Ileft(clk, in1, out1);
   right  Iright(clk, in2, out2);

   pass pass(.clk(clk), .out(out3), .in2(I1.Ileft.internal), .in1(I1.Iright.internal));

endmodule

module left(clk, in, out);
   parameter width_l = 1;
   input clk;
   input [width_l-1:0] in;
   output [width_l-1:0] out;
   reg 	  [width_l-1:0] out;

   wire [width_l-1:0] 	internal;

   assign internal = ~in;

   always @(posedge clk)
     begin: main
	out = internal & out;
     end

endmodule


module right(clk, in, out);
   parameter width_r = 1;

   input clk;
   input  [width_r-1:0] in;
   output [width_r-1:0] out;
   reg 	  [width_r-1:0] out;
   reg   [width_r-1:0] internal;

   always @(posedge clk)
     begin: main
	internal = ~in;
	out = internal & out;
     end

endmodule


module pass(clk,in1, in2, out);
   parameter width_p = 1;
   input clk;
   input  [width_p-1:0] in1, in2;
   output [width_p-1:0] out;
   reg 	  [width_p-1:0] out;
   
   always @(posedge clk)
     begin: main
	out = in1 & in2 ;
     end

endmodule
