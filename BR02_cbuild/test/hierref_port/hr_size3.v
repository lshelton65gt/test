// Testcase to check that we emit some hierrefs as pointers and others
// as direct reverences. The direct references should not increase the
// size of the model.
module top(a,b,out);
   input a,b;
   output out;

   wire   cross;
   
   // hierarchical reference port connections
   ander ander(storage.a, // Code as ptr.
	       storage.b, // Code as ptr.
	       cross);

   storage storage(out);

   // Hierarchical references separate from a port connection. 
   // Code as a direct reference.
   assign storage.out = cross;
endmodule

module ander(a,b,o);
   input a,b;
   output o;
   assign o = a & b;
endmodule

module storage(out);
   output out;
   wire   a = top.a; // Non-local: Code as ptr.
   wire   b = top.b; // Non-local: Code as ptr.
endmodule
