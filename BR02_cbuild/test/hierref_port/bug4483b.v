module bug4483(in,a,b,c,d);
   input in;
   output a,b,c,d;

   reader r0(a,r1.in);
   reader r1(b,r2.in);
   reader r2(c,r3.in);
   reader r3(d,in);
endmodule

module reader(out,in);
   output out;
   input  in;
   assign out = ~in;
endmodule
