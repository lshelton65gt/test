// filename: test/sv/MultiDim/resynth_01.sv
// this test makes sure that memoryResynth is working for NULMemselLvalue when
// the memory was declared with no unpacked dimension.  In this example that is
// mem1.  Note that mem2 is equivalent but was explicitly declared with a
// unpacked dimension.

module resynth_01(input clock, input [7:0] in1, in2, output reg [7:0] out1, out2,  output reg out3);

   reg [1:0][7:0] mem1;		// has no unpacked dimension, but will be added by resynthesizememories
   reg [1:0][7:0] mem2 [0:0];
   always @(posedge clock)
     begin: main
	// first do the same operation to both memories
	mem1 = {in1, in2};
	mem2[0] = {in1, in2};

	// then the same operation but using the two memories on the RHS using different access methods
	out1 = (mem1 & (mem1>>8));
	out2 = (mem2[0] & (mem2[0][1]));

	// finally a single bit that shows that we have correct results
	out3 = ((out1 == out2 ) && (out1 == (in1 & in2))); // should always be true
       out1++;
     end
endmodule
