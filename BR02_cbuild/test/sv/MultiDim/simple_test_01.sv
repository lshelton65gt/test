// demonstrates a codegen error, gets warning:
// tmp.f_000000.cxx:5800: warning: left shift count >= width of type
// the issue was that arrays declared with no unpacked dimensions were not properly handled.

module simple_test_01(
    input bit [15:0][3:0] bit_arr1,
    output bit result);

//   byte        unsigned [1:0]   eq_byte;
   bit [15:0][3:0] bit_arr1_loc;
   longint 		  unsigned eq_longint1;
   longint 		  unsigned eq_longint2;
   bit 		   res1;
   

    always @ *
    begin
       bit_arr1_loc = bit_arr1;
       bit_arr1_loc[1] = 1;
       eq_longint1 = bit_arr1_loc;
       eq_longint2 = bit_arr1_loc;
       res1 = (eq_longint1 == bit_arr1_loc);
        
       result = res1;
    end

endmodule

