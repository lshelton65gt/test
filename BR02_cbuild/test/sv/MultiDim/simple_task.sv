// demonstrates a codegen error, gets warning:
// tmp.f_000000.cxx:5800: warning: left shift count >= width of type

module simple_task(
    input wire [7:0]     in1, in2,
    output bit result);

   reg [3:0] bit_arr1 [15:0][1:0];

   longint 		  unsigned eq_longint1;
   bit 		   res1;

   
task doplus;
//  output wire [3:0] o [1:0];
 output  [3:0] o [1:0];   
//  input wire [8:1] a;
  input [8:1] a;   
  input [8:1] b;
  o[1] <= a + b;
  o[0] <= a - b;
endtask
      

    always @ *
    begin
//       bit_arr1[0] = 1;
       doplus(bit_arr1[2], in1, in2);
//       eq_longint1 = bit_arr1;
       res1 = (eq_longint1 == bit_arr1[1][0]);
        
       result = res1;
    end

endmodule

