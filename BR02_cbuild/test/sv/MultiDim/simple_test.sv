module simple_test(input bit [15:0][3:0] bit_arr1,
               output bit result);

    byte unsigned eq_byte1;

    bit [15:14][3:0] eq_byte_arr1;

    bit res1, res2;

    always @ *
    begin
        eq_byte1 = bit_arr1[15:14];
        eq_byte_arr1 = bit_arr1[15:14];

        res1 = ((eq_byte1 == eq_byte_arr1));
        

        result = (res1) ? 1 : 0;
    end

endmodule

