// filename: test/sv/MultiDim/simple_19_12_test2.sv
// Description:  This test is a simplified version of
// test/sv/Verific/Synthesizable/19/19.12_test2_test.sv, but without the
// typedef, and with modifications to the instance array, and
// the port types on the 'bot' module. The original RTL
// does not compile with VCS or NCVerilog, and is probably
// illegal SystemVerilog code.

module simple_19_12_test2 (in1, out, clk);
    input [0:7] in1;
    input clk;
    output [0:7] out;
    
    logic [1:0] temp1 [0:7];
    logic [1:0] temp2 [0:7];

    assign {temp1[0], temp1[1], temp1[2], temp1[3]} = in1;
    assign out[0:7] = {temp2[0], temp2[1], temp2[2], temp2[3]};

    bot i1 [0:7] (clk, temp1, temp2);

endmodule

module bot (input clk, input logic [1:0] in1, output logic [1:0] out1);

    always @ (posedge clk)
        out1 = in1;

endmodule

