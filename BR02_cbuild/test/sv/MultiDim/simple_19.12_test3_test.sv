// simpleified version of the test
// test/sv/Verific/Synthesizable/19/simple_19.12_test3_test.sv
// this simplified version does not use typedef on module ports.

module test (in1, out, clk);
    input [0:7] in1;
    input clk;
    output [0:7] out;
    
    logic  [0:3] [0:1] temp1, temp2;

    assign temp1 = in1;
    assign out = temp2;

    bot i1 [0:3] (clk, temp1, temp2);

endmodule

module bot (input clk, input logic [0:1]  in1, output logic [0:1]  out1);

    always @ (posedge clk)
        out1 = in1;

endmodule

