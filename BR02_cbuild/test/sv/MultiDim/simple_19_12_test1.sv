// filename: test/sv/MultiDim/simple_19_12_test1.sv
// Description:  This test is a simplified version of
// test/sv/Verific/Synthesizable/19/19.12_test1_test.sv, but without the
// typedef. It is is expected that it will get the same simulation 
// results as simple_19_12_test1.sv

module simple_19_12_test1 (in1, out, clk);
    input [0:3] in1;
    input clk;
    output [0:7] out;
    
    logic temp1 [0:1] [0:3];
    logic temp2 [0:1] [0:3];

    assign {temp1[0][0], temp1[0][1], temp1[0][2], temp1[0][3]} = in1;
    assign {temp1[1][0], temp1[1][1], temp1[1][2], temp1[1][3]} = in1;
    assign out = {temp2[1][0], temp2[1][1], temp2[0][2], temp2[0][3]};

    bot i1 [0:1] (clk, temp1, temp2);

endmodule

module bot (input clk, input logic in1[0:3], output logic out1[0:3]);

    always @ (posedge clk)
        out1 = in1;

endmodule

