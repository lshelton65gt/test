// filename: simple_range.sv
// Description:  This test had a problem with indicies

module simple_range(input clock, input in1, input [7:0] in2, output reg [7:0] out1);
   reg [0:7][3:0]m;

   initial
      begin
	 m = 32'h12345678;
      end

   
   always @(posedge clock)
     begin: main
	if ( in1 )
	   begin
	      out1 = m[6:7];
	      m[6:7] = in2;
	      $display("m: 0x%x", m);
	   end
     end
endmodule
