// filename: test/sv/MultiDim/simple_range_01.sv
// Description:  This test was inspired by test/sv/Verific/Positive/5/ area
// the problem is that the second unpacked dimension of memory m was not properly normalized

module simple_range_02(input clock, input in1, output reg [3:0] out1[8:1]);
   reg [3:0] m [1:0][0:7];

   integer   ii,jj;
   initial
      begin
	 for (ii = 1; ii >= 0; ii = ii -1)
	    begin
	       for (jj = 0; jj <= 7; jj = jj + 1)
		  begin
		     m[ii][jj]= ii*jj;
		  end
	    end
      end

   
   always @(posedge clock)
     begin: main
	out1[8:7] = m[in1][6:7];
     end
endmodule
