// demonstrates a codegen error, gets warning:
// tmp.f_000000.cxx:5800: warning: left shift count >= width of type
// this is a recoded simple_test_02.sv file to see what code it generates
module simple_test_03(
    input [63:0] bit_arr1,
    output bit result);

   longint 		  unsigned eq_longint1;
   bit 		   res1;
   

    always @ *
    begin
       eq_longint1 = bit_arr1[0];
       res1 = (eq_longint1 == bit_arr1[0]);
        
       result = res1;
    end

endmodule

