// filename: test/sv/MultiDim/test_2_1_b.sv
// Description: This test verifies correct indexing of packed arrays.
module child (
input wire [3:0] n1,
input wire [3:0] n2,
input wire [3:0] n3,
input wire [3:0] n4,
output reg [3:0] on11,
output reg [3:0] on10,
output reg [3:0] on01,
output reg [3:0] on00,
output reg [15:0] ow
);

reg [1:0][1:0][3:0] a;


always_comb
begin
   a[1][1] = n1;
   a[1][0] = n2;
   a[0][1] = n3;
   a[0][0] = n4;     
   {on11, on10} = a[1];
   {on01, on00} = a[0];
   ow = a;
end

endmodule

module top (
input wire [3:0] n1,
input wire [3:0] n2,
input wire [3:0] n3,
input wire [3:0] n4,
output wire [3:0] on11,
output wire [3:0] on10,
output wire [3:0] on01,
output wire [3:0] on00,
output wire [15:0] ow
);

child i1 (n1, n2, n3, n4, on11, on10, on01, on00, ow);

endmodule
