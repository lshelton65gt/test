// filename: test/sv/MultiDim/test_3_0_b.sv
// Description: This test verifies correct indexing of packed arrays.
// Currently it segfaults, due to non-zero LSB in the second
// packed dimension of 'a'.
module child (
input wire [3:0] n1,
input wire [3:0] n2,
input wire [3:0] n3,
input wire [3:0] n4,
output reg [3:0] on11,
output reg [3:0] on10,
output reg [3:0] on01,
output reg [3:0] on00,
output reg [31:0] ow
);

reg [1:0][4:1][3:0] a;


always_comb
begin
   a[0] = 16'hffff;              // low 16 bits
   a[1] = {8'b10101010, n1, n2}; // high 16 bits
   a[0][4:3] = {n3, n4};         // upper half of low 16 bits
   {on11, on10} = a[1][4:3];
   {on01, on00} = a[0][2:1];
   ow = a;
end

endmodule

module top (
input wire [3:0] n1,
input wire [3:0] n2,
input wire [3:0] n3,
input wire [3:0] n4,
output wire [3:0] on11,
output wire [3:0] on10,
output wire [3:0] on01,
output wire [3:0] on00,
output wire [31:0] ow
);

child i1 (n1, n2, n3, n4, on11, on10, on01, on00, ow);

endmodule
