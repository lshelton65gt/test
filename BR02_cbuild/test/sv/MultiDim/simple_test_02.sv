// demonstrates a codegen error, gets warning:
// tmp.f_000000.cxx:5800: warning: left shift count >= width of type
// the issue was that arrays declared with no unpacked dimensions were not properly handled.

module simple_test_02(
    input [15:0][3:0] bit_arr1_inp,
    output  logic     result);

   longint 	      unsigned eq_longint1;
   longint 	      unsigned eq_longint2;
   bit 		      res1;
   logic [15:0][3:0]  bit_arr1;

//   initial
//     $monitor("M: %x, %x", eq_longint1, bit_arr1);

   always @ *
     begin
	bit_arr1 = bit_arr1_inp;
	bit_arr1[1] = 1;
	eq_longint1 = bit_arr1;
	eq_longint2 = bit_arr1;
	res1 = (eq_longint1 == bit_arr1);
        
	result = res1;
     end

endmodule

