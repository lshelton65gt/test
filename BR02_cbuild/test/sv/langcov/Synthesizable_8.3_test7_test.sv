
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog includes the C assignment operators. In this
// test case operators <<<= and >>>= are tested.

// Carbon: Removed unsupported language constructs:
//    int
//
// Tests <<<=, >>>= assignment operators
// Added extra level of hierarchy to encapsulate test, test2 to
// simplify testing.

module test (clk, in, out) ;
    input clk, in ;
    output out ;
    // int in, out ;
    integer in, out ;
    
    always @ (posedge clk)
    begin
        out = in ;
        out <<<= 4;
    end
endmodule : test

module test2 (clk, in, out) ;
    input clk, in ;
    output out ;
    // int in, out ;
    integer in, out ;
    
    always @ (posedge clk)
    begin
        out = in ;
        out >>>= 21;
    end
endmodule : test2

// Following added by carbon

module top (clk, in1, in2, out1, out2);
input clk, in1, in2;
output out1, out2;
integer in1, in2, out1, out2;

test u1 (clk, in1, out1);
test2 u2 (clk, in2, out2);

endmodule : top
