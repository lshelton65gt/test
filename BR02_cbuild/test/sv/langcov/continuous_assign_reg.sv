// Simple test for continuous assign to a register.
// LRM P1800-2012 10.3
module top (input wire [15:0] a, input wire clk, output wire [15:0] b);

reg [15:0] temp2;
reg [15:0] temp1 = 0;

always @(posedge clk)
begin
   int i;
   // int in a for loop, auto increment     
   // i++ causes an assertion     
   for (i = 0; i < 16; i = i + 1)
      begin
         temp1[i] = a[15-i];
      end
end 

// Continuous assign to reg
assign temp2 = temp1;

assign b = temp2;

endmodule : top
