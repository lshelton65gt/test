
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'continuous assignment' on variables of new
// data types that SystemVerilog introduced. SystemVerilog supports
// 'continuous assignment' to any type of variables.

// Carbon: Modified to remove continuous assign array - not supported.
module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

bit b;
bit [width-1:0] bpa;
byte bt;
logic l;
logic [width-1:0] lpa;
reg r;

`ifdef CONT_ASSIGN_ARRAY_NOT_SUPPORTED
assign
        b = in1[0] ^ in2[0],
        bpa = in1 + in2,
        bt = { 8 { in1[0] | in2[width-1] ^ in1[width-1] - in2[0] } },
        l = in1[0] * in2[0],
        r = ^in1,
        lpa = in1 & in2;
`else
assign        b = in1[0] ^ in2[0];
assign        bpa = in1 + in2;
assign        bt = { 8 { in1[0] | in2[width-1] ^ in1[width-1] - in2[0] } };
assign        l = in1[0] * in2[0];
assign        r = ^in1;
assign        lpa = in1 & in2;
`endif

assign out1 = b | l & bt;
assign out2 = lpa + b - bpa;

endmodule

