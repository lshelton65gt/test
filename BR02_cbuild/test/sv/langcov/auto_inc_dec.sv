// This is a simple test for auto increment and auto decrement.
// It tests two scenarios: 
// 1) auto inc/dec statement
// 2) auto inc/dec in an expression
//
// Currently (rev 1512) this causes an assertion in CheetahContext.cxx:449
// P1800-2012 LRM, 11.4.2

module top (
   input wire [1:0] op, // Selects reset, load, auto-inc, or auto-dec
   input integer pi, 
   input integer mi, 
   input wire rst, 
   input wire clk, 
   output integer po, 
   output integer mo
);

integer incp = 0;
integer incm = 10000;

integer pi_temp;
integer mi_temp;
integer ctemp;

always @(posedge clk)
begin
        // Reset 
        if (rst == 1) 
           begin
              pi_temp = 0;
              mi_temp = 0;
           end
        // Load
        else if (op == 0)
           begin
              pi_temp = pi;
              mi_temp = mi;
           end
        // auto inc/dec statement
        else if (op == 1)
          begin
             pi_temp++;
             mi_temp--;
          end
        // auto inc/dec rhs expression
        else  
          begin
             pi_temp = incp++;
             mi_temp = incp--;
          end
end

assign po = pi_temp;
assign mo = mi_temp;

endmodule


        
                      
