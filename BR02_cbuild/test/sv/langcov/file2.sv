// compile this with the command:
// cbuild -q -useVerific -sverilog empty.sv file2.sv
// for some reason this causes the carbon translate_off to be ignored!
// but when you compile it with the command:
// cbuild -q -useVerific -sverilog        file2.sv
// it works fine.

module file2(input clock, input [7:0] in1, in2, output reg [7:0] out1);
//carbon translate_off
   This_is_invalid_verilog; // a line that will cause a syntax error if included 
//carbon translate_on   
   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
       out1++;
     end
endmodule
