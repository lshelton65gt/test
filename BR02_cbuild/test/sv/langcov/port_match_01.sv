// filename: test/sv/langcov/port_match_01.sv
// Description:  This test is a trimmed down and slightly modified version of Verific/Synthesizable/4/4.9_test18_test.sv 
// with r1953 this gets a CARBON INTERNAL ERROR because the actual arg of
// instance b1 is not same shape as formal arg o1

module port_match_01(clk, i1, i2, i3, i4, o1);
   typedef bit [0:3] nibble;
   //typedef nibble [0:W2 -1] dtype;  // old style
   typedef bit [0:31] dtype;	// use vector instead of 1packed 1unpacked dimension here

   input 	      clk;
   input signed [0:31] i1, i2, i3, i4;
   output reg signed [0:31] o1;

   dtype d3;

   always @(posedge clk)
     begin
	o1 = (i2);
     end

   bottom b1(clk, i2, d3);		// d3 value is unused but port connection causes problem 

endmodule

module bottom(clk, i1, o1);

   typedef bit [0:3] nibble;
   typedef nibble [0:7] dtype;

   input 	     clk;
   input 	     dtype i1;
   output 	     dtype o1;

   always @(posedge clk)
     begin
	o1 = i1;
	o1[1] = i1[2];
     end
endmodule
