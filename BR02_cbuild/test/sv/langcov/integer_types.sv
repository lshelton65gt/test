// This is a sanity test for all "integer types" as defined in
// IEEE P1800-2012, 6.11


module top (

   // inputs
   input shortint a,
   input int b,
   input longint c,
   input byte d,
   input bit [31:0] e,

   // outputs
   output shortint oa,
   output int ob,
   output longint oc,
   output byte od,
   output bit [31:0] oe,

   // All outputs
   output logic [154:0] all // long enout to hold all outputs + 3
);

assign oa = a + 16'h7FFF;
assign ob = b + 32'hdeadbeef;
assign oc = c + a + b;
assign od = d ^ 8'hcc;
assign oe = e ^ 32'hffff0000;

// terms of a concat are self determined so this assign will 
// show that the populated size is correct for each term.  
// The 3'b101 on the left will be lost if the population creates
// values that are too wide.
assign all = {3'b101, oe, od, oc, ob, oa};

endmodule : top
