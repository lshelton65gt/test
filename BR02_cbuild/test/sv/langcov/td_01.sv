// filename: td_01.sv
// Description:  This test duplicates a problem seen at Customer1, in particular bad recovery from unsupported construct (typedef reference)


typedef logic [7:0] S_t;

module td_01(input clock, input [7:0] in1, in2, output reg [7:0] out1);

   

   function S_t f0;
      input   b1;
      input   S_t i1;
      begin
	 f0 = i1;
	 if ( b1 ) f0[3] = i1[3]^i1[4];
      end
   endfunction


   function S_t f1;
      input   b1;
      input   S_t i1;
      begin
	 f1 = f0(b1, i1);
      end
   endfunction


   
   always @(posedge clock)
     begin: main
       out1 = f1(in1[0], in2);
     end
endmodule
