// This tests SystemVerilog for loops. 
// IEEE P1800-1200 12.7
// The following cases are tested
//
// 1) initializer declarations {for (int i = 0; ...}
// 2) nested for loops
// 3) serial for loops using the same name for the
//    initializer declaration (each for loop requires
//    it's own scope).
// 4) Multiple initializers
// 5) No initializers

module top (input wire [15:0] a, input wire clk, output wire [15:0] b);

reg [15:0] temp;

always @(posedge clk)
   begin
     integer k;
     // integer declarations and initialization in an unnamed block
     reg [15:0] temp1;
     reg [15:0] temp2;

     // Test nested for loops with iteger declarations
     k = 0;
     for (integer i = 0; i < 4; i = i + 1)
           for (integer j = 0; j < 4; j = j + 1, k = k + 1)
              temp1[k] = a[15-k];

     // Serial loop using same index name (different type), 
     // and using int
     for (int i = 15; i >= 0; i = i - 1)
           temp2[i] = temp1[15-i];


     // Test labelled loop, with multiple declarations in 
     // the initializer,   
     // one of them shadowing the declaration of k above.
     // NOTE: Vcs doesn't seem to like the loop label.
     // Modelsim is ok with it.
     forlooplabel: for (integer j = 0, k = 0; j <= 3; j = j + 1, k = 0)
        begin
           // Null initializers are illegal in P1800-1200,
           // but vcs accepts them. Warning in Verific.
           for (;k <= 3; k = k + 1) 
              temp[j*4 + k] = temp2[15 - (j*4 + k)];
        end
   end

assign b = temp;

endmodule : top
