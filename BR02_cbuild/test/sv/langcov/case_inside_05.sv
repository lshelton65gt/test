
module case_inside_05(clk, a, i, b);

input clk;
input [2:0] a;
input i;
output reg [2:0] b;

initial b = 0;

 always @ (posedge clk)
 begin
   b = a;
   case(b) inside
      i, [1:2]: $display("Case inside variable range i, constant range 1 to 2");
      5  : $display("Case inside 5");
      6  : $display("Case inside 6");
      default: $display("default case");
   endcase
 end

endmodule
