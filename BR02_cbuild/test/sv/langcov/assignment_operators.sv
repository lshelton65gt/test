// This tests assignment operators.
// IEEE P1800-2012 LRM, 11.4.1

module top (
   input wire clk,
   input wire [7:0] i1, 
   input wire [3:0] op, 
   output wire [15:0] o
);

reg [7:0] o_buf;

reg [7:0] operand = 8'b10101111;

always @(posedge clk)
  begin
     o_buf = i1;
     case (op)
        4'b0000: o_buf += operand;
        4'b0001: o_buf -= operand;
        4'b0010: o_buf *= operand;
        4'b0011: o_buf |= operand;
        4'b0100: o_buf &= operand;
        4'b0101: o_buf %= operand;
        4'b0110: o_buf ^= operand;
        4'b0111: o_buf <<= operand;
        4'b1000: o_buf >>= operand;
        4'b1001: o_buf /= operand;
        4'b1010: o_buf <<<= operand;
        4'b1011: o_buf >>>= operand;
        default: o_buf = operand;
     endcase
  end

assign o = o_buf;

endmodule : top


