// SystemVerilog supports variable declarations with initializers in unnamed blocks.
// Verilog 2001 did not allow this. It did seem to allow declarations in named blocks,
// but initializers were not supported.

module top (input wire [31:0] a, input wire [31:0] b, output wire [31:0] c);

integer temp_a, temp_b;

always @(*)
begin 
  integer temp1 = 0;
  if (a > 32'hffffff)
     begin   
        integer temp2 = 32;
        temp_a = temp1;
        temp_b = a;
     end
  else 
     begin
        temp_a = a;
        temp_b = b;
     end
end

assign c = temp_a + temp_b;

endmodule

