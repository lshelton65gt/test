// file: dot_name_port_connect.sv
// This tests SystemVerilog implicit name port connections,
// IEEE 1800-2012 23.3.2.3
module top(input wire a, output wire b);

   bottom bottom_u1(.a, .b);

endmodule

module bottom(input wire a, output wire b);

   assign b = ~a;

endmodule
