// filename: function_port_is_mem.sv
// Description:  This test duplicates a problem seen at Customer 1. The problem
// is caused by fact that function input is a memory (1packed 1 unpacked) and it
// was populated without the correct 2D flags


module function_port_is_mem(input clock, input [7:0] in1, in2, output reg [255:0] out1);
function [255:0]    function1 ;
   input [7:0]    input_bits [0:31] ;
   reg   [255:0]  temp ;
   integer        t_1, t_2, t_3, t_4 ;
   begin
     for (t_1=0; t_1<16; t_1=t_1+1)
       begin
       t_3 = (t_1 > 7) ? 0 : 1;
       t_4  = (t_1 > 7) ? (t_1-8) : (t_1);
       for (t_2=0; t_2<16; t_2=t_2+1)
         temp[(t_1*16)+t_2] = input_bits[(t_3*16)+t_2][t_4];
      end
     function1 = temp;
   end
endfunction

   reg [7:0]    mem1 [0:31] ;

   always @(posedge clock)
     begin: main
	mem1[in1] = in2;
	out1 = function1(mem1);
     end
endmodule
