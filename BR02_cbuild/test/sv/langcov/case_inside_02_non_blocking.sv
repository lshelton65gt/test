
module case_inside_02_non_blocking(clk, a, b);

input clk;
input [2:0] a;
output reg [2:0] b;

initial b = 0;

 always @ (posedge clk)
 begin
   // check non-blocking assignment
   b <= a;
   case(b) inside
      [0:3]: $display("Case inside 0 to 3");
      4  : $display("Case inside 4");
      5  : $display("Case inside 5");
      default: $display("default case");
   endcase
 end

endmodule
