// Simple test for genvar variable declared inside a
// for loop (with auto increment).
module top(input wire [2:0] a, output wire [2:0] b);


for (genvar i = 0; i < 3; i++)
   begin
      bottom u (a[i], b[i]);
   end

endmodule : top

module bottom (input wire a, output wire b);

assign b = a;

endmodule
