// file: while.sv
// Tests a function as the only statement in a while loop body.
// This requires special processing by the population code.
module top (a, b);

   input wire [15:0] a;
   output wire [15:0] b;
   logic [15:0]         temp_b = 0;
   
   function [15:0] increment(input logic [15:0] i);
      increment = i + 3;
   endfunction

   always @(*)
     begin
        temp_b = a;
        while (temp_b < 12)
          temp_b = increment(temp_b);
      end        

   assign         b = temp_b;

endmodule
