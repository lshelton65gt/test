
module case_inside_04(clock, a, b);

input clock;
input [2:0] a;
output reg [2:0] b;

initial b = 0;


always @(posedge clock)
begin
    b = a;
    priority case (b) inside
    1, 3 : $display("values: 1 and 3"); // matches 'b001 and 'b011
    3'b0?0, [4:7]: $display("values: 4 to 7"); // matches 'b000 'b010 'b0x0 'b0z0
    endcase // 'b100 'b101 'b110 'b111
    // priority case fails all other values including  'b00x 'b01x 'bxxx
end

endmodule

