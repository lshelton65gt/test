// Test hierarchical references to variables declared inside generate blocks.
module top #(parameter N = 2) (
   input wire [N:0] in1, 
   input wire [N:0] in2, 
   output wire [N-1:0] out1,
   output wire [N-1:0] out2
);

   reg [N:0] a [N-1:0];      

   always @(in1, in2)
      begin
         a[0] = in1;
         a[1] = in2;
      end

   for (genvar i = 0; i < N; i++) begin: CKEPD
     for (genvar j = 0; j < N; j++) begin: CKEPE   
      reg [1:0] var1;
      always_comb begin
         var1 = {a[i][j], a[j][2]};
      end // always
      end // generate
   end // generate

   assign out1 =  CKEPD[0].CKEPE[0].var1 ^ CKEPD[0].CKEPE[1].var1;
   assign out2 =  CKEPD[1].CKEPE[0].var1 ^ CKEPD[1].CKEPE[1].var1;     
endmodule
