// This tests auto increment/decrement in a for loop.
// LRM P1800-2012 11.4.2
//

module top (
input wire [7:0] a,
input wire [7:0] b,
output wire [7:0] c
);

integer i;
reg [7:0] temp_c;

always @(a, b)
begin
    temp_c = a;
    for (i = 0; i < 3; i++)
        temp_c = temp_c + b;
    for (i = 3; i > 0; i--)
        temp_c = temp_c + b;
end

assign c = temp_c;

endmodule : top
