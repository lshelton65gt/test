// filename: test_inc02.sv
// Description:  This test duplicates a problem seen at Customer1.  
// the issue is that the `define for MYWIDTH is not visible  in the second file test_inc02_mid.sv
// the problem is that we were compiling with SFCU as the default (which is the
// default for systemverilog).

`define MY_WIDTH 8


module test_inc02(input clock, input [(`MY_WIDTH-1):0] in1, in2, output reg [(`MY_WIDTH-1):0] out1);

   test_inc02_mid u1(clock,  in1, in2, out1);
   
endmodule
