// filename: /home/cds/cloutier/Wrushc_BR01_02/Experiments/wire_02.v
// Description:  This test duplicates a problem seen at Customer1,
// an undeclared wire (created within a generate loop) is on lhs of a continuous assign
// current result is CARBON INTERNAL ERROR
// this is a verilog (v2k) version of wire_01.sv

module wire_02(clock, in1, in2, out1);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   output [7:0] out1;

   genvar g0;
   
generate if(1) begin : gen_loop
   for( g0=0; g0<2;g0 = g0 + 1)
     begin : u_gen_loop
	if (g0 > 1) begin : g1
	end else begin
	   assign one_bit = in1>in2;  // this is the undeclared wire that is causing the CARBON INTERNAL ERROR 
	   sub u_sub(in1[g0], one_bit, out1[g0]);
	end
     end
end
endgenerate
   
endmodule

module sub(i1, i2, o1);
   input i1, i2;
   output o1;
   reg 	  o1;
   always @(*)
     begin
	o1 = i1 ^ i2;
     end
endmodule
