// filename: case_inside_01.sv
// Description:  This test duplicates a problem seen at customer 1, ends in a CARBON INTERNAL ERROR


module case_inside_01(input [11:8] in4, output reg [15:0] out1, out2, out3, out4, input in0);
   parameter CLIENT_PORTS = 2;


    always @*
      begin //{
	out3 = 0;
	out2= 0;
	out1     = 0;
	out4= 1'b0;

	case (in4[11:8]) inside
	  [0:1]  : out1[(in4[11:8])]         = in0;
	  [2:3]  : out2[(in4[11:8])-2]    = in0;
	  [4:4+CLIENT_PORTS]  : out3[(in4[11:8]-4)]  = in0;
	  default: out4    = in0;
	endcase
     end //}

endmodule
