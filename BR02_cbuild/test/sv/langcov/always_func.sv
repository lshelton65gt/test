// file: always_func.sv
// purpose: this tests various combinations of the always construct with
// nested function calls, and missing statements. These present special
// cases for the Verific flow that were not being handled correctly.

module top 
(
input wire clk, 
input wire [7:0] din1, 
output reg [7:0] dout1,
input wire [7:0] din2, 
output reg [7:0] dout2,
input wire [7:0] din3,
output reg [7:0] dout3
);

   function [7:0] increment;
      input [7:0] din;
      begin
         increment = din + 1;
      end
   endfunction

   // A begin/end has to be inserted to handle
   // the function processing.
   always @(posedge clk)
       dout1 <= 2 + increment(din1);

   // Ditto, begin/end required.
   always_comb
       dout2 <= increment(din2);

   // This was causing an assertion.
   // It should, instead, warn about an empty always.
   always @(posedge clk);

   // No begin/end required here
   always @(posedge clk)
       dout3 <= din3;

endmodule
