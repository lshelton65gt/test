// This tests shadowing of a declaration in an unnamed begin/end
// block.
//
// Currently, this test takes an assertion in cbuild.

module top (input wire din, input wire clk, output wire dout);

reg temp;

// Shadows declaration in always block
wire foo;

always @(posedge clk)
begin
   integer foo;
   foo = din;
   temp = foo;
end

assign foo = temp;
assign dout = foo;

endmodule : top
