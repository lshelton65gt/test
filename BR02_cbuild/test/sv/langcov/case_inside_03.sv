
module case_inside_03(clk, a, b);

input clk;
input bit [2:0] a;
output bit [2:0] b;

assign b = a;

always @ (posedge clk)
    begin
    case(a) inside
       0,1: $display("0 or 1");
       2: $display("2");
       4: $display("4");
    endcase
end

endmodule
