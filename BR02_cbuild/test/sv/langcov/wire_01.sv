// filename: /home/cds/cloutier/Wrushc_BR01_02/Experiments/wire_01.sv
// Description:  This test duplicates a problem seen at Customer1,
// an undeclared wire (created within a generate loop) is on lhs of a continuous assign
// current result is CARBON INTERNAL ERROR


module wire_01(input clock, input [7:0] in1, in2, output reg [7:0] out1);

generate if(1) begin : gen_loop
   for(genvar g0=0; g0<2;g0++)
     begin : u_gen_loop
	if (g0 > 1) begin : g1
	end else begin
	   assign one_bit = in1>in2;  // this is the undeclared wire that is causing the CARBON INTERNAL ERROR 
	   sub u_sub(in1[g0], one_bit, out1[g0]);
	end
     end
end
endgenerate
   
endmodule

module sub(input i1, i2, output reg o1);
   always @(*)
     begin
	o1 = i1 ^ i2;
     end
endmodule
