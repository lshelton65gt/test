module binary_op_inside(clock, in1, in2, in3, out1) ;
input clock;
input [2:0] in1, in2, in3;
output [2:0] out1;
reg [2:0] out1;

always @(posedge clock)
begin: main
    if ( in1 inside {in2, in3} )
        out1 = in1 & in2;
end

endmodule
