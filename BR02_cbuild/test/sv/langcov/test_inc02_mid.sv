// filename: test_inc02_mid.v
// Description:  see test_inc02.sv for comments (note the .sv extension)


module test_inc02_mid(clock, in1, in2, out1) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   test_inc02_bot #(.WIDTH(`MY_WIDTH)) u1(clock, in1, in2, out1) ;

endmodule

module test_inc02_bot(clock, in1, in2, out1) ;
   parameter WIDTH = 4;
   input clock;
   input [(WIDTH-1):0] in1, in2;
   output [(WIDTH-1):0] out1;
   reg [(WIDTH-1):0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule
