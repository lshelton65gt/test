
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: : NOT SYNTHESIZABLE POSITIVE TESTCASE (UNCLEAR)

// TESTING FEATURE(S): Test that static data declared inside a function/task
// can be accessed by hierarchical name from outside, but automatic 
// data can not be accessed.

module test(auto_out, static_out);
    parameter p = 8;
    output int auto_out [0: p-1], static_out [0:p-1];

    task automatic my_task;
        output int result;
        input int num;
        static int static_result;
        int tmp;

        begin
            if(num == 0)
            begin
                result = 1;
                static_result = 1;
            end
            else
            begin
                my_task(tmp, num -1);
                result = num * tmp;
                static_result = num * tmp;
            end
        end
    endtask

    generate
        genvar i;
        for(i = 0; i < p; i = i +1)
        begin : b
            int tmp;
            assign tmp = i;
            
        initial
            begin
                my_task(auto_out[i], i);
                static_out[i] = my_task.static_result; 
            end
        end
    endgenerate

endmodule

