
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE (POTENTIALLY POSITIVE - UNCLEAR ) TESTCASE

// TESTING FEATURE(S): Checking equality operations on an object array or slice
// of the object array, the object being instantiation of different classes.

module test ;

    class Student ;
    //////////Properties///////
        string name ;
        int roll ;
        string address ;
    //////////Methods//////////
        function new ;
            name = "" ;
            address = "" ;
            roll = 10 ;
        endfunction

        function void setdata(string n, int r, string a) ;
            name = n ;
            roll = r ;
            address = a ;
        endfunction

        function string getname() ;
            getname = name ;
        endfunction
    
        function string getaddress() ;
            getaddress = address ;
        endfunction
    
        function int getroll() ;
            getroll = roll ;
        endfunction
    endclass

    class Album ;
        string singer ;
        string producer ;
        int cost ;
        int total_number_of_songs ;
    
        function new(string s, string p, int c, int t) ;
            singer = s ;
            producer = p ;
            cost = c ;
            total_number_of_songs = t ;
        endfunction
    
        function void show ;
            $display("%s, %s, %d, %d", singer, producer, cost, total_number_of_songs) ;
        endfunction
    endclass

    Album a[4] ;
    Student s[4] ;

    initial begin
        s[0] = new ;
        s[1] = new ;
        s[2] = new ;
        s[3] = new ;

        if (a == s)
             $display(" Album and Student are same") ;
        if (a != s)
             $display(" Yes, Album and Student are different things") ;
    end

endmodule

