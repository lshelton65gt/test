
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE ( POTENTIALLY POSITIVE - NEGATIVE) TESTCASE

// TESTING FEATURE(S): Equality operations on the array or slice of the array, the
// arrays are of a parent and child class.

module test ;

    class Student ;
    //////////Properties///////
        string name ;
        int roll ;
        string address ;
    //////////Methods//////////
        function new ;
            name = "" ;
            address = "" ;
            roll = 10 ;
        endfunction

        function void setdata(string n, int r, string a) ;
            name = n ;
            roll = r ;
            address = a ;
        endfunction

        function string getname() ;
            getname = name ;
        endfunction
    
        function string getaddress() ;
            getaddress = address ;
        endfunction
    
        function int getroll() ;
            getroll = roll ;
        endfunction
    endclass

    class Batch extends Student;

        int year ;
        int total_number;
    
        function new(int y, int t) ;
            year = y ;
            total_number = t ;
        endfunction
    
        function void show ;
            $display("%d, %d", year, total_number) ;
        endfunction

    endclass

    Batch b[4] ;
    Student s[4] ;

    initial begin
        s[0] = new ;
        s[1] = new ;
        s[2] = new ;
        s[3] = new ;

        if (b == s) 
             $display(" Album and Student are same") ;
        if (b != s)
             $display(" Yes, Album and Student are different things") ;
    end

endmodule

