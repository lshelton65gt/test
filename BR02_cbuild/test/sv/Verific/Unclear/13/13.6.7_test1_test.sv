
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE (UNCLEAR)

// TESTING FEATURE(S): The following design defines a function which generates
// and returns a queue of random numbers in the range given by its arguments.

// NOTE : The testcase, cited as an example in the LRM is possibly wrong. As
// function declaration does not support array or queue return types.

module test (input in, output bit [0:2] out) ;

    function int [$] GenQueue (int low, int high) ;
        int [$]  q ;
        randsequence (main) 
            TOP : BOUND (low) LIST BOUND (high) ;
            LIST : LIST ITEM := 8 { q = {q, ITEM } ; }
                   | ITEM := 2 { q = {q , ITEM } ;}
                   ;
            int ITEM : { return $urandom_range (low, high) ;} ;
            BOUND (int b) : {q = {q, b} ; } ;
        endsequence
        GenQueue = q ;
    endfunction
    
    assign out = in ;

endmodule
