
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE (UNCLEAR)

// TESTING FEATURE(S): SystemVerilog allows specifying array of data type for formal
// arguments of a function. This design defines a function and defines input and
// output type formal argument as arrays.

// Note : According to LRM of P1800(5.2) an unpacked array can be assigned to another unpacked
// type provided they are of the same type and dimension. But it is not very clear as to whether 
// an unpacked array being returned from a function can be assigned to another unpacked array.

module test #(parameter width=8)
             (input clk,
              input in1[width-1:0], in2[width-1:0], 
              output reg out1[width-1:0], out2[width-1:0]);

logic u[width-1:0];
logic v[width-1:0];
logic x[width-1:0] = in1;
logic y[width-1:0] = in2;
logic z[width-1:0];

always @(posedge clk)
begin
    u = func1(x, y, v);
    out2 = func1(u, v, z);
end

typedef bit unpacked_array [width-1:0] ;
function unpacked_array func1(input logic a [width-1:0], b [width-1:0], output logic c [width-1:0]);
    bit  t1[width-1:0], t2[width-1:0], r[width-1:0];

    for(int i=width-1; i>=0; --i)
    begin
        t1[i] = a[i] * b[width-1-i];
        t2[i] = b[width-1-i] | a[i] - t1[i];
        r[i] = t1[i] ^ t2[i];
    end

    return r;
endfunction

endmodule

