
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE (POTENTIALLY NEGATIVE - UNCLEAR)

// TESTING FEATURE(S): Testing of parameters passing void data type.

module test #(parameter type p = int)(input wire [31:0] x, y, output int v, z);

function p swap(inout int a, b);
    a = a + b;
    b = a - b;
    a = a - b;
endfunction

int a, b;

always@(x or y)
begin
    a = x;
    b = y;

    swap(a, b);

    v = a;
    z = b;
end

endmodule

module bench;
int a, b;
wire [31:0] c, d;

test #(.p(void)) instanMod1(a, b, c, d);

defparam instanMod1.p = int;

initial
begin
    $display("Tool should give a warning");
    a = 5;
    b = 10;
    repeat(20)
    begin
        a += 5;
        b -= 5;
    end
    $monitor("%d %d \n", a, b, c, d);
end

endmodule

