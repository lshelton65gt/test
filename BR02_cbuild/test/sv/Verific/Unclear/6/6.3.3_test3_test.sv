
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE (POTENTIALLY NEGATIVE - UNCLEAR)

// TESTING FEATURE(S): Testing for the warning/error which should be issued when a
// type parameter initialised  and used as non-void data-type is changed to void type.

module test(input int in, output int out);
parameter type p = int;

function automatic p func(input int in);
    if(in == 0)
        return 1;
    else
        return (in * func(in-1));
endfunction

initial
    out = func(in);
endmodule 

module bench;
int in, out;

test #(.p(void))instanMod1(in, out);
defparam instanMod1.p = void;
initial
begin
    in = 5;
    $display("out = %d", out);
end
endmodule

