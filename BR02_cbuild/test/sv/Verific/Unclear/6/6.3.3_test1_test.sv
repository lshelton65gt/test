
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE (UNCLEAR)

// TESTING FEATURE(S): Use enumeration type to override parameter values.

module test (output int res, input int in1, in2) ;

    parameter option = 2 ;

    arith_op #(.p(enum { ADD, SUB, MULT, DIV, POW }))
                            I(res, in1, in2, option) ;

endmodule

module arith_op(output int res, input int in1, in2, option) ;

    parameter type p = int ;

    p op_option ;

    always @(in1, in2, option)
    begin
        case(option)
            0 : op_option = ADD ;
            1 : op_option = SUB ;
            2 : op_option = MULT ;
            3 : op_option = DIV ;
            default: op_option = POW ;
        endcase

        case(op_option)
            ADD : res = in1 + in2 ;
            SUB : res = in1 - in2 ;
            MULT : res = in1 * in2 ;
            DIV : res = in1 / in2 ;
            POW : res = in1 ** in2 ;
        endcase 
    end

endmodule

