#!/bin/bash
# This script is based on sandbox/test/write-test.run.template, see that file for additional comments
#
# NOTE: this must be run with bash (Not sh, csh, tcsh...)
#
#   This is the script that writes a new   'test.run'  command
#    file for the tests in this directory
#
#   This is run by the test harness *every* time the runinplace is
#   run.  It can be invoked like this:
#               ./write-test.run  >  test.run
#   or
#               ./write-test.run -quick >  test.run
#

                ###
                ###   Define the test lists:
                ###     Note that it is only necessary to define the lists you 
                ###     want to use, undefined lists are handled as if they 
                ###     were empty lists.
                ###

# Do check for CARBON INTERNAL ERRORS for this directory
DO_CHECK_FOR_CARBON_INTERNAL_ERROR=1

LIST_OF_KNOWN_CARBON_INTERNAL_ERRORS="\
"

                ### NOTUSED  tests that we don't care about, they have constructs we
                ### aren't going to implement, or the test is in some other way unusable.
                ###

# These tests have no top level ports, but would
# otherwise be good tests.
NOTUSED="\
"

# These tests have no top level ports, and don't
# look like they'd be suitable.
NOTUSED="$NOTUSED \
"
                ###   XPASS       tests that pass cbuild and simulation
                ###   
XPASS="\
4.9_test1_test \
"

                ###   XPASSNOSIM  tests that pass cbuild and NO simulation run
                ###   

XPASSNOSIM="\
"

                ###   XPEXIT      tests that are in PEXIT state for cbuild,
                ###               and no simulation needed.
                ###
XPEXIT="
4.11_test1_test \
"

                ###   XTDIFF      tests that are in TDIFF state for cbuild
                ###               and no simulation needed.
                ###   
XTDIFF="\
"

                ###   XTEXIT      tests that are in TEXIT state for cbuild
                ###               and no simulation needed.
                ###   
XTEXIT=""

                ###   XTNOTEXIT   tests where cbuild should fail but incorrectly 
                ###               passes today.  No simulation run.
                ### 
XTNOTEXIT=""

                ###   XTEXITDIFF tests that are in TEXIT and TDIFF state for cbuild
                ###   (non-zero cbuild exit, and diffs in cbld.log.gold file, 
                ###   no simulation needed)
                ###

# Most of these tests take a CARBON INTERNAL ERROR,
# or have memory leaks
XTEXITDIFF="\
"

                ###  XTEXITBACKEND tests that are in TEXIT state only when backend
		###                compile is run (usually this is a backend compile TEXIT)
		###                These tests will always run codegen and backend compile
		###                even if -quick is specified.

XTEXITBACKEND=""


                ###   XSIMPEXIT   tests that pass cbuild but PEXIT simulation
                ###   
XSIMPEXIT=""

                ###   XSIMPEXITDIFF   tests that pass cbuild but PEXIT 
                ###                   simulation and PDIFF the results
                ###   
XSIMPEXITDIFF=""

                ###   XSIMTEXIT   tests that pass cbuild but TEXIT simulation
                ###   
XSIMTEXIT="\
"

                ###   XSIMTEXITDIFF tests that pass cbuild but TEXIT 
                ###                 simulation and TDIFF those results
                ###   

# These tests cause assertions in ./design.exe
XSIMTEXITDIFF="\
"

                ###   XSIMPDIFF   tests that pass cbuild but PDIFF simulation
                ###   
XSIMPDIFF=""

                ###   XSIMTDIFF   tests that pass cbuild but TDIFF simulation
                ###   

# These tests produces wrong simulation results.
XSIMTDIFF="\
"

                ###   XFSDBTEXIT   tests that pass cbuild and simulation but TEXIT fsdb
                ###   
XFSDBTEXIT=""

                ###   XFSDBTDIFF   tests that pass cbuild and simulation but TDIFF fsdb
                ###   
XFSDBTDIFF=""

                ###   XFSDBTEXITDIFF   tests that pass cbuild and simulation but TEXIT and TDIFF fsdb
                ###   
XFSDBTEXITDIFF=""

                ###
                ###   Optionally define any extra command line options for the
                ###   cbuild command, if undefined then none will be used.
                ###

TRAILINGCBUILDOPTS=""

                ###
                ###   Optionally define the cbuild command line to be used for
                ###   all builds, if undefined then "cbuild" will be used
                ###

CBUILD="cbuild -q -useVerific -sverilog"

                ###
                ###   Define default command line arguments to cdsDiff
                ###   Applies to all tests in specified in the write-test.run file
                ###   To replace the default options for a test, use <root>.cdsDiffOpts.
                ###

DEFAULTCDSDIFFOPTS="-cbld"

                ###
                ###   Define default command line arguments to xz-diff
                ###   Applies to all tests in specified in the write-test.run file
                ###   To replace the default options for a test, use <root>.xzDiffOpts.
                ###

DEFAULTXZDIFFOPTS=""

                ###
                ###   Define default command line arguments to the nCompare script
                ###   Applies to all tests in specified in the write-test.run file
                ###   To replace the default options for a test, use <root>.nCompareOpts.
                ###

DEFAULTNCOMPAREOPTS=""

                ###
                ###   Optionally define TESTSUFFIX as a string that will be
		###   appended to the testname to define the names of all output
		###   files.  if undefined then no suffix is used.
                ###
#TESTSUFFIX=""

                ###
                ###   Optionally define TESTOUTPUT as a non-zero value
		###   and each cbuild test will have an output specification of
		###   -o lib${Test}${TESTSUFFIX}.a
		###   if this variable is not defined then cbuild will use 
                ###   -o libdesign${TESTSUFFIX}.a  (multiple tests write to same directory)
		###   and if TESTSUFFIX is not defined then all cbuild commands
		###   will write to the default: libdesign.a
		###
TESTOUTPUT=1




############# below this line you should not need to modify this file

# pick up common routines, and write tests for standard variable lists
source $CARBON_HOME/test/write-test.common
f_writeTestsForStandardLists "$@ $TRAILINGCBUILDOPTS"
