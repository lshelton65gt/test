
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE (POTENTIALLY POSITIVE - UNCLEAR)

// TESTING FEATURE(S): This design forward declare a new type twice. The actual
// definition is given at the end. This is an error. Same thing can not be forward
// defined more than once.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    typedef b_t;

    typedef b_t;

    always@(posedge clk)
    begin
        b_t t1, t2;

        t1 = in2 - in1;
        t2 = in1 - in2;

        out1 = t2 - ~t1;
        out2 = t1 - ~t2;
    end

    typedef bit [width-1:0] b_t;

endmodule

