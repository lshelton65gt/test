
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE (Structure literal usage without casting - UNCLEAR)

// TESTING FEATURE(S): Testing whether non-constant values can be passed to structures.

typedef struct {
                 int a[3:0] ;
                 bit [3:0]sel ;
               } myType ;

function myType func ;
    input int s[3:0] ;
    input bit[3:0] funcBit ;
    myType var1 ;
    var1 = '{s[3:0], funcBit} ;
    return var1 ;
endfunction

module mod(var1) ;

    input myType var1 ;
    int b[3:0] ;
    bit [3:0]modBit2 ;

    int a[3:0] ; 
    bit [3:0]modBit1 ;

    assign a = var1.a ;
    assign modBit1 = var1.sel ;
    myType obj ;

    initial
    begin
         obj = func(a[3:0], modBit1) ;
    end

endmodule


module test ;

    int x[3:0] ;
    bit [3:0]y ;

    initial
    begin
        y = '1 ;
        repeat(20)
        begin
            for(int i = 3 ; i >=0  ; i--)
            begin
                x[i] = $random ;
                #5 $monitor("input int %d = output int %d", x[i], y[i]) ;
            end
            y = ~y ;
        end
    $monitor("input bit %b = output bit %b", y, instanMod.modBit2) ;
    end

mod instanMod('{x[3:0], y}) ;

endmodule

