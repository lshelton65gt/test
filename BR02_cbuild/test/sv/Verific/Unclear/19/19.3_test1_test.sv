
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE (Use of $root - UNCLEAR)

// TESTING FEATURE(S): This is the other file of the set which makes the $root scope.
// A module is defined in this file. Inside the module a function
// with the same name as that of a function defined in the global
// $root scope is defined. This is called from the module and the
// other function is called using hierarchical name.

`include "test1.v"

module test ;

    function void f1 ;
        $display ("function f1 defined in module m\n") ;
    endfunction

    initial
    begin
        $root.f1() ;
        f1() ;
    end
    
endmodule

