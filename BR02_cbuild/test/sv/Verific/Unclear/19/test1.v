
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE (Use of $root - UNCLEAR)

// TESTING FEATURE(S): This is one of the files of the set which makes the $root
// scope. A function is defined in this file. This is called from a module in the
// other file. A function of same name is defined in that module, so hierarchical
// naming is used to call this one.

function void f1;
	$display ("$root function f1\n");
endfunction

