
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE (POTENTIALLY POSITIVE - UNCLEAR)

// TESTING FEATURE(S): Property cannot be instantiated in a sequence. This design
// instantiates a property from within a sequence. This is an error.
// NOTE: Can't find the objective in IEEE-1800 LRM!

module test (input clk, sig1, sig2, sig3, sig4) ;

    sequence s2;
        sig3 ##[1:3] sig4;
    endsequence

    property p1;
        @(posedge clk) sig1 && sig2 |=> s2;
    endproperty

    sequence s1;
        @(posedge clk) sig1 ##[1:3] p1;
    endsequence

    a1: assert property (p1);
    a2: assert property (s1);
    c1: cover property (p1);
    c2: cover property (s1);

endmodule

