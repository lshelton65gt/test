
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE (POTENTIALLY NEGATIVE - UNCLEAR)

// TESTING FEATURE(S): $insetz(...) function, returns true if the first expression is equal
// to at least other expression argument. The comparison is performed using casez semantics,
// so `z' or `?' bits are treated as don't-cares. Here, it does not match, so it returns false.

module test (input [7:0] data, checking, input clk, output [7:0] data_out) ;

    sequence seq1 ;
         $insetz(data, checking) ;
    endsequence 

    property header ;
         @(posedge clk) seq1 ;
    endproperty     

    assert property(header) ;

    assign data_out = data ;

endmodule

