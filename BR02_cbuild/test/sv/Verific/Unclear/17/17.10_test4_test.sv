
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE (POTENTIALLY NEGATIVE - UNCLEAR)

// TESTING FEATURE(S): This design uses $inset system function. $inset(expr, expr, expr)
// returns true if the first expression is equal to at least one of the subsequent
// expression arguments.

module test (input [7:0] data, checking, input clk, output [7:0] data_out) ;

    sequence seq1 ;
         $inset(data, checking) ;
    endsequence 

    property header ;
         @(posedge clk) seq1 ;
    endproperty     

    assert property(header) ;

    assign data_out = data ;

endmodule

