
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE (POTENTIALLY NEGATIVE - UNCLEAR)

// TESTING FEATURE(S): $inset(...) function, returns true if the first expression is equal
// to at least one of the subsequent expression arguments, here, it does not match, so it
// returns false

module test (input [7:0] data, input clk, output [7:0] data_out) ;

    wire [7:0] checking = 1010111 ;

    sequence seq1 ;
         $inset(data, checking) ; 
    endsequence 

    property header ;
         @(posedge clk) seq1 ;
    endproperty     

    assert property(header) ;

    assign data_out = data ;

endmodule

