
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): To test the 'foreach' construct.
// The type of 'foreach' loop variable is implicitly declared to be 
// consistent with the type of array index. It shall be an error
// for any loop variable to have the same identifier as the array.

module test (input int k[3][3], bit clk, output bit out);
    int sum ;
    always @ (posedge clk)
    begin
        sum = 0 ;
        foreach( k[ k, m ] )
        begin
            k[k][m] = k * m;
            sum = sum + k[k][m] ;
        end

        if (sum == 1)
            out = 1'b0 ;
        else
            out = 1'b1 ;
    end
endmodule

