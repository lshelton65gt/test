
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Block name, if specified, after 'end' of a 'begin-end' block must
// be same. This design checks that.

module test;
int i = 0;
initial
begin : block1
	logic clk = 0;
	repeat (10) #5 clk = ~clk;
end : block2
always @ (block1.clk)
begin
	i++;
	$display("%d\n", i);
end
endmodule
