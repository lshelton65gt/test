
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses delay or event control expression in %= and &= blocking
// assignments. This type of assignments are illegal, only = and <= assignments
// can have delay or event control expression.

module test(clk, in1, in2, out);

parameter width = 8;

input clk;
input [width-1:0] in1, in2;
output reg [width-1:0] out;

reg [width-2:0] r1, r2;

initial
begin
    // legal event control in assignment
    r1 = @clk in1;
    r2 = @clk in2;

    // illegal delay in assignment
    r1 %= #5 in2;
    r2 &= #10 in1;

    // legal assignment
    out = r1 & r2;
end

endmodule

