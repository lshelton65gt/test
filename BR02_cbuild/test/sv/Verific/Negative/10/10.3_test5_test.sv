
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses delay or event control expression in <<= and >>= 
// blocking assignments. This type of assignments are illegal, only
// = and <= assignments can have delay or event control expression.

module test(clk, in1, in2, out);

parameter width = 8;

input clk;
input [width-1:0] in1, in2;
output reg [width-1:0] out;

automatic reg [width-1:0] r1, r2;

initial
begin
    // legal delay or event control in assignment
    r1 = @(clk) in1;
    r2 = #5 in2;

    // illegal delay or event control in assignment
    r1 <<= #10 (width/2);
    r2 >>= @(negedge clk) (width/2);

    // legal assignment
    out = r1 | r2;
end

endmodule

