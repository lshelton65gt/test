
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines loop control variable inside the loop. 
// So this variable becomes visible to only in the loop scope. Now use it outside 
// the loop. This is an error since the variable is not declared outside the loop.

module test (clk, i, o) ;
    parameter size = 8 ; 

    input clk ; 
    input [(size - 1):0] i ; 
    output reg [(size - 1):0] o ; 

    always@(posedge clk)
    begin
        // Define j to be inside the for-scope
        for (int j = 0 ; (j < size) ; j ++ )
            o[j] = i[((size - j) - 1)] ;

        // Use j outside of the for scope
        if ((j == size)) 
            o = ~(o) ;
    end
endmodule

