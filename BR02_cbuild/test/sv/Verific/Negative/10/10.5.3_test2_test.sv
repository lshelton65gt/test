
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): To test the 'foreach' construct. Using String datatypes.
// The type of 'foreach' loop variable is implicitly declared to be 
// consistent with the type of array index. It shall be an error
// for any loop variable to have the same identifier as the array.

module test (input string str_arr[5:0], bit clk, output bit out);
    int no_os_str ;

    always @ (posedge clk)
    begin
        no_os_str = 0 ;
        foreach( str_arr[ str_arr ] )    // Error: using array name as index
            no_os_str = no_os_str + 1 ;

        if (no_os_str == 6)
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

