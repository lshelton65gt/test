
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case with potential overlap in unique if.

module test;
int i, j;
initial
begin
	i = 0;
	j = 0;
	unique if ( i == 0)
		$display("i = %d", i);
	else if( i == 0 || j == 0 )
		$display("i = %d, j = %d", i, j);
end
endmodule
