
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses delay or event control expression in |= and ^= blocking
// assignments. This type of assignments are illegal, only = and <= assignments
// can have delay or event control expression.

module test(clk, in1, in2, out);

parameter width = 8;

input clk;
input [width-1:0] in1, in2;
output reg [width-1:0] out;

reg [width-1:0] r1, r2;

initial
begin
    // legal delay in assignment
    r1 = #(5ns:7ns:9ns) in1;
    r2 = #5ns in2;

    // illegal delay or event control in assignment
    r1 |= @(posedge clk iff r2) in2;
    r2 ^= @(negedge clk iff r1) in1;

    // legal assignment
    out = r1 - r2;
end

endmodule

