
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows a label to be specified before a statement. A label
// can not be used before end of a 'begin-end 'block. As this is not a statement
// it is part of another statement. This design defines a label before end in a
// 'begin-end' block.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = ~in1 + in2;
    r2 = in1 - ~in2;

    if (~r1 ~^ r2)
    begin
        out1 = ~r1 & r2;
    if_true: end
    else
    begin
        out1 = r1 + ~r2;
    if_false: end

    out2 = ~in1 - in2 | ~r1 & r2 + ~out1;
end

endmodule

