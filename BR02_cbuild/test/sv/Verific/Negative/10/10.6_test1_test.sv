
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The SystemVerilog 'break' statement can only be used inside a loop. This
// design uses the 'break' statement outside a loop, to check whether this
// is flagged as an error by the tool.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    if (^in1)
        r1 = in1 | in2;
    else
        r1 = in1 & in2;

    if (&in2)
        r2 = in1 - in2;
    else
        r2 = in1 + in2;

    if (r1 == r2)
        break;

    out1 = r1 * r2;
    out2 = r2 ^ r1;
end

endmodule

