
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): System Verilog does not allow assignment operator in expression. 
// This design tests use of '=' operator in event expression 

module test;
int i, j;
int result;
initial
begin
	i = 5;
	j = 10;
    #10 i = 25;
	j = 2;
    #10 i = 10;
	j = 3;
end

always @((result = i * j))
begin
	$display($time,,, "i = %d, j = %d, result = %d", i, j, result);
end
endmodule

