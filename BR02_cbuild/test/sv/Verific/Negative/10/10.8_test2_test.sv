
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog supports label declaration before a statement and a block name
// after a 'begin-end' or 'fork-join' block. But it is illegal to use both of them
// at the same time. This design defines a named 'begin end' block which has a 
// label also. This should be detected as an error by the tool.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = in2<<2 | in1>>2;
    r2 = in2>>2 & in1<<2;

    if (r1 >= r2)
    label1: begin: name1
        out1 = r1 ^ r2;
    end: name1
    else
    label2: begin: name2
        out1 = r2 - r1;
    end: name2

    out2 = out1 ^ r2 + in1;
end

endmodule

