
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The loop condition can be any expression which can be treated as a boolean. 
// This test case uses expression as condition which cannot be treated as a boolean expression.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = in1 - in2;
    r2 = r1 ^ in2;

    do
    begin
        r2 -= r1;
    end
    while ((r1<<=2))
end

endmodule

