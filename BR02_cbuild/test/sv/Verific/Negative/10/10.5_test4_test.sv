
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing hierarchical access to the for loop variable.

module test;
initial
begin: label
    for( int i = 0; i < 5; i++) $display(i);
end
always @(label.i)
	$display("%d\n", label.i);
endmodule

