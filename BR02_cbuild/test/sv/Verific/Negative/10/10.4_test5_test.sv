
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses unique keyword with casex statement. unique denotes that
// there is no overlapping in the conditions of casex statement, this design
// does not handles all the possible conditions and does not provide a default
// item, when unexpected value comes, it should generate a runtime warning.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1;
reg [0:p-1] r2;
reg [0:1] r3;

always @(posedge clk)
begin
    r1 = in1>>p/2 + in2<<p/2;
    r2 = r1<<p/2 | in1<<(3*p/4) | in2>>(3*p/4);
    r3 = r1[p/2:p/2-1] | { r2[p-1], r2[0] };

    unique casex (r3) // values 10 and 01 will generate runtime warning
    	2'b00:
        begin
            out1 = r1 | -r2;
            out2 = r2 & ~r2;
        end
    	2'b11:
        begin
            out1 = ~r2 ^ r1;
            out2 = -r1 + r2;
        end
    endcase
end

endmodule

