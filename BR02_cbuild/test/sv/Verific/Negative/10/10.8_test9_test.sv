
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows a matching block name to be specified for a 'fork-join'
// block after the 'join' keyword. If present both of them should be same.
// This design defines a named 'fork-join' block but having different names in
// two places.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = in1 & in2;
    r2 = in1 - in2;

    fork: begin_fork
        out1 = r1 + r2;
        out2 = r2 ^ r1;
    join: end_join
end

endmodule

