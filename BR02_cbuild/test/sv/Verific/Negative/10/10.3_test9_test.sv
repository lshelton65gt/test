
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case with assignment operators (other than =)
// having an event control statement on the right hand side.

module test;
int i, j;
initial
begin
	i = 0;
	j = 0;
   #5 	i = 1;
   #50  $finish;
end
always
begin
	j += (@ i) 5;
end
endmodule
