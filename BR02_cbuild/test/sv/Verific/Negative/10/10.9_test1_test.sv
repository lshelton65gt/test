
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Disabling a function is illegal in SystemVerilog, because the return value,
// then, will be undefined. This design defines a function and disables it to 
// check whether this is trapped as an error by the tool.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    for(int i=0; i<width; i++)
    begin
        r1 = dis_func(in1[i], in2[width-1-i]);
        r2 = dis_func(in2[i], in1[width-1-i]);
    end
    out1 = r1 + r2;
    out2 = r1 - r2;
end

always@(posedge clk)
begin
    for(int i=0; i<width; ++i)
        disable dis_func;
end

function automatic int dis_func(input int a, b);
    int t1, t2;

    t1 = a>>8;
    t2 = b<<8;

    return t1 ^ t2;
endfunction

endmodule

