
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The final block Test, it can call $finish
// Execution of $finish,  tf_dofinish(), or vpi_control(vpiFinish,...) from 
// within a final block shall cause the simulation to end immediately.
// In this test case delay control is used in final block, so it should be error

module test( input int p, output int q) ;

    assign q = p ;

    task ch() ;
       int result ;
       $display("first task called") ;
       for (int n = 0; n <= 7; n = n+1) begin
           result = fact(n);
           $display("%0d factorial=%0d", n, result);
       end
     endtask

    function automatic int fact(int n) ;
       $display("second function called") ;
       if (n >= 2)
              fact = fact (n - 1) * n;
       else
              fact = 1;
    endfunction

      final 
        begin
            #5 ch() ;
            $finish ;
            $display(" finish called implicitly") ;
        end

endmodule
        

module bench ;
    int p = 10, q ;
    reg clk ;

    test t1(p, q) ;

    initial begin
       q = 0 ;
       clk = 1 ;
    end

    initial 
        #10 $finish ;

    always
       #5 clk = ~clk ;

    always @ (posedge clk) begin
       p = $random ;
    end

    initial 
       $monitor("clk = %b, p = %d, q = %d", clk, p,q) ;

    final
       $display(" Last final block executed" ) ;

endmodule

