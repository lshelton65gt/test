
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case with assignment operators (other than =)
// having a delay control statement in the right hand side.

module test;
int i;
initial
begin
	i += #5 2;
end
endmodule
