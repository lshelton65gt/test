
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses non-blocking-assignments on automatic variables.
// These types of assignments are illegal. 

module test(clk, in1, in2, out);

parameter width = 8;

input clk;
input [width-1:0] in1, in2;
output reg [width-1:0] out;

task non_blocking ;
    // r1 and r2 are automatic variables. non-blocking assignments are illegal on them
    automatic reg [width-1:0] r1, r2;
    r1 <= in1 | in2;
    r2 <= r1 ^ in2 - in1;

    out <= r1 & r2;
endtask

endmodule

