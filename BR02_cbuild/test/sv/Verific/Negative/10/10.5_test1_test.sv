
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In SystemVerilog the loop control variable can be declared inside the loop
// itself. This design declares the loop control variable for a 'for-loop' inside
// the loop itself. The loop control variable declaration is equivalent to automatic
// variable declaration in an unnamed block. This design tries to access loop control
// variable using hierarchical name. As automatic variable can not be accessed by
// hierarchical name, this is also an error.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin: blk1
    r1 = in2;
    loop1 :for (integer i=0; i<width; i++)
    begin
        out1[i] = r1[i] | in2[i];
        blk2.loop2.j = blk2.loop2.j - 1;
    end
end: blk1

always @(posedge clk)
begin: blk2
    r2 = in2;
    loop2: for (integer j=0; j<width; j++)
    begin
        out2[j] = r2[j] & in1[j];
        blk1.loop1.i = blk1.loop1.i - 1;
    end
end: blk2

endmodule

