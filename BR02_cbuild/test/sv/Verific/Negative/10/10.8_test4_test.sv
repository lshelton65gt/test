
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows a label to be specified before a statement. A label
// can not be specified before join of a fork join block. As this is not a 
// statement by itself, it is part of another statement. This design defines
// a label before join in a fork join block.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = in1 | in2;
    r2 = in2 ^ in1;

    fork
        out1 = r1 & r2;
        out2 = r2 + r1;
    end_join: join
end

endmodule

