
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): More than one case item can be specified in a single line separated by
// comma and if any of the items match with the case condition, it is then
// executed. This design uses a case item and default in the same line with
// a comma separation. This is illegal and should be treated as an error.

typedef enum { INVALID_MONTH, JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC } months;

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    months mnths;

    always@(posedge clk)
    begin
        out1 = days(in1);
        out2 = days(in2);
    end

    function int days(months m);
        int n;

        case(m)
            FEB:                               n = 28;
            APR, JUN, SEP, NOV:                n = 30;
            JAN, MAR, MAY, JUL, AUG, OCT, DEC: n = 31;
            INVALID_MONTH, default:            n = 0;  /* error: expression and default used as case item as a single item */
        endcase

        return n;
    endfunction
endmodule

