
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case in having both block name and label 
// before a block.

module test;
int a = 5;
int b = 10;
initial
label1 : fork : block1
			a = #5 b;
			b = #5 a;
join
endmodule
