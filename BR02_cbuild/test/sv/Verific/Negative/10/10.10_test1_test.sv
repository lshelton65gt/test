
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): It is illegal to have an unpacked structure in event control.

module test(i1, i2, i3, i4, o1, o2, o3);

parameter IN_WIDTH = 8;
parameter OUT_WIDTH = 8;

typedef struct
{
    bit signed [OUT_WIDTH-1:0] s1;
    bit signed [OUT_WIDTH-1:0] s2;
    bit signed [OUT_WIDTH-1:0] s3;
    bit [OUT_WIDTH-1:0] t1;
    bit [OUT_WIDTH-1:0] t2;
    bit [OUT_WIDTH-1:0] t3;
}dtype;

input signed [IN_WIDTH-1:0] i1, i2;
input [IN_WIDTH-1:0] i3, i4;
output reg signed [OUT_WIDTH-1:0] o1,o2,o3;
dtype dt,dt1;

function dtype fn(dtype x);
    dtype tmp;
    tmp.s1=x.s1++;
    tmp.s2=x.s2++;
    tmp.s3=x.s3++;
    tmp.t1=x.t1--;
    tmp.t2=x.t2--;
    tmp.t3=x.t3--;
    return tmp;
endfunction
always @(i1, i2, i3, i4)
begin
        if (i1[IN_WIDTH-1] == 1)
                dt.t1 = ~(i1-1);
        else
                dt.t1 = i1;

        dt.t2 = (i1[IN_WIDTH-1] ? -i1 : i1);

        if (dt.t1 > dt.t2)
                dt.t3 = 1;
        else
        begin
                if (dt.t1 == dt.t2)
                        dt.t3 = 0;
                else
                        dt.t3 = -1;
        end

        dt.s1 = dt.t3 * (i2[IN_WIDTH-1] ? i2 : (~i2 + 1));
        dt.s2 = dt.s1 | i3;
        if ( dt.s1 !== dt.s2)
                dt.s3 = dt.s1 ^ dt.s2;
        else
                dt.s3 = dt.s1 & dt.s2;

end

always @(dt)	// Error: unpacked structure in sensitivity list
begin
    dt1=fn(dt);
    {o1,o2,o3}={dt1.s1,dt1.t1,dt1.t2};
end

endmodule

