
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case in having a label name before 'end' or 'join'

module test;
int a = 5;
int b = 10;

initial
    fork
	    a = #5 b;
	    b = #5 a;
    label1 : join
endmodule



