
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses 'return' statement to return a value from a function. The
// type of the value returned is different from the one that is declared. This is
// an error and should be captured by the tool.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2) ;

    reg [width-1:0] r1 ;
    reg [0:width-1] r2 ;

    always @(posedge clk)
    begin
        r1 = ret_func(in1, in2) ;
        r2 = ret_func(in2, in1) ;

        out1 = ret_func(r1, r2) ;
        out2 = ret_func(r2, r1) ;
    end

endmodule

function automatic integer ret_func(input integer a, b) ;
    struct { logic [63:0] c ;} new_str ;

    new_str.c = (a-b)/2 ;

    return new_str ;
endfunction

