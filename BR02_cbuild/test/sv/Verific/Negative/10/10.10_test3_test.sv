
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): System Verilog does not allow '+=' assignment operator in
// event control expression. This design uses this operator in event expression.

module test;
int a, b, c;
int count = 0;
initial
begin
	a = 5;
	b = 10;
	c = 0;
    #5  c = 5;
end

always @((c += a * b))
begin
	count++;
	$display($time,,,"count = %d\t a = %d, b = %d, c = %d", count, a, b, c);
end
endmodule

