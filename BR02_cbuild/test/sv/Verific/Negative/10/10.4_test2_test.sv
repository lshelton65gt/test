
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses priority keyword with casez statement. As with if-else-if
// the keyword priority denotes that if there is same condition multiple times
// then select the first one. If all the possible cases are not handled then,
// when unexpected value comes, it should generate a runtime warning.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1;
reg [0:p-1] r2;
reg [1:0] r3;

always @(posedge clk)
begin
    r1 = in1 - in2;
    r2 = ~in1 ^ ~in2;
    r3 = r1 | r2;

    priority casez (r3)  // value 01 will generate runtime warning
	2'b00:
        begin
            out1 = r1 - r2;
            out2 = r1 + r2;
        end
	2'b1?:
        begin
            out1 = r1 & r2;
            out2 = r1 | r2;
        end
	2'b00:
        begin
            out1 = r1 * r2;
            out2 = r1 ^ r2;
        end
    endcase
end

endmodule

