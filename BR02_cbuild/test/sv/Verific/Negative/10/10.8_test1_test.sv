
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Labels in SystemVerilog does not create a hierarchical scope. So it cannot
// be used in an hierarchical identifier. This design uses label in hierarchical
// identifier to check whether the tool can detect it as an error.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = in2 - in1;
    r2 = in1 - in2;

    blk1: begin
        reg [width-1:0] r3;
        if (r1 == r2)
        begin
            r3 = in1 - r2 | r1;
            out1 = r3 | r2;
        end
        else
        begin
            r3 = in2 ^ r2 + r1;
            out1 = r3 | r1;
        end
    end

    out2 = out1 ^ r2 - in1 | blk1.r3;
end

endmodule

