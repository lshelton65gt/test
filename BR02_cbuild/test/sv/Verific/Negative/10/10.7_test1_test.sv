
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Within a function , a final block is used, --- it is error

module test(input int p, output int q) ;

    function int func(int p) ;
        
        return p ;
        final
            $display("call of function: value of p: %d", p) ;

    endfunction

    assign q = func(p) ;

endmodule

module bench ;
    int p = 10, q = 0 ;

    test(p,q) ;

    always @(q)
        $display("value of q changed: %d", q) ;

endmodule

