
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test incorrect use of jump statements. Here 'break' is used
// outside any loop statement. This is an error.

module test ;
    int a[5] = '{ 1, 6, 3, 10, 56 } ;
    int ser = 7;

    function int search(int n) ;
        int ret ;
        for(int i = 0; i < 5; i++) begin
            ret = 0 ;
            if ( n == a[i] ) begin
                ret = 1 ;
                break ;
            end
            else
                continue ;
        end
        if(ret == 1)
            break ;
        else
            return ret ;
    endfunction

    initial begin:pss 
       // ser = 3 ;
        if (search(ser) == 1)
            $display(" eureka !!") ;
        else
            $display("not found") ;
     end
endmodule

