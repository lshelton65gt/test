
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing whether class can be used as a inout port.

class Album ;

    string singer ;
    string producer ;
    rand int cost ;
    rand int total_number_of_songs ;

    function new(string s, string p, int c, int t) ;
        singer = s ;
        producer = p ;
        cost = c ;
        total_number_of_songs = t ;
    endfunction

    function void show ;
        $display("%s, %s, %d, %d", singer, producer, cost, total_number_of_songs) ;
    endfunction

endclass

module test (input clk, inout Album a); 

    always @(posedge clk) begin
        if ( a.cost > 20)
            a.cost = a.randomize() ; 
        else
            a.cost = 20 ;

        if (a.total_number_of_songs > 50)
            a.total_number_of_songs = a.randomize() ;
        else
            a.total_number_of_songs = 20 ;

        a.singer = "no" ;
        a.producer = "yesy" ;
    end

endmodule

module bench ;
    reg clk ;
    Album a = new("wqqw","ew",23,56) ;
     
    initial
        clk = 1 ;
    always
        #5 clk = ~clk ;

    initial begin
        a.show() ;
        #10 $finish ;
    end

    test t(clk, a) ;

endmodule

