
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses class objects as members of alias statement.
// This is an error as alias members must be nets.

class Student ;
//////////Properties///////
    string name ;
    int roll ;
    string address ;
//////////Methods//////////
    function new(string n, int r, string a) ;
        name = n ;
        address = a ;
        roll = r ;
    endfunction

    function void setdata(string n, int r, string a) ;
        name = n ;
        roll = r ;
        address = a ;
    endfunction

    function void show ;
        $display("name = %s, roll = %d, address = %s", name, roll, address) ;
    endfunction

    function string getname() ;
        getname = name ;
    endfunction

    function string getaddress() ;
        getaddress = address ;
    endfunction

    function int getroll() ;
        getroll = roll ;
    endfunction

endclass

module test(input Student s, output Student s1) ;

    alias s = s1 ; // Error : Members of alias must be signal/net

    initial begin
        s.setdata("dfg","dfghjk",60) ;
        s1.show() ;
    end

endmodule

module top ;
    reg clk ;
    Student s = new("qwer","qwerty",30), s1 ;

    initial
        clk = 1 ;

    always
        #5 clk = ~clk ;

    initial
        #1001 $finish ;

    test t(s,s1) ;

    initial begin
        s1.show() ;
    end
endmodule

