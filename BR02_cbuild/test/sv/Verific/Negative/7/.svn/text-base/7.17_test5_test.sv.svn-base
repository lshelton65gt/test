
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of data hiding and encapsulation. Protected methods
// cannot be accessed form outside the class scope, except from derived class's
// scope. Here we are testing an illegal access of a protected member from 
// a derived class.

class Packet ;    //parent class
    local integer value ;  // local property
    function new (int x = 0) ;
        value = x ;
    endfunction
    local function integer overhead() ; // local method
        overhead = value * 10 ;
    endfunction
    function integer delay() ;
        delay = value * value ;
    endfunction
endclass

class LinkedPacket ;    
    local integer value ; // local property
    Packet pkt = new ;
    function new (int x = 0) ;
        value = x;
    endfunction
    local function integer overhead() ; // local method
        overhead = value * 10 ;
    endfunction
    function integer delay() ;
        delay =  overhead() + pkt.delay() ; // Error: local method of class Packet not accesssible in this scope
    endfunction
endclass


module test (clk) ;
    input clk ;

    Packet pkt1 = new ;
    LinkedPacket lk_pkt1 = new ;
    int delay1 ;
    int delay2 ;
    int tot_delay ;
    always @ (posedge clk)
    begin
        delay1 = pkt1.delay() ;   // Error: local method of class Packet not accesssible in this scope
        delay2 = lk_pkt1.delay() ;   // Error: local method of class LinkedPacket not accesssible in this scope
        tot_delay = delay1 + delay2 ;
    end
endmodule

