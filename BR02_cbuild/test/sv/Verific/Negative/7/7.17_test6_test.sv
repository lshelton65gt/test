
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of data hiding and encapsulation.
// Local properties cannot be accessed outside the class scope.

class Packet ;    //parent class
    local integer value ;  // local property
    function new (int x = 0) ;
        value = x ;
    endfunction
    function integer delay() ;
        delay = value * value ;
    endfunction
endclass

class LinkedPacket extends Packet ;
 
    local integer value ;  // local property
    function new (int x = 0, int y = 0) ;
        super.new (x) ;
        value = y ;
    endfunction
    function integer delay() ;
        delay = super.delay() + value * super.value ;
    endfunction
endclass


module test (clk) ;
    input clk ;

    Packet pkt1 = new ;
    LinkedPacket lk_pkt1 = new ;
    
    always @ (posedge clk)
    begin
        pkt1.value = lk_pkt1.value ; // cannot access local properties of a class.
    end

endmodule

