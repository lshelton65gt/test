
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A static method has no access to non-static members (properties
// or methods). Access to non-static members is illegal and results in a compiler error. 

class Student;
    static int current = 0;
    string name ;
    int roll ;

    static function next_num();
        current ++ ;
        name = "" ;   // Error: Non-static member called from static method
        roll = 0 ;    // Error: Non-static member called from static method
    endfunction

    function void setdata(string n, int r) ;
        name = n ;
        roll = r ;
    endfunction

endclass

module test ;
    Student s1 = new ;
    initial
    begin
        s1.setdata("qwer",23) ;
        Student::next_num() ;
    end
endmodule

