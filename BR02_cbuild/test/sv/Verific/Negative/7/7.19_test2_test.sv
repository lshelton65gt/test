
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): If an abstract class has any virtual methods then all of
// these methods must be overridden for the subclass to be instantiated. The
// following design tests this.

virtual class Base ; // abstract class
    int value ;
    string data ;
    virtual function int GetValue ; // virtual class 
    endfunction
    virtual function string GetData ; // virtual class 
    endfunction
endclass

class Derived extends Base ;
    function int GetValue ; // defintion of one virtual function
        return value ;
    endfunction
endclass

module test (input in, output out) ;

    assign out = in ;
    Derived d = new ;    // Error: Class derived still does not define virtual function GetData

endmodule

