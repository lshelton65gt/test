
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): It is never legal to directly assign a superclass variable
// to a subclass variable. The following design checks this.

class A ;
    int a ;
endclass

class B ;
    int b ;
endclass

module test (input in, output out) ;

    assign out = in ;
    A x = new ;
    B y = new ;
    initial 
        $cast(y,x) ;

endmodule

