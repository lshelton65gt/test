
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Static methods cannot be virtual. This design checks this.

class Student;
    static int current = 0;
    string name ;
    int roll ;

    virtual static function next_num();  // Error: static method cannot be virtual
        current ++ ;
        name = "" ;
        roll = 0 ;
    endfunction

    function void setdata(string n, int r) ;
        name = n ;
        roll = r ;
    endfunction

endclass

class Batch extends Student;
    int year_of_passing ;
    static function next_num() ;
        current = current + 10 ;
    endfunction
endclass

module test ;

    Batch b1 = new ;

    initial
    begin
        b1.setdata("qwer",23) ;
        b1::next_num() ;
        $display(" Value of current: %d",Batch.current) ;
    end

endmodule

