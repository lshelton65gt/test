
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): If a property of a 'class' is declared as local, then it
// cannot be inherited by a subclass. Any attempts at  using this local member
// should be deemed as an error.

class Student ;

    string name ;
    local int roll ;        // local variable
    string address ;
    static integer num_of_student = 0 ;

    function new () ;

       name = "" ;
       address = "" ;

    endfunction

endclass

class Batch extends Student ;

    int year_of_joining ;

    function new(int yr) ;

        super.new() ;
        year_of_joining = yr ;
        roll = 100 ;    // Error: tries to accesses 'roll' which is a local variable of the parent class and has not been inherited

    endfunction

endclass

