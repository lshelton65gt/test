
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A SV object pointer cannot be used for arithmetic operations.
// Here, it is used for such operations. This is an error.

class Student;
    string name ;
    string address ;
endclass

class Batch extends Student ;
    int year_of_joining ;
endclass


module test ;
    Student s[0:1] ;
    initial
    begin
    s[0] = new ;
    s[1] = new ;

        s[0] = s[0] + 1 ;
        if(s[0] == s[1])
            $display("arithmetic operation possible") ;
    end

endmodule
    
