
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A class variable cannot be used without creating an
// object of that class using the 'new' function.

class Abc ;
    int a ;
    int b ;

    function new (int x, int y) ;
        a = x ;
        b = y ;
    endfunction
endclass

module test ;
    Abc aa, bb, cc ; // Uninitialized object handles are set to null by default
                     // An uninitialized object can be
                     // detected by comparing its handle with null.
    chandle dd, ee, ff ;
    initial begin
        cc = aa - bb ; // Error: This is illegal
        ff = dd - ee ; // Error: This is illegal
    end
endmodule

