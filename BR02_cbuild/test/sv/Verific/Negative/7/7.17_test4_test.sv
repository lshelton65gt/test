
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of data hiding and encapsulation.
// 'protected' methods cannot be accessed from outside the class scope.
// except from the inherited class's scope.

class Packet ;    // parent class
    local integer value ;  // Use of local properties.
    function new (int x = 0) ;
        value = x ;
    endfunction
    local function integer overhead() ;
        overhead = value * 10 ;
    endfunction
    protected function integer delay() ; // Protected property
        delay = value * value ;
    endfunction
endclass

class LinkedPacket  extends Packet ;    
    local integer value ;
    function new (int x = 0, int y = 0) ;
        super.new (x) ;
        value = y ;
    endfunction
    local function integer overhead() ;
        overhead = value * 10 ;
    endfunction
    protected function integer delay() ; // Protected property
        delay =  overhead() + value * value ;
    endfunction
endclass


module test (clk) ;
    input clk ;

    Packet pkt1 = new ;
    LinkedPacket lk_pkt1 = new ;
    int delay1 ;
    int delay2 ;
    int tot_delay ;
    always @ (posedge clk)
    begin
        delay1 = pkt1.delay() ;    // Error: cannot access protected methods of a class.
        delay2 = lk_pkt1.delay() ; // Error: cannot access protected methods of a class.
        tot_delay = delay1 + delay2 ;
    end
endmodule

