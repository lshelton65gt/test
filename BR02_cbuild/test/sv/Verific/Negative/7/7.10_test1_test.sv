
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The 'this' keyword shall only be used within non-static 
// class methods. This design uses the 'this' keyword within a static method.
// This is an error.

class Student;
    static int current ;
    string name ;
    int roll ;

    static function next_num(int current) ;
        this.current = current ;
    endfunction

    function new( string name , int roll) ;
        this.name = name ;
        this.roll = roll ;
    endfunction

    function void setdata(string n, int r) ;
        name = n ;
        roll = r ;
        this.printdata() ;
    endfunction

    function void printdata() ;
        $display("Name :%s, Roll: %d",name, roll) ;
    endfunction

endclass 

module test ;

    Student s1 = new ;
    initial begin 
        s1.setdata("qwer",23) ;
        Student::next_num(12) ;
    end

endmodule

