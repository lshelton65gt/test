
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A static method has no access to non-static members 
// (ie. properties or methods),  but it can directly access static class 
// properties  or call static methods of the same class. Access to 
// non-static members or to the special 'this' handle within the body 
// of a static method is illegal and should result in error.

class Abc ;
    static int change = 0 ;
    static int prev = 0 ;
    function int is_equal ;
        if (prev == change)
            return 1 ;
        return 0 ;
    endfunction
    static function int next_id ();
        if (is_equal())  // Error: cannot access non-static member from static method
            $display ("\n Sorry can't change") ;
        else
            begin
               prev = change ;
               change++ ;
            end
    endfunction
endclass

module test (clk) ;
    input logic clk ;
    Abc aa ;
    Abc bb ;
    int p ;
    always @ (posedge clk)
        p = Abc::next_id () ; 
endmodule

