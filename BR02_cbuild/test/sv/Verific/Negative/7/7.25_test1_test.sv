
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design tries to cast an int type variable to an 'int'
// This should be an error.


class A ;
    int data ;
endclass

module test(input in, output out) ;

    A a ;
    int x = a`(x) ;

    assign out = in ;

endmodule

