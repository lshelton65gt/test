
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): An instance of a class (an object handle) can also be declared
// with the 'const' keyword. It cannot be overwritten. The arguments to the 'new'
// method must be a constant expression.

module test ;

    class Student ;
    //////////Properties///////
        string name ;
        int roll ;
        string address ;
    //////////Methods//////////
        function new(string n, int r, string a) ;
            name = n ;
            address = a ;
            roll = r ;
        endfunction

        function void setdata(string n, int r, string a) ;
            name = n ;
            roll = r ;
            address = a ;
        endfunction

        function string getname() ;
            getname = name ;
        endfunction
    
        function string getaddress() ;
            getaddress = address ;
        endfunction
    
        function int getroll() ;
            getroll = roll ;
        endfunction

        function void show() ;
            $display("name: %s, roll: %d, address: %s", name, roll, address) ;
        endfunction
    endclass

    string str = "qwerty" ;
    int r = 67 ;
    const Student s = new (str, r, "wew"); // Error : For const instance of class arguments of new must be constant

    initial begin
       s.setdata("asd","ffgh",45) ;
       s.show() ;
    end

endmodule

