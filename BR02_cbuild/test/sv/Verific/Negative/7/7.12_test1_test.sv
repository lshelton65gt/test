
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The inheritance mechanism provided by SystemVerilog is 
// called single-Inheritance, ie. each class is derived from a single parent class.
// Here, a child class is inherited from two parent classes, this is an error

class Teacher ;
    string sub_name ;
    string qualification ;
    string date_of_joining ;
endclass 

class Student;
    string name ;
    string address ;
endclass

class Batch extends Student, Teacher ;
    int year_of_joining ;
    function new (string s, string q, string doj, string n, string a, int yr) ;
        sub_name = s ;
        qualification = q ;
        date_of_joining = doj ;
        name = n ;
        address = a ;
        year_of_joining = yr ;
    endfunction
endclass

Batch b1 = new ("comp sc","mtech","12/5/02","qwer","salt lake", 2001) ;    

