
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Arithmetic operations(such as incrementing) is not allowed
// on SV object handle.

class Abc ;
    int data1 ;
    function new (int x) ;
        data1 = x ;
    endfunction
endclass

module test (clk) ;

    input logic clk ;
    Abc aa ;
    int p ;
    always @ (posedge clk)
    begin
        p = aa + 1 ;
        $display ("p=%d", p) ;
    end

endmodule

