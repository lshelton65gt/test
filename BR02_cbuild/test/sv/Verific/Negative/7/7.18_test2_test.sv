
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Instance constants cannot be declared as static.

class InstanceConst ;
    static const string inst_string ;  // Error: instance constant cannot be declared static
    int num ;
    bit [0:1] data ;
    function new() ;
        inst_string = "instance " ;
        num = 10 ;
        data = 0 ;
    endfunction
endclass

module test ;

    InstanceConst i1 = new() ;
    InstanceConst i2 = new() ;

    initial begin
        $display("%s, %d", i1.inst_string, i1.data) ;
        $display("%s, %d", i2.inst_string, i2.data) ;
    end

endmodule

