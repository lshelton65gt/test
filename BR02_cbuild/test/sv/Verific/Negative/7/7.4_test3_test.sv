
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Arithmetic operations (such as incrementing) is not allowed
// in either SV object handle or SV pointer. This design tries to increment object
// handle. This is an error.

class Abc ;
    int a ;
    int b ;

    function new (int x, int y) ;
        a = x ;
        b = y ;
    endfunction
endclass

module test (clk) ;

    input logic clk ;
    Abc aa ;
    chandle bb ;
    always @ (posedge clk) 
    begin
        aa++ ; // can not do this according to LRM SV-3.1 page 80
        bb++ ; // can not do this according to LRM SV-3.1 page 80
    end

endmodule

