
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing if class can be used as an input port, but with a
// ref keyword, so, its an error.

class Album ;
    string singer ;
    string producer ;
    int cost ;
    int total_number_of_songs ;

    function new(string s, string p, int c, int t) ;
        singer = s ;
        producer = p ;
        cost = c ;
        total_number_of_songs = t ;
    endfunction

    function void show ;
        $display("%s, %s, %d, %d", singer, producer, cost, total_number_of_songs) ;
    endfunction

endclass

module test (ref input Album a); 

    initial begin
        if(a.total_number_of_songs <= 10)
              $display("Its  a flop") ;
        else if(a.total_number_of_songs <= 50)
              $display("Its a hit") ;
        else if(a.total_number_of_songs >= 100)
              $display("Its a super hit") ;
    end

endmodule

module bench ;

    Album a = new("sad","sds",23,45) ;
    test t(a) ;

endmodule

