
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Sometimes a class variable needs to be declared before the
// class itself has been declared. Otherwise in the course of processing the 
// declaration for the first class, if the compiler encounters an undefined
// reference to the second class, it flags an error. 

class List ;
    Node first ; // Error: Tries to use class Node which has been defined later
    Node last ;

    function new() ;
       first = 0 ;
       last = 0 ;
    endfunction
    
    function void insert(int num) ;
        Node n =new(num) ;
        if(first == 0 )
            first = n ;
        else
            last.next = n ;
        last = n ;
    endfunction

endclass

class Node ;
    int data ;
    Node next ;
   
    function new(int num) ;
        data = num ;
        next = 0 ;
    endfunction

endclass

List l = new ;

l.insert(5) ;
l.insert(6) ;
l.insert(7) ;
l.insert(8) ;
l.insert(9) ;
l.insert(10) ;

