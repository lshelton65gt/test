
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): When using the 'super' within 'new', 'super.new' shall be
// the first statement executed in the constructor. The following design defines
// a derived class having some statement before a call to the 'super.new' function
// in the constructor of the derived class.


class A ;
    int x ;
    function new () ;
        x = 1 ;
    endfunction
endclass

class B extends A ;
    int y ;
    function new () ;
        y = 1 ; // Error : super.new should be the first statement in the constructor for the derived class
        super.new() ;
    endfunction
endclass

module test(input in, output out) ;

    B b = new ;
    assign out = in ;

endmodule
