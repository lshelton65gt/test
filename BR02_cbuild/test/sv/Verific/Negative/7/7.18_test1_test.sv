
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Global constants cannot be assigned a value anywhere 
// other than in the declaration.
// Global constants are also declared static since they are the same for 
// all instances of the class. However, an instance constant cannot be declared
// static, since that would disallow all assignments in the constructor.

class GlobalConst ;
    static const string global_string = "global" ; // global constant
    int data ;
    function new() ;
        data = 15 ;
        global_string = "changed" ;
    endfunction
endclass

class InstanceConst ;
    static const string inst_string ; // global constant
    int num ;
    function new() ;
        inst_string = "instance " ; // Error: static member cannot be initialized in constructor
        num = 10 ;
    endfunction
endclass

module test ;

        GlobalConst g1 = new() ;
        GlobalConst g2 = new() ;

        InstanceConst i1 = new() ;
        InstanceConst i2 = new() ;
    initial begin
        $display("%s, %d", g1.global_string, g1.data) ;
        $display("%s, %d", g2.global_string, g2.data) ;
        $display("%s, %d", i1.inst_string, i1.data) ;
        $display("%s, %d", i2.inst_string, i2.data) ;
    end

endmodule

