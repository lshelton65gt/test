
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a class and a structure. In structure
// definition an object of that class is used as member. Parameters are defined
// of type class and structure. In default value of such parameters 'new' method 
// is used to construct class object. This is an error to specify 'new' method
// in default value of parameters.
 

class Check ;
    int c ;
    int d ;
 
    function new (int c, int d) ;
        this.c = c ; 
        this.d = d ;
    endfunction
endclass

typedef Check che ;

typedef struct {
    int a ;
    int b ;
    Check C ;
} num ;

module test ;
    num n = '{a:3, b:5} ;
    parameter che ch = new(4,5) ;
    parameter num p = '{1,3, ch } ;
    bot #(p) b1() ;
endmodule

module bot #(parameter num n = '{2,4, new (6,8)}) ;
    initial
       $display(n.a) ;
endmodule

