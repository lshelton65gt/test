
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assigning an array to another array of different class type.

module test ;
class Student ;
//////////Properties///////
    string name ;
    int roll ;
    string address ;
//////////Methods//////////
    function new ;
        name = "" ;
        address = "" ;
        roll = 10 ;
    endfunction

    function void setdata(string n, int r, string a) ;
        name = n ;
        roll = r ;
        address = a ;
    endfunction

    function string getname() ;
        getname = name ;
    endfunction

    function string getaddress() ;
        getaddress = address ;
    endfunction

    function int getroll() ;
        getroll = roll ;
    endfunction
endclass

class Album ;
//////////Properties///////
    string singer ;
    string producer ;
    int cost ;
    int total_number_of_songs ;

//////////Methods//////////
    function new(string s, string p, int c, int t) ;
        singer = s ;
        producer = p ;
        cost = c ;
        total_number_of_songs = t ;
    endfunction

    function void show ;
        $display("%s, %s, %d, %d", singer, producer, cost, total_number_of_songs) ;
    endfunction
endclass

    Album a[4] ;
    Student s[4] ;

    initial begin
        s[0] = new ;
        s[1] = new ;
        s[2] = new ;
        s[3] = new ;

        a = s ; // Error : 'a' and 's' are different objects of different types
        a[0].show () ;
    end

endmodule

