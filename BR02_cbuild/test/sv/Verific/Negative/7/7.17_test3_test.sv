
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE
       
// TESTING FEATURE(S): Test of data hiding and encapsulation.
// Protected properties cannot be accessed from outside the class scope,
// except from the inherited classes.

class Packet ;    //parent class
    protected integer value ;  // Use of protected property.
    function new (int x = 0) ;
        value = x ;
    endfunction
    function integer delay() ; 
        delay = value * value ;
    endfunction
endclass

class LinkedPacket extends Packet ;   
    local integer value ;
    function new (int x = 0, int y = 0) ;
        super.new (x) ;
        value = y ;
    endfunction
    function integer delay() ;
        delay = super.value + value + super.delay() ; // Inherited class can access protected properties.
    endfunction
endclass

module test (clk) ;

    input clk ;

    Packet pkt1 = new ;
    integer tot_delay ;
    always @ (posedge clk)
        tot_delay = pkt1.value ; // cannot access proteced properties of a class.

endmodule

