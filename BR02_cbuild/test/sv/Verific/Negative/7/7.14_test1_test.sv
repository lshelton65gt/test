
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The 'super' keyword can be used to access members residing
// one level up in the class hierarchy. There is no way to reach higher using 
// super.super. This is an error.

class Packet ;    //parent class
    integer value ;
    function integer delay() ;
        delay = value * value ;
    endfunction
endclass

class LinkedPacket extends Packet ;    //derived class
    integer value ;
    function integer delay() ;
        delay = super.delay() + value * super.value ;
    endfunction
endclass

class ChildLkPacket extends LinkedPacket ; // derived from derived class
    integer value ;
    function integer delay() ;
        delay = super.super.delay() + super.delay() + value * super.value * super.super.value ;
                           // This cannot be done. Compilation error.
    endfunction
endclass

module test (clk) ;
    input clk ;

    Packet pkt1 = new ;
    LinkedPacket lk_pkt1 = new ;
    ChildLkPacket ch_pkt1 = new ;

    always @ (posedge clk)
    begin
        pkt1 = lk_pkt1 ;
        pkt1.delay() ;     // It should call delay() of Packet class,
                           // because it is not virtual.
        lk_pkt1.delay() ;  // It should call delay of LinkedPacket class.
        ch_pkt1.delay() ;
    end
endmodule

