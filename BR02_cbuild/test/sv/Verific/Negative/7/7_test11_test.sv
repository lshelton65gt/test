
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing if class can be used as a output port.

class Album ;
    string singer ;
    string producer ;
    int cost ;
    int total_number_of_songs ;

    function new(string s, string p, int c, int t) ;
        singer = s ;
        producer = p ;
        cost = c ;
        total_number_of_songs = t ;
    endfunction

    function void show ;
        $display("%s, %s, %d, %d", singer, producer, cost, total_number_of_songs) ;
    endfunction

endclass

module test (input clk, output Album a); 
    string  singer;
    string producer;
    int cost;
    int total_number_of_songs;


    always @(posedge clk) begin
        cost = $random ;
        total_number_of_songs = $random ;
        singer = "no" ;
        producer = "yesy" ;
        a = new (singer, producer, cost, total_number_of_songs);
    end

endmodule

module bench ;
    reg clk ;
    Album a ;
     
    initial
    begin
        clk = 1 ;
        a.show() ;
    end
    always
        #5 clk = ~clk ;

    initial 
        #100 $finish ;

    test t(clk, a) ;

endmodule

