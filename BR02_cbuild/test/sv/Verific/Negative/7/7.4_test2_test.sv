
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a class. A parameter is defined, the 
// data type of that parameter is class and default value of this parameter is
// the constructor call of that class. This is an error to specify 'new' method
// as constant expression in parameter's default value.

class Check ;
    int c ;
    int d ;
 
    function new (int c, int d) ;
        this.c = c ; 
        this.d = d ;
    endfunction
endclass

typedef Check che ;

module test ;
    parameter che ch = new(4,5) ;
    bot #(ch) b1() ;
endmodule

module bot #(parameter che n = new (6,8) ) ;
    initial
       $display(n.a) ;
endmodule

