
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Casting of SV object handles to built-in types like
// shortreal, int etc is not allowed.

class Abc ;
    int a ;
    int b ;

    function new (int x, int y) ;
        a = x ;
        b = y ;
    endfunction
endclass


module test (clk);
    input logic clk ;
    Abc aa = new (100, 200) ;
    chandle cc = null ;
    shortreal ss;
    int tt ;                  
    always @ (posedge clk)
    begin
        ss = shortreal'(aa) ; // Error
        ss = shortreal'(cc) ; // Error
        tt = int'(aa) ;       // Error
        tt = int'(cc) ;       // Error
    end
endmodule

