
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of data hiding and encapsulation.
// Local methods cannot be accessed from outside the 'class' scope.

class Packet ;    //parent class
    local integer value ;  // local member
    function new (int x = 0) ;
        value = x ;
    endfunction
    local function integer overhead() ;
        overhead = value * 10 ;
    endfunction
    local function integer delay() ; // local member
        delay = value * value ;
    endfunction
endclass

class LinkedPacket ;    
    local integer value ; // local member
    function new (int x = 0) ;
        value = x ;
    endfunction
    local function integer overhead() ; // local member
        overhead = value * 10 ;
    endfunction
    local function integer delay() ; // local member
        delay =  overhead() + value * value ;
    endfunction
endclass


module test (clk) ;
    input clk ;

    Packet pkt1 = new ;
    LinkedPacket lk_pkt1 = new ;
    int delay1 ;
    int delay2 ;
    int tot_delay ;
    always @ (posedge clk)
    begin
        delay1 = pkt1.delay() ;   // Error: cannot access local method of a class.
        delay2 = lk_pkt1.delay() ; // Error: cannot access local method of a class.
        tot_delay = delay1 + delay2 ;
    end
endmodule

