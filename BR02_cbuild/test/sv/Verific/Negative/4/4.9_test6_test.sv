
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog supports forward declaration. This design uses forward
// declaration and provides the actual definition inside an 'if-generate'
// construct. Each path of the 'if-else' statement defines it in a different
// way. This is an error as full definition of forward typedef should be defined
// in same scope.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    typedef my_type_t;   // forward definition

    my_type_t t1, t2;    // use of forward defined data type

    generate
        if (w<=4)
        begin
            my_type_t t5;    // use of forward defined data type

            always@(posedge clk)
            begin
                t5 = in1 - in2;
                t1 = in2 ^ t5;
                t2 = in1 | t1 + t5;
            end

            typedef logic [w-1:0] my_type_t;    // actual definition
        end
        else
            if (8==w)
            begin
                my_type_t t4;    // use of forward defined data type

                always@(posedge clk)
                begin
                    t4 = in2 & in1;
                    t2 = t4 - in1;
                    t1 = in1 ^ t2;
                end

                typedef byte my_type_t;    // actual definition, different from the previous one
            end
            else
            begin
                my_type_t t3;    // use of forward defined data type

                always@(posedge clk)
                begin
                    t3 = in2 & in1;
                    t2 = t3 | in1;
                    t1 = in2 + t2 - in1;
                end

                typedef struct    // actual definition, different from the previous definitions
                {
                    bit [(w<<1)-1:0] memb1;
                    logic [0:(w<<1)-1] memb2;
                } my_type_t;
            end
    endgenerate

    always@(posedge clk)
    begin
        out1 = t1 + t2;
        out2 = t2 - t1;
    end

endmodule

