
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test with circular dependency of structures.

typedef myTypeB;

typedef struct {
    int strInt;
    struct {int a; real b; myTypeB c; } varA;
} myTypeA;

typedef struct {
    int strInt;
    struct {logic a; real b; myTypeA c; } varB;
} myTypeB;

module test;

    myTypeA a;
    myTypeB b;

    initial
    begin
        repeat(20)
        begin
            a.strInt = $random;
            a.varA.a = $random;
            a.varA.b = {2{$random}};

            b.strInt = $random;
            b.varB.a = $random;
            b.varB.b = {2{$random}};
        end
            $monitor("integerA1 = %d, integerA2 = %d, realA1 = %d, integerB1 = %d, integerB2 = %d, realB1 = %d", a.strInt, a.varA.a, a.varA.b, b.strInt, b.varB.a, b.varB.b);
    end

endmodule

