
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): It is an error to declare an enumeration with two literals
// having the same value. While one may be explicitly specified, the other is
// implicitly derives its value from the previous enum member. This design assigns
// the same value to two different literals.

module test;

parameter P = 2;

    enum
    {
       A = 0,
       B = ((P>4) ? 0 : 1),    // B == 0 if P>4 and it is an error then
       C,
       D,
       E,
       F
    } state ;

endmodule

module top;

test #(.P(8)) t_mod();

endmodule

