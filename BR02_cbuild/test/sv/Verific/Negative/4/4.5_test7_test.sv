
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines an enum type. Data type can be specified
// in an enum declaration. it specifies void as the data type of the enum literal.
// This should be treated as error.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1,
              output reg [0:width-1] out1);

    enum void { A, B, C, D, E } void_enum;

    always@(posedge clk)
    begin
        out1 = (D - A) ^ in1;
    end

endmodule

