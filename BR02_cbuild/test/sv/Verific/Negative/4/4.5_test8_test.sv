
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design type defines a new type. The actual type is
// void. It then declares data element of the new type and uses them in the
// module. Since, data element of void type is illegal, hence, this is a 
// negative test case.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2) ;

    typedef void void_type ;

    void_type vt1, vt2 ;

    always@(posedge clk)
    begin
        vt1 = in1 - in2 ;
        vt2 = in2 ^ in1 ;

        out1 = vt1 * vt2 ;
        out2 = vt2 - vt1 ;
    end

endmodule

