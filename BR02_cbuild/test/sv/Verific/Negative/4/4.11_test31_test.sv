
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case using real and shortreal in packed structure.

module test;

    struct packed {
        real str_real;
        shortreal str_short;
        int str_int;
    } var1;

endmodule

