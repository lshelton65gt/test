
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use void variable in port connection of module instantiation.

module test ;

    reg [7:0] in_val ;
    void out_val ;

    initial
    begin
        in_val = 0 ;
        repeat(10)
        #10 in_val++ ;
    end

    inv I(out_val, in_val) ;

endmodule

module inv(output [7:0] out, input [7:0] in) ;

    assign out = ~in ;

endmodule

