
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines void type function argument in a function
// definition. This is not allowed. A function can have void return type but not
// void type arguments. So, this is a negative test case.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    always@(posedge clk)
    begin
        void_fn(in1|in2, out1);
        void_fn(in1^in2, out2);
    end

    function void void_fn(input void [width-1:0] a, output void [width-1:0] b);
        b = ~a;
    endfunction

endmodule

