
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case with 2 enum types having same members.

module test;
    enum {mem1, mem2, mem3}var1;
    enum {mem4, mem2, mem5}var2;
endmodule

