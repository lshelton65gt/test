
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The name() method returns the string representation of 
// the given enumeration value. This design assigns the value returned by
// the name() method to a module parameter. This is an error as the name will
// vary depending on the value assigned to the enum variable.

typedef enum {red, yellow, blue, green, white} colors ;

module test ;
    colors C = green ;
    parameter string e = C.name() ;
    bot #(e) b1() ;
    initial
       $display("%s", e) ;
endmodule

module bot #(parameter string c = "yellow" ) ;
    parameter le = c.len() ; 
    bot1 #(le) b2() ;
    initial
       $display("%s", c) ;
endmodule 

module bot1#(parameter p = 21) ;
    int r = cal(p) ;
    function int cal(int p) ;
        return p++ * ++p ;
    endfunction
    initial
       $display("%d", r) ;
endmodule

