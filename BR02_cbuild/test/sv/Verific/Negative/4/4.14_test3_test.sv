
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): When casting a literal/data element the type can be only
// simple type. This design casts a data element to union data type. The data type
// is defined in the casting statement itself. So, this is not a simple type. Thus,
// this is a negative test case.

module test (input clk,
             input [7:0] in1, in2,
             output reg [0:7] out1);

    always@(posedge clk)
        out1 = union packed signed {
                  bit [7:0] b;
                  logic [0:7] l;
                  byte c;
               }'(|in1 | |in2);

endmodule

