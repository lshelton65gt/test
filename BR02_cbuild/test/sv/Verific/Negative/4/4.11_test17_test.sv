
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that unpacked structures cannot be  declared as signed.

module test ;

    typedef struct signed
    {
         int day ;
         int month ;
         int year ;
         byte name[30:0] ;
         int salary_sum ;
    } em_info ;

    em_info em[99:0] ;

endmodule

