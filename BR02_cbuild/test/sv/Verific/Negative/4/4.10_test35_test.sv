
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Uses int type as the data type of enum declaration, and the
// values given are string , so it is an error.

module test ;

    typedef enum int { name = "qwerty", hobby, occupation } person;

    person p = p.first;
    initial begin
        forever begin 
           $display( "%s : %d\n", p.name, p );
           if( p == p.last ) break;
            p = p.next;
        end 

    end

endmodule

