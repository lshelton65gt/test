
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use of void variable to call function. Return value of a
// non-void function call is assigned to a void type variable.

module test(in_val, out_val) ;

    input int in_val ;
    output int out_val ;

    void value = in_val ;

    function int func(input int in) ;
        return in+1 ;
    endfunction

    always @(in_val)
        out_val = func(value) ;

endmodule

