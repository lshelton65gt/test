
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case. Value implicitly assigned to unassigned
// unassigned enum member used to explicitly assign another enum member.

module test;

    enum integer { mem1 = 5, mem2, mem3 = 6 } var1;

endmodule

