
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case using real and shortreal in packed union.

module test;

union packed { real un_real; }  var1;      
union packed { shortreal un_short; }  var2;

endmodule

