
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assign x or z values to a member of an enum declaration without
// specifying the data type.

module test ;

    enum
    { 
       AND = 32'b00 ,
       OR = 32'bx0 ,
       XOR = 32'b1z ,
       DEFAULT = 32'b11 
    } my_enum ;

endmodule

