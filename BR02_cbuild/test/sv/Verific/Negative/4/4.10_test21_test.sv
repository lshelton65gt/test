
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Declare an enumeration with constant range, assign a value
// of different width to an enumeration name.

module test ;

   enum bit [1:0]
   {
       AND = 2'b00,
       OR = 2'b01,
       NAND = 3'b111,
       XOR = 2'b10
   } gates ;

endmodule

