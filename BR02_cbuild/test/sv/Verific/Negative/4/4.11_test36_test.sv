
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Members of different sizes are allowed in unpacked unions,
// but not in packed unions. This design defines an unpacked union with members of
// different size and access them by using names, part select etc.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2) ;

    typedef union            // unpacked union
    {
        byte memb1 ;          // 8 bit member element
        bit [15:0] memb2 ;    // 16 bit member element
    } t_ps_un ;

    t_ps_un un1, un2 ;

    always@(posedge clk)
    begin
        un1.memb2 = { in1, ~in2 } ;
        un2 = ~un1.memb1 ^ un1.memb2 ; // Error: assignment to unpacked type
        un2[7] = '1 ;                  // Error: assignment to unpacked type

        out1 = un1[7:0] ^ un2.memb2 ;              // Error: Illegal part select
        out2 = un1.memb2 + {un2[3:0], un2[7:4]} ;  // Error: Illegal part select
    end

endmodule

