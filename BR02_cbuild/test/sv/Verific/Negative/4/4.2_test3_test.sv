
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design forward defines a new types using typedef keyword.
// Then it type defines an union type which has a member of another unnamed union
// in it. That unnamed union has a member of another unnamed union and that union
// has a member of the forward defined type. Finally the union is defined to be the
// previous forward defined type. This makes a circular link and hence is a negative
// test case.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef self; /* forward definition of self */

    typedef union {
        int  memb1;
        union {
            int memb1;
            union {
                int memb1;
                self memb2;  /* self type member */
                byte memb3;
            } memb2;
            byte memb3;
        } memb2;
        byte memb3;
    } self;  /* this is the self type */

    self s;

    always@(posedge clk)
    begin
        s = '1;
        out1 = in1 | in2 * s.memb2.memb2.memb1;
        out2 = in2 - in1 ^ s.memb2.memb2.memb2;
    end

endmodule

