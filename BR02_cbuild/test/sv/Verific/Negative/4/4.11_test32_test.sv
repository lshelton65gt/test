
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case using unpacked array in packed structure.

module test;

    struct packed {
        int str_int;
        byte str_char [3:0];
    } var1;

endmodule

