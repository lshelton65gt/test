
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Definition of user defined types can be deferred with a forward
// declaration. This design uses a forward declared type in different places and defines
// the actual definition inside for-generate. This is an error, since both the forward
// and the actual definition need to be in the same scope.
// NOTE: SystemVerilog 3.0 LRM has support for this, but IEEE-1800 LRM don't - so this is
// now a negative test case according to the SystemVerilog IEEE-1800 LRM!

module test (output int out, input int in1, in2) ;
    parameter p = 8 ;

    typedef \b[0].type1 ;
    typedef \b[1].type2 ;

    \b[0].type1 fact_res1 ;
    \b[1].type2 fact_res2 ;

    function automatic int fact(input int n) ;
        if (n <= 1)
            fact = 1 ;
        else
            fact = n * fact(n -1) ;
    endfunction

    always @(in1, in2)
    begin
        fact_res1 = fact(in1) ;
        fact_res2 = fact(in2) ;

        if (in1 > in2)
        begin
            out = fact_res1 - fact_res2 ;
        end
        else
        begin
            out = fact_res2 - fact_res1 ;
        end
    end
       
    generate
        genvar i ;

        for(i=p; i<p+1; i=i+1)
        begin: b
            if(i%2==0)
                typedef int type1 ;
            else
                typedef int type2 ;
        end
    endgenerate

endmodule
 
