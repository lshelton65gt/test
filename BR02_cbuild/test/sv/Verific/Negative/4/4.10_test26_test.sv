
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a module with three parameters. The
// parameter values are used as the values of the enum literals in an enum declaration. 
// When that module is instantiated, the parameter values are overridden and set to same
// value. So, all the enum literals get the same value. This is illegal. 

module test;

    localparam l=8;

    reg clk;
    reg [l-1:0] in;
    wire [l-1:0] out;

    bot #(.p1(l), .p2(l), .p3(l), .state(l)) inst_bot(clk, in, out);

endmodule

module bot(clk, in, out);
    parameter p1 = 4;
    parameter p2 = 8;
    parameter p3 = 16;
    parameter state = p1;

    input clk;
    input [p1-1:0] in;
    output reg [p3-1:0] out;

    logic [p2-1:0] temp;

    typedef enum { start = p1, middle = p2, finish = p3 } states;

    generate
        case(state)
            p1, p3:
                begin
                    always@(posedge clk)
                    begin
                        temp = { 1'b0, in }; 
                        out = temp; 
                    end
                end
            p2: begin
                    always@(posedge clk)
                    begin
                        temp = { in, 1'b0 }; 
                        out = temp; 
                    end
                end
            default: 
                begin
                    always@(posedge clk)
                    begin
                        temp = '1;
                        out = in ^ temp; 
                    end
                end
        endcase
    endgenerate

endmodule

