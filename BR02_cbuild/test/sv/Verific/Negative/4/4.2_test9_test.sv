
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design forward defines a new type using typedef keyword.
// Then it type defines 4 unions which has a member of the new type defined previously.
// That new type is also an union and has a member of the other union type. This makes
// a circular link and hence is a negative test case.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef self4;

    typedef union {
        int  memb1;
        self4 memb2;
        byte memb3;
    } self1;

    typedef union {
        int  memb1;
        self1 memb2;
        byte memb3;
    } self2;

    typedef union {
        int  memb1;
        self2 memb2;
        byte memb3;
    } self3;

    typedef union {
        int  memb1;
        self3 memb2;
        byte memb3;
    } self4;

    self1 a;
    self2 b;
    self3 c;
    self4 d;

    always@(posedge clk)
    begin
        a = '0;
        b = '1;
        c = '0;
        d = '1;
        out1 = in1 | in2 * d.memb2.memb2.memb2.memb2.memb1 ^ a.memb2.memb3;
        out2 = in2 - in1 ^ c.memb2.memb2.memb2.memb1 - b.memb2.memb2.memb3;
    end

endmodule

