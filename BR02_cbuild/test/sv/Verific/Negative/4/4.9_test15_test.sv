
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use a forward declared type in different places and define the
// actual definition inside case-generate. But the elaborated generate statement does
// not contain the actual definition. Actual typedef must be in the same scope of the
// forward declaration. Here the two are in different scope.
// NOTE: SystemVerilog 3.0 has support for this but IEEE 1800 don't - so this is now a
//       negative test case! (SV IEEE 1800 LRM section 4.9)

module test (output real v1, i2, 
             input real i1, v2) ;

    parameter p = 9 ;

    typedef h_params;

    h_params hp ;

    always @(i1, v2)
    begin
        if(v2 == 0)
        begin
            hp.h11 = v1/i1 ;
            hp.h21 = i2/i1 ;
        end

        if(i1 == 0)
        begin
            hp.h12 = v1/v2 ;
            hp.h22 = i2/v2 ;
        end

        v1 = hp.h11 * i1 + hp.h12 * v2 ;
        i2 = hp.h21 * i1 + hp.h22 * v2 ;
    end

    generate
        case (p)
            2 : typedef struct { real h11, h12, h21, h22; } h_params ;
            default : ;
        endcase
    endgenerate

endmodule

