
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design assigns a module parameter to another one declared
// before it. The latter parameter is is assigned the value returned by the name() method.

typedef enum {red, yellow, blue, green, white} colors ;
colors C = green ;

module test ;
    parameter string s = e ;  // Error : e hasn't yet been declared
    parameter string e = C.name() ;
    bot #(e) b1() ;
    initial
       $display("%s", e) ;
endmodule

module bot #(parameter string c = "yellow" ) ;
    parameter le = c.len() ; 
    bot1 #(le) b2() ;
    initial
       $display("%s", c) ;
endmodule 

module bot1#(parameter p = 21) ;
    int r = cal(p) ;
    function int cal(int p1) ;
        int p2 = p1 ;
        return p2++ * ++p2 ;
    endfunction
    initial
       $display("%d", r) ;
endmodule

