
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use void variable to task call. Void type variable connected
// to input port of a task. This is a negative test case, as object declaration of
// type void is not allowed in module.

module test(output int out, input int in) ;

    void in_val = in ;

    task mytask(output int out, input int in) ;
        out = in;
    endtask

    always @(in)
        mytask(out, in_val) ;

endmodule

