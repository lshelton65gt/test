
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that unpacked arrays are not allowed in packed structure or union.

module test ;

    struct packed
    {
        int day ;
        int month ;
        int year ;
        union packed
        {
           byte name[30:0] ;
           bit [30:0][7:0] bit_name ;
        } em_name ;
        int salary[11:0] ;
        real salary_sum ;
    } em_info[99:0] ;

    initial
    begin
        em_info[0].day = 1 ;
        em_info[0].month = 9 ;
        em_info[0].year = 2002 ;
        em_info[0].em_name.name = "John Smith" ;
        em_info[0].salary = '{'{2{10000}}, '{10{15000}}} ;
        em_info[0].salary_sum = 2 * 10000 + 10 * 15000 ;
    end

endmodule
 
