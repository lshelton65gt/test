
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Enumeration can have a data type. According to the LRM this
// data type can not be an user defined data type. This design defines an enumeration
// with another enumeration type. The type is unnamed. It is defined at the place of
// defining the enumeration. This is illegal in SystemVerilog and hence it is negative
// test case.

module test (input clk,
             input [2:0] in1, in2,
             output reg [0:7] out1, out2);

    enum enum { a = 10, b, c, d = 15, e, f } { A, B = b, C, D, E, F, G, H } alphabet;
               /* only 6 literals defined */  /* what will be the values values of G, H? */
                                              /* also incrementing values will not work! */
                                              /* and a can not be 0! by assuming enum of */
                                              /* enum type is possible, but it is not so */
    always@(posedge clk)
    begin
        out1 = alphabet'(in1+A);
        out2 = alphabet'(in2+A);
    end

endmodule

