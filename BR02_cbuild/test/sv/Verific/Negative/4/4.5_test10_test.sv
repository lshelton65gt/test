
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Void data type can be used as the return type of a function
// to indicate that the function does not return any value. This design defines a
// function with void type and signed property. Since void function type is illegal
// to be specified as signed, hence this is a negative test case.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    always@(posedge clk)
    begin
        void_fn(in1|in2, out1);
        void_fn(in1^in2, out2);
    end

    function signed void void_fn(input [width-1:0] a, output [width-1:0] b);
        b = ~a;
    endfunction

endmodule

