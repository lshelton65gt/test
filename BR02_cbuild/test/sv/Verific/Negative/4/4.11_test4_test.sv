
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case in having signed unpacked unions.

typedef union signed {
    int a ;
    shortreal b ;
} myType ;

module test ;

    myType a = '{32, 1.5} ;

    initial
       $display("Negative test case") ;
    
endmodule

