
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that unpacked structures or unions are not allowed in
// packed structure or union.

module test;

    typedef struct
    {
        int day, month, year ;
    } join_date ;

    typedef union
    {
        byte name[30:0] ;
        bit [30:0][7:0] bit_name ;
    } name_info ;

    struct packed
    {
        join_date date ;
        name_info name ;
        int salary_sum;
    } em_info[99:0] ;

    union packed
    {
        name_info name ;
        bit [$bits(name) - 1:0] bit_name ;
    } my_union_var ;

endmodule

