
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Definition of user defined types can be deferred with a forward
// declaration. This design uses a forward declared type in different places and defines
// the actual definition inside case-generate. This is an error, since both the forward
// and the actual definition need to be in the same scope.
// NOTE: SystemVerilog 3.0 LRM has support for this, but IEEE-1800 LRM don't - so this is
// now a negative test case according to the SystemVerilog IEEE-1800 LRM!

module test(output real v1, i2, 
            input real i1, v2) ;

    parameter p = 8 ;

    typedef h_params;

    h_params hp ;

    always @(i1, v2)
    begin
        if(v2 == 0)
        begin
            hp.h11 = v1/i1 ;
            hp.h21 = i2/i1 ;
        end

        if(i1 == 0)
        begin
            hp.h12 = v1/v2 ;
            hp.h22 = i2/v2 ;
        end

        v1 = hp.h11 * i1 + hp.h12 * v2 ;
        i2 = hp.h21 * i1 + hp.h22 * v2 ;
    end

    generate
        case (p)
            2 : typedef struct { real h11, h12, h21, h22; } h_params ;
            default : typedef real h_params ;
        endcase
    endgenerate

endmodule

