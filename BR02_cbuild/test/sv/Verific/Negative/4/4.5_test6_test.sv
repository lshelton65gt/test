
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a module with a type parameter. The default
// type of that is int. It applies $bits on a data element of the type. But the type
// is overridden by the bench module with void data type. Now, it is illegal to use
// $bits on void data type. Hence, this is a negative test case.

module test #(parameter width = 8, parameter type dt = byte)
             (input clk,
              input [width-1:0] in1,
              output reg [0:width-1] out1);

    dt temp;

    always@(posedge clk)
    begin
        out1 = $bits(temp) & in1;
    end

endmodule

module bench;

    parameter width = 8;

    reg clk;
    bit [width-1:0] in1;
    logic [width-1:0] out1;

    test #(.width(width), .dt(void)) t_mod (clk, in1, out1);

    always
        #5 clk = ~clk;

    always@(negedge clk)
        in1 = $random;

    initial
    begin
        clk = 0;
        $monitor("in1 = %d, out1 = %d", in1, out1);
        #501 $finish;
    end

endmodule

