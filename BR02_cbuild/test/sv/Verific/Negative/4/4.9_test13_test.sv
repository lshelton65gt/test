
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Declare a type inside interface and use it outside interface
// without redefining it. References to type identifiers defined within an interface
// through port are allowed provided they are locally redefined before being used.
// In this case, we are not referencing it through ports or redefining it, we are
// directly using the type.

interface myinterface ;

    typedef struct { bit [7:0] bit_vec; int a; } my_struct ;

endinterface

module test ;

    my_struct st ;

    assign st.bit_vec = 8'b00110101,
           st.a = 90 ;

endmodule

