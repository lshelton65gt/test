
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The default data type of enum literal is 'int'. So, it is
// illegal to declare a packed array of an enum type which is defined without any
// explicit data type. This design defines a packed array of an enumeration. The
// enumeration is defined without any explicit data type. Hence this is a negative
// test case.

typedef enum /* no data type: default 'int' */ { INVALID_MONTH, JAN, FEB,
               MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC } months ;

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2) ;

    months [4:7] m4 ;  /* error: packed array of type 'enum of type int' is not allowed */

    always@(posedge clk)
    begin
        m4 = { INVALID_MONTH, JAN, APR, JUL } ;

        out1 = ((JAN == m4[MAY]) ? in1 - in2 : '0) ;
        out2 = ((m4[JUN]<m4[JUL]) ? in2 - in1 : '0) ;
    end

endmodule

