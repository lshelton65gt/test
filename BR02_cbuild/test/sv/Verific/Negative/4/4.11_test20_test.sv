
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): All members of packed union should be of same size.
// This design declares packed unions with members having different sizes.
// This is an error.

typedef struct packed { // default unsigned 
    bit [3:0] ABC ; 
    bit [7:0] DEF ; 
    bit [11:0] GHI ; 
    bit [7:0] JKL ; 
    bit [33:0] MNO ; 
} struct64bit ; // Altotal 66 bits

typedef union packed { // default unsigned 
    struct64bit st1;           // 66 bits So creates an error.
    bit [63:0] bit_slice;      // 64 bits 
    bit [7:0][7:0] byte_slice; // 64 bits
} union64bit; 

module test ;
    union64bit u1;
    byte b ;
    bit [3:0] nib ;
    int number ;
    initial 
    begin
       number = 5000 ;
       u1.bit_slice = number ;
       if ( u1.bit_slice[33:24] == u1.byte_slice[4] && u1.st1.JKL == u1.byte_slice[4])
           $display ("Success") ;
       else $display ("Failure") ;
    end
endmodule

