
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that constant expressions of genvar variable can not
// be used in size casting.

module test(sum_out, sub_out, mult_out, in1, in2) ;
    parameter p = 10 ;
    output int sum_out[p-1:0], sub_out[p-1:0], mult_out[p-1:0] ;
    input int in1, in2 ;

    generate
        genvar i ;
        for(i = 0; i < p; i = i +1)
        begin:b
            assign sum_out[i] = (i+1)'(in1) + (i+1)'(in2) ;
            assign sub_out[i] = (i+1)'(in1) - (i+1)'(in2) ;
            assign mult_out[i] = (i+1)'(in1) * (i+1)'(in2) ;
        end
    endgenerate

endmodule

