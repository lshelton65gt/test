
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design applies $bits on void data type. This is an error.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1,
              output reg [0:width-1] out1);

    always@(posedge clk)
    begin
        out1 = $bits(void) | in1;
    end

endmodule

