
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Unpacked structures or unions cannot be declared as signed
// or unsigned. This should be treated as an error.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1) ;

    typedef union signed    // Error: unpacked union declared as signed
    {
        byte first ;
        bit [0:7] middle ;
        logic [7:0] last ;
    } p_un ;

    p_un temp ;
    bit [1:0] which ;

    always@(posedge clk)
    begin
        if (in1 > in2)
        begin
            temp.first = in1 ;
            which = 1 ;
        end
        else
        begin
            if (in2 > in1)
            begin
                temp.last = in2 ;
                which = 2 ;
            end
            else
            begin
                temp.middle = in1 | in2 ;
                which = 3 ;
            end
        end

        case (which)
            1: out1 = {8{&temp.first}} ;
            2: out1 = {8{^temp.last}} ;
            3: out1 = {8{|temp.middle}} ;
            default: out1 = {2{4'b1010}} ;
        endcase
    end

endmodule

