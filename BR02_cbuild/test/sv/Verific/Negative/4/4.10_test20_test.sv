
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Declare an enumeration with two literals having same name.
// Use the name of an enum literal of the previously declared enum as literal name
// for another literal of the next enum. 

module test ;

    enum
    {
       A_UP,
       B_UP,
       C_UP,
       C_UP,
       D_UP,
       S1
    } state ;

    enum
    {
       SET,
       RESET,
       S1,
       PREV,
       R1
    } fstate ;

endmodule

