
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Using user defined type before declaration and without declaration.

module test ;

     myint int1 ;
     myreal real1 ; 
    
     assign int1 = 10 ,
            real1 = 5.7e-05 ;
    
     typedef int myint ;

endmodule
    
