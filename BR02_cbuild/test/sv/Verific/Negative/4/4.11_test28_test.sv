
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test with different sizes for the members of a packed union.

module test;

    bit [3:0] mod_bit;

    typedef struct packed {
        int str_int;
        bit [7:0] str_bit;
        byte str_byte;
    }  my_str;

    union packed {
        my_str var1;
        bit [48:0] un_bits;
        //bit [5:0][7:0] un_bit;
        bit [5:0][6:0] un_bit;
    }  var2;

endmodule

