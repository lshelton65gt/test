
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog supports forward definition. This design uses forward
// definition and provides the actual definition inside a generate
// construct. This is an error as actual definition of forward typedef should be declared
// in the same scope.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    generate
        genvar i;

        typedef my_type_t;   // forward definition
        my_type_t t1, t2;    // use of forward defined data type

        for(i=0; i<w; i++)
        begin: for_gen_i
            always@(posedge clk)
            begin
                if (1'b0 == i[0])
                begin
                    t1[i] = in1[i] + in2[i];
                    t2[i] = in2[i] - in1[i];
                end
                else
                begin
                    t1[i] = in1[i] - in2[i];
                    t2[i] = in2[i] + in1[i];
                end
            end

            always@(posedge clk)
            begin
                out1[i] = t1[i] + t2[i];
                out2[i] = t2[i] - t1[i];
            end
        end

        if (1'b1 == w[0])    // actual definition is here
            typedef struct { bit [w/2-1:0] m; reg [1:w<<1] n; } my_type_t;
        else
            typedef logic [w-1:0] my_type_t;

    endgenerate

endmodule

