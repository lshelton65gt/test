
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design typedefines new types using typedef keyword.
// First it forward declares a new type. Then it declares other types using typedef
// in chain and finally defines the forward declared type to be the last type defined
// in the chain, making it a recursive definition. This is an error.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef cir_start;

    typedef cir_start cir_start1;
    typedef cir_start1 cir_start2;
    typedef cir_start2 cir_start3;
    typedef cir_start3 cir_start4;
    typedef cir_start4 cir_start5;

    typedef cir_start5 cir_start;

    cir_start c;
    cir_start3 b;

    always@(posedge clk)
    begin
        c = '0;
        b = '1;
        out1 = in1 | in2 * b;
        out2 = in2 - in1 ^ c;
    end

endmodule

