
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A sized constant can be used to set the size of the enumeration
// literals. All sizes must be the same.Adding a constant range to the enum declaration
// can be used to set the size of the type. If any of the enum members are defined with
// a different sized constant, this shall be an error.

module test ;
     enum bit [3:0] {bronze=5'h13, silver, gold=3'h5} medal;

     initial begin
        medal = medal.first() ;
        forever begin
                $display( "%s : %d\n", medal.name(), medal );
                if( medal == medal.last() ) break;
                medal = medal.next();
        end
      end
endmodule

