
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use a forward declared type in different places and define
// the actual definition inside for-generate. But in elaborated generate statement
// the actual definition does not appear.
// NOTE: SystemVerilog 3.0 has support for this but IEEE 1800 don't - so this is now
// a negative test case! (SV IEEE 1800 LRM section 4.9)

module test(output int out, input int in1, in2) ;

    typedef type1 ;
    typedef type2 ;

    type1 fact_res1 ;
    type2 fact_res2 ;

    function automatic int fact(input int n) ;
        if(n <= 1)
            fact = 1 ;
        else
            fact = n * fact(n -1) ;
    endfunction

    always @(in1, in2)
    begin
        fact_res1 = fact(in1) ;
        fact_res2 = fact(in2) ;

        if(in1 > in2)
        begin
            out = fact_res1 - fact_res2 ;
        end
        else
        begin
            out = fact_res2 - fact_res1 ;
        end
    end

    generate
        genvar i ;

        for(i = 0; i < 2; i = i+ 1)
        begin : b
            if(i == 0)
                typedef int type1 ;
            else
                typedef int type2 ;
        end
    endgenerate

endmodule

