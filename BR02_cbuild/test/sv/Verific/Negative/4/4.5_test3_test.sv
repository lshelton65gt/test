
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines void type task arguments in a task definition.
// It is not allowed to have void type arguments. So, this is a negative test case.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    always@(posedge clk)
    begin
        void_tsk(in1|in2, out1);
        void_tsk(in1^in2, out2);
    end

    task void_tsk(input void [width-1:0] a, output void [width-1:0] b);
        b = ~a;
    endtask

endmodule

