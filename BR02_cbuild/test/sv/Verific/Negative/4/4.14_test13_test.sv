
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design casts a bit type vector variable to a structure
// and then casts the casted structure's member to the same structure type.

typedef struct {
    int a ;
    struct { int x ; bit [3:0]y ; } var1 ;
} myType ;

myType x ;

module test ;

    typedef bit [$bits(x) - 1 : 0] bits ;
    bits b = '1 ;

    typedef int myType2 ;
    myType2 modInt ;

    initial
    begin
        x = myType'(b) ;
        modInt = x.a ;
        modInt = myType'(modInt) ;
    end

endmodule

