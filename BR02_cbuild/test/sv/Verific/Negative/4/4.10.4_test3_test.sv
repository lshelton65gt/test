
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses enumeration type to declare parameter and
// built-in methods of enumeration type and enumeration literals  are used to set
// values to those parameters.

typedef enum {red, yellow, blue, green, white} colors ;

module test ;
    colors C = yellow ;
    parameter colors e = C.next(2) ;
    bot #(e) b1() ;
    initial
       $display("%s", e.name) ;
endmodule

module bot #(parameter colors c = yellow ) ;
    parameter le = c ; 
    bot1 #(le) b2() ;
    initial
       $display("%s", c.name) ;
endmodule 

module bot1#(parameter p = 21) ;
    int r = cal(p) ;
    function int cal(int p) ;
        return p++ * ++p ;
    endfunction
    initial
       $display("%d", r) ;
endmodule

