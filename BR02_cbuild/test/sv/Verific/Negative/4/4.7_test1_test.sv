
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): On String data type arithematic operation is not allowed,
// it is an error.

module test ;
   string s = "hello" ;
   string d = "world" ;
   string a ;
   initial
   begin
      a = s * d ;
      a = s + d ;
      a = s - d ;
      a = s / d ;
      $display("The string: %s", a) ;
   end
endmodule

