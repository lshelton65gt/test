
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): An enumerated name with x or z assignments assigned to an
// enum with no explicit data type or an explicit 2-state declaration shall be a
// syntax error.

module test ;

    typedef enum { 
        total = 'X,
        age = 10
    } person;

    person p = p.first;

    initial
    begin
        forever
        begin 
           $display( "%s : %d\n", p.name, p );
           if( p == p.last ) break;
            p = p.next;
        end 
    end

endmodule

