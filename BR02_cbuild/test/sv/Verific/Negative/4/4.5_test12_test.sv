
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines void type output port for a module. This
// is not allowed. So, this is a negative test case.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output void [0:width-1] out1, out2);

    always@(posedge clk)
        out1 = in2 * in1;

    assign out2 = in1 - in2;

endmodule

