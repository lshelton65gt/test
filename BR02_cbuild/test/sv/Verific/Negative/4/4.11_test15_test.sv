
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that in packed union all members should be of same size.

module test ;

    typedef union packed {
        bit [20:0] [7:0] bit_name ;
        logic [30:0] str_name ;
        reg [7:0] byte_arr;
    } my_union ;

    my_union un[10:0] ;

endmodule

