
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Enumeration can have a data type. According to the LRM this
// data type can not be an user defined data type. This design defines an enumeration
// with union type. The type is unnamed. It is defined at the place of defining the
// enumeration. This is illegal in SystemVerilog and hence this is negative test case.

module test (input clk,
             input [3:0] in1, in2,
             output reg [0:7] out1, out2);

    enum union {
        bit [7:0] ascii_val;
        byte char_val;
    } { A = 65, B, C, D,
        E, F, G, H, I, J,
        K, L, M, N, O, P,
        Q, R, S, T, U, V,
        W, X, Y, Z = "Z"
    } alphabet;

    always@(posedge clk)
    begin
        out1 = (alphabet'(in1+A)).ascii_val;
        out2 = (alphabet'(in2+A)).char_val;
    end

endmodule

