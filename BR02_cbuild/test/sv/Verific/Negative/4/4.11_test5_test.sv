
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design has a nested structure with the inner structure
// having a member of type outer structure. This is an error.

typedef myType ;

typedef struct {
    int strInt ;
    struct {
        int a ;
        real b ;
        myType c ;
    } var1 ;
} myType ;

module test ;

    myType a ;

    initial
    begin
        $monitor("integer1 = %d, integer2 = %d, real1 = %d", a.strInt, a.var1.a, a.var1.b) ;
        repeat(20)
        begin
            a.strInt = $random ;
            a.var1.a = $random ;
            a.var1.b = {2{$random}} ;
        end
    end

endmodule

