
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Packed array is not allowed on packed data types like int.
// This design checks whether we can type define a packed array of int and then
// use that type to declare a packed array of integer.

module test (input integer a, b, c, d, output integer o) ;

    typedef int [0:3] four_int;

    four_int int1 = { a, b, c, d }; // equivalent of: int [0:3] int1

    assign o = int1[0] ^ int1[3] ^ int1[2] ^ int1[1] ;

endmodule

