
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that a cast is required for expressions that is assigned
// to enum variable where type of the expression is not equivalent to the enumeration
// type of the variable.

typedef enum { a, b, c, d } num1 ;

module test (input num1 e, e1,output num1 n, n1) ;

   assign n = e1 + 2 ; // RHS expression should be casted to enumeration type num1
   assign n1 = e - 1 ; // RHS expression should be casted to enumeration type num1

endmodule

module bench ;

   wire o1 ;
   num1 e = c ;
   num1 e1 = b ;
   num1 n, n1 ;

   test t1 (e, e1, n, n1) ;

endmodule

