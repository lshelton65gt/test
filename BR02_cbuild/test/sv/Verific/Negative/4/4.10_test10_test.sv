
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Packed arrays can only be declared of single bit type objects.
// This design defines an enumeration with data type and packed dimension. The data
// type is byte. As byte is not of type single bit, so we can not have another packed
// dimension. So this is an error and hence is a negative test case.

module test (input clk,
             input [3:0] in1, in2,
             output reg [0:31] out1, out2);

    typedef enum byte [3:0] { A = "abc", B, C, D, E, F, G, H,
                              I, J, K, L, M, N, O, P } alphabet;

    always@(posedge clk)
    begin
        out1 = alphabet'(in1+A);
        out2 = alphabet'(in2+A);
    end

endmodule

