
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows casting an element to another data type.
// This design casts data element to a type of type parameter. When the module is
// instantiated, the type is overridden with void data type. Void data type can only
// be used in function return type to indicate no return value. Hence this is a 
// negative test case.

module test#(parameter type t = int)
            (input clk,
             input [7:0] in1, in2,
             output reg [0:7] out1);

    t temp;

    always@(posedge clk)
    begin
        temp = t'(in1 & in2);
        out1 = temp;
    end

endmodule

module bench;

    parameter w = 8;

    reg clk;
    bit [w-1:0] in1, in2;
    logic [w-1:0] out1;

    test #(.t(void)) t_mod (clk, in1, in2, out1);

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in1, in2 } = $random;

    initial
    begin
        clk = 0;
        $monitor("in1 = %d, in2 = %d, out1 = %d", in1, in2, out1);
        #501 $finish;
    end

endmodule

