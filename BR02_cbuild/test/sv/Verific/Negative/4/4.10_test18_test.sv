
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines two enumerations. While the first enum
// has one of its literals assigned to an x value (the other literals remaining
// unassigned) the second enum has got two of its literals assigned the same x value.
// This is a negative test case.

module test ;

    enum integer
    {
       a,
       b = 'x ,
       c,
       d,
       e
    } alphabet ;

    enum integer
    {
       RED,
       GREEN = 'x,
       BLUE = GREEN,
       YELLOW = 'x 
    } colors ;

endmodule

