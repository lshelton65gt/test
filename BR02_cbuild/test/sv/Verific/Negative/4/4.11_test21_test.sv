
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case on testing recursive structure.

typedef myStr1;

typedef struct packed 
{
    bit [3:0] strBit;
    myStr1 str;
} myStr;

typedef struct packed
{
    bit [3:0] strBit;
    myStr str;
} myStr1;

