
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that unpacked union cannot be declared as signed or unsigned.

module test ;

    typedef union unsigned
    {
        int i ;
        real r ;
        shortreal sr ;
        integer j ;
    } my_union ;

    typedef enum 
    {
        INT_2_S,
        REAL,
        SHORTREAL,
        INT_4_S
    } my_enum ;

    typedef struct
    {
         my_union data ;
         my_enum type1 ;
    } my_struct ;

endmodule

