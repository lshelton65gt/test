
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): An enum is declared inside an union declaration. The enum
// literals should not be visible outside of the union declaration. This design
// uses the enum literals outside of the union definition.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef union packed
    {
        logic [31:0] memb1;
        enum { A, B, C } memb2;
    } t_ps_un;

    t_ps_un un1, un2, un3, un4;

    always@(posedge clk)
    begin
        un1.memb1 = 'x;
        un2.memb1 = 'z;

        if (un1 === 32'bx)
        begin
            un3.memb1 = ~(in2 - in1);
            un4.memb2 = B;
        end
        else
        begin
            if (un2 === 32'bz)
            begin
                un3.memb2 = C;
                un4.memb1 = un3.memb1;
            end
            else
            begin
                un3 = '0;
                un4 = A;
            end
        end

        out1 = un4 & un3;
        out2 = un3 ^ un4;
    end

endmodule

