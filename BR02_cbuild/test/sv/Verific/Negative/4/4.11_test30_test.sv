
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines packed structure type. A module is defined
// whose output port is of that type. A local variable is declared and connected
// to the instantiation of that module. The variable is assigned to the output port
// of the current module. SystemVerilog does not allow driving a variable from another
// place which is being driven from an always_comb block.

typedef struct packed {
    logic [3:0] a;
    logic [3:0] b;
    logic [3:0] c;
    logic [3:0] d;
} dtype;

module test(clk,i1, i2, i3, i4, i5, i6, o1);
input clk;
input [3:0] i1, i2, i3, i4, i5, i6;
output [15:0] o1;
reg [3:0] r1;

bit c1=0, c2=0, c3=0, c4=0, c5=0, c6=0;
int i=1;
dtype d1;

always @(posedge clk)
begin
    case (i)
        1: c1++;
        2: c2++;
        3: c3++;
        4: c4++;
        5: c5++;
        6: c6++;
        7: i=0;
    endcase
    i++;
end

always_comb
    r1 = (c1 ? i1 : 4'bz);

always_comb
    r1 = ((! c2) ? i2 : 4'bz);

always_comb
    r1 = (( c3) ? i2 : 4'bz);

always_comb
    r1 = ((! c4) ? i2 : 4'bz);

always_comb
    r1 = ((! c5) ? i2 : 4'bz);

always_comb
    r1 = (( c6) ? i2 : 4'bz);

bottom b1(clk,r1,d1);

assign o1 = d1;

endmodule

module bottom(clk, i1, o1);
input clk;
input [3:0] i1;
output dtype o1;

always @(posedge clk)
begin
     o1.a = i1 ^ '1;   
     o1.b = i1 ^ '0;   
     o1.c = i1 & '1;   
     o1.d = i1 | '0;   
end

endmodule

