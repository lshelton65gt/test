
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): When declaring a packed union, all the member of the union
// must be of same size. This design defines two parameters both of which have same
// default values. When the module is instantiated, the value of the parameters are
// overridden with different values. These parameters are used to declare the size
// of the members of the packed union. This should be treated as an error.

module test#(parameter w1 = 8, w2 = 8)
            (input clk,
             input [0:w1-1] in1, in2,
             output reg [w2-1:0] out1);

    typedef union packed
    {
        bit [w1-1:0] first;
        logic [0:w2-1] middle;
        bit [w1|w2-1:0] last;
    } p_un;

    p_un temp;
    bit [1:0] which;

    always@(posedge clk)
    begin
        if (in1 > in2)
        begin
            temp.first = in1;
            which = 1;
        end
        else
        begin
            if (in2 > in1)
            begin
                temp.last = in2;
                which = 2;
            end
            else
            begin
                temp.middle = in1 | in2;
                which = 3;
            end
        end

        case (which)
            1: out1 = {8{^temp.first}};
            2: out1 = {8{|temp.last}};
            3: out1 = {8{&temp.middle}};
            default: out1 = {4{2'b10}};
        endcase
    end

endmodule

module bench;

    parameter w1 = 7;
    parameter w2 = 9;

    reg clk;
    bit [w1-1:0] in1, in2;
    logic [w2-1:0] out1;

    test #(.w1(w1), .w2(w2)) t_mod (clk, in1, in2, out1);

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in1, in2 } = $random;

    initial
    begin
        clk = 0;
        $monitor("in1 = %d, in2 = %d, out1 = %b", in1, in2, out1);
        #501 $finish;
    end

endmodule

