
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Mismatch in forward declaration and actual definition of
// a variable should result in an error. The following design verifies this.

module test ;

    typedef enum x ;

    typedef bit x ; // Error: x forward declared as enum

    x a ;

endmodule

