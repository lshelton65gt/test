
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assign same value to two different enumeration literals, one
// explicitly and the other by default.

module test ;

    enum 
    {
       a = 4,
       b = 6,
       c = 7,
       d = 7,
       e
    } alphabets ;

    enum 
    {
       red = 8,
       green,
       blue = 9,
       yellow = 14
    } colors ;

endmodule

