
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): An unassigned enumerated name that follows an enum name with
// x or z assignments shall be a syntax error.

module test ;

    typedef enum integer {
        black, 
        white, 
        pink, 
        xyz = 'x,
        red, 
        blue
    } color;

    color c = c.first;

    initial
    begin
        forever
        begin 
           $display( "%s : %d\n", c.name, c );
           if( c == c.last ) break;
            c = c.next;
        end 
    end

endmodule

