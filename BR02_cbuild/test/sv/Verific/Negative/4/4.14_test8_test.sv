
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): When casting, a decimal number in place of a data type indicates
// that it is the size of the expression or the number of bits. This design uses fraction
// or real when casting the size. This is illegal and hence a negative test case.

module test(input clk,
            input [7:0] in1, in2,
            output reg [0:7] out1);

    always@(posedge clk)
    begin
        out1 = 10.5'(in1 & in2);
    end

endmodule

