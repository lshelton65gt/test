
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing the accessing of structure members of a structure 
// array without using indices.

typedef struct {
                 logic [3:0]a;
                 int b;
               }myType;

function bit [3:0] func;
    input myType in[1:0];
    bit [3:0]log;
    log = in.a;  // Error: no index given for 'in'
    return log;
endfunction

module test(input bit clk, output logic [3:0]log) ;
    always @ (posedge clk)
        log = func('{'{4'bz, 10}, '{4'b1, 5}});
endmodule

