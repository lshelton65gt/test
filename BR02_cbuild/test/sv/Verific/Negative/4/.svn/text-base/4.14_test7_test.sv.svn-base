
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In SystemVerilog a packed array cannot be assigned to an
// unpacked array. This design typedefs two types. One is of type packed array
// while the other is of type unpacked array. After defining the packed and unpacked
// arrays the packed type is casted and assigned to the unpacked array. Hence this
// is a negative test case.

module test(input clk,
            input [7:0] in1, in2,
            output reg [0:7] out1);

    typedef logic [7:0] packed_arr;
    typedef logic unpacked_arr [7:0];

    unpacked_arr u_arr;
    packed_arr p_arr;

    always@(posedge clk)
    begin
        p_arr = { in1 ^ in2 };
        u_arr = unpacked_arr'(p_arr);

        out1 = u_arr;
    end

endmodule

