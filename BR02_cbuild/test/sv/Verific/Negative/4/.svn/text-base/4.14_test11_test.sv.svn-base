
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In SystemVerilog a packed array can not be assigned to an
// unpacked array. This design defines two type parameters and they are by default
// of type packed array. When instantiating the module, one of them is overridden
// with packed type while the other by unpacked type. The packed type is casted and
// assigned to the unpacked array. This is an error and hence this is a negative test
// case.

typedef logic [7:0] packed_arr ;

module test#(parameter type ut = packed_arr, pt = packed_arr)
            (input clk,
             input [7:0] in1, in2,
             output reg [0:7] out1) ;

    ut u_arr ;
    pt p_arr ;

    always@(posedge clk)
    begin
        p_arr = { in1 ^ in2 } ;
        u_arr = ut'(p_arr) ;

        out1 = u_arr ;
    end

endmodule

module bench ;

    parameter w = 8 ;

    reg clk ;
    bit [w-1:0] in1, in2 ;
    logic [w-1:0] out1 ;

    typedef logic unpacked_arr [7:0] ;
    typedef logic [7:0] packed_arr ;

    test #(.pt(packed_arr), .ut(unpacked_arr)) t_mod (clk, in1, in2, out1) ;

    always
        #5 clk = ~clk ;

    always@(negedge clk)
        { in1, in2 } = $random ;

    initial
    begin
        clk = 0 ;
        $monitor("in1 = %d, in2 = %d, out1 = %d", in1, in2, out1) ;
        #501 $finish ;
    end

endmodule

