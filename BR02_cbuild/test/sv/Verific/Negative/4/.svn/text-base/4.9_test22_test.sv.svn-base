
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Definition of user defined types can be deferred with a forward
// declaration. This design uses a forward declared type in different places and defines
// the actual definition inside for-generate using genvar as part of the definition. This
// is an error, since both the forward and the actual definition need to be in the same scope.
// NOTE: SystemVerilog 3.0 LRM has support for this, but IEEE-1800 LRM don't - so this is
// now a negative test case according to the SystemVerilog IEEE-1800 LRM!

module test (output reg [7:0] out,
             output int int_out,
             input [7:1] in1,
             input [7:2] in2,
             input int int1, int2) ; 

    typedef \b[0].my_type ;
    typedef \b[1].my_type ;

    \b[0].my_type type_var1 ;
    \b[1].my_type type_var2 ;

    always @(in1, in2, int1, int2)
    begin
        type_var1.bit_vec = in1 ;
        type_var1.int_arr[0] = int1 ;
        type_var1.int_arr[1] = int1 + 2 ;

        type_var2.bit_vec = in2 ;
        type_var2.int_arr[0] = int2 ;
        type_var2.int_arr[1] = int2 + 2 ;
        type_var2.int_arr[2] = int2 + 4 ;

        out = type_var1.bit_vec + type_var2.bit_vec ;
        int_out = type_var1.int_arr[0] * type_var2.int_arr[0] +
                  type_var1.int_arr[1] - type_var2.int_arr[1] *
                  type_var2.int_arr[2] ;
    end

    generate
        genvar i ;

        for(i=0; i<2; i=i+1)
        begin: b
            typedef struct {
                bit [7:i+1] bit_vec ;
                int int_arr [0:i+1] ;
            } my_type ;
        end
    endgenerate

endmodule

