
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design type casts another data type into a void type.
// Since void data type is only used in the function return type to indicate that
// the function does not return any value, so it is illegal to cast to void data
// type. Thus this is a negative test case.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    always@(posedge clk)
    begin
        out1 = void'(in1 & in2);
        out2 = void'(in1 ^ in2);
    end

endmodule

