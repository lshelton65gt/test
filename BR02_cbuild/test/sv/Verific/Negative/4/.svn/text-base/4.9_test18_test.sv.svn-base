
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines two types with packed range. This types
// are used as the type of the arguments of a function declaration. The function
// manipulates some global variables that are used by other always and always_comb
// blocks. The always block manipulates and set values of other data elements that
// are used to set the output port value. SystemVerilog does not allow driving a
// variable from any other place which is being driven from an always_comb block.

typedef logic [7:0] l_type;
typedef bit [7:0] b_type;

module test(input clk, i1, i2, input [7:0] i3, output [7:0] o1);

l_type r1, r2;
reg r3;

function void fn(l_type a, b_type b);
    for(int i=0;i <8; i++)
    begin
        if(a[i] == b[i])
        begin
           r1 = 1;
           r2 = 0;
        end
        else
        begin
           r1 = 'z;
           r2 = ~b;
        end
    end
endfunction

always_comb
begin
    if(i1)
        r1 = 0;
    else
    begin
        if(r3)
            r1 ^= 1;
    end
end

always @(posedge clk)
begin
    fn(i3,{ 4 {i1,i2}});
    if(!r3 || i2)
        r2 = i3;

    if(i1)
        r3 = 0;
    else
    begin
        if(i2)
            r3 = 1;
        else
        begin
            if(r1 & '1 )
                r3 = 0;
        end
    end
end

assign o1 = 8'(r3) | r2;

endmodule

