
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): real/shortreal data types are not allowed in packed structures.
// This design defines a type parameter and that type is used in packed structure
// declaration. The default value of the type is byte, but it is overridden with real
// data type when instantiating the module. This should be treated as an error.

module test#(parameter type t = byte)
            (input clk,
             input signed [0:7] in1, in2,
             output reg signed [7:0] out1);

    typedef struct packed {
        byte first;
        t middle;
        logic [7:0] last;
    } p_st;

    p_st temp;

    always@(posedge clk)
    begin
        temp.first = in1;
        temp.last = in2;
        temp.middle = temp.first ^ temp.last;
        out1 = {8{|temp}};
    end

endmodule

module bench;

    parameter w = 8;

    reg clk;
    bit [w-1:0] in1, in2;
    logic [w-1:0] out1;

    test #(.t(real)) t_mod (clk, in1, in2, out1);

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in1, in2 } = $random;

    initial
    begin
        clk = 0;
        $monitor("in1 = %d, in2 = %d, out1 = %b", in1, in2, out1);
        #501 $finish;
    end

endmodule

