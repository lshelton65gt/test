
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Unpacked structure can not be declared as signed, because
// the bits of the structures may not be contiguous inside the memory. This design
// defines an unpacked structure as signed. This is a negative test case.

module test (input clk,
             input signed [0:7] in1, in2,
             output reg signed [7:0] out1) ;

    typedef struct signed    // Error: unpacked structure defined as signed
    {
        byte first ;
        bit [7:0] middle ;
        logic [7:0] last ;
    } p_st ;

    p_st temp ;

    always@(posedge clk)
    begin
        temp.first = in1 ;
        temp.last = in2 ;
        temp.middle = temp.first ^ temp.last ;
        out1 = {8{temp.first^temp.last&temp.middle}} ;
    end

endmodule

