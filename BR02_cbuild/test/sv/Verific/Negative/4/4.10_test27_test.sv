
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog enumerated types are strongly typed, thus, 
// a variable of type enum cannot be directly assigned a value that is not of type
// enumeration. The assignment is invalid because of the strict typing rules enforced 
// by enumerated types. 

typedef enum {
    red, 
    green,
    blue, 
    yellow, 
    white,
    black
} Colors; 

module test ;
   Colors c ;
   initial begin
       c = 1 ;
       if( 1 == c)
           $display("c has a value 1") ;
   end
endmodule

