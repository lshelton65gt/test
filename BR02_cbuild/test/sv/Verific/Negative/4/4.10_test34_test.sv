
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use class type as the data type of enum declaration -- this is an error.

class Student ;

    string name ;
    int roll ;
    string address ;

    function new ;
        name  = "default" ;
        roll = 0 ;
        address = "def" ;
    endfunction

endclass

module test(input in, output out) ;

    typedef enum Student { s1, s2, s3 }  group ; 

    group g = g.first;

    initial
    begin
        forever
        begin 
           $display( "%s : %d\n", g.name, g );
           if( g == g.last ) break;
            g = g.next;
        end 
    end

    assign out = in ;

endmodule

