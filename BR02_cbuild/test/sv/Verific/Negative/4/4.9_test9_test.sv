
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog supports forward declaration. This design uses forward
// declaration and provides the actual definition inside a generate construct. This is an 
// error as actual definition should be declared in the same scope.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    typedef my_type_t;    // forward declaration
    my_type_t t1, t2;    // use of forward defined data type

    generate
        if (w > 8)
        begin
            typedef my_type_t;    // another forward declaration with same type name
            my_type_t t1, t2;    // use of forward defined data type

            always@(posedge clk)
            begin
                t1 = in2 & in1;
                t2 = in1 - in2;

                out1 = t1 | t2;
            end

            typedef bit [w-1:0] my_type_t;   // actual definition is here
        end
        else
        begin
            typedef my_type_t;    // yet another forward declaration with same type name
            my_type_t t1, t2;    // use of forward defined data type

            always@(posedge clk)
            begin
                t1 = in2 ^ in1;
                t2 = in1 | in2;

                out1 = t1 + t2;
            end

            typedef reg [w-1:0] my_type_t;   // actual definition is here
        end
    endgenerate

    always@(posedge clk)
    begin
        t1 = in2 - in1;
        t2 = in1 - in2;

        out2 = t1 ^ t2;
    end

    typedef struct { bit [w/2-1:0] m; reg [1:w<<1] n; } my_type_t;   // actual definition is here

endmodule

