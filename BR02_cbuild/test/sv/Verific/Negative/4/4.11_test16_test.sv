
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that tri0 and wire data types are not allowed inside
// structures or unions.

module test(output out, input in1, in2) ;

    typedef union
    {
        wire sub_out;
        reg sub_out ;
    } my_union ;

    typedef struct
    {
        reg in1, in2 ;
        tri0 out ;
        my_union out_info ;
    } add_info ;

    add_info add;

    always @(in1, in2)
    begin
        add.in1 = in1;
        add.in2 = in2;
        add.out_info.sub_out = add.in1 - add.in2 ;
    end

    assign add.out = add.in1 + add.in2,
           out = add.out ;

endmodule

