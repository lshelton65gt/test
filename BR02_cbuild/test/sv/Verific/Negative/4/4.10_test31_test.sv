
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog enumerated types are strongly typed, thus,
// a variable of type enum cannot be directly assigned a value that lies outside
// the enumeration set. This is an error.

module test ;

   typedef enum { a, b, c, d } num1 ;
   num1 n ;

   initial begin

      n = 5 ;
      n = n.first;

      forever begin 
        $display( "%s : %d\n", n.name, n );
        if( n == n.last ) break;
           n = n.next;
      end 

   end

endmodule

