
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case with enum. x/z assignment to enum member 
// without explicit data type declaration.

module test;

    enum{mem1, mem2 = 'x, mem3 = 'z} var1, var2;
    initial
        $display("%d",mem1);

endmodule

