
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use a forward declared type in different places and define
// the actual definition inside named block. This is an error since, the actual
// definition should be defined in the same scope as that of the forward definition.

module test (output real drift_curr,
             input real diffusion_curr, maj_diff_curr, min_diff_curr) ;

    typedef curr_type ;

    curr_type d_curr ;

    always @(diffusion_curr, maj_diff_curr, min_diff_curr)
    begin : b
        typedef struct {
            real Dn ;
            real Dp ;
            real Ip ;
        } curr_type;
        d_curr = { maj_diff_curr, min_diff_curr, diffusion_curr } ;
        drift_curr = (d_curr.Dn/d_curr.Dp -1) * d_curr.Ip ;
    end

endmodule

