
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Tests if error is produced when the expression to be casted
// is not enclosed in parentheses or within concatenation/replication braces.

module test;

    bit [10:0] vec1, vec2;
    int int1, int2, int3;

    always @(vec1, vec2)
    begin
        int1 = int' 8(vec1) ;
        int2 = int'8(vec2) ;
        int3 = int'2{8'hab, 8'hfe};
    end

endmodule

