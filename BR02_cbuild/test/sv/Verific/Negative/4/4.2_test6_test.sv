
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design forward defines a new type using typedef keyword.
// Then it typedefines an union to be of the forward defined type. The union contains
// a member of the self type. This is an error.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef self;

    typedef union /* unpacked by default */ {
        int  memb1;
        self memb2;
        byte memb3;
    } self;

    self c;
    self b;

    always@(posedge clk)
    begin
        c = '0;
        b = '1;
        out1 = in1 | in2 * b.memb2.memb2.memb1;
        out2 = in2 - in1 ^ c.memb1;
    end

endmodule

