
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case using unpacked array in packed unions.

module test;

    union packed {
        bit un_bit [15:0];
        byte un_byte[1:0];
    } var1;

endmodule

