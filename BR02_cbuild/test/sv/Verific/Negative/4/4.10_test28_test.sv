
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The values of enumeration literals can be set for some and
// not set for others. A literal without a value is automatically assigned an increment
// of the value of the previous literal. If an automatically incremented value is 
// assigned to any other literal in the same enumeration, this shall be an error.

module test ;
   enum { a=2, b, c=3 } check ;
   initial begin
       check = check.last() ;
       forever begin
           $display("%s : %d",check.name(), check) ;
           if(check == check.first() ) break ;
               check = check.prev() ;
       end
   end
endmodule

