
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case in testing self recursive structure.

typedef struct packed {
    bit [3:0]strBit;
    myStr str;
} myStr;

module test (input myStr in, output myStr out) ;

    assign out = in ;
    
endmodule

