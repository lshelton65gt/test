
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Unpacked data type is not allowed in packed unions. This
// design defines a type parameter and that type is used in packed union declaration.
// The default value of the type is byte, but it is overridden with unpacked data type
// when instantiating the module. This should be treated as an error.

module test#(parameter type t = byte)
            (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1) ;

    typedef union packed
    {
        byte first ;
        t middle ;         // legal if t is packed type and of size 8
        logic [7:0] last ;
    } p_un ;

    p_un temp ;
    bit [1:0] which ;

    always@(posedge clk)
    begin
        if (in1 > in2)
        begin
            temp.first = in1 ;
            which = 1 ;
        end
        else
        begin
            if (in2 > in1)
            begin
                temp.last = in2 ;
                which = 2 ;
            end
            else
            begin
                temp.middle = in1 | in2 ;
                which = 3 ;
            end
        end

        case (which)
            1: out1 = {8{|temp.first}} ;
            2: out1 = {8{|temp.last}} ;
            3: out1 = {8{|temp.middle}} ;
            default: out1 = {8{1'b1}} ;
        endcase
    end

endmodule

module bench ;

    parameter w = 8 ;

    typedef logic upack [7:0] ;

    reg clk ;
    bit [w-1:0] in1, in2 ;
    logic [w-1:0] out1 ;

    test #(.t(upack)) t_mod (clk, in1, in2, out1) ;

    always
        #5 clk = ~clk ;

    always@(negedge clk)
        { in1, in2 } = $random ;

    initial
    begin
        clk = 0 ;
        $monitor("in1 = %d, in2 = %d, out1 = %b", in1, in2, out1) ;
        #501 $finish ;
    end

endmodule

