
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Attribute instance can be specified for interfaces.
// The default type of the attribute is bit with value 1. If the type
// of the value is different then (like using type casting) the attribute
// specification takes that type. This design assigns non-constant value
// to the attribute specifications, hence it is a negative test case.

int i = 1;

(* first_attr, second_attr = i, third_attr = logic'(i) *) interface iface(input clk);
    logic [7:0] in1;
    logic [7:0] in2;
    logic [7:0] out1;
    logic [7:0] out2;

    modport std(input in1, in2, output out1, out2);
endinterface

module test (iface ifc);

    always@(ifc.clk)
    begin
        ifc.out1 = ifc.in1 | ifc.in2;
        ifc.out2 = ifc.in2 ^ ifc.in1;
    end

endmodule

