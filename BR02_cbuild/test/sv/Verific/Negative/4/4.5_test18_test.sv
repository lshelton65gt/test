
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use of void data type to override parameter values.

module test ;

    reg [7:0] out, in ;

    initial
    begin
        in = 0 ;
        repeat(10) #10 in++ ;
    end

    child #(void) I(out, in) ;

endmodule

module child #(parameter type p = int)(out, in) ;

    output p out ;
    input p in ;

    assign out = in + 2 ;

endmodule

