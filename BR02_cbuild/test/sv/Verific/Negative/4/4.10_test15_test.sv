
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Enumeration can have a data type and size. This design defines
// an enum declaration with bit type and of size 2 bits and it defines only 3 enum
// literals in it, but the value of the first one is explicitly specified as 2. So,
// the last enum literal will have its value as 4 and 4 can not be accommodated in 2
// bits, so it is an error.

module test (input clk,
             input [3:0] in1, in2,
             output reg [0:7] out1, out2);

    enum bit [1:0] /* 2 bit */ {
        A = 2, B, C /* C == 4 */
        /* 3 literals */
    } err_size; // error: can not accomodate 4 in 2 bits

    always@(posedge clk)
    begin
        out1 = in1 + in2;
        out2 = in2 - in1;
    end

endmodule

