
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing forward declaration of typedef, while actual definition
// is inside for-generate.
// NOTE: SystemVerilog 3.0 has support for this but 3.1 don't - so this is now a
// negative test case! (SV IEEE 1800 LRM section 4.9)

module test(bin, gray);

    parameter size = 8;
    output bit [size-1:0] bin;
    input bit [size-1:0] gray;
    typedef myType;
    
    myType a1;

    genvar i;

    generate for (i = 0; i < size; i++)
    begin:blk
            myType a;
            typedef int myType;
            assign a = i ; 
            always @(gray[size-1:i])
            bin[i] = ^gray[size-1:i];
        end     
    endgenerate

endmodule

