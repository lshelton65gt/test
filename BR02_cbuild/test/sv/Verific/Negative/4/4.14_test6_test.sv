
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows casting element to other data types.
// This design casts data element to void data type. Void can only be used in
// function return types to indicate no return value. Hence this is a negative
// test case.

module test(input clk,
            input [7:0] in1, in2,
            output reg [0:7] out1);

    always@(posedge clk)
    begin
        out1 = void'(in1 & in2);
    end

endmodule

