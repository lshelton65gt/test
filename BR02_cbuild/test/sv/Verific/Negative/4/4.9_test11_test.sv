
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Enum types must be defined before its use. This design passes
// the enum type as the type parameter without first defining it.

module test #(parameter w = 4, parameter type p = int)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    p m1 = p'(in1), m2 = p'(in2);

    always@(posedge clk)
    begin
        case(m1)
            FEB:                               out1 = 28;
            APR, JUN, SEP, NOV:                out1 = 30;
            JAN, MAR, MAY, JUL, AUG, OCT, DEC: out1 = 31;
            INVALID_MONTH:                     out1 = 0;
            default:                           out1 = 0;
        endcase

        case(m2)
            FEB:                               out2 = 28;
            APR, JUN, SEP, NOV:                out2 = 30;
            JAN, MAR, MAY, JUL, AUG, OCT, DEC: out2 = 31;
            INVALID_MONTH:                     out2 = 0;
            default:                           out2 = 0;
        endcase
    end

`include "enum_def.vh"

endmodule

