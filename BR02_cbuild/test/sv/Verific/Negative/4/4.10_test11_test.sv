
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Enumeration can have a data type. According to the LRM this
// data type can not be an user defined data type. This design defines two enumerations
// the data type of the second enumeration being the first enumeration. This is
// illegal in SystemVerilog and hence this is a negative test case.

module test (input clk,
             input [3:0] in1, in2,
             output reg [0:7] out1, out2);

    typedef enum { A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P } alphabet;
    enum alphabet { a = A, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p } small_alphabet;

    always@(posedge clk)
    begin
        out1 = alphabet'(in1+A);
        out2 = small_alphabet'(in2+a);
    end

endmodule

