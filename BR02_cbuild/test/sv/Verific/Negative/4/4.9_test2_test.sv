
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing of forward declaration of typedef. This design declares
// a forward declaration of a type using typedef in the $root scope but defines the
// actual type declaration inside module. This is an error since actual definition
// should be in the same scope as the forward declaration.

typedef myType;

module test;
    myType var1;
    int returnValue;

    assign returnValue = func(var1);

    typedef int myType;

    initial
    begin
        var1 = 0;
        repeat(20)
            var1+=2;
        $monitor("%d = %d + 1", returnValue, var1);
    end

endmodule

function int func;
    input myType a;
    return a++;
endfunction

