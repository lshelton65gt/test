
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Enumeration can have a data type and size. This design defines
// an enum declaration with bit type and of size 2 bits. But it defines more than 4
// enum literals in it. This should be an error, because the value of the fifth literal
// (4) can not be accommodated in 2 bits.

module test (input clk,
             input [3:0] in1, in2,
             output reg [0:7] out1, out2);

    enum bit [1:0] /* 2 bit */ {
        A, B, C, D, E, F, G, H
        /* 8 literals */
    } eight_letters; // error: cannot accomodate 4 in 2 bits

    always@(posedge clk)
    begin
        out1 = in1 + in2;
        out2 = in2 - in1;
    end

endmodule

