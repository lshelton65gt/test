
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test with structure tags before '{'.

module test;

     struct str {
         int in [7:0];
         byte ch[3:0]; 
     } my_str;

endmodule

