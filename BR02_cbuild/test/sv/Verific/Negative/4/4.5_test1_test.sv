
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of void data type. This design uses 'void' as a return
// type of a function which returns a value. This is an error returns a value.

module test ;

    function void incr(input int in) ;
        return ++in;
    endfunction

    int val = incr(3);

endmodule

