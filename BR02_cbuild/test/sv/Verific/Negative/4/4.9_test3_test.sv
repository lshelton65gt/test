
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing the case where typedef forward declaration is outside
// the generate block and actual is inside if-generate.
// NOTE: SystemVerilog 3.0 has support for this but 3.1 don't - so this is now a
// negative test case! (SV IEEE 1800 LRM section 4.9).

module test(data, out);

    parameter width = 1;

    input bit [width-1:0]data;
    output int out;

    typedef myInt;

    myInt s = 1;

    generate
        if(width > 1)
        begin   
            myInt x = 2;
            mod1 #(.p(width))instanMod1(data+x, out);
        end
        else
        begin
            typedef int myInt;
            mod2 instanMod2(data+s, out);
        end     
    endgenerate

endmodule

module mod1#(parameter p = 1)(input bit [p-1:0]data, output int out);

    initial
    begin
        for(int i = p; i >= 0; i--)
            out = out + data[i];
    end

endmodule

module mod2(input bit data, output int out);

    assign out = data;

endmodule

module top;

    parameter p = 2;
    int out;
    bit [p-1:0]data;

    test #(.width(p))instanMod(data, out);

    initial
    begin
        data[p-1] = 0;
        for(int i = p - 2; i >= 0; i--)
            data[i] = ~data[i+1];
    end
        
    always @(*)
    begin
        #5 $display("input = %b", data);
        $display("output = %d", out);
    end

endmodule

