
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines parameter of type void (not a type parameter).
// Parameters can have a data type. This design defines a parameter of type void.
// It is not allowed to have parameter of type void. So, this is a negative test case.

module test #(parameter width = 8, parameter void vp = 4)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    always@(posedge clk)
    begin
        out1 = in1 * in2;
        out2 = in2 - in1;
    end

endmodule

