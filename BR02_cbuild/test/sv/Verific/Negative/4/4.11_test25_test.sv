
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case on wire type variable declaration inside
// a structure.

typedef wire myVar;

module test;

    struct {
        myVar [3:0]a;
        wire [3:0]x;
    } var1;

    initial
    begin
        myVar = '0;
        x = '1;
    end

endmodule

