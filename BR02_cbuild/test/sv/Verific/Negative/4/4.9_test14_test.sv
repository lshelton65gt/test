
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use a forward declared type in different places and define the
// actual definition inside if-generate. But in elaborated generate statement actual
// definition does not appear. Actual typedef must be in the same scope of the forward
// declaration. Here the two are in different scope.
// NOTE: SystemVerilog 3.0 has support for this but IEEE 1800 don't - so this is now a
//       negative test case! (SV IEEE 1800 LRM section 4.9)

module test(output real drift_curr,
            input real diffusion_curr, maj_diff_curr, min_diff_curr) ;

    parameter p = 10 ;

    typedef curr_type ;

    curr_type d_curr ;

    always @(diffusion_curr, maj_diff_curr, min_diff_curr)
        drift_curr = (d_curr.Dn/d_curr.Dp -1) * d_curr.Ip ;

    generate
        if(p < 10)
        begin
            typedef struct {
                real Dn ;
                real Dp ;
                real Ip ;
            } curr_type;
            assign d_curr = { maj_diff_curr, min_diff_curr, diffusion_curr} ;
        end
        else
        begin
            typedef int curr_type ;
        end
    endgenerate

endmodule

