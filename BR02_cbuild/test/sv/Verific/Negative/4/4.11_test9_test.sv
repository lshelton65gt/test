
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that structure declaration does not support a tag before {.

module test ;

    struct my_struct {
        int i ;
        bit [7:0] bit_vec ;
        byte [1:0] byte_vec ;
        byte str [7:0] ;
    } struct_var ;

endmodule

