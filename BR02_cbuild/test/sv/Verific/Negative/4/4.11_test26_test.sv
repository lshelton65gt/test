
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case in declaring wire variable in a union.

module test;

    typedef wire myVar;

    union {
        myVar [3:0]a;
        wire  [4:0]b;
    } var1;

endmodule

