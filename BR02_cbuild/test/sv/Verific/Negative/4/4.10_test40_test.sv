
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case on unassigned enum member that follows
// another enum member with x or z assignment.

module test;

    enum integer { mem1, mem2 = 'x, mem3, mem4 = 4 } var1, var2;

endmodule

