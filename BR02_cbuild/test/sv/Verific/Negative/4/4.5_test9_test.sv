
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design forward declares a new type. Inside if-generate
// the actual type is defined depending on the if-generate condition. Either it is
// a packed array of void type or packed array of logic type. Since data element of
// void type is illegal, hence this is a negative test case.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    typedef my_type;

    generate
        if (width > 8)
        begin
            typedef void [width-1:0] my_type;
        end
        else
        begin
            typedef logic [width-1:0] my_type;
        end

        my_type t1, t2;
    endgenerate

    always@(posedge clk)
    begin
        t1 = in1 | in2;
        t2 = in2 + in1;

        out1 = t1 & t2;
        out2 = t2 ^ t1;
    end

endmodule

