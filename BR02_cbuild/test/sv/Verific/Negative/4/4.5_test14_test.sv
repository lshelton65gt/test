
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case on declaring objects of void type
// inside class.

module test ;
    class node ;
       void data;
       node next ;
    endclass

    int int_val ;
    real real_val ;

    node int_list, real_list;

    node prev ;
 initial
 begin
     // create link list of integer value starting from int_val
     for (int i = 0; i < 10; i++) 
     begin
         if (int_list == null) // create head of list
         begin
             int_list = new ;
             int_list.data = int_val ;
             prev = int_list ;
         end
         else 
         begin
             node new_node ;
             new_node = new ;  // create object of class node
             new_node.data = int_val + 1 ; // store integer value as data
             prev.next = new_node ;
             prev = new_node ;
         end
     end
     // create link list of real value starting from real_val
     for (int i = 0; i < 10; i++) 
     begin
         if (real_list == null) // create head of list
         begin
             real_list = new ;
             real_list.data = real_val ;
             prev = real_list ;
         end
         else 
         begin
             node new_node ;
             new_node = new ;  // create object of class node
             new_node.data = real_val + 1 ; // store integer value as data
             prev.next = new_node ;
             prev = new_node ;
         end
     end

 end

endmodule

