
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The void type is not actually a type, it is there to indicate
// of no data type, like return type of a function when set to void indicates that it
// does not return any value. This design defines a void function and actually returns
// a value by casting it to void. This is an error.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    always@(posedge clk)
    begin
        out1 = void_return(in1 | in2);
        out2 = void_return(in1 ^ in2);
    end

    function void void_return(int n);
        return void'(~n);
    endfunction

endmodule

