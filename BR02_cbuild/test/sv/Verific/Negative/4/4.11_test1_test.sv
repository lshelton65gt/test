
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing defparam to override structure type parameter.

module test ;

  mod1 #(p.strBit(4'b1010))instanMod1() ; //LRM is not very clear whether this type of parameter overwriting is allowed.

  defparam instanMod1.p.strInt = 5 ;

endmodule

module mod1 ;

parameter struct { int strInt ; bit [31:0]strBit ;} p = '{10, 32'b101} ;

initial
  begin
      $display("Overridden parameter value (should be 5 & 1010) = %d & %b", p.strInt, p.strBit) ;
  end
  
endmodule

