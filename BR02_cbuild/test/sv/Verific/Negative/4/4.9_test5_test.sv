
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing of forward declaration of typedef outside case generate,
// while the actual definition is inside it.
// NOTE: SystemVerilog 3.0 has support for this but 3.1 don't - so this is now a
// negative test case! (SV IEEE 1800 LRM section 4.9)

typedef enum {addInt, addBit, addMultBit} add;
typedef myType;

module test(myType a, b, output int out);
    parameter add var1 = addInt;

    generate
        initial
            $display("%d bit addition");
        case(var1)
            addInt:
                begin 
                    typedef int myType;
                    intAdd instanIntAdd(out, a, b);
                end
            addBit:
                begin 
                    typedef bit myType;
                    bitAdd instanBitAdd(out, a, b);
                end
            addMultBit:
                begin
                    typedef bit [7:0] myType;
                    multBitAdd #(.p(8))instanMultBitAdd(out, a, b);
                end
            default: initial $display("ERROR, input invalid for this design");
        endcase
    endgenerate

endmodule

module intAdd(output int out, input int a, b);
  
  assign out = a + b;
  
endmodule

module bitAdd(output int out, input bit a, b);

    assign out = a + b;

endmodule

module multBitAdd#(parameter p = 1)(output int out, input bit [p-1:0] a, b);

    assign out = a + b;

endmodule

module top;

     parameter add var1= addBit;
     myType a, b;
     int out;

     test #(var1) instanMod(a, b, out);

    initial
    begin
        case(var1)
            addBit:
            begin : blk1
                typedef bit myType;
                a = 1'b0;
                b = 1'b1;
            end
            addInt: 
            begin : blk2
                typedef int myType;
                a = 5 ;
                b = 5;
            end
            addMultBit: 
            begin : blk3
                typedef bit [7:0] myType;
                a = '1;
                b = '1;
            end
        endcase
        $display("input1 %d + input2 %d = output %d", a, b, out);
    end

endmodule

