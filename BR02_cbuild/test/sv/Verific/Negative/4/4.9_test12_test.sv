
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Forward declare an enumeration type and use it before actual
// definition. Enumeration type and enumeration literals must be declared before
// their use. So, this is an error and should be detected by the tool.

module test ;

    typedef main_colors ;

    main_colors color = yellow ;

    typedef struct {
        int a ;
        main_colors c ;
        real r ;
    } my_struct ;

    typedef enum { red, green, yellow } main_colors ;

endmodule
 
