
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): It is an error to declare an enumeration with two literals
// having the same value. Even one may be explicitly specified and the other is
// derived. This design assigns value to two different literals one with direct.
// constant value and the other using ? : operator with parameter in the condition
// list. Overriding the parameter value may make it a negative test case, but by
// default it is not.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    parameter P = 4;

    enum
    {
       A = 0,
       B,
       C= ((P>4) ? 0 : 1),    // B == 1 if P<=4
       D,
       E,
       F
    } state ;

    always@(posedge clk)
    begin
        out1 = in1 ^ in2;
        out2 = in2 - in1;
    end

endmodule

