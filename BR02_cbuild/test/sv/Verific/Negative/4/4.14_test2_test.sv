
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Tests if error is produced when the expression to be casted
// is not enclosed in parentheses.

module test(sum_out, sub_out, a, b) ;

    bit [7:0] a, b;
    
    int sum_out, sub_out ;

    always @(a, b)
    begin

        sum_out = int'a+b;
        sub_out = int'a-b; 
    end

endmodule
     
