
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design declares an enum literal and assigns it the parameter
// value, defined after the enum definition. This is an error.

enum {GREEN, RED} colors;

module test ;

    enum {RED=12, GREEN=p} colors; // Error: 'p' hasn't yet been declared
    parameter p = GREEN;

    initial #5 $display ("%d", GREEN);

endmodule

