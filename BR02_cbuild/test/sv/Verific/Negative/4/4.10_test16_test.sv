
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assign x or z values to members of an enum declaration.
// The data type of the declaration is bit which is 2-state.

module test ;

    enum bit [1:0]
    {
        AND,
        OR = 2'bx0 ,
        XOR = 2'b11,
        XNOR = 2'b1z
    } my_enum ;

endmodule

