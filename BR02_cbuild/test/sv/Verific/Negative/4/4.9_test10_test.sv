
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing whether multiple forward declaration is allowed in
// different and same scope. This design forward defines same type twice in same
// scope. For which only a single actual typedef is defined later in that scope.
// In another scope, the actual of a forward typedef is not defined.

typedef myInt; // This one is not defined: error

module test;

    typedef myInt;

    myInt in = 1;

    initial
        repeat(10)
            in++;

    typedef myInt;

    myInt in1 = in;

    initial
        $monitor("%d = %d", in1, in);

    typedef int myInt;

endmodule

