
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design forward defines a new type using typedef keyword.
// Then it type defines a structure which has a member of the new type defined
// previously. That new type is an union and has a member of the previous structure
// type. This makes a circular link and hence is a negative test case.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef self1;

    typedef struct {
        int  memb1;
        self1 memb2;
        byte memb3;
    } self2;

    typedef union {
        int  memb1;
        self2 memb2;
        byte memb3;
    } self1;

    self1 c;
    self2 b;

    always@(posedge clk)
    begin
        c = '0;
        b = '1;
        out1 = in1 | in2 * b.memb2.memb2.memb1;
        out2 = in2 - in1 ^ c.memb2.memb2.memb1;
    end

endmodule

