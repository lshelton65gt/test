
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines void type ports in interfaces. Interface
// can have ports of input, output or inout type. Data type can also be specified in
// the interface ports. This design defines interface port of type void. This is an
// error.

interface iface(input void [7:0] in1, output void [0:7] out1);
    logic in2;
    logic out2;
endinterface

module test (input clk,
             input [7:0] in1, in2,
             output reg [0:7] out1,
             output [0:7] out2);

    iface if1(in2 & in1, out2);

    always@(posedge clk)
    begin
        if1.in2 = in1 ^ in2;
        out1 = if1.out1 ^ if1.out2;
    end

    bottom bot1(clk, if1);

    module bottom(input clk, iface a);

        always@(posedge clk)
        begin
            a.out1 = a.in1 + a.in2;
            a.out2 = a.in2 - a.in1;
        end

    endmodule

endmodule

