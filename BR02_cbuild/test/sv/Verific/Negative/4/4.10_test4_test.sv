
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case with same enum literal and an object in 
// the same scope.

module test(gray, bin);

enum {red,  yellow, blue, green, gray}colors;

input bit [7:0]gray;
output bit [7:0]bin;

endmodule

