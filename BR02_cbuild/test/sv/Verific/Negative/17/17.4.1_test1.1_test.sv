

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;
    int t1, t2;
    string str1, str2;

always @(posedge clk)
begin
    str2=str1;
end
    
    assert property( @(posedge clk) ##1 str1 ) ; // Error: for using variable of String data type in concurrent assertion 
      
endmodule

