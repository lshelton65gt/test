
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use of side effect operator '++' in concurrent assertion is not allowed.


module test (input int in, output out) ;

        int arr[] ;
        int sum_of_nos = 0 ;
        int pqr[] ;
        int int_default ;

     initial 
        begin
        pqr = new[10] ; 
        for (int i = 0; i < pqr.size; i++) 
            pqr[i] = i + 1 ;
        end

     assert property(##1 (++sum_of_nos) ) ; // Error: for using side effect operator '++' in concurrent assertion

endmodule

