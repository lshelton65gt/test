
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): System task calls in a sequence is illegal. This design calls
// the system task '$display' from within a sequence. Hence, it is an error.

module test (input data, input clk, output reg data_out) ;

    sequence seq1 ;
         data ##5 $fell(clk) ## 1 $display("system task invoked from sequence!") ;
    endsequence 

    property header ;
         @(posedge clk) seq1[*3] ;
    endproperty     
    
    assert property(header) $display("assertion passed") ;

    always@(posedge clk) data_out = data ;

endmodule

