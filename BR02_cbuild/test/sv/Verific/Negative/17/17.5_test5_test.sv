
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The operator |-> overlaps the end of its antecedent with the
// beginning of its consequent, the clock for the end of the antecedent must be the
// same as the clock for the beginning of the consequent. Here the sub-sequences
// s1, s2 and s3 are not clocked, so this is ambiguous. Hence this is an error.

module test (input data, input clk1, clk2, clk3, output reg data_out) ;

    sequence s1 ;
        data ##1 !data ;
    endsequence

    sequence s2 ;
        data ##2 !data ;
    endsequence

    sequence s3 ;
        data ##3 !data ;
    endsequence

    property header ;
         @(posedge clk1) s1 |-> @(posedge clk2) s2 ##1 @(posedge clk3) s3 ;
    endproperty

    assert property(header) ;

    always@(posedge clk1)
        data_out = data ;

endmodule

