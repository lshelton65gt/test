
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The possibility of an empty match data[*0:3] will make the
// sequence ambiguous whether the ending clocking event is posedge clk or posedge
// clk1. Hence this is an error.

module test (input data, input clk, clk1, output reg data_out) ;

    property seq2 ;
         @(posedge clk) data[*0:3] ##1 @(posedge clk1) !data ;
    endproperty 

    always@(posedge clk) data_out = ~data ;

    assert property(seq2) ;

endmodule

