
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): [* n] notation can be used to repeat previous expression. When
// repeating expression using [* n] notation n must be literal or constant expression.
// This design uses negative n in [* n] to repeat previous expression; this is an error
// and should be trapped.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

function reg signed [31:0] return_neg(reg [31:0] a);
    reg signed [31:0] a1 ;
    a1 = -a;
    return_neg = a1; // return negative of the argument
endfunction

always @(posedge clk)
begin
    out1 = in1 ^ in2;
    out2 = in2 - in1;
end

property p1 ;
    @(posedge clk) (in1[0][* -3] ##1 in2[width-1][* return_neg(2)] ##1 in2[0]) ;
endproperty

assert property(p1) ;

endmodule

