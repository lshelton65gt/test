module m (input in1, clk, in3, output reg out) ;

property pp1 ;
    @(posedge clk) not (in3 ##1 in1) ;
endproperty

always @ (posedge clk)
begin
  label: assert property (pp1) assert (in1) ;
end

endmodule
