module m (input in1, in2, in3, output reg out) ;


property pp ;
    in1 ##1 in3 ;
endproperty


always @ (in1 or posedge in2 )
begin
  label: assert property (pp) $display("ha ha") ;
         else $display("ho ho") ;
  // No clock inferred for procedural assert or cover property
end

endmodule
