
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Cyclic reference to the sequence is not allowed.


module test (input data, input clk, output reg data_out, input a, input b) ;

    sequence s1 ;
        @(posedge clk) (a##1 s2);    //Error: for cyclic reference ( s1 <-> s2 ) to the sequences
    endsequence

    sequence s2 ;
        @(posedge clk) (b##1 s1);
    endsequence

    always@(posedge clk) data_out = data ;

endmodule

