module m (input in1, clk, in3, output reg out) ;

property pp ;
    @(posedge clk) not (in1 ##1 in3) ;
endproperty

property pp1 ;
    @(posedge clk) not (in3 ##1 in1) ;
endproperty

always @ (posedge clk)
begin
  label: assert property (pp) assert property (pp1) ;
  // Procedural assert or cover property not allowed in action block of Concurrent or Immediate Assertion
end

endmodule
