
module test (input data, input clk, output reg data_out, input a, input b, input c, input reset);

    sequence s1(x,y) ;
        @(posedge clk) (x ##1 y);
    endsequence

    
    property prop_always(p);
      p and prop_always(p); 
    endproperty

    property check_write2;
          (prop_always(c));   
    endproperty

    always@(posedge clk)
    begin
     label: assert property(check_write2);
    end
endmodule

