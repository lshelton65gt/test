module test(input clk);

    bit a[string];

    always@(posedge clk)
    assert property (##1 a);  // Error: for using associative array in concurrent assertion

endmodule

