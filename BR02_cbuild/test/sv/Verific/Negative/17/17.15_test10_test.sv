
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a bind directive which contains two
// instantiations with the same name. This is an error.

module child1;
  parameter p = 10 ;
endmodule

module child2;
  parameter p = 10 ;
endmodule

bind test.I5 child1 #(3) I1(), I1() ; // Error : Two instantiations with the same name

module test ;

  middle #(4) I5() ;
  middle I2() ;
  middle #(4) I3() ;
endmodule

module middle ;
  parameter q = 20 ;
endmodule

