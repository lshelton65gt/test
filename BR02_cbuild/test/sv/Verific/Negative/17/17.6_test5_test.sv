// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Cyclic reference to the sequence is not allowed. 


module test (input data, input clk, output reg data_out, input a, input b, input c);

    sequence s1(x,y) ;
        @(posedge clk) (x ##1 y);
    endsequence

    sequence s2 ;
        @(posedge clk) (c##1 s1($,b));   
    endsequence

    label: assert property (s2);

endmodule

