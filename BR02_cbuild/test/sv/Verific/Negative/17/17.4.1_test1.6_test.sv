
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Using a dynamic array in concurrent assertion is not allowed. 


module test(input clk) ;

    integer s[];
    initial
    s = new[10];
   
    always@(posedge clk)
    assert property (##1 s);  // Error: for using a dynamic array in concurrent assertion

endmodule

