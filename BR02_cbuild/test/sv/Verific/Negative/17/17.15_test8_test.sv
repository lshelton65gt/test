
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): As per SV IEEE Std 1800 LRM, hierarchical references to 
// a bind_instantiation's paramters may not be used outside the instantiation
// in any context that requires a constant expression. The following design
// tests this.

interface ifc(input clk) ;
    parameter w = 8 ;
    const logic w1 = test.D1.w ;
    
    reg [0:w-1] i1, i2 ;
    reg [w-1:0] o1, o2, o3 ;

    modport mp(input i1, output o1) ;
    modport mp2(input i2, output o2) ;
endinterface: ifc

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2) ;

    ifc if1(clk),
        if2(clk) ;
    bot bot_1(if1) ;
    bot bot_2(if2) ;

    always@(negedge clk)
    begin
        if1.i1 = ~in1 ;
        if2.i2 = -in2 ;
    end

    always@(posedge clk)
    begin
        out1 = -if1.o1 ;
        out1 = ~if2.o2 ;
    end


endmodule
    module bot(ifc if1) ;

        always@(posedge if1.clk)
        begin
            if1.o1 = ~if1.i1 ;
        end

    endmodule

module dummy #(parameter w = 8)(input in, output out) ;
    assign out = in ;
endmodule

bind test dummy D1 (i1, o3) ;
