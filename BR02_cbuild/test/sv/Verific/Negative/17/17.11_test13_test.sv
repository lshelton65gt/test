
module test (input data, input clk, output reg data_out, input a, input b, input c, input reset);

    sequence s1(x,y) ;
        @(posedge clk) (x ##1 y);
    endsequence

    property check_write;
    bit e,f;
    disable iff (reset)
            disable iff (clk)
            ##1 b |-> ##[2:10] c;
      
    endproperty

endmodule

