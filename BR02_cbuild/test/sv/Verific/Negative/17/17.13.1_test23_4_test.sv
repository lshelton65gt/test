module m (input in1, in2, in3, output reg out) ;

property pp ;
    not (in1 ##1 in3) ;
endproperty

always @ (posedge in2)
begin
  out <= in1 ; #10 ;
  label: assert property (pp) $display("ha ha") ;
             else $display("ho ho") ;
  // Procedural assert or cover property not allowed after delay or event
end

endmodule
