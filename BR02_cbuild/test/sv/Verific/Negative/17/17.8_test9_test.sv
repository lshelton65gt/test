

module test (input data, input clk, output reg data_out, input a, input b, input c);

    sequence s1(x,y) ;
    @(posedge clk) (x ##1 y);
    endsequence

    sequence s2 ;
       bit s;
        @(posedge clk) (c ##1 s1(s,c));     
    endsequence

    always@(posedge clk) begin
      data_out = data ;
        assert property (s2);
   end

endmodule


