
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Concurrent assertion has been used inside initial block
// and no clock is associated with the assertion either explicitly or implicitly
// Concurrent assertions should be associated with some clock.

module test #(parameter width=8)
             (input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

time t1;
bit [width-1:0] b1, b2;

initial
begin
    t1 = $time;
    assert property (##1 out1>out2) 
        $display("out1 > out2; started at time %d, executed at time %d", t1, $time);
    else
        $display("out1 <= out2; started at time %d, executed at time %d", t1, $time);

    b1 = { in1[0:width/2-1], in2[width/2:width-1] };
    b2 = in1>>width/2 ^ in2<<width/2;
    out1 = in2 + in1 ^ b1;
    out2 = in1 | in2 - b2;
end

endmodule

