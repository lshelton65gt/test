
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines bind directive inside module. Undeclared
// instance names are specified as target. This is an error, bind directive should 
// specify declared instance name as target.

module child1;
  parameter p = 10 ;
endmodule

module child2;
  parameter p = 10 ;
endmodule


module test ;
bind I1 child1 #(3) I1() ;
bind I2 child1 #(4) I3() ;
bind middle child2 #(3) I2() ;

  middle #(4) I4() ;
  middle I6() ;
  middle #(4) I5() ;
endmodule

module middle ;
  parameter q = 20 ;
endmodule

