

module test (input data, input clk, output reg data_out, input a, input b, input c, input reset);

    property check_write;
       1'b1[*0] |-> ##[2:10] c;   //Error: Antecedent of an overlaping implication should not be degenerate
    endproperty

    always@(posedge clk)
    begin
      assert property(check_write);
    end
endmodule

