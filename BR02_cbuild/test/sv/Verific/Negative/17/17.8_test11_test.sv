
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Local variable doesn't flow out of the composite sequences in case of OR

module test (input data, input clk, output reg data_out, input bit a, input bit b, input bit c);

    sequence s1(x,y) ;
        bit x1;
      ((1'b1, x1=x) ##1 x1 ) or (x1 ##1 y);  //Error: should flag as a local variable doesn't flow out of the composite sequences - since it doesn't flow out of each of the operand sequences - in case of OR 
    endsequence

    sequence s2 ;
        @(posedge clk) (c##1 s1(c,b));
    endsequence

    always@(posedge clk)
    begin
       data_out = data ;
       assert property (s2);
    end
endmodule

