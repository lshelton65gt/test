

module test (input data, input clk, output reg data_out, input a, input b, input c);

    sequence s1(x,y) ;
        @(posedge clk) (x ##1 y);
    endsequence

    sequence s2 ;
       bit s;
        @(posedge clk) (c##1 s1((s==c),b).ended);  // Error: local variables can be passed only as entire actual argument to sequence on which ended is applied.   
    endsequence

    always@(posedge clk) data_out = data ;

endmodule


