

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;
    int t1, t2;
    event data;

    assert property(@(posedge clk)##1 data ) ; // Error: for using variable of event data type in concurrent assertion 
      
endmodule

