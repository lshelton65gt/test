
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Concurrent assertions has to be associated with a clock.
// Here global assert statement is not associated with any clock but assert 
// inside always block is associated with the clock of the always block. 

wire out1, out2 ;
    assert property(##1 out1!=out2) 
        $display("out1 != out2;  executed at time %d" , $time);
    else
        $display("out1 == out2;  executed at time %d", $time);


module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

time t1;
logic [width-1:0] l1, l2;

always @(posedge clk)
begin
    t1 = $time;
    assert property(##1 out1!=out2) 
        $display("out1 != out2; started as time %d, executed at time %d", t1, $time);
    else
        $display("out1 == out2; started as time %d, executed at time %d", t1, $time);

    l1 = in1<<width/2 ^ in2>>width/2;
    l2 = { in1[width/2:width-1], in2[0:width/2-1] };
    out1 = in1 ^ in2 * l2;
    out2 = in1 + in2 & l1;
end

endmodule

