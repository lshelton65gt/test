
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Any sequence which is a property shouldn't be degenerate.

module test (input data, input clk, output reg data_out, input a, input b, input c, input reset);

    property check_write;
        1'b1[*0];                //Error: Any sequence which is a property shouldn't be degenerate
    endproperty

    always@(posedge clk)
    begin
      label: assert property(check_write);
    end

endmodule

