
module test (input data, input clk, output reg data_out, input a, input b, input c, input reset);

    property prop_always(p);
      p and (1'b1 or prop_always(p)); 
    endproperty

    always@(posedge clk)
    begin
     label: assert property(prop_always(c)) else $display("Test case");
    data_out <= data;
   $display("Test case2");
    end

endmodule

