
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A concurrent assertion statement can be used within an always
// block. The clock is inferred from the event control of always block. Inferred clock
// expression variables must not be used anywhere in the always block. This design uses
// inferred clock variable in the pass statement of always block. This is an error.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    assert property(in2[0] && in1[0][*1:2]) @(posedge clk);

    r1 = in1<<width/2 | in2>>width/2;
    r2 = in2<<width/2 | in1>>width/2;

    out1 = r1 + r2;
    out2 = r1 ^ r2;
end

endmodule

