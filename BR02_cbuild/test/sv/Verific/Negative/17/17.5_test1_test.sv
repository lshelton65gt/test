
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A number of steps can be skipped in sequential assertions,
// using various notations. When skipping steps using the (n) notation, n must not
// be a negative number. This design uses negative n in (n) to skip steps; this is
// an error and should be trapped.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

function reg signed [31:0] return_neg(reg [31:0] a);
    reg signed [31:0] a1 ;
    a1 = a ;
    a1[31] = 1'b1; // make it negative
    return a1;
endfunction

reg [0:width-1] r1, r2;
reg [0:width-1] n1, n2;

always @(posedge clk)
begin
    n1 = in1 ;
    n2 = in2 ;
    r1 = ++n1<<width/2 | n2>>width/2;
    r2 = n1 & --n2;

    out1 = r1 | r2;
    out2 = r1 ^ r2;
end

property p1 ;
    @(posedge clk) (in1[0] ##(-2) in2[width-1] ##(-0) in1[width-1] ##(return_neg(2)) in2[0]) ;
endproperty

assert property (p1) ;

endmodule

