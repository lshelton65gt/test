module m (input in1, in2, in3, output reg out) ;


property pp ;
    in1 ##1 in3 ;
endproperty


always @ (posedge in2 or in1)
begin
  out <= in2;
 
  label: assert property (in1 ##1 in2) $display("ha ha") ;
         else $display("ho ho") ;
  // No clock inferred for procedural assert or cover property
   
  // out <= in1;

end
endmodule
