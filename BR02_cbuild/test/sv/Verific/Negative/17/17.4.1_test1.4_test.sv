
class Album ;
    string singer ;
    string producer ;
    int cost ;
    int total_number_of_songs ;

    function new(string s, string p, int c, int t) ;
        singer = s ;
        producer = p ;
        cost = c ;
        total_number_of_songs = t ;
    endfunction

    function void show ;
        $display("%s, %s, %d, %d", singer, producer, cost, total_number_of_songs) ;
    endfunction

endclass

module bench(input clk) ;
   
    Album a = new("sad","sds",23,45) ;
    assert property ( @(posedge clk) ##1 a);  // Error: for using class variable in concurrent assertion. 
endmodule

