
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In Immediate assertions sequence is enforced as a checker.
// Sequences can only be used in concurrent assertions. Immediate assertions are
// checked on expressions. So, this should be an error.

module test (input clk, output reg d, input sig1, input sig2) ;

      sequence s1;
           sig1 ##4 sig2;
      endsequence

      function int ch() ; 
          assert (s1) return 1 ;
                      else return 0 ;
      endfunction

      always@(posedge clk)
      begin
         assert (ch()) ;
         d = sig1 & sig2 ;
      end

endmodule

