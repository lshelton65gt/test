
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Cyclic dependency between 3 sequence. This design defines
// three sequences which are not independent and also they depends on one another
// in such a way that there is a circle. This is an error.

module test (input data, input clk, output reg data_out) ;

    sequence seq1 ;
          data ##5 seq3 ;
    endsequence 

    sequence seq2 ;
          clk ##1 seq3 ##2 !data ; 
    endsequence 

    sequence seq3 ;
          seq1 ##2 seq2 ;
    endsequence

    property header ;
         @(posedge clk) data ##1 seq1 ##1 seq2;
    endproperty     

    assert property(header) ;

    always@(posedge clk) data_out = ~data ;

endmodule

