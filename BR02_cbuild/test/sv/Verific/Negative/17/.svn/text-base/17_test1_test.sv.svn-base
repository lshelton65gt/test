
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Concurrent assertions by definition takes time. A function
// should return immediately, hence they cannot be used in functions. This design
// defines a function and uses a concurrent assertion inside that function. This
// is an error.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    r1 = fn(clk, 0, 0);
    r2 = fn(clk, in1, 0);
    r1 = fn(clk, r1, in2);
    r2 = fn(clk, in1, r2);

    out1 = r1 - r2;
    out2 = r2 * r1;
end


function [0:width-1] fn(clk, reg [width-1:0] a, b);
    int t1, t2;

    assert property(##1 a>0 && b<0) ; // Error: concurrent assertion not allowed in task/function

    t1 = a | b;
    t2 = a + b;

    return (t1 ^ t2);
endfunction
endmodule

