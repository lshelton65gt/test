
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): [* n] notation can be used to repeat previous expression.
// When repeating expression using [* n] notation n must be literal or constant
// expression. This design uses non-constant n in [* n] to repeat previous expression;
// this is an error and should be trapped.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    r1 = in1;
    r2 = 4;

    r1 = ((in1[0])?(-in1|in2):(in1^-in2));
    r2 = in2-in1|r1;

    out1 = r1 & ~r2;
    out2 = ~r2 ^ r1;
end

property p1 ;
    @(posedge clk) ((in2[width-1] [* r1]) ##1 (in1[0] [* r2]) ##1 (in2[0]) ##1 (in1[width-1]));
endproperty

assert property(p1) ;

endmodule

