module m (input in1, clk, in3, output reg out) ;

property pp1(p) ;
  disable iff(in3)
    p; 
    // Operator "not" can not be applied to recursive properties
endproperty

always @ (posedge clk)
begin
  label: assert property (pp1(in1)) ;
end

endmodule
