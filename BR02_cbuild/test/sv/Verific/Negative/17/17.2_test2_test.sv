
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows immediate asserts to be nested. That is pass
// or fail statement of an assertion can be another assert statement. This design defines
// the pass statement of an assertion to be another assert statement, but it does not have
// a semi-colon at the end of the assert statement.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = in2 * in1;
    out2 = in1 | in2;

    assert(in2>in1)
        @(posedge clk) assert(in2==in1)
            @(posedge clk) assert (in1>in2)
                @(posedge clk) assert (in1!=in2)
end

endmodule

