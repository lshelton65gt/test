
module test (input data, input clk, output reg data_out, input a, input b, input c);

    sequence s1(x,y) ;
        @(posedge clk) (x ##1 y);
    endsequence

    property check_write;
    bit e,f;
    disable iff (s1(e,f).ended) //Error: .matched and .ended are not allowed in the reset expression 
       ##1 b |-> ##[2:10] c; 
    endproperty

    always@(posedge clk)
     begin
      assert property (check_write);
     end

endmodule

