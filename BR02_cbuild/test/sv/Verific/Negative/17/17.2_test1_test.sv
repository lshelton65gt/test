
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The assert expression of immediate assertion as a whole can not be empty. 
// This design uses an empty expression in assert statement. This is an
// error and should be flagged by the tool.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    assert() @(posedge clk);

    r1 = in1 | in2;
    r2 = in2 - in1;

    out1 = r1 & r2;
    out2 = r1 + r2;
end

endmodule

