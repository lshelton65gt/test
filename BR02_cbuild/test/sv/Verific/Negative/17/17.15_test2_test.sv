
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines three bind directives. Two bind directives 
// are trying to add two different instantiations of same name to target module.
// This is an error.

module child1;
  parameter p = 10 ;
endmodule

module child2;
  parameter p = 10 ;
endmodule

bind test.I5 child1 #(3) I1() ;
bind test.I2 child1 #(4) I2() ;

module test ;

  bind middle child2 #(3) I1() ; // I1 is already declared in test.I1 by bind 
  middle #(4) I5() ;
  middle I2() ;
  middle #(4) I3() ;
endmodule

module middle ;
  parameter q = 20 ;
endmodule

