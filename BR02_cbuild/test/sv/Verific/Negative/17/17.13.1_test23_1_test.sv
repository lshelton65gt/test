module m (input in1, in2, in3, output reg out) ;

property pp ;
    @(posedge in2) not (in1 ##1 in3) ;
endproperty

always @ (posedge in2)
begin
  label: assert property (pp) $display("ha ha") ;
         else $display("ho ho") ;
  // Inferred and specified clocks do not match
end

endmodule
