

module test (input data, input clk, output reg data_out, input bit a, input bit b, input bit c);

    sequence s1(x,y) ;
        bit x;               // Error: Local variable ( bit x; )should not have the same name as the formal argument ( x )
      (1'b1, x=y) ##0 ##1 y;
    endsequence

    sequence s2 ;
        @(posedge clk) (c##1 s1(c,b));
    endsequence

    always@(posedge clk) data_out = data ;

endmodule

