
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In the design two bind directives use same target instance
// and same instantiation name (to be added). This is an error as same target can 
// not have two instantiations of same instance name.

module child1;
  parameter p = 10 ;
endmodule

module child2;
  parameter p = 10 ;
endmodule

bind top.I3.I2 child2 #(3) I5() ;
module top ;
  test I3() ;
  test I4() ;
  bind top.I3.I1 child1 #(3) I1() ;
  bind top.I3.I2 child1 #(4) I5() ;
  bind top.I3.I2 child2 #(3) I2() ;
endmodule
module test ;


  middle I1() ;
  middle I2() ;
  middle I3() ;
endmodule

module middle ;
  parameter q = 20 ;
endmodule

