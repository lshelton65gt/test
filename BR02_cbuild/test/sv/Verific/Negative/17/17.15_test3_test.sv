
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines bind directive inside a module that
// is instantiated twice inside top level module. This is an error, as both
// instances of the instantiated module will try to add same instance to target module.

module child1;
  parameter p = 10 ;
endmodule

module child2;
  parameter p = 10 ;
endmodule

module top ;
  test I3() ;
  test I4() ;
endmodule
module test ;

  bind top.I3.I1 child1 #(3) I5() ;
  bind top.I3.I2 child1 #(3) I6() ;

  middle I1() ;
  middle I2() ;
  middle I3() ;
endmodule

module middle ;
  parameter q = 20 ;
endmodule

