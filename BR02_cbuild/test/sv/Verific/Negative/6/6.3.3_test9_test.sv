
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines an 'if-generate' construct. Inside the
// 'if-generate' it uses a 'case-generate' statement. The condition of the 
// 'case-generate' statement is the type parameter of the module and case items
// are data types. This is not allowed and hence it is a negative test case.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output [w-1:0] out1, out2);

    module bottom #(parameter type t = int,
                    parameter p = 8)
                   (input clk,
                    input [p-1:0] in,
                    output reg [p-1:0] out);

        t [p-1:0] r1;

        generate
            if (p<8)
                always@(posedge clk)
                begin
                    r1 = ~in;
                    out = r1 + 1'b1;
                end
            else
                case(t)    // error: using type parameter as case condition
                    int:
                        always@(posedge clk)
                        begin
                            r1 = ~in;
                            out = r1 - 1'b1;
                        end
                    logic:    // error: using data types as case items
                        always@(posedge clk)
                        begin
                            r1 = ~in;
                            out = r1 & 1'b1;
                        end
                    default:
                        always@(posedge clk)
                        begin
                            r1 = ~in;
                            out = r1 ^ 1'b1;
                        end
                endcase
        endgenerate
    endmodule

    bottom bot1(clk, in2, out1);
    bottom #(.p(8), .t(reg)) bot2(clk, in1, out2);

endmodule

