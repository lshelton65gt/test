
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a structure type. It is used as the data type
// for a parameter of a module. The default value of that parameter
// is specified as structure literal. The parameter value is overridden by the instantiating module.
// This parameter is used to define range of port declaration. Range bounds requires constant
// expression and hierarchical identifier is not allowed in constant expression. So, this is
// an error.

typedef struct
{
    int in_width;
    int out_width;
} my_struct_t;

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output [w-1:0] out1, out2);

    bottom #({w, w}) bot1(clk, in1, out2);
    bottom #({w, w}) bot2(clk, in2, out1);

endmodule

module bottom #(parameter my_struct_t p = '{ 2, 8 })
               (input clk,
                input [p.in_width-1:0] in,
                output reg [p.out_width-1:0] out);

    always@(posedge clk)
    begin
        out = ~in;
    end

endmodule

