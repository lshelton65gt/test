
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S):  Negative test case on hierarchical access of a data 
//  declared in an unnamed block.

module test;
initial
begin
	int i = 10;
end
endmodule

module top;
test instan_mod();
assign instan_mod.i = 5;
endmodule
