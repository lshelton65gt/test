
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Data declared inside an automatic function or task or a block,
// is local to that function or task or the block and automatic in lifetime of the
// function call or task invocation or block in execution. This design
// hierarchically references these type of automatic variables from outside
// the block. This is an error, hence this is a negative test case.

module test (input clk,
             input [7:0] in1,
             output reg [7:0] out1);

    int prev, after;

    task automatic tsk(input int i, output int o);
        int temp;    // automatic, as it is declared inside automatic task

        if (i<$bits(i))
            temp = 0;
        else
            temp = 1;

        if (temp)
            o = i*i;
        else
            o = i;
    endtask

    always@(posedge clk)
    begin
        prev = tsk.temp;    // error: automatic variable inside automatic function referenced hierarchically

        tsk(in1, out1);

        after = tsk.temp;    // error: automatic variable inside automatic function referenced hierarchically

        out1 = (after != prev)?(++out1):(--out1);
    end

endmodule

