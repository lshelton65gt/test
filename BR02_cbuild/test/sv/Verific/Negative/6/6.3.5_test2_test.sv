
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing on passing of const type data to parameter.

module test (out, in);

    parameter p = 8;

    output [p-1:0] out;
    input logic [p-1:0] in;

    assign out = in;
  
    initial
    begin
        in = '1;
	$display("%b = %b", out, in);
    end

endmodule

module top;

    const bit [3:0] con = {4{1'b1}};
    logic [15:0] in, out;

    mod #(.p(con)) instanMod (.*);        // Error: constants are set during simulation,

endmodule

