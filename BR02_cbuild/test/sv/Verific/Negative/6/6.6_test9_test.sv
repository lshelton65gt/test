
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case with automatic variable used in 
// non-blocking assignments.

module test;
    int in = 0;
initial
begin : blk1
    automatic int mod_int = 10 ;
    in = 5;
    #10   mod_int <= 2;
    in = 0;
end
endmodule
