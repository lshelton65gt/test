
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of uninitialized constant. 

module test(input int in, output int out);

   int i;
   const int val ;

   always @ *
   begin
       for(i = 0; i < 10; i = i + 1)
       begin : b
           int offset = val;
           out = in + offset;
       end
   end

endmodule

