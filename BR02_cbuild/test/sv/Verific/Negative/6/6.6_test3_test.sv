
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that automatic variables cannot be written 
// with a non-blocking statement.

module test(output logic q, input logic data, enable);

    always @ (data, enable)
    begin
        automatic logic tmp = 1'b0 ;

        if(enable)
            tmp <= data;           // Error: Non-blocking statement having an automatic variable as LHS

        q = tmp;
    end

endmodule
