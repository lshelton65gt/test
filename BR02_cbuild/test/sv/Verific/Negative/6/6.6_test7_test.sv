
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case in hierarchical access of automatic variables.

module test;
initial
begin : named_block
	automatic int mod_int = 0;
end
assign named_block.mod_int = 5;
endmodule
