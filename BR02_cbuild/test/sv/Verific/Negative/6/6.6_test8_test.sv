
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case with automatic variable used in event.

// Note: As per SV 3.1 LRM section 5.5: automatic or dynamic variables cannot be
// used to trigger an event expression. However the corresponding section 6.6 of
// P1800 does not specify anything about this.

module test;

    initial
    begin: blk
        automatic int i ;
        i = 0;
        #5 i = 5;
        @(i) $display($time,,"Failure\n");
    end

endmodule

