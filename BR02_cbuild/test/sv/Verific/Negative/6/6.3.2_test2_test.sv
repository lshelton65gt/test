
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a value parameter, but pass data type
// as overridden value of that parameter. This is an error.

typedef struct packed { bit [0:3] f1, f2; } TYPE1;
typedef bit [0:3][0:3] TYPE2;
typedef bit [0:7] TYPE3;
typedef TYPE1 TYPE4;

typedef enum {t1, t2, t3, t4} typeNo;

module test(output [0:3]out, input [0:15] in);
parameter typeNo p = t1;

    generate
        case (p)
            t1:
                begin : b1
                    TYPE1 in1, in2;
                    assign {in1, in2} = in;
                    bot #(TYPE1)  i1 (.*);
                end : b1
            t2:
                begin : b2
                    TYPE2 in1, in2;
                    assign {in1, in2} = in;
                    bot #(TYPE2)  i1 (.*);
                end : b2
            t3:
                begin : b3
                    TYPE3 in1, in2;
                    assign {in1, in2} = in;
                    bot #(TYPE3)  i1 (.*);
                end : b3
            default:
                begin : b4
                    TYPE4 in1, in2;
                    assign {in1, in2} = in;
                    bot #(TYPE4)  i1 (.*);
                end : b4
        endcase
    endgenerate
endmodule


module bot #(parameter T = 4)
    (input [T-1:0] in1, input [T-1:0] in2, output [T-1:0] out);

    assign out = in1 > in2;

endmodule

