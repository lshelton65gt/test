
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): '<<<' and '>>>' operator can not be applied on (short)real type
// data elements. This design passes real data type through type parameter to another
// module. That module uses '<<<' and '>>>' on the data element declared of the type
// passed to it. So if the type is 'real'/'shortreal' then it should be treated as an
// error. This checks whether such a condition can be handled properly by the tool or not.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output [w-1:0] out1, out2);

    bottom #(.dt1(real), .p(w))
           bot1 (clk, in1, in2, out1);

    bottom #(.dt1(shortreal), .p(w))
           bot2 (clk, in2, in1, out2);

endmodule

module bottom #(parameter p = 4,
                parameter type dt1 = byte)
               (input clk,
                input [p-1:0] in1, in2,
                output reg [p-1:0] out);

    dt1 t1, t2;

    always@(posedge clk)
    begin
        t1 = in2 - in1;
        t2 = in1 - in2;

        out = t1 >>> (p/2) | t2 <<< (p/2);    // only possible is dt1 is not real/shortreal
    end

endmodule

