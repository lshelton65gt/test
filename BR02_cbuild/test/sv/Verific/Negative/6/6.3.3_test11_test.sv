
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing type parameter in nested module.

bit [3:0] bit1;

module m1 #(parameter type p = int, parameter p p1 = 1)(output bit[p1:0]out);
    assign out = '1;
endmodule

m1 #(.p(byte))I1(bit1);

module m2;
    parameter type p2 = int;
    int size = m3.p;
    p2 c[7:0] = "HELLO";
endmodule

m2 #(.p2(byte))I2();

module m3;
    parameter p = 7;
    module m1;
        parameter type p1 = bit;
        p1 [p:0]a = 'x;
    endmodule
    
    m1 #(byte) I3(); // Error: As packed array of bytes is not allowed
endmodule

m3 I3();

module test;

    initial
    begin
        $display("Expected output : m1.out = 11, HELLO, 0");
        $display("m1.out = %b", I1.out);
        for(int i = m3.size; i >= 0; i--)
            $display("%c", I2.c[i]);
        $display("%b", I3.I3.a);
    end
    
endmodule

