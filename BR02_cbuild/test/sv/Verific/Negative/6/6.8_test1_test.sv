
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The members of an alias list are signals whose bits share
// the same physical nets. This is illegal because the bits of w are being aliased
// to itself 

module test (inout int w[31:0], input int a[31:0], b[31:0]); 

    alias w[7:0] = a[7:0] ;
    alias w[31:8] = {b[0:15], w[31:24]};
    
endmodule 

