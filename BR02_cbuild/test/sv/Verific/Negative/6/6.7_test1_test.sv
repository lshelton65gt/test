
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design has a continuous assignment and a 
// procedural assignment writing to the same logic variable. This is an error.

module test(input in, output reg out);
logic mod_log;
assign mod_log = 1; // continuous assignment to mod_log
always @(in)
begin
    mod_log = ~in; // procedural assginment to mod_log
    out = in + mod_log ;
end
endmodule
