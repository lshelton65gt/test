
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design has a continuous assignment(through port) and
// a procedural assignment writing to the same logic variable. This is an error.

module test(output logic mod_log);
initial 
begin
	mod_log = 0;
end
endmodule

module top;
logic top_log;
initial top_log = 1;
test instan_mod(top_log);
endmodule
