
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of constant declaration where the value of
// the constant is set by calling a constant function.

module test(address, write, read, data);

    const int addrs_width = fact(3) + 2;
    input [addrs_width - 1:0] address; // Error: const evaluated at simulation time
    input write, read;
    inout [7:0] data;

    function automatic integer fact;
        input integer n;
        begin
            if(n == 0)
                fact = 1;
            else
                fact = n * fact(n-1);
        end
    endfunction

    logic [7:0] data_store [255 :0];

   always @ *
    begin
        if(write)
            data_store[address] = data;
    end

    assign data = (read) ? data_store[address] : 0 ;

endmodule
