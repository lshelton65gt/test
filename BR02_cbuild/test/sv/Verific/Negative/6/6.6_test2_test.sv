
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that automatic variables can not be accessed 
// through hierarchical reference.

module test(input int num, output int fact_res);

    function int fact;
        input int in;
        automatic int result;

        begin
        if(in == 0)
            result = 1;
        else
            result = in * fact(in - 1);
        fact = result;
        end
    endfunction

    always @ (num)
    begin
        fact(num);
        fact_res = fact.result;
    end

endmodule

