
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing const in enum.

module test;
const int con = instanMod1.p;
enum {mem1 = con, mem2, meme3}var1;       // Error: constants set during simulation time

initial
  begin
      var1 = mem1;
	  $display("Var1 = (should be 5)%d", var1);
  end

test1 instanMod1();

endmodule

module test1;

  parameter p = 5;

endmodule


