
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case on overriding const.

const bit[3:0]con = 10;

module test;

initial
  begin
      $root.con = '1;
	  $display("NEGATIVE TEST CASE");
  end

endmodule


