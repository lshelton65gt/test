
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Automatic variables can not be written with a non-blocking statement.
// This design writes to an automatic variable through a non-blocking
// statement. This is an error, hence this is a negative test case.

module test (input clk,
             input [7:0] in1,
             output reg [7:0] out1);

    always@(posedge clk)
    begin: blk
        automatic logic [7:0] non_blocking ;
        non_blocking <= in1;
        out1 <= non_blocking;
    end

endmodule

