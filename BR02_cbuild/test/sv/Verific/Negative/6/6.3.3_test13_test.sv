
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Parameter of a module can be a type. This design defines a module and
// a type parameter for that. The parameter type is union by default. As port variable cannot 
// of type struct/union, so, this is an error.

module test #(parameter type t = union packed { int i; real r; bit t;})
             (input clk,
              input t in1, in2,
              output t out1, out2);

    always@(posedge clk)
    begin
        if (in1.t)
            if (in2.t)
            begin
                out1.r = in1.r | in2.r;
                out1.t = 1;
                out2.r = in2 ^ in1;
                out2.t = 1;
            end
            else
            begin
                out1.r = in1.r | in2.i;
                out1.t = 1;
                out2.r = in2.r ^ in1.i;
                out2.t = 1;
            end
        else
            if (in2.t)
            begin
                out1.r = in1.i | in2.r;
                out1.t = 1;
                out2.r = in2.r ^ in1.i;
                out2.t = 1;
            end
            else
            begin
                out1.i = in1.i | in2.i;
                out1.t = 0;
                out2.i = in2.i ^ in1.i;
                out2.t = 0;
            end
    end

endmodule

