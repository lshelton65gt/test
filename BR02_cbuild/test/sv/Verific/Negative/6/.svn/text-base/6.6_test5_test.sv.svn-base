
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Data declared inside an automatic function or task or a block, is local
// to that function or task or the block and automatic in lifetime of the
// function call or task invocation or block in execution. These design
// hierarchically references these type of automatic variable from outside
// of the block. This is an error, hence this is a negative test case.

module test (input clk,
             input [7:0] in1,
             output reg [7:0] out1);

    int prev, after;

    function automatic int fn(int i);
        int temp;    // automatic, as it is declared inside automatic task

        if (i<$bits(i))
        begin: less
            int tl;    // automatic, as it is declared inside block of an automatic task

            tl = 2*i ^ i;
            temp = tl | i;
        end
        else
        begin: more
            int tm;    // automatic, as it is declared inside block of an automatic task

            tm = (i<<1) & i;
            temp = tm + i;
        end

        return temp;
    endfunction

    always@(posedge clk)
    begin
        prev = fn.less.tl + fn.more.tm;    // error: automatic variable inside block of an automatic function referenced hierarchically

        out1 = fn(in1);

        after = fn.less.tl & fn.more.tm;    // error: automatic variable inside automatic function referenced hierarchically

        out1 = (after != prev)?(++out1):(--out1);
    end

endmodule

