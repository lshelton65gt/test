
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Interfaces can have ports. The direction of the ports can
// be specified using modport keyword. This design defines an interface with with
// two modport declarations inside it. Both the modports are having the same name.
// This is an error.

interface iface #(parameter w = 8)(input clk);
    reg [0:w-1] i1;
    reg [w-1:0] o1;

    modport dir(input i1, output o1); /* both of the modport are having same name */
    modport dir(input o1, output i1);
endinterface

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    iface if1(clk);
    iface if2(clk);

    bot bot_1(if1.dir);
    bot bot_2(if2);

    always@(negedge clk)
    begin
        if1.i1 = in1 * in2;
        if2.i1 = in2 - in1;
    end

    always@(posedge clk)
    begin
        out1 = if1.o1 & if2.o1;
        out1 = if2.o1 ^ if1.o1;
    end

    module bot(iface.dir if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = -(~if1.i1+1);
        end

    endmodule

endmodule

