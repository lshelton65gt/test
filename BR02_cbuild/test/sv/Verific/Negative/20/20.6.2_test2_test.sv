
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface. The interface also contains
// three tasks. Two modports are defined inside that interface. The modports just
// bundle those tasks in them using import. Import requires full task/function
// prototype, but this only specifies the name. This is an error.

interface interface1;

    modport modport1(import task addr4(), // import requires full task prototype
                     import task mult(input logic [3:0] in1, in2,
                               output logic [7:0] out));

    modport modport2(import task addr8(input logic [7:0] in1, in2,
                                       output logic [8:0] out),
                     import task mult(input logic [3:0] in1, in2,
                               output logic [7:0] out));

    task addr4(input logic [3:0] in1, in2,
              output logic [4:0] out);
        out = in1 + in2;
    endtask

    task addr8(input logic [7:0] in1, in2,
              output logic [8:0] out);
        out = in1 + in2;
    endtask

    task mult(input logic [3:0] in1, in2,
              output logic [7:0] out);
        out = in1 * in2;
    endtask

endinterface : interface1

module top ;
    interface1 inst(); 
    logic [3:0] in1, in2;
    logic [7:0] in3, in4;
    logic [4:0] addr_out;
    logic [8:0] addr_out8;
    logic [7:0] mult_out;

    test I1(inst.modport1, in1, in2, addr_out, mult_out);
    test1 I2(inst.modport2, in1, in2, in3, in4, addr_out8, mult_out);

endmodule

module test (interface1.modport1 inst, 
           input logic [3:0] in1, in2,
           output logic [4:0] addr_out,
           logic [7:0] mult_out);

always @(in1, in2)
begin
    inst.addr4(in1, in2, addr_out);     
    inst.mult(in1, in2, mult_out);
end

endmodule

module test1 (interface1.modport2 inst,
             input logic [3:0] in1, in2,
             logic [7:0] in3, in4,
             output logic [8:0] addr_out,
             logic [7:0] mult_out);

always @(in1, in2)
begin
    inst.mult(in1, in2, mult_out);
    inst.addr8(in3, in4, addr_out);
end    

endmodule

