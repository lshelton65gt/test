
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): It is an error to instantiate a module multiple times that
// exports a task. In this case extern forkjoin is used in the interface to define
// the tasks to be exported. Functions can not be exported multiple times using
// this mechanism. This design exports multiple functions using extern forkjoin.
// So, this is an error.

interface multiple_task_export(input bit clk);

extern forkjoin function [7:0] shift(input logic [7:0] data);

modport modport1(export function [7:0] shift());

modport modport2(import function [7:0] shift(input logic [7:0] data));

endinterface

module mod1(multiple_task_export.modport1 a);

function [7:0] a.shift(input logic [7:0] data);
     return  data >> 2;
endfunction

endmodule

module mod2(multiple_task_export.modport2 a,
            input [7:0] data,
            output reg [7:0] out_data);

always @(posedge clk)
begin
    out_data = a.shift(data);
end        

endmodule

module test;

logic clk;
logic [7:0] data, data_out;
multiple_task_export inst(clk);

mod1 I1(inst); 
mod1 I2(inst);
mod2 I3(inst, data, data_out);

initial
    repeat(20) #5 clk = ~clk;

endmodule

