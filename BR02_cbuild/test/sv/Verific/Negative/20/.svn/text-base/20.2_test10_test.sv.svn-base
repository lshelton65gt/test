
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A matching name can be given after the endinterface keyword,
// just line a named begin...end block. This design defines an interface with a
// matching name after the endinterface keyword, but the name actually does not
// match with that of name of the interface.

interface ifc(input clk);
    reg [0:7] i1, i2;
    reg [7:0] o1, o2;

    modport mp(input i1, i2, output o1, o2);

endinterface: ifd /* the end name does not match with the actual name */

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    ifc if1(clk);
    bot2 bot2_1(if1);

    always@(negedge clk)
    begin
        if1.i1 = ~in1;
        if1.i2 = -in2;
    end

    always@(posedge clk)
    begin
        out1 = -if1.o1;
        out2 = ~if1.o2;
    end

    module bot2(ifc if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = ~if1.i1 - if1.i2;
            if1.o2 = -if1.i2 + ~if1.i1;
        end

    endmodule

endmodule

