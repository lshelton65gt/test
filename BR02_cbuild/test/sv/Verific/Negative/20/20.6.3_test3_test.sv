
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface. Modports are used inside
// that interface. It uses export to export a task.  In a module the task is not
// defined referring to the modport that exports the task. This is an error.

interface interface1;

modport modport1(export task addr4(input logic [3:0] in1, in2, output logic [4:0] out));

modport modport2(import task addr4(input logic [3:0] in1, in2,
                                   output logic [4:0] out));

endinterface : interface1

module mod1 (interface1.modport1 inst);

// This module exports task addr4 but task addr4 is not defined here.

endmodule

module mod2 (interface1.modport2 obj,
             input logic [3:0] in1, in2,
             output logic [4:0] addr_out);

always @(in1, in2)
begin
    obj.addr4(in1, in2, addr_out);
end    
    
endmodule

module test;

interface1 b();
logic [3:0] in1, in2;
logic [4:0] out1;

mod1 I1(b);
mod2 I3(b, in1, in2, out1); 

endmodule

