
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing the declaration of typedef inside interface.
// But the type cannot be used outside the interface without redefining it.

interface it;
    typedef int myInt;
endinterface

module test(it a);
    myInt in = 5;
endmodule

module top;

    it a() ;
    test t1(a) ;

  initial
     $display(" %d = 5 ", test.in);

endmodule

