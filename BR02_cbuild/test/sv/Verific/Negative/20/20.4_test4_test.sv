
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): All of the names used in a modport declaration 
// shall be declared in the same interface as the modport itself.

interface i; 
    wire x, y; 
    interface illegal_i; 
        wire a, b, c, d; 
        // x, y not declared by this interface 
        modport master(input a, b, x, output c, d, y) ; // Error
        modport slave(input a, b, x, output c, d, y) ; // Error
    endinterface : illegal_i 
    illegal_i ch1, ch2; 
    modport master2 (ch1.master, ch2.slave); 
endinterface : i

module module1(interface i) ;
    assign i.ch1.c = ~i.ch2.a ;
    assign i.ch1.d = ~i.ch2.b ;
    assign i.ch1.y = ~i.ch2.x ;
endmodule

module test ;
    illegal_i i() ;
    module1 mod1(.i(i.master2)) ;
endmodule

