
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing the declaration of typedef inside interface with class
// declaration. If the type is defined within an interface, it must be redefined locally 
// before being used otherwise it will result in error.

interface music;

    class Album ;
        string singer ;
        string producer ;
        int cost ;
        int total_number_of_songs ;
    
        function new(string s, string p, int c, int t) ;
            singer = s ;
            producer = p ;
            cost = c ;
            total_number_of_songs = t ;
        endfunction

        function void show ;
            $display("%s, %s, %d, %d", singer, producer, cost, total_number_of_songs) ;
        endfunction

    endclass

    typedef Album mysongs ;

endinterface

module test(interface a);

    music.mysongs m1 = new("wqewq","werwe",23,45) ;

    initial 
        m1.show ;

endmodule

module bench ;

    music m2 ();
    test t(m2) ;

endmodule

