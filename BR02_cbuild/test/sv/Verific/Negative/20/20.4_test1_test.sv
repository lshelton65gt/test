
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Interfaces can have ports. The direction of the ports can be specified
// using modport keyword. This design defines an interface with with modport
// declaration to specify the direction of the ports. The modport is then
// used in typedef to define a new data type. Hence this is a negative test case.

interface iface(input clk);
    reg [0:7] i1;
    reg [7:0] o1;

    modport dir(input i1, output o1);
endinterface

typedef iface.dir if_dir;  /* ERROR: typedef the modport with the interface to be a new type */

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    if_dir if1(clk); /* define elements of the new type */
    if_dir if2(clk);

    bot bot_1(if1);
    bot bot_2(if2);

    always@(negedge clk)
    begin
        if1.i1 = ~in1 | in2;
        if2.i1 = -in2 ^ in1;
    end

    always@(posedge clk)
    begin
        out1 = if1.o1 | if2.o1;
        out1 = if2.o1 - if1.o1;
    end
endmodule

    module bot(if_dir if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = ~(-if1.i1) + 1;
        end

    endmodule


