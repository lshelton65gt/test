
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): It is an error to instantiate a module multiple times that
// exports a task. In that case 'extern forkjoin' must be used in the interface to
// define the tasks to be exported. This design exports multiple task but does not
// use 'extern forkjoin'. So, this is an error.

interface neg_export_task;

modport modport1(export task add());

endinterface

module mod1(neg_export_task.modport1 a);

task a.add(input a, b, output c);
   c = a + b;
endtask
endmodule

module mod2(neg_export_task.modport1 a);

task a.add(input [2:0] a, b,
           output [3:0] c);

   c = a + b;
endtask
endmodule

module test;
neg_export_task inst1();

mod1 I1(inst1); // Two definitions of task add for two instances 
mod2 I2(inst1); // of same interface

endmodule

