
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design checks syntax of an interface without providing
// ';' after interface header.

interface simple_bus  // Error: ';' after interface identifier missing
    logic req, gnt;
    logic [7:0] addr, data = 0 ;
    logic [1:0] mode; 
    logic start, rdy;
endinterface: simple_bus

module memMod(simple_bus a,
              input bit clk);
    logic avail;
    always @(posedge clk) a.gnt <= a.req & avail;
endmodule 
module cpuMod(simple_bus b, input bit clk);
    logic avail ;
    
    always @ (posedge clk) b.addr[5] = ~b.data ;
endmodule 

module test;
    logic clk = 0;
    simple_bus sb_intf();
    memMod mem(sb_intf, clk);
    cpuMod cpu(.b(sb_intf), .clk(clk));
endmodule 

