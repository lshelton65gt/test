
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE
              
// TESTING FEATURE(S): This design defines a module with a generic interface
// port type. When instantiating the module it does so using .* port connection
// rule. An implicit port connection can not be used to connect to a generic
// interface, so this is an error.

interface vars_and_nets;
    logic [4:0] addr_out;
    logic [3:0] in1, in2;
    logic [7:0] mult_out;
    bit greater_than_out, less_than_out;
endinterface:vars_and_nets

module test(interface var1, input bit clk);

    assign var1.addr_out = var1.in1 + var1.in2;

    always @(posedge clk)
    begin
        var1.mult_out = var1.in1 * var1.in2;
        var1.greater_than_out = var1.in1 > var1.in2;
        var1.less_than_out = var1.in1 < var1.in2;
    end

endmodule

module top;
    logic clk = 0;
    vars_and_nets var1();

    test inst(.*); // Error: An implicit port cannot be used to reference a generic interface

    initial
        repeat(20)
            #5 clk = ~clk;

endmodule

