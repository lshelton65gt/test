
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Accessing members of an interface not included in modport
// is not allowed. This design attempts that and should result in an error.

interface in #(parameter p1 = 8, parameter p2 = 8)(input logic clk);
    logic [p1-1:0] bus1;
    logic [p2-1:0] bus2;
    logic out;

    modport portConn(input bus1, bus2);
endinterface

module mod(in.portConn a);

    always @(a.clk)
        a.out = (a.bus1 == a.bus2);

endmodule

module test ;

    reg clk;

    in i(clk);

    mod m(i);

    initial repeat(20)
        #5 clk = ~clk;

endmodule

