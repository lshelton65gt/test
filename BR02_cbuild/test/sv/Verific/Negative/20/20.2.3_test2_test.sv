
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design references to an undeclared interface member.
// This is illegal. So, this will be a negative test case.

interface vars_and_nets;
    bit [3:0] in1, in2;
    bit [7:0] mult_out;
    bit greater_than_out, less_than_out;
endinterface:vars_and_nets

module test(interface var1, input bit clk);

    assign var1.addr_out = var1.in1 + var1.in2; // Error: addr_out is not defined in interface vars_and_nets

    always @(posedge clk)
    begin
        var1.mult_out = var1.in1 * var1.in2;
        var1.greater_than_out = var1.in1 > var1.in2;
        var1.less_than_out = var1.in1 < var1.in2;
    end

endmodule

module top;

    logic clk = 0;
    vars_and_nets var1();

    test inst(var1, clk);

    initial
        repeat(20)
            #5 clk = ~clk;

endmodule

