
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): It is an error to instantiate a module multiple times that
// exports a task. In this case extern forkjoin is used in the interface to define
// the tasks to be exported. This design exports multiple task but does not use
// 'extern forkjoin'. The module ports are declared to be of generic interface type.
// So this is an error.

interface neg_export_task;

modport modport1(export task add());

endinterface

module mod1(interface a);

task a.add(input a, b, output c);
   c = a + b;
endtask
endmodule

module test;
neg_export_task inst();

mod1 I1(inst.modport1); // Two module export the same task add
mod1 I2(inst.modport1);

endmodule

