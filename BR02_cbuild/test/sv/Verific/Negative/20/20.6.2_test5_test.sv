
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Functions can be declared inside an interface. This design
// defines an interface. No task is defined inside that interface, but modport
// inside it imports a task and the task is called from a module with hierarchical
// name referencing that interface/modport. This is an error.

interface iface(input clk);
    reg [0:7] i1, i2;
    reg [7:0] o1, o2;

    modport mp1(input i1, i2, output o1, import task set_all());    /* non-existing tasks imported */
    modport mp2(input i1, i2, output o2, import task reset_all());
endinterface: iface

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    iface if1(clk);

    bot1 bot_1(if1.mp1);
    bot2 bot_2(if1.mp2);

    always@(negedge clk)
    begin
        if1.i1 = in1 + in2;
        if1.i2 = in1 & in2;
    end

    always@(posedge clk)
    begin
        out1 = if1.o1 * if1.o2;
        out2 = if1.o2 ^ if1.o1;

        if (out1 == out2)
            if1.reset_all;
        else
            if1.set_all;
    end

    module bot1(interface if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = ~(if1.i2 - if1.i1);
        end

    endmodule

    module bot2(interface if1);

        always@(posedge if1.clk)
        begin
            if1.o2 = if1.i2 - if1.i1;
        end

    endmodule

endmodule

