
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface that does not export any
// task from it. A module is defined and it uses a generic interface type port in
// it. Inside the module hierarchical tasks that belong to the generic interface
// are defined. Another module is defined which instantiates this module with the
// generic port connected to an instantiation of the defined interface. The module
// defines the tasks referring to the interface which lacks any 'export' statement.
// This is an error.

interface iface(input clk);
    reg [0:7] i1, i2;
    reg [7:0] o1, o2;

    modport mp1(input i1, i2, output o1, o2); /* no task is exported from this interface */
endinterface: iface

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    iface if1(clk); /* interface instantiated */

    bot1 bot_1(if1.mp1); /* module instantiated with the modport of the interface instantiation */
    bot2 bot_2(if1.mp1); /* module instantiated with the modport of the interface instantiation */

    always@(negedge clk)
    begin
        if1.i1 = in1 + in2;
        if1.i2 = in1 & in2;
    end

    always@(posedge clk)
    begin
        out1 = if1.o1 * if1.o2;
        out2 = if1.o2 ^ if1.o1;
    end

    module bot1(interface if1);  /* generic interface type port */

        always@(posedge if1.clk)
        begin
            if (if1.i1 == if1.i2)
                if1.set_all; /* task called as to be exported from generic interface if1 */
            else
                if1.reset_all; /* task called as to be exported from generic interface if1 */

            if1.o1 = ~(if1.i2 - if1.i1);
            if1.o2 = if1.i1 - if1.i2;
        end

    endmodule

    module bot2(interface if1);  /* generic interface type port */

        task if1.set_all; /* task defined as to be exported from generic interface if1 */
            if1.i1 = (if1.i2 = (if1.o1 = (if1.o2 = 1)));
        endtask

        task if1.reset_all; /* task defined as to be exported from generic interface if1 */
            if1.i1 = (if1.i2 = (if1.o1 = (if1.o2 = 0)));
        endtask

    endmodule

endmodule

