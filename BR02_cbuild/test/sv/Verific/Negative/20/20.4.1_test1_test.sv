
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Modport of an interface is connected using named port
// connection style. The connected type of modport is different than that of
// the declared/expected.

interface i;

   int a, b, c, d;

   modport modport1(input a, b, output c, d);
   modport modport2(input c, d, output a, b);
endinterface

module mod1(i.modport1 a);

endmodule

module mod2(i.modport2 b);

endmodule

module test ;

    i interface_inst();

    mod1 I(.a(interface_inst.modport2)); // Different modport connected to
                                         // the same module
    mod2 J(.b(interface_inst.modport1));

endmodule

