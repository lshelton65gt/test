
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Module declaration is not allowed in an interface.
// This design defines a module inside an interface. So, this is to be
// considered as a negative test case.

interface i;
    int i, j;

    module test;
          input int i;
          output int j;
    endmodule

endinterface        

