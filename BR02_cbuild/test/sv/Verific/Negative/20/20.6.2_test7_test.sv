
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface that does not import any
// function in it. A module is defined and it uses a generic interface type port
// in it. This module is instantiated with the instantiation of the modport
// declared inside the interface. The module calls functions referring to the
// interface port. This is an error, since it does not import any function in
// the modport.

interface iface(input clk);
    reg [0:7] i1, i2;
    reg [7:0] o1, o2;

    modport mp1(input i1, i2, output o1, o2); /* no function is imported in this modport */

    function if1.set_all; /* function defined but not imported */
        if1.i1 = (if1.i2 = (if1.o1 = (if1.o2 = 1)));
    endfunction

    function if1.reset_all; /* function defined but not imported */
        if1.i1 = (if1.i2 = (if1.o1 = (if1.o2 = 0)));
    endfunction

endinterface: iface

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    iface if1(clk); /* interface instantiated */

    bot1 bot_1(if1.mp1); /* module instantiated with the modport of the interface instantiation */

    always@(negedge clk)
    begin
        if1.i1 = in1 + in2;
        if1.i2 = in1 & in2;
    end

    always@(posedge clk)
    begin
        out1 = if1.o1 * if1.o2;
        out2 = if1.o2 ^ if1.o1;
    end

    module bot1(interface if1);  /* generic interface type port */

        always@(posedge if1.clk)
        begin
            if (if1.i1 == if1.i2)
                if1.set_all; /* function called as to be imported in the generic interface if1 */
            else
                if1.reset_all; /* function called as to be imported in the generic interface if1 */

            if1.o1 = ~(if1.i2 - if1.i1);
            if1.o2 = if1.i1 - if1.i2;
        end

    endmodule

endmodule

