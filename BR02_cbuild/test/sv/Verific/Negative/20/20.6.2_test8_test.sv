
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface. Two modports are defined
// inside that interface. The interface also contains three tasks. The modports
// just bundle those tasks in them. These modports are used as a port in module
// declaration and tasks are invoked using that modport. In modport declaration,
// task are imported but 'import' keyword is omitted. This is an error.

interface interface1;

    modport modport1(task addr4(input logic [3:0] in1, in2,
                                       output logic [4:0] out),
                     task mult(input logic [3:0] in1, in2,
                               output logic [7:0] out));

    modport modport2(task addr8(input logic [7:0] in1, in2,
                                       output logic [8:0] out),
                     task mult(input logic [3:0] in1, in2,
                               output logic [7:0] out));

    task addr4(input logic [3:0] in1, in2,
              output logic [4:0] out);
        out = in1 + in2;
    endtask

    task addr8(input logic [7:0] in1, in2,
              output logic [8:0] out);
        out = in1 + in2;
    endtask

    task mult(input logic [3:0] in1, in2,
              output logic [7:0] out);
        out = in1 * in2;
    endtask

endinterface : interface1

module test (interface1.modport1 inst, 
           input logic [3:0] in1, in2,
           output logic [4:0] addr_out,
           output logic [7:0] mult_out);

    always @(in1, in2)
    begin
        inst.addr4(in1, in2, addr_out);     
        inst.mult(in1, in2, mult_out);
    end

endmodule

module test1 (interface1.modport2 inst,
             input logic [3:0] in1, in2,
             logic [7:0] in3, in4,
             output logic [8:0] addr_out,
             logic [7:0] mult_out);

    always @(in1, in2)
    begin
        inst.mult(in1, in2, mult_out);
        inst.addr8(in3, in4, addr_out);
    end

endmodule

module bench;

    interface1 inst();
    logic [3:0] in1, in2;
    logic [7:0] in3, in4;
    logic [4:0] addr_out;
    logic [8:0] addr_out1;
    logic [7:0] mult_out, mult_out1;

    test I(inst, in1, in2, addr_out, mult_out);
    test1 I2(inst, in1, in2, in3, in4, addr_out1, mult_out1);

    initial
    begin
        in1 = 1;
        in2 = 1;
        in3 = 1;
        in4 = 8;
        $monitor("%d, %d, %d, %d, %d, %d, %d, %d", in1, in2, in3, in4, addr_out, addr_out1, mult_out, mult_out1) ;
        repeat(120)
        begin
            #5;
            {in1, in2} = { in1, in2} + 1;
            in3 = in3 + 3;
            in4 = in4 + 5;
        end
    end

endmodule    

