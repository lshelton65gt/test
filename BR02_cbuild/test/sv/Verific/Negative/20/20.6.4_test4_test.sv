
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Functions cannot be exported multiple times even using extern
// forkjoin mechanism because they must always write to the result./ This design exports
// multiple functions using extern forkjoin. The functions are of void type. So they
// don't need to write to the result.

interface multiple_function_export(input bit clk);

    extern forkjoin function void shift(input logic [7:0] data);

    modport modport1(export function void shift());
    modport modport2(import function void shift(input logic [7:0] data));

endinterface

module test(input clk, input logic [7:0] in1, output logic [7:0] out1);

    mod1 I1(inst); 
    mod1 I2(inst);
    mod2 I3(inst, in1, out1);

    module mod1(multiple_function_export.modport1 a);

        function void a.shift(input logic [7:0] data);
            $display("Shifted to %b", data >> 2);
        endfunction

    endmodule

    module mod2(multiple_function_export.modport2 a, input [7:0] data, output reg [7:0] out_data);

        always @(posedge clk)
        begin
            a.shift(data);
        end        

    endmodule

endmodule

