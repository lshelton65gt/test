
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses a typedef to forward define a user defined data
// type. This type is used to define ports of a module. In the module these ports are
// used like interface or struct by referencing its member elements using hierarchical
// notation. When the module is instantiated the ports are connected with instance of an
// interface. The interface is not defined before that and the forward defined type is
// never actually defined!

typedef ifc ;

module test(input clk, ifc in, ifc out);

    reg [0:3] temp;

    always@(posedge clk)
    begin
        if (in.enable && in.read)
            temp = in.data;

        if (out.enable && out.write)
            out.data = temp;
    end

endmodule

module bench;

    reg clk;

    iface in();
    iface out();

    test t_mod(clk, in, out);

    initial
    begin
        clk = 0;
        $monitor($time,, "read = %d, read enable = %d, read data = %d, write = %d, write enable = %d, writtem data = %d",
				in.read, in.enable, in.data, out.write, out.enable, out.data);
        #1001 $finish ;
    end

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in.read, in.enable, in.data, out.enable, out.write } = $random;

endmodule

interface iface();
    bit read;
    bit write;
    bit enable;
    logic [0:3] data;

    modport mp_in(input read, enable, output data);
    modport mp_out(input write, enable, output data);
endinterface

