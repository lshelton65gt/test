
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): When interfaces are used as a port type, direction cannot
// be specified. The default direction is inout. This design explicitly specifies
// the direction of a  port of type interface as input. This is an error.

interface vars_and_nets;
    wire [4:0] addr_out;
    wire [3:0] in1, in2;
    reg [7:0] mult_out;
    bit greater_than_out, less_than_out;
endinterface

module test(vars_and_nets var1, input bit clk);

    input var1;  // direction of interface obj can only be set by  modport
    assign var1.addr_out = var1.in1 + var1.in2;

    always @(posedge clk)
    begin
        var1.mult_out = var1.in1 * var1.in2;
        var1.greater_than_out = var1.in1 > var1.in2;
        var1.less_than_out = var1.in1 < var1.in2;
    end

endmodule

