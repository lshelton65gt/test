
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface. Modports are used inside
// that interface. It uses export/import to export/import a task. In a module a task
// is called referring to the modport that exports that task and the task is defined
// referring to the modport that imports the task. This is just the opposite of what
// should be. So this is an error.

interface i;

modport modport1(export task addr(input logic [3:0] in1, in2,output logic [3:0] out));
modport modport2(import task addr(input logic [3:0] in1, in2,
                                  output logic [3:0] out));
endinterface

module mod1(i.modport1 a, input [3:0] in1, in2,
            output reg [3:0] out);

always @(in1, in2)
    a.addr(in1, in2, out);    // Can not call task addr
endmodule

module mod2(i.modport2 a);

task a.addr(input logic [3:0] in1, in2,  // Can not define task addr
          output logic [3:0] out);

     out = in1 + in2;
endtask

endmodule

module test;

reg [3:0] in1, in2;
wire [3:0] out1;

i a();

mod1 I(a, in1, in2, out1);
mod2 I2(a);

endmodule

