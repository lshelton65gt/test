
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing whether interface items can be accessed hierarchically 
// using the interface name if modport is used for instantiation.

interface in #(parameter p1 = 8, parameter p2 = 8)(input logic clk);
    logic [p1-1:0] bus1;
    logic [p2-1:0] bus2;
    logic out;

    modport portConn(input bus1, bus2, clk, output out);
endinterface

module test(in.portConn a);

    always @(a.clk)
        in.out = (a.bus1 == a.bus2);  // in.out cannot be used, a.out should be used instead

endmodule

module top;

    reg clk;
    
    in i1(clk);

    test m1(i1);

endmodule

