
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Interfaces may be used as the data type of ports in a module.
// This design defines an interface that is used as the port type. The interface is
// instantiated without any parentheses. This is an error.

interface vars_and_nets;
    bit [4:0] addr_out;
    bit [3:0] in1, in2;
    bit [7:0] mult_out;
    bit greater_than_out, less_than_out;
endinterface

module test(vars_and_nets var1, input bit clk);

assign var1.addr_out = var1.in1 + var1.in2;

always @(posedge clk)
begin
    var1.mult_out = var1.in1 * var1.in2;
    var1.greater_than_out = var1.in1 > var1.in2;
    var1.less_than_out = var1.in1 < var1.in2;
end
endmodule

module bench;
    bit clk = 0;

    vars_and_nets interface_inst;

    test mod_inst(interface_inst, clk);

    initial
    begin
        interface_inst.in1 = 0;
        interface_inst.in2 = 0;
        repeat(20)
        begin
            #5;
            interface_inst.in1 = interface_inst.in1 + 3;
            interface_inst.in2 = interface_inst.in2 + 4;
        end
    end

    initial
        repeat(25)
        #5 clk = ~clk;

    initial
        $monitor($time,,, "interface_inst.in1 = %b, interface_inst.in2 = %b, interface_inst.addr_out = %b, interface_inst.mult_out = %b, interface_inst.greater_than_out = %b, interface_inst.less_than_out = %b", interface_inst.in1, interface_inst.in2, interface_inst.addr_out, interface_inst.mult_out, interface_inst.greater_than_out, interface_inst.less_than_out);

endmodule

