
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Interfaces can be declared and instantiated in modules 
// (either flat or hierarchical) but modules can neither be declared nor 
// instantiated in interfaces. This design instantiates a module within an
// interface which is an error.

interface it;
    mod instanMod();
endinterface

//module test;
module mod;

  initial
      $display("This should not be displayed");

endmodule

