
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing input type ports in interface in modports. An input
// type port of the interface is used a output type port in a modport declaration
// of the same interface. This is an error.

interface combination(input reg clk) ; // clk is input

    reg r ;
    int num ;

    modport master(input clk, output r, output num) ;
    modport slave(input num, output r, output clk) ; // Error: clk used as output

endinterface

module test(interface a);
    always @(a.clk) begin
       a.r = 1001 ;
       a.num = a.r || a.clk ;     
    end
endmodule

module hier ;

   reg clock ;
   combination com(clock) ;
   
endmodule

module bench ;

   hier check() ;

   test t1(check.com.master) ;
   test_slave ts (check.com.slave) ;

endmodule

module test_slave(interface b) ;
   initial begin
      b.clk = 0 ;
      #100 $finish ;
   end

   always  
      #5 b.clk = ~b.clk ;

   always @(b.num)
      b.r = b.num ;

endmodule

