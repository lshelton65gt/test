
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Interfaces can be declared and instantiated in modules but
// modules can not be declared or instantiated in interfaces. Test cases should
// declare and instantiate modules inside an interface declaration. This design
// defines and instantiates a module inside an interfacer(which is in the $root
// scope).

interface ifc(input clk);
    reg [0:7] i1, i2;
    reg [7:0] o1, o2;

    modport mp(input i1, i2, output o1, o2);

    module bot1(input clk, input [0:7] i1, output reg [7:0] o1);

        always@(posedge clk)
        begin
            o1 = ~i1;
        end

    endmodule

    bot1 bot1_1(clk, i1, o2);
    bot1 bot1_2(clk, i2, o1);

endinterface: ifc

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    ifc if1(clk);
    bot2 bot2_1(if1);

    always@(negedge clk)
    begin
        if1.i1 = ~in1;
        if1.i2 = -in2;
    end

    always@(posedge clk)
    begin
        out1 = -if1.o1;
        out2 = ~if1.o2;
    end

    module bot2(ifc if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = ~if1.i1 - if1.i2;
            if1.o2 = -if1.i2 + ~if1.i1;
        end

    endmodule

endmodule

