
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Interfaces can be declared and instantiated in modules but
// modules can not be declared or instantiated in interfaces. This design defines
// and instantiates a module inside an interface(which is inside another module).

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    interface ifc(input clk);
        reg [0:w-1] i1, i2;
        reg [w-1:0] o1, o2;

        modport mp(input i1, i2, output o1, o2);

        bot1 bot1_1(clk, i2, o1);
        bot1 bot1_2(clk, i1, o2);

        module bot1(input clk, input [w-1:0] i1. output reg [0:w-1] o1);

            always@(posedge if1.clk)
            begin
                o1 = -i1;
            end

        endmodule

    endinterface: ifc

    ifc if1(clk);
    bot2 bot2_1(if1);

    always@(negedge clk)
    begin
        if1.i1 = ~in1;
        if1.i2 = -in2;
    end

    always@(posedge clk)
    begin
        out1 = -if1.o1;
        out2 = ~if1.o2;
    end

    module bot2(ifc if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = ~if1.i1 - if1.i2;
            if1.o2 = -if1.i2 + ~if1.i1;
        end

    endmodule

endmodule

