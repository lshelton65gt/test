
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): An implicit port cannot be used to connect to a generic
// interface, a named port must be used instead. This design defines an interface
// and a module with a generic interface. The module is instantiated using implicit
// .name port connection style. This is not allowed in SystemVerilog, this is an error.

interface ifc(input clk);
    reg [0:7] i1, i2;
    reg [7:0] o1, o2;
endinterface: ifc

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    ifc if1(clk);

    bot bot_1(.if1); // Error: implicit .name port connection

    always@(negedge clk)
    begin
        if1.i1 = ~in1;
        if1.i2 = -in2;
    end

    always@(posedge clk)
    begin
        out1 = -if1.o1;
        out1 = ~if1.o2;
    end

    module bot(interface if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = ~if1.i1;
            if1.o2 = -if1.i2;
        end

    endmodule

endmodule

