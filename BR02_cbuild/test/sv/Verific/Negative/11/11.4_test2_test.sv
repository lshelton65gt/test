
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing whether only one event control is allowed in 
// 'always_ff'.

module test(input data, clk, reset, output reg out);

always_ff @ (negedge reset)
begin
   if(reset) 
       out = 0;
   else
       out = @(posedge clk) data;
end
endmodule

