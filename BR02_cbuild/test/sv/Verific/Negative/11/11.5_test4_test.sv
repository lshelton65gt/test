
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses continuous assignments on variables.
// It is an error to assign values to variables of type reg,
// integer, real, time, realtime etc.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

real r;
time t;
realtime rt;

assign r = ^in1,
       t = &in2,
       rt = t + r;

assign r = in2[width-1],
       t = in1[width-1],
       rt = in1[0] | in2 [0];

assign out1 = r * t,
       out2 = rt - r;

endmodule

