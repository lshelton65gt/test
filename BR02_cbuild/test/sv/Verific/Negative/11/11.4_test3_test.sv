
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The 'always_ff' block imposes the restriction that it 
// contains one and only one event control and no blocking timing controls.
// This design specifies an 'always_ff' block without any event control. This
// is an error.


module test; 
bit [3:0] bit1, bit2; 
bit [3:0] resultBit;
initial
begin
    bit1 = 4'b 0000;
    bit2 = 4'b 0001;
    repeat(10) 
    begin
        bit1 += 1;
        bit2 += 2;
    end
end

always_ff
begin
    resultBit = bit1 & bit2;
    $display("Result = %b", resultBit);
end
endmodule

