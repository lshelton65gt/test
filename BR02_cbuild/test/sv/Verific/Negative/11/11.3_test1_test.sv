
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog 'always_latch' impose a restriction that the 
// variables written in the 'always_latch' block cannot be written by another process.
// This design does just the opposite to check whether the restriction is properly 
// implemented or not.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always_latch
begin
    r1 = ~in1 & -in2;
    r2 = ~in2 | -in1;

    out1 = r1 + r2;
    out2 = r2 ^ r1;
end

always@*
begin
    r1 = in2 * in1;
    r2 = in1 - in2;
end

endmodule

