
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses multiple continuous assignments on variables
// other than nets. It is an error to assign multiple values to 
// variables of type logic, int, real, time, realtime etc.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

logic l;
int i;
byte c;

assign l = |in1,
       c = |in2,
       i = l + c;

assign l = in2[0],
       c = in1[0],
       i = in1[width-1] | in2 [width-1];

assign out1 = { width { l + c } },
       out2 = { width { i + l } };

endmodule

