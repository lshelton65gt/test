
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): System Verilog does not allow both continuous and procedural
// assignment on a variable. This design tests this.

module test(q, en, clk, d);
    output q ;
    int q = 10 ;
    input en , clk, d ;
    assign q = en ? d : 'z ;

    always_ff @ (posedge clk)
        if (en)
          q = d ;
endmodule

