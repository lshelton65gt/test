
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): System Verilog does not allow both continuous and procedural
// assignment on variable. This design assigns a variable through initialization 
// and blocking assignment. 

primitive udp(out, in);
output out;
input in;

table
    0 : 0;
    1 : 1;
    x : 0;
    z : 0;
endtable
endprimitive

module test;
logic in;
bit out;

udp I(out, in);
initial
    out = 1'b1;
endmodule

