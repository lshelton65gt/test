
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog 'always_comb' imposes a restriction that the 
// variables written in the 'always_comb' block can not be written by another
// process. This design does just the opposite to check whether the restriction 
// is properly implemented.

module test #(parameter p=8)
             (input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1, r2;

always_comb
begin
    r1 = in1 ^ in2;
    r2 = in2 - in1;

    out1 = r1 | r2;
    out2 = r2 + r1;
end

always@*
begin
    r1 = in2 - in1;  // r1 is being driven both by 'always_comb' and 'always @*' processes
    r2 = in1 ^ in2;  // r2 is being driven both by 'always_comb' and 'always @*' processes
end

endmodule

