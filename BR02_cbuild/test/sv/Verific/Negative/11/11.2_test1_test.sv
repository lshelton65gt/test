
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The elements that are written to inside an 'always_comb' block
// can not be written to, from anywhere else. This design writes to a
// variable from two always_comb blocks. This is an error.

module test;

reg out1;

wire a, b;

	always_comb
		out1 = a;

	always_comb
		out1 = b;

endmodule

