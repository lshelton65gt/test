
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses multiple continuous assignments on nets. This
// is supported by SystemVerilog. The assignment is through assign
// statements, primitive and module instantiations. What makes it a
// negative test case is that it is using ++/-- on net type variables.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

wire w1, w2;
wand a1, a2;
wor o1, o2;

mod m1(w1, o2, a1);

assign w1 = |in1 & ~&in2,
       w2 = ~^in2 - |in1,
       a1 = &in1 + ~|in2,
       a2 = ~|in1 ^ ^in2,
       o1 = |in1 | ~&in2,
       o2 = ~&in1 * &in2;

or og(o2, a1, ++a2);
and ag(o1, --w2, w1);

assign out1 = o1 ^ o2;
assign out2 = o2 & o1;

endmodule

module mod(output o1, inout i1, i2);

assign o1 = (i1 ^ i2) + (i1 & i2);

endmodule

