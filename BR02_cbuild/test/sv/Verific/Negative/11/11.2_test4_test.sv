
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines two always_comb blocks. Both blocks
// manipulate variables that are used to set the value of the output port. This
// is not allowed in SystemVerilog.
 
module test(i1, o1);

parameter WIDTH = 8;

input [WIDTH-1:0] i1;
output [WIDTH-1:0] o1;

reg x;
logic [WIDTH-1:0] temp, temp1;
int n, val;

always_comb
begin
        temp = i1;
        val = 0;
        n = 1;

        for(int i=1; i<=WIDTH; i++)
        begin
                temp1 = temp >> i;
                temp1 <<= i;
                x = (temp - temp1) >> (i-1);
                val += x*n;
                n *= 2;
        end
end
always_comb
begin
    for(int i=0;i<WIDTH;i++)
    begin
        val[i]++;
        val[i] |= val[i+1];
    end
end

assign o1 = val;

endmodule

