
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case on assigning to the same variable, which
// is already assigned by 'always_comb'.

module test;
logic [1:0] log1, log2, result;
initial
begin
    log1 = 2'b11;
    log2 = 2'b00;
    result = log1 | log2;
end

always@*
    result = log1 & log2; 

endmodule


module test1;
logic [1:0] log1, log2, result;
initial
begin
    log1 = 2'b11;
    log2 = 2'b00;
    result = log1 | log2;
end

always_comb
    result = log1 & log2; 

endmodule

