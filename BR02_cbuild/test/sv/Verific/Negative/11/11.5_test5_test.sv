
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses multiple continuous assignments on variables
// other than nets. 

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

bit b;
byte t;
longint l;
shortint s;

assign b = ~|in1,
       t = ~^in2,
       l = &in1 * ~&in2,
       s = b | l & t;

assign b = ^in1,
       t = &in2,
       l = |in1 + ~^in2,
       s = b - l | t;


assign out1 = { width { ~l ^ s } },
       out2 = { width { s - ~l } };

endmodule

