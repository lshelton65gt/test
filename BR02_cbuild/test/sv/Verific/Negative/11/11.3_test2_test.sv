
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The variables written on the left-hand side of assignments
// in an 'always_latch' may not be written to by any other process.

module test(input enable, data, output reg out);
initial
    out = 0;

always_latch
begin
    if(enable)
	    out = data;
end
endmodule

