
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses 'always_ff block'. In 'always_ff' block
// only one event control is allowed. This design uses multiple event
// controls in 'always_ff' block to check whether it is flagged as an error or not.

module test #(parameter width=8)
             (input clk, reset,
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;
reg [0:width-1] r3, r4;

always_ff@(posedge clk)
begin
    if (!reset)
        out1 = (out2 = 0);
    else
    begin
        out1 = @(negedge reset)in1 ^ in2;
        out2 = in2 & in1;
    end
end

endmodule

