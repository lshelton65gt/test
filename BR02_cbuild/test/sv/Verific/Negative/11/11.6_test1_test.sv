
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A return statement within the context of a fork...join 
// statement is illegal and shall result in a compilation error 

module test ;

task wait_20_30 ;
   fork 
      begin 
         $display( "First Block\n" ) ;
         # 20ns ;
         return ;
      end 
      begin 
         $display( "Second Block\n" ) ;
         # 30ns ;
      end 
   join_none

   wait fork ;
endtask

   initial 
      wait_20_30 ;

endmodule

