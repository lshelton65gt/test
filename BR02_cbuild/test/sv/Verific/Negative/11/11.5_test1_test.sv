
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): System Verilog allows continuous assignment on non-net 
// objects. However, it shall be an error for a variable driven by a continuous
// assignment to have an initializer in the declaration or any procedural assignment.

module test #(parameter width=8) (input [width-1:0] in1, 
                                  input [0:width-1] in2, 
                                  output[width-1:0] out1, out2) ;

    integer i=0 ;
    integer iv ;
    real r ;
    time t=0;
    realtime rt ;

    initial
    begin
        r = 0 ;
        rt = 0 ;
        iv = 0 ;
    end
    assign
           i = in1[0] | in2[width-1],
           r = in1[width-1] ^ in2[0],
           t = in1[0] * in2[0],
           rt = in1[width/2-1] & in2[width/2-1],
           iv = in1<<width/2 - in2 ;

    assign out1 = i | iv + t ;
    assign out2 = r + t - rt ;

endmodule

