
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses continuous assignments and procedural
// assignment on the same variables. It is an error to use continuous assign and
// procedural assignments on the same variables.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

logic r;
bit t;

assign r = ^in2 + ~&in1;
assign t = ~|in1 & ~^in2;

always@(in1, in2)
begin
    r = in1[0] | in2[width-1];
    t = in1[width-1] ^ in2[0];
end

assign out1 = { width { r - t } },
       out2 = { width { t * r } };

endmodule

