
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): If an identifier has been imported from a pcakage using
// a wildcard import and the same identifier has also been locally delcared or
// explicitly imported then the tool should issue an error. The following design
// checks this.

package p ;
    typedef enum { FALSE, TRUE } BOOL ;
    const BOOL c = FALSE ;
endpackage

package q ;
    typedef enum { ORIGINAL, FALSE } teeth_t ;
    const int c = 0 ;
endpackage

module test ;

    import q::* ;
    wire a = c ;
    import p::c ; // The conflict with q::c and p::c creates an error.

endmodule: test

