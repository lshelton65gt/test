
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses named and .* port connection together.
// This is legal, but for the second instantiation it uses only .* port connection
// for which one of the corresponding port is not available. This is an error.

typedef bit [0:7] MYTYPE1;
typedef enum {a,b,c,d,e,f,g,h} MYTYPE2;

module test ;

MYTYPE1 out1;
MYTYPE2 in2, out2;

bot i1 (.in1(), .*);
bot i2 (.*);

endmodule

module bot(input MYTYPE1 in1, MYTYPE2 in2, output MYTYPE1 out1, MYTYPE2 out2);

    always @ (*)
    begin
        out1 = in1;
        out2 = in2;
    end

endmodule

