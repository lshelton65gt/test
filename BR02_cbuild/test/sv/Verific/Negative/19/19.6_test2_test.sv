
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a module inside generate. It is an error, 
// nested Module cannot be defined inside a generate case

typedef struct packed { bit [0:3] f1, f2; } TYPE1;
typedef bit [0:3][0:3] TYPE2;
typedef bit [0:7] TYPE3;
typedef TYPE1 TYPE4;

typedef enum {t1, t2, t3, t4} typeNo;

module test(output out, input [0:15] in);
parameter typeNo p = t1;

	generate
		case (p)
			t1:
				begin : b1
					module bot #(parameter type T = TYPE1)
						(input T in1, input T in2, output out);
						assign out = in1 > in2;
					endmodule

					TYPE1 in1, in2;
					assign {in1, in2} = in;
					bot #(TYPE1)  i1 (.*);
				end : b1
			default:
				begin : b4
					module bot #(parameter type T = TYPE1)
						(input T in1, input T in2, output out);
						assign out = in1 > in2;
					endmodule

					TYPE4 in1, in2;
					assign {in1, in2} = in;
					bot #(TYPE4)  i1 (.*);
				end : b4
		endcase
	endgenerate
endmodule


