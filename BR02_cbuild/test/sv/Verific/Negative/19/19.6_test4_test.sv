
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Same identifier names can be used for more than one declaration,
// provided no two of them are in the same scope. This design defines more than one
// identifier with same name in the same scope, so this is an error and should be
// caught by the tool.

reg [31:0] global_var;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output [width-1:0] out1,
              output reg [width-1:0] out2);

reg [width-1:0] local_module_var;
initial global_var = complmnt(200);

always@(posedge clk)
begin
    int local_block_var;

    local_block_var = in1 | in2;
    out2 = local_block_var ^ in1 + in2;
end

assign local_module_var = out1 * in2;
assign out1 = local_module_var - in1;

int local_module_var; // Error: identifier already declared

endmodule

function int complmnt(int a);
    return ~a;
endfunction

logic [31:0] global_var;

