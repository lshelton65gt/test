
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case where timeprecision is larger that timeunit.

timeunit 10ns;
timeprecision 100000ps;

module test;
    int a;

    initial
    begin
        $monitor($time,"a = %d", a);
        a = 10;
        repeat(20)
            #5.34112ns a++;
  end

endmodule

