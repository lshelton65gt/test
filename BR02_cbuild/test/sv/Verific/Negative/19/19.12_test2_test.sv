
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Should create multiple driver on temp2 of type logic which is an error.

typedef logic UNPACKED_ARRAY[0:3];

module test (in1, out, clk);
    input in1 [0:3] ;
    input clk;
    output [0:3] out;
    
    UNPACKED_ARRAY temp1, temp2;

    assign temp1 = in1;
    assign out = {temp2[0], temp2[1], temp2[2], temp2[3]};

    bot i1 [0:1] (clk, temp1, temp2);

    always@(posedge clk)
        temp2 = in1;

endmodule

module bot (input clk, input UNPACKED_ARRAY in1, output UNPACKED_ARRAY out1);

    always @ (posedge clk)
        out1 = in1;

endmodule

