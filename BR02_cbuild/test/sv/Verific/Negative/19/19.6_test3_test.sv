
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a task and a function with same name but
// in different scopes. The function/task is enabled from within the inner scope.
// So, it will invoke the function in that scope. But the intention was to invoke
// the task.

module test(clk, in1, in2, out1, out2);

    input clk, in1, in2;
    output out1, out2;

    task f;
	    input in1, in2;
	    output out;

	    out = in1 | in1;
	    return;
    endtask

	m1 i1 (clk, in1, in2, out1, out2);

	module m1 (input clk, in1, in2, output reg out1, out2);

		always @ (posedge clk)
		begin
			f (out1, in1, in2); // intended function call
			f (in1, in2, out2); // intended task call
			// But both will bind to local function, hence will
			// result in error due to port direction mismatch
		end

		function void f;
			output out1;
			input in1, in2;
			reg t;

			out1 = in1 & in1;
			return;
		endfunction
	endmodule

endmodule

