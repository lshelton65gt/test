
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Default port direction is inout. This design defines two ports
// without any direction - making them inout. The data type of these ports is set to
// be of type 'wire'. One of these ports is used in the left hand side in the sequential
// area. This is not valid.

module test (output reg out1, wire in1, in2);

    always @ (*)
    begin
        out1 = in1 & in2;
        in1 = in2; // generate warning
    end

endmodule

