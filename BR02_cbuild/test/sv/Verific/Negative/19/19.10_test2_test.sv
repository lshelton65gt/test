
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Timeunit and timeprecision must be a power of 10 units.
// This design uses a timeprecision that is not power of 10, it is even not
// multiple of 10.

timeunit 100ns;
timeprecision 0.11ns;

module test;
    int a;

    initial
    begin
        $monitor($time,"a = %d", a);
        a = 10;
        repeat(20)
            #5.34112ns a++;
  end

endmodule

