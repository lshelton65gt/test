
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines an unpacked array of class inside a 
// block. That array is accessed hierarchically and is used as actual to instance
// array selecting some elements by +: range. Dynamic variables shall not be written
// by continuous assignment, hence this is illegal.

class Student ;
    string name ;
    string addr ;
    int roll ;

    function new(string n, string a, int r) ;
        name = n ; 
        addr = a ;
        roll = r ;
    endfunction

    function void show() ;
        $display("%s, %s, %d", name, addr, roll) ;
    endfunction
endclass

module top ;
    parameter p = 2 ;
    test #(p) t1() ;
endmodule

module test #(parameter size = 2) ;

    initial begin:che
       Student s[0:size] ;
       s[2] = new("asd","fgh",09) ;
       s[1] = new("qwe","asd",12) ;
       s[0] = new("rty","fgh",34) ;
    end

    bot b1[1:0] (che.s[0+:size]) ;

endmodule

module bot(input Student s1[0:1]) ;
    initial begin
        s1[0].show() ;
        s1[1].show() ;
     end
endmodule

