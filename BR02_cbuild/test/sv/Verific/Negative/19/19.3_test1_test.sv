
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design calls a task from compilation scope to check whether
// it is supported by the tool or not.

integer i, j, k, l;

task1(i, j);
mod1 #(1) m (k, l);

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    i = in1 | in2;
    k = in2 - in1;

    out1 = j ^ l;
    out2 = l + j;
end

endmodule

module mod1(input integer a, output integer b);
    parameter p = 4;

    always@(a)
        b = a<<p;
endmodule

task task1(input integer a, output integer b);
    b = a<<1;
endtask

