
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design connects tri0 to tri1 net implicitly. This is
// a syntax error. It is legal to connect both of them by
// using named port connection but not using .* port connection.

module test (in1, in2, out1, clk);

output tri1 out1;
input clk;
input [0:1] in1, in2;

reg [0:3] in;
tri1 [0:3] out;
	always@ (posedge clk)
	begin
		in = {in1, in2};
	end

	bot i1 (.*, .clk());

	assign out1 = out;
endmodule


module bot (in, out, clk);
input [0:3] in;
input clk;
output tri0 [0:3] out;

	assign out = in;
endmodule

