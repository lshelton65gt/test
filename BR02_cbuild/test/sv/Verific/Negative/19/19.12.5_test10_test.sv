
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation where the actual does not have any
// dimension and the formal has a unpacked dimension.

module test ;
    byte C ;
    integer I ;

    initial #1 C = 8'b11110000 ;

    bot b1[1:0] (C, I) ;

endmodule

module bot (input C [3:0], input [15:0] I) ;
    initial
        $monitor($time,, "%m %b %b %b %b, %b\n", C[3], C[2], C[1], C[0], I) ;
endmodule

