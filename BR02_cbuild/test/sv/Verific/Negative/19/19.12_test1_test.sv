
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): An output port can be connected to a variable (or concatenation)
// of a compatible data type, and has shared variable behaviour if multiple outputs are
// connected the last value remains. This design connects same variable to multiple
// outputs to check this behaviour.

// NOTE: This was for SystemVerilog 3.0. SV 3.1 does not allow multiple assignments,
// please see section 5.6 page 41 of SV 3.1 final LRM. So it now becomes a negative
// test case!

module test #(parameter width=32)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    int o;

    bot bot_1 (clk, in1, in2, o, o);

    always@(posedge clk)
    begin
        out1 = -o;
        out2 = ~o;

        if (o == ((in1==in2)?(2*in1):(in1+in2)))
            $display("last write wins - SUCCESS");
        else
            $display("first write wins - FAIL");
    end

    module bot (input clk,
                input int in1, in2,
                output int out1, out2);

        always@(posedge clk)
        begin
            out2 = ((in1>in2)?(in1-in2):(in2-in1));
            out1 = ((in1==in2)?(2*in1):(in1+in2));   /* out1 writes last */
        end

    endmodule

endmodule

