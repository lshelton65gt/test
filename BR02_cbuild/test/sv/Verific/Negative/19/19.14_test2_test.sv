
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Hierarchical names consist of instance names separated by periods,
// where an instance name may be an array element. This design
// references a non-existing hierarchical name from a generate block.
// This is an error and should be detected by the tool.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    genvar i;

    generate

        for(i=0; i<width; i++)
        begin: for_gen_i
            always@(posedge clk)
            begin
                out1[i] = ^test.all_beg.i; /* all_beg does not exist */
            end
        end

    endgenerate

    always@(posedge clk)
    begin: al_beg
        reg [width-1:0] i;

        i = in1 + in2;

        out2 = i ^ {width{1'b1}};
    end

endmodule

