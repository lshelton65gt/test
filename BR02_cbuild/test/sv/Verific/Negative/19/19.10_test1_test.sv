
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Timeprecision must be a power of 10. This design uses a
// timeprecision which is not a power of 10 units. This should result in an error.

timeunit 100ns;
timeprecision 0.12ns;

module test;
  int a;

  initial
  begin
      a = 0;
      repeat(10)
        #5.21414ns a++;
      $monitor($time,,,"a = %d", a);
  end

endmodule


