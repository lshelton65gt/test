
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog supports type of ports to be user defined data
// type. This design defines a new type and uses that type as the data type of the
// ports of module. The full definition of the type is specified after the use of
// the type in ports. But forward declaration is not applied to enumeration values.

typedef portType;

module test(input portType in1, in2, output portType out1);
    
    always @ (in1, in2)
    begin
        if ((in1._type == in2._type) && (in1._type == FOUR_VAL))
        begin
            out1._bus = in1._bus === in2._bus;
            out1._type = FOUR_VAL;
        end
        else
        begin
            out1._bus = in1._bus == in2._bus;
            out1._type = TWO_VAL;
        end
    end

endmodule

typedef enum {FOUR_VAL, TWO_VAL} valType;

typedef struct {
    valType _type;
    union {
        bit [7:0] v4_bus;
        bit [7:0] v2_bus;
    } _bus;
} portType;

