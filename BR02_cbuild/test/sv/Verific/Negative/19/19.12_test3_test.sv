
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Port connection width mismatch while instantiating module in
// array style. also logic type port is connected in output port of two instantiation.

typedef logic [0:1] PACKED_ARRAY;

module test (in1, out, clk);
    input [0:7] in1;
    input clk;
    output [0:7] out;
    
    PACKED_ARRAY [0:3] temp1, temp2, temp3;

    assign temp1 = in1;
    assign out = temp2 | temp3;

    bot i1 [0:1] (clk, temp1, temp2); // port connection width is less, error

    bot i2 [0:7] (clk, temp1, temp3); // port connection width is more, error

endmodule

module bot (input clk, input PACKED_ARRAY in1, output PACKED_ARRAY out1);

    always @ (posedge clk)
        out1 = in1;

endmodule

