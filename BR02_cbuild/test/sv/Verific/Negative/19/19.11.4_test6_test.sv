
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design connects ports of an instantiation using .* port
// connection rule. But the type of the elements connected using this style is
// different than that of the actual. This is an error.

typedef bit [0:3] MYTYPE1;
typedef enum {a,b,c,d,e,f,g,h} MYTYPE2;

module test ;

    MYTYPE1 in1, in2;
    MYTYPE2 out1, out2;

    bot i1 (.*);

endmodule

module bot(input MYTYPE1 in1, MYTYPE2 in2, output MYTYPE1 out1, MYTYPE2 out2);

    always @ (*)
    begin
        out1 = in1;
        out2 = in2;
    end

endmodule

