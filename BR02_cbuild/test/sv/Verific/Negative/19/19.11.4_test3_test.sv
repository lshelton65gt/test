
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines two modules and instantiates one from the
// other. The ports are connected using .* port connection rule.
// For some of the ports the type differs but they can be implicitly
// converted to the target type like wire to reg.
// The .* port connection width for is mismatching with port widths. This is an error.

module test (in1, in2, out1, clk);

output [0:3] out1;
input clk;
input [0:1] in1, in2;

reg [0:3] in;
wire [0:3] out;
       always@ (posedge clk)
       begin
              in = {in1, in2};
       end

       bot i1 (.*);

       assign out1 = out;
endmodule


module bot (in, out, clk);
input [0:2] in;
input clk;
output reg [0:3] out;
reg temp ;

       always @ (negedge clk) begin
           temp = in ;
           out = temp++;
       end

endmodule

