
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The default port direction is inout in SystemVerilog. This
// design defines a list of non-ansi port declaration. So this can be used to define
// default port direction, but it assumes the direction to be default, input. So this
// is a negative test case.

module test (clk, in1, in2, out1, out2);
    input clk;
    input [0:3] in1;
    output reg [0:3] out1;

    wire [0:3] in2;
    reg [0:3] out2;

    always @ (posedge clk)
    begin
        out1 <= in1 + out2; // out2 assumed as inout
        in2 <= in1; // in2 assumed as inout
        out2 <= in2;
    end

endmodule

