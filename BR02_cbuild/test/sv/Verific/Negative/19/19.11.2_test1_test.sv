
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a module having two ports of structure
// type. In its instantiation actuals for every member of structure type port is
// passed using different terminals mentioning member name with named port connection.
// This type of connection is illegal. One formal should have one actual.

typedef myType;

module test(str, out);
input myType str;
output myType out;

assign out = str;

endmodule

typedef struct {
                 int a;
		 bit [3:0]b;
	   } myType;


module bench;

myType out;

int in;
bit [3:0]benchBit;

  test instanMod(.str.a(in), .str.b(benchBit), .out); 
  
  //LRM is not clear whether this named port connection will be supported.
  
  initial
    begin
        repeat(20)
		  begin
		      in = $random;
			  benchBit = $random;
		  end
		$monitor("output int %d = input int %d, output bit %b = output bit %b", out.a, in, out.b, benchBit);  
	end

endmodule

