
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog supports default data type and default direction
// for module ports. But if no data type and direction is specified for the first port
// then it is assumed to be a non-ansi port declaration. Non-ansi and ansi port
// declaration can not be mixed. This design mixes the two type of port declarations.

module test(a, input b, output c);

    // no module item required, it's enough to have only this

endmodule

