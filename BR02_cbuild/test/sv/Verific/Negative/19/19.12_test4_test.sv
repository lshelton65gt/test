
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A logic type variable can not have two drivers from structural
// and behavioral constructs. This design defines two logic type variables and connects
// them to output ports of an instance. The same variables are used with ++/-- operator
// in a procedural area.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    logic [width-1:0] r1, r2;

    always@(posedge clk)
    begin
        out1 = r1<<width/2 + ++r2;
        out2 = --r1 - r2>>width/2;
    end

    bot #(width) bot_1 (clk, in2-in1, in1-in2, r1, r2);

    module bot #(parameter width=4)
                (input clk,
                 input [width-1:0] in1, in2,
                 output wire [0:width-1] out1, out2);

        assign out1 = in1 | ~in2;
        assign out2 = ~in1 ^ in2;

    endmodule

endmodule

