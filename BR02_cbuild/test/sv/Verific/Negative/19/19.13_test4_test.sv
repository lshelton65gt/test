
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Function and task declaration can not be nested. This design
// defines nested task - task inside another task to check the restriction.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    always@(posedge clk)
    begin
         task1(in1, in2, out1);
         task1(in2, in1, out2);
    end

    task automatic task1(input int i1, i2, output int out);

        task1(i2, i1, out);
        out++;

        task automatic task2(input int i1, i2, output int out); /* Error: task defined inside another task */
            out = ((i1 + i2) ^ (i2 - i1));
        endtask

    endtask

endmodule

