
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Function and task declaration can not be nested. This design
// defines nested function - function inside a task to check the restriction.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    always@(posedge clk)
    begin
        task1(in1, in2, out1);
        task1(in2, in1, out2);
    end

    task automatic task1(input int i1, i2, output int out);

        out = func1(i1, i2) | func1(i2, i1);

        function automatic int func1(input int i1, i2); /* Error: function defined inside a task */
            return ((i1 - i2) | (i2 - i1));
        endfunction

    endtask

endmodule

