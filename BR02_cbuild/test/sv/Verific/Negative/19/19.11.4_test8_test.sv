
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Implicit .* port connection must match the data type, name
// and size of the port connecting and the port being connected. This design connects
// ports of an instantiation using .* port connection rule. But the name of the ports
// do not match. This is an error.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    logic [width-1:0] i1, o1;
    bit [0:width-1] i2, O2;

    test_bot #(width) bot_1 (.*); /* non-matching name O2 <=> o2 */

    always@(negedge clk)
    begin
        i1 = in1 - -in2;
        i2 = -in1 | in2;
    end

    always@(posedge clk)
    begin
        out1 = -O2 * -o1;
        out2 = o1 ^ O2;
    end

    module test_bot #(parameter w = 4)
                     (input clk,
                      input bit [w-1:0] i1,
                      input logic [w-1:0] i2,
                      output logic [0:w-1] o1,
                      output bit [0:w-1] o2);

        always_ff@(posedge clk)
        begin
            o1 = i1 & -i2;
            o2 = -i2 ^ i1;
        end

    endmodule

endmodule

