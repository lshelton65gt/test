
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Hierarchical names consist of instance names separated
// by periods, where an instance name may be an array element. This design
// references non-existing hierarchical names from various places. This is an
// error and should be detected by the tool.

real i = 0;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    int i = 1, j = 2;

    always@(posedge clk)
    begin: al_beg
        reg [0:width-1] r1;

        r1 = in2 - in1;

        i /* of module test */ = test.in2 | test.k; /* element k does not exist */
        test.j = test.in1 ^ test.all_beg.r1; /* one of the elements, all_beg does not exist, though r1 exists */

        test.al_beg.r1[0+:width/2] = i & j;
        test.all_neg.r1[width/2-1-:width/2] = i * j; /* all_neg does not exist */

        test.out1 = r1 + $root.i - i;
        test.out2 = r1 + j;
    end

endmodule

