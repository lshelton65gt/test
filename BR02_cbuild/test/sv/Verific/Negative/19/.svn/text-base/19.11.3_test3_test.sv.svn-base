
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): It is illegal to implicitly connect ports of an instantiation
// with same name but incompatible data types. This is an error. This design connects
// events with real data type using implicit 'dot-name i.e .(<port_expression>)' port
// connection style.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output [width-1:0] out1, out2) ;

    reg [0:width-1] in ;
    reg en ;
    wire [0:width-1] out ;

    always@ (posedge clk)
    begin
        in = { in1[0+:width/2], in2[width/2-1-:width/2] } ;
        en = ^in ;
    end

    bot #(width) bot_1(.in, .en, .out) ;

    assign out1 = ~out, out2 = -out ;

endmodule


module bot #(parameter width=8)
            (input [0:width-1] in,
             input event en,
             output reg [width-1:0] out) ;

    always@(en, in)
    begin
        if(en)
            out = 1*in*1 ;
        else
            out = 0 ;
    end

endmodule

