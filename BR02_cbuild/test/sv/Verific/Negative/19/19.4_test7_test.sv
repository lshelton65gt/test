
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): $root scope can not have procedural statements. This design
// uses procedural statements in the $root scope to initialize some variables.

int count, sum;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    always@(posedge clk)
    begin
        count++;
        sum += count;

        out1 = in1<<width/2 & in2>>width/2;
        out2 = { in2[0:width/2-1], in1[width/2:width-1] };
    end

endmodule

initial 
begin
    count = 0;
    sum = count;
end

