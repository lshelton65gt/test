
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): When using implicit .* port connection, all the matching
// variables must be declared either as a port of the parent module or as a net
// or variable, default net is not allowed. This design connects ports of an
// instantiation using .* port connection rule but does not declare all the
// variables and thus rely on default net. This is an error.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    logic [width-1:0] i1;
    bit [0:width-1] i2, o2;

    test_bot #(width) bot_1 (.*); /* o1 is not declared */

    always@(negedge clk)
    begin
        i1 = -in1 * -in2;
        i2 = in1 & in2;
    end

    always@(posedge clk)
    begin
        out1 = -o2;
        out2 = ~o2;
    end

    module test_bot #(parameter w = 4)
                     (input clk,
                      input bit [w-1:0] i1,
                      input logic [w-1:0] i2,
                      output logic [0:w-1] o1,
                      output bit [0:w-1] o2);

        always_ff@(posedge clk)
        begin
            o1 = -i1 | i2;
            o2 = i2 + -i1;
        end

    endmodule

endmodule

