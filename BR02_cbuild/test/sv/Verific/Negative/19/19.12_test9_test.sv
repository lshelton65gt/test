
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In the case of unpacked arrays as a port type, both the
// variables must have same dimension and size. This design connects ports of
// type unpacked array for which the number of unpacked dimensions do not match.

logic r1 [7:0][7:0][7:0] ;
logic r2 [0:3][0:3][0:3] ;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2) ;

    bot #(width) bot_1 (clk, r1, r2) ;

    int i, j ;

    always@(negedge clk)
    begin
        int k = 0 ;
        for(i=0 ; i<4 ; i++)
            for(j=0 ; j<4 ; j++)
                for(k=0 ; k<4 ; j++)
            begin
                r1[i][j][k] = in1[k] + in2[width-k-1] ;
                r2[i][j][k] = in1[k] - in2[width-k-1] ;
                k++ ;
                if (k>=width)
                    k = 0 ;
            end
    end

    always@(posedge clk)
    begin
        int k = 0 ;
        for(i=0 ; i<4 ; i++)
            for(j=0 ; i<8 ; i++)
            begin
                out1[k] = r1[i][j][k] | r2[i][j][k] ;
                k++ ;
                if (k>=width)
                    k = 0 ;
            end
    end

endmodule

module bot #(parameter width=4)
            (input clk,
             input in[7:0][3:0],
             output reg out[0:3][0:7]) ;

    always@(posedge clk)
    begin
        int i, j ;
        for(i=0 ; i<4 ; i++)
            for(j=0 ; j<8 ; j++)
                out[i][j] = '1 ^ in[j][i] ;
    end

endmodule

