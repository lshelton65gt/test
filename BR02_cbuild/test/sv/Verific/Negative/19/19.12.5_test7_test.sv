
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design instantiates an interface array. This array 
// instance is accessed hierarchically from another module and used as actual
// to instance array of module. In actual expression modport name of interface
// is specified by selecting on interface array instance. This is an error, 
// selection should be on single interface i.e on an element of interface array.

interface combination(input reg clk) ;
    reg r ;
    int num ;

    modport master(input clk, input r, output num) ;
    modport slave(input clk, output r, input num) ;
endinterface

module test(interface a);
    always @(posedge a.clk) begin
       a.num = a.r ;     
    end
endmodule

module hier ;

   reg clock ;
   combination com[1:0] (clock) ;
   
endmodule

module bench ;

   hier check() ;

   test t1[1:0](check.com.master) ; // Error: can not be selected on com!
   test_slave ts (check.com[0].slave) ;

   initial begin
      check.clock = 0 ;
      check.com[0].r = '1 ;
      check.com[1].r = '0 ;
      check.com[0].num = '0 ;
      check.com[1].num = '1 ;
      #100 $finish ;
   end

   always  
      #5 check.clock = ~check.clock ;

endmodule

module test_slave(interface b) ;

   always @(negedge b.clk)
      b.r = b.num ;

endmodule

