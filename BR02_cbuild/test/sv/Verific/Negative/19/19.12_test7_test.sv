
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): If the vector widths of an inout port connection do not
// match, then it is an error. This design connects an inout port to an element
// of different vector width. This is an error and should be trapped by the tool.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    wire [width/2-1:0] r1;
    wire [(3*width)/2-1:0] r2;

    always@(posedge clk)
    begin
        out1 = r1<<width/4 & r2>>width/2;
        out2 = r1>>width/4 | r2<<width/2;
    end

    bot #(width) bot_1 (clk, r1, r2); /* ERROR: port width for the inout ports do not match */

    module bot #(parameter width=4)
                (input clk,
                 inout [0:width-1] io1, io2);

        wire [width-1:0] t1, t2;

        assign t1 = io1 * io2;
        assign t2 = io2 ^ io1;

        assign io1 = t1 + t2;
        assign io2 = t2 - t1;

    endmodule

endmodule

