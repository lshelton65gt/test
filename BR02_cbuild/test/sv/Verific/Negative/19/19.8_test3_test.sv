
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): No declaration of in2 exists. This is an error.

`default_nettype wire

module test;
    wire [0:3] out1, out2;
    wire [0:3] n4;
    reg [0:3] in1;
    reg clk;

    // error as inout port in2 has size 1 and net n4 is of size 4
    bot i (clk, in1, n4, out1, out2);

endmodule

module bot(clk, in1, in2, out1, out2);
    input clk;
    input [0:3] in1;
    output reg [0:3] out1, out2;

    always @ (posedge clk)
    begin
        out1 <= in1 + out2; // out1 is inout
        in2 <= in1;         // should not generate a warning as in2 is inout
        out2 <= in2;        // out2 is inout
    end

endmodule

