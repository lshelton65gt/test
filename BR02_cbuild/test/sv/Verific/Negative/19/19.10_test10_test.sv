
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Timeprecision and timeunit declaration should precede any
// other item within module scope, because other items may contain delay values,
// which depends on timeunit or timeprecision. This design uses a timeunit
// declaration preceded by an assignment statement that actually uses delay control
// expression.

module test;

    int a;
    assign #5 a = 100;
    timeunit 100ns; // Error : timeunit should be the first module item

    initial
    begin
        $monitor($time,,, "data (base 10ns) = %d", a);
        a = 20;
        repeat(5)
            #5 a++;
    end

endmodule

