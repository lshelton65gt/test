
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines two modules. One of the modules is instantiated
// twice from the other. The ports are connected using dot name port connection rule, but
// for one instantiation the size does not match the overridden size. This is an error.

module test (out, in, clk);

    input [0:7] in;
    input clk;
    output [0:7] out;

    bot #(.p(8)) i1 (.out, .in, .clk);
    bot #(.p(4)) i2 (.out, .in, .clk);

endmodule

module bot (out, in, clk);
    parameter p = 4;
    input clk;
    input [0:p-1] in;
    output [0:p-1] out;
    reg [0:p-1] out;
    reg [0:p-1] temp ;

   always@ (posedge clk) begin
       temp = in ;
       out = temp++;
   end

endmodule

