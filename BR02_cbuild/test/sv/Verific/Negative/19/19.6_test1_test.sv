
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses a 'for-generate' loop statement. The
// for-generate contains a nested module and an instantiation of that module.
// This is an error as module can not be declared inside generate

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [width-1:0] in2, 
              output reg [width-1:0] out1);

    generate
        genvar i;
        for(i=0; i< width; i++)
        begin: for_gen_i
           module inner (input in1, in2, output out1);
               always@(posedge clk)
                  out1 = in1 | in2 ;
           endmodule

           inner I(in1[i], in2[i], out1[i]) ;
        end
    endgenerate


endmodule

