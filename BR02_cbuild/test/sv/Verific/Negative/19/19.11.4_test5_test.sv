
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design connects ports of an instantiation using .* port
// connection rule. But the size of the elements connected using this style is
// different than that of the actual. This is an error.

module test (out, in, clk);

    input [0:7] in;
    input clk;
    output [0:7] out;

    bot #(.p(8)) i1 (.*);
    bot #(.p(4)) i2 (.*);

endmodule

module bot (out, in, clk);
    parameter p = 4;

    input clk;
    input [0:p-1] in;
    output [0:p-1] out;

    reg [0:p-1] out;
    reg temp ;

    always@ (posedge clk) begin
        temp = in ;
        out = temp++;
    end

endmodule

