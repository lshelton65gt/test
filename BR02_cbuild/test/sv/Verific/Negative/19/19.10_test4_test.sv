
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Timeprecision and timeunit declaration should precede any other
// item in the top level, because other items may contain delay values, which depends
// on timeunit or timeprecision. This design uses a timeprecision declaration preceded
// by some other item.

int i = 10;

timeunit 100ns;
timeprecision 0.10ns;

module test;

    int a;

    initial
    begin
        $monitor($time,,, "data (base 10ns) = %d", a);
        a = 20;
        repeat(10)
            #5.34112ns a++;
    end

endmodule

