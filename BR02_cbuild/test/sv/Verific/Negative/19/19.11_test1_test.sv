
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a module and the module instantiates itself
// inside the module. This causes infinite recursion. This is an error.

module test #(parameter w = 8)
             (input clk, input [w-1:0] in1, output [w-1:0] out1);

    test #(.w(w)) test1 (clk, in1, out1);

endmodule

