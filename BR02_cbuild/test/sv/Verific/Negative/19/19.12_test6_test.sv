
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A named interface type port can not be connected to an interface
// of a different type (not a generic interface). This design connects a named interface
// to a named interface port of different name but they are having same elements in them
// - this is a special case.

module test #(parameter width=8)
             (input clk,
              input logic [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    iface2 ifc1(clk);

    bot bot_1 (ifc1.mp);

    always@(negedge clk)
    begin
        ifc1.in1 = in1 - in2;
        ifc1.in2 = in2 + in1;
    end

    always@(posedge clk)
    begin
        out1 = ifc1.out1 ^ ifc1.out2;
        out2 = ifc1.out2 | ifc1.out1;
    end

endmodule

module bot (iface1.mp ifc);

    always@(ifc.clk)
    begin
        ifc.out1 = ifc.in1 & ifc.in2;
        ifc.out2 = ifc.in2 * ifc.in1;
    end

endmodule

interface iface1(input clk);
    int in1, in2;
    int out1, out2;

    modport mp(input in1, in2,clk, output out1, out2);
endinterface

interface iface2(input clk);
    int in1, in2;
    int out1, out2;

    modport mp(input in1, in2, clk,output out1, out2);
endinterface

