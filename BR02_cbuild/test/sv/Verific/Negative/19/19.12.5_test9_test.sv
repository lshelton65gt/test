
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation with multiple unpacked dimensions.
// The formal have multiple unpacked dimensions but the actual does not have same number
// of unpacked dimensions.

module test ;
    reg [3:0][1:0] C [1:0][1:0][5:4] ;

    initial begin
        #1 C[1][1][5] = 8'b01010011 ;
           C[0][1][5] = 8'b00000011 ;
           C[1][1][4] = 8'b01010000 ;
           C[0][1][4] = 8'b11110011 ;
    end

    bot b[1:0] (C) ;

endmodule

module bot (input [7:0] C [2:3]) ;
    initial
    begin
        #5 $display($time,,"%m %b\n", C[2]) ;
           $display($time,,"%m %b\n", C[3]) ;
    end
endmodule
