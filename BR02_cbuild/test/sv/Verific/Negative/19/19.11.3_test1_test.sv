
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): If modport name is specified in interface type port declaration,
// then only the modport members can be accessed. Here port 'clk' of interface is accessed
// though it is not defined within modport.

interface ifc(input clk);
    reg [7:0] i1, i2;
    reg [7:0] o1, o2;

    modport mp(input i1, i2, output o1, o2);
endinterface

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    always@(negedge clk)
    begin
        if1.i1 = in1 ^ in2;
        if1.i2 = in1 - in2;
    end

    always@(posedge clk)
    begin
        out1 = if1.o1 | if1.o2;
        out2 = if1.o1 + if1.o2;
    end

    module test_bot (ifc.mp if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = if1.i1 * if1.i2;
            if1.o2 = if1.i2 & if1.i1;
        end

    endmodule

    test_bot bot_1 (.if1);

    ifc if1(clk);

endmodule

