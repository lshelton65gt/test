
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design checks whether the tool can detect the difference
// in data type for ports connected by the dot name connection rule. This design uses
// different user defined data types than the declared to connect to ports. This should
// be detected as an error.

typedef bit [0:7] MYTYPE1;
typedef enum {a,b,c,d,e,f,g,h} MYTYPE2;

module test ;

MYTYPE1 in1, in2;
MYTYPE2 out1, out2;

bot i1 (.in1, .in2, .out1, .out2);

endmodule

module bot(input MYTYPE1 in1, MYTYPE2 in2, output MYTYPE1 out1, MYTYPE2 out2);

    always @ (*)
    begin
        out1 = in1;
        out2 = in2;
    end

endmodule

