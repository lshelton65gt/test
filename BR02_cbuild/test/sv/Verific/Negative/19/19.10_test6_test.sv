
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Timeunit and timeprecision must be a power of 10 units.
// This design uses a timeunit that is not power of 10. This is an error.

timeunit 33ns;

module test;

    int a;

    initial
    begin
        $monitor($time,,, "data (base 10ns) = %d", a);
        a = 20;
        repeat(5)
            #5 a++;
    end

endmodule

