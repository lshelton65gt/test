
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines two packages. Both packages define 
// enum type having literal 'FALSE'. One import declaration imports all identifiers
// of package 'p' and other imports only enum type of package 'q'. So enum
// literal 'FALSE' of package 'p' is visible to module. This literal is assigned
// to object of enum type declared in package 'q'. This is an error in type resolving.

package p ;
    typedef enum { FALSE, TRUE } bool_t ;
endpackage

package q ;
    typedef enum { ORIGINAL, FALSE } teeth_t ;
endpackage

module test ;

    import p::* ;
    import q:: teeth_t ;

    teeth_t myteeth ;

    initial
    begin
        myteeth = FALSE ; // Literal of enum bool_t assigned to obj of teeth_t :ERROR
    end

endmodule: test

module test2 ;
    import p::* ;
    import q::teeth_t, q::ORIGINAL, q::FALSE ;
    teeth_t myteeth ;

    initial myteeth = FALSE ;
endmodule

