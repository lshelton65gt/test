
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): $root top level cannot have an initial or always block.
// This design defines an always_comb block in the $root scope, this should
// be flagged as an error by the tool.

reg global_var;
int count;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    always@(posedge clk)
    begin
        out1 = in1 - in2;
        out2 = in2 - in1;
        global_var = ~global_var;
    end

endmodule

always_comb
begin
    count += global_var;
end

