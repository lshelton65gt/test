
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design tests the cases of name conflicts that SystemVerilog
// allows. It defines a new structure type using typedef in the global
// scope. A data element of that type is defined in the global area with
// the same name as a module.

typedef struct {
    int b;
} myType;

myType test;

module test;

    int a;

    assign a = 10;
    assign test.a = 100;

endmodule

