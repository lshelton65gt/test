
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case, when timeunit is not a power of 10.

timeunit 50ns;

module test;

    timeunit 1s;

    module nestMod;
        timeunit 10ms;
    endmodule

    initial
        $display("THERE SHOULD BE TWO ERRORS DISPLAYED BY THE TOOL");
        
endmodule


