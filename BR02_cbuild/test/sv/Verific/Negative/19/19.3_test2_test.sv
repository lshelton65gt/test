
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): No module is defined, all the work is done in $root

`define SIZE 16

int array[0:`SIZE-1]; 
//int array[0:`SIZE-1] = { 23, 3, 2, 10, 12, 0, 15, 44, 33, 9, 10, 11, 17, 22, 5, 7 }; 
array[0] = 23;
array[1] = 3;
array[2] = 2;
array[3] = 10;
array[4] = 12;
array[5] = 0;
array[6] = 15;
array[7] = 44;
array[8] = 33;
array[9] = 9;
array[10] = 10;
array[11] = 11;
array[12] = 17;
array[13] = 22;
array[14] = 5;
array[15] = 7;

function void display_array(input int a[0:`SIZE-1]);
	for (int i = 0; i < `SIZE; i++)
	begin
		int data;
		data = a[i];
		$display ("Loc:%d Value:%d\n", i, data);
	end
	return;
endfunction

function void swap (input int a[0:`SIZE-1], left, right);
	int temp;
	temp = a[left];
	a[left] = a[right];
	a[right] = temp;
	return;
endfunction

function void sort (input int a[0:`SIZE-1]);
	for (int i = 0; i < `SIZE; i++)
	begin
		for (int j = 0; j < `SIZE-1; j++)
		begin
			if (a[j] > a[j+1])
			begin
				swap (a, j, j+1);
			end
		end
	end
	return;
endfunction


task main(input int a[0:`SIZE-1]);
	display_array(a);
	sort(a);
	display_array(a);
	return;
endtask

main(array);

