
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Instantiate modules defined in different scopes using hier names.

module test(i, o) ;

    parameter p = 8 ;

    input [p-1:0] i ;
    output [p-1:0] o ;

    bot1      b1(o[p/2-1:0], i) ;
    test.bot2 b2(o[p-1:p/2], i) ;

    module bot2(o, i) ;

        parameter p = 4 ;

        input [p-1:0] i ;
        output [p-1:0] o ;
        assign o = ~i ;

    endmodule

endmodule

module bot1(o, i) ;

    parameter p = 4 ;

    input [p-1:0] i ;
    output [p-1:0] o ;

    assign o = i ;

endmodule

