
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): $root top level should not have an initial or always block.
// This design defines an initial block in the $root scope, this should be flagged
// as an error.

int global_var;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    always@(posedge clk)
    begin
        out1 = in1 - in2;
        out2 = in2 - in1;
        global_var++;
    end

endmodule

initial
begin
    global_var = 0;
end

