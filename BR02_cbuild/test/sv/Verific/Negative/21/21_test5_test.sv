
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In design statement, the identified cell is configuration.
// It is an error case.

library lib1 "test.v" ;

config config1;
     design lib1.foo;  // Identified cell is configuration
     default liblist lib1 ;
endconfig

config foo;
    design lib1.test;
    default liblist lib1;
endconfig

module test(output out, input in1, in2) ;

    assign_g I1(out, in1, in2) ;

endmodule

module assign_g(output out, input in1, in2) ;

    assign out = in1 + in2 ;

endmodule

