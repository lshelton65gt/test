
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): If a file name matches path specifications in multiple library
// definitions, it should be an error.

library lib1 "lib/rtl/g*.v" ;
library lib2 "lib/rtl/" ;
library lib3 "lib/../lib/rtl/*.v" ; // module andg and org of file lib/rtl/gates.v are
                                    // mapped to library lib1 and lib3, so error.

module top(output out1, out2, input in1, in2) ;

    andg I1(out1, in1, in2) ;
    org I2(out2, in1, in2) ;

endmodule

