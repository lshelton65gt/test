
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Multiple default selection clause in configuration.
// It is an error case.

library aLib "test.v" ;
library rtlLib "gates.v" ;
library gateLib "gates.vg" ;

config cfg1 ;
    design MUX2_1 ;
    default liblist rtlLib;
    default liblist gateLib;
endconfig

module MUX2_1(input [1:0] D, input S, output Y) ;

    wire S_bar ;
    assign S_bar = ~S;

    wire and_out0, and_out1 ;

    andg I0(and_out0, S_bar, D[0]) ;
    andg I1(and_out1, S, D[1]) ;
    org  I3(Y, and_out0, and_out1) ;

endmodule

