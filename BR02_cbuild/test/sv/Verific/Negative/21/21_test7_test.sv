
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of configuration without design statement. It is an error case.

config config1 ;
    default liblist ;
endconfig

module top (output out, input in) ;

   inv I1(out, in) ;

endmodule

module inv(output out, input in) ;

    assign out = ~in ;

endmodule

