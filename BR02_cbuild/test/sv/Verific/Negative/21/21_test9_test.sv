
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of instance selection clause where top level module
// name is omitted. It is error case.

library aLib "test.v" ;
library bLib "sub.v" ;
library cLib "add.v" ;

config cfg ;
    design aLib.top ;
    instance I1 use cLib.child; // instance name without top level module name. 
endconfig

module top(cout, sum, in1, in2) ;
    parameter SIZE = 4 ;
    
    output cout ;
    output [SIZE-1:0] sum ; 
    input [SIZE-1:0] in1, in2 ;
    
    child I1(cout, sum, in1, in2) ;

endmodule

module child(cout, sum, in1, in2) ;
    parameter SIZE = 4 ;
    
    output cout ;
    output [SIZE-1:0] sum ; 
    input [SIZE-1:0] in1, in2 ;

    adder I1(cout, sum, in1, in2) ;

endmodule

module adder(cout, sum, in1, in2) ;
    parameter SIZE = 4 ;
    
    output cout ;
    output [SIZE-1:0] sum ; 
    input [SIZE-1:0] in1, in2 ;

    assign {cout, sum} = in1 + in2 + 1;

endmodule

