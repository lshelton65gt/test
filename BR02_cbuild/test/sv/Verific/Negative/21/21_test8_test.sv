
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test of hierarchical configuration. If an instance
// clause specifies a hierarchical path to an instance which occurs within a hierarchy
// specified by another config. So, it is an error.

library lib1 "test.v" ;
library lib2 "volt.v" ;
library lib3 "current.v" ;
library hLib "hparams.v" ;

module two_port_network(output real v1, i2,
                        input real i1, v2) ;

    voltMod I0(v1, i1, v2) ;
    currentMod I1(i2, i1, v2) ;

endmodule

config cfg ;
    design two_port_network ;
    instance two_port_network.I0 use lib2.voltMod:config ;
    instance two_port_network.I1 use lib3.currentMod:config ;
    instance two_port_network.I0.I1_1 use hLib.h11_gen ; 
    // ERROR can't set use clause for two_port_network.I0.I1_1 from this config
endconfig
 
