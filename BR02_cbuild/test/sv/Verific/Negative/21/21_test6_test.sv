
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Design statement after default config rule. This is an error case.

config config1 ;
    default liblist ;
    design top ;
endconfig

module top (output out, input in) ;

   inv I1(out, in) ;

endmodule

module inv(output out, input in) ;

    assign out = ~in ;

endmodule

