
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of configuration with multiple design statements.
// It is an error case

library lib1 "test.v";
library lib2 "adder.*" ;
library lib3 "adder.vg" ;

config config1 ;
    design lib1.BinAdder ;
    design lib3.full_adder;
    default liblist lib2 lib1;
endconfig

module BinAdder(output cout, output [3:0] sum,
                input [3:0] A, B, C, input Cin) ;

    wire C0, C1, C2;

    full_adder f0(C0, sum[0], A[0], B[0], Cin) ;
    full_adder f1(C1, sum[1], A[1], B[1], C0) ;
    full_adder f2(C2, sum[2], A[2], B[2], C1) ;
    full_adder f3(cout, sum[3], A[3], B[3], C2) ;

endmodule

module half_adder(output reg C, S, input A, B) ;

    always @(A, B)    
    begin
        if(A != B)
            S = 1;
        else
            S = 0;
        C = A & B ;
    end

endmodule

