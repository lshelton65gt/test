
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of library mapping pattern matching. It just tests some
//                     patterns and should error out for multiple pattern matches.

module test (i, o) ;
    input i ;
    output o ;
    wire t ;

    bot1 b1 (i, t) ;
    bot2 b2 (t, o) ;
endmodule 

