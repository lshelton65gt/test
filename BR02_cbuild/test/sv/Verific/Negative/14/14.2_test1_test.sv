
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design tries to use a semaphore without first creating
// it by calling 'new'. This is an error.

module test ;
    semaphore sm ;
    initial begin 
        sm = new(-10) ;
        if (sm == null)
            $display(" Semaphore not created ") ;
         else
            $display(" Semaphore created" );
         sm.put(-3) ;   //Error: call of method on a semaphore variable which hasn't been created
    end    
endmodule

