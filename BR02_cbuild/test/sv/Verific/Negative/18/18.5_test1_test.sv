
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Cross coverage is allowed only between coverage points
// defined within the same coverage group. Attempts to cross items from different
// coverage groups shall result in a compiler error.

module test (input clk, output out) ;
    bit [7:0] v_a, v_b ;
    bit [3:0] xyz ;
    covergroup cg1 @(posedge clk) ;
        a: coverpoint v_a
        {
            bins a1 = { [0:63] } ;
            bins a2 = { [64:127] } ;
            bins a3 = { [128:191] } ;
            bins a4 = { [192:255] } ;
        }
        b: coverpoint v_b
        {
            bins b1 = {0} ;
            bins b2 = { [1:84] } ;
            bins b3 = { [85:169] } ;
            bins b4 = { [170:255] } ;
        }
    endgroup 

    covergroup cg2 @(posedge clk) ;
        c : coverpoint xyz
        {
            bins c1 = {0} ;
            bins other = default ;
        }
        d : cross v_a, c // Error : cross between two different coverpoints defined in two different covergroups
        {
            bins d3 = binsof(a.a1) && binsof(cg1.b) ;
        }
    endgroup

    assign out = 1'b1 ^ clk ;

    cg cov = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;
endmodule

