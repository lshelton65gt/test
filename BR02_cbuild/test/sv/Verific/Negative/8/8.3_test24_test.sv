
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S):  Negative test case on having timing control with incrementor 
//  or decrementor operator.

module test;

initial
begin
	for(int i = 0; i <= 1; #5 i++)
		$display("FAILURE\n");
end

endmodule

