
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a structure containing a member of type real.
// They are used with operators that are not allowed on real variables. So this is an error.

typedef struct {
    real r;
} st_r_sr;

module test;

    st_r_sr r1, r2;

    initial
    begin
        r1.r = +0.01;

        r2.r ^= r1.r;

        $display("r1.r = ", r1.r);
        $display("r2.r = ", r2.r);

        r2.r &= r1.r;

        $display("r1.r = ", r1.r);
        $display("r2.r = ", r2.r);

        r2.r |= r1.r;

        $display("r1.r = ", r1.r);
        $display("r2.r = ", r2.r);
    end

endmodule

