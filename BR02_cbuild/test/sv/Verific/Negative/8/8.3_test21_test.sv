
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In SystemVerilog, assignment operators can be used inside an
// expression, provided there is no delay/timing control. This design
// uses assignment operators inside expressions with
// explicit timing control inside a generate block. This is an error.

module test#(parameter w = 8)
            (input [w-1:0] in1, in2,
             output reg [0:w-1] out1, out2);

    reg [w-1:0] a, b;
    genvar i;

    generate

        for(i=0; i<w; i++)
        begin: for_gen_i
            always@(in1[i], in2[i], a[i], b[i])
            begin
                 a[i] = (in1[i] |= #5 1);
                 b[i] = (in2[i] &= #5ns a);

                 out1[i] = #5ns a[i] * in2[i];

                 out2[i] = #5 b[i] + in1[i];
            end
        end

    endgenerate

endmodule

