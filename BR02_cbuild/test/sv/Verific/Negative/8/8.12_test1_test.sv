
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Unsized literal can not be used in concatenation, but when 
// concatenation is used as array literal, unsized literal can be used.

module test;

    bit [3:0] b1;
    logic [3:0] l1;
    bit b2 [3:0] = '{1, 0, 1};  /* Legal array initialization */
    logic l2 [3:0] = '{'bx, 0, 1};  /* Legal array initialization */

    initial
    begin
        b1 = '{0, 1, 0}; /* Error: unsized literal can not be used in concatenations */ 

        l1 = '{0, 1, 'bx}; /* Error: unsized literal can not be used in concatenations */ 
    end

endmodule

