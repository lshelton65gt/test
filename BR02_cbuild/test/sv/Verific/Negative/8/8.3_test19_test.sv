
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a task. The task has an inout port. The task
// is called with the inout port connected to a data element that
// uses '++' in pre and post increment modes. This is illegal for inout
// ports and hence this is a negative test case.

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    logic [w-1:0] temp1, temp2;

    always@(posedge clk)
    begin
        temp1 = in1 - in2;
        temp2 = in2 + in1;

        tsk1(temp1++);
        tsk1(++temp1);

        out1 = temp1 & temp2;
        out2 = temp2 ^ temp1;
    end

    task tsk1(inout [w-1:0] io);
        io = ((io > (1<<(w/2))) ? io<<1 : io >> 1);
    endtask

endmodule

