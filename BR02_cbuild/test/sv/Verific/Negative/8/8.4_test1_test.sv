
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Unary reduction operators cannot be applied to unpacked
// arrays. This design applies unary reduction operators to an unpacked array.
// This is an error.

module test ;

    bit b1 [3:0] ; /* 2-state */
    logic l1 [3:0] ; /* 4-state */

    initial
    begin
        for(int i=0; i<4; i++)
        begin
            b1[i] = 1'b0 ;
            l1[i] = 1'bx ;
        end

        if (^b1 !== 1'b1) // Error: b1 is unpacked array
            $display("^ is not working properly") ;

        if (|l1 !== 1'b1) // Error: l1 is unpacked array
            $display("| is not working properly") ;
    end

endmodule

