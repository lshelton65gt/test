
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In SystemVerilog, assignment operators can be used inside an
// expression, provided there is no delay/timing control. This design
// uses assignment operators inside expressions with
// explicit timing control in a generate step control expression.
// This is an error.

module test#(parameter w = 8)
            (input [w-1:0] in1, in2,
             output reg [0:w-1] out1, out2);

    reg [w-1:0] a, b;
    genvar i;

    generate

        for(i=0; i<w; i += #5 1)
        begin: for_gen_i
            always@(in1[i], in2[i], a[i], b[i])
            begin
                 a[i] = (in1[i] *= 1);
                 b[i] = (in2[i] ^= a);

                 out1[i] = a[i] - in2[i];

                 out2[i] = b[i] | in1[i];
            end
        end

    endgenerate

endmodule

