
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S) : The width of the streaming operator must be greater than
// or equal to the source. The following design checks this.

module test (input in, output out) ;
    int a, b, c;
    int j = {>>{ a, b, c }};    // error: j is 32 bits < 96 bits
    assign out = in ;
endmodule

