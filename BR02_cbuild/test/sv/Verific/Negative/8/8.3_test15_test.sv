
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines two modules. The bottom module is instantiated
// from the other module. The inout port of the instantiations are
// connected to data elements that use '++' in pre and post increment
// mode. This is illegal for inout ports and hence it is a negative
// test case.

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    logic [w-1:0] temp1, temp2;

    always@(negedge clk)
    begin
        temp1 = in1 - in2;
        temp2 = in2 - in1;
    end

    always@(posedge clk)
    begin
        out1 = temp1 & temp2;
        out2 = temp2 ^ temp1;
    end

    bottom #(.w(w)) bot1(clk, temp1++);
    bottom #(.w(w)) bot2(clk, ++temp2);

    module bottom #(parameter w = 8)
                   (input clk, inout reg [w-1:0] io);

        always@(posedge clk)
            io = ~io;

    endmodule

endmodule

