
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses casting on left hand side operand. The casting
// actually sets the size of the expression. This is an error. This
// should be handled by the tool.

module test #(parameter w = 8)
             (input clk,
              input bit [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    bit [w-1:0] b1, b2;

    always@(posedge clk)
    begin
        4'(b1) = in1; /* Error: casting on left hand side */
        32'(b2) = in2; /* Error: casting on left hand side */

        out1 = 16'(b1);
        out2 = 8'(b2);
    end

endmodule

