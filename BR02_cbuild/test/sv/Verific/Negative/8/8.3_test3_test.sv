
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assignement operators inside an expression must be enclosed inside
// parentheses, like a=(b*=2). This design uses assignment operators
// inside expressions without the parentheses.

module test#(parameter w = 8)
            (input [w-1:0] in1, in2,
             output reg [0:w-1] out1, out2);

    reg [w-1:0] a, b;

    always@(in1, in2, a, b)
    begin
         a = ++in1;
         a = a += 5;
         b = a *= in2;

         out1 = a | b;
         out2 = b ^ a;
    end

endmodule

