
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses casting on left hand side operand. The casting
// actually changes the type of the expression. This is illegal.

module test #(parameter w = 8)
             (input clk,
              input bit [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    bit [w-1:0] b1, b2;
    typedef bit [2*w-1:0] my_width ;

    always@(posedge clk)
    begin
        my_width'{b1, b2} = in1 ^ in2; 

        out1 = 16'({b1, b2});
        out2 = 8'({b2, b1});
    end

endmodule

