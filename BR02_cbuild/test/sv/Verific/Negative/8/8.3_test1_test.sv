
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog does not allow assignment operators in event
// control expression. This design uses the assignment operator in the event control
// expression.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2, r3;

always @((r1 = in1 ^ in2))
begin
    r2 = r1 + in1;
    r3 = r1 | in2;

    out1 = r1 - r2 + r3;
    out2 = r1 | r2 & r3;
end

endmodule

