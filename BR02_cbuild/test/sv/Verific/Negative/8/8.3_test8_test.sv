
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines two modules. The bottom module is instantiated
// from the other module. The output port of that instantiations are
// connected with data elements that uses '--' in pre and post decrement
// mode. This is illegal for output ports and hence it is a negative
// test case.

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    wire [w-1:0] temp1, temp2;

    always@(posedge clk)
    begin
        out1 = temp1 & temp2;
        out2 = temp2 ^ temp1;
    end

    bottom #(.w(w)) bot1(clk, in1, temp1--);
    bottom #(.w(w)) bot2(clk, in2, --temp2);

    module bottom #(parameter w = 8)
                   (input clk, input [w-1:0] in, output reg [w-1:0] out);

        always@(posedge clk)
            out = ~in;

    endmodule

endmodule

