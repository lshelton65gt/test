
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Timing control in a blocking assignment is not allowed.

module test;
int a, b;
initial
begin
	a = (#5 b = 5);
end
endmodule 
