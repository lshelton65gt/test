
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog added new increment and decrement operators
// in pre or post mode like ++/--. These operators can only be
// used with blocking assignments. This design uses these pre and
// post increment operators in concurrent area with assign statement.

module test#(parameter w = 8)
            (input [w-1:0] in1, in2,
             output [0:w-1] out1, out2);

    wire [w-1:0] a, b;

    assign a = ++in1;
    assign out1 = a--;

    assign b = in2++;
    assign out2 = --b;

endmodule

