
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines two modules. The bottom module is instantiated
// from the other module. The output port of that instantiations are
// connected with data elements that uses '--' in pre and post decrement
// mode. This is illegal for output ports and hence it is a negative
// test case.

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    logic [w-1:0] temp1, temp2;
    assign temp1 = in1;
    assign temp2 = in2;

    bottom #(.w(w)) bot1(clk, temp1--);
    bottom #(.w(w)) bot2(clk, --temp2);

    module bottom #(parameter w = 8)
                   (input clk, output reg [w-1:0] io);

        always@(posedge clk)
            io = ~io;

    endmodule

endmodule


