
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses '++' operator as step assignment in for-generate,
// but it increments a genvar that is not loop index variable. This is an error as loop
// index variable should be incremented in step assignment.

module test (i1, o1) ;

    parameter p1 = 4 ;

    input [p1-1:0] i1 ;
    output [p1-1:0] o1 ;

    generate
        genvar i, j ;
        for (i=0; i<p1; j++)
        begin: for_gen_i
            assign o1[i] = i1[i] ;
        end
    endgenerate
    
endmodule

