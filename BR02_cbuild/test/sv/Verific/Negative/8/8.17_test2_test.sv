
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S) : The number of bits in the source expression should be
// greater than or equal to the number of bits in the target for an unpack
// operation using streaming operator.

module test (input in, output out) ;
    int a, b, c ;
    initial
        {>>{ a, b, c }} = 23'b1 ;    // error: too few bits in stream
    assign out = in ;
endmodule

