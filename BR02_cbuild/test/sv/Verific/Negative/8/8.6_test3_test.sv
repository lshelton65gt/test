
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): If any operand is real then the result is real. This design checks this.

module test;

    real r1;
    real r2;

    integer i, j;

    initial
    begin
        r1 = 10.10;
        i = (j = 10);
        r2 = (j |= (r1 + i)); /* r1 + i should be real. '|' is not defined on real, so error */

        $display("real r2, casted to integer = %d", int'(r2));
    end

endmodule

