
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case with real and shortreal data types with 
// various assignment operators.

module test;
real mod_real = 12.3;
shortreal mod_short = 1.37;
initial
begin
	mod_real &= 4'b0101;
	mod_short |= 'x;
	mod_real ^= mod_short;
end
endmodule
