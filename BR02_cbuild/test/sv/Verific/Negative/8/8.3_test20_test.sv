
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In SystemVerilog, assignment operators can be used inside an
// expression, provided there is no delay/timing control. This design
// uses assignment operators inside expressions with
// explicit timing control such as a=(b+=#2 1). This is an error.

module test#(parameter w = 8)
            (input [w-1:0] in1, in2,
             output reg [0:w-1] out1, out2);

    reg [w-1:0] a, b;

    always@(in1, in2, a, b)
    begin
         a = (in1 += #5 1);
         b = (in2 -= #5ns a);

         out1 = #5ns a | in2;

         out2 = #5 b ^ in1;
    end

endmodule

