
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Only integral variables are allowed in a 'constraint' block.

class B ;
    rand bit s ;
    rand string d ; // Error
    constraint c { s->d == 0 ; }
    constraint order { solve s before d ; }
endclass

