
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Random cyclic variables can only be of type bit or enumerated
// types. The following design declares a random-cyclic variable of type int.

module test ;

    class RandConstraint ;
        rand int a ;
        randc int b ; // Error : randc cannot be of type 'int'
    endclass

    RandConstraint randconst = new ;

    initial begin
        repeat(10) begin
            if (randconst.randomize() == 1)
                $display("randconst---b: %b, a: %d", randconst.b, randconst.a) ;
            if (randconst.randomize() != 1) 
                $display("Randomization failed") ;
        end
    end        

endmodule

