
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The number of loop variables must not exceed the number
// of array dimensions.

class RandConstraint ;
    rand byte A[7] ;
    int i [2] ;
    constraint C2 { foreach (A[i,j]) A[j] > 2 * j ; } // Error
endclass

