
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): According to System Verilog a dist operation cannot be
// applied to randc variables. This is an error.

class RandConstraint ;

    randc int a ;
    rand int b ;
    
    constraint check { a dist {[10:20] :/ 10, 30:=2, 40:=3} ; }

endclass

RandConstraint randconst = new ;

module test ;
    initial begin
        repeat(10) begin
            if (randconst.randomize() == 1)
                $display("randconst---b: %h, a: %d", randconst.b, randconst.a) ;
            if (randconst.randomize() != 1) 
                $display("Randomization failed") ;
        end
     end
endmodule

