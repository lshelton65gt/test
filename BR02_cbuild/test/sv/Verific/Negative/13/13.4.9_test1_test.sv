
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): 'randc' variables are not allowed in variable ordering.

class B ;
    randc byte s ;
    rand string d ; // Error
    constraint c { s->d == 0 ; }
    constraint order { solve s before d ; }
endclass

B randconst = new ;

module test ;

    initial begin
        repeat(10) begin
            if (randconst.randomize() == 1)
                $display("randconst---s: %h, d: %d", randconst.s, randconst.d) ;
            if (randconst.randomize() != 1)
                $display("Randomization failed") ;
        end
     end

endmodule

