
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Iterative constraints allow arrayed variables to be constrained
// in a parameterized manner using loop variables and indexing expressions.

class RandConstraint ;
    rand byte A[7] ;
    int i [2] ;
    constraint C1 { foreach (A[A]) A[A] inside {2, 4, 8, 16} ; } // Error: Loop variable should not have the same id as the array
    constraint C2 { foreach (A[j]) A[j] > 2 * j ; }
endclass

