
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The 'repeat' expression must evaluate to a non-negative
// value which specifies the number of times the corresponding production is
// generated. The following design has a repeat expression with a negative value.
// This is an error.

module test (input in, output bit[0:2] out) ;

    int repeat_expr = -10 ;
    initial
    begin
        randsequence () 
            PUSH_OPER  : repeat ( repeat_expr ) PUSH ;
            PUSH       : { $display ("Push called") ; } ;
        endsequence
    end
    assign out = in ;

endmodule
