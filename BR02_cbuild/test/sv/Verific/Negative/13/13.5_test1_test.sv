
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): System function 'randomize' called but, no variable is 
// declared as rand. This is an error.

class RandConstraint ;

    int a ;
    bit [15:0] b ;

endclass

class NoConstraint ;

    int a ;
    bit [15:0] b ;

endclass


RandConstraint randconst = new ;
NoConstraint noconst = new ;

module test ;

    initial begin

        repeat(10) begin

            if (randconst.randomize() == 1)
                $display("randconst---b: %h, a: %d", randconst.b, randconst.a) ;
            if (noconst.randomize() == 1 )
                $display("noconst---b:%h, a:%d", noconst.b, noconst.a) ;
            if (randconst.randomize() != 1 || noconst.randomize() != 1)
                $display("Randomization failed") ;
        end

     end        

endmodule

