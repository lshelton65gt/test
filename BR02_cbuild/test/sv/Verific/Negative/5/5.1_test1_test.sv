
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that unpacked arrays can not be used as a whole.

module test ;

    bit [7:0] vec;
    bit [7:0] vec_arr [3:0 ] ;

    initial
    begin
        vec = '1;
        vec_arr = '0;
    end

endmodule
