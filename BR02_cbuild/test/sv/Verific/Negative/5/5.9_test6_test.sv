
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that a 4-state index containing X or Z is invalid for
// associative arrays.

class check ;

   int roll ;
   string name ;

   function new( int r, string n) ;
       roll = r  ;
       name  = n ;
   endfunction

endclass

module test ;

    int a[integer] ;
    int b[string] ;
    check c[integer] ;
    check ch1 = new(1,"asdad") ;
    check ch2 = new(2,"ewe") ;
    check ch3 ;

    initial begin
       a['X] = 2 ;
       a['Z] = 'X ;
       a[3] = 10 ;

       b[""] = 12 ;
       b["yesy"] = 34 ;
 
       c[12] = ch1 ;
       c[3] = ch2 ;
       c['X] = ch3 ;
    end

endmodule

