
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that unpacked arrays can not be assigned to packed arrays.

module test (array1, array2) ;
    output logic [1:0][3:0] array1 ;
    output logic [1:0][3:0] array2 ;
    int count1 = 0 ;
    int count2 = 7 ;
    int i, j ;
    always@(array1[0][0], array2[0][0], count1, count2)
    begin
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array1[i][j] = count1 ++ ;
                    array2[i][j] = count2 -- ;
                end
    end
endmodule

module bench (output out1[1:0][3:0], output out2[1:0][3:0], input in);
    test initialization (out1, out2) ; // out1 & out2 both are unpacked arrays
endmodule

