
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assignment from an integer like d = 4'1111 allowed only with packed
// arrays. This design uses it with unpacked array. This is an error.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    byte arr[3:0];    // unpacked array of bytes

    always@(posedge clk)
    begin
        arr = 32'b11111111111111111111111111111111;    // illegal with unpacked array, but legal with packed array

        out1 = arr | in2;
        out2 = arr ^ in1;
    end

endmodule

