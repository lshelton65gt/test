
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that packed arrays can not be assigned to unpacked arrays.

module test;

    bit [3:0][31:0] bit_vec;

    int i_arr [3:0] ;

    initial
    begin
        bit_vec[3] = 789;
        bit_vec[2] = 90;
        bit_vec[1] = '1;
        bit_vec[0] = 0;

        i_arr = bit_vec; // Error: Packed array cannot be assigned to an unpacked array without explicit casting

    end

endmodule
