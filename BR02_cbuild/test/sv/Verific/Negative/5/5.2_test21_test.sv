
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The following operations can be performed on packed 
// arrays, but not on unpacked arrays. The examples provided with these rules 
// assume that A is an array.
// -- Assignment from an integer, e.g., A = 8'b11111111 ;

module test (array1, array2) ;
    output int array1 [1:0][3:0] ;
    output int array2 [1:0][3:0] ;
    //-- Assignment from an integer, e.g., A = 8'b11111111
    assign array1 = 8'b11110000 ;
    assign array2 = 8'b00001111 ;
endmodule

module bench ;
    int array3 [1:0][3:0] ;
    int array4 [1:0][3:0] ;
    task showArray ;
        input int array[1:0][3:0] ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $write("%d",  array[i][j]) ;
        end 
    endtask
    test initialization (array3, array4) ;
    initial begin
        #15
        showArray (array3) ;
        showArray (array4) ;
    end
endmodule

