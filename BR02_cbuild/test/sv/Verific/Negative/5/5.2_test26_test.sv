
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case using packed array of byte type.

module test;

    byte [7:0] mod_byte;
    assign mod_byte = "Hello";

endmodule
