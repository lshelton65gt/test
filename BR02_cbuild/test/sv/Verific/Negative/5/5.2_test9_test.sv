
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of assignment of unpacked arrays where 
// source and target array lengths do not match.

module test;

    bit [7:0] arr1[3:0];
    bit [4:0] arr2[3:0];

    bit [7:0] result;

    initial
    begin
        arr2[3] = 10;
        arr2[2] = 9;
        arr2[1] = 8;
        arr2[0] = 7;
        arr1 = arr2;

        result = arr1[3] + arr1[2] + arr1[1] + arr1[0];
    end

endmodule
