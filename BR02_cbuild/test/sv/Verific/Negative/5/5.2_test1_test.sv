
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Genvar can be declared inside a generate block. This design
// declares another genvar inside a for-generate block. This genvar is used to
// declare a packed range of a variable declaration. This is a negative test case
// as a value should be assigned to genvar inside for-generate before using it .

module test (input clk,
             input [7:0] in1,
             output reg [7:0] out1);

    genvar i;

    generate
        for(i=0; i<8; i=i+1)
        begin: for_gen_i
            genvar j;

            bit [j:0] x;

            always@(posedge clk)
            begin
                //$display("length of x = %d", $length(x, 1));
                x = in1[i];
                out1[i] = x;
            end
        end
    endgenerate

endmodule

