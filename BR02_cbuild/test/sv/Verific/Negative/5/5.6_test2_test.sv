
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses 'new' with non-dynamic array  inside
// 'for-generate' loop. This is an error.

module test ;
    genvar i ;
    generate 
    for (i=1; i<=10; i++)
    begin : for_gen
       bit b1 [i] ;
       bit b2 [i] = new [i] ;
       initial b1 = new [i] ;
    end
    endgenerate
endmodule 

