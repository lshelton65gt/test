
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A dynamic array can be assigned to a fixed-sized array
// of an equivalent type if the size of the dynamic array dimension is the 
// same as the length of the fixed-size array dimension. The following design
// checks this.

module test (in, out) ;

    parameter p = 4 ;
    localparam start = 30 ;

    input in [2 * p-1:0] ;
    output out [2 * p-1:0] ;

    int arr [] ; // Define a dynamic array, don't set the size

    generate
        // Set the size of the dynamic array inside generate
        case(p)
           1: always @(*) 
               begin
                  arr = new [1] ;
                  arr = in ; // assign the bit selected array here
               end
           3: always @(*)
              begin
                  arr = new [5] ;
                  arr = in ; // assign the bit selected array here
              end
     default: always @ (*)
              begin
                  arr = new [p] ;
                  arr = in ; // assign the bit selected array here
              end
        endcase
    endgenerate

    assign out = arr ;  // Error: array length differs for out and arr

endmodule

