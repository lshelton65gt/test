
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case using unpacked array in an expression.

module test ;

    bit mod_bit [7:0] = '1 ;

    initial
    begin
        mod_bit = mod_bit + 3 ; // Error: unpacked array used in expression
    end

endmodule
