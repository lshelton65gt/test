
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing variable slicing of an unpacked array.
// The following design has a named slicing of an upacked bit array
// assigned to an unpacked array of byte. Now, byte is signed while
// bit is unsigned. Assignement of a unsigned array to a signed one
// is erroneous.

module test ;

    bit [7:0] mod_bit [7:0] = '{8{$random}} ;
    byte mod_byte [3:0] ;

    initial
    begin
        for(int i = 7 ; i >= 3 ; i = i-4)
        begin
            mod_byte = mod_bit[i -: 4] ; // Error: mob_bit is unsigned while mod_byte is signed
        end
    end

endmodule
