
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This tests that packed arrays can only be made of the 
// scalar bit types (bit, logic, reg, wire, and the other net types) and
// recursively other packed arrays and packed structures. Here packed 
// arrays are created of 'int' data type.

module test (array1, array2) ;
    output int [1:0][3:0] array1 ; // Packed array can not be of type int or integer.
    output integer [1:0][3:0] array2 ;
    int count1 = 0 ;
    int count2 = 7 ;
    int i, j ;
    initial begin
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array1[i][j] = count1 ++ ;
                    array2[i][j] = count2 -- ;
                end
    end
endmodule

module bench ;
    int [1:0][3:0] array3 ;
    int [1:0][3:0] array4 ;
    logic out ;
    int [1:0][3:0] temp ;
    test initialization (array3, array4) ; 
    task showArray ;
        input int array[1:0][3:0] ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $display("%d  ",  array[i][j]) ;
        end 
    endtask
    initial begin
        #15
        temp = array3 ;
        $display ("\n both are equal %b ,\n  %b", array3, temp) ;
    end
endmodule

