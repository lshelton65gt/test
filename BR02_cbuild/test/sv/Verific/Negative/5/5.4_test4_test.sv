
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A slice name of an unpacked array is an unpacked array.
// The following desing has a slice name of an unpacked array assigned to a
// packed array. This is an error.

module test ;

    bit [7:0] mod_bit [3:0] = '{8'($random), 8'($random), 8'($random), 8'($random)} ;
    byte mod_byte [2:0] ;
    initial
        mod_byte[0] = mod_bit[3:1] ; // Error: unpacked type assigned to a packed type

endmodule
