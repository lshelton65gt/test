
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assignment of packed array to unpacked type is not allowed.
// Hence this is a negative test case.

module test#(parameter w = 4)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    logic [w-1:0] arr1;
    logic arr2 [w-1:0] ;

    always@(posedge clk)
    begin
        arr1 = in1 ^ in2;

        arr2 = arr1;    // Error: a packed array can not be assigned to an unpacked array

        { out1, out2 } = { arr2[0] ^ arr2[3], arr2[2] - arr2[1] };
    end

endmodule

