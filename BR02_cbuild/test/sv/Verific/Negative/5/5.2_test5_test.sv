
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that integer type can not have packed
// array dimensions declared.

module test;

    integer [-1:0] i_arr [2:0] ; // Error: packed dimension with integer type not allowed

    initial
    begin
        i_arr[2][-1] = -90;
        i_arr[1][0] = -7890;
    end

endmodule
