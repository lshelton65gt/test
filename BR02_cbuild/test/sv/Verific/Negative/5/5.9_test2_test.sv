
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Other types of arrays cannot be assigned to an associative
// array, nor can associative arrays be assigned to other types of arrays, whether
// fixed-size or dynamic. Such illegal assignment is an error.

module test ;

    typedef bit [4:1] unsign_index ;
 
    int a[unsign_index] ;
    string b[] ;
    string s ;
    initial begin
       a[-1] = 3 ;
       a[8] = 4 ;
       a[2] = 2 ;
    
       b = '{"fshf","sdhgag", "swedw"} ;
    end
    initial begin
       $display("\n Number of element in array a: %d",a.num()) ;
       $display("\n String b: %s", b) ;
       b = a ;
       $display("\n String b: %s", b) ;
    end
 
endmodule

