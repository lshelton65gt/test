
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that shortint and int type cannot have packed dimensions.

module test ;

    shortint [-2:1] shortint_arr ;

    int [1:0] int_arr ;

    initial
    begin
        shortint_arr[-2] = 10 ;
        shortint_arr[-1] = 20 ;
        shortint_arr[0] = 35 ;
        shortint_arr[1] = 89 ;

        int_arr[1] = 780 ;
        int_arr[0] = 987 ;
    end

endmodule

