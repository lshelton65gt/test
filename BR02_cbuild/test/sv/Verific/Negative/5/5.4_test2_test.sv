
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses variable slicing, slicing is allowed only
// for one single dimension. Other dimension may have single index value but not 
// slicing. This design uses slicing for more than one dimension. Hence this is a
// negative test case.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg out1[w-1:0], out2[w-1:0]) ;

    logic m [0:w-1][31:0] ;

    int x ;
    always@(posedge clk)
    begin
        m = '{16{in1, in2}} ;

        out1  = m[0][5:2] ;  // Ok: slicing for one dimension only, single index for others
        out2 = m[5:2] ; // Error: slicing on more than one dimension
    end

endmodule

