
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design tests the rules of queue. An invalid index
// (i.e., a 4-state expression with X's or Z's, or a value that lies outside 0...$+1) 
// shall cause a write operation to be ignored and a run-time warning to be
// issued. Note that writing to Q[$+1] is legal.

module test (input clk, bit [7:0] n, output bit out);
    int Q[$] ;
    always @ (posedge clk) 
    begin
        for (int i = 0; i < 10; i++)
            Q = {Q, i*n} ;

        Q[$+1] = 6 ; // Error
        if (Q[$+1] == 6 ) out = 1'b1 ;
        else out = 1'b0 ;
    end
endmodule

