
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use size and delete methods on non-dynamic arrays, it is an
// error condition.

module test (input int in[4], output int j[4]) ;

    always @(*)
    begin
       j = new [10] ;
       for (int i = 0; i < in.size; i++)
           j[i] = in[i] ;
       in.delete ;
    end
        
endmodule

