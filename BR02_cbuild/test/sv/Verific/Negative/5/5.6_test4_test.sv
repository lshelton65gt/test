
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design tests the Dynamic Array method new() providing
// negative number as size of dynamic array. This is an error

module test ;
    parameter p1 = func() ; 

    function integer func() ;
        integer arr[] ;

        arr = new[-10] ; // Error: The number of elements in the array. 
                         // Must be a non-negative integral expression.
        return arr.size ;
    endfunction
endmodule

