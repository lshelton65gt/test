
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case on assigning to an unpacked array, when 
// length of the unpacked dimensions do not match.

module test;

    bit [3:0]bit1 [0:3];
    bit [7:0]bit2 [0:3];

    assign bit2 = bit1;

    initial
    begin
        for(int i = 3; i >= 0; i++)
                bit1[i] = '1;
    end

endmodule
