
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of assignment of unpacked arrays where
// source and target have different number of unpacked dimensions.

module test;

    int i_arr1 [3:0];
    int i_arr2 [7:0];

    int result;

    initial
    begin
        i_arr1[3] = 100;
        i_arr1[2] = -90;
        i_arr1[1] = 789;
        i_arr1[0] = 89;

        i_arr2 = i_arr1;
        i_arr2[4] = 678;
        i_arr2[5] = 56;
        i_arr2[6] = 876;
        i_arr2[7] = 1234;

        result = i_arr2[7] + i_arr2[6] + i_arr2[5] + i_arr2[4] +
                 i_arr2[3] + i_arr2[2] + i_arr2[1] + i_arr2[0]; 
    end

endmodule
 
