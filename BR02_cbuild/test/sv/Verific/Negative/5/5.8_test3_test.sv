
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Multidimensional arrays can be used as arguments to function
// or task, but with exact size of each dimension. Use of incompatible size is an error.

module test #(parameter width=8)
             (input clk,
              output int b[3:1][4:1] );

    always @(posedge clk)
        b = func(b) ;

    typedef int my_type [3:1][3:1] ;
    function my_type func(output int a[3:1][3:1]) ;

       for (int i=1; i<4; i++)
          for (int j=1; j<4; j++)
              a[i][j] = i+j ;
       return a ;
    endfunction

endmodule

