
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case in assigning a packed array to an unpacked one.

module test;

bit [7:0] mod_bit1;
bit mod_bit2 [7:0];

initial
begin
	mod_bit1 = '1;
	mod_bit2 = mod_bit1;
end

endmodule 
