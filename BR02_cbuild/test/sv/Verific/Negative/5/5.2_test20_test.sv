
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Packed arrays can only be of single bit types, but
// unpacked arrays are allowed of any type. This design defines a module with a
// type parameter, the default value of which is single bit type. When 
// the module is instantiated the parameter is overridden with multi-bit type 
// like byte. This is an error and hence this is a negative test case.

module test#(parameter type pt = bit, parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    pt [w-1:0] arr;    // legal if pt is of type single bit, but the bench file overrides this with multi-bit type

    always@(posedge clk)
    begin
        arr = in1 | in2;

        out1 = arr * in2;
        out2 = arr & in1;
    end

endmodule

module bench;

    parameter w = 8;

    reg clk;
    bit [w-1:0] in1, in2;
    logic [w-1:0] out1, out2;

    test #(.pt(byte), .w(w)) t_mod_1 (clk, in1, in2, out1, out2);

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in1, in2 } = $random;

    initial
    begin
        clk = 0;
        $monitor("in1 = %d, in2 = %d, out1 = %d, out2 = %d", in1, in2, out1, out2);
        #501 $finish;
    end

endmodule
