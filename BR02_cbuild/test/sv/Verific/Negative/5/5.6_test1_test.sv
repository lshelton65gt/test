
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): It is an error to use 'new' with non-dynamic array.

module test ;

    bit b1 [10] ;
    bit b2 [10] = new [10] ;
    initial b1 = new [10] ;

endmodule 

