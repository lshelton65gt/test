
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The following operations can be performed on packed 
// arrays, but not on unpacked arrays. The examples provided with these rules 
// assume that A is an array.
//-- Treatment as an integer in an expression, e.g., (A + 3)

module test ;
    int array3 [1:0][3:0] ;
    int array4 [1:0][3:0] ;
    int temp [1:0][3:0] ;
    task showArray ;
        input int array[1:0][3:0] ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $write("%d",  array[i][j]) ;
        end 
    endtask
    mod initialization (array3, array4) ;
    initial begin 
        #15 ;
        array3 = array3 + 3 ;
        showArray (array3) ;
    end
endmodule

module mod (array1, array2) ;
    output int array1 [1:0][3:0] ;
    output int array2 [1:0][3:0] ;
    int count1 = 0 ;
    int count2 = 7 ;
    int i, j ;
    initial begin
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array1[i][j] = count1 ++ ;
                    array2[i][j] = count2 -- ;
                end
    end
endmodule

