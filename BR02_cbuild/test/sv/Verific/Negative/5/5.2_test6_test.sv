
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that unpacked arrays cannot be assigned a value by an integer.

module test;

    bit [1:0][3:0] p_arr;
    bit u_arr [1:0][3:0] ;

    initial
    begin
        p_arr = 8'b10101101;
        u_arr = 8'b01011111;
    end

endmodule
