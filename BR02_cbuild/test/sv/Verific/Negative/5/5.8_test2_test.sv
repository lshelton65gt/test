
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): If the port of a function/task is a multi-dimensional unpacked
// array, at its call we should pass unpacked array with same number of dimensions
// and size. Otherwise it is an error.

module test #(parameter width=8)
             (input clk,
              output int b[3:1]);

    always @(posedge clk)
        b = func(b) ;

    typedef int my_type [3:1][3:1] ;
    function my_type func(output int a[3:1][3:1]);

       for (int i=1; i<4; i++)
          for (int j=1; j<4; j++)
              a[i][j] = i+j ;
       return a ;
    endfunction

endmodule

