
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Packed arrays can be treated as an integer in an expression.
// However this is not allowed for unpacked arrays. This design defines a type parameter and that
// type is used to define an element. The element is treated as a whole
// in this way. This type is overridden with a type having unpacked
// dimensions. So, this is a negative test case.

module test#(parameter type pt = bit, parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    pt arr1, arr2; 

    always@(posedge clk)
    begin
        arr1 = in1 | in2;
        arr2 = arr1 + 4;    // legal only if pt does not have unpacked dimensions

        out1 = arr1 - in2;
        out2 = arr2 + in1;
    end

endmodule

module bench;

    parameter w = 8;

    reg clk;
    bit [w-1:0] in1, in2;
    logic [w-1:0] out1, out2;

    typedef bit Byte [7:0];

    test #(.pt(Byte), .w(w)) t_mod_1 (clk, in1, in2, out1, out2);

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in1, in2 } = $random;

    initial
    begin
        clk = 0;
        $monitor("in1 = %d, in2 = %d, out1 = %d, out2 = %d", in1, in2, out1, out2);
        #501 $finish;
    end

endmodule
