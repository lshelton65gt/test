
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case with packed array of type longint.

module test;

    longint [3:0] mod_long;
    assign mod_long = '1;

    initial 
       $display ("The length of 'longint [3:0]' is %d", $bits(mod_long)) ;
endmodule
