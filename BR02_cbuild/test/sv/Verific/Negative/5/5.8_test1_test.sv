
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Multidimensional array can be passed to function/task, 
// but actual and formal should have same number of unpacked dimensions. Here
// incompatible unpacked dimensions are used. This is an error.

module test #(parameter width=8)
             (input clk,
              output reg b[1:3][0:2][1:0]);

    always @(posedge clk)
    begin
        b = func(b) ;
    end

    typedef int my_type [3:1][3:1] ;
    function my_type func(output int a[3:1][3:1]) ;

       for (int i=1; i<4; i++)
          for (int j=1; j<4; j++)
              a[i][j] = i+j ;
       return a ;
    endfunction

endmodule

