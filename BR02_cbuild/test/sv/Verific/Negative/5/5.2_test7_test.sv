
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that unpacked arrays cannot be used  as an integer in an expression, 

module test ;

    logic u_arr1 [1:0][3:0], u_arr2 [1:0][3:0] ;

    typedef enum
    {
      INCR,
      ADD,
      SUB,
      MULT,
      SHIFT,
      UNDEF
    } options ;

    options op ;

    logic [7:0] result ;

    initial
    begin
        case(op)
            // Error : unpacked array used like an integer in expressions
            INCR : result = u_arr1 + 1 ;
             ADD : result = u_arr1 + u_arr2 ;
             SUB : result = u_arr1 - u_arr2 ;
            MULT : result = u_arr1 * u_arr2 ;
           SHIFT : result = u_arr1 << u_arr2 ;
           UNDEF : result = 0 ;
        endcase
    end

endmodule
