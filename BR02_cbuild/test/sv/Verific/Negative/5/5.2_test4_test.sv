
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that longint type can not have packed 
// array dimensions declared.

module test;

    longint [3:0] l_arr;

    initial
    begin
        l_arr[3] = 1234;
        l_arr[2] = 9876;
        l_arr[1] = 2345;
        l_arr[0] = -789;
    end

endmodule

