
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that slices of an array can only apply to one dimension.
// This design tries to slice a multidimensional array. This is an error.

module test;

    bit arr [7:0][31:0];

    int arr2[2:0];
    shortint arr3 [1:0];

    always @ *
    begin
         arr2 = arr[7:5];
         arr3 = arr[4:3][31:16];
    end

endmodule

