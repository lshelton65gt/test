
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): In associative array, indices can be strings or string 
// literals of any length. Other types are illegal in associative array having
// string index and shall result in a type check error.

module test ;

class check ;

   int roll ;
   string name ;

   function new( int r, string n) ;
       roll = r  ;
       name  = n ;
   endfunction

endclass
    int a[*] ;
    int b[string] ;
    check ch = new(2,"ewe") ;

    initial begin
       a['x] = 2 ;
       a['z] = 'X ;
       a[3] = 10 ;

       b[""] = 12 ;
       b["yesy"] = 34 ;
       b[ch] = 23 ; // Error: Expect string or string literal instead of class object

    end

endmodule

