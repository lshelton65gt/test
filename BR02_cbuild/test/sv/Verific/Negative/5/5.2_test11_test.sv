
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The following operations can be performed on packed
// arrays, but not on unpacked arrays. The examples provided with these rules
// assume that A is an array.
// -- Treatment as an integer in an expression, e.g., (A + 3)

module test ;
    reg array3 [1:0][3:0];
    reg [1:0][3:0] array4 ;
    reg [1:0][3:0] temp ;
    task showArray ;
        input reg [1:0][3:0] array ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $write("%d",  array[i][j]) ;
        end 
    endtask
    mod initialization (array3, array4) ;
    initial begin 
        #15 ;
        array3 = array3 + 3 ;
        showArray (array3) ;
    end
endmodule

module mod (array1, array2) ;
    output reg array1 [1:0][3:0];
    output reg [1:0][3:0] array2  ;
    int count1 = 0 ;
    int count2 = 7 ;
    int i, j ;
    initial begin
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array1[i][j] = count1 ++ ;
                    array2[i][j] = count2 -- ;
                end
    end
endmodule

