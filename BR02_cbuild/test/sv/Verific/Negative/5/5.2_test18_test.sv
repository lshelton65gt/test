
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assignment from an integer like d = 4'1111 allowed only with packed
// arrays. This design defines a type parameter and that type is used
// to define an element. The type is overridden with a type having unpacked dimensions.
// This is an error and hence this is a negative test case.

module test#(parameter type pt = bit, parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    pt arr; 

    always@(posedge clk)
    begin
        arr = 8'b11111111;    // legal only if pt does not have unpacked dimension

        out1 = arr - in2;
        out2 = arr + in1;
    end

endmodule

module bench;

    parameter w = 8;

    reg clk;
    bit [w-1:0] in1, in2;
    logic [w-1:0] out1, out2;

    typedef bit Byte [7:0];

    test #(.pt(Byte), .w(w)) t_mod_1 (clk, in1, in2, out1, out2);

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in1, in2 } = $random;

    initial
    begin
        clk = 0;
        $monitor("in1 = %d, in2 = %d, out1 = %d, out2 = %d", in1, in2, out1, out2);
        #501 $finish;
    end

endmodule
