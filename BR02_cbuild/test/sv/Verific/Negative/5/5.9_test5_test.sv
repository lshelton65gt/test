
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test class index in associative array. Indices can be objects
// of that particular class or derived from that type. Any other type is illegal and
// shall be check error.

class check ;
   int roll ;
   string name ;

   function new( int r, string n) ;
       roll = r  ;
       name  = n ;
   endfunction

endclass

module test ;

    int a[*] ;
    int c[check] ;
    check ch,ch1 ;

    initial begin
       a['x] = 2 ;
       a['z] = 'X ;
       a[3] = 10 ;

       ch = new(1,"qwer") ;
       ch1 = new(2,"fdfd") ;

       c[ch] = ch1 ;
       c["ert"] = ch1 ; // Error: use string instead of class object

    end

endmodule

