
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test that a packed array can not be assigned to an
// unpacked array.

module test (array1, array2) ;
    output reg [1:0][3:0] array1 ;
    output reg [1:0][3:0] array2 ;
    int count1 = 0 ;
    int count2 = 7 ;
    int i, j ;
    initial
    begin
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array1[i][j] = count1 ++ ;
                    array2[i][j] = count2 -- ;
                end
    end
endmodule

module bench ;
    int array3 [1:0][3:0] ;
    int array4 [1:0][3:0] ;
    logic out ;
    int temp [1:0][3:0] ;
    test initialization (array3, array4) ; // array3 & array4 both are unpacked arrays
                                          // So arguments will raise a compilation error.
    task showArray ;
        input int array[1:0][3:0] ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $display("%d  ",  array[i][j]) ;
        end 
    endtask
    initial begin
        #5 out = array3 == array4 ;
        if (out) begin 
            $display ("\n Both modules are equal") ;
            showArray(array3) ;
            $display ("   ") ;
            showArray(array4) ;
            $display ("\n\n") ;
        end
        else begin
            $display ("\n Both modules are not equal") ;
            showArray(array3) ;
            $display ("   ") ;
            showArray(array4) ;
            $display ("\n\n") ;
        end
    end
    initial begin
        //-- Reading and writing the array, e.g., A = B
        #15
        temp = array3 ;
        $display ("\n both are equal") ;
        showArray(array3) ;
        $display ("   ") ;
        showArray(temp) ;
        $display ("\n\n") ;
    end
endmodule

