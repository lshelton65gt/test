
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing unpacked array of user defined type, where the user
// defined type is a packed array of defined type.

module test ;
typedef bit [3:0] bsix;
typedef bsix mem_type[7:0];
mem_type [7:0] bar;

 bar x, y ;

 initial
 begin
     x[0] = '1 ;
     y[0] = x[1] + x[0] ;
 end 
endmodule
  
