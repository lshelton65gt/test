
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): When assigning to an unpacked array, the source and target must be
// arrays with the same number of unpacked dimensions, and the length
// of each dimension must be the same. Assignment to an unpacked array
// is carried out by assigning each element of the source unpacked array
// to the corresponding element of the target unpacked array. This design
// assigns to an unpacked array another unpacked array with different lengths
// for the unpacked dimensions. This is an error and hence this is a negative test case.

module test (input clk,
             input [7:0] in1, in2,
             output reg [7:0] out1, out2);

    logic [0:8][1:0] arr1 [3:0];    // arr1 and arr2 have different lengths for the same
    logic [0:2][4:0] arr2 [3:0];    // dimension though they have same number of elements.

    always@(posedge clk)
    begin
        for(int i=0; i<4; i++)
        begin
            arr1[0] = { 2 { in1, in2 } };
            arr1[1] = { 2 { in2, in1 } };
            arr1[2] = { 2 { in1, in1 } };
            arr1[3] = { 2 { in2, in2 } };
        end

        arr2 = arr1;    // arr1 can not be assigned to arr2, they do not match in dimension and length

        { out1, out2 } = { arr2[0] ^ arr2[3], arr2[2] - arr2[1] };
    end

endmodule

