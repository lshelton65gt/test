
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test using unpacked array as single vector.

module test (output bit out [7:0], input [7:0] in1, in2[3]);


    always @ (in1, in2)
       out  = 8'b10101010 | (in1 + in2);

endmodule
