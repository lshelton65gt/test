
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): $unpacked_dimensions take in an unpacked array as an argument
// and returns the number of unpacked dimensions. This design calls the system function
// with two arguments and hence is an error case.

module test ;

    int x [2];
    int arr [10] ;
    int unpacked_dims = 0 ;
    initial
        #10
        unpacked_dims = $unpacked_dimensions(arr, x) ; // ERROR: only one argument accepted

endmodule

