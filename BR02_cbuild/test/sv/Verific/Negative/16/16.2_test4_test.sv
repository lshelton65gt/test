
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A program can be declared inside module, but a module can
// not be declared within program.
// any module.

module test (input wire clk, b, output wire c) ;
   int shared ;
    program p1 ;
        wire a = 1'b0 ;
        wire b ;
        module check (input a, output b) ; // Error: Program cannot contain any module
            initial 
                $display("Module inside program block") ;
            assign a = b ;
        endmodule
        check(a,b) ;
    endprogram
    assign c = clk & b ;
endmodule

module bench ;
     wire a, b, c;
     reg clk ;
     initial 
         clk = 0 ;
     initial
         #15 $finish ;
     always
         #5 clk = ~clk ;
     always @ (c)
           $display("It should be error ") ;
     test t( clk, b, c) ;
endmodule

