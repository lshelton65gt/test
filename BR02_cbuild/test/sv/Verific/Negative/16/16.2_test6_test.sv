
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A 'program' block can be declared within an interface, but
// interface can not be declared within program.

module test (input wire clk, b, output wire c) ;
   int shared  = 24 ;
    program p1 (wire clk);
        interface in1(input clk) ; // Error: Program block cannot contain an interface
            string name ;
            logic flag ;
            int data ;
        endinterface
        
        in1 A ;
        initial begin
            A.name  = "hello" ;
            A.flag  = 0 ;
            A.data  = 41 ;    
            $display(" Shared variaable is also access: %d", shared) ;
            $display(" %s, %b, %d", A.name, A.flag, A.data) ;
        end
    endprogram
    assign c = clk & b ;
endmodule

module bench ;
     wire a, b, c;
     reg clk ;
     initial 
         clk = 0 ;
     initial
         #15 $finish ;
     always
         #5 clk = ~clk ;
     always @ (c)
           $display("It should be error ") ;
     test t( clk, b, c) ;
endmodule

