
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Type and data declarations within a 'program' block are 
// local to the 'program' scope and have static lifetime. 'Program' variables 
// can only be assigned using blocking assignments. Non-'program' variables 
// can only be assigned using nonblocking assignments. Using nonblocking 
// assignments with 'program' variables or blocking assignments with design
// (non-'program') variables is not allowed.

module test (input wire a, b, output wire c) ;

   int shared ;

    program p1 ;

      int a = 10 ;
      initial
          shared = a ;

    endprogram

    program p2 ;

       int b = 20 ;
       initial 
           shared = 5 ; 

    endprogram

    assign c = a & b ;

endmodule

module bench ;

     wire a, b, c;

     always @ (c)
           $display("It should be error ") ;
     test t( a, b, c) ;
endmodule

