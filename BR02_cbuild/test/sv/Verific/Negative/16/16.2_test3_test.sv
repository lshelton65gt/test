
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A program block can contain one or more initial blocks.
// It cannot contain UDPs. 

module test (input wire clk, b, output wire c) ;

   int shared ;

    program p1 ;
      wire mux, control, dataA, dataB ;
      
      primitive multiplexer (mux, control, dataA, dataB); // Error: program cannot contain any UDP
      output mux;
      input control, dataA, dataB;
      table
      // control dataA dataB     mux
             0    1     0    :    1 ;
             0    1     1    :    1 ;
             0    1     x    :    1 ;
             0    0     0    :    0 ;
             0    0     1    :    0 ;
             0    0     x    :    0 ;
             1    0     1    :    1 ;
             1    1     1    :    1 ;
             1    x     1    :    1 ;
             1    0     0    :    0 ;
             1    1     0    :    0 ;
             1    x     0    :    0 ;
             x    0     0    :    0 ;
             x    1     1    :    1 ;
      endtable
      endprimitive
      
      multiplexer(mux, control,dataA, dataB) ;

    endprogram
    
    program p2 ;
       int b = 20 ;
       initial 
           shared = 5 ; 
    endprogram

    assign c = clk & b ;

endmodule

module bench ;

     wire a, b, c;
     reg clk ;

     initial 
         clk = 0 ;
     initial
         #15 $finish ;

     always
         #5 clk = ~clk ;

     always @ (c)
           $display("It should be error ") ;

     test t( clk, b, c) ;
endmodule

