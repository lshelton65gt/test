
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Calling program tasks or functions from within design 
// modules is illegal and shall result in an error. This is because the design
// must not be aware of the testbench.

module test (input wire clk, b, output wire c) ;
   int shared  = 24 ;
    program p1 (wire clk);
        string name ;
        logic flag ;
        int data ;
        
        task T( input clk, output o) ;
            o = ~clk ;
        endtask
    endprogram

        p1 A() ;

        initial begin
            A.name  = "hello" ;
            A.flag  = 0 ;
            A.data  = 41 ;    
            $display(" Shared variable is also access: %d", shared) ;
            $display(" %s, %b, %d", A.name, A.flag, A.data) ;
        end

    assign c = clk & b ;

endmodule

module bench ;
     wire a, b, c;
     reg clk, o;

     initial begin
         clk = 0 ;
         test.p1.T(clk,o) ;
     end

     initial
         #15 $finish ;
     always
         #5 clk = ~clk ;
     always @ (c, o)
           $display("It should be error ") ;

     test t( clk, b, c) ;
endmodule

