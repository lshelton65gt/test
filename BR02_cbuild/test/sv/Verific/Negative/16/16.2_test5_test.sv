
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A module can be declared inside module, but a 'program' block
// can not be declared within another program.

module test (input wire clk, b, output wire c) ;
   int shared ;
    program p1 ;
        string a = "hello"
        program p2(input string a) ; // Error : Program block cannot contain other program blocks
           initial
               s = {s," world"} ;
        endprogram
    endprogram
    assign c = clk & b ;
endmodule

module bench ;
     wire a, b, c;
     reg clk ;
     initial 
         clk = 0 ;
     initial
         #15 $finish ;
     always
         #5 clk = ~clk ;
     always @ (c)
           $display("It should be error ") ;
     test t( clk, b, c) ;
endmodule

