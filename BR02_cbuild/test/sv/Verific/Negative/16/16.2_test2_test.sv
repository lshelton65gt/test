
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A 'program' block can contain one or more 'initial' blocks.
// It cannot contain 'always' blocks. This is an error.

module test (input wire clk, b, output wire c) ;

   int shared ;

    program p1 ;
      int a = 10 ;

      clocking cd1 @(posedge clk);
           inout data;
      endclocking 

      always @(posedge clk) // Error: Program cannot cotain any 'always' block
      begin
          if(cd1.data == 10)
              ##1 ;    
      end
    endprogram

    program p2 ;
       int b = 20 ;
       initial 
           shared = 5 ; 
    endprogram

    assign c = clk & b ;

endmodule

module bench ;

     wire a, b, c;
     reg clk ;

     initial 
         clk = 0 ;
     initial
         #15 $finish ;

     always
         #5 clk = ~clk ;

     always @ (c)
           $display("It should be error ") ;

     test t( clk, b, c) ;
endmodule

