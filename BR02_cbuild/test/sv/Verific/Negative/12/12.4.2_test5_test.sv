
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Unpacked array types are equivalent to elements having 
// identical types and shapes. Shape is defined as the number of dimensions and
// the number of elements in each dimension, not the actual range of
// the dimension.
// eg.:
//             bit [9:0]    A [0:5];
//             bit [1:10] B [6]; 
//             typedef bit [10:1] uint10;
//             uint10 C [6:1];     // A, B and C have equivalent types
//             typedef int anint [0:0];     // anint is not type equivalent to int
// According to System Verilog only equivalent types can be passed as actual to 
// formals defined by 'ref' keyword.


typedef bit [10:3] uint10 ;

function bit donothing ( ref uint10 C [6:1], ref uint10 D [6:1]) ; // Error: equivalant types but cannot be referred as they are not matching types
     return 1'b1 ;
endfunction

module test (input bit [9:0] A [0:5], bit [1:10] B [6], bit clk, output bit out) ;
    bit [9:0] A1 [0:5] ;
    bit [1:10] B1 [6] ;
    always @ (posedge clk)
    begin
        A1 = A ;
        B1 = B ;
        out = donothing(A1, B1) ;
    end
endmodule

