
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): To test the 'ref' (referance) keyword.
// Arguments passed by reference must be matched with equivalent data types.
// This design tests that.

typedef struct {
    byte msg [1000:0] ;
    bit [7:0] addr ;
    bit parity ;
} packet1;

typedef struct {
    byte msg [1000:0] ;
    bit [7:0] addr ;
    bit parity ;
} packet2 ;

function bit crc( ref packet2 pack ) ; // Error: The variables are of different types
    byte result = 8'hff ;              
    for( int j = 1; j <= 1000; j++ )
    begin
        result ^= pack.msg[j] ;
    end
    if (result < 8'h0f)
        return 1'b0 ;
    else return 1'b1 ;
endfunction

module test ( input  bit enable,
                      input  byte message[1000:0],
                      input  clk,
                      output bit valid ) ;
    packet1 pack ;
    always @ (posedge clk)
    begin
        pack.msg = message ;
        pack.addr = 8'b11001100 ;
        pack.parity = '1;
    end
    always @ (negedge clk)
    begin
        valid = 1'b0 ;
        if (enable)
        begin
            if (crc( pack))
                valid = 1'b1 ;

             else valid = 1'b0 ;
        end
        else valid = 1'b0 ;
    end
endmodule

