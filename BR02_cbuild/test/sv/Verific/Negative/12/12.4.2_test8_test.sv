
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Combining ref with any other directional qualifier like input 
// output or inout is illegal. This design checks this.

function bit donothing ( ref input bit [7:0]accept) ; // Error: Directional qualifier along with 'ref' is not allowed.
    return 1'b1 ;
endfunction

module test(input [7:0] bus, bit clk, output bit out);
    always @ (posedge clk)
        out = donothing(bus) ;
endmodule 

