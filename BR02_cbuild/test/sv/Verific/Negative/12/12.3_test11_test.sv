
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A void function can not be used inside/as an expression as
// it does not return a value. This design defines a void function and uses that
// in/as an expression. This is an error and should be detected by the tool.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [0:width-1] r1, r2;

always @(posedge clk)
begin
    out1 = n(in1, r1) - n(in2, r2);
    out2 = n(r2|r1, r1);

    out1 = r1 ^ r2;
    out2 = in1 & in2;
end

function void n(int a, output int b);
    b = ~a;
endfunction

endmodule

