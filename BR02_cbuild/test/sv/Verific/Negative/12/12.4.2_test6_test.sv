
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design tests that, only variables, not nets, can be passed 
// by reference to function. 

function bit donothing ( ref bit [7:0]accept) ;
    return 1'b1 ;
endfunction

module test(input wire [7:0] bus, bit clk, output bit out);
    always @ (posedge clk)
        out = donothing(bus) ; // Error: Net 'bus' cannot be referenced
endmodule

