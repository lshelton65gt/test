
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Combining ref with any other directional qualifier like input 
// output or inout is illegal. This design checks this.


function donothing ( input bit [7:0]accept, ref inout status) ; // refferenced variable can not be qualified
    if (status == 0) 
         status = 1'b1 ;
endfunction

module test (input bit [7:0] drive, output reg out);
    reg status = 0 ;
    always @ (drive or status)
    begin
        donothing(drive, status) ;
        out = status ;
    end
endmodule

