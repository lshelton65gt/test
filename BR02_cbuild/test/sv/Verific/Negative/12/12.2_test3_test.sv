
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The default direction of a task port is input. This
// design does not define direction for  the first two ports . First one is
// correctly treated as input port but the second one, which inherits its 
// direction from the first one, is treated as output port. This is an error
// and should be flagged out by the tool. This also checks whether the tool 
// can detect the output treatment of the second inherited port and flags 
// it as an error.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    task1(in1, out1, in2);
    task1(in2, out2, in1);
end

task task1(int a, b /* default direction is input, but used as output */, input int c);
    b = c - a & c | a;
endtask

endmodule

