
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing of default data type in a function. 'net' type
// element used in LHS in sequential area.

module test(input logic [31:0]modLog, output [31:0]modBit);

function void myfunction(a, output bit x);
    x = a;
endfunction:myfunction


initial
begin
    myfunction(modLog, modBit);
end

endmodule
