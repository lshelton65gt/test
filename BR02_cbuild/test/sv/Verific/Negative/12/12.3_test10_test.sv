
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A non-void function must return a value. This design
// defines a non-void function that does not return a value, it even does not
// assign anything to the function name. The tool should produce warning/error
// message for this.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    if (func1(in1, in2) && func1(in2, in1))
        out1 = in1 - in2;
    else
        out2 = in2 - in1;
end

function logic [width-1:0] func1(logic [width-1:0] a, b);
    bit [0:width-1] t1, t2;

    t1 = a[width/2-1:0] * b[width-1:width/2];
    t2 = a>>width/2 | b<<width/2;

    t1 = t1 ^ t2;
    t2 = t1 ^ t2;
    t1 = t1 ^ t2;
endfunction

endmodule

