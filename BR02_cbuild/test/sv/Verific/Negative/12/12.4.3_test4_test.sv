
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a function. The parameters of the function
// have default values. The default value of first parameter is the second parameter 
// itself, so it is an error to use non constant value as default value of function.

module test ;
   parameter p = 2 ;
   generate
       if( fact(.ctrl(2), .op() ) > 2)
           mod t1() ;
       else
           mod #(p) t1() ;
   endgenerate

   function integer fact(input op = ctrl, input ctrl = fact);
      integer tmp = 0 ;
      
      for( int i=0; i<ctrl; i++)
          tmp += op * i ;
      return tmp ;
   endfunction

endmodule

module mod ;
   parameter p1 = 10 ;
endmodule

