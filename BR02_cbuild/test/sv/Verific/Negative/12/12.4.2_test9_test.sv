
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Combining ref with any other directional qualifier like input 
// output or inout shall be illegal.

function bit donothing (ref bit [7:0]accept, ref output bit status) ;
    return 1'b1 ;
endfunction

module test(input [7:0] bus, bit status, bit clk, output bit out);
    always @ (posedge clk)
        out = donothing(bus, status) ;
endmodule 

