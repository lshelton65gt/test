
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The default direction of a task port is input. This design
// does not define direction for two ports and treats them as output ports. This
// is an error and should be flagged out by the tool. This also checks whether
// the tool can detect the output treatment of the second inherited port and
// flags it as an error.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    task1(r1, r2, in1, in2);
    task1(out1, out2, in2+r1, in1-r2);
end

task task1(/* default direction is input, but used as output */ int a, b, input int c, d);
begin
    a = c - d;
    b = d - c;

    a = b + a;
    b = d--;
end
endtask

endmodule

