
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses two always_comb blocks and one of the blocks 
// contains a function call. The function in turn changes the value of a global
// variable. The return value of the function also depends on the value of the global
// variable. The global variable is also modified from within the other always block.
// Since, SystemVerilog does not allow driving a variable both from always_comb and from
// any other place, it is negative test case.

module test(clk, i1, i2, o1);

parameter IN_WIDTH = 8;

input clk;
input signed [IN_WIDTH-1:0] i1, i2;
output reg o1;

reg t1, t2;
reg [3:0] t3='0;

function bit fn(int x,int y);
    int z;
    z=(t3++) + x;
    z=(t3++) - y;
    return ^(z|t3);
endfunction

always_comb
begin
        t1 = (i1 == i2);
        if (t1)
        begin
                t2 = (i1 != i2) + fn(i1, i2);
        end
        else
        begin
                t2 = (i1 == i2) + fn(i1, i2);
        end

        t2 = !t2;
end

always_comb
begin
    if (t2 && (!t1))
    begin
        o1 = (t3=((t1 || t2) == (i1 && i2)));
    end
    else
    begin
        o1 = (t3=! ((t1 && t2) != (i1 || i2)));
    end
end

endmodule

