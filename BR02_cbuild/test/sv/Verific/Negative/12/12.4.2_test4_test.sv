
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): To test the 'ref' (referance) keyword.
// Arguments passed by reference must be matched with equivalent data types.
// This design checks that.

typedef struct packed {
    bit [0:1000][0:7] msg ;
    bit [7:0] addr ;
    bit parity ;
} packet1;

typedef bit [9000:0] stream ; 

function bit crc( ref stream pack ) ; // Error: The  variables are of not of equivalent types
                                  // The size should be {1001*8 + 8 + 1} but we are providing 9000 so can not be refferenced
    byte result = 8'hff ;
    for( int j = 0; j <= 8000; j+=8 )
    begin
        result ^= pack[j+:7] ;
    end
    if (result > 8'h0f) return 1'b1 ;
    else return 1'b0 ; 
endfunction

module test ( input  bit enable,
                      input  bit [0:1000][0:7] message,
                      input  clk,
                      output bit valid ) ;
    packet1 pack ;
    always @ (posedge clk)
    begin
        pack.msg = message ;
        pack.addr = 8'b11001100 ;
        pack.parity = '1;
    end
    always @ (negedge clk)
    begin
        valid = 1'b0 ;
        if (enable)
        begin
            if (crc( pack ))
                valid = 1'b1 ;

             else valid = 1'b0 ;
        end
        else valid = 1'b0 ;
    end
endmodule

