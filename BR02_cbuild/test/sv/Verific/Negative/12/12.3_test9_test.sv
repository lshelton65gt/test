
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A non-void function must return a value. This design
// defines a non-void function and uses its name to assign a value, but
// afterwords uses 'return'(without any value) to return from the function
// without returning any value. So a non-void function is not returning a
// value. The tool should issue an error/ warning message for this.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    r1 = in1 ^ in2;
    r2 = func1(r1, in2);
    r1 = func1(r2, r1);

    out1 = r1 * r2;
    out2 = r1 | r2;
end

function int func1(int a, b);
    int r;

    r = a + b;

    func1 = r;

    r = r | a & b;

    return;
endfunction

endmodule

