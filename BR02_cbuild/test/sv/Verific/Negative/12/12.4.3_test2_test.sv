
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design defines a function having two ports. The default
// value of the first port uses the first port name itself. This is an error

module test ;
   parameter p = 2 ;
   generate
       if(fact(, .ctrl(2)) > 2)
           mod t1() ;
       else
           mod #(p) t1() ;
   endgenerate

   function integer fact(input op = create_input(.i(op)), input ctrl = 23);
       integer tmp = 0 ;

       for( int i=0; i<ctrl; i++)
          tmp += op * i ;
          
       return tmp ;
   endfunction

   function int create_input(int i = 2) ;
       create_input = (((i+2)*5)/10) ;
   endfunction

endmodule

module mod ;
    parameter p1 = 10 ;
endmodule

