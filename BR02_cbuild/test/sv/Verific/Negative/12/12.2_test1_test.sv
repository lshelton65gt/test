
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The default direction of a task port is input. This design
// does not define direction for a task port and treats the port as an output port.
// This is an error and should be flagged out by the tool.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    task1(out1, in1, in2);
    task1(out2, in2, in1);
end

task task1(/* default direction is input, but used as output */ int a, input int b, c);
begin
    a = b | c - b;
end
endtask

endmodule

