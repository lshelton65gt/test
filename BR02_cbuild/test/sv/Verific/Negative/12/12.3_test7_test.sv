
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): A void function cannot return a value. This design defines
// a void function that is returning a value. This is an error and should be detected
// by the tool. The second point is that the value is ignored.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    func1(in1, in2);
    func1(in2, in1);
    out1 = in1 - in2;
    out2 = in2 - in1;
end

function void func1(int a, b);
    return a|b;
endfunction

endmodule

