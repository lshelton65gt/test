
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Input to a module cannot be passed to another subroutine
// by reference as a change in the task/function will result in modification of
// the input variable, which is illegal.

module test (input int a) ;
    task check(ref int data );
        $display("data: %d", data) ;
    endtask

    initial begin
        check( a) ;
    end

endmodule

