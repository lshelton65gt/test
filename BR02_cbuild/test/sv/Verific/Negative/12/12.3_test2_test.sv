
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The return type of a function does not match with the
// variable used to store the return value. They not only don't match they
// are very much different in nature/type. This may be an error. This
// design checks whether it is detected as an error or not.

typedef union  { byte b; real r;  } u_type;
typedef enum   { A, B, C, D, E, G } e_type;
typedef struct { byte b; real r;  } s_type;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

e_type et;
s_type st;

always @(posedge clk)
begin
    et = func1(in1, in2);
    st = func1(in2, in1);

    out1 = A | st.b;
    out2 = A ^ st.b;
end

function u_type func1(int a, b);
    u_type ret;

    ret.b = a^b;
    ret.r = ret.b + 0.125;

    return ret;
endfunction

endmodule

