
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows using the void data type to discard
// a function's return value, which is done by casting the function to the void 
// type to avoid warning, if used with a void function, it gives error.Function 
// with return type of void cannot be used in/as an expression

module test ;
class Student ;
    string name ;
    int roll ;
    string addr ;
 
    function new(string n, int r, string a) ;
        name  = n ;
        roll = r ;
        addr = a ;
    endfunction

    function void show() ;
        $display("name = %s, roll = %d, addr = %s", name, roll, addr) ;
    endfunction

endclass

function void create(string n, int r, string a) ; 
    Student s1 ;
    s1 = new(n, r, a) ;

    if (s1.show())
        return 1 ;

endfunction


    reg clk ;
    int r ;
    string n, a ;

    initial
        clk = 1 ;

    always
        #5 clk = ~clk ;

    always @(posedge clk) begin
        r = $random ;
    end

    initial
        #20 $finish ;

    initial
       $monitor("clk = %b, a = %d, b = %d", clk, a, b) ;

    always @(*)
       void' (create("wqe",r,"err") );

endmodule

