
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design  tests only variables, not nets, can be passed 
// by reference to functions. 

function bit donothing ( ref wire [7:0]accept) ; 
    return 1'b1 ;
endfunction

module test(input [7:0] bus, bit clk, output bit out);
    always @ (posedge clk)
        out = donothing(bus) ;
endmodule 

