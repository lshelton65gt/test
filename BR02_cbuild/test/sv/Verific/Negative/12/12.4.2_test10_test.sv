
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): To test the ref (referance) keyword.
// Arguments passed by reference must be matched with equivalent data types.
// A simple typedef or type parameter override that renames a built-in or 
// user-defined type is equivalent to that built-in or user-defined type 
// within the scope of the type identifier.

typedef struct {
    byte msg [1000:0] ;
    bit [7:0] addr ;
    bit parity ;
} packet1;

typedef struct {
    byte msg [1000:0] ;
    bit [7:0] addr ;
    bit parity ;
} packet2 ;

function bit crc(ref packet2 pk ) ; // Error: The variables are not equivalent types
    byte ret_value = 8'h00 ;
    for( int j = 1; j <= 1000; j++ )
    begin
        ret_value ^= pk.msg[j] ;
    end
    if (ret_value > 8'h0f) return 1'b0 ;
    else return 1'b0 ;
endfunction

module test ( input  bit enable, bit clk,
                      input  byte message[1000:0], 
                      output bit valid ) ;
    packet1 pack ;
    always @ (negedge clk) 
    begin
        pack.msg = message ;
        pack.addr = 8'b11001100 ;
        pack.parity = '1;
    end
    always @ (posedge clk)
    begin
        valid = 1'b0 ;
        if (enable)
        begin
            if (crc( pack )==1'b1)
                valid = 1'b1 ;

             else valid = 1'b0 ;
        end
        else valid = 1'b0 ;
    end
endmodule

