
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing function returning enum type. This design attempts
// to assign int literals to the implicit variable(with the same name as the 
// function name). This is an error.

typedef enum {mem0 ,mem1, mem2, mem3, mem4} enumType;

function enumType func(input bit [3:0]funcBit);
    case(funcBit)
        4'b0001 : func = 1;
        4'b0010 : func = 2;
        4'b0100 : func = 3;
        4'b1000 : func = 4;
        default : func = 0;
    endcase
endfunction

module test(input bit [3:0]modBit, output int out);

initial
    out = func(modBit);

endmodule

