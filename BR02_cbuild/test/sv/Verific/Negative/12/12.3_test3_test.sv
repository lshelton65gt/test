
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The return type of a function does not match with the type
// of the value actually returned. They not only don't match they are very much
// different in nature/type. This may be an error this design checks whether it
// is detected as an error or not.

typedef union { byte b ; real r ;  } u_type ;
typedef enum  { A, B, C, D, E, G } e_type ;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2) ;

    u_type ut1, ut2 ;

    always @(posedge clk)
    begin
        ut1 = func1(in1, in2) ;
        ut2 = func1(in2, in1) ;

        out1 = ut1.b & ut2.b ;
        out2 = ut1.b + ut2.b ;
    end

    function u_type func1(int a, b) ;
        e_type ret ;

        ret = A ;

        return ret ;
    endfunction

endmodule

