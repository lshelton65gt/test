
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): No value is specified for data, neither by default mechanism
// nor by passing value.

module test ;
    string a ;
    function check(int j = 2, int k = 3, int data) ;
        $display("j: %d, k:%d, data: %d", j, k, data) ;
    endfunction

    initial begin
        a = "asdfgh" ;
        check( 4, 9) ;
    end

endmodule

