
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assigning unsized multi bit values with a preceding apostrophe.

module test(output [63:0] out1, out2, out3,
            output [31:0] out4, out5, out6,
            output [7:0] out7, out8, out9) ;


    assign out1 = '10,
           out2 = 'x1,
           out3 = 'z1,

           out4 = '10,
           out5 = 'x11,
           out6 = 'z1,

           out7 = '10,
           out8 = 'x01,
           out9 = 'z10;

endmodule
 
