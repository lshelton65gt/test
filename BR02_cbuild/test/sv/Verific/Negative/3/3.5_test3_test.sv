
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test of time literals. Use of illegal time literal value like -10. 

module test ;

time t1, t2, t3 ;

initial
begin
    t1 = -10fs;
    t2 = (-3.4e-03)s ;
    t3 = 3'sb111ms;
end

endmodule

