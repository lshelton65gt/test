
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assigning unsized multi bit values with a preceding apostrophe.

module test(output [63:0] out1, out2, out3,
            output [31:0] out4, out5, out6,
            output [7:0] out7, out8, out9) ;


    assign out1 = '1z0,
           out2 = 'x1z,
           out3 = '0z,

           out4 = '0x,
           out5 = 'x10,
           out6 = '0z,

           out7 = '1z0,
           out8 = 'x10,
           out9 = '0z1;

endmodule
 
