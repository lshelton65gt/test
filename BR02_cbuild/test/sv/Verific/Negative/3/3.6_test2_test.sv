
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use escape sequence in string literal for characters that are
// supported as special characters but ' is not supported as string quote.

module test;

    bit [79:0] str1;
    bit [79:0] str2;

    assign str1 = "str\ring',
           str2 = 'st\bring";

endmodule

