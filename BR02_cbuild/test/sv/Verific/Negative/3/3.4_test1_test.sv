
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing of real literals. As per LRM IEEE-1800 section 4.14:
// The expression inside the cast must be an integral value when changing the size
// or signing. 

module test(output shortreal out1, out2,
                     output real out3, out4) ;

logic [31:0] tmp ;
logic [63:0] tmp2 ;

    initial
    begin
        out1 = 3.5 ;
        tmp = 32'(out1) ;
        out2 = shortreal'(tmp) ;

        out3 = 7.3046 ;
        tmp2 = 64'(out3) ;
        out4 = real'(tmp2) ;
    end

endmodule

