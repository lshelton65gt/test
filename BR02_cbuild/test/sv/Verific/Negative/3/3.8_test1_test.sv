
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing of passing constant expression to function port.

typedef struct {
    logic [3:0]a ;
    int b ;
} myType ;

function bit [3:0] func ;
    input myType in[1:0] ;
    bit [3:0]log ;
    log = in.a ;  // Error: no index given for in
    return log ;
endfunction

module test ;

    logic [3:0]log ;
    initial
    begin
        log = func('{'{4'bz, 10}, '{4'b1, 5}}) ;
        $display("input logic = %b, output logic (should be 4'b0) = %b", func.in[1].a, log) ;
    end

endmodule

