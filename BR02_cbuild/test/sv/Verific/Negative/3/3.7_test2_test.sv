
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of array literals where nesting of
// braces do not follow the number of dimensions

module test ;

    int n1 [0:1][0:2] = '{3 {'{2{6}}}} ;
    int n2 [0:1][0:2] = '{ '{1, 2, 3}, '{4{7}}} ;

endmodule

