
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assign literal values to different type
// variables, whose types cannot be determined from the context.

module test ;

    int f ;
    typedef bit [1:2] frac ;
    
    assign f = {1, 2} ;

endmodule

