
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of structure literals where nested braces do not resemble
// the structure.

module test ;

    typedef struct {
        int int1, int2 ;
        shortreal real1;
    } st ;

    st starr[1:0] = '{ '{ 7, 6}, '{3.4, 8, 10, 5.6}} ;

endmodule

