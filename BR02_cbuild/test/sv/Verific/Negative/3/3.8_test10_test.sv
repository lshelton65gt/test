
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The struct literals are initialized half when declaring , 
// and rest within a begin-end block, this is not possible,
// s1[1] is initialized while declaring, s1[0] is initialized inside the block
// It is a error condition.

typedef struct{ string name ;
                int roll ;
                string addr ;
              } student ;

module test ;
   
    student s1[1:0][1:0] = '{ '{'{"qew",21,"wer"},'{"cvb",23,"fgfg"}} } ;

    initial begin

        s1[0] = '{ '{"ghj",45,"dfdf"},'{"asd",56,"dfd"} };

        for(int i=0 ; i<2 ; i++)
            for(int j=0 ; j<2 ; j++)
                 $display("%s  %d  %s",s1[i][j].name,s1[i][j].roll,s1[i][j].addr) ;
     end

endmodule

