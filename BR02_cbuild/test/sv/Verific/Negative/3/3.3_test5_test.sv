
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case using unsized literal value in concat.

module test(output [7:0] out, input [2:0] in1, input [2:0] in2) ;

    assign out = { in1, 'bx, in2 } ;

endmodule

