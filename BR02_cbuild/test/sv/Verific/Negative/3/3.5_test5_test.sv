
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Time literals can use either unsigned decimal integer values
// or real values. This design specifies time literals with base, like 2'b01ps.
// This is illegal and should be caught by the tool.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    time tm1 = 5'd10fs, tm2 = 8'h8ps, tm3 = 6'b100110s;

    always@(posedge clk)
    begin
        out1 = in1 + in2;
        out2 = in2 - in1;
    end

endmodule

