
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Structure literals can be specified in various ways 
// Specify using data type and assign values to it when no such data type
// actually exists in the structure. This is an error.

typedef struct{ int a ;
                int b ;
              } student ;

module test ;
   student s1 ;
   
   initial begin
      #1 s1 = '{bit: "qwer"} ;
      #1 $display("Struct student: %d, %d", s1.a, s1.b) ;
      #1 $display(s1) ;
   end

endmodule

