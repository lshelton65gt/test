
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assignment of structure literal to general variable without any cast.

module test ;

    typedef struct { int a; bit [7:0] str ;} st ;

    bit [39:0] b ;
   
    assign b = '{ 10, "C"} ;

endmodule

