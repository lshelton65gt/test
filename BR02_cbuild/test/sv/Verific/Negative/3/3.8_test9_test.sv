
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Initialization of the structure members inside structure declaration
// is not allowed.

typedef struct { int a = 5 ;
                 string b = "qwert" ;
               } check ;

module test ;
    check ch1 ;

    initial 
        $display("%d %s", ch1.a, ch1.b) ;
endmodule

