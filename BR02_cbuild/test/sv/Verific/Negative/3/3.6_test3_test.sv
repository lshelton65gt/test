
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Test of string literals. Use unpacked arrays of character
// to store string literals. The value is assigned as the initialization.

module test;

// Number of elements are different while initialization
byte str1 [0:15] = '{ "Hello World\n" };
byte str2 [0:10] = '{ "String\n" };

initial
begin
    if(str1[0] == "H")
         $display("Success");
     else
         $display("Failed");
     if(str2[0] == "S") 
         $display("Success");
     else
         $display("Failed");
end

endmodule

