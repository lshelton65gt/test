
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test case testing proper use of braces in initializing
// an array of structures.

module test(input real str_real[3:0], input longint str_longint[1:0]) ;

    typedef struct{ 
        real  var_real[1:0] ; 
        longint var_longint ;
    } mystruct ;

    mystruct str [1:0] = '{str_real[3], str_real[2], str_longint[0], str_real[1], str_real[0], str_longint[1]} ;

endmodule

