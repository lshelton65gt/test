
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog supports unsized literals using '0, '1 etc.
// This design uses '1'b0, '1'b1 etc. This is illegal and hence is a negative test case.

module test #(parameter w = 4)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    reg [w-1:0] t1;
    reg [0:w-1] t2;

    always@(posedge clk)
    begin
        t1 = '1'b1;
        t2 = '1'b0;

        out1 = in2 - in1 & t1;
        out2 = in1 - in2 ^ t2;
    end

endmodule

