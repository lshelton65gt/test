
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing whether casting can be done using the initialization methods.

typedef struct {
    int a;
    real c;
} check;   

module test(input wire [31:0] a, input real c, output check ch);

    always@(*)
        ch = check'(ch.a : a, c);

endmodule

module bench;
    check ch1;

    test t1(12, 1.0, ch1);

    always @(*)
    begin
        if(ch1.a == 0 && ch1.c == 1)
        $display("Testing Successful");
            $display(ch1) ;
    end

endmodule

