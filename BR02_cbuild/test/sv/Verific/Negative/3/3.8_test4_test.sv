
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Structure literals can be specified in various ways like
// specifying default value, or specifying value with name of the member or 
// specifying value of a particular data type when all member of that data type
// will have that value. Specify names of members and assign values to it when
// no such member or data type actually exists in the structure. --Error case

typedef struct{ int a ;
                int b ;
                string name ;
              } student ;

module test ;
   student s1 ;

   initial begin
      #1 s1 = '{bit:1, string:"qwerty"} ;
      #1 $display(s1) ;
      #1 s1 = '{a:12, b:45, n : "asdf"} ;
      #1 $display(s1) ;
   end
endmodule

