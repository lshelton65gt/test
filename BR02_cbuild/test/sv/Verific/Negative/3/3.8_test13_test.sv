
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Mix of positional and member: value syntax in structure
// literal is an error.

typedef struct{ int a ;
                int b ;
                string name ;
              } student ;

module test ;
   student s1 ;
   
   initial begin
      #1 s1 = '{3, 12, name : "wqee"} ;
      #1 $display("Struct student: %d, %d %s", s1.a, s1.b, s1.name) ;
      #1 $display(s1) ;
   end

endmodule

