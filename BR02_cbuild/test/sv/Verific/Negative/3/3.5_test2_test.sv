
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Negative test of time literals. Use illegal time unit like Fs, sec, Ms.

module test ;

time t1, t2, t3 ;

initial
begin
    t1 = 10.2Fs;
    t2 = 3.4e-03sec ;
    t3 = 7Ms;
end

endmodule

