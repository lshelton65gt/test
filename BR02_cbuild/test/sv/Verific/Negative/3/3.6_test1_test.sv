
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Use single quote instead of double quote to enclose a string literal.

module test ;

    byte str1 [6:0];
    byte str2 [7:0];

    assign str1 = 'string1',
           str2 = 'string2';

endmodule

