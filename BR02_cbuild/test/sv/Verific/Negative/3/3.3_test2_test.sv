
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Assigning unsized multi bit values with a preceding apostrophe.

module test(output [63:0] out1, out2, out3,
            output [31:0] out4, out5, out6,
            output [7:0] out7, out8, out9) ;


    assign out1 = '01,
           out2 = '1x,
           out3 = '1z,

           out4 = '01,
           out5 = '1x1,
           out6 = '1z,

           out7 = '01,
           out8 = '1x,
           out9 = '1z;

endmodule
 
