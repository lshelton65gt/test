
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Nested clocking blocks are not allowed.


module test;
   logic phi1, phi2;
   wire [8:1] cmd; 
   reg data, write, state, s ;

   clocking cd1 @(posedge phi1) ;
      default input #10 output #10 ;
      input data ;
      output write ;
      input state ; 
      clocking cd1 @(posedge phi2) ;
          default input #5 ;
          input s = state ;
      endclocking
   endclocking

endmodule 

