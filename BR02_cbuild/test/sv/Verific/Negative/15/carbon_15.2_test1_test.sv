// carbon modified to have a case that includes a clocking declaration within a
// module (by declaring the missing variables from 15.2_test1.test.sv

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): The variables inside clocking declaration are not declared
// within module. This is an error.


module test;
   logic phi1;
   wire [8:1] cmd;
   logic      data;
   logic      write;
   logic      state;

   clocking cd1 @(posedge phi1) ;
      default input #10 output #10 ;
      input data ;
      output write ;
      input state ; 
   endclocking

endmodule 

