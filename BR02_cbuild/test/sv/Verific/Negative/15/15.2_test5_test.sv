
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): clocking cannot be declared at the global level.

reg data, state ;
logic phi1 = 0 ;

clocking cd1 @(posedge phi1) ;
   default input #10 ;
   input data ;
   input state ; 
endclocking


module test;

   initial begin
      #5  phi1 = ~phi1 ;
      #20 $finish ;
    end

endmodule 

