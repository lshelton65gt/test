
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Only one default clocking can be specified in a program,
// module, or interface. This design specifies more than one default clocking.
// This is an error.

program test( input bit clk, input reg [15:0] data ) ;

    default clocking bus @(posedge clk);
        inout data;
    endclocking 
    initial begin 
        ## 5
        if ( bus.data == 10 )
           ;
        ##4 else 
           ;
    end 

    default clocking cd1 @(negedge clk);
        inout data ;
    endclocking 
endprogram 


module top;
    reg [15:0] data;
    bit clk ;

    test main(clk, data );
endmodule 

