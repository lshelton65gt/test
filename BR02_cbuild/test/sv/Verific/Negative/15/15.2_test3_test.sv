
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design tests that a 'clocking' block cannot be 
// declared inside functions.

module test;
   logic phi1 = 0 ;

   initial begin
      #5  phi1 = ~phi1 ;
       call_fun(phi1) ;
      #20 $finish ;
    end

   function void call_fun(phi1) ;
       reg data, state ;
       clocking cd1 @(posedge phi1) ;
          default input #10 ;
          input data ;
          input state ; 
       endclocking
   endfunction

endmodule 

