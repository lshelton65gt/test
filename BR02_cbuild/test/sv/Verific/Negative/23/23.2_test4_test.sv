
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design include a second file from this file using `include.
// The included file in turn includes this file. This is a circular include directive
// and should fail/be detected as an error.

`include "test1.v"

