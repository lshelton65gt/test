
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses `define to substitute values. It defines
// an integer number as the symbol. This is an error. Symbols must be identifiers.

`define 3 3

module test(input clk, input in, output reg out);

    always@(posedge clk)
        out = in & `3;

endmodule

