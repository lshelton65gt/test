
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses `define to substitute values. It defines
// symbols with a circular reference. This should be caught by the tool.

`define a `b
`define b `c
`define c `d
`define d `e
`define e `f
`define f `g
`define g `h
`define h `i
`define i `a

module test(input clk, input in, output reg out);

    always@(posedge clk)
        out = in & `a;

endmodule

