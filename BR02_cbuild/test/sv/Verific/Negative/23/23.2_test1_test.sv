
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): Testing the use of `` in `define. A `` delimits lexical tokens
// without introducing whitespace, allowing identifiers to be constructed from arguments. 

// Error: the identifier bar_suffix is not defined.
`define foo(f) f``_suffix

module test;

initial
begin
    $display("Output %d",`foo(bar));
end

endmodule

