
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a property with a multiple clock sequence.
// Both of the clocks are different signals passed through the ports to this module.

module test (input data, input clk, input clk2, output data_out) ;

    sequence seq1 ;
          (data [*0] ##3 clk) or clk ##1 !data;
    endsequence 

    sequence seq2 ;
         first_match(seq1) ; 
    endsequence 

    property header ;
         @(posedge clk)data ##1 @(posedge clk2)seq2;
    endproperty    

    assert property(header) ;

    assign data_out = data ;

endmodule

