
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines bind directives using instance name and module
// name as target. 

module bot1;
    parameter p = 10 ;
    initial
        $display("The module bot1 instantiated with parameter value %d", p) ;
endmodule

module bot2;
    parameter p = 10 ;
    initial
        $display("The module bot2 instantiated with parameter value %d", p) ;
endmodule

bind test.I1 bot1 #(3) I1() ;
bind test.I2 bot1 #(4) I3() ;
bind middle bot2 #(33) I2() ;

module test(input in, output out) ;
    middle #(4) I1() ;
    middle I2() ;
    middle #(4) I3() ;
    assign out = in + 1 ;
endmodule

module middle ;
    parameter q = 20 ;
endmodule

