
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To repeat a sequence [*min:max] notation can be used. This design
// uses this notation to repeat a sequence.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    r1 = in1 - in2;
    r2 = in2 - in1;

    out1 = r1 & ~r2 ;
    out2 = -r2 + r1;
end

assert property (@(posedge clk) (in1[0] ##1 in2[0])[* 1:3] ) ;

endmodule

