
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Fail statement of an assertion is associated with a severity
// level like fatal, error etc. This design sets the severity level to warning by
// using the $warning system task.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    assert(in1 == in2)
        $display("assertion passed");
    else
        $warning("assertion in1 == in2 failed");

    out1 = in1 | in2;
    out2 = in2;
end

endmodule

