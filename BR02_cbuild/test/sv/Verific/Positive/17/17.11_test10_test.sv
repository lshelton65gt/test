
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses $past function. $past function can be used to
// access past values of an expression. It takes three arguments - expr1, number_of_ticks,
// expr2, clocking_event. This design uses the first two of them.

module test (input data, input clk, output data_out) ;

    property header ;
         @(posedge clk) data |=> (data_out == 1) ##1 ($past(clk,3) != 0) ##1 data_out ;
    endproperty     
    
    assert property(header) ;

    assign data_out = data ;

endmodule

