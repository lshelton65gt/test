
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design specifies instance name in bind directive. This indicates
// bind directive specified instantiation should be added to only that particular instance.

module child1;
    parameter p = 10 ;
    initial
        $display("The module child instantiated with parameter value %d", p) ;
endmodule

bind test.I1 child1 #(3) I1() ;

module test ;
    middle I1() ;
    middle I2() ;
    middle I3() ;
endmodule

module middle ;
    parameter q = 20 ;
endmodule

