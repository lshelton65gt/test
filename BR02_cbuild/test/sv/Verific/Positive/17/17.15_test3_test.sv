
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses module name in bind directive. This indicates
// that instantiation specified in bind directive should be added to all instances of
// the target module.

module child1;
    parameter p = 10 ;
    initial $display("%m in child1") ;
endmodule

bind middle child1 #(3) I1() ;

module test ;
    middle I1() ;
    middle I2() ;
    middle I3() ;
endmodule

module middle ;
    parameter q = 20 ;
endmodule

