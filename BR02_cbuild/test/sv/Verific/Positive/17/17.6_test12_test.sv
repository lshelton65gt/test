
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines sequence and a property. There are
// two parameters of the sequence. Both of them has a default value with iff
// statement.

// Note: From P1800 LRM of system verilog, it is not clear that if it is supported or not, however 
// in the pre-P1800 LRM it used to be supported.
 
module test (input clk, en,
             input in1, in2,
             output reg out1, out2);

always@(posedge clk iff en)
begin
    out1 <= in1;
    out2 <= in2;
end

sequence seq1(a = in1 iff en, b = out1 iff en);
    a ##1 out1;
endsequence

property p1;
    @(posedge clk) seq1(in1, out1);
endproperty

property p2;
    @(posedge clk) seq1(in1);
endproperty

property p3;
    @(posedge clk) seq1;
endproperty

delay1: assert property(p1) else $display("assert %m failed!");
delay2: assert property(p2) else $display("assert %m failed!");
delay3: assert property(p3) else $display("assert %m failed!");

endmodule

