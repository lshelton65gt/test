
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The design uses event control statement as the pass statement
// of immediate assertion used in a combinational block.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

int count = 0 ;
reg [width-1:0] r1, r2;

always@(r1 or r2)
begin
    assert(count && 1 && r1!=r2) @(negedge clk)
    begin
        r1 = in1 * in2;
        r2 = in2 | in1;
        assert(count==2);
    end

    out1 = r1 ^ r2;
    out2 = r2 | r1;
end

endmodule

