
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The inference made is the enabling condition for a property.
// Such derivation takes place when a property is placed in an if...else block or a
// case block. The enabling condition assumed from the context is used as the antecedent 
// of the property. Here a case block is used.

module test (input data, input a, input clk, output reg data_out) ;

    property p1;
        @(posedge clk)(data_out != data);
    endproperty 

    always @(posedge clk)
    begin 
        case (a) 
            1: begin 
                  data_out <= data;
                  check: assert property (p1);
            end 
            default: data_out <= 0 ;
        endcase 
    end 

endmodule

