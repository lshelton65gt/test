
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines bind directives using instance name as target.

module bot1;
    parameter p = 10 ;
    initial
        $display("The module bot1 instantiated with parameter value %d", p) ;
endmodule

module bot2;
    parameter p = 99 ;
    initial
        $display("The module bot2 instantiated with parameter value %d", p) ;
endmodule

bind test.I1 bot1 #(3) I1() ;
bind test.I3 bot2 #(31) I2() ;

module test(input in, output int out) ;
    middle #(4) I1() ;
    middle I2() ;
    middle #(4) I3() ;
    assign out = {32{in,1'b1}} ;
    initial #100 $display("%d", out) ;
endmodule

module middle ;
    parameter q = 100 ;
endmodule

