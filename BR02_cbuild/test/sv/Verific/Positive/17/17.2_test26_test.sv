
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Pass or fail statement can be omited from an assertion.
// This design defines an assertion without the pass statement, only the fail statement is provided.

module test(output reg [4:0] out, input [4:0] in1, in2, in3);

always @(in1, in2, in3)
begin
    out = in1 + in2 + in3;
    assert(out)
    else
        $info("All three inputs are zero at time %t", $time); // fails at 75
end
endmodule

module bench;
wire [4:0] out;
reg [4:0] in1, in2, in3;

test I(out, in1, in2, in3);

initial
begin
    in1 = '1;
    in2 = 1;
    in3 = 0;
    repeat(18)
    begin
        #5;
        in1 = in1 + 1;
    end
end

endmodule

