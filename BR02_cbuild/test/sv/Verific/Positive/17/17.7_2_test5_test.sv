
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Repetition of expression is allowed using trailing *[min:max].
// The repeat count expression *[n:n] is same as *[n]. So [0:0] is same as [0], and both
// of them has no effect. This design uses *[0:0] to check whether it is equivalent to *[0].

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = in1 | in2;
end

always @(posedge clk)
begin
    out2 = in1 | in2;
end

assert property(@(posedge clk) in1[0] [* 0:0] ##1 in2[0]) ;
assert property(@(posedge clk) in1[0][* 0] ##1 in2[0] );

endmodule

