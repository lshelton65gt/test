
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a multi-clocked sequence. The clocks used
// are two different signals. This sequence in turn instantiates two other sequences.

module test (input data, input clk, input clk2, output reg data_out) ;

    sequence seq1 ;
          (data [*0] ##3 clk) or clk ##1 !data;
    endsequence 

    sequence seq2 ;
         first_match(seq1) ; 
    endsequence 

    sequence header ;
         @(posedge clk) data ##1 @(posedge clk2) seq2;
    endsequence    

    assert property(header) ;

    always@(posedge clk) data_out = data ;

endmodule

