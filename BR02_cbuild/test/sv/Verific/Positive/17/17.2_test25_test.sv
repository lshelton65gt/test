
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks immediate assertion inside initial block. 

module packet (input clk, input d) ;
    int a = 10 ;
      function int ch(int a) ;
          a = a++ ; 
          return a ;
      endfunction
      initial 
         clock_assert : assert (1) $display("value of a: %d",ch(a)) ;
                                 else disable clock_assert ;
endmodule

module test ;
    reg clk ,d;
    initial 
        clk = 1 ; 
    always 
        #5 clk = ~clk ;
    always @(posedge clk) 
        d = $random ;
    initial 
        #1001 $finish ;
    packet p(d) ; 
    initial
        $monitor("clk = %b, data = %b", clk, d) ; 
endmodule

