
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): cover statements, one is named , another one unnamed statement
// Results of coverage for a sequence shall include:
// -- Number of times attempted 
// -- Number of times matched (each attempt can generate multiple matches) 
// -- Each attempt with attemptId and time 
// -- Each match with clock step, attemptID, and time

module test (input clk, sig1, sig2, sig3, sig4) ;

    sequence s2;
        sig3 ##[1:3] sig4;
    endsequence

    sequence s1;
        @(posedge clk) sig1 ##[1:3] sig2;
    endsequence

    property p1;
        @(posedge clk) sig1 && sig2 |=> s2;
    endproperty

    a1: assert property (p1);
    a2: assert property (s1);
    c1: cover property (p1);
    cover property (s1);
endmodule

module bench ;
    reg clk ,d, sig1, sig2, sig3, sig4;
    initial
        clk = 1 ;
    always
        #5 clk = ~clk ;
    always @(posedge clk)
        d = $random ;
    initial
        #1001 $finish ;
    test t(clk, sig1, sig2, sig3, sig4) ;
    initial
        $monitor("clk = %b, sig1 = %b, sig2 = %b, sig3 = %b, sig4 = %b ", clk, d, sig1, sig2, sig3, sig4) ;
endmodule

