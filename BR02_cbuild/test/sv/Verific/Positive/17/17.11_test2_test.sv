
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines sequence and a property. The property
// uses the sequence internally.

module test (input clk,
             input in1, in2,
             output reg out1, out2);

always@(posedge clk)
begin
    out1 <= in1;
    out2 <= in2;
end

sequence seq1;
    in1 ##1 out1;
endsequence

property p1;
    @(posedge clk) seq1;
endproperty

delay1: assert property(p1) else $display("assert %m failed!");

endmodule

