
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines bind directives in $root and uses
// hierarchical instance name to add different instantiation in two instances 
// of a particular module.

module child1 ;
    parameter p = 10 ;
    initial
        $display("The module child instantiated with parameter value %d", p) ;
endmodule

bind test.I1 child1 #(3) I1() ;
bind test.I3 child1 #(5) I2() ;

module test(input in, output out) ;
    middle #(5) I1() ;
    middle #(6) I2() ;
    middle #(5) I3() ;
    assign out = in ;
endmodule

module middle ;
    parameter q = 20 ;
endmodule

