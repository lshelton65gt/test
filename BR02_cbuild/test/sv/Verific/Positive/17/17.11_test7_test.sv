
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Local variables can be declared inside property declaration.
// This design tests this.

module packet (input int data_in, input read, write, output int sum_out) ;

int tmp_out = 0;

    always @(read, data_in, write)
    begin
       if (read)
          tmp_out = tmp_out + data_in ;
       else if (write)
           sum_out = tmp_out ;
    end
endmodule

module test ;
    reg clk ;
    int data_in, sum_out;
    bit read, write ;

    initial begin
        clk = 1 ;
        data_in = 0 ;
        read = 1;
        #35 ;
        read = 0 ;
        #5 ;
        write = 1 ;
        #10 ;
        $finish ;
    end

    always 
        #5 clk = ~clk ;

    always @(posedge clk)
    begin
        data_in = data_in + 100 ;
    end

    property header ;
         int x ;
         @(posedge clk)(read, x = data_in) ##1 (read, x = x + data_in)[*3] ##1 write && (sum_out == x) ;
    endproperty     

    assert property(header) $display("sequence is correct") ;


    packet p(data_in, read, write, sum_out) ; 

    initial
        $monitor("data_in = %d, sum_out=%d write=%b", data_in, sum_out, write) ; 
endmodule

