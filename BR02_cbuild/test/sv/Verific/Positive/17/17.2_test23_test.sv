
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog allows 'iff' expression in event control. This 
// design uses event control with iff construct as pass statement of immediate assertion.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    assert(in1 == in2) @(posedge clk iff (in1[0]==in2[0]));

    out1 = in1 * in2;
    out2 = in2 + in1;
end

endmodule

