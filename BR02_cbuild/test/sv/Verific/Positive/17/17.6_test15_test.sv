
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines sequence and a property. The property
// uses the sequence internally and the sequence has a parameter with default
// value with an edge specification. The sequence is instantiated thrice, once with
// actual with an edge specification connected to the default formal parameter,
// once with actual without an edge specification and another without the actual.

// Note: From the P1800 LRM it is not clear if it's supported in System Verilog, but 
// in pre-P1800, it used to be supported.

module test (input clk,
             input in1, in2,
             output reg out1, out2);

always@(posedge clk)
begin
    out1 <= in1;
    out2 <= in2;
end

sequence seq1(a = posedge in1);
    a ##1 out1;
endsequence

property p1;
    @(posedge clk) seq1(posedge in1);
endproperty

property p2;
    @(posedge clk) seq1(in1);
endproperty

property p3;
    @(posedge clk) seq1;
endproperty

delay1: assert property(p1) else $display("assert %m failed!");
delay2: assert property(p2) else $display("assert %m failed!");
delay3: assert property(p3) else $display("assert %m failed!");

endmodule

