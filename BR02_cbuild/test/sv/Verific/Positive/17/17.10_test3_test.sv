
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses $countones function in immediate assertions.
// $countones function returns the number of ones in a bit vector expression.

module test(input clk, input [7:0] d, output reg [3:0] out) ;

    function int ch() ; 
        assert($countones(d)>1) return $countones(d) ;
                                else return 0 ;
    endfunction

    always@(posedge clk)
        out = ch() ;

endmodule

