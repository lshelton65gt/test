
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design uses a bind directive to bind a program instantiation
// to a module.

module test (input clk,
             input in1, in2,
             output reg out1, out2) ;

    wire o1, o2 ;

    always@(posedge clk)
    begin
        out1 <= in1 ;
        out2 <= in2 ;
    end

endmodule
module bench1 ;
    bit clk, in1, in2, out1, out2 ;
    test T1 (clk, in1, in2, out1, out2) ;
    test T2 (clk, in1, in2, out111, out211) ;
    bit out11, out22 ;
    prog t1 (clk, in1, in2, ou11, out22) ;
endmodule

program prog (input clk, in1, in2, output reg out1, out2) ;

    sequence seq1(a, b = out1) ;
      a ##3 b ##2 a ;
    endsequence

    property p1 ;
        @(posedge clk) seq1(in1, out1) ;
    endproperty

    property p2 ;
        @(posedge clk) seq1(in1) ;
    endproperty

    delay1: assert property(p1) else $display("assert %m failed!") ;
    delay2: assert property(p2) else $display("assert %m failed!") ;

endprogram

bind test : bench1.T1, bench1.T2 prog T1(clk,in1,in2, o1, o2) ;

