
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The inference made is the enabling condition for a property.
// Such derivation takes place when a property is placed in an if...else block or a
// case block. The enabling condition assumed from the context is used as the antecedent 
// of the property. Here an if...else block is used.

module test (input data, a, input clk, output reg data_out) ;

    property check;
        @(posedge clk) data_out != data ;
    endproperty 

    always @(posedge clk) begin 
        if (a) begin 
            data_out <= data;
            ch1: assert property (check);
        end 
    end 

endmodule

