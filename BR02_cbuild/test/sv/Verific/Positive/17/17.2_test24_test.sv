
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S):  This design checks immediate assertions in function 

module packet(input clk, input d) ;
      function int ch() ; 
          assert (clk == d) return 1 ;
                            else return 0 ;
      endfunction
      initial 
         assert (ch()) $display("this function called") ;
                       else $display("function not called") ;
endmodule

module test ;
    reg clk ,d;

    initial 
        clk = 1 ; 
    always 
        #5 clk = ~clk ;
    always @(posedge clk) 
        d = $random ;
    initial 
        #1001 $finish ;
    packet p( clk, d) ; 

    initial
        $monitor("clk = %b, data = %b", clk, d) ; 
endmodule

