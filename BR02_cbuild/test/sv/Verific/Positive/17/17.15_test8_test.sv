
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines bind directives inside module and uses
// hierarchical instance name to specify where to add instantiations.

module child1;
    parameter p = 10 ;
    initial
        $display("The module child1 instantiated with parameter value %d", p) ;
endmodule

module child2;
    parameter p = 100 ;
    initial
        $display("The module child2 instantiated with parameter value %d", p) ;
endmodule

module test(input in, output out) ;
    middle #(4) I1() ;
    middle I2() ;
    middle #(4) I3() ;
    child2 C() ;
    assign out = in ;
    // Bind statements within module
    bind I1 child1 #(3) I1() ;
    bind I2 child1 #(4) I3() ;
    bind middle  child2 #(300) I2() ;
endmodule

module middle ;
    parameter q = 20 ;
endmodule

