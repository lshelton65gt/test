
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines sequence and a property. The property
// uses the sequence internally and the sequence has a parameter with default
// value. The sequence is instantiated twice, once with actual connected to the
// default formal parameter and once without the actual.

module test (input clk,
             input in1, in2,
             output reg out1, out2);

always@(posedge clk)
begin
    out1 <= in1;
    out2 <= in2;
end

delay1: assert property(p1) else $display("assert %m failed!");
delay2: assert property(p2) else $display("assert %m failed!");

sequence seq1(a = in1);
    a ##1 out1;
endsequence

property p1;
    @(posedge clk) seq1(in1);
endproperty

property p2;
    @(posedge clk) seq1;
endproperty

endmodule

