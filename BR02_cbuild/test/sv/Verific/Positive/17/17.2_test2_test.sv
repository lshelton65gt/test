
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If the expression of an assertion evaluates to X, Z or 0 then
// the assertion fails. This design checks whether evaluation to
// X means fail.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = in1 + in2;

    assert ('x)
        $display("x does not fail an assert!");
    else
        $display("x does fail an assert.");

    out2 = in2 ^ in1;
end

endmodule

