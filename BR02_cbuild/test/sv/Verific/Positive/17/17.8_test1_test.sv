
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design assigns local variable 'y' of sequence 's1' in
// one threads of operator 'and' and then it is accessed outside the operands. It is legal.
// Note: IEEE LRM shows these type of examples in section 17.8.

`define true 1

module packet (input data,input clk,output data_out) ;

    sequence s1;
        int x,y;

        ((data,  x = data, y = data) 
             and (clk , x = data ) ##0 (clk==x)) ##1 (y==data);
    endsequence 

    property header ;
         @(posedge clk)data ##1 data ##1 data ##1 s1 ;
    endproperty     

    assert property(header) $display(" Call of sequence") ;
endmodule

module test ;
    reg clk ,d;
    wire data_out ;

    initial
        clk = 1 ;

    always 
        #5 clk = ~clk ;

    always @(posedge clk)
        d = $random ;

    initial 
        #1001 $finish ;

    packet p(d, clk,data_out) ; 

    initial
        $monitor("clk = %b, data = %b, data_out = %b", clk, d, data_out) ; 
endmodule

