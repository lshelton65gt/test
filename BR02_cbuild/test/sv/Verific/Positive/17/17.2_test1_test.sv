
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows constant/always true/false assert expressions.
// This design defines an assertion and uses various type of constant, always true assert
// expression.

`define ONE 1

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    assert(1 && `ONE && 2 && width/4) ; 

    out1 = in1 + in2;
    out2 = in2 * in1;

    assert(out1[0] | ~out1[0]);
    assert(~out2[0] ^ out2[0]);
end

endmodule

