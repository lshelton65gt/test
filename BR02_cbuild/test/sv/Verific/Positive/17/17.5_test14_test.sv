
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses $onehot function. $onehot function can be
// used to check whether only one bit of the given expression is high (1). In that
// case it returns 1, otherwise it returns 0.

module test (input [7:0] data, input clk, output data_out) ;

    sequence seq1 ;
         $onehot(data) ;
    endsequence 

    property header ;
         @(posedge clk) seq1 ;
    endproperty     

    assert property(header) ;

    assign data_out = data ;

endmodule

