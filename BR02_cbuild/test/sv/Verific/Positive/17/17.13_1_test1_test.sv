
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an concurrent assertion. Concurrent
// assertions are a property that must always be true throughout the simulation
// cycle. This is used in concurrent area and the property is declared inside
// the module scope.

module test (input clk,
             input [3:0] in1, in2,
             output reg [0:3] out1, out2);

reg check_now;
property p1;
    int i;
    @(posedge clk) (check_now, i = in1) |-> ##5 (out1 == i+1);
endproperty

always@(posedge clk)
begin
    check_now = 1;
    tsk(in2, out1);
    tsk(in1, out2);
    check_now = 0;
end

in_val: assert property(p1) else $display("assert %m failed!");

task tsk (input reg [0:3] in, output reg [3:0] out);
    reg [3:0] in_tmp ;
    in_tmp = in ;
    #5 in_tmp += 1;
    out = 4'(in_tmp + 1);
endtask

endmodule

