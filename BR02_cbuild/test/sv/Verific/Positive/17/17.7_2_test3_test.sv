
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Repetition of expression is allowed using trailing [*n] and
// [*min:max] expressions, n should be literal or constant expression. This design uses
// [*min:max] notation to repeat an expression for a number of times.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

logic [width-1:0] t1, t2;
logic a, b, c;

function [31 : 0]const_func(reg [31 : 0] n);
    const_func = n;
endfunction

always @(posedge clk)
begin
    a = in1[width-1];
    b = in2[width-1];
    c = in1[width-1]|in2[width-1];

    t1 = in1<<width/2 | in2>>width/2;
    t2 = { in1[width/2:width-1], in2[0:width/2-1] };

    out1 = t1 | t2;
    out2 = (out1==0)?(t2-t1):(t1+t2);
end

assert property(@(posedge clk) a[* 1:const_func(width/2)]##1 (a##1 b)[* width/2]##1 c[* 1:const_func(2)] ) ; 

endmodule

