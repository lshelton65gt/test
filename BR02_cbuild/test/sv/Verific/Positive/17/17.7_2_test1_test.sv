
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses [* min:max] repetition in defining sequence
// expression. 

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    r1 = in1 & in2;
    r2 = in2 & in1;

    out1 = r1 | r2;
    out2 = r1 ^ r2;
end

property p1 ;
    @(posedge clk) (in1[width-1][* 0:2] ##1 in2[0][* 0:3] ##1 in1[0]) ;
endproperty

assert property(p1) ;

endmodule

