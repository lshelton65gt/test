
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Delay can be used to concatenate two sequences. This design uses
// ##n notation delay to concatenate sequences.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = in1 | in2;
    out2 = in2 + in1;
end

assert property(@(posedge clk) in1[0] ##2 in2[0] ##3 in1[0]) ;

endmodule

