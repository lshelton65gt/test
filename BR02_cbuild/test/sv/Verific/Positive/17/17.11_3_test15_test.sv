
module test (input clk, input b, input c);

    property check_write;
       ##1 b |-> ##[2:10] c;
    endproperty

    property check_write2;
       check_write;    
    endproperty

   always@(posedge clk)
   begin
    label: assert property(check_write2) $display("The values are b = %b, and c = %b", b,c) ;
   end

endmodule

