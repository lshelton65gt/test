
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an concurrent assertion. Concurrent
// assertions are a property that must always be true throughout the simulation
// cycle. This is used in concurrent area and the sequence is declared in the
// $root scope.

sequence s1(reset, load, finish);
    reset ##5 load ##[5:$] finish;
endsequence

module test (input clk, reset, load, finish, data, output reg res, done);

int i = 0;
reg [31:0] r = 0;

always@(posedge clk or negedge reset or posedge load or posedge finish or posedge data)
begin
    done = 0;

    if (!reset)
    begin
        res = 0;
        #5 done = 1;
    end
    else
    begin
        if (load)
        begin
            r <<= 1;
            r[i++] = data;
            #5 done = 1;
        end
        else
        begin
            if (finish)
            begin
                res = ^r;
                i = 0;
                r = 0;
                #5 done = 1;
            end
            else
                res = data;
        end
    end

end

cover property(@(posedge clk) s1(reset, load, finish)) $display("protocol error!");

endmodule

