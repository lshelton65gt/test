
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses first_match operator. The first_match operator
// matches the first of possibly multiple matches of an evaluation attempt of its
// operand sequence.

module test (input data, input clk, output data_out) ;

    sequence seq1 ;
          (data [*0] ##3 clk) or clk ##1 !data;
    endsequence 

    sequence seq2 ;
         first_match(seq1) ; 
    endsequence 

    property header ;
         @(posedge clk)data ##1 seq2;
    endproperty     

    assert property(header) ;

    assign data_out = |data ;

endmodule

