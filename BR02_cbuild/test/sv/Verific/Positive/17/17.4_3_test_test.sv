module test (input clk, input int in, output out) ;

        int arr[] ;
        int sum_of_nos = 0 ;
        int pqr[] ;
        int int_default ;

     initial 
        begin
        pqr = new[10] ; 
        for (int i = 0; i < pqr.size; i++) 
            pqr[i] = i + 1 ;
        end

     assert property( @(posedge clk) ##1 (++sum_of_nos) ) ; // Error: for using side effect operator '++' in concurrent assertion

endmodule

