
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $isunknown (<expression>) returns true if any bit of the expression
// is `x'. This is equivalent to ^<expression> === 'bx. Here, it returns false.

module test (input [7:0] data, input clk, output [7:0] data_out) ;

    sequence seq1 ;
         $isunknown(data) ;
    endsequence 

    property header ;
         @(posedge clk) seq1 ;
    endproperty     

    assert property(header) ;

    assign data_out = data ;

endmodule

