
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To access a local variable of a sub-sequence, the local 
// variable must be declared and passed to the instantiated sub-sequence
// through an argument. 

module packet (input data,input clk,output data_out) ;

    sequence seq2(v2) ;
          v2 ##1 !v2 ; 
    endsequence 

    sequence seq1 ;
         reg v1 ;
         (data ##1 !data, v1 = data) ##1 (data_out == v1) ##1 seq2(v1) ;
    endsequence 

    property header ;
         @(posedge clk)data ##1 data ##1 data ##1  seq1;
    endproperty     

    assert property(header) $display(" Call of sequence") ;
endmodule

module test ;
    reg clk ,d;
    wire data_out ;

    initial
        clk = 1 ;

    always 
        #5 clk = ~clk ;

    always @(posedge clk)
        d = $random ;

    initial 
        #1001 $finish ;

    packet p(d, clk,data_out) ; 

    initial
        $monitor("clk = %b, data = %b, data_out = %b", clk, d, data_out) ; 
endmodule

