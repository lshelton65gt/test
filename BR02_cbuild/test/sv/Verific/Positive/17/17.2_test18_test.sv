
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Pass or fail statement of an assertion can trigger another
// event. This design triggers an event from within the fail statement of an assertion.

module test(input int a, b,
                         output int count1, count2);

event myevent;
initial 
begin
    count1 = 0;
    count2 = 0;
end

always @(a, b)
begin
    assert(a > b)
        count1 += 1;
    else
        ->myevent;
end

always @(myevent)
    count2++;

endmodule

module bench;
int a, b;
int count1, count2;

test I(a, b, count1, count2);

initial
begin
    repeat(20)
    begin
       #5;
        a = $random;
        b = $random;
    end
    $display("a greater than b %d times", count1);
    $display("a less than b %d times", count2);
end

endmodule

