
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A label before the assert statement creates a named block
// around the assertion statement. This name can be printed with
// %m in $display. This design uses a label before an assert 
// statement and uses that label in $display with %m, and disables
// that assert using the label name.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    for(int i=0; i<width; i++)
    begin
        r1 = in2[i] ^ in1[i];
        r2 = in1[i] & in2[i];
        assert_r1_equal_to_r2: assert(r1 == r2)
                                     $display("%m succeeded"); 
                                 else 
                                     $display("%m failed");
        if (in1[i] == in2[i])
            disable assert_r1_equal_to_r2 ;
    end
end

endmodule

