
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines bind directives to add instantiations 
// in two instances of a module.

module child1;
    parameter p = 10 ;
    initial
        $display("The module child1 instantiated with parameter value %d", p) ;
endmodule

module child2;
    parameter p = 19 ;
    initial
        $display("The module child2 instantiated with parameter value %d", p) ;
endmodule

bind test.I1 child1 #(3) I1() ;
bind test.I3 child2 #(3) I2() ;

module test(input int in, output out) ;
    middle I1() ;
    middle I2() ;
    middle I3() ;
    assign out = $onehot(in) ;
endmodule

module middle ;
    parameter q = 20 ;
endmodule

