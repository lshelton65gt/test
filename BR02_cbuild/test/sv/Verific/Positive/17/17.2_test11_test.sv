
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A label can be specified before an assert statement. This name of the
// label identifier can be printed with %m in $display statement of
// the corresponding assertion. This design defines some labeled
// assertions and prints the label name with %m in $display.

module test 
       #(parameter int p = 4)
       (output reg [p -1 :0] out1, out2, out3,
        input [p -1 :0] in1, in2, in3);

always @(in1, in2, in3)
begin
    div_1 : assert(in2)
                out1 = in1 / in2;
            else
                $fatal(2, "%m failed at time %t. Division by zero.", $time);

    div_2 : assert(in3)
                out2 = in2 / in3;
            else    
                $fatal(2, "%m failed at time %t. Division by zero.", $time);

    div_3 : assert(in1)
                out3 = in3 / in1;
            else
                $fatal(2, "%m failed at time %t. Division by zero.", $time);

end

endmodule

module bench;
parameter p = 4;

wire [p-1:0] out1, out2, out3;
bit [p-1:0] in1, in2, in3;

test I(out1, out2, out3, in1, in2, in3);

initial
begin
    in1 = 1; in2 = 2; in3 = 4;
    repeat(20)
    begin
        #5 ;
        in1++;
        in2++;
        in3++;
    end
    #100 $finish;
end
endmodule

