
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Fail or pass statement can be used to trigger an event to
// activate a block on other place. This design uses this to 
// trigger events when an assertion passed or failed.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

event p_ev, f_ev;

always @(posedge clk)
begin
    assert(in1 == in2)
        ->p_ev;
    else
        ->f_ev;

    out1 = in1 + in2;
    out2 = 2*in1;
end

always@(p_ev)
    $display("assertion passed");

always@(f_ev)
    $display("assertion failed");

endmodule

