
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): property with named sequences at different clocks. In this case,
// if s1 contains a clock, then it must be identical to (posedge clk1). Similarly, if s2
// contains a clock, it must be identical to (posedge clk2).

module test (input data, a, b, input clk, input clk2, output data_out) ;

    sequence seq1 ;
        @(posedge clk) a ##1 data ;
    endsequence 

    sequence seq2 ;
        @(posedge clk2) b ##3  a ;        
    endsequence 

    property header ;
        @(posedge clk) seq1 |=> @(posedge clk2) seq2 ;
    endproperty    

    assert property(header) ;

    assign data_out = data ;

endmodule

