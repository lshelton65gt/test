
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an immediate assertion. Immediate
// assertions are a property that must be true when the assert statement is
// evaluated. This is used inside sequential blocks.

module test #(parameter word_size = 4)
             (input signed [word_size -1:0] in1, in2,
              output reg [word_size -1:0] out);

always @(in1, in2)
begin
    assert(in2)
        out = in1 / in2;
    else
        $error("Division by zero or x");
end

endmodule

