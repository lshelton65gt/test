
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an immediate assertion. Immediate assertions
// use a property that must be true when the assert statement is being evaluated.

module test #(parameter word_size = 4)
                         (input [word_size -1:0] in1,
                         input [word_size -1:0] in2, 
                         output reg [word_size -1:0] out);

always @(in1, in2)
begin
    assert(in2)
        out = in1 / in2;
    else
        $error("Division by zero or x");
end

endmodule

module bench;
parameter p = 4;
reg [p -1:0] in1, in2;
wire [p -1:0] out;

test I(in1, in2, out);

initial
begin
    in1 = 1;
    in2 = 1;
    repeat(20)
    begin
        #5 in1 = in1 + 4;
           in2++;
    end
end

initial
    $monitor($time,,,"in1 = %b, in2 = %b, out = %b", in1, in2, out);

endmodule

