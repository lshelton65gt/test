
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Using x, z or 0 as the condition for an assert statement leads to failure.
// This design uses assert statement with x and z expression, so the assert should fail.

module test;

reg a;

    initial 
    begin
    #1;
    a = 1'bx;
    #1;
    a = 1'bz;
    end

    always @ (a)
    begin
        assert (a) 
            $display ("Fails : this should not be displayed");
        else 
            $warning("Assert fails for X or Z. Correct result.");
    end
endmodule

