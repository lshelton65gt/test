
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines bind directive inside module to add
// parameterized instantiations to target instances.

module bot1;
    parameter p = 10 ;
    initial
        $display("The module bot1 instantiated with parameter value %d", p) ;
endmodule

module bot2;
    parameter p = 100 ;
    initial
        $display("The module bot2 instantiated with parameter value %d", p) ;
endmodule

module test(input in, output out) ;
    bind test.I1 bot1 #(3) I4() ;
    bind test.I2 bot1 #(3) I5() ;

    middle I1() ;
    middle I2() ;
    middle I3() ;
endmodule

module middle ;
    parameter q = 90 ;
endmodule

