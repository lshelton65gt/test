
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A number of steps can be skipped in  assertions, using
// various notations, like using always true expression in sequence. This design uses
// always true expression in sequence to skip some steps.

`define true 1

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

    always @(posedge clk)
    begin
        out1 = in1 * in2;
        out2 = in2 ^ in1;
    end

    assert property (@(posedge clk) in1[0]##1 `true ##1 in2[0] ##1 always_true(1) ##1 in1[0]);

    function int always_true(int i);
        return i;
    endfunction

endmodule

