
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines sequence and a property. There are
// two parameters of the sequence. The second one of them has a default value.
// In one sequence instance two actuals are passed to overwrite default formal 
// value, in another instance one argument is passed to use default value.

module test (input clk,
             input in1, in2,
             output reg out1, out2);

always@(posedge clk)
begin
    out1 <= in1;
    out2 <= in2;
end

sequence seq1(a, b = out1);
  a ##3 b ##2 a ;
endsequence

property p1;
    @(posedge clk) seq1(in1, out1);
endproperty

property p2;
    @(posedge clk) seq1(in1);
endproperty

delay1: assert property(p1) else $display("assert %m failed!");
delay2: assert property(p2) else $display("assert %m failed!");

endmodule

