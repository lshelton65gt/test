
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines sequence and a property. The parameter
// of the sequence has default value, the value is a list of events. It is used
// twice, once with no actuals and another time with the same actual as the
// default value. So, both of them should behave similarly.

// Note : This construct used to be supported in pre-P1800 System Verilog LRM, however 
// from P1800 System Verilog LRM it is not clear if it's supported or not.

module test (input clk, reset,
             input in1, in2,
             output reg out1, out2);

always@(posedge clk)
begin
    out1 <= in1;
    out2 <= in2;
end

sequence seq1(a = posedge clk or negedge reset);
    a ##1 out1;
endsequence

property p1;
    @(posedge clk) seq1();
endproperty

property p2;
    @(posedge clk) seq1(posedge clk or negedge reset);
endproperty

delay1: assert property(p1) else $display("assert %m failed!");
delay2: assert property(p2) else $display("assert %m failed!");

endmodule

