
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a property that uses the $fell function.
// $fell returns true if the value of the given expression changed to 0.

module test (input data, input clk, output data_out) ;

    sequence seq1 ;
         data ##5 $fell(clk); 
    endsequence 

    property header ;
         @(posedge clk) seq1[*3] ;
    endproperty
    
    assert property(header) ;

    assign data_out = data ;

endmodule

