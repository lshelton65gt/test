
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Using 0 as the repetition number, an empty sequence results, 
// An empty sequence is not illegal but it does not match over any positive number
// of clock ticks. 

module test (input data, input clk, output data_out) ;

    sequence seq1 ;
          data [*0] ##3 clk ;
    endsequence 

    sequence seq2 ;
          clk ##1 seq1 ##2 !data ; 
    endsequence 

    property header ;
         @(posedge clk) data ##1 seq1 ##1 seq2;
    endproperty     

    assert property(header) ;

    assign data_out = data ;

endmodule

