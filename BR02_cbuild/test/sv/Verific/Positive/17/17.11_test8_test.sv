
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a property that uses the $stable function.
// $stable returns true if the value of the given expression did not change.

module test (input data, input clk, output data_out) ;

    property header ;
         @(posedge clk) $stable(data)[*10];
    endproperty     

    assert property(header) ;

    assign data_out = ~data ;

endmodule

