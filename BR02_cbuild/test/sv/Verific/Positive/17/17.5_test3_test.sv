
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A delay can be used to concatenate two sequences. A range expression
// ##[min:max] can be used in delay. This design uses [0:0] and (0) delay indication to 
// actually introduce no delay between sequences, because (0) or [0:0] should have no effect.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    r1 = in1 - in2;
    r2 = in2 - in1;

    out1 = r1 & ~r2 ;
    out2 = (-r2) + r1;
end

assert property(@(posedge clk) in1 ##0 in2 ##[0:0] in1) ;
assert property(@(posedge clk) in1 ##(width-1) in2 ##(width-1) in1);

endmodule

