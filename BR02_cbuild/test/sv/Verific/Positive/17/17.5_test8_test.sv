
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a property with a multiple clock implications.
// It uses named sequence instance from within the property to use other sequence in the
// check. The two clocks used are different and are passed to the module as ports.

module test (input data, input clk, input clk2, output reg data_out) ;

    sequence seq1 ;
          (data [*1] ##3 clk) or clk ##1 !data;
    endsequence 

    property header ;
         @(posedge clk) data ##1 seq1 |-> @(posedge clk2) data ;
    endproperty    

    assert property(header) ;

    always@(posedge clk) data_out = data ;

endmodule

