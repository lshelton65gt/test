
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines sequence and a property. The parameter
// of the sequence has default value, the value is a list of events with iff
// statement.

// Note: It is not clear from the P1800 system Verilog LRM if this construct is 
// supported or not, however in the pre-P1800 LRM it used to be supported.

module test (input clk, en, reset,
             input in1, in2,
             output reg out1, out2);

always@(posedge clk iff en or negedge reset)
begin
    if (!reset)
    begin
        out1 <= 0;
        out2 <= 0;
    end
    else if (en)
    begin
        out1 <= in1;
        out2 <= in2;
    end
end

property p1(in2 = in1);
    @ (posedge in2) seq1(in2);
endproperty

delay1: assert property(p1) else $display("assert %m failed!");

sequence seq1(a = posedge clk iff en or negedge reset);
     a ##1 out1;
endsequence

endmodule

