
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines sequence and a property. The parameter
// of the sequence has default value, the value is a list of events with iff statement.

// Note: From the P1800 System Verilog LRM it is not clear if it is supported or not,
// however in pre-P1800 LRM, it used to supported.

module test (input clk, en, reset,
             input in1, in2,
             output reg out1, out2);

always@(posedge clk)
begin
    out1 <= in1;
    out2 <= in2;
end

sequence seq1(a = posedge clk iff en or negedge reset);
    a ##1 out1;
endsequence

property p1;
    @(posedge clk) seq1(in1);
endproperty

property p2;
    @(posedge clk) seq1();
endproperty

delay1: assert property(p1) else $display("assert %m failed!");
delay2: assert property(p2) else $display("assert %m failed!");

endmodule

