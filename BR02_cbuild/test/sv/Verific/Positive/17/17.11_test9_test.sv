
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses $past function. $past can be used to access
// past values of an expression.

module test (input data, input clk, output data_out) ;

    property header ;
         @(posedge clk) $past(data) |-> $fell(clk) ;
    endproperty
    
    assert property(header) ;

    assign data_out = ^ data ;

endmodule

