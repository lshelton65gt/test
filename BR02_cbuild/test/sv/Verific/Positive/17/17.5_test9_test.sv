
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): property with implication, where antecedent and consequent are named 
// multi-clocked sequences 

module test (input data, a, b, input clk, input clk2, output reg data_out) ;

    sequence seq1 ;
        a ##1 @(posedge clk2) data ;
    endsequence 

    sequence seq2 ;
        b ##2 @(posedge clk2) a ;        
    endsequence 

    property header ;
        @(posedge clk) seq1 |-> seq2 ;
    endproperty    

    always @(posedge clk) begin
        assert property(header) ;
        data_out = ~data ;
    end

endmodule

