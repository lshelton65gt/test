
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Fail statement of an assertion is associated with a severity
// level like fatal, error etc. This design uses such a system call(the $error system task) in the fail statement
// of the assert.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    assert(in1 == in2)
        $display("assertion passed");
    else
        $error("assertion in1 == in2 failed");

    out1 = in1 ^ in2;
    out2 = in1 - in2;
end

endmodule

