
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks concurrents assertions triggered by a property.

module packet (input data,input clk) ;
    int totalbits ;
    initial 
        totalbits = 0 ;
    property header ;
        @(clk) data ##1 data ##1 data |-> totalbits ;
    endproperty

    calculate: assert property (header) $display("If part") ;
               else $display("Else part") ;
endmodule

module test ;
    reg clk ,d;
    initial 
        clk = 1 ; 
    always 
        #5 clk = ~clk ;
    always @(posedge clk) 
        d = $random ;
    initial 
        #1001 $finish ;
    packet p(d, clk) ; 
    initial
        $monitor("clk = %b, data = %b, ", clk, d) ; 
endmodule

