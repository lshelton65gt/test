
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines sequence and a property. The parameter
// of the sequence has default value which is 'or' of two signals. The property
// is instantiated thrice. Once with 'or' two signals, once with a single signal
// and another time without any actual.

module test (input clk, reset,
             input in1, in2,
             output reg out1, out2);

always@(posedge clk)
begin
    out1 <= in1;
    out2 <= in2;
end

sequence seq1(a = clk or reset);
    a ##1 out1;
endsequence

property p1;
    @(posedge clk) seq1(in1 or in2);
endproperty

property p2;
    @(posedge clk) seq1(in1);
endproperty

property p3;
    @(posedge clk) seq1();
endproperty

delay1: assert property(p1) else $display("assert %m failed!");
delay2: assert property(p2) else $display("assert %m failed!");
delay3: assert property(p3) else $display("assert %m failed!");

endmodule

