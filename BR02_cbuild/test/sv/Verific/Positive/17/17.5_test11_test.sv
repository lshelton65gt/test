
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a property that uses the $rose function.
// $rose returns true if the value of the given expression changed to 1.

module test (input data, input clk, output data_out) ;

    sequence seq1 ;
          (data [*1] ##3 clk) or clk ##1 !data;
    endsequence 

    property header ;
         @(posedge clk) $rose(clk);
    endproperty     
    
    assert property(header) ;

    assign data_out = data ;

endmodule

