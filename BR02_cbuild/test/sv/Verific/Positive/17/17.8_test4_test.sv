
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If a local variable of sequence is assigned at both threads
// of operator 'or', it can be accessed outside operands.

`define true 1 

module packet (input data,input clk,output data_out) ;

    sequence s1;
        int x,y;

        ((data,  x = data, y = data) 
        or (clk ##1 `true, x = data, y = data)) ##1 (y==data);
    endsequence 

    property header ;
         @(posedge clk)data ##1 data ##1 data ##1  s1 ;
    endproperty

    assert property(header) $display(" Call of sequence") ;
endmodule

module test ;
    reg clk ,d;
    wire data_out ;

    initial
        clk = 1 ;

    always 
        #5 clk = ~clk ;

    always @(posedge clk)
        d = $random ;

    initial 
        #1001 $finish ;

    packet p(d, clk,data_out) ; 

    initial
        $monitor("clk = %b, data = %b, data_out = %b", clk, d, data_out) ; 
endmodule

