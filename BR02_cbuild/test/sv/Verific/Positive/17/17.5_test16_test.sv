
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A sequence itself can be used in assertion. This design uses
// a sequence in the assert statement. Normally sequences are written in property or
// sequence block and we assert on the block identifier. Here it is directly asserting
// on the sequence.

module test (input clk, sig1, sig2, sig3, sig4) ;

    sequence s2;
        sig3 ##[1:3] sig4;
    endsequence

    cover property (@(posedge clk) sig1 ##[1:3] sig2 ##3 s2) ;

endmodule

