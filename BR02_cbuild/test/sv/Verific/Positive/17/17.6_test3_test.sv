
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines sequence and a property. The parameter
// of the sequence has default value, the value is an event with 'or' and edge.
// It is instantiated with different combination of edge, 'or' and default value.

// Note: From P1800 System Verilog LRM it is not clear if this feature is supported 
// or not. But in pre-P1800 System Verilog LRM it used to be supported.

module test (input clk, reset,
             input in1, in2,
             output reg out1, out2);

always@(posedge clk)
begin
    out1 <= in1;
    out2 <= in2;
end

sequence seq1(a = posedge clk or negedge reset);
    a ##1 out1;
endsequence

property p1;
    @(posedge clk) seq1(posedge in1 or negedge in2);
endproperty

property p2;
    @(posedge clk) seq1(posedge in1 or in2);
endproperty

property p3;
    @(posedge clk) seq1(in1 or negedge in2);
endproperty

property p4;
    @(posedge clk) seq1(in1 or in2);
endproperty

property p5;
    @(posedge clk) seq1(in1);
endproperty

property p6;
    @(posedge clk) seq1();
endproperty

delay1: assert property(p1) else $display("assert %m failed!");
delay2: assert property(p2) else $display("assert %m failed!");
delay3: assert property(p3) else $display("assert %m failed!");
delay4: assert property(p4) else $display("assert %m failed!");
delay5: assert property(p5) else $display("assert %m failed!");
delay6: assert property(p6) else $display("assert %m failed!");

endmodule

