// Carbon note: This test is invalid SystemVerilog, because the
// instance array of 'bot' modules is size 2, but the array passed through
// the port is an unpacked array of size 4 (1800-2012 section 23.3.3.5,
// paragraph 3). Verific will eventually fix this, but for now it
// causes an error to be emitted with the latest Verific drop (Mar 2014).

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an unpacked array of structure with unpacked 
// width 4 as actual to an array instance whose formal is unpacked array of structure
// with width 2. So, unpacked array of width 2 will be connected to each single
// instance of multiple instances represented by array instance.

typedef struct { int c ;
                 int d ;
               } check ;

typedef struct { int a ;
                 int b ;
                 check C ;
               } num ;

module test ; 

    num n[3:0] ;
    initial begin
       n[0] = '{2,4,'{6,8}} ;
       n[1] = '{1,3,'{5,7}} ;
       n[2] = '{1,2,'{3,4}} ;
       n[3] = '{4,3,'{2,1}} ;
    end

    bot b1[1:0] (n) ;

endmodule

module bot(input num n1[1:0]) ;
    initial 
        $monitor("\n %d, %d, %d, %d,   %d, %d, %d, %d", n1[0].a, n1[0].b, n1[0].C.c, n1[0].C.d, n1[1].a, n1[1].b, n1[1].C.c, n1[1].C.d) ;
    
endmodule

