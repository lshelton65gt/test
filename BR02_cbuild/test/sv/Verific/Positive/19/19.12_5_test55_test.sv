
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance passing byte type variable
// as actual for formal having bit-width 4.

module test(input in, output int out) ;

    byte C ;
    integer I ;

    initial #1 C = 8'b11110000 ;

    bot b1[1:0] (C, I) ;

    assign out = {16{1'b1, in}} ;

endmodule

module bot (input [3:0] C, input [15:0] I) ;
    initial
        $monitor($time,, "%m C=%b, I=%b\n", C, I) ;
endmodule

