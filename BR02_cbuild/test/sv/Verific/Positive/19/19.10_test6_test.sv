
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of timeunit in nested modules.

module test(output bit mod_bit);

    timeunit 10ns;

    module nested_mod;

        initial
        begin
            mod_bit = 1'b0;
            #5 mod_bit = 1'b1;
        end

    endmodule

    nested_mod I();

endmodule

module bench;

    bit bench_bit;

    test instan_test(bench_bit);

    initial $monitor("Time = %f\t Data = %b", $time, bench_bit);

endmodule

