
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design declares an unpacked array of class object inside
// a block and a slice of that array is used as actual to instance array. 

class Student ;
    string name ;
    string addr ;
    int roll ;

    function new(string n, string a, int r) ;
        name = n ; 
        addr = a ;
        roll = r ;
    endfunction

    function void show() ;
        $display("%s, %s, %d", name, addr, roll) ;
    endfunction
endclass

module test #(parameter size = 2)(input in, output out) ;
    initial begin:che
       Student s[size:0] ;
       s[2] = new("asd","fgh",09) ;
       s[1] = new("qwe","asd",12) ;
       s[0] = new("rty","fgh",34) ;
    end
    bot b1[1:0] (che.s) ;


    module bot(input Student s1[1:0]) ;
        initial begin
            s1[0].show() ;
            s1[1].show() ;
         end
    endmodule

    assign out = in ;

endmodule
