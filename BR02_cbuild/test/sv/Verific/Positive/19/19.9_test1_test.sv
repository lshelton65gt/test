
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design shows named port declaration in
// ansi style port declaration of a module.

// P1800 System Verilog LRM reference: 19.9

module test (output logic .P1(r[3:0]),
              output logic .P2(r[7:4]),
              inout .Y(x),
              input bit R) ;

    logic [7:0] r ;
    int x ;

    always @(*) begin
        P1 = R + 4'b1100 ;
        P2 = Y + x ;
    end

    initial
      $display($time,,,"r=%d        x=%d", r, x) ;
    
endmodule

