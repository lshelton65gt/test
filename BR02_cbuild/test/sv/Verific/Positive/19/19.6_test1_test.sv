
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines nested module. A different module with the
// same name as that of the inner module is declared in the $root scope. This module
// is instantiated in the scope of the top level module. So the module in the current
// scope will be instantiated, not the one in the $root scope.

module test (a, b, o);
    input [7:0] a, b;
    output [7:0] o;

	module add (a, b, o);
		parameter p = 4;
		input [p-1:0] a, b;
		output reg [p-1:0] o;

		always @(a, b)
			o = a + b;

	endmodule

	add #(8) i1 (a, b, o);

endmodule

module add (a, b, o);
    parameter p = 4;
    input [p-1:0] a, b;
    output [p-1:0] o;

    initial
        $display ("This msg should not be displayed\n");

endmodule

