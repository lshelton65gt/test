// Carbon modified to eliminate 'initial'

// This test produces a warning:
// Warning 51445: event expressions must result in a singular type
// and then has a codegen error resulting in backend compilation errors.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with two dimensional unpacked
// array of type byte. This is connected as actual for one dimensional unpacked array
// of byte formal.

module test(input int in, output int out) ;

   function int countones(input int in);
      begin
         int temp, count;
         temp = in;
         count = 0;
         for (int i = 0; i<32; ++i)
           begin
              if (temp & 1) count++;
              temp>>=1;
           end
         countones = count;
      end
   endfunction
   
    byte C [7:0][1:0] ;
    int i, j ;
    initial
    begin
        for(i=0; i<8; i++)
        begin
            for(j=0; j<2; j++)
            begin
                C[i][j] = i*j ;
            end
        end
    end

    bot b1[7:0] (C) ;
    assign out = countones(in) ;

endmodule

module bot(input byte i [1:0]) ;
    always @(i)
        $display("i[1]=%b   i[0]=%b", i[1], i[0]) ;
endmodule

