
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): When connecting ports of an instantiation if the data type
// of the variable connecting to the port and the data type of the port being
// connected are different then an initial value change event may be triggered
// at time zero. This design connects a logic to bit type variable, logic is of
// 4 state type, while bit is of 2 state.

module test #(parameter width=8)
             (input clk,
              input logic [0:width-1] in1, in2,
              output wire [width-1:0] out1, out2);

    bot #(width) bot_1 (clk, in1, in2, out1, out2);

    module bot #(parameter width=8)
                (input clk,
                 input bit [width-1:0] in1, in2,
                 output reg [0:width-1] out1, out2);

        int called = 0;
        int showed = 0;

        always@(posedge clk)
        begin
            out1 = -in1<<width/2 | ~in2>>width/2;
            out2 = ~in1>>width/2 & in2;
        end

        always@(in1, in2)
        begin
            if (0 == $time)
                called = 1;
            else
            begin
                if (0 == showed && 0 == called)
                begin
                    $display("Not called at time 0");
                    showed = 1;
                end
            end
        end

    endmodule

endmodule

