
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation inside for-generate. The actual
// is of type byte and has multiple unpacked dimensions. The actual connected to the
// instantiation uses +: range selection method.

module test(input in, output out) ;

    byte C [0:3][1:0] ;

    genvar i ;

    generate
        for(i=1; i<2; i++)
        begin: for_gen_i
            bot b1[1:0] (C[0+:i]) ;
        end
    endgenerate

    assign out = in ;

endmodule

module bot(input byte i [1:0]) ;
    initial
        $monitor($time,,,"i[1]=%b, i[0]=%b", i[1], i[0]) ;
endmodule

