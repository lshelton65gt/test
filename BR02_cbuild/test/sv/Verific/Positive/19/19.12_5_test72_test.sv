
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation using packed dimensions. One
// dimension will be selected for the array instantiation and the other dimension
// will be connected for the formal, since the formal has a packed dimension.

module test(input in, output out) ;

    reg [1:0][4:2] C ;

    initial begin
        #1 C[0] = 3'b010 ;
           C[1] = 3'b101 ;
    end

    bot b[1:0] (C) ;

    assign out = in ;

endmodule

module bot (input [2:0] C) ;
    initial
        #5 $display($time,,"%m %b\n", C) ;
endmodule

