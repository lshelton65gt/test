
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with a variable of type reg
// having two unpacked and two packed dimensions for formal of type wire having two
// packed and one unpacked dimension. The port connection is the whole array.

module test(input in, output out) ;

    reg [15:0][1:0] C [1:0][1:0] ;

    bot b1[1:0] (C) ;

    assign out = in ;

endmodule

module bot(input [15:0][1:0] i [1:0]) ;
    initial $display("Instantiating module bot") ;
endmodule

