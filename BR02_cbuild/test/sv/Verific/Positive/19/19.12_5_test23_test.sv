
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines an unpacked array of structure inside
// a named block and use part select of that unpacked structure array as actual
// to array instance of nested module. The port of instantiated module is a
// unpacked array of structure.

typedef struct { int c ;
                 int d ;
               } check ;

typedef struct { int a ;
                 int b ;
                 check C ;
               } num ;

module test ;

    initial begin:che
       num n[2:0] ;
       n[0] = '{2,4, '{6,8}} ;
       n[1] = '{2,4, '{6,8}} ;
       n[2] = '{1,2, '{3,4}} ;
    end

    bot b1[1:0] (che.n[2:1]) ;

    module bot(input num n1[1:0]) ;
        initial 
            $monitor("\n %d, %d, %d, %d,   %d, %d, %d, %d", n1[0].a, n1[0].b, n1[0].C.c, n1[0].C.d, n1[1].a, n1[1].b, n1[1].C.c, n1[1].C.d) ;
    
    endmodule

endmodule

