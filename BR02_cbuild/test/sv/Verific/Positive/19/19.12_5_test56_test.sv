
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with actual having one
// unpacked and two packed dimensions of type reg. The formal contains only two
// unpacked dimensions.

module test(input in, output out) ;
    reg [15:0][1:0] C [1:0] ;

    bot b1[1:0] (C) ;
    assign out = in + 1 ;

endmodule

module bot(input [15:0][1:0] i) ;
    initial
    $monitor($time,,,"i=%b", i) ;
endmodule

