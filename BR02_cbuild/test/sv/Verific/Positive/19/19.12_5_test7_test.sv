
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses unpacked array of enumeration as actual to
// instance array of module having enumeration type port.

typedef enum {red, yellow, blue, green, white} colors ;

module test ;
    colors C[4:0] ;
    int str[4:0] ;
    initial begin
        C[0] = red ;
        C[1] = yellow ;
        C[2] = blue ;
        C[3] = green ;
        C[4] = white ;
    end

    bot b0[4:0](C, str) ;

    initial
        $display("str[0]: %d, str[1]:%d, str[2]:%d, str[3]:%d, str[4]:%d", str[0], str[1], str[2], str[3], str[4]) ;

endmodule
module bot (input colors C, output int str) ;
    always @(C)     
    case(C)
       red: str = 0 ;
       yellow: str = 1 ;
       blue: str = 2 ;
       green: str = 3 ;
       white: str = 4 ;
    endcase

endmodule

