
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the cases of name conflicts that SystemVerilog
// allows. It defines a task in the global scope. The task has same
// name as a module.

task test ;
    int a ;
endtask

module test(input in, output out) ;

    int a ;

    assign a = 10 ;

    assign out = in ;

    initial
        $display("in=%b", in) ;

endmodule

