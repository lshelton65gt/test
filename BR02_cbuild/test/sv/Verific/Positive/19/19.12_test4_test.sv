
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Ports can remain open/unconnected. If a wire (default) type
// input port is left unconnected, then it gets z value. This design leaves an 
// input port unconnected and checks its value to be z.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output [width-1:0] out1, out2);

    bot #(width) bot_1 (.clk(clk), .in1(in1), .in2(), .out1(out1), .out2(out2));

    always@(posedge clk)
    begin
        if (out1 !== 'z)
            $display("Failed - unconnected port value is not z");
    end

    module bot #(parameter width=8)
                (input clk,
                 input [width-1:0] in1, in2,
                 output reg [0:width-1] out1, out2);

        always@(posedge clk)
        begin
            out2 = in1;
            out1 = in2;
        end

    endmodule

endmodule

