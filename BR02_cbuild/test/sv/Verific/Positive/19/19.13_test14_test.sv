
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function and a module 
// having the same name. This module instantiate another module with
// an identifier and that identifier with similar name is also
// present as a block name in the function. The top module
// hierarchically references to a data element that is present in
// the second module, but the path to it is present in the function
// also. 

module test;

    int a;

    assign a = 10;
    mod I();

    initial
        $display("%d", test.I.a) ;

endmodule

module mod;

    parameter a = 1000;

endmodule

function test;
begin:I
    parameter b = 100;
end
endfunction

