
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design declares unpacked array of two enumeration type.
// Those arrays are used as actual to instance array of module whose one port is
// unpacked array of enumeration type and another port is of enumeration type.

typedef enum {red = 10, yellow, blue, green, white} colors ;
typedef enum {add, minus, divide, multiply} operator ;

module test(input in, output int out) ;

    colors C1[2:0] ;
    operator op[1:0] ;
    int res[1:0] ;

    initial begin
        C1[0] = green ;
        C1[1] = red ;
        C1[2] = blue ;
        op[1] = multiply ;
        op[0] = minus ;
    end

    bot b1[1:0] (C1[1:0], op, res) ;

    assign out = {32{in}} ;

    initial
       $monitor("res[0] = %d, res[1] = %d", res[0], res[1]) ;

endmodule

module bot(input colors C[1:0], input operator op, output int res ) ;

    always @(op)
    case(op) 
      add : res = C[1] + C[0] ;
      minus : res = C[1] - C[0] ;
      divide : res = C[1] / C[0] ;
      multiply : res = C[1] * C[0] ;
    endcase

endmodule

