
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation inside for-generate loop using
// multiple unpacked dimension select. The actual has both packed and unpacked
// dimensions. The formal also has unpacked dimensions. The actual connected to
// the instantiation uses a +: part selected range inside for-generate.

module test(input in, output int out) ;

    reg [7:0] C [1:0][1:0][0:3][2:1][1:2] ;

    genvar i ;

    generate
        for(i=2; i<3; i++)
        begin: for_gen_i
            bot b1[1:0] (C[1][0][1+:i]) ;
        end
    endgenerate

    assign out = {16{1'b1, in}} ;

endmodule

module bot(input reg [7:0] i [1:0][1:0]) ;
    initial
        $monitor($time,,,"i[1][0]=%b, i[0][0]=%b, i[0][0]=%b, i[0][1]=%b", i[1][0], i[0][0], i[0][0], i[0][1]) ;
endmodule

