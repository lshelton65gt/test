
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation having unpacked dimensions and byte
// type. The actual connected is a range select using +: token which will be used to
// select for the array instantiation. The other dimension is connected to the unpacked
// dimension of the formal.

module test(input in, output out) ;
    byte C [3:0][1:0] ;

    dummy b1[1:0] (C[1+:2]) ;

    assign out = in ;
endmodule

module dummy(input byte i[1:0]) ;
    parameter p = 10 ;
    initial $display("Instatiating module dummy") ;
endmodule

