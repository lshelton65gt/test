
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with two unpacked dimensions
// of type byte. The instantiation is such created that it need to select in the 0th
// unpacked dimension.

module test(input int in, output out) ;
    struct {
        byte C [7:0][1:0] ;
    } ss ;

    bot b1[7:0] (ss.C) ;

    assign out = $countones(in) ;

endmodule

module bot(input byte i [1:0]) ;
    initial
        $monitor($time,,,"i[1]=%b, i[0]=%b", i[1], i[0]) ;
endmodule

