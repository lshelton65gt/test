
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance using two dimensional unpacked
// array of 8 bit width array as actual for formal having one unpacked dimension and 
// three packed dimension. 

module test(input in, output out) ;

    reg [7:0] C [1:0][5:4] ;

    initial begin
        #1 C[1][5] = 8'b01010011 ;
           C[0][5] = 8'b00000011 ;
           C[1][4] = 8'b01011111 ;
           C[0][4] = 8'b01111111 ;
    end

    bot b[1:0] (C) ;
    assign out = in ;

endmodule

module bot (input [1:0][2:3][5:4] C [2:3]) ;
    initial
    begin
        #5 $display($time,,"%m %b\n", C[2]) ;
           $display($time,,"%m %b\n", C[3]) ;
    end
endmodule

