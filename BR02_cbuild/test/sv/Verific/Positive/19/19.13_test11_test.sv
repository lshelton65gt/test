
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function and a module in the global/same
// scope having same name. The module has an element named 'a'. The
// module hierarchically references 'a'. 

module test;

    int a;

    assign a = 10;

    initial
        $display("%d", test.a) ;

endmodule

function test;

    parameter b = 100;

endfunction

