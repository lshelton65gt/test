
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines array instance passing two dimensional
// unpacked array of byte as actual for one dimensional unpacked array formal.

module test(input in, output int out) ;
    byte C [3:0][1:0] ;

    bot b1[1:0] (C[2:1]) ;

    assign out = {8{1'b1, 1'b0, in, in}} ;

endmodule

module bot(input byte i [1:0]) ;
    initial
        $monitor($time,,,"i[1]=%b, i[0]=%b", i[1], i[0]) ;
endmodule

