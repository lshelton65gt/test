// Carbon note: This test is invalid SystemVerilog, because the
// instance array of 'bot' modules is size 2, but the array passed through
// the port is an unpacked array of size 4 (1800-2012 section 23.3.3.5,
// paragraph 3). Verific will eventually fix this, but for now it
// causes an error to be emitted with the latest Verific drop (Mar 2014).

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a structure and use its +: select
// as actual in array instantiation. The instantiated module contains a port
// of that structure type with one unpacked dimension.

typedef struct { int c ;
                 int d ;
               } check ;

typedef struct { int a ;
                 int b ;
                 check C ;
               } num ;
module test ;
    parameter p = 4 ;
    top1 #(p) t1() ;
endmodule

module top1 #(parameter size = 2) ;

       num n[0:size-1] ;
    initial begin:che
       n[0] = '{2,4, '{6,8}} ;
       n[1] = '{2,4, '{6,8}} ;
       n[2] = '{1,2, '{3,4}} ;
       n[3] = '{1,2, '{3,4}} ;
    end

    bot b1[1:0] (n[0+:size]) ;

endmodule

module bot(input num n1[0:1]) ;
    initial 
        $monitor("\n %d, %d, %d, %d,   %d, %d, %d, %d", n1[0].a, n1[0].b, n1[0].C.c, n1[0].C.d, n1[1].a, n1[1].b, n1[1].C.c, n1[1].C.d) ;
    
endmodule

