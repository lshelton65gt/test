
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If multiple entities having the same name are declared in different
// scopes, the entity in the closest enclosing scope is matched when referencing an entity
// only with its name. This design defines more than one element with same name and refers 
// to them from various places to check the above behaviour.

int i = 0;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    real i = 1.0;

    module inner_mod(input clk, input int in, output int out);

        int i = 2;

        always@(posedge clk)
        begin
            real i = 3.2;

            if(3.2 != i)
            begin
                int i = 4;
                $display("Error: Closest enclosing scope not used!");
                out = 0;
                if (4 != i)
                    $display("Error: Closest enclosing scope not used!");
            end
            else
            begin
                real i = 5.4;
                out = 1;
                begin
                    if (5.4 != i)
                        $display("Error: Closest enclosing scope not used!");
                end
                if (5.4 != i)
                    $display("Error: Closest enclosing scope not used!");
            end
        end

        initial
            if (2 != i)
                $display("Error: Closest enclosing scope not used!");

    endmodule

    initial
        if (1.0 != i)
            $display("Error: Closest enclosing scope not used!");

endmodule


