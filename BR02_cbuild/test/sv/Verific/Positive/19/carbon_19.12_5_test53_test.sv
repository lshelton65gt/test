// Carbon modified to eliminate initial construct, wrote a $countones function

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Checking array instance with two unpacked dimensions and two
// packed dimension of type reg.

module test(input int in, output out) ;
    reg [3:0][1:0] C [1:0][5:4] ;

   function int countones(input int in);
      begin
         int temp, count;
         temp = in;
         count = 0;
         for (int i = 0; i<32; ++i)
           begin
              if (temp & 1) count++;
              temp>>=1;
           end
         countones = count;
      end
   endfunction
   
    initial begin
        #1 C[1][5] = 8'b01010011 ;
           C[0][5] = 8'b00000011 ;
           C[1][4] = 8'b01010000 ;
           C[0][4] = 8'b11110011 ;
    end

    bot b[1:0] (C) ;
    assign out = countones(in) ;

endmodule

module bot (input [7:0] C [2:3]) ;
always @(*)
    begin
        #5 $display($time,,"%m %b\n", C[2]) ;
           $display($time,,"%m %b\n", C[3]) ;
    end
endmodule

