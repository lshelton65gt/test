
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of timeunit in $root name space.

timeunit 10ps;
timeprecision 0.1ps;

module mod1(output int mod_in1);

    timeunit 100ps;

    initial
    begin
        mod_in1 = 0;
        #5 mod_in1 = 1;
    end

endmodule

module mod2(output int mod_in2);

    initial
    begin
        mod_in2 = 0;
        #5 mod_in2 = 5;
    end

endmodule

module test;

    int bench_in1, bench_in2;

    mod1 instan_mod1(bench_in1);
    mod2 instan_mod2(bench_in2);

    initial $monitor(" Time = %d (base 10ns)\tbench_in1 (base 1000ps) = %d\tbench_in2 (base 10ns) = %d", $time, bench_in1, bench_in2);  

endmodule

