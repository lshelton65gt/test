
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an unpacked array of class object as actual
// to an instance array. The port of instantiated module is an unpacked array of class.

class Student ;
    string first_name ;
    string last_name ;
    int roll ;

    function new(string n, string l, int r) ;
        first_name = n ; 
        last_name = l ;
        roll = r ;
    endfunction

    function void show() ;
        $display("name: %s %s, roll: %d", first_name, last_name, roll) ;
    endfunction
endclass

module test(input in, output out) ;
    Student s[3:0] ;
    initial begin
        s[3] = new("Hermione","Granger",78) ;
        s[2] = new("Harry","Potter",90) ;
        s[1] = new("Ronald","Weasley",12) ;
        s[0] = new("Ginny","Weasley",34) ;
    end
    bot b1[1:0](s) ;
    assign out = in + 1 ;
endmodule

module bot(input Student s1[3:0]) ;
    initial begin
        s1[0].show() ;
        s1[1].show() ;
        s1[2].show() ;
        s1[3].show() ;
    end
endmodule

