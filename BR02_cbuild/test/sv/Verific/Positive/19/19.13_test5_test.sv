
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the cases of name conflicts that SystemVerilog
// allows. It defines a new structure type using typedef in the global
// scope. A data element of that type is defined in a module with the
// same name as the module.

typedef struct {
    int a;
} myType;

module test;

    int a;
    myType test;


    assign a = 10;
    assign test.a = 100;

endmodule

module bench;

initial
    $monitor(" test.a = %d", test.a);

    test t1() ;

endmodule

