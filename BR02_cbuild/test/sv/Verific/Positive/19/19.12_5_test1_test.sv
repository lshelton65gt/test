
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an unpacked array of class object as 
// actual to an instance array. The instantiated module contains a class type
// port.

class Student ;
    string name ;
    string addr ;
    int roll ;

    function new(string n, string a, int r) ;
        name = n ; 
        addr = a ;
        roll = r ;
    endfunction

    function void show() ;
        $display("name: %s, addr: %s, roll: %d", name, addr, roll) ;
    endfunction
endclass

module test(input in, output out) ;

    Student s[1:0] ;

    initial begin
        s[1] = new("Sumeet","Chawla",12) ;
        s[0] = new("Abhishek","Diwan",34) ;
    end

    bot b1[1:0] (s) ;

    assign out = in ;

endmodule

module bot(input Student s1) ;
    initial
        s1.show() ;
endmodule

