
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantation using actual having packed and
// unpacked dimensions. The formal also has multiple packed dimensions.

module test(input in, output out) ;
    reg [1:0][4:2] C [5:4] ;

    initial begin
        #1 C[5] = 6'b010111 ;
           C[4] = 6'b101000 ;
    end

    bot b[1:0] (C) ;

    assign out = !in ;

endmodule

module bot (input [3:2][2:0] C) ;
    initial
        #5 $display($time,,"%m %b\n", C) ;
endmodule
