// carbon modified to get index expression correct

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines array instance of a module having one
// dimensional unpacked array of width 2 as port. The instantiating module defines
// an three dimensional unpacked array of byte and passes that as actual to array
// instance specifying one dimension.

module test(input int in, output out) ;
    byte C [3:2][2:1][5:4] ;

    initial begin
        #1 C[3][2][5] = 8'b01010011 ;
           C[3][1][5] = 8'b00001111 ;
           C[3][2][4] = 8'b11110000 ;
           C[3][1][4] = 8'b00110011 ;
           C[2][2][5] = 8'b01010010 ;
           C[2][1][5] = 8'b00001110 ;
           C[2][2][4] = 8'b11110001 ;
           C[2][1][4] = 8'b00110010 ;
    end

    bot b[1:0] (in, C[3]) ;

    assign out = ^in ;

endmodule

module bot (input int in, input byte C [2:3]) ;
   always @(posedge in[0])
    begin
       $display($time,,"%m %b %b\n", C[2], C[3] ) ;
    end
endmodule

