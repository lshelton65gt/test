
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If the outer module does not have a timeunit declaration
// and it has a `timescale compiler directive then it is used as the timeunit.
// This design uses `timescale to infer timeunit.

`timescale 10ms / 1us

module test(input int in, output out) ;

    int a ;

    initial
    begin
        a = 20 ;
        repeat(5)
            #5 a++ ;
        $monitor($time,,, "data (base 10ms/1us) = %d", a) ;
    end

    assign out = $onehot(in) ;

endmodule

