// Carbon modified to remove initial and monitor.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with three unpacked 
// dimensions of type byte.

module test(input wire clk, input in, output out) ;
   byte C [1:0][2:1][1:0] = '{ '{'{8'h00, 8'h01}, '{8'h02, 8'h03}}, '{'{8'h04, 8'h05}, '{8'h06, 8'h07}}};

    bot b1[1:0] (clk, C[1]) ;
    assign out = in + 1 ;

endmodule

module bot(input wire clk, input byte i [1:0]) ;
    always @(posedge clk)
        $display("i[1]=%b, i[0]=%b", i[1], i[0]) ;
endmodule

