
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines array instance of a module having two 
// dimensional unpacked array of byte as port. In instantiating module a four 
// dimensional unpacked array of width 8 is defined and it is passed as actual
// specifying only one dimension.

module test(input in, output out) ;
    reg [7:0] C [1:0][2:1][1:0][1:0] ;

    bot b1[1:0] (C[1]) ;

endmodule

module bot(input reg [7:0] i [1:0][1:0]) ;
    initial
        $monitor($time,,,"i[1][0]=%b, i[0][1]=%b", i[1][0], i[0][1]) ;
endmodule

