
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation inside for-generate with multiple
// packed and unpacked dimensions. The selected dimension is a part selection. The formal
// also has unpacked dimensions and is a multibit byte type.

module test(input in, output out) ;

    reg [7:0] C [1:0][1:0][3:0][1:0][1:0] ;

    genvar i ;

    generate
        for(i=2; i<3; i++)
        begin: for_gen_i
            bot b1[1:0] (C[1][0][2-:i]) ;
        end
    endgenerate

    assign out = in + 1 ;

endmodule

module bot(input reg [7:0] i [1:0][1:0]) ;
    initial
        $monitor($time,,,"i[1][0]=%b, i[0][0]=%b, i[0][0]=%b, i[0][1]=%b", i[1][0], i[0][0], i[0][0], i[0][1]) ;
endmodule

