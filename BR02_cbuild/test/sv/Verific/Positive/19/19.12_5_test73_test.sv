
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation with byte type and unpacked dimensions.
// The actual instantiation is inside a for-generate block. The formal port also has an
// unpacked dimension.

module test(input int in, output out) ;
    byte C [3:0][1:0] ;

    genvar i ;

    generate
        for(i=2; i<3; i++)
        begin: for_gen_i
            bot b1[1:0] (C[2-:i]) ;
        end
    endgenerate

    assign out = $countones(in) ;

endmodule

module bot(input byte i [1:0]) ;
    initial
        $monitor($time,,,"i[1]=%b, i[0]=%b", i[1], i[0]) ;
endmodule

