
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines an unpacked array of structure inside a
// named block and select expression of that array is used as actual to array instance
// of a nested module. Instantiated module contains a port of structure type having 
// one unpacked dimension.

typedef struct { int c ;
                 int d ;
               } check ;

typedef struct { int a ;
                 int b ;
                 check C ;
               } num ;
module test ;

    function int fact(int p) ;
        if (p == 0 || p == 1)
            return 1 ;
        return (p * fact(p-1) );
    endfunction
    parameter p = 2 ;
    top1 #(fact(p)) t1() ;

endmodule


module top1 #(parameter size = 2) ;

    initial begin:che
       num n[size:0] ;
       n[0] = '{2,4, '{6,8}} ;
       n[1] = '{2,4, '{6,8}} ;
       n[2] = '{1,2, '{3,4}} ;
    end

    module bot(input num n1[1:0]) ;

        initial 
            $monitor("\n %d, %d, %d, %d, %d, %d, %d, %d", n1[0].a, n1[0].b, n1[0].C.c, n1[0].C.d, n1[1].a, n1[1].b, n1[1].C.c, n1[1].C.d) ;
    
    endmodule
    bot b1[1:0] (che.n[2:1]) ;

endmodule

