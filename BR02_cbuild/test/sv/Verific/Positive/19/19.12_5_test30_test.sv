
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with a variable of type 
// byte having two unpacked dimensions for formal of type byte having one unpacked
// dimension. The instantiation is inside a for-generate loop.

module test(input in, output out) ;

    byte C [7:0][1:0] ;

    genvar i ;

    generate
        for(i=2; i<8; i++)
        begin: for_gen_i
            bot b1[(i - 1):0] (C[i]) ;
        end
    endgenerate

    assign out = in ;

endmodule

module bot(input byte i [1:0]) ;
    initial $display("Instatiating bot") ;
endmodule

