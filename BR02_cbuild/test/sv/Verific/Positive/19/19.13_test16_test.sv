
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the cases of name conflicts that SystemVerilog
// allows. It defines a new structure type using typedef in the global
// scope. It declares a data element in the module and assigns values
// to it through simple and hierarchical naming.

typedef struct {
    int a;
} myType;

myType test1;

module test;

    int a;

    assign a = 10;
    assign test1.a = 100;

endmodule

module bench;

initial
    $monitor(" test.a = %d", test.a);

    test t1() ;

endmodule

