
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Modules that are not explicitly instantiated get implicitly
// instantiated in $root scope. This design defines a module but does not instantiate
// that anywhere to check whether it is implicitly instantiated in the global $root
// scope or not.

module test #(parameter width=8)
             (input clk,
              input reg [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    always@(posedge clk)
    begin
        out1 = ~in1 | in2;
        out2 = (in2-1) ^ ~in1;
    end

endmodule

module root_inst();

    always
        #10ns $display("Implicit instantiation: Another 10ns passed away!");

endmodule

