
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function and a module in the global/same
// scope having same name. Another module is also defined in that
// scope, but with a different name. This module is instantiated
// in the other module with an identifier and that same identifier
// is also present as a block name in the function. The top module
// hierarchically references to a data element that is present in
// the function only, but the path to it is present in the module
// also. It is to check the behaviour of the tool in such a situation.

module test;

    int a;

    assign a = 10;
    mod I();

    initial
        $display("%d", test.I.a) ;

endmodule

module mod;

    parameter b = 1000;

endmodule

function test;
begin:I
    parameter a = 100;
end
endfunction

