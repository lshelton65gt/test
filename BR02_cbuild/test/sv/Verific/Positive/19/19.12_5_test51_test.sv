
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance using one dimensional
// unpacked array of 8 bit width vector as actual for formal of type byte.

module test(input int in, output out) ;

    reg [7:0] C [1:0] ;

    initial
    begin
        C[0] = "A" ;
        C[1] = "b" ;
    end
        

    bot b1[1:0] (C) ;

    assign out = in + 1 ;

endmodule

module bot(input byte i) ;
    initial
        $display("i = %b", i) ;
endmodule

