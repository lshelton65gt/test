
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether timeunit can be inherited from the top level.

timeunit 10ns;

module test;

    int a;

    module nestedMod;
        timeunit 100ms;

        initial
        begin
	    a = 10;
            repeat(5)
                #5 a++;
            $monitor($time,,, "data (base 100ms) = %d", a);
        end
    endmodule

    initial
    begin
        a = 20;
        repeat(5)
            #5 a++;
        $monitor($time,,, "data (base 10ns) = %d", a);
    end

endmodule

