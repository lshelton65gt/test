
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Modules can be explicitly instantiated in $root top level.
// This design defines a module and instantiates that one in the global $root
// scope.

root_inst ri();   // explicitly instantiated in the $root scope.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    always@(posedge clk)
    begin
        out1 = ~in1 - in2;
        out2 = in2 - ~in1;
    end

endmodule

module root_inst();

    always
        #10 $display("Another 10ns passed away!");
    initial #1000 $finish;
	

endmodule

