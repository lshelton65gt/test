
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with three dimensional
// unpacked  array of type byte. The instantiation is created in such a way that
// it need to select in the last of the selected unpacked dimension.

module test(input in, output int out) ;

    byte C [1:0][7:0][1:0] ;

    bot b1[1:0] (C[1][0][1:0]) ;

    assign out = {16{1'b0,in}} ;

endmodule

module bot(input byte i[0:1]) ;
    initial
        $display("i[1] =%d, i[0] =%d", i[1], i[0]) ;
endmodule

