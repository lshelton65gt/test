
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with two dimensional unpacked
// array of type byte. This is connected as actual for one dimensional unpacked array
// of byte formal.

module test(input in, output out) ;

    byte C [7:0][1:0] ;
    int i, j ;
    initial
    begin
        for(i=0; i<7; i++)
        begin
            for(j=0; j<7; j++)
            begin
                C[i][j] = j ;
            end
        end
    end

    bot b1[7:0] (C) ;
    assign out = $countones(in) ;

endmodule

module bot(input byte i [1:0]) ;
    initial
        $display("i[1]=%b   i[0]=%b", i[1], i[0]) ;
endmodule

