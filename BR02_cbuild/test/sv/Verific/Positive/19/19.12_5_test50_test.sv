
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with variable having five
// unpacked dimensions and one packed dimension of type reg.
// The port connection is +: range on third unpacked dimension inside a for-generate 
// loop.

module test(input in1, in2, output int out) ;

    reg [7:0] C [1:0][1:0][3:0][2:1][1:2] ;

    genvar i ;

    generate
        for(i=2; i<3; i++)
        begin: for_gen_i
            bot b1[1:0] (C[1][0][1+:i]) ;
        end
    endgenerate

    assign out = {16{in1, in2}} ;

endmodule

module bot(input reg [7:0] i [1:0][1:0]) ;
    initial
        $display("Instantiating module bot") ;
endmodule

