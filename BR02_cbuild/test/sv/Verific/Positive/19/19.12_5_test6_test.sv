
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses unpacked array of enumeration as actual
// to instance array of module having enumeration type port.

typedef enum {red, yellow, blue, green, white} colors ;

module test ;
    colors C[1:0] ;

    initial begin
        C[1] = red ;
        C[0] = green ;
    end

    bot b1[1:0] (C) ;

endmodule

module bot (input colors C) ;
    initial
        $display("%s \n", C.name) ;
endmodule

