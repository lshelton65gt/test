
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): In SystemVerilog ports can be of type net, interface,
// event and of any type including an array or a structure or union. This
// design uses an event type element as port.

module test #(parameter width=8)
             (input event evt1,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

always@(evt1 or in1 or in2)
begin
    out1 = in1 - ~in2;
    out2 = ~in2 - in1;
end

endmodule

