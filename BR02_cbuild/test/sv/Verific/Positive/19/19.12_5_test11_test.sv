
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design declares unpacked arrays of two enumeration types
// inside a block and used those as actual to instance array by hierarchical references.

typedef enum {red = 10, yellow, blue, green, white} colors ;
typedef enum {add, minus, divide, multiply} operator ;

module test(input colors C[1:0], input operator op, output int res ) ;

    always @(op)
    case(op) 
      add : res = C[1] + C[0] ;
      minus : res = C[1] - C[0] ;
      divide : res = C[1] / C[0] ;
      multiply : res = C[1] * C[0] ;
    endcase

endmodule

module bench #(parameter size = 10) ;

    initial begin: ch
        begin: ch1
           colors C1[size:0] ;
           operator op[1:0] ;
           int res[1:0] ;

           C1[0] = green ;
           C1[1] = red ;

           op[1] = multiply ;
           op[0] = minus ;
        end
    end

    test t1[1:0] (ch.ch1.C1[1:0], ch.ch1.op, ch.ch1.res) ;

    initial
       $monitor("res[0] = %d, res[1] = %d", ch.ch1.res[0], ch.ch1.res[1]) ;

endmodule

