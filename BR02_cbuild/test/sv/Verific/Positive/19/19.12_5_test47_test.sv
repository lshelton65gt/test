
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with variable having five
// unpacked dimensions and one packed dimension of type reg for port having two
// unpacked dimensional of type byte.
// The port connection is a +: range.

module test(input in, output out) ;

    reg [7:0] C [1:0][1:0][3:0][4:5][2:1] ;

    bot b1[1:0] (C[1][0][1+:2]) ;

    assign out = in ;

endmodule

module bot(input reg [7:0]  i [1:0][1:0]) ;
    initial
        $display("Intantiating module bot") ;
endmodule

