
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Tasks can be declared as well as called in the $root scope.
// This design defines two tasks in the $root scope and invokes one of them from
// within the $root scope. The other task is invoked from within an always block
// inside a module.

task header;
    $display ("This tests task declaration in $root\n");
endtask

typedef int myType;

task incr(output myType out, input myType in);
    out = in++;
endtask

module test;

    int out, in;

    initial
    begin
        in = 0;
        out = 0;
        header();
        $monitor($time, "out=%d, in=%d", out, in);
        #100 $finish;
    end

    always
        #5 incr(out, in);
    
endmodule

