
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design instantiates an interface array inside a nested
// module. This interface instance is accessed hierarchically from module 'bench'
// and its part select is used as actual to instance array of modules. Both instantiated
// module have a unpacked array type generic interface port.

interface combination(input reg clk) ;
    reg r ;
    int num1 ;
endinterface

module test(interface a[1:0]);
    always @(a[0].clk) begin
       a[0].r = 1001 ;
       a[0].num1 = a[0].r || a[0].clk ;     
    end
endmodule

module hier ;

   module lowest ;
       reg clock ;

       combination com[2:0] (clock) ;
   endmodule

   lowest lo() ;
   
endmodule

module bench ;

   hier check() ;

   test t1[1:0](check.lo.com[2:1]) ;
   test_slave ts[1:0](check.lo.com[1:0]) ;

   initial begin
      check.lo.clock = 0 ;
      #100 $finish ;
   end

   always  
      #5 check.lo.clock = ~check.lo.clock ;

    initial
        $monitor("t1[1].a[0].r=%b, t1[1].a[0].num=%d, t1[0].a[0].r=%b, t1[0].a[0].num=%d ts[1].b[0].r=%b, ts[1].b[0].num=%d, ts[0].b[0].r=%b, ts[0].b[0].num=%d", t1[1].a[0].r, t1[1].a[0].num1, t1[0].a[0].r, t1[1].a[0].num1, ts[1].b[0].r, ts[1].b[0].num1, ts[0].b[0].r, ts[1].b[0].num1) ;
    
    initial
        #1001 $finish ;
endmodule

module test_slave(interface b[1:0]) ;

   always @(b[0].num1)
      b[0].r = b[0].num1 ;

endmodule

