
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation with formal having one packed
// dimension which matches the size of type byte and has one unpacked dimension.
// The actual has multiple unpacked dimension and is of type byte. The actual
// connected to the instance is selected in one dimension and the one is used
// for the array instance select and the other will be connected to unpacked
// dimension of the formal.

module test(input in, output out) ;
    byte C [3:2][2:1][5:4] ;

    initial begin
        #1 C[3][1][5] = 8'b01010011 ;
           C[3][0][5] = 8'b00001111 ;
           C[3][1][4] = 8'b11110000 ;
           C[3][0][4] = 8'b00110011 ;
    end

    bot b[1:0] (C[3]) ;

    assign out = in + 1 ;

endmodule

module bot (input byte C [2:3]) ;
    initial
    begin
        #5 $display($time,,"%m %b\n", C[2]) ;
           $display($time,,"%m %b\n", C[3]) ;
    end
endmodule

