
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation where formal is of type byte.
// The actual has a packed dimension that matches the size of byte and also multiple,
// unapcked dimensions. The formal also has multiple unpacked dimensions. The actual
// connected is selected in one dimension and the other one is selected for the array
// instantiation whereas the remaining dimensions will be connected to the unpacked
// dimension of the formal.

module test(input in, output out) ;

    reg [7:0] C [1:0][2:1][1:0][1:0] ;

    bot b1[1:0] (C[1]) ;

    assign out = in ;

endmodule

module bot(input reg [7:0] i [1:0][1:0]) ;
    initial
        $display($time,,,"i[1][0]=%b, i[0][0]=%b, i[0][0]=%b, i[0][1]=%b", i[1][0], i[0][0], i[0][0], i[0][1]) ;
endmodule

