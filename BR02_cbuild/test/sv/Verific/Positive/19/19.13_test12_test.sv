
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function and a module in the global/same
// scope having same name. Another module is also defined in that
// scope, but with a different name. This module is instantiated
// in the another module with an identifier and that same identifier
// is also present as a block name in the function. All the three
// scopes have a data element named 'a'. The top module hierarchically
// references this data element. As all of the scopes satisfy this
// reference, it is to check which one is chosen by the tool.

module test;

    int a;

    assign a = 10;
    mod I();

    initial
        $display("%d", test.I.a) ;

endmodule

module mod;

    parameter a = 1000;

endmodule

function test;
begin:I
    parameter a = 100;
end
endfunction

