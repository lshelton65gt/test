
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with a variable of type
// reg having five unpacked and one packed dimensions for formal of type byte having 
// two unpacked dimensions. The port connection is a -: variable part select on 
// third unpacked dimension.

module test(input in, output out) ;
    reg [7:0] C [1:0][1:0][3:0][1:0][1:0] ;

    bot b1[1:0] (C[1][0][2-:2]) ;
    assign out = ~in ;
endmodule

module bot(input reg [7:0] i [1:0][1:0]) ;
    initial
        $display("Instantiating module bot") ;
endmodule

