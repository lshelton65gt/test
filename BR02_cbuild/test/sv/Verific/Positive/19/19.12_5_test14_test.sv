
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design instantiates an interface inside a module. This
// instantiation is accessed hierarchically from another module and is used as actual
// to instance array of module having generic interface type ports.

interface combination(input reg clk) ;
    reg r ;
    int num ;
endinterface

module test(interface a);
    always @(a.clk) begin
       a.r = 1001 ;
       a.num = a.r || a.clk ;     
    end
endmodule

module hier ;

   reg clock ;
   combination com (clock) ;
   
endmodule

module bench ;

   hier check() ;

   test t1[1:0](check.com) ;
   test_slave ts[1:0](check.com) ;

   initial begin
      check.clock = 0 ;
      #100 $finish ;
   end

   always  
      #5 check.clock = ~check.clock ;

    initial
        $monitor("t1[1].a.r=%b, t1[1].a.num=%d, t1[0].a.r=%b, t1[0].a.num=%d ts[1].b.r=%b, ts[1].b.num=%d, ts[0].b.r=%b, ts[0].b.num=%d", t1[1].a.r, t1[1].a.num, t1[0].a.r, t1[1].a.num, ts[1].b.r, ts[1].b.num, ts[0].b.r, ts[1].b.num) ;
    
    initial
        #1001 $finish ;

endmodule

module test_slave(interface b) ;

   always @(b.num)
      b.r = b.num ;

endmodule

