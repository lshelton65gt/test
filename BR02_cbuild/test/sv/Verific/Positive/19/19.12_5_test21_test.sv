
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses a structure variable as actual to an array
// instance whose formal is also a structure variable. So, the same structure variable
// will be used by 2 single instances represented by array instance.

typedef struct { int c ;
                 int d ;
               } check ;

typedef struct { int a ;
                 int b ;
                 check C ;
               } num ;

module test(input [31:0] in, output out) ;

    num n ;

    initial begin
       n = '{1,2, '{3,4}} ;
    end

    bot b1[1:0] (n) ;

    assign out = $onehot(in) ;

endmodule

module bot(input num n1) ;
    initial 
        $display("%d, %d, %d, %d",n1.a, n1.b, n1.C.c, n1.C.d) ;
   
endmodule

