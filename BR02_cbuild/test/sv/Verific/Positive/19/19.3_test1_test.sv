
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design instantiates an udp and a module in compilation scope and
// this is done in such a way that both of them look alike.

reg r1, r2;
wire w1, w2;

// module_id	param_over_ride		(ports)
mod #(1) md (w1, r1);
// udp_id	delay2		(terminals)
l2b #(1) lb (w2, r2);

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    r1 = |in1;
    r2 = ^in2;

    out1 = in1 | in2 ^ { r1, r2, w1, w2 };
    out2 = in2 - in1 | { w1, r2, r1, w2 };
end

endmodule

module mod(output reg out, input in);
    parameter p = 4;

    always@(in)
        out = p | in;
endmodule

primitive l2b(out, in);
    output out;
    input in;

    table
        0 : 0;
        1 : 1;
        x : x;
    endtable
endprimitive

