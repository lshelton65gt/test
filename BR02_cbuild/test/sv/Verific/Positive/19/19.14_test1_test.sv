
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design references hierarchical names from various places.
// Those identifiers are also defined in various places.

module test(input clk);

    real r = 2.2;

    always@(posedge clk)
    begin: curr_scope

        real r = 3.3;

        $display("$root top level value = %d", $root.r);
        $display("module level value = %d", test.r);
        $display("current scope value = %d", test.curr_scope.r);
    end

endmodule

real r = 1.1;

