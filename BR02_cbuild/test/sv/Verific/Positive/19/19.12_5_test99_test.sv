
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation having unpacked dimensions and byte
// type. The actual connected is a range select using -: token which will be used to
// select for the array instantiation. The other dimension is connected to the unpacked
// dimension of the formal.

module test(input in, output out) ;

    byte C [3:0][1:0] ;

    bot b1[1:0] (C[2-:2]) ;

    assign out = in + 1 ;

endmodule

module bot(input byte i [1:0]) ;
    initial
        $display($time,,,"i[1]=%b, i[0]=%b", i[1], i[0]) ;
endmodule

