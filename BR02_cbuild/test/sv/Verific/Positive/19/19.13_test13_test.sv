
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of the scope rule defining functions in root scope and
// inside modules with same name.

function int func;
    input a;
    reg t ;
    t = a;
    return t++;
endfunction

module test(out);
  output bit[3:0]out;

  function int func(input a);
    reg t ;
    t = a;
    t++;
    return t ;
  endfunction

  mod2 #(.p(func(2)))instanMod1(out);
endmodule

module mod2(out);
parameter p = 0;
output bit [p:0]out;

function int func(input a);
    return (a+2);
endfunction

initial
begin
    out = func(0) + func(0);
end

endmodule


