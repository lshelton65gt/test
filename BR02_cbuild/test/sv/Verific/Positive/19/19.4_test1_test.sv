
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Functions can be declared as well as called in the $root
// scope. This design defines two functions in the $root scope and calls one of
// them from within the $root scope. The other function is called from within an
// always block inside a module.

function void header;
    $display ("This tests function declaration in $root\n");
endfunction

typedef int myType;

function void incr(output myType out, input myType in);
    out = in++;
endfunction

module test;
    int out, in;

    initial
    begin
        in = 0;
        out = 0;
    end

    always
        #5 incr(out, in);

    initial 
    begin
        header();
        $monitor($time, "out=%d, in=%d", out, in);
        #100 $finish;
    end

endmodule

