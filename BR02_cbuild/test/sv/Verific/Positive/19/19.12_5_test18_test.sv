
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an unpacked array of structure as actual to
// an array instance whose formal is of structure type.

typedef struct { int c ;
                 int d ;
               } check ;

typedef struct { int a ;
                 int b ;
                 check C ;
               } num ;

module test(input in, output out) ;

    num n[1:0] ;
    initial begin
       n[1] = '{2, 4, '{6,8}} ;
       n[0] = '{1, 3, '{5,7}} ;
    end
    bot b1[1:0] (n) ;

    assign out = in ;

endmodule

module bot(input num n1) ;
    initial
        $display("%d, %d, %d, %d",n1.a, n1.b, n1.C.c, n1.C.d) ;
endmodule

