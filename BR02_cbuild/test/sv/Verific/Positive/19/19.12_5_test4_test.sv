
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an object of a class as actual to an array
// instance of module whose port is a class object.

class Student ;
    string name ;
    string addr ;
    int roll ;

    function new(string n, string a, int r) ;
        name = n ; 
        addr = a ;
        roll = r ;
    endfunction

    function void show() ;
        $display("%s, %s, %d", name, addr, roll) ;
    endfunction
endclass

module test ;
    Student s ;
    initial begin
       s = new("rty","fgh",34) ;
    end
    bot b1[1:0] (s) ;
endmodule

module bot(input Student s1) ;
    initial 
        s1.show() ;
endmodule

