
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of new data type timeunit.

module test(output int in);

    timeunit 100ps;
    timeprecision 10fs;

    initial
    begin
        in = 5;
        #5 in = 10;
    end

endmodule

module bench;

    timeunit 100ps;
    timeprecision 10fs;
    int bench_in;

    test instan_test(bench_in);

    initial $monitor($time,,"data = %d", bench_in);

endmodule

