
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design connects ports of an instantiation using dot name
// port connection rule. The specialty is that it connects ports of type event and 
// does not maintain the order of the ports as they are declared.

module test #(parameter width=8)
             (input clk,
              input event evt,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    bit [0:width-1] i1, i2;
    logic [width-1:0] o1;

    test_bot #(width) bot_1 (.i1, .clk, .i2, .o1, .evt);

    always@(negedge clk)
    begin
        i1 = in1 | in2;
        i2 = in1 ^ in2;
    end

    always@(posedge clk)
    begin
        out1 = o1 + in1;
        out2 = o1 - in2;
    end

    module test_bot #(parameter w = 4)
                     (input clk,
                      input event evt,
                      input bit [w-1:0] i1, i2,
                      output logic [0:w-1] o1);

        event data ;

        always_ff@(posedge clk)
        begin
            data = evt ;
            o1 = i1 ^ i2;
        end

    endmodule

endmodule

