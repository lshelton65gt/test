
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the cases of name conflicts that SystemVerilog
// allows. It defines a function in the global scope. This function has the
// same name as a module.

function void test;
    int a;
endfunction

module test;

    int a;

    assign a = 10;
    assign test.a = 100;

endmodule

module bench;

initial
    $monitor(" test.a = %d", test.a);

    test t1() ;

endmodule

