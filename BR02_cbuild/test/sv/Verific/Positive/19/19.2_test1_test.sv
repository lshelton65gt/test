
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The package declaration creates a scope that contains
// declarations intended to be shared among one or more compilation units,
// modules, macromodules, interfaces, or programs. The following design 
// defines a package.

package ComplexPkg ;

    typedef struct {
        real i, r ;
    } Complex ;

    function Complex add(Complex a, b) ;
        add.r = a.r + b.r ;
        add.i = a.i + b.i ;
    endfunction

    function Complex mul (Complex a, b) ;
            mul.r = (a.r * b.r) - (a.i * b.i) ;
        mul.i = (a.r * b.i) + (a.i *b.r) ;
    endfunction

endpackage : ComplexPkg

module test() ;

    ComplexPkg::Complex a, b ;
    ComplexPkg::Complex out ;

    initial
    begin
        a.i = $random() ;
        b.i = $random() ;
        out = ComplexPkg::mul(a,b) ;
        $display("out = %f", out.r) ;
    end

endmodule: test

