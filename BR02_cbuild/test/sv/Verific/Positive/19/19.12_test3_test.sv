
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Ports can remain open/unconnected. This design instantiates
// a module and does not connect a port  which remains open. The data type of the
// open port is wire(net type).

module test ;

    reg [0:3] a;

    wire [0:3] o1, o2;

    initial a = 4'b1111;

    bot T(.in1(a), .in2(), .out1(o1), .out2(o2)); 

initial $monitor ("a=%b,o1=%b,o2=%b", a, o1, o2);

endmodule

module bot (input wire [0:3] in1, in2, output reg [0:3] out1, out2);

    always @ (in1 or in2)
    begin
        out1 = in1;
        out2 = in2;
    end

endmodule

