// Carbon modified to eliminate initial block

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of new data type timeunit.

module test(input wire [31:0] in, output wire [31:0] out);

//    timeunit 100ps;
//    timeprecision 10fs;

   assign out = in;

endmodule

module bench (input wire [31:0] in, output wire [31:0] out);

   timeunit 100ps;
   timeprecision 10fs;

    test instan_test(in, out);

   always @(in)
     $display($time, ", in = %d", in);

endmodule

