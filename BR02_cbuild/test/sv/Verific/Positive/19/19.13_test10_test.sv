
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function and a module in the global/same
// scope having the same name. Both of them have a common element with
// name 'a'. The module hierarchically references to 'a'. As both of
// them have same name, so it can be any of the two data elements.
// It is to check which one is picked by the tool.

module test;

    int a;

    assign a = 10;

    initial
        $display("%d", test.a) ;

endmodule

function test;

    parameter a = 100;

endfunction

