
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines two packages. Both packages define 
// enum type having literal 'FALSE'. One import declaration imports all identifiers
// of package 'p' and other imports only enum type of package 'q'. So enum
// literal 'FALSE' of package 'p' is visible to module. 

package p ;
    typedef enum { FALSE, TRUE } bool_t ;
endpackage

package q ;
    typedef enum { ORIGINAL, FALSE } teeth_t ;
endpackage

module test(input in, output out) ;

    import p::* ;
    import q:: teeth_t ;

    teeth_t myteeth ;
    bool_t my_bool ;

    test2 I(in, out) ;

    initial
    begin
        my_bool = FALSE ; // p::FALSE
        $display("mybool = %d", my_bool) ;
    end

    assign out = in ;

endmodule: test

module test2(input in, output out) ;

    import p::* ;
    import q::teeth_t, q::ORIGINAL, q::FALSE ;
    teeth_t myteeth ;

    initial myteeth = FALSE ;
    
    assign out = in ;

endmodule

