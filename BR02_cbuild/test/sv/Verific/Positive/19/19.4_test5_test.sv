
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $root scope can have procedural statements. These statements
// are executed only once. This design defines a function in the $root scope and calls
// that function from inside a module.

int fn;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    always@(posedge clk)
    begin
        out1 = fibo(in1);
        out2 = fibo(in2);
    end

    initial
    begin
        fn = fibo(10);
        $display("10 th Fibonachchi number is %d", fn);
    end

endmodule

function int fibo(int N);
    int f = 0, n = 1;

    for(int i=0; i<N; i++)
    begin
        int t = n;
        n += f;
        f = t;
    end

    return n;
endfunction

