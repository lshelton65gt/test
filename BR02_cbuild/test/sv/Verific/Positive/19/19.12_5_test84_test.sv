
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation with multiple unpacked dimensions.
// The selected dimension is a part selection using +: token. The formal also has
// unpacked dimensions and is a multibit byte type.

module test(input in, output out) ;

    byte C [3:0][1:0] ;

    bot b1[1:0] (C[1+:2]) ;

    assign out = in ;

endmodule

module bot(input byte i [1:0]) ;
    initial
        $monitor($time,,,"i[1]=%b, i[0]=%b", i[1], i[0]) ;
endmodule

