
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation with single packed and unpacked
// dimensions. The selection for the array instantiation should be in the unpacked
// dimension. The formal does not have a dimension but the type is byte.

module test(input in, output out) ;

    reg [7:0] C [1:0] ;

    bot b1[1:0] (C) ;
    
    assign out = in + 1 ;

endmodule

module bot(input byte i) ;
    initial
        $monitor($time,,,"i=%b", i) ;
endmodule

