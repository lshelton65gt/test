
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation using actual having unpacked
// dimension. The actual is multibit byte type. The formal is bit type but uses
// packed dimensions. The unpacked dimension is selected by the array instantiation.

module test(input in, output out) ;
    byte C [1:0] ;

    bot b1[1:0] (C) ;
    assign out = in ;

endmodule

module bot(input [7:0] i) ;
    initial
        $monitor($time,,,"i[1]=%b, i[0]=%b", i[1], i[0]) ;
endmodule

