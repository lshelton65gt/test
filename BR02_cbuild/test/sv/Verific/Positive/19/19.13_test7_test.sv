
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE NEGATIVE TESTCASE

// TESTING FEATURE(S): This design tests the cases of name conflicts that SystemVerilog
// allows. But it defines an interface in the global scope. The interface
// is instantiated in the global scope but at the end of the source
// text with the same name as a But the module instantiation in compilation scope are not 
// allowed. Hence the error.

interface myInterface;
    int a;
endinterface

module test;

    int a;

    assign a = 10;

endmodule

myInterface test();

module bench;

initial
    $monitor(" test.a = %d", test.a);

    test t1 () ;

endmodule

