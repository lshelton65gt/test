
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation where the actual is of type integer
// and has an unpacked dimension. The formal is also of type integer.

module test(input byte in, output out) ;
    integer C[1:0] ;

    initial begin
        #1 C[1] = 0 ;
        C[0] = 1 ;
    end

    bot b1[1:0] (C) ;

    assign out = &in ;

endmodule

module bot (input integer C) ;
    initial
        $monitor($time,, "%m %d \n", C) ;
endmodule

