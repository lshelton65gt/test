
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation where the actual is of type enum.
// The unpacked array dimension is selected for the array instantiation.

typedef enum {red, yellow, blue, green, white} colors ;

module test(input in1, in2, output out) ;

    colors C[1:0] ;

    initial begin
        #1 C[1] = red ;
        C[0] = green ;
    end

    bot b1[1:0] (C) ;

    assign out = in1 & in2 ;

endmodule

module bot (input colors C) ;
    initial
        $monitor($time,, "%m %s \n", C.name) ;
endmodule

