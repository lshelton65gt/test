
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation where both the formal and the
// actual has same number of packed dimensions. The array selection will be in the
// unpacked dimension of the actual connected.

module test(input int in, output int out) ;

    reg [15:0][1:0] C [1:0] ;

    bot b1[1:0] (C) ;

    assign out = in ;

endmodule

module bot(input [15:0][1:0] i) ;
    initial
        $display($time,,,"i = %d", i) ;
endmodule

