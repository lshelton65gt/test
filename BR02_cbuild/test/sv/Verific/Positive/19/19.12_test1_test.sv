
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design connects an inout port of type wire of an
// instantiation to a logic type variable.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2) ;

    logic [width-1:0] r1, r2 ;

    always@(posedge clk)
    begin
        out1 = r1<<width/2 - r2 ;
        out2 = r1 + r2>>width/2 ;
    end

    bot #(width) bot_1 (clk, r1, r2) ;

    module bot #(parameter width=4)
                (input clk,
                 inout wire [0:width-1] io1, io2) ;

        wire [width-1:0] t1, t2 ;

        assign t1 = io1 - io2 ;
        assign t2 = io2 - io1 ;

        assign io1 = t1 | t2 ;
        assign io2 = t2 ^ t1 ;

        initial
            $display("Module bot instantiated") ;

    endmodule

endmodule

