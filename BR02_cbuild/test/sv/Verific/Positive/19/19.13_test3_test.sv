
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the cases of name conflicts that SystemVerilog
// allows. It defines a new structure type using typedef in the global
// scope. A data element of that type is defined in the global area with
// the same name as a module.

typedef struct {
    int a;
} myType;

myType test;

module test;

    int a;

    assign a = 10;

endmodule

module bench;

initial
    $monitor(" test.a = %d", test.a);

    test t1() ;

endmodule

