
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation inside generate-for with unpacked
// dimensions. The formal is byte type and also have unpacked dimensions.

module test(input in, output out) ;

    byte C [3:0][1:0] ;

    genvar i ;

    generate
        for(i=2; i<3; i++)
        begin: for_gen_i
            bot b1[1:0] (C[3:2]) ;
        end
    endgenerate

    assign out = in + 1 ;

endmodule

module bot(input byte i[1:0]) ;
    initial
        $display("i[1]=%d, i[0]=%d", i[1], i[0]) ;
endmodule

