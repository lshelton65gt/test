
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation with multiple unpacked dimensions.
// The formal is of type byte and has unpacked dimensions too.

module test(input in, output out) ;
    reg [7:0] C [1:0][1:0][3:0][1:0][1:0] ;

    bot b1[1:0] (C[1][0][2:1]) ;

    assign out = ~in ;
endmodule

module bot(input reg [7:0] i [1:0][1:0]) ;
    initial
        $monitor($time,,,"i[1][0]=%b, i[0][0]=%b, i[0][0]=%b, i[0][1]=%b", i[1][0], i[0][0], i[0][0], i[0][1]) ;
endmodule

