
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses enumeration type object and enumeration  array
// as actual to instance array of module having enumeration type ports.

typedef enum {red = 10, yellow, blue, green, white} colors ;
typedef enum {add, minus, divide, multiply} operator ;

module test ;
    colors C1 ;
    operator op[1:0] ;
    int res[1:0] ;

    initial begin
        C1 = green ;
        op[1] = multiply ;
        op[0] = minus ;
    end

    bot #(blue) b1[1:0] (C1, op, res) ;

    initial
       $monitor("res[0] = %d, res[1] = %d", res[0], res[1]) ;

endmodule
module bot(input colors C, input operator op, output int res ) ;
    parameter p = -2 ;

    always @(op)
    case(op) 
      add : res = p + C ;
      minus : res = p - C ;
      divide : res = p / C ;
      multiply : res = p * C ;
    endcase

endmodule

