
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Same identifier names can be used for more than one
// declaration, provided no two of them are in the same scope. This design
// defines more than one identifier with same name, but they are in different
// scopes.

int same_var_name;    // global $root scope

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output [width-1:0] out1,
              output reg [width-1:0] out2);

    int same_var_name;    // local module scope

    always@(posedge clk)
    begin
        int same_var_name;    // local block scope
        assert(same_var_name == 500);

        same_var_name = in1 ^ in2;
        out2 = same_var_name - in1 | in2;
        same_var_name = avg(400, 600);
    end

    assign out1 = same_var_name + in1;

endmodule

function int avg(int a, b);
    return (a+b)<<1;
endfunction

