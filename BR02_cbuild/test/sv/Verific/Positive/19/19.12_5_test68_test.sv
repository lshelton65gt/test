
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation. The formal is single bit type having packed
// dimension of size 8. The actual connected is byte type and has an unpacked dimension. The
// unpacked dimension is selected for the array instantiation and byte is equivalent to the
// formal size 8.

module test(input in, output out) ;

    byte C [5:4] ;

    initial begin
        #1 C[5] = 8'b01010011 ;
           C[4] = 8'b10100000 ;
    end

    bot b[1:0] (C) ;

    assign out = in ;

endmodule

module bot (input [7:0] C) ;
    initial
        #5 $display($time,,"%m %b\n", C) ;
endmodule
