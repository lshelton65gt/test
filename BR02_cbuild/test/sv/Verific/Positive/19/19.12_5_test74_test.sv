
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation with variables having both packed
// and unpacked dimensions. The formal too have both packed and unpacked dimensions.

module test(input in, output out) ;
    reg [15:0][1:0] C [1:0][1:0] ;

    bot b1[1:0] (C) ;

    assign out = ~in ;

endmodule

module bot(input [15:0][1:0] i [1:0]) ;
    initial
        $monitor($time,,,"i[1]=%b, i[0]=%b", i[1], i[0]) ;
endmodule

