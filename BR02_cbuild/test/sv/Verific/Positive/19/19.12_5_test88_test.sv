
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation using multiple unpacked dimension select.
// The actual has both packed and unpacked dimensions. The formal also has unpacked dimensions.

module test(input int in, output out) ;

    reg [7:0] C [1:0][1:0][1:0][2:1][3:2][4:3] ;

    bot b1[1:0] (C[1][0][1]) ;

    assign out = $onehot(in) ;

endmodule

module bot(input reg [7:0] i [1:0][1:0]) ;
    initial
        $monitor($time,,,"i[1][0]=%b, i[0][0]=%b, i[0][0]=%b, i[0][1]=%b", i[1][0], i[0][0], i[0][0], i[0][1]) ;
endmodule

