
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with one variable of type 
// byte having two unpacked dimensions for formal of type byte having one unpacked
// dimension. The port connection is a normal part select.

module test(input in, output out) ;
    byte C [3:0][1:0] ;

    bot b1[1:0] (C[2:1]) ;
    assign out = in ;

endmodule

module bot(input byte i [1:0]) ;
    initial
        $display("Instantiating module bot") ;
endmodule

