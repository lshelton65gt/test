// Carbon modified to remove initializers in task.
// compilation scope declaration of fn is 
// unsupported.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $root scope can have procedural statements. These statements
// are executed only once. This design defines a task in the $root scope and invokes
// that task from the module.

int fn;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);


       always@(posedge clk)
      begin
        fibo(10, fn);
        $display("10 th Fibonachchi number is %d", fn);
        fibo(in1, out1);
        $display("Fibonachchi number for %x is %d", in1, out1);
        fibo(in2, out2);
        $display("Fibonachchi number for %x is %d", in2, out2);
    end

endmodule

task fibo(input int N, output int F);
   int f,n;
   f = 0;
   n = 1; 
    for(int i=0; i<N; i++)
    begin
       int t;
        t = n;
        n += f;
        f = t;
    end

    F = n;
endtask


