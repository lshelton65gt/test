
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of timeprecision with timeunit.

module test(output int test_in);

    timeunit 100ns;
    timeprecision 100ps;

    initial #10.1132 test_in = 1;

endmodule

module bench;

    timeunit 100ns;
    timeprecision 100ps;

    int bench_in;

    test instan_test(bench_in);

    initial $monitor("Time = %f\tData = %d ", $time, bench_in);

endmodule

