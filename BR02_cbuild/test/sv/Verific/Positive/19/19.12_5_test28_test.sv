
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with a variable of type
// byte having two unpacked dimensions for formal of type byte having one unpacked
// dimension. The instantiation is inside a for-generate loop. The port connection
// is a -: range.

module test(input int in, output out) ;

    byte C [3:0][1:0] ;

    genvar i ;

    generate
        for(i=2; i<3; i++)
        begin: for_gen_i
            dummy D[1:0] (C[2-:i]) ;
        end
    endgenerate

    assign out = $onehot(in) ;

endmodule

module dummy(input byte i [1:0]) ;
    initial $display("Instantiating module dummy") ;
endmodule

