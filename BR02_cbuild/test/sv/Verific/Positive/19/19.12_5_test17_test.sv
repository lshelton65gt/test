
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface and it is instantiated inside
// a nested module. From top level module this interface is accessed hierarchically
// and pass part select of that as actual to array instance. The port of the 
// instantiated module is an unpacked array of generic interface.

interface combination(input reg clk) ;
    reg r ;
    int num ;
endinterface

module test(interface a[1:0]);
    always @(a[0].clk) begin
        a[0].r = 1001 ;
        a[0].num = a[0].r || a[0].clk ;     
    end
endmodule

module hier #(parameter p = 4);

    module lowest #(parameter size = 1) ;
        reg clock ;

        combination com[size:0] (clock) ;
    endmodule

    lowest #(p) lo() ;
   
endmodule

module bench ;

    hier #(2) check() ;

    test t1[1:0](check.lo.com[2:1]) ;
    test_slave ts[1:0](check.lo.com[1:0]) ;

    initial begin
        check.lo.clock = 0 ;
        #100 $finish ;
    end

    always  
        #5 check.lo.clock = ~check.lo.clock ;

    initial
        $monitor("t1[1].a[0].r=%b, t1[1].a[0].num=%d, t1[0].a[0].r=%b, t1[0].a[0].num=%d ts[1].b[0].r=%b, ts[1].b[0].num=%d, ts[0].b[0].r=%b, ts[0].b[0].num=%d", t1[1].a[0].r, t1[1].a[0].num, t1[0].a[0].r, t1[1].a[0].num, ts[1].b[0].r, ts[1].b[0].num, ts[0].b[0].r, ts[1].b[0].num) ;
    
    initial
        #1001 $finish ;

endmodule

module test_slave(interface b[1:0]) ;

    always @(b[0].num)
        b[0].r = b[0].num ;

endmodule

