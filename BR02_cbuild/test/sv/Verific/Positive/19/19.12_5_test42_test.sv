
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with a variable of type
// byte having two unpacked dimensions for formal of type byte having one unpacked
// dimension. The port connection is a -: range inside a for-generate loop.

module test(input int in, output out) ;
    byte C [3:0][1:0] ;

    genvar i ;

    generate
        for(i=2; i<3; i++)
        begin: for_gen_i
            bot b1[1:0] (C[2-:i]) ;
        end
    endgenerate

    assign out = in ^ 1 ;

endmodule

module bot(input byte i [1:0]) ;
    initial
        $display("Instantiating bot") ;
endmodule

