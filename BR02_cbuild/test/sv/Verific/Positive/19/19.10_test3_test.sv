
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If the outer module has a timeunit declaration and the inner
// module does not have one then the timeunit of the parent is used as the timeunit
// of the current module.

module test;

    timeunit 10ns;

    int a;

    module nested_mod;

        initial
        begin
	    a = 10;
            repeat(5)
                #5 a++;
            $monitor($time,,, "data (base 100ms) = %d", a);
        end

    endmodule

    nested_mod n_mod();

    initial
    begin
        a = 20;
        repeat(5)
            #5 a++;
        $monitor($time,,, "data (base 10ns) = %d", a);
    end

endmodule

