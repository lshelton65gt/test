
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design connects ports of an instantiation using named
// port connection rule. The specialty is that it connects ports of type event
// using named port connection.

module test #(parameter width=8)
             (input clk,
              input event evt1, evt2,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    logic [width-1:0] l1, l2;
    bit [0:width-1] b1, b2;

    test_bot #(width) bot_1 (.out1(l1), .in1(b1), .clk(clk), .evt(evt1));
    test_bot #(width) bot_2 (.evt(evt2), .out1(l2), .in1(b2), .clk(clk));

    always@(negedge clk)
    begin
        l1 = in1 & in2;
        l2 = in1 - in2;
    end

    module test_bot #(parameter w = 4)
                     (input clk,
                      input event evt,
                      input bit [w-1:0] in1,
                      output logic [0:w-1] out1);
       event data ;         

        always_ff@(posedge clk)
        begin
            data = evt ;
            out1 = ~in1;
        end

    endmodule

endmodule

