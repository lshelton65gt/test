
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines nested modules and defines tasks/functions
// within the modules. These tasks and functions are called using hierarchical names
// where the intended function/task is not in the current scope.

module test(clk, in1, in2, out1, out2);

    input clk, in1, in2;
    output out1, out2;

    task f;
	    input in1, in2;
	    output out;

	    out = in1 | in2 ;
	    return;
    endtask

	m1 i1 (clk, in1, in2, out1, out2);

	module m1 (input reg clk, in1, in2, output reg out1, out2);

		always @ (posedge clk)
		begin
			f (out1, in1, in2); // function call
			test.f (in1, in2, out2); // task call
		end

		function void f;
			output out1;
			input in1, in2;
			reg t;

			out1 = in1 & in2 ;
			return;
		endfunction
	endmodule

endmodule

