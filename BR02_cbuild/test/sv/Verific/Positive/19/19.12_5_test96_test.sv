
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation where both formal and actual are
// of type byte. The actual has multiple (three) unapcked dimensions. The
// formal has a single unpacked dimension. The actual connected is selected in one
// dimension and the other one is selected for the array instantiation whereas the
// remaining will be connected to the unpacked dimension of the formal.

module test(input in, output out) ;

    byte C [1:0][2:1][1:0] ;

    bot b1[1:0] (C[1]) ;

endmodule

module bot(input byte i [1:0]) ;
    initial
        $display($time,,,"i[1]=%b, i[0]=%b, i[0]=%b, i[0]=%b", i[1], i[0], i[0], i[0]) ;
endmodule

