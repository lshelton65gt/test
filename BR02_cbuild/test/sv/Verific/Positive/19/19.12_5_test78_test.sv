
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation with multiple unpacked dimensions.
// The formal is of type byte and has unpacked dimensions too.

module test(input in, output out) ;

    reg [7:0] C [1:0][1:0][3:0][1:0][1:0] ;
    int i,j,k,l,m ;
    initial
    begin
        for (i=0; i<2; i++)
            for (j=0; j<2; j++)
                for (k=0; k<4; k++)
                    for (l=0; l<2; l++)
                       for (m=0; m<2; m++)
                          C[i][j][k][l][m] = i + j + k + l + m ;
    end

    bot b1[1:0] (C[1][0][2-:2]) ;

    assign out = in ;

endmodule

module bot(input reg [7:0] i [1:0][1:0]) ;
    initial
        $monitor($time,,,"i[1][0]=%b, i[0][0]=%b, i[0][0]=%b, i[0][1]=%b", i[1][0], i[0][0], i[0][0], i[0][1]) ;
endmodule

