
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the cases of name conflicts that SystemVerilog
// allows. It defines a new structure type using typedef in the global
// scope. It declares a data element in the module and assigns value
// to it through simple name and hierarchical naming.

typedef struct {
    int a;
} myType;

module test;

    wire a;

    assign a = 10;
    assign test.a = 100;

endmodule

myType test;

module bench;

initial
    $monitor(" test.a = %d", test.a);

    test t1() ;

endmodule

