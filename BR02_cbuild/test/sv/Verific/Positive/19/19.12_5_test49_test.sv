
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with actual of type byte 
// having one unpacked dimension.

module test(input in, output int out) ;

    byte C [5:4] ;

    initial begin
        #1 C[5] = 8'b01010011 ;
           C[4] = 8'b10100000 ;
    end

    bot b[1:0] (C) ;

    assign out = {32{in}} ;

endmodule

module bot (input [7:0] C) ;
    initial
        #5 $display($time,,"%m %b\n", C) ;
endmodule

