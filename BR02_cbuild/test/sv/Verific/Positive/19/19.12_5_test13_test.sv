
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design instantiates an interface array inside a module.
// This instance array is accessed hierarchically from another module and is used
// as actual to instance array of modules having unpacked array of interfaces as port.

interface combination(input reg clk) ;
    reg r ;
    int num ;
endinterface

module test(combination a[2:0]) ;
    always @(a[0].clk)
    begin
       a[0].r = 1001 ;
       a[0].num = a[0].r || a[0].clk ;     
    end
endmodule

module hier ;

   reg clock ;
   combination com[5:0] (clock) ;
   
endmodule

module bench ;

   hier check() ;

   test t1[1:0](check.com[5:3]) ;
   test_slave ts[1:0](check.com[3:5]) ;

   initial
   begin
      check.clock = 0 ;
      #100 $finish ;
   end

   always  
      #5 check.clock = ~check.clock ;

    initial
        $monitor("t1[1].a[0].r=%b, t1[1].a[0].num=%d, t1[0].a[0].r=%b, t1[0].a[0].num=%d ts[1].b[0].r=%b, ts[1].b[0].num=%d, ts[0].b[0].r=%b, ts[0].b[0].num=%d", t1[1].a[0].r, t1[1].a[0].num, t1[0].a[0].r, t1[1].a[0].num, ts[1].b[0].r, ts[1].b[0].num, ts[0].b[0].r, ts[1].b[0].num) ;
    
    initial
        #1001 $finish ;

endmodule

module test_slave(combination b[2:0]) ;

   always @(b[0].num)
      b[0].r = b[0].num ;

endmodule

