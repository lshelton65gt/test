
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an unpacked array of class object as actual 
// to an array instance. The port of the instantiated module is an unpacked array of class.

class Student ;
    string name ;
    string addr ;
    int roll ;

    function new(string n, string a, int r) ;
        name = n ; 
        addr = a ;
        roll = r ;
    endfunction

    function void show() ;
        $display("%s, %s, %d", name, addr, roll) ;
    endfunction
endclass

module test ;
    Student s[2:0] ;
    initial begin
       s[2] = new("asd","fgh",09) ;
       s[1] = new("qwe","asd",12) ;
       s[0] = new("rty","fgh",34) ;
    end
    bot b1[1:0] (s) ;
endmodule

module bot(input Student s1[2:0]) ;
    initial begin
        s1[0].show() ;
        s1[1].show() ;
        s1[2].show() ;
     end
endmodule

