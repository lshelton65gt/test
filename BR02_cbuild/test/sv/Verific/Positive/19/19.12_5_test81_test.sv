
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array instantiation inside for-generate with multiple
// packed and unpacked dimension. The selected dimension is a part selection using +:
// token. The formal also has unpacked dimensions and is a multibit byte type.

module test(input [0:7] in, output out) ;

    reg [7:0] C [1:0][1:0][3:0][1:0][1:0] ;

    initial
    begin
        int i, j, k, l, m ;
        for (i=0; i<2; i++)
            for (j=0; j<2; j++)
                for (k=0; k<4; k++)
                    for (l=0; l<2; l++)
                       for (m=0; m<2; m++)
                          C[i][j][k][l][m] = "A" + i ;
    end

    genvar i ;

    generate
        for(i=0; i<1; i++)
        begin: for_gen_i
            bot b1[1:0] (C[1][0][1:0]) ;
        end
    endgenerate

    assign out = $onehot0(in) ;

endmodule

module bot(input reg [7:0] i [1:0][1:0]) ;
    initial
        $monitor($time,,,"i[1][0]=%b, i[0][0]=%b, i[0][0]=%b, i[0][1]=%b", i[1][0], i[0][0], i[0][0], i[0][1]) ;
endmodule

