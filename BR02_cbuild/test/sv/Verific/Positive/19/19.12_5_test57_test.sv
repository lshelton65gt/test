
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance with one unpacked array of
// type enum. The instantiated module contains enum type port.

typedef enum {red, yellow, blue, green, white} colors ;

module test(input colors in, output int out) ;
    colors C[1:0] ;

    initial begin
        #1 C[1] = red ;
        C[0] = green ;
    end

    bot b1[1:0] (C) ;

    assign out = in + 1 ;

endmodule

module bot (input colors C) ;
    initial
        $monitor($time,, "%m %s \n", C.name) ;
endmodule

