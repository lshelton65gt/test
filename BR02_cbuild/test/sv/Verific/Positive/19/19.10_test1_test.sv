
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If the outer module does not have any timeunit declarations
// then $root scope is searched, if $root also does not have any timeunit declaration
// then it is set to the simulator's default timeunit. This design defines a module,
// for which no timeunit is specified inside it or in the $root scope. Another module
// is declared inside the previous module with a timeunit declaration. So the outer
// module will use the simulator's default timeunit, while the inner will use its own.

module test;

    int a;

    module nested_mod;

        timeunit 10ns;

        initial
        begin
	    a = 10;
            repeat(5)
                #5 a++;
            $monitor($time,,, "data (base 100ms) = %d", a);
        end

    endmodule

    nested_mod n_mod();

    initial
    begin
        a = 20;
        repeat(5)
            #5 a++;
        $monitor($time,,, "data (base 10ns) = %d", a);
    end

endmodule

