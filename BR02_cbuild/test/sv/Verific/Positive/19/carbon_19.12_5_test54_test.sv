// Carbon modified to use $display instead of $monitor

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks array instance passing one dimensional 
// unpacked array of integer as actual for integer type formal.

module test(input in, output out) ;
    integer C[1:0] ;

    initial begin
        #1 C[1] = 2 ;
        C[0] = 1 ;
    end

    bot b1[1:0] (C) ;
    assign out = ~in ;

endmodule

module bot (input integer C) ;
    always @(C)
        $display("%m %d \n", C) ;
endmodule

