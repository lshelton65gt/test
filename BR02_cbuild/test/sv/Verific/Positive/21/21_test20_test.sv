
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test case to change unbound instance's module name using
// use expansion clause

library lib1 "test.v" ;
library cLib "true*.v" ;
library mLib "mux*.v" ;

config cfg;
     design lib1.top ;
     instance top.I1 use cLib.true_complement ; // bind with module true_complement
     instance top.I2 use mLib.mux2_1 ; // bind with module mux2_1
endconfig

module top(output Y1, Y2, input A, L, M) ;

    arith_func I1(Y1, A, L, M) ; // behave as true/complement
    arith_func I2(Y2, A, L, M) ; // behave as 2 to 1 mux

endmodule

