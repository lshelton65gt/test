
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S):  To bind an instance, if a most specific binding expansion
// clause and a liblist clause both exists, specific binding clause takes precedence.
// Test the above using instance clause.

library lib1 "test.v" ;
library lib2 "file1.v" ;
library lib3 "file2.v" ;
library lib4 "file3.v" ;

config cfg ;
    design lib1.top ;
    instance top.I1 liblist lib3 lib4 ;
    instance top.I1 use lib4.comm_int ; // use expansion clause most specific. Instance
                                        // top.I1 shall use lib4.comm_int
    instance top.I2 use lib2.transMod ;
    instance top.I3 use lib2.receiveMod ;
endconfig 
    
module top(input in, output out) ;

    logic clk = 0 ;
    logic reset = 1;
    comm_int I1(clk) ;

    initial
    begin
        #5 reset = 0 ;
        repeat(20)
            #10 clk ++ ;
    end

    transMod I2(I1.tx, reset) ;
    receiveMod I3(I1.rx, reset) ;

    assign out = in ;

endmodule

