
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S):  Use of library declaration clause to map design units in
// different libraries.

library rtlLib "lib1/*.v" ;
library gateLib "lib2/andg.v" ;
library gateLib "lib2/*.v" ;

module top(output out1, out2, out3, 
           input in1, in2) ;

    andg I1(out1, in1, in2) ;
    org  I2(out2, in1, in2) ;
    xorg I3(out3, in1, in2) ;

endmodule

