
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If a file does not match any library's file path specification,
// that file will be compiled into a library named work.

library lib1 "./andg.v" ; // module andg of file andg.v will
                          // be mapped to library lib1.

                          // module org of file org.v will by
                          // default be mapped to library work

module top (output out1, out2, input in1, in2) ;

    andg I1(out1, in1, in2) ;
    org I2(out2, in1, in2) ;

endmodule

