
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of hierarchical configuration. Use configuration to
// bind instance I1.

library lib1 "test.v" ;
library lib2 "volt.v" ;
library lib3 "current.v" ;
library hLib "hparams.v" ;

module two_port_network(output real v1, i2,
                        input real i1, v2) ;

    voltMod I0(v1, i1, v2) ;
    currentMod I1(i2, i1, v2) ;

endmodule

config cfg ;
    design two_port_network ;
    instance two_port_network.I0 use lib2.voltMod ; // bind with module voltMod not config
    instance two_port_network.I1 use lib3.cfg_cMod:config ;
    instance two_port_network.I0.I1_1 use hLib.h11_gen ;
    instance two_port_network.I0.I1_2 use hLib.h12_gen ;
endconfig
 
