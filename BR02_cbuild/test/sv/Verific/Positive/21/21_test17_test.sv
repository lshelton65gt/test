
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of instance selection clause with liblist expansion
// clause. Liblist expansion clause contains a single library name.

library aLib "test.v" ;
library bLib "sub.v" ;

config cfg ;
    design aLib.top ;
    instance top.I1 liblist bLib; // will be bound with bLib.child 
endconfig

module top(cout, sum, in1, in2) ;
    parameter SIZE = 4 ;
    
    output cout ;
    output [SIZE-1:0] sum ; 
    input [SIZE-1:0] in1, in2 ;
    
    child I1(cout, sum, in1, in2) ;

endmodule

// Implements adder
module child(cout, sum, in1, in2) ;

    parameter SIZE = 4 ;
    
    output cout ;
    output [SIZE-1:0] sum ; 
    input [SIZE-1:0] in1, in2 ;

    assign {cout, sum} = in1 + in2 ;

    initial
        $display("Instantiating module child") ;

endmodule

