
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of design statement where the identified design has
// the same name as config.

library aLib "test.v" ;
library bLib "top2.v" ;
library rtlLib "gates.v" ;
library gateLib "gates.vg" ;

config MUX4_1 ;
    design aLib.MUX4_1;
    default liblist gateLib rtlLib;
endconfig

config MUX2_1;
    design bLib.MUX2_1;
    default liblist rtlLib gateLib ;
endconfig

module MUX4_1(input [3:0] D, input [1:0] S, output Y) ;

    wire S0_bar, S1_bar ;
    assign S0_bar = ~S[0],
           S1_bar = ~S[1] ;

    wire [3:0] and_out ;
    
    andg I0(and_out[0], S1_bar, S0_bar) ;
    andg I1(and_out[1], S[1], S0_bar) ;
    andg I2(and_out[2], S1_bar, S[0]) ;
    andg I3(and_out[3], S[1], S[0]) ;
    or (Y, and_out[0], and_out[1], and_out[2], and_out[3]) ;

    initial
        $display("Instantiating module MUX4_1") ;

endmodule

