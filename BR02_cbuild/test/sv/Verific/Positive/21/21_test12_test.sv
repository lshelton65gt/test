
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test case to change the module name of unbound instance
// using cell clause

library lib1 "test.v" ;
library cLib "true*.v" ;
library mLib "mux*.v" ;

config cfg;
     design lib1.test ;
     cell lib1.mux2_1 use cLib.true_complement ; // master name changed from
                                                 // mux2_1 to true_complement 
     cell lib1.true_complement use mLib.mux2_1 ; 
     // master name changed from true_complement to mux2_1

endconfig

module test(output Y1, Y2, input A, L, M) ;

    mux2_1 I1(Y1, A, L, M) ; 
    true_complement I2(Y2, A, L, M) ; 

endmodule

