
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S):  Test of instance selection clause with liblist expansion
// clause. Liblist expansion clause contains multiple library names.

library aLib "test.v" ;
library bLib "sub.v" ;
library cLib "add.v" ;

config cfg ;
    design aLib.top ;
    instance top.I1 liblist cLib bLib; // will be bound with cLib.child 
endconfig

module top(cout, sum, in1, in2) ;
    parameter SIZE = 4 ;
    
    output cout ;
    output [SIZE-1:0] sum ; 
    input [SIZE-1:0] in1, in2 ;
    
    child I1(cout, sum, in1, in2) ;

endmodule

