
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of default selection clause with empty liblist expansion
// clause. Instances of module andg shall be bound with aLib.andg and instances of
// module org shall be bound with aLib.org.

library aLib "test.v" ;
library aLib "gates.v" ;
library gateLib "gates.vg" ;

config cfg1 ;
    design MUX2_1 ;
    default liblist;
endconfig

module MUX2_1(input [1:0] D, input S, output Y) ;

    wire S_bar ;
    assign S_bar = ~S;

    wire and_out0, and_out1 ;

    andg I0(and_out0, S_bar, D[0]) ;
    andg I1(and_out1, S, D[1]) ;
    org  I3(Y, and_out0, and_out1) ;

endmodule

