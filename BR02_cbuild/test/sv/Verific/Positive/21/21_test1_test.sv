
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of cell selection clause with optional library name.

library lib1 "test.v" ;
library cLib "true*.v" ;
library mLib "mux*.v" ;

config cfg;
     design lib1.test ;
     cell lib1.true_complement use cLib.true_complement ; 
     cell lib1.mux2_1 use mLib.mux2_1 ; 
endconfig

module test(output Y1, Y2, input A, L, M) ;

    mux2_1 I1(Y1, A, L, M) ; 
    true_complement I2(Y2, A, L, M) ; 

endmodule

