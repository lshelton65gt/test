
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of file path resolution in library declaration and use
// of ".." to specify parent directory.

library lib1 "lib/rtl/g*.v" ;
library lib2 "lib/rtl/" ;
library lib3 "lib/../lib/rtl/gates.v" ; // module andg and org of file lib/rtl/gates.v
                                        // will be mapped to library lib3

module top(output out1, out2, input in1, in2) ;

    andg I1(out1, in1, in2) ;
    org I2(out2, in1, in2) ;

endmodule

