
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test for use expansion clause where library name is omitted.
// In config rule 'instance top.I1 use comm_int' library name is omitted in use clause.
// So, instance top.I1 shall use lib1.comm_int as lib1 is the library of parent cell "top" . 

library lib1 "test.v" ;
library lib2 "file1.v" ;
library lib1 "file2.v" ;

config cfg ;
    design lib1.top ;
    instance top.I1 use comm_int ; 
    instance top.I2 liblist lib2 ;
    instance top.I3 liblist lib2 ;
endconfig 

module top(input in, output out) ;
    logic clk = 0 ;
    logic reset = 1;
    comm_int I1(clk) ;

    initial
    begin
       #5 reset = 0 ;
       repeat(20)
           #10 clk++ ;
    end

    transMod I2(I1.tx, reset) ;
    receiveMod I3(I1.rx, reset) ;

    assign out = in ;

endmodule

