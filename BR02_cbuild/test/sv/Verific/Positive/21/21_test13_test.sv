
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of default configuration which says without configuration
// the libraries are searched according to the library declaration order in $root.
// For the following design all instances of module full_adder shall use lib2.full_adder
// and all instances of module half_adder shall use lib1.half_adder.

library lib1 "test.v";
library lib2 "adder.*" ;
library lib3 "adder.vg" ;

module BinAdder(output cout, output [3:0] sum,
                input [3:0] A, B, C, input Cin) ;

    wire C0, C1, C2;

    full_adder f0(C0, sum[0], A[0], B[0], Cin) ;
    full_adder f1(C1, sum[1], A[1], B[1], C0) ;
    full_adder f2(C2, sum[2], A[2], B[2], C1) ;
    full_adder f3(cout, sum[3], A[3], B[3], C2) ;

endmodule

module half_adder(output reg C, S, input A, B) ;

    always @(A, B)    
    begin
        if(A != B)
            S = 1;
        else
            S = 0;
        C = A & B ;
    end

endmodule

