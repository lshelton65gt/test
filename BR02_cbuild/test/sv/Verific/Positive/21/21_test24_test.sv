
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If multiple cells with the same name map to the same library,
// then the LAST cell encountered shall be written to the library.

library rtlLib "./lib1/*.v" ;
library rtlLib "./lib1/andg.v" ;
library gateLib "./lib2/" ;
library rtlLib "./lib2/andg.v" ; // Overwrite module "andg" of file lib1/andg.v
                                 // with module "andg" of file lib2/andg.v  

module top(output out1, out2, out3, 
           input in1, in2) ;

    andg I1(out1, in1, in2) ;
    org  I2(out2, in1, in2) ;
    xorg I3(out3, in1, in2) ;

endmodule

