
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of cell selection clause where the library name is omitted
// from the cell name and expansion clause is liblist, specifying multiple libraries.

library aLib "test.v" ;
library rtlLib "mux.v" ;
library gateLib "mux.vg" ;

module mux4_1(input [3:0] x, input [1:0] sel, output y) ;

    wire mux_out0, mux_out1;

    mux2_1 I0 (mux_out0, x[0], x[1], sel[0]) ;
    mux2_1 I1 (mux_out1, x[2], x[3], sel[0]) ;
    mux2_1 I2 (y, mux_out0, mux_out1, sel[1]) ;

endmodule

config cfg ;
    design aLib.mux4_1 ;
    cell mux2_1 liblist aLib rtlLib gateLib; // mux2_1 shall use rtlLib.mux2_1
endconfig
 
