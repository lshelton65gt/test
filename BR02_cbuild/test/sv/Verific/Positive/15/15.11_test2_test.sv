
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): An existing clocking can be assigned as default clocking.
// This design tests this.

interface bus_A (input clk) ;
    logic [15:0] data ;
    logic write ;
    modport test (input clk, data, output write) ;
    modport dut (output data, input write) ;
endinterface 

interface bus_B (input clk) ;
    logic [8:1] cmd ;
    logic enable ;
    modport test (input clk, enable,output cmd) ;
    modport dut (output enable) ;
endinterface 

program prog( bus_A.test a, bus_B.test b ) ;
    clocking cd1 @(posedge a.clk) ;
        input data = a.data ;
        output write = a.write ;
    endclocking 

    clocking cd2 @(posedge b.clk) ;
        input #2 output #4ps cmd = b.cmd ;
        input enable = b.enable ;
    endclocking 
   
    default clocking cd1 ;

    initial
    begin
        cd1.write <= cd2.enable ;
        $monitor ("test.in=%b , test.out=%b", test.in, test.out) ;
    end
    initial
        #100 $finish ;
endprogram 

module test (input in, output out) ;

    assign out = in ;
    logic phi1, phi2 ;
    bus_A a(phi1) ;
    bus_B b(phi2) ;
    prog main( a, b ) ;

endmodule 

