
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests clocking block specifying interface's 
// port as its clocking signal.

interface bus_A (input clk);
    logic [15:0] data;
    logic write;
    modport test (input data, output write);
    modport dut (output data, input write);
endinterface 

module test;

    logic phi1 = 0 ;
    bus_A a(phi1);

    clocking cd1 @(posedge a.clk);
        input data = a.data;
        output write = a.write;
    endclocking 

    initial begin
        #5 phi1 = ~phi1 ;
        #20 $finish ;
    end

    always @ (cd1)
        $display("printing") ;
endmodule 

