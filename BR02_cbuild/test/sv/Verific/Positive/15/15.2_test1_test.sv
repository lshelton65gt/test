
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing use of 'clocking' declaration within 'program'.

program prog(   input phi1, input [15:0] data, output logic write,
                input phi2, inout [8:1] cmd, input enable
            ) ;
    reg [8:1] cmd_reg ; 

    clocking cd1 @(posedge phi1) ;
        input data ;
        output write ;
    endclocking 

    clocking cd2 @(posedge phi2) ;
        input #2 output #4 cmd ;
        input enable ;
    endclocking 

    initial
    begin 
        @ cd1 write <= cd2.enable ;
    end 

    assign cmd = enable ? cmd_reg: 'z ; 
endprogram 

module test(input in, output out) ;

    logic phi1, phi2 ;
    wire [8:1] cmd ; 
    logic [15:0] data ;
    logic enable ;
    prog main(phi1, data, write, phi2, cmd, enable) ;
    assign out = in + 1 ;
    initial
        #100 $display("phi1 =%b, data =%b, phi2 =%b, cmd =%b, enable =%b, write =%b", phi1, data, phi2, cmd, enable, write) ;
    
endmodule 

