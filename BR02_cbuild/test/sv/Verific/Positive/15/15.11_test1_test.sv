
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): One clocking can be specified as default for all cycle
// delay operations within a given program. This design tests this.

program prog( input bit clk, inout [15:0] data ) ;
    default clocking bus @(posedge clk) ;
        inout data ;
    endclocking 

    initial begin 
        $display("A program with default clocking") ;
        ## 5
        if ( bus.data > 10 )
           ;
        else 
           ;
    end 
endprogram 


module test(input in, output out) ;

    wire [15:0] data ;
    bit clk ;
    prog main(clk, data ) ;
    assign out = ~in ;

endmodule 

