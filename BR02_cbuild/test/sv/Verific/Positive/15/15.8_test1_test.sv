
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A clocking encapsulates a set of signals that share a common
// clock. This design uses interface type ports and defines interface declared 
// signals in clocking block.

interface bus_A (input clk) ;
    logic [15:0] data ;
    logic write ;
    modport test (input clk, data, output write) ;
    modport dut (output data, input write, clk) ;
endinterface

interface bus_B (input clk) ;
    logic [8:1] cmd ;
    logic enable ;
    modport test (input enable, clk, output cmd) ;
    modport dut (output enable) ;
endinterface

program prog( bus_A.test a, bus_B.test b ) ;
    clocking cd1 @(posedge a.clk) ;
        input data = a.data ;
        output write  = a.write ;
        inout state = test.cpu.state ;
    endclocking

    clocking cd2 @(posedge b.clk) ;
        input #2 output #4ps cmd = b.cmd ;
        input enable = b.enable ;
    endclocking

    initial
    begin
        cd1.write <= cd2.enable ;
        $display("test.in =%b, test.out =%b", test.in, test.out) ;
    end
endprogram

module test(input in, output out) ;

    logic phi1, phi2 ;
    bus_A a(phi1) ;
    bus_B b(phi2) ;
    prog main( a, b ) ;
    CPU cpu () ;
    assign out = in ;

endmodule

module CPU ;

    logic state ;

endmodule

