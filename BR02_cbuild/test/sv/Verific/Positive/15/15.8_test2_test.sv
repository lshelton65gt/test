
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests use of clocking block along with
// interface so that both clocking and interface can share the same clock.

interface bus_A (input clk) ;
    logic [15:0] data ;
    logic write ;
    modport test (input clk, data, output write) ;
endinterface 

interface bus_B (input clk) ;
    logic [8:1] cmd ;
    logic enable ;
    modport test (input clk, enable, output cmd) ;
endinterface 

program prog( bus_A.test a, bus_B.test b ) ;
    clocking cd1 @(posedge a.clk) ;
        input data = a.data ;
        output write = a.write ;
    endclocking 
    clocking cd2 @(posedge b.clk) ;
        input #2 output #4ps cmd = b.cmd ;
        input enable = b.enable ;
    endclocking 
    initial
    begin 
        cd1.write <= cd2.enable ;
        $display("a.data =%b, a.write =%b test.in = %b test.out = %b", a.data, a.write, test.in, test.out) ;
    end 
endprogram 

module test(input int in, output out) ;

    logic phi1 = 1 ;
    logic phi2 = 0 ;

    bus_A a(phi1) ;
    bus_B b(phi2) ;
    prog main( a, b ) ;

    assign out = $onehot(in) ;

endmodule 

