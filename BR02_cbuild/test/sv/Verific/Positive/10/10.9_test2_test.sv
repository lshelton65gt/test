
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog 'disable' statement can be used to disable a block other than
// the current block. This design uses 'disable' statement to disable another block.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    begin: for_loop1
        for(integer i=0; i<width; i++)
        begin
            r1[i] = in1[i] ^ in2[width-1-i];
            if (i==width/2)
                disable for_loop2;
            out1[i] = r1[i] | r2[i];
        end
    end

    out1[width/2:0] = in1[width/2:0] | ~in2[0:width/2];
end

always @(posedge clk)
begin
    begin: for_loop2
        for(integer i=0; i<width; i++)
        begin
            r2[i] = r1[i] + in1[i];
            if (i==width/2)
                disable for_loop1;
            out2[i] = r2[i] ^ r1[i];
        end
    end

    out2[width/2:0] = in1[width/2:0] ^ ~in2[0:width/2];
end

endmodule

