
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test final block.

module test( input int p, output int q) ;

    parameter period = 10 ;

    assign q = p ;

    final
    begin
        $display("Number of cycles executed %d", $time/period) ;
        $display(" finish called implicitly") ;
    end

endmodule

