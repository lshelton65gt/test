
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing 'break' and 'continue' with 'repeat' and 'forever' loops.

module test;
int i = 0;
initial 
begin
	repeat (10)
	begin
		i += 5;
		if (i <= 20)
			continue;
		else
		begin
			$display("%d", i);
			break;
		end
	end
	i = 10;
	forever 
	begin
		i++;
		if(i < 15)
			continue;
		if(i >= 15)
		begin
			$display("\n%d", i);
			break;
		end
	end
end
endmodule
