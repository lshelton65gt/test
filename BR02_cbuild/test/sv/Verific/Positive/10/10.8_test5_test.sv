
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing block name with `define macro.

`define name(a) a``One

module test;

initial
  begin:`name(blk)
      $display ("Hello");
  end:`name(blk)

endmodule


