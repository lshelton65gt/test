
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The 'repeat' loop might just contain a null statement.
// The following design checks this.

module test ;
    initial
        repeat (10)
            ;
endmodule
