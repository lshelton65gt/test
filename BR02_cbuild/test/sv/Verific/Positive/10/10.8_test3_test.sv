
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows a label to be specified before any statement
// following a colon. This label can be used to identify the statement so it can be used 
// to disable that statement using its label name. This design uses a disable statement
// to disable itself.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

int i;

always @(posedge clk)
begin
    r1 = { in1[0 +: width/2], in2[width-1 -: width/2] };
    r2 = in1 >> width/2 | in2 <<  width/2;

    if (r1 != r2)
        myself: disable myself;

    out1 = ((r1>r2)?(r2-r1):(r1-r2));
    out2 = ((r1[width-1] == r2[0])?(r1|r2):r1&r2);
end

endmodule

