
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests 'foreach' construct to iterate over
// array of string data type.

module test (input string str_arr[5:0], bit clk, output bit out);
    int no_os_str ;

    always @ (posedge clk)
    begin
        no_os_str = 0 ;
        foreach( str_arr[ k ] )
            no_os_str = no_os_str + 1 ;

        if (no_os_str == 6)
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

