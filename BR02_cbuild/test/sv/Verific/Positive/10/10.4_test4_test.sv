
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses 'unique' keyword in case statement to define 
// a state machine.

typedef enum [0:3] {
			b0=4'b0000, 
			s0=4'b0001, 
			s1=4'b0010,
			b1=4'b0011,
			s2=4'b0100,
			s3=4'b1000
			} ALL_STATES;

module test(clk,reset,in1,out1);
input clk,reset;
input [31:0] in1;
output [31:0] out1;
reg [31:0] out1;
ALL_STATES state;


     always @(posedge clk)
     begin
      if(reset)
      begin
        state = s0;
        out1 = 0;
      end
      else
	    begin
		assert($onehot(state))
        	unique case(state)
          		s0 : begin
                		out1 = in1;
                		state = s1;
               	 	end
          		s1 : begin
                		out1 = out1 + in1;
                		state = s2;
               	 	end
          		s2 : begin
                		out1 = out1 - in1;
                		state = s3;
               	 	end
          		s3 : begin
                		out1 = out1 + in1;
                		state = s0;
               	 	end
       		endcase
			// end assert true
	   else
	   	    $error("Illegal state");
	   end
  end
endmodule

