// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing a 'for' loop with a label.

module test (input in, output out) ;
int value ;
int a [100] ;

initial begin
    for ( int count = 0; count < 3; count++ )
        value = value +((a[count]) * (count+1));
    end
initial begin
    loop2 : for ( int count = 0, j = 0; j * count < 125; j++, count++)
        $display("Value j = %d\n", j );
    end
    assign out = in + value ;
endmodule

