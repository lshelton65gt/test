
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test case with a label, written by a macro.

`define myMac label1

module test;

int a;
initial
    `myMac: a = 10;

endmodule


module top;

   test instanMod1();

   initial
       $display("value = (should be 10) %d", instanMod1.a);

endmodule


