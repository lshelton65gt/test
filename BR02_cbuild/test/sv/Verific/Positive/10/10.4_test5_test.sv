
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If in a 'priority-if' statement none of the conditions are true
// then a run-time warning should be issued. This design checks that.

module test;
int i, j;
initial
begin
	i = 0;
	j = 0;
	priority if ( i > 0 )
		$display("i = %d", i);
	else if( i > 0 || j > 5 )
		$display("i = %d, j = %d", i, j);
end
endmodule
