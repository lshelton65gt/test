
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses pattern matching in 'if' statement.

  typedef union tagged {
      struct {
          bit [4:0] reg1, reg2, regd ;
      } Add ;
      union tagged {
          bit [9:0] JmpU ;
          struct {
              bit [1:0] cc ;
              bit [9:0] addr ;
          } JmpC ;
      } Jmp ;
  } Instr ;

module test(input Instr e, output bit [1:0] c1, output bit [9:0] addr1) ;

    Instr instr ;

    always @*
    begin
        if (e matches (tagged Jmp (tagged JmpC '{cc:.c, addr:.a})))
        begin
            c1 = c ;
            addr1 = a ; 
            $display("We have a match") ;
        end
        else
        begin
            c1 = 'z ;
            addr1 = 'z ;
            $display("The pattern does not match") ;
        end 
    end

endmodule 

