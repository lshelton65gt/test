
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog introduces 'iff' keyword with expression
// in event expression to add an enabling condition along with the event. This
// test case uses 'iff' to add clock enable in always block.

module test;
logic clk = 0;
logic enable = 0;
logic q = 0;
initial
begin
	repeat (10) 
	begin
 		#5 clk = ~clk;
		#10 enable = ~ enable;
	end
end

always @(clk iff enable == 1)
begin
	q = q+1;
	$display("q = %b\n", q);
end
endmodule

