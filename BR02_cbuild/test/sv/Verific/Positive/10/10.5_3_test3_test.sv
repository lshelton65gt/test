// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The number of loop variables in a 'foreach' construct
// must match the number of array variables. This design tests this.

module test (input in, output out) ;
    assign out = in ;
    always @ (posedge in) begin
        string words [2] = '{ "hello", "world" } ;
        int prod [1:8] [1:3] ;
        foreach( words [ j ] )
            $display( j , words[j] ) ;    // print each index and value 
        foreach( prod[ k, m ] )
        prod[k][m] = k * m;    // initialize
    end
endmodule

