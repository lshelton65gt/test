
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing 'unique case' statement.

module test;
bit [3:0] mod_bit;
initial
begin
	mod_bit = 4'b 0x0z;
	unique case(mod_bit)
		4'b 0x0? : $display("%b\n", mod_bit);
		4'b 0000 : $display("%b\n", mod_bit); 
		4'b 0x00 : $display("%b\n", mod_bit); 
		4'b 0x0z : $display("%b\n", mod_bit); 
	endcase
end
endmodule
