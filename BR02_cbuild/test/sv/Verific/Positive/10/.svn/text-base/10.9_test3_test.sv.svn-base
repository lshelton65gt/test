
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Disabling a task should stop execution of that task and return from it.
// This design defines a task and uses a 'disable' statement to disable it. 

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    ret_task(in1, in2, r1);

    disable ret_task;

    out1 = r1 + ~r2;
end

always @(posedge clk)
begin
    ret_task(in2, in1, r2);

    disable ret_task;

    out2 = ~r1 - r2;
end

task automatic ret_task(input int a, b, output int o);
    int t1, t2;

    t1 = a>>8;
    t2 = b<<8;

    o = t1 ^ t2;
endtask

endmodule

