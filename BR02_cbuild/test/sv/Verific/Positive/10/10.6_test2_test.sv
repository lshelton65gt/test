
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing 'break' and 'continue' statement with 'for' and 'while' loops. 

module test;
string mod_char = "H1e#llo";
logic flag = 0;
initial
begin
	while(1)
	begin
		for(int i = 0; 1; i++)
	        begin
			if (((mod_char[i] >= "A" && mod_char[i] <= "Z") ||  (mod_char[i] >= "a" && mod_char[i] <= "z"))) 
				continue;
			if(i == mod_char.len)
			begin		
				flag = 1;
                $display("%d", i);
				break;
			end
			$display("%c", mod_char[i]);
		end
		if(flag == 1)
			$display("%s", mod_char);
			break;
	end
end
endmodule
