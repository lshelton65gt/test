
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing local scope of the 'for' loop variable.

module test;
initial
begin
	for( int i = 0; i < 2; i++)
		$display("From the first 'for' loop: %d\t", i);
	for( int i = -1; i < 1; i ++)
		$display("From the second 'for' loop: %d\t", i);
end
endmodule

