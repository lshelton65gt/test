
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of matching block name after the block.

module test;
int i = 0;
initial
begin : block1
	logic clk = 0;
	repeat (10) #5 clk = ~clk;
end : block1
always @ (block1.clk)
begin
	i++;
	$display("%d\n", i);
end
endmodule
