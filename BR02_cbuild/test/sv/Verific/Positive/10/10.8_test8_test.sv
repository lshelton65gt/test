
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of a label before a statement.

module test;
bit [3:0] mod_bit = 4'b 0100;
always @(mod_bit)
label1: begin
	label2:	repeat(10)
		begin
			mod_bit = mod_bit+1;
			if(mod_bit > 4'b 1000)
			begin
				$display("%b", mod_bit);
				disable label2;
			end
		end
	end
endmodule
