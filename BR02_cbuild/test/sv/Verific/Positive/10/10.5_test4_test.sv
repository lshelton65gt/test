
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows the initial declaration or assignment
// statement to be one or more comma-separated statements in for-loop. The step
// assignment can also be one or more comma-separated assignment statements.


typedef struct{ int p ;
                int q ;
                int r ;
              } num ;
module test(input in, output out) ;
    
    int a = 3 ;
    num n1[2][2] = '{ '{ '{2,a,4},'{5,a,7} }, '{ '{1,a,4},'{8,a,0} } } ;

    initial begin
          for (int i = 0; i<4; i++)
            for (int j = 0; j<4; j++) begin
               $display(" Diagonal values: %d\n", n1[i][j].p) ;
               $display(" Diagonal values: %d\n", n1[i][j].q) ;
               $display(" Diagonal values: %d\n", n1[i][j].r) ;
            end
    end

    assign out = in ;

endmodule

