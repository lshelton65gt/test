
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): In SystemVerilog 'disable' can be used inside a function to disable the
// caller block, though the function can not be disabled. This design defines
// a named block and a function. The function is called from within the named
// block and inside the function a 'disable' statement disables the named block.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = in2 - in1;
    r2 = in1 - in2;

    begin: caller_block
        r1 = dis_caller(r1, in2);
        r2 = dis_caller(in1, r2);
        r1 = dis_caller(r1, r2);
        r2 = dis_caller(in1, in2);
    end: caller_block

    out1 = r1 * r2;
    out2 = r2 ^ r1;
end

function automatic int dis_caller (int a, b);
    int t1, t2;
    static int i;

    t1 = a | b;
    t2 = a ^ b;

    if (t1 != t2)
         disable caller_block;

    i++;
    $display("Called %d time", i);
    return (t1+t2)/2;
endfunction

endmodule

