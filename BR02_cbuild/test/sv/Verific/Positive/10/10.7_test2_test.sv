
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The final block Test, it can call $finish. Execution of
// $finish,  tf_dofinish(), or vpi_control(vpiFinish,...) from within a final block
// shall cause the simulation to end immediately.

module test( input int p, output int q) ;

    parameter period = 10 ;

    assign q = p ;

    final
    begin
        $display("Number of cycles executed %d", $time/period) ;
        $finish ;
        $display("finish called explicitly") ;
    end

endmodule

