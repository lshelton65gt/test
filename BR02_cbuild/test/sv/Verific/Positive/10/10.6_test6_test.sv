
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the use of 'return' in tasks.

module test;
bit [3:0] mod_bit1, mod_bit2;

task my_task(input bit [3:0] task_bit, output bit [3:0] inv_bit);
  for(int i = 3; i >= 0; i--)
  begin
	if(task_bit[i] == 1'b0)
		return;
	else
		inv_bit[i] = task_bit[i];
  end
endtask
initial
begin
	mod_bit2 = 4'b1010;
	repeat (5) #5 mod_bit1 = mod_bit2 + 1;
end
always @(mod_bit1)
begin
	my_task(mod_bit1, mod_bit2);
	$display("Source = %b, Copied = %b \n", mod_bit1, mod_bit2);
end
endmodule
