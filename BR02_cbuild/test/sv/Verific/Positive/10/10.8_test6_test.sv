
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows a matching block name to be specified for a
// 'fork-join' block after the 'join' keyword. This design defines two such 'fork-join'
// block having a name after 'join'.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    fork: fork_1
        r1 = in1 & in2;
        r2 = in2 ^ in1;
    join: fork_1

    out1 = r1 + r2;
    out2 = r2 - r1;
end

endmodule

