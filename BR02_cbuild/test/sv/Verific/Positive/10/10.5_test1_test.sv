
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses 'do...while' loop statement. This works
// like 'do...while' loop in C. The condition is checked at the end of the body,
// so it always executes the body at least once.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = in1;
    r2 = r1 + in2;

    do
    begin
        r2 = (r1 + r2)/3;
        r1 = r1/2;
    end
    while (r2 > r1) ;
    out1 = r1;
    out2 = r2;
end

endmodule

