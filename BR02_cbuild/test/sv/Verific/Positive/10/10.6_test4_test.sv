
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing 'break' and 'continue' with 'do-while'.

module test;
int i = 0;
initial
begin
	do
	begin
		i++;
		if(i < 5)
			continue;
		if(i >= 5)
		begin
			$display("%d", i);
			break;
		end
	end
	while(i>0)  ;
end	
endmodule
