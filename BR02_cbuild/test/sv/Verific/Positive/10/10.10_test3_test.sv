
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Initializing a variable does not generate an event. If an event
// is required, we have to assign the value inside an 'initial' or
// 'always' block. This design declares and initializes a variable
// inside an 'if-generate' statement and assigns a value to it in the  'else' part.
// So in the first case, no event is generated but in
// the second case an event is generated.

byte b = "B";    // this should not generate an event
byte c;

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    bit [w-1:0] t1 = '0, t2 = '0;
    initial 
        c = "C";    // this should generate an event

    always@(b)    // catch the event, if any
        assign t1 = b;

    always@(c)    // catch the event
        assign t1 = b;

    always@(posedge clk)
    begin
        if (t1 == "B")    // check if initialization fired an event
        begin
            out1 = '0;
            out2 = '0;
        end
        else
        begin
            if (t2 == "C")
            begin
                out1 = in1 - in2;
                out2 = in2 - in1;
            end
            else
            begin
                out1 = '1;
                out2 = '1;
            end
        end
    end

endmodule

