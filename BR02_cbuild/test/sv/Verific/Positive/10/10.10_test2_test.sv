
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog introduces logic data type which accepts four
// state value. This design uses variables with logic data type in event control

module test #(parameter width = 8)(input clk,
              input logic [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(in1+in2)
begin
    $display($time,,"in1+in2 changed");

    out1 = in1 | in2;
end

always @(in1 or in2)
begin
    $display($time,,"in1 or in2 changed");

    out2 = in1 ^ in2;
end

endmodule

