
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The 'do-while' loop might just contain a null statement.
// The following design checks this.

module test ;
    int i = 100 ;
    initial
    begin
        do
            ; // Null statement in do-while loop
        while (i> 10) ;
    end
endmodule
