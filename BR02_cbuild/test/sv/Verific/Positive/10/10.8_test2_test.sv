
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of label in disabling a statement.

module test;
int in, out;

  task T;
     input int a;
	 output int b;
	 b = a;
  endtask

  initial
      repeat(20)
	     #5 in = $random;

  always @(in)
      always1: begin
	             t1:T(in, out);
	             $display(" %d = %d ", out, in);
	           end

endmodule

module top;

    test instanMod();

    initial
	begin
        #10 disable instanMod.always1.t1;
		$display("Task Call disabled, output should not match with input");
	end	

endmodule
