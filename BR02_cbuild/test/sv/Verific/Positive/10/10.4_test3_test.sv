
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an 'if-else' statement coupled with the
// 'unique' keyword. One of the conditions checked is determined by the value
// of the parameter which is being overridden from the instantiating module. 
// The overridden value matches with one of the already specified conditions.
// This should generate a run-time warning.

module test;
parameter int p = 0;

int a;

initial
    begin
	   repeat(10)
	       a = $random;
	   unique if((a == p) || (a == 1)) $display("%d or 1", p);
	   else if(a == 2) $display("2");
	   else if(a == 4) $display("4");
	end

endmodule

module top;

    test #(2)instanMod1();

endmodule


