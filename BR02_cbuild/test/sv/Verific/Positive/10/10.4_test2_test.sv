
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses unique keyword with case statement. unique denotes that
// there is no overlapping in the conditions of case statement, this design
// uses an overlapping condition. It should generate a runtime warning.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1;
reg [0:p-1] r2;

always @(posedge clk)
begin
    r1 = in1 & in2;
    r2 = r1 - in1 | in2;

    unique case (r1[1:0]+r2[p-2:p-1])
    	2'b00:
        begin
            out1 = ~r1 & r2;
            out2 = r1 * r2;
        end
    	2'b01:
        begin
            out1 = r1 ^ ^r2;
            out2 = ~r1 + +r2;
        end
    	2'b00:                // Overlapping condition
        begin
            out1 = r1 * r2--;
            out2 = ++r1 - --r2;
        end
    	2'b11:
        begin
            out1 = ~r1<<2 - ~r2;
            out2 = r1 | r2>>2;
        end
    endcase
end

endmodule

