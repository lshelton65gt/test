
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of 'priority case' statement.

module test;
int i = 0;
initial
	repeat (10) i = i + 1;
always @ (i)
begin
	priority case(i)
		1 : $display("%d\n", i);
		3 : $display("%d\n", i);
		5 : $display("%d\n", i);
		8 : $display("%d\n", i);
		9 : $display("%d\n", i);
	endcase
end
endmodule
