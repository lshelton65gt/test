
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses a do...while loop. SystemVerilog introduced this new type
// of loop. It works in the same way as do...while loops in C.

module test #(parameter port_width = 16) 
             (input clk, 
              input [port_width-1:0] in1, in2, 
              output reg [0:port_width-1] out);

reg [port_width-1:0] r1, r2;
int i=0;

always @(posedge clk)
begin
    do
    begin
        if(i<0 || i>=port_width)
            break;

        r1[i] = in1[i] & in2[port_width-1-i];
        r2[i] = in1[port_width-1-i] | in2[i];

        out[i] = r1[i] ^ r2[i];
    end
    while (++i<port_width) ;
end

endmodule

