
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows a label to be specified before any statement following
// a colon. This label can be used to identify the statement. Even unnamed
// blocks can have a label before it. This design uses an unnamed fork join
// block with label.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    fork_1: fork
        r1 = (in1[width-1]?-in1:in1);
        r2 = (in2[width-1]?in2:-in2);
    join

    if (r1 - r2 == 0)
        out1 = r1 + r2;
    else
        out1 = r1 - r2;

    if (r1 == r2)
        out2 = r1 - r2;
    else
        out2 = r1 + r2;
end

endmodule

