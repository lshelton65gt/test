
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If in an 'unique-if' statement none of the conditions are true
// then a run-time warning should be issued. This design checks that.

module test;
int i, j;
initial
begin
	i = 5;
	j = 10;
	unique if( i < 0 )
		$display(" Condition (%d < 0) is satisfied ", i);
	else if( j > 0)
		$display(" Condition (%d > 0) is satisfied", j);
	unique if( i < 0 )
		$display(" Condition (%d < 0) is satisfied ", i);
	else if( j < 0)
		$display(" Condition (%d < 0) is satisfied", j);
end
endmodule
