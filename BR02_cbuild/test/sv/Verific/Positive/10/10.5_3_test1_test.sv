
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests 'foreach' construct with multiple
// loop variables.

//module test (input int prod[3][3], bit clk, output bit out);
module test (input int prod[3:0][3:0], bit clk, output bit out);
    int sum ;
    always @ (posedge clk)
    begin
        sum = 0 ;
        foreach( prod[ k, m ] ) // 10.5.3: loop variables are automatic and implicitly declared.
        begin
            prod[k][m] = k * m;
            sum = sum + prod[k][m] ;
        end

        if (sum == 1)
            out = 1'b0 ;
        else
            out = 1'b1 ;
    end
endmodule

