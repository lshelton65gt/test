
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing 'do-while' loop.

module test;

    int i = 5;

    initial
    begin
        i = 4;
        do
        begin
            i += 5;
            $display("%d", i);
        end
        while(i < 0) ; 
    end

endmodule
