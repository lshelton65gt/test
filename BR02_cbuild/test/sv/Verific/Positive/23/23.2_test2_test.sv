
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The macro can include `` in the body to have the arguments
// concatenated with the other. This is the counter part of the ## preprocessor
// directive of C for SystemVerilog.

`define paste(l, r) l``r

module test;

    bit [3:0] b1, b2;
    bit [7:0] b3;

    initial
    begin
        b1 = 4'b0101;
        b2 = 4'b1010;
        b3 = `paste(8'b1010, 0101) ;

        if ( b3 == { b2, b1 })
            $display("Success: 8'b1010``0101 == { 4'b1010, 4'b0101 }");
        else
            $display("Failure: 8'b1010``0101 != { 4'b1010, 4'b0101 } : b3 = %b ", b3);
    end

endmodule

