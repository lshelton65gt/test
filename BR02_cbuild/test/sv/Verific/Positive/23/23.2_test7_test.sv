
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the use of \" in `define macro text.

`define print `"Printing `\`"adfa`\`"sd`"

module test;

initial
    $display(`print) ;

endmodule

