
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A macro can include `` in the body to have the arguments
// concatenated with the other. This is the equivalent construct in System Verilog
// for the ## preprocessor directive of C . This design uses that macro call inside
// a for-generate to check whether the genvar variable is replaced with its value.

`define paste(l, r) l``r

module test;

    reg [3:0] a1;
    reg [3:0] a2;
    reg [3:0] a3;
    reg [3:0] a4;

    reg [3:0] ai;

    genvar i;

    generate

        for(i=1; i<=4; i++)
        begin: for_gen_i
            initial
                #5 $display("a%d = %d (should be 0)", i, `paste(a, i));
        end

    endgenerate

    initial
    begin
        a4 = (a3 = (a2 = (a1 = 1) + 1) + 1) + 1;
        ai = '0;
    end

endmodule

