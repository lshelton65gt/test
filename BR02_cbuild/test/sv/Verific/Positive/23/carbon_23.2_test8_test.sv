// modified for carbon so that it can use the names we have selected for source files

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The file to include in `include compiler directive can in turn be
// another macro, like `define incfl "header.h"; `include `incfl.
// This design defines a macro and uses it to include a file.

`define inc_file "carbon_23.2_test8_test1.v"

`include `inc_file

        $display("From the original file");
    end

assign out1 = 0;
endmodule

