
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog supports line continuation when defining a macro
// using a backslash (\) character. This design defines the whole module using the
// line continuation character.

`define def_mod module test;                                                          \
                                                                                      \
    int n1, n2;                                                                       \
                                                                                      \
    initial                                                                           \
    begin                                                                             \
        n1 = -5;                                                                      \
                                                                                      \
        n2 = abs(n1);                                                                 \
        n1 = abs(n2+2*n1);                                                            \
                                                                                      \
        if (n1 == 5 && n2 ==5 )                                                       \
            $display("Success");                                                      \
        else                                                                          \
            $display("abs function defined through macro is not working properly");   \
    end                                                                               \
                                                                                      \
    function automatic int abs(int n);                                                \
        if (n<0)                                                                      \
            return -n;                                                                \
                                                                                      \
        return n;                                                                     \
    endfunction                                                                       \
                                                                                      \
endmodule

`def_mod

