
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the use of `` in `define. A `` delimits lexical tokens
// without introducing whitespace, allowing identifiers to be constructed from arguments. 

`define foo(f) f``_suffix

module test;

int `foo(bar);

initial
begin
    `foo(bar) = 10;

    $display("The following output values should be equal");
    $display("Output %d",`foo(bar));
    $display("Output %d",bar_suffix);
end

endmodule

