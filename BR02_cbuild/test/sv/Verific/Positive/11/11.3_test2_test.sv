
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog provides 'always_latch' procedure for modeling
// latched logic. This design tests this.

module test(enable, q, d) ;

    input enable, d ;
    output q ;
    reg q ;

    always_latch
    begin
        if(enable) q = d ;
        $display("Within always latch") ;
    end

endmodule

module top ;

    reg enable, d ;
    wire q ;

    initial
    begin
        enable = 0 ;
        d = 0 ;
        #10 d = 1 ;
        enable = 1 ;
    end
    test instMod(enable, q, d) ;

endmodule

