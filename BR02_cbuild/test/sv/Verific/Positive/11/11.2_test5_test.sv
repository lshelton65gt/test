
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog provides 'always_comb' procedure for 
// combinational behavior. This design tests this.

module test;
bit [3:0] bit1, bit2; 
bit [3:0] resultBit;
initial
begin
    bit1 = 4'b 0000;
    bit2 = 4'b 0001;
    repeat(10) 
    begin
        bit1 += 1;
        bit2 += 2;
    end
end

always_comb
begin
    resultBit = bit1 & bit2;
    $display("Result = %b", resultBit);
end
endmodule

