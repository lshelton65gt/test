
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses 'fork..join' construct to create two
// concurrent processes. One process tries to disable another process by using
// disable construct.

module test #(parameter width=8)
             (input clk,
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2, r3, r4, r5, r6, r7, r8;

always@(posedge clk)
begin
    r1 = in1 | in2;
    r2 = in2 - in1;

    fork
    begin
        if (in1 == in2)
            disable proc_block;
    end

    begin
        r3 = r2 & in1;

        begin: proc_block
            r4 = r3 + in2;
            r5 = r1 | r2;
        end: proc_block

        r6 = r1 ^ in2;
    end
    join

    r7 = r3 & r5;
    r8 = r4 ^ r6;

    out1 = r7<<width/2 ^ r8>>width/2;
    out2 = r7>>width/2 | r8<<width/2;
end

endmodule

