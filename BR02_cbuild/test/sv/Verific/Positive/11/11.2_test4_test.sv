 
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog provides 'always_comb' block for combinational
// logic behavior. Tool may generate a warning if a latch is inferred by always_comb
// The procedure is triggered once at time zero.

module test(output reg o, input a, input e) ;

    always_comb
    begin
        if (e) o = a ;
        $display("Within always comb") ;
    end

endmodule

