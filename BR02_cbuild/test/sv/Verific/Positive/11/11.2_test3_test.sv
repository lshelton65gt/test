
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an 'always_comb' block. The 'always_comb'
// block is executed once at time zero after all 'initial' and 'always' block. 
// This design checks that.

module test #(parameter p=8)
             (input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1, r2;
reg [0:p-1] r3, r4;
time t;

always_comb
begin
    r1 = in1<<p/2 ^ in1>>p/2;
    r2 = in1 ^ in2;

    out1 = r1 & r2;
    out2 = r1 | r2;

    t = $time;

    $display("Now the simulation time is %d", t);
end

endmodule

