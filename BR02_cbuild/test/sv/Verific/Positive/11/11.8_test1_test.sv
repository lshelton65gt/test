
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test the disable fork statement, that terminates all
// active descendants (sub-processes) of the calling process.  

module test ;

task wait_20_30 ;
   fork 
      begin 
         $display( "First Block\n" ) ;
         # 20ns ;
      end 
      begin 
         $display( "Second Block\n" ) ;
         # 30ns ;
      end 
   join 

   disable fork ;
endtask

   initial 
      wait_20_30 ;

endmodule

