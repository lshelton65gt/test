
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the difference between 'always_comb' and 'always @*' 
// with respect to changes in the content of a function.

function void func1(output int in1);
    in1 = 5;
    $display("\ninput unchanged ... \n");
endfunction

function void func2(input int in1);
    $display("\ninput changed ... \n");
endfunction

module test;
  int in1;

  always_comb
  begin
      $display("\nalways_comb output\n");
      func1(in1);
  end

  always @*
  begin
      #20 $display("\nalways @* output\n");
      func2(in1);
  end
endmodule

