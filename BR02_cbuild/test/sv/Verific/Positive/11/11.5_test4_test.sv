
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog allows continuous assignment on non-net 
// objects. This design uses continuous assignments on variables of different
// data types.

module test #(parameter width=8) (input [width-1:0] in1, 
                                  input [0:width-1] in2, 
                                  output[width-1:0] out1, out2) ;

    integer i ;
    integer iv ;
    real r ;
    time t ;
    realtime rt ;
   

    assign
           i = in1[0] | in2[width-1],
           r = in1[width-1] ^ in2[0],
           t = in1[0] * in2[0],
           rt = in1[width/2-1] & in2[width/2-1],
           iv = in1<<width/2 - in2 ;

    assign out1 = i | iv + t ;
    assign out2 = r + t - rt ;

endmodule

