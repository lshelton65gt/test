
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): always_comb automatically executes once at time zero, whereas
// always @* waits until a change occurs on a signal in the inferred sensitivity list.


module test;

reg [0:1] a;

int count1;
int count2;

	initial
	begin
	#1 a = 1;

	#1;
	if (count1 == 2) $display ("Pass: always_comb");
	else $display ("Fail: always_comb");

	if (count2 == 1) $display ("Pass: always");
	else $display ("Fail: always");

	end

	always_comb
	begin
		count1++;
		$display ("Value from always_comb %b", a);
	end

	always @ ( * )
	begin
		count2++;
		$display ("Value from always %b", a);
	end
endmodule

