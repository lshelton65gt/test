
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses continuous assignments and initialization 
// on the same variables.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

wor r = '0;
triand t = '1;

assign r = |in2 & ^in1;
assign t = &in1 ^ |in2;

assign out1 = { width { r | t } },
       out2 = { width { t ^ r } };

endmodule

