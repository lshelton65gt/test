
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): always_comb is sensitive to changes within the contents of
// a function, where as always @ (*) is only sensitive to the changes in the arguments.

module test;

int a,b;

int count1 ;
int count2 ;
int y1, y2;

	initial
	begin
	#1;
	if (count1 < count2) $display ("Pass: always_comb, count1 = %d count2 = %d", count1, count2);
	else $display ("Fail: always_comb, count1 = %d count2 = %d", count1, count2);

	end

	always_comb
	begin
		count1++;
                a++;
                b--;
		y1 = f (a, b);
	end

	always @ ( * )
	begin
		count2++;
		y2 = f (a, b);
	end

function int f(int a, b);

	return a + b;
endfunction
endmodule

