
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog provided 'always_latch' procedure triggers
// at time zero automatically. This design tests this.

module test;
reg modReg = 1'b0;

initial
    repeat (5) 
        modReg = #5 ~modReg;

always_latch
begin
    $display("Output of always_latch ... \n");
    $display($time ,,, "%b\n", modReg);
end    

always @*
begin
    $display("Output of always @* ... \n");
    $display($time ,,, "%b\n", modReg);
end

endmodule

