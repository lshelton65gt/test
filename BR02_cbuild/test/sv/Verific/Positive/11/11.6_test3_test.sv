
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test join_any in fork..join block -- The parent process 
// blocks until any one of the processes spawned by this fork complete.

module test ;

    task wait_20_30 ;
       fork 
          begin 
             $display( "First Block\n" ) ;
             # 20ns ;
          end 
          begin 
             $display( "Second Block\n" ) ;
             # 30ns ;
          end 
       join_any 
    endtask

    initial 
       wait_20_30 ;

endmodule

