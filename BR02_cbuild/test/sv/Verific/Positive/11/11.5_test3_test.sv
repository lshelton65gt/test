
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testcase checks as to whether continuous assignment can be 
// applied on reg.

module test (input d, en, output reg q);

    assign q = en ? d : 'z ;
    initial
        $monitor($time," q=%b, en =%b, d=%b", q, en,d) ;

endmodule

