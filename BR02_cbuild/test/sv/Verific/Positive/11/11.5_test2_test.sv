
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests continuous assignments on variables of 
// type bit, byte, integer, int and logic.

module test;
bit [3:0] mod_bit;
byte mod_byte;
integer mod_in;
int mod_int;
logic [3:0] mod_log;

assign mod_bit = 4'b1010;
assign mod_byte = {2{mod_bit}};
assign mod_in = 32'b100x;
assign mod_int = mod_bit/2;
assign mod_log = 'x;

initial
begin
    $display("Testing continuous assignments on bit, byte, integer, int, byte and logic data types ... \n");
    if((mod_bit == 4'b1010) && (mod_byte == 8'b10101010) && (mod_in === 4'b100x) && (mod_int == 3'b101) && (mod_log === 4'bx)) 
        $display("Testing successful\n");
    else
        $display("******  FAILURE ******\n mod_byte%b mod_in %b mod_int= %b mod_log = %b mod_bit = %b", mod_byte, mod_in, mod_int, mod_log, mod_bit);
end

endmodule

