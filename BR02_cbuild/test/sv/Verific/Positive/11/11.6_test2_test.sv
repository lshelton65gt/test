
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): In the following example, two processes are forked, 
// the first one waits for 20ns and the second waits for 30ns. Because the
// join_none keyword is specified, the parent process shall not block.
// But use of wait_fork makes the parent process wait until the two processes
// completed

module test ;

task wait_20_30 ;
   fork 
      begin 
         $display( "First Block\n" ) ;
         # 20ns ;
      end 
      begin 
         $display( "Second Block\n" ) ;
         # 30ns ;
      end 
   join_none 

   wait fork ;
endtask

   initial 
      wait_20_30 ;

endmodule

