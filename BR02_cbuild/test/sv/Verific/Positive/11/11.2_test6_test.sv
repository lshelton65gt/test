
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog provided 'always_comb' procedure executes
// at simulation time 0. This design tests this.

module test;
reg modReg = 1'b0;
initial
    repeat (5) 
        modReg += 1'b1;

always_comb
    $display($time ,,, "%b\n", modReg);
always @*
    $display($time ,,, "%b\n", modReg);
endmodule

