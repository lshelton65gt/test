
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an 'always_latch' block. The 'always_latch'
// block is executed once at time zero after all 'initial' and 'always' block. 
// This design checks this.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;
reg [0:width-1] r3, r4;
time t;

always_latch
begin
    r1 = { in1[width/2-1:0], in2[0:width/2-1] };
    r2 = in1 ^ { {width/2{1'b0}}, in1[width/2-1:0] } | 
	 in2 ^ { in2[0:width/2-1], {width/2{1'b0}} };

    out1 = r1 + r2;
    out2 = r1 - r2;

    t = $time;

    $display("Now the simulation time is %d", t);
end

endmodule

