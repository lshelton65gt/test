
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a type parameter and class is used
// as a the overridden value of that 'type parameter'.

class Student ;
    int roll ;
    int age ;

    function new(int n, int a) ;
        roll = n ;
        age = a ;
    endfunction

    function void print() ;
        $display(" %d    %d", roll, age) ;
    endfunction

endclass

module test ;
   
    bot #(.roll(3), .t1(Student), .age(5)) b1() ;

endmodule

module bot #(parameter roll = 10, parameter type t1 = Student, parameter age = 15) ;
    t1 s[roll] ;

    genvar i ;
    generate for (i = 0 ; i < roll; i++)
    begin : gen 
       initial begin  
           s[i] = new (i, 15+(roll/2) ) ;
           s[i].print() ;         
       end
    end
    endgenerate

endmodule

