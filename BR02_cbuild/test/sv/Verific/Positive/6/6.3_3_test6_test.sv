
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of parameters to specify data types of another parameter.

module test(input in, output int out);

    parameter type p = bit[3:0] ;
    parameter p p1 = 4'b1100 ;

    assign out = in  + p1 ;

endmodule
