
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of assignment to a parameter of structure type.

module test;

   parameter struct packed { int a; bit[3:0]b; }p = '1;

   initial
       $display("Parameter value is (should be all 1) = %b", p);

endmodule

module top ;

   test #('x) /* should be converted to 0 */ I() ;
endmodule


