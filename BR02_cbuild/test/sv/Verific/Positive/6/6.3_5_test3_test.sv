
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing const with hierarchical path name.

int rootIn = 5;

module test;
const int con = $root.rootIn;

initial
  begin
      $display("Positive test case");
  end

endmodule

