
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing scope of the variable declared in interface.

interface root_inter;
int mod_int;
bit [7:0] mod_bit;
endinterface

module test;
root_inter a();
initial
begin
	a.mod_int = 10;
	a.mod_bit = '1;
	$display("mod_int = %d \nmod_bit = %b", a.mod_int, a.mod_bit);
end
endmodule
