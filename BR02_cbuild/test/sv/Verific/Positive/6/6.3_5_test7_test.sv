
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing const declaration containing an unpacked array.

module test (input bit in, output logic out [1:0]);

    const bit modBit [1:0] = '{1'b1, 1'b0};

    always @ (in)
        out = '{in + modBit[1], in - modBit[0]} ;

    initial
        $display("modBit[1] = %b   modBit[0] = %b", modBit[1], modBit[0]) ;

endmodule

