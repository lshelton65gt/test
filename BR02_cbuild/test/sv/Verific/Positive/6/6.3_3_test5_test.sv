
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing type parameter in nested module.

bit [3:0] bit1;

module m1 #(parameter type p = int, parameter p p1 = 1)(output bit[p1:0]out);
    assign out = '1;
endmodule

module m2;
    parameter type p2 = int;
    p2 c[7:0] = "HELLO";
endmodule

module m3;
    parameter p = 7;
    module m1;
        parameter type p1 = bit;
        p1 [p:0]a = 'x;
    endmodule
    
    m1 #(logic)I3();
endmodule

module test;

m1 #(.p(byte))I1(bit1);
m2 #(.p2(byte))I2();
m3 I3();
    initial
    begin
        $display("Expected output : m1.out = 11, HELLO, 0");
        $display("m1.out = %b", I1.out);
        for(int i = I2.c.size(); i >= 0; i--)
            $display("%c", I2.c[i]);
        $display("%b", I3.I3.a);
    end
    
endmodule

