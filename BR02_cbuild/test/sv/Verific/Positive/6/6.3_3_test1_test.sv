
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of type parameters in casting.

module param_type(input in, output out) ;

    string a[7:0] ;
    parameter type p1 = byte ;
    parameter type p2 = byte ;

    p1 t = p1'(a[3]) ;
    initial
    begin
        for(int i=7; i>= 0; i--)
        begin
            a[i] = "HELLO" ;
        end
        a[4] = string'(t) ;
        $display("Instantiating module param_type") ;
    end

    assign out = in ;

endmodule

module test(input in, output out) ;

    typedef struct packed 
    {
        bit [3:0] strBit ;
        byte strChar ;
    } str ;

    typedef bit [$bits(str)-1 : 0] typeBit ;

    param_type #(.p1(typeBit), .p2(str)) instanMod(in, out) ;

endmodule
