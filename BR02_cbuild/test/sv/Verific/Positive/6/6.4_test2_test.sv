
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): As per the LRM, if a data type is not specified while
// declaring variables with 'var' keyword, then the data type logic shall be
// inferred. This design checks that. 


module test(input logic val, output bit out) ;
    var v1 ;    // equivalant to logic v
    logic v2 ;
    
    always @ (val)
    begin
        v1 = val ;
        v2 = val ;

        if (v1 === v2)
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

