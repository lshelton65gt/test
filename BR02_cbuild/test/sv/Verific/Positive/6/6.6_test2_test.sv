
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows specific data to be declared as automatic even
// inside a static task. These are automatic in nature and behaves
// as the regular automatic variables. This design checks that.


`define    ERROR    0.1

module test (input clk,
             input [7:0] in1,
             output reg [7:0] out1);

    always@(posedge clk)
    begin
        sqrt(in1, out1 );
    end

    task static sqrt(input int n, output int o);
        automatic real r1, r2;

        if (n < 2)
        begin
            o = 1;
            return;
        end

        r1 = n/2;
        r2 = n/r1;
        while (((r2>r1)?(r2-r1):(r1-r2)) > `ERROR)
        begin
            r1 = (r1+r2)/2;
            r2 = n/r1;
        end 
        o = (r1+r2)/2;
        return;
    endtask

endmodule

