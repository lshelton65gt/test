
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test that a constant declaration can contain an
// expression with any hierarchical path name

module test(input int in1, in2,
                  output int out);

    const int option = f.b.const2;

    always @ *
    begin
        case (option)
            1: out = in1 + in2;
            2: out = in1 - in2;
            3: out = in1 * in2;
        endcase 
    end

    function int f;
        begin : b
            const int const2 = 2;
            f = 1;
        end
    endfunction

endmodule
