
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design declares a global variable and assigns it a value 
// through assign in $root scope and module scope. 

int var1 = 10;

module test;
    initial $root.var1 = 5;
endmodule

