
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): When type reference is used in case equality/inequality
// comparison, it shall only be compared with another type reference. Two type
// references shall be considered equal in such comparisons, if and only if,
// the types to which they refer match.
// The following design checks this.

module test (input clk,
             input [7:0] in1,
             output reg [7:0] out1);
    parameter size = 12 ;

    bit [size: 0] A_bus, B_bus ;
    parameter type bus_t = type (A_bus) ;

    generate
        case(type(bus_t))
            type(bit[12:0]) : addfixed_int # (bus_t) (A_bus, B_bus) ;
            type (real) : add_float #(type(A_bus)) (A_bus, B_bus) ;
        endcase
    endgenerate

    always@(posedge clk)
    begin
        out1 = in1 * 3 ;
    end
endmodule

