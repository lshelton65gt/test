
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing const type with hierarchical expression.

module hier_test(input in, output out) ;

    const int mod_int = 16 ;
    assign out = in ;
    initial
        $display("Instantiating module hier_test") ;

endmodule

module test(input in, output out) ;

    hier_test instan_mod(in, out) ;
    const bit [3:0] top_bit = instan_mod.mod_int ;

endmodule
