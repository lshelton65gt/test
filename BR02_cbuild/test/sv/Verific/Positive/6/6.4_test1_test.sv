
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing for the event generation when initialization is 
// made while declaration.

module test;
int i = 10;
logic mod_log1, mod_log2;
always_comb
begin
	mod_log1 = (i==10);
end
always @*
begin
	mod_log2 = (i==10);
end

initial
	$display("Output from always_comb block = %b\nOutput from always @* block = %b", mod_log1, mod_log2);
endmodule
