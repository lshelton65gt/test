
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing const keyword with hierarchical scope.

module test1;

    parameter p = 8;
    bit [p-1:0] mod1_bit;
    const int mod1_in = 10;

    initial
    begin : Block1
            const logic mod1_log = 1;
    end

endmodule

module test2;

    parameter p = 4;

    function void f2;
        const bit [1:0] func_bit = {1'b1, 1'b0};
    endfunction

endmodule

module test;

    const int top1 = instan_mod1.p;  // 8
    const int top2 = instan_mod1.mod1_in;  // 10
    const logic top3 = instan_mod1.Block1.mod1_log; // 1
    const int top4 = instan_mod2.p; // 4
    const bit [1:0] top5 = instan_mod2.f2.func_bit;  // 2'b10
    test1 instan_mod1();
    test2 instan_mod2();
    bit [8: 1] top_bit;

    initial
    begin
            top_bit = '1;
            #30
            $display("Output1 = (should be 11111111) %b\n", top_bit);
            $display("Output2 = (Should be 10) %d \n", top2);
            $monitor("Output3 =  (Should be 10) %b", top5);
    end

endmodule 
