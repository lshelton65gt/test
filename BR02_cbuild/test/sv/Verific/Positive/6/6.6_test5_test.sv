
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing scope of data declaration in named and unnamed blocks.

module test;
initial
begin:named_block
	reg [55:0] my_reg = "Success";
end
initial
begin
	reg [55:0] my_reg = "Success";
	$display("String in the named block = %s \nString in the unnamed block = %s", named_block.my_reg, my_reg);
end
endmodule
