
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Any data declared inside task is by default static if the
// task or the data is not explicitly declared as automatic.
// This design defines a variable inside a task to check this.

module test (input clk,
             input signed [3:0] in1, in2,
             output reg signed [3:0] out1, out2);

    int x, y;

    task abs(input int a, output int b);
        int c;

        c = ((a<0)?(0-a):(a));

        b = c;
    endtask

    always@(posedge clk)
    begin
        abs(in1, x);
        y = abs.c;

        if (x != y)
            out1 = -1;
        else
            out1 = x;

        abs(in2, x);
        y = abs.c;

        if (x != y)
            out2 = -1;
        else
            out2 = y;
    end

endmodule

