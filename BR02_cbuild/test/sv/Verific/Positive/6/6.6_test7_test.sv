
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test case with combination of automatic and static variables
// in static tasks and functions.

module test;

function void f1(input int en);
  static int func_stat;
  automatic int func_auto;

  if (en) begin
    // initialize
    func_stat = 0;
    func_auto = 0;
    test.f1(0);
    return;
  end
  func_stat = func_stat + 1;
  func_auto = func_auto + 1;
  $display("Static = %d, Automatic = %b\n", func_stat, func_auto);
  if (func_stat < 3)
    test.f1(0);
endfunction
initial
begin
	f1(1);
end
endmodule
