
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): In System Verilog another form of variable declaration begins
// with keyword 'var'. This design uses 'var' keyword to declare variable.

module test(input clk, byte val, output bit out) ;
    var byte my_byte1 ;    // equivalant to byte my_byte1
    byte my_byte2 ;
    
    always @ (posedge clk)
    begin
        my_byte1 = val ;
        my_byte2 = val ;
    end

    always @ (negedge clk)
    begin
        if (my_byte1 == my_byte2)
            out = 1'b1 ;
        else     
            out = 1'b0 ;
    end
endmodule

