
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing const data type initialized with constant function.

module test;
function int f1(input int in);
  int out;
  out = 5*in;
  return out;
endfunction
const int my_const = f1(5);
initial
begin
	if(my_const == 25)
		$display("Testing initialization of const type with constant function is a success");
end
endmodule
