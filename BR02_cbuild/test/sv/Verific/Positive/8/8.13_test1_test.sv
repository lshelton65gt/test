
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses assignment pattern as actual in module
// instantiation. The formal is an array of int data type. An actual connected
// to a formal is an assignment like context. Hence we can use assignment pattern
// here according to IEEE 1800 LRM section 8.13.

module test(input in, output out) ;

    int f [0:2], f2 [0:2], f3[0:2] ;
    int g ;
    typedef int frac [1:3] ;
    
    child I('{1, 2, 3}, f) ;
    child I2(f2, f3) ;

endmodule

module child (input int in[2:0], output int out[2:0]) ;

   assign out = in ;
   initial
        $display("Instantiating module child") ;

endmodule

