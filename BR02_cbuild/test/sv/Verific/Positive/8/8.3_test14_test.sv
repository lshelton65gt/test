
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses assignment operator '^=' in constant function.

module top ;
    parameter p = 10, q = 20 ;
    test  b1() ;
    initial
       $monitor(" %d,   %d", p,q) ;
endmodule

module test #(parameter p = 12, q = 21, parameter type t1 = int) ;

    function int swap(t1 p, t1 q) ;
        int p_tmp, q_tmp ;
        p_tmp = p ;
        q_tmp = q ;
        p_tmp ^= q_tmp ;
        q_tmp ^= p_tmp ;
        p_tmp ^= q_tmp ;
        swap = p_tmp ;
    endfunction
    defparam top.q = swap(p,q) ;

endmodule

