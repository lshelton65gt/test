
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a structure array and assigns value to
// every element of structure array by using assignment pattern.

typedef struct {  int name ;
                  int roll ;
               } student ;
module test(input in, output out) ;

    const int a = 5 ;
    bot #(.width(3),.t1(student)) b1() ;
    assign out = in ;

endmodule

module bot #(parameter width = 10, parameter type t1 = int ) ;

    t1 s[width-1 : 0] ;

    initial begin
       for (int i = 1 ; i <= width; i++) begin
           s[i] = '{name : 21, roll : 41} ;
       end
       $display("Instantiating module bot with parameter width = %2d", width) ;
    end

endmodule

