
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If the replication operator is equal to '0' then 
// a warning should get generated. The following design uses a concatenation
// with the replication operator equal to '0'.

module test (input int in, output out) ;

    parameter p1 = {{{0}{1'b0}}} ;
    parameter p2 = 7 ;
    bit [p1 : p2] b ;
    assign b[p2 - 1] = in ;
    assign out = &b | |in ;

endmodule              

