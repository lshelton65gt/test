
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing incrementor/decrementor operator with real and 
// shortreal operands.

module test;
initial
begin
	for(real i = 1.3; i < 2.1; i++)
		$display("Testing incrementor operator with real data type is verified");
	for(shortreal j = 3.7; j >= 2.75; j--)
		$display("\nTesting decrementor operator with shortreal data type is also verified");
end
endmodule
