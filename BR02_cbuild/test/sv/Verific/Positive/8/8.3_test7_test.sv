
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests assignment operator - >>>= in constant function.


module test ;

    parameter p = (8>>>1), q = 10 ;

    bot #(p) b1() ;

    initial
        $monitor(" %d,  %d", p, q) ;

endmodule

module bot #(parameter p = 12, q = divd(8,1)) ;

    int res = calcu(p,q) ;

    function int calcu(int p, int q) ;
        int r ;
        r = p++ ;
        p += (r++) - (--q) ;
        return p ;
    endfunction

    function int signed divd(int num, int signed times) ;
        num >>>= times ;
        return num ;
    endfunction
    
    defparam test.q = calcu( p, q) ;
    initial
       $display("%d", res) ;

endmodule

