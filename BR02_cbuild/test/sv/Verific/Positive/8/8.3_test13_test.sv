
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): When a binary operator has one operand of type bit and another 
// of type logic, the result is of type logic. If one operand is of type int and the
// other of type integer, the result is of type integer.

module test (input [31:0] int1, input [31:0] integer1, output reg[31:0] out1) ;
    initial begin
        out1 = 0 ;
        #10 ;
        out1 = int1 | integer1 ;
    end
endmodule

module test2 (input [31:0] int1, input [31:0] integer1, output reg[31:0] out1) ;
    initial begin
        out1 = 0 ;
        #10 ;
        out1 = int1 & integer1 ;
    end
endmodule

module test3 (input [31:0] int1, input [31:0] integer1, output reg[31:0] out1) ;
    initial begin
        out1 = 0 ;
        #10 ;
        out1 = int1 + integer1 ;
    end
endmodule

module test4 (input [31:0] int1, input [31:0] integer1, output reg[31:0] out1) ;
    initial begin
        out1 = 0 ;
        #10 ;
        out1 = int1 | integer1 ;
    end    
endmodule



