
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog added new operators like '++'. This design uses
// this operator in pre increment mode.

module test;

    reg [3:0] a, b;

    initial
    begin
        b = (a = 5);
        b = ++a;            // Pre increment
            ++b;            // Pre increment
        if (6 == a && 7 == b)
            $display("Pre increment successfull");
    end

endmodule

