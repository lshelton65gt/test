
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of '!=?' operator where X and Z values act as wild cards.

module test ;
   bit a = 8'b11xx00zz ;
   bit b = 8'b10z0z1xx ;
   int i = 0 ;

   always @(i) 
       $display("value of i changed ") ;

   equality E(a,b,i) ;

endmodule

module equality(input bit a, input bit b, output int i) ;

   always @ (a, b) begin
       if (a !=? b)
          i = 1 ;
   end

endmodule

