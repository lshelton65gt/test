
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines constant functions with signed and unsigned
// return type. 

module bench ;

    parameter signed p = -5, q = 40 ;
    test #(p,240) b1() ;

    initial
        $display ("p = %1d,  q = %1d", p, q) ;

endmodule

module test #(parameter p = 12, q = 21)(input in, output out) ;

    int res = calcu(p,q) ;

    function signed int calcu(int p, int q) ;
        p -= p++ - q-- ;

        $display(" %d,   %d", p, q) ;
        return p ;
    endfunction

    function signed int masking (bit[7:0] num, bit[7:0] mask) ;
         num |= mask ; 
         return num ;
    endfunction

    defparam bench.q = masking( p, q) ;

    initial
        $display("%d", res) ;

    assign out = in ;

endmodule

