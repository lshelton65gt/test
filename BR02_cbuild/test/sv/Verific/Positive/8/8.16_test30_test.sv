
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '=' operator overloading.
// Using an user defined data type and an int as arguments.

typedef struct {
    int c_real ;
    int c_imag ;
} complex ;

function int cplx_assign (int a, complex b) ;
    b.c_real = a ;
    b.c_imag = a ;
    return a ;
endfunction 
 
bind = function complex cplx_assign(int, complex) ;

module test(input bit clk, int a, output bit out) ;
    complex b ;
    
    always @ (posedge clk)
    begin
        b = a ;

        if (b.c_real == a && b.c_imag == a)
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

