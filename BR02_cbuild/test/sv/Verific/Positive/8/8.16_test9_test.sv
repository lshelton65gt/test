
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '=' operator overloading. Using only the user
// defined data types as the arguments.

typedef struct {
    int c_real ;
    int c_imag ;
} complex ;

function complex cplx_assign (complex a, complex b) ;
    b.c_real = a.c_real ;
    b.c_imag = a.c_imag ;
    return a ;
endfunction 
 
bind = function complex cplx_assign (complex, complex) ;

module test(input bit clk, complex a, output bit out) ;
    complex b ;
    
    always @ (posedge clk)
    begin
        b = a ;

        if (a.c_real == b.c_real && a.c_imag == b.c_imag)
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

