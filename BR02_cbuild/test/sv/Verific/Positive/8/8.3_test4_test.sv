
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog added new operators like '++'. This design uses
// this operator in post increment mode.

module test;

    reg [3:0] a, b;

    initial
    begin
        b = (a = 5);
        b = a++;            // Post increment mode
            b++;            // Post increment mode
        if (6 == a && 6 == b)
            $display("Post increment successfull");
    end

endmodule

