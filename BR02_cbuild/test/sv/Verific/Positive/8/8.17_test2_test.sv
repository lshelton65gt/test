
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : Testing streaming concatenation.

byte stream[$] ;  // byte stream

class Packet ;
    rand int header ;
    rand int len ;
    rand byte payload[] ;
    int crc ;
    constraint G { len > 1 ; payload.size == len ; }
    function void post_randomize ; crc = payload.sum ; endfunction 
    function void show ;
        $display("header = %4d, len = %4d, crc = %4d", header, len, crc) ;
    endfunction
endclass 

module test(input in, output out) ;

    initial
    begin :send    // Create random packet and transmit
        byte q[$] ;
        Packet p ;
        p = new ;
        void'(p.randomize()) ;
        q = {<< byte{p.header, p.len, p.payload, p.crc}} ; // pack
        stream = {stream, q} ;    // append to stream
        p.show() ;
    end

    initial
    begin : receive   // Receive packet, unpack, and remove
        byte q[$] ;
        Packet p ;
        p = new ;
        {<< byte{ p.header, p.len, p.payload with [0 +: p.len], p.crc }} = stream ;
        stream = stream[$bits(p) / 8 : $] ;    // remove packet
        p.show() ;
    end 

    assign out = in ;

endmodule

