
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '==?' operator. Using logic packed array.
// The '==?' and '!=?' operators may result in X if the left operand 
// contains an X or Z that is not being compared with a wildcard 
// in the right operand.

module test(input logic [7:0]val1, logic [7:0]val2, output bit out) ;
    always @ (val2)
    begin
        if (val1 ==? val2) out = 1'b0 ; // This equality results in X
        else out = 1'b1 ;
    end
endmodule

