
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '==' operator overloading.
// Using only the user defined data types as the arguments.

typedef struct {
    int c_real ;
    int c_imag ;
} complex ;

function bit cplx_eq_comp(complex a, complex b) ;
    complex c ;
    if (a.c_real == b.c_real && a.c_imag == b.c_imag)
        return 1'b1 ;
    else 
        return 1'b0 ;
endfunction 
 
bind == function bit cplx_eq_comp(complex, complex) ;

module test(input bit clk, complex a, complex b, output bit out) ;
    always @ (posedge clk)
        out = (a == b) ;
endmodule

