
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog added new operators like '--'. This design uses
// this operator in pre decrement mode.

module test;

    reg [3:0] a, b;

    initial
    begin
        b = (a = 5);
        b = --a;       // Pre decrement
            --b;       // Pre decrement
        if (4 == a && 3 == b)
            $display("Pre decrement successfull");
    end

endmodule

