
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '--' (pre-decrement) operator overloading.
// Using only the user defined data types as arguments.

typedef struct {
    int c_real ;
    int c_imag ;
} complex ;

function complex cplx_pre_decr (ref complex a) ;
    a.c_real = a.c_real - 1 ;
    a.c_imag = a.c_imag - 1 ;
    return a ;
endfunction 

bind -- function complex cplx_pre_decr(complex) ;

module test(input bit clk, complex a, output bit out) ;
    complex c ;
    
    always @ (posedge clk)
    begin
        c = --a ;

        if (c.c_real == a.c_real  && c.c_imag == a.c_imag )
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

