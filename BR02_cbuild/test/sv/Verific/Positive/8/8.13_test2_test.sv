
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): initialization of the struct data type, using structure literal
// with  data_type : value, member_name : value association.

module test ;

  typedef struct {
    int x ;
    int y ;
  } flag ;

  flag f1 ;
  int f = 3 ;

  initial begin 

    #1 f1 = '{ 1, 2 + f } ;
    #1 $display( f1.x, f1.y) ;
    #1 f1 = '{ x : 2, y : 3 + f } ;
    #1 $display( f1) ;
    #1 f1 = '{ int : 5 } ;
    #1 $display( f1 ) ;
    #1 $finish ;

  end 

endmodule 

