
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Unary reduction operators can be applied to any integer expression,
// including packed arrays. It returns a single value of type logic,
// if the packed type is 4-state value and bit type if it is 2-state type.
// This design checks this behaviour.

module test;

    int i1; /* 2-state */
    integer i2; /* 4-state */
    bit [7:0] b1; /* 2-state */
    logic [7:0] l1; /* 4-state */

    initial
    begin
        i1 = 512; /* 10'b1000000000 */
        i2 = 8'b0011xx00;
        b1 = 4'b1100;
        l1 = 8'b11xx0011;

        if (~|i1 !== 1'b0)
            $display("~| is not working properly");

        if (&i2 !== 1'b0)
            $display("& is not working properly");

        if (~^b1 !== 1'b1)
            $display("~^ is not working properly");

        if (|l1 !== 1'b1)
            $display("| is not working properly");
    end

endmodule

