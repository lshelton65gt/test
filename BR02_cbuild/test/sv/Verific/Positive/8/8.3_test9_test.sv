
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing various assignment operators.

module test;
int i = 150;
int j = 5;
initial
begin
        $display("Testing assignment operators +=, -=, *=, /=, and %%= \n");
	i /= (j *= 5) ;
        j += i;
	if(i == 6)
	    $display("Testing of *=, \/= is a success");
	j %= (i -= -1);
	if(j == 3)
	    $display("\nTesting of +=, -= and %%= is a success");
end
endmodule
