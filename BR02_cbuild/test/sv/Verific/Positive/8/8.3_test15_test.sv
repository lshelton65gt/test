
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests assignment operator ' >>=' in constant function.


module test ;
    parameter p = 8, q = 10 ;

    bot #(.p(p)) b1() ;

    initial
        $monitor(" %d,  %d", p, q) ;
endmodule

module bot #(parameter p = 12 /* 8 */, q = divd(8,1) /* 4 */) ;
    int res = calcu(p,q); // 14

    function integer divd(integer num /* 8 */, integer times /* 1 */) ;
         num >>= times ; // num = 8 >> 1 = 4
         return num ; // 4
    endfunction

    function integer calcu(integer p /* 8 */, integer q /* 4 */) ;
        integer r ;
        r = p++ ;             // r = 8, p = 9
        p += (r++) - (--q) ;  // p = 9+(8-3) = 14, r = 9, q = 3
        return p ;            // 14

    endfunction
     
    defparam test.q = calcu( p, q) ; // 14
    initial
       $display("%d", res) ;
endmodule

