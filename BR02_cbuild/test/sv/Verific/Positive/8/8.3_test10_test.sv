
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses assignment operator <<<= in constant 
// function and calls that function to assign default value to parameters


module test ;

    function int multi(int num, int times) ;
         num <<<= times ;
         return num ;
    endfunction
    parameter p = multi(8,1), q = 10 ;

    bot #(p) b1() ;

    initial
        $monitor(" %d,  %d", p, q) ;
    

    module bot #(parameter p = 12, q = multi(8,1)) ;

          int res = calcu(p,q) ;
    
        function int calcu(int p, int q) ;
            int r = p++ ;
            p += (r++) - (--q) ;
            $display(" %d,   %d,   %d", p, q, r) ;
            return p ;
        endfunction
         
        initial
        $display("The value is: %d",res) ;   
 
    endmodule

endmodule

