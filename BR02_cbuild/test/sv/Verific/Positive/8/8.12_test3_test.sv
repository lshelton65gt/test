
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): When a concatenation is used and the result is assigned to a smaller
// sized element an warning is generated. This design uses a concatenation
// and assigns that to a smaller sized element. This should generate an warning.

module test;

    bit [3:0] b1, b2;
    logic [3:0] l1, l2;

    initial
    begin
        b1 = (l1 = 4'b0x0x);

        b2 = 4'({ b1, l1 });  /* No warning: size is specified */
        l2 = 4'({ l1, b1 });  /* No warning: size is specified */

        if (b2 !== l2)
            $display("Error: truncation not properly carried out");
    end

endmodule

