
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '**' operator overloading.
// Using an user defined data type and an int as the arguments.

typedef struct {
    int s_real ;
    int s_imag ;
} simplex ;

function simplex cplx_pow (simplex a, int b) ;
    simplex c ;
    c.s_real = a.s_real ** b ;
    c.s_imag = a.s_imag ** b ;
    return c ;
endfunction 
 
bind ** function simplex cplx_pow(simplex, int) ;

module test(input bit clk, simplex a, int b, output bit out) ;
    simplex c ;
    
    always @ (posedge clk)
    begin
        c = a ** b ;

        if (c.s_real == (a.s_real ** b) && c.s_imag == (a.s_imag ** b))
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

