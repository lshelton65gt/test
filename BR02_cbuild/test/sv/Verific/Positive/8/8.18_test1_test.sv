
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests conditional operator '&&&'.

module test (input int i, int j, bit clk, output bit out);
    always @ (posedge clk)
    begin
        out = ((i >= j) &&& (i == j)) ? 1'b1 : 1'b0 ;
    end
endmodule

