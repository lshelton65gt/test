
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Real and shortreal data type can take the incrementor (++) and
// decrementor (--) operators and the increment and decrement carried
// out is by 1.0. This design checks this.

module test;

    shortreal r1;
    shortreal r2;
    shortreal r3;

    initial
    begin
        r1 = -4.05;
        r2 = --r1;
        r3 = r2--;

        if (r1 != -5.05 || r2 != -6.05)
            $display("pre decrement operator on shortreal is not working properly!");

        if (r2 != -6.05 || r3 != -5.05)
            $display("post decrement operator on shortreal is not working properly!");
    end

endmodule

