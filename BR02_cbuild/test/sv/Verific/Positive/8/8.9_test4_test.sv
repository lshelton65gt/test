
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing precedence of >>>= operator.

module test;
logic [3:0] test_logic1;
logic [3:0] test_logic2;

initial
begin
	test_logic1 = 4'b 11xz;
	test_logic2 = 4'b 11xz;
	test_logic1 >>>= (( test_logic1 >> 1) === 4'b011x);
	test_logic2 >>>= test_logic2 >> 1 === 4'b011x;
	if(test_logic1 === test_logic2)
		$display("Success");
end
endmodule
