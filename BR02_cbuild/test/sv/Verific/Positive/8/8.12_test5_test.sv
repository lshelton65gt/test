
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A warning should be  generated if the replication operator
// has got a non-positive value. This design defines a replication operator with
// a negative value.

module test (input in, output out) ;

    parameter p1 = {-1{1'b0}} ;
    parameter p2 = 7 ;
    bit [p1 : p2] b ;
    assign b[p2 - 1] = in ;
    assign out = ^b | in ;

endmodule
