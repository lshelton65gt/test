
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '<=' operator overloading.
// Using an user defined data type and an int as arguments.

typedef struct {
    int c_real ;
    int c_imag ;
} complex ;

function bit cplx_lth_eq_comp(complex a, int b) ;
    complex c ;
    if (a.c_real <= b && a.c_imag <= b)
        return 1'b1 ;
    else 
        return 1'b0 ;
endfunction 
 
bind <= function bit cplx_lth_eq_comp(complex, int) ;

module test(input bit clk, complex a, int b, output bit out) ;
    always @ (posedge clk)
        out = (a <= b) ;
endmodule

