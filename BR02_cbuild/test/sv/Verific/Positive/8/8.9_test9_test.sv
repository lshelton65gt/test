
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing precedence of %= operator.

module test;
bit [3:0] test_bit1;
bit [3:0] test_bit2;

initial
begin
	test_bit1 = 4'b 1101;
	test_bit2 = 4'b 1101;
	test_bit1 %= (2 ** 3);
	test_bit2 %= 2 ** 3;
	if(test_bit1 == test_bit2)
		$display("Success");
end
endmodule
