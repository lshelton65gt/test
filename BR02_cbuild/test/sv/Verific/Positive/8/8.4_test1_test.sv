
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): When binary operators has one operand of type 2 state and other
// operand of type 4 state, the result is of type 4 state. This design
// checks this behaviour with bit and logic type operands.

module test;

    bit [3:0] b1, b2;
    logic [0:3] l1, l2;

    initial
    begin
        b1 = 4'b0101;
        l1 = 4'bxx00;

        l2 = b1 | l1;  /* l2 = 4'bx101 */
        b2 = bit'(l2); /* b2 = 4'b0101 */

        if (l2 == b2)
            $display("binary operator with bit and logic does not produce logic type");
        else
            $display("binary operator with bit and logic produces logic type");

        l2 = b1 & l1;  /* l2 = 4'b0x00 */
        b2 = bit'(l2); /* b2 = 4'b0000 */

        if (l2 == b2)
            $display("binary operator with bit and logic does not produce logic type");
        else
            $display("binary operator with bit and logic produces logic type");
    end

endmodule

