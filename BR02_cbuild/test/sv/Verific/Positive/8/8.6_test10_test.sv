
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing shortreal type with allowed assignment operators.

module test(input in, output out) ;

    shortreal mod_short = 1.5 ;
    logic flag = 1 ;
    initial
    begin
            mod_short += 2.3 ;
            if(mod_short != 3.8)
            begin
                    flag = 0 ;
                    $display("Testing of += with shortreal type is a failure") ;
            end		
            mod_short -= 0.3 ;
            if(mod_short != 3.5)
            begin
                    flag = 0 ;
                    $display("Testing of -= with shortreal type is a failure") ;
            end		
            mod_short *= 1.5 ;
            if(mod_short != 5.25)
            begin
                    flag = 0 ;
                    $display("Testing of *= with shortreal type is a failure") ;
            end		
            mod_short /= 0.5 ;
            if(mod_short != 10.5)
            begin
                    flag = 0 ;
                    $display("Testing of /= with shortreal type is a failure") ;
            end		
            mod_short /= 2 ;
            if(mod_short != 5.25)
            begin
                    flag = 0 ;
                    $display("Testing of %%= with shortreal type is a failure") ;
            end		
            if(flag == 1)
                    $display(" Testing of all allowed assignment operators with shortreal type is verified") ;
    end

    assign out = in + 1 ;

endmodule
