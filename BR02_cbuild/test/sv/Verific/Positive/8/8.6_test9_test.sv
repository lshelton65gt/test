
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing real type with allowed assignment operators.

module test;
real mod_real = 1.5;
logic flag = 1;
initial
begin
	mod_real += 2.3;
	if(mod_real != 3.8)
	begin
		flag = 0;
		$display("Testing of += with real type is a failure");
	end		
	mod_real -= 0.3;
	if(mod_real != 3.5)
	begin
		flag = 0;
		$display("Testing of -= with real type is a failure");
	end		
	mod_real *= 1.5;
	if(mod_real != 5.25)
	begin
		flag = 0;
		$display("Testing of *= with real type is a failure");
	end		
	mod_real /= 0.5;
	if(mod_real != 10.5)
	begin
		flag = 0;
		$display("Testing of /= with real type is a failure");
	end		
	mod_real /= 2;
	if(mod_real != 5.25)
	begin
		flag = 0;
		$display("Testing of %%= with real type is a failure");
	end		
	if(flag == 1)
		$display(" Testing of all allowed assignment operators with real type is verified");
end
endmodule
