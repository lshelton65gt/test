
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a structure. There are two members in the structure.
// They are of type real and shortreal. These are used with the allowed
// operators on real and shortreal, other than ++ and -- (pre and post).

typedef struct {
    real r;
    shortreal sr;
} st_r_sr;

module test;

    st_r_sr r1;
    real r3;

    initial
    begin
        r1.r = 36.20;
        r1.sr = 7.10;

        r3 = (r1.r /= r1.sr); /* 5.1 */

        if (r3 != 5.10 || r1.r != 5.1 || r1.sr != 7.10)
            $display("/= operator on real/shortreal is not working properly!");

        r3 = (r1.r *= r1.sr); /* 36.2 */

        if (r1.r != 36.2 || r3 != 36.20 || r1.sr != 7.10)
            $display("*= operator on real/shortreal is not working properly!");

        r3 = (r1.r -= r1.sr); /* 29.1 */

        if (r3 != 29.10 || r1.r != 29.1 || r1.sr != 7.10)
            $display("-= operator on real/shortreal is not working properly!");

        r3 = (r1.r += r1.sr); /* 36.2 */

        if (r1.r != 36.2 || r3 != 36.20 || r1.sr != 7.10)
            $display("+= operator on real/shortreal is not working properly!");
    end

endmodule

