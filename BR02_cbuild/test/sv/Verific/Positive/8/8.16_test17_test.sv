
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '++' (pre-increment) operator overloading.
// Using an user defined data type and an int as arguments.

typedef struct {
    int c_real ;
    int c_imag ;
} complex ;


function complex cplx_pre_incr (ref complex a, int b) ;
    a.c_real = a.c_real + b ;
    a.c_imag = a.c_imag + b ;
    return a ;
endfunction 

bind ++ function complex cplx_pre_incr(complex, int) ;

module test(input bit clk, complex a, int b, output bit out) ;
    complex c ;
    
    always @ (posedge clk)
    begin
        c = ++a ;

        if (c.c_real == (a.c_real + b)  && c.c_imag == (a.c_imag + b))
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

