
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing various assignment operators.

module test;
reg [3:0] mod_reg1, mod_reg2;

initial
begin
	mod_reg1 = 4'b 1101;
	mod_reg2 = 4'b 0101;
	mod_reg1 &= mod_reg2;
	if(mod_reg1 == 4'b0101)
		$display("Testing &= is a success\n");
	mod_reg1 ^= mod_reg2;
	if(mod_reg1 == 4'b1010)
		$display("Testing ^= is a success\n");
	mod_reg1 |= mod_reg2;
	if(mod_reg1 == 4'b1111)
		$display("Testing |= is a success\n");
end
endmodule
