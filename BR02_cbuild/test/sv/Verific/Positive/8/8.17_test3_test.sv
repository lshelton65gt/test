
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : Using streaming concatenation to initialize a parameter.

module test (input int in, output int out) ;

    parameter bit [0:1]a = 1 ;
    parameter bit [0:1]b = 3 ;
    parameter bit [0:1]c = 2 ;
    parameter bit [0:5] y = {>>{ a, b, c }} ;
    
    parameter int i = 2 ;
    parameter int z = y[0:1] + i ;
    initial
        $display("parameter values are : a = %d,  b = %d, c = %d,  y = %d  i = %d,  z = %d", a, b, c, y, z) ;

    assign out = in + y ;

endmodule

