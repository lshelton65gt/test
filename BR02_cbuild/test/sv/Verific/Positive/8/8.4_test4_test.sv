
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The operators != returns an X if either operand contains an X or Z.
// This X is converted to 0 if the result is converted to type bit, e.g. when
// used as a condition of if statement or explicitly type casted. This
// design checks this behaviour.

module test;

    bit [3:0] b1, b2;
    logic [0:3] l1, l2;

    initial
    begin
        b1 = 4'b0;
        l1 = 4'bz;

        b2 = (b1 != l1);
        $display("4'b0 != 4'bz: %b(2-state)", b2);
        if (b2 != 0)
            $display("!= does not return X when one of its operands is X");

        l2 = (b1 != l1);
        $display("4'b0 != 4'bz: %b(4-state)", l2);
        if (l2 !== 'bx)
            $display("!= does not return X when one of its operands is X");

        b2 = (b1 == l1);
        $display("4'b0 == 4'bz: %b(2-state)", b2);
        if (b2 != 0)
            $display("== does not return X when one of its operands is X");

        l2 = (b1 != l1);
        $display("4'b0 == 4'bz: %b(4-state)", l2);
        if (l2 !== 'bx)
            $display("== does not return X when one of its operands is X");
    end

endmodule

