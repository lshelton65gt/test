
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing prevention of warning on size mismatch by casting.

module test;
integer mod_in = '1;
byte mod_byte;
initial
begin
	mod_byte = 8'(mod_in);
	$display("%b", mod_byte);
end
endmodule
