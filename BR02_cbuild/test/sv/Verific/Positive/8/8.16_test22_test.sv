
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '*' operator overloading. Using only the user
// defined data types as arguments.

typedef struct {
    int c_real ;
    int c_imag ;
} complex ;

function complex cplx_mul(complex a, complex b) ;
    complex c ;
    c.c_real = (a.c_real * b.c_real) + (a.c_imag * b.c_imag) ;
    c.c_imag = (a.c_imag * b.c_real) - (a.c_real * b.c_imag) ;
    return c ;
endfunction 
 
bind * function complex cplx_mul(complex, complex) ;

module test(input bit clk, complex a, complex b, output bit out) ;
    complex c ;
    int rel, img ;
    always @ (posedge clk)
    begin
        c = a * b ;
        
        rel = (a.c_real * b.c_real) + (a.c_imag * b.c_imag) ;
        img = (a.c_imag * b.c_real) - (a.c_real * b.c_imag) ;
        
        if (c.c_real == rel && c.c_imag == img)
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

