
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Unary reduction operators can be applied to any integer expression,
// including packed arrays, it will return a single value of type logic,
// if the packed type is 4-state value and bit if it is 2-state type.
// This design checks this behaviour.

module test;

    integer i1; /* 4-state */
    logic [7:0] l1; /* 4-state */

    initial
    begin
        i1 = 'bx;
        l1 = 8'bx;

        if ((&i1 !== 1'bx) || (~&i1 !== 1'bx))
            $display("& or ~& is not working properly");

        if (^l1 !== 1'bx)
            $display("^ is not working properly");
    end

endmodule

