
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : Testing streaming concatenation.

module test (input int in1, in2, in3, output int out) ;

    int a, b, c ;
    logic [10:0] up [3:0] ;
    logic [11:1] p1, p2, p3, p4 ;
    bit [96:1] y = {>>{a, b, c}} ;    // OK: pack a, b, c
    bit [99:0] d = {>>{a, b, c}} ;    // OK: b is padded with 4 bits
    initial
    begin
        a = in1 ;
        b = in2 ;
        c = in3 ;
        {>>{a, b, c}} = 96'b1 ;     // OK: unpack a = 0, b = 0, c = 1
        {>>{a, b, c}} = 100'b1 ;    // OK: unpack as above (4 bits unread
        {>>{p1, p2, p3, p4}} = up ; // OK: unpack p1 = up[3], p2 = up[2],
    end

    assign out = in1 + in2 + in3 ;

endmodule

