
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing various assignment operators.

module test;
reg [3:0] mod_reg;
initial
begin
        $display("Testing <<=, <<<=, >>= and >>>= shift operators ... \n");
	mod_reg = 4'b0101;
	mod_reg <<= 3;
	if(mod_reg == 4'b1000)
		$display("Testing <<= is a success\n");
	mod_reg >>>= 2;
	if(mod_reg == 4'b1110)
		$display("Testing >>>= is a success\n");
	mod_reg <<<= 2;
	if(mod_reg == 4'b1000)
		$display("Testing <<<= is a success\n");
	mod_reg >>= 3;
	if(mod_reg == 4'b0001)
		$display("Testing >>= is a success\n");
end
endmodule
