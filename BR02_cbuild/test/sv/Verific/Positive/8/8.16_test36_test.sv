
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '%' operator overloading.
// Using only the user defined data types as the arguments.

typedef struct {
    int s_real ;
    int s_imag ;
} simplex ;

function simplex cplx_mod (simplex a, simplex b) ;
    simplex c ;
    c.s_real = a.s_real % b.s_real ;
    c.s_imag = a.s_imag % b.s_imag ;
    return c ;
endfunction 
 
bind % function simplex cplx_mod(simplex, simplex) ;

module test(input bit clk, simplex a, simplex b, output bit out) ;
    simplex c ;
    
    always @ (posedge clk)
    begin
        c = a % b ;

        if (c.s_real == (a.s_real % b.s_real) && c.s_imag == (a.s_imag % b.s_imag))
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

