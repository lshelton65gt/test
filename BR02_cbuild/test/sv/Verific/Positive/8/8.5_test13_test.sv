
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '!=?' operator. Using logic packed array.

module test(input logic [7:0]val1, logic [7:0]val2, output bit out) ;
    always @ (val2)
    begin
        if (val1 !=? val2) out = 1'b1 ;
        else out = 1'b0 ;
    end
endmodule

