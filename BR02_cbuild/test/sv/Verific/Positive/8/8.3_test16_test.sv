
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses assignment operator '&=' in constant function.

module bench ;
    parameter p = 139, q = 240 ;
    test #(p,240) b1() ;
    initial
        $monitor(" %d,  %d", p, q) ;
endmodule

module test #(parameter p = 12, q = 21) ;
    int res =masking (p,q) ;

    function int calcu(int p, int q) ;
        p -= p++ - q-- ;

        $display(" %d,   %d", p, q) ;
        return p ;
    endfunction

    function int masking (bit[7:0] num, bit[7:0] mask) ;
         num &= mask ; 
         return num ;
    endfunction

    defparam bench.q = masking( p, q) ;
    initial
       $display("%d", res) ;
endmodule

