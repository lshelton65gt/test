
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of structure literal using data_type: value and 
// member_name : value syntax.

module test ;
  typedef struct{
     int a ;
     bit b ;
     string c ;
  } st ;

  st check ;
  initial begin
     check = '{int : 5, c:"asdww", bit:0} ;
     $display("a = %d, b= %b, c = %s",check.a, check.b, check.c) ;
  end
endmodule
  
