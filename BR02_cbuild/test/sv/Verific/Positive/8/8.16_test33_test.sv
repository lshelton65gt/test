
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the ++ (post-increment) operator overloading.
// Using only the user defined data types as arguments.

typedef struct {
    int c_real ;
    int c_imag ;
} complex ;

function complex cplx_post_incr (ref complex a, int i) ;
                                                
    complex c ;
    c.c_real = a.c_real ;
    c.c_imag = a.c_imag ;
   
    a.c_real = a.c_real + 1 ;
    a.c_imag = a.c_imag + 1 ;
    
    return c ;
endfunction 

bind ++ function complex cplx_post_incr(complex, int) ;

module test(input bit clk, complex a, output bit out) ;
    complex c ;

    always @ (posedge clk)
    begin
        c = a++ ;

        if (c.c_real == (a.c_real - 1) && c.c_imag == (a.c_imag - 1))
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

