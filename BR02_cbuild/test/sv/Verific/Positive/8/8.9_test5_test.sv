
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing precedence of ++ and -- operator.

module test;
int test_int1, test_int2;

initial
begin
	test_int1 = 1;
	test_int2 = 1;
	test_int1 += ((++test_int1) << (test_int1--));
	test_int2 += ++test_int2 << test_int2--;
	if(test_int1 == test_int2)
		$display("Success");
end
endmodule
