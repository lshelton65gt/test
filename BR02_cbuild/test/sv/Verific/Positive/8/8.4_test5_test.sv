
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the state of result if one of the operands is of 2 state.

module test;
bit [3:0] mod_bit = 4'b 1z00;
logic [3:0] mod_log = 4'b 10x0;
bit [3:0] bit_result;
logic [3:0] log_result;
int mod_int = 10;
integer mod_in = 'x;
int int_result;
integer in_result;

initial 
begin
    bit_result = mod_bit + mod_log;
    log_result = mod_bit + mod_log;
    int_result = mod_int + mod_in;
    in_result  = mod_int + mod_in;
    if(bit_result == 4'b0 && log_result === 4'bx)
    begin
	if(int_result == 10 && in_result === 'bx)
       	    $display("Result is 4 state");
    end
    else
	$display("Result is 2 state");
end
endmodule
