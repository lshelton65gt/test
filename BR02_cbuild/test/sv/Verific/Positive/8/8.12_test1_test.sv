
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Concatenations are treated as packed vectors of bits
// (or logic, if any operand is of type logic). This design checks whether
// the concatenation is treated a logic if any of the operands is of type
// logic.

module test;

    bit [3:0] b1;
    bit [7:0] b2;
    logic [3:0] l1;
    logic [7:0] l2;

    initial
    begin
        b1 = (l1 = 4'b0x0x);

        b2 = bit'((l2 = { b1, l1 }));

        if (b2 === l2)
            $display("Error: concatenation is not treated as logic type when one operand is of type logic");
    end

endmodule

