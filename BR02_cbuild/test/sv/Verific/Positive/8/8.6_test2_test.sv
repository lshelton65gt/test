
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Real and shortreal data types can take the incrementor (++) and
// decrementor (--) operators and the increment and decrement carried
// out is by 1.0. This design checks this.

module test;

    real r1;
    real r2;
    real r3;

    initial
    begin
        r1 = -4.05;
        r2 = ++r1;
        r3 = r2++;

        if (r1 != -3.05 || r2 != -2.05)
            $display("pre increment operator on real is not working properly!");
        else
            $display("Success");

        if (r2 != -2.05 || r3 != -3.05)
            $display("post increment operator on real is not working properly!");
        else
            $display("Success");
    end

endmodule

