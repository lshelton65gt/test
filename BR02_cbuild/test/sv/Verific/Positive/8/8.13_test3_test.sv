
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests assignment pattern using it as initial
// value.

typedef struct {  int name ; 
                  int roll ;
               } student ;

module test(input int in, output out) ;

    bot #(.width(3),.t1(student)) b1() ;

    assign out = $onehot(in) ;

endmodule

module bot #(parameter width = 10, parameter type t1 = int ) ;
    t1 s = '{name : 21, roll : 41} ;
    initial
        $display("Instantiating module bot with parameter width = %2d", width) ;
endmodule 

