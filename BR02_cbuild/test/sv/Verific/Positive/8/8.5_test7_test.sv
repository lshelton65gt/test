
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the '==?' operator in constant function, using 
// packed array of type logic as operands of this operator.

module test(input in, output out) ;

    parameter p1 = func1() ; // After static elab. p1 = 11.
    parameter p2 = func2() ; // After static elab. p2 = 11.
    parameter p3 = func3() ; // After static elab. p3 = 11.
    parameter p4 = func4() ; // After static elab. p4 = 11.
    parameter p5 = func5() ; // After static elab. p5 = 11.

    initial 
        $display("p1 = %1d,   p2 = %1d,   p3 = %1d,   p4 = %1d,   p5 = %1d", p1, p2, p3, p4, p5) ;
    
    function integer func1() ;
        logic [7:0] val1 = 8'b10111001 ;
        logic [7:0] val2 = 8'b10111001 ;

        if (val1 ==? val2) 
            return 11 ;
        else
            return 10 ;
    endfunction
    
    function integer func2() ;
        logic [7:0] val1 = 8'b10111001 ;
        logic [7:0] val2 = 8'b1x111x01 ;

        if (val1 ==? val2) 
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func3() ;
        logic [7:0] val1 = 8'b10111001 ;
        logic [7:0] val2 = 8'bx01110z1 ;

        if (val1 ==? val2) 
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func4() ;
        logic [7:0] val1 = 8'b10111001 ;
        logic [7:0] val2 = 8'bx011100z ;

        if (val1 ==? val2) 
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func5() ;
        logic [7:0] val1 = 8'b10111001 ;
        logic [7:0] val2 = 8'b1z11z001 ;

        if (val1 ==? val2) 
            return 11 ;
        else
            return 10 ;
    endfunction
    assign out = in ;

endmodule

