
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a structure. There are two members in the structure.
// They are of type real and shortreal. These are used with the allowed
// operators (pre and port ++ and --) on real and shortreal.

typedef struct {
    real r;
    shortreal sr;
} st_r_sr;

module test;

    st_r_sr r1, r2;
    real r3, r4;

    initial
    begin
        r1.r = +0.01;
        r1.sr = 0.01;
        r2.r = 0-0.01;
        r2.sr = -0.01;

        r3 = r1.r++;
        r4 = ++r1.r;

        if (r3 != 0.01 || r4 != 2.01)
            $display("pre/post decrement operator on real is not working properly! r3=%g r4=%g", r3, r4);
        else 
            $display("Success");

        r3 = r2.r--;
        r4 = --r2.r;

        if (r3 != -0.01 || r4 != -2.01)
            $display("pre/post decrement operator on real is not working properly! r3=%g r4=%g", r3, r4);
        else 
            $display("Success");

        r3 = r1.sr++;
        r4 = ++r1.sr;

        if (r3 != 0.01 || r4 != 2.01)
            $display("pre/post decrement operator on real is not working properly! r3=%g r4=%g r1.sr = %g", r3, r4, r1.sr);
        else 
            $display("Success");

        r3 = r2.sr--;
        r4 = --r2.sr;

        if (r3 != -0.01 || r4 != -2.01)
            $display("pre/post decrement operator on real is not working properly! r3=%g r4=%g", r3, r4);
        else 
            $display("Success");
    end

endmodule

