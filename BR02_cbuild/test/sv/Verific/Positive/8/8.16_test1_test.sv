
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the '+' operator overloading using 'bind'
// directive. Only user defined data types are used as operands to this operator.

typedef struct {
    int c_real ;
    int c_imag ;
} complex ;

function complex cplx_add (complex a, complex b) ;
    complex c ;
    c.c_real = a.c_real + b.c_real ;
    c.c_imag = a.c_imag + b.c_imag ;
    return c ;
endfunction 
 
bind + function complex cplx_add(complex, complex) ;

module test(input bit clk, complex a, complex b, output bit out) ;
    complex c ;
    
    always @ (posedge clk)
    begin
        c = a + b ;

        if (c.c_real == (a.c_real + b.c_real) && c.c_imag == (a.c_imag + b.c_imag))
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

