
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function without any argument but 
// with the parenthesis and it is called with the parenthesis without any argument.

module test (i, o) ;

    parameter p = 8 ;

    input [p-1:0] i ;
    output [0:p-1] o ;

    assign o = ~i ;

    initial
    begin
        $display("%d", fn1()) ;
    end

    function integer fn1() ;
        return 8 ;
    endfunction

endmodule

