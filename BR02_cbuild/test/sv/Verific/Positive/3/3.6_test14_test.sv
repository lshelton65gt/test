
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests casting of string literals to an unpacked array of
// type logic of width 4. 

typedef logic [0:3] my_type[11:0];
typedef logic [0:3][7:0]  restore_type ;
module test ;

    my_type obj = my_type'("String"); // String property lost due to casting
    restore_type str = { obj[7], obj[6], obj[5], obj[4], obj[3], obj[2], obj[1], obj[0]}; // Attempt to restore string literal
    
    initial
    begin
        #10 for (int i = 11; i >= 0; i=i-2)
            begin
                $display("%b", obj[i]);
                $display("%b", obj[i-1]);
            end
        $display("Restored literal") ;
        for (int i =0; i <= 3; i++)
            $display("%c", str[i]) ;
    end

endmodule

