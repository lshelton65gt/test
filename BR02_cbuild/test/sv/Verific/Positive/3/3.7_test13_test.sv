
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Use of multi concat operator to represent
// array literal.

module test(int1, real1) ;

  output int int1 [4:5][2:0] = '{ '{3 {10}}, '{3 {20}}} ;
  output real real1 [1:0][3:1] = '{ 2 {'{3 {5.6}}}} ;

endmodule
 
module bench ;

    int int1 [4:5][2:0] ;
    real real1 [1:0][3:1] ;

    test I(int1, real1) ;

    initial
    begin
        #10 ;
        if((int1[4][2] == 10) && (real1[0][1] == 5.6))
            $display("Success") ;
        else
            $display("Failed") ;
    end

endmodule

