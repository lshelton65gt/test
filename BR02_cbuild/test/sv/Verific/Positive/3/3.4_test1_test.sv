
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of real type literals.

module test (output real out1, out2, out5,
                    output shortreal out3, out4)  ;

    initial
    begin
       out1 = 1.7e308 ;
       out3 = shortreal'(out1)  ;
       out5 = real'(out3)  ;
       out4 = 3.4e38 ;
       out2 = real'(out4)  ;
    end

endmodule

