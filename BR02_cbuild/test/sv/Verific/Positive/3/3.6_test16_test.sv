
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests casting of string literals to a packed bit array.

typedef bit[7:0] my_type;

module test(output my_type obj);

    initial
        obj = my_type'("String");

endmodule

module bench;

    my_type bench_obj;

    test instan_mod(bench_obj);

    initial
        #5 $display("%c", bench_obj);

endmodule

