
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses casting to assign real type value to a time
// type data element. This checks whether it is possible or not. If possible it then
// checks what is the possible time unit.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    time time1 = time'(4.9);
    time time2 = time'(1.2);

    always@(posedge clk)
    begin
        if (time1 > 5fs)
            if (time1 > 5ps)
                if (time1 > 5ns)
                    if (time1 > 5us)
                        if (time1 > 5ms)
                            $display("sec");
                        else
                            $display("ms");
                    else
                        $display("ns");
                else
                    $display("ns");
            else
                $display("ps");
        else
            $display("fs");

        out1 = in1 + time1;
        out2 = time2 - in2;
    end

endmodule

