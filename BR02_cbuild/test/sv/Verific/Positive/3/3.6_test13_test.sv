
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests casting of string literals to a packed array of
// logic type. Refer section 2.6 for details.

typedef  logic[63:0] my_type;

module test (output my_type obj);

    initial
       obj = my_type'("String");

endmodule

module bench;

    my_type bench_obj;
    test instan_mod(bench_obj);

    initial
        $display("%s", bench_obj);

endmodule

