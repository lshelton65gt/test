
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function having unpacked array as 
// port. Array literal having structure literal is passed as actual to such formal.

typedef struct {
                 logic [3:0]a;
                 int b;
               }myType;

function bit [3:0] func;
    input myType in[1:0];
    bit [3:0]log;
    log = in[1].a;
    return log;
endfunction

module test(input bit clk, output logic [3:0]log) ;
    always @ (posedge clk)
        log = func('{'{4'bz, 10}, '{4'b1, 5}});
endmodule

module bench ;
    logic [3:0]log;
    bit clk = 1'b1 ;

    always #10 clk = ~clk ;
    test m1(clk, log) ;
    always @ (negedge clk)
        $display ("output logic (should be 4'b0) = %b", log) ;
    initial #100 $finish() ;
endmodule

