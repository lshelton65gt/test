
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the assignment of string literals to a packed
// array and also tests the truncation mismatch.

module test(input [0:31]in, output out) ;

    reg [11:0][7:0] array1, array2 ;
    initial
    begin
        array1 = "Hello World" ;                // array1 will contain "Hello World"
        array2 = "Test for justification" ;     // array2 will contain "ustification"
        $display("array1 = %s", array1) ;
        $display("array2 = %s", array2) ;
    end

    assign out = $onehot(in) ;

endmodule

