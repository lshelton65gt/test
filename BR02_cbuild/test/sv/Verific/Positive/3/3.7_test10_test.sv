
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Unpacked array can be assigned in procedural assignment
// using array literal if casting to proper context can be used instead of 
// assignment pattern.This design assigns an array providing context by casting.

module test(input clk, output bit array [3:0]);

    typedef bit myreg [3:0] ; // array of 4 regs

    always @ (posedge clk)
	    array = myreg'{1'b1, 1'b0, 1'b1, 1'b1}; // concatenation gets context from cast, it will
                                    // be treated as array literal

endmodule

module bench;
    bit array [3:0];
    bit clk = 1'b0 ;
    test instan_mod(clk, array) ;
    always #10 clk = ~clk ;

    initial
      #100 $finish ;
    initial 
	    $monitor("array[0] = %b array[1]=%b array[2]=%b array[3]=%b",array[0], array[1], array[2], array[3]) ;
endmodule

