
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of time literals. Use time units fs and ps.

module test(output reg out, input in) ;

    parameter intrinsic_delay = 10ps,
              path_delay = 5.6fs,
              set_up_time = 0.78ps ;

    always @(in)
         out = #(intrinsic_delay + path_delay + set_up_time) in ;

endmodule
              
