
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Use escape sequence in string literal for characters that
// are supported as special characters.

module test(input in, output out) ;

    bit [79:0] str1 ;
    bit [79:0] str2 ;

    assign str1 = "str\ning", // = str<new_line>ing
           str2 = "st\bring" ; // = sring

    initial
    begin
        if (str2 ^ "sring")
            $display("success") ;
    end

    assign out = in ;

endmodule

