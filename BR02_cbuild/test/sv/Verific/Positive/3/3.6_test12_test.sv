
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests casting of string literals to an unpacked array
// of byte type.

typedef byte my_type[5:0];

module test ;

my_type mod_obj = my_type'("String");

initial
begin   
    for(int i = 7; i >= 0; i--)
        $display("%c", mod_obj[i]);
end

endmodule

