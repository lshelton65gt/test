
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests casting of array literals to user defined type.

module test(output reg [3:0] array);

    typedef reg[3:0] myreg;

    initial
        array = myreg'({1'b1, 1'b0, 1'b1, 1'b1});

endmodule

module bench;

    wire [3:0] array;

    test instan_mod(array);

    initial 
        #5 $display("array = %b",array);

endmodule

