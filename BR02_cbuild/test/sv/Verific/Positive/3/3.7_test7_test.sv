
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether casting is necessary while declaring and 
// initializing an user defined type object.

typedef union packed {
    int a;
    integer b;
    bit [31:0] c;
} myType;

module test;
myType a = 10;

initial
begin
     if(a.c == 32'b1010)
         $display("TESTING SUCCESSFUL");
end

endmodule

