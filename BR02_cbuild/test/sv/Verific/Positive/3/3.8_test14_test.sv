
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses enum variable inside a structure and 
// initialize that structure using structure literal.

typedef enum {north, south, east, west} direction ;
typedef struct {  int up ;
                  int down ;
                  direction dir ;
               }  route ;

module test ;
    route r1 = '{up:3,down:31, dir:west} ;

    initial begin
        r1.dir = east ;
        #1 $display("Struct route: %d, %d %s", r1.up, r1.down, r1.dir.name());
     end

endmodule

