
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design assigns structure type variables by
// continuous assignment providing structure literals as value.

module test;

    typedef struct packed
            {
                bit [10:0][7:0] str; 
                byte single_char ;
                bit [7:0]  bit_val ;
            } struct1 ;

    struct1 st1, st2 ;

    assign st1 = '{ "char string", "C", "B" } ,
           st2 = '{ "my string\n", "D", 8'b10101010 };

endmodule

module bench ;

    test I() ;

    initial
    begin
        #10 ;
        if((I.st1.str == "char string") &&
           (I.st2.single_char== "D"))
            $display("Success") ;
        else
            $display("Failed") ;
    end

endmodule

