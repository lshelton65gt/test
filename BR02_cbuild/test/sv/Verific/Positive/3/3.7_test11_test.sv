
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the use of replicate operators in initializing
// an unpacked array.

module test(array1);
    output int array1 [1:0][3:0] ='{'{2{0,1}}, '{2{1,0}}};
endmodule

module bench;
    int array1 [1:0][3:0];
    test instan_mod(array1);
    initial
    begin
        #10 ;
	    $display("array1 = ");
	    for(int i = 3; i>=0 ; i--)
	    	for(int j = 1; j >= 0; j = j-1)
	    		$display("%d", array1[i][j]);
      end     
endmodule                   

