
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing passing of unsized values to module ports.

module mod(in, out);
parameter int p = 7;
input bit [p:0]in;
output bit [p:0]out;

initial
begin
     for(int i = p; i >= 0; i = i - 2)
     begin
         out[i] = ~in[i];
     end
end

endmodule

module test;
bit [3:0]in, out;

mod #(.p(3))instanMod(in, out);

initial
begin
     in = '0 ;
     #5 $display("input = %b, output = %b", in, out); 
end

endmodule

