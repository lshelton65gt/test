
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If the type of the literal cannot be determined then the
// literal should be casted to the appropriate type.

module test(output bit b[1:0]) ;

    typedef bit duplet [1:0];

    initial 
        b = duplet'{1'b0, 1'b1} ;

endmodule

