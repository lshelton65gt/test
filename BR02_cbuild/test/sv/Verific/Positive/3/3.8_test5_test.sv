
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of structure literals.

module test ;

    typedef struct {
        bit [10:0][7:0] str; 
        real real_num ;
        bit [7:0]  bit_val ;
    } struct1 ;

    struct1 st1, st2 ;

    assign st1 = '{ "char string", 5.8e-09, "B" } ,
           st2 = '{ "my string\n", 6.9e-05, 8'b10101010 } ;

endmodule

