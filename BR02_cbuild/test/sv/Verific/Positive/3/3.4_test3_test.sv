
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Type casting using real data type.

module test (input clk,
             input [7:0] in1, in2,
             output reg [7:0] out1, out2);

    always@(posedge clk)
    begin
        out1 = (out2 = '0);

        if (real'(shortreal'(32.5)) == 32.5)
        begin
            out1 = in1 ^ in2;
            out2 = in2 & in1;
        end
        else
            $display("Failure");
    end

endmodule

