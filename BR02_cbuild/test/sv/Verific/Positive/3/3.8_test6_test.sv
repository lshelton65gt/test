
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of structure literals. Use nested braces to initialize structure.

module test ;

    typedef struct {
        int int1;
        real real1;
    } st1 ;

    typedef struct {
        bit [7:0] bit1;
        byte str [7:0];
    } st2 ;

    st2 stArr[1:0] = '{ '{ "V", "String\n"}, '{ 8'b10101010, "my name"}} ;

    typedef struct { 
        st1 s;
        shortreal r ;
    } st3 ;

    st3 s;

    assign s = '{ '{9, 4.5}, 7.3e-09 } ;

endmodule

