
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a module with parameters. One of the
// parameter is specified with data type. The data type is time. This is used to
// set the value of a time type data element. The default value of the parameter
// is overridden from the bench file with another value and time unit. This checks
// whether time parameters/literals are properly handled between modules.

module test#(parameter time tm = 98.3fs, parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    time time1 = tm;

    always@(posedge clk)
    begin
        out1 = in1 + time1;
        out2 = time1 - in2;
    end

endmodule

