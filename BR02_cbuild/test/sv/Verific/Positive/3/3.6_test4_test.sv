
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of string literals. Check if a string literal can be
// assigned to an unpacked array.

module test(input in, output out) ;

    byte str1 [0:11] = "Hello World\n" ;
    byte str2 [0:6] = "String\n" ;
    byte c ;

    initial
    begin
        if((str1[0] == "H") && (str2[0] == "S"))
            $display("Success") ;
        else
            $display("Failed");
    end

    assign out = in ;

endmodule

