
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a module having structure type port. 
// Structure literal is used as actual to structure type formal.

typedef struct packed {
                 bit [31:0][3:0]a;
                 bit [3:0]sel;
               } myType;

function myType func;
    input bit [31:0][3:0]s; 
    input bit[3:0] funcBit;
    myType var1 ;
    var1 = {s[3:0], funcBit};
    return var1;
endfunction

module test(var1);
    input myType var1;
    bit [31:0][3:0]b;
    bit [3:0]modBit2;

    bit [31:0][3:0]a; 
    bit [3:0]modBit1;

    assign {a[3], modBit1} = var1;

    always @ (*)
         {b[0], modBit2} = func(a, modBit1);
endmodule

module bench ;
    bit [31:0][3:0]x ;
    bit [3:0]y ;
    int i;

    initial
    begin
         y = '1 ;
         repeat(20)
         begin
             for(i = 3; i >=0 ; i--)
             begin
                 x[i] = $random ;
                 #5 $monitor("input int %d = output int %d", x[i], y[i]) ;
             end
             y = ~y ;
         end
    	 $monitor("input bit %b = output bit %b", y, instanMod.modBit2) ;
    end
    test instanMod({x, y}) ;
endmodule

