
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of string literals. Checking the left/right justification,
// that occurs, when a string literal is assigned to a packed array.

module test(input in, output out) ;

    // String literals can be assigned to unpacked arrays
    bit [0:15] [0:7] str1 = "Hello World" ;
    bit [0:10] [0:7] str2 = "String\n" ;

    initial
    begin
        if((str1[15] == "d") && (str2[4] == "S"))
            $display("String literals assigned to packed array are right justified") ;
        else
            $display("String literals assigned to packed array are left justified") ;
    end

    assign out = in ;

endmodule

