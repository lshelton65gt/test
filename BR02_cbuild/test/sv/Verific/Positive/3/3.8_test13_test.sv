
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests initialization of the structure variable using
// structure literal.

typedef struct {
           int A;
           struct {
              int B, C;
           } BC1, BC2;
       } check ;

module test ;
       check ABC = '{A:1, BC1:'{B:2, C:3}, BC2:'{B:4,C:5}};
       check DEF = '{default:10};
       initial begin
           $display(" %d %d %d %d %d",ABC.A, ABC.BC1.B, ABC.BC1.C,ABC.BC2.B, ABC.BC2.C) ;
           $display(" %d %d %d %d %d",DEF.A, DEF.BC1.B, DEF.BC1.C,DEF.BC2.B, DEF.BC2.C) ;
       end
endmodule

