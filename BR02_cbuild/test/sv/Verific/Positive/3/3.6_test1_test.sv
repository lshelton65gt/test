
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): According to LRM IEEE-1800 sec 4.7: All "\0" characters in the
// string literal are ignored (i.e., removed from the string). This design assigns
// a string to a concatenation of two data elements. The string contains a \0 in it.

module test(input in, output out) ;

    bit [3:0][7:0] str1 ;
    bit [7:0][7:0] str2 ;

    bit [31:0] str3 ;
    bit [63:0] str4 ;

    assign { str1, str2 } = "Hi\0\0Everyone" ;
    assign { str3, str4 } = "Hi\0\0Everyone" ;

    initial
    begin
        $display("'%s', '%s'", str1, str2) ;
        $display("'%s', '%s'", str3, str4) ;
    end

    assign out = ~in ;

endmodule

