
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test that structure literals can be specified in various 
// ways like specifying the data type or member name

typedef struct{ int a ;
                int b ;
                string name ;
              } student ;

module test ;
   student s1 ;
   
   initial begin
      #1 s1 = '{int:23, name: "qwer"} ;
      #1 $display("Struct student: %d, %d", s1.a, s1.b) ;
      #1 $display(s1) ;
   end

endmodule

