
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the special new string characters like \f (form
// feed), \v (vertical// tab), \a (Bell), \x (Hex number).

module test;

reg  [7:0] out1, out2, out3, out4;
initial
begin
    out1 = "\f";
    out2 = "\v";
    out3 = "\a";
    out4 = "\xA";
    $display("out1 = %s out2 = %s out3 = %s out4 = %d",out1, out2, out3, out4);
end

endmodule

