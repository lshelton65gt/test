
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of assignment of string literals to packed arrays,
// and also testing of truncation in case of size mismatch and zero termination.

module test;
    string str = "Test for right justification";
    bit [7:0] [7:0] array1 = "Hello World";
    bit [11:0] [7:0] array2 = str;
    initial
    begin
        #5 $display(" array1 = ", array1);
        if(array1[0] == 0)
            $display("\n  Zero Terminated  \n");
        else
            $display("\nStrings assigned to packed array does not follow zero termination rule");
        $display("array2 = ", array2);

        if(array2!="Test for right justification" && $bits(array2) < (str.len*8))
            $display("\n  String str has been truncated by assignment  \n");

    end

endmodule

