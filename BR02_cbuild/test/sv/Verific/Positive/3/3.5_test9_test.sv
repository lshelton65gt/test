
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a task. The arguments of the task is of
// type time. The task is invoked with time type data elements to check whether time
// type data elements are properly handled between task invocation.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    time tm1, tm2, tm3;

    always@(posedge clk)
    begin
        tm1 = 21.3ps;
        tm2 = 13.2us;

        time_task(tm1, tm2, tm3);

        if (tm3 >= 30ps)
        begin
            out1 = in1 ^ in2;
            out2 = in1 & in2;
        end
        else
        begin
            out1 = in1 | in2;
            out2 = in1 + in2;
        end
    end

    task time_task(input time t1, t2, output time t3);
        if (t1 > t2)
            t3 = t1 + t2;
        else
            t3 = t2 - t1;
    endtask

endmodule

