
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function. The arguments of the function
// is of type time. The function is called with time type data elements to check
// whether time type data elements are properly handled between function calls.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    time tm1, tm2;

    always@(posedge clk)
    begin
        tm1 = 21.3ps;
        tm2 = 13.2us;

        if (time_func(tm1, tm2) != 0)
        begin
            out1 = in1 ^ in2;
            out2 = in1 & in2;
        end
        else
        begin
            out1 = in1 | in2;
            out2 = in1 + in2;
        end
    end

    function int time_func(time t1, t2);
        if (t1 > t2)
            return 1;

        return 0;
    endfunction

endmodule

