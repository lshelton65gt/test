
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a module with a type parameter. 'time'
// is specified as the default value of the type parameter. Data elements has been
// declared of this type. They are assigned constant literal values in this testcase.

module test#(parameter type tt = time, parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    tt time1 = 5.9, time2 = 15;

    always@(posedge clk)
    begin
        out1 = in1 + time1;
        out2 = time2 - in2;
    end

endmodule

module bench;

    parameter width = 4;

    reg clk;
    bit [width-1:0] in1, in2;
    logic [width-1:0] out1, out2;

    test #(.w(width)) t_mod (clk, in1, in2, out1, out2);

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in1, in2 } = $random;

    initial
    begin
        clk = 0;
        $monitor("in1 = %d, in2 = %d, out1 = %d, out2 = %d", in1, in2, out1, out2);
        #501 $finish;
    end

endmodule

