
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This test case defines a function with array formal. In 
// functional call structure literal is passed as actual. Structure literal 
// used type:value and name:value syntax.

typedef struct { logic l[4];
                 bit b;
               } check;

function bit func(check ch[2]) ;

    bit b1 ;
    b1 = ch[1].b ;
    $display("ch[1]--- l: %b, b: %b", ch[1].l[1], ch[1].b) ;
    $display("ch[0]--- l: %b, b: %b", ch[6].l[0], ch[0].b) ;
    return b1 ;

endfunction

module test ;

    logic l1 ;

    initial begin
        l1 =  func('{'{logic: 4'b1, b:0}, '{logic: 4'bz, b:1}}) ;
        $display("return from function: %b", l1) ;
    end

endmodule

