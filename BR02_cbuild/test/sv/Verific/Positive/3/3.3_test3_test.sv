
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the passing of unsized values to interface ports.

typedef struct packed {
    int a;
    bit [3:0]b;
} myType;

interface i;
    logic [7:0]intLog;
    myType a;
endinterface

module mod(interface a);

    int x;
    myType y;

    assign x = a.intLog;
    assign y = a.a;

endmodule

module test ;

    i a();

    mod instanMod(a);

    initial
    begin
        a.intLog = '1;
        a.a = '0;
        $display("input logic %b = output logic %b", a.intLog, instanMod.x);
        $display("input myType %b = output myType %b", a.a, instanMod.y);
    end

endmodule

