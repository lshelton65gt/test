
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A string literal must be contained in a single line unless
// the new line is immediately preceded by a \ (back slash).

module test(input in, output out) ;

    string a = "This is to check \
                  the property of string" ;
    initial
        $display("Value of string a: %s and size of a: %d", a, $bits(a)) ;

    assign out = in ;

endmodule

