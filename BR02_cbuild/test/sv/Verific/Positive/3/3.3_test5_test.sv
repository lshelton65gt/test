
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the initialization with unsized literal values (with `).

module test(output reg out1, output reg [7:0] out2, 
            output reg [7:0] out3, output reg [40:0] out4,
            output reg [40:0] out5);

initial
begin
    out1 = '1;
    out2 = '1;
    out3 = 'z;
    out4 = '0;
    out5 = 'x;
end

endmodule

module bench;

wire out1;
wire [7:0] out2, out3;
wire [40:0] out4, out5;

test inst_mod1(out1, out2, out3, out4, out5);

initial
     #10 $display("out1 = %b\t out2 = %b\t out3 = %b\t out4 = %b\t out5 = %b", out1, out2, out3, out4, out5);   

endmodule

