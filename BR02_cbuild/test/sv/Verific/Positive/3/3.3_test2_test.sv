
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing passing of unsized values to function port.

function int func;
    input  bit[7:0] funcBit;
    real r = $bits(funcBit);
    int out = int'(r);
    return out;
endfunction

module test;
logic [7:0]modBit;
int funcOut;

initial
begin
     modBit = '0;
     #5 modBit = '1;
     #5 modBit = 'x;
     #5 modBit = 'z;
     $monitor("logic input = %b, integer output = %d", modBit, funcOut);
end

always @(modBit)
     funcOut = func(modBit);

endmodule

