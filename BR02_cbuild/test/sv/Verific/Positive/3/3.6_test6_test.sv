
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of string literals. Check proper truncation by assigning
// literals larger than the storage size to both packed and unpacked arrays.

module test(input in, output out) ;

    byte str1 [0:7] = "byte string" ;
    bit [0:3][0:7] str2 = "HELLO" ;
    byte str3 [0:7] = "byte string" ;
    bit [0:11] [0:7] str4 = str2 ;

    initial
    begin
        if(str2 == "ELLO")
            $display("string when assigned to packed array is truncated from left hand side") ;
        else
            $display("Failed") ;

        if(str3[0] == "b")
            $display("string when assigned to unpacked array is truncated from right hand side") ;
        else
            $display("Failed") ;
    end

    assign out = in ;

endmodule

