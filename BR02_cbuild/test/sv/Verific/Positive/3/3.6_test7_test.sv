
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Check how the string is interpreted when a zero termination
// is used inside the string and that string is assigned to an unpacked array.

module test(input in, output out) ;

    byte str1[0:12] = "byte s\0tring" ;
    byte str2 [0:12] = str1 ;

    initial
        if (str1==str2) $display("null character in str1 didn't effect it") ;

    assign out = in ;

endmodule

