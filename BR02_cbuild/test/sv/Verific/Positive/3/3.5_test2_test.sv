
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the use of integer and fixed point format time
// literals with time unit specified. Refer section 2.5 for details.

module test (input in, output out) ;

    reg [3:0] r;
    initial
    begin
        r = 'z;
        #5 r = 'x;
        #0.25 r = '1;
        $monitor($time,,,"out = %b", r);
    end
    assign out = in ;

endmodule

