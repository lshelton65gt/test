
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array literals. Use concatenation operator for initialization.

module test(int1, real1) ;
    output int int1 [1:3][1:3] ;
    output real real1 [0:1][0:2] ;

    assign int1 = '{'{0, 1, 2}, '{4, 5, 6}, '{10, 11, 12}} ; 
    assign real1 = '{'{1.2, 2.3, 3.4}, '{4.5, 5.6, 6.7e-03}} ;

endmodule
 
