
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of string literals. Use special characters in string literals.

module test(input [0:7]in, output out) ;

    bit [8:0][7:0] str1 = "Str\ving1" ;
    bit [8:0][8:0] str2 = "Strin\fg2" ;
    bit [8:0][9:0] str3 = "St\aring3" ;
    bit [8:0][4:0] str4 = "\xhh" ;

    initial
    begin
        #10 ;
        $monitor("str1=%s , str2=%s , str3=%s , str4=%s ", str1, str2, str3, str4) ;
    end

    assign out = $onehot(in) ;

endmodule

