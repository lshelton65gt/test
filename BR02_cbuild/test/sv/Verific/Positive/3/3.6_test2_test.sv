
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of string literals. Use unpacked arrays of byte to store
// string literals.

module test(input in, output out) ;

    // String literals can be assigned to unpacked array (section 3.6, page 13 of IEEE 1800 LRM) 
    byte str1 [0:15] = "Hello World\n" ;
    byte str2 [0:10] = "String\n" ;

    initial
    begin
        if(str1[0] == "H")
            $display("Success") ;
        else
            $display("Failed") ;
        if(str2[0] == "S") 
            $display("Success") ;
        else
            $display("Failed") ;
    end

    assign out = in + 1 ;

endmodule

