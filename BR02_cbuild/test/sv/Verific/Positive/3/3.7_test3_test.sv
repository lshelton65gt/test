
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the use of replicate operators in initializing
// an unpacked array. Refer section 3.7 of IEEE 1800 LRM for details.

module test(array1);
   function f (input int array1 [1:0][3:0] = '{'{2{0,1}}, '{2{1,0}}}) ;
      f = 1;
   endfunction
   output int array1 [1:0][3:0]; 
   initial array1  = '{'{4{1}}, '{4{0}}};
endmodule

module bench;
    int array1 [1:0][3:0];
    test instan_mod(array1);
    initial
    begin
        $display("array1 = ");
        for(int i = 1; i>=0 ; i--)
            for(int j = 3; j >= 0; j = j-1)
                $display("%d", array1[i][j]);
    end     
endmodule                   

