
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses time type member in structure definition.
// It also initializes the data element declared of the structure type with
// values. It then checks the value of the time type member of the structure.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2) ;

    typedef struct packed {
        int  i ;
        time t ;
        byte b ;
    } st_time_t ;

    always@(posedge clk)
    begin
        st_time_t st1 = '{ 239, 13.5ps, 8'd4 } ;
        st_time_t st2 = '1 ;

        if (st1.t == 13.5ps)
        begin
            out1 = in2 ^ in1 ;
            out2 = in2 - in1 ;
        end
        else
        begin
            out1 = '1 ;
            out2 = '0 ;
        end

        if (st2.t == 18446744073709551615fs) /* 18446744073709551615 = 2**64-1 */
            if (st2.t == 18446744073709551615ps)
                if (st2.t == 18446744073709551615ns)
                    if (st2.t == 18446744073709551615us)
                        if (st2.t == 18446744073709551615ms)
                            $display("sec") ;
                        else
                            $display("ms") ;
                    else
                        $display("ns") ;
                else
                    $display("ns") ;
            else
                $display("ps") ;
        else
            $display("fs") ;
    end

endmodule

