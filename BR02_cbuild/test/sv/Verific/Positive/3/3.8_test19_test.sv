
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of structure literal. Use nested braces to initialize
// structure.

module test(input in, output out) ;

    typedef struct { int int1 ; int real1 ; } st1 ;

    typedef struct packed { bit [7:0] bit1 ; bit[7:0] [6:0]str ; } st2 ;

    st2 stArr[1:0] = '{'{"V", "String\n"}, '{8'b10101010, "my name"}} ;

    typedef struct { 
                       st1 s ;
                       int r ;
                   } st3 ;

    st3 s ;

    assign s = '{'{9, 4.5}, 7 } ;

endmodule
    
