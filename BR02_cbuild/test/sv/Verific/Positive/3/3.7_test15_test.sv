
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the use of replicate operators in packed array
// initialization.

module test(array1, array2);
    output bit array1 [1:0][3:0] = '{'{4{1'b1}}, '{4{1'b0}}};
    output bit array2 [3:0][1:0] = '{'{1'b1, 1'b1}, '{1'b1, 1'b1}, '{1'b0, 1'b0}, '{1'b0, 1'b0}};
endmodule

module bench;
    bit array1 [1:0][3:0], array2 [3:0][1:0];
    logic out;
    test instan_test(array1, array2);
    always @(*)
    begin
        for(int i = 0; i < 2; i++)
            for (int j = 0; j < 4; j++)
                $display("array1[%d][%d] =%b array2[%d][%d] =%b", i, j, array1[i][j], j, i, array2[j][i]) ;
    end
endmodule

