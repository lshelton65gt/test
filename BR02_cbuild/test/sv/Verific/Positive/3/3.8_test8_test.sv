
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test that structure literals can be specified in various
// ways like specifying default value

typedef struct{ int a ;
                int b ;
              } student ;

module test ;
   student s1 ;
   
   initial begin
      #1 s1 = '{default: 3} ;
      #1 $display("Struct student: %d, %d", s1.a, s1.b) ;
      #1 $display(s1) ;
   end

endmodule

