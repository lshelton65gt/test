
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests structure literals.

typedef struct{ int a ;
                int b ;
              } student ;

module test ;
   student s1 [1:0] = '{'{a:9, b:10}, '{int:45}} ;
   
   initial begin
      #1 $display(" %d %d %d %d", s1[1].a, s1[1].b,s1[0].a, s1[0].b) ;
   end

endmodule

