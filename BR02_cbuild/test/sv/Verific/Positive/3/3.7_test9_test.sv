
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the use of replicate operators in packed array 
// initialization.

module test(array1, array2);
    output reg [3:0][1:0] array1 = {{4{1'b1}},{4{1'b0}}};
    output reg [3:0][1:0] array2 = {{1'b1, 1'b1, 1'b1, 1'b1}, {1'b0, 1'b0, 1'b0, 1'b0}};
endmodule

module bench;
    bit [3:0][1:0] array1, array2;
    logic out;   
    test instan_mod(array1, array2);
    initial
     begin
    	out = array1 == array2;
    	if(out == 0)
    		$display(" Arrays did not match ");
    	else
    		$display(" Arrays matched ");
     end
endmodule

