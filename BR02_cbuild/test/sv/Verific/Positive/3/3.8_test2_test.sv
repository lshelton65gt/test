
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design assigns a structure type variable by blocking
// assignment using structure literal.

typedef struct {
                 int a ;
                 real C ;
               } check ;   

module test(input wire [31:0] p, input real q, output check ch) ;

    always@(*)
        ch = '{ a:p, C:q} ;

endmodule

module bench ;

    check ch1 ;

    test t1(12, 1.0, ch1) ;

    always @(*)
    begin
        if(ch1.a == 12 && ch1.C == 1.0)
	        $display("Testing Successful") ;
    end

endmodule

