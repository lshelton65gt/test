
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether casting is required while passing constant
// values to the structure type port.

typedef struct {
    int a ;
    logic b ;
    logic signed [31:0] c ;
} myType ;   

module test(input myType a, output myType var2) ;

    always@(*) var2 = myType'{a.a, a.b, a.c} ;

endmodule

module top ;
    myType out1, out2 ;

    myType in = '{0, 1'b1, -1} ;
    myType out ;
    test instanMod('{0, 1'b1, -1}, out1) ;
    test instanMod2(in, out2) ;

    initial
    begin
    #10 ;
        if(out1.a == 0 && out1.b == 1 && out1.c== -1)
            $display("Casting is not required to use structure literal as actual for structure type formal") ;
        else if(out2.a == 0 && out2.b == 1'b1 && out2.c== -1)
            $display("Casting is required to use structure literal as actual for structure type formal") ;

    end

    assign out = in ;

endmodule

