
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design assigns a structure type variable providing 
// context by casting.

typedef struct packed {
                 int a;
                 logic b;
                 reg c;
               } myType;   

module test(input wire [31:0] a, input wire b, input reg c, output myType var2);

    always@(*)
        var2 = myType'{a, b, c};

endmodule

module bench;
    myType out;

    test instanMod(0, 1'b1, 1'b0, out);

    initial
    begin
    #5 if(out == myType'{32'b0, 1'b1, 1'b0})
	        $display("Testing Successful");
    end
endmodule

