
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Use of multi concat operator to represent an array literal.

module test(int1, real1) ;

  output int int1 [4:5][2:0] = '{'{3{10}}, '{3{20}}} ;
  output real real1 [1:0][3:0] = '{2{'{4{5.6}}}} ;

endmodule
 
