
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing multi-concat in array literal, whether expression is
// evaluated first then replicated, or replicated first and then evaluated!

module test ;

    integer rnd_var[3:0] = '{4{$random}} ;

    initial
    begin
        if ((rnd_var[0] == rnd_var[1]) &&
            (rnd_var[1] == rnd_var[2]) &&
            (rnd_var[2] == rnd_var[3]))
            $display("First evaluated and then replicated!") ;
        else
            $display("First replicated and then evaluated!") ;
    end

endmodule

