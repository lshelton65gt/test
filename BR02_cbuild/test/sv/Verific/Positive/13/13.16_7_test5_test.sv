
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Each production has its own seperate scope. The following
// testcase defines two separate productions. Both the productions declare a variable
// with the same name. This is correct and should not result in any error.

module test (input [0:1] in, output [0:2] out) ;

    initial
    begin
        randsequence (main) 
            main   : first second ;
            first  : add | dec ;
            second : pop | push ;
            add    : gen ("add") ;
            dec    : Gen ("dec") ;
            pop    : gen ("pop") ;
            push   : gen ("push") ;
            gen (string s = "done") : { $display (s) ; } ;
            Gen (string s = "done") : { $display (s) ; } ;
        endsequence
    end
    
    assign out = in ;

endmodule
