
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The 'return' statement aborts the generation of the current
// production.  When a 'return' statement is executed from within a production
// block, the current production is aborted. Sequence generation continues
// with the next production following the aborted production. The following
// design checks this.

module test (input [0:1] in, output [0:2] out) ;

    int flag = in + 1 ;
    initial
    begin
        randsequence () 
            TOP : P1 P2 ;
            P1  : A B C ;
            P2  : A { if (flag == 1) return ;} B C ;
            A   : {$display ("A") ;} ;
            B   :  { if (flag == 2) return ; $display ("B") ;} ;
            C   :  { $display ("C") ;} ;
        endsequence
    end

    assign out = in ;

endmodule
