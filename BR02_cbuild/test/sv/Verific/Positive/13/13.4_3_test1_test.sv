
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests use of 'inside' operator in constraint
// declaration.

class RandConstraint ;

    rand int a ;
    rand int b ;
    
    constraint check { a inside { 10, 20, 30, [0:10] }; }
    constraint check1{ b inside { a }; }

endclass

RandConstraint randconst = new ;

module test ;
    initial begin
        repeat(20) begin
            if (randconst.randomize() == 1)
                $display("randconst---b: %h, a: %d", randconst.b, randconst.a) ;
            if (randconst.randomize() != 1)
                $display("Randomization failed") ;
        end
     end        
endmodule

