
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The 'constraint' provide a mechanism for ordering variables
// so that 'rand' variables can be chosen independently. This mechanism defines a 
// partial ordering on the evaluation of variables and is specified using the
// 'solve' keyword.

class B ;
    rand bit s ;
    rand bit [31:0] d ;
    constraint c { s->d == 0 ; }
    constraint order { solve s before d ; }
endclass

B randconst = new ;

module test ;

    initial begin
        repeat(10) begin
            if (randconst.randomize() == 1)
                $display("randconst---s: %h, d: %d", randconst.s, randconst.d) ;
            if (randconst.randomize() != 1)
                $display("Randomization failed") ;
        end
     end

endmodule

