
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): When an object member of a class is declared 'rand', all of
// its constraints and random variables are randomized simultenously along with
// the other class variables and constraints. Constraint expressions involving
// random variables from other objects are called global constraints.


class A ; // leaf node
    rand bit [7:0] v ;
endclass

class B extends A ; // heap node
    rand A left ;
    rand A right ;
    constraint heapcond { left.v <= v ; right.v <= v ; }

    function new ;
        left = new ;
        right = new ;
        v = 0 ;
    endfunction:new
endclass

B randconst = new ;

module test ;

    initial begin
        repeat(10) begin
            if (randconst.randomize() == 1)
                $display("randconst---left: %d, right: %d, v : %d", randconst.left.v, randconst.right.v, randconst.v) ;
            if (randconst.randomize() != 1)
                $display("Randomization failed") ;
        end
     end

endmodule

