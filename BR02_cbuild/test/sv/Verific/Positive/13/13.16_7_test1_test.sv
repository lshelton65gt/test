
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Data can be passed down to a production about to be
// generated, and generated productions can return data to non-terminals
// that triggered their generation. Passing data to a production is
// similar to a task call and folows the same sequence.

module test (input [0:1] in, output [0:2] out) ;

    initial
    begin
        randsequence (main) 
            main   : first second ;
            first  : add | dec ;
            second : pop | push ;
            add    : gen ("add") ;
            dec    : gen ("dec") ;
            pop    : gen ("pop") ;
            push   : gen ("push") ;
            gen (string s = "done") : { $display (s) ; } ;
        endsequence
    end
    
    assign out = in ;

endmodule
