
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A production can be made conditionally by means of an
// if...else statement. The following design checks this.

module test (input in, output bit [0:2] out) ;

    parameter MAX = 20 ;
    int depth = 1 ;
    int stack [MAX] ;
    initial
    begin
        randsequence () 
            PP_PO : if ( depth < 2 ) PUSH else POP ;
            PUSH  : { ++depth ; do_push(1) ;  $display ("PUSH") ; } ;
            POP   : { do_pop() ; --depth ; $display ("POP") ; } ;
        endsequence
    end

    function int do_pop() ;
        do_pop = stack[depth] ; // Pop from stack
    endfunction

    task do_push (input int x) ; // Push into stack
        stack[depth] = x ;
    endtask

    assign out = !in ;

endmodule
