
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Iterative constraints allow arrayed variables to be constrained
// in a parameterized manner using loop variables and indexing expressions.

class RandConstraint ;
    rand byte A[7] ;
    constraint C1 { foreach (A[i]) A[i] inside {2, 4, 8, 16} ; }
    constraint C2 { foreach (A[j]) A[j] > 2 * j ; }
endclass

RandConstraint randconst = new ;

module test ;

    initial
    begin
        for (int i=0; i<7; i++)
        begin
            if (randconst.randomize() == 1)
                $display("randconst---b: , a: %b , i= %d", randconst.A[i], i ) ;
            if (randconst.randomize() != 1)
                $display("Randomization failed") ;
        end
     end

endmodule

