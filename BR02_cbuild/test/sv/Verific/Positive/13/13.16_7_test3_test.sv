
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Productions that do not specify a return type will assume
// a void type. The following design has a production which does not return anything
// but has a return statement returning nothing. 


module test (input in, output [0:2] out) ;

    initial
    begin
        randsequence (bin_op) 
            void bin_op     : value operator value // void type is optional
                              {$display (" %s %b %b", operator, value[1], value[2]) ; return ; } ; // optional return type
            bit [7:0] value : { return $urandom ; } ;
            string operator : add := 5 { return "+" ;} | dec := 2 { return "-" ;} | mult := 3 { return "*" ; } ;
            add    : { $display ("add") ; } ;
            dec    : { $display ("dec") ; } ;
            mult   : { $display ("mult") ; } ;
        endsequence
    end

endmodule
