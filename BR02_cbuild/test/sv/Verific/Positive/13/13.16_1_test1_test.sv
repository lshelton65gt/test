
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The probability that a production list is generated can be
// changed by assigning weights to production lists. The following design checks this.

module test (input in, output bit [0:2] out) ;

    initial
    begin
        randsequence (main) 
            main   : first second done ;
            first  : add := 3 | dec := (1 + 1) ;
            second : pop | push ;
            done   : { $display ("done") ; } ;
            add    : { $display ("add") ; } ;
            dec    : { $display ("dec") ; } ;
            pop    : { $display ("pop") ; } ; 
            push   : { $display ("push") ; } ;
        endsequence
    end

    assign out = in ;

endmodule
