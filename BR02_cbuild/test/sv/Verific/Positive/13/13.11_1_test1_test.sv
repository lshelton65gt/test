
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : The 'std::randomize()..with' form of the scope randomize
// function allows users to specify random constraints to be applied to the local
// scope variables. When specifying constraints, the arguments to the scope randomize
// function become the random variables; all other variables are state variables.


module test (input in, output out) ;
task stimulus( int length ) ;
    int a, b, c, success ;
    success = std::randomize( a, b, c ) with { a < b; a + b < length; } ;
    $display ("success = %d", success) ;
    success = std::randomize( a, b ) with { b - a > length; } ;
    $display ("success = %d", success) ;
endtask 
    always 
        stimulus($random) ;

    initial 
        #1001 $finish ;

    assign out = !in ;
endmodule
