
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A production can be selected from a set of alternatives
// using a 'case' production statement. The following design checks this.

module test (input in, output bit [0:2] out) ;

    int device = 9 ;
    initial
    begin
        randsequence () 
            SELECT : 
            case (device & 7)
                0       : NETWORK ;
                1, 2    : DISK ;
                default : MEMORY ;
            endcase ;
            NETWORK : { $display ("Network") ;} ;
            DISK    : { $display ("Disk") ;} ;
            MEMORY  : { $display ("Memory") ;} ;
        endsequence
    end

    assign out = in ;

endmodule
