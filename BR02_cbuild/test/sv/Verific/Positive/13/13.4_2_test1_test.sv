
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests that a constraint in the derived class
// that uses the same name as a constraint in its parent class overrides the 
// parent class constraint.

class RandConstraint ;

    rand int a ;
    rand bit [15:0] b ;
    
    constraint check{ b[1:0] == 2'b0 ; a < 10 ; }

endclass


class InheritConstraint extends RandConstraint ;

    constraint check{ a > 10 ; }

endclass


RandConstraint randconst = new ;
InheritConstraint inheritconst = new ;

module test ;

    initial begin

        repeat(10) begin

            if (randconst.randomize() == 1)
                $display("randconst---b: %h, a: %d", randconst.b, randconst.a) ;
            if (inheritconst.randomize() == 1 )
                $display("noconst---b:%h, a:%d ", inheritconst.b, inheritconst.a) ;
            if (randconst.randomize() != 1) 
                $display("randconst - Randomization failed") ;
            if ( inheritconst.randomize() != 1)
                $display("inheritconstraint - Randomization failed") ;
        end

     end        

endmodule

