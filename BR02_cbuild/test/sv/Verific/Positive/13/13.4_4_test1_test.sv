
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Every item of distribution set can have a weight, which is
// specified using ':=' or ':/' operators. This weighted distribution set is used
// in constraint declaration. This design tests this.

class RandConstraint ;

    rand int a ;
    rand int b ;
    
    constraint check { a dist {[10:20] :/ 10, 30:=2, 40:=3} ; }

endclass

RandConstraint randconst = new ;

module test ;

    initial begin

        repeat(10) begin

            if (randconst.randomize() == 1)
                $display("randconst---b: %h, a: %d", randconst.b, randconst.a) ;
            if (randconst.randomize() != 1) 
                $display("Randomization failed") ;
        end

     end        

endmodule

