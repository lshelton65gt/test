
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the use of implication operator in constraints.

typedef enum {low, mid, high} AddrType;

class RandConstraint ;

     rand AddrType atype;
     rand int n [4] ;
     constraint addr_range
     {
          (atype == low )  -> n[0] inside { [00 : 15] } ; 
          (atype == low )  -> n[1] inside { [00 : 15] } ; 
          (atype == mid )  -> n[2] inside { [16 : 31] } ; 
          (atype == high ) -> n[3] inside { [16 : 31] } ; 
     }
    
endclass

RandConstraint randconst = new ;

module test ;

    initial begin
        repeat(10) begin
            if (randconst.randomize() == 1)
                $display("randconst---enum: %d, a: %d, b: %d", randconst.atype, randconst.n[0], randconst.n[1]) ;
            if (randconst.randomize() != 1)
                $display("Randomization failed") ;
        end
     end

endmodule

