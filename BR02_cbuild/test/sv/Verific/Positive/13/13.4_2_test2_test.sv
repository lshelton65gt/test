
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Using inheritance to build layered constraint systems enables the 
// development of general-purpose models that can be constrained to perform 
// application-specific functions.

class RandConstraint ;

    rand int a ;
    rand bit [15:0] b ;
    
    constraint check{ b[1:0] == 2'b0 ; a < 10 ; }

endclass

typedef enum {low, mid, high} AddrType;

class InheritConstraint extends RandConstraint ;

     rand AddrType atype;
     constraint addr_range
     {
          (atype == low ) -> a inside { [00 : 15] } ; 
          (atype == mid ) -> a inside { [16 : 31] } ;
          (atype == high) -> a inside { [32 : 63] } ;
     }
    
endclass


RandConstraint randconst = new ;
InheritConstraint inheritconst = new ;

module test ;

    initial begin

        repeat(10) begin

            if (randconst.randomize() == 1)
                $display("randconst---b: %h, a: %d", randconst.b, randconst.a) ;
            if (inheritconst.randomize() == 1 )
                $display("noconst---b:%h, a:%d, enum: %d", inheritconst.b, inheritconst.a, inheritconst.atype) ;
            if (randconst.randomize() != 1 || inheritconst.randomize() != 1)
                $display("Randomization failed") ;
        end

     end        

endmodule

