
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Class constraint provides implication operator '->' for 
// conditional relations. This design uses implication operator.

typedef enum {low, mid, high} AddrType;

class RandConstraint ;

     rand AddrType atype;
     int a ;
     constraint addr_range
     {
          (atype == low ) -> a inside { [00 : 15] } ; 
          (atype == mid ) -> a inside { [16 : 31] } ;
     }
    
endclass

RandConstraint randconst = new ;

module test ;
    initial begin
        repeat(10) begin
            if (randconst.randomize() == 1)
                $display("randconst---a: %d", randconst.a) ;
            if (randconst.randomize() != 1)
                $display("Randomization failed") ;
        end
     end
endmodule

