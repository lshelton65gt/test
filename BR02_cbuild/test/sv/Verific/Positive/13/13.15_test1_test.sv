
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The 'randcase' statement randomly selects one of its
// branches. The randcase item expressions are non-negative integral values tha
// constitute the branch weights. An item's weight divided by the sum of all
// weights gives the probability of taking that branch. The following example
// checks this.

module top ;

    bit in = 0 ;
    bit [0:1] out ;
    test T (in, out) ;

endmodule

module test (input in, output bit [0:1]out) ;
    
    initial
    begin
        randcase
            3 : out = in + 1 ;
            1 : out = in + 2 ;
            4 : out = in + 3 ;
        endcase

        if (out == (in + 1)) $display ("out = in + 1") ;
        if (out == (in + 2)) $display ("out = in + 2") ;
        if (out == (in + 3)) $display ("out = in + 3") ;
    end

endmodule

