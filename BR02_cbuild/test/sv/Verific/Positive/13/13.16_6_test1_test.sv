
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The 'break' statement terminates the sequence generation.
// When a 'break' statement is executed from within a production block, it
// forces a jump out of the 'randsequence' block. The following design checks this.

module test (input in, output bit [0:2] out) ;

    parameter max_length = 100 ;
    int fifo_length = 1 ;
    initial
    begin
        randsequence () 
            WRITE   : SETUP DATA ;
            SETUP   : { if (fifo_length >= max_length) break ;} COMMAND ;
            DATA    : {$display ("DATA") ;} ;
            COMMAND : {$display ("COMMAND") ;} ;
        endsequence
    end

    assign out = in ;

endmodule
