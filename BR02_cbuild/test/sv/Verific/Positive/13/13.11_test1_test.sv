// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : In the following design the scope randomize function
// behaves exactly the same as a class randomize method, except that it 
// operates on the variables of the current scope instead of class member variables.

module test (input in, output out) ;
    bit [15:0] addr ;
    bit [31:0] data ;
    function bit gen_stim() ;
        bit success, rd_wr ;
        success = randomize( addr, data, rd_wr ) ;    // call std::randomize 
        if (success) $display ("scope randomize successfull") ; // scope randomize returns 1 on success
        return rd_wr ;
    endfunction 
    assign out = in ;
endmodule 

