
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Random System Functions - $urandom_range(), string data
// types are passed

class RandConstraint ;
    string b ;
    rand int a ;

    function int getrandom() ;
        this.randomize(a) ;
        //b = $urandom_range("qwwew",a) ;
        b = string'($urandom_range("qwwew",a)) ;
        return 1 ;
    endfunction

endclass


module test ;

RandConstraint randconst = new ;
string c =string'($urandom_range("qweqwe","xzzxz")) ;

    initial begin

        repeat(10) begin

            if (randconst.getrandom() == 1)
                $display("randconst---b: %s, a: %d", randconst.b, randconst.a) ;
            $display(" Value of c: %s",c) ;
        end
     end
endmodule

