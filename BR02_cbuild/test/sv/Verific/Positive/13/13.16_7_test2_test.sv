
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A value can be returned by a production using a 'return'
// statement.  generated, and generated productions can return data to non-terminals
// that triggered their generation. Passing data to a production is similar to a
// task call and folows the same sequence.

module test (input in, output [0:2] out) ;

    initial
    begin
        randsequence (bin_op) 
            void bin_op     : value operator value // void type is optional
                              {$display (" %s %b %b", operator, value[1], value[2]) ;} ;
            bit [7:0] value : { return $urandom ; } ;
            string operator : add := 5 { return "+" ;} | dec := 2 { return "-" ;} | mult := 3 { return "*" ; } ;
            add    : { $display ("add") ; } ;
            dec    : { $display ("dec") ; } ;
            mult   : { $display ("mult") ; } ;
        endsequence
    end

endmodule

