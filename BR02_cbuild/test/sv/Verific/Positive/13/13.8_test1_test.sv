
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The constraint_mode() method can be used to control whether
// a constraint is active or inactive. When a constraint is inactive, it is not
// considered by 'randomize()' method.

module test ;

    class RandConstraint ;
        rand int a ;
        rand bit [15:0] b ;
        constraint check{ b[1:0] == 2'b0 ; a < 10 ; }
    endclass

    RandConstraint randconst = new ;
  
    function int  T(RandConstraint rc) ;
        int ret ;
        rc.check.constraint_mode(0) ;
        ret = rc.randomize() with { rc.a<10 && rc.a>0 ; } ;   
        return ret ;
    endfunction

    initial begin
        repeat (10) begin
            $display( "The value of the function : %d", T(randconst) ) ;
            $display("a: %d, b: %b", randconst.a, randconst.b) ;
        end
    end

endmodule

