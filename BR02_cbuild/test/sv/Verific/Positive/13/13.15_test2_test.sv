
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The 'randcase' weights can be arbitrary expressions,
// not just constants. The following example demonstrates this.

module top ;
    bit in = 0 ;
    bit [0:2] out ;
    test T(in, out) ;
endmodule

module test (input in, output bit [0:2] out) ;
    byte a, b ;
    initial
    begin
        randcase
            a + b   : out = in + 1 ;
            a - b   : out = in + 2 ;
            a ^ ~b  : out = in + 3 ;
            12'h800 : out = in + 4 ;
        endcase
    if (out == (in + 1)) $display ("out = in + 1") ;
    if (out == (in + 2)) $display ("out = in + 2") ;
    if (out == (in + 3)) $display ("out = in + 3") ;
    if (out == (in + 4)) $display ("out = in + 4") ;
    end
endmodule

