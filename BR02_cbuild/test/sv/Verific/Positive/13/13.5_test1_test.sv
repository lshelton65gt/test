
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog uses an object-oriented method for assigning 
// random values to the member variables of an object, subject to user-defined 
// constraints.

class RandConstraint ;
    rand int a ;
    rand bit [15:0] b ;
    constraint check{ b[1:0] == 2'b0 ; a < 10 ; }
endclass

class NoConstraint ;
    rand int a ;
    rand bit [15:0] b ;
endclass

RandConstraint randconst = new ;
NoConstraint noconst = new ;

module test ;

    initial
    begin
        repeat(10)
        begin
            if (randconst.randomize() == 1)
                $display("randconst---b: %h, a: %d", randconst.b, randconst.a) ;
            if (noconst.randomize() == 1 )
                $display("noconst---b:%h, a:%d", noconst.b, noconst.a) ;
            if (randconst.randomize() != 1 || noconst.randomize() != 1)
                $display("Randomization failed") ;
        end
     end        

endmodule

