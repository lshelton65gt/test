
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The 'rand join' production control is used to randomly
// interleave two or more production sequences while maintaining the relative
// order of each sequence.

module test (input in, output out) ;

    initial
    begin
        randsequence (TOP) 
            TOP : rand join S1 S2 ;
            S1  : A B ;
            S2  : C D ;
            A   : { $display ("A") ; } ; 
            B   : { $display ("B") ; } ; 
            C   : { $display ("C") ; } ; 
            D   : { $display ("D") ; } ; 
        endsequence
    end

    assign out = !in ;

endmodule
