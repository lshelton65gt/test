
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests 'randomize'...'with' construct which can be
// used to declare in-line constraint at the point where randomize() method is
// called.

class RandWith;
    rand integer x;
endclass 

class Check;
    integer x;
    integer y;

    function integer ch ( RandWith f, integer y, integer z) ;
        this.x = z ;
        this.y = y ;
        ch = f.randomize() with {f.x < (this.x + this.y);} ;
    endfunction 
endclass 

module test ;

    RandWith rw = new ;
    Check check = new ;

    initial begin
        repeat(10) begin
            if (check.randomize() == 1)
                $display("check x: %d, y: %d", check.x, check.y);
            $display("call of func ch():  %d", check.ch(rw,23,12) ) ;
        end
     end        

endmodule

