
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing random system functions : $urandom_range()

class RandConstraint ;
    int b ;
    rand int a ;

    function int getrandom() ;
        return 1 ;
    endfunction

endclass

RandConstraint randconst = new ;

module test ;
int c = $urandom_range(20) ;

    initial begin

        repeat(10) begin

            if (randconst.getrandom() == 1)
                $display("randconst---b: %d, a: %d", randconst.b, randconst.a) ;
            $display(" Value of c: %d",c) ;
        end
     end
endmodule

