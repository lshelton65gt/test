
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of the implication operator ie. '->' within 'inside' construct.


typedef enum {low, mid, high} AddrType;

class RandConstraint ;

     rand AddrType atype;
     rand int a ;
     int b ;
     constraint addr_range
     {
          (atype == low ) -> (a+2) inside { [1 : 10] } ;
          (atype == mid ) -> (a+1) inside { [16 : 20] } ;
          (atype == high ) ->(a+b) inside { [20 : 31] } ;
     }
    
endclass

RandConstraint randconst = new ;

module test ;
    initial begin
        repeat(10) begin
            if (randconst.randomize() == 1)
                $display("randconst---b: %h, a: %d, atype: %d", randconst.b, randconst.a, randconst.atype) ;
            if (randconst.randomize() != 1)
                $display("Randomization failed") ;
        end
     end
endmodule

