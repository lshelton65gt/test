
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Using randomize ...with construct, users can declare in-line
// constraints at the point where the randomize method is called. This design tests
// this.

class RandConstraint ;
    rand int a ;
    rand bit [15:0] b ;
    constraint check{ b[1:0] == 2'b0 ; a > 10 ; }
endclass

typedef enum {low, mid, high} AddrType;

class InheritConstraint extends RandConstraint ;
     rand AddrType atype;
     constraint addr_range
     {
          (atype == low ) -> a inside { [00 : 15] } ; 
          (atype == mid ) -> a inside { [16 : 31] } ;
          (atype == high) -> a inside { [32 : 63] } ;
     }
endclass

RandConstraint randconst = new ;
InheritConstraint inheritconst = new ;

module test ;
  
    function int  T(InheritConstraint ic) ;
        int ret ;
        ret = ic.randomize() with { a<32 && a>15 ; atype == mid ; } ;   
        return ret ;
    endfunction

    initial
    begin
        repeat (10)
        begin
            $display( "The value of the function : %d", T(inheritconst) ) ;
            $display("a: %d, b:%b, enum: %d", inheritconst.a, inheritconst.b, inheritconst.atype) ;
        end
    end

endmodule

