
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Every class contains pre_randomize() and post_randomize()
// methods, which are automatically called by 'randomize()' method before and 
// after computing new random values. This design overwrites those methods to
// verify their calling sequences.

class RandConstraint ;
    rand int a ;
    rand bit [15:0] b ;
    
    constraint check{ b[1:0] == 2'b0 ; a < 10 ; }
   
    function void pre_randomize() ;
        $display("call of pre_randomize, a: %d", a) ;
    endfunction

    function void post_randomize() ;
        $display("call of post_randomize, a: %d", a) ;
    endfunction
endclass

RandConstraint randconst = new ;

module test ;
  
    function int  T(RandConstraint rc) ;
        int ret ;
        rc.check.constraint_mode(0) ;
        ret = rc.randomize() with { rc.a<10 && rc.a>0 ; } ;   
        return ret ;
    endfunction

    initial begin
        repeat (10) begin
            $display( "The value of the function : %d", T(randconst) ) ;
            $display("a: %d, b: %b", randconst.a, randconst.b) ;
        end
    end

endmodule

