// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : Testing the built-in class randomize function.

class stimc;
    static rand bit [15:0] addr;
    static rand bit [31:0] data;
    static rand bit rd_wr;
    static function bit gen_stim( stimc p );
        bit success;
        success = p.randomize();
        if (success) $display ("Randomization successfull") ;
        addr = p.addr;
        data = p.data;
        return p.rd_wr;
    endfunction 
endclass 

module test (input in, output out) ;
    stimc xxx = new ;
    assign out = stimc::gen_stim ( xxx ) ;
endmodule
