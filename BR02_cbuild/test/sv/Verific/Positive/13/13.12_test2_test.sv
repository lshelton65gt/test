
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests random System Functions $urandom.

class RandConstraint ;

    int a ;
    rand int b ;

    function int getrandom() ;
        b = this.randomize() ;
        a = $urandom(b) ;
        return 1 ;
    endfunction

endclass

RandConstraint randconst = new ;

module test ;

    initial begin

        repeat(10) begin

            if (randconst.getrandom())
                $display("randconst---b: %s, a: %d", randconst.b, randconst.a) ;
        end
     end
endmodule

