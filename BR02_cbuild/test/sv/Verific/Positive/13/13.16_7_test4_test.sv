
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design uses a 'randsequence' block to produce
// random traffic for a DSL packet network.

class DSL ;
    int crc;
endclass

module test (input in, output out) ;

    initial
    begin
        randsequence (STREAM) 
            STREAM : GAP DATA := 80 | DATA := 20 ;

            DSL PACKET (bit bad) :
                {       
                    DSL d = new ;
                    if (bad) d.crc ^= 23 ; // mangle crc
                    return d ;
                } ;

            GAP : {  $urandom_range (1, 20) ; } ;

            DATA : PACKET (0) := 94 
                { $display ("Transmit good packet") ; }
                | PACKET (1) := 6 { $display ("Transmit bad packet") ; }
                ;
        endsequence
    end

endmodule

