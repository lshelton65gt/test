
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing weighted distribution using  ':=', ':/' and 'dist' keyword. 

class RandConstraint ;

    rand int a ;
    rand int b ;
    
    constraint check { (a+2) dist {[10:20] :/ b, 30:=2, 40:=(b/10)} ; } 

endclass

RandConstraint randconst = new ;

module test ;

    initial begin

        repeat(10) begin

            if (randconst.randomize() == 1)
                $display("randconst---b: %h, a: %d", randconst.b, randconst.a) ;
            if (randconst.randomize() != 1) 
                $display("Randomization failed") ;
        end

     end        

endmodule

