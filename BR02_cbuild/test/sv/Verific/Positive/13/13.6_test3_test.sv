
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : By using the 'randomize()...with' construct, users can
// declare in-line constraints at the point where the randomize() method is called.

class SimpleSum ;
    rand bit [7:0] x, y, z ;
    constraint c {z == x + y ;}
endclass 

task InlineConstraintDemo(SimpleSum p) ;
    int success ;
    success = p.randomize() with {x < y ;} ;
    $display ("success = %d", success) ;
endtask 

module test (input in, output out) ;

    SimpleSum ss = new ;

    initial begin
        repeat(10) begin
            InlineConstraintDemo(ss) ;
            $display("ss x: %d, y: %d", ss.x, ss.y);
        end
     end
     assign out = in ;

endmodule

