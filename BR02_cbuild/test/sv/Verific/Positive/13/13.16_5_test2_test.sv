
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The optional expression following the 'rand join' 
// keywords must be a real number in the range of 0.0 to 1.0. The value of
// this expression represents the degree to which the length of the sequences
// to be interleaved affects the probability of selecting the sequence.

module test (input in, output out) ;

    initial
    begin
        randsequence (TOP) 
            TOP : rand join (0.0) S1 S2 ;
            S1  : A B ;
            S2  : C D ;
            A   : { $display ("A") ; } ; 
            B   : { $display ("B") ; } ; 
            C   : { $display ("C") ; } ; 
            D   : { $display ("D") ; } ; 
        endsequence
    end

    assign out = in ;

endmodule
