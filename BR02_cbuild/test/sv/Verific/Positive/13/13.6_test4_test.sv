
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : The 'randomize-with' constraint can also reference local
// variables, task and function arguments. The 'randomize()...with' is brought
// into scope at the innermost nesting level. 
// In the following example, in the 'f.randomize()...with' constraint block, x
// is a member of class Foo and hides x argument in 'doit()' task.

class Foo ;
    rand int x ;
endclass 

class Bar ;
    int x ;
    int y ;
    task doit(Foo f, int z) ;
        int result ;
        x = y + z ;
        y = x + z ;
        result = f.randomize() with {f.x < y + z + x ;};
    endtask 
endclass 

module test (input in, output out) ;

    Foo foo = new ;
    Bar bar = new ;

    initial begin
        repeat(10)
        begin
            if (foo.randomize==1) bar.doit(foo, foo.x) ;
            $display("bar x: %d, y: %d", bar.x, bar.y) ;
        end
    end
    assign out = in ;

endmodule

