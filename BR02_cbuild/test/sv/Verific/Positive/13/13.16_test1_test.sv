
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The 'randsequence' keyword can be followed by an optional
// production name (inside the parentheses) that designates the name of the top-level
// production. This design defines a 'randsequence' with a production name to 
// designate the top-level production.

module test (input in, output out) ;

    initial
    begin
        randsequence (main) 
            main   : first second done ;
            first  : add | dec ;
            second : pop | push ;
            done   : { $display ("done") ; } ;
            add    : { $display ("add") ; } ;
            dec    : { $display ("dec") ; } ;
            pop    : { $display ("pop") ; } ; 
            push   : { $display ("push") ; } ;
        endsequence
    end

endmodule
