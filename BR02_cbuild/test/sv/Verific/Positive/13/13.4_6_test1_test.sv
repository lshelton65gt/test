
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests 'if-else' style constraint which is used
// for declaring conditional relations .

class RandConstraint ;

    rand int a ;
    rand int b ;

    constraint ifelse
    {
        if (b < 10)
           if (a < 10)
               b > 10 ;
    
        else
            (a == (b == 10)) ;
    }

endclass

RandConstraint randconst = new ;

module test ;

    initial begin
        repeat(10) begin
            if (randconst.randomize() == 1)
                $display("randconst---b: %h, a: %d", randconst.b, randconst.a) ;
            if (randconst.randomize() != 1)
                $display("Randomization failed") ;
        end
     end

endmodule

