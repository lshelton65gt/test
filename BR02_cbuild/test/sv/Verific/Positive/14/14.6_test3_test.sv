
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): 'wait_order' requires the events specified to be called
// in order. The following design has the events called in a different order.
// This design checks this.

module test ;

    event a, b, c ;

    bit clk ;

    initial clk = 0 ;

    always
        #5 clk = ~clk ;

    always @(negedge clk)
    begin
        task1() ;
        wait_order (a, b, c) else $display ("Error: events out of order") ;
    end

    task task1 () ;
        ->a ;
        ->c ;
        ->b ;
    endtask

endmodule
