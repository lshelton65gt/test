
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing implementation of semaphore.

module test ;
    semaphore sm ; 
    initial begin 
        sm = new(10) ;
        if (sm == null)
            $display(" Semaphore not created ") ;
        else
            $display(" Semaphore created" ) ;
    end    
endmodule

