
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a module that sorts its input bits
// and assigns the sorted input pattern to the output port. The sorting is done
// by calling a user-defined function seqSort. This function in turn calls
// another function swap to swap two elements.

function void swap(inout int a, b);
    int temp;
    temp = a;
    a = b;
    b = temp;
    return;
endfunction

function void seqSort;
    parameter p = 10;
    inout int in[0:p];

    for(int i = 0; i <= p-1; i++)
    begin
        for(int j = i+1; j <= p; j++)
        begin
            if(in[i] > in[j])
                swap(in[i], in[j]);
        end
    end
    return;
endfunction

module test(in, out);
    input int in[0:10];
    output int out[0:10];

    int io [0:10];

    initial
    begin
        #10 io = in;
        seqSort(io);
        out = io;
    end
    
endmodule

