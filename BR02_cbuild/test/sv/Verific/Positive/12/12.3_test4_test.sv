
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing a function which returns an enum type.

function enum int {mem0, mem1, mem2, mem3, mem4, mem5, mem6} func;
    input int in;
	
	case(in)
	    1: return mem1;
		2: return mem2;
		3: return mem3;
		4: return mem4;
		5: return mem5;
		6: return mem6;
		default: return mem0;
	endcase
	
endfunction

module test;
    int a;

	initial
	begin
	    a = 0;
		#5 a = 1;
		#5 a = 2;
		#5 a = 3;
		#5 a = 4;
		#5 a = 5;
		#5 a = 6;
		#5 a = 7;
	end	

	always @(a)
	    $monitor("input = %d, output = %d", a, func(a));

endmodule


