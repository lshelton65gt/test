
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing function with return type as an enum.

typedef enum {mem0 ,mem1, mem2, mem3, mem4} enumType;

function enumType func(input bit [3:0]funcBit);
    enumType var1;
    case(funcBit)
        4'b0001 : var1 = mem1;
        4'b0010 : var1 = mem2;
        4'b0100 : var1 = mem3;
        4'b1000 : var1 = mem4;
        default : var1 = mem0;
    endcase
    return var1;
endfunction

module test(input bit [3:0]modBit, output int out);

initial
    out = func(modBit);

endmodule

