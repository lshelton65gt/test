
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing functions with return type as union.

typedef union packed {
    bit [3:0]a;
    logic [3:0]b;
} myUn;

function myUn func(funcLog);
    myUn var1;
    var1 = funcLog;
    return var1;
endfunction

module test(input [3:0] inLogic, output reg [3:0] outBit, outLogic);
myUn var2;
initial
begin
    var2 = func(inLogic);
    outBit = var2.a;
    outLogic = var2.b;
end

endmodule

