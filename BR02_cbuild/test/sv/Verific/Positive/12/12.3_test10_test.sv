
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing for the warning if non-void function is used in a statement.

enum {false, true}var1;

function int func(input int a, output int out);
    out  = a;
    var1 = true;
    return var1;
endfunction

module test(input int in, output int out);

initial
    func(in, out);

endmodule

