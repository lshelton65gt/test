
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Only equivalent types can be passed by reference to subroutine.
// Unpacked array types are equivalent to elements having identical types and shapes.
// Shape is defined as the number of dimensions and the number of elements in each 
// dimension and not the actual range of the dimension.
//             bit [9:0]  A [0:5] ;
//             bit [1:10] B [6] ; 
//             typedef bit [10:1] uint10 ;
//             uint10 C [6:1] ;     // A, B and C have equivalent types
//             typedef int anint [0:0] ;     // 'anint' is not type equivalent to int

typedef bit [10:1] uint10 ;

function automatic bit donothing (ref uint10 C [6:1], ref uint10 D [6:1]) ;
    if (C == D) return 1'b1 ;
    else return 1'b0 ;
endfunction

module test(input  bit [9:0] A [0:5], bit [1:10] B [6], bit clk,
           output bit out) ;

    bit [9:0] A1[0:5] ;
    bit [9:0] B1[0:5] ;

    initial begin 
        A1 = A ;
        B1 = B ;
    end

    always @ (posedge clk)  out = donothing(A1, B1) ;

endmodule

