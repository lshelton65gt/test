
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Return value from a function can be a union. This design
// defines a non-void function and returns a union type variable from the
// function. 

typedef union { int i; real r; } val_t;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    bit t;
    val_t v;

    v = func1(in1, in2, t);

    if(t)
        out1 = int'(2*v.r);
    else
        out1 = v.i;

    out2 = ((t)?(int'(v.r)):(v.i));
end

function val_t func1(int a, b, output bit what);
    val_t v1;
    int t2;
    
    t2 = a + b;

    what = 0;
    if (t2[0])
    begin
        v1.r = t2<<1;
        v1.r += 0.5;
        what = 1;
    end
    else
        v1.i = t2<<1;

    return v1;
endfunction

endmodule

