
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function with one default argument 
// and it is called without any argument.

module test (i, o) ;

    parameter p = 8 ;

    function integer fn1(input integer i = 8) ;
        return i ;
    endfunction

    input [p-1:0] i ;
    output [0:p-1] o ;

    assign o = ~i ;
    integer ret_val ;
    initial
    begin
        ret_val = fn1() ;
        $display("%d", ret_val) ;
    end


endmodule

