
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function without any ports even without
// the parenthesis and it is called.

module test (i, o) ;

    parameter p = 8 ;

    input [p-1:0] i ;
    output [0:p-1] o ;

    assign o = ~i ;

    integer ret_val ;
    initial
    begin
        ret_val = fn1() ;
        $display("%d", ret_val) ;
    end

    function integer fn1 ;
        return 8 ;
    endfunction

endmodule

