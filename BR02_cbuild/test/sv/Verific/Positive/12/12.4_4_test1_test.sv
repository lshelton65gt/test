
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function with one default argument
// and it is called with named argument connection.

module test (i, o) ;

    parameter p = 8 ;

    input [p-1:0] i ;
    output [0:p-1] o ;

    assign o = ~i ;

    initial
    begin
        $display("%d", fn1(.i(7))) ;
    end

    function integer fn1(input integer i = 8) ;
        return i ;
    endfunction

endmodule

