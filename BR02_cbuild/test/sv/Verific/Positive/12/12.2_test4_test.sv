
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of automatic and static variable in static task.

module test;

task T;
integer i = 0;
automatic integer j = 0;
begin
    $display("Static task called");
    $display ("automatic value = %d", j);
    $display ("static value = %d", i);
    i = 10;
    j = 10;
    #10;
end
endtask


initial
fork
  T;
  T;
join

endmodule
