
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): In System Verilog function declarations default to formal
// direction input if no direction has been specified. This design tests this.

function bit add_func( int no1, int no2, output bit out1, out2 ) ;
    int sum ;
    sum = no1 + no2 ;

    $display("The input values no1 =%d, no2 =%d", no1, no2) ;

    if (sum == (no1 + no2))
    begin
        out1 = 1'b1 ;
        out2 = 1'b1 ;
    end
    else
    begin
        out1 = 1'b0 ;
        out2 = 1'b0 ;
    end
    return 1'b1 ;
endfunction

module test(input bit clk, int no1, int no2, output bit mod_out ) ;

    bit out1, out2 ;

    always @ (posedge clk)
        mod_out = add_func(no1, no2, out1, out2) ;

endmodule

