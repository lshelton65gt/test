
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests recursive function call.

module bench;

wire  [31:0] out1;
reg  [7:0] in1;
reg clk;

initial clk = 0;

always #5 clk = ~clk;

test I (out1, in1, clk);

always @ (negedge clk)
    in1 = $random;

initial
  $monitor ($time,, "out1=%d in1=%d", out1,in1);

initial #100 $finish;
endmodule

function automatic int fact(input int in1);
    if(in1 < 2)
        return 1;
    else
        return in1*fact(in1 - 1);
endfunction

module test(out1, in1, clk);
output reg [31:0] out1;
input  [31:0] in1;
input clk;


    always @ (posedge clk)
        out1 = fact(in1);

endmodule

