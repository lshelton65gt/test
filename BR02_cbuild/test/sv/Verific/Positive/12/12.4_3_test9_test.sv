
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Default argument in function.

class Batch ;
    int yr ;
    int total ;
 
    function new(int y, int t) ;
        yr = y ;
        total = t ;
    endfunction

    function void show() ;
        $display("year = %d, total = %d", yr, total) ;
    endfunction

endclass

module test ;

   int a=10, b=20 ;

   function void swap(int a = 1, int b, Batch bat = null) ; 
       int tmp ;
       tmp = a ;
       a = b ;
       b = tmp ;
       if (bat == null) 
           bat = new(2004, 100) ;
       bat.show() ;
   endfunction
   
   initial begin:bbb
       $display("The numbers a---%d, b---%d",a,b) ;
       swap(,b) ;
       $display("The swapped numbers a---%d, b---%d",a,b) ;
   end
   
endmodule

