
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the combinations of automatic and static variables.

task mytask(output int o1, o2, o3);
    automatic int a = 0;
    int b = 0;
    static int c = 0;
    o1 = ++a ;
    o2 = ++b ;
    o3 = ++c ;
endtask

module test;

int a, b, c ;
initial
begin
    mytask(a, b, c) ;
    mytask(a, b, c) ;
    if((a == 1) && (b == 2) && (c == 2))
        $display("Testing success \n");
    else
        $display("Testing failed \n");
end

endmodule
