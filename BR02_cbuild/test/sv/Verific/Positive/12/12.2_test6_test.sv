
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of default direction of a task.

module test;

task mytask(int a, output int b);
begin
  b = a;
  a = 6;
end
endtask:mytask

int x, y;

assign x = 10;
initial
begin
    $display("Testing default direction of task, which is input, as the testcase tries to write to an input port, tool should produce an warning\n");
    mytask(x, y);
    if(y == 10)
        $display("Testing of default direction of task is a success\n");
    else
        $display("Testing FAILED");
end
endmodule
