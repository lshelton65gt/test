
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of multiple statements without begin-end.

module test(input logic clk, data, output int a[3:0], out);

function myfunction;
    output int a[3:0], out;
    input logic clk, data;
    for(int i = 3; i >= 0; i--)
        a[i] = i;
    
    if (clk)
        out = data; 
endfunction

initial
    myfunction(a[3:0], out, clk, data);

endmodule


