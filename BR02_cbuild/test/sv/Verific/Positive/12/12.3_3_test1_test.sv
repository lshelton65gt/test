
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing constant function call.

module test(input in, output out) ;

    parameter p = fn(0) ;

    function integer fn(input integer value) ;
        logic b1[4] ;
        b1[3] = 1'b1 ;
        b1[2] = 1'b0 ;
        b1[1] = 1'b1 ;
        b1[0] = 1'b1 ;
        fn = integer'(b1[3]) + integer'(b1[2]) + integer'(b1[1]) + integer'(b1[0] + value) ;
    endfunction

    assign out = in + 1 ;

    initial 
        $display("The value returned by the constant function is %d", p) ;

endmodule

