
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the default direction of a function.

module test(input int x, output int y);

function void myfunction(int a, output int b);
begin
  b = a;
  a = 6;
end
endfunction:myfunction

initial
    myfunction(x, y);

endmodule

