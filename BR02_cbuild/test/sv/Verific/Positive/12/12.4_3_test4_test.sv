
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function with one default argument and
// it is called without any argument and parentheses as condition of for-generate.

module test(input [1:0]in1, in2, output [1:0]o) ;

    generate
        genvar i ;
        for (i=0; i<fact(); i++)
        begin:b
            mod t1(in1[i], in2[i], o[i]) ;
        end
    endgenerate

    function integer fact(input int op = create_input());
        if (op >= 2)
            fact = (op - 1) * op ;
        else
            fact = 1;
    endfunction

    function int create_input(int i = 2) ;
        create_input = (((i+2)*5)/10) ;
    endfunction

endmodule

module mod (input in1, in2, output o) ;
    assign o = (in1 ^ in2) >> fact(1) ;
    initial
        $display("Instantiating module mod") ;
endmodule

