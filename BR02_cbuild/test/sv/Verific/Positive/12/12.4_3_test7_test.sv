
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog allows a subroutine declaration to specify
// a default value for each singular argument. This design tests this.

module test ;

   int a=10, b=20 ;

   function void swap(int a = 1, int b) ; 
       int tmp ;
       tmp = a ;
       a = b ;
       b = tmp ;
   endfunction
   
   initial begin
       $display("The numbers a---%d, b---%d",a,b) ;
       swap(,b) ;
       $display("The swapped numbers a---%d, b---%d",a,b) ;
   end
   
endmodule

