
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether port direction and data types are inherited.

function void my_function(inout [3:0] a, b);
    b = a  ;
endfunction

module test(x1, y);
    inout [3:0] x1 ;
    reg [3:0] x ;
    output reg [3:0] y;

    initial begin
        x = x1 ;
	    my_function(x, y);
    end

endmodule

