
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): In SystemVerilog, once a direction has been specified for a formal,
// the subsequent formals without a direction are treated to be of same direction as the
// one previously declared. This design defines a function with a formal argument declared
// as input and checks whether the next formal (unspecified direction) is also treated 
// as an input or not by assigning to the input variable. This should generate an error/warning.

module test #(parameter width=8) 
             (input clk,
              input [width-1:0] in1, in2,
              output reg[width-1:0] out1, out2);

reg [31:0] r1, r2, r3, r4;

always@(posedge clk)
begin
    r1 = in1 * in2;
    r2 = in2 - in1;

    out1 = func1(r1, r2, r3);
    out2 = func1(r1|r3, r2^r3, r4);
end

function automatic int func1(input int a, b /* treating b as input */, output int c);
    int i;

    for(i=0; i<31; i++)
    begin
        c[i] = a[i] | b[31-i] ^ &a[i+1] | ^b[i+1];
    end

    b = c;

    return b;
endfunction

endmodule

