
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of array declaration in formal argument of a task.

module test(input logic [31:0]m, n[3:0], output int size);

task mytask(input [3:0][7:0] a, b[3:0], output int size);
    size = $bits(b);
endtask

initial
    mytask(m, n, size);

endmodule

