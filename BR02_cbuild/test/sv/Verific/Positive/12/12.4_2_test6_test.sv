
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Arguments passed by reference are not copied into the
// subroutine area, rather, a reference to the original argument is passed 
// to the subroutine. The subroutine can then access the argument data via 
// the reference. To indicate argument passing by reference, the argument
// declaration is preceded by the ref keyword. 

function automatic void swap(ref int a, ref int b) ; 
    int tmp ;
    tmp = a ;
    a = b ;
    b = tmp ;
endfunction

module test ;

    reg clk ;
    int a, b ;

    initial
        clk = 1 ;

    always
        #5 clk = ~clk ;

    always @(posedge clk) begin
        a = $random ;
        b = $random ;
    end

    initial
        #20 $finish ;

    initial
       $monitor("clk = %b, a = %d, b = %d", clk, a, b) ;

    always @(*)
       swap(a,b) ;

endmodule

