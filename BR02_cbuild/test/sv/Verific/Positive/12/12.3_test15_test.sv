
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing function with return type as reg (not allowed in Verilog 2001).

function reg[31:0] func;
    input int in;
	reg[31:0] out;
	out = in;
        return out;
endfunction

module test;
integer in = 5;
reg [31:0]funcOut;

initial
    $display("Function Output  = %b", func(in));
	
endmodule

