
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function with one default argument 
// and it is called without any argument and without parenthesis.

module test(input int in, output out) ;

    parameter p = 2 ;
    generate
        genvar i ;
        for (i = 0; i<fact(); i++)
        begin:b
            mod #(i+p) t1() ;
        end
    endgenerate

    function integer fact(input int op = 3);
        if (op >= 2)
            fact = (op - 1) * op ;
        else
            fact = 1;
    endfunction

    assign out = |in ;

endmodule

module mod ;

    parameter p1 = 10 ;

    initial
        $display("Instantiating module 'mod' with parameter p1 = %1d", p1) ;

endmodule

