
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function with one default argument 
// and it is called twice, once without any argument and once with
// argument.

module test (input in1, in2, output o);

   generate
       if ( fact() > 2)
           mod t1(in1, in2, o) ;
       if (fact(4) == 12)
           mod t1(in1, ~in2, o) ;
   endgenerate

   function integer fact(input int op = create_input());
      if (op >= 2)
          fact = (op - 1) * op ;
      else
          fact = 1;
   endfunction

   function int create_input(int i = 2) ;
      if (i == 2) $display("Function called without arguments") ;
      else $display("Function called with arguments") ;
      create_input = (((i + 2) * 5) / 100) ;
   endfunction

endmodule

module mod (input in1, in2, output o);

   assign o = in1 ^ in2 ;

endmodule

