
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the combinations of automatic and static variables.

task automatic mytask;
    int cntr1 = 0;
    static int cntr2  = 0;
    automatic int cntr3 = 0;
    // value of cntr2 increments with every call to mytask whereas that of the other variables do not
    $display("count = %d", ++cntr1) ;
    $display("static count = %d", ++cntr2) ; 
    $display("automatic count = %d", ++cntr3) ;
endtask

module test;

initial
begin
    mytask();
    mytask();
    mytask();
end

endmodule

