
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing function with structure return type.

function struct packed {
                         int a;
                         bit[3:0] b;
                       } func;
    input int x;
    input bit[3:0]y;

    return {x, y};
endfunction

module test;
    int m;
    bit [3:0]n;

    assign {m, n} = func(5, 4'b1010);

    initial
        $display(" %d = 5, %b = 1010", m, n);
endmodule

