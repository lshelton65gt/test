
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of non-void function getting used in expression.

function [15:0] func;
    return 32'b 10;
endfunction

module test(input int in, output int out);

initial
begin
    out = in + func();
end

endmodule

