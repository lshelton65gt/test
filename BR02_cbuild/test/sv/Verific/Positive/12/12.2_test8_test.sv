
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether port direction and data type have been inherited.

task my_task(input logic[3:0] a, b, output bit [3:0] out);
    b = a;
    out = b;
endtask

module test(x, y, z);
    input logic [3:0] x, y;
    output bit [3:0] z;

    initial
	    my_task(x, y, z);

endmodule
