
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): According to System Verilog arguments passed by reference 
// are not copied to the subroutine area. To indicate argument passing by reference,
// the argument declaration is preceded by 'ref' (reference) keyword. This design 
// tests this.

function automatic bit crc(ref reg [7:0] packet [1000:0] ) ;
    byte ret_value = 8'h00 ;
    for( int j = 1; j <= 1000; j++ )
    begin
        ret_value ^= packet[j] ;
    end
    if (ret_value > 8'h0f) return 1'b0 ;
    else return 1'b0 ;
endfunction

module test ( input  bit enable, bit clk,
                      input  reg [7:0]packet [1000:0],
                      output bit valid ) ;

    always @ (posedge clk) 
    begin
        valid = 1'b0 ;
        if (enable)
        begin
            if (crc( packet ) == 1'b1) valid = 1'b1 ;
            else valid = 1'b0 ;
        end
        else valid = 1'b0 ;
    end
endmodule

