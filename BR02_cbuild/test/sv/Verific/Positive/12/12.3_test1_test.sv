
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of an array as a formal argument to a function.

function void myfunc(input [31:0] in[7:0], output [31:0] out[7:0]);
    for(int i = 7; i >= 0; i--)
        out[i] = in[i];
endfunction

module test(input [31:0] x[7:0], output reg [31:0] y[7:0]);
    initial
        myfunc(x[7:0], y[7:0]);

endmodule
