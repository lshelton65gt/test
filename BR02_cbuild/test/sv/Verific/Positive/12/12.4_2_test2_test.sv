
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Arguments passed by reference must be matched with equivalent
// data types. This design checks this.

function automatic bit crc(ref bit [0:7] packet [1000:0] ) ;
    byte ret_value = 8'h00 ;
    for( int j = 1; j <= 1000; j++ )
    begin
        ret_value ^= packet[j] ;
    end
    if (ret_value > 8'h0f) return 1'b0 ;
    else return 1'b0 ;
endfunction

typedef bit[7:0] bit_8 ;  // Creating an equivalent data type

module test ( input  bit enable, bit clk,
                      input bit [7:0] packet [1000:0],
                      output bit valid ) ;

    bit [0:7]  pkt[1000:0] ;
    always @ (posedge clk)
    begin
        pkt = packet ;
        valid = 1'b0 ;
        if (enable)
        begin
            if (crc( pkt ))
                valid = 1'b1 ;
              
             else valid = 1'b0 ;
        end
        else valid = 1'b0 ;
    end
endmodule

