
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of default data type in a function.

module test(input logic [31:0]modLog, output reg [31:0]modBit);

function void myfunction(a, output bit x);
    x = a;
endfunction:myfunction


initial
begin
    myfunction(modLog, modBit);
end

endmodule
