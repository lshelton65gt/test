
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of user defined type in formals of a task.

typedef bit [3:0] bsix ;
typedef bsix mem_type [3:0] ;

task myTask(mem_type bar, bit [3:0] taskBit[3:0]) ;
begin
    bit error = 1 ;
    for(int i = 3; i >= 0; i--)
    begin
        if(bar[i] == taskBit[i])
            continue ;
        $display("Testing Failed\n") ;
        error = 0 ;
    end
    if(error == 1)
        $display("Testing Successfull\n") ;
end
endtask 

module test(in, out) ;

    bit [3:0] modBit[3:0] ;
    input mem_type in ;
    output mem_type out ;

    initial
    begin
        $display("Testing of user defined type as an argument to the formals of a task ...\n") ;
        for(int j = 3; j >= 0; j--)
            modBit[j] = (in[j] == 4'b1111) ;

        myTask(in, modBit) ;
    end

    assign out = in ;

endmodule

