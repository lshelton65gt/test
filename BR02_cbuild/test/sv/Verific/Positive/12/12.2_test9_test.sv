
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of multiple statements in a task without begin-end.

module test;

task mytask;
    output int a[3:0], out;
    input logic clk, data;
    for(int i = 3; i >= 0; i--)
        a[i] = i;
    
    if (clk)
        out = data; 
endtask

logic clk, data;
int a[3:0], out;
initial
begin
    clk = 0;
    repeat(10) clk = #5 ~clk;
    data = 1;
    repeat (5) data = #10 ~data;
    mytask(a[3:0], out, clk, data);
    $display("Testing multiple statements in a task without begin-end, so the output shouldn't show any error messege(s) \n");
end

endmodule
