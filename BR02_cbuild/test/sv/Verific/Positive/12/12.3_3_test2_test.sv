
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a function and use that function as constant
// function by calling it from parameter value overwriting list of module instantiation.

module test ;
    const int a = 5 ;
    parameter p = 5 ;
    bot #(fact(12)) f1() ;

function int fact(int p) ;
    if (p == 0 || p == 1)
        return 1 ;
    return (p * fact(p-1) );
endfunction
endmodule

module bot #(parameter p = 10) ;    
    int res = fact(p) ;
    initial
       $display("%d", res) ;
endmodule 

