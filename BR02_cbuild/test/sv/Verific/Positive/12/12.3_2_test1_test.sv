
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): SystemVerilog allows using the 'void' data type to discard
// a function's return value, which is done by casting the function to the 'void'
// type to avoid warning.

class Student ;
    string name ;
    int roll ;
    string addr ;
 
    function new(string n, int r, string a) ;
        name  = n ;
        roll = r ;
        addr = a ;
    endfunction

    function void show() ;
        $display("name = %s, roll = %d, addr = %s", name, roll, addr) ;
    endfunction

endclass

function int create(string n, int r, string a) ; 
    Student s1 ;
    s1 = new(n, r, a) ;

    s1.show() ;
    return 1 ;

endfunction

module test ;

    reg clk ;
    int r ;

    initial
        clk = 1 ;

    always
        #5 clk = ~clk ;

    always @(posedge clk) begin
        r = $random ;
    end

    initial
        #20 $finish ;

    initial
       void' (create("wqe",r,"err") );

endmodule

