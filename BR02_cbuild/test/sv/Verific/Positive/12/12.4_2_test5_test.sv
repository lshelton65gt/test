
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Arguments passed by reference are not copied into the 
// subroutine area. This design checks this.

function automatic donothing(ref bit [32:1] msg) ;
    msg = 32'haaaaaaaa ;
endfunction

module test(input bit [32:1] msg, bit clk, output bit changed ) ;
    bit [32:1] cur_msg ;
    bit [32:1] message ;
    
    always @ (posedge clk)
    begin
        message = msg ;
        cur_msg = message ; // Storing the message before calling the function
        donothing(message) ;
        // After calling the function
        if (cur_msg == message) changed = 1'b1 ; // message has not Changed
        else changed = 1'b0 ; // message has Changed
    end
endmodule

