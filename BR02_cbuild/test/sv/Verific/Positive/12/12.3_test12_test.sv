
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing a function with structure return type.

typedef struct {
    bit [3:0] strBit;
    real a;
} myStr;

module test(input real x, bit [3:0] y, output real realOut, bit [3:0] bitOut);
myStr modstr;
initial
begin
    modstr = func(x, y);
    realOut = modstr.a;
    bitOut = modstr.strBit;
end
endmodule

function myStr func (input real x, bit [3:0] y);
    myStr str;
    str.a = x;
    str.strBit = y;
    return str;
endfunction

