
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the 'ref' (reference) keyword.
// Arguments passed by reference must be matched with equivalent data types.
// This design checks that.

typedef struct {
    byte msg [1000:0] ;
    bit [7:0] addr ;
    bit parity ;
} packet ;

function automatic bit crc(ref packet pk ) ;
    byte ret_value = 8'h00 ;

    for( int j = 1; j <= 1000; j++ )
        ret_value ^= pk.msg[j] ;

    if (ret_value > 8'h0f) return 1'b0 ;
    else return 1'b0 ;
endfunction

module test ( input  bit enable, bit clk,
                      input  byte message[1000:0],
                      output bit valid ) ;

    typedef packet pack ; // Equivalent types are generated
    pack p1 ;

    always @ (negedge clk)
    begin
        p1.msg = message ;
        p1.addr = 8'b11001100 ;
        p1.parity = '1;
    end

    always @ (posedge clk)
        valid = (enable && crc( p1 )) ? 1'b1 : 1'b0 ;

endmodule

