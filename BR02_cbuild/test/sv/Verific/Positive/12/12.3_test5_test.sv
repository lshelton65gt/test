
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether the return statement overrides the value
// assigned to the function name.

function int func;
    func = 0;
    return 1;
endfunction

module test(input int in, output int out);

    initial
    begin
        out = in * func();
    end

endmodule
