// Carbon rewrite to make compatible with testdriver, and to eliminate
// initial block.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing part select of a packed array.

module test (input wire a, output wire b);
reg [63:0] mod_int;
reg [7:0] mod_byte;
always @(posedge a)
  begin
	mod_int = '1;
	mod_byte = mod_int[50:43];
	$display("Copied version (Should be 11111111) = %b", mod_byte);
  end
assign b = a;
endmodule
