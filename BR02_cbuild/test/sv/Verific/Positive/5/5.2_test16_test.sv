
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing signedness of a packed array.

module test;

    bit signed [7:0]modBit1;

    initial
      begin
          modBit1 = '1;
              if(modBit1 < 0)
                  $display("Declared array is signed");
              else
                  $display("Declared array is unsigned");
      end

endmodule


