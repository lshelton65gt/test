
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing write to a multi-dimensional array.

typedef logic [0:1][0:1] array1 [0:1] ;
typedef array1 array2[0:1] ;

module test(output array2 out, input array1 in1, wire [0:1][0:1] in2, wire [0:3] in3) ;

    always @(*)
    begin
        out[0] = in1 ;
        out[1][0] = in2 ;
        out[1][1] = in3 ;
    end

    initial
        #5 $display("out[1][1][0] = %1d", out[1][1][0]) ;

endmodule
