
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of slicing of packed array.

module test;
bit [3:0][7:0] mod_bit;
byte mod_byte;
initial
begin
	mod_bit = '1;
	mod_byte = mod_bit[3];
	$display("(Should print 11111111) %b", mod_byte);
end
endmodule
