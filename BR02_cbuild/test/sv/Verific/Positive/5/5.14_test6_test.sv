
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the rules for queues. 
// Deleting the first 2 elements in position 0 & 1.

module test (input init1, init2, output bit out);
    int Q[$] ;
    int ele ;

    always @ (init1)
    begin
        for (int i = 0; i < 10; i++)
            Q = {Q, i*10} ;
    end

    always @ (init2)
    begin
        ele = Q[2] ; // holding the 3rd element of Q

        Q = Q[2:$] ; // deleting the first 2 element of pos. 0 & 1

        if (Q[0] == ele)
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

