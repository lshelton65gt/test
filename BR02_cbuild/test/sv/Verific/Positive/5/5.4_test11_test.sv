 
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing variable slicing of a packed array.

module test(input in, output out) ;

    bit [31:0][7:0] mod_bit [3:0] ;
    integer mod_in [3:0] ;

    initial 
    begin
        mod_bit = '{4{{8{$random}}}} ;
        for(int i = 7 ; i >= 3 ; i = i-4)
        begin
            for(int j = 3 ; j >= 0 ; j--)
               mod_in[j] = mod_bit[j][i-:4] ;
    
            for(int j = 3 ; j >= 0 ; j--)
                $display("\n%b", mod_in[j]) ;
        end
    end

    assign out = in ;

endmodule

