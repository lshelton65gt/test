
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of indexing on multi dimensional packed array.

module indexing(input logic [4:1][0:7] arr1,
                input logic [1:0][2:0][3:0] arr2,
                output logic [7:0] arr1_and_out,
                output logic [11:0] arr2_and_out);


    logic [7:0] arr1_ele1, arr1_ele2, arr1_ele3, arr1_ele4;
    logic [2:0][3:0] arr2_ele1, arr2_ele2;

    always @ *
    begin
        arr1_ele1 = arr1[4];
        arr1_ele2 = arr1[3];
        arr1_ele3 = arr1[2];
        arr1_ele4 = arr1[1];

        arr2_ele1 = arr2[1];
        arr2_ele2 = arr2[0];

        arr1_and_out = arr1_ele1 & arr1_ele2 &
                       arr1_ele3 & arr1_ele4;

        arr2_and_out = arr2_ele1 & arr2_ele2; 
    end

endmodule

