
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of run time warning for out of range index.

module test;
bit [7:0] mod_bit = '1;
int mod_in = 5;
initial
  begin
	repeat (10) #1 mod_in = mod_in + 1;
  end
always @ (mod_in)
  begin
	$display("%b", mod_bit[mod_in]);
  end
endmodule
