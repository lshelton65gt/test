
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether an one dimensional unpacked array can be 
// assigned as a whole by slicing.

module test(input in, output out);
    bit mod_bit1 [5:0] = '{6{1'b1}};
    bit mod_bit2 [3:0];
    initial
    begin
        mod_bit2 [3:0] = mod_bit1 [3:0];
        $display("Should print 1111 ");
        for( int i = 3; i >= 0; i--)
            $display("%b", mod_bit2[i]);
    end
    assign out = in ;
endmodule	
