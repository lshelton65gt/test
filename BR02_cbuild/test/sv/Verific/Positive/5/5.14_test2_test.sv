
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the rules of queue. It deletes
// the 1st and last item of a queue at a time.

module test (input init1, init2, output bit out);
    int Q[$] ;
    int first, last ;

    always @ (init1)
    begin
        for (int i = 0; i < 10; i++)
            Q = {Q, i*10} ;
    end

    always @ (init2)
    begin
        first = Q[1] ;  // holding the 2nd element of Q.
        last = Q[$-1] ; // holding the last but one element of Q.

        Q = Q[1:$-1] ;  // deleting the first and last element of Q.

        if ((Q[0] == first) && (Q[$] == last))
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

