
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of indexing on multidimensional unpacked array.

typedef enum 
        {
          PHYS = 0,
          CHE, 
          BIO,
          MATHS
        } subjects;

module indexing #(parameter stu_num = 8)
                 (input int num_arr [stu_num -1:0][0:3],
                  output int phys_avg, che_avg, 
                       bio_avg, maths_avg);

    int phys_sum, che_sum, bio_sum, maths_sum;
    int i;

    always @(*)
    begin
       phys_sum = 0;

       for(i = stu_num -1; i >= 0; i=i-1)
           phys_sum = phys_sum + num_arr[i][PHYS] ;

       phys_avg = phys_sum / stu_num;

       che_sum = 0;

       for(i = stu_num -1; i >= 0; i=i-1)
           che_sum = che_sum + num_arr[i][CHE] ;

       che_avg = che_sum / stu_num;

       bio_sum = 0;

       for(i = stu_num -1; i >= 0; i=i-1)
           bio_sum = bio_sum + num_arr[i][BIO] ;

       bio_avg = bio_sum / stu_num;

       maths_sum = 0;

       for(i = stu_num -1; i >= 0; i=i-1)
           maths_sum = maths_sum + num_arr[i][MATHS] ;

       maths_avg = maths_sum / stu_num;

    end

endmodule

