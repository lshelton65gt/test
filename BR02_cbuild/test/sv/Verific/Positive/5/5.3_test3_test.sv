
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Declare same array variable both in true path and false
// path of if generate with different dimensions 

module test(output bit result);

    int f_width, s_width;

    child #(2) I(f_width);
    child I1(s_width);

    always @(f_width, s_width)
        if(f_width != s_width)
            result = 1;
        else
            result = 0;
endmodule

module child(output int bit_width);
    parameter p = 5;
    generate
      begin : b
        if(p < 4)
            logic [3:0][0:7] arr [3:0];
        else
            logic [0:5] arr [3:0][3:0] ;
      end

    endgenerate

    assign bit_width = $bits(b.arr[2][2]);

endmodule
