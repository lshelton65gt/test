
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing system function $left, $right, $low, $high, 
// $increment, $length, $dimension.

module test;
parameter p = 15;
parameter q = 7;
parameter r = 3;
parameter s = 1;
logic left, right, low, high, increment, length;
bit [r:0][p:q] mod_bit [s:0][3:0];
initial
begin
	for(int i = 1; i <= $dimensions(mod_bit); i++)
	begin
		case(i)
			1: begin
			   	left = ($left(mod_bit, i) == s);
			   	right = ($right(mod_bit, i) == 0);
				high = ($high(mod_bit, i) == ((s>0)? s: 0));
				low = ($low(mod_bit, i) == ((s<0)? s:0));
				increment = ($increment(mod_bit, i) == ((s>0)? 1: -1));
				length = ($length(mod_bit, i) == ($high(mod_bit, i) - $low(mod_bit, i) +1));
			   end
			2: begin
			   	left = ($left(mod_bit, i) == 3);
			   	right = ($right(mod_bit, i) == 0);
				high = ($high(mod_bit, i) == 3);
				low = ($low(mod_bit, i) == 0);
				increment = ($increment(mod_bit, i) == 1);
				length = ($length(mod_bit, i) == ($high(mod_bit, i) - $low(mod_bit, i) +1));
			   end
			3: begin
			   	left = ($left(mod_bit, i) == r);
			   	right = ($right(mod_bit, i) == 0);
				high = ($high(mod_bit, i) == ((r>0)? r: 0));
				low = ($low(mod_bit, i) == ((r<0)? r:0));
				increment = ($increment(mod_bit, i) == ((r>0)? 1: -1));
				length = ($length(mod_bit, i) == ($high(mod_bit, i) - $low(mod_bit, i) +1));
			   end
			4: begin
			   	left = ($left(mod_bit, i) == p);
			   	right = ($right(mod_bit, i) == q);
				high = ($high(mod_bit, i) == ((p>q)? p: q));
				low = ($low(mod_bit, i) == ((p<q)? p:q));
				increment = ($increment(mod_bit, i) == ((p>q)? 1: -1));
				length = ($length(mod_bit, i) == ($high(mod_bit, i) - $low(mod_bit, i) +1));
			   end
		endcase
		if(left && right && high && low && increment && length)
			$display(" Testing of $left, $right, $high, $low, $increment, $length and $dimensions for dimension  %d is a success\n", i);
	end
end
endmodule

module top;
test instan_mod1();
test #(.p(7), .q(15), .r(7), .s(-3)) instan_mod2();
endmodule
