// Carbon modified to make compatible with cbuild and testdriver.
//

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests reading and writing a slice of the array,
// e.g., A[i:j] = B[i:j]

module test(input in, output logic out) ;
    reg [1:0][3:0] array3 ;
    reg [1:0][3:0] array4 ;
    reg [1:0][3:0] temp ;
    int start1, end1, start2, end2 ;
    task showArray ;
        input reg [1:0][3:0]array ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $write("%d ",  array[i][j]) ;
        end 
    endtask
    int start3, end3, start4, end4 ;
`ifdef NOTUSED
    mod initialization (array3, array4) ;
`endif
    always @(posedge in)
    begin
        int count1;
        int count2;
        int i, j ;
        count1 = 0;
        count2 = 7;
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
            begin
                array3[i][j] = count1 ++ ;
                array4[i][j] = count2 -- ;
            end
        //-- Reading and writing a slice of the array, e.g., A[i:j] = B[i:j] 
        #15
        temp[1][3:2] = array3[1][3:2] ;
        showArray(temp) ;
        $display ("\n ") ;
        showArray(array3) ;
        $display ("\n ") ;
        temp[1][3:2] = array4[0][1:0] ; 
        showArray(temp) ;
        $display ("\n ") ;
        showArray(array4) ;
        $display ("\n ") ;
    end
    assign out = in ;
endmodule

`ifdef NOTUSED
module mod (array1, array2) ;
    output reg [1:0][3:0] array1 ;
    output reg [1:0][3:0] array2 ;
    int count1 = 0 ;
    int count2 = 7 ;
    int i, j ;
    initial begin
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
            begin
                array1[i][j] = count1 ++ ;
                array2[i][j] = count2 -- ;
            end
    end
endmodule
`endif
