
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing packed arrays with bit and reg types.

module test;

    bit [7:0] mod_bit;
    reg [7:0] mod_reg;

    initial
    begin
        mod_bit = '1;
        mod_reg = "!";
        if((mod_bit == 8'b11111111) && mod_reg == "!")
            $display("Testing of packed array with bit and reg type is successfull");
    end

endmodule
