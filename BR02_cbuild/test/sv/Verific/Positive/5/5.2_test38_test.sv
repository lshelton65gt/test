
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests unpacked array of enum type.

module test;
typedef enum {mem0, mem1, mem2} my_type;
my_type var1[3:0];
initial
begin
	for(int i = 3; i >= 0; i--)
		case(i)
			3 : var1[i] = mem2;
			2 : var1[i] = mem1;
			1 : var1[i] = mem0;
			0 : var1[i] = mem2;
		endcase
	for(int j = 3; j >= 0; j--)
		$display("%d\n", var1[j]);
end
endmodule

