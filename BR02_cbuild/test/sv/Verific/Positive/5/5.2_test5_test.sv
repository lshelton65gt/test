
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of packed and unpacked arrays of bit and reg.

module test(arr1, arr2, arr3, arr4, arr5, arr6) ;

    input bit [2:0][5:3][-2:1] arr1 [4:0][-3:-2] ;
    input bit arr2 [5:8][-1:2][3:0] ;
    input bit [7:5][2:0] arr3 ;

    output bit [2:0][5:3][-2:1] arr4 [4:0][-3:-2] ;
    output bit arr5 [5:8][-1:2][3:0] ;
    output bit [7:5][2:0] arr6 ;
    //output reg [2:0][5:3][-2:1] arr4 [4:0][-3:-2] ;
    //output reg arr5 [5:8][-1:2][3:0] ;
    //output reg [7:5][2:0] arr6 ;

    assign arr4 = arr1 ;
    assign arr5 = arr2 ;
    assign arr6 = arr3 ;
    
endmodule
    
