
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests clearing of a queue by assigning '{}'
// to it.

module test (input init1, init2, output bit out);
    int Q[$] ;

    always @ (init1)
    begin
        for (int i = 0; i < 10; i++)
            Q = {Q, i*10} ;
    end

    always @ (init2)
    begin
        Q = {} ;

        if (Q.size == 0) // clear the queue Q
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

