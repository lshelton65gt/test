
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests Dynamic Array method delete(),

module test(input in, output out) ;

    parameter p1 = prod_of_N() ;

    initial
        $display("The value of parameter p1 = %1d", p1) ;

    assign out = in + func() ;

    function integer func() ;
        int arr [] = new[ 10 ];    // create a temporary array of size 10 

        arr.delete;    // delete the array contents
        if (arr.size() == 0)
            return 11 ;
        else 
            return 10 ;
    endfunction

    function integer prod_of_N() ;
        int prod = 1 ;
        for (int i=1; i<11; i++)
           prod = prod + i ;
        return prod ;
    endfunction

endmodule

