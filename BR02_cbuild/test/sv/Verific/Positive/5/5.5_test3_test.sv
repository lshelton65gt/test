
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array query function $high on packed array. 
// $high returns the maximum of $left and $right of the dimension.

module test #(parameter dim1 = 2, parameter dim2 = 1)
              (output integer high1, high2);

    parameter SIZE = 8;
    logic [0:1][4:-(SIZE-1)][-1:-(get_size(SIZE))] arr1;
    int arr2;

    function integer get_size;
        input integer in_size;
        get_size = in_size - 2;
    endfunction

    initial 
    begin
        high1 = $high(arr1, dim1);
        high2 = $high(arr2, dim2);
    end

endmodule
