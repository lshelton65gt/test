
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing reading from and writing to a variable slice of a packed array.

module test;
bit [7:0] mod_bit;
logic [7:0] mod_log;
int i=7;
int j=0;
initial
  begin
	mod_bit = 8'b 01010101;
	while(i>0)
	  begin
		mod_log[i -: 2] = mod_bit [j +: 2];
		i = i-2;
		j = j+2;
	  end 
	$display("Output = %b", mod_log);
  end
endmodule
