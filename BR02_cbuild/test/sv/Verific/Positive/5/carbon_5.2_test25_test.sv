// Carbon modified to be compatible with testdriver, and to remove
// initial block.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing unpacked arrays with various data types.

module test(input in, output logic out) ;

    integer mod_in[3:0] ;
    logic mod_log[3:0] = '{4{1'b0}} ;
    bit mod_bit[3:0] = '{4{1'b1}} ;

    always @(posedge in)
    begin
        for(int i = 3; i >= 0; i--)
        begin	
            mod_in[i] = $random ;
            out = (mod_log[i] == 0) && (mod_bit[i] == 1) ;
        end
        if(out == 1)
            $display("Testing unpacked array with integer, logic and bits successfull") ;
    end


endmodule
