
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of slicing on one dimensional unpacked array.

module test(input int arr [1:7],
               output int root_val,
               output int avg_of_sibsum);


    int sib1 [1:0], sib2 [1:0], sib3 [1:0] ;
    int sib1_sum, sib2_sum, sib3_sum;

    always @ (*)
    begin
        sib1 = arr[2:3];
        sib2 = arr[4:5];
        sib3 = arr[6:7];

        sib1_sum = sib1[1] + sib1[0];
        sib2_sum = sib2[1] + sib2[0];
        sib3_sum = sib3[1] + sib3[0];

        root_val = arr[1];
        avg_of_sibsum = (sib1_sum + sib2_sum + sib3_sum ) / 6;
    end

endmodule
