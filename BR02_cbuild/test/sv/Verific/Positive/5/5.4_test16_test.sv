
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses variable slicing, for which starting position
// may be variable but the size of the slice must be constant.
// The constant used is the genvar variable of a for-generate loop.

module test (input clk,
             input [7:0] in1, in2,
             output reg [7:0] out1, out2);

    logic [0:7][31:0]m;
    byte b;

    genvar g;

    generate
        for(g=0; g<8; g++)
        begin: for_gen_g
            always@(posedge clk)
            begin
                b[g] = ^{in1, in2};
                m[g] = {2{in1, in2}};

                out1[g] = |m[b[g]+:(g<2?1:g-1)];
                out2[g] = |m[b[g]+:(g<2?1:g-1)];
            end
        end
    endgenerate

endmodule

