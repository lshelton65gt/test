
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing assignment to a packed array.

module test(output bit result,
             input logic [7:0] bit_vec,
             input bit option) ;

    bit [1:0][3:0] p_arr;
              

    always @ *
    begin
        if(option)
            p_arr = 8'b11011000;
        else
            p_arr = bit_vec;

        if(((p_arr[1][3] == 1'b1) || (p_arr[1][3] == bit_vec[7])) &&
           ((p_arr[0][0] == 1'b0) || (p_arr[0][0] == bit_vec[0])))
            result = 1;
        else
            result = 0;
    end

endmodule

