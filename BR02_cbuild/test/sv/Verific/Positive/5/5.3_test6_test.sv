
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of part select on multidimensional array.

module test(input int num1_arr [3:0], num2_arr[3:0],
                output int sum1, sum2, avg1, avg2);


    bit signed [7:0][15:0] tmp_arr [1:0];

    always @ *
    begin
        tmp_arr[1][7:6] = num1_arr[3];
        tmp_arr[1][5:4] = num1_arr[2];
        tmp_arr[1][3:2] = num1_arr[1];
        tmp_arr[1][1:0] = num1_arr[0];

        tmp_arr[0][7:6] = num2_arr[3];
        tmp_arr[0][5:4] = num2_arr[2];
        tmp_arr[0][3:2] = num2_arr[1];
        tmp_arr[0][1:0] = num2_arr[0];

        sum1 = tmp_arr[1][7:6] + tmp_arr[1][5:4] +
               tmp_arr[1][3:2] + tmp_arr[1][1:0] ;

        sum2 = tmp_arr[0][7:6] + tmp_arr[0][5:4] +
               tmp_arr[0][3:2] + tmp_arr[0][1:0] ;
       
        avg1 = sum1 / 4;
        avg2 = sum2 / 4; 
    end

endmodule
 
