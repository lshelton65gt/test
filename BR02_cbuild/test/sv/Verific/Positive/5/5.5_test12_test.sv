
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array query function $low on array.

module test(input int dim,
              output integer low1, low2, low3); 

    parameter SIZE = 8;

    logic [0:-(SIZE  * 2) -1][-2:3] arr1 [0:-(get_size(SIZE)) -1][4:-3];

    logic arr2 [-1: -(get_size(SIZE)) + 2][0:-SIZE -1];

    int arr3 [-7 :-1];

    function integer get_size(integer in_size);
        get_size = in_size - 2;
    endfunction

    always @ (*)
    begin
        low1 = $low(arr1, dim);
        low2 = $low(arr2, dim);
        low3 = $low(arr3, dim);
    end

endmodule
