// Modified to make this compatible with testdriver, and to eliminate non-constant values
// in the initial block.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of reading from and writing to multidimensional arrays.

module test(input wire a, output wire b);

bit [3:0][8:1] mod_bit [1:5];

always @(posedge a)
  begin
	for(int i = 1; i <=5; i++)
		mod_bit[i] = '0;
	mod_bit [2] = mod_bit [1] + 2;
    	mod_bit [3][0] = mod_bit [2][0];
	if(mod_bit[2] != 2)
		$display("Failure");
	else if(mod_bit[3][0] != 8'b 00000010)
		$display("\nFailure"); 
	else 
		$display("Testing of reading from and writing to a multidimensional array is a success");
  end

assign b = a;

endmodule
