
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing packed array of packed structures.

module test;
typedef struct packed {
			  int str_int;
			  bit [7:0] str_bit;
		      } my_str;
my_str [3:0] str;
assign str = '1;
initial
  begin
	$display(" Testing packed array of packed structure is positive ");
  end
endmodule 

