
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Typedef can be used to define a new type on multidimensional arrays
// both packed and unpacked. This design uses typedef on both packed
// and unpacked arrays and uses the new type to define array in the design.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2) ;

    typedef bit [1:0][0:3] double_dim [0:1] ;    // double_dim is unpacked array of double dimensional packed array of bits

    double_dim dd1, dd2 ;

    always@(posedge clk)
    begin
        dd1 = '{ in1, in2 } ;
        dd2 = '{ in2, in1 } ;

        out1 = {dd1[0], dd2[1]} ^ {dd1[1], dd2[0]} ;
        out2 = {dd1[1], dd2[0]} & {dd1[0], dd2[1]} ;
    end

endmodule
