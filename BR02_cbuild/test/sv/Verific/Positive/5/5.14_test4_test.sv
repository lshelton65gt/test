
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the rules of queue. 
// If in Q[a:b], either 'a' or 'b' are 4-state expressions containing 
// X or Z values then, it yields the an empty queue {}.

module test (input init1, init2, output bit out);
    parameter logic a = 1'bx ;
    parameter logic b = 1'bz ;
    int Q[$] ;
    int Q1[$] ;

    always @ (init1)
    begin
        for (int i = 0; i < 10; i++)
            Q = {Q, i*10} ;
    end

    always @ (init2)
    begin
        Q1 = Q[a:b] ;

        if (Q1.size == 0)
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

