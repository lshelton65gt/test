
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests reading and writing the array, e.g., A = B  

module test (output [1:0][3:0] out1, output [1:0][3:0] out2, input in, clk) ;

    logic out ;
    bit [1:0][3:0]temp ;
    mod initialization (out1, out2) ; 

    task showArray ;
        input [1:0][3:0] out ;
    endtask

    always @ (posedge clk)
    begin   
        #5 out = out1 == out2 ;
        if (out) begin 
            showArray(out1) ;
            showArray(out2) ;
        end
        else begin
            showArray(out1) ;
            showArray(out2) ;
        end
    end

    initial begin
        //-- Reading and writing the array, e.g., A = B
        #15
        temp = out1 ;
        showArray(out1) ;
        showArray(temp) ;
    end

endmodule

module mod (array1, array2) ;

    output reg [1:0][3:0] array1 ;
    output reg [1:0][3:0] array2 ;
    int count1 = 0 ;
    int count2 = 7 ;
    int i, j ;
    initial begin
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array1[i][j] = count1 ++ ;
                    array2[i][j] = count2 -- ;
                end
        $display("Instatiating module mod") ;
    end

endmodule

