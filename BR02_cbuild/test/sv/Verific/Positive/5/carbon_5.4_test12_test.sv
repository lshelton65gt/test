// this is a carbon 'improved' version of the 5.4_test12_test.sv file
// changes are:
// 1. moved logic out of initial block so that carbon can generate code for it
// 2. changed $display to $fwrite

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether an one dimensional unpacked array can be 
// assigned as a whole by slicing.

module test(input in, output out);
    bit mod_bit1 [5:0] = '{6{1'b1}};
    bit mod_bit2 [3:0];
    always @(*)
    begin
        mod_bit2 [3:0] = mod_bit1 [3:0];
        $display("Should print 1111 ");
        $display("Actual print %b%b%b%b", mod_bit2[3],mod_bit2[2],mod_bit2[1],mod_bit2[0]);
    end
    assign out = in ;
endmodule	
