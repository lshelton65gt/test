
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of reading from and writing to a packed array.

module test;

    bit [7:0] mod_bit1, mod_bit2;

    initial
    begin
        mod_bit1 = 8'b1;
        mod_bit2 = mod_bit1;
        if(mod_bit2 == mod_bit1)
            $display("Testing of reading from and writing to a packed array is a success");
    end

endmodule
