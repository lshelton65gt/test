
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether a data type can adapt to the vector width of the
// packed array, to which it is assigned.

module test;

    bit signed [47:0] mod_bit;

    initial
    begin
        mod_bit = 1;
        mod_bit = mod_bit * (-1);
        if(mod_bit == -1)
            $display("Data type can adapt to the vector width of the packed array");
        else
            $display("Data type can not adapt to the vector width of the packed array");
    end

endmodule		 
