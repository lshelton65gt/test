
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test associative array with class index. Indices are class
// 'check''s objects created by 'new' method and object not created by 'new' method.

class check ;
    int roll ;
    string name ;

    function new( int r, string n) ;
        roll = r  ;
        name = n ;
    endfunction
    function display ;
        $display("Roll: %1d   Name: %s", roll, name) ;
    endfunction
endclass

module test(input int in, output out) ;

    int a[*] ;
    int c[check] ;
    check ch, ch1, ch2 ;

    initial begin
        a['x] = 2 ;
        a['z] = 'X ;
        a[3] = 10 ;

        ch = new(1,"Abhishek") ;
        ch1 = new(2,"Subhojit") ;

        c[ch] = 78 ;
        c[ch1] = 56;
        c[ch2] = 34 ;

        ch.display() ;
        ch1.display() ;
    end

    assign out = $onehot(in) ;

endmodule

