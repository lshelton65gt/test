
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the Dynamic Array method size(),
// along with the dynamic array initialization by another dynamic array.

module test(input in, output out) ;

    parameter p1 = prod_of_N() ;

    initial
        $display("parameter p1 = %d", p1) ;

    function integer func() ;
        integer arr[] ;
        integer sum_of_nos = 0 ;
        integer pqr[] ;

        pqr = new[10] ; // Creating pqr
        for (int i = 0; i < pqr.size; i++) // Assigning pqr
            pqr[i] = i + 1 ;

        arr = new[10](pqr) ; // Creating arr and initializing with the contents of pqr

        for (int i = 0; i < arr.size; i++)
            sum_of_nos += arr[i] ;
        if (sum_of_nos == 55)
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer prod_of_N() ;
        int prod = 1 ;
        for (int i=1; i<11; i++)
           prod = prod + i ;
        return prod ;
    endfunction

    assign out = in + func() ;

endmodule

