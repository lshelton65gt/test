
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test that multiple unpacked dimensions can also 
// be defined in stages with typedef.

module test(input byte byte_arr [3:0],
                output byte byte_and, byte_or, byte_xor);


    typedef byte my_byte_arr [1:0];
    my_byte_arr arr [1:0];

    always @*
    begin
        arr[1][1] = byte_arr[3];
        arr[1][0] = byte_arr[2];
        arr[0][1] = byte_arr[1];
        arr[0][0] = byte_arr[0];

        byte_and = arr[1][1] & arr[1][0] & arr[0][1] 
                   & arr[0][0];
        byte_or = arr[1][1] | arr[1][0] | arr[0][1] 
                   | arr[0][0];
        byte_xor = arr[1][1] ^ arr[1][0] ^ arr[0][1] 
                   ^ arr[0][0];
    end

endmodule
