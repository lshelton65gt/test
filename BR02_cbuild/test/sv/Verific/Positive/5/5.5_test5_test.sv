
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array query function $increment on packed array.

module test #(parameter dim1 = 1, dim2 = 2)
                 (output integer incr1, incr2);

    parameter SIZE = 8;
    logic [1:1][-(SIZE-1):4][-1:-(get_size(SIZE))] arr1;
    int arr2[-(SIZE-1):4];

    function integer get_size;
        input integer in_size;
        get_size = in_size - 2;
    endfunction

    initial #5 
    begin
        incr1 = $increment(arr1, dim1);
        incr2 = $increment(arr2, dim2);
    end

endmodule
