
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of variable slicing on packed array

module test(input bit [7:0][0:3] arr1,
               output bit result);

    byte unsigned byte_arr [3:0];

    bit [3:0] red_and, red_or;
    bit reduct_and_res1, reduct_and_res2;
    bit reduct_or_res1, reduct_or_res2;

    int i, j;

    always @ (*)
    begin
        j = 3;
        for(i = 7; i > 0; i=i -2)
        begin
            byte_arr[j] = arr1[i -: 2];        
            red_and[j] = & byte_arr[j];
            red_or[j] = | byte_arr[j];
            j = j - 1;
        end

        reduct_and_res1 = & red_and;
        reduct_and_res2 = & arr1;

        reduct_or_res1 = | red_or;
        reduct_or_res2 = | arr1;

        result = 0;
        if((reduct_and_res1 == reduct_and_res2) &&
           (reduct_or_res1 == reduct_or_res2))
            result = 1;
    end

endmodule
