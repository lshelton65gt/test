// Carbon modified to make compatible with testdriver, and to
// eliminate initial block.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing assignment to a packed array.

module test (input wire i, output wire o);

    bit [4:0][3:0] bit1;
    shortint a, b;

    assign a = 5;
    assign b = 10;

    assign bit1 = a - b;

    always @(posedge i)
    begin
          $display("bit width of the operation performed:(should be 20) %d bits = %d bits - %d bits", $bits(bit1), $bits(b), $bits(a));
          $display(" %b = %b - %b", bit1, a, b);
    end

    assign o = i;
    
endmodule



