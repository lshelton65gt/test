
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of variable slicing on an unpacked array.

module test #(parameter p = 8)(input shortint in_arr[p:0],
               output shortint sum [0 : p-1]);

    generate

        genvar i;
        for(i = 1; i <= p; i = i + 1)
        begin : b
            shortint arr [i:0];

            assign arr = in_arr[p -: (i + 1)];
            ADD #(i) I(sum[i -1], arr);            
        end

    endgenerate

endmodule

module ADD #(parameter p = 1) 
            (output shortint sum, 
             input shortint arr [p :0]);

    int i ;

    always @ (*)
    begin
        sum = 0;
        for(i = p; i >= 0; i = i - 1)
            sum = sum + arr[i];
    end

    initial
        $display("Instantiating module ADD") ;

endmodule
 
