// Carbon rewrite to make compatible with testdriver, and
// to eliminate initial block.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing part select/slicing of packed array.

module test (input wire a, output wire b);

    integer mod_in [0:1];
    bit [31:0] middle [0:1];
    byte mod_byte [3:0];

    always @(posedge a)
    begin
        mod_in[1] = 10;
        mod_in[0] = 16;
        middle[0] = mod_in[0];
        middle[1] = mod_in[1];
        mod_byte = '{middle[0][7:0], middle[1][7:0], middle[0][31:24], middle[1][31:24]} ;

        $display("Should print 10, 16, 0, 0");
        for( int i = 3; i >= 0; i--)
            $display("%d", mod_byte[i]);
    end

    assign b = a;    
endmodule

