
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of reading from and writing to an element of a 
// packed array.

module test;
bit [7:0] mod_bit1, mod_bit2;
initial
begin
	mod_bit1 = 8'b 10101010;
	for( int i = 7; i >= 4; i--)
            mod_bit2[i] = mod_bit1[7-i];
	$display("Source = %b, Copied = %b", mod_bit1, mod_bit2);
end
endmodule
