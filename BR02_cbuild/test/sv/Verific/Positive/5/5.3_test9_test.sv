
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Declare multiple arrays in a single declaration

typedef enum 
        {
             ARRAY1,
             ARRAY2,
             ARRAY3
        } my_enum;

module test(input my_enum sel,
                input int dim1, dim2, dim3,
                output byte byte_out,
                output int unsigned int_out);


    logic [3:0][0:7] arr1 [1:3][2:0], arr2 [3:0], arr3;

    
    always @ *
    begin
        case(sel)
        ARRAY1 : begin
                     arr1[dim1][dim2][dim3] = 47;
                     arr1[dim1-1][dim2 +1][dim3] = 
                         arr1[dim1][dim2][dim3] * 2;
                     byte_out = arr1[dim1-1][dim2+1][dim3] +
                                arr1[dim1][dim2][dim3];
                     arr1[dim1 * 2][dim2] = 890;
                     int_out = arr1[dim1*2][dim2] * 5;                    
                 end
        ARRAY2 : begin
                     arr2[dim1][dim2] = 47;
                     arr2[dim1-1][dim2 +1]  = 
                         arr2[dim1][dim3] * 2;
                     byte_out = arr2[dim1-1][dim2+1] +
                                arr2[dim1][dim3];
                     arr2[dim1 * 2] = 890;
                     int_out = arr2[dim2] * 5; 
                 end
        ARRAY3 : begin
                     arr3[dim3] = 47;
                     arr3[dim2 +1]  = 
                         arr3[dim3] * 2;
                     byte_out = arr3[dim2+1] +
                                arr3[dim3];
                     arr3 = 890;
                     int_out = arr3 * 5; 
                 end
        endcase
    end

endmodule
