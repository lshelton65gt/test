
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Dynamic arrays are one-dimensional arrays whose size can
// be set or changed at runtime. The space for a dynamic array doesn't exist until 
// the array is explicitly created at runtime.
// Check of the built in method - new[], size(), delete()

module test ;

  int a[] ;
  string d[1:5] = '{ "w", "o", "r", "l", "d" };
  string p[] ;

  initial begin
      p = '{ d[1], "hello", d[4] }; 
      a = new [10] ;
      for(int i=0; i<10 ;i++)
            a[i] = i ;
    

      $display("The size of a--%d \n", a.size()) ;

      for(int i=0; i<10 ;i++)
          $display("value of a -- %d", a[i]) ;

      a = new [7](a) ;

      $display("The size of a--%d \n ",a.size()) ;
      for (int i=0; i<p.size(); i++)
      $display("Size of string p-- %d \n value of string p-- %s",p.size(), p[i]) ;

      a.delete() ;
      $display("The size of a--%d \n ",a.size()) ;
  end

endmodule

