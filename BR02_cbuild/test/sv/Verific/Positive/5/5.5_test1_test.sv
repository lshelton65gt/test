
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array query function $dimensions on packed array

module test (arr1, arr2, dim1, dim2); 
    output int dim1, dim2;

    parameter SIZE = 8;
    input logic [1:1][-(SIZE-1):4][-1:-(get_size(SIZE))] arr1;
    input int arr2;    

    function integer get_size;
        input integer in_size;
        get_size = in_size - 2;
    endfunction

    always @(arr1, arr2)
    begin
        dim1 = $dimensions(arr1);
        dim2 = $dimensions(arr2);
    end

endmodule
