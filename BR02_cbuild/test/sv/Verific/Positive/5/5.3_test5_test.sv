
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of bit select on multidimensional array.

module test(input clk);

    bit [1:4] [1:8] arr1 [1:3];

    logic [3:0][7:0] arr2 [2:0][1:0];

    always @(posedge clk)
    begin
        arr1[1] = '1;
        arr1[2] = ~(arr1[1]) + 4;
        arr1[3][1] = 8'b10101101;
        arr1[1][2][7] = ~arr1[2][4][6];

        arr2[2][1] = 'x;
        arr2[2][0] = '1;
        arr2[2][1][3] = 8'b10111101;
        arr2[1][0][2][6] = 1'b1; 
    end

endmodule
 
