
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the use of typedef in multidimensional arrays.

module test;
typedef bit [1:5] first_stage;
typedef first_stage [1:10] second_stage;
typedef second_stage third_stage [0:3];
third_stage final1 [0:7];
initial
begin
	$display("Size of the First dimension = %d\n", $bits(final1[0][0][1][1]));
	$display("Size of the Second dimension = %d\n", $bits(final1[0][0][1]));
	$display("Size of the Third dimension = %d\n", $bits(final1[0][0]));
	$display("Size of the Fourth dimension = %d\n", $bits(final1[0]));
end
endmodule
