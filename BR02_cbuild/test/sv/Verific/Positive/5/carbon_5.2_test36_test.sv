// Carbon modified to make compatible with cbuild and testdriver.
//

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the use of replicate operators in 
// initializing of an unpacked array. 

`ifdef NOTUSED
module test (array1, array2) ;
    output reg array1 [3:0][1:0] ;
    output reg array2 [3:0][1:0] ;

    reg array3 [3:0][1:0] = '{'{2{1'b1}}, '{2{1'b1}}, '{2{1'b0}}, '{2{1'b0}}} ;
    reg array4 [3:0][1:0] = '{'{1'b1, 1'b1}, '{2{1'b1}}, '{2{1'b0}}, '{2{1'b0}}} ;

    initial
    begin
        array1 = array3 ;
        array2 = array4 ;
    end
endmodule
`endif

module bench (input wire a, output wire b) ;
    assign b = a;
    reg array1 [3:0][1:0] ;
    reg array2 [3:0][1:0] ;
`ifdef NOTUSED
    reg array3 [3:0][1:0] = '{'{2{1'b1}}, '{2{1'b1}}, '{2{1'b0}}, '{2{1'b0}}} ;
    reg array4 [3:0][1:0] = '{'{1'b1, 1'b1}, '{2{1'b1}}, '{2{1'b0}}, '{2{1'b0}}} ;
`endif
    
    logic out ;
`ifdef NOTUSED
    test instance_mod(array1, array2) ;
`endif
    int i , j ;
   always @(posedge a)
      begin
          // array1 = '{'{2{1'b1}}, '{2{1'b1}}, '{2{1'b0}}, '{2{1'b0}}} ;
          array1 = '{'{1'b1, 1'b1}, '{1'b0, 1'b0}, '{1'b0, 1'b1}, '{1'b1, 1'b0}};
          array2 = '{'{1'b1, 1'b1}, '{2{1'b1}}, '{2{1'b0}}, '{2{1'b0}}} ;
          #5 out = (array1 == array2 ) ;
          for(i = 3 ; i >= 0 ; i--)
              for (j = 1 ; j >= 0 ; j--)
                  $display("array1[%d][%d]=%b  array2[%d][%d]=%b", i, j, array1[i][j], i, j, array2[i][j]) ;
          if (out) $display("\n both arrays are equal\n\n") ;
          else $display("\n Not equal \n\n") ;
      end
endmodule

