// Modified by carbon to make compatible with testdriver, and
// to eliminate initial block.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of reading from and writing to a slice of a packed array.

module test(input wire a, output wire b);

    reg [95:0] mod_reg1;
    reg [103:0] mod_reg2;

    always @(posedge a)
    begin
        mod_reg1 = "Hello World!";
        mod_reg2 [103:63] = mod_reg1 [95:55];
        $display("Source text = %s, Copied text = %s", mod_reg1, mod_reg2);
    end

    assign b = a;
    
endmodule
