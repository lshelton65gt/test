
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Two or more arrays can be declared in a single declaration, like...
// bit [3:0] [0:7] b1 [0:3] [7:0], b2 [15:0], b3 ; all of these have the
// same multiple packed dimension of [3:0][0:7]. This design checks this.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2) ;

    bit [1:0][0:3] b1 [0:2][4:0], b2 [5:0], b3 ;

    always@(posedge clk)
    begin
        b1 = '{'{in1, in2, in1, in1+in2, in1 - in2}, '{in1, in2, in1, in1+in2, in1 - in2}, '{in1, in2, in1, in1+in2, in1 - in2}} ;
        b2 = '{in2, in1, in2, in1, in2, in1} ;

        out1 = b1[0][3] | b2[2] ;
        out2 = b3 + b2[1][0] ;
    end

endmodule

