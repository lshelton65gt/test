
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Associative arrays can be passed as arguments only to 
// associative arrays of a compatible type and with the same index type. Other
// types of arrays, whether fixed-size or dynamic, cannot be passed to 
// subroutines that accept an associative array as an argument. Likewise, 
// associative arrays cannot be passed to subroutines that accept other types
// of arrays. 

// Passing an associative array by value causes a local copy of the 
// associative array to be created. so, no chnage in array b in module test is seen.

function void checking(int a[string]) ;
    string s ;
    a["e"] = 5 ; 
    a["f"] = 6 ; 
    a["g"] = 7 ; 
    a["h"] = 8 ; 

    $display("Printing elements of array a from function: ") ;
    if ( a.first( s ) )
       do 
          $display( " %d\n", a[ s ] );
       while ( a.next( s ) );
endfunction

module test ;

   string s ;
   int b[string] ;
   initial begin
       b["a"] = 1 ;
       b["b"] = 2 ;
       b["c"] = 3 ;
       b["d"] = 4 ;
   end

   initial begin
      $display("\n Number of element in array b: %d",b.num()) ;
      $display("Printing elements of array b, before call of function: ") ;
      if ( b.first( s ) )
         do 
             $display( " %d\n", b[ s ] );
         while ( b.next( s ) );
      checking(b) ;
      $display("Printing elements of array b, after call of function: ") ;
      if ( b.first( s ) )
         do 
             $display( " %d\n", b[ s ] );
         while ( b.next( s ) );
   end

endmodule

