
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array where width of the array depends on genvar.

module test(sum, in1, in2, reset);

    parameter p = 8 ;

    output int sum;
    input [p-1:0] in1, in2;
    input reset;

    generate
        genvar i, j;

        for(i = 1; i < p; i=i+1)
        begin:out
   
            bit [i:0] b[i:0], a[i:0], c[i:0];
           
            for(j = i; j >= 0; j=j-1)
            begin:inner
               always @(*) begin
                if (reset)
                   sum = 0 ;
                else begin
                    b[j] = in1;
                    a[j] = in2;
                    c[j] = a[j] + b[j];
                    sum = sum + c[j] ;
                end
               end
            end
                   
        end

    endgenerate

endmodule
