// Carbon modified to make compatible with cbuild and testdriver.
// This test uses an initial block in a module instantiation to
// initialize packed arrays, which are then accessed in other
// initial blocks. Since we can only use constants in initial
// blocks, these have all been consolidated into a single always
// block. Also, the showArray task hadn't been written.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests reading and writing the array, e.g., A = B  

module test (output reg [1:0][3:0] out1, output reg [1:0][3:0] out2, input in, clk) ;

    logic out ;
    bit [1:0][3:0]temp ;
`ifdef DONT_USE
    mod initialization (out1, out2) ; 
`endif
    task showArray ;
        input [1:0][3:0] out ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $write("%d",  out[i][j]) ;
        end 
    endtask

    always @ (posedge clk)
    begin   
       int count1;
       int count2;
       int i, j ;
       count1 = 0 ;
       count2 = 7 ;
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    out1[i][j] = count1 ++ ;
                    out2[i][j] = count2 -- ;
                end
        #5 out = out1 == out2 ;
        if (out) begin 
            showArray(out1) ;
            showArray(out2) ;
        end
        else begin
            showArray(out1) ;
            showArray(out2) ;
        end
        //-- Reading and writing the array, e.g., A = B
        #15
        temp = out1 ;
        showArray(out1) ;
        showArray(temp) ;
    end

`ifdef DONT_USE
    initial begin
        //-- Reading and writing the array, e.g., A = B
        #15
        temp = out1 ;
        showArray(out1) ;
        showArray(temp) ;
    end
`endif
   
endmodule

`ifdef DONT_USE
module mod (array1, array2) ;

    output reg [1:0][3:0] array1 ;
    output reg [1:0][3:0] array2 ;
    int count1 = 0 ;
    int count2 = 7 ;
    int i, j ;
    initial begin
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array1[i][j] = count1 ++ ;
                    array2[i][j] = count2 -- ;
                end
        $display("Instatiating module mod") ;
    end

endmodule
`endif
