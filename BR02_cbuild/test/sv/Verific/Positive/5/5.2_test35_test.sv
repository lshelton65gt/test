
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether a packed array can be assigned to a member of an unpacked array or not.

module test;

integer mod_in[3:0];
reg [63:0] mod_reg;

initial
begin
	mod_reg = {$random, $random};
	mod_in[3] = mod_reg[31:0];
	mod_in[2] = mod_reg[63:32];
	for (int i = 3; i > 1; i--)
		$display("%d\t", mod_in[i]);
end

endmodule
