// Carbon modified to fix find $display loop, to make this test
// compatible with testdriver, and to get rid of the initial block.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing unpacked arrays with various data types.

module test(input wire i, output wire o);

    assign o = i;

    bit [7:0] mod_char[5:0] = { "H", "e", "l", "l", "o", "\n"} ; 
    typedef byte mod_byte[10:0];
    mod_byte my_byte = {"H","e","l","l","o"," ", "W","o","r","l","d"};
    int mod_int [3:0];
    int size;

    always @(posedge i)
    begin
        for (int i = 3; i >= 0; i--)
            mod_int[i] = $random;
        size = $bits(mod_char);
        if(size == 64)
            $display("$bits works on unpacked arrays");
        for (int j = 5; j >= 0; j--)
            $display("%c", mod_char[j]);
    end    

endmodule
