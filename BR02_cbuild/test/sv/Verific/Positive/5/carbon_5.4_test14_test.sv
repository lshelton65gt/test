// Carbon modified to be compatible with cbuild and testdriver.
//
// This test has been modified significantly from the original:
// 1) ios add to top level
// 2) The test had relied previously on 'initial' blocks in
//    different modules running in a certain sequence. Since
//    cbuild does not support arbitrary statements in initial
//    blocks, the test was changed to consolidate all the
//    initial blocks into a single always block, triggered by
//    an input change.
//
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests reading and writing a variable slice of 
// unpacked array


module test (input wire a, output wire b);
    assign b = a;
    reg startup = 1;
    int array3 [1:0][3:0] ;
    int array4 [1:0][3:0] ;
    logic out ;
    int temp [1:0][3:0] ;
    task showArray ;
        input int array[1:0][3:0] ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $write("%d",  array[i][j]) ;
        end 
    endtask
    int start1, start2 ;
    int i,j;
    int count1 = 0, count2 = 7; 
    always @(posedge a)
    begin
        $display("\n Initializing arrays\n") ;
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array3[i][j] = count1 ++ ;
                    array4[i][j] = count2 -- ;
                end
        //-- Reading and writing a slice of the array, e.g., A[i:j] = B[i:j] 
        #15
        start1 = 2 ; 
        temp[1][start1-:2] = array3[1][start1-:2] ;
        showArray(temp) ;
        $display ("\n ") ;
        showArray(array3) ;
        $display ("\n ") ;
        start1 = 2 ;
        start2 = 3 ;
        temp[1][start1-:1] = array4[0][start2-:1] ; 
        showArray(temp) ;
        $display ("\n ") ;
        showArray(array4) ;
        $display ("\n ") ;
        #15 ;
        if (array3[1][3:2] != array4[1][3:2] && array3[1][3:3] == array4[0][0:0])
            $display("\n Its OK") ;
        else 
            $display("\n Its not OK") ;
    end
endmodule

