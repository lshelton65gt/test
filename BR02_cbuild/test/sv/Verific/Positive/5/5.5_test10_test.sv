
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array query function $length on array

module test(input int dim,
              output integer length1, length2, length3); 

    parameter SIZE = 8;

    logic [0:-(SIZE  * 2) -1][-2:3] arr1 [-(SIZE *2):-(get_size(SIZE)) -1][4:-3];

    logic arr2 [-1: -(get_size(SIZE)) + 2][0:SIZE -1];

    int arr3 [-7 :-2];

    function integer get_size(integer in_size);
        get_size = in_size - 2;
    endfunction

    always @ (*)
    begin
        length1 = $length(arr1, dim);
        length2 = $length(arr2, dim);
        length3 = $length(arr3, dim);
    end

endmodule
