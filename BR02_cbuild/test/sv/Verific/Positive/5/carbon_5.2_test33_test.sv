// Carbon modified to make compatible with cbuild and testdriver.
//

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of reading from and writing to an element of an
// unpacked array.

module test(input wire a, output wire b) ;
assign b = a;
    bit mod_bit1 [7:0] = '{8{1'b1}} ;
    bit mod_bit2 [7:0] = '{8{1'b0}} ;

    always @(posedge a)
    begin
	    for( int i = 7 ; i >= 4 ; i--)
		    mod_bit2[i] = mod_bit1[7-i] ;

	    $display("Source = ") ;

	    for( int k = 7 ; k >= 0 ; k--)
		    $display("%b", mod_bit1[k]) ;

	    $display("\nCopied = ") ;

	    for( int k = 7 ; k >= 0 ; k--)
		    $display("%b", mod_bit2[k]) ;
    end
endmodule

