
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of packed array in an expression.

module test;
bit [7:0] mod_bit;
initial
begin
	mod_bit = '1;
	mod_bit = mod_bit + 3;
	$display("%b", mod_bit);
end
endmodule
