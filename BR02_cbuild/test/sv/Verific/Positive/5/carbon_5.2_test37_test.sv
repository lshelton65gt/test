// Carbon modified to make compatible with cbuild and testdriver.
//

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the unpacked array. Here unpacked 
// array is used as formal, actual, operand of binary operator .

`ifdef NOTUSED 
module test (array1, array2) ;
    output int array1 [1:0][3:0] ;
    output int array2 [1:0][3:0] ;
    int count1 = 0 ;
    int count2 = 7 ;
    int i, j ;
    initial begin
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array1[i][j] = count1 ++ ;
                    array2[i][j] = count2 -- ;
                end
    end
endmodule
`endif

module bench(input wire a, output wire b) ;
    assign b = a;
    int array3 [1:0][3:0] ;
    int array4 [1:0][3:0] ;
    logic out ;
    int temp [1:0][3:0] ;
`ifdef NOTUSED   
    test initialization (array3, array4) ;
`endif   
    task showArray ;
        input int array[1:0][3:0] ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $display("%d  ",  array[i][j]) ;
        end 
    endtask
    always @(posedge a)
    begin
       int count1;
       int count2;
       int i, j ;
       count1 = 0;
       count2 = 7;
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array3[i][j] = count1 ++ ;
                    array4[i][j] = count2 -- ;
                end
        #5 out = array3 == array4 ;
        if (out) begin 
            $display ("\n Both modules are equal") ;
            showArray(array3) ;
            $display ("   ") ;
            showArray(array4) ;
            $display ("\n\n") ;
        end
        else begin
            $display ("\n Both modules are not equal") ;
            showArray(array3) ;
            $display ("   ") ;
            showArray(array4) ;
            $display ("\n\n") ;
        end
        //-- Reading and writing the array, e.g., A = B
        #15
        temp = array3 ;
        $display ("\n both are equal") ;
        showArray(array3) ;
        $display ("   ") ;
        showArray(temp) ;
        $display ("\n\n") ;
    end
`ifdef NOTUSED
    initial begin
        //-- Reading and writing the array, e.g., A = B
        #15
        temp = array3 ;
        $display ("\n both are equal") ;
        showArray(array3) ;
        $display ("   ") ;
        showArray(temp) ;
        $display ("\n\n") ;
    end
`endif   
endmodule

