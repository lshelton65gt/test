
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of slicing on multidimensional packed array.

module test(input bit [15:0][3:0] bit_arr1,
               input bit [3:0][3:0][3:0] bit_arr2,
               output bit result);

    byte unsigned eq_byte1, eq_byte2;
    shortint unsigned eq_shortint1, eq_shortint2;
    int unsigned eq_int1, eq_int2;
    longint unsigned eq_longint1, eq_longint2;

    bit [15:14][3:0] eq_byte_arr1;
    bit [13:10][3:0] eq_shortint_arr1;
    bit [8:1][3:0] eq_int_arr1;


    bit [1:0][3:0] eq_byte_arr2;
    bit [3:0][3:0] eq_shortint_arr2;    
    bit [2:1][3:0][3:0] eq_int_arr2;
    
    bit res1, res2;

    always @ *
    begin
        eq_byte1 = bit_arr1[15:14];
        eq_byte_arr1 = bit_arr1[15:14];

        eq_byte2 = bit_arr2[2][1:0];
        eq_byte_arr2 = bit_arr2[2][1:0];

        eq_shortint1 = bit_arr1[13:10];
        eq_shortint_arr1 = bit_arr1[13:10];

        eq_shortint2 = bit_arr2[1];
        eq_shortint_arr2 = bit_arr2[1];

        eq_int1 = bit_arr1[8:1];
        eq_int_arr1 = bit_arr1[8:1];

        eq_int2 = bit_arr2[2:1];
        eq_int_arr2 = bit_arr2[2:1];

        eq_longint1 = bit_arr1;
        eq_longint2 = bit_arr2;

        res1 = 0;
        res2 = 0;

        if((eq_byte1 == eq_byte_arr1) &&
           (eq_shortint1 == eq_shortint_arr1) &&
           (eq_int1 == eq_int_arr1) &&
           (eq_longint1 == bit_arr1))
             res1 = 1;
        
        if((eq_byte2 == eq_byte_arr2) &&
           (eq_shortint2 == eq_shortint_arr2) &&
           (eq_int2 == eq_int_arr2) &&
           (eq_longint2 == bit_arr2))
             res2 = 1;

        result = (res1 && res2) ? 1 : 0;
    end

endmodule

