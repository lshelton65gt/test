
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Associative array literals use the {index:value} syntax
// with an optional default index. Like all other arrays, an associative array
// can be written one entry at a time, or the whole array contents can be assigned
// using an array literal. 

function void checking( int a[string]) ;
    string s ;
    a["e"] = 50 ;
    a["f"] = 60 ;
    a["g"] = 70 ;
    a["h"] = 80 ;

    $display("Printing elements of array a from function: ") ;
    if ( a.first( s ) )
       do
          $display( " %d\n", a[ s ] );
       while ( a.next( s ) );
endfunction

module test ;

   int b[string] = '{"a" : 10, "b" : 20, "c" : 30, "d" : 40, default: 22};
   string s ;

   initial begin
      $display("\n Number of element in array b: %d",b.num()) ;
      $display("Printing elements of array b, before call of function: ") ;
      if ( b.first( s ) )
         do
             $display( " %d\n", b[ s ] );
         while ( b.next( s ) );
      checking(b) ;
      $display("Printing elements of array b, after call of function: ") ;
      if ( b.first( s ) )
         do
             $display( " %d\n", b[ s ] );
         while ( b.next( s ) );
      $display("Print the default value: %d",b["fdgD"]) ;
   end

endmodule

