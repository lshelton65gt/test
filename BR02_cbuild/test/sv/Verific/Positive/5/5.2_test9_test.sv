
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests reading and writing a variable slice of the
// unpacked array e.g., A[x+:c] = B[y+:c]
// It also tests equality operations on the array or slice of the array, e.g. A==B, A[i:j] != B[i:j]

module test ;
    int array3 [1:0][3:0] ;
    int array4 [1:0][3:0] ;
    logic out ;
    int temp [1:0][3:0] ;
    task showArray ;
        input int array[1:0][3:0] ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $write("%d",  array[i][j]) ;
        end 
    endtask
    int start1, start2 ;
    mod initialization (array3, array4) ;
    initial begin
        //-- Reading and writing a slice of the array, e.g., A[i:j] = B[i:j] 
        #15
        start1 = 2 ; 
        temp[1][start1-:2] = array3[1][start1-:2] ;
        showArray(temp) ;
        $display ("\n ") ;
        showArray(array3) ;
        $display ("\n ") ;
        start1 = 2 ;
        start2 = 3 ;
        temp[1][start1-:1] = array4[0][start2-:1] ; 
        showArray(temp) ;
        $display ("\n ") ;
        showArray(array4) ;
        $display ("\n ") ;
    end
    initial begin 
        #15 ;
        if (array3[1][3:2] != array4[1][3:2] && array3[1][3:3] == array4[0][0:0])
            $display("\n Its OK") ;
        else 
            $display("\n Its not OK") ;
    end
endmodule

module mod (array1, array2) ;
    output int array1 [1:0][3:0] ;
    output int array2 [1:0][3:0] ;
    int count1 = 0 ;
    int count2 = 7 ;
    int i, j ;
    initial begin
        for (i = 1 ; i >= 0 ; i--)
            for(j = 3 ; j >= 0 ; j--)
                begin
                    array1[i][j] = count1 ++ ;
                    array2[i][j] = count2 -- ;
                end
    end
endmodule

