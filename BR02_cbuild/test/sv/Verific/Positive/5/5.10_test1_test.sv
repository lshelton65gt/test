
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test methods num, exists, delete for associative array.

module test ;

   typedef bit [4:1] unsign_index ;
   int a[unsign_index] ;

   initial begin 
       a[-1] = 3 ;
       a[20] = 4 ;
       a[2] = 2 ;
   end

   initial begin
      $display("\n Number of element in array a: %d",a.num()) ;
      if( a.exists(-1) == 1)
          a.delete(-1) ;
   end

endmodule

