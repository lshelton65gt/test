// Carbon modified. The input read address and write address were
// modified to be 'modulo the memory size', so we aren't always
// indexing outside of the memory range. Added $display statements
// for more visibility.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Declares arrays inside a for-generate loop. Vary the 
// width of the array by genvar.

module test #(parameter p = 5)(input wire clk, logic [7:0] rd_addr, wr_addr,
                output logic [3:0][1:0] out_byte[p-1:0]);
  
    generate
        genvar i;
        for(i=0; i < p; i = i+1)
        begin:b

`ifdef DONT_USE        
            // I don't know how to size (i+1) to get
            // the initializer correct. Instead, just make
            // the memory 32 bits wide.
            logic [7:0] mem [i+1 : 0] = '{ (i+2) { i+i}};
`endif                 
            logic [31:0] mem [i+1 : 0] = '{ (i+2) { i+i}};

            always @ (posedge clk)
              begin
                 for (int k = 0; k < i+2; k++)
                   $display("%d: mem[%x] = %x", i, k, mem[k]);
                 $display("%d: wr_addr = %x, rd_addr = %x", i, wr_addr%(i+2), rd_addr%(i+2));
                 $display("%d: mem[%x] = %x", i, rd_addr%(i+2), mem[rd_addr%(i+2)]);
                 mem[wr_addr%(i+2)] = mem[rd_addr%(i+2)] + 2;
              end
           assign out_byte[i] = mem[wr_addr%(i + 2)];
        end
    endgenerate

endmodule
