
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the rules of queue. An invalid index value
// (i.e., a 4-state expression with X's or Z's, or a value that lies outside 0...$) 
// shall cause a read operation (e = Q[n]) to return the 
// default initial value for the type of queue item.

`define a -5
`define b 200

module test (input init1, init2, output bit out);
    int Q[$] ;
    int Q1[$] ;
    int ele1[$] ;
    int ele2[$] ;
    int ele3[$] ;
    int ele4[$] ;
    int ele5[$] ;
    int ele6[$] ;

    always @ (init1)
    begin
        for (int i = 0; i < 10; i++)
            Q = {Q, i*10} ;
    end

    always @ (init2)
    begin
        ele1 = Q[`a:$] ;
        ele2 = Q[0:`b] ;
        ele3 = Q['bxxxx:$] ;
        ele4 = Q['bzzzz:$] ;
        ele5 = Q[0:'bzzzz] ;
        ele6 = Q[$:'bxxxx] ;

        if ((ele1 === ele2) && (ele2 === ele3) && (ele3 === ele4) && (ele4 === ele5) && (ele5 === ele6) && (ele6 === `a)) // Since 'a' stores the default value of type Q
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

