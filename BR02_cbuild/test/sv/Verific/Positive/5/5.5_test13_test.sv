
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array query function $right on packed array

module test(input int dim1, dim2,
                 output integer lsb1, lsb2);

    parameter SIZE = 8;
    logic [0:1][0:(SIZE-1)][-1:get_size(SIZE)] arr1;
    int arr2[-7:-2];

    function integer get_size(integer in_size);
        get_size = in_size - 2;
    endfunction

    always @(dim1, dim2)
    begin
        lsb1 = $right(arr1, dim1);
        lsb2 = $right(arr2, dim2);
    end

endmodule
