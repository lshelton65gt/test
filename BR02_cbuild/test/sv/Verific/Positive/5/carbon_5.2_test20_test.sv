// Carbon modified to be compatible with testdriver, and to eliminate
// initial block.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing signedness of part select of a packed array.

module test (input wire i, output wire o);

    assign o = i;

    logic signed [7:0] mod_logic;
    logic signed [7:0] mod_log;
    assign mod_logic = 8'b11110000;

    always @(posedge i)
      begin
            mod_log = mod_logic[7:4];
            if(mod_log < 5)
                    $display("Part select of a packed array is signed (should be unsigned)");
            else
                    $display("Part select of a packed array is unsigned (should be unsigned)");
      end

endmodule		
