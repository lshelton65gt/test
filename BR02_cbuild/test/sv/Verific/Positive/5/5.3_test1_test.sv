
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test that multidimensional arrays can be used as a whole.

module test(input bit [1:0][3:0] arr [1:0][3:0],
                output bit result) ;

    bit [1:0][3:0] copy_arr [1:0][3:0] ;

    bit signed [1:0][3:0] p_arr1, p_arr2, p_arr3, p_arr4, 
                   p_arr5, p_arr6, p_arr7, p_arr8 ;

    byte u_arr [1:0][3:0] ;

    longint p_arr_res, u_arr_res ;

    always @ *
    begin
        copy_arr = arr ;
        p_arr1 = copy_arr[1][3] ;
        p_arr2 = copy_arr[1][2] ;
        p_arr3 = copy_arr[1][1] ;
        p_arr4 = copy_arr[1][0] ;
        p_arr5 = copy_arr[0][3] ;
        p_arr6 = copy_arr[0][2] ;
        p_arr7 = copy_arr[0][1] ;
        p_arr8 = copy_arr[0][0] ;

        u_arr[1][3] = arr[1][3] ; 
        u_arr[1][2] = arr[1][2] ;
        u_arr[1][1] = arr[1][1] ;
        u_arr[1][0] = arr[1][0] ;
        u_arr[0][3] = arr[0][3] ;
        u_arr[0][2] = arr[0][2] ;
        u_arr[0][1] = arr[0][1] ;
        u_arr[0][0] = arr[0][0] ;        

        p_arr_res = p_arr1 + p_arr2 + p_arr3 + p_arr4 +
                    p_arr5 + p_arr6 + p_arr7 + p_arr8 ;

        u_arr_res = u_arr[1][3] + u_arr[1][2] + u_arr[1][1] +
                    u_arr[1][0] + u_arr[0][3] + u_arr[0][2] +
                    u_arr[0][1] + u_arr[0][0] ;

        if(p_arr_res == u_arr_res)
            result = 1 ;
        else
            result = 0 ;
    end

endmodule
