
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the use of replicate operators in 
// initializing of an unpacked array. 

module test (array1, array2) ;
    output reg array1 [3:0][1:0] ;
    output reg array2 [3:0][1:0] ;

    reg array3 [3:0][1:0] = '{'{2{1'b1}}, '{2{1'b1}}, '{2{1'b0}}, '{2{1'b0}}} ;
    reg array4 [3:0][1:0] = '{'{1'b1, 1'b1}, '{2{1'b1}}, '{2{1'b0}}, '{2{1'b0}}} ;

    initial
    begin
        array1 = array3 ;
        array2 = array4 ;
    end
endmodule

module bench ;
    reg array1 [3:0][1:0] ;
    reg array2 [3:0][1:0] ;
    
    logic out ;
    test instance_mod(array1, array2) ;
    int i , j ;
   initial
      begin
          #5 out = (array1 == array2 ) ;
          for(i = 3 ; i >= 0 ; i--)
              for (j = 1 ; j >= 0 ; j--)
                  $display("array1[%d][%d]=%b  array2[%d][%d]=%b", i, j, array1[i][j], i, j, array2[i][j]) ;
          if (out) $display("\n both arrays are equal\n\n") ;
          else $display("\n Not equal \n\n") ;
      end
endmodule

