
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test methods last, prev and delete for associative array.

module test ;

    typedef bit [4:1] unsign_index ;

    int a[unsign_index] ;
    int b[unsign_index] ;
    unsign_index uin;
    initial begin
       a[-1] = 3 ;
       a[20] = 4 ;
       a[2] = 2 ;
       a[1] = 10 ; // check whether error or overrides the value
    
       b[-1] = 5 ;
       b[1] = 6 ;
    end
    initial begin
       $display("\n Number of element in array a: %d",a.num()) ;
       if( a.exists(-1) == 1)
           a.delete(-1) ;
       else
           b.delete() ;
 
       $display("Printing elements of array a in reverse order: ") ;
 
       if ( a.last( uin ) )
          do 
              $display( " %d\n", a[ uin ] );
          while ( a.prev( uin ) );
 
    end
 
endmodule

