
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses variable slicing, for which starting position
// may be variable but the size of the slice must be constant.
// The constant used is the local parameter of the module.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    localparam p = w/2;

    logic [0:w-1][31:0] m;
    int i;

    always@(posedge clk)
    begin
        i = |{in1, in2};
        m = {16{in1, in2}};

        i = |m[i+:p/2];
        out1 = i;
        i = |m[i+:p];
        out2 = i;
    end

endmodule

