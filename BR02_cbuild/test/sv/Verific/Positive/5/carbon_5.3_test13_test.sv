// Carbon modified to make compatible with cbuild and testdriver.
//

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing 4 state index expression to a 4 state array.  

module test (input wire a, output wire b);
assign b = a;
logic [7:0] mod_log = '1;
integer index;
always @(posedge a)
  begin
	index = 1'bx;
	mod_log[7] = mod_log[index];  
	$display("8th bit is %b (should be x)", mod_log[7]);
	index = 1'bz;
	mod_log[index] = mod_log[0];
  end
endmodule
