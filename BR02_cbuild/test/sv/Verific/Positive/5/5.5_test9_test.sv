
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array query function $length on packed array.

module test(input int dim1, dim2,
                 output integer length1, length2);

    parameter SIZE = 8;
    logic [1:1][-(SIZE-1):4][-1:-(get_size(SIZE))] arr1;
    int arr2[0:1];

    function integer get_size;
        input integer in_size;
        get_size = in_size - 2;
    endfunction

    always @(dim1, dim2)
    begin
        length1 = $length(arr1, dim1);
        length2 = $length(arr2, dim2);
    end

endmodule
