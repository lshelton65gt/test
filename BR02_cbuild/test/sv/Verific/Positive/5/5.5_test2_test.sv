
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array query function $dimensions on array.

module test(arr1, arr2, arr3, dim1, dim2, dim3);

    output int dim1, dim2, dim3; 

    parameter SIZE = 8;

    input logic [0:-(SIZE  * 2) -1][-2:3] arr1 [-(SIZE *2):-(get_size(SIZE)) -1][4:-3];

    input logic arr2 [-1: -(get_size(SIZE)) + 2][0:SIZE -1];

    input int arr3 [-7 :-2];

    function integer get_size;
        input integer in_size;
        get_size = in_size - 2;
    endfunction

    always_comb
    begin
        dim1 = $dimensions(arr1);
        dim2 = $dimensions(arr2);
        dim3 = $dimensions(arr3);
    end

endmodule
