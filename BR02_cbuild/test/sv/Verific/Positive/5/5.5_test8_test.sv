
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array query function $left on array

module test(input int dim,
              output integer msb1, msb2, msb3); 

    parameter SIZE = 8;

    logic [(SIZE  * 2) -1:0][3:0] arr1 [get_size(SIZE)-1 :0][4:-3];

    logic arr2 [get_size(SIZE) + 2: -1][SIZE -1 :0];

    int arr3 [-7 :-1];

    function integer get_size (input integer in_size);
        //input integer in_size;
        get_size = in_size - 2;
    endfunction

    always @ (*)
    begin
        msb1 = $left(arr1, dim);
        msb2 = $left(arr2, dim);
        msb3 = $left(arr3, dim);
    end

endmodule
