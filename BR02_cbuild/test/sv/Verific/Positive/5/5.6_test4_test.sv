
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the Dynamic Array methods size(), along
// with the property of dynamic array initialization with another dynamic array.

module test (input int in, output out) ;

    parameter p1 = sum_of_N() ;

    initial
        $display("The value of parameter p1 = %1d", p1) ;

    function integer func() ;
        int arr[] ;
        int sum_of_nos = 0 ;
        int pqr[] ;
        int int_default ;

        pqr = new[10] ; // Creating pqr
        for (int i = 0; i < pqr.size; i++) // Assigning pqr
            pqr[i] = i + 1 ;

        arr = new[15](pqr) ; // Creating arr and initializing with the contents of pqr

        arr = new[arr.size() * 4](arr) ; // Creating a quadruple arr array
                                         // preserving the values of arr

        for (int i = 15; i < arr.size; i++)
            sum_of_nos += arr[i] ;
        if (sum_of_nos == int_default)
            return 11 ;
        else 
            return 10 ;
    endfunction

    function integer sum_of_N() ;
        int sum = 0 ;
        for (int i=1; i<11; i++)
           sum = sum + i ;
        return sum ;
    endfunction

    assign out = in + func() ;

endmodule

