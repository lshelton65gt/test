// Carbon modified to make compatible with cbuild and testdriver.
//

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of run time warning for out of range index.

module test (input wire a, output wire b);
assign b = a;
bit [7:0] mod_bit = '1;
int mod_in = 5;
always @(posedge a)
  begin
        mod_in = mod_in + 10;
  end
always @ (*)
  begin
	$display("%b", mod_bit[mod_in]);
  end
endmodule
