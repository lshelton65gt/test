
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the Dynamic Array method new().

module test(input in, output out) ;

    parameter p1 = prod_of_N() ;

    initial
        $display("The value of parameter p1 = %1d", p1) ;

    function integer func() ;
        integer arr[] ;
        arr = new[10] ;
        return arr.size ;
    endfunction

    function integer prod_of_N() ;
        int prod = 1 ;
        for (int i=1; i<11; i++)
            prod = prod + i ;
            return prod ;
    endfunction

    assign out = in + prod_of_N() ;

endmodule

