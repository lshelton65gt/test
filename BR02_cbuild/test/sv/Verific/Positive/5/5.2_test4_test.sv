
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test to check if vector expressions can be assigned
// to packed arrays.

module test(output bit u_result, s_result, input cal);

    logic [1:0][3:0] u_p_arr;
    logic signed [1:0][3:0] s_p_arr;

    logic [7:0] u_vec;
    logic signed [7:0] s_vec;

    always @(cal)
    begin
        u_p_arr = 4'b1010 * 3'sd2;
        s_p_arr = 4'b1010 * 3'sd2;
       
        u_vec = 4'b1010 * 3'sd2;
        s_vec = 4'b1010 * 3'sd2;

        if(u_p_arr == u_vec)
            u_result = 1;
        else
            u_result = 0;

        if(s_p_arr == s_vec)
            s_result = 1;
        else
            s_result = 0;
    end

endmodule
  
