
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Declare array inside case generate. Dimensions 
// of arrays are different in different paths.

module test(output bit result) ;

    int width1, width2, width3;

    child #(5) I1(width1);
    child #(1) I2(width2);
    child #(3) I3(width3);

    always @(width1, width2, width3)
        if((width1 == 9) && (width2 == 15) &&
           (width3 == 32))
            result = 1;
        else
            result = 0;

endmodule


module child(output int bit_width);

    parameter p = 8;

    generate
        case(p)
         1 : begin 
                 logic [2:0][-1:3] arr [0:2][5:0][1:0];
                 assign bit_width = $bits(arr[0][2][p]);
             end
         3 : begin 
                 logic [-1:0][31:0] arr [-1:0][0:4];
                 assign bit_width = $bits(arr[0][2][0]);
             end
         default :begin 
                 logic [7:0][3:0][0:8] arr [p -1 :0];
                 assign bit_width = $bits(arr[0][2][p > 3 ? 3 : 0]);
             end
        endcase
    endgenerate

endmodule
