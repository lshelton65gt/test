
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing 4 state index expression to a 2 state array.

module test(input in, output out) ;

    bit [7:0] mod_bit = '1 ;
    int index ;
    initial
    begin
        index = 1'bx ;
        mod_bit[7] = mod_bit[index] ;  
        index = 1'bz ;
        mod_bit[index] = mod_bit[0] ;
        $display("mod_bit = %d", mod_bit) ;
    end

    assign out = in ;

endmodule
