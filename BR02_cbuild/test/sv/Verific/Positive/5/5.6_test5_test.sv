
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests Dynamic Array methods size(),
// The size() method returns zero if the array has not been created.

module test(input in, output out) ;

    parameter p1 = sum_of_N() ;

    initial
        $display("The value of parameter p1 = %1d", p1) ;

    function integer func() ;
        int arr[] ;
        
        if (arr.size() == 0)
            return 111 ;
        else 
            return 110 ;
    endfunction

    function integer sum_of_N() ;
        int sum = 0 ;
        for (int i=1; i<11; i++)
           sum = sum + i ;
        return sum ;
    endfunction

    assign out = in + func() ;

endmodule

