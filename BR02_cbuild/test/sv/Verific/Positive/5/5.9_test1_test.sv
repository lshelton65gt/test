
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing associative array assignment. Associative arrays
// can be assigned only to another associative array of a compatible type and with
// the same index type. 

module test ;

    typedef bit [4:1] unsign_index ;
 
    int a[unsign_index] ;
    int b[unsign_index] ;
    unsign_index uin;
    initial begin
       a[-1] = 3 ;
       a[8] = 4 ;
       a[2] = 2 ;
    
       b[-1] = 5 ;
       b[3] = 6 ;
    end
    initial begin
       $display("\n Number of element in array a: %d",a.num()) ;
 
       $display("Printing elements of array b: ") ;
       if ( b.first( uin ) )
          do 
              $display( " %d\n", b[ uin ] );
          while ( b.next( uin ) );
 
       b = a ;
 
       $display("Printing elements of array b: ") ;
       if ( b.first( uin ) )
          do 
              $display( " %d\n", b[ uin ] );
          while ( b.next( uin ) );
 
    end
 
endmodule

