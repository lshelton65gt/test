
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing part select of a packed array.

module test;
reg [63:0] mod_int;
reg [7:0] mod_byte;
initial
  begin
	mod_int = '1;
	mod_byte = mod_int[50:43];
	$display("Copied version (Should be 11111111) = %b", mod_byte);
  end
endmodule
