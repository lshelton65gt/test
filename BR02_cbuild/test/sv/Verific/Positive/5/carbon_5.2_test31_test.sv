// Carbon modified to make compatible with cbuild and testdriver.
// VCS and MTI get the wrong answer! But cbuild seems to be correct.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing reading from and writing to a variable slice of an
// unpacked array.

module test (input wire a, output wire b);
assign b = a;
byte mod_char[10:0] = {"H", "e", "l", "l", "o", " ", "W", "o", "r", "l", "d"}; //{$random, $random};
byte mod_byte[10:0];
int i=7;
int j=0;
always @(posedge a)
  begin
        i = 7;
        j = 0;
	while(i>0)
	  begin
		mod_byte[i -: 2] = mod_char[j +: 2];
		i = i-2;
		j = j+2;
	  end 
	for(int k = 7; k >= 0; k--)
          begin
		$display("%d\t%d\n", mod_byte[k], mod_char[k]);
          end
  end
endmodule
