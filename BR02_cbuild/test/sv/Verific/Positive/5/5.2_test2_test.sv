
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of variable part select on packed and unpacked arrays.

module test(input int  num_arr [7:0],
             output int avg);

    int u_arr1 [0:3], u_arr2 [0:3];

    bit [15:0][15:0] p_arr;
    int sum;
    
    always @ *
    begin
        u_arr1[0+:2] = num_arr[7-:2];
        u_arr1[2+:2] = num_arr[5-:2];
        u_arr2[0+:2] = num_arr[3-:2];
        u_arr2[2+:2] = num_arr[1-:2];
    
        p_arr[14+:2] = u_arr2[0];
        p_arr[12+:2] = u_arr2[1];
        p_arr[10+:2] = u_arr2[2];
        p_arr[8+:2]  = u_arr2[3];
        p_arr[6+:2]  = u_arr1[0];
        p_arr[4+:2]  = u_arr1[1];
        p_arr[2+:2]  = u_arr1[2];
        p_arr[0+:2]  = u_arr1[3];

        sum = p_arr[15-:2] + p_arr[13-:2] + p_arr[11-:2] +
              p_arr[9-:2] + p_arr[7-:2] + p_arr[5-:2] +
              p_arr[3-:2] + p_arr[1-:2];       

        avg = sum / 8 ; 
    end

endmodule
