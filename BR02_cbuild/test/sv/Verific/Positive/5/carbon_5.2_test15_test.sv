// Carbon modified to make compatible with testdriver, and
// to eliminate initial block.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test on assignments to packed array, having width greater
// than the expression on RHS.

module test(input wire i, output wire o);

    assign o = i;
    
    bit [4:0][3:0] bit1;
    shortint unsigned a, b;

    assign a = 5;
    assign b = 10;
    assign bit1 = a - b;

    always @(posedge i)
    begin
        $display(" %b = %b - %b", bit1, a, b);
    end

endmodule


