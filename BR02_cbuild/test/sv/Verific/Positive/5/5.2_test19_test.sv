
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests use of structure literal to initialize
// unpacked array of structure.

typedef struct {
    string name ;
    int roll ;
    string address ;
} student ;

module test ;

   student s1[2][2] ='{ '{'{"rups",41,"salt lake"},'{"himani",23,"salt lake city"}} , '{'{"erty",34,"dfg"},'{"rob",56,"california"}} };
   
   initial begin
      for(int i=0; i<2; i++)
         for(int j=0; j<2; j++)
             $display("%s  %d  %s", s1[i][j].name, s1[i][j].roll, s1[i][j].address) ;
   end

endmodule 
   
