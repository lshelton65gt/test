
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): When assigning to an unpacked array, the source and target must be
// arrays with the same number of unpacked dimensions, and the length
// of each dimension must be the same. Assignment to an unpacked array
// is carried out by assigning each element of the source unpacked array
// to the corresponding element of the target unpacked array. This design
// defines two modules. One module defines unpacked arrays with packed
// dimension depending on module parameters while  the other one instantiates
// this module twice with different values of the parameters so that both
// legal and illegal assignments can be checked.

module test#(parameter d1_1 = 2, d2_1 = 6, d1_2 = 9, d2_2 = 3, d3 = 4)
            (input clk,
             input [7:0] in1, in2,
             output reg [7:0] out1, out2);

    logic [0:d1_2-1][d1_1-1:0] arr1 [d3-1:0];
    logic [0:d2_2-1][d2_1-1:0] arr2 [d3-1:0];

    always@(posedge clk)
    begin
        for(int i=0; i<4; i++)
        begin
            arr1[0] = { 2 { in1, in2 } };
            arr1[1] = { 2 { in2, in1 } };
            arr1[2] = { 2 { in1, in1 } };
            arr1[3] = { 2 { in2, in2 } };
        end

        arr2 = arr1;    // assignment will be illegal if d1_1 * d1_2 !=  d2_1 * d2_2

        out1 = arr2[0] | arr2[2];
        out2 = arr2[1] + arr2[3];
    end

endmodule

