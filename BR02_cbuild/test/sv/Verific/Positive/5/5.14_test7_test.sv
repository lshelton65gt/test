
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing deletion of the last (rightmost) item of a queue.

module test (input init1, init2, output bit out);
    int Q[$] ;
    int ele ;

    always @ (init1)
    begin
        for (int i = 0; i < 10; i++)
            Q = {Q, i*10} ;
    end

    always @ (init2)
    begin
        ele = Q[$-1] ; // holding the last element of Q.

        Q = Q[0:$-1] ; // deleting the last element of Q.

        if (Q[$] == ele)
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

