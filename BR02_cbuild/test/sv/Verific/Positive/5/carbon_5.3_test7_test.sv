// Carbon modified to make compatible with cbuild and testdriver.
//

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test how the dimension of a multidimensional array varies.

module test (input wire i, output wire o);
    assign o = i;
    bit [3:0][3:0] b = { { 1'b0, 1'b0, 1'b0, 1'b1 },
                         { 1'b0, 1'b0, 1'b1, 1'b0 },
                         { 1'b0, 1'b1, 1'b0, 1'b0 },
                         { 1'b1, 1'b0, 1'b0, 1'b0 } };
    always @(posedge i)
    begin
        if ((b[0] == 4'b1000) &&
            (b[1] == 4'b0100) &&
            (b[2] == 4'b0010) &&
            (b[3] == 4'b0001))
            $display("multi-dimensional array is varying properly");
    else
            $display("multi-dimensional array is not varying properly");
    end

endmodule
