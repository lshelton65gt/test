
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of part select on packed and unpacked arrays.

module test(input int num_arr [7:0],
             output int avg) ;

    int u_arr1 [3:0], u_arr2 [3:0] ;

    bit [15:0][7:0] p_arr ;
    int sum ;
    
    always @ *
    begin
        u_arr1[3:2]  = num_arr[7:6] ;
        u_arr1[1:0]  = num_arr[5:4] ;
        u_arr2[3:2]  = num_arr[3:2] ;
        u_arr2[1:0]  = num_arr[1:0] ;
    
        p_arr[15:14] = u_arr2[0] ;
        p_arr[13:12] = u_arr2[1] ;
        p_arr[11:10] = u_arr2[2] ;
        p_arr[9:8]   = u_arr2[3] ;
        p_arr[7:6]   = u_arr1[0] ;
        p_arr[5:4]   = u_arr1[1] ;
        p_arr[3:2]   = u_arr1[2] ;
        p_arr[1:0]   = u_arr1[3] ;

        sum = p_arr[15:14] + p_arr[13:12] + p_arr[11:10] +
              p_arr[9:8] + p_arr[7:6] + p_arr[5:4] +
              p_arr[3:2] + p_arr[1:0] ;       

        avg = sum / 8 ; 
    end

endmodule

