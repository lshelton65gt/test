
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Declares arrays inside a for-generate loop. Vary the 
// width of the array by genvar.

module test #(parameter p = 5)(input int rd_addr, wr_addr,
                output [3:0][1:0] out_byte[p-1:0]);
  
    generate
        genvar i;
        for(i=0; i < p; i = i+1)
        begin:b

            logic [7:0] mem [i+1 : 0] = '{ (i+2) { i+i}};

            always @ (rd_addr, wr_addr)
            begin
               mem[wr_addr] = mem[rd_addr] + 2;
            end            
            assign  out_byte[i] = mem[wr_addr] + 1;
        end
    endgenerate

endmodule
