
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the rule of queue i.e insertion in the
// middle of a queue.

module test (input clk, init, bit [3:0] pos, int num1, int num2, bit [4:0] qout, output int out) ;
    int Q[$] ;

    // queue initialization
    always @ (init)
    begin
        for (int i = 0; i < 16; i++)
            Q = {Q, i*10} ;
    end
    
    always @ (posedge clk)
    begin
        Q = { Q[0:pos-1], num1, Q[pos:$] } ;  // insert num1 at position pos
        Q = { Q[0:pos], num2, Q[pos+1:$] } ;  // insert num2 after position pos
    end

    // For each increment of qout in the bench we can have the 
    // values in Q position Q[qout] which are actually passed
    // to the bench for printing.
    always @ (qout) 
        out = Q[qout] ;
endmodule

