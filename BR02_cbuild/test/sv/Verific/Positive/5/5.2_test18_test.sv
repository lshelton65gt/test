
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following operations can be performed on all arrays, packed
// or unpacked. The rules that A and B as arrays followed are --
// A=B, A[i:j] = B[i:j], A[i] = B[i], A == B, A[i:j] != B[i:j],A[x+:c] = B[y+:c]
// A and B have to be of the same shape and type.

module test ;
   int a[4] ;
   int b[3:0] ;

   initial begin
       for (int i=0; i<4; i=i+1)
       begin
             b[i] = i ;
             a[i] = b[i] ;
            $display("%d %d", b[i] , a[i]) ;
       end

       a[1:3] = b[2:0] ;
       if( a[0:2] != b[2:0] )
           $display("It works, Both are different") ;
       a[0 +: 2] = b[1 +: 2] ;
   end

   always @(*) 
   begin
       a = b ;
       if( a ==b )
           $display("Both arrys are same") ;
   end
endmodule

