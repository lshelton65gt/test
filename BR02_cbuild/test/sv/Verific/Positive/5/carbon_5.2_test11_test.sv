// Carbon modified to make compatible with cbuild and testdriver.
//

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following operations can be performed on packed 
// arrays, but not on unpacked arrays. The examples provided with these rules
// assume that A is an array.
// -- Assignment from an integer, e.g., A = 8'b11111111 ;

`ifdef NOTUSED
module test (in, array1, array2) ;

    input int in ;
    output wire [1:0][3:0] array1 ;
    output wire [1:0][3:0] array2 ;
    //-- Assignment from an integer, e.g., A = 8'b11111111
    assign 
    assign array2 = 8'b00001111 + in ;

endmodule
`endif

module bench (input wire a, output wire b);
    assign b = a;
    reg [1:0][3:0] array3 ;
    reg [1:0][3:0] array4 ;
    task showArray ;
        input reg [1:0][3:0] array ;
        begin
            for (int i = 1 ; i >= 0 ; i--)
                for (int j = 3 ; j >= 0 ; j--)
                    $write("%d",  array[i][j]) ;
        end 
    endtask
`ifdef NOTUSED
    test initialization (array3, array4) ;
`endif
    initial begin
        int in;
        in = 7;
        array3 = 8'b11110000 + in ;
        array4 = 8'b00001111 + in ;
        #15
        showArray (array3) ;
        showArray (array4) ;
    end

endmodule

