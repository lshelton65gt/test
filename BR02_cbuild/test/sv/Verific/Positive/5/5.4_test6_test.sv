
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of slicing on unpacked array.

module test(input byte arr1 [63:0], output longint sum) ;

    byte arr2 [3:0] ;
    byte arr3 [-3:0] ; 

    always @ *
    begin
        arr2 = arr1[4:1] ;
        arr3 = arr1[63:60] ;

        sum = arr2[3] + arr2[2] + arr2[1] + arr2[0] +
               arr3[-3] + arr3[-2] + arr3[-1] + arr3[0] ;

     end

endmodule
