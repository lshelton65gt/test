// Carbon modified to make compatible with cbuild and testdriver.
//

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If an index expression is out of the address bounds or if 
// any bit in the address is X or Z, then the index shall be invalid. The result 
// of reading from an array with an invalid index shall return the default 
// uninitialized value for the array element type. Writing to an array with 
// an invalid index shall perform no operation. This design checks this.

module test(input in, output out) ;

    int rd_idx ;
    int wr_idx ;
    bit [3:0][7:0] arr [2:0] = '{34, 56, 90} ;

    always @(posedge in)
    begin
        rd_idx = 3 ;
        wr_idx = 0 ;
        arr[wr_idx] = arr[rd_idx] + 2 ;

        $display("arr[%1d] = %1d", wr_idx, arr[wr_idx]) ;
    end

    assign out = in ;

endmodule

