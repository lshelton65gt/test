
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of reading from and writing to a slice of an unpacked array.

module test;

    byte mod_str1[10:0] = {"H", "e", "l", "l", "o", " ",  "W", "o", "r", "l", "d"};
    byte mod_str2[10:0] ;

    initial
    begin
        mod_str2 [4:0] = mod_str1 [4:0];
        $display("Copied text = ");
        for(int i = 10; i >= 0; i--)
            $display("%c", mod_str2[i]);
    end

endmodule
