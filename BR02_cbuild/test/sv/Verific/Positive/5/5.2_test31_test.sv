
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing reading from and writing to a variable slice of an
// unpacked array.

module test;
byte mod_char[10:0] = {"H", "e", "l", "l", "o", " ", "W", "o", "r", "l", "d"}; //{$random, $random};
byte mod_byte[10:0];
int i=7;
int j=0;
initial
  begin
	while(i>0)
	  begin
		mod_byte[i -: 2] = mod_char[j +: 2];
		i = i-2;
		j = j+2;
	  end 
	for(int k = 7; k >= 0; k--)
		$display("%d\t%d\n", mod_byte[k], mod_char[k]);
  end
endmodule
