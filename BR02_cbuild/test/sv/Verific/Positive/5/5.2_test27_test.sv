
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of reading from and writing to an unpacked array.

module test(input in, output out) ;

    bit mod_bit1 [7:0] = '{8{1'b1}} ;
    bit mod_bit2 [7:0] ;

    initial
    begin
        mod_bit2 = mod_bit1 ;
        if(mod_bit2 == mod_bit1)
            $display("Testing of reading from and writing to an unpacked array is a success") ;
    end

    assign out = in ;

endmodule
