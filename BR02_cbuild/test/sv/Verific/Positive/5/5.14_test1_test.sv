
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If the index of the queue lies outside its  limits
// then a write operation should be ignored and appropriate warning issued.

module test (input clk, bit [7:0] n, output bit out);
    int Q[$:20] ; 
    always @ (posedge clk)
    begin
        for (int i = 0; i < 25; i++) // Generate warning due to out of bound writing
            Q = {Q, i*n} ;

        if (Q[$] == 6 ) out = 1'b1 ;
        else out = 1'b0 ;
    end
endmodule

