
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of array query function $left on packed array.

module test(input int dim1, dim2,
                 output integer msb1, msb2);

    parameter SIZE = 8;
    logic [1:0][(SIZE-1) :0][get_size(SIZE) : -1] arr1;
    int arr2[0:1];

    function integer get_size;
        input integer in_size;
        get_size = in_size - 2;
    endfunction

    always @(dim1, dim2)
    begin
        msb1 = $left(arr1, dim1);
        msb2 = $left(arr2, dim2);
    end

endmodule
 
