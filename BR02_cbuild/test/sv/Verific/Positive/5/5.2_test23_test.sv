
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing packed arrays with logic and wire data types.

module test;

    parameter width = 8;
    wire [width-1:0] mod_wire;
    logic [width-1:0] mod_log;
    assign mod_wire = 8'b00001111;
    assign mod_log = 8'b1;

    initial
    begin
        if(mod_wire == 15 && mod_log == 1)
            $display("Testing packed array with logic and wire data type is successfull");
    end

endmodule
