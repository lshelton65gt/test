
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A 'program' block can contain one or more 'initial' blocks.

interface in1 ;
    string name1 ;
    bit[3:0] flag ;
    int data1 ;    
endinterface

program p1(interface i1) ;
    initial
        i1.name1 <= "its working" ;
    initial
        i1.flag <= 'b0011 ;
    initial
        i1.data1 <= 12 ;
endprogram

module test() ;
    in1 A() ;
    p1 check(A) ;

    always @(A.name1, A.flag, A.data1)
        $display(" %s, %b, %d", A.name1, A.flag, A.data1) ;
endmodule
 
