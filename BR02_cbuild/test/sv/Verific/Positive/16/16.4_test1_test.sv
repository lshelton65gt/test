
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Programs are allowed to call tasks or functions defined in other
// programs or within design modules.

program call_task ;
    int a =10;
    int b = 5 ;
    int c;
   
    initial begin
        test.T(a,b,c) ;
        $display("Value of C: %d",c);
    end
    
endprogram

module test(input int a, b, output int c) ;

   task T (input int a, input int b, output int c);
       c = a + b ;
   endtask

endmodule

