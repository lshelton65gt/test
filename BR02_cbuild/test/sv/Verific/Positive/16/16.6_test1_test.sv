
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Each program can be finished by calling the $exit system task. 
// When all programs exit, the simulation ends.

program test (input reg clk, a, b, output reg o);
    
    task T( input clk, input a, output o) ;
        o = ~clk & a;
    endtask

    initial begin
        $display(" variable are: %b, %b, %b", clk, a, o) ;
        $exit() ;
        T(clk, a, o) ;
    end

endprogram

module bench ;
     reg clk, o, a, b;

     initial begin
         clk = 0 ;
     end

     initial
         #15 $finish ;
     always
         #5 clk = ~clk ;
     always @(o)
         $display(" value changed" ) ;
     test I(clk,a,b,o) ;

endmodule

