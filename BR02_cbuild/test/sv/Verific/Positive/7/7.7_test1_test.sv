
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog provides a mechanism for initializing an
// object at the time the object is created. This is done by executing the 'new'
// function associated with the class.

typedef enum {IDLE, BUSY, START, STOP} cmd ;

class Packet ;

    integer command ;
    function new ;
        command = IDLE ;
    endfunction

endclass

module test ;
    Packet p = new ;

    initial
    begin
        case (p.command)
        (IDLE)  : $display("idle")  ;
        (BUSY)  : $display("busy")  ;
        (START) : $display("start") ;
        (STOP)  : $display("stop")  ;
        endcase
    end

endmodule

