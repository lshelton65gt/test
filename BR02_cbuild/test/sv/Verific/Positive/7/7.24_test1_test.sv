
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Sometimes a class variable needs to be declared before the
// class itself has been declared. This can be done by using 'typedef' forward declaration.

typedef class Node ;  // Forward declaration of class Node

class List ;
    Node first ;
    Node last ;

    function new() ;
       $display("Within constructor of List") ;
       first = null ;
       last = null ;
    endfunction
    
    function void insert(int num) ;
        Node n = new(num) ;
        if(first == null)
            first = n ;
        else
            last.next = n ;
        last = n ;
    endfunction

endclass

class Node ;
    int data ;
    Node next ;
   
    function new(int num) ;
       $display("Within constructor of Node") ;
        data = num ;
    endfunction

endclass

module test(input in, output out) ;

    List l = new ;
    initial
    begin
        l.insert(5) ;
        l.insert(6) ;
        l.insert(7) ;
        l.insert(8) ;
        l.insert(9) ;
        l.insert(10) ;
    end
    assign out = in ;

endmodule

