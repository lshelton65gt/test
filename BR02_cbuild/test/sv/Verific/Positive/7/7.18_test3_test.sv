
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Global constant properties are those that include an initial value 
// as part of their declaration. They cannot be assigned a value anywhere 
// other than in the declaration.
// Instance constants do not include an initial value in their declarations.
// The assignment can be done only once, in the corresponding class constructor.

class GlobalConst ;
    const string global_string = "global" ; // global constant
    int data ;
    function new() ;
        data = 15 ;
    endfunction        
endclass

class InstanceConst ;
    const string inst_string ; // instance constant
    int num ;
    function new() ;
        inst_string = "instance " ;
        num = 10 ;
    endfunction
endclass

module test(input in, output out) ;

    GlobalConst g1 = new() ;
    GlobalConst g2 = new() ;

    InstanceConst i1 = new() ;
    InstanceConst i2 = new() ;
        
    initial begin
        $display("%s, %d", g1.global_string, g1.data) ;
        $display("%s, %d", g2.global_string, g2.data) ;
        $display("%s, %d", i1.inst_string, i1.num) ;
        $display("%s, %d", i2.inst_string, i2.num) ;
    end

    assign out = ~in ;

endmodule 

