
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation. 

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests shallow copy of class objects using
// 'new'. In shallow copy objects are not copied, only their handles are copied.

class Deep ;

   int j = 5; 
   string n ;

endclass 

class Shallow ;

   int i = 1;
   int data = 2;
   Deep a = new;

endclass

module test ;

    Shallow s1 = new ;
    Shallow s2 = new s1 ;

    initial
    begin
        s2.i = 12 ;
        s2.data = 23 ;

        s2.a.j = 15 ;
        s2.a.n = "hello" ;

        $display("Changes are present in the values" ) ;
        $display("s1.i: %d, s1.data: %d, s1.a.j: %d, s1.a.n: %s",s1.i, s1.data, s1.a.j, s1.a.n) ;
        $display("s2.i: %d, s2.data: %d, s2.a.j: %d, s2.a.n: %s",s2.i, s2.data, s2.a.j, s2.a.n) ;
    end

endmodule

