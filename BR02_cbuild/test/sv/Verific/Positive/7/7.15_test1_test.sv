
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): It is always legal to assign a subclass variable to a variable
// of a class higher in the inheritance tree. The following design checks this.

class Packet ;    //parent class
    integer value ;
    function integer delay() ;
        $display("super class value =%d", value) ;
        delay = value * value ;
    endfunction
endclass

class LinkedPacket extends Packet ;    //derived class
    integer value ;
    function integer delay() ;
        $display("sub-class value =%d", value) ;
        delay = super.delay() + value * super.value ;
    endfunction
endclass

module test (input in, output out) ;

    Packet pkt1 ;
    LinkedPacket lk_pkt1 = new ;

    initial
    begin
        // $cast( singular dest_handle, singular source_handle ) ;      
	int is_legal_class = $cast( pkt1, lk_pkt1 ) ; // Assigns a subclass variable to a superclass handle
        pkt1.delay() ;     // It should call delay() of LinkedPacket class.
        lk_pkt1.delay() ;  // It should call delay of Packet class.
        $display("is_legal_class = %d", is_legal_class) ;
    end
endmodule

