
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a class and use 'new' operator to
// instantiate the class array inside generate block.

class Student ;
    string name ; 
    int roll ;

    function new(string n, int r) ;
        name = n ;
        roll = r ;
    endfunction

    function void print() ;
        $display(" %s   %d", name, roll) ;
    endfunction
endclass

module test ;
    bot #(5,2) b1() ;
endmodule

module bot #(parameter width = 10, size = 5) ;
    Student s[width] ;
    genvar i ;
    generate for(i = 0 ; i < width; i++) begin : b
       initial begin 
         s[i] = new("default", size*i) ;
         s[i].print() ;
       end
    end
    endgenerate
endmodule 

