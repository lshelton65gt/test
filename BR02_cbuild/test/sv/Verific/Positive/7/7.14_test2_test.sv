
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing SystemVerilog specific 'this' and 'super' keywords.
// 'this' refers to the current class object and 'super' refers to the base class
// part of the current class object. Testing this in single inheritance.

class B ;
    int n ;

    function new(int i) ;
        this.n = i ;
    endfunction
endclass

class A extends B ;
    int n ;

    function new(int i, j) ;
        super.new(i) ;
        this.n = i ;
        super.n = j ;
    endfunction

    function int fn(int i) ;
        return this.n + super.n + i ;
    endfunction
endclass

module test (i, o) ;

    input int i ;
    output int o ;

    A a1 = new (1, 2) ;

    initial
    begin: dd
        $display ("fn = %d", a1.fn(i)) ;
    end

endmodule

