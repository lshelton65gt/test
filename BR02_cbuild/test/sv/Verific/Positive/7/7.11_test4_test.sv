
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests 'deep' copy by defining function 'copy' so
// that all member objects are copied.

class A ;

    int a ;
    int obj_id ; // id takes up different value for different objects
    static int id = 0 ;
    function new (int x = 0) ;
        id = id + 1 ;
        a = x ;
        obj_id = id ;
    endfunction

    function void copy (A yy) ;
        this.a = yy.a ;
        // obj_id = yy.obj_id ; // ids should be different and hence should not be copied
    endfunction

    function void SetA(int value) ;
        this.a = value ;
    endfunction

endclass

class B ;

    int b ;
    A aa = new ;
    function new (int y = 0) ;
        b = y ;
    endfunction
    function void copy(B xx) ;
        this.b = xx.b ;
        aa.copy(xx.aa) ;
    endfunction
    function void SetB(int value) ;
        this.b = value ;
    endfunction
    function void SetAB(int value) ;
        this.b = value ;
        this.aa.a = value/2 ;
    endfunction

endclass

module test (input in, output out) ;

    B pp = new ;
    B qq = new ;
    initial
    begin
        pp.SetAB(10) ;
        qq.SetAB(2000) ;
        $display("Before copy") ;
        $display("pp.b =%d, pp.aa.a =%d, qq.b =%d, qq.aa.a =%d", pp.b, pp.aa.a, qq.b, qq.aa.a) ;
        $display("pp obj_id = %d, qq obj_id = %d", pp.aa.obj_id, qq.aa.obj_id) ;
        pp.copy(qq) ; // Copying qq to pp
        $display("After copy(deep)") ;
        $display("pp.b =%d, pp.aa.a =%d, qq.b =%d, qq.aa.a =%d", pp.b, pp.aa.a, qq.b, qq.aa.a) ;
        $display("pp obj_id = %d, qq obj_id = %d", pp.aa.obj_id, qq.aa.obj_id) ;
    end

    assign out = in ;

endmodule

