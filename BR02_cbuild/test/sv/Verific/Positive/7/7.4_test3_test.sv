
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses 'new' inside generate block to allocate class object

class P #(int size = 8) ;
    int a [size] ;
    const int count = size ;

    function new() ;
        for( int i=0 ;i<count; i++)
            a[i] = i ;
    endfunction

    function void print() ;
        $display("parameter value = %d", count) ;
        for( int i=0 ;i<count; i++)
            $display("a[%d] = %d", i, a[i]) ;
    endfunction
endclass

module test ;

    bot #(.width(16)) b1() ;

endmodule

module bot #(parameter width = 10) ;

    P #(width) p1 [width] ;
    genvar i ;
    generate
        for(i = 0 ; i < width; i++)
        begin: for_gen_i
            initial
            begin
                p1[i] = new() ;
                p1[i].print() ;
            end
        end
    endgenerate

endmodule 

