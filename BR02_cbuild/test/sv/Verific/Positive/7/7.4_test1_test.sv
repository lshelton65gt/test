
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a class with constructor. The default value
// of a parameter is determined by using '$bits' function with class object as argument.

module test ;

    class Check ;
        int c ;
        int d ;
     
        function new (int c, int d) ;
            this.c = c ; 
            this.d = d ;
        endfunction
    endclass

    Check ch = new(3,5) ;
    parameter int p = $bits(ch) ;
    bot #(p) b1() ;

endmodule

module bot #(parameter int n = 2) ;

    initial $display(n) ;

endmodule

