
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The methods of an object can be used by qualifying
// method names with an instance name.

class Abc ;
    
    local int _status ;

    function new (int a, int b) ;
        _status = 0 ;
    endfunction

    function int get_current_status ;
        return _status ;
    endfunction 

    function void set_current_status(int s) ;
        _status = s ;
    endfunction 

endclass

module test(clk, count) ;

    input logic clk ;
    input int count ;
    int count1 ;
    Abc aa = new(100, 200) ;

    always @(count)
    begin
        count1 = count+1 ;
        if (aa.get_current_status==0) 
        begin
            aa.set_current_status(count1) ;
            $display("Test: clk=%b status=%d", clk, aa.get_current_status) ;
        end
    end

endmodule

