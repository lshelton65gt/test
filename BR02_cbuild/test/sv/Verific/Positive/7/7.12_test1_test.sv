
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a class named 'Student' which is 
// inherited by a 'Batch' class.

class Student ;
//////////Properties///////
    string name ;
    int roll ;
    string address ;
    static integer num_of_student = 0 ;
//////////Methods//////////
    function new() ;
        name = "" ;
        address = "" ;
        roll = 12 ;
        num_of_student ++ ;
    endfunction

    function void setdata(string n, int r, string a) ;
        name = n ;
        roll = r ;
        address = a ;
    endfunction

    function string getname() ;
        getname = name ;
    endfunction

    function string getaddress() ;
        getaddress = address ;
    endfunction

    function int getroll() ;
        getroll = roll ;
    endfunction
endclass

class Batch extends Student ;
    int year_of_joining ;
    function new(int yr);
        super.new() ;
        year_of_joining = yr ;
    endfunction
   
    function int getyear() ;
        getyear = year_of_joining ;
    endfunction

endclass

module test ;
        Batch b1 = new(2000) ;
    initial begin
        b1.setdata("qwe", 23,"qwq") ;
        $display("Details of Batch class: %s, %d, %s, %d", b1.getname(), b1.getroll(), b1.getaddress(), b1.getyear()) ;
     end
endmodule

