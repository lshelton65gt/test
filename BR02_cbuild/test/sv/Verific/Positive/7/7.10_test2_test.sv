
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design qualifies a class member with 'this' keyword
// to distinguish this member from a local variable declared within a function.

class Abc ;
    int x ;
    function new (int x) ;
        this.x = x ; // use of 'this' to explicitly point to class member 'x'
    endfunction

    function int getVal ;
        return this.x ;
    endfunction
endclass

module test (clk) ;

    input clk ;
    Abc aa = new (10) ;
    initial $display ("x =%d", aa.getVal()) ;

endmodule

