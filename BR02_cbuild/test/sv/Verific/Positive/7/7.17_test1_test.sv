
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
       
// TESTING FEATURE(S): 'local' keyword can be used in class member declarations
// to make it available only to methods inside the class. This design tests this.

class Packet ;    //parent class
    local integer value ;  // local property
    function new (int x = 0) ;
        value = x ;
    endfunction
    function integer delay() ;
        delay = value * value ; // Use of local property
        $display("Call to Base class delay") ;
    endfunction
endclass

class LinkedPacket extends Packet ;
 
    local integer value ; // local property
    function new (int x = 0, int y = 0) ;
        super.new (x) ;
        value = y ;
    endfunction
    function integer delay() ;
        delay = super.delay() + value ; // Use of local property
        $display("Call to Derived class delay") ;
    endfunction
endclass

module test (input in, output out) ;

    Packet pkt1 = new ;
    LinkedPacket lk_pkt1 = new ;

    assign out = in ;
    initial
    begin
        pkt1.delay() ;
        lk_pkt1.delay() ;
    end

endmodule

