
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NON-SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a parameterized class and uses that to
// define data types of module port. Parameter of the class is overridden.

class A#(type T = int) ;
    T data ;
    function void param_size ;
        $display("The size of the parameter type is: %d", $bits(data)) ;
    endfunction
endclass

module test(input in, output out) ;

    A #(bit) a = new ;

    initial
    begin
        $display("The type parameter of class A has been overridden with type logic") ;
        a.param_size() ;
    end

    assign out = in ;

endmodule: test

