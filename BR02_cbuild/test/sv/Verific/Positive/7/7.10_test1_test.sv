
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The 'this' keyword is used to refer to properties or methods
// of the current instance. The 'this' keyword can only be used within non-static
// class methods.

class Student ;

    string name ;
    int roll ;

    function new( string name , int roll) ;
        this.name = name ;
        this.roll = roll ;
    endfunction

    function void setdata(string n, int r) ;
        name = n ;
        roll = r ;
        this.printdata() ;
    endfunction

    function void printdata() ;
        $display("Name :%s, Roll: %d",name, roll) ;
    endfunction

endclass 

module test ;

    Student s1 = new ("Jackie",34) ;
    initial 
       s1.setdata("Synthia",23) ;

endmodule

