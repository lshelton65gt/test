
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests chaining constructors. If the initialization
// method of the superclass requires arguments, there are two choices. 
// To always supply the same arguments, or to use the super keyword. Here first  
// choice is tested. If the arguments are always the same, then they 
// can be specified at the time the class is extended.

class Packet ;    //parent class
    integer value ;
    function new (int x = 0) ;
        value = x ;
        $display("From class LinkedPacket") ;
    endfunction
    function integer delay() ;
        delay = value * value ;
    endfunction
endclass

class LinkedPacket extends Packet(5) ;    // We are always supplying the same argument
                                          // to the base class constructor.
    integer value ;
    function new () ;
        super.new(5) ;
        value = 0 ;
        $display("From class LinkedPacket") ;
    endfunction

    function integer delay() ;
        delay = super.delay() + value * super.value ;
    endfunction

endclass

module test (input in, output out) ;

    Packet pkt1 = new(10) ;
    LinkedPacket lk_pkt1 = new ;
    assign out = in ;

endmodule

