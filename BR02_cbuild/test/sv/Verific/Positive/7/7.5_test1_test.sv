
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The data fields of an object can be used by qualifying
// property names with an instance name.

class Abc ;

    int data1 ;
    int data2 ;

    function new (int a, int b) ;
        data1 = a ;
        data2 = b ;
    endfunction

    function void swap () ;
        int temp ;
        temp = data1 ;
        data1 = data2 ;
        data2 = temp ;
    endfunction 

endclass

module test(clk, count) ;
    input logic clk ;
    input int count ;
    int count1 ;
    always @(count)
        count1 = count+1 ;
    Abc aa = new(100, 200) ;

    always @(posedge clk)
    begin
        aa.data1 = count1 ;
        aa.data2 = count1 + 10 ;
        aa.swap() ;
        count1 += 50 ;
        $display("Test: clk=%b count=%d", clk, count1) ;
    end

endmodule

