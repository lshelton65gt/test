
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Like modules, classes can nest. This design tests nested class
// by declaring class 'Node' inside class 'List'.

class List ;
    class Node ;  // class Node declared within class List
        Node next ;
        int data ;
        function newnode(int num) ;
            data = num ;
            next = Node'(0) ;
        endfunction
    endclass
    Node first ;
    Node last ;

    function new() ;
        first = Node'(0) ;
        last = Node'(0) ;        
    endfunction

    function void insert(int num) ;
        Node n = new () ;
        n.newnode(num) ;
        if( first == Node'(0))
            first = n ;
        else
            last.next = n ;
        last = n ; 
    endfunction

    function void print() ;
        Node n ;
        for( n = first; n !=last ; n = n.next) 
            $display("%d",n.data) ;
    endfunction
endclass

module test(input in, output out) ;

    List l = new ;
    initial 
    begin
        #5
        l.insert(5) ;
        l.insert(6) ;
        l.insert(7) ;
        l.insert(8) ;
        l.insert(9) ;
        l.insert(10) ;
        l.print() ;
    end
    assign out = in ;

endmodule

