
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Verilog parameter mechanism can be used to parameterize
// a class. This design defines a parameter in a class and members of that class
// use that parameter to define their widths.

class P #(int size = 8) ;
    int a [size] ;
    const int count = size ;

    function new() ;
        for( int i=0 ;i<count; i++)
            a[i] = i ;
    endfunction

    function print() ;
        for( int i=0 ;i<count; i++)
            $display("%d",a[i]) ;
    endfunction

endclass

module test(input in, output out) ;

    P #(16) p1 = new ;
    initial p1.print() ;
    assign out = ~in ;

endmodule

