
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The class members can be of any data type. This design 
// defines class members of user defined types.

class Type ;
    string str_type ;
    function new(string str) ;
        str_type = str ;
    endfunction

    function void func() ;
        $display("Enjoy") ;
    endfunction
endclass

typedef enum { RED, YELLOW} color ;
typedef struct {
    Type t ;
    color C ; 
} comb ;

class Album ;
    string singer ;
    string producer ;
    int cost ;
    int total_number_of_songs ;
    comb C ;

    function new(string s, string p, int c, int t, comb Co) ;
        singer = s ;
        producer = p ;
        cost = c ;
        total_number_of_songs = t ;
        C = Co ;
    endfunction

    function void show ;
        $display("%s, %s, %d, %d", singer, producer, cost, total_number_of_songs) ;
    endfunction

    function void callfunc() ;
        C.t.func() ;
    endfunction
endclass

module test ;
    Type t1 = new ("ghazal") ;
    comb C = '{t:t1, C:RED} ;
    Album A = new ("qwerty", "uiop", 10, 7, C) ;

    initial begin
       A.callfunc() ;
    end
endmodule                      

