
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests abstract classes and virtual functions.

virtual class Student ;  // Abstract class
//////////Properties///////
    string name ;
    int roll ;
    string address ;
    static integer num_of_student = 0 ;
//////////Methods//////////
    function new() ;
        name = "" ;
        address = "" ;
        roll = 12 ;
        num_of_student ++ ;
    endfunction

    function void setdata(string n, int r, string a) ;
        name = n ;
        roll = r ;
        address = a ;
    endfunction

    function string getname() ;
        getname = name ;
    endfunction

    function string getaddress() ;
        getaddress = address ;
    endfunction

    function int getroll() ;
        getroll = roll ;
    endfunction

    virtual function print() ; 
    endfunction

endclass

class Batch extends Student ;

    int year_of_joining ;
    function new(int yr);
        super.new() ;
        year_of_joining = yr ;
    endfunction

    function int getyear() ;
        getyear = year_of_joining ;
    endfunction

    function print() ;
        $display("Details of the Batch: %s %d %s %d", name, roll, address, year_of_joining) ;
    endfunction

endclass

class Stream extends Student ;

    string subject ;
    function new(string s);
        super.new() ;
        subject = s ;
    endfunction

    function print() ;
        $display("Details of the stream: %s %d %s %s", name, roll, address,subject) ;
    endfunction

endclass


module test (input in, output out) ;
    Batch b1 = new(2006) ;
    Stream s1 = new("Humanities") ;

    Student s = b1 ;
    initial 
    begin
        s.print() ; 
        s = s1 ;
        s.print() ;
    end

    assign out = in ;
endmodule

