
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the 'super' keyword. It is necessary to use 'super'
// to access members of a parent class when those members are overridden 
// by the derived class.

class Packet ; //parent class
    integer value ;
    function integer delay() ;
        delay = value * value ;
    endfunction 
    function new ;
        value = 10 ;
    endfunction
endclass

class LinkedPacket extends Packet ; //derived class
    integer value ;
    function integer delay() ;
        delay = super.delay() + value * super.value ;
    endfunction 
    function new ;
        super.new() ;
        value = 5 ;
    endfunction
endclass 

module test (clk, in1, in2, out) ;

    input clk ;
    input in1, in2 ;
    output bit [0:3] out ;

    Packet pkt1 = new ;
    LinkedPacket lk_pkt1 = new ;

    initial 
    begin
        pkt1 = lk_pkt1 ;
        $display("delay of Packet class = %d", pkt1.delay()) ; // delay() of Packet class called as delay() is not virtual
        $display("delay of  Linked packet class = %d", lk_pkt1.delay()) ; // delay() of LinkedPacket class called
    end

    assign out = in1 + in2 ;

endmodule

