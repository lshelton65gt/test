
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests inheritance by extending a class from 
// another class.

class A ;
    int data1 ;
    int data2 ;
    function new (int x = 0, int y = 0) ;
        $display("Display from class A data1 = %d, data2 = %d", x, y) ;
        data1 = x ;
        data2 = y ;
    endfunction
endclass

class B extends A ;
    int data3 ;
    function new (int x = 0, int y = 0, int z = 0) ;
        super.new (x, y) ;
        $display("Display from class B data3 =%d", z) ;
        data3 = z ;
    endfunction
    function B add (B b1, B b2) ;
        B temp ;
        temp.data1 = b1.data1 + b2.data1 ;
        temp.data2 = b1.data2 + b2.data2 ;
        temp.data3 = b1.data3 + b2.data3 ;
        return temp ;
    endfunction
endclass

module test (input in, output out) ;

    B b1 = new (10, 20, 30) ;
    B b2 = new (40, 50, 60) ;
    B b3 = new ;
    
    assign out = in ;
endmodule

