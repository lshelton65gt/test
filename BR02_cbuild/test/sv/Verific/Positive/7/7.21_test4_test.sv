
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Class scope resolved expressions can be triggered off. The
// following design checks this.

class A ;
    int mem ;
endclass

module test(input int in, output int out) ;

    assign out = ~in ;
    A a = new ;
    always @(in)
       a.mem = in ;
    always @(a.mem)
        $display("class property triggered") ;

endmodule

