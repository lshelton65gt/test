
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests shallow copy using 'new'. In this copy
// mechanism objects are not copied.

class A ;

    int a ;
    int obj_id ; // id takes up different value for different objects
    static int id = 0 ;
    function new (int x = 0) ;
        id = id + 1 ;
        a = x ;
        obj_id = id ;
    endfunction

endclass

class B ;

    int b ;
    A aa = new ;
    function new (int y = 0) ;
        b = y ;
    endfunction

    function void SetAB(int value) ;
        this.b = value ;
        this.aa.a = value/2 ;
    endfunction

endclass

module test (input in, output out) ;

       
        B pp = new ;
        B qq = new pp ; // This is an example of 'shallow' copy. Object ids will no longer be different
                        // as both pp and qq wil have the same object of class A. In other words, pp.aa
                        // and qq.aa point to the same object.
    initial
    begin
        $display("After copy(shallow)") ;
        $display("pp.b =%d, pp.aa.a =%d, qq.b =%d, qq.aa.a =%d", pp.b, pp.aa.a, qq.b, qq.aa.a) ;
        $display("pp obj_id = %d, qq obj_id = %d", pp.aa.obj_id, qq.aa.obj_id) ;
        pp.SetAB(999) ;
        $display("After setting resetting the values") ;
        $display("pp.b =%d, pp.aa.a =%d, qq.b =%d, qq.aa.a =%d", pp.b, pp.aa.a, qq.b, qq.aa.a) ;
        $display("pp obj_id = %d, qq obj_id = %d", pp.aa.obj_id, qq.aa.obj_id) ;

    end
    assign out = in ;

endmodule

