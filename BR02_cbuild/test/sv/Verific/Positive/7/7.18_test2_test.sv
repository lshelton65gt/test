
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Instance constants do not include an initial value in
// their declaration, only the const qualifier. This type of constant can be
// assigned a value at run-time, but the assignment can only be done once in 
// the corresponding class constructor. This design checks this.

class Abc ;
    bit pp[] ;
    const int size ; // instance constant, does not have any initial value
    function new (int x = 0) ;
        size = $random + x ;
        pp = new[size ? size : 1] ;
        $display("Instance constant size = %d", size) ;
    endfunction
endclass

module test (input in, output out) ;

    Abc aa1 = new (7) ;
    Abc aa2 = new (2) ;
    assign out = in ;

endmodule

