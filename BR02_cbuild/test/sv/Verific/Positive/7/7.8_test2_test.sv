
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks static properties of a class. A static
// member is shared by all instances of a class.

class dummy ;

    int data1 ;
    static int data2 = 50 ; // Test target
    function new(int val=0) ;
        data1 = val ;
    endfunction

endclass

module test(input in, output out) ;
  
    parameter size = 10 ;
    dummy D[size] ;  // An array of handles for dummy declared, but no object is created.
    initial
    begin
        for (int i=0; i<size; i++)
        begin
            D[i] = new(i) ;
        end
        for (int i=0; i<size; i++)
        begin
            $display ("automatic data1 = %d, static data2 = ", D[i].data1,  D[i].data2) ; // static value should remain the same for all the array members
        end
    end
    assign out = !in ;

endmodule

