
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests use of 'local' and 'protected' keywords, 
// 'local' members cannot be inherited while 'Protected' members can be.

class Student;
//////////Properties///////
    string name ;
    local int roll ; // local member, cannot be inherited
    protected string address ; // protected member, can be inherited
    function new(int roll) ;
       this.roll = roll ;
    endfunction
endclass

class Batch extends Student;
    int year_of_joining ;
    function new(int yr, int r, string addr, string n) ;
        super.new(r) ;
        year_of_joining = yr ;
        address = addr ;
        $display("Accessing protected member address = %s", address) ;
        name = n ;
    endfunction
endclass

module test (input in, output out) ;

    Batch b = new (2000, 5,"Antpur", "Debashis") ;
    assign out = in ;

endmodule

