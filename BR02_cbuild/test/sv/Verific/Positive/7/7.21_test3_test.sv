
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The '::' operator allows access to the public or protected members
// of a superclass from within the subclass.

class A ;
    typedef enum {bin, oct, hex} radix ;

    task print(radix r, int n) ;
        case(r)
            bin: $display("Binary value is n=%b", n) ;
            oct: $display("Octal value is n=%o", n) ;
            hex: $display("Hex value is n=%x", n) ;
        endcase
    endtask

endclass

module test(input in, output out) ;

    assign out = ~in ;
    A a = new ;
    initial 
        a.print(A::oct, 17) ;

endmodule
