
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Creating a Student class with a constructor, and a
// static variable to know the total number of students.

class Student ;
//////////Properties///////
    string name ;
    int roll ;
    string address ;
    static integer num_of_student = 0 ;
//////////Methods//////////
    function new() ;
        name = "" ;
        address = "" ;
        roll = 12 ;
        num_of_student ++ ;
    endfunction

    function void setdata(string n, int r, string a) ;
        name = n ;
        roll = r ;
        address = a ;
    endfunction

    function string getname() ;
        getname = name ;
    endfunction

    function string getaddress() ;
        getaddress = address ;
    endfunction

    function int getroll() ;
        getroll = roll ;
    endfunction
endclass

module test ;


        Student s1=new ;
        Student s2=new ;

    initial begin
        s1.setdata("rupali",41, "BE-363") ;
    
        s2.setdata("anita",21, "Bangalore") ;
    end
 
    always@ (s1.num_of_student)
        $display("Total number of student: %d", s1.num_of_student) ;

endmodule

