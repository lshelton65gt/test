
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): System Verilog also allows to pass arguments to the class
// constructor, which allows run-time customization of an object.

typedef enum {IDLE, BUSY, START, STOP} cmd ;

class Packet ;

    integer command ;
    bit [0:12] address ;
    int time_requested ;

    function new (integer cmd = IDLE, bit [0:12] addr=0, int cmd_time) ;
        command = cmd ;
        address = addr ;
        time_requested = cmd_time ;
    endfunction

endclass

module test ;

    Packet p = new (START, $random, $time) ;

    initial
    begin
        case (p.command)
        (IDLE)  : $display("idle")  ;
        (BUSY)  : $display("busy")  ;
        (START) : $display("start") ;
        (STOP)  : $display("stop")  ;
        endcase
    end

endmodule

