
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
        
// TESTING FEATURE(S): 'local' qualifier can be used in any class method declaration
// to make it available only to methods inside the class. This design tests use of 
// local methods.

class Packet ;    //parent class
    local integer value ;  // local variable
    function new (int x = 0) ;
        value = x ;
    endfunction
    local function integer overhead() ; // local method
        $display("local member function of  base class") ;
        overhead = value * 10 ;
    endfunction
    function integer delay() ;
        $display("public member function of  base class") ;
        delay = overhead + value * value ;
    endfunction
endclass

class LinkedPacket extends Packet ;
    local integer value ; // local variable
    function new (int x = 0, int y = 0) ;
        super.new (x) ;
        value = y ;
    endfunction
    local function integer overhead() ; // local method
        $display("local member function of  derived class") ;
        overhead = overhead + value * 10 ;
    endfunction
    function integer delay() ;
        $display("public member function of  derived class") ;
        delay =  overhead() + value * value ;
    endfunction
endclass

module test (input in, output out) ;

    Packet pkt1 = new ;
    LinkedPacket lk_pkt1 = new ;
    int delay1 ;
    int delay2 ;
    int tot_delay ;
    initial
    begin
        delay1 = pkt1.delay() ;    // can access
        delay2 = lk_pkt1.delay() ; // non-local methods of a class.
        tot_delay = delay1 + delay2 ;
    end

    assign out = in ;

endmodule

