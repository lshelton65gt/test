
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests that subclass objects are also legal
// representative objects of their parent classes.

class A ;
    int data1 ;
    int data2 ;

    function void SetA(int x, int y) ;
        data1 = x ;
        data2 = y ;
    endfunction
    
    function A add (A a1, A a2) ;
        A temp = new ;
        temp.data1 = a1.data1 + a2.data1 ;
        temp.data2 = a1.data2 + a2.data2 ;
        return temp ;
    endfunction
endclass

class B extends A ;
    int data3 ;
    function B add (B b1, B b2) ;
        B temp = new ;
        temp.data1 = b1.data1 + b2.data1 ;
        temp.data2 = b1.data2 + b2.data2 ;
        temp.data3 = b1.data3 + b2.data3 ;
        return temp ;
    endfunction
    function void SetB(int x, int y, int z) ;
        SetA(x,y) ;
        data3 = z ;
    endfunction
endclass

module test (input in, output out) ;

    B b1 = new ;
    B b2 = new ;
    B b3 = new ;
    A a1 = new ;
    A a2 = new ;
    A a3 = new ;
    initial
    begin
        b1.SetB(100,50,25) ;
        b2.SetB(10,5,2) ;
        a1.SetA(110,15) ;
        a1.SetA(11,1) ;
        a1 = b1 ;
        b3 = b1.add (b1, b2) ;
        $display ("b3.data1 =%d, b3.data2=%d, b3.data3=%d", b3.data1, b3.data2, b3.data3) ;
        a3 = a1.add (a1, a2) ;
        $display ("a3.data1 =%d, a3.data2=%d", a3.data1, a3.data2) ;
    end

endmodule

