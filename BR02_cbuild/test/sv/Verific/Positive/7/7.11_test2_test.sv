
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation. 

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design implements deep copy using a function 'copy'.
// If objects are copied using 'new', objects are not copied. This function 'copy'
// recreates member objects using 'new'.

class Deep ;
   int j = 5; 
   string n ;
endclass 

class Shallow ;
   int i = 1;
   int data = 2;
   Deep a = new;
   function void  copy (ref Shallow s) ;
       i = s.i ;
       data = s.data ;
       //a = new (s.a) ; // This will make a deep copy of Deep as Deep do not have any references
       a = s.a ; // This will make a deep copy of Deep as Deep do not have any references
   endfunction
endclass

module test (input in, output out) ;

    Shallow s1 = new ;
    Shallow s2 = new ;

    initial
    begin
        s2.copy(s1) ;
        s2.i = 12 ;
        s2.data = 23 ;

        s2.a.j = 15 ;
        s2.a.n = "hello" ;
    
        $display("No changes are present") ;
        $display("s1.i: %d, s1.data: %d, s1.a.j: %d, s1.a.n: %s",s1.i, s1.data, s1.a.j, s1.a.n) ;
        $display("s2.i: %d, s2.data: %d, s2.a.j: %d, s2.a.n: %s",s2.i, s2.data, s2.a.j, s2.a.n) ;
    end

    assign out = ~in ;

endmodule

