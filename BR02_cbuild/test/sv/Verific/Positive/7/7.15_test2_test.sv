
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): It is legal to assign a superclass handle to a subclass
// variable if the superclass handle refers to an object of the given subclass.

class Packet ;    //parent class
    integer value ;
    function integer delay() ;
        $display("super class value =%d", value) ;
        delay = value * value ;
    endfunction
endclass

class LinkedPacket extends Packet ;    //derived class
    integer value ;
    function integer delay() ;
        $display("sub-class value =%d", value) ;
        delay = super.delay() + value * super.value ;
    endfunction
endclass

module test (input in, output out) ;

    LinkedPacket lk_pkt1 = new ;
    Packet pkt1 = lk_pkt1 ; // legal. super-class handle pointing to sub-class variable
    LinkedPacket lk_pkt2 ;
   
    initial
    begin
        //$cast( singular dest_handle, singular source_handle ) ;      
	int is_legal_class = $cast( lk_pkt2, pkt1 ) ; // Assigns a superclass variable to a sub-class handle
        pkt1.delay() ;     // It should call delay() of LinkedPacket class.
        lk_pkt2.delay() ;  // It should call delay of Packet class.
        $display("is_legal_class = %d", is_legal_class) ;
    end
endmodule

