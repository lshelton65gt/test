
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
       
// TESTING FEATURE(S): Test of data hiding and encapsulation.
// Use of protected  methods.

class Packet ;    //parent class
    local integer value ;  // local property
    function new (int x = 0) ;
        value = x ;
    endfunction
    protected function integer delay() ; //protected method
        delay = value * value ;
    endfunction
endclass

class LinkedPacket extends Packet ;
    local integer value ;
    function new (int x = 0, int y = 0) ;
        super.new (x) ;
        value = y ;
    endfunction
    function integer delay() ;
        $display("Accessing protected member function delay = %d", super.delay()) ;
        delay = super.delay() + value * value ; // Can access parent class's protected member
    endfunction
endclass


module test (input in, output out) ;

    LinkedPacket lk_pkt1 = new ;
    int delay1 ;
    int tot_delay ;
    initial
        delay1 = lk_pkt1.delay() ; // accessing non-protected non-local method of class.

    assign out = 1 + in ;
endmodule

