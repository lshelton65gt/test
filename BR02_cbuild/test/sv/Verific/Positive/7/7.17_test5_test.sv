
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
       
// TESTING FEATURE(S): A protected class property or method has all the characteristics
// of local member, except that it can be inherited. This design tests 'protected'
// property.

class Packet ;    // parent class
    protected integer value ;  // protected property
    function new (int x = 0) ;
        value = x ;
    endfunction
    function integer delay() ;
        delay = value * value ;
    endfunction
endclass

class LinkedPacket extends Packet ;    
    integer overhead ;
    function new (int x = 0, int y = 0) ;
        super.new (x) ;
        overhead = y ;
    endfunction
    function integer delay() ;
        delay = value * overhead + super.value; // Accessing protected property of class Packet which has been inherited
        $display("Accessing protected member value from derived class value= %d", super.value) ;
    endfunction
endclass

module test (input int value, overhead, output int tot_delay) ;

    LinkedPacket lk_pkt1 ;
    initial
    begin
        lk_pkt1 = new(value, overhead) ;
        tot_delay = lk_pkt1.delay() + value ;
    end

endmodule

