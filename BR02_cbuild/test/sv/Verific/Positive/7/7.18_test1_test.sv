
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Global constant properties are those that include an
// initial value as part of their declaration. They are similar to other 
// constant variables in that they cannot be assigned a value anywhere other 
// than in the declaration. The following tests this feature.

class Jumbo_Packet ;
    const int max_size = 9 * 1024 ; // global constant
    byte payload [] ;
    function new( int size ) ;
        payload = new[ size > max_size ? max_size : size ] ;
        $display("Global constant max_size = %d", max_size) ;
    endfunction
endclass

module test (input in, output out) ;

    Jumbo_Packet pkt1 = new (1024) ;
    Jumbo_Packet pkt2 = new (9 * 1024) ;
    Jumbo_Packet pkt3 = new (10 * 1024) ;
    assign out = in ;

endmodule

