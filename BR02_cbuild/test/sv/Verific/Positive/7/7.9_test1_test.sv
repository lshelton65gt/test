
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A static method has no access to non-static members
// (properties or methods), but it can directly access static class properties 
// or call static methods of the same class. It can be called without any 
// class instantiations using the '::' operator.

class Student ;
    static int current = 0 ;
    string name ;
    int roll ;

    static function int next_num() ;
        next_num = ++current ; // OK to access static class property
    endfunction 
 
    function void setdata(string n, int r) ;
        name = n ;
        roll = r ;
    endfunction

endclass 

module test ;
    Student s1 = new ;
    initial
    begin
        s1.setdata("Tony Jaa",23) ;
        $display("Next Number = %d", Student::next_num()) ;
        $display("Name : %s    Roll : %d", s1.name, s1.roll) ;
    end
endmodule

