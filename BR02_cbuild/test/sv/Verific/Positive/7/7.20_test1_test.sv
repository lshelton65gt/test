
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design demonstrates polymorphism in System Verilog.

virtual class Employee ;  // Abstract class
    virtual function void print ;
    endfunction
endclass

class Worker extends Employee ;
    virtual function void print ;
        $display("A factory worker") ;
    endfunction
endclass

class Manager extends Employee ;
    virtual function void print ;
        $display("A Manager") ;
    endfunction
endclass

class ContractWorker extends Employee ;
    virtual function void print ;
        $display("A worker on a contractual basis") ;
    endfunction
endclass

module test(input in, output out) ;
    Employee emp[3] ; // an array of handles of type base class
    Worker w = new ;
    Manager m = new ;
    ContractWorker cw = new ;
    initial
    begin
        emp[0] = w ;
        emp[1] = m ;
        emp[2] = cw ;

        for(int i=0; i<3; i++)
            emp[i].print() ;
    end

    assign out = in + 1 ;
endmodule
