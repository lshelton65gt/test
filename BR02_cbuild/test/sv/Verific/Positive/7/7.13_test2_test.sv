
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If class properties and methods in a derived class are
// overridden, these overridden members referred through a base class pointer,
// get the original members in the 'Packet' class. However, from the base class
// all the overridden members in the derived class are now hidden.


class Packet ;
    integer i = 1 ;
    function integer get() ;
        get = i ;
    endfunction
endclass

class LinkedPacket extends Packet ;
    integer i = 2 ;
    function integer get () ;
        get = -i ;
    endfunction
endclass

module test (input in, output out) ;

    LinkedPacket lp = new ;
    Packet p = lp ;

    initial
    begin
        $display("p.i =%d       p.get() =%d", p.i, p.get()) ;
        $display("lp.i =%d      lp.get() =%d", lp.i, lp.get()) ;
    end

endmodule

