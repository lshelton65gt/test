
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design demonstrates polymorphism in System Verilog.

virtual class Bird ;  // Abstract class
    function new ;
    endfunction
    virtual function void print ;
    endfunction
    string name ;
endclass

class FlyingBird extends Bird ;
    function new (string str) ;
        super.new() ;
        name = str ;
    endfunction
    virtual function void print ;
        $display("%s: This is a flying bird", name) ;
    endfunction
endclass

class NonFlyingBird extends Bird ;
    function new (string str) ;
        super.new() ;
        name = str ;
    endfunction
    virtual function void print ;
        $display("%s:This is a non-flying bird", name) ;
    endfunction
endclass

class MountainBird extends FlyingBird ;
    function new (string str) ;
        super.new(str) ;
    endfunction
    virtual function void print ;
        $display("%s: This type of birds are found in the mountains", name) ;
    endfunction
endclass

class MythologicalBird extends NonFlyingBird ;
    function new (string str) ;
        super.new(str) ;
    endfunction
    virtual function void print ;
        $display("%s: This is a mythological bird.", name) ;
    endfunction
endclass

module test(input in, output out) ;

    parameter size = 6 ;
    Bird b[size] ;
    FlyingBird swan = new("Swan") ;
    FlyingBird pigeon = new("Pigeon") ;
    NonFlyingBird penguin = new("Penguin") ;
    NonFlyingBird ostrich = new("Ostrich") ;
    MountainBird eagle = new("Eagle") ;
    MythologicalBird phoenix = new("Phoenix") ;

    initial 
    begin
        b[0] = swan ;
        b[1] = pigeon ;
        b[2] = penguin ;
        b[3] = ostrich ;
        b[4] = eagle ;
        b[5] = phoenix ;
        for(int i=0; i<size; i++)
            b[i].print() ; // call the virtual function print
    end

endmodule
