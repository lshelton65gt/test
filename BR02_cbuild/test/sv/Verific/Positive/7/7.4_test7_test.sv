
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a class and uses variable of that
// class as an input port.

class Abc ;
    int x;
    function new() ;
     x = 1 ;
    endfunction
endclass

module test(clk, o) ;

    Abc abc = new ;
    input clk ;
    output o ;
    bot b(clk, abc, o) ;

endmodule

module bot(clk, a, o) ;

    input clk ;
    input Abc a ;
    output reg o ;

    always@(posedge clk)
    begin
        o = is_null(a) ;
    end

    function is_null(Abc aa) ;
        if (aa == null) return 1'b1 ;
        return 1'b0 ;
    endfunction
    initial
        $display("x=%d", a.x) ;

endmodule

