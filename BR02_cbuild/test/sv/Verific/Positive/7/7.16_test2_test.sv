
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests chaining constructors. If the initialization
// method of the superclass requires arguments, there are two choices. 
// To always supply the same arguments, or to use the 'super' keyword. Here second
// choice using the 'super' keyword is tested.

class Packet ;    //parent class
    integer value ;
    function new (int x = 0) ;
        value = x ;
        $display("In class Packet::new") ;
    endfunction
    function integer delay() ;
        delay = value * value ;
    endfunction
endclass

class LinkedPacket extends Packet ;

    integer value ;
    function new (int x = 0, int y = 0) ;
        super.new(x) ;         // Using the 'super' keyword to access the base class's constructor
        value = x ;
        $display("In class LinkedPacket::new") ;
    endfunction
    function integer delay() ;
        delay = super.delay() + value * super.value ;
    endfunction
endclass

module test (input in, output out) ;

    Packet pkt1 = new ;
    LinkedPacket lk_pkt1 = new ;
    assign out = ~in ;

endmodule

