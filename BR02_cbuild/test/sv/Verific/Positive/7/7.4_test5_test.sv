
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Uninitialized object handles are set to null by default. 
// An uninitialized object can be detected by comparing its handle with null.

class Abc ;
    int a ;
    int b ;
    function new (int x, int y) ;
        a = x ;
        b = y ;
    endfunction
    function void show() ;
        $display ("\n a = %d,   b = %d", a, b) ;
    endfunction
endclass

module test ;
    function void swap(Abc aa) ;
        int temp ;
        if (aa == null) 
            aa = new (100, 200) ;    
        temp = aa.a ;
        aa.a = aa.b ;
        aa.b = temp ;
        
    endfunction
    logic clk ;
    Abc bb ;
    initial
    begin
        bb = new (500,600) ;
        bb.show() ;
        swap(bb) ;
        bb.show() ;
    end
endmodule

