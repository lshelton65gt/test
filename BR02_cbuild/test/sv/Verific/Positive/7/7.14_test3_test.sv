
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing SystemVerilog specific 'this' and 'super' keywords.
// 'this' refers to the current class object and 'super' refers to the base class
// part of the current class object. Testing 'this' in multi-level inheritance.

class C ;
    int n ;

    function new(int i) ;
        this.n = i ;
    endfunction

    function int fn(int i) ;
        return this.n + i ;
    endfunction
endclass

class B extends C ;
    int n ;

    function new(int i, j) ;
        super.new(j) ;
        this.n = i ;
    endfunction

    function int fn(int i) ;
        return this.n + super.fn(i) ;
    endfunction
endclass

class A extends B ;
    int n ;

    function new(int i, j, k) ;
        super.new(j, k) ;
        this.n = i ;
    endfunction

    function int fn(int i) ;
        return this.n + super.fn(i) ;
    endfunction
endclass

module test (i, o) ;

    input int i ;
    output int o ;

    int k = 2 ;

    int p = fn() ;

    function integer fn ;
        A a1 = new (k, 2, 3) ;
        int ret_val =  a1.fn(k) ;
        return ret_val ;
    endfunction

    initial 
        #100 $display ("The value of p = %d", p) ;

    assign o = i + p ;

endmodule

