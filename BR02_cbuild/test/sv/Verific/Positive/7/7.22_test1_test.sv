
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design defines a method outside the body of
// a class.

class A ;
    extern function void out_of_class_print() ;
endclass

function void A::out_of_class_print() ;
    $display("This member function is defined out of the class") ;
endfunction

module test(input in, output out) ;
    
    A a = new ;
    initial
        a.out_of_class_print() ;

    assign out = in ;

endmodule


