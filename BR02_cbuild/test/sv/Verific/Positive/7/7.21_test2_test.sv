
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design uses the '::' operator to specify a 
// static function of a class.

class A ;
    static task mem_function ;
        $display("Printing from a static member function") ;
    endtask
endclass

module test(input in, output out) ;

    assign out = ~in ;
    initial 
        A::mem_function ; // calling the static member task of class A using the scope resolution operator

endmodule
