
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A class object is used by first declaring a variable
// of that class type (that holds an object handle) and then creating an object 
// by calling new and assigning it to the variable.
// eg.: 
//      Packet p; // declare a variable of class Packet
//      p = new;  // initialize variable to a new allocated object of the class Pack

class Abc ;
    int a  ;
    int b ;
    
    function new (int x, int y ) ;
        a = x ;
        b = y ;
    endfunction

    function void show () ;
        $display ("\n a = %d,   b = %d", a, b) ;
    endfunction
endclass

module test ;
    Abc A = new (100, 200) ;
    initial
        #5 A.show() ;
endmodule

