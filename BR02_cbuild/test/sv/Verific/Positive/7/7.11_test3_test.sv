
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Declaring a class variable only creates the name by which
// the object is known. Thus, if another variable is declared and assigned the 
// old handle, then there is still only one object, which can be referred to
// with either of the names (bb1 or bb2 in this design). As new was 
// executed only once, so only one object has been created.  This design checks this.

class A ;
    int a ; // Every class has a default (built-in) new method. 
    function void SetA(int num) ;
        a = num ;
    endfunction
endclass

class B ;
    int b ;
    A aa = new ;
    function void SetB(int num) ;
        aa.SetA(num/2) ;
        b = num ;
    endfunction
endclass

module test (input in, output out) ;

    B bb1 = new ;
    B bb2 ;
    
    initial
    begin
        bb1.SetB(10) ;
        bb2 = bb1 ; 
        $display("bb1.aa.a = %d, bb1.b = %d", bb1.aa.a, bb1.b) ;
        $display("bb2.aa.a = %d, bb2.b = %d", bb2.aa.a, bb2.b) ;
    end

endmodule

