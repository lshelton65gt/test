
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NON-SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a parameterized class and uses that to
// define data types of module port. Parameter of the class is overridden.

class ComplexPkg # (type T = int) ;
    typedef struct {
        T i, r ;
    } Complex ;

    function Complex add(Complex a, b) ;
        add.r = a.r + b.r ;
        add.i = a.i + b.i ;
        $display("The sum is %d +%di", add.r, add.i) ;
    endfunction

    function Complex mul (Complex a, b) ;
        mul.r = (a.r * b.r) - (a.i * b.i) ;
        mul.i = (a.r * b.i) + (a.i *b.r) ;
        $display("The product is %d +%di", mul.r, mul.i) ;
    endfunction

endclass : ComplexPkg


module test(input ComplexPkg::Complex #(bit [7:0]) a, b , output ComplexPkg::Complex #(bit [7:0]) out) ;

    initial
    begin: b1
        ComplexPkg::Complex #(logic [7:0]) tmp ;
        tmp = ComplexPkg::mul(a,b) ;
        out = tmp ;
    end

endmodule: test

