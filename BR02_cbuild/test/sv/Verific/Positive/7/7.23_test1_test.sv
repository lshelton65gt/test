
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The normal Verilog parameter mechanism can be used to 
// parameterize a class. This design defines a type parameter in a class and
// members of that class are of that type parameter type. In this way this 
// class be used to specify different types members inside class.

class Node #(type T = int ) ;
    T data ;
    Node #(T) next ;
   
    function new(T num) ;
        data = num ;
        next =Node'(0) ;
    endfunction

endclass

class List #(parameter type T = int ) ;
    Node #(T) first ;
    Node #(T) last ;

    function new() ;
       first = Node'(0) ;
       last = Node'(0) ;
    endfunction
    
    function void insert(T num) ;
        Node #(T) n = new(num) ;
        if(first == Node'(0) )
            first = n ;
        else
            last.next = n ;
        last = n ;
    endfunction

    function void print() ;
        Node #(T) n ;
        for (n = first; n != last; n = n.next) ;
            $display( n.data) ; 
    endfunction

endclass

module test(input in, output out) ;

List #(string) l = new ;
    initial 
    begin
        l.insert("asd") ;
        l.insert("sdf") ;
        l.insert("dfg") ;
        l.insert("fgh") ;
        l.insert("ghj") ;
        l.insert("hjk") ;
        l.print() ;
    end

    assign out = in ;
endmodule

