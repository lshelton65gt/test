
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design casts a bit type variable to a structure and then
// assigns the casted variables' structure member to another variable.

typedef struct packed {
    int a ;
    struct packed { int x ; bit [3:0] y ; } var1 ;
} myType ;

myType x ;

module test(input int in, output out) ;

    typedef bit [$bits(x)-1 : 0] bits ;
    bits b = '1 ;

    typedef int myType1 ;
    myType1 modInt ;

    initial
    begin
        x = myType'(b) ; 
        modInt = x.a ;
    end

    initial
        $display("x.a = %b,   x.var1.x = %b,    x.var1.y = %b", x.a, x.var1.x, x.var1.y) ;

    assign out = &in ;

endmodule

