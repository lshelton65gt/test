
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test the string method substr(). 
//                    The 'outof range' string index handling, in substr()


module test(input in, output out) ;

    parameter p1 = func1() ;  
    parameter p2 = func2() ;  
    parameter p3 = func3() ;  
    parameter p4 = func4() ;  
    parameter p5 = func5() ;  

    initial
        $display("p1 = %d,  p2 = %d,  p3 = %d,  p4 = %d", p1, p2, p3, p4) ;

    function integer func1() ;
        string str =  "This is a test case for string methods." ;
        string ret_str ;
        string str_modified ;
        int i, j ;
 
        str_modified = str ;
        ret_str = str.substr(-2, 13) ;
        if (ret_str == "" && str_modified == str) 
            return 11 ;
        else
            return 10 ;
    endfunction
    
    function integer func2() ;
        string str =  "This is a test case for string methods." ;
        string ret_str ;
        string str_modified ;
        int i, j ;
 
        str_modified = str ;
        ret_str = str.substr(31, 43) ;
        if (ret_str == "" && str_modified == str) 
            return 11 ;
        else
            return 10 ;
    endfunction
    
    function integer func3() ;
        string str =  "This is a test case for string methods." ;
        string ret_str ;
        string str_modified ;
        int i, j ;
 
        i = -2 ;
        j = 5 ;
        str_modified = str ;
        ret_str = str.substr(i, j) ;
        if (ret_str == "" && str_modified == str) 
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func4() ;
        string str =  "This is a test case for string methods." ;
        string ret_str ;
        string str_modified ;
        int i, j ;
 
        i = 31 ;
        j = 43 ;
        str_modified = str ;
        ret_str = str.substr(i, j) ;
        if (ret_str == "" && str_modified == str) 
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func5() ;
        string str =  "This is a test case for string methods." ;
        string ret_str ;
        string str_modified ;
        int i, j ;
 
        i = 24 ; // if i is greater than j str.substr(i, j) returns ""
        j = 20 ;
        str_modified = str ;
        ret_str = str.substr(i, j) ;
        if (ret_str == "" && str_modified == str) 
            return 11 ;
        else
            return 10 ;
    endfunction

    assign out = in ;

endmodule

