
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If the size (in bits) of a packed array is not a multiple
// of 8, then the packed array is zero filled on the left. 

module test() ;

    string s1 = "Hello World" ;
    bit [127:0] b;

    initial 
    begin
        b = s1 ;
        $display("%b", b) ;
        $display("\n String replicated by 2: %s ", {2{s1}}) ;
    end

endmodule

