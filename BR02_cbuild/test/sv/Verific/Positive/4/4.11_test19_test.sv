
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing masking feature of a packed structure.

module test;

struct packed {
    int str_int;
    integer str_integer;
} var1;

initial
begin
    var1.str_integer = 4'b1x1z;
    var1.str_int = 10;
    $display("Members of the structure are str_int = (should be 10) %d, and str_integer = (should be 1x1z) %b", var1.str_int, var1.str_integer);
end

endmodule    

