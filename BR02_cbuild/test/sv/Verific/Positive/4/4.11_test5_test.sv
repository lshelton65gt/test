
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test case on accessing the members of a packed structure.

typedef struct packed {
    bit[3:0]strBit;
    logic [3:0]strLog;
} myType;

module test;

    myType a;
    logic [3:0]b;

    assign b = a[5:2];

    initial
    begin
        a.strBit = '1;
        a.strLog = 'z;
        $display("Part select output  = (should be 11xx) %b", b);
    end

endmodule

