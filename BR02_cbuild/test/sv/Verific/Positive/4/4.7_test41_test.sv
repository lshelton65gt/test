
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design is to test the string method atoreal(). If the
// string starts with any non digit then the method atoreal() will return 0.

module test(input in, output out) ;

    parameter p1 = func() ; 

    initial
        $display("p1 = %d", p1) ;

    function integer func() ;
        string str = "p210.00122" ;
        string str_modified ;
        real r ;
        str_modified = str ;
        r = str.atoreal() ;
        if (r == 0 && str_modified == str) // the 2nd cond. is used so that str remains unchanged
            return 11 ;
        else
            return 10 ;
    endfunction

    assign out = ~in ;

endmodule

