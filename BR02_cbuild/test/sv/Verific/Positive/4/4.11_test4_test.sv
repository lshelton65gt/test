
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Declaring reg variable inside an union.

typedef union {
    reg [3:0] a ;
    int b ;
    shortreal c ;
} myUn ;

module test(input int in, output out) ;

    myUn var1 ;

    assign var1.b = 0 ;

    assign out = $onehot(in) ;

    initial
        $display("var1.b=%b", var1.b) ;

endmodule

