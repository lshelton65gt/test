
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Tests reg declaration inside a structure.

module test(input in, output out) ;

    struct packed {
        reg [3:0] a ;
        bit [7:0] x ;
    } var1 ;

    assign var1 = '1 ;

    assign out = in ;

    initial
        $display("var1.a = %b,   var1.x = %b", var1.a, var1.x) ;

endmodule

