
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses putc(..) method of string data type.

module test ;
    parameter string str = manipulate("hello") ;

    string str1 ;

    initial
    begin
       str1 = "hello" ;
       $display("%s", str) ;
       str1.putc(4, "world") ;
       $display("%s", str1) ;
       if (str != "hellw" || str1 != str) $display("Error") ;
    end

    function string manipulate(string a) ;
        a[4] = "world" ; // equivalent to a.putc(5, "world")
        return a ;
    endfunction

endmodule

