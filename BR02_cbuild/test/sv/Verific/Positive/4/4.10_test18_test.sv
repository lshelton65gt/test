
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing value of unassigned first member of an enum.

module test;

    enum { mem1, mem2 = 6, mem3 = 10 } var1;

    initial
        $display("Value of the unassigned first member is : %d (should be zero)", mem1);

endmodule

