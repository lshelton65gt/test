
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing array of structures.

module test;

    typedef struct {
        logic [63:0] str_reg; 
        int in;
    } str;
    str my_str[1:0];

    initial
    begin
        my_str[1].str_reg = "Hello!";
        my_str[1].in = $random;
        $display("Str.reg = (should print Hello!)\t%s, Str.int = (should print a random number)\t%d", my_str[1].str_reg, my_str[1].in);
    end

endmodule

