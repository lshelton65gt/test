
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Check of the methods last, prev, next, name and num of a enum.
// The elements of a enum is printed from last to first

typedef enum {north = 1, south, east, west} direction ;

module test ;
    direction dir ;

    initial begin
        $display("Total number of elemnts is: %d", dir.num()) ;
        dir = dir.last() ;

        forever begin
             $display("%s : %d",dir.name(), dir) ;
             if(dir == dir.first() ) break ;
             dir = dir.prev() ;
         end
    end
endmodule

