
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of type casting. The expression to be cast is enclosed
// within concatenation or replication braces.

module test(output shortint out1, output int out2,
               output reg result,
               input bit [7:0] bit_vec1, bit_vec2) ;

    typedef struct
    {
        int i;
        real r;
    } my_struct ;

    typedef struct
    {
        my_struct st;
        shortreal sr;
    } derived_struct;

    my_struct b_st;
    derived_struct d_st;

    int int1, int2;

    function int getInt(input my_struct s);
        getInt = s.i ;
    endfunction

    always @(bit_vec1, bit_vec2)
    begin
        out1 = shortint'({bit_vec1, bit_vec2}) ;
        out2 = int'({4{bit_vec2 - bit_vec1}}) ;

        d_st.st.i = out2;
        d_st.st.r = 5.7e80;
        d_st.sr = 3.2 ;
        b_st.i = d_st.st.i ;
        b_st.r = d_st.st.r ;

        int1 = getInt(b_st) ;
        int2 = getInt(my_struct'(d_st)) ;

        if(int1 == int2)
            result = 1;
        else
            result = 0; 
    end

endmodule

