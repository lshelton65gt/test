
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of structure declaration with a typedef-ed enum as a member.

module test(output real sum, input int int1, 
            input real real1, input int sel, input we, reset) ;
     
    parameter p = 10 ;

    typedef union { int i; real r; } my_union ;
    typedef enum { INT, REAL} my_enum ;    
    typedef struct {
         my_enum opt ;
         my_union data ;
    } num ;

    num count [p -1 : 0] ;

    int i ;

    always @(int1, real1, sel, reset)
    begin
        if(reset == 1'b1)
            i = 0 ;
        else 
        begin
            if((sel/2) == 0)
            begin
                count[i].opt = INT ;
                count[i].data.i = int1 ;
            end
            else
            begin
                count[i].opt = REAL ;
                count[i].data.r = real1 ; 
            end
            i = i + 1 ;
            if(i == (p -1))
                i = 0 ;            
        end
    end

    int j ;

    always @(we)
    begin
        sum = 0 ;
        for(j = 0 ; j < p; j = j + 1)
        begin
            case(count[j].opt)
                 INT : sum = sum + count[j].data.i ;
                 REAL : sum = sum + count[j].data.r ;
            endcase
        end                
    end

endmodule

