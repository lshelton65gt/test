
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the string method icompare(). 

module test(input int in, output out) ;

    parameter p1 = func1() ;  
    parameter p2 = func2() ; 
    parameter p3 = func3() ;  
    parameter p4 = func4() ;  
    parameter p5 = func5() ;  
    parameter p6 = func6() ;  
    
    initial
        $display("p1 = %d, p2 = %d, p3 = %d, p4 = %d, p5 = %d, p6 = %d", p1, p2, p3, p4, p5, p6) ;

    function integer func1() ;
        string str1 =  "ABCD" ;
        string str2 = "abcdefgh" ; // differ in size and case
        
        if ( str1.icompare(str2) < 0 )
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func2() ;
        string str2 = "abcdefgh" ; // differ in size
        string str3 = "abcd" ;   // differ in case
        
        if ( str2.icompare(str3) == 0 )
            return 10 ; // FAILED
        else
            return 11 ; // PASSED
    endfunction

    function integer func3() ;
        string str2 = "abcdefgh" ;
        
        if ( str2.icompare(str2) == 0 )
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func4() ;
        string str1 =  "ABCD" ;
        string str2 = "abcdefgh" ; // differ in size and case
        
        if ( str2.icompare(str1) > 0 )
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func5() ;
        string str1 =  "ABCD" ;
        
        if ( str1.icompare(str1.tolower()) == 0 )
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func6() ;
        string str1 =  "ABCD" ;
        string str3 = "abcd" ;   // differ in case
        
        if ( str3.icompare(str1) == 0 )
            return 11 ;
        else
            return 10 ;
    endfunction

    assign out = |in ;

endmodule

