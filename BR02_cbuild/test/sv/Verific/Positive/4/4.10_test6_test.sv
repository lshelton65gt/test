
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Assign parameter and local parameter to enum members.

module enum_type(output int sum[3:0], 
                 input int num[3:0][-4:-1]) ;

    parameter p = 4 ;
    localparam q = p - 1 ;

    enum logic [31:0] 
    {
         MATH = -p,
         PHY = -q,
         CHE = -q + 1,
         BIO = -p + 3
    } subject ;    

    always @(*)
    begin
        sum[3] = num[3][MATH] + num[3][PHY] + num[3][CHE] + num[3][BIO] ;
        sum[2] = num[2][MATH] + num[2][PHY] + num[2][CHE] + num[2][BIO] ;
        sum[3] = num[1][MATH] + num[1][PHY] + num[1][CHE] + num[1][BIO] ;
        sum[3] = num[0][MATH] + num[0][PHY] + num[0][CHE] + num[0][BIO] ;
    end

endmodule

