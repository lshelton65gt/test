
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Compares $signed and $unsigned.

module test;

    reg signed [0:5] a;
    reg signed [0:7] b;
    reg signed [0:7] c;

    reg unsigned [0:7] x;
    reg unsigned [0:7] y;
    reg unsigned [0:5] z;

    initial
    begin
        a = -5;
        x = $unsigned (a);
        y = unsigned'(a);
        $display ("a=%d, x=%d, y=%d", a, x, y);

        #1;
        z = 6'b100110;
        b = $signed(z);
        c = signed'(z);
        $display ("z=%b, b=%b, c=%b", z, b, c);

        #1;
        z = 6'b010110;
        b = $signed(z);
        c = signed'(z);
        $display ("z=%b, b=%b, c=%b", z, b, c);
    end

endmodule

