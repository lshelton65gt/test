
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing signedness of packed structures.

module test;

    struct packed { 
        int str_int;
        byte str_byte;
        bit [7:0] str_bit;
    } var0, var1;

    struct packed signed { 
        int str_int;
        byte str_byte;
        bit [7:0] str_bit;
    } var2, var3;

    assign var0 = '0;
    assign var1 = '1;
    assign var2 = '0;
    assign var3 = '1;

    initial
    begin
        #5
        if(var0 < var1)
            $display("Structure unsigned (should be unsigned)");
        else
            $display("Structure signed (should be unsigned)");

        if(var2 < var3)
            $display("Structure unsigned (should be signed)");
        else
            $display("Structure signed (should be signed)");
    end

endmodule

