
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests new data type shortint and system function $bits.

module test(output shortint mod_short);

    initial
    begin
        mod_short = '1;
        #5 mod_short = 'x;
    end

endmodule

module bench;

    shortint mod_short;

    test instan(mod_short);

    initial
    begin
        $monitor("mod_short = %d \n", mod_short);
        #20 $display("n\n%d", $bits(mod_short));
    end

endmodule 

