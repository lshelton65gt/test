
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of casting to various data types.

module test;

    longint i;

    initial
    begin
        i = int'(4294967295);
        #5  i = byte'(i);
    end

    initial
    begin
        $monitor($time,,,"i =%d",i);
    end

endmodule 

