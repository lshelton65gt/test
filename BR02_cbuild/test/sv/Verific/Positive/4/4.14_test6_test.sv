
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test case with casting to structure type.

typedef struct packed {
    bit a ;
    bit [3:0] b ;
} myType ;

typedef bit[$bits(myType) - 1 : 0] secondType ;

module test(input in, output out) ;

    secondType a ;
    bit [3:0]bits ;
    myType abc ;

    assign a = '1 ;

    initial
    begin
        abc = myType'(a) ;
        bits = abc.b ;
    end

    assign out = in ;

    initial
        $display("abc.a = %b    abc.b = %b", abc.a, abc.b) ;

endmodule

