
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A non constant replication operator can be used with
// string, but not with other data type

module test #(parameter i = 3)(output string b) ;
   string str ;
   initial begin 
       str = "hello" ;
       b = {i{str}} ;
   end

endmodule

