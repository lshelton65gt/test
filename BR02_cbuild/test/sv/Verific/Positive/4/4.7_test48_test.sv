
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Check of the conversion methods used on string data type
 

module test ;
   string s = "123", s1 ;
   int i = 23 ;
   initial begin
       $display("The integer value: %d", s.atoi()) ;
       $display("The hex value %d", s.atohex()) ;
       $display("The oct value %d", s.atooct()) ;
       $display("The binary value %d", s.atobin()) ;
       $display("The real value: %d", s.atoreal()) ;

       s1.itoa(i) ;
       $display(" The ASCII decimal representation of i into str: %s", s) ;
       s1.hextoa(i) ;
       $display(" The ASCII hex representation of i into str: %s", s) ;
       s1.octtoa(i) ;
       $display(" The ASCII hex representation of i into str: %s", s) ;
       s1.bintoa(i) ;
       $display(" The ASCII hex representation of i into str: %s", s) ;
       s1.realtoa(i) ;
       $display(" The ASCII hex representation of i into str: %s", s) ;
   end

endmodule

