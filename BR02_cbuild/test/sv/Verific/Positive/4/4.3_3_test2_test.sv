
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the signedness of byte data type.

module test(output byte unsigned mod_byte1, output byte signed mod_byte2);

    initial
    begin
        mod_byte1 = 8'b11101011; 
        mod_byte2 = 8'b10010101;
    end

endmodule

module bench;

    byte unsigned bench_byte1;
    byte signed bench_byte2;

    test instan_mod(bench_byte1, bench_byte2);

    initial
    begin
        #5 $display("Unsigned value = %d, Signed value = %d", bench_byte1, bench_byte2); 
        #5
        if(bench_byte1 > 5)
            $display(" bench_byte1 is unsigned (should be unsigned)");
        else
            $display(" bench_byte1 is signed (should be unsigned)");

        if(bench_byte2 > 10)
            $display(" bench_byte2 is unsigned (should be signed)");
        else
            $display(" bench_byte2 is signed (should be signed)");
    end

endmodule

