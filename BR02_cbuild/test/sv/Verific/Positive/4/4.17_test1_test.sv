
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Attribute instance can be specified for interfaces. The default
// type of the attribute is bit with value 1. If the type of the value is different
// then (like using type casting) then the attribute specification takes that type. 

module test#(parameter p = 2)(input clk, input [7:0] in1, in2, output [7:0] out1, out2) ;

    (* first_attr, second_attr = 1, third_attr = logic'(8-p-1) *) interface iface ;
        logic [7:0] in1 ;
        logic [7:0] in2 ;
        logic [7:0] out1 ;
        logic [7:0] out2 ;

        modport std(input in1, in2, output out1, out2) ;

    endinterface


    iface ifc() ;

    always@(posedge clk)
    begin
        ifc.in1 = in1 & in2 ;
        ifc.in2 = in2 - in1 ;
        ifc.out1 = ifc.in1  ;
        ifc.out2 = ~ifc.in2  ;
    end

    assign out1 = ifc.out1  ;
    assign out2 = ifc.out2  ;

endmodule

