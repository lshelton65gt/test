
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Attribute instance can be specified for interfaces. The default
// type of the attribute is bit with value 1. If the type of the
// value is different then (like using type casting) then the
// attribute specification takes that type. 


function int const_fn(input int i) ;
    return i<<1 ;
endfunction

//(* first_attr, second_attr = const_fn(7), third_attr = logic'(const_fn(8-1-1)) *) interface iface ;
interface iface ;
    logic [7:0] in1 ;
    logic [7:0] in2 ;
    logic [7:0] out1 ;
    logic [7:0] out2 ;

    modport std(input in1, in2, output out1, out2) ;
    assign out1 = in1 & in2 , 
           out2 = in1 | in2 ;
endinterface

module test(input in, output out) ;

   iface I() ;
   dummy D(I) ;
   assign out = in ;

endmodule

module dummy(iface.std ifc) ;
    initial
        $display("ifc.in1 = %1d, ifc.in2 = %1d,  ifc.out1 = %1d, ifc.out2 = %1d", ifc.in1, ifc.in2, ifc.out1, ifc.out2) ;
endmodule
