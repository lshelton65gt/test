
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses string data type to define parameter and ports 
// of function.

module test ;
    parameter string str = manipulate("he", "l") ;
    bot #(str) b1() ;
    initial
       $display("%s", str) ;

    function string manipulate(string a, string b) ;
        string temp ;
        temp = {a, {1 * 2{b}}} ;
        return temp ;
    endfunction

endmodule

module bot #(parameter string s = "heaven" ) ;
    parameter le = s.len() ; 
    bot1 #(le) b2() ;
    initial
       $display("%s", s) ;
endmodule 

module bot1#(parameter p = 21) ;
    int r = cal(p) ;
    function int cal(int p) ;
        return p++ * ++p ;
    endfunction
    initial
       $display("%d", r) ;
endmodule

