
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of casting to various data types.

module test;

    logic i;

    initial
    begin
        i = bit'(1'bx);
        $display(" Output = (should print 0) %d", i);
        i = bit'(1'bz);
        $display(" Output = (should print 0) %d", i);
    end

endmodule

