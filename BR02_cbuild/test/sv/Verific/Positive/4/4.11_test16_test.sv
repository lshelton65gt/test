
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing whether structure can be assigned as a whole.

module test;

    typedef struct { 
        int str_in; 
        shortint str_shortint; 
        logic [31:0] str_reg;
    } my_str;
    my_str var1;

    function void f1(output my_str var1);
        my_str var2;
        var2.str_in = $random;
        var2.str_shortint = $random;
        var2.str_reg = "ABC";
        var1 = var2;
    endfunction

    initial
    begin
        f1(var1);
        $display("Str.string (should be ABC) = %s", var1.str_reg);
    end

endmodule

