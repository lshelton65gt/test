
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the string method tolower(). 

module test(input in, output out) ;

    parameter p1 = func1() ; 

    initial
        $display("p1 = %d", p1) ;

    function integer func1() ;
        string str =  "THIS IS a TEST CASE FOR STRING METHODS." ; 
        string str1 = "this is a test case for string methods." ;
        string ret_str ;
        string str_modified ;
        str_modified = str ;
        ret_str = str.tolower() ;
        if (ret_str == str1 && str_modified == str) // for str.tolower() str is unchanged
            return 11 ;
        else
            return 10 ;
    endfunction

    assign out = in ;

endmodule

