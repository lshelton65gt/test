
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing with enum, where any or more members are assigned
// to the valued returned by constant function call.

module test(input in, output out) ;

    enum {mem1 = const_func(5), mem2 = const_func(0), mem3, mem4 = const_func(10)} var1 ;

    initial
    begin
        $display("Value of the enum members are = (should be 6, 1, 2, 11) %d, %d, %d, %d", mem1, mem2, mem3, mem4) ;
    end

    function int const_func ;
        input int in ;
        const_func = in+1 ;
    endfunction

    assign out = in ;

endmodule

