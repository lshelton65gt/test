
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Check whether $cast() when used as function returns 0.

module test() ;
   int i = 4 ;
   typedef enum{north=4, south, east, west} direction ;
   direction dir ;
   initial
      begin
          $cast(dir, 2 + 5) ;
          dir = direction'(2 + 3) ;
          if($cast(dir, 3 + 5) == 0)
             $display(" Casting failed") ;
      end
endmodule

