
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of shortreal data type.

module test(output shortreal mod_short1);

    assign mod_short1 = -1.23;

endmodule

module bench;

    shortreal mod_short1;

    test instan_mod(mod_short1);

    initial
    begin
        #10
        if(mod_short1 < 0)
            $display(" mod_short1 is signed (should be unsigned)");
        else
            $display(" mod_short1 is unsigned (should be unsigned)");
     end
     
endmodule 

