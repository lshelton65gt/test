
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses string data type to define ports of function
// and parameters. It also tests indexing on string type variables (str[j] = "x")

module test ;
    parameter string str = manipulate("hello") ;

    function string manipulate(string a) ;
        string temp = a ;
        temp[3] = "q" ; 
        return temp ;
    endfunction

    bot #(str) b1() ;
    initial
       $display("%s", str) ;
endmodule

module bot #(parameter string s = "heaven" ) ;
    parameter int le = s.len() ; 
    bot1 #(le) b2() ;
    initial
       $display("%s,   %d", s, le) ;
endmodule 

module bot1#(parameter p = 21) ;
    int r = cal(p) ;
    function int cal(int p) ;
        return p++ / ++p ;
    endfunction
    initial
       $display("%d", r) ;
endmodule

