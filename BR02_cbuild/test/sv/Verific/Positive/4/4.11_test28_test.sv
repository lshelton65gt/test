
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design checks if a parameter of type structure type
// is assigned properly through an assignment pattern.

typedef struct {
    integer c ;
    integer d ;
} check ;

typedef struct {
    integer a ;
    integer b ;
    check C ;
} num ;

module test (input in, output out) ;

    parameter num p = '{1,3,'{5,7}} ;
    parameter integer abc = func1 (10) ; // Value should be 18

    initial #10 $display("Value of parameter abc = %d", abc) ;

    function integer func1 (integer x ) ;
        integer a = p.a + x + p.C.d ;
        func1 = a ;
    endfunction

    assign out = in ;

endmodule

