
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing system functions $signed and $unsigned.

module test;
reg [3:0] mod_reg1, mod_reg2;
byte mod_byte1, mod_byte2;
logic result1, result2;

initial
begin
    mod_reg1 = 8'b1;
    mod_reg2 = 8'b0;
    result1 = (mod_reg1 > mod_reg2);
    result2 = ($signed(mod_reg1) < $signed(mod_reg2));
    if(result1 == ~result2)
        $display("$signed converted both the registers to signed value"); 
end

int un_result1, un_result2;

initial
begin
    un_result1 = -4 * $unsigned(-7);
    un_result2 = -4 * -7;
    if(un_result2 > un_result1)
        $display("\nTesting $unsigned is a success");
    else
        $display("\nTesting $unsigned failed unresult1=%d  unresult2=%d", un_result1, un_result2);
end

endmodule

