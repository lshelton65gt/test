
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of casting to a number of bits and signed types.

module test;
integer in1 = 0;
byte unsigned mod_byte;
initial
begin
    #10 in1 = 8'(1023);
        mod_byte = '1;
    #10 in1 = signed'(mod_byte);
end

integer count = 0;
always @ (in1)
begin
    #11
    if (count == 0)
    begin
      if (in1 == 255) $display ("First assignment is correct");
      else $display ("First assignment is wrong. in1= %d",in1);
    end
    else if (count == 1)
    begin
      if (in1 == -1) $display ("Second assignment is correct");
      else $display ("Second assignment is wrong. in1=%d",in1);
     end
    else 
      $display ("in1 is changing more than expected number of times");

    count++;
end
endmodule

