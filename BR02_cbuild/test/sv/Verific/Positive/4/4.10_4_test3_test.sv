
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test the enum method first(). 

module test(input int in, output out) ;

    parameter p1 = fun1() ; // After static elab p1 = 55
    
    typedef enum integer {msg1 = 1, msg2, msg3, msg4, msg5, msg6, msg7, msg8, msg9, msg10 } Message; 

    function integer fun1() ;
        Message m = m.first ;
        integer ret_value = 0 ;
        integer i ;
        for ( i = 1; i <= 10; i++)
        begin
            ret_value += m ;
            m = m.next ;
        end
        return ret_value ;
    endfunction

    initial
        $display("p1 =%b", p1) ;

    assign out = &in ;

endmodule

