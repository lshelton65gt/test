
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the string method substr(). 

module test(input in, output out) ;

    parameter p1 = func1() ; 
    parameter p2 = func2() ; 
    
    initial
        $display("p1 = %d, p2 = %d", p1, p2) ;

    function integer func1() ;
        string str =  "This is A test case for string methods." ;
        string ret_str ;
        string str1 = "test" ;
        string str_modified ;
        str_modified = str ;
        ret_str = str.substr(10, 13) ;
        if (ret_str == str1 && str_modified == str) // the 2nd cond. is used so 
            return 11 ; 
        else
            return 10 ;    
    endfunction

    function integer func2() ;
        string str =  "This is A test case for string methods." ;
        string ret_str ;
        string str1 = "test" ;
        string str_modified ;
        int i, j ;
        str_modified = str ;

        i = 10 ;
        j = 13 ;
        ret_str = str.substr(i, j) ;
        if (ret_str == str1 && str_modified == str) // the 2nd cond. is used so 
            return 11 ; 
        else
            return 10 ;    
    endfunction

    assign out = in ;

endmodule

