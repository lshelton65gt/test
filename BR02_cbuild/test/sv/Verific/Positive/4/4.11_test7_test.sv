
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a structure and used that structure as data type
// of a parameter. That parameter is overwritten by another parameter of same structure type

typedef struct {
    int a ;
    int b ;
} num ;

module test(input in, output out) ;
    num n = '{a:3, b:5} ;
    parameter num p = '{1,3} ;
    bot #(p) b1() ;
    assign out = !in ;
endmodule

module bot #(parameter num n = '{2,4}) ;
    initial
       $display(n.a) ;
endmodule

