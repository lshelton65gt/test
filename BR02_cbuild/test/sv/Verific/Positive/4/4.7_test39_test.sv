
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design is to test the string method atoreal(). 
// The string is converted until the first non-digit (except first .)is encountered.

module test(input in, output out) ;

    parameter p1 = func() ; 

    initial
        $display("p1 = %d", p1) ;

    function integer func() ;
        string str = "210.00122asee" ;
        string str_modified ;
        real r ;
        str_modified = str ;

        r = str.atoreal() ;
        if (r == 210.00122 && str_modified == str) // the 2nd cond. is used so that str remains unchanged
            return 11 ;
        else
            return 10 ;
    endfunction

    assign out = in ;

endmodule

