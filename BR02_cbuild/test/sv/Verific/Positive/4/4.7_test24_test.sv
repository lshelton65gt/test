
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests built-in function 'octtoa()' on string type 
// variables.

module test ;
    string str ;
    initial
     begin
       str.octtoa(1234) ;
       $display("\n %s", str) ;
     end
endmodule
