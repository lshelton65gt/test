
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses string data type to define parameters
// and ports of a function. Built-in method 'substr' is used within the design. 

module test ;

    parameter string str = manipulate("hello", "o") ;
    bot #(str) b1() ;
    initial
       $display("%s", str) ;

    function string manipulate(string a, string b) ;
        string temp1, temp2 ;
        temp1 = {a,{5*2{b}}} ;
        temp2 = temp1.substr(5, temp1.len()) ;
        return temp2 ;
    endfunction

endmodule

module bot #(parameter string s = "heaven" ) ;
    parameter int le = s.len() ; 
    bot1 #(le) b2() ;
    initial
       $display("%s", s) ;
endmodule 

module bot1#(parameter p = 21) ;
    int r = cal(p) ;
    function int cal(int p) ;
        return p++ * ++p ;
    endfunction
    initial
       $display("%d", r) ;
endmodule

