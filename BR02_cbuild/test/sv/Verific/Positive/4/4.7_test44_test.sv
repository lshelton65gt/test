
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design is to test the string method toupper(). 

module test(input in, output out) ;

    parameter p1 = func1() ;

    initial
        $display("p1 = %d", p1) ;
    
    function integer func1() ;
        string str =  "This is A test case for string methods." ;
        string str1 = "THIS IS A TEST CASE FOR STRING METHODS." ;
        string ret_str ;
        string str_modified ;
        str_modified = str ;
        ret_str = str.toupper() ;
        if (ret_str == str1 && str_modified == str) // for str.toupper() str is unchanged
            return 11 ;
        else
            return 10 ;
    endfunction 

    assign out = in ;

endmodule

