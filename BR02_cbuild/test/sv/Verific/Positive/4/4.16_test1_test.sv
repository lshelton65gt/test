
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Types that can be packed into a stream of bits are called
// bit-stream types. This design tests Bit-stream casting. 

typedef struct {
    shortint address ;
    bit [3:0] code ;
    byte command[2] ;
} Control ;

typedef bit Bits [36:1] ;

module test (input bit clk, shortint add, bit [3:0] cod , byte comd [2], output Control out) ;
    Control pkt ;
    Bits stream[$] ;

    always @ (negedge clk)
    begin
        pkt.address = add ;  // initialize control packet
        pkt.code = cod ;
        pkt.command = comd ;
    
        stream = {stream, Bits'(pkt)} ; // append packet to unpacked queue of bits
    
        out = Control'(stream[0]) ;   // convert stream back to a Control packet
        stream = stream[1:$] ;        // remove packet from stream
    end
endmodule

