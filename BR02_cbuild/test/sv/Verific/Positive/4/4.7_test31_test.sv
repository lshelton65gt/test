
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the string method atohex(). The string
// is converted until the first non-hex character is encountered.

module test(input byte in, output out) ;
      
    function integer func1() ;
        string str = "Sreyoshi" ;
        string str_modified ;
        integer i ;
        str_modified = str ;

        i = str.atohex() ;
        if (i == 2587 && str_modified == str) 
            return 11 ;
        else
            return 10 ;
    endfunction

    assign out = in ;
endmodule

