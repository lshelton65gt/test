
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of constant ranged enum declaration.

module test;

    enum bit [3:0] { mem1, mem2, mem3 } my_type;

    initial $display("Sizes of the members are (Should be all 4) %d %d %d", $bits(mem1), $bits(mem2), $bits(mem3),); 

endmodule

