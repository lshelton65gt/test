
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): User defined types can be created using typedef. This design
// defines a structure as a user defined type and declares object and assign values
// to the structure items.

module test ;
    typedef struct {
        logic [63:0] str_reg; 
        bit          str_bit;
    } my_str;

    my_str str;

    initial
    begin
        str.str_reg = "Hellow!";
        str.str_bit = 0;
        $display("Should print 'Hellow!' thrice:"); // once for initializing str.str_bit and them changing it
        repeat(2) #5 str.str_bit = ~ str.str_bit; 
    end

    always @(str.str_bit)
    begin
        $display("%s\n", str.str_reg) ;
    end
endmodule

