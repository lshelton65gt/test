
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Compares a+b, a+b+0, 3'(a+b). a, b are both 2 bit numbers.
// This design checks the carry/overflow bit by shifting one bit right.

module test;

reg [0:1] o1, o2, o3, o4;
reg [0:1] a, b;

initial
begin
    a = 2;
    b = 2;
    o1 = (a+b) >> 1;
    o2 = (a+b+0) >> 1;
    o3 = 3'(a+b) >> 1;
    o4 = (3'(a) + b)>> 1;

    $display ("o1=%b, o2=%b, o3=%b o4=%b",o1, o2, o3, o4);
end

endmodule

