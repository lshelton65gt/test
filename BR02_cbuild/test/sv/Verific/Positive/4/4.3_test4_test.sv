
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests data type string.

module test;

    string mod_str = "ABC";

    initial
    begin
        #5 $display("Input string :");
           $display("%s",mod_str);
    end

endmodule

