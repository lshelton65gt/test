// Modified by Carbon to eliminate the multiple packed dimensions
// in the enum base type. Only one packed dimension is allowed.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing enum with multiple packed dimension ranges.

module test ;
    typedef bit[63:0] myType;
    typedef enum myType { name = "SumantaP", hobby, occupation } person;

    person p ;

    initial
    begin
           $display("%s\n", p.first());
           $display("%s\n", p.next());
    end

endmodule

