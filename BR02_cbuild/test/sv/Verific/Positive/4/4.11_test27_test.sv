
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing unpacked union, having members of different sizes.

typedef union { 
    int a;
    bit [3:0]b;
    real c;
} myType;

module test ;

    myType var1;

    assign var1.a = '1;

    initial 
        $display("Maximum size of the member in the current union is = %d (should be 64) bits", $bits(var1));

endmodule

