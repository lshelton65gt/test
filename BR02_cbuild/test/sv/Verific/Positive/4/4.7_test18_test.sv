
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests function 'atoi()' on string type variables

module test ;
    parameter int p1 = manipulate("hello", "world"), p2 = 3 ;
    bot #(p1) b1(3) ;
    initial
       $display("%d", p1) ;

    function int manipulate(string a, string b) ;
        int temp ;
        string s ;
        s = {a,b} ; 
        temp = s.atoi() ;    
        return temp ;
    endfunction

endmodule

module bot #(parameter int s = 2)(input int num) ;
    localparam le = product(s) ; 
    bot1 #(le) b2() ;
    initial
       $display("%d", s) ;
    function product(input in) ;
        product = in >> 1 ;
    endfunction
endmodule 

module bot1#(parameter int p = 21) ;
    int r = cal(p) ;
    function int cal(int p) ;
        return p++ * ++p ;
    endfunction
    initial
       $display("%d", r) ;
endmodule

