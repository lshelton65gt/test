
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing single bit signed object.

module test;

    bit signed modBit;

    assign modBit = 1'b1;

    initial
    begin
        logic result = (modBit > 0);
        if(result == 0)
            $display("Testing is successful");
        else
            $display("Testing failed");
    end

endmodule

