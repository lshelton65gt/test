
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the new data type byte.

module test(output byte mod_byte);

    initial
    begin
        mod_byte = '1;
        #5 mod_byte = 1'bz;
    end

endmodule

module bench;

    byte bench_byte;

    test instan_mod(bench_byte);

    initial
    begin 
        $monitor(" Value = %d", bench_byte);
        #10 $display(" \n\nsizeof(byte) = %d bit",$bits(bench_byte));
    end

endmodule

