
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an enum literal as a constant, and  assigns
// it to a module parameter.

typedef enum {red, yellow, blue, green, white} colors ;

module test (input in, output out) ;
    colors C = red ;
    parameter colors e = blue ;
    bot #(e) b1() ;
    initial
       $display("%s", e.name) ;
    assign out = in ;
endmodule

module bot #(parameter colors c = yellow ) ;
    parameter le = c ; 
    bot1 #(le) b2() ;
    initial
       $display("%s", c.name) ;
endmodule 

module bot1#(parameter p = 21) ;
    int r = cal(p) ;
    function int cal(int p) ;
        return p++ * ++p ;
    endfunction
    initial
       $display("%d", r) ;
endmodule

