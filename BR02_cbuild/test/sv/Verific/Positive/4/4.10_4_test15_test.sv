
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the enumeration method prev(int unsigned N ). 

module test(input in, output out) ;

    typedef enum {Red = 5, Green, Blue, Yalow, Pink, Black, Orange} Colors ;
    typedef enum {Mo = 7,Tu,We,Th,Fr,Sa,Su} Week ;
    const int N = 2 ;

    parameter p1 = func1() ; 
    parameter p2 = func2() ; 
    
    function int func1() ;
        Colors c = c.first ;
        c = c.next(3) ;
        c = c.prev(N) ;
        return c ;
    endfunction

    function int func2() ;
        Week w = w.first ;
        w = w.next(5) ;
        w = w.prev(N+1) ;
        return w ;
    endfunction

    initial
        $display("p1 =%b    p2 =%b", p1, p2) ;

    assign out = in ;

endmodule

