
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): User defined types can be created using typedef. This design
// defines a forward typedef and use that to declare an object before actual definition.
// Then it assigns value. After this comes the actual definition of the user defined type.

module test;

    typedef my_int;

    my_int i = 1;

    typedef int my_int;

    initial
        $display("(Should print 1)\t%d",i);

endmodule

