
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines two structures, second structure contains the 
// first structure as one element. This structure is used as data type of a parameter. 
// This structure type parameter is overwritten by instantiation

typedef struct {
    int c ;
    int d ;
} check ;

typedef struct {
    int a ;
    int b ;
    check C ;
} num ;

module test ;
    num n = '{a:3, b:5, C:'{1, 2}} ;
    parameter num p = '{1,3,'{5,7}} ;
    bot #(p) b1() ;
endmodule

module bot #(parameter num n = '{2,4,'{6,8}}) ;
    initial
       $display(n.a) ;
endmodule

