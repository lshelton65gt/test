
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The value of the enum exceeds, since static SystemVerilog
// cast operation used so no value is printed in c.name

module test ;

    typedef enum {
        black, 
        white, 
        pink, 
        red, 
        blue
    } color;
    color c ;

    initial
    begin
         if ($cast(c, 10))
             $display("%s",c.name()) ;
         else
             $display("Error in cast") ;
    end

endmodule

