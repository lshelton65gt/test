
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of structure declaration.

module test(output logic [31:0] Idc, Vdc, input logic [31:0] Vm, Rf, Rl) ;

    struct {
        real peak_volt;
        real forward_res ;
        real load_res ;
        real peak_curr ;        
    } rectifier_params ;

    always @(Vm, Rf, Rl)
    begin
        rectifier_params.peak_volt = Vm;
        rectifier_params.forward_res = Rf;
        rectifier_params.load_res = Rl ;
    
        rectifier_params.peak_curr =  rectifier_params.peak_volt / 
                     (rectifier_params.forward_res + rectifier_params.load_res) ;

        Idc = rectifier_params.peak_curr / 3.14 ;
        Vdc = Idc * rectifier_params.load_res ;    
    end

endmodule

