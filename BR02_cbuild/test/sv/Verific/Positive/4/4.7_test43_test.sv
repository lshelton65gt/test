
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design is to test the string method getc(). 

module test(input in, output out) ;

    parameter p1 = func1() ;  
    parameter p2 = func2() ;  
    parameter p3 = func3() ;  
    parameter p4 = func4() ;  

    initial
        $display("p1 = %d, p2 = %d, p3 = %d, p4 = %d", p1, p2, p3, p4) ;

    function integer func1() ;
        string str = "This is A test case for string methods." ;
        int i = 8 ;
        int ret_value ;
        ret_value = str.getc(i) ; // using variable
        if (ret_value == 65)
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func2() ;
        string str = "This is A test case for string methods." ;
        int ret_value ;
        ret_value = str.getc(8) ; // using int const. value
        if (ret_value == 65)
            return 11 ;
        else
            return 10 ;
    endfunction
    

    function integer func3() ;
        string str = "This is A test case for string methods." ;
        int i = 8 ;
        byte ret_byte ;
        ret_byte = str.getc(i) ; // using variable
        if (ret_byte == "A")
            return 11 ;
        else
            return 10 ;
    endfunction
    
    function integer func4() ;
        string str = "This is A test case for string methods." ;
        byte ret_byte ;
        ret_byte = str.getc(8) ; // using int const. value 
        if (ret_byte == "A")
            return 11 ;
        else
            return 10 ;
    endfunction
    
endmodule

