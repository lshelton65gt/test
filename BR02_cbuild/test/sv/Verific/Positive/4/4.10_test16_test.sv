
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the value assigned to unassigned member.

module test;

    enum { mem1 = 5, mem2, mem3 } var1;

    initial
        $display("Value of the unassigned member = %d (should be 6)", mem2);

endmodule

