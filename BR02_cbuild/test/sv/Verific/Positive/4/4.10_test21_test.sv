
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test assignment of non-enum type expression to an enum type
// variable using a cast.

typedef enum { a, b, c, d } enum_type ;

module test(input integer in1, in2, in3, input enum_type option, output enum_type o) ;

    always @(option, in1, in2, in3)
    begin
        case (option)
        a : o = enum_type'(in1) ;
        b : o = enum_type'(in2) ;
        default : o = enum_type'((in1 + in2 - in3) % 4) ;
        endcase
    end

endmodule

