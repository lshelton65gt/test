
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a structure and uses that structure as
// data type of ports.

typedef struct { bit [0:3] f1, f2 ; } MY_TYPE ;

module test(input in, output out) ;

    MY_TYPE a, b ;

    initial 
    begin
        a.f1 = 4'b0001 ;
        a.f2 = 4'b0010 ;
        #1 ;
        a.f1 = 4'b0011 ;
        a.f2 = 4'b0110 ;
    end

    initial
        $monitor("%b %b %b %b", a.f1, a.f2, i1.a.f1, i1.a.f2) ;

    initial
        #1001 $finish ;

    bot i1 (.in1(a), .a(b)) ;

    assign out = in ;

endmodule

module bot(input MY_TYPE in1, output MY_TYPE a) ;

    always @ (*)
        a = in1 ;

endmodule

