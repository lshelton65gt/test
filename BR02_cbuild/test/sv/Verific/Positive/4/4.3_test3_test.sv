
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test the default signed property of integer data type.

module test(output reg out1, out2, out3, out4) ;

    integer i1, i2 ;
    integer unsigned i3, i4 ;

    initial
    begin
        i1 = -12/3;
        i2 = -2 * 3;
        out1 = i1 < 0;
        out2 = i2 < 0;
        i3 = -12/3;
        i4 = -2 * 3;
        out3 = i3 < 0;
        out4 = i4 < 0;
     end

endmodule

