
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing signedness of shortint data type.

module test; 

    shortint signed mod_short1;
    shortint unsigned mod_short2;

    initial
    begin
        mod_short1 = '1;
        mod_short2 = 5;
        mod_short1 = mod_short1*(-1);
        mod_short2 = mod_short2*(-1);
    end

    initial
    begin
        #5
        if(mod_short1 < 5)
            $display(" mod_short1 is signed (should be signed)");
        else
            $display(" mod_short1 is unsigned (should be signed)");
        if(mod_short2 > 5)
            $display(" mod_short2 is unsigned (should be unsigned)");
        else
            $display(" mod_short2 is signed (should be unsigned)");
    end

endmodule

