
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test the enumeration method next(int unsigned N ). 

module test (input int in, output out) ;

    typedef enum {Red = 5, Green, Blue, Yalow, Pink, Black, Orange} Colors ;
    typedef enum {Mo = 7,Tu,We,Th,Fr,Sa,Su} Week ;
    const int N = 2 ;

    parameter p1 = func1() ; 
    parameter p2 = func2() ; 

    initial
        $display("parameter p1 = %1d   p2 = %1d", p1, p2) ;
    
    function int func1() ;
        Colors c = c.first ;
        c = c.next(N) ;
        return c ;
    endfunction

    function int func2() ;
        Week w = w.first ;
        w = w.next(N+1) ;
        return w ;
    endfunction

    assign out = $onehot(in) ;

endmodule

