
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing advantage of casting to a number of bits, preserving
// of bit pattern.

module test;

shortreal mod_short1 ;
shortreal mod_short2;
int mod_int;
int i;

initial
begin
    mod_short1 = -2.5;
    for (i = 0; i < 10; i++)
    begin
        mod_int = int'(mod_short1);
        mod_short2 = shortreal'(mod_int);
        $display ("int typecasting changed %f to %f", mod_short1, mod_short2);

        if( mod_short1 == mod_short2)
            $display("Success, bit pattern is preserved by casting to number of bits");
        else
            $display("Failure, bit pattern can not be preserved by casting to a number of bits");

        #1 mod_short1 -= 3.2;
    end
end

endmodule

