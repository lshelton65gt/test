
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing an user defined structure inside for-generate block.

module test;

    parameter size = 8;

    genvar i;

    generate
        for(i = 0; i < size; i++)
        begin: blk
            typedef struct packed {
                bit[i:0] strBit;
                int strInt;
            } myType;     
            myType var1;
            assign var1 = '1;
        end

        for(i = 0; i < size; i++) 
        begin: dis
            initial
                $display("Size of the structure member whose vector width is a function of genvar, is = %d", $bits(blk[i].var1.strBit));
        end
    endgenerate

endmodule

