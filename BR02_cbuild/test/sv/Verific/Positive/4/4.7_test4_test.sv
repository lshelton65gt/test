
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): String data type dynamically resize itself to store the 
// content of the string literal assigned to it. Here a large string is assigned
// after small one to recognize the string size increasing.

module test(input in, output out) ;

   string a ;
   int len1, len2 ;

   initial begin
       a = "A" ;
       len1 = a.len() ;
       $display("Value: %s, and size: %d", a, a.len()) ;

       a = "Small to Big" ;
       len2 = a.len() ;
       $display("Value: %s, and size: %d", a, a.len()) ;

       if (len2 > len1) 
           $display("String size increased") ;
       else
           $display("String size reduced") ;
   end

   assign out = in ;

endmodule

