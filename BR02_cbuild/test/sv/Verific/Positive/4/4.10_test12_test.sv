
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The values can be set for some of the enumeration literals 
// and not set for other literals. A literal without a value is automatically 
// assigned an increment of the value of the previous literal.

typedef enum {a=3, b=7, c} alphabet;
module test ;
    alphabet a ;
    initial begin
        a = a.first() ;
        forever begin 
                $display( "%s : %d\n", a.name(), a );
                if( a == a.last() ) break;
                a = a.next();
        end 
    end
endmodule

