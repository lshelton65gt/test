
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): User defined types can be created using typedef. This design
// tests typedef declaration after their forward declaration and uses. The actual
// type is real here.

module test (output real out, input real in) ;

    typedef my_real ; // forward declaration

    my_real real1 ;

    always @(in)
    begin
        real1 = in ;
        out = - real1 ;
    end

    typedef real my_real ; // actual declaration

endmodule

