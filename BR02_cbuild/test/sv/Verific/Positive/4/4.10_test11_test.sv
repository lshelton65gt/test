
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of scope rule, when an object and an enum literal 
// of same name are declared in different scopes.

module test;

enum {mem1, mem2, mem3, mem4, mem5}var1;

initial
begin
    var1 = mem5;
    for(int mem1 = 0; mem1 < mem5; mem1++)
        $display("%d", mem1);
end

endmodule

