
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of casting to various data types.

module test;

    integer i;

    initial
    begin
        i = shortint'(65536);
        #5  i = byte'(129);
    end

    initial
    begin
        $display("Should display 0 at time 0, -127 at time 5");
        $monitor($time,,,"i =%d",i);
    end

endmodule 

