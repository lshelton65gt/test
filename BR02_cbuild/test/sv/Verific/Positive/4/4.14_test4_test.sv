
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test if information is lost when shortreal is converted to int.

module test(output reg result, input shortreal in) ;

    int i;
    shortreal tmp;

    always @(in)
    begin
        i = int'(in) ;
        tmp = shortreal'(i);
        if(tmp == in)
            result = 1;
        else
            result = 0;                
    end

endmodule

