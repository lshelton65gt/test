
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test case on passing an union to and from a task.

module test;

    typedef union {
        longint uni_long;
    } my_uni;

    my_uni var1, var2;

    task my_task;
        input my_uni var1;
        output my_uni var2;
        var2 = var1;
    endtask

    initial
    begin
        var1.uni_long = "HELLO!";
        my_task(var1, var2);
        $display("Output string = (should be HELLO!) %s ", var2.uni_long);
    end

endmodule

