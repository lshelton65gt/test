
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of user defined enum types.

module test;

    typedef enum {YES, NO} bool;
    bool boolean;

    initial
    begin
          boolean = YES;
       #5 boolean = NO;
    end

    always @(boolean)
        case(boolean)
             YES:$display("YES\n");
             NO: $display("NO");
        endcase

endmodule

