
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests built-in function 'itoa()' on string data type.

module test ;

    string str ;
initial 
    str.itoa(1234) ;

    initial
       $display("\n %s", str) ;

endmodule


