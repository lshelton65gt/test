
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Use genvar in the assigned value of enum members 

module test(output int sum[3:0], 
                 input int num[3:0][4:1]) ;

    generate 
        genvar i ;
        for(i = 4; i > 2; i--)
        begin : b
            if(i == 4)
            begin
                enum  
                {
                    MATH = i,
                    PHY  = i - 1,
                    CHE  = i - 2,
                    BIO  = i - 3 
                } subject ;    
            end
            else
            begin
                always @(*)
                begin
                    sum[3] = num[3][b[4].MATH] + num[3][b[4].PHY] + num[3][b[4].CHE] + num[3][b[4].BIO] ;
                    sum[2] = num[2][b[4].MATH] + num[2][b[4].PHY] + num[2][b[4].CHE] + num[2][b[4].BIO] ;
                    sum[1] = num[1][b[4].MATH] + num[1][b[4].PHY] + num[1][b[4].CHE] + num[1][b[4].BIO] ;
                    sum[0] = num[0][b[4].MATH] + num[0][b[4].PHY] + num[0][b[4].CHE] + num[0][b[4].BIO] ;
                end
            end
       end
    endgenerate    

endmodule

