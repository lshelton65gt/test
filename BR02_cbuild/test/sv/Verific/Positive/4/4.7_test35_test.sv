
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests equality operators on strings.

module test() ;

    string s1 = "hello world" ;
    string s2 = "world" ;
    string s3 = {"hello ",s2} ;

    initial 
    begin
        if(s1 == s3)
            $display("Same") ;
        else if(s1 != s2)
            $display("Not Equal") ;
        else if(s2 < s1)
            $display("s2 less than s1") ;
 
        $display("\n String replicated by 2: %s ", {2{s2}}) ;
    end

endmodule
      
