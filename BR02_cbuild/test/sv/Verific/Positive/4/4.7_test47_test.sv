
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the string method compare(). 

module test(input int in, output out) ;
    parameter p1 = func1() ;  
    parameter p2 = func2() ;  
    parameter p3 = func3() ;  
    parameter p4 = func4() ;  

    initial
        $display("p1 = %d, p2 = %d, p3 = %d, p4 = %d", p1, p2, p3, p4) ;

    function integer func1() ;
        string str1 =  "ABCD" ;
        string str2 = "abcd" ;
        if ( str1.compare(str2) < 0 )
            return 11 ;
        else
            return 10 ;
    endfunction

    function integer func2() ;
        string str1 =  "ABCD" ;
        if ( str1.compare(str1) == 0 )
            return 11 ;
        else
            return 10 ;
    endfunction
    
    function integer func3() ;
        string str1 =  "ABCD abcd" ;
        string str2 =  "ABCD abcd" ;
        if ( str1.compare(str2) == 0 )
            return 11 ;
        else
            return 10 ;
    endfunction
    
    function integer func4() ;
        string str1 =  "ABCD" ;
        string str2 = "abcd" ;
        if ( str2.compare(str1) > 0 )
            return 11 ;
        else
            return 10 ;
    endfunction
    
    assign out = |in ;

endmodule

