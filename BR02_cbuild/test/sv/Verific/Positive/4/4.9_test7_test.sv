
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): User defined types can be created using typedef. This design
// tests typedef declaration with structure and union and uses the structure or
// union elements individually to assign values to them.

module test ;

    typedef struct {
        bit [7:0] bit1;
        int  int1 ;
        shortreal real1 ;
    } struct1 ;

    typedef enum { sun, mon, tue, wed, thu, fri, sat} days ;

    struct1 st = '{ 8'b10101011, 67, 7.8 } ;

    days my_day = wed ;

    typedef union {
        int a;
        real r;
    } my_union ;
    
    my_union un1, un2 ;
   
    assign un1.a = st.int1 ,
           un2.r = st.real1 ;

endmodule

