
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing void return type of function.

function void f1;
    input in;
    $display("Void Function");
endfunction

module test;
    initial f1(1'b1);
endmodule

