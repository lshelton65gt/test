
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): In a packed union if any member is of type 4-state, the
// whole union is then 4-state. This design defines a packed union and defines two
// members, one of 2-state type and the other of 4-state type.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef union packed
    {
        logic [7:0] memb1;
        bit [7:0] memb2;
    } t_ps_un;

    t_ps_un un1, un2, un3, un4;

    always@(posedge clk)
    begin
        un1.memb1 = 'x;
        un2.memb1 = 'z;

        if (un1 === 8'bx)
        begin
            un3.memb1 = in2;
            un4.memb2 = ~(in1 + in2);
        end
        else
        begin
            if (un2 === 8'bz)
            begin
                un3.memb2 = in1 - in2;
                un4.memb1 = un3.memb1;
            end
            else
            begin
                un3 = '0;
                un4 = '0;
            end
        end

        out1 = un4 & un3;
        out2 = un3 ^ un4;
    end

endmodule

