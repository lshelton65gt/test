
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing structure member definition inside a structure.

typedef struct {
    int strInt ;
    struct {int a ; real b ;}var1 ;
} myType ;

module test(input [0:31] in, output out) ;

    myType a = '{5, '{5, 1.0}} ;

    initial
    begin
        $monitor("integer1 = %d, integer2 = %d, real1 = %d", a.strInt, a.var1.a, a.var1.b) ;
        repeat(20)
        begin
            a.strInt = $random ;
            a.var1.a = $random ;
            a.var1.b = {2{$random}} ;
        end
    end

    assign out = |in ;

endmodule

