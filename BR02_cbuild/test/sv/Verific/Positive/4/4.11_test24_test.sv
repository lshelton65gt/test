
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing default sign (unsigned) of packed structures.

module test;

    struct packed { 
        int str_int;
        byte str_byte;
        bit [7:0] str_bit;
    } var1, var2;

    assign var1 = '0;
    assign var2 = '1;

    initial
    begin
         #5
         if(var1 < var2)
             $display(" Structure is unsigned (should be unsigned)");
         else
             $display(" Structure is signed (should be unsigned)");
    end

endmodule

