
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses string in constant function evaluation. 
// This design also defines a enumeration and default value of first enumeration
// literal is specified by constant function call.

module test(input int in, output out) ;

    parameter string str = manipulate("he", "l") ; // str = hell
    bot #(str) b1() ;
    initial
       $display("%s", str) ;

    function string manipulate(string a, string b) ;
        string temp ;
        temp = {a,{2{b}}} ;
        return temp ;
    endfunction

    assign out = |in ;

endmodule

module bot #(parameter string s = "heaven" ) ; // s = hell

    parameter le = s.len() ; // le = 4
    bot1 #(le) b2() ;
    initial
       $display("%s", s) ;
endmodule

module bot1#(parameter p = 21) ; // p = 4
    
    typedef enum {red = cal(p), yellow, blue, green, white} colors ;

    int r = cal(p) ;

    function int cal(int p) ; // p = 4
        return (p++) * (++p) ; // 4 * 6 = 24
    endfunction

    initial
       $display("%d", r) ;

endmodule

