
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A struct is typedefed and used within 'always_comb' and 'always'
// blocks of different modules.

typedef struct packed{
    logic [0:3] x;
    logic [0:3] y;
}dtype;

module test(input int in, output out) ;

    reg [0:3] a;
    reg [0:3] b;
    wire [0:3] c;

    bot i(a, b, c);

    initial
    begin
        a = 4'b0011;
        b = 4'b0111;
        #1;
        $display ("%b %b %b", a, b, c);
        #1;
        a = 4'b0011;
        b = 4'b0011;
        #1;
        $display ("%b %b %b", a, b, c);
        #1;
        a = 4'b0011;
        b = 4'b0110;
        #1;
        $display ("%b %b %b", a, b, c);
        #1;
        a = 4'b0011;
        b = 4'b0010;
        #1;
        $display ("%b %b %b", a, b, c);
        #1;
    end

    assign out = $onehot(in) ;

endmodule


module bot (in1, in2, out1);

    input [0:3] in1, in2;

    output reg [0:3] out1;
    dtype dout;

    always_comb
    begin
            out1 = ((dout.x - dout.y)>>1);
    end

    bot1 b11(in1, in2, dout);

    endmodule

    module bot1(in1, in2, out1);
    input [0:3] in1, in2;
    output dtype out1;

    assign out1 = {in1 + in2, in1 - in2};

endmodule

