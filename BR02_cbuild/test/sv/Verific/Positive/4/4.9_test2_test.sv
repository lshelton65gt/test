
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): User defined types can be created using typedef. This design
// defines two user defined types and declares object and assign values to them.

module test ;

    typedef int my_int;
    typedef bit[31:0] my_bit;
    my_int mod_in;
    my_bit mod_bit;

    initial
    begin
        mod_in = 0;
        mod_bit = "ABC";
        $display("My_type_int (should be 0) = %d, My_type_bit (should be ABC) = %s", mod_in, mod_bit);
    end

endmodule 

