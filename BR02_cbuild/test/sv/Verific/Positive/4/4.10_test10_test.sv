
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines an enumeration type and declares a 
// packed array of that type.

module test;
typedef enum bit {mem0, mem1} my_type;
my_type [2:0]var1;
initial
  begin
	var1[0] = mem0;
	var1[1] = mem1;
	
        $display ("%b", var1);
  end
endmodule

