
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing initialization of a structure member.

typedef struct {
    int a = 5;
    bit [3:0]strBit = 4'b1010;
} myType;

module test;

    myType a;

    initial
        $display("Default initial value of the structure members are (should be 5, 1010) %d & %b", a.a, a.strBit);

endmodule

