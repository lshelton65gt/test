
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of structure and union.

module test ;

    struct {
        bit [7:0] bit1 ;
        byte byte1 ;
    }  st ;

    initial begin
        st.bit1 = 8'b10101010 ;
        st.byte1 = 4 ;
    end

    union {
        bit [7:0] bit1 ;
        byte byte1 ;
    } un ;

    assign un.bit1 = 1 ;

endmodule

