
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Usage of various methods of string data type.

module test(input int in, output out) ;

    string s ;
    int i ;

    initial 
    begin
        $display("The 3 rd character is: %c", s.getc(3) ) ;
        $display("The UPPER case %s", s.toupper()) ;
        $display("The LOWER case %s", s.tolower()) ;
        $display("The sensitive compare with hello is %d", s.compare("hello")) ;
        $display("The insensitive compare with HEllo %d", s.icompare("HEllo")) ;
        $display("The sub string is  %s", s.substr(2,5)) ;
    end

    assign out = &in ;

endmodule

