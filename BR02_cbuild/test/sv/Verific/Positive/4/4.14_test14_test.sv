
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of casting to an user defined data type.

module test;
typedef struct packed {
    int str_int;
    logic [63:0] str_reg;
}  my_str;
my_str str1, str2;
typedef bit [$bits(str1) - 1 : 0] my_bit;
initial
begin
    str1.str_int = 10;
    str1.str_reg = "Hellow!";
end
my_bit mod_bit = my_bit'(str1);
initial
begin
   #20 str2 = my_str'(mod_bit);
   if (str1 == str2)
        $display(" Casting to an user defined data type is successful");
end
endmodule

