
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design is to test the string method atobin(). 
// If the string starts with a non binary then it returns 0

module test(input int in, output out) ;

    parameter p1 = func1() ; 

    initial
        $display("p1 = %d", p1) ;

    function integer func1() ;

        string str = "21000122" ; 
        string str_modified ;
        integer i ;
        str_modified = str ;
        i = str.atobin() ;
        
        if (i == 0 && str_modified == str) 
            return 11 ;
        else
            return 10 ;
    endfunction

    assign out = &in ;

endmodule

