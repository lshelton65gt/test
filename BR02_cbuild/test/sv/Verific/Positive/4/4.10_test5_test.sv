
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Assign constant literal to enum members.

module test(output int sum[3:0], 
                 input int num[3:0][-4:-1]) ;

    enum logic signed [3:0] 
    {
         MATH = 4'hc,
         PHY  = 4'hd,
         CHE  = 4'he,
         BIO  = 4'hf
    } subject ;    

    always @(*)
    begin
        sum[3] = num[3][MATH] + num[3][PHY] + num[3][CHE] + num[3][BIO] ;
        sum[2] = num[2][MATH] + num[2][PHY] + num[2][CHE] + num[2][BIO] ;
        sum[3] = num[1][MATH] + num[1][PHY] + num[1][CHE] + num[1][BIO] ;
        sum[3] = num[0][MATH] + num[0][PHY] + num[0][CHE] + num[0][BIO] ;
    end

endmodule

