
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of shortreal data type.

module test(output shortreal out1, out2,
            output int shortreal_size) ;

    assign out1 = 3.4e37 ,
           out2 = 4.5e39 ,
           shortreal_size = $bits(out1) ;

endmodule

