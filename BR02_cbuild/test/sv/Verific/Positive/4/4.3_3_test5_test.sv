
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing signedness of byte data type.

module test(mod_byte1, mod_byte2);

    output byte signed mod_byte1;
    output byte unsigned mod_byte2;

    initial
    begin
        mod_byte1 = '1;
        mod_byte2 = '1;
    end

endmodule

module bench;

    byte signed bench_byte1;
    byte unsigned bench_byte2;

    test instan_mod(bench_byte1, bench_byte2);

    initial
    begin
        #10
        if(bench_byte1 < 0)
            $display(" bench_byte1 is signed (should be signed)");
        else
            $display(" bench_byte2 is unsigned (should be signed)");
    
        if(bench_byte2 < 0)
            $display(" bench_byte2 is signed (should be unsigned)");
        else
            $display(" bench_byte2 is unsigned (should be unsigned)");
    end

endmodule

