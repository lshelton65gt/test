
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of 2-state data type int and 4-state data type integer.

module test(output reg eq1, eq2) ;

    integer intA, intB;
    int intAA, intBB;

    initial
    begin
        intA  = -12/3;
        intAA = -12/3;
        eq1   = (intA == intAA);
        intB  = -'sbx101;
        intBB = -'sbx101;
        eq2   = (intB == intBB); 
    end

endmodule

