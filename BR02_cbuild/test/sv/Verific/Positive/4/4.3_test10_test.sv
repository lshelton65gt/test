
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of new data type logic.

module test(mod_logic);

    output logic mod_logic;

    initial
    begin
        mod_logic = 2;      // value stored is '0' of '10'(the binary value of 2)
        #5 mod_logic = 1'bx;
        #5 mod_logic = 1'bz;
    end

endmodule

module bench;

    logic bench_logic;

    test instan_test(bench_logic);

    initial
    begin
        $monitor(" Value = %b  ", bench_logic);
        #20 $display(" sizeof(logic) = %d", $bits(bench_logic));
    end

endmodule

