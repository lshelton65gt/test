
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of typedef within interface.

interface my_interface;
    typedef bit my_bit;
endinterface

module test;

    my_interface mod_interface();
    typedef mod_interface.my_bit my_bit;
    my_bit mod_bit;

    initial
    begin    
        mod_bit = 1'b0;
        $display("(Should print 0)\t%b",mod_bit);
    end

endmodule

