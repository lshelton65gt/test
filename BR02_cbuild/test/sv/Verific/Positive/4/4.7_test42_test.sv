
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design is to test the string method putc(). 

module test(input in, output out) ;

    byte c = "A" ;
    string str1 = "This is a test case for string methods." ;
    initial
    str1.putc(9, c) ;

    initial
          $display("P1 = %s", str1) ;
endmodule

