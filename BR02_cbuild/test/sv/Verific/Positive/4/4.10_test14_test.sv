
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing the scopes of enum literals.

enum {GREEN, RED} colors;

module test ;

    parameter p = GREEN;
    enum {RED=12, GREEN=p} colors;

    initial #5 $display ("%d", GREEN);  // Value of GREEN should be '0'

endmodule

