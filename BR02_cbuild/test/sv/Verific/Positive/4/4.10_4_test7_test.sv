
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test the enum method prev( N ) where N exceeds 
// the size of enumeration. 

module test(input in, output out) ;

    typedef enum {Red = 5, Green, Blue, Yalow, Pink, Black, Orange} Colors ;
    typedef enum {Mo = 7,Tu,We,Th,Fr,Sa,Su} Week ;

    parameter p1 = func1() ; 
    parameter p2 = func2() ; 

    function int func1() ;
        Colors c = c.first ;
        c = c.next(4) ;
        if (c.name == "Pink")
            return 1 ;
        else 
            return 0 ;
    endfunction

    function int func2() ;
        Week w = w.first ;
        w = w.prev(5) ;
        if (w.name == "We")
            return 1 ;
        else 
            return 0 ;
    endfunction

    initial
        $display("p1 =%b,   p2 =%b", p1, p2) ;

    assign out = !in ;

endmodule

