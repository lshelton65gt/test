
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the new data type longint.

module test(mod_long);

    output longint mod_long;

    initial
    begin
        mod_long = '1;
        #5 mod_long = 1'bx;
    end

endmodule

module bench;

    longint bench_long;

    test instan_mod(bench_long);

    initial
    begin
        $monitor("Longint = %d",bench_long);
        #10 $display(" \n\n sizeof(longint) = %d",$bits(bench_long));
    end

endmodule

