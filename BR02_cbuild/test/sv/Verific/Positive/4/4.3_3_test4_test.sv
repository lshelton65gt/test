
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing signedness of int data type.

module test(output int signed in1, output int unsigned in2);

    initial
    begin
        in1 = '1;
        in2 = '1;
    end

endmodule

module bench;

    int signed bench_in1;
    int unsigned bench_in2;

    test instan_mod(bench_in1, bench_in2);

    initial
    begin
        #10
        if(bench_in1 <= -1)
            $display(" bench_in1 is signed (should be signed)");
        else
            $display(" bench_in1 is unsigned (should be signed)");

        if(bench_in2 <= -1)
            $display(" bench_in2 is signed (should be unsigned)");
        else
            $display(" bench_in2 is unsigned (should be unsigned)");
    end

endmodule

