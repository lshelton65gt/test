
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of the in-built methods defined on enumeration variables.
// The elements are printed using first, last and next methods. If the first name is not
// assigned a value, it is given the initial value of 0.

module test ;
   enum{green, white, blue, safforn} col ;

   initial begin
       col = col.first() ;
       forever begin
            $display( "%s : %d\n", col.name(), col );
            if( col == col.last() ) break;
            col = col.next();
       end
   end
endmodule

