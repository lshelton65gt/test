
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of system functions $realtobits and $bitstoreal.

module test;
wire [63:0] out;
real R, R1;

assign out = $realtobits(R);

initial
begin
    R = 0.0;
    #1;
    repeat (10) #1 R = R + 100.5;
end

always@(out)
    R1 = $bitstoreal(out);

always @ (R, R1)
  if (R != R1)
    $display ("%g was not converted back correctly %g", R, R1);

initial
    $monitor($time,,,"Bit pattern of %g is %b",R, out);

endmodule

