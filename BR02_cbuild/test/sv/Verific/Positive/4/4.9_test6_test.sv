
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): User defined types can be created using typedef. This design
// tests typedef declaration which are again an user defined type created by using
// typedef.

module test (output real out_real, output int out_int);

    typedef real my_real ;
   
    my_real real1 = 2.4e-09 ;

    typedef my_real real_type ;

    real_type real2 = real1 ;

    typedef bit [7:0][11:0] string_12 ;

    string_12 str = "my string\n" ;

    typedef int four_int [0:3] ;

    four_int int1 = '{ 9, 10, 11, 12} ;

    initial
    begin
        out_real = real2;
        out_int = int1[3];
    end

endmodule

