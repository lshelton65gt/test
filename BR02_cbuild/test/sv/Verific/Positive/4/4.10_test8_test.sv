
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing comparison between two enum variables.

module test;
typedef enum {mem1 = 7, mem2 = 5, mem3 = 2, mem4} my_type;
my_type var1, var2;
initial
  begin
	var1 = mem1;
	var2 = mem4;
	if(var1 > var2)
		$display("Order of enum members depend on the key value assigned to them");
	else
		$display("Order of the enum membes does not depend on the key value, it is always in the ascending order");
  end	
endmodule

