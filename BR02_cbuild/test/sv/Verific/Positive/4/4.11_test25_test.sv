
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing signedness, unsignedness and default sign of packed union.

module test;

union packed signed { bit [3:0] un_bit1; } var1;
union packed unsigned {  bit [3:0] un_bit2; } var2;
union packed { bit [3:0] un_bit3; } var3;

initial
begin
    var1 = '1;
    var2 = '1;
    var3 = '1;
    $display(" Should print -1, 15, 15\nUnion1 = %d , Union2 = %d, Union3 = %d ", var1, var2, var3);
end

endmodule

