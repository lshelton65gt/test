
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): String data type dynamically resize itself to store the 
// content of the string literal assigned to it. Here a small string literal
// is assigned after a long one to recognize the string size reducing.

module test ;
   string a ;
   int len1, len2 ;

   initial begin
       a = "A long string" ;
       len1 = a.len() ; // length of string after assigning long string

       $display("Value: %s, and size: %d", a, a.len()) ; 

       a = "Small" ;
       len2 = a.len() ; // length of string after assigning small string
       
       $display("Value: %s, and size: %d", a, a.len()) ;

       if (len1 > len2) 
          $display("String size reduced") ;
       else
          $display("String size not reduced") ;
   end
endmodule
 
