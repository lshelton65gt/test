
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests the 'int' data type. int is a 4 state data type
// so, it assigns z value to a int type variable.

module test(output int in1, output integer size);

    initial
    begin
        in1 = '1;
        #5 in1 = 1'bz;
        size = $bits(in1);
    end

endmodule     

module bench;

    int in1;
    integer size;

    test instan_mod(in1, size);

    initial
    begin
        $monitor(" Input = %d", in1);
        #20 $display(" \n\nsizeof(int) = %d", size);
    end

endmodule

