
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test case on passing unions to and from a function.

module test;

    typedef union {
        shortint uni_short;
        byte  uni_byte;
    } my_uni;

    my_uni var1, var2;

    function f1(output my_uni var2, input my_uni var1);
         var2.uni_short = var1.uni_short*10;
    endfunction

    initial
    begin
         var1.uni_byte = $random;
         f1(var2, var1);
         $display("Output shortint =  %d", var2.uni_short);
    end

endmodule

