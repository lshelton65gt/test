
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The value assigned to the enum variable exceeds the maximum
// allowed value. Hence $cast is used.

module test ;

    typedef enum {
        black, 
        white, 
        pink, 
        red, 
        blue
    } color;
    color c ;
    initial
    begin
         if (!$cast(c,10))
              $display(" Illegal casting") ;
         else
              $display("%s",c.name()) ;
    end

endmodule

