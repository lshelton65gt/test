
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test the enum method num() where N exceeds 
// the size of enumeration. 

module test(input in, output out) ;

    typedef enum {Red = 5, Green, Blue, Yalow, Pink, Black, Orange} Colors ;
    typedef enum {Mo = 7,Tu,We,Th,Fr,Sa,Su} Week ;

    parameter p1 = func1() ; 
    parameter p2 = func2() ; 

    function int func1() ;
        Colors c = c.first ;
        return c.num ;
    endfunction

    function int func2() ;
        Week w = w.first ;
        return w.num ;
    endfunction

    initial
        $display("p1 =%b     p2 =%b", p1, p2) ;

    assign out = in ;

endmodule

