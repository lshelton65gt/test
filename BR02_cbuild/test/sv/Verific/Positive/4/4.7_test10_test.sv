// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses string data type to define parameters and
// ports of a function. It also tests string variables with '>' operator.

module test ;

    parameter string str1 = manipulate("helloooo", "hen") ;
    bot #(str1) b2() ;
    initial
       $display("%s", str1) ;

    parameter string str = manipulate("he", "hello") ;
    bot #(str) b1() ;
    initial
       $display("%s", str) ;

    function string manipulate(string a, string b) ;
        string temp ;
        if( a > b)
            temp = a ;
        else
            temp = b ;
    
        return temp ;
    endfunction
endmodule

module bot #(parameter string s = "heaven" ) ;
    localparam int le = s.len() ;
    bot1 #(le) b2() ;
    initial
       $display("%s    %d", s, le) ;
endmodule 

module bot1#(parameter p = 21) ;
    int r = cal(p) ;
    function int cal(int p) ;
        return p++ * ++p ;
    endfunction
    initial
       $display("%d", r) ;
endmodule

