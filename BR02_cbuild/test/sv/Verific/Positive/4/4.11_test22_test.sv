
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests packed union.

typedef union packed { // default unsigned 
    bit [63:0] bit_slice; 
    bit [7:0][7:0] byte_slice; 
} u_atmcell; 

module test (input int number, bit clk, output bit out) ;
    u_atmcell u1;
    always @ (number)
    begin
       u1.bit_slice = number ;
       if ( u1.bit_slice[31:24] == u1.byte_slice[3])
           out = 1'b1 ;
       else
           out = 1'b0 ;
    end
    initial 
      $display("Packed union is being used.\n") ;
 
endmodule

