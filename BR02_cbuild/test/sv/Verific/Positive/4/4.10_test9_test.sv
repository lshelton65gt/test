
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses enum literals as case items.

typedef enum {READ_EDGE, READ_START_NODE, PROCESS_GRAPH, WRITE_GRAPH} OPTYPE;

typedef struct { int l; int r;} Edge;
typedef enum {OFF, ON} BoolType;

module test #(parameter int N = 4)
(
	input OPTYPE opCode, input Edge e,
	input int sNode, input clk
);
BoolType G[0:N-1][0:N-1];
int startNode;

	always @ (posedge clk)
	begin
		case (opCode)
			READ_EDGE: G[e.l][e.r] = ON;
			READ_START_NODE: startNode = sNode;
			PROCESS_GRAPH: graph_algo(G, startNode);
			WRITE_GRAPH: ;
		endcase
	end

function void graph_algo(input BoolType G[0:N-1][0:N-1], int snode);
	int nodeArray[N-1:0];
	int inEdgeCount[N-1:0];
	int idx = 0;

	for (int i = 0; i < N; i++)
		inEdgeCount[i] = 0;

	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			if (G[i][j] == ON)
				inEdgeCount[j]++;

	for (int j = 0; j < N; j++)
		nodeArray[j] = -1;

	nodeArray[idx++] = snode;
	for (int j = 0; j < N; j++)
	begin
		$display ("Node : %d", nodeArray[j]);
		for (int i = 0; i < N; i++)
		begin
			if (G[nodeArray[j]][i] == ON)
			begin
				inEdgeCount[i]--;
				if (inEdgeCount[i] == 0)
					nodeArray[idx++] = i;
			end
		end
	end
endfunction

endmodule

