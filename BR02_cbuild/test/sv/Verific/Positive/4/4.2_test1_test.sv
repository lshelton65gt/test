
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test of enumerated data type and typedef.

module test ;

    enum { sun, mon, tue, wed, thu, fri, sat } days ;
    typedef real myreal ;

    myreal real1 = 5.6e-09 ;
    
    assign days = sun ;

endmodule

