
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test the default unsigned property of bit, reg and logic data types.

module test(output reg out1, out2, out3, out4, out5, out6) ;

    bit [7:0] bit1 ;
    reg [7:0] reg1 ;
    logic [7:0] logic1 ;

    bit signed [7:0] bit2 ;
    reg signed [7:0] reg2 ;
    logic signed [7:0] logic2 ;

    initial
	begin
        bit1 = -1;
        reg1 = -1;
        logic1 = -1;
        bit2 = -1;
        reg2 = -1;
        logic2 = -1;
        out1 = bit1 < 0;
        out2 = reg1 < 0;
        out3 = logic1 < 0;
        out4 = bit2 < 0;
        out5 = reg2 < 0;
        out6 = logic2 < 0 ;
    end

endmodule

