
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing system function $rtoi and $itor.

module test;

real mod_real;
integer mod_int;

initial
begin
    mod_real = $itor(10);
    mod_int = $rtoi(101.234);
    $display("Value of the real constant = %f, Value of the integer constant = %d", mod_real, mod_int);
end

endmodule

