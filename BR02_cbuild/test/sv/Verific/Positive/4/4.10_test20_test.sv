
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Range of literals can be specified when declaring enumerations
// like enum { add=10, sub[5], jmp[6:8] } it is same as  enum { add=10, sub0, sub1,
// sub2, sub3, sub4, jmp6, jmp7, jmp8 }. Testing of this feature, the elements are
// printed using the methods.

module test ;
   typedef enum{add=10, sub[5], jmp[6:8]} col ;

   initial begin: blk
       col c ;
       c = c.first() ;
       forever begin
            $display( "%s : %d\n", c.name(), c );
            if( c == c.last() ) break;
            c = c.next();
       end
   end
endmodule

