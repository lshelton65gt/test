
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests the string method len(). 

module test ;
    parameter p1 = func1() ; 

    function integer func1() ;
        string str = "This is a test case for string methods." ;
        return str.len ;
    endfunction

endmodule

