
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of casting shortreal onto real.

function shortreal real2short;
    input real funcReal;
    shortreal short = shortreal'(funcReal);
    return short;
endfunction

module test;

    real a;
    shortreal b;

    initial
    begin
        a = 100.0;
        repeat(50)
        a = a * 10;
    end

    always @(a)
    begin
        b = real2short(a);
        $display("Real input = %f, shortreal output = %f", a, b);
    end

endmodule

