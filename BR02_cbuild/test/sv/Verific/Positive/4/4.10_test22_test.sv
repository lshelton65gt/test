
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of sized constants.

module test;

    enum bit [0:3] { mem1 = 4'b1, mem2, mem3 = 4'b11 } var1;

    initial
         $display(" Sizes of the members are: %d %d %d (should be 4 in all cases)", $bits(mem1), $bits(mem2), $bits(mem3));

endmodule

