
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test the string method atoi(). 
//                    The string is converted until the first non-digit is encountered.

module test(input int in, output out) ;

    parameter p1 = func1() ; 

    initial
        $display("p1 = %d", p1) ;

    function integer func1() ;
        string str = "123Abc" ; // The string is converted until the first non-digit is encountered.
        string str_modified ;
        int i ;
        str_modified = str ;

        i = str.atoi() ;
        if (i == 123 && str_modified == str) 
            return 11 ;
        else
            return 10 ;
    endfunction

endmodule

