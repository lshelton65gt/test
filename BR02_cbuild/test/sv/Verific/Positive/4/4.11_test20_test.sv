
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing unions with both 2 state and 4 state data types.

module test;

union packed {
    logic [3:0] un_log;
    bit [3:0] un_bit;
} var1;

initial
begin
    var1 = 4'b1xz0;
    $display(" Value of the logic variable = %b, Value of the bit variable = %b", var1.un_log, var1.un_bit);
end

endmodule

