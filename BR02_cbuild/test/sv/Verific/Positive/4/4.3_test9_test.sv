
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This tests new data type bit.

module test(output bit [3:0]mod_bit);

    initial
    begin
        mod_bit = '1;
        #5 mod_bit = 1'bz;
    end

endmodule

module bench;

    bit [3:0] bench_bit;

    test I1(bench_bit);

    initial
    begin
        $monitor(" Value = %b", bench_bit);
        #10 $display(" sizeof(bit) = %d", $bits(bench_bit)/4 );
    end

endmodule

