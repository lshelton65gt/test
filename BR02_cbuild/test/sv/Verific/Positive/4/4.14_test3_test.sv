
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): When shortreal is casted to int a possible loss of information
// may occur. This design converts a shortreal value to integer value using type
// casting and checks whether information is lost or not.

module test (input clk,
             input [7:0] in1,
             output reg [0:7] out1);

    shortreal r1, r2, r3;

    always@(posedge clk)
    begin
        r1 = (in1[0]?in1/2:(in1+1)/2);   // r1 must have a fraction
        r2 = int'(r1);   // r2 should not, possible loss of information
        r3 = shortreal'(r2);

        out1 = ((r1 == r3) ? (~in1) : (in1));
    end

endmodule

