
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): If the data type of an enum declaration is 4-state value,
// then the elements may contain  'x' or 'z' values. This design uses integer as
// type of the enumeration, as it is 4-state so we can assign Z value to the enum
// literals.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    enum integer { A = 'z, B = 0, C=1 } enum_t;

    always@(posedge clk)
    begin
        enum_t = A;

        if (enum_t === 'z)
        begin
            out1 = in1 - in2;
            out2 = in2 - in1;
        end
        else
        begin
            out1 = '0;
            out2 = '0;
        end
    end

endmodule

