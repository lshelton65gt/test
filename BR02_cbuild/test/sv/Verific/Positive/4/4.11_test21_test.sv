
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test packed signed - 4 state structure,
// where the structure is defined without using typedef,
// and to test the signed bit of the packed structure. 

struct packed signed{ // Without using typedef
    int a ; 
    shortint b ;
    logic [7:0] c ; // 4 - state
    bit [7:0] d ;
} pack1 ; // signed, 4-state

module test ;
    int var1 ;
    initial 
    begin
       pack1.a = -50 ;
       pack1.b =  30 ;
       pack1.c =  'x ;
       pack1.d =  20 ;
       var1 = ~pack1.a + 1 ; // 2's complement
    end

    initial #5
    begin
        $display ("2's complement of a = %d", pack1[63:32]) ;  // will display  2's complement os -50
        $display ("a = %d", -var1) ;  // will display  -50
        $display ("b = %d", pack1[31:16]) ;  // will display  30 
        $display ("c = %d", pack1[15:8]) ;   // will display  x
        $display ("d = %d", pack1[7:0]) ;    // will display  20
        $display ("Signed bit = %b ", pack1[63]) ; // will display 1
    end
endmodule

