
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): str.putc(i, s) replaces the ith character in str with 's'.
// If i < 0 or i > str.len(), str is unchanged. Test it. 

module test(input byte in, output out) ;

    string s ;
    initial 
    begin
        s = "hello" ;
        $display("The length of string: %d", s.len()) ;
        s.putc(10,"a") ; // Try to replace character beyond the length
        s.putc(-2, "b") ; // Try to replace character beyond the length
        if (s.compare(s) == 0)
            $display("String is unchanged after putc operation") ;
        else
            $display("String is changed after putc operation") ;
    end

    assign out = &in ;

endmodule

