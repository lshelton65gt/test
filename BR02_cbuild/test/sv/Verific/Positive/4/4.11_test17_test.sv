
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Test case on passing a structure to and from a task.

module test;

    typedef struct { 
        logic [63:0] str_reg; 
        int str_int;
    } my_str;

    my_str str1, str2;

    task my_task;
         input my_str str1;
         output my_str str2;
         str2 = str1;
         str2.str_int = str2.str_int*5;
    endtask

    initial
    begin
        str1.str_reg = "ABCDEF!";
        str1.str_int = 5;
        my_task(str1, str2);
        $display("Output_reg = (should be ABEDEF!) %s, Output_int = (should be 25) %d", str2.str_reg, str2.str_int);
    end

endmodule

