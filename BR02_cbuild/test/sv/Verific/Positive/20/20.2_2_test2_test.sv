
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface at the beginning. Two modports
// are also defined in the interface. A module is defined which uses the interface
// type ports in it. Another module is defined that instantiates that interface and
// connects the modports in the module instantiation.

interface iface();
    bit read;
    bit write;
    bit enable;
    logic [0:3] data;

    modport mp_in(input read, enable, output data);
    modport mp_out(input write, enable, output data);
endinterface

module test(input clk, iface in, iface out);

    reg [0:3] temp;

    always@(posedge clk)
    begin
        if (in.enable && in.read)
            temp = in.data;

        if (out.enable && out.write)
            out.data = temp;
    end

endmodule

module bench;

    reg clk;

    iface in();
    iface out();

    test t_mod(clk, in.mp_in, out.mp_out);

    initial
    begin
        clk = 0;
        $monitor($time,, "read = %d, read enable = %d, read data = %d, write = %d, write enable = %d, writtem data = %d",
				in.read, in.enable, in.data, out.write, out.enable, out.data);
        #1001 $finish ;
    end

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in.read, in.enable, in.data, out.enable, out.write } = $random;

endmodule

