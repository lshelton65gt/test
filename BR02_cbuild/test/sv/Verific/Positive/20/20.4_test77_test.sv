
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): To test a case with the following:  
//      Using modport name in module instantiation.
//      Using a single interface instantiation.
//      Using multiple instantiation of modules with multiple directions(modport) 

interface simple_bus (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data) ;

endinterface: simple_bus

module abcMod (simple_bus a) ; // interface name and modport name

    parameter p1 = 4, p2 = 4 ;
    bit [7:0] avail1 = p1 ;
    bit [7:0] avail2 = p2 ;
    always @(posedge a.clk) // the clk signal from the interface
        a.addr <= avail1 + avail2 ; // the gnt and req signal in the interface
    initial
        $display("Instantiating module abcMod") ;

endmodule

module xyzMod (simple_bus b) ;

    parameter p1 = 1, p2 = 1 ;
    always @(posedge b.clk)
        b.gnt = p1 & p2 ;
    initial
        $display("Instantiating module xyzMod") ;

endmodule

module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus sb_intf(clk) ; // Instantiate the interface
    simple_bus sb_intf1(clk) ; // Instantiate the interface
    
    abcMod #(10,20) abc1(sb_intf.master) ;
    abcMod #(10,20) abc2(sb_intf1.slave) ;

    xyzMod #(1,1) yz2(sb_intf.master) ;

    assign out = in ;

endmodule 

