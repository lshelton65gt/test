
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a module that has <interface_name>.<modport_name>
// type ports before the actual definition of the interface.  Next a module instantiates the
// interface and connects the interface instances in the module instantiation which uses
// <interface>.<modport> type ports.

module test(input clk, iface.mp_in in, iface.mp_out out);

    reg [0:3] temp;

    always@(posedge clk)
    begin
        if (in.enable && in.read)
            temp = in.data;

        if (out.enable && out.write)
            out.data = temp;
    end

endmodule

interface iface();
    bit read;
    bit write;
    bit enable;
    logic [0:3] data;

    modport mp_in(input read, enable, output data);
    modport mp_out(input write, enable, output data);
endinterface

module bench;

    reg clk;

    iface in();
    iface out();

    test t_mod(clk, in, out);

    initial
    begin
        clk = 0;
        $monitor($time,, "read = %d, read enable = %d, read data = %d, write = %d, write enable = %d, writtem data = %d",
				in.read, in.enable, in.data, out.write, out.enable, out.data);
        #1001 $finish ;
    end

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in.read, in.enable, in.data, out.enable, out.write } = $random;

endmodule

