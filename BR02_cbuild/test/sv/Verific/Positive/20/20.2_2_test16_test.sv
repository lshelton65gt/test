
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses an interface type port in interface declaration.

interface iface1();
    bit read;
    bit write;
    bit enable;
    logic [0:3] data;

    modport mp_in(input read, enable, output data);
    modport mp_out(input write, enable, output data);
endinterface

interface iface2(iface1 a);
    bit read;
    bit write;
    bit enable;
    logic [0:3] data;

    modport mp_in(input read, enable, output data);
    modport mp_out(input write, enable, output data);
endinterface

module test(input clk, iface1 in, iface2 out);

    reg [0:3] temp;

    always@(posedge clk)
    begin
        temp = in.read;
        out.write = temp;
    end

endmodule

module bench;

    reg clk;

    iface1 in();
    iface2 out(in);

    test t_mod(clk, in.mp_in, out.mp_out);

    initial
    begin
        clk = 0;
        $monitor($time,, "read = %d, write = %d", in.read, out.write);
        #1001 $finish ;
    end

    always
        #5 clk = ~clk;

    always@(negedge clk)
        in.read = $random;

endmodule

