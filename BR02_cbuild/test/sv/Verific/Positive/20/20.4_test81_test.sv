
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): To test a case with the following:  
//      Using modport name in module definition.
//             Ex.    module abcMod (simple_bus.master a) ;
//             module xyzMod (simple_bus.slave b) ;
//      Using a single interface instantiation
//             Ex.    simple_bus sb_intf (clk) ; 
//      Using single instantiation of modules
//          with specific direction(modport)
//             Ex.    abcMod abc(sb_intf) ;
//                    xyzMod xyz(sb_intf) ;

interface simple_bus (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data) ;

endinterface: simple_bus

module abcMod (simple_bus.master a) ; // interface name and modport name

    logic avail ;

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & avail ; // the gnt and req signal in the interface
    initial
        $display("Instantiating module abcMod") ;
endmodule 

module xyzMod (simple_bus.slave b) ;
    always @(posedge b.clk)
        b.data = ~b.data ;
    initial
        $display("Instantiating module xyzMod") ;
endmodule 

module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus sb_intf(clk) ; // Instantiate the interface
    
    abcMod abc(.a(sb_intf)) ; // Connect the interface to the module instance
    xyzMod xyz(.b(sb_intf)) ;

    assign out = in ;

endmodule 

