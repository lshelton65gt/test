
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface and use it as port 
// of module.

interface simple_bus; // Define the interface
    logic req, gnt;
    logic [7:0] addr, data = 0 ;
    logic [1:0] mode; 
    logic start, rdy;
endinterface: simple_bus

module memMod(simple_bus a, // Use the simple_bus interface
              input bit clk);
    logic avail;
    // a.req is the req signal in the 'simple_bus' interface
    always @(posedge clk)
        a.gnt <= a.req & avail;
    initial
        $display("clk = %b", clk) ;
endmodule 

module cpuMod(simple_bus b, input bit clk);
    logic avail ;
    always @ (posedge clk)
        b.addr[5] = ~b.data ;
    initial
        $display("clk = %b", clk) ;
endmodule 

module test(input in, output int out);

    logic clk = 0;
    simple_bus sb_intf(); // Instantiate the interface
    memMod mem(sb_intf, clk); // Connect the interface to the module instance
    cpuMod cpu(.b(sb_intf), .clk(clk)); // Either by position or by name
    assign out = {32{in}} ;

endmodule 

