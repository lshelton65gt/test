
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the generic bundle with three interfaces.

module memMod (interface a) ;
    logic avail ;
    always @(posedge a.clk) a.gnt <= a.req & avail ;
    initial
        $display("Instantiating module memMod") ;
endmodule
    
module cpuMod(interface b) ;
    always @(posedge b.clk) b.data1 = ~b.data1 ;
    initial
        $display("Instantiating module cpuMod") ;
endmodule 

module plaMod(interface c) ;
    always @(posedge c.clk) c.req = ~c.req ;
    initial
        $display("Instantiating module plaMod") ;
endmodule

interface simple_bus1(input bit clk) ; // Define the 1st interface
    logic req, gnt ;
    logic [7:0] addr, data1 ;
    logic [1:0] mode ;
    logic start, rdy ;
endinterface: simple_bus1

interface simple_bus2(input bit clk); // Define the 2nd interface
    logic  data1 ;
    logic start, rdy ;
endinterface: simple_bus2

interface simple_bus3(input bit clk); // Define the 3rd interface
    logic req ;
    logic start, rdy ;
endinterface: simple_bus3

module test(input in, output out) ;

    simple_bus1 sb_intf1() ;
    simple_bus2 sb_intf2() ;
    simple_bus3 sb_intf3() ;

    memMod mem1 (.a(sb_intf1)) ; // Connect by name
    memMod mem2 (.a(sb_intf1)) ; 
    memMod mem3 (.a(sb_intf1)) ; 
    memMod mem4 (.a(sb_intf1)) ; 

    cpuMod cpu1 (.b(sb_intf2)) ;
    cpuMod cpu2 (.b(sb_intf2)) ;
    cpuMod cpu3 (.b(sb_intf2)) ;
    cpuMod cpu4 (.b(sb_intf2)) ;

    plaMod pla1 (sb_intf3) ;  // Connect by position
    plaMod pla2 (sb_intf3) ;
    plaMod pla3 (sb_intf3) ;
    plaMod pla4 (sb_intf3) ;

    assign out = in ;

endmodule 

