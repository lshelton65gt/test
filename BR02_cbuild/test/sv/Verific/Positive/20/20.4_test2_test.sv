
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): To test a design with the following features:  
//      Using PARAMETERS in the INTERFACE definition.
//      Not using PARAMETERS in the MODULE definition.
//      Using generic interface in module definition.
//      Using a single interface instantiation.
//      Using single instantiation of modules with specific direction(modport)  

interface simple_bus #(parameter p1 = 8, p2 = 2)(input bit clk) ;

    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic [p2-1:0] mode ;
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data) ;

endinterface: simple_bus

module abcMod (interface a) ; // interface name and modport name

    logic avail ;

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & avail; // the gnt and req signal in the int
    initial
        $display("Instantiating module abcMod") ;

endmodule

module xyzMod (interface b) ;

    always @(posedge b.clk)
        b.gnt = b.start & b.req ;
    initial
        $display("Instantiating module xyzMod") ;

endmodule

module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus #(.p1(16), .p2(3)) sb_intf(clk) ;

    abcMod abc(sb_intf.master) ;
    xyzMod xyz(sb_intf.slave) ;

    assign out = in ;

endmodule 

