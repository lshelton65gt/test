
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the generic bundle with more than one interface
// in the test case.

module memMod (interface a, input bit clk) ;
    logic avail ;
    always @(posedge clk) a.gnt <= a.req & avail ;
    initial
        $display("clk =%b    a.addr=%d a.data1=%d", clk, a.addr, a.data1) ;
endmodule 
    
module cpuMod(interface b, input bit clk) ;
    logic avail ;
    always @(posedge clk) b.data1 = ~b.data1 ;
endmodule 

interface simple_bus ; // Define the interface
    logic req, gnt ;
    logic [7:0] addr, data1 ;
    logic [1:0] mode ; 
    logic start, rdy ;
endinterface: simple_bus

interface simple_bus1 ; // Define the interface
    logic  data1 ;
    logic start, rdy ;
endinterface: simple_bus1

module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus sb_intf() ; // Instantiate the interface
    simple_bus1 sb_intf1() ;
    // Connect the sb_intf instance of the simple_bus
    // interface to the generic interfaces of the
    // memMod and that of sb_intf1 to cpuMod modules
    memMod mem (.a(sb_intf), .clk(clk)) ;
    cpuMod cpu (.b(sb_intf1), .clk(clk)) ;
    assign out = in ;

endmodule 

