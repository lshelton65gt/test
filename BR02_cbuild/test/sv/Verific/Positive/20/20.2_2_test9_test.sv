
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a module with generic interface type ports.
// The interface is defined after the declaration of the module. Then another module
// instantiates the interface and connects them in the module instantiation. The
// interface is defined with modports, but the modports are not used anywhere.

module test(input clk, interface in, interface out);

    reg [0:3] temp;

    always@(posedge clk)
    begin
        if (in.enable && in.read)
            temp = in.data1;

        if (out.enable && out.write)
            out.data1 = temp;
    end

endmodule

interface iface();
    bit read;
    bit write;
    bit enable;
    logic [0:3] data1;

    modport mp_in(input read, enable, output data1);
    modport mp_out(input write, enable, output data1);
endinterface

module bench;

    reg clk;

    iface in();
    iface out();

    test t_mod(clk, in, out);

    initial
    begin
        clk = 0;
        $monitor($time,, "read = %d, read enable = %d, read data1 = %d, write = %d, write enable = %d, writtem data1 = %d",
				in.read, in.enable, in.data1, out.write, out.enable, out.data1);
        #1001 $finish ;
    end

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in.read, in.enable, in.data1, out.enable, out.write } = $random;

endmodule

