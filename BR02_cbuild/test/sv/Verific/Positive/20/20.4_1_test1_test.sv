
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Modport can be used to bundle interface elements and
// specify their directions. This design defines an interface and also uses
// modport to create a set of two directed bundle that are used when declaring
// modules.

interface bidir_bus;

logic [7:0] packet;
logic ready, request, ack, send;

    modport trans(output packet, ready, send,
                  input request, ack);

    modport receive(input packet, ready, send, 
                    output request, ack);
                   
endinterface

module transmitter(bidir_bus.trans a, input bit clk);

logic [7:0] mem[7:0];
logic [7:0] addr;
int count = 0;

initial 
begin
    a.ready = 1;
    addr = 0;
    a.send = 0;
    for(int i = 0; i < 8; i++)
        mem[i] = i + 2;
end    

always @(posedge clk)
    if(a.request)
    begin
        a.send = 1;
        a.packet = mem[addr];
        count++;
        addr++;
        a.ready = 0;
    end    
endmodule

module receiver(bidir_bus.receive a, input bit clk);

logic [7:0] mem[7:0];
logic [7:0] addr;
int count = 0;

initial
begin
    addr = 0;
    a.request = 0;
end

always @(a.ready)
begin
    a.request = 1;
end    

always @(posedge clk)
    if(a.send)
    begin
        mem[addr] = a.packet;
        addr++;
        count++;
        a.ack = 1;
    end
endmodule

