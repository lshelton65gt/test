
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design writes an interface with modports having 
// modport expressions.

interface intf_t #(parameter clients = 2) ;
    bit [clients-1:0] req ;

    generate
        genvar i ;
        for (i = 0 ; i < clients ; i++)
        begin : mps
            modport client_mp(output .client_req(req[i])) ;
        end
    endgenerate
endinterface

module client_m (intf_t client_ifc, input in) ;
    assign client_ifc.client_req = in ;
    initial
        $display("Message from client") ;
endmodule

module test #(N = 2)(input int in, output out) ;

    intf_t #(.clients(N)) intf() ;

    generate
        genvar j ;
        for(j =0 ; j < N ; j++) 
        begin : clients
           client_m client (.client_ifc(intf.mps[j].client_mp)) ;
        end
    endgenerate
    assign out = &in ;

endmodule

