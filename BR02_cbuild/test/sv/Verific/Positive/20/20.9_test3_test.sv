
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing of interface, while accessing it without ports.
// Here interface name is same as that of a port of a module in which
// the interface is instantiated.

interface in;
    task lock;
	    $display("Locked");
	endtask

	function unlock;
	    $display("Unlocked");
	endfunction
endinterface

function int f(input int i);
    return i;
endfunction

function int g(input int i);
    return i;
endfunction

module test(input int in, output int out);

    in mutex(); // same name for both interface and a port
	always @(in)
	begin
	    #10 mutex.lock;
		@(in) out = f(in);
		mutex.unlock();
	end

	always @(in)
	begin
	    #10 mutex.lock;
		@(in) out = g(in);
		mutex.unlock();
	end
endmodule


