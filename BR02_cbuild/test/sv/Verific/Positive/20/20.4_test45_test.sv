
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing multiple interface in a single port list.

module memMod (interface a, interface b) ;
    logic avail ;
    always @(posedge a.clk) 
    begin
        a.gnt <= a.req & avail ;
        b.gnt <= b.req & avail ;
    end
    initial
        $display("Instantiating module memMod") ;
endmodule
    
module cpuMod(interface b) ;
    always @(posedge b.clk) b.data1 = ~b.data1 ;
    initial
        $display("Instantiating module cpuMod") ;
endmodule 

module plaMod(interface c, interface d) ;
    logic avail ;
    always @(posedge c.clk)
    begin
        c.req = ~c.req ;
        d.gnt <= d.req & avail ;
    end
    initial
        $display("Instantiating module plaMod") ;
endmodule

interface simple_bus1(input bit clk) ; // Define the 1st interface
    logic req, gnt ;
    logic [7:0] addr, data1 ;
    logic [1:0] mode ;
    logic start, rdy ;
endinterface: simple_bus1

interface simple_bus2(input bit clk); // Define the 2nd interface
    logic  data1 ;
    logic start, rdy ;
endinterface: simple_bus2

interface simple_bus3(input bit clk); // Define the 3rd interface
    logic req, gnt ;
    logic start, rdy ;
endinterface: simple_bus3

module test(input int in, output out) ;

    logic clk ;
    simple_bus1 sb_intf1(clk) ;
    simple_bus2 sb_intf2(clk) ;
    simple_bus3 sb_intf3(clk) ;

    memMod mem1 (.a(sb_intf1), .b(sb_intf3)) ; // Connect by name
    memMod mem2 (.a(sb_intf1), .b(sb_intf3)) ;
    memMod mem3 (.a(sb_intf1), .b(sb_intf3)) ;
    memMod mem4 (.a(sb_intf1), .b(sb_intf3)) ;

    plaMod cpu1 (.c(sb_intf3), .d(sb_intf1)) ;
    plaMod cpu2 (.c(sb_intf3), .d(sb_intf1)) ;
    plaMod cpu3 (.c(sb_intf3), .d(sb_intf1)) ;
    plaMod cpu4 (.c(sb_intf3), .d(sb_intf1)) ;

    cpuMod pla1 (sb_intf2) ;  // Connect by position
    cpuMod pla2 (sb_intf2) ;
    cpuMod pla3 (sb_intf2) ;
    cpuMod pla4 (sb_intf2) ;

    assign out = $onehot(in) ;

endmodule 

