
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): To test a design with the following features:  
//      Using generic interface along with modport name in module definition.
//      Only those interface definitions which has master & slave modports.
//      Using a single interface instantiation.
//      Using multiple instantiation of modules
//          with specific direction(modport)
 


interface simple_bus1 #(parameter p1 = 8, p2 = 2)(input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic [p2-1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start,
                          ref data) ;

endinterface: simple_bus1

interface simple_bus2 (input bit clk) ; // Define the interface

    logic req1, gnt1 ;
    logic [7:0] addr1, data1 ;
    logic [1:0] mode1 ; 
    logic start1, rdy1 ;
    modport slave (input req1, addr1, mode1, start1, clk,
                   output gnt1, rdy1, ref data1) ;
    modport master(input gnt1, rdy1, clk,
                   output req1, addr1, mode1, start1, ref data1) ;

endinterface: simple_bus2

module abcMod (interface a) ; // interface name and modport name

    parameter p1 = 8 ;
    bit [p1-1:0] avail1 ;
    bit [p1-1:0] avail2 ; 
    always @(posedge a.clk) // the clk signal from the interface
        a.addr <= avail1 + avail2 ;
    always @(a.addr)
    begin
        avail1 = a.addr ;
        avail2 = a.addr ;
    end
    initial
        $display("Instantiating module abcMod") ;

endmodule 

module uvwMod (interface.slave b) ;

    parameter p1 = 1, p2 = 1 ;
    always @(posedge b.clk)
        b.gnt1 = p1 & p2 ;
    initial
        $display("Instantiating module uvwMod") ;

endmodule

module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus1 #(.p1(16), .p2(3) )sb_intf1(clk) ; // Instantiate the interface
    simple_bus1 #(10,3) sb_intf2(clk) ; 

    simple_bus2 sb_intf3(clk) ;
    simple_bus2 sb_intf4(clk) ;

    abcMod #(16) abc11(sb_intf1) ;
    abcMod #(16) abc21(sb_intf1) ;
    abcMod #(10) abc12(sb_intf2) ;
    abcMod #(10) abc22(sb_intf2) ;

    uvwMod #(1,0) xyz11(sb_intf3) ;
    uvwMod #(1,0) xyz21(sb_intf3) ;
    uvwMod #(1,0) xyz12(sb_intf4) ;
    uvwMod #(1,0) xyz22(sb_intf4) ;

    assign out = in ^ 1 ;

endmodule 

