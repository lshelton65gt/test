
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests modports which specify directions of
// objects defined within interface and used within module.

interface i2;
    wire a, b, c, d;
    modport master (input a, b, output c, d);
    modport slave (output a, b, input c, d);
endinterface 

module m (i2 i);
    assign i.c = ~i.a ;
    assign i.d = ~i.b ;
endmodule 

module s (i2 i);
    assign i.a = ~i.c ;
    assign i.b = ~i.d ;
    initial
        $display("Instantiating module s") ;
endmodule 

module test(input in, output out) ;

    i2 intf();
    m u1(.i(intf.master));
    s u2(.i(intf.slave));
    assign out = in ;

endmodule 

