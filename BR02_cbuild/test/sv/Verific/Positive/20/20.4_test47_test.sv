
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests modports which specify directions of
// objects declared within interface and used in modules where interface is
// connected as port. 

interface i2 ;
    wire a, b, c, d ;
    modport master (input a, b, output c, d) ;
    modport slave (output a, b, input c, d) ;
endinterface 

module m (i2.master i) ;
    assign i.c = ~i.a ;
    assign i.d = ~i.b ;
    initial
        $display("Instantiating module m") ;
endmodule 

module s (i2.slave i) ;
    assign i.a = ~i.c ;
    assign i.b = ~i.d ;
    initial
        $display("Instantiating module s") ;
endmodule 

module test(input in, output out) ;

    i2 intf() ;
    m u1(.i(intf)) ;

    s s1(.i(intf)) ;

    assign out = in + 1 ;

endmodule 

