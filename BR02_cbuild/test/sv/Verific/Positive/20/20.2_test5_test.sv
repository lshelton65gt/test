
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design tests use of interface as the generic bundle.

module memMod (interface a, input bit clk) ; // generic interface
    logic avail ;
    always @(posedge clk) a.gnt <= a.req & avail ;
endmodule 
    
module cpuMod(interface b, input bit clk) ; // generic interface
    logic avail ;
    always @(posedge clk) b.data1 = ~b.data1 ;
endmodule 

interface simple_bus ; // Define the interface
    logic req, gnt ;
    logic [7:0] addr, data1 ;
    logic [1:0] mode ; 
    logic start, rdy ;
endinterface: simple_bus

module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus sb_intf() ; // Instantiate the interface
    // Connect the sb_intf instance of the simple_bus
    // interface to the generic interfaces of the
    // memMod and cpuMod modules
    memMod mem (.a(sb_intf), .clk(clk)) ;
    cpuMod cpu (.b(sb_intf), .clk(clk)) ;
    assign out = !in ;

    initial
    begin
        $monitor($time,,,"out =%b", out) ;
        #100 $finish ;
    end

endmodule 

