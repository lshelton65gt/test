
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses a generic interface type port before any
// interface declaration. It uses that interface type ports and its elements inside
// the module. It even instantiates and connects modports of the interface before its
// declaration. The actual declaration of the interface comes at the end.

module test(input clk, interface in, interface out);

    reg [0:3] temp;

    always@(posedge clk)
    begin
        if (in.enable && in.read)
            temp = in.data1;

        if (out.enable && out.write)
            out.data1 = temp;
    end

endmodule

module bench;

    reg clk;

    iface in();
    iface out();

    test t_mod(clk, in.mp_in, out.mp_out);

    initial
    begin
        clk = 0;
        $monitor($time,, "read = %d, read enable = %d, read data1 = %d, write = %d, write enable = %d, writtem data1 = %d",
				in.read, in.enable, in.data1, out.write, out.enable, out.data1);
        #1001 $finish ;
    end

    always
        #5 clk = ~clk;

    always@(negedge clk)
        { in.read, in.enable, in.data1, out.enable, out.write } = $random;

endmodule

interface iface();
    bit read;
    bit write;
    bit enable;
    logic [0:3] data1;

    modport mp_in(input read, enable, output data1);
    modport mp_out(input write, enable, output data1);
endinterface

