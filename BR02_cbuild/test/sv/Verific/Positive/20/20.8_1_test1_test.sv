
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : As per SV LRM IEEE Std 1800 sec 20.8.1, virtual interfaces
// and clocking blocks can be combined to represent the interconnect between
// synchronous blocks. The following design tests this.

interface SyncBus( input bit clk );
    wire a, b, c;
    clocking sb @(posedge clk); 
        input a;
        output b;
        inout c;
    endclocking 
endinterface 

typedef virtual SyncBus VI;    // A virtual interface type

task do_it( VI v );    // handles any SyncBus via clocking sb
    if( v.sb.a == 1 )
        v.sb.b <= 0;
    else 
        v.sb.c <= 1;
    $display("task do_it called") ;
endtask 

module test() ;

    bit clk ;

    SyncBus b1(clk) ;
    SyncBus b2(clk) ;

    initial begin
        VI v[2] = '{b1, b2} ;

        repeat (20)
            do_it (v[$urandom_range(0,1)]) ;
    end

endmodule

