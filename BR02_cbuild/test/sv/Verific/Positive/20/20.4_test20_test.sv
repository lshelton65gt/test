
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): To test a case with the following:  
//      Using generic interface along with modport name in module definition.
//      Interface definitions some have master, slave modports along with some have different modports. 
//      Using a multiple interface instantiation.
//      Using multiple instantiation of modules
//            with specific direction(modport) 

interface simple_bus1 #(parameter p1 = 8, p2 = 2)(input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [p1-1:0] addr, data1 ;
    logic [p2-1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data1) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data1) ;

endinterface: simple_bus1

interface simple_bus2 (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data1 ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data1) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data1) ;

endinterface: simple_bus2

module abcMod (interface.master a) ; 

    logic avail ;

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & avail; // the gnt and req signal in the interface
    initial
        $display("Instantiating module abcMod") ;

endmodule 

module xyzMod (interface.slave b) ;

    always @(posedge b.clk)
        b.gnt = b.start & b.req ;

endmodule 


interface simple_bus3 (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data1 ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport connection1(input req, addr, mode, start, clk,
                   output gnt, rdy, ref data1) ;
    modport connection2(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data1) ;

endinterface: simple_bus3

interface simple_bus4 (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data1 ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport connection1(input req, addr, mode, start, clk,
                   output gnt, rdy, ref data1) ;
    modport connection2(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data1) ;

endinterface: simple_bus4

module defMod (interface.connection1 d, interface.connection2 e) ;

    always @(posedge d.clk)
        d.data1 = ~e.data1 ;
    initial
        $display("Instantiating module defMod") ;

endmodule


module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus1 #(.p1(16), .p2(3)) sb_intf1(clk) ; // Instantiate the interface
    simple_bus1 sb_intf2(clk) ; 

    simple_bus2 sb_intf3(clk) ;
    simple_bus2 sb_intf4(clk) ;
    
    abcMod abc11(sb_intf1) ;
    abcMod abc21(sb_intf1) ;
    abcMod abc12(sb_intf2) ;
    abcMod abc22(sb_intf2) ;

    xyzMod xyz11(sb_intf3) ;
    xyzMod xyz21(sb_intf3) ;
    xyzMod xyz12(sb_intf4) ;
    xyzMod xyz22(sb_intf4) ;

    simple_bus3 sb_con1(clk) ; // Instantiate the interface
    simple_bus4 sb_con2(clk) ;
    defMod def(sb_con1, sb_con2) ;

    assign out = !in ;

endmodule 

