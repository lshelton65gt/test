
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a parameterized interface and use that as
// module port.

function void ifprint() ;
    $display(" This is if condition") ;
endfunction

function void elseprint() ;
    $display(" This is else condition") ;
endfunction

interface simple_bus #(parameter AWIDTH = 8, DWIDTH = 8)
                      (input bit clk);
    logic req, gnt;
    logic [AWIDTH-1:0] addr;
    logic [DWIDTH-1:0] data;
    logic [1:0] mode; 
    logic start, rdy;

endinterface: simple_bus

module test(interface a);

    logic avail;
    initial 
        a.mode[0] = 1'b0 ;

    always @(posedge a.clk)
        a.gnt <= a.req & avail; 

    always @(a.start) begin
        if (a.mode[0] == 1'b0)
            ifprint() ;
        else 
            elseprint() ;
     end
     initial 
        $display("Instantiating module test.\n") ;

endmodule 

module top;

    logic clk = 0;
    simple_bus sb_intf(clk);
    simple_bus #(.DWIDTH(16)) wide_intf(clk);

    initial repeat(10) 
        #10 clk++;

    test  mem(sb_intf);  
    test  mem1(wide_intf);  

endmodule 

