
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface inside a module.


module test(input in, output out) ;

    interface simple_bus ; 
        logic req, gnt ;
        logic [7:0] addr, data = 0 ;
        logic [1:0] mode ; 
        logic start, rdy ; 
    endinterface: simple_bus

    module memMod(simple_bus a, input clk) ;
        logic avail ;
        always @(posedge clk)
            a.gnt <= a.req & avail ;
        initial
            $display("clk = %b", clk) ;
    endmodule 

    module cpuMod(simple_bus b, input bit clk) ;
        logic avail ;
        always @ (posedge clk)
            b.addr[5] = ~b.data ;
        initial
            $display("clk = %b", clk) ;
    endmodule 

    logic clk = 0 ;
    simple_bus sb_intf() ; 
    memMod mem(sb_intf, clk) ;
    cpuMod cpu(.b(sb_intf), .clk(clk)) ; 

endmodule 

