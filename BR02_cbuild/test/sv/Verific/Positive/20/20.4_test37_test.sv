
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): To test a design with the following features:  
//      Using modport name in module instantiation.
//      Using a single interface instantiation.
//      Using multiple instantiation of modules with multiple directions(modport) 

interface simple_bus (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start,
                          ref data) ;

endinterface: simple_bus

module abcMod (simple_bus a) ; // interface name and modport name

    logic avail ;

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & avail; // the gnt and req signal in the interface
    initial
        $display("Instantiating module abcMod") ;

endmodule 

module xyzMod (simple_bus b) ;

    always @(posedge b.clk)
        b.data = ~b.data ;
    initial
        $display("Instantiating module xyzMod") ;

endmodule 

module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus sb_intf(clk) ; // Instantiate the interface
    
    abcMod abc1(sb_intf.master) ;
    abcMod abc2(sb_intf.slave) ;
    abcMod abc3(sb_intf.master) ;

    xyzMod xyz1(sb_intf.slave) ;
    xyzMod xyz2(sb_intf.master) ;
    xyzMod xyz3(sb_intf.slave) ;

    assign out = in - 1 ;

endmodule 

