
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): To test a case with the following:  
//      Using generic interface along with modport name in module definition.
//      Only those interface definitions which has master & slave modports. 
//      Using a single interface instantiation.
//      Using multiple instantiation of modules with specific direction(modport) 

interface simple_bus1 #(parameter p1 = 8, p2 = 2)(input bit clk) ; // Define the interface
    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic [p2-1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk, output gnt, rdy, ref data) ;
    modport master(input gnt, rdy, clk, output req, addr, mode, start, ref data) ; 
endinterface: simple_bus1

interface simple_bus2 (input bit clk) ; // Define the interface
    logic req, gnt ;
    logic [7:0] addr, data ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk, output gnt, rdy, ref data) ;
    modport master(input gnt, rdy, clk, output req, addr, mode, start, ref data) ;
endinterface: simple_bus2

module abcMod (interface.master a) ; 
    logic avail ;
    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & avail; // the gnt and req signal in the interface
    initial
        $display("Instantiating module abcMod") ;
endmodule 

module xyzMod (interface.slave b) ;
    always @(posedge b.clk)
        b.gnt = b.start & b.req ;
    initial
        $display("Instantiating module xyzMod") ;
endmodule 

module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus1 #(.p1(16), .p2(3)) sb_intf1(clk) ; // Instantiate the interface
    simple_bus2 sb_intf2(clk) ; // Instantiate the interface
    
    abcMod abc1(.a(sb_intf1)) ; // Connect the interface to the module instance
    abcMod abc2(.a(sb_intf1)) ;
    abcMod abc3(.a(sb_intf1)) ;

    xyzMod xyz1(sb_intf2) ;
    xyzMod xyz2(sb_intf2) ;
    xyzMod xyz(sb_intf2) ;

    assign out = in ;

endmodule 

