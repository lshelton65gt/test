
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): This design defines a parameterized interface with two 
// modports. Two modules are defined having interface type port. Defined interface
// is instantiated thrice providing same input signal and each instance is passed
// in two module instantiations specifying different modport names.

interface simple_bus #(parameter p1 = 8, p2 = 2)(input bit clk) ; //

    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic [p2-1:0] mode ;
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data) ;

endinterface: simple_bus

module abcMod (simple_bus a) ; // interface name and modport name

    logic avail ;

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & avail; // the gnt and req signal in the int
    initial
        $display("Instantiating module abcMod") ;

endmodule

module xyzMod (simple_bus b) ;

    always @(posedge b.clk)
        b.gnt = b.start & b.req ;
    initial
        $display("Instantiating module xyzMod") ;

endmodule


module test(input in, input logic clk, output out) ;

    simple_bus #(.p1(16),.p2(3)) sb_intf1(clk) ; // Instantiate the interface
    simple_bus sb_intf2(clk) ; // Instantiate the interface
    simple_bus #(.p1(16),.p2(3)) sb_intf3(clk) ; // Instantiate the interface
    
    abcMod abc1(sb_intf1.master) ;
    abcMod abc2(sb_intf2.master) ;
    abcMod abc3(sb_intf3.master) ;

    xyzMod xyz1(sb_intf1.slave) ;
    xyzMod xyz2(sb_intf2.slave) ;
    xyzMod xyz3(sb_intf3.slave) ;

    assign out = in ;

endmodule 

