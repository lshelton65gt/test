
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): To test a design with the following features:  
//      Using generic interface along with modport name in module definition.
//      Interface definitions some have master, slave modports along with some have different modports.
//      Using a single interface instantiation.
//      Using single instantiation of modules with specific direction(modport)
 

interface simple_bus1 #(parameter p1 = 8, p2 = 2)(input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [p1-1:0] addr, data1 ;
    logic [p2-1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data1) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start,
                          ref data1) ;

endinterface: simple_bus1

interface simple_bus2 (input bit clk) ; // Define the interface

    logic req1, gnt1 ;
    logic [7:0] addr1, data11 ;
    logic [1:0] mode1 ; 
    logic start1, rdy1 ;
    modport slave (input req1, addr1, mode1, start1, clk,
                   output gnt1, rdy1,
                          ref data11) ;
    modport master(input gnt1, rdy1, clk,
                   output req1, addr1, mode1, start1,
                          ref data11) ;

endinterface: simple_bus2

interface simple_bus3 (input bit clk) ; // Define the interface

    logic req2, gnt2 ;
    logic [7:0] addr2, data1 ;
    logic [1:0] mode2 ; 
    logic start2, rdy2 ;
    modport connection1(input req2, addr2, mode2, start2, clk,
                   output gnt2, rdy2, ref data1) ;
    modport connection2(input gnt2, rdy2, clk,
                   output req2, addr2, mode2, start2, ref data1) ;

endinterface: simple_bus3

interface simple_bus4 (input bit clk) ; // Define the interface

    logic req3, gnt3 ;
    logic [7:0] addr3, data1 ;
    logic [1:0] mode3 ; 
    logic start3, rdy3 ;
    modport connection1(input req3, addr3, mode3, start3, clk,
                   output gnt3, rdy3, ref data1) ;
    modport connection2(input gnt3, rdy3, clk,
                   output req3, addr3, mode3, start3, ref data1) ;

endinterface: simple_bus4

module defMod (interface.connection1 d, interface.connection2 e) ;

    always @(posedge d.clk)
        d.data1 = ~e.data1 ; 
    initial
        $display("Instantiating module defMod") ;

endmodule

module abcMod (interface.master a) ; // interface name and modport name

    parameter p1 = 8 ;
    bit [p1-1:0] avail1 ;
    bit [p1-1:0] avail2 ; 
    always @(posedge a.clk) // the clk signal from the interface
        a.addr <= avail1 + avail2 ;
    always @(a.addr)
    begin
        avail1 = a.addr ;
        avail2 = a.addr ;
    end
    initial
        $display("Instantiating module abcMod") ;

endmodule 

module xyzMod (interface.slave b) ;

    parameter p1 = 1, p2 = 1 ;
    always @(posedge b.clk)
        b.gnt1 = p1 & p2 ;
    initial
        $display("Instantiating module xyzMod") ;

endmodule 

module test(input int in, output out) ;

    logic clk = 0 ;
    simple_bus1 #(.p1(16), .p2(3)) sb_intf1(clk) ; // Instantiate the interface
    simple_bus2 sb_intf2(clk) ;

    simple_bus3 sb_con1(clk) ; // Instantiate the interface
    simple_bus4 sb_con2(clk) ;
    
    abcMod #(16) abc(.a(sb_intf1)) ; // Connect the interface to the module instance
    xyzMod xyz(sb_intf2) ;

    defMod def(sb_con1, sb_con2) ;

    assign out = $onehot(in) ;

endmodule 

