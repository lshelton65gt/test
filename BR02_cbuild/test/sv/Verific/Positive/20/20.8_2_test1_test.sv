
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : As per SV LRM IEEE Std 1800 sec 20.8.1, virtual interfaces
// and clocking blocks can be combined to represent the interconnect between
// synchronous blocks. The following design tests this.

interface A_Bus( input bit clk ) ;
    wire req, gnt ;
    wire [7:0] addr, data ;
    clocking sb @(posedge clk) ;
        input gnt ;
        output req, addr ;
        inout data ;
        property p1 ;
            req ##[1:3] gnt ;
        endproperty
    endclocking 
    modport DUT (input clk, req, addr, output gnt, inout data) ;
    modport STB (input gnt, output req, addr, inout data) ;
endinterface 

interface SyncBus( input bit clk ) ;
    wire a, b, c ;
    clocking sb @(posedge clk) ;
        input a ;
        output b ;
        inout c ;
    endclocking
endinterface

typedef virtual SyncBus VI ;    // A virtual interface type

task do_it( VI v ) ;    // handles any SyncBus via clocking sb

    if( v.sb.a == 1 )
        v.sb.b <= 0 ;
    else 
        v.sb.c <= 1 ;
    $display("task do_it called") ;

endtask

module test(input in, output out) ;

    bit clk ;

    SyncBus b1(clk) ;
    SyncBus b2(clk) ;

    initial begin
        VI v[2] ;  = '{b1, b2} ;

        repeat (20)
            do_it (v[$urandom_range(0,1)]) ;
    end
    assign out = in ;

endmodule

program T (A_Bus.STB b1, A_Bus.STB b2) ; // Testbench: 2 synchronous ports
    typedef virtual A_Bus.STB SYNCTB ;

    task request (SYNCTB s) ;
        s.sb.req <= 1 ;
    endtask

     task wait_grant(SYNCTB s) ;
         wait (s.sb.gnt == 1) ;
     endtask

     task drive (SYNCTB s, logic [7:0] adr, data) ;
         if (s.sb.gnt == 0) begin
             request (s) ; // acquire bus if needed
             wait_grant(s) ;
         end
         s.sb.addr = adr ;
         s.sb.data = data ;
         repeat (2) @s.sb ;
         s.sb.req = 0 ;
     endtask

     assert property (b1.p1) ; // assert property from within program

     initial begin
         drive (b1, $random, $random) ;
         drive (b2, $random, $random) ;
     end
endprogram

