
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): To test a case with the following:  
//      Using generic interface along with modport name in module definition.
//      Only those interface definitions which has master & slave  
//      Using multiple interface instantiation.
//      Using multiple instantiation of modules
//                   with specific direction(modport) 

interface simple_bus1 (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data) ;

endinterface: simple_bus1

interface simple_bus2 (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data) ;

endinterface: simple_bus2

module abcMod (interface.master a) ; // interface name and modport name

    parameter p1 = 4, p2 = 4 ;
    bit [7:0] avail1 = p1 ;
    bit [7:0] avail2 = p2 ;
    always @(posedge a.clk) // the clk signal from the interface
        a.addr <= avail1 + avail2 ; // the gnt and req signal in the interface
    initial
        $display("Instantiating module abcMod") ;

endmodule

module xyzMod (interface.slave b) ;

    parameter p1 = 1, p2 = 1 ;
    always @(posedge b.clk)
        b.gnt = p1 & p2 ;
    initial
        $display("Instantiating module xyzMod") ;

endmodule


module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus1 sb_intf1(clk) ; // Instantiate the interface
    simple_bus1 sb_intf2(clk) ; 

    simple_bus2 sb_intf3(clk) ;
    simple_bus2 sb_intf4(clk) ;
    
    abcMod abc11(sb_intf1) ;
    abcMod abc21(sb_intf1) ;
    abcMod #(10,20) abc12(sb_intf2) ;
    abcMod #(10,20) abc22(sb_intf2) ;

    xyzMod #(1,0) xyz11(sb_intf3) ;
    xyzMod #(1,0) xyz21(sb_intf3) ;
    xyzMod xyz12(sb_intf4) ;
    xyzMod xyz22(sb_intf4) ;

    assign out = |in ;

endmodule 

