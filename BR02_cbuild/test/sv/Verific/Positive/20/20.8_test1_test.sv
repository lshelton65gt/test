
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S) : As per SV LRM IEEE Std 1800, sec 20.8, virtual interfaces
// can be declared as class properties, which can be initialized procedurally or
// by an argument to new(). This allows the same virtual interface to be used in
// different classes. The following design tests this.

interface SBus ;    // A Simple bus interface
    logic req, grant ;
    logic [7:0] addr, data ;
endinterface 

class SBusTransactor ;    // SBus transactor class
    virtual SBus bus;    // virtual interface of type Sbus
    function new( virtual SBus s ) ;
        bus = s ;     // initialize the virtual interface
    endfunction 
    task request() ;    // request the bus
        bus.req <= 1'b1 ;
    endtask 
    task wait_for_bus() ;    // wait for the bus to be granted
        @(posedge bus.grant) ;
    endtask 
endclass 

module devA( SBus s ) ;
    initial
        $display("Instantiating module devA") ;
endmodule     // devices that use SBus

module devB( SBus s ) ;
    initial
        $display("Instantiating module devB") ;
endmodule 

module test(input in, output out) ;

    SBus s[1:4] () ;    // instantiate 4 interfaces
    devA a1( s[1] ) ;    // instantiate 4 devices
    devB b1( s[2] ) ;
    devA a2( s[3] ) ;
    devB b2( s[4] ) ;

    initial begin 
        SBusTransactor t[1:4] ;    // create 4 bus-transactors and bind
        t[1] = new( s[1] ) ;
        t[2] = new( s[2] ) ;
        t[3] = new( s[3] ) ;
        t[4] = new( s[4] ) ;
        // test t[1:4]
    end 

    assign out = in ;

endmodule 

