
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface. This interface is instantiated
// within a module and its instance is passed as actual to module using .* port
// connection.

interface simple_bus;
    logic req, gnt;
    logic [7:0] addr, data1;
    logic [1:0] mode; 
    logic start, rdy;
endinterface: simple_bus

module memMod (simple_bus a, input bit clk);
    logic avail;
    always @(posedge clk)
        a.gnt <= a.req & avail;
endmodule 

module cpuMod (simple_bus b, input bit clk);
    logic avail;
    always @(posedge clk)
        b.data1 = ~b.data1 ;
    initial
        $display("clk = %b", clk) ;
endmodule

module test(input in, output byte out) ;

    logic clk = 0;
    simple_bus a(), b();
    memMod mem (.*);
    cpuMod cpu (.*);
    assign out = {4{in, 1'b0}} ;

endmodule

