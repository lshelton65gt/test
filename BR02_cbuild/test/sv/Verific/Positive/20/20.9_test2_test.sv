
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Interfaces can be defined/used without any port. This design defines
// an interface that does not use a port directly, it is used to implement
// avoiding mutual exclusion like condition.

interface iface;

    bit l; /* this is not used directly, instead it is used through lock/unlock task/function */

    task lock;
        while(1'b1 == l)
            #10 ;

        l = 1'b1;
    endtask

    function unlock;
        l = 1'b0;
    endfunction

endinterface

module test(input int in, output int out);

    iface mutex();

    initial
        mutex.unlock();

    always
    begin
        #10 mutex.lock;
    	@(in) out = fn1(in);
    	mutex.unlock();
    end
    
    always
    begin
        #10 mutex.lock;
    	@(in) out = fn2(in);
    	mutex.unlock();
    end

    function int fn1(input int i);
        return (~i+1);
    endfunction

    function int fn2(input int i);
        return -i;
    endfunction

endmodule

