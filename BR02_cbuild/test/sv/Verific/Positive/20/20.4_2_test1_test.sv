
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines an interface with two modport declarations.
// It also defines two modules. The module ports are declared to be of type interface
// (ie. the previously defined interface). They are instantiated using the modports 
// of the declared interface.

interface bus(input logic clk);

    logic [3:0] data1, data2;
    logic [4:0] addr_out;

   modport gen(output data1, data2, input addr_out, clk); 
   modport addr(input data1, data2, clk, output addr_out); 
endinterface

module test(bus b);

initial
begin
    b.data1 = 0;
    b.data2 = 0;
end

always @(posedge b.clk)
begin
    b.data1 += 1;
    b.data2 += 2;
end
endmodule

module add(bus b);

always @(posedge b.clk)
    b.addr_out = b.data1 + b.data2;

endmodule    

