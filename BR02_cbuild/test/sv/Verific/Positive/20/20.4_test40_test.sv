
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): To test the generic bundle with more than one
// interface in the design.

module memMod (interface a, input bit clk);
    logic avail;
    always @(posedge clk) a.gnt <= a.req & avail;
    initial
        $display("Instantiating module memMod") ;
endmodule 
    
module cpuMod(interface b, input bit clk);
    logic avail;
    always @(posedge clk) b.data = ~b.data ;
    initial
        $display("Instantiating module cpuMod") ;
endmodule 

interface simple_bus; // Define the interface
    logic req, gnt;
    logic [7:0] addr, data;
    logic [1:0] mode; 
    logic start, rdy;
endinterface: simple_bus

interface simple_bus1; // Define the interface
    logic  data;
    logic start, rdy;
endinterface: simple_bus1

module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus sb_intf() ;
    simple_bus1 sb_intf1() ;
    simple_bus2 sb_intf2() ; // Interface simple_bus2 is not defined
                             // But for the sake of incermental 
                             // binding at the time of static elab
                             // Analysis should pass this with no
                             // Error or warning.
    memMod mem1 (.a(sb_intf), .clk(clk)) ; 
    memMod mem2 (.a(sb_intf), .clk(clk)) ; 
    memMod mem3 (.a(sb_intf), .clk(clk)) ; 
    memMod mem4 (.a(sb_intf), .clk(clk)) ; 
    memMod mem5 (.a(sb_intf), .clk(clk)) ; 
    memMod mem6 (.a(sb_intf), .clk(clk)) ; 
    memMod mem7 (.a(sb_intf), .clk(clk)) ; 
    memMod mem8 (.a(sb_intf), .clk(clk)) ;     

    cpuMod cpu1 (.b(sb_intf1), .clk(clk)) ;
    cpuMod cpu2 (.b(sb_intf1), .clk(clk)) ;
    cpuMod cpu3 (.b(sb_intf1), .clk(clk)) ;
    cpuMod cpu4 (.b(sb_intf1), .clk(clk)) ;

    assign out = in ;

endmodule 

