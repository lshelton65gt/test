
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE
  
// TESTING FEATURE(S): To test a case with the following:  
//      Using generic interface along with modport name in module definition.
//      Only those interface definitions which has master & slave modports.
//      Using a single interface instantiation
//      Using single instantiation of modules with specific direction(modport) 

interface simple_bus1 (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data1 ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data1) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data1) ;

endinterface: simple_bus1

interface simple_bus2 (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data1 ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data1) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start, ref data1) ;

endinterface: simple_bus2

module abcMod (interface.master a) ; // interface name and modport name

    logic avail ;

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & avail ; // the gnt and req signal in the interface
    initial
        $display("Instantiating module abcMod") ;

endmodule 

module xyzMod (interface.slave b) ;

    always @(posedge b.clk)
        b.data1 = ~b.data1 ;
    initial
        $display("Instantiating module xyzMod") ;

endmodule 

module test(input in, output out) ;

    logic clk = 0 ;
    simple_bus1 sb_intf1(clk) ; // Instantiate the interface
    simple_bus2 sb_intf2(clk) ; // Instantiate the interface
    
    abcMod abc(.a(sb_intf1)) ; // Connect the interface to the module instance
    xyzMod xyz(sb_intf2) ;

    assign out = in ;

endmodule 

