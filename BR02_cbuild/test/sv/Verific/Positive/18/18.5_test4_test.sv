
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design declares a coverage group which 
// specifies cross coverage between two variables and also between a variable
// and two coverpoints.

module test (input clk, output out) ;
    enum {red, green, blue} color ;
    bit[3:0] pixel_adr, pixel_offset, pixel_hue ;

    covergroup g2 @(posedge clk) ;
        Hue    : coverpoint pixel_hue ;
        Offset : coverpoint pixel_offset ;
        Axc    : cross color, pixel_adr ; // cross 2 variables (implicitly declared coverpoints)
        all    : cross color, Hue, Offset ; // cross 1 variable and 2 coverpoints
    endgroup

    assign out = 1'b1 ^ clk ;

    g2 cov = new ;
    initial $display ("coverage = %f", $get_coverage()) ;
endmodule

