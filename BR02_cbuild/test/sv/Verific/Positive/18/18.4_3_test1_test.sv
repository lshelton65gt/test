
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The wildcard  bins  definition causes all X,  Z, or ?  
// to be treated as wildcards for 0 or 1 (similar to the ==? operator). The
// following example shows this.

module test (input clk, output out) ;

    bit reset ;
    reg [4:1] v_a;
    covergroup cg @(posedge clk);
        coverpoint v_a
        {
            wildcard bins g12_16 = { 4'b11?? };
            bins allother = default sequence ; 
        }
    endgroup 
    assign out = 1'b1 ^ clk ;

    cg cov = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule
