
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design declares a coverage group embedded
// within a class. In this example, data members m_x and m_y of class xyz are
// sampled on every change of data member m_z.

module test (input clk, output out) ;
    bit reset ;
    class xyz ;
        bit [3:0] m_x ;
        bit m_z ;
        covergroup cov1 @m_z ;
            coverpoint m_x iff (!reset) ;
        endgroup
        function new () ;
            cov1 = new ;
        endfunction
    endclass
    xyz obj = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;
    assign out = 1'b1 ^ clk ;
endmodule
