
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Cross coverage between expressions previously defined as
// coverage points is also allowed. The following design demonstrates this.

module test (input clk, output out) ;

    bit [3:0] a, b, c ;
    covergroup cov @(posedge clk) ;
        BC: coverpoint b+c ;
        aXb : cross a, BC ;
    endgroup 
    assign out = 1'b1 ^ clk ;

    cov cg = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule

