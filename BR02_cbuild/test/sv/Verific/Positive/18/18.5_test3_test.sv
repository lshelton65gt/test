
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design has a coverage group 'cov' which 
// crosses variable b_var with coverage point A (labeled CC). Variable b_var
// automatically creates 16 bins (auto[0]...auto[15]). Coverage point A 
// explicitly creates 10 bins yy[0]...yy[9].

module test (input clk, output out) ;

    bit [31:0] a_var ;
    bit [3:0] b_var ;
    covergroup cov @(posedge clk) ;
        A: coverpoint a_var { bins yy[] = { [0:9] } ; }
        CC: cross b_var, A ;
    endgroup 
    assign out = 1'b1 ^ clk ;

    cov cg = new ;
    initial $display ("coverage = %f", $get_coverage()) ;

endmodule

