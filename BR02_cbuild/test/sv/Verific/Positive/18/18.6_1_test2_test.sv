
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Type options can be set procedurally at any time during
// simulation. The following design tests this.
// P1800 System Verilog LRM ref : 18.6.1

module test (input clk, output out) ;

    bit [3:0] a, b ;
    bit [7:0] a_var, b_var ;
    covergroup gc @(posedge clk) ;
        a : coverpoint a_var ;
        b : coverpoint b_var ;
    endgroup 

    initial 
    begin
        gc::type_option.comment = "Here is a comment for covergroup" ;
        // Set the weight for coverpoint "a" of covergroup g1
        gc::a::type_option.weight = 3 ;
    end

    gc g1 = new ;
    assign out = 1'b1 ^ clk ;

    gc cov = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule

