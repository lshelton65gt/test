
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a covergroup containing some coverage points.

module test(input clk) ;

    bit [9:0] v_a, v_b ;
    bit b ;
    covergroup cg @(posedge clk) ;
        b : coverpoint v_a
        {
           bins a = {[0:63], 65} ;
           bins a1 = { [1:2], [3:4]} ;
        }
        b1 : coverpoint v_b
        {
           bins a = {[0:63], 65} ;
           bins a1 = { [1:2], [3:4]} ;
        }
        c: cross b, b1
        {
           bins c = binsof (b) ;
        }
    endgroup

    cg cov = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule

