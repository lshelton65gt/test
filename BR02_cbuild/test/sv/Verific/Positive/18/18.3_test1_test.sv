
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design declares a coverage group embedded
// within a class. In this example, data members x and y of class xyz are
// sampled on every change of data member z.

class xyz ;
    bit [3:0] x ;
    bit[0:2] y ;
    bit z ;
    covergroup cov1 @(z) ;
        coverpoint x ;
        coverpoint y ;
    endgroup
    function new () ;
        cov1 = new ;
    endfunction
endclass

module test (input clk, output out) ;
    xyz obj = new ;
    int a, b ;
    initial
    begin
      {obj.x, obj.y, obj.z} = 0 ;
      repeat (256)
          #1 {obj.x, obj.y, obj.z} = {obj.x, obj.y, obj.z} + 1 ;
      $display ("type coverage = %f", obj.cov1.get_coverage()) ;
    end
endmodule
