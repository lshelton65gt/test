
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Cross coverage of a set of N coverage points is defined
// as the coverage of all combinations of all bins associated with the N coverage
// points, that is, the Cartesian product of the N sets of coverage-point bins.
// The following example specifies the cross coverage of two 4-bit variables, a and b. 
// SystemVerilog implicitly creates a coverage point for each variable with each coverage
// point having 16 bins.

module test (input clk, output out) ;

    bit [3:0] a, b ;
    covergroup cov @(posedge clk) ;
        aXb : cross a, b ;
    endgroup 
    assign out = 1'b1 ^ clk ;

    cov cg = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule

