
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a coverpoint containing transition bins.

module test (input clk) ;

    bit [4:1] v_a ;

    covergroup cg @(posedge clk) ;
        coverpoint v_a
        {
            bins sa = (4=>5=>6), ([7:9], 10=>11, 12) ;
            bins sb[] = (4=> 5 => 6), ([7:9], 10=>11, 12) ;
            bins allother = default sequence ;
        }
    endgroup

    cg cov = new ;
    initial $display ("Coverage = %f", $get_coverage()) ;

endmodule

