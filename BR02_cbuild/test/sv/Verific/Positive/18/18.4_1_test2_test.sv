
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a coverpoint containing transition bins.
// LRM (P1800) Reference: 18.4.1

module test (input clk) ;

    bit [4:1] v ;

    covergroup sg @(posedge clk) ;
        coverpoint v
        {
            bins b2 = (2 [-> 3:5]) ;
            bins b3 = (3[->3:5]) ;
            bins b5 = (5 [* 3]) ;
        }
    endgroup

    sg cov = new ;
    initial $display ("Coverage = %f", $get_coverage()) ;

endmodule


