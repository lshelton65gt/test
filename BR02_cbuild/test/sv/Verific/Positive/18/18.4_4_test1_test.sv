
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S):  A set of values or transitions associated with a 
// coverage-point can be explicitly excluded from coverage by specifying
// them as ignore_bins.

module test (input clk, output out) ;

    bit reset ;
    reg [4:1] a ;
    covergroup cg ;
        coverpoint a
        {
            ignore_bins ignore_vals = {7,8} ;
            ignore_bins ignore_trans = (1=>3=>5) ;
        }
    endgroup
    assign out = 1'b1 ^ clk ;

    cg cov = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule

