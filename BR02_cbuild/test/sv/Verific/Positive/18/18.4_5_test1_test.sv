
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A set of values or transitions associated with a coverage-point
// can be marked as illegal by specifying them as illegal_bins. The following design
// demonstrates this.

module test (input clk, output out) ;

    bit reset ;
    reg [4:1] a ;
    covergroup cg ;
        coverpoint a
        {
            illegal_bins bad_vals = {1,2,3} ;
            illegal_bins bad_trans = (4=>5=>6) ;
        }
    endgroup 
    assign out = 1'b1 ^ clk ;

    cg cov = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule

