
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): All values or transitions associated with ignore bins are
// excluded from coverage. Ignored bins or transitions are excluded even if they
// are included in some other bin.

module test (input clk, output out) ;

    bit reset ;
    reg [4:1] a ;
    covergroup cg ;
        coverpoint a
        {
            ignore_bins ignore_vals = {7,8} ;
            bins a = {[1:10]} ; // The values 7,8 should be excluded even from this bin
        }
    endgroup
    assign out = 1'b1 ^ clk ;

    cg cov = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule

