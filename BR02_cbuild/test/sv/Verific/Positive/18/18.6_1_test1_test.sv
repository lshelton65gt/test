
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Different instances of a covergroup  cannot assign different
// values to type options. This is syntactically disallowed, since these options
// can only be initialized via constant expressions. The following design tests this.

module test (input clk, output out) ;

    bit [7:0] a_var, b_var ;
    covergroup g1 (int w, string instComment) @(posedge clk) ; 
        // track coverage information for each instance of g1 in addition 
        // to the cumulative coverage information for covergroup type g1
        option.per_instance = 1 ;
        type_option.comment = "Coverage model for features foo and bar" ;
        type_option.strobe = 1 ;    // sample at the end of the time slot
        // comment for each instance of this covergroup
        option.comment = instComment ;
        a : coverpoint a_var
        {
            // Use weight 2 to compute the coverage of each instance
            option.weight = 2 ;
            // Use weight 3 to compute the cumulative (type) coverage for g1
            type_option.weight = 3 ;
            // NOTE: type_option.weight = w would cause syntax error.
        }
        b : coverpoint b_var 
        {
            // Use weight w to compute the coverage of each instance
            option.weight = w ;
            // Use weight 5 to compute the cumulative (type) coverage of g1
            type_option.weight = 5 ;
        }
    endgroup 
    assign out = 1'b1 ^ clk ;

    g1 cov = new(1, "comment") ;
    initial $display ("coverage = %f", $get_coverage()) ;

endmodule

