
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): All cross products that satisfy the select expression are
// excluded from coverage. Illegal cross-products take precedence over any other
// cross products, i.e., they will result in a run-time error even if they are 
// also explicitly ignored(using ignore_bins) or included in another cross bin.

module test (input clk, output out) ;

    bit [3:0] x, y ;
    covergroup zz(int bad) ;
        coverpoint x
        {
           bins x1 = {[0:63], 65} ;
           bins x2 = { [1:2], [3:4]} ;
        }
        coverpoint y
        {
           bins y1 = {[0:63], 65} ;
           bins y2 = {[1:2], [3:4]} ;
        }
        cross x, y
        {
            illegal_bins foo = binsof(y) intersect {bad} ;
            bins another = binsof(y) intersect{bad} || binsof(x) ;
        }
    endgroup 
    assign out = 1'b1 ^ clk ;

    zz cov = new(1) ;
    initial $display("coverage = %f", $get_coverage()) ;

endmodule

