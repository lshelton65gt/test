
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The instance specific options mentioned above can be set
// in the covergroup definition. The syntax for setting these options in the 
// covergroup definition is:
//      option.<option_name> = <expression> ;
// The identifier option is a built-in member of any coverage group.
// The following design uses this feature.

module test (input clk, output out) ;

    byte a_var, b_var ;
    covergroup g1 (int w, string instComment) @(posedge clk) ;
        // track coverage information for each instance of g1 in addition 
        // to the cumulative coverage information for covergroup type g1
        option.per_instance = 1 ;
        // comment for each instance of this covergroup
        option.comment = instComment ;
        // when true, a warning is issued when there is an overlap between the
        // range list of two bins of a coverpoint.
        option.detect_overlap = 1 ;
        a : coverpoint a_var 
        {
            // Create 128 automatic bins for coverpoint "a" of each instance of g1
            option.auto_bin_max = 128 ;
            bins a1 = {1, 4} ;
        }
        b : coverpoint b_var 
        {
            // This coverpoint contributes w times as much to the coverage of an
            // instance of g1 as coverpoints "a" and "c1"
            option.weight = w ;
            // Specifies the minimum number of hits for this coverpoint. A bin
            // with a hit count which is less than this number is not considered
            // covered.
            option.at_least = 2 ;
            bins b1 = { [1:2], [3:4]} ;
        }
        c1 : cross a_var, b_var ;
    endgroup 
    assign out = 1'b1 ^ clk ;

    g1 cov = new(1, "comment") ;
    initial $display ("coverage = %f", $get_coverage()) ;

endmodule

