
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A group of bins can be excluded from coverage by specifying
// a select expression using ignore_bins. The following design demonstrates this.

module test (input clk, output out) ;

    bit [3:0] a, b ;
    bit [7:0] v_a, v_b ;
    covergroup yy ;
        coverpoint a
        {
           bins a1 = {[0:63], 65} ;
           bins a2 = { [1:2], [3:4]} ;
        }
        coverpoint b ;
        cross a, b
        {
            ignore_bins foo = binsof(a) intersect { 5, [1:3] } ;
        }
    endgroup 
    assign out = 1'b1 ^ clk ;

    yy cov = new ;
    initial $display ("coverage = %f", $get_coverage()) ;

endmodule

