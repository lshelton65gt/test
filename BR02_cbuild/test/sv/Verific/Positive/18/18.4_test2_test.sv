
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a covergroup containing one coverage 
// point with explicitly declared bins.

module test (input clk) ;
    bit [9:0] v_a ;

    covergroup cg @(posedge clk) ;
        coverpoint v_a 
        {
            bins a = { [0:63], 65} ;
            bins b[] = {[127:150], [148:191]} ;
            bins c[] = {200, 201, 202} ;
            bins d = { [1000:$] } ;
            bins others[] = default ;
        }
    endgroup

    cg cov = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule

