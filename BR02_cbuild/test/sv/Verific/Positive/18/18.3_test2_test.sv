
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A class can have more than one 'covergroup's. The following
// design shows two covergroups in a class.

class xyz ;
    logic [3:0] m_x ;
    local logic m_z ;
    bit m_e ;
    bit clk ;
    covergroup cv1 @(posedge clk) ;
        coverpoint m_x ;
    endgroup

    covergroup cv2 @m_z ;
        coverpoint m_e ;
    endgroup
    function new () ;
        m_x = 0 ;
        m_z = 0 ;
        m_e = 0 ;
        clk = 0 ;

        cv1 = new ;
        cv2 = new ;
    endfunction
endclass

module test (input clk, output out) ;
    xyz obj = new ;
    assign out = 1'b1 ^ clk ;
    initial
    begin
        repeat (16)
            #1 obj.m_x = obj.m_x + 1 ;
        $display("coverage = %f", obj.cv1.get_inst_coverage()) ;
        obj.m_e = obj.m_e + obj.clk + 1 ;
        $display("coverage = %f", obj.cv2.get_inst_coverage()) ;
    end
endmodule

