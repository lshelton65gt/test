
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design demonstrates use of some predefined
// coverage methods.

module test (input clk, output out) ;

    bit [7:0] v_a, v_b ;
    covergroup cg ;
        a: coverpoint v_a
        {
            bins a1 = { [0:63] } ;
            bins a2 = { [64:127] } ;
            bins a3 = { [128:191] } ;
            bins a4 = { [192:255] } ;
        }
        b: coverpoint v_b
        {
            bins b1 = {0} ;
            bins b2 = { [1:84] } ;
            bins b3 = { [85:169] } ;
            bins b4 = { [170:255] } ;
        }
    endgroup 
    assign out = 1'b1 ^ clk ;
    cg cov = new ;

    always @(posedge clk) cov.sample() ;

    int covered_bin_value ;
    int num_bins ;

    initial
    begin
        $display ("value of $get_coverage = %f value of the covered bins = %d number of bins for cov = %d", cov.get_coverage(covered_bin_value, num_bins), covered_bin_value, num_bins) ;
        $display ("value of $get_coverage = %f value of the covered bins = %d number of bins for cov = %d", cov.get_inst_coverage(covered_bin_value, num_bins), covered_bin_value, num_bins) ;
        $display ("the coverage number = %f", cov.get_inst_coverage()) ;
        cov.set_inst_name("New Inst") ;
    end

endmodule

