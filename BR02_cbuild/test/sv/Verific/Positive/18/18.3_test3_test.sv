
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design declares a coverage group embedded
// within a class. This covergroup does not have any passed-in arguments and
// uses explicit instantiation to synchronize with another object.

class Helper ;
    int ev ;
endclass

class MyClass ;
    Helper obj ;
    int a ;
    bit [3:0] x ;
    int y ;
    bit z ;
    covergroup cov1 @z ;
        coverpoint x ;
        coverpoint y ;
    endgroup
    function new () ;
        a = 0 ;
        x = 0 ;
        y = 0 ;
        z = 0 ;
        cov1 = new ;
    endfunction
endclass

module test (input clk, output out) ;
    MyClass obj = new ;
    initial
    begin
        $display("%f", $get_coverage()) ;
    end
    assign out = 1'b1 ^ clk ;
endmodule

