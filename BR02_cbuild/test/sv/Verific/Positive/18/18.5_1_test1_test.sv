
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design demonstrates use of user-defined
// cross-coverage and select-expressions.

module test (input clk, output out) ;

    bit [7:0] v_a, v_b ;
    covergroup cg @(posedge clk) ;
        a: coverpoint v_a
        {
            bins a1 = { [0:63] } ;
            bins a2 = { [64:127] } ;
            bins a3 = { [128:191] } ;
            bins a4 = { [192:255] } ;
        }
        b: coverpoint v_b
        {
            bins b1 = {0} ;
            bins b2 = { [1:84] } ;
            bins b3 = { [85:169] } ;
            bins b4 = { [170:255] } ;
        }
        c : cross a, b
        {
            bins c1 = ! binsof(a) intersect {[100:200]} ; // 4 cross products
            bins c2 = binsof(a.a2) || binsof(b.b2) ; // 7 cross products
            bins c3 = binsof(a.a1) && binsof(b.b4) ; // 1 cross product
        }
    endgroup 
    assign out = 1'b1 ^ clk ;

    cg cov = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule

