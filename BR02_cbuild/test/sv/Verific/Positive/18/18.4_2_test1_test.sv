
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design describes a coverage point which does
// not explicitly define any bins. The tool should automatically create three bins.

module test (input clk, output out) ;

    enum {red, green, blue} color ;

    covergroup g1 @(posedge clk) ;
        c: coverpoint color ;
    endgroup

    assign out = clk ;

    g1 cov = new ;
    initial $display ("type coverage = %f", $get_coverage()) ;

endmodule

