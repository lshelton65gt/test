
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The following design shows how arguments are passed to a
// coverage group embedded in a class to set a coverage option of the coverage group.

module test (input clk, output out) ;
    class C1 ;
        bit [7:0] x ;
        covergroup cv  (int arg) @ (posedge clk) ;
            option.at_least = arg ;
            coverpoint x ;
        endgroup
        function new (int p1) ;
            cv = new (p1) ;
        endfunction
    endclass
    assign out = 1'b1 ^ clk ;
    initial begin 
        C1 obj = new (4) ;
        $display("coverage = %f", $get_coverage()) ;
    end
endmodule

