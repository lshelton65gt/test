
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): A group of bins can be marked as illegal by specifying a
// select expression using illegal_bins. The following examples shows this.

module test (input clk, output out) ;

    bit [3:0] x, y ;
    covergroup zz(int bad) ;
        coverpoint x
        {
           bins x1 = {[0:63], 65} ;
           bins x2 = { [1:2], [3:4]} ;
        }
        coverpoint y
        {
           bins y1 = {[0:63], 65} ;
           bins y2 = {[1:2], [3:4]} ;
        }
        cross x, y
        {
            illegal_bins foo = binsof(y) intersect {bad} ;
        }
    endgroup 
    assign out = 1'b1 ^ clk ;

    zz cov = new(1) ;
    initial $display ("coverage = %f", cov.get_inst_coverage()) ;

endmodule

