
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The $bits system function returns the number of bits 
// required to hold an expression as a bit stream. Bit-stream casting for a 
// definition of legal types. A 4 state value counts as one bit. This design
// tests the $bit system function.

module test;

    logic c;
    bit b;

    initial
    begin
        c = 'x; /* should store x in c */
        b = 'x; /* should convert x to 0 and store 0 in b */

        if ($bits(b) != $bits(c))
            $display("$bits system function is not functioning properly!");
        else
            if ($bits(c) != 1)
                $display("$bits system function is not working properly!");
            else
                $display("$bits system function is working properly!");
    end

endmodule

