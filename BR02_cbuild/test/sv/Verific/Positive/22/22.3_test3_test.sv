
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The $bits system function gives the size of a class in bits .

class Student ;

    string name ;
    int roll ;
    string address ;

    function new ;
        name  = "default" ;
        roll = 0 ;
        address = "def" ;
    endfunction

    function void show ;
        $display("name: %s, roll: %d, address: %s", name, roll, address) ;
    endfunction

endclass

module test ;

    Student s1 = new, s2 ;
    typedef bit [$bits(s1) - 1 : 0] totalbits_student; 
    totalbits_student t ; 

    initial begin
        t = totalbits_student'(s1); 
        s2 = Student'(t);  
        s2.show ;
    end

endmodule

