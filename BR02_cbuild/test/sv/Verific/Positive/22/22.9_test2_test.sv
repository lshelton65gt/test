
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $onehot returns true if one and only one bit of its expression
// is high. This design uses $onehot with different expressions.

module test;

    typedef struct {
        reg [3:0] r;
        logic [1:0] l;
    } d_type;

    d_type dt1 = '{ 4'b0010, 2'b00 };
    d_type dt2 = '{ 4'b0010, 2'b01 };
    d_type dt3;

    initial
    begin
        $display("$onehot(512) returns ", $onehot(512));
        $display("$onehot(9'b1) returns ", $onehot(9'b1));
        $display("$onehot(5'b0x010) returns ", $onehot(9'b0x010));
        $display("$onehot(dt1) returns (dt1 = { {0010}, {00} }) ", $onehot(dt1));
        $display("$onehot(dt2) returns (dt2 = { {0010}, {01} })", $onehot(dt2));
        $display("$onehot(dt3) returns (dt3 uninitialized)", $onehot(dt3));

        if($onehot(10000'b01))
            $display("Success in if condition");
        else
            $display("Failure in if condition");
    end

endmodule

