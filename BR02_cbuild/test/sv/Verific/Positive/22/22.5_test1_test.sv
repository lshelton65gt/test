
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Checking use of system functions 'shortrealtobits' and
// 'bitstoshortreal'.

module test ;

    shortreal sr1 = 56.6 ;
    reg [0:31] bit_val1 ;
    reg [0:31] bit_val2 ;
    real r ;
    initial
    begin
        #100
        bit_val1 = $shortrealtobits(sr1) ;
        sr1 = $bitstoshortreal(bit_val1) ;
        $display ("%f ", sr1) ;
    end

endmodule
