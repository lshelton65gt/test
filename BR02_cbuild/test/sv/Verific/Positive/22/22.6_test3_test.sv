
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses array query functions in system 
// task/function mode and method call mode.

module test (i, o) ;

    parameter p = 8 ;

    input [p-1:0] i ;
    output reg [0:p-1] o ;


    reg r [] ;

    initial
    begin
        r = new[p] ;
        for(int idx = 0; idx < p; idx++)
        begin
           r[idx] = i; 
           o ^= r[idx] ;
        end
        $display("%d", $size(r, 1)) ;
        $display("%d", r.size()) ;
    end

endmodule

