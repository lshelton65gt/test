
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $onehot0 returns true if at most one bit of the expression
// is low. This design uses $onehot0 with different expressions.

module test;

    typedef struct {
        reg [3:0] r;
        logic [1:0] l;
    } d_type;

    d_type dt1 = '{ 4'b1011, 2'b10 };
    d_type dt2 = '{ 4'b1111, 2'b11 };
    d_type dt3;

    initial
    begin
        $display("$onehot0(511) returns ", $onehot0(511));
        $display("$onehot0(9'b11111111) returns ", $onehot0(9'b11111111));
        $display("$onehot0(5'b01111) returns ", $onehot0(9'b01111));
        $display("$onehot0(dt1) returns (dt1 = { {1011}, {10} }) ", $onehot0(dt1));
        $display("$onehot0(dt2) returns (dt2 = { {1111}, {11} })", $onehot0(dt2));
        $display("$onehot0(dt3) returns (dt3 un-initialized)", $onehot0(dt3));

        if($onehot0(4'b1101))
            $display("Success in if condition");
        else
            $display("Failure in if condition");
    end

endmodule

