
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $high system function returns the maximum of $left and $right
// bound for a given dimension. This design applies $high() on packed as well as
// unpacked dimensions of an array.

module test;

    bit [-31:-63][1:1][0:-7] dt [0:15][-1:2][0:0];
    /*     4       5     6         1     2    3  */

    initial
    begin
        if (15 != $high(dt, 1))
            $display("$high returned in-correct high value for 1st dimension!");
        else
            $display("$high returned correct high value for 1st dimension");

        if (2 != $high(dt, 2))
            $display("$high returned in-correct high value for 2nd dimension!");
        else
            $display("$high returned correct high value for 2nd dimension");

        if (0 != $high(dt, 3))
            $display("$high returned in-correct high value for 3rd dimension!");
        else
            $display("$high returned correct high value for 3rd dimension");

        if (-31 != $high(dt, 4))
            $display("$high returned in-correct high value for 4th dimension!");
        else
            $display("$high returned correct high value for 4th dimension");

        if (1 != $high(dt, 5))
            $display("$high returned in-correct high value for 5th dimension!");
        else
            $display("$high returned correct high value for 5th dimension");

        if (0 != $high(dt, 6))
            $display("$high returned in-correct high value for 6th dimension!");
        else
            $display("$high returned correct high value for 6th dimension");
    end

endmodule

