
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $dimensions system function returns the number of dimensions
// of an array. This design defines an array and calls $dimensions on it to check
// whether it is giving the correct number of dimensions.

module test #(parameter width = 8);

    int n;
    logic [width-1:0][width/2-1:0][width/4-1:0] dim [width/4-1:0][width/2-1:0];
    /*         3rd         4th          5th              1st          2nd      => dimensions */

    initial
    begin
        n = $dimensions(dim);
        if (5 != n)
            $display("$dimensions returned in-correct number of dimensions!");
        else
            $display("$dimensions returned correct number of dimensions.");
    end

endmodule

