
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses array querying functions on different
// packed/unpacked dimensions.

module test (i, o) ;

    input [31:0] i ;
    output reg [31:0] o ;

    reg [15:0] t1 [0:1] ;
    reg [3:0] t2 [0:1][0:3] ;
    reg [1:0][15:0] t3 ;
    reg t4 [0:3][7:0] ;
    reg t5 [31:0] ;
    reg [31:0] t6 ;

    parameter p1 = $size(t1, 1),
              p2 = $size(t1, 2),
              p3 = $size(t2, 1),
              p4 = $size(t2, 2),
              p5 = $size(t2, 3),
              p6 = $size(t3, 1),
              p7 = $size(t3, 2),
              p8 = $size(t4, 1),
              p9 = $size(t4, 2),
              p10 = $size(t5, 1),
              p11 = $size(t6, 1) ;

    always
    begin
        #5 o = ~i ;
        $display("%d (should be 2)", p1) ;
        $display("%d (should be 16)", p2) ;
        $display("%d (should be 2)", p3) ;
        $display("%d (should be 4)", p4) ;
        $display("%d (should be 4)", p5) ;
        $display("%d (should be 2)", p6) ;
        $display("%d (should be 16)", p7) ;
        $display("%d (should be 4)", p8) ;
        $display("%d (should be 8)", p9) ;
        $display("%d (should be 32)", p10) ;
        $display("%d (should be 32)", p11) ;
        $finish ;
    end

endmodule

