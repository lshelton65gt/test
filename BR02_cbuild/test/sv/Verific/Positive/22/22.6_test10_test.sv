
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $left system function returns the left bound for a given
// dimension. This design applies $left() on packed as well as unpacked dimensions
// of an array.

module test;

    bit [-31:0][1:1][0:-7] dt [0:15][-1:2][0:0];
    /*     4     5     6         1     2    3  */

    initial
    begin
        if (0 != $left(dt, 1))
            $display("$left returned in-correct left bound for 1st dimension!");
        else
            $display("$left returned correct left bound for 1st dimension");

        if (-1 != $left(dt, 2))
            $display("$left returned in-correct left bound for 2nd dimension!");
        else
            $display("$left returned correct left bound for 2nd dimension");

        if (0 != $left(dt, 3))
            $display("$left returned in-correct left bound for 3rd dimension!");
        else
            $display("$left returned correct left bound for 3rd dimension");

        if (-31 != $left(dt, 4))
            $display("$left returned in-correct left bound for 4th dimension!");
        else
            $display("$left returned correct left bound for 4th dimension");

        if (1 != $left(dt, 5))
            $display("$left returned in-correct left bound for 5th dimension!");
        else
            $display("$left returned correct left bound for 5th dimension");

        if (0 != $left(dt, 6))
            $display("$left returned in-correct left bound for 6th dimension!");
        else
            $display("$left returned correct left bound for 6th dimension");
    end

endmodule

