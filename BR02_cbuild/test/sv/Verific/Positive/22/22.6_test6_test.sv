
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $dimensions system function returns the number of dimensions
// of an array. This design applies $dimensions() on an element defined with typedef.

module test;

    typedef reg [7:0][7:0] mem_8x8;

    int n;
    mem_8x8 [3:0] mem [3:0];

    initial
    begin
        n = $dimensions(mem);

        if (4 != n)
            $display("$dimensions returned in-correct number of dimensions!");
        else
            $display("$dimensions returned correct number of dimensions.");
    end

endmodule

