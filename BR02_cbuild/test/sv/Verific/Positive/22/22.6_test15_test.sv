
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $high, $increment and $length functions expect an array
// identifier and a dimension number. If an out-of-range dimension is specified
// then the above functions should return a logic X. This design checks whether
// specifying an  out of bound dimension as an argument to $high, $increment and
// $length system functions actually give logic X value.

module test;

    logic c;
    bit [-31:0][1:1][0:-7] dt [0:15][-1:2][0:0];
    /*     4     5     6         1     2    3  */

    initial
    begin
        c = $high(dt, 9);
        if ('x != c)
            $display("$high does not return X for out of bound array dimension!");
        else
            $display("$high return X for out of bound array dimension");

        c = $increment(dt, -1);
        if ('x != c)
            $display("$increment does not return X for out of bound array dimension!");
        else
            $display("$increment return X for out of bound array dimension");

        c = $length(dt, 2000);
        if ('x != c)
            $display("$length does not return X for out of bound array dimension!");
        else
            $display("$length return X for out of bound array dimension");
    end

endmodule

