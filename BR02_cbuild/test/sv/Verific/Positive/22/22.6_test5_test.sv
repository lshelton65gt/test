
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): The LRM says that for an integer N declared without a range
// specifier, its bounds are assumed to be [$bits(N)-1:0] (16.3). This design applies
// $dimensions system function on an integer number to check that it returns 1,
// because only dimension 1 is defined.

module test;

    int n; /* is same as 'bit [$bits(1)-1:0] n' */
    bit [$bits(n)-1:0] m;

    int i, j;

    initial
    begin
        i = $dimensions(n);
        j = $dimensions(m);

        if (1 == i && 1 == j)
            $display("$dimensions returned correct number of dimensions.");
        else
            if (1 != i)
                $display("$dimensions returned in-correct number of dimensions for 'int'!");
            else
                $display("$dimensions returned in-correct number of dimensions for 'bit [$bits(1)-1:0]'!");
    end

endmodule

