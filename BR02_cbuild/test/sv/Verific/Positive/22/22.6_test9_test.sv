
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $increment system function returns 1 if $left is less than
// or equal to $right for a given dimension. This design applies $increment() on
// packed as well as unpacked dimensions of an array.

module test;

    bit [-31:-63][1:1][0:-7] dt [0:15][-1:2][0:0];
    /*     4       5     6         1     2    3  */

    initial
    begin
        if (-1 != $increment(dt, 1))
            $display("$increment returned in-correct value for 1st dimension!");
        else
            $display("$increment returned correct value for 1st dimension");

        if (-1 != $increment(dt, 2))
            $display("$increment returned in-correct value for 2nd dimension!");
        else
            $display("$increment returned correct value for 2nd dimension");

        if (1 != $increment(dt, 3))
            $display("$increment returned in-correct value for 3rd dimension!");
        else
            $display("$increment returned correct value for 3rd dimension");

        if (1 != $increment(dt, 4))
            $display("$increment returned in-correct value for 4th dimension!");
        else
            $display("$increment returned correct value for 4th dimension");

        if (1 != $increment(dt, 5))
            $display("$increment returned in-correct value for 5th dimension!");
        else
            $display("$increment returned correct value for 5th dimension");

        if (1 != $increment(dt, 6))
            $display("$increment returned in-correct value for 6th dimension!");
        else
            $display("$increment returned correct value for 6th dimension");
    end

endmodule

