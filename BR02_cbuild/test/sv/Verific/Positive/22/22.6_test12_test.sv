
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $low system function returns the minimum of $left and $right
// bound for a given dimension. This design applies $low() on packed as well as
// unpacked dimensions of an array.

module test;

    bit [-31:0][1:1][0:-7] dt [0:15][-1:2][0:0];
    /*     4     5     6         1     2    3  */

    initial
    begin
        if (0 != $low(dt, 1))
            $display("$low returned in-correct low value for 1st dimension!");
        else
            $display("$low returned correct low value for 1st dimension");

        if (-1 != $low(dt, 2))
            $display("$low returned in-correct low value for 2nd dimension!");
        else
            $display("$low returned correct low value for 2nd dimension");

        if (0 != $low(dt, 3))
            $display("$low returned in-correct low value for 3rd dimension!");
        else
            $display("$low returned correct low value for 3rd dimension");

        if (-31 != $low(dt, 4))
            $display("$low returned in-correct low value for 4th dimension!");
        else
            $display("$low returned correct low value for 4th dimension");

        if (1 != $low(dt, 5))
            $display("$low returned in-correct low value for 5th dimension!");
        else
            $display("$low returned correct low value for 5th dimension");

        if (-7 != $low(dt, 6))
            $display("$low returned in-correct low value for 6th dimension!");
        else
            $display("$low returned correct low value for 6th dimension");
    end

endmodule

