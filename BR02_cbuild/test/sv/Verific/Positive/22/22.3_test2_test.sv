
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): Testing $bits to check the size of time data type.

module test ;

   time t;

   initial
   begin
       t = $time;
       $display("sizeof(time) = %d bits", $bits(t));
   end

endmodule

