
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $isunknown returns true if any bit of the expression is X.
// This is equivalent to ^<expression> === 'bx. This design uses $isunknown with
// different expressions.

module test;

    logic x;
    logic [7:0] expr;

    initial
    begin
        if($isunknown(5'b01x10))
            $display("$isunknown working properly");
        else
            $display("$isunknown is not working properly");

        $display("$isunknown(8'b1) returns ", $isunknown(8'b1));

        x = $isunknown(4'b0011);
        if (x)
            $display("$isunknown is not working properly");
        else
            $display("$isunknown is working properly");

        expr = 8'b01x01011;
        if ($isunknown(expr) == (^expr === 'bx))
            $display("Success: $isunknown(expr) is equivalent to (^expr === 'bx)");
        else
            $display("Failure: $isunknown(expr) is not equivalent to (^expr === 'bx)");
    end

endmodule

