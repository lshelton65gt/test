
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $left, $right and $low functions expect an array identifier
// and a dimension number. If an out-of-range dimension is specified then the above
// functions should return a logic X. This design checks whether specifying an out of
// bound dimension as an argument of any of the above mentioned system functions
// actually gives logic X value for $left, $right and $low.

module test;

    logic c;
    bit [-31:0][1:1][0:-7] dt [0:15][-1:2][0:0];
    /*     4     5     6         1     2    3  */

    initial
    begin
        c = $left(dt, 9);
        if ('x != c)
            $display("$left does not return X for out of bound array dimension!");
        else
            $display("$left return X for out of bound array dimension");

        c = $right(dt, -1);
        if ('x != c)
            $display("$right does not return X for out of bound array dimension!");
        else
            $display("$right return X for out of bound array dimension");

        c = $low(dt, 2000);
        if ('x != c)
            $display("$low does not return X for out of bound array dimension!");
        else
            $display("$low return X for out of bound array dimension");
    end

endmodule

