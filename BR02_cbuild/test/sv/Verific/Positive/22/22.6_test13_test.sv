
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): $right system function returns the right bound for a given
// dimension. This design applies $right() on packed as well as unpacked dimensions
// of an array.

module test;

    bit [-31:0][1:1][0:-7] dt [0:15][-1:2][0:0];
    /*     4     5     6         1     2    3  */

    initial
    begin
        if (15 != $right(dt, 1))
            $display("$right returned in-correct right value for 1st dimension!");
        else
            $display("$right returned correct right value for 1st dimension");

        if (2 != $right(dt, 2))
            $display("$right returned in-correct right value for 2nd dimension!");
        else
            $display("$right returned correct right value for 2nd dimension");

        if (0 != $right(dt, 3))
            $display("$right returned in-correct right value for 3rd dimension!");
        else
            $display("$right returned correct right value for 3rd dimension");

        if (0 != $right(dt, 4))
            $display("$right returned in-correct right value for 4th dimension!");
        else
            $display("$right returned correct right value for 4th dimension");

        if (1 != $right(dt, 5))
            $display("$right returned in-correct right value for 5th dimension!");
        else
            $display("$right returned correct right value for 5th dimension");

        if (-7 != $right(dt, 6))
            $display("$right returned in-correct right value for 6th dimension!");
        else
            $display("$right returned correct right value for 6th dimension");
    end

endmodule

