
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: NOT SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design uses array querying functions on different
// packed/unpacked dimensions.

module test (i, o) ;

    input [31:0] i ;
    output reg [31:0] o ;

    reg [15:0] t1 [0:1] ;
    reg [3:0] t2 [0:1][0:3] ;
    reg [1:0][15:0] t3 ;
    reg t4 [0:3][7:0] ;
    reg t5 [31:0] ;
    reg [31:0] t6 ;

    always
    begin
        #5 o = ~i ;
        $display("%d (should be 2)", $size(t1, 1)) ;
        $display("%d (should be 16)", $size(t1, 2)) ;
        $display("%d (should be 2)", $size(t2, 1)) ;
        $display("%d (should be 4)", $size(t2, 2)) ;
        $display("%d (should be 4)", $size(t2, 3)) ;
        $display("%d (should be 2)", $size(t3, 1)) ;
        $display("%d (should be 16)", $size(t3, 2)) ;
        $display("%d (should be 4)", $size(t4, 1)) ;
        $display("%d (should be 8)", $size(t4, 2)) ;
        $display("%d (should be 32)", $size(t5, 1)) ;
        $display("%d (should be 32)", $size(t6, 1)) ;
        $finish ;
    end

endmodule

