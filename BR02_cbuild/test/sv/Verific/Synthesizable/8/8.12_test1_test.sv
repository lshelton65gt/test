
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing for a warning for a size mismatch in a concat.

module test (output bit [1:0] packed_bit);

always
begin
        int test_int = {1'b1, 1'b0};
        packed_bit = {32'b1, 32'b0};
end
endmodule
