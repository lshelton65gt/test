
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test C assignment operators, such as +=, and the C increment
// and decrement operators, ++ and --.

module test (clk, in, out) ;
    input clk, in ;
    output out ;
    int in ;
    wire [31:0] out ;
    int int_variable = in ;

    assign out = int_variable ;
    
    always@(posedge clk)
        int_variable += 10 ;
endmodule

module test2 (clk, in, out) ;
    input clk, in ;
    output out ;
    int in ;
    wire [31:0] out ;
    int int_variable = in ;

    assign out = int_variable ;

    always @ (posedge clk)
        int_variable++ ;
endmodule

module test3 (clk, in, out) ;
    input clk, in ;
    output out ;
    int in ;
    wire [31:0] out ;
    int int_variable = in ;

    assign out = int_variable ;

    always @ (posedge clk)
        int_variable-- ;
endmodule

