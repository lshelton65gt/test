
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design instantiates a module with part selected ports.
// The module applies some arithmatic operations on the input and returns it. The
// returned value is taken in a part selected variable. The input port of the module
// is of type logic.

module test #(parameter WIDTH = 8)(input clk,input signed [WIDTH-1:0] i1, i2,output [WIDTH-1:0] o1, o2);

bottom #(WIDTH/2) b2_1(clk, i1[(WIDTH-1) -: (WIDTH/2)], o2[(WIDTH/2-1):0]);
bottom #(WIDTH/2) b2_2(clk, i2[(WIDTH/2-1):0], o1[(WIDTH-1) -: (WIDTH/2)]);
bottom #(WIDTH/2) b2_3(clk, i1[(WIDTH/2-1):0], o1[(WIDTH/2-1):0]);
bottom #(WIDTH/2) b2_4(clk, i2[(WIDTH-1) -: WIDTH/2], o2[(WIDTH-1) -: (WIDTH/2)]);

endmodule

module bottom #(parameter m = 8)(input clk,input logic [m-1:0] i,output reg [m-1:0] o);

always @(posedge clk)
begin
        o = 0;
        for(int j=0; j<m; j++)
                o += i[j];
end

endmodule

