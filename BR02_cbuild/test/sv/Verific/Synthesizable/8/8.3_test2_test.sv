
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Tests unary increment decrement operators in both pre and post order.

module test(i, i1, o1, t1, o2, t2, o3, t3, o4, t4) ;

    input [8-1:0] i1 ;      // input [42:0] i1 ;
    input [3:0] i ;
    output reg [3:0] o1, o2, o3, o4 ;
    output reg [3:0] t1, t2, t3 ;
    output reg [5:0] t4 ;

    always@(i)
    begin
        t1 = i ;     // t1 = i - 1 ;
        o1 = t1-- ;  // o1 = i ;

        t2 = i ;     // t2 = i - 1 ;
        o2 = --t2 ;  // o2 = i - 1 ;

        t3 = i ;     // t3 = i + 1 ;
        o3 = t3++ ;  // o3 = i ;

        //t4 = i ;     // t4 = i + 1 ;
        t4 = fn(10) ;
        o4 = ++t4 ;  // o4 = i + 1 ;
    end

    function int fn(input int in) ;
        int i ;
        int j ;
        int k ;
        int l ;
        i = ++in ;
        j = i++ ;
        k = --in ;
        l = in-- ;
        fn = j+i+k+l ;
    endfunction
    
endmodule

