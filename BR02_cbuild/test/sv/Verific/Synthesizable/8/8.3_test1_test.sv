
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows += operator. This design uses
// += operator in for-generate statement in step control expression.

module test #(parameter lb=32, parameter rb=16)
             (input clk,
              input [lb-1:rb] in1, in2,
              output reg [lb-1:rb] out1, out2);

genvar i;

generate

    for(i=0; i<lb-rb; i+=1)
    begin: for_gen_i_1
        always@(in1 or in2)
        begin
            out1[rb+i] = in1[rb+i] | in2[rb+i];
            out2[rb+i] = in2[rb+i] ^ in1[rb+i];
        end
    end

    for(i=0; i<rb-lb; i+=1)
    begin: for_gen_i_2
        always@(in1 or in2)
        begin
            out1[lb+i] = in1[lb+i] - in2[lb+i];
            out2[lb+i] = in2[lb+i] & in1[lb+i];
        end
    end

endgenerate

endmodule

