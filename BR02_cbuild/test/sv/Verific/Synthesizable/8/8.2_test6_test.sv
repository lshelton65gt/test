
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses assignment operators like *=. This operator
// is used inside a for-loop to set the value of some local data elements. These
// local data elements are then used to set the value of the output port.

module test(clk, i1, i2, o1, o2, o3, o4);

input clk;
input int i1, i2;
output int o1, o2, o3, o4;

int t2, t4;

always @(posedge clk)
begin
        o1 = (i1 == i2);
        t2 = 1;
        for(int i=0; i<32; i++)
        begin
                if (i1[i] == i2[i]) t2 *= 1'b1; else t2 *= 1'b0;
        end

        t4 = 1;

        for(int i=0; i<32; i++)
        begin
                if (i1[i] != i2[i]) t4 *= 1'b1; else t4 *= 1'b0;
        end
        o4 = t4;
end

assign o2 = ~t2;
assign o3 = (i1 != i2);

endmodule

