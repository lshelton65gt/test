
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two always blocks. The first one is
// sensitive to 4 inputs and uses them to calculate and store values to a local
// variables. The other always block is sensitive to that local variables and the
// one input port. This block calculates and assigns value to the output port.

module test(i1, i2, i3, i4, o1);

parameter IN_WIDTH = 8;
parameter OUT_WIDTH = 8;

input signed [IN_WIDTH-1:0] i1, i2;
input [IN_WIDTH-1:0] i3, i4;
output reg signed [OUT_WIDTH-1:0] o1;

reg signed [OUT_WIDTH-1:0] t1, t2, t3;

always @(i1, i2, i3, i4)
begin
        t1 = i1 * { 1'b1, i4[IN_WIDTH-2:0]};
        t2 = (t1 >>> 2) + i2[IN_WIDTH-1:2];
        t3 = (t2 += i3);
        t3 = ((t2 -= i3) <<< 2) & 2'sb01;
        t3 >>>=2;
end

always @(i4, t3)
begin
    for(int i=0;i<OUT_WIDTH;i++)
    begin
        o1[i] = -t3[i] - -i4;
    end
end

endmodule

