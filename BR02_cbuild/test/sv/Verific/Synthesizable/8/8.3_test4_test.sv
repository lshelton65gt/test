
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses assignment operators in a 'case-generate statement'.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [w-1:0] out1, out2);

    generate
        case(w)  /* synopsys parallel_case */
            0, 1, 2, 4, 8:
            begin
                always@(negedge clk)
                begin
                    out1 *= in1 - in2;
                    out2 = in2 * in1;
                end
            end
            3, 4, 5, 6, 8:
            begin
                always@(negedge clk)
                begin
                    out1 = in1 & in2;
                    out2 += in2 | in1;
                end
            end
            default:
            begin
                always@(negedge clk)
                begin
                    out1 = in1 ^ in2;
                    out2 %= in2 + in1;
                end
            end
        endcase
    endgenerate

endmodule

