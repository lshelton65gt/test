
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): '+', '-' and '~' operators operate on signed numbers and preserve/give
// signedness of a signed element. This design uses them on signed numbers.

module test (input signed [3:0] in1, in2,
             output reg signed [3:0] out1, out2, out3);

    always@(in1 or in2)
    begin
        out1 = in1 + in2;
        out2 = in2 - in1;
        out3 = ~in1;
    end

endmodule

