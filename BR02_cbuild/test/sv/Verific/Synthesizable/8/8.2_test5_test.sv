
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design checks different arithmatic operators on one bit
// signed/unsigned variables. Mainly arithmatic and unary operators are applied on
// the one bit variables.

module test(clk, i1, i2, i3, i4, i5, i6, i7, o1, o2);

input clk;
input i1;
input signed i2;
input i3;
input signed i4;
input i5;
input signed i6;
input i7;
output reg signed o1, o2;

logic l1;
logic signed l2, l3, l4;
logic [7:0] t1;

always @(negedge clk)
begin
    l1 = (i1 + i2) - i3;
    l2 = (i4 - i5) + i6;
    l3 = (i7 * 2) - (i7 << 1);
    l4 = (((i3 + i4) + i2) - i5) * i6;
end

always_comb
begin
    t1={2{l1, l2, l3, l4}};

    if(l1) t1 &= 8'b01110111; else t1 &= 8'b10001000;
    if(l2) t1 &= 8'b10111011; else t1 &= 8'b01000100;
    if(l3) t1 &= 8'b11011101; else t1 &= 8'b00100010;
    if(l4) t1 &= 8'b11101110; else t1 &= 8'b00010001;
end

always_comb
begin
    o1 = (l1 + -l2)|t1;
    o2 = (-l1 - +l2)|t1;
end

endmodule

