
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a 'for-generate' loop. The step control part of
// the 'for-generate' statement has a '?:' operator. It also uses the '+='
// assignment operator.

module test #(parameter w = 8)
             (input clk,
              input signed [w-1:0] in1, in2,
              output reg signed [w-1:0] out1, out2);

    logic signed [w-1:0] t1, t2;

    generate
    genvar i;
        for (i=0; i<w; i+=((2*(i<<1)==i)?2:1))
        begin: for_gen_i_even
            always@(posedge clk)
            begin
                t1[i] = in1[i] | in2[i];
            end
        end

        for (i=1; i<w; i+=((2*(i<<1)!=i)?2:1))
        begin: for_gen_i_odd
            always@(posedge clk)
            begin
                t2[i] = in2[i] & in1[i];
            end
        end
    endgenerate

    always_comb
    begin
        out1 = t1 ^ t2;
        out2 = t2 + t1;
    end

endmodule

