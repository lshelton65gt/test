
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure, it swaps two variables 
// of that type without using a third store, and applies arithmatic and bitwise
// operations on them before and after swap. The swapping is performed using only
// the xor operators and without using a temporary variable.

module test(i1, i2, i3, i4, o1, o2);

parameter WIDTH = 8;
typedef struct packed {
    logic [WIDTH -1:0] x1;
    logic [WIDTH -1:0] x2;
} psdtype;

input [WIDTH-1:0] i1, i2, i3, i4;
output reg [2*WIDTH-1:0] o1, o2;
psdtype t1, t2, t3, t4;

always_comb
begin
        t1 = {i1,i2};
        t2 = {i3,i4};

        t3 = (t1 - t2) * (t2 - t1);
        t4 = (~t2 & ~t1) + (t1 | t2);
        o1 = t4 - t3;

        //start swap
        t1 = t1 ^ t2;
        t2 = t2 ^ t1;
        t1 = t1 ^ t2;
        //done swap

        t3 = (t1 - t2) * (t2 - t1);
        t4 = (~t2 & ~t1) + (t1 | t2);
        o2 = t4 - t3;
end

endmodule

