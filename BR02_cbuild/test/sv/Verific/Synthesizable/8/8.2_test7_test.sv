
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design carries out reduction and, reduction or and 
// reduction xor inside modules. Three modules are defined for the tree operators
// and all of them are instantiated in the other module. Also the same task is
// carried out manually in an always block to check the parity with the working
// of the module version.

module test(in1, out1, out2, out3, out4, out5, out6);

parameter WIDTH = 16;

input [0:WIDTH-1] in1;
output reg out1, out3, out5;
output out2, out4, out6;

always @(in1)
begin
        out1 = &in1;
        out3 = |in1;
        out5 = ^in1;
end

reduction_AND #(.WIDTH(WIDTH)) ra (in1, out2);
reduction_OR #(.WIDTH(WIDTH)) ro (in1, out4);
reduction_XOR #(.WIDTH(WIDTH)) rx (in1, out6);

endmodule

module reduction_AND (in, out);

parameter WIDTH = 16;

input [0:WIDTH-1] in;
output reg out;

integer i;
reg t;

always @(in)
begin
        t = in[0];
        for(i=1; i<WIDTH; i=i+1)
        begin
                t &= in[i];
        end

        out = t;
end

endmodule

module reduction_OR (in, out);

parameter WIDTH = 16;

input [0:WIDTH-1] in;
output reg out;

integer i;
reg t;

always @(in)
begin
        t = in[0];
        for(i=1; i<WIDTH; i=i+1)
        begin
                t |= in[i];
        end

        out = t;
end

endmodule

module reduction_XOR (in, out);

parameter WIDTH = 16;

input [0:WIDTH-1] in;
output reg out;

integer i;
reg t;

always @(in)
begin
        t = in[0];
        for(i=1; i<WIDTH; i=i+1)
        begin
                t ^= in[i];
        end

        out = t;
end

endmodule

