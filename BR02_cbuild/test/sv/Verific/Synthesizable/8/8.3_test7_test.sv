
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog includes the C assignment operators. In this
// test case operators <<<= and >>>= are tested.

module test (clk, in, out) ;
    input clk, in ;
    output out ;
    int in, out ;
    
    always @ (posedge clk)
    begin
        out = in ;
        out <<<= 4;
    end
endmodule

module test2 (clk, in, out) ;
    input clk, in ;
    output out ;
    int in, out ;
    
    always @ (posedge clk)
    begin
        out = in ;
        out >>>= 21;
    end
endmodule

