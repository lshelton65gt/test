
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses '++' operator as step assignment in
// in two nested 'for-generate' loops.

module test (i1, o1) ;

    parameter p1 = 4 ;

    input [p1-1:0][p1-1:0] i1 ;
    output [p1-1:0][p1-1:0] o1 ;

    generate
        genvar i, j ;
        for (i=0; i<p1; i++)
        begin: for_gen_i
            for (j=0; j<p1; j++)
            begin: for_gen_j
                assign o1[i][j] = i1[i][j] ;
            end
        end
    endgenerate
    
endmodule

