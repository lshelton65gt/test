
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses various arithmetic, bitwise, shift operators
// in expressions. Increment and decrement operators are also used. The arithmetic
// operations are performed inside an always block which is triggered when the negative
// edge of clock arrives.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter WIDTH = 16;

input clk;
input signed [WIDTH-1:0] i1, i2, i3, i4;
output reg signed [WIDTH-1:0] o1, o2;

bit l1, l2, l3, l4;
reg [14:0] r1;
reg signed [15:0] r2;
reg signed [16:0] r3;

function int fn(int x,int size);
    
    int ret='0;
    case (size)
    1: ret=2'(x<<1);
    2: ret=3'(x<<2);
    3: ret=4'(x<<3);
    4: ret=5'(x<<4);
    5: ret=6'(x<<5);
    default:;
    endcase
 
    return ret;

endfunction

always @(negedge clk)
begin
        l1 = i2 >> (WIDTH-1);
        l2 = |(i3 << (WIDTH-2));
        l3 = ^(i1 <<< (WIDTH/2));
        l4 = &((i4 >>> WIDTH-1) - (i4 <<< WIDTH-1));

        r1 = (i2 >>> WIDTH/2) - (i1 <<< WIDTH/2);
        r2 = {i3[i4[i1[i2[0]]]], i4[WIDTH-2:0] };
        r3 = (((i4 >> 2) * (i2 >> 2)) >>> 2) <<< 2;

        for(int i=0;i<5;i++)
        begin
            r1 += fn(r1,i);
            r2 -= fn(r2,i++);
            r3 += fn(r3,i--);
        end

        if (l1 - l2)
        begin
             o1 = r1 - r3;
             o2 = r3 + r2;
        end
        else
        begin
             o1 = r3 ^ r1;
             o2 = r2 | r3;
        end
end

endmodule

