
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Checks the increment and decrement operators.

module test(i, i1, o1, t1, o2, t2, o3, t3, o4, t4) ;

    parameter p = fn(10) ;  // p should be 43!
    
    input [p-1:0] i1 ;      // input [42:0] i1 ;
    input [3:0] i ;
    output reg [3:0] o1, o2, o3, o4 ;
    output reg [3:0] t1, t2, t3, t4 ;

    always@(i)
    begin
        t1 = i ;     // t1 = i - 1 ;
        o1 = t1-- ;  // o1 = i ;

        t2 = i ;     // t2 = i - 1 ;
        o2 = --t2 ;  // o2 = i - 1 ;

        t3 = i ;     // t3 = i + 1 ;
        o3 = t3++ ;  // o3 = i ;

        t4 = i ;     // t4 = i + 1 ;
        o4 = ++t4 ;  // o4 = i + 1 ;
    end

    function integer fn(input integer in) ; // for in=10
        integer i ;
        integer j ;
        integer k ;
        integer l ;
        i = ++in ;     // i=11, in=11
        j = i++ ;      // j=11, i=12
        k = --in ;     // k=10, in=10
        l = in-- ;     // l=10, in=9
        fn = j+i+k+l ; // fn=11+12+10+10=43
    endfunction
    
endmodule

