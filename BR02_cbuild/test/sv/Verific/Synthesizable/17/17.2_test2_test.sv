
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'assert' statement on control signal. This control
// signal then drives the output port with an expression. As we have
// used 'assert' on the control signal so the effective result of the
// expression removes that part of the expression which uses ~control with
// multiplication. This is to check if the tool can further simplify
// the synthesized output.

module test #(parameter w = 8)
             (input clk,
              input control,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    always@(posedge clk)
    begin
        assert(control);

        out1 = control*in1 + (~control)*in2;
        out2 = (~control)*in1 | control*in2;
    end

endmodule

