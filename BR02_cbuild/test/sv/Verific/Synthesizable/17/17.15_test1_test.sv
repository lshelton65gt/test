
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines bind directives in $root to add 
// instances in two instantiations of module 'middle'. 

module child1(input in, output out);
  parameter p = 10 ;

  assign out = in << p ;
endmodule

bind test.I1 child1 #(3) I1(in2, shift_out) ;
bind test.I3 child1 #(3) I2(in1, shift_out) ;

module test (in1, in2, out1, out2, out3);
  parameter size1 = 5, size2 = size1 + 1 ;
  input logic [size1 -1 : 0] in1, in2 ;
  output logic [size1 - 1: 0] out1, out3 ;
  output logic [size2 -1 : 0] out2 ;

  middle #(5) I1(in1, in2, out1) ;
  middle #(6) I2(in1 | 6'b101010, in2 | 6'b101010, out2) ;
  middle #(5) I3(in1, in2, out3) ;
endmodule

module middle #(parameter size = 20)(in1, in2, out1) ;
input logic [size -1 : 0 ] in1, in2 ;
output logic [size -1 : 0] out1 ;
wire  shift_out ;

    always @(*)
        out1 = shift_out ^ in1 >> in2 ;
endmodule

