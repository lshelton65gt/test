
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Both the fail and pass statement portion of an immediate 
// assertion can be omitted. This design omits both the pass and fail statement in
// an assert statement.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = in1 * in2;

    assert(in1!=0 && in2==0)
        /* no pass statement */; 
        /* no fail statement */ 

    out2 = in1 + in2;
end

endmodule

