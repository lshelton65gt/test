
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses assert statement on control signal. This control
// signal then drives a '? :' operator as the condition. So the effective
// result of the '? :' operator is always the true part as control is
// always high (int the true part of the 'assert' statement) . This is to check if the tool can further
// simplify the synthesized output.

module test #(parameter w = 8)
             (input clk,
              input control,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    always@(posedge clk)
    begin
        assert(control);

        out1 = (control) ? in1 : in2;
        out2 = (control) ? in2 : in1;
    end

endmodule

