
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an always block. Inside the always block
// it sorts the bits of the input and sets the sorted value to the second output
// port. While the first output port gets swap statistics for the input to be sorted.
// In the always block it uses nested for-loop to sort the input.

module test(clk, i1, o1, o2);

parameter WIDTH = 16;

input clk;
input [WIDTH-1:0] i1;
output reg [WIDTH-1:0] o1, o2;

int i,j;
reg [WIDTH-1:0] r1;
bit flag=0,t=0;

always @(posedge clk)
begin
        r1=i1;
        for(i=0;i<WIDTH;i++)
        begin
            flag=0;
            for(j=0;j<WIDTH-1;j++)
            begin
                if(r1[j]>r1[j+1])
                begin
                    flag=1;
                    t=r1[j];
                    r1[j]=r1[j+1];
                    r1[j+1]=t;
                end
            end
            o1[i]=flag;
        end

        o2 = r1;
end

endmodule

