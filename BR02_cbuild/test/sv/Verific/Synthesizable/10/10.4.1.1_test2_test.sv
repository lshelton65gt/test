
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses nested case constructs. The case items are 
// literal values and case expressions are concatenation of different combinations 
// of input ports. It uses various arithmatic, bitwise and relational operators to 
// perform some calculations. The case statements are placed inside an always block.
  
module test(input clk, input [1:0] i1, i2, input i3, i4, i5, i6, output reg [4:0] o1);

    reg [4:0] r1, r2;

    always@(posedge clk)
    begin 
        case ({i3, i4})
             0: begin 
                    case ({i5, i6})
                        0: r1 = i1 | i2 & i2 + i1;
                        2: r1 = i2 ^ i1 ;
                        3: r1 = (i2 - (i1 & i1[1])) * i2[1];
                        1: r1 = i2 | i1;
                    endcase
                    r2 = r1 + i1;
                end
             1: begin 
                    case ({i6, i5})
                        0: r1 = i1 ^ i2;
                        1: r1 = i2 >= i1 ;
                        default : r1 = i1[1] > i2[0];
                    endcase
                    r2 = r1 - i1;
                end
             2: begin 
                    case ({i5, i5})
                        0: r1 = i2 ^ i1;
                        1: r1 = i1[1:0] + i2[1:0];
                        2, 3: r1 ={i1[0], i2[0]} << {i1[1], i2[1]};
                        default :;
                    endcase
                    r2 = r1 ^ i1;
                end
             3: begin 
                    r1 = { i1, i2 };
                    r2 = { i2, i1 };
                end
        endcase
    end

    always_comb
    begin
        o1 = r1 ^ r2;
    end

endmodule

