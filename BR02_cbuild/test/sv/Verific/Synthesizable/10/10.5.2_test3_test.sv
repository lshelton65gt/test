
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses nested for-loops. Different arithmatic and
// bitwise operators are also used to perform some calculations whose results are 
// assigned to output ports. These operations are performed whenever the positive
// edge of clock pulse arrives.

module test(input clk, 
            input [15:0] in1, in2, 
            output logic [15:0] out1, out2, out3);

    localparam int CYCLES = 10;


    always@(posedge clk)
    begin 
        out1 = 0;
        out2 = 0;
        out3 = 0;
        for (int i = 0; i <= CYCLES; i++)
        begin 
            out1 = out1 + in1;
            for (int j = 0; j <= CYCLES; j++)
            begin
                out2 = in2 * out2;
                for (int k = 0; k <= CYCLES; k++)
                begin
                    out3 = out3 ^ (in1 | in2);
                end
            end
        end
    end

endmodule

