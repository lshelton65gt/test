
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses a task to introduce procedural statements.
// The default direction of ports in System Verilog task is input. This property
// is used in defining the task, also no begin...end block is used in the task.
// Some of the ports of the module has unpacked array dimensions.

module test #(parameter size = 8) 
             (input clk, in1[size-1:0], 
              input [0:size-1] in2, 
              output reg [0:size-1] out [size-1:0]);

reg [0:size-1] r1, r2;

always@(posedge clk)
begin
    for(int i=0; i<size; ++i)
    begin
        r1[i] = ^(in2 ^ {size{in1[i]}});
        r2[i] = in1[i] | in2[size-1-i];
    end

    for(int i=size-1; i>=0; --i)
        proc_task(r1[size-1-i], r2[i], out[i]);
end

task proc_task(a, b /* default direction is input */, output [0:size-1] o);
    reg [size/2-1:0] t1, t2;

    t1 = {size/2{1'b0}};
    t2 = {size/2{1'b1}};

    o = {t1, t2};
endtask: proc_task

endmodule

