
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'continue'  statement to go to the beginning
// of a loop without executing the rest of the statements in the loop.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    for(integer i=0; i<width; i++)
    begin
        r1[i] = in1[i] ^ in2[width-1-i];
        r2[i] = r1[i] + in1[i];
        if (i==width/2)
            continue;
        out1[i] = r1[i] | r2[i];
        out2[i] = r2[i] ^ r1[i];
    end
    out1[width/2] = 1'b0;
    out2[width/2] = 1'b1;
end

endmodule

