
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'unique' keyword with 'if-else-if' conditional expression.
// The keyword 'unique' denotes that there is no overlapping in the conditions
// of the 'if-else-if' statement. But this design uses overlapping conditions.
// Hence, a warning message should be issued on simulation.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1;
reg [0:p-1] r2;

always @(posedge clk)
begin
    r1 = in1 & in2;
    r2 = r1 | in1 * in2;

    unique if (r2[0:1] == 0)
    begin
        out1 = r1 - r2;
        out2 = r1 ^ -r2;
    end
    else if (r2[0] == 1'b0)
    begin
        out1 = -r1 * |r2;
        out2 = r1 ^ -r2;
    end
    else if (r2[0:1] == 2)
    begin
        out1 = r1 & -r2;
        out2 = -r1 ^ -r2;
    end
    else if (r2[1] == 1'b1)
    begin
        out1 = -r1 * -r2;
        out2 = +r1 ^ +r2;
    end
end

endmodule

