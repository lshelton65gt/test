
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses System Verilog break statement to break out
// of a for-loop. The break statement does not require a named
// block. This design defines two nested for-loops, break is
// used to break out of the inner for-loop.

module test(clk, in1, in2, out1);

parameter width = 8;

input clk;
input [width-1:0] in1, in2;
output reg [0:width-1] out1;

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    r1 = '1;
    r2 = '0;
    for(integer i=0; i<width; i++)
    begin
        r1[i] = r1[i] ^ in1[i] & in2[i];
        r2[i] = r2[i] + in1[i] ^ in2[i];
        for(int j=width-1; j>=0; --j)
        begin
            if (i==j)
                break;            // Breaks out of the inner for loop
            r1[j] |= r2[j];
            r2[j] ^= r1[j];
        end
        out1[i] = r1[i] | r2[i];
    end
end

endmodule

