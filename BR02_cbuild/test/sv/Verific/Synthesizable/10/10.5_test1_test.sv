
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines loop control variable inside the loop.
// So this variable becomes visible to only in the loop scope. Define same variable 
// outside the loop also. After the loop executes, use the outer loop control variable, 
// it should get the previous value, not the exit value of the loop control variable.

module test (clk, i, o) ;
    parameter size = 8 ; 

    input clk ; 
    input [(size - 1):0] i ; 
    output reg [(size - 1):0] o ; 

    always@(posedge clk)
    begin
        // Define j to be outside the for-scope
        integer j = 0 ;
        // Define another j to be inside the for-scope
        for (int j = 0 ; (j < size) ; j ++ )
            o[j] = i[((size - j) - 1)] ;

        // Use the first j outside of the for scope
        if ((j == size)) 
            o = ~(o) ;
    end
endmodule

