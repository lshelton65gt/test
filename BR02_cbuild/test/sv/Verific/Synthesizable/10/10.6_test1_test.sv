
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog 'break' statement does not have any effect on the non-blocking
// statement that has already been started. This design checks this.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    for (int i=0; i<width; ++i)
    begin
        r1[i] <= in1[i] - in2[width-1-i];
        r2[i] <= in1[width-1-i] & in2[i];

        out1[i] <= r1[i] | r2[i];   // this should always get the value
        if (i==width-1)
            break;
        out2[i] <= r1[i] & ~r2[i];   // this should not get the value when the condition is true
    end
end

endmodule

