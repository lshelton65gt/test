
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S):  This design uses if-else statements in the place of a case
// statement. It uses an if statement for each of the case items. It is same as the
// case control flow.

module test #(parameter width = 8)
             (input clk,
              input signed [width-1:0] in1, in2,
              output reg signed [width-1:0] out1, out2);

    logic signed [width-1:0] r1, r2, r3, r4;
    logic [1:0] cnd;

    always@(posedge clk)
    begin
        r1 = -in1 - in2;
        r2 = r1 | ~in1 ^ in2;
     
        cnd = { r1[0] & r2[width-1], r1[width-1] + r2[0] };
        if (cnd == 2'sb11)
            r3 = in1 & in2;
        else
        begin
            if (cnd == 2'b00)
                r3 = { in1[width/2-1:0], in2[width-1:width/2] };
            else
            begin
                if (cnd == 2'sb10)
                    r3 = in1 << width/2 | in2 >> width/2;
                else
                begin
                    if (cnd == 2'b01)
                        r3 = in1 * in2;
                    else
                        r4 = r3 ^ (r1 >= r2) & in1 - in2;
                end
            end
        end
    end
     
    always_comb
    begin
        out1 = -r2 & r3;
        out2 = out1 | ~r3 ^ r1;
    end

endmodule

