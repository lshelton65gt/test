
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses always_ff block to introduce procedural statements.
// always_ff block introduces sequential logic.

module test(input clk, reset, input [7:0] in, output reg [7:0] out);

always_ff@(posedge clk or negedge reset)
begin
    if(!reset)
        out = '0;
    else
        out = in;
end

endmodule

