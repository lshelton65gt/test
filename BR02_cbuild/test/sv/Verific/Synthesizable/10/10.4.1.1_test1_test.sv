
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design checks the expression widths of a case statement.
// The width of the condition is 4 bit, but one of the case item uses integer 1,
// making the expression width 32. So all the expressions should be evaluated in 32
// bit width.

module test(clk, i1, i2, o1);

parameter InWidth = 4, OutWidth = 8;
typedef struct packed {
    logic [InWidth-1:0] a;
    logic [InWidth-1:0] b;
} atype;

input clk;
input [InWidth-1:0] i1, i2;
output reg [OutWidth-1:0] o1;

reg [InWidth-1:0] t1 = '1, t2 = '0, t3, t4;
atype a1;
always @(posedge clk)
begin
        case(a1.a)
                4'b0000:        t1 += i1 + i2;
                4'b0001, 1:     t1 += i1 ^ i2;
                4'b1000:        t1 += i1 - i2;
                4'b1001:        t1 += i1 | i2;
                default:        t1 += i1 & i2;
        endcase

        case(a1.b)
                4'b0000:        t2 += i1 + i2;
                4'b0001, 1:     t2 += i1 ^ i2;
                4'b1000:        t2 += i1 - i2;
                4'b1001:        t2 += i1 | i2;
                default:        t2 += i1 & i2;
        endcase

        t3 = t2 ^ i1;
        t4 = t2 & t3;
        o1 = t4 + t3;
end

bottom #(.InWidth(InWidth), .OutWidth(OutWidth)) b1(clk, i1, i2, a1);
endmodule

module bottom( clk, i1, i2, o1);
parameter InWidth = 4, OutWidth = 8;

typedef struct packed {
    logic [InWidth-1:0] a;
    logic [InWidth-1:0] b;
} atype;

input [InWidth-1:0] i1, i2;
input clk;
output atype o1;

always @(posedge clk)
begin
    o1 = i1 ^ i2;
end
endmodule

