
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an always block to introduce procedural statements.
// Both the input and the output ports are multi-dimensional arrays.
// Always block uses two 'for' statements to manipulate these arrays.

module test #(parameter p1 = 8) 
             (input clk,
              input logic [p1-1:0] in [0:p1-1], 
              output reg [0:p1-1] out [p1-1:0]);

always@(posedge clk)
begin
    for(int i=0; i<p1; ++i)
    begin
        out[i] = in[p1-1-i];
    end

    for(int i=0; i<p1; i++)
    begin
        for(int j=0; j<p1; j++)
        begin
            out[j][p1-1-i] = ^out[j] | in[p1-1-j][i];
        end
    end
end

endmodule

