
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'unique' keyword with 'casez' statement. As with the 'if-else-if',
// the keyword 'unique' denotes that there is no overlapping in the conditions
// of 'casez' statement.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1;
reg [0:p-1] r2;

always @(posedge clk)
begin
    r1 = { in1[p-1:p/2], in2[0:p/2-1] };
    r2 = { in1[p/2-1:0], in1[p-1:p/2] };

    unique casez (r1[p/2:p/2-1] - {r2[p-1], r2[0]})
    	2'b00:
        begin
            out1 = r1>>p/2 | r2<<p/2;
            out2 = { r1[p-1:p/2], r2[p/2:p-1] };
        end
    	2'b?1:
        begin
            out1 = r1 - r2;
            out2 = r2 - r1;
        end
    	2'b10:
        begin
            out1 = r1 ^ -r2;
            out2 = r2 ^ -r1;
        end
    endcase
end

endmodule

