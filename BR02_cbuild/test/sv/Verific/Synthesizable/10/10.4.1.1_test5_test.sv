
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S):  This design uses generate statement. It contains a for-generate
// and inside that a case generate statement is used with the corresponding genvar 
// variable used as its condition and the case items are constant

module test #(parameter in_width = 8)
             (input clk,
              input signed [in_width-1:0] in1, in2,
              output reg signed [1*in_width-1:0] out1, out2);

    generate
        genvar i;

        for (i=0; i<in_width; i=i+1)
        begin: for_gen_i
            always @(posedge clk)
            begin
                out1[i] = in1[i] - in2[in_width-i-1];
            end

            case (i)
                1*in_width/4:
                    begin
                        always @(posedge clk)
                        begin
                            out2[in_width/4-1:0] = in1[in_width/4-1:0] & in2[in_width-1:3*in_width/4];
                        end
                    end
                2*in_width/4:
                    begin
                        always @(posedge clk)
                        begin
                            out2[in_width/2-1:in_width/4] = in1[3*in_width/4-1:in_width/2] + in2[in_width/2-1:in_width/4];
                        end
                    end
                3*in_width/4:
                    begin
                        always @(posedge clk)
                        begin
                            out2[3*in_width/4-1:in_width/2] = in1[in_width/2-1:0] ^ in2[in_width-1:in_width/2];
                        end
                    end
                default:
                    begin
                        always @(posedge clk)
                        begin
                            out2[in_width-1:3*in_width/4] = in1[in_width-1:in_width/2] | in1[3*in_width/4-1:0];
                        end
                    end
            endcase

        end

    endgenerate

endmodule

