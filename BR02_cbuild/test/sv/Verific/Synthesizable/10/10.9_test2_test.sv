
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog 'disable' statement does not have any effect on the non-blocking
// statement that has already been started. This design checks this.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 <= in1 | in2;
    r2 <= in2 & in1;

    lbl: out1 <= r1 ^ r2;
    disable lbl;   // out1 should get the value inspite of the disable
    out2 <= r2 + r1;
end

endmodule

