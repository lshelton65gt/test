
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Initializing a variable does not generate an event. If an event
// is required, we have to assign the value inside an 'initial' or
// 'always block'. This design declares and initializes a variable
// inside an 'if-generate' statement, in the else part it assigns value
// to that variable. So in the first case, no event generated but in
// the second case an event is generated.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    bit [w-1:0] t1 = '0;

    generate
        if (w <= 8)
        begin
            byte b = "B";    // no event should be generated

            always@(b)    // catch the event, if any
                t1 = b;
        end
        else
        begin
            byte b;

            always@(posedge clk)
                b = "C";     // event should be generated

            always@(b)    // catch the event
                t1 = b;
        end
    endgenerate

    always@(posedge clk)
    begin
        if (t1 == "B")    // check if initialization fired an event
        begin
            out1 = '0;
            out2 = '0;
        end
        else
        begin
            out1 = in1 - in2;
            out2 = in2 - in1;
        end
    end

endmodule

