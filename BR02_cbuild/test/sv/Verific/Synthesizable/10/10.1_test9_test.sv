
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses Systemverilog return statement inside a function
// to return from the function. The return statement also does not
// require a named block. 'return' statement returns from the function
// immediately.

function void ret_func(input int a, b, output int c);
    int avg;

    avg = a+b;
    avg = avg/2;

    c = avg;

    return;

    c = c*2;
endfunction

module test(clk, in1, in2, out1);

parameter width = 8;

input clk;
input [width-1:0] in1, in2;
output reg [0:width-1] out1;

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    ret_func(in1, in2, r1);
    ret_func(r1, in1|in2, r2);

    ret_func(r1, r2, out1);
end

endmodule


