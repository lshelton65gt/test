
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'priority' keyword with 'casez' statement. As with 'if-else-if'
// the keyword 'priority' denotes that if there is same condition multiple times
// then select the first one.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1;
reg [0:p-1] r2;
reg [1:0] r3;

always @(posedge clk)
begin
    r1 = in1<<p/2 | in2;
    r2 = -in1 + in2>>p/2;
    r3 = { ^r1, |r2 };

    priority casez (r3)
	2'b0?:
        begin
            out1 = r1 ^ r2;
            out2 = out1 | r1;
        end
	2'b?1:
        begin
            out1 = -r1 - r2;
            out2 = -out1 - r2 + r1;
        end
	2'b?0:
        begin
            out1 = ~r1 + r2;
            out2 = ~out1 | r1;
        end
        default:
        begin
            out1 = r1 & -r2;
            out2 = -out1 ^ r2 - -r1;
        end
    endcase
end

endmodule

