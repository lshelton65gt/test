
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'priority' keyword with 'if-else-if' conditional expression.
// The keyword 'priority' denotes that if the same condition occurs multiple times
// then select the first one.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1;
reg [0:p-1] r2;
reg [1:0] r3;

always @(posedge clk)
begin
    r1 = in1 + in2;
    r2 = r1 - in1 | in2;
    r3 = { ^r1, |r2 };

    priority if (0 == r3)
    begin
        out1 = { r1[p/2:0], r2[0:p/2] };
        out2 = r1>>p/2 | r2<<p/2;
    end
    else if (1 == r3)
    begin
        out1 = r1[p-1-:p] | ~(r2[0+:p]);
        out2 = ~r1[p-1:0] - r2[0:p-1];
    end
    else if (2 == r3)
    begin
        out1 = ~r1 ^ ~r2;
        out2 = r1 & r2;
    end
    else if (1 == r3)
    begin
        out1 = r1 + ~r2;
        out2 = ~r1 ^ r2;
    end
    else
    begin
        out1 = r1 * r2;
        out2 = ~r1 - ~r2;
    end
end

endmodule

