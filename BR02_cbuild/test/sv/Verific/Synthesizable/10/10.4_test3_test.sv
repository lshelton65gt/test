
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses unique keyword with casez statement. unique denotes that
// there is no overlapping in the conditions of casez statement, this design
// does not handle all possible conditions and does not provide an explicit
// else. Hence, this design should generate run-time warning.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1;
reg [0:p-1] r2;
reg [0:1] r3;

always @(posedge clk)
begin
    r1 = in1 | in2;
    r2 = in2 ^ in1;
    r3 = {r1[0], in2[p-1]} | {in1[0], r2[p-1]};

    unique casez (r3) // values 00 and 11 will generate runtime warning
    	2'b01:
        begin
            out1 = ~(~r1 ^ ~r2);
            out2 = -(-r1 & -r2);
        end
    	2'b10:
        begin
            out1 = r1 << p/2 | r2 >> p/2;
            out2 = { r1[p-1:p/2], r2[p/2:p-1] };
        end
    endcase
end

endmodule

