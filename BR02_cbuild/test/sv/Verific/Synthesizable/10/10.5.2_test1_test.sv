
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a 'for' loop. The loop first loops from w/2-1 to 0,
// then loop from w/2 to w-1. For this it uses '?:' operator in both the
// condition and step control expressions of the 'for' loop.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [w-1:0] out1, out2);

    always@(posedge clk)
    begin
        /* first loop from w/2-1 to 0, then loop from w/2 to w-1 */
        for(int i=w/2-1; ((i<w/2)?(i>=0):(i<w)); i = ((i<w/2)?
                                                      ((0==i)?(i=w/2):(i-1)):
                                                                             (i+1)))
        begin
            out1[i] = in1[i] | in2[i];
            out2[i] = in1[i] * in2[i];
        end
    end

endmodule

