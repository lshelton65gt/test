
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses Systemverilog 'return' statement inside a function
// to return a value from the function. The return statement also
// does not require a named block. return statement overrides any value
// assigned to the implicit variable of same name as the function.
// return statement does not execute statement followed by itself,
// it returns immediately.

module test(clk, in1, in2, out1);

parameter width = 8;

input clk;
input [width-1:0] in1, in2;
output reg [0:width-1] out1;

reg [width-1:0] r1, r2;

always @(posedge clk)
begin
    r1 = ret_func(in1, in2);
    r2 = ret_func(r1, in1|in2);

    out1 = ret_func(r1, r2);
end

endmodule

function int ret_func(input int a, b);
    int avg;

    avg = a+b;

    ret_func = avg;

    avg = avg/2;

    return avg;

    avg = 2*avg;    
endfunction

