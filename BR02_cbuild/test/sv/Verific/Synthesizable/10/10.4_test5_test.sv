
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'priority' keyword with 'case' statement. As with 'if-else-if',
// the keyword priority denotes that if there is same condition multiple times then select the first one.

module test #(parameter p=8)
             (input clk, 
              input bit [p-1:0] in1, 
              input bit [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1, tmp1, tmp2;
reg [0:p-1] r2;
reg [1:0] r3;

always @(posedge clk)
begin
    tmp1 = in1 ;
    tmp2 = in2 ;
    r1 = --tmp1 - tmp2;
    r2 = tmp1 + ++tmp2;
    r3 = { ~^r1, ~&r2 };

    priority case (r3)
	2'b00:
        begin
            out1 = { r1[p/2:0], r2[0:p/2] };
            out2 = r1<<p/2 ^ r2>>p/2;
        end
	2'b01:
        begin
            out1 = r1[0+:p] * ~(r2[p-1-:p]);
            out2 = ~r1[p-1:0] & r2[0:p-1];
        end
	2'b00:
        begin
            out1 = -r1 | ~r2;
            out2 = r1-- - +r2;
        end
	2'b01:
        begin
            out1 = ~r1 & r2;
            out2 = r1 * ~r2;
        end
        default:
        begin
            out1 = -r1 * -r2;
            out2 = r1 ^ r2;
        end
    endcase
end

endmodule

