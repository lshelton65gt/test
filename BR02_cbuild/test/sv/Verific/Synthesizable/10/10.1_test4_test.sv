
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an always block to introduce procedural statements. Some of
// the input ports are unpacked arrays. The default input width is more than
// the default working variables' width which is less than the output width.
// This checks truncation and extension.

module test #(parameter inwidth=10, 
              parameter outwidth=8, 
              parameter workwidth=(outwidth+inwidth)/2)
             (input logic in1[inwidth-1:0], 
              input logic [0: inwidth-1] in2, 
              output reg [outwidth-1:0] out);

reg [workwidth-1:0] r1;
reg [inwidth-1:0] r2;

always @(*)
begin
    for(int i=0; i<inwidth; i++)
        r2[i] = in2[i] | in1[i];

    r1 = r2 ^ '1;

    out = r1 & in2;
end

endmodule

