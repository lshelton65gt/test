
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'return' statement to return from a function without executing
// the rest of the function. It then executes the statement immediately following the statement that 
// called the function.

function automatic integer ret_func(input integer a, b);
    integer c;

    c = (a-b)/2;

    return c;

    c <<= 1;
endfunction

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = ret_func(in1, in2);
    r2 = ret_func(in2, in1);

    out1 = ret_func(r1, r2);
    out2 = ret_func(r2, r1);
end

endmodule


