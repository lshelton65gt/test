
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog introduced a new keyword 'iff'. It is used
// along with event control expression. This design uses the 'iff' keyword.

module test #(parameter width=8)
             (input clk, enable, load,
              input [0:width-1] data, 
              output reg [width-1:0] out);

always @(posedge clk or posedge load iff enable)
begin
    if (load)
        out = data;
    else
        out = '0;
end

endmodule

