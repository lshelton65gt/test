
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S):  This design implements a function which increases the value
// passed to it by one. The function's return value is then assigned to loop control 
// variable. The function is invoked with loop control variable as argument.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1,
              output reg [w-1:0] out1, out2);

    int i;

    function int step(input int i);
        return (1+i);
    endfunction

    always@(posedge clk)
    begin 
        for (i=0; i<w; i=step(i))
            out1[i] = in1[i];

        for (i=step(w-2); i>=0; i=step(i)-2)
            out2[i] = in1[i];
    end

endmodule

