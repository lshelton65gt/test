
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design finds out the class, network, net_id, host_id of
// a given ip address and the net_mask. This design uses casex statement to determine
// the class of the given ip address. The network information is then stored in a
// packed structure. The class of an ip address is defined as follows...
//
// Class A:
//    IP: 0xxxxxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx => first 8 bit net_id, rest 24 bit host_id
//        Netmask: 255.0.0.0 => without subnet
//        Netmask: 255.255.0.0 => with subnet
// Class B:
//    IP: 10xxxxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx => first 16 bit net_id, rest 16 bit host_id
//        Netmask: 255.255.0.0 => without subnet
//        Netmask: 255.255.255.0 => with subnet
// Class C:
//    IP: 110xxxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx => first 24 bit net_id, rest 8 bit host_id
//        Netmask: 255.255.255.0 => without subnet
//        Netmask: 255.255.255.192 => with subnet
// Class D:
//    IP: 1110xxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx   => multicast address
// Class E:
//    IP: 1111xxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx => reserved for future use

module test (clk, i1, i2, o1, o2, o3, o4);

input clk;
input [31:0] i1, i2;
output reg [7:0] o1;
output reg [31:0] o2;
output reg [23:0] o3, o4;

typedef struct packed {
    logic [7:0] net_class;
    logic [31:0] network;
    logic [23:0] net_id, host_id;
} n_info;

function automatic [7:0] get_net (input [7:0] ip, mask);
begin
        return ((mask == 8'd255)?(8'd255):( (mask == 8'd0)?(8'd0):(mask & ip)));
end
endfunction

function  n_info get_ninfo(input logic [31:0] ip, net_mask);
n_info ret;
        casex(ip[31 -: 4])
                4'b0xxx:
                        begin
                                ret.net_class = "A";
                                ret.net_id = ip[31 -: 8];
                                ret.host_id = ip[23 -: 24];
                        end
                4'b10xx:
                        begin
                                ret.net_class = "B";
                                ret.net_id = ip[31 -: 16];
                                ret.host_id = ip[15 -: 16];
                        end
                4'b110x:
                        begin
                                ret.net_class = "C";
                                ret.net_id = ip[31 -: 24];
                                ret.host_id = ip[7 -: 8];
                        end
                4'b1110:
                        begin
                                ret.net_class = "D";
                                ret.net_id = 23'bx;
                                ret.host_id = 23'bx;
                        end
                4'b1111:
                        begin
                                ret.net_class = "E";
                                ret.net_id = 23'bx;
                                ret.host_id = 23'bx;
                        end
        endcase
        ret.network = {     get_net(ip[31 -: 8], net_mask[31 -: 8]), get_net(ip[23 -: 8], net_mask[23 -: 8]), get_net(ip[15 -: 8], net_mask[15 -: 8]), get_net(ip[7 -: 8], net_mask[7 -: 8]) };
        return ret;

endfunction

always @(posedge clk)
begin
    {o1,o2,o3,o4}=get_ninfo(i1,i2);
end

endmodule

