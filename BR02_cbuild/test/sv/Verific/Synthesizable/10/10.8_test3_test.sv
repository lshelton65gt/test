
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows a matching block name to be specified for a begin
// end block after the 'end' keyword. This design defines two such 'begin-end'
// block having a name after 'end'.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    if (in1>in2)
    begin: in1_greater_than_in2
        r1 = in1 - in2;
        r2 = in1 | in2;
    end: in1_greater_than_in2
    else
    begin: in1_less_than_equal_to_in2
        r1 = in2 + in1;
        r2 = in1 ^ in2;
    end: in1_less_than_equal_to_in2

    out1 = in1 & in2;
    out2 = in1 ^ in2;
end

endmodule

