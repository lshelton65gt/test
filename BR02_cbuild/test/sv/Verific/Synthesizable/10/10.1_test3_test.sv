
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses always_latch block to write procedural statements.
// always_latch block introduces latched logic.

module test(input enable, input [7:0] in1, in2, output reg [7:0] out);

reg [0:7] r1, r2;

always_latch
begin
    if(enable)
    begin
        r1 = in1 + in2;
        r2 = in1 ^ in2;
        out = (in1 & r2) | (r1 - in2);
    end
end

endmodule

