
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S):  This design defines a case, the condition of the case 
// statement is the absolute value of the eight bit input. The case items are all
// constant, except one. The non-constant item is the other input.

module test (input clk,
             input signed [7:0] in1, in2,
             output reg signed [7:0] out1, out2);

    logic signed [0:7] t1, t2;

    always@(posedge clk)
    begin
        case((in1<0)?(-in1):(in1))
            8'd3, 8'd9, 8'd55:
            begin
                t1 = in2 & ~in1;
                t2 = t1 + in1 | in2;
            end
            8'd33, 8'd101, 8'd31:
            begin
                t1 = in1 & in2;
                t2 = in1 * in2>>1 - t1;
            end
            8'd0, 8'd45, 8'd255:
            begin
                t1 = { 4 { &in1 ^ in2 } };
                t2 = ~|t1 ^ ~in1 ^ in2;
            end
            in2:
            begin
                t1 = (in1 + in2);
                t2 = ((t1)?(~in1):(in2));
            end
            default:
            begin
                t1 = |(&in1) & (-in2);
                t2 = t1 * 8'd66;
            end
        endcase
    end

    always_comb
    begin
        out1 = t1 - t2;
        out2 = t2 - t1;
    end

endmodule

