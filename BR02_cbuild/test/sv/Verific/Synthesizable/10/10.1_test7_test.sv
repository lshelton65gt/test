
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses a function to introduce procedural statements.
// No begin...end block is used in the function as this is not
// necessary in System Verilog. Some of the ports of the module has
// unpacked array dimension.

module test #(parameter size = 8) 
             (input clk, input [size-1:0]in1, 
              input [0:size-1] in2, 
              output reg [0:size-1] out [size-1:0]);

reg [0:size-1] r1, r2;

always@(posedge clk)
begin
    for(reg [0:size-1] i=0; i<size; i++)
    begin
        r1 = ((in1[i]&in2[i])?({size/2{in1[i]+in2[i]+2'b00}}):({size/2{{in2[i], in1[i]}}}));
        r2 = r1 ^ in2 - {size{in1[i]}};
        out[i] = proc_fn(r1, r2);
    end
end

function logic [size-1:0] proc_fn(input logic [0:size-1] a, b);
    logic [0:size-1] l1, l2, l3;

    for(integer i=size-1; i>=0; --i)
    begin
        l1[i] = ^{ a[i], b[i] };
        l2[i] = b[i] ^ a[i];
        if (l1[i] == l2[i])
            l3[i] = l1[i] & l2[i];
        else
            l3[i] = l1[i] ^ l2[i];
    end

    proc_fn = l3;
endfunction

endmodule

