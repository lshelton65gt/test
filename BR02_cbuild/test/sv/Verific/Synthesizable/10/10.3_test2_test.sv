
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses a blocking assignment. Value of larger width
// is assigned to a smaller store. Information is lost here.
// In case of information loss, warning may be generated.
// This design checks whether any warning is generated or not.

module test(input clk, input [7:0] in, output reg [0:7] out);

reg [7:0] r1, r2;

always @(posedge clk)
begin
    r1 = 32'd21555 | in;
    r2 = r1 ^ in + 32'b0;

    out = r1 + r2;
end

endmodule

