
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'break' statement to break out of a loop without executing the
// rest of the statements in the loop. After execution of the 'break' statement, the control jumps to the 
// statement immediately after the loop. This design checks this.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    for(integer i=0; i<width; i++)
    begin
        r1[i] = in1[i] ^ in2[width-1-i];
        r2[i] = r1[i] + in1[i];
        if (i==width/2)
            break;
        out1[i] = r1[i] | r2[i];
        out2[i] = r2[i] ^ r1[i];
    end
    out1[width/2:0] = in1[width/2:0] | ~in2[0:width/2];
    out2[width/2:0] = in1[width/2:0] ^ ~in2[0:width/2];
end

endmodule

