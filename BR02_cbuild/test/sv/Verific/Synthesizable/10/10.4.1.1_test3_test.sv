
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S):  This design uses two always blocks the first one is triggered
// whenever a positive edge of clock or a negative edge of reset arrives. Inside this
// always block some value is assigned to a register variable. Whenever the value of 
// the register changes the second always block is invoked. The second always block
// contains a case statement whose case items are parameters.

module test(input clk, input reset, output reg [1:0] out1);

    parameter p1 = 0, p2 = p1+1, p3 = p2+1, p4 = p3+1;

    reg [1:0] r1, r2, r3;

    always@(posedge clk or negedge reset)
    begin 
        if(! reset)
            r1 = p1;
        else
            r1 = r2;
    end

    always@(r1)
    begin 
        case (r1)
            p2 :
            begin 
                r2 = p3;
                r3 = 1;
            end
            p1 :
            begin 
                r2 = p2;
                r3 = 0;
            end
            p4 :
            begin 
                r2 = p1;
                r3 = 3;
            end
            p3 :
            begin 
                r2 = p4;
                r3 = 2;
            end
        endcase
    end

    always_comb
        out1 = r3;

endmodule

