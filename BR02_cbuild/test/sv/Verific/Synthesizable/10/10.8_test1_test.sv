
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): System Verilog allows a label to be specified before any
// statement following a colon. This label can be used to identify the statement.
// Even unnamed blocks can have a label before it. This design uses an unnamed
// begin end block with label.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = in1 * in2;

    if (r1[width-1] == r1[0])
    begin_true: begin
        r2 = in1 | in2;
    end
    else
    begin_false: begin
        r2 = in1 ^ in2;
    end

    if (r2[width-1] != r1[0] && r1[width-1] != r2[0])
    begin_true_out: begin
        out1 = r1 & r2;
        out2 = r1 - r2;
    end
    else
    begin_false_out: begin
        out1 = r1 + r2;
        out2 = r1 | r2;
    end
end

endmodule

