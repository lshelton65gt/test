
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog 'iff' has precedence over 'or'. This design
// checks whether the tool correctly implements this precedence rule.

module test #(parameter width=8)
             (input clk, enable, load,
              input [0:width-1] data, 
              output reg [width-1:0] out1, out2);

always @(posedge clk iff enable or posedge load iff enable)
begin
    if (load)
        out1 = data;
    else
        out1 = 0;
end

always @(posedge clk or posedge load iff enable)
begin
    if (load)
        out2 = data;
    else if (enable)
        out2 = 0;
end

endmodule

