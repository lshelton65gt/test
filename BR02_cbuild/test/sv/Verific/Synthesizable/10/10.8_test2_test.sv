
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows a label to be specified before any statement following
// a colon. This label can be used to identify the statement so it can be used 
// to disable that statement using its label name. This design disables a such
// way a statement having a label before it.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

int i;

always @(posedge clk)
begin
    r1 = 0;
    r2 = 0;

    for (i=0; i<2*width; i++)
    for_loop_body: begin
        if(width==i)
            disable for_loop_body;

        r1[i] = in2[i] * in1[i];
        r2[i] = in1[i] + in2[i];
    end

    out1 = r1 ^ r2;
    out2 = r1 - r2;
end

endmodule

