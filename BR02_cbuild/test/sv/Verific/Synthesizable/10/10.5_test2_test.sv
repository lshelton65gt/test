
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): In SystemVerilog the loop control variable can be declared inside the loop
// itself. This design declares the loop control variable for a for-loop inside
// the loop itself. This control variable is used in the body of the loop also.

module test #(parameter width=8)
             (input clk, 
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1;
reg [0:width-1] r2;

always @(posedge clk)
begin
    r1 = in2;
    r2 = r1 ^ in1;
    for (integer i=0; i<width; i++)
    begin
        out1[i] = |{r1[width-1-i], r2[i]};
        out2[i] = r1[i] ^ r2[width-1-i];
    end
end

endmodule

