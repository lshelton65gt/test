
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing of parameters to specify data types.

module test(out);

parameter type p = bit signed[3:0];
parameter p p1 = 4'b 1100;
output p out;

assign out = p1;

endmodule
