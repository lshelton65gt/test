
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses '?:' operator in declaring range of packed dimension.
// It then uses the same thing in local parameter declaration to set the
// value of the same. The value of the local parameter then used to set
// the value of packed range of a data element.

module test #(parameter w = 8)
             (input clk,
              input [(w<=8 ? 7 : w-1):0] in1, in2,
              output reg [(w<=8 ? 7 : w-1):0] out1, out2);

    localparam p = (w<8 ? 8 : w);

    logic [p-1:0] t1, t2;

    always@(posedge clk)
    begin
        t1 = in2 | in1;
        t2 = in1 - in2;

        out1 = t1 ^ t2;
        out2 = t2 + t1;
    end

endmodule

