
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing of parameters by passing void data type.

module mod #(parameter type p = int)(input wire [31:0] x, y, output int v, z);

function p swap(inout int a, b);
    a = a + b;
    b = a - b;
    a = a - b;
endfunction

int a, b;

always@(x or y)
begin
    a = x;
    b = y;

    swap(a, b);

    v = a;
    z = b;
end

endmodule

module test (input wire [31:0] a, b, output int c, d);
    mod #(.p(void)) instanMod1(a, b, c, d);
endmodule

