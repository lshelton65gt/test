
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing of parameters to specify data types.

module test(in, out);
parameter type p = int;

input int in;
output int out;

function p func;
input p in;
p out = in;
return out; 
endfunction

endmodule


