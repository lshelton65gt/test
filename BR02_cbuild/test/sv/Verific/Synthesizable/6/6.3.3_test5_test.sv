
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design tests type parameter assigning it a structure data type.

typedef struct { logic [0:3] a,b; } MY_TYPE;

module test #(parameter p = 8) (in1, in2, clk, out1);
input [0:p-1] in1, in2;
input clk;
output reg [0:p-1] out1;

parameter type T = MY_TYPE;

T temp1, temp2, temp3;
	
	always @ (posedge clk)
	begin
		temp1.a = in1[0+:4];
		temp1.b = in1[4+:4];

		temp2.a = in2[0+:4];
		temp2.b = in2[4+:4];
		temp3 = f (temp1, temp2);

		out1[0+:4] = temp3.a;
		out1[4+:4] = temp3.b;
	end

    function MY_TYPE f (input MY_TYPE a, b);
	    MY_TYPE t;
	    t.a = a.a + b.a;
	    t.b = a.b + b.b;

	    return t;
    endfunction

endmodule

