
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of variable initialization by constant function call.

typedef enum
        {
           ADD,
           SUB, 
           MULT,
           FACT_10
        } arith_option;

module test(input int in1, in2,
                     input arith_option option,
                     output int result);

    int factorial_10 = fact(10);

    function automatic int fact;
        input int in;
        if(in == 0)
            fact = 1;
        else
            fact = in * fact(in -1);
    endfunction

    always @ *
    begin
       case(option)
          ADD : result = in1 + in2;
          SUB : result = in1 - in2;
          MULT : result = in1 * in2;
          FACT_10 : result = factorial_10;
       endcase
    end

endmodule
     
