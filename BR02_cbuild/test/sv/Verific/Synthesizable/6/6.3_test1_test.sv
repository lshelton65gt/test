
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of localparam.

module test(address, write, read, data, read_data);

    parameter D_WIDTH = 8;
    parameter R_DEPTH = 256;
    localparam adder_width = clogb2(R_DEPTH);

    input [adder_width - 1 :0] address;
    input write, read;
    input [D_WIDTH - 1:0] data;
    output reg [D_WIDTH - 1:0] read_data ;

    function integer clogb2;
        input integer depth;
        integer i, result;
        begin
            for(i = 0; 2 ** i < depth; i = i +1)
                result = i + 1;
            clogb2 = result;
        end
    endfunction

    reg [D_WIDTH - 1:0] data_store [R_DEPTH -1 :0] ;

    always @(write, read, data, address)
    begin
        if(write)
            data_store[address] = data;
        else if(read)
            read_data = data_store[address];
    end

endmodule
