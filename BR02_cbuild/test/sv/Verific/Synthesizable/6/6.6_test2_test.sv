
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that data declared automatic inside a static
// task is initialized at each entry to that task.

module test(input bit in,
                output int count1, count2);

    task my_task;
        output int count1, count2;
        int st_count;
        automatic int auto_count;
        begin
            st_count = st_count + 1;
            auto_count = auto_count + 1;
            count1 = st_count;
            count2 = auto_count;
        end
    endtask

    always @ (in)
    begin
        my_task(count1, count2);        
    end

endmodule


    
