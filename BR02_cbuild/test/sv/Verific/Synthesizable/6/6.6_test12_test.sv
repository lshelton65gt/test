
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows specific data to be declared as static even
// inside an automatic function. These are static in nature and behave
// like regular static variables. This design checks that.

module test (input clk,
             input [3:0] in1,
             output reg [15:0] out1);

    always@(posedge clk)
    begin
        out1 = fibonacci(in1);
    end

    function automatic int fibonacci(int n);
        static int out = 1, in = 0;

        if (in == n)
            return out;

        if (n < 0)
            return -1;

        if (0 == n || 1 == n)
            return 1;

        out = fibonacci(n-1) + fibonacci(n-2);
        in = n;

        return out;
    endfunction

endmodule

