
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S):  This design overrides the type parameter value of a module
// in a case-generate block.

module test(in1, in2, out);
output [0:31] out;
input [0:31] in1, in2;
parameter int type_idx = 0;

typedef bit [0:31] PACKED_INT;
typedef bit UNPACKED_INT [0:31];


    generate 
        case (type_idx)
        2'b00:
            add i1 (.*);
        2'b01:
            begin : a
                PACKED_INT temp1, temp2, temp3;
                assign temp1 = in1;
                assign temp2 = in2;
                assign out = temp3;

                add #(.type_name(PACKED_INT)) i1 (temp1, temp2, temp3);
            end
        default: 
            begin : b
                UNPACKED_INT temp1, temp2, temp3;
                add # (UNPACKED_INT)i1 (temp1, temp2, temp3);
            end
        endcase
    endgenerate

endmodule

module add #(parameter type type_name = int)
(input type_name in1, in2, output type_name out);

    assign out = in1 + in2; // used as vector

endmodule

