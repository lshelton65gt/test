
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Carbon modified version: Test that data declared as automatic inside a static
// function is initialized at each entry to that function (this case is working, it seems we treat every net declared in function as an automatic).

module test(input int in1, in2,
                output int auto_out_1, auto_out_2,
                output int sum_out);
                

    function my_function;
        output int auto_out_1, auto_out_2;
        output int sum_out;
        input int in1, in2;

        automatic int auto_var_1;
        automatic int auto_var_2;

        begin
            auto_out_1 = auto_var_1;
            auto_out_2 = auto_var_2;
           
            auto_var_1 = 20;
            auto_var_2 = 30;
            sum_out = in1 + in2 + auto_var_2 - auto_var_1;
        end
    endfunction
   
    always @ (*)
    begin
        my_function(auto_out_1, auto_out_2, sum_out, in1, in2);
    end

endmodule

        
