
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a constant and assigns different types of values
// to the constant through type casting. This prevents the tool warning message.

module test#(parameter w = 8) 
            (input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    logic [31:0] temp ;
    const int i = int'(const_fn_call(w));     // constant function call returns logic type
    const int j = int'(temp);   // hierarchical reference of logic type

    always@(*)
    begin
        for (int k = 0; k < w; k++)
        begin
            if (i == k)
            begin
                out1[k] = in1[k] & in2[k]; 
                out2[k] = in2[k] - in1[k]; 
            end
            else if (j == k)
            begin
                out1[k] = in1[k] + in2[k]; 
                out2[k] = in2[k] ^ in1[k]; 
            end
            else
            begin
                out1[k] = in1[k] | in2[k]; 
                out2[k] = in2[k] & in1[k]; 
            end
        end
    end

    function logic [31:0] const_fn_call(int n);
        logic [31:0] temp;

        if (n>32)
            temp = 32<<1;
        else
            temp = n<<1;

        return temp;
    endfunction

endmodule

