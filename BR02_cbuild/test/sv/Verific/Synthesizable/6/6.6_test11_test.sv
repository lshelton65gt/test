
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Any data declared inside a module or an interface, but outside a task
// or function, is local to where it is declared and static in lifetime
// w.r.t that module/interface. This design checks that.

`define		OPPOSITE	2'b0
`define		CONJUGATE	2'b1
`define		DEFAULT		default

logic [1:0] g_action;

module test (input signed [3:0] in1, in2,
             output reg signed [3:0] out1, out2);

    typedef struct
    {
        int x;
        int y;
    } point_t;

    point_t point, ret_point;

    task swap;
        ret_point.x = point.y;
        ret_point.y = point.x;
    endtask

    task conjugate;
        ret_point.x = -point.x;
        ret_point.y = -point.y;
    endtask

    task opposite;
        ret_point.x = -point.y;
        ret_point.y = -point.x;
    endtask

    always@(in1 or in2 or g_action)
    begin
        case(g_action)
            `OPPOSITE: begin
                           point.x = in1;
                           point.y = in2;
                           opposite;
                           out1 = ret_point.x;
                           out2 = ret_point.y;
                       end
            `CONJUGATE: begin
                            point.x = in2;
                            point.y = in1;
                            conjugate;
                            out1 = ret_point.x;
                            out2 = ret_point.y;
                        end
            `DEFAULT: begin
                          point.x = in1|in2;
                          point.y = in2^in1;
                          swap;
                          out1 = ret_point.x;
                          out2 = ret_point.y;
                      end
        endcase
    end

endmodule

