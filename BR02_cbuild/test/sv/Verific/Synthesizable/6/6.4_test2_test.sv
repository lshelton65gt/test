
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): A variable can be declared with an initializer, which must be
// constant. As a const value is not set during elaboration, it is
// set after elaboration, so this design assigns the result of an
// arithmetic operation between a 'const' value and parameter/literal/
// localparam when declaring it (initialization).

module test#(parameter w = 8)
            (input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    const int i = 10 * w;      // constant declared with 'const'
    logic [w-1:0] l = i | w;   // initialized with the 'const' value

    always@(*)
    begin
        if (i == l)
        begin
            out1 = in2 + in1;
            out2 = 0;
        end
        else
        begin
            out1 = 0;
            out2 = in1 | in2;
        end
    end

endmodule

