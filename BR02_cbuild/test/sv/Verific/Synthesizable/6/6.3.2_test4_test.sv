
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a parameter of type enumeration. That parameter
// is used inside 'case-generate' statement as case condition and case item conditions. 

typedef enum { A, B, C, D, E, F, G, H} enum_type ;
module test #(parameter [2:0] w = 7, parameter enum_type q = A)
             (input clk,
              input [w:0] in1, in2,
              output reg [w:0] out1, out2);

    generate
        case(q)  /* synopsys full_case */
            A, B, C, D:
            begin
                always@(negedge clk)
                begin
                    out1 = in1 - in2;
                    out2 = in2 * in1;
                end
            end
            E, F, G, H:
            begin
                always@(negedge clk)
                begin
                    out1 = in1 & in2;
                    out2 = in2 | in1;
                end
            end
        endcase
    endgenerate

endmodule

