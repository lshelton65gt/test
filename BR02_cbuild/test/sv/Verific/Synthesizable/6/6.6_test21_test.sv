
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows data to be declared both in named and unnamed
// blocks, unlike Verilog2001 where variables can be defined only
// inside a named block. This design checks that.

module test (input [3:0] in1,
             output reg [3:0] out1);

    always@(in1)
    begin
        int i;    // declared inside an unnamed block;

        for(i=0; i<4; i++)
        begin
            int j;    // declared inside an unnamed block;

            j = in1[i];
            out1[i] = j;
        end
    end

endmodule

