
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines parameter with packed dimension that is
// overridden from another module.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output [w-1:0] out1, out2);

    bottom #(.p(w)) bot1(clk, in1, out2);
    bottom #(.p(w)) bot2(clk, in2, out1);

endmodule

module bottom #(parameter [7:0] p = 8'b0100)
               (input clk,
                input [p-1:0] in,
                output reg [p-1:0] out);

    always@(posedge clk)
    begin
        out = ~in;
    end

endmodule

