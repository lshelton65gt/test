
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): wire w = a; is a continuous assignment, while logic c = b is just
// an initialization. This design checks whether this is maintained by the tool.

module test (input [3:0] in1, in2,
             output reg [3:0] out1, out2);

    bit [3:0] a, b;          // a and b has default bit value '0'.
    wire [3:0] w = a | b;    // changing a or b should also change w as it is a continuous assignment.
    logic [3:0] c = a + b;   // c is immune to change of values of a and b as it is an initialization.

    always@(in1 or in2)
    begin
        a = in1 + in2;
        b = in2 - in1;

        out1 = w;   // w = a | b [== (in1+in2) | (in2-in1)]
        out2 = c;   // c = 0 + 0 [== 0]
    end

endmodule

