
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that static data declared inside a function/task
// can be accessed by hierarchical name but this is not allowed with automatic variables.

module test(input int num, 
                        output int auto_result, static_result,
                        output bit result); 


    function int fact(input int n);
        automatic int result;
        automatic int num;
        int static_result;
        begin
            num = n;
            if(n == 0)
                static_result = 1;
            else
                static_result = n * fact(n -1);
            if(num == 0)
            begin
                result = 1;
            end
            else
            begin
                result = num * fact(num -1);
            end
            return result;
        end
    endfunction

    task compare(output bit result);
        result = auto_result == fact.static_result;
    endtask

    always @ (*)
    begin
        auto_result = fact(num);
        static_result = fact.static_result;
        compare(result);
    end

endmodule
