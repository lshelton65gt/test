
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): As per the LRM, if a data type is not specified while
// declaring variable using 'var' keyword, then the data type logic shall be
// inferred. This design checks that by declaring an array variable with the
// 'var' keyword. 

module test(input logic [15:0] val, output bit out) ;
    var [15:0] v1;    // equivalent to "var logic [15:0] v1;" 
    logic [15:0] v2;
    
    always @ (val)
    begin
        v1 = val ;
        v2 = val ;

        if (v1 === v2)
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

