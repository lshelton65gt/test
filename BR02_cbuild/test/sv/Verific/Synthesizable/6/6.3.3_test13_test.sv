
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing of parameters to specify data types.

typedef struct {
    bit [3:0] b;
    real a;
} myStr;

module test(x, y, out);

parameter type p = myStr;

input p  x, y;
output p out;


function p func (input p x, p y);
    p str;
    str.a = x.a + y.a;
    str.b = y.b - x.b;
    return str;
endfunction

always@(*)
begin
    out = func(x, y);
end

endmodule

