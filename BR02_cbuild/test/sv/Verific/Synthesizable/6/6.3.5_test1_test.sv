
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of keyword const.

module test(input int C,
                  output int F);

    const int faren_low = 32;

    always @ *
        F = (9 * C) / 5 + faren_low;

endmodule
