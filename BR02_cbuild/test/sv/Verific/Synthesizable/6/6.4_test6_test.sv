
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that initializing a variable does not
// generate an event.

module test(output int return_int1, return_int2);

    int local_int1 = 10;

    int local_int2;

    initial
        local_int2 = 30;

    always @ (local_int1)
    begin
        return_int1 = local_int1;
    end

    always @ (local_int2)
        return_int2 = local_int2;

endmodule
