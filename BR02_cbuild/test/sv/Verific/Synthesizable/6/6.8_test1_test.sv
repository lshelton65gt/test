
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test alias declaration, the members of an alias list are
// signals whose bits share the same physical nets. 

module test (output wire [31:0] w, input wire [31:0] a, b); 

    alias w[7:0] = a[7:0] ;
    alias w[31:8] = {b[15:0], a[31:24]};
    
endmodule 

