
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that data declared as automatic inside a static
// function is initialized at each entry to that function.

module test(input int in1, in2,
                output int static_out, auto_out,
                output int sum_out);
                

    function my_function;
        output int static_out, auto_out;
        output int sum_out;
        input int in1, in2;

        int static_var;
        automatic int auto_var;

        begin
            static_out = static_var;
            auto_out = auto_var;
           
            static_var = 20;
            auto_var = 30;
            sum_out = in1 + in2 + auto_var - static_var;
        end
    endfunction
   
    always @ (*)
    begin
        my_function(static_out, auto_out, sum_out, in1, in2);
    end

endmodule

        
