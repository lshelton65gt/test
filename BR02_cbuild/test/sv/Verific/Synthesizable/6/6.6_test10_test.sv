
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that global data can be used anywhere in the design.

`ifdef STATE2
    typedef bit bit_t;
`else
    typedef logic bit_t;
`endif

localparam SIZE = 8;

module test(input bit_t [(SIZE-1) : 0] in1, in2,
                   output bit_t [(SIZE -1) : 0] out);

    function bit_t [(SIZE-1):0] cand
          (input bit_t [(SIZE -1) :0] in1, in2);

         cand = in1 & in2;
    endfunction

    task task_cand(input bit_t [(SIZE-1) : 0] in1, in2,
                   output bit_t [(SIZE -1) : 0] out);

         out = in1 & in2;
    endtask

`ifdef BY_FUNC_CALL
    always @ *
        out = cand(in1, in2);
`else
    always @ *
        task_cand(in1, in2, out);
`endif
       
endmodule

