
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that data can be declared in un named block.

module test(input int in,
                   output int out, static_out, auto_out);
                   


    always @ (in)
    begin : outer_block         // named block
        begin
            int static_var;
            automatic int auto_var;
            static_out = static_var;
            auto_out = auto_var;
        
            static_var = 20;
            auto_var = 30;
            out = in;
        end
    end

endmodule

        
