
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): A variable can be declared with an initializer, which must be
// constant. As a const value is not set during elaboration, it is
// set after elaboration, so this design assigns a const value to a
// variable while declaring it.

module test (input [3:0] in1, in2,
             output reg [3:0] out1, out2);

    const int i = 10;    // constant declared with 'const'
    logic [7:0] l = i;   // initialized with the 'const' value

    always@(*)
    begin
        if (i == l)
        begin
            out1 = in2 + in1;
            out2 = 0;
        end
        else
        begin
            out1 = 0;
            out2 = in1 | in2;
        end
    end

endmodule
