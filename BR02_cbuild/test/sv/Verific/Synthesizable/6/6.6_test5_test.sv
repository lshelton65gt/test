
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that variables can be declared both in named and unnamed blocks.

module test(input int in,
                   output int out1, out2) ;

    always @ (*)
    begin : outer_block
        int offset;
        offset = 10;
        begin // unnamed block
            int offset;
            offset = 20;
            out1 = in + offset; 
        end
    end

    always @ (*)
        out2 = in + outer_block.offset;    

endmodule

 
