
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules. The bottom level module
// defines a type parameter. This type parameter is used to define a new type with
// packed range. Variables of that type is then passed to a function and the return
// type is also of that type.

module test (clk, i1, i2, i3, o1, o2, o3);

parameter PortWidth = 8;

input clk;
input signed [PortWidth-1:0] i1, i2, i3;
output signed [PortWidth-1:0] o1;
output signed [PortWidth-1:0] o2;
output signed [PortWidth-1:0] o3;

bottom #(.PortWidth(PortWidth), .T(reg)) b1(clk, i1, i2, i3, o1, o2, o3);

endmodule

module bottom(clk, i1, i2, i3, o1, o2, o3);
parameter PortWidth = 8;
parameter type T = reg;
input clk;
input signed [PortWidth-1:0] i1, i2, i3;
output reg signed [PortWidth-1:0] o1;
output reg signed [PortWidth-1:0] o2;
output reg signed [PortWidth-1:0] o3;

typedef T [PortWidth-1:0] g_type;

g_type t1, t2;

function g_type fn(g_type x, g_type y);

    if(x>y)
        return x;
    else
        return y;

endfunction

always @(posedge clk)
begin
        t1 = fn(fn(i1, i2), fn(i2, i3));
        if (i1 == t1)
        begin
                o1 = i1;
                t2 = fn(i2, i3);
                if (i2 == t2)
                begin
                        o2 = i2;
                        o3 = i3;
                end
                else
                begin
                        o2 = i3;
                        o3 = i2;
                end
        end
        else
        begin
                if (i2 == t1)
                begin
                        o1 = i2;
                        t2 = fn(i1, i3);
                        if (i1 == t2)
                        begin
                                o2 = i1;
                                o3 = i3;
                        end
                        else
                        begin
                                o2 = i3;
                                o3 = i1;
                        end
                end
                else
                begin
                        o1 = i3;
                        t2 = fn(i1, i2);
                        if (i2 == t2)
                        begin
                                o2 = i2;
                                o3 = i3;
                        end
                        else
                        begin
                                o2 = i3;
                                o3 = i2;
                        end
                end
        end
end

endmodule

