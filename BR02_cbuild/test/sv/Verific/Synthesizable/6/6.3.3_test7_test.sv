
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses type parameters to set the value of output
// ports of modules. While the modules are instantiated the values of the parameters 
// are overridden. This design also performs continuous assignment to any type of
// variable except reg.

module test(i1, i2, o1, o2, o3, o4);

parameter WIDTH=32;

input signed [WIDTH-1:0] i1;
input [WIDTH-1:0] i2;
output signed [WIDTH-1:0] o1, o2;
output [WIDTH-1:0] o3, o4;

bottom1 #(.W1(WIDTH),.T1(bit)) b1 (i1, o1);
bottom1 #(.W1(WIDTH),.T1(logic)) b2 (i2, o2);
bottom2 #(.W2(WIDTH),.T2(bit)) b3 (i1, o3);
bottom2 #(.W2(WIDTH),.T2(logic)) b4 (i2, o4);

endmodule

module bottom1(i1, o1);

parameter W1 = 32;
parameter type T1=logic;

input [W1-1:0] i1;
output T1 [W1-1:0] o1;

assign o1 = ~i1;

endmodule

module bottom2(i1, o1);

parameter W2= 32;
parameter type T2=logic;

input signed [W2-1:0] i1;
output T2 [W2-1:0] o1;

assign o1 = ~i1;

endmodule

