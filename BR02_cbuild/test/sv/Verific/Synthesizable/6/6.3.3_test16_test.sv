
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Parameter of a module can be a type. This design defines a module and
// a type parameter for that. The parameter type is 'byte signed' by default.

module test #(parameter type t = byte signed)
             (input clk,
              input t in1, in2,
              output t out1, out2);

    always@(posedge clk)
    begin
        out1 = in1 | in2;
        out2 = in2 ^ in1;
    end

endmodule

