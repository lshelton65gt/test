
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that data declared inside module or interface
// can be accessed from generate block in the module or interface.

module test (A, B, Cin, Cout, S);
    parameter SIZE = 4;
    input logic [(SIZE-1) :0] A, B;
    input logic Cin;
    output logic Cout;
    output logic [(SIZE-1):0] S;

    genvar i;
 
    logic [(SIZE-1):0] C;

    generate
 
        for(i = 0; i < SIZE; i = i +1)
        begin : blk
            if(i == 0)
                Fadder I(A[i], B[i], Cin, S[i], C[i]);
            else
                Fadder I(A[i], B[i], C[i-1], S[i], C[i]);
                     
        end
        assign Cout = C[(SIZE-1)];
    endgenerate

endmodule

module Fadder(input logic A, B, Cin, output S, C);

    logic A_bar, B_bar, C_bar;

    assign A_bar = ~A, B_bar = ~B, C_bar = ~Cin;

    assign S = (A_bar & B_bar & Cin) | (A_bar & B & C_bar) |
               (A & B & C_bar) | (A & B & Cin);

    assign C = (A_bar & B & Cin) | (A & B_bar & Cin) |
               (A & B & C_bar) | (A & B & Cin);

endmodule
 
