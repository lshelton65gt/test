
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of variable initialization where initial value
// contains parameter.

typedef enum
        {
           ADD,
           SUB, 
           MULT,
           FACT_N
        } arith_option;

module test(input int in1, in2,
                     input arith_option option,
                     output int result);

    parameter N = 10;
    parameter f = fact(N);
    int factorial_n = f;

    function automatic int fact;
        input int in;
        if(in == 0)
            fact = 1;
        else
            fact = in * fact(in -1);
    endfunction

    always @ *
    begin
       case(option)
          ADD : result = in1 + in2;
          SUB : result = in1 - in2;
          MULT : result = in1 * in2;
          FACT_N : result = factorial_n;
       endcase
    end

endmodule
     
