
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of specparam.

module test(input a, b, c, d,
                       output q);

    parameter p = 8;
    logic and_out1, and_out2;

    assign and_out1 = a & b;
    assign and_out2 = c & d;
    assign q = and_out1 & and_out2;

    specify
        specparam a_to_q = 8,
                  b_to_q = 10,
                  c_d_to_q = 18;

        (a => q) = a_to_q;
        (b => q) = b_to_q;
        (c,d *> q) = c_d_to_q;
        
    endspecify

endmodule
