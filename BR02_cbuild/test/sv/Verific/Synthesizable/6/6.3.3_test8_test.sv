
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses type parameter to specify the type of input
// and output ports of a module. The type parameters are, then, overridden while
// instantiating the modules.

module test(clk, i1, i2, o1, o2, o3, o4);

parameter WIDTH=32;

input clk;
input signed [0:WIDTH-1] i1, i2;
output reg signed [WIDTH-1:0] o1;
output signed [15:0] o2;
output signed [31:0] o3;
output signed [63:0] o4;

reg signed [15:0] r1;
reg signed [WIDTH-1:0] r2;
reg signed [31:0] r3;
reg signed [63:0] r4;
reg [WIDTH-1:0] t1, t2, t3;

always @(posedge clk)
begin
    r1 = i2 * i1;
    t1 = (i1[0]?(-i1):i1);
    t2 = (i2[0]?(-i2):i2);
    t3 = (t1 * t2);
    r2 = ((i1[0]^i2[0])?(-t3):t3);
    r3 = (({(~i1[0]), i1[1:WIDTH-1]}) > ({(~i2[0]), i2[1:WIDTH-1]}));
    r4 = i1 > i2;

    if (r1 == r2)
    begin
            o1 = (r1 | r2);
    end
    else
    begin
            o1 = ((r1>r2)?(r1-r2):(r2-r1));
    end
end

    bottom #(.WIDTH(16),.T(shortint)) b1(clk, r1, o2);
    bottom #(.WIDTH(32),.T(int)) b2(clk, r3, o3);
    bottom #(.WIDTH(64),.T(longint)) b3(clk, r4, o4);

endmodule



module bottom (clk, i1, o1);
parameter WIDTH=32;
parameter type T=int;

input clk;
input T i1;
output T o1;
T r1;

always @(posedge clk)
begin
    r1 = i1 ^ ~i1;
    for(int i=0;i<WIDTH;i++)
    begin
        o1[i] = r1++;
    end
end

endmodule

