
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses enumeration type as initial value of type
// parameter. This parameter is overridden by instantiations within a generate block.

typedef struct packed { bit [0:3] f1, f2; } TYPE1;
typedef bit [0:3][0:3] TYPE2;
typedef bit [0:7] TYPE3;
typedef TYPE1 TYPE4;

typedef enum {t1, t2, t3, t4} typeNo;

module test(output out, input [0:15] in);
parameter typeNo p = t1;

    generate
        case (p)
            t1:
                begin : b1
                    TYPE1 in1, in2;
                    assign {in1, in2} = in;
                    bot #(TYPE1)  i1 (.*);
                end : b1
            t2:
                begin : b2
                    TYPE2 in1, in2;
                    assign {in1, in2} = in;
                    bot #(TYPE2)  i1 (.*);
                end : b2
            t3:
                begin : b3
                    TYPE3 in1, in2;
                    assign {in1, in2} = in;
                    bot #(TYPE3)  i1 (.*);
                end : b3
            default:
                begin : b4
                    TYPE4 in1, in2;
                    assign {in1, in2} = in;
                    bot #(TYPE4)  i1 (.*);
                end : b4
        endcase
    endgenerate
endmodule


module bot #(parameter type T = TYPE1)
    (input T in1, input T in2, output out);

    assign out = in1 > in2;

endmodule

