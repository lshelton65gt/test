
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Any data declared inside a function is by default static if the
// function or the data is not explicitly declared as automatic.
// This design defines a variable inside a function to check this.

module test (input clk,
             input signed [3:0] in1, in2,
             output reg signed [3:0] out1, out2);

    int x, y;

    function int abs(int a);
        int b;

        b = ((a<0)?(0-a):(a));

        return b;
    endfunction

    always@(posedge clk)
    begin
        x = abs(in1);
        y = abs.b;

        if (x != y)
            out1 = -1;
        else
            out1 = x;

        x = abs(in2);
        y = abs.b;

        if (x != y)
            out2 = -1;
        else
            out2 = y;
    end

endmodule

