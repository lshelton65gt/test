
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The procedural continuous 'assign' statement overrides the normal
// procedural assignments on logic variables. This design checks that.
// The procedural continuous assignment is deprecated but it is still
// supported in SystemVerilog 3.0.

module test (input [3:0] in1, in2,
             output reg [3:0] out1, out2);

    logic [3:0] b1, b2;

    always@(in1)
    begin
        assign b1 = 1;
        assign b2 = 0;
        b1 = in1;
        b2 = in1;
        deassign b2;
        b2 = ~in2;

        out1 = b1;
        out2 = b2;
    end

endmodule

