
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that any data declared inside a module or
// interface can be accessed from any construct within the module.

module test(input int in1, in2, 
                    output int out,
                    input int option);

    enum 
    {
       AND = 10,
       OR,
       XOR
    } op_type;

    always @(*)
    begin
        case(option)
            1 : op_type = AND;
            2 : op_type = OR;
            default : op_type = XOR;
        endcase

        out = op_func(in1, in2);
    end

    function int op_func(input int in1, in2);
        case (op_type)
            AND : op_func = in1 & in2;
            OR : op_func = in1 | in2;
            XOR : op_func = in1 ^ in2;
        endcase
    endfunction

endmodule
