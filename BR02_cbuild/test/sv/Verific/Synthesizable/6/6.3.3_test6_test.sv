
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design models an if-generate block. Depending on the
// flow of the if-generate it defines two instantiations inside it - one without
// any parameter override, one with normal parameter override and the other with
// both normal and type parameter override. The bottom module has two parameters
// one of which is type parameter.

module test(i1, i2, i3, i4, o1, o2, o3, o4);

parameter WIDTH=8;

input signed [WIDTH-1:0] i1, i2, i3, i4;
output signed [WIDTH-1:0] o1, o2, o3, o4;

generate
        if (WIDTH < 8)
        begin
                assign o1 = (i1-i2) | (i3+i4);
                assign o2 = (i1|i2) + (i3&i4);
                bottom b1(i1, i2, o3);
                bottom b2(i3, i4, o4);
        end
        else
        begin
                if (WIDTH < 16)
                begin
                        assign o1 = i1[7:0] * i2[7:0];
                        assign o2 = i3[7:0] + i4[7:0];
                        bottom #(.W(WIDTH)) b1(i1, i2, o3);
                        bottom #(.W(WIDTH)) b2(i3, i4, o4);
                end
                else
                begin
                        assign o1 = i1[15:8] | i2[7:0];
                        assign o2 = i3[7:0] & i4[15:0];
                        bottom #(.W(WIDTH),.T(logic)) b1(i1, i2, o3);
                        bottom #(.W(WIDTH),.T(logic)) b2(i3, i4, o4);
                end
        end
endgenerate

endmodule

module bottom #(parameter W=8, parameter type T=logic)(input T [W-1:0] i1, i2, output reg [W-1:0] o1);

always_comb
begin
    for(int i=0; i<W; i++)
    begin
        o1[i] = ^{i1[i],i2[i]}; 
    end
end

endmodule

