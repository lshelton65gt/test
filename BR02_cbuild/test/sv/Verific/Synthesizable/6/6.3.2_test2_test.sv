
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses '? :' operator in overriding parameter value of an
// instantiation. It then uses the same thing in local parameter
// declaration to set the value of the same. The value of the local
// parameter is used to set the value of range of a packed array.

module test (clk, in1, in2, out1, out2);

parameter w = 8;

localparam p = (w<=8?8:w);

input clk;
input [p-1:0] in1, in2;
output [p-1:0] out1, out2;

    bottom #(.p(p)) bot1(clk, in2, out1);
    bottom #(.p(w<=8?8:w)) bot2(clk, in1, out2);

endmodule

module bottom #(parameter p = 4)
               (input clk,
                input [p-1:0] in,
                output reg [0:p-1] out);

    always@(posedge clk)
    begin
        out = ~in;

        out += 1;
    end

endmodule

