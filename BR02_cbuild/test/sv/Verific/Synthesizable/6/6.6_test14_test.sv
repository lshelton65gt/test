
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Any data declared outside of module, interface, task and function
// is global in scope and static in lifetime. So the variable can be
// used anywhere in the design after its declaration. This design
// defines a variable in another file and `includes that file.

`include "defs.v"

module test (input signed [3:0] in1, in2,
             output reg signed [3:0] out1, out2);

    point_t point, ret_point;

    always@(in1 or in2 or g_action)
    begin
        case(g_action)
            `OPPOSITE: begin
                           point.x = in1;
                           point.y = in2;
                           ret_point = opposite(point);
                           out1 = ret_point.x;
                           out2 = ret_point.y;
                       end
            `CONJUGATE: begin
                            point.x = in2;
                            point.y = in1;
                            ret_point = conjugate(point);
                            out1 = ret_point.x;
                            out2 = ret_point.y;
                        end
            `DEFAULT: begin
                          point.x = in1|in2;
                          point.y = in2^in1;
                          ret_point = swap(point);
                          out1 = ret_point.x;
                          out2 = ret_point.y;
                      end
        endcase
    end

endmodule

