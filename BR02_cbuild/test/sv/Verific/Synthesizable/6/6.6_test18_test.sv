
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function with a static variable in it.
// Depending on the value of that static variable it returns different values from
// the function and increments the value of the static variable. It models behavior
// of something like a state machine. The function is called from an always block
// four times. The value of the output ports are set from the return values of
// that function call.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter IN_WIDTH=16, OUT_WIDTH=18;

input clk;
input signed [IN_WIDTH-1:0] i1, i2;
input [IN_WIDTH-1:0] i3, i4;
output reg [OUT_WIDTH-1:0] o1;
output reg signed [OUT_WIDTH-1:0] o2;

reg signed [(IN_WIDTH-1):0] r1, r2;
reg [(OUT_WIDTH+31):32] r3, r4;
function int fn(reg [IN_WIDTH-1:0] i1,reg [IN_WIDTH-1:0] i2,reg [IN_WIDTH-1:0] i3,reg [IN_WIDTH-1:0] i4);
static int i = 0;
    
    if(i>3)
        i -=3;
    case (i)
    0:
    begin
        i++;
        return { { IN_WIDTH/4 { 1'b0 } }, i1[IN_WIDTH/4-1:0], { IN_WIDTH/4 { 1'b1 } } };
    end
    1:
    begin
        i++;
        return ((i2 - r1) >> IN_WIDTH/2) * ((i1>i2)?(i1[IN_WIDTH/2-1:0]):(i2[IN_WIDTH/2:0]));
    end
    2:
    begin
        i++;
        return (i3 - i2 + i1 - r2);
    end
    3:
    begin
        i++;
        return  ((r3 >> IN_WIDTH/2) + (i4 << IN_WIDTH/2));
    end
    endcase
endfunction

always @ (negedge ~clk)
begin
    
    r1 = fn(i1,i2,i3,i4);
    r2 = fn(i1,i2,i3,i4);
    r3 = fn(i1,i2,i3,i4);
    r4 = fn(i1,i2,i3,i4);

    o1 = signed'((r1 - r3)^(r4 + r2));
    o2 = unsigned'((r2 + r3)|(r1 - r4));
end

endmodule

