
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines four modules and instantiates three modules
// from the other module. All the instantiated modules have type parameter and all of
// them are over-ridden while instantiations. All of the instantiated modules uses
// an always_comb block inside them.

module test(i1, i2, o1, o2, o3, o4, o5, o6);

parameter WIDTH = 16;

input signed [WIDTH-1:0] i1;
input [WIDTH-1:0] i2;
output [WIDTH-1:0] o1, o2, o3, o4, o5, o6;

reg [WIDTH-1:0] t1, t3, t5;

always @(i1, i2)
begin
        t1 = i1 & i2;
        t3 = i1 | i2;
        t5 = i1 ^ i2;
end

assign o1 = t1;
assign o3 = t3;
assign o5 = t5;

bottom1 #(.WIDTH(WIDTH), .T(int)) b1 (i1, i2, o2);
                 
bottom2 #(.WIDTH(WIDTH), .T(shortint)) b2 (i2, i1, o4);
                 
bottom3 #(.WIDTH(WIDTH), .T(longint)) b3 (i1-i2, i2-i1, o6);

endmodule

module bottom1(i1, i2, o);

parameter WIDTH = 32;
parameter type T = int;

input T i1, i2;
output T o;

always_comb
begin
        for(int i=0; i<WIDTH; i++)
        begin
                o[i] = (i1[i] & i2[i]);
        end
end

endmodule

module bottom2(i1, i2, o);

parameter WIDTH = 32;
parameter type T = int;

input T i1, i2;
output T o;

always_comb
begin
        for(int i=0; i<WIDTH; i++)
        begin
                o[i] = (i1[i] | i2[i]);
        end
end

endmodule

module bottom3(i1, i2, o);

parameter WIDTH = 32;
parameter type T = int;

input T i1, i2;
output T o;

always_comb
begin
        for(int i=0; i<WIDTH; i++)
        begin
                o[i] = (i1[i] ^ i2[i]);
        end
end

endmodule

