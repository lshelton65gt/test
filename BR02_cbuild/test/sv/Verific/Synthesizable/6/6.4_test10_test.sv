
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): In SystemVerilog, initializing a variable does not generate an event.
// If an event is required then the value should be assigned inside an
// initial or always block. This design checks that.

module test (input [3:0] in1, in2,
             output reg [3:0] out1, out2);

    byte b = { 1'b1, 1'b0,  1'b1, 1'b0,  1'b1, 1'b0,  1'b1, 1'b0 }; // initialize b

    always@(in1 or in2)
    begin
        out1 = b | in2 - in1;
    end

    always @(b)// should not fire this block
    begin
        out2 = in1 & in2;
    end

endmodule

