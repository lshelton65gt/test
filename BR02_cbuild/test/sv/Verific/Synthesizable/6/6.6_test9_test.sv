
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that global data can be used anywhere in the design.

`ifdef STATE2
    typedef bit bit_t;
`else
    typedef logic bit_t;
`endif

localparam SIZE = 8;

module test(input bit_t [(SIZE-1) : 0] in1, in2,
                   output bit_t [(SIZE -1) : 0] out);

    cand I(out, in1, in2);

endmodule

module cand(output bit_t [(SIZE -1) :0] out,
            input bit_t [(SIZE -1 ) :0] in1, in2);

    generate
        genvar i;
        for(i = 0; i < SIZE; i = i +1)
        begin : b
            assign out[i] = in1[i] & in2[i];
        end
    endgenerate

endmodule
 
