
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): A variable can be declared with an initializer, which must be
// constant. As a const value is not set during elaboration, it is
// set after elaboration, so this design initializes a variable with a previously declared
// variable.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1,
             output reg [w-1:0] out1);

    logic [w-1:0] temp;
    logic c;

    always@(posedge clk)
    begin
        for (int i=0; i<w; i++)
            temp[i] = in1[i];

        for (int i=w-1; i>=0; i--)
            for (int j=i; j<w-1; j++)    // j is initialized with a variable i
            begin
                if (temp[j] > temp[j+1])
                begin
                    c = temp[j];
                    temp[j] = temp[j+1];
                    temp[j+1] = c;
                end
            end

        for (int i=0; i<w; i++)
            out1[i] = temp[i];
    end

endmodule

