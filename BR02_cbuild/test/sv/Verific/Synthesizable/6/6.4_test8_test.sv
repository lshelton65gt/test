
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of variable initialization using genvar variable.

module test #(parameter p = 8)
                      (input int in,
                       output int out_arr [ (p-1):0]);


    generate
        genvar i;
        for(i = 0; i < p; i = i +1)
        begin : b
            int offset = i + 1;
            assign out_arr[i] = in + offset;
        end
    endgenerate

endmodule
