
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses '$dimensions' system function on variable declared
// of type typeparameter with unpacked dimension. The default type is
// logic but it is overridden with a type that is typedefined in the
// other module and has one packed and another unpacked dimensions. So
// the declared data element has three dimensions. This testcase checks if
// the tool can detect this correctly.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output [w-1:0] out1, out2);

    /* new type having both packed and unpacked dimensions */
    typedef bit [1:0] pu_arr [0:1];

    module bottom #(parameter p = 8, parameter type t = logic)
                   (input clk,
                    input [p-1:0] in1, in2,
                    output reg [p-1:0] out1, out2);

        /* declare data element having unpacked dimension */
        t r1 [1:0];

        always@(posedge clk)
        begin
            r1 = '{2{1'b0}};
            out1 = $dimensions(r1);
            if ($dimensions(r1) != 3)    // $dimensions should return 3
                out2 = '0;
            else
                out2 = '1;
        end

    endmodule

    bottom #(.t(pu_arr), .p(w)) bot2(clk, in1, in2, out1, out2);

endmodule

