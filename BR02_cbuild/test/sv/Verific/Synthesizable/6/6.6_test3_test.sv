
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that automatic variables cannot trigger an event.

module test(output int out,
                input int in);

    task auto_var_usage ;
        automatic int tmp;
        input in ;
        output out ;
        tmp = in ;
        @tmp out = tmp ;
    endtask

    always @ (in)
        auto_var_usage(in, out);

endmodule
