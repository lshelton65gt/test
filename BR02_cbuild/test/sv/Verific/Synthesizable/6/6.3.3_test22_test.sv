
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing of parameters to specify data types.

`include "type.v"

module test #(parameter type p = myStr)
            (input [$bits(p)-1:0] x,  y, output p out);

p i1, i2;

function p func (input p x, y);
    p str;
    str.a = x.a - y.a;
    str.b = y.b + x.b;
    return str;
endfunction

always@(x or y)
begin
    i1 = p'(x);
    i2 = p'(y);

    out = func(i1, i2);
end

endmodule

