
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. This type is used as
// the return type of a function. The function is called from an always block to 
// set the output port value. It sets two of its output port in such way and the
// other two are set by instantiating a module. The module uses a type parameter. 
// Instantiation overrides the type parameter.

typedef struct packed {
    int a;
    int b;
    int c;
    int d;
} spdt;

module test(clk, i1, i2, i3, i4, o1, o2, o3, o4);

parameter WIDTH = 32;

input clk;
input signed [WIDTH -1:0] i1, i2, i3, i4;
output reg signed [WIDTH -1:0] o1;
output reg signed [WIDTH -1:0] o2;
output signed [31:0] o3;
output signed [127:0] o4;

reg signed [WIDTH -1:0] r1, r2;
reg [31:0] r3, r4;
reg [WIDTH -1:0] t1, t2, t3;
spdt d1, d2;

function spdt fn(int x, int y);
spdt ret;

    ret.a= x + y;
    ret.b= x ^ y;
    ret.c= x | y;
    ret.d= x & y;

    return ret;
endfunction
always @(posedge clk)
begin
    t1 = (i1[0]?(~(i1-1)):i1);
    t2 = (i2[0]?(~(i2-1)):i2);
    r1 = i2 * i1;
    t3 = (t1 * t2);
    r2 = ((i1[0]^i2[0])?(~t3+1):t3);

    r3 = t1 < t2;
    r4 = i1 < i2;
    d1 = fn(r1, r2);
    d2 = fn(r3, r4);
    if (r1 == r2) o1 = (r1 | r2); else o1 = ((r1>r2)?(r1-r2):(r2-r1));
    if (r3 == r4) o2 = (r3 | r4); else o2 = (2'b10 - 2'b10);
end
    
    bottom #(.W(32), .T(int)) b1(clk, r3, r4, o3);
    bottom #(.W(128), .T(spdt)) b2(clk, d1, d2, o4);

endmodule


module bottom(clk, i1, i2, o1);
parameter W=32;
parameter type T=int;

input clk;
input T i1, i2;
output T o1;

T r1,r2;
int i=0;
    
always @(posedge clk)
begin
    if(i>(W-1))
        i = '0;

    r1= (i1 + i2) ^ '1;

    if (r1[i++]) r2=(r1++) & i; else r2=(r1--) & i;
    
end

assign o1= r1 ^ r2;

endmodule

