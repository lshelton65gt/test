
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows specific data to be declared as static even
// inside an automatic task. These are static in nature and behave
// as the regular static variables. This design checks that.

module test (input clk,
             input [3:0] in1,
             output reg [15:0] out1);

    always@(posedge clk)
    begin
        fibonacci(in1, out1);
    end

    task automatic fibonacci(input int n, output int o);
        static int out = 1, in = 0;
        int t1, t2;

        if (in == n)
        begin
            o = out;
            return;
        end

        if (n < 0)
        begin
            o = -1;
            return;
        end

        if (0 == n || 1 == n)
        begin
            o = 1;
            return;
        end

        fibonacci(n-1, t1);
        fibonacci(n-2, t2);
        out = t1 + t2;
        in = n;

        o = out;
    endtask

endmodule

