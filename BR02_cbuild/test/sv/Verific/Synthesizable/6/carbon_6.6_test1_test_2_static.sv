
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Carbon modifed version: Test that data declared as static (by default) inside a static
// function is initialized only in first call of function.

module test(input int in1, in2,
                output int static_out_1, static_out_2,
                output int sum_out);
                

    function my_function;
        output int static_out_1, static_out_2;
        output int sum_out;
        input int in1, in2;

        int static_var_1;
        int static_var_2;

        begin
            static_out_1 = static_var_1;
            static_out_2 = static_var_2;
           
            static_var_1 = 20;
            static_var_2 = 30;
            sum_out = in1 + in2 + static_var_2 - static_var_1;
        end
    endfunction
   
    always @ (*)
    begin
        my_function(static_out_1, static_out_2, sum_out, in1, in2);
    end

endmodule

        
