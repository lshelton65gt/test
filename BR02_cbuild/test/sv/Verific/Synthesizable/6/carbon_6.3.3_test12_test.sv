
// This is somewhat similar to 6.3.3_test12_test.sv, however in this example
// there is no function and the testing of the ability to get the type
// information from a parameter is done within a module.

module test(clock, in1, in2, out1);
   parameter type p = int;
   input clock;
   input int in1, in2;
   output int out1;


   p temp2;			// type information from parameter p
   
   always @(posedge clock)
     begin: main
	temp2 = in1 & in2;
	out1 = temp2 + 1;
     end
endmodule

