
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing of parameters to specify data types.

module test #(parameter type p = int) (input [$bits(p)-1:0] a, output p out);

always@(a)
    out = a;

endmodule

