
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses static integer variable inside a task to
// remember the number of times the task has been called. It also uses various
// increment and decrement operators.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter IN_WIDTH=32, OUT_WIDTH=32;

input clk;
input signed [0:IN_WIDTH-1] i1, i2;
input [0:IN_WIDTH-1] i3, i4;
output reg [OUT_WIDTH-1:0] o1;
output reg signed [0:OUT_WIDTH-1] o2;

reg signed [IN_WIDTH/2-1:-IN_WIDTH/2] r1;
reg [(OUT_WIDTH+31):32] r2;
reg signed [30:0] r3;
reg signed [31:0] r4;
reg signed [32:0] r5;

task task1(input signed [0:IN_WIDTH-1] i1, i2,input [0:IN_WIDTH-1] i3, i4);
static int i = 0;

    if(i>4)
        i-=5;

    case (i)
    0:
    begin
        i++;
        r1 = ((i1>i2)?(~i1):(i2));
    end
    1:
    begin
        i++;
        r2 = (({ ~i2[0], i2[1:IN_WIDTH-1] } <= i3)?(~(i2-1)):(i3));
    end
    2:
    begin
        i++;
        r3 = { { IN_WIDTH/2 { (i3 >= i4) } }, { IN_WIDTH/2 { |i4[0:IN_WIDTH/2-1] } } };
    end
    3:
    begin
        i++;
        r4 = ((r1==r2)*r3)|{i2};
    end
    4:
    begin
        i++;
        r5 = ((r3<r4)*r2)^i3;
    end
    endcase
endtask

always @ (posedge clk)
begin

        task1(i1,i2,i3,i4);
        task1(i1,i2,i3,i4);
        task1(i1,i2,i3,i4);
        task1(i1,i2,i3,i4);

        o1 = r5-(r3+r4);
        o2 = r3^r5/r1;
end

endmodule

