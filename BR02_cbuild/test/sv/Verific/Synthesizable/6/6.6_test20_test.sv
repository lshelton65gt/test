
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function with a static variable in it.
// Depending on the value of that static variable it returns different values from
// the function and increments the value of the static variable. It models behavior
// of something like a state machine. The function is called from an always block
// four times. The values of some local variables are set from the return values
// of the functions. These values are used to set the values of the output ports.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter WIDTH=8;

input clk;
input signed [WIDTH-1:0] i1, i2, i3, i4;
output reg signed [0:-WIDTH+1] o1, o2;

reg t1, t2, t3, t4, t5;
reg [WIDTH-2:0] r1, r2;
reg [WIDTH:0] r3, r4;

function int fn(int a, b, c, d);
static int i = 0 ;
    if(i>3)
        i -=4;   
    case (i)
    0:
    begin
        i++;
        return (~i1 & i2);
    end
    1:
    begin
        i++;
        return ~(i2 | i3);
    end
    2:
    begin
        i++;
        return (i4 ^ i1);
    end
    3:
    begin
        i++;
        return ((r1 ~^ r2)?(r3-r1+r2):((r1 ^~ r3)?(r1+r3-r2):(r2+r1-r3)));
    end
    endcase

endfunction
always @ (posedge clk)
begin
        r1 = fn(i1,i2,i3,i4);
        r2 = fn(i1,i2,i3,i4);
        r3 = fn(i1,i2,i3,i4);
        r4 = fn(i1,i2,i3,i4);

        t1 = &r1 | |r2;
        t2 = ^r4 ^ ^ r3;
        t3 = (((~|r1) ~^ ~(|r1))?|r2:~|r2);
        t4 = (((~(&r2)) ^~ (~&r2))?~&r3:&r3);
        t5 = (((~^r3) ~^ ~(^r3))?^r4:^~r4);

        o1 = { 2'sb10, t1, t2, t3, t4, t5, 1'b1 };
        o2 = { |{ 2'sb10, t1, t2, t3, t4, t5, 1'b1 }, 2'b01, t3, t5 };
end

endmodule

