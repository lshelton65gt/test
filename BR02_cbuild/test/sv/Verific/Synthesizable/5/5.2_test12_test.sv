
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Packed array when declared as signed and when used as a single
// unit, behaves like a signed entity, but when part-select is applied on it,
// it becomes unsigned. This design checks the signedness and the
// unsignedness of the packed array both as a whole and when part select is applied on it
// by using sign aware operators like < and > on them.

module test (input clk,
             input [7:0] in1,
             output reg [7:0] out1);

    bit signed [31:0] s_arr;

    always@(posedge clk)
    begin
        s_arr = { 32 { 1'b1 } };

        if (s_arr > s_arr[31:0])
            out1 = ~in1;
        else
            if (s_arr < s_arr[31:0])
                out1 = in1;
            else
                out1 = 1;
    end

endmodule
