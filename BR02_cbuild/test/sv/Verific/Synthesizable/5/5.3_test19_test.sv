
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Typedef can be used to define a new type having both packed and unpacked
// dimensions. This design defines such a type.

module test#(parameter w = 2)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    typedef bit [1:0] double_dim [0:1][0:3];

    double_dim dd1, dd2;

    always@(posedge clk)
    begin
        dd1 = '{ '{4{in1}}, '{4{in2}} };
        dd2 = '{ '{4{in2}}, '{4{in1}} };

        out1 = {dd1[0][0][0], dd2[1][1][1]} + {dd1[1][0][0], dd2[0][1][1]};
        out2 = {dd1[1][3][1], dd2[0][1][1]} | {dd1[0][3][0], dd2[1][2][1]};
    end

endmodule

