
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Typedef can be used to define a new type on multidimensional arrays
// both packed and unpacked. This design uses typedef on packed arrays
// and uses the new type to define unpacked array in the design.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    typedef bit [1:0][0:1] double_dim;    // double_dim is double dimensional packed array of bits

    double_dim dd1[0:1], dd2[1:0];

    always@(posedge clk)
    begin
        for (int i=0; i<2; i++)
            for (int j=0; j<2; j++)
                for (int k=0; k<2; k++)
                    dd1[i][j][k] = in1[4*i+2*j+k] + in2[4*i+2*j+k] ;

        dd2 = dd1;    // possible to assign on unpacked array SV 3.1 LRM, section 4.2

        out1 = { dd1[0], dd2[1] } ^ { dd1[1], dd2[0] };
        out2 = { dd2[1], dd1[0] } & { dd2[0], dd1[1] };
    end

endmodule

