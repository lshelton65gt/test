
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Typedef can be used to define a new type having 
// both packed and unpacked dimensions. This design defines such type
// and uses the new type to define object in the design.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [1:0] out1[1:0][w/2 -1:0], out2[1:0][w/2 -1:0]) ;

    typedef reg [1:0] double_dim [0:7] ;    

    double_dim dd1[0:1] ;

    always@(posedge clk)
    begin
        dd1[0][0] = in1[7:6] ^ in2[7:6] ;   
        dd1[0][1] = in1[5:4] ^ in2[5:4] ;   
        dd1[0][2] = in1[3:2] ^ in2[3:2] ;   
        dd1[0][3] = in1[1:0] ^ in2[1:0] ;   
        dd1[1][0] = in1[7:6] | in2[7:6] ;  
        dd1[1][1] = in1[5:4] | in2[5:4] ;  
        dd1[1][3] = in1[3:2] | in2[3:2] ;  
        dd1[1][2] = in1[1:0] | in2[1:0] ;  

        out1 = '{ dd1[0][0+:4], dd1[0][2+:4] } ;
        out2 = '{ dd1[1][0+:4], dd1[1][2+:4] } ;
    end

endmodule

