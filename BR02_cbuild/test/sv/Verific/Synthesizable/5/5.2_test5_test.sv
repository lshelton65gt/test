
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing the maximum size of a packed array using $bits.

module test(output bit compare,
             output int len1, len2,
             input  bit [(2**16) -1 : 0] parr1,
             input bit [(2**17) -1 : 0] parr2);


    always @(parr1, parr2)
    begin
        len1 = $bits(parr1);
        len2 = $bits(parr2);
        if(len1 == len2)
            compare = 1;
        else
            compare = 0;        
    end

endmodule
