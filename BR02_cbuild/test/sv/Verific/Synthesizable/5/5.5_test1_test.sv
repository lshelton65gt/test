
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): $left array query function returns the left bound(msb) of the dimension.
// This design uses that on array declared with a type parameter.
// The parameter type is overridden from the bench.

module test#(parameter w = 8,
             parameter type t = logic signed [w-1:0])
            (input clk,
             output reg [w-1:0] out1, out2);

    t ar1;
    t ar2 [w/2-1:0];

    always@(posedge clk)
    begin
        out1 = $left(ar1, 1);
        out2 = $left(ar2, 1);
    end

endmodule
