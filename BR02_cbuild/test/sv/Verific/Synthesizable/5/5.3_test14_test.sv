
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design declares arrays inside case-generate, width of which
// depends on the path of the case-generate. These arrays are accessed from
// within the same scope.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    generate
        case (w)
            1,2,3,4:
                     begin
                         logic [3:0] arr;
                         always@(posedge clk)
                         begin
                             arr = in1 * in2;
                      
                             out1 = (arr[0] ? (in1 + in2)|arr : (in2 - in1));
                             out2 = (arr[0] ? (in1 ^ in2) : (in2 & in1)^arr);
                         end
                     end
            5,6,7,8:
                     begin
                         logic [7:0] arr;
                         always@(posedge clk)
                         begin
                             arr = in1 * in2;
                      
                             out1 = (arr[0] ? (in1 + in2)|arr : (in2 - in1));
                             out2 = (arr[0] ? (in1 ^ in2) : (in2 & in1)^arr);
                         end
                     end
            default:
                     begin
                         logic [-w:0] arr;
                         always@(posedge clk)
                         begin
                             arr = in1 * in2;
                      
                             out1 = (arr[0] ? (in1 + in2)|arr : (in2 - in1));
                             out2 = (arr[0] ? (in1 ^ in2) : (in2 & in1)^arr);
                         end
                     end
        endcase
    endgenerate

endmodule

