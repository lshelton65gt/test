
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing arbitrary length integer arithmetic 
// by using packed array.

module test #(parameter SIZE = 48) 
              (input bit signed [SIZE -1:0] s_in1, s_in2, 
               input bit [SIZE -1:0] u_in1, u_in2,
               output bit signed [SIZE -1:0] s_mult_out,
               output bit [SIZE -1:0] u_mult_out,
               output bit s_greater_out, u_greater_out) ;

    always @(s_in1, s_in2, u_in1, u_in2)
    begin

        s_mult_out = s_in1 * s_in2;
        u_mult_out = u_in1 * u_in2;

        if((~s_in1 +1) == u_in1)
        begin
            s_greater_out = s_in1 > 0;
            u_greater_out = u_in1 > 0 ;
        end

    end

endmodule
