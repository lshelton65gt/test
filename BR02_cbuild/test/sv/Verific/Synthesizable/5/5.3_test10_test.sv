
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If the index expression is out of bound for a 2-state array when
// reading or writing, a run-time warning shall be generated.
// This design checks that when reading from an array with out of
// bound index.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    bit [0:7] arr;    // 2 state array

    always@(posedge clk)
    begin
        arr = in1 * in2;

        out1 = in1 + in2 | arr[-1];    // warning: -1 is out of bound index for arr (arr[-1])
        out2 = in1 ^ in2 | arr[8];     // warning: 8 is out of bound index for arr (arr[8])
    end

endmodule

