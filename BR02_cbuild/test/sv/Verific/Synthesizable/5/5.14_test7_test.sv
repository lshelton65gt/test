
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design tests the rules of queue. 
// Q[ n : n ] yields a queue with one item, the one at position n.
// Thus, Q[ n : n ] === { Q[n] }. 

`define a 2
`define b 2

module test (input init1, init2, output bit out);
    int Q[$] ;
    int Q1[$] ;

    always @ (init1)
    begin
        for (int i = 0; i < 10; i++)
            Q = {Q, i*10} ;
    end

    always @ (init2)
    begin
        Q1 = Q[`a:`b] ;

        if (Q1[0] == Q[2])
            out = 1'b1 ;
        else
            out = 1'b0 ;
    end
endmodule

