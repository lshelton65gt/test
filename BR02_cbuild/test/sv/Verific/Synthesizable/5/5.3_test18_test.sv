
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Typedef can be used to define a new type on multidimensional arrays
// both packed and unpacked. This design uses typedef on unpacked arrays
// and uses the new type to define data in the design.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    typedef bit single_dim [w-1:0];    // single_dim is single dimensional unpacked array of bits

    single_dim dd1, dd2; // dd1 and dd2 are single_dim type array

    always@(posedge clk)
    begin
        for (int i=0; i<w; i++)
            dd1[i] = in1[i] ^ in2[i] ;

        dd2 = dd1 ;    // possible to assign on unpacked array SV 3.1 LRM, section 4.2

        for (int i=0; i<w; i++)
        begin
            out1[i] = |{ dd1[i], dd2[i] } ;
            out2[i] = &{ dd1[i], dd2[i] } ;
        end
    end

endmodule

