
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of assignment to packed and unpacked arrays.

module test(output bit p_assign, u_assign,
             input logic [7:0] p_arr,
             input int u_arr [-2:0]);

    logic [9:2] p_arr_copy;
    int u_arr_copy [0:2] ;

    always @(*)
    begin
        p_arr_copy = p_arr;
        u_arr_copy = u_arr;

        if(p_arr_copy[9] === p_arr[7])
            p_assign = 1;
        else
            p_assign = 0;

        if(u_arr_copy[1] == u_arr[-1])
            u_assign = 1;
        else
            u_assign = 0;
    end

endmodule
