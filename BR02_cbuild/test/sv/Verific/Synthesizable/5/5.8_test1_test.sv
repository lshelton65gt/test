
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test a function with formal dynamic array and return type
// is also dynamic array.

module test(input int p[], output int a[]) ;

    always_comb
        a = func(p) ;

    typedef int my_type[] ;

    function my_type func(int p[]) ;

        func = new [10] (p) ;
        for(int i = 5; i < 10; i++)
            func[i] = i + 2 ;
        return func ;

    endfunction

endmodule

