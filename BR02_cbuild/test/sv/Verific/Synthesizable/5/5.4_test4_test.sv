
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses variable slicing, for which starting position
// may be variable but the size of the slice must be constant.
// The constant used is the return value of a constant function call.

module test (input clk,
             input [7:0] in1, in2,
             output reg [7:0] out1, out2);

    logic [31:0][0:7] m;
    int i;

    always@(posedge clk)
    begin
        i = |{in1, in2};
        m = {8{in1, in2, in1, in2}};

        i = |m[i+:by2(4)];
        out1 = i;
        i = |m[i+:by2(6)];
        out2 = i;
    end

    function int by2(int n);
        if (n<0)
            return (-n)<<1;

        if (n==0 || n==1)
            return 0;

        return n<<1;
    endfunction

endmodule

