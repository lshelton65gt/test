
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): $dimesions returns the number of dimensions of the array or 0 for scalar. 
// This design uses that on array declared with a type parameter.
// The parameter type is overridden from the bench.

module test#(parameter w = 8,
             parameter type t = logic signed [w-1:0])
            (input clk,
             output reg [w-1:0] out1, out2);

    t ar1;
    t ar2 [w-1:w/2];

    always@(posedge clk)
    begin
        out1 = $dimensions(ar1);
        out2 = $dimensions(ar2);
    end

endmodule

