
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Multidimensional arrays can be used as arguments in function
// or task. Here, same type, dimension, & size (different ranges) are used in formal
// and actual.

module test (input clk, output int sum, input int b[1:3][0:2]);

    always @(posedge clk)
    begin
        sum <= func(b) ;
    end

    function int func(input int a[3:1][3:1]) ;

       func = 0 ;
       for (int i=1; i<4; i++)
          for (int j=1; j<4; j++)
              func += a[i][j] ;
    endfunction

endmodule

