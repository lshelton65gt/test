
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of indexing on single dimensional unpacked array.

module test(input int int_arr [1:0],
                  input logic logic_arr [-1:0],
                  input byte byte_arr [1:0],
                  output int width1, width2, 
                  width3, width4, width5);

    always_comb
    begin
        width1 = $bits(int_arr[0]);
        width2 = $bits(int_arr[0][31]);
       
        width3 = $bits(logic_arr[-1]);

        width4 = $bits(byte_arr[1]);
        width5 = $bits(byte_arr[1][7]);
    end

endmodule
