
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design declares arrays inside if-generate block and their widths
// depend on the paths of the if-generate. They are accessed from within the same scope.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    generate
        if (w < 0)
        begin
            bit [-w:0] arr;
            always@(posedge clk)
            begin
                arr = in1 * in2;
         
                out1 = (arr[0] ? (in1 * in2)|arr : (in2 | in1));
                out2 = (arr[0] ? (in1 - in2) : (in2 ^ in1)^arr);
            end
        end
        else
        begin
            if (w <= 4)
            begin
                bit [3:0] arr;
                always@(posedge clk)
                begin
                    arr = in1 * in2;
         
                    out1 = (arr[0] ? (in1 | in2)|arr : (in2 & in1));
                    out2 = (arr[0] ? (in1 + in2) : (in2 * in1)-arr);
                end
            end
            else
            begin
                if (w <= 8)
                begin
                    bit [7:0] arr;
                    always@(posedge clk)
                    begin
                        arr = in1 * in2;
         
                        out1 = (arr[0] ? (in1 + in2)|arr : (in2 - in1));
                        out2 = (arr[0] ? (in1 ^ in2) : (in2 & in1)^arr);
                    end
                end
                else
                begin
                    bit [15:0] arr;
                    always@(posedge clk)
                    begin
                        arr = in1 * in2;
         
                        out1 = (arr[0] ? (in1 ^ in2)|arr : (in2 & in1));
                        out2 = (arr[0] ? (in1 - in2) : (in2 | in1)*arr);
                    end
                end
            end
        end
    endgenerate

endmodule

