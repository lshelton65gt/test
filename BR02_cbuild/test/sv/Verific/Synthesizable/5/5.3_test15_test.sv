
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design declares arrays inside for-generate loop. The
// width of the arrays depend on the value of the genvar variable. They are accessed in
// a common way without reference to its width.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    genvar i;

    generate
    for(i=0; i<w; i=i+1)
    begin: for_gen_i
        bit [i:0] arr;

        always@(posedge clk)
        begin
            arr = in1 * in2;
     
            out1[i] = (arr[i] ? (in1 + in2)|arr : (in2 - in1));
            out2[i] = (arr[i] ? (in1 ^ in2) : (in2 & in1)^arr);
        end
    end
    endgenerate

endmodule

