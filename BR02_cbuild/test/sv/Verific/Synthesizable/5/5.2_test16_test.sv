
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Packed arrays can only be made of the single bit types, but
// unpacked arrays are allowed of any type. This design defines a
// type parameter. The default value is of single bit type, but
// when it is instantiated the parameter is overridden with
// multi-bit type like byte.  

module test#(parameter type pt = int, parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    pt [w-1:0] arr;    // illegal if pt is not of single bit type or if overridden with a multi-bit type.

    always@(posedge clk)
    begin
        arr = in1 | in2;

        out1 = arr * in2;
        out2 = arr & in1;
    end

endmodule

