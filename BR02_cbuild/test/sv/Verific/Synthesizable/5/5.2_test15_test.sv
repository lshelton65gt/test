
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design typedefines an unpacked array and then uses that
// new type to define another unpacked array. This unpacked array is used in the design.

module test (input clk,
             input in1[3:0], in2[3:0] ,
             output reg [7:0] out1[1:0], out2[1:0]);

    typedef bit unpacked_bits [3:0];
    unpacked_bits pub[3:0] ;

    always@(posedge clk)
    begin
        pub[0][2] = in1[2] + in2[3];
        pub[0][1] = in1[3] + in2[2];
        pub[0][0] = in1[2] + in2[1];
        pub[0][3] = in1[1] + in2[0];

        pub[1][0] = in1[0] & in2[2];
        pub[1][1] = in1[1] & in2[2];
        pub[1][2] = in1[2] & in2[2];
        pub[1][3] = in1[3] & in2[2];

        pub[2][1] = in2[1] | in1[1];
        pub[2][2] = in2[1] | in1[0];
        pub[2][3] = in2[2] | in1[1];
        pub[2][0] = in2[1] | in1[3];

        pub[3][0] = in2[1] ^ in1[0];
        pub[3][1] = in2[2] ^ in1[0];
        pub[3][2] = in2[3] ^ in1[1];
        pub[3][3] = in2[3] ^ in1[0];

        out1[0] = pub[3][0] * pub[0][2];
        out1[1] = pub[2][1] * pub[1][1];
        out2[0] = pub[1][2] * pub[2][0];
        out2[1] = pub[0][3] * pub[3][3];

    end

endmodule

