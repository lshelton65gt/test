
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): $length array query function returns the length of the dimension of the array.
// This design uses that on array declared with a type parameter.
// The parameter type is overridden from the bench.

module test#(parameter w = 8,
             parameter type t = logic signed [w-1:0])
            (input clk,
             output reg [w-1:0] out1, out2);

    t ar1;
    t ar2 [w-1:w/2];

    always@(posedge clk)
    begin
        out1 = $length(ar1, 1);
        out2 = $length(ar2, 1);
    end

endmodule

