
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If the index expression is out of bounds for a 2-state array when
// reading or writing, a warning shall be generated.
// This design checks that when writing to an array with out of
// bound index.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    bit [0:7] arr;    // 2 state array

    always@(posedge clk)
    begin
        for(int i=4; i<12; i++) 
            arr[i] = |in1 ^ &in2;    // warning: 8 to 11 is out of bound index for arr (arr[i], i=8, 9, 10, 11)

        out1 = in1 + in2 | arr;
        out2 = in1 ^ in2 | arr;
    end

endmodule
