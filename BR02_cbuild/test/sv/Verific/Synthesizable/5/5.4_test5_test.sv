
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses variable slicing, for which starting position
// may be a variable but the size of the slice must be constant.
// The constant used is the parameter of the module.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    logic [0:w-1][31:0] m ;
    int i;

    always@(posedge clk)
    begin
        i = |{in1, in2};
        m = {w{{16/w{in1, in2}}}} ;

        i = |m[i+:w/2];
        out1 = i;
        i = |m[i+:w/4];
        out2 = i;
    end

endmodule

