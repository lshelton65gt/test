
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of signed packed array

module test(output int s_mult_out, u_mult_out,
                    output reg s_less_out1, s_less_out2,
                               u_less_out1, u_less_out2,
                    input bit signed [7:0] in1, in2) ;

    always @(in1, in2)
    begin
        s_mult_out = in1 * in2 ;
        u_mult_out = in1[7:0] * in2[7:0] ;
        s_less_out1 = in1 < 0 ;
        s_less_out2 = in2 < 0 ;
        u_less_out1 = in1[7:0] < 0 ;
        u_less_out2 = in2[7:0] < 0 ;
    end

endmodule
