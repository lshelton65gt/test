
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of indexing on single dimensional packed array.

module test(input logic [0:7] in1, in2,
                  output logic [0:7] and_out, or_out);


    always @ *
    begin
        and_out[0] = in1[0] & in2[0];
        and_out[1] = in1[1] & in2[1];
        and_out[2] = in1[2] & in2[2];
        and_out[3] = in1[3] & in2[3];
        and_out[4] = in1[4] & in2[4];
        and_out[5] = in1[5] & in2[5];
        and_out[6] = in1[6] & in2[6];
        and_out[7] = in1[7] & in2[7];

        or_out[0] = in1[0] | in2[0];
        or_out[1] = in1[1] | in2[1];
        or_out[2] = in1[2] | in2[2];
        or_out[3] = in1[3] | in2[3];
        or_out[4] = in1[4] | in2[4];
        or_out[5] = in1[5] | in2[5];
        or_out[6] = in1[6] | in2[6];
        or_out[7] = in1[7] | in2[7];        
    end

endmodule
