
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of unpacked array of integers.

module test(input integer int_arr [1:0][-2:0][2:0], output integer intgr_arr2 [0:2][1:3][3:5]) ;

    integer integer_arr [-3:-1][2:0] ;

    always_comb
    begin
        integer_arr = int_arr[0] ;
        for (int i=0 ; i<2 ; i++)
            intgr_arr2[i]= integer_arr ;
    end
endmodule

