
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of slicing of an uni-dimensional packed array.

module test(input logic [7:0] bit_vec,
               output logic red_and, red_or);


    bit and_res [0:5], or_res [0:5];
    bit [1:0] part_sel[3:0] ;

    always @ (*)
    begin
        part_sel[3] = bit_vec[7:6];
        part_sel[2] = bit_vec[5:4];
        part_sel[1] = bit_vec[3:2];
        part_sel[0] = bit_vec[1:0]; 

        and_res[0] = & part_sel[3];
        and_res[1] = & part_sel[2];
        and_res[2] = & part_sel[1];
        and_res[3] = & part_sel[0];

        and_res[4] = and_res[0] & and_res[1];
        and_res[5] = and_res[2] & and_res[3];
        red_and = and_res[4] & and_res[5];

        or_res[0] = | part_sel[3];
        or_res[1] = | part_sel[2];
        or_res[2] = | part_sel[1];
        or_res[3] = | part_sel[0];

        or_res[4] = or_res[0] | or_res[1];
        or_res[5] = or_res[2] | or_res[3];
        red_or = or_res[4] | or_res[5];

    end

endmodule
