
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The maximum size of the packed array might be limited, but it
// should be at least 65536 (2**16) bit wide. This design defines
// a packed array of size expressed in terms of its parameter value. The parameter is 
// overridden with this value.

module test#(parameter w = 8, t = 2**16)
            (input clk,
             input [w-1:0] in1,
             output reg [w-1:0] out1);

    bit signed [t-1:0] sp_arr;
    bit unsigned [t-1:0] up_arr;

    always@(posedge clk)
    begin
        up_arr = in1;
        up_arr++;
        sp_arr = --up_arr;
        out1 = sp_arr | 8'b11111111;
    end

endmodule

