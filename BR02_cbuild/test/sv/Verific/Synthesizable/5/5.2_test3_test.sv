
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Tests if an element of an unpacked array can be
// a packed array.

module test(input logic [7:0] vec1, vec2, 
             output bit result);


    bit [1:0][3:0] arr [5:4] ;

    bit [7:0] add_res1, add_res2, mult_res1, mult_res2;

    always @(vec1, vec2)
    begin
        arr[5] = vec1;
        arr[4] = vec2;

        add_res1 = arr[5] + arr[4];
        add_res2 = vec1 + vec2;

        mult_res1 = arr[5] * arr[4];
        mult_res2 = vec1 * vec2;

        if((add_res1 == add_res2) &&
           (mult_res1 == mult_res2))
            result = 1;
        else
            result = 0;                
    end

endmodule
