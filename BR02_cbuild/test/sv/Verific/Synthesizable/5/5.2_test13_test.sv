
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Packed arrays when declared as signed and when used as a single
// unit, behaves like a signed entity. However when part-select is applied on it,
// it becomes unsigned. This design checks the signedness and the
// unsignedness of a packed array with and without part select by using sign
// aware operator * (multiplication) on them.

module test (input clk,
             input [7:0] in1,
             output reg [7:0] out1);

    bit signed [7:0] s_arr;
    bit signed [31:0] s_arr_mul;

    always@(posedge clk)
    begin
        s_arr = { 8 { 1'b1 } };
        s_arr_mul = s_arr * s_arr[7:0];

        if (s_arr_mul < 0 && s_arr_mul[31:0] > 0)
            out1 = in1;
        else
            out1 = ~in1;
    end

endmodule

