
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Use typedef to define multiple packed dimensions in stages.

module test(input byte byte_arr [3:0],
                output byte byte_or, byte_and,
                output logic reduce_and, reduce_or);


    typedef bit [7:0] my_byte;

    my_byte [5:2] my_byte_arr;

    always @ *
    begin
        my_byte_arr[5] = byte_arr[3];
        my_byte_arr[4] = byte_arr[2];
        my_byte_arr[3] = byte_arr[1];
        my_byte_arr[2] = byte_arr[0];

        byte_or = my_byte_arr[5] | my_byte_arr[4] | 
                  my_byte_arr[3] | my_byte_arr[2] ;
        byte_and = my_byte_arr[5] & my_byte_arr[4] & 
                  my_byte_arr[3] & my_byte_arr[2] ;

        reduce_and = & my_byte_arr;
        reduce_or = | my_byte_arr;
    end

endmodule
 
