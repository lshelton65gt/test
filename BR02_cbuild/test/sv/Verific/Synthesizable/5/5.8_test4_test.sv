
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Dynamic arrays can be used as arguments in function/task.
// These can take arrays of any size for the dynamic dimension of compatible type.

module test #(parameter p = 10)(input clk,
              output string arr[]);

    always @(posedge clk)
        T(arr) ;

    task T(output string a[]);
        a = new[p] ;
        for(int i = 0; i < p; i++)
          a[i] = "this is a check" + i;
    endtask

endmodule

