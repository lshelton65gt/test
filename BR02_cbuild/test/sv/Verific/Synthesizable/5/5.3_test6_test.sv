
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If an index expression is of 4-state type, like logic, but the array
// is of a 2-state type like bit then an x or z in the index expression
// will generate a run-time warning and the index will be treated as 0.
// This design checks that for array write when index is x.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    logic [0:2] i;    // 4 state type index
    bit [0:7] arr;    // 2 state type array

    always@(posedge clk)
    begin
        i = 3'bxxx;
        arr = in1 | in2;
        arr[i] = |in1 | |in2;    // should issue a runtime warning and arr[0] should be set to |in1 | |in2

        out1 = in1 + in2 | arr;
        out2 = in1 ^ in2 - arr;
    end

endmodule

