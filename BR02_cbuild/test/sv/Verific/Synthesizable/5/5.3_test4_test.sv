
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If an index expression is of 4-state type, like logic, and the
// array is of a 4-state type like reg then an x or z in the index
// expression will cause a read to return x. This design checks that.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    logic [0:2] i;
    reg [0:7] arr;

    always@(posedge clk)
    begin
        i = 'z;
        arr = in1 | in2;

        out1 = in1 + in2;
        out1[0] = arr[i];
        out2 = in1 ^ in2;
        out2[0] = arr[i];
    end

endmodule

