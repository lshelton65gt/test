
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that packed arrays can be treated like integers in an expression

module test(output bit [7:0] incr_out, sub_out, shift_out,
             output bit reduce_and_out1, reduce_and_out2,
             reduce_or_out1, reduce_or_out2,
             input bit [1:0][3:0] p_arr1, p_arr2);

    always @ *
    begin
    
        incr_out = p_arr1 + 1;
        sub_out = p_arr1 - p_arr2;
        shift_out = p_arr1 << 3;
        reduce_and_out1 = & p_arr1;
        reduce_and_out2 = & p_arr2;
        reduce_or_out1 = | p_arr1;
        reduce_or_out2 = | p_arr2;
    end

endmodule

             
