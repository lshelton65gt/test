
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): A queue is a variable size, ordered collection of 
// homogeneous elements. This design creates and tests 'queue'.  

module test (input bit change, string str, int number,  output bit out) ;
    int q[$] ;
    int e ;
    string names[$] = {"Bob"} ;
    
    always @ (*)
    begin
        names = {names, str} ;
        names = {names, "Rob"} ;

        out = 1'b1 ;
    end

    always @ (number)
    begin
        e = q[2] ; 
        q = { q, number } ;    // insert number at the end (append 6)
        q = { e, q } ;    // insert 'e' at the beginning (prepend e)

        out = 1'b1 ;
    end

    always @ (change)
    begin
        q = q[1:$] ;    // delete the first (leftmost) item
        q = q[0:$-1] ;    // delete the last (rightmost) item
        q = q[1:$-1] ;    // delete the first and last items
        
        q = {} ;    // clear the queue (delete all items)
        names = {} ;

        out = 1'b1 ;
    end
endmodule

