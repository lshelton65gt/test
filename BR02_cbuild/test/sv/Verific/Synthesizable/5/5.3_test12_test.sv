
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of array when index expression is of 4-state 
// type and contains x or z

module test(input logic [1:0] idx,
                input logic [3:0] in,
                output logic result);

  logic [3:0] tmp;
  logic my_logic_bit;

  always @ *
  begin
      my_logic_bit = in[idx];
      result = 0;
      if((idx[0] === 1'bx) || (idx[0] === 1'bz) ||
         (idx[1] === 1'bx) || (idx[1] === 1'bz))
          if(my_logic_bit === 1'bx)
              result = 1;
      tmp[idx] = '1;             
  end

endmodule
