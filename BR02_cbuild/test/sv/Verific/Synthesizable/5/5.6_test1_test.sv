
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a dynamic array. The actual size of the
// array is defined inside an initial block.

module test (in, out) ;

    parameter p = 4 ;
    localparam start = 30 ;

    input int in [2 * p-1:0] ;
    output int out [2 * p-1:0] ;

    int arr [] ; // Define a dynamic array, don't set the size

    initial
    begin
        arr = new [2*p-1] ;
        arr = in ; // assign the bit selected array here
    end
     
    assign out = arr ;  // assign the output port from the bit selected array

endmodule

