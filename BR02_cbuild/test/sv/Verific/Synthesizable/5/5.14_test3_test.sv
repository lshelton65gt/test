
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design creates an empty queue by providing downto
// range in queue elements selection.

`define a 10
`define b 5

module test (input clk, output bit out) ;
    int Q1[$] ;
    int Q2[$] ;

    always @ (posedge clk)
    begin
        for (int i = 0; i < 20; i++)
            Q1 = {Q1, i*10} ;

        Q2 = Q1[`a:`b] ;  // Its not an error but an empty queue. 
        if (Q2.size == 0) out = 1'b1 ;
        else out = 1'b0 ;
    end
endmodule

