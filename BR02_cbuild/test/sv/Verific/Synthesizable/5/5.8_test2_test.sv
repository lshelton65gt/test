
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Dynamic arrays can be used as arguments in function/task.
// These can take arrays of any size for the dynamic dimension of compatible type.
// Here, bit and logic is used as compatible type.

module test #(parameter width=10)
             (input clk,
              output logic b[]);

    logic b1[] ;
    always @(posedge clk)
    begin
        b1 = new[width] ;
        b = func(b1) ;
    end

    typedef logic my_type [] ;
    function my_type func(logic a[]) ;
       logic a1[] = new [10] (a) ;
       for (int i=0; i<10; i++)
           a1[i] = 1'b1 ;
       return a1 ;
    endfunction

endmodule

