
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design tests the rule of queue. (Q[n:n] = Q[n])

module test (input clk, output bit out) ;
    int Q[$] ; 
    
    always @ (posedge clk)
    begin
        for (int i = 0; i < 20; i++)
            Q = {Q, i*10} ;
        
        if (Q[10:10] == Q[10])  out = 1'b1 ;
        else out = 1'b0 ;
    end
endmodule

