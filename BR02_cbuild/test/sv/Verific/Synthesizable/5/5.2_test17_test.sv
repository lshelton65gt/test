
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If a packed array is declared as signed, then the array, viewed as a single vector,
// shall also be signed. However, a part-select of a packed array shall be unsigned.

module test(i, o) ;

    parameter out_width = 4 ;
    parameter in_width = 2*out_width ;

    input [in_width-1:0] i ;
    output reg [out_width-1:0] o ;

    // Declare two signed packed arrays
    logic signed [0:out_width-1] a ;
    logic signed [0:out_width-1] b ;

    always@(i)
    begin
        {a, b} = i ; // a and b is signed 
        if (a > 0)
            a = -a ; // if a is not negative, make it negative
        if (b > 0)
            b = -b ; // if b is not negative, make it negative

        if (a < 0 && b < 0)
            // We should always come here, a & b both are negative
            o = a[0:out_width-1] | b[0:out_width-1] ;
        else
            // Only here if a or b gets zero value from i
            o = 0 ;
    end

endmodule

