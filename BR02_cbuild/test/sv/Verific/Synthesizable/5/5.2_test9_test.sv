
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of bit select on packed and unpacked arrays.

module test(input bit [3:0][7:0] p_arr,
             input byte u_arr [4:2],
             output bit p_arr_sel, u_arr_sel) ;

    bit [4:1][7:0] p_arr_copy ;
    byte u_arr_copy [3:1] ;

    always @*
    begin
        p_arr_copy[4] = p_arr[3] ;
        p_arr_copy[3] = p_arr[2] ;
        p_arr_copy[2] = p_arr[1] ;
        p_arr_copy[1] = p_arr[0] ;

        u_arr_copy[3] = u_arr[4] ;
        u_arr_copy[2] = u_arr[3] ;
        u_arr_copy[1] = u_arr[2] ;

        if(p_arr_copy[3] == p_arr[2])
            p_arr_sel = 1 ;
        else
            p_arr_sel = 0 ;

        if(u_arr_copy[1] == u_arr[2])
            u_arr_sel = 1 ;
        else
            u_arr_sel = 0 ;
    end

endmodule
 
