
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Pass dynamic array or normal array to function/task having
// dynamic array argument. 

module test #(parameter width=8)
             (input clk,
              input string in1, in2,
              output string out1[width: 0], output string arr[]);
    
    always @(posedge clk)
    begin
         arr = new[width] ;
         T(arr) ;
         T(out1) ;
    end

    task T(inout string a[]) ;
        for (int i = 0; i < width; i++) begin
            a[i] = {"this is", i ? in1: in2} ;
        end
    endtask

endmodule

