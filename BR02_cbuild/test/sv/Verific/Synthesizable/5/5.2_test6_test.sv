
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of unpacked array of byte and shortint.

module test(input byte in1, in2, output byte out1, output shortint out2);

    byte byte_arr [2:0][-3:1];
    shortint short_int_arr [8:5][4:0][1:0] ;

    always @(*) 
    begin
        byte_arr[2][-3] = in1 ;
        byte_arr[2][-2] = in2 ;
        byte_arr[2][-1] = ~in1 ;
        byte_arr[2][0] = ~in2 ;
        byte_arr[2][1] = in1 | in2 ;
        short_int_arr[8][4][1] = byte_arr[2][1] << 10 ;
        short_int_arr[8][3][0] = byte_arr[2][0] + short_int_arr[8][4][1] << 12 ;
        out1 = byte_arr[2][-3] + byte_arr[2][-1] * byte_arr[2][-2] - byte_arr[2][1] ;
        out2 = short_int_arr[8][3][0] + short_int_arr[8][4][1] >> 1 ;
    end
endmodule

