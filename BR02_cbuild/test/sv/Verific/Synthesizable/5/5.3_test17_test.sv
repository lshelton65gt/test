
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Typedef can be used to define a new type on multidimensional arrays
// both packed and unpacked. This design uses typedef on packed arrays
// and uses the new type to define data type in the design.

module test#(parameter w = 8)
            (input clk,
             input [w-1:0] in1, in2,
             output reg [w-1:0] out1, out2);

    typedef bit [1:0][0:3] double_dim;    // double_dim is double dimensional packed array of bits

    double_dim dd1, dd2;

    always@(posedge clk)
    begin
        dd1 = in1 ^ in2;
        dd2 = in2 - in1;

        out1 = { dd1[0][2+:1], dd1[1][0:1], dd2[1][0:3] };
        out2 = { dd2[0][0+:3], dd1[1][2:3], dd1[0][0+:1] };
    end

endmodule
