
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function which returns the multiplied
// value of its two input arguments. This function is called recursively from an
// always block.

module test(i1, i2, o1, o2);

parameter WIDTH = 8;

input signed [WIDTH-1:0] i1, i2;
output reg signed [WIDTH-1:0] o1, o2;
reg [WIDTH-1:0] t1, t2;
reg [(WIDTH/2) - 1:0] t3, t4;

function bit [WIDTH-1:0] fn(int x, int y);
    return x * y ;
endfunction

always @(i1 or i2)
begin
        t1 = fn((i1 + i2) , (i1 - i2));
        t2 = t1 + fn(2, fn(i1, i2)) + fn(2, fn(i2, i2));
        t3 = t2 + (i2 - i1);
        t4 = i2 >>2;
        t1 = fn(t3, (t4 - t3));
        t2 = (t4 - (t1 + t3));

        o1 = (t1 + ~t2);
        o2 = (t2 & t1);
end

endmodule

