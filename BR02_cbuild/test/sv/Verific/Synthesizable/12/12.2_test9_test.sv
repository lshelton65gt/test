
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The direction of a task port is inherited by the non-specified ports
// from its previously declared port. This design uses this property and defines a task
// which does not define direction for the second and third port. These are of same
// direction as the first port. The first port is defined to be an inout port.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg r1, r2, r3;

always @(posedge clk)
begin
    r1 = in2 | in1;
    r2 = { in1[0:width/2-1], in2[width/2:width-1] };
    r3 = in2<<width/2 + in1>>width/2;

    task1(r1, r2, r3);
    out1 = r2 - r1 & r3;

    task1(r1, r2, r3);
    out2 = r1 ^ r2 + r3;
end

task task1(inout int a, b, c);
begin
    reg t1, t2, t3;

    t1 = a + b + c;
    t2 = a | b | c;

    if (t1 == t2)
        t3 = a ^ b ^ c;
    else
        t3 = a & b & c;

    a = t1 | t2 ^ t3;
    b = t3 - t1 + t2;
    c = t2 & t3 | t1;
end
endtask

endmodule

