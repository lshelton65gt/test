
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function having two ports and last
// port contains default value. This function is called providing only first argument
// and in another call two arguments are specified.

module test (i, o) ;

    parameter p = fn1(2) ;
    
    initial
      $display("The obtained value is : %d", p) ;
    
    input [p-1:0] i ;
    output [0:p-1] o ;
    integer i1 ;
    assign o = ~fn1(i, i) ;
    initial
      $display("The obtained value is : %d", i1) ;


    function integer fn1(int in1, int in2 = 4) ;
        return in1 % in2 ;
    endfunction

endmodule

    
