
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a constant function. A 'case-generate' statement is used
// to generate one of the 'always' blocks defined. The 'case' items are constant
// function calls. More than one 'case' item is used. All of them are constant
// function calls.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [w-1:0] out1, out2);

    generate

        case(w)
            case_item(1), case_item(2), case_item(3), case_item(4):    // case items are function calls
                always@(posedge clk)
                begin
                    out1 = in1 - in2;
                    out2 = in2 * in1;
                end
            case_item(5), case_item(6), case_item(7), case_item(8):    // case items are function calls
                always@(posedge clk)
                begin
                    out1 = in1 & in2;
                    out2 = in2 | in1;
                end
            default:
                always@(posedge clk)
                begin
                    out1 = in1 ^ in2;
                    out2 = in2 + in1;
                end
        endcase

    endgenerate

    function int case_item(int n);    // the constant function is defined here
        int r;

        r = n - 1;    // just returns value decremented by one

        return r;
    endfunction

endmodule

