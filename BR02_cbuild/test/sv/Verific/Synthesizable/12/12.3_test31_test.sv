
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function. Inside the function it uses a
// for-loop and a case statement. Depending on them it returns different values.
// Inside an always_block it calls the function and uses that return value to set
// the output port value.

module test(input clk, input [31:0] i1, input [32*64-1:0] i2, output reg [32*64-1:0] o1);
    
function logic [31:0] f(input [31:0] x, y);
reg [31:0] t1[7:0];
t1[0] = 4;
for(int i=0; i<=7;i++)
    t1[i] = i[0] ? {4'(x), 4'(y)}:{4'(~x), 4'(~y)};
case (x)
     0,1,2: return t1[1];
     3,4,5: return t1[2];
     6:     return t1[3];
    default:return 0;
endcase
endfunction

always @(posedge clk)
begin
    for (int m=32*64 -1; m>=0; m -= 32)
    begin
        o1[m-:32] = f(i1, i2);
    end
end

endmodule

