
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): In SystemVerilog the data type for formal argument is same as
// that of the previously defined type if no type is specified. This design defines a
// function and defines the data type for the first formal output argument and does not
// specify the type for the next argument to check whether it inherits the data type.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2, r3, r4;

always @(posedge clk)
begin
    r1 = func1(in1, in2, r3, r4);
    out1 = r1 | r4 * r3;

    r2 = func1(in2, in1, r3, r4);
    out2 = r2 & r3 ^ r4;
end

function automatic int func1(input int a, b, output byte c, d);
    c = d ;
    d = a ^ b - a;
    d |= c; 

    return c;
endfunction

endmodule

