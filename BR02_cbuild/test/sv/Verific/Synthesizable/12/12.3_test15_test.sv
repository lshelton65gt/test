
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): In SystemVerilog, once a direction has been specified for a formal,
// the subsequent formals without a direction are treated to be of same direction as the
// one previously declared. This design defines a function with a formal argument with
// unspecified direction, making it an input type implicitly. Then it checks whether the
// next formal (also of unspecified direction) is treated as input.

module test #(parameter width=8) 
             (input clk,
              input [width-1:0] in1, in2,
              output reg [width-1:0] out1, out2);

reg [31:0] r1, r2;

always@(posedge clk)
begin
    r1 = { 8'b0, {8{|in1}}, 8'd255, {8{^in2}} };
    r2 = r1 | in1[width/2-1:0] + in2[width-1:width/2];

    out1 = func1(r1-in1, r2+in2);
    out2 = func1(r2*in1, r1^in2);
end

function automatic int func1(int a, b /* treating b as input */);
    int t, u;

    t = ((^a)?(a-b):(b-a));
    u = ((^b)?(b-t):(t-b));

    return ((t|u)<<1);
endfunction

endmodule

