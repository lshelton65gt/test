
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows static variables to be declared inside an
// automatic task. This design defines an automatic task and defines a static variable 
// inside that to check whether it is actually static in nature.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    task1(in1, in2, out1);
    task1(in2, in1, out2);
end

task automatic task1(input [width-1:0] a, b, output [width-1:0] c);
    static int count = 0;

    count++;

    c = a ^ b - a & b;
endtask

endmodule

