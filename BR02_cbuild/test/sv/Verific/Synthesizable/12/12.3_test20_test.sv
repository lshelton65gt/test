
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Values from a function can be returned by assigning the
// value to the name of the function or by using the return statement. This
// design defines a non-void function and returns something from it/ using
// the 'return' statement.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = ret_func(ret_func(in1, in2), in2);
    out2 = ret_func(in1, ret_func(in2, in1));
end

function [0:width-1] ret_func(reg [0:width-1] a, b);
    reg [width-1:0] r1, r2;

    r1 = a<<1 - b>>1;
    r2 = a>>1 + b<<1;

    return r1 | r2;
endfunction

endmodule

