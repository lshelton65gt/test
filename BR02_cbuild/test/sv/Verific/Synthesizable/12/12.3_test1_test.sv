
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog supports empty functions. This design defines
// a void, empty function and calls that function from an always block.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = in1 * in2;
    func1(in1, in2);
    out2 = in1 ^ in2;
    func1(in2, in1);
    if ({width{1'b1}}==out1|out2)
    begin
        out1 = out1&out2;
        out2 = out1^out2;
    end
end

function void func1(int a, b);
endfunction

endmodule

