
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The direction of a task port is inherited by the non-specified ports
// from its previously declared port. This design uses this property and defines a task
// which does not define direction for the second and third ports. These are of same
// direction as the first port. The first port is defined to be an input port.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    task1(in1, in2, in1|in2, out1);
    task1(in2, in2-in1, in1, out2);
end

task task1(input int a, b, c, output int d);
begin
    reg r1, r2;

    r1 = a + b;
    r2 = c - b;

    d = c + a - r1 | r2;
end
endtask

endmodule

