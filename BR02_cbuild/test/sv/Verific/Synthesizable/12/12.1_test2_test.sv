
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two structure types. It also defines one
// task and two functions - all of them take arguments of the new types. The two
// functions return data element of those new type. They are called from an always
// block to set the output port.

module test(clk, i1, i2, o1, o2, o3, o4);

parameter WIDTH = 32;

typedef logic [0:2*WIDTH -1] tfl_type;
typedef bit [0:2*WIDTH -1] tfb_type;

input clk;
input [0:2*WIDTH-1] i1, i2;
output reg [0:2*WIDTH-1] o1, o2;
output reg [0:(2*(2*WIDTH+5))-1] o3;
output reg signed o4;

always @(posedge clk)
begin
    o1 = fn1(i1);
    tsk_rev(i2, o2);
    
    o3 = { i1, i2 };
    
    o4 = fn2(i1, i2);
end

task tsk_rev(input tfl_type in ,output tfl_type out);
        for(int i=0; i<WIDTH;i++)
        begin
                out[(WIDTH-i-1)*2 +: 2] = in[i*2 +: 2];
        end
endtask

function tfl_type fn1(input tfl_type in);
reg [0:2*WIDTH-1] t3;

        for(int i=0; i<WIDTH; i++)
        begin
                t3[(WIDTH-i-1)*2 +: 2] = in[i*2 +: 2];
        end

        return t3;

endfunction

function signed fn2 (input tfb_type [0:2*WIDTH-1] i1, i2);

    for(int i=0; i<WIDTH; i++)
    begin
        if (i1[i*2 +: 2] > i2[i*2 +: 2])
            return 1'b0;
        else
            if (i1[i*2 +: 2] < i2[i*2 +: 2])
                return 1'sb1;
    end
    return 1'b1;
endfunction

endmodule

