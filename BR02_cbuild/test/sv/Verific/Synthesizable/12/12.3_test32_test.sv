
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows void data type as the return
// value from a function, which means that the function does not return
// a value. This design defines a function with void return type.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    void_func(in1, in2, out1);
    void_func(in2, in1, out2);
end

function void void_func(input int a, b, output int c);
    int t1, t2;

    t1 = a<<16|a>>16;
    t2 = b>>16|b<<16;

    c = t1 ^ t2;
endfunction

endmodule

