
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The direction of a task port is inherited by the non-specified ports
// from its previously declared port. This design uses this property and defines a task
// which does not define direction for the second and third ports. These should inherit 
// the direction of the first port. The direction of the first port is also not specified 
// making it an implicit input port.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg r1, r2;

always @(posedge clk)
begin
    task1(in1, in2, in1&in2, r1, r2);
    out1 = r2 ^ r1;

    task1(in2^in1, in1, in1-in2, r2, r1);
    out2 = r1 & r2;
end

task task1(int a, b, c, output int d, e);
begin
    reg t1, t2;

    t1 = a ^ b - c;
    t2 = b | c & a;

    d = t1 & t2;
    e = t2 + t1;
end
endtask

endmodule

