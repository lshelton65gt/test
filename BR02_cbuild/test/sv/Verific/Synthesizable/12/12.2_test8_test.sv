
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The direction of a task port is inherited by the non-specified ports
// from its previously declared port. This design uses this property and defines a task
// which does not define direction for the second and third port. These are of same
// direction as the first port. The first port is defined to be an output port.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg r1, r2, r3;

always @(posedge clk)
begin
    task1(r1, r2, r3, in1, in2);
    out1 = r2 - r1 & r3;

    task1(r1, r2, r3, in2, in1);
    out2 = r1 ^ r2 + r3;
end

task task1(output int a, b, c, input int d, e);
begin
    reg t1, t2;

    t1 = d - e;
    t2 = d<<width/2 | e>>width/2;

    a = t1 + t2 & d;
    b = t2 ^ t1 - e;
    c = t1 | t2 + d;
end
endtask

endmodule

