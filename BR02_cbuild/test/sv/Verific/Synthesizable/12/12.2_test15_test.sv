
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): In SystemVerilog task can stop executing mid-way and return
// to the caller using the return statement. This design defines a task and returns
// from it in mid-way using the return statement.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    task1(in1, in2, out1);
    task1(in2, in1, out2);
end

task task1(input [width-1:0] a, b, output [width-1:0] d);
    reg [width-1:0] r1, r2;

    r1 = { a[width-1:width/2], b[width/2-1:0] };
    r2 = { b[width-1:width/2], a[width/2-1:0] };

    d = r1 | r2;
    d = ((0 == d)?('1):(d));

    return;

    d = 0;  /* This is never executed */
endtask

endmodule

