
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design does chaining of arithmatic, bit-wise etc operators
// by instantiating a task and providing the output of the previous instance of the
// task to the next instance. The task's output port is of a packed structure type.

module test(clk, i1, i2, o1, o2, o3, o4);

parameter WIDTH = 16;

typedef struct packed {
    logic [0:WIDTH-1] x1;
    logic [0:WIDTH-1] x2;
} ptype;

input clk;
input signed [0:WIDTH-1] i1, i2;
output reg signed [0:WIDTH-1] o1, o2, o3, o4;

reg [WIDTH-1:0] t1, t2, t3, t4;

always @(posedge clk)
begin
        task1(i1, {t1, t2});
        task1(i2, {t3, t4});

        task1(t1+t4,{o1, t3});
        task1(t2+t3,{t4, o2});
        task1(t3+t4,{o3, o4});
end

task task1;
input signed [WIDTH-1:0] a;
output ptype b;
reg [WIDTH-1:0] r1, r2;

        r1 = a[WIDTH/2-1:0] * a[WIDTH/2:0];
        r2 = { ~a[WIDTH-1], a[WIDTH-2:0] } ^ ~a;

        b = {r1 + r2, r1 | r2};
endtask

endmodule

