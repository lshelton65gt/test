
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function with two ports. First port 
// has a default value, but second port has no default value. This function is called
// once with providing only argument having no default value and second time with actuals
// for both ports.

module test (i1, i2, o) ;

    parameter p = fn1(.j(2)) ;

    input [p-1:0] i1, i2 ;
    output [0:p-1] o ;

    assign o = ~fn1(i1, i2) ;

    function integer fn1(int i = 1, int j) ;
        return i+ j ;
    endfunction

endmodule

