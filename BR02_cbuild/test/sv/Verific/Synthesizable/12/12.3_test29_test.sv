
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function and calls it to carry out some
// arithmatic, concatenation, shift etc operations on the inputs. The return value 
// is then again feed to the same function to generate chaining. The design also 
// performs some logical, bitwise and arithmetic operators on variables of packed
// structure type.

module test(clk, i1, i2, o1, o2);

parameter WIDTH = 16;

typedef struct packed {
    bit [WIDTH -1:0] a;
    bit [WIDTH -1:0] b;
    bit [WIDTH -1:0] c;
    bit [WIDTH -1:0] d;
} dtype;

input clk;
input signed [0:WIDTH-1] i1, i2;
output reg signed [0:WIDTH-1] o1, o2;

reg [WIDTH-1:0] t1, t2, t3, t4;
dtype d1,d2;

always_comb
begin
        t1 = func1(i1, i2);
        t2 = func1(i2, i1);
        t3 = func1(t1, t2);
        t4 = func1(t2, t1);
        d1 = {func1(t1,t2), func1(t2,t3), func1(t3,t4), func1(t4,t1)};
        d2 = {func1(t1,t3), func1(t2,t4), func1(t3,t1), func1(t4,t2)};
end

always @(posedge clk)
begin
        if(d1==d2)
        begin
            o1 = func1(t3, t4) + func1(t4, t3);
            o2 = func1(t2, t4) - func1(t3, t1);
        end
        else
        begin
            o1 = func1(t3, t4) | func1(t4, t3);
            o2 = func1(t2, t4) & func1(t3, t1);
        end
end

function int func1(int i1,int i2);
reg signed [WIDTH-1:0] t1, t2, t3, t4, t5, t6, t7, t8, t9, t10;
begin
        t1 = i1[WIDTH/2-1:0] << WIDTH/4;
        t2 = i1[WIDTH-1:WIDTH/2] >> WIDTH/4;
        t3 = i2[WIDTH/2-1:0] << WIDTH/4;
        t4 = i2[WIDTH-1:WIDTH/2] >> WIDTH/4;

        t5 = t1 | t2;
        t6 = t3 | t4;
        t7 = { t1[WIDTH-1:WIDTH/2], t2[WIDTH/2-1:0] };
        t8 = { t3[WIDTH-1:WIDTH/2], t4[WIDTH/2-1:0] };
        t9 = t1 + t2;
        t10 = t3 + t4;

        t1 = t5 ^ t6;
        t2 = t7 - t8;
        t3 = t9 & t10;

        func1 = t1 - t2 + t3;
end
endfunction

endmodule

