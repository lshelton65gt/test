
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Desig

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): System Verilog allows inout and output ports in function
// declaration. This design uses input and output port in function.

function bit add_func(input int no1, int no2, output bit out) ;
    int sum ;
    sum = no1 + no2 ;

    if (sum == (no1 + no2))
        out = 1'b1 ;
    else
        out = 1'b0 ;

    return 1'b1 ;
    
endfunction

module test(input bit clk, int no1, int no2, output bit mod_out ) ;
    bit out ;

    always @ (posedge clk)
        mod_out = add_func(no1, no2, out) ;
endmodule

