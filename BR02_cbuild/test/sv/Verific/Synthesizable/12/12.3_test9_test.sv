
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows direction of formal parameters of a function
// to be specified as input or output. This design does not specify any direction and
// checks that it is treated as an input argument.

module test #(parameter width=8) 
             (input clk,
              input [width-1:0] in1, in2,
              output reg [width-1:0] out1, out2);

reg [31:0] r1, r2;

always@(posedge clk)
begin
    r1 = in2 * in1;
    r2 = in1 ^ in2;

    out1 = func1(r1|in1, r2);
    out2 = func1(r2, r1|in2);
end

function automatic int func1(int a, b);
    int t1, t2, t3;

    t1 = a|'1;
    t2 = b^'1;

    t3 = t1 + t2 & a - b;

    return t3;
endfunction

endmodule

