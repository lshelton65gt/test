
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two tasks and a function. The tasks are 
// called from an always block. Both of the task manipulates some global variables
// by calling the function. These global variables are used inside an always_comb
// block to set the value of the output port.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter WIDTH = 32;

input clk;
input signed [WIDTH-1:0] i1, i2, i3, i4;
output reg signed [WIDTH-1:0] o1, o2;

bit signed l1, l2, l3, l4;
reg [30:0] r1;
reg signed [31:0] r2;
reg signed [32:0] r3;

function bit fn(int x, bit [1:0] y);
bit tmp;
tmp=x[0];
    for(int i=1;i<32;i++)
    begin
        case(y)
        2'b00: tmp|=x[i];
        2'b01: tmp&=x[i];
        2'b10: tmp^=x[i];
        2'b11: tmp+=x[i];
        endcase
    end
    return tmp;
endfunction

task task1;

    l1 = fn(i1,1) | ~fn(i2,1);
    l2 = fn(i3,0) & fn(i4,0);
    l3 = fn(i2,2) + ~fn(i4,0);
    l4 = fn(i4,3) - fn(i1,3);

endtask

task task2;

    r1 = i3 * l2;
    r2 = (i2 + i3) - l3;
    r3 = (i1 ^ i4) + l4;

endtask

always @(posedge clk)
begin
    task1;
    task2;
end

always_comb
begin

    if ((l1 || l4) && !(l2 == l3))
    begin
        o1 = ~(r2 - r1);
        o2 = (r3 + r2)/r1;
    end
    else
    begin
        o1 = r1 + r2;
        o2 = r3 - r2;
    end

end

endmodule

