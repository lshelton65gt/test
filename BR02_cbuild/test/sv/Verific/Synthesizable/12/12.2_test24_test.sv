
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a task. Calls/enables the task to do
// some operation. The task is a blocking task but it is called in every clock
// tick at posedge to check the blocking task handling capability.

module test(clk, in1, in2, in3, o1, o2);

parameter WIDTH = 16;

typedef struct packed {
    logic [0:WIDTH -1] a;
    bit [0:WIDTH -1] b;
    logic [0:WIDTH -1] c;
} atype;

input clk;
input [0:WIDTH-1] in1, in2, in3;
output [0:WIDTH-1] o1, o2;
atype d1, d2, d3, d4;
int r1 = 0, r2 = 0;

always_comb
begin
    d1 = {in1 + in2, in1 - in2, in1 ^ in2};
    d2 = {in2 + in3, in2 - in3, in2 ^ in3};
    d3 = {in3 + in1, in3 - in1, in3 ^ in1};
    d4 = {in1 + in3, in1 - in3, in1 ^ in3};
end

always @(posedge clk)
begin
        task1(d1, d2);
        task1(in2, d3);
        task1(d3, d4);
end

assign o1 = r1;
assign o2 = r2;

task task1(atype in1, in2);
reg [WIDTH-1:0] t1, t2;
    t1 = in1 | ~in2;
    t2 = in1 ^ in2;
    
    if (t1 == t2)
    begin
        r1 |= t1 + t2;
    end
    else
    begin
        r1 ^= t1 << 1 + t2 >> 1;
    end
    t1 = in1 - in2;
    t2 = in2 - in1;
    
    if (t1 + t2 == 0)
    begin
        r2 += ((t1>t2)?(t1):(t2));
    end
    else
    begin
        r2 -= ((t1<0)?(-t1):((t2<0):(-t2):(t2)));
    end
endtask

endmodule

