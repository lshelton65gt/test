
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Values from a function can be returned by assigning the
// value to the name of the function or by using the return statement. The 
// return statement should override the value assigned to the name of the
// function. This design defines a non-void function and returns something
// from it using the 'return' statement which overrides a previously assigned
// value to the name of the function.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = func1(in1, in2);
    out2 = func1(in2, in1);
end

function [0:width-1] func1(reg [0:width-1] a, b);
    func1 = a | b;
    return ~func1 ;
endfunction

endmodule

