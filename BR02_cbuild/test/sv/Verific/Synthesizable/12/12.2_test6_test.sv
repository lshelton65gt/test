
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The default direction of a task port is input. This design uses this 
// property and defines a task which does not have input ports declared explicitly. 

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    task1(in1, in2, out1);
    task1(in2, in1, out2);
end

task task1(/* default direction is input */ int a, b, output int c);
begin
    c = a - b;
end
endtask

endmodule

