
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function with two arguments. Depending
// on the 0th element of the second argument it carries out some action on them and
// returns the result. This function is called inside and always block recursively.
// The result is then used in expression to assign the values of the output port.

module test(clk, i1, i2, o1);

parameter IN_WIDTH = 8;
parameter OUT_WIDTH = 16;

input clk;
input signed [IN_WIDTH-1:0] i1, i2;
output reg signed [OUT_WIDTH-1:0] o1;

reg signed [OUT_WIDTH-1:0] r1, r2, r3, r4, r5;

function int fn(int x, int y);
    int pos,z=x;
    if(y[0])
    begin
        for(int i=1;i<32;i++)
        begin
            if(y[i])
                z <<= i;
        end
        
    end
    else
    begin
        for(int i=1;i<32;i++)
        begin
            if(y[i])
                z <<= i;
        end
        z+=x;
    end
    return z;
endfunction

always @(posedge clk)
begin
        r1 = ((i1 == { IN_WIDTH { 1'b0 } }) ? { IN_WIDTH { 1'b1 } } : i1);
        r2 = ((i2 != { IN_WIDTH { 1'b1 } }) ? { IN_WIDTH { 1'b1 } } : i2);

        if (r1 != r2)
        begin
                r3 = fn((r1 + r2),fn((r1 + r2),(r1 + r2)));
                r4 = fn(r1,fn(r1,r1)) + 3*fn(r1,fn(r2,(r1 + r2))) + fn(r2,fn(r2,r2));
                r5 = fn(fn(r1,r1),r1) + 3*fn(r1,fn(r1,r2)) + 3*fn(r1,fn(r2,r2)) + fn(r2,fn(r2,r2));
        end
        else
        begin
                r3 = fn((r1 - r2),fn((r1 - r2),(r1 - r2)));
                r4 = r1*r1*r1 - 3*r1*r2*(r1 - r2) - r2*r2*r2;
                r5 = r1*r1*r1 - 3*r1*r1*r2 - 3*r1*r2*r2 - r2*r2*r2;
        end

        if (r1 == r2 == r3)
        begin
                o1 = 3*((r1+r2+r3)/3);
        end
        else
        begin
                o1 = fn(r1,r2*r3);
        end
end

endmodule

