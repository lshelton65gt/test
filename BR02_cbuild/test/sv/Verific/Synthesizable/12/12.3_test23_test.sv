
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows empty functions. This design defines
// a non-void empty function to check how it is handled by the tool. As it is
// non-void, it must return a value, but it is an empty function so it can not
// return a value.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = func1(in1, in2);
    out2 = func1(in2, in1);
end

function int func1(int a, b);
endfunction

endmodule

