
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function with two ports. The first
// port has a default value, but the second one does not have any default value.
// This function is used as constant function by providing only second argument
// in positional connection mode and it is used as non constant function call by
// providing two arguments.

module test (i1, i2, o) ;

    parameter p = fn1(, 2) ;

    input [p-1:0] i1, i2 ;
    output [0:p-1] o ;

    assign o = ~fn1(i1, i2) ;

    function integer fn1(int i = 4, int j) ;
        return i * j ;
    endfunction

endmodule

