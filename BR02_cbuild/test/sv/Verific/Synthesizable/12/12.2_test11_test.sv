
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The direction and data type of a task port is inherited
// by the non-specified ports from its previously declared port. This design uses
// this property and defines a task which does not define type or direction for
// the second and third ports. These are of same direction as the first port.
// The direction of the first port is also not specified making it an implicit
// input port.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg r1, r2;

always @(posedge clk)
begin
    task1(|in1, &in2, ^{width{1'bz}}, out1, r1);
    task1(^in2, &in1, |(r1|{width{1'bz}}), r2, out2);

    out1 = out1|r2;
    out2 = out2^r2;
end

task task1(a, b, c /* default type is logic and direction is input */, output [width-1:0] d, e);
begin:bb
    d = {width{a + b + c}};
    e = {width{a | b | c}};

    d[width/2-1] = 1'bz;
    e[width/2-1] = 1'bz;
end
endtask

endmodule

