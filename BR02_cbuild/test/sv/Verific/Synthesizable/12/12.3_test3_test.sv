
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows specifying array of data type for formal
// arguments of a function. This design defines a function and defines a formal
// argument of type output to be an array.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

bit ret;
logic [31:0] x;

always @(posedge clk)
begin
    ret = func1(in2, in1, x);
    out1 = x[width-1:0];
    if (1 == ret)
        out2 = in1 & in2;
    else
        out2 = in1 ^ in2;
end

function bit func1(input logic [31:0] a, b, output logic [31:0] c);
    bit r ;
    
    r = 0;
    for(int i=0; i<32; i++)
    begin
        c[i] = ((a[i]*b[i])?(1'o1):(1'h0));
        r ^= c[i];
    end

    return r;
endfunction

endmodule

