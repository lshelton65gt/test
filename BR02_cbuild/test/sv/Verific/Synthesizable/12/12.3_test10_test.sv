
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing the user defined type in formal argument.

typedef int myint[1:0];
typedef struct {
		  bit [3:0]strBit;
                  myint strint;
	       }  myStr;

function void func(bit [3:0]funcBit, myint funcint, output myStr str);
    str.strBit = funcBit;
    for(int i = 1; i >= 0; i--)
        str.strint[i] = funcint[i];
endfunction

module test(input bit [3:0]inModBit, int inModInt[1:0], output bit [3:0]outModBit, int outModInt[1:0]);

    myStr str;
    initial
    begin
        func(inModBit, inModInt[1:0], str);
        outModBit = str.strBit;
        for(int j = 1; j >= 0; j--)
            outModInt[j] = str.strint[j];
    end

endmodule

