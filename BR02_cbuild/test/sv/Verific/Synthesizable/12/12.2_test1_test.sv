
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): In SystemVerilog array can be specified as a formal
// argument to a task. This design defines a task with arguments of array type.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

reg ir1 [width-1:0];
reg ir2 [width-1:0];
reg ir3 [width-1:0];
reg ir4 [width-1:0];

always @(posedge clk)
begin
    for(int i=0; i<width; i++)
    begin
        ir1[i] = in1[i] & in2[i];
        ir2[i] = in1[i] | in2[i];
        ir3[i] = in1[i] + in2[i];
        ir4[i] = in1[i] ^ in2[i];
    end

    task1(ir1, ir2, out1);
    task1(ir3, ir4, out2);
end

task task1(input a [width-1:0], b [width-1:0], output [width-1:0] d);
begin
    for(int i=0; i<width; i++)
    begin
        d[i] = a[i] ^ b[i];
    end
end
endtask

endmodule

