
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Function declared without any argument but with parentheses
// is called without any parentheses or argument.

module test (i, o) ;

    parameter p = fn1() ;

    input [p-1:0] i ;
    output [0:p-1] o ;

    assign o = ~i ;

    function integer fn1() ;
        return 8 ;
    endfunction

endmodule

