
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): In SystemVerilog every port has an associated data type. The default
// data type of a port is logic. This design checks the default data type.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    task1(in1, in2, out1);
    task1(in2, in1, out2);
end

task task1(input [width-1:0] a, b, output [width-1:0] d);
begin
    d = a - b;
    d[width/2-1] = 1'bz; /* default data type is logic, 4 state */
end
endtask

endmodule

