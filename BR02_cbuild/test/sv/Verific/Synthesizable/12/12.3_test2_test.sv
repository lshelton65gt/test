
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows specifying array of data type for formal
// arguments of a function. This design defines a function and defines a formal
// argument of type input to be an array.

module test #(parameter width=8)
             (input clk,
              input  logic in1[0:width-1], in2[0:width-1], 
              output reg [0:width-1] out1) ;


always @(posedge clk)
begin
    out1 = func1(in1, in2);
end

function int func1(input logic a[width-1:0], b[width-1:0]);
    int ret;
    for(int i=0; i<width; i++) 
        ret[i] = ((a[i]==b[i])?(1'b0):(1'd1));

    return ret;
endfunction

endmodule

