
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Using a non-void function as a statement should generate an
// warning. This design defines a non-void function and uses it as statement.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    func1(in1, in2, out1);
    func1(in2, in1, out2);
end

function byte func1(byte a, b, output [width-1:0] o);
    o = a - b;

    return a * b;
endfunction

endmodule

