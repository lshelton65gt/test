
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog parameters are passed call by value. That means
// the value is copied to the formal parameter before calling if they are of input
// type, and copied back to caller variable if they are of output type. This design
// defines a function test this property.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = func1(in1, in2, out2);
end

function automatic int func1(input int a, b, output int o);
    int t1, t2;

    t1 = a | b;
    t2 = b - a;

    o = t1 ^ t2;

    a = (b = '0);
    b = 1'd1;

    return (t1 & t2);
endfunction

endmodule

