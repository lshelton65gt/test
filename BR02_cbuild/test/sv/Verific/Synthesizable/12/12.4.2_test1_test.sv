
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): To test the 'ref'(referance) keyword.
// Arguments passed by reference must be matched with equivalent data types.
// Packed arrays, packed structures, and built-in integral types are equivalent 
// if they contain the same number of total bits, are either all 2-state or 
// all 4-state, and are either all signed or all unsigned. 
// eg.: 
//      typedef bit signed [7:0] BYTE;     // equivalent to the byte type
//      typedef struct packed signed {bit[3:0] a, b;} uint8; // equivalent to the byte type. 

typedef struct packed signed {
    bit [0:1000][0:7] msg ;
    bit [7:0] addr ;
    bit parity ;
} packet1;

typedef bit signed [8016:0] stream ;  // 1001*8 + 8 + 1

function automatic bit crc(ref stream pack) ; 
    byte ret_value = 8'h00 ;
    for( int j = 0; j <= 8000; j+=8 )
    begin
        ret_value ^= pack[j+:7] ;
    end
    if (ret_value > 8'h0f) return 1'b0 ;
    else return 1'b0 ;
endfunction

module test ( input  bit enable, bit clk,
                      input  bit [0:1000][0:7] message, 
                      output bit valid ) ;
    packet1 pack ;
    always @ (negedge clk)
    begin
        pack.msg = message ;
        pack.addr = 8'b11001100 ;
        pack.parity = '1;
    end

    always @ (posedge clk)
    begin
        valid = 1'b0 ;
        if (enable)
        begin
            if (crc( pack ))
                valid = 1'b1 ;

             else valid = 1'b0 ;
        end
        else valid = 1'b0 ;
    end
endmodule

