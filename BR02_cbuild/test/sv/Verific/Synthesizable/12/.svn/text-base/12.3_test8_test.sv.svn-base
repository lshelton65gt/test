
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows direction of formal parameters of a function
// to be specified as input or output. This design defines a function with a formal
// argument declared as output and checks whether it is actually acting as an output
// parameter.

module test #(parameter width=8) 
             (input clk,
              input [width-1:0] in1, in2,
              output reg [width-1:0] out1, out2);

reg [31:0] r1, r2;

always@(posedge clk)
begin
    r1 = in2 - in1;
    r2 = in1 & in2;

    out1 = func1(r1, r2, out2);
end

function automatic int func1(int a, b, output int c);
begin
    int t1, t2;

    t1 = a>>width/2;
    t2 = b<<width/2;

    c = t1 | t2 - a + b;

    return c;
end
endfunction

endmodule

