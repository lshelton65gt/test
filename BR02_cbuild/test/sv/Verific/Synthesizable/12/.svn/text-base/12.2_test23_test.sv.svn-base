
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two comparable structure type. Two data
// elements of each are declared. These are manipulated inside a task. The task is
// called from an always_comb block. They are used to set the value of some of the
// output ports.

module test(clk, i1, i2, i3, o1, o2, o3, o4, o5);

parameter WIDTH = 8;
typedef struct {
    int x;
    int y;
} dtype;

typedef struct packed {
    int x;
    int y;
} dtype1;

input clk;
input [WIDTH-1:0] i1, i2, i3;
output reg [3*WIDTH-1:0] o1, o2, o3, o4;
output [3*WIDTH-1:0] o5;

reg [3*WIDTH-1:0] t1, t2, t3;
dtype d1,d2;
dtype1 e1,e2;

task task1(input bit [WIDTH -1:0] a,input bit [WIDTH -1:0] b);
    d1.x = a + b;
    d1.y = 2*a + (b<<2);
    d2.x = a - b;
    d2.y = 4*a - (b<<1);
    e1 = {d1.x, d2.x};
    e2 = e1 | {d1.y, d2.y};
endtask

always_comb
begin
        o1 = {i1, i2, i3};

        t1 = i1 << 2*WIDTH;
        t2 = i2 << WIDTH;
        t3 = i3;
        task1(i1,i2);
       
        o2 = d1.x + d1.y - t1 + t2 + t3;

        o3 = (t1 | t2 | t3) & d1.x & d1.y ;

        o4 = i1 * (1<<(2*WIDTH)) + i2 * (1<<WIDTH) + i3 + (e1 ^ e2);
end

assign o5[WIDTH-1:0] = i1;
assign o5[2*WIDTH-1:WIDTH] = i2;
assign o5[3*WIDTH-1:2*WIDTH] = i3;

endmodule

