
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two always blocks and a function. The 
// function returns logical not value of its input. Some local variables are 
// manipulated inside an always block by binary operations and by calling the 
// function. These local variables are used to set output ports.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter WIDTH=16;

input clk;
input signed [WIDTH-1:0] i1, i2, i3, i4;
output signed [WIDTH-1:0] o1, o2;

reg signed [WIDTH-1:0] r1;
reg [WIDTH-1:0] r2;
reg signed [WIDTH:0] r3, r4;

function logic one_bit_not(input reg in);
reg ret;
begin
        casex (in)
            1'b0: ret = 1'b1;
            1'b1: ret = 1'b0;
            1'bx: ret = 1'bx;
            1'bz: ret = 1'bz;
            default: ret = 1'bx;
        endcase

        return ret;
end
endfunction

always @ (posedge clk)
begin
    r1 = i1|i3&i4^i2;
    r2 = i1^i2&i4|i3;
    r3 = r1|r2;
    r4 = r1&r2;
    for(int j=0; j<WIDTH; j++)
    begin
            r1[j] = one_bit_not(r3[j]);
            r2[j] = one_bit_not(r4[j]);
    end
end

assign o1 = r1;
assign o2 = r2;

endmodule

