
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two constant functions. A module is defined 
// inside the top module and the default value of its parameter is the return value of
// one of the function calls. The module is instantiated with the value of that
// parameter overridden with the other constant function call.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output [w-1:0] out1, out2);

    function int const_fn1(int n);    // the constant functions are defined here
        int r = n;
        ++r;
        return r;
    endfunction

    function int const_fn2(int n);
        int r = n;
        r--;
        return r;
    endfunction

    module bottom #(parameter p = const_fn1(7)) // constant function call to set the defaut value
                   (input clk,
                    input [p-1:0] in,
                    output reg [p-1:0] out);

        always@(posedge clk)
        begin
            out = ~in + 1'b1;
        end

    endmodule

    bottom bot1(clk, in2, out1);
    bottom #(.p(const_fn2(9))) bot2(clk, in1, out2); // constant function call to over-ride the defaut value

endmodule

