
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Return value from a function can be a structure. This
// design defines a non-void function and returns a structure type variable
// from the function.

typedef struct { int x ; int y ; } point_t ;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2) ;

always @(posedge clk)
begin
    point_t p ;

    p = func1(in1, in2) ;

    out1 = p.x ;
    out2 = p.y ;
end

function point_t func1(int a, b) ;
    point_t p1 = '{ a, b } ;

    p1.x = -p1.x ;
    p1.y = -p1.y ;

    return p1 ;
endfunction

endmodule

