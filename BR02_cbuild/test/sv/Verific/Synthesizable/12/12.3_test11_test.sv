
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): In SystemVerilog the data type for formal argument is same as
// that of the previously defined type if no type is specified. This design defines a
// function and defines the data type for the first formal argument and does not specify
// the type for the second argument to check whether it inherits the data type.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = func1(in1, {in2[0:width/2-1], 4'bxxxx});
    out2 = func1(in2, {in1[width/2:width-1], 4'bxxxx});
end

function automatic bit [8:0] func1(input byte a, b);
    byte b1, b2;

    b1 = a * b;
    b2 = b - a;

    return (b1+b2)>>1;
endfunction

endmodule

