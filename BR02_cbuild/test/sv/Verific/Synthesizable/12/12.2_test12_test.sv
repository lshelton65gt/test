
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog does not need the begin end block for a task.
// This design defines a task without the begin end block.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    task1(in1, in2, out1);
    task1(in2, in1, out2);
end

task task1(input [width-1:0] a, b, output [width-1:0] d);
    logic [width-1:0] r1, r2;

    r1 = a | b;
    r2 = b ^ a;

    d = r1<<width/2 + r2>>width/2;
endtask

endmodule

