
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design checks task enabling with inout port in that
// task. A task retains the value of an inout port when it is not manipulated by
// the task. It uses conditional assignment to avoid assigning value to the inout
// port in some condition. The design also defines an enumerated data type whose
// second value is calculated from the first value.

module test(clk, i1, i2, o1, o2, o3);

parameter WIDTH = 8, ALT_WIDTH = 4;

typedef enum {l1=ALT_WIDTH-1,l2=2*l1-1} limits;

input clk;
input [WIDTH-1:0] i1, i2;
output reg [WIDTH-1:0] o1, o2;
output reg o3;

reg [WIDTH-1:0] r1, r2;
reg [ALT_WIDTH-1:0] r3, r4;

always @(posedge clk)
begin
        r1 = i1;
        r2 = i2;
        r3 = i1[l2:(l2+1)/2] | i2[l1:0];
        r4 = r3;

        task1(r1, r3, r2);

        if (r3 ^~ r4)
        begin
                o1 = r1 + r3;
                o2 = r2 + r4;
                o3 = 1;
        end
        else
        begin
                o1 = r3 | r4;
                o2 = r2 & r1;
                o3 = 0;
        end
end

task task1(logic [l1:0] i1, inout [l1:0] inout1, output [l1:0] o1);

        if (i1 >= inout1)
        begin
                o1 = ~i1 + -inout1;
        end
        else
        begin
                o1 = i1 - +(~inout1);
                inout1 += i1;
        end

endtask

endmodule

