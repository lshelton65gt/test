
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design tests recursive function call.

module test(out1);
output  [31:0] out1;

function automatic int fact;
input int in1;
begin
        if(in1 == 1)
            return in1;
        else
            return in1*fact(in1 - 1);
end
endfunction

    assign out1 = fact(3);

endmodule

