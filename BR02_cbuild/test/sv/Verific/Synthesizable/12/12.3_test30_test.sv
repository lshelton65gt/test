
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses a function having a lot of arithmatic, bit
// wise and replication operators. The function is called multiple times with
// different variables, signed/unsigned.

module test(clk, in1, in2, in3, out1, out2, out3, out4, out5);

parameter WIDTH = 16;

input clk;
input signed [0:WIDTH-1] in1, in2, in3;
output reg signed [0:WIDTH-1] out1, out2, out3, out4, out5;

reg [0:WIDTH-1] t1, t2, t3;

always @(*)
begin
        t1 = func1(in1, in2);
        t2 = func1(in2, in3);
        t3 = func1(in3, in1);

        out1 = func1(t1, in3) + func1(in3, t3);
        out2 = func1(in1, t2) - func1(t2, in2);
        out3 = func1(t3, in2) + func1(t1, t3);
        out4 = func1(t2, in1) + func1(t2, in3);
        out5 = func1(in2, t1) + func1(in1, in3);
end

function automatic [WIDTH-1:0] func1;
input signed [WIDTH-1:0] in1, in2;
reg signed [WIDTH-1:0] r1, r2, r3, r4;
begin
        r1 = in1 - { WIDTH/5 { 5'hf } };
        r2 = in2 | in1;
        r3 = r2 + r1;
        r4 = r3 * ({ WIDTH/3 { 3'o7 } } & r2);

        if (in1 && r3)
        begin
                func1 = r2 << 2 + r3 >>> 3 | r4;
        end
        else
        begin
                func1 = r3 >> 3 - r2 <<< 2 + r4;
        end
end
endfunction

endmodule

