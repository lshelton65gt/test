
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses a task call to calculate modulus of two
// numbers without using the % operator. The direction of the input ports of the
// task are not specified, thus it should default to input. The task also returns
// by return statement before reaching the endtask keyword.

module test #(parameter WIDTH = 4)(input clk,input [WIDTH-1:0] i1, i2,output reg [WIDTH-1:0] o1, o2);

always @(negedge clk)
begin
        task1(i1, i2, o1);
        task1(i2, i1, o2);
end

task task1(logic [WIDTH-1:0] a, b, output [WIDTH-1:0] m);
int done;
reg [2*WIDTH:0] max_loop;
reg [WIDTH-1:0] dividend;

        max_loop = 2**WIDTH;
        dividend = a;
        done = 0;
        m = 0 ;

        for(int i=0; i<max_loop; i++)
        begin
                if (done == 0)
                begin
                        if (dividend > b)
                        begin
                                dividend -= b;
                        end
                        else
                        begin
                                m = dividend;
                                return;
                        end
                end
        end
endtask

endmodule

