
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Values from a function can be returned by assigning the
// value to the name of the function or by using the return statement. This
// design defines a non-void function and returns something from it by
// assigning the value to the name of the function.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = func1(in1|'1, in2^'0);
    out2 = func1(in1^'1, in2|'0);
end

function [0:width-1] func1(reg [0:width-1] a, b);
    reg [width-1:0] r1, r2;

    func1 = '0;

    for(int i=0; i<width; i++)
    begin
        r1[i] = a[i] * b[width-1-i];
        r2[i] = ^{ b[i], a[width-1-i] };
    end

    if (r1^r2)
        func1 = r1 | r2;
    else
        func1 = '1;
endfunction

endmodule

