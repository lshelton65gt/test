
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): In SystemVerilog, once a direction has been specified for a formal,
// the subsequent formals without a direction are treated to be of same direction as the
// one previously declared. This design defines a function with a formal argument declared
// as output and checks whether the next formal (unspecified direction) is treated also
// as output.

module test #(parameter width=8) 
             (input clk,
              input [width-1:0] in1, in2,
              output reg [width-1:0] out1, out2);

reg [31:0] r1, r2, r3, r4;

always@(posedge clk)
begin
    r1 = in1|in2;
    r2 = in1&in2;

    r3 = func1(r1, r2, out1, r4);
    func1(r1+r3|out1, r2-r4^out1, out1, out2);
end

function automatic int func1(int a, b, output int c, d /* treating d as output */);
    c = a - b;
    d = b - a;

    return ((c+d)<<1);
endfunction

endmodule

