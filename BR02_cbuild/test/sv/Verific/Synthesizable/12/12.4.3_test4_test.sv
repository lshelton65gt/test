
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function with one default argument and
// it is called without any argument and with argument 
// which overrides the default value.

module test (i, o) ;

    parameter p = fn1() ;

    input [p-1:0] i ;
    output [0:p-1] o ;

    assign o = ~fn1(i) ;

    function integer fn1(input integer i = 8) ;
        fn1 = i ;
        fn1 *= i ;
        return fn1 ;
    endfunction

endmodule

