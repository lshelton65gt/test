
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design implements a shifter. For this it defines a
// function which takes two arguments - one is the register and the other is the
// position. It returns the value at that position if it is valid or x otherwise.

module test(clk, reset, i1, o1, finished);

parameter WIDTH = 8;

input clk, reset;
input [WIDTH-1:0] i1;
output reg o1, finished;
function logic ith_bit(reg [WIDTH-1:0] data,int i);
    if(i<WIDTH)
        return data[i];
    else
        return 'x;
endfunction

int i;

always @(posedge clk or negedge reset)
begin
    if (!reset)
                i = 0;
    else
        begin
                o1 = ith_bit(i1,i++);
                if (i >= WIDTH)
                begin
                        i = 0;
                        finished = 1;
                end
                else
                        finished = 0;
        end
end

endmodule

