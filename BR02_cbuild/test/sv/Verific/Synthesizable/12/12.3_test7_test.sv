
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows direction of formal parameters of a function
// to be specified. This design defines a function and defines the direction of its ports
// as inputs.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = func1(in1|in2, in2^in1);
    out2 = func1(in2-in1, in1+in1);
end

function automatic int func1(input int a, b);
begin
    return a - b;
end
endfunction

endmodule

