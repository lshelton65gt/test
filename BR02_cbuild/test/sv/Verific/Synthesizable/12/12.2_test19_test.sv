
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses a task whose direction and data type of the
// ports are not specified. The task can return from mid-way depending on certain
// condition. This design also uses a constant function, whose return value is used
// in replication operator to replicate other variables.

module test(clk, i1, i2, o1, o2);

parameter IN_WIDTH = 8;
parameter OUT_WIDTH = 16;

input clk;
input signed [IN_WIDTH-1:0] i1, i2;
output reg signed [OUT_WIDTH-1:0] o1, o2;

function bit [7:0] fn(int x,bit y);
    if(y)
        return x[7:0];
    else
        return x[15:8];
endfunction

task mytask(signed [IN_WIDTH-1:0] a, b, output signed [OUT_WIDTH-1:0] c);
reg [OUT_WIDTH-1:0] x1, x2;

    if((a==='x)||(b==='x))
        return;
    x1 = { fn(OUT_WIDTH,1){ ~& a } };
    x2 = { fn(OUT_WIDTH,0){ ~| b } };
    x1 = ~x1 ^ b;
    x2 = ~x2 & a;
    c = x1 + x2;
endtask

always @(posedge clk)
        mytask(i1, i2, o1);

always @(negedge clk)
        mytask(i2, i1, o2);

endmodule

