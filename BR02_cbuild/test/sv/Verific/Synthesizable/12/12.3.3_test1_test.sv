
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function. A 'case' statement is used in the 'always'
// block to set the value of the output ports. The 'case' items are function
// calls. More than one 'case' item is used. All of them are function calls.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [w-1:0] out1, out2);

    always@(posedge clk)
    begin
        case(w)
            case_item(1), case_item(2), case_item(3), case_item(4):    // case items are function calls
            begin
                out1 = in1 - in2;
                out2 = in2 * in1;
            end
            case_item(5), case_item(6), case_item(7), case_item(8):    // case items are function calls
            begin
                out1 = in1 & in2;
                out2 = in2 | in1;
            end
            default:
            begin
                out1 = in1 ^ in2;
                out2 = in2 + in1;
            end
        endcase
    end

    function int case_item(int n);    // the function is defined here
        int r;

        r = n - 1;

        return r;
    endfunction

endmodule

