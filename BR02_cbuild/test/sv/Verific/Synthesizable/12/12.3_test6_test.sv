
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows specifying data type for formal arguments
// of a function, if no data type is specified then the default data type logic is
// assumed. This design defines a function and does not specify the data type for its
// formal arguments. The type for the formal arguments should be logic - the default
// data type.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

int i;

always @(posedge clk)
begin
    for (i=0; i<width; i++)
    begin
        out1[i] = func1(in1[i], in2[width-1-i]);
        out2[i] = func1(in2[i], in1[width-1-i]);
    end
end

function automatic func1(input a, b);
begin
    logic g = 1'bz;
    return g;
end
endfunction

endmodule

