
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Values from a function can be returned by assigning the value to
// the name of the function or by using the return statement. The return statement should
// override the value assigned to the name of the function. This design defines a non-void
// function and returns something from it using the 'return' statement which overrides a
// previously assigned value to the name of the function. After the return statement
// there are some more statements that assign values to the name of function. This tests
// whether the value returned is the one through return statement.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = func1(func1(in1, in2), func1(in2, in1));
    out2 = func1(func1(in2, in1), func1(in1, in2));
end

function int func1(int a, b);
    int i, j;

    i = a * b;
    func1 = ~i;

    j = a ^ b;
    i = j & i;

    return ~i;

    j = i - j;
    func1 = ~j & '0;
endfunction

endmodule

