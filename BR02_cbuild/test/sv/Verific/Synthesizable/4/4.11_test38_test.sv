
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. Four data elements
// are declared of that type. Some of the data elements are manipulated inside an 
// always_comb block and they are used to set the value of the output port.

// If 'NO_SIM_HANG' is defined then simulation completes, otherwise it hangs. It can
// be defined by the next line, or by using command line option of the simulator used.

//`define    NO_SIM_HANG

module test(clk, i1, i2, i3, i4, o1, o2);

parameter WIDTH = 16;

typedef bit [0:WIDTH -1] atype;
typedef logic [0:WIDTH -1] btype;

typedef struct packed {
    atype a;
    atype b;
    btype c;
    btype d;
} ctype;

input clk;
input signed [0:WIDTH -1] i1, i2;
input [0:WIDTH -1] i3, i4;
output reg signed [0:WIDTH -1] o1, o2;

reg signed [0:WIDTH -1] t1, t2;  
reg [0:WIDTH -1] t3, t4;   
reg [0:WIDTH -1] t5, t6;  
reg [0:WIDTH -1] t7, t8;    

ctype c1, c2, c3, c4;

always_comb
begin
        c1 = {i1, i2, i3, i4};
        t1 = i1 | i2;
        t2 = ((t1)?(~^i3 + ~&i4):(&i2 - ~|i1));
        c2 = {i1, i2, t1, t2};
end

always @(posedge clk)
begin
        if (t1 | t2)
        begin
                t3 = { i1[0:WIDTH/2], { WIDTH/4 { 1'b1 } } };
                t4 = i3 << WIDTH/2 + t1 - i2 <<< 2;
                t5 = i4 & ~t1;
                t6 = i4 | t5 ^ i3 - 15'b1;
                c3 = {t3, ~t4, t5, ~t6};
                c4 = {i3, ~t4, i4, ~t6};
        end
        else
        begin
                t3 = (t1==t2)?({ WIDTH { 1'b1 } } & i2):(i1 | i3);
                t4 = i4 | i3 - t3;
                t5 = i2 + t4 >>> 3;
                t6 = i1 <<< 2 - t1 + t5;
                c3 = {t3, t4, ++t5, t6};
                c4 = {i3, t4, i4, --t6};
        end
end

always_comb
begin
`ifdef NO_SIM_HANG
   $display("2 always_comb");
`endif
        t7 = -t5 +t6 + c4 - c3 + 2*c2;
        t8 = +t6 -t5 - c3 + c4 -c2;

        o1 = t7 + t6 + c4;
        o2 = t8 | t5 - c3;
end

endmodule

