
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enum variable. The literals defined
// are then used to define packed range of a data element, as the value of the enum
// literals are treated as constants.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    enum { A, B = 7, C } enum_t;

    logic [B:A] temp;

    always@(posedge clk)
    begin
        out1 = '0;
        out2 = '0;
        if (C == $length(temp, 1))
        begin
            out1 = in1 ^ in2;
            out2 = in2 + in1;
        end
    end

endmodule

