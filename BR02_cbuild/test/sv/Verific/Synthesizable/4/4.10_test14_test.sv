
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enum variable. The literals defined
// are then used to define range of another enum variable. Enum declaration can
// have range specified. This design checks that. The value of the enum literals
// are treated as constants.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    enum { A, B = 3, C } enum_range_t;
    enum [A:C-1] { L = 4'b0, M = 4'b1, N } enum_t;

    always@(posedge clk)
    begin
        out1 = '0;
        out2 = '0;
        if ((L == B-3) && (1'b1 == N-M))
        begin
            out1 = in1 ^ in2;
            out2 = in2 + in1;
        end
    end

endmodule

