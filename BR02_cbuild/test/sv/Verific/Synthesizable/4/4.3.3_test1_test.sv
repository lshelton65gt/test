
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing signedness of new data type longint.

module test(input longint signed mod_long1, 
           input longint signed mod_long2, 
           output bit comp1, comp2, 
           output longint signed mod_long3);

always@(*)
begin
    comp1 = (mod_long1 > mod_long2);
    comp2 = (mod_long1 <= mod_long2);

    mod_long3 = mod_long1 * mod_long2;
end

endmodule

