
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines global type. It has two modules. Both 
// of the modules defines data element of that type. The top module defines a task
// that is called with data element of that type. The bottom module is instantiated
// with that type of data elements as argument.

typedef struct packed {
    logic [3:0] a;
    logic [3:0] b;
} dtype;

module test (clk, i1, i2, i3, i4, o1, o2);
input clk;
input signed [3:0] i1, i2, i3, i4;
output signed [7:0] o1;
output signed [7:0] o2;

supply1 s1;
supply0 s0;

dtype d1 = '0;
dtype d2 = '0;

task automatic task1(input signed [3:0] in, output signed [7:0] out);
int i;
begin
        out = s0 + s1 - in;
        if (s1 >= s0+1)
        begin
                for (i=0; i<8; i=i+1)
                begin
                        out[i] = in[((i>3)?(i-4):i)]-s0;
                end
        end
end
endtask

always @(posedge clk)
begin
        if ((i1 & i2) > 4'b0100)
        begin
                d1.a = i3 | i1-s1;
                d1.b = i2 & i1;
                task1(i3+0 << 4 + i4-s0, d2);
        end
        else
        begin
                task1(i2 + i3 & i1 & s1, d1);
                d2.a = (i1[0] << 4) + (i1[1] << 4) + (i1[2] << 4) + (i1[3] << 4);
                d2.b = {s0+i3[0], i3[1], i3[2], i3[3] };
        end
end

bottom b1(clk, i1, d1, o1);
bottom b2(clk, i2, d2, o2);

endmodule

module bottom(clk, i1, i2, o1);
input clk;
input signed [3:0] i1;
input dtype i2;
output signed [7:0] o1;

dtype d;
int t;

always @(posedge clk)
begin
    d = i2;

    if(d.a >d.b)
    begin
        t = d.a;
        d.a = d.b;
        d.b = t;
    end
    else
    begin
        d.b = d.a ^ i1;
    end
end

assign o1 = d;

endmodule

