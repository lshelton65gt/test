
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test the default data type of enumeration.

module test (output int out, input int in1, in2, 
                 output reg com_res) ;

    enum {add, sub, mul, div} op ;

    int i = 0 ;

    always @(in1, in2, i)
    begin
        case (i)
            0 : op = add ;
            1 : op = sub ;
            2 : op = mul ;
            3 : op = div ;
        endcase

        i++ ;
        if(i > 3)
            i = 0 ;        
        case (op)
            add : out = in1 + in2 ;
            sub : out = in1 - in2 ;
            mul : out = in1 * in2 ;
            div : out = in1 / in2 ;
        endcase

        com_res = $bits(sub) == $bits(i) ;
    end

endmodule

