
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Return type of a function can be of void type to indicate
// that it does not return anything. This design defines a function that returns
// something but it does not specify the return type or width. It stores the returned
// value in a data element.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    always@(posedge clk)
    begin
        out2 = fn(in1^in2, out1);
    end

    function fn(input logic [width-1:0] a, output [width-1:0] b);
        b = (a>width) ? a : a<<1;
        return b;
    endfunction

endmodule

