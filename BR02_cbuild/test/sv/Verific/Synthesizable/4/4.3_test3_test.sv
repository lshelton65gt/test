
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test the default signed property of byte data type.

module test(output out1, out2, out3, out4) ;

    byte ch1, ch2 ;
    byte unsigned ch3, ch4 ;

    assign ch1 = -12/3,
           ch2 = -2 * 3,
           out1 = ch1 < 0,
           out2 = ch2 < 0,
           ch3 = -12/3,
           ch4 = -2 * 3,
           out3 = ch3 < 0,
           out4 = ch4 < 0;

endmodule

