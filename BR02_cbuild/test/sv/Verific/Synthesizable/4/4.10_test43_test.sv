
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses escaped identifiers to perform arithmetic
// bit-wise and logical operations. It also uses a function which returns an
// enumerated type.

module test(clk, i1, i2, o1);

parameter IN_WIDTH = 8;
parameter OUT_WIDTH = 2*IN_WIDTH;

input clk;
input signed [IN_WIDTH-1:0] i1, i2;
output reg [OUT_WIDTH-1:0] o1;

reg [IN_WIDTH-1:-IN_WIDTH] \r+1 , \r-2 , \r*3 ;
typedef enum {eight=IN_WIDTH, sixteen=OUT_WIDTH} e_type;

function e_type fn(int x);
    if (x==IN_WIDTH)
        return (eight);
    else
        return (sixteen);
endfunction

always @(posedge clk)
begin
        \r+1 = { i1, i2 };
        \r-2 = i1 << fn(IN_WIDTH);
        \r-2 = \r-2 + i2;
        \r*3 = i1 << fn(IN_WIDTH) + i2;

        if (\r+1 != \r-2 )
        begin
                o1 = { { (fn(IN_WIDTH)/2) { 1'b0 } }, i1[IN_WIDTH/2-1:0], i2[IN_WIDTH-1:IN_WIDTH/2], { fn(IN_WIDTH)/2 { 1'b1 } } };
        end
        else
        begin
                o1 = (\r+1 << IN_WIDTH) + (\r-2 >> IN_WIDTH);
        end

        if (\r-2 ^~ \r*3 )
        begin
                o1 = (\r-2 + \r*3 );
        end
        else
        begin
                o1 = (\r-2 - \r*3 );
        end
end

endmodule

