
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Assigning a value to string data type in sequential
// area and select characters from it.

module test(in, seq_str, sel_str) ;
  output string seq_str, sel_str ;
  input string in ;

   always @(*) 
   begin
       seq_str = in ; // Assigned in sequential area
       sel_str = seq_str.substr(2,5) ; // Select characters 
   end

endmodule

