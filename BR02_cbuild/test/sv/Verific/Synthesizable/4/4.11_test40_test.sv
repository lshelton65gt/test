
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure data-type consisting of two
// fields, then it sorts an unpacked array of this type. The sorting is performed
// inside a task. Whenever a condition is satisfied the control returns from the task.

module test (clk, reset, i1, i2, i3, i4, i5, i6, i7, i8, o1, o2, o3, o4, o5, o6, o7, o8);
parameter WIDTH=8;

typedef struct {
    logic [0:WIDTH-1] key;
    logic [0:WIDTH-1] id;
} k_type;

input clk, reset ;
input [0:WIDTH-1] i1, i2, i3, i4, i5, i6, i7, i8;
output reg [0:WIDTH-1] o1, o2, o3, o4, o5, o6, o7, o8;
k_type arr [7:0];

task sort(bit onKey);
bit swapped;
if(onKey)
begin    
    for (int i=0; i<8; i++)
    begin
            for (int j=0; j<8-i-1; j++)
            begin
                    if (arr[j].key > arr[j+1].key)
                    begin
                            arr[j].key = arr[j].key ^ arr[j+1].key;
                            arr[j+1].key = arr[j].key ^ arr[j+1].key;
                            arr[j].key = arr[j].key ^ arr[j+1].key;
                            swapped = 1;
                    end
            end
            if ( swapped!= 1)
                return;
            swapped = 0;
    end
end
else
begin
    for (int i=0; i<8; i++)
    begin
            for (int j=0; j<8-i-1; j++)
            begin
                    if (arr[j].id > arr[j+1].id)
                    begin
                            arr[j].id = arr[j].id ^ arr[j+1].id;
                            arr[j+1].id = arr[j].id ^ arr[j+1].id;
                            arr[j].id = arr[j].id ^ arr[j+1].id;
                            swapped = 1;
                    end
            end
            if ( swapped!= 1)
                return;
            swapped = 0;
    end
end
endtask

always @(posedge clk or negedge reset)
begin
        arr[0].key = i1;
        arr[0].id  = 0;
        arr[1].key = i2;
        arr[0].id  = 1;
        arr[2].key = i3;
        arr[0].id  = 2;
        arr[3].key = i4;
        arr[0].id  = 3;
        arr[4].key = i5;
        arr[0].id  = 4;
        arr[5].key = i6;
        arr[0].id  = 5;
        arr[6].key = i7;
        arr[0].id  = 6;
        arr[7].key = i8;
        arr[0].id  = 7;
        
        if(!reset) sort(1); else sort(0);

        o1 = arr[0].key;
        o2 = arr[1].key;
        o3 = arr[2].key;
        o4 = arr[3].key;
        o5 = arr[4].key;
        o6 = arr[5].key;
        o7 = arr[6].key;
        o8 = arr[7].key;
end

endmodule

