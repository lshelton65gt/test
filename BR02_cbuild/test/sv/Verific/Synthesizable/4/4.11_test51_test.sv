
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules. Both of them defines a 
// comparable structure type. The bottom level module uses this as the type of
// its output port. The other module instantiates it with elements of that type
// connected to the output port of the instantiation. These data elements are
// used to set the output port of the current module.

module test #(parameter width = 8)(input clk, async_reset, sync_reset, output [width-1:0] out);

typedef struct packed {
    logic [width-1:0] a;
    logic [width-1:0] b;
} dtype;

dtype dout1, dout2;

test1 #(.WIDTH(width)) m1(.clk(clk), .p(async_reset), .reset(sync_reset), .enable(1'b1), .i1(8'b11001), .o1(dout1));
test1 #(.WIDTH(4)) m2(.clk(clk), .p(~async_reset), .reset(~sync_reset), .enable(1'b1), .i1(4'b1001), .o1(dout2[7:0]));

assign out = (dout1 == dout2) ? dout1 : dout2;

endmodule

module test1 (clk, p, reset, enable, i1, o1);

parameter WIDTH=4;

typedef struct packed {
    logic [WIDTH-1:0] a;
    logic [WIDTH-1:0] b;
} dtype;

input clk, p;
input reset, enable;
input [WIDTH-1:0] i1;
output dtype o1;

always @ (posedge clk,negedge p, posedge reset, negedge enable)
begin
    if(!p)
        o1 <= {2*WIDTH{ 1'b0 }};
    else
    begin
        if(reset)
            o1 <= {2*WIDTH{ 1'b0 }};
        else
        begin
            if(!enable)
            begin
                if(o1 ^ i1)
                    o1 <= {2*WIDTH{ 1'b0 }};
                else
                    o1 <= (1'b1 | i1);
            end
        end
    end
end

endmodule

