
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Enumeration can have a packed dimension and it can have signing
// property. This design defines an enum type having signed property.

module test #(parameter w = 4)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    enum logic signed [3:0] { INVALID_MONTH = 'x, JAN = 4'd1, FEB, MAR,
                   APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC } months;

    always@(posedge clk)
    begin
        if (JUL>0 && AUG<0)    // JUL = 7 = 4'b0111, AUG = 7+1 = 4'b1000 = -8
        begin
            out1 = in1 - in2;
            out2 = in2 ^ in1;
        end
    end

endmodule

