
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design type defines two new type having packed range.
// A structure type is defined having two members of the new types defined. A function
// is defined that returns value of the structure type. Inside an always_comb block
// the function is called. The return value is used to set the output port.

module test(i1, i2, i3, i4, o1, o2, o3, o4);

parameter WIDTH=8, OWIDTH=lg(WIDTH);

typedef logic [WIDTH -1:0] a_type;
typedef logic [OWIDTH -1:0] b_type;

typedef struct packed {
    a_type a;
    b_type b;
} dtype;

input [WIDTH-1:0] i1, i2, i3, i4;
output reg [OWIDTH-1:0] o1, o2, o3, o4;
dtype d1,d2,d3,d4;
a_type r1,r2;

function integer lg(input [31:0] x);
reg [31:0] a;
begin: blk
    a=1;
    if(x == 1)
        return 0;
    for(int i=1; i<32; i++)
    begin
        if(((a<<i)<x)&&((a<<(i+1))>=x))
            lg=i+1;
    end
end
endfunction

function dtype fn(int x, int y);
dtype ret;
    ret = {a_type'(x), b_type'(y)};
    ret &= '1; 
    return ret;
endfunction

always_comb
begin
    r1 = i1 + (i2[1:0]<<1) - 2'b01;
    r2 = i2 + (i1[1:0]<<1) - 2'b10;

    d1 = fn(i1,r1 + r2);
    d2 = fn(i2,r1 - r2);
    
    {o1,o2,o3,o4} = {d1.b, d2.b, d3.b, d4.b};
end

assign d3 = {i1 + i2, i3[2:0] + i4[2:0]};
assign d4 = {i1[2:0] - i2[7:5], i3 - i4};

endmodule

