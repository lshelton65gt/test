
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If any data type within a packed structure is masked, the
// whole structure is treated as masked. Any unmasked members are converted as if
// cast, i.e. an X will be read as 0 if it is a member of type bit. This design
// defines an enum inside a structure declaration without any explicit data type.
// This makes it 2-state int type.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef struct packed   // packed structure
    {
        bit [7:0] memb1;              // 2-state member element
        enum { A1, B1, C1 } memb2;    // 2-state member element
    } t_ps_st1;

    typedef struct packed   // packed structure
    {
        logic [7:0] memb1;            // 4-state member element
        enum { A2, B2, C2 } memb2;    // 2-state member element
    } t_ps_st2;

    t_ps_st1 st1;
    t_ps_st2 st2;

    always@(posedge clk)
    begin
        st1 = 'x;    // assign x to the whole element
        st2 = 'x;    // assign x to the whole element

        // now st1 === { 40 { 1'bx } }, st2 === { 40 { 1'bx } }

        if ((st1 === 'x) && (st2 === 'x))
        begin
            out1 = in1 - in2;
            out2 = in2 - in1;
        end
        else
        begin
            out1 = '0;
            out2 = '0;
        end
    end

endmodule

