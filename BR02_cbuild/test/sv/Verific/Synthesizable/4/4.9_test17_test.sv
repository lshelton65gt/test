
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules. One module is instantiated
// from the other module. The instantiated module defines a type having both packed
// and unpacked range. The type of the input port of the module is of that type.
// While instantiating we connect the port with concatenated data values.

module test(clk, i1, i2, i3, i4, o1, o2, o3, o4) ;

    parameter WIDTH = 16 ;
    parameter WIDTH1 = 4 ;

    input clk ;
    input signed [0:WIDTH-1] i1, i2, i3, i4 ;
    output signed [0:WIDTH-1] o1, o2, o3, o4 ;

    reg signed [0:WIDTH-1] t1, t2, t3, t4 ;

    always @(posedge clk)
    begin
        t1 = i1 ^ i2 ;
        t2 = i3 | i4 ;
        t3 = i2 - i1 ;
        t4 = i4 & i2 ;
    end

    mod2 #(.WIDTH(WIDTH),.WIDTH1(WIDTH1)) m2 ('{t1,t2,t3,t4}, o1) ;
    mod2 #(.WIDTH(WIDTH),.WIDTH1(WIDTH1)) m3 ('{t2,t3,t4,t1}, o2) ;
    mod2 #(.WIDTH(WIDTH),.WIDTH1(WIDTH1)) m4 ('{t3,t4,t1,t2}, o3) ;
    mod2 #(.WIDTH(WIDTH),.WIDTH1(WIDTH1)) m5 ('{t4,t1,t2,t3}, o4) ;

endmodule

module mod2(in, out) ;

    parameter WIDTH = 16 ;
    parameter WIDTH1 = 16 ;
    typedef bit [WIDTH -1:0] dtype [WIDTH1 -1:0] ;

    input dtype in ;
    output reg [0:WIDTH-1] out ;
    bit [WIDTH -1:0] r1 ;
    int l ;

    always @(*)
    begin
        l = 0 ;
        out = 'b0 ;
        for(int i=0 ; i<WIDTH1 ; i++)
        begin
            r1=in[i] ;
            for(int j=0 ; j<WIDTH ; j++)
            begin
                if (1'b1 == r1[j])
                begin
                    out[l] = 1'b1 ;
                    l++ ;
                end
            end
        end
    end

endmodule

