
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a new structure type. Three data elements
// are declared of that type. Expressions of these data elements are used as case
// items in the always block.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter WIDTH = 16;

typedef struct packed {
    bit [0:WIDTH -1] a;
    logic [0:WIDTH -1] b;
} dtype;

input clk;
input signed [0:WIDTH-1] i1, i2, i3, i4;
output reg signed [0:WIDTH-1] o1, o2;

reg signed [0:WIDTH-1] t1, t2, t3, t4;
dtype d1, d2, d3;

always_comb
begin
        t1 = i1 & i2 >> 2;
        t2 = i3 | (i3 * i4);
        d1 = {t1, t2};
        t3 = (i1 > i2)?(i2 >> 2):(i3<<3);
        t4 = { WIDTH { (i3 == i4) } };
        d2 = {t3, t4};
        d3 = {t2, t1};
end

always @(posedge clk)
begin

        if (3'sb111 <= 2'sb11)
        begin
                o1 = t3 * t4;
                o2 = t3 >>> 2 | t4 <<< 3;
        end
        else
        begin
                case (2'b11)
                        {|d1[WIDTH-1:0], &d1[2*WIDTH -1:WIDTH]}:
                                begin
                                        o1 = t3 - t2 + d1;
                                        o2 = -t1 + t4;
                                end
                        {^d2[WIDTH-1:0],~&d2[2*WIDTH -1:WIDTH]}:
                                begin
                                        o1 = t1 + t4;
                                        o2 = t2 | t3;
                                end
                        {~&d3[WIDTH-1:0],~^d3[2*WIDTH -1:WIDTH]}:
                                begin
                                        o1 = ^t1 & ~t2;
                                        o2 = +t4 + -t3;
                                end
                        default:
                                begin
                                        o1 = 'bx;
                                        o2 = 'bx;
                                end
                endcase
        end
end

endmodule

