
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Use integer type as the data type of enum declaration.

module test(output reg Q, input S, R, input clk) ;

    enum integer { 
        UNDEF= 'bx, 
        PREV='b00, 
        SET='b10, 
        RESET='b01
    } state;

    always @(posedge clk)
    begin
        if((S == 1) && (R == 0))
        begin
            Q = 1'b1 ;
            state = SET ;
        end
        else if((S == 0) && (R == 1))
        begin
            Q = 1'b0 ;
            state = RESET ;
        end
        else if((S == 1) && (R == 1))
        begin
            Q = 1'bx ;
            state = UNDEF ;
        end
        else
            state = PREV ;
    end
    
endmodule

