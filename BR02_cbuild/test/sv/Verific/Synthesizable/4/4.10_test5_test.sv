
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Use enum type variables to connect ports of modules.

typedef enum { ADD, SUB, MULT, DIV, POW } arith_op ;

module test(output int res, 
                 input int in1, in2,
                 input arith_op option) ;

    always @(in1, in2, option)
    begin
        case (option)
            ADD : res = in1 + in2 ;
            SUB : res = in1 - in2 ;
            MULT: res = in1 * in2 ;
            DIV : res = in1 / in2 ;
            POW : res = in1 ** in2 ;
        endcase
    end

endmodule

