
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a state machine like process, but it
// does not uses the usual case statement. Instead, it uses four if statement.
// Each if statement checks the state and depending on that it carries out some
// assignments and updates the state variable.

typedef enum [2:0] {ONE=1,TWO,THREE,FOUR} states;

module test#(parameter WIDTH = 4) (input clk,input [WIDTH-1:0] i1,output [WIDTH-1:0] o1);

typedef struct {
    reg [WIDTH -1:0] t1;
    reg [WIDTH -1:0] t2;
} tregs;

tregs tr1;
reg x;
integer val;
states s=ONE;

always @(posedge clk)
begin
    if(s==ONE)
    begin
        tr1.t1 = i1 >> 1;
        tr1.t1 <<= 1;
        x = i1 - tr1.t1;
        val = x;
        // s++;
        s = TWO;
    end
    if(s==TWO)
    begin
        tr1.t2 = tr1.t1 >> 2;
        tr1.t2 <<= 2;
        x = (tr1.t1 - tr1.t2) >> 1;
        val += x<<1;
        tr1.t1 = tr1.t2;
        // s++;
        s = THREE;
    end

    if(s==THREE)
    begin
        tr1.t2 = tr1.t1 >> 3;
        tr1.t2 <<= 3;
        x = (tr1.t1 - tr1.t2) >> 2;
        val += x<<2;
        tr1.t1 = tr1.t2;
        // s++;
        s = FOUR;
    end
    if(s==FOUR)
    begin
        tr1.t2 = tr1.t1 >> 4;
        tr1.t2 = tr1.t2 << 4;
        x = (tr1.t1 - tr1.t2) >> 3;
        val += x<<3;
        // s-=3;
        s = ONE;
    end
end

assign o1 = val;

endmodule

