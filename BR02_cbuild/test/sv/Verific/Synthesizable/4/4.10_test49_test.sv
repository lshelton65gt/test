
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enum type. It also defines a constant
// function. This function is called with module parameter. The return value is set
// as the range of the output port.

typedef enum [0:0] {false='0, true} bool;

module test (clk, i1, i2, i3, i4, o1);

parameter InWidth = 16;

input clk;
input [InWidth-1:0] i1, i2, i3, i4;
output reg signed [log2(InWidth):0] o1;

integer i;

always @(posedge clk)
begin
    o1 = 0;
    for (int i=0; i<InWidth; i++)
    begin
        if(fn1(i1,i2)==fn1(i3,i4))
        begin
            if (i1[i] == 1'b1)
            begin
                o1 += '1;
            end
        end
        else
        begin
            if (i2[i] == 1'b1)
            begin
                o1 += '1;
            end
        end
    end
end

function automatic int log2(input integer n);
    for (int i=0; i<32; i++)
    begin
        if (((1<<i) < n) && (n <= (1<<(i+1))))
        begin
            return i+1;
        end
    end
    return 1;
endfunction

function bool fn1(int x,int y);
    for(int i=0; i<32; i++)
    begin
        if(x[i]!=y[i])
            return false;
    end
    return true;
endfunction

endmodule

