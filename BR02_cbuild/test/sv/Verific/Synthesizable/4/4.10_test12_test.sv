
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The value of the unassigned first element of an enum literal
// should be 0 by default. This design defines an enum of type int and checks if
// the value of the unassigned first element is 0 or not.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    enum int { A, B = 7, C } enum_t;

    always@(posedge clk)
    begin
        enum_t = A;

        if (enum_t == 0)
        begin
            out1 = in1 - in2;
            out2 = in2 - in1;
        end
        else
        begin
            out1 = '0;
            out2 = '0;
        end
    end

endmodule

