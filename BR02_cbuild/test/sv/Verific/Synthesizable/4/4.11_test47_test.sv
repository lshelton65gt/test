
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. A task is defined
// that takes output argument of that type. From inside an always block it manipulates
// the data elements of that type and from an always_comb block it sets values of
// some local variables that are used to drive the output port.

typedef struct packed {
    logic [7:0] a;
    int b;
    logic [7:0] c;
} dtype;

module test (clk, i1, i2, i3, o1, o2);

input clk; 
input signed [0:7] i1, i2, i3; 
output signed [7:0] o1, o2;

reg signed [0:7] t1, t2, t3, t4;
dtype d1, d2;
int r1, r2;
byte r3, r4;

task automatic task1(input signed [7:0] i1, i2, output dtype out);
reg signed [0:7] t1, t2, t3;
begin
        t1 = i1 + i2 ^ i1;
        if (t1[7]>t1[0])
        begin
                t2 = { i1[7], t1[1:6], i2[0] };
                t3 = t1|t2;
        end
        else
        begin
                if (t1[0:3] == t1[4:7])
                begin
                        t2 = ~i1-i2^t1;
                        t3 = t3&t1;
                end
                else
                begin
                        t2 = -i2 - +i1 + +t1;
                        t3 = t1^t2;
                end
        end
        out = {t1, int'(~t3), t2};
end
endtask

always @(posedge clk)
begin
        t1 = i1<<4+i2>>4;
        t2 = {i2[0:3], i3[4:7]};
        t3 = {i3[4:7], i1[0:3]};
        t4 = i2<<4|i3>>4;

        task1(t1, t2, d1);
        task1(t3, t4, d2);
end

always_comb
begin
    r1 = int'(d1>>8);
    r2 = int'(d2>>8);
    r3 = d1[47:40];
    r4 = d2[47:40];
end

assign o1 = r1 ^ r3;
assign o2 = r2 ^ r4;

endmodule

