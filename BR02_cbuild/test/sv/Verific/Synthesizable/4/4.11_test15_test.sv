
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of packed signed structures.

module test(output reg one_hot, is_signed, input [10:0] data, input clk) ;

    typedef struct packed signed {
         bit start_bit;
         byte data ;
         bit [1:0] end_bits ;
    } data_struct ;

    data_struct in_data ;

    int i , j ;

    always @(posedge clk)
    begin
        in_data = data ;
        one_hot = 0 ;
        is_signed = 0 ;

        if(in_data < 0)
        begin
            is_signed = 1'b1 ;            
            if(in_data[1:0] == 2'b11)
            begin
                j = 0 ;
                for(i = 9; i >= 2; i = i -1)
                begin
                    if(in_data[i] == 1'b1)
                        j = j + 1;
                end
                if(j == 1)
                    one_hot = 1'b1 ;            
            end
        end
    end

endmodule

