
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Usual arithmatic and logic operations can be carried out
// on packed structures as a whole. Packed structures can be signed or unsigned,
// This design applies to various sign aware operators like >, < etc on signed
// packed structure type variables.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef struct packed signed    // this is packed signed structure
    {
        byte memb1;
        bit [7:0] memb2;
    } t_ps_st;

    t_ps_st st1, st2, st3, st4;

    always@(posedge clk)
    begin
        st1.memb1 = '1;
        st1.memb2 = '0;    // st1 is now 16'b1111111100000000
        st2 = ~st1;        // st2 is now 16'b0000000011111111

        if (st2 < st1)
        begin
            st3.memb1 = in1 ^ in2;
            st3.memb2 = ~(in1 | in2);
            st4.memb1 = in1 & in2;
            st4.memb2 = ~(in1 + in2);
        end
        else
        begin
            if (st2 > st1)
            begin
                st3.memb1 = ~in1;
                st3.memb2 = in2;
                st4.memb1 = ~st3.memb2;
                st4.memb2 = st3.memb1;
            end
            else
            begin
                st3.memb1 = in1 - in2;
                st3.memb2 = in1 * in2;
                st4 = ~st3;
            end
        end

        out1 = st2.memb2 ^ st4.memb1 | st3.memb2;
        out2 = st1 + st3;
    end

endmodule

