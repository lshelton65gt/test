
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design includes another design (test1.v) where the enum
// definition is given which is used over here in this design.

`include "test1.v"

module test #(parameter w = 4)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    months mnths;

    always@(posedge clk)
    begin
        out1 = days(months'(in1));
        out2 = days(months'(in2));
    end

    function months days(months m);
        months n;

        case(m)
            JAN:           n = FEB;
            FEB:           n = MAR;
            MAR:           n = APR;
            APR:           n = MAY;
            MAY:           n = JUN;
            JUN:           n = JUL;
            JUL:           n = AUG;
            AUG:           n = SEP;
            SEP:           n = OCT;
            OCT:           n = NOV;
            NOV:           n = DEC;
            DEC:           n = JAN;
            INVALID_MONTH: n = JAN;
            default:       n = JAN;
        endcase

        return n;
    endfunction

endmodule

