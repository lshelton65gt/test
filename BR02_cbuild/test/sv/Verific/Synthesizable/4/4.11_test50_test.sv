
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines packed structure type. This type is used 
// as the return type of a function. The function is called from an always_comb
// block and the return value are used to set some local variable of that type.
// These local data elements are then used to drive the output port.

typedef struct packed {
    logic [1:0] a;
    int b;
} dtype;

module test(i1, i2, i3, o1, o2, o3);
input [15:0] i1, i2;
input [1:0]i3;
output [15:0] o1; 
output o2, o3;
dtype d1, d2, d3;

function dtype f1(input [15:0] i1, i2,input i3);
reg o2, o3;
reg [15:0] o1;
    o2 = i1[1];
    o3 = i2[1];
    o1[0] = i3;
    for (int i=1; i<16; i++)
    begin
        o2 = o2 ^ i1[i];
        o3 = i2[i] | (o3 && i2[i]);
        o1[i] = i2[i] || (o1[i-1] ^ i1[i-1]);
    end
    return {o1, o2, o3};
endfunction

always_comb
begin
     d1 = f1(i1, i2, i3) ;
     d2 = {i1, i2, i3} | f1(i1^i2, i1 | i2, i3);
     d3 = d2 - d1;
end
assign {o1, o2, o3} = {2{d1, d3, d2}};

endmodule

