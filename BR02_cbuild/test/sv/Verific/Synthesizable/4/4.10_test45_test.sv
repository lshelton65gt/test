
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enumerated data type whose values
// are set using previous enum values. It also defines two functions which takes
// this enumerated data type as arguments. The first one takes the current value
// and returns the next value and the second one takes the current value and returns
// the previous value.

module test(clk, i1, i2, i3, i4, i5, i6, i7, i8,
                        o1, o2, o3, o4);

parameter WIDTH = 1, OUT_WIDTH = 128;
parameter ALT_WIDTH_1 = 31, ALT_WIDTH_2 = 32;
parameter ALT_WIDTH_3 = 33, ALT_WIDTH_4 = 63;
parameter ALT_WIDTH_5 = 64, ALT_WIDTH_6 = 65;
parameter ALT_WIDTH_7 = 128;

typedef enum {aw1=31,aw2=aw1+1,aw3=aw2+1,aw4=63,aw5=aw4+1,aw6=aw5+1,aw7=128} alt_widths;

input clk;
input signed [WIDTH-1:0] i1;
input [ALT_WIDTH_1-1:0] i2;
input signed [ALT_WIDTH_2-1:0] i3;
input [ALT_WIDTH_3-1:0] i4;
input [ALT_WIDTH_4-1:0] i5;
input signed [ALT_WIDTH_5-1:0] i6;
input [ALT_WIDTH_6-1:0] i7;
input signed [ALT_WIDTH_7-1:0] i8;

output reg [OUT_WIDTH-1:0] o1, o2, o3, o4;

reg signed [ALT_WIDTH_1-1:0] r1;
reg signed [ALT_WIDTH_2-1:0] r2;
reg [ALT_WIDTH_3-1:0] r3;
reg [ALT_WIDTH_4-1:0] r4;
reg signed [ALT_WIDTH_5-1:0] r5;
reg [ALT_WIDTH_6-1:0] r6;

function alt_widths next_p(alt_widths current);
    case (current)
        aw1: return aw2;
        aw2: return aw3;
        aw3: return aw4;
        aw4: return aw5;
        aw5: return aw6;
        aw6: return aw7;
        aw7: return aw1;
        default: return aw7;
    endcase
endfunction

function alt_widths prev_p(alt_widths current);
    case (current)
        aw1: return aw7;
        aw2: return aw1;
        aw3: return aw2;
        aw4: return aw3;
        aw5: return aw4;
        aw6: return aw5;
        aw7: return aw6;
        default: return aw1;
    endcase
endfunction

alt_widths p = aw1;

always @(posedge clk)
begin
        r1 = (i1 - i2)|p;
        r2 = (i2 + i3)|next_p(p);
        r3 = (i3 - i4)|next_p(next_p(p));
        r4 = (i4 + i5)|prev_p(p);
        r5 = (i6 - i7)|prev_p(prev_p(p));
        r6 = (i7 + i8)|prev_p(next_p(p));

        o1 = r1 + r2 - r3 + r4 - r5 + r6;

        r1 = i1 + i8;
        r2 = i2 - i7;
        r3 = i3 + i6;
        r4 = i4 - i5;
        p=next_p(p);
        o2 = (r1 - r2 + r3 - r4)&p;

        r1 = i1 - i5;
        r2 = i2 + i6;
        r3 = i3 - i7;
        r4 = i4 + i8;
        p=prev_p(prev_p(prev_p(prev_p(p))));
        o3 = (r1 + r2 + r3 + r4 + p) & p;

        p=prev_p(next_p(p) + prev_p(p));
        r1 = (i1 + i3)&p;
        r2 = i2 - i4;
        r3 = i3 + i5;
        r4 = i4 - i6;
        r5 = i5 + i7;
        r6 = i6 - i8;
        p=prev_p(0);
        o4 = (-r1 - r2 - r3 - r4 - r5 - r6)|p;
end

endmodule

