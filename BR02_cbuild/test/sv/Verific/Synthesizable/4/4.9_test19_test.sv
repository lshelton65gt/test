
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design type defines a new type with packed range using
// typedef. Four elements of this data type are defined. They are manipulated within
// an always_ff block and are used to set the value of the output port.

module test(clk, reset, i1, i2, i3, i4, o1, o2);

parameter WIDTH = 32;

typedef logic [0:WIDTH -1] atype;

input clk, reset;
input signed [0:WIDTH-1] i1, i2;
input signed [0:WIDTH-1] i3, i4;
output reg signed [0:WIDTH -1] o1, o2;

reg signed t1, t2;     
reg signed [0:7] t3;    
reg signed [0:15] t4;     
reg [0:31] t5;            
atype r1, r2, r3, r4;

always_ff @(posedge clk, negedge reset)
begin
    if (!reset)
    begin
        r1 = i1;
        r2 = i2 ^ i1;
        r3 = i3 ^ i2;
        r4 = i3 ^ i4;
    end
    
    t1 = ~|((i2+i3)*(i1-i4));
    t2 = ^{ |i3, ^i2, &i1, ~|i4, ~&i2, ~^i3, ~^i1 };
    
    case (2'd01)
        {t1,t2}: t3 = i1 & i2 + i3;
        {t2,t1}: t3 = i3 + +i1;
        {t1,t1}: t3 = { (i4 - i1) << 3, { 3 { 1'b1 } } };
        {t2,t2}: t3 = ((i2 * i1) >>> WIDTH) - { WIDTH { 1'b1 } };
        default: t3 = i3 + (i2 | i1);
    endcase
    
    t4 = i2 ^ t3 - {t1, t2} + r1;
    t5 = i1 | (i2 - i3) + r2;
    
    o1 = t4 * t1 + t5 + r3;
    o2 = t4 - t5 * t2 + r4;
end

endmodule

