
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that the bit pattern of a packed structure/union can
// be stored in bit type packed arrays and this can be restored back to original
// structure/union type variable.

typedef struct packed
{
    int i ;
    bit [7:0] bit_vec ;
    logic [1:0] byte_vec ;
} packed_st ;

typedef union packed
{
      bit [3:0][7:0] bit_vec ;
      int i ;
} packed_un ;

module test (output bit is_restored, input packed_st st,
                    input packed_un un) ;

    packed_st res_st ;
    packed_un res_un ;

    bit [$bits(res_st) -1 :0] eq_st ;
    bit [$bits(res_un) -1 :0] eq_un ;

    always @(st, un)
    begin
        eq_st = st ;
        res_st = eq_st ;

        eq_un = un;
        res_un = eq_un ;

        is_restored = 0 ;
        if((st.i == res_st.i) && (st.bit_vec == res_st.bit_vec) &&
           (st.byte_vec === res_st.byte_vec) && (un.i == res_un.i))
            is_restored = 1'b1 ;
    end

endmodule

