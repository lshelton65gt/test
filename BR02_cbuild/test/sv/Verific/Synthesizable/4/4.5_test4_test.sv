
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of void data type.

module test(output int out, input int in) ;

    function void incr(output int out, input int in) ;
        out = in + 1;
    endfunction

    always @(in)
        incr(out, in) ;

endmodule

