
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function which takes three argument,
// two of which are packed structure type. The function uses <= and >= to compare
// their values. This function is called from an always_comb block.

module test(i1, i2, o1, o2);

parameter WIDTH = 8;
typedef struct packed {
    bit [3:0] x1;
    bit [3:0] x2;
} ps;

input [WIDTH-1:0] i1, i2;
output reg o1, o2;

function bit fn_cmp(ps x, ps y, bit val);
    if(val)
        return (x>=y);
    else
        return (x<=y);
endfunction

always_comb
begin
        if (i1 > i2)
        begin
                o1 = (i1 == i2);
                o2 = fn_cmp(i1,i2,0);
        end
        else
        begin
                if (i1 == i2)
                begin
                        if (fn_cmp(i1, i2, 1))
                        begin
                                o1 = i1 - i2;
                                o2 = i2 - i1;
                        end
                        else
                        begin
                                o1 = & i1;
                                o2 = | i2;
                        end
                end
                else
                begin
                        o1 = (fn_cmp(i1, i2, 1));
                        o2 = (i1 == i2);
                end
        end
end

endmodule

