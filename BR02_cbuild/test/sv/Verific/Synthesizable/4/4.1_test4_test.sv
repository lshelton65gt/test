
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of logic value to bit value conversion. Logic value x,
// X, z, Z converts to 0.

module test(output bit [7:0] bit1, bit2,
            output logic [7:0] logic1, logic2) ;


    assign logic1 = 8'bx11z1100,
           bit1 = logic1,
           logic2 = 8'b110X00ZZ,
           bit2 = logic2;

endmodule

