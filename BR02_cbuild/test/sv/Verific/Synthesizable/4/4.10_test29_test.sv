
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enumeration and declares some data
// elements of that type having packed range and manipulates those data elements.

typedef enum bit { FALSE, TRUE } Boolean;

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    Boolean [0:3] b4;

    always@(posedge clk)
    begin
        b4 = { TRUE, FALSE, FALSE, TRUE };

        out1 = ((TRUE == b4[FALSE]) ? in1 ^ in2 : '0);
        out2 = ((b4[TRUE]<b4[FALSE]) ? in2 | in1 : '0);
    end

endmodule

