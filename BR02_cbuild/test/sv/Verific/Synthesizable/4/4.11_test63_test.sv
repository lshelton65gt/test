
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. One object is defined
// of that type. Inside an always_comb block that object is assigned a constant
// concatenated value. Another always_comb block sets the value of the output port
// from this data object.

typedef struct packed {
    logic [7:0] a;
    bit [7:0] b;
} d_type_t;

module test(input [7:0] i1, i2, i3,input i4, i5, output reg [7:0] o1);
d_type_t d1;

always_comb
begin
    d1 = {4'b0110, 4'(i1), 4'(i2), 4'(i3)};
end

always_comb
begin
    if(i4 ^ i5)
    begin
        if(d1.a & 8'd20)
            o1 = d1[15:2] ^ {i1,i2[7:2]};
        else
            o1 = d1 - 24;
    end
    else
    begin
        if( 4'(d1 >> 4) & 4'b0101)
            o1 = d1.a - d1.b;
        else
            o1 = d1.b + '1;
    end
end

endmodule

