
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enumeration. A constant function is
// also defined. This function takes one parameter of that enum type. Depending on
// the value of that parameter it returns a constant module parameter value. This
// function is called within an always block in a loop condition.

module test(i1, i2, o1);

parameter A_WIDTH = 4;
parameter B_WIDTH = 8;
parameter OUT_WIDTH = 16;
typedef enum {w1=A_WIDTH,w2=B_WIDTH,w3=OUT_WIDTH} p_width;

input [A_WIDTH-1:0] i1;
input [B_WIDTH-1:0] i2;
output [OUT_WIDTH-1:0] o1;

reg [OUT_WIDTH-1:0] val;
p_width pw1;

function [7:0] fn(p_width w);
    case (w)
       w1:fn=A_WIDTH;
       w2:fn=B_WIDTH;
       w3:fn=OUT_WIDTH;
    endcase
endfunction

always @(i1, i2)
begin
        val = 0;
        for(int i=0; i<fn(w1); i++)
        begin
                if (1 == i1[i])
                        val += (i2 << i);
        end
end

assign o1 = val;

endmodule

