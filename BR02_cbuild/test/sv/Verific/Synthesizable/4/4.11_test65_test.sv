
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two new types and two new packed structure
// types.  One of the packed structure type contains two state type data elements and
// the other has data members of four state type. Data elements of all these types
// are declared and they are assigned values inside an always_comb block. These
// values then used to drive the output ports inside an always block.

typedef logic [7:0] l_type_t;
typedef bit [7:0] b_type_t;

typedef struct packed {
    logic [3:0] a;
    logic [3:0] b;
} fourstate_padt_t;

typedef struct packed {
    bit [3:0] a;
    bit [3:0] b;
} twostate_padt_t;

module test(input clk, input [7:0] i1, i2, output reg [7:0] o1, o2);

l_type_t r1;
b_type_t r2;

fourstate_padt_t t1;
twostate_padt_t t2;

always_comb
begin
    r1 ^= i1 + i2;
    r2 ^= i1 + i2;
    t1 &= i1 - i2;
    t2 &= i1 - i2;
end

always @(posedge clk)
begin
    o1 = {^r1, ^r2, &r1, &r2, |r1, |r2, ~|r1, ~|r2};
    o2 = {^t1, ^t2, &t1, &t2, |t1, |t2, ~|t1, ~|t2};

end
    
endmodule

