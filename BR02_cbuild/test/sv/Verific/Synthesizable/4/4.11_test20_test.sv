
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Structure type variables can be assigned to other variables
// of same type as a whole. This design defines two structures, one is packed while
// the other is unpacked. Assignment is done between variables of similar types. 

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef struct packed     // this is packed
    {
        byte memb1;
        bit [7:0] memb2;
    } t_p_st;

    typedef struct packed signed    // this is packed and also signed
    {
        byte memb1;
        bit [7:0] memb2;
    } t_ps_st;

    typedef struct    // unpacked is default
    {
        byte memb1;
        bit [8:0] memb2;
    } t_u_st;

    t_p_st st1, st2;
    t_ps_st st3, st4;
    t_u_st st5, st6;

    always@(posedge clk)
    begin
        st1.memb1 = in1;
        st1.memb2 = in2;
        st3.memb1 = in1 ^ in2;
        st3.memb2 = in1 | in2;
        st5.memb1 = in1 & in2;
        st5.memb2 = in1 + in2;

        st2 = st1;    // structures can be assigned as a whole to another variable of same type
        st4 = st3;
        st6 = st5;

        out1 = st2.memb2 - st4.memb1 + st6.memb2;
        out2 = st6.memb1 | st4.memb2 ^ st2;        // structures can be used as a whole for packed stucture
    end

endmodule

