
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): It defines two structure types. A union type is defined whose
// two members are of the two structure type defined previously. Within a casex
// statement the union type variable is assigned different values depending on the
// two input port. The variable is connected to a port of instantiation of a module
// which also defines these types.

module test (clk, in1, in2, in3, out1);

parameter Width = 8;

typedef struct packed {
    logic [Width/2 -1:0] a;
    logic [Width/2 -1:0] b;
} a_type;

typedef struct packed {
    logic [Width/4 -1:0] a;
    logic [3*Width/4 -1:0] b;
} b_type;

typedef union packed {
    a_type x1;
    b_type x2;
} c_type;

input clk;
input [3:0] in1;
input [Width-1:0] in2, in3;
output [Width-1:0] out1;

reg [3:0] t;
c_type res;

always @(posedge clk)
begin
        t = in1 ^ in2;

        casex(t)
                4'b00xx: res = { { Width/2 { 1'b0 } }, (in2|in3) >> Width/2 };
                4'b0001: res = { { (3*Width)/4 { 1'b0 } }, (in2^in3) >> (3*Width)/4 };
                4'bxx11: res = { (in2&in3) >> Width/2, { Width/2 { 1'b0 } } };
                4'b1001: res = { { Width/4 { 1'b1 } }, (in2+in3) >> Width/2, { Width/4 { 1'b1 } } };
                default: res = { Width { 1'b0 } };
        endcase
end

bottom #(.Width(Width)) b1(clk, res, out1);

endmodule


module bottom(clk, i1, o1);
parameter Width = 8;
typedef struct packed {
    logic [Width/2 -1:0] a;
    logic [Width/2 -1:0] b;
} a_type;

typedef struct packed {
    logic [Width/4 -1:0] a;
    logic [3*Width/4 -1:0] b;
} b_type;

typedef union packed {
    a_type x1;
    b_type x2;
} c_type;

input clk;
input c_type i1;
output reg [Width -1:0] o1;
int r1,r2;

always @(posedge clk)
begin
    r1 = i1.x1 | i1.x2;
    r2 = r1 + i1.x1 + 8'(i1);
    o1 = r2 - r1;
end

endmodule

