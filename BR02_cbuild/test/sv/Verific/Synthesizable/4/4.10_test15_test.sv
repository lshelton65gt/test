
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Enum declaration can have a type. This design defines an
// enum with a logic data type. It has a packed range also.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    enum logic [3:0] { A, B = 3, C } enum_t;

    always@(posedge clk)
    begin
        out1 = '0;
        out2 = '0;
        if ((A+3 == B) && (1'b1 == C-B))
        begin
            out1 = in1 ^ in2;
            out2 = in2 + in1;
        end
    end

endmodule

