
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a enum type having range. A module is
// defined which has a port of that enum type. Depending on the value of that 
// port, output port gets different values. The output port then casted to the
// enum type and again passes through another case statement and its value gets
// modified.

typedef enum [1:0] {s0, s1, s2, s3} en_type_t;

module test(input clk, input en_type_t i1, input [1:0] i2, i3, output reg [2:0] o1);

always @(posedge clk)
begin
    o1 =  1 ;
    case (i3)
         s0:
          begin
                if(i1 ^ i2)
                begin
                end
                else
                    o1 =  0 ;
          end
         s1:
          begin
                if(i2 || i1)
                begin
                    if(i2 && i1)
                        o1 = i2 + i1 ;
                end
          end
         s2:
          begin
                if((i2 + i1) |  1 )
                begin
                    if((i2[1] & i1[1]) ^  0 )
                        o1 = o1 ~^  3'b001 ;
                    else
                        o1 = i2 & i1;
                end
          end
         s3: o1 = (i1 + i2)<<1;
         default:;
    endcase
    case (en_type_t'(o1))
         s0: o1 = 3'b010 ~^ o1;
         s1: o1 = o1 +  3'b010 ;
         s2: if(o1[1] >= i1[1])
                  o1 = !o1;
         s3: o1 = o1 &&  3'b010;
         default: o1 = i2 && (o1 < i1);
    endcase
end

endmodule

