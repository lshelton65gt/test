
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two types having packed range. These are
// used to define member elements in a structure type definition. This structure
// type and the other two data type are then used in the design in various places
// and type casting.

module test(clk, reset, i1, i2, o1, o2);

parameter WIDTH = 16;
typedef logic [0:WIDTH -1] l_type;
typedef bit [0:WIDTH -1] b_type;

typedef struct packed {
    l_type a;
    b_type b;
} d_type;

input clk, reset;
input signed [0:WIDTH-1] i1, i2;
output signed [0:WIDTH-1] o1, o2;

reg signed [0:WIDTH-1] t1, t2, t3, t4;
d_type d1,d2;

function d_type fn(l_type x, b_type y);
    int a,b;
    a = x | y;
    b = ~y & ~x;
    return {l_type'(a), b_type'(b)}; 
endfunction

always_ff  @(posedge clk,negedge reset)
begin
        if(!reset)
        begin
            t1 = i1 ^ i1;
            t2 = i1 & ~i1;
            d1 = fn(t1,t2);
            d2 = fn(i1&t1, i2&t2);
        end

        if (t1 < t2)
        begin
                for(int i=1; i<WIDTH; i=i+1)
                begin
                        t3[i] = i1[i-1] + i1[i];
                        t4[WIDTH-i] = i2[i] & i2[i-1] - d1[i] + d2[i];
                end
        end
        else
        begin
                if (t1 > t2)
                begin
                        t3[1:WIDTH-1] = (i1[1:WIDTH-1] + i2[1:WIDTH-1]) ^ d1[WIDTH-1:1]; 
                        t4[1:WIDTH-1] = (i2[0:WIDTH-2] - i1[0:WIDTH-2]) ^ d2[WIDTH-1:1];
                end
                else
                begin
                        t3 = i1 | t1 ^ i2 + l_type'(d1);
                        t4 = i2 + t2 | i1 + b_type'(d2);
                end
        end

        t3[0] = 1'b1;
        t4[0] = 1'b0;
end

assign o1 = t3 & t4;
assign o2 = t4 | t3;

endmodule

