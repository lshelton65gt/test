
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Part select on arrays makes a signed array unsigned. signed
// in casting interprets unsigned element as signed. This design compares (whole)
// part-selected signed casted array with the original array. Both of them should
// be of similar types - bench checks that.

module test (input clk,
             input signed [7:0] in1,
             output reg signed [0:7] out1, out2);

    reg [7:0] temp;

    always@(posedge clk)
    begin
        temp = in1[7:0];
        out1 = signed'(temp);        
        out2 = in1;
    end

endmodule

