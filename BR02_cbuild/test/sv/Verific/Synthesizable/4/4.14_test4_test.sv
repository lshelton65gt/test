
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Part select on arrays makes a signed array unsigned. Signed
// in casting interprets unsigned element as signed. This design declares a signed
// array and then casts another array inside a for-gen block to it. Both of them
// should be same.

module test#(parameter w = 8)
            (input clk,
             input signed [0:w-1] in1,
             output reg signed [w-1:0] out1);

    genvar i;

    generate

    for(i=w-1; i>=1; i--)
    begin: for_gen_i
        reg [0:i] temp;

        always@(posedge clk)
        begin
            temp = in1[0+:i];
            out1[i] = ((signed'(temp)) == (in1>>(w-i)));
        end
    end

    endgenerate

    always@(posedge clk)
        out1[0] = 1'b1;

endmodule

