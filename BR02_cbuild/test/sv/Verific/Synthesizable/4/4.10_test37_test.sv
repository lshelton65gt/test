
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Use constant function call as the value of enum members.

module test(output Q, input J, K, clk) ;

    parameter p = 1 ;

    function integer func(input integer in) ;

        func = in >> 1 ;
    endfunction

    enum 
    {
        J_K = func(2) ,
        D_TYPE = func(4) ,
        TOGGLE = func(8)
    } ff_type ;

    generate
        case (p)
            J_K : FF I(Q, J, K, clk) ;
            D_TYPE : FF I1(Q, J, ~J, clk) ;
            TOGGLE : FF I2(Q, J, J, clk) ;
        endcase
    endgenerate

endmodule
 
module FF(output reg Q, input J, K, clk) ; 

    reg prev_state ;

    always @(posedge clk)
    begin
        if((J == 1'b0) && (K == 1'b0))
            Q = prev_state ;
        else if((J == 1'b1) && (K == 1'b0))
            Q = 1'b1 ;
        else if((J == 1'b0) && (K == 1'b1))
            Q = 1'b0 ;
        else
            Q = ~prev_state ;
        prev_state = Q ;
    end

endmodule

