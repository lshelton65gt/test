
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Usual arithmatic and logic operations can be carried out
// on packed structures as a whole. Packed structures can be signed or unsigned,
// This design applies various sign aware operators like *, >=, <= etc on signed
// packed structure type variables.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef struct packed signed    // this is packed signed structure
    {
        byte memb1;
        bit [7:0] memb2;
    } t_ps_st;

    t_ps_st st1, st2, st3, st4;

    always@(posedge clk)
    begin
        st1.memb1 = '1;
        st1.memb2 = '0;    // st1 is now 16'b1111111100000000
        st2 = ~st1;        // st2 is now 16'b0000000011111111

        if (st2 <= st1)
        begin
            st3 = {in1, ~in2};
            st3 = {~in1, in2};
            st4 = st3 * st1;
        end

        if (st2 >= st1)
        begin
            st3 = {~in1, in2};
            st3 = {in1, ~in2};
            st4 = st3 * st2;
        end

        out1 = st2 ^ st3;
        out2 = st4 + st2;
    end

endmodule

