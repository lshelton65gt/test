
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. Two data elements 
// are declared of this type and they are manipulated inside an always block. These
// are used to connect the port of a module instance. The output port is connected 
// to the output port of the instantiated module.

typedef struct packed {
    logic [3:0] w1;
    logic [3:0] w2;
    logic [3:0] w3;
    logic [3:0] w4;
} d_type_t;

module test(input clk, reset, input [7:0] i1, i2, output [7:0] o1, o2);

d_type_t d1,d2;

always @(posedge clk)
begin
    d1.w1 = i1[7:4] + i2[7:4];
    d2.w1 = i1[3:0] - i2[3:0];

    d1.w2 = 4'(d1.w1>>2);
    d2.w2 = 4'(d1.w1>>1);

    d1.w3 = i1[7:4] | i2[3:0] - d1.w1;
    d2.w3 = i1[3:0] & i2[3:0] - d1.w2;

    d1.w4 = (d2.w1 - d1.w1)>>2;
    d2.w4 = (d1.w3 - d2.w3)>>2;
end

bottom b1(.clk(clk), .reset(reset), .i1(d1), .o1(o1[3:0])); 
bottom b2(.clk(clk), .reset(reset), .i1(~d1), .o1(o1[7:4])); 
bottom b3(.clk(clk), .reset(reset), .i1(d2), .o1(o2[3:0])); 
bottom b4(.clk(clk), .reset(reset), .i1(~d2), .o1(o2[7:4])); 

endmodule

module bottom(input clk, reset, input d_type_t i1, output reg [3:0] o1);
int i;
always @(posedge clk or negedge reset)
begin
    if(!reset)
        i = 1;
    else
    begin
        case (i++)
            1:
            begin
                o1 = {i1.w1[0], i1.w2[0], i1.w3[0], i1.w4[0]};
            end
            2:
            begin
                o1 = {~i1.w1[1], i1.w2[1], ~i1.w3[1], i1.w4[1]};
            end
            3:
            begin
                o1 = {i1.w1[2], ~i1.w2[2], i1.w3[2], ~i1.w4[2]};
            end
            4:
            begin
                o1 = {~i1.w1[3], ~i1.w2[3], ~i1.w3[3], ~i1.w4[3]};
            end
            default: i=1;
    endcase
    end

end

endmodule

