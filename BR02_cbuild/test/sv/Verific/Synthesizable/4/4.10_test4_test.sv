
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Use enumeration type variable as an argument of task call.

module test(output int res, input int in1, in2, in_op) ;

    typedef enum 
    {
        ADD = 2,
        SUB,
        MULT,
        DIV,
        UNDEF
    } arith_op;
    
    arith_op option ;

    task arith_task(input int in1, in2, 
                    input arith_op op,
                    output int result) ;

        case(op)
            ADD : result = in1 + in2 ;
            SUB : result = in1 - in2 ;
            MULT : result = in1 * in2 ;
            DIV : result = in1 / in2 ;
            UNDEF : result = 0 ; 
        endcase
    endtask

    always @(in1, in2, in_op)         
    begin
        case(in_op)
            2 : option = ADD ;
            3 : option = SUB ;
            4 : option = MULT ;
            5 : option = DIV ;
            default : option = UNDEF ;
        endcase

        arith_task(in1, in2, option, res) ;
    end

endmodule

