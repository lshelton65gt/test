
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Structures can be packed or unpacked; unpacked is the default
// when packed is not specified. Packed structures can be signed or unsigned; unsigned
// is the default when nothing is specified. This design defines two packed structures,
// one is signed and the other is unsigned (by default).

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef struct packed signed    // signed structure
    {
        byte memb1;
        bit [7:0] memb2;
    } t_ps_st;

    typedef struct packed    // unsigned is the default signing
    {
        byte memb1;
        bit [7:0] memb2;
    } t_pu_st;

    t_ps_st st1;
    t_pu_st st2;

    always@(posedge clk)
    begin
        st1.memb1 = '1;
        st1.memb2 = '0;
        st2.memb1 = '1;
        st2.memb2 = '0;

        if (st1 < 0)
            out1 = st1 + in1 | st2.memb1;    // Packed structure can also be used as a whole with arithmetic and logical operators
        else
            out1 = '0;

        if (st2 > 0)
            out2 = st2.memb2 | in2 ^ st1.memb1;
        else
            out2 = '0;
    end

endmodule

