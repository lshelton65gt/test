
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two new types having packed range. One
// of them is signed and the other is unsigned. Data elements of these types are
// declared and are used inside the always_comb block. These are then assigned to
// the output port.

typedef logic signed [3:0] sl_type_t;
typedef logic [3:0] ul_type_t;

module test(input [3:0] i1, i2, output signed [3:0] o1, o2);
sl_type_t r1;
ul_type_t r2;

always_comb
begin
    if(i1 > i2)
    begin
        r1 = i2 - i1;
        r2 = r1;
    end
    else
    begin
        r1 = i1 - i2;
        r2 = r1;
    end   

end

assign {o1, o2} = {r1, r2};

endmodule

