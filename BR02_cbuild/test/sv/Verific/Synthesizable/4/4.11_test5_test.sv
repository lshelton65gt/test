
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that packed signed structures can be assigned to other
// variables of same type as a whole.

module test(output reg result,
            input [10:0] data,
            input [7:0] mask) ;

    typedef struct packed signed
    {
         bit start_bit;
         byte ac_data ; 
         bit [1:0] end_bits ;
    } data_st ;

    data_st in_data ;

    function parity_check(input data_st in_data,
                          input [7:0] mask) ;

        bit [7:0] masked_source;
        bit tmp ;
        int i ;
        if((in_data < 0) && (in_data[1:0] == 2'b11))
        begin
            masked_source = in_data[9:2] && mask ;
            tmp = masked_source[0] ;
            for(i = 1; i < 8; i = i +1)
                tmp = tmp ^ masked_source[i] ;
            parity_check = tmp ;
        end
    endfunction        

    always @(data, mask)
    begin
        in_data = data ;
        result = parity_check(in_data, mask) ;        
    end

endmodule

