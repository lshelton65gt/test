
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that the signedness can be changed by type casting.

module test(output byte u_sum_out, s_sum_out,
            output bit s_less_out1, s_less_out2, u_less_out1, u_less_out2,
            input bit [7:0] in1, in2) ;

    always @(in1, in2)
    begin
        u_sum_out = in1 + in2 ;
        s_sum_out = signed'(in1) + signed'(in2) ;
        s_less_out1 = signed'(in1) < 0;
        s_less_out2 = signed'(in2) < 0;
        u_less_out1 = in1 < 0;
        u_less_out2 = in2 < 0;
    end

endmodule

