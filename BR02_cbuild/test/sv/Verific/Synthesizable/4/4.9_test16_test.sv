
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design typedefines a new type with packed range. This
// type is used as the return type of a function. The function uses type casts to
// return value of that type. The function is called from an always block to set
// the value of some local variables that are used to drive the output port.

module test(clk, in1, in2, out1, out2, out3, out4, out5);

parameter WIDTH = 16;
typedef logic signed [0:WIDTH -1] dtype;

input clk;
input signed [0:WIDTH-1] in1, in2;
output reg signed [0:WIDTH-1] out1, out2, out3, out4, out5;

reg signed [0:0] t1;
reg [0:1] t2;
reg signed [0:WIDTH-2] t3;
reg [0:WIDTH-1] t4;
reg signed [0:WIDTH] t5;

function dtype fn(logic [0:WIDTH -1] x, logic [0:WIDTH -1] y);
    int tmp = 0;
    for(int i=0; i<WIDTH; i++)
    begin
        tmp += x[i] & y[i];
    end
    return dtype'(tmp);
endfunction

always @(posedge clk)
begin
        t3 = fn(in1, in2) - 10'sb1110010110;
        t5 = fn(in1, t3) * 3'd6;
        t2 = fn(in2, 4'Hd) + 3'SO5;
        t1 = 2'so3 - 2'sD1;
        t4 = in2 & 9'D313;

        out1 = fn(t2 | t3, t1 &t4);
        out2 = t1 * t4;
        out3 = t5 - 5'SB01_00_0_;
        out4 = t2 + t1 - t3;
        out5 = t2 * 5'SD9;
end

endmodule

