
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Use enum type as return type of functions.

typedef enum
{
    AND_GATE,
    OR_GATE,
    XOR_GATE,
    XNOR_GATE,
    UNDEF
} gate_type ;
 
module test(output reg out, input in1, in2,
                 input string gateName ) ;

    function gate_type func(input string gateName) ;

        if(gateName.len()>3 && gateName[0:2] == "and")
            func = AND_GATE ;
        else if(gateName.len()>2 && gateName[1:0] == "or")
            func = OR_GATE ;
        else if(gateName.len()>3 && gateName[2:0] == "xor")
            func = XOR_GATE ;
        else if(gateName.len()>4 && gateName[3:0] == "xnor")
            func = XNOR_GATE ;
        else
            func = UNDEF ;
    endfunction

    gate_type type1 ;

    always @(*)
    begin
        type1 = func(gateName) ;
        case(type1)
           AND_GATE : out = in1 & in2 ;
           OR_GATE : out = in1 | in2 ;
           XOR_GATE : out = in1 ^ in2 ;
           XNOR_GATE : out = in1 ~^ in2 ;
           UNDEF : out = 1'bx ;
        endcase     
    end

endmodule

