
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohireged
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Some data types are signed by default and some are unsigned.
// reg data type is unsigned by default. This design checks this property of regs.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    enum reg [31:0] { 
        A = 32'b11111111_11111111_11111111_11111111,
        B,  // == A+1 == 0
        C = ((A<0) ? 0 : B+1)    // as reg is unsigned by default, so A will be positive and C will be B+1
    } enum1;

    always@(posedge clk)
    begin
        out1 = '0;
        out2 = '0;
        if (A == C)
        begin
            out1 = in1 & in2;
            out2 = in1 ^ in2;
        end
    end

endmodule

