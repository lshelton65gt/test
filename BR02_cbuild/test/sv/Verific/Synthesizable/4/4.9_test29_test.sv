
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): User defined types can be created using typedef. This design
// tests typedef declaration after their forward declaration and uses. The actual
// type is structure here.

module test() ;

    typedef my_int ;
    typedef my_struct;

    typedef struct {
        my_int mi[1:0] ;
        my_struct ms[1:0] ;
    } my_test ;

    typedef my_test test1 ;

    test1 st1 ;

    assign st1.mi = '{ 6, 7} ,
           st1.ms[1] = '{ 32'd9, '{ 1, 2, 3}} ,
           st1.ms[0] = '{ 32'd40,'{ 4, 7, 9}} ;

    typedef struct {
        bit [31:0] b ;
        my_int i[2:0] ;
    } my_new_struct ;

    typedef int my_int ;
    typedef my_new_struct my_struct ;

endmodule

