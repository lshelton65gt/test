
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines one packed and one unpacked structure 
// type. Another module is defined that uses these two types as input and output ports.
// The other module defines data elements of the type and connects them to the ports 
// of the instantiation. The output port is driven by the value of the data elements 
// connected to the output port of the instantiation.

typedef struct packed {
    logic [1:0] a ;
    logic [1:0] b ;
    logic [3:0] c ;
    logic [3:0] d ;
} psdt_t ;

typedef struct {
    bit a ;
    bit b ;
    bit c ;
    bit d ;
} usdt_t ;

module test #(parameter p=3)(input clk, input [1:0] i1, i2, input [3:0] i3, i4, output [4:0] o1) ;

psdt_t d1 ;
usdt_t ud1 ;

always @(posedge clk)
begin
    d1 = 0 ;
    for (int i=0 ; i<=10 ; i++)
    begin

        d1 = (i1 | d1) ^ i ;

        if ((p || i) && 7)
            d1 = d1^i2 ;
        else
        begin

            if (7-i) d1 =d1||2 ; else d1 =i1*d1 ;

        end
    end
end

bottom b1(clk, d1,ud1) ;

assign o1 = { ud1.a, ud1.b, ud1.c, ud1.d } ;

endmodule

module bottom #(parameter p=3)(input clk, input psdt_t i1, output usdt_t o1) ;
int i=0 ;
always @(posedge clk)
begin
   case(i)
       0: o1 = '{ 1'(i1.a), 1'(i1.b), 1'(i1.c), 1'(i1.d)} ;
       1: o1 = '{ 1'(i1.a>>2), 1'(i1.b>>2), 1'(i1.c>>2), 1'(i1.d>>2)} ;
       2: o1 = '{ 1'(i1.a>>3), 1'(i1.b>>3), 1'(i1.c>>3), 1'(i1.d>>3)} ;
       3: o1 = '{ 1'(i1.a>>4), 1'(i1.b>>4), 1'(i1.c>>4), 1'(i1.d>>4)} ;
       default: i-=4 ;
   endcase

   i++ ;
end

endmodule

