
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that unpacked union type variables can be assigned to
// other variables of same type as a whole.

typedef union
{
     int i ;
     real r ;
     byte b ;
     bit signed [7:0] c ;
} my_union ;

typedef enum
{
    INT, 
    REAL,
    BYTE,
    CHAR,
    UNDEF
} my_enum ;
 
module test #(parameter p = 5)
             (output my_union out, 
             input my_union in,
             input my_enum in_sel, out_sel,
             input int pos);

    typedef struct
    {
        my_union u ;
        my_enum type1 ;
    } data_type ;

    data_type data[p-1:0] ;

    int ac_pos ;

    always @ (*)
    begin
         ac_pos = pos ;
         if(pos >= p)
             ac_pos = 0 ;
         
         data[ac_pos].u = in;
         data[ac_pos].type1 = in_sel ;  
    end

    int i ;

    always @(out_sel)
    begin
        for(i = 0; i < p; i = i + 1)
        begin
            if(data[i].type1 == out_sel)
                out = data[i].u;                        
        end
    end

endmodule

