
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enumeration and declares some data
// elements of that type having unpacked range and manipulates those data elements.

typedef enum { INVALID_MONTH, JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC } months ;

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2) ;

    months m5 [3:7] ;

    always@(posedge clk)
    begin
        m5 = '{ INVALID_MONTH, JAN, APR, JUL, OCT } ;

        out1 = ((JAN == m5[APR]) ? in1 - in2 : '0) ;
        out2 = ((m5[JUN]<m5[JUL]) ? in2 - in1 : '0) ;
    end

endmodule

