
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. Data elements of this
// structure type are connected to port in an instantiation of a module. The module
// also defines a comparable structure type and uses that as the port type. It also
// defines automatic task and inside that uses a priority casex statement.

module test (clk, i1, i2, i3, o1, o2, o3, o4);

parameter PortWidth = 8;

typedef struct packed {
    logic signed [PortWidth-1:0] a;
    logic signed [PortWidth-1:0] b;
    logic signed [PortWidth-1:0] c;
    logic signed [PortWidth-1:0] d;
} dtype;

dtype din, dout;

input clk;
input signed [PortWidth-1:0] i1, i2, i3;
output signed [PortWidth-1:0] o1, o2, o3, o4;

reg signed [PortWidth-1:0] t1, t2, t3, t4;

task automatic task1(input signed [PortWidth-1:0] in, inout signed [PortWidth-1:0] io, output signed [PortWidth-1:0] out);
begin
        out = io | in;
        io = out ^ in;
        priority casex (in)      
                4'b01xx: out = in|4'b1010;
                4'b1x00: io  = in^4'b1100;
                4'bx0x1:
                begin
                        out = in&4'b1010;
                        io = in|~4'b0011;
                end
        endcase
end
endtask

always @(posedge clk)
begin
        t1 = i1 - i2;
        t2 = i3 - i1;

        task1(i3^i2, t1, t3);
        task1(i3+i1, t2, t4);
        din = {t1, t2, t3, t4};

end

bottom #(.PortWidth(PortWidth)) b1(clk, din, dout);

assign {o1, o2, o3, o4} = dout;

endmodule


module bottom(clk, i1, o1);

parameter PortWidth = 8;

typedef struct packed {
    logic signed [PortWidth-1:0] a;
    logic signed [PortWidth-1:0] b;
    logic signed [PortWidth-1:0] c;
    logic signed [PortWidth-1:0] d;
} dtype;

input clk;
input dtype i1;
output dtype o1;
dtype rd1;
int i=0;
logic signed [PortWidth-1:0] t;

always @(posedge clk)
begin
    rd1 = i1;
    if(i>9)
    begin
        t = rd1.d;
        rd1.d = rd1.c;
        rd1.c = rd1.b;
        rd1.b = rd1.a;
        rd1.a = t;
        i=0;
    end
    else
        i++;

end

assign o1 = rd1;

endmodule

