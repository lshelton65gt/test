
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design models a simple state machine. When the reset
// signal is low, it initializes itself. Otherwise it changes the state according
// to the previous state and sets the output accordingly.

module test(clk, reset, in, out);

parameter In_Width = 8, Out_Width = 8;

typedef enum [1:0] {State_0,State_1,State_2,State_3} STATES;

input clk, reset;
input [In_Width-1:0] in;
output reg [Out_Width-1:0] out;

STATES state;

always @(posedge clk or negedge reset)
begin
        if (reset == 0)
        begin
                state = State_0;
                out = 1'b0;
        end
        else
        begin
                case (state)
                        State_0:
                                begin
                                        state = State_1;
                                        out = in;
                                end
                        State_1:
                                begin
                                        state = State_2;
                                        out = out ^ in;
                                end
                        State_2:
                                begin
                                        state = State_3;
                                        out = out | in;
                                end
                        State_3:
                                begin
                                        state = State_0;
                                        out = out & in;
                                end
                endcase
        end
end

endmodule

