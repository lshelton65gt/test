
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Use enumeration type variable as an argument of function call.

module test(output int res, input int in1, in2, in_op) ;

    typedef enum {
        ADD = 2,
        SUB,
        MULT,
        DIV,
        UNDEF
    } arith_op;
    
    arith_op option ;

    function int arith_func(input int in1, in2, 
                            input arith_op op) ;

        case(op)
            ADD : arith_func = in1 + in2 ;
            SUB : arith_func = in1 - in2 ;
            MULT : arith_func = in1 * in2 ;
            DIV : arith_func = in1 / in2 ;
            UNDEF : arith_func = 0 ; 
        endcase
    endfunction

    always @(in1, in2, in_op)         
    begin
        case(in_op)
            2 : option = ADD ;
            3 : option = SUB ;
            4 : option = MULT ;
            5 : option = DIV ;
            default : option = UNDEF ;
        endcase

        res = arith_func(in1, in2, option) ;
    end

endmodule

