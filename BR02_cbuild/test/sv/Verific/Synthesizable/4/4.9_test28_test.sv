
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): User defined types can be created using typedef. This design
// defines user defined type in different places and mixed with forward definition
// actual definition of which are specified later.

module test (output int out1, out2, out3, input int in) ;

    typedef my_type ;

    function automatic my_type fact(input my_type n) ;
        if(n <= 1)
            fact = 1 ;
        else
            fact = n * fact(n -1) ;
    endfunction

    task inv(output my_type out, input my_type in) ;
        out = ~in ;
    endtask

    assign out1 = fact(in) ;

    always @(in)
        inv(out2, in) ;

    module shifter(out, in) ;
        parameter type p = real ;    
        output p out;    
        input p in ;
        assign out = in << 4 ;
    endmodule

    shifter #(.p(my_type)) I(out3, in) ;

    typedef int my_type ;

endmodule

