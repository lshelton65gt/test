
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design declares an user-defined data-type, this data
// type is used as return type of a function. This type is also used in casting
// variables of other types.

typedef bit signed [63:0] int_64;

module test(clk, i1, i2, i3, i4, o1, o2);

parameter WIDTH = 64;

input clk;
input signed [WIDTH-1:0] i1, i2;
input [WIDTH-1:0] i3, i4;
output reg signed [WIDTH-1:0] o1, o2;

logic l1, l2, l3, l4;
reg [62:0] r1;
reg signed [63:0] r2;
reg signed [64:0] r3;

function int_64 mix(int x, int y);
int_64 tmp;
    tmp = 0 ;
    for(int i=0;i<32;i++)
    begin
       tmp[i]   = x[i];
       tmp[i+1] = y[i]; 
    end
    return tmp;
endfunction

always @(posedge clk)
begin
        l1 = (i1 === i2);
        l2 = (i3 !== i4);
        l3 = (&i1 > ~&i3);
        l4 = (|i2 < ~|i4);

        if (!l1)
        begin
                r1 = mix(i1, i2);
                r2 = mix(i1, i3);
                r3 = mix(i2, i3);
        end
        else
        begin
                r1 = mix(i2, i1);
                r2 = mix(i3, i1);
                r3 = mix(i3, i2);
        end

        if (l1 && (~|i1))
        begin
                o1 = int_64'(r1) + int_64'(r3);
                o2 = int_64'(r2) - int_64'(r1);
        end
        else
        begin
                if (l3 || (~^ i3))
                begin
                        o1 = int_64'(r1) ^ int_64'(r2);
                end
                else
                begin
                        o2 = int_64'(r3) - int_64'(r2);
                end
        end

        if (l1 && (^i1))
        begin
                o1 = int_64'(r1) | int_64'(~r3);
                o2 = int_64'(r1) & int_64'(r2);
        end
        else
        begin
                if (l3 || (^~ i3))
                begin
                        o1 = int_64'(r2);
                end
                else
                begin
                        o2 = int_64'(r3);
                end
        end
end

endmodule

