
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog supports forward type definition. This design forward
// defines a type and declares a data element of that type. It then uses
// $bits system function on that data element to check whether it returns
// correct number of bits in the data element.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    typedef my_type;    // forward definition
    typedef my_type_in;    // forward definition

    my_type r1;

    bottom #(.p(w), .t(my_type)) bot1 (clk, in1^in2, out2);

    always@(posedge clk)
    begin
        if ($bits(r1) == (w<<1)+8)    // check the width of the data element
            out1 = in2 - in1;
        else
            out1 = 0;
    end

    // actual type defined here
    typedef struct
    {
        logic [w-1:0] memb1;  // w bit }
        byte memb2;           // 8 bit } total 2w+8 bit
        /* my_type_in is also forward defined */
        my_type_in memb3;     // w bit }
    } my_type;

    /* actual definition of my_type_in is here */
    typedef struct
    {
        reg [w/2-1:0] memb1;  // w/2 bit } total w bit
        bit [w/2-1:0] memb2;  // w/2 bit }
    } my_type_in;

endmodule

module bottom #(parameter p = 4, parameter type t = byte)
               (input clk,
                input [p-1:0] in,
                output reg [p-1:0] out);

    always@(posedge clk)
    begin
        if ($bits(t) == (p<<1)+8)
            out = ~in;
        else
            out = 0;
    end

endmodule

