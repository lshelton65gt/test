
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The values of the enum literals should be constant. This
// design defines an enumeration inside a for-generate loop and the genvar variable
// is assigned as the value of the enum literal.

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    generate
    genvar i;
        for(i=0; i<w; i++)
        begin: for_gen_i
            always@(posedge clk)
            begin
                enum { A = i, B } enum_t;

                out1[i] = in1[i] ^ in2[i];
                out2[i] = in2[i] + in1[i];
            end
        end
    endgenerate

endmodule

