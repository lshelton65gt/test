
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Finding the differences between int and longint data type.

module test (output int size1, size2,
             input int int1, input longint int2) ;

    assign size1 = $bits(int1) ,
           size2 = $bits(int2) ;

endmodule

