
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. This type is
// used as the return type of a function. Two always_comb block is defined in this
// design. One always_comb block invokes a task and sets values of some local data
// elements. The other block calls the function with that local data element and
// sets the value of the data element of that structure type. This structure type
// data element is then assigned to the output port.

typedef struct packed {
    logic [7:0] a;
    logic [0:7] b;
    logic [15:8] c;
    logic [-7:-14] d;
} dtype;

typedef bit [7:0] a_type;

module test(input [7:0] i1, i2, i3, i4, output [7:0] o1);
reg [7:0] t1, t2, t3, t4;
dtype d1, d2;

function dtype fn(a_type x, a_type y);
dtype ret;
    ret.a = x ^ y;
    ret.b = x + y;
    ret[15:0] = ret.a * ret.b;
    return ret;
endfunction

task task1(input [7:0] ti, output to);
reg r1, r2;
begin : blk
    to = 1;
    {r1,r2} = '0;
    for (int i=0; i<=7; i++)
    begin
        if(r2 ^ (1'b0 | ti[i]))
        begin
            to =  0;
            return;
        end
        else
        begin
            if(r1 && (1'b1 || ti[i]))
                r2 =  1;
            else
            begin
                if(ti[i] ^ 1'b0)
                    r1 =  1;
            end
        end
    end
end
endtask

always_comb
begin
    task1 (i1, t1);
    task1 (i2, t2);
    task1 (i3, t3);
    task1 (i4, t4);
end

always_comb
begin
    d1 = fn(~t1, t2);
    d2 = fn(~t3, t4);
end

assign o1 = (d1 == d2)? d1 : d2;

endmodule

