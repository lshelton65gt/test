
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. This type is used as
// the return type of a function. This function is called from an always block to
// manipulate some local data elements. These data elements are then used to set
// the value of the output port.

module test#(parameter WIDTH =4)(input clk, reset,input [WIDTH -1:0] i1, i2, i3, output reg signed [2 * WIDTH -1:0] o1);

typedef struct packed {
    logic [WIDTH -1:0] a;
    logic [WIDTH -1:0] b;
} d_type_t;

logic [2*WIDTH -1:0] r1;
bit b;
d_type_t d1, d2;

function d_type_t fn(logic [WIDTH -1:0] x, y, bit z);
d_type_t ret;
    if(z)
    begin
        ret.a = i1;
        ret.b = i2;
    end
    else
    begin
        ret.b = i1;
        ret.a = i2;
    end

    return ret;

endfunction

always @(posedge clk or negedge reset)
begin
    if(!reset)
    begin
        b = 0;
        o1 = 0 ;
    end
    else
    begin
        b++;
        for(int i=0; i<WIDTH; i++)
        begin
            r1 = fn(i1,i2,b);
            case(r1)
                '0: d1 = r1;
                '1: d1 = ~r1;
                default: d1 = {i1,~i2};
            endcase
        end
        o1 = d2 ^ d1;
    end
   

end

assign d2 = fn(i1,i2,clk);

endmodule

