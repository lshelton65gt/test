
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that arithmatic and logical operations can be carried
// out on packed structures.

typedef struct packed 
{
    int x ;
    int y ;
} u_point ;

typedef struct packed signed
{
    int x ;
    int y ;
} s_point ;

module test(output s_point s_sum_out, u_sum_out,
            output s_point s_or_out, u_or_out,
            output reg s_logand_out, u_logand_out,
            output reg s_less_out, u_less_out,
            input s_point s_in[1:0],
            input u_point u_in[1:0]) ;

    always @ *
    begin

        s_sum_out = s_in[1] + s_in[0] ;
        u_sum_out = u_in[1] + u_in[0] ;

        s_or_out = s_in[1] | s_in[0] ;
        u_or_out = u_in[1] | u_in[0] ;

        s_logand_out = s_in[1] && s_in[0] ;
        u_logand_out = u_in[1] && u_in[0] ;

        s_less_out = s_in[1] < s_in[0] ;
        u_less_out = u_in[1] < u_in[0] ;       
    end

endmodule

