
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enum. The value of the enum literals
// are set by calling a constant function. Two variables of the enum type is declared
// and initialized. This variables are then used in calculation as array indices.

module test(i1, i2, o1, o2, o3, o4);

parameter I1_WIDTH = 8;
parameter I2_WIDTH = 7;
parameter OUT_WIDTH = 16;


input signed [I1_WIDTH-1:0] i1, i2;
output signed [OUT_WIDTH-1:0] o1, o2;
output o3, o4;

reg signed [OUT_WIDTH-1:0] val, val1;
reg [OUT_WIDTH-1:0] t1, t2, t3;
reg c1, c2;

function [32:0] fn(int width);
    fn=width-1;
endfunction

typedef enum {v1=fn(I1_WIDTH),v2=fn(I2_WIDTH)} en_type;

en_type ent1;
en_type ent2;

always @(i1, i2)
begin
        ent1 = v1 ;
        ent2 = v2 ;
        val1 = (i1 * i2);
        t1 = (i1[ent1] ? (-i1) : i1);
        t2 = (i2[ent2] ? (-i2) : i2);
        t3 = (t1 * t2);
        val = ((i1[ent1] ^ i2[ent2]) ? (-t3) : t3);

        //signed comparison
        c1 = (i1 > i2);
        //unsigned comparison
        c2 = ({ ~i1[ent1], i1[ent1] } > { ~i2[ent2], i2[ent2] });
end

assign o1 = val;
assign o2 = val1;
assign o3 = c1;
assign o4 = c2;

endmodule

