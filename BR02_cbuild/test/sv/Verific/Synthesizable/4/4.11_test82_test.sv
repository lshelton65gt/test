
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The bit pattern of a packed union can be stored in bit type
// packed arrays; this can be restored back to original union type variable without
// loss of information. This design checks that.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef union packed    // packed union
    {
        byte memb1;
        bit [7:0] memb2;
    } t_ps_un;

    t_ps_un un1, un2;
    bit [7:0] s1, s2;

    always@(posedge clk)
    begin
        un1.memb1 = in1 ^ in2;    // assign values to the packed union type data elements
        un2.memb2 = in2 & in1;

        s1 = un1.memb2;    // store the value in packed array of bits
        s2 = un2.memb1;

        un1.memb1 = '0;    // manipulate the data elements
        un2.memb2 = '0;

        un1 = s1;          // restore back the values
        un2.memb1 = s2;


        if (un1 == (in1 ^ in2) && un2 == (in2 & in1))
        begin
            out1 = un1 + un2.memb1;
            out2 = un1.memb2 + un2;
        end
        else
        begin
            out1 = '0;
            out2 = '0;
        end
    end

endmodule

