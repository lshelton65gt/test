
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The values of the enum literals should be constant. This
// design defines an enumeration and assigns the value of a localparam to the enum
// literal. The value of the localparam is derived from another enum literal of a
// previous enum declaration.

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    enum { A = w+1, B } enum_int_t;

    localparam p = A-1;

    enum { C = p, D } enum_t;

    always@(posedge clk)
    begin
        out1='0; out2= (out1 = '0);
        if (C == w)
        begin
            out1 = in1 ^ in2;
            out2 = in2 | in1;
        end
    end

endmodule

