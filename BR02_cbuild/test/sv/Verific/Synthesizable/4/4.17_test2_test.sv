
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Attribute instance can be specified for interfaces. The default
// type of the attribute is bit with value 1. If the type of the
// value is different then (like using type casting) then the
// attribute specification takes that type. 


parameter logic [7:0] l = 7<<2;
(* first_attr, second_attr = l, third_attr = byte'(l) *) interface iface;
    logic [7:0] in1;
    logic [7:0] in2;
    logic [7:0] out1;
    logic [7:0] out2;

    modport std(input in1, in2, output out1, out2);
endinterface

module test(input clk, input [7:0] in1, in2, output [7:0] out1, out2);
        iface ifc();

        always@(posedge clk)
        begin
            ifc.in1 = in1 & in2;
            ifc.in2 = in2 - in1;
        end

        bot t_mod (ifc);

        assign out1 = ifc.out1 - ifc.out2,
               out2 = ifc.out1 + ifc.out2;

        module bot (iface ifc);

            always@(clk)
            begin
                ifc.out1 = ifc.in1 | ifc.in2;
                ifc.out2 = ifc.in2 ^ ifc.in1;
            end
        endmodule

endmodule

