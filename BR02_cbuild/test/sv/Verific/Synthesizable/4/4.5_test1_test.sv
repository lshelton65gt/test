
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Return type of a function can be of void type to indicate
// that it does not return anything. This design defines a function that does not
// return any value but it does not specify the return type either.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    always@(posedge clk)
    begin
        fn(in1^in2, out1);
        fn(in2-in1, out2);
    end

    function fn(input logic [width-1:0] a, output [width-1:0] b);
        b = (a>width) ? a : a<<1;
    endfunction

endmodule

