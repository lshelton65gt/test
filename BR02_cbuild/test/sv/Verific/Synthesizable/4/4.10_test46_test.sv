
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enum type. A data element is defined 
// of that enum type. A task is defined that operates on its input ports depending
// on the value of that enum type variable. It sets the values of some global variables
// which are used to set the output port value.

module test(clk, i1, i2, i3, i4, i5, i6, i7, i8, o1, o2, o3, o4);

parameter WIDTH = 1, OUT_WIDTH = 128;
parameter ALT_WIDTH_1 = 31, ALT_WIDTH_2 = 32;
parameter ALT_WIDTH_3 = 33, ALT_WIDTH_4 = 63;
parameter ALT_WIDTH_5 = 64, ALT_WIDTH_6 = 65;
parameter ALT_WIDTH_7 = 128;

typedef enum [1:0] {stage1, stage2, stage3, stage4} stages;

input clk;
input signed [WIDTH-1:0] i1;
input [ALT_WIDTH_1-1:0] i2;
input signed [ALT_WIDTH_2-1:0] i3;
input [ALT_WIDTH_3-1:0] i4;
input [ALT_WIDTH_4-1:0] i5;
input signed [ALT_WIDTH_5-1:0] i6;
input [ALT_WIDTH_6-1:0] i7;
input signed [ALT_WIDTH_7-1:0] i8;

output reg [OUT_WIDTH-1:0] o1, o2, o3, o4;
reg signed [ALT_WIDTH_1-1:0] r1;
reg signed [ALT_WIDTH_2-1:0] r2;
reg [ALT_WIDTH_3-1:0] r3;
reg [ALT_WIDTH_4-1:0] r4;
reg signed [ALT_WIDTH_5-1:0] r5;
reg [ALT_WIDTH_6-1:0] r6;

stages st=stage1;

always @(posedge clk)
begin

        mytask(i1, i2, i3, i4, i5, i6, i7, i8, st);
        o1 = r1 * r2 | r3 * r4 | r5 * r6;

        st = stage2;
        mytask(i1, i2, i3, i4, i5, i6, i7, i8, st);
        o2 = r1 | r2 * r3 | r4;

        st = stage3;
        mytask(i1, i2, i3, i4, i5, i6, i7, i8, st);
        o3 = r1 * r2 * r3 * r4;

        st = stage4;
        mytask(i1, i2, i3, i4, i5, i6, i7, i8, st);
        o4 = r1 | r2 | r3 | r4 | r5 | r6;

        st = stage1;
end

task mytask(int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8, stages s);
        r1 = i1 | i2;
        r2 = i2 * i3;
        r3 = i3 | i4;
        r4 = i4 * i5;
        r5 = i6 | i7;
        r6 = i7 * i8;

        if(s==stage1)
            return;
        r1 = i1 * i8;
        r2 = i2 | i7;
        r3 = i3 * i6;
        r4 = i4 | i5;

        if(s==stage2)
            return;
        r1 = i1 | i5;
        r2 = i2 * i6;
        r3 = i3 | i7;
        r4 = i4 * i8;

        if(s==stage3)
            return;

        r1 = i1 * i3;
        r2 = i2 | i4;
        r3 = i3 * i5;
        r4 = i4 | i6;
        r5 = i5 * i7;
        r6 = i6 | i8;
endtask

endmodule

