
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an always_comb to carry out some combinational
// procedural statements. Inside the always_comb block it uses type casting to unsigned
// using unsigned'(id) to make a signed element unsigned.

module test(i1, i2, i3, i4, o1, o2, o3);
parameter WIDTH = 8;

input [WIDTH-1:0] i1, i2, i3, i4;
output reg [WIDTH-1:0] o1, o2, o3;

reg signed t1, t2, t3;
reg [WIDTH-1:0] t4;

always_comb
begin
        t1 = &i1;
        t2 = ~& i1;
        t3 = ~t1;

        if (unsigned'(t2) >= unsigned'(t3))
        begin
                if (t2 == t3)
                        t4 = i2 | ~t2;
                else
                        t4 = i2 & t3;
        end
        else
                t4 = t1 ^ ~t2 ^ t3;

        o1 = i2 + t4;

        t1 |= | i2;
        t2 |= ~| i2;
        t3 |= ~t1;

        if (t2 <= t3)
                o2 = ~t2 + i3;
        else
                o2 = { (t3 <<< WIDTH-2), 2'b10 } ~^ i3;

        t1 &= ^ i3;
        t2 &= ~^ i3;
        t3 &= ~t1;

        if (unsigned'(t2) <= unsigned'(t3))
                o3 = '1^{ WIDTH { t3 } };
        else
        begin
                t4 = { { 2'b11 { t2 } }, t3, { (WIDTH-4) { (& 3'b101) } } };
                o3 = '1^(i1 + i2 - i3 - t4);
        end

end

endmodule

