
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two new types with packed range. Data
// elements with unpacked range is declared of this type. These data elements are
// manipulated and assigned values inside an always block. Inside another always_comb
// block they are used to set the value of the output port.

typedef logic [7:0] a_type;
typedef bit [7:0] b_type;

module test(input clk, reset, input [7:0] i1, i2, i3, output reg [7:0] o1, o2);

a_type r1[7:0];
b_type r2[7:0];
int i;

always @(posedge clk or negedge reset)
begin
    if(!reset)
        i = 0;
    else
    begin
        if(i++<4)
        begin
            r1[i] = i1;
            r2[i] = i2;
        end
        else
            i=0;
        if(i1[0])
        begin
            case (i2)
                'h033: r1[0]++;
                'h063: r1[1]++;
                default:r2[1]++;
            endcase
        end

        for (i = 4; i<8; i = i + 1)
            r2[i] = r1[i] & (r1[i] ^ { 7{ 1'b1 }});
    end
end

always_comb
begin
     o1 = (r1[0] + r1[1] + r1[2] + r1[3]) ^ (r1[4] + r1[5] + r1[6] + r1[7]);
     o2 = (r2[0] + r2[1] + r2[2] + r2[3]) ^ (r2[4] + r2[5] + r2[6] + r2[7]);
end

endmodule

