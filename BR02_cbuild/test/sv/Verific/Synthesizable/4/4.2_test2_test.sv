
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of longint and byte data types.

module test(int1, int2, byte1, byte2, longint_size, byte_size) ;

    output longint int1 [1:0] = '{ -(2 ** 34), 2 ** 45} ;
    output longint int2 = - (2 ** 63) ; 
    output byte byte1 [1:0] = '{ -128, 128} ;
    output byte byte2 = 2 ** 8 ;
    output int longint_size, byte_size ;

    assign longint_size = $bits(int2) ,
           byte_size  = $bits(byte2) ;
endmodule 

