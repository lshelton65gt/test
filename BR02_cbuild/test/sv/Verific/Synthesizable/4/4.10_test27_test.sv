
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enumeration and declares some data
// elements of that type. These data elements are then passed to a function.

typedef enum { INVALID_MONTH, JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC } months;

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1,
              output months out2);

    always@(posedge clk)
    begin
        if (in2 > NOV || in2 < JAN)
            out1 = JAN;
        else
            out1 = months'(in2+1);
    end

    bottom bot1(clk, 32'(in1), out2);

endmodule

module bottom (input clk, input [31:0] in1, output months out1);

    always@(posedge clk)
    begin
        case(in1)
            JAN: out1 = FEB;
            FEB: out1 = MAR;
            MAR: out1 = APR;
            APR: out1 = MAY;
            MAY: out1 = JUN;
            JUN: out1 = JUL;
            JUL: out1 = AUG;
            AUG: out1 = SEP;
            SEP: out1 = OCT;
            OCT: out1 = NOV;
            NOV: out1 = DEC;
            DEC: out1 = JAN;
            INVALID_MONTH: out1 = JAN;
            default: out1 = JAN;
        endcase
    end

endmodule

