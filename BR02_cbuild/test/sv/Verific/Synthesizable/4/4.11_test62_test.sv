
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a new structure type. One data element 
// is declared of this type. This is assigned a constant concatenated value inside 
// an always_comb block in reset state and different value in clock ticks.It is 
// then used inside an always block to set the value of the output port.

module test #(parameter WIDTH = 2)(input clk, reset, input [WIDTH -1:0] i1, i2, output reg [WIDTH -1:0] o1);

typedef struct packed {
    logic [1:0] a;
    logic [1:0] b;
    logic [1:0] c;
    logic [1:0] d;
} d_type_t;

d_type_t r1;
int i;

always @(posedge clk or negedge reset)
begin
    if(!reset)
        i = 0;
    else
    begin
        if(!i1[0])   
        begin
            if(i++ <4)
                o1 = 2'(r1>>(2*i));
            else
                i = 0;
        end
        else
            o1  = '1;
    end

end

always @(posedge clk or negedge reset)
begin
    if (!reset)
          r1 = {2'b00, 2'b01, 2'b10, 2'b11};
    else 
    if(i<2)
        r1++;
    else
        r1+=2;
end

endmodule

