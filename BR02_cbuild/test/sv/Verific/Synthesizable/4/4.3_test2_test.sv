
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test the default signed property of byte data type.

module test(output out1, out2, out3, out4) ;

    byte b1, b2 ;
    byte unsigned b3, b4 ;

    assign b1 = -12/3,
           b2 = -2 * 3,
           out1 = b1 < 0,
           out2 = b2 < 0,
           b3 = -12/3,
           b4 = -2 * 3,
           out3 = b3 < 0,
           out4 = b4 < 0;

endmodule

