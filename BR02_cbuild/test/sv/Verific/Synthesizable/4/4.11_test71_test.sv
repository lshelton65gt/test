
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): To test packed signed structures,
// where the structure is defined using typedef.

typedef struct packed unsigned { // using typedef
    int a ; 
    shortint b ;
    byte c ;
    bit [7:0] d ;
} pack ; // signed, 2-state

module test(input  clk, 
                  int aa,
                  shortint bb,
                  byte cc,
                  bit [7:0] dd,
           output bit out) ;
    
    pack pk ;
    always @ (posedge clk)
    begin
       pk.a = aa ;
       pk.b = bb ;
       pk.c = cc ;
       pk.d = dd ;
    end

    always @ (negedge clk)
    begin
        // Since the structure is a packed structure
        if (pk[63:32] == aa && pk[31:16] == bb && pk[15:8] == cc && pk[7:0] == dd)
            out = 1'b1 ; // Test PASSED
        else 
            out = 1'b0 ; // Test FAILED
    end
    
endmodule

