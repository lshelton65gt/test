
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Definition of user defined types can be deferred with a forward
// declaration. This design forward declare a type, declare variables of that type and
// use them in function calls, task invocation, module instantiations and nested module
// declaration. The actual definition is specified at the end of the module.

module test (output int out1, out2, out3, input int in) ;

    typedef my_type ;

    my_type var1;

    always @(in)
        var1 = in ;

    function automatic int fact(input int n) ;
        if(n <= 1)
            fact = 1 ;
        else
            fact = n * fact(n -1) ;
    endfunction

    task inv(output int out, input int in) ;
        out = ~in ;
    endtask

    assign out1 = fact(var1) ;

    always @(var1)
        inv(out2, var1) ;

    module shifter(out, in) ;
        output my_type out;
        input my_type in ;
        assign out = in << 4 ;
    endmodule

    shifter I(out3, var1) ;

    typedef int my_type ;

endmodule

