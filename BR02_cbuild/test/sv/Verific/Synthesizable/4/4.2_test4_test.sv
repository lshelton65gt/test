
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of void data type.

module test(output reg [7:0] out, input [7:0] in) ;

    function void void_func(output [7:0] out, input [7:0] in) ;
         out = ~in ;
    endfunction

    always @(in)
        void_func(out, in) ;
     
endmodule

