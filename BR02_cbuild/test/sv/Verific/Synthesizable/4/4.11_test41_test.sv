
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. Two data elements
// are declared of this type. A task is defined whose output argument is of that type.

module test (clk, i1, i2, i3, i4, out);
parameter WIDTH=4;

typedef struct packed {
    logic [WIDTH/2 -1:0] a;
    logic [WIDTH/2 -1:0] b;
} a_type;

input clk;
input signed [WIDTH -1:0] i1, i2, i3, i4;
output reg [WIDTH -1:0] out;
a_type x1, x2;
task init_struct(input i1, i2, i3, i4, output a_type x, y);

    x = (i1 ^ i2) | i3;
    y = (i1 & i2) ^ i3;
   
endtask

always @(posedge clk)
begin
        init_struct(i1, i2, i3, i4, x1, x2);

        case(x1)
                4'b0000:  out = ^x2;
                4'b0001:  out = |x2;
                4'sb0010: out = &x2;
                4'b0011:  out = ~|x2;
                4'sb0100: out = ~&x2;
                4'b0101:  out = ~^x2;
                4'sb0110: out = x2<<2;
                4'b0111:  out = x2-3;
                default: out = x2+2;
        endcase
end

endmodule

