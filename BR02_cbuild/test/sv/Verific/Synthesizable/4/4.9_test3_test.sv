
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design forward declare a new type. It then declares
// data element of the new type and connects them to ports of an instantiation of
// another module. The actual definition is at the bottom of the module.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output [0:width-1] out1,
              output reg [0:width-1] out2);

    typedef b_t;

    b_t t1, t2;

    always@(posedge clk)
    begin
        t1 = in1 ^ in2;
        out2 = in2 - in1;
    end

    assign out1 = t2;

    bottom #(.w(width)) bot1 (clk, t1, t2);

    module bottom #(parameter w = 4)
                   (input clk,
                    input [w-1:0] in1,
                    output reg [w-1:0] out1);

        always@(posedge clk)
        begin
            out1 = (in1[w-1] ? ~in1 : in1);
        end

    endmodule

    typedef logic [width-1:0] b_t;

endmodule

