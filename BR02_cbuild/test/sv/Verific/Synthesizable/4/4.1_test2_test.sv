
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of 4 value data type logic.

module test(output logic [3:0] logic1, logic2) ;

    assign logic1 = 4'b0x1z,
           logic2 = 4'bX10Z ;

endmodule

