
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a new type having packed range. It also
// defines two constant functions. These functions are, then, used in replication
// operation as replicator and in expression. As replicator function is called with
// a constant as argument but as expression function is called with non-constant 
// expression as argument.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter WIDTH = 16;
typedef  logic signed [0:WIDTH-1] atype;
typedef  bit signed [0:WIDTH-1] btype;

input clk;
input signed [0:WIDTH-1] i1, i2;
input signed [0:WIDTH-1] i3, i4;
output reg signed [0:WIDTH-1] o1, o2;

reg [WIDTH-1:0] t1, t2, t3, t4, t5, t6;
int i;

always @(posedge clk)
begin
        t1 = fn1(i1) + fn2(i2);
        t2 = { fn1(-WIDTH) { fn2(i3[WIDTH/2]) } };
        t3 = ((fn1(i4)==fn2(i4))?(i3):(i4));
        t4 = (((fn1(i1)>i1)>(fn2(i2)<i2))?(fn2(i1)):(fn1(i2)));
        t5 = { fn1(WIDTH) { fn1(-fn2(-fn1(i1)))!=i1 } };
        t6 = { |fn1(i3) | ~|fn2(i2) + ^fn1(i1) - &fn2(i4), { WIDTH-1 { 1'b0 } } };

        o1 = t1 + t2 ^ t4 - t3;
        o2 = ~t5 + ^t1 ^ +t6 - t2;
end

function btype fn1 (atype a);
begin
        if (a < 0)
        begin
                fn1 = -a;
        end
        else
        begin
                fn1 = a;
        end
end
endfunction

function btype fn2 (atype a);
begin
        if (a[0] == 1)
        begin
                fn2 = ~(a-1);
        end
        else
        begin
                fn2 = a;
        end
end
endfunction

endmodule

