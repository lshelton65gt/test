
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of packed unsigned structures.

module test(output reg one_hot, input [10:0] data, input clk) ;

    typedef struct packed {
         bit [1:0] start_bits;
         byte data ;
         bit end_bit ;
    } data_struct ;

    data_struct in_data ;

    int i , j ;

    always @(posedge clk)
    begin
        in_data = data ;
        one_hot = 0 ;
        if(in_data[10:9] == 2'b11)
        begin
            if(in_data[0] == 1'b1)
            begin
                j = 0 ;
                for(i = 8; i >= 1; i = i -1)
                begin
                    if(in_data[i] == 1'b1)
                        j = j + 1;
                end
                if(j == 1)
                    one_hot = 1'b1 ;            
            end
        end
    end

endmodule

