
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): To declare a new union type, typedef must be used, otherwise
// one can define an immediate variable of that union. This design defines an unpacked 
// union with immediate variable and a typedef union with variable elsewhere.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    union
    {
        byte memb1;
        bit [8:0] memb2;
        logic [9:0] memb3;
    } un1, un2;    // Immediate unnamed union type variables

    typedef union
    {
        byte memb1;
        bit [8:0] memb2;
        logic [9:0] memb3;
    } t_un;    // Named union type

    t_un un3;     // Variable of named union type

    always@(posedge clk)
    begin
        un1.memb2 = { 1'b0, in2 };
        un2.memb3 = { 1'b0, in1, 1'b1 };

        un3.memb1 = in2 + in1;

        out1 = un3.memb1 ^ un1.memb2;
        out2 = un2.memb3 | un3.memb1;
    end

endmodule

