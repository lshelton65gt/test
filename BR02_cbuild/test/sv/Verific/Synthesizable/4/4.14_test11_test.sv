
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): $unsigned converts a signed number to unsigned and $signed converts
// an unsigned number to signed. This design uses these system
// functions in cascade to check if they perform properly.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    always@(posedge clk)
    begin
        out1 = '0;
        if ($signed($unsigned(-1)) == -1)
            out1 = in1 - in2;

        out2 = '0;
        if ($unsigned($signed(1)) == 1)
            out2 = in2 - in1;
    end

endmodule

