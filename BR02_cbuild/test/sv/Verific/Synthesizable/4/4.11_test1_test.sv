
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. This type is used to create
// variable inside a function. A member of that structure is returned from the function.

typedef struct {
    int a ;
    byte b ;
    reg c ;
} my_st ;

module test(i, o) ;

    parameter p = fn(3) ;

    input [p-1:0] i ;
    output reg [p-1:0] o ;

    always@(i)
    begin
        o = ~i ;
    end

    function int fn(input int i) ;
        my_st st ;
        st = '{ 3, 8'b00000100, 1'b1 } ;
        fn = i ;
        fn += st.b ;
        return fn ;
    endfunction

endmodule

