
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Part select on arrays makes a signed array unsigned. Unsigned
// in casting interprets signed element as unsigned. This design compares (whole)
// part-selected and unsigned casted array. Both of them should be same - bench
// checks that.

module test (input clk,
             input signed [7:0] in1,
             output reg signed [0:7] out1, out2);

    always@(posedge clk)
    begin
        out1 = in1[7:0];
        out2 = unsigned'(in1);
    end

endmodule

