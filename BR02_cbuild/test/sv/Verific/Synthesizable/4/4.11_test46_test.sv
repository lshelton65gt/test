
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. A task is defined
// that takes inputs of that data type. Inside the task it sets the value of output
// argument in a casex statement. The data elements of that type are used to drive
// the output port. It uses type cast operator to set the width of data elements.

module test (clk, i1, i2, i3, o1, o2);

typedef struct packed {
    logic [0:3] a;
    logic [3:0] b;
    logic [0:3] c;
    logic [3:0] d;
} dtype;

input clk;
input signed [0:7] i1, i2, i3;
output reg signed [7:0] o1, o2;

int t1, t2, t3, t4;
dtype d1, d2, d3, d4;

task automatic tskcase(input dtype i1, i2, output signed [0:7] out);
int t1, t2, t3;
begin
        t1 = i1 + i2^i1;
        casex ({t1[7], t1[0]})
                2'b0x:
                        begin
                                t2 = { i1[7], t1[6:1], i2[0] };
                                t3 = t1|t2 + i1.a;
                        end
                2'b1x:
                        begin
                                t2 = ~i1-i2^t1;
                                t3 = t3&t1 + i1.b;
                        end
                default:
                        begin
                                t2 = -i2 - i1 + t1;
                                t3 = t1^t2 + i1.c;
                        end
        endcase
        out = ~t3 + i1[7:0] ^ i2[15:8];
end
endtask

always @(posedge clk)
begin
        t1 = i1<<4 + i2>>4;
        t2 = {i2[0:3], i3[4:7]};
        t3 = {i3[4:7], i1[0:3]};
        t4 = i2<<4|i3>>4;
        d1 = {4'(t1), 4'(t2<<1), 4'(t3<<2), 4'(t4<<3)};
        d2 = {4'(t2), 4'(t3<<1), 4'(t4<<2), 4'(t1<<3)};
        d3 = {4'(t3), 4'(t4<<1), 4'(t1<<2), 4'(t2<<3)};
        d4 = {4'(t4), 4'(t1<<1), 4'(t2<<2), 4'(t3<<3)};

        tskcase(d1, d2, o1);
        tskcase(d3, d4, o2);
end

endmodule

