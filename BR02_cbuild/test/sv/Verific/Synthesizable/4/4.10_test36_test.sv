// testcase was wrong from verific, updated to be similar to test35, but this
// makes it not match the comment. was this change correct?

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Enumeration can have a data type. This design defines an
// enumeration of type bit and with multiple packed dimensions with signed property.
// The first element is initialized with a 'char' type constant as char and bits
// are comparable with appropriate packed dimensions.

module test (input clk,
             input [3:0] in1, in2,
             output reg [0:31] out1, out2);

    typedef enum bit signed [0:7] { A = "a", B, C, D, E, F, G, H,
                                         I, J, K, L, M, N, O, P } alphabet;

    always@(posedge clk)
    begin
        out1 = alphabet'(in1+A);
        out2 = alphabet'(in2+A);
    end

endmodule

