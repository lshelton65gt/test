
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog adds a new keyword unsigned. This makes a
// signed type unsigned. int is by default signed, we qualify it with this unsigned
// qualifier to make it unsigned.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    int isd;
    int signed ise;
    int unsigned us;

    always@(posedge clk)
    begin
        isd = -1;
        ise = -1;
        us = -1;

        out1 = '0;
        out2 = '0;
        if (us > 0 && isd < 0 && ise < 0)
        begin
            out1 = in1 ^ in2;
            out2 = in2 - in1;
        end
    end

endmodule

