
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enumeration and declares some data
// elements of that type. This data element is then passed to a function.

typedef enum { INVALID_MONTH, JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC } months;

module test #(parameter i_w = 4, o_w = 8)
             (input clk,
              input [0:i_w-1] in1, in2,
              output reg [o_w-1:0] out1, out2);

    months mnths;

    always@(posedge clk)
    begin
        out1 = days(months'(in1));
        out2 = days(months'(in2));
    end

    function int days(months m);
        int n;

        case(m)
            FEB:                               n = 28;
            APR, JUN, SEP, NOV:                n = 30;
            JAN, MAR, MAY, JUL, AUG, OCT, DEC: n = 31;
            INVALID_MONTH:                     n = 0;
            default:                           n = 0;
        endcase

        return n;
    endfunction
endmodule

