
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a 'for-generate' construct. Inside the 'for-generate'
// it defines an enumeration. The width of that depends on the 'generate'
// variable and the first enum literal is assigned the value of the
// 'genvar' variable. Enum literals are then used in the design.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    generate
        genvar i;

        for(i=0; i<w; i++)
        begin: for_gen_i
            enum [0:i+1] { A = i, B } my_enum;

            always@(posedge clk)
            begin
                if (1<<(i+1) == A<<(i+1))
                begin
                    out1[i] = in1[i] - in2[i];
                    out2[i] = in2[i] - in1[i];
                end
                else
                begin
                    out1[i] = in2[i] - in1[i];
                    out2[i] = in1[i] - in2[i];
                end
            end
        end
    endgenerate

endmodule

