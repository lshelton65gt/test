
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. One data element is
// defined of that structure type. Inside an always block this is manipulated and
// value of the output port is set. Actually it tries to convert integer values in
// base 10 to BCD.

typedef struct packed {
    int xj;
    int xk;
    int xv;
} d_type;

module test(clk, i1, o1, o2);

parameter WIDTH = 8;

input clk;
input [0:WIDTH-1] i1;
output reg [WIDTH-1:0] o1;
output reg o2;

reg [3:0] r1;
d_type d1;

always @(posedge clk)
begin
        for(int i=0; i<WIDTH; i+=4)
        begin
                if (i==0)
                begin
                        o2 = 0;
                        d1.xv = 0;
                        d1.xj=0;
                end
                else
                begin
                        d1.xj += 10;
                end

                r1 = i1[i +: 4];
                d1.xv = (d1.xv * d1.xj) + r1;

                if (r1 > 9)
                begin
                        o2 = 1;
                        d1.xv = 'bx;
                end
                d1.xk = i;
        end

        if (WIDTH % 4)
        begin
                r1 = i1[(d1.xk - 1) +: (WIDTH%4)+1];
                d1.xv *= ( d1.xj + 10);
                d1.xv += r1;

                if (r1 > 9)
                begin
                        o2 = 1;
                        d1.xv = 'bx;
                end
        end

        o1 = d1.xv;
end

endmodule

