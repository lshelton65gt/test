
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. Four data elements
// of that type are declared. A function is defined with return type of that structure
// type. The function is called from an always_comb block to set the value of that
// data elements. These data elements are then used to drive the output port.

typedef struct packed {
    logic [7:0] a;
    logic [7:0] b;
    bit [7:0] c;
    bit [7:0] d;
} d_type_t;

module test(input [7:0] i1, i2, i3, i4, output [31:0] o1, o2);
d_type_t d1, d2, d3, d4;
    
function d_type_t fn(int x, int y);
d_type_t ret;
    if(8'(x) <= 8'(y))
    begin
        ret.a = 8'(y - x);
        ret.b = y>>2 - x>>2;
        ret.c = ret.a++ + ret.b++;
        ret.d = ret.c--;
    end
    else
    begin
        ret.a = 8'(y - x);
        ret.b = y>>2 - x>>2;
        ret.c = ret.a++ + ret.b++;
        ret.d = ret.c--;
    end
    return ret;
endfunction

always_comb
begin
    d1 = fn(i1, i2);
    d2 = fn(i2, i3);
    d3 = fn(i3, i4);
    d4 = fn(i1, i3);
end

assign o1 = d1 ^ d2;
assign o2 = d3 ^ d4;

endmodule

