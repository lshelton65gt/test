
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The values of the enum literals are treated as constants.
// This design uses enum literals in if-generate condition.

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    enum { A = w } enum_t;

    generate
        if (A > 8)
        begin
            always@(posedge clk)
            begin
                out1 = in1 ^ in2;
                out2 = in2 + in1;
            end
        end
        else
        begin
            always@(posedge clk)
            begin
                out1 = in1 - in2;
                out2 = in2 & in1;
            end
        end
    endgenerate

endmodule

