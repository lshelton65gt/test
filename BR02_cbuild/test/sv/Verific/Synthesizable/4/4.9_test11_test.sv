
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design forward declares a new type and declares data
// element of that type and uses them. The actual definition is at the end of the
// module and that is typedefinition of the type of the type parameter passed to
// this module. This type of the type parameter is overridden in the instantiating
// module with another forward declaration and that is defined at the end of that
// module and is of different type than that of the default type specified in
// defining module.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1,
              output [0:width-1] out2);

    always@(posedge clk)
    begin
        out1 = ~in2;
    end

    typedef b_t;

    bottom #(.dt(b_t), .w(8)) bot1(clk, in1, out2);

    typedef byte b_t;

endmodule

module bottom #(parameter type dt = int, parameter w = 32)
               (input clk,
                input [w-1:0] in1,
                output reg [w-1:0] out1);

    typedef my_dt;

    my_dt temp;

    always@(posedge clk)
    begin
        temp = ~in1;
        out1 = ~temp;
    end

    typedef dt my_dt;

endmodule

