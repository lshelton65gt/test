
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines packed structure type. The value of the 
// input port is assigned to the data element of the type. Then the member element
// of the structure type is used as the expression of a case statement and depending
// on which the value of the output port is set.

typedef struct packed {
    logic f1; 
    logic f2;
    logic f3;
    logic f4;
    logic f5;
    logic f6;
    logic f7;
    logic f8;
    logic f9;
    logic f10;
    logic f11;
    logic f12;
    logic f13;
} dtype;

module test(clk, i1, o1);
input clk;
input [12:0] i1;
output reg [4:0] o1;
dtype d1;

assign d1 = i1;

always @ (posedge clk)
begin 
    o1 <= 5'd0;
    case (d1.f1)
        1'b1: o1 <= 5'd19;
        1'b0:
            begin 
                case (d1.f2)
                    1'b1: o1 <= 5'd10;
                    1'b0:
                    begin 
                        case (d1.f3)
                            1'b1:
                                o1 <= 5'd12;
                            1'b0:
                            begin 
                                case (d1.f4)
                                    1'b1:
                                        o1 <= 5'd14;
                                    1'b0:
                                    begin 
                                        case (d1.f5)
                                            1'b1:
                                                o1 <= 5'd9;
                                            1'b0:
                                            begin 
                                                case (d1.f6)
                                                    1'b1:
                                                        o1 <= 5'd10;
                                                    1'b0:
                                                    begin 
                                                        case (d1.f7)
                                                            1'b1:
                                                                o1 <= 5'd1;
                                                            1'b0:
                                                            begin 
                                                                case (d1.f8)
                                                                    1'b1:
                                                                        o1 <= 5'd13;
                                                                    1'b0:
                                                                    begin 
                                                                        case (d1.f9)
                                                                            1'b1:
                                                                                o1 <= 5'd11;
                                                                            1'b0:
                                                                            begin 
                                                                                case (d1.f10)
                                                                                    1'b1:
                                                                                        o1 <= 5'd2;
                                                                                    1'b0:
                                                                                    begin 
                                                                                        case (d1.f11)
                                                                                            1'b1:
                                                                                                o1 <= 5'd25;
                                                                                            1'b0:
                                                                                            begin 
                                                                                                case (d1.f12)
                                                                                                    1'b1: o1 <= 5'd1;
                                                                                                    1'b0:
                                                                                                    begin 
                                                                                                        case (d1.f13)
                                                                                                            1'b1: o1 <= 5'd2;
                                                                                                            1'b0: o1 <= o1;
                                                                                                            default: o1 <= 5'bx;
                                                                                                        endcase
                                                                                                    end
                                                                                                    default: o1 <= 5'bx;
                                                                                                endcase
                                                                                            end
                                                                                            default: o1 <= 5'bx;
                                                                                        endcase
                                                                                    end
                                                                                    default: o1 <= 5'bx;
                                                                                endcase
                                                                            end
                                                                            default: o1 <= 5'bx;
                                                                        endcase
                                                                    end
                                                                    default: o1 <= 5'bx;
                                                                endcase
                                                            end
                                                            default: o1 <= 5'bx;
                                                        endcase
                                                    end
                                                    default: o1 <= 5'bx;
                                                endcase
                                            end
                                            default: o1 <= 5'bx;
                                        endcase
                                    end
                                    default: o1 <= 5'bx;
                                endcase
                            end
                            default: o1 <= 5'bx;
                        endcase
                    end
                    default: o1 <= 5'bx;
                endcase
            end
            default: o1 <= 5'bx;
        endcase
end

endmodule

