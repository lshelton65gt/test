
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of structure declaration.

module test(day_av, month_av, year_av, day, month, year) ;

    parameter p = 5 ;
    output int day_av, month_av, year_av ;
    input int day[p-1 :0], month[p-1 :0], year[p-1 :0];


    typedef struct
            {
                int day ;
                int month ;
                int year ;
            } date ;

   date date_count [p-1 :0] ;   

   integer i ;

   int sum_day, sum_month, sum_year ;

   always @(*)
   begin
       for(i = 0; i < p; i= i+1)
       begin
           date_count[i].day = day[i] ;
           date_count[i].month = month[i] ;
           date_count[i].year = year[i] ;
       end
       
       sum_day = 0 ;
       sum_month = 0 ;
       sum_year = 0 ;

       for(i = 0 ; i < p; i= i+1)
       begin
           sum_day = sum_day + date_count[i].day ;
           sum_month = sum_month + date_count[i].month ;
           sum_year = sum_year + date_count[i].year ;
       end

       day_av = sum_day / p ;
       month_av = sum_month / p ;
       year_av = sum_year / p ;       
   end                

endmodule

