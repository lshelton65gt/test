
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two new types - one signed and the other
// unsigned. A packed structure type is defined that have member of the two new types.
// This structure type is used as the function parameter and the return type. The
// return value of the function call is used to set the value of the output port.

typedef logic signed [7:0] sbyte;
typedef logic [7:0] ubyte;

typedef struct packed {
    sbyte a;
    ubyte b;
    sbyte c;
    ubyte d;
    sbyte e;
    ubyte f;
} m_type;

module test (clk, i1, i2, i3, o1, o2);

input clk; 
input signed [0:7] i1, i2, i3;
output reg signed [7:0] o1, o2;
reg signed [0:7] r1, r2, r3;
m_type d1, d2;

function automatic m_type shuffle(m_type prev);
    m_type ret;

    ret.a = prev.b;
    ret.b = prev.c;
    ret.c = prev.d;
    ret.d = prev.e;
    ret.e = prev.f;
    ret.f = prev.a;
    return ret;
endfunction

function automatic [0:7] fncif(input signed [7:0] i1, i2);
reg signed [0:7] t1, t2, t3;
begin
        t1 = i1 + i2^i1;
        if (t1[7] < t1[0])
        begin
                t2 = { i1[7], t1[1:6], i2[0] };
                t3 = t1|t2;
        end
        else
        begin
                if (t1[0:3] >= t1[4:7])
                begin
                        t2 = ~i1 - i2 ^ t1;
                        t3 = t2 | t1;
                end
                else
                begin
                        t2 = -i2 - +i1 + +t1;
                        t3 = t1^t2;
                end
        end
        fncif = ~t3;
end
endfunction

always @(posedge clk)
begin
    r1 = i1 + i2;
    r2 = i2 + i3;
    r3 = i1 + i3;
    d1 = {r1, ~r1, r2, ~r2, r3, ~r3};
    d2 = {~r1, r1, ~r2, r2, ~r3, r3};

    for(int i=0; i<6; i++)
    begin
        if(d1>d2)
            d1 = shuffle(d1);
        else
            break;
    end
    
    o1 = fncif(i1<<4 ^ 8'(d1), i2<<4 | 8'(d1>>8));
    o2 = fncif({d1[7:4], d1[3:0]}, {d2[3:0], d2[7:4]});
end

endmodule

