
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design forward declare two new types. It then typedefines
// the second type as the first forward defined type. After that it defines the first
// type as packed array of bits.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    typedef b_t;
    typedef my_bt;

    always@(posedge clk)
    begin
        my_bt t1, t2;

        t1 = in2 - in1;
        t2 = in1 - in2;

        out1 = t2 - ~t1;
        out2 = t1 - ~t2;
    end

    typedef b_t my_bt;
    typedef bit [width-1:0] b_t;

endmodule

