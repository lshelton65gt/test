
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): According to LRM IEEE-1800 sec 4.11: If any data type within a
// packed structure is 4-state, the structure as a whole is treated as a 4-state
// vector. If there are also 2-state members in the structure, there is an implicit 
// conversion from 4-state to 2-state when reading those members, and from 2-state 
// to 4-state when writing them.  Test that if any member of a packed structure is
// 2-state, then the whole structure is converted to 2-state. This design tests this.

module test(output bit is_converted,
                   input bit [7:0] bit_vec,
                   input logic [3:0] logic_vec,
                   input int in) ;

    typedef struct packed
    {
         bit [7:0] bit_vec ;
         logic [3:0] logic_vec ;
         int i ;
    } packed_st;

    packed_st st1;

    int is_x_z, x_z, i ;

    always @(bit_vec, logic_vec, i) 
    begin
        st1.bit_vec = bit_vec ;
        st1.logic_vec = logic_vec ;
        st1.i = in ;

        is_x_z = 0 ;
        is_converted = 0 ;
        for(i = 0 ; i < 4; i = i + 1)
        begin
            if((logic_vec[i] === 1'bx) ||
               (logic_vec[i] === 1'bz))
                is_x_z = 1 ; 
        end

        if(is_x_z)
        begin
            x_z = 0 ;
            for(i = 0; i < 4; i = i+1)
            begin
                if((st1.logic_vec[i] === 1'bx) ||
                   (st1.logic_vec[i] === 1'bz))
                    x_z = 1 ;
            end
            is_converted = x_z ? 1'b0 : 1'b1 ;            
        end
    end  

endmodule

