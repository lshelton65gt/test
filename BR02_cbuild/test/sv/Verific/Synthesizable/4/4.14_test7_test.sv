
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that if a whole part selected packed array is casted
// to signed and sign aware operators are applied on it and the original signed
// packed  array, equivalent results are produced.

module test(output reg result, input bit signed [7:0] in) ;

    reg less_out1, less_out2;

    always @(in)
    begin
        less_out1 = signed'(in[7:0]) < 0;
        less_out2 = in < 0 ;
        if(less_out1 == less_out2)
            result = 1 ;
        else
            result = 0 ;
    end

endmodule

