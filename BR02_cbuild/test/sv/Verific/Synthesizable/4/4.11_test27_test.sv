
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type and two modules. One
// of the ports of the bottom module is of that structure type. The top module
// defines a function which returns that structure type element. The bottom module
// is instantiated twice, one with an element of the required data type whose value
// is set by calling the function and the other with a concatenation as actual.

typedef struct packed {
    logic x1;
    logic x2;
    logic x3;
    logic x4;
    logic x5;
    logic x6;
    logic x7;
} dtype;

module test(clk, i1, i2, i3, i4, i5, i6, i7, o1, o2, o3);
    input clk;
    input i1;
    input signed [0:0] i2;
    input [0:0] i3;
    input signed [1:1] i4;
    input [1:1] i5;
    input signed [11:11] i6;
    input [11:11] i7;
    output reg signed [2:2] o1;
    output signed [2:2] o2, o3;

    reg l1;
    reg signed l2;
    reg signed [0:0] l3;
    reg signed [1:1] l4;

    dtype dt2;

    function dtype fn(reg x1, reg x2, reg x3, reg x4, reg x5, reg x6, reg x7);
        dtype ret;

        ret.x1 = x1; 
        ret.x2 = x2; 
        ret.x3 = x3; 
        ret.x4 = x4; 
        ret.x5 = x5; 
        ret.x6 = x6; 
        ret.x7 = x7; 

        return ret;
    endfunction

    always @(negedge clk)
    begin
        l1 = i1 && i2;
        l2 = (i4 || i5) && !i6;
        l3 = (i7 << 1) - (i7 >> 1);
        l4 = ((i3 <<< 1) || i2) + ((i5 >>> 1) && i6);

        o1 = l1 || !l2;
        dt2 = fn(i1, i2, i3, i4, i5, i6, i7);
    end
 
    bottom b1(clk, {i1,i2,i3,i4,i5,i6,i7}, o2);
    bottom b2(clk, dt2, o3);

endmodule


module bottom(clk, i1, out);
    input dtype i1;
    input clk;
    output reg out;

    int i;

    always @(negedge clk)
    begin
        i = int'(i1); 
        out = ^i;
    end
endmodule

