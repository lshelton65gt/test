
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses a function to perform shift operations,
// the function takes an argument which is of an enumerated type. The enumerated
// type is used to specify the direction of the shift operation.

module test(clk, i1, i2, o1, o2);

parameter WIDTH = 8;

typedef enum {left,right} direction;

input clk;
input signed [WIDTH-1:0] i1, i2;
output reg signed [WIDTH-1:0] o1, o2;

reg signed [WIDTH-1:0] r1, r2, r3, r4;

always @(posedge clk)
begin
        for(int i=1; i<WIDTH; i++)
        begin
                r1 = fn(i1, i, left);
                r2 = fn(i2, i, left);
                r3 = fn(i1, i, right);
                r4 = fn(i2, i, right);

                o1 = fn(r1, WIDTH-i, right) + fn(r3, WIDTH-i, left);
                o2 = fn(r2, WIDTH-i, right) + fn(r4, WIDTH-i, left);
        end
end

function [WIDTH-1:0] fn(reg [WIDTH-1:0] num, reg pos, direction dir);
reg store;
reg [WIDTH-1:0] ret;
begin
        if (dir == left)
        begin
                store = num[WIDTH-1];
                ret = num << pos;
                ret[0] = store;
        end
        else
        begin
                store = num[0];
                ret = num >> pos;
                ret[WIDTH-1] = store;
        end

        fn = ret;
end
endfunction

endmodule

