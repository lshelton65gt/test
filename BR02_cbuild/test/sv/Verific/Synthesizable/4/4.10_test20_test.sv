
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that the value of the unassigned first element of enum
// should be 0.

module test(output reg compare, 
                 output reg [3:0] Q, input clk, reset) ;

    int clk_count ;

    enum integer { ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, UNDEF='x } state ;

    always @(posedge clk, reset)
        if(reset == 1)
            clk_count = 0 ;
        else    
            clk_count++ ;


    always @(clk_count)
    begin
        case (clk_count)
            0 : state = ZERO ;
            1 : state = ONE ;
            2 : state = TWO ;
            3 : state = THREE ;
            4 : state = FOUR ;
            5 : state = FIVE ;
            6 : state = SIX ;
            7 : state = SEVEN ;
            8 : state = EIGHT ;
            default : state = UNDEF ;
        endcase        
        Q = state ;
        compare = (ZERO == 0) ? 'b1 : 0 ;
    end

endmodule

