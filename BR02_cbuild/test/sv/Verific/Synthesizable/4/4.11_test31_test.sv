
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure data-type and performs
// various arithmetic and bitwise operations on variables of this data-type. Part
// selects of packed structures are also used in expressions.

typedef struct packed signed {
    logic [7:0] a;
    int b;
    logic [15:0] c;
} spsd;

module test(clk, i1, i2, o1, o2);

parameter WIDTH = 16;

input clk;
input signed [0:WIDTH-1] i1, i2;
output reg signed [WIDTH-1:0] o1, o2;

reg r1;
reg [0:0] r2;
reg [14:0] r3;
reg [15:0] r4, r5, r6, r7;
reg [16:0] r8;
spsd s1,s2,s3,s4;

always @(posedge clk)
begin
        r1 = &i1 | 1'b1;
        r2 = |i2 + 2'sb11;                 
        r3 = i1 + i2[0:WIDTH-1];    
        
        s1=fn(r1,r2,r3);

        r4 = i1[0:WIDTH-2] -i2;    
        r5 = r4 + r3;             
        r6 = r3 * r1;           

        s2 = fn(r4, r5, r6) +1 ;

        r7 = r3 * r2;          
        r8 = (-r7 - +r6)|s2[10:0];       

        s3 = fn(r7, r8, r7|r8);

        s4 = fn((s1<<3),(s2<<2),(s3<<1));
end

always_comb
begin
        if(s1 > s2)
            o1 = 16'(s1 - s2);        
        else
            o1 = 16'(s2 - s1);

        if (s3 > s4)
            o2 = 16'(s3+1 - s4+1);
        else
            o2 = 16'(s4+1 - s3+1);
end

function spsd fn(int x, int y, int z);
    spsd tmp;
    tmp.a=8'(x);
    tmp.b=y;
    tmp.c=16'(z);
    return tmp;
endfunction

endmodule

