
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses two packed structure data types. The first
// one has  members of both 2-state and 4-state types. The second type has members
// of 4 state type. Then 4-state values are assigned to variables of both the types. 
// Various arithmetic, logical and bitwise operations are also performed on these
// structure types.

module test(clk, i1, i2, i3, o1, o2, o3);

parameter WIDTH = 16;
typedef struct packed {
    bit [0:WIDTH-1] a;
    logic [0:WIDTH-1] b;
} m_type;

typedef struct packed {
    logic [0:WIDTH-1] a;
    logic [0:WIDTH-1] b;
} h_type;

input clk;
input signed [0:WIDTH-1] i1, i2, i3;
output reg signed [0:WIDTH-1] o1, o2, o3;

reg signed [0:WIDTH-1] t1, t2, t3, t4, t5, t6;
m_type m1;
h_type h1;

always @(posedge clk)
begin
        t1 = ~i1 & i2 + (i2 > i1);
        t2 = &i3 | i1 - (i1 <= i3);
        m1 = {t1,t2};
        t3 = ^i2 | i3 * (t1 != t2);
        t4 = t1 - t2 | i1 ~^ i2;
        h1 = {t3,t4};
        t5 = (i1 != i2) * (t4 - t3);

        if (m1 == h1)
        begin
                o1 = {t1,t2} | {t3,t4};
                o2 = {t1,t2} & {t3,t4};
        end
        else
        begin
                o1 = m1 | h1;
                o2 = m1 & h1;
        end

        o3 = t1 ^ (t3 | (t2 | t4));
end

endmodule

