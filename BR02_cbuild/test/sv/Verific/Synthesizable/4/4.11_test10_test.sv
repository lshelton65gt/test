
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that if any member of a packed union is 4-state, the
// whole union is then 4-state .

module test (output reg is_unchanged, input bit [7:0] bit_vec,
                  input logic [7:0] logic_vec);

    typedef union packed 
    {
        bit [7:0] bit_vec ;
        logic [7:0] logic_vec;
    } packed_u ;

    packed_u u[1:0] ;

    int i;
    always @(bit_vec, logic_vec) 
    begin
        u[1].bit_vec = bit_vec ;
        u[0].logic_vec = logic_vec ;

        is_unchanged = 0 ;
        for(i = 0; i < 8; i = i + 1)
        begin
            if((logic_vec[i] === 1'bx) || (logic_vec[i] === 1'bz))
            begin
                if(logic_vec[i] == u[0][i])
                    is_unchanged = 1'b1 ;
            end
        end
    end

endmodule

