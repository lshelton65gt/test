
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Tests if conversion from 4-state value to 2-state value
// cause loss of information.

typedef struct
{
     logic [7:0] logic_vec;
     bit [7:0] bit_vec ;
     integer i;
} my_struct ;

module test(output reg result, input my_struct st);

    my_struct tmp_st ;
    typedef bit [$bits(my_struct) -1 :0] eq_bit_arr ;

    eq_bit_arr arr1 ;

    always @(*)
    begin
        arr1 = eq_bit_arr'(st) ;
        tmp_st = my_struct'(arr1) ;
        if((st.logic_vec == tmp_st.logic_vec) &&
           (st.bit_vec == tmp_st.bit_vec) &&
           (st.i == tmp_st.i))
            result = 0;
        else
            result = 1; 
    end

endmodule

