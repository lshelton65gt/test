
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. Unpacked variable of
// that type is declared and used by a task and the always block. The task is invoked
// from an always_comb block to set the output port value.

typedef struct {
    int indx;
    bit [7:0] key;
} data;

module test(input clk, reset, input [7:0] i1, output logic [7:0] o1,output logic [31:0] o2);
int i;
data r[15:0];
bit filled;

task task1(input bit [7:0] x,  output logic [7:0] y,output logic [31:0] z);
    for(int i=0; i<16; i++)
    begin
        if(r[i].key == x)
        begin
            y = r[i].key;
            z = r[i].indx;
            return;
        end
    end
    y = 0 ;
    z = 0 ;
endtask

always @(posedge clk or negedge reset)
begin
    if(!reset)
    begin
        i = 0;
        filled = 0;
    end
    else
    begin
        if(!filled)
        begin
            if(i>15)
            begin
                i = 0;
                filled = 1;
            end
            else
            begin
                r[i].key = i1;
                r[i].indx = i;
            end
        end
        if(i>49)
            filled = 0;
        i++;
        
    end

end

always_comb
    task1(i1, o1, o2);

endmodule

