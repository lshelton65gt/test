
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing the byte type. byte is a 2-state signed data type,
// that is defined to be exactly 8 bits.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    byte b;

    always@(posedge clk)
    begin
        b = -4;

        if ($bits(b)==8)
            out1 = in1 | in2;
        else
            out1 = '0;

        if (b<0)
            out2 = in2 - in1;
        else
            out2 = '0;
    end

endmodule

