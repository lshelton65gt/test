
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design designs a state machine using enumeration type
// state variable.

typedef enum [1:0] {s0=2'b00,s1=2'b01,s2=2'b10,s3=2'b11} ALL_STATES;

module test(clk,reset,in1,out1);
input clk,reset;
input [1:0] in1;
output [31:0] out1;
reg [31:0] out1;
ALL_STATES state;


     always @(posedge clk)
     begin
      if(reset)
      begin
        state = s0;
        out1 = 0;
      end
      else
        case(state)
          s0 : begin
                out1 = in1;
                state = s1;
              end
          s1 : begin
                out1 = out1 + in1;
                state = s2;
              end
          s2 : begin
                out1 = out1 - in1;
                state = s3;
              end
          s3 : begin
                out1 = out1 + in1;
                state = s0;
              end
       endcase
  end
endmodule

