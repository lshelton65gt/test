
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Unions can be packed or unpacked; unpacked is the default
// when packed is not specified. Packed unions can be signed or unsigned; unsigned
// is the default when nothing is specified. This design defines two packed unions,
// one is signed while the other is unsigned.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef union packed signed    // signed union
    {
        byte memb1;
        bit [7:0] memb2;
        logic [7:0] memb3;
    } t_ps_un;

    typedef union packed    // unsigned is the default signing
    {
        byte memb1;
        bit [7:0] memb2;
        logic [7:0] memb3;
    } t_pu_un;

    t_ps_un un1;
    t_pu_un un2;

    always@(posedge clk)
    begin
        un1.memb1 = '1;
        un2.memb2 = '1;

        if (un1 < 0)
            out1 = un1 + in1;    // Packed union can also be used as a whole with arithmetic and logical operators
        else
            out1 = '0;

        if (un2 > 0)
            out2 = un2.memb3 ^ in2;    // We can read back a packed union member that was written as another member.
        else
            out2 = '0;
    end

endmodule

