
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): By default 'byte' and 'int' are signed data types but 'bit' and 'reg' are
// unsigned data types. This design assigns negative values to both
// type data elements and checks that this property holds true for both the 
// types of data.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    bit [w-1:0] r1;
    reg [w-1:0] r2;
    byte r3;
    int r4;

    always@(posedge clk)
    begin
        r1 = -1;
        r2 = -1;
        r3 = -1;
        r4 = -1;

        out1 = '0;
        out2 = '0;

        if ((r1[w-1] == 1'b1 && r1 > 0) &&
            (r2[w-1] == 1'b1 && r2 > 0))
            out1 = in1 | in2;

        if ((r3[7] == 1'b1 && r3 < 0) &&
            (r4[31] == 1'b1 && r4 < 0))
            out2 = in1 + in2;
    end

endmodule

