
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The values of the enum literals should be constant. This
// design defines an enumeration and assigns the return value of a constant
// function call as the value of the enum literal.

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2);

    enum { A = const_fn(w), B } enum_t;

    always@(posedge clk)
    begin
        out2 = (out1 = '0);
        if (w/2 == A)
        begin
            out1 = in1 ^ in2;
            out2 = in2 | in1;
        end
    end

    function int const_fn(int n);
        return (n>>1);
    endfunction

endmodule

