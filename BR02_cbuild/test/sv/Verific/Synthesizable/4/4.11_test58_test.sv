
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two types. It also defines two structure
// types. Both of them have member element of the new type - one of four state
// and the other of two state. A function is defined that returns four state type
// value, but depending on its inputs it may return two state type value. This
// function is used as the case items. According to the case the output port gets
// different values.

typedef bit [3:0] b_type;
typedef logic [3:0] l_type;

typedef struct packed {
    b_type a;
    int b;
} twos_type_t;

typedef struct packed {
    l_type a;
    integer b;
} fours_type_t;

module test(input clk, input [2:0] i1, input [1:0] i2, i3, output reg [2:0] o1);
fours_type_t d1;

function fours_type_t func(input [1:0] i2, i3);
twos_type_t ret1;
fours_type_t ret2;
    ret1.a = (ret2.a = i1 ^ i2) ;
    ret1.b = (ret2.b = i1 | i2) ;
    
    if(i1[0] && i2[0])
        return ret1;
    else
        return ret2;
endfunction

always_comb
begin
    d1.a = i1 ^ i2;
    d1.b = i1 | i2;
    case (d1)
        func(i2,i3): o1 = 1;
        func(i2,1): o1 = 2;
        func(1,i3): o1 = 3;
        func(i1,i2): o1 = 4;
        default : o1 = 7;
    endcase
end

endmodule

