
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type outside of any
// module. Inside an always_comb block it sets some variable that are used inside 
// an always block to set the value of the data element of the new type. These data
// elements are then used to drive the output port.

typedef struct packed {
    logic [1:0] a;
    bit [1:0] b;
} psdt_t;

module test #(parameter P1 = 0, parameter P2 = 1) (input clk, input [1:0] i1, input i2, output [1:0] o1);

reg [1:0] r, r1, r2;
psdt_t d1, d2;

always_comb
begin
    if(i2)
    begin
        r2 = 1'b1 ^ P1;
        r = 0;
    end
    else
    begin
        r2 = 0;
        r = o1;
        case (1'b1)
            r1[P1]:
                if(! i1[0])
                    r2[P2] = 1'b1;
                else
                    r2[P1] = 1'b1;
            r1[P2]: r = i1;
            default:;
        endcase
    end
end

always @(posedge clk)
begin
    d1[3:2] = r1++ + r2;
    d1[1:0] = r1 + r2;
    d2.a = 2'(d1>>2);
    d2.b = 2'(--d1);
    r1 = r2;
end

assign o1 = r1[0] ? d1.a ^ d1.b : d2.a | d2.b;

endmodule

