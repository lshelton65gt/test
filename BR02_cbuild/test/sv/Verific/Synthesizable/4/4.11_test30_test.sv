
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules and a structure type. One
// variable is declared of that structure type. The element is connected to three
// instantiations of another two modules. Two of that connection are with the input
// port and the other to the output port.

typedef struct packed {
    logic [7:0] x1;
    logic [7:0] x2;
    logic [7:0] x3;
    logic [7:0] x4;
} spdt;

module test(i1, i2, i3, i4, o1, o2, o3);
input signed [15:0] i1, i2, i3, i4;
output [15:0] o1, o2;
output reg [15:0] o3;

reg [62:0] r1, r2;
reg signed [63:0] r3, r4;
reg signed [64:0] r5, r6;
spdt s1;

always_comb
begin
        r1 = (i1 >> 4) ^ (i3 <<< 5);
        r2 = (i2 <<< 12) | (i4 >> 2);

        r3 = { { 32 { ~^r1 } }, { 32 { ~(^~r1) } } };
        r4 = r2[31:0] + r2[62:30];
        r5 = r3 + r1;
        r6 = r4 - r4;
        //s1 += (r5 + r6);
        //o3=s1;
        o3 = r5 + r6;
end

bottom1 b11(s1, o1);
bottom1 b12(s1, o2);
bottom2 b21({i1, i1}, {i2,i2}, {i3, i3}, {i4, i4}, s1);

endmodule

module bottom1(input spdt i1,output reg [15:0] o1);
spdt r1;

always_comb
begin
    r1 = i1;
    o1 = r1++ - 32'(r1<<2) + 32'(r1<<3);
end
endmodule

module bottom2(input int i1,input int i2, input int i3, input int i4,output spdt o1);
always_comb
begin
    o1 = {16'(i1),16'(i2),16'(i3),16'(i4)};
end
endmodule 

