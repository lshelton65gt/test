
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that packed union variables can be assigned to other
// variables of same type as a whole. Also test that arithmatic and logical operations
// can be carried out on packed unions.

typedef union packed signed
{
    int i ;
    logic [31:0] b ;
} signed_union ;

typedef union packed
{
    int i ;
    logic [31:0] b;
} unsigned_union ;

module test(output signed_union s_sum_out, u_sum_out,
            output reg s_less_out, u_less_out, s_greater_out,
            u_greater_out, input signed_union s_in[1:0],
            input unsigned_union u_in [1:0]) ;

    always @(*)
    begin
        s_sum_out = s_in[1] + s_in[0] ;
        s_less_out = s_in[1] < s_in[0] ;
        s_greater_out = s_in[1] > s_in[0] ;

        u_sum_out = u_in[1] + u_in[0] ;
        u_less_out = u_in[1] < u_in[0] ;
        u_greater_out = u_in[1] > u_in[0] ;
    end

endmodule

