
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): To test packed unsigned structure,
// where the structure is defined without using typedef.

struct packed unsigned { // Without using typedef
    int a ; 
    shortint b ;
    byte c ;
    bit [7:0] d ;
} pack1 ; // unsigned, 2-state

module test(input  clk, 
                  int aa,
                  shortint bb,
                  byte cc,
                  bit [7:0] dd,
           output bit out) ;
    
    always @ (posedge clk)
    begin
       pack1.a = aa ;
       pack1.b = bb ;
       pack1.c = cc ;
       pack1.d = dd ;
    end

    always @ (negedge clk)
    begin
        // Since the structure is a packed structure
        if (pack1[63:32] == aa && pack1[31:16] == bb && pack1[15:8] == cc && pack1[7:0] == dd)
            out = 1'b1 ; // Test PASSED
        else 
            out = 1'b0 ; // Test FAILED
    end
    
endmodule

