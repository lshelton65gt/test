
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): System Verilog supports new type by defining structures. This
// design defines a structure inside for-generate block and uses that from inside the
// generate block.

module test(out, in);
    parameter size = 8;
    input logic [size-1:0] in;
    output logic [size-1:0] out;

    genvar i;
    generate
        for(i =  0; i < size; i++)
        begin:blk
            typedef union {
                logic binLog;
                bit grayBit;
            } un;
            un var1;
            assign var1.grayBit = in[i];
            assign var1.binLog = ^var1.grayBit;
            assign out[i] = var1.binLog;
        end     
    endgenerate

endmodule

