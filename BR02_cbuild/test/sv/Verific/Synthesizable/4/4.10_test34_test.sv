
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Enumeration can have a data type. This design defines an
// enumeration of type 'int'. The first element is initialized with a 'char' type
// constant. This type is then used afterwords.

module test (input clk,
             input [3:0] in1, in2,
             output reg [0:7] out1, out2);

    typedef enum int { A = 3 /* initialized with a 'int' type constant */,
                        B, C, D, E, F, G, H, I, J, K, L, M, N, O, P } alphabet;

    always@(posedge clk)
    begin
        out1 = alphabet'(in1+A);    // 0<=in1<=15: A<=out1<=P
        out2 = alphabet'(in2+A);    // 0<=in2<=15: A<=out2<=P
    end

endmodule

