
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Compares width casting with and without using the casting
// operator. Here in1 and in2 are of size 8 bits.

module test(output reg [0:9] out1, out2, input [0:7] in1, in2, input clk);

always @ (posedge clk)
begin
    out1 = 8'(in1+in2) >> 1;
    out2 = (in1+in2) >> 1;
end

endmodule

