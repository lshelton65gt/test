
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The bit pattern of a packed structure can be stored in bit
// type packed arrays; this can be restored back to original structure type variable
// without loss of information. This design checks that.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef struct packed    // packed structure
    {
        byte memb1;
        bit [7:0] memb2;
    } t_ps_st;

    t_ps_st st1;
    bit [7:0] s1, s2;

    always@(posedge clk)
    begin
        st1.memb1 = in1 ^ in2;    // assign values to the packed structure type data elements
        st1.memb2 = in2 & in1;

        s1 = st1.memb1;          // store the value in packed array of bits
        s2 = st1.memb2;

        st1.memb1 = '0;          // manipulate the data elements
        st1.memb2 = '0;

        st1.memb1 = s1;          // restore back the values
        st1.memb2 = s2;


        if ((st1.memb1 == (in1 ^ in2)) && (st1.memb2 == (in2 & in1)))
        begin
            out1 = st1.memb2 ^ st1.memb1;
            out2 = st1.memb1 | st1.memb2;
        end
        else
        begin
            out1 = '0;
            out2 = '0;
        end
    end

endmodule

