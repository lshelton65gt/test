
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): One or more elements or parts of a packed structure type
// variable can be selected. This design checks that.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef struct packed
    {
        byte memb1;
        bit [7:0] memb2;
    } t_ps_st;

    t_ps_st st1, st2;

    always@(posedge clk)
    begin
        st1[7:0] = in1;    // is equivalen to st1.memb2 = in1
        st1[15:8] = in2;   // is equivalen to st1.memb1 = in2
        st2 = ~st1;
        st2[8] = '0;

        out1 = st1[11:4] ^ st2.memb1;
        out2 = st1.memb2 + {st2[15:12], st2[3:0]};
    end

endmodule

