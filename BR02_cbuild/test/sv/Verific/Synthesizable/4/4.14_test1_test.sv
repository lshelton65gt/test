
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that a decimal number can be used as a data type for casting.

module test(output bit [7:0] sum_out, sub_out, mult_out, input int in1, in2) ;

    always @(in1, in2)
    begin
        sum_out = 8'(in1) + 8'(in2) ; 
        sub_out = 8'(in1) - 8'(in2) ;
        mult_out = 8'(in1) * 8'(in2) ; 
    end

endmodule

