
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design forward declare a new type and passes that as the
// parameter type overriding. The actual type is defined after the overriding.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1,
              output [0:width-1] out2);

    always@(posedge clk)
    begin
        out1 = ~in2;
    end

    typedef b_t;

    bottom #(.dt(b_t), .w(8)) bot1(clk, in1, out2);

    typedef byte b_t;

endmodule

module bottom #(parameter type dt = int, parameter w = 32)
               (input clk,
                input [w-1:0] in1,
                output reg [w-1:0] out1);

    dt temp;

    always@(posedge clk)
    begin
        temp = ~in1;
        out1 = ~temp;
    end

endmodule

