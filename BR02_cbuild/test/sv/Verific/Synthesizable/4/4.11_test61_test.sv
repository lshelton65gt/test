
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure outside of any module.
// Data element of that type is declared and inside an always_comb block, it is assigned
// to the output port. Another module is defined in this design and that is instantiated
// from the other module. The output port is connected to the members of the data
// element of the structure type.

typedef struct packed {
    bit [1:0] a;
    bit [1:0] b;
} d_type_t;

module test(input [3:0] i1, i2, output reg [3:0] o1);
d_type_t d1;
    
always_comb
begin
    o1 = d1;
end

bottom b1(i1[0],i1[1], i2[0], i2[1], d1.a);
bottom b2(i1[1],i1[0], i2[1], i2[0], d1.b);
endmodule

module bottom(input i1, i2, i3, i4, output reg [1:0] o1);

function logic fn(bit i1, i2, i3, i4);
    return (i1 ? (i4 ? i2 : 'x ) : (i3 ? 'z : i1));
endfunction 

always_comb
    o1 = {fn(i1,i2,i3,i4), fn(i3,i4,i1,i2)};

endmodule

