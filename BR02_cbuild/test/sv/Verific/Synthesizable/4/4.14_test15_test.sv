
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses increment and decrement operators in a nested
// fashion in arithmetic expressions. The design also employs casting to set the size
// of expressions.

module test(clk, i1, i2, o1);

parameter IN_WIDTH = 8, OUT_WIDTH = 16;

input clk;
input signed [IN_WIDTH-1:0] i1;
input [IN_WIDTH-1:0] i2;
output reg signed [OUT_WIDTH-1:0] o1;

reg [IN_WIDTH-1:0] r1, r2, r3, r4;

always_comb
begin
        r1 = ~(i1 - 1);

        if (r1 == i2)
        begin
                r2 = { ~i1[0], i1[IN_WIDTH-2:0] };
                r3 = { ~i2[0], i2[IN_WIDTH-2:0] };
                r3 += ((r3++) + (r2+=(r1+=(r3 &= r1))));
                r4 = r2 | (~r3 + 1);
        end
        else
        begin
                r2 = { 1'b0, i1[IN_WIDTH-2:0] };
                r2 |= {2'(r1++),~(2'(r1++)),2'(r1++),~(2'(r1++))};
                r3 = { 1'b1, i2[IN_WIDTH-2:0] };
                r4 = r2 & r3;
        end

        o1 = r4 - r1;
end

endmodule

