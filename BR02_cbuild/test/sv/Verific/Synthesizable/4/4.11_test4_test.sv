
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that packed unsigned structures can be assigned to other
// variables of same type as a whole.

module test(output reg compare, input [9:0] data, 
                   input clk, reset) ;

    typedef struct packed
    {
         bit [1:0] start_bits ;
         bit [7:0] ac_data ;
    } data_st ;

    data_st prev_st, curr_st ;

    always @(posedge clk, posedge reset)
    begin
        if(reset)
        begin
            prev_st = 0 ;
        end
        else
        begin
            curr_st = data ;
            
            if(prev_st == curr_st) 
                compare = 1'b1 ;
            else
                compare = 1'b0 ;
            prev_st = curr_st ;
        end
    end

endmodule

