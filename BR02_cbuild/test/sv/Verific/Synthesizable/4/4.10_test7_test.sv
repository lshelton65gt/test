
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Enumerations declared without an explicit data type is assumed
// to be of 'int' type. The int data type is 2-state. This design checks to see whether 
// that is properly implemented.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    enum /* no explicit data type, int is the default */
    {
        FIRST = -30,
        SECOND = -15,
        THIRD = int'('x),
        FOURTH = 15,
        FIFTH = 30
    } five_fifteen;

    int standard = 0;

    always@(posedge clk)
    begin
        out2 = (out1 = '0);
        if ((THIRD == standard) && ($bits(THIRD) == $bits(standard)))
        begin
            out1 = in2 - FIRST + FOURTH;
            out2 = in1 + FIFTH - SECOND;
        end
    end

endmodule

