
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a global structure type. Data elements
// of that type is declared. These data elements are manipulated inside two
// always_ff and one always_comb. In another always_comb block, the value of the
// output port is set by expressions of those data elements.

typedef struct packed {
    logic [3:0] a;
    logic [3:0] b;
} psdt_t;

module test(clk, reset, i1, i2, i3, i4, o1, o2);

input clk, reset;
input [3:0] i1, i2, i3, i4;
output reg [3:0] o1, o2;
psdt_t d1, d2;

always_ff @(posedge clk iff reset==1 or negedge reset)
    d1.a = (!reset) ? i1 : '0;

always_ff @(posedge clk iff reset==0 or posedge reset)
    d2.a = (reset) ? i2 : '0;

always_comb
begin
    d1.b = i1 + i2;
    d2.b = i1 - i2;
end


always_comb
begin
    o1 = d1 ^ {d1.b, d2.b};
    o2 = d2 ^ {d2.b, d1.b};
end

endmodule

