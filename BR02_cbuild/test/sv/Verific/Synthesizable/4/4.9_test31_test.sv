module test(in, out) ;
    typedef my_new_type ;
    parameter type dt = my_new_type ;
    parameter p = 8 ;
    input [p-1:0] in ;
    output [p-1:0] out ;

    typedef struct {
        bit [p-3:0] a ;
        bit [p-3:0] b ;
    } my_new_type ;

    generate
        if (p>2)
        begin
            dt ports ;

            test #(.dt(my_new_type), .p(p-1)) t1 (ports.a, ports.b) ;

            assign ports.a = in[p-2:0] ;
            assign out[p-2:0] = ports.b ;
        end
        if (p==2) assign out[p-2] = in[p-2] ;
    endgenerate

    assign out[p-1] = in[p-1] ;

endmodule

module top (in, out) ;
    parameter p = 16 ;
    input [p-1:0] in ;
    output [p-1:0] out ;

    typedef struct {
        bit [p-2:0] a ;
        bit [p-2:0] b ;
    } my_type ;

    test #(.dt(my_type), .p(p)) t2 (in, out) ;

endmodule

