
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two types having packed range. A function
// is defined which returns one of this types. Inside an always block this function 
// is called and the return value is used to set the value of the output port.

module test #(parameter W=8)(input clk, input [W-1:0] i1, output reg [W-1:0] o1);

typedef logic [W-1:0] i_type_t;
typedef bit [W/2 -1:0] o_type_t;

function o_type_t fn(i_type_t x);
    o_type_t ret ;
    ret = 0 ;
    for(int i=0; i<W; i+=2)
    begin
        ret += (x[i]<<i);
    end
    return ret;
endfunction

always @(posedge clk)
begin
    o1 = fn(i1);
end

endmodule

