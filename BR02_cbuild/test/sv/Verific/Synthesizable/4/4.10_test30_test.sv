
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enumeration and declares some data
// elements of that type having both packed and unpacked range and manipulates
// those data elements.

typedef enum bit { FALSE, TRUE } Boolean ;

module test #(parameter w = 8)
             (input clk,
              input [0:w-1] in1, in2,
              output reg [w-1:0] out1, out2) ;

    Boolean [0:3] b4 [4:7] ;

    always@(posedge clk)
    begin
        b4 = '{ '{ TRUE, FALSE, FALSE, FALSE },
               '{ FALSE, TRUE, FALSE, FALSE },
               '{ FALSE, FALSE, TRUE, FALSE },
               '{ FALSE, FALSE, FALSE, TRUE } } ;

        if (b4[4] == 8       &&
            b4[5] == 4'd4    &&
            b4[6] == 4'b0010 &&
            b4[7] == 4'h1)
        begin
            out1 = in1 & in2 ;
            out2 = in2 ^ in1 ;
        end
    end

endmodule

