
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. A function is
// defined that takes one output argument of that type. Inside an always_ff block, 
// it manipulates some variables. From an always_comb block, it calls the function.
// The function uses those manipulated variables and return a value. The return
// value is used to set the output port.

module test #(parameter p1 = 4'd12,parameter  p2 = 1'd1,parameter  p3 = 2'b11) (input clk, input [1:0] i1, input [4:0] i2, output [4:0] o1);

typedef struct packed {
    logic [p2:0] a;
    logic [p3:0] b;
    logic [p2:0] c;
    logic [p3:0] d;
} psdt_t;

reg [4:0] m1[p1:p2];
bit [3:0] r1;
psdt_t d1,d2;

function void fn(input [1:0] x, input [4:0] y,output psdt_t z);
    z = '0;
    z.a = ^x + r1;
    z.b = ^y + m1[1] + r1;
    z.c = z.a + z.b;
    z.d = z.b + z.c;
endfunction

always_ff @(posedge clk)
begin
    r1 = ^i1;
    for (int i = p2; i<=p1; i++)
    begin
        m1[i] = 5'(i1 - i2);
    end

    case (d1)
        p3:
            begin
                m1[r1] = i2;
                r1++;
            end
        p2:
            begin
                m1[r1] = 5'(i1 ^ i2);               
                r1++;
            end
        p1:
            begin
                m1[r1] = 5'(i1 - i2);
                r1++;
            end
        default:
            begin
                m1[r1] = 5'(i1 + i2);
                r1++;
            end
    endcase
end

always_comb
begin
   fn(i1,i2,d1);
   fn(~i1,~i2,d2); 
end

assign o1 = 5'(d1 - d2);

endmodule

