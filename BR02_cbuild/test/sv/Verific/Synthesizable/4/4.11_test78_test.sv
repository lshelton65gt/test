
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Union type variables can be assigned to other variables of
// same type as a whole. This design defines two unions, one is packed while the
// other is unpacked and they are assigned to another variable of same type.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef union packed     // this is packed
    {
        byte memb1;
        bit [7:0] memb2;
        logic [7:0] memb3;
    } t_p_un;

    typedef union packed signed    // this is packed and also signed
    {
        byte memb1;
        bit [7:0] memb2;
        logic [7:0] memb3;
    } t_ps_un;

    typedef union    // unpacked is default
    {
        byte memb1;
        bit [8:0] memb2;
        logic [9:0] memb3;
    } t_u_un;

    t_p_un un1, un2;
    t_ps_un un3, un4;
    t_u_un un5, un6;

    always@(posedge clk)
    begin
        un1.memb1 = in1;
        un3.memb2 = in2;
        un5.memb3 = in1^in2;

        un2 = un1;    // unions can be assigned as a whole to another variable of same type
        un4 = un3;
        un6 = un5;

        out1 = un2.memb2 - un4.memb1;  // members that are not written can be read for packed unions
        out2 = un6.memb3 ^ un2;        // unions can be uses as a whole for packed unions
    end

endmodule

