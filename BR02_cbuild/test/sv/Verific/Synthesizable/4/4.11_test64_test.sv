
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. It has three
// members. Two of which are of type that are defined in this design and have packed
// range. Variables of these types are declared. These variables are manipulated
// inside an always block. Inside another always_comb block the output port is set
// from the values of these data elements.

typedef logic [3:0] a_type_t;
typedef bit [3:0] b_type_t;

typedef struct packed {
    a_type_t a;
    b_type_t b;
    int c;
} d_type_t;

module test(input clk, i1, input [3:0] i2, i3, input [7:0] i4, output reg signed [7:0] o1);

a_type_t a1, a2;
b_type_t b1,b2;
d_type_t d1, d2;
int i;

always @(posedge clk, negedge i2[0])
begin
    if(!i2[0])
    begin
        i = '0;
        a1 = (a2 = (b1 =( b2 = '0)));
    end
    else
    begin
        if(i3)
        begin
            a1 = i2 + 2'b01;
            b1 = a1 - i3;
            a2 = b1 ^ i2;
            b2 = a2 - a1; 
        end
        else
        begin
            if(i1)
            begin
                a1 = i2 - 2'b11;
                b1 = a1 + i3 - 4'(4*i1);
                a2 = b1 - i2;
                b2 = a2 + a1; 
            end
            else
            begin
                a1 = i2 + 2'b10;
                b1 = a1 + i3 + 4'(2*i1);
                a2 = b1 + i2;
                b2 = a2 + a1; 
            end
        end
    end
end

assign d1 = {a1, b1, i};
assign d2 = {a2, b2, i};

always_comb
    o1 = {(d1.a - d2.a),(d1.b - d2.b)} ^ (d1.c & d2.c);

endmodule

