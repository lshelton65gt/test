
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two structures. First structure is
// a member of second structure.

typedef struct packed {
    bit [2:0] opcode ;
    bit [15:0] data ;
}instruction ;

typedef struct {
    instruction instruct1 [20:1] ;
}programe ;

function int operation (input instruction IR) ;
     case (IR.opcode)
         8'h1 : operation = IR.data + IR.data;
         8'h2 : operation = IR.data - IR.data;
         8'h3 : operation = IR.data * IR.data;
         8'h0 : operation = IR.data / IR.data;
         default : operation = 0 ;
    endcase
endfunction

module test (input start, ready, instruction IR, output int result) ;
    always @ (IR.opcode or start or ready)
        if((start == 1)  && (ready == 1)) 
            result = operation(IR) ;
endmodule

