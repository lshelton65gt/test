
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. Two data elements are 
// defined of this structure type. A task is defined that manipulates those data
// elements. These data element are used to set the value of the output ports.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter WIDTH = 16;
typedef struct packed {
    logic [0:WIDTH -1] a;
    bit [0:WIDTH -1] b;
} dtype;

input clk;
input signed [0:WIDTH -1] i1, i2;
input signed [0:WIDTH -1] i3, i4;
output reg signed [0:WIDTH-1] o1, o2;

reg signed [0:WIDTH -1] t1, t2;
reg [0:WIDTH -1] t3, t4, t5, t6, t7, t8;
dtype d1 = '1, d2 = '0;

task task1(dtype x1, x2);
    d1 += x1;
    d2 -= x2;   
endtask

always @(posedge clk)
begin
        t3 = 1; t4 = 0; t5 = 1; t6 = 0;

        for(int i=-WIDTH+1; i>0; i++)
        begin
                t3 *= i1[i+:2];
        end

        for(int i=0; i<WIDTH-1; i++)
        begin
                t4 += i3[i+:2];
        end

        for(t1=-WIDTH+1; t1>0; t1++)
        begin
                t5 *= i2[t1+:2];
        end

        for(t2=0; t2<WIDTH-1; t2++)
        begin
                t6 += i4[t2+:2];
        end

        t7 = t3 + t6 - t2 | i1;
        task1({t3, t6}, {t2, i1});
        t8 = t5 ^ t4 + t1 - i3;
        task1({t5, t4}, {t1, i3});

        o1 = t7 | t8 - i4 ^ d1.a & d2.a;
        o2 = t7 & t8 | i2 + d2.b ^ d1.b;
end

endmodule

