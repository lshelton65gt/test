
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enum type with range. This type is used
// as the return value of a function. Another set of two functions are defined. The
// last of the series is called from an always_comb block to set the value of output
// port. That function in turn calls another function and that one call the other
// function.

typedef enum [1:1] {false,true} bool;

module test(input i1, i2, output reg [1:0] o1);

function bool fn(int x, int y);
    return ((x ^ y) < (x & y));
endfunction

function logic [1:0] fn1(int x, int y);
    if(fn(x,y) == fn(y,x))
        return x;
    else
        return y;
endfunction

function logic [1:0] fn2(int x, int y);
    return {x[0],fn1(x,y)};
endfunction


always_comb
begin
    o1 = fn2(i1,i2);
end

endmodule

