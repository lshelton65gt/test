
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing whether unpacked structures can be assigned to other
// variables of same type as a whole.

module test(output reg compare, input [9:0] data, 
                   input clk, reset) ;

    typedef struct
    {
         bit [1:0] start_bits ;
         bit [7:0] ac_data ;
    } data_st ;

    data_st prev_st, curr_st ;

    always @(posedge clk, posedge reset)
    begin
        if(reset)
        begin
            prev_st.start_bits = 2'b00 ;
            prev_st.ac_data = '0 ;
        end
        else
        begin
            curr_st.start_bits = data[9:8] ;
            curr_st.ac_data = data[7:0] ;

            if((prev_st.start_bits == curr_st.start_bits) &&
               (prev_st.ac_data == curr_st.ac_data))
                compare = 1'b1 ;
            else
                compare = 1'b0 ;
            prev_st = curr_st ;
        end
    end

endmodule
 
