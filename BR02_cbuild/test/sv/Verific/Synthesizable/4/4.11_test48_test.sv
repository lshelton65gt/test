
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines packed structure type. The type is used
// as the type of the output argument of a task. The task is called from an always
// block. The value of the output argument of the task invocation is used to set
// the value of the output port of the module.

typedef logic [1:0] a_type;

typedef struct packed {
    logic [3:0] a;
    bit [3:0] b;
} dtype;

module test(clk, i1, i2, i3, o1, o2, o3);
input clk;
input [3:0] i1, i2, i3;
output signed [3:0] o1, o2, o3;
dtype d1, d2;

task t1(input a_type a, b, c, output dtype out);
    out = { 2'b0,  2'b0,  2'b0 };
    if((b < a) && (a > c))
    begin
        out[5:4]  = a;
        if(b < c)
        begin
            out[3:2]  = c;
            out[1:0]  = b;
        end
        else
        begin
            out[3:2]  = b;
            out[1:0]  = c;
        end
    end
    else
    begin
        if((a < b) && (b > c))
        begin
            out[5:4]  = b;
            if(a > c)
            begin
                out[3:2]  = a;
                out[1:0]  = c;
            end
            else
            begin
                out[3:2]  = c;
                out[1:0]  = a;
            end
        end
        else
        begin
            out[5:4]  = c;
            if(b > a)
            begin
                out[3:2]  = b;
                out[1:0]  = a;
            end
            else
            begin
                out[3:2]  = a;
                out[1:0]  = b;
            end
        end
    end
endtask

always @(posedge clk)
begin
    t1 (i1, i2, i3, d1);
    t1 (~i1, ~i2, ~i3, d2);
end

assign o1 = d1.a ^ d2.b;
assign o2 = d1.b | d2.a;
assign o3 = d1.a + d2.b;

endmodule

