
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two enum types. One of them is used as
// a port of the module. Depending on the value of that port, output port gets
// different values. This uses type casting to the enum types for the case
// expressions.

typedef enum [1:0] {s0, s1, s2, s3} en_type_t;
typedef enum [2:0] {t0, t1, t2, t3, t4, t5, t6, t7} en_type1_t;

module test(input clk,input [1:0] i1, i2,input en_type_t i3, output reg signed [3:0] o1);

bit [1:0] r1;

assign r1 = i3;
always @(posedge clk)
begin
    o1 = 0;
    case (en_type_t'(r1))
         s0:  if(i1 | i3) o1 = {~r1, i2}; else o1 = {~i3,i2} ;
         s1:  if(i2 | i3) o1 = {~r1, i1}; else o1 = {~i3,i1} ;
         s2:  if((i1[1:0] - i2[1:0])>i3)
                    if(i1[1]) o1 = i1[1:0]; else o1 = i1[1:1];
              else
                    o1 = '1 ^ 1'b1;
         s3:
            begin
                case (en_type1_t'({i3,i1[0]}))
                  t0 : if(i1 ^ i2) o1 = '0;  else o1 = '1;
                  t1 : if(i1 | i2) o1 = i1; else o1 = ~i1;
                  t2 : if(i1 & i2) o1 = ~i2; else o1 = i2;
                  t3 : if(i1 ^ ~i2) o1 = ~i3; else o1 = i3;
                  default: o1 = i3;
                endcase
            end
        default:;
    endcase
end

endmodule

