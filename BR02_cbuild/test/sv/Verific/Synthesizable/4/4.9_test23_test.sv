
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two data type having packed range. It
// defines two modules. The bottom module defines ports of these types. The other
// module instantiates that module with generic data elements.

typedef bit [3:0] b_type;
typedef logic [3:0] l_type;

module test(input clk,input [3:0]  i1, i2, output [3:0] o1);

reg r1[0:3], r2[0:3];
int j=0;

always_comb
begin
    if(i1 ^ 1'b1)
    begin
        for (int i = 1; i <= 3; i++)
            r2[i] = r1[i];
    end
    if(1'b0 || i1)
    begin
        for (int i = 1 ;i <= 3; i++)
        begin
            r2[i&1] = ^ i;
        end
    end
end

always @(posedge clk)
begin
    if(j++ >10)
        j=0;
    else
        r1 = r2;
end

bottom b1(clk, {r1[0], r1[1], r1[2], r1[3]} ^ {r2[0], r2[1], r2[2], r2[3]}, o1); 

endmodule

module bottom(input clk,input b_type i1, output l_type out);
    
always @(posedge clk)
begin
    if(i1[0] & i1[1]) out[0] = '1; else out[0] = '0; 
    if(i1[1] & i1[2]) out[1] = '0; else out[1] = '1; 
    if(i1[2] & i1[3]) out[2] = '1; else out[2] = '0; 
    if(i1[3] & i1[0]) out[3] = '0; else out[3] = '1; 
end

endmodule

