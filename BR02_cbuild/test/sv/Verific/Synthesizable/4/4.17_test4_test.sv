
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Attribute instance can be specified for interfaces.
// The default type of the attribute is bit and value is 1. If the type
// of the value is different then (like using type casting) the attribute
// specification takes that type. This design assigns the return value of
// a constant function to the attribute specification.

localparam int i = 10;

function integer const_fn(integer sr);
    const_fn = i>>sr;
endfunction

(* first_attr, second_attr = const_fn(1), third_attr = logic'(const_fn(2)) *) interface iface(input clk);
    logic [7:0] in1;
    logic [7:0] in2;
    logic [7:0] out1;
    logic [7:0] out2;

    modport std(input in1, in2, output out1, out2);
endinterface

module test (iface ifc);

    always@(ifc.clk)
    begin
        ifc.out1 = ifc.in1 | ifc.in2;
        ifc.out2 = ifc.in2 ^ ifc.in1;
    end

endmodule

