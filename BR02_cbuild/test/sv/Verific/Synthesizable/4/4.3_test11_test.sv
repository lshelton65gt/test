
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): By default 'shortint' and 'longint' are signed data types but 'reg' and
// 'logic' are unsigned data types. This design assigns negative values
// to both type data elements and checks that this property holds true for
// both types of data.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    reg [w-1:0] r1;  // Unsigned data type
    logic [w-1:0] r2;// Unsigned data type
    shortint r3;     // Signed data type
    longint r4;      // Signed data type

    always@(posedge clk)
    begin
        r1 = (r2 = (r3 = (r4 = -1)));

        out2 = (out1 = '0);

        if ((r1[w-1] == 1'b1 && r1 > 0) &&
            (r2[w-1] == 1'b1 && r2 > 0))
            out1 = in1 ^ in2;

        if ((r3[$high(r3)] == 1'b1 && r3 < 0) &&
            (r4[$left(r4)] == 1'b1 && r4 < 0))
            out2 = in1 - in2;
    end

endmodule

