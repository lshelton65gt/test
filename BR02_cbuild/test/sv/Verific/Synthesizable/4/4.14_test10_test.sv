
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): A decimal number given instead of a data type when casting means
// that the width of the expression is casted. This design checks to
// see if width casting is properly implemented by the tool.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    always@(posedge clk)
    begin
        out1 = '0;
        if ($bits(2'(in1)) == 2)    // check if size down casting is correct
            out1 = in1 - in2;

        out2 = '0;
        if ($bits(1000'(in1)) == 1000)    // check if size up casting is correct
            out2 = in2 - in1;
    end

endmodule

