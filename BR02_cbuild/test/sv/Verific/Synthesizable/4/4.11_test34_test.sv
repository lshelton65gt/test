
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type inside a module. It 
// defines two such structure type variables and assigns those in an always block
// by using concatenation. They are finally used to set the value of the output port.

module test(clk, i1, i2, o1, o2);

parameter WIDTH=16;
typedef struct packed {
    logic [WIDTH -1:0] a;
    logic [WIDTH -1:0] b;
    logic [WIDTH -1:0] c;
    logic [WIDTH -1:0] d;
} spdt;

input clk;
input signed [WIDTH-1:0] i1, i2;
output signed [WIDTH-1:0] o1, o2;

reg r1;
spdt r2, r3;

always@(posedge clk)
begin
        r1 = (i1 === i2);

        if (r1)
        begin
                r2 = '1 & {i1 | i2, i1 & i2, i1 ^ i2, i1 + i2} ;
                r3 = { ++r2, (i1 & i2), ~r2 };
        end
        else
        begin
                r2 = '0 ^ {(i1 & i2),(i1 & i2),(i1 & i2),(i1 & i2)};
                r3 = { ~r2, (i1 | i2), --r2 };
        end
end

                assign o1 = r2 | r3;
                assign o2 = r2 + r3;
endmodule

