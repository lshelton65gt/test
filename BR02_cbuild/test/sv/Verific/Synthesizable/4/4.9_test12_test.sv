
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design forward defines two types. One of them is passed
// to an instantiation of a module which is defined inside this module. The nested
// module defines a type parameter. The default value of that type is the other
// forward defined type. The nested module again forward defines another type and
// uses data element of that type. The actual definitions are at the end of the
// corresponding modules.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1,
              output [0:width-1] out2);

    always@(posedge clk)
    begin
        out1 = ~in2;
    end

    typedef b_t;
    typedef my_bt;

    bottom #(.dt(my_bt), .w(8)) bot1(clk, in1, out2);

    module bottom #(parameter type dt = b_t, parameter w = 32)
                   (input clk,
                    input [w-1:0] in1,
                    output reg [w-1:0] out1);
     
        typedef my_dt;
     
        my_dt temp;
     
        always@(posedge clk)
        begin
            temp = ~in1;
            out1 = ~temp;
        end
     
        typedef dt my_dt;

    endmodule

    typedef b_t my_bt;
    typedef byte b_t;

endmodule

