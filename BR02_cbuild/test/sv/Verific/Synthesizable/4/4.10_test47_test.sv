
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an enum type. Internally it models a
// state machine. By default it starts from initial state and forwarded to the next
// state, but it can be forcefully set to the initial state by reseting the reset
// signal.

typedef enum [1:0] {state0,state1,state2,state3} STATES;

module test(clk, reset,i1, i2, i3, i4, o1, o2);

parameter WIDTH = 32;

input clk,reset;
input signed [0:WIDTH-1] i1, i2;
input [0:WIDTH-1] i3, i4;
output reg [WIDTH-1:0] o1, o2;

reg l1, l2, l3, l4;
reg signed [30:0] r1, r2;
reg [0:31] r3, r4;
reg [0:32] r5, r6;
STATES s=state0;

always @(posedge clk, negedge reset)
begin
    if(!reset)
        s=state0;
    else
    begin
        case(s)
            state0:
            begin 
                l1 = (i1 > i2);
                l2 = (i2 <= i3);
                l3 = (i3 < i4);
                l4 = (i4 >= i1);
                //s++;
                s = state1;
            end
            state1:
            begin
                r1 = ((i2 == i4)? i1 : i3);
                r3 = ((i1 > i3)? i2: i4);
                r5 = ((l2)?(i2+i3):(i1-i4));
                //s++;
                s = state2;
            end
            state2:
            begin
                r2 = (l1 <= l3)*r1;
                r4 = (r1 | (r2 << (l2 == l3)));
                r6 = (r5 * ((&l4) | (~(l3 > l4))));
                //s++;
                s = state3;
            end
            state3:
            begin
                o1 = r6 ^ r2;
                o2 = r4 << (WIDTH >> 2);
                s = state0;
            end
        endcase
    end
end

endmodule

