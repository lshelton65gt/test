
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Usual arithmatic and logic operations can be carried out
// on packed unions as a whole. Packed unions can be signed or unsigned. This
// design applies to various sign aware operators like >, <, etc. on signed
// packed union type variables.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef union packed signed    // this is packed signed union
    {
        byte memb1;
        bit [7:0] memb2;
    } t_ps_un;

    t_ps_un un1, un2, un3, un4;

    always@(posedge clk)
    begin
        un1.memb1 = {4'b1, 4'b0};    // memb1 of un1 is now 8'b11110000
        un2.memb2 = ~un1.memb2;      // memb2 of un2 is now 8'b00001111

        if (un2 < un1)
        begin
            un3.memb1 = in1 ^ in2;
            un4.memb2 = ~(in1 + in2);
        end
        else
        begin
            if (un2 > un1)
            begin
                un3.memb2 = in2 - in1;
                un4.memb1 = ~un3.memb1;
            end
            else
            begin
                un3 = in1 - in2;
                un4 = ~un3;
            end
        end

        out1 = un2 ^ un4.memb1;
        out2 = un3.memb2 & un1;
    end

endmodule

