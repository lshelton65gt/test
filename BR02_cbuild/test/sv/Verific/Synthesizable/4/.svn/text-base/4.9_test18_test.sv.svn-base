
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines multiple packed dimensions in stages.
// This is done using typedefs. The final data-type created is then used in various
// arithmetic, bitwise operations and in connecting ports.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter W1 = 4;
parameter W2 = 8;

typedef bit [0:W1 -1] nibble;
typedef nibble [0:W2 -1] dtype;

input clk;
input signed [0:W1*W2 -1] i1, i2, i3, i4;
output reg signed [0:W1*W2 -1] o1, o2;

reg signed [0:W1*W2 -1] t1, t2, t3, t4;
reg [0:W1*W2 -1] t5, t6, t7, t8;
dtype d1, d2, d3, d4;

always @(posedge clk)
begin
    t1 = i1 + i2;
    t2 = i3 - i4;
    t3 = i1[W1/2:W1] * i3;
    t4 = { i2[0:W1/2+1], i4};
    d1 = '1;
    d2 = '0;
       
    t5 = t1 | t2 ^ t3 >> 2;
    t6 = ~t4 & t3;
    t7 = t5 << 2 + t4;
    t8 = t6 >>> 2 & t5;
    d1 &= (t4 + t5) ^ (t1 + t3);
    d2 |= (t4 - t5) ^ (t1 - t3);
    
    o1 = (t7 <<< 3 ^ t6) ^ d3;
    o2 = (t8 >> 4 | t5) ^ d4;
end

bottom #(.W1(W1), .W2(W2)) b1(clk, d1, d2, d3, d4);

endmodule

module bottom(clk, i1, i2, o1, o2);
parameter W1 = 4;
parameter W2 = 8;

typedef bit [0:W1 -1] nibble;
typedef nibble [0:W2 -1] dtype;

input clk;
input dtype i1, i2;
output dtype o1, o2;

always @(posedge clk)
begin
    for(int i=0; i<W2; i++)
    begin
        o1[i] = i1[i] ^ i2[i];
        o2[i] = i1[i] | i2[i];
    end
end
endmodule

