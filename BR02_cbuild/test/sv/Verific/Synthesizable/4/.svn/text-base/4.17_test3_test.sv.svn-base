
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Attribute instance can be specified for interfaces. The default
// type of the attribute is bit with value 1. Attribute value should be constant. This
// design uses constant multiconcatenation as attribute value.

(* first_attr, second_attr = 5, third_attr = {4{1'b1}} *) interface iface(input clk);
    logic [7:0] in1;
    logic [7:0] in2;
    logic [7:0] out1;
    logic [7:0] out2;

    modport std(input in1, in2, output out1, out2);
endinterface
module test(clk, in1, in2, out1, out2);

parameter  w = 8;

input clk;
input [w-1:0] in1, in2;
output [w-1:0] out1, out2;


iface ifc(clk);

always@(posedge clk)
begin
    ifc.in1 = in1 & in2;
    ifc.in2 = in2 - in1;
end

bot t_mod (ifc);

assign out1 = ifc.out1 - ifc.out2,
       out2 = ifc.out1 + ifc.out2;

endmodule
module bot (iface ifc);

    always@(ifc.clk)
    begin
        ifc.out1 = ifc.in1 | ifc.in2;
        ifc.out2 = ifc.in2 ^ ifc.in1;
    end
endmodule


