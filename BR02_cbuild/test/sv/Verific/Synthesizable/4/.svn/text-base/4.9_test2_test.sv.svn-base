
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design forward declares a new type. It then declares
// data element of the new type and uses it as the actual argument of a function
// call. The actual definition is at the end of the module.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    typedef b_t;

    b_t t1, t2;

    always@(posedge clk)
    begin
        t1 = in1 - in2;
        t2 = in2 - in1;

        out1 = test_fn(t1, t2);
        out2 = test_fn(t2, t1);
    end

    function b_t test_fn(b_t a, b);
        return (a>b ? a ^ b : a + b);
    endfunction

    typedef bit [width-1:0] b_t;

endmodule

