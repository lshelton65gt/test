
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed union having a packed
// structure as member. One member of that union is assigned within always
// block, but assigned values are tested accessing different members.

typedef struct packed { // default unsigned 
    bit [3:0] ABC ; 
    bit [7:0] DEF ; 
    bit [11:0] GHI ; 
    bit [7:0] JKL ; 
    bit [31:0] MNO ; 
} struct64bit ; 

typedef union packed { // default unsigned 
    struct64bit st1;
    bit [63:0] bit_slice; 
    bit [7:0][7:0] byte_slice; 
} union64bit; 

module test (input int number, bit clk, output bit out) ;
    union64bit u1;
    always @ (number) 
    begin
       u1.bit_slice = number ;
       if ( u1.bit_slice[31:24] == u1.byte_slice[3] && u1.st1.JKL == u1.byte_slice[4])
           out = 1'b1 ;
       else
           out = 1'b0 ;
    end
endmodule

