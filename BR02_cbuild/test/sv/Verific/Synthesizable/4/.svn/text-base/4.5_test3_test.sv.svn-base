
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a void function having two input and
// one output ports. The function also uses return to return from it but does not
// return a value. It is called inside an if statement inside an always block.

module test(i1, i2, o1);

parameter A_WIDTH = 8;
parameter B_WIDTH = 8;
parameter RES_WIDTH = 16;

input signed [A_WIDTH-1:0] i1;
input [B_WIDTH-1:0] i2;
output reg signed [RES_WIDTH-1:0] o1;

function void fn1(reg [A_WIDTH-1:0] x, reg [B_WIDTH-1:0] y, output reg [RES_WIDTH-1:0] z);
    z=(x<<<2) - (y>>>2);
    return;
endfunction

always @(i1, i2)
begin
        if ( i1 > i2)
                fn1(i1, i2, o1);
        else
        begin
                if ( i1 == i2)
                        o1 = i1 + i2;
                else
                        o1 = i1[A_WIDTH-4:0] * i2[B_WIDTH-1:B_WIDTH-5];
        end
end

endmodule

