
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog supports forward type definition. This design forward
// defines a type and declares a data element of that type. It then uses
// $bits system function on that data element to check whether it returns
// correct number of bits in the data element.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    typedef logic_t;    // forward definition

    logic_t r1;    // data element of forward defined type

    always@(posedge clk)
    begin
        r1 = '1;    // assigned values using ' notation

        out2 = '0;
        out1 = '0;
        if ($bits(r1) == 8)    // check the width of the data element
            out1 = in2 - in1;

        if ($bits(r1) == $bits(logic_t))    // check the width of the data element with the width of the type
            out2 = in1 - in2;
    end

    // actual type defined here
    typedef logic [13:6] logic_t;

endmodule

