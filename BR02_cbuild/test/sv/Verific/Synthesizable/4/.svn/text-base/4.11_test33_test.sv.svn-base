
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function which returns a structure data
// type. The members of the function return value are then used in expressions.
// The design also casts literals and variables to signed or unsigned.

typedef struct packed {
    int a;
    int b;
    int c;
    int d;
} sdt;

module test(clk, i1, i2, i3, i4, o1, o2, o3, o4);

parameter WIDTH=64;

input clk;
input signed [WIDTH-1:0] i1, i2, i3, i4;
output reg signed [0:-WIDTH+1] o1, o2;
output signed [0:-WIDTH+1] o3, o4;

reg signed [WIDTH-1:0] r1, r2;
reg [WIDTH-2:0] r3, r4;
reg signed [WIDTH:0] r5, r6;
function sdt fn(longint x, longint y);
sdt d1;
    d1.a = 32'(x) & 32'(x>>32);
    d1.b = 32'(x) ^ 32'(y);
    d1.c = 32'(y>>32) + 32'(y);
    d1.d = (x | y) & (y + d1.c) - (y + d1.b);
    return d1;
endfunction

always @ (posedge clk)
begin
        r1 = (unsigned'(i1) <<< 4 <<< 2);
        r2 = fn(i1, i2) >>> 2;
        r3 = fn(i3, i2) << 3;
        r4 = fn(i4, i2) >> 4;
        r5 = fn(i1-i3, i2) >>> 2;
        r6 = (i1*fn(i1,i2)) >> WIDTH;

        o1 = r1^r2^r3;
        o2 = (r3&r2)^(r4&r3)^(r6&r5);
end
        assign o3 = i1^i2;
        assign o4 = ((r5 * r6) < signed'('1)) ? r5: r6;
endmodule

