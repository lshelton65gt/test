
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure. It defines a function
// which returns value of this type. Data elements are declared of that type and
// are used to set the value of the output port inside an casex statement.

module test (clk, i1, i2, i3, o1);

parameter PortSize = 8;

typedef struct packed {
    logic [0:PortSize -1] a;
    logic [0:PortSize -1] b;
} dtype;

input clk;
input [0:PortSize-1] i1;
input [0:PortSize-1] i2;
input [0:PortSize-1] i3;
output reg [0:PortSize-1] o1;

dtype t1, t2, t3;

function dtype fn(int x, int y);
    dtype ret;
    ret.a = x ^ y;
    ret.b = x & y;
    return ret;
endfunction

always @(posedge clk)
begin
        casex ({i1[2], i2[PortSize/2-1]})
                2'bx0:
                        begin
                                t1 = fn(i1, i3);
                                t2 = fn(i1, i2);
                                t3 = t2 - t1;
                                if (t3 == (i3-i2))
                                        o1 = t3;
                                else
                                        o1 = (t1 ^ t2) << PortSize | t3;
                        end
                2'b1x:
                        begin
                                t1 = fn(i1 << PortSize, i2);
                                t2 = fn(i3 << PortSize, i2);
                                t3 = (t2 - t1) >> PortSize;
                                if (t3 == (i3-i1))
                                        o1 = i3-i1;
                                else
                                        o1 = (t1 ^ t2) >> PortSize + t3;
                        end
                default:
                        o1 = { { PortSize/2 { 1'b1 } }, { PortSize/2 { 1'b0 } } };
        endcase
end

endmodule

