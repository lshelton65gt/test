
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): A member of a packed union can also be accessed using part
// select similar to packed structure. This design checks that.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    typedef union packed
    {
        byte memb1;
        bit [7:0] memb2;
    } t_ps_un;

    t_ps_un un1, un2;

    always@(posedge clk)
    begin
        { un1[7:4], un1[3:0]} = in1 ^ in2;
        un2 = ~un1;
        un2[4] = '0;

        out1 = un1[7:0] * un2.memb1;
        out2 = un1.memb2 + {un2[3:0], un2[7:4]};
    end

endmodule

