
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. A data element
// is declared of this type. A function is defined and it is called inside an 
// always_comb block to set the member element of that type. This data element
// is then assigned to the output port.

typedef struct packed {
    bit [1:0] a;
    bit [1:0] b;
} dtype_t;

module test(input [1:0] i1, i2, i3, output [1:0] o1, o2);

dtype_t d1;

function logic [1:0] func(input [1:0] i1, i2, i3);

    if(i1 ^ i2)
    begin
        if(i3 || i1)
            return i1;
        else
        begin
            if(i2 & i3)
                return i2;
            else
                return i3;
        end
    end
    else
    begin
        if(i3 && i2)
            return i2;
        else
        begin
            if(i1 - i3)
                return i1;
            else
                return i2;
        end
    end
endfunction

always_comb
begin
    d1.a = func(i1, i2, i3) ;
    d1.b = func(i1, ~i2, ~i3) ;
end

assign o1 = d1.a;

assign o2 = d1>>2;

endmodule

