
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure data-type and instantiates
// a module whose input port is of that type. Various arithmetic operations are performed
// on variables of this type.

typedef struct packed {
    logic [127:0] x1;
    logic [127:0] x2;
} tspdt;
module test(clk, i1, i2, o1, o2);

parameter WIDTH = 128;

input clk;
input signed [WIDTH-1:0] i1, i2;
output signed [WIDTH-1:0] o1, o2;

tspdt t1,t2;

always @(clk, i1, i2)
begin
        t1 = {(i2 << WIDTH/2) >>> WIDTH/2,~((i2 << WIDTH/2) >>> WIDTH/2)};
        t2 = {(i1 <<< WIDTH/2) >> WIDTH/2,~((i1 <<< WIDTH/2) >> WIDTH/2)};
end

bottom #(.WIDTH(WIDTH)) m1(~clk, t1, o1);
bottom #(.WIDTH(WIDTH)) m2(clk, t2, o2);

endmodule

module bottom(clk, i1, o1);

parameter WIDTH = 128;

input clk;
input tspdt i1;
output reg [WIDTH-1:0] o1;

reg [125:0] r1;
reg signed [126:0] r2;
reg signed [127:0] r3;

always @(posedge clk)
begin
        r1 = { i1.x1[WIDTH/2-1:0], i1.x1[WIDTH-1:WIDTH/2] };
        r2 = ((i1 << WIDTH/2) | (i1 >> WIDTH/2));
        r3 = i1 ^ ((i1 >> WIDTH/2) << WIDTH/2) + ((i1 << WIDTH/2) >> WIDTH/2) ^ i1;

        o1 = (r1 | r2) & r3;
end

endmodule

