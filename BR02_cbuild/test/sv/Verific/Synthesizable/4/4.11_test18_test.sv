
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): To declare a new structure type, typedef must be used, otherwise
// one can define an immediate variable of that structure. This design defines an
// unpacked structure with immediate variable and a typedef structure.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    struct {
         byte memb1;
         bit [8:0] memb2;
         logic [9:0] memb3;
    } st1;      // Immediate unnamed structure type variables

    typedef struct {
         byte memb1;
         bit [8:0] memb2;
         logic [9:0] memb3;
    } t_st;    // Named structure type

    t_st st2;  // Variable of named structure type

    always@(posedge clk)
    begin
        st1.memb2 = { 1'b0, in1 };
        st2.memb3 = { 1'b0, in2, 1'b1 };

        out1 = st1.memb1 ^ st1.memb2;
        out2 = st2.memb3 | st2.memb1;
    end

endmodule

