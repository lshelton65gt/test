
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. Two variables 
// of that type are declared. A task is defined having the output port type set to
// the new structure type. The task is enabled from an always_comb block to set the
// value of the data elements of the new type declared. These data elements are then
// used to set the value of the output port.

typedef struct packed {
    logic [3:0] a;
    logic [3:0] b;
    logic [3:0] c;
    logic [3:0] d;
} d_type_t;

module test(input i1, i2, input [3:0] i3, i4, output reg [7:0] o1, o2);

d_type_t d1, d2;

task task1(input [3:0] i1, i2, output d_type_t o1);
    o1.a = ((i1 + i2)<<2);
    o1.b = i2 - o1[15:12];
    o1.c = i2 - i1;
    o1.d = o1.a - o1.b;
endtask    

always_comb
begin
    task1(i1, i3, d1);
    task1(i2, i4, d2);
    o1 = d1[15:8] - d2[7:0];
    o2 = d1[7:0] - d2[15:8];
end

endmodule

