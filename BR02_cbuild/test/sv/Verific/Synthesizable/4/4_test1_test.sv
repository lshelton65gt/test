
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Time data type is a special data type; it is 64 bit integer
// of time steps. This design checks whether the tool can correctly assign and hold
// big integer numbers in the time type data element.

module test #(parameter width = 8)
             (input clk,
              input [width-1:0] in1, in2,
              output reg [0:width-1] out1, out2);

    time t1, t2;

    always@(posedge clk)
    begin
        t1 = 3^38;
        t2 = t1 + 2^18;

        if (t2 - 3^38 == 2^18)
        begin
            out1 = in1 + in2;
            out2 = in2 - in1;
        end
        else
            out2 = (out1 = '0);
    end

endmodule

