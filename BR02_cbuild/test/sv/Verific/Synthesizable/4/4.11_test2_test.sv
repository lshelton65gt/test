
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure. Top level module 
// contains one input and one output port of that structure type. These ports 
// are passed as actual to a module instantiation. The ports of instantiated 
// module are vectors with width equal to that of defined structure. This is
// achieved by using $bits in parameter value.

typedef struct packed {
    int c ;
    int d ;
} check ; 
 
module test(input check in, output check out) ;
    check ch = '{3,5} ;
    parameter int p = $bits(ch) ;
    bot #(p) b1(in, out) ;
endmodule

module bot #(parameter int n = 2) (input [n-1 : 0] in, output [n-1:0] out);
    parameter le = n ;
    bot1 #(le) b2(in, out) ;
endmodule

module bot1#(parameter p = 21) (input check in, output check out);
    function int cal(int p1) ;
        return p1* p1 ;
    endfunction

    assign out = in + cal(p) ;
endmodule

