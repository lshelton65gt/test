
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing whether packed union can be used as expression.

typedef union packed {
                       int a;
                       integer b;
                       bit [31:0] c;
                     } myType;

module test(input bit clk, output bit out);
    myType m ;

    always @ (posedge clk)
    begin
        m = 10 ;
        if(m.c == 32'b1010) out = 1'b1 ;
        else out = 1'b0 ;
    end
endmodule
