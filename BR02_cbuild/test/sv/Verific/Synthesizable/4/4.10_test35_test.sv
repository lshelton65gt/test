// verific updated this test twice, this is the latest version

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Enumeration can have a data type. This design defines an
// enumeration of type bit and with multiple packed dimensions. The first element
// is initialized with a 'char' type constant as char and bits are comparable with
// appropriate packed dimensions. This is then used afterwords.

module test (input clk,
             input [3:0] in1, in2,
             output reg [0:31] out1, out2);

    typedef enum bit [0:7] { A = "a", B, C, D, E, F, G, H,
                                  I, J, K, L, M, N, O, P } alphabet;

    always@(posedge clk)
    begin
        out1 = alphabet'(in1+A);
        out2 = alphabet'(in2+A);
    end

endmodule

