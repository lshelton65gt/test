
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If the data type of an enum declaration is 4-state value,
// then the values of the elements may contain x or z values. This design type
// uses integer as the data type when declaring the enum. As 'integer' type is
// 4-state so we assign X value to the enum literals.

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    enum integer { A = 'x, B = 0, C = 1 } enum_t;

    always@(posedge clk)
    begin
        enum_t = A;

        if (enum_t === 'x)
        begin
            out1 = in1 - in2;
            out2 = in2 - in1;
        end
        else
        begin
            out1 = '0;
            out2 = '0;
        end
    end

endmodule

