
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a packed structure type. Data elements
// of this type are declared of type logic. These data elements are assigned
// inside one always_comb block. Other two always_comb blocks are defined that
// set the value of the slices of the output port from that data elements.

typedef struct packed {
    logic [3:0] a;
    logic [3:0] b;
    logic [3:0] c;
    logic [3:0] d;
} d_type_t;

module test(input [7:0] i1, i2, i3, output reg signed [7:0] o1, o2);

d_type_t d1,d2;

always_comb
begin
    d1 = {i1,i2} ^ {i2,~i1};
    d2 = {i1,i3} ^ {i3,~i1};
end

always_comb
begin
    o1 [3:0] = d1.a + d2.b;
    o2 [3:0] = d1.b + d2.a;
end

always_comb
begin
    o1 [7:4] = d1.c - d2.d;
    o2 [7:4] = d1.d - d2.c;
end

endmodule

