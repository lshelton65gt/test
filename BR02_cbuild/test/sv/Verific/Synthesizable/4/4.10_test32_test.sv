
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses a for-generate loop. The initial and condition
// part of the loop uses enum literals - as the enum literals are treated as constants.

typedef enum { INVALID_MONTH, JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC } months;

module test (input clk,
             input [1:12] in1, in2,
             output reg [12:1] out1, out2);

    generate
    genvar i;
        for(i=JAN; i<=DEC; i++)
        begin: for_gen_i
            always@(posedge clk)
            begin
                out1[i] = in1[i] - in2[13-i];
                out2[13-i] = in2[i] ^ in1[13-i];
            end
        end
    endgenerate

endmodule

