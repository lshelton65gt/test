
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a new enum type with range. This type 
// is then used to type cast the value of some input ports in expressions of case
// statements. These case statements then sets values to the output ports depending
// on the case path.

typedef enum [1:0] {s0 = 2'b00, s1 = 2'b01, s2 = 2'b10, s3 = 2'b11} en_type_t;

module test(input [1:0] i1, i2, i3, i4, i5, i6, output reg [1:0] o1, o2);

task task1(input [1:0] i1, i2, i3, i4, i5, i6, output [1:0] o1, o2);
    begin
        case (en_type_t'(i5))
        s0 :
            begin
                o1 = i1 & i2;
                case (en_type_t'(i6))
                    s0 : o2 = i4 <= i3;
                    s1 : o2 = i3 ~^ i4;
                    s2 : o2 = i4 ^ i3;
                    s3 : o2 = i3 + i4;
                    default :;
                endcase
            end
        s1 :
            begin
                o1 = i2 < i1;
                case (en_type_t'(i6))
                    s0 : o2 = i3 < i4;
                    s1 : o2 = i4 + i3;
                    s2 : o2 = i3 >= i4;
                    s3 : o2 = i4 << i3;
                    default :;
                endcase
            end
        default :
             begin
                o1 = 0 ;
                o2 = 0 ;
             end
        endcase
    end
endtask

always_comb
        task1 (i1, i2, i3, i4, i5, i6, o1, o2);

endmodule

