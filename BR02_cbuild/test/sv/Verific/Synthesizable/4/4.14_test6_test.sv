
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test that signedness can be changed by type casting.

module test(output byte u_sum_out, s_sum_out,
            output reg s_less_out1, s_less_out2, u_less_out1, u_less_out2,
            input byte in1, in2) ;

    always @(in1, in2)
    begin
        s_sum_out = in1 + in2 ;
        u_sum_out = unsigned'(in1) + unsigned'(in2) ;
        u_less_out1 = unsigned'(in1) < 0;
        u_less_out2 = unsigned'(in2) < 0;
        s_less_out1 = in1 < 0;
        s_less_out2 = in2 < 0;
    end

endmodule

