
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two types having range. Variables (unpacked
// and packed) of this type are declared. These are then used inside an always block
// to set values into them. Another always block drives the output port by these data
// variables.

typedef logic [3:0] a_type_t;
typedef bit [1:0] b_type_t;

module test(input clk, reset, input [3:0] i1, i2, output reg [3:0] o1);

a_type_t r[3:0];
b_type_t sel;
int i;

assign sel=i1[1:0] ;

always @(posedge clk or negedge reset)
begin
    if(!reset)
        i = 0;
    else
    begin
        if(i++ >3)
            i = 0;
        else
        begin
            r[i] = i2;
        end
    end
end

always @(posedge clk)
    o1 = r[sel];

endmodule

