
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): To test packed signed - 4 state structure,
// where the structure is defined without using typedef.

struct packed signed { // Without using typedef
    int a ; 
    bit [15:0] b ;
    logic [7:0] c ; // 4 - state
    bit [7:0] d ;
} pack1 ; // signed, 4-state

module test(input  clk,
                  integer aa, //4 state
                  logic [15:0] bb, // 4 state
                  logic [7:0] cc, // 4 state
                  logic [7:0] dd, // 4 state
           output bit out) ;

    always @ (posedge clk)
    begin
       pack1.a = aa ;
       pack1.b = bb ;
       pack1.c = cc ;
       pack1.d = dd ;
    end

    always @ (negedge clk)
    begin
        // Since the structure is a packed structure
        if (pack1[63:32] === aa && pack1[31:16] === bb && pack1[15:8] === cc && pack1[7:0] === dd)
            out = 1'b1 ; // Test PASSED
        else
            out = 1'b0 ; // Test FAILED
    end

endmodule

