
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of packed unsigned structures. Check if the the structure
// is unsigned by default.

module test(output reg st_signed, in_signed, input int in) ;

    typedef struct packed {
       byte byte1 ;
       bit [15:0] bit_vec ;
       byte byte2 ;
    } my_struct ;

    my_struct un_st ;

    always @(in)
    begin
        un_st = in ;
        st_signed = un_st < 0 ;
        in_signed = in < 0 ;        
    end

endmodule
 
