
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): An example of a named port bundle.


interface simple_bus (input bit clk); // Define the interface

    logic req, gnt;
    logic [7:0] addr, data;
    logic [1:0] mode; 
    logic start, rdy, avail;
    modport slave (input req, addr, mode, start, clk, avail,
                   output gnt, rdy,
                          ref data);
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start,
                          ref data);

endinterface: simple_bus

module abcMod (simple_bus.slave a); // interface name and modport name

    always @(posedge a.clk) // the clk signal from the interface
        a.gnt <= a.req & a.avail; // the gnt and req signal in the interface
endmodule 

module xyzMod (simple_bus.master b);
    always @(posedge b.clk)
        b.data = ~b.data ;

endmodule 
 
module test(input logic clk) ;

    simple_bus sb_intf(clk); // Instantiate the interface

    abcMod abc(.a(sb_intf)); // Connect the interface to the module instance
    xyzMod xyz(.b(sb_intf));

endmodule
