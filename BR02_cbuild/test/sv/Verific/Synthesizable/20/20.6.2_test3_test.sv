
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface. Two modports are defined
// inside that interface. The interface also contains three tasks and functions.
// The modports just bundle those tasks and functions in them using import.
// These modports are used as a port in module declaration and the tasks and
// functions are invoked/called using that modport.

interface interface1;

    modport modport1(import task addr4(input logic [3:0] in1, in2,
                                             output logic [4:0] out),
                     import function [7:0] mult(input logic [3:0] in1, in2));

    modport modport2(import task  addr8(input logic [7:0] in1, in2,
                                  output logic [8:0] out),
                     import function [7:0] mult(input logic [3:0] in1, in2));

    task addr4(input logic [3:0] in1, in2,
               output logic [4:0] out);
        out = in1 + in2;
    endtask

    task addr8(input logic [7:0] in1, in2,
               output logic [8:0] out);
        out = in1 + in2;
    endtask

    function [7:0] mult(input logic [3:0] in1, in2);
        mult = in1 * in2;
    endfunction

endinterface : interface1

module test (interface1.modport1 inst, 
           input logic [3:0] in1, in2,
           output logic [4:0] addr_out,
           output logic [7:0] mult_out);

    always @(in1, in2)
    begin
        inst.addr4(in1, in2, addr_out);     
        mult_out = inst.mult(in1, in2);
    end

endmodule

module test1 (interface1.modport2 inst,
             input logic [3:0] in1, in2,
             logic [7:0] in3, in4,
             output logic [8:0] addr_out,
             logic [7:0] mult_out);

    always @(in1, in2, in3, in4)
    begin
        mult_out = inst.mult(in1, in2);
        inst.addr8(in3, in4, addr_out);
    end

endmodule

