
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface with two modports. Two
// modules are defined with generic interface type port. One single instance of
// interface is created and it is passed as actual to three instantiations
// specifying different modport names.

interface simple_bus #(parameter AWIDTH = 16, DWIDTH = 1 )(input bit clk); // Define the interface

    logic req, gnt;
    logic [AWIDTH:0] addr, data1;
    logic [DWIDTH:0] mode; 
    logic start, rdy, avail;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data1);
    modport master(input gnt, rdy, clk, avail,
                   output req, addr, mode, start,
                          ref data1);

endinterface: simple_bus

module abcMod (interface a); // uses just the interface

    always @(posedge a.clk) // the clk signal from the interface
        a.gnt <= a.req & a.avail; // the gnt and req signal in the interface
endmodule 

module xyzMod (interface b);
    always @(posedge b.clk)
        b.data1 = ~b.data1 ;

endmodule 

module test (input logic clk);

    simple_bus #(.AWIDTH(7), .DWIDTH(3)) sb_intf(clk); // Instantiate the interface

    abcMod abc1(.a(sb_intf.master)); // Connect the interface to the module instance

    xyzMod xyz3(sb_intf.slave);

endmodule 

