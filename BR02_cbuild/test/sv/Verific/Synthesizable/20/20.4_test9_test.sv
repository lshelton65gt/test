
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Nested interface can be declared in System Verilog. This
// design define a nested interface and use that in modport of outer interface.

interface intf ;

    wire x, y ;

    interface nest_intf ;
        wire a, b, c, d ;
        modport master(input a, b, output c, d) ;
        modport slave(output a, b, input c, d) ;
    endinterface : nest_intf 

    nest_intf ch1(), ch2() ;
    modport master2 (ch1.master, ch2.master, input x, output y) ;

endinterface : intf

module module1(intf i) ;

    assign i.ch1.c = ~i.ch2.a ;
    assign i.ch1.d = ~i.ch2.b ;

    assign i.ch2.c = ~i.ch1.a ;
    assign i.ch2.d = ~i.ch1.b ;

    assign i.x = ~i.y ;

endmodule

module test ;

    intf i() ;
    module1 mod1(.i(i.master2)) ;

endmodule

