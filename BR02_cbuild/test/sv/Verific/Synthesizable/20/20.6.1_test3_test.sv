
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface. Two tasks are defined
// inside the interface. It also defines two modules that use this interface as
// their port types. The module calls the tasks from inside the interface.

interface interface1;

    task addr(input logic [3:0] in1, in2,
              output logic [4:0] out);
        out = in1 + in2;
    endtask

    task mult(input logic [3:0] in1, in2,
              output logic [7:0] out);
        out = in1 * in2;
    endtask

endinterface : interface1

module test (interface1 inst, 
           input logic [3:0] in1, in2,
           output logic [4:0] addr_out,
           output logic [7:0] mult_out);

    always @(in1, in2)
    begin
        inst.addr(in1, in2, addr_out);     
        inst.mult(in1, in2, mult_out);
    end

endmodule

module test1 (interface1 inst,
             input logic [3:0] in1, in2,
             output logic [7:0] mult_out);

    always @(in1, in2)
        inst.mult(in1, in2, mult_out);

endmodule

