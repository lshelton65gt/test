
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Interfaces are commonly used as a bundle of variables or nets.
// When an interface is used as a port, the variables and nets in it are assumed to be
// inout ports. This design defines an interface and uses its elements as inout ports.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    interface ifc(input clk);
        parameter w = 8;

        reg [0:w-1] i1;
        reg [w-1:0] o1;
    endinterface: ifc

    ifc if1(clk);

    bot bot_1(if1);

    always@(negedge clk)
    begin
        if1.i1 = ~in1;  /* treat both as input ports */
        if1.o1 = -in2;
    end

    always@(posedge clk)
    begin
        out1 = -if1.i1;  /* treat both as output ports */
        out2 = ~if1.o1;
    end

    module bot(ifc if1);
        parameter w = 8;

        reg [0:w-1] r1, r2;

        always@(posedge if1.clk)
        begin
            r1 = if1.i1 | if1.o1;  /* treat both as inout ports */
            r2 = if1.o1 - if1.i1;
        end

    endmodule

endmodule

