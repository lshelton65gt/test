
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE
  
// TESTING FEATURE(S): This design defines parameterized interface with two modport
// definitions. This interface along with modport specification is passed as actual
// to module instantiations.
 
interface simple_bus #(parameter p1 = 4, p2 = 1)(input bit clk) ;

    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic [p2-1:0] mode ;
    logic start, rdy ;
    logic avail ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data) ;
    modport master(input gnt, rdy, clk, avail,
                   output req, addr, mode, start,
                          ref data) ;

endinterface: simple_bus

module abcMod (interface a) ; // interface name and modport name

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & a.avail; // the gnt and req signal in the int

endmodule

module xyzMod (interface b) ;

    always @(posedge b.clk)
        b.gnt = b.start & b.req ;

endmodule


module test(input logic clk);

    simple_bus #(.p1(16),.p2(3)) sb_intf1(clk) ;
    simple_bus #(8, 2) sb_intf2(clk) ;
    
    abcMod abc1(sb_intf1.master) ;
    abcMod abc2(sb_intf2.master) ;

    xyzMod xyz1(sb_intf1.slave) ;
    xyzMod xyz2(sb_intf2.slave) ;

endmodule 

