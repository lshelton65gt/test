
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface. Two functions are defined
// inside the interface. It also defines two modules that uses this interface as
// the type of their ports. The module calls the functions from inside the interface.

interface interface1;

    function [4:0] addr(input [3:0] in1, in2);
        addr = in1 + in2;
    endfunction

    function mult(input logic [3:0] in1, in2);
        mult = in1 * in2;
    endfunction

endinterface : interface1

module test (interface1 inst, 
           input logic [3:0] in1, in2,
           output logic [4:0] addr_out,
           output logic [7:0] mult_out);


    always @(in1, in2)
    begin
        addr_out = inst.addr(in1, in2);     
        mult_out = inst.mult(in1, in2);
    end

endmodule

module test1 (interface1 inst,
             input logic [3:0] in1, in2,
             output logic [7:0] mult_out);

    always @(in1, in2)
        mult_out = inst.mult(in1, in2);
   
endmodule

