
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Nested Interface declared with modports. This interfaces are 
// instantiated in module port connections and then used within an always_ff and an always block.

// NOTE:
//  LRM is not very clear about whether we can access the clk through instantiation of
//  a modport (masterTop or slaveTop) of interface i1, since the modports do not include
//  the clk in their port lists. The same logic applies to the local instantiations of other
//  other interfaces and their items.

interface i1(input clk);
    interface i2;
        wire a, b, c, d;
        modport master(input a, b, clk,  output c, d);
        modport slave(input c, d, clk, output a, b);
    endinterface
    i2 inter1(), inter2();
    modport masterTop(inter1.master, inter2.master);
    modport slaveTop(inter1.slave);
endinterface

module mod1(i1.masterTop i);

always_ff @(posedge i.clk)
begin
    if(i.inter1.a)
    begin
        i.inter1.c = 1 ;
        i.inter1.d = 1 ;
    end
    else if(!i.inter1.b)
    begin
        i.inter1.c = 0 ;
        i.inter1.d = 0 ;
    end
    else
    begin
         i.inter2.c = i.inter1.a;
         i.inter2.d = i.inter1.b;
    end
end
endmodule

module mod2(interface a);

always @(posedge a.inter1.clk or posedge a.inter1.c or negedge a.inter1.d)
begin
    if(a.inter1.c)
    begin
        a.inter1.a = mod1.i.inter2.c;
        a.inter1.b = mod1.i.inter2.d;
    end
    else if(!a.inter1.d)
    begin
        a.inter1.a = 0 ;
        a.inter1.b = 0 ;
    end
    else
    begin
        a.inter1.a = a.inter1.c;
        a.inter1.a = a.inter1.d;
    end
end
endmodule

module test;
logic clk = 0;

initial
    repeat (10) clk = ~clk;

i1 i(clk);

mod1 instanMod1(i.masterTop);
mod2 instanMod2(i.slaveTop);
endmodule

