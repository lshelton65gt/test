
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a module and an interface. A modport is
// declared inside the interface. One of the module port is of
// type of the declared interface. This module when instantiated
// the interface port is connected to the modport of that instantiation.

interface vars_and_nets;
    bit [4:0] addr_out;
    bit [3:0] in1, in2;
    bit [7:0] mult_out;
    bit greater_than_out, less_than_out;

    modport dir_spec(input in1, in2, 
                     output addr_out, mult_out, 
                     greater_than_out, less_than_out);
endinterface

module test(vars_and_nets var1, input bit clk);

assign var1.addr_out = var1.in1 + var1.in2;

always @(posedge clk)
begin
    var1.mult_out = var1.in1 * var1.in2;
    var1.greater_than_out = var1.in1 > var1.in2;
    var1.less_than_out = var1.in1 < var1.in2;
end
endmodule
