
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface. Two modports are defined
// inside that interface. The interface also contains three functions. The modports
// just bundle those functions in them using import. These modports are used as a
// port in module declaration and functions are called using that modport.

interface interface1;

    modport modport1(import function [4:0] addr4(input logic [3:0] in1, in2),
                     import function [7:0] mult(input logic [3:0] in1, in2));

    modport modport2(import function [8:0] addr8(input logic [7:0] in1, in2),
                     import function [7:0] mult(input logic [3:0] in1, in2));

    function [4:0] addr4(input logic [3:0] in1, in2);
        addr4 = in1 + in2;
    endfunction

    function [8:0] addr8(input logic [7:0] in1, in2);
        addr8 = in1 + in2;
    endfunction

    function [7:0] mult(input logic [3:0] in1, in2);
        mult = in1 * in2;
    endfunction

endinterface : interface1

module test (interface1.modport1 inst, 
           input  logic [3:0] in1, in2,
           output logic [4:0] addr_out,
           output logic [7:0] mult_out);

    always @(in1, in2)
    begin
        addr_out = inst.addr4(in1, in2);     
        mult_out = inst.mult(in1, in2);
    end

endmodule

module test1 (interface1.modport2 inst,
             input logic [3:0] in1, in2,
             logic [7:0] in3, in4,
             output logic [8:0] addr_out,
             logic [7:0] mult_out);

    always @(in1, in2, in3, in4)
    begin
        mult_out = inst.mult(in1, in2);
        addr_out = inst.addr8(in3, in4);
    end    
    
endmodule

