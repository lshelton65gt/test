interface ifc (clk) ;
    parameter p1 = 4 ;
    input clk ;
    logic [p1-1:0] in ;
    logic [p1-1:0] out ;
endinterface: ifc

module test (ifc i) ;
    parameter p1 = 16 ;
    reg [p1-1:0] data ;

    assign data = i.in ;
    assign i.out = data ;

endmodule

module top (clk, i1, o1) ;
    parameter p1 = 8 ;
    input clk ;
    input [p1-1:0] i1 ;
    output [p1-1:0] o1 ;

    ifc #(p1) ports1 (clk) ;
    test #(p1) t1 (ports1) ;

    assign o1 = ports1.out ;
    assign ports1.in = i1 ;

endmodule

