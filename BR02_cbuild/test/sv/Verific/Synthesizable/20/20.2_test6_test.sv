
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface with an input port. This
// interface is instantiated inside a module which also instantiates two modules
// that have port of that type of interface. The output port is directly connected
// to these instantiations.

interface iface(input clk);
    logic [3:0] a;
    logic [1:0] b;
    logic [1:0] c;
endinterface: iface

module test(input clk, input [3:0] i1, i2, output [3:0] o1, o2);

iface iface1(clk);

bottom1 b1(.a(iface1),.i1(i1), .o1(o1));
bottom2 b2(.b(iface1),.i1(i2), .o1(o2));

endmodule

module bottom1(iface a, input [3:0] i1, output reg [3:0] o1);
always @(posedge a.clk)
begin
    if(i1[0])
    begin
        a.a = i1 + 2'b01;
        a.b = ~(i1[3:2]) + 2'b01;
        a.c = ~(i1[1:0]) + 2'sb11;
    end

end

endmodule

module bottom2(iface b, input [3:0] i1, output reg [3:0] o1);

always @(posedge b.clk)
begin
    if(b.c &  1'bz )
        o1 = i1;
    else
    begin
        if(i1 ~^  1'bx )
            o1 = b.a ^ i1;
        else
            o1 = b.b + i1;
    end

end

endmodule

