
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): It is an error to instantiate a module multiple times that
// exports a task. In this case 'extern forkjoin' is used in the interface to define
// the 'tasks' to be exported. This design uses this to export multiple tasks.

interface multiple_task_export(input bit clk);

logic [7:0] data;

extern forkjoin task read(input logic [7:0] data);
extern forkjoin task write(output logic [7:0] data);

modport modport1(export read,
                        write, output data);

modport modport2(import task read(input logic [7:0] data),
                        task  write(output logic [7:0] data), input clk);

endinterface

module test(multiple_task_export.modport1 a);

task a.read(input logic [7:0] data);
     a.data = data;
endtask

task a.write(output logic [7:0] data);
    data = a.data;
endtask

endmodule

module test1(multiple_task_export.modport2 a,
            input [7:0] data,
            output reg [7:0] out_data);

bit mode;
initial
    mode = 1;
    
always @(posedge a.clk)
begin
    if(mode)
        a.read(data);
    else
        a.write(out_data);
    mode++;    
end        

endmodule

