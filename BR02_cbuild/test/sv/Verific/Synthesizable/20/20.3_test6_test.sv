
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Interfaces can have ports. The direction of the ports can
// be specified. This design defines an interface with ansi ports of input direction.

interface ifc(input clk); /* clk is an ansi input port */
    parameter w = 8;

    reg [0:w-1] i1;
    reg [w-1:0] o1;
endinterface: ifc

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    ifc if1(clk);
    ifc if2(clk);

    bot bot_1(if1);
    bot bot_2(if2);

    always@(negedge clk)
    begin
        if1.i1 = ~in1 | in2;
        if2.i1 = -in2 ^ in1;
    end

    always@(posedge clk)
    begin
        out1 = -if1.o1 - if2.o1;
        out2 = ~if2.o1 + if1.o1;
    end

    module bot(ifc if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = ~if1.i1 + 1;
        end

    endmodule

endmodule

