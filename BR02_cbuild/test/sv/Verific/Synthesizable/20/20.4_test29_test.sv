
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE
  
// TESTING FEATURE(S): This design defines a parameterized interface with two modport
// constructs. This interface is used as port in two modules, but the modport is 
// specified in module instantiations.

interface simple_bus #(parameter p1 = 8, p2 = 2)(input bit clk) ;

    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic [p2-1:0] mode ;
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start,
                          ref data) ;

endinterface: simple_bus

module abcMod (simple_bus a, input logic avail) ; // interface name and modport name

    always @(posedge a.clk) // the clk signal from the interface
    begin
        if (a.rdy) 
        begin
            a.start = 1 ;
            a.data = 1 ;
            a.mode = 0 ;
            a.addr = ~a.data ;
        end
        else
        begin
            a.req <= a.gnt & avail; // the gnt and req signal in the int
            a.addr <= a.data ;
        end
    end

endmodule

module xyzMod (simple_bus b) ;

    always @(posedge b.clk)
    begin
        if (b.start) 
        begin
            b.rdy = 0 ;
        end
        else
        begin
            b.gnt = b.start & b.req ;
            b.rdy = 1 ;
        end
    end

endmodule

module test(input logic clk, input logic avail) ;

    simple_bus #(.p1(16), .p2(3)) sb_intf1(clk) ;
    simple_bus sb_intf2(clk) ;

    abcMod abc11(sb_intf1.master, avail) ;

    xyzMod xyz11(sb_intf1.slave) ;

endmodule 

