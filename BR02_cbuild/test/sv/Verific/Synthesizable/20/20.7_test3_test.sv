
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Parameters can be defined for interfaces also, in the same
// way as we define parameters for modules. This design defines an interface with
// parameter and while instantiating it, overrides the parameter value.

interface ifc #(parameter w = 4, parameter type t = logic)
               (input clk);
    t [0:w-1] i1;
    t [w-1:0] o1;

    modport mp1(input i1, input clk, output o1);
endinterface: ifc

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    reg out;
    ifc #(.w(w), .t(bit)) if1 (clk), if2 (clk);

    bot bot_1(if1.mp1);
    bot bot_2(if2.mp1);

    always@(negedge clk)
    begin
        if1.i1 = ~in1 | in2;
        if2.i1 = -in2 ^ in1;
        out = |(in1 ^ in2);
    end

    always@(posedge clk)
    begin
        out1 = if1.o1 | if2.o1;
        out2 = if2.o1 - if1.o1;
    end

    module bot(ifc if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = ~if1.i1 + 1;
        end

    endmodule

endmodule

