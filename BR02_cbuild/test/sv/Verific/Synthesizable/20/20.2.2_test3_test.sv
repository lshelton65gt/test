
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If the name of the connecting and connected ports of an
// instantiation are same and they are of same interface type, then implicit .name
// or .* port connection is  legal. This design defines an interface and uses the
// same name and type as the module port and the module is instantiated using .name
// port connection style.

interface ifc(input clk);
    parameter w = 8;

    reg [0:w-1] i1;
    reg [w-1:0] o1;
endinterface: ifc

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    ifc if1(clk);

    bot bot_1(.if1); /* .name style port connection */

    always@(negedge clk)
    begin
        if1.i1 = -in1;
        if1.o1 = ~in2;
    end

    always@(posedge clk)
    begin
        out1 = ~if1.i1;
        out1 = -if1.o1;
    end

    module bot(ifc if1);
        parameter w = 8;

        reg [0:w-1] r1, r2;

        always@(posedge if1.clk)
        begin
            r1 = if1.i1 ^ if1.o1;
            r2 = if1.o1 & if1.i1;

            if1.o1 = -(~r1 | r2);
            if1.i1 = ~(r1 * -r2);
        end

    endmodule

endmodule

