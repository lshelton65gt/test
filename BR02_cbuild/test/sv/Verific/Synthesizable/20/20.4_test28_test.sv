
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE
  
// TESTING FEATURE(S): This design defines parameterized interface with two modport
// definitions. This is used as port in two modules and modport is specified in
// module instantiations.

interface simple_bus #(parameter p1 = 8, p2 = 2)(input bit clk) ; 

    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic start, rdy, avail ;
    modport slave (input req, addr, start, clk,
                   output gnt, rdy,
                          ref data) ;
    modport master(input gnt, rdy, clk, avail,
                   output req, addr, start,
                          ref data) ;

endinterface: simple_bus

module abcMod (simple_bus a) ; // interface name and modport name

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & a.avail; // the gnt and req signal in the int

endmodule

module xyzMod (simple_bus b) ;

    always @(posedge b.clk)
        b.gnt = b.start & b.req ;

endmodule

module test(input logic clk);

    simple_bus #(.p1(16), .p2(3)) sb_intf1(clk) ;
    simple_bus sb_intf2 (clk) ;

    abcMod abc11(sb_intf1.master) ;
    abcMod abc12(sb_intf2.master) ;

    xyzMod xyz11(sb_intf1.slave) ;
    xyzMod xyz12(sb_intf2.slave) ;

endmodule 

