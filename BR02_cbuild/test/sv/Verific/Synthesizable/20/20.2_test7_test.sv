
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface with parameters and input
// port. The interface is instantiated in a module and connected to instantiations
// of other two modules that have ports of that type of interface. The local data
// elements that are connected to the output port of the instantiations are then
// assigned to the output port of the local module.

interface iface #(parameter W1 = 4,parameter W2 = 2)(input clk);
    logic [W1-1:0] a;
    logic [W1-1:0] b;
    logic [W2-1:0] c;
    logic [W2-1:0] d;
endinterface: iface

module test #(parameter W1 = 4,parameter W2 = 2)(input clk,input [3:0] i1, i2,output reg [3:0] o1, o2);

logic [W1 - 1:0] r1, r2;

iface #(.W1(W1), .W2(W2)) if1(clk);

bottom1 #(.W(W1)) b1(if1, r1);
bottom2 #(.W(W1)) b2(if1, r2);

always@(negedge clk)
begin
    if1.a = i1;
    if1.b = i2;
    if1.c = i1[3:2] - i2[3:2];
    if1.d = i2[1:0] - i1[1:0];
end

always @(posedge clk)
begin
    o1 = r1 + r2;
    o2 = r1 - r2;
end
 
endmodule


module bottom1 #(parameter W = 4)(iface a,output reg [W - 1:0] o1);

always @(a.clk)
begin
    if(a.c[0] || a.d[0])
        o1 = a.c - a.b;
    else
        o1 = a.a - a.d;
end

endmodule


module bottom2 #(parameter W = 4)(iface b,output reg [W - 1:0] o1);

bit [W-1:0] r1, r2;

always_comb
begin
    r1 = b.a;
    r2 = b.b;
end
always @(posedge b.clk)
begin
    if(r1[0] && r2[0])
        o1 = b.c - b.d;
    else
        o1 = b.a | b.b;
end

endmodule

