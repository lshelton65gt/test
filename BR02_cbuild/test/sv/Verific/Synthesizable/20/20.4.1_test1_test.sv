
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface and modports inside the
// interface. It uses the interface name and modport name in the module to declare
// a port. When instantiating the module, it connects a modport to the modport in
// the instantiated module.

interface iface(input clk);
    reg [0:7] i1, i2;
    reg [7:0] o1, o2;

    modport mp1(input i1, i2, output o1);
    modport mp2(input i2, i1, output o2);
endinterface

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    iface if1(clk);
    iface if2(clk);

    bot1 bot_1(.if1(if1));
    bot2 bot_2(.if2(if2));

    always@(negedge clk)
    begin
        if1.i1 = in1 + in2;
        if2.i1 = in2 - in1;
        if1.i2 = in1 & in2;
        if2.i2 = in2 | in1;
    end

    always@(posedge clk)
    begin
        out1 = if1.o1 * if2.o2;
        out2 = if2.o2 ^ if1.o1;
    end

    module bot1(iface.mp1 if1);

        always@(*) 
        begin
            if1.o1 = ~(if1.i2 - if1.i1);
        end

    endmodule

    module bot2(iface.mp2 if2);

        always@(*)
        begin
            if2.o2 = if2.i1 - if2.i2;
        end

    endmodule

endmodule

