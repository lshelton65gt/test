
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Interfaces can be nested, ie. an interface declaration can have
// another interface declared inside it. This design defines such a nested interface.

interface ifc(input clk);
    interface nested(input clk);  // interface inside interface
        logic [7:0] memb1;
        logic [7:0] memb2;
        logic [7:0] memb3;
    endinterface: nested

    logic [7:0] memb1;
    nested memb2(clk);    // member of type interface declared nested inside this interface itself
    logic [7:0] memb3;
endinterface: ifc

module test (input clk,
             input [7:0] in1, in2,
             output reg [0:7] out1, out2);

    ifc if1(clk);

    bot bot_1(if1);

    always@(negedge clk)
    begin
        if1.memb2.memb1 = in1 - in2;
        if1.memb2.memb2 = in2 - in1;
        if1.memb2.memb3 = in1 ^ in2;
    end

    always@(posedge clk)
    begin
        out1 = if1.memb1 - if1.memb3;
        out2 = if1.memb3 ^ if1.memb1;
    end

    module bot(ifc if1);

        reg [0:7] r1, r2;

        always@(posedge if1.memb2.clk /* indirectly using the clock */)
        begin
            r1 = if1.memb2.memb1 & if1.memb2.memb3;
            r2 = if1.memb2.memb2 + if1.memb2.memb1;

            if1.memb1 = r1 ^ r2;
            if1.memb3 = r2 - r1;
        end

    endmodule

endmodule

