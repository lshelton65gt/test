
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface and a modport declaration
// inside that. It also defines a module. One of the ports of a module is an
// hierarchical modport element of a generic interface. This module is instantiated
// with the port connected to an instantiation of the declared interface.

interface vars_and_nets;
    logic [4:0] addr_out;
    logic [3:0] in1, in2;
    logic [7:0] mult_out;
    bit greater_than_out, less_than_out;

    modport dir_spec(input in1, in2, 
                     output addr_out, mult_out, 
                     greater_than_out, less_than_out);
endinterface

module test(interface.dir_spec var1, input bit clk);

    assign var1.addr_out = var1.in1 + var1.in2;

    always @(posedge clk)
    begin
        var1.mult_out = var1.in1 * var1.in2;
        var1.greater_than_out = var1.in1 > var1.in2;
        var1.less_than_out = var1.in1 < var1.in2;
    end
endmodule

