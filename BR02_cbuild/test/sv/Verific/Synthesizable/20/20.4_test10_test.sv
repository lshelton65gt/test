
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE
  
// TESTING FEATURE(S): This design tests parameterized interface with modports. 
 

interface simple_bus #(p1 = 8, p2 = 2)(input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic [p2-1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start,
                          ref data) ;

endinterface: simple_bus

module abcMod (interface a) ; // interface name and modport name

    parameter p1 = 8 ;
    bit [p1-1:0] avail1 ;
    bit [p1-1:0] avail2 ; 
    always @(posedge a.clk) // the clk signal from the interface
        a.addr <= avail1 + avail2 ;
    always @(a.addr)
    begin
        avail1 = a.addr ;
        avail2 = a.addr ;
    end
    initial
        $display("It's in abcMod\n The value of the parameter is %d",p1) ;

endmodule 

module xyzMod (interface b) ;

    parameter p1 = 1, p2 = 1 ;
    always @(posedge b.clk)
        b.gnt = p1 & p2 ;
    initial
        $display("It's in xyzMod\n The values of the parameters are  p1 = %d and p2 = %d", p1, p2) ;

endmodule 

module test (input clk) ;

    simple_bus #(.p1(16), .p2(3)) sb_intf1(clk) ;
    simple_bus #(10,3) sb_intf2(clk) ;

    abcMod #(16) abc1(sb_intf1.master) ;
    abcMod #(10) abc2(sb_intf2.master) ; 

    xyzMod #(1,0) xyz1(sb_intf1.slave) ;
    xyzMod #(1,0) xyz2(sb_intf2.slave) ;

endmodule 

