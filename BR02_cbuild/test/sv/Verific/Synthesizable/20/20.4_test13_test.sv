
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE
  
// TESTING FEATURE(S): This design defines an interface with modports. Two modules
// are also defined having interface type port with specific modport. The interface
// is instantiated twice and each instance is connected with two modules.

interface simple_bus (input bit clk); // Define the interface

    logic req, gnt;
    logic [7:0] addr, data;
    logic [1:0] mode; 
    logic start, rdy, avail;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data);
    modport master(input gnt, rdy, clk, avail,
                   output req, addr, mode, start,
                          ref data);

endinterface: simple_bus

module abcMod (simple_bus.master a); // interface name and modport name

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & a.avail; // the gnt and req signal in the interface
endmodule 

module xyzMod (simple_bus.slave b);
    always @(posedge b.clk)
        b.data = ~b.data ;

endmodule 

module test(input logic clk) ;

    simple_bus sb_intf1(clk); // 1st instantiate the interface
    simple_bus sb_intf2(clk); // 2nd instantiate the interface

    abcMod abc1(.a(sb_intf1)); // Connect the interface to the module instance
    abcMod abc2(sb_intf2);

    xyzMod xyz1(.b(sb_intf1));
    xyzMod xyz2(sb_intf2);

endmodule

