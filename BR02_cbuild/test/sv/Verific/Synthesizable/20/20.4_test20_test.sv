
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a parameterized interface with two 
// modports. Two parameterized modules are defined with generic interface type port.
// During instantiations of these modules different parameter values and interface
// instances are provided.

interface simple_bus #(parameter p1 = 8, p2 = 2)(input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic [p2-1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy, ref data) ;
    modport master(input gnt, rdy, clk,
                   output req, addr, mode, start,
                          ref data);
endinterface: simple_bus

module abcMod (interface a) ; // interface name and modport name

    parameter p1 = 8 ;
    bit [p1-1:0] avail1 ;
    bit [p1-1:0] avail2 ; 
    always @(posedge a.clk) // the clk signal from the interface
        a.addr <= avail1 + avail2 ;
    always @(a.addr)
    begin
        avail1 = a.addr ;
        avail2 = a.addr ;
    end

endmodule 

module xyzMod (interface b) ;

    parameter p1 = 1, p2 = 1 ;
    always @(posedge b.clk)
        b.gnt = p1 & p2 ;

endmodule 

module test(input logic clk) ;

    simple_bus #(.p1(16), .p2(3)) sb_intf1(clk) ;
    simple_bus #(10,3) sb_intf2(clk) ;

    abcMod #(16) abc11(sb_intf1.master) ;
    abcMod #(10) abc12(sb_intf2.master) ;

    xyzMod #(1,0) xyz11(sb_intf1.slave) ;
    xyzMod #(1,0) xyz12(sb_intf2.slave) ;

endmodule 

