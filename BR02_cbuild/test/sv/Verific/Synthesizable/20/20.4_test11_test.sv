
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): All of the names used in a modport declaration 
//             shall be declared by the same interface as is the 
//             modport itself.

interface intf ;

    wire x, y ;

    interface nest_intf ;
        wire a, b, c, d ;
        modport master(input a, b, output c, d) ;
        modport slave(output a, b, input c, d) ;
    endinterface : nest_intf 

    nest_intf ch1(), ch2() ;
    modport master2 (ch1.master, ch2.slave, input x, output y) ;

endinterface : intf

module module1(intf i) ;

    assign i.ch2.a = ~i.ch1.a ;
    assign i.ch2.b = ~i.ch1.b ;

    assign i.ch1.c = ~i.ch2.c ;
    assign i.ch1.d = ~i.ch2.d ;

    assign i.x = ~i.y ;

    initial 
        $display("Instantiating module module1.\n") ;

endmodule

module test ;
    
    intf i() ;
    module1 mod1(.i(i.master2)) ;
        
endmodule

