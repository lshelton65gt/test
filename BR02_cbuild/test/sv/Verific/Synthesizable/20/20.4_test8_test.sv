
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE
  
// TESTING FEATURE(S): This design tests use of generic interface port along with 
// modport name.


interface simple_bus1 (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [7:0] addr, data, avail1, avail2 ;
    logic [1:0] mode ; 
    logic start, rdy ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data) ;
    modport master(input gnt, rdy, clk, avail1, avail2,
                   output req, addr, mode, start,
                          ref data) ;

endinterface: simple_bus1

interface simple_bus2 (input bit clk) ; // Define the interface

    logic req1, gnt1 ;
    logic [7:0] addr1, data1 ;
    logic [1:0] mode1 ; 
    logic start1, rdy1 ;
    modport slave (input req1, addr1, mode1, start1, clk,
                   output gnt1, rdy1,
                          ref data1) ;
    modport master(input gnt1, rdy1, clk,
                   output req1, addr1, mode1, start1,
                          ref data1) ;

endinterface: simple_bus2

interface simple_bus3 (input bit clk) ; // Define the interface

    logic req2, gnt2 ;
    logic [7:0] addr2, data2 ;
    logic [1:0] mode2 ;
    logic start2, rdy2 ;
    modport connection1(input req2, addr2, mode2, start2, clk,
                   output gnt2, rdy2,
                          ref data2) ;
    modport connection2(input gnt2, rdy2, clk,
                   output req2, addr2, mode2, start2,
                          ref data2) ;

endinterface: simple_bus3

interface simple_bus4 (input bit clk) ; // Define the interface

    logic req3, gnt3 ;
    logic [7:0] addr3, data3 ;
    logic [1:0] mode3 ;
    logic start3, rdy3 ;
    modport connection1(input req3, addr3, mode3, start3, clk,
                   output gnt3, rdy3,
                          ref data3) ;
    modport connection2(input gnt3, rdy3, clk,
                   output req3, addr3, mode3, start3,
                          ref data3) ;

endinterface: simple_bus4

module defMod (interface.connection1 d, interface.connection2 e) ;

    always @(posedge d.clk)
        d.data2 = ~e.data3 ;

endmodule

module abcMod (interface.master a) ; // interface name and modport name

    always @(posedge a.clk) // the clk signal from the interface
        a.addr <= a.avail1 + a.avail2 ; // the gnt and req signal in the interface

endmodule

module xyzMod (interface.slave b) ;

    parameter p1 = 1, p2 = 1 ;
    always @(posedge b.clk)
        b.gnt1 = p1 & p2 ;

endmodule

module test(input clk) ;

    simple_bus1 sb_intf1(clk) ; // Instantiate the interface
    simple_bus1 sb_intf2(clk) ; 

    simple_bus2 sb_intf3(clk) ;
    simple_bus2 sb_intf4(clk) ;
    
    abcMod #(10,20) abc11(sb_intf1) ;
    abcMod #(10,20) abc12(sb_intf2) ;

    xyzMod #(1,0) xyz11(sb_intf3) ;
    xyzMod #(1,0) xyz12(sb_intf4) ;

    simple_bus3 sb_con1(clk) ; // Instantiate the interface
    simple_bus4 sb_con2(clk) ;
    defMod def(sb_con1, sb_con2) ;

endmodule 

