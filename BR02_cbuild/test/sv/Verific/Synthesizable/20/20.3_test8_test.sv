
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Interfaces can have ports. The direction of the
// ports can be specified. This design defines an interface with ansi
// ports of inout direction.

interface ifc(input clk, inout wire io); /* clk is an ansi input port, io is an ansi inout port */
    parameter w = 8;

    reg [0:w-1] i1;
    reg [w-1:0] o1;
endinterface: ifc

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    logic out;
    wire wire_out;

    ifc if1(clk, wire_out);
    ifc if2(clk, wire_out);
    assign wire_out = out;

    bot bot_1(if1);
    bot bot_2(if2);

    always@(negedge clk)
    begin
        if1.i1 = ~in1 | in2;
        if2.i1 = -in2 ^ in1;
        out = |(in1 ^ in2);
    end

    always@(posedge clk)
    begin
        if (1'b1 == out)
        begin
            out1 = if1.o1 | if2.o1;
            out2 = if2.o1 - if1.o1;
        end
        else
        begin
            out1 = if1.o1 * if2.o1;
            out2 = if2.o1 & if1.o1;
        end
    end

    module bot(ifc if1);

        always@(posedge if1.clk)
        begin
            if (if1.io)
                if1.o1 = ~if1.i1 + 1;
            else
                if1.o1 = -if1.i1;
        end

    endmodule

endmodule

