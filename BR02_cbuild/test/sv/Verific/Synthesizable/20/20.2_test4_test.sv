
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface. The interface has a input
// port. Two other modules are defined which have interface type port. These modules
// are instantiated with interface instantiation.

interface iface(input clk);
   logic [7:0] c1; 
   logic [3:0] c2; 
   logic [7:0] c3; 
   logic [3:0] c4; 
endinterface: iface

module test (clk, i1, i2, o1, o2);
input clk;
input signed [15:0] i1;
input signed [0:15] i2;
output signed [0:7] o1;
output signed [0:15] o2;

iface iface1(clk);

bottom1 b11(i1,iface1,o1);
bottom2 b12(i2,iface1,o2);

endmodule

module bottom1(input signed [15:0] i1, iface ifa, output reg signed [0:7] out);

parameter val = 4'b1101;
parameter l = 0, r = 3;
parameter signed [l:r] indx = val;

reg signed [7:0] t1, t2;
reg signed [0:15] t3, t4;

always @(posedge ifa.clk)
begin
        t1 = ifa.c1 ^ ifa.c2;
        t2 = t1 | i1[15:8] ;

        t3 = { t1, t2 };
        t4 = t3 - t2*2'b10;
        ifa.c3 = t3 - t2;
        ifa.c4 = t4 - t1;

        out = (( t3 == t4) ? (ifa.c1 | t3 - t4 & i1):(i1 ^ t4 + ifa.c2 | t3));
end
endmodule

module bottom2(input signed [15:0] i1, iface ifb, output reg signed [0:15] out);

parameter val = 4'b1101;
parameter l = 0, r = 3;
parameter signed [l:r] indx = val;

reg signed [7:0] t1, t2;
reg signed [0:15] t3, t4;

always @(posedge ifb.clk)
begin
        t1 = ifb.c3[3:0] + ifb.c4;
        t2 = t1 | i1;

        t3 = { t1, t2 };
        t4 = t2 - t3;
        ifb.c1 = t3 - t2;
        ifb.c2 = t4 - t1;

        out = ((t3 == t4) ? (i1 | t3- t4 & ifb.c3) : ( ifb.c3 ^ t4 + i1 | ifb.c4));
end

endmodule

