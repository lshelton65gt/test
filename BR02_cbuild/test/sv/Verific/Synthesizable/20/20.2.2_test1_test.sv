
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Interfaces may be used as the data type of ports in a module.
// This design defines an interface that is also used as the port type.

interface vars_and_nets;
    bit [4:0] addr_out;
    bit [3:0] in1, in2;
    bit [7:0] mult_out;
    bit greater_than_out, less_than_out;
endinterface

module test(vars_and_nets var1, input bit clk);

    assign var1.addr_out = var1.in1 + var1.in2;

    always @(posedge clk)
    begin
        var1.mult_out = var1.in1 * var1.in2;
        var1.greater_than_out = var1.in1 > var1.in2;
        var1.less_than_out = var1.in1 < var1.in2;
    end

endmodule

