
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Two interfaces are declared, one is used within the other interface
// This interfaces are used by modules.


interface i1(input clk);
    i2 inter1(clk), inter2(clk);
    modport masterTop(inter1.master, inter2.master);
    modport slaveTop(inter1.slave);
endinterface

interface i2(input clk);
    logic a, b, c, d;
    modport master(input a, b, clk, output c, d);
    modport slave(input c, d, clk, output a, b);
endinterface


module mod1(i1.masterTop i);

always_ff @(posedge i.inter1.clk)
begin
    if(i.inter1.a) 
    begin
        i.inter1.c = 1 ;
        i.inter1.d = 1 ;
    end
    else if(!i.inter1.b)
    begin
        i.inter1.c = 0 ;
        i.inter1.d = 0 ;
    end
    else
    begin
         i.inter1.c = i.inter1.a;
         i.inter1.d = i.inter1.b;
    end
end
endmodule

module mod2(interface a);

always @(posedge a.inter1.clk or posedge a.inter1.c or negedge a.inter1.d)
begin
    if(a.inter1.c)
    begin
        a.inter1.a = mod1.i.inter2.c;
        a.inter1.b = mod1.i.inter2.d;
        //a.inter1.a = a.inter1.c;
        //a.inter1.b = a.inter1.d;
    end
    else if(!a.inter1.d)
    begin
        a.inter1.a = 0 ;
        a.inter1.b = 0 ;
    end
    else
    begin
        a.inter1.a = a.inter1.c;
        a.inter1.b = a.inter1.d;
    end
end
endmodule

module test();

logic clk ;
initial
    repeat (10) clk = ~clk;

i1 i(clk);

mod1 instanMod1(i.masterTop);
mod2 instanMod2(i.slaveTop);
endmodule

