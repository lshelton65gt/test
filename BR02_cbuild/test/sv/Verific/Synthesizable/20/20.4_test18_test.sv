
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two interfaces to act as bundle of nets.
// Two modules are defined having generic interface type port. During module 
// instantiations separate interface instances are provided to two module instantiations. 

module memMod (interface a, input bit clk, input avail);
    //logic avail;
    always @(posedge clk) a.gnt <= a.req & avail;
endmodule 
    
module cpuMod(interface b, input bit clk);
    logic avail;
    always @(posedge clk) b.data1 = ~b.data1 ;
endmodule 

interface simple_bus; // Define the interface
    logic req, gnt;
    logic [7:0] addr, data1;
    logic [1:0] mode; 
    logic start, rdy;
endinterface: simple_bus

interface simple_bus1; // Define the interface
    logic  data1;
    logic start, rdy;
endinterface: simple_bus1

module test (input logic clk, input avail) ;
    simple_bus sb_intf() ;
    simple_bus1 sb_intf1() ;

    memMod mem1 (.a(sb_intf), .clk(clk), .avail(avail)) ; 

    cpuMod cpu (.b(sb_intf1), .clk(clk)) ;
endmodule 

