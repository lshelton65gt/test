
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface. Modports are used inside
// that interface. It uses export to export task/function from two modports. The
// exported entity is imported from the other modport. This checks whether export
// or import is properly supported by the tool.

interface interface1;

modport modport1(export task addr(input logic [3:0] in1, in2, output logic [3:0] out),
                 import function [7:0] mult(input logic [3:0] in1, in2));

modport modport2(import task addr(input logic [3:0] in1, in2,
                                  output logic [3:0] out),
                 export function [7:0] mult(input logic [3:0] in1, in2));

endinterface

module test(interface1.modport1 a, 
            input [3:0] in1, in2,
            output reg [7:0] mult_out);

task a.addr(input logic [3:0] in1, in2,
            output logic [3:0] out);
    out = in1 + in2;
endtask

always @(in1, in2)
    mult_out = a.mult(in1, in2);
endmodule

module test1(interface1.modport2 a,
            input [3:0] in1, in2,
            output logic [3:0] addr_out);

function [7:0] a.mult(input logic [3:0] in1, in2);

    return in1 * in2;
endfunction

always @(in1, in2)
    a.addr(in1, in2, addr_out);
endmodule

