
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface. Three modules are defined. 
// Two of them uses interface type ports. The other module instantiates these two 
// modules and connects the instantiated interface to the corresponding ports.

interface iface(input wire clk);
    logic [1:0] a;
    logic [3:0] b;
    logic [1:0] c;
    logic [4:0] d;
endinterface:iface

module test(input clk,input signed [3:0] i1, i2, output [5:0] o1, o2, o3, o4);

iface if1(clk);

bottom1 b1(if1,o1, o3);
bottom2 b2(if1,o2, o4);

endmodule

module bottom1(iface a,output reg [5:0] o1, o2);

reg [5:0] r1, r2, r3, r4;
int i = 1;

always @(posedge a.clk)
begin
    r2 |= (a.d[3:1]^3'b010) ? { 1'b0, (~ a.d)} : { 1'b1, a.d};
    r3 = r2;
    r4 = a.c;
    for (int i=5; i>=0; i--)
    begin
        r1[i] = r3[i];
        for (int j=i+1; j<=3; j++)
        begin
            r1[i] = r2[j] || r1[i];
            a.a = r1[i+:2];
            a.b = r2[j+:2];
        end
    end
    o1 = r1 ;

    if (a.c[0]) o1 = r1; else o1 = ~r1;
    if (a.d[0]) o2 = r1; else o2 = ~r1;
end
endmodule

module bottom2(iface b, output reg [5:0] o1, o2);
always @(posedge b.clk)
begin
    if(b.a[0]|b.b[0]) 
    begin
        o1 = b.a ^ b.b;
        b.c = b.a + b.b;
    end
    else
    begin
        o1 = b.a | b.b;
        b.c = b.a - b.b;
    end

    if(b.a[1]| b.b[1]) 
    begin
        o2 = b.a + b.b - 4'b0110;
        b.d = b.a - b.b*4'b0011;
    end
    else
    begin
        o2 = b.a & b.b - 2'b01;
        b.d = b.a ^ b.b;
    end

end

endmodule

