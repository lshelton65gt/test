
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Interfaces can have both parameters and ports like a module.
// This design defines an interface with parameters and ports. This is instantiated
// from inside the module with overridden parameter value.

interface iface #(parameter s = 4, w = 8) /* parameterized interface */
                 (input clk);
    reg [0:s-1] i1;
    reg [s-1:0] o1;
endinterface

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    iface #(.s(8)) if1(clk); /* override parameter values */
    iface #(8, 8) if2(clk); /* override parameter values */

    bot bot_1(if1);
    bot bot_2(if2);

    always@(negedge clk)
    begin
        if1.i1 = ~in1 | in2;
        if2.i1 = -in2 ^ in1;
    end

    always@(posedge clk)
    begin
        out1 = if1.o1 | if2.o1;
        out2 = if2.o1 - if1.o1;
    end

    module bot(iface if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = ~(-if1.i1) + 1;
        end

    endmodule

endmodule

