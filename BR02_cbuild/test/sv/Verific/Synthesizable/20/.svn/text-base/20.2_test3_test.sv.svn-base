
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface. The interface is instantiated
// as an array of interfaces. This array has 2 elements which are passed
// to two module instantiations.

interface iface(input clk);
    logic [0:7] in;
    logic [0:7] out;
endinterface

module test (input clk,
             input [7:0] in1, in2,
             output reg [7:0] out1, out2);

    iface if1 [1:0](clk);

    always@(posedge clk)
    begin
        out1 = if1[0].out - if1[1].out;
        out2 = if1[1].out - if1[0].out;
    end

    bottom bot1(if1[0]);
    bottom bot2(if1[1]);

    always@(negedge clk)
    begin
        if1[0].in = in1 | in2;
        if1[1].in = in1 ^ in2;
    end

endmodule

module bottom(iface ifc);

    always@(posedge ifc.clk)
    begin
        ifc.out = ~ifc.in;
    end

endmodule

