
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Functions can be declared inside an interface. This design
// defines an interface and two functions inside that interface. These functions
// inside the interface are called using hierarchical referencing.

interface iface(input clk);
    reg [0:7] i1, i2;
    reg [7:0] o1, o2;

    function set_all;
        i1 = (i2 = (o1 = (o2 = 1)));
    endfunction

    function reset_all;
        i1 = (i2 = (o1 = (o2 = 0)));
    endfunction
endinterface: iface

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [0:w-1] out1, out2);

    iface if1(clk);

    bot1 bot_1(if1);

    always@(negedge clk)
    begin
        if1.i1 = in1 + in2;
        if1.i2 = in1 & in2;
    end

    always@(posedge clk)
    begin
        out1 = if1.o1 * if1.o2;
        out2 = if1.o2 ^ if1.o1;

        if (out1 == out2)
            if1.reset_all;
        else
            if1.set_all;
    end

    module bot1(iface if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = ~(if1.i2 - if1.i1);
            if1.o2 = -(~if1.i1 + ~if1.i2);
        end

    endmodule

endmodule

