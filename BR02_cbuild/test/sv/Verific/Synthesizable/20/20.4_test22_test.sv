
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE
  
// TESTING FEATURE(S): This design defines an interface with two modports. Two
// modules are defined having interface type port with modport specification.
// In module instantiations interface instance is passed as actual.

interface simple_bus #(parameter p1 = 8, p2 = 2)
                         (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic [p2-1:0] mode ;
    logic start, rdy, avail ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data) ;
    modport master(input gnt, rdy, clk, avail,
                   output req, addr, mode, start,
                          ref data) ;

endinterface: simple_bus

module abcMod (simple_bus.master a) ; // interface name and modport 

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & a.avail ; // the gnt and req signal in the in

endmodule

module xyzMod (simple_bus.slave b) ;

    always @(posedge b.clk)
        b.gnt = b.start & b.req ;

endmodule


module test(input logic clk);

    simple_bus #(.p1(16), .p2(3)) sb_intf1(clk); // 1st instantiate the interface
    simple_bus sb_intf2(clk); // 2nd instantiate the interface
    
    abcMod abc1(.a(sb_intf1)); // Connect the interface to the module instance
    abcMod abc2(sb_intf2);

    xyzMod xyz1(.b(sb_intf1));
    xyzMod xyz2(sb_intf2);

endmodule 

