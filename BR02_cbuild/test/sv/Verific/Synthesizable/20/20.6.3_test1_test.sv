
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface. Modports are used inside
// that interface. It uses export to export a function. This checks whether export
// is properly supported by the tool.

interface interface1;

    modport modport1(export function [4:0] addr4(input logic [3:0] in1, in2),
                            function [7:0] mult(input logic [3:0] in1, in2));

    modport modport2(import function [4:0] addr4(input logic [3:0] in1, in2),
                     function [7:0] mult(input logic [3:0] in1, in2));

endinterface : interface1

module test (interface1.modport1 inst);

    function [4:0] inst.addr4(input logic [3:0] in1, in2);
        return in1 + in2;
    endfunction

    function [7:0] inst.mult(input logic [3:0] in1, in2);
        return  in1 * in2;
    endfunction

endmodule

module test1 (interface1.modport2 obj,
             input logic [3:0] in1, in2,
             output logic [4:0] addr_out,
             logic [7:0] mult_out);

    always @(in1, in2)
    begin
        mult_out = obj.mult(in1, in2);
        addr_out = obj.addr4(in1, in2);
    end

endmodule

