interface ifc #(parameter p1 = 4)
    (input [p1-1:0] in) ;
    parameter p2 = 8 ;
    logic [p2-1:0] out ;
endinterface: ifc

module test (ifc i) ;
    parameter p1 = 16 ;
    reg [p1-1:0] data ;

    assign data = i.in ;
    assign i.out = data ;

endmodule

module top (i1, o1) ;
    parameter p1 = 8 ;
    input [p1-1:0] i1 ;
    output [p1-1:0] o1 ;

    ifc #(p1) ports1 (i1) ;
    test #(p1) t1 (ports1) ;

    assign o1 = ports1.out ;

endmodule

