
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an interface. Modports are used inside
// that interface. It uses export to export a task. This checks whether export is
// properly supported by the tool.

interface interface1;

modport modport1(export task addr4(input logic [3:0] in1, in2,output logic [4:0] out),
                        task mult(input logic [3:0] in1, in2,output logic [7:0] out));

modport modport2(import task addr4(input logic [3:0] in1, in2,
                                   output logic [4:0] out),
                 task mult(input logic [3:0] in1, in2,
                           output logic [7:0] out));

endinterface : interface1

module test (interface1.modport1 inst);

task inst.addr4(input logic [3:0] in1, in2,
          output logic [4:0] out);

    out = in1 + in2;
endtask

task inst.mult(input logic [3:0] in1, in2,
          output logic [7:0] out);

    out = in1 * in2;
endtask

endmodule

module test1 (interface1.modport2 obj,
             input logic [3:0] in1, in2,
             output logic [4:0] addr_out,
             logic [7:0] mult_out);

always @(in1, in2)
begin
    obj.mult(in1, in2, mult_out);
    obj.addr4(in1, in2, addr_out);
end    
    
endmodule

