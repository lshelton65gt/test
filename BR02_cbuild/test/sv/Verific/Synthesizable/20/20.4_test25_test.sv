
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE
  
// TESTING FEATURE(S): This design defines a parameterized interface with two
// modports. This interface is used as port in two modules and at their
// instantiations modport names are provided.

interface simple_bus #(parameter p1 = 8, p2 = 2)(input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [p1-1:0] addr, data ;
    logic [p2-1:0] mode ; 
    logic start, rdy, avail ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data) ;
    modport master(input gnt, rdy, clk, avail,
                   output req, addr, mode, start,
                          ref data) ;

endinterface: simple_bus

module abcMod (simple_bus a) ; // interface name and modport name

    //logic avail ;

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & a.avail; // the gnt and req signal in the interface

endmodule 

module xyzMod (simple_bus b) ;

    always @(posedge b.clk)
        b.gnt = b.start & b.req ;

endmodule 

module test(input logic clk) ;

    simple_bus #(.p1(16),.p2(3)) sb_intf(clk) ; // Instantiate the interface
    abcMod abc(sb_intf.master) ; // Connect the interface to the module instance
    xyzMod xyz(sb_intf.slave) ;

endmodule
