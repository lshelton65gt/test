
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test how interface can act as a port bundle and how it can
// be connected to generic interface type port.

interface simple_bus (input bit clk); // Define the interface

    logic req, gnt;
    logic [7:0] addr, data1;
    logic [1:0] mode; 
    logic start, rdy, avail;
    modport slave (input req, addr, mode, start, clk, avail,
                   output gnt, rdy,
                          ref data1);
    modport master(input gnt, rdy, clk, avail,
                   output req, addr, mode, start,
                          ref data1);

endinterface: simple_bus

module abcMod (interface a); // uses just the interface

    always @(posedge a.clk) // the clk signal from the interface
        a.gnt <= a.req & a.avail; // the gnt and req signal in the interface
endmodule 

module xyzMod (interface b);
    always @(posedge b.clk)
        b.data1 = ~b.data1 ;

endmodule 

module test(input logic clk) ;

    simple_bus sb_intf(clk); // Instantiate the interface

    abcMod abc1(.a(sb_intf.master)); // Connect the interface to the module instance
    abcMod abc2(.a(sb_intf.slave));
    abcMod abc3(.a(sb_intf.master));

    xyzMod xyz2(sb_intf.master);

endmodule
