interface ifc (input clk) ;
    parameter p1 = 8 ;
    logic [p1-1:0] in ;
    logic [p1-1:0] out ;
    modport mp (input clk, input in, output out) ;
endinterface: ifc

module test (ifc i) ;
    parameter p1 = 16 ;
    reg [p1-1:0] data ;

    always@(negedge i.clk)
        data = i.in ;

    always@(posedge i.clk)
        i.out = data ;
endmodule

module top (clk, i1, i2, o1, o2) ;
    parameter p1 = 4 ;
    input clk ;
    input [p1-1:0] i1 ;
    input [4*p1-1:0] i2 ;
    output reg [p1-1:0] o1 ;
    output reg [4*p1-1:0] o2 ;

    always@(negedge clk)
    begin
        ports1.in = i1 ;
        ports2.in = i2 ;
    end

    always@(posedge clk)
    begin
        o1 = ports1.out ;
        o2 = ports2.out ;
    end

    ifc #(p1) ports1 (clk) ;
    test #(p1) t1 (ports1.mp) ;

    ifc #(4*p1) ports2 (clk) ;
    test #(4*p1) t2 (ports2.mp) ;

endmodule

