
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Testing parameterized interface. The parameter is overridden
// when instantiating the interface from the bench.

interface in #(parameter p1 = 8, parameter p2 = 8)(input logic clk);
    logic [p1-1:0] bus1;
    logic [p2-1:0] bus2;
    logic out;

    modport portConn(input bus1, bus2, output out);
endinterface

module test(in.portConn a);

    always @(*)
        a.out = (a.bus1 == a.bus2);

endmodule

