
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Interface declaration can have ports. This design defines
// an ansi port in an interface declaration.

interface vars_and_nets(input bit clk);
    logic [4:0] addr_out;
    logic [3:0] in1, in2;
    logic [7:0] mult_out;
    logic greater_than_out, less_than_out;
endinterface

module test(vars_and_nets var1);
    assign var1.addr_out = var1.in1 + var1.in2;

    always @(posedge var1.clk)
    begin
        var1.mult_out = var1.in1 * var1.in2;
        var1.greater_than_out = var1.in1 > var1.in2;
        var1.less_than_out = var1.in1 < var1.in2;
    end

endmodule

