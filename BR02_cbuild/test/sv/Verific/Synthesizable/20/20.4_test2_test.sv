
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines nested interface. The inner interface also
// contains modport declarations. Within the outer interface, the inner interface is
// instantiated. The modport declared in the outer interface is used as a port type
// of the declared module.

interface top_interface;
    interface child_interface;
        wire a, b, c, d;
        modport child_dir_spec1(input a, b, output c, d);
        modport child_dir_spec2(output a, b, input c, d);
    endinterface : child_interface

    child_interface child1(), child2();

    modport top_dir_spec(child1.child_dir_spec1, child2.child_dir_spec2);
endinterface    

module test(top_interface.top_dir_spec inst);

assign inst.child1.c = inst.child2.c > inst.child2.d,
       inst.child1.d = inst.child1.a < inst.child1.b,
       inst.child2.a = inst.child1.a < inst.child1.b,
       inst.child2.b = inst.child2.c > inst.child2.d;

endmodule
    
