
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses parameterized interfaces such that two modules
// can communicate with each other and share external data. The default direction of
// the variables are assumed to be inout. The interface is instantiated before its
// declaration.

module test (clk, reset, load, i1, i2, o1);

parameter Width = 16;

input clk, reset, load;
input [Width-1:0] i1, i2;
output [Width-1:0] o1;

iface #(.Width(Width)) a(clk,reset,load,o1);

bottom1 #(.Width(Width)) b11(i1,i2,a);
bottom2 #(.Width(Width)) b12(i1,i2,a);

endmodule

interface iface #(parameter Width=16)
                 (input wire clk, reset, load, output reg [Width-1:0] out);
    logic [Width-1:0] a1, a2;
    logic [Width-1:0] b1, b2;
endinterface: iface

module bottom1 #(parameter Width = 16) (
    input [Width-1:0] i1,i2,
    iface a);
always @(posedge a.clk, negedge a.reset, posedge a.load)
begin
    if (!a.reset)
    begin
        a.a1 = 0;
        a.b1 = 0;
    end
    else
    begin
        if (a.load)
        begin
            a.a1 = i1 ^ i2;
            a.b1 = i1 & i2;
        end
        else
        begin
            a.a1 = 'bz;
            a.b1 = 'bz;
        end
    end
end
endmodule


module bottom2 #(parameter Width = 16)( input [Width-1:0] i1,i2, iface b);
always @(posedge b.clk, negedge b.reset, posedge b.load)
begin
    if (!b.reset)
        b.out = 0;
    else
    begin
        if (b.load)
        begin
            b.a2 = b.a1 ^ i1;
            b.b2 = b.b1 ^ i2;
            b.out = b.a2 - b.b2;
        end
        else
        begin
            b.out = 'bz;
        end
    end
end
endmodule

