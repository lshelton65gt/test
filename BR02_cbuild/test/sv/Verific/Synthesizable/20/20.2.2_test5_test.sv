
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure inside an interface declaration.
// The structure is un-named and a data element is declared in the interface immediately
// after the structure declaration.

interface ifc(input clk);
    struct packed signed {
        reg [0:7] i1;
        reg [7:0] o1;
    } st;
endinterface: ifc

module test (input clk,
             input [7:0] in1, in2,
             output reg [0:7] out1, out2);

    ifc if1(clk);

    bot bot_1(if1);

    always@(negedge clk)
    begin
        if1.st.i1 = ~in1;
        if1.st.o1 = -in2;
    end

    always@(posedge clk)
    begin
        out1 = -if1.st.i1;
        out2 = ~if1.st.o1;
    end

    module bot(ifc if1);

        reg [0:7] r1, r2;

        always@(posedge if1.clk)
        begin
            r1 = if1.st.i1 | if1.st.o1;
            r2 = if1.st.o1 - if1.st.i1;
        end

    endmodule

endmodule

