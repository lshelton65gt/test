
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE
  
// TESTING FEATURE(S): This design defines an interface with modports. Two modules
// are defined having generic interface type port, but with modport name. In 
// module instantiations interface name is provided by passing interface instance
// as actual.

interface simple_bus #(parameter p1 = 8, p2 = 2)
                         (input bit clk) ; // Define the interface

    logic req, gnt ;
    logic [p1 - 1:0] addr, data ;
    logic [p2 - 1:0] mode ;
    logic start, rdy, avail ;
    modport slave (input req, addr, mode, start, clk,
                   output gnt, rdy,
                          ref data) ;
    modport master(input gnt, rdy, clk, avail,
                   output req, addr, mode, start,
                          ref data) ;

endinterface: simple_bus

module abcMod (interface.master a) ; // interface name and modport 

    always @(posedge a.clk) // the clk signal from the interface
        a.req <= a.gnt & a.avail ; // the gnt and req signal in the in

endmodule

module xyzMod (interface.slave b) ;

    always @(posedge b.clk)
        b.gnt = b.start & b.req ;

endmodule


module test(input logic clk);

    simple_bus #(.p1(16),.p2(3)) sb_intf1(clk); // 1st instantiate the interface
    simple_bus sb_intf2(clk); // 2nd instantiate the interface
    
    abcMod abc11(sb_intf1) ;
    abcMod abc12(sb_intf2) ;

    xyzMod xyz11(sb_intf1) ;
    xyzMod xyz12(sb_intf2) ;

endmodule 

