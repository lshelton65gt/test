
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two unions inside an interface declaration.
// The unions are un-named and data elements are declared in the interface immediately
// after the union declaration.

interface ifc(input clk);
    union packed signed {
        reg [0:7] i1;
        reg [7:0] o1;
    } un1;
    union packed signed {
        reg [0:7] i1;
        reg [7:0] o1;
    } un2;
endinterface: ifc

module test (input clk,
             input [7:0] in1, in2,
             output reg [0:7] out1, out2);

    ifc if1(clk);

    bot bot_1(if1);

    always@(negedge clk)
    begin
        if1.un1.i1 = ~in1;
        if1.un2.o1 = -in2;
    end

    always@(posedge clk)
    begin
        out1 = -if1.un2.i1;
        out2 = ~if1.un1.o1;
    end

    module bot(ifc if1);

        reg [0:7] r1, r2;

        always@(posedge if1.clk)
        begin
            r1 = if1.un1.i1 | if1.un2.o1;
            r2 = if1.un1.o1 - if1.un2.i1;
        end

    endmodule

endmodule

