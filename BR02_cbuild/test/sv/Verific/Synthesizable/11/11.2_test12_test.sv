
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an 'if-generate' statement. The condition of that
// 'if-generate' statement uses '?:' operators. They are then used with the
// '<' operator in it. The 'always_comb' block actually assigns value to
// the output port.

module test #(parameter w = 8)
             (input clk,
              input signed [w-1:0] in1, in2,
              output reg signed [w-1:0] out1, out2);

    logic signed [w-1:0] t1, t2;

    generate
        if (((w>4)?8:4) < ((w<4)?16:8))
        begin
            always@(posedge clk)
            begin
                t1 = in1 & in2;
                t2 = in2 - in1;    
            end
        end
        else
        begin
            always@(posedge clk)
            begin
                t1 = in1 ^ in2;
                t2 = in2 + in1;    
            end
        end
    endgenerate

    always_comb
    begin
        out1 = t1 ^ t2;
        out2 = t2 + t1;
    end

endmodule

