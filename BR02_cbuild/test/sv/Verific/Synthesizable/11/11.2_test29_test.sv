
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S):  This design uses non-blocking (<=) operator to assign values
// to variables. The variables changed due to assigning values are used in another
// always_comb block, so they are in the implied sensitivity list for that always_comb
// block. From there another always_comb block is activated in the same way which
// actually assigns some values to the output port.

module test #(parameter width = 8)
             (input clk,
              input signed [width-1:0] in1, in2,
              input [width-1:0] in3, in4,
              output reg [width-1:0] out1,
              output reg [width-1:0] out2);

    reg signed [width-1:0] r1, r2, r3, r4;
    reg [width-1:0] r5, r6, r7, r8;

    always@(posedge clk)
    begin
	r1 <= in1 | ~in2 & in4 ^ in3[width/2-1:0] - in3[width-1:width/2] ;
	r2 <= in3 + ~in1 ^ { in2 + width & ~in4 & width };
        r3 <= in2 ^ in1 & in4; 
        r4 <= in3 - in4 | in2;
    end

    always_comb
    begin
	r5 <= ((r1 == ~r2) ? (r1 * ~r2 + r4) : (r2 + ~r3 & r1));
	r6 <= r1 ^ ^r2 & r3 - r4[width-1:width/2] * ~r4[width/2-1:0];
        r7 <= r3 - r4 ^ r2;
        r8 <= r4 & r2 - r1;
    end

    always_comb
    begin
	out1 <= (r5 * r6) + r8 | (r7 << width/2);
	out2 <= (r8 + r5) & ~r7[width/2-1:0] & r6;
    end

endmodule

