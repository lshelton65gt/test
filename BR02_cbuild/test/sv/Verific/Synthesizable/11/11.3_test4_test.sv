
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design tests 'always_latch' construct used for coding latched
// logic.

module test(data, ena, out1, out2);

input [0:3] data;
input ena;

output [0:3] out1, out2;
reg [0:3] out1, out2;

	always_latch
		if (ena)
		out1 <= data;

	always @ (data or ena)
		if (ena)
			out2 <= data;
endmodule

