
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'always_ff' block. 'always_ff' blocks
// can be used to model synthesizable sequential behaviour. When it does not
// represent a sequential logic an warning should be generated.
// This design does not represent a sequential behaviour and checks for the warning.

module test #(parameter width=8)
             (input clk,
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;
reg [0:width-1] r3, r4;

always_ff@(clk, in1, in2)
begin
    if (clk)
    begin
        out1 = in1 | in2;
        out2 = in1 ^ in2;
    end
end

endmodule

