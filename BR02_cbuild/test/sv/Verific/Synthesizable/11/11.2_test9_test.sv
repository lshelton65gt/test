
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an 'always_comb' block. The 'always_comb'
// block is sensitive to the contents of a task, not only to its arguments. This
// design defines a task that uses global variables. The argument does not determine
// the output from the task. The task is invoked from inside both the always@*
// and 'always_comb' block to test how they are triggered. The task will be
// invoked whenever its arguments or the global variables are changed.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1, r2;
reg [0:p-1] r3, r4;
int trig_comb, trig_star;

always_comb
begin
    task1(in1, in2, r1);

    task1(r1, r2, out1);
    trig_comb++;
end

always@*
begin
    task1(in2, in1, r2);

    task1(r2, r1, out2);
    trig_star++;
end

always@(posedge clk)
begin
    r3 = in1 + in2;
    r4 = in2 - in1;
end

task automatic task1(input int a, b, output int c);
    int t1, t2;

    t1 = (a - a) + r3;
    t2 = (b ^ b) ^ r4;
    
    c = t2 * t1;
endtask

endmodule

