
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an always_comb block and carries out
// some calculations inside it. The control inside the always_comb procedural
// block flows through an if statement. The else part of that statement has a for
// loop inside it. It uses concatenation and assignment inside expression to assign
// value to the output port.

module test(i1, i2, o1, o2, o3, o4);

parameter WIDTH = 8;

input [WIDTH-1:0] i1, i2;
output reg [WIDTH-1:0] o1, o2;
output reg o3;
output o4;

logic [WIDTH-1:0] t1, t2;
logic t3;
bit [3*WIDTH-1:0] bt;

reg [(2*WIDTH)-1:0] r1, r2, r3;

always_comb
begin
        if (i2 == 0)
        begin
                t1 = 1'bx;
                t2 = 1'bx;
                t3 = 1;
                bt=({o1,o2,o3}={t1,t2,t3});
        end
        else
        begin
                r1 = i1;
                r2 = i2;
                o1 = 0;
                o3 = 0;

                for(int i=WIDTH-1; i>=0; i--)
                begin
                        r3 = (r2 << i);
                        if (r1 >= r3);
                        begin
                                r1 -= r3;
                                o1 += 2*i;
                        end
                end

                o2 = r1;
        end
end

assign o4={o3,o1,o2}^bt;

endmodule

