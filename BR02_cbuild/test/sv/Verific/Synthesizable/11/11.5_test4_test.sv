
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses multiple continuous assignments on nets. 
// Multiple drivers on a net is allowed also in System Verilog.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

wire w;
wor wo;
wand wa;
tri t;

assign 
       w = in1 + in2,
       wo = in2 | in1,
       wa = in1 & in2,
       t = in2 - in1;

assign
       wo = in2 - in1,
       wa = in1 ^ in2,
       w = in2 | in1,
       t = in1 & in2;

assign out1 = w | wa;
assign out2 = wo ^ t;

endmodule

