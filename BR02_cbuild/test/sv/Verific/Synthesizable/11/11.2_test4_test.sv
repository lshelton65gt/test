
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an 'always_comb' block. The 'always_comb'
// block is sensitive to the contents of a function, not only to its arguments. 
// This design defines a function that uses global variables and arguments. The
// arguments does not interfere the return value. It is called from inside both the
// always block to test triggering. The function will be called whenever its
// arguments or the global variables are changed.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1, r2;
reg [0:p-1] r3, r4;
int trig_comb, trig_star;

always_comb
begin
    r1 = func1(in1, in2);

    out1 = func1(r1, r2);

    trig_comb++;
end

always@*
begin
    r2 = func1(in2, in1);

    out2 = func1(r2, r1);

    trig_star++;
end

always@(posedge clk)
begin
    r3 = in1 ^ in2;
    r4 = in2 | in1;
end

function automatic int func1(input int a, b);
    int t1, t2;

    t1 = ((a - b)-(a - b)) | r3;
    t2 = ((b - a)+(a - b)) & r4;
    
    return (t2-t1);
endfunction

endmodule

