
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an always_ff block and asynchronous set
// or reset. It sets values of some data elements. These data elements are used
// to drive the output port.

module test #(parameter WIDTH=4)( input clk, reset,input [WIDTH -1:0] i1, i2, output reg [WIDTH -1:0] o1, o2);

reg [WIDTH -1:0] r1, r2;

always_ff @(posedge clk iff reset==0 or posedge reset)
begin
    r2 = (reset) ? i1 | i2 : 0;
    r1 = (reset) ? i1 ^ i2 : r2;
end

always @(posedge clk)
begin
    if(r1[0])
        o1 = {r1[1:0], r1[2:1]} ;
    else
        o1 = {r1[1:0], ~r1[2:1]} ;

    if(r2[0])
        o2 = {r2[1:0], r2[2:1]} ;
    else
        o2 = {r2[1:0], ~r2[2:1]} ;

end

endmodule

