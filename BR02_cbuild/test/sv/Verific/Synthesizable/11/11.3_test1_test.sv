
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an 'always_latch' block. An warning can be
// given by the tool if the behaviour of 'always_latch' does not match that of 
// latched logic. This design models a combinational logic. So an warning should be given.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;

always_latch
begin
    r1 = in1 - in2;
    r2 = in2 - in1;

    out1 = ~r1 ^ ~r2;
    out2 = r2 | r1;
end

endmodule

