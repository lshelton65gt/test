
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'continuous assignments' on variables of new
// data types that SystemVerilog introduced. SystemVerilog supports
// 'continuous assignment' to any type of variables.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

byte c;
shortint si;
int i;
longint li;
reg r;

assign
        c = in1,
        si = 1 + in1<<8,
        i = 5 | in1 & in2,
        li = in1 * in2,
        r = 0;

assign out1 = c | i & si;
assign out2 = li + i - c;

endmodule

