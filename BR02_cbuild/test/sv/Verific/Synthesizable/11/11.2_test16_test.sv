
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a type parameter, whose default type
// is int. This type is used to define a variable and that variable is manipulated
// through a task invocation from an always_comb block. This design defines two
// always_comb block.

module test(i1, i2, i3, i4, o1, o2);

parameter IN_WIDTH = 8;
parameter OUT_WIDTH = 8;
parameter type V_TYPE=int;
parameter V_WIDTH=32;

input signed [IN_WIDTH-1:0] i1, i2;
input [IN_WIDTH-1:0] i3, i4;
output reg signed [OUT_WIDTH-1:0] o1;
output reg [V_WIDTH-1:0] o2;

reg signed [IN_WIDTH-1:0] r1, r2, r3, r4;
V_TYPE i;

task t_fill_one;
    i='1;
endtask

always_comb
begin
        r1 = 2'b0x;
        r2 = 2'bzx;
        
        t_fill_one();

        if (i3 == i4)
        begin
                r1 = (i1[IN_WIDTH-1] ? { ~i1[IN_WIDTH-1], i1[IN_WIDTH-2:0] } : i1);
                r2 = (i2[IN_WIDTH-1] ? { ~i2[IN_WIDTH-1], i2[IN_WIDTH-2:0] } : i2);
        end

        if (i3 != i4)
        begin
                r1 = i1;
                r2 = i2;
        end

end

always_comb
begin
        r3 = r1[IN_WIDTH-1:IN_WIDTH-5] * r2[IN_WIDTH-4:0];
        r4 = r1[IN_WIDTH-4:0] * r2[IN_WIDTH-1:IN_WIDTH-5];

        if (r3 ^~ r4)
                o1 = r3 ^ r4;
        else
                o1 = r3 | r4;
        o2=i^o1;

end

endmodule

