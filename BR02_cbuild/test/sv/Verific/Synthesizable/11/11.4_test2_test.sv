
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'always_ff' block. 'always_ff' blocks can
// be used to model synthesizable sequential behaviour. This design uses
// such 'always_ff' block to model synthesizable sequential logic.

module test #(parameter width=8)
             (input clk,
              input [width-1:0] in1, 
              input [0:width-1] in2, 
              output reg [width-1:0] out1, out2);

reg [width-1:0] r1, r2;
reg [0:width-1] r3, r4;

always_ff@(posedge clk)
begin
    r1 <= ((in1^in2)?(in1|in2):(in1&in2));
    if (in1 == in2)
        r2 <= in1;
    else
        r2 <= in2-in1;

    out1 <= r1 - r2;
    out2 <= r2 - r1;
end

endmodule

