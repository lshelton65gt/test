
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'always_comb' block. The block has an 
// inferred sensitivity list of all the variables that are read in the procedure. 
// So it uses one 'always_comb' and one 'always@*' block and carries out same 
// operations inside them. The bench file checks whether the outputs are same. 
// The output should only differ at time zero. This is to be noted that the
// 'always@*' block obeys the restrictions imposed by 'always_comb' so that they
// perform almost similarly.

module test #(parameter p=8)
             (input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1, r2;
reg [0:p-1] r3, r4;

always_comb // changes in any of in1, in2, r1 or r2 triggers this block
begin
    r1 = in2 - in1;
    r2 = in1 - in2;

    out1 = r1 ^ r2;
end

always@*
begin
    r3 = in2 - in1;
    r4 = in1 - in2;

    out2 = r3 ^ r4;
end

endmodule

