
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses 'always_comb' construct used to code 
// combinational logic behavior.

module test (clk, reset, set, out1, in1, in2);

input reset, set, clk;
input [0:3] in1, in2;
output [0:3] out1;

reg temp [0:3];

	always_comb
	begin
		reg [0:3] t;
		t = in1 + in2;
		for (int i=0; i < 4; i++)
			temp[i] = t[i];
	end

genvar i;
	generate
	for (i = 0; i < 4; i++)
	begin:blk
		ff i1 (.*, .q(out1[i]), .d(temp[i]));
	end
	endgenerate
endmodule

module ff(reset, set, clk, q, d);
input reset, set, clk, d;
output q;
reg q;

	always_ff @ (posedge clk iff ((reset == 0) || (set == 0)) 
		or posedge set iff (reset == 0) or posedge reset)
	begin
		if (reset)
		begin
			q = 0;
		end
		else if (set)
		begin
			q = 1;
		end
		else
		begin
			q = d;
		end
	end

endmodule

