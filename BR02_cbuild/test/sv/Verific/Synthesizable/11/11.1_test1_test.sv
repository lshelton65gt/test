
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an always_comb and an always_ff block.
// The always_ff block manipulates some local data elements and the always_comb
// block sets the value of the output port form that data elements.

module test(input clk, i1, i2, output logic [15:0] o1);

logic [15:0] r1;

always_comb
begin

    case (i2)
        '0: o1 = r1;
        '1: o1 = '0;
        default: o1 = ~r1;
    endcase
end

always_ff @(posedge clk iff i1==1 or negedge i1)
begin
    if(!i1)
        r1 = '1;
    else
    begin
        r1[12'ha:0] = '0;
        r1 ^= 7;
    end
end

endmodule

