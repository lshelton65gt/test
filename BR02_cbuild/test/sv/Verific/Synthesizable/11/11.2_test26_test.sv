
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines three always_comb block and one always
// block. The always block manipulates variables that are used in the next always_comb
// block. This block manipulates variables that are used in the next always_comb
// block. The last always_comb block sets the value of one output port. The other
// output port is driven by an instantiation of another module whose input ports
// are connected to the variables that are manipulated by the second by the last
// such block.

module test(clk, i1, i2, i3, i4, o1, o2);

parameter WIDTH = 16;

input clk;
input signed [WIDTH-1:0] i1, i2;
input [WIDTH-1:0] i3, i4;
output reg signed [WIDTH-1:0] o1;
output signed [2*WIDTH-1:0] o2;

reg [0:0] l1, l2;
reg [14:0] r1;
reg signed [15:0] r2, r3;
reg signed [15:0] r4, r5;

always @(posedge clk)
begin
        r1 = (~i1 & i2);
        r2 = (i2 | (i3 - ~i4));
        r4 = ((i3 ^ ~i1) | i2);
end

always_comb
begin
        r3 = (r1 + r2);
        r5 = (r3 - r1);
end

always_comb
begin
        l1 = |(r3 ^~ r5);
        l2 = |(r5 ~^ r3);
end

always_comb
begin
        if (~(l1 ^ l2))       
        begin
                o1 = r5 + r2;
        end
        else
        begin
                o1 = r3 - r5;
        end
end

bottom b1(clk,{r5,r3},o2);

endmodule

module bottom(clk, i1, o1);
input int i1;
output int o1;
input clk;
int r1;
genvar i;

generate
    for(i=0;i<32;i++)
    begin:blk
        assign o1[i] = r1[i];
    end
endgenerate

always @(posedge clk)
begin
    r1+=i1;
end

endmodule

