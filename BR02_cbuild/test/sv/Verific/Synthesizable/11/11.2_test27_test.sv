
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines four modules. Three of which are 
// instantiated in another module. All the modules use always_comb block to
// set the value of the output port. The top level module also uses always_comb
// and instantiations to set some of the output ports.

module test(i1, i2, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10);

parameter WIDTH = 16;

input signed [0:WIDTH-1] i1;
input [0:WIDTH-1] i2;
output [WIDTH-1:0] o1, o2, o3, o4, o5, o6, o7, o8, o9, o10;

reg [WIDTH-1:0] t1, t2, t7, t9;

always_comb
begin
        t1 = ~i1;
        t2 = ~i2;
        t7 = i1 ~^ i2;
        t9 = i1 ^~ i2;
end

assign o1 = t1, o2 = t2, o7 = t7, o9 = t9;

bottom1 #(.WIDTH(WIDTH)) b11 (i1, o3);
bottom1 #(.WIDTH(WIDTH)) b12 (i2, o4);

bottom2 #(.WIDTH(WIDTH)) b21 (i1, i2, o8);
bottom2 #(.WIDTH(WIDTH)) b22 (i2, i1, o5);

bottom3 #(.WIDTH(WIDTH)) b31 (i1, i2, o10);
bottom3 #(.WIDTH(WIDTH)) b32 (i2, i1, o6);

endmodule

module bottom1(i1, o);
parameter WIDTH = 16;

input [WIDTH-1:0] i1;
output logic [WIDTH-1:0] o;

always_comb
begin
        for(int i=0; i<WIDTH; i++)
        begin
                o[i] = ~i1[i];
        end
end

endmodule

module bottom2(i1, i2, o);
parameter WIDTH = 16;

input [WIDTH-1:0] i1, i2;
output logic [WIDTH-1:0] o;

always_comb
begin
        for(int i=0; i<WIDTH; i++)
        begin
                o[i] = (i1[i] ^~ i2[i]);
        end
end

endmodule

module bottom3(i1, i2, o);
parameter WIDTH = 16;

input [WIDTH-1:0] i1, i2;
output logic [WIDTH-1:0] o;

always_comb
begin
        for(int i=0; i<WIDTH; i++)
        begin
                o[i] = (i1[i] ~^ i2[i]);
        end
end

endmodule

