
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test for the warning if 'always_comb' procedure does not 
// represent combinational logic.

module test(input a, input b, output reg out);
always_comb
begin
    if(a)
        out = 0;
    else if(b)
        out = 1;	
end
endmodule

