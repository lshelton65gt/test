
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a 'case-generate'. The condition of the 
// 'case-generate' is a '?:' operator. The operator returns enum literals
// in both cases. The 'case' items of the 'case-generate' are integer
// literals. It uses a 'always_comb' block to set the value of the
// output port.

module test #(parameter w = 8)
             (input clk,
              input signed [w-1:0] in1, in2,
              output reg signed [w-1:0] out1, out2);

    logic signed [w-1:0] t1, t2;
    enum { odd = 1, even = 2 } enum_t;

    generate

        case((2*(w<<1)==w) ? (even) : (odd))
            1:
                always@(posedge clk)
                begin
                    t1 = in1 | in2;
                    t2 = in2 - in1;
                end
            2:
                always@(posedge clk)
                begin
                    t1 = in1 ^ in2;
                    t2 = in2 & in1;
                end
            default:
                /* should not come here */;
        endcase

    endgenerate

    always_comb
    begin
        out1 = t1 * t2;
        out2 = t2 + t1;
    end

endmodule

