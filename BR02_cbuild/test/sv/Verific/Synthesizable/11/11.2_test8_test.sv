
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an 'always_comb' block. The 'always_comb'
// block is sensitive to the contents of a task, not only to its arguments. This 
// design defines a task that does not uses global variables and the output is not
// dependent on its inputs. The task is invoked from inside the 'always_comb'
// block and the 'always@*' block to test the triggering.

module test #(parameter p=8)
             (input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1, r2;
int trig_comb, trig_star;

always_comb
begin
    task1(in1, in2, r1);

    task1(r1, r2, out1);
    trig_comb++;
end

always@*
begin
    task1(in2, in1, r2);

    task1(r2, r1, out2);
    trig_star++;
end

task automatic task1(input int a, b, output int c);
    c = (a^b)|(a&b);
endtask

endmodule

