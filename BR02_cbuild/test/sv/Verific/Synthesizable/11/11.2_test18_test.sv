
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. It uses two always_comb
// blocks. The first always_comb block assigns values to some local data element from
// the input ports. The other always_comb block uses those local data elements and
// assigns to the output port. Another module is defined also. This module is, then,
// instantiated with port connected to element of the data type defined.

typedef struct packed {
    logic [7:0] x1;
    logic [7:0] x2;
} udtype;

module test(i1, i2, o1, o2);

parameter WIDTH = 8;

input signed [WIDTH-1:-WIDTH] i1, i2;
output reg [WIDTH-1:-WIDTH] o1;
output [WIDTH-1:0] o2;

reg signed [0:WIDTH-1] r1, r2;
reg signed [WIDTH-1:0] r3, r4;
reg [WIDTH-1:0] t1, t2;
udtype d1;

always_comb
begin
        r1 = i1[WIDTH-1:0];
        r2 = i2[WIDTH-1:0];

        r3 = i1[1:-WIDTH];
        r4 = i2[1:-WIDTH];

        r1 = (r1[0] ? ({ ~r1[0], r1[1:WIDTH-1] }) : r1);
        r2 = (r2[0] ? -r2 : r2);
        r3 = (r3[WIDTH-1] ? ~(r3-1) : r3);
        r4 = (r4[0] ? ({ ~r4[WIDTH-1], r4[WIDTH-2:0] }) : r4);

end
 
always_comb
begin
        t1 = r1 * r3;
        t2 = r2 + r4;

        o1 = ~t1 - t2;
        d1=~{r1,r2};
end

bottom #(WIDTH) b1(d1,o2);

endmodule

module bottom(i1, o1);
    parameter WIDTH = 8;
    input udtype i1;
    output reg [WIDTH-1:0] o1;
    
    always @(i1)
    begin
        o1=^i1;
    end
endmodule

