
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an always latch within a generate block.

module test(in1, out1, e);
parameter width = 8;
input  [width-1 : 0] in1;
output [width-1 : 0] out1;

input e;
reg [width-1 : 0] out1;
genvar i;

    generate 
    for( i = 0; i < width; i = i + 1)
    begin : b1
        always_latch
            if (e) out1[i] = ~in1[i];
    end
    endgenerate
endmodule

