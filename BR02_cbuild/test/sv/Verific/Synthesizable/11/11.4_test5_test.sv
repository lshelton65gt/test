
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two always_ff blocks with asynchronous
// set/reset. Inside those always blocks, it uses ? : operator to set the value of
// the output port.

module test(input clk, reset, input [1:0] i1, i2, output reg [3:0] o1, o2);

always_ff @(posedge clk iff reset==0 or posedge reset)
begin
    o1[0] = reset ? i1[0] * i2[0] : i1[0] ^ ~i2[0];
    o1[1] = reset ? i1[1] ^ i2[1] : i1[1] - ~i2[1];
    o1[2] = reset ? i1[0] | i2[0] : i1[0] | ~i2[0];
    o1[3] = reset ? i1[0] + i2[0] : i1[0] & ~i2[0];
end

always_ff @(posedge clk iff reset==1 or negedge reset)
begin
    o2[0] = !reset ? i1[1] & i2[0] : i1[1] + ~i2[0];
    o2[1] = !reset ? i1[0] - i2[1] : i1[0] | ~i2[1];
    o2[2] = !reset ? i1[1] | i2[1] : i1[1] * ~i2[1];
    o2[3] = !reset ? i1[1] + i2[1] : i1[1] ^ ~i2[1];
end

endmodule

