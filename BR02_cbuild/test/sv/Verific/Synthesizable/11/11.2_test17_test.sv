
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a function which carries out arithmatic
// shift operation on its parameter. The direction and number of bits to shift is
// also specified through the parameter. This function is called inside of an if
// statement in always_comb block.

module test(i1, i2, o1);

parameter WIDTH = 8;

input [WIDTH-1:0] i1, i2;
output reg [WIDTH-1:0] o1;

reg [WIDTH-1:0] t1, t2;
int i;

function int slr(int val, int pos, bit lr);
 
    if(lr)
        return val<<<pos;
    else
        return val>>>pos;
    
endfunction

always_comb 
begin
        i=0;
        t1 = ~i1;

        for(int i=0; i<WIDTH; i++)
        begin
                t2[i] = i2[WIDTH-i-1];
        end

        if (t1 === t2)
        begin
                if (t1 ^~ t2)
                begin
                        o1 = slr(t1,2,0) + slr(i2,2,1);
                end
                else
                begin
                        o1 = slr(t2,2,1) - slr(i1,2,0);
                end
        end
        else
        begin
                o1 = ( i1[WIDTH-1:WIDTH-5] + t1[WIDTH-4:0] )
                                - ( { i2[WIDTH-1], t2[WIDTH-2:1], i2[0] } );
        end
end

endmodule

