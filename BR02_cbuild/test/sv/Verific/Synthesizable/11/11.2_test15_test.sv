
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules. Both modules have one
// always_comb block. The bottom module calculates the output depending on the
// input value inside the always_comb block. The other module instantiates the
// bottom module and uses a always_comb block to decide the value of another
// output port.

module test #(parameter WIDTH = 4, parameter OUT_WIDTH = 16)
             (input [WIDTH-1:0] i1, i2, output [OUT_WIDTH-1:0] o1, o2, o3);

bottom #(WIDTH, OUT_WIDTH) d1(i1, o1);
bottom #(WIDTH, OUT_WIDTH) d2(i2, o2);

reg [OUT_WIDTH-1:0] r;

always_comb
begin
        if(i1 === i2)
                r = {i1, i2};
        else
                r = i1 + i2;
end

assign o3 = r;

endmodule

module bottom(i1, o1);
parameter WIDTH = 4;
parameter OUT_WIDTH = 16;

input [WIDTH-1:0] i1;
output [OUT_WIDTH-1:0] o1;

int t;

always_comb
begin
        if(i1==0)
                t = '0;
        else
        begin
                t = '1;
                if(i1>0)
                        t = t << (i1-1);
        end
end

assign o1 = t;

endmodule

