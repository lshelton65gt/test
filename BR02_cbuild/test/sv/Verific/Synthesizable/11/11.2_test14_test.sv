
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses two always_comb blocks. In one block it
// assigns constant literal value to a structure type variable. In the other block
// it uses a case statement, the case items are the members of the said structure
// type variable. Depending on the value of the case expression the output gets
// different values.

typedef struct {
    reg [1:0] x1 ;
    reg [1:0] x2 ;
    reg [1:0] x3 ;
    reg [1:0] x4 ;
} dtype ;

module test #(parameter OUT_WIDTH = 32, parameter IN_WIDTH = 8)
             (input [IN_WIDTH-1:0] i1, i2,input [1:0] x,output reg [OUT_WIDTH-1:0] o1) ;

dtype d1 ;

always_comb
    d1='{2'b00, 2'b01, 2'b10, 2'b10} ;

always_comb
begin
        case (x)
                d1.x1: o1 = i1 - i2 ; 
                d1.x2: o1 = i1 << i2[3:0] ;  
                d1.x3: o1 = {i1[IN_WIDTH-2:1], i2[IN_WIDTH-1:2]} ;
                d1.x4: o1 = i2[IN_WIDTH-1:0] + i1 ;
        endcase
end

endmodule

