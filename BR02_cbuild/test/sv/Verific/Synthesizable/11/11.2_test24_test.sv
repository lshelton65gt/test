
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules. In the top level module 
// there are two always_comb blocks one block assigns values to locally declared
// data element from the input port. The other always_comb block assigns another
// set of locally defined variables to the output port. These variables are
// connected to the output port of an instantiation while the previous set of
// variables are connected to the input port. The instantiated module also uses
// an always_comb block.

module test(i1, i2, i3, i4, o1, o2);

parameter WIDTH = 16;

input [WIDTH-1:0] i1, i2, i3, i4;
output reg [WIDTH-1:0] o1, o2;

reg signed [31:0] r1, r2;
reg [31:0] r3, r4;
wire signed [31:0] r5, r6;

always_comb
begin
        r3 = { 29 { &i1 } };
        r4 = { r3[5], i2[WIDTH/4-1:0], { 6 { |i2 } }, 1'b0 };
        r1 = { 30 { i3 == i1 } };
        r2 = i4 - ~i2;

        r1 = { r3[15:0], r1[20-:18] };
        r2 = { r2[21-:20], r4[15:2] };
        r3 = r1;
end

bottom b1(.i1(r1), .i2(r2), .i3(r3), .o1(r5),.o2(r6));

always_comb
begin
        o1 = r5 | r6;
        o2 = r5 ^ r6;
end

endmodule

module bottom(i1,i2,i3,o1,o2);
input int i1,i2,i3;
output int o1, o2;
    always_comb
    begin
       o1=32'({i1,i2,3'(i3)});
       o2=32'({10'(i1),10'(i2),13'(i3)});
    end
endmodule

