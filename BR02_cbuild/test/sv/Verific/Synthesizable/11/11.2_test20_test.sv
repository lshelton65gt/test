
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an always_comb block. Inside the block,
// it calculates and assigns the input ports to some local data element declared.
// Then the local elements are used in arithmatic operation and the result is
// assigned to the output port.

module test(i1, i2, o1, o2);

parameter WIDTH = 8;

input signed [WIDTH-1:0] i1;
input [WIDTH-1:0] i2;
output reg [WIDTH-1:0] o1;
output reg signed [WIDTH-1:0] o2;
bit [WIDTH-1:0] t1, t2;

always_comb
begin
    t1 = { i1[WIDTH-1:0] };
    t2 = { i2[WIDTH-1:0] };

    t1 = { t1[0], t2[WIDTH-1:1] };
    t2 = { t2[0], t1[WIDTH-1:1] };

    t1 += { 1'b0, { (WIDTH-1) { t2[WIDTH/2] } } };
    t2 -= (t1[WIDTH-1:WIDTH/2] * t2[WIDTH/2-1:0]);

    t2 += (t1 |='1);

    o1 = t1 - t2;
    o2 = t2 - t1;
end

endmodule

