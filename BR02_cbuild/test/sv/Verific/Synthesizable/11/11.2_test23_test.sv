
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules. In the top level module 
// an always block is defined. In this block it sets values of some locally defined
// data elements. This elements are connected to the input port of an instantiation
// of another module. The other module uses a always_comb block to drive the output
// port from the input port.

module test(clk, i1, i2, o1);

parameter IN_WIDTH = 8;

input clk;
input signed [IN_WIDTH-1:0] i1, i2;
output o1;

reg [IN_WIDTH-1:0] r1, r2, r3, r4;

always @(posedge clk)
begin
        r1 = i1;
        r2 = i2;

        r3 = -r1 - -r2;
        r4 = +r1 + +r2;

        r1 = r3 * r4;
        r2 = r4 - r3;

end

bottom #(.WIDTH(IN_WIDTH)) b1(r1, r2, r3, r4, o1);

endmodule

module bottom(i1, i2, i3, i4, out);
parameter WIDTH = 8;
input [WIDTH-1:0] i1, i2, i3, i4;
output reg out;
always_comb
begin
        if (i1 >= i2)
        begin
                out = !(i1 > i2) && (i1 == i2);
        end
        else
        begin
                out = (i1 < i2) || (i1 == ((i3 != i4) ? (i4 - i3) : i2));
        end
end
endmodule

