
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an 'always_comb' block. The 'always_comb'
// block is sensitive to the contents of a function, not only to its arguments.
// This design defines a function that does not use global variables. So both 
// the 'always_comb' and 'always@*' should be triggered same number of times due
// to change of their sensitivity list specified items. But 'always_comb' is 
// called at simulation time 0. So totally 'always_comb' will be triggered extra
// once than 'always@*'.

module test #(parameter p=8)
             (input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1, r2;
int trig_comb, trig_star ;

always_comb
begin
    r1 = func1(in1, in2);

    out1 = func1(r1, r1);

    trig_comb++;
end

always@*
begin
    r2 = func1(in2, in1);

    out2 = func1(r2, r2);

    trig_star++;
end

function automatic int func1(input int a, b);
    return (a+b);
endfunction

endmodule

