
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines one always block and one always_comb
// block. The always block is sensitive to the positive edge of clock and it sets
// values of some locally defined data elements. These are used in an always_comb
// block to assign values to output port.

module test(clk, i1, i2, o1, o2, o3);

parameter IN_WIDTH = 8;

input clk;
input signed [IN_WIDTH-1:0] i1, i2;
output reg o1, o2, o3;

reg t1, t2, t3, t4;
wire [IN_WIDTH-1:0] r1, r2, r3;

always @(posedge clk)
begin
        t1 = ( &i1 | |i2);
        t2 = ~( ~&i1 & ~|i2);

        t3 = (^i1 & ~^i2);
        t4 = ~(^~i1 | ^i2);

end

always_comb
begin
        o1 = t1 | t2 ^ r1;
        o2 = t2 | t3 ^ r2;
        o3 = t3 | t4 ^ r3;
end

bottom #(.WIDTH(IN_WIDTH)) b1(clk, i1,i2,r1,r2,r3);

endmodule

module bottom(clk, i1, i2, o1, o2, o3);
parameter WIDTH=8;
input clk;
input signed [WIDTH-1:0] i1, i2;
output reg [WIDTH-1:0] o1, o2, o3;
int i='0;
always @(posedge clk)
begin
    if(i++<WIDTH)
        o1=(o2=i2[1]|(o3=i1[i]));
    else
        o1= (o2=(o3='1));
    
end

endmodule

