
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses multiple continuous assignments on nets. This
// is supported by SystemVerilog. The assignment is made through assign
// statements and primitive instantiations.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

wire w1, w2;
wand a1, a2;
wor o1, o2;

assign w1 = |in1 | |in2,
       w2 = ^in2 ^ ^in1 + w1,
       a1 = &in1 & &in2,
       a2 = ~^in1 - |in2,
       o1 = ^in1 + ~&in2,
       o2 = &in1 ^ ~|in2;

or og(o1, w1, a1);
and ag(o2, a2, w2);

assign out1 = o1 | o2;
assign out2 = o2 - o1;

endmodule

