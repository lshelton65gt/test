
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a 'case-generate' construct. The condition of the 
// 'case-generate' is a '?:' operator. The operator returns enum literals
// or a parameter value depending on the value of the parameter.
// The case items of the 'case-generate' are also '?:' operators.
// It uses an 'always_comb' block to set the value of the output port.

module test #(parameter w = 8)
             (input clk,
              input signed [w-1:0] in1, in2,
              output reg signed [w-1:0] out1, out2);

    logic signed [w-1:0] t1, t2;
    enum { none, first, second, third, fourth, fifth, sixth, seventh, eighth,
           ninth, tenth, eleventh, twelfth, thirteenth, fourteenth, fifteenth } enum_t;

    generate

        case((w>fifteenth)?none:w)
            (0==w)?none:
                   ((1==w)?first:
                           ((2==w)?second:
                                   ((3==w)?third:none))):
                always@(posedge clk)
                begin
                    t1 = in1 | in2;
                    t2 = in2 - in1;
                end
            (4==w)?fourth:
                   ((5==w)?fifth:
                           ((6==w)?sixth:
                                   ((7==w)?seventh:fourth))):
                always@(posedge clk)
                begin
                    t1 = in1 ^ in2;
                    t2 = in2 & in1;
                end
            (8==w)?eighth:
                   ((9==w)?ninth:
                           ((10==w)?tenth:
                                   ((11==w)?eleventh:eighth))):
                always@(posedge clk)
                begin
                    t1 = in1 * in2;
                    t2 = in2 + in1;
                end
            (12==w)?twelfth:
                   ((13==w)?thirteenth:
                           ((14==w)?fourteenth:
                                   ((15==w)?fifteenth:twelfth))):
                always@(posedge clk)
                begin
                    t1 = in1 & in2;
                    t2 = in2 | in1;
                end
            default:
                always@(posedge clk)
                begin
                    t1 = in1 + in2;
                    t2 = in2 ^ in1;
                end
        endcase

    endgenerate

    always_comb
    begin
        out1 = t1 * t2;
        out2 = t2 + t1;
    end

endmodule

