
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses multiple continuous assignments on nets.
// This is supported by SystemVerilog. The assignment is through assign
// statements, primitive and module instantiations. In this case
// multiple continuous assignments are used inside a for-generate loop.

module test #(parameter width=8)
             (input [width-1:0] in1, 
              input [0:width-1] in2, 
              output [width-1:0] out1, out2);

wire w1, w2;
wand a1, a2;
wor o1, o2;

genvar i;

generate

    for (i=0; i<width; i++)
    begin:blk
        assign w1 = in1[i] | in2[i],
               w2 = in1[width-1-i] ^ in2[i],
               a1 = in1[i] & in2[width-1-i],
               a2 = in1[width-1-i] * in2[width-1-i],
               o1 = |in1,
               o2 = ^in2;

        or or1(o1, w1, a1);
        and and1(o2, w2, a2);

        mod m1(a1, o1, o2);
        mod m2(a2, w1, w2);
        mod m3(w1, a1, a2);
        mod m4(w2, a1, o2);

        assign out1[i] = w1 + a2;
        assign out2[i] = o2 - w2;
    end

endgenerate

endmodule

module mod(output o1, inout i1, i2);

assign o1 = i1 + i2 | i1 * i2;

endmodule

