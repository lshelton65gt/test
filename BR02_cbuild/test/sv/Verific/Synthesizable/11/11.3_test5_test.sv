
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): 'always_latch' procedure is provided for modeling latched
// logic behavior. An warning may be issued if it does not represent latched logic.

module test(clk, d, q);
input clk, d;
output q;
reg q;

always_latch
begin
    if(clk)
        q = d;
end
endmodule

