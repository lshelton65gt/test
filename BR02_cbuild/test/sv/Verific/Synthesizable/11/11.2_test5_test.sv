
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an 'always_comb' block. An warning can be
// given by the tool if the behaviour of 'always_comb' does not match a combinational
// logic. This design models a latched logic. So a warning should be given.

module test #(parameter p=8)
             (input enable, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1, r2;

always_comb
begin
    if (enable)
    begin
        r1 = {in1[p-1:p/2], in2[0:p/2-1]};
        r2 = in1>>p/2 | in1<<p/2;

        out1 = r1 & r2;
        out2 = r2 - r1;
    end
end

endmodule

