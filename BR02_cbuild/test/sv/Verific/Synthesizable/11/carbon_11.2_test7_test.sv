// Carbon modified to remove races.

// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an 'always_comb' block. The 'always_comb'
// block is sensitive to the contents of a task, not only to its arguments. This
// design defines a task that uses global variables and invokes that from inside the
// 'always_comb' block to test that. The task will be invoked whenever its
// arguments or the global variables are changed.

module test #(parameter p=8)
             (input clk, 
              input [p-1:0] in1, 
              input [0:p-1] in2, 
              output reg [p-1:0] out1, out2);

reg [p-1:0] r1, r2;
reg [p-1:0] r1_temp, r2_temp;
reg [p-1:0] out1_temp, out2_temp;
reg [0:p-1] r3, r4;
int trig_comb, trig_star;

// always_comb
always @(posedge clk)
begin
    task1(in1, in2, r1_temp);

    task1(r1, r2, out1_temp);
    r1 <= r1_temp;
    out1 <= out1_temp;
    trig_comb++;
end

// always@*
always @(posedge clk)
begin
    task1(in2, in1, r2_temp);

    task1(r2, r1, out2_temp);
    r2 <= r2_temp;
    out2 <= out2_temp;
    trig_star++;
end

always@(posedge clk)
begin
    r3 <= in1 + in2;
    r4 <= in2 - in1;
end

// task automatic task1(input int a, b, output int c);
// Carbon removed 'automatic', but this is not required for the test to work.
// (It was removed only because my sandbox currently incorrectly flags 'automatic' as an error.)   
task task1(input int a, b, output int c);   
    int t1, t2;

    t1 = a & b + r3;
    t2 = b | a ^ r4;
    
    c = t2 * t1;
endtask

endmodule

