
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines an always_comb block. A function is also
// defined in the design. This function is called from the always_comb block. The
// function uses unsized constants in a case statement and uses return to return
// value from it.

module test(i1, i2, o1, o2);

parameter WIDTH = 8;

input [WIDTH-1:0] i1, i2;
output reg [WIDTH-1:0] o1, o2;
reg [WIDTH-1:0] t1,t2;

function bit [7:0] fn(int x, int y);
    reg [7:0] r1,r2,r3,r4;
    reg [31:0] temp;
    temp = x|y;
    {r1, r2, r3, r4}=temp;
    casex ({temp[7], temp[15], temp[23], temp[31]})
        '1: return r1;
        '0: return r2;
        'x: return r3;
        'z: return r4;
        default: return '1;
    endcase
endfunction

always_comb
begin
        if (i2 != 0)
        begin
                t1 = (i1 + i2);
                o1 = (t1&=fn(i1,i2));
                t2 = (i1 - i2);
                t2 *= i2;
                o2 = i1 - t2;
        end
        else
        begin
                o1 = '1;
                o2 = '0;
        end
end

endmodule

