
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design contains two always blocks, the first one is
// triggered with positive edge of clk, while the second one is an always_comb.
// The variables manipulated in the first one is checked in the second block and
// depending on their values some values are driven to the output port. The first
// always block performs some arithmetic operations in two different ways and the
// second one checks whether the results are same.

module test(clk, i1, i2, i3, o1, o2, o3);

parameter WIDTH = 16;

input clk;
input signed [WIDTH-1:0] i1, i2, i3;
output reg o1, o2, o3;

reg [31:0] r1, r2;
reg signed [31:0] r3, r4;
reg signed [7:0] r5, r6;

always @(posedge clk)
begin
        r1 = i1 << WIDTH;
        r2 = fn(i1, WIDTH,'1);
        r3 = (i3 >> WIDTH) | (i2 << WIDTH);
        r4 = fn(i3, WIDTH, 0)|fn(i2,WIDTH,1);
        r5 = { i2, i3 }<<WIDTH;
        r6 = fn({i1,i2},WIDTH,1);

end

always_comb
begin
        if(r1 == r2)
            o1 ='1;
        else
            o1='0;

        if(r3 == r4)
            o2 = '1;
        else
            o2='0;

        if(r5 == r6)
            o3 = '1;
        else
            o3 = '0;
end

function int fn(int x, int y, bit z);
int t;
    t=x;
    for(int i= 0;i<WIDTH;i++)
    begin
        if(z)
            t <<=1;
        else
            t >>=1;
    end
    return t;
endfunction
endmodule

