
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): System Verilog provides a special 'always_comb' procedure
// for modeling combinational logic behavior. This design tests this.

module test(input a, b, c, d, output reg out);

logic t1, t2;

	always_comb
	begin
		t1 = a;
		t2 = c;
		t1 = t1 & b;
		t2 = t2 & d;
		out = t1 & t2;
	end

endmodule

