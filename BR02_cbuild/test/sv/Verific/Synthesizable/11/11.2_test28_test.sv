
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines four always_comb block to set values of
// four output port. Inside each always block, it uses nested for-loops. These loops 
// calculates and sets the value of the output port.

module test(input [3:0] i1, i2, i3, i4,output reg signed [3:0] o1, o2, o3, o4);

always_comb
begin
    for (int j=2 ;j >=0; j--)
    begin
        for(int k=2 ;k>=0; k--)
        begin
            o1[j^k] = i1[j];
        end
    end
end

always_comb
begin
    for (int j=2 ;j >=0; j--)
    begin
        for(int k=2 ;k>=0; k--)
        begin
            o2[j^k] = i2[j];
        end
    end
end

always_comb
begin
    for(int j=2 ;j >=0; j--)
    begin
        for (int k=2 ;k>=0; k--)
        begin
            o3[j^k] = i3[j];
        end
    end
end

always_comb
begin
    for(int j=2 ;j >=0; j--)
    begin
        for (int k=2 ;k>=0; k--)
        begin
            o4[j^k] = i4[j];
        end
    end
end

endmodule

