
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): System Verilog provides 'always_ff' procedure for modeling
// sequential logic behaviour. This design tests this.

module test(input clock, reset, set, data, output reg q);

    always_ff @(posedge clock iff reset ==  0 or negedge set)
        if (set)
           q = 1 ;
        else 
           /* The statement inside if should never be executated.
              The reset should go into the clock-enable of the synthesized design.
           if (reset)
             q = 0 ;
           else */
             q = data;

endmodule

