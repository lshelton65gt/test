
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog supports unsized literal single bit values with a
// preceding ', but without the base specifier. All bits of the unsized
// value are set to the value of the specified bit. This design forward
// defines a type and declares a data element of that type. It assigns
// value with preceding '. 

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    typedef logic_t;    // forward definition

    logic_t r1, r2;    // data element of forward defined type

    always@(posedge clk)
    begin
        r1 = '1; r2 = '0;    // assigned values using ' notation

        out2 = '0;
        out1 = '0;
        if (r1 == 8'b1111_1111)    // check the actual value assigned
            out1 = in2 - in1;

        if (r2 == 8'b0000_0000)    // check the actual value assigned
            out2 = in1 - in2;
    end

    // actual type defined here
    typedef logic [13:6] logic_t;

endmodule

