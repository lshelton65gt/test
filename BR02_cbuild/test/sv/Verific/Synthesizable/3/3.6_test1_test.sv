
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog string literals can contain special characters
// with preceding \ (backslash) like \n for newline, \t for tab etc. \ddd is treated as
// an octal number. This design tests this behaviour.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output logic [w-1:0] out1, out2);

    reg [7:0] r1, r2;

    always@(posedge clk)
    begin
        r1 = "\112";    // assign an escaped octal number
        r2 = 8'd74;     // assign an equvalent decimal number

        out2 = '0;
        out1 = '0;
        if (r1 == r2)    // check their equality
        begin
            out1 = in2 & in1;
            out2 = in1 - in2;
        end
    end

endmodule

