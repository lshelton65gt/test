
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design assigns/partially assigns to data elements inside 'case'
// statement or inside conditional statement which is in turn inside a
// 'for' loop. It uses 'Z literal in assignment.

module test #(parameter width=8)
             (input clk, 
              input [width+1:2] in1, 
              input [4:width+3] in2, 
              output reg [width+2:3] out1);

    typedef enum { LEFT = -1, MIDDLE, RIGHT } sides;

    generate
        genvar i;
        for(i=0; i<3; i++)
        begin: for_gen_i
            case (sides'(i-1)) /* synopsys full_case */
                LEFT:
                    always @(posedge clk)
                    begin
                        if (in1[2] > in2[4])
                            out1[3] = in2[width+3] ^ in1[width+1];
                        else
                            out1[3] = 'z;
     
                        if (in1[2] <= in2[4])
                            out1[3] = in2[4] & in1[2];
                    end
                MIDDLE:
                    always @(posedge clk)
                    begin
                        if (in2[width+3] < in1[width+1])
                            out1[width+2] = in2[4] * in1[2];
                        else
                            out1[width+2] = 'z;
                 
                        if (in2[width+3] >= in1[width+1])
                            out1[width+2] = in1[width+1] - in2[width+3];
                    end
                RIGHT:
                    always @(posedge clk)
                    begin
                        if (|in1[width:3] & ^in2[5:width+2])
                            out1[width+1:4] = in1[width:3] + in2[5:width+2];
                        else
                            out1[width+1:4] = 'z;
         
                        if (!(|in1[width:3] & ^in2[5:width+2]))
                            out1[width+1:4] = in1[width:3] | in2[5:width+2];
                    end
            endcase
        end
    endgenerate

endmodule

