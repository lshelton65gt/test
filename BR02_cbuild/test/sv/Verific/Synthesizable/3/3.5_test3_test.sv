
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules. The bottom module is
// instantiated with a time type data element connected to the port of the module.
// It checks whether time type port and the data element connected to it is properly
// handled by the tool.

module test (input clk,
             input [7:0] in1, in2,
             output reg [7:0] out1, out2);

    wire [63:0] temp;

    always@(posedge clk)
    begin
        out1 = temp[53:46];
        out2 = temp[25:18];
    end

    bottom bot (clk, { 8 {in1} }, temp);

    module bottom(input clk, input [63:0] in, output time out);

        always@(posedge clk)
        begin
            out = in;
        end

    endmodule

endmodule

