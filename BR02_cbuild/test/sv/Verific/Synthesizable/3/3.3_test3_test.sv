
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of logic literals 'X and 'Z.

module logic_literals(output [127:0] out1, out2, out3, out4,
                      output [15:0] out5, out6, out7, out8) ;


    assign out1 = 'X,
           out2 = {128{1'bX}},
           out3 = 'Z,
           out4 = {128{1'bZ}},
           out5 = 'X,
           out6 = {16{1'bX}},
           out7 = 'Z,
           out8 = {16{1'bZ}} ;

endmodule

