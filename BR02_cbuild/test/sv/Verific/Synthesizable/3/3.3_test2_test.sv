
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of logic literals 'x and 'z.

module logic_literals(output [67:0] out1, out2,
                      output [31:0] out3, out4,
                      output [7:0] out5, out6) ;

    assign out1 = 'x ,
           out2 = 'z ,
           out3 = 'x ,
           out4 = 'z ,
           out5 = 'x ,
           out6 = 'z ;

endmodule

