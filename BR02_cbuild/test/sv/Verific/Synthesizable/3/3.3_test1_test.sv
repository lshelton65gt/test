
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of integer literals '0 and '1.

module integer_literals(output reg [63:0] out1,
                        output reg [31:0] out2,
                        output reg [7:0] out3, 
                        input option) ;

    always @(option)
    begin
        if(option == 1)
        begin
            out1 = '1 ;
            out2 = '1 ;
            out3 = '1 ;
        end
        else
        begin
            out1 = '0 ;
            out2 = '0 ;
            out3 = '0 ;
        end
    end
endmodule

