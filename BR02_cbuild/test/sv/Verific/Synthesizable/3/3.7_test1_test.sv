
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If the type of the literal cannot be determined then
// the literal should be casted to the appropriate type.

module test(input bit clk, output bit b[1:0]) ;

    typedef bit duplet [1:2] ;

    always @ (posedge clk) 
     	b = duplet'{0, 1} ;

endmodule

