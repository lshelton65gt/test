
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of time literals. Use ms and s.

module test(output reg out1, out2, input in1, in2) ;

    reg tmp1, tmp2 ;

    always @(in1, in2)
    begin
        tmp1 = #1.23ms in1;
        #10.11ms out1 = tmp1; 
        tmp2 = #0.0806s in2;
        #6s out2 = tmp2 ;          
    end

endmodule

