
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of string literals. Use packed arrays to store string
// literals. Check that string literals are right justified when assigned to packed
// arrays.

module test(output bit [0:15][7:0] v,
            output bit [0:15][7:0] str);

    assign v = "Hello World" ,
           str = "String";

endmodule

