
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design assigns unsized literal 'z to target expressions.

module test #(parameter width=8)
             (input clk, 
              input [width+1:2] in1, 
              input [4:width+3] in2, 
              output reg [width+2:3] out1);

    always @(posedge clk)
    begin
        if (in1[2] > in2[4])
            out1[3] = in2[width+3] ^ in1[width+1];
        else
            out1[3] = 'z;

        if (in1[2] <= in2[4])
            out1[3] = in2[4] & in1[2];

        if (in2[width+3] > in1[width+1])
            out1[width+2] = in2[4] * in1[2];
        else
            out1[width+2] = 'z;

        if (in2[width+3] <= in1[width+1])
            out1[width+2] = in1[width+1] - in2[width+3];

        if (|in1[width:3] & ^in2[5:width+2])
            out1[width+1:4] = in1[width:3] + in2[5:width+2];
        else
            out1[width+1:4] = 'z;

        if (!(|in1[width:3] & ^in2[5:width+2]))
            out1[width+1:4] = in1[width:3] | in2[5:width+2];
    end

endmodule

