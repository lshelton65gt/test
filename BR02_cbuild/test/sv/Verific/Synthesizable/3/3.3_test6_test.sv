
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design assigns/partially assigns to data elements inside 'case'
// statement or inside conditional statement which is inside a 'for' loop.
// It uses 'Z literal in assignment. 

module test #(parameter width=8)
             (input clk, 
              input [width+1:2] in1, 
              input [4:width+3] in2, 
              output reg [width+2:3] out1);

    enum { LEFT = -1, MIDDLE, RIGHT } sides;

    always @(posedge clk)
    begin
        sides = LEFT;
        for(int i=0; i<3; i++)
        begin
            case (sides) /* synopsys full_case */
                LEFT:
                    begin
                        if (in1[2] > in2[4])
                            out1[3] = in2[width+3] ^ in1[width+1];
                        else
                            out1[3] = 'z;
     
                        if (in1[2] <= in2[4])
                            out1[3] = in2[4] & in1[2];

                        sides = RIGHT;
                    end
                MIDDLE:
                    begin
                        if (in2[width+3] < in1[width+1])
                            out1[width+2] = in2[4] * in1[2];
                        else
                            out1[width+2] = 'z;
                 
                        if (in2[width+3] >= in1[width+1])
                            out1[width+2] = in1[width+1] - in2[width+3];
     
                        sides = LEFT;
                    end
                RIGHT:
                    begin
                        if (|in1[width:3] & ^in2[5:width+2])
                            out1[width+1:4] = in1[width:3] + in2[5:width+2];
                        else
                            out1[width+1:4] = 'z;
         
                        if (!(|in1[width:3] & ^in2[5:width+2]))
                            out1[width+1:4] = in1[width:3] | in2[5:width+2];

                        sides = MIDDLE;
                    end
            endcase
        end
    end

endmodule

