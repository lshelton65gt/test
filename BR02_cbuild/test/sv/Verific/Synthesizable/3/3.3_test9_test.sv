
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses unsized binary and decimal literals. It
// models asynchronous reset block. On reset it initializes the outputs and control
// variables, other it carries out some calculations and sets the value to the
// output port.

module test(clk, reset, i1, o1, o2);

parameter WIDTH=32;

input clk, reset;
input signed [WIDTH-1:0] i1;
output reg signed [WIDTH-1:0] o1, o2;

int i;

always@(posedge clk or negedge reset)
begin
        if (!reset)
        begin
                i = 0;
                o1 = 'b0;
                o2 = 'b0;
        end
        else
        begin
                o1[i] = i1[i];
                o2 = (i1 >> (WIDTH-1-(i++)));
                if (i>WIDTH)
                begin
                        i = '0;
                end
        end
end

endmodule

