
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S):  Assigning '1 and 1'b1 to different variables of same length
// will lead to different values.

module literals(output [63:0] out1, out2,
                output [31:0] out3, out4,
                output [7:0] out5, out6) ;

    assign out1 = '1,
           out2 = 1'b1,
           out3 = 'x,
           out4 = 1'bx,
           out5 = 'z,
           out6 = 1'bz;

endmodule

