
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): $bits should return 1 for x and z, ie, the four state values.
// This design checks how many bits are required to store x or z.
// It uses 'x and 'z instead of 1'bx or 1'bz to check how $bits
// behave in such condition.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output [w-1:0] out1,
              output reg [w-1:0] out2);

    always@(posedge clk)
        out2 = (1==$bits('x)) ? (in1 ^ in2) : (in2 - in1);

    assign out1 = (1==$bits('z)) ? (in2 - in1) : (in1 ^ in2);

endmodule

