
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses '$left' system function on data elements declared
// of type typeparameter with unpacked dimension. The default type is
// 'logic' but it is overridden with a type that is typedefined in the
// other module and has one packed and other unpacked dimensions. So
// the declared data element has a total of three dimensions. It checks if
// the tool can detect the left bound of each dimension correctly.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output [w-1:0] out1, out2);

    /* new type having both packed and unpacked dimensions */
    typedef bit [2:0] pu_arr [0:1];

    module bottom #(parameter p = 8, parameter type t = logic)
                   (input clk,
                    input [p-1:0] in1, in2,
                    output reg [p-1:0] out1, out2);

        /* declare data element having unpacked dimension */
        t r1 [3:0];

        always@(posedge clk)
        begin
            out2='0;
            out1='0;
            if (3 == $left(r1, 1))    // $left should return 3 for dimension 1 
                out1 = '1;

            if (0 == $left(r1, 2))    // $left should return 0 for dimension 2
                out2 = '1;

            if (2 == $left(r1, 3))    // $left should return 2 for dimension 3
                out1 = '1;
        end

    endmodule

    bottom #(.t(pu_arr), .p(w)) bot2 (clk, in1, in2, out1, out2);

endmodule

