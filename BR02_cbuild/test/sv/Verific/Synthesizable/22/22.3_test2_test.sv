
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The bit data type loses X values. If these are to be preserved,
// the logic type should be used instead.

class Student ;

    string name ;
    int roll ;
    string address ;

    function new(string name, int roll, string a) ;
        this.name  = name ;
        this.roll = roll ;
        address = a ;
    endfunction

    function void show ;
        $display("name: %s, roll: %d, address: %s", name, roll, address) ;
    endfunction

endclass

module test ;

    Student s1 = new("qrewt", 'X,"sgdsj") , s2 ;
    typedef logic [$bits(s1) - 1 : 0] totalbits_student; 
    totalbits_student t ; 

    initial begin
        t = totalbits_student'(s1) ; 
        s2 = Student'(t) ;  
        $display("%s",s2.name) ;
       // s2 =new s1  ;  
        //s2.show ;
    end

endmodule

