
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The default port direction is inout in SystemVerilog. This
// design defines a port with default port direction and assigns some value to it.
// As it is of type inout so it is perfectly legal to do this.

module test(wire [0:3] in2, out2, input clk, input [0:3] in1, output [0:3] out1);

     assign out1 = in1 + out2,  // out2 is inout
            out2 = in2,
            in2 = in1;          // should not generate a warning as in2 is inout

endmodule

