
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design instantiates a module with .* and positional
// port connection. This is not allowed in SystemVerilog.

// Note: As per LRM 12.7.4 this is an error. However as per LRM IEEE-1800 sec 19.11.4 
// this is allowed.

typedef bit [0:7] MYTYPE1;
typedef enum {a,b,c,d,e,f,g,h} MYTYPE2;
MYTYPE1 in1, out1;
MYTYPE2 in2, out2;

module test (input MYTYPE1 in1, MYTYPE2 in2, output MYTYPE1 out1, MYTYPE2 out2);

bot i1 (.*, out1, out2);

endmodule

module bot(input MYTYPE1 in1, MYTYPE2 in2, output MYTYPE1 out1, MYTYPE2 out2);

    always @ (*)
    begin
        out1 = in1;
        out2 = in2;
    end

endmodule

