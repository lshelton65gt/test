
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows declaration outside a named module
// or interface. This is treated as global declaration. This can be a variable
// declaration or a task or function declaration or a type. This design declares
// a function outside of any module or interface, in the $root top level. This
// should be treated as global declaration.

function int global_func(input int i1, i2);
    int temp;

    temp = i1 * i2;
    temp |= i2;

    return temp;
endfunction

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

always @(posedge clk)
begin
    out1 = global_func(in1, in2);
    out2 = global_func(in2, in1);
end

endmodule


