
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two (nested) modules. The inner module is
// instantiated within the outer module in the scope where it is defined. The ports
// of this instantiation is connected using dot named port connection style.

module test(in1, in2, out1, clk);

    input in1, in2, clk;
    output out1;

	module bot (in1, in2, out1, clk);
		input in1, in2, clk;
		output reg out1;

		wire temp;
		assign temp = in1 & in2;

		always @ (posedge clk)
			out1 <= temp;
	endmodule

    bot i1 (.in1, .in2, .out1, .clk);

endmodule

