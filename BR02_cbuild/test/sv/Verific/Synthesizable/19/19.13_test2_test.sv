
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines 'for-generate' inside 'if-generate' construct. Two 
// 'for-generate' loops are there, one inside the true part of the 'if-generate'
// and the other in the false part. Both the 'for-generate' loops have the same block name.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [w-1:0] out1, out2);

    generate
    genvar i;

        if (w >= 8)
        begin
            for(i=0; i<w; i++)
            begin: for_gen_i
                always@(posedge clk)
                begin
                    out1[i] = in2[i] - in1[i];
                    out2[i] = in1[i] - in2[i];
                end
            end
        end
        else
        begin
            for(i=w-1; i>=0; --i)
            begin: for_gen_i
                always@(posedge clk)
                begin
                    out1[i] = in1[i] - in2[i];
                    out2[i] = in2[i] - in1[i];
                end
            end
        end

    endgenerate

endmodule

