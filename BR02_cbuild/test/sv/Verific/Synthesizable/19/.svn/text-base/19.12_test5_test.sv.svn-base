
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): An inout port can be connected to a variable (or concatenation)
// of same data type or left un-connected. This design connects one of the inout ports
// of an instantiation to a concatenation of wire data type element and the other is
// left un-connected.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output [width-1:0] out1, out2);

    wire [width/2-1:0] r1, r2;

    bot #(width) bot_1 (.clk(clk), .io1(), .io2({ r1, r2 }));  /* (1) open & (2) connect inout port to concatenation */

    assign out1 = { ~r2, r1 }, out2 = { r1, -r2 };

    module bot #(parameter width=4)
                (input clk,
                 inout [0:width-1] io1, io2);

        int temp;

        assign temp = io1,
               io1 = ~io2,
               io2 = -temp;

    endmodule

endmodule

