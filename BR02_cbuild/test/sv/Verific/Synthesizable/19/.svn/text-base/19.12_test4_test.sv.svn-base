
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): An inout port can be connected to a variable (or concatenation)
// of same data type. This design connects inout port of an instantiation to a concatenation.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    logic [width/2-1:0] r1, r2;

    bot #(width) bot_1 (clk, { r1, r2 });  /* connect inout port to concatenation */

    always@(posedge clk)
    begin
        out1 = { ~r2, r1 };
        out2 = { r1, -r2 };
    end

    module bot #(parameter width=4)
                (input clk,
                 inout [0:width-1] io);

        int temp;

        assign temp = ~io,
               io = -temp;

    endmodule

endmodule

