
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): A module or interface can be used prior to its declaration.
// This design defines an interface in the global $root scope and uses i.e.,
// instantiates that interface before its declaration.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output [width-1:0] out1, out2);

    ifc if1(clk);
    ifc if2(clk);

    internal intrnl1(if1, out2);
    internal intrnl2(if2, out1);

    always@(posedge clk)
    begin
        if1.in1 = ~in2;
        if1.in2 = -in1;
        if2.in1 = -in1;
        if2.in2 = ~in2;
    end

    module internal(interface i1, output reg [width-1:0] out1);
        always@(posedge i1.clk)
        begin
            out1 = i1.in1 ^ ~i1.in2;
        end
    endmodule

endmodule

interface ifc(input clk);
    reg [7:0] in1;
    reg [7:0] in2;
endinterface

