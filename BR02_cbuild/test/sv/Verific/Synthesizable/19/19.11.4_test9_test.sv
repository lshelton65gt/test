
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If implicit .* port connection is used, unconnected ports
// must be shown using named port connection. This design connects ports of an 
// instantiation using .* port connection while specifies open ports using named
// port connection.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    bit [width-1:0] i1;
    logic [0:width-1] i2, o1;

    test_bot #(width) bot_1 (.o2(), .*); /* o2 is remain open */

    always@(negedge clk)
    begin
        i1 = in1 - in2;
        i2 = in1 * in2;
    end

    always@(posedge clk)
    begin
        out1 = -o1;
        out2 = ~o1;
    end

    module test_bot #(parameter w = 4)
                     (input clk,
                      input bit [w-1:0] i1,
                      input logic [w-1:0] i2,
                      output logic [0:w-1] o1,
                      output bit [0:w-1] o2);

        always@(posedge clk)
        begin
            o1 = i1 + i2;
            o2 = i2 | i1;
        end

    endmodule

endmodule

