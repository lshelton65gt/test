
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows unpacked arrays to be used in ports.
// This design defines and instantiates module with unpacked array elements.

typedef logic UNPACKED_ARRAY[0:3];

module test (in1, out, clk);
    input [0:3] in1;
    input clk;
    output [0:7] out;
    
    UNPACKED_ARRAY temp1[0:1];
    logic temp2[0:1][0:3];

    assign {temp1[0][0], temp1[0][1], temp1[0][2], temp1[0][3]} = in1;
    assign {temp1[1][0], temp1[1][1], temp1[1][2], temp1[1][3]} = in1;
    assign out = {temp2[1][0], temp2[1][1], temp2[0][2], temp2[0][3]};

    bot i1 [0:1] (clk, temp1, temp2);

endmodule

module bot (input clk, input UNPACKED_ARRAY in1, output UNPACKED_ARRAY out1);

    always @ (posedge clk)
        out1 = in1;

endmodule

