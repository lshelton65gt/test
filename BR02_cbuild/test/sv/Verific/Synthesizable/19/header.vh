
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

`ifndef _HEADER_VH_
`define _HEADER_VH_

typedef reg [31:0] my_int_t ; // This is also a global typedef.
typedef bit [7:0] my_byte_t ; // This is a global typedef.

`endif

