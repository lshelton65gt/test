
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a structure type. Data element of the type is
// declared with unpacked range. An array of module instantiation is
// there. The data element is connected to that array of instantiation.

typedef struct packed
{
    int m1;
    int m2;
} my_struct_t;

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [w-1:0] out1, out2);

    my_struct_t st [0:1];
    wire [w-1:0] out [0:1];

    always@(negedge clk)
    begin
        st[0] = my_struct_t'(in2 + in1);
        st[1] = my_struct_t'(in2 ^ in1);
    end

    bottom #(.p(w)) bot0 [1:0] (clk, st, out);

    always@(posedge clk)
    begin
        out1 = out[0] - out[1];
        out2 = out[1] - out[0];
    end

endmodule

module bottom #(parameter p = 4)
               (input clk,
                input my_struct_t in,
                output reg [p-1:0] out);

    always@(posedge clk)
    begin
        out = ~in.m1 ^ in.m2;
    end

endmodule

