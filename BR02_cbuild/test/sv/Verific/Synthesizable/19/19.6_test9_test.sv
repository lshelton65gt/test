
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// Module definitions can be nested. The inner module can access
// elements of the outer module. This design defines some nested
// module and instantites that module and also uses elements of
// outer module.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output [width-1:0] out1, out2);

    reg [width-1:0] r0_1;
    reg [width-1:0] r0_2;

    always@(in1 or in2)
    begin
        r0_1 = in1 | in2;
        r0_2 = in1 ^ in2;
    end

    internal1 #(width) intr1(in1, in2, out1, out2);

    module internal1 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r1_1;
    reg [width-1:0] r1_2;

    always@(r0_1 or r0_2)
    begin
        r1_1 = r0_1 | r0_2;
        r1_2 = r0_2 ^ r0_1;
    end

    internal2 #(width) intr2(in1, in2, out1, out2);

    module internal2 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r2_1;
    reg [width-1:0] r2_2;

    always@(r1_1 or r1_2)
    begin
        r2_1 = r1_1 | r1_2;
        r2_2 = r1_2 ^ r1_1;
    end

    internal3 #(width) intr3(in1, in2, out1, out2);

    module internal3 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r3_1;
    reg [width-1:0] r3_2;

    always@(r2_1 or r2_2)
    begin
        r3_1 = r2_1 | r2_2;
        r3_2 = r2_2 ^ r2_1;
    end

    internal4 #(width) intr4(in1, in2, out1, out2);

    module internal4 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r4_1;
    reg [width-1:0] r4_2;

    always@(r3_1 or r3_2)
    begin
        r4_1 = r3_1 | r3_2;
        r4_2 = r3_2 ^ r3_1;
    end

    internal5 #(width) intr5(in1, in2, out1, out2);

    module internal5 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r5_1;
    reg [width-1:0] r5_2;

    always@(r4_1 or r4_2)
    begin
        r5_1 = r4_1 | r4_2;
        r5_2 = r4_2 ^ r4_1;
    end

    internal6 #(width) intr6(in1, in2, out1, out2);

    module internal6 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r6_1;
    reg [width-1:0] r6_2;

    always@(r5_1 or r5_2)
    begin
        r6_1 = r5_1 | r5_2;
        r6_2 = r5_2 ^ r5_1;
    end

    internal7 #(width) intr7(in1, in2, out1, out2);

    module internal7 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r7_1;
    reg [width-1:0] r7_2;

    always@(r6_1 or r6_2)
    begin
        r7_1 = r6_1 | r6_2;
        r7_2 = r6_2 ^ r6_1;
    end

    internal8 #(width) intr8(in1, in2, out1, out2);

    module internal8 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r8_1;
    reg [width-1:0] r8_2;

    always@(r7_1 or r7_2)
    begin
        r8_1 = r7_1 | r7_2;
        r8_2 = r7_2 ^ r7_1;
    end

    internal9 #(width) intr9(in1, in2, out1, out2);

    module internal9 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r9_1;
    reg [width-1:0] r9_2;

    always@(r8_1 or r8_2)
    begin
        r9_1 = r8_1 | r8_2;
        r9_2 = r8_2 ^ r8_1;
    end

    internal10 #(width) intr10(in1, in2, out1, out2);

    module internal10 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r10_1;
    reg [width-1:0] r10_2;

    always@(r9_1 or r9_2)
    begin
        r10_1 = r9_1 | r9_2;
        r10_2 = r9_2 ^ r9_1;
    end

    internal11 #(width) intr11(in1, in2, out1, out2);

    module internal11 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r11_1;
    reg [width-1:0] r11_2;

    always@(r10_1 or r10_2)
    begin
        r11_1 = r10_1 | r10_2;
        r11_2 = r10_2 ^ r10_1;
    end

    internal12 #(width) intr12(in1, in2, out1, out2);

    module internal12 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r12_1;
    reg [width-1:0] r12_2;

    always@(r11_1 or r11_2)
    begin
        r12_1 = r11_1 | r11_2;
        r12_2 = r11_2 ^ r11_1;
    end

    internal13 #(width) intr13(in1, in2, out1, out2);

    module internal13 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r13_1;
    reg [width-1:0] r13_2;

    always@(r12_1 or r12_2)
    begin
        r13_1 = r12_1 | r12_2;
        r13_2 = r12_2 ^ r12_1;
    end

    internal14 #(width) intr14(in1, in2, out1, out2);

    module internal14 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r14_1;
    reg [width-1:0] r14_2;

    always@(r13_1 or r13_2)
    begin
        r14_1 = r13_1 | r13_2;
        r14_2 = r13_2 ^ r13_1;
    end

    internal15 #(width) intr15(in1, in2, out1, out2);

    module internal15 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r15_1;
    reg [width-1:0] r15_2;

    always@(r14_1 or r14_2)
    begin
        r15_1 = r14_1 | r14_2;
        r15_2 = r14_2 ^ r14_1;
    end

    internal16 #(width) intr16(in1, in2, out1, out2);

    module internal16 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r16_1;
    reg [width-1:0] r16_2;

    always@(r15_1 or r15_2)
    begin
        r16_1 = r15_1 | r15_2;
        r16_2 = r15_2 ^ r15_1;
    end

    internal17 #(width) intr17(in1, in2, out1, out2);

    module internal17 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r17_1;
    reg [width-1:0] r17_2;

    always@(r16_1 or r16_2)
    begin
        r17_1 = r16_1 | r16_2;
        r17_2 = r16_2 ^ r16_1;
    end

    internal18 #(width) intr18(in1, in2, out1, out2);

    module internal18 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r18_1;
    reg [width-1:0] r18_2;

    always@(r17_1 or r17_2)
    begin
        r18_1 = r17_1 | r17_2;
        r18_2 = r17_2 ^ r17_1;
    end

    internal19 #(width) intr19(in1, in2, out1, out2);

    module internal19 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r19_1;
    reg [width-1:0] r19_2;

    always@(r18_1 or r18_2)
    begin
        r19_1 = r18_1 | r18_2;
        r19_2 = r18_2 ^ r18_1;
    end

    internal20 #(width) intr20(in1, in2, out1, out2);

    module internal20 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r20_1;
    reg [width-1:0] r20_2;

    always@(r19_1 or r19_2)
    begin
        r20_1 = r19_1 | r19_2;
        r20_2 = r19_2 ^ r19_1;
    end

    internal21 #(width) intr21(in1, in2, out1, out2);

    module internal21 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r21_1;
    reg [width-1:0] r21_2;

    always@(r20_1 or r20_2)
    begin
        r21_1 = r20_1 | r20_2;
        r21_2 = r20_2 ^ r20_1;
    end

    internal22 #(width) intr22(in1, in2, out1, out2);

    module internal22 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r22_1;
    reg [width-1:0] r22_2;

    always@(r21_1 or r21_2)
    begin
        r22_1 = r21_1 | r21_2;
        r22_2 = r21_2 ^ r21_1;
    end

    internal23 #(width) intr23(in1, in2, out1, out2);

    module internal23 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r23_1;
    reg [width-1:0] r23_2;

    always@(r22_1 or r22_2)
    begin
        r23_1 = r22_1 | r22_2;
        r23_2 = r22_2 ^ r22_1;
    end

    internal24 #(width) intr24(in1, in2, out1, out2);

    module internal24 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r24_1;
    reg [width-1:0] r24_2;

    always@(r23_1 or r23_2)
    begin
        r24_1 = r23_1 | r23_2;
        r24_2 = r23_2 ^ r23_1;
    end

    internal25 #(width) intr25(in1, in2, out1, out2);

    module internal25 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r25_1;
    reg [width-1:0] r25_2;

    always@(r24_1 or r24_2)
    begin
        r25_1 = r24_1 | r24_2;
        r25_2 = r24_2 ^ r24_1;
    end

    internal26 #(width) intr26(in1, in2, out1, out2);

    module internal26 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r26_1;
    reg [width-1:0] r26_2;

    always@(r25_1 or r25_2)
    begin
        r26_1 = r25_1 | r25_2;
        r26_2 = r25_2 ^ r25_1;
    end

    internal27 #(width) intr27(in1, in2, out1, out2);

    module internal27 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r27_1;
    reg [width-1:0] r27_2;

    always@(r26_1 or r26_2)
    begin
        r27_1 = r26_1 | r26_2;
        r27_2 = r26_2 ^ r26_1;
    end

    internal28 #(width) intr28(in1, in2, out1, out2);

    module internal28 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r28_1;
    reg [width-1:0] r28_2;

    always@(r27_1 or r27_2)
    begin
        r28_1 = r27_1 | r27_2;
        r28_2 = r27_2 ^ r27_1;
    end

    internal29 #(width) intr29(in1, in2, out1, out2);

    module internal29 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r29_1;
    reg [width-1:0] r29_2;

    always@(r28_1 or r28_2)
    begin
        r29_1 = r28_1 | r28_2;
        r29_2 = r28_2 ^ r28_1;
    end

    internal30 #(width) intr30(in1, in2, out1, out2);

    module internal30 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r30_1;
    reg [width-1:0] r30_2;

    always@(r29_1 or r29_2)
    begin
        r30_1 = r29_1 | r29_2;
        r30_2 = r29_2 ^ r29_1;
    end

    internal31 #(width) intr31(in1, in2, out1, out2);

    module internal31 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r31_1;
    reg [width-1:0] r31_2;

    always@(r30_1 or r30_2)
    begin
        r31_1 = r30_1 | r30_2;
        r31_2 = r30_2 ^ r30_1;
    end

    internal32 #(width) intr32(in1, in2, out1, out2);

    module internal32 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r32_1;
    reg [width-1:0] r32_2;

    always@(r31_1 or r31_2)
    begin
        r32_1 = r31_1 | r31_2;
        r32_2 = r31_2 ^ r31_1;
    end

    internal33 #(width) intr33(in1, in2, out1, out2);

    module internal33 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r33_1;
    reg [width-1:0] r33_2;

    always@(r32_1 or r32_2)
    begin
        r33_1 = r32_1 | r32_2;
        r33_2 = r32_2 ^ r32_1;
    end

    internal34 #(width) intr34(in1, in2, out1, out2);

    module internal34 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r34_1;
    reg [width-1:0] r34_2;

    always@(r33_1 or r33_2)
    begin
        r34_1 = r33_1 | r33_2;
        r34_2 = r33_2 ^ r33_1;
    end

    internal35 #(width) intr35(in1, in2, out1, out2);

    module internal35 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r35_1;
    reg [width-1:0] r35_2;

    always@(r34_1 or r34_2)
    begin
        r35_1 = r34_1 | r34_2;
        r35_2 = r34_2 ^ r34_1;
    end

    internal36 #(width) intr36(in1, in2, out1, out2);

    module internal36 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r36_1;
    reg [width-1:0] r36_2;

    always@(r35_1 or r35_2)
    begin
        r36_1 = r35_1 | r35_2;
        r36_2 = r35_2 ^ r35_1;
    end

    internal37 #(width) intr37(in1, in2, out1, out2);

    module internal37 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r37_1;
    reg [width-1:0] r37_2;

    always@(r36_1 or r36_2)
    begin
        r37_1 = r36_1 | r36_2;
        r37_2 = r36_2 ^ r36_1;
    end

    internal38 #(width) intr38(in1, in2, out1, out2);

    module internal38 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r38_1;
    reg [width-1:0] r38_2;

    always@(r37_1 or r37_2)
    begin
        r38_1 = r37_1 | r37_2;
        r38_2 = r37_2 ^ r37_1;
    end

    internal39 #(width) intr39(in1, in2, out1, out2);

    module internal39 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r39_1;
    reg [width-1:0] r39_2;

    always@(r38_1 or r38_2)
    begin
        r39_1 = r38_1 | r38_2;
        r39_2 = r38_2 ^ r38_1;
    end

    internal40 #(width) intr40(in1, in2, out1, out2);

    module internal40 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r40_1;
    reg [width-1:0] r40_2;

    always@(r39_1 or r39_2)
    begin
        r40_1 = r39_1 | r39_2;
        r40_2 = r39_2 ^ r39_1;
    end

    internal41 #(width) intr41(in1, in2, out1, out2);

    module internal41 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r41_1;
    reg [width-1:0] r41_2;

    always@(r40_1 or r40_2)
    begin
        r41_1 = r40_1 | r40_2;
        r41_2 = r40_2 ^ r40_1;
    end

    internal42 #(width) intr42(in1, in2, out1, out2);

    module internal42 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r42_1;
    reg [width-1:0] r42_2;

    always@(r41_1 or r41_2)
    begin
        r42_1 = r41_1 | r41_2;
        r42_2 = r41_2 ^ r41_1;
    end

    internal43 #(width) intr43(in1, in2, out1, out2);

    module internal43 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r43_1;
    reg [width-1:0] r43_2;

    always@(r42_1 or r42_2)
    begin
        r43_1 = r42_1 | r42_2;
        r43_2 = r42_2 ^ r42_1;
    end

    internal44 #(width) intr44(in1, in2, out1, out2);

    module internal44 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r44_1;
    reg [width-1:0] r44_2;

    always@(r43_1 or r43_2)
    begin
        r44_1 = r43_1 | r43_2;
        r44_2 = r43_2 ^ r43_1;
    end

    internal45 #(width) intr45(in1, in2, out1, out2);

    module internal45 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r45_1;
    reg [width-1:0] r45_2;

    always@(r44_1 or r44_2)
    begin
        r45_1 = r44_1 | r44_2;
        r45_2 = r44_2 ^ r44_1;
    end

    internal46 #(width) intr46(in1, in2, out1, out2);

    module internal46 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r46_1;
    reg [width-1:0] r46_2;

    always@(r45_1 or r45_2)
    begin
        r46_1 = r45_1 | r45_2;
        r46_2 = r45_2 ^ r45_1;
    end

    internal47 #(width) intr47(in1, in2, out1, out2);

    module internal47 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r47_1;
    reg [width-1:0] r47_2;

    always@(r46_1 or r46_2)
    begin
        r47_1 = r46_1 | r46_2;
        r47_2 = r46_2 ^ r46_1;
    end

    internal48 #(width) intr48(in1, in2, out1, out2);

    module internal48 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r48_1;
    reg [width-1:0] r48_2;

    always@(r47_1 or r47_2)
    begin
        r48_1 = r47_1 | r47_2;
        r48_2 = r47_2 ^ r47_1;
    end

    internal49 #(width) intr49(in1, in2, out1, out2);

    module internal49 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r49_1;
    reg [width-1:0] r49_2;

    always@(r48_1 or r48_2)
    begin
        r49_1 = r48_1 | r48_2;
        r49_2 = r48_2 ^ r48_1;
    end

    internal50 #(width) intr50(in1, in2, out1, out2);

    module internal50 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r50_1;
    reg [width-1:0] r50_2;

    always@(r49_1 or r49_2)
    begin
        r50_1 = r49_1 | r49_2;
        r50_2 = r49_2 ^ r49_1;
    end

    internal51 #(width) intr51(in1, in2, out1, out2);

    module internal51 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r51_1;
    reg [width-1:0] r51_2;

    always@(r50_1 or r50_2)
    begin
        r51_1 = r50_1 | r50_2;
        r51_2 = r50_2 ^ r50_1;
    end

    internal52 #(width) intr52(in1, in2, out1, out2);

    module internal52 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r52_1;
    reg [width-1:0] r52_2;

    always@(r51_1 or r51_2)
    begin
        r52_1 = r51_1 | r51_2;
        r52_2 = r51_2 ^ r51_1;
    end

    internal53 #(width) intr53(in1, in2, out1, out2);

    module internal53 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r53_1;
    reg [width-1:0] r53_2;

    always@(r52_1 or r52_2)
    begin
        r53_1 = r52_1 | r52_2;
        r53_2 = r52_2 ^ r52_1;
    end

    internal54 #(width) intr54(in1, in2, out1, out2);

    module internal54 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r54_1;
    reg [width-1:0] r54_2;

    always@(r53_1 or r53_2)
    begin
        r54_1 = r53_1 | r53_2;
        r54_2 = r53_2 ^ r53_1;
    end

    internal55 #(width) intr55(in1, in2, out1, out2);

    module internal55 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r55_1;
    reg [width-1:0] r55_2;

    always@(r54_1 or r54_2)
    begin
        r55_1 = r54_1 | r54_2;
        r55_2 = r54_2 ^ r54_1;
    end

    internal56 #(width) intr56(in1, in2, out1, out2);

    module internal56 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r56_1;
    reg [width-1:0] r56_2;

    always@(r55_1 or r55_2)
    begin
        r56_1 = r55_1 | r55_2;
        r56_2 = r55_2 ^ r55_1;
    end

    internal57 #(width) intr57(in1, in2, out1, out2);

    module internal57 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r57_1;
    reg [width-1:0] r57_2;

    always@(r56_1 or r56_2)
    begin
        r57_1 = r56_1 | r56_2;
        r57_2 = r56_2 ^ r56_1;
    end

    internal58 #(width) intr58(in1, in2, out1, out2);

    module internal58 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r58_1;
    reg [width-1:0] r58_2;

    always@(r57_1 or r57_2)
    begin
        r58_1 = r57_1 | r57_2;
        r58_2 = r57_2 ^ r57_1;
    end

    internal59 #(width) intr59(in1, in2, out1, out2);

    module internal59 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r59_1;
    reg [width-1:0] r59_2;

    always@(r58_1 or r58_2)
    begin
        r59_1 = r58_1 | r58_2;
        r59_2 = r58_2 ^ r58_1;
    end

    internal60 #(width) intr60(in1, in2, out1, out2);

    module internal60 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r60_1;
    reg [width-1:0] r60_2;

    always@(r59_1 or r59_2)
    begin
        r60_1 = r59_1 | r59_2;
        r60_2 = r59_2 ^ r59_1;
    end

    internal61 #(width) intr61(in1, in2, out1, out2);

    module internal61 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r61_1;
    reg [width-1:0] r61_2;

    always@(r60_1 or r60_2)
    begin
        r61_1 = r60_1 | r60_2;
        r61_2 = r60_2 ^ r60_1;
    end

    internal62 #(width) intr62(in1, in2, out1, out2);

    module internal62 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r62_1;
    reg [width-1:0] r62_2;

    always@(r61_1 or r61_2)
    begin
        r62_1 = r61_1 | r61_2;
        r62_2 = r61_2 ^ r61_1;
    end

    internal63 #(width) intr63(in1, in2, out1, out2);

    module internal63 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r63_1;
    reg [width-1:0] r63_2;

    always@(r62_1 or r62_2)
    begin
        r63_1 = r62_1 | r62_2;
        r63_2 = r62_2 ^ r62_1;
    end

    internal64 #(width) intr64(in1, in2, out1, out2);

    module internal64 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r64_1;
    reg [width-1:0] r64_2;

    always@(r63_1 or r63_2)
    begin
        r64_1 = r63_1 | r63_2;
        r64_2 = r63_2 ^ r63_1;
    end

    internal65 #(width) intr65(in1, in2, out1, out2);

    module internal65 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r65_1;
    reg [width-1:0] r65_2;

    always@(r64_1 or r64_2)
    begin
        r65_1 = r64_1 | r64_2;
        r65_2 = r64_2 ^ r64_1;
    end

    internal66 #(width) intr66(in1, in2, out1, out2);

    module internal66 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r66_1;
    reg [width-1:0] r66_2;

    always@(r65_1 or r65_2)
    begin
        r66_1 = r65_1 | r65_2;
        r66_2 = r65_2 ^ r65_1;
    end

    internal67 #(width) intr67(in1, in2, out1, out2);

    module internal67 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r67_1;
    reg [width-1:0] r67_2;

    always@(r66_1 or r66_2)
    begin
        r67_1 = r66_1 | r66_2;
        r67_2 = r66_2 ^ r66_1;
    end

    internal68 #(width) intr68(in1, in2, out1, out2);

    module internal68 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r68_1;
    reg [width-1:0] r68_2;

    always@(r67_1 or r67_2)
    begin
        r68_1 = r67_1 | r67_2;
        r68_2 = r67_2 ^ r67_1;
    end

    internal69 #(width) intr69(in1, in2, out1, out2);

    module internal69 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r69_1;
    reg [width-1:0] r69_2;

    always@(r68_1 or r68_2)
    begin
        r69_1 = r68_1 | r68_2;
        r69_2 = r68_2 ^ r68_1;
    end

    internal70 #(width) intr70(in1, in2, out1, out2);

    module internal70 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r70_1;
    reg [width-1:0] r70_2;

    always@(r69_1 or r69_2)
    begin
        r70_1 = r69_1 | r69_2;
        r70_2 = r69_2 ^ r69_1;
    end

    internal71 #(width) intr71(in1, in2, out1, out2);

    module internal71 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r71_1;
    reg [width-1:0] r71_2;

    always@(r70_1 or r70_2)
    begin
        r71_1 = r70_1 | r70_2;
        r71_2 = r70_2 ^ r70_1;
    end

    internal72 #(width) intr72(in1, in2, out1, out2);

    module internal72 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r72_1;
    reg [width-1:0] r72_2;

    always@(r71_1 or r71_2)
    begin
        r72_1 = r71_1 | r71_2;
        r72_2 = r71_2 ^ r71_1;
    end

    internal73 #(width) intr73(in1, in2, out1, out2);

    module internal73 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r73_1;
    reg [width-1:0] r73_2;

    always@(r72_1 or r72_2)
    begin
        r73_1 = r72_1 | r72_2;
        r73_2 = r72_2 ^ r72_1;
    end

    internal74 #(width) intr74(in1, in2, out1, out2);

    module internal74 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r74_1;
    reg [width-1:0] r74_2;

    always@(r73_1 or r73_2)
    begin
        r74_1 = r73_1 | r73_2;
        r74_2 = r73_2 ^ r73_1;
    end

    internal75 #(width) intr75(in1, in2, out1, out2);

    module internal75 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r75_1;
    reg [width-1:0] r75_2;

    always@(r74_1 or r74_2)
    begin
        r75_1 = r74_1 | r74_2;
        r75_2 = r74_2 ^ r74_1;
    end

    internal76 #(width) intr76(in1, in2, out1, out2);

    module internal76 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r76_1;
    reg [width-1:0] r76_2;

    always@(r75_1 or r75_2)
    begin
        r76_1 = r75_1 | r75_2;
        r76_2 = r75_2 ^ r75_1;
    end

    internal77 #(width) intr77(in1, in2, out1, out2);

    module internal77 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r77_1;
    reg [width-1:0] r77_2;

    always@(r76_1 or r76_2)
    begin
        r77_1 = r76_1 | r76_2;
        r77_2 = r76_2 ^ r76_1;
    end

    internal78 #(width) intr78(in1, in2, out1, out2);

    module internal78 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r78_1;
    reg [width-1:0] r78_2;

    always@(r77_1 or r77_2)
    begin
        r78_1 = r77_1 | r77_2;
        r78_2 = r77_2 ^ r77_1;
    end

    internal79 #(width) intr79(in1, in2, out1, out2);

    module internal79 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r79_1;
    reg [width-1:0] r79_2;

    always@(r78_1 or r78_2)
    begin
        r79_1 = r78_1 | r78_2;
        r79_2 = r78_2 ^ r78_1;
    end

    internal80 #(width) intr80(in1, in2, out1, out2);

    module internal80 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r80_1;
    reg [width-1:0] r80_2;

    always@(r79_1 or r79_2)
    begin
        r80_1 = r79_1 | r79_2;
        r80_2 = r79_2 ^ r79_1;
    end

    internal81 #(width) intr81(in1, in2, out1, out2);

    module internal81 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r81_1;
    reg [width-1:0] r81_2;

    always@(r80_1 or r80_2)
    begin
        r81_1 = r80_1 | r80_2;
        r81_2 = r80_2 ^ r80_1;
    end

    internal82 #(width) intr82(in1, in2, out1, out2);

    module internal82 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r82_1;
    reg [width-1:0] r82_2;

    always@(r81_1 or r81_2)
    begin
        r82_1 = r81_1 | r81_2;
        r82_2 = r81_2 ^ r81_1;
    end

    internal83 #(width) intr83(in1, in2, out1, out2);

    module internal83 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r83_1;
    reg [width-1:0] r83_2;

    always@(r82_1 or r82_2)
    begin
        r83_1 = r82_1 | r82_2;
        r83_2 = r82_2 ^ r82_1;
    end

    internal84 #(width) intr84(in1, in2, out1, out2);

    module internal84 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r84_1;
    reg [width-1:0] r84_2;

    always@(r83_1 or r83_2)
    begin
        r84_1 = r83_1 | r83_2;
        r84_2 = r83_2 ^ r83_1;
    end

    internal85 #(width) intr85(in1, in2, out1, out2);

    module internal85 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r85_1;
    reg [width-1:0] r85_2;

    always@(r84_1 or r84_2)
    begin
        r85_1 = r84_1 | r84_2;
        r85_2 = r84_2 ^ r84_1;
    end

    internal86 #(width) intr86(in1, in2, out1, out2);

    module internal86 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r86_1;
    reg [width-1:0] r86_2;

    always@(r85_1 or r85_2)
    begin
        r86_1 = r85_1 | r85_2;
        r86_2 = r85_2 ^ r85_1;
    end

    internal87 #(width) intr87(in1, in2, out1, out2);

    module internal87 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r87_1;
    reg [width-1:0] r87_2;

    always@(r86_1 or r86_2)
    begin
        r87_1 = r86_1 | r86_2;
        r87_2 = r86_2 ^ r86_1;
    end

    internal88 #(width) intr88(in1, in2, out1, out2);

    module internal88 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r88_1;
    reg [width-1:0] r88_2;

    always@(r87_1 or r87_2)
    begin
        r88_1 = r87_1 | r87_2;
        r88_2 = r87_2 ^ r87_1;
    end

    internal89 #(width) intr89(in1, in2, out1, out2);

    module internal89 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r89_1;
    reg [width-1:0] r89_2;

    always@(r88_1 or r88_2)
    begin
        r89_1 = r88_1 | r88_2;
        r89_2 = r88_2 ^ r88_1;
    end

    internal90 #(width) intr90(in1, in2, out1, out2);

    module internal90 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r90_1;
    reg [width-1:0] r90_2;

    always@(r89_1 or r89_2)
    begin
        r90_1 = r89_1 | r89_2;
        r90_2 = r89_2 ^ r89_1;
    end

    internal91 #(width) intr91(in1, in2, out1, out2);

    module internal91 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r91_1;
    reg [width-1:0] r91_2;

    always@(r90_1 or r90_2)
    begin
        r91_1 = r90_1 | r90_2;
        r91_2 = r90_2 ^ r90_1;
    end

    internal92 #(width) intr92(in1, in2, out1, out2);

    module internal92 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r92_1;
    reg [width-1:0] r92_2;

    always@(r91_1 or r91_2)
    begin
        r92_1 = r91_1 | r91_2;
        r92_2 = r91_2 ^ r91_1;
    end

    internal93 #(width) intr93(in1, in2, out1, out2);

    module internal93 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r93_1;
    reg [width-1:0] r93_2;

    always@(r92_1 or r92_2)
    begin
        r93_1 = r92_1 | r92_2;
        r93_2 = r92_2 ^ r92_1;
    end

    internal94 #(width) intr94(in1, in2, out1, out2);

    module internal94 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r94_1;
    reg [width-1:0] r94_2;

    always@(r93_1 or r93_2)
    begin
        r94_1 = r93_1 | r93_2;
        r94_2 = r93_2 ^ r93_1;
    end

    internal95 #(width) intr95(in1, in2, out1, out2);

    module internal95 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r95_1;
    reg [width-1:0] r95_2;

    always@(r94_1 or r94_2)
    begin
        r95_1 = r94_1 | r94_2;
        r95_2 = r94_2 ^ r94_1;
    end

    internal96 #(width) intr96(in1, in2, out1, out2);

    module internal96 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r96_1;
    reg [width-1:0] r96_2;

    always@(r95_1 or r95_2)
    begin
        r96_1 = r95_1 | r95_2;
        r96_2 = r95_2 ^ r95_1;
    end

    internal97 #(width) intr97(in1, in2, out1, out2);

    module internal97 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r97_1;
    reg [width-1:0] r97_2;

    always@(r96_1 or r96_2)
    begin
        r97_1 = r96_1 | r96_2;
        r97_2 = r96_2 ^ r96_1;
    end

    internal98 #(width) intr98(in1, in2, out1, out2);

    module internal98 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r98_1;
    reg [width-1:0] r98_2;

    always@(r97_1 or r97_2)
    begin
        r98_1 = r97_1 | r97_2;
        r98_2 = r97_2 ^ r97_1;
    end

    internal99 #(width) intr99(in1, in2, out1, out2);

    module internal99 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

    reg [width-1:0] r99_1;
    reg [width-1:0] r99_2;

    always@(r98_1 or r98_2)
    begin
        r99_1 = r98_1 | r98_2;
        r99_2 = r98_2 ^ r98_1;
    end

    internal100 #(width) intr100(in1, in2, out1, out2);

    module internal100 #(parameter width = 8) (input [width-1:0] in1, in2, output [0:width-1] out1, out2);

        assign out1 = in1 | in2 & r99_1;
        assign out2 = in1 ^ in2 + r99_2;

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

    endmodule

endmodule

