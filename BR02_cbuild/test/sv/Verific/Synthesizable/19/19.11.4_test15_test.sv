
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules. The bottom module is instantiated
// from the other module. The ports are connected by implicit .* port connection rule.
// In the bottom module uses post increment operator ++ on an array index directly.

module test(clk, i1, i2, i3, i4, i5, i6, i7, o1, o2);

input clk;
input [31:0] i1;
input signed [31:0] i2;
input [31:0] i3;
input signed [31:0] i4;
input i5;
input signed  i6;
input i7;
output signed [31:0] o1;
output reg signed o2;

reg l1;
reg signed l2;
reg signed l3;
reg signed l4;

bottom b1(.*);
always @(posedge clk)
begin
    l1 = i1 & i2;
    l2 = (i4 | i5) ^ ~i6;
    l3 = ((i7==i5)?(i1<=i2):(i3>i4));
    l4 = ((i1!=i6)?(i1<i3):(i5>=i7));

    o2 =(l1 & 1) | !(l2 | 1);
end

endmodule

module bottom(clk, i1, i2, i3, i4, o1);
input clk;
input int i1,i2,i3,i4;
output int o1;
int i = 0,r1;
always @(posedge clk)
begin
    r1= ((i1 + i2)<<2) - i1 - i2 + i3*i4;
    o1=r1[i++];
end
endmodule

