
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of timeunit keyword.

module test(output reg [7:0] out, input [7:0] in) ;

    timeunit 1ns ;
    timeprecision 1ns ;

    always @(in)
        out = #10.1 in ; // literal 10.1 will be multiplied by timeunit

endmodule

