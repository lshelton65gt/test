
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design instantiates a nested module(bot) within a generate block.

module test(in1, in2, out1);
parameter width = 8;
input  [width-1 : 0] in1;
input  [width-1 : 0] in2;
output [width-1 : 0] out1;

genvar i;
    generate 
            for( i = 0; i < width; i = i + 1)
            begin : b1
                bot inst (out1[i], in1[i], in2[i]);
            end
    endgenerate

    module bot (out, in1, in2);
        output out;
        input in1, in2;

        assign out = in1 & in2;
    endmodule

endmodule

