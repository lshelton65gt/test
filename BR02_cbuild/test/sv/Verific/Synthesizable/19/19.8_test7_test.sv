
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Direction for in2 has not been specified. Hence in2
// is wire and its direction is inout

`default_nettype wire

module test(wire [0:3] in2, output reg [0:3] out2, input clk, input [0:3] in1, output reg [0:3] out1);

	always @ (posedge clk)
	begin
		out1 <= in1; 
		out2 <= in2;
	end

    assign in2 = in1; // should not generate a warning as in2 is inout

endmodule

