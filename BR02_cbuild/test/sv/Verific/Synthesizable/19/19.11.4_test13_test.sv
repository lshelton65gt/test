
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Implicit .* port connection can be used with implicit .name
// port connection in the same instantiation. This design connects ports of an
// instantiation using a mix of mix of .* and .name port connection.

// Note: According to LRM 12.7.4 this is an error. However according to section
// 19.11.4 of LRM P!800 this is allowed.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    logic [width-1:0] i1, o1;
    bit [0:width-1] i2, o2;

    test_bot #(width) bot_1 (.*, .clk);

    always@(negedge clk)
    begin
        i1 = in1 + -in2;
        i2 = ~in1 - in2;
    end

    always@(posedge clk)
    begin
        out1 = o2 | ~o1;
        out2 = -o1 & o2;
    end

    module test_bot #(parameter w = 4)
                     (input clk,
                      input bit [w-1:0] i1,
                      input logic [w-1:0] i2,
                      output logic [0:w-1] o1,
                      output bit [0:w-1] o2);

        always_ff@(posedge clk)
        begin
            o1 = i1 ^ ~i2;
            o2 = -i2 * -i1;
        end

    endmodule

endmodule

