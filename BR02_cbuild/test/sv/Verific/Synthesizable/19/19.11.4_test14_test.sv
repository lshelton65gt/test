
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design instantiates a module which performs some arithmetic
// and bitwise computations. The module is instantiated using implicit port connection
// using (.*).

module test(clk, i1, i2, o1);

parameter IN_WIDTH = 8, OUT_WIDTH = 16;

input clk;
input [IN_WIDTH-1:0] i1, i2;
output wire [OUT_WIDTH-1:0] o1;

reg [IN_WIDTH-1:0] ib1, ib2;

bottom #(.WIDTH_2(OUT_WIDTH), .WIDTH_1(IN_WIDTH))
        m2 (.*);

always @(*)
begin
        ib1 = i1 & ~i2;
        ib2 = ~i1 | i2;
end

endmodule

module bottom(clk, ib1, ib2, o1);

parameter WIDTH_1 = 4, WIDTH_2 = 8;

input clk;
input [WIDTH_1-1:0] ib1, ib2;
output wire [WIDTH_2-1:0] o1;

reg [WIDTH_1-1:0] r;

always @(negedge clk)
begin
        r = ~ib1 & ib2;
end

assign o1 = r + ib1;

endmodule

