
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design references an hierarchical name within a generate block.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

generate
    genvar i, e ;

    for(i=0; i<width; i++)
        begin: for_gen_i
            always@(posedge clk)
            begin: gen_beg
                int j;
                out1[i] = in1[i] ^ in2[width-1-i];
                j = i+1;
            end
        end

    for(e=0; e<width; e++)
        begin:b
            always@(posedge clk)
            begin : s
                int k ;
                k = for_gen_i[e].gen_beg.j ;  /* reference j using hierarchical names. */
                out2[e] = in1[width-1-e] & in2[e] + out1[k];
            end
        end
endgenerate

endmodule

