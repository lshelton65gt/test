
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a nested module and uses 'generate construct' inside
// nested module. Nested module is instantiated with different parameter settings and generate
// construct uses that parameter value in ?: operator. 

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output [w-1:0] out1, out2);

    module bottom #(parameter p = 8)
                   (input clk,
                    input [p-1:0] in,
                    output reg [p-1:0] out);

        generate    // generate
            logic [p-1:0] r1;

            assign r1 = ((p<8) ? ~in : -in) ;   // if-generate

            always@(posedge clk)
            begin
                out = ((p<8) ? -r1 : ~r1) ;   // if-generate
            end
        endgenerate

    endmodule

    bottom bot1(clk, in2, out1);
    bottom #(.p(8)) bot2(clk, in1, out2);

endmodule

