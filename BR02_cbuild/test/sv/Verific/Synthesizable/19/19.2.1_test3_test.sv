
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines packages and uses those in modules

package Q ;
  logic y = 1 ;
endpackage

package P ;
  import Q ::* ;
  logic x = 0 ;
  logic y2 = 1 ; 
endpackage 

module bot(input in, output reg out) ;
 import P::* ;

 always @(in)
    out = in + x ;
endmodule

module test (input in1, in2, output out1, out2) ;
  import Q::* ;
  bot I1(in1, out1) ;
  assign z = in2 + y ;
  bot I2(z, out2) ;
endmodule

