
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows declaration outside named modules or
// interfaces. This is treated as a global declaration. This can be a declaration
// for a variable, a task, function or a type. This design declares a global
// variable outside of any module or interface, in the $root top level. This
// should be treated as global variable.

int count; // This is the global variable. It is used in the bench file also
reg [128:0] msg; // This is another global variable and is used in bench file

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, 
              output reg [width-1:0] out1, out2);

logic [width-1:0] l1, l2;

always @(posedge clk)
begin
    if (count>=0)
        count++;
    else
        count = 0;

    l1 = (count)?(in1|in2):(in1^in2);
    l2 = (count)?(in1^in2):(in1&in2);

    out1 = count*l1-l2;
    out2 = count+out1|l2;

    msg = (count[0])?("Odd number"):("Even number");
end

endmodule

