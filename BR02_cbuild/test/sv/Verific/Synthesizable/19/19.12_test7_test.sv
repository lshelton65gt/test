
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If a port declaration is of generic interface type, then it
// can be connected to any interface type. This design connects a named interface
// to a generic interface port.

interface i1(input clk);
    int i;
    int o;

    modport mp(input i, input clk, output o);
endinterface

module test #(parameter width=8)
             (input clk,
              input logic [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    i1 ifc1(clk);
    i1 ifc2(clk);

    bot bot_1 (ifc1);
    bot bot_2 (ifc2.mp);

    always@(negedge clk)
    begin
        ifc1.i = in1 + in2;
        ifc2.i = in2 ^ in1;
    end

    always@(posedge clk)
    begin
        out1 = ifc2.o - ifc1.o;
        out2 = ifc1.o & ifc2.o;
    end

endmodule

module bot (interface ifc);

    always@(ifc.clk)
        ifc.o = 32'd999999998 - ifc.i;

endmodule


