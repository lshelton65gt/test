
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines packages and imports those inside
// another package as well as in a module.

package Q ;
  logic y = 0 ;
endpackage

package P ;
  import Q ::* ;   // Import package Q
  logic x = y + 1;
endpackage 

module test(input in, output reg out) ;

 import P::* ;     // Import package P

 always @(in) begin
    out = in + x ;
 end

endmodule
