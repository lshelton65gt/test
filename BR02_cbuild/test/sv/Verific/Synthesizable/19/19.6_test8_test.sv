
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines nested modules and functions within those
// modules. The scope where the functions are defined also contains the function calls.
// So the function corresponding to that scope is called rather than the other function.
// The name of the functions are same for this case.

module test (clk, in1, in2, out1, out2);

    input clk, in1, in2;
    output out1, out2;

    reg out2;

    function f;
	    input in1, in2;
	    reg t;

	    t = in1 | in1;
	    return t;
    endfunction

	module m1 (input clk, in1, in2, output reg out1);

		always @ (posedge clk)
			out1 = f (in1, in2);

		function f;
			input in1, in2;
			reg t;

			t = in1 & in1;
			return t;
		endfunction
	endmodule

	m1 i1 (clk, in1, in2, out1);

	always @ (posedge clk)
		out2 = f (in1, in2);

endmodule

