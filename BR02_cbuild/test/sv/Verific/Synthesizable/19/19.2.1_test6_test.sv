
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a package having a structure declaration
// and a function declaration. This package is used to define data types of module ports.


package ComplexPkg ;

    typedef struct packed {
        int i, r ;
    } Complex ;

    function Complex add(Complex a, b) ;
        add.r = a.r + b.r ;
        add.i = a.i + b.i ;
    endfunction

    function Complex mul (Complex a, b) ;
        mul.r = (a.r * b.r) - (a.i * b.i) ;
        mul.i = (a.r * b.i) + (a.i *b.r) ;
    endfunction

endpackage : ComplexPkg

module test (a, b, out);

    import ComplexPkg::Complex, ComplexPkg::mul ;

    input Complex a, b ;
    output Complex out;
    assign out = mul(a,b) ;

endmodule: test

