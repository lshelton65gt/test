
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a nested module. Inner module defines a 'if-generate'.
// Inside the 'if-generate' it uses a 'case-generate'.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output [w-1:0] out1, out2);

    module bottom #(parameter p = 8)
                   (input clk,
                    input [p-1:0] in,
                    output reg [p-1:0] out);
                    

        logic [p-1:0] r1;

        generate
            genvar i;
            if (p<8)    // if-generate
                always@(posedge clk)
                begin
                    r1 = ~in;
                    out = r1 + 1'b1;
                end
            else
                case(p)    // case-generate
                    8, 9, 10, 11:
                        for (i=0; i<p; i++)    // for-generate
                        begin: for_gen_i
                            always@(posedge clk)
                            begin
                                r1[i] = 1'b1 - in[i];
                                out[i] = ((0 == i) ? (r1[i] + 1'b1) : r1[i]);    // if-generate
                            end
                        end
                    default:
                        always@(posedge clk)
                        begin
                            r1 = -in;
                            out = r1;
                        end
                endcase
        endgenerate

    endmodule

    bottom bot1(clk, in2, out1);
    bottom #(.p(8)) bot2(clk, in1, out2);

endmodule

