
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules. One of them is instantiated
// from the other module. A task is declared in the $root scope. This task is invoked
// from the instantiated module.

parameter int n = 8;

task t1;
    input [0:n-1] in;
    output [0:n-1] out;

    out = in;
endtask

module child #(parameter integer p = 4)
              (input [0:p-1] in1,
               output reg [0:p-1] out,
               input clk);

    always @ (posedge clk)
    begin
        t1 (in1, out);
    end

endmodule

module test (in1, out1, out2, clk);
    input [0:7] in1;
    output [0:7] out1;
    output [0:3] out2;
    input clk ;

    child #(8) i1 (in1, out1, clk);
    child #(4) i2 (in1[0+:4], out2, clk);

endmodule

