
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): As long as the connecting variables are ordered correctly
// and are of the same size as the instance ports that they are connected to,
// there will be no warnings and the simulation will work as expected. This
// design connects ports of an instantiation in positional style but the data
// type are different.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    logic [width-1:0] l1;
    bit [0:width-1] b1;

    test_bot #(width) bot (clk, l1, b1);

    always@(negedge clk)
        l1 = in1 | in2;

    always@(posedge clk)
        out1 = (out2 = b1) ^ -b1;

endmodule

module test_bot #(parameter w = 4)
                 (input clk,
                  input bit [w-1:0] in1,
                  output logic [0:w-1] out1);

    always@(posedge clk)
    begin
        out1 = ~in1;
    end

endmodule

