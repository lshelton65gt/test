
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design tests whether forward declaration and its use
// is allowed as a module port type .

typedef bit [0:3] TYPE1 [0:3] ;
typedef bit [0:3] TYPE2 [0:1] ;

`define RANGE [0:7]

module test (input TYPE1 a, b, TYPE2 c, output `RANGE o) ;

    reg `RANGE temp ;

    always @ (*)
    begin
        temp = {a[1][0+:2], b[0][0], c[1][1], a[0][0:1]} ;
    end

    assign #5 o = temp ;

endmodule

