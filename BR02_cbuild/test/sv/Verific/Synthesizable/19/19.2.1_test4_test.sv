
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines some packages and imports them to other
// packages, root item and module.

package P1 ;

  typedef struct packed { bit [7:0] a; logic [3:0] b; } my_struct ;

  typedef struct { my_struct s; logic x; } st2 ;

endpackage 

package P2 ;
 import P1::my_struct ;
 my_struct x, y ;
endpackage

package P3 ;
 import P1::st2 ;
 st2 x1, y1 ;
endpackage

import P1::* ;

module test (input my_struct in1, in2, output st2 out1, out2) ;
  import P2::* ;
  import P3::* ;

  assign x.a = in1.a | in2.b ;
  assign x.b = ~in2.b ;

  assign y = x ; // FIXME : using 'y' of package P2, not implicit 'y'
  assign out1.s = y ;
  assign out1.x = 1 ;

  assign out2 = x1 ;

endmodule

