
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design connects ports of an instantiation using named port
// connection rule. The specialty is that it connects ports of type interface using
// named port connection.

interface ifc(input clk);
    reg [7:0] i1, i2;
    reg [7:0] o1, o2;

    modport mp(input i1, i2, clk, output o1, o2);
endinterface

module test (input clk,
             input [0:7] in1, in2,
             output reg [7:0] out1, out2);

    logic [7:0] l1, l2;
    bit [0:7] b1, b2;
    
    ifc ifc1(clk);

    test_bot bot_1 (.if1(ifc1.mp));


    always@(negedge clk)
    begin
        ifc1.i1 = in1 * in2;
        ifc1.i2 = in1 | in2;
    end

    always@(posedge clk)
    begin
        out1 = ifc1.o1 + ifc1.o2;
        out2 = ifc1.o1 & ifc1.o2;
    end

    module test_bot (ifc.mp if1);

        always@(posedge if1.clk)
        begin
            if1.o1 = if1.i1 | if1.i2;
            if1.o2 = if1.i2 ^ if1.i1;
        end

    endmodule

endmodule

