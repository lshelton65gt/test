
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a nested module and uses two 'generate'
// constructs inside it. Nested module is instantiated with different parameter value.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1,
              output [w-1:0] out1, out2);

    bottom #(.p(w)) bot2(clk, in1, in1, out1, out2);

    module bottom #(parameter p = 8)
                   (input clk,
                    input [p-1:0] in1, in2,
                    output reg [p-1:0] out1, out2);

        generate
            case(p)    // case generate
                1, 2, 3, 4:
                    always@(posedge clk)
                    begin
                        out1 = ~in1;
                    end
                5, 6, 7, 8:
                    always@(posedge clk)
                    begin
                        out1 = in1;
                    end
                default:
                    always@(posedge clk)
                    begin
                        out1 = -in1;
                    end
            endcase
        endgenerate

        generate
            always@(posedge clk)
            begin    // equivalent case-generate
                out2 = (((1<=p) && (4>=p)) ? (~in2) : ((5<=p) && (8>=p))? (in2): (-in2));
            end
        endgenerate

    endmodule


endmodule

