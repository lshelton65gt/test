
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses an user defined type (a structure) as the
// data type of the ports of a module. This module in turn instantiates another
// module with the elements of the user defined data type (structure).

typedef struct {
    bit [0:3] data1;
    bit [0:3] data2;
} dataType;

module test (input dataType in1, in2, input wire clk, output dataType out);

    bot i1 (in1.data1, in1.data2, clk, out.data1);
    bot i2 (in2.data1, in2.data2, clk, out.data2);

endmodule

module bot (input [0:3] in1, in2, input clk, output reg [0:3] out);

    always @ (posedge clk)
    begin
        out = {in1[0+:2], in2[2+:2]};
    end

endmodule

