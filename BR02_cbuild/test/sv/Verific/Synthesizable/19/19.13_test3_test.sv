
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines 'for-generate' inside 'case-generate' construct. Three
// 'for-generate' loops are there, one inside two case items and the other
// inside the 'default' case item. The statements within the 'for-generate' loop have the
// same block name.

module test #(parameter w = 8)
             (input clk,
              input [w-1:0] in1, in2,
              output reg [w-1:0] out1, out2);

    generate
    genvar i;

        case (w)
            1, 2, 3, 4:
                for(i=0; i<w; i++)
                begin: for_gen_i
                    always@(posedge clk)
                    begin
                        out1[i] = in2[i] - in1[i];
                        out2[i] = in1[i] - in2[i];
                    end
                end
            5, 6, 7, 8:
                for(i=w-1; i>=0; --i)
                begin: for_gen_i
                    always@(posedge clk)
                    begin
                        out1[i] = in1[i] - in2[i];
                        out2[i] = in2[i] - in1[i];
                    end
                end
            default:
                /* first loop from w/2-1 to 0, then loop from w/2 to w-1 */
                for(i=w/2-1; ((i<w/2)?(i>=0):(i<w)); i = ((i<w/2)?  
             ((0==i)?(i=w/2):(i-1)):
                (i+1)))
                begin: for_gen_i
                    always@(posedge clk)
                    begin
                        out1[i] = in1[i] ^ in2[i];
                        out2[i] = in1[i] & in2[i];
                    end
                end
        endcase

    endgenerate

endmodule

