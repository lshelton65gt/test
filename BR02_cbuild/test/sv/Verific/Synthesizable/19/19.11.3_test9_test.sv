
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design instantiates a module with dot name and dot
// star port connections. This is not allowed in SystemVerilog. So, this should
// be flagged as an error.

// Note: As per System Verilog LRM 3.0 section 12.7.3 this is a negative testcase
// however as per LRM IEEE-1800 section 19.11.4 this is a positive testcase.

typedef bit [0:7] MYTYPE1;
typedef enum {a,b,c,d,e,f,g,h} MYTYPE2;

MYTYPE1 in1, out1;
MYTYPE2 in2, out2;

module test (input MYTYPE1 in1, MYTYPE2 in2, output MYTYPE1 out1, MYTYPE2 out2);

bot i1 (.in1, .in2, .*);

endmodule

module bot(input MYTYPE1 in1, MYTYPE2 in2, output MYTYPE1 out1, MYTYPE2 out2);

    always @ (*)
    begin
        out1 = in1;
        out2 = in2;
    end

endmodule

