
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): If the port declaration has a named interface type then it must
// be connected to a generic interface or to a interface of same type. This design 
// connects a named interface to an interface port of same type.

module test #(parameter width=8)
             (input clk,
              input logic [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    i1 ifc1(clk);
    i1 ifc2(clk);

    bot bot_1 (ifc1.mp);
    bot bot_2 (ifc2.mp);

    always@(negedge clk)
    begin
        ifc1.i = in1 * in2;
        ifc2.i = in2 - in1;
    end

    always@(posedge clk)
    begin
        out1 = ifc2.o ^ ifc1.o;
        out2 = ifc1.o + ifc2.o;
    end

endmodule

module bot (i1.mp ifc);

    always@(ifc.clk)
        ifc.o = 32'd99999999 - ifc.i;

endmodule

interface i1(input clk);
    int i;
    int o;

    modport mp(input i, clk, output o);
endinterface

