
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): System Verilog allows complex and user defined data type
// to be defined for ports. This design type defines and connects to port of type
// packed array

module test (in1, out1, clk);
    typedef logic [0:1][0:1] T1;

    input [0:31] in1;
    output [0:31] out1;
    input clk;

    T1 a;
    logic [0:1][0:1] b;

    assign a = in1[0:3];
    assign b = in1[4:7];

    bot i1 (a, b, out1[0:3], clk);

    typedef logic [0:1][0:1] T2 [0:1];

    T2 c;
    assign c[0] = in1[8:11];
    bot i2 (c[0], 4'b1001, out1[4:7], clk);

    logic [0:7] x;
    logic [0:1] y;
    assign {x, y} = in1[22:31];
    bot i3 (x[0:3], {y, x[4:5]}, out1[8:11], clk);

endmodule

module bot(input [0:1][0:1] in1, in2, output reg [0:3] out, input clk);

    always  @ (posedge clk)
    begin
        out[0] = in1[0][0] & in2[0][0];
        out[1] = in1[0][1] | in2[0][1];
        out[2] = in1[1][0] ^ in2[1][0];
        out[3] = in1[1][1] ~^ in2[1][1];
    end

endmodule

