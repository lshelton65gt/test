
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows declaration outside a named module
// or interface. This is treated as global declaration. This can be a variable
// declaration or a task or function declaration or a type. This design declares
// a new type in global scope using typedef. This should be accessible from anywhere.

`include "header.vh"

module test (input clk,
             input my_byte_t in1, in2,
             output my_int_t out1, out2);

my_byte_t b1;
my_int_t i1;

always @(posedge clk)
begin
    my_byte_t b2;
    my_int_t i2;

    b1 = in1<<2 + in2;
    b2 = in2>>2 | in1;

    i1 = in1*in2;
    i2 = in1<<2|in2;

    out1 = b1 ^ i1;
    out2 = i2 & b2;
end

endmodule

