
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design connects tri0 to tri1 net using named port connection
// rule. This is allowed, only an warning message is given when we
// run simulation on it.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output [width-1:0] out1, out2);

    reg [0:width-1] in;
    tri1 [0:width-1] out;

    always@ (posedge clk)
    begin
        in = in1<<width/2 | in2>>width/2;
    end

    bot #(width) bot(.out(out), .in(in));

    assign out1 = -out, out2 = ~out;
endmodule


module bot #(parameter width=8)
            (input [0:width-1] in,
             output tri0 [width-1:0] out);

    assign out = in;

endmodule

