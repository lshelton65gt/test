
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): The top level module that are not instantiated by the
// programmer is implicitly instantiated by the environment. SystemVerilog 
// allow explicit instantiation of modules in $root scope. This design
// explicitly instantiates a module in $root scope and connects the ports
// with variables of type packed structure.

localparam c = 8;

struct packed { bit [0: c - 1] f1; bit f2;} out1, in1, in2;

top_sysv #(c+1) inst (out1[c:0], in1[c:0], in2[c:0]);

module test (a, b, c, d, o);
    input a, b, c, d;
    output o;

    wire temp1, temp2;

    bot1 i1 (temp1, a, b);
    bot2 i2 (temp2, c, d);

    top_sysv #(2) i3 (o, temp1, temp2);

endmodule

module bot1 (o, a, b);
    output o;
    input a, b;

    assign o = a & b;

endmodule

module bot2 (o, a, b);
    output o;
    input a, b;

    assign o = a | b;

endmodule

module top_sysv (o, a, b);
    parameter p = 2;
    output [p-1:0] o;
    input [p-1:0] a, b;

    genvar i;
    generate
        for (i = 0; i < p; i = i + 1)
        begin : blk
            bot2 i1 (o[i], a[i], b[i]);
        end
    endgenerate

endmodule

