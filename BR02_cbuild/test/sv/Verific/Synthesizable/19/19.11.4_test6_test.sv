
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules (nested) and instantiates
// the inner module from within the outer module. The ports of the instantiation
// is connected using .* port connection style. The remaining ports are connected
// using the named port connection rule.

module test(in1, in2, out1, clk);

    input in1, in2, clk;
    output out1;

    module bot (in1, in2, out1, clk);
        input in1, in2, clk;
        output reg out1;

        wire temp;
        assign temp = in1 & in2;

        always @ (posedge clk)
            out1 <= temp;
    endmodule

    wire temp;

    bot i1 (.out1(), .*);
    bot i2 (.*, .in2(temp));

endmodule

