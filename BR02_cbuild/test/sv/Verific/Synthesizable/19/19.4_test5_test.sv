
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows declaration outside named modules
// or interfaces. This is treated as a global declaration. This can be a
// declaration for a variable, task, function or type. This design declares
// a global variable outside of any module or interface, in the $root top
// level. This should be treated as global variable.

typedef struct {
    int r;
    int i;
} complex_t;

module test (input clk,
             input complex_t ci1, ci2,
             output complex_t co1);

complex_t c_temp;

always @(posedge clk)
begin

    co1.r = ci1.r + ci2.r;
    co1.i = ci1.i + ci2.i;
end

endmodule

