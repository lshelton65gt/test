
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules and instantiates one from the
// other. The ports are connected using .name port connection rule.
// For some of the ports the type differs but they can be implicitly
// converted to the target type like wire to reg.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output [width-1:0] out1, out2);

    reg [0:width-1] in;
    wire [width-1:0] out;

    always@(negedge clk)
        in = {in1[0:width/2-1], in2[width/2:width-1]};

    bot #(width) bot_1(.out, .in, .clk);

    assign out1 = out, out2 = ~out;

endmodule

module bot #(parameter width=2)
            (input clk,
             input [0:width-1] in,
             output reg [width-1:0] out);

    always@(posedge clk)
        out = in - 1;

endmodule

