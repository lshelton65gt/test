
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines a package having a structure declaration
// and a function declaration. This package is used to define data types of module ports
// without using the 'import' keyword.

package ComplexPkg ;

    typedef struct packed{
        int i, r ;
    } Complex ;

    function Complex add(Complex a, b) ;
        add.r = a.r + b.r ;
        add.i = a.i + b.i ;
    endfunction

    function Complex mul (Complex a, b) ;
            mul.r = (a.r * b.r) - (a.i * b.i) ;
        mul.i = (a.r * b.i) + (a.i * b.r) ;
    endfunction

endpackage : ComplexPkg


module test ( input ComplexPkg::Complex a, b, output ComplexPkg::Complex out) ;

always @(*)
  begin: b1
    ComplexPkg::Complex tmp ;
    tmp = ComplexPkg::mul(a, b) ;
    out = ComplexPkg::add(tmp, b) ;
  end

endmodule: test

