
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Hierarchical names consist of instance names separated
// by periods, where an instance name may be an array element. This design
// references hierarchical names from within a generate block. The referred
// name is same as the genvar name.

int i = 0;

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    genvar i;

    generate

        for(i=0; i<width; i++)
        begin: for_gen_i
            always@(posedge clk)
            begin
                $root.i = i ;
                out1[i] = in1[i] + in2[$root.i];
                out2[$root.i] = in1[$root.i] + in2[i];
                $root.i++;
            end
        end

    endgenerate

endmodule

