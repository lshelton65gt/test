
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design checks whether declaring packed dimension is allowed
// in typedef and packed/unpacked arrays that type can be used as the data type of ports
// for a module.

typedef logic MYTYPE;
typedef logic [0:1][0:1] MYTYPE1;

module test (input MYTYPE [0:1][0:1]a[0:3],
             input MYTYPE [0:3]b[0:3],
             output MYTYPE1 o[0:3],
             input clk);

    always @ (posedge clk)
    begin
        o[0] = a[0] + b [0];
        o[1] = a[1] - b [1];
        o[2] = {a[2][0] , b [2][0+:2]};
        o[3] = {a[3][1][0] , b [3][1:3]};
    end

endmodule

