
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE POSITIVE TESTCASE

// TESTING FEATURE(S): This design defines packages and imports them in concurrent
// area of one module and inside 'if-generate' construct of another.

package Q ;
  logic y = 0 ;
endpackage

package P ;
  logic x1 = 0 ;
  logic y2 = 1 ; 
endpackage 

module bot(input in, output reg out) ;
 import P::* ;

 always @(in)
    out = in + x1 + y2 ;
endmodule

module test (input in1, in2, output out1, out2, out3) ;
import P::* ;
  generate
      if (1)
      begin: b
          import Q::* ;
          bot I1(in1, out1) ;
          assign z = in2 + y ;
          bot I2(z, out2) ;
      end
  endgenerate
  assign out3 = x1 + in1 ;
endmodule
