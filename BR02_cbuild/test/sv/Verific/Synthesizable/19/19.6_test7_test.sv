
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines nested modules. The module is instantiated
// in the scope where it is defined. It is instantiated using dot star (.*) port connect style.

module test (clk, in1, in2, out1, out2);

    input clk, in1, in2;
    output out1, out2;

	module m1 (input clk, in1, output reg out1);
		always @ (posedge clk)
			out1 = in1;
	endmodule

    wire t1, t2;

	m1 i1 (.*, .in1(t2));
	m1 i2 (clk, t1, out2);

	assign t1 = in1 & in2;
	assign t2 = in2 | in1;

endmodule

