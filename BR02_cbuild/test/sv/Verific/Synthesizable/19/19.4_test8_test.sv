
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Variables can be declared in the global $root scope. This
// design defines some of them and in case where they are also defined in the
// current scope, are called using $root.id hierarchical reference.

parameter int c = 8;
int i = 0 ;

wire syn_reset;

module test (out, in1, in2, op, clk, syn_reset);

    parameter p = c ;
    output reg [0:p-1] out;
    wire [0:p-1] temp;
    input clk, syn_reset;
    input [0:1] op;
    input [0:p-1] in1, in2;

    assign $root.syn_reset = ^in1;

    always @ (posedge clk)
    begin
        if (($root.syn_reset == 1) || (syn_reset == 1))
            out = 0;
        else
            out = temp;
    end

    arith #(p) i1 (temp, in1, in2, op, clk);

endmodule

module arith (out, in1, in2, op, clk);

    parameter p = c;

    output reg [0:p-1] out;
    input clk;
    input [0:1] op;
    input [0:p-1] in1, in2;

    always @ (posedge clk)
    begin
        unique case (op)
        2'b00: out = in1 + in2;
        2'b01: out = in1 - in2;
        2'b10: out = in1 * in2;
        2'b11: out = in1 / in2;
        endcase
    end

endmodule

