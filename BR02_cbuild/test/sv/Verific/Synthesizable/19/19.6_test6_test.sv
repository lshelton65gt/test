
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Modules can be nested. This design defines 3 modules inside
// a module. These modules are instantiated from within that top module.

module test(d, clk, pr, clr, q, nq);
    input d, clk, pr, clr;
    output q, nq;

    wire q1, nq1, nq2;

	module ff1 (input d, clk, clr, nq2, output q1, nq1);
		nand i1 (nq1, d, clr, q1);
		nand i2 (q1, clk, nq2, nq1);
	endmodule

	ff1 i1 (d, clk, clr, nq2, q1, nq1);

	module ff2 (input nq1, clk, clr, pr, output nq2);
		wire q2;
		nand i1 (nq2, clk, clr, q2);
		nand i2 (q2, nq1, pr, nq2);
	endmodule

	ff2 i2 (nq1, clk, clr, pr, nq2);

	module ff3 (input nq2, clr, q1, pr, output q, nq);
		nand i1 (q, nq2, clr, nq);
		nand i2 (nq, q1, pr, q);
	endmodule

	ff3 i3 (nq2, clr, q1, pr, q, nq);

endmodule

