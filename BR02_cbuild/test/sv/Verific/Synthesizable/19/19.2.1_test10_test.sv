
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test package import from a module. This module has self-recursive
// structural recursion inside generate block which is proper and valid. This is to test
// whether the values between recursion is kept for the items imported from a package.

package FractionArith ;
    typedef struct {
        byte numerator ;
        byte denominator ;
    } Fraction ;

    function Fraction Add(Fraction a, b) ;
        Add.numerator = a.numerator*b.denominator + a.denominator*b.numerator ;
        Add.denominator = a.denominator * b.denominator ;
    endfunction

    Fraction A ;
    Fraction B ;
endpackage : FractionArith

module test (a, b, o) ;
    parameter p = 8 ;
    input [p-1:0] a, b ;
    output [p-1:0] o ;

    import FractionArith::* ;
    import FractionArith::Add ;
    import FractionArith::Fraction ;

    always@(a, b)
    begin
        A.numerator = a ;
        B.numerator = b ;
        A.denominator = 1 ;
        B.denominator = 1 ;
    end

    generate
    if (p>1)
        test #(p-1) t1 (A.numerator[p-1:1], B.numerator[p-1:1], o[p-1:1]) ;
    endgenerate

    Fraction C ;
    assign C = Add(A, B) ;
    assign o[0] = C.numerator[0] ;

endmodule

