
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Module declaration can have another module declaration inside
// it. This design defines a module. Another module is defined inside this top module.
// Yet another module is defined inside the inner module. Each inner module is
// instantiated by each of the outer modules.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2, in3,
              output reg [width-1:0] out1, out2, out3);

    wire [width-1:0] r1, r2;

    test1 test11(in1, in3, r2, out1);
    test2 test21(in2, r1);

    always @(posedge clk)
    begin
        out2 = r2 & r1;
        out3 = r1 ^ r2;
    end

    module test1 #(parameter width=4)
                  (input clk,
                  input [0:width-1] in1, in2,
                  output reg [width-1:0] out1, out2);

        test2 test22(in1, out2);

        always @(posedge clk)
        begin
            out1 = ~in1 | in2;
        end

        module test2 #(parameter width=2)
                      (input clk,
                      input [0:width-1] in1,
                      output reg [width-1:0] out1);

            always @(posedge clk)
            begin
                out1 = -in1 | ~in1;
            end

        endmodule

    endmodule

endmodule

