
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules and instantiates one from the
// other. The ports are connected using .name port connection rule.
// For some of the ports the type differs but they can be implicitly
// converted to the target type like int typedef to myint and int itself.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output [width-1:0] out1, out2);

    typedef int myint;

    int in;
    wire [width-1:0] out;

    always@(negedge clk)
        in = { in2, in1 };

    bot #(width) bot_1(.in, .clk, .out);

    assign out1 = ~out, out2 = -out;

    module bot #(parameter width=16)
                (input clk,
                 input myint in,
                 output reg [width-1:0] out);

        always@(posedge clk)
            out = in + 1;

    endmodule

endmodule

