
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines a module and a type, using typedef, having same
// name. In the global scope a data element of the type is instantiated and assigned a value.

module test #(parameter w = 8)
             (input clk,
              input signed [w-1:0] in1, in2,
              output signed [w-1:0] out1, out2);

    bottom #(.p(w)) bot1 (clk, in2, out1);
    bottom #(.p(w)) bot2 (clk, in1, out2);

endmodule

module bottom #(parameter p = 4)
               (input clk,
                input [p-1:0] in,
                output reg [p-1:0] out);

    always@(posedge clk)
    begin
        out = ~in;
    end

endmodule

typedef int bottom;

bottom bot1 = 10;

