
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design connects ports of an instantiation using .*
// port connection rule. The rule says that the data type of element and the port
// connected to must be compatible. So, we connect logic with bit and bit with
// logic. In this case they are compatible.

module test #(parameter width=8)
             (input clk,
              input [0:width-1] in1, in2,
              output reg [width-1:0] out1, out2);

    logic [width-1:0] i1, o2;
    bit [0:width-1] i2, o1;

    test_bot #(width) bot_1 (.*);

    always@(negedge clk)
    begin
        i1 = in1 - in2;
        i2 = in1 * in2;
    end

    always@(posedge clk)
    begin
        out1 = o2 ^ o1;
        out2 = o1 & o2;
    end

    module test_bot #(parameter w = 4)
                     (input clk,
                      input bit [w-1:0] i1,
                      input logic [w-1:0] i2,
                      output logic [0:w-1] o1,
                      output bit [0:w-1] o2);

        always_ff@(posedge clk)
        begin
            o1 = i1 + i2;
            o2 = i2 | i1;
        end

    endmodule

endmodule

