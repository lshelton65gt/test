
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design instantiates a module with dot name and
// positional port connection. This is not allowed in SystemVerilog. So, this
// should be flagged as an error.

// Note: According to SV 3.0 sec. 12.7.3  this is a negative testcase but is
// a positive testcase as per LRM P1800 sec 19.11.4

typedef bit [0:7] MYTYPE1;
typedef enum {a,b,c,d,e,f,g,h} MYTYPE2;

module test(input MYTYPE1 in1, MYTYPE2 in2, output MYTYPE1 out1, MYTYPE2 out2) ;

bot i1 (in1, in2, .out1(out1), .out2(out2));

endmodule

module bot(input MYTYPE1 in1, MYTYPE2 in2, output MYTYPE1 out1, MYTYPE2 out2);

    always @ (*)
    begin
        out1 = in1;
        out2 = in2;
    end

endmodule

