
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): By default the type of unspecified port type is wire.
// This can be changed using `default_nettype compiler directive. This design
// defines the default data type to be wand using that compiler directive.

`default_nettype wand

module test (input [0:3] a, b, output [0:3] out);

    reg [0:3] temp1, temp2;

    always @ (a or b)
    begin
        temp1 = a;
        temp2 = b;
    end

    assign out = temp1;
    assign out = temp2;

endmodule

