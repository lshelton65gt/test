
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): Test of time precision.

module test(output reg [7:0] out, input [7:0] in) ;

    timeunit 1ns ;
    timeprecision 0.1ns ;

    always @(in)
        out = #10.11 in; // Literal 10.11 will  be multiplied by time unit
                         // and rounded according to time precision 

endmodule

