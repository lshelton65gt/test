
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules in the $root scope. The type
// of ports of both of them are user defined type. One of them is instantiated within
// the other module. The ports are connected using dot name port connection rule.

typedef struct packed {
    bit [0:3] data1;
    bit [0:3] data2;
} dataType;

module test (input dataType in1, in2, input wire clk, output dataType out);

	bot i1 (.in1, .in2, .out, .clk);

endmodule

module bot (input dataType in1, in2, output dataType out, input wire clk);

	always @ (posedge clk)
	begin
		out.data1 = in1.data1 + in1.data1;
		out.data1 = in1.data1 - in1.data1;
	end

endmodule

