
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design defines two modules and instantiates one from
// the other. The ports of the instantiation is connected using .* port connection
// style. The ports are of user defined type.

typedef struct packed  {
    bit [0:3] data1;
    bit [0:3] data2;
} dataType;

module test (input dataType in1, in2, input wire clk, output dataType out);

    bot i1 (.*);

endmodule

module bot (input dataType in1, in2, output dataType out, input wire clk);

    always @ (posedge clk)
    begin
        out.data1 = in1.data1 + in2.data1;
        out.data2 = in2.data1 - in1.data1;
    end

endmodule

