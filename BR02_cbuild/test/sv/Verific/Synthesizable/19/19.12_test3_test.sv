
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): SystemVerilog allows packed arrays to be used in ports.
// This design defines and instantiates module with packed array elements.

typedef logic [0:1] PACKED_ARRAY;

module test (in1, out, clk);
    input [0:7] in1;
    input clk;
    output [0:7] out;
    
    PACKED_ARRAY [0:3] temp1, temp2;

    assign temp1 = in1;
    assign out = temp2;

    bot i1 [0:3] (clk, temp1, temp2);

endmodule

module bot (input clk, input PACKED_ARRAY in1, output PACKED_ARRAY out1);

    always @ (posedge clk)
        out1 = in1;

endmodule

