
# These tests make sure that the -libMap option works under all circumstances.

# Check that the libmap file is created for VHDL
rm -rf libdesign.* test1.libmap
cbuild -q -libMap test1.libmap -vhdlLib A test1.vhdl -vhdlTop test1 >& test1.cbld.log

# Check that the libmap file is created for VLOG
rm -rf libdesign.* test2.libmap
cbuild -q -libMap test2.libmap -vlogLib A test2.v -vlogTop test2 >& test2.cbld.log

# Check that the libmap file is created with multiple entries and that the last library is the work library
rm -rf libdesign.* test3.libmap
cbuild -q -libMap test3.libmap -vhdlLib A test3a.vhdl -vhdlLib B test3b.vhdl -vlogLib C test3c.v -vlogTop top >& test3.cbld.log

# Read a previously created libmap file
cbuild -q -libMap test3.libmap -vlogTop top >& test4.cbld.log

# Compile the files separately
rm -rf libdesign.* test5.libmap
cbuild -q -libMap test5.libmap -compileLibOnly -vhdlLib A test3a.vhdl >& test5a.cbld.log

cbuild -q -libMap test5.libmap -compileLibOnly -vhdlLib B test3b.vhdl >& test5b.cbld.log

cbuild -q -libMap test5.libmap -compileLibOnly -vlogLib C test3c.v >& test5c.cbld.log

## check to see that you can have a vhdl top level wrapper of a verilog design with the vhdl model as the only item included in source on the command line
## This test used to pass (prior to revision 1381), but was elaborating the wrong top. It was elaborating the module in test3c.v ('top'), 
## rather than 'topvhdl' in test5e.vhdl. Now, it is taking an assertion error here:
## VerificVhdlExpressionWalker.cpp:652 INFO_ASSERT(formal) failed
VTSKIP cbuild -q -libMap test5.libmap -vhdlTop topvhdl test5e.vhdl -verboseHierarchy >& test5e.cbld.log
VTSKIP cat libdesign.hierarchy >& test5e.libdesign.hierarchy

## also make sure you can compile with the verilog at the top
cbuild -q -libMap test5.libmap -vlogTop top >& test5d.cbld.log

# This is a test for bug 12441. where cbuild would crash with the option "-libmap"  (there is a -libMap option, but we are testing here the use of an invalid option)
# This test should fail with an error message about no such option -libmap
PEXIT cbuild -q -libmap test5a.libmap -compileLibOnly -vhdlLib A test3a.vhdl >& test6.cbld.log

