library ieee;
use ieee.std_logic_1164.all;



entity topvhdl is
  
  port ( min  : in  std_logic;
         mout : out std_logic);

end topvhdl;

architecture topvhdlarch of topvhdl is

  component top
    port ( tin  : in  std_logic;
           tout : out std_logic);
  end component;
  
begin  -- topvhdlarch

  b1 : top port map (
    tin  => min,
    tout => mout);

end topvhdlarch;
