library ieee;
use ieee.std_logic_1164.all;

entity bottom is
  
  port ( bin  : in  std_logic;
         bout : out std_logic);

end bottom;

architecture bottomarch of bottom is

begin  -- bottomarch

  bout <= bin;

end bottomarch;
