library ieee;
use ieee.std_logic_1164.all;

entity test1 is
  
  port ( in1  : in  std_logic;
         out1 : out std_logic);

end test1;

architecture toparch of test1 is

begin  -- toparch

  out1 <= in1;

end toparch;
