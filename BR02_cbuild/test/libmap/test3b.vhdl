library ieee;
use ieee.std_logic_1164.all;

library A;

entity middle is
  
  port ( min  : in  std_logic;
         mout : out std_logic);

end middle;

architecture middlearch of middle is

  component bottom
    port ( bin  : in  std_logic;
           bout : out std_logic);
  end component;
  
begin  -- middlearch

  b1 : bottom port map (
    bin  => min,
    bout => mout);

end middlearch;
