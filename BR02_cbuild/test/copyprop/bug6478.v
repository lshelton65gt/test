module top(clk,in,addr,out1,out2);
   input clk;
   input [15:0] in;
   input [3:0] 	addr;
   output out1,out2;
   reg 		out1,out2;
   reg [3:0] 	a1,a2,a3;
   initial out1 = 0;
   initial out2 = 0;
   always @(posedge clk) begin
      a1 = addr-1;
      a2 = addr-2;
      a3 = addr-3;
      out1 = in[a1] | in[a2] | in[a3];
      out2 = |in[addr+13 +: 3];
`ifdef CARBON
`else
      // The outputs can simulate to x if the addressing is
      // out-of-range. Convert Aldec x to 0 so we can compare with
      // Carbon.
      if (out1===1'bx) out1 = 1'b0;
      if (out2===1'bx) out2 = 1'b0;
`endif
   end
endmodule
