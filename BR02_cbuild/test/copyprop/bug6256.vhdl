-- last mod: Fri Oct  6 05:40:29 2006
-- filename: test/copyprop/bug6256.vhdl
-- Description:  This test shows the problem seen in bug6256 where there was a
-- problem with folding a sign extended value.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity WEMA_CALC is
  port (
  WBEN : in  std_logic_vector(1 downto 0);
  WEMA : out std_logic_vector(63 downto 0)
    );
end WEMA_CALC;

architecture WEMA_CALC of WEMA_CALC is
  signal temp : std_logic_vector(63 downto 0);
  signal tempbit0 : std_logic_vector(0 downto 0);
  signal tempbit1 : std_logic_vector(0 downto 0);
begin
  tempbit0(0) <= not(WBEN(0));
  temp(0*32+(31) downto 0*32) <= SXT(tempbit0, 32);
  tempbit1(0) <= not(WBEN(1));
  temp(1*32+(31) downto 1*32) <= SXT(tempbit1, 32);
  WEMA <= temp;
end WEMA_CALC;
    

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6256 is
  port (
  CLKA : in std_logic;
  CLKB : in std_logic;
  WEA : in std_logic;
  MEA : in std_logic;
  MEB : in std_logic;
  WBE : in std_logic_vector(1 downto 0);
  ADRA : in std_logic_vector(5 downto 0);
  DA : in std_logic_vector(63 downto 0);
  QOUT : out std_logic_vector(63 downto 0)
    );
end bug6256;

architecture bug6256 of bug6256 is
  signal WBEN   :  std_logic_vector(1 downto 0);
  signal WEMA   : std_logic_vector(63 downto 0);
  TYPE     MEM IS ARRAY (63 downto 0) of std_logic_vector(63 downto 0);
  signal memcore     : MEM := (others => (others => '0'));
  signal QB : std_logic_vector(63 downto 0) := (others => '0');
  signal ADRB : std_logic_vector(5 downto 0);

  component WEMA_CALC is
  port (
    WBEN : in  std_logic_vector(1 downto 0);
    WEMA : out std_logic_vector(63 downto 0)
    );
  end component WEMA_CALC;
begin

  WBEN(0) <= not WBE(0);
  WBEN(1) <= not WBE(1);

      WEMA_INST : WEMA_CALC port map(WBEN,WEMA);
  
process(CLKA)
  variable iadr : integer;
     begin
       -- write process
       if(CLKA'event and CLKA='1')then
         if(WEA='1' and MEA = '1')then
           iadr := conv_integer(unsigned(ADRA));
           for i in WEMA'range loop
             if (WEMA(i)='1') then
               memcore(iadr)(i) <= DA(i);
             end if;
           end loop;
         end if;
         ADRB <= ADRA;                  -- save away last address for reads.
       end if;
     end process;

  process(CLKB)
    VARIABLE iadr        : integer;
  begin
    -- read process
    if(CLKB'event and CLKB='1')then
      iadr := conv_integer(unsigned(ADRB));
      QB <= memcore(iadr);
    end if;
  end process;

  QOUT <= QB;
end bug6256;
