module divider(clk, half_clk);
   input clk;
   output half_clk;
   
   wire   clk;
   wire   tmp;
   wire   half_clk;

   // without proper flattening or copy propagation, we would see an
   // artificial cycle here.
   
   latch L1 (~clk, ~half_clk, tmp);
   latch L2 (clk, tmp, half_clk);
   
endmodule

module latch(en, d, q);
   input en, d;
   output q;
   
   wire   en, d;
   reg 	  q;
   
   initial q = 0;
   always @(en or d)
     if (en)
       q = d;
   
endmodule
