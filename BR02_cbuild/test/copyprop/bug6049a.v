module UV(ru0fix_pre_dExp_,__ru0fix_pre_man2_tmp1__in,ru0fix_pre_man2_);
   input [7:0] ru0fix_pre_dExp_;
   input [64:0] __ru0fix_pre_man2_tmp1__in;
   output [31:0] ru0fix_pre_man2_;

   wire [6:0] ru0fix_pre_dExp_n_;
   wire [32:0] ru0fix_pre_man2_tmp1_;

   assign      ru0fix_pre_dExp_n_ = -ru0fix_pre_dExp_[6:0];
   assign      ru0fix_pre_man2_tmp1_ = (__ru0fix_pre_man2_tmp1__in >> ru0fix_pre_dExp_n_[4:0]);
   assign      ru0fix_pre_man2_ = ru0fix_pre_man2_tmp1_[32:1];
endmodule
