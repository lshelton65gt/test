module top (clk, a, x);
   input clk;
   input [7:0] a;
   output [7:0] x;
   reg [7:0] 	x;
   reg [7:0] 	tmp;
   // assignment aliasing does not alias tmp and x because of the
   // initial driver on x. copy propagation should propagate through tmp and
   // eliminate its necessity.
   initial x = 0;
   always @(posedge clk)
     begin
	tmp = a;
	x = tmp;
     end
endmodule
