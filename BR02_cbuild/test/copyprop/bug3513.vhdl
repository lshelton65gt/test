library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug3513 is
  port (
  clk : in std_logic;
  wen : in std_logic;
  addr : in std_logic_vector(3 downto 0);
  data : in std_logic_vector(3 downto 0);
  output : out std_logic_vector(3 downto 0)
    );
end bug3513;

architecture bug3513 of bug3513 is
  type memory is array(15 downto 0) of std_logic_vector(3 downto 0);
  signal mem : memory := (others => "0000");
  signal q : std_logic_vector(3 downto 0) := "0000";
begin
  process(clk)
    variable iadr : integer;
  begin
    if (clk'event and clk='1') then
      -- population creates a variable with the output of the conv_integer call
      -- and assigns that to iadr. we perform assignment aliasing between these
      -- two variables but do not alias iadr and addr (they are of different
      -- signedness). copy propagation needs to understand that this is a safe
      -- propagation situation and replace iadr with addr.
      iadr := conv_integer(unsigned(addr));
      if (wen='1') then
        mem(iadr) <= data;
      else
        q <= mem(iadr);
      end if;
    end if;
  end process;

  process(q)
  begin
    output <= q;
  end process;
end bug3513;  
