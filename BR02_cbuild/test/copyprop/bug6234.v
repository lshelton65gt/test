module top(R_wr_byte_en, R_wr_en, wr_byte_count);
   input R_wr_en;
   input [63:0] R_wr_byte_en;
   output   integer wr_byte_count;

   integer  iteratorA;
   
   always @(R_wr_en or R_wr_byte_en)
     begin
        wr_byte_count = 0;
        if ( R_wr_en )
          begin
             for ( iteratorA = 0 ; iteratorA < 64 ;iteratorA = iteratorA + 1 )
               begin
                  wr_byte_count = wr_byte_count + R_wr_byte_en [ iteratorA ] ;
               end
          end
     end
endmodule
