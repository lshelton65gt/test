// Test case for copy propagation with large vector
// From sunfire CW000401_1_0_txl_32_crcq
module top(din,
	   pclkp, resetp, crc_data_in0, crc_data_in1, crc_data_in2, 
	   crc_data_in3, crc_data_in4, crc_data_in5, crc_data_in6, 
	   crc_data_in7, crc_data_in8, crc_data_in9, crc_data_ina, 
	   crc_data_inb, crc_data_inc, crc_data_ind, crc_data_ine, 
	   crc_data_inf, crc_data_inf_0, crc_data_inf_1
	   );

   output [143:0] din;
   reg [143:0] din;

   input pclkp;
   input resetp;

   input [7:0] crc_data_in0; // lane0  transmit data 
   input [7:0] crc_data_in1; // lane1  transmit data
   input [7:0] crc_data_in2;// lane2  transmit data
   input [7:0] crc_data_in3;// lane3  transmit data
   input [7:0] crc_data_in4;// lane4  transmit data
   input [7:0] crc_data_in5;// lane5  transmit data
   input [7:0] crc_data_in6;// lane6  transmit data
   input [7:0] crc_data_in7;// lane7  transmit data
   input [7:0] crc_data_in8;// lane8  transmit data   
   input [7:0] crc_data_in9;// lane9  transmit data
   input [7:0] crc_data_ina;// lane10  transmit data
   input [7:0] crc_data_inb;// lane11  transmit data
   input [7:0] crc_data_inc;// lane12  transmit data
   input [7:0] crc_data_ind;// lane13  transmit data
   input [7:0] crc_data_ine;// lane14  transmit data
   input [7:0] crc_data_inf;// lane15  transmit data
   input [7:0] crc_data_inf_0;// lane16  transmit data
   input [7:0] crc_data_inf_1;// lane17  transmit data

   wire [143:0] din_wire;

   assign din_wire = {crc_data_inf_1, crc_data_inf_0, 
		      crc_data_inf,crc_data_ine,crc_data_ind,
		      crc_data_inc,crc_data_inb,crc_data_ina,crc_data_in9,crc_data_in8,
		      crc_data_in7,crc_data_in6,crc_data_in5,crc_data_in4,crc_data_in3,
		      crc_data_in2,crc_data_in1,crc_data_in0};

   // Copy propagation should realize that din_wire and din compute
   // the same value once din_wire is moved into the if statement.
   always @ (posedge pclkp)
     if (resetp)
       begin
	  din <= 144'h000000000000000000000000000000000000;
       end	  
     else
       begin
	  din <= din_wire;
       end	  
endmodule // top
