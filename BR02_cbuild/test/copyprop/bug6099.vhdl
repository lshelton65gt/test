LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.std_logic_arith.ALL;

PACKAGE tcpkg is
    FUNCTION  AND_RED	(ARG: STD_LOGIC_VECTOR)	RETURN STD_LOGIC;
    FUNCTION  OR_RED	(ARG: STD_LOGIC_VECTOR)	RETURN STD_LOGIC;

END tcpkg;

PACKAGE BODY tcpkg IS

    FUNCTION  AND_RED	(ARG: STD_LOGIC_VECTOR)	return STD_LOGIC is
   variable TEMP: STD_LOGIC;
    BEGIN
	TEMP := '1';
	for i in ARG'range loop
	    TEMP := TEMP and ARG(i);
	END loop;
	return TEMP;
    END;

    FUNCTION OR_RED	(ARG: STD_LOGIC_VECTOR)	return STD_LOGIC is
    variable TEMP: STD_LOGIC;
    BEGIN
	TEMP := '0';
	for i in ARG'range loop
	    TEMP := TEMP or ARG(i);
	END loop;
	return TEMP;
    END;


END tcpkg;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE work.tcpkg.ALL;


ENTITY bug6099 is
	PORT (
  tc_and_out :  OUT  STD_LOGIC; 
  tc_or_out :  OUT  STD_LOGIC; 
 tc_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
	);

END ENTITY;

ARCHITECTURE rtl OF bug6099 IS

BEGIN
  tc_and_out <= AND_RED (tc_in);

  tc_or_out <= OR_RED (tc_in);

END ARCHITECTURE rtl;
