library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity bug6046 is
  port( nwtptr  : in  std_logic_vector(1 downto 0);
        nselinN : out std_logic_vector(1 downto 0) );
end bug6046;

architecture bug6046 of bug6046 is
   signal niwtptr : std_logic_vector(0 downto 0);
begin
   niwtptr <= nwtptr(0 downto 0);
   process (niwtptr) begin
      nselinN <= (others => '1');
      nselinN(conv_integer(unsigned(niwtptr))) <= '0';
   end process;
end bug6046;
