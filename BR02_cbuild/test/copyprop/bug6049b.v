module UV(cw0flt,sz,dex);
  input        [31:0]         cw0flt;
  input                       sz;
  output       [15:0]         dex;
  wire         [14:0]         dex_19_;
  assign dex = (dex_19_ + 16'h0001);
  assign dex_19_ = (~cw0flt[14:0]);
endmodule
