-- last mod: Fri Oct  6 14:10:32 2006
-- filename: test/copyprop/bug6256_a.vhdl
-- Description:  This test is based on test bug6256.vhdl in this
-- case I found that the inclusion of two assignments to the same signal
-- (indicated below) caused a simulation mismatch.  seen in 1.5036
-- cbuild should report a warning or alert on this. bug2805


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6256_a is
  port (
    CLKA : in std_logic;
    WEA : in std_logic;
    ADRA : in std_logic_vector(5 downto 0);
    DA : in std_logic_vector(63 downto 0);
    QOUT : out std_logic_vector(63 downto 0)
    );
end bug6256_a;

architecture bug6256_a of bug6256_a is
  signal WB   :  std_logic;
  TYPE     MEM IS ARRAY (63 downto 0) of std_logic_vector(63 downto 0);
  signal memcore     : MEM := (others => (others => '0'));
  signal QB : std_logic_vector(63 downto 0) := (others => '0');
  signal ADRB : std_logic_vector(5 downto 0);

begin

  WB <= WEA;
  WB <= not (WEA);  -- inclusion of this line causes a simulation mismatch
                    -- because it is converted into a second continuous assign
                    -- to WB

  
  process(CLKA)
    variable iadr : integer;
  begin
    -- write process
    if(CLKA'event and CLKA='1')then
      if(WB='1')then
        iadr := conv_integer(unsigned(ADRA));
        memcore(iadr) <= DA;
        ADRB <= ADRA;                  -- save address for next read.
      else
        iadr := conv_integer(unsigned(ADRB));
        QB <= memcore(iadr);
      end if;
    end if;
  end process;


  QOUT <= QB;
end bug6256_a;

