module top(multi_phy, phy_enable);
   input [31:0] phy_enable;
   output [31:0] multi_phy;
   reg [31:0] 	 multi_phy;
   integer 	 indx;
   always @(phy_enable) begin
      multi_phy = 0;
      for (indx = 0; indx < 32; indx = (indx + 1))
	multi_phy = (multi_phy + phy_enable[indx]);
   end
endmodule
