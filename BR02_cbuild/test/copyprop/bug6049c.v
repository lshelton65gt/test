module FE( dzdx22, ptmp01, xmaxdiff22_0 );
   input [21:0] dzdx22;
   input [3:0]  ptmp01;
   output [21:0] xmaxdiff22_0;
   wire [3:0]  bptmp01;
   assign      bptmp01 = (~ptmp01);
   assign      xmaxdiff22_0 = (dzdx22 * bptmp01[2:0]);
endmodule
