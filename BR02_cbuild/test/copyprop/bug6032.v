module fubar(clk,en,ymin,right,d,q);
   input clk;
   input en;
   input [3:0] ymin;
   output [7:0] right;
   input 	d;
   output 	q;
   reg [7:0] 	right;
   reg 		q;
   initial right = 8'b00000000;
   always @(posedge clk) begin
      if (en) begin
	 casex(ymin[2])
	   1'b0: right[7:0] = 8'b00000000;
	   1'b1: right[7:0] = 8'b00001111;
	 endcase
      end else begin
	 right[7:0] = 8'b00000000;
      end
   end
   initial q = 1'b0;
   always @(posedge clk) begin
      q = d;
   end
endmodule
