module top(adrs,a,b,c,d,anyone_on);
   input [31:0] adrs; // intentionally larger than the 2 used bits.
   input [3:0] 	a,b,c,d; 	
   output 	anyone_on;
   
   function sel4to1;
      input [1:0] adr;
      input a,b,c,d;
      case(adr)
	0: sel4to1 = a;
	1: sel4to1 = b;
	2: sel4to1 = c;
	3: sel4to1 = d;
      endcase
   endfunction

   wire [3:0] selected;

   // lots of assignment aliasing and copy propagation opportunities, here.
   assign     selected[0] = sel4to1( adrs[1:0], a[0], b[0], c[0], d[0] );
   assign     selected[1] = sel4to1( adrs[1:0], a[1], b[1], c[1], d[1] );
   assign     selected[2] = sel4to1( adrs[1:0], a[2], b[2], c[2], d[2] );
   assign     selected[3] = sel4to1( adrs[1:0], a[3], b[3], c[3], d[3] );

   assign     anyone_on = (|selected);
   
endmodule
