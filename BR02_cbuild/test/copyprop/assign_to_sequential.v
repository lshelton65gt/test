module top(clk,d_,q);
   input clk;
   input d_;
   output q;
   reg 	  q;
   
   wire  d = ~d_;
   
   initial q = 1'b0;
   always @(posedge clk) begin
      q <= d;
   end
endmodule
