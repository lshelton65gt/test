module top(sel,x,y,z,a,b,c,d,o1,o2,o3);
   input [3:0] sel;
   input [15:0] x,y,z;
   input [3:0] 	a,b,c,d;
   output [3:0] o1,o2;
   output [15:0] o3;

   function [3:0] sel4to1_4bits;
      input [1:0] selector;
      input [3:0] a,b,c,d;
      case(selector)
	0: sel4to1_4bits = a;
	1: sel4to1_4bits = b;
	2: sel4to1_4bits = c;
	3: sel4to1_4bits = d;
      endcase
   endfunction

   function [7:0] sel4to1_8bits;
      input [1:0] selector;
      input [7:0] a,b,c,d;
      case(selector)
	0: sel4to1_8bits = a;
	1: sel4to1_8bits = b;
	2: sel4to1_8bits = c;
	3: sel4to1_8bits = d;
      endcase
   endfunction

   // lots of assignment aliasing and copy propagation opportunities, here.
   assign 	  o1 = sel4to1_4bits(sel[1:0],a,b,c,d);
   assign 	  o2 = sel4to1_4bits(sel[1:0],d,b,c,a);

   // we currently do not propagate concats like {a,b}.
   assign 	  o3[15:8] = sel4to1_8bits(sel[3:2],x[15:8],y[15:8],z[15:8],{a,b});
   assign 	  o3[7:0]  = sel4to1_8bits(sel[3:2],x[7:0],y[7:0],z[7:0],{c,d});

endmodule
