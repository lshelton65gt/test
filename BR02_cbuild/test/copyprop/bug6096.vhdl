LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.std_logic_arith.ALL;

PACKAGE tcpkg is

    FUNCTION "and"	(L: STD_LOGIC_VECTOR;	R: STD_LOGIC)	RETURN STD_LOGIC_VECTOR;

END tcpkg;

PACKAGE BODY tcpkg IS

 
    FUNCTION "and"  (L: std_logic_vector; R: std_logic) RETURN std_logic_vector IS
        VARIABLE TEMP : std_logic_vector (L'range);
    BEGIN
        TEMP := (others => R);
        RETURN (L and TEMP);
    END;


END tcpkg;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;

USE work.tcpkg.ALL;


ENTITY bug6096 is
	PORT (
  tc_out :  OUT  STD_LOGIC_VECTOR (7 DOWNTO 0); 
  din1 :  IN  STD_LOGIC_VECTOR (7 DOWNTO 0); 
  din2 :  IN  STD_LOGIC_VECTOR (7 DOWNTO 0); 
  waddr :  IN  STD_LOGIC_VECTOR (2 DOWNTO 0); 
  raddr :  IN  STD_LOGIC_VECTOR (2 DOWNTO 0); 
  clk :  IN  STD_LOGIC; 
  rst :  IN  STD_LOGIC; 
  en1 :  IN  STD_LOGIC; 
  en2 :  IN  STD_LOGIC 
	);

END ENTITY;

ARCHITECTURE rtl OF bug6096 IS

  TYPE DATA_REG_TYPE   IS ARRAY (0 TO 7) OF STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL data_reg1 : DATA_REG_TYPE := (others => (others => '0'));
  SIGNAL data_reg2 : DATA_REG_TYPE := (others => (others => '0'));
  SIGNAL dout1 : STD_LOGIC_VECTOR (7 DOWNTO 0) := (others => '0');
  SIGNAL dout2 : STD_LOGIC_VECTOR (7 DOWNTO 0) := (others => '0');

BEGIN


  mem_proc: process (clk)
   begin
     if (rst='0') then
       data_reg1 <= (others => (others => '0'));
       data_reg2 <= (others => (others => '0'));
     elsif (rising_edge(clk)) then
       data_reg1(CONV_INTEGER (UNSIGNED(waddr))) <= din1;
       data_reg2(CONV_INTEGER (UNSIGNED(waddr))) <= din2;
    end if;
   end process;

   dout1 <= data_reg1(CONV_INTEGER (UNSIGNED(raddr)));
   dout2 <= data_reg2(CONV_INTEGER (UNSIGNED(raddr)));

   tc_out <= (dout1 AND en1) OR (dout2 AND en2);

END ARCHITECTURE rtl;
