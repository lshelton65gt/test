module bug(in0, in1, in2, in3, in4, in5, in6, in7, in8, sel, out);
   input [116:0] in0, in1, in2, in3, in4, in5, in6, in7, in8;
   input [8:0] 	 sel;
   output [116:0] out;

   wire [117*9-1:0]	  in = {in8, in7, in6, in5, in4, in3, in2, in1};

   mux #(117, 9) mux(.in(in), .sel(sel), .out(out));

endmodule


module mux(in, sel, out);
   parameter w = 32;
   parameter n = 2;

   input [n*w-1:0] in;
   input [n-1:0]   sel;
   output [w-1:0]  out;

   reg [w-1:0]  out;
   reg [n*w-1:0] temp;
   reg [w-1:0]  o_int;
   
   integer 	i, j;

   always @(in or sel) begin
      temp = {(n*w){1'b0}};
      o_int = {w{1'b0}};

      for (i = n-1; i >= 0; i = i - 1) begin
	 for (j = w-1; j >= 0; j = j - 1) begin
	    temp[i*w+j] = in[i*w+j] & sel[i];
	    o_int[j] = o_int[j] | temp[i*w+j];
	 end
      end

      out = o_int;
   end
endmodule
