-- This is the same test as depobs2.vhd but the port I/O is done with
-- 31 bits instead of 8 bits.
library ieee;
use ieee.std_logic_1164.all;

entity myreg is
  
  port (
    clk : in  std_logic;
    io1 : inout std_logic_vector (31 downto 0);
    io2 : inout std_logic_vector (31 downto 0);
    dir1 : in std_logic;
    dir2 : in std_logic);

end myreg;

architecture rtl of myreg is

  signal out1 : std_logic_vector (31 downto 0) := (others => '0');
  signal out2 : std_logic_vector (31 downto 0) := (others => '0');
  signal en1 : std_logic := '0';
  signal en2 : std_logic := '0';

begin  -- arch

  process (clk)
  begin
    if clk'event and clk = '1' then
      out1 <= io2;
      out2 <= io1;
      en1 <= dir2;
      en2 <= dir1;
    end if;
  end process;

  io1 <= out1 when en1 = '1' else (others => 'Z');
  io2 <= out2 when en2 = '1' else (others => 'Z');
  
end rtl;
