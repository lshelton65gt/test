/*
 Test for bug 4937. Make sure that vhdlCase is not ignored.
 This also requires a port split to happen on an enabled signal in order
 to simulate what happened at the customer site.
 */

module top (out, in1, zout, out2, );
   output [1:0] out;
   output       out2;
   wire enOut0, enOut1, enOut2;

   inout [1:3] zout;
   
   input       in1;

   wire [2:0] en; // carbon depositSignal

   wire [1:0] bus;

   mysub d(zout, in1, out);
   
   
// named association
   /*
   sub s1 (.EN(en[2]), .OUTP(zout[1]), .INP(in1));
   sub s2 (.EN(en[0]), .outp(zout[0]), .INP(in1));
   sub s3 (.EN(en[1]), .outp(zout[2]), .INP(1'b1));
    */
   
// positional association
   /*
   sub2 S2 (bus[0], zout[1], en[0]);
   sub2 S3 (bus[1], zout[0], en[1]);
   sub2 S4 (out[0], zout[2], en[2]);
*/
   /*
   sub2 S2 (bus[0], zout[1], en[0]);
   sub2 S3 (bus[1], zout[0], en[0]);
   sub2 S4 (out[0], zout[2], en[0]);

   sub s5 (.en(in1), .OUTP(en[2]), .inp(en[1]));
   sub s6 (.en(in1), .OUTP(en[1]), .inp(en[0]));
   sub s7 (.en(in1), .OUTP(en[0]), .inp(1'b0));
    */
/*
   assign     enOut0 = en[0];

   assign     enOut1 = en[1];

   assign     enOut2 = en[2];

   assign     out2 = enOut0 ^ enOut1 + enOut2;
*/ 
/*  
   assign     en[2] = in1 ? in1 ^ en[1] : 1'bz;
   assign     en[1] = in1 ? in1 | en[0] : 1'bz;
   assign     en[0] = in1 ? in1 ^ en[1] : 1'bz;   
  */ 
endmodule // top

module mysub(zout, in1, out);
   inout [2:0] zout;
   output [1:0] out;
   input  in1;

      wire [1:0] bus;
   wire [2:0] zen; // carbon depositSignal
   
// named association
   sub s1 (.en(zen[2]), .OUTP(zout[1]), .INP(in1));
   sub s2 (.en(zen[0]), .outp(zout[0]), .INP(in1));
   sub s3 (.EN(zen[1]), .outp(zout[2]), .INP(1'b1));
   
// positional association
   /*
   sub2 S2 (bus[0], zout[1], en[0]);
   sub2 S3 (bus[1], zout[0], en[1]);
   sub2 S4 (out[0], zout[2], en[2]);
*/
   sub2 S2 (bus[0], zout[1], zen[0]);
   sub2 S3 (bus[1], zout[0], zen[0]);
   sub2 S4 (out[0], zout[2], zen[0]);

   sub s5 (.en(in1), .OUTP(zen[2]), .inp(zen[1]));
   sub s6 (.en(in1), .OUTP(zen[1]), .inp(zen[0]));
   sub s7 (.en(in1), .OUTP(zen[0]), .inp(1'b0));
/*
   assign     enOut0 = en[0];

   assign     enOut1 = en[1];

   assign     enOut2 = en[2];

   assign     out2 = enOut0 ^ enOut1 + enOut2;
*/   
   assign     zen[2] = in1 ? in1 ^ zen[1] : 1'bz;
   assign     zen[1] = in1 ? in1 | zen[0] : 1'bz;
   assign     zen[0] = in1 ? in1 ^ zen[1] : 1'bz;   
endmodule // mysub

module constant(o);
   output o;
   assign o = 1;
endmodule // constant

module enabler(i, o, e);
   input i, e;
   output o;
   
   assign o = e ? i : 1'bz;
endmodule // enabler

module assigner(o, i);
   input i;
   output o;

   assign o = i;
endmodule // assigner
