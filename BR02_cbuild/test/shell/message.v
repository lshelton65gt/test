/*
 * this module calls $readmemh on every clock on a missing file
 * in order to generate a runtime error message that is output 
 * via the model's message context.
 */

module message(clk, o);

  input clk;
  output o;
  reg [0:31] o;

  reg [0:7] mem [0:10];

  initial
    o = 0;

  always @ (posedge clk)
    begin
      $readmemh("missing.file", mem);   // generate an error message
      o = mem[o];
    end

endmodule