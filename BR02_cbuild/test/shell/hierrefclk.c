
#include "libhierrefclk.h"
#include <assert.h>

int main()
{
  CarbonObjectID* hdl;

  CarbonWaveID* wave;

  hdl = carbon_hierrefclk_create(eCarbonFullDB,  eCarbon_NoFlags);
  assert(hdl);
  wave = carbonWaveInitVCD(hdl, "hierrefclk.dump.vcd", e1ns);
  assert(wave);
  carbonDumpVars(wave, 0, "hierrefclk");
  carbonDestroy(&hdl);
  return 0;
}
