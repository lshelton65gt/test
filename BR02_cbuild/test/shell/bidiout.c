#include "libbidiout.h"
#include <stdio.h>

static const int fail = 3;

int main()
{
  CarbonObjectID* hdl;

  CarbonNetID* in;
  CarbonNetID* clk;
  CarbonNetID* iout;
  CarbonNetID* iout2;

  int i;
  UInt32 drv;
  CarbonTime time = 0;
  int stat = 0;

  hdl = carbon_bidiout_create(eCarbonFullDB, eCarbon_NoFlags);
  if (! hdl)
    return fail;

  in = carbonFindNet(hdl, "bidiout.in");
  if (! in)
  {
    stat = fail;
    fprintf(stderr, "in not found\n");
  }

  clk = carbonFindNet(hdl, "bidiout.clk");
  if (! clk)
  {
    stat = fail;
    fprintf(stderr, "clk not found\n");

  }

  iout = carbonFindNet(hdl, "bidiout.iout");
  if (! iout)
  {
    stat = fail;
    fprintf(stderr, "iout not found\n");
  }

  iout2 = carbonFindNet(hdl, "bidiout.iout2");
  if (! iout2)
  {
    stat = fail;
    fprintf(stderr, "iout2 not found\n");
  }


  carbonDepositWord(hdl, in, 0, 0, 0);
  carbonDepositWord(hdl, clk, 0, 0, 0);
  carbonSchedule(hdl, time);
  time += 10;

  carbonDepositWord(hdl, clk, 1, 0, 0);

  carbonSchedule(hdl, time);

  drv = 0xffffffff;
  carbonExamineWord(hdl, iout, NULL, 0, &drv);

  if (drv != 0)
  {
    stat = fail;
    fprintf(stderr, "iout is not being driven internally!\n");
  }

  if (carbonIsTristate(iout2) == 0)
  {
    stat = fail;
    fprintf(stderr, "iout1 is not a tristate!\n");
  }

  drv = 0;
  carbonExamineWord(hdl, iout2, NULL, 0, &drv);

  if (drv != 0x1)
  {
    stat = fail;
    fprintf(stderr, "iout2 is being driven internally!\n");
  }

  return stat;
}
