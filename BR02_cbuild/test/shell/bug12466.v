module bug(clk, in, out);
   input clk;
   input [31:0] in;
   output [31:0] out;

   parameter MASK = 8'b00001111;

   generate
      genvar     i;
      for (i = 0; i < 8; i = i + 1) begin: g
         if (MASK[i]) begin
            mid mid(.clk(clk), .in(in[i*4+3:i*4]), .out(out[i*4+3:i*4]));
         end
         else begin
            assign out[i*4+3:i*4] = 4'b0000;
         end
      end

   endgenerate
endmodule
   
module mid(clk, in, out);
   input clk;
   input [3:0] in;
   output [3:0] out;

   reg [3:0]    in_r;

   always @(posedge clk)
     in_r <= in;

   sub sub(.clk(clk), .in(in_r), .out(out));
   
endmodule

module sub(clk, in, out);
   input clk;
   input [3:0] in;
   output [3:0] out;

   reg [3:0]    out;
   reg          in_r_0;
   reg          in_r_1;
   reg          in_r_2;
   reg          in_r_3;

   always @(posedge clk) begin
      {in_r_3, in_r_2, in_r_1, in_r_0} <= in;
      out <= {in_r_3, in_r_2, in_r_1, in_r_0};
   end
endmodule
