
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;


entity RAM1RW is 
   generic(
	databits	: 	INTEGER := 44; 
	adrbits		:	INTEGER := 5;         
	memsize		:	INTEGER := 28);         

    port(clk	: in STD_ULOGIC; 				
	 adr	: in STD_LOGIC_VECTOR ((adrbits-1) downto 0); 	
	 din	: in STD_LOGIC_VECTOR ((databits-1) downto 0); 
 	 wen	: in STD_ULOGIC;				
	 dout	: out STD_LOGIC_VECTOR ((databits-1) downto 0)); 
end RAM1RW;


architecture CARBONARCH of  RAM1RW is
	
--   TYPE t_memcore is ARRAY (0 TO ((2**adrbits)-1)) of STD_LOGIC_VECTOR((databits-1) DOWNTO 0);
   TYPE t_memcore is ARRAY (0 TO ((memsize)-1)) of STD_LOGIC_VECTOR((databits-1) DOWNTO 0);

begin


       mem_process: process(clk)
         variable memcore  	 : t_memcore;
         VARIABLE i_adr              : integer;
       begin
         if(clk'event and clk='0')then
            if(wen ='0')then
                  -- memcore(conv_integer(unsigned(adr))) <= din; 
                  i_adr := conv_integer(unsigned(adr));
                  memcore(i_adr) := din;
		  dout <= din;
	    elsif (wen = '1')  then
                  -- dout  <= memcore(conv_integer(unsigned(adr))); 
                  i_adr := conv_integer(unsigned(adr));
                  dout <= memcore(i_adr);
            end if;
         end if;
      end process;

end CARBONARCH;

