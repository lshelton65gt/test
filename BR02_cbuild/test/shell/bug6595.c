
#include "libbug6595.h"
#include "carbon/carbon_dbapi.h"
#include <assert.h>

int main()
{
  CarbonObjectID* hdl;
  CarbonDB* db;
  const CarbonDBNode* b;
  int lsb,msb;
  
  hdl = carbon_bug6595_create(eCarbonFullDB, eCarbon_NoFlags);
  db = carbonGetDB(hdl);
  b = carbonDBFindNode(db, "top.b");
  lsb = carbonDBGetLSB(db, b);
  msb = carbonDBGetMSB(db, b);
  assert(lsb == 1 && msb == 9);
  return 0;
}
