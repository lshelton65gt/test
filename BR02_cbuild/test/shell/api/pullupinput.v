/* 
 * testcase from bug 565. make sure that pulled inouts are coerced
 * to inouts and that deposits/examines of these inouts works as
 * expected.
 */
module pullupinput(clk, in, out);
   input      clk;
   input [10:5] in; // expect coercion to inout with -coercePorts
   output [6:1] out;
   reg [6:1] 	out;
   
   pullup p1 [10:5](in);

   always @(negedge clk)
     out[1] <= in[10];
   
   always @(negedge clk)
     out[2] <= in[9];
   
   always @(negedge clk)
     out[3] <= in[8];
   
   always @(negedge clk)
     out[4] <= in[7];

   always @(negedge clk)
     out[5] <= in[6];
   
   always @(negedge clk)
     out[6] <= in[5];
         
endmodule
