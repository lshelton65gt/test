
`ifdef VERILOG
module stupid;
   reg in_en;
   reg in1;
   reg [7:0] in8;
   reg [64:0] in65;
   reg clk;
   
   wire bid1;
   wire [7:0] bid8;
   wire [64:0] bid65;
   wire [73:0] allout;

   reg [73:0]  bid1_reg;


   assign      bid1 = ~in_en ? bid1_reg[0] : 1'bz;
   assign      bid8 = ~in_en ? bid1_reg[7:0] : 8'bz;
   assign      bid65 = ~in_en ? bid1_reg : 65'bz;
   
   testbidi tbd (clk, bid1, bid8, bid65, in_en, in1, in8, in65, allout);

   always #10 clk = ~clk;

   initial
     begin
        bid1_reg <= 0;
        clk <= 0;
        in_en <= 1;
        in1 <= 0;
        in8 <= 0;
        in65 <= 0;

        $dumpvars(0, tbd);
        
        repeat (2) @(posedge clk);
        bid1_reg = bid1_reg + 1;
        @(negedge clk)
          in_en <= 0;
        
        repeat (2) @(posedge clk) bid1_reg = bid1_reg + 1;
        
        @(negedge clk)
          in_en <= 1;
        bid1_reg = 1;
        repeat (2) @(posedge clk);
        $finish;
     end
endmodule // stupid
`endif //  `ifdef VERILOG

module testbidi(clk, bid1, bid8, bid36, bid65, in_en, in1, in8, in65, allout, nutherout);
   
   input clk;
   inout bid1;
   inout [7:0] bid8;
   inout [35:0] bid36;
   inout [64:0] bid65;
   
   input        in_en;
   input        in1;
   
   input [7:0]  in8;
   
   input [64:0] in65;

   output [73:0]    allout;
   reg [73:0]       allout;
   output [35:0]    nutherout;
   reg [35:0]       nutherout;

   

   reg              bid1_reg;
   reg [7:0]        bid8_reg;
   reg [64:0]       bid65_reg;
   reg [35:0]       bid36_reg;
   
   assign           bid1 = in_en ? bid1_reg : 1'bz;
   assign           bid8 = in_en ? bid8_reg : 8'bz;
   assign           bid65 = in_en ? bid65_reg : 65'bz;
   assign           bid36 = in_en ? bid36_reg: 36'bz;
            
   always @(posedge clk or in_en)
     begin
        bid65_reg <= in65;
        bid8_reg <= in8;
        bid1_reg <= in1;
        bid36_reg <= in65;
        
        if (in_en == 0)
          begin
             allout <= {bid65, bid8, bid1};
             nutherout <= bid36;
          end
     end
endmodule // testbidi
