/* 
 * testdriver from bug 565. make sure that pulled inouts are coerced
 * to inouts and that deposits/examines of these inouts works as
 * expected.
 */

#include "libpullupinput.h"
#include <stdio.h>              // For NULL
#include <cassert>

int main()
{
  CarbonObjectID* hdl = carbon_pullupinput_create(eCarbonIODB, eCarbon_NoFlags);
  assert(hdl);
  CarbonWaveID* wave = carbonWaveInitVCD(hdl, "pullupinputvcd.dump", e1ns);
  assert(wave);
  assert(carbonDumpVars(wave, 0, "pullupinput") == eCarbon_OK);

  CarbonNetID* clk = carbonFindNet(hdl, "pullupinput.clk");
  assert(clk);

  CarbonNetID* in = carbonFindNet(hdl, "pullupinput.in");
  assert(in);
  assert(carbonIsTristate(in));
  assert(carbonGetNumUInt32s(in) == 1);
  
  CarbonNetID* out = carbonFindNet(hdl, "pullupinput.out");
  assert(out);

  int i;
  UInt32 clkval = 0;

  UInt32 inval = 0;
  UInt32 indrv = 0;

  UInt32 outval = 0;
  UInt32 outdrv = 0;

  CarbonTime time = 0;

  assert(carbonGetPullMode(hdl, in) == eCarbonPullUp);
  assert(carbonGetPullMode(hdl, clk) == eCarbonNoPull);

  // TEST 1: Ensure that 'in' is pulled by the model and that pull is read by 'out'.

  // Run (negedge clk) schedule without driving in.
  clkval = 1;
  carbonDeposit(hdl, clk, &clkval, NULL);
  carbonSchedule(hdl, time);
  time += 10;

  clkval = 0;
  carbonDeposit(hdl, clk, &clkval, NULL);
  carbonSchedule(hdl, time);
  time += 10;

  
  carbonExamine(hdl, in, &inval, &indrv);
  assert(inval == 0x3f); // value from pullup.
  assert(indrv == 0x3f); // weakly driven.

  carbonExamine(hdl, out, &outval, &outdrv);
  assert(outval == 0x3f); // value read from in (which is pulled).
  assert(outdrv == 0x00); // strongly driven.

  // TEST 2: Drive 'in' externally and verify that value is read by 'out'.

  // Drive 'in'.
  inval = 0;
  indrv = 0;
  carbonDeposit(hdl, in, &inval, &indrv);

  // Run (negedge clk) schedule.
  clkval = 1;
  carbonDeposit(hdl, clk, &clkval, NULL);
  carbonSchedule(hdl, time);
  time += 10;

  clkval = 0;
  carbonDeposit(hdl, clk, &clkval, NULL);
  carbonSchedule(hdl, time);
  time += 10;

  carbonExamine(hdl, in, &inval, &indrv);
  assert(inval == 0x00); // value from our drive.
  assert(indrv == 0x3f); // strongly driven.

  carbonExamine(hdl, out, &outval, &outdrv);
  assert(outval == 0x00); // value read from in (which is driven).
  assert(outdrv == 0x00); // strongly driven.

  // TEST 3: Remove drive on 'in' and ensure that the pullup again has effect.

  // Remove drive on 'in'.
  inval = 0;
  indrv = 0x3f;
  carbonDeposit(hdl, in, &inval, &indrv);

  // Run (negedge clk) schedule.
  clkval = 1;
  carbonDeposit(hdl, clk, &clkval, NULL);
  carbonSchedule(hdl, time);
  time += 10;

  clkval = 0;
  carbonDeposit(hdl, clk, &clkval, NULL);
  carbonSchedule(hdl, time);
  time += 10;

  
  carbonExamine(hdl, in, &inval, &indrv);
  assert(inval == 0x3f); // value from pullup.
  assert(indrv == 0x3f); // weakly driven.

  carbonExamine(hdl, out, &outval, &outdrv);
  assert(outval == 0x3f); // value read from in (which is pulled).
  assert(outdrv == 0x00); // strongly driven.

  carbonDestroy(&hdl);
  return 0;
}
