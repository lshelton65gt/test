
module tristate1 (clk, 
                  in1, en1, out1, 
                  in8, en8, out8, 
                  in16, en16, out16, 
                  in32, en32, out32, 
                  in64, en64, out64, 
                  in72, en72, out72);
   input clk;
   input in1, en1;
   input [7:0] in8;
   input [7:0] en8;
   input [15:0] in16;
   input [15:0] en16;
   input [31:0] in32;
   input [31:0] en32;
   input [63:0] in64;
   input [63:0] en64;
   input [71:0] in72;
   input [71:0] en72;
   output       out1;
   tri          out1;
   output [7:0] out8;
   tri [7:0]    out8;
   output [15:0] out16;
   tri [15:0]    out16;
   output [31:0] out32;
   tri [31:0]    out32;
   output [63:0] out64;
   tri [63:0]    out64;
   output [71:0] out72;
   tri [71:0]    out72;
   
   reg           out1reg; // carbon depositSignal
   reg [7:0]     out8reg;
   reg [15:0]    out16reg;
   reg [31:0]    out32reg;
   reg [63:0]    out64reg;
   reg [71:0]    out72reg;
   
   assign        out1 = en1 ? out1reg : 1'bz;
   assign        out8[0] = en8[0] ? out8reg[0] : 1'bz;
   assign        out8[1] = en8[1] ? out8reg[1] : 1'bz;
   assign        out8[2] = en8[2] ? out8reg[2] : 1'bz;
   assign        out8[3] = en8[3] ? out8reg[3] : 1'bz;
   assign        out8[4] = en8[4] ? out8reg[4] : 1'bz;
   assign        out8[5] = en8[5] ? out8reg[5] : 1'bz;
   assign        out8[6] = en8[6] ? out8reg[6] : 1'bz;
   assign        out8[7] = en8[7] ? out8reg[7] : 1'bz;
   
   bufif1 tri16 [15:0] (out16, out16reg, en16);
   bufif1 tri32 [31:0] (out32, out32reg, en32);
   bufif1 tri64 [63:0] (out64, out64reg, en64);
   bufif1 tri72 [71:0] (out72, out72reg, en72);

   dummy dum1 (.in(in1), .en(en1));
   
   always @(posedge clk)
     begin
        out1reg <= in1;
        out8reg <= in8;
        out16reg <= in16;
        out32reg <= in32;
        out64reg <= in64;
        out72reg <= in72;
     end // always @ (posedge clk)
endmodule // tristate1

module dummy(in, en);
   input in, en;

   subdum sub1(.in(in), .en(en));
endmodule // dummy

module subdum(in, en);
   input in, en;
endmodule // subdum
