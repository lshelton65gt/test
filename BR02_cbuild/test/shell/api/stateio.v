
module stateio(in, out, iout, clk);
   input in;
   output out;
   reg    out;
   
   inout  iout;
   input  clk;

   wire   chk;
   assign chk = in | iout;
   
   always @(negedge clk)
     out = chk;
   
   inst inst1(iout, clk, in);
   
endmodule // stateio

   
module inst(out, clk, in);
   output out;
   reg    out;
   
   input  clk;
   input  in;
   
   wire d;
   wire notclk;
   assign notclk = ~clk ^ in;
   assign d = notclk;
   
   always @(negedge d)
     out = in;
endmodule // inst

   