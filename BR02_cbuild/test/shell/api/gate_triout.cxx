#include "libgate_triout.h"
#include <cassert>
#include <cstring>

int main()
{
  CarbonObjectID* hdl = carbon_gate_triout_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonObjectID* hdlCheck = carbon_gate_triout_create(eCarbonFullDB, eCarbon_NoFlags);
  assert(hdl);
  assert(hdlCheck);
  CarbonWaveID* wave = carbonWaveInitFSDB(hdl, "gate_trioutfsdb.dump", e1ns);
  assert(wave);
  assert(carbonDumpVars(wave, 0, "gate_triout") == eCarbon_OK);
  
  CarbonNetID* clk = carbonFindNet(hdl, "gate_triout.clk");
  CarbonNetID* in32 = carbonFindNet(hdl, "gate_triout.in32");
  CarbonNetID* en32 = carbonFindNet(hdl, "gate_triout.en32");
  CarbonNetID* in72 = carbonFindNet(hdl, "gate_triout.in72");
  CarbonNetID* en72 = carbonFindNet(hdl, "gate_triout.en72");
  assert(clk);
  assert(in32);
  assert(en32);
  assert(in72);
  assert(en72);

  UInt32 clkv = 0;
  UInt32 in32v = 0;
  UInt32 en32v = 0;
  UInt32 in72v[3];
  memset (in72v, 0, 3 * sizeof(UInt32));
  UInt32 en72v[3];
  memset (en72v, 0, 3 * sizeof(UInt32));

  UInt32 checkClkv = 0;
  UInt32 checkIn32v = 0;
  UInt32 checkEn32v = 0;
  UInt32 checkIn72v[3];
  UInt32 checkEn72v[3];

  UInt32 out32v;
  UInt32 drive32v;
  UInt32 out72v[3];
  UInt32 drive72v[3];

  UInt32 checkOut32v;
  UInt32 checkDrive32v;
  UInt32 checkOut72v[3];
  UInt32 checkDrive72v[3];

  CarbonNetID* out32 = carbonFindNet(hdl, "gate_triout.out32");
  CarbonNetID* out72 = carbonFindNet(hdl, "gate_triout.out72");
  CarbonNetID* outc = carbonFindNet(hdl, "gate_triout.outc");
  CarbonNetID* outd = carbonFindNet(hdl, "gate_triout.outd");
  CarbonNetID* oute = carbonFindNet(hdl, "gate_triout.oute");
  CarbonNetID* outf = carbonFindNet(hdl, "gate_triout.outf");
  CarbonNetID* outa = carbonFindNet(hdl, "gate_triout.outa");
  CarbonNetID* outb = carbonFindNet(hdl, "gate_triout.outb");
  
  assert(out32);
  assert(out72);
  assert(outc);
  assert(outd);
  assert(oute);
  assert(outf);
  assert(outa);
  assert(outb);

  CarbonNetID* checkOut32 = carbonFindNet(hdl, "gate_triout.out32");
  CarbonNetID* checkOut72 = carbonFindNet(hdl, "gate_triout.out72");
  CarbonNetID* checkOutc = carbonFindNet(hdl, "gate_triout.outc");
  CarbonNetID* checkOutd = carbonFindNet(hdl, "gate_triout.outd");
  CarbonNetID* checkOute = carbonFindNet(hdl, "gate_triout.oute");
  CarbonNetID* checkOutf = carbonFindNet(hdl, "gate_triout.outf");
  CarbonNetID* checkOuta = carbonFindNet(hdl, "gate_triout.outa");
  CarbonNetID* checkOutb = carbonFindNet(hdl, "gate_triout.outb");
  CarbonNetID* checkClk = carbonFindNet(hdl, "gate_triout.clk");
  CarbonNetID* checkIn32 = carbonFindNet(hdl, "gate_triout.in32");
  CarbonNetID* checkEn32 = carbonFindNet(hdl, "gate_triout.en32");
  CarbonNetID* checkIn72 = carbonFindNet(hdl, "gate_triout.in72");
  CarbonNetID* checkEn72 = carbonFindNet(hdl, "gate_triout.en72");
  assert(checkClk);
  assert(checkIn32);
  assert(checkEn32);
  assert(checkIn72);
  assert(en72);
  
  assert(checkOut32);
  assert(checkOut72);
  assert(checkOutc);
  assert(checkOutd);
  assert(checkOute);
  assert(checkOutf);
  assert(checkOuta);
  assert(checkOutb);
  
  // initialize and run a cycle.
  carbonDeposit(hdl, clk, &clkv, 0);
  carbonDeposit(hdl, in32, &in32v, 0);
  carbonDeposit(hdl, en32, &en32v, 0);
  carbonDeposit(hdl, in72, in72v, 0);
  carbonDeposit(hdl, en72, en72v, 0);

  int i;
  CarbonTime time = 0;
  for (i = 0; i < 3; ++i)
  {
    carbonSchedule(hdl, time);
    time += 10;
    clkv = ! clkv;
    carbonDeposit(hdl, clk, &clkv, 0);
  }
  
  // check output v. expected
  carbonExamine(hdl, out32, &out32v, &drive32v);
  carbonExamine(hdl, out72, out72v, drive72v);
  assert(carbonSave(hdl, "examine1.chkpt") == eCarbon_OK);
  assert(carbonRestore(hdlCheck, "examine1.chkpt") == eCarbon_OK);
  assert(carbonGetSimulationTime(hdlCheck) == carbonGetSimulationTime(hdl));
  // do save/restore check
  
  // CODEGEN BUG - needs to distinguish btwn. x and z.

  assert(drive32v == 0xffffffff);
  assert(drive72v[0] == 0xffffffff);
  assert(drive72v[1] == 0xffffffff);
  assert(drive72v[2] == 0xff);

  carbonExamine(hdlCheck, checkClk, &checkClkv, &checkDrive32v);
  assert(memcmp(&checkClkv, &clkv, sizeof checkClkv) == 0);
  assert(checkDrive32v == 1);

  carbonExamine(hdlCheck, checkEn72, checkEn72v, checkDrive72v);
  assert(memcmp(checkEn72v, en72v, sizeof en72v) == 0);
  assert(checkDrive72v[2] == 0xff);
  assert(checkDrive72v[1] == 0xffffffff);
  assert(checkDrive72v[0] == 0xffffffff);

  carbonExamine(hdlCheck, checkIn72, checkIn72v, checkDrive72v);
  assert(memcmp(checkIn72v, in72v, sizeof in72v) == 0);
  assert(checkDrive72v[2] == 0xff);
  assert(checkDrive72v[1] == 0xffffffff);
  assert(checkDrive72v[0] == 0xffffffff);

  carbonExamine(hdlCheck, checkIn32, &checkIn32v, &checkDrive32v);
  assert(memcmp(&checkIn32v, &in32v, sizeof in32v) == 0);
  assert(checkDrive32v == 0xffffffff);
  
  carbonExamine(hdlCheck, checkEn32, &checkEn32v, &checkDrive32v);
  assert(memcmp(&checkEn32v, &en32v, sizeof en32v) == 0);
  assert(checkDrive32v == 0xffffffff);

  carbonExamine(hdl, outd, out72v, drive72v);
  carbonExamine(hdlCheck, checkOutd, checkOut72v, checkDrive72v);

  assert(drive72v[0] == 0);
  assert(drive72v[1] == 0);
  assert((drive72v[2] & 0xff) == 0);
  assert(out72v[0] == in72v[0]);
  assert(out72v[1] == in72v[1]);
  assert(out72v[2] == in72v[2]);

  assert(memcmp(checkOut72v, out72v, sizeof out72v) == 0);
  assert(memcmp(checkDrive72v, drive72v, sizeof drive72v) == 0);
  /*
  assert(drive72v[0] == checkDrive72v[0]);
  assert(drive72v[1] == checkDrive72v[1]);
  assert(drive72v[2] == checkDrive72v[2]);
  assert(out72v[0] == checkOut72v[0]);
  assert(out72v[1] == checkOut72v[1]);
  assert(out72v[2] == checkOut72v[2]);
  */

  out32v = 0;
  carbonExamineWord(hdl, out72, &out32v, 2, &drive32v);
  assert(drive32v == 0xff);
  
  carbonExamine(hdl, outc, &out32v, &drive32v);
  carbonExamine(hdlCheck, checkOutc, &checkOut32v, &checkDrive32v);
  assert(drive32v == 0);
  assert(out32v == in32v);
  assert(drive32v == checkDrive32v);
  assert(out32v == checkOut32v);
  
  carbonExamine(hdl, oute, &out32v, &drive32v);
  carbonExamine(hdlCheck, checkOute, &checkOut32v, &checkDrive32v);
  assert(drive32v == 1);
  assert(out32v == (in72v[0] & 0x1));
  assert(drive32v == checkDrive32v);
  assert(out32v == checkOut32v);

  carbonExamine(hdl, outf, &out32v, &drive32v);
  carbonExamine(hdlCheck, checkOutf, &checkOut32v, &checkDrive32v);
  assert(drive32v == 0xffffffff);
  assert(drive32v == checkDrive32v);
  assert(out32v == checkOut32v);
  
  // Check carbonGetExternalDrive functionality
  drive32v = 0;
  carbonGetExternalDrive(hdl, outf, &drive32v);
  assert(drive32v == 0xffffffff);

  carbonExamine(hdl, outa, &out32v, &drive32v);
  carbonExamine(hdlCheck, checkOuta, &checkOut32v, &checkDrive32v);
  assert(drive32v == 0x1f);
  assert(drive32v == checkDrive32v);
  assert(out32v == checkOut32v);

  carbonExamine(hdl, outb, &out32v, &drive32v);
  carbonExamine(hdlCheck, checkOutb, &checkOut32v, &checkDrive32v);
  assert(drive32v == 0);
  assert(out32v == ((in32v >> 1) & 0xf));
  assert(drive32v == checkDrive32v);
  assert(out32v == checkOut32v);

  // word and range enabling/valchange
  in32v = 0xff;
  en32v = 0x1ff;
  carbonDepositRange(hdl, in32, &in32v, 18, 10, 0);
  carbonDepositRange(hdl, en32, &en32v, 18, 10, 0);
  in72v[1] = 0x0f0f0f0f;
  in72v[0] = 0xf;
  carbonDepositWord(hdl, in72, in72v[1], 1, 0);
  carbonDepositRange(hdl, in72, in72v, 31, 28, 0);
  {
    UInt32 tmp = 1;
    carbonDepositRange(hdl, in72, &tmp, 71, 71, 0);
  }
  en72v[0] = 0xffffffff;
  en72v[1] = 0xffffffff;
  en72v[2] = 0x9f;
  carbonDepositRange(hdl, en72, en72v, 71, 0, 0);
  
  // Run 2 cycles
  for (i = 0; i < 5; ++i)
  {
    carbonSchedule(hdl, time);
    time += 10;
    clkv = ! clkv;
    carbonDeposit(hdl, clk, &clkv, 0);
  }
  
  // check all the outputs again, with range's and words too
  carbonExamine(hdl, out32, &out32v, &drive32v);
  assert(out32v == 0x3fc00);
  assert(drive32v == UInt32(~(0x7fc00)));
  carbonExamineWord(hdl, out32, &out32v, 0, &drive32v);
  assert(out32v == 0x3fc00);
  assert(drive32v == UInt32(~(0x7fc00)));
  drive32v = 0;
  out32v = 0;
  carbonExamineRange(hdl, out32, &out32v, 18, 10, &drive32v);
  assert(out32v == in32v);
  assert(drive32v == 0);

  carbonExamine(hdl, out72, out72v, drive72v);
  assert(drive72v[0] == 0);
  assert(drive72v[1] == 0);
  assert(drive72v[2] == 0x60);
  assert(out72v[0] == 0xf0000000);
  assert(out72v[1] == in72v[1]);
  out32v = 0;
  drive32v = 0xf;
  carbonExamineWord(hdl, out72, &out32v, 1, &drive32v);
  assert(out32v == in72v[1]);
  assert(drive32v == 0);
  drive32v = 0xf;
  out32v = 0;
  carbonExamineRange(hdl, out72, &out32v, 31, 28, &drive32v);
  assert(out32v == 0xf);
  assert(drive32v == 0);
  
  carbonExamine(hdl, outd, out72v, drive72v);
  assert(drive72v[0] == 0xffffffff);
  assert(drive72v[1] == 0xffffffff);
  assert((drive72v[2] & 0xff) == 0x9f);
  assert((out72v[2] & 0x9f) == (in72v[2] & 0x9f));
  
  out32v = 0;
  drive32v = 0;
  carbonExamine(hdl, outc, &out32v, &drive32v);
  assert(drive32v == 0x7fc00);
  
  out32v = 0;
  drive32v = 0;
  carbonExamine(hdl, oute, &out32v, &drive32v);
  assert(drive32v == 1);

  out32v = 0;
  drive32v = 0;
  carbonExamine(hdl, outf, &out32v, &drive32v);
  assert(drive32v == 0);
  
  out32v = 0;
  drive32v = 0;
  carbonExamineRange(hdl, outa, &out32v, 4, 1, &drive32v);
  assert(drive32v == 0xc);
  assert(out32v  == 0);
  
  out32v = 0;
  drive32v = 0;
  carbonExamineRange(hdl, outa, &out32v, 0, 0, &drive32v);
  assert(drive32v == 0);
  assert(out32v == 1);

  carbonDepositWord(hdl, en32, 0, 0, 0);
  carbonSchedule(hdl, time);
  time += 10;
  carbonExamine(hdl, out32, &out32v, &drive32v);
  assert(out32v == 0);
  assert(drive32v == 0xffffffff);
  carbonDepositWord(hdl, out32, 0xffffffff, 0, 0);

  // check external drive on a deposited non-bidi tristate. Since we
  // cannot keep xdrive separate from idrive on a non-bidi tristate,
  // we always say that the external drive on a deposited tristate is
  // all ones (not driving).
  drive32v = 0;
  carbonGetExternalDrive(hdl, out32, &drive32v);
  assert(drive32v == 0xffffffff);

  carbonSchedule(hdl, time);
  carbonExamine(hdl, out32, &out32v, &drive32v);
  
  // This is wrong. See bug6760. out32v should == 0
  // and drive32v should == 0xffffffff
  assert(out32v == 0xffffffff);
  assert(drive32v == 0);

  drive32v = 0;
  carbonGetExternalDrive(hdl, out32, &drive32v);
  assert(drive32v == 0xffffffff);
  
  carbonDestroy(&hdl);
  carbonDestroy(&hdlCheck);
  return 0;
}
