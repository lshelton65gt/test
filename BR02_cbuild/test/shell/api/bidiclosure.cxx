
#include "libbidiclosure.h"
#include <assert.h>

int main() 
{
  CarbonObjectID* model = carbon_bidiclosure_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonWaveID* wave = carbonWaveInitVCD(model, "bidiclosure.dump", e1ns);

  // If bidi counting is incorrect, we will assert here
  carbonDumpVars(wave, 0, "top");
  
  // just in case, close up the wave processing;
  carbonWaveHierarchyClose(wave);
  carbonDestroy(&model);
}
