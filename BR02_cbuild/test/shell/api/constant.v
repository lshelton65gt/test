
module top(clk, in, out1, out2);
   input clk;
   input in;
   output [73:0] out1;
   output [3:0]  out2;
   
   wire    s1;
   wire [20:0] s21;
   wire [31:0] s32;
   wire [34:0] s35;
   wire [63:0] s64;
   wire [71:0] s72;
   wire [3:0]  stri;
   wire [9:0]  sx;
   
   reg [73:0]  out1_reg;

/*
   assign        s1 = 1'b1;
   assign        s21 = 21'h10f0f0;
   assign        s32 = 32'haaaaaaaa;
   assign        s35 = 35'hbbbbbbbb;
   assign        s64 = 64'h1;
   assign        s72 = 72'hccccccccdddd;
   assign        stri = 4'b10z0;
*/
   constant c(s1, s21, s32, s35, s64, s72, stri, sx);

   assign     out2 = stri;
   assign     out1 = out1_reg;
   
   always @(posedge clk)
     out1_reg = (in ^ s1) + s21 - s35 * (s64 ^ (s72 - s32)) + (sx & 1);
   
endmodule // top

module constant(s1, s21, s32, s35, s64, s72, stri, sx);
   output    s1;
   output [21:1] s21;
   output [31:0] s32;
   output [34:0] s35;
   output [63:0] s64;
   output [71:0] s72;
   output [3:0]  stri;
   output [9:0]  sx;

   wire [9:0]    to_sx = 10'bx;
   
   assign        s1 = 1'b1;
   assign        s21 = 21'h10f0f0;
   assign        s32 = 32'haaaaaaaa;
   assign        s35 = 35'hbbbbbbbb;
   assign        s64 = 64'h1;
   assign        s72 = 72'hf0000ccccccccdddd;
   assign        stri = 4'b10z0;
   assign        sx = to_sx;
endmodule // constant
