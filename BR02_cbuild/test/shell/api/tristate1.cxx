#include "libtristate1.h"
#include <cassert>
#include <iostream>

int main()
{
  CarbonObjectID* model = carbon_tristate1_create(eCarbonFullDB, eCarbon_NoFlags);
  assert(model);
  //  CarbonWave* wave = model->waveCreate("tristate1fsdb.dump",
  //  eWaveFileTypeFSDB, e1ns);
#if 1
  CarbonWaveID* wave = carbonWaveInitFSDB(model, "tristate1fsdb.dump", e1ns);
#else
  CarbonWaveID* wave = carbonWaveInitVCD(model, "tristate1fsdb.vcd", e1ns);
#endif

  assert(wave);
  
  CarbonNetID* clk = carbonFindNet(model, "tristate1.clk");
  CarbonNetID* in1 = carbonFindNet(model, "tristate1.in1");
  CarbonNetID* en1 = carbonFindNet(model, "tristate1.en1");
  CarbonNetID* in8 = carbonFindNet(model, "tristate1.in8");
  CarbonNetID* en8 = carbonFindNet(model, "tristate1.en8");
  CarbonNetID* in16 = carbonFindNet(model, "tristate1.in16");
  CarbonNetID* en16 = carbonFindNet(model, "tristate1.en16");
  CarbonNetID* in32 = carbonFindNet(model, "tristate1.in32");
  CarbonNetID* en32 = carbonFindNet(model, "tristate1.en32");
  CarbonNetID* in64 = carbonFindNet(model, "tristate1.in64");
  CarbonNetID* en64 = carbonFindNet(model, "tristate1.en64");
  CarbonNetID* in72 = carbonFindNet(model, "tristate1.in72");
  CarbonNetID* en72 = carbonFindNet(model, "tristate1.en72");
  CarbonNetID* out1 = carbonFindNet(model, "tristate1.out1");
  CarbonNetID* out8 = carbonFindNet(model, "tristate1.out8");
  CarbonNetID* out16 = carbonFindNet(model, "tristate1.out16");
  CarbonNetID* out32 = carbonFindNet(model, "tristate1.out32");
  CarbonNetID* out64 = carbonFindNet(model, "tristate1.out64");
  CarbonNetID* out72 = carbonFindNet(model, "tristate1.out72");

  assert(carbonIsTristate(out1) == 1);
  assert(carbonIsPrimaryPort(model, out1) == 1);
  assert(carbonGetWaveTime(wave) == 0);

  assert(clk);
  assert(in1);
  assert(en1);
  assert(out1);
  assert(in8);
  assert(en8);
  assert(out8);
  assert(in16);
  assert(en16);
  assert(out16);
  assert(in32);
  assert(en32);
  assert(out32);
  assert(in64);
  assert(en64);
  assert(out64);
  assert(in72);
  assert(en72);
  assert(out72);

  assert(carbonDumpVar(wave, clk) == eCarbon_OK);
  assert(carbonDumpVar(wave, in1) == eCarbon_OK);
  assert(carbonDumpVar(wave, en1) == eCarbon_OK);
  assert(carbonDumpVar(wave, out1) == eCarbon_OK);
  assert(carbonDumpVar(wave, in8) == eCarbon_OK);
  assert(carbonDumpVar(wave, en8) == eCarbon_OK);
  assert(carbonDumpVar(wave, out8) == eCarbon_OK);
  assert(carbonDumpVar(wave, in16) == eCarbon_OK);
  assert(carbonDumpVar(wave, en16) == eCarbon_OK);
  assert(carbonDumpVar(wave, out16) == eCarbon_OK);
  assert(carbonDumpVar(wave, in32) == eCarbon_OK);
  assert(carbonDumpVar(wave, en32) == eCarbon_OK);
  assert(carbonDumpVar(wave, out32) == eCarbon_OK);
  assert(carbonDumpVar(wave, in64) == eCarbon_OK);
  assert(carbonDumpVar(wave, en64) == eCarbon_OK);
  assert(carbonDumpVar(wave, out64) == eCarbon_OK);
  assert(carbonDumpVar(wave, in72) == eCarbon_OK);
  assert(carbonDumpVar(wave, en72) == eCarbon_OK);
  assert(carbonDumpVar(wave, out72) == eCarbon_OK);

  assert(carbonGetWaveTime(wave) == 0);
  assert(carbonGetSimulationTime(model) == 0);

  carbonDumpOn(wave);

  // some partial vector enabling
  UInt32 clkVal = 0;
  UInt32 in1Val = 1;
  UInt32 en1Val = 1;
  UInt32 in8Val = 1;
  UInt32 en8Val = 1;
  UInt32 in16Val = 1;
  UInt32 en16Val = 1;
  UInt32 in32Val = 1;
  UInt32 en32Val = 1;

  UInt32 in64Val[2] = { 1,1 };
  UInt32 en64Val[2] = { 1,1 };
  
  UInt32 in72Val[3] = { 1,1,1 };
  UInt32 en72Val[3] = { 1,1,1 };

  UInt32 out1Ex[1];
  UInt32 out8Ex[1];
  UInt32 out16Ex[1];
  UInt32 out32Ex[1];
  UInt32 out64Ex[2];
  UInt32 out72Ex[3];
  
  carbonDeposit(model, clk, &clkVal, 0);
  carbonDeposit(model, in1, &in1Val, 0);
  carbonDeposit(model, en1, &en1Val, 0);
  carbonDeposit(model, in8, &in8Val, 0);
  carbonDeposit(model, en8, &en8Val, 0);
  carbonDeposit(model, in16, &in16Val, 0);
  carbonDeposit(model, en16, &en16Val, 0);
  carbonDeposit(model, in32, &in32Val, 0);
  carbonDeposit(model, en32, &en32Val, 0);
  carbonDeposit(model, in64, in64Val, 0);
  carbonDeposit(model, en64, en64Val, 0);
  carbonDeposit(model, in72, in72Val, 0);
  carbonDeposit(model, en72, en72Val, 0);

  int i;
  CarbonTime time = 10;
  CarbonWaveUserDataID* mydata1 = NULL;

  { // scope junk to make sure we don't reference it again
    // if we do, we'll get strange errors. EMC does this!
    double junk = 12.2;
    mydata1 = carbonAddUserData(wave, eCARBON_VT_VCD_REAL,
                                eCarbonVarDirectionImplicit,
                                eCARBON_DT_VERILOG_REAL,
                                0,0, &junk, "junk", 
                                eCARBON_BYTES_PER_BIT_8B,
                                "C++.for1", ".");
    assert(mydata1);

    for (i = 0; i < 3; ++i) {

      // skip the first i. Make sure addUserData didn't register a
      // change
      if (i != 0)
        carbonWaveUserDataChange(mydata1);
      carbonSchedule(model, time);
      clkVal = !clkVal;
      carbonDeposit(model, clk, &clkVal, 0);
      time += 10;
      junk += 2.8;

    }
  }
  
  // check carbonExternalDrive on all the outputs. They should return
  // all 1s.
  carbonGetExternalDrive(model, out1, out1Ex);
  carbonGetExternalDrive(model, out8, out8Ex);
  carbonGetExternalDrive(model, out16, out16Ex);
  carbonGetExternalDrive(model, out32, out32Ex);
  carbonGetExternalDrive(model, out64, out64Ex);
  carbonGetExternalDrive(model, out72, out72Ex);
  
  if ((out1Ex[0] != 1) ||
      (out8Ex[0] != 0xff) ||
      (out16Ex[0] != 0xffff) ||
      (out32Ex[0] != 0xffffffff) ||
      (out64Ex[0] != 0xffffffff) ||
      (out64Ex[1] != 0xffffffff) ||
      (out72Ex[0] != 0xffffffff) ||
      (out72Ex[1] != 0xffffffff) ||
      (out72Ex[2] != 0xff))
    assert("carbonGetExternalDrive incorrect for tri outputs" == NULL);

  // full enable
  in1Val = ~in1Val + 1;
  en1Val = ~en1Val + 1;
  in8Val = ~in8Val + 1;
  en8Val = ~en8Val + 1;
  in16Val = ~in16Val + 1;
  en16Val = ~en16Val + 1;
  in32Val = ~in32Val + 1;
  en32Val = ~en32Val + 1;
  
  in64Val[0] = 0xffffffff;
  in64Val[1] = 0xffffffff;
  en64Val[0] = 0xffffffff;
  en64Val[1] = 0xffffffff;
  
  in72Val[0] = 0xffffffff;
  in72Val[1] = 0xffffffff;
  in72Val[2] = 0xffffffff;
  en72Val[0] = 0xffffffff;
  en72Val[1] = 0xffffffff;
  en72Val[2] = 0xffffffff;

  carbonDeposit(model, in1, &in1Val, 0);
  carbonDeposit(model, en1, &en1Val, 0);
  carbonDeposit(model, in8, &in8Val, 0);
  carbonDeposit(model, en8, &en8Val, 0);
  carbonDeposit(model, in16, &in16Val, 0);
  carbonDeposit(model, en16, &en16Val, 0);
  carbonDeposit(model, in32, &in32Val, 0);
  carbonDeposit(model, en32, &en32Val, 0);
  carbonDeposit(model, in64, in64Val, 0);
  carbonDeposit(model, en64, en64Val, 0);
  carbonDeposit(model, in72, in72Val, 0);
  carbonDeposit(model, en72, en72Val, 0);


  CarbonWaveUserDataID* mydata2 = NULL;
  { // scoped. Will free mydata2 out of scope
    UInt32 junk2 = 5;
    mydata2 = carbonAddUserData(wave, eCARBON_VT_VCD_INTEGER,
                                eCarbonVarDirectionImplicit,
                                eCARBON_DT_VERILOG_INTEGER,
                                0,0, &junk2, "junk_int", 
                                eCARBON_BYTES_PER_BIT_4B,
                                "C++.for2", ".");
    assert(mydata2);
    
  
    for (i = 0; i < 3; ++i) {
      carbonWaveUserDataChange(mydata2);
      carbonSchedule(model, time);
      clkVal = !clkVal;
      carbonDeposit(model, clk, &clkVal, 0);
      time += 10;
      junk2 -= 1;
    }
  }

  carbonFreeWaveUserData(&mydata2);
  assert(mydata2 == NULL);


  // on the last time, deposit to out1reg and update the waveform
  // manually
  CarbonNetID* out1reg = carbonFindNet(model, "tristate1.out1reg");
  assert(out1reg);
  assert(carbonDepositWord(model, out1reg, 0, 0, 0) == eCarbon_OK);
  assert(carbonWaveSchedule(wave) == eCarbon_OK);

  // disable completely
    
  en1Val = 0;
  en8Val = 0;
  en16Val = 0;
  en32Val = 0;

  en64Val[0] = 0;
  en64Val[1] = 0;
  
  en72Val[0] = 0;
  en72Val[1] = 0;
  en72Val[2] = 0;

  carbonDeposit(model, en1, &en1Val, 0);
  carbonDeposit(model, en8, &en8Val, 0);
  carbonDeposit(model, en16, &en16Val, 0);
  carbonDeposit(model, en32, &en32Val, 0);
  carbonDeposit(model, en64, en64Val, 0);
  carbonDeposit(model, en72, en72Val, 0);

  CarbonWaveUserDataID* mydata3 = NULL;
  CarbonLogicByteType vec2[2];
  mydata3 = carbonAddUserData(wave, eCARBON_VT_VCD_REG,
                              eCarbonVarDirectionInout,
                              eCARBON_DT_VERILOG_STANDARD,
                              4,5, vec2, "junk_logic", 
                              eCARBON_BYTES_PER_BIT_1B,
                              "C++.for3", ".");
  
  for (i = 0; i < 3; ++i) {
    if (i == 0)
    {
      vec2[0] = eCARBON_BT_VCD_1;
      vec2[1] = eCARBON_BT_VCD_Z;
    }
    else if (i == 1)
    {
      vec2[0] = eCARBON_BT_VCD_0;
      vec2[1] = eCARBON_BT_VCD_X;
    }
    else if (i == 2)
    {
      vec2[0] = eCARBON_BT_VCD_1;
      vec2[1] = eCARBON_BT_VCD_1;
    }
    
    carbonWaveUserDataChange(mydata3);
    carbonSchedule(model, time);
    clkVal = !clkVal;
    carbonDeposit(model, clk, &clkVal, 0);
    time += 10;
  }
  
  assert(carbonGetWaveTime(wave) == carbonGetSimulationTime(model));
  carbonDestroy(&model);
  return 0;
}
