#include "libinput_samplesched.h"
#include <cassert>
#include <stdio.h>
#include <string.h>

void valChangeCallback_ci1(CarbonObjectID * hdl,
			   CarbonNetID * /* net */,
			   CarbonClientData /* data */,
			   UInt32 * /* value */,
			   UInt32 * /* drive */)
{
   printf("Testbench: valChangeCallback_ci1 called at time %u\n", (UInt32)carbonGetSimulationTime(hdl));
}

int main(int argc, char* argv[])
{

  /*
   * disable buffering on stdout and stderr to force messages from the test
   * driver and model come out in a consistent order.
   */
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);

  bool doStaleChk = ((argc > 1) && (strcmp(argv[1], "-examineStale") == 0));
  
  CarbonObjectID* hdl = carbon_input_samplesched_create(eCarbonFullDB, eCarbon_NoFlags);
  assert(hdl);
  if (! doStaleChk)
  {
    CarbonWaveID* wave = carbonWaveInitVCD(hdl, "top_samplesched.vcd", e1ns);
    assert(wave);
    assert(carbonDumpVars(wave, 0, "top") == eCarbon_OK);
  }
  
  CarbonNetID* clk = carbonFindNet(hdl, "top.clk");
  assert(clk);
  CarbonNetID* in1 = carbonFindNet(hdl, "top.in1");
  assert(in1);
  CarbonNetID* in2 = carbonFindNet(hdl, "top.in2");
  assert(in2);
  CarbonNetID* ci1 = carbonFindNet(hdl, "top.ci1");
  assert(ci1);
  
  // add value change callback
  CarbonNetValueCBDataID* cbId = carbonAddNetValueChangeCB(hdl, valChangeCallback_ci1, 0, ci1);
  assert(cbId != 0);

  carbonDepositWord(hdl, clk, 1, 0, 0);
  carbonDepositWord(hdl, in1, 1, 0, 0);
  CarbonTime time = 0;
  carbonSchedule(hdl, time);
  time += 10;
  
  carbonDepositWord(hdl, clk, 0, 0, 0);
  
  carbonSchedule(hdl, time);
  time += 10;
  
  // do examine staleness tests
  // Run this no matter what. With waves, we won't have any stale
  // nets because the examined net is sample scheduled.
  
  UInt32 chkNetValue;
  // the warning should return eCarbon_OK.
  assert(carbonExamine(hdl, ci1, &chkNetValue, 0) == eCarbon_OK);
  carbonSchedule(hdl, time);
  time += 10;

  carbonDepositWord(hdl, in1, 1, 0, 0);
  carbonDepositWord(hdl, in2, 1, 0, 0);
  carbonSchedule(hdl, time);
  time += 10;

  assert(carbonExamine(hdl, ci1, &chkNetValue, 0) == eCarbon_OK);
  printf("ci value: %d\n", chkNetValue);
  time += 10;

  carbonDepositWord(hdl, in2, 0, 0, 0);
  // turn off the callback so we can check if the examine updates the
  // value too
  carbonDisableNetCB(hdl, cbId);
  carbonSchedule(hdl, time);
  time += 10;

  assert(carbonExamine(hdl, ci1, &chkNetValue, 0) == eCarbon_OK);
  printf("ci value: %d\n", chkNetValue);

  carbonEnableNetCB(hdl, cbId);
  carbonSchedule(hdl, time);
  time += 10;

  // do range and word versions as well
  assert(carbonExamineWord(hdl, ci1, &chkNetValue, 0, 0) == eCarbon_OK);
  carbonSchedule(hdl, time);
  time += 10;

  assert(carbonExamineRange(hdl, ci1, &chkNetValue, 0, 0, 0) == eCarbon_OK);
  carbonSchedule(hdl, time);
  time += 10;
  
  // now suppress it
  assert(carbonChangeMsgSeverity(hdl, 5014, eCarbonMsgSuppress) == eCarbon_OK);
  assert(carbonChangeMsgSeverity(hdl, 5045, eCarbonMsgSuppress) == eCarbon_OK);
  // do all the examines again.
  // the warning should return eCarbon_OK.
  assert(carbonExamine(hdl, ci1, &chkNetValue, 0) == eCarbon_OK);
  carbonSchedule(hdl, time);
  time += 10;

    // do range and word versions as well
  assert(carbonExamineWord(hdl, ci1, &chkNetValue, 0, 0) == eCarbon_OK);
  carbonSchedule(hdl, time);
  time += 10;

  assert(carbonExamineRange(hdl, ci1, &chkNetValue, 0, 0, 0) == eCarbon_OK);
  
  // make it an alert
  assert(carbonChangeMsgSeverity(hdl, 5014, eCarbonMsgAlert) == eCarbon_OK);
  assert(carbonChangeMsgSeverity(hdl, 5045, eCarbonMsgAlert) == eCarbon_OK);
  // These won't be stale if we are dumping waves.
  if (doStaleChk)
  {
    // do all the examine again, but this time they should return
    // eCarbon_ERROR
    // have to disable the callback though since that will cause the
    // debug sched to run.
    carbonDisableNetCB(hdl, cbId);
    carbonSchedule(hdl, time);
    time += 10;

    assert(carbonExamine(hdl, ci1, &chkNetValue, 0) == eCarbon_ERROR);
    carbonSchedule(hdl, time);
    time += 10;

    // do range and word versions as well
    assert(carbonExamineWord(hdl, ci1, &chkNetValue, 0, 0) == eCarbon_ERROR);
    carbonSchedule(hdl, time);
    time += 10;

    assert(carbonExamineRange(hdl, ci1, &chkNetValue, 0, 0, 0) == eCarbon_ERROR);
    carbonSchedule(hdl, time);
    time += 10;

  }
  
  carbonDestroy(&hdl);
  return 0;
}
