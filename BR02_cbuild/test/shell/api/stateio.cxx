#include "libstateio.h"
#include <stdio.h>              // For NULL
#include <cassert>

struct WaveInfo
{
  WaveInfo(const char* thename, unsigned int thedepth,
           const char* thespecifier) :
    name(thename), specifier(thespecifier), depth(thedepth)
  {}
  const char* name;
  const char* specifier;
  unsigned int depth;
};

int main()
{
  WaveInfo wave1("stateio.full.vcd", 0, "stateio");
  WaveInfo wave2("stateio.toplevel.vcd", 1, "stateio");
  WaveInfo wave3("stateio.inst1.vcd", 0, "stateio.inst1");
  
  WaveInfo* waveArr[3];
  waveArr[0] = &wave1;
  waveArr[1] = &wave2;
  waveArr[2] = &wave3;
  
  for (int i = 0; i < 3; ++i)
  {
    CarbonObjectID* hdl = carbon_stateio_create(eCarbonFullDB, eCarbon_NoFlags);
    assert(hdl);
    WaveInfo* wInfo = waveArr[i];
    CarbonWaveID* wave = carbonWaveInitVCD(hdl, wInfo->name, e1ns);
    assert(wave);
    assert(carbonDumpStateIO(wave, wInfo->depth, wInfo->specifier) == eCarbon_OK);

    CarbonNetID* clk = carbonFindNet(hdl, "stateio.clk");
    assert(clk);
    CarbonNetID* in = carbonFindNet(hdl, "stateio.in");
    CarbonNetID* iout = carbonFindNet(hdl, "stateio.iout");
    CarbonNetID* out = carbonFindNet(hdl, "stateio.inst1.out");
    
    assert(out);

    carbonDepositWord(hdl, clk, 1, 0, 0);
    carbonDepositWord(hdl, in, 1, 0, 0);
    carbonDepositWord(hdl, iout, 0, 0, 0);
    CarbonTime time = 0;
    carbonSchedule(hdl, time);
    time += 10;
    
    carbonDepositWord(hdl, iout, 0, 0, 1);
    carbonDepositWord(hdl, clk, 0, 0, 0);
    
    carbonSchedule(hdl, time);
    time += 10;

    UInt32 clkVal;
    UInt32 inVal;
    carbonExamine(hdl, clk, &clkVal, NULL);
    carbonExamine(hdl, in, &inVal, NULL);
    for (int j = 0; j < 10; ++j)
    {

      clkVal = !clkVal;
      carbonDeposit(hdl, clk, &clkVal, 0);
      if ((j % 2) == 0)
      {
        inVal = ! inVal;
        carbonDeposit(hdl, in, &inVal, 0);
      }
      carbonSchedule(hdl, time);
      time += 10;
    }
    carbonDestroy(&hdl);
  }
  return 0;
}
