#include "libconstant.h"
#include <stdio.h>              // For NULL
#include <string.h>             // For strcmp()
#include <cassert>

static void constNetCB(CarbonObjectID*, CarbonNetID*, CarbonClientData, CarbonUInt32*, CarbonUInt32*)
{
  assert(0);
}

int main()
{
  CarbonObjectID* model = carbon_constant_create(eCarbonFullDB, eCarbon_NoFlags);
  assert(model);
  CarbonWaveID* wave = carbonWaveInitFSDB(model, "constantfsdb.dump", e1ns);
  assert(wave);
  assert(carbonDumpVars(wave, 0, "top") == eCarbon_OK);
  
  CarbonNetID* clk = carbonFindNet(model, "top.clk");
  CarbonNetID* in = carbonFindNet(model, "top.in");
  assert(clk && in);
  
  CarbonTime time = 0;
  for (UInt32 i = 0; i < 64; ++i)
  {
    UInt32 val = i & 0x1;
    carbonDeposit(model, clk, &val, 0);
    val = !val;
    carbonDeposit(model, in, &val, 0);
    carbonSchedule(model, time);
    time += 10;
  }

  CarbonNetID* s1 = carbonFindNet(model, "top.s1");
  CarbonNetID* s21 = carbonFindNet(model, "top.s21");
  CarbonNetID* s32 = carbonFindNet(model, "top.s32");
  CarbonNetID* s35 = carbonFindNet(model, "top.s35");
  CarbonNetID* s64 = carbonFindNet(model, "top.s64");  
  CarbonNetID* s72 = carbonFindNet(model, "top.s72");  
  CarbonNetID* stri = carbonFindNet(model, "top.stri");  

  assert(s1);
  assert(s21);
  assert(s32);
  assert(s35);
  assert(s64);
  assert(s72);
  assert(stri);

  assert(carbonIsConstant(s1));
  assert(carbonIsConstant(s21));
  assert(carbonIsConstant(s32));
  assert(carbonIsConstant(s35));
  assert(carbonIsConstant(s64));
  assert(carbonIsConstant(s72));

  CarbonNetValueCBDataID* s1cb = carbonAddNetValueChangeCB(model, constNetCB, NULL, s1);
  assert(s1cb);
  // make sure use of the cb id doesn't cause warnings.
  carbonDisableNetCB(model, s1cb);
  carbonEnableNetCB(model, s1cb);

  UInt32 s1Ex[1];
  UInt32 s32Ex[1];
  UInt32 s35Ex[2];
  UInt32 s72Ex[3];

  carbonGetExternalDrive(model, s1, s1Ex);
  carbonGetExternalDrive(model, s32, s32Ex);
  carbonGetExternalDrive(model, s35, s35Ex);
  carbonGetExternalDrive(model, s72, s72Ex);

  if ((s1Ex[0] != 0x1) ||
      (s32Ex[0] != 0xffffffff) ||
      (s35Ex[0] != 0xffffffff) ||
      (s35Ex[1] != 0x7) ||
      (s72Ex[0] != 0xffffffff) ||
      (s72Ex[1] != 0xffffffff) ||
      (s72Ex[2] != 0xff))
    assert("carbonGetExternalDrive incorrect on constants." == NULL);
  
  CarbonNetID* cs1 = carbonFindNet(model, "top.c.s1"); assert (cs1);
  CarbonNetID* cs21 = carbonFindNet(model, "top.c.s21"); assert (cs21);
  CarbonNetID* cs32 = carbonFindNet(model, "top.c.s32");
  CarbonNetID* cs35 = carbonFindNet(model, "top.c.s35"); assert (cs35);
  CarbonNetID* cs64 = carbonFindNet(model, "top.c.s64"); assert (cs64);
  CarbonNetID* cs72 = carbonFindNet(model, "top.c.s72"); assert (cs72);
  CarbonNetID* cto_sx = carbonFindNet(model, "top.c.to_sx"); assert (cto_sx);
  
  UInt32 oneWord;
  //  UInt32 oneDrive;
  UInt32 twoWords[2];
  UInt32 threeWords[3];

  carbonExamine(model, s1, &oneWord, NULL);
  assert(oneWord == 1);
  carbonExamineWord(model, s1, &oneWord, 0, NULL);
  assert(oneWord == 1);
  carbonExamineRange(model, s1, &oneWord, 0, 0, NULL);
  assert(oneWord == 1);

  carbonExamine(model, s21, &oneWord, NULL);
  assert(oneWord == 0x10f0f0);
  oneWord = 0;
  carbonExamineWord(model, s21, &oneWord, 0, NULL);
  assert(oneWord == 0x10f0f0);
  oneWord = 0;
  carbonExamineRange(model, s21, &oneWord, 20, 16, NULL);
  assert(oneWord == 0x10);
  
  oneWord = 0;
  carbonExamine(model, s32, &oneWord, NULL);
  assert(oneWord == 0xaaaaaaaa);
  oneWord = 0;
  carbonExamineWord(model, s32, &oneWord, 0, NULL);
  assert(oneWord == 0xaaaaaaaa);
  oneWord = 0;
  carbonExamineRange(model, s32, &oneWord, 3, 1, NULL);
  assert(oneWord == 0x5);
  oneWord = 0;

  carbonExamine(model, s35, twoWords, NULL);
  assert((twoWords[0] == 0xbbbbbbbb) &&
         (twoWords[1] == 0));
  carbonExamineWord(model, s35, &oneWord, 0, NULL);
  assert(oneWord == 0xbbbbbbbb);
  oneWord = 1;
  carbonExamineRange(model, s35, &oneWord, 34, 32, NULL);
  assert(oneWord == 0);

  twoWords[0] = 0xf;
  twoWords[1] = 0xe;

  carbonExamine(model, s64, twoWords, NULL);
  assert((twoWords[0] == 0x1) &&
         (twoWords[1] == 0));
  carbonExamineWord(model, s64, &oneWord, 0, NULL);
  assert(oneWord == 0x1);
  carbonExamineWord(model, s64, &oneWord, 1, NULL);
  assert(oneWord == 0);
  oneWord = 1;
  carbonExamineRange(model, s64, &oneWord, 63, 32, NULL);
  assert(oneWord == 0);
  carbonExamineRange(model, s64, &oneWord, 0, 0, NULL);
  assert(oneWord == 1);

  carbonExamine(model, s72, threeWords, NULL);
  assert((threeWords[0] == 0xccccdddd) &&
         (threeWords[1] == 0xcccc) &&
         (threeWords[2] == 0xf));
  carbonExamineWord(model, s72, &oneWord, 0, NULL);
  assert(oneWord == 0xccccdddd);
  oneWord = 0;
  carbonExamineWord(model, s72, &oneWord, 1, NULL);
  assert(oneWord == 0xcccc);
  carbonExamineWord(model, s72, &oneWord, 2, NULL);
  assert(oneWord == 0xf);

  oneWord = 1;
  carbonExamineRange(model, s72, &oneWord, 71, 48, NULL);
  assert(oneWord == 0xf0000);
  carbonExamineRange(model, s72, twoWords, 71, 16, NULL);
  assert((twoWords[0] == 0xcccccccc) &&
         twoWords[1] == 0xf0000);
  carbonExamineRange(model, s72, threeWords, 71, 4, NULL);
  assert(threeWords[0] == 0xcccccddd);
  assert(threeWords[1] == 0xf0000ccc);
  assert(threeWords[2] == 0);

  assert(carbonExamine(model, s1, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(model, s21, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(model, s32, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(model, s35, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(model, s64, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(model, s72, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(model, stri, NULL, NULL) == eCarbon_OK);

  assert(carbonExamineRange(model, s1, NULL, 0, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(model, s21, NULL, 1, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(model, s32, NULL, 1, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(model, s35, NULL, 1, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(model, s64, NULL, 1, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(model, s72, NULL, 1, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(model, stri, NULL, 0, 0, NULL) == eCarbon_OK);


  oneWord = 0xffffffff;
  assert(carbonExamine(model, s1, NULL, &oneWord) == eCarbon_OK);
  assert(oneWord == 0);
  oneWord = 0xffffffff;
  assert(carbonExamine(model, s21, NULL, &oneWord) == eCarbon_OK);
  assert(oneWord == 0);
  oneWord = 0xffffffff;
  assert(carbonExamine(model, s32, NULL, &oneWord) == eCarbon_OK);
  assert(oneWord == 0);

  twoWords[0] = 0xffffffff;
  twoWords[1] = 0xffffffff;
  assert(carbonExamine(model, s35, NULL, twoWords) == eCarbon_OK);
  assert(twoWords[0] == 0);
  assert(twoWords[1] == 0);

  twoWords[0] = 0xffffffff;
  twoWords[1] = 0xffffffff;
  assert(carbonExamine(model, s64, NULL, twoWords) == eCarbon_OK);
  assert(twoWords[0] == 0);
  assert(twoWords[1] == 0);

  threeWords[0] = 0xffffffff;
  threeWords[1] = 0xffffffff;
  threeWords[2] = 0xffffffff;
  assert(carbonExamine(model, s72, NULL, threeWords) == eCarbon_OK);
  assert(threeWords[0] == 0);
  assert(threeWords[1] == 0);
  assert(threeWords[2] == 0);

  
  oneWord = 0x1;  
  assert(carbonExamineRange(model, s1, NULL, 0, 0, &oneWord) == eCarbon_OK);
  assert(oneWord == 0);
  oneWord = 0x3;
  assert(carbonExamineRange(model, s21, NULL, 1, 0, &oneWord) == eCarbon_OK);
  assert(oneWord == 0);
  oneWord = 0x3;  
  assert(carbonExamineRange(model, s32, NULL, 1, 0, &oneWord) == eCarbon_OK);
  assert(oneWord == 0);

  twoWords[0] = 1;
  twoWords[1] = 1;
  assert(carbonExamineRange(model, s35, NULL, 34, 0, twoWords) == eCarbon_OK);
  assert(twoWords[0] == 0);
  assert(twoWords[1] == 0);
  
  twoWords[0] = 1;
  twoWords[1] = 1;
  assert(carbonExamineRange(model, s64, NULL, 34, 0, twoWords) == eCarbon_OK);
  assert(twoWords[0] == 0);
  assert(twoWords[1] == 0);

  threeWords[0] = 1;
  threeWords[1] = 1;
  threeWords[2] = 1;
  assert(carbonExamineRange(model, s72, NULL, 64, 0, threeWords) == eCarbon_OK);
  assert(threeWords[0] == 0);
  assert(threeWords[1] == 0);
  assert(threeWords[2] == 0);

  char buf[100];
  // binary formatting is tested with vcd dumping. We have oct/hex/dec
  // to do
  assert(carbonFormat(model, cs1, buf, 100, eCarbonHex) == eCarbon_OK);
  assert(strcmp(buf, "1") == 0);
  assert(carbonFormat(model, cs1, buf, 100, eCarbonOct) == eCarbon_OK);
  assert(strcmp(buf, "1") == 0);
  assert(carbonFormat(model, cs1, buf, 100, eCarbonDec) == eCarbon_OK);
  assert(strcmp(buf, "1") == 0);
  assert(carbonFormat(model, cs1, buf, 100, eCarbonUDec) == eCarbon_OK);
  assert(strcmp(buf, "1") == 0);

  assert(carbonFormat(model, cs21, buf, 100, eCarbonHex) == eCarbon_OK);
  assert(strcmp(buf, "10f0f0") == 0);
  assert(carbonFormat(model, cs21, buf, 100, eCarbonOct) == eCarbon_OK);
  assert(strcmp(buf, "4170360") == 0);
  assert(carbonFormat(model, cs21, buf, 100, eCarbonDec) == eCarbon_OK);
  assert(strcmp(buf, "-986896") == 0);
  assert(carbonFormat(model, cs21, buf, 100, eCarbonUDec) == eCarbon_OK);
  assert(strcmp(buf, "1110256") == 0);
  
  assert(carbonFormat(model, cs32, buf, 100, eCarbonHex) == eCarbon_OK);
  assert(strcmp(buf, "aaaaaaaa") == 0);
  assert(carbonFormat(model, cs32, buf, 100, eCarbonOct) == eCarbon_OK);
  assert(strcmp(buf, "25252525252") == 0);
  assert(carbonFormat(model, cs32, buf, 100, eCarbonDec) == eCarbon_OK);
  assert(strcmp(buf, "-1431655766") == 0);
  assert(carbonFormat(model, cs32, buf, 100, eCarbonUDec) == eCarbon_OK);
  assert(strcmp(buf, "2863311530") == 0);
  
  assert(carbonFormat(model, cs35, buf, 100, eCarbonHex) == eCarbon_OK);
  assert(strcmp(buf, "0bbbbbbbb") == 0);
  assert(carbonFormat(model, cs35, buf, 100, eCarbonOct) == eCarbon_OK);
  assert(strcmp(buf, "027356735673") == 0);
  assert(carbonFormat(model, cs35, buf, 100, eCarbonDec) == eCarbon_OK);
  assert(strcmp(buf, "3149642683") == 0);
  assert(carbonFormat(model, cs35, buf, 100, eCarbonUDec) == eCarbon_OK);
  assert(strcmp(buf, "3149642683") == 0);

  assert(carbonFormat(model, cs64, buf, 100, eCarbonHex) == eCarbon_OK);
  assert(strcmp(buf, "0000000000000001") == 0);
  assert(carbonFormat(model, cs64, buf, 100, eCarbonOct) == eCarbon_OK);
  assert(strcmp(buf, "0000000000000000000001") == 0);
  assert(carbonFormat(model, cs64, buf, 100, eCarbonDec) == eCarbon_OK);
  assert(strcmp(buf, "1") == 0);
  assert(carbonFormat(model, cs64, buf, 100, eCarbonUDec) == eCarbon_OK);
  assert(strcmp(buf, "1") == 0);


  assert(carbonFormat(model, cto_sx, buf, 100, eCarbonHex) == eCarbon_OK);
  assert(strcmp(buf, "xxx") == 0);
  assert(carbonFormat(model, cto_sx, buf, 100, eCarbonOct) == eCarbon_OK);
  assert(strcmp(buf, "xxxx") == 0);
  assert(carbonFormat(model, cto_sx, buf, 100, eCarbonDec) == eCarbon_OK);
  assert(strcmp(buf, "x") == 0);
  assert(carbonFormat(model, cto_sx, buf, 100, eCarbonUDec) == eCarbon_OK);
  assert(strcmp(buf, "x") == 0);
  
  /*
    carbonExamine(model, stri, &oneWord, &oneDrive);
    assert(oneWord == 8);
    assert(oneDrive == 2);
  */
  carbonDestroy(&model);

  model = carbon_constant_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID* aliasFreeChk = carbonFindNet(model, "top.s1");
  carbonFreeHandle(model, &aliasFreeChk);
  assert(aliasFreeChk == NULL);
  carbonDestroy(&model);
  return 0;
}
