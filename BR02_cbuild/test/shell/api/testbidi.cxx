#include "libtestbidi.h"
#include <cassert>
#include <cstring>

int main(int argc, char* argv[]) 
{
  CarbonObjectID* model = carbon_testbidi_create(eCarbonFullDB, eCarbon_NoFlags);
  assert(model);
  CarbonWaveID* wave = carbonWaveInitVCD(model, "testbidivcd.dump", e1ns);
  assert(wave);
  assert(carbonDumpVars(wave, 0, "testbidi") == eCarbon_OK);
  
  bool doSaveRestoreTest = false;
  CarbonUInt32 i;
  for (i = 1; i < argc; ++i)
  {
    if (strcmp(argv[i], "-saveRestoreTest") == 0)
      doSaveRestoreTest = true;
  }
  
  CarbonNetID* in_en = carbonFindNet(model, "testbidi.in_en");
  CarbonNetID* clk = carbonFindNet(model, "testbidi.clk");
  CarbonNetID* bid1 = carbonFindNet(model, "testbidi.bid1"); 
  CarbonNetID* bid8 = carbonFindNet(model, "testbidi.bid8");
  CarbonNetID* bid65 = carbonFindNet(model, "testbidi.bid65");
  CarbonNetID* in1 = carbonFindNet(model, "testbidi.in1"); 
  CarbonNetID* in8 = carbonFindNet(model, "testbidi.in8");
  CarbonNetID* in65 = carbonFindNet(model, "testbidi.in65");
  CarbonNetID* allout = carbonFindNet(model, "testbidi.allout");
  CarbonNetID* bid36 = carbonFindNet(model, "testbidi.bid36");

  assert(in_en);
  assert(clk);
  assert(bid1);
  assert(bid8);
  assert(bid65);
  assert(in1);
  assert(in8);
  assert(in65);
  assert(allout);
  assert(bid36);

  UInt32 clkv = 0;
  UInt32 inenv = 0;
  UInt32 bid1v = 0;  // doubles for in1
  UInt32 drv1v = 1;
  UInt32 bid8v = 0; // doubles of in8
  UInt32 drv8v = ~0;
  UInt32 bid65v[3];  // doubles for in65
  UInt32 drv65v[3];
  memset(bid65v, 0, sizeof(UInt32) * 3);
  memset(drv65v, 0xffffffff, sizeof(UInt32) * 3);

  
  UInt32 bid1Ex[1];
  UInt32 bid8Ex[1];
  UInt32 bid65Ex[3];

  bid65Ex[0] = 0;
  bid65Ex[1] = 0;
  bid65Ex[2] = 0;

  carbonGetExternalDrive(model, bid1, bid1Ex);
  carbonGetExternalDrive(model, bid8, bid8Ex);
  carbonGetExternalDrive(model, bid65, bid65Ex);

  if ((bid1Ex[0] != 1) ||
      (bid8Ex[0] != 0xff) ||
      (bid65Ex[0] != 0xffffffff) ||
      (bid65Ex[1] != 0xffffffff) ||
      (bid65Ex[2] != 1))
    assert("carbonGetExternalDrive failed for undriven bids." == NULL);
  
  carbonDeposit(model, clk, &clkv, 0);
  carbonDeposit(model, in_en, &inenv, 0);
  carbonDeposit(model, bid36, drv65v, NULL);
  //  model->depositWord(bid1, bid1v, 0, drv1v);
  // model->deposit(bid8, &bid8v, &drv8v);
  // model->deposit(bid65, bid65v, drv65v);

  carbonDeposit(model, in1, &bid1v, &drv1v);
  carbonDeposit(model, in8, &bid8v, &drv8v);
  carbonDeposit(model, in65, bid65v, drv65v);
  
  CarbonTime time = 0;
  for (i = 0; i < 3; ++i)
  {
    carbonSchedule(model, time);
    time += 10;
    clkv = !clkv;
    carbonDeposit(model, clk, &clkv, 0);
  }

  // We expect the bids to be undriven
  carbonExamine(model, bid1, &bid1v, &drv1v);
  carbonExamine(model, bid8, &bid8v, &drv8v);
  carbonExamine(model, bid65, bid65v, drv65v);
  
  assert(drv1v == 1);
  assert(drv8v == 0xff);
  assert(drv65v[0] == 0xffffffff);
  assert(drv65v[1] == 0xffffffff);
  assert(drv65v[2] == 1);

  // enable the inputs
  inenv = !inenv;
  carbonDeposit(model, in_en, &inenv, 0);

  for (i = 0; i < 3; ++i)
  {
    carbonSchedule(model,time);
    time += 10;
    clkv = !clkv;
    carbonDeposit(model, clk, &clkv, 0);
  }

  // We expect the bids to be driven
  carbonExamine(model, bid1, &bid1v, &drv1v);
  carbonExamine(model, bid8, &bid8v, &drv8v);
  carbonExamine(model, bid65, bid65v, drv65v);
  assert(drv1v == 0);
  assert(drv8v == 0);
  assert((drv65v[0] == 0) && 
         (drv65v[1] == 0) && 
         (drv65v[2] == 0));
  assert(bid1v == 0);
  assert(bid8v == 0);
  assert((bid65v[0] == 0) && 
         (bid65v[1] == 0) && 
         (bid65v[2] == 0));


  // disable the inputs and change the drivers
  inenv = 0;
  carbonDeposit(model, in_en, &inenv, 0);
  
  bid1v = 1;
  //  drv1v = 0;
  carbonDeposit(model, bid1, &bid1v, &drv1v);
  
  bid8v = 255;
  carbonDeposit(model, bid8, &bid8v, &drv8v);
  
  bid65v[0] = 0xff;
  bid65v[1] = 0xff0;
  bid65v[2] = 1;
  carbonDeposit(model, bid65, bid65v, drv65v);

  carbonGetExternalDrive(model, bid1, bid1Ex);
  carbonGetExternalDrive(model, bid8, bid8Ex);
  carbonGetExternalDrive(model, bid65, bid65Ex);

  if ((bid1Ex[0] != 0) ||
      (bid8Ex[0] != 0) ||
      (bid65Ex[0] != 0) ||
      (bid65Ex[1] != 0) ||
      (bid65Ex[2] != 0))
    assert("carbonGetExternalDrive failed for driven bids." == NULL);
  

  if (doSaveRestoreTest)
  {
    assert(carbonSave(model, "model.save") == eCarbon_OK);
    assert(carbonRestore(model,  "model.save") == eCarbon_OK);
    carbonExamine(model, bid1, bid1Ex, 0);
    carbonExamine(model, bid65, bid65Ex, 0);
    
    assert(bid1Ex[0] == 1);
    assert(bid65Ex[0] == 0xff);
    assert(bid65Ex[1] == 0xff0);
    assert(bid65Ex[2] == 1);
    
    bid1v = 0;
    //  drv1v = 0;
    carbonDeposit(model, bid1, &bid1v, &drv1v);
    bid65v[0] = 0x0;
    bid65v[1] = 0x0;
    bid65v[2] = 0;
    carbonDeposit(model, bid65, bid65v, drv65v);
    
    assert(carbonRestore(model,  "model.save") == eCarbon_OK);
    carbonExamine(model, bid1, bid1Ex, 0);
    assert(bid1Ex[0] == 1);
    assert(bid65Ex[0] == 0xff);
    assert(bid65Ex[1] == 0xff0);
    assert(bid65Ex[2] == 1);
  }

  bid1v = 0;
  carbonDeposit(model, in1, &bid1v, &drv1v);

  bid8v = 192;
  carbonDeposit(model, in8, &bid8v, &drv8v);

  bid65v[0] = 0xff00;
  bid65v[1] = 0x1;
  bid65v[2] = 0;
  carbonDeposit(model, in65, bid65v, drv65v);
  

  for (i = 0; i < 3; ++i)
  {
    carbonSchedule(model, time);
    time += 10;
    clkv = !clkv;
    carbonDeposit(model, clk, &clkv, 0);
  }

  // check allout. It should be the same values as bid ins
  memset(bid65v, 0, sizeof(UInt32) * 3);
  carbonExamineRange(model, allout, bid65v, 0, 0, NULL);
  assert(bid65v[0] == 1);
  carbonExamineRange(model, allout, bid65v, 8, 1, NULL);
  assert(bid65v[0] == 255);
  carbonExamineRange(model, allout, bid65v, 73, 9, NULL);
  assert((bid65v[0] == 0xff) &&
         (bid65v[1] == 0xff0) &&
         (bid65v[2] == 1));
  
  // now enable the drivers and check the output
  inenv = !inenv;
  carbonDeposit(model, in_en, &inenv, 0);

  // disable external drivers
  drv1v = 1;
  //carbonDeposit(model, bid1, NULL, &drv1v);
  assert(carbonDeAssertXdrive(model, bid1) == eCarbon_OK);

  drv8v = 0xff;
  // carbonDeposit(model, bid8, NULL, &drv8v);
  assert(carbonDeAssertXdrive(model, bid8) == eCarbon_OK);

  drv65v[0] = 0xffffffff;
  drv65v[1] = 0xffffffff;
  drv65v[2] = 0x1;
  //carbonDeposit(model, bid65, NULL, drv65v);
  assert(carbonDeAssertXdrive(model, bid65) == eCarbon_OK);
  
  for (i = 0; i < 3; ++i)
  {
    carbonSchedule(model, time);
    time += 10;
    clkv = !clkv;
    carbonDeposit(model, clk, &clkv, 0);
  }

  // We expect the bids to be driven
  carbonExamine(model, bid1, &bid1v, &drv1v);
  carbonExamine(model, bid8, &bid8v, &drv8v);
  carbonExamine(model, bid65, bid65v, drv65v);
  assert(drv1v == 0);
  assert(drv8v == 0);
  assert((drv65v[0] == 0) && 
         (drv65v[1] == 0) && 
         (drv65v[2] == 0));
  assert(bid1v == 0);
  assert(bid8v == 192);
  assert((bid65v[0] == 0xff00) && 
         (bid65v[1] == 1) && 
         (bid65v[2] == 0));

  // This next part is for noflow differences.
  carbonSchedule(model, time);
  time += 10;
  carbonExamine(model, clk, &clkv, NULL);
  if (clkv == 1)
  {
    clkv = !clkv;
    carbonDeposit(model, clk, &clkv, 0);
    carbonSchedule(model, time);
    time += 10;
  }

  // disabling the inputs should cause allout to be updated
  // immediately with flow, and updated on the next cycle with no
  // flow.
  assert(inenv == 1);
  inenv = ! inenv;
  carbonDeposit(model, in_en, &inenv, 0);

  // enable external drivers
  drv1v = 0;
  carbonDeposit(model, bid1, NULL, &drv1v);
  
  drv8v = 0x0;
  carbonDeposit(model, bid8, NULL, &drv8v);
  
  drv65v[0] = 0;
  drv65v[1] = 0;
  drv65v[2] = 0;
  carbonDeposit(model, bid65, NULL, drv65v);

  clkv = ! clkv;
  carbonDeposit(model, clk, &clkv, 0);
  for (i = 0; i < 4; ++i)
  {
    carbonSchedule(model, time);
    time += 10;
    clkv = !clkv;
    carbonDeposit(model, clk, &clkv, 0);
  }

  // conflict detection. enable each bidi externally and internally,
  // run the schedule and examine each comparing the i/xdrives.

  // First, there shouldn't be any conflict currently.
  assert(carbonHasDriveConflict(model, bid1) == 0);
  assert(carbonHasDriveConflict(model, bid8) == 0);
  assert(carbonHasDriveConflict(model, bid65) == 0);


  bid1v = 1;
  drv1v = 0;
  carbonDeposit(model, bid1, &bid1v, &drv1v);
  bid8v = 55;
  drv8v = 0;
  carbonDeposit(model, bid8, &bid8v, &drv8v);

  bid65v[0] = 0xf0f0;
  bid65v[1] = 0xff00;
  bid65v[2] = 0x00ff;
  drv65v[0] = 0;
  drv65v[1] = 0;
  drv65v[2] = 0;
  carbonDeposit(model, bid65, bid65v, drv65v);
  

  // enable the internal drive
  inenv = 1;
  carbonDeposit(model, in_en, &inenv, 0);

  // really only need to do 1 schedule call here, but 2 is fine also.
  for (i = 0; i < 2; ++i)
  {
    carbonSchedule(model, time);
    time += 10;
    clkv = !clkv;
    carbonDeposit(model, clk, &clkv, 0);
  }
  
  // test the isDriveConflict with non-tristate in and out.
  assert(carbonHasDriveConflict(model, in_en) == 0);
  assert(carbonHasDriveConflict(model, allout) == 0);
  
  assert(carbonHasDriveConflict(model, bid1) == 1);
  assert(carbonHasDriveConflict(model, bid8) == 1);
  assert(carbonHasDriveConflict(model, bid65) == 1);

  carbonDestroy(&model);
  return 0;
}
