
module gate_triout (clk,
                    in32, en32, out32, 
                    in72, en72, out72,
                    outa, outb, outc, outd, oute, outf);
   input clk;
   input [31:0] in32;
   input [31:0] en32;
   input [71:0] in72;
   input [71:0] en72;
   output [31:0] out32;
   tri [31:0]    out32;
   output [71:0] out72;
   tri [71:0]    out72;
   output [31:0] outc;
   tri [31:0]    outc;
   output [71:0] outd;
   tri [71:0]    outd;
   output        oute;
   tri           oute;
   output [31:0] outf;
   tri [31:0]    outf;
   
   output [4:0]  outa;
   tri [4:0]     outa;
   output [4:0]  outb;
   tri [4:0]     outb;

   reg [31:0]    out32reg;
   reg [71:0]    out72reg;
   
   bufif1 tri32 [31:0] (out32, out32reg, en32);
   bufif1 tri72 [71:0] (out72, out72reg, en72);
   bufif1 a_buf [3:0] (outa[4:1], out72reg[15:12], en72[70:67]);
   bufif1 b_buf (outa[0], out72reg[71], en72[71]);
   bufif0 c_buf [3:0] (outb[4:1], out32reg[4:1], en72[70:67]);
   bufif0 d_buf [3:0] (outb[0], out72reg[71], en32[0]);
   bufif0 e_buf [31:0] (outc, out32reg, en32);
   bufif0 f_buf [71:0] (outd, out72reg, en72);
   bufif1 g_buf (oute, out72reg[0], en32[31]);
   bufif1 h_buf [31:0] (outf, out32reg, en72[71]);
   
   always @(posedge clk)
     begin
        out32reg <= in32;
        out72reg <= in72;
     end // always @ (posedge clk)
endmodule // tristate1
