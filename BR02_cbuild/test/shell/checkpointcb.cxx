// Test interaction of checkpointing and callbacks.
// Some code stolen from flopreg.cxx.

#include "libflopreg.h"
#include <stdlib.h>
#include <iostream>
#include <cassert>
//#include "util/CarbonPlatform.h"
using namespace std;

static CarbonTime sTime = 0;
static CarbonNetID* sNetID = NULL;
static CarbonObject* sHdl = NULL;

// Callback function
static void netCB(CarbonObjectID* model, CarbonNetID* net,
                  CarbonClientData data, UInt32* val, UInt32* drv)
{
  int* i = static_cast<int*>(data);
  ++(*i);

  assert(val != NULL);
  assert(drv != NULL);

  cerr << "net changed, new value " << *val << '\n';
}

void depositAndRun(CarbonUInt32 val)
{
  carbonDeposit(sHdl, sNetID, &val, 0);
  carbonSchedule(sHdl, sTime++);
}

int main()
{
  cerr << "Creating model.\n";
  sHdl = carbon_flopreg_create(eCarbonFullDB, eCarbon_NoFlags);
  if (!sHdl) {
    cerr << "create failed.\n";
    return EXIT_FAILURE;
  }

  cerr << "Finding net.\n";
  sNetID = carbonFindNet(sHdl, "flopreg.clk");
  if (!sNetID)
  {
    cerr << "carbonFindNet failed.\n";
  }

  int numCBs = 0;      // Number of times callback function called.
  carbonAddNetValueChangeCB(sHdl, &netCB, &numCBs, sNetID);
  cerr << "Deposit 1\n";
  depositAndRun(1);

  cerr << "Save when 1\n";
  carbonSave(sHdl, "flopreg.checkpoint");

  cerr << "Deposit 0\n";
  depositAndRun(0);             // Alternatively, could recreate design.


  //
  // First restore from checkpoint and ensure that there are no
  // spurious callbacks when we don't deposit.
  //

  numCBs = 0;                   // Start fresh
  cerr << "Restore, to 1\n";
  carbonRestore(sHdl, "flopreg.checkpoint");

  // Verify callback isn't called when there's no change after restore.
  cerr << "After restore, call schedule and expect no callback.\n";
  carbonSchedule(sHdl, sTime++);
  // This previously failed because restore didn't reset shadow values.
  assert(numCBs == 0);          // No callback called with no change.


  //
  // Restore again and deposit to verify that we do get the callback.
  //

  cerr << "Deposit 0\n";
  depositAndRun(0);

  numCBs = 0;                   // Start fresh
  cerr << "Restore, to 1\n";
  carbonRestore(sHdl, "flopreg.checkpoint");

  cerr << "Deposit 0 and expect callback\n";
  depositAndRun(0);
  // This previously failed because restore didn't reset shadow values.
  assert(numCBs == 1);          // Assert that deposit caused callback

  cerr << "Deposit 1 and expect callback\n";
  depositAndRun(1);
  assert(numCBs == 2);          // Assert that deposit caused callback
}
