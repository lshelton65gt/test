
module top(a, wide66, wide35, wide17, wide9, wide3, out1, out2, out3, out4, out5);
   input a;
   
   input signed [65:0] wide66;
   input signed [34:0] wide35;
   input signed [16:0] wide17;
   input signed [8:0] wide9;
   input signed [2:0] wide3;
   
   output out1, out2, out3, out4, out5;

   wire signed [65:0] wideC66;
   wire signed [34:0] wideC35;
   wire signed [16:0] wideC17;
   wire signed [8:0] wideC9;
   wire signed [2:0] wideC3;   
   assign wideC66 = a;
   assign wideC35 = a;
   assign wideC17 = a;
   assign wideC9 = a;
   assign wideC3 = a;

   assign out1 = (wideC66 < wide66) ? 0 : 1;
   assign out2 = (wideC35 < wide35) ? 0 : 1;
   assign out3 = (wideC17 < wide17) ? 0 : 1;
   assign out4 = (wideC9 < wide9) ? 0 : 1;
   assign out5 = (wideC3 < wide3) ? 0 : 1;
endmodule
