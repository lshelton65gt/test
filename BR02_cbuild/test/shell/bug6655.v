module dut(input clk, input in, output reg out);
   always @(posedge clk)
     out <= in;
endmodule
