// last mod: Mon Mar 28 13:24:10 2005
// filename: test/shell/tempvar.v
// Description:  This test is designed to check that carbonSave will save temp 
// variables that happen to be defined at the top level.  To get such a variable 
// we define a module that will be flattened and has a local register.
// as of 03/28/05 this testcase fails with -noFlatten because the save operation
// did not save the tempvar.


module tempvar(clock, in, out);
   input clock;
   input [7:0] in;
   output [7:0] out;

   mid i1 (clock, out, in);
endmodule

module mid(clk, out, in);
   input clk;
   input [7:0] in;
   output [7:0] out;
   reg [7:0] local;		// we expect this reg to be promoted to the top
				// level when module mid is flattened

   initial
     local = 0;
   assign    out = local | in;
   always @(posedge clk)
     local = local + 1 ;
endmodule
