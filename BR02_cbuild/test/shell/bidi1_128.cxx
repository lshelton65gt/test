#include "libbidi1_128.h"
//#include "shell/CarbonModel.h"
#include <iostream>

bool findNet(CarbonObjectID* hdl, const char* netName, CarbonNetID** netRef)
{
  bool error = false;
  *netRef = carbonFindNet(hdl, netName);
  if (*netRef == NULL)
  {
    error = true;
    std::cerr << "Error: Failed to get a handle to " << netName << "\n";
  }
  return error;
}

void
outputCallback(CarbonObjectID* model, CarbonNetID* cdsHndl, 
               void* param, UInt32* val, UInt32* drive)
{
  std::cout << std::dec
            << "Callback called at time " << carbonGetSimulationTime(model)
            << "\n";
  for (int i = 0; i < 4; ++i) {
    std::cout << "Value[" << i << "] = " << std::hex << val[i]
              << ", Drive[" << i << "] = " << drive[i]
              << "\n";
  }
}

int main()
{
  CarbonObjectID* hdl;
  CarbonTime time = 0;
  hdl = carbon_bidi1_128_create(eCarbonIODB, eCarbon_NoFlags);
  bool error = false;
#if 0
  CarbonWaveID* wave = carbonWaveInitVCD(hdl, "bidi1_128.vcd", e1ns);
  carbonDumpVars(wave, 0, "top");
#endif

  // Get access to the nets
  CarbonNetID* clkRef;
  CarbonNetID* dirRef;
  CarbonNetID* ioRef;
  error |= findNet(hdl, "top.clk", &clkRef) != eCarbon_OK;
  error |= findNet(hdl, "top.dir", &dirRef) != eCarbon_OK;
  error |= findNet(hdl, "top.io", &ioRef) != eCarbon_OK;

  // Create callbacks
  carbonAddNetValueChangeCB(hdl, outputCallback, NULL, ioRef);

  // Start by driving the bidi from the TB
  UInt32 drive[4] = {0, 0, 0, 0};
  UInt32 io[4] = {1, 0, 0, 0};
  UInt32 clk = 0;
  UInt32 dir = 0;
  error |= carbonDeposit(hdl, clkRef, &clk, 0) != eCarbon_OK;
  error |= carbonDeposit(hdl, dirRef, &dir, 0) != eCarbon_OK;
  error |= carbonDeposit(hdl, ioRef, io, drive) != eCarbon_OK;
  error |= carbonSchedule(hdl, time) != eCarbon_OK;
  error |= carbonExamine(hdl, ioRef, io, drive) != eCarbon_OK;
  if ((io[0] != 1) || (drive[0] != 0xffffffff)) {
    std::cout << std::dec
              << "Error: " << time << ": expecting io = (data: 1, drive: ffffffff), "
              << std::hex
              << "actual = (data: " << io[0] << ", drive: " << drive[0] << ")\n";
    error = true;
  }

  // Run a clock
  time += 5;
  clk = 1;
  error |= carbonDeposit(hdl, clkRef, &clk, 0) != eCarbon_OK;
  error |= carbonSchedule(hdl, time) != eCarbon_OK;
  error |= carbonExamine(hdl, ioRef, io, drive) != eCarbon_OK;
  if ((io[0] != 1) || (drive[0] != 0xffffffff)) {
    std::cout << std::dec
              << "Error: " << time << ": expecting io = (data: 1, drive: ffffffff), "
              << std::hex
              << "actual = (data: " << io[0] << ", drive: " << drive[0] << ")\n";
    error = true;
  }

  // Clock goes down, nothing changes
  time += 5;
  clk = 0;
  error |= carbonDeposit(hdl, clkRef, &clk, 0) != eCarbon_OK;
  error |= carbonSchedule(hdl, time) != eCarbon_OK;
  error |= carbonExamine(hdl, ioRef, io, drive) != eCarbon_OK;
  if ((io[0] != 1) || (drive[0] != 0xffffffff)) {
    std::cout << std::dec
              << "Error: " << time << ": expecting io = (data: 1, drive: ffffffff), "
              << std::hex
              << "actual = (data: " << io[0] << ", drive: " << drive[0] << ")\n";
    error = true;
  }

  // Change the direction. Call Carbon twice. In the first call both
  // Carbon and TB are driving and in the second call only Carbon is
  // driving
  time += 5;
  clk = 1;
  dir = 1;
  error |= carbonDeposit(hdl, clkRef, &clk, 0) != eCarbon_OK;
  error |= carbonDeposit(hdl, dirRef, &dir, 0) != eCarbon_OK;
  error |= carbonSchedule(hdl, time) != eCarbon_OK;
  error |= carbonExamine(hdl, ioRef, io, drive) != eCarbon_OK;
  if ((io[0] != 21) || (drive[0] != 0)) {
    std::cout << std::dec
              << "Error: " << time << ": expecting io = (data: 21, drive: 0), "
              << std::hex
              << "actual = (data: " << io[0] << ", drive: " << drive[0] << ")\n";
    error = true;
  }
  drive[0] = 0xffffffff;
  drive[1] = 0xffffffff;
  drive[2] = 0xffffffff;
  drive[3] = 0xffffffff;
  error |= carbonDeposit(hdl, ioRef, io, drive) != eCarbon_OK;
  error |= carbonSchedule(hdl, time) != eCarbon_OK;
  error |= carbonDeposit(hdl, ioRef, io, drive) != eCarbon_OK;
  error |= carbonSchedule(hdl, time) != eCarbon_OK;
  error |= carbonExamine(hdl, ioRef, io, drive) != eCarbon_OK;
  if ((io[0] != 21) || (drive[0] != 0)) {
    std::cout << std::dec
              << "Error: " << time << ": expecting io = (data: 21, drive: 0), "
              << std::hex
              << "actual = (data: " << io[0] << ", drive: " << drive[0] << ")\n";
    error = true;
  }

  // Clock goes down, nothing changes
  time += 5;
  clk = 0;
  error |= carbonDeposit(hdl, clkRef, &clk, 0) != eCarbon_OK;
  error |= carbonSchedule(hdl, time) != eCarbon_OK;
  error |= carbonExamine(hdl, ioRef, io, drive) != eCarbon_OK;
  if ((io[0] != 21) || (drive[0] != 0)) {
    std::cout << std::dec
              << "Error: " << time << ": expecting io = (data: 21, drive: 0), "
              << std::hex
              << "actual = (data: " << io[0] << ", drive: " << drive[0] << ")\n";
    error = true;
  }

  carbonDestroy(&hdl);
  return error;
} // int main
