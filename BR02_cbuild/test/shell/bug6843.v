module bug(clk, addr, we, din, dout);
   input clk;
   input [3:0] addr;
   input       we;
   input [31:4] din;
   output [31:4] dout;

   reg [31:4]    dout;
   reg [31:4]    mem[0:15];
   
   always @(posedge clk)
     if (we)
       mem[addr] <= din;
     else
       dout <= mem[addr];
endmodule
