
module dbreset(clk1, clk2, reset1, reset2, in1, in2, q1, q2);
   input clk1, clk2, reset1, reset2, in1, in2;
   output q1, q2;
   reg    q1, q2;

   always @(posedge clk1 or negedge reset1)
     begin
        if (! reset1)
          q1 <= 0;
        else
          q1 <= in1;
     end

   always @(negedge clk2 or posedge reset2)
     begin
        if (reset2)
          q2 <= 0;
        else
          q2 <= in2;
     end
   
endmodule

   