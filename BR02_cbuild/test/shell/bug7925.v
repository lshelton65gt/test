module test(clk, in, out1, out2, en);   // carbon disallowFlattening
   input clk;
   input [3:0] in;
   output [3:0] out1;  // carbon forceSignal
   output [3:0] out2;
   input        en;

   mypull pull10(.z(out1[0]));
   mypull pull11(.z(out1[1]));
   mypull pull12(.z(out1[2]));
   mypull pull13(.z(out1[3]));
   mypull pull20(.z(out2[0]));
   mypull pull21(.z(out2[1]));
   mypull pull22(.z(out2[2]));
   mypull pull23(.z(out2[3]));
   flop flop(.clk(clk), .in(in), .out(out2));
endmodule

module mypull(z);
   input z;
   pullup(z);
endmodule

module flop(clk, in, out);
   input clk;
   input [3:0] in;
   output [3:0] out;

   reg [3:0]    out;

   always @(posedge clk)
     out <= in;
   
endmodule
