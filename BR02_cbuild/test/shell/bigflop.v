
module bigflop(in1, in2, in3, in4, in5, out1, out2, out3, out4, out5, clk);
   input clk;
   
   input [499:0] in5;
   input [399:0] in4;
   input [299:0] in3;
   input [199:0] in2;
   input [99:0]  in1;

   output [499:0] out5;
   output [399:0] out4;
   output [299:0] out3;
   output [199:0] out2;
   output [99:0]  out1;

   reg [499:0]    out5;
   reg [399:0]    out4;
   reg [299:0]    out3;
   reg [199:0]    out2;
   reg [99:0]     out1;
   
   always @(posedge clk)
     begin
        out1 <= in1;
        out2 <= in2;
        out3 <= in3;
        out4 <= in4;
        out5 <= in5;
     end
endmodule // bigflop

