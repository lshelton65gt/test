#include "librand1.h"
#include "librand2.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <vector>
#include <string>
#include <iostream>

#include "util/OSWrapper.h"

// Entry point for calltree --toggle-collect=simulate --collect-state=no
extern "C" void simulate(CarbonObjectID* hdl, const CarbonTime& time) {
  carbonSchedule(hdl, time);
}

void do_design(CarbonObjectID* hdl, bool doPrint) {
  carbonChangeMsgSeverity(hdl, 5065, eCarbonMsgSuppress);
  CarbonTime time = 0;

  CarbonNetID * m_clk = carbonFindNet(hdl, "top.clk");
  assert(m_clk);
  CarbonNetID * m_out = carbonFindNet(hdl, "top.out");
  assert(m_out);

  std::vector<std::string> stuff;

  for (UInt64 i = 0; i < 64; i++)
  {
    carbonDepositWord(hdl, m_clk, i & 1, 0, 0);

    simulate(hdl, time);
    time += 100; 

    if (!doPrint) continue;
    const size_t strSize = 33;
    char s[strSize];
    carbonFormat(hdl, m_clk, s, strSize, eCarbonBin);
    stuff.push_back(std::string(s));
    fputs(s, stdout);
    carbonFormat(hdl, m_out, s, strSize, eCarbonBin);
    stuff.push_back(std::string(s));
    fputs(s, stdout);
    fputc('\n', stdout);
  }

  if (doPrint)
    std::cout << "Collected " << stuff.size() << " string" << std::endl;
  
} // void do_design

int main(int argc, char **argv)
{
  bool doPrint = true;
  int arg = 1;
  while (argc > arg) {
    if (strcmp(argv[arg], "-noprint") == 0) {
      doPrint = false;
      ++arg;
    }
  }
  CarbonObjectID *hdl1 = carbon_rand1_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonWaveID* hdl1Wave = carbonWaveInitFSDB(hdl1, 
                                              "rand1.hdl1.fsdb", 
                                              e1ns);
  carbonDumpVars(hdl1Wave, 0, "top");
  
  CarbonObjectID *hdl2 = carbon_rand2_create(eCarbonIODB, eCarbon_NoFlags);
  do_design(hdl1, doPrint);
  carbonDestroy(&hdl1);
  do_design(hdl2, doPrint);
  carbonDestroy(&hdl2);

  hdl1 = carbon_rand1_create(eCarbonFullDB, eCarbon_NoFlags);
  hdl1Wave = carbonWaveInitVCD(hdl1, 
                               "rand1.hdl1.dump.vcd", 
                               e1ns);
  carbonDumpVars(hdl1Wave, 0, "top");

  do_design(hdl1, false);
  carbonDestroy(&hdl1);

  return 0;
} // int main
