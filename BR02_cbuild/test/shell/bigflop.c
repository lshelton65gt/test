
#include "libbigflop.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

int main()
{
  CarbonObjectID* hdl;
  CarbonWaveID* wave;
  CarbonNetID* in1;
  CarbonNetID* in2;
  CarbonNetID* in3;
  CarbonNetID* in4;
  CarbonNetID* in5;
  CarbonNetID* out1;
  CarbonNetID* out2;
  CarbonNetID* out3;
  CarbonNetID* out4;
  CarbonNetID* out5;
  CarbonNetID* clk;

  int i, j;
  int stat = 0;
  CarbonTime simTime = 0;  

  UInt32 val500[16];
  UInt32 val400[13];
  UInt32 val300[10];
  UInt32 val200[7];
  UInt32 val100[4];
  UInt32 clkval;

  hdl = carbon_bigflop_create(eCarbonFullDB, eCarbon_NoFlags);
  assert(hdl);
  wave = carbonWaveInitFSDBAutoSwitch(hdl, "bigflop", e1ns, 2, 1);
  assert(wave == NULL);
  /* makes minimum size 2 megs when spec'd with 1 */
  wave = carbonWaveInitFSDBAutoSwitch(hdl, "bigflop", e1ns, 1, 2);
  assert(wave);
  assert(carbonDumpVars(wave, 0, "bigflop") == eCarbon_OK);
  
  
  in1 = carbonFindNet(hdl, "bigflop.in1");
  in2 = carbonFindNet(hdl, "bigflop.in2");
  in3 = carbonFindNet(hdl, "bigflop.in3");
  in4 = carbonFindNet(hdl, "bigflop.in4");
  in5 = carbonFindNet(hdl, "bigflop.in5");
  clk = carbonFindNet(hdl, "bigflop.clk");

  out1 = carbonFindNet(hdl, "bigflop.out1");
  out2 = carbonFindNet(hdl, "bigflop.out2");
  out3 = carbonFindNet(hdl, "bigflop.out3");
  out4 = carbonFindNet(hdl, "bigflop.out4");
  out5 = carbonFindNet(hdl, "bigflop.out5");

  assert(in1);
  assert(in2);
  assert(in3);
  assert(in4);
  assert(in5);

  assert(out1);
  assert(out2);
  assert(out3);
  assert(out4);
  assert(out5);

  memset(val500, 0, 16 * sizeof(UInt32));
  memset(val400, 0, 13 * sizeof(UInt32));
  memset(val300, 0, 10 * sizeof(UInt32));
  memset(val200, 0, 7 * sizeof(UInt32));
  memset(val100, 0, 4 * sizeof(UInt32));
  
  val100[2] = 1;
  val200[5] = 2;
  val300[8] = 4;
  val400[11] = 8;
  val500[14] = 16;
  clkval = 0;

  carbonDeposit(hdl, in1, val100, 0);
  carbonDeposit(hdl, in2, val200, 0);
  carbonDeposit(hdl, in3, val300, 0);
  carbonDeposit(hdl, in4, val400, 0);
  carbonDeposit(hdl, in5, val500, 0);

  for (i = 0; i < 4; ++i)
  {
    carbonDeposit(hdl, clk, &clkval, 0);
    carbonSchedule(hdl, simTime);
    simTime += 10;
    clkval = !clkval;
  }

  for (j = 0; j < 50000; ++j)
  {
    ++val100[0];
    ++val200[0];
    ++val300[0];
    ++val400[0];
    ++val500[0];

    carbonDeposit(hdl, in1, val100, 0);
    carbonDeposit(hdl, in2, val200, 0);
    carbonDeposit(hdl, in3, val300, 0);
    carbonDeposit(hdl, in4, val400, 0);
    carbonDeposit(hdl, in5, val500, 0);
    
    carbonDeposit(hdl, clk, &clkval, 0);
    carbonSchedule(hdl, simTime);
    simTime += 10;
    clkval = !clkval;
  }

  /* We switched once. Now, switch again, overwriting _0.fsdb */
  val100[1] = 1;
  val200[4] = 2;
  val300[7] = 4;
  val400[10] = 8;
  val500[13] = 16;

  for (j = 0; j < 50000; ++j)
  {
    carbonDeposit(hdl, in1, val100, 0);
    carbonDeposit(hdl, in2, val200, 0);
    carbonDeposit(hdl, in3, val300, 0);
    carbonDeposit(hdl, in4, val400, 0);
    carbonDeposit(hdl, in5, val500, 0);

    ++val100[0];
    ++val200[0];
    ++val300[0];
    ++val400[0];
    ++val500[0];

    
    carbonDeposit(hdl, clk, &clkval, 0);
    carbonSchedule(hdl, simTime);
    simTime += 10;
    clkval = !clkval;
  }
  
  carbonDestroy(&hdl);
  return stat;
}
