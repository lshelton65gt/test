module readmemRange(clk, addr, sel, we, din, dout);
   input clk;
   input [3:0] addr;
   input       sel, we;
   input [7:0] din;
   output [7:0] dout;

   reg [7:0]    dout;
   reg [7:0]    memLowToHigh [0:15];
   reg [7:0]    memHighToLow [15:0];

   always @(posedge clk)
     if (we) begin
        if (sel)
          memHighToLow[addr] <= din;
        else
          memLowToHigh[addr] <= din;
     end
     else begin
        if (sel)
          dout <= memHighToLow[addr];
        else
          dout <= memLowToHigh[addr];
     end

endmodule
