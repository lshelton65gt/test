module top(clk, in, out);
   input clk, in;
   output out;
   reg    out;

   always @(posedge clk)
     out <= in;
endmodule
