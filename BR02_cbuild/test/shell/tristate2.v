module top (out, clk, in1, in2, in3, en);
   inout [2:0] out;
   input  clk, in1, in2, in3, en;

   // Register some of the inputs
   reg    rin1, rin2, rin3;
   always @ (posedge clk)
     begin
        rin1 <= in1;
        rin2 <= in2;
        rin3 <= in3;
     end

   // First tristate
   assign out[0] = en ? rin1 : 1'bz;

   // Second tristate
   assign out[1] = ~en ? 1'bz : rin2;

   // Input tristate
   assign out[2] = en ? rin3 : 1'bz;

endmodule // top
