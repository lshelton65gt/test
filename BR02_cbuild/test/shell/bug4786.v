module U(OQrit, mHSz);
input OQrit;
output mHSz;

wire [1:13] OQrit;
reg [0:2] mHSz;

reg ILgk_;
reg [17:25] FdMJH;
reg [82:97] j;
reg [8:86] aRg;
wire [10:38] Hdi;
reg [4:5] iWRVv;

assign Hdi[21:30] = ~(FdMJH[19:21]);

always @(&OQrit[4:8])
  if (~OQrit[4:8])
    iWRVv[4:5] <= ~2'b11;

l Q ({aRg[30:53],iWRVv[4:5]}, Hdi[21:30]);

always @(&aRg[30:53])
  if (aRg[30:53])
    j[89:91] = iWRVv[4:5];

always @(ILgk_)
if (~ILgk_)
begin
case (j[89:91])
3'd5:
begin
mHSz[2] = {aRg[30:53],Hdi[10:26]};
end
endcase
end

endmodule


module l(L, Ao);
input L;
output Ao;

wire [5:98] L;
wire [20:29] Ao;

wire [93:115] S;

assign S[100:101] = ((~3'b111));

IJy x (Ao[22:27], {S[100:101],Ao[25]}, S[100:101]);

endmodule

module IJy(x, bcdtg, L);
output x;
output bcdtg;
output L;

wire [25:30] x;
reg [76:115] bcdtg;
reg [16:17] L;

wire [15:29] FS_hv;
wire jlI;

assign x[27:29] = FS_hv[18:26];

always @(posedge jlI)
  bcdtg[83:100] <= ~FS_hv[18:26];

assign FS_hv[20:29] = ~L[16:17];

always @(posedge jlI)
  if (jlI == 1'b0)
    L[17] = FS_hv[18:26];

endmodule
