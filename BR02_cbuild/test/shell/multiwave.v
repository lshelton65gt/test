module top(clk, in1a, in1b, in2a, in2b, out1, out2);
   input clk;
   input [7:0] in1a, in1b, in2a, in2b;
   output [7:0] out1, out2;

   sub1 sub1(.clk(clk), .ina(in1a), .inb(in1b), .out(out1));
   sub2 sub2(.clk(clk), .ina(in2a), .inb(in2b), .out(out2));
endmodule

module sub1(clk, ina, inb, out);
   input clk;
   input [7:0] ina, inb;
   output [7:0] out;

   reg [7:0]    out, ina_r, inb_r;

   always @(posedge clk) begin
      ina_r <= ina;
      inb_r <= inb;
      out <= ina_r & inb_r;
   end
endmodule

module sub2(clk, ina, inb, out);
   input clk;
   input [7:0] ina, inb;
   output [7:0] out;

   reg [7:0]    out, ina_r, inb_r;

   always @(posedge clk) begin
      ina_r <= ina;
      inb_r <= inb;
      out <= ina_r | inb_r;
   end
endmodule
