
module top(ck, d, wr, q2);
   input ck, wr;
   input [1:0] d;
   output [1:0] q2;   
   
   wire [1:0] q1;
   assign q1 = 2'b00;
      
   foo f1 (ck, d, wr, q1);
   bar b1 (ck, q1, q2);

endmodule // top

module foo(ck, d, wr, q);
   input ck, wr;
   input [1:0] d;
   output [1:0] q;   
   
   reg [1:0] 	q;
   
   integer cnt;
   initial cnt = 0;
   
   always @(posedge ck) begin
      if (wr) q <= d;      
   end
endmodule // foo


module bar(ck, d, q);
   input ck;
   input [1:0] d;
   output [1:0] q;
   reg [1:0] q;

   always @(posedge ck) begin
      q <= d;      
   end
endmodule // bar
