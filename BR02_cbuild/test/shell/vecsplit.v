module top(en,in,data,out, a, b, c, d);
   input en;
   input [1:0] in;
   input [3:0] data;
   output [1:0] out;
   input        a, b, c, d;
   
   //wire a,b,c,d,e,f;
   sub sub0 (a,b,c,d);
   sub sub1 (c,d,e,f);

   enabled en0(a,b,c,d,en,data);
   assign {e,f} = in;
   assign out = {a,b};

endmodule

module sub(a,b,c,d);
   inout a,b,c,d;
   assign {a,b,c,d} = {c,d,1'bz,1'bz};
endmodule

module enabled(a,b,c,d,en,data);
   inout a,b,c,d;
   input en;
   input [3:0] data;
   assign      {a,b,c,d} = en ? data : 4'bzzzz;
endmodule
