#include "libautodbmulti.h"
#include "carbon/carbon_dbapi.h"
#include <iostream>

int main()
{
  CarbonObjectID* obj1 = carbon_autodbmulti_create(eCarbonAutoDB, eCarbon_NoFlags);
  CarbonObjectID* obj2 = carbon_autodbmulti_create(eCarbonAutoDB, eCarbon_NoFlags);

  CarbonDB* db1 = carbonGetDB(obj1);
  CarbonDB* db2 = carbonGetDB(obj2);

  const char* type = carbonDBGetType(db1);
  std::cout << "model1: " << type << std::endl;
  type = carbonDBGetType(db2);
  std::cout << "model2: " << type << std::endl;

  // Simple sanity check to see if the model is working
  CarbonNetID* in1 = carbonFindNet(obj1, "autodbmulti.in");
  CarbonNetID* in2 = carbonFindNet(obj2, "autodbmulti.in");
  CarbonNetID* out1 = carbonFindNet(obj1, "autodbmulti.out");
  CarbonNetID* out2 = carbonFindNet(obj2, "autodbmulti.out");

  CarbonUInt32 val = 0x12345678;
  carbonDeposit(obj1, in1, &val, 0);
  val = 0x87654321;
  carbonDeposit(obj2, in2, &val, 0);
  carbonSchedule(obj1, 0);
  carbonSchedule(obj2, 0);

  carbonExamine(obj1, out1, &val, 0);
  std::cout << "out1 = 0x" << std::hex << val << std::endl;
  carbonExamine(obj2, out2, &val, 0);
  std::cout << "out2 = 0x" << std::hex << val << std::endl;

  carbonDestroy(&obj1);
  carbonDestroy(&obj2);

  return 0;
}
