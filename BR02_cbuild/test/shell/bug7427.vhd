-- This test creates a VHM with multiple I/O types and sizes as
-- well as testing whether sequential, combinational, and
-- asynchronous ouputs arrive on time.
--
library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity bug is
    port(   clk1         : in bit;
            clk2         : in bit;
            enum_a       : in boolean := false;
            enum_b       : in boolean := false;
            enum_out     : out boolean;
            int_a        : in integer := 0;
            int_b        : in integer := 0;
            int_out      : out integer;
            array_a      : in bit_vector(7 downto 0) := "00000000";
            array_b      : in bit_vector(7 downto 0) := "00000000";
            array_out    : out bit_vector(7 downto 0);
            count1       : in std_logic_vector(7 downto 0) := "00000000";
            count2       : out std_logic_vector(7 downto 0) := "00000000";
            count3       : in std_logic_vector(7 downto 0) := "00000000";
            count4       : out std_logic_vector(7 downto 0) := "00000000";
            count5       : in std_logic_vector(7 downto 0) := "00000000";
            count6       : out std_logic_vector(7 downto 0) := "00000000";
            count7       : out std_logic_vector(7 downto 0) := "00000000";
            count8       : out std_logic_vector(7 downto 0) := "00000000";
            combo1       : out std_logic;
            combo2       : out std_logic;
            combo3       : out std_logic;
            in1          : in std_logic;
            in2          : in std_logic;
            async1       : out std_logic;
            async2       : out std_logic;
            inu1         : in std_ulogic;
            inu2         : in std_ulogic;
            outu         : out std_ulogic;
            inuvec1      : in std_ulogic_vector(31 downto 0) := (others => '0');
            inuvec2      : in std_ulogic_vector(31 downto 0) := (others => '0');
            outuvec      : out std_ulogic_vector(31 downto 0);
            unsigned_in1 : in unsigned(7 downto 0);
            unsigned_in2 : in unsigned(7 downto 0);
            unsigned_out : out unsigned(7 downto 0);
            signed_in1   : in signed(7 downto 0);
            signed_in2   : in signed(7 downto 0);
            signed_out   : out signed(7 downto 0);
            natural_in1  : in natural;
            natural_in2  : in natural;
            natural_out  : out natural
        );
end;

architecture rtl of bug is
  signal count7_int : std_logic_vector(7 downto 0) := "00000000";
  signal count8_int : std_logic_vector(7 downto 0) := "00000000";
  
begin
  enum_out  <= enum_a and enum_b;
  int_out   <= int_a + int_b;
  array_out <= array_a and array_b;
  outu <= inu1 and inu2;
  outuvec <= inuvec1 xor inuvec2;
  unsigned_out <= unsigned_in1 + unsigned_in2;
  signed_out <= signed_in1 + signed_in2;
  natural_out <= natural_in1 + natural_in2;

  -- Add two flop cycles to make sure data flows correctly
  -- If we get called multiple times for the same cycle
  -- and different clocks change, we will get the wrong answer
  process (clk1)
  begin  -- process
    if clk1'event and clk1 = '1' then  -- rising clock edge
      count7_int <= count1;
      count8 <= count8_int;
    end if;
  end process;
  process (clk2)
  begin  -- process
    if clk2'event and clk2 = '1' then  -- rising clock edge
      count8_int <= count3;
      count7 <= count7_int;
    end if;
  end process;

  process (clk2)
  begin  -- process
    if clk2'event and clk2 = '1' then
      count2 <= count1;
      count6 <= count5;
    end if;
  end process;

  process (clk1)
  begin  -- process
    if clk1'event and clk1 = '1' then  -- rising clock edge
      count4 <= count3;
    end if;
  end process;

  process (count1)
  begin  -- process
    if count1 = "00000010" then
      combo1 <= '1';
    else
      combo1 <= '0';
    end if;
  end process;

  process (count3)
  begin  -- process
    if count3 = "00000100" then
      combo2 <= '0';
    else
      combo2 <= '1';
    end if;
  end process;

  process (count5)
  begin  -- process
    if count5 = "00000110" then
      combo3 <= '1';
    else
      combo3 <= '0';
    end if;
  end process;

  process (in1, in2)
  begin  -- process
    async1 <= in1 and in2;
    async2 <= in1 or in2;
  end process;
end;

