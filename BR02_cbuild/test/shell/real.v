`timescale 10ns/1ns

module mt46v64m4(clk, data);
  parameter tRC = 1.12345;
  input clk;
  output data;
  reg data;

  real realout;
  real RC_chk3;

   real crazy;
   
  initial begin
    RC_chk3 = 2.71828E-1;
    realout = $realtime;
  end

  always @(posedge clk)
  begin
    if (($realtime - RC_chk3) < tRC) begin
      $display( "%m : at time %t ERROR: tRC violation during Activate bank",
                $realtime);
    end
    else
    begin
    $display( "%m : at time %t", $realtime);
    realout = realout + 47.4317;
    end
    if (RC_chk3 < tRC) begin
      $display( "%m : at time %t Message 2",
                $realtime);
    end
    realout = 1.2 + realout + (1.1 + $realtime);
    $display( "%m : at time %t realout = %g",
                $realtime, realout);
  end

    always @(posedge clk )
     begin : outputassign
       if ( realout > 3.14159 )
         begin
           data <= 1'b1;
         end
       else
         begin
           data <= 1'b0;
         end
       end
endmodule
