#include "libbug4954.h"
#include <stdio.h>

int main()
{
    UInt32 val, drive;
    CarbonObjectID *obj = carbon_bug4954_create(eCarbonFullDB, eCarbon_NoFlags);
    CarbonNetID *out = carbonFindNet(obj, "bug.out");


    carbonSchedule(obj, 0);
    /*
      this works!
        carbonExamine(obj, out, &val, &drive);
      this doesn't
    */
    carbonExamineWord(obj, out, &val, 0, &drive);
    printf("out is %d\n", val);

    carbonDestroy(&obj);
    return 0;
}
