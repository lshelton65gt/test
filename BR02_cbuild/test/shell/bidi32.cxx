#include "libbidi32.h"
#include "shell/carbon_capi.h"
#include <iostream>

extern "C" {
extern CarbonStatus carbonDataSchedule(CarbonObjectID* model, CarbonTime time);
}

bool findNet(CarbonObjectID* hdl, const char* netName, CarbonNet** netRef)
{
  bool error = false;
  *netRef = carbonFindNet(hdl, netName);
  if (*netRef == NULL)
  {
    error = true;
    std::cerr << "Error: Failed to get a handle to " << netName << "\n";
  }
  return error;
}

void printNetValue(const char* str, CarbonObjectID* hdl, CarbonNetID* net,
                   UInt32* value, UInt32* drive)
{
  char  name[256];
  carbonGetNetName(hdl, net, name, 256);
  std::cout << str << " for " << name << "\n";
  int size = carbonGetNumUInt32s(net);
  for (int i = 0; i < size; ++i) {
    std::cout << "  data[" << i << "] = " << std::hex << value[i]
              << ", drive[" << std::dec << i << "] = " << std::hex << drive[i]
              <<"\n";
  }
}

void ioCallback(CarbonObjectID* hdl, CarbonNetID* net, void* data, UInt32* value,
                UInt32* drive)
{
  printNetValue("IO callback called", hdl, net, value, drive);

  // Set the flag to true
  bool* changed = (bool*)data;
  *changed = true;
}

int main()
{
  CarbonObjectID* hdl = carbon_bidi32_create(eCarbonFullDB, eCarbon_NoFlags);
  bool error = false;

  CarbonWave* wave = carbonWaveInitVCD(hdl, "bidi32.vcd", e1ns);
  carbonDumpVars(wave, 0, "myreg");
  carbonDumpOn(wave);

  // Get access to the pertinent nets
  CarbonNet* clkRef;
  CarbonNet* dir1Ref;
  CarbonNet* dir2Ref;
  CarbonNet* io1Ref;
  CarbonNet* io2Ref;
  error |= findNet(hdl, "myreg.clk", &clkRef);
  error |= findNet(hdl, "myreg.dir1", &dir1Ref);
  error |= findNet(hdl, "myreg.dir2", &dir2Ref);
  error |= findNet(hdl, "myreg.io1", &io1Ref);
  error |= findNet(hdl, "myreg.io2", &io2Ref);
  if (error) {
    return 1;
  }

  // Create a callback that sets a boolean on io1 changes
  bool changed = true;
  CarbonNetValueCBDataID* id = carbonAddNetValueChangeCB(hdl, ioCallback,
                                                         &changed, io1Ref);
  if (id == NULL) {
    std::cout << "Did not register callback on io1!\n";
  }
  id = carbonAddNetValueChangeCB(hdl, ioCallback, &changed, io2Ref);
  if (id == NULL) {
    std::cout << "Did not register callback on io2!\n";
  }

  // Set clock and the bidi enables to 0, Carbon never drives
  UInt32 zero = 0;
  UInt32 one = 1;
  carbonDeposit(hdl, clkRef, &zero, NULL);
  carbonDeposit(hdl, dir1Ref, &one, NULL);
  carbonDeposit(hdl, dir2Ref, &zero, NULL);

  // In a loop drive the io bus and call schedule
  UInt32 data[1] = {1};
  UInt32 drive1[1] = {0};
  UInt32 drive2[1] = {0xffffffff};
  UInt32 count = 10;
  while (changed && (count > 0)) {
    changed = false;
    carbonDeposit(hdl, io1Ref, data, drive1);
    printNetValue("Deposit", hdl, io1Ref, data, drive1);
    carbonDeposit(hdl, io2Ref, data, drive2);
    printNetValue("Deposit", hdl, io2Ref, data, drive2);
    carbonDataSchedule(hdl, 5);

    // Get the io1 ref value and print it
    UInt32 cdata[1], cdrive[1];
    carbonExamine(hdl, io1Ref, cdata, cdrive);
    printNetValue("Examine", hdl, io1Ref, cdata, cdrive);
    carbonExamine(hdl, io2Ref, cdata, cdrive);
    printNetValue("Examine", hdl, io2Ref, cdata, cdrive);
    --count;
  }

  // Close the model to close the files
  carbonDestroy(&hdl);

  if (error) {
    return 2;
  }
  return 0;
} // int main
