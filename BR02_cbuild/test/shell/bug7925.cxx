#include "libdesign.h"

int main()
{
  CarbonObjectID *obj = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonWaveID *vcd = carbonWaveInitVCD(obj, "test.vcd", e1ns);
  carbonDumpVars(vcd, 0, "test");
  carbonSchedule(obj, 0);
  carbonDestroy(&obj);
  return 0;
}
