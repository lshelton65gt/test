#include "libconstantSplit.h"
#include <stdio.h>

int main()
{
  CarbonObjectID* hdl;
  CarbonNetID* rst;
  CarbonNetID* forceMe;
  UInt32 rstVal;
  int stat = 0;

  hdl = carbon_constantSplit_create(eCarbonFullDB, eCarbon_NoFlags);
  if (!hdl)
  {
    fprintf(stderr, "Unable to create design\n");
    return 3;
  }

  rst = carbonFindNet(hdl, "test.rst");
  if (! rst)
  {
    fprintf(stderr, "Unable to find rst net\n");
    return 3;
  }

  forceMe = carbonFindNet(hdl, "test.forceMe");
  if (! forceMe)
  {
    fprintf(stderr, "Unable to find forceMe net\n");
    return 3;
  }

  /* init forceMce */
  if (carbonForceWord(hdl, forceMe, 0, 0) != eCarbon_OK)
  {
    fprintf(stderr, "Unable to force forceMe net\n");
    return 3;
  }

  /* try to force with a null value. It should return eCarbon_ERROR */
  if (carbonForce(hdl, rst, NULL) != eCarbon_ERROR)
  {
    fprintf(stderr, "force with null value didn't fail.\n");
    stat = 3;
  }

  if (carbonForceRange(hdl, rst, NULL, 0, 0) != eCarbon_ERROR)
  {
    fprintf(stderr, "forceRange with null value didn't fail.\n");
    stat = 3;
  }

  rstVal = 1;
  if (carbonForce(hdl, rst, &rstVal) != eCarbon_OK)
  {
    fprintf(stderr, "force on rst failed.\n");
    stat = 3;
  }

  carbonSchedule(hdl, 0);
  carbonDestroy(&hdl);
  return stat;
}
