// this is a version of mem.v that uses a generate block to select the style of
// memory to create, but all styles are identical, so the generated memory
// should be functionally identical to what you find in mem.v, only the names
// will be adjusted because of the generate block

module top (out, clk, wen, data, waddr, raddr);
   output [`MEM_WIDTH-1:0]    out;
   input 		      clk, wen;
   input [`MEM_WIDTH-1:0]     data;
   input [`MEM_ADDR_BITS-1:0] waddr, raddr;

   parameter 		      STYLE = 0;
   
   generate
   begin:gb0
      case (STYLE)
	1:
	  begin:style1
	     // This is the memory
	     reg [`MEM_WIDTH-1:0]       Z1_mem [`MEM_DEPTH-1:0];

	     // Write the memory
	     always @ (negedge clk)
	       if (wen)
		 Z1_mem[waddr] = data;

             // Read the memory
             assign  out = Z1_mem[raddr];
          end
	default:
	  begin   // has no name
	     // This is the memory
	     reg [`MEM_WIDTH-1:0]       Z2_mem [`MEM_DEPTH-1:0];

	     // Write the memory
	     always @ (negedge clk)
	       if (wen)
		 Z2_mem[waddr] = data;

             // Read the memory
             assign out = Z2_mem[raddr];
          end
      endcase
   end
   endgenerate
   
endmodule // top


