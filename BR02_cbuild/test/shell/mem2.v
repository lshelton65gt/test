/*
 Running in NC yields:

 for mem.dat.3:
 
Warning!  Too many data words in file "mem.dat" at line 2   [Verilog-TMDW]
          "mem2.v", 11: $readmemh("mem.dat", mem, 7);

Warning!  The contents of memory (mem) have been altered    [Verilog-TCMHA]
          "mem2.v", 11: $readmemh("mem.dat", mem, 7);
                  11 out = x, in = 4
                  31 out = x, in = 4
                  51 out = x, in = 5
                  71 out = x, in = 6
                  91 out = 5, in = 7

 for mem.dat.2:
 
Warning!  Address given in "mem.dat" at line 2 is out of
          bounds                                            [Verilog-ADROB]
          "mem2.v", 11: $readmemh("mem.dat", mem, 7);

Warning!  The contents of memory (mem) have been altered    [Verilog-TCMHA]
          "mem2.v", 11: $readmemh("mem.dat", mem, 7);
                  11 out = x, in = 4
                  31 out = x, in = 4
                  51 out = x, in = 5
                  71 out = x, in = 6
                  91 out = 0, in = 7

 for mem.dat.1:
  
Warning!  Address given in "mem.dat" at line 1 is out of
          bounds                                            [Verilog-ADROB]
          "mem2.v", 11: $readmemh("mem.dat", mem, 7);

Warning!  The contents of memory (mem) have not been
          altered                                           [Verilog-TCMHNA]
          "mem2.v", 11: $readmemh("mem.dat", mem, 7);
                  11 out = x, in = 4
                  31 out = x, in = 4
                  51 out = x, in = 5
                  71 out = x, in = 6
                  91 out = x, in = 7

 */
module top(in, clk, out);
   input [2:0] in;
   input clk;
   
   output [2:0] out;
   reg [2:0]    out;
   
   reg [2:0] mem [4:7];

   initial
     $readmemh("mem.dat", mem, 7);

   always @(posedge clk)
     out <= mem[in];
endmodule // top

`ifdef SIMULATE   
module carbon_test_driver;
   reg clk;
   reg [2:0] in;
   wire [2:0] out;

   top top(in, clk, out);
   
   always #10 clk = !clk;
   
   initial begin
      clk = 0;
      in = 4;

      repeat(2) @(negedge clk);

      in = 5;

      @(negedge clk);

      in = 6;

      @(negedge clk);
      
      in = 7;

      @(negedge clk);
      $finish;

   end // initial begin

   always @(posedge clk)
     #1 $display($time, " out = %h, in = %h", out, in);
   
endmodule // test
`endif