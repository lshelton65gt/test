
module flopreg(clk, a, b, c, d, e, f, q, bout, aout, eout, cout, fout);
  input clk;
  input [9:0] a;
  input b;
  input [31:0] c;
  input [33:0] d;
  input [3:0] e;
  input [71:0] f;
  output [33:0] q;
  output bout;
  output [9:0] aout;
  output [3:0] eout;
  output [31:0] cout;
  output [71:0] fout;
  reg [33:0] q;
  reg bout;
  reg [9:0] aout;
  reg [3:0] eout;
  reg [31:0] cout;
  reg [71:0] fout;

  always @(posedge clk)
  begin
    q = d;
    bout = b;
    aout = a;
    eout = e;
    cout = c;
    fout = f;
  end
endmodule
