#include "libbug7994.h"
#include <iostream>
#include <cassert>
#include <cstring>
#include <cstdlib>

// Test bug7994 and its fallout:
// 0.  No bug - generate gold FSDB
// 1.  Open FSDB, immediately switch
// 2.  Same as #1, but do dumpvars first
// 3.  Open VCD, immediately flush

int main(int argc, char** argv)
{
  std::string fsdbFilename("bug7994_");
  int test = -1;
  for (int i = 1; i < argc; ++i) {
    if ((strcmp(argv[i], "-test") == 0) && (++i < argc)) {
      test = atoi(argv[i]);
      fsdbFilename += argv[i];
    }
  }
  fsdbFilename += ".fsdb";

  if (test == -1) {
    std::cout << "Need to select a test" << std::endl;
    return 1;
  }


  CarbonObjectID *obj = carbon_bug7994_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonWaveID *wave;
  CarbonStatus stat;

  if (test < 3) {
    // Open an FSDB file and immediately switch it.  This triggers the bug.
    wave = carbonWaveInitFSDB(obj, fsdbFilename.c_str(), e1ns);
    if ((test == 0) || (test == 2)) {
      carbonDumpVars(wave, 0, "bug7994");
    }
    if (test != 0) {
      stat = carbonWaveSwitchFSDBFile(wave, fsdbFilename.c_str());
      assert(stat == eCarbon_OK);
    }
  }

  if (test == 3) {
    // Open a VCD file and immediately flush it.  This triggers the bug.
    wave = carbonWaveInitVCD(obj, "bug7994.vcd", e1ns);
    stat = carbonDumpFlush(wave);
    assert(stat == eCarbon_OK);
  }

  if ((test == 1) || (test == 3)) {
    carbonDumpVars(wave, 0, "bug7994");
  }

  // Simulate starting at a time other than 0.  This exposed a bug as well
  CarbonNetID *clk = carbonFindNet(obj, "bug7994.clk");
  CarbonUInt32 val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, 10);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, 20);

  carbonDestroy(&obj);

  return 0;
}
