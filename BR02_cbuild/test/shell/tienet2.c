
#include "libtienet2.h"
#include "carbon/carbon_dbapi.h"
#include <stdio.h>

int main()
{
  UInt32 val;
  CarbonObjectID* hdl = carbon_tienet2_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID* en = carbonFindNet(hdl, "top.en");

  /* DB API should agree that it is tied */
  CarbonDB* db = carbonGetDB(hdl);
  const CarbonDBNode* node = carbonNetGetDBNode(hdl, en);
  if (carbonDBIsTied(db, node) != 1) {
    fprintf(stderr, "top.en not reported as tied.\n");
    return 1;
  }

  val = 0;
  if (carbonDeposit(hdl, en, &val, 0) != eCarbon_ERROR)
  {
    fprintf(stderr, "top.en deposit did not fail as it should.\n");
    return 1;
  }
  
  carbonSchedule(hdl, 0);
  
  carbonExamine(hdl, en, &val, 0);
  if (val != 1)
  {
    fprintf(stderr, "tied net top.en was changed.\n");
    return 1;
  }
  
  carbonDestroy(&hdl);
  return 0;
}
