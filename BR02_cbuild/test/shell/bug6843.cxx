#include "carbon/carbon_dbapi.h"
#include <cassert>

int main()
{
  CarbonDB *db = carbonDBCreate("libbug6843.symtab.db", 0);
  const CarbonDBNode *mem = carbonDBFindNode(db, "bug.mem");
  assert(carbonDBGetMSB(db, mem) == 31);
  assert(carbonDBGetLSB(db, mem) == 4);
  carbonDBFree(db);
  return 0;
}
