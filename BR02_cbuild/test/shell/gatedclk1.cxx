#include "libgatedclk1.h"
#include "shell/carbon_capi.h"
#include <iostream>

bool findNet(CarbonObjectID* hdl, const char* netName, CarbonNet** netRef)
{
  bool error = false;
  *netRef = carbonFindNet(hdl, netName);
  if (*netRef == NULL)
  {
    error = true;
    std::cerr << "Error: Failed to get a handle to " << netName << "\n";
  }
  return error;
}

void printNetValue(const char* str, CarbonObjectID* hdl, CarbonNetID* net,
                   UInt32* value, UInt32* drive)
{
  char  name[256];
  carbonGetNetName(hdl, net, name, 256);
  std::cout << str << " for " << name << "\n";
  int size = carbonGetNumUInt32s(net);
  for (int i = 0; i < size; ++i) {
    std::cout << "  data[" << i << "] = " << std::hex << value[i]
              << ", drive[" << std::dec << i << "] = " << std::hex << drive[i]
              <<"\n";
  }
}

int main()
{
  CarbonObjectID* hdl = carbon_gatedclk1_create(eCarbonFullDB, eCarbon_NoFlags);
  bool error = false;

  CarbonWave* wave = carbonWaveInitVCD(hdl, "gatedclk1.vcd", e1ns);
  carbonDumpVars(wave, 0, "dut");
  carbonDumpOn(wave);

  CarbonUInt64 numSchedCalls = 0;
  
  // Get access to the pertinent nets
  CarbonNet* clkRef;
  CarbonNet* enRef;
  CarbonNet* gclkRef;
  error |= findNet(hdl, "dut.clk", &clkRef);
  error |= findNet(hdl, "dut.en", &enRef);
  error |= findNet(hdl, "dut.gclk", &gclkRef);
  if (error) {
    return 1;
  }

  // First cycle, everything is 0
  UInt32 zero = 0;
  UInt32 one = 1;
  CarbonTime time = 0;
  UInt32 data;
  UInt32 drive;
  carbonDeposit(hdl, clkRef, &zero, NULL);
  carbonDeposit(hdl, enRef, &zero, NULL);
  carbonExamine(hdl, gclkRef, &data, &drive);
  printNetValue("Pre-sim Examine", hdl, gclkRef, &data, &drive);
  carbonSchedule(hdl, time);
  ++numSchedCalls;

  carbonExamine(hdl, gclkRef, &data, &drive);
  printNetValue("Examine", hdl, gclkRef, &data, &drive);

  // Next cycle make both 1, to get a 1 gclk
  time += 10;
  carbonDeposit(hdl, clkRef, &one, NULL);
  carbonDeposit(hdl, enRef, &one, NULL);
  carbonSchedule(hdl, time);
  ++numSchedCalls;

  carbonExamine(hdl, gclkRef, &data, &drive);
  printNetValue("Examine", hdl, gclkRef, &data, &drive);

  // Next cycle make clk 0 so that gclk becomes 0
  time += 10;
  carbonDeposit(hdl, clkRef, &zero, NULL);
  carbonSchedule(hdl, time);
  ++numSchedCalls;

  carbonExamine(hdl, gclkRef, &data, &drive);
  printNetValue("Examine", hdl, gclkRef, &data, &drive);

  if (carbonGetTotalNumberOfScheduleCalls(hdl) != numSchedCalls)
  {
    std::cerr << "Number of schedule calls incorrect.";
    error = true;
  }

  // Close the model to close the files
  carbonDestroy(&hdl);
  
  if (error) {
    return 2;
  }
  return 0;
} // int main
