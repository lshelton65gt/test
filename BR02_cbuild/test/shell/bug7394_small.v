// Generated with: ./rangen -s 263 -n 0xfd24b8cc -o /w/xenon/rsayde/build/randbugs/obj/Linux.product/test/rangen/rangen/testcases.053107/cbld/assert6.v
// Using seed: 0xfd24b8cc
// Writing cyclic circuit of size 263 to file '/w/xenon/rsayde/build/randbugs/obj/Linux.product/test/rangen/rangen/testcases.053107/cbld/assert6.v'
// Module count = 10

// module tof
// size = 48 with 6 ports
module tof(vAX, CMEq, mh, HlpNk, rGR, Fr7eG);
   inout vAX;
   inout CMEq;
   input mh;
   input HlpNk;
   input rGR;
   input Fr7eG;

   wire [22:96] vAX;
   wire [2:6]   CMEq;
   wire         mh;
   wire         HlpNk;
   wire [12:32] rGR;
   wire [2:3]   Fr7eG;
   reg [0:9]    _sl3U;
   wire [4:9]   C;
   wire         O1;
   reg [23:28]  TCE;
   reg [5:6]    bI;
   wire         ExfZ;
   wire [21:27] DrFg;
   reg [39:113] VI;
   reg [2:4]    FR;
   reg [5:5]    H;
   reg [2:3]    kVSkt [14:18]; // memory
   reg [9:30]   K;
   reg          R6dO;
   reg [2:6]    D3ZD;
   reg [7:7]    y;
   wire [0:3]   tyoN;
   wire [0:6]   f;
   reg [2:2]    ne [4:6]; // memory
   reg [23:28]  FH1Mn;
   wire [21:22] eEA;
   reg [0:3]    F3;
   reg [1:6]    WtHr;
   reg          ptc;
   wire [31:119] qhV8;
   reg [55:110]  cZo8h;
   wire          UK6Da;
   reg [7:14]    GJ0Ic;
   wire          qQsOB;
   wire [2:8]    E69;
   reg           Q02;
   reg [10:13]   G2H;
   reg           irn;
   reg [113:116] lQ;
   reg [12:76]   Lj;
   reg [1:1]     Qus;
   reg [0:1]     KTpb4;

   assign qhV8[43:70] = ~((~(Lj[34:40]))) + (~2'b01);

   assign vAX[72:93] = (E69[4:7] != 4'b1110) ? 4'b0011 : 22'bz;

   wire   qK;

   Msl N (Qg, UK6Da, ptc, AluP, {y[7],Qg,bI[6],HlpNk,y[7],CMEq[3:5],Fr7eG[2:3],Qg,CMEq[2:6],qhV8[43:70],46'b1010001110100000110010010110101101110101101000}, CMEq[3:6], {Qg,ptc,bI[6],UK6Da,1'b0}, qQsOB, qQsOB, {tyoN[0:1],ExfZ,f[0:6],E69[3:6],CMEq[2:6],O1}, E69[3:6]);

   wire   AR;

   assign E69[5:6] = (~WtHr[1:3]) + (~qhV8[43:70]^AR);

   wire   G;
   wire   lzi;
   reg    VUOh;
   wire   BFW2;

   always @(G or lzi) begin
      if (G) begin
         VUOh = lzi; // enable assign
      end
   end

   assign BFW2 = ~G & VUOh; // gated clock

   always @(posedge BFW2)
     begin
        begin
           Lj[19:55] = ~(Lj[19:55]);
        end
     end

   always @(&WtHr[1:3])
     if (~WtHr[1:3])
       begin
          Lj[19:55] = vAX[79:86];
       end
     else
       begin
       end

endmodule

// module Msl
// size = 157 with 11 ports
module Msl(t, jmcv, xCct, tG86, t7Pj, Q, Rt, N, A3lrT, MTly6, k);
   input t;
   input jmcv;
   input xCct;
   input tG86;
   input t7Pj;
   inout Q;
   input Rt;
   output N;
   output A3lrT;
   output MTly6;
   inout  k;

   wire   t;
   wire   jmcv;
   wire   xCct;
   wire   tG86;
   wire [19:108] t7Pj;
   wire [21:24]  Q;
   wire [2:6]    Rt;
   reg           N;
   reg           A3lrT;
   reg [3:22]    MTly6;
   wire [1:4]    k;
   reg [4:16]    Uh5;
   reg           R1UjD;
   wire          OSLr;
   reg           GfM;
   reg [26:29]   dU;
   reg [14:29]   NOG;
   wire [4:7]    H;
   reg [12:32]   W;
   reg [5:9]     WX_ZS;
   reg [0:0]     iau [45:51]; // memory
   reg [10:14]   YmN4 [27:119]; // memory
   reg [27:60]   QWhs;
   reg [12:21]   E;
   reg [3:6]     s;
   reg           LWXQ;
   reg           QU;
   reg [5:5]     o2ZXl;
   wire [1:5]    jG;
   reg [6:24]    axlx8;
   reg           cLKRQ;
   reg           oKq;
   reg [2:28]    Cg;
   reg [0:0]     xJtg [1:7]; // memory
   reg [10:29]   qDUR;
   reg [0:3]     cn;
   wire [0:4]    U6;
   reg [110:125] z;
   reg           yc;
   reg           Dz;
   reg [31:70]   HJ [2:4]; // memory
   reg [18:23]   pnD;
   wire          ieTtk;
   wire [5:6]    X;
   wire [7:7]    B;
   reg [3:5]     cCpd;
   reg [6:11]    KNfL;
   reg [2:10]    nAp;
   reg [1:30]    e;
   reg [4:5]     E0W0l;
   reg [6:15]    Yv;
   reg [12:27]   tBYj7;
   wire [10:28]  QXtA7;
   reg [1:1]     EpG;
   reg           MFPJt;
   reg [5:11]    M;
   wire          aWjM;
   reg [3:6]     N8GL3;
   reg [1:7]     A;
   reg [0:0]     nC [1:6]; // memory
   reg           FIo;
   reg           hxeZ1K;
   reg [6:10]    BQ;
   reg [5:15]    bgHQA;
   reg [16:31]   Xl;
   reg [5:5]     _;
   wire          ZgoN;
   wire          ROZxU;
   reg [0:2]     o;
   reg [3:5]     Tu;
   wire          c7Tg3;
   reg           pJhxJ;
   reg [0:3]     Uak [19:125]; // memory
   wire [6:6]    qz;
   reg           ig;
   wire          kX;
   reg [82:115]  lB;
   reg [0:0]     K [2:16]; // memory
   reg [58:96]   FY;
   wire [6:18]   yfR;
   reg [0:31]    r;
   reg [0:0]     O78Y0;
   reg [1:1]     u;
   reg           rbrcXq;
   reg [0:2]     LS0m_;
   reg           oKb7j;
   reg [14:26]   P;
   reg [2:4]     tsdyL;
   reg [29:125]  Pe6rd;
   wire [0:5]    IAG;
   reg [1:4]     PCBlu;
   reg           TFH;
   reg           BUYj;
   reg [5:28]    SZvVu;
   reg [2:10]    nr43;
   reg [24:28]   hOids;
   reg           MxS1q;
   reg [5:6]     eG1;
   reg           bVao;
   reg [5:26]    O;
   reg [1:13]    lXRB;
   wire [12:27]  Y6N_a;
   reg [0:1]     at;
   wire [3:19]   i;
   reg           TQ;
   reg [2:5]     OT;
   reg [2:4]     UL_P3;
   reg [3:7]     lQSL;
   reg [0:4]     b1;
   reg [5:28]    ut;
   reg [16:22]   An;
   reg [4:4]     eC76;
   reg [5:29]    kO0;
   reg [0:22]    XM;
   reg           FChTv;
   reg           OKXI;
   reg [9:12]    Y7Y;
   reg           ka;
   reg [3:3]     Z;
   reg           BK28S;
   wire [21:30]  oM;
   reg           Ri82;
   reg [7:15]    Q7C;
   reg [10:14]   f9;
   reg           q6Lz;

   wire          Y_c;

   always @(posedge &b1[1:4])
     begin
        begin
           eC76[4] = ~An[16:19]|Y_c;
        end
     end

   wire D0ktRk;
   wire nmn;
   reg  TCg;
   wire OtQ;

   assign D0ktRk = UL_P3[3]; // clock

   assign   nmn = MTly6[5:9]; // enable data
   
   always @(D0ktRk or nmn) begin
      if (~D0ktRk) begin
         TCg = nmn; // enable assign
      end
   end

   assign OtQ = D0ktRk & TCg; // gated clock
   
   always @(negedge OtQ)
     begin
        begin
           BUYj = ~4'b1000;
        end
     end

   wire zq6;

   always @(&MTly6[5:9])
     if (MTly6[5:9])
       begin
          if (BUYj == 1'b1)
            begin
               { o[0:1], b1[1:3], YmN4[57]/* memuse */, nr43[5:6], A[3:5] } = UL_P3[3]^zq6;
            end
          else
            begin
            end
          if (O[13:23] == 11'b10110000010)
            begin
            end
       end
     else
       begin
          { oKq, kO0[9:26], qDUR[13:21], nr43[5:6] } = BUYj;
       end

   always @(&UL_P3[2:3])
     if (~UL_P3[2:3])
       begin
          { rbrcXq, N, axlx8[13:19], o2ZXl[5], FY[76:93], nC[1]/* memuse */, UL_P3[2] } = 3'b000;
       end

   wire   YygrQj;
   wire   P_;
   reg    kPk1d;

   assign   YygrQj = N; // clock

   wire Oo;

   assign Oo = YygrQj & kPk1d; // gated clock

   wire F9FQ;

   assign k[2:4] = ~(~nr43[5:6]);

   always @(posedge kO0[14])
     begin
        begin
           Xl[20:23] = {nmn,F9FQ};
        end
     end

   always @(negedge eC76[4])
     begin
        begin
           KNfL[6:7] = ~Oo;
        end
     end

   wire S0 = &KNfL[6:7];
   always @(posedge &Xl[20:23] or negedge S0)
     if (~S0)
       begin :svkY
          reg [0:2] vi;
          begin
          end
       end
     else
       begin
          begin
             { M[7:8], kO0[24:27], A[3:5], nr43[5:6] } = ~(Yv[9:15]&Cg[11:15]);
          end
       end

endmodule

