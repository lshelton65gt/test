// Testing new ranged readmem functions
#include "libreadmemRange.h"
#include <iostream>

void doReadMemAndPrint(CarbonObjectID* obj, CarbonMemoryID* mem, const char* file,
                       CarbonSInt64 startAddr, CarbonSInt64 endAddr, CarbonRadix radix)
{
  const CarbonSInt64 cLoAddr = 0;
  const CarbonSInt64 cHighAddr = 15;

  char memName[100];
  carbonGetMemName(obj, mem, memName, 100);

  std::cout << "Reading memory " << memName << ", range " << std::dec << startAddr << "-" << endAddr << " from file " << file << std::endl;
  
  if (radix == eCarbonHex) {
    carbonReadmemhRange(mem, file, startAddr, endAddr);
  } else {
    carbonReadmembRange(mem, file, startAddr, endAddr);
  }

  CarbonUInt32 val;
  for (CarbonSInt64 addr = cLoAddr; addr <= cHighAddr; ++addr) {
    carbonExamineMemory(mem, addr, &val);
    std::cout << "  data[" << std::dec << addr << "] = 0x" << std::hex << val << std::endl;
  }
  std::cout << std::endl;
}

int main()
{
  CarbonObjectID* obj = carbon_readmemRange_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonMemoryID* memLowToHigh = carbonFindMemory(obj, "readmemRange.memLowToHigh");
  CarbonMemoryID* memHighToLow = carbonFindMemory(obj, "readmemRange.memHighToLow");

  // Read both memories with a complete hex file, with a range from low to high
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex1.dat", 3, 6, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex1.dat", 3, 6, eCarbonHex);

  // Read both memories with a complete binary file, with a range from low to high
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin1.dat", 3, 6, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin1.dat", 3, 6, eCarbonBin);

  // Read both memories with a complete hex file, with a range from high to low
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex2.dat", 6, 3, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex2.dat", 6, 3, eCarbonHex);

  // Read both memories with a complete binary file, with a range from high to low
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin2.dat", 6, 3, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin2.dat", 6, 3, eCarbonBin);

  // Read both memories with a hex file containing too few entries, with a range from low to high
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex3.dat", 3, 6, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex3.dat", 3, 6, eCarbonHex);

  // Read both memories with a binary file containing too few entries, with a range from low to high
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin3.dat", 3, 6, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin3.dat", 3, 6, eCarbonBin);

  // Read both memories with a hex file containing too few entries, with a range from high to low
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex4.dat", 6, 3, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex4.dat", 6, 3, eCarbonHex);

  // Read both memories with a binary file containing too few entries, with a range from high to low
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin4.dat", 6, 3, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin4.dat", 6, 3, eCarbonBin);

  // Read both memories with a hex file containing too many entries, with a range from low to high
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex5.dat", 3, 6, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex5.dat", 3, 6, eCarbonHex);

  // Read both memories with a binary file containing too many entries, with a range from low to high
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin5.dat", 3, 6, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin5.dat", 3, 6, eCarbonBin);

  // Read both memories with a hex file containing too many entries, with a range from high to low
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex6.dat", 6, 3, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex6.dat", 6, 3, eCarbonHex);

  // Read both memories with a binary file containing too many entries, with a range from high to low
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin6.dat", 6, 3, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin6.dat", 6, 3, eCarbonBin);

  // Read both memories with a hex file containing an in-range address specifier, with a range from low to high
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex7.dat", 3, 6, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex7.dat", 3, 6, eCarbonHex);

  // Read both memories with a binary file containing an in-range address specifier, with a range from low to high
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin7.dat", 3, 6, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin7.dat", 3, 6, eCarbonBin);

  // Read both memories with a hex file containing an in-range address specifier, with a range from high to low
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex8.dat", 6, 3, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex8.dat", 6, 3, eCarbonHex);

  // Read both memories with a binary file containing an in-range address specifier, with a range from high to low
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin8.dat", 6, 3, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin8.dat", 6, 3, eCarbonBin);

  // Read both memories with a hex file containing an out-of-range address specifier, with a range from low to high
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex9.dat", 3, 6, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex9.dat", 3, 6, eCarbonHex);

  // Read both memories with a binary file containing an out-of-range address specifier, with a range from low to high
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin9.dat", 3, 6, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin9.dat", 3, 6, eCarbonBin);

  // Read both memories with a hex file containing an out-of-range address specifier, with a range from high to low
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex10.dat", 6, 3, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex10.dat", 6, 3, eCarbonHex);

  // Read both memories with a binary file containing an out-of-range address specifier, with a range from high to low
  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin10.dat", 6, 3, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin10.dat", 6, 3, eCarbonBin);

  // Do the out-range-cases again, but demote the alert to a warning.
  carbonChangeMsgSeverity(obj, 5024, eCarbonMsgWarning);

  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex9.dat", 3, 6, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex9.dat", 3, 6, eCarbonHex);

  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin9.dat", 3, 6, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin9.dat", 3, 6, eCarbonBin);

  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_hex10.dat", 6, 3, eCarbonHex);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_hex10.dat", 6, 3, eCarbonHex);

  doReadMemAndPrint(obj, memLowToHigh, "readmemRange_bin10.dat", 6, 3, eCarbonBin);
  doReadMemAndPrint(obj, memHighToLow, "readmemRange_bin10.dat", 6, 3, eCarbonBin);

  carbonDestroy(&obj);
  return 0;
}
