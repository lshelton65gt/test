module top (out, clk, wen, data, waddr, raddr);
   output [`MEM_WIDTH-1:0]    out;
   input 		      clk, wen;
   input [`MEM_WIDTH-1:0]     data;
   input [`MEM_ADDR_BITS:0]   waddr, raddr;

   // These are the memories
   reg [`MEM_WIDTH-1:0]        mem1 [`MEM_DEPTH+`MEM_DEPTH-1:`MEM_DEPTH];
   reg [`MEM_WIDTH-1:0]        mem2 [`MEM_DEPTH:`MEM_DEPTH+`MEM_DEPTH-1];

   // Write the first memory and read the second (creates a cycle)
   reg [`MEM_WIDTH-1:0]        rdata1, rdata2;
   always @ (posedge clk)
     begin
	if (wen)
	  mem1[waddr] = data;
	rdata2 = mem2[raddr];
     end
   
   // Write the second memory and read the first
   always @ (posedge clk)
     begin
	if (wen)
	  mem2[waddr] = data;
	rdata1 = mem1[raddr];
     end
   
   // Send the memory to our caller (the two reads should be identical)
   assign 		       out = rdata1 & rdata2;

endmodule // top

		  
