#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <assert.h>
#include <cstring>
using namespace std;

#define carbon_design_create  MODEL(create)

struct DesignInterface
{

  DesignInterface() : 
    m_wen(NULL),
    m_waddr(NULL),
    m_clk(NULL),
    m_raddr(NULL),
    m_out(NULL),
    m_data(NULL),
    mHdl(NULL),
    time(0)
  {}

  void initNets(CarbonObjectID* hdl)
  {
    mHdl = hdl;
    m_wen = carbonFindNet(mHdl, "top.wen");
    m_waddr = carbonFindNet(mHdl, "top.waddr");
    m_clk = carbonFindNet(mHdl, "top.clk");
    m_raddr = carbonFindNet(mHdl, "top.raddr");
    m_out = carbonFindNet(mHdl, "top.out");
    m_data = carbonFindNet(mHdl, "top.data");

    assert(m_wen && m_waddr && m_clk && m_raddr && m_out && m_data);
  }

  CarbonNetID* m_wen;
  CarbonNetID* m_waddr;
  CarbonNetID* m_clk;
  CarbonNetID* m_raddr;
  CarbonNetID* m_out;
  CarbonNetID* m_data;

  CarbonObjectID* mHdl;
  CarbonTime time;
};

template <typename T, typename V> void
printError(const char* str, int index, int wordIndex, T got, V exp)
{
  cerr << "Error: " << str << " top.gb0.genblk1.Z2_mem[" << index << "][" << wordIndex;
  cerr << "] = " << got << "; should be " << exp << ".\n";
}

#if 0
void
printError(const char* str, int index, int wordIndex, UInt64 got, UInt64 exp)
{
  cerr << "Error: " << str << " top.gb0.genblk1.Z2_mem[" << index << "][" << wordIndex;
  cerr << "] = " << got << "; should be " << exp << ".\n";
}
#endif

int
readSimulation(const char* str, CarbonObjectID* hdl, DesignInterface& ci,
	       UInt32 offset, int dataSize)
{
  int status = 0;

  UInt32 scalarVal = 0;
  carbonDeposit(hdl, ci.m_wen, &scalarVal, 0);
  carbonDeposit(hdl, ci.m_waddr, &scalarVal, 0);
  for (UInt32 i = 0; i < MEM_DEPTH; ++i)
  {
    // Run a cycle
    scalarVal = 0;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    carbonDeposit(hdl, ci.m_raddr, &i, 0);
    carbonSchedule(hdl, ci.time);
    ci.time += 10;
    
    scalarVal = 1;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    carbonSchedule(hdl, ci.time);
    ci.time += 10;

#if (((MEM_WIDTH + 31)/32) == 2)
    UInt32 exVal = 0;
    carbonExamineWord(hdl, ci.m_out, &exVal, 0, 0);

    if (exVal != (i + offset))
    {
      printError(str, i, 0, exVal, i + offset);
      status = 1;
    }

    carbonExamineWord(hdl, ci.m_out, &exVal, 1, 0);

    if (exVal != (i + offset + 1))
    {
      printError(str, i, 0, exVal, i + offset + 1);
      status = 1;
    }
#else
    for (int j = 0; j < dataSize; ++j)
    {
      UInt32 exVal = 0;
      carbonExamineWord(hdl, ci.m_out, &exVal, j, 0);
      if (exVal != (i + j + offset))
      {
	printError(str, i, j, exVal, i + j + 1);
        status = 1;
      }
    }
#endif
  } // for
  return status;
}

void
writeSimulation(CarbonObjectID* hdl, DesignInterface& ci, UInt32 offset,
		int dataSize)
{
  UInt32 scalarVal = 1;
  carbonDeposit(hdl, ci.m_wen, &scalarVal, 0);
  scalarVal = 0;
  carbonDeposit(hdl, ci.m_raddr, &scalarVal, 0);

  for (UInt32 i = 0; i < MEM_DEPTH; ++i)
  {
    // Run a cycle
    scalarVal = 0;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    carbonDeposit(hdl, ci.m_waddr, &i, 0);

#if (((MEM_WIDTH + 31)/32) == 2)
    scalarVal = i + offset;
    carbonDepositWord(hdl, ci.m_data, scalarVal, 0, 0);
    scalarVal = i + offset + 1;
    carbonDepositWord(hdl, ci.m_data, scalarVal, 1, 0);
#else
    for (UInt32 j = 0; j < dataSize; ++j)
      carbonDepositWord(hdl, ci.m_data, i + j + offset, j, 0);
#endif
    carbonSchedule(hdl, ci.time);
    ci.time += 10;
    scalarVal = 1;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    carbonSchedule(hdl, ci.time);
    ci.time += 10;
  }
  scalarVal = 0;
  carbonDeposit(hdl, ci.m_wen, &scalarVal, 0);
} // writeSimulation


void
checkSubrangeBounds(CarbonMemoryID* mem_ref)
{
  UInt32 mem_ref_val[1];
  mem_ref_val[0] = 0x0;
  assert(mem_ref);
  assert(carbonExamineMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH, 0) == eCarbon_ERROR);

  assert(carbonExamineMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH - 1, -1) == eCarbon_ERROR);

  assert(carbonExamineMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH, -1) == eCarbon_ERROR);
  assert(carbonExamineMemoryRange(mem_ref, -1, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);

  assert(carbonExamineMemoryRange(mem_ref, MEM_DEPTH, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);

  assert(carbonDepositMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH, 0) == eCarbon_ERROR);
  assert(carbonDepositMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH - 1, -1) == eCarbon_ERROR);
  assert(carbonDepositMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH, -1) == eCarbon_ERROR);
  assert(carbonDepositMemoryRange(mem_ref, -1, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);
  assert(carbonDepositMemoryRange(mem_ref, MEM_DEPTH, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);
}


int
main()
{
  int status = 0;
  DesignInterface ci;
  ci.time = 10;
  CarbonObjectID* hdl = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonObjectID* hdlCheck = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);

  ci.initNets(hdl);
  
  CarbonStatus cstatus;

  // Create a vcd file
  ostringstream stream;
  stream << "mem" << MEM_DEPTH << "_" << MEM_WIDTH << "vcd.dump";
  CarbonWaveID* wave = carbonWaveInitVCD(hdl, stream.str().c_str(), e1ns);
  if (! wave)
  {
    cerr << "dumpInit failed" << endl;
    return 1;
  }

  if (carbonDumpVars(wave, 0, "top") != eCarbon_OK)
  {
    cerr << "dumpvars failed" << endl;
    return 1;
  }
  
  // Get a handle to the memory to do all our work, here we use a name
  // that is a combination of names defined in gen_mem.v 'top.gb0' and
  // a name created by carbon for the unnamed block 'genblk1'
  CarbonMemoryID* mref = carbonFindMemory(hdl, "top.gb0.genblk1.Z2_mem");
  if (mref == NULL)
  {
    cerr << "Getting handle for top.gb0.genblk1.Z2_mem failed\n";
    return 1;
  }
  
  checkSubrangeBounds(mref);

  // Allocate memory to do API I/O
  int dataSize = carbonMemoryRowNumUInt32s(mref);
  assert(dataSize == (MEM_WIDTH + 31)/32);
  UInt32* data = new UInt32[dataSize];
  UInt32* dataCheck = new UInt32[dataSize];
  
  int i, j;

  // Write through the API
  for (i = 0; i < MEM_DEPTH; ++i)
  {
    for (j = 0; j < dataSize; ++j)
      data[j] = i + j + 1;
    
    cstatus = carbonDepositMemory(mref, i, data);
    if (cstatus != eCarbon_OK)
    {
      status = 1;
      cerr << "Failed to write top.gb0.genblk1.Z2_mem[" << i << "]\n";
    }
  }
  // save the simulation and check that the memory is restored
  ostringstream saveFile;
  saveFile << SInt32(MEM_DEPTH) << "_" << SInt32(MEM_WIDTH) << "_save.chkpt";
  assert(carbonSave(hdl, saveFile.str().c_str()) == eCarbon_OK);
  assert(carbonRestore(hdlCheck, saveFile.str().c_str()) == eCarbon_OK);

  CarbonMemoryID* mrefCheck = carbonFindMemory(hdlCheck, "top.gb0.genblk1.Z2_mem");
  if (mrefCheck == NULL)
  {
    cerr << "Getting handle for top.gb0.genblk1.Z2_mem for hdlCheck failed\n";
    return 1;
  }

  carbonDumpAddressRange(mref,"mref.top.gb0.genblk1.Z2_mem" , MEM_DEPTH - 1, 0, eCarbonHex);
  carbonDumpAddressRange(mrefCheck,"mrefCheck.top.gb0.genblk1.Z2_mem" , MEM_DEPTH - 1, 0, eCarbonHex);

  for (i = 0; i < MEM_DEPTH; ++i)
  {
    for (j = 0; j < dataSize; ++j)
      data[j] = i + j + 1;
    
    cstatus = carbonExamineMemory(mrefCheck, i, dataCheck);
    if (cstatus != eCarbon_OK)
    {
      status = 1;
      cerr << "Failed to examine top.gb0.genblk1.Z2_mem[" << i << "]\n";
    }
    else if (memcmp(data, dataCheck, dataSize * sizeof *dataCheck) != 0)
    {
      status = 1;
      cerr << "Restore failed. Address " << i << " incorrect. data:" << endl;
      for (j = 0; j < dataSize; ++j)
        cerr << "     data[" << j << "] should be: " << data[j] << " is: " << dataCheck[j] << endl;

    }
  }
  
  {
    char memBuf[MEM_WIDTH + 1];
    carbonFormatMemory(mrefCheck, memBuf, MEM_WIDTH + 1, eCarbonBin, 0);
    if (memBuf[MEM_WIDTH - 1] != '1')
    {
      status = 1;
      cerr << "carbonFormatMemory failed." << endl;
    }
  }
  // Read and write a range.
  UInt32 orig_range_data[1];
  UInt32 range_data[1];
  
  orig_range_data[0] = 0x00000000;
  if (carbonExamineMemoryRange(mref, 0, orig_range_data, 15, 0) != eCarbon_OK)
  {
    status = 1;
    cerr << "Failed to read top.gb0.genblk1.Z2_mem[0]\n";
  }
  cout << orig_range_data[0] << endl;
  range_data[0] = ~ orig_range_data[0];
  if (carbonDepositMemoryRange(mref, 0, range_data, 15, 0) != eCarbon_OK)
  {
    status = 1;
    cerr << "Failed to write top.gb0.genblk1.Z2_mem[0]\n";
  }
  range_data[0] = 0x00000000;
  if (carbonExamineMemoryRange(mref, 0, range_data, 15, 0) != eCarbon_OK)
  {
    status = 1;
    cerr << "Failed to read top.gb0.genblk1.Z2_mem[0]\n";
  }
  cout << range_data[0] << endl;
  if (carbonDepositMemoryRange(mref, 0, orig_range_data, 15, 0) != eCarbon_OK)
  {
    status = 1;
    cerr << "Failed to write top.gb0.genblk1.Z2_mem[0]\n";
  }
  range_data[0] = 0x00000000;
  if (carbonExamineMemoryRange(mref, 0, range_data, 15, 0) != eCarbon_OK)
  {
    status = 1;
    cerr << "Failed to read top.gb0.genblk1.Z2_mem[0]\n";
  }
  cout << range_data[0] << endl;
  

  // Make sure data is 0 so that vcd dumping is consistent
  for (int j = 0; j < dataSize; ++j)
    carbonDepositWord(hdl, ci.m_data, 0, j, 0);

  // Read through simulation
  status |= readSimulation("API/Sim mismatch", hdl, ci, 1, dataSize);
  
  // Write through simulation
  writeSimulation(hdl, ci, 3, dataSize);

  // Read through the API
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    cstatus = carbonExamineMemory(mref, i, data);
    if (cstatus != eCarbon_OK)
    {
      status = 1;
      cerr << "Failed to read from top.gb0.genblk1.Z2_mem[" << i << "]\n";
    }
    else
    {
      for (int j = 0; j < dataSize; ++j)
	if (data[j] != (UInt32) (i + j + 3))
	{
	  printError("Sim/API mismatch", i, j, data[j], (i + j + 3));
	  status = 1;
	}
    }
  }

  // Write through the API using the word routines
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    for (int j = 0; j < dataSize; ++j)
    {
      cstatus = carbonDepositMemoryWord(mref, i, i + j + 33, j);
      if (cstatus != eCarbon_OK)
      {
	status = 1;
	cerr << "Failed to write top.gb0.genblk1.Z2_mem[" << i << "][" << j << "]\n";
      }
    }
  }
  
  
  // Read out a bit, change it, then replace it to orig.



  // Read through simulation again
  status |= readSimulation("Word API/Sim mismatch", hdl, ci, 33, dataSize);
  
  // Write through the simulation again
  writeSimulation(hdl, ci, 0x123400, dataSize);

  // Read from the word API
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    for (int j = 0; j < dataSize; ++j)
    {
      data[0] = carbonExamineMemoryWord(mref, i, j);
      if (data[0] != (unsigned)(i + j + 0x123400))
      {
        printError("Sim/API mismatch", i, j, data[0], (i + j + 0x123400));
        status = 1;
      }
    }
  }

  ostringstream memFilePrefix;
  memFilePrefix << "gen_mem" << MEM_DEPTH << "x" << MEM_WIDTH;
  ostringstream memFile;

  memFile << memFilePrefix.str() << "_hex";
  carbonDumpAddressRange(mref, memFile.str().c_str(), MEM_DEPTH - 1, 0, eCarbonHex);
  memFile.str("");
  memFile << memFilePrefix.str() << "_bin";
  carbonDumpAddressRange(mref, memFile.str().c_str(), MEM_DEPTH - 1, 0, eCarbonBin);
  memFile.str("");
  memFile << memFilePrefix.str() << "_oct";
  carbonDumpAddressRange(mref, memFile.str().c_str(), MEM_DEPTH - 1, 0, eCarbonOct);
  memFile.str("");
  memFile << memFilePrefix.str() << "_dec";
  carbonDumpAddressRange(mref, memFile.str().c_str(), MEM_DEPTH - 1, 0, eCarbonDec);
  memFile.str("");
  memFile << memFilePrefix.str() << "_udec";
  carbonDumpAddressRange(mref, memFile.str().c_str(), MEM_DEPTH - 1, 0, eCarbonUDec);

  // Shutdown and return the results
  carbonDumpFlush(wave);
  carbonFreeMemoryHandle(hdl, &mref);
  assert(mref == NULL);
  carbonDestroy(&hdl);
  carbonDestroy(&hdlCheck);
  delete [] data;
  delete [] dataCheck;
  return status;
} // main
