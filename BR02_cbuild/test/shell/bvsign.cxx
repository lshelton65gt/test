#include "libbvsign.h"
#include <cassert>
#include <iostream>

int main()
{
  CarbonObjectID* hdl;
  CarbonNetID* in;
  CarbonNetID* in2;
  CarbonNetID* en;
  CarbonNetID* out;

  CarbonUInt32 val[3];
  val[0] = 1;
  val[1] = 0;
  val[2] = 0;

  CarbonUInt32 wordVal = 0;

  hdl = carbon_bvsign_create(eCarbonFullDB, eCarbon_NoFlags);
  assert(hdl);

  // non-tristate first
  in = carbonFindNet(hdl, "bvsign.in");
  in2 = carbonFindNet(hdl, "bvsign.in2");
  out = carbonFindNet(hdl, "bvsign.out");

  assert(in);
  assert(out);
  assert(in2);

  assert(! carbonIsTristate(out));
  assert(! carbonIsTristate(in2));

  carbonDeposit(hdl, in, val, 0);
  carbonSchedule(hdl, 0);
  carbonExamine(hdl, out, val, 0);
  carbonExamineWord(hdl, out, &wordVal, 2, 0);
  
  assert(val[0] == 0xffffffff);
  assert(val[1] == 0xffffffff);
  assert(val[2] == 0x3f);
  assert(wordVal == 0x3f);

  // now non-tristate input
  val[2] = 0xffffffff;
  carbonDeposit(hdl, in2, val, 0);
  carbonExamine(hdl, in2, val, 0);
  carbonExamineWord(hdl, in2, &wordVal, 2, 0);

  assert(val[0] == 0xffffffff);
  assert(val[1] == 0xffffffff);
  assert(val[2] == 0x3f);
  assert(wordVal == 0x3f);
  
  // now tristate

  // non-input
#ifndef BUG6983
  CarbonNetID* justout = carbonFindNet(hdl, "bvsign.justout");
  assert(justout);

  val[0] = 0xffffffff;
  val[1] = 0xffffffff;
  val[2] = 0xffffffff;
  wordVal = 0xffffffff;

  assert(carbonIsTristate(justout));
  
  carbonExamine(hdl, justout, val, 0);
  carbonExamineWord(hdl, justout, &wordVal, 2, 0);
  assert(val[0] == 0xffffffff);
  assert(val[1] == 0xffffffff);
  assert(val[2] == 0x3f);
  assert(wordVal == 0x3f);
  
  // bidirect
  CarbonNetID* io = carbonFindNet(hdl, "bvsign.io");
  val[0] = 0xffffffff;
  val[1] = 0xffffffff;
  val[2] = 0xffffffff;
  wordVal = 0xffffffff;

  assert(carbonIsTristate(io));
  
  carbonExamine(hdl, io, val, 0);
  carbonExamineWord(hdl, io, &wordVal, 2, 0);

  assert(val[0] == 0xffffffff);
  assert(val[1] == 0xffffffff);
  assert(val[2] == 0x3f);
  assert(wordVal == 0x3f);
#endif

  carbonDestroy(&hdl);
  return 0;
}
