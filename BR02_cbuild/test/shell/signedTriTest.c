
#include "libsignedTriTest.h"
#include <stdio.h>

int main()
{
  CarbonNetID* a;
  CarbonNetID* wide66;
  CarbonNetID* wide35;
  CarbonNetID* wide17;
  CarbonNetID* wide9;
  CarbonNetID* wide3;

  CarbonNetID* out1;
  CarbonNetID* out2;
  CarbonNetID* out3;
  CarbonNetID* out4;
  CarbonNetID* out5;

  CarbonUInt32 aV[1];
  CarbonUInt32 wide66V[3];
  CarbonUInt32 wide35V[2];
  CarbonUInt32 wide17V[1];
  CarbonUInt32 wide9V[1];
  CarbonUInt32 wide3V[1];
  CarbonUInt32 out1Val, out2Val, out3Val, out4Val, out5Val;
  
  CarbonObjectID* obj;
  CarbonTime simTime = 0;
  int stat = 0;

  obj = carbon_signedTriTest_create(eCarbonFullDB, eCarbon_NoFlags);
  if (! obj)
    return 1;

  a = carbonFindNet(obj, "top.a");
  wide66 = carbonFindNet(obj, "top.wide66");
  wide35 = carbonFindNet(obj, "top.wide35");
  wide17 = carbonFindNet(obj, "top.wide17");
  wide9 = carbonFindNet(obj, "top.wide9");
  wide3 = carbonFindNet(obj, "top.wide3");
  out1 = carbonFindNet(obj, "top.out1");
  out2 = carbonFindNet(obj, "top.out2");
  out3 = carbonFindNet(obj, "top.out3");
  out4 = carbonFindNet(obj, "top.out4");
  out5 = carbonFindNet(obj, "top.out5");

  aV[0] = 0;
  wide66V[2] = 0;
  wide66V[1] = 0;
  wide66V[0] = 0;
  wide35V[1] = 0;
  wide35V[0] = 0;
  wide17V[0] = 0;
  wide9V[0] = 0;
  wide3V[0] = 0;
  
  aV[0] = 1;
  wide66V[2] = 2;
  wide66V[0] = 0xffff;
  wide35V[1] = 1 << 2;
  wide35V[0] = 1;
  wide17V[0] = 0x100ff;
  wide9V[0] = 0x10f;
  wide3V[0] = 5;

  carbonDeposit(obj, a, aV, 0);
  carbonDeposit(obj, wide66, wide66V, 0);
  carbonDeposit(obj, wide35, wide35V, 0);
  carbonDeposit(obj, wide17, wide17V, 0);
  carbonDeposit(obj, wide9, wide9V, 0);
  carbonDeposit(obj, wide3, wide3V, 0);

  carbonSchedule(obj, simTime);
  simTime += 10;
  carbonSchedule(obj, simTime);
  simTime += 10;

  carbonExamine(obj, out1, &out1Val, 0);
  carbonExamine(obj, out2, &out2Val, 0);
  carbonExamine(obj, out3, &out3Val, 0);
  carbonExamine(obj, out4, &out4Val, 0);
  carbonExamine(obj, out5, &out5Val, 0);

  printf("out1 = %d\n", out1Val);
  printf("out2 = %d\n", out2Val);
  printf("out3 = %d\n", out3Val);
  printf("out4 = %d\n", out4Val);
  printf("out5 = %d\n", out5Val);
  
  if ((out1Val != 1) || (out2Val != 1) ||
      (out3Val != 1) || (out4Val != 1) ||
      (out5Val != 1))
    stat = 1;

  carbonDeposit(obj, wide66, 0, wide66V);
  carbonDeposit(obj, wide35, 0, wide35V);
  carbonDeposit(obj, wide17, 0, wide17V);
  carbonDeposit(obj, wide9, 0, wide9V);
  carbonDeposit(obj, wide3, 0, wide3V);

  carbonSchedule(obj, simTime);
  simTime += 10;
  carbonSchedule(obj, simTime);

  printf("out1 = %d\n", out1Val);
  printf("out2 = %d\n", out2Val);
  printf("out3 = %d\n", out3Val);
  printf("out4 = %d\n", out4Val);
  printf("out5 = %d\n", out5Val);
  
  if ((out1Val != 1) || (out2Val != 1) ||
      (out3Val != 1) || (out4Val != 1) ||
      (out5Val != 1))
    stat = 1;
  
  carbonDestroy(&obj);
  return stat;
}
