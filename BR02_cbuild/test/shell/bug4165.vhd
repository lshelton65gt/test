library IEEE;
use IEEE.std_logic_1164.all;
                                                                               
entity system is
    port (
        clk        : in std_logic;
        rst        : in std_logic;
        parity_out : buffer std_logic;
        carry_out  : buffer std_logic;
        borrow_out : buffer std_logic;
        count_out  : buffer std_logic_vector(8 downto 0));
end system;
                                                                               
                                                                               
architecture rtl of system is

   component updowncnt
      port (
          clk        : in std_logic;
          up         : in std_logic;
          down       : in std_logic;
          data_in    : in std_logic_vector(8 downto 0);
          parity_out : buffer std_logic;
          carry_out  : buffer std_logic;
          borrow_out : buffer std_logic;
          count_out  : buffer std_logic_vector(8 downto 0));
   end component;

   component updowncnt_stim
       port (
           clk  : in std_logic;
           rst  : in std_logic;
           up   : buffer std_logic;
           down : buffer std_logic;
           load : buffer std_logic_vector(8 downto 0));
   end component;

   signal load  : std_logic_vector(8 downto 0);
   signal up    : std_logic;
   signal down  : std_logic;
                                                                               
begin
   
   updowncnt_inst1: updowncnt
     port map (
        clk		=> clk, 
        up		=> up, 
        down		=> down, 
        data_in	        => load,
        parity_out	=> parity_out, 
        carry_out	=> carry_out,
        borrow_out	=> borrow_out, 
        count_out	=> count_out);

   updowncnt_stim_inst1: updowncnt_stim 
     port map (
        clk		=> clk, 
        rst		=> rst, 
        load		=> load, 
        up		=> up, 
        down		=> down);
 
end rtl; 
--
-- This module controls the up/down counter
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
                                                                                
entity updowncnt_stim is
    port (
        clk  : in     std_logic;
        rst  : in     std_logic;
        up   : buffer std_logic;
        down : buffer std_logic;
        load : buffer std_logic_vector(8 downto 0));
end updowncnt_stim;

architecture rtl of updowncnt_stim is

    signal cycles : std_logic_vector(1 downto 0);
    -- dir == 1 for up, 0 for down
    signal dir : std_logic;

begin
   
   load <= "000011111";

   up_down_process: process (rst, dir)
   begin
      if (rst = '1' or rst = 'H') then
          up <= '0';
          down <= '0';
      else
          up <= dir;
          down <= NOT(dir);
      end if;
   end process up_down_process ;

   cycles_process: process (clk, rst)
   begin
      if (rst = '1' or rst = 'H') then
         dir <= '0';
  	 cycles <= "00";
      elsif rising_edge(clk) then
  	 cycles <= cycles + 1;
         if (cycles = "00") then
             dir <= NOT(dir);
         end if;
      end if;
   end process cycles_process ;

end rtl;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity updowncnt is
    port (
        clk        : in std_logic;
        up         : in std_logic;
        down       : in std_logic;
        data_in    : in std_logic_vector(8 downto 0);
        parity_out : buffer std_logic;
        carry_out  : buffer std_logic;
        borrow_out : buffer std_logic;
        count_out  : buffer std_logic_vector(8 downto 0));
end updowncnt;
                                                                                      
architecture rtl of updowncnt is
  component parity
    port (
      bus_in:     in     std_logic_vector(8 downto 0);
      parity_out: buffer std_logic );
  end component;

  signal count  : std_logic_vector(8 downto 0);
begin

  updowncnt_p1 : PROCESS (clk)
     variable count_nxt : std_logic_vector(8 downto 0);
     variable cnt_up    : std_logic_vector(9 downto 0);
     variable cnt_dn    : std_logic_vector(9 downto 0);
     variable load      : std_logic;
     variable updown    : std_logic_vector(1 downto 0);
  begin
    if (clk'event and clk = '1') then
       cnt_dn := std_logic_vector(unsigned(count_nxt) - 5);
       cnt_up := std_logic_vector(unsigned(count_nxt) + 3);
       load := '1';
       updown := up & down;
       case (updown) is
         when "00" => count_nxt := data_in;
         when "01" => count_nxt := cnt_dn(8 downto 0);
         when "10" => count_nxt := cnt_up(8 downto 0);
         when "11" => load := '0';
         when others => null;
       end case;
   
       if (load = '1') then
         carry_out <= up and cnt_up(9);
         borrow_out <= down and cnt_dn(9);
         count <= count_nxt;
       end if;
    end if;
  end process updowncnt_p1;

  count_out <= count;


  parity_inst1 : parity
    port map (
      bus_in     => count_out,
      parity_out => parity_out);

end rtl;
library ieee;
use ieee.std_logic_1164.all;  
use ieee.numeric_std.all;    
use ieee.numeric_extra.all;

entity parity is
    port (
      bus_in     : in     std_logic_vector(8 downto 0);
      parity_out : buffer std_logic);
end parity;

architecture comb_logic of parity is
begin

  parity_out <= xor_reduce(bus_in);

end comb_logic;
