
module top(a, b, c);
   input [7:0] a;
   output [7:0] c;
   
   output [9:1] b;

   assign       c = a;
endmodule // top
