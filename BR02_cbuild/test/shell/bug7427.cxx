#include "libdesign.h"
#include <iostream>

extern "C" CarbonStatus carbonClkSchedule(CarbonObjectID* model, CarbonTime time);
extern "C" CarbonStatus carbonDataSchedule(CarbonObjectID* model, CarbonTime time);

static void val_change_cb(CarbonObjectID *obj, CarbonNetID *net, CarbonClientData data, CarbonUInt32 *val, CarbonUInt32*)
{
  CarbonTime t = *(reinterpret_cast<CarbonTime*>(data));
  char name[100];
  carbonGetNetName(obj, net, name, 100);
  std::cout << "(callback) " << name << " = " << std::hex << *val << " at time " << std::dec << t << std::endl;
}

static void print(CarbonObjectID *obj, CarbonNetID *net, const CarbonTime &t)
{
  char name[100];
  CarbonUInt32 val;
  carbonGetNetName(obj, net, name, 100);
  carbonExamine(obj, net, &val, 0);
  
  std::cout << "(examine) " << name << " = " << std::hex << val << " at time " << std::dec << t << std::endl;
}

int main()
{
  CarbonObjectID *obj = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk1 = carbonFindNet(obj, "bug.clk1");
  CarbonNetID *count1 = carbonFindNet(obj, "bug.count1");
  CarbonNetID *array_a = carbonFindNet(obj, "bug.array_a");
  CarbonNetID *array_b = carbonFindNet(obj, "bug.array_b");
  CarbonNetID *array_out = carbonFindNet(obj, "bug.array_out");

  CarbonTime t = 0;
  CarbonUInt32 val, drive;

  // Initialize the inputs.
  val = 0x66;
  drive = 0;
  carbonDeposit(obj, array_a, &val, &drive);
  val = 0xcc;
  carbonDeposit(obj, array_b, &val, &drive);
  val = 0;
  carbonDeposit(obj, clk1, &val, &drive);

  // Register the callbacks and print the current output values
  carbonAddNetValueChangeCB(obj, val_change_cb, &t, array_out);
  print(obj, array_out, t);
  carbonAddNetValueChangeCB(obj, val_change_cb, &t, count1);
  print(obj, count1, t);

  // Deposit to the clock and run the clock schedule
  val = 0;
  carbonDeposit(obj, clk1, &val, &drive);
  carbonClkSchedule(obj, t);
  
  // Deposit to the data inputs and run the data schedule
  // This should trigger a callback
  val = 0x66;
  carbonDeposit(obj, array_a, &val, &drive);
  val = 0xcc;
  carbonDeposit(obj, array_b, &val, &drive);
  carbonDataSchedule(obj, t);

  carbonDestroy(&obj);
  return 0;
}
