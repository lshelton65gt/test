#include <iostream>
#include "libbug6587.h"

int main() 
{
    
  const UInt32 databits = 44;
  const UInt32 memsize = 32; // 2**5
  /*
  ** Create the Carbon Object
  */
  CarbonObjectID* mem1rw = carbon_bug6587_create(eCarbonFullDB, eCarbon_NoFlags);

  /* 
  ** Get CarbonNets for each of the inputs
  */
  CarbonNetID* clk     = carbonFindNet(mem1rw, "ram1rw.clk");
  CarbonNetID* adr     = carbonFindNet(mem1rw, "ram1rw.adr");
  CarbonNetID* din     = carbonFindNet(mem1rw, "ram1rw.din");
  CarbonNetID* wen     = carbonFindNet(mem1rw, "ram1rw.wen");
  
  /*
  ** Get the CarbonNet for one of the outputs
  */
  CarbonNetID* dout = carbonFindNet(mem1rw, "ram1rw.dout");
  
  /*
  ** Open up a dump file and turn on dumping
  */
  CarbonWaveID* dump = carbonWaveInitVCD(mem1rw, "dump.vcd", e1ns);
  carbonDumpVars(dump, 0, "ram1rw");

  /*
  ** initialize memory 
  */
  CarbonMemoryID* memcore = carbonFindMemory(mem1rw, "ram1rw.mem_process.memcore");
  if (carbonReadmemh(memcore, "bug6587.dat") != eCarbon_OK ) {
	  std::cerr << "Unable to readmemh bug6587.dat!" << std::endl;
  }

  /*
  ** Initial inputs
  */
  
  UInt32 zero = 0;
  UInt32 one = 1;
  UInt32 loadData [2];
  loadData[0] = 0; 
  loadData[1] = 0; 

  UInt32 loadAdr  = 0x00;
  UInt32 readData [2];
  readData[0] = 0; 
  readData[1] = 0; 

  CarbonTime time = 0;


  carbonDeposit(mem1rw, clk,  &zero, 0);
  carbonDeposit(mem1rw, wen,  &one, 0);
  carbonDeposit(mem1rw, din,  loadData, 0);
  carbonDeposit(mem1rw, adr,  &loadAdr, 0);
  carbonSchedule(mem1rw, time);

  /*
  ** Load the mem1rw
  ** By advancing one clock
  */


  time +=  5;
  carbonDeposit(mem1rw, clk, &one, 0);
  carbonSchedule(mem1rw, time);

  /*
   * read memory
  */
  std::cout << std::endl;
  std::cout << "reading memory initial values" << std::endl;
  for(loadAdr = 0; loadAdr < memsize; loadAdr++) {
    time +=  5;
    carbonDeposit(mem1rw, adr,  &loadAdr, 0);
    carbonDeposit(mem1rw, clk,      &zero, 0);
    carbonSchedule(mem1rw, time);
    time +=  5;
    carbonDeposit(mem1rw, clk,      &one, 0);
    carbonSchedule(mem1rw, time);
    carbonExamine(mem1rw, dout, readData, 0);
    std::cout << "Time: " << time << " adr: " << loadAdr << " din: " << *loadData << " dout: " << *readData << " wen: 1" << std::endl ;
  }
  
  /*
   * write memory
  */
  std::cout << std::endl;
  std::cout << "writing to memory" << std::endl;
  carbonDeposit(mem1rw, wen,  &zero, 0);
  for(loadAdr = 0; loadAdr < memsize; loadAdr++) {
    *loadData += 1;
    time +=  5;
    carbonDeposit(mem1rw, adr,  &loadAdr, 0);
    carbonDeposit(mem1rw, din,  loadData, 0);
    carbonDeposit(mem1rw, clk,      &zero, 0);
    carbonSchedule(mem1rw, time);
    time +=  5;
    carbonDeposit(mem1rw, clk,      &one, 0);
    carbonSchedule(mem1rw, time);
    carbonExamine(mem1rw, dout, readData, 0);
    std::cout << "Time: " << time << " adr: " << loadAdr << " din: " << *loadData << " dout: " << *readData << " wen: 0" << std::endl;
  }
 /*
  * Read memory
 */ 

  std::cout << std::endl;
  std::cout << "writing out memory contents" << std::endl;
  for(SInt64 s_adr = 0; s_adr < memsize; s_adr++) {
    carbonExamineMemory(memcore, s_adr, readData);
    std::cout << " mem adr: " << s_adr << " mem data: " << *readData <<  std::endl;
  }

  /* 
  ** Clean up
  */
  carbonDestroy(&mem1rw);


  return 0;
}
