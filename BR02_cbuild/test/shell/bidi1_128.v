`define SIZE 128

module top(out, io, clk, dir);
   output [`SIZE-1:0] out;
   inout [`SIZE-1:0]  io;
   input              clk;
   input              dir;

   reg                en;
   reg [`SIZE-1:0]    out;
   always @ (posedge clk)
     begin
        out <= io;
        en <= dir;
     end

   assign io = en ? (out + 20) : `SIZE'bz;

endmodule

