#include "libbug12984.h"

int main()
{
  CarbonObjectID* obj = carbon_bug12984_create(eCarbonIODB, eCarbon_NoFlags);
  CarbonWaveID* fsdb = carbonWaveInitFSDB(obj, "bug12984.fsdb", e1ns);
  carbonDumpVars(fsdb, 0, "top");

  CarbonUInt32 val;
  CarbonNetID* clk = carbonFindNet(obj, "top.clk");
  CarbonNetID* rst = carbonFindNet(obj, "top.rst");
  CarbonNetID* in = carbonFindNet(obj, "top.in");
  CarbonTime t = 0;

  val = 1;
  carbonDeposit(obj, in, &val, 0);
  carbonDeposit(obj, rst, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  val = 0;
  carbonDeposit(obj, rst, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonDestroy(&obj);
  return 0;
}
