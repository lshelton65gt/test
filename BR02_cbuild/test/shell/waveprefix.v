// This test makes sure we can add wave prefixes to nested hierarchy

module top(out1, out2, clk, en, in1, in2);
   output [7:0] out1, out2;
   input        clk, en;
   input [7:0]  in1, in2;

   flop F1 (out1, clk, en, in1);
   flop F2 (out2, clk, en, in2);

endmodule

module flop (q, clk, en, d);
   output [7:0] q;
   input        clk, en;
   input [7:0]  d;

   wire         gclk;
   clk CLK (gclk, clk, en);

   reg [7:0]    q;
   initial q = 0;
   always @ (posedge gclk)
     q <= d;

endmodule

module clk(gclk, clk, en);
   output gclk;
   input  clk, en;

   reg    lat_en;
   always @ (clk or en)
     if (~clk)
       lat_en <= en;
   assign gclk = clk & lat_en;

endmodule
