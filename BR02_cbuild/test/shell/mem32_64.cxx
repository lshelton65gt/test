#include "libmem32_64.h"
#define MODEL(N) carbon_mem32_64_ ## N
#define MEM_DEPTH	32
#define MEM_WIDTH	64
#define MEM_ADDR_BITS	5
#include "mem.cxx"
