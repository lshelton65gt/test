
module bvsign(in, in2, out, out2, en
`ifndef BUG6983
              ,io
              ,justout
`endif
              );
   input [31:0] in;
   input        signed [69:0] in2;
   input        en;
   
   output       signed [69:0] out;

   output       signed [69:0] out2;
`ifndef BUG6983
   inout        signed[69:0] io;
   output       signed[69:0] justout;
`endif
   
   assign       out = 0 - in;

`ifndef BUG6983
   assign       io = en ? 0 - in : 70'bz;
   assign       justout =  en ? 0 - in : 70'bz;
`endif
   assign       out2 = in2;
   
endmodule // bvsign
