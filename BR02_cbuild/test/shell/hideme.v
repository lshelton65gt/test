
/*
 Tests the hidemodule directive. The module hidethis and anything under should not be seen in a vcd file nor accessible from the api.
 */
module top(in, clk, out);
   input in;
   input clk;
   output out;

   // not acccessible
   hidethis mama(in, clk, out);
   // accessible
   nuther papa(in, clk, out);
endmodule // top

module nuther(in, clk, out);
   input in;
   input clk;
   output out;

   wire   myIn;
   assign myIn = ~in;

   // not accessible
   hidethis b(myIn, clk, out);
   
endmodule
   
module hidethis(in, clk, out);
   input in;
   input clk;
   output out;

   myreg reg1 (in, clk, out);
endmodule // hidethis

module myreg(in, clk, out);
   input in;
   input clk;
   output out;
   reg    out;
   wire   dead;

   always @(posedge clk)
     out = in || dead;
endmodule // myreg
