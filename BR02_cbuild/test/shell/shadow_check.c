
#include "libshadow_check.h"

static void netCB(CarbonObjectID* model, CarbonNetID* net, CarbonClientData data, UInt32* nada1, UInt32* nada2)
{
  int* i = (int*)(data);
  ++(*i);
}

int main()
{
  CarbonObjectID* hdl;
  CarbonNetID* eref;
  int numClkCBs;

  numClkCBs = 0;

  hdl = carbon_shadow_check_create(eCarbonFullDB, eCarbon_NoFlags);
  if (! hdl)
    return 1;
  
  eref = carbonFindNet(hdl, "flopreg.e");
  
  if (! eref)
    return 1;

  carbonAddNetValueChangeCB(hdl,
                            &netCB,
                            &numClkCBs, 
                            eref);
  
  
  carbonFreeHandle(hdl, &eref);
  carbonDestroy(&hdl);
  return 0;
}

