`timescale 1ps/1ps

`ifndef   AW
  `define AW 8
`endif
`ifndef   DW
  `define DW 64
`endif

module top(clk, rst, din, dout, web, adr);

input clk,rst,web;
input  [`DW-1:0] din;
input  [`AW-1:0] adr;
output [`DW-1:0] dout;

mem1 mem (
        .DO(dout) ,
        .DI(din) ,
        .A(adr) ,
        .WEB(web) ,
        .CSB(1'b0) ,
        .BE(clk) ,
        .TBE(1'b0) ,
        .TEST(1'b0) ,
        .BUB(1'b0)
);

endmodule

`ifndef   AW
  `define AW 8
`endif
`ifndef   DW
  `define DW 64
`endif

module mem1 (
        DO ,
        DI ,
        A ,
        WEB ,
        CSB ,
        BE ,
        TBE ,
        TEST ,
        BUB
);

input  [`DW-1:0] DI ;
input  [`AW-1:0] A ;
input  WEB ;
input  CSB ;
input  BE ;
input  TBE ;
input  TEST ;
input  BUB ;
output [`DW-1:0] DO ;

reg    [`DW-1:0] DO ;
reg    [`DW-1:0] memory [0:((1<<(`AW-1))-1)] ; // carbon observeSignal
                                               // carbon depositSignal

always @( posedge BE )
  if ( (CSB == 1'b0) && (WEB == 1'b0) )
      DO  <= DI ;
  else if ( CSB == 1'b0 )
      DO  <= memory[ A ] ;
  else
      DO  <= DO ;

always @( posedge BE )
  if ( (CSB == 1'b0) && (WEB == 1'b0) )
      memory [ A ]  <= DI ;

endmodule

