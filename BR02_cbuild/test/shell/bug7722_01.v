// This test case forces the creation of a vectorised port that is subsequently split by port splitting.
// The original scalar ports are marked with "carbon forceSignal" so that a CarbonExpr is constructed
// that features the port. This led to an assertion failure at runtime because the intrinsic type
// data was not written to the IO database files.
//
// Compile with "-netVec 2 -testdriver -testdriverDumpVCD"

module top(o1, o2, o3, en, in, in2, clk);
   inout [3:0] o1;
   output [3:0] o2, o3;
    input  en;
    input [1:0] in;
    input [3:0] in2;
    input       clk;

   wrap wrap(.o10(o1[0]),
             .o11(o1[1]),
             .o12(o1[2]),
             .o13(o1[3]),
             .o2(o2),
             .o3(o3),
             .en(en),
             .in(in),
             .in2(in2),
             .clk(clk));
   
endmodule

   
module wrap(o10, o11, o12, o13, o2, o3, en, in, in2, clk); // carbon disallowFlattening
  output [3:0] o2, o3;
  inout        o10;      // carbon forceSignal
  inout        o11;      // carbon forceSignal
  inout        o12;      // carbon forceSignal
  inout        o13;      // carbon forceSignal
  input        en;
  input [1:0]  in;
  input [3:0]  in2;
  input        clk;
  pullwrap pullwrap1({o11, o10});
  pullwrap pullwrap2({o13, o12});
  pullwrap pullwrap3(o2[3:2]);
  pullwrap pullwrap4(o2[1:0]);
  flop F2 (o2, clk, {in[0], o2[3:1]});
  vecthis vecthis (o13, o12, o11, o10, o3);
  vecthat vecthat (.io({o13, o12, o11, o10}), .in(in2), .en(en));
endmodule

module pullwrap(io);
  inout [1:0] 	io;
  mypull mypull0(io[0]);
  mypull mypull1(io[1]);
endmodule

module mypull(io);
  inout  io;
  pulldown (io);
endmodule

module flop(q, clk, d);
  output [3:0] q;
  input        clk;
  input [3:0]  d;
  reg [3:0]    q;
  initial q = 0;
  always @ (posedge clk)
    begin
      q <= d;
    end

endmodule

module vecthis (a, b, c, d, x);
  input a, b, c, d;
  output [3:0] x;
  assign       x = {a, b, c, d};
  
endmodule // vecthis

module vecthat(io, in, en);
  inout [3:0] io;
  input [3:0] in;
  input       en;

  assign      io = en ? in : {4{1'bz}};
endmodule
