/* Hand-written C testbench to drive twocounter example */

#define CARBON_NO_UINT_TYPES 1  /* avoid typedefing for UInt32, etc */
#include "libtwocounter.h"      /* generated header file */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/*
 * Just to make sure the Carbon include files don't typedef UInt32
 * and friends, define routines of those names.  That would be a
 * syntax error of UInt32 was a typedef
 */
int UInt8() {return 0;}
int UInt16() {return 0;}
int UInt32() {return 0;}
int UInt64() {return 0;}
int SInt8() {return 0;}
int SInt16() {return 0;}
int SInt32() {return 0;}
int SInt64() {return 0;}

/* Convenience routine to examine one 32 bit value */
static unsigned long examine1(CarbonObjectID *model,
                              CarbonNetID* net)
{
  CarbonUInt32 val, drive;
  (void) carbonExamine(model, net, &val, &drive);
  return val;
}

static unsigned long examine1Input(CarbonObjectID *model,
                                   CarbonNetID* net)
{
  CarbonUInt32 val, drive;
  (void) carbonExamine(model, net, &val, &drive);
  return val;
}

int main()
{
  CarbonTime t = 0;
  int clk1tick = 0;
  int clk2tick = 0;
  CarbonNetID *clk1, *clk2, *reset1, *reset2, *out1, *out2;
  CarbonWaveID *fsdb;
  CarbonUInt32 one = 1;
  CarbonUInt32 zero = 0;

  /* Instantiate a two counter model */
  CarbonObjectID *twocounter =
    carbon_twocounter_create(eCarbonIODB, eCarbon_NoFlags);

  if (twocounter == NULL)
  {
    printf("Unable to create Carbon model\n");
    exit(EXIT_FAILURE);
  }

  /* Initialize wave-form dumper for FSDB format output */
  fsdb = carbonWaveInitFSDB (twocounter, "twocounter.fsdb", e1us);

  /* Get handles to all the I/O nets */
  clk1 = carbonFindNet(twocounter, "twocounter.clk1");
  assert(clk1);
  clk2 = carbonFindNet(twocounter, "twocounter.clk2");
  assert(clk2);
  reset1 = carbonFindNet(twocounter, "twocounter.reset1");
  assert(reset1);
  reset2 = carbonFindNet(twocounter, "twocounter.reset2");
  assert(reset2);
  out1 = carbonFindNet(twocounter, "twocounter.out1");
  assert(out1);
  out2 = carbonFindNet(twocounter, "twocounter.out2");
  assert(out2);

  /* Dump all primary i/o's */
  carbonDumpVars (fsdb, 1, "twocounter");
  carbonDumpOn (fsdb);

  /* Start out in reset for both domains */
  carbonDeposit(twocounter, reset1, &one, NULL);
  carbonDeposit(twocounter, reset2, &one, NULL);

  for (t = 0; t < 100000; t += 100)
  {
    /*
     * Advance clk1 every 700 ticks
     */
    if ((t % 700) == 0)
    {
      CarbonUInt32 val;
      ++clk1tick;
      val = clk1tick & 1;
      carbonDeposit(twocounter, clk1, &val, NULL);

      /* After 5 edges de-assert reset */
      if (clk1tick == 5)
        carbonDeposit(twocounter, reset1, &zero, NULL);
    }

    /*
     * Advance clk2 every 800 ticks.  There will be an occasional
     * coincident edge with clk1
     */
    if ((t % 800) == 0)
    {
      CarbonUInt32 val;
      ++clk2tick;
      val = clk2tick & 1;
      carbonDeposit(twocounter, clk2, &val, NULL);

      /* After 5 edges de-assert reset */
      if (clk2tick == 5)
        carbonDeposit(twocounter, reset2, &zero, NULL);
    }

    /*
     * Run a schedule, print out results
     */
    carbonSchedule(twocounter, t);
    fprintf(stdout,
            "%lu:\tclk1=%ld reset1=%ld clk2=%ld reset2=%ld out1=%ld out2=%ld\n",
            (unsigned long) t,
            examine1Input(twocounter, clk1),
            examine1Input(twocounter, reset1),
            examine1Input(twocounter, clk2),
            examine1Input(twocounter, reset2),
            examine1(twocounter, out1),
            examine1(twocounter, out2));
  }

  assert (carbonDumpFlush (fsdb) == eCarbon_OK);
  carbonDestroy(&twocounter);
  return 0;
}
