
module top(a, en, wide66, wide35, wide17, wide9, wide3, wide1, out1, out2, out3, out4, out5);
   input a, en;
   
   inout signed [65:0] wide66;
   inout signed [34:0] wide35;
   inout signed [16:0] wide17;
   inout signed [8:0] wide9;
   inout signed [2:0] wide3;
   inout signed wide1;
   
   output out1, out2, out3, out4, out5;

   wire signed [65:0] wideC66;
   wire signed [34:0] wideC35;
   wire signed [16:0] wideC17;
   wire signed [8:0] wideC9;
   wire signed [2:0] wideC3;   
   assign wideC66 = a;
   assign wideC35 = a;
   assign wideC17 = a;
   assign wideC9 = a;
   assign wideC3 = a;

   assign wide66 = en ? 0 : 66'bz;
   assign wide35 = en ? 0 : 35'bz;
   assign wide17 = en ? 0 : 17'bz;
   assign wide9 = en ? 0 : 9'bz;
   assign wide3 = en ? 0 : 3'bz;
   assign wide1 = en ? 0 : 1'bz;
   
   assign out1 = (wideC66 < wide66) ? 0 : 1;
   assign out2 = (wideC35 < wide35) ? 0 : 1;
   assign out3 = (wideC17 < wide17) ? 0 : 1;
   assign out4 = (wideC9 < wide9) ? 0 : 1;
   assign out5 = (wideC3 < wide3) ? 0 : 1;
endmodule
