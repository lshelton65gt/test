-- Testcase for bug 4937

library ieee;
use ieee.std_logic_1164.all;

entity sub is
  port (
    outp    : inout std_logic;
    inp, EN : in  std_logic);
end sub;

architecture a of sub is
begin
  outP <= inp xor outP when en = '1' else 'Z';
end a;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity sub2 is
  port (
    ouTP    : out std_logic;
    inp, en : in  std_logic);
end sub2;

architecture a of sub2 is
begin
  Outp <= inp and en;
end a;
