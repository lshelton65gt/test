// Testing tristates

#include "libbasic10_verilog.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_basic10_verilog_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *clk = carbonFindNet(obj, "basic10_verilog.clk");
  CarbonNetID *i = carbonFindNet(obj, "basic10_verilog.i");
  CarbonNetID *o = carbonFindNet(obj, "basic10_verilog.o");
  CarbonNetID *en_l = carbonFindNet(obj, "basic10_verilog.en_l");
  CarbonNetID *en_h = carbonFindNet(obj, "basic10_verilog.en_h");
  CarbonNetID *io = carbonFindNet(obj, "basic10_verilog.io");
  CarbonNetID *io_2 = carbonFindNet(obj, "basic10_verilog.io[2]");
  CarbonNetID *io_3 = carbonFindNet(obj, "basic10_verilog.io[3]");
  CarbonNetID *io_4 = carbonFindNet(obj, "basic10_verilog.io[4]");
  CarbonNetID *io_5 = carbonFindNet(obj, "basic10_verilog.io[5]");

  CarbonUInt32 val, drive;
  CarbonTime t = 0;

  char valStr[10];

  // test examines

  val = 0x55;
  carbonDeposit(obj, i, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;
  
  val = 1;
  carbonDeposit(obj, en_l, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;
  
  val = 1;
  carbonDeposit(obj, en_h, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;
  
  val = 0;
  carbonDeposit(obj, en_l, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;
  
  val = 0;
  carbonDeposit(obj, en_h, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;
  
  // Now test deposits
  drive = 0;
  val = 0;
  carbonDeposit(obj, io_2, &val, &drive);
  val = 1;
  carbonDeposit(obj, io_3, &val, &drive);
  val = 0;
  carbonDeposit(obj, io_4, &val, &drive);
  val = 1;
  carbonDeposit(obj, io_5, &val, &drive);
  
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;

  drive = 0;
  val = 0xff;
  carbonDeposit(obj, io, &val, &drive);
  
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;

  drive = 1;
  val = 0;
  carbonDeposit(obj, io_2, &val, &drive);
  carbonDeposit(obj, io_3, &val, &drive);
  carbonDeposit(obj, io_4, &val, &drive);
  carbonDeposit(obj, io_5, &val, &drive);
  
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;

  drive = 0x0f;
  val = 0xaa;
  carbonDeposit(obj, io, &val, &drive);
  
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;

  drive = 0;
  val = 1;
  carbonDeposit(obj, en_l, &val, &drive);
  
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;

  carbonDestroy(&obj);
  return 0;
}
