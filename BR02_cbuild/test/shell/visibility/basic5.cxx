// Testing 2-D arrays

#include "libbasic5.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_basic5_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "basic5.clk");
  CarbonNetID *addr = carbonFindNet(obj, "basic5.addr");
  CarbonNetID *we = carbonFindNet(obj, "basic5.we");
  CarbonNetID *din = carbonFindNet(obj, "basic5.din");
  CarbonNetID *dout = carbonFindNet(obj, "basic5.dout");

  CarbonDB *db = carbonGetDB(obj);

  const CarbonDBNode *node_mem = carbonDBFindNode(db, "basic5.mem");
  int index = 5;
  const CarbonDBNode *node_mem5 = carbonDBGetArrayElement(db, node_mem, &index, 1);
  CarbonNetID *mem5 = carbonDBGetCarbonNet(db, node_mem5);

  CarbonUInt32 val;
  CarbonTime t = 0;

  val = 0x55555555;
  carbonDeposit(obj, mem5, &val, 0);

  for (CarbonUInt32 i = 0; i < 10; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  index = 30;
  const CarbonDBNode *node_mem5_30 = carbonDBGetArrayElement(db, node_mem5, &index, 1);
  CarbonNetID *mem5_30 = carbonDBGetCarbonNet(db, node_mem5_30);
  val = 0x1;
  carbonDeposit(obj, mem5_30, &val, 0);

  for (CarbonUInt32 i = 0; i < 10; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  // carbonDepositRange should also work
  val = 0x82;
  carbonDepositRange(obj, mem5, &val, 20, 27, 0);

  for (CarbonUInt32 i = 0; i < 10; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  // Now test examines
  char binVal[100];

  val = 4;
  carbonDeposit(obj, addr, &val, 0);
  val = 0x12345678;
  carbonDeposit(obj, din, &val, 0);
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  
  index = 4;
  const CarbonDBNode *node_mem4 = carbonDBGetArrayElement(db, node_mem, &index, 1);
  CarbonNetID *mem4 = carbonDBGetCarbonNet(db, node_mem4);
  carbonExamine(obj, mem4, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonFormat(obj, mem4, binVal, 100, eCarbonBin);
  std::cout << "Binary: " << binVal << std::endl;

  index = 29;
  const CarbonDBNode *node_mem4_2 = carbonDBGetArrayElement(db, node_mem4, &index, 1);
  CarbonNetID *mem4_2 = carbonDBGetCarbonNet(db, node_mem4_2);
  carbonExamine(obj, mem4_2, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonFormat(obj, mem4_2, binVal, 100, eCarbonBin);
  std::cout << "Binary: " << binVal << std::endl;
  
  index = 28;
  const CarbonDBNode *node_mem4_3 = carbonDBGetArrayElement(db, node_mem4, &index, 1);
  CarbonNetID *mem4_3 = carbonDBGetCarbonNet(db, node_mem4_3);
  carbonExamine(obj, mem4_3, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonFormat(obj, mem4_3, binVal, 100, eCarbonBin);
  std::cout << "Binary: " << binVal << std::endl;

  carbonExamineRange(obj, mem4, &val, 16, 27, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;


  carbonDepositArrayFromFile(obj, node_mem, eCarbonHex, "basic5.mem");

  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  carbonDepositArrayFromFile(obj, node_mem4, eCarbonBin, "basic5.mem2");

  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  carbonExamineArrayToFile(obj, node_mem, eCarbonBin, "basic5.dump");
  carbonExamineArrayToFile(obj, node_mem4, eCarbonHex, "basic5.dump2");

  carbonDepositArrayFromFile(obj, node_mem, eCarbonHex, "basic5.mem3");

  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  carbonDestroy(&obj);
  return 0;
}
