library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity scalar2 is
  port (
    clk      : in  std_logic;
    we1      : in  std_logic;
    we2      : in  std_logic;
    we3      : in  std_logic;
    we4      : in  std_logic;
    addr1    : in  std_logic_vector(2 downto 0);
    addr2    : in  std_logic_vector(1 downto 0);
    int32in  : in  integer;
    int16in  : in  integer range -32768 to 32767;
    int8in   : in  integer range -256 to 255;
    charin   : in  character;
--    realin   : in  real;
    int32out : out integer;
    int16out : out integer range -32768 to 32767;
    int8out  : out integer range -256 to 255;
    charout  : out character);
--    realout  : out real);
end scalar2;

architecture arch of scalar2 is

  type myrec is record
                  int32 : integer;
                  int16 : integer range -32768 to 32767;
                  int8  : integer range -256 to 255;
                  c     : character;
--                  r     : real;
                end record;

  type recarray1d is array (7 downto 0) of myrec;

  type int32arr1d is array (7 downto 0) of integer;
  type int16arr1d is array (7 downto 0) of integer range -32768 to 32767;
  type int8arr1d is array (7 downto 0) of integer range -256 to 255;
  type chararr1d is array (7 downto 0) of character;
--  type realarr1d is array (7 downto 0) of real;
  
  type recarray2d is array (7 downto 0, 3 downto 0) of myrec;

  type int32arr2d is array (7 downto 0, 3 downto 0) of integer;
  type int16arr2d is array (7 downto 0, 3 downto 0) of integer range -32768 to 32767;
  type int8arr2d is array (7 downto 0, 3 downto 0) of integer range -256 to 255;
  type chararr2d is array (7 downto 0, 3 downto 0) of character;
--  type realarr2d is array (7 downto 0, 3 downto 0) of real;

  signal ra1d : recarray1d;

  signal int32a1d : int32arr1d;
  signal int16a1d : int16arr1d;
  signal int8a1d : int8arr1d;
  signal chara1d : chararr1d;
--  signal reala1d : realarr1d;

  signal ra2d : recarray2d;

  signal int32a2d : int32arr2d;
  signal int16a2d : int16arr2d;
  signal int8a2d : int8arr2d;
  signal chara2d : chararr2d;
--  signal reala2d : realarr2d;

  signal addr1_int : integer;
  signal addr2_int : integer;
  
begin

  addr1_int <= conv_integer(unsigned(addr1));
  addr2_int <= conv_integer(unsigned(addr2));
  
  process (clk)
  begin
    if clk'event and clk = '1' then

      -- inputs to 1d array of records
      if we1 = '1' then
        ra1d(addr1_int).int32 <= int32in;
        ra1d(addr1_int).int16 <= int16in;
        ra1d(addr1_int).int8 <= int8in;
        ra1d(addr1_int).c <= charin;
--      ra1d(addr1_int).r <= realin;
      end if;

      -- 1d array of records to 1d array of scalars
      if we2 = '1' then
        int32a1d(addr1_int) <= ra1d(addr1_int).int32;
        int16a1d(addr1_int) <= ra1d(addr1_int).int16;
        int8a1d(addr1_int) <= ra1d(addr1_int).int8;
        chara1d(addr1_int) <= ra1d(addr1_int).c;
--      reala1d(addr1_int) <= ra1d(addr1_int).r;
      end if;
      
      -- 1d array of scalars to 2d array of records
      if we3 = '1' then
        ra2d(addr1_int, addr2_int).int32 <= int32a1d(addr1_int);
        ra2d(addr1_int, addr2_int).int16 <= int16a1d(addr1_int);
        ra2d(addr1_int, addr2_int).int8 <= int8a1d(addr1_int);
        ra2d(addr1_int, addr2_int).c <= chara1d(addr1_int);
--      ra2d(addr1_int, addr2_int).r <= reala1d(addr1_int);
      end if;

      -- 2d array of records to 2d array or scalars
      if we4 = '1' then
        int32a2d(addr1_int, addr2_int) <= ra2d(addr1_int, addr2_int).int32;
        int16a2d(addr1_int, addr2_int) <= ra2d(addr1_int, addr2_int).int16;
        int8a2d(addr1_int, addr2_int) <= ra2d(addr1_int, addr2_int).int8;
        chara2d(addr1_int, addr2_int) <= ra2d(addr1_int, addr2_int).c;
--      reala2d(addr1_int, addr2_int) <= ra2d(addr1_int, addr2_int).r;
      end if;

      -- 2d array of scalars to outputs
      int32out <= int32a2d(addr1_int, addr2_int);
      int16out <= int16a2d(addr1_int, addr2_int);
      int8out <= int8a2d(addr1_int, addr2_int);
      charout <= chara2d(addr1_int, addr2_int);
--      realout <= reala2d(addr1_int, addr2_int);

    end if;
  end process;
  
end arch;
