module dbapi2_verilog(input clk,
                      input [3:0] addr,
                      input we,
                      input [31:0] din,
                      output reg [31:0] dout);

   reg [31:0]                arr [15:0];

   always @(posedge clk)
     if (we)
       arr[addr] <= din;
     else
       dout <= arr[addr];

endmodule
