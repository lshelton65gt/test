library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package mypack is
  type t_a0 is array (-40 to -40) of std_logic;
  type t_r0 is record
    s0 : std_logic;
    s1 : bit;
  end record;
  type t_r1 is record
    s0 : t_a0;
    s1 : t_r0;
  end record;
  type t_a1 is array (10 downto -1, 10 downto 8) of t_r1;
  type t_a2 is array (12 downto 8) of t_a1;
  type t_a3 is array (-3 downto -14, -12 to 13) of t_r1;
  type t_r2 is record
    s0 : t_a3;
  end record;
  type t_r3 is record
    s0 : std_logic;
  end record;
  type t_r4 is record
    s0 : bit;
    s1 : t_a0;
    s2 : t_r3;
  end record;
  type t_a4 is array (-3 downto -7) of t_r4;
end mypack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

use work.mypack.all;

entity random3 is
  port (
    clk : in std_logic;
    i0 : in std_logic;
    i1 : in bit;
    i2 : in t_a0;
    addr0 : in std_logic_vector (2 downto 0);
    addr1 : in std_logic_vector (3 downto 0);
    addr2 : in std_logic_vector (1 downto 0);
    addr3 : in std_logic_vector (3 downto 0);
    addr4 : in std_logic_vector (4 downto 0);
    addr5 : in std_logic_vector (2 downto 0);
    o0 : out std_logic;
    o1 : out bit;
    o2 : out t_a0);
end random3;

architecture arch of random3 is

  signal s0 : t_a2;
  signal s1 : t_r2;
  signal s2 : t_a4;
  signal addr0_int : integer;
  signal addr1_int : integer;
  signal addr2_int : integer;
  signal addr3_int : integer;
  signal addr4_int : integer;
  signal addr5_int : integer;

begin

  addr0_int <= conv_integer(unsigned(addr0)) +8;
  addr1_int <= conv_integer(unsigned(addr1)) -1;
  addr2_int <= conv_integer(unsigned(addr2)) +8;
  addr3_int <= conv_integer(unsigned(addr3)) -14;
  addr4_int <= conv_integer(unsigned(addr4)) -12;
  addr5_int <= conv_integer(unsigned(addr5)) -7;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s0(addr0_int)(addr1_int, addr2_int).s1.s0 <= i0;
      s0(addr0_int)(addr1_int, addr2_int).s1.s1 <= i1;
      s0(addr0_int)(addr1_int, addr2_int).s0 <= i2;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s1.s0(addr3_int, addr4_int).s1.s0 <= s0(addr0_int)(addr1_int, addr2_int).s1.s0;
      s1.s0(addr3_int, addr4_int).s1.s1 <= s0(addr0_int)(addr1_int, addr2_int).s1.s1;
      s1.s0(addr3_int, addr4_int).s0 <= s0(addr0_int)(addr1_int, addr2_int).s0;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s2(addr5_int).s2.s0 <= s1.s0(addr3_int, addr4_int).s1.s0;
      s2(addr5_int).s0 <= s1.s0(addr3_int, addr4_int).s1.s1;
      s2(addr5_int).s1 <= s1.s0(addr3_int, addr4_int).s0;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      o0 <= s2(addr5_int).s2.s0;
      o1 <= s2(addr5_int).s0;
      o2 <= s2(addr5_int).s1;
    end if;
  end process;


end arch;
