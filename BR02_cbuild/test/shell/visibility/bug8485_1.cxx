// This actually tests bugs 8485 and 8498
#include "libbug8485_1.h"
#include <cstring>
#include <string>

int main(int argc, char** argv)
{
  bool putPrefix = false;
  bool replaceRoot = false;

  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-putPrefix") == 0) {
      putPrefix = true;
    } else if (strcmp(argv[i], "-replaceRoot") == 0) {
      replaceRoot = true;
    }
  }

  std::string fsdbFile("bug8485_1");
  if (putPrefix) {
    if (replaceRoot) {
      fsdbFile += ".3";
    } else {
      fsdbFile += ".2";
    }
  }
  fsdbFile += ".fsdb";

  // All we care about is the wave hierarchy, so create a model, dump
  // waves, and exit
  CarbonObjectID* obj = carbon_bug8485_1_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonWaveID* fsdb = carbonWaveInitFSDB(obj, fsdbFile.c_str(), e1ns);
  if (putPrefix) {
    carbonPutPrefixHierarchy(fsdb, "fake1.fake2", replaceRoot);
  }
  carbonDumpVars(fsdb, 0, "bug8485_1");
  carbonSchedule(obj, 0);
  carbonDestroy(&obj);
  return 0;
}
