// Testing access of individual bits of ports

#include "libports1.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <cassert>
#include <iostream>

void printNet(CarbonObjectID* obj, CarbonNetID* net, CarbonClientData data, CarbonUInt32* val, CarbonUInt32*)
{
  CarbonTime t = *static_cast<CarbonTime*>(data);

  char name[100];
  carbonGetNetName(obj, net, name, 100);
  std::cout << "At time " << std::dec << t << " net " << name << " changed value to " << std::hex << *val << std::endl;
}

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_ports1_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "ports1.clk");
  CarbonNetID *i = carbonFindNet(obj, "ports1.i");
  CarbonNetID *o = carbonFindNet(obj, "ports1.o");

  CarbonDB *db = carbonGetDB(obj);

  CarbonNetID *ibits[8];
  CarbonNetID *obits[8];

  const CarbonDBNode *i_node = carbonNetGetDBNode(obj, i);
  const CarbonDBNode *o_node = carbonNetGetDBNode(obj, o);

  CarbonUInt32 val;
  CarbonTime t = 0;

  for (CarbonSInt32 j = 0; j < 8; ++j) {
    const CarbonDBNode *node = carbonDBGetArrayElement(db, i_node, &j, 1);
    ibits[j] = carbonDBGetCarbonNet(db, node);
    node = carbonDBGetArrayElement(db, o_node, &j, 1);
    obits[j] = carbonDBGetCarbonNet(db, node);
    carbonAddNetValueChangeCB(obj, printNet, &t, obits[j]);
  }

  for (CarbonUInt32 j = 0; j < 8; ++j) {
    val = 1;
    carbonDeposit(obj, ibits[j], &val, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  carbonDestroy(&obj);
  return 0;
}
