// Testing unique scope names in FSDB file

#include "libbug8468.h"

int main(int argc, char** argv)
{
  // All we care about is the wave hierarchy, so create a model, dump
  // waves, and exit
  CarbonObjectID* obj = carbon_bug8468_create(eCarbonFullDB, eCarbon_NoFlags);
  carbonDumpVars(carbonWaveInitFSDB(obj, "bug8468.fsdb", e1ns), 0, "bug8468");
  carbonDestroy(&obj);
  return 0;
}
