// Testing force/release

#include "libondemand2.h"
#include "carbon/carbon_dbapi.h"
#include <iostream>

static const char *modes[] = {"looking", "idle", "backoff"};

static void mode_change_cb(CarbonObjectID* context,
                           void* userContext,
                           CarbonOnDemandMode from,
                           CarbonOnDemandMode to,
                           CarbonTime simTime)
{
  std::cout << "On-demand entering " << modes[to] << " mode at time " << std::dec << simTime << std::endl;
}

static void run_cycles(CarbonUInt32 count, CarbonObjectID *obj, CarbonNetID *clk, CarbonTime &t)
{
  CarbonUInt32 val;
  for (CarbonUInt32 i = 0; i < (count * 2); ++i) {
    val = t & 0x1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, ++t);
  }
}

static void timestamp(const CarbonTime &t)
{
  std::cout << "TIME: " << std::dec << t << std::endl;
}

int main()
{
  CarbonObjectID *obj = carbon_ondemand2_create(eCarbonFullDB, eCarbon_OnDemand);
  carbonOnDemandSetMaxStates(obj, 50);
  carbonOnDemandSetBackoffStrategy(obj, eCarbonOnDemandBackoffConstant);
  carbonOnDemandSetBackoffCount(obj, 50);
  carbonOnDemandAddModeChangeCB(obj, mode_change_cb, 0);
  carbonOnDemandEnableStats(obj);
  carbonOnDemandSetDebugLevel(obj, eCarbonOnDemandDebugAll);

  CarbonNetID *clk = carbonFindNet(obj, "ondemand2.clk");
  CarbonNetID *rst = carbonFindNet(obj, "ondemand2.rst");
  CarbonNetID *in = carbonFindNet(obj, "ondemand2.in");
  CarbonNetID *inv = carbonFindNet(obj, "ondemand2.inv");
  CarbonNetID *out = carbonFindNet(obj, "ondemand2.out");

  // Get a net for each bit of inv
  CarbonDB* db = carbonGetDB(obj);
  const CarbonDBNode* inv_node = carbonNetGetDBNode(obj, inv);
  CarbonNetID* inv_bits[64];
  for (int i = 0; i < 64; ++i) {
    const CarbonDBNode* node = carbonDBGetArrayElement(db, inv_node, &i, 1);
    inv_bits[i] = carbonDBGetCarbonNet(db, node);
  }

  CarbonTime t = 0;
  CarbonUInt32 val[2];

//   carbonDumpVars(carbonWaveInitFSDB(obj, "ondemand2.fsdb", e1ns), 0, "ondemand2");

  // reset

  *val = 1;
  carbonDeposit(obj, rst, val, 0);
  val[1] = 0x12345678;
  val[0] = 0x90abcdef;
  carbonDeposit(obj, in, val, 0);
  run_cycles(5, obj, clk, t);

  *val = 0;
  carbonDeposit(obj, rst, val, 0);
  run_cycles(5, obj, clk, t);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);

  // carbonForce()/carbonRelease()
  // Idle mode should be exited.
  timestamp(t);
  val[1] = 0xfedcba09;
  val[0] = 0x87654321;
  carbonForce(obj, inv, val);
  run_cycles(1, obj, clk, t);
  carbonExamine(obj, out, val, 0);
  std::cout << "out[63:32] is 0x" << std::hex << val[1] << std::endl;
  std::cout << "out[31:0] is 0x" << std::hex << val[0] << std::endl;
  run_cycles(50, obj, clk, t);
  carbonRelease(obj, inv);
  run_cycles(1, obj, clk, t);
  carbonExamine(obj, out, val, 0);
  std::cout << "out[63:32] is 0x" << std::hex << val[1] << std::endl;
  std::cout << "out[31:0] is 0x" << std::hex << val[0] << std::endl;
  run_cycles(50, obj, clk, t);

  // carbonForce()/carbonRelease()
  // Idle mode should be exited.
  timestamp(t);
  *val = 0xaaaaaaaa;
  // Force bits [63:32]
  for (int i = 32; i < 64; ++i) {
    CarbonUInt32 depVal = (*val >> (i - 32)) & 0x1;
    carbonForce(obj, inv_bits[i], &depVal);
  }
  run_cycles(1, obj, clk, t);
  carbonExamine(obj, out, val, 0);
  std::cout << "out[63:32] is 0x" << std::hex << val[1] << std::endl;
  std::cout << "out[31:0] is 0x" << std::hex << val[0] << std::endl;
  run_cycles(50, obj, clk, t);
  for (int i = 32; i < 64; ++i) {
    carbonRelease(obj, inv_bits[i]);
  }
  run_cycles(1, obj, clk, t);
  carbonExamine(obj, out, val, 0);
  std::cout << "out[63:32] is 0x" << std::hex << val[1] << std::endl;
  std::cout << "out[31:0] is 0x" << std::hex << val[0] << std::endl;
  run_cycles(50, obj, clk, t);

  // carbonForce()/carbonRelease()
  // Idle mode should be exited.
  timestamp(t);
  *val = 0xbbbbbbbb;
  // Force bits [47:16]
  for (int i = 16; i < 48; ++i) {
    CarbonUInt32 depVal = (*val >> (i - 16)) & 0x1;
    carbonForce(obj, inv_bits[i], &depVal);
  }
  run_cycles(1, obj, clk, t);
  carbonExamine(obj, out, val, 0);
  std::cout << "out[63:32] is 0x" << std::hex << val[1] << std::endl;
  std::cout << "out[31:0] is 0x" << std::hex << val[0] << std::endl;
  run_cycles(50, obj, clk, t);
  for (int i = 16; i < 48; ++i) {
    carbonRelease(obj, inv_bits[i]);
  }
  run_cycles(1, obj, clk, t);
  carbonExamine(obj, out, val, 0);
  std::cout << "out[63:32] is 0x" << std::hex << val[1] << std::endl;
  std::cout << "out[31:0] is 0x" << std::hex << val[0] << std::endl;
  run_cycles(50, obj, clk, t);

  carbonDestroy(&obj);
  return 0;
}
