module ondemand1(clk, rst, in, out);
   input clk, rst;
   input [3:0] in;
   output [3:0] out;

   reg [3:0]    count;

   always @(posedge clk or posedge rst)
     if (rst)
       count <= 4'b0;
     else
       count <= count + 4'b1;

   assign       out = count + in;
endmodule

