library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package mypack is
  type t_r0 is record
    s0 : std_logic;
  end record;
  type t_a0 is array (-1 downto -15, -15 to -14, 11 downto -10) of t_r0;
  type t_a1 is array (-6 to -5) of t_r0;
  type t_a2 is array (11 downto 9, 9 downto -11) of std_logic;
  type t_a3 is array (-4 to 10) of t_a2;
  type t_r1 is record
    s0 : t_a3;
  end record;
end mypack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

use work.mypack.all;

entity bug8285_2 is
  port (
    clk : in std_logic;
    i0 : in std_logic;
    addr0 : in std_logic_vector (3 downto 0);
    addr1 : in std_logic_vector (0 to 0);
    addr2 : in std_logic_vector (4 downto 0);
    addr3 : in std_logic_vector (0 to 0);
    addr4 : in std_logic_vector (3 downto 0);
    addr5 : in std_logic_vector (1 downto 0);
    addr6 : in std_logic_vector (4 downto 0);
    o0 : out std_logic);
end bug8285_2;

architecture arch of bug8285_2 is

  signal s0 : t_a0;
  signal s1 : t_a1;
  signal s2 : t_r1;
  signal addr0_int : integer;
  signal addr1_int : integer;
  signal addr2_int : integer;
  signal addr3_int : integer;
  signal addr4_int : integer;
  signal addr5_int : integer;
  signal addr6_int : integer;

begin

  addr0_int <= conv_integer(unsigned(addr0)) -15;
  addr1_int <= conv_integer(unsigned(addr1)) -15;
  addr2_int <= conv_integer(unsigned(addr2)) -10;
  addr3_int <= conv_integer(unsigned(addr3)) -6;
  addr4_int <= conv_integer(unsigned(addr4)) -4;
  addr5_int <= conv_integer(unsigned(addr5)) +9;
  addr6_int <= conv_integer(unsigned(addr6)) -11;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s0(addr0_int, addr1_int, addr2_int).s0 <= i0;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s1(addr3_int).s0 <= s0(addr0_int, addr1_int, addr2_int).s0;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s2.s0(addr4_int)(addr5_int, addr6_int) <= s1(addr3_int).s0;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      o0 <= s2.s0(addr4_int)(addr5_int, addr6_int);
    end if;
  end process;


end arch;
