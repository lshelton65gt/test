// Testing complex arrays of records

#include "librecarray2.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_recarray2_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "recarray2.clk");
  CarbonNetID *a = carbonFindNet(obj, "recarray2.a");
  CarbonNetID *b = carbonFindNet(obj, "recarray2.b");
  CarbonNetID *o = carbonFindNet(obj, "recarray2.o");
  CarbonNetID *we = carbonFindNet(obj, "recarray2.we");
  CarbonNetID *addr = carbonFindNet(obj, "recarray2.addr");

  CarbonUInt32 val;
  CarbonTime t = 0;

  CarbonDB *db = carbonGetDB(obj);
  const CarbonDBNode *node_arr = carbonDBFindNode(db, "recarray2.arr");
  int index = 11;
  const CarbonDBNode *node_arr_1 = carbonDBGetArrayElement(db, node_arr, &index, 1);
  const CarbonDBNode *node_arr_1_b = carbonDBGetStructFieldByName(db, node_arr_1, "b");
  index = 16;
  const CarbonDBNode *node_arr_1_b_4 = carbonDBGetArrayElement(db, node_arr_1_b, &index, 1);
  index = 15;
  const CarbonDBNode *node_arr_1_b_5 = carbonDBGetArrayElement(db, node_arr_1_b, &index, 1);

  CarbonNetID *arr_1_b = carbonDBGetCarbonNet(db, node_arr_1_b);
  CarbonNetID *arr_1_b_4 = carbonDBGetCarbonNet(db, node_arr_1_b_4);
  CarbonNetID *arr_1_b_5 = carbonDBGetCarbonNet(db, node_arr_1_b_5);

  val = 0xc3;
  carbonDeposit(obj, arr_1_b, &val, 0);

  for (CarbonUInt32 i = 0; i < 4; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Out is 0x" << std::hex << val << std::endl;
  }

  val = 1;
  carbonDeposit(obj, arr_1_b_4, &val, 0);
  carbonDeposit(obj, arr_1_b_5, &val, 0);

  for (CarbonUInt32 i = 0; i < 4; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Out is 0x" << std::hex << val << std::endl;
  }

  val = 0x7;
  carbonDepositRange(obj, arr_1_b, &val, 15, 18, 0);

  for (CarbonUInt32 i = 0; i < 4; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Out is 0x" << std::hex << val << std::endl;
  }

  val = 1;
  carbonDeposit(obj, we, &val, 0);
  carbonDeposit(obj, addr, &val, 0);
  val = 0x55;
  carbonDeposit(obj, b, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonExamine(obj, arr_1_b, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_1_b_4, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_1_b_5, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamineRange(obj, arr_1_b, &val, 14, 17, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;

  carbonDestroy(&obj);
  return 0;
}
