library ieee;
use ieee.std_logic_1164.all;

entity ports1 is
  port (
    clk : in  std_logic;
    i   : in  std_logic_vector (7 downto 0);
    o   : out std_logic_vector (7 downto 0));
end ports1;

architecture arch of ports1 is
begin
  process (clk)
  begin
    if clk'event and clk = '1' then
      o <= i;
    end if;
  end process;
end arch;
