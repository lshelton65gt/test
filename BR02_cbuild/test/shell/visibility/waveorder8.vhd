library ieee;
use ieee.std_logic_1164.all;

entity waveorder8 is
  port (
    clk : in  std_logic;
    i   : in  std_logic_vector (7 downto 0);
    o   : out std_logic);
end waveorder8;

architecture arch of waveorder8 is

  type subrec is record
                   c : std_logic;
                   a : std_logic;
                   b : std_logic;
                 end record;
  
  type myrec is record
                  z : std_logic;
                  w : subrec;
                  y : subrec;
                  x : std_logic;
                end record;
  signal r : myrec;     -- carbon observeSignal

begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      r.z <= i(0);
      r.w.c <= i(1);
      r.w.a <= i(2);
      r.w.b <= i(3);
      r.y.c <= i(4);
      r.y.a <= i(5);
      r.y.b <= i(6);
      r.x <= i(7);
      o <= r.z and r.w.c and r.w.a and r.w.b and r.y.c and r.y.a and r.y.b and r.x;
    end if;
  end process;
  
end arch;
