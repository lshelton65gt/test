module array2_verilog(input clk,
                      input we,
                      input sel,
                      input [3:0] din,
                      output reg [3:0] dout);

   reg [15:12]               arr [8:9];

   always @(posedge clk)
     if (we)
       if (sel) begin
          arr[9][15] <= din[3];
          arr[9][14] <= din[2];
          arr[9][13] <= din[1];
          arr[9][12] <= din[0];
       end
       else begin
          arr[8][15] <= din[3];
          arr[8][14] <= din[2];
          arr[8][13] <= din[1];
          arr[8][12] <= din[0];
       end
     else
       if (sel) begin
          dout[3] <= arr[9][15];
          dout[2] <= arr[9][14];
          dout[1] <= arr[9][13];
          dout[0] <= arr[9][12];
       end
       else begin
          dout[3] <= arr[8][15];
          dout[2] <= arr[8][14];
          dout[1] <= arr[8][13];
          dout[0] <= arr[8][12];
       end
       
endmodule
