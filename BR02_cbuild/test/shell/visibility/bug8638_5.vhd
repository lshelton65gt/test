library ieee;
use ieee.std_logic_1164.all;

package mypack is
  type rec1 is record
                  a : std_logic;
                end record;

  type array1 is array (2 to 3) of rec1;
  
end mypack;

library ieee;
use ieee.std_logic_1164.all;
use work.mypack.all;

entity bug8638_5 is
  port (
    clk : in  std_logic;
    i   : in  array1;
    sel : in std_logic;
    o   : out std_logic);
end bug8638_5;

architecture arch of bug8638_5 is
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if sel = '0' then
        o <= i(2).a;
      else
        o <= i(3).a;
      end if;
    end if;
  end process;
  
end arch;
