// Testing reimplemented legacy DB API functions

#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <iostream>

int main()
{
  CarbonDB *db = carbonDBCreate("libdbapi9.symtab.db", 1);

  // Get a handle to the top of the design
  const CarbonDBNode *top = carbonDBFindNode(db, "dbapi9");

  // Get a handle to the array
  const CarbonDBNode *arr = carbonDBFindNode(db, "dbapi9.arr");
  assert(carbonDBNodeGetParent(db, arr) == top);

  const CarbonDBNode *arr_alt = carbonDBFindChild(db, top, "arr");
  assert(arr_alt == arr);

  // This should not be reported as a 2-D array, since its base
  // element type is a struct.
  assert(carbonDBIsScalar(db, arr) == 0);
  assert(carbonDBIsVector(db, arr) == 0);
  assert(carbonDBIs2DArray(db, arr) == 0);
  // These shouldn't crash, and should return 0
  assert(carbonDBGetWidth(db, arr) == 0);
  assert(carbonDBGetMSB(db, arr) == 0);
  assert(carbonDBGetLSB(db, arr) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr) == 0);
  assert(carbonDBGet2DArrayRightAddr(db, arr) == 0);

  // Loop over all children, saving the first one
  std::cout << "Children of " << carbonDBNodeGetFullName(db, arr) << std::endl;
  const CarbonDBNode *arr_0 = NULL;
  CarbonDBNodeIter *iter = carbonDBLoopChildren(db, arr);
  const CarbonDBNode *node;
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    if (arr_0 == NULL) {
      arr_0 = node;
    }
    std::cout << "  " << carbonDBNodeGetFullName(db, node) << std::endl;
  }

  assert(carbonDBNodeGetParent(db, arr_0) == arr);
  // Also not a simple scalar, vector, or 2-D array
  assert(carbonDBIsScalar(db, arr_0) == 0);
  assert(carbonDBIsVector(db, arr_0) == 0);
  assert(carbonDBIs2DArray(db, arr_0) == 0);
  assert(carbonDBGetWidth(db, arr_0) == 0);
  assert(carbonDBGetMSB(db, arr_0) == 0);
  assert(carbonDBGetLSB(db, arr_0) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_0) == 0);
  assert(carbonDBGet2DArrayRightAddr(db, arr_0) == 0);

  // Loop over all children, saving the first one
  std::cout << "Children of " << carbonDBNodeGetFullName(db, arr_0) << std::endl;
  const CarbonDBNode *arr_0_0 = NULL;
  iter = carbonDBLoopChildren(db, arr_0);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    if (arr_0_0 == NULL) {
      arr_0_0 = node;
    }
    std::cout << "  " << carbonDBNodeGetFullName(db, node) << std::endl;
  }

  assert(carbonDBNodeGetParent(db, arr_0_0) == arr_0);
  // Also not a simple scalar, vector, or 2-D array
  assert(carbonDBIsScalar(db, arr_0_0) == 0);
  assert(carbonDBIsVector(db, arr_0_0) == 0);
  assert(carbonDBIs2DArray(db, arr_0_0) == 0);
  assert(carbonDBGetWidth(db, arr_0_0) == 0);
  assert(carbonDBGetMSB(db, arr_0_0) == 0);
  assert(carbonDBGetLSB(db, arr_0_0) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_0_0) == 0);
  assert(carbonDBGet2DArrayRightAddr(db, arr_0_0) == 0);

  // Loop the three children by name
  const CarbonDBNode *arr_0_0_x = carbonDBFindChild(db, arr_0_0, "x");
  assert(arr_0_0_x != NULL);
  const CarbonDBNode *arr_0_0_y = carbonDBFindChild(db, arr_0_0, "y");
  assert(arr_0_0_y != NULL);
  const CarbonDBNode *arr_0_0_z = carbonDBFindChild(db, arr_0_0, "z");
  assert(arr_0_0_z != NULL);

  // x is a scalar
  assert(carbonDBIsScalar(db, arr_0_0_x) == 1);
  assert(carbonDBIsVector(db, arr_0_0_x) == 0);
  assert(carbonDBIs2DArray(db, arr_0_0_x) == 0);
  assert(carbonDBGetWidth(db, arr_0_0_x) == 1);
  assert(carbonDBGetMSB(db, arr_0_0_x) == 0);
  assert(carbonDBGetLSB(db, arr_0_0_x) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_0_0_x) == 0);
  assert(carbonDBGet2DArrayRightAddr(db, arr_0_0_x) == 0);

  // y is a vector
  assert(carbonDBIsScalar(db, arr_0_0_y) == 0);
  assert(carbonDBIsVector(db, arr_0_0_y) == 1);
  assert(carbonDBIs2DArray(db, arr_0_0_y) == 0);
  assert(carbonDBGetWidth(db, arr_0_0_y) == 32);
  assert(carbonDBGetMSB(db, arr_0_0_y) == 31);
  assert(carbonDBGetLSB(db, arr_0_0_y) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_0_0_y) == 0);
  assert(carbonDBGet2DArrayRightAddr(db, arr_0_0_y) == 0);

  // z is a memory
  assert(carbonDBIsScalar(db, arr_0_0_z) == 0);
  assert(carbonDBIsVector(db, arr_0_0_z) == 0);
  assert(carbonDBIs2DArray(db, arr_0_0_z) == 1);
  assert(carbonDBGetWidth(db, arr_0_0_z) == 32);
  assert(carbonDBGetMSB(db, arr_0_0_z) == 31);
  assert(carbonDBGetLSB(db, arr_0_0_z) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_0_0_z) == 15);
  assert(carbonDBGet2DArrayRightAddr(db, arr_0_0_z) == 0);


  // Loop over all children of y, saving the first one
  std::cout << "Children of " << carbonDBNodeGetFullName(db, arr_0_0_y) << std::endl;
  const CarbonDBNode *arr_0_0_y_0 = NULL;
  iter = carbonDBLoopChildren(db, arr_0_0_y);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    if (arr_0_0_y_0 == NULL) {
      arr_0_0_y_0 = node;
    }
    std::cout << "  " << carbonDBNodeGetFullName(db, node) << std::endl;
  }

  assert(carbonDBNodeGetParent(db, arr_0_0_y_0) == arr_0_0_y);

  // y[0] is a scalar
  assert(carbonDBIsScalar(db, arr_0_0_y_0) == 1);
  assert(carbonDBIsVector(db, arr_0_0_y_0) == 0);
  assert(carbonDBIs2DArray(db, arr_0_0_y_0) == 0);
  assert(carbonDBGetWidth(db, arr_0_0_y_0) == 1);
  assert(carbonDBGetMSB(db, arr_0_0_y_0) == 0);
  assert(carbonDBGetLSB(db, arr_0_0_y_0) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_0_0_y_0) == 0);
  assert(carbonDBGet2DArrayRightAddr(db, arr_0_0_y_0) == 0);
  
  // Loop over all children of z, saving the first one
  std::cout << "Children of " << carbonDBNodeGetFullName(db, arr_0_0_z) << std::endl;
  const CarbonDBNode *arr_0_0_z_0 = NULL;
  iter = carbonDBLoopChildren(db, arr_0_0_z);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    if (arr_0_0_z_0 == NULL) {
      arr_0_0_z_0 = node;
    }
    std::cout << "  " << carbonDBNodeGetFullName(db, node) << std::endl;
  }

  assert(carbonDBNodeGetParent(db, arr_0_0_z_0) == arr_0_0_z);

  // z[0] is a vector
  assert(carbonDBIsScalar(db, arr_0_0_z_0) == 0);
  assert(carbonDBIsVector(db, arr_0_0_z_0) == 1);
  assert(carbonDBIs2DArray(db, arr_0_0_z_0) == 0);
  assert(carbonDBGetWidth(db, arr_0_0_z_0) == 32);
  assert(carbonDBGetMSB(db, arr_0_0_z_0) == 31);
  assert(carbonDBGetLSB(db, arr_0_0_z_0) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_0_0_z_0) == 0);
  assert(carbonDBGet2DArrayRightAddr(db, arr_0_0_z_0) == 0);

  // Loop over all children of z[0], saving the first one
  std::cout << "Children of " << carbonDBNodeGetFullName(db, arr_0_0_z_0) << std::endl;
  const CarbonDBNode *arr_0_0_z_0_0 = NULL;
  iter = carbonDBLoopChildren(db, arr_0_0_z_0);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    if (arr_0_0_z_0_0 == NULL) {
      arr_0_0_z_0_0 = node;
    }
    std::cout << "  " << carbonDBNodeGetFullName(db, node) << std::endl;
  }

  assert(carbonDBNodeGetParent(db, arr_0_0_z_0_0) == arr_0_0_z_0);

  // z[0][0] is a scalar
  assert(carbonDBIsScalar(db, arr_0_0_z_0_0) == 1);
  assert(carbonDBIsVector(db, arr_0_0_z_0_0) == 0);
  assert(carbonDBIs2DArray(db, arr_0_0_z_0_0) == 0);
  assert(carbonDBGetWidth(db, arr_0_0_z_0_0) == 1);
  assert(carbonDBGetMSB(db, arr_0_0_z_0_0) == 0);
  assert(carbonDBGetLSB(db, arr_0_0_z_0_0) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_0_0_z_0_0) == 0);
  assert(carbonDBGet2DArrayRightAddr(db, arr_0_0_z_0_0) == 0);

  // These should all fail
  node = carbonDBFindNode(db, "top.arr.x");
  assert(node == NULL);
  node = carbonDBFindNode(db, "top.arr.y");
  assert(node == NULL);
  node = carbonDBFindNode(db, "top.arr.z");
  assert(node == NULL);

  node = carbonDBFindChild(db, arr, "x");
  assert(node == NULL);
  node = carbonDBFindChild(db, arr, "y");
  assert(node == NULL);
  node = carbonDBFindChild(db, arr, "z");
  assert(node == NULL);

  // Test population of loop functions
  std::cout << "Observable nets:" << std::endl;
  iter = carbonDBLoopObservable(db);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    std::cout << "  " << carbonDBNodeGetFullName(db, node) << std::endl;
  }
  carbonDBFreeNodeIter(iter);

  carbonDBFree(db);
  return 0;
}
