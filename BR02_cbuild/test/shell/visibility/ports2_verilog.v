module ports2_verilog(input [7:0] clk,
              input [7:0] i,
              output reg [7:0] o);

   wire                    clk0 = clk[0];
   wire                    clk1 = clk[1];
   wire                    clk2 = clk[2];
   wire                    clk3 = clk[3];
   wire                    clk4 = clk[4];
   wire                    clk5 = clk[5];
   wire                    clk6 = clk[6];
   wire                    clk7 = clk[7];
   
   always @(posedge clk0)
     o[0] <= i[0];
   
   always @(posedge clk1)
     o[1] <= i[1];
   
   always @(posedge clk2)
     o[2] <= i[2];
   
   always @(posedge clk3)
     o[3] <= i[3];
   
   always @(posedge clk4)
     o[4] <= i[4];
   
   always @(posedge clk5)
     o[5] <= i[5];
   
   always @(posedge clk6)
     o[6] <= i[6];
   
   always @(posedge clk7)
     o[7] <= i[7];
   
endmodule
