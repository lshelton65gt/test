// Testing 2-D arrays

#include "libarray4.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_array4_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "array4.clk");
  CarbonNetID *sel = carbonFindNet(obj, "array4.sel");
  CarbonNetID *we = carbonFindNet(obj, "array4.we");
  CarbonNetID *din = carbonFindNet(obj, "array4.din");
  CarbonNetID *dout = carbonFindNet(obj, "array4.dout");

  CarbonDB *db = carbonGetDB(obj);

  const CarbonDBNode *node_arr = carbonDBFindNode(db, "array4.arr");
  int index = 8;
  const CarbonDBNode *node_arr_8 = carbonDBGetArrayElement(db, node_arr, &index, 1);
  CarbonNetID *arr_8 = carbonDBGetCarbonNet(db, node_arr_8);
  index = 9;
  const CarbonDBNode *node_arr_9 = carbonDBGetArrayElement(db, node_arr, &index, 1);
  CarbonNetID *arr_9 = carbonDBGetCarbonNet(db, node_arr_9);

  CarbonUInt32 val;
  CarbonTime t = 0;

  val = 0x1;
  carbonDeposit(obj, arr_8, &val, 0);
  val = 0x2;
  carbonDeposit(obj, arr_9, &val, 0);

  for (CarbonUInt32 i = 0; i < 2; ++i) {
    carbonDeposit(obj, sel, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data with select = " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  index = 13;
  const CarbonDBNode *node_arr_8_14 = carbonDBGetArrayElement(db, node_arr_8, &index, 1);
  CarbonNetID *arr_8_14 = carbonDBGetCarbonNet(db, node_arr_8_14);
  const CarbonDBNode *node_arr_9_14 = carbonDBGetArrayElement(db, node_arr_9, &index, 1);
  CarbonNetID *arr_9_14 = carbonDBGetCarbonNet(db, node_arr_9_14);

  val = 0x1;
  carbonDeposit(obj, arr_8_14, &val, 0);
  carbonDeposit(obj, arr_9_14, &val, 0);

  for (CarbonUInt32 i = 0; i < 2; ++i) {
    carbonDeposit(obj, sel, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data with select = " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  // carbonDepositRange should also work
  val = 0x2;
  carbonDepositRange(obj, arr_8, &val, 12, 13, 0);
  carbonDepositRange(obj, arr_9, &val, 12, 13, 0);

  for (CarbonUInt32 i = 0; i < 2; ++i) {
    carbonDeposit(obj, sel, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data with select = " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  // Now test examines
  val = 0;
  carbonDeposit(obj, sel, &val, 0);
  val = 0xb;
  carbonDeposit(obj, din, &val, 0);
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  
  val = 1;
  carbonDeposit(obj, sel, &val, 0);
  val = 0xc;
  carbonDeposit(obj, din, &val, 0);
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  
  carbonExamine(obj, arr_8, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_9, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;

  carbonExamine(obj, arr_8_14, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_9_14, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;

  carbonExamineRange(obj, arr_8, &val, 12, 14, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamineRange(obj, arr_9, &val, 12, 14, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;

  carbonDestroy(&obj);
  return 0;
}
