library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity recarray2 is
  
  port (
    a    : in  std_logic_vector(0 to 7);
    b    : in  std_logic_vector(0 to 7);
    clk  : in  std_logic;
    o    : out std_logic_vector(0 to 7);
    we   : in  std_logic;
    addr : in  std_logic_vector(3 downto 0));

end recarray2;

architecture arch of recarray2 is

  type subarray is array (13 to 20) of std_logic;

  type myrec is record
                  a : subarray;
                  b : subarray;
                end record;

  type myarray is array (25 downto 10) of myrec;

  signal arr : myarray;

  signal addr_int : integer;
  
begin

  addr_int <= conv_integer(unsigned(addr)) + 10;
  
  process (clk)
  begin
    if  clk'event and clk = '1' then
      if we = '1' then
        arr(addr_int).a(13) <= a(0);
        arr(addr_int).a(14) <= a(1);
        arr(addr_int).a(15) <= a(2);
        arr(addr_int).a(16) <= a(3);
        arr(addr_int).a(17) <= a(4);
        arr(addr_int).a(18) <= a(5);
        arr(addr_int).a(19) <= a(6);
        arr(addr_int).a(20) <= a(7);
        arr(addr_int).b(13) <= b(0);
        arr(addr_int).b(14) <= b(1);
        arr(addr_int).b(15) <= b(2);
        arr(addr_int).b(16) <= b(3);
        arr(addr_int).b(17) <= b(4);
        arr(addr_int).b(18) <= b(5);
        arr(addr_int).b(19) <= b(6);
        arr(addr_int).b(20) <= b(7);
      else
        o(0) <= arr(addr_int).a(13) or arr(addr_int).b(13);
        o(1) <= arr(addr_int).a(14) or arr(addr_int).b(14);
        o(2) <= arr(addr_int).a(15) or arr(addr_int).b(15);
        o(3) <= arr(addr_int).a(16) or arr(addr_int).b(16);
        o(4) <= arr(addr_int).a(17) or arr(addr_int).b(17);
        o(5) <= arr(addr_int).a(18) or arr(addr_int).b(18);
        o(6) <= arr(addr_int).a(19) or arr(addr_int).b(19);
        o(7) <= arr(addr_int).a(20) or arr(addr_int).b(20);
      end if;
      
    end if;
  end process;
  
end arch;
