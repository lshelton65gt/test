library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package mypack is
  type t_r0 is record
    s0 : std_logic;
    s1 : std_logic_vector (4 downto 0);
    s2 : std_logic_vector (3 downto 0);
    s3 : integer;
  end record;
  type t_a0 is array (3 downto 0) of t_r0;
  type t_a1 is array (7 downto 0) of std_logic;
  type t_a2 is array (7 downto 0) of integer;
  type t_r1 is record
    s0 : std_logic_vector (4 downto 0);
    s1 : std_logic_vector (3 downto 0);
    s2 : t_a1;
    s3 : t_a2;
  end record;
  type t_a3 is array (5 downto 2, 1 to 4) of t_r0;
end mypack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

use work.mypack.all;

entity random1 is
  port (
    clk : in std_logic;
    i0 : in std_logic;
    i1 : in std_logic_vector (4 downto 0);
    i2 : in std_logic_vector (3 downto 0);
    i3 : in integer;
    addr0 : in std_logic_vector (1 downto 0);
    addr1 : in std_logic_vector (2 downto 0);
    addr2 : in std_logic_vector (2 downto 0);
    addr3 : in std_logic_vector (1 downto 0);
    addr4 : in std_logic_vector (1 downto 0);
    o0 : out std_logic;
    o1 : out std_logic_vector (4 downto 0);
    o2 : out std_logic_vector (3 downto 0);
    o3 : out integer);
end random1;

architecture arch of random1 is

  signal s0 : t_a0;
  signal s1 : t_r1;
  signal s2 : t_a3;
  signal addr0_int : integer;
  signal addr1_int : integer;
  signal addr2_int : integer;
  signal addr3_int : integer;
  signal addr4_int : integer;

begin

  addr0_int <= conv_integer(unsigned(addr0));
  addr1_int <= conv_integer(unsigned(addr1));
  addr2_int <= conv_integer(unsigned(addr2));
  addr3_int <= conv_integer(unsigned(addr3)) +2;
  addr4_int <= conv_integer(unsigned(addr4)) +1;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s0(addr0_int).s0 <= i0;
      s0(addr0_int).s1 <= i1;
      s0(addr0_int).s2 <= i2;
      s0(addr0_int).s3 <= i3;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s1.s2(addr2_int) <= s0(addr0_int).s0;
      s1.s0 <= s0(addr0_int).s1;
      s1.s1 <= s0(addr0_int).s2;
      s1.s3(addr1_int) <= s0(addr0_int).s3;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s2(addr3_int, addr4_int).s0 <= s1.s2(addr2_int);
      s2(addr3_int, addr4_int).s1 <= s1.s0;
      s2(addr3_int, addr4_int).s2 <= s1.s1;
      s2(addr3_int, addr4_int).s3 <= s1.s3(addr1_int);
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      o0 <= s2(addr3_int, addr4_int).s0;
      o1 <= s2(addr3_int, addr4_int).s1;
      o2 <= s2(addr3_int, addr4_int).s2;
      o3 <= s2(addr3_int, addr4_int).s3;
    end if;
  end process;


end arch;
