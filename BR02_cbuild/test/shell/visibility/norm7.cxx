// Testing normalization of arrays of records

#include "libnorm7.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_norm7_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *clk = carbonFindNet(obj, "norm7.clk");
  CarbonNetID *o = carbonFindNet(obj, "norm7.o");
  CarbonNetID *we = carbonFindNet(obj, "norm7.we");
  CarbonNetID *sel = carbonFindNet(obj, "norm7.sel");

  CarbonDB *db = carbonGetDB(obj);

  const CarbonDBNode *arr = carbonDBFindNode(db, "norm7.arr");
  int index = 1;
  const CarbonDBNode *arr1 = carbonDBGetArrayElement(db, arr, &index, 1);
  const CarbonDBNode *b = carbonDBGetStructFieldByName(db, arr1, "b");

  CarbonNetID *net = carbonDBGetCarbonNet(db, b);

  const CarbonDBNode *b_alt = carbonDBFindNode(db, "norm7.arr[1].b");
  if (b_alt != b) {
    std::cout << "pointers don't match" << std::endl;
  }

  CarbonUInt32 val;
  CarbonTime t = 0;

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;

  val = 1;
  carbonDeposit(obj, sel, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;

  val = 1;
  carbonDeposit(obj, net, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;

  val = 0;
  carbonDeposit(obj, sel, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;

  carbonDBFree(db);
  carbonDestroy(&obj);
  return 0;
}
