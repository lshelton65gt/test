// Testing array types

#include "libarrayType1.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>
#include <cstdlib>


void printArrayType(CarbonDB *db, const CarbonDBNode *node)
{
  int numDims = carbonDBGetArrayNumDeclaredDims(db, node);
  int *indices = (int*) (malloc(numDims*sizeof(int)));
  int num_carbon_nets  = 1;

    
  std::cout << "array(";
  for (int i = 0; i < numDims; i++)
  {
    int left_bound = carbonDBGetArrayDimLeftBound(db, node, i);
    int right_bound = carbonDBGetArrayDimRightBound(db, node, i);
    if(left_bound > right_bound)
      std::cout << left_bound << " downto " << right_bound;
    else
      std::cout << left_bound << " to " << right_bound;

    if(i != numDims-1)
      std::cout << ", ";

    indices[i] = left_bound;
  }

  std::cout << ") of ";

  const CarbonDBNode *node_elem = carbonDBGetArrayElement(db, node, indices, numDims);
  bool isArray = carbonDBIsArray(db, node_elem);
  if(isArray)
    printArrayType(db, node_elem);
  else
    std::cout << carbonDBIntrinsicType(db, node_elem) << ";";

  free(indices);
}



int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_arrayType1_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonDB *db = carbonGetDB(obj);

  const CarbonDBNode *node_arr1 = carbonDBFindNode(db, "arraytype1.arr1");
  std::cout << "signal arr1 is ";
  printArrayType(db, node_arr1);
  std::cout << std::endl;

  const CarbonDBNode *node_arr2 = carbonDBFindNode(db, "arraytype1.arr2");
  std::cout << "signal arr2 is ";
  printArrayType(db, node_arr2);
  std::cout << std::endl;

  const CarbonDBNode *node_arr3 = carbonDBFindNode(db, "arraytype1.arr3");
  std::cout << "signal arr3 is ";
  printArrayType(db, node_arr3);
  std::cout << std::endl;

  const CarbonDBNode *node_arr4 = carbonDBFindNode(db, "arraytype1.arr4");
  std::cout << "signal arr4 is ";
  printArrayType(db, node_arr4);
  std::cout << std::endl;

  std::cout << std::endl;
  carbonDestroy(&obj);
  return 0;
}



