module ondemand3(clk, clk_en, rst0, rst1, bidi_clk, in, out);
   input [1:0] clk, clk_en;
   input rst0, rst1;
   inout [1:0] bidi_clk;
   input [3:0] in;
   output [3:0] out;

   reg [3:0]    count0, count1;

   assign        bidi_clk[0] = clk_en[0] ? clk[0] : 1'bz;
   assign        bidi_clk[1] = clk_en[1] ? clk[1] : 1'bz;
   pulldown pd[1:0] (bidi_clk);

   wire          bidi_clk_0 = bidi_clk[0];
   wire          bidi_clk_1 = bidi_clk[1];
   
   always @(posedge bidi_clk_0 or posedge rst0)
     if (rst0)
       count0 <= 4'b0;
     else
       count0 <= count0 + 4'h3;

   always @(posedge bidi_clk_1 or posedge rst1)
     if (rst1)
       count1 <= 4'b0;
     else
       count1 <= count1 + 4'h1;

   assign       out = count0 + count1 + in;

endmodule
