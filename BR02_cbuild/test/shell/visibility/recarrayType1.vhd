-- December 2008
-- This file is used to test composite types.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity recarrayType1 is
  
  port (
    clk   : in  std_logic
    );

end recarrayType1;

architecture arch of recarrayType1 is

  type subarr1 is array (15 downto 12) of std_logic;
  type subarr2 is array (5 to 8, 11 downto 9) of std_logic;
  type subarr3 is array (19 downto 13) of std_logic_vector(7 downto 4);

  type myrec is record
                  a : subarr1;
                  b : subarr2;
                  c : subarr3;
                  d : std_logic;
                  e : std_logic_vector(27 to 29);
                end record;

  type myarray1 is array (22 downto 12, 17 downto 1) of myrec;
  type myarray2 is array (25 downto 15) of myrec;
  type myarray3 is array (20 downto 18) of myarray2;

  signal comp1 : myarray1;   -- carbon observeSignal
  signal comp2 : myarray2;   -- carbon observeSignal
  signal comp3 : myarray3;   -- carbon observeSignal
begin

  
  process (clk)
  begin
    if  clk'event and clk = '1' then
    end if;
  end process;
  
end arch;
