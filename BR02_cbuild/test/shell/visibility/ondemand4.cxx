// Testing callbacks

#include "libondemand4.h"
#include <iostream>

static const char *modes[] = {"looking", "idle", "backoff"};

static void mode_change_cb(CarbonObjectID* context,
                           void* userContext,
                           CarbonOnDemandMode from,
                           CarbonOnDemandMode to,
                           CarbonTime simTime)
{
  std::cout << "On-demand entering " << modes[to] << " mode at time " << std::dec << simTime << std::endl;
}

static void run_cycles(CarbonUInt32 count, CarbonObjectID *obj, CarbonNetID *clk, CarbonTime &t)
{
  CarbonUInt32 val;
  for (CarbonUInt32 i = 0; i < (count * 2); ++i) {
    val = t & 0x1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, ++t);
  }
}

static void val_change_cb(CarbonObjectID *obj, CarbonNetID *net, CarbonClientData data, CarbonUInt32 *val, CarbonUInt32*)
{
  CarbonTime t = *(reinterpret_cast<CarbonTime*>(data));
  char name[100];
  carbonGetNetName(obj, net, name, 100);
  std::cout << name << " is " << std::dec << *val << " at time " << t << std::endl;
}

static void timestamp(const CarbonTime &t)
{
  std::cout << "TIME: " << std::dec << t << std::endl;
}

int main()
{
  carbonChangeMsgSeverity(0, 5181, eCarbonMsgSuppress);
  CarbonObjectID *obj = carbon_ondemand4_create(eCarbonFullDB, eCarbon_OnDemand);
  carbonOnDemandSetMaxStates(obj, 50);
  carbonOnDemandSetBackoffStrategy(obj, eCarbonOnDemandBackoffConstant);
  carbonOnDemandSetBackoffCount(obj, 50);
  carbonOnDemandAddModeChangeCB(obj, mode_change_cb, 0);
  carbonOnDemandEnableStats(obj);
  carbonOnDemandSetDebugLevel(obj, eCarbonOnDemandDebugAll);

  CarbonNetID *clk = carbonFindNet(obj, "ondemand4.clk");
  CarbonNetID *rst = carbonFindNet(obj, "ondemand4.rst");
  CarbonNetID *out3 = carbonFindNet(obj, "ondemand4.out[3]");
  CarbonNetID *out2 = carbonFindNet(obj, "ondemand4.out[2]");

  CarbonTime t = 0;
  CarbonUInt32 val;

  CarbonNetValueCBDataID *cb = carbonAddNetValueChangeCB(obj, val_change_cb, &t, out3);

  // reset

  val = 1;
  carbonDeposit(obj, rst, &val, 0);
  run_cycles(5, obj, clk, t);

  val = 0;
  carbonDeposit(obj, rst, &val, 0);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);

  // Disable the callback
  // Should not cause idle state to be exited
  timestamp(t);
  carbonDisableNetCB(obj, cb);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);

  // Enable the callback
  // Should not cause idle state to be exited
  timestamp(t);
  carbonEnableNetCB(obj, cb);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);

  // Disable the callback
  // Should not cause idle state to be exited
  timestamp(t);
  carbonDisableNetCB(obj, cb);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);

  // Register a new callback
  // Should cause idle state to be exited
  timestamp(t);
  CarbonNetValueCBDataID *cb2 = carbonAddNetValueChangeCB(obj, val_change_cb, &t, out2);

  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);

  // Disable both callbacks
  carbonDisableNetCB(obj, cb);
  carbonDisableNetCB(obj, cb2);

  // Run for a while, then reset to diverge
  run_cycles(50, obj, clk, t);
  val = 1;
  carbonDeposit(obj, rst, &val, 0);
  run_cycles(5, obj, clk, t);
  val = 0;
  carbonDeposit(obj, rst, &val, 0);

  // Run long enough to enter the looking state, but not to reach idle
  run_cycles(25, obj, clk, t);

  // Enable the callback
  // Should not cause search to be aborted
  timestamp(t);
  carbonEnableNetCB(obj, cb);
  
  // Run long enough to reach idle
  run_cycles(50, obj, clk, t);

  // Enable the callback
  // Should not cause idle mode to be exited
  timestamp(t);
  carbonEnableNetCB(obj, cb2);
  
  // Run long enough to reach idle
  run_cycles(50, obj, clk, t);

  carbonDestroy(&obj);
  return 0;
}
