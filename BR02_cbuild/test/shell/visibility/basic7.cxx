// Testing callbacks

#include "libbasic7.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

void printNet(CarbonObjectID* obj, CarbonNetID* net, CarbonClientData, CarbonUInt32* val, CarbonUInt32*)
{
  char name[100];
  carbonGetNetName(obj, net, name, 100);
  std::cout << "Net " << name << " changed value to " << std::hex << *val << std::endl;
}

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_basic7_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "basic7.clk");
  CarbonNetID *addr = carbonFindNet(obj, "basic7.addr");
  CarbonNetID *we = carbonFindNet(obj, "basic7.we");
  CarbonNetID *ain = carbonFindNet(obj, "basic7.ain");
  CarbonNetID *bin = carbonFindNet(obj, "basic7.bin");

  CarbonNetID *arr_3_a = carbonFindNet(obj, "basic7.arr[3].a");
  CarbonNetID *arr_3_b = carbonFindNet(obj, "basic7.arr[3].b");
  CarbonNetID *arr_3_b_5 = carbonFindNet(obj, "basic7.arr[3].b[5]");

  carbonAddNetValueChangeCB(obj, printNet, 0, arr_3_a);
  carbonAddNetValueChangeCB(obj, printNet, 0, arr_3_b);
  carbonAddNetValueChangeCB(obj, printNet, 0, arr_3_b_5);

  CarbonUInt32 val;
  CarbonTime t = 0;

  std::cout << "Writing to address 2" << std::endl;

  val = 1;
  carbonDeposit(obj, we, &val, 0);
  val = 2;
  carbonDeposit(obj, addr, &val, 0);
  val = 1;
  carbonDeposit(obj, ain, &val, 0);
  val = 0xff;
  carbonDeposit(obj, bin, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  std::cout << "Writing to address 3" << std::endl;

  val = 3;
  carbonDeposit(obj, addr, &val, 0);
  val = 1;
  carbonDeposit(obj, ain, &val, 0);
  val = 0xf;
  carbonDeposit(obj, bin, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  std::cout << "Writing to address 3 again" << std::endl;

  val = 0;
  carbonDeposit(obj, ain, &val, 0);
  val = 0xf0;
  carbonDeposit(obj, bin, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonDestroy(&obj);
  return 0;
}
