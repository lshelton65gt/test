// Testing force/release

#include "libbasic9.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_basic9_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *clk = carbonFindNet(obj, "basic9.clk");
  CarbonNetID *a = carbonFindNet(obj, "basic9.a");
  CarbonNetID *b = carbonFindNet(obj, "basic9.b");
  CarbonNetID *addr = carbonFindNet(obj, "basic9.addr");
  CarbonNetID *we = carbonFindNet(obj, "basic9.we");
  CarbonNetID *o = carbonFindNet(obj, "basic9.o");

  CarbonNetID *arr_4_b = carbonFindNet(obj, "basic9.arr[4].b");
  CarbonNetID *arr_5_b = carbonFindNet(obj, "basic9.arr[5].b");
  CarbonNetID *arr_6_b = carbonFindNet(obj, "basic9.arr[6].b");
  CarbonNetID *arr_7_b = carbonFindNet(obj, "basic9.arr[7].b");

  CarbonUInt32 val;
  CarbonTime t = 0;

  // Read all the addresses
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is " << val << std::endl;
  }

  // Force the signals
  val = 1;
  carbonForce(obj, arr_4_b, &val);
  val = 0;
  carbonForce(obj, arr_5_b, &val);
  val = 1;
  carbonForce(obj, arr_6_b, &val);
  val = 0;
  carbonForce(obj, arr_7_b, &val);

  // Read all the addresses
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is " << val << std::endl;
  }

  // Write 1s to all the addresses
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  carbonDeposit(obj, b, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  // Read all the addresses
  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is " << val << std::endl;
  }

  // Release the signals
  carbonRelease(obj, arr_4_b);
  carbonRelease(obj, arr_5_b);
  carbonRelease(obj, arr_6_b);
  carbonRelease(obj, arr_7_b);

  // Write 1s to all the addresses
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  carbonDeposit(obj, b, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  // Read all the addresses
  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is " << val << std::endl;
  }

  carbonDestroy(&obj);
  return 0;
}
