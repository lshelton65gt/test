package base is

   type myType is record
                    a : bit;
                    b : bit;
   end record;

end base;

use work.base.all;

entity bug8468 is
   port ( --clk   : in  bit;
          pin1  : in  myType;
          pout1 : out myType
          );
end bug8468;


use work.base.all;

architecture bug8468 of bug8468 is

   signal tmp1a, tmp1b : bit;
   
begin

  tmp1a <= pin1.a;
  tmp1b <= pin1.b;
  pout1.a <= tmp1a;
  pout1.b <= tmp1b;

end bug8468;
