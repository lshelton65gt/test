// Testing simple nested records

#include "libbasic1.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_basic1_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *clk = carbonFindNet(obj, "basic1.clk");
  CarbonNetID *o = carbonFindNet(obj, "basic1.o");
  CarbonNetID *we = carbonFindNet(obj, "basic1.we");
  CarbonNetID *sel = carbonFindNet(obj, "basic1.sel");

  CarbonDB *db = carbonGetDB(obj);

  const CarbonDBNode *r = carbonDBFindNode(db, "basic1.r");
  const CarbonDBNode *sel1 = carbonDBGetStructFieldByName(db, r, "sel1");
  const CarbonDBNode *b = carbonDBGetStructFieldByName(db, sel1, "b");

  CarbonNetID *net = carbonDBGetCarbonNet(db, b);

  CarbonUInt32 val;
  CarbonTime t = 0;

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;

  val = 1;
  carbonDeposit(obj, sel, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;

  val = 1;
  carbonDeposit(obj, net, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;

  val = 0;
  carbonDeposit(obj, sel, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;

  carbonDBFree(db);
  carbonDestroy(&obj);
  return 0;
}
