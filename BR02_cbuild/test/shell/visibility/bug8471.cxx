#include "carbon/carbon_dbapi.h"
#include <iostream>

int main(int argc, char **argv)
{
  CarbonDB* db = carbonDBCreate("libbug8471.symtab.db", 1);

  CarbonDBNodeIter* iter;
  const CarbonDBNode* node;

  iter = carbonDBLoopAsyncs(db);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    ;
  }
  carbonDBFreeNodeIter(iter);

  iter = carbonDBLoopAsyncOutputs(db);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    ;
  }
  carbonDBFreeNodeIter(iter);

  carbonDBFree(db);
  return 0;
}
