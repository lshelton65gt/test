package mypack is
  type write_state is (read, write);
end mypack;

library ieee;
use ieee.std_logic_1164.all;
use work.mypack.all;

entity dbapi1 is
  
  port (
    clk  : in  std_logic;
    we   : in  std_logic;
    sel  : in  std_logic;
    din  : in  std_logic_vector (1 downto 0);
    dout : out std_logic_vector (1 downto 0);
    ws   : out write_state);

end dbapi1;

architecture arch of dbapi1 is
  type myrecord is record
                     a: std_logic;
                     b: std_logic;
                   end record;

  type myarray is array (1 downto 0) of myrecord;

  signal arr: myarray;

begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        if sel = '1' then
          arr(1).a <= din(1);
          arr(1).b <= din(0);
        else
          arr(0).a <= din(1);
          arr(0).b <= din(0);
        end if;
        ws <= write;
      else
        if sel = '1' then
          dout(1) <= arr(1).a;
          dout(0) <= arr(1).b;
        else
          dout(1) <= arr(0).a;
          dout(0) <= arr(0).b;
        end if;
        ws <= read;
      end if;
    end if;
  end process;
  
end arch;
