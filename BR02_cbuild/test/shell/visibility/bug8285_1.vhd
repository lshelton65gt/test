library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package mypack is
  type t_a0 is array (-13 to -1, 14 downto -14) of std_ulogic;
  type t_r0 is record
    s0 : t_a0;
  end record;
  type t_r1 is record
    s0 : t_r0;
  end record;
  type t_a1 is array (-6 to -1) of std_ulogic;
  type t_r2 is record
    s0 : t_a1;
  end record;
  type t_a2 is array (-10 downto -13, 7 downto -15) of std_ulogic;
  type t_a3 is array (-2 downto -3, 13 downto 5) of t_a2;
  type t_r3 is record
    s0 : t_a3;
  end record;
end mypack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

use work.mypack.all;

entity bug8285_1 is
  port (
    clk : in std_logic;
    i0 : in std_ulogic;
    addr0 : in std_logic_vector (3 downto 0);
    addr1 : in std_logic_vector (4 downto 0);
    addr2 : in std_logic_vector (2 downto 0);
    addr3 : in std_logic_vector (0 to 0);
    addr4 : in std_logic_vector (3 downto 0);
    addr5 : in std_logic_vector (1 downto 0);
    addr6 : in std_logic_vector (4 downto 0);
    o0 : out std_ulogic);
end bug8285_1;

architecture arch of bug8285_1 is

  signal s0 : t_r1;
  signal s1 : t_r2;
  signal s2 : t_r3;
  signal addr0_int : integer;
  signal addr1_int : integer;
  signal addr2_int : integer;
  signal addr3_int : integer;
  signal addr4_int : integer;
  signal addr5_int : integer;
  signal addr6_int : integer;

begin

  addr0_int <= conv_integer(unsigned(addr0)) -13;
  addr1_int <= conv_integer(unsigned(addr1)) -14;
  addr2_int <= conv_integer(unsigned(addr2)) -6;
  addr3_int <= conv_integer(unsigned(addr3)) -3;
  addr4_int <= conv_integer(unsigned(addr4)) +5;
  addr5_int <= conv_integer(unsigned(addr5)) -13;
  addr6_int <= conv_integer(unsigned(addr6)) -15;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s0.s0.s0(addr0_int, addr1_int) <= i0;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s1.s0(addr2_int) <= s0.s0.s0(addr0_int, addr1_int);
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s2.s0(addr3_int, addr4_int)(addr5_int, addr6_int) <= s1.s0(addr2_int);
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      o0 <= s2.s0(addr3_int, addr4_int)(addr5_int, addr6_int);
    end if;
  end process;


end arch;
