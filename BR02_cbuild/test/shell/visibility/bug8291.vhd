library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package mypack is
  type t_r0 is record
    s0 : std_ulogic;
  end record;
  type t_a0 is array (7 downto -8, -5 downto -15) of t_r0;
  type t_a1 is array (10 to 14, 11 to 15) of std_ulogic;
  type t_r1 is record
    s0 : t_a1;
  end record;
  type t_r2 is record
    s0 : t_r1;
  end record;
  type t_a2 is array (-5 to 9) of t_r2;
end mypack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

use work.mypack.all;

entity bug8291 is
  port (
    clk : in std_logic;
    i0 : in std_ulogic;
    addr0 : in std_logic_vector (3 downto 0);
    addr1 : in std_logic_vector (3 downto 0);
    addr2 : in std_logic_vector (3 downto 0);
    addr3 : in std_logic_vector (2 downto 0);
    addr4 : in std_logic_vector (2 downto 0);
    o0 : out std_ulogic);
end bug8291;

architecture arch of bug8291 is

  signal s0 : t_a0;
  signal s1 : t_a2;
  signal addr0_int : integer;
  signal addr1_int : integer;
  signal addr2_int : integer;
  signal addr3_int : integer;
  signal addr4_int : integer;

begin

  addr0_int <= conv_integer(unsigned(addr0)) -8;
  addr1_int <= conv_integer(unsigned(addr1)) -15;
  addr2_int <= conv_integer(unsigned(addr2)) -5;
  addr3_int <= conv_integer(unsigned(addr3)) +10;
  addr4_int <= conv_integer(unsigned(addr4)) +11;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s0(addr0_int, addr1_int).s0 <= i0;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s1(addr2_int).s0.s0(addr3_int, addr4_int) <= s0(addr0_int, addr1_int).s0;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      o0 <= s1(addr2_int).s0.s0(addr3_int, addr4_int);
    end if;
  end process;


end arch;
