library ieee;
use ieee.std_logic_1164.all;

entity waveorder1 is
  port (
    clk : in  std_logic;
    i   : in  std_logic_vector (3 downto 0);
    o   : out std_logic);
end waveorder1;

architecture arch of waveorder1 is

  type myrec is record
                  z : std_logic;
                  w : std_logic;
                  y : std_logic;
                  x : std_logic;
                end record;
  signal r : myrec;     -- carbon observeSignal

begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      r.z <= i(0);
      r.w <= i(1);
      r.y <= i(2);
      r.x <= i(3);
      o <= r.w and r.x and r.y and r.z;
    end if;
  end process;
  
end arch;
