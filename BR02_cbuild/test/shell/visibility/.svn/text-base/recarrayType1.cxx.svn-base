// Testing composite types

#include "librecarrayType1.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>
#include <cstdlib>

void printCompositeType(CarbonDB *db, const CarbonDBNode *node);

void printSpaces(int spaces)
{
  for(int i = 0; i < spaces; i++)
  {
    std::cout << " ";
  }
}

void printArrayType(CarbonDB *db, const CarbonDBNode *node)
{
  int numDims = carbonDBGetArrayNumDeclaredDims(db, node);
  int *indices = (int*) (malloc(numDims*sizeof(int)));
  int num_carbon_nets  = 1;

    
  std::cout << "array(";
  for (int i = 0; i < numDims; i++)
  {
    int left_bound = carbonDBGetArrayDimLeftBound(db, node, i);
    int right_bound = carbonDBGetArrayDimRightBound(db, node, i);
    if(left_bound > right_bound)
      std::cout << left_bound << " downto " << right_bound;
    else
      std::cout << left_bound << " to " << right_bound;

    if(i != numDims-1)
      std::cout << ", ";

    indices[i] = left_bound;
  }

  std::cout << ") of ";

  const CarbonDBNode *node_elem = carbonDBGetArrayElement(db, node, indices, numDims);
  printCompositeType(db, node_elem);  

  free(indices);
}

void printStructType(CarbonDB *db, const CarbonDBNode *node)
{
  static int spaces = 0;
  const CarbonDBNode* fieldNode;

  spaces += 2;
  std::cout << std::endl;
  printSpaces(spaces);
  std::cout << "record\n";

  CarbonDBNodeIter* iter = carbonDBLoopChildren(db, node);
  while ((fieldNode = carbonDBNodeIterNext(iter)) != NULL) 
  {

    spaces += 2;
    const char* fieldName = carbonDBNodeGetLeafName(db, fieldNode);
    printSpaces(spaces);
    std::cout << fieldName << " is ";
    printCompositeType(db, fieldNode);
    std::cout << std::endl;
    spaces -= 2;
  }
  carbonDBFreeNodeIter(iter);

  printSpaces(spaces);
  std::cout << "end record;" << std::endl;
  spaces -= 2;
}

void printCompositeType(CarbonDB *db, const CarbonDBNode *node)
{
  bool isStruct = carbonDBIsStruct(db, node);
  bool isArray = carbonDBIsArray(db, node);
  if(isStruct)
    printStructType(db, node);
  else if(isArray)
    printArrayType(db, node);
  else
    std::cout << carbonDBIntrinsicType(db, node) << ";";
}


int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_recarrayType1_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonDB *db = carbonGetDB(obj);

  const CarbonDBNode *node_comp1 = carbonDBFindNode(db, "recarraytype1.comp1");
  std::cout << "signal comp1 is ";
  printCompositeType(db, node_comp1);
  std::cout << std::endl;

  const CarbonDBNode *node_comp2 = carbonDBFindNode(db, "recarraytype1.comp2");
  std::cout << "signal comp2 is ";
  printCompositeType(db, node_comp2);
  std::cout << std::endl;

  const CarbonDBNode *node_comp3 = carbonDBFindNode(db, "recarraytype1.comp3");
  std::cout << "signal comp3 is ";
  printCompositeType(db, node_comp3);
  std::cout << std::endl;

  std::cout << std::endl;
  carbonDestroy(&obj);
  return 0;
}



