// Testing wildcard matching

#include "libdbapi12.h"
#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <iostream>
#include <vector>

void printMatching(CarbonDB* db, const char* matchString)
{
  std::cout << "Nodes matching " << matchString << ":" << std::endl;

  CarbonDBNodeIter* iter = carbonDBLoopMatching(db, matchString);
  const CarbonDBNode* node;
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    const char* name = carbonDBNodeGetFullName(db, node);
    std::cout << "  " << name << std::endl;
  }
  carbonDBFreeNodeIter(iter);
} 

int main()
{
  CarbonObjectID* obj = carbon_dbapi12_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonDB* db = carbonGetDB(obj);

  // This should match exactly the specified node
  printMatching(db, "dbapi12.sub1.y");
  // This should match the signals and the record in the instance
  printMatching(db, "dbapi12.sub1.*");
  // This should match the two instances and the four signals in the top scope
  printMatching(db, "dbapi12.sub*");
  // This should match only the two instances
  printMatching(db, "dbapi12.sub?");
  // This should match sub1zz (but not the z in the sub1 instance)
  printMatching(db, "dbapi12.sub1?z");
  // This should match all x except the one in the record
  printMatching(db, "*.x");
  // This should match the y in each instance
  printMatching(db, "sub.y");
  // This should match nothing
  printMatching(db, "fake.*");
  // This should match nothing
  printMatching(db, "*.fake");


  carbonDestroy(&obj);
  return 0;
}
