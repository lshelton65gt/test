module basic8_verilog(input clk,
                      input en,
                      input [11:0] i,
                      output reg [11:0] o);

   reg                       a;
   reg [0:0]                 b;
   reg [1:1]                 c;
   reg [2:2]                 d [1:1];
   reg [5:2]                 e [1:1];
   reg [1:1]                 f [2:5];

   always @(posedge clk)
     if (en) begin
        a <= i[0];
        b[0] <= i[1];
        c[1] <= i[2];
        d[1][2] <= i[3];
        e[1] <= i[7:4];
        f[2][1] <= i[8];
        f[3][1] <= i[9];
        f[4][1] <= i[10];
        f[5][1] <= i[11];
     end
     else begin
        o[0] <= a;
        o[1] <= b[0];
        o[2] <= c[1];
        o[3] <= d[1][2];
        o[7:4] <= e[1];
        o[8] <= f[2][1];
        o[9] <= f[3][1];
        o[10] <= f[4][1];
        o[11] <= f[5][1];
     end
   
endmodule
