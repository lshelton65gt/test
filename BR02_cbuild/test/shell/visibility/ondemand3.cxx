// Testing bidis/tristates

#include "libondemand3.h"
#include <iostream>

static const char *modes[] = {"looking", "idle", "backoff"};

static void mode_change_cb(CarbonObjectID* context,
                           void* userContext,
                           CarbonOnDemandMode from,
                           CarbonOnDemandMode to,
                           CarbonTime simTime)
{
  std::cout << "On-demand entering " << modes[to] << " mode at time " << std::dec << simTime << std::endl;
}

static void run_cycles(CarbonUInt32 count, CarbonObjectID *obj, CarbonNetID *clk, CarbonTime &t)
{
  CarbonUInt32 val;
  for (CarbonUInt32 i = 0; i < (count * 2); ++i) {
    val = t & 0x1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, ++t);
  }
}

static void run_cycles_2(CarbonUInt32 count, CarbonObjectID *obj, CarbonNetID *clk1, CarbonNetID *clk2, CarbonTime &t)
{
  CarbonUInt32 val;
  for (CarbonUInt32 i = 0; i < (count * 2); ++i) {
    val = t & 0x1;
    carbonDeposit(obj, clk1, &val, 0);
    carbonDeposit(obj, clk2, &val, 0);
    carbonSchedule(obj, ++t);
  }
}

static void print(CarbonObjectID *obj, CarbonNetID *net, const CarbonTime &t)
{
  char name[100];
  CarbonUInt32 val;
  carbonGetNetName(obj, net, name, 100);
  carbonExamine(obj, net, &val, 0);
  
  std::cout << name << " = " << std::hex << val << " at time " << std::dec << t << std::endl;
}

static void timestamp(const CarbonTime &t)
{
  std::cout << "TIME: " << std::dec << t << std::endl;
}

int main()
{
  CarbonObjectID *obj = carbon_ondemand3_create(eCarbonFullDB, eCarbon_OnDemand);
  carbonOnDemandSetMaxStates(obj, 50);
  carbonOnDemandSetBackoffStrategy(obj, eCarbonOnDemandBackoffConstant);
  carbonOnDemandSetBackoffCount(obj, 50);
  carbonOnDemandAddModeChangeCB(obj, mode_change_cb, 0);
  carbonOnDemandEnableStats(obj);
  carbonOnDemandSetDebugLevel(obj, eCarbonOnDemandDebugAll);

  CarbonNetID *clk = carbonFindNet(obj, "ondemand3.clk[1]");
  CarbonNetID *rst0 = carbonFindNet(obj, "ondemand3.rst0");
  CarbonNetID *rst1 = carbonFindNet(obj, "ondemand3.rst1");
  CarbonNetID *clk_en = carbonFindNet(obj, "ondemand3.clk_en[1]");
  CarbonNetID *bidi_clk = carbonFindNet(obj, "ondemand3.bidi_clk[1]");
  CarbonNetID *in = carbonFindNet(obj, "ondemand3.in");
  CarbonNetID *out = carbonFindNet(obj, "ondemand3.out");

  CarbonTime t = 0;
  CarbonUInt32 val, drive;

//   carbonDumpVars(carbonWaveInitFSDB(obj, "ondemand3.fsdb", e1ns), 0, "ondemand3");

  // reset

  val = 1;
  carbonDeposit(obj, rst0, &val, 0);
  carbonDeposit(obj, rst1, &val, 0);
  val = 0x3;
  carbonDeposit(obj, in, &val, 0);
  run_cycles(5, obj, bidi_clk, t);

  val = 0;
  carbonDeposit(obj, rst1, &val, 0);
  run_cycles(5, obj, bidi_clk, t);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, bidi_clk, t);
  print(obj, out, t);

  // carbonDeposit() with non-null drive
  // Idle mode should be exited.
  val = 0;
  drive = 0;
  carbonDeposit(obj, bidi_clk, &val, &drive);
  carbonSchedule(obj, ++t);
  val = 1;
  drive = 0;
  carbonDeposit(obj, bidi_clk, &val, &drive);
  carbonSchedule(obj, ++t);

  // Run long enough to get in an on-demand state
  run_cycles(50, obj, bidi_clk, t);
  print(obj, out, t);

  // carbonDeAssertXdrive()
  // Idle mode should be exited.
  timestamp(t);
  carbonDeAssertXdrive(obj, bidi_clk);
  // Run some more cycles.  The internal clock won't
  // be running because the enable is off, so the
  // counter shouldn't advance.
  run_cycles(5, obj, clk, t);
  print(obj, out, t);
  // Enable the internal clock, and run some more.
  // The counter should advance.
  val = 1;
  carbonDeposit(obj, clk_en, &val, 0);
  run_cycles(5, obj, clk, t);
  print(obj, out, t);
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  print(obj, out, t);

  // Examine the bidi clk.
  // Idle mode should not be exited.
  timestamp(t);
  carbonExamine(obj, bidi_clk, &val, &drive);
  std::cout << "bidi_clk: val = " << std::hex << val << " drive = " << drive << std::endl;
  // Get the external drive.
  // Idle mode should not be exited.
  drive = 0xffffffff;
  carbonGetExternalDrive(obj, bidi_clk, &drive);
  std::cout << "bidi_clk: xdrive = " << std::hex << drive << std::endl;
  // Check for drive conflict
  // Idle mode should not be exited.
  int drive_conflict = carbonHasDriveConflict(obj, bidi_clk);
  std::cout << "bidi_clk: drive_conflict = " << std::hex << drive_conflict << std::endl;
  
  // Start driving both clocks, creating a drive conflict.
  // Give them the same values so the counter output is
  // predictable.
  // This breaks out of idle mode because the stimulus
  // is different, so run until we get back

  run_cycles_2(50, obj, clk, bidi_clk, t);
  print(obj, out, t);

  // Examine the bidi clk.
  // Idle mode should not be exited.
  timestamp(t);
  carbonExamine(obj, bidi_clk, &val, &drive);
  std::cout << "bidi_clk: val = " << std::hex << val << " drive = " << drive << std::endl;
  // Get the external drive.
  // Idle mode should not be exited.
  drive = 0xffffffff;
  carbonGetExternalDrive(obj, bidi_clk, &drive);
  std::cout << "bidi_clk: xdrive = " << std::hex << drive << std::endl;
  // Check for drive conflict
  // Idle mode should not be exited.
  drive_conflict = carbonHasDriveConflict(obj, bidi_clk);
  std::cout << "bidi_clk: drive_conflict = " << std::hex << drive_conflict << std::endl;

  // Resolve the drive conflict
  // Idle mode should not be exited.
  timestamp(t);
  carbonResolveBidi(obj, bidi_clk);

  // Examine the bidi clk.
  // Idle mode should not be exited.
  timestamp(t);
  carbonExamine(obj, bidi_clk, &val, &drive);
  std::cout << "bidi_clk: val = " << std::hex << val << " drive = " << drive << std::endl;
  // Get the external drive.
  // Idle mode should not be exited.
  drive = 0xffffffff;
  carbonGetExternalDrive(obj, bidi_clk, &drive);
  std::cout << "bidi_clk: xdrive = " << std::hex << drive << std::endl;
  // Check for drive conflict
  // Idle mode should not be exited.
  drive_conflict = carbonHasDriveConflict(obj, bidi_clk);
  std::cout << "bidi_clk: drive_conflict = " << std::hex << drive_conflict << std::endl;

  carbonDestroy(&obj);
  return 0;
}
