#include "libbug8452_2.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <cassert>
#include <iostream>

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_bug8452_2_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *pin2 = carbonFindNet(obj, "bug8452_2.pin1[2]");
  CarbonNetID *pout2 = carbonFindNet(obj, "bug8452_2.pout1[2]");

  CarbonUInt32 val = 1;
  carbonDeposit(obj, pin2, &val, 0);

  carbonSchedule(obj, 0);

  char valStr[32];
  carbonFormat(obj, pout2, valStr, 32, eCarbonBin);
  std::cout << "pout[2] is " << valStr << std::endl;

  carbonDestroy(&obj);
  return 0;
}
