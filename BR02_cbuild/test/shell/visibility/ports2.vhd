library ieee;
use ieee.std_logic_1164.all;

entity ports2 is
  port (
    clk : in  std_logic_vector (7 downto 0);
    i   : in  std_logic_vector (7 downto 0);
    o   : out std_logic_vector (7 downto 0));
end ports2;

architecture arch of ports2 is

  signal clk0, clk1, clk2, clk3, clk4, clk5, clk6, clk7 : std_logic;

begin

  clk0 <= clk(0);
  clk1 <= clk(1);
  clk2 <= clk(2);
  clk3 <= clk(3);
  clk4 <= clk(4);
  clk5 <= clk(5);
  clk6 <= clk(6);
  clk7 <= clk(7);


  process (clk0)
  begin
    if clk0'event and clk0 = '1' then
      o(0) <= i(0);
    end if;
  end process;

  process (clk1)
  begin
    if clk1'event and clk1 = '1' then
      o(1) <= i(1);
    end if;
  end process;

  process (clk2)
  begin
    if clk2'event and clk2 = '1' then
      o(2) <= i(2);
    end if;
  end process;

  process (clk3)
  begin
    if clk3'event and clk3 = '1' then
      o(3) <= i(3);
    end if;
  end process;

  process (clk4)
  begin
    if clk4'event and clk4 = '1' then
      o(4) <= i(4);
    end if;
  end process;

  process (clk5)
  begin
    if clk5'event and clk5 = '1' then
      o(5) <= i(5);
    end if;
  end process;

  process (clk6)
  begin
    if clk6'event and clk6 = '1' then
      o(6) <= i(6);
    end if;
  end process;

  process (clk7)
  begin
    if clk7'event and clk7 = '1' then
      o(7) <= i(7);
    end if;
  end process;
  
end arch;
