-- December 2008
-- This file is used to test array types.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity arrayType1 is
  
  port (
    clk  : in  std_logic);

end arrayType1;

architecture arch of arrayType1 is

  type subarr1 is array (15 downto 12) of std_logic;
  type subarr2 is array (5 to 8, 15 downto 12) of std_logic;
  type subarr3 is array (15 downto 12) of std_logic_vector(7 downto 4);
  type myarr1 is array (7 to 10, 9 downto 8) of subarr1;
  type myarr2 is array (7 to 10) of subarr2;
  type myarr3 is array (7 to 10) of subarr3;
  type myarr4 is array (7 to 10, 9 downto 8) of subarr3;
  signal arr1 : myarr1;                   -- carbon observeSignal
  signal arr2 : myarr2;                   -- carbon observeSignal
  signal arr3 : myarr3;                   -- carbon observeSignal
  signal arr4 : myarr4;                   -- carbon observeSignal
  
begin
  
  process (clk)
  begin
    if clk'event and clk = '1' then
    end if;
  end process;

end arch;
