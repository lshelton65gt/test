library ieee;
use ieee.std_logic_1164.all;

entity basic8 is
  port (
    clk  : in std_logic;
    en   : in std_logic;
    i    : in std_logic_vector (11 downto 0);
    o    : out std_logic_vector (11 downto 0));
end basic8;

architecture arch of basic8 is

  signal a : std_logic;
  signal b : std_logic_vector (0 downto 0);
  signal c : std_logic_vector (1 to 1);
  type d_array is array (1 to 1) of std_logic_vector (2 downto 2);
  signal d : d_array;
  type e_array is array (1 downto 1) of std_logic_vector (5 downto 2);
  signal e : e_array;
  type f_array is array (2 to 5) of std_logic_vector (1 to 1);
  signal f : f_array;

begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if en = '1' then
        a <= i(0);
        b(0) <= i(1);
        c(1) <= i(2);
        d(1)(2) <= i(3);
        e(1) <= i(7 downto 4);
        f(2)(1) <= i(8);
        f(3)(1) <= i(9);
        f(4)(1) <= i(10);
        f(5)(1) <= i(11);
      end if;
      o(0) <= a;
      o(1) <= b(0);
      o(2) <= c(1);
      o(3) <= d(1)(2);
      o(7 downto 4) <= e(1);
      o(8) <= f(2)(1);
      o(9) <= f(3)(1);
      o(10) <= f(4)(1);
      o(11) <= f(5)(1);
    end if;
  end process;
  
end arch;
