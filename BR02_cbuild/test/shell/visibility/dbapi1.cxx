// Testing simple arrays and structures

#include "libdbapi1.h"
#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <cstring>
#include <iostream>

int main(int argc, char** argv)
{
  bool standalone = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-standalone") == 0)
      standalone = true;
  }

  CarbonObjectID *obj = NULL;
  CarbonDB *db;
  if (standalone) {
    db = carbonDBCreate("libdbapi1.symtab.db", 1);
  } else {
    obj = carbon_dbapi1_create(eCarbonFullDB, eCarbon_NoFlags);
    db = carbonGetDB(obj);
  }

  // Get a handle to the array
  const CarbonDBNode *arr = carbonDBFindNode(db, "dbapi1.arr");
  assert(carbonDBIsArray(db, arr) == 1);
  assert(carbonDBGetArrayDims(db, arr) == 1);
  assert(carbonDBGetArrayLeftBound(db, arr) == 1);
  assert(carbonDBGetArrayRightBound(db, arr) == 0);
  assert(carbonDBCanBeCarbonNet(db, arr) == 0);
  assert(carbonDBGetBitSize(db, arr) == 4);
  assert(carbonDBGetWidth(db, arr) == 0);
  std::cout << "Node " << carbonDBNodeGetLeafName(db, arr) << " is declared in file " << carbonDBGetSourceFile(db, arr) << ", line " << carbonDBGetSourceLine(db, arr) << std::endl;
  assert(carbonDBIsContainedByComposite(db, arr) == 0);

  // Get a handle to element 1 of the array, which is a struct
  int index = 1;
  const CarbonDBNode *arr1 = carbonDBGetArrayElement(db, arr, &index, 1);
  assert(carbonDBIsStruct(db, arr1) == 1);
  assert(carbonDBGetNumStructFields(db, arr1) == 2);
  assert(carbonDBCanBeCarbonNet(db, arr1) == 0);
  assert(carbonDBGetBitSize(db, arr1) == 2);
  assert(carbonDBGetWidth(db, arr1) == 0);
  std::cout << "Node " << carbonDBNodeGetLeafName(db, arr1) << " is declared in file " << carbonDBGetSourceFile(db, arr1) << ", line " << carbonDBGetSourceLine(db, arr1) << std::endl;
  assert(carbonDBIsContainedByComposite(db, arr1) == 1);

  // Get a handle to field "b"
  const CarbonDBNode *b = carbonDBGetStructFieldByName(db, arr1, "b");
  
  // Get a CarbonNetID for the node, and deposit to it.
  assert(carbonDBCanBeCarbonNet(db, b) == 1);
  assert(carbonDBGetBitSize(db, b) == 1);
  assert(carbonDBGetWidth(db, b) == 1);
  std::cout << "Node " << carbonDBNodeGetLeafName(db, b) << " is declared in file " << carbonDBGetSourceFile(db, b) << ", line " << carbonDBGetSourceLine(db, b) << std::endl;
  assert(carbonDBIsContainedByComposite(db, b) == 1);
  CarbonNetID *net = carbonDBGetCarbonNet(db, b);
  if (standalone) {
    assert(net == NULL);
  } else {
    assert(net != NULL);
  }

  // Test isEnum()
  const CarbonDBNode *ws = carbonDBFindNode(db, "dbapi1.ws");
  assert(carbonDBIsEnum(db, ws) == 1);

  carbonDBFree(db);
  if (!standalone) {
    carbonDestroy(&obj);
  }
  return 0;
}
