// Testing legacy API functions

#include "libdbapi6.h"
#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <cstring>
#include <iostream>

int main(int argc, char** argv)
{
  bool standalone = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-standalone") == 0)
      standalone = true;
  }

  CarbonObjectID *obj = NULL;
  CarbonDB *db;
  if (standalone) {
    db = carbonDBCreate("libdbapi6.symtab.db", 1);
  } else {
    obj = carbon_dbapi6_create(eCarbonFullDB, eCarbon_NoFlags);
    db = carbonGetDB(obj);
  }

  // 
  // Get a handle to the array
  const CarbonDBNode *arr = carbonDBFindNode(db, "dbapi6.arr");
  // Make sure the legacy children API functions work
  const CarbonDBNode *arr_3 = carbonDBFindChild(db, arr, "3");
  assert(arr_3 != NULL);
  std::cout << carbonDBNodeGetFullName(db, arr_3) << " is declared as " << carbonDBdeclarationType(db, arr_3);
  std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, arr_3) << ")" << std::endl;
  const CarbonDBNode *arr_3_1 = carbonDBFindChild(db, arr_3, "1");
  assert(arr_3_1 != NULL);
  std::cout << carbonDBNodeGetFullName(db, arr_3_1) << " is declared as " << carbonDBdeclarationType(db, arr_3_1);
  std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, arr_3_1) << ")" << std::endl;
  const CarbonDBNode *arr_3_1_r = carbonDBFindChild(db, arr_3_1, "r");
  assert(arr_3_1_r != NULL);
  std::cout << carbonDBNodeGetFullName(db, arr_3_1_r) << " is declared as " << carbonDBdeclarationType(db, arr_3_1_r);  std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, arr_3_1_r) << ")" << std::endl;
  const CarbonDBNode *arr_3_1_r_y = carbonDBFindChild(db, arr_3_1_r, "y");
  assert(arr_3_1_r_y != NULL);
  std::cout << carbonDBNodeGetFullName(db, arr_3_1_r_y) << " is declared as " << carbonDBdeclarationType(db, arr_3_1_r_y);
  std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, arr_3_1_r_y) << ")" << std::endl;
  const CarbonDBNode *arr_3_1_r_y_5 = carbonDBFindChild(db, arr_3_1_r_y, "5");
  assert(arr_3_1_r_y_5 != NULL);
  std::cout << carbonDBNodeGetFullName(db, arr_3_1_r_y_5) << " is declared as " << carbonDBdeclarationType(db, arr_3_1_r_y_5);
  std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, arr_3_1_r_y_5) << ")" << std::endl;

  // Loop over all the array children
  CarbonDatabaseNodeIter *iter;

  iter = carbonDBLoopChildren(db, arr);
  const CarbonDBNode *node;
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    std::cout << carbonDBNodeGetFullName(db, node) << " is declared as " << carbonDBdeclarationType(db, node);
    std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, node) << ")" << std::endl;
  }
  carbonDBFreeNodeIter(iter);
  
  iter = carbonDBLoopChildren(db, arr_3);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    std::cout << carbonDBNodeGetFullName(db, node) << " is declared as " << carbonDBdeclarationType(db, node);
    std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, node) << ")" << std::endl;
  }
  carbonDBFreeNodeIter(iter);

  iter = carbonDBLoopChildren(db, arr_3_1);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    std::cout << carbonDBNodeGetFullName(db, node) << " is declared as " << carbonDBdeclarationType(db, node);
    std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, node) << ")" << std::endl;
  }
  carbonDBFreeNodeIter(iter);

  iter = carbonDBLoopChildren(db, arr_3_1_r);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    std::cout << carbonDBNodeGetFullName(db, node) << " is declared as " << carbonDBdeclarationType(db, node);
    std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, node) << ")" << std::endl;
  }
  carbonDBFreeNodeIter(iter);

  iter = carbonDBLoopChildren(db, arr_3_1_r_y);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    std::cout << carbonDBNodeGetFullName(db, node) << " is declared as " << carbonDBdeclarationType(db, node);
    std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, node) << ")" << std::endl;
  }
  carbonDBFreeNodeIter(iter);

  carbonDBFree(db);
  if (!standalone) {
    carbonDestroy(&obj);
  }
  return 0;
}
