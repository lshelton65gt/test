// Testing IODB

#include "libdbapi8.h"
#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <cstring>

int main(int argc, char** argv)
{
  bool standalone = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-standalone") == 0)
      standalone = true;
  }

  CarbonObjectID *obj = NULL;
  CarbonDB *db;
  if (standalone) {
    db = carbonDBCreate("libdbapi8.io.db", 1);
  } else {
    obj = carbon_dbapi8_create(eCarbonIODB, eCarbon_NoFlags);
    db = carbonGetDB(obj);
  }

  // Get a handle to the array
  const CarbonDBNode *arr = carbonDBFindNode(db, "dbapi8.arr");
  assert(arr == NULL);

  // If the full hierarchy was known, this could have been done all at once:
  // 1. Through the DB API
  const CarbonDBNode *arr_3_1_r_y_6 = carbonDBFindNode(db, "dbapi8.arr[3][1].r.y[6]");
  assert(arr_3_1_r_y_6 == NULL);
  // 2. Through the C API
  if (!standalone) {
    CarbonNetID *net_arr_3_1_r_y_6 = carbonFindNet(obj, "dbapi8.arr[3][1].r.y[6]");
    assert(net_arr_3_1_r_y_6 == NULL);
  }

  carbonDBFree(db);
  if (!standalone) {
    carbonDestroy(&obj);
  }
  return 0;
}
