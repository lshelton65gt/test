// Testing bug8638, in which normalization of arrays is done incorrectly
#include "libbug8638_5.h"
#include <iostream>

void doSelect(CarbonObjectID* obj, CarbonNetID* sel, CarbonUInt32 sel_val, CarbonNetID* clk, CarbonNetID* o, CarbonTime* t)
{
  carbonDeposit(obj, sel, &sel_val, 0);
  CarbonUInt32 val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, *t);
  ++t;
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, *t);
  ++t;
  carbonExamine(obj, o, &val, 0);
  std::cout << "sel = " << sel_val << ", o = " << val << std::endl;
}

int main()
{
  CarbonObjectID* obj = carbon_bug8638_5_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID* clk = carbonFindNet(obj, "bug8638_5.clk");
  CarbonNetID* i_2_a = carbonFindNet(obj, "bug8638_5.i[2].a");
  CarbonNetID* i_3_a = carbonFindNet(obj, "bug8638_5.i[3].a");
  CarbonNetID* sel = carbonFindNet(obj, "bug8638_5.sel");
  CarbonNetID* o = carbonFindNet(obj, "bug8638_5.o");

  CarbonUInt32 val;
  CarbonTime t = 0;

  // Write a 1 to [2]
  val = 1;
  carbonDeposit(obj, i_2_a, &val, 0);
  val = 0;
  carbonDeposit(obj, i_3_a, &val, 0);

  // Read all values
  doSelect(obj, sel, 0, clk, o, &t);
  doSelect(obj, sel, 1, clk, o, &t);
  
  // Write a 1 to [3]
  val = 0;
  carbonDeposit(obj, i_2_a, &val, 0);
  val = 1;
  carbonDeposit(obj, i_3_a, &val, 0);

  // Read all values
  doSelect(obj, sel, 0, clk, o, &t);
  doSelect(obj, sel, 1, clk, o, &t);
  
  carbonDestroy(&obj);
  return 0;
}
