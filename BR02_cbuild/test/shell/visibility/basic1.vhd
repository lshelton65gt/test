library ieee;
use ieee.std_logic_1164.all;

entity basic1 is
  
  port (
    clk : in  std_logic;
    a   : in  std_logic;
    b   : in  std_logic;
    sel : in  std_logic;
    we  : in  std_logic;
    o   : out std_logic);

end basic1;

architecture arch of basic1 is

  type recab is record
                  a : std_logic;
                  b : std_logic;
                end record;

  type recsel is record
                   sel0 : recab;
                   sel1 : recab;
                 end record;

  signal r : recsel;
  
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        if sel = '1' then
          r.sel1.a <= a;
          r.sel1.b <= b;
        else
          r.sel0.a <= a;
          r.sel0.b <= b;
        end if;
      else
        if sel = '1' then
          o <= r.sel1.a or r.sel1.b;
        else
          o <= r.sel0.a or r.sel0.b;
        end if;
      end if;
    end if;
  end process;
  

end arch;
