package base is

   type tMdtN_bit_vector is array(0 downto 0) of bit_vector(0 to 0);

end base;

use work.base.all;

entity bottom is
   port ( --clk   : in bit;
          bin1  : in tMdtN_bit_vector;
          bout1 : out tMdtN_bit_vector
          );
end bottom;

use work.base.all;

architecture archbottom of bottom is

begin

  bout1 <= bin1;
end archbottom;

use work.base.all;

entity bug8424 is
   port ( --clk   : in  bit;
          pin1  : in  tMdtN_bit_vector;
          pout1 : out tMdtN_bit_vector
          );
end bug8424;


use work.base.all;

architecture archbug8424 of bug8424 is

   component bottom is
      port ( --clk   : in  bit;
             bin1  : in  tMdtN_bit_vector;
             bout1 : out tMdtN_bit_vector
             );
   end component;

begin

   u1 : bottom port map ( --clk   => clk,
                          bin1  => pin1,
                          bout1 => pout1
                          );

end archbug8424;
