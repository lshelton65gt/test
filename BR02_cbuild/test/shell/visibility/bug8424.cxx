#include "libbug8424.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <cassert>

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_bug8424_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *pin1 = carbonFindNet(obj, "bug8424.pin1[0]");
  CarbonNetID *pout1 = carbonFindNet(obj, "bug8424.pout1[0]");

  CarbonUInt32 depval, exval;
  CarbonTime t = 0;

  for (CarbonUInt32 i = 0; i < 16; ++i) {
    depval = i & 0x1;
    carbonDeposit(obj, pin1, &depval, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, pout1, &exval, 0);
    assert(depval == exval);
  }

  carbonDestroy(&obj);
  return 0;
}
