#include "libbug8400.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_bug8400_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *clk = carbonFindNet(obj, "bug8400.clk");
  CarbonNetID *we = carbonFindNet(obj, "bug8400.we");
  CarbonNetID *i = carbonFindNet(obj, "bug8400.i");
  CarbonNetID *addr = carbonFindNet(obj, "bug8400.addr");

  carbonDumpVars(carbonWaveInitVCD(obj, "bug8400.vcd", e1ns), 0, "bug8400");

  CarbonUInt32 val;
  CarbonTime t = 0;

  val = 1;
  carbonDeposit(obj, we, &val, 0);

  for (CarbonUInt32 j = 0; j < 16; ++j) {
    carbonDeposit(obj, addr, &j, 0);
    carbonDeposit(obj, i, &j, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  carbonDestroy(&obj);
  return 0;
}
