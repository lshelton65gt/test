module basic10_verilog(input clk,
                       input [7:0] i,
                       output reg [7:0] o,
                       input en_l,
                       input en_h,
                       inout [7:0] io);

   reg [7:0]                       i_reg;

   always @(posedge clk) begin
      i_reg <= i;
      o <= io;
   end

   assign io[3:0] = en_l ? i_reg[3:0] : 4'bzzzz;
   assign io[7:4] = en_h ? i_reg[7:4] : 4'bzzzz;
endmodule
   
