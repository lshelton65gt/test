module basic4_verilog(input clk,
                      input [3:0] addr,
                      input we,
                      input [31:0] din,
                      output reg [31:0] dout);

   reg [31:0]                mem [0:15];

   always @(posedge clk)
     if (we)
       mem[addr] <= din;
     else
       dout <= mem[addr];

endmodule
