library ieee;
use ieee.std_logic_1164.all;

package mypack is
  type rec1 is record
                  a : std_logic;
                end record;

  type array1 is array (2 to 3) of rec1;
  type array2 is array (4 to 5) of array1;
  
end mypack;

library ieee;
use ieee.std_logic_1164.all;
use work.mypack.all;

entity bug8638_3 is
  port (
    clk : in  std_logic;
    i   : in  array2;
    sel1 : in std_logic;
    sel2 : in std_logic;
    o   : out std_logic);
end bug8638_3;

architecture arch of bug8638_3 is
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if sel2 = '0' then
        if sel1 = '0' then
          o <= i(4)(2).a;
        else
          o <= i(4)(3).a;
        end if;
      else
        if sel1 = '0' then
          o <= i(5)(2).a;
        else
          o <= i(5)(3).a;
        end if;
      end if;
    end if;
  end process;
  
end arch;
