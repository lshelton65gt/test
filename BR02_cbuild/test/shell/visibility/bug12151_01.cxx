// Testing 4-D arrays

#include "libbug12151_01.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_bug12151_01_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "bug12151_01.clk");
  CarbonNetID *in1 = carbonFindNet(obj, "bug12151_01.in1");
  CarbonNetID *out1 = carbonFindNet(obj, "bug12151_01.out1");

  // Dump VCD to test bug bug12151_01
  carbonDumpVars(carbonWaveInitVCD(obj, "bug12151_01.vcd", e1ns), 0, "bug12151_01");

  CarbonDB *db = carbonGetDB(obj);

  const CarbonDBNode *node_mem1 = carbonDBFindNode(db, "bug12151_01.mem1");
  const CarbonDBNode *node_in1 = carbonDBFindNode(db, "bug12151_01.in1");
  const int indexes1[3] = {7, 5, 3};
  const CarbonDBNode *node_elem1 = carbonDBGetArrayElement(db, node_mem1, indexes1, 3);
  CarbonNetID *elem1 = carbonDBGetCarbonNet(db, node_elem1);


  CarbonUInt32 val, val1, val2;
  CarbonTime t = 0;

  val1 = 0x7;
  val2 = 0x5;
  carbonDeposit(obj, in1, &val1, 0);

  for (CarbonUInt32 i = 0; i < 3; ++i) {
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, elem1, &val, 0);
    std::cout << "Elem Value is = " << std::dec << " is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, out1, &val, 0);
    std::cout << "Out1 Value is = " << std::dec << " is 0x" << std::hex << val << std::endl;

    carbonDeposit(obj, in1, &val2, 0);
  }

  carbonDestroy(&obj);
  return 0;
}
