// Testing arrays of non-bit scalars (integers, chars, reals)

#include "libscalar2.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>
#include <cassert>

void maybePrintValue(CarbonObjectID* obj, CarbonNetID* net, CarbonMemoryID* mem = NULL, CarbonSInt32 index = 0)
{
  // Print the net's value if it's not 0
  char name[100];
  CarbonUInt32 val;
  carbonExamine(obj, net, &val, 0);
  if (val != 0) {
    carbonGetNetName(obj, net, name, 100);
    std::cout << name << " is 0x" << std::hex << val << std::endl;
  }
  // If we have an equivalent memory, examine it and make sure the
  // value is the same
  if (mem != NULL) {
    CarbonUInt32 val2;
    carbonExamineMemory(mem, index, &val2);
    assert(val2 == val);
  }
}


int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_scalar2_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *clk = carbonFindNet(obj, "scalar2.clk");
  CarbonNetID *we1 = carbonFindNet(obj, "scalar2.we1");
  CarbonNetID *we2 = carbonFindNet(obj, "scalar2.we2");
  CarbonNetID *we3 = carbonFindNet(obj, "scalar2.we3");
  CarbonNetID *we4 = carbonFindNet(obj, "scalar2.we4");
  CarbonNetID *addr1 = carbonFindNet(obj, "scalar2.addr1");
  CarbonNetID *addr2 = carbonFindNet(obj, "scalar2.addr2");
  CarbonNetID *int32in = carbonFindNet(obj, "scalar2.int32in");
  CarbonNetID *int16in = carbonFindNet(obj, "scalar2.int16in");
  CarbonNetID *int8in = carbonFindNet(obj, "scalar2.int8in");
  CarbonNetID *charin = carbonFindNet(obj, "scalar2.charin");
  CarbonNetID *int32out = carbonFindNet(obj, "scalar2.int32out");
  CarbonNetID *int16out = carbonFindNet(obj, "scalar2.int16out");
  CarbonNetID *int8out = carbonFindNet(obj, "scalar2.int8out");
  CarbonNetID *charout = carbonFindNet(obj, "scalar2.charout");
  
  CarbonDB *db = carbonGetDB(obj);

  // Nodes/nets from 1D array of records
  const CarbonDBNode *node_ra1d;
  const CarbonDBNode *node_ra1d_elem[8];
  const CarbonDBNode *node_ra1d_int32[8];
  const CarbonDBNode *node_ra1d_int16[8];
  const CarbonDBNode *node_ra1d_int8[8];
  const CarbonDBNode *node_ra1d_c[8];

  CarbonNetID *ra1d_int32[8];
  CarbonNetID *ra1d_int16[8];
  CarbonNetID *ra1d_int8[8];
  CarbonNetID *ra1d_c[8];

  node_ra1d = carbonDBFindNode(db, "scalar2.ra1d");
  assert(carbonDBIsArray(db, node_ra1d) == 1);

  for (int i = 0; i < 8; ++i) {
    // Get the array element, which is a struct
    node_ra1d_elem[i] = carbonDBGetArrayElement(db, node_ra1d, &i, 1);
    assert(carbonDBIsStruct(db, node_ra1d_elem[i]) == 1);
    // Get all the fields
    node_ra1d_int32[i] = carbonDBGetStructFieldByName(db, node_ra1d_elem[i], "int32");
    assert(carbonDBIsScalar(db, node_ra1d_int32[i]) == 1);
    node_ra1d_int16[i] = carbonDBGetStructFieldByName(db, node_ra1d_elem[i], "int16");
    assert(carbonDBIsScalar(db, node_ra1d_int16[i]) == 1);
    node_ra1d_int8[i] = carbonDBGetStructFieldByName(db, node_ra1d_elem[i], "int8");
    assert(carbonDBIsScalar(db, node_ra1d_int8[i]) == 1);
    node_ra1d_c[i] = carbonDBGetStructFieldByName(db, node_ra1d_elem[i], "c");
    assert(carbonDBIsScalar(db, node_ra1d_c[i]) == 1);
    // Get all the nets
    ra1d_int32[i] = carbonDBGetCarbonNet(db, node_ra1d_int32[i]);
    assert(ra1d_int32[i] != NULL);
    ra1d_int16[i] = carbonDBGetCarbonNet(db, node_ra1d_int16[i]);
    assert(ra1d_int16[i] != NULL);
    ra1d_int8[i] = carbonDBGetCarbonNet(db, node_ra1d_int8[i]);
    assert(ra1d_int8[i] != NULL);
    ra1d_c[i] = carbonDBGetCarbonNet(db, node_ra1d_c[i]);
    assert(ra1d_c[i] != NULL);
  }

  // Nodes/nets from 1D arrays of scalars
  const CarbonDBNode *node_int32a1d;
  const CarbonDBNode *node_int16a1d;
  const CarbonDBNode *node_int8a1d;
  const CarbonDBNode *node_chara1d;
  const CarbonDBNode *node_int32a1d_elem[8];
  const CarbonDBNode *node_int16a1d_elem[8];
  const CarbonDBNode *node_int8a1d_elem[8];
  const CarbonDBNode *node_chara1d_elem[8];

  CarbonNetID *int32a1d_elem[8];
  CarbonNetID *int16a1d_elem[8];
  CarbonNetID *int8a1d_elem[8];
  CarbonNetID *chara1d_elem[8];

  node_int32a1d = carbonDBFindNode(db, "scalar2.int32a1d");
  assert(carbonDBIsArray(db, node_int32a1d) == 1);
  node_int16a1d = carbonDBFindNode(db, "scalar2.int16a1d");
  assert(carbonDBIsArray(db, node_int16a1d) == 1);
  node_int8a1d = carbonDBFindNode(db, "scalar2.int8a1d");
  assert(carbonDBIsArray(db, node_int8a1d) == 1);
  node_chara1d = carbonDBFindNode(db, "scalar2.chara1d");
  assert(carbonDBIsArray(db, node_chara1d) == 1);

  // Each of these can be a memory
  CarbonMemoryID* mem_int32a1d = carbonFindMemory(obj, "scalar2.int32a1d");
  assert(mem_int32a1d != NULL);
  CarbonMemoryID* mem_int16a1d = carbonFindMemory(obj, "scalar2.int16a1d");
  assert(mem_int16a1d != NULL);
  CarbonMemoryID* mem_int8a1d = carbonFindMemory(obj, "scalar2.int8a1d");
  assert(mem_int8a1d != NULL);
  CarbonMemoryID* mem_chara1d = carbonFindMemory(obj, "scalar2.chara1d");
  assert(mem_chara1d != NULL);

  for (int i = 0; i < 8; ++i) {
    // Get each array element, which is a scalar
    node_int32a1d_elem[i] = carbonDBGetArrayElement(db, node_int32a1d, &i, 1);
    assert(carbonDBIsScalar(db, node_int32a1d_elem[i]) == 1);
    node_int16a1d_elem[i] = carbonDBGetArrayElement(db, node_int16a1d, &i, 1);
    assert(carbonDBIsScalar(db, node_int16a1d_elem[i]) == 1);
    node_int8a1d_elem[i] = carbonDBGetArrayElement(db, node_int8a1d, &i, 1);
    assert(carbonDBIsScalar(db, node_int8a1d_elem[i]) == 1);
    node_chara1d_elem[i] = carbonDBGetArrayElement(db, node_chara1d, &i, 1);
    assert(carbonDBIsScalar(db, node_chara1d_elem[i]) == 1);
    // Get all the nets
    int32a1d_elem[i] = carbonDBGetCarbonNet(db, node_int32a1d_elem[i]);
    assert(int32a1d_elem[i] != NULL);
    int16a1d_elem[i] = carbonDBGetCarbonNet(db, node_int16a1d_elem[i]);
    assert(int16a1d_elem[i] != NULL);
    int8a1d_elem[i] = carbonDBGetCarbonNet(db, node_int8a1d_elem[i]);
    assert(int8a1d_elem[i] != NULL);
    chara1d_elem[i] = carbonDBGetCarbonNet(db, node_chara1d_elem[i]);
    assert(chara1d_elem[i] != NULL);
  }


  // Nodes/nets from 2D array of records
  const CarbonDBNode *node_ra2d;
  const CarbonDBNode *node_ra2d_elem[32];
  const CarbonDBNode *node_ra2d_int32[32];
  const CarbonDBNode *node_ra2d_int16[32];
  const CarbonDBNode *node_ra2d_int8[32];
  const CarbonDBNode *node_ra2d_c[32];

  CarbonNetID *ra2d_int32[32];
  CarbonNetID *ra2d_int16[32];
  CarbonNetID *ra2d_int8[32];
  CarbonNetID *ra2d_c[32];

  node_ra2d = carbonDBFindNode(db, "scalar2.ra2d");
  assert(carbonDBIsArray(db, node_ra2d) == 1);

  for (int i = 0; i < 8; ++i) {
    for (int j = 0; j < 4; ++j) {
      int dims[2];
      dims[0] = i;
      dims[1] = j;
      int index = i * 4 + j;
      // Get the array element, which is a struct
      node_ra2d_elem[index] = carbonDBGetArrayElement(db, node_ra2d, dims, 2);
      assert(carbonDBIsStruct(db, node_ra2d_elem[index]) == 1);
      // Get all the fields
      node_ra2d_int32[index] = carbonDBGetStructFieldByName(db, node_ra2d_elem[index], "int32");
      assert(carbonDBIsScalar(db, node_ra2d_int32[index]) == 1);
      node_ra2d_int16[index] = carbonDBGetStructFieldByName(db, node_ra2d_elem[index], "int16");
      assert(carbonDBIsScalar(db, node_ra2d_int16[index]) == 1);
      node_ra2d_int8[index] = carbonDBGetStructFieldByName(db, node_ra2d_elem[index], "int8");
      assert(carbonDBIsScalar(db, node_ra2d_int8[index]) == 1);
      node_ra2d_c[index] = carbonDBGetStructFieldByName(db, node_ra2d_elem[index], "c");
      assert(carbonDBIsScalar(db, node_ra2d_c[index]) == 1);
      // Get all the nets
      ra2d_int32[index] = carbonDBGetCarbonNet(db, node_ra2d_int32[index]);
      assert(ra2d_int32[index] != NULL);
      ra2d_int16[index] = carbonDBGetCarbonNet(db, node_ra2d_int16[index]);
      assert(ra2d_int16[index] != NULL);
      ra2d_int8[index] = carbonDBGetCarbonNet(db, node_ra2d_int8[index]);
      assert(ra2d_int8[index] != NULL);
      ra2d_c[index] = carbonDBGetCarbonNet(db, node_ra2d_c[index]);
      assert(ra2d_c[index] != NULL);
    }
  }


  // Nodes/nets from 2D arrays of scalars
  const CarbonDBNode *node_int32a2d;
  const CarbonDBNode *node_int16a2d;
  const CarbonDBNode *node_int8a2d;
  const CarbonDBNode *node_chara2d;
  const CarbonDBNode *node_int32a2d_elem[32];
  const CarbonDBNode *node_int16a2d_elem[32];
  const CarbonDBNode *node_int8a2d_elem[32];
  const CarbonDBNode *node_chara2d_elem[32];

  CarbonNetID *int32a2d_elem[32];
  CarbonNetID *int16a2d_elem[32];
  CarbonNetID *int8a2d_elem[32];
  CarbonNetID *chara2d_elem[32];

  node_int32a2d = carbonDBFindNode(db, "scalar2.int32a2d");
  assert(carbonDBIsArray(db, node_int32a2d) == 1);
  node_int16a2d = carbonDBFindNode(db, "scalar2.int16a2d");
  assert(carbonDBIsArray(db, node_int16a2d) == 1);
  node_int8a2d = carbonDBFindNode(db, "scalar2.int8a2d");
  assert(carbonDBIsArray(db, node_int8a2d) == 1);
  node_chara2d = carbonDBFindNode(db, "scalar2.chara2d");
  assert(carbonDBIsArray(db, node_chara2d) == 1);

  for (int i = 0; i < 8; ++i) {
    for (int j = 0; j < 4; ++j) {
      int dims[2];
      dims[0] = i;
      dims[1] = j;
      int index = i * 4 + j;

      // Get each array element, which is a scalar
      node_int32a2d_elem[index] = carbonDBGetArrayElement(db, node_int32a2d, dims, 2);
      assert(carbonDBIsScalar(db, node_int32a2d_elem[index]) == 1);
      node_int16a2d_elem[index] = carbonDBGetArrayElement(db, node_int16a2d, dims, 2);
      assert(carbonDBIsScalar(db, node_int16a2d_elem[index]) == 1);
      node_int8a2d_elem[index] = carbonDBGetArrayElement(db, node_int8a2d, dims, 2);
      assert(carbonDBIsScalar(db, node_int8a2d_elem[index]) == 1);
      node_chara2d_elem[index] = carbonDBGetArrayElement(db, node_chara2d, dims, 2);
      assert(carbonDBIsScalar(db, node_chara2d_elem[index]) == 1);
      // Get all the nets
      int32a2d_elem[index] = carbonDBGetCarbonNet(db, node_int32a2d_elem[index]);
      assert(int32a2d_elem[index] != NULL);
      int16a2d_elem[index] = carbonDBGetCarbonNet(db, node_int16a2d_elem[index]);
      assert(int16a2d_elem[index] != NULL);
      int8a2d_elem[index] = carbonDBGetCarbonNet(db, node_int8a2d_elem[index]);
      assert(int8a2d_elem[index] != NULL);
      chara2d_elem[index] = carbonDBGetCarbonNet(db, node_chara2d_elem[index]);
      assert(chara2d_elem[index] != NULL);
    }
  }

  CarbonUInt32 val;
  CarbonTime t = 0;

  // Deposit to all inputs and set we1
  val = 0x12345678;
  carbonDeposit(obj, int32in, &val, 0);
  val = 0x90ba;
  carbonDeposit(obj, int16in, &val, 0);
  val = 0xcd;
  carbonDeposit(obj, int8in, &val, 0);
  val = 0xef;
  carbonDeposit(obj, charin, &val, 0);

  val = 1;
  carbonDeposit(obj, we1, &val, 0);
  val = 5;
  carbonDeposit(obj, addr1, &val, 0);
  val = 2;
  carbonDeposit(obj, addr2, &val, 0);

  // Run a clock
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Print the values of the 1D array of records
  for (int i = 0; i < 8; ++i) {
    maybePrintValue(obj, ra1d_int32[i]);
    maybePrintValue(obj, ra1d_int16[i]);
    maybePrintValue(obj, ra1d_int8[i]);
    maybePrintValue(obj, ra1d_c[i]);
  }


  // Set we2, and deposit to the 1D array of records
  val = 0;
  carbonDeposit(obj, we1, &val, 0);
  val = 1;
  carbonDeposit(obj, we2, &val, 0);

  val = 0xfedcba09;
  carbonDeposit(obj, ra1d_int32[5], &val, 0);
  val = 0x8765;
  carbonDeposit(obj, ra1d_int16[5], &val, 0);
  val = 0x43;
  carbonDeposit(obj, ra1d_int8[5], &val, 0);
  val = 0x21;
  carbonDeposit(obj, ra1d_c[5], &val, 0);

  // Run a clock
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Print the values of the 1D arrays of scalars
  // Also check that the equivelent word in the CarbonMemoryID is correct
  for (int i = 0; i < 8; ++i) {
    maybePrintValue(obj, int32a1d_elem[i], mem_int32a1d, i);
    maybePrintValue(obj, int16a1d_elem[i], mem_int16a1d, i);
    maybePrintValue(obj, int8a1d_elem[i], mem_int8a1d, i);
    maybePrintValue(obj, chara1d_elem[i], mem_chara1d, i);
  }


  // Set we3, and deposit to the 1D arrays of scalars
  val = 0;
  carbonDeposit(obj, we2, &val, 0);
  val = 1;
  carbonDeposit(obj, we3, &val, 0);

  val = 0x87654321;
  carbonDeposit(obj, int32a1d_elem[5], &val, 0);
  val = 0xba09;
  carbonDeposit(obj, int16a1d_elem[5], &val, 0);
  val = 0xdc;
  carbonDeposit(obj, int8a1d_elem[5], &val, 0);
  val = 0xfe;
  carbonDeposit(obj, chara1d_elem[5], &val, 0);

  // Run a clock
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Print the values of the 2D array of records
  for (int i = 0; i < 8; ++i) {
    for (int j = 0; j < 4; ++j) {
      int index = i * 4 + j;
      maybePrintValue(obj, ra2d_int32[index]);
      maybePrintValue(obj, ra2d_int16[index]);
      maybePrintValue(obj, ra2d_int8[index]);
      maybePrintValue(obj, ra2d_c[index]);
    }
  }


  // Set we4, and deposit to the 2D array of records
  val = 0;
  carbonDeposit(obj, we3, &val, 0);
  val = 1;
  carbonDeposit(obj, we4, &val, 0);

  val = 0x90abcdef;
  carbonDeposit(obj, ra2d_int32[22], &val, 0);
  val = 0x5678;
  carbonDeposit(obj, ra2d_int16[22], &val, 0);
  val = 0x34;
  carbonDeposit(obj, ra2d_int8[22], &val, 0);
  val = 0x12;
  carbonDeposit(obj, ra2d_c[22], &val, 0);

  // Run a clock
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Print the values of the 2D arrays of scalars
  for (int i = 0; i < 8; ++i) {
    for (int j = 0; j < 4; ++j) {
      int index = i * 4 + j;
      maybePrintValue(obj, int32a2d_elem[index]);
      maybePrintValue(obj, int16a2d_elem[index]);
      maybePrintValue(obj, int8a2d_elem[index]);
      maybePrintValue(obj, chara2d_elem[index]);
    }
  }


  // Clear we4, and deposit to the 2D arrays of scalars
  val = 0;
  carbonDeposit(obj, we4, &val, 0);

  val = 0x08192a3b;
  carbonDeposit(obj, int32a2d_elem[22], &val, 0);
  val = 0x4c5d;
  carbonDeposit(obj, int16a2d_elem[22], &val, 0);
  val = 0x6e;
  carbonDeposit(obj, int8a2d_elem[22], &val, 0);
  val = 0x7f;
  carbonDeposit(obj, chara2d_elem[22], &val, 0);

  // Run a clock
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Print the values of the 2outputs
  maybePrintValue(obj, int32out);
  maybePrintValue(obj, int16out);
  maybePrintValue(obj, int8out);
  maybePrintValue(obj, charout);


  carbonDBFree(db);
  carbonDestroy(&obj);
  return 0;
}
