library ieee;
use ieee.std_logic_1164.all;

entity scalar1 is
  port (
    clk      : in  std_logic;
    we       : in  std_logic;
    int32in  : in  integer;
    int16in  : in  integer range -32768 to 32767;
    int8in   : in  integer range -256 to 255;
    charin   : in  character;
--    realin   : in  real;
    int32out : out integer;
    int16out : out integer range -32768 to 32767;
    int8out  : out integer range -256 to 255;
    charout  : out character);
--    realout  : out real);
end scalar1;

architecture arch of scalar1 is

  type myrec is record
                  int32 : integer;
                  int16 : integer range -32768 to 32767;
                  int8  : integer range -256 to 255;
                  c     : character;
--                  r     : real;
                end record;

  signal r : myrec;
  
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        r.int32 <= int32in;
        r.int16 <= int16in;
        r.int8 <= int8in;
        r.c <= charin;
--        r.r <= realin;
      end if;

      int32out <= r.int32;
      int16out <= r.int16;
      int8out <= r.int8;
      charout <= r.c;
--      realout <= r.r;

    end if;
  end process;
  
end arch;
