library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity basic7 is
  port (
    clk  : in  std_logic;
    addr : in  std_logic_vector (3 downto 0);
    we   : in  std_logic;
    ain  : in  std_logic;
    bin  : in  std_logic_vector (7 downto 0);
    aout : out std_logic;
    bout : out std_logic_vector (7 downto 0));
end basic7;

architecture arch of basic7 is

type myrec is record
                a : std_logic;
                b : std_logic_vector (7 downto 0);
              end record;

type myarray is array (15 downto 0) of myrec;

signal arr : myarray;
signal addr_int : integer;

begin

  addr_int <= conv_integer(unsigned(addr));

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        arr(addr_int).a <= ain;
        arr(addr_int).b <= bin;
      else
        aout <= arr(addr_int).a;
        bout <= arr(addr_int).b;
      end if;
    end if;
  end process;
  
end arch;
