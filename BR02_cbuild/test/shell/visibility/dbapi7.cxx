// Testing expected functionality when user type data is not available

#include "libdbapi7.h"
#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <cstring>
#include <iostream>

int main(int argc, char** argv)
{
  bool standalone = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-standalone") == 0)
      standalone = true;
  }

  CarbonObjectID *obj = NULL;
  CarbonDB *db;
  if (standalone) {
    db = carbonDBCreate("libdbapi7.symtab.db", 1);
  } else {
    obj = carbon_dbapi7_create(eCarbonFullDB, eCarbon_NoFlags);
    db = carbonGetDB(obj);
  }

  // Get a handle to the array
  const CarbonDBNode *arr = carbonDBFindNode(db, "dbapi7.arr");
  assert(carbonDBIsArray(db, arr) == 0);
  assert(carbonDBIsStruct(db, arr) == 0);
  assert(carbonDBGetArrayDims(db, arr) == -1);
  assert(carbonDBGetArrayLeftBound(db, arr) == -1);
  assert(carbonDBGetArrayRightBound(db, arr) == -1);
  assert(carbonDBCanBeCarbonNet(db, arr) == 0);
  assert(carbonDBGetBitSize(db, arr) == 0);
  std::cout << "Node " << carbonDBNodeGetLeafName(db, arr) << " is declared in file " << carbonDBGetSourceFile(db, arr) << ", line " << carbonDBGetSourceLine(db, arr) << std::endl;
  assert(carbonDBIsContainedByComposite(db, arr) == 0);

  const CarbonDBNode *node;
  const char *name;

  // Get a handle to element (3)(1) of the array, which is a struct
  int indices[2];
  indices[0] = 3;
  indices[1] = 1;
  node = carbonDBGetArrayElement(db, arr, indices, 2);
  assert(node == NULL);

  // Struct functions should also not crash
  assert(carbonDBGetNumStructFields(db, arr) == -1);
  node = carbonDBGetStructFieldByName(db, arr, "foo");
  assert(node == NULL);

  // Make sure the legacy children API functions work
  const char *declType;
  const CarbonDBNode *arr_r = carbonDBFindChild(db, arr, "r");
  assert(arr_r != NULL);
  assert(carbonDBCanBeCarbonNet(db, arr_r) == 0);
  assert(carbonDBGetBitSize(db, arr_r) == 0);
  declType = carbonDBdeclarationType(db, arr_r);
  std::cout << carbonDBNodeGetFullName(db, arr_r) << " is declared as " << (declType ? declType : "<unknown>") << std::endl;
  assert(carbonDBIsContainedByComposite(db, arr_r) == 0);

  const CarbonDBNode *arr_r_y = carbonDBFindChild(db, arr_r, "y");
  assert(arr_r_y != NULL);
  assert(carbonDBCanBeCarbonNet(db, arr_r_y) == 0);
  // This appears to be an array
  assert(carbonDBGetBitSize(db, arr_r_y) == 512);
  declType = carbonDBdeclarationType(db, arr_r_y);
  std::cout << carbonDBNodeGetFullName(db, arr_r_y) << " is declared as " << (declType ? declType : "<unknown>") << std::endl;
  assert(carbonDBIsContainedByComposite(db, arr_r_y) == 0);

  // Loop over all the array children
  CarbonDatabaseNodeIter *iter;

  iter = carbonDBLoopChildren(db, arr);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    declType = carbonDBdeclarationType(db, node);
    std::cout << carbonDBNodeGetFullName(db, node) << " is declared as " << (declType ? declType : "<unknown>") << std::endl;
    assert(carbonDBIsContainedByComposite(db, node) == 0);
  }
  carbonDBFreeNodeIter(iter);
  
  iter = carbonDBLoopChildren(db, arr_r);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    declType = carbonDBdeclarationType(db, node);
    std::cout << carbonDBNodeGetFullName(db, node) << " is declared as " << (declType ? declType : "<unknown>") << std::endl;
    assert(carbonDBIsContainedByComposite(db, node) == 0);
  }
  carbonDBFreeNodeIter(iter);

  iter = carbonDBLoopChildren(db, arr_r_y);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    declType = carbonDBdeclarationType(db, node);
    std::cout << carbonDBNodeGetFullName(db, node) << " is declared as " << (declType ? declType : "<unknown>") << std::endl;
    assert(carbonDBIsContainedByComposite(db, node) == 0);
  }
  carbonDBFreeNodeIter(iter);

  carbonDBFree(db);
  if (!standalone) {
    carbonDestroy(&obj);
  }
  return 0;
}
