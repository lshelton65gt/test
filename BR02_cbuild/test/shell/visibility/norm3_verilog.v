module norm3_verilog(input clk,
                     input [3:0] addr,
                     input we,
                     input [31:0] din,
                     output reg [31:0] dout);

   reg [-20:-51]              mem [-10:-25];
   integer                    addr_int;

   always @(addr)
     addr_int <= addr - 25;

   always @(posedge clk)
     if (we)
       mem[addr_int] <= din;
     else
       dout <= mem[addr_int];
   
endmodule
