library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity array5 is
  
  port (
    clk  : in  std_logic;
    we   : in  std_logic;
    addr : in  std_logic_vector(1 downto 0);
    sel  : in  std_logic;
    din  : in  std_logic_vector(3 downto 0);
    dout : out std_logic_vector(3 downto 0));

end array5;

architecture arch of array5 is

  type subarr is array (15 downto 12) of std_logic;
  type myarr is array (9 downto 8) of subarr;
  type superarray is array (7 to 10) of myarr;
  signal arr : superarray;
  signal addr_int : integer;
  
begin

  addr_int <= conv_integer(unsigned(addr)) + 7;
  
  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        if sel = '1' then
          arr(addr_int)(9)(15) <= din(3);
          arr(addr_int)(9)(14) <= din(2);
          arr(addr_int)(9)(13) <= din(1);
          arr(addr_int)(9)(12) <= din(0);
        else
          arr(addr_int)(8)(15) <= din(3);
          arr(addr_int)(8)(14) <= din(2);
          arr(addr_int)(8)(13) <= din(1);
          arr(addr_int)(8)(12) <= din(0);
        end if;
      else
        if sel = '1' then
          dout(3) <= arr(addr_int)(9)(15);
          dout(2) <= arr(addr_int)(9)(14);
          dout(1) <= arr(addr_int)(9)(13);
          dout(0) <= arr(addr_int)(9)(12);
        else
          dout(3) <= arr(addr_int)(8)(15);
          dout(2) <= arr(addr_int)(8)(14);
          dout(1) <= arr(addr_int)(8)(13);
          dout(0) <= arr(addr_int)(8)(12);
        end if;
      end if;
      
    end if;
  end process;

end arch;
