package base is

  subtype myType is bit;
--    type myType is record
--                     a : bit;
--                     b : bit;
--    end record;

end base;

use work.base.all;

entity bug8485_2 is
   port (
          pin1  : in  myType;
          pout1 : out myType;
          pin2  : in  myType;
          pout2 : out myType
          );
end bug8485_2;


use work.base.all;

architecture bug8485_2 of bug8485_2 is

component sub
  port (
    i : in myType;
    o : out myType
    );
end component;

begin

  sub1 : sub port map (pin1, pout1);
  sub2 : sub port map (pin2, pout2);

end bug8485_2;

use work.base.all;
entity sub is
  port (
    i : in myType;
    o : out myType
    );
end sub;

use work.base.all;
architecture arch of sub is

component bottom
  port (
    i : in myType;
    o : out myType
    );
end component;

begin

  bottom1 : bottom port map (i, o);
  
end arch;


use work.base.all;
entity bottom is
  port (
    i : in myType;
    o : out myType
    );
end bottom;

use work.base.all;
architecture arch of bottom is

begin

  o <= i;
  
end arch;
