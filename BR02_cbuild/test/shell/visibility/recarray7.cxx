// Testing complex arrays of records

#include "librecarray7.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_recarray7_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "recarray7.clk");
  CarbonNetID *a = carbonFindNet(obj, "recarray7.a");
  CarbonNetID *b = carbonFindNet(obj, "recarray7.b");
  CarbonNetID *o = carbonFindNet(obj, "recarray7.o");
  CarbonNetID *we = carbonFindNet(obj, "recarray7.we");
  CarbonNetID *addr1 = carbonFindNet(obj, "recarray7.addr1");
  CarbonNetID *addr2 = carbonFindNet(obj, "recarray7.addr2");
  CarbonNetID *addr3 = carbonFindNet(obj, "recarray7.addr3");

  CarbonUInt32 val;
  CarbonTime t = 0;

  CarbonDB *db = carbonGetDB(obj);
  const CarbonDBNode *node_arr = carbonDBFindNode(db, "recarray7.arr");
  int index = 15;
  const CarbonDBNode *node_arr_15 = carbonDBGetArrayElement(db, node_arr, &index, 1);
  index = 7;
  const CarbonDBNode *node_arr_15_7 = carbonDBGetArrayElement(db, node_arr_15, &index, 1);
  const CarbonDBNode *node_arr_15_7_b = carbonDBGetStructFieldByName(db, node_arr_15_7, "b");
  index = 9;
  const CarbonDBNode *node_arr_15_7_b_9 = carbonDBGetArrayElement(db, node_arr_15_7_b, &index, 1);
  index = 17;
  const CarbonDBNode *node_arr_15_7_b_9_4 = carbonDBGetArrayElement(db, node_arr_15_7_b_9, &index, 1);
  index = 18;
  const CarbonDBNode *node_arr_15_7_b_9_5 = carbonDBGetArrayElement(db, node_arr_15_7_b_9, &index, 1);

  CarbonNetID *arr_15_7_b_9 = carbonDBGetCarbonNet(db, node_arr_15_7_b_9);
  CarbonNetID *arr_15_7_b_9_4 = carbonDBGetCarbonNet(db, node_arr_15_7_b_9_4);
  CarbonNetID *arr_15_7_b_9_5 = carbonDBGetCarbonNet(db, node_arr_15_7_b_9_5);

  val = 0x43;
  carbonDeposit(obj, arr_15_7_b_9, &val, 0);

  val = 3;
  carbonDeposit(obj, addr1, &val, 0);
  val = 6;
  carbonDeposit(obj, addr2, &val, 0);

  for (CarbonUInt32 i = 0; i < 5; ++i) {
    carbonDeposit(obj, addr3, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Out is 0x" << std::hex << val << std::endl;
  }

  val = 1;
  carbonDeposit(obj, arr_15_7_b_9_4, &val, 0);
  carbonDeposit(obj, arr_15_7_b_9_5, &val, 0);

  for (CarbonUInt32 i = 0; i < 5; ++i) {
    carbonDeposit(obj, addr3, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Out is 0x" << std::hex << val << std::endl;
  }

  val = 0x7;
  carbonDepositRange(obj, arr_15_7_b_9, &val, 18, 15, 0);

  for (CarbonUInt32 i = 0; i < 5; ++i) {
    carbonDeposit(obj, addr3, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Out is 0x" << std::hex << val << std::endl;
  }

  val = 3;
  carbonDeposit(obj, we, &val, 0);
  carbonDeposit(obj, addr3, &val, 0);
  val = 0x55;
  carbonDeposit(obj, b, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonExamine(obj, arr_15_7_b_9, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_15_7_b_9_4, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_15_7_b_9_5, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamineRange(obj, arr_15_7_b_9, &val, 19, 16, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;


  carbonDepositArrayFromFile(obj, node_arr_15_7_b, eCarbonHex, "recarray7.mem");
  
  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 11; ++i) {
    carbonDeposit(obj, addr1, &i, 0);
    for (CarbonUInt32 j = 0; j < 19; ++j) {
      carbonDeposit(obj, addr2, &j, 0);
      for (CarbonUInt32 k = 0; k < 5; ++k) {
        carbonDeposit(obj, addr3, &k, 0);
        val = 0;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        val = 1;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        carbonExamine(obj, o, &val, 0);
        if (val != 0) {
          std::cout << "Output at address (" << std::dec << i << ", " << j << ", " << k << ") is 0x" << std::hex << val << std::endl;
        }
      }
    }
  }    

  carbonDepositArrayFromFile(obj, node_arr_15_7_b_9, eCarbonHex, "recarray7.mem2");
  
  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 11; ++i) {
    carbonDeposit(obj, addr1, &i, 0);
    for (CarbonUInt32 j = 0; j < 19; ++j) {
      carbonDeposit(obj, addr2, &j, 0);
      for (CarbonUInt32 k = 0; k < 5; ++k) {
        carbonDeposit(obj, addr3, &k, 0);
        val = 0;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        val = 1;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        carbonExamine(obj, o, &val, 0);
        if (val != 0) {
          std::cout << "Output at address (" << std::dec << i << ", " << j << ", " << k << ") is 0x" << std::hex << val << std::endl;
        }
      }
    }
  }    

  carbonExamineArrayToFile(obj, node_arr_15_7_b, eCarbonHex, "recarray7.dump");
  carbonExamineArrayToFile(obj, node_arr_15_7_b_9, eCarbonHex, "recarray7.dump2");

  carbonDestroy(&obj);
  return 0;
}
