# Duplicates dbapi1.cxx using the Python interface

import Carbon
import Carbon.DB

db = Carbon.DB.DBContext("libdbapi1.symtab.db")

# Get a handle to the array
arr = db.findNode("dbapi1.arr")
if (arr.isArray() != 1):
  raise "arr.isArray() != 1"
if (arr.getArrayDims() != 1):
  raise "arr.getArrayDims() != 1"
if (arr.getArrayLeftBound() != 1):
  raise "arr.getArrayLeftBound() != 1"
if (arr.getArrayRightBound() != 0):
  raise "arr.getArrayRightBound() != 0"
if (arr.canBeCarbonNet() != 0):
  raise "arr.canBeCarbonNet() != 0"
if (arr.getBitSize() != 4):
  raise "arr.getBitSize() != 4"
if (arr.getWidth() != 0):
  raise "arr.getWidth() != 0"
print "Node " + arr.leafName() + " is declared in file " + str(arr.getSourceFile()) + ", line " + str(arr.getSourceLine())
print "Type: " + arr.declarationType() + " intrinsic type: " + arr.intrinsicType()
if (arr.isContainedByComposite() != 0):
  raise "arr.isContainedByComposite() != 0"

# Get a handle to element 1 of the array, which is a struct
index = [1]
arr1 = arr.getArrayElement(index, 1)
if (arr1.isStruct() != 1):
  raise "arr1.isStruct() != 1"
if (arr1.getNumStructFields() != 2):
  raise "arr1.getNumStructFields() != 2"
if (arr1.canBeCarbonNet() != 0):
  raise "arr1.canBeCarbonNet() != 0"
if (arr1.getBitSize() != 2):
  raise "arr1.getBitSize() != 2"
if (arr1.getWidth() != 0):
  raise "arr1.getWidth() != 0"
print "Node " + arr1.leafName() + " is declared in file " + str(arr1.getSourceFile()) + ", line " + str(arr1.getSourceLine())
print "Type: " + arr1.declarationType() + " intrinsic type: " + arr1.intrinsicType()

if (arr1.isContainedByComposite() != 1):
  raise "arr1.isContainedByComposite() != 1"

b = arr1.getStructFieldByName("b")
if (b == None):
  raise "b == None"
if (b.canBeCarbonNet() != 1):
  raise "arr.canBeCarbonNet() != 1"
if (b.getBitSize() != 1):
  raise "b.getBitSize() != 1"
if (b.getWidth() != 1):
  raise "b.getWidth() != 1"
print "Node " + b.leafName() + " is declared in file " + str(b.getSourceFile()) + ", line " + str(b.getSourceLine())
print "Type: " + b.declarationType() + " intrinsic type: " + b.intrinsicType()

if (b.isContainedByComposite() != 1):
  raise "b.isContainedByComposite() != 1"

# Test isEnum()
ws = db.findNode("dbapi1.ws")
if (ws.isEnum() != 1):
  raise "ws.isEnum() != 1"
