// March 2009
// This file tests four dimensional memories.

module bug12151_01(clk,
		   in1, in2, in3, in4, out1, out2, out3, out4, out5, out6, out7, out8);
   input clk;
   input[3:0]   in1;
   input[3:0]   in2;
   input[3:0]   in3;
   input[3:0]   in4;
   output    out1, out2, out3, out4, out5, out6, out7, out8;
   reg[3:0]  out1, out2, out3, out4, out5, out6, out7, out8;
   reg[14:11]   mem1[8:7][6:5][4:3];

   integer 	i, j, k, m, n;
   
   always @(posedge clk)
     begin
	mem1[7][5][3] <= in1;
	mem1[7][5][4] <= ~in1;
	mem1[7][6][3] <= in2;
	mem1[7][6][4] <= ~in2;
	mem1[8][5][3] <= in3;
	mem1[8][5][4] <= ~in3;
	mem1[8][6][3] <= in4;
	mem1[8][6][4] <= ~in4;

	// This is part select
	out1 <= mem1[7][5][3];
	out2 <= mem1[7][5][4];
	out3 <= mem1[7][6][3];
	out4 <= mem1[7][6][4];
	out5 <= mem1[8][5][3];
	out6 <= mem1[8][5][4];
	out7 <= mem1[8][6][3];
	out8 <= mem1[8][6][4];
     end

endmodule			  
