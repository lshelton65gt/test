library ieee;
use ieee.std_logic_1164.all;

entity basic10 is
  port (
    clk  : in    std_logic;
    i    : in    std_logic_vector (7 downto 0);
    o    : out   std_logic_vector (7 downto 0);
    en_l : in    std_logic;
    en_h : in    std_logic;
    io   : inout std_logic_vector (7 downto 0));
end basic10;

architecture arch of basic10 is

  signal i_reg : std_logic_vector (7 downto 0);

begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      i_reg <= i;
      o <= io;
    end if;
  end process;

  io(3 downto 0) <= i_reg(3 downto 0) when en_l = '1' else (others => 'Z');
  io(7 downto 4) <= i_reg(7 downto 4) when en_h = '1' else (others => 'Z');
  
end arch;
