library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug8358_2 is
  port (
    clk  : in  std_logic;
    i    : in  std_logic;
    we   : in  std_logic;
    addr : in  std_logic_vector (3 downto 0);
    o    : out std_logic);
end bug8358_2;

architecture arch of bug8358_2 is

  type enum2 is (A, B);
  type enum3 is (X, Y, Z);
  subtype int2 is integer range 0 to 1;
  type array2 is array (15 downto 0) of enum2;
  type array3 is array (15 downto 0) of enum3;
  type arrayint is array (15 downto 0) of int2;
  signal arr2 : array2;        -- carbon observeSignal
  signal arr3 : array3;        -- carbon observeSignal
  signal arrint : arrayint;    -- carbon observeSignal
  signal addr_int : integer;
  
begin
  
  addr_int <= conv_integer(unsigned(addr));

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        if i = '0' then
          arr2(addr_int) <= A;
          arr3(addr_int) <= Y;
          arrint(addr_int) <= 0;
        else
          arr2(addr_int) <= B;
          arr3(addr_int) <= Z;
          arrint(addr_int) <= 1;
        end if;
      else
        if (arr2(addr_int) = A) and (arr3(addr_int) = Y) and (arrint(addr_int) = 0) then
          o <= '0';
        else
          o <= '1';
        end if;
      end if;
    end if;
  end process;

end arch;
