library ieee;
use ieee.std_logic_1164.all;

entity array2 is
  
  port (
    clk  : in  std_logic;
    we   : in  std_logic;
    sel  : in  std_logic;
    din  : in  std_logic_vector(3 downto 0);
    dout : out std_logic_vector(3 downto 0));

end array2;

architecture arch of array2 is

  type subarr is array (15 downto 12) of std_logic;
  type myarr is array (8 to 9) of subarr;
  signal arr : myarr;
  
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        if sel = '1' then
          arr(9)(15) <= din(3);
          arr(9)(14) <= din(2);
          arr(9)(13) <= din(1);
          arr(9)(12) <= din(0);
        else
          arr(8)(15) <= din(3);
          arr(8)(14) <= din(2);
          arr(8)(13) <= din(1);
          arr(8)(12) <= din(0);
        end if;
      else
        if sel = '1' then
          dout(3) <= arr(9)(15);
          dout(2) <= arr(9)(14);
          dout(1) <= arr(9)(13);
          dout(0) <= arr(9)(12);
        else
          dout(3) <= arr(8)(15);
          dout(2) <= arr(8)(14);
          dout(1) <= arr(8)(13);
          dout(0) <= arr(8)(12);
        end if;
      end if;
      
    end if;
  end process;

end arch;
