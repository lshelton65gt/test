// Testing access to visibility nets that wrap memories, when the
// memories are marked with onDemandIdleDeposit

#include "libondemand6.h"
#include "carbon/carbon_dbapi.h"
#include <iostream>

static const char *modes[] = {"looking", "idle", "backoff"};

static void mode_change_cb(CarbonObjectID* context,
                           void* userContext,
                           CarbonOnDemandMode from,
                           CarbonOnDemandMode to,
                           CarbonTime simTime)
{
  std::cout << "On-demand entering " << modes[to] << " mode at time " << std::dec << simTime << std::endl;
}

static void run_cycles(CarbonUInt32 count, CarbonObjectID *obj, CarbonNetID *clk, CarbonTime &t)
{
  CarbonUInt32 val;
  for (CarbonUInt32 i = 0; i < (count * 2); ++i) {
    val = t & 0x1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, ++t);
  }
}

static void val_change_cb(CarbonObjectID *obj, CarbonNetID *net, CarbonClientData data, CarbonUInt32 *val, CarbonUInt32*)
{
  CarbonTime t = *(reinterpret_cast<CarbonTime*>(data));
  char name[100];
  carbonGetNetName(obj, net, name, 100);
  std::cout << name << " is " << std::dec << *val << " at time " << t << std::endl;
}

static void print(CarbonObjectID *obj, CarbonNetID *net, const CarbonTime &t)
{
  char name[100];
  CarbonUInt32 val;
  carbonGetNetName(obj, net, name, 100);
  carbonExamine(obj, net, &val, 0);
  
  std::cout << name << " = " << std::hex << val << " at time " << std::dec << t << std::endl;
}

static void timestamp(const CarbonTime &t)
{
  std::cout << "TIME: " << std::dec << t << std::endl;
}

int main()
{
  CarbonObjectID *obj = carbon_ondemand6_create(eCarbonFullDB, eCarbon_OnDemand);
  carbonOnDemandSetMaxStates(obj, 50);
  carbonOnDemandSetBackoffStrategy(obj, eCarbonOnDemandBackoffConstant);
  carbonOnDemandSetBackoffCount(obj, 50);
  carbonOnDemandAddModeChangeCB(obj, mode_change_cb, 0);
  carbonOnDemandEnableStats(obj);
  carbonOnDemandSetDebugLevel(obj, eCarbonOnDemandDebugAll);

  CarbonNetID *clk = carbonFindNet(obj, "ondemand6.clk");
  CarbonNetID *addr = carbonFindNet(obj, "ondemand6.addr");
  CarbonNetID *we = carbonFindNet(obj, "ondemand6.we");
  CarbonNetID *din = carbonFindNet(obj, "ondemand6.din");
  CarbonNetID *dout = carbonFindNet(obj, "ondemand6.dout");

  CarbonDB* db = carbonGetDB(obj);
  const CarbonDBNode* mem_array = carbonDBFindNode(db, "ondemand6.mem_array");

  // Get nets for each word
  CarbonNetID* mem_array_words[16];
  for (int i = 0; i < 16; ++i) {
    const CarbonDBNode* node = carbonDBGetArrayElement(db, mem_array, &i, 1);
    mem_array_words[i] = carbonDBGetCarbonNet(db, node);
  }

  CarbonTime t = 0;
  CarbonUInt32 val;

  CarbonNetValueCBDataID *cb = carbonAddNetValueChangeCB(obj, val_change_cb, &t, dout);

//   carbonDumpVars(carbonWaveInitFSDB(obj, "ondemand6.fsdb", e1ns), 0, "ondemand6");

  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  timestamp(t);

  // Readmemh - should break from idle state
  carbonDepositArrayFromFile(obj, mem_array, eCarbonHex, "ondemand6.readmemh");
  print(obj, mem_array_words[7], t);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  timestamp(t);

  // Readmemb - should break from idle state
  carbonDepositArrayFromFile(obj, mem_array, eCarbonBin, "ondemand6.readmemb");
  print(obj, mem_array_words[7], t);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  timestamp(t);

  // carbonDepositMemory - should break from idle state
  val = 0xa;
  carbonDeposit(obj, mem_array_words[1], &val, 0);

  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  timestamp(t);

  // carbonDepositMemoryWord - should break from idle state
  carbonDepositWord(obj, mem_array_words[2], 0xb, 0, 0);

  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  timestamp(t);

  // carbonDepositMemoryRange - should break from idle state
  val = 0xf;
  carbonDepositRange(obj, mem_array_words[3], &val, 5, 2, 0);

  // Now, we need to test examine-ish API calls.
  // We want to make sure that they force a state restore,
  // but don't break out of idle, so start writing a sequence
  // of values so an idle state is detected

  val = 0x1;
  carbonDeposit(obj, we, &val, 0);
  val = 0x4;
  carbonDeposit(obj, addr, &val, 0);
  for (CarbonUInt32 i = 0; i < 10; ++i)
    for (val = 0x11; val <= 0x88; val += 0x11) {
      carbonDeposit(obj, din, &val, 0);
      run_cycles(1, obj, clk, t);
    }

  // Run a few more clocks, so we're not in the first saved state
  val = 0x11;
  carbonDeposit(obj, din, &val, 0);
  run_cycles(1, obj, clk, t);
  val = 0x22;
  carbonDeposit(obj, din, &val, 0);
  run_cycles(1, obj, clk, t);
  val = 0x33;
  carbonDeposit(obj, din, &val, 0);
  run_cycles(1, obj, clk, t);

  // carbonExamineMemory - should not break from idle
  val = 0;
  timestamp(t);
  carbonExamine(obj, mem_array_words[4], &val, 0);
  std::cout << "--> value is " << std::hex << val << std::endl;

  val = 0x44;
  carbonDeposit(obj, din, &val, 0);
  run_cycles(1, obj, clk, t);

  // carbonExamineMemoryWord - should not break from idle
  val = 0;
  timestamp(t);
  carbonExamineWord(obj, mem_array_words[4], &val, 0, 0);
  std::cout << "--> value is " << std::hex << val << std::endl;

  val = 0x55;
  carbonDeposit(obj, din, &val, 0);
  run_cycles(1, obj, clk, t);

  // carbonExamineMemoryRange - should not break from idle
  val = 0;
  timestamp(t);
  carbonExamineRange(obj, mem_array_words[4], &val, 3, 0, 0);
  std::cout << "--> value is " << std::hex << val << std::endl;

  val = 0x66;
  carbonDeposit(obj, din, &val, 0);
  run_cycles(1, obj, clk, t);

  // carbonFormatMemory - should not break from idle
  char strval[100];
  val = 0;
  timestamp(t);
  carbonFormat(obj, mem_array_words[4], strval, 100, eCarbonHex);
  std::cout << "--> value is " << strval << std::endl;

  val = 0x77;
  carbonDeposit(obj, din, &val, 0);
  run_cycles(1, obj, clk, t);

  // carbonDumpAddressRange - should not break from idle
  timestamp(t);
  carbonExamineArrayToFile(obj, mem_array, eCarbonHex, "ondemand6.dump");

  // Stop writing the input
  val = 0x1;
  carbonDeposit(obj, we, &val, 0);
  run_cycles(10, obj, clk, t);

  // Now, start writing to the memory words in a cyclic manner.  An
  // idle state should be reached.
  timestamp(t);
  for (CarbonUInt32 i = 0; i < 20; ++i) {
    for (CarbonUInt32 j = 0; j < 4; ++j) {
      val = j << 4;
      carbonDeposit(obj, mem_array_words[5], &val, 0);
      run_cycles(1, obj, clk, t);
    }
  }

  // Write a different value.  We should diverge.
  timestamp(t);
  val = 0xff;
  carbonDeposit(obj, mem_array_words[5], &val, 0);
  run_cycles(1, obj, clk, t);

  carbonDestroy(&obj);
  return 0;
}
