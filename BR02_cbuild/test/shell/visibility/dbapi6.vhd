library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity dbapi6 is
  port (
    clk   : in  std_logic;
    addr1 : in  std_logic_vector (3 downto 0);
    addr2 : in  std_logic_vector (2 downto 0);
    we    : in  std_logic;
    xin   : in  std_logic_vector (31 downto 0);
    yin   : in  std_logic_vector (3 downto 0);
    zin   : in  std_logic_vector (15 downto 0);
    xout  : out std_logic_vector (31 downto 0);
    yout  : out std_logic_vector (3 downto 0);
    zout  : out std_logic_vector (15 downto 0));
end dbapi6;

architecture arch of dbapi6 is

  type subrec is record
                   x: std_logic_vector(31 downto 0);
                   y: std_logic_vector(7 downto 4);
                 end record;

  type myrec is record
                  r: subrec;
                  z: std_logic_vector (15 downto 0);
                end record;

  type myarray is array (15 downto 0, 7 downto 0) of myrec;

  signal arr: myarray;
  signal addr1_int : integer;
  signal addr2_int : integer;

begin

  addr1_int <= conv_integer(unsigned(addr1));
  addr2_int <= conv_integer(unsigned(addr2));

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        arr(addr1_int, addr2_int).r.x <= xin;
        arr(addr1_int, addr2_int).r.y <= yin;
        arr(addr1_int, addr2_int).z <= zin;
      else
        xout <= arr(addr1_int, addr2_int).r.x;
        yout <= arr(addr1_int, addr2_int).r.y;
        zout <= arr(addr1_int, addr2_int).z;
      end if;
    end if;
  end process;
  
end arch;
