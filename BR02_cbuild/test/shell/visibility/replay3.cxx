// Testing tristates

#include "libreplay3.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>
#include <cstring>
#include <cassert>

static const char *replay_modes[] = {"normal", "record", "playback", "recover"};

static void replay_mode_change_cb(CarbonObjectID* context,
                                  void* userContext,
                                  CarbonVHMMode current,
                                  CarbonVHMMode changingTo)
{
  CarbonTime t = *static_cast<CarbonTime*>(userContext);
  std::cout << std::dec << t << ": Replay changing from " << replay_modes[current] << " mode to " << replay_modes[changingTo] << " mode" << std::endl;
}

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);

  // Are we recording or playing back?
  bool record = false;
  bool playback = false;
  bool diverge1 = false;
  bool diverge2 = false;
  bool diverge3 = false;
  bool diverge4 = false;
  bool diverge5 = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-record") == 0)
      record = true;
    else if (strcmp(argv[i], "-playback") == 0)
      playback = true;
    else if (strcmp(argv[i], "-diverge1") == 0)
      diverge1 = true;
    else if (strcmp(argv[i], "-diverge2") == 0)
      diverge2 = true;
    else if (strcmp(argv[i], "-diverge3") == 0)
      diverge3 = true;
    else if (strcmp(argv[i], "-diverge4") == 0)
      diverge4 = true;
    else if (strcmp(argv[i], "-diverge5") == 0)
      diverge5 = true;
  }

  assert(!(record && playback));

  CarbonObjectID *obj = carbon_replay3_create(eCarbonFullDB, eCarbon_Replay);
  CarbonNetID *clk = carbonFindNet(obj, "replay3.clk");
  CarbonNetID *i = carbonFindNet(obj, "replay3.i");
  CarbonNetID *o = carbonFindNet(obj, "replay3.o");
  CarbonNetID *en_l = carbonFindNet(obj, "replay3.en_l");
  CarbonNetID *en_h = carbonFindNet(obj, "replay3.en_h");
  CarbonNetID *io = carbonFindNet(obj, "replay3.io");
  CarbonNetID *io_2 = carbonFindNet(obj, "replay3.io[2]");
  CarbonNetID *io_3 = carbonFindNet(obj, "replay3.io[3]");
  CarbonNetID *io_4 = carbonFindNet(obj, "replay3.io[4]");
  CarbonNetID *io_5 = carbonFindNet(obj, "replay3.io[5]");

  CarbonUInt32 val, drive;
  CarbonTime t = 0;

  char valStr[10];

  // set up replay
  CarbonReplayInfoID *info = carbonGetReplayInfo(obj);
  carbonReplayInfoPutDB(info, "replay3_db", "replay3");
  carbonReplayInfoAddModeChangeCB(info, replay_mode_change_cb, &t);
//   carbonReplayInfoAddCheckpointCB(info, replay_checkpoint_cb, &t);
  if (record) {
    carbonReplayInfoPutDirAction(info, eCarbonDirOverwrite);
    carbonReplayInfoPutSaveFrequency(info, 0, 5);
    carbonReplayRecordStart(obj);
  } 
  if (playback) {
    carbonReplayInfoPutDirAction(info, eCarbonDirNoOverwrite);
    carbonReplaySetVerboseDivergence(obj, 1);
    carbonReplayPlaybackStart(obj);
  }

  // test examines

  val = 0x55;
  carbonDeposit(obj, i, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;
  
  val = 1;
  carbonDeposit(obj, en_l, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;
  
  val = 1;
  carbonDeposit(obj, en_h, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;
  
  val = 0;
  carbonDeposit(obj, en_l, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;
  
  val = 0;
  carbonDeposit(obj, en_h, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;
  
  // Now test deposits
  drive = 0;
  val = diverge1 ? 1 : 0;
  if (!diverge2) {
    if (diverge3) {
      CarbonUInt32 drive2 = 1;
      carbonDeposit(obj, io_2, &val, &drive2);
    } else {
      carbonDeposit(obj, io_2, &val, &drive);
    }
  }
  val = 1;
  carbonDeposit(obj, io_3, &val, &drive);
  val = 0;
  carbonDeposit(obj, io_4, &val, &drive);
  val = 1;
  carbonDeposit(obj, io_5, &val, &drive);
  
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;

  drive = 0;
  val = 0xff;
  carbonDeposit(obj, io, &val, &drive);
  
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;

  drive = 1;
  val = 0;
  if (!diverge4) {
    if (diverge5) {
      CarbonUInt32 drive2 = 0;
      carbonDeposit(obj, io_2, &val, &drive2);
    } else {
      carbonDeposit(obj, io_2, &val, &drive);
    }
  }
  carbonDeposit(obj, io_3, &val, &drive);
  carbonDeposit(obj, io_4, &val, &drive);
  carbonDeposit(obj, io_5, &val, &drive);
  
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;

  drive = 0x0f;
  val = 0xaa;
  carbonDeposit(obj, io, &val, &drive);
  
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;

  drive = 0;
  val = 1;
  carbonDeposit(obj, en_l, &val, &drive);
  
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonFormat(obj, io, valStr, 10, eCarbonBin);
  std::cout << "io is " << valStr << std::endl;
  carbonFormat(obj, io_2, valStr, 10, eCarbonBin);
  std::cout << "io[2] is " << valStr << std::endl;
  carbonFormat(obj, io_3, valStr, 10, eCarbonBin);
  std::cout << "io[3] is " << valStr << std::endl;
  carbonFormat(obj, io_4, valStr, 10, eCarbonBin);
  std::cout << "io[4] is " << valStr << std::endl;
  carbonFormat(obj, io_5, valStr, 10, eCarbonBin);
  std::cout << "io[5] is " << valStr << std::endl;
  carbonFormat(obj, o, valStr, 10, eCarbonBin);
  std::cout << "o is " << valStr << std::endl;

  carbonDestroy(&obj);
  return 0;
}
