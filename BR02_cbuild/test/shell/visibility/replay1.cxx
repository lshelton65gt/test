// Testing replay

#include "libreplay1.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>
#include <cstring>
#include <cassert>

static const char *replay_modes[] = {"normal", "record", "playback", "recover"};

static void replay_mode_change_cb(CarbonObjectID* context,
                                  void* userContext,
                                  CarbonVHMMode current,
                                  CarbonVHMMode changingTo)
{
  CarbonTime t = *static_cast<CarbonTime*>(userContext);
  std::cout << std::dec << t << ": Replay changing from " << replay_modes[current] << " mode to " << replay_modes[changingTo] << " mode" << std::endl;
}

static void replay_checkpoint_cb(CarbonObjectID* context,
                                 void* userContext,
                                 CarbonUInt64 totalScheduleCalls,
                                 CarbonUInt32 checkpointNumber)
{
  CarbonTime t = *static_cast<CarbonTime*>(userContext);
  std::cout << std::dec << t << ": Replay checkpoint #" << checkpointNumber << std::endl;
}

void printNet(CarbonObjectID* obj, CarbonNetID* net, CarbonClientData data, CarbonUInt32* val, CarbonUInt32*)
{
  CarbonTime t = *static_cast<CarbonTime*>(data);

  char name[100];
  carbonGetNetName(obj, net, name, 100);
  std::cout << std::dec << t << ": Net " << name << " changed value to 0x" << std::hex << *val << std::endl;
}

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);

  // Are we recording or playing back?
  bool record = false;
  bool playback = false;
  bool diverge1 = false;
  bool diverge2 = false;
  bool diverge3 = false;
  bool diverge4 = false;
  bool diverge5 = false;
  bool diverge6 = false;
  bool callback = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-record") == 0)
      record = true;
    else if (strcmp(argv[i], "-playback") == 0)
      playback = true;
    else if (strcmp(argv[i], "-diverge1") == 0)
      diverge1 = true;
    else if (strcmp(argv[i], "-diverge2") == 0)
      diverge2 = true;
    else if (strcmp(argv[i], "-diverge3") == 0)
      diverge3 = true;
    else if (strcmp(argv[i], "-diverge4") == 0)
      diverge4 = true;
    else if (strcmp(argv[i], "-diverge5") == 0)
      diverge5 = true;
    else if (strcmp(argv[i], "-diverge6") == 0)
      diverge6 = true;
    else if (strcmp(argv[i], "-callback") == 0)
      callback = true;
  }

  assert(!(record && playback));

  CarbonObjectID *obj = carbon_replay1_create(eCarbonFullDB, eCarbon_Replay);

  CarbonUInt32 val;
  CarbonTime t = 0;

  // set up replay
  CarbonReplayInfoID *info = carbonGetReplayInfo(obj);
  carbonReplayInfoPutDB(info, "replay1_db", "replay1");
  carbonReplayInfoAddModeChangeCB(info, replay_mode_change_cb, &t);
//   carbonReplayInfoAddCheckpointCB(info, replay_checkpoint_cb, &t);
  if (record) {
    carbonReplayInfoPutDirAction(info, eCarbonDirOverwrite);
    carbonReplayInfoPutSaveFrequency(info, 0, 10);
    carbonReplayRecordStart(obj);
  } 
  if (playback) {
    carbonReplayInfoPutDirAction(info, eCarbonDirNoOverwrite);
    carbonReplaySetVerboseDivergence(obj, 1);
    carbonReplayPlaybackStart(obj);
  }

  CarbonNetID *clk = carbonFindNet(obj, "replay1.clk");
  CarbonNetID *a = carbonFindNet(obj, "replay1.a");
  CarbonNetID *b = carbonFindNet(obj, "replay1.b");
  CarbonNetID *o = carbonFindNet(obj, "replay1.o");
  CarbonNetID *we = carbonFindNet(obj, "replay1.we");
  CarbonNetID *addr1 = carbonFindNet(obj, "replay1.addr1");
  CarbonNetID *addr2 = carbonFindNet(obj, "replay1.addr2");
  CarbonNetID *addr3 = carbonFindNet(obj, "replay1.addr3");
  CarbonNetID *count = carbonFindNet(obj, "replay1.count");

  CarbonDB *db = carbonGetDB(obj);
  const CarbonDBNode *node_arr_15_7_b = carbonDBFindNode(db, "replay1.arr[15][7].b");
  CarbonNetID *arr_15_7_b_9 = carbonFindNet(obj, "replay1.arr[15][7].b[9]");
  const CarbonDBNode *node_arr_15_7_b_9 = carbonNetGetDBNode(obj, arr_15_7_b_9);
  CarbonNetID *arr_15_7_b_9_17 = carbonFindNet(obj, "replay1.arr[15][7].b[9][17]");

  if (callback) {
    carbonAddNetValueChangeCB(obj, printNet, &t, arr_15_7_b_9);
    carbonAddNetValueChangeCB(obj, printNet, &t, arr_15_7_b_9_17);
  }

  // Run a few clocks
  for (CarbonUInt32 i = 0; i < 3; ++i) {
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }
  carbonExamine(obj, count, &val, 0);
  std::cout << "At time " << std::dec << t << " count is " << val << std::endl;
  if (!callback) {
    carbonExamine(obj, arr_15_7_b_9, &val, 0);
    std::cout << "  arr[15][7].b[9] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_15_7_b_9_17, &val, 0);
    std::cout << "  arr[15][7].b[9][17] is 0x" << std::hex << val << std::endl;
  }

  // Write some values
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  val = 3;
  carbonDeposit(obj, addr1, &val, 0);
  val = 6;
  carbonDeposit(obj, addr2, &val, 0);

  for (CarbonUInt32 i = 0; i < 5; ++i) {
    carbonDeposit(obj, addr3, &i, 0);
    val = 0x11 * (i + 1);
    carbonDeposit(obj, b, &val, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  val = 0;
  carbonDeposit(obj, we, &val, 0);

  // See what's in the memory
  for (CarbonUInt32 i = 0; i < 11; ++i) {
    carbonDeposit(obj, addr1, &i, 0);
    for (CarbonUInt32 j = 0; j < 19; ++j) {
      carbonDeposit(obj, addr2, &j, 0);
      for (CarbonUInt32 k = 0; k < 5; ++k) {
        carbonDeposit(obj, addr3, &k, 0);
        val = 0;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        val = 1;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        carbonExamine(obj, o, &val, 0);
        if (val != 0) {
          std::cout << "Output at address (" << std::dec << i << ", " << j << ", " << k << ") is 0x" << std::hex << val << std::endl;
        }
      }
    }
  }    
  carbonExamine(obj, count, &val, 0);
  std::cout << "At time " << std::dec << t << " count is " << val << std::endl;
  if (!callback) {
    carbonExamine(obj, arr_15_7_b_9, &val, 0);
    std::cout << "  arr[15][7].b[9] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_15_7_b_9_17, &val, 0);
    std::cout << "  arr[15][7].b[9][17] is 0x" << std::hex << val << std::endl;
  }

  // Load the array from a file
  if (diverge1) {
    carbonDepositArrayFromFile(obj, node_arr_15_7_b, eCarbonHex, "replay1.mem2");
  } else if (!diverge2) {
    carbonDepositArrayFromFile(obj, node_arr_15_7_b, eCarbonHex, "replay1.mem");
  }

  for (CarbonUInt32 i = 0; i < 11; ++i) {
    carbonDeposit(obj, addr1, &i, 0);
    for (CarbonUInt32 j = 0; j < 19; ++j) {
      carbonDeposit(obj, addr2, &j, 0);
      for (CarbonUInt32 k = 0; k < 5; ++k) {
        carbonDeposit(obj, addr3, &k, 0);
        val = 0;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        val = 1;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        carbonExamine(obj, o, &val, 0);
        if (val != 0) {
          std::cout << "Output at address (" << std::dec << i << ", " << j << ", " << k << ") is 0x" << std::hex << val << std::endl;
        }
      }
    }
  }    
  carbonExamine(obj, count, &val, 0);
  std::cout << "At time " << std::dec << t << " count is " << val << std::endl;
  if (!callback) {
    carbonExamine(obj, arr_15_7_b_9, &val, 0);
    std::cout << "  arr[15][7].b[9] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_15_7_b_9_17, &val, 0);
    std::cout << "  arr[15][7].b[9][17] is 0x" << std::hex << val << std::endl;
  }

  // Deposit the vector
  if (diverge3) {
    val = 0x7f;
    carbonDeposit(obj, arr_15_7_b_9, &val, 0);
  } else if (!diverge4) {
    val = 0x43;
    carbonDeposit(obj, arr_15_7_b_9, &val, 0);
  }

  for (CarbonUInt32 i = 0; i < 11; ++i) {
    carbonDeposit(obj, addr1, &i, 0);
    for (CarbonUInt32 j = 0; j < 19; ++j) {
      carbonDeposit(obj, addr2, &j, 0);
      for (CarbonUInt32 k = 0; k < 5; ++k) {
        carbonDeposit(obj, addr3, &k, 0);
        val = 0;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        val = 1;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        carbonExamine(obj, o, &val, 0);
        if (val != 0) {
          std::cout << "Output at address (" << std::dec << i << ", " << j << ", " << k << ") is 0x" << std::hex << val << std::endl;
        }
      }
    }
  }    
  carbonExamine(obj, count, &val, 0);
  std::cout << "At time " << std::dec << t << " count is " << val << std::endl;
  if (!callback) {
    carbonExamine(obj, arr_15_7_b_9, &val, 0);
    std::cout << "  arr[15][7].b[9] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_15_7_b_9_17, &val, 0);
    std::cout << "  arr[15][7].b[9][17] is 0x" << std::hex << val << std::endl;
  }

  // Load the vector from a file
  if (diverge5) {
    carbonDepositArrayFromFile(obj, node_arr_15_7_b_9, eCarbonHex, "replay1.mem4");
  } else if (!diverge6) {
    carbonDepositArrayFromFile(obj, node_arr_15_7_b_9, eCarbonHex, "replay1.mem3");
  }

  for (CarbonUInt32 i = 0; i < 11; ++i) {
    carbonDeposit(obj, addr1, &i, 0);
    for (CarbonUInt32 j = 0; j < 19; ++j) {
      carbonDeposit(obj, addr2, &j, 0);
      for (CarbonUInt32 k = 0; k < 5; ++k) {
        carbonDeposit(obj, addr3, &k, 0);
        val = 0;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        val = 1;
        carbonDeposit(obj, clk, &val, 0);
        carbonSchedule(obj, t++);
        carbonExamine(obj, o, &val, 0);
        if (val != 0) {
          std::cout << "Output at address (" << std::dec << i << ", " << j << ", " << k << ") is 0x" << std::hex << val << std::endl;
        }
      }
    }
  }    
  carbonExamine(obj, count, &val, 0);
  std::cout << "At time " << std::dec << t << " count is " << val << std::endl;
  if (!callback) {
    carbonExamine(obj, arr_15_7_b_9, &val, 0);
    std::cout << "  arr[15][7].b[9] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_15_7_b_9_17, &val, 0);
    std::cout << "  arr[15][7].b[9][17] is 0x" << std::hex << val << std::endl;
  }

  if (record)
    carbonReplayRecordStop(obj);

  carbonDestroy(&obj);
  return 0;
}
