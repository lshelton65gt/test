// Testing normalization of arrays

#include "libnorm1_verilog.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_norm1_verilog_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "norm1_verilog.clk");
  CarbonNetID *addr = carbonFindNet(obj, "norm1_verilog.addr");
  CarbonNetID *we = carbonFindNet(obj, "norm1_verilog.we");
  CarbonNetID *din = carbonFindNet(obj, "norm1_verilog.din");
  CarbonNetID *dout = carbonFindNet(obj, "norm1_verilog.dout");

  CarbonDB *db = carbonGetDB(obj);

  const CarbonDBNode *node_mem = carbonDBFindNode(db, "norm1_verilog.mem");
  int index = 15;
  const CarbonDBNode *node_mem5 = carbonDBGetArrayElement(db, node_mem, &index, 1);
  CarbonNetID *mem5 = carbonDBGetCarbonNet(db, node_mem5);

  CarbonUInt32 val;
  CarbonTime t = 0;

  val = 0x55555555;
  carbonDeposit(obj, mem5, &val, 0);

  for (CarbonUInt32 i = 0; i < 10; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  index = 21;
  const CarbonDBNode *node_mem5_1 = carbonDBGetArrayElement(db, node_mem5, &index, 1);
  CarbonNetID *mem5_1 = carbonDBGetCarbonNet(db, node_mem5_1);
  val = 0x1;
  carbonDeposit(obj, mem5_1, &val, 0);

  for (CarbonUInt32 i = 0; i < 10; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  // carbonDepositRange should also work
  val = 0x82;
  carbonDepositRange(obj, mem5, &val, 31, 24, 0);

  for (CarbonUInt32 i = 0; i < 10; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  // Now test examines
  val = 4;
  carbonDeposit(obj, addr, &val, 0);
  val = 0x12345678;
  carbonDeposit(obj, din, &val, 0);
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  
  index = 14;
  const CarbonDBNode *node_mem4 = carbonDBGetArrayElement(db, node_mem, &index, 1);
  CarbonNetID *mem4 = carbonDBGetCarbonNet(db, node_mem4);
  carbonExamine(obj, mem4, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;

  index = 22;
  const CarbonDBNode *node_mem4_2 = carbonDBGetArrayElement(db, node_mem4, &index, 1);
  CarbonNetID *mem4_2 = carbonDBGetCarbonNet(db, node_mem4_2);
  carbonExamine(obj, mem4_2, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  
  index = 23;
  const CarbonDBNode *node_mem4_3 = carbonDBGetArrayElement(db, node_mem4, &index, 1);
  CarbonNetID *mem4_3 = carbonDBGetCarbonNet(db, node_mem4_3);
  carbonExamine(obj, mem4_3, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;

  carbonExamineRange(obj, mem4, &val, 35, 24, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;

  carbonDestroy(&obj);
  return 0;
}
