library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity replay2 is
  port (
    clk  : in  std_logic;
    ain  : in  std_logic;
    bin  : in  std_logic_vector (31 downto 0);
    addr : in  std_logic_vector (3 downto 0);
    we   : in  std_logic;
    aout : out std_logic;
    aout_bar : out std_logic;
    bout : out std_logic_vector (31 downto 0);
    count : out std_logic_vector(31 downto 0));
end replay2;

architecture arch of replay2 is

  type myrec is record
                  a     : std_logic;
                  a_bar : std_logic;
                end record;
  type myarray is array (15 downto 0) of myrec;

  signal arr : myarray;
  signal b : std_logic_vector (31 downto 0);
  signal addr_int : integer;
  signal count_int : std_logic_vector (31 downto 0) := (others => '0');
    
begin

  addr_int <= conv_integer(unsigned(addr));
  count <= count_int;

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        arr(addr_int).a <= ain;
        arr(addr_int).a_bar <= not ain;
      else
        aout <= arr(addr_int).a;
        aout_bar <= arr(addr_int).a_bar;
      end if;

      if we = '1' then
        b <= bin;
      end if;
      bout <= b;
      count_int <= count_int + 1;
    end if;
  end process;
  
end arch;
