module ondemand2(clk, rst, in, out);
   input clk, rst;
   input [63:0] in;
   output [63:0] out;

   reg [3:0]    count;

   wire [63:0]   inv;      // carbon forceSignal

   assign       inv = ~in;
   
   always @(posedge clk or posedge rst)
     if (rst)
       count <= 4'b0;
     else
       count <= count + 4'b1;

   assign       out = {16{count}} + inv;

endmodule
