library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity replay4 is
  
  port (
    clk  : in  std_logic;
    a    : in  std_logic;
    b    : in  std_logic;
    addr : in  std_logic_vector (3 downto 0);
    we   : in  std_logic;
    o    : out std_logic);

end replay4;

architecture arch of replay4 is

  type myrec is record
                  a : std_logic;
                  b : std_logic;
                end record;

  type myarray is array (15 downto 0) of myrec;

  signal arr: myarray;

  signal addr_int : integer;

begin

  addr_int <= conv_integer(unsigned(addr));
  
  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        arr(addr_int).a <= a;
        arr(addr_int).b <= b;
      else
        o <= arr(addr_int).a or arr(addr_int).b;
      end if;
    end if;
  end process;
  

end arch;
