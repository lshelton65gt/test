// Testing bug8638, in which normalization of arrays is done incorrectly
#include "libbug8638_2.h"
#include <iostream>

void doSelect(CarbonObjectID* obj, CarbonNetID* sel2, CarbonNetID* sel1,
              CarbonUInt32 sel2_val, CarbonUInt32 sel1_val, CarbonNetID* clk, CarbonNetID* o, CarbonTime* t)
{
  carbonDeposit(obj, sel2, &sel2_val, 0);
  carbonDeposit(obj, sel1, &sel1_val, 0);
  CarbonUInt32 val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, *t);
  ++t;
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, *t);
  ++t;
  carbonExamine(obj, o, &val, 0);
  std::cout << "sel2 = " << sel2_val << ", sel1 = " << sel1_val << ", o = " << val << std::endl;
}

int main()
{
  CarbonObjectID* obj = carbon_bug8638_2_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID* clk = carbonFindNet(obj, "bug8638_2.clk");
  CarbonNetID* i_4_2_a = carbonFindNet(obj, "bug8638_2.i[4][2].a");
  CarbonNetID* i_4_3_a = carbonFindNet(obj, "bug8638_2.i[4][3].a");
  CarbonNetID* i_5_2_a = carbonFindNet(obj, "bug8638_2.i[5][2].a");
  CarbonNetID* i_5_3_a = carbonFindNet(obj, "bug8638_2.i[5][3].a");
  CarbonNetID* sel1 = carbonFindNet(obj, "bug8638_2.sel1");
  CarbonNetID* sel2 = carbonFindNet(obj, "bug8638_2.sel2");
  CarbonNetID* o = carbonFindNet(obj, "bug8638_2.o");

  CarbonUInt32 val;
  CarbonTime t = 0;

  // Write a 1 to [4][2]
  val = 1;
  carbonDeposit(obj, i_4_2_a, &val, 0);
  val = 0;
  carbonDeposit(obj, i_4_3_a, &val, 0);
  val = 0;
  carbonDeposit(obj, i_5_2_a, &val, 0);
  val = 0;
  carbonDeposit(obj, i_5_3_a, &val, 0);

  // Read all values
  doSelect(obj, sel2, sel1, 0, 0, clk, o, &t);
  doSelect(obj, sel2, sel1, 0, 1, clk, o, &t);
  doSelect(obj, sel2, sel1, 1, 0, clk, o, &t);
  doSelect(obj, sel2, sel1, 1, 1, clk, o, &t);
  
  // Write a 1 to [4][3]
  val = 0;
  carbonDeposit(obj, i_4_2_a, &val, 0);
  val = 1;
  carbonDeposit(obj, i_4_3_a, &val, 0);
  val = 0;
  carbonDeposit(obj, i_5_2_a, &val, 0);
  val = 0;
  carbonDeposit(obj, i_5_3_a, &val, 0);

  // Read all values
  doSelect(obj, sel2, sel1, 0, 0, clk, o, &t);
  doSelect(obj, sel2, sel1, 0, 1, clk, o, &t);
  doSelect(obj, sel2, sel1, 1, 0, clk, o, &t);
  doSelect(obj, sel2, sel1, 1, 1, clk, o, &t);
  
  // Write a 1 to [5][2]
  val = 0;
  carbonDeposit(obj, i_4_2_a, &val, 0);
  val = 0;
  carbonDeposit(obj, i_4_3_a, &val, 0);
  val = 1;
  carbonDeposit(obj, i_5_2_a, &val, 0);
  val = 0;
  carbonDeposit(obj, i_5_3_a, &val, 0);

  // Read all values
  doSelect(obj, sel2, sel1, 0, 0, clk, o, &t);
  doSelect(obj, sel2, sel1, 0, 1, clk, o, &t);
  doSelect(obj, sel2, sel1, 1, 0, clk, o, &t);
  doSelect(obj, sel2, sel1, 1, 1, clk, o, &t);
  
  // Write a 1 to [5][3]
  val = 0;
  carbonDeposit(obj, i_4_2_a, &val, 0);
  val = 0;
  carbonDeposit(obj, i_4_3_a, &val, 0);
  val = 0;
  carbonDeposit(obj, i_5_2_a, &val, 0);
  val = 1;
  carbonDeposit(obj, i_5_3_a, &val, 0);

  // Read all values
  doSelect(obj, sel2, sel1, 0, 0, clk, o, &t);
  doSelect(obj, sel2, sel1, 0, 1, clk, o, &t);
  doSelect(obj, sel2, sel1, 1, 0, clk, o, &t);
  doSelect(obj, sel2, sel1, 1, 1, clk, o, &t);
  
  carbonDestroy(&obj);
  return 0;
}
