// Testing replay

#include "libreplay2.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>
#include <cstring>
#include <cassert>

static const char *replay_modes[] = {"normal", "record", "playback", "recover"};

static void replay_mode_change_cb(CarbonObjectID* context,
                                  void* userContext,
                                  CarbonVHMMode current,
                                  CarbonVHMMode changingTo)
{
  CarbonTime t = *static_cast<CarbonTime*>(userContext);
  std::cout << std::dec << t << ": Replay changing from " << replay_modes[current] << " mode to " << replay_modes[changingTo] << " mode" << std::endl;
}

static void replay_checkpoint_cb(CarbonObjectID* context,
                                 void* userContext,
                                 CarbonUInt64 totalScheduleCalls,
                                 CarbonUInt32 checkpointNumber)
{
  CarbonTime t = *static_cast<CarbonTime*>(userContext);
  std::cout << std::dec << t << ": Replay checkpoint #" << checkpointNumber << std::endl;
}

void printNet(CarbonObjectID* obj, CarbonNetID* net, CarbonClientData data, CarbonUInt32* val, CarbonUInt32*)
{
  CarbonTime t = *static_cast<CarbonTime*>(data);

  char name[100];
  carbonGetNetName(obj, net, name, 100);
  std::cout << std::dec << t << ": Net " << name << " changed value to 0x" << std::hex << *val << std::endl;
}

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);

  // Are we recording or playing back?
  bool record = false;
  bool playback = false;
  bool diverge1 = false;
  bool diverge2 = false;
  bool diverge3 = false;
  bool diverge4 = false;
  bool diverge5 = false;
  bool diverge6 = false;
  bool callback = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-record") == 0)
      record = true;
    else if (strcmp(argv[i], "-playback") == 0)
      playback = true;
    else if (strcmp(argv[i], "-diverge1") == 0)
      diverge1 = true;
    else if (strcmp(argv[i], "-diverge2") == 0)
      diverge2 = true;
    else if (strcmp(argv[i], "-diverge3") == 0)
      diverge3 = true;
    else if (strcmp(argv[i], "-diverge4") == 0)
      diverge4 = true;
    else if (strcmp(argv[i], "-diverge5") == 0)
      diverge5 = true;
    else if (strcmp(argv[i], "-diverge6") == 0)
      diverge6 = true;
    else if (strcmp(argv[i], "-callback") == 0)
      callback = true;
  }

  assert(!(record && playback));

  CarbonObjectID *obj = carbon_replay2_create(eCarbonFullDB, eCarbon_Replay);

  CarbonUInt32 val;
  CarbonTime t = 0;

  // set up replay
  CarbonReplayInfoID *info = carbonGetReplayInfo(obj);
  carbonReplayInfoPutDB(info, "replay2_db", "replay2");
  carbonReplayInfoAddModeChangeCB(info, replay_mode_change_cb, &t);
//   carbonReplayInfoAddCheckpointCB(info, replay_checkpoint_cb, &t);
  if (record) {
    carbonReplayInfoPutDirAction(info, eCarbonDirOverwrite);
    carbonReplayInfoPutSaveFrequency(info, 0, 10);
    carbonReplayRecordStart(obj);
  } 
  if (playback) {
    carbonReplayInfoPutDirAction(info, eCarbonDirNoOverwrite);
    carbonReplaySetVerboseDivergence(obj, 1);
    carbonReplayPlaybackStart(obj);
  }

  CarbonNetID *clk = carbonFindNet(obj, "replay2.clk");
  CarbonNetID *ain = carbonFindNet(obj, "replay2.ain");
  CarbonNetID *bin = carbonFindNet(obj, "replay2.bin");
  CarbonNetID *addr = carbonFindNet(obj, "replay2.addr");
  CarbonNetID *we = carbonFindNet(obj, "replay2.we");
  CarbonNetID *aout = carbonFindNet(obj, "replay2.aout");
  CarbonNetID *bout = carbonFindNet(obj, "replay2.bout");
  CarbonNetID *count = carbonFindNet(obj, "replay2.count");

  CarbonDB *db = carbonGetDB(obj);
  const CarbonDBNode *node_b = carbonDBFindNode(db, "replay2.b");
  CarbonNetID *b_8 = carbonFindNet(obj, "replay2.b[8]");
  CarbonNetID *b_9 = carbonFindNet(obj, "replay2.b[9]");
  CarbonNetID *arr_5_a = carbonFindNet(obj, "replay2.arr[5].a");
  CarbonNetID *arr_6_a = carbonFindNet(obj, "replay2.arr[6].a");

  if (callback) {
    carbonAddNetValueChangeCB(obj, printNet, &t, b_8);
    carbonAddNetValueChangeCB(obj, printNet, &t, b_9);
    carbonAddNetValueChangeCB(obj, printNet, &t, arr_5_a);
    carbonAddNetValueChangeCB(obj, printNet, &t, arr_6_a);
  }

#if 0
  // Deposit a different value than the one we're going to deposit
  // later, to allow divergence to be detected.  This is needed
  // because of bug 7347.
  val = 1;
  carbonDeposit(obj, arr_5_a, &val, 0);
  carbonDeposit(obj, b_8, &val, 0);
  carbonDeposit(obj, b_9, &val, 0);
#endif

  // Run a few clocks
  for (CarbonUInt32 i = 0; i < 3; ++i) {
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }
  carbonExamine(obj, count, &val, 0);
  std::cout << "At time " << std::dec << t << " count is " << val << std::endl;
  if (!callback) {
    carbonExamine(obj, b_8, &val, 0);
    std::cout << "  b[8] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, b_9, &val, 0);
    std::cout << "  b[9] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_5_a, &val, 0);
    std::cout << "  arr[5].a is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_6_a, &val, 0);
    std::cout << "  arr[6].a is 0x" << std::hex << val << std::endl;
  }

  // Write some values
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  val = 0x55555555;
  carbonDeposit(obj, bin, &val, 0);

  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = i % 2;
    carbonDeposit(obj, ain, &val, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  val = 0;
  carbonDeposit(obj, we, &val, 0);

  // See what's in the memory
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, aout, &val, 0);
    std::cout << "Output (a) at address (" << std::dec << i << ") is " << val << std::endl;
  }    
  carbonExamine(obj, count, &val, 0);
  std::cout << "At time " << std::dec << t << " count is " << val << std::endl;
  if (!callback) {
    carbonExamine(obj, b_8, &val, 0);
    std::cout << "  b[8] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, b_9, &val, 0);
    std::cout << "  b[9] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_5_a, &val, 0);
    std::cout << "  arr[5].a is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_6_a, &val, 0);
    std::cout << "  arr[6].a is 0x" << std::hex << val << std::endl;
  }

  // Deposit directly to the array of records
  if (!diverge1) {
    val = 0;
    carbonDeposit(obj, arr_5_a, &val, 0);
  }
  if (!diverge2) {
    val = 1;
    carbonDeposit(obj, arr_6_a, &val, 0);
  }

  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, aout, &val, 0);
    std::cout << "Output (a) at address (" << std::dec << i << ") is " << val << std::endl;
  }    
  carbonExamine(obj, count, &val, 0);
  std::cout << "At time " << std::dec << t << " count is " << val << std::endl;
  if (!callback) {
    carbonExamine(obj, b_8, &val, 0);
    std::cout << "  b[8] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, b_9, &val, 0);
    std::cout << "  b[9] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_5_a, &val, 0);
    std::cout << "  arr[5].a is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_6_a, &val, 0);
    std::cout << "  arr[6].a is 0x" << std::hex << val << std::endl;
  }

  // Load the vector from a file
  if (diverge3) {
    carbonDepositArrayFromFile(obj, node_b, eCarbonHex, "replay2.mem2");
  } else if (!diverge4) {
    carbonDepositArrayFromFile(obj, node_b, eCarbonHex, "replay2.mem");
  }

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, bout, &val, 0);
  std::cout << "Output (b) is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, count, &val, 0);
  std::cout << "At time " << std::dec << t << " count is " << val << std::endl;
  if (!callback) {
    carbonExamine(obj, b_8, &val, 0);
    std::cout << "  b[8] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, b_9, &val, 0);
    std::cout << "  b[9] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_5_a, &val, 0);
    std::cout << "  arr[5].a is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_6_a, &val, 0);
    std::cout << "  arr[6].a is 0x" << std::hex << val << std::endl;
  }

  // Write the vector bits directly
  if (!diverge5) {
    val = 0;
    carbonDeposit(obj, b_8, &val, 0);
  }
  if (!diverge6) {
    val = 0;
    carbonDeposit(obj, b_9, &val, 0);
  }

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, bout, &val, 0);
  std::cout << "Output (b) is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, count, &val, 0);
  std::cout << "At time " << std::dec << t << " count is " << val << std::endl;
  if (!callback) {
    carbonExamine(obj, b_8, &val, 0);
    std::cout << "  b[8] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, b_9, &val, 0);
    std::cout << "  b[9] is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_5_a, &val, 0);
    std::cout << "  arr[5].a is 0x" << std::hex << val << std::endl;
    carbonExamine(obj, arr_6_a, &val, 0);
    std::cout << "  arr[6].a is 0x" << std::hex << val << std::endl;
  }

  if (record)
    carbonReplayRecordStop(obj);

  carbonDestroy(&obj);
  return 0;
}
