// Testing complex arrays and structs

#include "libdbapi3.h"
#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <cstring>
#include <iostream>

int main(int argc, char** argv)
{
  bool standalone = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-standalone") == 0)
      standalone = true;
  }

  CarbonObjectID *obj = NULL;
  CarbonDB *db;
  if (standalone) {
    db = carbonDBCreate("libdbapi3.symtab.db", 1);
  } else {
    obj = carbon_dbapi3_create(eCarbonFullDB, eCarbon_NoFlags);
    db = carbonGetDB(obj);
  }

  // Get a handle to the array
  const CarbonDBNode *arr = carbonDBFindNode(db, "dbapi3.arr");
  assert(carbonDBIsArray(db, arr) == 1);
  assert(carbonDBGetArrayDims(db, arr) == 2);
  assert(carbonDBGetArrayLeftBound(db, arr) == 15);
  assert(carbonDBGetArrayRightBound(db, arr) == 0);

  // Get a handle to element (3)(1) of the array, which is a struct
  int indices[2];
  indices[0] = 3;
  indices[1] = 1;
  const CarbonDBNode *arr_3_1 = carbonDBGetArrayElement(db, arr, indices, 2);
  assert(carbonDBIsStruct(db, arr_3_1) == 1);
  assert(carbonDBGetNumStructFields(db, arr_3_1) == 2);

  // Get a CarbonNetID for the struct, which should fail
  CarbonNetID *net_arr_3_1 = carbonDBGetCarbonNet(db, arr_3_1);
  assert(net_arr_3_1 == NULL);



  // Continue traversing the structure
  const CarbonDBNode *arr_3_1_r = carbonDBGetStructFieldByName(db, arr_3_1, "r");
  assert(carbonDBIsStruct(db, arr_3_1_r) == 1);
  assert(carbonDBGetNumStructFields(db, arr_3_1_r) == 2);

  const CarbonDBNode *arr_3_1_z = carbonDBGetStructFieldByName(db, arr_3_1, "z");
  assert(carbonDBIsArray(db, arr_3_1_z) == 1);
  assert(carbonDBGetArrayDims(db, arr_3_1_z) == 1);
  assert(carbonDBGetArrayLeftBound(db, arr_3_1_z) == 15);
  assert(carbonDBGetArrayRightBound(db, arr_3_1_z) == 0);

  const CarbonDBNode *arr_3_1_r_y = carbonDBGetStructFieldByName(db, arr_3_1_r, "y");
  assert(carbonDBIsArray(db, arr_3_1_r_y) == 1);
  assert(carbonDBGetArrayDims(db, arr_3_1_r_y) == 1);
  assert(carbonDBGetArrayLeftBound(db, arr_3_1_r_y) == 7);
  assert(carbonDBGetArrayRightBound(db, arr_3_1_r_y) == 4);

  // Get a CarbonNetID for bit 6 and deposit
  int index = 6;
  const CarbonDBNode *arr_3_1_r_y_6 = carbonDBGetArrayElement(db, arr_3_1_r_y, &index, 1);
  CarbonNetID *net_arr_3_1_r_y_6 = carbonDBGetCarbonNet(db, arr_3_1_r_y_6);
  if (standalone) {
    assert(net_arr_3_1_r_y_6 == NULL);
  } else {
    assert(net_arr_3_1_r_y_6 != NULL);
    const CarbonDBNode *node = carbonNetGetDBNode(obj, net_arr_3_1_r_y_6);
    assert(node == arr_3_1_r_y_6);

    CarbonUInt32 val = 1;
    carbonDeposit(obj, net_arr_3_1_r_y_6, &val, 0);
  }

  // If the full hierarchy was known, this could have been done all at once:
  // 1. Through the DB API
  const CarbonDBNode *arr_3_1_r_y_6_alt = carbonDBFindNode(db, "dbapi3.arr[3][1].r.y[6]");
  assert(arr_3_1_r_y_6_alt == arr_3_1_r_y_6);
  // 2. Through the C API
  if (!standalone) {
    CarbonNetID *net_arr_3_1_r_y_6_alt = carbonFindNet(obj, "dbapi3.arr[3][1].r.y[6]");
    assert(net_arr_3_1_r_y_6_alt == net_arr_3_1_r_y_6);
  }

  CarbonTime t = 0;
  if (!standalone) {
    // Read from the appropriate address to make sure the net was deposited properly
    CarbonNetID *clk = carbonFindNet(obj, "dbapi3.clk");
    CarbonNetID *addr1 = carbonFindNet(obj, "dbapi3.addr1");
    CarbonNetID *addr2 = carbonFindNet(obj, "dbapi3.addr2");
    CarbonNetID *yout = carbonFindNet(obj, "dbapi3.yout");

    CarbonUInt32 val = 3;
    carbonDeposit(obj, addr1, &val, 0);
    val = 1;
    carbonDeposit(obj, addr2, &val, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, yout, &val, 0);
    std::cout << "yout is 0x" << std::hex << val << std::endl;
  }

  carbonDBFree(db);
  if (!standalone) {
    carbonDestroy(&obj);
  }
  return 0;
}
