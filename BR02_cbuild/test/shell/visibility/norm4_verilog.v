module norm4_verilog(input clk,
                     input [3:0] addr,
                     input we,
                     input [0:31] din,
                     output reg [0:31] dout);

   reg [-51:-20]              mem [-25:-10];
   integer                    addr_int;

   always @(addr)
     addr_int <= addr - 25;

   always @(posedge clk)
     if (we)
       mem[addr_int] <= din;
     else
       dout <= mem[addr_int];
   
endmodule
