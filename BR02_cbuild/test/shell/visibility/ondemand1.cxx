// Testing basic operation: idle detection, non-interrupting examine, ineligible deposit

#include "libondemand1.h"
#include <iostream>

static const char *modes[] = {"looking", "idle", "backoff"};

static void mode_change_cb(CarbonObjectID* context,
                           void* userContext,
                           CarbonOnDemandMode from,
                           CarbonOnDemandMode to,
                           CarbonTime simTime)
{
  std::cout << "On-demand entering " << modes[to] << " mode at time " << std::dec << simTime << std::endl;
}

static void run_cycles(CarbonUInt32 count, CarbonObjectID *obj, CarbonNetID *clk, CarbonTime &t)
{
  CarbonUInt32 val;
  for (CarbonUInt32 i = 0; i < (count * 2); ++i) {
    // Don't mask the value - ensure don't care bits are ignored
    val = t;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, ++t);
  }
}

static void print(CarbonObjectID *obj, CarbonNetID *net, const CarbonTime &t)
{
  char name[100];
  CarbonUInt32 val;
  carbonGetNetName(obj, net, name, 100);
  carbonExamine(obj, net, &val, 0);
  
  std::cout << name << " = " << std::hex << val << " at time " << std::dec << t << std::endl;
}

static void timestamp(const CarbonTime &t)
{
  std::cout << "TIME: " << std::dec << t << std::endl;
}

int main()
{
  CarbonObjectID *obj = carbon_ondemand1_create(eCarbonFullDB, eCarbon_OnDemand);
  carbonOnDemandSetMaxStates(obj, 50);
  carbonOnDemandSetBackoffStrategy(obj, eCarbonOnDemandBackoffConstant);
  carbonOnDemandSetBackoffCount(obj, 50);
  carbonOnDemandAddModeChangeCB(obj, mode_change_cb, 0);
  carbonOnDemandEnableStats(obj);
  carbonOnDemandSetDebugLevel(obj, eCarbonOnDemandDebugAll);

  CarbonNetID *clk = carbonFindNet(obj, "ondemand1.clk");
  CarbonNetID *rst = carbonFindNet(obj, "ondemand1.rst");
  CarbonNetID *in0 = carbonFindNet(obj, "ondemand1.in[0]");
  CarbonNetID *in1 = carbonFindNet(obj, "ondemand1.in[1]");
  CarbonNetID *in2 = carbonFindNet(obj, "ondemand1.in[2]");
  CarbonNetID *in3 = carbonFindNet(obj, "ondemand1.in[3]");
  CarbonNetID *out0 = carbonFindNet(obj, "ondemand1.out[0]");
  CarbonNetID *out1 = carbonFindNet(obj, "ondemand1.out[1]");
  CarbonNetID *out2 = carbonFindNet(obj, "ondemand1.out[2]");
  CarbonNetID *out3 = carbonFindNet(obj, "ondemand1.out[3]");

  CarbonTime t = 0;
  CarbonUInt32 val;

  // reset

  val = 1;
  carbonDeposit(obj, rst, &val, 0);
  val = 1;
  carbonDeposit(obj, in0, &val, 0);
  carbonDeposit(obj, in1, &val, 0);
  val = 0;
  carbonDeposit(obj, in2, &val, 0);
  carbonDeposit(obj, in3, &val, 0);
  run_cycles(5, obj, clk, t);

  val = 0;
  carbonDeposit(obj, rst, &val, 0);
  run_cycles(5, obj, clk, t);
  
  print(obj, out0, t);
  print(obj, out1, t);
  print(obj, out2, t);
  print(obj, out3, t);

  // Run long enough to get in an on-demand state
  timestamp(t);
  run_cycles(50, obj, clk, t);

  // Make sure state is restored and correct value returned.
  // Idle mode should not be exited.
  print(obj, out0, t);
  print(obj, out1, t);
  print(obj, out2, t);
  print(obj, out3, t);

  // Run some more
  run_cycles(50, obj, clk, t);

  // Deposit to the input.  This should break out of on-demand
  timestamp(t);
  val = 1;
  carbonDeposit(obj, in2, &val, 0);
  run_cycles(1, obj, clk, t);
  print(obj, out0, t);
  print(obj, out1, t);
  print(obj, out2, t);
  print(obj, out3, t);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);

  // Try to get another CarbonNetID for a net we've already
  // found.  The same pointer should be returned, so we should
  // be able to use it transparently and not exit idle state.
  timestamp(t);
  CarbonNetID *clk2 = carbonFindNet(obj, "ondemand1.clk");
  run_cycles(50, obj, clk2, t);
  print(obj, out0, t);
  print(obj, out1, t);
  print(obj, out2, t);
  print(obj, out3, t);

  // Make sure the nets can be freed
  carbonFreeHandle(obj, &clk);
  carbonFreeHandle(obj, &clk2);
  carbonFreeHandle(obj, &rst);
  carbonFreeHandle(obj, &in0);
  carbonFreeHandle(obj, &in1);
  carbonFreeHandle(obj, &in2);
  carbonFreeHandle(obj, &in3);
  carbonFreeHandle(obj, &out0);
  carbonFreeHandle(obj, &out1);
  carbonFreeHandle(obj, &out2);
  carbonFreeHandle(obj, &out3);

  carbonDestroy(&obj);
  return 0;
}
