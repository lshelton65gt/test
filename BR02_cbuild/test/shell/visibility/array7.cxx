// Testing 3-D arrays

#include "libarray7.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_array7_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "array7.clk");
  CarbonNetID *addr = carbonFindNet(obj, "array7.addr");
  CarbonNetID *sel = carbonFindNet(obj, "array7.sel");
  CarbonNetID *we = carbonFindNet(obj, "array7.we");
  CarbonNetID *din = carbonFindNet(obj, "array7.din");
  CarbonNetID *dout = carbonFindNet(obj, "array7.dout");

  CarbonDB *db = carbonGetDB(obj);

  CarbonNetID *arr_7_8 = carbonFindNet(obj, "array7.arr[7][8]");
  CarbonNetID *arr_7_9 = carbonFindNet(obj, "array7.arr[7][9]");
  CarbonNetID *arr_8_8 = carbonFindNet(obj, "array7.arr[8][8]");
  CarbonNetID *arr_8_9 = carbonFindNet(obj, "array7.arr[8][9]");
  CarbonNetID *arr_9_8 = carbonFindNet(obj, "array7.arr[9][8]");
  CarbonNetID *arr_9_9 = carbonFindNet(obj, "array7.arr[9][9]");
  CarbonNetID *arr_10_8 = carbonFindNet(obj, "array7.arr[10][8]");
  CarbonNetID *arr_10_9 = carbonFindNet(obj, "array7.arr[10][9]");

  CarbonUInt32 val;
  CarbonTime t = 0;

  val = 0x1;
  carbonDeposit(obj, arr_7_8, &val, 0);
  val = 0x2;
  carbonDeposit(obj, arr_7_9, &val, 0);
  val = 0x3;
  carbonDeposit(obj, arr_8_8, &val, 0);
  val = 0x4;
  carbonDeposit(obj, arr_8_9, &val, 0);
  val = 0x5;
  carbonDeposit(obj, arr_9_8, &val, 0);
  val = 0x6;
  carbonDeposit(obj, arr_9_9, &val, 0);
  val = 0x7;
  carbonDeposit(obj, arr_10_8, &val, 0);
  val = 0x8;
  carbonDeposit(obj, arr_10_9, &val, 0);

  for (CarbonUInt32 i = 0; i < 4; ++i) {
    for (CarbonUInt32 j = 0; j < 2; ++j) {
      carbonDeposit(obj, addr, &i, 0);
      carbonDeposit(obj, sel, &j, 0);
      val = 0;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      val = 1;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      carbonExamine(obj, dout, &val, 0);
      std::cout << "Data with addr = " << std::dec << i << ", select = " << std::dec << j << " is 0x" << std::hex << val << std::endl;
    }
  }

  CarbonNetID *arr_7_8_15 = carbonFindNet(obj, "array7.arr[7][8][15]");
  CarbonNetID *arr_7_9_15 = carbonFindNet(obj, "array7.arr[7][9][15]");
  CarbonNetID *arr_8_8_15 = carbonFindNet(obj, "array7.arr[8][8][15]");
  CarbonNetID *arr_8_9_15 = carbonFindNet(obj, "array7.arr[8][9][15]");
  CarbonNetID *arr_9_8_15 = carbonFindNet(obj, "array7.arr[9][8][15]");
  CarbonNetID *arr_9_9_15 = carbonFindNet(obj, "array7.arr[9][9][15]");
  CarbonNetID *arr_10_8_15 = carbonFindNet(obj, "array7.arr[10][8][15]");
  CarbonNetID *arr_10_9_15 = carbonFindNet(obj, "array7.arr[10][9][15]");

  val = 1;
  carbonDeposit(obj, arr_7_8_15, &val, 0);
  carbonDeposit(obj, arr_7_9_15, &val, 0);
  carbonDeposit(obj, arr_8_8_15, &val, 0);
  carbonDeposit(obj, arr_8_9_15, &val, 0);
  carbonDeposit(obj, arr_9_8_15, &val, 0);
  carbonDeposit(obj, arr_9_9_15, &val, 0);
  carbonDeposit(obj, arr_10_8_15, &val, 0);
  val = 0;
  carbonDeposit(obj, arr_10_9_15, &val, 0);

  for (CarbonUInt32 i = 0; i < 4; ++i) {
    for (CarbonUInt32 j = 0; j < 2; ++j) {
      carbonDeposit(obj, addr, &i, 0);
      carbonDeposit(obj, sel, &j, 0);
      val = 0;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      val = 1;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      carbonExamine(obj, dout, &val, 0);
      std::cout << "Data with addr = " << std::dec << i << ", select = " << std::dec << j << " is 0x" << std::hex << val << std::endl;
    }
  }

  // carbonDepositRange should also work
  val = 0x3;
  carbonDepositRange(obj, arr_7_8, &val, 15, 14, 0);
  carbonDepositRange(obj, arr_7_9, &val, 15, 14, 0);
  carbonDepositRange(obj, arr_8_8, &val, 15, 14, 0);
  carbonDepositRange(obj, arr_8_9, &val, 15, 14, 0);
  carbonDepositRange(obj, arr_9_8, &val, 15, 14, 0);
  carbonDepositRange(obj, arr_9_9, &val, 15, 14, 0);
  carbonDepositRange(obj, arr_10_8, &val, 15, 14, 0);
  carbonDepositRange(obj, arr_10_9, &val, 15, 14, 0);

  for (CarbonUInt32 i = 0; i < 4; ++i) {
    for (CarbonUInt32 j = 0; j < 2; ++j) {
      carbonDeposit(obj, addr, &i, 0);
      carbonDeposit(obj, sel, &j, 0);
      val = 0;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      val = 1;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      carbonExamine(obj, dout, &val, 0);
      std::cout << "Data with addr = " << std::dec << i << ", select = " << std::dec << j << " is 0x" << std::hex << val << std::endl;
    }
  }

  // Now test examines
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 4; ++i) {
    for (CarbonUInt32 j = 0; j < 2; ++j) {
      carbonDeposit(obj, addr, &i, 0);
      carbonDeposit(obj, sel, &j, 0);
      val = 0xb - (2 * i) - j;
      carbonDeposit(obj, din, &val, 0);
      val = 0;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      val = 1;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
    }
  }

  carbonExamine(obj, arr_7_8, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_7_9, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_8_8, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_8_9, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_9_8, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_9_9, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_10_8, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_10_9, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;

  carbonExamine(obj, arr_7_8_15, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_7_9_15, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_8_8_15, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_8_9_15, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_9_8_15, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_9_9_15, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_10_8_15, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_10_9_15, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;


  carbonExamineRange(obj, arr_7_8, &val, 15, 13, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamineRange(obj, arr_7_8, &val, 15, 13, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamineRange(obj, arr_8_8, &val, 15, 13, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamineRange(obj, arr_8_8, &val, 15, 13, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamineRange(obj, arr_9_8, &val, 15, 13, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamineRange(obj, arr_9_8, &val, 15, 13, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamineRange(obj, arr_10_8, &val, 15, 13, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamineRange(obj, arr_10_8, &val, 15, 13, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;

  const CarbonDBNode *node_arr = carbonDBFindNode(db, "array7.arr");
  const CarbonDBNode *node_arr_8 = carbonDBFindNode(db, "array7.arr[8]");
  const CarbonDBNode *node_arr_8_9 = carbonNetGetDBNode(obj, arr_8_9);

  carbonDepositArrayFromFile(obj, node_arr, eCarbonHex, "array7.mem");
  
  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 4; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    for (CarbonUInt32 j = 0; j < 2; ++j) {
      carbonDeposit(obj, sel, &j, 0);
      val = 0;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      val = 1;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      carbonExamine(obj, dout, &val, 0);
      std::cout << "Output at address (" << std::dec << i << ", " << j << ") is 0x" << std::hex << val << std::endl;
    }
  }    

  carbonDepositArrayFromFile(obj, node_arr_8, eCarbonHex, "array7.mem2");
  
  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 4; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    for (CarbonUInt32 j = 0; j < 2; ++j) {
      carbonDeposit(obj, sel, &j, 0);
      val = 0;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      val = 1;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      carbonExamine(obj, dout, &val, 0);
      std::cout << "Output at address (" << std::dec << i << ", " << j << ") is 0x" << std::hex << val << std::endl;
    }
  }    

  carbonDepositArrayFromFile(obj, node_arr_8_9, eCarbonHex, "array7.mem3");
  
  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 4; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    for (CarbonUInt32 j = 0; j < 2; ++j) {
      carbonDeposit(obj, sel, &j, 0);
      val = 0;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      val = 1;
      carbonDeposit(obj, clk, &val, 0);
      carbonSchedule(obj, t++);
      carbonExamine(obj, dout, &val, 0);
      std::cout << "Output at address (" << std::dec << i << ", " << j << ") is 0x" << std::hex << val << std::endl;
    }
  }    


  carbonExamineArrayToFile(obj, node_arr, eCarbonHex, "array7.dump");
  carbonExamineArrayToFile(obj, node_arr_8, eCarbonHex, "array7.dump2");
  carbonExamineArrayToFile(obj, node_arr_8_9, eCarbonHex, "array7.dump3");

  carbonDestroy(&obj);
  return 0;
}
