library ieee;
use ieee.std_logic_1164.all;

entity norm9 is
  
  port (
    clk : in  std_logic;
    a   : in  std_logic;
    b   : in  std_logic;
    sel : in  std_logic;
    we  : in  std_logic;
    o   : out std_logic);

end norm9;

architecture arch of norm9 is

  type myrec is record
                  a : std_logic;
                  b : std_logic;
                end record;

  type myarray is array (6 to 7) of myrec;

  signal arr: myarray;
  
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        if sel = '1' then
          arr(7).a <= a;
          arr(7).b <= b;
        else
          arr(6).a <= a;
          arr(6).b <= b;
        end if;
      else
        if sel = '1' then
          o <= arr(7).a or arr(7).b;
        else
          o <= arr(6).a or arr(6).b;
        end if;
      end if;
    end if;
  end process;
  

end arch;
