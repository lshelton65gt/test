// Testing simple arrays of records

#include "libbasic2.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_basic2_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *clk = carbonFindNet(obj, "basic2.clk");
  CarbonNetID *o = carbonFindNet(obj, "basic2.o");
  CarbonNetID *we = carbonFindNet(obj, "basic2.we");
  CarbonNetID *sel = carbonFindNet(obj, "basic2.sel");

  carbonDumpVars(carbonWaveInitFSDB(obj, "basic2.fsdb", e1ns), 0, "basic2");

  CarbonDB *db = carbonGetDB(obj);

  const CarbonDBNode *arr = carbonDBFindNode(db, "basic2.arr");
  int index = 1;
  const CarbonDBNode *arr1 = carbonDBGetArrayElement(db, arr, &index, 1);
  const CarbonDBNode *b = carbonDBGetStructFieldByName(db, arr1, "b");
  std::cout << carbonDBNodeGetFullName(db, b) << " is declared as " << carbonDBdeclarationType(db, b);
  std::cout << " (intrinsic type: " << carbonDBIntrinsicType(db, b) << ")" << std::endl;

  CarbonNetID *net = carbonDBGetCarbonNet(db, b);

  const CarbonDBNode *b_alt = carbonDBFindNode(db, "basic2.arr[1].b");
  if (b_alt != b) {
    std::cout << "pointers don't match" << std::endl;
  }

  CarbonUInt32 val;
  CarbonTime t = 0;
  char binVal[100];

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;
  carbonFormat(obj, o, binVal, 100, eCarbonBin);
  std::cout << "Binary: " << binVal << std::endl;

  val = 1;
  carbonDeposit(obj, sel, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;
  carbonFormat(obj, o, binVal, 100, eCarbonBin);
  std::cout << "Binary: " << binVal << std::endl;

  val = 1;
  carbonDeposit(obj, net, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;
  carbonFormat(obj, o, binVal, 100, eCarbonBin);
  std::cout << "Binary: " << binVal << std::endl;

  val = 0;
  carbonDeposit(obj, sel, &val, 0);

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "Out is " << val << std::endl;
  carbonFormat(obj, o, binVal, 100, eCarbonBin);
  std::cout << "Binary: " << binVal << std::endl;

  carbonDBFree(db);
  carbonDestroy(&obj);
  return 0;
}
