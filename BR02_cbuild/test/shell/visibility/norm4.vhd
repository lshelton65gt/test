library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity norm4 is
  port (
    clk  : in std_logic;
    addr : in std_logic_vector (3 downto 0);
    we   : in std_logic;
    din  : in std_logic_vector (0 to 31);
    dout : out std_logic_vector (0 to 31));
end norm4;

architecture arch of norm4 is

  type myvector is array (integer range <>) of std_logic;
  type myarray is array (-25 to -10) of myvector (-51 to -20);
  signal mem : myarray;
  signal addr_int : integer;

begin

  addr_int <= conv_integer(unsigned(addr)) - 25;
  
  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        for i in din'range loop
          mem(addr_int)(i - 51) <= din(i);
        end loop;
      else
        for i in dout'range loop
          dout(i) <= mem(addr_int)(i - 51);
        end loop;
      end if;
    end if;
  end process;
  
end arch;
