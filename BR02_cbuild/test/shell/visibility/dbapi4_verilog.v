module dbapi4_verilog(input clk,
                      input [2:0] addr1,
                      input signed [3:0] addr2,
                      input we,
                      input [31:0] din,
                      output reg [31:0] dout);

   reg [31:0]                arr [4:11][7:-8];

   integer                   addr1_int, addr2_int;

   always @(*) begin
      addr1_int = addr1 + 4;
      addr2_int = addr2;
   end

   always @(posedge clk)
     if (we)
       arr[addr1_int][addr2_int] <= din;
     else
       dout <= arr[addr1_int][addr2_int];

endmodule
