// Testing arrays with a single element in a dimension

#include "libbasic8.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <cassert>
#include <iostream>

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_basic8_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "basic8.clk");
  CarbonNetID *en = carbonFindNet(obj, "basic8.en");
  CarbonNetID *i = carbonFindNet(obj, "basic8.i");
  CarbonNetID *o = carbonFindNet(obj, "basic8.o");

  CarbonDB *db = carbonGetDB(obj);

  // a
  const CarbonDBNode *node_a = carbonDBFindNode(db, "basic8.a");
  assert(carbonDBIsStruct(db, node_a) == 0);
  assert(carbonDBIsArray(db, node_a) == 0);
  CarbonNetID *a = carbonDBGetCarbonNet(db, node_a);
  assert(a != NULL);
  CarbonNetID *a_alt = carbonFindNet(obj, "basic8.a");
  assert(a_alt == a);

  // b
  const CarbonDBNode *node_b = carbonDBFindNode(db, "basic8.b");
  assert(carbonDBIsStruct(db, node_b) == 0);
  assert(carbonDBIsArray(db, node_b) == 1);
  assert(carbonDBGetArrayDims(db, node_b) == 1);
  assert(carbonDBGetArrayLeftBound(db, node_b) == 0);
  assert(carbonDBGetArrayRightBound(db, node_b) == 0);
  CarbonNetID *b = carbonDBGetCarbonNet(db, node_b);
  assert(b != NULL);
  CarbonNetID *b_alt = carbonFindNet(obj, "basic8.b");
  assert(b_alt == b);

  int index = 0;
  const CarbonDBNode *node_b_0 = carbonDBGetArrayElement(db, node_b, &index, 1);
  assert(carbonDBIsStruct(db, node_b_0) == 0);
  assert(carbonDBIsArray(db, node_b_0) == 0);
  CarbonNetID *b_0 = carbonDBGetCarbonNet(db, node_b_0);
  assert(b_0 != NULL);

  const CarbonDBNode *node_b_0_alt = carbonDBFindNode(db, "basic8.b[0]");
  assert(node_b_0_alt == node_b_0);
  CarbonNet *b_0_alt = carbonFindNet(obj, "basic8.b[0]");
  assert(b_0_alt == b_0);

  // c
  const CarbonDBNode *node_c = carbonDBFindNode(db, "basic8.c");
  assert(carbonDBIsStruct(db, node_c) == 0);
  assert(carbonDBIsArray(db, node_c) == 1);
  assert(carbonDBGetArrayDims(db, node_c) == 1);
  assert(carbonDBGetArrayLeftBound(db, node_c) == 1);
  assert(carbonDBGetArrayRightBound(db, node_c) == 1);
  CarbonNetID *c = carbonDBGetCarbonNet(db, node_c);
  assert(c != NULL);
  CarbonNetID *c_alt = carbonFindNet(obj, "basic8.c");
  assert(c_alt == c);

  index = 1;
  const CarbonDBNode *node_c_1 = carbonDBGetArrayElement(db, node_c, &index, 1);
  assert(carbonDBIsStruct(db, node_c_1) == 0);
  assert(carbonDBIsArray(db, node_c_1) == 0);
  CarbonNetID *c_1 = carbonDBGetCarbonNet(db, node_c_1);
  assert(c_1 != NULL);

  const CarbonDBNode *node_c_1_alt = carbonDBFindNode(db, "basic8.c[1]");
  assert(node_c_1_alt == node_c_1);
  CarbonNet *c_1_alt = carbonFindNet(obj, "basic8.c[1]");
  assert(c_1_alt == c_1);

  // d
  const CarbonDBNode *node_d = carbonDBFindNode(db, "basic8.d");
  assert(carbonDBIsStruct(db, node_d) == 0);
  assert(carbonDBIsArray(db, node_d) == 1);
  assert(carbonDBGetArrayDims(db, node_d) == 2);
  assert(carbonDBGetArrayLeftBound(db, node_d) == 1);
  assert(carbonDBGetArrayRightBound(db, node_d) == 1);
  CarbonNetID *d = carbonDBGetCarbonNet(db, node_d);
  assert(d == NULL);
  CarbonNetID *d_alt = carbonFindNet(obj, "basic8.d");
  assert(d_alt == d);

  index = 1;
  const CarbonDBNode *node_d_1 = carbonDBGetArrayElement(db, node_d, &index, 1);
  assert(carbonDBIsStruct(db, node_d_1) == 0);
  assert(carbonDBIsArray(db, node_d_1) == 1);
  assert(carbonDBGetArrayDims(db, node_d_1) == 1);
  assert(carbonDBGetArrayLeftBound(db, node_d_1) == 2);
  assert(carbonDBGetArrayRightBound(db, node_d_1) == 2);
  CarbonNetID *d_1 = carbonDBGetCarbonNet(db, node_d_1);
  assert(d_1 != NULL);

  const CarbonDBNode *node_d_1_alt = carbonDBFindNode(db, "basic8.d[1]");
  assert(node_d_1_alt == node_d_1);
  CarbonNet *d_1_alt = carbonFindNet(obj, "basic8.d[1]");
  assert(d_1_alt == d_1);

  index = 2;
  const CarbonDBNode *node_d_1_2 = carbonDBGetArrayElement(db, node_d_1, &index, 1);
  assert(carbonDBIsStruct(db, node_d_1_2) == 0);
  assert(carbonDBIsArray(db, node_d_1_2) == 0);
  CarbonNetID *d_1_2 = carbonDBGetCarbonNet(db, node_d_1_2);
  assert(d_1_2 != NULL);

  const CarbonDBNode *node_d_1_2_alt = carbonDBFindNode(db, "basic8.d[1][2]");
  assert(node_d_1_2_alt == node_d_1_2);
  CarbonNet *d_1_2_alt = carbonFindNet(obj, "basic8.d[1][2]");
  assert(d_1_2_alt == d_1_2);

  // e
  const CarbonDBNode *node_e = carbonDBFindNode(db, "basic8.e");
  assert(carbonDBIsStruct(db, node_e) == 0);
  assert(carbonDBIsArray(db, node_e) == 1);
  assert(carbonDBGetArrayDims(db, node_e) == 2);
  assert(carbonDBGetArrayLeftBound(db, node_e) == 1);
  assert(carbonDBGetArrayRightBound(db, node_e) == 1);
  CarbonNetID *e = carbonDBGetCarbonNet(db, node_e);
  assert(e == NULL);
  CarbonNetID *e_alt = carbonFindNet(obj, "basic8.e");
  assert(e_alt == e);

  index = 1;
  const CarbonDBNode *node_e_1 = carbonDBGetArrayElement(db, node_e, &index, 1);
  assert(carbonDBIsStruct(db, node_e_1) == 0);
  assert(carbonDBIsArray(db, node_e_1) == 1);
  assert(carbonDBGetArrayDims(db, node_e_1) == 1);
  assert(carbonDBGetArrayLeftBound(db, node_e_1) == 5);
  assert(carbonDBGetArrayRightBound(db, node_e_1) == 2);
  CarbonNetID *e_1 = carbonDBGetCarbonNet(db, node_e_1);
  assert(e_1 != NULL);

  const CarbonDBNode *node_e_1_alt = carbonDBFindNode(db, "basic8.e[1]");
  assert(node_e_1_alt == node_e_1);
  CarbonNet *e_1_alt = carbonFindNet(obj, "basic8.e[1]");
  assert(e_1_alt == e_1);

  index = 4;
  const CarbonDBNode *node_e_1_4 = carbonDBGetArrayElement(db, node_e_1, &index, 1);
  assert(carbonDBIsStruct(db, node_e_1_4) == 0);
  assert(carbonDBIsArray(db, node_e_1_4) == 0);
  CarbonNetID *e_1_4 = carbonDBGetCarbonNet(db, node_e_1_4);
  assert(e_1_4 != NULL);

  const CarbonDBNode *node_e_1_4_alt = carbonDBFindNode(db, "basic8.e[1][4]");
  assert(node_e_1_4_alt == node_e_1_4);
  CarbonNet *e_1_4_alt = carbonFindNet(obj, "basic8.e[1][4]");
  assert(e_1_4_alt == e_1_4);

  // f
  const CarbonDBNode *node_f = carbonDBFindNode(db, "basic8.f");
  assert(carbonDBIsStruct(db, node_f) == 0);
  assert(carbonDBIsArray(db, node_f) == 1);
  assert(carbonDBGetArrayDims(db, node_f) == 2);
  assert(carbonDBGetArrayLeftBound(db, node_f) == 2);
  assert(carbonDBGetArrayRightBound(db, node_f) == 5);
  CarbonNetID *f = carbonDBGetCarbonNet(db, node_f);
  assert(f == NULL);
  CarbonNetID *f_alt = carbonFindNet(obj, "basic8.f");
  assert(f_alt == f);

  index = 3;
  const CarbonDBNode *node_f_3 = carbonDBGetArrayElement(db, node_f, &index, 1);
  assert(carbonDBIsStruct(db, node_f_3) == 0);
  assert(carbonDBIsArray(db, node_f_3) == 1);
  assert(carbonDBGetArrayDims(db, node_f_3) == 1);
  assert(carbonDBGetArrayLeftBound(db, node_f_3) == 1);
  assert(carbonDBGetArrayRightBound(db, node_f_3) == 1);
  CarbonNetID *f_3 = carbonDBGetCarbonNet(db, node_f_3);
  assert(f_3 != NULL);

  const CarbonDBNode *node_f_3_alt = carbonDBFindNode(db, "basic8.f[3]");
  assert(node_f_3_alt == node_f_3);
  CarbonNet *f_3_alt = carbonFindNet(obj, "basic8.f[3]");
  assert(f_3_alt == f_3);

  index = 1;
  const CarbonDBNode *node_f_3_1 = carbonDBGetArrayElement(db, node_f_3, &index, 1);
  assert(carbonDBIsStruct(db, node_f_3_1) == 0);
  assert(carbonDBIsArray(db, node_f_3_1) == 0);
  CarbonNetID *f_3_1 = carbonDBGetCarbonNet(db, node_f_3_1);
  assert(f_3_1 != NULL);

  const CarbonDBNode *node_f_3_1_alt = carbonDBFindNode(db, "basic8.f[3][1]");
  assert(node_f_3_1_alt == node_f_3_1);
  CarbonNet *f_3_1_alt = carbonFindNet(obj, "basic8.f[3][1]");
  assert(f_3_1_alt == f_3_1);

  // Check examine
  CarbonUInt32 val;
  CarbonTime t = 0;

  val = 0xfff;
  carbonDeposit(obj, i, &val, 0);
  val = 1;
  carbonDeposit(obj, en, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonExamine(obj, a, &val, 0);
  std::cout << "a is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, b, &val, 0);
  std::cout << "b is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, b_0, &val, 0);
  std::cout << "b[0] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, c, &val, 0);
  std::cout << "c is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, c_1, &val, 0);
  std::cout << "c[1] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, d_1, &val, 0);
  std::cout << "d[1] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, d_1_2, &val, 0);
  std::cout << "d[1][2] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, e_1, &val, 0);
  std::cout << "e[1] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, e_1_4, &val, 0);
  std::cout << "e[1][4] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, f_3, &val, 0);
  std::cout << "f[3] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, f_3_1, &val, 0);
  std::cout << "f[3][1] is 0x" << std::hex << val << std::endl;

  val = 0;
  carbonDeposit(obj, i, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonExamine(obj, a, &val, 0);
  std::cout << "a is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, b, &val, 0);
  std::cout << "b is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, b_0, &val, 0);
  std::cout << "b[0] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, c, &val, 0);
  std::cout << "c is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, c_1, &val, 0);
  std::cout << "c[1] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, d_1, &val, 0);
  std::cout << "d[1] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, d_1_2, &val, 0);
  std::cout << "d[1][2] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, e_1, &val, 0);
  std::cout << "e[1] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, e_1_4, &val, 0);
  std::cout << "e[1][4] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, f_3, &val, 0);
  std::cout << "f[3] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, f_3_1, &val, 0);
  std::cout << "f[3][1] is 0x" << std::hex << val << std::endl;

  val = 0x555;
  carbonDeposit(obj, i, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  carbonExamine(obj, a, &val, 0);
  std::cout << "a is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, b, &val, 0);
  std::cout << "b is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, b_0, &val, 0);
  std::cout << "b[0] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, c, &val, 0);
  std::cout << "c is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, c_1, &val, 0);
  std::cout << "c[1] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, d_1, &val, 0);
  std::cout << "d[1] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, d_1_2, &val, 0);
  std::cout << "d[1][2] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, e_1, &val, 0);
  std::cout << "e[1] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, e_1_4, &val, 0);
  std::cout << "e[1][4] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, f_3, &val, 0);
  std::cout << "f[3] is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, f_3_1, &val, 0);
  std::cout << "f[3][1] is 0x" << std::hex << val << std::endl;

  // Check deposit
  val = 0;
  carbonDeposit(obj, en, &val, 0);
  val = 0;
  carbonDeposit(obj, a, &val, 0);
  val = 1;
  carbonDeposit(obj, b, &val, 0);
  val = 0;
  carbonDeposit(obj, c, &val, 0);
  val = 1;
  carbonDeposit(obj, d_1, &val, 0);
  val = 0xa;
  carbonDeposit(obj, e_1, &val, 0);
  val = 1;
  carbonDeposit(obj, f_3, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "o is 0x" << std::hex << val << std::endl;

  val = 0;
  carbonDeposit(obj, b_0, &val, 0);
  val = 1;
  carbonDeposit(obj, c_1, &val, 0);
  val = 0;
  carbonDeposit(obj, d_1_2, &val, 0);
  val = 1;
  carbonDeposit(obj, e_1_4, &val, 0);
  val = 0;
  carbonDeposit(obj, f_3_1, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  carbonExamine(obj, o, &val, 0);
  std::cout << "o is 0x" << std::hex << val << std::endl;

  carbonDestroy(&obj);
  return 0;
}
