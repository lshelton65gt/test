library ieee;
use ieee.std_logic_1164.all;

entity sub is
  port (
    clk : in  std_logic;
    a   : in  std_logic;
    b   : in  std_logic;
    z   : out std_logic);
end sub;

architecture arch of sub is

  type myrec is record
                  x : std_logic;
                end record;

  signal r : myrec;  -- carbon observeSignal
  signal x : std_logic;  -- carbon observeSignal
  signal y : std_logic;  -- carbon observeSignal
  
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      x <= a;
      y <= b;
      r.x <= x and y;
      z <= r.x;
    end if;
  end process;

end arch;

library ieee;
use ieee.std_logic_1164.all;

entity dbapi12 is
  port (
    clk : in  std_logic;
    a1  : in  std_logic;
    a2  : in  std_logic;
    b1  : in  std_logic;
    b2  : in  std_logic;
    z   : out std_logic);
end dbapi12;

architecture arch of dbapi12 is

  component sub
    port (
    clk : in  std_logic;
    a   : in  std_logic;
    b   : in  std_logic;
    z   : out std_logic);
  end component;

  type myrec is record
                  x : std_logic;
                end record;

  signal r : myrec;  -- carbon observeSignal
  signal x : std_logic;  -- carbon observeSignal
  signal sub1out : std_logic;  -- carbon observeSignal
  signal sub2out : std_logic;  -- carbon observeSignal
  signal sub1zz : std_logic;  -- carbon observeSignal
  signal sub2zz : std_logic;  -- carbon observeSignal
  
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      sub1zz <= sub1out;
      sub2zz <= sub2out;
      r.x <= sub1zz or sub2zz;
      x <= r.x;
      z <= x;
    end if;
  end process;
  
  sub1 : sub
    port map (
      clk => clk,
      a   => a1,
      b   => b1,
      z   => sub1out);
    
  sub2 : sub
    port map (
      clk => clk,
      a   => a2,
      b   => b2,
      z   => sub2out);
    
end arch;
