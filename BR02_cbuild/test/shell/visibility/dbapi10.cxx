// Testing reimplemented legacy DB API functions with no user type information

#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <iostream>

int main()
{
  CarbonDB *db = carbonDBCreate("libdbapi10.symtab.db", 1);

  // Get a handle to the top of the design
  const CarbonDBNode *top = carbonDBFindNode(db, "dbapi10");

  // Get a handle to the array
  const CarbonDBNode *arr = carbonDBFindNode(db, "dbapi10.arr");
  assert(carbonDBNodeGetParent(db, arr) == top);

  const CarbonDBNode *arr_alt = carbonDBFindChild(db, top, "arr");
  assert(arr_alt == arr);

  // This should not be reported as a 2-D array, since its base
  // element type is a struct.
  assert(carbonDBIsScalar(db, arr) == 0);
  assert(carbonDBIsVector(db, arr) == 0);
  assert(carbonDBIs2DArray(db, arr) == 0);
  // These shouldn't crash, and should return 0
  assert(carbonDBGetWidth(db, arr) == 0);
  assert(carbonDBGetMSB(db, arr) == 0);
  assert(carbonDBGetLSB(db, arr) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr) == 0);
  assert(carbonDBGet2DArrayRightAddr(db, arr) == 0);

  // Loop the three children by name
  const CarbonDBNode *arr_x = carbonDBFindChild(db, arr, "x");
  assert(arr_x != NULL);
  const CarbonDBNode *arr_y = carbonDBFindChild(db, arr, "y");
  assert(arr_y != NULL);
  const CarbonDBNode *arr_z = carbonDBFindChild(db, arr, "z");
  assert(arr_z != NULL);

  // Due to array resynthesis, x is a memory
  assert(carbonDBIsScalar(db, arr_x) == 0);
  assert(carbonDBIsVector(db, arr_x) == 0);
  assert(carbonDBIs2DArray(db, arr_x) == 1);
  assert(carbonDBGetWidth(db, arr_x) == 8);
  assert(carbonDBGetMSB(db, arr_x) == 7);
  assert(carbonDBGetLSB(db, arr_x) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_x) == 3);
  assert(carbonDBGet2DArrayRightAddr(db, arr_x) == 0);

  // Due to array resynthesis, y is a memory
  assert(carbonDBIsScalar(db, arr_y) == 0);
  assert(carbonDBIsVector(db, arr_y) == 0);
  assert(carbonDBIs2DArray(db, arr_y) == 1);
  assert(carbonDBGetWidth(db, arr_y) == 32);
  assert(carbonDBGetMSB(db, arr_y) == 31);
  assert(carbonDBGetLSB(db, arr_y) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_y) == 31);
  assert(carbonDBGet2DArrayRightAddr(db, arr_y) == 0);

  // Due to array resynthesis, z is a memory
  assert(carbonDBIsScalar(db, arr_z) == 0);
  assert(carbonDBIsVector(db, arr_z) == 0);
  assert(carbonDBIs2DArray(db, arr_z) == 1);
  assert(carbonDBGetWidth(db, arr_z) == 32);
  assert(carbonDBGetMSB(db, arr_z) == 31);
  assert(carbonDBGetLSB(db, arr_z) == 0);
  assert(carbonDBGet2DArrayLeftAddr(db, arr_z) == 511);
  assert(carbonDBGet2DArrayRightAddr(db, arr_z) == 0);

  // Test population of loop functions
  std::cout << "Observable nets:" << std::endl;
  CarbonDBNodeIter *iter = carbonDBLoopObservable(db);
  const CarbonDBNode *node;
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    std::cout << "  " << carbonDBNodeGetFullName(db, node) << std::endl;
  }
  carbonDBFreeNodeIter(iter);

  carbonDBFree(db);
  return 0;
}
