library ieee;
use ieee.std_logic_1164.all;

entity bug8400 is
  port (
    clk  : in  std_logic;
    i    : in  std_logic_vector (31 downto 0);
    addr : in  integer range 0 to 255;
    we   : in  std_logic;
    o    : out std_logic_vector (31 downto 0));
end bug8400;

architecture arch of bug8400 is

  type myarray is array (0 to 255) of std_logic_vector (31 downto 0);
  signal mem : myarray;
  
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        mem(addr) <= i;
      else
        o <= mem(addr);
      end if;
    end if;
  end process;
  
end arch;
