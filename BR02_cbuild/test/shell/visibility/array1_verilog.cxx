// Testing 2-D arrays

#include "libarray1_verilog.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>
#include <cassert>

int main(int argc, char** argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_array1_verilog_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonNetID *clk = carbonFindNet(obj, "array1_verilog.clk");
  CarbonNetID *sel = carbonFindNet(obj, "array1_verilog.sel");
  CarbonNetID *we = carbonFindNet(obj, "array1_verilog.we");
  CarbonNetID *din = carbonFindNet(obj, "array1_verilog.din");
  CarbonNetID *dout = carbonFindNet(obj, "array1_verilog.dout");

  // Dump VCD to test bug 8357
  carbonDumpVars(carbonWaveInitVCD(obj, "array1_verilog.vcd", e1ns), 0, "array1_verilog");

  CarbonDB *db = carbonGetDB(obj);

  const CarbonDBNode *node_arr = carbonDBFindNode(db, "array1_verilog.arr");
  int index = 8;
  const CarbonDBNode *node_arr_8 = carbonDBGetArrayElement(db, node_arr, &index, 1);
  CarbonNetID *arr_8 = carbonDBGetCarbonNet(db, node_arr_8);
  index = 9;
  const CarbonDBNode *node_arr_9 = carbonDBGetArrayElement(db, node_arr, &index, 1);
  CarbonNetID *arr_9 = carbonDBGetCarbonNet(db, node_arr_9);

  // The old carbonFindMemory should still work with this
  CarbonMemoryID* mem = carbonFindMemory(obj, "array1_verilog.arr");
  assert(mem != NULL);

  CarbonUInt32 val, val2;
  CarbonTime t = 0;

  val = 0x1;
  carbonDeposit(obj, arr_8, &val, 0);
  val = 0x2;
  carbonDeposit(obj, arr_9, &val, 0);

  for (CarbonUInt32 i = 0; i < 2; ++i) {
    carbonDeposit(obj, sel, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data with select = " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  index = 14;
  const CarbonDBNode *node_arr_8_14 = carbonDBGetArrayElement(db, node_arr_8, &index, 1);
  CarbonNetID *arr_8_14 = carbonDBGetCarbonNet(db, node_arr_8_14);
  const CarbonDBNode *node_arr_9_14 = carbonDBGetArrayElement(db, node_arr_9, &index, 1);
  CarbonNetID *arr_9_14 = carbonDBGetCarbonNet(db, node_arr_9_14);

  val = 0x1;
  carbonDeposit(obj, arr_8_14, &val, 0);
  carbonDeposit(obj, arr_9_14, &val, 0);

  for (CarbonUInt32 i = 0; i < 2; ++i) {
    carbonDeposit(obj, sel, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data with select = " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  // carbonDepositRange should also work
  val = 0x2;
  carbonDepositRange(obj, arr_8, &val, 15, 14, 0);
  carbonDepositRange(obj, arr_9, &val, 15, 14, 0);

  for (CarbonUInt32 i = 0; i < 2; ++i) {
    carbonDeposit(obj, sel, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "Data with select = " << std::dec << i << " is 0x" << std::hex << val << std::endl;
  }

  // Now test examines
  val = 0;
  carbonDeposit(obj, sel, &val, 0);
  val = 0xb;
  carbonDeposit(obj, din, &val, 0);
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  
  val = 1;
  carbonDeposit(obj, sel, &val, 0);
  val = 0xc;
  carbonDeposit(obj, din, &val, 0);
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  
  carbonExamine(obj, arr_8, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  // Memory API should return same value
  carbonExamineMemory(mem, 8, &val2);
  assert(val2 == val);
  carbonExamine(obj, arr_9, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  // Memory API should return same value
  carbonExamineMemory(mem, 9, &val2);
  assert(val2 == val);

  carbonExamine(obj, arr_8_14, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  carbonExamine(obj, arr_9_14, &val, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;

  carbonExamineRange(obj, arr_8, &val, 15, 13, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  // Memory API should return same value
  val2 = 0;     // bug 8270
  carbonExamineMemoryRange(mem, 8, &val2, 15, 13);
  assert(val2 == val);
  carbonExamineRange(obj, arr_9, &val, 15, 13, 0);
  std::cout << "Examine value is 0x" << std::hex << val << std::endl;
  // Memory API should return same value
  val2 = 0;     // bug 8270
  carbonExamineMemoryRange(mem, 9, &val2, 15, 13);
  assert(val2 == val);

  carbonDestroy(&obj);
  return 0;
}
