// Testing illegal API requests

#include "libdbapi5.h"
#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <cstring>

int main(int argc, char** argv)
{
  bool standalone = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-standalone") == 0)
      standalone = true;
  }

  CarbonObjectID *obj = NULL;
  CarbonDB *db;
  if (standalone) {
    db = carbonDBCreate("libdbapi5.symtab.db", 1);
  } else {
    obj = carbon_dbapi5_create(eCarbonFullDB, eCarbon_NoFlags);
    db = carbonGetDB(obj);
  }

  int val;
  const CarbonDBNode *node;
  const char *name;
  CarbonNetID *net;

  // Get a handle to the array
  const CarbonDBNode *arr = carbonDBFindNode(db, "dbapi5.arr");
  assert(carbonDBIsArray(db, arr) == 1);
  assert(carbonDBIsStruct(db, arr) == 0);

  // Try some struct functions
  val = carbonDBGetNumStructFields(db, arr);
  assert(val == -1);
  node = carbonDBGetStructFieldByName(db, arr, "foo");
  assert(node == NULL);
  node = carbonDBGetStructFieldByName(db, arr, NULL);
  assert(node == NULL);

  // Name lookup should fail, too.
  node = carbonDBFindNode(db, "dbapi5.arr.foo");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr.foo");
    assert(net == NULL);
  }
  node = carbonDBFindNode(db, "dbapi5.arr.foo.bar");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr.foo.bar");
    assert(net == NULL);
  }

  // Try to exceed the array's dimensions
  int indices[4];
  indices[0] = 1;
  indices[1] = 2;
  indices[2] = 3;
  indices[3] = 4;
  node = carbonDBGetArrayElement(db, arr, indices, 4);
  assert(node == 0);

  // Try to exceed its bounds
  indices[0] = 20;
  node = carbonDBGetArrayElement(db, arr, indices, 1);
  assert(node == 0);

  indices[0] = 5;
  indices[1] = -5;
  node = carbonDBGetArrayElement(db, arr, indices, 2);
  assert(node == 0);

  // Same thing with strings
  node = carbonDBFindNode(db, "dbapi5.arr[1][2][3][4]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[1][2][3][4]");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[20]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[20]");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[5][-5]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[5][-5]");
    assert(net == NULL);
  }



  // Now, get a node for the struct
  indices[0] = 5;
  indices[1] = 2;
  const CarbonDBNode* str = carbonDBGetArrayElement(db, arr, indices, 2);
  assert(carbonDBIsArray(db, str) == 0);
  assert(carbonDBIsStruct(db, str) == 1);
  
  // Make sure array functions don't work
  val = carbonDBGetArrayLeftBound(db, str);
  assert(val == -1);
  val = carbonDBGetArrayRightBound(db, str);
  assert(val == -1);
  val = carbonDBGetArrayDims(db, str);
  assert(val == -1);
  node = carbonDBGetArrayElement(db, str, indices, 2);
  assert(node == NULL);
  node = carbonDBGetArrayElement(db, str, indices, 1);
  assert(node == NULL);
  node = carbonDBGetArrayElement(db, str, NULL, 0);
  assert(node == NULL);

  // Name lookup should fail, too.
  node = carbonDBFindNode(db, "dbapi5.arr[5][2][5][2]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[5][2][5][2]");
    assert(net == NULL);
  }
  node = carbonDBFindNode(db, "dbapi5.arr[5][2][5]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[5][2][5]");
    assert(net == NULL);
  }

  // Try to get non-existent fields
  node = carbonDBGetStructFieldByName(db, str, "foo");
  assert(node == NULL);
  node = carbonDBGetStructFieldByName(db, str, NULL);
  assert(node == NULL);

  // Name lookup should fail, too.
  node = carbonDBFindNode(db, "dbapi5.arr[5][2].foo");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[5][2].foo");
    assert(net == NULL);
  }
  node = carbonDBFindNode(db, "dbapi5.arr[5][2].foo.bar");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[5][2].foo.bar");
    assert(net == NULL);
  }



  // Try to get an unindexed instance of the struct.  This should
  // fail because it's not a valid visibility node
  const CarbonDBNode *str2 = carbonDBFindNode(db, "dbapi5.arr.r");
  assert(str2 == NULL);

  // Shouldn't be able to get a net for it
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr.r");
    assert(net == NULL);
  }

  // Continue with a child
  const CarbonDBNode *str3 = carbonDBFindNode(db, "dbapi5.arr.r.x");
  assert(str3 == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr.r.x");
    assert(net == NULL);
  }

  // And this isn't a valid array reference, so the node isn't valid
  node = carbonDBFindNode(db, "dbapi5.arr.r.x[0]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr.r.x[0]");
    assert(net == NULL);
  }

  // This node should also be invalid, because the array index is missing
  const CarbonDBNode *str4 = carbonDBFindNode(db, "dbapi5.arr.z");
  assert(str4 == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr.z");
    assert(net == NULL);
  }

  // And this isn't a valid array reference, so the node isn't valid
  node = carbonDBFindNode(db, "dbapi5.arr.z[0]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr.z[0]");
    assert(net == NULL);
  }


  // Not specifying enough dimensions shouldn't return anything
  node = carbonDBFindNode(db, "dbapi5.arr[1].r");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[1].r");
    assert(net == NULL);
  }
  node = carbonDBFindNode(db, "dbapi5.arr[1].z");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[1].z");
    assert(net == NULL);
  }



  // Make sure this doesn't crash
  net = carbonDBGetCarbonNet(db, NULL);
  assert(net == NULL);



  // Test some badly-formed requests
  node = carbonDBFindNode(db, "dbapi5.arr[1");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[1");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[[1]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[[1]");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[[1]]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[[1]]");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[1]]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[1]]");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr1]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr1]");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[]1");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[]1");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr][1");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr][1");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[]");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[x]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[x]");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[1][]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[1][]");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[][1]");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[][1]");
    assert(net == NULL);
  }

  node = carbonDBFindNode(db, "dbapi5.arr[1][].z");
  assert(node == NULL);
  if (!standalone) {
    net = carbonFindNet(obj, "dbapi5.arr[1][].z");
    assert(net == NULL);
  }

  carbonDBFree(db);
  if (!standalone) {
    carbonDestroy(&obj);
  }
  return 0;
}
