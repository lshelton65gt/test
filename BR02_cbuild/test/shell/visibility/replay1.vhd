library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity replay1 is
  
  port (
    a     : in  std_logic_vector(6 downto 0);
    b     : in  std_logic_vector(6 downto 0);
    clk   : in  std_logic;
    o     : out std_logic_vector(6 downto 0);
    we    : in  std_logic;
    addr1 : in  std_logic_vector(3 downto 0);
    addr2 : in  std_logic_vector(4 downto 0);
    addr3 : in  std_logic_vector(2 downto 0);
    count : out std_logic_vector(31 downto 0));

end replay1;

architecture arch of replay1 is

  type subarray is array (10 downto 6) of std_logic_vector (19 downto 13);

  type myrec is record
                  a : subarray;
                  b : subarray;
                end record;

  type myarray is array (22 downto 12, 19 downto 1) of myrec;

  signal arr : myarray;

  signal addr1_int : integer;
  signal addr2_int : integer;
  signal addr3_int : integer;

  signal count_int : std_logic_vector (31 downto 0) := (others => '0');
  
begin

  addr1_int <= conv_integer(unsigned(addr1)) + 12;
  addr2_int <= conv_integer(unsigned(addr2)) + 1;
  addr3_int <= conv_integer(unsigned(addr3)) + 6;
  count <= count_int;
  
  process (clk)
  begin
    if  clk'event and clk = '1' then
      if we = '1' then
        arr(addr1_int, addr2_int).a(addr3_int) <= a;
        arr(addr1_int, addr2_int).b(addr3_int) <= b;
      else
        o <= arr(addr1_int, addr2_int).a(addr3_int) or arr(addr1_int, addr2_int).b(addr3_int);
      end if;
      
      count_int <= count_int + 1;
    end if;
  end process;
  
end arch;
