module array8_verilog(input clk,
                      input we,
                      input [1:0] addr,
                      input sel,
                      input [3:0] din,
                      output reg [3:0] dout);

   reg [15:12]               arr [10:7][8:9];
   integer                   addr_int;

   always @(addr)
     addr_int = addr + 7;

   always @(posedge clk)
     if (we)
       if (sel) begin
          arr[addr_int][9][15] <= din[3];
          arr[addr_int][9][14] <= din[2];
          arr[addr_int][9][13] <= din[1];
          arr[addr_int][9][12] <= din[0];
       end
       else begin
          arr[addr_int][8][15] <= din[3];
          arr[addr_int][8][14] <= din[2];
          arr[addr_int][8][13] <= din[1];
          arr[addr_int][8][12] <= din[0];
       end
     else
       if (sel) begin
          dout[3] <= arr[addr_int][9][15];
          dout[2] <= arr[addr_int][9][14];
          dout[1] <= arr[addr_int][9][13];
          dout[0] <= arr[addr_int][9][12];
       end
       else begin
          dout[3] <= arr[addr_int][8][15];
          dout[2] <= arr[addr_int][8][14];
          dout[1] <= arr[addr_int][8][13];
          dout[0] <= arr[addr_int][8][12];
       end
       
endmodule
