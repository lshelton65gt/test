// Testing iteration of record ports, both primary and internal

#include "libdbapi11.h"
#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <iostream>
#include <vector>
#include <cstring>

typedef std::vector<CarbonNetID*> NetVector;
typedef std::vector<const CarbonDBNode*> NodeVector;

void checkValue(CarbonObjectID* obj, CarbonDB* db, CarbonUInt32 index, CarbonUInt32 expected, const NodeVector& nodeVec, const NetVector& netVec)
{
  // Look up the node and net
  const CarbonDBNode* node = nodeVec[index];
  CarbonNetID* net = netVec[index];

  // If the node is a scalar, only the LSB of the expected value will be there.
  if (carbonDBIsScalar(db, node)) {
    expected &= 0x1;
  }

  CarbonUInt32 actual;
  carbonExamine(obj, net, &actual, 0);
  if (actual != expected) {
    std::cout << "Wrong value for " << carbonDBNodeGetFullName(db, node) << ": actual 0x" << std::hex << actual << " expected 0x" << expected << std::endl;
  }
} 

int main()
{
  CarbonObjectID* obj = carbon_dbapi11_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonDB* db = carbonGetDB(obj);

  CarbonNetID* clk;
  CarbonNetID* en;
  NetVector inputNets;
  NetVector observeNets;
  NodeVector observeNodes;

  CarbonDBNodeIter* iter;
  const CarbonDBNode* node;

  // Loop over all primary inputs.  This includes both a complex
  // record an an array of those records.
  std::cout << "Primary inputs:" << std::endl;
  iter = carbonDBLoopPrimaryInputs(db);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    const char* name = carbonDBNodeGetFullName(db, node);
    std::cout << "  " << name << std::endl;
    // Get a net for this
    CarbonNetID* net = carbonDBGetCarbonNet(db, node);
    assert(net != NULL);
    // If this is clock or enable, set the specific net, else push it
    // into the input vector list
    if (strcmp(name, "dbapi11.clk") == 0) {
      clk = net;
    } else if (strcmp(name, "dbapi11.en") == 0) {
      en = net;
    } else {
      inputNets.push_back(net);
    }
  }
  carbonDBFreeNodeIter(iter);

  // Loop over all observable nets.  This includes both a complex
  // record an an array of those records.
  std::cout << "Observable nets:" << std::endl;
  iter = carbonDBLoopObservable(db);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    std::cout << "  " << carbonDBNodeGetFullName(db, node) << std::endl;
    // Get a net for this
    CarbonNetID* net = carbonDBGetCarbonNet(db, node);
    assert(net != NULL);
    // Save both the node and net
    observeNets.push_back(net);
    observeNodes.push_back(node);
  }
  carbonDBFreeNodeIter(iter);

  // The set of filtered inputs should be the same size as the set of
  // observes.
  CarbonUInt32 numNets = inputNets.size();
  assert(numNets == observeNets.size());

  // Since the observed nets are just registered versions of the
  // inputs, we can deposit to the inputs, run a clock, and check the
  // observes to make sure they match.
  CarbonUInt32 val;
  CarbonTime t = 0;

  // First, write all ones
  val = 0xffffffff;
  for (CarbonUInt32 i = 0; i < numNets; ++i) {
    carbonDeposit(obj, inputNets[i], &val, 0);
  }

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Check the observe values
  for (CarbonUInt32 i = 0; i < numNets; ++i) {
    checkValue(obj, db, i, 0xffffffff, observeNodes, observeNets);
  }

  // Now, write zero
  val = 0x0;
  for (CarbonUInt32 i = 0; i < numNets; ++i) {
    carbonDeposit(obj, inputNets[i], &val, 0);
  }

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Check the observe values
  for (CarbonUInt32 i = 0; i < numNets; ++i) {
    checkValue(obj, db, i, 0x0, observeNodes, observeNets);
  }

  // Finally, do incrementing values
  for (CarbonUInt32 i = 0; i < numNets; ++i) {
    carbonDeposit(obj, inputNets[i], &i, 0);
  }

  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Check the observe values
  for (CarbonUInt32 i = 0; i < numNets; ++i) {
    checkValue(obj, db, i, i, observeNodes, observeNets);
  }

  carbonDestroy(&obj);
  return 0;
}
