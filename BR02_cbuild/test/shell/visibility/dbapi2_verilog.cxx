// Testing 2-D arrays

#include "libdbapi2_verilog.h"
#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <cstdlib>
#include <cstring>

int main(int argc, char** argv)
{
  bool standalone = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-standalone") == 0)
      standalone = true;
  }

  CarbonObjectID *obj = NULL;
  CarbonDB *db;
  if (standalone) {
    db = carbonDBCreate("libdbapi2_verilog.symtab.db", 1);
  } else {
    obj = carbon_dbapi2_verilog_create(eCarbonFullDB, eCarbon_NoFlags);
    db = carbonGetDB(obj);
    // Dump waveforms, to test bug 8340
    carbonDumpVars(carbonWaveInitFSDB(obj, "dbapi2_verilog.fsdb", e1ns), 0, "dbapi2_verilog");
  }

  // Get a handle to the array
  const CarbonDBNode *arr = carbonDBFindNode(db, "dbapi2_verilog.arr");
  assert(carbonDBIsArray(db, arr) == 1);
  assert(carbonDBGetArrayDims(db, arr) == 2);
  assert(carbonDBGetArrayLeftBound(db, arr) == 15);
  assert(carbonDBGetArrayRightBound(db, arr) == 0);
  // Get a CarbonNetID for the whole memory, which should fail
  CarbonNetID *net_arr = carbonDBGetCarbonNet(db, arr);
  assert(net_arr == NULL);

  // Get a handle to element 5 of the array, which is std_logic_vector, and therefore an array
  int index = 5;
  const CarbonDBNode *arr5 = carbonDBGetArrayElement(db, arr, &index, 1);
  assert(carbonDBIsArray(db, arr5) == 1);
  assert(carbonDBGetArrayDims(db, arr5) == 1);
  assert(carbonDBGetArrayLeftBound(db, arr5) == 31);
  assert(carbonDBGetArrayRightBound(db, arr5) == 0);

  // Get a CarbonNetID for the whole memory word, and deposit to it
  CarbonNetID *net_arr5 = carbonDBGetCarbonNet(db, arr5);
  if (standalone) {
    assert(net_arr5 == NULL);
  } else {
    assert(net_arr5 != NULL);
  }

  // Get a handle to element 2 of the subarray, i.e. arr(5)(2)
  index = 2;
  const CarbonDBNode *arr5_2 = carbonDBGetArrayElement(db, arr5, &index, 1);
  assert(carbonDBIsArray(db, arr5_2) == 0);

  // Get a CarbonNetID for the bit, and deposit to it
  CarbonNetID *net_arr5_2 = carbonDBGetCarbonNet(db, arr5_2);
  if (standalone) {
    assert(net_arr5_2 == NULL);
  } else {
    assert(net_arr5_2 != NULL);
  }

  // Alternatively, we could have specified all the dimensions at once
  int indices[2];
  indices[0] = 5;
  indices[1] = 2;
  const CarbonDBNode *arr5_2_alt = carbonDBGetArrayElement(db, arr, indices, 2);
  // Everything is cached in the CarbonDatabase, so the pointers should be identical
  assert(arr5_2 == arr5_2_alt);

  if (!standalone) {
    const CarbonDBNode *node = carbonNetGetDBNode(obj, net_arr5_2);
    assert(node == arr5_2);
  }

  carbonDBFree(db);
  if (!standalone) {
    carbonDestroy(&obj);
  }
  return 0;
}
