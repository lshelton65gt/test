library ieee;
use ieee.std_logic_1164.all;

package mypack is
  type subrec is record
                   a : std_logic;
                   b : std_logic_vector (31 downto 0);
                 end record;

  type midrec is record
                   c : subrec;
                   d : std_logic;
                 end record;
  
  type subarray is array (3 downto 0) of midrec;


  type myrec is record
                  w : std_logic;
                  x : std_logic_vector (31 downto 0);
                  y : midrec;
                  z : subarray;
                end record;

  type myarray is array (1 downto 0) of myrec;
  
end mypack;

library ieee;
use ieee.std_logic_1164.all;
use work.mypack.all;

entity sub is
  port (
    clk    : in  std_logic;
    recin  : in  myrec;      -- carbon observeSignal
    arrin  : in  myarray;    -- carbon observeSignal
    en     : in  std_logic;
    recout : out myrec;
    arrout : out myarray);
end sub;

architecture arch of sub is

  signal recreg : myrec;
  signal arrreg : myarray;

begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if en = '1' then
        recreg <= recin;
        arrreg <= arrin;
      end if;
    end if;
  end process;

  recout <= recreg;
  arrout <= arrreg;
  
end arch;

library ieee;
use ieee.std_logic_1164.all;
use work.mypack.all;

entity dbapi11 is
  port (
    clk    : in  std_logic;
    recin  : in  myrec;
    arrin  : in  myarray;
    en     : in  std_logic;
    recout : out myrec;
    arrout : out myarray);
end dbapi11;

architecture arch of dbapi11 is

  component sub
    port (
      clk    : in  std_logic;
      recin  : in  myrec;
      arrin  : in  myarray;
      en     : in  std_logic;
      recout : out myrec;
      arrout : out myarray);
  end component;

  signal recin2 : myrec;
  signal arrin2 : myarray;

begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      recin2 <= recin;
      arrin2 <= arrin;
    end if;
  end process;
  
  sub0 : sub
    port map (
      clk    => clk,
      recin  => recin2,
      arrin  => arrin2,
      en     => en,
      recout => recout,
      arrout => arrout);
  
end arch;
