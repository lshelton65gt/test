// Testing non-bit scalars (integers, chars, reals)

#include "libscalar1.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>
#include <cassert>

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_scalar1_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *clk = carbonFindNet(obj, "scalar1.clk");
  CarbonNetID *we = carbonFindNet(obj, "scalar1.we");
  CarbonNetID *int32in = carbonFindNet(obj, "scalar1.int32in");
  CarbonNetID *int16in = carbonFindNet(obj, "scalar1.int16in");
  CarbonNetID *int8in = carbonFindNet(obj, "scalar1.int8in");
  CarbonNetID *charin = carbonFindNet(obj, "scalar1.charin");
#if 0
  // We don't support reals in VHDL yet
  CarbonNetID *realin = carbonFindNet(obj, "scalar1.realin");
#endif
  CarbonNetID *int32out = carbonFindNet(obj, "scalar1.int32out");
  CarbonNetID *int16out = carbonFindNet(obj, "scalar1.int16out");
  CarbonNetID *int8out = carbonFindNet(obj, "scalar1.int8out");
  CarbonNetID *charout = carbonFindNet(obj, "scalar1.charout");
#if 0
  CarbonNetID *realout = carbonFindNet(obj, "scalar1.realout");
#endif
  
  CarbonDB *db = carbonGetDB(obj);

  // Get the record
  const CarbonDBNode *r = carbonDBFindNode(db, "scalar1.r");

  // Get each field.  They should all be scalars

  const CarbonDBNode *node_r_int32 = carbonDBGetStructFieldByName(db, r, "int32");
  assert(carbonDBIsScalar(db, node_r_int32) == 1);
  assert(carbonDBGetWidth(db, node_r_int32) == 32);
  const CarbonDBNode *node_r_int16 = carbonDBGetStructFieldByName(db, r, "int16");
  assert(carbonDBIsScalar(db, node_r_int16) == 1);
  assert(carbonDBGetWidth(db, node_r_int16) == 16);
  const CarbonDBNode *node_r_int8 = carbonDBGetStructFieldByName(db, r, "int8");
  assert(carbonDBIsScalar(db, node_r_int8) == 1);
  // Despite the name, this is a 9-bit integer
  assert(carbonDBGetWidth(db, node_r_int8) == 9);
  const CarbonDBNode *node_r_c = carbonDBGetStructFieldByName(db, r, "c");
  assert(carbonDBIsScalar(db, node_r_c) == 1);
  assert(carbonDBGetWidth(db, node_r_c) == 8);
#if 0
  const CarbonDBNode *node_r_r = carbonDBGetStructFieldByName(db, r, "r");
  assert(carbonDBIsScalar(db, node_r_r) == 1);
#endif

  // Get nets for each
  CarbonNetID *r_int32 = carbonDBGetCarbonNet(db, node_r_int32);
  CarbonNetID *r_int16 = carbonDBGetCarbonNet(db, node_r_int16);
  CarbonNetID *r_int8 = carbonDBGetCarbonNet(db, node_r_int8);
  CarbonNetID *r_c = carbonDBGetCarbonNet(db, node_r_c);
#if 0
  CarbonNetID *r_r = carbonDBGetCarbonNet(db, node_r_r);
#endif

  CarbonUInt32 val;
  // TODO - how does the API interact with reals?
  CarbonReal realval;
  CarbonTime t = 0;

  // Deposit to all inputs and set we
  val = 0x12345678;
  carbonDeposit(obj, int32in, &val, 0);
  val = 0x90ab;
  carbonDeposit(obj, int16in, &val, 0);
  val = 0xcd;
  carbonDeposit(obj, int8in, &val, 0);
  val = 0xef;
  carbonDeposit(obj, charin, &val, 0);
#if 0
  realval = 3.14159;
  carbonDeposit(obj, realin, reinterpret_cast<CarbonUInt32*>(&realval), 0);
#endif

  val = 1;
  carbonDeposit(obj, we, &val, 0);

  // Run a clock
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Print the values of the struct elements
  carbonExamine(obj, r_int32, &val, 0);
  std::cout << "r.int32 is " << std::hex << val << std::endl;
  carbonExamine(obj, r_int16, &val, 0);
  std::cout << "r.int16 is " << std::hex << val << std::endl;
  carbonExamine(obj, r_int8, &val, 0);
  std::cout << "r.int8 is " << std::hex << val << std::endl;
  carbonExamine(obj, r_c, &val, 0);
  std::cout << "r.c is " << std::hex << val << std::endl;

#if 0
  realval = 0.0;
  carbonExamine(obj, r_r, reinterpret_cast<CarbonUInt32*>(&realval), 0);
  std::cout << "r.r is " << std::hex << realval << std::endl;
#endif

  // Clear we, and deposit to the struct elements
  val = 0;
  carbonDeposit(obj, we, &val, 0);

  val = 0xfedcba09;
  carbonDeposit(obj, r_int32, &val, 0);
  val = 0x8765;
  carbonDeposit(obj, r_int16, &val, 0);
  val = 0x43;
  carbonDeposit(obj, r_int8, &val, 0);
  val = 0x21;
  carbonDeposit(obj, r_c, &val, 0);


  // Run a clock
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Print the values of the outputs
  carbonExamine(obj, int32out, &val, 0);
  std::cout << "int32out is " << std::hex << val << std::endl;
  carbonExamine(obj, int16out, &val, 0);
  std::cout << "int16out is " << std::hex << val << std::endl;
  carbonExamine(obj, int8out, &val, 0);
  std::cout << "int8out is " << std::hex << val << std::endl;
  carbonExamine(obj, charout, &val, 0);
  std::cout << "charout is " << std::hex << val << std::endl;

  carbonDBFree(db);
  carbonDestroy(&obj);
  return 0;
}
