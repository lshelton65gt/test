#include "libbug8358_1.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  CarbonObjectID *obj = carbon_bug8358_1_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID *clk = carbonFindNet(obj, "bug8358_1.clk");
  CarbonNetID *we = carbonFindNet(obj, "bug8358_1.we");
  CarbonNetID *i = carbonFindNet(obj, "bug8358_1.i");
  CarbonNetID *addr = carbonFindNet(obj, "bug8358_1.addr");

  carbonDumpVars(carbonWaveInitFSDB(obj, "bug8358_1.fsdb", e1ns), 0, "bug8358_1");

  CarbonUInt32 val;
  CarbonTime t = 0;

  val = 1;
  carbonDeposit(obj, we, &val, 0);

  for (CarbonUInt32 j = 0; j < 16; ++j) {
    carbonDeposit(obj, addr, &j, 0);
    carbonDeposit(obj, i, &j, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  carbonDestroy(&obj);
  return 0;
}
