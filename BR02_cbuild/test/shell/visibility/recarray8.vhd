library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity recarray8 is
  
  port (
    a     : in  std_logic_vector(0 to 6);
    b     : in  std_logic_vector(0 to 6);
    clk   : in  std_logic;
    o     : out std_logic_vector(0 to 6);
    we    : in  std_logic;
    addr1 : in  std_logic_vector(3 downto 0);
    addr2 : in  std_logic_vector(4 downto 0);
    addr3 : in  std_logic_vector(2 downto 0));

end recarray8;

architecture arch of recarray8 is

  type subarray is array (6 to 10) of std_logic_vector (13 to 19);

  type myrec is record
                  a : subarray;
                  b : subarray;
                end record;

  type myarray is array (12 to 22, 1 to 19) of myrec;

  signal arr : myarray;

  signal addr1_int : integer;
  signal addr2_int : integer;
  signal addr3_int : integer;
  
begin

  addr1_int <= conv_integer(unsigned(addr1)) + 12;
  addr2_int <= conv_integer(unsigned(addr2)) + 1;
  addr3_int <= conv_integer(unsigned(addr3)) + 6;
  
  process (clk)
  begin
    if  clk'event and clk = '1' then
      if we = '1' then
        arr(addr1_int, addr2_int).a(addr3_int) <= a;
        arr(addr1_int, addr2_int).b(addr3_int) <= b;
      else
        o <= arr(addr1_int, addr2_int).a(addr3_int) or arr(addr1_int, addr2_int).b(addr3_int);
      end if;
      
    end if;
  end process;
  
end arch;
