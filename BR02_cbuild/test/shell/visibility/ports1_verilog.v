module ports1_verilog(input clk,
                      input [7:0] i,
                      output reg [7:0] o);

   always @(posedge clk)
     o <= i;
endmodule
