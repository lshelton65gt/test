module ondemand4(clk, rst, out);
   input clk, rst;
   output [3:0] out;

   reg [3:0]    count;

   always @(posedge clk or posedge rst)
     if (rst)
       count <= 4'b0;
     else
       count <= count + 4'b1;

   assign       out = count;

endmodule
