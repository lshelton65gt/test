// Testing 3-D arrays

#include "libdbapi4_verilog.h"
#include "carbon/carbon_dbapi.h"
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <iostream>

int main(int argc, char** argv)
{
  bool standalone = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-standalone") == 0)
      standalone = true;
  }

  CarbonObjectID *obj = NULL;
  CarbonDB *db;
  if (standalone) {
    db = carbonDBCreate("libdbapi4_verilog.symtab.db", 1);
  } else {
    obj = carbon_dbapi4_verilog_create(eCarbonFullDB, eCarbon_NoFlags);
    db = carbonGetDB(obj);
  }

  // Get a handle to the array
  const CarbonDBNode *arr = carbonDBFindNode(db, "dbapi4_verilog.arr");
  assert(carbonDBIsArray(db, arr) == 1);
  assert(carbonDBGetArrayDims(db, arr) == 3);
  assert(carbonDBGetArrayLeftBound(db, arr) == 4);
  assert(carbonDBGetArrayRightBound(db, arr) == 11);
  // Get a CarbonNetID for the whole memory, which should fail
  CarbonNetID *net_arr = carbonDBGetCarbonNet(db, arr);
  assert(net_arr == NULL);

  // Get a handle to element 5 of the array, which is a 2-D array
  int index = 5;
  const CarbonDBNode *arr5 = carbonDBGetArrayElement(db, arr, &index, 1);
  assert(carbonDBIsArray(db, arr5) == 1);
  assert(carbonDBGetArrayDims(db, arr5) == 2);
  assert(carbonDBGetArrayLeftBound(db, arr5) == 7);
  assert(carbonDBGetArrayRightBound(db, arr5) == -8);

  // Get a CarbonNetID for it, which should fail
  CarbonNetID *net_arr5 = carbonDBGetCarbonNet(db, arr5);
  assert(net_arr5 == NULL);

  // Get a handle to element -2 of the subarray, i.e. arr(5)(-2), which is a std_logic_vector
  index = -2;
  const CarbonDBNode *arr5_2 = carbonDBGetArrayElement(db, arr5, &index, 1);
  assert(carbonDBIsArray(db, arr5_2) == 1);
  assert(carbonDBGetArrayDims(db, arr5_2) == 1);
  assert(carbonDBGetArrayLeftBound(db, arr5_2) == 31);
  assert(carbonDBGetArrayRightBound(db, arr5_2) == 0);

  // Get a CarbonNetID for the whole memory word, and deposit to it
  CarbonNetID *net_arr5_2 = carbonDBGetCarbonNet(db, arr5_2);
  if (standalone) {
    assert(net_arr5_2 == NULL);
  } else {
    assert(net_arr5_2 != NULL);

    CarbonUInt32 val = 0x12345678;
    carbonDeposit(obj, net_arr5_2, &val, 0);
  }

  CarbonNetID *clk = NULL;
  CarbonNetID *addr1 = NULL;
  CarbonNetID *addr2 = NULL;
  CarbonNetID *dout = NULL;
  CarbonTime t = 0;

  if (!standalone) {
    // Read from the appropriate address to make sure the net was deposited properly
    clk = carbonFindNet(obj, "dbapi4_verilog.clk");
    addr1 = carbonFindNet(obj, "dbapi4_verilog.addr1");
    addr2 = carbonFindNet(obj, "dbapi4_verilog.addr2");
    dout = carbonFindNet(obj, "dbapi4_verilog.dout");

    CarbonUInt32 val = 1;
    carbonDeposit(obj, addr1, &val, 0);
    val = 0xe;  // -2
    carbonDeposit(obj, addr2, &val, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "dout is 0x" << std::hex << val << std::endl;
  }

  // Get a handle to bit 7 of the std_logic_vector, which is a scalar
  index = 7;
  const CarbonDBNode *arr5_2_7 = carbonDBGetArrayElement(db, arr5_2, &index, 1);
  assert(carbonDBIsArray(db, arr5_2_7) == 0);

  // Get a CarbonNetID for the bit, and deposit to it
  CarbonNetID *net_arr5_2_7 = carbonDBGetCarbonNet(db, arr5_2_7);
  if (standalone) {
    assert(net_arr5_2_7 == NULL);
  } else {
    assert(net_arr5_2_7 != NULL);

    CarbonUInt32 val = 1;
    carbonDeposit(obj, net_arr5_2_7, &val, 0);
  }

  if (!standalone) {
    // Read from the appropriate address to make sure the net was deposited properly
    CarbonUInt32 val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, dout, &val, 0);
    std::cout << "dout is 0x" << std::hex << val << std::endl;
  }

  // Alternatively, we could have specified all the dimensions at once
  int indices[3];
  indices[0] = 5;
  indices[1] = -2;
  indices[2] = 7;
  const CarbonDBNode *arr5_2_7_alt = carbonDBGetArrayElement(db, arr, indices, 3);
  // Everything is cached in the CarbonDatabase, so the pointers should be identical
  assert(arr5_2_7_alt == arr5_2_7);

  // We can look up various nodes/nets by string, too
  const CarbonDBNode *arr5_alt = carbonDBFindNode(db, "dbapi4_verilog.arr[5]");
  assert(arr5_alt == arr5);

  if (!standalone) {
    CarbonNetID *net_arr5_alt = carbonFindNet(obj, "dbapi4_verilog.arr[5]");
    assert(net_arr5_alt == NULL);
  }

  const CarbonDBNode *arr5_2_alt = carbonDBFindNode(db, "dbapi4_verilog.arr[5][-2]");
  assert(arr5_2_alt == arr5_2);

  if (!standalone) {
    CarbonNetID *net_arr5_2_alt = carbonFindNet(obj, "dbapi4_verilog.arr[5][-2]");
    assert(net_arr5_2_alt == net_arr5_2);
  }

  const CarbonDBNode *arr5_2_7_alt2 = carbonDBFindNode(db, "dbapi4_verilog.arr[5][-2][7]");
  assert(arr5_2_7_alt2 == arr5_2_7);

  if (!standalone) {
    CarbonNetID *net_arr5_2_7_alt = carbonFindNet(obj, "dbapi4_verilog.arr[5][-2][7]");
    assert(net_arr5_2_7_alt == net_arr5_2_7);
  }

  // Just for kicks, these should all fail
  index = 3;
  const CarbonDBNode *arr5_2_7_bad = carbonDBGetArrayElement(db, arr5_2_7, &index, 1);
  assert(arr5_2_7_bad == NULL);

  indices[0] = -2;
  indices[1] = 7;
  indices[2] = 3;
  const CarbonDBNode *arr5_2_7_bad_alt = carbonDBGetArrayElement(db, arr5, indices, 3);
  assert(arr5_2_7_bad_alt == NULL);

  const CarbonDBNode *arr5_2_7_bad_alt2 = carbonDBFindNode(db, "dbapi4_verilog.arr[5][-2][7][3]");
  assert(arr5_2_7_bad_alt2 == NULL);

  if (!standalone) {
    CarbonNetID *net_arr5_2_7_bad_alt = carbonFindNet(obj, "dbapi4_verilog.arr[5][-2][7][3]");
    assert(net_arr5_2_7_bad_alt == NULL);
  }

  carbonDBFree(db);
  if (!standalone) {
    carbonDestroy(&obj);
  }
  return 0;
}
