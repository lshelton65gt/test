library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity basic4 is
  port (
    clk  : in std_logic;
    addr : in std_logic_vector (3 downto 0);
    we   : in std_logic;
    din  : in std_logic_vector (31 downto 0);
    dout : out std_logic_vector (31 downto 0));
end basic4;

architecture arch of basic4 is

  type myarray is array (0 to 15) of std_logic_vector (31 downto 0);
  signal mem : myarray;
  signal addr_int : integer;

begin

  addr_int <= conv_integer(unsigned(addr));
  
  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        mem(addr_int) <= din;
      else
        dout <= mem(addr_int);
      end if;
    end if;
  end process;
  
end arch;
