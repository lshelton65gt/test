// Testing ordering of composites in waveform dumping

#include "libwaveorder4.h"

int main(int argc, char** argv)
{
  // All we care about is the wave hierarchy, so create a model, dump
  // waves, and exit
  CarbonObjectID* obj = carbon_waveorder4_create(eCarbonFullDB, eCarbon_NoFlags);
  carbonDumpVars(carbonWaveInitFSDB(obj, "waveorder4.fsdb", e1ns), 0, "waveorder4");
  carbonDestroy(&obj);
  return 0;
}
