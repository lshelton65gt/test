#!/bin/tcsh -f

# All tests must go in this list
set all_tests = ( \
    bug12151_01 \
    arrayType1 \
    recarrayType1 \
    dbapi1 \
    dbapi2 \
    dbapi2_verilog \
    dbapi3 \
    dbapi4 \
    dbapi4_verilog \
    dbapi5 \
    dbapi6 \
    dbapi7 \
    dbapi8 \
    dbapi9 \
    dbapi10 \
    dbapi11 \
    dbapi12 \
    basic1 \
    basic2 \
    basic3 \
    basic3_verilog \
    basic4 \
    basic4_verilog \
    basic5 \
    basic5_verilog \
    basic6 \
    basic6_verilog \
    basic7 \
    basic8 \
    basic8_verilog \
    basic9 \
    basic10 \
    basic10_verilog \
    norm1 \
    norm1_verilog \
    norm2 \
    norm2_verilog \
    norm3 \
    norm3_verilog \
    norm4 \
    norm4_verilog \
    norm5 \
    norm5_verilog \
    norm6 \
    norm6_verilog \
    norm7 \
    norm8 \
    norm9 \
    norm10 \
    norm11 \
    array1 \
    array1_verilog \
    array2 \
    array2_verilog \
    array3 \
    array3_verilog \
    array4 \
    array4_verilog \
    array5 \
    array5_verilog \
    array6 \
    array6_verilog \
    array7 \
    array7_verilog \
    array8 \
    array8_verilog \
    array9 \
    array10 \
    array11 \
    array12 \
    recarray1 \
    recarray2 \
    recarray3 \
    recarray4 \
    recarray5 \
    recarray6 \
    recarray7 \
    recarray8 \
    ports1 \
    ports1_verilog \
    ports2 \
    ports2_verilog \
    scalar1 \
    scalar2 \
    random1 \
    random2 \
    random3 \
    replay1 \
    replay2 \
    replay3 \
    replay4 \
    ondemand1 \
    ondemand2 \
    ondemand3 \
    ondemand4 \
    ondemand5 \
    ondemand6 \
    ondemand7 \
    waveorder1 \
    waveorder2 \
    waveorder3 \
    waveorder4 \
    waveorder5 \
    waveorder6 \
    waveorder7 \
    waveorder8 \
    bug8285_1 \
    bug8285_2 \
    bug8285_3 \
    bug8291 \
    bug8358_1 \
    bug8358_2 \
    bug8400 \
    bug8424 \
    bug8452_1 \
    bug8452_2 \
    bug8468 \
    bug8471 \
    bug8485_1 \
    bug8485_2 \
    bug8638_1 \
    bug8638_2 \
    bug8638_3 \
    bug8638_4 \
    bug8638_5 \
    bug8638_6 \
)

# TEXIT tests additionally go into this list
set TEXIT_tests = ( \
)

# TDIFF tests additionally go into this list
set TDIFF_tests = ( \
)

echo "make clean >& clean.log"
echo

foreach test ($all_tests)

  # determine compile options
  set testopts = ""
  set vhdl = ""
  set cwtb = ""
  set static_libcarbon = ""
  set numruns = 1
  set testargs = ""
  if (-e $test.dir) then
    set testopts = "$testopts -directive $test.dir"
  endif
  if (-e $test.cmd) then
    set testopts = "$testopts -f $test.cmd"
  endif
  set testopts = 'TEST_OPTS="'"$testopts"'"'
  if (-e $test.vhd) then
    set vhdl = "VHDL=1"
  endif
  if (-e $test.cwtb) then
    set cwtb = "CWAVE=1 "'CWAVE_OPTS="'"`cat $test.cwtb`"'"'
  endif
  # If the CarbonMem class is used for leak detection, we need to use the static
  # libcarbon because it's not exposed in shared libcarbon.
  grep CarbonMem $test.cxx > /dev/null
  if ($? == 0) then
    set static_libcarbon = "STATIC_LIBCARBON=1"
  endif
  if (-e $test.num) then
    set numruns = `cat $test.num`
  endif
  if (-e $test.args) then
    set testargs = `cat $test.args`
  endif

  echo "make -s TEST=$test $vhdl $cwtb $static_libcarbon $testopts >& $test.build.log"

  # This test might be texit/tdiff
  set texit_tdiff = ""
  echo $TEXIT_tests | grep $test >& /dev/null
  if ($? == 0) then
    set texit_tdiff = "$texit_tdiff TEXIT"
  endif
  echo $TDIFF_tests | grep $test >& /dev/null
  if ($? == 0) then
    set texit_tdiff = "$texit_tdiff TDIFF"
  endif

  echo "$texit_tdiff runtest ./$test.exe $testargs >& $test.run.log"

  if (-e $test.dump.gold) then
    echo "diff -b $test.dump $test.dump.gold >& $test.dump.diff"
  endif

  if (-e $test.dump2.gold) then
    echo "diff -b $test.dump2 $test.dump2.gold >& $test.dump2.diff"
  endif

  if (-e $test.dump3.gold) then
    echo "diff -b $test.dump3 $test.dump3.gold >& $test.dump3.diff"
  endif

  if (-e $test.vcd.gold) then
    echo "wavequery -mode diff -in $test.vcd -in $test.vcd.gold >& $test.vcd.diff"
  endif

  if (-e $test.python.log.gold) then
    echo "carbon python $test.py >& $test.python.log"
  endif

  if (-e $test.fsdbhier.log.gold) then
    echo "printFSDBHier $test.fsdb >& $test.fsdbhier.log"
  endif

  # This test might have multiple runs
  if ("$numruns" != "1") then
    @ stop = $numruns + 1
    set curr = 2
    while ($curr != $stop)
      set testargs = `cat $test.args$curr`
      echo "runtest ./$test.exe $testargs >& $test.run$curr.log"

      if (-e $test.fsdbhier$curr.log.gold) then
        echo "printFSDBHier $test.$curr.fsdb >& $test.fsdbhier$curr.log"
      endif

      @ curr++
    end
  endif

  echo
end
