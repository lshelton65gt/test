package base is

--  subtype myType is integer;
   type myType is record
                    a : bit;
                    b : bit;
   end record;

end base;

use work.base.all;

entity bug8485_1 is
   port (
          pin1  : in  myType;
          pout1 : out myType;
          pin2  : in  myType;
          pout2 : out myType
          );
end bug8485_1;


use work.base.all;

architecture bug8485_1 of bug8485_1 is

component sub
  port (
    i : in myType;
    o : out myType
    );
end component;

begin

  sub1 : sub port map (pin1, pout1);
  sub2 : sub port map (pin2, pout2);

end bug8485_1;

use work.base.all;
entity sub is
  port (
    i : in myType;
    o : out myType
    );
end sub;

use work.base.all;
architecture arch of sub is

begin

  o <= i;
  
end arch;
