// Testing access to visibility nets that wrap memories, when the
// memories are marked with onDemandIdleDeposit, but the memory state
// isn't saved.

#include "libondemand7.h"
#include "carbon/carbon_dbapi.h"
#include <iostream>

static const char *modes[] = {"looking", "idle", "backoff"};

static void mode_change_cb(CarbonObjectID* context,
                           void* userContext,
                           CarbonOnDemandMode from,
                           CarbonOnDemandMode to,
                           CarbonTime simTime)
{
  std::cout << "On-demand entering " << modes[to] << " mode at time " << std::dec << simTime << std::endl;
}

static void run_cycles(CarbonUInt32 count, CarbonObjectID *obj, CarbonNetID *clk, CarbonTime &t)
{
  CarbonUInt32 val;
  for (CarbonUInt32 i = 0; i < (count * 2); ++i) {
    val = t & 0x1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, ++t);
  }
}

static void val_change_cb(CarbonObjectID *obj, CarbonNetID *net, CarbonClientData data, CarbonUInt32 *val, CarbonUInt32*)
{
  CarbonTime t = *(reinterpret_cast<CarbonTime*>(data));
  char name[100];
  carbonGetNetName(obj, net, name, 100);
  std::cout << name << " is " << std::dec << *val << " at time " << t << std::endl;
}

static void print(CarbonObjectID *obj, CarbonNetID *net, const CarbonTime &t)
{
  char name[100];
  CarbonUInt32 val;
  carbonGetNetName(obj, net, name, 100);
  carbonExamine(obj, net, &val, 0);
  
  std::cout << name << " = " << std::hex << val << " at time " << std::dec << t << std::endl;
}

static void timestamp(const CarbonTime &t)
{
  std::cout << "TIME: " << std::dec << t << std::endl;
}

int main()
{
  CarbonObjectID *obj = carbon_ondemand7_create(eCarbonFullDB, eCarbon_OnDemand);
  carbonOnDemandSetMaxStates(obj, 50);
  carbonOnDemandSetBackoffStrategy(obj, eCarbonOnDemandBackoffConstant);
  carbonOnDemandSetBackoffCount(obj, 50);
  carbonOnDemandAddModeChangeCB(obj, mode_change_cb, 0);
  carbonOnDemandEnableStats(obj);
  carbonOnDemandSetDebugLevel(obj, eCarbonOnDemandDebugAll);

  CarbonNetID *clk = carbonFindNet(obj, "ondemand7.clk");
  CarbonNetID *addr = carbonFindNet(obj, "ondemand7.addr");
  CarbonNetID *we = carbonFindNet(obj, "ondemand7.we");
  CarbonNetID *din = carbonFindNet(obj, "ondemand7.din");
  CarbonNetID *dout = carbonFindNet(obj, "ondemand7.dout");

  CarbonDB* db = carbonGetDB(obj);
  const CarbonDBNode* mem_array = carbonDBFindNode(db, "ondemand7.r.mem_array");

  // Get nets for each word
  CarbonNetID* mem_array_words[16];
  for (int i = 0; i < 16; ++i) {
    const CarbonDBNode* node = carbonDBGetArrayElement(db, mem_array, &i, 1);
    mem_array_words[i] = carbonDBGetCarbonNet(db, node);
  }

  CarbonTime t = 0;
  CarbonUInt32 val;

  CarbonNetValueCBDataID *cb = carbonAddNetValueChangeCB(obj, val_change_cb, &t, dout);

//   carbonDumpVars(carbonWaveInitFSDB(obj, "ondemand7.fsdb", e1ns), 0, "ondemand7");

  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  timestamp(t);

  // Readmemh - should break from idle state
  carbonDepositArrayFromFile(obj, mem_array, eCarbonHex, "ondemand7.readmemh");
  print(obj, mem_array_words[7], t);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  timestamp(t);

  // Readmemb - should break from idle state
  carbonDepositArrayFromFile(obj, mem_array, eCarbonBin, "ondemand7.readmemb");
  print(obj, mem_array_words[7], t);
  
  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  timestamp(t);

  // carbonDepositMemory - should break from idle state
  val = 0xa;
  carbonDeposit(obj, mem_array_words[1], &val, 0);

  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  timestamp(t);

  // carbonDepositMemoryWord - should break from idle state
  carbonDepositWord(obj, mem_array_words[2], 0xb, 0, 0);

  // Run long enough to get in an on-demand state
  run_cycles(50, obj, clk, t);
  timestamp(t);

  // carbonDepositMemoryRange - should break from idle state
  val = 0xf;
  carbonDepositRange(obj, mem_array_words[3], &val, 5, 2, 0);

  // Now, start writing to the memory words in a cyclic manner.  An
  // idle state should not be reached, because we can't save the
  // memory's state.
  timestamp(t);
  for (CarbonUInt32 i = 0; i < 10; ++i) {
    for (CarbonUInt32 j = 0; j < 4; ++j) {
      val = j << 4;
      carbonDeposit(obj, mem_array_words[5], &val, 0);
      run_cycles(1, obj, clk, t);
    }
  }

  carbonDestroy(&obj);
  return 0;
}
