library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
        
package base is

   type tMdtN_natural is array(3 downto 2) of natural;

end base;

package body base is
  
end base;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity bug8452_1 is
   port (
          pin1  : in  tMdtN_natural;
          pout1 : out tMdtN_natural
);
end bug8452_1;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archMdtN_natural of bug8452_1 is

begin

  pout1 <= pin1;
  
end archMdtN_natural;
