library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity ondemand7 is
  port (
    clk  : in std_logic;
    addr : in std_logic_vector (3 downto 0);
    we   : in std_logic;
    din  : in std_logic_vector (31 downto 0);
    dummy : in std_logic_vector (31 downto 0);
    dout : out std_logic_vector (31 downto 0));
end ondemand7;

architecture arch of ondemand7 is

  type myarray is array (0 to 15) of std_logic_vector (31 downto 0);
  type myrec is record
                  mem_array   : myarray;
                  dummy : std_logic_vector (31 downto 0);
                end record;
  signal r : myrec;
  signal addr_int : integer;

begin

  addr_int <= conv_integer(unsigned(addr));
  
  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        r.mem_array(addr_int) <= din;
        r.dummy <= dummy;
      else
        dout <= r.mem_array(addr_int) and r.dummy;
      end if;
    end if;
  end process;
  
end arch;
