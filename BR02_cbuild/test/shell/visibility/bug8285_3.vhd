library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package mypack is
  type t_a0 is array (-9 to 10) of std_logic_vector (51 to 62);
  type t_a1 is array (-7 downto -10, 6 downto 1) of std_logic_vector (51 to 62);
  type t_a2 is array (11 downto 10) of t_a1;
  type t_a3 is array (0 to 1, -7 downto -11, -11 to -3) of std_logic_vector (51 to 62);
end mypack;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

use work.mypack.all;

entity bug8285_3 is
  port (
    clk : in std_logic;
    i0 : in std_logic_vector (51 to 62);
    addr0 : in std_logic_vector (4 downto 0);
    addr1 : in std_logic_vector (0 to 0);
    addr2 : in std_logic_vector (1 downto 0);
    addr3 : in std_logic_vector (2 downto 0);
    addr4 : in std_logic_vector (0 to 0);
    addr5 : in std_logic_vector (2 downto 0);
    addr6 : in std_logic_vector (3 downto 0);
    o0 : out std_logic_vector (51 to 62));
end bug8285_3;

architecture arch of bug8285_3 is

  signal s0 : t_a0;
  signal s1 : t_a2;
  signal s2 : t_a3;
  signal addr0_int : integer;
  signal addr1_int : integer;
  signal addr2_int : integer;
  signal addr3_int : integer;
  signal addr4_int : integer;
  signal addr5_int : integer;
  signal addr6_int : integer;

begin

  addr0_int <= conv_integer(unsigned(addr0)) -9;
  addr1_int <= conv_integer(unsigned(addr1)) +10;
  addr2_int <= conv_integer(unsigned(addr2)) -10;
  addr3_int <= conv_integer(unsigned(addr3)) +1;
  addr4_int <= conv_integer(unsigned(addr4));
  addr5_int <= conv_integer(unsigned(addr5)) -11;
  addr6_int <= conv_integer(unsigned(addr6)) -11;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s0(addr0_int) <= i0;
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s1(addr1_int)(addr2_int, addr3_int) <= s0(addr0_int);
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      s2(addr4_int, addr5_int, addr6_int) <= s1(addr1_int)(addr2_int, addr3_int);
    end if;
  end process;

  process (clk)
  begin
    if clk'event and clk = '1' then
      o0 <= s2(addr4_int, addr5_int, addr6_int);
    end if;
  end process;


end arch;
