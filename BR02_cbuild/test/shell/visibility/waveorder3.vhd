library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity waveorder3 is
  port (
    clk  : in  std_logic;
    addr : in  integer range 8 to 11;
    we   : in  std_logic;
    din  : in  std_logic_vector (7 downto 0);
    dout : out std_logic_vector (7 downto 0));
end waveorder3;

architecture arch of waveorder3 is

  type myarr is array (11 downto 8) of std_logic_vector (7 downto 0);
  signal a : myarr;     -- carbon observeSignal
  
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        a(addr) <= din;
      else
        dout <= a(addr);
      end if;
    end if;
  end process;
  
end arch;
