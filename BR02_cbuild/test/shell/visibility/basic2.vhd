library ieee;
use ieee.std_logic_1164.all;

entity basic2 is
  
  port (
    clk : in  std_logic;
    a   : in  std_logic;
    b   : in  std_logic;
    sel : in  std_logic;
    we  : in  std_logic;
    o   : out std_logic);

end basic2;

architecture arch of basic2 is

  type myrec is record
                  a : std_logic;
                  b : std_logic;
                end record;

  type myarray is array (1 downto 0) of myrec;

  signal arr: myarray;
  
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        if sel = '1' then
          arr(1).a <= a;
          arr(1).b <= b;
        else
          arr(0).a <= a;
          arr(0).b <= b;
        end if;
      else
        if sel = '1' then
          o <= arr(1).a or arr(1).b;
        else
          o <= arr(0).a or arr(0).b;
        end if;
      end if;
    end if;
  end process;
  

end arch;
