library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity norm5 is
  port (
    clk  : in std_logic;
    addr : in std_logic_vector (3 downto 0);
    we   : in std_logic;
    din  : in std_logic_vector (31 downto 0);
    dout : out std_logic_vector (31 downto 0));
end norm5;

architecture arch of norm5 is

  type myvector is array (integer range <>) of std_logic;
  type myarray is array (5 downto -10) of myvector (11 downto -20);
  signal mem : myarray;
  signal addr_int : integer;

begin

  addr_int <= conv_integer(unsigned(addr)) - 10;
  
  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        for i in din'range loop
          mem(addr_int)(i - 20) <= din(i);
        end loop;
      else
        for i in dout'range loop
          dout(i) <= mem(addr_int)(i - 20);
        end loop;
      end if;
    end if;
  end process;
  
end arch;
