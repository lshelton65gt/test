// Testing force/release

#include "libreplay4.h"
#include "carbon/carbon_dbapi.h"
#include "carbon/c_memmanager.h"
#define __CarbonTypes_h_
#include "carbon/MemManager.h"
#include <iostream>
#include <cstring>
#include <cassert>

static const char *replay_modes[] = {"normal", "record", "playback", "recover"};

static void replay_mode_change_cb(CarbonObjectID* context,
                                  void* userContext,
                                  CarbonVHMMode current,
                                  CarbonVHMMode changingTo)
{
  CarbonTime t = *static_cast<CarbonTime*>(userContext);
  std::cout << std::dec << t << ": Replay changing from " << replay_modes[current] << " mode to " << replay_modes[changingTo] << " mode" << std::endl;
}

int main(int argc, char **argv)
{
  CarbonMem carbonMem(&argc, argv, NULL);
  // Are we recording or playing back?
  bool record = false;
  bool playback = false;
  bool diverge1 = false;
  bool diverge2 = false;
  bool diverge3 = false;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-record") == 0)
      record = true;
    else if (strcmp(argv[i], "-playback") == 0)
      playback = true;
    else if (strcmp(argv[i], "-diverge1") == 0)
      diverge1 = true;
    else if (strcmp(argv[i], "-diverge2") == 0)
      diverge2 = true;
    else if (strcmp(argv[i], "-diverge3") == 0)
      diverge3 = true;
  }

  assert(!(record && playback));

  CarbonObjectID *obj = carbon_replay4_create(eCarbonFullDB, eCarbon_Replay);
  CarbonNetID *clk = carbonFindNet(obj, "replay4.clk");
  CarbonNetID *a = carbonFindNet(obj, "replay4.a");
  CarbonNetID *b = carbonFindNet(obj, "replay4.b");
  CarbonNetID *addr = carbonFindNet(obj, "replay4.addr");
  CarbonNetID *we = carbonFindNet(obj, "replay4.we");
  CarbonNetID *o = carbonFindNet(obj, "replay4.o");

  CarbonNetID *arr_4_b = carbonFindNet(obj, "replay4.arr[4].b");
  CarbonNetID *arr_5_b = carbonFindNet(obj, "replay4.arr[5].b");
  CarbonNetID *arr_6_b = carbonFindNet(obj, "replay4.arr[6].b");
  CarbonNetID *arr_7_b = carbonFindNet(obj, "replay4.arr[7].b");

  CarbonUInt32 val;
  CarbonTime t = 0;

  // set up replay
  CarbonReplayInfoID *info = carbonGetReplayInfo(obj);
  carbonReplayInfoPutDB(info, "replay4_db", "replay4");
  carbonReplayInfoAddModeChangeCB(info, replay_mode_change_cb, &t);
//   carbonReplayInfoAddCheckpointCB(info, replay_checkpoint_cb, &t);
  if (record) {
    carbonReplayInfoPutDirAction(info, eCarbonDirOverwrite);
    carbonReplayInfoPutSaveFrequency(info, 0, 5);
    carbonReplayRecordStart(obj);
  } 
  if (playback) {
    carbonReplayInfoPutDirAction(info, eCarbonDirNoOverwrite);
    carbonReplaySetVerboseDivergence(obj, 1);
    carbonReplayPlaybackStart(obj);
  }

  // Read all the addresses
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is " << val << std::endl;
  }

  // Force the signals
  val = diverge1 ? 0 : 1;
  if (!diverge2) {
    carbonForce(obj, arr_4_b, &val);
  }
  val = 0;
  carbonForce(obj, arr_5_b, &val);
  val = 1;
  carbonForce(obj, arr_6_b, &val);
  val = 0;
  carbonForce(obj, arr_7_b, &val);

  // Read all the addresses
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is " << val << std::endl;
  }

  // Write 1s to all the addresses
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  carbonDeposit(obj, b, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  // Read all the addresses
  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is " << val << std::endl;
  }

  // Release the signals
  if (!diverge3) {
    carbonRelease(obj, arr_4_b);
  }
  carbonRelease(obj, arr_5_b);
  carbonRelease(obj, arr_6_b);
  carbonRelease(obj, arr_7_b);

  // Write 1s to all the addresses
  val = 1;
  carbonDeposit(obj, we, &val, 0);
  carbonDeposit(obj, b, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  // Read all the addresses
  val = 0;
  carbonDeposit(obj, we, &val, 0);
  for (CarbonUInt32 i = 0; i < 16; ++i) {
    carbonDeposit(obj, addr, &i, 0);
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    carbonExamine(obj, o, &val, 0);
    std::cout << "Data at address " << std::dec << i << " is " << val << std::endl;
  }

  carbonDestroy(&obj);
  return 0;
}
