module ondemand6(clk, addr, we, din, dout);
   input clk;
   input [3:0] addr;
   input       we;
   input [31:0] din;
   output [31:0] dout;

   reg [31:0]    dout;
   reg [31:0]    mem_array[0:15];

   always @(posedge clk)
     if (we)
       mem_array[addr] <= din;
     else
       dout <= mem_array[addr];
endmodule
