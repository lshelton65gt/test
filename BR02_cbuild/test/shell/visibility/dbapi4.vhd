library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity dbapi4 is
  
  port (
    clk   : in  std_logic;
    addr1 : in  std_logic_vector (2 downto 0);
    addr2 : in  std_logic_vector (3 downto 0);
    we    : in  std_logic;
    din   : in  std_logic_vector (31 downto 0);
    dout  : out std_logic_vector (31 downto 0));

end dbapi4;

architecture arch of dbapi4 is

  type myarray is array (4 to 11, 7 downto -8) of std_logic_vector(31 downto 0);

  signal arr: myarray;  

  signal addr1_int : integer;
  signal addr2_int : integer;
  
begin

  addr1_int <= conv_integer(unsigned(addr1)) + 4;
  addr2_int <= conv_integer(signed(addr2));

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        arr(addr1_int, addr2_int) <= din;
      else
        dout <= arr(addr1_int, addr2_int);
      end if;
    end if;
  end process;
  
end arch;
