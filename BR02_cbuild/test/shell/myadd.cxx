
#include "libmyadd.h"
#include <cassert>

int main()
{
  CarbonObjectID* hdl = carbon_myadd_create(eCarbonFullDB, eCarbon_NoFlags);
  assert(hdl);

  CarbonNetID* a = carbonFindNet(hdl, "myadd.a");
  assert(a);
  CarbonNetID* b = carbonFindNet(hdl, "myadd.b");
  assert(b);
  CarbonNetID* clk = carbonFindNet(hdl, "myadd.clk");
  assert(clk);
  CarbonNetID* out = carbonFindNet(hdl, "myadd.out");
  assert(out);
  CarbonNetID* pro = carbonFindNet(hdl, "myadd.u1.u2.u3.pro");
  assert(pro);

  CarbonTime simTime = 0;
  
  carbonSchedule(hdl, simTime++);

  assert(carbonSave(hdl, "myadd.save") == eCarbon_OK);

  CarbonUInt32 val = 1;  
  carbonExamine(hdl, a, &val, 0);
  assert(val == 0);
  
  carbonExamine(hdl, b, &val, 0);
  assert(val == 0);

  carbonExamine(hdl, clk, &val, 0);
  assert(val == 0);

  carbonExamine(hdl, pro, &val, 0);
  assert(val == 0);

  carbonExamine(hdl, out, &val, 0);
  assert(val == 0);

  val = 1;
  carbonDeposit(hdl, a, &val, 0);
  carbonDeposit(hdl, b, &val, 0);
  carbonDeposit(hdl, clk, &val, 0);

    
  carbonSchedule(hdl, simTime++);
  carbonExamine(hdl, a, &val, 0);
  assert(val == 1);
  
  carbonExamine(hdl, b, &val, 0);
  assert(val == 1);

  carbonExamine(hdl, clk, &val, 0);
  assert(val == 1);

  carbonExamine(hdl, pro, &val, 0);
  assert(val == 1);

  carbonExamine(hdl, out, &val, 0);
  assert(val == 2);

  assert(carbonRestore(hdl, "myadd.save") == eCarbon_OK);
  val = 1;  
  carbonExamine(hdl, a, &val, 0);
  assert(val == 0);
  
  carbonExamine(hdl, b, &val, 0);
  assert(val == 0);

  carbonExamine(hdl, clk, &val, 0);
  assert(val == 0);

  carbonExamine(hdl, pro, &val, 0);
  assert(val == 0);

  carbonExamine(hdl, out, &val, 0);
  assert(val == 0);
  carbonDestroy(&hdl);
  return 0;
}
