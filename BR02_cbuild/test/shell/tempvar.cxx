#include "libtempvar.h"
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <assert.h>
using namespace std;


int
main()
{
  int status = 0;
  CarbonObjectID* hdlOrig = carbon_tempvar_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonObjectID* hdlCopy = carbon_tempvar_create(eCarbonFullDB, eCarbon_NoFlags);

  CarbonStatus cstatus;

  CarbonNetID* clkOrig = carbonFindNet(hdlOrig, "tempvar.clock");
  if (clkOrig == NULL) {
    cerr << "Getting handle for tempvar.clock failed\n";
    return 1;
  }

  CarbonNetID* inOrig = carbonFindNet(hdlOrig, "tempvar.in");
  if (clkOrig == NULL) {
    cerr << "Getting handle for tempvar.in failed\n";
    return 1;
  }

  CarbonNetID* netOrig = carbonFindNet(hdlOrig, "tempvar.out");
  if (netOrig == NULL) {
    cerr << "Getting handle for tempvar.out failed\n";
    return 1;
  }

  int cycles;
  UInt32 inValue = 0;

  UInt32 clkv = 0;
  UInt32 time = 0;

  // simulate for a couple of cycles
  for ( cycles = 0; cycles <= 7; ++cycles )
  {
    carbonDeposit(hdlOrig, clkOrig, &clkv, 0);
    carbonSchedule(hdlOrig, time);
    time += 10;

    inValue++;
    carbonDeposit(hdlOrig, inOrig, &inValue, 0);
    clkv =!clkv;
    carbonDeposit(hdlOrig, clkOrig, &clkv, 0);
    carbonSchedule(hdlOrig, time);
    time += 10;
  }

  


  // now save the simulation, then restore
  std::stringstream saveFile;
  saveFile << "tempvar_save.chkpt";
  assert(carbonSave(hdlOrig, saveFile.str().c_str()) == eCarbon_OK);
  assert(carbonRestore(hdlCopy, saveFile.str().c_str()) == eCarbon_OK);

  UInt32 dataOrig[1];
  UInt32 dataCopy[1];



  CarbonNetID* clkCopy = carbonFindNet(hdlCopy, "tempvar.clock");
  if (clkOrig == NULL) {
    cerr << "Getting handle for tempvar.clock failed\n";
    return 1;
  }

  CarbonNetID* inCopy = carbonFindNet(hdlCopy, "tempvar.in");
  if (clkOrig == NULL) {
    cerr << "Getting handle for tempvar.in failed\n";
    return 1;
  }
  CarbonNetID* netCopy = carbonFindNet(hdlCopy, "tempvar.out");
  if (netCopy == NULL) {
    cerr << "Getting handle for tempvar.out failed\n";
    return 1;
  }


  // now check that all regs were restored
  carbonExamine(hdlOrig, netOrig, dataOrig, NULL);
  carbonExamine(hdlCopy, netCopy, dataCopy, NULL);
  if ( dataCopy[0] != dataOrig[0] ){
      status = 1;
      cerr << "Restore failed. orig data:" << dataOrig[0] << " restored data:" << dataCopy[0]  << endl;
  }

  // simulate more, both designs in parallel
  for ( cycles = 8; cycles <= 16; ++cycles )
  {
    carbonDeposit(hdlOrig, clkOrig, &clkv, 0);
    carbonDeposit(hdlCopy, clkCopy, &clkv, 0);
    carbonSchedule(hdlOrig, time);
    carbonSchedule(hdlCopy, time);
    time += 10;


    clkv =!clkv;
    inValue++;
    carbonDeposit(hdlOrig, inOrig, &inValue, 0);
    carbonDeposit(hdlCopy, inCopy, &inValue, 0);
    carbonDeposit(hdlOrig, clkOrig, &clkv, 0);
    carbonDeposit(hdlCopy, clkCopy, &clkv, 0);
    carbonSchedule(hdlOrig, time);
    carbonSchedule(hdlCopy, time);
    time += 10;
  }


  // now check that the restored design simulated in parallel with orig
  cstatus = carbonExamine(hdlOrig, netOrig, dataOrig, NULL);
  if (cstatus != eCarbon_OK) {
      status = 1;
      cerr << "Failed to examine original tempvar.out" <<  "\n";
  }
  cstatus = carbonExamine(hdlCopy, netCopy, dataCopy, NULL);
  if (cstatus != eCarbon_OK) {
      status = 1;
      cerr << "Failed to examine restored tempvar.out" <<  "\n";
  }
  if ( dataCopy[0] != dataOrig[0] ){
      status = 1;
      cerr << "Restore failed. orig data:" << dataOrig[0] << " restored data:" << dataCopy[0]  << endl;
  }


  carbonDestroy(&hdlOrig);
  carbonDestroy(&hdlCopy);
  return status;
} // main
