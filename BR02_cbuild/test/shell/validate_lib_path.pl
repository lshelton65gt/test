#!/bin/sh
# -*-Perl-*-
PATH=$CARBON_HOME/bin:$PATH
    LANG=C
    export LANG
    exec perl -S -x -- "$0" "$@"
#!perl


## this command is used to check for the existence of a required file ($ARGV[0]).
## if the file does not exist then a warning message  about tests related to $ARGV[1] is printed

    unless(-e $ARGV[0]) {
	print "Error: The library $ARGV[0] does not exist\n";
	print "so some of the following tests (related to $ARGV[1]) will fail.\n";
	unless ( $ENV{CARBON_BIN} =~ product ) {
	    print "\n";
	    print "Hint:\n";
	    print "It looks like you are running with CARBON_BIN set to something other than 'product'.\n";
	    print "You can resolve this error by taking the following steps:\n";
	    print " 1. do a mkcds with CARBON_BIN set to product\n";
	    print " 2. then switch CARBON_BIN back to $ENV{CARBON_BIN} and run your test.\n";
	}
  	exit 1;
    }

