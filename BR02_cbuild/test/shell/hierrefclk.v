
module hierrefclk(in1,clk, out, out2);
   input in1, clk;
   output out, out2;
   

   wire real_clk; 
   
   
   sub sub(in1, real_clk, out);
   more more(in1, out2);
endmodule // hierrefclk

module sub(in1, clk, out);
   input in1;
   input clk; // carbon depositSignal
   output out;
   reg    out;
   wire   myclk; //carbon observeSignal
   
   assign myclk = ~clk;
   
   always @(posedge myclk)
     out <= in1;
endmodule // sub

module more(in, out);
   input in;

   output out;
   reg    out;
   
   always@(posedge hierrefclk.sub.clk)
     out <= in;
endmodule // more
