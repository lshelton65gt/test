
#include "libhideme.h"
#include "util/CarbonPlatform.h"
#include <stdio.h>

static const int fail = 3;

/*
 Tests the hidemodule directive. The module hidethis and anything under should not be seen in a vcd file nor accessible from the api.
*/

int main()
{
  CarbonObjectID* hdl;
  CarbonWaveID* wave;

  CarbonNetID* b;
  CarbonNetID* c;
  CarbonNetID* d;

  CarbonNetID* in;
  CarbonNetID* clk;
  CarbonNetID* dead;
  CarbonNetID* topout;

  UInt32 clkVal;
  UInt32 inVal;
  UInt32 topoutVal;
  int i;

  CarbonTime time = 0;
  int stat = 0;

  /* test setfilepath */
#if pfUNIX
  carbonSetFilePath("./:/some/crazy/place/in/alaska");
#elif pfWINDOWS
  carbonSetFilePath("./;/some/crazy/place/in/alaska");
#endif
  hdl = carbon_hideme_create(eCarbonFullDB, eCarbon_NoFlags);
  if (! hdl)
    return fail;

  wave = carbonWaveInitVCD(hdl, "hidemevcd.dump", e10ns);

  carbonPutPrefixHierarchy(wave, "something.mytop", 1);

  if (carbonDumpVars(wave, 2, "top.mama.in top.papa.b") == eCarbon_OK)
  {
    stat = fail;
    fprintf(stderr, "Hidden nets found in dumpvars\n");
  }

  if (carbonDumpVars(wave, 0, "top") != eCarbon_OK)
  {
    stat = fail;
    fprintf(stderr, "Legal dumpvars failed\n");
  }

  in = carbonFindNet(hdl, "top.in");
  if (! in)
  {
    stat = fail;
    fprintf(stderr, "Legal net not found\n");
  }

  b = carbonFindNet(hdl, "top.mama");
  if (b)
  {
    stat = fail;
    fprintf(stderr, "Illegal net of hidden path found\n");

  }

  c = carbonFindNet(hdl, "top.mama.clk");
  if (c)
  {
    stat = fail;
    fprintf(stderr, "Legal net of hidden path found\n");
  }

  d = carbonFindNet(hdl, "top.mama.reg1.in");
  if (d)
  {
    stat = fail;
    fprintf(stderr, "Legal net of hidden path found\n");
  }

  clk = carbonFindNet(hdl, "top.clk");
  if (! clk)
  {
    stat = fail;
    fprintf(stderr, "clk not found\n");
  }

  topout= carbonFindNet(hdl, "top.out");
  if (! topout)
  {
    stat = fail;
    fprintf(stderr, "top.out not found\n");
  }

  dead = carbonFindNet(hdl, "top.mama.reg1.dead");
  if (! dead)
  {
    stat = fail;
    fprintf(stderr, "Deposit signal in hidden module not found\n");
  }

  /* check if observesignal is working here */
  if (carbonFindNet(hdl, "top.mama.reg1.out") == NULL)
  {
    stat = fail;
    fprintf(stderr, "Observe signal in hidden module not found\n");
  }

  if (carbonFindNet(hdl, "top.mama.out") == NULL)
  {
    stat = fail;
    fprintf(stderr, "Expose signal in hidden module not found\n");
  }

  clkVal = 0;
  inVal = 0;
  topoutVal = 1;

  carbonDeposit(hdl, clk, &clkVal, NULL);

  for (i = 0; i < 2; ++i)
  {
    carbonSchedule(hdl, time);
    ++time;
  }

  /* bug1871 */
  carbonDumpOff(wave);
  /* deposit to top.out */
  carbonDeposit(hdl, topout, &topoutVal, NULL);
  for (i = 0; i < 2; ++i)
  {
    carbonSchedule(hdl, time);
    ++time;
  }
  carbonDumpOn(wave);

  carbonDeposit(hdl, in, &inVal, NULL);
  carbonDepositWord(hdl, dead, 0, 0, 0);

  if (stat == 0)
  {
    for (i = 0; i < 20; ++i)
    {
      carbonSchedule(hdl, time);
      ++time;
      clkVal = !clkVal;
      if ((i % 3) == 0)
        inVal = !inVal;
      carbonDeposit(hdl, clk, &clkVal, NULL);
      carbonDeposit(hdl, in, &inVal, NULL);
    }
  }
  carbonDestroy(&hdl);
  return stat;
}
