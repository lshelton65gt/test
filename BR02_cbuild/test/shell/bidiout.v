module bidiout(in, iout, iout2, clk);
   
   input in, clk;
   inout iout;
   inout iout2;
   
   reg   d;

   assign iout = d;
   assign iout2 = in ? 1'b1 : 1'bz;
   
   always @(posedge clk)
     d <= in;
endmodule // bidiout
