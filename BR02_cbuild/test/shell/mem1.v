/* 
 Running in NC yields:

 for mem.dat.3 
                  11 out = 5, in = 4
                  31 out = 5, in = 4
                  51 out = 4, in = 5
                  71 out = 3, in = 6
                  91 out = 2, in = 7
 
 for mem.dat.2
                  11 out = 3, in = 4
                  31 out = 3, in = 4
                  51 out = 2, in = 5
                  71 out = 1, in = 6
                  91 out = 0, in = 7

 for mem.dat.1
                  11 out = x, in = 4
                  31 out = x, in = 4
                  51 out = 4, in = 5
                  71 out = 3, in = 6
                  91 out = 2, in = 7
 */

module top(in, clk, out);
   input [2:0] in;
   input clk;
   
   output [2:0] out;
   reg [2:0]    out;
   
   reg [2:0] mem [7:4];

   initial
     $readmemh("mem.dat", mem);

   always @(posedge clk)
     out <= mem[in];
endmodule // top


`ifdef SIMULATE   
module carbon_test_driver;
   reg clk;
   reg [2:0] in;
   wire [2:0] out;

   top top(in, clk, out);
   
   always #10 clk = !clk;
   
   initial begin
      clk = 0;
      in = 4;

      repeat(2) @(negedge clk);

      in = 5;

      @(negedge clk);

      in = 6;

      @(negedge clk);
      
      in = 7;

      @(negedge clk);
      $finish;

   end // initial begin

   always @(posedge clk)
     #1 $display($time, " out = %h, in = %h", out, in);
   
endmodule // test

`endif      
