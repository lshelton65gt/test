
#ifdef MEM
#include "libmem.h"
#elif MEM2
#include "libmem2.h"
#elif MEM3
#include "libmem3.h"
#endif

#include <assert.h>
#include <stdio.h>

static void sDoClkSched(CarbonObjectID* hdl, CarbonNetID* clk, CarbonNetID* out, UInt32 inVal, UInt32* clkVal, CarbonTime* curTime)
{
  UInt32 outVal;

  carbonDeposit(hdl, clk, clkVal, 0);
  carbonSchedule(hdl, *curTime);
  if (*clkVal == 1)
  {
    carbonExamine(hdl, out, &outVal, NULL);
    fprintf(stdout, "out = %d, in = %d\n", outVal, inVal);
  }
  *clkVal = ! *clkVal;
  *curTime += 10;
}

int main()
{
  int i;
  CarbonObjectID* hdl;
  CarbonNetID* in;
  CarbonNetID* out;
  CarbonNetID* clk;
  UInt32 inVal;
  UInt32 clkVal;
  UInt32 outVal;
  CarbonTime curTime = 0;

#ifdef MEM
  hdl = carbon_mem_create(eCarbonFullDB, eCarbon_NoFlags);
#elif MEM2
  hdl = carbon_mem2_create(eCarbonFullDB, eCarbon_NoFlags);
#elif MEM3
  hdl = carbon_mem3_create(eCarbonFullDB, eCarbon_NoFlags);
#endif

  assert(hdl);

  in = carbonFindNet(hdl, "top.in");
  out = carbonFindNet(hdl, "top.out");
  clk = carbonFindNet(hdl, "top.clk");
  
  assert(in);
  assert(out);
  assert(clk);

  inVal = 4;
  clkVal = 0;
  carbonDeposit(hdl, in, &inVal, 0);
  
  for (i = 0; i < 4; ++i)
  {
    sDoClkSched(hdl, clk, out, inVal, &clkVal, &curTime);
  }
  
  
  inVal = 5;
  carbonDeposit(hdl, in, &inVal, 0);
  for (i = 0; i < 2; ++i)
  {
    sDoClkSched(hdl, clk, out, inVal, &clkVal, &curTime);
  }

  inVal = 6;
  carbonDeposit(hdl, in, &inVal, 0);
  for (i = 0; i < 2; ++i)
  {
    sDoClkSched(hdl, clk, out, inVal, &clkVal, &curTime);
  }

  inVal = 7;
  carbonDeposit(hdl, in, &inVal, 0);
  for (i = 0; i < 2; ++i)
  {
    sDoClkSched(hdl, clk, out, inVal, &clkVal, &curTime);
  }
  
  carbonDestroy(&hdl);
  return 0;
}
