//#include "util/CarbonTypes.h"
#include "shell/CarbonModel.h"
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <assert.h>
using namespace std;

#define carbon_design_create  MODEL(create)

struct DesignInterface
{

  DesignInterface() : 
    m_wen(NULL),
    m_waddr(NULL),
    m_clk(NULL),
    m_raddr(NULL),
    m_out(NULL),
    m_data(NULL),
    mHdl(NULL),
    time(0)
  {}

  void initNets(CarbonObjectID* hdl)
  {
    mHdl = hdl;
    m_wen = carbonFindNet(mHdl, "top.wen");
    m_waddr = carbonFindNet(mHdl, "top.waddr");
    m_clk = carbonFindNet(mHdl, "top.clk");
    m_raddr = carbonFindNet(mHdl, "top.raddr");
    m_out = carbonFindNet(mHdl, "top.out");
    m_data = carbonFindNet(mHdl, "top.data");

    assert(m_wen && m_waddr && m_clk && m_raddr && m_out && m_data);
  }

  CarbonNetID* m_wen;
  CarbonNetID* m_waddr;
  CarbonNetID* m_clk;
  CarbonNetID* m_raddr;
  CarbonNetID* m_out;
  CarbonNetID* m_data;

  CarbonObjectID* mHdl;
  CarbonTime time;
};

template <typename T, typename V> void
printError(const char* str, int index, int wordIndex, T got, V exp)
{
  cerr << "Error: " << str << " top.memX[" << index << "][" << wordIndex;
  cerr << "] = " << got << "; should be " << exp << ".\n";
}


int
readSimulation(const char* str, CarbonObjectID* hdl, DesignInterface& ci,
	       UInt32 offset, int dataSize)
{
  int status = 0;

  UInt32 scalarVal = 0;
  carbonDeposit(hdl, ci.m_wen, &scalarVal, 0);
  scalarVal = MEM_DEPTH;
  carbonDeposit(hdl, ci.m_waddr, &scalarVal, 0);

  for (UInt32 i = 0; i < MEM_DEPTH; ++i)
  {
    // Run a cycle
    scalarVal = 0;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    scalarVal = i + MEM_DEPTH;
    carbonDeposit(hdl, ci.m_raddr, &scalarVal, 0);
    carbonSchedule(hdl, ci.time);
    ci.time += 10;
    
    scalarVal = 1;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    carbonSchedule(hdl, ci.time);
    ci.time += 10;

#if (((MEM_WIDTH + 31)/32) == 2)
    UInt32 exVal = 0;
    carbonExamineWord(hdl, ci.m_out, &exVal, 0, 0);

    if (exVal != (i + offset))
    {
      printError(str, i + MEM_DEPTH, 0, exVal, i + offset);
      status = 1;
    }

    carbonExamineWord(hdl, ci.m_out, &exVal, 1, 0);

    if (exVal != (i + offset + 1))
    {
      printError(str, i + MEM_DEPTH, 0, exVal, i + offset + 1);
      status = 1;
    }
#else
    for (int j = 0; j < dataSize; ++j)
    {
      UInt32 exVal = 0;
      carbonExamineWord(hdl, ci.m_out, &exVal, j, 0);
      if (exVal != (i + j + offset))
      {
	printError(str, i + MEM_DEPTH, j, exVal, i + j + 1);
        status = 1;
      }
    }
#endif
  } // for
  return status;
}

#if 0
int
readSimulation(const char* str, CarbonModel* hdl, carbon_design_interface& ci,
	       UInt32 offset, int )
{
  int status = 0;
  ci.m_wen = 0;
  ci.m_waddr = MEM_DEPTH;
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    // Run a cycle
    ci.m_clk = 0;
    ci.m_raddr = i + MEM_DEPTH;
    carbon_design_schedule(hdl, &ci);
    ci.time += 10;
    ci.m_clk = 1;
    carbon_design_schedule(hdl, &ci);
    ci.time += 10;

    // Compare the results - for some reason the I/O interface is
    // different than the API interface.
#if (((MEM_WIDTH + 31)/32) == 1)
    if (ci.m_out != i + offset)
    {
      printError(str, i + MEM_DEPTH, 0, ci.m_out, i + 1);
      status = 1;
    }
#elif (((MEM_WIDTH + 31)/32) == 2)
    UInt64 expect = (((UInt64)(i + offset + 1) << 32) | (i + offset));
    if (ci.m_out != expect)
    {
      status = 1;
      printError(str, i + MEM_DEPTH, 0, ci.m_out, expect);
    }
#else
    for (int j = 0; j < dataSize; ++j)
    {
      if (ci.m_out[j] != (i + j + offset))
      {
	printError(str, i + MEM_DEPTH, j, ci.m_out[j], i + j + 1);
	status = 1;
      }
    }
#endif
  } // for
  return status;
}
#endif

void
writeSimulation(CarbonObjectID* hdl, DesignInterface& ci, UInt32 offset,
		int dataSize)
{
  UInt32 scalarVal = 1;
  carbonDeposit(hdl, ci.m_wen, &scalarVal, 0);
  scalarVal = MEM_DEPTH;
  carbonDeposit(hdl, ci.m_raddr, &scalarVal, 0);

  for (UInt32 i = 0; i < MEM_DEPTH; ++i)
  {
    // Run a cycle
    scalarVal = 0;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    scalarVal = i + MEM_DEPTH;
    carbonDeposit(hdl, ci.m_waddr, &scalarVal, 0);

#if (((MEM_WIDTH + 31)/32) == 2)
    scalarVal = i + offset;
    carbonDepositWord(hdl, ci.m_data, scalarVal, 0, 0);
    scalarVal = i + offset + 1;
    carbonDepositWord(hdl, ci.m_data, scalarVal, 1, 0);
#else
    for (UInt32 j = 0; j < dataSize; ++j)
      carbonDepositWord(hdl, ci.m_data, i + j + offset, j, 0);
#endif
    carbonSchedule(hdl, ci.time);
    ci.time += 10;
    scalarVal = 1;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    carbonSchedule(hdl, ci.time);
    ci.time += 10;
  }
  scalarVal = 0;
  carbonDeposit(hdl, ci.m_wen, &scalarVal, 0);
} // writeSimulation

#if 0
void
writeSimulation(CarbonModel* hdl, carbon_design_interface& ci, UInt32 offset,
		int )
{
  ci.m_wen = 1;
  ci.m_raddr = MEM_DEPTH;
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    // Run a cycle
    ci.m_clk = 0;
    ci.m_waddr = i + MEM_DEPTH;

    // For some reason the I/O interface is different than the API interface.
#if (((MEM_WIDTH + 31)/32) == 1)
    ci.m_data = i + offset;
#elif (((MEM_WIDTH + 31)/32) == 2)
    ci.m_data = (((UInt64)(i + offset + 1) << 32) | (i + offset));
#else
    for (int j = 0; j < dataSize; ++j)
      ci.m_data[j] = i + j + offset;
#endif
    carbon_design_schedule(hdl, &ci);
    ci.time += 10;
    ci.m_clk = 1;
    carbon_design_schedule(hdl, &ci);
    ci.time += 10;
  }
  ci.m_wen = 0;
} // writeSimulation
#endif

int
main()
{
  int status = 0;
  DesignInterface ci;
  ci.time = 10;

  CarbonObjectID* hdl = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);

  ci.initNets(hdl);

  CarbonStatus cstatus;

  // Create a vcd file
  ostringstream stream;
  stream << "stateupdatemem" << MEM_DEPTH << "_" << MEM_WIDTH << "vcd.dump";
  CarbonWaveID* wave = carbonWaveInitVCD(hdl, stream.str().c_str(), e1ns);
  if (! wave)
  {
    cerr << "dumpInit failed" << endl;
    return 1;
  }
  
  if (carbonDumpVars(wave, 0, "top") != eCarbon_OK)
  {
    cerr << "dumpvars failed" << endl;
    return 1;
  }

  // Get a handle to the memories to do all our work
  CarbonMemoryID* mem1 = carbonFindMemory(hdl, "top.mem1");
  CarbonMemoryID* mem2 = carbonFindMemory(hdl, "top.mem2");
  if ((mem1 == NULL) || (mem2 == NULL))
  {
    cerr << "Getting handle for top.mem failed\n";
    return 1;
  }

  // Allocate memory to do API I/O
  int dataSize =carbonMemoryRowNumUInt32s(mem1);
  assert(dataSize == (MEM_WIDTH + 31)/32);
  assert(dataSize == carbonMemoryRowNumUInt32s(mem2));
  UInt32* data = new UInt32[dataSize];

  // Write through the API
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    for (int j = 0; j < dataSize; ++j)
      data[j] = i + j + 1;
    
    cstatus = carbonDepositMemory(mem1, i + MEM_DEPTH, data);
    if (cstatus != eCarbon_OK)
    {
      status = 1;
      cerr << "Failed to write top.mem1[" << i << "]\n";
    }
    cstatus = carbonDepositMemory(mem2, i + MEM_DEPTH, data);
    if (cstatus != eCarbon_OK)
    {
      status = 1;
      cerr << "Failed to write top.mem2[" << i << "]\n";
    }
  }

  // Make sure data is 0 so that vcd dumping is consistent
  for (int j = 0; j < dataSize; ++j)
    carbonDepositWord(hdl, ci.m_data, 0, j, 0);

  // Read through simulation
  status |= readSimulation("API/Sim mismatch", hdl, ci, 1, dataSize);

  // Write through simulation
  writeSimulation(hdl, ci, 3, dataSize);

  // Read through the API
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    cstatus = carbonExamineMemory(mem1, i + MEM_DEPTH, data);
    if (cstatus != eCarbon_OK)
    {
      status = 1;
      cerr << "Failed to read from top.mem1[" << i + MEM_DEPTH << "]\n";
    }
    else
    {
      for (int j = 0; j < dataSize; ++j)
	if (data[j] != (unsigned)(i + j + 3))
	{
	  printError("Sim/API mismatch", i + MEM_DEPTH, j, data[j], (i + j + 3));
	  status = 1;
	}
    }
    cstatus = carbonExamineMemory(mem2, i + MEM_DEPTH, data);
    if (cstatus != eCarbon_OK)
    {
      status = 1;
      cerr << "Failed to read from top.mem2[" << i + MEM_DEPTH << "]\n";
    }
    else
    {
      for (int j = 0; j < dataSize; ++j)
	if (data[j] != (unsigned)(i + j + 3))
	{
	  printError("Sim/API mismatch", i + MEM_DEPTH, j, data[j], (i + j + 3));
	  status = 1;
	}
    }
  }

  // Write through the API using the word routines
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    for (int j = 0; j < dataSize; ++j)
    {
      cstatus = carbonDepositMemoryWord(mem1, i + MEM_DEPTH, i + j + 33, j);
      if (cstatus != eCarbon_OK)
      {
	status = 1;
	cerr << "Failed to write top.mem1[" << i + MEM_DEPTH << "][";
	cerr << j << "]\n";
      }
      cstatus = carbonDepositMemoryWord(mem2, i + MEM_DEPTH, i + j + 33, j);
      if (cstatus != eCarbon_OK)
      {
	status = 1;
	cerr << "Failed to write top.mem2[" << i + MEM_DEPTH << "][";
	cerr << j << "]\n";
      }
    }
  }

  // Read through simulation again
  status |= readSimulation("Word API/Sim mismatch", hdl, ci, 33, dataSize);

  // Write through the simulation again
  writeSimulation(hdl, ci, 0x123400, dataSize);

  // Read from the word API
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    for (int j = 0; j < dataSize; ++j)
    {
      data[0] = carbonExamineMemoryWord(mem1, i + MEM_DEPTH, j);
      if (cstatus != eCarbon_OK)
      {
	status = 1;
	cerr << "Failed to read from top.mem1[" << i + MEM_DEPTH << "][";
	cerr << j << "]\n";
      }
      else
      {
	if (data[0] != (unsigned)(i + j + 0x123400))
	{
	  printError("Sim/API mismatch", i + MEM_DEPTH, j, data[0],
		     (i + j + 0x123400));
	  status = 1;
	}
      }
      data[0] = carbonExamineMemoryWord(mem2, i + MEM_DEPTH, j);
      if (data[0] != (unsigned) (i + j + 0x123400))
      {
        printError("Sim/API mismatch", i + MEM_DEPTH, j, data[0],
                   (i + j + 0x123400));
        status = 1;
      }
    }
  }
  
  // Shutdown and return the results
  carbonDumpFlush(wave);
  carbonDestroy(&hdl);
  return status;
} // main
