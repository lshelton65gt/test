module myadd(a, b, out, clk);
   input a, b, clk;
   output [1:0] out;

   adder u1(a, b, out, clk);
endmodule

module adder(a, b, out, clk);
   input a, b, clk;
   output [1:0] out;

   theadd1 u2(a, b, out, clk);
endmodule

module theadd1(a, b, out, clk);
   input a, b, clk;
   output [1:0] out;

   theadd u3(a, b, out, clk);
endmodule
   
module theadd(a, b, out, clk);
   input a, b, clk;
   output [1:0] out;

   reg    [1:0] out;
   wire   pro; // carbon observeSignal

   assign pro = a | b;

   always @ (posedge clk)
     out <= a + b;
endmodule
