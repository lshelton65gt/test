
#include "libreal.h"
#include <stdio.h>

static const int fail = 3;

int main()
{
  CarbonObjectID* hdl;
  CarbonWaveID* wave;

  CarbonNetID* clk;
  CarbonNetID* realout;
  CarbonNetID* RC_chk3;

  CarbonTime time = 0;
  int stat = 0;
  int i;

  UInt32 clkVal;
  CarbonReal realVal;
  CarbonReal RC_chk3Val;

  hdl = carbon_real_create(eCarbonFullDB, eCarbon_NoFlags);
  if (! hdl)
    return fail;

  wave = carbonWaveInitVCD(hdl, "real.vcd.dump", e10ns);
  
  if (carbonDumpVars(wave, 0, "mt46v64m4") != eCarbon_OK)
  {
    stat = fail;
    fprintf(stderr, "Dumping of all nets failed\n");
  }

  clk = carbonFindNet(hdl, "mt46v64m4.clk");
  if (! clk)
  {
    stat = fail;
    fprintf(stderr, "Net 'clk' not found\n");
  }

  realout = carbonFindNet(hdl, "mt46v64m4.realout");
  if (! realout)
  {
    stat = fail;
    fprintf(stderr, "Real net 'realout' not found\n");
  }

  RC_chk3 = carbonFindNet(hdl, "mt46v64m4.RC_chk3");
  if (! RC_chk3)
  {
    stat = fail;
    fprintf(stderr, "Real net 'RC_chk3' not found\n");
  }

  clkVal = 0;
  realVal = 0.0;
  RC_chk3Val = 0.0;

  for (i = 0; i < 20; ++i)
  {
    carbonDeposit(hdl, clk, &clkVal, NULL);
    carbonSchedule(hdl, time);
    ++time;
    if ( clkVal == 1 )
      clkVal = 0;
    else
      clkVal = 1;
  }

  carbonDestroy(&hdl);
  return stat;
}
