#include "libmultiwave.h"

int main()
{
  CarbonObjectID* obj = carbon_multiwave_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID* clk = carbonFindNet(obj, "top.clk");
  CarbonNetID* in1a = carbonFindNet(obj, "top.in1a");
  CarbonNetID* in1b = carbonFindNet(obj, "top.in1b");
  CarbonNetID* in2a = carbonFindNet(obj, "top.in2a");
  CarbonNetID* in2b = carbonFindNet(obj, "top.in2b");
  CarbonTime t = 0;
  CarbonUInt32 val;

  CarbonLogicByteType myReg[2] = {eCARBON_BT_VCD_0, eCARBON_BT_VCD_1};
  int myInt = 10;

  // Create first waveform
  CarbonWaveID* wave = carbonWaveInitFSDB(obj, "multiwave1.fsdb", e1ns);
  // Add user data register
  CarbonWaveUserDataID* userData = carbonAddUserData(wave,
                                                     eCARBON_VT_VCD_REG,
                                                     eCarbonVarDirectionImplicit,
                                                     eCARBON_DT_VERILOG_STANDARD,
                                                     1, 0,
                                                     myReg,
                                                     "myReg",
                                                     eCARBON_BYTES_PER_BIT_1B,
                                                     "top.myData",
                                                     ".");
  carbonWaveUserDataChange(userData);
  // Dump just sub1
  carbonDumpVars(wave, 0, "top.sub1");

  // Run some cycles
  val = 0x10;
  carbonDeposit(obj, in1a, &val, 0);
  val = 0x20;
  carbonDeposit(obj, in1b, &val, 0);
  val = 0x30;
  carbonDeposit(obj, in2a, &val, 0);
  val = 0x40;
  carbonDeposit(obj, in2b, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  val = 0x11;
  carbonDeposit(obj, in1a, &val, 0);
  val = 0x21;
  carbonDeposit(obj, in1b, &val, 0);
  val = 0x31;
  carbonDeposit(obj, in2a, &val, 0);
  val = 0x41;
  carbonDeposit(obj, in2b, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Change user data
  myReg[0] = eCARBON_BT_VCD_1;
  myReg[1] = eCARBON_BT_VCD_0;
  carbonWaveUserDataChange(userData);

  // Run some cycles
  val = 0x12;
  carbonDeposit(obj, in1a, &val, 0);
  val = 0x22;
  carbonDeposit(obj, in1b, &val, 0);
  val = 0x32;
  carbonDeposit(obj, in2a, &val, 0);
  val = 0x42;
  carbonDeposit(obj, in2b, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  val = 0x13;
  carbonDeposit(obj, in1a, &val, 0);
  val = 0x23;
  carbonDeposit(obj, in1b, &val, 0);
  val = 0x33;
  carbonDeposit(obj, in2a, &val, 0);
  val = 0x43;
  carbonDeposit(obj, in2b, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Close first waveform and open another
  carbonWaveClose(obj);
  wave = carbonWaveInitFSDB(obj, "multiwave2.fsdb", e1ps);      // change timescale
  // Add user data integer
  userData = carbonAddUserData(wave,
                               eCARBON_VT_VCD_INTEGER,
                               eCarbonVarDirectionImplicit,
                               eCARBON_DT_VERILOG_INTEGER,
                               0, 0,
                               &myInt,
                               "myInt",
                               eCARBON_BYTES_PER_BIT_4B,
                               "top.myData",
                               ".");
  carbonWaveUserDataChange(userData);
  // Dump just sub2
  carbonDumpVars(wave, 0, "top.sub2");

  // Roll back time
  t = 0;

  // Run some cycles
  val = 0x14;
  carbonDeposit(obj, in1a, &val, 0);
  val = 0x24;
  carbonDeposit(obj, in1b, &val, 0);
  val = 0x34;
  carbonDeposit(obj, in2a, &val, 0);
  val = 0x44;
  carbonDeposit(obj, in2b, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  val = 0x15;
  carbonDeposit(obj, in1a, &val, 0);
  val = 0x25;
  carbonDeposit(obj, in1b, &val, 0);
  val = 0x35;
  carbonDeposit(obj, in2a, &val, 0);
  val = 0x45;
  carbonDeposit(obj, in2b, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Change user data
  myInt = 11;
  carbonWaveUserDataChange(userData);

  // Run some cycles
  val = 0x16;
  carbonDeposit(obj, in1a, &val, 0);
  val = 0x26;
  carbonDeposit(obj, in1b, &val, 0);
  val = 0x36;
  carbonDeposit(obj, in2a, &val, 0);
  val = 0x46;
  carbonDeposit(obj, in2b, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  val = 0x17;
  carbonDeposit(obj, in1a, &val, 0);
  val = 0x27;
  carbonDeposit(obj, in1b, &val, 0);
  val = 0x37;
  carbonDeposit(obj, in2a, &val, 0);
  val = 0x47;
  carbonDeposit(obj, in2b, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);

  // Clean up
  carbonDestroy(&obj);
  return 0;
}
