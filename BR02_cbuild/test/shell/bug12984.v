module top(clk, rst, in, out);
   input clk, rst, in;
   output out;

   reg   rst_int;
   reg   out;

   always @(posedge clk or posedge rst)
     if (rst)
       rst_int <= 1'b1;
     else
       rst_int <= 1'b0;

   always @(posedge clk or posedge rst_int)
     if (rst_int)
       out <= 1'b0;
     else
       out <= in;
   
endmodule
