#include "libmem32_128.h"
#define MODEL(N) carbon_mem32_128_ ## N
#define MEM_DEPTH	32
#define MEM_WIDTH	128
#define MEM_ADDR_BITS	5
#include "mem.cxx"
