#include "libbug6122.h"
#include <stdio.h>

#define PAT_NUM 5000

int main()
{
  CarbonNetID* clk;
  CarbonMemoryID* mem;
  CarbonTime time = 0;
  int i ;
  CarbonUInt32 ONE  = 1 ;
  CarbonUInt32 ZERO = 0 ;
  int lsb, msb; 
  int bit63=63, bit32=32; 
  CarbonUInt32 val;
  CarbonUInt32 fullval[2];
  CarbonSInt64 adr;
  
  /* Instantiate a top model */
  CarbonObjectID* top =
    carbon_bug6122_create(eCarbonFullDB, eCarbon_NoFlags); 
  if ( NULL == top ) {
    printf("Error: CarbonObjectID NULL\n");
    return 1;
  }

  clk = carbonFindNet(top, "top.clk");
  mem = carbonFindMemory(top, "top.mem.memory");

  if ( NULL == mem ) {
    printf("Error: CarbonMemoryID NULL\n");
    return 1;
  }

  for(i=0; i<PAT_NUM*2; i +=2){
    carbonDeposit(top, clk, &ZERO, NULL);
    if(i==0){
      carbonDeposit(top, clk, &ZERO, NULL);
    } else if (i==10) {
      carbonDeposit(top, clk, &ONE, NULL);
    } else if(i==500) {
      if( carbonIsMemDepositable(mem) == 1 ) {
         printf("Info MEM OK\n");
      }else if( carbonIsMemDepositable(mem) == 0 ) {
         printf("Error: NOT Depositable\n");
      }else if( carbonIsMemDepositable(mem) == -1 ) {
         printf("Error: Invalid\n");
      }
      carbonReadmemh(mem,"bug6122.dat");
    } else if(i==1000) {
      adr = 0;
      carbonExamineMemory(mem, adr, fullval);
      val = fullval[0];
      printf("1 adr=%x, val=%x\n",  (CarbonSInt32)adr,val);
      fullval[1] = 0;
      fullval[0] = 0x12345678;
      carbonDepositMemory(mem, adr, fullval);
      carbonExamineMemory(mem, adr, fullval);
      printf("2 adr=%x, val=%x\n",  (CarbonSInt32) adr,fullval[0]);
      carbonExamineMemoryRange(mem, adr, &val, bit63, bit32 );
      printf("3 adr=%x, val=%x\n",  (CarbonSInt32)adr,val);
      fullval[1] = 0;
      fullval[0] = 0x00000000;
      carbonDepositMemory(mem, adr, fullval);
      val = 0x5a5a5a5a;
      carbonDepositMemoryRange(mem, adr, &val, bit63, bit32 );
      carbonExamineMemory(mem, adr, fullval );
      printf("4 adr=%x, val=%x\n",  (CarbonSInt32)adr,fullval[1]);
      carbonExamineMemoryRange(mem, adr, &val, bit63, bit32 );
      printf("5 adr=%x, val=%x\n",  (CarbonSInt32)adr,val);
      adr = 1;
      msb = 15, lsb = 14 ; 
      val = 3;
      carbonDepositMemoryRange(mem, adr, &val, msb,lsb );
      msb = 63, lsb = 63 ; 
      val = 1;
      carbonDepositMemoryRange(mem, adr, &val, msb,lsb );
      carbonExamineMemory(mem, adr, fullval);
      printf("6 adr=%x, val=%x\n",  (CarbonSInt32)adr,fullval[1]);
      carbonExamineMemoryRange(mem, adr, &val, bit63, bit32 );
      printf("7 adr=%x, val=%x\n",  (CarbonSInt32)adr,val);
      msb = 15, lsb = 14 ; 
      carbonExamineMemoryRange(mem, adr, &val, msb,lsb );
      printf("8 adr=%x, val=%x\n",  (CarbonSInt32)adr,val);
      msb = 63, lsb = 63 ; 
      carbonExamineMemoryRange(mem, adr, &val, msb,lsb );
      printf("9 adr=%x, val=%x\n",  (CarbonSInt32)adr,val);
      
    }
    carbonSchedule(top, i+1);
    carbonDeposit(top, clk, &ONE, 0);
    carbonSchedule(top, i+2);

  }
  carbonDestroy(&top);
  return 0 ;
}
