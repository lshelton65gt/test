module top(clk, out);
  input clk;
  output [31:0] out;
  reg [31:0]    out;
  integer       seed;
  reg           foo;

  initial begin
    seed = 53;
    out = $random(seed);          // provide seed
  end

  always @(posedge clk)
    out <= $random(seed) + foo;
endmodule
