#include "libmem32_32.h"
#define MODEL(N) carbon_mem32_32_ ## N
#define MEM_DEPTH	32
#define MEM_WIDTH	32
#define MEM_ADDR_BITS	5
#include "mem.cxx"
