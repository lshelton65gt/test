#include "libbug6655.h"

eCarbonMsgCBStatus msgHandler(CarbonClientData clientData,
			      CarbonMsgSeverity severity,
			      int number,
			      const char* text,
			      unsigned int len) {

   int noPrint = 0;
   char sevtext[8];
   char source[8];
   switch (severity) {
   case eCarbonMsgSuppress:	noPrint = 1; break;
   case eCarbonMsgStatus:	strcpy(sevtext, "Status"); break;
   case eCarbonMsgNote:		strcpy(sevtext, "Note"); break;
   case eCarbonMsgWarning:	strcpy(sevtext, "Warning"); break;
   case eCarbonMsgAlert:	strcpy(sevtext, "Alert"); break;
   case eCarbonMsgError:	strcpy(sevtext, "Error"); break;
   case eCarbonMsgFatal:	strcpy(sevtext, "Fatal"); break;
   default: 	                strcpy(sevtext, "Message"); break;  // invalid severity
   }

   if (clientData == 0) {
      strcpy(source, "Unknown");
   } else {
      strncpy(source, clientData, 7);
   }

   if (noPrint == 0) {
      printf("Carbon %s %s %d: %s\n", source, sevtext, number, text);
   }

   return eCarbonMsgStop;
}

int main(int argc, void** argv) {
   /* Install global message handler */
   CarbonMsgCBDataID *msgCbId_glbl = carbonAddMsgCB(0, msgHandler, "Global");
   /* Create VHM */
   CarbonObjectID *vhm = carbon_bug6655_create(eCarbonFullDB, eCarbon_NoFlags);
   /* Install instance specific message handler */
   CarbonMsgCBDataID *msgCbId_vhm = carbonAddMsgCB(vhm, msgHandler, "VHM");
   /* Cleanup */
   carbonDestroy(&vhm);
   return 0;
}
