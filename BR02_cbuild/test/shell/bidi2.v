
module bidi2(ioe_bid, ioe_en);
   inout ioe_bid;
   input ioe_en;
   
   assign      ioe_bid = ioe_en ? 1'b0 : 1'bz;

endmodule // bidi

`ifdef VERILOG_TEST
module test;
   reg ioe_bid_in;
   reg ioe_en;
   wire ioe_bid;

   assign ioe_bid = ioe_en ? 1'bz : ioe_bid_in;
   
   bidi2 top(ioe_bid, ioe_en);

   initial 
     begin
        $dumpvars(0, top);
        ioe_bid_in = 0;
        
        ioe_en = 0;
        
        #1
          ioe_en <= 1;
        #1
          $finish;
     end // initial begin
   
   
     
endmodule // test
`endif //  `ifdef VERILOG_TEST
