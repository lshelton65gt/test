#
#  Makefile for   test/shell
#

include ../Makefile.common
#include $(CARBON_HOME)/makefiles/Makefile.carbon.vsp

all: message flopreg mem32_32 mem32_64 mem32_128 gen_mem32_32 stateupdatemem32_32 hideme bidiout real rand1 hierrefclk tempvar bug4954 mem1 mem2 mem3 tienet2

tristate1 gate_triout testbidi constant stateio bidiclosure:
	cd api; $(MAKE) $@

clean::
	$(RM) -r *.$(OBJEXT) *.exe *.h *dump *.tmp *.diff *.$(STATEXT) libdesign* librand* checkpointcb


## extra cbuild flags
CBEXTRAFLAGS =

CXXFLAGS += 
#CBFLAGS = -Wc -g -noEncrypt
CBFLAGS = -g -q $(CBEXTRAFLAGS) -noEncrypt

%:	%.exe
	runtest ./$@.exe

bvsign.exe: bvsign.v $(CBUILD_EXE)
	vspcompiler $(CBFLAGS) -2001 -noEncrypt -q +define+BUG6983 $< -o libbvsign.$(STATEXT)
	$(CARBON_CXX) -DBUG6983 -g -O0 -c bvsign.cxx
	$(CARBON_LINK) -o $@ bvsign.$(OBJEXT) libbvsign.$(STATEXT) $(CARBON_LIB_LIST)

bug7925.exe: bug7925.v $(CBUILD_EXE)
	vspcompiler $(CBFLAGS) -q -noEncrypt -directive bug7925.dir $<
	$(CARBON_CXX) -c bug7925.cxx
	$(CARBON_LINK) -o $@ bug7925.$(OBJEXT) libdesign.$(STATEXT) $(CARBON_LIB_LIST)

bug7427.exe: bug7427.vhd $(CBUILD_EXE)
	vspcompiler $(CBFLAGS) -q $< -vhdlTop bug
	$(CARBON_CXX) -c bug7427.cxx
	$(CARBON_LINK) -o $@ bug7427.$(OBJEXT) libdesign.$(STATEXT) $(CARBON_LIB_LIST)

bug6843.exe: bug6843.v $(CBUILD_EXE)
	vspcompiler $(CBFLAGS) -directive bug6843.dir -q $< -o libbug6843.$(STATEXT)
	$(CARBON_CXX) -c bug6843.cxx
	$(CARBON_LINK) -o $@ bug6843.$(OBJEXT) libbug6843.$(STATEXT) $(CARBON_LIB_LIST)

bug6595.exe: bug6595.v $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -q $< -o libbug6595.$(STATEXT)
	$(CARBON_CC) -c bug6595.c
	$(CARBON_LINK) -o $@ bug6595.$(OBJEXT) libbug6595.$(STATEXT) $(CARBON_LIB_LIST)

bug6587.exe: bug6587.vhd
	$(CBUILD) $(CBFLAGS) -q -vhdlLib WORK -vhdlTop ram1rw bug6587.vhd -o libbug6587.$(STATEXT) 
	$(CARBON_CXX) -c bug6587.cxx
	$(CARBON_LINK) -o $@ bug6587.$(OBJEXT) libbug6587.$(STATEXT) $(CARBON_LIB_LIST)

# do not use CBARGS for bug7722_01... -g turns off net vectorisation and this bug
# was related to a port constructed during net vectorisation
bug7722_01.exe: bug7722_01.v
	$(CBUILD) -q -netVecMinCluster 2 -verboseSplitPorts -testdriver -testdriverDumpVCD -o libbug7722_01.$(STATEXT) $<

# This uses CARBON_COMPILE so I can compile the OSSleep call.
# That is tested here because I had it segv'ing at one point with 
# this test.
rand1.exe: rand1.v $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -o librand1.$(STATEXT) $< 
	$(CBUILD) $(CBFLAGS) -o librand2.$(STATEXT) $< 
	$(CARBON_COMPILE) -c rand1.cxx
	$(CARBON_LINK) -o $@ rand1.$(OBJEXT) librand1.$(STATEXT) librand2.$(STATEXT) $(CARBON_LIB_LIST)

tienet2.exe: tienet2.v $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -q -directive tienet2.directive -constantPropagation -verboseConstantPropagation 2 -g -O0 -noFlatten $< -o libtienet2.$(STATEXT)
	$(CARBON_CC) -g -c tienet2.c
	$(CARBON_LINK) -o $@ tienet2.$(OBJEXT) libtienet2.$(STATEXT) $(CARBON_LIB_LIST)

signedTest.exe: signedTest.v signedTest.c $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -q  -2001 $< -o libsignedTest.$(STATEXT)
	$(CARBON_CC) -c signedTest.c
	$(CARBON_LINK) -o $@ signedTest.$(OBJEXT) libsignedTest.$(STATEXT) $(CARBON_LIB_LIST)

signedTriTest.exe: signedTriTest.v signedTriTest.c $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -q  -2001 $< -o libsignedTriTest.$(STATEXT)
	$(CARBON_CC) -c signedTriTest.c
	$(CARBON_LINK) -o $@ signedTriTest.$(OBJEXT) libsignedTriTest.$(STATEXT) $(CARBON_LIB_LIST)

message.exe: message.v $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -enableOutputSysTasks $< -o libmessage.$(STATEXT)
	$(COMPILE) -c message.c
	$(CARBON_LINK) -o $@ message.$(OBJEXT) libmessage.$(STATEXT) $(CARBON_LIB_LIST)

flopreg.exe: flopreg.v flopreg.cxx $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) $< -o libflopreg.$(STATEXT)
	$(CARBON_CXX) -c -g -O0 flopreg.cxx
	$(CARBON_LINK) -o $@ flopreg.$(OBJEXT) -L. -lflopreg $(CARBON_STATIC_LIB_LIST)

# This rule was used to build the compatibility-testing files.  It's not used when running the test.
flopreg_58xx.object.$(CARBON_TARGET_ARCH) : flopreg.v flopreg.cxx
	$(CBUILD) $(CBFLAGS) $< -o libflopreg_58xx.$(STATEXT)
	mv libflopreg_58xx.h libflopreg.h
	$(CARBON_CXX) -c -DCARBON_CREATE_FUNC=carbon_flopreg_58xx_create -g -o flopreg_58xx.$(OBJEXT) -O0 flopreg.cxx
	mv flopreg_58xx.$(OBJEXT) $@
	mv libflopreg_58xx.symtab.db libflopreg_58xx.symtab.$(CARBON_TARGET_ARCH)_db
	mv libflopreg_58xx.$(STATEXT) libflopreg_58xx.staticlib.$(CARBON_TARGET_ARCH)

run_flopreg_saved_%.exe : flopreg_%.$(CARBON_TARGET_ARCH)_exe libflopreg_%.symtab.$(CARBON_TARGET_ARCH)_db
	@cp $< flopreg_saved_$*.exe
	@cp libflopreg_$*.symtab.$(CARBON_TARGET_ARCH)_db libflopreg_$*.symtab.db
	@env LD_LIBRARY_PATH=${CARBON_HOME}/${CARBON_TARGET_ARCH}/gcc/lib:${CARBON_HOME}/${CARBON_TARGET_ARCH}/lib/gcc/shared:${LD_LIBRARY_PATH} runtest flopreg_saved_$*.exe

libflopreg_%.$(STATEXT) : libflopreg_%.staticlib.$(CARBON_TARGET_ARCH)
	cp $< $@

flopreg_%.$(OBJEXT) : flopreg_%.object.$(CARBON_TARGET_ARCH)
	cp $< $@

flopreg_%.exe: flopreg_%.$(OBJEXT) libflopreg_%.$(STATEXT)
	$(CARBON_LINK) -o $@ $< -L. -lflopreg_$* $(CARBON_LIB_LIST)

bug4954.exe: bug4954.v bug4954.c $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) $< -o libbug4954.$(STATEXT) 
	$(CARBON_CC) -c bug4954.c
	$(CARBON_LINK) -o $@ bug4954.$(OBJEXT) -L. -lbug4954 $(CARBON_LIB_LIST)

bigflop.exe: bigflop.v bigflop.c $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS)  $<  -o libbigflop.$(STATEXT)
	$(CARBON_CC) -c bigflop.c
	$(CARBON_LINK) -o $@ bigflop.$(OBJEXT) libbigflop.$(STATEXT) $(CARBON_LIB_LIST)

hierrefclk.exe: hierrefclk.v $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) $<  -o libhierrefclk.$(STATEXT) ; \
	$(CARBON_CC) -c hierrefclk.c ; \
	$(CARBON_LINK) -o $@ hierrefclk.$(OBJEXT) libhierrefclk.$(STATEXT) $(CARBON_LIB_LIST)

hideme.exe: hideme.v hideme.c $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -noEmbedDB -directive hideme.dir $<  -o libhideme.$(STATEXT) ; \
	$(CARBON_CC) -c hideme.c ; \
	$(CARBON_LINK) -o $@ hideme.$(OBJEXT) libhideme.$(STATEXT) $(CARBON_LIB_LIST)

shadow_check.exe: flopreg.v $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) $< -o libshadow_check.$(STATEXT) ; \
	$(CARBON_CC) -c shadow_check.c ; \
	$(CARBON_LINK) -o $@ shadow_check.$(OBJEXT) libshadow_check.$(STATEXT) $(CARBON_LIB_LIST)


real.exe: real.v $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -directive real.dir -enableOutputSysTasks $< -o libreal.$(STATEXT) ; \
	$(CARBON_CC) -c -g real.c ; \
	$(CARBON_LINK) -o $@ real.$(OBJEXT) libreal.$(STATEXT) $(CARBON_LIB_LIST)


bidiout.exe: bidiout.v $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -o libbidiout.$(STATEXT) $< ;\
	$(CARBON_CC) -c bidiout.c ; \
	$(CARBON_LINK) -o $@ bidiout.$(OBJEXT) libbidiout.$(STATEXT) $(CARBON_LIB_LIST)


constantSplit.exe: constantSplit.v $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -noEmbedDB -directive constantSplit.dir $< -o libconstantSplit.$(STATEXT) ; \
	$(CARBON_CC) -c constantSplit.c ; \
	$(CARBON_LINK) -o $@ constantSplit.$(OBJEXT) libconstantSplit.$(STATEXT) $(CARBON_LIB_LIST)

tempvar.exe: tempvar.v $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) -checkpoint -o libtempvar.$(STATEXT) -dumpVerilog -noEncrypt $< ; \
	$(CARBON_CXX) -c tempvar.cxx ; \
	$(CARBON_LINK) -o $@ tempvar.$(OBJEXT) libtempvar.$(STATEXT) $(CARBON_LIB_LIST)

mem1.exe : mem1.v readmem.c $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) $< -q -o libmem.$(STATEXT) -noEncrypt
	$(CARBON_CC) -DMEM -c readmem.c
	$(CARBON_LINK) -o $@ readmem.$(OBJEXT) libmem.$(STATEXT) $(CARBON_LIB_LIST)

mem2.exe : mem2.v readmem.c $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) $< -q -o libmem2.$(STATEXT) -noEncrypt
	$(CARBON_CC) -DMEM2 -c readmem.c
	$(CARBON_LINK) -o $@ readmem.$(OBJEXT) libmem2.$(STATEXT) $(CARBON_LIB_LIST)

mem3.exe : mem3.v readmem.c $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) $< -q -o libmem3.$(STATEXT) -noEncrypt -Wc -g -O0
	$(CARBON_CC) -DMEM3 -c readmem.c
	$(CARBON_LINK) -o $@ readmem.$(OBJEXT) libmem3.$(STATEXT) $(CARBON_LIB_LIST)

bug6122.exe : bug6122.v bug6122.c bug6122.dir $(CBUILD_EXE)
	$(CBUILD) $(CBFLAGS) $< -v2k -q -o libbug6122.$(STATEXT) -directive bug6122.dir -noEncrypt -Wc -g -O0
	$(CARBON_CC)  -c bug6122.c
	$(CARBON_LINK) -o $@ bug6122.$(OBJEXT) libbug6122.$(STATEXT) $(CARBON_STATIC_LIB_LIST)

## the following default rule should only be used for examples that want to use the struct api

%.exe: %.v $(CBUILD_EXE)
	$(CBUILD) -2001 -q -checkpoint $(CBFLAGS) -noEncrypt $< -o lib$(*F).$(STATEXT)
	$(CARBON_CXX) -c $(*F).cxx
	$(CARBON_LINK) -o $@ $(*F).$(OBJEXT) lib$(*F).$(STATEXT) $(CARBON_LIB_LIST)

# Pass LIB_LIST_NAME in.  Default to CARBON_LIB_LIST
LIB_LIST_NAME ?= CARBON_LIB_LIST
# Guard against typos in test.run.  Note we're testing
# $(LIB_LIST_NAME), not LIB_LIST_NAME.
ifndef $(LIB_LIST_NAME)
  $(error LIB_LIST_NAME not defined: $(LIB_LIST_NAME))
endif

twocounter.exe: .carbon.libtwocounter/main.$(OBJEXT) libtwocounter.$(STATEXT)
	$(CARBON_CC_LINK) -o $@ $^ $($(LIB_LIST_NAME))

libtwocounter.$(STATEXT) .carbon.libtwocounter/main.o .carbon.libtwocounter/main.cxx: twocounter.v
	cbuild -q $^ -testdriverNoLink -o libtwocounter.$(STATEXT)

# Compile main.cxx with Visual C++ if needed
.carbon.libtwocounter/main.obj: .carbon.libtwocounter/main.cxx
	# Using $(@F) rather than $@ because the visual_cxx script cd's into the source directory.
	$(CARBON_CXX) -c $< -o $(@F)

twocounter.$(OBJEXT): twocounter.c libtwocounter.$(STATEXT)
	$(CARBON_CC) -c twocounter.c -I$(CARBON_HOME)/include

checkpointcb: checkpointcb.cxx flopreg.v
	$(CBUILD) -q flopreg.v -o libflopreg.$(STATEXT)
	$(CARBON_LINK) $@.cxx -L. -lflopreg $(CARBON_LIB_LIST) -o $@

libmyadd.$(STATEXT) libmyadd.h: myadd.v
	$(CBUILD) -noEncrypt $^ -o libmyadd.$(STATEXT) -noFlatten -g

myadd.$(OBJEXT) : libmyadd.h myadd.cxx
	$(CARBON_CXX) -c myadd.cxx

myadd.exe: myadd.$(OBJEXT) libmyadd.$(STATEXT) 
	$(CARBON_LINK) -o $@ $^ $(CARBON_STATIC_LIB_LIST)

%.fsdb.txt: %.fsdb
	${FSDBREPORT} $^ -s "*" -o $@

bogus/libcarbon${CARBON_LIBRARY_VERSION}.so: bogus.c
	$(CARBON_LINK) -shared -fPIC -o $@ $^
