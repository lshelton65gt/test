#include "libflopreg.h"
#include <cstring>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include "shell/carbon_misc.h"

// CREATE_FUNC is sometimes overridden with -D option
#ifndef CARBON_CREATE_FUNC
#define CARBON_CREATE_FUNC carbon_flopreg_create
#endif

using namespace std;

static const int fail = 3;
static const int xfail = 4;

static CarbonNetID* sSomeNet = NULL;
static int sSomeNetChgCnt = 0;

static void netCB(CarbonObjectID* model, CarbonNetID* net, CarbonClientData data, UInt32* val, UInt32* drv)
{
  int* i = static_cast<int*>(data);
  ++(*i);

  assert(val != NULL);
  assert(drv != NULL);

  if ((net == sSomeNet) && (*i == 1))
  {
    CarbonNetID* ref = carbonFindNet(model, "flopreg.b");
    carbonAddNetValueChangeCB(model, &netCB, &sSomeNetChgCnt, ref);
  }
}

static void printRow(const UInt32* row, SInt64 addr, ostream& out, UInt32 numWords)
{
  out << "@0000" << setfill('0') << setw(4) << hex  << addr << " ";
  for (SInt32 i = numWords - 1; i >= 0; --i)
    out << setfill('0') << setw(8) << hex  << row[i];
  out << endl;
}

int main(int argc, char* argv[])
{
  int stat = 0;
  int numClkChanges = 0;
  int numClkCBs = 0;
  
#if pfUNIX
  carbonSetFilePath("/foo:/tmp:./");
#elif pfWINDOWS
  carbonSetFilePath("/foo;/tmp;./");
#endif

  SInt64 addr;
  const UInt32* row;


  /* test for bug4554, also test memfile in general */
  CarbonMemFileID* memFile = carbonReadMemFile(NULL, "nonexistent", eCarbonHex, 128, 0);
  if (memFile)
  {
    cerr << "Memfile did not fail as it should have.\n";
    return fail;
  }
  
  memFile = carbonReadMemFile(NULL, "flopreg_mem_test.dat", eCarbonHex, 128, 0);
  if (! memFile)
  {
    cerr << "Memfile read 1 failed.\n";
    return fail;
  }

  SInt64 firstAddr, lastAddr;
  if (carbonMemFileGetFirstAndLastAddrs(memFile, &firstAddr, &lastAddr) != eCarbon_OK)
  {
    cerr << "First and Last Addr 1 retrieve failed.\n";
    return fail;
  }

  if ((firstAddr != 0x2200) || (lastAddr != 0x25f9))
  {
    cerr << "Memfile read: Incorrect first and last addresses\n";
    return fail;
  }

  {
    ofstream out1("flopreg_mem_forward.dat");
    if (! out1)
    {
      cerr << "Failed to open flopreg_mem_forward.dat\n";
      return fail;
    }
    
    for (addr = firstAddr; addr <= lastAddr; ++addr)
    {
      row = carbonMemFileGetRow(memFile, addr);
      if (row)
      {
        printRow(row, addr, out1, 4);
      }
    }
  }
  
  carbonFreeMemFileID(&memFile);

  /* Now read the reverse file */
  memFile = carbonReadMemFile(NULL, "flopreg_mem_test_reverse.dat", eCarbonHex, 128, 1);
  if (! memFile)
  {
    fprintf(stderr, "Memfile read 2 failed.\n");
    return fail;
  }

  if (carbonMemFileGetFirstAndLastAddrs(memFile, &firstAddr, &lastAddr) != eCarbon_OK)
  {
    fprintf(stderr, "First and Last Addr 2 retrieve failed.\n");
    return fail;
  }
  
  if ((firstAddr != 0x25f9) || (lastAddr != 0x2200))
  {
    fprintf(stderr, "Memfile read: Incorrect first and last addresses\n");
    return fail;
  }

  {
    ofstream out2("flopreg_mem_reverse.dat");
    if (! out2)
    {
      cerr << "Failed to open flopreg_mem_reverse.dat\n";
      return fail;
    }
    
    for (addr = firstAddr; addr >= lastAddr; --addr)
    {
      row = carbonMemFileGetRow(memFile, addr);
      if (row)
      {
        printRow(row, addr, out2, 4);
      }
    }
  }
  
  carbonFreeMemFileID(&memFile);

  //  carbon_design_interface ci;
  CarbonObjectID* hdl_prev = CARBON_CREATE_FUNC(eCarbonFullDB, eCarbon_NoFlags);
  if (! hdl_prev)
    return fail;
  
  CarbonWaveID* waveEmpty = carbonWaveInitVCD(hdl_prev, "flopregvcd.empty.dump", e1ns);
  if (! waveEmpty)
  {
    cerr << "empty wave dumpinit failed" << endl;
    stat = fail;
    carbonDestroy(&hdl_prev);
    return stat;
  }
  
  carbonDestroy(&hdl_prev);


  int i;
  CarbonObjectID* hdl = CARBON_CREATE_FUNC(eCarbonFullDB, eCarbon_NoFlags);
  if (! hdl)
    return fail;
  
  CarbonWaveID* wave = carbonWaveInitVCD(hdl, "flopregvcd.dump", e1ns);
  if (! wave)
  {
    cerr << "wave dumpinit failed" << endl;
    stat = fail;
    carbonDestroy(&hdl);
    return stat;
  }
  
  if (carbonPutPrefixHierarchy(wave, "faketop.fakescope", 0) != eCarbon_OK)
  {
    cerr << "wave putPrefixHierarchy failed" << endl;
    stat = fail;
  }
  
  // check for bad dumpvars usage
  if (carbonDumpVars(wave, 0, "") == eCarbon_OK) {
    cerr << "Bad dumpvars check failed." << endl;
    stat = fail;
  }

  // check for bad hdl path in dumpvars
  if (carbonDumpVars(wave, 0, "jujubean") == eCarbon_OK) {
    cerr << "Bad hdl path in dumpvars check failed." << endl;
    stat = fail;
  }

  if (carbonDumpVars(wave, 0, "flopreg") != eCarbon_OK) {
    cerr << "dumpvars failed" << endl;
    stat = fail;
  }

  // try to open another waveform. Should error. Not currently
  // supported
  {
    CarbonWaveID* wave2 = carbonWaveInitVCD(hdl, "flopreg.try2.dump", e1ns);
    if (wave2 != NULL)
    {
      cerr << "Multiple waveform open did not fail as expected." << endl;
      stat = fail;
    }
  }
  
  carbonWaveHierarchyClose(wave);

  CarbonTime time = 10;
  //  cerr << "clk\td\tq" << endl;
  
  CarbonNetID* aref = carbonFindNet(hdl, "flopreg.a");
  if (! aref)
  {
    stat = fail;
    cerr << "getref a failed." << endl;
  }
  
  CarbonNetID* bref = carbonFindNet(hdl, "flopreg.b");
  if (! bref)
  {
    stat = fail;
    cerr << "getref b failed." << endl;
  }


  // At this point, let's check if we are suppose to record or play
  bool doRecord = false;
  bool doPlay = false;
  char* sysDir = NULL;
  char* instDir = NULL;
  for (i = 1; i < argc; ++i)
  {
    if (strcmp(argv[i], "-record") == 0) 
      doRecord = true;
    if (strcmp(argv[i], "-play") == 0) 
      doPlay = true;

    else if (strcmp(argv[i], "-sys") == 0)
    {
      assert(i + 1 < argc);
      sysDir = argv[i + 1];
    }
    else if (strcmp(argv[i], "-inst") == 0)
    {
      assert(i + 1 < argc);
      instDir = argv[i + 1];
    }
  }

  if (doPlay || doRecord)
  {
    assert(sysDir);
    assert(instDir);

    // try to do record start or play start without setting up the
    // replayinfo
    assert(carbonReplayRecordStart(hdl) == eCarbon_ERROR);
    assert(carbonReplayPlaybackStart(hdl) == eCarbon_ERROR);

    // set up the replayinfo
    CarbonReplayInfoID* replayInfo = carbonGetReplayInfo(hdl);
    if (replayInfo == NULL)
    {
      stat = fail;
      cerr << "carbonGetReplayInfo failed." << endl;
    }

    if (carbonReplayInfoPutDB(replayInfo, sysDir, instDir) != eCarbon_OK)
    {
      stat = fail;
      cerr << "carbonReplayInfoPutDB failed." << endl;
    }

    carbonReplayInfoPutDirAction(replayInfo, eCarbonDirOverwrite);
    carbonReplayInfoPutSaveFrequency(replayInfo, 0, 0);
  }

  if (doRecord && carbonReplayRecordStart(hdl) != eCarbon_OK)
  {
    stat = fail;
    cerr << "carbonReplayRecordStart failed." << endl;
  }

  if (doPlay && carbonReplayPlaybackStart(hdl) != eCarbon_OK)
  {
    stat = fail;
    cerr << "carbonReplayPlaybackStart failed." << endl;
  }
  
  CarbonNetID* cref = carbonFindNet(hdl, "flopreg.c");
  if (! cref)
  {
    stat = fail;
    cerr << "getref c failed." << endl;
  }

  CarbonNetID* dref = carbonFindNet(hdl, "flopreg.d ");
  if (! dref)
  {
    stat = fail;
    cerr << "getref d failed." << endl;
  }

  {
    // make sure findMemory fails and reports
    CarbonMemoryID* mem = carbonFindMemory(hdl, "flopreg.e");
    if (mem) {
      stat = fail;
      cerr << "Memory returned for non-memory" << endl;
    }
  }
  
  CarbonNetID* eref = carbonFindNet(hdl, "flopreg.e");
  if (! eref)
  {
    stat = fail;
    cerr << "getref e failed." << endl;
  }

  CarbonShadowStructID* fShadow = carbonFindNetAllocShadow(hdl, "flopreg.f");
  if (! fShadow)
  {
    stat = fail;
    cerr << "getref fShadow failed." << endl;
  }

  // Get the net, so I don't have to change the entire tb
  CarbonNetID* fref = carbonShadowStructGetNet(fShadow);
  assert(fref);

  CarbonNetID* clkref = carbonFindNet(hdl, "flopreg.clk");
  if (! clkref)
  {
    stat = fail;
    cerr << "getref clk failed." << endl;
  }

  // This is absolutely crazy, but it is a good way to test the cb
  // mechanism. Callback anytime the clock changes.
  CarbonNetValueCBDataID* cb = carbonAddNetValueChangeCB(hdl,
                                                         &netCB,
                                                         &numClkCBs, 
                                                         clkref);


  if (carbonGetNumUInt32s(dref) != 2)
  {
    stat = fail;
    cerr << "GetNumUint32s returned incorrect size" << endl;
  }

  UInt32 arefDep[1];
  UInt32* brefDep = new UInt32[1];  
  UInt32 crefDep[1];
  UInt32* drefDep = new UInt32[2];
  UInt32 erefDep[1];
  UInt32 frefDep[3];
  UInt32 clkRefDep[1];

  UInt32 arefEx[1];
  UInt32 brefEx[1];
  UInt32 crefEx[1];
  UInt32 drefEx[2];
  UInt32 frefEx[3];
  
  arefDep[0] = 0;
  brefDep[0] = 1;
  crefDep[0] = 0xf0f0f0f0;
  drefDep[0] = 0;
  drefDep[1] = 0;
  erefDep[0] = 0xf;
  frefDep[0] = 0xfffffff0;
  frefDep[1] = 0x00000000;
  frefDep[2] = 0x000000ff;  
  clkRefDep[0] = 0;

  carbonDeposit(hdl, aref, arefDep, 0);
  carbonDeposit(hdl, bref, brefDep, 0);
  // Returns error, because cref is a non-tristate and the drive value
  // is not 0, but the value is still deposited.
  //assert(hdl->deposit(cref, crefDep, crefDep) == eCarbon_ERROR);
  carbonDeposit(hdl, cref, crefDep, crefDep);
  
  carbonDeposit(hdl, dref, drefDep, 0);
  carbonDeposit(hdl, eref, erefDep, 0);
  carbonDeposit(hdl, fref, frefDep, 0);
  carbonDeposit(hdl, clkref, clkRefDep, 0);
  
  for (i = 0; i < 3; ++i)
  {
    clkRefDep[0] = ! clkRefDep[0];
    carbonDeposit(hdl, clkref, clkRefDep, 0);
    ++numClkChanges;
    //    ci.m_clk = !ci.m_clk;
    //   carbon_design_schedule(hdl, &ci);
    carbonSchedule(hdl, time);
    time += 10;
  }
  
  carbonDumpAll(wave);

  // Check carbonGetExternalDrive on all the inputs. They should
  // return all 0s.
  UInt32 clkRefEx;
  carbonGetExternalDrive(hdl, aref, arefEx);
  carbonGetExternalDrive(hdl, bref, brefEx);
  carbonGetExternalDrive(hdl, cref, crefEx);
  carbonGetExternalDrive(hdl, dref, drefEx);
  carbonGetExternalDrive(hdl, fref, frefEx);
  carbonGetExternalDrive(hdl, clkref, &clkRefEx);

  if ((arefEx[0] != 0) ||
      (brefEx[0] != 0) ||
      (crefEx[0] != 0) ||
      (drefEx[0] != 0) ||
      (drefEx[1] != 0) ||
      (frefEx[0] != 0) ||
      (frefEx[1] != 0) ||
      (frefEx[2] != 0) ||
      (clkRefEx != 0))
  {
    cerr << "carbonGetExternalDrive incorrect for inputs" << endl;
    stat = fail;
  }
  
  CarbonNetID* bout = carbonFindNet(hdl, "flopreg.bout");
  CarbonNetID* coutVar = carbonFindNet(hdl, "flopreg.cout");
  CarbonNetID* dout = carbonFindNet(hdl, "flopreg.q");
  CarbonNetID* fout = carbonFindNet(hdl, "flopreg.fout");
  
  CarbonNetID* aout = carbonFindNet(hdl, "flopreg.aout");

  // check carbonExternalDrive on all the outputs. They should return
  // all 1s.
  carbonGetExternalDrive(hdl, aout, arefEx);
  carbonGetExternalDrive(hdl, bout, brefEx);
  carbonGetExternalDrive(hdl, coutVar, crefEx);
  carbonGetExternalDrive(hdl, dout, drefEx);
  carbonGetExternalDrive(hdl, fout, frefEx);

  if ((arefEx[0] != 0x3ff) ||
      (brefEx[0] != 1) ||
      (crefEx[0] != 0xffffffff) ||
      (drefEx[0] != 0xffffffff) ||
      (drefEx[1] != 0x3) ||
      (frefEx[0] != 0xffffffff) ||
      (frefEx[1] != 0xffffffff) ||
      (frefEx[2] != 0xff))
  {
    cerr << "carbonGetExternalDrive incorrect for outputs" << endl;
    stat = fail;
  }
  
  UInt32 wordCheck;
  if (carbonExamineWord(hdl, dref, &wordCheck, 1, 0) != eCarbon_OK)
  {
    stat = fail;
    cerr << "carbonExamineWord failed" << endl;
  }
  if (wordCheck != 0)
  {
    stat = fail;
    cerr << "word incorrect from carbonExamineWord" << endl;
  }
  
  UInt32 drefVal[2];
  carbonExamine(hdl, dref, drefVal, 0);
  if (! ((drefVal[0] == 0) && (drefVal[1] == 0)))
  {
    stat = fail;
    cerr << "word incorrect from carbonExamine" << endl;
  }
  
  if (carbonExamineRange(hdl, dref, drefVal, 35, 16, 0) != eCarbon_ERROR)
  {
    stat = fail;
  }
  if (carbonExamineRange(hdl, dref, drefVal, 16, -1, 0) != eCarbon_ERROR)
  {
    stat = fail;
  }
  if (carbonExamineRange(hdl, dref, drefVal, 35, -1, 0) != eCarbon_ERROR)
  {
    stat = fail;
  }
 
  if (carbonDepositRange(hdl, dref, drefVal, 35, 16, NULL) != eCarbon_ERROR)
  {
    stat = fail;
  }
  if (carbonDepositRange(hdl, dref, drefVal, 16, -1, 0) != eCarbon_ERROR)
  {
    stat = fail;
  }
  if (carbonDepositRange(hdl, dref, drefVal, 35, -1, 0) != eCarbon_ERROR)
  {
    stat = fail;
  }

  char drefStr[10];
  if (carbonFormat(hdl, dref, drefStr, 10, eCarbonHex) != eCarbon_OK)
  {
    stat = fail;
    cerr << "Unable to format string" << endl;
  }
  else
  {
    const char* chkval = "000000000";
    if (strncmp(drefStr, chkval, 9) != 0)
    {
      stat = fail;
      cerr << "Formatted hex string is incorrect" << endl;
    }
  }
  
  char drefStr2[50];
  if (carbonFormat(hdl, dref, drefStr2, 50, eCarbonOct) != eCarbon_OK)
  {
    stat = fail;
    cerr << "Unable to format octal string" << endl;
  }
  else
  {
    const char* chkval = "00000000000";
    if (strncmp(drefStr2, chkval, 11) != 0)
    {
      stat = fail;
      cerr << "Formatted octal string is incorrect" << endl;
    }
  }


  if (carbonFormat(hdl, dref, drefStr2, 50, eCarbonDec) != eCarbon_OK)
  {
    stat = fail;
    cerr << "Unable to format decimal string" << endl;
  }
  else
  {
    const char* chkval = "0";
    if (strcmp(drefStr2, chkval) != 0)
    {
      stat = fail;
      cerr << "Formatted decimal string is incorrect" << endl;
    }
  }

  if (carbonGetNumUInt32s(bref) != 1)
  {
    stat = fail;
    cerr << "carbonGetNumUint32s returned incorrect size for scalar" << endl;
  }

  if (carbonExamineWord(hdl, bref, &wordCheck, 0, 0) != eCarbon_OK)
  {
    stat = fail;
    cerr << "Valid handle not processed in carbonExamineWord" << endl;
  }
  else if (wordCheck != 1)
  {
    stat = fail;
    cerr << "word incorrect from carbonExamineWord" << endl;
  }
  
  UInt32 brefVal[1];
  carbonExamine(hdl, bref, brefVal, 0);
  if (brefVal[0] != 1)
  {
    stat = fail;
    cerr << "word incorrect from carbonExamine for scalar" << endl;
  }
  
  char brefStr[2];
  if (carbonFormat(hdl, bref, brefStr, 2, eCarbonHex) != eCarbon_OK)
  {
    stat = fail;
    cerr << "Unable to format string" << endl;
  }
  else
  {
    const char* chkval = "1";
    if (strcmp(brefStr, chkval) != 0)
    {
      stat = fail;
      cerr << "Formatted hex string is incorrect for scalar" << endl;
    }
  }
  
  if (carbonFormat(hdl, bref, brefStr, 2, eCarbonBin) != eCarbon_OK)
  {
    stat = fail;
    cerr << "Unable to format string" << endl;
  }
  else
  {
    const char* chkval = "1";
    if (strcmp(brefStr, chkval) != 0)
    {
      stat = fail;
      cerr << "Formatted binary string is incorrect for scalar" << endl;
    }
  }

  // regular
  UInt32 crefVal[1];
  crefVal[0] = 0x0;
  if (carbonExamineRange(hdl, cref, crefVal, 7, 4, 0) != eCarbon_OK)
  {
    stat = fail;
  }
  cout << crefVal[0] << endl;

  crefVal[0] = 0x0;
  if (carbonDepositRange(hdl, cref, crefVal, 7, 4, 0) != eCarbon_OK)
  {
    stat = fail;
  }

  if (! doPlay)
  {
    // If playing, we don't see all error messages, especially
    // out-of-range errors.
    UInt32 tmp = 1;
    if (carbonDepositRange(hdl, cref, crefVal, 7, 4, &tmp) != eCarbon_ERROR)
    {
      cerr << "DepositRange supposed to return error when setting non-tristate xdrive to nonzero" << endl;
      stat = fail;
    }
  }

  crefVal[0] = 0xf;
  if (carbonExamineRange(hdl, cref, crefVal, 7, 4, 0) != eCarbon_OK)
  {
    stat = fail;
  }
  cout << crefVal[0] << endl;

  crefVal[0] = 0xf;
  if (carbonDepositRange(hdl, cref, crefVal, 7, 4, 0) != eCarbon_OK)
  {
    stat = fail;
  }
  crefVal[0] = 0x0;
  if (carbonExamineRange(hdl, cref, crefVal, 7, 4, 0) != eCarbon_OK)
  {
    stat = fail;
  }
  cout << crefVal[0] << endl;
  

  
  if (stat != 0)
    // bail out at this point.
    return stat;
  
  
  if (! carbonShadowStructCompareUpdate(hdl, fShadow)) {
    stat = fail;
    cerr << "carbonShadowStructCompareUpdate returned false unexpectedly" << endl;
  }
  // make sure it updated
  if (carbonShadowStructCompareUpdate(hdl, fShadow)) {
    stat = fail;
    cerr << "compareUpdate did not update." << endl;
  }

  carbonShadowStructFreeShadow(hdl, &fShadow);

  char frefStr[19];
  if (carbonGetNumUInt32s(fref) != 3)
  {
    stat = fail;
    cerr << "Num uints is wrong for 72 bits" << endl;
  }

  if (carbonFormat(hdl, fref, frefStr, 19, eCarbonHex) != eCarbon_OK)
  {
    stat = fail;
    cerr << "Unable to format string" << endl;
  }
  else
  {
    const char* chkval = "ff00000000fffffff0";
    if (strcmp(frefStr, chkval) != 0)
    {
      stat = fail;
      cerr << "Formatted hex string is incorrect for 72 bit" << endl;
      cerr << "     got " << frefStr << endl;
      cerr << "expected " << chkval << endl;
    }
  }

  arefDep[0] = 0;
  brefDep[0] = 1;
  crefDep[0] = 0xf0f0f0f0;
  erefDep[0] = 0xf;
  frefDep[0] = 0xfffffff0;
  frefDep[1] = 0x00000000;
  frefDep[2] = 0x000000ff;  
  
  carbonDeposit(hdl, aref, arefDep, 0);
  carbonDeposit(hdl, bref, brefDep, 0);
  carbonDeposit(hdl, cref, crefDep, 0);
  carbonDeposit(hdl, eref, erefDep, 0);
  carbonDeposit(hdl, fref, frefDep, 0);
 
  carbonDisableNetCB(hdl, cb);

  sSomeNet = aout;
  if (! aout)
  {
    stat = fail;
    cerr << "getref aout failed." << endl;
  }

  int aoutNum = 0;
  // Check that the maps get regenerated in the callback scheduler.
  // will crash in schedule call if the callback mechanism is not
  // reset properly
  CarbonNetValueCBDataID* aoutCB = carbonAddNetValueChangeCB(hdl,
                                                             &netCB,
                                                             &aoutNum, 
                                                             aout);
  
  if (numClkChanges != numClkCBs)
  {
    cerr << "Callback calls wrong, expected " << numClkChanges << ", got "
         << numClkCBs << endl;
    stat = fail;
  }

  for (i = 0; i < 8; ++i)
  {
    if ((i & 0x1) == 0)
    {
      ++(drefDep[0]);
      carbonDeposit(hdl, dref, drefDep, 0);
      brefDep[0] = ! brefDep[0];
      carbonDeposit(hdl, bref, brefDep, 0);
    }
    clkRefDep[0] = ! clkRefDep[0];
    carbonDeposit(hdl, clkref, clkRefDep, 0);
    if (i == 5)
    {
      carbonEnableNetCB(hdl, cb);

      carbonDumpOff(wave);

      if (carbonFormat(hdl, dref, drefStr, 10, eCarbonHex) != eCarbon_OK)
      {
        stat = fail;
        cerr << "Unable to format string 2" << endl;
      }
      else
      {
        const char* chkval = "000000003";
        if (strncmp(drefStr, chkval, 9) != 0)
        {
          stat = fail;
          cerr << "Formatted hex string is incorrect" << endl;
        }
      }

      brefDep[0] = 1;
      carbonDeposit(hdl, bref, brefDep, 0);

      drefDep[1] = 1;
      drefDep[0] = 0xfffffff0;
      carbonDeposit(hdl, dref, drefDep, 0);

      UInt32 chk;
      if (carbonExamineWord(hdl, bref, &chk, 0, 0) == eCarbon_ERROR)
      {
        stat = fail;
        cerr << "ExamineWord for b failed" << endl;

      }
      else if (chk != 1)
      {
        stat = fail;
        cerr << "ExamineWord for b returned incorrect result" << endl;
      }
      if (carbonExamineWord(hdl, dref, &chk, 0, 0)  == eCarbon_ERROR)
      {
        stat = fail;
        cerr << "ExamineWord for dref failed" << endl;
        
      }
      else if (chk != 0xfffffff0)
      {
        stat = fail;
        cerr << "ExamineWord for dref[0] returned incorrect result" << endl;
      }

      if (carbonExamineWord(hdl, dref, &chk, 1, 0)  == eCarbon_ERROR)
      {
        stat = fail;
        cerr << "ExamineWord for dref failed" << endl;
        
      }
      else if (chk != 1)
      {
        stat = fail;
        cerr << "ExamineWord for dref[1] returned incorrect result" << endl;
      }

      // don't exceed the verilog size or a debug assert will fire.
      //chk = 0xfeedface;
      chk = 0x2;
      if (carbonDepositWord(hdl, dref, chk,  1, 0) == eCarbon_ERROR)
      {
        stat = fail;
        cerr << "Deposit for vector failed" << endl;
      }
      
      if (! doPlay)
      {
        if (carbonDepositWord(hdl, dref, chk,  1, 1) != eCarbon_ERROR)
        {
          stat = fail;
          cerr << "DepositWord for vector returned non-error when trying to unset drive bits on non-tristate" << endl;
        }
      }

      // suppress the previous message and try it again.
      // first try a bogus message number
      if (carbonChangeMsgSeverity(hdl, 5555555, eCarbonMsgError) == eCarbon_OK)
      {
        stat = fail;
        cerr << "Msg Severity change succeeded on bogus msg number" << endl;
      }

      // now try to change a fatal
      if (carbonChangeMsgSeverity(hdl, 5064, eCarbonMsgWarning) == eCarbon_OK)
      {
        stat = fail;
        cerr << "Msg Severity change succeeded changing a fatal" << endl;
      }

      if (carbonChangeMsgSeverity(hdl, 5065, eCarbonMsgSuppress) != eCarbon_OK)
      {
        stat = fail;
        cerr << "Msg Severity change didn't succeed" << endl;
      }
      
      if (! doPlay)
      {
        if (carbonDepositWord(hdl, dref, chk,  1, 1) != eCarbon_ERROR)
        {
          stat = fail;
          cerr << "DepositWord for vector returned non-error when trying to unset drive bits on non-tristate II" << endl;
        }
      }

      // unsuppress
      if (carbonChangeMsgSeverity(hdl, 5065, eCarbonMsgWarning) != eCarbon_OK)
      {
        stat = fail;
        cerr << "Msg Severity change didn't succeed II" << endl;
      }
      
      chk = 1;
      if (carbonExamineWord(hdl, dref, &chk, 0, 0)  == eCarbon_ERROR)
      {
        stat = fail;
        cerr << "ExamineWord for dref failed" << endl;
        
      }
      else if (chk != 0xfffffff0)
      {
        stat = fail;
        cerr << "ExamineWord for dref[0] returned incorrect result" << endl;
      }

      if (carbonExamineWord(hdl, dref, &chk, 1, 0)  == eCarbon_ERROR)
      {
        stat = fail;
        cerr << "ExamineWord for dref failed" << endl;
      }
      else if (chk != 0x2)
      {
        stat = fail;
        cerr << "ExamineWord for dref[1] returned incorrect result" << endl;
      }

      arefDep[0] = 0x3ff;
      carbonDeposit(hdl, aref, arefDep, 0);

      UInt32 arefchk[1];
      carbonExamine(hdl, aref, arefchk, 0);
      
      if (arefchk[0] != 0x3ff)
      {
        stat = fail;
        cerr << "Incorrect value for examine returned for 10 bit vec" << endl;
      }
      
      if (carbonExamineWord(hdl, aref, &chk, 0, 0) == eCarbon_ERROR)
      {
        stat = fail;
        cerr << "examineword for 10 bit vec failed" << endl;
      }
      else if (chk != 0x3ff)
      {
        stat = fail;
        cerr << "Incorrect value for examine returned for 10 bit vec" << endl;
      }

      erefDep[0] = 0x9;
      carbonDeposit(hdl, eref, erefDep, 0);

      UInt32 erefchk[1];
      carbonExamine(hdl, eref, erefchk, 0);
      if (erefchk[0] != 0x9)
      {
        stat = fail;
        cerr << "Incorrect value for examine returned for 4 bit vec" << endl;
      }

      if (carbonExamineWord(hdl, eref, &chk, 0, 0) == eCarbon_ERROR)
      {
        stat = fail;
        cerr << "examineword for 4 bit vec failed" << endl;
      }
      else if (chk != 0x9)
      {
        stat = fail;
        cerr << "Incorrect value for examine returned for 4 bit vec" << endl;
      }

      crefDep[0] = 0x0f0f0f00;
      carbonDeposit(hdl, cref, crefDep, 0);

      UInt32 crefchk[1];
      carbonExamine(hdl, cref, crefchk, 0);
      if (crefchk[0] != 0x0f0f0f00)
      {
        stat = fail;
        cerr << "Incorrect value for examine returned for 32 bit vec" << endl;
      }
      
      if (carbonExamineWord(hdl, cref, &chk, 0, 0) == eCarbon_ERROR)
      {
        stat = fail;
        cerr << "examineword for 32 bit vec failed" << endl;
      }
      else if (chk != 0x0f0f0f00)
      {
        stat = fail;
        cerr << "Incorrect value for examine returned for 32 bit vec" << endl;
      }
      
      frefDep[0] = 0x0000000f;
      frefDep[1] = 0xffffffff;
      frefDep[2] = 0x00000000;
      carbonDeposit(hdl, fref, frefDep, 0);
      
      UInt32 frefchk[3];
      carbonExamine(hdl, fref, frefchk, 0);

      if ((frefchk[0] != frefDep[0]) || (frefchk[1] != frefDep[1])
          || frefchk[2] != frefDep[2])
      {
        stat = fail;
        cerr << "Incorrect value for examine returned for 72 bit vec" << endl;
      }

      if (carbonExamineWord(hdl, fref, &chk, 1, 0) == eCarbon_ERROR)
      {
        stat = fail;
        cerr << "examineword for 72 bit vec failed" << endl;
      }
      else if (chk != 0xffffffff)
      {
        stat = fail;
        cerr << "Incorrect value for examine returned for 72 bit vec" << endl;
      }
    }
    
    if (i == 7)
    {
      carbonDisableNetCB(hdl, cb);
      carbonDumpOn(wave);
    }
    
    carbonSchedule(hdl, time);
    time += 10;
  }

  assert(carbonExamine(hdl, clkref, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(hdl, aref, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(hdl, bref, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(hdl, cref, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(hdl, dref, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(hdl, eref, NULL, NULL) == eCarbon_OK);
  assert(carbonExamine(hdl, fref, NULL, NULL) == eCarbon_OK);

  assert(carbonExamineRange(hdl, clkref, NULL, 0, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(hdl, aref, NULL, 1, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(hdl, bref, NULL, 0, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(hdl, cref, NULL, 1, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(hdl, dref, NULL, 1, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(hdl, eref, NULL, 1, 0, NULL) == eCarbon_OK);
  assert(carbonExamineRange(hdl, fref, NULL, 1, 0, NULL) == eCarbon_OK);
  
  // Try the free handle function
  carbonFreeHandle(hdl, &dref);
  if (dref != NULL)
  {
    stat = fail;
    cerr << "Setting of freed dref to NULL failed." << endl;
  }

  // deposit null to clock
  carbonDeposit(hdl, clkref, NULL, NULL);

  // deposit null to clock
  carbonDepositRange(hdl,clkref, NULL, 0, 0, NULL);
  
  carbonDumpFlush(wave);
  carbonDestroy(&hdl);

  delete [] drefDep;

  // check the call back number again
  if (numClkChanges + 1 != numClkCBs)
  {
    cerr << "Callback calls wrong after disable, expected " 
         << numClkChanges + 1 << ", got "
         << numClkCBs << endl;
    stat = fail;
  }

  // Now change input b before a posedge, run the clock, save,
  // restore, change b again, run schedule, and then change b back and
  // run the schedule. This makes sure that the vhm gets restored with
  // the proper change array and init attributes.
  hdl = CARBON_CREATE_FUNC(eCarbonFullDB, eCarbon_NoFlags);
  if (! hdl)
    return fail;

  time = 0;

  bout = carbonFindNet(hdl, "flopreg.bout");
  clkref = carbonFindNet(hdl, "flopreg.clk");
  bref = carbonFindNet(hdl, "flopreg.b");

  // 1: Get the flop in a negedge state. Should be anyway, but just in
  // case
  clkRefDep[0] = 0;
  carbonDeposit(hdl, clkref, clkRefDep, 0);
  carbonSchedule(hdl, time);
  time += 10;
  
  clkRefDep[0] = 1;
  carbonDeposit(hdl, clkref, clkRefDep, 0);
  carbonSchedule(hdl, time);
  time += 10;

  clkRefDep[0] = 0;
  carbonDeposit(hdl, clkref, clkRefDep, 0);
  carbonSchedule(hdl, time);
  time += 10;

  //2: deposit a change to b and a 1 to clk
  brefDep[0] = 1;
  carbonDeposit(hdl, bref, brefDep, 0);
  clkRefDep[0] = 1;
  carbonDeposit(hdl, clkref, clkRefDep, 0);
  carbonSchedule(hdl, time);
  CarbonTime savedTime = time;
  time += 10;
  
  //3: Make sure that bout sees the change
  CarbonUInt32 boutRef = 0;
  carbonExamine(hdl, bout, &boutRef, 0);
  assert(boutRef == brefDep[0]);
  
  // Change the value of clk to a 0, but do not run the schedule
  //  clkRefDep[0] = 0;
  //carbonDeposit(hdl, clkref, clkRefDep, 0);

  //4: Save
  carbonSave(hdl, "initcheck_vhm.db");
  
  //5: Destroy, create, and restore
  carbonDestroy(&hdl);
  hdl = CARBON_CREATE_FUNC(eCarbonFullDB, eCarbon_NoFlags);
  if (! hdl)
    return fail;
  
  time = 0;
  
  bout = carbonFindNet(hdl, "flopreg.bout");
  clkref = carbonFindNet(hdl, "flopreg.clk");
  bref = carbonFindNet(hdl, "flopreg.b");
  
  carbonRestore(hdl, "initcheck_vhm.db");
  time = carbonGetSimulationTime(hdl);
  assert(time == savedTime);

  //6: Deposit a value that will not affect bout (no posedge)
  CarbonUInt32 origBVal = boutRef;
  brefDep[0] = ! brefDep[0];
  carbonDeposit(hdl, bref, brefDep, 0);
  // Deposit a 1 to the clk again
  carbonDeposit(hdl, clkref, clkRefDep, 0);
  carbonSchedule(hdl, time);
  time += 10;
  carbonExamine(hdl, bout, &boutRef, 0);
  assert(boutRef == origBVal);

  //7: Run a negedge schedule
  clkRefDep[0] = 0;
  carbonDeposit(hdl, clkref, clkRefDep, 0);
  carbonSchedule(hdl, time);
  time += 10;

  //8: change b back and run a posedge schedule
  brefDep[0] = ! brefDep[0];
  carbonDeposit(hdl, bref, brefDep, 0);
  clkRefDep[0] = 0;
  carbonDeposit(hdl, clkref, clkRefDep, 0);
  carbonSchedule(hdl, time);
  time += 10;
  carbonExamine(hdl, bout, &boutRef, 0);
  assert(boutRef == brefDep[0]);

  carbonDestroy(&hdl);

  delete [] brefDep;
  return stat;
}
