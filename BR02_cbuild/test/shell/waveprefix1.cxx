#include "libwaveprefix1.h"
#include "shell/CarbonModel.h"
#include <iostream>

bool findNet(CarbonObjectID* hdl, const char* netName, CarbonNetID** netRef)
{
  bool error = false;
  *netRef = carbonFindNet(hdl, netName);
  if (*netRef == NULL)
  {
    error = true;
    std::cerr << "Error: Failed to get a handle to " << netName << "\n";
  }
  return error;
}

int main()
{
  CarbonTime time = 0;
  bool error = false;
  CarbonObjectID* hdl = carbon_waveprefix1_create(eCarbonFullDB, eCarbon_NoFlags);

  // Create the waveform for a lower level
  CarbonWaveID* wave = carbonWaveInitVCD(hdl, "waveprefix1.vcd", e1ns);
  carbonPutPrefixHierarchy(wave, "tb.core", true);
  carbonDumpVars(wave, 0, "top.F1,top.F2");

  // Get access to the nets
  CarbonNetID* clkRef;
  CarbonNetID* enRef;
  CarbonNetID* in1Ref;
  CarbonNetID* in2Ref;
  error |= findNet(hdl, "top.clk", &clkRef);
  error |= findNet(hdl, "top.en", &enRef);
  error |= findNet(hdl, "top.in1", &in1Ref);
  error |= findNet(hdl, "top.in2", &in2Ref);
  if (error) {
    return 1;
  }

  // Stimulate the design
  for (int i = 0; i < 256 && !error; ++i, time += 5) {
    UInt32 clk = i & 1;
    UInt32 en = i & 0x8;
    UInt32 in1 = i & 0xFF;
    UInt32 in2 = ((i & 0xF) << 4) | ((i & 0xF0) >> 4);
    error |= carbonDeposit(hdl, clkRef, &clk, 0) != eCarbon_OK;
    error |= carbonDeposit(hdl, enRef, &en, 0) != eCarbon_OK;
    error |= carbonDeposit(hdl, in1Ref, &in1, 0) != eCarbon_OK;
    error |= carbonDeposit(hdl, in2Ref, &in2, 0) != eCarbon_OK;
    error |= carbonSchedule(hdl, time) != eCarbon_OK;
  }

  carbonDestroy(&hdl);
  if (error) {
    return 2;
  } else {
    return 0;
  }
} // int main
