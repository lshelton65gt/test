#include "libmessage.h"
#include <stdio.h>

/*
 * print the message using user-data as prefix
 *
 * to test stop/continue semantic, return continue 
 * unless given prefix data is NULL.  thus a NULL callback prefix
 * should prevent any other messages from coming out.
 */
eCarbonMsgCBStatus messageFunc(CarbonClientData data, 
                               CarbonMsgSeverity severity, 
                               int number,
                               const char* text,
                               unsigned int len)
{
  char *prefix;
  prefix = (char *) data;

  fprintf(stderr, "%s: severity %d, number %d: %s\n", prefix == NULL ? "STOP CALLBACK" : prefix, severity, number, text);

  return prefix == NULL ? eCarbonMsgStop : eCarbonMsgContinue;
}

void generateMessage(const char *message, CarbonObjectID *model)
{
  static UInt32 t = 0;
  static CarbonNetID *clk = NULL;
  static UInt32 zero = 0;
  static UInt32 one = 1;

  /* output given message to explain what is being tested */
  fprintf(stderr, message);

  if (clk == NULL)
    clk = carbonFindNet(model, "message.clk");

  /* global message only */
  carbonSetFilePath("");

  /* find a net that does not exist, to generate a message from the shell */
  carbonFindNet(model, "unknown");

  /* run the schedule to generate a message from the model */
  carbonDeposit(model, clk, &zero, 0);
  carbonDeposit(model, clk, &one, 0);
  if (model != NULL)
    carbonSchedule(model, t++);

  fprintf(stderr, "\n");
}

main()
{
  CarbonObjectID    *model;
  CarbonMsgCBDataID *globalcb, *cb1, *cb2, *cb3;

  /*
   * disable buffering on stdout and stderr to force messages from the test
   * driver and model come out in a consistent order.
   */
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);

  /* generate a message */
  generateMessage("no callbacks defined (and no model created).\n", NULL);

  /* register a callback before creating a model, generate a message */
  globalcb = carbonAddMsgCB(NULL, messageFunc, (void *) "GLOBAL CALLBACK");
  generateMessage("global callback defined (and no model created).\n", NULL);

  model = carbon_message_create(eCarbonFullDB, eCarbon_NoFlags);

  /* generate a message */
  generateMessage("only global callback defined.\n", model);

  /* register a callback, generate the message again */
  cb1 = carbonAddMsgCB(model, messageFunc, (void *) "CALLBACK 1");
  generateMessage("one callback defined.\n", model);

  /* 
   * register a second callback, generate again.
   * Note that the second callback mnessage should come BEFORE the first. 
   */
  cb2 = carbonAddMsgCB(model, messageFunc, (void *) "CALLBACK 2");
  generateMessage("two callbacks defined.\n", model);

  /*
   * register a third, this one with NULL data.
   * this causes the message function to return 'STOP', and should
   * prevent the output of the first two callbacks AND the system handler.
   */
  cb3 = carbonAddMsgCB(model, messageFunc, NULL);
  generateMessage("stopper callback defined.\n", model);

  /* now remove the third callback, should see the other two. */
  carbonRemoveMsgCB(model, &cb3);
  generateMessage("two callbacks defined.\n", model);

  /* remove the first callback just to mic it up */
  carbonRemoveMsgCB(model, &cb1);
  generateMessage("only callback 2 defined.\n", model);

  /* remove the last model-specific callback */
  carbonRemoveMsgCB(model, &cb2);
  generateMessage("no model-specific callbacks defined.\n", model);

  /* remove the global callback */
  carbonRemoveMsgCB(model, &globalcb);
  generateMessage("no callbacks defined.\n", model);

  /* clean up */
  carbonDestroy(&model);
}
