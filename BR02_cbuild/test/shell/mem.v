module top (out, clk, wen, data, waddr, raddr);
   output [`MEM_WIDTH-1:0]    out;
   input 		      clk, wen;
   input [`MEM_WIDTH-1:0]     data;
   input [`MEM_ADDR_BITS-1:0] waddr, raddr;

   // This is the memory
   reg [`MEM_WIDTH-1:0]        mem [`MEM_DEPTH-1:0];

   // Write the memory
   always @ (negedge clk)
     if (wen)
       mem[waddr] = data;

   // Read the memory
   assign 		       out = mem[raddr];

endmodule // top

		  
