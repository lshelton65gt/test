#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <assert.h>
#include <cstring>
using namespace std;

#define carbon_design_create  MODEL(create)

struct DesignInterface
{

  DesignInterface() : 
    m_wen(NULL),
    m_waddr(NULL),
    m_clk(NULL),
    m_raddr(NULL),
    m_out(NULL),
    m_data(NULL),
    mHdl(NULL),
    time(0)
  {}

  void initNets(CarbonObjectID* hdl)
  {
    mHdl = hdl;
    m_wen = carbonFindNet(mHdl, "top.wen");
    m_waddr = carbonFindNet(mHdl, "top.waddr");
    m_clk = carbonFindNet(mHdl, "top.clk");
    m_raddr = carbonFindNet(mHdl, "top.raddr");
    m_out = carbonFindNet(mHdl, "top.out");
    m_data = carbonFindNet(mHdl, "top.data");

    assert(m_wen && m_waddr && m_clk && m_raddr && m_out && m_data);
  }

  CarbonNetID* m_wen;
  CarbonNetID* m_waddr;
  CarbonNetID* m_clk;
  CarbonNetID* m_raddr;
  CarbonNetID* m_out;
  CarbonNetID* m_data;

  CarbonObjectID* mHdl;
  CarbonTime time;
};


template <typename T, typename V> void
printError(const char* str, int index, int wordIndex, T got, V exp)
{
  cerr << "Error: " << str << " top.mem[" << index << "][" << wordIndex;
  cerr << "] = " << got << "; should be " << exp << ".\n";
}

int
readSimulation(const char* str, CarbonObjectID* hdl, DesignInterface& ci,
	       UInt32 offset, int dataSize)
{
  int status = 0;

  UInt32 scalarVal = 0;
  carbonDeposit(hdl, ci.m_wen, &scalarVal, 0);
  carbonDeposit(hdl, ci.m_waddr, &scalarVal, 0);
  for (UInt32 i = 0; i < MEM_DEPTH; ++i)
  {
    // Run a cycle
    scalarVal = 0;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    carbonDeposit(hdl, ci.m_raddr, &i, 0);
    carbonSchedule(hdl, ci.time);
    ci.time += 10;
    
    scalarVal = 1;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    carbonSchedule(hdl, ci.time);
    ci.time += 10;

#if (((MEM_WIDTH + 31)/32) == 2)
    UInt32 exVal = 0;
    carbonExamineWord(hdl, ci.m_out, &exVal, 0, 0);

    if (exVal != (i + offset))
    {
      printError(str, i, 0, exVal, i + offset);
      status = 1;
    }

    carbonExamineWord(hdl, ci.m_out, &exVal, 1, 0);

    if (exVal != (i + offset + 1))
    {
      printError(str, i, 0, exVal, i + offset + 1);
      status = 1;
    }
#else
    for (int j = 0; j < dataSize; ++j)
    {
      UInt32 exVal = 0;
      carbonExamineWord(hdl, ci.m_out, &exVal, j, 0);
      if (exVal != (i + j + offset))
      {
	printError(str, i, j, exVal, i + j + 1);
        status = 1;
      }
    }
#endif
  } // for
  return status;
}

#if 0
int
readSimulation(const char* str, CarbonObjectID* hdl, carbon_design_interface& ci,
	       UInt32 offset, int dataSize)
{
  (void) dataSize;
  int status = 0;
  ci.m_wen = 0;
  ci.m_waddr = 0;
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    // Run a cycle
    ci.m_clk = 0;
    ci.m_raddr = i;
    carbon_design_schedule(hdl, &ci);
    ci.time += 10;
    ci.m_clk = 1;
    carbon_design_schedule(hdl, &ci);
    ci.time += 10;

    // Compare the results - for some reason the I/O interface is
    // different than the API interface.
#if (((MEM_WIDTH + 31)/32) == 1)
    if (ci.m_out != i + offset)
    {
      printError(str, i, 0, ci.m_out, i + 1);
      status = 1;
    }
#elif (((MEM_WIDTH + 31)/32) == 2)
    UInt64 expect = (((UInt64)(i + offset + 1) << 32) | (i + offset));
    if (ci.m_out != expect)
    {
      status = 1;
      printError(str, i, 0, ci.m_out, expect);
    }
#else
    for (int j = 0; j < dataSize; ++j)
    {
      if (ci.m_out[j] != (i + j + offset))
      {
	printError(str, i, j, ci.m_out[j], i + j + 1);
	status = 1;
      }
    }
#endif
  } // for
  return status;
}
#endif

void
writeSimulation(CarbonObjectID* hdl, DesignInterface& ci, UInt32 offset,
		int dataSize)
{
  UInt32 scalarVal = 1;
  carbonDeposit(hdl, ci.m_wen, &scalarVal, 0);
  scalarVal = 0;
  carbonDeposit(hdl, ci.m_raddr, &scalarVal, 0);

  for (UInt32 i = 0; i < MEM_DEPTH; ++i)
  {
    // Run a cycle
    scalarVal = 0;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);

    carbonDeposit(hdl, ci.m_waddr, &i, 0);

#if (((MEM_WIDTH + 31)/32) == 2)
    scalarVal = i + offset;
    carbonDepositWord(hdl, ci.m_data, scalarVal, 0, 0);
    scalarVal = i + offset + 1;
    carbonDepositWord(hdl, ci.m_data, scalarVal, 1, 0);
#else
    for (UInt32 j = 0; j < dataSize; ++j)
      carbonDepositWord(hdl, ci.m_data, i + j + offset, j, 0);
#endif
    carbonSchedule(hdl, ci.time);
    ci.time += 10;
    scalarVal = 1;
    carbonDeposit(hdl, ci.m_clk, &scalarVal, 0);
    carbonSchedule(hdl, ci.time);
    ci.time += 10;
  }
  scalarVal = 0;
  carbonDeposit(hdl, ci.m_wen, &scalarVal, 0);
} // writeSimulation

#if 0
void
writeSimulation(CarbonObjectID* hdl, carbon_design_interface& ci, UInt32 offset,
		int dataSize)
{
  (void) dataSize;

  ci.m_wen = 1;
  ci.m_raddr = 0;
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    // Run a cycle
    ci.m_clk = 0;
    ci.m_waddr = i;

    // For some reason the I/O interface is different than the API interface.
#if (((MEM_WIDTH + 31)/32) == 1)
    ci.m_data = i + offset;
#elif (((MEM_WIDTH + 31)/32) == 2)
    ci.m_data = (((UInt64)(i + offset + 1) << 32) | (i + offset));
#else
    for (int j = 0; j < dataSize; ++j)
      ci.m_data[j] = i + j + offset;
#endif
    carbon_design_schedule(hdl, &ci);
    ci.time += 10;
    ci.m_clk = 1;
    carbon_design_schedule(hdl, &ci);
    ci.time += 10;
  }
  ci.m_wen = 0;
} // writeSimulation
#endif


#if 0
void
checkSubrangeBounds(CarbonMemory* mem_ref)
{
  UInt32 mem_ref_val[1];
  mem_ref_val[0] = 0x0;
  assert(mem_ref);
  assert(mem_ref->examineMemoryRange(0, mem_ref_val, MEM_WIDTH, 0) == eCarbon_ERROR);

  assert(mem_ref->examineMemoryRange(0, mem_ref_val, MEM_WIDTH - 1, -1) == eCarbon_ERROR);

  assert(mem_ref->examineMemoryRange(0, mem_ref_val, MEM_WIDTH, -1) == eCarbon_ERROR);
  assert(mem_ref->examineMemoryRange(-1, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);

  assert(mem_ref->examineMemoryRange(MEM_DEPTH, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);

  assert(mem_ref->depositMemoryRange(0, mem_ref_val, MEM_WIDTH, 0) == eCarbon_ERROR);
  assert(mem_ref->depositMemoryRange(0, mem_ref_val, MEM_WIDTH - 1, -1) == eCarbon_ERROR);
  assert(mem_ref->depositMemoryRange(0, mem_ref_val, MEM_WIDTH, -1) == eCarbon_ERROR);
  assert(mem_ref->depositMemoryRange(-1, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);
  assert(mem_ref->depositMemoryRange(MEM_DEPTH, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);
}
#endif

void
checkSubrangeBounds(CarbonMemoryID* mem_ref)
{
  UInt32 mem_ref_val[1];
  mem_ref_val[0] = 0x0;
  assert(mem_ref);
  assert(carbonExamineMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH, 0) == eCarbon_ERROR);

  assert(carbonExamineMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH - 1, -1) == eCarbon_ERROR);

  assert(carbonExamineMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH, -1) == eCarbon_ERROR);
  assert(carbonExamineMemoryRange(mem_ref, -1, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);

  assert(carbonExamineMemoryRange(mem_ref, MEM_DEPTH, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);

  assert(carbonDepositMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH, 0) == eCarbon_ERROR);
  assert(carbonDepositMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH - 1, -1) == eCarbon_ERROR);
  assert(carbonDepositMemoryRange(mem_ref, 0, mem_ref_val, MEM_WIDTH, -1) == eCarbon_ERROR);
  assert(carbonDepositMemoryRange(mem_ref, -1, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);
  assert(carbonDepositMemoryRange(mem_ref, MEM_DEPTH, mem_ref_val, MEM_WIDTH - 1, 0) == eCarbon_ERROR);
}

int
main()
{
  int status = 0;
  DesignInterface ci;
  ci.time = 10;
  CarbonObjectID* hdl = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonObjectID* hdlCheck = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);

  ci.initNets(hdl);
  
  CarbonStatus cstatus;

  // Create a vcd file
  ostringstream stream;
  stream << "mem" << MEM_DEPTH << "_" << MEM_WIDTH << "vcd.dump";
  CarbonWaveID* wave = carbonWaveInitVCD(hdl, stream.str().c_str(), e1ns);
  if (! wave)
  {
    cerr << "dumpInit failed" << endl;
    return 1;
  }

  if (carbonDumpVars(wave, 0, "top") != eCarbon_OK)
  {
    cerr << "dumpvars failed" << endl;
    return 1;
  }
  
  // Get a handle to the memory to do all our work
  CarbonMemoryID* mref = carbonFindMemory(hdl, "top.mem");
  if (mref == NULL)
  {
    cerr << "Getting handle for top.mem failed\n";
    return 1;
  }
  
  checkSubrangeBounds(mref);

  // Allocate memory to do API I/O
  int dataSize = carbonMemoryRowNumUInt32s(mref);
  assert(dataSize == (MEM_WIDTH + 31)/32);
  UInt32* data = new UInt32[dataSize];
  UInt32* dataCheck = new UInt32[dataSize];
  
  int i, j;

  // save the simulation
  ostringstream saveFile;
  saveFile << SInt32(MEM_DEPTH) << "_" << SInt32(MEM_WIDTH) << "_presave.chkpt";
  assert(carbonSave(hdl, saveFile.str().c_str()) == eCarbon_OK);

  // now write to an address
  for (j = 0; j < dataSize; ++j)
    data[j] = 1;
  carbonDepositMemory(mref, 0,  data);

  // now restore
  assert(carbonRestore(hdl, saveFile.str().c_str()) == eCarbon_OK);

  // make sure that the address we wrote to is cleared
  carbonExamineMemory(mref, 0,  data);
  for (j = 0; j < dataSize; ++j)
    assert(data[j] == 0);
  
  // Write through the API
  for (i = 0; i < MEM_DEPTH; ++i)
  {
    for (j = 0; j < dataSize; ++j)
      data[j] = i + j + 1;
    
    cstatus = carbonDepositMemory(mref, i, data);
    if (cstatus != eCarbon_OK)
    {
      status = 1;
      cerr << "Failed to write top.mem[" << i << "]\n";
    }
  }
  // save the simulation and check that the memory is restored
  saveFile.clear();
  saveFile << SInt32(MEM_DEPTH) << "_" << SInt32(MEM_WIDTH) << "_save.chkpt";
  assert(carbonSave(hdl, saveFile.str().c_str()) == eCarbon_OK);
  assert(carbonRestore(hdlCheck, saveFile.str().c_str()) == eCarbon_OK);
  
  CarbonMemoryID* mrefCheck = carbonFindMemory(hdlCheck, "top.mem");
  if (mrefCheck == NULL)
  {
    cerr << "Getting handle for top.mem for hdlCheck failed\n";
    return 1;
  }

  for (i = 0; i < MEM_DEPTH; ++i)
  {
    for (j = 0; j < dataSize; ++j)
      data[j] = i + j + 1;
    
    cstatus = carbonExamineMemory(mrefCheck, i, dataCheck);
    if (cstatus != eCarbon_OK)
    {
      status = 1;
      cerr << "Failed to examine top.mem[" << i << "]\n";
    }
    else if (memcmp(data, dataCheck, dataSize * sizeof *dataCheck) != 0)
    {
      status = 1;
      cerr << "Restore failed. Address " << i << " incorrect." << endl;
    }
  }
  
  {
    char memBuf[MEM_WIDTH + 1];
    carbonFormatMemory(mrefCheck, memBuf, MEM_WIDTH + 1, eCarbonBin, 0);
    if (memBuf[MEM_WIDTH - 1] != '1')
    {
      status = 1;
      cerr << "carbonFormatMemory failed." << endl;
    }
  }
  // Read and write a range.
  UInt32 orig_range_data[1];
  UInt32 range_data[1];
  
  orig_range_data[0] = 0x00000000;
  if (carbonExamineMemoryRange(mref, 0, orig_range_data, 15, 0) != eCarbon_OK)
  {
    status = 1;
    cerr << "Failed to read top.mem[0]\n";
  }
  cout << orig_range_data[0] << endl;
  range_data[0] = ~ orig_range_data[0];
  if (carbonDepositMemoryRange(mref, 0, range_data, 15, 0) != eCarbon_OK)
  {
    status = 1;
    cerr << "Failed to write top.mem[0]\n";
  }
  range_data[0] = 0x00000000;
  if (carbonExamineMemoryRange(mref, 0, range_data, 15, 0) != eCarbon_OK)
  {
    status = 1;
    cerr << "Failed to read top.mem[0]\n";
  }
  cout << range_data[0] << endl;
  if (carbonDepositMemoryRange(mref, 0, orig_range_data, 15, 0) != eCarbon_OK)
  {
    status = 1;
    cerr << "Failed to write top.mem[0]\n";
  }
  range_data[0] = 0x00000000;
  if (carbonExamineMemoryRange(mref, 0, range_data, 15, 0) != eCarbon_OK)
  {
    status = 1;
    cerr << "Failed to read top.mem[0]\n";
  }
  cout << range_data[0] << endl;
  

  // Make sure data is 0 so that vcd dumping is consistent
  for (int j = 0; j < dataSize; ++j)
    carbonDepositWord(hdl, ci.m_data, 0, j, 0);

  // Read through simulation
  status |= readSimulation("API/Sim mismatch", hdl, ci, 1, dataSize);
  
  // Write through simulation
  writeSimulation(hdl, ci, 3, dataSize);

  // Read through the API
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    cstatus = carbonExamineMemory(mref, i, data);
    if (cstatus != eCarbon_OK)
    {
      status = 1;
      cerr << "Failed to read from top.mem[" << i << "]\n";
    }
    else
    {
      for (int j = 0; j < dataSize; ++j)
	if (data[j] != (UInt32) (i + j + 3))
	{
	  printError("Sim/API mismatch", i, j, data[j], (i + j + 3));
	  status = 1;
	}
    }
  }

  // Write through the API using the word routines
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    for (int j = 0; j < dataSize; ++j)
    {
      cstatus = carbonDepositMemoryWord(mref, i, i + j + 33, j);
      if (cstatus != eCarbon_OK)
      {
	status = 1;
	cerr << "Failed to write top.mem[" << i << "][" << j << "]\n";
      }
    }
  }
  
  
  // Read out a bit, change it, then replace it to orig.



  // Read through simulation again
  status |= readSimulation("Word API/Sim mismatch", hdl, ci, 33, dataSize);
  
  // Write through the simulation again
  writeSimulation(hdl, ci, 0x123400, dataSize);

  // Read from the word API
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    for (int j = 0; j < dataSize; ++j)
    {
      data[0] = carbonExamineMemoryWord(mref, i, j);
      if (data[0] != (unsigned)(i + j + 0x123400))
      {
        printError("Sim/API mismatch", i, j, data[0], (i + j + 0x123400));
        status = 1;
      }
    }
  }

  ostringstream memFilePrefix;
  memFilePrefix << "mem" << MEM_DEPTH << "x" << MEM_WIDTH;
  ostringstream memFile;

  memFile << memFilePrefix.str() << "_hex";
  carbonDumpAddressRange(mref, memFile.str().c_str(), MEM_DEPTH - 1, 0, eCarbonHex);
  memFile.str("");
  memFile << memFilePrefix.str() << "_bin";
  carbonDumpAddressRange(mref, memFile.str().c_str(), MEM_DEPTH - 1, 0, eCarbonBin);
  memFile.str("");
  memFile << memFilePrefix.str() << "_oct";
  carbonDumpAddressRange(mref, memFile.str().c_str(), MEM_DEPTH - 1, 0, eCarbonOct);
  memFile.str("");
  memFile << memFilePrefix.str() << "_dec";
  carbonDumpAddressRange(mref, memFile.str().c_str(), MEM_DEPTH - 1, 0, eCarbonDec);
  memFile.str("");
  memFile << memFilePrefix.str() << "_udec";
  carbonDumpAddressRange(mref, memFile.str().c_str(), MEM_DEPTH - 1, 0, eCarbonUDec);

  // Shutdown and return the results
  carbonDumpFlush(wave);
  carbonFreeMemoryHandle(hdl, &mref);
  assert(mref == NULL);
  carbonDestroy(&hdl);
  carbonDestroy(&hdlCheck);

  // Restart the vhm and check that carbonReadmemh works
  hdl = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  mref = carbonFindMemory(hdl, "top.mem");
  memFile.str("");
  memFile << memFilePrefix.str() << "_hex";
  assert(carbonReadmemh(mref, memFile.str().c_str()) == eCarbon_OK);
  CarbonMemFileID* memFileID = carbonReadMemFile(hdl, memFile.str().c_str(), 
                                                 eCarbonHex, MEM_WIDTH, 0);
  assert(memFileID);


  CarbonUInt32* dataZero = new UInt32[dataSize];
  memset(dataZero, 0, dataSize * sizeof(CarbonUInt32));
  memset(data, 0, dataSize * sizeof(CarbonUInt32));
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    carbonExamineMemory(mref, i, data);
    const CarbonUInt32* memFileRow = carbonMemFileGetRow(memFileID, i);
    if (memFileRow)
      assert(memcmp(memFileRow, data, dataSize * sizeof(CarbonUInt32)) == 0);
    else
      assert(memcmp(data, dataZero, dataSize * sizeof(CarbonUInt32)) == 0);
  }
  carbonFreeMemFileID(&memFileID);
  carbonDestroy(&hdl);

  // Restart the simulation and make sure that carbonReadmemb works
  hdl = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  mref = carbonFindMemory(hdl, "top.mem");
  memFile.str("");
  memFile << memFilePrefix.str() << "_bin";
  assert(carbonReadmemb(mref, memFile.str().c_str()) == eCarbon_OK);
  memFileID = carbonReadMemFile(hdl, memFile.str().c_str(), 
                                eCarbonBin, MEM_WIDTH, 0);
  assert(memFileID);


  memset(data, 0, dataSize * sizeof(CarbonUInt32));
  for (int i = 0; i < MEM_DEPTH; ++i)
  {
    carbonExamineMemory(mref, i, data);
    const CarbonUInt32* memFileRow = carbonMemFileGetRow(memFileID, i);
    if (memFileRow)
      assert(memcmp(memFileRow, data, dataSize * sizeof(CarbonUInt32)) == 0);
    else
      assert(memcmp(data, dataZero, dataSize * sizeof(CarbonUInt32)) == 0);
  }
  carbonFreeMemFileID(&memFileID);
  carbonDestroy(&hdl);

  delete [] dataZero;
  delete [] data;
  delete [] dataCheck;
  return status;
} // main
