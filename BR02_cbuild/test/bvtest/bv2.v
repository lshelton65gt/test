module bv2(in1,in2,in3,out1,out2,out3);
input [31:0] in1;
input [64:0] in2;
input [128:0] in3;
output [31:0] out1;
output [64:0] out2;
output [128:0] out3;

wire [73:0] net1 = {{5{^in2[7:4],&in1[31:24],|in3[123:94],~in2[64]}},(in3[128:93] ^ in3[127:95]), {in2[64:60]&in3[97:88],in2[64:57]}};

assign out1 = 0;
assign out2 = out3[127:73]|out3[120:80];
//assign out2 = 43;
assign out3 = (net1 << 55) | net1;

endmodule
