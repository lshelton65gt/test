module top(clk,o);
   input clk;
   output [39:0] o;
   reg [39:0] 	 o;
   reg [159:0] 	 operand;
   initial o = 0;
   always @(posedge clk) begin
      operand[159:80] = 80'shFFFFFFFFFF0000010000;
      o=operand[159:120];
   end
endmodule



