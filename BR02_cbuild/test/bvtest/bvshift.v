// Test shifts of bitvectors

module bvs(a,b,o,x,y,w);
   input [127:0] a;
   input [127:0] b;
   output [127:0] x;
   output [127:0] y;
   output [127:0] o;
   output [1:0]	  w;

   assign 	  o = a << b;
   assign         x = a >> b;

   assign 	  y = a << 128'hffffffffffffffffffffffff00000000;

   // Boundary case.  This should ALWAYS evaluate to zero
   assign 	  w[0] = (a << 128'd128) == 0;

   // This should always evaluate to one
   assign 	  w[1] = ((a|(128'd1<<127)) >> 128'd127) == 1;	  
   
endmodule
 	  
