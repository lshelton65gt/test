// left operand of shift should not be overloaded when it is negated.
// in this case, 3 bits is negated, and used as unsigned.
module fold(clk, mm, idx00, o0);
   input clk;
   input [41:0] mm;
   input [3:0] 	idx00;
   output [64:0] o0;
   reg [64:0] o0;
   wire [64:0] 	idx1[2:0];
   reg [7:0] 	idx;

   assign 	idx1[1] = mm<<(idx00[3:1]);
   assign 	idx1[0] = idx1[1] << (-idx00[2:0]);

   always @(posedge clk) begin
      o0<=idx1[0];
   end
endmodule
