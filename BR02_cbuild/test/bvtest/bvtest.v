module test(in0,in1,in2,in3,out1,out2,out3,out4);
input [39:0] in0;
input [79:0] in1;
input [159:0] in2;
input [319:0] in3;
output [79:0] out1;
output [159:0] out2;
output [319:0] out3;
output         out4;

assign out1 = {40{out4,1'b1}};
assign out2 = (in3[270:111] ^ in2) & {10{{8{&in0}},{4{~out1[17:12]}},in1[78:75]}};
assign out4 = |in0;
assign out3 = {{4{out2[155:140]}},{128{in1[65:63] == 3'b010}},{127{in1==80'h0ffabcde}},(in1==in2)};

endmodule
