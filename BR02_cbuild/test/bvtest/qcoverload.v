// Generated with: ./generator -b  -n 1849064556 -s 50 -o candidate.v
// Using seed: 0x6e367c6c
// Writing circuit of size 50 to file 'candidate.v'
module top(clk, N);
input clk;

  // net declarations
  wire clk;
  reg  [31:0] KGh=0;
  reg  [31:0] jK=0;
  reg  [31:0] gpNOM=0;
  reg  [31:0] H=0;
  reg  [31:0] tvQ_g=0;
  wire [93:16] dKiV=0;
  reg  [54:122] re=0;
  output [89:53] N;

  wire [36:107] K=0;
  wire tiUbE=0;
  wire HBJvz=0;
  wire o=0;
  reg   signed [18:117] I=0;
  wire q=0;
  wire  signed [29:97] y=0;
  reg  [20:83] u=0;
  wire [76:53] b=0;
  wire E=0;
  wire [96:51] l_=0;
  wire [20:32] oHc=0;
  reg   signed UF=0;
  reg  [5:95] Sjdkz=0;
  reg  [44:100] BsEHn=0;


  // data generators
  initial begin
    KGh = 32'h5e0688c2;
  end
  always@(posedge clk)
  begin
    KGh = 279470273*(KGh % 15) - 102913196 * (KGh / 15);
  end

  initial begin
    jK = 32'h5cc6b659;
  end
  always@(posedge clk)
  begin
    jK = 279470273*(jK % 15) - 102913196 * (jK / 15);
  end

  initial begin
    gpNOM = 32'h68079b5b;
  end
  always@(posedge clk)
  begin
    gpNOM = 279470273*(gpNOM % 15) - 102913196 * (gpNOM / 15);
  end

  initial begin
    H = 32'h2e34ff51;
  end
  always@(posedge clk)
  begin
    H = 279470273*(H % 15) - 102913196 * (H / 15);
  end

  initial begin
    tvQ_g = 32'h3e70c296;
  end
  always@(posedge clk)
  begin
    tvQ_g = 279470273*(tvQ_g % 15) - 102913196 * (tvQ_g / 15);
  end


  // assignments
  assign dKiV = (|H[8 : 2]);

  always@(posedge clk)
  begin
    re[54 : 122] = ((-54'h82e675c6)!=(^tvQ_g[H[31 : 16] +: 2]));
  end

  assign N[89 : 53] = ((((^((gpNOM<=110'h6bac057e)!=(115'h27127bed>(7'hd==(~13'h1cb0)))))<<(
(((61'h160be2c6 ? ((63'h2e147d70>107'h3c76c94c)!=(72'h2491bb60||112'hee690478)) : (jK<=(109'h5113912a ? 63'h81081492 : 122'he7794756)))>=H[28 : 3]) ? jK[1 : 1] : (re!=43'h9d0052a9)) ==
 ({(|gpNOM[26 : 4]),(KGh[31 : 0]|(79'h132f8f8a+(!tvQ_g))),H[(re[66 : 95]<<dKiV) -: 30],((jK ? KGh : ((54'h86d337d0&25'haff678)*KGh[1 : 0]))<<(|84'hd7541769))})
))&&69'h4bf68c85)>=(115'h3f21642a>=(re>=gpNOM)));
endmodule
