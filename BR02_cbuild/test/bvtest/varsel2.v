module varsel ( out, in, sel);
   input  in;
   input [7:0]    sel;
   output [0:64] out;
   reg [0:64] 	out;
   
   always @(in or sel)
     begin
        out = 65'b0;
        out[sel +: 32] = 64'habcdefabcdef0000;
        
     end
endmodule // varsel
