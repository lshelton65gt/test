// test of field extractions and insertions....

module testacc(val,val2,o);
   input [15:0] val;
   input [15:0] val2;
   output [71:0] o;
   
   wire [17:0] 	addr1 = genpar18(val, 1'b1);
   wire [17:0] 	addr2 = genpar18(val2, 1'b1);
   
   wire [35:0] 	addr1_2 = {addr1[17:16], addr2[17:16], addr1[15:0], addr2[15:0]};
   wire [71:0] 	wr_cmd = { 4'b0, addr1_2[35:32], 32'b1, addr1_2[31:0]};

   assign 	o = wr_cmd;

   function [17:0] genpar18;
      input [15:0] data;
      input odd;
            
      begin
	 genpar18 = {(^data[15:8])^odd, (^data[7:0])^odd, data};
      end
   endfunction
endmodule
