// test 8, 16, 32 bit field store operations.

module top(i,o);
   input [63:0] i;
   output [63:0] o;

   wire [63:0] 	 A;
   swapwords a(i,A);

   wire [63:0] 	 B; 	 
   swapshorts b(A,B);

   wire [63:0] 	 C;
   swapbytes c(B,C);

   // Should be completely swapped...
   assign 	 o = C;
   

endmodule

module swapwords(i,o);
   input [63:0] i;
   output [63:0] o;

   reg [63:0] r;
   always @(i) r = i;

   assign 	 {o[31:0], o[63:32]} = r;

endmodule

module swapshorts(i,o);
   input [63:0] i;
   output [63:0] o;

   reg [63:0] r;
   always @(i) r = i;

   assign 	 {o[47:32],o[63:48],o[15:0], o[31:16]} = r;
endmodule

module swapbytes(i,o);
   input [63:0] i;
   output [63:0] o;

   reg [63:0] r;
   always @(i) r = i;
   
   assign {o[55:48],o[63:56],
	   o[39:32],o[47:40],
	   o[23:16],o[31:24],
	   o[ 7: 0],o[15: 8]} = r;
endmodule
