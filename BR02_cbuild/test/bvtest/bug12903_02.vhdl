-- September 2009
-- This test case was created based on the bug12903_01.vhdl.
-- The line, which caused the failure, is simplified,
-- Now it contains only the slice operation.


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;


entity bug12903_02 is
  generic (
    W1 : integer := 73;
    W2 : integer := 66;
    S1 : integer := 4);
  
  port (
    in1  : in  signed(W1 downto 0);
    out1 : out signed(W2 downto 0));

end bug12903_02;


architecture arch of bug12903_02 is
  signal temp : signed(W2 downto 0) := (others => '1');    -- carbon1 observeSignal
begin  -- arch

  temp <= in1(W2+S1 downto S1);         -- this line is tested
  p1: process (in1, temp)
  begin

    out1 <= temp - 1;
  end process p1;

end arch;
