-- September 2009
-- This test case was created based on the customer design.
-- The issue was with slice of a vector, which is longer than 64 bits,
-- That operation was causing simulation mismatch.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;


entity bug12903_01 is
  generic (
    W1 : integer := 73);
  
  port (
    in1  : in  signed(W1 downto 0);
    out1 : out unsigned(W1-1 downto 0));

end bug12903_01;


architecture arch of bug12903_01 is

begin  -- arch

  
  p1: process (in1)
    variable temp_s : signed(W1 downto 0) := (others => '0');
    variable temp_u : unsigned(W1-1 downto 0) := (others => '0');
  begin

   temp_s := in1;
   temp_s(W1-1 downto 0) := temp_s(W1-1 downto 0) - 1;

   -- this line is tested
   temp_u := unsigned(not (std_logic_vector(temp_s(W1-1 downto 0))));  
  
   out1 <= temp_u;
  end process p1;

end arch;
