module shf(x,y,c);
   input [127:0] x;
   output [127:0] y;
   input [15:0] 	  c;

   assign 	  y = x << c;
endmodule

module crashme(i,o);
   input i;
   output o;

   wire [127:0] r;

   
   shf oob(r,r,16'd512);

   assign      o = ^r;
endmodule
