`define TOPBIT0 39	//39
`define TOPBIT1 79	//79
`define TOPBIT2 159	//159
`define TOPBIT3 159	//319


module test(in0,in1,in2,in3,out1,out2,out3,out4);
input [`TOPBIT0:0] in0;
input [`TOPBIT1:0] in1;
input [`TOPBIT2:0] in2;
input [`TOPBIT3:0] in3;
output [`TOPBIT1:0] out1;
output [`TOPBIT2:0] out2;
output [`TOPBIT3:0] out3;
output         out4;

assign out1 = in1 & {2{in0}};
//assign out2 = {out1[78:41],out1[79:42],out1[69:60]};	//bug208
assign out2[`TOPBIT2:0] = {out1[`TOPBIT1:0],out1[`TOPBIT1:0]};
assign out3[159:150] = 4;
assign out3[149:100] = {100{in2[111:110] == 2'b10}};		//bug209
assign out3[99:90] = in0[4:0];				//bug213
assign out3[89:68] = in1[72:60];
assign out3[67:60] = in3[110:100] ^ in3[109:99];
assign out3[59:0] = 32'habcdef98 << 8;			//bug211
assign out4 = (|out1) & ~(^out2) | (&out3);

endmodule
