module varsel ( out, in, sel,sel2 );
   input [0:64] in;
   input [7:0]    sel;
   input [7:0]    sel2;
   output [0:64] out;
   reg [0:64] 	out;
   
   always @(in or sel or sel2)
     begin
        out = 65'b0;
        out[sel +: 7] = in [sel2 +: 12];
        
     end
endmodule // varsel
