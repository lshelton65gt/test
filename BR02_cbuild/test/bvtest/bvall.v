module test(clk,ena,in0,in1,in2,out0,out1,out2,out3,out4,out5,out6,out7, out8, in9, in10, out9, out10);
  input clk, ena;
input [31:0] in0;
input [63:0] in1;
input [127:0] in2;
output [31:0] out0;
output [63:0] out1;
output [127:0] out2;
output [255:0] out3;
output [127:0] out4;
output [127:0] out5;
output [127:0] out6;
output [127:0] out7;
output      out8;
  input [127:0] in9, in10;
  output [127:0] out9, out10;
  reg [127:0] out9, out10;

wire [31:0] add32 = in0 + 67 + in2[75:44];
wire [31:0] sub32 = in0 - 987 - in1[63:32];
wire [31:0] or32 = in0 | 9238 | in1[44:13];
wire [31:0] and32 = in2[120:89] & in2[118:87] & 928;
wire [31:0] xor32 = in1[55:24] ^ in0 ^ in2[127:96];
wire [63:0] add64 = in1 + (-64'd2342) + in2[120:60];
wire [63:0] sub64 = in1 - 987 - out0;
wire [63:0] or64 = in1 | -64'd9238 | in2[74:13];
wire [63:0] and64 = in2[120:69] & in2[118:67] & 64'hfedcba9876543210;
wire [63:0] xor64 = in2[125:24] ^ in1 ^ in2[127:66];		
wire [127:0] add128 = add32 + add64 + in2 + 67'hdeadbeefa5a5a5def;	
wire [127:0] sub128 = in1 - 987 - out0;
wire [127:0] or128  = in1 | -64'd9238 | in2[74:13];
wire [127:0] and128 = in2[120:69] & in2[118:67] & 64'hfedcba9876543210;
wire [127:0] xor128 = in2[125:64] ^ in1 ^ in2[127:66];	

// test function parameters
   function fun;
      input [127:0] v;
      begin
	 fun = v[in0 & 7'h7f];		// implicit upreference
      end
   endfunction
   

assign out0 = add32 & sub32 ^ (or32 & ^and32) ^~ xor32;
assign out1 = ~(add64 | sub64) & or64 ~^ and64 ^ xor64;
assign out2 = - add128 - ~sub32 ^ or128 & and128 + xor128;
assign out3 = {{add32,33'h0,or32,and32[13:0],{14{~and64[63:61]}}},{13{out0[24:23]==out0[23:22]}}} * 128;
assign out4 = add32 ^ add64 ^ add128;
assign out5[127] = ~^add32 ^ |sub32 ^ (~&or32) ^ ~or128;
assign out5[126:100] = out4[93:44];
assign out5[99:70] = out5[120:100];
assign out5[69:0] = out3;
assign out6 = xor128 | and128 ^ or128 ^ sub128 ^ add128 ~^ {xor64,and64} & {64{or64[33:32]}} ^ sub64 << 64 ^ sub64;
assign out7 = 0;
  
assign out8 = fun(in2);

always @(posedge clk) begin
  if (ena) begin
    out9 <= in9;
    out10 <= in10;
  end else begin
    out9 <= -out9;         // want to see out9.negate(); without any extra temps generated
    out10 <= ~out10;       // want out10.flip();
  end
end
endmodule
