// Test bug1564, bug1565 bitvector memory getting extra copy constructors
// or passing bitvectors by reference to tasks and functions

module bug(i,o,c,p);

   input [7:0] i;
   output [65:0] o;
   input 	 c;
   output 	 p;

   reg [65:0] 	 mem[127:0];
   integer 	 k;
   reg 	      t;
   reg [65:0] r;
   
   initial begin
      for (k=0; k<128; k=k+1)
	mem[k] = k;
   end

   function parity;
      input [65:0] word;
      begin
	 parity = ^word;
      end
   endfunction
   
   always @(posedge c)
     begin
	mem[i] = i;
	t = parity(mem[i]);
     end




   assign     p = t;
   assign     o = r;

   always @(negedge c)
     begin
	r = mem[i];
	t = parity(mem[i]);
     end
endmodule
