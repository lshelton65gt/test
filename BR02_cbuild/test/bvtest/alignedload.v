// test 8, 16, 32 bit field load operations.

module top(i,o);
   input [63:0] i;
   output [63:0] o;

   wire [63:0] 	 A;
   swapwords a(i,A);

   wire [63:0] 	 B; 	 
   swapshorts b(A,B);

   wire [63:0] 	 C;
   swapbytes c(B,C);

   // Should be completely swapped...
   assign 	 o = C;

endmodule

module swapwords(i,o);
   input [63:0] i;
   output [63:0] o;

   reg [63:0] 	 r;

   always @(i)
     r = i;

   assign 	 o = {r[31:0],r[63:32]};	 

endmodule

module swapshorts(i,o);
   input [63:0] i;
   output [63:0] o;

   reg [63:0] 	 r;
   always @(i) r = i;

   function ident;
      input [15:0] i;
      ident = i;
   endfunction

   assign 	 o = {ident(r[47:32]),
		      ident(r[63:48]),
		      ident(r[15:0]),
		      ident(r[31:16])};
endmodule

module swapbytes(i,o);
   input [63:0] i;
   output [63:0] o;

   reg [63:0] 	 r;
   always @(i) r=i;

   function swb; 
      input [15:0] r;
      begin
	 swb = {r[7:0],r[15:0]};
      end
   endfunction
   
      
   assign o = {swb(r[63:48]),
	       swb(r[47:32]),
	       swb(r[31:16]),
	       swb(r[15:0])};

endmodule
