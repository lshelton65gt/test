// Test that we inline llvalue(k*32, 64) calls
//
// Trick here is that BitVector::llvalue() is instrumented with BVC perf
// counters, but direct codegen accesses don't get instrumented.
module top (input i, input [127:0] src, output [63:0] dest);
   assign dest = src[(i*32) +: 64] + src[((i*32)+32) +:64] + src[((i*32)+64) +: 32];
endmodule
