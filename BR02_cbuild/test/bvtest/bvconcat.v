// simple test that bvconcat's don't generate extraneous temporary

// How many BV<> constructors should we have?
// BV<128>() 68 constructions  (bug.m has 3 because of the value,idrive,xdrive,
//                              64 constructions  (test driver input memory)
//				and 1 for bug.i
// BV<132>() 1 member constructor for bug.o
// 
//

module bug(i,o,m,e,c);
   input [127:0] i;
   input 	 e;
   input 	 c;
   output [131:0] o;
   output [127:0] m;
   // Word swap, but no bitvector temp needed as we should reduce to multiple assignments
   assign 	  o[65:0] = {1'b0, i[31:0], i[63:32]};

   //  If we were really clever, no temp is required, there is no overlap here
   assign 	  o[131:66] = {2'b0, o[63:0]};

   reg [127:0] 	  x;
   assign 	  m = e?x:128'b0;

   initial x = 128'b0;
   always @(posedge c)
     begin
	x = m;
	x = {x[63:0], x[127:64]};
     end
   
endmodule
