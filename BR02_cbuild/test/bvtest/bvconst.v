// Test bitvector <op> constant cases for better codegen
//
// Can do BV <op> UInt32 without materializing a bitvector for the constant

module testme(i,o);
   input [31:0] i;
   output [31:0] o;

   wire [95:0]    iv;
   assign        iv = {i,i,i};
   
   assign        o[0] = (iv != 5);
   assign        o[1] = (iv|5) != 0;
   assign        o[2] = (iv - 5) != 5;
   assign        o[3] = (iv == 32'hffffffff);
   assign        o[4] = (iv == 96'h1ffffffff);// need a Bitvector or stripmine!
endmodule

                         
   
