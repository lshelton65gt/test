library ieee;
use ieee.std_logic_1164.all;

entity orig is
  
  port (o_in  : in  std_logic;
        o_out : out std_logic);

end orig;

architecture archorig of orig is

begin  -- archorig

  o_out <= o_in;

end archorig;

library ieee;
use ieee.std_logic_1164.all;

entity sub is
  
  port (s_in  : in  std_logic;
        s_out : out std_logic);

end sub;

architecture archsub of sub is

begin  -- archorig

  s_out <= not s_in;

end archsub;

library ieee;
use ieee.std_logic_1164.all;

entity vhdl_top is
  
  port (t_in  : in  std_logic;
        t_out : out std_logic);

end vhdl_top;

architecture archtop of vhdl_top is

begin  -- archorig

  u1 : entity work.orig 
    port map (o_in  => t_in,
              o_out => t_out);


end archtop;
