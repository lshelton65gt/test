module a(, c, b);

input c;
output b;

assign b = c;

endmodule


module k(, c, b);

input c;
output b;

a a1(.c(c), .b(b));

endmodule
