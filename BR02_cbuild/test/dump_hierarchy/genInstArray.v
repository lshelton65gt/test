// filename: test/langcov/Generate/genInstArray.v
// Description:  This test demonstrates a problem where an instance array was
// defined within a module that was defined within a generate IF



module genInstArray(clock, in1,  out1);
   input clock;
   input [7:0] in1;
   output [7:0] out1;
   parameter has=1;

   generate if (has) begin: NEON6
      mid u_mid(clock, in1, out1);
   end
   endgenerate
endmodule


module mid (clock, in1, out1);
   input clock;
   input [7:0] in1;
   output [7:0] out1;

   
   bot u_bot[0:1](clock, in1, out1); // the instance array

endmodule

module bot(clock, in1, out1);
   input clock;
   input [7:0] in1;
   output [7:0] out1;
   reg [7:0] 	out1;
   
   always @(posedge clock)
     begin: main
	out1 = in1;
     end


endmodule
