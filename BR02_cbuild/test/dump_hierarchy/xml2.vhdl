library ieee;
use ieee.std_logic_1164.all;
entity sub_foo_vhdl is
  generic (f_sub_foo_left  : integer := 3;
           f_sub_foo_right : integer := 0);
  port (f_sub_foo_pin  : in  std_logic_vector(1 downto 0);
        f_sub_foo_pout : out std_logic_vector(1 downto 0));
end sub_foo_vhdl;

architecture toparch1 of sub_foo_vhdl is
begin  -- toparch1
  f_sub_foo_pout <= f_sub_foo_pin;
end toparch1;

architecture toparch2 of sub_foo_vhdl is
begin  -- toparch2
  f_sub_foo_pout <= f_sub_foo_pin;
end toparch2;

library ieee;
use ieee.std_logic_1164.all;
entity foo_vhdl is
  generic (f_foo_left  : integer := 7;
           f_foo_right : integer := 0);
  port (f_foo_pin  : in  std_logic_vector(1 downto 0);
        f_foo_pout : out std_logic_vector(1 downto 0));
end foo_vhdl;
architecture toparch of foo_vhdl is
  component sub_foo_vhdl 
    generic (f_sub_foo_left  : integer := 3;
             f_sub_foo_right : integer := 0);
    port (f_sub_foo_pin  : in  std_logic_vector(1 downto 0);
          f_sub_foo_pout : out std_logic_vector(1 downto 0));
  end component;  
begin  -- toparch
  fs: sub_foo_vhdl generic map (f_foo_left, f_foo_right) port map (f_foo_pin, f_foo_pout);
end toparch;

library ieee;
use ieee.std_logic_1164.all;
entity bar_vhdl is
  generic (f_bar_left  : integer := 16;
           f_bar_right : integer := 0);
  port (f_bar_pin  : in  std_logic_vector(1 downto 0);
        f_bar_pout : out std_logic_vector(1 downto 0));
end bar_vhdl;
architecture toparch of bar_vhdl is
begin  -- toparch
  f_bar_pout <= f_bar_pin;
end toparch;

library ieee;
use ieee.std_logic_1164.all;
entity top_vhdl is
  generic (f_top_left  : integer := 8;
           f_top_right : integer := 1);
  port (f_top_pin  : in  std_logic_vector(1 downto 0);
        f_top_pout : out std_logic_vector(1 downto 0));
end top_vhdl;
architecture toparch of top_vhdl is
  component foo_vhdl 
    generic (f_foo_left  : integer := 3;
             f_foo_right : integer := 0);
    port (f_foo_pin  : in  std_logic_vector(1 downto 0);
          f_foo_pout : out std_logic_vector(1 downto 0));
  end component;
  component bar_vhdl
    generic (f_bar_left  : integer := 3;
             f_bar_right : integer := 0);    
    port (f_bar_pin  : in  std_logic_vector(1 downto 0);
          f_bar_pout : out std_logic_vector(1 downto 0));
  end component;
  component foo_v
    generic (f_foo_left  : integer := 3;
             f_foo_right : integer := 0);
    port (f_foo_pin  : in  std_logic_vector(1 downto 0);
          f_foo_pout : out std_logic_vector(1 downto 0));
  end component;
  component bar_v
    generic (f_bar_left  : integer := 3;
             f_bar_right : integer := 0);    
    port (f_bar_pin  : in  std_logic_vector(1 downto 0);
          f_bar_pout : out std_logic_vector(1 downto 0));
  end component;  
  signal tmp : std_logic_vector(1 downto 0);
begin  -- toparch
  f0: foo_vhdl generic map (f_top_right,            f_top_left)              port map (f_top_pin, tmp);
  b0: bar_vhdl generic map (f_top_left+f_top_right, f_top_left-f_top_right)  port map (tmp, f_top_pout);
  
  f1: foo_vhdl generic map (f_top_left-f_top_right, f_top_left+f_top_right)  port map (f_top_pin, tmp);
  b1: bar_vhdl generic map (f_top_left,             f_top_right)             port map (tmp, f_top_pout);
   
  f2: foo_vhdl generic map (f_foo_right => f_top_left-f_top_right, f_foo_left => f_top_left+f_top_right)  port map (f_top_pin, tmp);
  b2: bar_vhdl generic map (f_bar_right => f_top_left,             f_bar_left =>f_top_right)               port map (tmp, f_top_pout);
end toparch;
