module orig(o_in,o_out);
   input o_in;
   output o_out;

   assign o_out = o_in;
   
endmodule

module sub(s_in,s_out);
   input s_in;
   output s_out;

   assign s_out = !s_in;
   
endmodule

module vlog_top(t_in1,t_in2,t_out1,t_out2);
   input t_in1,t_in2;
   output t_out1,t_out2;

   orig s1(t_in1, t_out1);
   orig s2(.o_in(t_in2),.o_out(t_out2));
   
endmodule
