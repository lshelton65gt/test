library ieee;
use ieee.std_logic_1164.all;

library work;
use work.defs.all;

entity top5 is
  
  port (
    in1  : in  atype;
    out1 : out atype);

end top5;

architecture toparch of top5 is

begin  -- toparch

  out1 <= in1;

end toparch;
