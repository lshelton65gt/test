library ieee;
use ieee.std_logic_1164.all;

entity Bottom4 is
  
  port (Bin  : in  std_logic;
        Bout : out std_logic);

end Bottom4;

architecture BOTTOMARCH of bottom4 is

begin  -- bottomarch

  bout <= bin;

end BOTTOMARCH;

library ieee;
use ieee.std_logic_1164.all;

entity top4 is
  
  port (Tin  : in  std_logic;
        Tout : out std_logic);

end top4;

architecture top4arch of top4 is

  component bottom4
    port (Bin  : in  std_logic;
          Bout : out std_logic);
  end component;
  
  
begin  -- top4arch

  U1 : bottom4 port map (Bin  => Tin,
                         Bout => Tout);
    
end top4arch;

