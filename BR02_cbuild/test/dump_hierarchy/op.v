
`include "and1.v"

`undef FIRST
`define FIRST 1
`undef SECOND
`define SECOND 2

module op(a, b, c);

input a;
input b;
output c;

`undef SECOND
`define SECOND 3

and1 a1(a, b, c);

endmodule
