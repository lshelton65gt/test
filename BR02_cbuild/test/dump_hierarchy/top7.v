module bottom(bin, bout);
   input bin;
   output bout;

   assign bout = bin;

endmodule

module top7(tin1, tin2, tout);
   input tin1;
   input tin2;
   output tout;

   bottom u1(tin1 && tin2, tout);
   
endmodule
