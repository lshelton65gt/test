// The definition for module bottom is missing on purpose
module top(tin, tout);
   input tin;
   output tout;

   bottom u1(tin, tout);

endmodule
