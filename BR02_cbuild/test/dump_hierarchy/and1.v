
`undef FIRST
`define FIRST ONE
`undef SECOND
`define SECOND "hello"

module and1(a, b, c);

input a;
input b;
output c;

assign c = a & b;

endmodule
