
module b(input inp, output out);

assign out = inp;

endmodule

module a(input inp, output out);

b b1(inp, out);

endmodule
