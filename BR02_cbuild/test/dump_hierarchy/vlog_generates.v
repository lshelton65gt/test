// This test uses 'if', 'case', and 'for' generates to select
// what is instantiated based on technology parameters,
// parameters, and defparams.
//
// Used for testing bug 14914

module mid(out1, out2, in1, in2);

   output [6:0] out1;
   output       out2;
   input [6:0]  in1;
   input        in2;

   genvar i;
   generate
   for (i = 0; i < 7; i = i + 1)
     begin : gscopefor
        sub t1(out1[i], in1[i]);
     end
   endgenerate

   sub t2(out2, in2);
endmodule // mid

module sub(z, a);
   output z;
   input  a;

   parameter Mode = 2;

   generate
   if (Mode == 0)
     begin: mode0
         reg m0; 
        assign z = 1'b0;
     end
   else if (Mode == 1)
     begin: mode1
         reg m1; 
        assign z = ~a;
     end
   else if (Mode == 2)
     begin: mode2
         reg m2; 
        assign z = a;
     end
   else if (Mode == 3)
     begin: mode3
         reg m3; 
        assign z = 1'b1;
     end
   endgenerate
endmodule // sub

module grandchild (input wire din, output wire dout);
   assign dout = din;
endmodule // grandchild

module child
  (
   input wire din,
   output wire dout
   );

   grandchild \foo\a_ (din, dout);

endmodule

// Flip Flop - Type 'A' (arbitrary designation)
module a_ff 
(
input wire din, 
input wire srst, 
input wire clk, 
output reg dout
);

always @(posedge clk)
   begin
   if (srst == 1)
      dout <= 0;
   else      
      dout <= din;
   end

endmodule

// Flip Flop - Type 'B' (arbitrary designation)
module b_ff 
(
input wire din, 
input wire srst, 
input wire clk, 
output reg dout
);

always @(posedge clk)
   begin
   if (srst == 1)
      dout <= 0;
   else      
      dout <= din;
   end

endmodule

// This is a paramterized A shift register, using A flip flops
// (where 'A' is an arbitrary designation)
module a_sr
(
input wire din,
input wire srst,
input wire clk,
output wire dout
);

   parameter SIZE = 4;
   wire [SIZE:0] shiftdata;

   assign shiftdata[0] = din;

   genvar i;
   generate
      for (i = 0; i < SIZE; i = i + 1)
         begin: shiftbit
         a_ff bit1 (shiftdata[i], srst, clk, shiftdata[i+1]);
         end
      endgenerate

   assign dout = shiftdata[SIZE];  

endmodule

// This is a paramterized B shift register, using B flip flops
// (where 'B' is an arbitrary designation)
module b_sr
(
input wire din,
input wire srst,
input wire clk,
output wire dout
);

   parameter SIZE = 4;
   wire [SIZE:0] shiftdata;

   assign shiftdata[0] = din;

   genvar i;
   generate 
      for (i = 0; i < SIZE; i = i + 1)
         begin: shiftbit
         b_ff bit1 (shiftdata[i], srst, clk, shiftdata[i+1]);
         end
      endgenerate

   assign dout = shiftdata[SIZE];  

endmodule

// Top level DUT. This instantiates one of two shiftregisters, 
// depending on the value of a parameter.

module top
(
    input wire         din,
    input wire         srst,
    input wire         clk,
    output wire        dout,
    output wire [13:0] out1,
    output wire [1:0]  out2,
    input wire  [13:0] in1,
    input wire  [1:0]  in2
);

// Select technology "A" or "X" based on this value.
localparam tech = "A";

// Length of generated shift register
localparam shift_length = 2;

// Conditionally generate a shift register, based
// on technology paramater 'tech'
generate
  if (tech == "A")
     begin : \gen-a
     a_sr #(shift_length) u1 (din, srst, clk, dout);
     end
  else
     begin : \gen-b
     b_sr #(shift_length) u2 (din, srst, clk, dout);
     end   
endgenerate

// Conditionally instantiate flip flops, based on technology.
// This also tests multiple instantiations of a the same module
// on the same line (an obscure Verilog feature that presents
// a special case to the software).
generate
  begin: genlabel
  wire dout1, dout2; 
  case (tech)
     "A": a_ff a1 (din, srst, clk, dout1), a2 (din, srst, clk, dout2);
     "X": b_ff a1 (din, srst, clk, dout1), a2 (din, srst, clk, dout2);
  endcase
  end
endgenerate

child \inst.foo[1]\bar[0] (din, dout);

genvar k;
generate
   for (k = 0; k < 4; k = k + 1)
       begin : foogen
       wire foo; // carbon observeSignal
       end
endgenerate


// Make sure defparams properly configure the
// design hierarchy
defparam u1.gscopefor[0].t1.Mode = 1;
defparam u2.gscopefor[0].t1.Mode = 3;

mid u1(out1[13:7], out2[1], in1[13:7], in2[1]);
mid u2(out1[6:0], out2[0], in1[6:0], in2[0]); 
   
endmodule

