module top(io,o);
   inout io;
   output o;
   child child(io,o);
endmodule

module child(io,o);
   input io;
   output o;
   pad pad(io);
   buf_wrap buf_wrap(io,o);
endmodule

module buf_wrap(io,o);
   input io;
   output o;
   my_buf my_buf(io,o);
endmodule
module my_buf(i,o);
   input i;
   output o;
   assign o = i;
endmodule

module pad(io);
   inout io;
endmodule
