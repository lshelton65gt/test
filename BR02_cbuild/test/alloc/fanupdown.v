module top(in, out, in2, out2);
  input in, in2;
  output out, out2;

  child1 u1(in, tmp, tmp2, out2);
  child1 u2(tmp, out, in2, tmp2);
endmodule

module child1(in, out, in2, out2);
  input in, in2;
  output out, out2;
  assign out = ~in;
  assign out2 = ~in2;
endmodule

  