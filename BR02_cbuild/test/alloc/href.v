module top(clk, o1, i1, o2, i2);
  input clk, i1, i2;
  output o1, o2;

  reg    o1;
  initial o1 = 0;

  always @(posedge clk)
    o1 <= i1;

  sub sub(i2, o2);
  assign sub.clk = clk;
endmodule

module sub(i, o);
  input i;
  output o;
  reg    o;

  initial o = 0;
  wire    clk;
  always @(posedge clk)
    o <= i;
endmodule
  