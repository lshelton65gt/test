module top(clk,rst,en,d,q);
   input clk,rst,en,d;
   output q;
   sub sub(clk,rst,en,d,q);
endmodule

module sub(clk,rst,en,d,q);
   input clk,rst,en,d;
   output q;
   reg 	  q;
   wire drst = rst & en;
   always @(posedge clk or posedge drst)
     begin
	if (drst) begin
	end else begin
	   q = d;
	end
     end
endmodule
