module top(i1, i2, e1, e2, xout);
  input i1, i2, e1, e2;
  output xout;

  wire  undriven;
  pullup(undriven);

  mid mid(undriven, xout, i1, i2, e1, e2);
endmodule

module mid(out, xout, i1, i2, e1, e2);
  output out, xout;
  input i1, i2, e1, e2;

  low low(out, xout, i1, i2, e1, e2);
endmodule

module low(out, xout, i1, i2, e1, e2);
  output out, xout;
  input i1, i2, e1, e2;

  tribuf u1(out, i1, e1, xout);
  tribuf u2(out, i2, e2, xout);
endmodule

module tribuf(out, i, e, xout);
  output out, xout;
  input  i, e;

  assign out = e? i: 1'bz;
  assign xout = ~out;
endmodule
