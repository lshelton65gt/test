module top(i1, e1, i2, e2, i3, e3, i4, e4, i, e, o1, o2);
  input i1, e1, i2, e2, i3, e3, i4, e4, i, e;
  output o1, o2;

  // In one instantiation of trihier1, we hook up tmp and drive it
  trihier u1(i1, e1, i2, e2, o1, t1);
  tribuf u1a(i, e, t1);

  // In another instantiation of trihier1, we leave it unhooked
  trihier u2(i3, e3, i4, e4, o2, t2);
endmodule

module trihier(i1, e1, i2, e2, out, tmp);
  input i1, e1, i2, e2;
  output out;
  inout  tmp;
  tribuf u1(i1, e1, tmp);
  tribuf u2(i2, e2, tmp);
  assign out = ~tmp;
endmodule

module tribuf(i, e, out);
  input i, e;
  output out;
  assign out = e? i: 1'bz;
endmodule
