module top(i_a, i_b, e_a, o_a, o_b);
  input i_a, e_a, i_b;
  output o_a, o_b;

  wire   e_b = ~e_a;

  sub1_a sub1_a(i_a, e_a, o_a, bus);
  sub1_b sub1_b(i_b, e_b, o_b, bus);
endmodule

module sub1_a(i, e, o, bus);
  input i, e;
  output o;
  inout  bus;

  sub2_a sub2_a(i, e, o, bus);
endmodule

module sub1_b(i, e, o, bus);
  input i, e;
  output o;
  inout  bus;

  sub2_b sub2_b(i, e, o, bus);
endmodule

module sub2_a(i, e, o, bus);
  input i, e;
  output o;
  inout  bus;

  sub3_a sub3_a(i, e, o, bus);
endmodule

module sub2_b(i, e, o, bus);
  input i, e;
  output o;
  inout  bus;

  sub3_b sub3_b(i, e, o, bus);
endmodule

module sub3_a(i, e, o, bus);
  input i, e;
  output o;
  inout  bus;

  sub4_a sub4_a(i, e, o, bus);
endmodule

module sub3_b(i, e, o, bus);
  input i, e;
  output o;
  inout  bus;

  sub4_b sub4_b(i, e, o, bus);
endmodule

module sub4_a(i, e, o, bus);
  input i, e;
  output o;
  inout  bus;

  leaf_a leaf_a(i, e, o, bus);
endmodule

module sub4_b(i, e, o, bus);
  input i, e;
  output o;
  inout  bus;

  leaf_b leaf_b(i, e, o, bus);
endmodule

module leaf_a(i, e, o, bus);
  input i, e;
  output o;
  inout  bus;

  assign bus = e? i: 1'bz;
  not(o, bus);
endmodule

module leaf_b(i, e, o, bus);
  input i, e;
  output o;
  inout  bus;

  assign bus = e? i: 1'bz;
  not(o, bus);
endmodule

