module top(i1, i2, i3, o1, o2, o3);
  input i1, i2, i3;
  output o1, o2, o3;

  mid mid(i1, i2, i3, o1, o2, o3);
endmodule

module mid(i1, i2, i3, o1, o2, o3);
  input i1, i2, i3;
  output o1, o2, o3;

  sub u1(i1, i2, o1, o2);
  sub u1a(i1, i2, o1, o2);      // redundant
  sub u2(i3, i2, o3);

  pulldown(o1);
  pulldown(o2);
endmodule

module sub(i1, i2, o1, o2);
  input i1, i2;
  output o1, o2;

  assign o1 = ~i1;
  assign o2 = ~i2;
endmodule
