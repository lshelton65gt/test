module top(in, out, clk, ena);
  input in, clk, ena;
  output out;

  wire   gated_clk = clk & ena;
  sub sub(in, out, gated_clk, gated_clk);
endmodule

module sub(in, out, clk1, clk2);
  input in, clk1, clk2;
  output out;
  reg    out;

  always @(posedge clk2)
    out <= in;
endmodule
