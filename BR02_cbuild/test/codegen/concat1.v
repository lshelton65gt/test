// This test case found a problem with codegen when there is
// a repeat count of exactly 32. The code to emit it shared
// a function with mask generation which gets turned off with
// 32-bit sizes.
module mux (A, X, E, O);
   input [33:0] A, X;
   input         E;
   inout [33:0] O;
   
   assign        O[1:0] = A[1:0];
   assign        O[33:2] = (({32 {E}} & A[33:2]) | ({32 {(~E)}} & X[33:2]));
endmodule
