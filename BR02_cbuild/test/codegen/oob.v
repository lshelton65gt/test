// test of out-of bounds array access

module oob(a,i, b, c);
   inout [127:0] a;
   input [15:0]  i;               // too large for the vector we index
   output        b;
   output [7:0]  c;

   reg [7:0]     mem[0:127];
   assign        c = mem[i + 32]; // guarantee some OOB accesses
   assign        b = a[i];
endmodule
