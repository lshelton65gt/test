// test that all interesting things are inlined...

module top(i,e,o,c);
   input i;
   output [2:0] o;
   input        c,e;

   wire w;
   reg r;

   function f;
      input a;
      f = a >> 1;
   endfunction

   assign   o[2] = f({i,e});
   
   assign w = 0;	// do as a hard constant....
   assign o[1] = e ? i : 1'bz;

   always @(posedge c) r = ~i;

   always @(negedge c) r = w;

   assign o[0] = r;
   
endmodule

