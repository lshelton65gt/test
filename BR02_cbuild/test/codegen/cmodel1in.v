// In this test a c-model is used for my_model which has a 32-bit
// output and a 1-bit input.  The 1-bit input requires a temp with
// associated assignment, so the generated code is not just a single
// function call to a an extern "C" function.  So it should not be inlined.

module top (out, in);
  output [31:0] out;
  input  in;

  my_model S1 (out, in);
endmodule // top

module my_model (out, in);
  output [31:0] out;
  input  in;
endmodule

