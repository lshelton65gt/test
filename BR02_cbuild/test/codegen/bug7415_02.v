// The AMCC Rigel design found a bug where codegen
// crashed in NUEnabledDriver::emitCode if tristates
// are enabled and the enable input is wider than
// the output.
//
// This test has the enable as a concat to mimic
// the AMCC code and has the input from a register
// to even more closely mimic the AMCC code.

// This test requires -tristate z
  
module top (clk, a0, enable, x, y);
  input clk;
  output x,y;		
  input  a0;	
  input [1:0] enable;
  reg 	      a;
  always @(posedge clk) a = a0;
  assign      y = {1{enable}};
  bufif1 b (x, a, {1{enable}});
endmodule // top
