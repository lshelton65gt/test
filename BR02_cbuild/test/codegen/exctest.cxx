// Simple exception-handling test

struct Error{
  int i;
  Error (int ii) : i (ii){};
};

void razor (int i)
{
  if (i < 0)
    throw Error (i);

  return;
}

int main (void)
{

  try
  {
    for (int i=5; i > -2; --i)
      razor (i);
  }
  catch (Error x) {
    if (x.i == -1)
      return 0;
    else
      return 1;
  }

  return 2;                     // oh-oh, no exceptions raised
}
