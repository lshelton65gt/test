// This test checks that "x = x op y" is not codegened as "x op= y" when
// an Extender<...> is required for x. The op= form is invalid because
// Extender<...> cannot be used as an lvalue. The "odd" (i.e. non byte
// or word) size of the nets forces the use of an Extender.
// (see bug7301)
//
// Note: this test *must* be run with -noCodeMotion.

module test (clock, in1, out1);
  input clock;
  input [14:0] in1;
  output       signed [14:0] out1;
  reg 	       signed [14:0] out1;
  initial out1 = 15'b0;
  always @(posedge clock)
    begin
      out1 = in1;
      out1 = out1 << 2'h1;
    end
endmodule // test
