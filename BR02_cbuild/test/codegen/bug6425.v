// test large integers..
module top(c,o);
   input c;
   output [75:0] o;

   assign o = c ? 76'b0 : 76'h0008888888888888888; // literals that fit into 64 bits
endmodule
