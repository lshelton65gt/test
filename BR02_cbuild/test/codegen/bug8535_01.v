// filename: test/codegen/bug8535_01.v
// Description:  This test duplicates the issue seen in bug 8535
// it will get an internal error if you run with the commands:
// cbuild -q bug8535_01.v  -noRescope

module bug8535_01 (
		   ctl1,
		   ctl2,
		   din,
		   outval
		   );

   input ctl1, ctl2;
   input [83:0] din;
   output [20:0] outval;

   sub  U1(din, ctl1, ctl2, outval);

endmodule


module sub (a, ctl1, ctl2, value);
   input [83:0] a;
   input 	ctl1;
   input 	ctl2;
   output [20:0] value;
   reg [20:0] 	 value;


   reg [20:0] 	 mem1[0:0];
   reg [1:0] 	 mem2[0:0];

   reg [20:0] 	 temp0;

   reg [20:0] 	 temp1;

   reg [83:0] 	 temp_a;
   integer 	 i, j,tmp1;

   always @(temp0)
     begin
	value = temp0;
     end

   always @(a or ctl1)
     begin

	mem1[0] = 0;
	mem2[0] = 0;
	tmp1= 0  ;

	case (tmp1)		// required: this must be a variable, but can have a const value
	  0:
	    begin
	       for(j=0; j<84; j=j+1) // required: this loop
		 temp_a[j] = a[j];
	       task1(temp_a, ctl1, mem1[0], mem2[0]);
	    end
	  3:			// this is an unused branch
	    begin
	       if (function1(temp1, temp_a[62:42], ctl1))
		 begin
		 end
	    end
	endcase
	temp0 = mem1[0];
     end

   function function1;
      input [20:0] A, B;
      input ctl1;
      begin
	 function1 = 0;
      end

   endfunction

   task task1;
      input [83:0] inputReg;
      input ctl1;
      output [20:0] resultReg;
      output [1:0] minorIndex;

      reg [20:0]  inReg2, inReg4;
      reg [20:0] tmpReg1, tmpReg2;
      begin
	 inReg2 = inputReg[41:21];
	 inReg4 = inputReg[83:63];


	 tmpReg1 = inReg2;
	 tmpReg2 = inReg4;
	 if (function1(tmpReg1, tmpReg2, ctl1))
	   begin
	      resultReg = tmpReg1;
	      minorIndex = {1'b0, 1'b1};
	   end
	 else
	   begin
	      resultReg = tmpReg2;
	      minorIndex = {1'b1, 1'b1};
	   end
      end

   endtask

endmodule
