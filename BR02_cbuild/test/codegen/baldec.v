// operator < not getting found for Signed bitvector BUG5887
module top(s,o);
   input signed[65:0] s;
   output [7:0] o;

   assign o[0] = s < 66'sd20;
   assign o[1] = $signed(s[64:0]) > 66'sh2ffffffffffffffff;
   assign o[2] = s != 0;
   assign o[3] = s < 66'd20;    // unsigned compare
   assign o[4] = s > -1;
   assign o[5] = s >= 66'sh10000000000000000;
   assign o[6] = (-s) == (~s + 66'sd1);
   assign o[7] = s < 0;
endmodule
