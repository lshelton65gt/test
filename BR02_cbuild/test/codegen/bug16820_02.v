// filename: test/codegen/bug16820_02.v
// Description:  This test is based on feedback from a customer for bug 16820.
// This test (as opposed to bug16820_01.v) does produce the same error message
// that was seen by the customer and therefore it is thought that this test
// duplicates the situation seen in the customer design.


module bug16820_02(input clock, input [7:0] in1, in2, output reg [7:0] out1);

   integer width;
   
   function integer is_narrow;
      input integer width;
      begin
	 is_narrow = ( width < 16 ) ? 1 : 0;
      end
   endfunction
   
   function integer log2n;
      input integer width;
      begin
	 log2n = ( (  16 <= width ) && ( width <=  31) ) ? 5 :
                 ( (  32 <= width ) && ( width <=  63) ) ? 6 :
                 ( (  64 <= width ) && ( width <= 127) ) ? 7 :
                 ( ( 128 <= width ) && ( width <= 255) ) ? 8 : 
                 ( ( 256 <= width ) && ( width <= 511) ) ? 9 : 10;
      end
   endfunction

   function integer byte_width;
      input integer width;
      begin
	 byte_width = is_narrow(width) ? 4 : (( 2 ** log2n(width)) / 8);
      end
   endfunction

   function integer dword_width;
      input integer width;
      begin
	 dword_width = byte_width(width) / 4 ;
      end
   endfunction
   
   
   always @(posedge clock)
     begin: main
	width = in1;
	out1 = dword_width(width);
     end
endmodule
