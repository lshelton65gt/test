// corner case test for signed part selects

module probe(c,i,o);
   parameter vwidth=1;          // vector width to access
   parameter iwidth=1;         // index expression width
   input     c;
   input [vwidth-1:0] i;        // Initial value
   output             o;        // computed result

   reg [vwidth-1: 0] vector = 0;

   reg signed [iwidth-1:0]  index;
   always @(posedge c)
     begin : varsel
        vector = $signed(i[0+:iwidth]);
     end

   assign o = ^vector;
endmodule // probe

module mid(c,i,o);
   parameter vwidth=1;
   input     c;
   input [vwidth-1:0] i;
   output [7:0] o;

   probe #(vwidth, 1) x1__1 (c,i[0+:vwidth],o[0]);
   probe #(vwidth, 7) x1__7 (c,i[0+:vwidth],o[1]);
   probe #(vwidth, 8) x1__8 (c,i[0+:vwidth],o[2]);
   probe #(vwidth,31) x1_31 (c,i[0+:vwidth],o[3]);
   probe #(vwidth,32) x1_32 (c,i[0+:vwidth],o[4]);
   probe #(vwidth,49) x1_49 (c,i[0+:vwidth],o[5]);
   probe #(vwidth,64) x1_64 (c,i[0+:vwidth],o[6]);
   probe #(vwidth,65) x1_65 (c,i[0+:vwidth],o[7]);
endmodule
   

module top(c,i,o);
   input c;
   input [64:0] i;
   output [63:0] o;

   mid #( 1)xA (c,i[0+:1], o[ 7: 0]);
   mid #( 7)xB (c,i[0+:7], o[15: 8]);
   mid #( 8)xC (c,i[0+:8], o[23:16]);
   mid #(31)xD (c,i[0+:31],o[31:24]);
   mid #(32)xE (c,i[0+:32],o[39:32]);
   mid #(49)xF (c,i[0+:49],o[47:40]);
   mid #(64)xG (c,i[0+:64],o[55:48]);
   mid #(65)xH (c,i[0+:65],o[63:56]);

endmodule
