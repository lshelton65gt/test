// filename: test/codegen/bug12917_c.v
// Description:  This test is a variant of bug12917_a except here the temp
// vector is not zero based.

module bug12917_c(sel, in1, in2, dummy, out);
   input  sel, in1, in2;
   input [5:1] dummy;
   output [5:1] out;

   wire [-5:-4] 	temp;
   assign temp[-5:-4] = {in1, in2};
   
   assign  out = sel ? dummy : { {3{temp[-5]}}, temp[-5:-4]};  // this duplicates the problem

endmodule

