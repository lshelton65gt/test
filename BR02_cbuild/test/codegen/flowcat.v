// test codegen of concat in flow context
module top(i,o);
   input [15:0] i;
   output [1:0] o;
   assign       o[0] = ({i[0], 3'b0, i[15]}) ? 1 : 0;
   assign       o[1] = ({i[0], 3'b111, i[0]}) ? 1 : 0; // always true
endmodule
