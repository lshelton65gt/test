module top(in, out);
  input signed [6:0] in;
  output [6:0] out;

  assign       out = in >> 1;
endmodule
