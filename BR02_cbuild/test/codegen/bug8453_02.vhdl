-- filename: test/codegen/bug8453_02.vhdl
-- Description:  This test has a codegen error, it was discovered while
-- debugging 8453.  Once the codegen error is resolved there is no need to run
-- simulation tests on this because the test itself is nonsense since regs is
-- not driven but is used in the design.
-- Codegen error is fixed (bug8479)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg is
  type     t_dataArray is array(natural range <>) OF std_ulogic_vector(18 - 1 downto 0);
  type     t_idxArray is array(natural range <>) OF natural range 0 to 31;
end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkg.all;

entity bug8453_02 is
  port (
    clock    : in  std_logic;
    in0, in1, in2, in3 : in  std_ulogic_vector(18 - 1 downto 0);
    s_IndexArray : in  t_idxArray(0 to 3);
    sel : in std_ulogic_vector(2 downto 0);
    regs : buffer t_dataArray(0 to 27); -- must be undriven for codgen error
    out1     : out natural range 0 to 4);
end;

architecture arch of bug8453_02 is

begin
  main: process (clock)
    variable v_TmpCnt   : natural range 0 to 4;
  begin
    if clock'event and clock = '1' then 
      -- adding the following 4 lines eliminates the codegen error
--      regs(0) <= in0;
--      regs(1) <= in1;
--      regs(2) <= in2;
--      regs(3) <= in3;
      v_TmpCnt         := 0;
      case sel is
        when "001"  =>
          for i in 0 to 3 loop
            if (signed(regs(s_IndexArray(i))(18 - 1 downto 18 - 2)) /= 0 and
                signed(regs(s_IndexArray(i))(18 - 1 downto 18 - 2)) /= - 1)
            then
              v_TmpCnt := v_TmpCnt + 1;
            end if;
          end loop;
        when "010"  =>
          for i in 0 to 3 loop
            if (signed(regs(s_IndexArray(i))(18 - 1 downto 18 - 3)) /= 0 and
                signed(regs(s_IndexArray(i))(18 - 1 downto 18 - 3)) /= - 1)
            then
              v_TmpCnt := v_TmpCnt + 1;
            end if;
          end loop;
        when others =>                  -- "000"  => 
          v_TmpCnt     := 0;
      end case;

      out1 <= v_TmpCnt;
    end if;
  end process;
end;
