module BIG(i,o);
   input i;
   output o;
   assign o = i;
endmodule

module big(i,o);
   input i;
   output o;
   assign o = ~i;
endmodule

module top(i,o);
   input i;
   output o;
   wire   w;

   big a(i,w);

   wire   wb;
   BIG b(w,wb);
   big c(wb,o);
endmodule
