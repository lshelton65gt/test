// new corner case - sign-extend various operand sizes and compute expressions
module mac(x,y,alu);
   parameter aluwidth=32, xwidth=16, ywidth=16;

   inout signed [aluwidth-1:0] alu;
   input     [xwidth-1:0] x;
   input [ywidth-1:0]     y;

   assign                 alu = alu + $signed(x) * $signed(y);
endmodule


module top(a,b,o16_sym, o16_asym, o16_expr, o32, o48, o80,VVGO);
   input [31:0] a,b;
   inout [15:0] o16_sym, o16_asym, o16_expr;
   inout [31:0] o32;
   inout [47:0] o48;
   inout [79:0] o80;
   output       VVGO;

   mac #(16, 12, 4) alu16_asym (a[11:0], b[3:0], o16_asym);
   mac #(16, 8, 8) alu16_sym(a[7:0], b[7:0], o16_sym);

   mac #(32, 32, 16) alu32(a, b[16:1], o32);
   mac #(48, 24, 24) alu48(a[31:8], b[23:0], o48);
   mac #(80, 32, 32) alu80(a, b, o80);

   // example of really funky signextension calculation
   
   wire         signval = ((b > 0) ? b[7:0] : a[7:0]) >> 7;
   assign       o16_expr = {{8{signval}}, ((b>0) ? b[7:0] : a[7:0])};
   
   // folding example that doesn't directly produce a sign-extension
   assign       VVGO = ((102'h90f9a9ca > a) ? ({(a[b[29 : 12] +: 27] <= b),(a << 123'h70a691b),(&a)}) : (b <= 116'hb0d5180e));

endmodule

   

   
