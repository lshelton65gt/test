module top(input signed [9:0] s1,
           input signed [9:0] s2,
           input [9:0] u3,
           output signed [31:0] s4);
   assign         s4 = s1 << (s2 + u3);
endmodule


           
