library IEEE;
use IEEE.std_logic_1164.all;

package tclib is

    FUNCTION is_unknown (arg: STD_LOGIC) RETURN BOOLEAN ;

end tclib;

package body tclib is


    FUNCTION is_unknown (arg: STD_LOGIC) RETURN BOOLEAN IS
    BEGIN
       CASE arg IS
          WHEN 'U' | 'X' | 'Z' | 'W' | '-' => RETURN TRUE;
          WHEN OTHERS => NULL;
       END CASE;
       RETURN FALSE;
    
    END; 
    
end tclib;


library IEEE;
use IEEE.std_logic_1164.all;
use work.tclib.all;

entity tc is
    port (
        hclk       : in std_logic;
        hreset     : in std_logic;
        data_in    : in std_logic_vector(1 downto 0);
        bridge_state  : out std_logic_vector(6 downto 0));
end tc;

architecture rtl of tc is

begin

    bridge_fsm_process: PROCESS(hclk)
    BEGIN
   
    IF hclk'EVENT AND hclk = '1' THEN
       IF is_unknown(hreset) THEN
         bridge_state <= "XXXXXXX";
       ELSIF hreset = '1' THEN
         bridge_state <= "0000001";
       ELSE
         CASE data_in IS
           WHEN "00" => bridge_state <= "0000010";
           WHEN "01" => bridge_state <= "0000100";
           WHEN "10" => bridge_state <= "0001000";
           WHEN "11" => bridge_state <= "0010000";
           WHEN OTHERS =>  bridge_state <= "0000001";
  	 end CASE; 
                 
       END IF;
   END IF;
   end process bridge_fsm_process;

end rtl;

