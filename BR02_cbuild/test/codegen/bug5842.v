// test problem with UtSINT32_MIN

module top(i,x,o);
   input [2:0] i;
   input       x;
   output      o;

   reg [-2147483648: -2147483641] mem [-2147483648: -2147483641];
   reg                            v;
   assign                         o = v;
   always @(posedge x)
     begin
        mem[-2147483648+i] = mem[-2147483648+i] + 1;
     end

   always @(negedge x)
     begin
        v = ^mem[-2147483648+i];
     end
endmodule

   
