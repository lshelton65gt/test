module top(input signed [9:0] s1,
           input [9:0] u2,
           input signed [9:0] s3,
           output signed [128:0] s4);
   assign                s4 = s1 + (u2 >> s3);
endmodule


           
