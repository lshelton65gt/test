/* simplest testcase with $display, used so make sure that code for  */
/* $display that is part of an always block is not emitted in .h file so it will  */
/* not be inlined.  Note that the code for $display from initial block can be  */
/* in-lined because there is no instruction cost for initial blocks. */
/* see also dd0_i.v */
module top (clk);
   input clk;
//   initial
//      begin
//      $display("in initial block");
//      end

   always @(posedge clk)
     begin: main
       $display("in always block");
     end

endmodule
