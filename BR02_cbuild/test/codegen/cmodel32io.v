// In this test a c-model is used for my_model, it has a 32-bit 
// input and a 32 bit output. The generated code should be a single
// function call to an extern "C" function, and so it should be
// in-lineable.
// At the moment, however, we are generating temps in codegen even
// when the cmodel I/Os are exactly 32 bits.  So for now we shouldn't inline
// this.  That's why this test is marked TEXIT.

module top (out, in);
  output [31:0] out;
  input [31:0]  in;

  my_model S1 (out, in);
endmodule // top

module my_model (out, in);
  output [31:0] out;
  input [31:0]  in;
endmodule

