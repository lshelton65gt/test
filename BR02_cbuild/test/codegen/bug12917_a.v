// filename: test/codegen/bug12917_a.v
// Description:  This test duplicates the issue seen in bug 12917, it also
// includes two (commented) variations that avoid the problem

module bug12917_a(sel, in1, in2, dummy, out);
   input  sel, in1, in2;
   input [4:0] dummy;
   output [4:0] out;

   wire [1:0] 	temp;
   assign temp[1:0] = {in1, in2};
   
   assign  out = sel ? dummy : { {4{temp[1]}}, temp[0]};  // this duplicates the problem

//   assign  out = sel ? dummy : { {4{temp[1]}}, in2};      // this avoids the problem

//    assign  out = sel ? dummy : { {3{temp[1]}}, temp};  // this is joe's workaround
endmodule

