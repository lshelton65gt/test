// test oob checking for bitvector aligned word accesses

module top(input signed [7:0] i, output [31:0] val, output [31:0] val2, output [31:0] val3, output[31:0] val4);

   reg [63:0]       dvector = {32'h05050505, 32'hc0c0c0c0};
   assign       val2 = dvector[i[1:0]*32+:32];
   
   reg [127:0] vector = {32'h01234567, 32'h76543210, 32'h89abcdef, 32'hfedcba98};
   assign       val = vector[i*32 +:32];

   reg [127:0] bvect[3:0];
   assign      val3 = bvect[i%15][(i[7:3]*32) +: 32];

   reg [63:0]  dvect[3:0];
   assign      val4 = dvect[i%15][i[7:6]*32 +: 32];

   always @(posedge i[0])
     begin
        dvect[i%15][i[7:6]*32 +: 32] = i+1;
        bvect[i%16][i[7:3]*32 +: 32] = i - 1;
        
        dvector[i[1:0]*32 +: 32] = val2 + 1;
        vector[i*32 +: 32] = val - 1;
     end
   
endmodule
