-- Run with cbuild -q -vhdlTop bug7648_02 -memoryBVLimit 0 bug7648_02.vhd
--
-- In this test there is an assignment of the form X = X & Y where
-- X is a varsel with a memsel as the ident. This was identified as candidate for
-- transformation to X &= Y resulting in bad C++ code.

library ieee;
use ieee.std_logic_1164.all;

package package_7648_02 is
  type ich_hard_dec_array_t    is array(0 to 3) of std_logic_vector(3 downto 0);
  type ich_outputs_array_t     is array(0 to 4) of ich_hard_dec_array_t;
end package_7648_02;

library ieee;
use ieee.std_logic_1164.all;
use work.package_7648_02.all;

entity bug7648_02 is
  port (
    clock: in std_logic;
    in1:   in std_logic_vector(7 downto 0);
    i:     in integer range 0 to 3;
    out1:  out std_logic_vector(3 downto 0));
end;

architecture arch of bug7648_02 is
  signal x              : ich_outputs_array_t := (others => (others => "1011"));
begin
  process (clock, in1, x)
  begin
    if clock = '1' then
      if (in1 /= "0") then
       x(i)(0)(3) <= x(i)(0)(3) and not(in1(2));
      end if;
      out1 <= x(2)(0);
    end if;
  end process;
end;
