#!/usr/bin/perl
#
# Column 10 (x[14]) of the output of constsel sometimes depends on the value
# of out-of-bounds part selects.  Examining constsel.v shows x[14] is set by:
#   sub  #(66,17)xv66d(v66, p, x[14]);
# When input  p  is greater than 66 - 17 (i.e., vsize - selsize), we read
# invalid data.
# This script reads the input test vector and when it finds an input too large,
# it replaces column 10 of the corresponding line of output with an "x".

$vsize = 66;
$selsize = 17;

open(VECTORS, "libconstsel.test.vectors");
while ($vector = hex(<VECTORS>))
{
    #print $vector . "\n";
    $out = <STDIN>;
    if ($vector > $vsize - $selsize) { # If this input is large...
        substr($out, 9, 1) = 'x';       # Replace 10th digit with 'x'
    }
    print $out;
}
