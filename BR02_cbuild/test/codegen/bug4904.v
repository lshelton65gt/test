module top(a,b,e1,e2,out,out2);
   input [3:0] a,b;
   input       e1,e2;
   output [3:0] out;
   output       out2;
   assign       {out2, out[2:0]} = e1?a : (e2?b: 4'bzzzz);
   assign       out[3] = 1'b0;   // want to see out &= ~8

endmodule
