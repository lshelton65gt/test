// corner case test for signed comparisons
//    different size operands, ranging from 1 bit thru bitvector sizes
// Requires V2k flag

module top(i,o);
   input [63:0] i;
   output [80:0] o;

   // operands
   wire signed          i1 = $signed(i[0]);
   wire signed [6:0] i7 = $signed(i[7:1]);
   wire signed [7:0] i8 = $signed(i[7:0]);
   wire signed [30:0]i31 = $signed(i[30:0]);
   wire signed [31:0]i32 = $signed(i[31:0]);
   wire signed [32:0]i33 = $signed(i[32:0]);
   wire signed [62:0]i63 = $signed(i[62:0]);
   wire signed [63:0]i64=$signed(i);
   wire signed [64:0]i65 = $signed({i[31:0], 1'b0, i[63:32]});

   // outputs
   assign     o[0] = (i1 < i1);
   assign     o[1] = (i1 < i7);
   assign     o[2] = (i1 < i8);
   assign     o[3] = (i1 < i31);
   assign     o[4] = (i1 < i32);
   assign     o[5] = (i1 < i33);
   assign     o[6] = (i1 < i63);
   assign     o[7] = (i1 < i64);
   assign     o[8] = (i1 < i65);

   assign     o[9] = (i7 < i1);
   assign     o[10] = (i7 < i7);
   assign     o[11] = (i7 < i8);
   assign     o[12] = (i7 < i31);
   assign     o[13] = (i7 < i32);
   assign     o[14] = (i7 < i33);
   assign     o[15] = (i7 < i63);
   assign     o[16] = (i7 < i64);
   assign     o[17] = (i7 < i65);

   assign     o[18] = (i8 < i1);
   assign     o[19] = (i8 < i7);
   assign     o[20] = (i8 < i8);
   assign     o[21] = (i8 < i31);
   assign     o[22] = (i8 < i32);
   assign     o[23] = (i8 < i33);
   assign     o[24] = (i8 < i63);
   assign     o[25] = (i8 < i64);
   assign     o[26] = (i8 < i65);

   assign     o[27] = (i31 < i1);
   assign     o[28] = (i31 < i7);
   assign     o[29] = (i31 < i8);
   assign     o[30] = (i31 < i31);
   assign     o[31] = (i31 < i32);
   assign     o[32] = (i31 < i33);
   assign     o[33] = (i31 < i63);
   assign     o[34] = (i31 < i64);
   assign     o[35] = (i31 < i65);

   assign     o[36] = (i32 < i1);
   assign     o[37] = (i32 < i7);
   assign     o[38] = (i32 < i8);
   assign     o[39] = (i32 < i31);
   assign     o[40] = (i32 < i32);
   assign     o[41] = (i32 < i33);
   assign     o[42] = (i32 < i63);
   assign     o[43] = (i32 < i64);
   assign     o[44] = (i32 < i65);

   assign     o[45] = (i33 < i1);
   assign     o[46] = (i33 < i7);
   assign     o[47] = (i33 < i8);
   assign     o[48] = (i33 < i31);
   assign     o[49] = (i33 < i32);
   assign     o[50] = (i33 < i33);
   assign     o[51] = (i33 < i63);
   assign     o[52] = (i33 < i64);
   assign     o[53] = (i33 < i65);

   assign     o[54] = (i63 < i1);
   assign     o[55] = (i63 < i7);
   assign     o[56] = (i63 < i8);
   assign     o[57] = (i63 < i31);
   assign     o[58] = (i63 < i32);
   assign     o[59] = (i63 < i33);
   assign     o[60] = (i63 < i63);
   assign     o[61] = (i63 < i64);
   assign     o[62] = (i63 < i65);

   assign     o[63] = (i64 < i1);
   assign     o[64] = (i64 < i7);
   assign     o[65] = (i64 < i8);
   assign     o[66] = (i64 < i31);
   assign     o[67] = (i64 < i32);
   assign     o[68] = (i64 < i33);
   assign     o[69] = (i64 < i63);
   assign     o[70] = (i64 < i64);
   assign     o[71] = (i64 < i65);

   assign     o[72] = (i65 < i1);
   assign     o[73] = (i65 < i7);
   assign     o[74] = (i65 < i8);
   assign     o[75] = (i65 < i31);
   assign     o[76] = (i65 < i32);
   assign     o[77] = (i65 < i33);
   assign     o[78] = (i65 < i63);
   assign     o[79] = (i65 < i64);
   assign     o[80] = (i65 < i65);
endmodule
