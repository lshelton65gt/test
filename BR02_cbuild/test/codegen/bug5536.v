module top(c1,c2,a,b,c,out); // carbon disallowFlattening
   input c1,c2,a,b,c;
   output out;

   sub sub (c1,c2,a,b,c,out);
endmodule

module sub(c1,c2,a,b,c,out,uncalled);
   input c1,c2,a,b,c;
   output out;
   output uncalled;
   reg out;
   reg uncalled;

   always @(posedge c1) out = a & b;
   always @(posedge c2) uncalled = b & c;
   
endmodule

