// Generated with: ./generator -b  -n -1394616037 -s 50 -o candidate.v
// Using seed: 0xacdfd91b
// Writing circuit of size 50 to file 'candidate.v'
module top(clk, VA, Kc, Gi, BvY, jHh, mKtmU, Hh, b, Ygx, x);
input clk;
output VA, Kc, Gi, BvY, jHh, mKtmU, Hh, b, Ygx, x;

  // net declarations
  wire clk;
  reg  [31:0] Md=0;
  reg  [31:0] Mou=0;
  reg  [31:0] Ceki=0;
  reg  [31:0] L_=0;
  reg  [31:0] klLu=0;
  reg  [57:123] uDipo=0;
  wire [6:55] Qhy;
  reg   signed AnfD=0;
  reg  Hjj=0;
  wire  signed [122:82] NU_r;
  wire  signed rku;
  wire  signed [2:88] LWqJc;
  wire  signed prHn;
  wire  signed zbx;
  wire oq_Or;
  wire KkjR;
  wire [78:37] WF;
  wire [17:5] t;
  reg   signed _hY=0;
  wire QuMeq;
  reg  IofhJ=0;
  wire [72:125] DJc;
  reg  [12:29] oYvSM=0;
  wire [17:125] Clr;
  wire [18:37] VA;
  reg  l=0;
  wire _tAsh;
  wire [114:73] bb;
  wire [41:96] Pzroi;
  wire  signed [101:25] qP;
  wire i;
  wire CqN;
  wire [103:93] Kc;
  wire bS;
  reg   signed [24:80] jhCtx=0;
  wire [57:115] Ii;
  reg  q=0;
  wire  signed [81:99] W;
  wire  signed CWr;
  wire  signed [48:33] IM;
  reg  [76:7] Vt=0;
  reg  [39:3] lv_=0;
  reg   signed [107:7] dq=0;
  reg  [57:110] uGQf=0;
  wire  signed gm;
  reg  Gi=0;
  reg  [57:88] BvY=0;
  wire KHNXP;
  wire jHh;
  wire  signed [59:99] AUZ;
  wire mKtmU;
  wire [26:103] Hh;
  reg  b=0;
  reg   signed Ygx=0;
  wire [72:82] x;


  // data generators
  initial begin
    Md = 32'h2a4388e0;
  end
  always@(posedge clk)
  begin
    Md = 279470273*(Md % 15) - 102913196 * (Md / 15);
  end

  initial begin
    Mou = 32'h7cb06022;
  end
  always@(posedge clk)
  begin
    Mou = 279470273*(Mou % 15) - 102913196 * (Mou / 15);
  end

  initial begin
    Ceki = 32'h32bedb07;
  end
  always@(posedge clk)
  begin
    Ceki = 279470273*(Ceki % 15) - 102913196 * (Ceki / 15);
  end

  initial begin
    L_ = 32'h686b8437;
  end
  always@(posedge clk)
  begin
    L_ = 279470273*(L_ % 15) - 102913196 * (L_ / 15);
  end

  initial begin
    klLu = 32'h10c6053c;
  end
  always@(posedge clk)
  begin
    klLu = 279470273*(klLu % 15) - 102913196 * (klLu / 15);
  end


  // assignments
  always@(posedge clk)
  begin
    uDipo[57 : 123] = (~((~klLu[Md[klLu[Ceki[22 : 19] -: 18] +: 24] -: 30])!=({(((((klLu<=Ceki[17 : 11]) ? ((|Ceki[31 : 0])&&({114'hcd257194,Ceki[31 : 0],15'h769f,78'hfab7f5cc})) : ({15'h3ad9,(~46'h4d7025e1),(^122'heddc751d)}))||(((1'h1<=18'h2dd41)&&(Mou[2 : 2]>=71'h1133229f))|(^(113'he8df87a1|100'h49ca4d39))))&&(Mou^72'h8fe6b88d))<<(^((Mou[31 : 14]|37'hd78c747e)|({Md,Mou[28 : 16],(38'h32e3128a|Md[21 : 3]),(~Mou),(klLu[31 : 0]!=(39'h28b30352-L_[6 : 6]))})))),klLu,(((~(((-82'h7b2640d)-(7'h36!=78'hd13938ab))*(|klLu[10 : 0])))+94'hbaf8e6de)&&Mou[31 : 0])})));
  end

  assign Qhy = uDipo[73 : 101];

  always@(posedge clk)
  begin
    AnfD = ((((Md^((L_[9 : 8]==Ceki[31 : 0])&(^((^(Qhy==L_))<<(({113'h39d9dcbe,31'h74c2db90,4'h6,Mou[18 : 12],6'h13})-17'h10332)))))>>35'h52990e91)&(({L_[((81'h9f1f891b<Qhy[31 : 52])>=(|(Md ? Qhy : 119'h24208d1d))) +: 3]})==uDipo))<12'hd59);
  end

  always@(posedge clk)
  begin
    Hjj = Ceki[12 : 4];
  end

  assign NU_r[122 : 82] = 8'hdc;

  assign rku = ((&(((25'hb165c1^(^L_[(93'h46066fce||115'h95197293) -: 29])) ? (L_[17 : 15]<=(|100'h8cad4416)) : (72'h57503bb5&&AnfD))&(78'h7233220==69'h3938198f)))-(^8'h3b));

  assign LWqJc = AnfD;

  assign prHn = (Mou>>Hjj);

  assign zbx = ((((((((~(123'hacafef3d&108'h9068e7db))|(|(^101'hb1479786)))!=(Mou+(~rku))) ? 121'h32eaca6b : (rku^({68'h785ba884,AnfD,(rku==(63'h4a9de0f>=99'h14bc4038))})))&Hjj)||(((|Hjj)&&(56'hb1a5db3d||prHn))<(80'h666d0973|Hjj)))*(|(42'h6aaed272||116'h12c357f8)))*(prHn*NU_r));

  assign oq_Or = (AnfD^Qhy[7 : 10]);

  assign KkjR = (98'h2bca1043-prHn);

  assign WF[48 : 46] = L_;

  assign t[14 : 6] = (76'hdb6e1b5d^rku);

  always@(posedge clk)
  begin
    _hY = Ceki[26 : 14];
  end

  assign QuMeq = t;

  always@(posedge clk)
  begin
    IofhJ = (^33'h5079637f);
  end

  assign DJc = ((((((IofhJ^(LWqJc[2 : 88]<=97'hddb3bb04))!=(L_&&(72'hf714c605==95'hdfb54d85)))==(((Qhy==KkjR) ? klLu[(66'h60b52e22>=21'h195913) -: 14] : (5'h17>(!(oq_Or*20'hfabc))))^((|((4'hf-IofhJ)-(-rku)))&(AnfD<<WF[78 : 37])))) ? ((!QuMeq)^20'h959dc) : (Md[24 : 9]&&({(~Ceki[31 : 0])})))*zbx)>(((&77'h2e338480)*(53'ha1798d^(|klLu)))!=(t[17 : 5]+(25'h7b7553<(^116'ha616d518)))));

  always@(posedge clk)
  begin
    oYvSM = 20'hcefb2;
  end

  assign Clr[70 : 103] = 60'h4de347e4;

  assign VA[32 : 32] = 24'hf7bb85;

  always@(posedge clk)
  begin
    l = Clr[104 : 111];
  end

  assign _tAsh = KkjR;

  assign bb[114 : 73] = ((108'hc57637bf<<91'hedc550a3)<79'h195ec518);

  assign Pzroi[61 : 87] = (^(&_hY));

  assign qP[75 : 31] = Md[26 : 2];

  assign i = (_hY==_hY);

  assign CqN = (Pzroi>>(Qhy[(^({(i&&NU_r[122 : 82]),t[17 : 5],prHn,117'h1fcbb7e3})) +: 24]<<((oYvSM[25 : 29]^18'h245cc)*((&Mou[8 : 7])*((21'h107f65&&76'hd7aa3bdd)||(!(((3'h0|QuMeq)<<(58'hd49e6195>76'h71afb39c)) ? (|(114'h50cb6ce3<<NU_r)) : (!QuMeq))))))));

  assign Kc = (_tAsh<_tAsh);

  assign bS = i;

  always@(posedge clk)
  begin
    jhCtx[51 : 66] = _tAsh;
  end

  assign Ii = (&({WF[49 : 43]}));

  always@(posedge clk)
  begin
    q = DJc[109 : 112];
  end

  assign W[81 : 99] = (~68'h68a54beb);

  assign CWr = (Pzroi>>(~(WF<uDipo[83 : 105])));

  assign IM = t[11 : 8];

  always@(posedge clk)
  begin
    Vt[62 : 58] = ({CWr,(-(prHn^LWqJc[74 : 80]))});
  end

  always@(posedge clk)
  begin
    lv_ = (L_!=((~Vt[76 : 7])>(CWr^31'h21a6ab0a)));
  end

  always@(posedge clk)
  begin
    dq[107 : 7] = (-(({(Md&(&t[17 : 5])),(!uDipo[(AnfD<(39'he3660e8f-(108'h90b188b6^72'h92fc349c))) -: 10]),_hY})<<(WF[56 : 43]>=98'h6c42cd16)));
  end

  always@(posedge clk)
  begin
    uGQf = dq[(((-(-KkjR))>=(-((^q)*(88'hdca5da34==(77'hc655db5+(AnfD||28'h8cf4ba1))))))^({(NU_r*Qhy[41 : 48]),(113'hba2884df+L_),(((((21'h301ee<33'h4254d7b1)>>({uDipo[84 : 109],zbx,61'hc9562d7f}))|oYvSM) ? (127'h306e3765>>(IofhJ==jhCtx[L_[17 : 11] +: 41])) : ({(68'h47b956a7^(4'h3^23'h193f90))}))&&W),lv_[32 : 32],(!((^(Md[31 : 23]==(&93'hbddaeb48)))>>(((85'hdd83b248||77'h3ca7b881)-(Vt[66 : 48]>>_hY))>=(82'ha8fa605+q))))})) +: 54];
  end

  assign gm = (((~dq[(!(^(84'h842bcc0c ? 6'h1e : (50'h8ff1acd!=11'h474)))) +: 45])^117'hd56e62b9)+uDipo[99 : 107]);

  always@(posedge clk)
  begin
    Gi = ((~oYvSM[qP[(!((^30'h2822d6e8)^(oYvSM[13 : 13]&40'hf2c7b57c))) +: 45] -: 2])<<(((IM[(_tAsh!=109'h4e2c5ff1) +: 9]^(~({W[lv_[39 : 3] +: 11],(-prHn)})))+({(&(-((zbx^(CqN<<Clr))<klLu[6 : 5]))),(|_tAsh),(((&({gm,l,(89'h60cd32e8&48'h52331a80),t,gm}))>=(((l>>3'h3)==zbx)!=(dq[56 : 44]!=99'h929eeb65)))+(DJc>7'h39)),dq})) ? Mou : ((!(^(95'h4ddb24e4<14'h30c)))<=Clr)));
  end

  always@(posedge clk)
  begin
    BvY = 115'h15403ad;
  end

  assign KHNXP = (^prHn);

  assign jHh = (39'h26f3c437||(QuMeq-uDipo[110 : 123]));

  assign AUZ[98 : 99] = 46'hc018a464;

  assign mKtmU = Ii[90 : 91];

  assign Hh = zbx;

  always@(posedge clk)
  begin
    b = (((t|(^64'he2dd3db9))-(bb>=KHNXP))<((Pzroi<=(uGQf<bS))<(&qP)));
  end

  always@(posedge clk)
  begin
    Ygx = AUZ;
  end

  assign x[82 : 82] = dq;

endmodule
