// filename: test/codegen/bug16820_01.v
// Description:  This test creates a codegen error, that is similar but not
// identical to the problem seen in bug16820.  This is not a duplicator.

// this now works if you specify -v2k -enable2005StylePowerOperator
// or
// -sverilog


module bug16820_01(input clock, input [7:0] in1, in2, output reg [7:0] out1);

   always @(posedge clock)
     begin: main
       out1 = (in1**2) >> (in2[1:0]);
     end
endmodule
