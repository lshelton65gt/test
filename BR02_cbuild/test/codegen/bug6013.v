module top(o, i, sel);
   input [15:0] i;
   input [1:0]  sel;
   output       o;
   assign       o = i[sel*3'd4];
endmodule
