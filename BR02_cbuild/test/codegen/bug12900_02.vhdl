-- Test negative offsets within a dynamic stride in various contexts (lhs, rhs,
-- small net (pod), large net (bv), index multiplier of 32, index not
-- multiplier of 32)

package pkg_bug is

  subtype int_small is integer range 10 to 31;
  subtype int_medium1 is integer range 10 to 63;
  subtype int_medium2 is integer range 1 to 2;
  subtype int_large1 is integer range 10 to 255;
  subtype int_large2 is integer range 105 to 255;
  subtype int_large3 is integer range 4 to 8;

end pkg_bug;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
library work;
use work.pkg_bug.all;

entity bug12900_02 is

  port (
    clk        : in std_logic;
    idx1_small   : in int_small;
    idx2_small   : in int_small;
    idx1_medium  : in int_medium1;
    idx2_medium  : in int_medium1;
    idx3_medium : in int_medium2;
    idx4_medium : in int_medium2;
    idx1_large   : in int_large1;
    idx2_large   : in int_large1;
    idx3_large   : in int_large2;
    idx4_large   : in int_large2;
    idx5_large   : in int_large3;
    idx6_large   : in int_large3;
    idx7_large   : in int_large3;
    idx8_large   : in int_large3;
    in_small    : in  std_logic_vector(31 downto 0);
    in1_medium   : in  std_logic_vector(63 downto 0);
    in2_medium   : in  std_logic_vector(63 downto 0);
    in1_large    : in  std_logic_vector(255 downto 0);
    in2_large    : in  std_logic_vector(255 downto 0);
    in3_large    : in  std_logic_vector(255 downto 0);
    in4_large    : in  std_logic_vector(255 downto 0);
    out_small   : out std_logic_vector(31 downto 0);
    out1_medium  : out std_logic_vector(63 downto 0);
    out2_medium  : out std_logic_vector(63 downto 0);
    out1_large   : out std_logic_vector(255 downto 0);
    out2_large   : out std_logic_vector(255 downto 0);
    out3_large   : out std_logic_vector(255 downto 0);
    out4_large   : out std_logic_vector(255 downto 0)
    );

end bug12900_02;


architecture rtl of bug12900_02 is
  
begin

  p1: process
  begin
    wait until clk'event and clk = '1';

    out_small <= ((others => '0'));
    out1_medium <= ((others => '0'));
    out2_medium <= ((others => '0'));
    out1_large <= ((others => '0'));
    out2_large <= ((others => '0'));
    out3_large <= ((others => '0'));
    out4_large <= ((others => '0'));
    
    -- pod tests
    out_small(idx1_small - 3 downto idx1_small - 5) <= in_small(idx2_small - 1 downto idx2_small - 3);
    out1_medium(idx1_medium - 3 downto idx1_medium - 5) <= in1_medium(idx2_medium - 1 downto idx2_medium - 3);
    -- special code for multiplier of 32 in codegen
    out2_medium(idx3_medium * 32 - 3 downto idx3_medium * 32 - 5) <= in2_medium(idx4_medium * 32 - 1 downto idx4_medium * 32 - 3);

    -- bv tests
    -- small slice
    out1_large(idx1_large - 3 downto idx1_large - 5) <= in1_large(idx2_large - 1 downto idx2_large - 3);
    -- large slice
    out2_large(idx3_large - 3 downto idx3_large - 103) <= in2_large(idx4_large - 1 downto idx4_large - 101);
    -- index multiplier of 32, small slice
    out3_large(idx5_large*32 - 3 downto idx5_large*32 - 5) <= in3_large(idx6_large*32 - 1 downto idx6_large*32 - 3);
    -- index multiplier of 32, large slice
    out4_large(idx7_large*32 - 3 downto idx7_large*32 - 103) <= in4_large(idx8_large*32 - 1 downto idx8_large*32 - 101);
  end process p1;

end rtl;
  
