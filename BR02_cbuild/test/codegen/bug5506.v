// not generating aligned assignments
module top(input [127:0] i, output [159:0] o);
   assign o[63:0] = i[63:0];
   assign o[159:96] = i[127:64];
endmodule
