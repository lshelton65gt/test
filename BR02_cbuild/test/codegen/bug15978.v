//note this file is used by tests bug15978_noflat.v and bug15978_flat.v

// filename: bug15978.v
// Description:  This test is a simplification of the test from bug 15978.  The
// feature of this testcase is that the signedness of the first port of module
// 'mid' does not match (formal vs actual).
// When the signedness does not match, and there are multiple instances of the
// same module, then a bug in GCC4 is exposed.

// It was found that with GCC4 that valid c++ code was emitted in this situation
// but that GCC would generate incorrect code.

// With bug 15978 it was discovered that gcc 4 generates incorrect code if 'const' is
// used to typecast the argument of a constructor, when the object being constructed is
// declared as a const ref.  It is not known if other versions of GCC have the same problem.
// This appears to be a problem when the constructor in question is used in a class
// initialization list like this:
//
// c_mid::c_mid(CarbonUInt2 const & a_mid_in):m_mid_in(a_mid_in){}  // ctor for class c_mid
// c_bug15978::c_bug15978():mTop (fixup_hierref_pointers ()),
// 	m_c_mid_1((const CarbonUInt2  & )m_counter){}  // the const on this line is the problem
//
// Not using the const in this situation is acceptable since gcc will still see the
// const in the declaration of the constructor for of the object (c_mid in example)
// and do the conversion anyway. The problem appears to be that gcc creates an
// intermediate but does not create the instructions to copy from the original
// to the intermediate as it should.  When the indicated 'const' is not emitted
// then no intermediate is created and the ref works as expected.
 



module bug15978 (input clk,
                 output reg [1:0] counter_out,
                 output wire [3:0] data_out_0,
                 output wire [3:0] data_out_1,
                 output wire [3:0] data_out_2);

   reg signed [1:0]   counter; 

   initial
     counter = 0;

   always@(posedge clk ) begin
      counter = counter + 1;
      counter_out = counter;
   end
   
   mid i_shift_0(counter, data_out_0);
   mid i_shift_1(counter, data_out_1);
   mid i_shift_2(counter, data_out_2);



endmodule

module mid (
    input [1:0] mid_in,
    output reg [3:0] mid_out);

   always @(mid_in) begin
      mid_out = mid_in;
   end
//   always @(mid_out or mid_in)
//      $display("%m,  mid_in=%x, mid_out=%x", mid_in, mid_out);
endmodule
