// test some crashs from op=
module top(i,c,t,o);
   input [31:0] i;
   input [5:0]  c;
   input        t;
   
   output [2:0] o;
   
   reg [2:0]  errors;
   assign     o = errors;
   integer    n;
   always @(posedge t)
     begin:blk
        errors = 0;
        n = i;
        n = n/c;

        if (c==0 && n!= i)
          errors[0] = 1'b1;

        n = n << c;
        if (c==32 && n != 0)
          errors[1] = 1'b1;

        n = n % c;
        if (c==0 && n != i)
          errors[2] = 1'b1;

        errors = errors + 3'b010; // want (errors += 2)&=7
     end
endmodule
   
