// last mod: Wed May 23 17:17:07 2007
// filename: test/codegen/bug7343_01.v
// Description:  This test duplicates the problem seen in bug 7343.
// the issue here is in the code that we generate where internally 8 bits are
// used to store dqs_out, when we assign to dqs_out in the line:
// 	  dqs_out = rdqs_cntr - RDQS_PST; // assign to whole variable
// we only accurately calculate the LSB, the other bits are incorrect
// later when we use dqs_out we do a test on all the bits.

module bug7343_01(clock,dqs_out, dqs_delay_out);
   input clock;
   output dqs_out, dqs_delay_out;
   reg dqs_out, dqs_delay_out;
   integer rdqs_cntr;
   parameter RDQS_PST = 1;

   initial
      begin
	 rdqs_cntr = 7;
      end
   
   always @(posedge clock)
     begin: main
	dqs_out = 0;
	if (rdqs_cntr > RDQS_PST )
	  dqs_out = rdqs_cntr - RDQS_PST; // assign to whole variable

	dqs_delay_out = 0;
	if ( dqs_out )		// test whole variable
	  dqs_delay_out = 1;
	rdqs_cntr = rdqs_cntr - 1;
	if (rdqs_cntr < 0 )
	  rdqs_cntr = 7;
     end
endmodule
