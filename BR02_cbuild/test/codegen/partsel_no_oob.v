// cleaned up corner case test for indexed part selects
// focus on:
//	- read & write
// 	- size of index expression (1,8,33,65
//	- size of vector (8, 64, 90)
//
// this testcase passes cleanly with Aldec.

module probe(c,i,o);
   parameter vwidth=1;          // vector width to access
   parameter iwidth=1;         // index expression width
   input     c;
   input [31:0] i;        // Initial value
   output             o;        // computed result

   reg [vwidth-1: 0] vector = 0;

   reg signed [iwidth-1:0]  index;
   always @(posedge c)
     begin : varsel
        // init all bits of vector from source
        integer j;
        for(j = vwidth; j >= 32; j = j - 32)
          vector = (vector << 32) | i;

        // write to wide variety of pieces.
        // incrementing by 1 is just TOO slow for CBUILD to compile
        for (j = 0; j < (vwidth-8); j = j+3)
          begin : v2
             integer k;
             index = j;
             vector[(index) +:7] = ~vector[(index) +: 7];

             k = k ^ (^index);
          end
     end

   assign o = ^vector;
endmodule // probe

module mid(c,i,o);
   parameter vwidth=1;
   input     c;
   input [31:0] i;
   output [7:0] o;

   probe #(vwidth,8) x1__1 (c,i,o[0]);
   probe #(vwidth,9) x1__7 (c,i,o[1]);
   probe #(vwidth,10) x1__8 (c,i,o[2]);
   probe #(vwidth,31) x1_31 (c,i,o[3]);
   probe #(vwidth,32) x1_32 (c,i,o[4]);
   probe #(vwidth,49) x1_49 (c,i,o[5]);
   probe #(vwidth,64) x1_64 (c,i,o[6]);
   probe #(vwidth,65) x1_65 (c,i,o[7]);
endmodule
   

module top(c,i,o);
   input c;
   input [31:0] i;
   output [63:0] o;

   mid #( 1)xA (c,i,o[ 7: 0]);
   mid #( 7)xB (c,i,o[15: 8]);
   mid #( 8)xC (c,i,o[23:16]);
   mid #(31)xD (c,i,o[31:24]);
   mid #(32)xE (c,i,o[39:32]);
   mid #(49)xF (c,i,o[47:40]);
   mid #(64)xG (c,i,o[55:48]);
   mid #(65)xH (c,i,o[63:56]);

endmodule
