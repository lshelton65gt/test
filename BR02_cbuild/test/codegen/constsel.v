// test selections of constants...
module sub(v,p,o);
   parameter vsize=1;
   parameter selsize=1;

   input [vsize-1:0] v;
   input [6:0]      p;
   output                 o;

   assign                  o = ^v[p+:selsize];
endmodule

module top(p,x);
   input [6:0] p;
   output [16:0] x;
   reg           v1 = 1;
   reg [6:0]     v7 = 7'h5c;
   reg [13:0]    v14 = 14'h1234;
   reg [32:0]    v33 = 33'h12345678a;
   reg [65:0]    v66 = 66'hfedcba987654321F;

   sub xv1a (v1, p, x[0]);
   sub xv1b (v1, 7'b0, x[16]);
            
   sub #(7) xv7a(v7, p, x[1]);
   sub #(7) xv7b(v7, 7'd3, x[2]);
   sub #(7,2)xv7c(v7, 7'd3, x[3]);

   sub #(14)xv14a(v14, p, x[4]);
   sub #(14,8)xv14b(v14, 7'd8, x[5]);
   sub #(14,2)xv14c(v14, 7'd9, x[6]);

   sub #(33) xv33a (v33, p, x[7]);
   sub  #(33,23) xv33b(v33, 7'd0, x[8]);
   sub  #(33,32) xv33c(v33, 7'd15, x[9]);
   sub  #(33,1) xv33d(v33, 7'd32, x[10]);

   sub  #(66)xv66a(v66, p, x[11]);
   sub  #(66,65)xv66b(v66, 7'd8, x[12]);
   sub  #(66,3)xv66c(v66, 7'd63, x[13]);
   sub  #(66,17)xv66d(v66, p, x[14]);
   sub  #(66,32)xv66e(v66, 7'd64, x[15]);

endmodule
