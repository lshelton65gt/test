library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity bug6012 is
  port (
    in1 : in std_logic_vector(31 downto 0);
    in2 : in std_logic_vector(31 downto 0);
    in3 : in std_logic_vector(9 downto 0);
    Sout_i : out std_logic_vector(31 downto 0)
    );
end;

architecture arch of bug6012 is
begin

  p1: process (in1, in2, in3)
    variable S1 : signed(31 downto 0);
    variable U2 : unsigned(31 downto 0);
    variable S3 : signed(9 downto 0);
  begin
	S1 := signed(in1);
	U2 := unsigned(in2);
	S3 := signed(in3);
	
        Sout_i <= EXT((  S1 * (U2  - S3)), 32);

  end process;
end arch;
