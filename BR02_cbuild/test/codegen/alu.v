module mul(a,b,r);
   input[79:0] a;
   input [79:0] b;
   output [79:0] r;
   
   assign r = a * b;
endmodule

module div(a,b,r);
   input[79:0] a;
   input [79:0] b;
   output [79:0] r;
   
   assign        r = a / b;
endmodule

module mod(a,b,r);
   input[79:0] a;
   input [79:0] b;
   output [79:0] r;
   
   assign        r = a % b;
endmodule

module top(x,y,o);
   input [31:0] x;
   input [31:0] y;
   output [5:0] o;

   wire [79:0]  acc1;
   mul mgood({16'b0,x,y}, 80'b100000000, acc1);

   assign       o[0] = ^acc1;

   wire [79:0]  acc2;
   div dgood({y,48'b0}, 80'd1024, acc2);
   assign       o[1] = ^acc2;

   wire [79:0]  acc3;
   mod modgood({48'b1, x}, 80'h8000000, acc3);
   assign       o[2] = ^acc3;

   // Now some bad ones...
`ifdef DOBAD
   wire [79:0]  acc4;
   mul mbad({48'b0, x}, {48'b0, y}, acc4);
   assign     o[3] = ^acc4;


   wire [79:0] acc5;
   div divbad({48'b0, x}, {48'b0,y}, acc5);
   assign      o[4] = ^acc5;

   wire [79:0] acc6;
   mod modbad({48'b0,x}, {y,48'b1}, acc6);
   assign      o[5] = ^acc6;
`else // !`ifdef DOBAD
  // get rid of the undriven warning... it causes differences between the "cbuild" output and
  // the "vspcompiler" output.
  assign 	o [5:3] = 3'b000;
`endif

endmodule


           
