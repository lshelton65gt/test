// Test partially OOB bitvector writes using a variety of operators

`define operate(name, OP) \
    module name(x, i, opc, opv);	\
      input [5:0] x; \
      input [64:0] i; \
      output [64:0] opc, opv; \
      reg [64:0]    rpc,rpv; \
      assign opc = rpc; \
      assign opv = rpv; \
      always @(i or x) \
        begin: blk \
           integer index;  \
           index = x;    \
           index = index - 32'd32; \
           rpc = i;	\
           rpv = i;	\
           rpc[index +: 32] = rpc[index +: 32] OP 17; \
           rpv[index +: 32] = rpv[index +: 32] OP i[(x&7)+:32]; \
           $display("index=%d",index); \
        end \
    endmodule

`operate(plus,+ )
`operate(minus, - )
`operate(bitand, & )
`operate(bitxor, ^ )
`operate(bitor, | )
`operate(times, * )

module top(x,a,c,d,e,f,g,h,i,j, k,l, m,n);
   input [5:0] x;               // 6 bits (0..63)
   input [64:0] a;
   output [64:0] c,d,e,f,g,h,i,j,k,l,m,n;

   plus  m1(x, a, c, d);
   minus m2(x, a, e, f);
   bitand m3(x, a, g, h);
   bitxor m4(x, a, i, j);
   bitor m5(x, a, k,l);
   times m6(x, a, m,n);
   
endmodule

