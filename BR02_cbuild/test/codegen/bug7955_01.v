// Bug7955 was induced when the pattern of coercions emitted for condition tickled
// a C++ error. Note that cbuild must be invoked with "-g" or else cbuild optimised
// away the problem.
//
//
// if (time < tWPSTmin * clkt) was emitted as:
//
// ((((CarbonReal(((CarbonReal)(CarbonUInt64(m_rtime)))))<CarbonReal((((CarbonUInt64)(((((CarbonReal)(SInt32 (m_$localcse_clkt_1_1_)))))*((1.00000000000001997e-03)))))))))
//
// If this is instead emitted as:
//
// ((((static_cast <CarbonReal>(((CarbonReal)(CarbonUInt64(m_rtime)))))<CarbonReal((((CarbonUInt64)(((((CarbonReal)(SInt32 (m_$localcse_clkt_1_1_)))))*((1.00000000000001997e-03)))))))))
//
// then the generated model is compiled correctly.

module top (clk, out);
  input clk;
  output out;
  time rtime;
  integer clkt;
  reg 	  out_reg;
  parameter tWPSTmin = 0.00100000000000002;
  assign out = out_reg;
  initial 
    begin
      out_reg = 0;
      clkt = 0;
    end
  always @(posedge clk)
    begin
      rtime = $realtime*1000;         
      if (rtime < tWPSTmin * clkt) // <-- this line induced the error
	out_reg = 1;
      else
	out_reg = 0;
      clkt = clkt + 1;
    end
endmodule
