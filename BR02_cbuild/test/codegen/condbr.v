module top(clk,in,out);
   input clk;
   input [1:0] in;
   output [1:0] out;

   wire 	write;

   wrap wrap(clk,in,write,out);
endmodule

module wrap(clk,in,write,out);
   input clk;
   input [1:0] in;
   input       write;
   output [1:0] out;

`ifdef MANUAL_COERCION
`else
   assign 	port0.write = write;
   assign 	port1.write = write;
`endif

   port port0(clk,in[0],write,out[0]);
   port port1(clk,in[1],write,out[1]);
endmodule

module port(clk,in,write,out);
   input clk;
   input in;
`ifdef MANUAL_COERCION
   inout write;
`else
   input write;
`endif
   output out;
   reg 	  out;

   initial out = 0;
   always @(posedge clk) begin
      if (write) begin
	 out <= in;
      end
   end
   
endmodule
