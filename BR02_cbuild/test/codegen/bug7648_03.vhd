-- Run with cbuild -q -vhdlTop bug7648_03 -memoryBVLimit 0 bug7648_03.vhd
--
-- This is similar to bug7648_01.vhd, except the loop has been unrolled by
-- hand. This was not identified as a candidate for the &= transformation
-- because the left operand was redordered into something different:
--
--   $reorder_x;1[3][3] = ((!in1[2]) & x[3][3]); // bug7648_03.vhd:36

library ieee;
use ieee.std_logic_1164.all;

package package_7648_03 is
  type ich_hard_dec_array_t    is array(0 to 3) of std_logic_vector(3 downto 0);
  type ich_outputs_array_t     is array(0 to 4) of ich_hard_dec_array_t;
end package_7648_03;

library ieee;
use ieee.std_logic_1164.all;
use work.package_7648_03.all;

entity bug7648_03 is
  port (
    clock    : in  std_logic;
    in1      : in  std_logic_vector(3 downto 0);
    out1     : out std_logic_vector(3 downto 0));
end;

architecture arch of bug7648_03 is
  signal x              : ich_outputs_array_t := (others => (others => "1011"));
begin
  process (clock, in1, x)
  begin
    if clock = '1' then
      if (in1 /= "0") then
        x(0)(0)(3) <= x(0)(0)(3) and not(in1(2));
        x(1)(0)(3) <= x(1)(0)(3) and not(in1(2));
        x(2)(0)(3) <= x(2)(0)(3) and not(in1(2));
        x(3)(0)(3) <= x(3)(0)(3) and not(in1(2));
        x(4)(0)(3) <= x(4)(0)(3) and not(in1(2));
      end if;
      out1 <= x(2)(0);
    end if;
  end process;
end;
