module test(xor64, in1, in2);
  output [63:0] xor64;
  input [63:0]  in1;
  input [127:0] in2;

  assign        xor64 = in2[125:24] ^ in1 ^ in2[127:66];
endmodule
