// bug2394 - overlapped partselects...
// This test crashes Aldec,  and APPEARS to have simulation failures running NC.
// There are 4 input values that mismatch between carbon and NC and I've examined the
// data differences in all 4.  In each case, NC writes a data value into OP that's
//  OFF by 15 bit positions from what it should be,
//
module top(clk,ip,index, errors, op,xp);
   input clk;
   input [127:0] ip;
   input [6:0]   index;
   output [127:0] op;
   inout [127:0]  xp;
   output [3:0]   errors;       // Should always be 4'b1111 or there was an error
   // try some random copies...

   wire [6:0]    i = ip[5:0];


   reg [127:0]    v;
   initial v=0;
   assign         op = v;

   reg [127:0]    xptemp;
   initial xptemp = 0;
   assign         xp = xptemp;
   reg [3:0]     err=4'b1111;
   assign         errors = err;
   
   always @(posedge clk)
     begin:posclk
        reg [65:0] temp66;
        reg [71:0] temp72;
        v=ip;
        temp72 = ip[(i-15)+:72];
        v[i+:72] = v[(i-15)+:72]; // bad overlap
        err[0] = (temp72 == v[i+:72]); // Did we have copy problems?
        
        xptemp = xp;
        temp66 = xp[index +:66];
        xptemp[(index-16) +: 66] = xptemp[index +: 66]; // good overlap
        err[1] = (temp66 == v[(index-16) +: 66]);
     end

   always @(negedge clk)
     begin: negclk
        reg [65:0] temp66;
        reg [71:0] temp72;
        v = ip;
        temp72 = ip[(i+15) +: 72];
        v[i+:72] = v[(i+15) +: 72]; // good overlap
        err[2] = (temp72 == v[i+:72]);
        
        xptemp = xp;
        temp66 = xp[index +:16];
        xptemp[(index+16) +: 66] = xptemp[index +: 66]; // bad overlap
        err[3] = (temp66 == xptemp[(index+16) +: 66]);
     end

endmodule


   
