package pkg_bug is
  subtype		cproc_reg_t			is integer range 0 to 15;
  
  constant N_LOCAL_REGS : integer := 7;
  constant N_PARAM1     : integer := 4;
  
end pkg_bug;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
library work;
use work.pkg_bug.all;


entity bug12900_01 is

  port (
    const1 : in  cproc_reg_t;
    in1    : in  std_logic_vector(N_LOCAL_REGS * 32 - 1 downto 0);
    out1   : out std_logic_vector(31 downto 0));

end bug12900_01;


architecture rtl of bug12900_01 is
  function foo (reg_i   : cproc_reg_t;
                data_in : std_logic_vector(N_LOCAL_REGS*32-1 downto 0))
  return std_logic_vector is

    variable temp : std_logic_vector(31 downto 0);
  begin
    if reg_i = 0 then
      temp := (others => '0');
    elsif  reg_i <= N_LOCAL_REGS then
      if reg_i > N_PARAM1 then
        temp := "10101010101010101010101010101010";
      else
         for i in 1 to N_LOCAL_REGS loop
           if i = reg_i then
--             temp := data_in(i * 32 - 1 downto (i - 1) * 32);
             temp := data_in(i * 32 - 9 downto (i - 1) * 32 + 0);
           end if;
         end loop;
      end if;
    else
      temp := (others => '1');
    end if;
    return temp;
  end;
  
begin

  p1: process (const1,  in1)
  begin
    out1 <= foo(const1, in1);
  end process p1;

end rtl;
  
