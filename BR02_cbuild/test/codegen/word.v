module word(i,o);
   input [31:0] i;
   output [31:0] o;
   swapper hw(o[15:0], i[31:16]);
              
   assign 	 o[31:16] = i[15:0];
endmodule // word

module swapper(o,i);
   output [15:0] o;
   input [15:0]  i;

   assign        o[7:0] = i[15:8];
   assign        o[15:8] = i[7:0];
endmodule

              
