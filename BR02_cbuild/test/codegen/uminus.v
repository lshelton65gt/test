// test unary minus with BV and partsels
module top(a,o);
   input [31:0] a;
   output [7:0] o;

   wire [95:0]   v;
   assign v = {a,~a,-a};

   assign        o[0] = -v[65:0] == (0-v[65:0]);
   assign        o[1] = -v == (0-v);
   assign        o[2] = ~v[65:1] == (v[65:1] ^ 65'h1ffffffffffffffff);
   assign        o[3] = ~v[63:32] == a;
   assign        o[4] = -v[7:0] == (a & 255);
   assign        o[5] = +v[64+:8] == a&255;
   assign        o[6] = -v[64:0] == -{a[0],~a,-a};
   assign        o[7] = ~v[31+:65] == ~a;
endmodule
