// This test case exercises a codegen bug than manifested itself as incorrect
// generated C++ code for an assignment of the form:
//
// address [n] = address [n] + a
//
// such that:
// - address is a one-word wide memory, so that address [n] is only valid, and address[n] 
//   denotes the entire memory
// - "address [n]" was not optimised into just "address" before codegen
// - address is converted into a bitvector by BVMem conversion
// Codegen would map this into address[n] += a, however address[n] is realised as an instance
// of WGET_SLICE_CHECK which cannot be used as an lvalue

module top (clk, a, b, n, x);
  input clk;
  input [7:0] a, b;
  input       n;
  output [7:0] x;
  
  reg [7:0] address [0:0];
  reg [7:0] tmp;
  initial
    begin
      address [0] = 7'b0;
      tmp = 7'b0;
    end
  always @(posedge clk)
    begin
      // Note that unless n is zero, this is indeed an invalid assignment. However, to exercise
      // the bug the assignment needs to be left this way, with n as an input port. At runtime
      // carbon_wset_slice_check does ensure that we do not perform any illegal writes.
      address [n] = address [n] + a; // this line exercises the bug
      tmp = address [n];
      if (n != 0)
	// ensure that we get a deterministic, non-X value in the output for simulation comparison.
	tmp = 8'hff;
    end
  assign x = tmp;
endmodule // top
