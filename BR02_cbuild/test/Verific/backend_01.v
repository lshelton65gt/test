// filename: test/Verific/backend_01.v
// Description:  This test fails with a backend compile error 1410, or precompiled header problem
// the issue appears to be the use of w3 in the $display argument list


module backend_01(in1, in2, in3, out1) ;
   input in1, in2, in3;
   output signed [3:0] out1;
   
   wire signed [0:0] S1;
   wire signed [0:0] S2;
   wire signed [0:0] S3;
   wire signed [3:0] w1;
   wire signed [3:0] w2;
   wire signed [3:0] w3;

   assign S1 = in1;
   assign S2 = in2;
   assign S3 = in3;

   assign out1 = $signed(S1 /  S2) > $signed(S3); // (signed `OP signed) `OP signed
   assign w1 = (S1 /  S2);
   assign w2 = S3;
   assign w3 = w1 > w2;
   always @ (out1 or w1 or w2 or w3 )
   //fails   $display("out1: 'b%b,%d    w1: 'b%b,%d, w2: 'b%b,%d,  w3: 'b%b,%d", out1,out1, w1, w1, w2, w2, w3, w3);
   //fails   $display("out1: 'b%b,%d    w1: 'b%b,%d, w2: 'b%b,%d,  w3: 'b%b,%d,  ", out1,out1, w1, w1, w2, w2, w3, w2);
   $display("out1: 'b%b,%d    w1: 'b%b,%d, w3: 'b%b,%d,   ", out1,out1, w1, w1, w3, w3);
endmodule
