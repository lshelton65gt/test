
module test(a, b, c, d, e, f);

input [8:1] a, b;
output [4:1] c, d, e, f;

reg [3:0] mem [4:1];
reg [7:0] C [3:2][2:1][5:4] ;

assign c[4] = a[0] & b[3];
assign d[4:1] = a[3:0] & b[3:0];
assign e = mem[0];
always @(a or b)
begin
    C[3][0][5] = 8'b00001111 ;
end

endmodule
