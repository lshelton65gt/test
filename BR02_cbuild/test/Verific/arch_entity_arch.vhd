-- filename: test/Verific/VHDL/arch_entity_arch.vhd
-- Description:  This test puts the entity and architecture definitions in
-- separate files.  This was once not handled by Verific flow.
-- see also arch_entity_entity.vhd

library ieee;
use ieee.std_logic_1164.all;

architecture arch of arch_entity_arch is
begin
  main: process (clock)
  begin
    if clock'event and clock = '1' then
      out1 <= in1 and in2;
    end if;
  end process;
end;
