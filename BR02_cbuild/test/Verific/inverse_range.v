module foo(input clock, input [7:0] in1, in2, output reg [7:0] out1) ;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2[0:7];  // note here the 0:7 is wrong order
     end

endmodule
