-- filename: test/Verific/VHDL/arch_entity_entity.vhd
-- Description:  This test puts the entity and architecture definitions in
-- separate files.  This was once not handled by Verific flow.
-- see also arch_entity_arch.vhd

library ieee;
use ieee.std_logic_1164.all;

entity arch_entity_arch is
  generic (
    WIDTH : integer := 4;
    DEPTH : integer := 4);

  port (
    clock    : in  std_logic;
    in1, in2 : in  std_logic_vector((WIDTH-1) downto 0);
    out1     : out std_logic_vector((WIDTH-1) downto 0));
end;
