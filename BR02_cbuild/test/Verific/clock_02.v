// filename: clock_02.v
// Description:  This test duplicates the problem seen in bug3522:
// a CARBON INTERNAL ERROR with verific  flow.

// if you define BUG then you get the error:
// with BUG defined and -useVerific this gets a CARBON INTERNAL ERROR
// to see problem run with:
// cbuild -q clock_02.v -v2k -useVerific +define+BUG
// this does not have CARBON INTERNAL ERROR
// cbuild -q clock_02.v -v2k -useVerific
`define BUG


module clock_02(clock, in1, in2, rst_n, out1, out2) ;
   input clock;
   input [7:0] in1, in2;
   input       rst_n;
   output [7:0] out1,out2;
   reg [7:0] out1r;

   assign out1 = out1r;
`ifdef BUG   
    always @(posedge 1'b0)
      begin: main
        out1r = in1 & in2;
      end
`endif   

   sub u1(clock, in1, in2, rst_n, out1, out2) ;

endmodule

module sub(clock, in1, in2, rst_n, out1, out2) ;
   input clock;
   input [7:0] in1, in2;
   input       rst_n;
   output [7:0] out1,out2;
   reg [7:0] out1,out2;

   
   always @(posedge clock or negedge rst_n)
     begin: main2
	if (rst_n === 1'b0 )
	  out2 = 0;
	else 
	  out2 = in1 & in2;
     end

`ifndef BUG   
    always @(posedge 1'b0)
      begin: main
        out1 = in1 & in2;
      end
`endif   

endmodule
