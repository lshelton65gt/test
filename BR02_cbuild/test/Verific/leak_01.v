// filename: leak_01.v
// demonstrates a memory leak in verific code, caused by the use of the 'delay2'
// value: "#1".  See SystemVerilog 2012 LRM, section 29.8
//
// It appeares that the StaticElaboration is handling this 'delay2' "#1" as a parameter.
//
// leak traceback
//       12 100%       1 operator new(unsigned int)
//                      Verific::VeriInteger::ToConstVal(Verific::VeriTreeNode const*) const
//                      Verific::VeriInteger::ToExpression(Verific::VeriTreeNode const*) const
//                      Verific::VeriExpression::StaticEvaluateToExprInternal(int, Verific::ValueTable*, Verific::VeriConstraint*, Verific::VeriIdDef const*, unsigned int) const
//                      Verific::VeriModuleInstantiation::StaticReplaceConstantExpr(unsigned int)
//                      Verific::VeriModule::StaticReplaceConstantExpr(unsigned int)
//                      Verific::VeriPseudoTreeNode::ReplaceConstantExpr(Verific::Set*) const
//                      Verific::VeriStaticElaborator::ElaborateInternal(unsigned int)
//                      Verific::VeriStaticElaborator::ElaborateVerilogTopLevelModules()
//                      Verific::VeriModule::StaticElaborateInternal(Verific::Map*, Verific::Array*, unsigned int, unsigned int*)


module leak_01(clock, in1, o1) ;
   input clock;
   input  in1;
   output o1;

   PRIM01 #1  u1(o1, 1'b0, 1'b0, clock, in1);

endmodule



primitive PRIM01(q, set, rst, clk, data);
output				q;
input				set;
input				rst;
input				clk;
input				data;
reg				q;
table
0    0    ?    *    : ?: -;
1    0    ?    ?    : ?: 1;
0    1    ?    ?    : ?: 0;
x    0    ?    ?    : 1: 1;
endtable
endprimitive
  
