module top (out, clk, d);
   output out;
   input  clk, d;
   reg    out;
   wire   dinv;

   reg    r1;
   initial begin
      r1 = 0;
      out = 0;
   end
   
   always @ (posedge clk)
     r1 <= d;

   assign dinv = ~r1;

   always @ (posedge clk)
     out <= dinv;

endmodule

