module top(clk,addr,data,out);
   input clk;
   input addr;
   input data;
   output [1:0] out;

   reg [1:0] 	out;
   reg [1:0] 	v;
`ifdef CARBON
`else
   initial v = 2'b00;
`endif
   always @(data or addr) begin
      v[addr] = data;
   end
   initial out = 2'b00;
   reg dclk;
   initial dclk = 0;
   always @(posedge clk) begin
      dclk = ~dclk;
   end
   always @(posedge dclk) begin
      out = ~v;
   end
endmodule
