// Simple multi instance merging by depth

module top (out1, out2, clk1, clk2, in1, in2, in3, in4);
   output out1, out2;
   input  clk1, clk2, in1, in2, in3, in4;

   sub S1 (out1, clk1, in1, in2, in3, in4);
   sub S2 (out2, clk2, in1, in2, in3, in4);

endmodule // top


module sub (out, clk, a, b, c, d);
   output out;
   input  clk, a, b, c, d;

   reg 	r1, r2, r3, r4;
   initial begin
      r1 = 0;
      r2 = 1;
      r3 = 0;
      r4 = 1;
   end
   always @ (posedge clk)
     begin
	r1 <= a;
	r2 <= ~b;
     end
   always @ (posedge clk)
     begin
	r3 <= c;
	r4 <= ~d;
     end

   reg 	t1, t2, t3, t4;
   initial begin
      t1 = 1;
      t2 = 0;
      t3 = 1;
      t4 = 0;
   end
   always @ (r1)
     begin
	t1 = ~r1;
	t3 = ~r3;
     end

   always @ (r2)
     begin
	t2 = ~r2;
	t4 = ~r4;
     end

   reg 	out;
   initial out = 0;
   always @ (posedge clk)
     out <= (t1 & t2) ^ (t3 & t4);

endmodule
