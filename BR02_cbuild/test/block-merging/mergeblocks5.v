module top (out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   // flop all the inputs
   reg 	  r1, r2;
   initial begin
      r1 = 0;
      r2 = 0;
   end
   always @ (posedge clk)
     begin
	r1 <= in1;
	r2 <= in2;
     end

   reg 	  t1, t2;
   initial begin
      t1 = 1;
      t2 = 0;
   end
   always @ (r1)
     begin
	t1 = ~r1;
	t2 = ~t1;
     end

   reg t0;
   initial t0 = 1;
   always @ (r2)
     t0 = ~r2;

   reg t3, t4;
   initial begin
      t3 = 1;
      t4 = 1;
   end
   always @ (t2 or r2 or t0)
     begin
	t3 = ~t2 & t0;
	t4 = ~r2;
     end

   // Create a false cycle
   reg t5, t6, t7;
   initial begin
      t5 = 1;
      t6 = 0;
      t7 = 1;
   end
   always @ (t3 or t4 or t7)
     begin
	t5 = t3 & t7;
	t6 = ~t4;
     end
   always @ (t6)
     t7 = ~t6;

   // Create more nodes that can be merged
   reg t8, t9, t10;
   initial begin
      t8 = 0;
      t9 = 0;
      t10 = 0;
   end
   always @ (t5)
     t8 = ~t5;
   always @ (t6 or t8 or t7)
     begin
	t9 = t8 & t6;
	t10 = ~t9 & ~t7;
     end
   
   reg out;
   initial out = 0;
   always @ (posedge clk)
     out <= t10;

endmodule
