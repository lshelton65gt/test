module top (in,clk,out);
   input in,clk;
   output out;
   reg t1;

   initial t1 = 1'b1;

   assign out = ~t1;

   always @ (posedge clk)
     begin
       t1 <= in;
     end

endmodule // top
