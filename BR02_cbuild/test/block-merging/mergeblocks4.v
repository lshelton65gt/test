module top (out1, out2, out3, out4, clk, in1, in2);
   output out1, out2, out3, out4;
   input  clk, in1, in2;

   reg 	  t1, t2;
   reg 	  t3, t4;
   reg 	  t5;
   wire   t6;
   reg 	  t7, t8;
   reg 	  r1, r2, r3, r4, r5, out1, out2, out3, out4;

   initial begin
      t1 = 0;
      t2 = 0;
      t3 = 0;
      t4 = 0;
      t7 = 0;
      t8 = 0;
      r1 = 0;
      r2 = 0;
      r3 = 0;
      r4 = 0;
      r5 = 0;
      out1 = 0;
      out2 = 0;
      out3 = 0;
      out4 = 0;
   end
   always @ (in1)
     begin
	t1 = ~in1;
	t2 = ~t1;
     end

   always @ (t2 or in2)
     begin
	t3 = ~t2;
	t4 = ~in2;
     end

   sub S1 (.out(t6),.in(~t2 & t4));

   always @ (t2 or t3 or t4 or r5)
     begin
	t5 = t2 & t3;
	t7 = ~r5;
	case ({t2, t3})
	  2'b10: t8 = t4;
	  2'b01: t8 = t5;
	  default: t8 = 0;
	endcase
     end

   always @ (posedge clk)
     begin
	r1 <= t3;
	r2 <= t5;
	r3 <= t6;
	r4 <= t4;
	out1 <= r1;
	out2 <= r2;
	out3 <= r3;
	out4 <= r4;
	r5 <= t1;
     end // always @ (posedge clk)

endmodule

module sub (out, in, in2);
   output out;
   input  in, in2;

   reg 	  out;
   reg 	  out2;
   always @ (in)
     begin
	out = ~in;
	out2 = ~in2;
     end

endmodule // sub
