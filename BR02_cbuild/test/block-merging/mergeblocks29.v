module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   reg 	  r1, r2;
   always @ (posedge clk)
     begin
	r1 <= in1;
	r2 <= in2;
     end

   wire   c1, c2;
   sub S1 (out1, c2, c1, r2);
   sub S2 (c1, out2, r1, c2);

endmodule // top

module sub (x, y, a, b);
   output x, y;
   input  a, b;

   assign x = ~a;
   assign y = ~b;

endmodule // sub

   
