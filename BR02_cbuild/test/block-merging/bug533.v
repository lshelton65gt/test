module weigh_mod ( clk, re_, cs_, we_, cin, addr, raw_match_s, cout, weigh_match, weigh_out_ff_ );
   input clk;
   input re_, cs_, we_, cin;
   input [8:0] addr;
   input [511:0] raw_match_s;
   output 	 cout;
   output 	 weigh_match;
   output 	 weigh_out_ff_;
   
   reg [511:0]  weigh_array;

   wire weigh_rd = ((re_ === 1'b0) & (cs_ === 1'b0)) ;
   wire weigh_wr = ((we_ === 1'b0) & (cs_ === 1'b0)) ;
   wire weigh_out_int_ = ((|(weigh_array & raw_match_s)) === 1'b0) ;
   wire [511:0] weigh_match_int = ({512{weigh_out_int_}} | weigh_array) & raw_match_s ;

   reg 		weigh_match;
   reg 		weigh_out_ff_;
   reg 		cout_tmp;

   initial 
     begin
	weigh_array = 0;
	weigh_match = 1'b0;
	weigh_out_ff_ = 1'b0;
	cout_tmp = 1'b0;
     end
   
   
   always @(posedge clk)
     begin : blk_clk2
	weigh_match <= weigh_match_int ;
	weigh_out_ff_ <= weigh_out_int_ ;
	cout_tmp <= weigh_rd ? (weigh_wr ? 1'bx : weigh_array[addr]) :
		    cout_tmp ;
	if(weigh_wr) weigh_array[addr] <= cin ;
     end
   assign cout = cout_tmp;
endmodule // weigh_mod
