// Simple multi instance merging by depth

module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   sub S1 (out1, clk, in1, in2);
   sub S2 (out2, clk, in2, in1);

endmodule // top


module sub (out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   reg 	r1, r2;
   initial begin
      r1 = 0;
      r2 = 0;
   end
   always @ (posedge clk)
     begin
	r1 <= in1;
	r2 <= ~in2;
     end

   reg 	t1, t2;
   initial begin
      t1 = 1;
      t2 = 1;
   end
   always @ (r1)
     t1 = ~r1;

   always @ (r2)
     t2 = ~r2;

   reg 	out;
   initial out = 1;
   always @ (posedge clk)
     out <= t1 & t2;

endmodule
