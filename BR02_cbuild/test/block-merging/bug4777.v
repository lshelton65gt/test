module top(clk, rst, o, a, b, c);
   input a, b, c;
   input clk, rst;
   output [3:0] o;

   reg          reg1, reg2;
   
   assign       o[3] = ~reg1;
   assign       o[2] = ~reg2;
   assign       o[1] = reg1;
   assign       o[0] = reg2;
   
   always @(posedge clk or negedge rst)
     begin
        if (~rst)
          begin
             reg2 = 0;
          end
        else
          begin
             reg1 = a & b;
             reg2 = a & c;
          end
     end
endmodule
