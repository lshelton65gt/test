module top (out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   // Create combos with no false cycles
   reg 	  n1, n2;
   initial begin
      n1 = 0;
      n2 = 0;
   end
   always @ (in1)
     n1 = ~in1;
   always @ (in2)
     n2 = ~in2;
   
   // Create a false cycle block that gets partially scheduled
   reg 	  c0, c1, c2, c3, c4;
   initial begin
      c0 = 0;
      c1 = 0;
      c2 = 0;
      c3 = 0;
      c4 = 0;
   end
   always @ (n1 or n2 or c3)
    begin
       c0 = n2;
       c1 = n1 & c0;
       c2 = c3;
    end

   // An intermiedate combo with no false cycles
   reg n3;
   initial n3 = 0;
   always @ (c2)
     n3 = ~c2;

   // Another false cycle block
   always @ (c1 or n3)
     begin
	c3 = ~c1;
	c4 = ~n3;
     end

   // Create more combos with no false cycles
   reg n4, n5;
   initial begin
      n4 = 0;
      n5 = 0;
   end
   always @ (c0)
     n5 = ~c0;
   always @ (c4)
     n4 = ~c4;

   reg out;
   initial out = 0;
   always @ (posedge clk)
     out = n5 & n4;

endmodule // top
