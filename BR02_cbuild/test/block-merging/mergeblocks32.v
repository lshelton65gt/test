// This test case found a bug in merging blocks with dead flow. We //
// were not merging them if it was dead in some instances. This can //
// reduce performance.
module top (out1, out2, out3, clk, in1, in2, in3);
   output out1, out2, out3;
   input  clk, in1, in2, in3;

   reg    rin1, rin2;
   initial begin
      rin1 = 0;
      rin2 = 0;
   end
   always @ (posedge clk)
     begin
        rin1 <= in1;
        rin2 <= in2;
     end

   wire   r1, r2, r3, r4, r5, r6;
   sub S1 (r1, r2, r3, rin1, rin2);
   sub S2 (r4, r5, r6, rin1, rin2);

   reg    out1, out2, out3;
   initial begin
      out1 = 0;
      out2 = 0;
      out3 = 0;
   end
   always @ (r1 or r2 or r3 or r4 or r5)
     begin
        out1 = r1;
        out2 = r2 & r4;
        out3 = r3 & r5;
     end

endmodule // top

module sub (q1, q2, q3, d1, d2);
   output q1, q2, q3;
   input  d1, d2;
   reg    q1, q2, q3;

   always @ (d1 or d2)
     begin
        q2 = d2;
        q3 = d1;
     end

   always @ (d1)
     begin
        q1 = ~d1;
     end

endmodule // sub
