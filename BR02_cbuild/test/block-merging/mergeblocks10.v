// Named block complexity

module top (out1, out2, clk, in1, in2, in3, in4);
  output out1, out2;
  input clk, in1, in2, in3, in4;

  reg c1, c2, c3, c4;
  always @ (in1 or in3)
    begin : n1
       reg t1;
       t1 = ~in1;
       c1 = ~t1;
       begin : n2
	  reg t3;
	  t3 = ~in3;
	  c3 = ~t3;
       end
    end

  always @ (in2 or in4)
    begin : n3
       reg t2, t4;
       t2 = in2 & in4;
       t4 = in2 | in4;
       c2 = ~t2;
       c4 = ~t4;
    end

   reg 	   c5, c6;
   always @ (c1 or c2 or c3 or c4)
     begin
	begin : n4
	   reg t1;
	   reg t2;
	   t1 = ~c1;
	   t2 = ~c2;
	   begin : n5
	      reg t3;
	      t3 = ~c3;
	      c5 = t1 & t2 & t3;
	   end
	end
	c6 = c4;
     end

  reg out1, out2;
  always @ (posedge clk)
    out1 <= c1 & c2;
   always @ (posedge clk)
     out2 <= c3 & c4;

endmodule
