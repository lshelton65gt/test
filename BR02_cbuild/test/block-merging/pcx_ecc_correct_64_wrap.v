module top(AD1, CBE1, ECC1, syndrome1, correctable1, cbe_correctable1, 
	   AD_corrected1, CBE_corrected1, ECC_corrected1, correctable_error1, 
	   uncorrectable_error1,
	   AD2, CBE2, ECC2, syndrome2, correctable2, cbe_correctable2, 
	   AD_corrected2, CBE_corrected2, ECC_corrected2, correctable_error2, 
	   uncorrectable_error2);

   input [63:0]  AD1;
   input [7:0] 	 CBE1;
   input [7:0] 	 ECC1;
   input [7:0] 	 syndrome1;
   input 	 correctable1;
   input 	 cbe_correctable1;
   output [63:0] AD_corrected1;
   output [7:0]  CBE_corrected1;
   output [7:0]  ECC_corrected1;
   output 	 correctable_error1;
   output 	 uncorrectable_error1;
   
   input [63:0]  AD2;
   input [7:0] 	 CBE2;
   input [7:0] 	 ECC2;
   input [7:0] 	 syndrome2;
   input 	 correctable2;
   input 	 cbe_correctable2;
   output [63:0] AD_corrected2;
   output [7:0]  CBE_corrected2;
   output [7:0]  ECC_corrected2;
   output 	 correctable_error2;
   output 	 uncorrectable_error2;

   pcx_ecc_correct_64 one(AD1, CBE1, ECC1, syndrome1, correctable1, cbe_correctable1, 
			  AD_corrected1, CBE_corrected1, ECC_corrected1, correctable_error1, 
			  uncorrectable_error1);
   pcx_ecc_correct_64 two(AD2, CBE2, ECC2, syndrome2, correctable2, cbe_correctable2, 
			  AD_corrected2, CBE_corrected2, ECC_corrected2, correctable_error2, 
			  uncorrectable_error2);
endmodule

module pcx_ecc_correct_64(AD, CBE, ECC, syndrome, correctable, cbe_correctable, 
	AD_corrected, CBE_corrected, ECC_corrected, correctable_error, 
	uncorrectable_error);

	input	[63:0]		AD;
	input	[7:0]		CBE;
	input	[7:0]		ECC;
	input	[7:0]		syndrome;
	input			correctable;
	input			cbe_correctable;
	output	[63:0]		AD_corrected;
	output	[7:0]		CBE_corrected;
	output	[7:0]		ECC_corrected;
	output			correctable_error;
	output			uncorrectable_error;

	wire	[63:0]		correct_ad;
	wire	[7:0]		correct_cbe;
	wire	[7:0]		correct_ecc;

	assign ECC_corrected = (ECC ^ correct_ecc);
	assign CBE_corrected = (CBE ^ correct_cbe);
	assign AD_corrected = (AD ^ correct_ad);
	assign correctable_error = (((|correct_ad) || (|correct_ecc)) || (|
		correct_cbe));
	assign uncorrectable_error = ((syndrome != 8'b0) & (!correctable_error))
		;
	assign correct_ecc[6] = ((syndrome == 8'h40) & correctable);
	assign correct_ecc[5] = ((syndrome == 8'h20) & correctable);
	assign correct_ecc[4] = ((syndrome == 8'h10) & correctable);
	assign correct_ecc[3] = ((syndrome == 8'h08) & correctable);
	assign correct_ecc[2] = ((syndrome == 8'h04) & correctable);
	assign correct_ecc[1] = ((syndrome == 8'h02) & correctable);
	assign correct_ecc[0] = ((syndrome == 8'b1) & correctable);
	assign correct_ecc[7] = ((syndrome == 8'h80) & correctable);
	assign correct_ad[0] = ((syndrome == 8'h49) & correctable);
	assign correct_ad[1] = ((syndrome == 8'h62) & correctable);
	assign correct_ad[2] = ((syndrome == 8'h52) & correctable);
	assign correct_ad[3] = ((syndrome == 8'h4c) & correctable);
	assign correct_ad[4] = ((syndrome == 8'h68) & correctable);
	assign correct_ad[5] = ((syndrome == 8'h23) & correctable);
	assign correct_ad[6] = ((syndrome == 8'h32) & correctable);
	assign correct_ad[7] = ((syndrome == 8'h46) & correctable);
	assign correct_ad[8] = ((syndrome == 8'h5b) & correctable);
	assign correct_ad[9] = ((syndrome == 8'h13) & correctable);
	assign correct_ad[10] = ((syndrome == 8'h31) & correctable);
	assign correct_ad[11] = ((syndrome == 8'h25) & correctable);
	assign correct_ad[12] = ((syndrome == 8'h61) & correctable);
	assign correct_ad[13] = ((syndrome == 8'h43) & correctable);
	assign correct_ad[14] = ((syndrome == 8'h51) & correctable);
	assign correct_ad[15] = ((syndrome == 8'h45) & correctable);
	assign correct_ad[16] = ((syndrome == 8'h6e) & correctable);
	assign correct_ad[17] = ((syndrome == 8'h37) & correctable);
	assign correct_ad[18] = ((syndrome == 8'h54) & correctable);
	assign correct_ad[19] = ((syndrome == 8'h34) & correctable);
	assign correct_ad[20] = ((syndrome == 8'h15) & correctable);
	assign correct_ad[21] = ((syndrome == 8'h3e) & correctable);
	assign correct_ad[22] = ((syndrome == 8'h26) & correctable);
	assign correct_ad[23] = ((syndrome == 8'h64) & correctable);
	assign correct_ad[24] = ((syndrome == 8'h3b) & correctable);
	assign correct_ad[25] = ((syndrome == 8'h2a) & correctable);
	assign correct_ad[26] = ((syndrome == 8'h6b) & correctable);
	assign correct_ad[27] = ((syndrome == 8'h5e) & correctable);
	assign correct_ad[28] = ((syndrome == 8'h29) & correctable);
	assign correct_ad[29] = ((syndrome == 8'h1a) & correctable);
	assign correct_ad[30] = ((syndrome == 8'h4a) & correctable);
	assign correct_ad[31] = ((syndrome == 8'h2c) & correctable);
	assign correct_ad[32] = ((syndrome == (8'h49 ^ 8'hff)) & correctable);
	assign correct_ad[33] = ((syndrome == (8'h62 ^ 8'hff)) & correctable);
	assign correct_ad[34] = ((syndrome == (8'h52 ^ 8'hff)) & correctable);
	assign correct_ad[35] = ((syndrome == (8'h4c ^ 8'hff)) & correctable);
	assign correct_ad[36] = ((syndrome == (8'h68 ^ 8'hff)) & correctable);
	assign correct_ad[37] = ((syndrome == (8'h23 ^ 8'hff)) & correctable);
	assign correct_ad[38] = ((syndrome == (8'h32 ^ 8'hff)) & correctable);
	assign correct_ad[39] = ((syndrome == (8'h46 ^ 8'hff)) & correctable);
	assign correct_ad[40] = ((syndrome == (8'h5b ^ 8'hff)) & correctable);
	assign correct_ad[41] = ((syndrome == (8'h13 ^ 8'hff)) & correctable);
	assign correct_ad[42] = ((syndrome == (8'h31 ^ 8'hff)) & correctable);
	assign correct_ad[43] = ((syndrome == (8'h25 ^ 8'hff)) & correctable);
	assign correct_ad[44] = ((syndrome == (8'h61 ^ 8'hff)) & correctable);
	assign correct_ad[45] = ((syndrome == (8'h43 ^ 8'hff)) & correctable);
	assign correct_ad[46] = ((syndrome == (8'h51 ^ 8'hff)) & correctable);
	assign correct_ad[47] = ((syndrome == (8'h45 ^ 8'hff)) & correctable);
	assign correct_ad[48] = ((syndrome == (8'h6e ^ 8'hff)) & correctable);
	assign correct_ad[49] = ((syndrome == (8'h37 ^ 8'hff)) & correctable);
	assign correct_ad[50] = ((syndrome == (8'h54 ^ 8'hff)) & correctable);
	assign correct_ad[51] = ((syndrome == (8'h34 ^ 8'hff)) & correctable);
	assign correct_ad[52] = ((syndrome == (8'h15 ^ 8'hff)) & correctable);
	assign correct_ad[53] = ((syndrome == (8'h3e ^ 8'hff)) & correctable);
	assign correct_ad[54] = ((syndrome == (8'h26 ^ 8'hff)) & correctable);
	assign correct_ad[55] = ((syndrome == (8'h64 ^ 8'hff)) & correctable);
	assign correct_ad[56] = ((syndrome == (8'h3b ^ 8'hff)) & correctable);
	assign correct_ad[57] = ((syndrome == (8'h2a ^ 8'hff)) & correctable);
	assign correct_ad[58] = ((syndrome == (8'h6b ^ 8'hff)) & correctable);
	assign correct_ad[59] = ((syndrome == (8'h5e ^ 8'hff)) & correctable);
	assign correct_ad[60] = ((syndrome == (8'h29 ^ 8'hff)) & correctable);
	assign correct_ad[61] = ((syndrome == (8'h1a ^ 8'hff)) & correctable);
	assign correct_ad[62] = ((syndrome == (8'h4a ^ 8'hff)) & correctable);
	assign correct_ad[63] = ((syndrome == (8'h2c ^ 8'hff)) & correctable);
	assign correct_cbe[0] = (((syndrome == 8'h5d) & correctable) & 
		cbe_correctable);
	assign correct_cbe[1] = (((syndrome == 8'h58) & correctable) & 
		cbe_correctable);
	assign correct_cbe[2] = (((syndrome == 8'h19) & correctable) & 
		cbe_correctable);
	assign correct_cbe[3] = (((syndrome == 8'h3d) & correctable) & 
		cbe_correctable);
	assign correct_cbe[4] = (((syndrome == (8'h5d ^ 8'hff)) & correctable) &
		cbe_correctable);
	assign correct_cbe[5] = (((syndrome == (8'h58 ^ 8'hff)) & correctable) &
		cbe_correctable);
	assign correct_cbe[6] = (((syndrome == (8'h19 ^ 8'hff)) & correctable) &
		cbe_correctable);
	assign correct_cbe[7] = (((syndrome == (8'h3d ^ 8'hff)) & correctable) &
		cbe_correctable);
endmodule
