module top (out1, out2, out3, out4, clk1, clk2, in1, in2, in3, in4);
   output out1, out2, out3, out4;
   input  clk1, clk2, in1, in2, in3, in4;

   sub S1 (out1, out2, in1, in2, clk2, clk1);
   sub S2 (out3, out4, in3, in4, clk1, clk2);

endmodule // top

module sub (o1, o2, i1, i2, c1, c2);
   output o1, o2;
   input  i1, i2, c1, c2;
   reg 	  o1, o2;

   always @ (posedge c1)
     o1 <= i1;

   always @ (posedge c2)
     o2 <= i2;

endmodule // sub
