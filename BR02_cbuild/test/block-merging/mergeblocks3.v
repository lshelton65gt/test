module top (out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   // Register the inputs
   reg 	  r1, r2;
   initial begin
      r1 = 0;
      r2 = 0;
   end
   always @ (posedge clk)
     begin
	r1 <= in1;
	r2 <= in2;
     end

   reg t1, t2;
   initial begin
      t1 = 1;
      t2 = 0;
   end
   always @ (r1)
     begin
	t1 = ~r1;
	t2 = ~t1;
     end

   reg t3, t4;
   initial begin
      t3 = 1;
      t4 = 1;
   end
   always @ (t2 or r2)
     begin
	t3 = ~t2;
	t4 = ~r2;
     end

   reg out;
   initial out = 1;
   always @ (posedge clk)
     out <= t3 & t4;

endmodule
