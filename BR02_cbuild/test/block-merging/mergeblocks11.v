module top (out, clk, in1, in2, in3, in4);
   output out;
   input  in1, in2, in3, in4, clk;
   reg 	  r1, r2, r3, r4;
   
   always @ (posedge clk)
     begin
	r1 <= in1;
	r2 <= in2;
	r3 <= in3;
	r4 <= in4;
     end

   wire   c1, c2, c3;
   assign c1 = ~r1;
   assign c2 = c1 & ~r2;
   assign c3 = c1 & r3;
   assign out = ~c2 & c1 & ~c3 & r4;
endmodule // top
