module stat_cntl ( i_ch_st_rw_addr,
		   i_ch_st_rw_cs,
		   i_ch_st_rw_wr,
		   out);

   input [4:0] i_ch_st_rw_addr;
   input       i_ch_st_rw_cs;
   input       i_ch_st_rw_wr;
   output      out;
   
   wire st_mem_txs_imau_cs = (i_ch_st_rw_addr[4:3] ==2'b11)? i_ch_st_rw_cs :1'b0;
   wire st_mem_txs_imau_re = ((st_mem_txs_imau_cs)&(~i_ch_st_rw_wr));
   wire st_mem_txs_imau_we = ((st_mem_txs_imau_cs)&(i_ch_st_rw_wr));
   assign out = st_mem_txs_imau_we & st_mem_txs_imau_re;
endmodule
