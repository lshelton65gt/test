module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   // Register the inputs
   reg 	  r1, r2;
   always @ (posedge clk)
     begin
	r1 <= in1;
	r2 <= in2;
     end

   // Create two distinct chains
   wire c1, c2, c3, c4;
   assign c1 = ~r1;
   assign c2 = ~r2;
   assign c3 = ~c1;
   assign c4 = ~c2;
   assign out1 = ~c3;
   assign out2 = ~c4;

endmodule // top
