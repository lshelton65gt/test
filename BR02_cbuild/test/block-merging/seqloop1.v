// This test has a cycle between two unelaborated always blocks 
// because there is flow from one instance to another for the same
// always block

module top (out1, out2, clk, d);
   output out1, out2;
   input  clk, d;

   wire   r1, r2;
   flop F1 (r1, clk, d);

   reg    out1;
   initial out1 = 1'b0;
   always @ (posedge clk)
     out1 <= r1;

   flop F2 (out2, clk, out1);

endmodule

module flop (q, clk, d);
   output q;
   input  clk, d;

   reg    q;
   initial q = 1'b0;
   always @ (posedge clk)
     q <= d;

endmodule
