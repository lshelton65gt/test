// Simple depth merging example

module top (out, clk, in1, in2);
  output out;
  input clk, in1, in2;

  reg t1, t2;
  always @ (in1)
    t1 = ~in1;

  always @ (in2)
    t2 = ~in2;

  reg out;
  always @ (posedge clk)
    out <= t1 & t2;

endmodule
