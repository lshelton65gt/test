module top (out1, out2, out3, out4, clk, in1, in2, in3, in4);
   output out1, out2, out3, out4;
   input  clk, in1, in2, in3, in4;

   sub S1 (out1, out2, clk, in1, in2);
   sub S2 (out3, out4, ~clk, in3, in4);

endmodule // top

module sub (q1, q2, clk, d1, d2);
   output q1, q2;
   input  clk, d1, d2;
   reg 	  q1, q2;

   initial begin
      q1 = 0;
      q2 = 0;
   end
   
   always @ (posedge clk)
     q1 <= d1;

   always @ (posedge clk)
     q2 <= d2;

endmodule // sub
