module top (out1, out2, clk1, clk2, d);
  output out1, out2;
  input clk1, clk2, d;
  reg out1, out2;
  wire dinv;

  assign dinv = ~d;

  always @ (posedge clk1)
    out1 <= dinv;

  always @ (posedge clk2)
    out2 <= dinv;

endmodule

