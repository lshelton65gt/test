-- This test case came from Hitachi (bug7266) and showed that we were
-- 2x slower than Tenison. func_core was their test. func was code we
-- added to instantiate 2 times. The customer instantiated it many
-- times (see test/bugs/bug7266).
--
-- Josh found analyzing this test case that additional sequential
-- block merging helped simulation performance. This helped show and
-- idea on how to do more aggressive sequential block merging.
--
-- With the new block merging (and flattening off) we now end up with
-- three always blocks (posedge clk, posedge rst, and a latch).
-- Before the change there were more flops.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity func_core is
port (
  rst : in std_logic;
  clk : in std_logic;
  data : in std_logic_vector(3 downto 0);
  in_p : in std_logic_vector(31 downto 0);
  in_m : in std_logic_vector(31 downto 0);
  out_p : out std_logic_vector(31 downto 0);
  out_m : out std_logic_vector(31 downto 0)
);
end func_core;

architecture SYN of func_core is
signal inc_s0 : std_logic_vector(31 downto 0);
signal dec_s0 : std_logic_vector(31 downto 0);
signal mul_s1 : std_logic_vector(63 downto 0);
signal add_s2 : std_logic_vector(31 downto 0);
signal sub_s2 : std_logic_vector(31 downto 0);
signal mul_s3 : std_logic_vector(63 downto 0);
signal add_s4 : std_logic_vector(31 downto 0);
signal sub_s4 : std_logic_vector(31 downto 0);
signal eb : std_logic_vector(3 downto 0);
signal reg0 : std_logic_vector(3 downto 0);
signal reg1 : std_logic_vector(3 downto 0);
signal reg2 : std_logic_vector(3 downto 0);
signal reg3 : std_logic_vector(3 downto 0);
signal reg4 : std_logic_vector(3 downto 0);
signal reg5 : std_logic_vector(3 downto 0);
signal reg6 : std_logic_vector(3 downto 0);
signal reg7 : std_logic_vector(3 downto 0);
signal reg8 : std_logic_vector(3 downto 0);
signal reg9 : std_logic_vector(3 downto 0);
signal regA : std_logic_vector(3 downto 0);
signal regB : std_logic_vector(3 downto 0);
signal regC : std_logic_vector(3 downto 0);
signal regD : std_logic_vector(3 downto 0);
signal regE : std_logic_vector(3 downto 0);
signal regF : std_logic_vector(3 downto 0);
signal regF_C : std_logic_vector(15 downto 0);
signal enable : std_logic;
signal buff_reg : std_logic_vector(63 downto 0);

begin

  process(rst,clk)
  begin
    if (rst='1') then
      reg0 <= (others=>'0');
      reg1 <= (others=>'0');
      reg2 <= (others=>'0');
      reg3 <= (others=>'0');
      reg4 <= (others=>'0');
      reg5 <= (others=>'0');
      reg6 <= (others=>'0');
      reg7 <= (others=>'0');
      reg8 <= (others=>'0');
      reg9 <= (others=>'0');
      regA <= (others=>'0');
      regB <= (others=>'0');
      regC <= (others=>'0');
      regD <= (others=>'0');
      regE <= (others=>'0');
      regF <= (others=>'0');
    elsif (clk'event and clk='1') then
      reg0 <= data;
      reg1 <= reg0;
      reg2 <= reg1;
      reg3 <= reg2;
      reg4 <= reg3;
      reg5 <= reg4;
      reg6 <= reg5;
      reg7 <= reg6;
      reg8 <= reg7;
      reg9 <= reg8;
      regA <= reg9;
      regB <= regA;
      regC <= regB;
      regD <= regC;
      regE <= regD;
      regF <= regE;
    end if;
  end process;

  regF_C <= regF & regE & regD & regC;

  process(regF_C)
  begin
    if (regF_C = "1111011000101000") then
      enable <= '1';
    else
      enable <= '0';
    end if;
  end process; 

  inc_s0 <= in_p;
  dec_s0 <= in_m;

  process(rst,clk)
  begin
    if (rst='1') then
      mul_s1 <= (others => '0');
    elsif (clk'event and clk='1') then
      if (enable='1') then
        mul_s1 <= inc_s0 * dec_s0;
      end if;
    end if;
  end process;

  process(rst,clk)
  begin
    if (rst='1') then
      add_s2 <= (others => '0');
    elsif (clk'event and clk='1') then
      if (enable='1') then
        add_s2 <= (mul_s1(47 downto 32) & mul_s1(63 downto 48)) + inc_s0;
      end if;
    end if;
  end process;

  process(rst,clk)
  begin
    if (rst='1') then
      sub_s2 <= (others => '0');
    elsif (clk'event and clk='1') then
      if (enable='1') then
        sub_s2 <= (mul_s1(15 downto 0) & mul_s1(31 downto 16))  - dec_s0;
      end if;
    end if;
  end process;

  process(rst,clk)
  begin
    if (rst='1') then
      mul_s3 <= (others => '0');
    elsif (clk'event and clk='1') then
      if (enable='1') then
        mul_s3 <= add_s2 * sub_s2;
      end if;
    end if;
  end process;

  process(mul_s3)
  begin
    if ((mul_s3(15 downto 8)>="11001100") and (mul_s3(7 downto 0)<="00001100")) then
      buff_reg(63 downto 16) <= mul_s3(63 downto 16);
      buff_reg(15 downto 8) <= mul_s3(7 downto 0);
      buff_reg(7 downto 0) <= mul_s3(15 downto 8);
    elsif ((mul_s3(47 downto 32)>="10001000") and (mul_s3(31 downto 16)<="01101010")) then
      buff_reg(63 downto 48) <= "0000010000000000";
      buff_reg(47 downto 32) <= mul_s3(15 downto 0);
    elsif (mul_s3(63 downto 58)>="001011") then
      buff_reg(63 downto 48) <= "1010101010101010";
      buff_reg(47 downto 32) <= mul_s3(55 downto 40);
      buff_reg(31 downto 0) <= mul_s3(45 downto 14);
    else
      buff_reg <= mul_s3;
    end if;
  end process;

  process(rst,clk)
  begin
    if (rst='1') then
      add_s4 <= (others => '0');
    elsif (clk'event and clk='1') then
      if (enable='1') then
        add_s4 <= (buff_reg(63 downto 48) & buff_reg(47 downto 32)) + inc_s0;
      end if;
    end if;
  end process;

  process(rst,clk)
  begin
    if (rst='1') then
      sub_s4 <= (others => '0');
    elsif (clk'event and clk='1') then
      if (enable='1') then
        sub_s4 <= (mul_s3(15 downto 0) & mul_s3(31 downto 16))  - dec_s0;
      end if;
    end if;
  end process;

  out_p <= add_s4;
  out_m <= sub_s4;
end SYN;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity func is
  
  port (
    rst : in std_logic;
    clk : in std_logic;
    in_p : in std_logic_vector(31 downto 0);
    in_m : in std_logic_vector(31 downto 0);
    out_p : out std_logic_vector(31 downto 0);
    out_m : out std_logic_vector(31 downto 0)
  );
end func;

architecture SYN of func is

  signal r1 : std_logic_vector(31 downto 0);
  signal r2 : std_logic_vector(31 downto 0);
  signal data : std_logic_vector(3 downto 0);
  signal count : std_logic_vector(1 downto 0) := (others => '0');

  component func_core is
  port (
    rst : in std_logic;
    clk : in std_logic;
    data : in std_logic_vector(3 downto 0);
    in_p : in std_logic_vector(31 downto 0);
    in_m : in std_logic_vector(31 downto 0);
    out_p : out std_logic_vector(31 downto 0);
    out_m : out std_logic_vector(31 downto 0)
  );
  end component;

  for u1 : func_core
    use entity work.func_core(syn);
  for u2 : func_core
    use entity work.func_core(syn);


begin  -- SYN

  process (clk, rst)
  begin  -- process
    if rst = '1' then                   -- asynchronous reset (active high)
      data <= (others => '0');
      count <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if count = "1111" then
        count <= "00";
      else
        count <= count + 1;
      end if;
      case count is
        when "00" => data <= "1111";
        when "01" => data <= "0110";
        when "10" => data <= "0010";
        when "11" => data <= "1000";
        when others => data <= "0000";
      end case;
    end if;
  end process;

  u1 : func_core
    port map (
      rst    => rst,
      clk    => clk,
      data   => data,
      in_p   => in_p,
      in_m   => in_m,
      out_p  => r1,
      out_m  => r2);
  u2 : func_core
    port map (
      rst    => rst,
      clk    => clk,
      data   => data,
      in_p   => r1,
      in_m   => r2,
      out_p  => out_p,
      out_m  => out_m);

end SYN;
