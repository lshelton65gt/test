module pad (out ,in ,io);
output out;
input  in;
inout  io;
  pmos i0 (io , in , 1'b0);
  buf  i1 (out, io);
endmodule 

module rif (ck ,eo ,ao);
parameter AW=8;
parameter DW=5;
input  ck;
output eo;
output [AW-1:0] ao;

reg    [DW-1:0] cnt;
reg    eo_q;
wire   eo_w;
wire   [AW-1:0] ao_w;
reg    [AW-1:0] ao_q;

always @(posedge ck) begin 
  cnt  <= cnt + 1;
  eo_q <= eo_w;
  ao_q <= ao_w;
end 

assign ao_w = ao_f(cnt);
assign eo   = eo_q;
assign ao   = ao_q;

function [AW-1:0] ao_f;
input    [DW-1:0] cnt;
  ao_f = cnt + 1;
endfunction 

endmodule 

module top (ck, io);
parameter DW=8, AW=8;
input ck;
inout io;

reg  [DW-1:0] do1, do2;
wire in_o = &{do1, do2};

pad pad0 (.io(io), .in(in_o), .out( ));

wire en1, en2;
wire [AW-1:0] a1, a2;

rif #(AW,5) r1 (.eo(en1), .ao(a1));
rif #(AW,5) r2 (.eo(en2), .ao(a2));

reg  [DW-1:0] m1[0:(1<<AW)-1], m2[0:(1<<AW)-1];
   initial begin
      do1 = 0;
      do2 = 0;
   end
always @ (posedge ck) begin
  if (en1) do1 <= m1[a1];
  else     do1 <= {DW{1'bx}};
  if (en2) do2 <= m2[a2];
  else     do2 <= {DW{1'bx}};
end

endmodule 

