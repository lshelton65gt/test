module top (out, clk, d);
  output out;
  input clk, d;
  reg out;
  wire dinv;

  assign dinv = ~d;

  always @ (posedge clk)
    out <= dinv;

endmodule

