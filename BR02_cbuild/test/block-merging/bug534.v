module ci_config_regs( reg_add_d, rx_ficr0_out_fc, rx_ficr0_out, rx_ficr1_out, rx_ficr2_out, gfir0_out, gfir1_out, iadrr_out, idata_out, tx_ficr0_out, o_data_out);

   input [3:0]  reg_add_d;
   input        rx_ficr0_out_fc;
   input [11:0] rx_ficr0_out;
   input [13:0] rx_ficr1_out;
   input [11:0] rx_ficr2_out;
   input [15:0] gfir0_out;
   input [15:0] gfir1_out;
   input [15:0] iadrr_out;
   input [15:0] idata_out;
   input [2:0] 	tx_ficr0_out;

   output [15:0] o_data_out;
   reg [15:0] o_data_out;
   
   wire [15:0] 	 rx_ficr0_concat;
   wire [15:0] 	 rx_ficr1_concat;
   wire [15:0] 	 rx_ficr2_concat;
   wire [15:0] 	 gfir0_concat;
   wire [15:0] 	 gfir1_concat;
   wire [15:0] 	tx_ficr0_concat;
   
   assign rx_ficr0_concat =
	  {rx_ficr0_out[11:3],2'h0,rx_ficr0_out[2],1'b0,rx_ficr0_out[1:0],rx_ficr0_out_fc}; 
   assign rx_ficr1_concat = {rx_ficr1_out[13:9],2'b0,rx_ficr1_out[8:0]}; 
   assign rx_ficr2_concat = {4'b0,rx_ficr2_out[11:0]}; 
   assign gfir0_concat = gfir0_out; 
   assign gfir1_concat = gfir1_out[15:0]; 
   assign tx_ficr0_concat = {13'h0,tx_ficr0_out}; 
   always @(reg_add_d or rx_ficr0_concat or rx_ficr1_concat or rx_ficr2_concat 
	    or gfir1_concat or gfir0_concat or tx_ficr0_concat or iadrr_out or idata_out ) 
     case (reg_add_d)
       4'b0001: o_data_out = rx_ficr0_concat;
       4'b0010: o_data_out = rx_ficr1_concat;
       4'b0011: o_data_out = rx_ficr2_concat;
       4'b0100: o_data_out = gfir0_concat;
       4'b0101: o_data_out = gfir1_concat;
       4'b0110: o_data_out = iadrr_out;
       4'b0111: o_data_out = idata_out; 
       4'b1000: o_data_out = tx_ficr0_concat;
       4'b1001: o_data_out = iadrr_out;
       4'b1010: o_data_out = idata_out; 
       default : o_data_out = 16'h0;
     endcase

endmodule
