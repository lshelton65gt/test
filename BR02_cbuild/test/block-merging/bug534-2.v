module doc_exu ( srptr_cur, brptr_present, rptr_present, srptr_present, crptr_present, abs_brptr, abs_rptr, crptr_cur_wd, dmem_addr, smem_addr, cmem_addr );
   
   input [13:0] srptr_cur;
   input 	brptr_present;
   input 	rptr_present;
   input 	srptr_present;
   input 	crptr_present;
   
   input [9:0] abs_brptr;
   input [9:0] abs_rptr;
   input [8:0] crptr_cur_wd;

   output [9:0] dmem_addr;
   output [7:0] smem_addr;
   output [8:0] cmem_addr;

   reg [9:0] 	dmem_addr;
   reg [7:0] 	smem_addr;
   reg [8:0] 	cmem_addr;
   
   wire [10:0] 	srptr_cur_wd;
   assign srptr_cur_wd = srptr_cur[13:3];
   always @ ( abs_brptr or abs_rptr or brptr_present
	      or crptr_cur_wd or crptr_present or rptr_present
	      or srptr_cur_wd or srptr_present) 
     begin
	dmem_addr = 10'b0;
	smem_addr = 8'b0;
	cmem_addr = 9'b0;
	if (brptr_present)
	  dmem_addr = abs_brptr;
	else if (rptr_present)
	  dmem_addr = abs_rptr;
	else if (srptr_present)
	  smem_addr = srptr_cur_wd[7:0];
	else if (crptr_present)
	  cmem_addr = crptr_cur_wd[8:0];
     end
endmodule
