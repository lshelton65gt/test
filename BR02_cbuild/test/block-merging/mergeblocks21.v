// Merge multi-hierarchy sequential blocks

module top (out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   wire   res1, res2;
   sub S1 (res1, clk, in1, in2);
   sub S2 (res2, clk, ~in1, ~in2);
   sub S3 (out, clk, res1, res2);

endmodule // top

module sub (out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   // Register the inputs
   reg 	  r1, r2;
   initial begin
      r1 = 0;
      r2 = 0;
   end
   always @ (posedge clk)
     begin
	r1 <= in1;
	r2 <= in2;
     end

   reg t1, t2;
   initial begin
      t1 = 0;
      t2 = 1;
   end
   always @ (r1 or r2)
     t1 = r1 & r2;

   always @ (t1)
     t2 = ~t1;

   reg out;
   initial out = 1;
   always @ (posedge clk)
     out <= t2;

endmodule
