module top(out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   // We create two subs2's and one sub1 so that only sub1 merges
   wire   o1, o2;
   sub2 S2_1 (o1, clk, in1, in2);
   sub2 S2_2 (o2, clk, in1, in2);
   sub1 S1 (out, clk, o1, o2);

endmodule // top

module sub1 (out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   reg 	  t1, t2;
   initial begin
      t1 = 0;
      t2 = 0;
   end
   always @ (in1)
     t1 = ~in1;

   always @ (in2)
     t2 = ~in2;

   reg 	  out;
   initial out = 0;
   always @ (posedge clk)
     out <= t1 & t2;

endmodule

module sub2 (out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   reg 	  t1, t2;
   initial begin
      t1 = 0;
      t2 = 0;
   end
   always @ (in1)
     t1 = ~in1;

   always @ (in2)
     t2 = ~in2;

   reg 	  out;
   initial out = 0;
   always @ (posedge clk)
     out <= t1 & t2;

endmodule
