module top(i1, i2, i3, i4, clk, o1, o2);
   input i1, i2;
   input i3, i4;
   input clk;
   output o1, o2;
   wire w1 = i1 & i2;
   wire w2 = ~i1 & i2;
   wire w3 = i1 & ~i2;
   middle middle1(w1, w2, clk, o1);
   middle middle2(w1, w3, clk, o2);
   assign o2 = w2 & i2;
endmodule

module middle(i1, i2, clk, o);
   input i1, i2;
   input clk;
   output o;
   reg    o;
   // observes prevent the temporaries from being optimized away.
   reg 	  t1; // carbon observeSignal
   reg 	  t2; // carbon observeSignal
   always @(posedge clk) begin
     t1 = ~i1;
     t2 = ~i2;
     o = (~t1) & (~t2);
   end
endmodule
