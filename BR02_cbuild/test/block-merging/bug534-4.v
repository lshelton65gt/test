module chcrc_exu( ac_discard, ac_length, ac_data, lane_num, chcrc_reqIf_status, chcrc_reqIf_ri, chcrc_reqIf_lanen, chcrc_reqIf_esn );

   input        ac_discard;
   input [15:0] ac_length;
   input 	ac_data;
   input 	lane_num;
   output [16:0] chcrc_reqIf_status;
   output 	 chcrc_reqIf_ri;
   output 	 chcrc_reqIf_lanen;
   output 	 chcrc_reqIf_esn;

   reg [16:0] 	reqf_status;
   reg 		reqf_ri;
   reg 		reqf_lanenum;
   reg 		reqf_esn;
   always @ (ac_data or ac_data
	     or ac_discard or ac_length or lane_num)
     begin
	reqf_status = { ac_discard, ac_length[15:0]};
	reqf_ri = ac_data;
	reqf_lanenum = lane_num;
	reqf_esn = ac_data;
     end
   assign chcrc_reqIf_status = reqf_status;
   assign chcrc_reqIf_ri = reqf_ri;
   assign chcrc_reqIf_lanen = reqf_lanenum;
   assign chcrc_reqIf_esn = reqf_esn;

endmodule
