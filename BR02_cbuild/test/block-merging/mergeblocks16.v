module top (in,clk,rst,out1);
   input in,clk,rst;
   output out1;
   reg out1;

   wire t1,t2,t3,t4;

   assign t1 = ~in;
   assign t2 = ~t1;
   assign t3 = ~t2;
   assign t4 = ~t3;

   always @ (posedge clk or posedge rst)
     begin
       if (rst) begin
         out1 <= 1'b0;
       end else begin
         out1 <= t4;
       end
     end

endmodule // top
