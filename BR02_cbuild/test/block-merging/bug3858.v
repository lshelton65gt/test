// Test of block merging being able to handle
// dead bits of a net.
module top(i1, i2, clk0, clk1, clk2, rst, a, b, c);
   input [1:0] i1, i2;
   input       clk0, clk1, clk2, rst;
   output      a, b, c;
   reg 	       a, b, c;
   wire [1:0]   r;
   wire [1:0]   s;
   wire         p, q;
   
   bottom bottom1 (i1, i2, clk1, clk2, rst, a1,b1,c1);
   bottom bottom2 (i1, i2, clk1, clk2, rst, a2,b2,c2);

   initial begin
      a=0; b=0; c=0;
   end
   
   always @(posedge clk0) begin
     a = a1 & c1 & c2;
     b = a2;
     c = b2;
   end
endmodule


module bottom(i1, i2, clk, clk2, rst, a, b, c);
   input [1:0] i1, i2;
   input        clk, clk2, rst;
   output 	a,b,c;
   reg 		a,b,c;

   reg [1:0]    o;
   reg          o2;

   initial begin
      a = 0; b = 0; c = 0; o = 0; o2 = 0;
   end

   always @(posedge clk2)
     a = o[1] & o2;
   always @(posedge clk2)
     b = o[0];
   always @(posedge clk2)
     c = o[1];
   
   always @(posedge clk or posedge rst)
     begin
     if (rst)
       begin
          o[0] = 1'b1;
          o2 = 1'b0;
       end
     else
       begin
          o[0] = i1[0] & i2[1];
          o2 =i1[0];
       end
     end
   
   always @(posedge clk or posedge rst)
     if (rst)
       o[1] = 1'b0;
     else
       o[1] = i1[1] & i2[0];

endmodule
