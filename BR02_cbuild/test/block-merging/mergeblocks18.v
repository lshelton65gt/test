module top (in,other,clk,out);
   input in,other,clk;
   output out;
   reg t1;

   assign out = other | ~t1;

   always @ (posedge clk)
     begin
       t1 <= in;
     end

endmodule // top
