module top (out1, out2, clk1, clk2, in1, in2);
   output out1, out2;
   input  clk1, clk2, in1, in2;

   // Create two different timing blocks so we don't merge them
   reg 	  r1, r2;
   initial begin
      r1 = 0;
      r2 = 0;
   end
   always @ (posedge clk1)
     r1 <= in1;
   always @ (posedge clk2)
     r2 <= in2;

   reg 	  t1, t2;
   initial begin
      t1 = 1;
      t2 = 1;
   end
   always @ (r1)
     t1 = ~r1;

   always @ (r2)
     t2 = ~r2;

   reg 	 out1, out2;
   initial begin
      out1 = 1;
      out2 = 1;
   end
   always @ (posedge clk1)
     out1 <= t1;
   always @ (posedge clk2)
     out2 <= t2;

endmodule
