// l1 and l2 are both exclusive
// l1 cannot be converted because en is defed and used as control
module top(clk, x, l1, l2);
input clk, x;
output l1, l2;

reg en, l1, l2;

   initial begin
      l1 = 0;
      l2 = 0;
   end

always @ (clk or en or x or l2)
begin
  if (clk & en)
  begin
    en = ~x;
    l1 = l2;
  end
end

always @ (clk or en or l1)
begin
  if (!clk & en)
    l2 = ~l1;
end

endmodule
