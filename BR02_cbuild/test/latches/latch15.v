// simple latch pair with asynchronous reset
// the reset is split into a different if statement
// l1 is exclusive but l2 is not (/rst & ~clk)
module top(clk, rst, l1, l2);
input clk, rst;
output l1, l2;

reg l1, l2;

   initial begin
      l1 = 0;
      l2 = 1;
   end

always @ (clk or l2 or rst)
begin
  if (clk)
    l1 = l2;
  if (rst)
    l1 = 0;
end

always @ (clk or l1)
begin
  if (~clk)
    l2 = ~l1;
end

endmodule
