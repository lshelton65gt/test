module memory_top(clk, i1, i2, value, out);
  input clk;
  input i1;
  input i2;
  input [1:0] value;
  output out;
   reg   out;
   
  reg [3:0]          foo;

  initial
    begin
      foo = 4'h0;
    end

   always @(i1 or value)
     foo[(i1 ? 2'h2 : 2'h0)+:2] = value;

  always @(posedge clk)
    begin
      out = foo[(i2 ? 2'h2 : 2'h0)];
    end
endmodule
