module top (out, clk, in1, in2, en);
   output out;
   input  clk, in1, in2, en;

   // Flop the inputs so that we have stable results with respect to
   // Aldec.
   reg    ren, rin1, rin2;
   initial ren = 0;
   initial rin1 = 0;
   initial rin2 = 0;
   always @ (negedge clk)
     begin
        ren <= en;
        rin1 <= in1;
        rin2 <= in2;
     end
   wire   gclk = clk & ren;

   reg    r1, r2;
   initial r1 = 0;
   initial r2 = 0;
   always @ (posedge gclk)
     r1 <= rin1;
   always @ (gclk or rin2)
     if (gclk)
       r2 <= rin2;

   reg    r3;
   initial r3 = 0;
   always @ (gclk or r1)
     if (~gclk)
       r3 <= r1 ^ r2;

   reg    out;
   initial out = 1;
   always @ (gclk or r3)
     if (gclk)
       out <= ~r3;

endmodule
