module top (out1, out2, clk, d1, d2);
   output      out1, out2;
   input [1:0] clk;
   input       d1, d2;

   // Register the data inputs so we can have exclusivity
   reg    r1, r2;
   initial begin
      r1 = 0;
      r2 = 0;
   end
   wire clk1;
   assign clk1 = clk[1];
   always @ (negedge clk1)
     begin
        r1 <= d1;
        r2 <= d2;
     end

   // Create two live and two dead latches
   wire tmp1, tmp2;
   sub S1 (out1, tmp1, clk, r1, r2);
   sub S2 (tmp2, out2, clk, r1, r2);

endmodule

module sub (l1, l2, clk, i1, i2);
   output      l1, l2;
   input [1:0] clk;
   input       i1, i2;

   reg    l1, l2;
   initial begin
      l1 = 0;
      l2 = 0;
   end

   always @ (clk or i1)
     if (clk[1])
       l1 <= i1;
   always @ (clk or i2)
     if (clk[1])
       l2 <= i2;

endmodule
