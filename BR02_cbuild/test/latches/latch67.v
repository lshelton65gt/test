// This is small version of bug5765 that needs
// both port splitting and one of the bits
// of a latch net to be dead to cause a crash
// in latch conversion. It occurs when there
// is both a clk and rst in the latch

module top (out1, out2, clk, rst, in1, in2);
    output out1, out2;
    input  clk, rst, in1, in2;

    // Flop the inputs to help conversion
    reg    r1, r2;
    initial begin
       r1 = 0;
       r2 = 0;
    end
    always @ (negedge clk)
      begin
         r1 <= in1;
         r2 <= in2;
      end

    wire   l1, l2;
    latches L1 ({out1, l1}, clk, rst, r1);
    latches L2 ({out2, l2}, clk, rst, r2);

    assign l2 = clk ? 1'bz : 1'b0;

endmodule

module latches (out, clk, rst, in);
    output [1:0] out;
    input        rst, clk, in;

    reg [1:0]    out;
    initial out = 0;
    always @ (clk or rst or in)
      if (~rst)
        begin
           out[0] <= 1'b0;
           out[1] <= 1'b1;
        end
      else if (clk)
        begin
           out[0] <= in ^ out[0];
           out[1] <= ~in;
        end

endmodule
