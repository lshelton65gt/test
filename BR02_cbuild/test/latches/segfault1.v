// This testcase has a combinational cycle in the clock
// path of a latch.

module top(io);
inout io;

wire io;

reg x;
reg y;

assign io = 1'b1;

always @(io)
  if (io)
    y <= ~x;

always @(io)
  if (io)
    x <= ~y;

sub U (y, io);

endmodule

module sub(clk, out);
input clk;
output out;

wire clk;
wire out;

reg [1:0] vec;
reg val;

always @(negedge clk)
  val <= 1'b1;

always @(negedge val)
  vec[0] = vec[1];

always @(clk)
  if (~clk)
    vec[1] <= 1'b0;

assign out = vec[0];

endmodule

