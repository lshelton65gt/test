// l1 and l2 are both exclusive
// this tests handling of latches of with complex enable expressions
module top(clk, rst, l1, l2);
input clk, rst;
output l1, l2;

reg l1, l2;

   initial begin
      l1 = 0;
      l2 = 0;
   end

always @ (clk or rst or l2)
begin
  if (clk & ~rst)
    l1 = l2;
  else if (rst)
    l1 = 0;
end

always @ (clk or rst or l1)
begin
  if (rst)
    l2 = 0;
  else if (~clk)
    l2 = l1;
end

endmodule
