// simple exclusive latch pair with synchronous set and reset
// both l1 and l2 are exclusive
module top(clk, rst0, rst1, l1, l2);
input clk, rst0, rst1;
output l1, l2;

reg l1, l2;

   initial begin
      l1 = 0;
      l2 = 0;
   end

always @ (clk or rst0 or rst1 or l2)
begin
  if (clk)
  begin
    if (rst0) 
      l1 = 0;
    else if (rst1)
      l1 = 1;
    else
      l1 = l2;
  end
end

always
begin @ (clk or l1)
  if (~clk)
    l2 = ~l1;
end

endmodule
