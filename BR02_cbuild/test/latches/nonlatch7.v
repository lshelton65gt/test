module top (out, sel);
  output out;
  input sel;

  reg out;
  always @ (sel)
    if (sel)
       out <= 1'b0;
    else if (~sel)
       out <= 1'b1;

endmodule
