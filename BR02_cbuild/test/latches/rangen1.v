// last mod: Thu Jul 27 16:50:19 2006
// filename: test/latches/rangen1.v
// Description:  This test was created by rangen, it caused a segfault in the
// latch analysis code.


// Generated with: ./rangen -s 79 -n 0xf0412068 -o /w/bohrium/cloutier/Wmaint2/obj/Linux.debug/test/rangen/rangen/testcases.072706/cbld/segfault1.v
// Using seed: 0xf0412068
// Writing cyclic circuit of size 79 to file '/w/bohrium/cloutier/Wmaint2/obj/Linux.debug/test/rangen/rangen/testcases.072706/cbld/segfault1.v'
// Module count = 8

// module rf
// size = 40 with 3 ports
module rf(rKG, e_QMl, j);
inout rKG;
input e_QMl;
inout j;

wire [2:16] rKG;
wire [2:5] e_QMl;
wire [0:1] j;
reg [8:28] UNZPm;
reg [4:23] QI;
reg [1:11] mVDI;
reg [1:2] M; // carbon observeSignal

reg [14:31] xVNc;
reg [13:56] QSgt;
wire [49:77] qX;
reg GEFZq;
reg [8:12] HzfjD;
wire [5:7] DcRQj; // carbon observeSignal

reg ItNG; // carbon observeSignal

reg xYf;
reg [0:5] Wah;
reg [45:85] ptoNL;
reg [0:0] Lp [3:5]; // memory
reg [19:27] GS;
reg iApT;
reg [4:6] oBM;
reg u;
wire [112:116] Ye;
reg UtUa; // carbon observeSignal

reg [3:6] dfOAJ;
reg DdeJ;
reg [6:26] dMB_;
reg [7:9] ZKJ;
always @(negedge &QSgt[31:47])
begin :Go
reg [4:13] BuWWy;
reg [17:18] _t;
reg [4:7] CDGnM; // carbon observeSignal


begin
end
end

always @(negedge &dMB_[18:23])
begin
begin
dfOAJ[4:6] = xVNc[22]^rKG[10:16];
end
end

always @(posedge DdeJ)
begin
begin
dMB_[10:17] = GEFZq;
end
end

always @(negedge &ptoNL[68:74])
begin :FLfWr
reg [2:4] oNa; // carbon observeSignal

reg Jl;

begin
dfOAJ[4:6] = ~(~(8'b01110100) + (dfOAJ[5:6]));
end
end

assign rKG[2:9] = (dfOAJ[5:6] != 2'b01) ? ptoNL[68:74] : 8'bz;

always @(posedge &dfOAJ[5:6])
begin
begin
u = j[1];
end
end

assign rKG[2:9] = ~dfOAJ[4:6];

always @(xVNc[22])
if (~xVNc[22])
begin
xVNc[16:29] = (~oBM[4:6]);
end

always @(&ptoNL[68:74])
if (ptoNL[68:74])
begin
end
else
begin
QI[11:14] = ~u;
end

always @(&rKG[6:9])
if (~rKG[6:9])
begin
Lp[3]/* memuse */ = QI[11:14];
end
else
begin
end

always @(negedge DdeJ)
begin
begin
QI[5] = ~(Lp[3]/* memuse */);
end
end

always @(negedge &dfOAJ[5:6])
begin
begin
QSgt[15:53] = DdeJ;
mVDI[2:5] = HzfjD[8:11];
end
end

PqmQt xPUC (u, {Lp[4]/* memuse */,ItNG}, {DdeJ,dMB_[18:23],HzfjD[8:11]}, ItNG);

always @(negedge &dMB_[10:17])
begin
begin
end
end

assign Ye[112:115] = xVNc[22];

always @(negedge &rKG[9:12])
begin :H
reg L;
reg tj;
reg [5:6] eH;

begin
mVDI[2:5] = dfOAJ[4:6]^ptoNL[68:74];
end
end

uzvii t (/* unconnected */, {GEFZq,dfOAJ[4:6],rKG[9:12],GEFZq,rKG[9:12],GEFZq,Lp[4]/* memuse */,1'b0}, rKG[10:16], /* unconnected */, {DdeJ,e_QMl[3:5],mVDI[2:5],rKG[5:14],ptoNL[68:74],mVDI[2:5],HzfjD[8:11],oBM[4:6],e_QMl[3:5],j[1],87'b011111101101000000010010110100001101101000001111110000001111000111101011001011110111000}, {Lp[4]/* memuse */,Lp[3]/* memuse */,DdeJ,xVNc[22],j[1],1'b1}, GEFZq, Lp[4]/* memuse */);

always @(negedge j[1])
begin
begin
HzfjD[9:10] = ~dfOAJ[5:6];
end
end

always @(&xVNc[16:29])
if (~xVNc[16:29])
begin
DdeJ = ~rKG[10:16]^DdeJ;
end

always @(posedge QI[5])
begin
begin
QI[5] = Lp[3]/* memuse */;
end
end

always @(&HzfjD[8:11])
if (~HzfjD[8:11])
begin
DdeJ = GEFZq;
end

xb R ({HzfjD[8:11],Lp[4]/* memuse */}, /* unconnected */);

always @(posedge &QSgt[15:53])
begin :f
reg bQL; // carbon observeSignal


begin
end
end

PqmQt JLi (QI[5], dfOAJ[5:6], {dMB_[10:17],u,Lp[4]/* memuse */,GEFZq}, Lp[3]/* memuse */);

always @(&rKG[9:12])
if (rKG[9:12])
begin
Wah[0] = ~mVDI[2:10]&DdeJ;
end
else
begin
Lp[3]/* memuse */ = ~rKG[9:12];
end

always @(posedge &dMB_[10:17])
begin
begin
case (dMB_[18:23])
6'd0:
begin
HzfjD[9:10] = rKG[6:9];
end
6'd1:
begin
HzfjD[9] = {rKG[6:9],HzfjD[9:10]};
end
6'd2:
begin
HzfjD[9:10] = ~e_QMl[3:5];
end
6'd3:
begin
Lp[3]/* memuse */ = QI[11:14]&oBM[4:6];
end
6'd4:
begin
ptoNL[62:76] = ~dMB_[18:23];
end
6'd5:
begin
UtUa = (ptoNL[62:76]^rKG[9:12]);
end
6'd6:
begin
end
6'd7:
begin
dMB_[21:23] = 9'b001100011;
end
6'd8:
begin
iApT = (~QI[11:14]^j[1]);
end
6'd9:
begin
iApT = ~xVNc[22];
end
default:
begin
dMB_[21:23] = ~DdeJ|j[1];
end
endcase
UtUa = dfOAJ[4:6];
end
end

rx pij (rKG[2:9], {j[0:1],DcRQj[5:6],DcRQj[5:6]}, rKG[10:16]);

always @(&oBM[4:6])
if (oBM[4:6])
begin
oBM[4:5] = ~(rKG[9:12]) + (~HzfjD[9]|dMB_[21:23]);
end

always @(xVNc[22])
if (xVNc[22])
begin
QI[5] = dMB_[21:23];
end

assign Ye[112:115] = ~{j[1],rKG[6:9]};

always @(posedge Lp[4]/* memuse */)
begin
begin
DdeJ = ~dMB_[10:17];
end
end

always @(&rKG[5:14])
if (rKG[5:14])
begin
QI[11:14] = DdeJ^iApT;
end
else
begin
dMB_[13:19] = {Lp[3]/* memuse */,Lp[4]/* memuse */};
end

endmodule

// module uzvii
// size = 39 with 8 ports
module uzvii(Iy, uc, eSA, r, JlQ, HdxaL, gs, SCQd);
inout Iy;
input uc;
input eSA;
inout r;
input JlQ;
input HdxaL;
input gs;
input SCQd;

wire Iy;
wire [7:22] uc;
wire [0:6] eSA;
wire r;
wire [0:126] JlQ; // carbon observeSignal

wire [25:30] HdxaL;
wire gs;
wire SCQd;
reg [5:31] WNtH;
reg [1:5] FgD;
reg m;
wire [3:7] HZFl;
reg [7:7] GECbD;
reg [9:18] DMIQ;
reg [0:0] PqKE [19:27]; // memory
reg [0:4] Eg;
reg Lt; // carbon observeSignal

wire [7:27] d;
reg [1:5] hXx;
wire b;
wire rFt;
reg [18:23] no;
reg AlSY;
reg S_Q;
wire BK;
reg [6:6] Q_ [73:99]; // memory
reg edyTn;
reg bc;
reg [12:27] U;
reg ynu;
reg Klmt;
reg [7:18] JU; // carbon observeSignal

wire fB;
reg [0:1] hzs;
reg Q;
wire [6:7] ruWT;
reg pQW;
wire [0:2] _rm;
wire [1:7] OONC;
assign HZFl[4:7] = 10'b1101010001;

always @(Iy)
if (Iy)
begin
no[19:23] = 10'b0011101111;
end
else
begin
S_Q = d[14:16];
Klmt = ~no[18:23]&pQW;
end

always @(negedge &DMIQ[10:12])
begin
begin
end
end

always @(&JlQ[43:52])
if (JlQ[43:52])
begin
no[19:23] = (Iy);
end

always @(posedge SCQd)
begin
begin
end
end

always @(negedge &d[16:22])
begin
begin
bc = Q;
end
end

always @(posedge &JlQ[43:76])
begin
begin
no[19:23] = ~(~bc&r);
end
end

always @(Q)
if (Q)
begin
for (edyTn=1'b1; edyTn<1'b0; edyTn=edyTn+1'b0)
begin
hXx[2:5] = _rm[0:2]^_rm[0:2];
end
end

always @(posedge &JlQ[43:52])
begin
begin
pQW = _rm[0:2];
end
end

always @(negedge _rm[2])
begin
begin
JU[7:13] = ~(~JlQ[43:52]|bc);
end
end

assign OONC[1:3] = pQW|HdxaL[25:30];

rx tO ({OONC[6:7],OONC[6:7],HZFl[4:7]}, {b,HZFl[4:7],BK}, {pQW,bc,_rm[0:2],U[27],pQW});

always @(posedge &d[11:19])
begin
begin
PqKE[19]/* memuse */ = hXx[2:5];
end
end

always @(&no[19:23])
if (~no[19:23])
begin
case (PqKE[25]/* memuse */)
1'd0:
begin
S_Q = ~hXx[2:5];
end
1'd1:
begin
PqKE[19]/* memuse */ = ~4'b1110;
end
endcase
end

always @(negedge &JlQ[28:53])
begin
begin
no[21] = ~uc[17:19]^PqKE[27]/* memuse */;
end
end

always @(posedge PqKE[25]/* memuse */)
begin :Av
reg I;
reg Jygc;
reg [6:26] VF;

begin
PqKE[25]/* memuse */ = JlQ[43:76];
end
end

PqmQt VsDVT (BK, {PqKE[27]/* memuse */,Iy}, {eSA[1:5],d[14:16],DMIQ[10:12]}, S_Q);

always @(negedge &eSA[1:5])
begin
begin
no[21:23] = (~bc);
end
end

always @(posedge &JU[7:13])
begin
begin
PqKE[19]/* memuse */ = pQW;
end
end

assign Iy = ~d[16:22]&no[19:23];

always @(negedge &HdxaL[25:30])
begin
begin
Q_[96]/* memuse */ = ~5'b10101;
end
end

always @(negedge &JU[7:13])
begin :jTlAW
reg [4:5] i;
reg [17:22] S;
reg Nfq;

begin
ynu = ~(~(~(PqKE[21]/* memuse */)));
end
end

assign _rm[2] = (BK&S_Q);

always @(posedge &JlQ[43:76])
begin
begin
Q_[97]/* memuse */ = PqKE[25]/* memuse */^DMIQ[10:12];
end
end

always @(posedge Iy)
begin
begin
no[21:23] = ~PqKE[19]/* memuse */;
end
end

always @(negedge &d[18:27])
begin
begin
edyTn = {bc,JU[7:13]};
end
end

always @(PqKE[25]/* memuse */)
if (~PqKE[25]/* memuse */)
begin
end

assign fB = d[11:19]^edyTn;

always @(posedge BK)
begin
begin
GECbD[7] = ~Q;
end
end

always @(&no[21:23])
if (~no[21:23])
begin
case (PqKE[19]/* memuse */)
1'd0:
begin
no[21] = ~pQW|pQW;
end
1'd1:
begin
AlSY = ~PqKE[26]/* memuse */;
end
endcase
end

zAMuu HA ({PqKE[25]/* memuse */,rFt,Q,PqKE[25]/* memuse */}, {no[21:23],no[21:23],ynu,DMIQ[10:12],DMIQ[10:12],no[19:23],rFt,SCQd,fB,PqKE[19]/* memuse */,6'b101100}, {Iy,Iy,r,_rm[2],Iy}, {Iy,rFt,Iy,r,OONC[1:3],OONC[1:3],BK,b,HZFl[4:7],r,OONC[6:7],BK,HZFl[4:7],ruWT[6:7],OONC[1:3],OONC[1:3],BK});

endmodule

// module zAMuu
// size = 4 with 4 ports
module zAMuu(nDhs, fhJY, LU, uefyi);
input nDhs;
input fhJY;
output LU;
output uefyi;

wire [3:6] nDhs;
wire [51:78] fhJY;
reg [0:4] LU;
reg [4:37] uefyi;
reg L;
wire [2:16] hRNsS;
always @(&uefyi[23:25])
if (uefyi[23:25])
begin
LU[0:1] = ~(~uefyi[23:25]) + (~uefyi[23:25]);
end

assign hRNsS[9] = ~(LU[0:1]^uefyi[13:37]) + (~uefyi[23:25]);

always @(negedge &LU[0:1])
begin
begin
LU[0:1] = ~uefyi[21:32];
end
end

always @(posedge &fhJY[59:76])
begin :ZfRdj
reg Ztem; // carbon observeSignal


begin
Ztem = ~uefyi[23:25];
end
end

endmodule

// module xb
// size = 2 with 2 ports
module xb(QFie, GjGe);
input QFie;
output GjGe;

wire [2:6] QFie;
reg GjGe;
reg [9:10] PKgd;
reg [5:6] WoVmv;
wire [40:104] YT; // carbon observeSignal

always @(posedge &QFie[4:6])
begin
begin
PKgd[9:10] = QFie[4:6];
end
end

assign YT[83:102] = ~YT[50:83];

endmodule

// module rx
// size = 5 with 3 ports
module rx(acCB, nT, Dj);
output acCB;
output nT;
input Dj;

wire [2:9] acCB;
reg [2:7] nT;
wire [1:7] Dj;
reg [4:29] OMtQ;
always @(posedge &Dj[2:5])
begin
begin
nT[5:6] = ~Dj[2:5]&Dj[2:5];
end
end

always @(negedge &Dj[2:5])
begin :zj
reg [2:7] MNgIM [5:10]; // memory

begin
end
end

always @(posedge &nT[5:6])
begin :iEfCQ
reg [8:26] IoUic; // carbon observeSignal

reg [0:2] aGhvc;

begin
IoUic[17:21] = Dj[2:5];
end
end

always @(&nT[5:6])
if (~nT[5:6])
begin
OMtQ[10] = (~8'b10111000);
end

assign acCB[7:8] = ~nT[5:6]^Dj[2:5];

always @(negedge OMtQ[10])
begin
begin
OMtQ[11:26] = Dj[2:5];
end
end

endmodule

// module PqmQt
// size = 3 with 4 ports
module PqmQt(VKl, cEmx, x, SS);
input VKl;
input cEmx;
input x;
input SS;

wire VKl;
wire [3:4] cEmx;
wire [15:25] x;
wire SS;
reg YA;
reg RtmEb;
reg [1:6] YI; // carbon forceSignal

reg fhz; // carbon observeSignal

reg [12:27] KMpS [1:31]; // memory
wire [43:119] jBTB;
always @(negedge VKl)
begin
begin
RtmEb = ~jBTB[86:91];
end
end

always @(posedge RtmEb)
begin
begin
end
end

always @(&x[24:25])
if (x[24:25])
begin
KMpS[21]/* memuse */ = x[24:25]&jBTB[69:101];
end

assign jBTB[77:86] = {SS,jBTB[83:91]};

endmodule
