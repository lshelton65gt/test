module top (out1, out2, clk, sel, in1, in2);
   output out1, out2;
   input  clk, in1, in2;
   input [1:0] sel;

   reg 	  out1, out2;
   always @ (posedge clk)
     out1 = in1 & in2;

   reg 	  mux1;
   always @ (sel or in1 or in2)
     casex (sel)
       2'bx1: mux1 = in1;
       2'b1x: mux1 = in2;
       2'b00: mux1 = 0;
     endcase

   always @ (negedge clk)
     out2 = mux1;

endmodule // top
