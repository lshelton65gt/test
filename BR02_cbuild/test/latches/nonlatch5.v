module top(clk,sel,out);
  input clk;
  input [1:0] sel;
  output [1:0] out; 
  reg [1:0] out;

  always @(sel)
    case (sel)
      //synopsys parallel_case full_case
      2'b00: out = 2'b01;
      2'b10: out = 2'b11;
      2'b11: out = 2'b00;
    endcase
endmodule
