// This found a bug in doing set_different on NUNetSet's
module top (aout, zout, fout, dout, cout, in1, in2, in3, in4, in5);
   output aout, zout, fout, dout, cout;
   input  in1, in2, in3, in4, in5;

   reg 	  aout, zout, fout, dout, cout;
   always @ (in1 or in2 or in3 or in4 or in5)
     begin
	aout = 0;
	zout = 0;
	fout = 0;
	dout = 0;
	cout = 0;
	case (in3)
	  1'b0:
	    begin
	       cout = in3;
	       zout = in4;
	       if (in3)
		 aout = in5;
	       else
		 aout = in2;
	       fout = in1;
	       case (in1 | in2)
		 1'b0: dout = in2;
		 1'b1: dout = in1;
	       endcase // case(in1 | in2)
	    end // case: default
	endcase // case(in3)
     end // always @ (in1 or in2 or in3 or in4 or in5)

endmodule // top

