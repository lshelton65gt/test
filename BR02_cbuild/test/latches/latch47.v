// multiple instances of a simple exclusive latch pair
module top (out1, out2, out3, out4, clk);
   output out1, out2, out3, out4;
   input  clk;

   sub S1 (clk, out1, out2);
   sub S2 (clk, out3, out4);

endmodule

module sub(clk, l1, l2);
input clk;
output l1, l2;

reg l1, l2;

initial l1 = 0;
initial l2 = 0;

always @(clk or l2)
begin
  if (clk)
    l1 = l2;
end

always @(clk or l1)
begin
  if (~clk)
    l2 = ~l1;
end

endmodule
