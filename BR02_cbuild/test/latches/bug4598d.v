/*
 When the contained latch 'tmp' is converted, some of the generated
 blocks define all of 'tmp', some of the blocks define none of 'tmp',
 while other blocks define only 'tmp[0]'. This caused rewrite and flow
 inconsistency issues, as we were not repairing fanout sets.
*/
module top(ctl1,ctl2,out0,out1);
   input ctl1,ctl2;
   output out0,out1;
   reg [1:0] 	tmp;

   initial tmp=0;
   always @(ctl1 or ctl2) begin
      if (ctl1) begin
	 tmp[0] = 1'b0;
      end else begin
	 if (ctl2) begin
	    tmp[0] = 1'b1;
	 end else begin
	    tmp[1] = 1'b0;
	 end
      end
   end

   assign out0 = tmp[0];
   assign out1 = tmp[1];
   
endmodule
