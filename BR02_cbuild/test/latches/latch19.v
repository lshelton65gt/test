// l2 is exclusive but l1 is not (because of val).
// l1 is really a latch on en, and cond is a data selector
module top(clk, cond_in, en_in, l1, l2, val_in);
input clk, cond_in, en_in, val_in;
output l1, l2;

  reg l1, l2;
  reg cond, en, val;

  initial
  begin
    l1 = 0;
    l2 = 0;
    cond = 0;
    en = 0;
    val = 0;
  end

  always@(posedge clk)
  begin
    cond <= cond_in;
    en   <= en_in;
    val  <= val_in;
  end

always @ (cond or en or l2 or val)
begin
  if (cond)
  begin
    if (en) 
      l1 = l2;
  end
  else
  begin
    if (en)
      l1 = val;
  end
end

always @ (en or l1)
begin
  if (~en)
    l2 = ~l1;
end

endmodule
