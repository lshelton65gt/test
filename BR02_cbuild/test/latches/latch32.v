// l1 and l2 are both exclusive
// l1 can only be converted if x is rescoped and reordered
module top(clk, l1, l2);
   input clk;
   output l1, l2;

   reg    l1, l2;
   reg    x;

   initial begin
      l1 = 0;
      l2 = 0;
      x = 0;
   end

   always @ (clk or x or l2)
     begin
        if (clk)
          l1 = ~x;
        x = l2;
     end

   always @ (clk or l1)
     begin
        if (!clk)
          l2 = l1;
     end

endmodule
