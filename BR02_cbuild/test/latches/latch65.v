module top(clk, rst, idx, l1, out);
input clk, rst, idx;
output l1, out;

wire clk, rst;
wire [1:0] idx;
reg l1;
reg [7:0] mem [0:3];
wire out;

  always @(clk or idx)
    if (!clk)
      l1 = mem[idx];

  reg [7:0] val;
  always @(clk or idx or l1)
  begin
    val = {8{~l1}};
    if (clk)
      if (rst)
      begin
        mem[0] = 7'b0;
        mem[1] = 7'b0;
        mem[2] = 7'b0;
        mem[3] = 7'b0;
      end
      else
      begin
        mem[idx] = val;
      end
  end

  assign out = mem[idx];

endmodule
