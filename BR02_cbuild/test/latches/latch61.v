module top (out, clk, in, en);
   output out;
   input  clk, in, en;
   reg    out;
   reg    ren1, ren2;

   wire   gclk;
   initial ren1 = 0;
   initial ren2 = 0;
   always @ (ren1 or clk)
     if (~clk)
       ren2 <= ren1;
   assign gclk = ren2 & clk;

   always @ (posedge gclk)
     ren1 <= en;

   initial out = 0;
   always @ (posedge gclk)
     out <= in;

endmodule
