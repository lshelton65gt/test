// l1 is in a block that would require splitting
module top(clk, en, val, l1, l2, x);
   input clk, en, val;
   output l1, l2, x;

   reg    l1, l2, x;

   initial begin
      l1 = 0;
      l2 = 0;
      x = 0;
   end

   // Create flopped version of the inputs because of the way we deal
   // with clocks and input flow. When I didn't have this code and
   // just used clk, cond, and val directly, Aldec would see a glitch
   // because clk transitions 1ns after cond and we see them as
   // coincident.
   reg dclk, ren, rval;
   initial begin
      dclk = 0;
      rval = 0;
      ren = 0;
   end
   always @ (posedge clk)
     begin
        dclk <= ~dclk;
     end
   always @ (posedge clk)
     begin
        rval <= val;
        ren <= en;
     end


   always @ (dclk or l2 or ren or rval)
     begin
        if (!dclk)
          begin
             l1 = l2;
             if (ren)
               x = rval;
          end
     end

   always @ (dclk or l1)
     if (dclk)
       l2 = ~l1;

endmodule

