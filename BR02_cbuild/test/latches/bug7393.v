// last mod: Fri Jun  1 13:21:03 2007
// filename: test/latches/bug7393.v
// Description:  This test duplicates the issue in bug 7393,
// the issue here is that latch analysis (ControlTree creation)
// incorrectly thinks that the two unrolled if statements are
// for the same def bits due to memory net ref pessimism.

module bug7393(clock, in1, out1);
   input clock;
   input [1:0] in1;
   output [1:0] out1;
   reg [1:0] out1;
   reg [1:0] mask [1:1];
   integer   i;

   always @ (in1)
     begin
	for (i = 1; i >= 0; i = i - 1)
	   begin
	      if (i < in1)
		mask[1][i] = 1'b1;
	      else
		mask[1][i] = 1'b0;
	   end
     end

   
   always @(posedge clock)
     begin: main
	out1 = mask[1];
     end
endmodule
