module LQE(hp);
output hp;

reg [9:28] hp;

reg [27:119] U_WNR;
reg nY;
reg [0:0] sohFf [2:7]; // memory
reg [17:66] B;
reg [4:19] bflP;
reg lb;
reg [4:5] qv;
reg [19:31] r_e;
reg [2:17] Me;
reg [18:100] kaxu;
reg [0:3] dy [0:4]; // memory

always @(negedge kaxu[66])
  hp[17:21] = 4'b1010;

always @(qv[4])
  if (~qv[4])
    r_e[22] = 1'b0;

always @(posedge lb)
  kaxu[28:81] = 5'b11100;

always @(posedge &bflP[5:14])
  bflP[5:14] = 5'b10010;

always @(Me[9])
if (Me[9])
begin
lb <= ~bflP[4:5];
dy[1] = 1'b0;
end
else
begin
sohFf[4] = dy[1];
end

always @(sohFf[4])
if (sohFf[4])
begin
for (U_WNR[100:109]=10'b0100001110; U_WNR[100:109]>10'b0011010100; U_WNR[100:109]=U_WNR[100:109]+10'b1101111110)
begin
nY = ~(~B[21:53]);
end
end

always @(posedge r_e[22])
  bflP[10:14] = ~U_WNR[104:113];

always @(negedge r_e[22])
  Me[9] = 1'b1;

endmodule
