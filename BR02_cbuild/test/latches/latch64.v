module top (out, clk, in, en);
   output out;
   input  clk, in, en;

   // Register the inputs so that they arrive at the same time
   reg rin, dclk, ren;
   initial rin = 0;
   initial dclk = 0;
   initial ren = 0;
   always @ (posedge clk)
     dclk <= ~dclk;
   always @ (posedge clk)
     begin
        rin <= in;
        ren <= en;
     end

   reg    l1, l2;
   initial l1 = 0;
   initial l2 = 0;

   always @ (dclk or ren or rin)
     if (dclk & ren)
       l1 <= rin;
   always @ (dclk or l1)
     if (dclk || !ren)
     begin
     end
     else
       l2 <= l1;

   reg    out;
   initial out = 0;
   always @ (l2 or dclk)
     if (dclk)
       out <= l2;

endmodule
