// latches l1 and l2 are exclusive, but the intervening
// assignment to x in the same block complicates things.
module top(clk, l1, l2);
input clk;
output l1, l2;

  reg l1, l2, x;
   initial begin
      l1 = 0;
      l2 = 0;
      x = 1;
   end

  always @(clk or l2)
  begin
    if (clk)
      l1 <= l2;
    x = ~l1;
  end

  always @(clk or x)
    if (!clk)
      l2 = x;

endmodule
