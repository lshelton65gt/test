-- last mod: Fri Jun  1 11:20:54 2007
-- filename: test/latches/bug7366_tiny.vhdl
-- Description:  This test duplicates the problem seen in bug 7366
-- the issue here is/was that for some reason mask is recognized as a latch,
-- and we incorrectly use a delayed value of in1 to set the values of
-- mask.  See the test test/latches/bug7366.vhdl for a larger version
-- of this test.


library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity bug7366_tiny is
  port (
    clock    : in  std_logic;
    in1  : in  std_ulogic_vector(1 downto 0);
    out1 : out std_ulogic_vector(1 downto 0));
end;

architecture arch of bug7366_tiny is
  type t_array is array (1 to 1) of std_ulogic_vector(1 downto 0);
  signal mask : t_array :=  ( others => (others => '0'));

begin
    p_RapMask : process (in1)
    begin
      for i in 1 downto 0 loop
        if i < to_integer(unsigned(in1)) then
          mask(1)(i) <= '1';
        else
          mask(1)(i) <= '0';
        end if;
      end loop;
    end process p_RapMask;


  main: process (clock)
  begin
    if clock'event and clock = '1' then 
       out1 <= mask(1);
    end if;
  end process;
  
end;
