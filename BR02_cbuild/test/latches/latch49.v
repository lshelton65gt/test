// l1 and l2 are exclusive, but the BDDs must be built
// for expressions all the way back to clk and en to see
// that they are exclusive.  Stopping at clk2 hides the
// relationship between clk2 and clk and en.
module top (clk, en, l1, l2);
input clk, en;
output l1, l2;

   // Create flopped version of the inputs because of the way we deal
   // with clocks and input flow. If the inputs are used directly,
   // Aldec would see a glitch because clk transitions are delayed 1ns.
   reg dclk, ren;
   initial begin
      dclk = 0;
      ren = 0;
   end
   always @ (posedge clk)
     dclk <= ~dclk;
   always @ (negedge clk)
     ren <= en;

clkgen cgen (dclk, ren, clk1, clk1n, clk2, clk2n);
latchB L1 (clk1n, ren, l1, ~l2);
latchA L2 (clk2, l2, l1);

endmodule

module clkgen(clk, en, clk1, clk1n, clk2, clk2n);
input clk, en;
output clk1, clk1n, clk2, clk2n;

  assign clk1  = ~clk;
  assign clk1n = ~clk1;
  assign clk2  = ~(clk & en);
  assign clk2n = ~clk2;

endmodule

module latchA(clk, q, d);
input clk, d;
output q;

reg q;

initial q = 0;

always @(clk or d)
begin
  if (clk)
    q = d;
end

endmodule

module latchB(clk, en, q, d);
input clk, en, d;
output q;

reg q;

initial q = 0;

always @(clk or en or d)
begin
  if (clk & en)
    q = d;
end

endmodule
