module top (out1, out2, in1, in2, sel);
   output out1, out2;
   input  in1, in2, sel;

   reg 	  out1, out2;
   always @ (in1 or in2 or sel)
     case (sel)
       1'b1: begin
	  out1 = in1;
	  out2 = in2;
       end
       1'b0: begin
	  out1 = in2;
	  out2 = in1;
       end
       default: out2 = 0;
     endcase // case(sel)

endmodule // top
