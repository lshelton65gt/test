// l1 and l2 are both exclusive
// but they would require the block to be split
module top(clk, l1, l2);
input clk;
output l1, l2;

reg l1, l2;

   initial begin
      l1 = 0;
      l2 = 1;
   end

always @ (clk or l2 or l1)
begin
  if (clk)
    l1 = l2;
  else
    l2 = ~l1;   
end

endmodule
