module top (out1, out2, clk, in);
   output out1, out2;
   input  clk, in;

   reg 	  out1, out2;
   always @ (posedge clk)
     out1 = in;

   reg 	  latch1;
   always @ (clk or in)
     if (clk)
       latch1 = in;

   always @ (negedge clk)
     out2 = latch1;

endmodule // top
