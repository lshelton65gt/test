// l1 and l2 are both exclusive
// l1 can only be converted if x is split from l1
// or reordered and rescoped (not done today)
module top(clk, rst, l1, l2);
input clk, rst;
output l1, l2;

reg l1, l2;
reg x;

   initial begin
      l1 = 0;
      l2 = 0;
      x = 0;
   end

always @ (rst or clk or x or l2)
begin
  if (rst)
    l1 = 0;
  else
  begin
    if (clk)
      l1 = ~x;
    x = l2;
  end
end

always @ (rst or clk or l1)
begin
  if (rst)
    l2 = 0;
  else if (!clk)
    l2 = l1;
end

endmodule
