// This is the same as clkgate2 except the AND gate order is reversed

module top (out, clk, en, in);
   output out;
   input  clk, en, in;

   reg    ren;
   reg    out;
   wire   gclk;
   
   // Sync the input enable
   reg    sen;
   always @ (posedge clk)
     sen <= en;

   always @ (clk or sen)
     if (clk)
       ren <= sen;
   assign gclk = ren & ~clk;

   initial out = 0;
   always @ (posedge gclk)
     out <= in;

endmodule
