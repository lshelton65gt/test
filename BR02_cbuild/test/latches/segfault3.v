module top(en, d);
input en;
output d;

  reg d;
  reg tmp_en;
  reg tmp_d;

  // this latch is really a NOP, pruning causing it go
  // away entirely, so we must handle when pruneUse() returns
  // false and not use the ControlTree anymore.

  always @(en or d)
  begin
    tmp_en = en;
    tmp_d = d;
    if (tmp_en)
      tmp_d = d;
    d = tmp_d;
  end

endmodule
