// This found a bug in making temps into latches.
module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   reg 	  out1, out2;
   always @ (posedge clk)
     out1 = in1 & in2;

   reg 	  mux1;
   always @ (clk or in1 or in2)
     begin
	mux1 = 0;
	if (clk)
	  case (in1 | in2)
	    1'b0: mux1 = in1;
	    1'b1: mux1 = in2;
	  endcase // case(in1 | in2)
     end

   always @ (negedge clk)
     out2 = mux1;

endmodule // top

