module top (out, clk, in);
   output [1:0] out;
   input 	clk;
   input [1:0] 	in;

   reg [1:0] 	comb1;
   always @ (in)
     begin
	comb1[0] = ~in[0];
	comb1[1] = in[1];
     end

   reg [1:0] out;
   always @ (posedge clk)
     out = comb1;

endmodule // top
