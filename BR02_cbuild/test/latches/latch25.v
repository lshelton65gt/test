// l1 is exclusive but l2 is not
// l1 uses temps in latch assignment
module top(clk, rst, l1, l2);
input clk, rst;
output l1, l2;

reg l1, l2, tmp;

   initial begin
      l1 = 0;
      l2 = 1;
   end

always @ (l1 or rst or clk or l2)
begin
  tmp = l1;
  if (rst)
    tmp = 0;
  else if (clk)
    tmp = l2;
  l1 = tmp;
end

always @ (clk or l1)
begin
  if (~clk)
    l2 = ~l1;
end

endmodule
