// l1, l2 and l3 are all exclusive
module top(clk, cond_in, en1_in, en2_in, l1, l2, l3);
input clk, cond_in, en1_in, en2_in;
output l1, l2, l3;

reg l1, l2, l3;
reg cond, en1, en2;

  initial begin
    l1 = 0;
    l2 = 1;
    l3 = 0;
    cond = 0;
    en1 = 0;
    en2 = 0;
  end

  always @(posedge clk)
  begin
    cond <= cond_in;
    en1 <= en1_in;
    en2 <= en2_in;
  end

always @ (cond or en1 or l2 or l3 or en2)
begin
  if (cond)
  begin
    if (en1) 
      l1 = ~l2;
    else
      l1 = ~l3;
  end
  else
  begin
    if (en2)
      l1 = l2;
  end
end

always @ (en2 or cond or l1)
begin
  if (~en2)
    if (~cond)
      l2 = ~l1;
end

always @ (cond or en1 or l2)
begin
  if (cond)
    if (en1)
      l3 = ~l2;
end

endmodule
