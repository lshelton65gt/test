// l1 and l2 are both exclusive
// this tests handling of latches of with complex enable expressions
module top(clk, clk2, en, l1, l2);
input clk, clk2, en;
output l1, l2;

reg l1, l2;

   initial begin
      l1 = 0;
      l2 = 0;
   end

   // Create flopped version of the inputs because of the way we deal
   // with clocks and input flow. When I didn't have this code and
   // just used clk, cond, and val directly, Aldec would see a glitch
   // because clk transitions 1ns after cond and we see them as
   // coincident.
   reg dclk, dclk2, ren;
   initial begin
      dclk = 0;
      dclk2 = 0;
      ren = 0;
   end
   always @ (posedge clk)
     begin
        dclk <= ~dclk;
     end
   always @ (posedge clk)
     begin
        ren <= en;
     end
   always @ (negedge clk)
     begin
        dclk2 <= clk2;
     end

always @ (dclk or ren or l2 or dclk2)
begin
  if (dclk & ren)
    l1 = ~l2;
  else if (dclk2)
    l1 = l2;
end

always @ (dclk2 or ren or l1)
begin
  if (!dclk2 & !ren)
    l2 = ~l1;
end

endmodule
