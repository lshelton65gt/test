// l1 and l2 do not feed each other, so don't need to be converted
module top(clk, en, val, l1, l2);
input clk, en, val;
output l1, l2;

reg l1, l2, f;

   initial begin
      l1 = 0;
      l2 = 1;
      f = 0;
   end

always @ (en or val)
  if (en)
    l1 = val;

always @(posedge clk)
  f = l1;

always @ (clk or f)
  if (~clk)
    l2 = ~f;

endmodule
