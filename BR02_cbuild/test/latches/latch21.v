// l2 is exclusive but l1 is not (because of val)
module top(clk, cond, l1, l2, val);
input clk, cond, val;
output l1, l2;

reg l1, l2;

   initial begin
      l1 = 0;
      l2 = 0;
   end

   // Create flopped version of the inputs because of the way we deal
   // with clocks and input flow. When I didn't have this code and
   // just used clk, cond, and val directly, Aldec would see a glitch
   // because clk transitions 1ns after cond and we see them as
   // coincident.
   reg dclk, rcond, rval;
   initial begin
      dclk = 0;
      rcond = 0;
      rval = 0;
   end
   always @ (posedge clk)
     begin
        dclk <= ~dclk;
     end
   always @ (posedge clk)
     begin
        rcond <= cond;
        rval <= val;
     end
   

always @ (dclk or rcond or rval or l2)
begin
  if (dclk)
  begin
    if (rcond) 
      l1 = rval;
    else
      l1 = l2;
  end
end

always @ (dclk or l1)
begin
  if (~dclk)
    l2 = ~l1;
end

endmodule
