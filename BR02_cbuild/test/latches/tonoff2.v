module xyz(out, in, sel, lout);
  output out;
  input  in;
  input [2:0] sel;
  output [3:0] lout;
  reg [3:0]    lout;

// synopsys translate_off
  
  assign out = ~in;
// synopsys translate_on

  always @(sel) begin
    case (sel) // synopsys full_case
      3'b000: lout = 2;
      3'b001: lout = 1;
      3'b010: lout = 0;
    endcase
  end
endmodule
