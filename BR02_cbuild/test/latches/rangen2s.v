// Generated with: ./rangen -s 154 -n 0x3a61002d -o /w/xenon/rsayde/build/randbugs/obj/Linux.product/test/rangen/rangen/testcases.053107/cbld/assert4.v
// Using seed: 0x3a61002d
// Writing cyclic circuit of size 154 to file '/w/xenon/rsayde/build/randbugs/obj/Linux.product/test/rangen/rangen/testcases.053107/cbld/assert4.v'
// Module count = 7

// The way latch conversion works, it does more aggresive
// expression folding during conversion than during clock
// analysis. This means that during the conversion process we
// could determine that two latch clocks are the same constant
// and replace it with the same net. The result is that when we
// replace the latch conditions with constants, it can find the
// same clock expression in the translation map. But it must
// always be replaced with the same value.
//
// The fix was to replace the restrictive assert with a check
// to make sure if we have a duplicate we replace it with the
// same constant value.
//
// This is a narrowed down version of the original bug.

// module T0O
// size = 154 with 9 ports
module T0O(Day2, jn3Hh, iJ_n, V, TJ, Y, _, ck, r);
   input Day2;
   input jn3Hh;
   input iJ_n;
   input V;
   input TJ;
   input Y;
   input _;
   inout ck;
   output r;

   wire [2:6] Day2;
   wire       jn3Hh;
   wire       iJ_n;
   wire [23:48] V;
   wire [2:7]   TJ;
   wire [3:5]   Y;
   wire [2:5]   _;
   wire [2:4]   ck;
   reg          r;
   wire [20:22] e;
   reg [9:9]    pWnQ;
   reg [6:6]    BXE;
   reg          RNG;
   reg [29:30]  g;
   wire         olkSJ;
   reg [47:89]  fCT;
   reg [4:6]    eagb;
   reg          Ik;
   reg [2:11]   WoRl;
   reg          xQEG5;
   reg          G2J5T;
   reg [15:89]  oGs9;
   reg [2:5]    d [5:7]; // memory
   reg [3:5]    BWgE;
   reg          f;
   wire [15:19] OGbM;
   reg [4:11]   S13X;
   reg [8:16]   UKLDs;
   wire [10:14] BCEL;
   reg          Hu;
   reg [1:1]    tqcE;
   reg [27:30]  ul;
   reg [9:23]   h;
   reg [28:30]  WTal;
   wire         L;
   reg [0:0]    vfS0;
   wire         CvBWC;
   reg          WfTj;
   wire         t;
   reg [2:2]    f9ypR;
   reg [1:3]    ok;
   reg [0:7]    rNct2;
   wire         YfFdG;
   reg [53:89]  mQ;
   reg          iAu;
   reg [10:31]  RJlv;
   reg          F;
   reg [4:6]    W6ZS;
   reg          uv;
   reg [12:73]  x [5:5]; // memory
   reg [11:16]  DV;
   reg [10:31]  l2k [1:5]; // memory
   reg [0:7]    OCN;
   reg          QV;
   wire [0:2]   kbN;
   wire         NQ;
   reg [17:24]  j3rVM [85:87]; // memory
   wire         RpL;
   reg [3:12]   AbtNt;
   reg [0:2]    WgJV;
   wire         cOh;
   reg          b;
   reg          pQCW;
   wire         cU;
   reg [45:93]  oPp;
   reg [4:6]    Kyz;
   reg [56:76]  fhZ;
   reg          P;
   reg          ms4tg;
   reg [22:27]  l1u;
   reg [4:10]   uqntR;
   wire         Njg;
   reg          eWotFi;
   reg [26:106] rt2k;
   reg [1:2]    C [0:2]; // memory
   reg [4:6]    OcRA;
   reg [22:29]  z7HZ;
   reg [3:4]    z;
   reg          Mh2Et;
   wire         Ht;
   reg [2:2]    XdI;
   wire [0:1]   nzkxg;
   reg [0:0]    Q5V [3:30]; // memory
   reg          dgt1s;
   reg [3:4]    Zzzr;
   reg          tVqQ;
   reg [9:24]   j;
   wire [22:27] tNQ6;
   wire [1:18]  q0;
   reg [1:5]    a;
   reg [99:127] n1;
   wire         Fj;
   reg [16:26]  _AF;
   reg [5:31]   P4C;
   reg [21:27]  Xyx;
   reg [0:1]    oSg;
   wire [2:5]   ZNL67;
   wire [12:108] _VzM;
   reg           eo;
   reg [6:6]     nYBTU;
   wire [2:28]   SP8W4s;
   reg [2:6]     J;
   reg [0:6]     v;
   reg [1:3]     U;
   reg           EE5T;
   reg           B8mT;
   reg           v7qx;
   wire          pajC;
   wire          Y1PQ;
   wire [1:7]    e4v;
   reg [12:24]   V3;
   reg           av;
   reg [1:28]    DkcGh;
   reg [2:3]     B2;
   reg [2:4]     A_f_;
   reg           WgHf;
   reg [0:0]     gGB [1:20]; // memory
   reg [9:31]    fv;
   reg [5:6]     r5c_;
   reg [10:19]   PgO;
   reg           aZsm;
   wire          WTq;
   reg           jk6Mv;
   reg [16:23]   V4;
   reg           mI;
   wire [21:30]  sSdSA;
   reg [7:25]    CYYz1;
   reg [3:14]    BvFFv;
   wire          I;

   wire          WzkC;
   wire          vFtmH;
   reg           JNjJK;
   wire          ga;

   always @(posedge eo)
     begin
        begin
           { Q5V[7]/* memuse */, oGs9[26:53] } = ~DV[11:12];
        end
     end

   always @(Q5V[16]/* memuse */)
     if (~Q5V[16]/* memuse */)
       begin
          { f, rt2k[28:59], fCT[73:85], z7HZ[22:25], BWgE[5], fv[18:22], U[1:2], DV[11:12] } = ~Day2[3:4];
       end
     else
       begin
          if (SP8W4s[12:26] == 15'b101101110100010)
            begin
               rt2k[28:59] = ~_VzM[31:48];
            end
          pQCW = ~fCT[47:89];
       end

   assign _VzM[27:65] = ~nzkxg[1]^Y[3:4];

   wire   dOYa;
   wire   CbmQ;

   assign L = ~(~_VzM[93:102]) + ((~(~{aZsm,Zzzr[3:4]})));

   wire   Z = rNct2[2];
   always @(negedge Q5V[7]/* memuse */ or negedge Z)
     if (~Z)
       begin
          begin
             pQCW = (~dOYa^tqcE[1]);
             AbtNt[5:10] = ~aZsm;
          end
       end
     else
       begin
          begin
             $display("module: %s, value: %b",~sSdSA[28]|CYYz1[13:15])
             ;end
       end

   assign { ck[2], e[20], L, SP8W4s[16:21] } = (JNjJK^L);

   jERnR A8Ra ({gGB[4]/* memuse */,sSdSA[28],Kyz[4:5],f,1'b0}, {SP8W4s[13:16],ZNL67[3:5],cU,cU,BCEL[12:13],NQ,RpL}, _VzM[70:73]);

   assign ZNL67[3:5] = ~pQCW^_VzM[24:80];

   always @(&rt2k[28:59])
     if (~rt2k[28:59])
       begin
          $display("module: %s, value: %b",q0[1:4])
          ;end
     else
       begin
          r = ~pajC;
       end

   always @(&ZNL67[3:5])
     if (ZNL67[3:5])
       begin
          aZsm = (CbmQ);
       end

   assign { RpL, I, tNQ6[24:26], SP8W4s[13:16], OGbM[17:18], q0[7:11], CvBWC, _VzM[70:73], cOh, SP8W4s[17:25] } = ~(~(~oGs9[26:53]));

   assign SP8W4s[13:16] = ~_VzM[27:65]^fCT[47:89];

endmodule

// module jERnR
// size = 1 with 3 ports
module jERnR(Vhp, Fi7CE, E4ui);
   input Vhp;
   output Fi7CE;
   output E4ui;

   wire [1:6] Vhp;
   reg [3:15] Fi7CE;
   wire [4:7] E4ui;

   // GATED-CLOCK
   wire       x = &Fi7CE[9:15]; // clock
   wire       gt2e = Vhp[3]|Fi7CE[9:15]; // enable data
   reg        vDW;
   always @(x or gt2e) begin
      if (~x) begin
         vDW = gt2e; // enable assign
      end
   end
   wire hhHn = x & vDW; // gated clock
   always @(negedge hhHn)
     begin
        begin
           Fi7CE[7:9] = ((~Vhp[3]|gt2e));
        end
     end

endmodule
