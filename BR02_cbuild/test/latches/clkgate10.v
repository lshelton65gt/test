// This is the same as clkgate2 except the latch is an output port.
// So the conversion should not be done

module top (out, ren, clk, en, in);
   output out, ren;
   input  clk, en, in;

   reg    ren;
   reg    out;
   wire   gclk;
   
   // Sync the input enable
   reg    sen;
   always @ (posedge clk)
     sen <= en;

   always @ (clk or sen)
     if (clk)
       ren <= sen;
   assign gclk = ~clk & ren;

   initial out = 0;
   always @ (posedge gclk)
     out <= in;

endmodule
