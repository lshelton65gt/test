module top (out1, out2, clk, in);
   output out1, out2;
   input  clk, in;

   reg 	  out1, out2;
   always @ (posedge clk)
     out1 = in;

   wire   latch1;
   assign latch1 = clk ? in : latch1;

   always @ (negedge clk)
     out2 = latch1;

endmodule // top
