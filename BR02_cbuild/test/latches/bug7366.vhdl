-- last mod: Fri Jun  1 11:18:13 2007
-- filename: test/latches/bug7366.vhdl
-- Description:  This test duplicates the problem seen in bug 7366
-- the issue here is/was that for some reason s_RapMask is recognized as a latch,
-- and we incorrectly use a delayed value of in1/RapCntrl to set the values of
-- s_RapMask.  See the test test/latches/bug7366_tiny.vhdl for a simple version
-- of this test.

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity bug7366 is
  port (
    clock    : in  std_logic;
    in1, in2 : in  std_ulogic_vector(3 downto 0);
    out1,out2 : out std_ulogic_vector(9 downto 0));
end;

architecture arch of bug7366 is
  constant c_IsramAddrWidth                   : integer := 10;
  constant c_RapNum                           : integer := 2;
  constant c_RapCntrlWidth                    : integer                         := 4;

  type t_RapArray is array (c_RapNum downto 1) of std_ulogic_vector(c_IsramAddrWidth-1 downto 0);
  type t_RapCntrlArray is array (c_RapNum downto 1) of std_ulogic_vector(c_RapCntrlWidth-1 downto 0);
  signal s_RapMask         : t_RapArray;
  signal RapCntrl : t_RapCntrlArray;

begin
  g_Raps : for j in 1 to c_RapNum generate
    p_RapMask : process (RapCntrl)
    begin
      -- The following code synthesized smaller than the above
      for i in s_RapMask(j)'range loop
        if i < to_integer(unsigned(RapCntrl(j))) then
          s_RapMask(j)(i) <= '1';
        else
          s_RapMask(j)(i) <= '0';
        end if;
      end loop;
    end process p_RapMask;
  end generate g_Raps;


  main: process (clock)
  begin
    if clock'event and clock = '1' then 
       RapCntrl(1) <= in1;
       RapCntrl(2) <= in2;
      out1 <= s_RapMask(1);
      out2 <= s_RapMask(2);
    end if;
  end process;
  
end;
