// simple exclusive latch pair with synchronous reset
module top(clk, rst, l1, l2);
input clk, rst;
output l1, l2;

reg l1, l2;

   initial begin
      l1 = 0;
      l2 = 0;
   end

always @ (clk or rst or l2)
begin
  if (clk)
  begin
    if (rst) 
      l1 = 0;
    else
      l1 = l2;
  end
end

always @ (clk or l1)
begin
  if (~clk)
    l2 = ~l1;
end

endmodule
