// l1 is exclusive but l2 is not
// l1's control structure is complex
module top(clk, rst0, rst1, l1, l2);
input clk, rst0, rst1;
output l1, l2;

reg l1, l2;

/* The control structure for l1 consists of
   two separate control trees:
   
     rst0          clk
     / \             \
    /   \             \
  rst1   0       (rst1 | rst0)
    \                 /
     \               /
      1             l2

   which get combined together (the second takes precedence)
   to give:

                 clk
                /   \
               /     \
              / (rst1 | rst0)
             /       / \
            /       /   \
          rst0     l2   rst0
          / \           / \
         /   \         /   \
       rst1   0      rst1   0
         \             \
          \             \
           1             1

  We can remove the redundant rst1 test in the right hand branch,
  but we would like to do much better, optimizing this to:

            rst0
            / \
           /   \
         rst1   0
         / \
        /   \
      clk    1
        \
         \
         l2
 */

   initial begin
      l1 = 0;
      l2 = 1;
   end

always @ (rst0 or rst1 or clk or l2)
begin
  if (rst0)
    l1 = 0;
  else if (rst1)
    l1 = 1;

  if (clk)
    if (!rst1)
      if (!rst0)
        l1 = l2;
end

always @ (clk or l1)
begin
  if (~clk)
    l2 = ~l1;
end

endmodule
