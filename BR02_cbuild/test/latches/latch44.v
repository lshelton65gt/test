// this tests our analysis to reject latches
// with mismatched widths (equivalent to partial-net latches)
module top(clk, l1, l2);
input clk;
output l1, l2;

reg [1:0] l1;
reg [1:0] l2;

   initial begin
      l1 = 0;
      l2 = 0;
   end

always @ (clk or l2)
begin
  if (clk)
    l1 = ~l2;
end

reg tmp;

always @ (l2 or clk or l1)
begin
  tmp = l2[0];
  if (!clk)
    tmp = l1[0];
  l2 = tmp;  // tmp is only 1-bit wide
end

endmodule
