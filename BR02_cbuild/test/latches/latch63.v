module top (out, clk, in, en);
   output out;
   input  clk, in;
   input [5:4] en;

   // Register the inputs so that they arrive at the same time
   reg         rin, dclk;
   reg [5:4]   ren;
   initial rin = 0;
   initial dclk = 0;
   initial ren = 0;
   always @ (posedge clk)
     dclk <= ~dclk;
   always @ (posedge clk)
     begin
        rin <= in;
        ren <= ren;
     end

   wire   t1 = ren[5] & ren[4];
   wire   t2 = ren[5] & $unsigned(ren[4] ? 1 : 0);

   reg    l1, l2;
   initial l1 = 0;
   initial l2 = 0;
   always @ (dclk or t1 or rin)
     if (dclk)
       if (t1)
         l1 <= rin;
   always @ (dclk or t2 or l1)
     if (~dclk)
       if (t2)
         l2 <= l1;

   reg    out;
   initial out = 0;
   always @ (l2 or dclk)
     if (dclk)
       out <= l2;

endmodule
