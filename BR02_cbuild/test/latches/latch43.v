// this tests our analysis to reject partial-net latches
module top(clk, l1);
input clk;
output l1;

reg [1:0] l1;

   initial l1 = 2'b0;

always @ (clk or l1)
begin
  if (clk)
    l1[0] = ~l1[1];
  else
    l1[1] = l1[0];
end

endmodule
