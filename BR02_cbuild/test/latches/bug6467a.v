// Generated with: ./rangen -s 145 -a -n 0x49510e4e -o /work/aron/build/clean/obj/Linux.product/test/rangen/rangen/testcases.080806/cbld/segfault7.v
// Using seed: 0x49510e4e
// Writing acyclic circuit of size 145 to file '/work/aron/build/clean/obj/Linux.product/test/rangen/rangen/testcases.080806/cbld/segfault7.v'
// Module count = 6

// module buX
// size = 29 with 7 ports
module buX(G, sSb, UV, hpimX, aaAje, as, m);
input G;
inout sSb;
input UV;
input hpimX;
inout aaAje;
output as;
input m;

wire [4:5] G;
wire [5:7] sSb;
wire [2:5] UV;
wire [19:28] hpimX;
wire [2:18] aaAje;
reg [1:3] as;
wire [3:9] m;
reg [1:3] tGD;
reg [1:3] vOxc;
reg q_;
reg [3:5] C_d;
wire [8:38] Bx;
reg [12:28] vfB_Z;
reg [4:5] gzuz;
wire [5:18] MNjo;
reg [0:0] kipQ;
reg QoOAB;
wire i;
reg WtVK;
reg [2:7] R;
reg Qdy;
reg [0:0] _i [9:24]; // memory
reg a;
reg apIY;
reg [13:17] OTWu;
_Ahu jT ({aaAje[15:16],G[4],UV[2:5],UV[2:5],UV[2:5]}, {G[4],hpimX[19]}, /* unconnected */, sSb[5], i, hpimX[19], {sSb[5],sSb[5],m[5:7],hpimX[19]}, hpimX[19], i);

iABuE u ({i,i}, i, {aaAje[3:18],hpimX[19],sSb[5],aaAje[3:18],i,aaAje[15:16],sSb[5],i,i});

assign Bx[28:36] = (i != 1'b1) ? 2'b11 : 9'bz;

WPH D ({UV[2:5],UV[2:5],i,aaAje[15:16],i}, i);

always @(&m[5:7])
if (m[5:7])
begin
apIY = ~7'b0100101;
end

assign MNjo[5:18] = (i);

WPH y ({hpimX[19],apIY,aaAje[15:16],i,hpimX[19],UV[2:5],i,i}, i);

always @(negedge G[4])
begin :Hrc

begin
if (hpimX[19] == 1'b1)
begin
a = ~i;
end
end
end

_Ahu fN ({aaAje[15:16],i,aaAje[15:16],G[4],sSb[5],i,i,aaAje[15:16],UV[2:5]}, aaAje[15:16], /* unconnected */, i, i, i, {i,hpimX[19],sSb[5],apIY,i,apIY}, G[4], i);

assign sSb[5:7] = ~sSb[5]^i;

_Ahu CX ({sSb[5],i,i,i,i,a,UV[2:5],m[5:7],i,i}, {i,hpimX[19]}, /* unconnected */, i, i, a, {i,i,i,i,i,a}, i, i);

always @(posedge i)
begin
begin
_i[16]/* memuse */ = i;
end
end

always @(negedge i)
begin
begin
vfB_Z[18] = ~(~sSb[5]&i);
end
end

_Ahu jJtQ ({hpimX[19],sSb[5],i,i,_i[16]/* memuse */,i,a,i,m[5:7],vfB_Z[18],3'b011}, {i,i}, /* unconnected */, hpimX[19], i, i, {i,hpimX[19],i,i,i,i}, vfB_Z[18], /* unconnected */);

assign Bx[9:38] = vfB_Z[18]&i;

always @(negedge &aaAje[15:16])
begin
begin
QoOAB = (~i);
end
end

assign Bx[9:38] = ~UV[2:5];

always @(&UV[2:5])
if (~UV[2:5])
begin
Qdy = a|QoOAB;
end

always @(negedge i)
begin
begin
q_ = ~vfB_Z[18]^G[4];
end
end

always @(negedge i)
begin
begin
a = i;
end
end

// GATED-CLOCK
wire MXm = i; // clock
wire LIfHX = apIY; // enable data
reg tMZEQ;
always @(MXm or LIfHX) begin
  if (MXm) begin
    tMZEQ = LIfHX; // enable assign
  end
end
wire OGmyZ = ~MXm & tMZEQ; // gated clock
always @(posedge OGmyZ)
begin
begin
vfB_Z[15:23] = G[4]|aaAje[3:18];
end
end

assign Bx[9:38] = ~OGmyZ;

assign i = ~i;

endmodule

// module iABuE
// size = 10 with 3 ports
module iABuE(Wm, GmOFG, F);
output Wm;
output GmOFG;
input F;

reg [2:3] Wm;
reg GmOFG;
wire [25:64] F;
reg [0:3] hZWP;
reg [3:4] P;
reg [0:3] Pg;
reg YbO;
reg qC_;
reg FpFs;
reg [10:26] SjZBL;
wire [6:6] Y;
WPH QNtnv ({F[52:59],F[30:33]}, {1'b1});

fz xO ({F[40:53],4'b0011}, {1'b0}, {F[30:33],F[25:33]});

fz GZIjC ({F[30:33],F[25:33],F[30:33],1'b1}, {1'b1}, {F[30:33],F[25:33]});

fz kYAd ({F[25:33],F[25:33]}, {1'b1}, {F[30:33],F[52:59],1'b0});

assign Y[6] = ~F[40:53];

sXzgd Bda ({2'b11}, {Y[6],Y[6],Y[6],Y[6],Y[6]}, F[30:33]);

WPH Qd ({Y[6],Y[6],Y[6],Y[6],Y[6],Y[6],Y[6],5'b01001}, Y[6]);

WPH hYgK_ ({F[30:33],F[30:33],Y[6],Y[6],2'b00}, Y[6]);

WPH zzY ({F[30:33],Y[6],F[30:33],Y[6],Y[6],Y[6]}, Y[6]);

fz rtQ ({Y[6],Y[6],Y[6],Y[6],F[25:33],Y[6],Y[6],Y[6],2'b10}, Y[6], {Y[6],Y[6],Y[6],F[30:33],Y[6],Y[6],Y[6],Y[6],2'b01});

fz HXk ({F[40:53],Y[6],Y[6],Y[6],Y[6]}, Y[6], {F[25:33],Y[6],Y[6],Y[6],Y[6]});

fz jAqKX ({Y[6],Y[6],F[40:53],Y[6],1'b1}, Y[6], {Y[6],Y[6],F[52:59],Y[6],Y[6],Y[6]});

assign Y[6] = ~6'b111100;

fz dfC ({Y[6],F[40:53],Y[6],Y[6],Y[6]}, Y[6], {Y[6],Y[6],Y[6],Y[6],Y[6],Y[6],Y[6],Y[6],5'b01111});

fz EHx ({Y[6],F[25:33],Y[6],Y[6],F[30:33],Y[6],Y[6]}, Y[6], {Y[6],F[30:33],Y[6],Y[6],F[30:33],2'b01});

sXzgd J ({Y[6],Y[6]}, {Y[6],Y[6],Y[6],Y[6],Y[6]}, {Y[6],Y[6],Y[6],Y[6]});

endmodule

// module _Ahu
// size = 60 with 9 ports
module _Ahu(xyd, _XJM, V, pcz_O, J, Mdg, c, NWEXC, zzT);
input xyd;
input _XJM;
inout V;
input pcz_O;
output J;
input Mdg;
input c;
input NWEXC;
output zzT;

wire [7:21] xyd;
wire [4:5] _XJM;
wire [16:30] V;
wire [7:7] pcz_O;
wire J;
wire Mdg;
wire [0:5] c;
wire NWEXC;
reg zzT;
reg [0:4] NNxXz;
wire AX;
reg [2:4] zF;
reg [4:5] IWUVL;
reg THNN;
reg [2:5] vDtrx;
wire [5:7] f;
wire [4:29] by;
reg [15:30] SuMv;
reg [15:24] yr;
reg [3:4] MRg;
reg [31:105] X_;
reg [1:4] vCvgG;
reg [1:8] blORh;
reg [14:20] _;
reg [4:4] hELj;
reg [1:27] _d;
reg [9:31] ZT;
reg R;
reg [11:24] G;
wire mgh;
wire D;
reg nHeI;
wire [10:26] DDK;
reg [6:28] C;
reg [19:26] Le [0:84]; // memory
reg [2:2] L;
reg [13:13] QtdN;
wire [0:6] ZdD;
reg [17:25] pMX;
reg n;
wire [8:26] A;
wire [0:5] LOM;
reg [10:12] JEp;
wire [0:5] Mg;
reg [19:29] qjjYra;
reg [2:5] ipiR;
reg [2:3] bKXz;
wire Rdv;
reg [14:15] oJx;
reg U_R;
reg __;
reg Wuq;
reg [1:2] mgc;
reg [5:7] GtG;
fz BxSto ({NWEXC,xyd[8:10],NWEXC,xyd[8:10],V[22:26],Mdg,pcz_O[7],Mdg,_XJM[4:5]}, Mdg, {V[22:26],V[22:26],_XJM[4:5],pcz_O[7]});

WPH DuhBE ({xyd[8:10],xyd[8:10],V[22:26],NWEXC}, c[1]);

always @(c[1])
if (c[1])
begin
if (NWEXC == 1'b0)
begin
pMX[20:24] = ~Mdg;
end
end
else
begin
n = ~xyd[8:10]^_XJM[4:5];
end

always @(posedge pcz_O[7])
begin :hkrQ
reg esY;
reg Lipfy;

begin
end
end

always @(posedge n)
begin
begin
n = pcz_O[7]^n;
end
end

always @(&V[22:26])
if (~V[22:26])
begin
Le[69]/* memuse */ = n;
end

// GATED-CLOCK
wire o = &xyd[8:10]; // clock
wire Sg = n|NWEXC; // enable data
reg PYE;
always @(o or Sg) begin
  if (o) begin
    PYE = Sg; // enable assign
  end
end
wire brW = ~o & PYE; // gated clock
always @(posedge brW)
begin :g
reg [3:5] Dhy_;
reg aHPt;
reg FlmlL;
reg [2:4] SggG;

begin
IWUVL[4] = o;
end
end

always @(posedge &_XJM[4:5])
begin :ykYbR
reg [28:28] itqF;
reg [79:112] Xz;
reg DYxB;

begin
QtdN[13] = ~V[22:26];
end
end

always @(n)
if (n)
begin
vCvgG[2:3] = ~10'b1011000010;
end

assign LOM[2:5] = (V[22:26] != 5'b11001) ? NWEXC : 4'bz;

assign D = 7'b0000101;

always @(brW)
if (~brW)
begin
$finish;
pMX[20:24] = xyd[8:10]^Sg;
end

always @(posedge &_XJM[4:5])
begin
begin
NNxXz[0:4] = ~c[1]^vCvgG[2:3];
end
end

assign AX = NNxXz[0:4]&o;

assign by[11:25] = ~((~(~n&xyd[8:10])));

// GATED-CLOCK
wire br = Sg; // clock
wire KTgz = (~br|c[1]); // enable data
reg syR;
always @(br or KTgz) begin
  if (~br) begin
    syR = KTgz; // enable assign
  end
end
wire Vccd = br & syR; // gated clock
always @(negedge Vccd)
begin :FtujO
reg tc_Zt;
reg [3:5] tVkh;

begin
_[18] = (~pcz_O[7]^PYE);
end
end

// GATED-CLOCK
wire hYv = brW; // clock
wire LW = Le[69]/* memuse */|Sg; // enable data
reg xCPaq;
always @(hYv or LW) begin
  if (hYv) begin
    xCPaq = LW; // enable assign
  end
end
wire VACW = ~hYv & xCPaq; // gated clock
always @(posedge VACW)
begin
begin
__ = ~brW;
end
end

always @(posedge brW)
begin
begin
end
end

always @(&pMX[20:24])
if (pMX[20:24])
begin
GtG[5:7] = 4'b0010;
end
else
begin
ZT[18:19] = Mdg^_XJM[4:5];
end

always @(brW)
if (~brW)
begin
end

WPH mmJL ({n,GtG[5:7],xyd[8:10],vCvgG[2:3],_XJM[4:5],__}, __);

always @(&vCvgG[2:3])
if (~vCvgG[2:3])
begin
IWUVL[4] = ~QtdN[13];
_[18] = n;
end

// GATED-CLOCK
wire gOsU = &_XJM[4:5]; // clock
wire l = (gOsU^xyd[8:10]); // enable data
reg MUThh;
always @(gOsU or l) begin
  if (~gOsU) begin
    MUThh = l; // enable assign
  end
end
wire iGUC = gOsU & MUThh; // gated clock
always @(negedge iGUC)
begin
begin
_d[23:25] = 8'b01110010;
end
end

always @(negedge _[18])
begin
begin
_d[23:25] = ~5'b11000;
_[14:19] = IWUVL[4]&GtG[5:7];
end
end

always @(posedge VACW)
begin
begin
_[14:19] = ~ZT[18:19];
end
end

always @(negedge Mdg)
begin
begin
R = pMX[20:24];
end
end

fz NjR ({QtdN[13],c[1],_XJM[4:5],MUThh,brW,n,IWUVL[4],R,QtdN[13],_XJM[4:5],6'b101101}, _[18], {ZT[18:19],KTgz,xCPaq,Le[69]/* memuse */,xCPaq});

// GATED-CLOCK
wire rQ = &_[14:19]; // clock
wire OoeeP = ~n; // enable data
reg cn;
always @(rQ or OoeeP) begin
  if (rQ) begin
    cn = OoeeP; // enable assign
  end
end
wire KDJu = ~rQ & cn; // gated clock
always @(posedge KDJu)
begin
begin
Le[22]/* memuse */ = ~_[14:19];
end
end

// GATED-CLOCK
wire uZ = syR; // clock
wire NaZ = ~6'b011101; // enable data
reg M;
always @(uZ or NaZ) begin
  if (~uZ) begin
    M = NaZ; // enable assign
  end
end
wire PBlh = uZ & M; // gated clock
always @(negedge PBlh)
begin :Pi
reg [3:5] tbHg;
reg [0:7] rnuE;
reg [0:1] y;
reg [20:30] dWFX;

begin
G[14:17] = ~brW;
end
end

always @(brW)
if (brW)
begin
_[17:20] = (~MUThh^Vccd);
end

always @(KTgz)
if (~KTgz)
begin
C[14:20] = ~VACW&KTgz;
end

// GATED-CLOCK
wire jJvf = cn; // clock
wire XrQHD = ~IWUVL[4]&xyd[8:10]; // enable data
reg BkNP;
always @(jJvf or XrQHD) begin
  if (jJvf) begin
    BkNP = XrQHD; // enable assign
  end
end
wire NQFzP = ~jJvf & BkNP; // gated clock
always @(posedge NQFzP)
begin
begin
oJx[14:15] = ~ZT[18:19];
end
end

always @(negedge NQFzP)
begin :vlB
reg lG;

begin
GtG[5:7] = ~R^vCvgG[2:3];
end
end

WPH PCY ({MUThh,vCvgG[2:3],pcz_O[7],Mdg,OoeeP,_[14:19]}, xCPaq);

wire tQqM = &C[14:20];
always @(posedge Mdg or negedge tQqM)
if (~tQqM)
begin
begin
end
end
else
begin
begin
__ = (Le[69]/* memuse */^ZT[18:19]);
end
end

always @(posedge &vCvgG[2:3])
begin
begin
end
end

always @(br)
if (~br)
begin
if (NaZ == 1'b1)
begin
ipiR[2:3] = ~(PYE);
end
else
begin
Le[36]/* memuse */ = NNxXz[0:4]^NNxXz[0:4];
end
end

assign D = GtG[5:7]&KTgz;

WPH gZS ({uZ,n,VACW,G[14:17],_[17:20],syR}, LW);

assign ZdD[2:4] = ~pMX[20:24]^pMX[20:24];

assign mgh = LW|ZT[18:19];

always @(posedge hYv)
begin
begin
ipiR[2:3] = ~(NaZ);
end
end

endmodule

// module fz
// size = 21 with 3 ports
module fz(eG, F, nhHKO);
input eG;
input F;
input nhHKO;

wire [68:85] eG;
wire F;
wire [12:24] nhHKO;
reg [2:9] uL;
reg [14:21] mM [0:5]; // memory
reg [2:6] CT;
reg [0:6] dXjc;
reg Q;
reg [32:97] Wbnt;
reg Lmv;
reg [0:7] l;
reg pIeoD;
reg CaxC;
reg BHPMT;
reg uDc;
wire GgtPW;
reg [3:25] SCWR;
reg c;
reg v;
reg ijzL;
reg [0:2] O;
sXzgd g (nhHKO[17:18], {GgtPW,GgtPW,GgtPW,GgtPW,GgtPW}, {eG[77:78],F,1'b0});

sXzgd JkaD (nhHKO[17:18], {GgtPW,GgtPW,GgtPW,GgtPW,GgtPW}, {GgtPW,GgtPW,GgtPW,GgtPW});

sXzgd DZl (nhHKO[17:18], {GgtPW,GgtPW,GgtPW,GgtPW,GgtPW}, {GgtPW,nhHKO[17:18],GgtPW});

always @(posedge GgtPW)
begin :TxK
reg [1:4] MaLk;
reg [0:2] JIOpZ;

begin
ijzL = ~F^GgtPW;
end
end

// GATED-CLOCK
wire nN = &nhHKO[17:18]; // clock
wire lv = GgtPW|GgtPW; // enable data
reg M;
always @(nN or lv) begin
  if (~nN) begin
    M = lv; // enable assign
  end
end
wire rJtbG = nN & M; // gated clock
always @(negedge rJtbG)
begin :oDm

begin
Q = GgtPW|GgtPW;
end
end

always @(negedge GgtPW)
begin
begin
v = GgtPW;
end
end

endmodule

// module WPH
// size = 25 with 2 ports
module WPH(vLfB, Ge);
input vLfB;
input Ge;

wire [13:24] vLfB;
wire Ge;
wire o_b;
reg [3:28] IiN;
reg [0:0] mPyo [0:17]; // memory
reg gmif;
reg zFgQ;
reg InTh;
reg [11:13] fln_;
wire Kr;
reg [4:6] T;
reg nhJk;
reg cgxq;
reg rm;
wire [6:31] z;
reg [28:30] qLbrR [5:6]; // memory
reg [2:5] _TBDu;
reg [1:5] VO;
wire VTf;
wire [20:29] _Y;
reg [22:22] RvC;
reg YFJ;
wire [0:2] eWdNG;
always @(&vLfB[14:16])
if (~vLfB[14:16])
begin
case (vLfB[14:16])
3'd0:
begin
end
3'd1:
begin
VO[2:4] = vLfB[14:16]|vLfB[14:16];
end
3'd2:
begin
end
3'd3:
begin
IiN[9:19] = ~Ge|VO[2:4];
end
3'd4:
begin
fln_[11:12] = ~(~VO[2:4]);
end
3'd5:
begin
end
3'd6:
begin
rm = fln_[11:12];
end
3'd7:
begin
qLbrR[5]/* memuse */ = (IiN[9:19]^vLfB[14:16]);
end
endcase
end

always @(negedge &VO[2:4])
begin
begin
RvC[22] = ~IiN[9:19]&Ge;
end
end

sXzgd OPm (fln_[11:12], {Kr,VTf,_Y[20:22]}, {rm,vLfB[14:16]});

always @(posedge Kr)
begin :dC
reg [1:3] yQ;
reg [5:5] kCW;
reg [1:5] Q [17:29]; // memory
reg [3:5] lXFEg;

begin
IiN[5:13] = 2'b10;
end
end

sXzgd MU (fln_[11:12], {_Y[24:27],VTf}, {Ge,VTf,Kr,rm});

// GATED-CLOCK
wire S = &IiN[9:19]; // clock
wire feg = ~((fln_[11:12])); // enable data
reg kUbh;
always @(S or feg) begin
  if (~S) begin
    kUbh = feg; // enable assign
  end
end
wire pTJb = S & kUbh; // gated clock
always @(negedge pTJb)
begin
begin
InTh = {_Y[24:27],kUbh};
end
end

always @(posedge VTf)
begin
begin
end
end

sXzgd UgY (fln_[11:12], {eWdNG[0:2],o_b,VTf}, _Y[24:27]);

sXzgd hrFza ({Kr,S}, {VTf,Kr,Kr,eWdNG[0],Kr}, {InTh,VTf,VTf,VTf});

always @(negedge o_b)
begin
begin
nhJk = ~(_Y[24:27]);
end
end

endmodule

// module sXzgd
// size = 1 with 3 ports
module sXzgd(I, Ij, JBhhg);
input I;
output Ij;
input JBhhg;

wire [5:6] I;
reg [0:4] Ij;
wire [1:4] JBhhg;
reg [9:16] rqtl_;
reg [2:18] FKdr;
wire [6:7] pf;
assign pf[6] = I[5];

endmodule
