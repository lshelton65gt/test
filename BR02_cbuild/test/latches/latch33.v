// l1, l2 and l3 are all exclusive
// l1 and/or l3 can be converted if x is moved out of their block
// or if x is reordered and rescoped (happens with -noAtomize)
module top(clk, l1, l2, l3);
input clk;
output l1, l2, l3;

reg l1, l2, l3;
reg x;

// We don't want the initial block in cbuild because it affects
// the rescoping of x
`ifdef EVENTSIM
   initial begin
      l1 = 0;
      l2 = 0;
      l3 = 0;
      x = 0;
   end
`endif

always @ (clk or x or l2)
begin
  if (clk)
    l1 = ~x;
  x = l2;
  if (clk)
    l3 = ~x;
end

always @ (clk or l1 or l3)
begin
  if (!clk)
    l2 = l1 ^ l3;
end

endmodule
