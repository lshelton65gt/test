module top (out, sel);
  output out;
  input sel;

  reg out;
  wire sel2 = ~sel;
  always @ (sel2)
    if (sel2)
       out <= 1'b0;
    else if (sel)
       out <= 1'b1;

endmodule
