// This testcase triggered a bug in replaceLeaves
// in which it was possible to delete an ident
// but fail to overwrite the pointer to it with its
// replacement.

module ZtW(pz, oVG, tq, Q, Ec, Uqal);
input pz;
output oVG;
input tq;
inout Q;
inout Ec;
input Uqal;

wire pz;
reg [4:5] oVG;
wire tq;
wire Q;
wire [4:4] Ec;
wire [1:30] Uqal;
reg [36:67] M;

reg [3:7] fsS [2:3]; // memory
reg [0:0] R [9:122]; // memory
reg [3:7] Zv;
reg [12:31] KhQJ;


always @(&KhQJ[16:28])
  if (KhQJ[16:28])
  begin
    oVG[4:5] = M[45:53];
  end

always @(&fsS[2])
  if (~fsS[2])
  begin
    Zv[5:6] = tq;
  end

always @(Q)
  if (~Q)
  begin
    KhQJ[16:31] <= ~R[107];
  end

always @(posedge pz)
begin
  R[107] = ~Zv[5:6];
end


endmodule
