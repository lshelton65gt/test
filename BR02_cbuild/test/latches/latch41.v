// l1 and l2 are both exclusive
// l1 will not be converted unless en is split from the block
module top(clk, x_in, l1, l2);
input clk, x_in;
output l1, l2;

reg en, l1, l2;

   initial begin
      l1 = 0;
      l2 = 1;
   end

  // Flop inputs for Aldec simulation
  reg dclk, x;
  initial dclk = 0;
  initial x = 0;

  always @(posedge clk)
  begin
    dclk = ~dclk;
    x = x_in;
  end

always @ (x or dclk or l2)
begin
  en = ~x;
  if (dclk & en)
    l1 = l2;
end

always @ (dclk or en or l1)
begin
  if (!dclk & en)
    l2 = ~l1;
end

endmodule
