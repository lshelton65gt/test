// l1, l2 and l3 are all exclusive, and when compiled with
// -noFlatten, they are all instances of the same latch.
// This tests the exclusivity frontier finding code for multiple
// instances.
module top(clk, en1, en2, en3, l1, l2, l3);
   input clk, en1, en2, en3;
   output l1, l2, l3;

   wire l1, l2, l3;

   reg ren1, ren2, ren3, ren3x;
   initial begin
      ren1 = 0;
      ren2 = 1;
      ren3 = 1;
      ren3x = 1;
   end
   always @ (posedge clk)
     begin
        ren1 <= en1;
        ren2 <= en2;
        ren3x <= en3;
     end

  // this latch allows ren3 to be stable so that it does not need
  // to be treated as a clock
  always @(ren1 or ren2 or ren3x)
  begin
    if (!ren1 & !ren2)
      ren3 <= ren3x;
  end

  LATCH L1 (l1, ~ren2, ren1,  ren3, l2, l3);
  LATCH L2 (l2,  ren1, ren2,  ren3, l3, l1);
  LATCH L3 (l3,  ren2, ren3, ~ren1, l1, l2);
  
endmodule

module LATCH(out, c1, c2, c3, in1, in2);
input c1, c2, c3, in1, in2;
output out;

  reg out;
  initial out = 0;

  always @(c1 or c2 or c3 or in1 or in2)
  begin
    if (c1)
    begin
      if (c2)
      begin
        if (c3)
          out = in2;
      end
      else
        out = in1;
    end
  end

endmodule
