// This is the same as clkgate1 except the latch is created by
// writing the data in the else clause

module top (out, clk, en, in);
   output out;
   input  clk, en, in;

   reg    ren;
   reg    out;
   wire   gclk;
   
   // Sync the input enable
   reg    sen;
   always @ (posedge clk)
     sen <= en;

   always @ (clk or sen)
     if (clk)
       ;
     else
       ren <= sen;
   assign gclk = clk & ren;

   initial out = 0;
   always @ (posedge gclk)
     out <= in;

endmodule
