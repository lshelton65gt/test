// simple exclusive latch pair
module top(clk, en, l1, l2, q1, q2);
   input clk, en;
   output l1, l2, q1, q2;

   reg    l1, l2;
   initial begin
      l1 = 0;
      l2 = 1;
   end

   reg dclk1, dclk2, ren1, ren2;
   initial begin
      dclk1 = 0;
      dclk2 = 0;
      ren1 = 0;
      ren2 = 1;
   end
   always @ (posedge clk)
     begin
        ren1 <= en;
        ren2 <= ~en;
     end
   always @ (posedge clk)
     dclk1 <= ~dclk1 & ren1;
   always @ (posedge clk)
     dclk2 <= ~dclk2 & ~ren2;

   always @ (dclk1 or l2)
     begin
        if (dclk1)
          l1 = l2;
     end

   always @ (dclk2 or l1)
     begin
        if (~dclk2)
          l2 = ~l1;
     end

   reg q1, q2;
   initial begin
      q1 = 0;
      q2 = 0;
   end
   always @ (posedge dclk1)
     q1 <= en;
   always @ (posedge dclk2)
     q2 <= en;
endmodule
