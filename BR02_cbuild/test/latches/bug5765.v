// Generated with: ./rangen -s 268 -n 0x816c989c -o /w/xenon/rsayde/build/rangen/obj/Linux.product/rangen/test/rangen/rangen/testcases.030906/cbld/assert4.v
// Using seed: 0x816c989c
// Writing cyclic circuit of size 268 to file '/w/xenon/rsayde/build/rangen/obj/Linux.product/rangen/test/rangen/rangen/testcases.030906/cbld/assert4.v'
// Module count = 12

// module GTFo
// size = 74 with 8 ports
module GTFo(toU, HQot, iJ, o, AI, ta, Y, DtBZ);
   input toU;
   inout HQot;
   inout iJ;
   input o;
   inout AI;
   input ta;
   output Y;
   input  DtBZ;

   wire [3:17] toU;
   wire [7:22] HQot;
   wire [2:16] iJ;
   wire [2:5]  o;
   wire        AI;
   wire [49:74] ta;
   reg          Y;
   wire         DtBZ;
   wire         mBuF;
   reg [2:26]   qvMQ;
   reg          CD;
   reg [4:30]   ctLpL [2:3]; // memory
   reg [0:7]    cdQ;
   reg          ipR;
   reg [22:29]  fzqk;
   reg [1:7]    ok;
   reg [2:7]    Q;
   reg [0:6]    SUr [91:102]; // memory
   reg [55:64]  WatL;
   reg [0:7]    iONBF;
   wire         ErUTS;
   reg [2:6]    vSIi;
   wire         SR;
   wire         EZmAF;
   reg [0:0]    skTnh [2:3]; // memory
   reg [0:4]    lvEWW;
   reg [1:4]    N;
   reg          d;
   wire [2:10]  NLI;
   reg [2:5]    hT;
   reg [7:19]   gXgje;
   wire [0:1]   qOq;
   reg          SNT;
   reg [2:18]   C;
   wire [0:3]   c;
   reg [22:28]  Psc;
   reg [0:1]    i;
   reg [0:6]    _ZDO;
   wire         P;
   reg [9:23]   Vjs;
   reg          IaW;
   wire         zQGfi;
   wire         kLj;
   reg [4:20]   Pt;
   reg [6:22]   Xr;
   wire [14:25] DC;
   reg [10:27]  GqLx;
   reg [2:2]    ZvH;
   reg [4:5]    ntmp;
   wire [6:82]  tstK;
   wire [0:6]   k;
   reg [8:53]   Wtr;
   reg [15:79]  q;
   reg [11:15]  dAu_;
   wire [1:28]  t;
   reg          xu;
   reg          dId;
   reg [12:14]  BcbM;
   reg          oOi;
   wire         E;
   reg [1:7]    LQ;
   reg [1:2]    FieGQ;
   reg [17:22]  b;
   reg [10:10]  cPefU;
   wire [1:4]   prerQ;
   always @(&HQot[12:17])
     if (~HQot[12:17])
       begin
          ctLpL[3]/* memuse */ = ~10'b1111111011;
       end

   always @(&HQot[12:17])
     if (~HQot[12:17])
       begin
          gXgje[11:17] = 1'b1;
       end

   a_k mIdSl ({Xr[13:21],i[1],C[6:16],AI,toU[3],i[1],ErUTS}, {DtBZ,Xr[10:15],WatL[58:61],HQot[8:13],t[5:13],FieGQ[1:2]}, kLj, qOq[0:1]);

   always @(&toU[5:11])
     if (~toU[5:11])
       begin
          dId = ~(~oOi^HQot[12:17]);
          ZvH[2] = ~(((~t[5:17])));
       end

   assign tstK[42:54] = E;

   a_k dcZS ({E,Xr[11:22],t[5:13],qOq[0],DtBZ,qOq[0]}, {toU[3],oOi,C[12:15],o[4:5],toU[3],P,dId,WatL[58:61],toU[7:11],gXgje[11:17],1'b1}, zQGfi, qOq[0:1]);

   assign t[24:27] = cdQ[3:6];

   assign AI = ~dAu_[12:14];

   HoAG qj (zQGfi, {P,Xr[9:16],Xr[11:22],dId,cdQ[3:6],dId,ZvH[2],3'b111}, {ErUTS,dId,c[0:1],tstK[8:15],P,C[12:15],ErUTS,o[4:5],ZvH[2],3'b011});

   tgAU XZM (HQot[13:14], HQot[8:13], iJ[7:8], {E,tstK[65:70]}, ErUTS, {P,LQ[2:6],C[12:15],1'b1}, o[4:5]);

   always @(posedge &tstK[37:73])
     begin
        begin
           Wtr[21:43] = (~dAu_[12:14]|AI);
        end
     end

   oT Im ({mBuF,iJ[4:9],NLI[3:9],prerQ[1:4],EZmAF,P}, ErUTS, /* unconnected */, mBuF, E, {HQot[8:21],iJ[4:9],prerQ[1:4],DC[15:19],qOq[0:1],AI}, HQot[9:16]);

   always @(negedge &tstK[65:70])
     begin
        begin
           fzqk[24:29] = (~qOq[0]);
        end
     end

   assign t[24:27] = ~((~Xr[9:16]&HQot[13:14]) + (~ErUTS));

   XK Tu (Xr[9:16], toU[3], DC[15:19], DC[15:19]);

   assign k[4:6] = gXgje[11:17]^toU[7:11];

   always @(&LQ[2:6])
     if (~LQ[2:6])
       begin
          if (t[5:17] == 13'b1001000000111)
            begin
            end
       end

   always @(&Xr[11:22])
     if (Xr[11:22])
       begin
          $finish;
       end
     else
       begin
       end

   a_k fZ ({LQ[2:6],HQot[9:16],iJ[4:10],toU[3],FieGQ[1:2],dId,ipR}, {ta[56:59],ErUTS,tstK[65:70],oOi,WatL[58:61],ta[56:59],Xr[9:16]}, mBuF, qOq[0:1]);

   a_k Yjy ({C[6:16],ZvH[2],i[1],C[12:15],E,_ZDO[4:6],4'b0101}, {dId,C[6:16],P,C[12:15],tstK[65:70],c[0:1],3'b011}, EZmAF, qOq[0:1]);

   always @(&cdQ[3:6])
     if (~cdQ[3:6])
       begin
          skTnh[2]/* memuse */ = (~(~t[5:17]));
       end

   always @(negedge &c[0:1])
     begin
        begin
           Vjs[10] = (tstK[14:25]);
        end
     end

   assign HQot[8:21] = 10'b0000001101;

   always @(negedge &HQot[12:17])
     begin
        begin
        end
     end

   always @(posedge P)
     begin :SaXHT

        begin
           skTnh[2]/* memuse */ = ~toU[5:11];
        end
     end

   always @(&iJ[7:8])
     if (~iJ[7:8])
       begin
          ZvH[2] = ~((dId^o[2:3]));
       end

   assign c[1:3] = Vjs[10]|DtBZ;

   always @(negedge &Xr[9:16])
     begin :umxrB
        reg mFo;
        reg U;
        reg Qu;
        reg [30:115] X;
        reg [1:6]    dvHeV;

        begin
           GqLx[13:25] = ~(~(LQ[2:6]));
        end
     end

   always @(posedge &tstK[17:49])
     begin
        begin
           IaW = Xr[10:15];
        end
     end

   a_k xfttE ({fzqk[24:29],ZvH[2],WatL[58:61],toU[7:11],o[2:3],_ZDO[3:5],P,qOq[0],2'b00}, {C[6:16],i[1],skTnh[2]/* memuse */,tstK[65:70],iJ[4:10],dId,IaW}, ErUTS, qOq[0:1]);

   a_k dsUO ({ZvH[2],toU[3],WatL[58:61],o[4:5],skTnh[2]/* memuse */,fzqk[24:29],C[12:15],P,ipR,4'b0001}, {dId,AI,Xr[10:15],dId,ZvH[2],FieGQ[1:2],skTnh[2]/* memuse */,Xr[9:16],_ZDO[4:6],_ZDO[3:5],1'b1}, ErUTS, qOq[0:1]);

   always @(dId)
     if (~dId)
       begin
          ipR = Xr[9:16];
       end
     else
       begin
          _ZDO[3:4] = cdQ[3:6]&toU[7:11];
       end

   always @(toU[3])
     if (toU[3])
       begin
          case (LQ[2:6])
            5'd0:
              begin
                 iONBF[7] = {iONBF[4:7],dAu_[12:14]};
              end
            5'd1:
              begin
                 ok[4:6] = LQ[2:6];
              end
            5'd2:
              begin
                 Vjs[12:22] = ~(~5'b11011);
                 C[7:17] = iJ[7:8]^P;
              end
            5'd3:
              begin
                 qvMQ[6:11] = 5'b10000;
              end
            5'd4:
              begin
                 ipR = ~(~oOi) + (ok[4:6]);
              end
            5'd5:
              begin
              end
            5'd6:
              begin
                 gXgje[11:17] = ~8'b10101100;
              end
            5'd7:
              begin
              end
            5'd8:
              begin
                 Vjs[10] <= (4'b0110);
              end
            5'd9:
              begin
                 iONBF[7] = ~HQot[13:14];
              end
            default:
              begin
                 Vjs[12:22] = ~4'b0101;
                 Wtr[46:49] <= oOi;
              end
          endcase
       end

   a_k aA ({fzqk[24:29],ErUTS,ZvH[2],Vjs[12:22],iONBF[7],oOi,dId,Vjs[10],DtBZ,1'b1}, {Xr[9:16],t[5:17],ipR,iONBF[4:7],o[2:3]}, AI, qOq[0:1]);

   tgAU MAEfH (_ZDO[3:4], HQot[8:13], c[0:1], toU[5:11], skTnh[2]/* memuse */, C[6:16], iJ[7:8]);

   assign t[16] = (toU[7:11] != 5'b11000) ? ~ErUTS : 1'bz;

   wire   zeNs = &ctLpL[3]/* memuse */;
   always @(negedge i[1] or posedge zeNs)
     if (zeNs)
       begin
          begin
             hT[2:4] <= ~iONBF[4:7];
          end
       end
     else
       begin
          begin
             oOi = {dId,skTnh[2]/* memuse */};
          end
       end

   always @(posedge &toU[7:11])
     begin
        begin
           ok[4:6] = WatL[58:61]&iONBF[4:7];
        end
     end

   always @(&o[2:3])
     if (o[2:3])
       begin
          _ZDO[1:5] = ~(tstK[65:70]);
       end
     else
       begin
          Wtr[46:49] = ~(ctLpL[3]/* memuse */^hT[2:4]);
       end

   wire svcv = ZvH[2];
   always @(posedge &iJ[7:8] or negedge svcv)
     if (~svcv)
       begin :aq_kP
          reg [25:29] KHUvA [8:22]; // memory
          reg [4:5]   tIUb;

          begin
             d <= ipR;
          end
       end
     else
       begin :Ajc
          reg [10:20] WZVSd;

          begin
             cdQ[6:7] = Vjs[10];
          end
       end

endmodule

// module tgAU
// size = 30 with 7 ports
module tgAU(N, O, CBsI, W, z, Iub, _);
   input N;
   input O;
   input CBsI;
   input W;
   input z;
   input Iub;
   input _;

   wire [2:3] N;
   wire [1:6] O;
   wire [3:4] CBsI;
   wire [11:17] W;
   wire         z;
   wire [7:17]  Iub;
   wire [3:4]   _;
   reg [1:4]    nVE;
   wire [7:7]   GqcO;
   reg [0:1]    iBLC [1:23]; // memory
   reg          JHNl;
   reg          sz;
   reg [3:3]    x;
   reg          _R;
   reg [0:0]    EY [12:24]; // memory
   reg [2:6]    scDTT;
   reg          tg;
   reg [1:6]    q_HQx;
   reg [1:1]    YAFg;
   reg [4:22]   r;
   reg [3:6]    UuDs [0:0]; // memory
   wire [8:20]  e;
   wire [22:27] fDUO;
   reg          JVvy;
   reg          NP;
   wire [1:3]   A_t;
   reg [2:28]   jUx;
   reg [1:4]    IW;
   reg [38:82]  FK;
   wire [98:123] IdG;
   reg [6:6]     WTdiR;
   reg [7:91]    kUGQl;
   always @(negedge &N[2:3])
     begin
        begin
           FK[56:74] <= ~FK[52:69];
        end
     end

   HoAG sm (GqcO[7], {W[13:17],O[1:6],N[2:3],O[3:6],_[3:4],A_t[1:3],CBsI[4],fDUO[24:27],4'b0110}, {O[1:6],CBsI[4],fDUO[23],A_t[1:3],e[12:16],fDUO[22:27],scDTT[5],1'b1});

   oT nRW_Q ({GqcO[7],GqcO[7],A_t[2:3],A_t[2:3],e[12:20],GqcO[7],A_t[1:2],GqcO[7],GqcO[7]}, GqcO[7], A_t[1:2], GqcO[7], CBsI[4], {IdG[113:117],A_t[1:2],A_t[2:3],A_t[2:3],e[12:20],e[12:20],GqcO[7],A_t[2:3]}, {e[12:16],Iub[8:9],z});

   C rpcfk ({A_t[1:2],A_t[2:3]}, {z,scDTT[5],scDTT[5],CBsI[4],sz,2'b01}, W[13:17], GqcO[7]);

   always @(negedge &nVE[2:4])
     begin :CHHt
        reg b;

        begin
           tg = ~O[3:6];
        end
     end

   oT L_Ia ({fDUO[22:26],IdG[113:117],A_t[1:2],A_t[1:2],GqcO[7],A_t[2:3],A_t[1:2],GqcO[7]}, GqcO[7], A_t[2:3], GqcO[7], tg, {fDUO[22:26],A_t[2:3],e[12:20],GqcO[7],A_t[1:2],IdG[113:117],GqcO[7],A_t[1:2],GqcO[7],A_t[1:2],GqcO[7],GqcO[7]}, {O[1:6],scDTT[5],sz});

endmodule

// module a_k
// size = 25 with 4 ports
module a_k(ia, mQNe, xcaMV, bOU);
   input ia;
   input mQNe;
   output xcaMV;
   inout  bOU;

   wire [0:24] ia;
   wire [0:27] mQNe;
   reg         xcaMV;
   wire [10:11] bOU;
   wire [7:7]   bAC;
   reg          z;
   reg          b;
   reg [14:14]  r;
   reg [18:30]  _YkJu;
   reg [2:5]    Yf;
   reg [1:6]    FH;
   wire         EexXZ;
   wire [5:6]   W;
   reg [1:4]    StJ;
   reg [0:4]    VT;
   reg [0:6]    hhUDy;
   reg [6:12]   uND_;
   reg [0:7]    COT;
   reg          eZ;
   reg          Q;
   wire [1:1]   l;
   reg [0:0]    iGRpp;
   reg [9:14]   HSgrP;
   reg [3:26]   JfKO;
   oT OJq ({bAC[7],bAC[7],W[5],bAC[7],EexXZ,EexXZ,l[1],l[1],bAC[7],l[1],l[1],bAC[7],W[5],W[5],bAC[7],l[1],EexXZ,bAC[7],l[1],bAC[7]}, W[5], bOU[10:11], W[5], Q, {EexXZ,bOU[10:11],bAC[7],l[1],bAC[7],bOU[10:11],l[1],l[1],EexXZ,bOU[10:11],W[5],EexXZ,EexXZ,l[1],l[1],l[1],l[1],EexXZ,bOU[10:11],l[1]}, {eZ,mQNe[19:23],bAC[7],Q});

endmodule

// module oT
// size = 36 with 7 ports
module oT(oRFnA, cI, RA, man, B, Di, cy);
   output oRFnA;
   inout  cI;
   inout  RA;
   inout  man;
   input  B;
   output Di;
   input  cy;

   reg [3:22] oRFnA;
   wire       cI;
   wire [5:6] RA;
   wire       man;
   wire       B;
   wire [0:31] Di;
   wire [0:7]  cy;
   reg [16:19] PDpA;
   reg [2:7]   oUQ;
   wire [19:24] rio;
   reg [3:6]    b;
   reg [2:6]    NOy;
   reg [7:29]   F;
   wire [26:29] OV;
   reg [4:5]    eqZz_;
   reg [96:119] BmiY;
   reg [0:3]    k;
   reg          ivVu;
   reg [11:30]  DUNGc;
   reg [4:7]    Qi;
   reg [3:5]    tDgn;
   reg [0:7]    moai;
   wire [7:31]  vY;
   wire [2:4]   zYlde;
   reg [9:27]   lESa;
   wire         FUU;
   reg [0:0]    I [4:7]; // memory
   always @(negedge &Di[28:29])
     begin
        begin
           NOy[2:5] = (~vY[9:13]) + ((~((~{cI,Di[28:29]}))));
        end
     end

   HoAG X (RA[5], {RA[5:6],DUNGc[12:19],lESa[11:16],lESa[12:25],eqZz_[4]}, {RA[5:6],DUNGc[26:28],rio[23:24],RA[5:6],DUNGc[19:23],cy[1:4],NOy[2:5],cI,1'b1});

   always @(&OV[27:28])
     if (~OV[27:28])
       begin
          lESa[9:15] = ~{man,DUNGc[12:19]};
       end
     else
       begin
          lESa[9:15] = RA[5:6]^DUNGc[19:23];
       end

   ieumg zElH ({vY[24:28],DUNGc[26:28],I[6]/* memuse */,Di[28:29],1'b0}, OV[27:28]);

   jiV c (FUU, DUNGc[19:23], DUNGc[26:28]);

   C g ({RA[5],OV[26],FUU,RA[5]}, lESa[9:15], vY[24:28], zYlde[3]);

   always @(&Di[16:23])
     if (Di[16:23])
       begin
          I[7]/* memuse */ = ~10'b1000010001;
       end

   HoAG K (man, {I[7]/* memuse */,I[7]/* memuse */,vY[24:28],NOy[2:5],Di[28:29],Di[11:24],4'b0100}, {Di[28:29],vY[10:24],rio[23:24],eqZz_[4],I[6]/* memuse */,man,RA[5:6]});

   HoAG G_A (OV[26], {RA[5:6],cI,I[6]/* memuse */,Di[11:24],eqZz_[4],DUNGc[12:19],OV[27:28],2'b00}, {OV[27:28],DUNGc[26:28],DUNGc[26:28],DUNGc[26:28],I[7]/* memuse */,B,man,cy[1:4],Di[28:29],4'b0111});

   HoAG KZhJ (zYlde[3], {Di[16:23],DUNGc[12:21],DUNGc[12:21],eqZz_[4],cI,1'b0}, {eqZz_[4],I[7]/* memuse */,Di[28:29],lESa[12:25],OV[27:28],cy[1:4]});

   always @(negedge &DUNGc[19:23])
     begin
        begin
           oRFnA[9:17] = ~DUNGc[12:19]^Di[11:24];
        end
     end

   ieumg xfDtS ({I[6]/* memuse */,DUNGc[26:28],lESa[9:15],1'b1}, {I[7]/* memuse */,eqZz_[4]});

   ieumg d ({cy[1:4],I[7]/* memuse */,Di[28:29],vY[24:28]}, Di[28:29]);

   always @(negedge &vY[9:13])
     begin
        begin
        end
     end

   always @(posedge &NOy[2:5])
     begin
        begin
           k[1:3] = ~lESa[12:25];
        end
     end

   HoAG Rc (cI, {RA[5:6],Di[16:23],I[7]/* memuse */,oRFnA[9:17],NOy[2:5],cy[1:4],eqZz_[4],2'b00}, {k[1:3],RA[5:6],Di[28:29],DUNGc[12:21],lESa[9:15]});

   always @(negedge I[7]/* memuse */)
     begin
        begin
           if (DUNGc[12:19] == 8'b01000001)
             begin
                NOy[5:6] = 4'b0111;
             end
        end
     end

   assign RA[5] = ((k[1:3]^DUNGc[26:28]));

   GKI x_OS (cI, NOy[5:6]);

   always @(&DUNGc[19:23])
     if (~DUNGc[19:23])
       begin
          oRFnA[9:17] = vY[9:13];
       end

   always @(&DUNGc[12:21])
     if (DUNGc[12:21])
       begin
       end

   always @(posedge &OV[27:28])
     begin
        begin
           eqZz_[4] = ({OV[27:28],B});
        end
     end

   C hL ({RA[5],OV[26],man,man}, {vY[9:13],I[7]/* memuse */,I[7]/* memuse */}, vY[9:13], FUU);

   assign FUU = ~(~((oRFnA[9:17]^oRFnA[9:17])));

   always @(negedge &lESa[9:15])
     begin
        begin
        end
     end

   HoAG z (FUU, {oRFnA[9:17],vY[24:28],DUNGc[12:19],vY[9:13],Di[28:29],cI,1'b0}, {lESa[9:15],man,Di[11:24],I[7]/* memuse */,eqZz_[4]});

   assign rio[21:22] = ~oRFnA[9:17];

   ieumg h ({DUNGc[19:23],eqZz_[4],B,DUNGc[26:28],eqZz_[4],1'b1}, Di[28:29]);

   HoAG C (OV[26], {DUNGc[26:28],I[7]/* memuse */,eqZz_[4],Di[11:24],B,oRFnA[9:17],man,1'b0}, {DUNGc[12:19],DUNGc[19:23],I[7]/* memuse */,I[6]/* memuse */,lESa[9:15],RA[5:6]});

   always @(I[6]/* memuse */)
     if (I[6]/* memuse */)
       begin
          I[4]/* memuse */ = ((~cI));
       end

   assign cI = ~OV[27:28];

   C K_zF ({zYlde[3],rio[21:22],FUU}, lESa[9:15], vY[9:13], cI);

endmodule

// module ieumg
// size = 0 with 2 ports
module ieumg(VMm, mEAR);
   input VMm;
   input mEAR;

   wire [1:12] VMm;
   wire [4:5]  mEAR;
   reg [19:28] BbrC;
   reg [10:16] QRZoF;
   reg [2:4]   ESD [5:7]; // memory
   reg         Pzo;
   reg [2:4]   WSlJ;
   reg         L;
   wire [108:109] yXid;
endmodule

// module HoAG
// size = 24 with 3 ports
module HoAG(Nn, mW, LXxOi);
   output Nn;
   input  mW;
   input  LXxOi;

   wire   Nn;
   wire [0:30] mW;
   wire [5:28] LXxOi;
   reg [3:7]   eoTQ;
   reg [6:6]   tctl;
   reg         sQ;
   reg [6:6]   c [6:9]; // memory
   reg [6:7]   SUpL;
   reg         Vecfh;
   reg         I;
   reg         cLz;
   reg [3:6]   t [15:20]; // memory
   reg [1:6]   DY [65:70]; // memory
   reg [2:3]   VtCNT [3:5]; // memory
   reg [7:7]   msLYe;
   reg [2:7]   tt;
   reg         un;
   reg         zayB;
   wire        uHthT;
   reg         ehiS;
   wire [2:5]  QkVQ;
   wire        _tED;
   wire [0:3]  FD;
   reg [8:30]  cI;
   GKI zfJ (Nn, mW[26:27]);

   GKI jisgB (FD[0], SUpL[6:7]);

   assign      uHthT = (DY[68]/* memuse */ != 6'b010101) ? DY[70]/* memuse */ : 1'bz;

   GKI Cbv (_tED, mW[26:27]);

   GKI SLv (FD[2], SUpL[6:7]);

   C BF (QkVQ[2:5], {I,QkVQ[3],Nn,SUpL[6:7],Nn,cLz}, {SUpL[6:7],SUpL[6:7],QkVQ[3]}, FD[0]);

   GKI n (FD[2], mW[26:27]);

   assign      FD[2] = ~LXxOi[6:25];

   C UtIPz (QkVQ[2:5], {SUpL[6:7],LXxOi[26:27],LXxOi[26:27],QkVQ[3]}, {I,SUpL[6:7],tctl[6],Nn}, FD[2]);

   gSv yX (Nn, FD[2]);

   assign      uHthT = DY[65]/* memuse */;

   GKI q (uHthT, LXxOi[26:27]);

   GKI dr (Nn, mW[22:23]);

   GKI xJxeI (FD[2], mW[22:23]);

   assign      FD[0] = ~10'b1010010110;

   always @(negedge I)
     begin
        begin
           Vecfh = ~cLz;
        end
     end

   GKI Sou (_tED, mW[26:27]);

   gSv rUc (I, uHthT);

   C qbInR (QkVQ[2:5], {mW[22:23],cLz,Vecfh,QkVQ[3],LXxOi[26:27]}, {LXxOi[26:27],Vecfh,QkVQ[3],Vecfh}, FD[0]);

   C QsihR (QkVQ[2:5], {tctl[6],SUpL[6:7],I,I,cLz,I}, {Vecfh,LXxOi[26:27],SUpL[6:7]}, _tED);

endmodule

// module GKI
// size = 1 with 2 ports
module GKI(NV, ETkN);
   output NV;
   input  ETkN;

   reg    NV;
   wire [6:7] ETkN;
   reg [2:3]  ethhQ;
   reg        VZYT;
   wire       aLva;
   assign     aLva = ~ETkN[6];

endmodule

// module C
// size = 35 with 4 ports
module C(Fzsji, uV, kKlM, mjSiq);
   output Fzsji;
   input  uV;
   input  kKlM;
   inout  mjSiq;

   reg [1:4] Fzsji;
   wire [12:18] uV;
   wire [5:9]   kKlM;
   wire [18:18] mjSiq;
   reg [1:6]    nte;
   wire         XZk;
   reg [2:23]   A;
   reg [3:6]    xaARI;
   reg [1:4]    _Jb;
   reg [2:3]    tke;
   reg [20:27]  oAi;
   reg [3:5]    JKkm;
   reg [5:29]   lDJEB;
   wire [21:23] _npA;
   reg [1:1]    Iny;
   reg          I;
   wire [6:13]  eG;
   reg [3:5]    J;
   reg [4:6]    IohS;
   reg [3:6]    FDI;
   reg          EtMNf;
   reg          MLW;
   reg [7:27]   Qrz;
   reg          ml;
   reg [0:1]    Ye__;
   reg          go;
   reg [1:10]   DtkgI;
   gSv Ju (Qrz[22], mjSiq[18]);

   always @(&_npA[21:22])
     if (_npA[21:22])
       begin
       end
     else
       begin
          go = (~10'b1011111111);
       end

   wire rLVN = &nte[2:4];
   always @(posedge &lDJEB[26:28] or posedge rLVN)
     if (rLVN)
       begin
          begin
             go = ~Qrz[16:17];
          end
       end
     else
       begin
          begin
             J[4] = ~9'b001001110;
          end
       end

   always @(negedge J[4])
     begin
        begin
           EtMNf = oAi[25:26];
        end
     end

   jiV Pdx (mjSiq[18], {go,J[4],EtMNf,kKlM[5:6]}, uV[16:18]);

   gSv uLM (go, XZk);

   gSv u (mjSiq[18], mjSiq[18]);

   wire e = &Qrz[13:16];
   always @(posedge &Qrz[15:23] or negedge e)
     if (~e)
       begin
          begin
             nte[1:6] = _npA[22:23]^A[7:14];
             JKkm[3] = ~(~_npA[22:23]&go);
             lDJEB[6:9] = mjSiq[18];
          end
       end
     else
       begin
          begin
             Ye__[0:1] <= mjSiq[18];
          end
       end

   jiV jPg (mjSiq[18], {Ye__[0:1],Ye__[0:1],go}, _Jb[1:3]);

   gSv M (JKkm[3], _npA[23]);

   gSv cQZh (mjSiq[18], _npA[23]);

   assign XZk = ~A[7:14];

   jiV gocq (_npA[21], {_Jb[1:2],_npA[21:23]}, {_npA[21:22],mjSiq[18]});

   always @(&Qrz[16:17])
     if (Qrz[16:17])
       begin
       end

   assign eG[6:13] = (oAi[25:26]^_npA[22:23]);

   gSv yBCgC (JKkm[3], _npA[23]);

   jiV lQuq (XZk, {mjSiq[18],Ye__[0:1],Qrz[22],Qrz[22]}, lDJEB[26:28]);

   XK K ({Qrz[16:17],lDJEB[26:28],oAi[25:26],mjSiq[18]}, mjSiq[18], /* unconnected */, {_npA[21],_npA[21],mjSiq[18],mjSiq[18],_npA[21]});

   always @(negedge &lDJEB[6:9])
     begin
        begin
           lDJEB[8:16] <= {Qrz[22],lDJEB[26:28]};
        end
     end

   jiV zf (XZk, {lDJEB[6:9],go}, _Jb[1:3]);

   assign eG[6:13] = Qrz[22]^kKlM[5:6];

   jiV hOSH (_npA[21], {_npA[21:22],JKkm[3],Ye__[0:1]}, {go,_npA[21:22]});

   jiV LGB (_npA[21], {EtMNf,uV[16:18],mjSiq[18]}, nte[2:4]);

   gSv F (go, XZk);

   wire   KBf = go;
   always @(posedge &lDJEB[6:9] or posedge KBf)
     if (KBf)
       begin
          begin
             JKkm[3] = ~nte[3:6];
          end
       end
     else
       begin
          begin
             EtMNf = ~lDJEB[6:9]&_Jb[1:2];
          end
       end

endmodule

// module gSv
// size = 3 with 2 ports
module gSv(no, QZVP);
   input no;
   inout QZVP;

   wire  no;
   wire [11:11] QZVP;
   reg          a;
   reg          sAC;
   assign       QZVP[11] = ~no;

   always @(posedge no)
     begin
        begin
           a <= ~(~({QZVP[11],QZVP[11]}));
        end
     end

   always @(posedge a)
     begin
        begin
           a = ~a;
        end
     end

endmodule

// module XK
// size = 5 with 4 ports
module XK(mTz, n, rtnL, HFto);
   input mTz;
   input n;
   inout rtnL;
   output HFto;

   wire [0:7] mTz;
   wire [0:0] n;
   wire [0:4] rtnL;
   wire [3:7] HFto;
   reg [32:56] _W;
   reg [3:3]   MDmQ;
   assign      HFto[5:6] = ~mTz[5:7]&n[0];

   assign      HFto[3:5] = rtnL[0];

   always @(posedge n[0])
     begin
        begin
           MDmQ[3] = n[0];
        end
     end

   assign HFto[5:6] = (rtnL[0] != 1'b0) ? (~{mTz[5:7],MDmQ[3]}) + (10'b0000100111) : 2'bz;

   wire   idr_ = rtnL[0];
   always @(negedge &mTz[5:7] or posedge idr_)
     if (idr_)
       begin
          begin
             MDmQ[3] = MDmQ[3];
          end
       end
     else
       begin
          begin
             _W[34:37] = ~(rtnL[0]|MDmQ[3]);
          end
       end

endmodule

// module jiV
// size = 4 with 3 ports
module jiV(bJd, Tod, obpQs);
   inout bJd;
   input Tod;
   input obpQs;

   wire  bJd;
   wire [0:4] Tod;
   wire [0:2] obpQs;
   wire [0:4] AxQX;
   reg [37:102] zxxP;
   reg [18:28]  ZJ;
   always @(negedge &Tod[3:4])
     begin
        begin
           ZJ[24:26] = ~1'b0;
        end
     end

   always @(&Tod[1:4])
     if (Tod[1:4])
       begin
          zxxP[51:77] = Tod[2:3];
       end

   assign AxQX[2:3] = ~Tod[2:3];

   assign AxQX[2:3] = ~7'b1101011;

endmodule
