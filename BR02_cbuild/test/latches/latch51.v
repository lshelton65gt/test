// this tests our handling of latches that will never load
module top(clk, en, l1, l2);
input clk, en;
output l1, l2;

   // Create flopped version of the inputs because of the way we deal
   // with clocks and input flow. If the inputs are used directly,
   // Aldec would see a glitch because clk transitions are delayed 1ns.
   reg dclk, ren;
   initial begin
      dclk = 0;
      ren = 0;
   end
   always @ (posedge clk)
     dclk <= ~dclk;
   always @ (posedge clk)
     ren = 0;  // Note: ren is constant 0

wire l1;
wire l2;

latch L1 (dclk, ren, l1, ~l2);

wire gclk = dclk & ren;
latch L2 (~gclk, 1'b1, l2, l1);

endmodule

module latch(clk, gate, q, d);
input clk, gate, d;
output q;

reg q;

initial q = 0;

always @(clk or gate or d)
  if (clk & gate)
    q = d;

endmodule
