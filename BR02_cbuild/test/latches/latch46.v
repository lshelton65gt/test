// this tests our ability to prove exclusivity using clock equivalence 
// we should be able to prove exclusivity with and without -noFlatten
module top(clk, en, l1, l2);
input clk, en;
output l1, l2;

   // Create flopped version of the inputs because of the way we deal
   // with clocks and input flow. If the inputs are used directly,
   // Aldec would see a glitch because clk transitions are delayed 1ns.
   reg dclk;
   reg ren;
   initial begin
      dclk = 0;
      ren = 0;
   end
   always @ (posedge clk)
     dclk <= ~dclk;
   always @ (posedge clk)
     ren <= en;

wire l1;
wire l2;

latch L1 (dclk,  ren, l1, ~l2);
latch L2 (~dclk, ren, l2, l1);

endmodule

module latch(clk, gate, q, d);
input clk, gate, d;
output q;

reg q;

initial q = 0;

always @(clk or gate or d)
  if (clk & gate)
    q = d;

endmodule
