module top (out1, out2, clk, in);
   output out1, out2;
   input  clk, in;

   reg 	  latch1, latch2;
   always @ (clk or in)
     if (clk)
       latch1 = in;
     else
       latch2 = in;

   reg 	  out1, out2;
   always @ (negedge clk)
     out1 = latch1;
   always @ (posedge clk)
     out2 = latch2;

endmodule // top
