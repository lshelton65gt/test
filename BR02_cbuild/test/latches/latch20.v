// l1 is exclusive, but l2 and l3 are not.
// l1 is a latch with a conditional enable
module top(cond, en1, en2, l1, l2, l3, val);
input cond, en1, en2, val;
output l1, l2, l3;

reg l1, l2, l3;

   initial begin
      l1 = 1;
      l2 = 0;
      l3 = 1;
   end

always @ (cond or en1 or l2 or en2 or l2)
begin
  if (cond)
  begin
    if (en1) 
      l1 = l3;
  end
  else
  begin
    if (en2)
      l1 = l2;
  end
end

always @ (en2 or l1)
begin
  if (~en2)
    l2 = ~l1;
end

always @ (en1 or val)
begin
  if (en1)
  begin
  end
  else
    l3 = ~val;
end

endmodule
