module buX(G, sSb, UV, hpimX, aaAje, as, m);
input G;
inout sSb;
input UV;
input hpimX;
inout aaAje;
output as;
input m;

wire [4:5] G;
wire [5:7] sSb;
wire [2:5] UV;
wire [19:28] hpimX;
wire [2:18] aaAje;
reg [1:3] as;
wire [3:9] m;
wire i;
_Ahu jT ({aaAje[15:16],G[4],UV[2:5],UV[2:5],UV[2:5]}, {G[4],hpimX[19]}, /* unconnected */, sSb[5], i, hpimX[19], {sSb[5],sSb[5],m[5:7],hpimX[19]}, hpimX[19], i);

assign sSb[5:7] = ~sSb[5]^i;

assign i = ~i;

endmodule


module _Ahu(xyd, _XJM, V, pcz_O, J, Mdg, c, NWEXC, zzT);
input xyd;
input _XJM;
inout V;
input pcz_O;
output J;
input Mdg;
input c;
input NWEXC;
output zzT;

wire [7:21] xyd;
wire [4:5] _XJM;
wire [16:30] V;
wire [7:7] pcz_O;
wire [0:5] c;
reg n;

WPH DuhBE ({xyd[8:10],xyd[8:10],V[22:26],NWEXC}, c[1]);

always @(c[1])
if (c[1])
begin
if (NWEXC == 1'b0)
begin
end
end
else
begin
n = ~xyd[8:10]^_XJM[4:5];
end

always @(posedge n)
begin
begin
n = pcz_O[7]^n;
end
end

// GATED-CLOCK
wire o = &xyd[8:10]; // clock
wire Sg = n|NWEXC; // enable data
reg PYE;
always @(o or Sg) begin
  if (o) begin
    PYE = Sg; // enable assign
  end
end
wire brW = ~o & PYE; // gated clock

always @(brW)
if (~brW)
begin
$finish;
end

endmodule

module WPH(vLfB, Ge);
input vLfB;
input Ge;

wire [13:24] vLfB;
wire Ge;
reg [11:13] fln_;
wire Kr;
reg rm;
wire VTf;
wire [20:29] _Y;

sXzgd OPm (fln_[11:12], {Kr,VTf,_Y[20:22]}, {rm,vLfB[14:16]});

sXzgd MU (fln_[11:12], {_Y[24:27],VTf}, {Ge,VTf,Kr,rm});

endmodule

module sXzgd(I, Ij, JBhhg);
input I;
output Ij;
input JBhhg;

wire [5:6] I;
reg [0:4] Ij;
wire [1:4] JBhhg;

endmodule

