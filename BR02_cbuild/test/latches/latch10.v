module testcase (in,sel,out);
  input [1:0] in;
  input [1:0] sel;
  output [1:0] out;
  reg [1:0] out;

  always @(sel or in)
    case (sel)
      0: out = in;
      1: out = ~in;
      2: out = {in[0],in[1]};
      7: out = {2'b0};
    endcase
  endmodule



