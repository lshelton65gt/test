module top (out1, out2, out3, clk, in);
   output out1, out2, out3;
   input  clk, in;

   wire   gclk;
   sub S1 (gclk, clk, 1'b1);
   sub S2 (gclk, clk, 1'b1);
   sub S3 (out2, 1'b1, in);

   reg    r1, r2, out1;
   initial r1 = 1;
   initial r2 = 1;
   initial out1 = 0;
   always @ (gclk or in)
     if (gclk)
       r1 <= in;
   always @ (clk or r1)
     if (clk)
       r2 <= r1;
   always @ (gclk or r2)
     if (gclk)
       out1 <= ~r2;

   reg    out3;
   initial out3 = 0;
   always @ (posedge clk)
     out3 <= in;

endmodule

module sub (o, i1, i2);
   output o;
   input  i1, i2;

   assign o = ~(i1 & i2);

endmodule
