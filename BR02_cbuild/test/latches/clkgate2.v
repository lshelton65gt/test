// Simple test of converting a latch used to gate a clock into a flop
// The latch should be converted into a negedge clk flop because that
// is when the latch closes.

module top (out, clk, en, in);
   output out;
   input  clk, en, in;

   reg    ren;
   reg    out;
   wire   gclk;
   
   // Sync the input enable
   reg    sen;
   always @ (posedge clk)
     sen <= en;

   always @ (clk or sen)
     if (clk)
       ren <= sen;
   assign gclk = ~clk & ren;

   initial out = 0;
   always @ (posedge gclk)
     out <= in;

endmodule
