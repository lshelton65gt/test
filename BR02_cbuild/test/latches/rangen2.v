// Generated with: ./rangen -s 154 -n 0x3a61002d -o /w/xenon/rsayde/build/randbugs/obj/Linux.product/test/rangen/rangen/testcases.053107/cbld/assert4.v
// Using seed: 0x3a61002d
// Writing cyclic circuit of size 154 to file '/w/xenon/rsayde/build/randbugs/obj/Linux.product/test/rangen/rangen/testcases.053107/cbld/assert4.v'
// Module count = 7

// The way latch conversion works, it does more aggresive
// expression folding during conversion than during clock
// analysis. This means that during the conversion process we
// could determine that two latch clocks are the same constant
// and replace it with the same net. The result is that when we
// replace the latch conditions with constants, it can find the
// same clock expression in the translation map. But it must
// always be replaced with the same value.
//
// The fix was to replace the restrictive assert with a check
// to make sure if we have a duplicate we replace it with the
// same constant value.


// module T0O
// size = 154 with 9 ports
module T0O(Day2, jn3Hh, iJ_n, V, TJ, Y, _, ck, r);
input Day2;
input jn3Hh;
input iJ_n;
input V;
input TJ;
input Y;
input _;
inout ck;
output r;

wire [2:6] Day2;
wire jn3Hh;
wire iJ_n;
wire [23:48] V;
wire [2:7] TJ;
wire [3:5] Y;
wire [2:5] _;
wire [2:4] ck;
reg r;
wire [20:22] e;
reg [9:9] pWnQ;
reg [6:6] BXE;
reg RNG;
reg [29:30] g;
wire olkSJ;
reg [47:89] fCT;
reg [4:6] eagb;
reg Ik;
reg [2:11] WoRl;
reg xQEG5;
reg G2J5T;
reg [15:89] oGs9;
reg [2:5] d [5:7]; // memory
reg [3:5] BWgE;
reg f;
wire [15:19] OGbM;
reg [4:11] S13X;
reg [8:16] UKLDs;
wire [10:14] BCEL;
reg Hu;
reg [1:1] tqcE;
reg [27:30] ul;
reg [9:23] h;
reg [28:30] WTal;
wire L;
reg [0:0] vfS0;
wire CvBWC;
reg WfTj;
wire t;
reg [2:2] f9ypR;
reg [1:3] ok;
reg [0:7] rNct2;
wire YfFdG;
reg [53:89] mQ;
reg iAu;
reg [10:31] RJlv;
reg F;
reg [4:6] W6ZS;
reg uv;
reg [12:73] x [5:5]; // memory
reg [11:16] DV;
reg [10:31] l2k [1:5]; // memory
reg [0:7] OCN;
reg QV;
wire [0:2] kbN;
wire NQ;
reg [17:24] j3rVM [85:87]; // memory
wire RpL;
reg [3:12] AbtNt;
reg [0:2] WgJV;
wire cOh;
reg b;
reg pQCW;
wire cU;
reg [45:93] oPp;
reg [4:6] Kyz;
reg [56:76] fhZ;
reg P;
reg ms4tg;
reg [22:27] l1u;
reg [4:10] uqntR;
wire Njg;
reg eWotFi;
reg [26:106] rt2k;
reg [1:2] C [0:2]; // memory
reg [4:6] OcRA;
reg [22:29] z7HZ;
reg [3:4] z;
reg Mh2Et;
wire Ht;
reg [2:2] XdI;
wire [0:1] nzkxg;
reg [0:0] Q5V [3:30]; // memory
reg dgt1s;
reg [3:4] Zzzr;
reg tVqQ;
reg [9:24] j;
wire [22:27] tNQ6;
wire [1:18] q0;
reg [1:5] a;
reg [99:127] n1;
wire Fj;
reg [16:26] _AF;
reg [5:31] P4C;
reg [21:27] Xyx;
reg [0:1] oSg;
wire [2:5] ZNL67;
wire [12:108] _VzM;
reg eo;
reg [6:6] nYBTU;
wire [2:28] SP8W4s;
reg [2:6] J;
reg [0:6] v;
reg [1:3] U;
reg EE5T;
reg B8mT;
reg v7qx;
wire pajC;
wire Y1PQ;
wire [1:7] e4v;
reg [12:24] V3;
reg av;
reg [1:28] DkcGh;
reg [2:3] B2;
reg [2:4] A_f_;
reg WgHf;
reg [0:0] gGB [1:20]; // memory
reg [9:31] fv;
reg [5:6] r5c_;
reg [10:19] PgO;
reg aZsm;
wire WTq;
reg jk6Mv;
reg [16:23] V4;
reg mI;
wire [21:30] sSdSA;
reg [7:25] CYYz1;
reg [3:14] BvFFv;
wire I;
always @(negedge Q5V[19]/* memuse */)
begin
begin
{ DkcGh[7:10], gGB[1]/* memuse */, _AF[22:23], Kyz[4:5], ms4tg } = (9'b001110010);
end
end

wire jS2 = &Y[3:4];
always @(posedge WfTj or negedge jS2)
if (~jS2)
begin
begin
Mh2Et = Q5V[8]/* memuse */|ZNL67[2:3];
end
end
else
begin
begin
n1[106:113] = ~(Zzzr[3:4]^Q5V[7]/* memuse */);
end
end

always @(&Day2[2:5])
if (~Day2[2:5])
begin
z[3:4] = Q5V[24]/* memuse */;
end
else
begin
end

always @(negedge &ZNL67[2:3])
begin
begin
B8mT = nzkxg[0];
end
end

always @(negedge nzkxg[0])
begin
begin
DV[11:12] = ~Q5V[16]/* memuse */;
end
end

assign OGbM[17:18] = ~5'b11010;

always @(posedge Q5V[8]/* memuse */)
begin
begin
end
end

eF mIF ({DkcGh[13:18],_[4:5],YfFdG,SP8W4s[12:26],XdI[2],Q5V[24]/* memuse */,XdI[2],37'b0000000110011000011000010100000001110}, f, dgt1s);

always @(negedge &Day2[3:4])
begin :v1EWf
reg ihM;
reg [19:24] uAL;
reg [2:6] B6;

begin
l2k[2]/* memuse */ = ~kbN[0:2]&Day2[3:4];
end
end

qj o0s ({ms4tg,q0[1:4],Zzzr[3:4],Q5V[11]/* memuse */,nYBTU[6],Kyz[4:5],5'b10011}, pajC, {_[4:5],XdI[2],Q5V[19]/* memuse */,oGs9[33:46],V[28:30],WgJV[0:2],kbN[0:2],v[0:4],CYYz1[19],7'b1111010});

always @(posedge EE5T)
begin :zK
reg [0:2] gecg;

begin
P4C[6:22] = ~Njg;
end
end

assign q0[7:11] = ((~{EE5T,OGbM[17:18]}) + (~jn3Hh&jn3Hh));

assign ZNL67[3:5] = _VzM[93:102];

always @(negedge &n1[106:113])
begin
begin
AbtNt[3:4] = q0[1:11]|x[5]/* memuse */;
end
end

assign _VzM[27:65] = (n1[103:113]|_AF[22:23]);

always @(&oGs9[56:84])
if (~oGs9[56:84])
begin
end

assign nzkxg[1] = (Njg != 1'b0) ? 1'b0 : 1'bz;

zja vV2 (Fj, CYYz1[13:15], Q5V[22]/* memuse */);

always @(posedge &ck[2:3])
begin
begin
P = 9'b011101111;
end
end

assign OGbM[17:18] = ~(l1u[23:27]);

assign kbN[0:1] = ~gGB[1]/* memuse */^e[20];

// GATED-CLOCK
wire WzkC = &q0[1:11]; // clock
wire vFtmH = _VzM[50:93]&jk6Mv; // enable data
reg JNjJK;
always @(WzkC or vFtmH) begin
  if (~WzkC) begin
    JNjJK = vFtmH; // enable assign
  end
end
wire ga = WzkC & JNjJK; // gated clock
always @(negedge ga)
begin
begin
gGB[1]/* memuse */ = ~10'b1011010001;
end
end

always @(Q5V[11]/* memuse */)
if (Q5V[11]/* memuse */)
begin
{ jk6Mv, rNct2[2], W6ZS[5], pWnQ[9], OCN[0], AbtNt[8] } = CYYz1[19]|DkcGh[7:10];
end

always @(&l1u[22:27])
if (l1u[22:27])
begin
eo = (WzkC);
end
else
begin
if (V[46:47] == 2'b00)
begin
UKLDs[10] = ~_VzM[53:102];
end
else
begin
end
end

assign { pajC, t, OGbM[17:18], ZNL67[3:5], L } = Q5V[19]/* memuse */;

always @(posedge &oGs9[54:68])
begin :GFdQf

begin
end
end

always @(negedge Q5V[11]/* memuse */)
begin :ZBO
reg zMB;
reg [0:3] ZPH8t;
reg [6:6] tj2Kg;

begin
for (ok[2:3]=2'b10; ok[2:3]<2'b10; ok[2:3]=ok[2:3]+2'b10)
begin
uqntR[9:10] = _VzM[18:48]^Q5V[16]/* memuse */;
end
end
end

always @(negedge &S13X[4:6])
begin
begin
xQEG5 = ~Q5V[8]/* memuse */;
end
end

assign _VzM[24:80] = (~jk6Mv);

always @(posedge &ck[3:4])
begin :CUP
reg [6:17] A;
reg An;
reg B;
reg [3:7] Er5;

begin
B = ~8'b00101011;
end
end

assign kbN[0:1] = Kyz[4:6]|Q5V[28]/* memuse */;

always @(posedge &AbtNt[3:4])
begin
begin
{ fv[18:22], b, jk6Mv, fhZ[58:69], BWgE[3:5], pWnQ[9], B2[3] } = ~oGs9[23:67];
end
end

always @(&fv[12:14])
if (~fv[12:14])
begin
l2k[2]/* memuse */ = f;
end
else
begin
end

always @(posedge eo)
begin
begin
{ Q5V[7]/* memuse */, oGs9[26:53] } = ~DV[11:12];
end
end

assign olkSJ = SP8W4s[12:26]&_VzM[27:65];

assign Njg = pWnQ[9]^OCN[0:4];

always @(posedge AbtNt[8])
begin
begin
BWgE[3:5] = {Q5V[24]/* memuse */,Q5V[12]/* memuse */};
end
end

assign _VzM[27:65] = ~DkcGh[13:18];

always @(&_VzM[18:48])
if (_VzM[18:48])
begin
end
else
begin
{ dgt1s, tqcE[1], V3[13:22], CYYz1[8:13], iAu, rNct2[2], RJlv[31], gGB[4]/* memuse */, b } = ~a[2:3];
end

wire pCY0 = &OGbM[17:18];
always @(posedge tqcE[1] or negedge pCY0)
if (~pCY0)
begin
begin
{ oGs9[26:53], mI, F, QV, Ik, aZsm } = ~tqcE[1]^oGs9[33:46];
end
end
else
begin
begin
pQCW = Q5V[19]/* memuse */&rNct2[2];
end
end

assign olkSJ = 2'b00;

always @(negedge eo)
begin
begin
end
end

assign I = ~OGbM[17:18]^eagb[4:5];

always @(&DkcGh[13:15])
if (~DkcGh[13:15])
begin
Hu = ~(10'b1100001100);
end

always @(posedge Q5V[11]/* memuse */)
begin
begin
RJlv[19:29] = 6'b100000;
end
end

assign CvBWC = ~OGbM[17:18];

always @(xQEG5)
if (~xQEG5)
begin
uqntR[9:10] = ga;
end

always @(&kbN[0:2])
if (kbN[0:2])
begin
aZsm = (~9'b101110101);
end
else
begin
fv[18:22] = EE5T;
end

always @(negedge &_[4:5])
begin
begin
end
end

always @(Q5V[16]/* memuse */)
if (~Q5V[16]/* memuse */)
begin
{ f, rt2k[28:59], fCT[73:85], z7HZ[22:25], BWgE[5], fv[18:22], U[1:2], DV[11:12] } = ~Day2[3:4];
end
else
begin
if (SP8W4s[12:26] == 15'b101101110100010)
begin
rt2k[28:59] = ~_VzM[31:48];
end
pQCW = ~fCT[47:89];
end

zja sN (L, ZNL67[3:5], pQCW);

always @(posedge xQEG5)
begin
begin
{ d[7]/* memuse */, iAu, aZsm, Q5V[7]/* memuse */, S13X[5:6], A_f_[3:4], x[5]/* memuse */, DkcGh[5:13], QV } = (l1u[24:25]);
end
end

always @(posedge &_VzM[64:94])
begin :PK2t
reg v6Mb;

begin
S13X[5:6] = Day2[2:5];
end
end

wire WWHg = NQ;
always @(negedge nYBTU[6] or negedge WWHg)
if (~WWHg)
begin
begin
l2k[2]/* memuse */ = Q5V[28]/* memuse */;
end
end
else
begin
begin
end
end

assign pajC = ~(~(((~pQCW&iAu))));

always @(posedge NQ)
begin :X5gy6d
reg ETBt;
reg [2:2] sp1_Y;

begin
if (CYYz1[21] == 1'b1)
begin
uqntR[9:10] = 8'b01011100;
end
end
end

always @(CvBWC)
if (~CvBWC)
begin
{ l1u[26:27], dgt1s } = (~x[5]/* memuse */);
l2k[2]/* memuse */ = JNjJK&pajC;
end
else
begin
z[3:4] = ~DV[11:12];
end

assign _VzM[24:80] = _[4:5];

assign q0[7:11] = Njg;

assign pajC = xQEG5;

assign sSdSA[28] = 5'b10101;

assign Ht = ~iJ_n^P;

always @(posedge &x[5]/* memuse */)
begin
begin
{ WgHf, _AF[22:23], J[2:4], h[17:22], QV } = ~Hu;
end
end

assign { e[20], _VzM[27:65], _VzM[24:80] } = ~YfFdG^_VzM[45:47];

assign t = 6'b011011;

assign nzkxg[1] = (iJ_n != 1'b1) ? CvBWC : 1'bz;

assign _VzM[27:65] = ~nzkxg[1]^Y[3:4];

always @(&eagb[4:5])
if (~eagb[4:5])
begin
_AF[22:23] = 3'b101;
end

always @(posedge &AbtNt[3:4])
begin :by
reg [21:40] Dj [4:5]; // memory

begin
end
end

// GATED-CLOCK
wire rp50f = dgt1s; // clock
wire dOYa = ~_AF[22:23]|Q5V[7]/* memuse */; // enable data
reg jzGac;
always @(rp50f or dOYa) begin
  if (rp50f) begin
    jzGac = dOYa; // enable assign
  end
end
wire CbmQ = ~rp50f & jzGac; // gated clock
always @(posedge CbmQ)
begin
begin
case (l2k[2]/* memuse */)
22'd0:
begin
WfTj = (~b^pajC);
end
22'd1:
begin
end
22'd2:
begin
$display("module: %s, value: %b",~Day2[3:4]&rNct2[2])
;end
22'd3:
begin
DkcGh[7:15] = (~gGB[4]/* memuse */^_VzM[18:48]);
end
22'd4:
begin
end
22'd5:
begin
{ eagb[5], rt2k[50:75], DkcGh[2:13], Q5V[7]/* memuse */ } = EE5T&l2k[2]/* memuse */;
end
22'd6:
begin
end
22'd7:
begin
uqntR[9:10] = ~5'b01001;
end
22'd8:
begin
P4C[6:22] = ~kbN[0:1];
end
22'd9:
begin
end
default:
begin
DkcGh[5:13] = ~((Q5V[3]/* memuse */));
end
endcase
end
end

assign { CvBWC, nzkxg[1], olkSJ, kbN[0:1], Njg } = S13X[4:6]^AbtNt[11:12];

assign Njg = P4C[6:22];

assign L = ~(~_VzM[93:102]) + ((~(~{aZsm,Zzzr[3:4]})));

wire Z = rNct2[2];
always @(negedge Q5V[7]/* memuse */ or negedge Z)
if (~Z)
begin
begin
pQCW = (~dOYa^tqcE[1]);
AbtNt[5:10] = ~aZsm;
end
end
else
begin
begin
$display("module: %s, value: %b",~sSdSA[28]|CYYz1[13:15])
;end
end

assign { WTq, Fj, L, _VzM[24:80], t, e[20], tNQ6[24:26] } = ~uqntR[9:10];

always @(posedge &J[2:5])
begin
begin
BXE[6] = x[5]/* memuse */^kbN[0:2];
BvFFv[7:13] = fhZ[58:69]&iJ_n;
end
end

assign { ck[2], e[20], L, SP8W4s[16:21] } = (JNjJK^L);

always @(jk6Mv)
if (jk6Mv)
begin
fCT[73:85] = (~(~SP8W4s[16:21]&fCT[73:85]));
end

assign YfFdG = 7'b0010110;

always @(posedge pWnQ[9])
begin :IXAX3
reg IE9A;

begin
Hu = SP8W4s[12:19];
end
end

assign e[20] = (V[28:30] != 3'b101) ? ~e[20]^kbN[0:1] : 1'bz;

always @(posedge Q5V[16]/* memuse */)
begin
begin
end
end

always @(posedge jn3Hh)
begin :t7

begin
end
end

always @(&z7HZ[22:25])
if (~z7HZ[22:25])
begin
fv[18:22] = Day2[2:5];
end
else
begin
$display("module: %s, value: %b",~CvBWC^_VzM[18:48])
;end

jERnR A8Ra ({gGB[4]/* memuse */,sSdSA[28],Kyz[4:5],f,1'b0}, {SP8W4s[13:16],ZNL67[3:5],cU,cU,BCEL[12:13],NQ,RpL}, _VzM[70:73]);

always @(cOh)
if (~cOh)
begin
P4C[6:22] = ~CYYz1[13:23];
end
else
begin
oGs9[26:53] = ~(BvFFv[7:13]);
end

always @(b)
if (~b)
begin
DkcGh[7:15] = Q5V[22]/* memuse */;
end

assign olkSJ = d[7]/* memuse */;

always @(posedge &l2k[2]/* memuse */)
begin
begin
WfTj = olkSJ;
end
end

always @(P)
if (~P)
begin
b = ~WfTj;
end
else
begin
AbtNt[8] = olkSJ|_VzM[83:90];
end

assign ZNL67[3:5] = ~pQCW^_VzM[24:80];

always @(posedge Q5V[7]/* memuse */)
begin
begin
$write("%x\n",a[2:3]);
end
end

assign _VzM[28:70] = ~9'b010011100;

assign tNQ6[24:26] = ~6'b110101;

always @(&rt2k[28:59])
if (~rt2k[28:59])
begin
$display("module: %s, value: %b",q0[1:4])
;end
else
begin
r = ~pajC;
end

always @(posedge Q5V[28]/* memuse */)
begin
begin
j3rVM[85]/* memuse */ = ~jk6Mv;
end
end

assign Y1PQ = ~cOh;

always @(posedge &_AF[22:23])
begin
begin
DkcGh[14:28] = (J[2:4]);
end
end

wire iY = &J[2:5];
always @(negedge Q5V[28]/* memuse */ or posedge iY)
if (iY)
begin
begin
end
end
else
begin
begin
end
end

always @(&ZNL67[3:5])
if (ZNL67[3:5])
begin
aZsm = (CbmQ);
end

wire OXT = b;
always @(negedge &_AF[22:23] or negedge OXT)
if (~OXT)
begin
begin
case (Q5V[7]/* memuse */)
1'd0:
begin
end
1'd1:
begin
$display("module: %s, value: %b",~CvBWC)
;end
endcase
end
end
else
begin
begin
A_f_[3:4] = ~(Q5V[7]/* memuse */) + (~iJ_n);
end
end

always @(negedge I)
begin
begin
gGB[4]/* memuse */ = ~2'b10;
end
end

assign { RpL, I, tNQ6[24:26], SP8W4s[13:16], OGbM[17:18], q0[7:11], CvBWC, _VzM[70:73], cOh, SP8W4s[17:25] } = ~(~(~oGs9[26:53]));

assign q0[7:11] = ~jk6Mv;

assign SP8W4s[13:16] = ~_VzM[27:65]^fCT[47:89];

assign q0[7:11] = (dgt1s != 1'b0) ? ~Mh2Et : 5'bz;

// GATED-CLOCK
wire S2qt = Hu; // clock
wire vhQv = dgt1s; // enable data
reg o_;
always @(S2qt or vhQv) begin
  if (S2qt) begin
    o_ = vhQv; // enable assign
  end
end
wire OOUx = ~S2qt & o_; // gated clock
always @(posedge OOUx)
begin
begin
G2J5T = ~jk6Mv;
end
end

always @(posedge &WgJV[0:2])
begin
begin
WgHf = ~(~(~fCT[73:85]));
end
end

assign Y1PQ = (BWgE[3:5]^_VzM[27:65]);

always @(XdI[2])
if (~XdI[2])
begin
V4[18:21] = ~J[2:5];
end

always @(negedge &_VzM[45:47])
begin
begin
AbtNt[8] = rNct2[2];
end
end

assign Fj = ~OGbM[17:18]^j3rVM[85]/* memuse */;

always @(pWnQ[9])
if (~pWnQ[9])
begin
end
else
begin
end

assign e[20] = ~6'b010110;

always @(posedge &_VzM[19:39])
begin
begin
U[1:2] = F;
end
end

always @(negedge &kbN[0:1])
begin
begin
S13X[5:6] = _VzM[24:100];
end
end

// GATED-CLOCK
wire MXOL = &AbtNt[11:12]; // clock
wire pjg = Q5V[11]/* memuse */; // enable data
reg fe_V;
always @(MXOL or pjg) begin
  if (MXOL) begin
    fe_V = pjg; // enable assign
  end
end
wire Bggu = ~MXOL & fe_V; // gated clock
always @(posedge Bggu)
begin
begin
end
end

assign RpL = ~BXE[6]^ZNL67[3:5];

endmodule

// module jERnR
// size = 1 with 3 ports
module jERnR(Vhp, Fi7CE, E4ui);
input Vhp;
output Fi7CE;
output E4ui;

wire [1:6] Vhp;
reg [3:15] Fi7CE;
wire [4:7] E4ui;
// GATED-CLOCK
wire x = &Fi7CE[9:15]; // clock
wire gt2e = Vhp[3]|Fi7CE[9:15]; // enable data
reg vDW;
always @(x or gt2e) begin
  if (~x) begin
    vDW = gt2e; // enable assign
  end
end
wire hhHn = x & vDW; // gated clock
always @(negedge hhHn)
begin
begin
Fi7CE[7:9] = ((~Vhp[3]|gt2e));
end
end

endmodule

// module zja
// size = 5 with 3 ports
module zja(Ql5, A, l);
output Ql5;
input A;
input l;

reg Ql5;
wire [1:3] A;
wire [0:0] l;
reg uB7zn;
reg cG9H;
wire [3:5] V;
wire [54:108] AVnp;
assign V[4] = ~A[2:3]|A[2:3];

assign AVnp[75:93] = A[2]&Ql5;

always @(V[4])
if (V[4])
begin
{ cG9H, Ql5, uB7zn } = A[2:3];
end

always @(negedge cG9H)
begin
begin
Ql5 = A[2];
end
end

assign V[4] = ~1'b1;

endmodule

// module qj
// size = 1 with 3 ports
module qj(bBdH, E0hL1, ntcLe);
input bBdH;
output E0hL1;
input ntcLe;

wire [7:22] bBdH;
wire E0hL1;
wire [4:43] ntcLe;
reg [4:5] RvHX;
// GATED-CLOCK
wire K = &ntcLe[15:28]; // clock
wire hWb2f = ~8'b10101011; // enable data
reg J;
always @(K or hWb2f) begin
  if (~K) begin
    J = hWb2f; // enable assign
  end
end
wire fmPKI = K & J; // gated clock
always @(negedge fmPKI)
begin
begin
end
end

endmodule

// module eF
// size = 1 with 3 ports
module eF(bLiJF, pLW, ylO);
input bLiJF;
input pLW;
input ylO;

wire [57:120] bLiJF;
wire pLW;
wire ylO;
wire [1:2] zRQo;
reg [5:7] xVW7m;
always @(posedge &xVW7m[5:7])
begin
begin
end
end

always @(posedge ylO)
begin :Fy
reg [0:2] Qc [0:2]; // memory
reg [60:102] BjZo;

begin
xVW7m[6:7] = ~ylO;
end
end

endmodule
