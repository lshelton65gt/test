// l1 and l2 are both exclusive
module top(clk, cond_in, l1, l2);
input clk, cond_in;
output l1, l2;

reg l1, l2;
reg dclk, cond;

  initial begin
    l1 = 0;
    l2 = 0;
    dclk = 0;
    cond = 0;
  end

  always@(posedge clk)
  begin
    dclk <= ~dclk;
    cond <= cond_in;
  end

always @ (cond or dclk or l2)
begin
  if (cond)
  begin
    if (dclk)
      l1 = l2;
  end
  else    
  begin
    if (dclk)
      l1 = ~l2;
  end
end

always @ (dclk or l1)
begin
  if (~dclk)
    l2 = l1;
end

endmodule
