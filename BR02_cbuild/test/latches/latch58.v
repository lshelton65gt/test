// latches l1, l2 and l3 are all exclusive
module top(clk, l1, l2, l3);
input clk;
output l1, l2, l3;

  reg [1:0] l1;
  reg l2, l3;

  reg dclk;
  initial
  begin
    dclk = 0;
    l1 = 0;
    l2 = 0;
    l3 = 0;
  end

  always @ (posedge clk)
    dclk <= ~dclk;

  always @(dclk or l2)
  begin
    if (dclk)
    begin
      l1[0] = l2;
      l1[1] = l3;
    end
  end

  reg tmp;
  always @(dclk or l1)
  begin
    tmp = l2;
    if (~dclk)
    begin
      tmp = ~l1[1];
      l3 = ~l1[0];
    end
    l2 = tmp;
  end

endmodule
