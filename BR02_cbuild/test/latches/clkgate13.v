// Same as clkgate1 but we use a vector of en.

module top (out1, out2, clk, en, in1, in2);
   output      out1, out2;
   input       clk;
   input [1:0] en;
   input       in1, in2;

   reg [1:0]   ren;
   reg         out1;
   reg         out2;
   wire        gclk1;
   wire        gclk2;
   
   // Sync the input enable
   reg [1:0] sen;
   always @ (posedge clk)
     sen <= en;

   always @ (clk or sen)
     if (clk)
       ren[0] <= sen[0];
   assign gclk1 = ~clk & ren[0];

   always @ (clk or sen)
     if (~clk)
       ren[1] <= sen[1];
   assign gclk2 = clk & ren[1];

   initial out1 = 0;
   always @ (posedge gclk1)
     out1 <= in1;

   initial out2 = 0;
   always @ (posedge gclk2)
     out2 <= in2;

endmodule
