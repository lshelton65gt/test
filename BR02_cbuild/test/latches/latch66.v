module top (d2mvad, ck1, en1, en2, in);
   output [7:0] d2mvad;
   input         ck1, en1, en2;
   input [7:0]  in;

   // Flop the inputs
   reg          ren1, ren2, dclk;
   reg [7:0]    rin;
   initial begin
      ren1 = 0;
      ren2 = 0;
      rin = 0;
      dclk = 0;
   end
   always @ (posedge ck1)
     begin
        ren1 <= en1;
        ren2 <= en2;
     end
   always @ (posedge ck1)
     begin
        dclk <= ~dclk;
     end

   always @ (posedge dclk)
     begin
        rin <= in;
     end

   sub S1 (d2mvad, dclk, ren1, ren2, rin);

endmodule

module sub (r2, ck1, en1, en2, in);
   output [7:0] r2;
   input         ck1, en1, en2;
   input [7:0]  in;

   wire         ck1_bc;
   wire         d1mbusytn;
   reg [7:0]    r2, r1;

   assign       ck1_bc = ck1 & en1;
   assign       d1mbusytn = en1 & en2;

   initial begin
      r2 = 8'h0;
      r1 = 8'h0;
   end
   
   always @(ck1_bc or d1mbusytn or r1)
     if(ck1_bc && d1mbusytn)
       r2[7:0] <= #1 r1[7:0];

   always @(ck1_bc or in)
     if(~ck1_bc)
       r1[7:0] <= #1 in;

endmodule
