module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   reg 	  out1, out2;
   always @ (posedge clk)
     out1 = in1 & in2;

   reg 	  mux1;
   always @ (clk or in1 or in2)
     case (clk)
       1'b0: mux1 = in1;
       default: mux1 = in2;
     endcase

   always @ (negedge clk)
     out2 = mux1;

endmodule // top
