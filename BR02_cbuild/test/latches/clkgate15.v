// Copy of the simple test case in clkgate1, but we added
// clock inversion through an assignment. That way we
// need expression synthesis to find this opportunity

module top (out, clk, en, in);
   output out;
   input  clk, en, in;

   reg    ren;
   reg    out;
   wire   gclk;
   wire   clk_;
   assign clk_ = ~clk;
   
   // Sync the input enable
   reg    sen;
   always @ (posedge clk)
     sen <= en;

   // Create the clock, the delay is needed to make Aldec avoid a glitch.
   always @ (clk or sen)
     if (clk_)
       ren <= sen;
   assign #1 gclk = clk & ren;

   initial out = 0;
   always @ (posedge gclk)
     out <= in;

endmodule
