module top (out1, out2, out3, out4, out5, out6, clk, d1, d2);
   output out1, out2, out3, out4, out5, out6;
   input  clk, d1, d2;

   // This is not a latch because it is a temp
   reg 	  nonlatch1, c1;
   always @ (clk or d1 or d2)
     if (clk)
       c1 = d1;
     else
       begin
	  nonlatch1 = ~d2;
	  c1 = nonlatch1 ^ d1;
       end
   reg out1;
   always @ (posedge clk)
     out1 <= c1;

   // This is a latch because it goes to an output port
   reg 	  latch1, c2;
   always @ (clk or d1 or d2)
     if (clk)
       c2 = d1;
     else
       begin
	  latch1 = ~d2;
	  c2 = latch1 ^ d1;
       end
   assign out2 = latch1;

   // This is a latch because it goes to another block
   reg 	  latch2, c3;
   always @ (clk or d1 or d2)
     if (clk)
       c3 = d1;
     else
       begin
	  latch2 = ~d2;
	  c3 = latch2 ^ d1;
       end
   assign out3 = c3 & ~latch2;

   // This is a latch because it goes to a sub module
   reg 	  latch3, c4;
   always @ (clk or d1 or d2)
     if (clk)
       c4 = d1;
     else
       begin
	  latch3 = ~d2;
	  c4 = latch3 ^ d1;
       end
   sub S1 (out4, clk, c4, latch3);

   // This is a latch because it goes to another block
   reg 	  latch4, c5;
   always @ (clk or d1 or d2)
     if (clk)
       c5 = d1;
     else
       begin
	  latch4 = ~d2;
	  c5 = latch4 ^ d1;
       end
   reg out5;
   always @ (c5 or latch4)
     out5 = c5 & ~latch4;


   // This is a latch because it goes to another block
   reg 	  latch5, c6;
   always @ (clk or d1 or d2)
     if (clk)
       c6 = d1;
     else
       begin
	  latch5 = ~d2;
	  c6 = latch5 ^ d1;
       end
   assign out6 = ~latch5;

endmodule // top

module sub (out, clk, d1, d2);
   output out;
   input  clk, d1, d2;

   reg 	  out;
   always @ (posedge clk)
     out <= d1 & ~d2;

endmodule // sub
