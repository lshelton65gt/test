// l1 is exclusive, but l2 is not
// l1's control structure is complex
module top(clk, rst0, rst1, l1, l2);
input clk, rst0, rst1;
output l1, l2;

reg l1, l2;

   initial begin
      l1 = 0;
      l2 = 1;
   end

always @ (clk or l2 or rst1 or rst0)
begin
  if (clk)
    l1 = l2;
  if (!clk)
  begin
    if (rst1)
      l1 = 1;
    if (rst0)
      l1 = 0;
  end
end

always @ (clk or l1)
begin
  if (~clk)
    l2 = ~l1;
end

endmodule
