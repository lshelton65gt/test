// l1 and l2 are both exclusive instances of the same latch module
// they should be converted to flops with or without -noFlatten
module top(clk, l1, l2);
input clk;
output l1, l2;

wire l1, l2;
latch L1 (clk, l1, ~l2);
nlatch L2 (clk, l2, l1);

endmodule

module latch(en, q, d);
input en, d;
output q;

reg q;

   initial q = 0;

always @ (en or d)
begin
  if (en)
    q <= d;
end

endmodule

module nlatch(nen, q, d);
input nen, d;
output q;

reg q;

   initial q = 0;
   
always @ (nen or d)
begin
  if (!nen)
    q <= d;
end

endmodule
