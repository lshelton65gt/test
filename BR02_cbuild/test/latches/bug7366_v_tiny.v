// last mod: Fri Jun  1 13:21:03 2007
// filename: test/latches/bug7366_v_tiny.v
// Description:  This test duplicates the issue in bug 7366,
// the issue here is/was that for some reason mask is recognized as a latch,
// and we incorrectly use a delayed value of in1 to set the values of
// mask.  See the tests bug7366*.vhdl for similar tests


module bug7366_v_tiny(clock, in1, out1);
   input clock;
   input [1:0] in1;
   output [1:0] out1;
   reg [1:0] out1;
   reg [1:0] mask [1:1];
   integer   i;

   always @ (in1)
     begin
	for (i = 1; i >= 0; i = i - 1)
	   begin
	      if (i < in1)
		mask[1][i] = 1'b1;
	      else
		mask[1][i] = 1'b0;
	   end
     end

   
   always @(posedge clock)
     begin: main
	out1 = mask[1];
     end
endmodule
