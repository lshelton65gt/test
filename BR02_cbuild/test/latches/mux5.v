module top (out1, out2, clk, sel, in);
   output out1, out2;
   input  clk;
   input [15:0] sel, in;

   reg 	  out1, out2;
   always @ (posedge clk)
     out1 = in;

   reg 	  latch1;
   always @ (sel or in)
     casex (sel)
       16'bxxxxxxxxxxxxxxx1: latch1 = in[0];
       16'bxxxxxxxxxxxxxx1x: latch1 = in[1];
       16'bxxxxxxxxxxxxx1xx: latch1 = in[2];
       16'bxxxxxxxxxxxx1xxx: latch1 = in[3];
       16'bxxxxxxxxxxx1xxxx: latch1 = in[4];
       16'bxxxzzxxxxx1xxxxx: latch1 = in[5];
       16'bxxxxxxxxx1xxxxxx: latch1 = in[6];
       16'bxxxxxxxx1xxxxxxx: latch1 = in[7];
       16'bxxZZxxx1xxxxxxxx: latch1 = in[8];
       16'bxxxxxx1xxxxxxxxx: latch1 = in[9];
       16'bxxxxx1xxxxxxxxxx: latch1 = in[10];
       16'bxxxx1xxxx??xxxxx: latch1 = in[11];
       16'bxxx1xxxxxxxxxxxx: latch1 = in[12];
       16'bxx1xxxxxxxxxxxxx: latch1 = in[13];
       16'bx1xxxXXxxxxxxxxx: latch1 = in[14];
       16'b1xxxxxxxxxxxxxxx: latch1 = in[15];
       16'b0000000000000000: latch1 = 0;
     endcase

   always @ (negedge clk)
     out2 = latch1;

endmodule // top
