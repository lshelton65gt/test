// This is the same as clkgate1 except there are multiple loads for the latch

module top (out1, out2, clk, en, in);
   output out1, out2;
   input  clk, en, in;

   reg    ren;
   reg    out1;
   reg    out2;
   wire   gclk;
   
   // Sync the input enable
   reg    sen;
   always @ (posedge clk)
     sen <= en;

   initial ren = 0;
   always @ (clk or sen)
     if (~clk)
       ren <= sen;
   assign gclk = clk & ren;

   initial out1 = 0;
   always @ (posedge gclk)
     out1 <= in;

   initial out2 = 0;
   always @ (posedge clk)
     out2 <= ren;

endmodule
