module top (out1, out2, clk, sel, in);
   output out1, out2;
   input  clk;
   input [3:0] sel;
   input [6:0] in;

   reg 	  out1, out2;
   always @ (posedge clk)
     out1 = in;

   reg 	  mux1;
   always @ (sel or in)
     casex (sel)
       4'b11xx: mux1 = in[0];
       4'bxx10: mux1 = in[1];
       4'bxx01: mux1 = in[2];
       4'b01xx: mux1 = in[3];
       4'bxx11: mux1 = in[4];
       4'b10xx: mux1 = in[5];
       4'b0000: mux1 = in[6];
     endcase

   always @ (negedge clk)
     out2 = mux1;

endmodule // top
