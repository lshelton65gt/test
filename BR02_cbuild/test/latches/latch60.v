module top(out, clk, in);
   output out;
   input  clk, in;

   // Flop the data so it arrives at the same time
   reg    r1, dclk;
   initial r1 = 0;
   initial dclk = 0;
   always @ (posedge clk)
     r1 <= in;
   always @ (posedge clk)
     dclk <= ~dclk;

   wire   dummy;
   sub S1 (out, dummy, dclk, in);

endmodule

   
module sub(out1, out2, clk, in);
   output out1, out2;
   input  clk, in;
   reg    out1, out2;

   reg    r1;
   initial r1 = 0;
   always @ (clk or in)
     if (~clk)
       r1 <= in;

   reg    r2;
   initial r2 = 0;
   initial out2 = 0;
   always @ (clk or r1)
     if (clk)
       begin
          r2 <= r1;
          out2 <= 1'b0;
       end

   initial out1 = 0;
   always @ (clk or r2)
     if (~clk)
       out1 <= r2;

endmodule
   
