module top (out1, out2, clk, in);
   output out1, out2;
   input  clk, in;

   reg 	  latch1;
   always @ (clk or in)
     if (clk)
       latch1 = in;

   assign out1 = latch1;

   reg 	  out2;
   always @ (negedge clk)
     out2 = latch1;

endmodule // top
