// last mod: Fri Apr 28 08:27:55 2006
// filename: test/protected-source/protected_hierarchy_03.v
// Description:  This test has a hierarchy of modules, the top 2 levels are
// visible, the remainder is `protected,
// top.left has a hier ref to a memory hidden inside the protected region
// top.right loads this memory with $readmemh


// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_hierarchy_03.v
// to create a protected_defines_03.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below


module protected_hierarchy_03(clock, in1, in2, out1);
   input clock;
   input [7:0] in1,in2;
   output [7:0] out1;

   left i_left(in1);
   right i_right(out1, clock, in2);

endmodule

module left(input [7:0] in);

   assign i_right.h1[1000].h2.HIDDEN_val1 = in[7:6];
   assign i_right.h1[1001].h2.HIDDEN_val1 = in[5:4];
   assign i_right.h1[1002].h2.HIDDEN_val1 = in[3:2];
   assign i_right.h1[1003].h2.HIDDEN_val1 = in[1:0];

   initial
      begin
	 $readmemh("./protected_hierarchy_03.dat", i_right.hm.mem);
      end

   always @(i_right.h1[1000].h2.HIDDEN_val2 or i_right.h1[1001].h2.HIDDEN_val2
	    or i_right.h1[1002].h2.HIDDEN_val2 or
	    i_right.h1[1003].h2.HIDDEN_val2 or i_right.in[2:0] or i_right.out2)
      begin
	 $display( "Hidden value: %b, HIDDEN_mem[%d]: %b",
		   {i_right.h1[1000].h2.HIDDEN_val2,i_right.h1[1001].h2.HIDDEN_val2,i_right.h1[1002].h2.HIDDEN_val2,i_right.h1[1003].h2.HIDDEN_val2},
		   i_right.in[2:0], i_right.out2);
      end
endmodule

module right(output [7:0] out, input clock, input[7:0] in);

   wire [7:0] out2;
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ65CQkBOQkJDj6AxbXt5lmqFmn9hkOry+XTvc3pnhp2HY
3z3cpCBY49/bnbnmol1fYLpaw6HCwcDAcJLyn9+Hh8cv6NMVnFP405OTk7uw8Yw7Ac/KMjMADAMP
DD0zAAk3+R00pKelx7GzhzRSmNnZJAEF0bzAjYeQqEK1aw@:
`endprotected
endmodule

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQb5CQkpqQkJBn6AwN3937UtCArefvVYys1ErT5rkkpITq
oPDA4wCGMdFapoly8iIZWdZhT6t5/qWGStNnJKN/804bU4UPvzNrNehUR1Y0Iw2P+alPY0jYYS45
badzv1JF/LTWkFBaunuaFPmkCRRhl2c6XkQNERSvafswcZ4Efof5wTXLp8jAeeFQiOmoPRAdYC6E
976BwPdmlGvh2mx3RNGleJRe9r+bjgwxW8LIEfzLBXtxjg4Xwii1Y/BQNIzHHE53R561Bk+9ilyx
6WgzmbRyOlrV0L5md7KHl+n8cVU7O4YjjD1bKBd47fI+pCvhqdkKuC2ejNjRl/HfmZQNiA:<
`endprotected


// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else


module protected_hierarchy_03(clock, in1, in2, out1);
   input clock;
   input [7:0] in1,in2;
   output [7:0] out1;

   left i_left(in1);
   right i_right(out1, clock, in2);

endmodule

module left(input [7:0] in);

   assign i_right.h1[1000].h2.HIDDEN_val1 = in[7:6];
   assign i_right.h1[1001].h2.HIDDEN_val1 = in[5:4];
   assign i_right.h1[1002].h2.HIDDEN_val1 = in[3:2];
   assign i_right.h1[1003].h2.HIDDEN_val1 = in[1:0];

   initial
      begin
	 $readmemh("./protected_hierarchy_03.dat", i_right.hm.mem);
      end

   always @(i_right.h1[1000].h2.HIDDEN_val2 or i_right.h1[1001].h2.HIDDEN_val2
	    or i_right.h1[1002].h2.HIDDEN_val2 or
	    i_right.h1[1003].h2.HIDDEN_val2 or i_right.in[2:0] or i_right.out2)
      begin
	 $display( "Hidden value: %b, HIDDEN_mem[%d]: %b",
		   {i_right.h1[1000].h2.HIDDEN_val2,i_right.h1[1001].h2.HIDDEN_val2,i_right.h1[1002].h2.HIDDEN_val2,i_right.h1[1003].h2.HIDDEN_val2},
		   i_right.in[2:0], i_right.out2);
      end
endmodule

module right(output [7:0] out, input clock, input[7:0] in);

   wire [7:0] out2;
`protect   
   HIDDEN_1 h1[1000:1003](out, clock, in);
   HIDDEN_mem hm(out2, clock, in[2:0]);

`endprotect
endmodule

`protect   
module HIDDEN_1(output [1:0] out, input clock, input[1:0] in);

   HIDDEN_2 h2(out, clock, in);
endmodule

module HIDDEN_2(output [1:0] out, input clock, input[1:0] in);

   wire [1:0] HIDDEN_val1;
   wire HIDDEN_val2;

   assign HIDDEN_val2 = HIDDEN_val1 + in;
   assign out = HIDDEN_val2;

endmodule

module HIDDEN_mem(output reg [7:0] out, input clock, input [2:0] in);

   reg [7:0] mem [7:0];

   always @(posedge clock)
      begin
	 out = mem[in];
      end
endmodule
`endprotect

   
`endif
