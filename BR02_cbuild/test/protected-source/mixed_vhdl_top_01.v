// last mod: Fri Apr 14 11:53:08 2006
// filename: test/protected-source/mixed_vhdl_top_01.v
// Description:  This test is a vhdl top mixed language test where the verilog
// module is fully protected.  The name 'should_never_appear' should never
// appear in a generated file.

// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect mixed_vhdl_top_01.v
// to create a mixed_vhdl_top_01.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQDpCQkDaQkJAG6Axj5lthnKcnZN+hudzcX11duj2kPKKg
ZbshuN+53pninRqi22ReomCdwKHEcVpd37ld2cW4Xli/XdkZX9u9270a37y42N28wkBYXLu4vcFY
XKNEkYLWqpppNbWwETJE5IBDwEMK29HRsaE3rCEiiFTCwNja3V9cc3KU2bpIEqS6ODkRJMrTNMNj
wigQiPK1h5BMhKSh
`endprotected
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else

`protect
module should_never_appear (input in1, in2, output reg out1);
   always
     begin
	out1 = in1 && in2;
     end
endmodule
`endprotect
`endif
