// last mod: Thu Apr 27 14:42:31 2006
// filename: test/protected-source/protected_hierarchy_01.v
// Description:  This test has a hierarchy of modules, the top 2 levels are
// visible, the remainder is `protected,
// top.left has a hier ref to a memory hidden inside the protected region
// should this be acceptable?


// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_hierarchy_01.v
// to create a protected_defines_01.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below


module protected_hierarchy_01(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   left i_left(in1);
   right i_right(out1, in2);

endmodule

module left(input [7:0] in);

   assign i_right.h1.h2.HIDDEN_val1 = in;

   always @(i_right.h1.h2.HIDDEN_val2)
      begin
	 $display( "Hidden value: %d", i_right.h1.h2.HIDDEN_val2);
      end
endmodule

module right(output [7:0] out, input[7:0] in);

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQxpCQkNiQkJDe6AzjW7ndvZ0dXBjapqccmJ8mpJrnv6c4
mtq9oJk8pJgeWL+d52ZdZ1/nxaHCwcDAcJLyn9+Hh8cv6NMVnNMdbEKCjRVcrN37vpCoBoRg
`endprotected
endmodule

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ7pCQkBWQkJDm6Ayj3tqdY2WjvNkh3CWkYGMYoGI9YF88
vB2a4mLevlnc5aGa22Nnm59kwaHCwcDAcFpd37ld2cVgYOTh4WUbp0RYv725uL3BGKYnohjFkOLk
hKJjEJKA7vYONqWHh8CLxCkB0hYBlsSBwLalv5XyjIeQP4K4uw?:
`endprotected

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQDJCQkE+QkJAE6AybHeA54L5l3VkZZKG946ZZ4qQ6OBgk
uGXlPtxmm+Fmn+Hfotle4t9exKHCwcDAcFpd37ld2cVgYOTh4WUbp0JYv725uL3BGKYnohjFkOLk
hKJjEJKA7vYONqWHh8DL6faBzN/J8h4xpYbRsxhCVHJyXGSs9OGVy/Q9mkrQ8z0BxJK91cGRorel
v5VywL6QPgzSSA@<
`endprotected

// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else


module protected_hierarchy_01(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   left i_left(in1);
   right i_right(out1, in2);

endmodule

module left(input [7:0] in);

   assign i_right.h1.h2.HIDDEN_val1 = in;

   always @(i_right.h1.h2.HIDDEN_val2)
      begin
	 $display( "Hidden value: %d", i_right.h1.h2.HIDDEN_val2);
      end
endmodule

module right(output [7:0] out, input[7:0] in);

`protect   
   HIDDEN_1 h1(out, in);
`endprotect
endmodule

`protect   
module HIDDEN_1(output [7:0] out, input[7:0] in);

   HIDDEN_2 h2(out, in);
endmodule
`endprotect

`protect   
module HIDDEN_2(output [7:0] out, input[7:0] in);

   wire HIDDEN_val1;
   wire HIDDEN_val2;

   assign HIDDEN_val2 = HIDDEN_val1 + in;
   assign out = HIDDEN_val2;

endmodule
`endprotect

   
`endif
