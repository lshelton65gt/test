// last mod: Fri Mar 16 11:31:54 2007
// filename: test/protected-source/bug7147_1.v
// Description:  This test demonstrates the problem described in bug7147.
// what we want to see is that the warning message about the multi-output always
// block will point to the protected region, and the net it talks about shows
// the full hierarchical name to the problem net, but the names from within the
// protected region are obscured. ($prot0 ...)
//
// This file contains both the protected and unprotected version of this file,
// if you ever need to create a new protected version comment out the definition
// for USEPROTECTED and use the command: cbuild +protect bug7147_1.v to create a .vp
// file, then copy it into this file in place of USEPROTECTED section.


`define USEPROTECTED 1
`ifdef USEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module bug7147_1(clk, out1, out2);
   input clk;
   output [31:0] out1, out2;

   mid U1(clk, out1);
   mid U2(clk, out2);
   
endmodule

`define LOOPLENGTH 1048576
module mid(clk, out);
   input clk;
   output [31:0] out;
   reg [31:0] out;
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQWpCQkGOQkJBS6AylHtubEtCEFUejL3K8yoS9Cg7ARjLV
iIGuopoasDXZv3iIqoZta4w1m4eeT3VzjmuMGbyAez2oGLAJg+nmqGmq7qQNKbfvh17WB/DEPq8N
4ReTuPQce+gogKf4CS28VpaTeBTaOKADuAMs2o/AKda0YTYCMsSKHwQGIbzwMlWd+bn5msODwZuh
ueBmXJvkmbuYmxS1futHZWaLV67+nchFS1cgV0lP+cKtNgCpy50AelHJznkKpN1LtxZukP290DQ?

`endprotected
   always @(posedge clk)
     begin
	out = 0;
     end
   
endmodule
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////
`else

module bug7147_1(clk, out1, out2);
   input clk;
   output [31:0] out1, out2;

   mid U1(clk, out1);
   mid U2(clk, out2);
   
endmodule

`define LOOPLENGTH 1048576
module mid(clk, out);
   input clk;
   output [31:0] out;
   reg [31:0] out;
`protect
   reg [31:0] temp;		// this name should not be visible
   integer    i;

   
   initial
     begin 
	for (i = 0; i < `LOOPLENGTH; i = i + 1)
	   begin
	      temp = i;
	   end
	out = temp;
     end
`endprotect
   always @(posedge clk)
     begin
	out = 0;
     end
   
endmodule

`endif
