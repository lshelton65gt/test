// last mod: Mon Apr 24 13:53:21 2006
// filename: test/protected-source/bug5915_4.v
// Description:  This test is a check to see that fully protected
// modules/instances are never visible in generate files.


// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect bug5915_4.v
// to create a protected_defines_01.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"


`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module bug5915_4(clk, out, in1, in2);
   input clk;
   input [15:0] in1, in2;
   output [15:0] out;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ9JCQkM+QkJDM6AybPJw7oKCimNldmOBcYmZlp6WYuWXn
oaXmH1iaIKQ/YNhb3l9b2Zykw6HCccLAwGChGr+4utm+wbigRNheWUbBWL+9GaYnohhFwVhcoxQj
HFC83fu+kPZyiyE<
`endprotected   
   L2_clear c1(clk, out[15:8], in1[15:8], in2[15:8]);
      
endmodule

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQApCQkFGQkJAa6Axbut2jWGKZYd5iPqTaXL6ZG2K95uXh
2mFi4mdh4Fu/mOZkud47Yl2mxaHCcVpd37ld2cVgoRq/uLrZvkHYXllGwVi/vUHBWFyjlIHWCkbM
mprQ1tH5GZLAhhmH/f71gJvHlwaQetRYkKlGzLDBj/MYeZqVFuCbMh0R2lAmUMnW8IZArpBCNqkK

`endprotected   
   L3_clear c1(clk, out[7:4], in1[7:4], in2[7:4]);

endmodule

module L2_clear(clk, out, in1, in2);
   input clk;
   input [7:0] in1, in2;
   output [7:0] out;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ9JCQkM+QkJDM6AybG1inGeJem+O5XRmYpaRgoKCaY99c
GKS5Wp2euqegWaZf4GfZ2hxgx6HCccLAwGChHr+4utm+wbigRNheWUbBWL+9GaYmohhFwVhcoxQj
HFC83fu+kOA3iy0<
`endprotected
   L3_clear c1(clk, out[7:4], in1[7:4], in2[7:4]);

endmodule



`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQApCQkFGQkJAa6Ay7o6dhvqWg52WgoR2im76lmJynoSSc
pODn36ZimN+94SPcJTyYXyfgxKHCcVpd37ld2cVgoR6/uLrZvkHYXllGwVi/vUHBWFyjlIHWCkbM
mprQ1tH5GZLAhhmH/fz1gJvHlwaQetRYkKlGzLDBj4MYeZqVFuCbMp0R2lAmUMnW8IZArpDgcKcF

`endprotected   
   L4_clear c1(clk, out[3:2], in1[3:2], in2[3:2]);

endmodule

module L3_clear(clk, out, in1, in2);
   input clk;
   input [3:0] in1, in2;
   output [3:0] out;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ9JCQkM+QkJDM6AwbYl295uW5PeRnYeSmuNgb3GOm2Bua
WOFaGdq8oOHeXFpd2qRYHeAfxKHCccLAwGChGb+4utm+wbigRNheWUbBWL+9GaYkohhFwVhcoxQj
HFC83fu+kD6tjBk=
`endprotected   
   L4_clear c1(clk, out[3:2], in1[3:2], in2[3:2]);

endmodule


`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQP5CQkHGQkJA36AyjYKQcGqLeWrzkY5pZGbjnneWhZrmg
nFm/pmceHBxaITjkome8W6OkxKHCcVpd37ld2cVgoRm/uLrZvkHYXllGwVi/vUHBWFyjlIHWCkbM
mprQ1tH5GZLAhhmH/fj1gJvHlwaQetRYkKnwUTJE5PSBAMDy3uny9SESE9bR7uH62uq6WPTdAJSA
tDU293bh4RKE15NlSBL8kKFE0CaQiUbMkERT0fI>
`endprotected
	 out[1] = in1[1] & in2[1];
      end

endmodule

module L4_clear(clk, out, in1, in2);
   input clk;
   input [1:0] in1, in2;
   output [1:0] out;
   reg [1:0] out;

   always @(posedge clk)
      begin
	 out[0] = in1[0] & in2[0];
	 out[1] = in1[1] & in2[1];
      end

endmodule

// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////
`else

module bug5915_4(clk, out, in1, in2);
   input clk;
   input [15:0] in1, in2;
   output [15:0] out;

`protect
   L2_prtct p1(clk, out[7:0], in1[7:0], in2[7:0]);
`endprotect   
   L2_clear c1(clk, out[15:8], in1[15:8], in2[15:8]);
      
endmodule

`protect
module L2_prtct(clk, out, in1, in2);
   input clk;
   input [7:0] in1, in2;
   output [7:0] out;

   L3_prtct p1(clk, out[3:0], in1[3:0], in2[3:0]);
`endprotect   
   L3_clear c1(clk, out[7:4], in1[7:4], in2[7:4]);

endmodule

module L2_clear(clk, out, in1, in2);
   input clk;
   input [7:0] in1, in2;
   output [7:0] out;

`protect
   L3_prtct p1(clk, out[3:0], in1[3:0], in2[3:0]);
`endprotect
   L3_clear c1(clk, out[7:4], in1[7:4], in2[7:4]);

endmodule



`protect
module L3_prtct(clk, out, in1, in2);
   input clk;
   input [3:0] in1, in2;
   output [3:0] out;

   L4_prtct p1(clk, out[1:0], in1[1:0], in2[1:0]);
`endprotect   
   L4_clear c1(clk, out[3:2], in1[3:2], in2[3:2]);

endmodule

module L3_clear(clk, out, in1, in2);
   input clk;
   input [3:0] in1, in2;
   output [3:0] out;

`protect
   L4_prtct p1(clk, out[1:0], in1[1:0], in2[1:0]);
`endprotect   
   L4_clear c1(clk, out[3:2], in1[3:2], in2[3:2]);

endmodule


`protect
module L4_prtct(clk, out, in1, in2);
   input clk;
   input [1:0] in1, in2;
   output [1:0] out;
   reg [1:0] out;

   always @(posedge clk)
      begin
	 out[0] = in1[0] & in2[0];
`endprotect
	 out[1] = in1[1] & in2[1];
      end

endmodule

module L4_clear(clk, out, in1, in2);
   input clk;
   input [1:0] in1, in2;
   output [1:0] out;
   reg [1:0] out;

   always @(posedge clk)
      begin
	 out[0] = in1[0] & in2[0];
	 out[1] = in1[1] & in2[1];
      end

endmodule

`endif
