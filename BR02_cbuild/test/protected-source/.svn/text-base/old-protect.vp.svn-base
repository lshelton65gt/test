/*
 * This module contains several sections bracketed by `protect and `endprotect
 * compiler directives.
 *
 * When processed by cbuild +protect, these should become `protected sections
 * containing the `protect source in encrypted and asci-encoded form.
 *
 * To facilitate testing with grep, lines with PROTECTED should no longer be 
 * visible after cbuild +protect, and lines with CLEAR should be visible.
 *
 * Calls to $display with both CLEAR and PROTECTED in them should execute
 * when either the protected or unprotected source is simulated.
 *
 * See the comments above the individual sections for more details.
 */
module protect();

reg c;
wire b;

initial
  begin

// Even though only one of these sections can be active for any give run,
// BOTH sections are converted by +protect processing.
`ifdef FOO
  `protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ+JCQkMmQkJDw6Azj2b/lXeQaWLydW1+6WaS8vmccnGGZ
2Fy9H2Tnnr7kHro7mL5nOdod4sDYxiDFoCTGcMLAwMDZWby+WNk8RMCamGKf4eWe4eUhwljc29nd
w+BjZ8dCJJa6kpClMIuu
`endprotected 
`else
  `protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ+ZCQkMqQkJDx6Ay7Xua8ujvaXCNg3xtiIaQhIGDdu1yZ
pdhiXBxY5ZymnmW6pqedO1xd5sDYxiDFoCLGcMLAwMDZWby+WNk8RMCamGKf4eWe4eUhwljcW9vZ
3cPgY2fHQiSWOpKQl7CK6g??
`endprotected 
`endif

/*
 * This section will NOT be protected, because the compiler directives
 * appear within a line comment.
 */
// `protect
   $display("CLEAR: line comment");
// `endprotect

// This section will NOT be protected, because the compiler directives
// appear within a block comment
/*
 `protect
 */
$display("CLEAR: block comment 1");
/*
 `endprotect
 */


// Likewise, this one is not protected.
/*
`protected
$display("CLEAR: block comment 2");
`endprotected
*/

/*
 * These lines should be left alone, because they are in a 
 * constant string.
 */
 $display("CLEAR: what about a `protect inside a constant string?");
 $display("CLEAR: that could be an `endprotect issue.");

/*
 * The `include is an interesting case.  The protected code before 
 * and after `include gets encrypted, but the actual `include does not.
 */
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ9JCQkMSQkJDM6AybGmK6plllpFpkPFqeWb9hnuJlnL9l
HGAZWOU+2Fy+OqKbvWBk4WKa5HDCwMDZWby+WNk8RMCamGKf4eWe4eUhwtja3Vu/2sXYWFzbXrnd
2cVCJHaSkDOTi6Y?
`endprotected
`include "include.v"
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ85CQkMOQkJDL6AyjWuK8Z2HlGuKkYqS7Ytq5uucZot6b
nZvmWZuhoqKnp5kfvOGk2bw74nDCwMDZWby+WNk8RMCamGKf4eWe4eUhwtjcu9m9wthYXNteud3Z
xUIkdpKQqFWJOA<<
`endprotected 

/*
 * Unlike `include, `protect can appear anywhere in an expression.
 * Here we check that the value 8 gets protected.
 */
$display("secret value ", `protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ3JCQkKyQkJDU6Awb2ubmXqTbYdxh3tybWSfc2Z3gmdrf
nuGe36Uk2Gdb41/jG2S8ZOXi5sDAmphin+HlnuHlIcLAQsEgQMGQkN2ugSI@
`endprotected );

end

assign b = c;

endmodule
