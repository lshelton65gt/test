// last mod: Fri Apr 14 10:01:21 2006
// filename: test/protected-source/bug5915_3.v
// Description:  This test is a check to see that when protected instances are
// nested, the subtree below the protected section does not become visible.  In
// this example only the first instance of a module are protected, see also
// bug5915_2 where only the first instance is protected (this pair of tests
// checks to make sure we do the proper protection without regard to the order
// that instances are processed)

// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file:
// comment out the definition for USEPROTECTED and use the command:
//   cbuild +protect bug5915_3.v
// to create a bug5915_3.vp file, then copy the generated module into
// this file in place of USEPROTECTED section.


`define USEPROTECTED 1
`ifdef USEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module bug5915_3(out, in1, in2);
   input [15:0] in1, in2;
   output [15:0] out;

   L2_clear c1(out[15:8], in1[15:8], in2[15:8]);
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQz5CQkMqQkJDH6AwjWKSYn1iZH5za5J49mLg7OJo4ZOTb
ZqC7p5ymmWOjn1tfmj4Y2pmexqHEccLAwGChGr+4utm+wbigRFi/vRmmJ6IYRcFYXKMUIxxQvN37
vpCHtouR
`endprotected   
      
endmodule

module L2_prtct(out, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out;

   L3_clear c1(out[7:4], in1[7:4], in2[7:4]);
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQz5CQkMqQkJDH6Ay7YpxeIWChvSfgPeRn3j5YmrqameSn
ILy4odhjvFjjn2KemN7nXl1ewKHEccLAwGChHr+4utm+wbigRFi/vRmmJqIYRcFYXKMUIxxQvN37
vpCA6op4
`endprotected   

endmodule

module L2_clear(out, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out;

   L3_prtct p1(out[3:0], in1[3:0], in2[3:0]);
   L3_clear c1(out[7:4], in1[7:4], in2[7:4]);

endmodule



module L3_prtct(out, in1, in2);
   input [3:0] in1, in2;
   output [3:0] out;

   L4_clear c1(out[3:2], in1[3:2], in2[3:2]);
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQz5CQkMqQkJDH6AwjpGJYpD1aurslXLnnO6TdphmkoGDn
obyjuqXbpOWb3+I6nJu54mZbwaHEccLAwGChGb+4utm+wbigRFi/vRmmJKIYRcFYXKMUIxxQvN37
vpB2horR
`endprotected   

endmodule

module L3_clear(out, in1, in2);
   input [3:0] in1, in2;
   output [3:0] out;

   L4_clear c1(out[3:2], in1[3:2], in2[3:2]);
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQz5CQkMqQkJDH6Azj32aiop+b5+O8p6ZZHFrn5L5an71Y
2CegoWXkXtujmmMbmuPnppnnw6HEccLAwGChGb+4utm+wbigRFi/vRmmJKIYRcFYXKMUIxxQvN37
vpBUpYkt
`endprotected   

endmodule


module L4_prtct(out, in1, in2);
   input [1:0] in1, in2;
   output [1:0] out;

   assign    out[1] = in1[1] & in2[1];
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQzZCQkMOQkJDF6AxboN1iWWKfndvm2RygZ2WhIaRjH7zh
5WQ6WqNfXuBm3uU54qU+YpxYxaHEccLAwNi8vl7cX5OihGLb2zKd8oX8haJjnNGcpbBTmFggdpKQ
uQeIqw;:
`endprotected

endmodule

module L4_clear(out, in1, in2);
   input [1:0] in1, in2;
   output [1:0] out;

   assign    out[0] = in1[0] & in2[0];
   assign    out[1] = in1[1] & in2[1];

endmodule

// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////
`else

module bug5915_3(out, in1, in2);
   input [15:0] in1, in2;
   output [15:0] out;

   L2_clear c1(out[15:8], in1[15:8], in2[15:8]);
`protect
   L2_prtct p1(out[7:0], in1[7:0], in2[7:0]);
`endprotect   
      
endmodule

module L2_prtct(out, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out;

   L3_clear c1(out[7:4], in1[7:4], in2[7:4]);
`protect
   L3_prtct p1(out[3:0], in1[3:0], in2[3:0]);
`endprotect   

endmodule

module L2_clear(out, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out;

   L3_prtct p1(out[3:0], in1[3:0], in2[3:0]);
   L3_clear c1(out[7:4], in1[7:4], in2[7:4]);

endmodule



module L3_prtct(out, in1, in2);
   input [3:0] in1, in2;
   output [3:0] out;

   L4_clear c1(out[3:2], in1[3:2], in2[3:2]);
`protect
   L4_prtct p1(out[1:0], in1[1:0], in2[1:0]);
`endprotect   

endmodule

module L3_clear(out, in1, in2);
   input [3:0] in1, in2;
   output [3:0] out;

   L4_clear c1(out[3:2], in1[3:2], in2[3:2]);
`protect
   L4_prtct p1(out[1:0], in1[1:0], in2[1:0]);
`endprotect   

endmodule


module L4_prtct(out, in1, in2);
   input [1:0] in1, in2;
   output [1:0] out;

   assign    out[1] = in1[1] & in2[1];
`protect
   assign    out[0] = in1[0] & in2[0];
`endprotect

endmodule

module L4_clear(out, in1, in2);
   input [1:0] in1, in2;
   output [1:0] out;

   assign    out[0] = in1[0] & in2[0];
   assign    out[1] = in1[1] & in2[1];

endmodule

`endif
