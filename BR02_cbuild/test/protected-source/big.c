#ifdef PROTECTED
#include "libbigprot.h"
#else
#include "libbig.h"
#endif

int main(int argc, char **argv)
{
  CarbonObjectID *model;
#ifdef PROTECTED
  model = carbon_bigprot_create(eCarbonFullDB, eCarbon_NoFlags);
#else
  model = carbon_big_create(eCarbonFullDB, eCarbon_NoFlags);
#endif
  carbonDestroy(&model);
  return 0;
}
