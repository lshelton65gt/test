// last mod: Mon Mar 19 11:36:47 2007
// filename: test/protected-source/latch12.v
// Description:  This test is used to check the information that is displayed
// about protected regions via the .latchs report
//
// This file contains both the protected and unprotected version of this file,
// if you ever need to create a new protected version comment out the definition
// for USEPROTECTED and use the command: cbuild +protect latch12.v to create a .vp
// file, then copy it into this file in place of USEPROTECTED section.

`define USEPROTECTED 1
`ifdef USEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below

module top(clk, l1, l2);
   input clk;
   output l1, l2;

   mid U1(clk, l1, l2);
   
endmodule


module mid(clk, l1, l2);
   input clk;
   output l1, l2;

   reg    l1, l2;
   initial begin
      l1 = 0;
      l2 = 0;
   end

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ5pCQkO+QkJD+6Ay7nuNbWKGmW2UZmlgbomfip6dYnB5c
XuGcmFg+vLvaOlyiZKSgYbogxKHGwcDAcJJyVAxiVFrylZeVHXQM/BVsshWMs90AKBLSwvrq9o4U
mZQJ+fCFCuCRlRWM05XLMOL7GMD6zpqHkG8qsME:
`endprotected
   always @ (clk or l1)
     begin
        if (~clk)
          l2 = ~l1;
     end

endmodule
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////
`else

module top(clk, l1, l2);
   input clk;
   output l1, l2;

   mid U1(clk, l1, l2);
   
endmodule


module mid(clk, l1, l2);
   input clk;
   output l1, l2;

   reg    l1, l2;
   initial begin
      l1 = 0;
      l2 = 0;
   end

`protect   
   always @ (clk or l2)
     begin
        if (clk)
          l1 = l2;
     end
`endprotect
   always @ (clk or l1)
     begin
        if (~clk)
          l2 = ~l1;
     end

endmodule
`endif
