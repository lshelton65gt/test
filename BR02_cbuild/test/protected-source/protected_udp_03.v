// last mod: Tue Apr 25 14:47:53 2006
// filename: test/protected-source/protected_udp_03.v
// Description:  This test checks for the handling of UDPs when there in-line
// directives (observeSignal) within a protected udp



// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_udp_03.v
// to create a protected_defines_03.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module protected_udp_03(output out1, out2, input a1, a2, b1, b2, c1, c2, d1, d2, e1, e2);

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQA5CQkDuQkJAb6Az9XK2bEsCEEHFNz+GWFxLQflm1CpI9
tFrcgAp07pnpY4jlX29/QExgvH9g+jrlQIv83E5hqhzVRX63Lk/yWPs2JqktDxd76Uc1CJLQsh7F
wQQHDk07j1OguNmbCKk6hfjxFIXe6JVbvfSJQJyfatTdRpN5WW9psKBurriP8RSF3ugE32KVXIy/
LA@<
`endprotected   
endmodule

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCStpCQmiKQkJKO6AxVBNH/C6CEV3/sGud48VuihQJTNnYQ
OibLK9Mn5g0iZYKJYtKMGNEmA0L/z0sosrSUBRIkGIgAbW9raS1X/6VeLu7rXo7XQwthZU/vfnNT
pFxuDA/fL7MpKG9htVfPr1Nw5uyiquEMHEOlBeQb4O3j6enlq0tcI0m6zrysaAD+dIDG0xgXgLb2
cCOxmAQjH4OXkETsJgFqppGorRUAYLD1AJaSaYbvQxgB1D8z3L8756LWjLO4bLYe7ge02EDhBHWa
sJY9kTPB48o0/vfgt11gWUN7Gg9GKpAO4sTfrgjXLUZHEw4KCKgDpBzFxHDKEcuYLO1R97sgl3Uo
tgSZgPQd3iHm13l/ui1tPay7qBxd+xg9hHx8cKZ8cIs30cVwxZSMdwuAsdOxxHlVSnZQv/SrESkF
UHr0LimGxnZHNR41gL6ZG8AQ+p0WgV6zRJVFahGBlf9Er70BORaA38QW24RG0FUHp40nSTCo5TuY
chrRAX0sueNdlm579W7+zYM5Bie9yJqSK0swflzxKtWetNnQ4lKUhQI2XITQ7631D3QN3WyifErf
HXMhcJdXQuXsz90QIstj/rZFLMe6D8uDUg7FYbTNwHXFBEBwmUkssATquGk5lD98/FB6OLXGb5ev
Z+Bo1oqOEicJKxdk/JtfJnz8G15nAWPtcHxYXT5IfNpdPkB8WlwmWHwTCO2QI62/a1D1jyR8gQZe
m2pbBh4@
`endprotected
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else

module protected_udp_03(output out1, out2, input a1, a2, b1, b2, c1, c2, d1, d2, e1, e2);

`protect   
   HIDDEN_udp_jkff1   ( out1, a1, b1, c1, d1, e1 ); // no instance name
   HIDDEN_udp_jkff2 u2( out2, a2, b2, c2, d2, e2 );
`endprotect   
endmodule

`protect

primitive HIDDEN_udp_jkff1 (out, j, k, clk, clr_, set_);
   output out;  // carbon observeSignal
   input  j, k, clk, clr_, set_;
   reg    out;

   table

// j  k  clk  clr_   set_   : Qt : Qt+1
//       
   0  1  r   ?   1    : ?  :  0  ; // clock in 0
   0  0  r   1   1    : ?  :  -  ; // output remains same
   1  0  r   1   ?    : ?  :  1  ; // clock in 1
   1  1  r   ?   1    : 1  :  0  ; // clock in 0
   1  1  r   1   ?    : 0  :  1  ; // clock in 1
   ?  0  *   1   ?    : 1  :  1  ; // reduce pessimism
   0  ?  *   ?   1    : 0  :  0  ; // reduce pessimism
   ?  ?  f   ?   ?    : ?  :  -  ; // no changes on negedge clk
   *  ?  b   ?   ?    : ?  :  -  ; // no changes when j switches
   *  0  x   1   ?    : 1  :  1  ; // no changes when j switches
   ?  *  b   ?   ?    : ?  :  -  ; // no changes when k switches
   0  *  x   ?   1    : 0  :  0  ; // no changes when k switches
   ?  ?  ?   ?   0    : ?  :  1  ; // set output
   ?  ?  b   1   *    : 1  :  1  ; // cover all transistions on set_
   ?  0  x   1   *    : 1  :  1  ; // cover all transistions on set_
   ?  ?  ?   0   1    : ?  :  0  ; // reset output
   ?  ?  b   *   1    : 0  :  0  ; // cover all transistions on clr_
   0  ?  x   *   1    : 0  :  0  ; // cover all transistions on clr_
   ?  ?  ?   ?   ?    : ?  :  x  ; // any notifier change

   endtable
endprimitive // udp_jkff

primitive HIDDEN_udp_jkff2 (out, j, k, clk, clr_, set_);
   output out;  
   input  j, k, clk, clr_, set_;
   reg    out;

   table

// j  k  clk  clr_   set_   : Qt : Qt+1
//       
   0  0  r   1   1   : ?  :  -  ; // output remains same
   0  1  r   ?   1   : ?  :  0  ; // clock in 0
   1  0  r   1   ?   : ?  :  1  ; // clock in 1
   1  1  r   ?   1   : 1  :  0  ; // clock in 0
   1  1  r   1   ?   : 0  :  1  ; // clock in 1
   ?  0  *   1   ?   : 1  :  1  ; // reduce pessimism
   0  ?  *   ?   1   : 0  :  0  ; // reduce pessimism
   ?  ?  f   ?   ?   : ?  :  -  ; // no changes on negedge clk
   *  ?  b   ?   ?   : ?  :  -  ; // no changes when j switches
   *  0  x   1   ?   : 1  :  1  ; // no changes when j switches
   ?  *  b   ?   ?   : ?  :  -  ; // no changes when k switches
   0  *  x   ?   1   : 0  :  0  ; // no changes when k switches
   ?  ?  ?   ?   0   : ?  :  1  ; // set output
   ?  ?  b   1   *   : 1  :  1  ; // cover all transistions on set_
   ?  0  x   1   *   : 1  :  1  ; // cover all transistions on set_
   ?  ?  ?   0   1   : ?  :  0  ; // reset output
   ?  ?  b   *   1   : 0  :  0  ; // cover all transistions on clr_
   0  ?  x   *   1   : 0  :  0  ; // cover all transistions on clr_
   ?  ?  ?   ?   ?   : ?  :  x  ; // any notifier change

   endtable
endprimitive // udp_jkff


`endprotect
`endif
