// last mod: Tue Apr 25 15:20:57 2006
// filename: test/protected-source/protected_taskenable_01.v
// Description:  This test contains a task with a name HIDDEN that is defined
// within a protected region, so the name HIDDEN should never appear in a
// generated file.

// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_taskenable_01.v
// to create a protected_defines_01.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module protected_taskenable_01(clock, sel, in1, out);
   input clock;
   input sel;
   input [7:0] in1;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 77;
     end
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQBJCQkEiQkJAc6AybZeLjvqJY2mIe4j3c3Obcp6BYpFo/
XGEluGUYvJ2mmb+94l6/n+Zjx6HCccLAwLjZvF7GYGDk4eFlI5ZhEbAvJDQwJNSxSkxasMaxj8W+
o5+xBfkYusoMCpODuNrd10efBdna3V9cc3LEWNzDQJD6QMSQIoXwOoT8AZzdXdtxEsCwp+KRkMgA
qBo=
`endprotected   
      
   always @(posedge clock)
     begin: main
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQxJCQkNSQkJDc6AzbnaammKFgPuCf2iFgpyJkGRpioLgY
uriZXWOdWlpg4VmYo1+mIxhkxaHCcXJkYOTh4WVDWL+9QcFYXKNEwbjeXUEkdpKQUXODKw?:
`endprotected
     end
endmodule
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else

module protected_taskenable_01(clock, sel, in1, out);
   input clock;
   input sel;
   input [7:0] in1;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 77;
     end
`protect
   task HIDDEN;
      output [7:0] oHIDDEN;
      input [7:0] i1;
      input sel;
      reg [7:0] oHIDDEN;

      begin
	 if (sel) 
	   oHIDDEN = i1;
      end
   endtask
`endprotect   
      
   always @(posedge clock)
     begin: main
`protect
	HIDDEN(out, in1, sel);
`endprotect
     end
endmodule
`endif
