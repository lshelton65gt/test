// last mod: Wed Apr 12 07:16:28 2006
// filename: test/protected-source/bug5915.v
// Description:  This test demonstrates the problem described in bug5915, this
// file contains both the protected and unprotected version of this file,
// if you ever need to create a new protected version comment out the definition
// for USEPROTECTED and use the command: cbuild +protect bug5915.v to create a .vp
// file, then copy it into this file in place of USEPROTECTED section.


`define USEPROTECTED 1
`ifdef USEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module bug5915(clock, in1, in2, out1);
   input clock;
   input [15:0] in1, in2;
   output [15:0] out1;


   level1_visible_interface_protected_contents   l1pf_i1(out1[3:0],in1[3:0],in2[3:0]);
   level1_visible_interface_half_contents_protected   l1ph_i2(out1[11:4],in1[11:4],in2[11:4]);
   level1_visible_everything   l1pn_i3(out1[15:12],in1[15:12],in2[15:12]);
endmodule


// this module has all the contents protected (the interface is visible)
module level1_visible_interface_protected_contents(out, in1, in2);
   output[3:0] out;
   input [3:0] in1, in2;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQKpCQkY6QkJAi6AztXtGbEqCEl2DrD/KXn5mN5Ls4bJiV
lbmYgdN3Nt9HhKcVamRJhukZTnVvTkhr/Yl0F3GOj78LqP2aORnotjeqT7W120d6rJTAev3zkr9k
hpixWde+oY3R076plfXov6JW+UcnFqNTm1pKecCJbS4dq9HxSZ52ikzGMNKH6Jqv0iZuMsW2VUcL
D+Di1fu7c+14fN8T/XPXr3T5eo82ksWq0kFWglSRVD0DaeO/fifPsA=<
`endprotected
endmodule

// this module has half the contents protected (the interface is visible)
module level1_visible_interface_half_contents_protected(out, in1, in2);
   output[7:0] out;
   input [7:0] in1, in2;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQJZCQkYKQkJA96AztXK2fEqCAlnBNz1FQMBmTJJtBSZRV
wd1ECJZ6RY6mFUBaFy/OMNaHc2Qu5ynrphYDt//95Jqi6dgC/YcE6SMbxStWHH5zVjwewtYhj2qD
X2OERDCYO0HwNhAxJsAZBJO7O1KSJ+D1LHgHSgpxCgQScSToSOPgCTOezQCu9lsvDNwFaNjPTAkC
dTPZhjHDTxUcdTdM38X3tXoLs6QrH5TlkOV/5Ojukbi/zRY?
`endprotected
   level2_visible_interface_protected_contents   l2pf_l(out[4],in1[4],in2[4]);
   level2_visible_interface_half_contents_protected   l2ph_m(out[6:5],in1[6:5],in2[6:5]);
   level2_visible_everything   l2pn_n(out[7],in1[7],in2[7]);
endmodule


// this module has nothing protected
module level1_visible_everything(out, in1, in2);
   output[3:0] out;
   input [3:0] in1, in2;

   level2_visible_interface_protected_contents   l2pf_i(out[0],in1[0],in2[0]);
   level2_visible_interface_half_contents_protected   l2ph_j(out[2:1],in1[2:1],in2[2:1]);
   level2_visible_everything   l2pn_k(out[3],in1[3],in2[3]);      
endmodule







// this module has all the contents protected (the interface is visible)
module level2_visible_interface_protected_contents(out, in1, in2);
   output out;
   input  in1, in2;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ/ZCQkPKQkJD16AyjGlpj2Z+e2mFmPWLdI1ynHOLevqZd
3D5ko+G9PTy6pt9nWd+dWDziwaHEccLAwFjZvdtdoR6/uFq/2d2+2d0Zl2IbutuiomNC0SJWlflh
CYppNbWqCekW0Pw0+V2VkOj7sZg@
`endprotected
endmodule

// this module has half the contents protected (the interface is visible)
module level2_visible_interface_half_contents_protected(out, in1, in2);
   output[1:0] out;
   input [1:0] in1, in2;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ4pCQkPuQkJD66Azjv5yiJ9yfpLhjYdu9nttlppgbmiYg
XNhcvFxYpZmhpuQnZF4loGFexaHEccLAwFjZvdtdoR6/uFq/2d2+2d0Zl2IbutuiomNC0SJWlflh
CYppNbVBliGqCekWgFqY2PnK45GQ1XWyzA=<
`endprotected
   level3_visible_everything   l3pn_j(out[1],in1[1],in2[1]);
endmodule


// this module has nothing protected
module level2_visible_everything(out, in1, in2);
   output out;
   input  in1, in2;

   level3_visible_everything   l3pf_i(out,in1,in2);

endmodule





`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQWZCQkZGQkJBR6AytHa2bUqCAFmdsGovUhJTLr7bhgOW6
ytE8qNn8XqYAztoDhHs/pyFy4ud/l199vnd4JOzhchvb5tue2RYEzGVLxUj8Z1ceN/20wi6JVJOm
DRK9NRHamzeAbAV7Osb4weXAp0ry/sGQ+U8ykrR1WhngSUInWfJjdKkSuSXDko6YPaRiMQBtEJrL
xGNrD+RnKnkuIAHm5nKJ4YgjYRqRECf691Nq2tnRbau7htz+HLy45PCNsohmiQJ4DyKf2iPHrw<:

`endprotected

// this module has nothing protected
module level3_visible_everything(out, in1, in2);
   output out;
   input  in1, in2;

   assign out = in1 & in2;
endmodule

// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////
`else

module bug5915(clock, in1, in2, out1);
   input clock;
   input [15:0] in1, in2;
   output [15:0] out1;


   level1_visible_interface_protected_contents   l1pf_i1(out1[3:0],in1[3:0],in2[3:0]);
   level1_visible_interface_half_contents_protected   l1ph_i2(out1[11:4],in1[11:4],in2[11:4]);
   level1_visible_everything   l1pn_i3(out1[15:12],in1[15:12],in2[15:12]);
endmodule


// this module has all the contents protected (the interface is visible)
module level1_visible_interface_protected_contents(out, in1, in2);
   output[3:0] out;
   input [3:0] in1, in2;

`protect
   level2_visible_interface_protected_contents   l2pf_i(out[0],in1[0],in2[0]);      
   level2_visible_interface_half_contents_protected   l2ph_j(out[2:1],in1[2:1],in2[2:1]);
   level2_visible_everything   l2pn_k(out[3],in1[3],in2[3]);      
`endprotect
endmodule

// this module has half the contents protected (the interface is visible)
module level1_visible_interface_half_contents_protected(out, in1, in2);
   output[7:0] out;
   input [7:0] in1, in2;

`protect
   level2_visible_interface_protected_contents   l2pf_i(out[0],in1[0],in2[0]);
   level2_visible_interface_half_contents_protected   l2ph_j(out[2:1],in1[2:1],in2[2:1]);
   level2_visible_everything   l2pn_k(out[3],in1[3],in2[3]);
`endprotect
   level2_visible_interface_protected_contents   l2pf_l(out[4],in1[4],in2[4]);
   level2_visible_interface_half_contents_protected   l2ph_m(out[6:5],in1[6:5],in2[6:5]);
   level2_visible_everything   l2pn_n(out[7],in1[7],in2[7]);
endmodule


// this module has nothing protected
module level1_visible_everything(out, in1, in2);
   output[3:0] out;
   input [3:0] in1, in2;

   level2_visible_interface_protected_contents   l2pf_i(out[0],in1[0],in2[0]);
   level2_visible_interface_half_contents_protected   l2ph_j(out[2:1],in1[2:1],in2[2:1]);
   level2_visible_everything   l2pn_k(out[3],in1[3],in2[3]);      
endmodule







// this module has all the contents protected (the interface is visible)
module level2_visible_interface_protected_contents(out, in1, in2);
   output out;
   input  in1, in2;

`protect
   level3_protected_everything   l3pf_i(out,in1,in2);
`endprotect
endmodule

// this module has half the contents protected (the interface is visible)
module level2_visible_interface_half_contents_protected(out, in1, in2);
   output[1:0] out;
   input [1:0] in1, in2;

`protect
   level3_protected_everything   l3pf_i(out[0],in1[0],in2[0]);
`endprotect
   level3_visible_everything   l3pn_j(out[1],in1[1],in2[1]);
endmodule


// this module has nothing protected
module level2_visible_everything(out, in1, in2);
   output out;
   input  in1, in2;

   level3_visible_everything   l3pf_i(out,in1,in2);

endmodule





`protect
// this module is completely protected nothing including the interface should be visible
module level3_protected_everything(out, in1, in2);
   output out;
   input  in1, in2;

   assign out = in1 & in2;
endmodule
`endprotect

// this module has nothing protected
module level3_visible_everything(out, in1, in2);
   output out;
   input  in1, in2;

   assign out = in1 & in2;
endmodule
`endif
