// last mod: Thu May  4 15:16:20 2006
// filename: test/protected-source/protected_port_coerce.v
// Description:  This test is based on test/bugs/bug95 except that it is
// protected to force the code through VerilogPopulate::checkCoercionLvalue
// must be run with -noCoercePorts
// cbuild -q +protect protected_port_coerce.v
// cbuild -q -noCoercePorts protected_port_coerce.vp


`protect
module portsize (in1, out1);

   input  [1:0] in1;
   output [1:0] out1;

   assign 	out1 = in1;

endmodule
`endprotect

module MISC160 (in11,out11,in21,out21);


   input [1:0] in11;
   output [1:0]out11;
   input [1:0] in21, out21;

   portsize inst1 (in11, out11);
`protect
   // the following line should generate an error message about requiring port
   // coercion, but since it is protected the user should not see it.
   portsize inst2 (in21, out21); // Note input port is being connected to output port
`endprotect

endmodule
