library ieee ; 
use ieee.std_logic_1164.all ;
use work.all ;

entity mixed_vhdl_top_01 is
      port (in1 : in  std_logic ;
            in2 : in  std_logic ;
            out1 : out std_logic) ;

end mixed_vhdl_top_01 ;

architecture arch_mixed_vhdl_top_01 of mixed_vhdl_top_01 is
   Signal temp1 : std_logic ;

   Component should_never_appear
     port (in1 : in  std_logic ;
           in2 : in  std_logic ;
           out1 : out std_logic) ;
   end Component ;

   begin

    U1 : should_never_appear
      Port Map ( in1, in2, temp1 ) ;

    U2 : should_never_appear
      Port Map ( temp1, temp1, out1 );

end arch_mixed_vhdl_top_01 ;
