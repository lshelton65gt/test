// This testcase makes sure we lack visibility to registers declared
// in protected regions

module top(a, b, c, out);
  input a, b, c;
  output out;

`protect
  // This is really a mux but no one outside should be able to
  // see any of the protected variables

  wire   protected1 = ~a;
  wire   protected2 = protected1 & b;
  wire   protected3 = a & c;
  assign out = protected2 | protected3;
`endprotect

endmodule
