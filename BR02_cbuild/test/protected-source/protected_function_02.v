// last mod: Fri May  5 09:36:16 2006
// filename: test/protected-source/protected_function_02.v
// Description:  This test has a hierarchical reference to a protected function,
// and is run with hierrefs disabled, so we should see an error message, but
// that message should not expose the protected name
// run this with 2 commands:
// cbuild -q +protect protected_function_02.v
// cbuild -q -2001 -disallowHierRefs protected_function_02.vp


module protected_function_02(clock, in1, in2, out1, out2);
   input clock;
   input [7:0] in1,in2;
   output [7:0] out1, out2;

   left  i_left(out1, in1);
   right i_right(out2, in2);

endmodule

module left(output [7:0] out, input [7:0] in);

   assign out = i_right.HIDDEN_fct(in);

endmodule

module right(output [7:0] out, input[7:0] in);

`protect
   function HIDDEN_fct;
      input [7:0] i1;
      begin
	 HIDDEN_fct = ~i1;
      end
   endfunction
   
   HIDDEN_1 h1(out, in);

`endprotect
endmodule

`protect   
module HIDDEN_1(output [7:0] out, input[7:0] in);

   assign out = in;
endmodule
`endprotect

