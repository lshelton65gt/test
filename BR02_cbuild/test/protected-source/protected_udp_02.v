// last mod: Tue Apr 25 14:30:36 2006
// filename: test/protected-source/protected_udp_02.v
// Description:  This test checks for the handling of UDPs in an udp error message



// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_udp_02.v
// to create a protected_defines_02.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module protected_udp_02(output out1, out2, input a1, a2, b1, b2, c1, c2, d1, d2, e1, e2);

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQA5CQkDuQkJAb6Az9XCGeEqCE0EENP+iTE7a2QIcIDKAl
BgjkQQ0EJrTwypzAxC5Oh/cXI0xxdvsxFH9HPTw7R7xhtP9F5iniha5uvejxxM5OVaQ1CJLQ0vRd
acmlQa4LcUHlHEKeViggk/hCgtO84Zxm11iimLNk8c7koJ4SZn5fl7FyfxNksb2hVIIXZNkuEAu/
rQ<>
`endprotected   
endmodule

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCSg5CQmguQkJKb6AxFBMv/C6CEEE9phXcxn8sGObbLq/np
IMTlSrrdQ4b5/WeEgeLYjFCwfLRkT18nGgCAxJq/HRihSE9hYRW/6+xqrLv3ad4A759nR68//hpX
O6tiYGdD9NL2YmsHuEhvLi/ev7aHDgeHvPUCfYDuTE1O7mulT75jabYeso9O9cupFl2IcqEUGbka
aYiUWmlrOZeQOq1P2u0LkniHHHt39lSAlT5QEsPfr1vwATB3zc5SlrCWXRDxxAsWRH27bAE2aHB7
7mY3yZJwOxg61lHE5c86m02klUWp3CKgxsmR4ZAhEO+c7OQQC8ER+VAokIE0eEkul7SttG1zMeyX
DHZ2J3TGkZezAAaBOBmkOpPunbjgKZ2B4oTSfSHYTYrAG4201voZpfFEzAFT+ENCYZxS5VDHuLBz
UOEVW4V6nUVsWNAbh2JrpeLlsFQOWeAdUpXAaQX9Q/2f8PdNuRMohlA+5kkm4n2ReGP1bjR5gDmz
fx7wlbxM+DM2qRdJnpuYApT0geBRFPSJp4swLw56DAQtE0C6fVbfPVDjWR9evUdp4SWROkPKZpvC
z2dKXge+2FAPRHbZhqGFW/4xQYNiNaUEOsQyxFAjtMtsqemTbqxCcZv96LpO5n8OSiM7rCsqIzuq
z/deRxkjN6dr+iM/pWvaIz+jK7rjmPOesVx+LozSB9NI4tDJ6m+RnC0eng??
`endprotected
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else

module protected_udp_02(output out1, out2, input a1, a2, b1, b2, c1, c2, d1, d2, e1, e2);

`protect   
   HIDDEN_udp_jkff1   ( out1, a1, b1, c1, d1, e1 ); // no instance name
   HIDDEN_udp_jkff2 u2( out2, a2, b2, c2, d2, e2 );
`endprotect   
endmodule

`protect

primitive HIDDEN_udp_jkff1 (out, j, k, clk, clr_, set_);
   output out;  
   input  j, k, clk, clr_, set_;
   reg    out;

   table

// j  k  clk  clr_   set_   : Qt : Qt+1
//       
   0  0  r   1   1    : ?  :  -  ; // output remains same
   0  1  r   ?   1    : ?  :  0  ; // clock in 0
   1  0  r   1   ?    : ?  :  1  ; // clock in 1
   1  1  r   ?   1    : 1  :  0  ; // clock in 0
   1  1  r   1   ?    : 0  :  1  ; // clock in 1
   ?  0  *   1   ?    : 1  :  1  ; // reduce pessimism
   0  ?  *   ?   1    : 0  :  0  ; // reduce pessimism
   ?  ?  f   ?   ?    : ?  :  -  ; // no changes on negedge clk
   *  ?  b   ?   ?    : ?  :  -  ; // no changes when j switches
   *  0  x   1   ?    : 1  :  1  ; // no changes when j switches
   ?  *  b   ?   ?    : ?  :  -  ; // no changes when k switches
   0  *  x   ?   1    : 0  :  0  ; // no changes when k switches
   ?  ?  ?   ?   0    : ?  :  1  ; // set output
   ?  ?  b   1   *    : 1  :  1  ; // cover all transistions on set_
   ?  0  x   1   *    : 1  :  1  ; // cover all transistions on set_
   ?  ?  ?   0   1    : ?  :  0  ; // reset output
   ?  ?  b   *   1    : 0  :  0  ; // cover all transistions on clr_
   0  ?  x   *   1    : 0  :  0  ; // cover all transistions on clr_
   ?  ?  ?   ?   ?    : ?  :  x  ; // any notifier change

   endtable
endprimitive // udp_jkff

primitive HIDDEN_udp_jkff2 (out, j, k, clk, clr_, set_);
   output out;  
   input  j, k, clk, clr_, set_;
   reg    out;

   table

// j  k  clk  clr_   set_   : Qt : Qt+1
//       
   0  0  r   1   1   : ?  :  -  ; // output remains same
   0  1  r   ?   1   : ?  :  0  ; // clock in 0
   1  0  r   1   ?   : ?  :  1  ; // clock in 1
   1  1  r   ?   1   : 1  :  0  ; // clock in 0
   1  1  r   1   ?   : 0  :  1  ; // clock in 1
   ?  0  *   1   ?   : 1  :  1  ; // reduce pessimism
   0  ?  *   ?   1   : 0  :  0  ; // reduce pessimism
   ?  ?  f   ?   ?   : ?  :  -  ; // no changes on negedge clk
   *  ?  b   ?   ?   : ?  :  -  ; // no changes when j switches
   *  0  x   1   ?   : 1  :  1  ; // no changes when j switches
   ?  *  b   ?   ?   : ?  :  -  ; // no changes when k switches
   0  *  x   ?   1   : 0  :  0  ; // no changes when k switches
   ?  ?  ?   ?   0   : ?  :  1  ; // set output
   ?  ?  b   1   *   : 1  :  1  ; // cover all transistions on set_
   ?  0  x   1   *   : 1  :  1  ; // cover all transistions on set_
   ?  ?  ?   0   1   : ?  :  0  ; // reset output
   ?  ?  b   *   1   : 0  :  0  ; // cover all transistions on clr_
   0  ?  x   *   1   : 0  :  0  ; // cover all transistions on clr_
   ?  ?  ?   ?   ?   : ?  :  x  ; // any notifier change

   endtable
endprimitive // udp_jkff


`endprotect
`endif
