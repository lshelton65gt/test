/*
 * This module contains several sections bracketed by `protect and `endprotect
 * compiler directives.
 *
 * When processed by cbuild +protect, these should become `protected sections
 * containing the `protect source in encrypted and asci-encoded form.
 *
 * To facilitate testing with grep, lines with PROTECTED should no longer be 
 * visible after cbuild +protect, and lines with CLEAR should be visible.
 *
 * Calls to $display with both CLEAR and PROTECTED in them should execute
 * when either the protected or unprotected source is simulated.
 *
 * See the comments above the individual sections for more details.
 */
module protect();

reg c;
wire b;

initial
  begin

// Even though only one of these sections can be active for any give run,
// BOTH sections are converted by +protect processing.
`ifdef FOO
  `protect c = 1; 
   $display("PROTECTED: ifdef FOO");
  `endprotect 
`else
  `protect c = 2; 
   $display("PROTECTED: ifndef FOO");
  `endprotect 
`endif

/*
 * This section will NOT be protected, because the compiler directives
 * appear within a line comment.
 */
// `protect
   $display("CLEAR: line comment");
// `endprotect

// This section will NOT be protected, because the compiler directives
// appear within a block comment
/*
 `protect
 */
$display("CLEAR: block comment 1");
/*
 `endprotect
 */


// Likewise, this one is not protected.
/*
`protected
$display("CLEAR: block comment 2");
`endprotected
*/

/*
 * These lines should be left alone, because they are in a 
 * constant string.
 */
 $display("CLEAR: what about a `protect inside a constant string?");
 $display("CLEAR: that could be an `endprotect issue.");

/*
 * The `include is an interesting case.  The protected code before 
 * and after `include gets encrypted, but the actual `include does not.
 */
`protect
  $display("PROTECTED: before `include");
`include "include.v"
  $display("PROTECTED: after `include");
`endprotect 

/*
 * Unlike `include, `protect can appear anywhere in an expression.
 * Here we check that the value 8 gets protected.
 */
$display("secret value ", `protect "PROTECTED: ", 8, `endprotect );

end

assign b = c;

endmodule
