// last mod: Wed May  3 07:11:21 2006
// filename: test/protected-source/bug5915_2.v
// Description:  This test is a check to see that when protected instances are
// nested, the subtree below the protected section does not become visible.  In
// this example only the first instance of a module are protected, see also
// bug5915_3 where only the second instance is protected (this pair of tests
// checks to make sure we do the proper protection without regard to the order
// that instances are processed)

// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file:
// comment out the definition for USEPROTECTED and use the command:
//   cbuild +protect bug5915_2.v
// to create a bug5915_2.vp file, then copy the generated module into
// this file in place of USEPROTECTED section.


`define USEPROTECTED 1
`ifdef USEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module bug5915_2(out, in1, in2);
   input [15:0] in1, in2;
   output [15:0] out;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQz5CQkMqQkJDH6Aw7GrrZ2rnnneHn2jqcnKagmefaZWI/
oLrc4p8Z3NzcX59kn+fdp6BewaHEccLAwGChGr+4utm+wbigRFi/vRmmJ6IYRcFYXKMUIxxQvN37
vpCK/4p9
`endprotected   
   L2_clear c1(out[15:8], in1[15:8], in2[15:8]);
      
endmodule

module L2_prtct(out, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQz5CQkMqQkJDH6Aw7pKRmHlqm259mZ9lmor+lod06XOA6
HNxc31paY6biHmKfZ7zkpL1lw6HEccLAwGChHr+4utm+wbigRFi/vRmmJqIYRcFYXKMUIxxQvN37
vpBq5ooE
`endprotected   
   L3_clear c1(out[7:4], in1[7:4], in2[7:4]);

endmodule

module L2_clear(out, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out;

   L3_prtct p1(out[3:0], in1[3:0], in2[3:0]);
   L3_clear c1(out[7:4], in1[7:4], in2[7:4]);

endmodule



module L3_prtct(out, in1, in2);
   input [3:0] in1, in2;
   output [3:0] out;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQz5CQkMqQkJDH6AzbumW8v12fp6GmZeM+pLtcWaXeYb7i
5eJbYN+m4+a5J9hbnGEcuGKlwKHEccLAwGChGb+4utm+wbigRFi/vRmmJKIYRcFYXKMUIxxQvN37
vpCU2IoD
`endprotected   
   L4_clear c1(out[3:2], in1[3:2], in2[3:2]);

endmodule

module L3_clear(out, in1, in2);
   input [3:0] in1, in2;
   output [3:0] out;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQz5CQkMqQkJDH6Ayj2KLkZuGnW5uavGHipNxi4706ZN9m
2Tq6mLsauKKhY6GiZqW8u7hcw6HEccLAwGChGb+4utm+wbigRFi/vRmmJKIYRcFYXKMUIxxQvN37
vpB5y4r7
`endprotected   
   L4_clear c1(out[3:2], in1[3:2], in2[3:2]);

endmodule


module L4_prtct(out, in1, in2);
   input [1:0] in1, in2;
   output [1:0] out;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQzZCQkMOQkJDF6AxbXBqcmp7ZXblcY6Fn4l/b4yNcOOAf
WpmmX59hvOImvGfavZydujikwaHEccLAwNi8vl7cX5OihGLb2zKd8oX8haJjnNGcpbBTmFggdpKQ
/qmJqg;?
`endprotected
   assign    out[1] = in1[1] & in2[1];

endmodule

module L4_clear(out, in1, in2);
   input [1:0] in1, in2;
   output [1:0] out;

   assign    out[0] = in1[0] & in2[0];
   assign    out[1] = in1[1] & in2[1];

endmodule


// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////
`else

module bug5915_2(out, in1, in2);
   input [15:0] in1, in2;
   output [15:0] out;

`protect
   L2_prtct p1(out[7:0], in1[7:0], in2[7:0]);
`endprotect   
   L2_clear c1(out[15:8], in1[15:8], in2[15:8]);
      
endmodule

module L2_prtct(out, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out;

`protect
   L3_prtct p1(out[3:0], in1[3:0], in2[3:0]);
`endprotect   
   L3_clear c1(out[7:4], in1[7:4], in2[7:4]);

endmodule

module L2_clear(out, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out;

   L3_prtct p1(out[3:0], in1[3:0], in2[3:0]);
   L3_clear c1(out[7:4], in1[7:4], in2[7:4]);

endmodule



module L3_prtct(out, in1, in2);
   input [3:0] in1, in2;
   output [3:0] out;

`protect
   L4_prtct p1(out[1:0], in1[1:0], in2[1:0]);
`endprotect   
   L4_clear c1(out[3:2], in1[3:2], in2[3:2]);

endmodule

module L3_clear(out, in1, in2);
   input [3:0] in1, in2;
   output [3:0] out;

`protect
   L4_prtct p1(out[1:0], in1[1:0], in2[1:0]);
`endprotect   
   L4_clear c1(out[3:2], in1[3:2], in2[3:2]);

endmodule


module L4_prtct(out, in1, in2);
   input [1:0] in1, in2;
   output [1:0] out;

`protect
   assign    out[0] = in1[0] & in2[0];
`endprotect
   assign    out[1] = in1[1] & in2[1];

endmodule

module L4_clear(out, in1, in2);
   input [1:0] in1, in2;
   output [1:0] out;

   assign    out[0] = in1[0] & in2[0];
   assign    out[1] = in1[1] & in2[1];

endmodule

`endif
