// last mod: Thu Apr 20 14:54:38 2006
// filename: test/protected-source/protected_init_01.v
// Description:  This test has code within an init block protected, make sure
// that that code is not made visible by any carbon generated files.

// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_init_01.v
// to create a protected_init_01.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module protected_init_01 (input clk, in1, output reg [31:0] out1);

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQC5CQkCSQkJAD6AzbPdyaphyaoeZeWjgaomBf4LplmJ6g
GKSfY7xjmZ/jX2UY4mdfYR88wqHCccLAwFhcW7xZ3FyRoRGwuaWto1/asbOjtbWlHxvDMbilzbFK
SEBasMaxPaS3N6IuMLgvtKUptKVVCpu4HbY28Dvw/DQOAQIKmESewlI5AM/K8piE1sc4PfD0nSWi
pb8Vm5BFEKfJ
`endprotected
   
   always @(posedge clk)
     begin
	out1 = out1 + in1;
     end
endmodule
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else

module protected_init_01 (input clk, in1, output reg [31:0] out1);

`protect
   initial
      begin: hidden
	 reg [31:0] fully_protected;

	 fully_protected = 32'hdeaddeed;
	 out1 = fully_protected + 2;
      end
`endprotect
   
   always @(posedge clk)
     begin
	out1 = out1 + in1;
     end
endmodule
`endif
