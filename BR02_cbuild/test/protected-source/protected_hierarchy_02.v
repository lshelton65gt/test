// last mod: Fri Apr 28 08:35:17 2006
// filename: test/protected-source/protected_hierarchy_02.v
// Description:  This test has a hierarchy of modules, the top 2 levels are
// visible, the remainder is `protected,
// top.left has a hier ref to a memory hidden inside the protected region



// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_hierarchy_02.v
// to create a protected_defines_02.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below

module protected_hierarchy_02(clock, in1, in2, out1);
   input clock;
   input [7:0] in1,in2;
   output [7:0] out1;

   left i_left(in1);
   right i_right(out1, in2);

endmodule

module left(input [7:0] in);

   assign i_right.h1[1000].h2.HIDDEN_val1 = in[7:6];
   assign i_right.h1[1001].h2.HIDDEN_val1 = in[5:4];
   assign i_right.h1[1002].h2.HIDDEN_val1 = in[3:2];
   assign i_right.h1[1003].h2.HIDDEN_val1 = in[1:0];

   always @(i_right.h1[1000].h2.HIDDEN_val2 or i_right.h1[1001].h2.HIDDEN_val2
	    or i_right.h1[1002].h2.HIDDEN_val2 or i_right.h1[1003].h2.HIDDEN_val2)
      begin
	 $display( "Hidden value: %b", {i_right.h1[1000].h2.HIDDEN_val2,i_right.h1[1001].h2.HIDDEN_val2,i_right.h1[1002].h2.HIDDEN_val2,i_right.h1[1003].h2.HIDDEN_val2});
      end
endmodule

module right(output [7:0] out, input[7:0] in);

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ8ZCQkMSQkJDJ6AybY7+YHZyi2iTYp5w6uJ4b3KYbXGec
n+Ce5lzY42MY4GFeoppgmaSjwqHCwcDAcJLyn9+Hh8cv6NMVnFP405OTk7uw8Yw7Ac/KMjMACTf5
XVWVkJzqhhw@
`endprotected
endmodule

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ7pCQkBWQkJDm6Ayj5OFeZlwnINhjY6dg2KJj2L9mvhu6
5GTcuZ695F2dY7jY3WfeWt+ixKHCwcDAcFpd37ld2cVgYOTh4WUbp0RYv725uL3BGKYkohjFkOLk
hKJjEJKA7vYONqWHh8CLxCkB0hYBlsSBwLalv5XyjIeQU2q49g;?
`endprotected

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQMZCQkHWQkJAJ6AzbnNm8OdgemlodGtq6Zh+8WRpiPZxj
ZNxbp9gf4r4cYJtiPOQiOFqlwaHCwcDAcFpd37ld2cVgYOTh4WUbp0JYv725uL3BGKYkohjFkOLk
hKJjEJKA7vYONqWHh8DL6fbBusSdxPfJ8h4xpcyasfiUQQDIzAwJDhe8PvAbPMXRi/gopQLaMEW4
utB2NHY10AxblZBBR9WE
`endprotected

// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else


module protected_hierarchy_02(clock, in1, in2, out1);
   input clock;
   input [7:0] in1,in2;
   output [7:0] out1;

   left i_left(in1);
   right i_right(out1, in2);

endmodule

module left(input [7:0] in);

   assign i_right.h1[1000].h2.HIDDEN_val1 = in[7:6];
   assign i_right.h1[1001].h2.HIDDEN_val1 = in[5:4];
   assign i_right.h1[1002].h2.HIDDEN_val1 = in[3:2];
   assign i_right.h1[1003].h2.HIDDEN_val1 = in[1:0];

   always @(i_right.h1[1000].h2.HIDDEN_val2 or i_right.h1[1001].h2.HIDDEN_val2
	    or i_right.h1[1002].h2.HIDDEN_val2 or i_right.h1[1003].h2.HIDDEN_val2)
      begin
	 $display( "Hidden value: %b", {i_right.h1[1000].h2.HIDDEN_val2,i_right.h1[1001].h2.HIDDEN_val2,i_right.h1[1002].h2.HIDDEN_val2,i_right.h1[1003].h2.HIDDEN_val2});
      end
endmodule

module right(output [7:0] out, input[7:0] in);

`protect   
   HIDDEN_1 h1[1000:1003](out, in);

`endprotect
endmodule

`protect   
module HIDDEN_1(output [1:0] out, input[1:0] in);

   HIDDEN_2 h2(out, in);
endmodule
`endprotect

`protect   
module HIDDEN_2(output [1:0] out, input[1:0] in);

   wire [1:0] HIDDEN_val1;
   wire HIDDEN_val2;

   assign HIDDEN_val2 = HIDDEN_val1 + in;
   assign out = HIDDEN_val2;

endmodule
`endprotect

   
`endif
