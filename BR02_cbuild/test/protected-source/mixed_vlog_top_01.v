// last mod: Wed Apr 12 15:12:58 2006
// filename: test/protected-source/mixed_vlog_top_01.v
// Description:  This test is just a mixed language testcase where the top level
// verilog is protected, we want to be sure that when dumping hierarchy nothing
// escapes.
//
// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file:
// comment out the definition for USEPROTECTED and use the command:
//   cbuild +protect cycle-dumping-test3.v
// to create a cycle-dumping-test3.vp file, then copy the generated module into
// this file in place of USEPROTECTED section.

`define USEPROTECTED 1
`ifdef USEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQOJCQkCCQkJAw6Ay9XVGbEqCElmBrbhqNrLgYaIblWPMY
6LnYgNvyQCZSCF5Jy9bv7QMqre7sT2dCKuh4g/tg/KdSxSs5QtfGjggtTloqSUvMyLOu1Y7Q0Oqz
DcYkf07aYgem5x72dLlg4aI4lOplgF6AQwnVibFNoKo0JBVlpi1c0KPyjL5kh3YVMOqy1fjHzvgI
aJQULo63TXCfGAaxpVhP42KVsuunHQ<>
`endprotected
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////
`else

`protect
module mixed_vlog_top_01_protected_top(w1, w2);

input  [15:0]w1 ;
output [15:0]w2 ;

test t1[3:0] (.out1(w2),.in1(w1));

endmodule
`endprotect

`endif
