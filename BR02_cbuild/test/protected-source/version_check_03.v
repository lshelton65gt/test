// last mod: Mon Apr 17 07:29:02 2006
// filename: test/protected-source/version_check_base.v
// Description:  This test is used to verify that new versions of cbuild can
// continue to read and process designs that were protected with old versions of
// cbuild.  With each increment of the protection version number this file
// should be re-protected (creating a new .vp file) and that file should be
// saved in CVS, and a new test should be added to test.run

// to create a new protected version of this file for a particular
// PROTECT_VERSION value use the followingcommands (substitute for
// <VERSIONNUMBER>):

// cbuild -q +protect version_check_base.v
// mv version_check_base.vp version_check_<VERSIONNUMBER>.v

// version_check_future.v is created by advancing the PROTECT_VERSION to a value
// beyond the current value, building cbuild and executing the commands:
// cbuild -q +protect version_check_base.v
// mv version_check_base.vp version_check_future.v
// then changing the PROTECT_VERSION back to the correct value


module version_check(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ7pCQkOqQkJDm6Aw7XLjhYp+ZpOHjnaY74mK456Ommril
5ttd3Ru6PTzaml8d3LmnpB5iw6HGccLAwNhcud88vMbgQLhYv97d2d/F2F5Z315GlNm5uLQ1Nvd2
ybl0tvZ20ZSEhGLb25yF/IWiY5yFRBA0AaXUsqW/FZuQg8aznQ?>
`endprotected
endmodule
