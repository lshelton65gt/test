// last mod: Fri Apr 14 10:24:08 2006
// filename: test/protected-source/multi_top_01.v
// Description:  This test has multiple top level modules, the one selected by
// vlogTop is not protected while others are.  Those names should never appear
// in any generated file.

// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect multi_top_01.v
// to create a multi_top_01.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module multi_top_01(out1, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out1;
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQxJCQkNiQkJDc6Awj4KWjuOUkYFmaXmNb4dokIODaZ9nh
YTigG1om3F6huxw44Bu6vqXYxaHEccLAwFhd3MFYpMRAWL+9oUTBWFyTg9YKRsyQigWEWQ=>
`endprotected
endmodule

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQDJCQkdKQkJAE6AwbnCAk3N9au6BkYr9hvVnfnjpYW+bn
vKegXd7jpaDgXN9cojsipGClxqHEcVpd37ld2cW4Xli/XdkZX1u/Gd/aHb9bvF7cWtmloERYv72h
RMFYXJOD1gpGzJqa0NbR+RnSJCkF0bzMlryRxBwAkXkkdpKZd/Y2uPT0NjQ0dom68IoICgeS4ZPu
N4hBRbmQS87zwQ:>
`endprotected
module mid(out1, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out1;
   assign out1 = in1 | in2;
endmodule

module can_be_visible01(out1, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out1;
   assign out1 = in1 & in2;
endmodule
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else
module multi_top_01(out1, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out1;
`protect
   mid i1 (out1, in1, in2);
`endprotect
endmodule

`protect
module should_not_be_visible01(out1, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out1;

   mid hidden1 (out1, in1, in2);
endmodule
module should_not_be_visible02(out1, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out1;

   mid hidden1 (out1, in1, in2);
endmodule
`endprotect
module mid(out1, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out1;
   assign out1 = in1 | in2;
endmodule

module can_be_visible01(out1, in1, in2);
   input [7:0] in1, in2;
   output [7:0] out1;
   assign out1 = in1 & in2;
endmodule
			
`endif
