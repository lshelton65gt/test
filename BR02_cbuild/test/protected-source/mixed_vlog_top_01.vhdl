----------------------------------------------------------
--  Copyright (c) 2003 Interra Systems, Inc.
--  Use, disclosure or distribution is prohibited without
--  prior written permission of Interra Systems, Inc.
----------------------------------------------------------

entity test is
    port (in1  : in bit_vector(0 to 3);
          out1 : out bit_vector(0 to 3));
end;

architecture arch of test is
begin
    process (in1)
    begin
        out1 <= in1;
    end process;
end;
