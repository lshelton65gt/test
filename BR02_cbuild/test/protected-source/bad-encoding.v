/*
 * This file contains a protected block that cannot be decoded by cbuild.
 * The idea is to test that we report the error and exit cleanly.
 */

module foo();

`protected
@pLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ+JCQkMmQkJDw6Axj5p9d5L3lI7hku2Q/4uIfpJtc5R2cd
phrYW5k[Z5GJg5Bs3N9bo5+h4sDYxiDFoCTGcMLAwMDZWby+WNk8RMCamGKf4eWe4eUhwljc289nd
w+BjZ8dCJJa6kpBZcok1
`endprotected

endmodule
