// last mod: Thu Apr 27 12:53:40 2006
// filename: test/protected-source/protected_clocks_01.v
// Description:  This test checks that clocks from the protected areas do not
// appear in the .clocks file.  It also checks that the .parameter file does not
// show protected parameters

// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_clocks_01.v
// to create a protected_defines_01.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module protected_clocks_01(in, out, clkvec, addr, we, mclk);
  input in, we, mclk;
  input [1:0] clkvec, addr;
  output      out;

  reg [1:0]   mem[0:3];

  always @(posedge mclk)
    if (we)
      mem[addr] = clkvec;

  sub #(1) subI(in, out, mem[0]);
endmodule


`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQc5CQkRaQkJBL6Ayj3V7jZl66p6CcmJkeYrze3p1lWePf
PuKmZJunvhrgm7tnv7mnotu7wqHCcVpd37ld2cW4vt1CYGDk4eFlG19cQ8EQomPb23B8dAx8IkT0
3fu+lRUSVDJUTEQCRLIIzBExEj0Sgdi6oz8wJNSR/oLSuIo+RFqwhkXcALIw3bDF8BAgjLTDBAo+
kJM4onUJ1Tn4otCTEb7Roa2K+M2MCgDxPKWHSPUJtQkZqdhWkqWLEBzeXLnfPLzG4EC4WL/e3dnf
1UI/WRV6gpWLy/RvNnY10BIEm5ALfuNX
`endprotected

// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else

module protected_clocks_01(in, out, clkvec, addr, we, mclk);
  input in, we, mclk;
  input [1:0] clkvec, addr;
  output      out;

  reg [1:0]   mem[0:3];

  always @(posedge mclk)
    if (we)
      mem[addr] = clkvec;

  sub #(1) subI(in, out, mem[0]);
endmodule


`protect
module sub(HIDDEN_in, HIDDEN_out, HIDDEN_clkvec);
  parameter HIDDEN_p1 = 2;
  input HIDDEN_in;
  input [HIDDEN_p1:0] HIDDEN_clkvec;
  output      HIDDEN_out;
  reg         HIDDEN_out;
  wire        HIDDEN_clk = HIDDEN_clkvec[0] ^ HIDDEN_clkvec[1];

  initial HIDDEN_out = 0;
  always @(posedge HIDDEN_clk)
    HIDDEN_out <= HIDDEN_in;
endmodule
`endprotect
`endif
