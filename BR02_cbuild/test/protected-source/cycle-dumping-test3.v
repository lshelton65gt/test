// last mod: Wed Apr 12 14:45:24 2006
// filename: test/protected-source/cycle-dumping-test2.v
// Description:  This test is the same as test/cycle-dumping except that part of
// the cycle is `protected.
// 
// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file:
// comment out the definition for USEPROTECTED and use the command:
//   cbuild +protect cycle-dumping-test3.v
// to create a cycle-dumping-test3.vp file, then copy the generated module into
// this file in place of USEPROTECTED section.


`define USEPROTECTED 1
`ifdef USEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below

module top (out1, out2, out3, out4, clk, en1, en2, en3, en4, in1, in2, in3, in4);
   output out1, out2, out3, out4;
   input  clk, en1, en2, en3, en4, in1, in2, in3, in4;

   wire   gclk1, gclk2, gclk3, gclk4;
   reg    ren1a, ren1b, ren2a, ren2b, ren3a, ren3b, ren4a, ren4b;
   
   always @ (posedge gclk4)
     ren4a = en4;
   assign gclk4 = ren4b & gclk3;
   always @ (gclk3 or ren4a)
     if (~gclk3)
       ren4b = ren4a;

   always @ (posedge gclk3)
     ren3a = en3;
   assign gclk3 = ren3b & gclk2;
   always @ (gclk2 or ren3a)
     if (~gclk2)
       ren3b = ren3a;

   always @ (posedge gclk2)
       ren2a = en2;
   assign gclk2 = ren2b & gclk1;
   always @ (gclk1 or ren2a)
     if (~gclk1)
       ren2b = ren2a;

   always @ (posedge gclk1 or negedge gclk4)
     if (~gclk4)
       ren1a = 0;
     else
       ren1a = en1;
   assign gclk1 = clk & ren1b;
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQ/pCQkP+QkJD26AxjHOC8G6KiX97dO9qhv59kYGW4oOQZ
GiJcmpuhGaDaprij3qbb4aXlxqHEwcDAcJJyVAxiVFrylZeVHXQM/BVsshUyRKxT1N0ABBLS9goS
1o3AjFrFkNu2uUjShMnjkZCUKY7q
`endprotected

   reg out1, out2, out3, out4;
   always @ (posedge gclk1)
     out1 <= in1;
   always @ (posedge gclk2)
     out2 <= in2;
   always @ (posedge gclk3)
     out3 <= in3;
   always @ (posedge gclk4)
     out4 <= in4;

endmodule

// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////
`else

module top (out1, out2, out3, out4, clk, en1, en2, en3, en4, in1, in2, in3, in4);
   output out1, out2, out3, out4;
   input  clk, en1, en2, en3, en4, in1, in2, in3, in4;

   wire   gclk1, gclk2, gclk3, gclk4;
   reg    ren1a, ren1b, ren2a, ren2b, ren3a, ren3b, ren4a, ren4b;
   
   always @ (posedge gclk4)
     ren4a = en4;
   assign gclk4 = ren4b & gclk3;
   always @ (gclk3 or ren4a)
     if (~gclk3)
       ren4b = ren4a;

   always @ (posedge gclk3)
     ren3a = en3;
   assign gclk3 = ren3b & gclk2;
   always @ (gclk2 or ren3a)
     if (~gclk2)
       ren3b = ren3a;

   always @ (posedge gclk2)
       ren2a = en2;
   assign gclk2 = ren2b & gclk1;
   always @ (gclk1 or ren2a)
     if (~gclk1)
       ren2b = ren2a;

   always @ (posedge gclk1 or negedge gclk4)
     if (~gclk4)
       ren1a = 0;
     else
       ren1a = en1;
   assign gclk1 = clk & ren1b;
`protect   
   always @ (clk or ren1a)
     if (~clk)
       ren1b = ren1a;
`endprotect

   reg out1, out2, out3, out4;
   always @ (posedge gclk1)
     out1 <= in1;
   always @ (posedge gclk2)
     out2 <= in2;
   always @ (posedge gclk3)
     out3 <= in3;
   always @ (posedge gclk4)
     out4 <= in4;

endmodule

 
 
`endif
