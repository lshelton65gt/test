// last mod: Thu Apr 27 12:52:49 2006
// filename: test/protected-source/protected_genblock_01.v
// Description:  This test is to check that genblock names within a protected
// region do not get included in generated files

// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_genblock_01.v
// to create a protected_defines_01.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module protected_genblock_01(clk, in1, in2, in3, in4, out1, out2, out3, out4);
   input clk;
   input [31:0] in1, in2, in3, in4;
   output [31:0] out1, out2, out3, out4;

   parameter 	 A = 1;
   parameter 	 B = 1;
   parameter 	 C = 1;
   parameter 	 D = 1;

`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCRuZCQlFyQkJGx6AwtQmHbE9CEl2APS89h7ogkbSTpd58l
pfj69BzKIIEhGHFkNVd8BE0dce9PSQRchJzHl3JhfnvDryx1dXLJGuguJGla3PaHzjzZ82H03k3L
Q6Yy14L5I8XgD+TZh5CqeoqcEKt/eFERWZComjlmDwVkRNceq9ey/ED37JRmDfVNrDwzLURGC6Di
J/nYvWaWLfkvBKqmWxP3INOPzjimii6GajhyyA2pcVHZlMxnctgwd7MocSY/emCtmseD8MyL9z/F
W4ESaYxCvxLZKX+hEkxb5zFY7jU+kbV3USBkcEe9d3hEWn3pk6tiU15YfN451LoF2Mu1Yvs1XDRZ
FIXYXO+Lt8qZ1UvSQQPQRZ7EEVpqE3lBu8FkJrjq4wSfilPMIQ=>
`endprotected   
      
endmodule
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else

// based on test/langcov/Generate/gennamedblocks1.v
module protected_genblock_01(clk, in1, in2, in3, in4, out1, out2, out3, out4);
   input clk;
   input [31:0] in1, in2, in3, in4;
   output [31:0] out1, out2, out3, out4;

   parameter 	 A = 1;
   parameter 	 B = 1;
   parameter 	 C = 1;
   parameter 	 D = 1;

`protect   
   // named with nested named
   generate begin: HIDDEN_out1_block
      if (A) begin: HIDDEN_nested1_block
	 reg [31:0] HIDDEN_outreg;
	 always @(posedge clk)
	   HIDDEN_outreg <= in1;
         assign out1 = HIDDEN_outreg;
      end
      else begin
         assign out1 = 32'h0;
      end
   end
   endgenerate

   // named with nested unnamed
   generate begin: HIDDEN_out2_block
      if (B) begin
	 reg [31:0] HIDDEN_outreg;
	 always @(posedge clk)
	   HIDDEN_outreg <= in2;
         assign out2 = HIDDEN_outreg;
      end
      else begin
         assign out2 = 32'h0;
      end
   end
   endgenerate

   // unnamed with nested named
   generate begin
      if (C) begin: HIDDEN_nested1_block
	 reg [31:0] HIDDEN_outreg;
	 always @(posedge clk)
	   HIDDEN_outreg <= in3;
         assign out3 = HIDDEN_outreg;
      end
      else begin
         assign out3 = 32'h0;
      end
   end
   endgenerate

   // unnamed with nested unnamed
   generate begin
      if (D) begin
	 reg [31:0] HIDDEN_outreg;
	 always @(posedge clk)
	   HIDDEN_outreg <= in4;
         assign out4 = HIDDEN_outreg;
      end
      else begin
         assign out4 = 32'h0;
      end
   end
   endgenerate
`endprotect   
      
endmodule


`endif
