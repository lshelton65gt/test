// last mod: Tue Apr 25 15:48:46 2006
// filename: test/protected-source/protected_function_01.v
// Description:  This test contains a function with the name HIDDEN that is
// defined within a protected region, so the name HIDDEN should never appear in
// a generated file or message.

// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_function_01.v
// to create a protected_defines_01.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
module protected_function_01(clock, sel, in1, out);
   input clock;
   input sel;
   input [7:0] in1;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 77;
     end
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQG5CQkDqQkJAT6AzbXL3jvaSmX9zbm+Sh3dubZqU5vOPZ
pJzinBwk3FmZot2/HrqhnNpfxKHCccLAwNi7Xdu+WVxfw2Bg5OHhZSOWGZHR9s7R+RnSJCkF0TzS
NrE6+OH6HqWHxLS5pa2jHxvDsaPd0ZO4PjmQ9LvQXcIg1UIKCgdSlTHwpuKRkKfZvoY@
`endprotected   
      
   always @(posedge clock)
     begin: main
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQxZCQkNWQkJDd6Axb3N68J2Sj4F244eG95bo+ZKLaXllZ
ud+f5ZmcpD+i598+YFgmGFq7xqHCcXJcv73BIMVgYOTh4WVDWFyjRMG43l1BJHaSkI8vhcE;
`endprotected
     end
endmodule

// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else


module protected_function_01(clock, sel, in1, out);
   input clock;
   input sel;
   input [7:0] in1;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 77;
     end
`protect
   function HIDDEN;
      input [7:0] i1;
      input sel;

      begin
	 if (sel) 
	   HIDDEN = i1;
      end
   endfunction
`endprotect   
      
   always @(posedge clock)
     begin: main
`protect
	out = HIDDEN(in1, sel);
`endprotect
     end
endmodule
`endif
