// last mod: Fri May 12 11:02:33 2006
// filename: test/protected-source/protected_defines_01.v
// Description:  This test has some `defines within a protected region, some
// outside of a module and some inside

// (note this file contains both the protected and the unprotected version.)
// If you ever need to create a new protected version of this file use the command:
//   cbuild +define+CREATEPROTECTED +protect protected_defines_01.v
// to create a protected_defines_01.vp file, then copy the generated module into
// this file between the lines that start with the comment that contains the string
//  "the protected version"
//

`ifndef CREATEPROTECTED
/////////////////////////////////////////////////////////////////////////////////
// the protected version of this file goes between this line and a line like it below
`define VISIBLE_01 16'heeef
 `protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQyJCQkNiQkJDA6AwbWj+g3WFh2r5Z5aVc3RqcWZq+5Vsb
pLmk5KQdmN+7mWcf5Kdi29qaxaHCcdrY2d1bXNvFYGDk4eFlG6egxKCkw1/Y2dnZc8KQkPAOhtc>

`endprotected
module protected_defines_01(clock, in1, in2, in3, in4, out1);
   input clock;
   input [15:0] in1, in2, in3, in4;
   output [63:0] out1;
   reg [63:0] out1;
 `define VISIBLE_02 16'hfeee
 `protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQyZCQkNmQkJDB6AxbWV2kY1lZp+Ti4qaYXxqkJrhjXGHY
5Oabp+dlZr+52iZcp95cGKLjxKHCccLY2NndW1zbxWBg5OHhZRunoMKgpMNf2NvZ2XHCkJDUWYUz

`endprotected
   always @(posedge clock)
     begin: main
	out1 = {(in1 & `VISIBLE_01), (in2 & `VISIBLE_02),
`protected
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCQzpCQkMmQkJDG6AwjYJrYo+ViYru8YqWhHFhZ51/l2WSl
21nbpOUlnLyiXFrbpV5jWWDfxaHCccTAwHBydERYXKPGwMPYYGDk4eFlG6egRETBkBoJsBuJ+eKR
kOMzh+0;
`endprotected		
		};
     end
endmodule
// the protected version of this file goes between this line and a line like it above
/////////////////////////////////////////////////////////////////////////////////

`else

`define VISIBLE_01 16'heeef
 `protect
`define HIDDEN_01 16'hdddf
 `endprotect
module protected_defines_01(clock, in1, in2, in3, in4, out1);
   input clock;
   input [15:0] in1, in2, in3, in4;
   output [63:0] out1;
   reg [63:0] out1;
 `define VISIBLE_02 16'hfeee
 `protect
 `define HIDDEN_02 16'hfddd
 `endprotect
   always @(posedge clock)
     begin: main
	out1 = {(in1 & `VISIBLE_01), (in2 & `VISIBLE_02),
`protect	   
		(in3 & `HIDDEN_01), (in4 & `HIDDEN_02)
`endprotect		
		};
     end
endmodule
`endif
