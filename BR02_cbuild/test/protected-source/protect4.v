// This testcase makes sure we lack visibility to registers declared
// in protected regions, but retain the visibilty in equivalent wires

module top(a, b, c, out, vis1, expose_out);
  input a, b, c;
  output out, vis1, expose_out;

`protect
  // This is really a mux but no one outside should be able to
  // see any of the protected variables

  wire   protected1 = ~a;
  wire   protected2 = protected1 & b;
  wire   protected3 = a & c;
  assign out = protected2 | protected3;
`endprotect

  // although 'protected2' is declared in a tick-protect block,
  // vis1 is not, and so it should be visible.  Same with expose.vis2.
  assign vis1 = protected2;

  expose expose(expose_out, protected3);

endmodule

module expose(expose_out, vis2);
  output expose_out;
  input  vis2;

  assign expose_out = ~vis2;
endmodule

