// last mod: Mon Apr 17 14:04:41 2006
// filename: test/protected-source/error_in_syntax.v
// Description:  This test has a syntax error in the protected region, make sure
// that we do not print anything about the protected module in the syntax error msg


module error_in_syntax(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   mid i1 (clock, in1, in2, out1);
endmodule

`protect
module mid(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   
   always @(posedge clock)
     begin: main
       out1 = in1 & 1'bx in2;	// syntax error
     end

endmodule
`endprotect
