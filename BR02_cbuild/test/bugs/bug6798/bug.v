// last mod: Mon Dec 11 15:29:47 2006
// filename: test/bugs/bug6798/bug.v
// Description:  This test is a standalone version of the files from bug6798,
// when run where BUG is defined then Cheetah incorrectly reports that there is
// a size mismatch for port addr for both u1 and u2
// when run without defining BUG then you get no warning messages, but the
// $display shows that addr_width and addr_width2 are still 8.


module top (clk, cs1, cs2, wr1, wr2, a1, a2, di1, di2, do1, do2);
   input clk, cs1, cs2, wr1, wr2;
   input [31:0] di1;
   input [15:0] di2;
   input [7:0] 	a1;
   input [7:0] 	a2;
   output [31:0] do1;
   output [31:0] do2;

   RAM1 #(32, 512)  u1 (clk, cs1, wr1, a1, di1, do1);
   RAM2 #(16, 1024) u2 (clk, cs2, wr2, a2, di2, do2);
   
endmodule

module RAM1 (clk, cs_n, wr_n, addr, data_in, data_out);

   parameter data_width = 4;
   parameter depth = 8;

`ifdef BUG
`define addr_width ((depth>16)?((depth>64)?((depth>128)?8:7):((depth>32)?6:5)):((depth>4)?((depth>8)?4:3):((depth>2)?2:1)))
`else
   parameter addr_width=((depth>16)?((depth>64)?((depth>128)?8:7):((depth>32)?6:5)):((depth>4)?((depth>8)?4:3):((depth>2)?2:1)));
`endif

   input [data_width-1:0]  data_in;
`ifdef BUG
   input [`addr_width-1:0] addr;
`else
   input [addr_width-1:0] addr;
`endif
   input 		   wr_n;
   input 		   cs_n;
   input 		   clk;

   output [data_width-1:0] data_out;
   reg [data_width-1:0] data_out;
   reg [data_width-1:0] mem [0:depth-1];

`ifdef BUG   
   initial $display("RAM1, `addr_width: %d", `addr_width);
`else
   initial $display("RAM1, addr_width: %d", addr_width);
`endif

   always @(posedge clk)
     if (~cs_n)
       begin
	  if (~wr_n)
	    mem[addr] = data_in;
	  data_out = mem[addr];
       end
endmodule 

module RAM2 (clk, cs, wr, addr, data_in, data_out);

   parameter data_width = 8;
   parameter depth = 16;

`ifdef BUG
 `define addr_width2 ((depth>16)?((depth>64)?((depth>128)?8:7):((depth>32)?6:5)):((depth>4)?((depth>8)?4:3):((depth>2)?2:1)))
`else
   parameter addr_width2=((depth>16)?((depth>64)?((depth>128)?8:7):((depth>32)?6:5)):((depth>4)?((depth>8)?4:3):((depth>2)?2:1)));
   
`endif
   input [data_width-1:0]  data_in;
`ifdef BUG
   input [`addr_width2-1:0] addr;
`else
   input [addr_width2-1:0] addr;
`endif
   input 		   wr;
   input 		   cs;
   input 		   clk;

   output [data_width-1:0] data_out;
   reg [data_width-1:0] data_out;
   reg [data_width-1:0] mem [0:depth-1];

`ifdef BUG   
   initial $display("RAM2, `addr_width2: %d", `addr_width2);
`else
   initial $display("RAM2, addr_width2: %d", addr_width2);
`endif

   always @(posedge clk)
     if (cs)
       begin
	  if (wr)
	    mem[addr] = data_in;
	  data_out = mem[addr];
       end
endmodule 
