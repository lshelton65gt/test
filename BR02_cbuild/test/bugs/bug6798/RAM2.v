module RAM2 (clk, cs, wr, addr, data_in, data_out);

   parameter data_width = 8;
   parameter depth = 16;

`ifdef BUG
 `define addr_width ((depth>16)?((depth>64)?((depth>128)?8:7):((depth>32)?6:5)):((depth>4)?((depth>8)?4:3):((depth>2)?2:1)))
`else
   parameter addr_width=((depth>16)?((depth>64)?((depth>128)?8:7):((depth>32)?6:5)):((depth>4)?((depth>8)?4:3):((depth>2)?2:1)));
   
`endif
   input [data_width-1:0]  data_in;
`ifdef BUG
   input [`addr_width-1:0] addr;
`else
   input [addr_width-1:0] addr;
`endif
   input 		   wr;
   input 		   cs;
   input 		   clk;

   output [data_width-1:0] data_out;
   reg [data_width-1:0] data_out;
   reg [data_width-1:0] mem [0:depth-1];

   always @(posedge clk)
     if (cs)
       begin
	  if (wr)
	    mem[addr] = data_in;
	  data_out = mem[addr];
       end
endmodule 
