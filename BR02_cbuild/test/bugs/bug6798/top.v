module top (clk, cs1, cs2, wr1, wr2, a1, a2, di1, di2, do1, do2);
   input clk, cs1, cs2, wr1, wr2;
   input [31:0] di1;
   input [15:0] di2;
   input [8:0] 	a1;
   input [9:0] 	a2;
   output [31:0] do1;
   output [31:0] do2;

   RAM1 #(32, 512)  u1 (clk, cs1, wr1, a1, di1, do1);
   RAM2 #(16, 1024) u2 (clk, cs2, wr2, a2, di2, do2);
   
endmodule
