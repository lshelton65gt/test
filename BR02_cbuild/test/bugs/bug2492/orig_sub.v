module orig_sub(clk, i1, i2, o1, o2);
   input  clk;
   input  i1, i2;
   output o1, o2;

   reg 	  o1, o2;

   always @(posedge clk)
     o1 <= i1;

   always @(negedge clk)
     o2 <= i2;

endmodule // orig_sub
