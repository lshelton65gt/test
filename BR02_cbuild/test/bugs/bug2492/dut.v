module dut(clk, in1, in2, out1, out2);
   input clk;
   input in1, in2;
   output out1, out2;

   orig_sub s1(clk, in1, in2, out1, out2);
`ifdef CARBON_FIX
   new_sub s2();
`endif

endmodule // dut
