module new_sub(clk, i1, i2, o1, o2);
   input clk;
   input i1, i2;
   output o1, o2;

   reg 	  o1, o2;

   always @(posedge clk) begin
      o2 <= i2;
      o1 <= o2;
   end

endmodule // new_sub
