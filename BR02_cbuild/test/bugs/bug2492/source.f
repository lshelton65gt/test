-testdriver
-q
dut.v
// the original bug fixer appears to have thought that the use of
//  -y . and +libext+.v+ should have made the new_sub module visible to the
// parser, but that is not true.  The file that contains the substituted
// module must be compiled, the easiest way to do this is to put it on the command line

// however the use of -y does allow the parser to find orig_sub.v so it is left in place
-y . +libext+.v+
new_sub.v

-vlogTop dut

-directive cbuild.dir

-showParseMessages

// +define+CARBON_FIX+
