//
// Mindspeed design had a snippet of code like this where the
// assignment tmp=in was not vectorized.
//

module vectorize_bug( sel, in, out, fold_to_bit, fold_to_net, not_folded );
input sel;
input [63:0] in;
output [31:0] out;
output [9999999:9999999] fold_to_bit;
output [0:0] fold_to_net;
output [1:0] not_folded;
integer i;
wire [63:0] tmp;

assign out = ( sel ? tmp[63:32] : tmp[31:0] );

assign tmp[0] = in[0];
assign tmp[1] = in[1];
assign tmp[2] = in[2];
assign tmp[3] = in[3];
assign tmp[4] = in[4];
assign tmp[5] = in[5];
assign tmp[6] = in[6];
assign tmp[7] = in[7];
assign tmp[8] = in[8];
assign tmp[9] = in[9];

assign tmp[10] = in[10];
assign tmp[11] = in[11];
assign tmp[12] = in[12];
assign tmp[13] = in[13];
assign tmp[14] = in[14];
assign tmp[15] = in[15];
assign tmp[16] = in[16];
assign tmp[17] = in[17];
assign tmp[18] = in[18];
assign tmp[19] = in[19];

assign tmp[20] = in[20];
assign tmp[21] = in[21];
assign tmp[22] = in[22];
assign tmp[23] = in[23];
assign tmp[24] = in[24];
assign tmp[25] = in[25];
assign tmp[26] = in[26];
assign tmp[27] = in[27];
assign tmp[28] = in[28];
assign tmp[29] = in[29];

assign tmp[30] = in[30];
assign tmp[31] = in[31];

// Test some other LHS fold cases
assign fold_to_bit[0:0] = in[0];

assign fold_to_net[0:0] = in[0];

assign not_folded[1:0] = in[1:0];

endmodule
