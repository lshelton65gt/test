module bar1(a, b);
   input a;
   output b;

   `uselib file=foo1.v
   foo foo(.a(a), .b(b));
   `uselib

endmodule // bar1
