`uselib file=foo2.v
module bar2(x, y ,z);
   input x, y;
   output z;

   foo foo(.x(x), .y(y), .z(z));
endmodule // bar2
`uselib
