module foo(a, b);
   input a;
   output b;

   assign b = a;
endmodule // foo
