module top(a, b, x, y, z);
   input a, x, y;
   output b, z;

   bar1 bar1(.a(a), .b(b));
   bar2 bar2(.x(x), .y(y), .z(z));
endmodule // top
