module Top(clk, output_1);


   input clk;
   output output_1;

   wire [3:0] 	wire4;
   wire 	wire1;

   assign output_1 = wire1;

   Mid uMid0( .clk (clk),  .input_a_4 (4'b0),  .output_b_4 (wire4)                    );
   Mid uMid1( .clk (clk),  .input_a_4 (wire4),                     .output_a_1 (wire1));
   Mid uMid2( .clk (clk));
endmodule

module Mid(clk, undriven1, undriven32, input_a_4, unused1, output_b_4, output_a_1);

   input        clk;
   input 	undriven1;
   input [31:0] undriven32;
   input [3:0] 	input_a_4;
   output       unused1;
   output [3:0] output_b_4;
   output 	output_a_1;

   wire [3:0] 	A6boc4;
   wire [9:0] 	M6boc4;
   reg [9:0] 	C7boc4;
   reg [7:0] 	S7boc4;

   assign M6boc4[2] = 0;
   assign M6boc4[6] = (A6boc4 && (!(input_a_4[3] && output_b_4[2])));
   assign output_b_4[3] = undriven32[0];
   assign output_a_1 = C7boc4[2];

   E8boc4 U8boc4( .K9boc4 (undriven32),  .S9boc4 (A6boc4),  .unused1 (unused1));

   always @(posedge clk) begin
      C7boc4 <= (undriven1 ? C7boc4 : M6boc4);
   end
endmodule

module E8boc4(K9boc4, S9boc4, unused1);

   input [31:0] K9boc4;
   output [3:0] S9boc4;
   output       unused1;

   wire 	Zaboc4;
   wire 	Lbboc4;
   wire 	Bcboc4;
   wire 	Ocboc4;
   wire 	Ddboc4;
   wire 	Sdboc4;
   wire 	Feboc4;
   wire 	Seboc4;
   wire 	Ffboc4;
   wire 	Sfboc4;
   wire 	Fgboc4;
   wire 	Sgboc4;
   wire 	Fhboc4;
   wire 	Shboc4;
   wire 	Giboc4;
   wire 	Viboc4;
   wire 	Jjboc4;
   wire 	Xjboc4;
   wire 	Lkboc4;
   wire 	Zkboc4;
   wire 	Nlboc4;
   wire 	Bmboc4;
   wire 	Pmboc4;
   wire 	Dnboc4;
   wire 	Rnboc4;
   wire 	Foboc4;
   wire 	Toboc4;
   wire 	Ipboc4;
   wire 	Xpboc4;
   wire 	Mqboc4;

   assign Ocboc4 = (((K9boc4[28] && (!K9boc4[27])) && K9boc4[26]) && K9boc4[25]);
   assign Ddboc4 = ((!K9boc4[25]) && K9boc4[24]);
   assign Sdboc4 = (((!K9boc4[23]) && (!K9boc4[22])) && (!K9boc4[21]));
   assign Feboc4 = ((!K9boc4[23]) && (!K9boc4[22]));
   assign Seboc4 = (((!K9boc4[23]) && K9boc4[22]) && K9boc4[21]);
   assign Ffboc4 = ((!K9boc4[23]) && K9boc4[21]);
   assign Sfboc4 = (K9boc4[23] && (!K9boc4[22]));
   assign Fgboc4 = ((K9boc4[23] && K9boc4[22]) && (!K9boc4[21]));
   assign Sgboc4 = (K9boc4[23] && K9boc4[22]);
   assign Fhboc4 = (K9boc4[23] && (!K9boc4[21]));
   assign Shboc4 = (K9boc4[30] && K9boc4[29]);
   assign Giboc4 = ((!K9boc4[20]) && K9boc4[16]);
   assign Viboc4 = (((K9boc4[15]) && (!K9boc4[14])) && (K9boc4[12]));
   assign Jjboc4 = ((!K9boc4[15]) && (!K9boc4[14]));
   assign Xjboc4 = ((((K9boc4[15]) && K9boc4[14]) && K9boc4[13]) && K9boc4[12]);
   assign Lkboc4 = (((!K9boc4[15]) && K9boc4[14]) && K9boc4[13]);
   assign Zkboc4 = (((!K9boc4[15]) && K9boc4[14]) );
   assign Bmboc4 = (((K9boc4[15] && (!K9boc4[14])))) ;
   assign Pmboc4 = (((K9boc4[15] && K9boc4[14])) );

   assign Dnboc4 = ((!K9boc4[11]) && (K9boc4[9]));
   assign Rnboc4 = ((K9boc4[11] && K9boc4[10]));
   assign Foboc4 = ((K9boc4[11] && K9boc4[9]) );

   assign Toboc4 = ((K9boc4[7]) && (K9boc4[4]));
   assign Ipboc4 = ((K9boc4[7]) && (K9boc4[3]));
   assign Xpboc4 = ((K9boc4[7]) && (K9boc4[5]));
   assign Mqboc4 = (K9boc4[6] && K9boc4[5]);
   assign S9boc4[2] = (((((Shboc4 && Fhboc4) && (!K9boc4[15])) || (((Shboc4 && Sfboc4) && Nlboc4) && (!K9boc4[10]))) || ((Shboc4 && Sfboc4) && Zkboc4)) || ((Shboc4 && Sfboc4) && Lkboc4));
   assign S9boc4[1] = (Seboc4 && Viboc4);
   assign unused1 = ((((((((((((((Lbboc4 && Zaboc4) && Bcboc4) || (Ffboc4 && Xjboc4)) || Giboc4) || (Sgboc4 && Bmboc4)) || Fgboc4) || Dnboc4) || ((Feboc4 && Foboc4) && Mqboc4)) || (Ddboc4 && Ipboc4)) || Toboc4) || (Rnboc4 && Xpboc4)) || (Sdboc4 && Jjboc4)) || Pmboc4) || Ocboc4);
endmodule
