// TOP LEVEL MODULE

module test(a,b,c, d, clk,sum, lbyte, cd);

   input a;
   input b;
   input c;
   input d;
   input clk;
   output sum;
   output cd;
   output [7:0] lbyte;

   wire         c;
   wire         d;
   wire         cd;         
   wire [31:0] a,b;
   wire        clk;
   wire [31:0] sum;
   wire [31:0] sum_internal;
   
   wire [31:0] out1;
   wire [15:0] out11;
   wire [15:0] out12;
   wire [31:0] out2;
   wire        cd_internal;
   
   assign cd_internal = c & d;
   assign cd = cd_internal;
   
   // Since out11 is forced, the top
   // 16 bits of the mvp_reg output
   // will be coerced to inout, and
   // the port will be split.
   mvp_reg #(32) u11 (a, clk, {out11, out12});
   assign out1 = {out12, out11};
   mvp_reg #(32) u2 (b, clk, out2);

   adder u3 (out1, out2, clk, sum_internal);

   mvp_reg #(8) u4 (sum_internal[7:0], clk, lbyte);

   assign sum = sum_internal;

endmodule

// PARAMETERIZABLE REGISTER

module mvp_reg(din, clk, dout);
   parameter WIDTH = 32;
   
   input din;
   input clk;
   output dout;
   wire [(WIDTH-1):0] din;
   wire        clk;
   reg [(WIDTH-1):0]  dout;

   always @(posedge clk)
        dout <= din;

endmodule
        
// SIMPLE ADDER

module adder(a,b,clk,sum);
   input wire [31:0] a;
   input wire [31:0] b;
   input wire        clk;
   output reg [31:0] sum;

   always @(posedge clk)
     sum = a + b;
endmodule
