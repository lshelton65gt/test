module dff_first(
	in1p, in2p, outp
)
	(* const integer foreign = "VHDL(event) WORK.DFF;first"; *); // ";" is not a valid separator
input in1p, in2p;
output outp;

endmodule
