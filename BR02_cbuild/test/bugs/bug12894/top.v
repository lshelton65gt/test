module top(a1x,a2x,a3x,b1x,b2x,b3x,c1x,c2x,c3x);
   input a1x, a2x,a3x;
   input b1x, b2x, b3x;
   output c1x, c2x, c3x;
   
   dff_first i1 (.in1p(a1x), .in2p(b1x), .outp(c1x));
 
   dff_second i2 (.in1p(a2x), .in2p(b2x), .outp(c2x));
 
   dff_third i3 (.in1p(a3x), .in2p(b3x), .outp(c3x));
  
endmodule
