module bug(clk,sel,a,b,c,d,e,f,out);
   input clk;
   input sel;
   input a,b,c,d,e,f;
   output [2:0] out;
   reg [2:0] out;

   always @(posedge clk) begin
      out = { (sel ? a : b),
	      (sel ? c : d),
	      (sel ? e : f) };
   end
endmodule
