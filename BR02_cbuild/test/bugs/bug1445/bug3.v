module bug(clk,sel,a,b,c,d,e,f,g,h,i,out);
   input clk;
   input sel;
   input a,b,c,d,e,f,g,h,i;
   output [5:0] out;
   reg [5:0] out;

   always @(posedge clk) begin
      out = { (sel ? a : b), g,
	      (sel ? c : d), h,
	      (sel ? e : f), i };
   end
endmodule
