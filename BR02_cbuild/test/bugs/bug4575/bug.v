// signed vs. unsigned constructors (fail10,17,20,5,8)

module top(i,o);
   input [7:0] i;
   output signed o;

   wire   signed [116:1] glyt = i << 28;

   assign      o = 112'h6f0ffd08 >= glyt[104:25];
endmodule
