module arb_bug(pci_clk, x, Parking);
input pci_clk;
input x;
output Parking;
reg Parking;

   task set_parking;
      input enable; 
      begin   
         @(posedge pci_clk);
         Parking <= enable; 
         @(posedge pci_clk);
      end     
   endtask 

   always @(posedge pci_clk)
      set_parking(x);

endmodule
