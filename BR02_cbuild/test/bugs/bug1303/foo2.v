module foo(x, y, z);
   input x, y;
   output z;

   assign z = y & x;
endmodule // foo
