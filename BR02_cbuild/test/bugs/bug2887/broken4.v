module top(clk,in,out);
   input clk;
   input in;
   output out;

   middle m(clk,in,out);
endmodule

module middle(clk,in,out);
   input clk;
   input in;
   output out;
   reg    out;
   broken b0();

   always @(posedge clk) begin
      middle.b0.check_stuff(in,out);
      middle.out <= in ^ middle.b0.r;
   end
endmodule

module broken;

   reg r;
   
   task check_stuff;
      input in;
      input out;
      begin
      r = 1'b0;
      if (in==0) begin
	 broken.error(in,out);
         broken.r = 1'b1;
      end
      end
   endtask
   
   task error;
      input in;
      input out;
      $write("in=%h out=%h\n",in,out);
   endtask
   
endmodule
