module top(clk,in,out);
   input clk;
   input in;
   output out;

   broken broken(clk,in,out);
endmodule

module broken(clk,in,out);
   input clk;
   input in;
   output out;
   reg 	  out;
   
   task check_stuff;
      if (in==0) begin
	 broken.error(8,in,out);
      end
   endtask
   
   task error;
      input [31:0] ctl;
      input in;
      output out;
      out = ~in;
   endtask
   
   always @(posedge clk) begin
      check_stuff;
      out = ~in;
   end
endmodule
