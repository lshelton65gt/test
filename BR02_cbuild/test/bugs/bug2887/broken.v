module top(clk,in,out);
   input clk;
   input in;
   output out;

   broken b0(clk,in,out);
endmodule

module broken(clk,in,out);
   input clk;
   input in;
   output out;
   reg 	  out;
   
   task check_stuff;
      input in;
      input out;
      if (in==0) begin
	 broken.error(in,out);
      end
   endtask
   
   task error;
      input in;
      input out;
      $write("in=%h out=%h\n",in,out);
   endtask
   
   always @(posedge clk) begin
      check_stuff(in,out);
      broken.out <= in;
   end
endmodule
