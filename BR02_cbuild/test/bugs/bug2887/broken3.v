module top(clk,in,out);
   input clk;
   input in;
   output out;

   broken broken(clk,in,out);
endmodule

module broken(clk,in,out);
   input clk;
   input in;
   output out;
   reg 	  out;
   
   task error;
      input [31:0] ctl;
      input in;
      output out;
      out = ~in;
   endtask
   
   always @(posedge clk) begin
      broken.error(8,in,out);
      if (in) begin
	 out = ~in;
      end
   end
endmodule
