// BUG5941 - case involving variable position rhs and possible shift amount...
module top(d1,ck,q);
   input [20:0] d1;
   input [3:0]	 ck;
   output [20:0] q;

   foo f0 ( .in1(d1[0 +: 20]), .idx(ck), .o(q[0 +: 20]));
endmodule // top

module foo(in1, idx, o);
   input [19:0] in1;
   input [3:0] 	idx;
   output [19:0] o;

   reg [19:0] 	 o_r;
   initial o_r = 0;
   always @(in1) 
     o_r [$signed(idx) +: 4] = in1[idx +: 4] ;
   assign o = o_r;
endmodule












