library ieee;
use ieee.std_logic_1164.all;

entity test_vhdl is

  port (
    tclk   : in  std_logic;
    trst_l : in  std_logic;
    ta     : in  std_logic_vector(7 downto 0);
    tb     : in  std_logic_vector(7 downto 0);
    ty     : out std_logic_vector(7 downto 0));

end test_vhdl;

architecture rtl of test_vhdl is

  component test_ver
    port (
      clk   : in  std_logic;
      rst_l : in  std_logic;
      a     : in  std_logic_vector(7 downto 0);
      b     : in  std_logic_vector(7 downto 0);
      c     : in  std_logic_vector(7 downto 0);
      d     : in  std_logic_vector(7 downto 0);
      y     : out std_logic_vector(7 downto 0));
  end component;

begin  -- rtl

  test_ver_inst: test_ver port map (
    clk   => tclk,
    rst_l => trst_l,
    a     => ta,
    b     => tb,
    c     => (others => '0'),           -- produces undriven net warning
    d     => X"00",                     -- doesn't produce the undriven net warning
    y     => ty);


end rtl;
