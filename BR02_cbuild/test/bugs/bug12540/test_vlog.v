module test_ver(/*AUTOARG*/
   // Outputs
   y,
   // Inputs
   clk, rst_l, a, b, c, d
   );

   input clk;
   input rst_l;

   input [7:0] a;
   input [7:0] b;
   input [7:0] c;
   input [7:0] d;

   output [7:0] y;

   /*AUTOREG*/
   // Beginning of automatic regs (for this module's undeclared outputs)
   reg [7:0]            y;
   // End of automatics

   always @(posedge clk or negedge rst_l)
     if (~rst_l)
       begin
          y <= 0;
       end
     else
       begin
          y <= a & b | c | d;
       end

endmodule // test_ver
