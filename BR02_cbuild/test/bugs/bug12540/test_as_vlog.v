module test_as_vlog(clk, rst_l, a, b, y);
   input clk;
   input rst_l;
   input [7:0] a;
   input [7:0] b;
   output [7:0] y;

   test_ver test_ver_inst(.clk(clk), .rst_l(rst_l), .a(a), .b(b), .c({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}), .y(y));

endmodule
