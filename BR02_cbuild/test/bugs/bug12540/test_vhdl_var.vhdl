library ieee;
use ieee.std_logic_1164.all;

entity test_vhdl_var is

  port (
    clk   : in  std_logic;
    rst_l : in  std_logic;
    a     : in  std_logic_vector(7 downto 0);
    b     : in  std_logic_vector(7 downto 0);
    v1    : in  std_logic_vector(3 downto 0);
    v2    : in  std_logic_vector(3 downto 0);
    y     : out std_logic_vector(7 downto 0));

end test_vhdl_var;

architecture rtl of test_vhdl_var is

  component test_ver
    port (
      clk   : in  std_logic;
      rst_l : in  std_logic;
      a     : in  std_logic_vector(7 downto 0);
      b     : in  std_logic_vector(7 downto 0);
      c     : in  std_logic_vector(7 downto 0);
      y     : out std_logic_vector(7 downto 0));
  end component;

begin  -- rtl

  test_ver_inst: test_ver port map (
    clk   => clk,
    rst_l => rst_l,
    a     => a,
    b     => b,
    c     => (v1(3), v1(2), v1(1), v1(0), v2(3), v2(2), v2(1), v2(0)),
    y     => y);


end rtl;
