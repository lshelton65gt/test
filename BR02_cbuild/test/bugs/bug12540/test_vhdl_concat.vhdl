library ieee;
use ieee.std_logic_1164.all;

entity test_vhdl_concat is

  port (
    clk   : in  std_logic;
    rst_l : in  std_logic;
    a     : in  std_logic_vector(7 downto 0);
    b     : in  std_logic_vector(7 downto 0);
    y     : out std_logic_vector(7 downto 0));

end test_vhdl_concat;

architecture rtl of test_vhdl_concat is

  component test_ver
    port (
      clk   : in  std_logic;
      rst_l : in  std_logic;
      a     : in  std_logic_vector(7 downto 0);
      b     : in  std_logic_vector(7 downto 0);
      c     : in  std_logic_vector(7 downto 0);
      d     : in  std_logic_vector(7 downto 0);      
      y     : out std_logic_vector(7 downto 0));
  end component;

begin  -- rtl

  test_ver_inst: test_ver port map (
    clk   => clk,
    rst_l => rst_l,
    a     => a,
    b     => b,
    c     => ('0', '0', '0', '0', '0', '0', '0', '0'),  -- produces undriven net warning
    d     => X"00",                                     -- this gets passed to verilog module correctly
    y     => y);


end rtl;
