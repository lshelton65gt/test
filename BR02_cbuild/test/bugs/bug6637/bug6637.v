// The fact that default_nettype has been set to none
// should generate all kind of messages from Cheetah to the
// effect that clk and in have to type defined (they should be regs)
// However, using MVV-2006.1.3.e produces nothing from Cheetah.
//
`default_nettype none

module bug6637 (
	   input clk,
	   input in,
	   output reg out
	   );

   always @(posedge clk)
     out <= in;
   
endmodule
