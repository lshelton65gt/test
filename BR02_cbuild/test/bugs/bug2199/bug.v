module top(o1,o2,i1,i2);
  output o1,o2;
  input i1,i2;
  reg o1;
  always @(posedge 1'b0)
    o1 = i1;
  sub sub(o2,i2);
endmodule

module sub(o,i);
  output o;
  input i;
  reg o;
  always @(posedge 1'b0)
    o = i;
endmodule
