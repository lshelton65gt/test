module foo(in, out);
   input in;
   output out;
   
   missing_module x(in, out);
endmodule // foo

module bar(in, out);
   input in;
   output out;

   assign out = in;
endmodule // bar
