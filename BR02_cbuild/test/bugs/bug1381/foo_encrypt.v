module foo(in, out);
   input in;
   output out;

   assign out = in;
endmodule // foo
