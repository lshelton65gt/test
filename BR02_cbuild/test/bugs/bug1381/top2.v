module top(in1, in2, out1, out2);
   input in1, in2;
   output out1, out2;

`uselib file=libfoo_cem.v
   foo foo(.in(in1), .out(out1));
`uselib
   
   bar bar(.in(in2), .out(out2));

endmodule // top
