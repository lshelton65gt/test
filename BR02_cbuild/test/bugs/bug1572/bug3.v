module top(clk,sel,cond1,cond2,o);
   input clk;
   input [4:0] sel;
   input [1:0] cond1;
   input [1:0] cond2;
   output [1:0] o;
   reg [1:0] 	o;
   reg [1:0] 	c;

   always @(sel or cond1 or cond2) begin
      casex(sel)
	// intentionally over-sized conditions.
	{cond1,cond2,cond1}    : c = 2'b01;
	{2'bx,cond1,2'bx}      : c = 2'b10;
	{{{{{{{1'bx}}}}}},cond1,cond2,1'bx}: c = 2'b11;
	default                : c = 2'b00;
      endcase
   end
   
   initial o = 0;

   always @(posedge clk) begin
      o <= c;
   end
endmodule
