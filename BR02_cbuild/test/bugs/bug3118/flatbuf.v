module top(out, a, b);
  output out;
  input  a, b;
  // carbon flattenModule
  mid mid(a, b);
  assign out = mid.sub.out ^ mid.out;
endmodule

module mid(a, b);
  input a, b;
  wire out = ~(a | mid.sub.out);
  sub sub(b);
endmodule

module sub(b);
  input b;
  wire  out;
  assign mid.sub.out = ~b;
endmodule
