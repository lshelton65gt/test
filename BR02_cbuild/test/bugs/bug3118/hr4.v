module bench(varen,varnum,out);
   input varen;
   input [1:0] varnum;
   output      out;
   reg 	       out;
   esim_pli pli();
   vinteractive vinteractive();
   updater updater();
endmodule

module esim_pli();
   // carbon disallowFlattening
   reg _vi_activity;
   reg _got_done;
   initial _vi_activity = 0;
   initial _got_done = 0;
endmodule

module vinteractive();
   task vi_vars;
      input [1:0] _varnum;
      input _value;
      begin
	 casex (_varnum)
	   {1'bx,1'b1}: bench.pli._vi_activity = _value;
	   default:     bench.pli._got_done = _value;
	 endcase
      end
   endtask

   reg loop;
   always @(bench.pli._vi_activity) begin
      loop = ~pli._vi_activity;
      if (bench.varen)  begin
	vi_vars(bench.varnum,loop);
      end
   end
endmodule

module updater();
   always @(bench.pli._vi_activity) begin
      bench.out = bench.pli._vi_activity;
   end
endmodule
