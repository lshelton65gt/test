module utxshell2(clk1,clk2,clkout, addr1, addr2, addrout, data, out);
   input clk1;
   input clk2;
   input clkout;
   input [7:0] addr1;
   input [7:0] addr2;
   input [7:0] addrout;
   input [5:0] data;
   output [5:0] out;
   reg [5:0] out;

   task t1;
     utx.liu.poll_select.schedule_list.mem[addr1] = data;
   endtask
   
   always @(posedge clk1)
     t1;

   utx2 utx();

endmodule

module utx2();
   utx_liu2 liu();
endmodule

module utx_liu2();
   utx_poll_select poll_select();

   task t2;
     liu.poll_select.schedule_list.mem[utxshell2.addr2] = utxshell2.data;
   endtask
   
   always @(posedge utxshell2.clk2)
     t2;
endmodule

module utx_poll_select();
   rf_utx_schedule schedule_list();
endmodule

module rf_utx_schedule();
   
   reg [5:0] mem [0:255];

   task tout;
     utxshell2.out = mem[utxshell2.addrout];
   endtask
   
   always @(posedge utxshell2.clkout)
     tout;
endmodule
