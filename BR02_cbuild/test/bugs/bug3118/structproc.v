module bench(clk,done);
   input clk;
   input done;
   task finish;
      $finish;
   endtask
   cmds_top cmds_top();
endmodule

module cmds_top();
   wire done = bench.done;
   always @(bench.clk)
     finish;
endmodule
