module bench(clkin,done,data,out,num_clks);
   input clkin;
   input done;
   input data;
   output out;
   input [31:0] num_clks;
   reg 		 out;

   board board();
   esim_pli pli();
   cmds_top cmds_top();
endmodule

module esim_pli();
   // carbon flattenModule
   always @(negedge bench.board.c0.sys_clk) begin
      $server_poll();
   end
endmodule

module cmds_top();
   // carbon flattenModule
   assign bench.board.c0.sys_clk = bench.clkin;
   always @(posedge bench.board.c0.sys_clk)
     bench.out = bench.data;
endmodule

module board();
   // carbon disallowFlattening
   mezz c0();
endmodule

module mezz();
   // carbon disallowFlattening
   wire sys_clk;
endmodule
