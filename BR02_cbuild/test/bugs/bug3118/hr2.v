module bench(clkin,done,data,out,num_clks);
   input clkin;
   input done;
   input data;
   output out;
   output [31:0] num_clks;
   reg 		 out;
   reg [31:0] 	 num_clks;
   board board();
   cmds_top cmds_top();

   assign bench.board.c0.sys_clk = clkin;
endmodule

module cmds_top();
   always @(posedge board.c0.sys_clk)
     bench.out = bench.data;
endmodule

module board();
   // carbon disallowFlattening
   mezz c0();
endmodule

module mezz();
   // carbon flattenModule
   wire sys_clk;
endmodule
