`timescale 1ps/1ps
// `define DMA_PRINT_ENABLED
module top (

// Clock and reset
	HCLK,             
	HRESETn,          
                       
// AHB master1 signals
	AHB_master1_HGRANT,
	AHB_master1_HREADYIN,    
	AHB_master1_HRESP,
	AHB_master1_HRDATA,    
	AHB_master1_HBUSREQ,    
	AHB_master1_HLOCK,     
	AHB_master1_HTRANS,    
	AHB_master1_HADDR,    
	AHB_master1_HSIZE,   
	AHB_master1_HBURST,  
	AHB_master1_HPROT,     
	AHB_master1_HWRITE,    
	AHB_master1_HWDATA,  
    
// AHB master2 signals                      
	AHB_master2_HGRANT,            
	AHB_master2_HREADYIN,                
	AHB_master2_HRESP,                   
	AHB_master2_HRDATA,  
	AHB_master2_HBUSREQ,         
	AHB_master2_HLOCK,                
	AHB_master2_HTRANS,               
	AHB_master2_HADDR,                    
	AHB_master2_HSIZE,                 
	AHB_master2_HBURST,                  
	AHB_master2_HPROT,                   
	AHB_master2_HWRITE,                  
	AHB_master2_HWDATA, 
	           
// AHB slave signals
	AHB_slave_HSEL,         
	AHB_slave_HWRITE,           
	AHB_slave_HTRANS,           
	AHB_slave_HADDR,            
	AHB_slave_HSIZE,           
	AHB_slave_HREADYIN,         
	AHB_slave_HWDATA,
	AHB_slave_HREADYOUT,        
	AHB_slave_HRESP,            
	AHB_slave_HRDATA, 
	                
// DMA request signals
	DMACBREQ,         
	DMACLBREQ,        
	DMACSREQ,         
	DMACLSREQ,
          
// Scan related signals
	SCANINHCLK,       
	SCANENABLE,      

// DMA response signals
	DMACCLR,          
	DMACTC,  
            
// DMA interrupt request signals
	DMACINTERR,       
	DMACINTTC,        
	DMACINTR, 
           
// Scan related signals
	SCANOUTHCLK    
);



// Clock and reset
input		HCLK;             
input		HRESETn;          
                       
// AHB master1 signals
input		AHB_master1_HGRANT;
input		AHB_master1_HREADYIN;    
input[1:0]	AHB_master1_HRESP;
input[31:0]	AHB_master1_HRDATA;    
output		AHB_master1_HBUSREQ;    
output		AHB_master1_HLOCK;     
output[1:0]	AHB_master1_HTRANS;    
output[31:0]	AHB_master1_HADDR;    
output[2:0]	AHB_master1_HSIZE;   
output[2:0]	AHB_master1_HBURST;  
output[3:0]	AHB_master1_HPROT;     
output		AHB_master1_HWRITE;    
output[31:0]	AHB_master1_HWDATA;  
    
// AHB master2 signals                      
input		AHB_master2_HGRANT;            
input		AHB_master2_HREADYIN;                
input[1:0]	AHB_master2_HRESP;                   
input[31:0]	AHB_master2_HRDATA;  
output		AHB_master2_HBUSREQ;         
output		AHB_master2_HLOCK;                
output[1:0]	AHB_master2_HTRANS;               
output[31:0]	AHB_master2_HADDR;                    
output[2:0]	AHB_master2_HSIZE;                 
output[2:0]	AHB_master2_HBURST;                  
output[3:0]	AHB_master2_HPROT;                   
output		AHB_master2_HWRITE;                  
output[31:0]	AHB_master2_HWDATA;             

// AHB slave signals
input		AHB_slave_HSEL;         
input		AHB_slave_HWRITE;           
input[1:0]	AHB_slave_HTRANS;           
input[9:0]	AHB_slave_HADDR;            
input[2:0]	AHB_slave_HSIZE;           
input		AHB_slave_HREADYIN;         
input[31:0]	AHB_slave_HWDATA;
output		AHB_slave_HREADYOUT;        
output[1:0]	AHB_slave_HRESP;            
output[31:0]	AHB_slave_HRDATA;  
               
// DMA request signals
input[15:0]	DMACBREQ;         
input[15:0]	DMACLBREQ;        
input[15:0]	DMACSREQ;         
input[15:0]	DMACLSREQ;
          
// Scan related signals
input		SCANINHCLK;       
input		SCANENABLE;      

// DMA response signals
output [15:0]	DMACCLR;          
output [15:0]	DMACTC;  
            
// DMA interrupt request signals
output		DMACINTERR;       
output		DMACINTTC;        
output		DMACINTR; 
           
// Scan related signals
output		SCANOUTHCLK;    

reg		AHB_master1_HBUSREQ;    
reg		AHB_master1_HLOCK;     
reg[1:0]	AHB_master1_HTRANS;    
reg[31:0]	AHB_master1_HADDR;    
reg[2:0]	AHB_master1_HSIZE;   
reg[2:0]	AHB_master1_HBURST;  
reg[3:0]	AHB_master1_HPROT;     
reg		AHB_master1_HWRITE;    
reg[31:0]	AHB_master1_HWDATA; 
 
reg		AHB_master2_HBUSREQ;         
reg		AHB_master2_HLOCK;                
reg[1:0]	AHB_master2_HTRANS;               
reg[31:0]	AHB_master2_HADDR;                    
reg[2:0]	AHB_master2_HSIZE;                 
reg[2:0]	AHB_master2_HBURST;                  
reg[3:0]	AHB_master2_HPROT;                   
reg		AHB_master2_HWRITE;                  
reg[31:0]	AHB_master2_HWDATA;
	            
reg		AHB_slave_HREADYOUT;        
reg[1:0]	AHB_slave_HRESP;            
reg[31:0]	AHB_slave_HRDATA; 
 
reg [15:0]	DMACCLR;          
reg [15:0]	DMACTC;  
reg		DMACINTERR;       
reg		DMACINTTC; 
reg		SCANOUTHCLK; 
   
reg [31:0]	DMAChanInt_Enable;
reg[31:0 ]	DMA_Enable;
reg [31:0]	DMAChan_Enable;
reg[31:0] 	DMAChaninteger_Enable;
reg[31:0] 	ChanConfig;
reg[31:0] 	ChanControl;
reg[31:0] 	SourceAddress;
reg[31:0] 	DestAddress;
reg[31:0] 	SourceMaster;
reg[31:0] 	DestMaster;
reg[31:0] 	SourceInc;
reg[31:0] 	DestInc;
reg[31:0]  	Data1;
reg[31:0]  	Data2;
reg[31:0] 	TransferCount;
reg 		ReadAllowed;
reg 		WriteAllowed;
reg   [3:0]  	RegHTRANS;
reg   [9:0] 	RegHADDR;
reg		RegHWRITE;   
reg		RegHSEL; 
reg		RegHSIZE; 
reg 		DMACINTR;
reg[2:0]	state;    
reg		RegAHB_master2_HGRANT;
reg		RegAHB_master2_HREADYIN;
wire		sigAHB_master2_HGRANT;

initial
  begin
  ReadAllowed = 0;
  WriteAllowed = 0;
  DMACINTR = 0;
end
always @ (posedge HCLK or negedge HRESETn)
begin :bus_respond

if (!HRESETn)
      begin
        AHB_slave_HREADYOUT <= 1'b1 ;
        AHB_slave_HRESP     <= 2'b00 ;
      end
    else
      begin
        AHB_slave_HREADYOUT <= 1'b1;
        AHB_slave_HRESP     <= 2'b00;
      end
end

always @ (posedge HCLK or negedge HRESETn)
begin :inter_select
`ifdef BUG1
`else
   if (~HRESETn)
     RegHSEL <= 1'b0;
   else
`endif
if (AHB_slave_HREADYIN)
begin
    RegHSEL <= AHB_slave_HSEL;
end
end

always @ (posedge HCLK or negedge HRESETn)
begin :bus_buffer
  if (!HRESETn)
      begin
        RegHTRANS <= 0;
	RegHADDR <= 0;
	RegHWRITE <=  0; 
	RegHSIZE <=  0; 
      end
  else
      begin
         if (AHB_slave_HREADYIN & AHB_slave_HSEL)
            begin
              	RegHTRANS <= AHB_slave_HTRANS;
		RegHADDR <= AHB_slave_HADDR;
		RegHWRITE <=  AHB_slave_HWRITE;     
		RegHSIZE <=  AHB_slave_HSIZE;  
            end
       end
   end     
     
always @ (RegHSEL or RegHWRITE or AHB_slave_HWDATA or RegHADDR)
begin :bus_write

  if(RegHSEL & RegHWRITE)
   begin

`ifdef DMA_PRINT_ENABLED
    $display("DMA AHB slave port: Write: %x = %x\n", RegHADDR, AHB_slave_HWDATA);
`endif

  	case(RegHADDR)
	10'h0:
	    begin
    		// DMA Config 
      		DMA_Enable =  (AHB_slave_HWDATA & 32'h01);

`ifdef DMA_PRINT_ENABLED
    $display("DMA config: DMA Enabled : %x \n",DMA_Enable);
`endif
      		if ( (DMAChan_Enable == 1) && (DMA_Enable == 1) )
      		begin
        		if ( TransferCount ) 
        		begin
          			ReadAllowed = 1;
          			WriteAllowed = 0;            
	  			if ( SourceMaster == 0 )
  	  				begin
           				 AHB_master1_HBUSREQ = 1;
`ifdef DMA_PRINT_ENABLED               
      $display("DMA config: DMA just enabled -> request bus for master1" );
`endif
          				end
	  			else
	  				begin
            				AHB_master2_HBUSREQ = 1; 
`ifdef DMA_PRINT_ENABLED
       $display("DMA config: DMA just enabled -> request bus for master2");
`endif
          				end
        		end
      		end 
      		else
      			begin
        		AHB_master1_HBUSREQ = 0;
        		AHB_master2_HBUSREQ = 0;
`ifdef DMA_PRINT_ENABLED
    $display("DMA config: DMA Disabled" );
`endif
        		ReadAllowed = 0;
        		WriteAllowed = 0;              
      			end
		end		
    	10'h1:
    	begin
    
      		DMACINTR <= 0;
`ifdef DMA_PRINT_ENABLED
   $display("DMA config: DMA Interrupt Acknowledged" );
`endif
    	end

    	10'h40:
    	begin
     	 // Channel 0 Source Address
      		SourceAddress = AHB_slave_HWDATA;
	
`ifdef DMA_PRINT_ENABLED
 $display("DMA config: Source Address: %x \n", SourceAddress);
`endif
    	end

    	10'h41:
    	begin
      	// Channel 0 Destination Address
     	 	DestAddress = AHB_slave_HWDATA;
`ifdef DMA_PRINT_ENABLED
$display("DMA config: Destination Address: %x \n", DestAddress);
`endif
    	end

    	10'h42:
    	begin
      	// Channel 0 Control Register
      		ChanControl = AHB_slave_HWDATA;
     		SourceInc = (ChanControl & 32'h01);
      		DestInc = (ChanControl & 32'h02) >> 1;
      		SourceMaster = (ChanControl & 32'h04) >> 2;
      		DestMaster = (ChanControl & 32'h08) >> 3;
      		TransferCount = (ChanControl & 32'h00fff000) >> 12;
`ifdef DMA_PRINT_ENABLED
     $display("DMA config: Source Increment: %x \n" ,SourceInc );
     $display("DMA config: Destination Increment: %x \n" ,DestInc );
     $display("DMA config: Master for the source: %x \n",SourceMaster );
     $display("DMA config: Master for the destination: %x \n" , DestMaster );
     $display("DMA config: Number of transfers: %x \n", TransferCount );
`endif
     	end

    	10'h43:
    	begin
      	// Channel 0 Configuration Register
      		ChanConfig = AHB_slave_HWDATA;
      		DMAChan_Enable = (ChanConfig & 32'h01);
      		DMAChanInt_Enable = (ChanConfig & 32'h02) >> 1;
`ifdef DMA_PRINT_ENABLED
      $display("DMA config: DMA Channel Enable: %x \n" , DMAChan_Enable );
      $display("DMA config: DMA InterruptEnable: %x \n" , DMAChanInt_Enable);
`endif
      		if ( (DMAChan_Enable == 1) && (DMA_Enable == 1) )
      		begin
        		if ( TransferCount ) 
        		begin
          			ReadAllowed = 1;
          			WriteAllowed = 0;            
	  			if ( SourceMaster == 0 )
	  				begin
            					AHB_master1_HBUSREQ  = 1;
	    
`ifdef DMA_PRINT_ENABLED
$display("DMA config: DMA just enabled -> request bus for master1");                  
`endif
          				end
	  				else
	  					begin
            					AHB_master2_HBUSREQ  = 1;       
`ifdef DMA_PRINT_ENABLED
$display("DMA config: DMA just enabled -> request bus for master2");                 
`endif
          					end
        		end
      		end 
      		else
      		begin
			AHB_master1_HBUSREQ  = 0;
			AHB_master2_HBUSREQ  = 0;
`ifdef DMA_PRINT_ENABLED
$display("DMA config: DMA Disabled");
`endif
			ReadAllowed = 0;
        		WriteAllowed = 0;              
      		end
    	 end

    	default:
	begin     
     	end
      
       endcase	
    end
    
    // READ
    else 
    begin
   // AHB_master1_HBUSREQ  = 1'bz;
   // AHB_master2_HBUSREQ  = 1'bz;
    //DMACINTR = 1'bz;
   // ReadAllowed = 1'bz;
   // WriteAllowed = 1'bz; 
    end
end 

always @ (posedge HCLK or negedge HRESETn)
begin:hhh
if(!HRESETn)
begin
  RegAHB_master2_HGRANT <= 0;
  RegAHB_master2_HREADYIN <= 0;
end
else 
begin
  RegAHB_master2_HGRANT <= sigAHB_master2_HGRANT;
  RegAHB_master2_HREADYIN <= AHB_master2_HREADYIN;
end

end

assign #1 sigAHB_master2_HGRANT =  AHB_master2_HGRANT;


always @ (posedge HCLK or negedge HRESETn)
begin:AHB_master1_AddrTrf

if(!HRESETn)
begin
    AHB_master1_HTRANS <= 1'b0;
    AHB_master2_HTRANS <= 1'b0;
    Data1 <= 0;
    Data2  <= 0;
    AHB_master2_HSIZE <= 0;
    AHB_master2_HADDR <=  0;
    AHB_master2_HWRITE <=  0;  
    AHB_master2_HBUSREQ <= 1'b0;
    AHB_master1_HBUSREQ <= 1'b0;
    AHB_master1_HBURST <= 0;
    AHB_master2_HBURST <= 0;
    state <= 3'h0;
end

else 
begin

case (state) 
 
 3'h0 :
 begin
 if(RegAHB_master2_HGRANT )
   begin
   if (AHB_master2_HREADYIN)
   begin
   if (TransferCount)
    begin
    if (SourceMaster == 1)
    begin
      if (ReadAllowed)
      begin        
`ifdef DMA_PRINT_ENABLED
$display("DMA read transfer on master2: Read data -> send address %x \n", SourceAddress);
`endif
        AHB_master2_HADDR <= SourceAddress;
        AHB_master2_HSIZE <= 2;
	AHB_master2_HTRANS <= 2;	
	if ( SourceInc )
	begin
	  SourceAddress <= SourceAddress + 4;
        end
        AHB_master2_HWRITE  <= 0;
	ReadAllowed <= 0;
	state <= 3'h7;
      end
      
    end  
    else
    begin
      if (WriteAllowed)
      begin  
            
`ifdef DMA_PRINT_ENABLED
$display("DMA write transfer on master2: Write data -> send address %x \n",DestAddress);
`endif
 
        AHB_master2_HADDR <= DestAddress;
        AHB_master2_HSIZE <= 2;
	AHB_master2_HTRANS <= 2;
	if ( DestInc )
	begin
	  DestAddress <= DestAddress + 4;
        end
        AHB_master2_HWRITE <= 1;
	WriteAllowed <= 0;
	state <= 3'h4;
      end 
    end
  end
  end
 end
end

3'h1 :
begin
  if(AHB_master2_HREADYIN)
    begin 
  	Data2 <= AHB_master2_HRDATA;	
`ifdef DMA_PRINT_ENABLED
	$display("DMA transfer on master 2: Read data -> reading data: %x \n ", Data2);
`endif	

       AHB_master2_HBUSREQ <= 0;
       AHB_master1_HBUSREQ <= 1;
       WriteAllowed <= 1;
       ReadAllowed <= 0;
       state <= 2;
    end    
end

3'h4 :
begin
  if(AHB_master2_HGRANT & AHB_master2_HREADYIN)
   begin
 
    AHB_master2_HWDATA <= Data1;

`ifdef DMA_PRINT_ENABLED
$display("DMA transfer on master2: Write data -> writing data: %x \n", Data1);
`endif
  
  TransferCount <= TransferCount - 1;
  if ( TransferCount == 0 ) 
  begin       
    AHB_master2_HBUSREQ <= 0;
    AHB_master1_HBUSREQ <= 0;
    ReadAllowed <= 0;
    WriteAllowed <= 0;
    AHB_master1_HTRANS <= 0;
    AHB_master2_HTRANS <= 0;
    state <= 7;  
    if ( DMAChanInt_Enable == 1 )
    begin
`ifdef DMA_PRINT_ENABLED
      $display("DMA write transfer on master2, completed burst and send interrupt");
`endif      //DMAINTR = 1; 
     DMACINTR <= 1;
     
      //Set_DMAInt.notify(SC_ZERO_TIME);
    end
  end
  else
  begin 
       AHB_master2_HBUSREQ <= 0;
       AHB_master1_HBUSREQ <= 1;   
       ReadAllowed <= 1;
       state <= 3;
  end  
 end 
end

3'h2 :
begin
  if(AHB_master1_HGRANT & AHB_master1_HREADYIN)
   begin

    if (TransferCount)
    begin
    if (SourceMaster == 0)
    begin
      if (ReadAllowed)
      begin
`ifdef DMA_PRINT_ENABLED
$display("DMA read transfer on master1: Read data -> send address %x \n", SourceAddress);
`endif        
        AHB_master1_HADDR <= SourceAddress;
        AHB_master1_HSIZE <= 2;
	AHB_master1_HTRANS <= 2;
	if ( SourceInc )
	begin
	  SourceAddress <= SourceAddress + 4;
        end
        AHB_master1_HWRITE <= 0;
 	ReadAllowed <= 0; 
	state <= 6;
      end 
    end
    else
    begin
      if (WriteAllowed)
      begin

`ifdef DMA_PRINT_ENABLED
$display("DMA write transfer on master1: Write data -> send address %x \n", DestAddress);
`endif
        
        AHB_master1_HADDR  <= DestAddress;
        AHB_master1_HSIZE <= 2;
	AHB_master1_HTRANS <= 2;
	if ( DestInc )
	begin
	  DestAddress <= DestAddress + 4;
        end
        AHB_master1_HWRITE <= 1;
	 WriteAllowed <= 0;
	 state <= 5;
      end
    end  
  end
end
end


3'h5 :
begin
  if(AHB_master1_HGRANT & AHB_master1_HREADYIN)
   begin
 
   AHB_master1_HWDATA <= Data2;

`ifdef DMA_PRINT_ENABLED
$display("DMA transfer on master1: Write data -> writing data: %x \n", Data2);
`endif
  
`ifdef BUG2
  TransferCount = TransferCount - 1;
`else
  TransferCount <= TransferCount - 1;
`endif
  if ( TransferCount == 0 ) 
  begin       
    AHB_master2_HBUSREQ <= 0;
    AHB_master1_HBUSREQ <= 0;
    AHB_master1_HTRANS <= 0;
    AHB_master2_HTRANS <= 0;
    ReadAllowed <= 0;
    WriteAllowed <= 0;
        
    if ( DMAChanInt_Enable == 1 )
    begin
`ifdef DMA_PRINT_ENABLED
      $display("DMA write transfer on master1, completed burst and send interrupt");
`endif
      //DMAINTR = 1; 
     DMACINTR <= 1;
      //Set_DMAInt.notify(SC_ZERO_TIME);
    end
  end
  else
  begin 
       AHB_master2_HBUSREQ <= 1;
       AHB_master1_HBUSREQ <= 0;
       AHB_master1_HTRANS <= 0;
       ReadAllowed <= 1;
  end  
  state <= 0;
 end 
end


3'h6 :
begin
 if(AHB_master1_HREADYIN)
  begin 
  	Data1 <= AHB_master1_HRDATA;	
`ifdef DMA_PRINT_ENABLED
	$display("DMA transfer on master 1: Read data -> reading data: %x \n ", Data1);
`endif	

       AHB_master1_HBUSREQ <= 0;
       AHB_master2_HBUSREQ <= 1;
       WriteAllowed <= 1;
       state <= 4;
    end 
end

3'h7 :
begin
  AHB_master2_HTRANS <= 0;
  state <= 3'h1; // wait one clockcycle
end

default : 
  begin
  end
 endcase 
end  

end 

endmodule

