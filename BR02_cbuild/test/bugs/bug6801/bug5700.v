// last mod: Wed Nov 22 12:58:24 2006
// filename: test/bugs/bug6801/bug5700.v
// Description:  This test was derived from the original test of bug 5700,
// at one time the error message for the first if(rst_n) would point to the
// second always block.


module bug5700(clk, rst_n, i1, i2, o1, o2);
   input clk, rst_n, i1, i2;
   output o1, o2;
   reg 	  o1, o2;

   always @(posedge clk or negedge i1)
     if (rst_n)
       o1 <= 1'b0;
     else
       o1 <= i1;

   always @(posedge clk or negedge rst_n)
     if (rst_n)
       o2 <= 1'b0;
     else
       o2 <= i2;
endmodule
