module top (clk, sel0, sel1, sel2, sel3, sel4, sel5, sel6, sel7,
	    en0, en1, en2, en3, en4, en5, en6, en7,
	    en8, en9, en10, en11, en12, en13, en14, en15,
	    en16, en17, en18, en19, en20, en21, en22, en23,
	    en24, en25, en26, en27, en28, en29, en30, en31);
   input clk;
   input [22:0] sel0, sel1, sel2, sel3, sel4, sel5, sel6, sel7;
   output 	en0, en1, en2, en3, en4, en5, en6, en7;
   output 	en8, en9, en10, en11, en12, en13, en14, en15;
   output 	en16, en17, en18, en19, en20, en21, en22, en23;
   output 	en24, en25, en26, en27, en28, en29, en30, en31;
   
   foo foo0  (clk, sel0, en0);
   foo foo1  (clk, sel1, en1);
   foo foo2  (clk, sel2, en2);
   foo foo3  (clk, sel3, en3);
   foo foo4  (clk, sel4, en4);
   foo foo5  (clk, sel5, en5);
   foo foo6  (clk, sel6, en6);
   foo foo7  (clk, sel7, en7);
   foo foo8  (clk, sel1^sel0, en8);
   foo foo9  (clk, sel2^sel1, en9);
   foo foo10 (clk, sel3^sel2, en10);
   foo foo11 (clk, sel4^sel3, en11);
   foo foo12 (clk, sel5^sel4, en12);
   foo foo13 (clk, sel6^sel5, en13);
   foo foo14 (clk, sel7^sel6, en14);
   foo foo15 (clk, sel0^sel7, en15);
   foo foo16 (clk, sel2^sel0, en16);
   foo foo17 (clk, sel3^sel1, en17);
   foo foo18 (clk, sel4^sel2, en18);
   foo foo19 (clk, sel5^sel3, en19);
   foo foo20 (clk, sel6^sel4, en20);
   foo foo21 (clk, sel7^sel5, en21);
   foo foo22 (clk, sel0^sel6, en22);
   foo foo23 (clk, sel1^sel7, en23);
   foo foo24 (clk, sel3^sel0, en24);
   foo foo25 (clk, sel4^sel1, en25);
   foo foo26 (clk, sel5^sel2, en26);
   foo foo27 (clk, sel6^sel3, en27);
   foo foo28 (clk, sel7^sel4, en28);
   foo foo29 (clk, sel0^sel5, en29);
   foo foo30 (clk, sel1^sel6, en30);
   foo foo31 (clk, sel2^sel7, en31);
   
endmodule

module foo (clk, sel, en);
   input clk;
   input [22:0] sel;
   output 	en;
   reg 		en;

`ifdef PERFOPT1
   always @(posedge clk)
     en <= (((sel[22:16] == 7'b0000000) & (sel[03:00] == 4'b0101)) |
	    ((sel[22:16] == 7'b0000001) & (sel[07:04] == 4'b0101)) |
	    ((sel[22:16] == 7'b0000010) & (sel[11:08] == 4'b0101)) |
	    ((sel[22:16] == 7'b0000011) & (sel[15:12] == 4'b0101)) |
	    ((sel[22:16] == 7'b0000100) & (sel[03:00] == 4'b1010)) |
	    ((sel[22:16] == 7'b0000101) & (sel[07:04] == 4'b1010)) |
	    ((sel[22:16] == 7'b0000110) & (sel[11:08] == 4'b1010)) |
	    ((sel[22:16] == 7'b0000111) & (sel[15:12] == 4'b1010)) |
	    ((sel[22:16] == 7'b0001000) & (sel[03:00] == 4'b1100)) |
	    ((sel[22:16] == 7'b0001001) & (sel[07:04] == 4'b1100)) |
	    ((sel[22:16] == 7'b0001010) & (sel[11:08] == 4'b1100)) |
	    ((sel[22:16] == 7'b0001011) & (sel[15:12] == 4'b1100)) |
	    ((sel[22:16] == 7'b0001100) & (sel[03:00] == 4'b0011)) |
	    ((sel[22:16] == 7'b0001101) & (sel[07:04] == 4'b0011)) |
	    ((sel[22:16] == 7'b0001110) & (sel[11:08] == 4'b0011)) |
	    ((sel[22:16] == 7'b0001111) & (sel[15:12] == 4'b0011)) |
	    ((sel[22:16] == 7'b0010000) & (sel[03:00] == 4'b1110)) |
	    ((sel[22:16] == 7'b0010001) & (sel[07:04] == 4'b1110)) |
	    ((sel[22:16] == 7'b0010010) & (sel[11:08] == 4'b1110))
	    );   
`else
   wire 	enout;
   
`ifdef PERFOPT2
   always @(posedge clk)
     en <= selector(sel);
`else
   always @(posedge clk)
     en <= enout;

   assign 	enout = selector(sel);
`endif

   function selector;
      input [22:0] sel;
      begin
`ifdef HAND_MINIMIZED
	 case(sel[22:16])
	   7'b0000000 : selector = (sel[3:0]  ==4'b0101);
	   7'b0000001 : selector = (sel[7:4]  ==4'b0101);
	   7'b0000010 : selector = (sel[11:8] ==4'b0101);
	   7'b0000011 : selector = (sel[15:12]==4'b0101);
	   7'b0000100 : selector = (sel[3:0]  ==4'b1010);
	   7'b0000101 : selector = (sel[7:4]  ==4'b1010);
	   7'b0000110 : selector = (sel[11:8] ==4'b1010);
	   7'b0000111 : selector = (sel[15:12]==4'b1010);
	   7'b0001000 : selector = (sel[3:0]  ==4'b1100);
	   7'b0001001 : selector = (sel[7:4]  ==4'b1100);
	   7'b0001010 : selector = (sel[11:8] ==4'b1100);
	   7'b0001011 : selector = (sel[15:12]==4'b1100);
	   7'b0001100 : selector = (sel[3:0]  ==4'b0011);
	   7'b0001101 : selector = (sel[7:4]  ==4'b0011);
	   7'b0001110 : selector = (sel[11:8] ==4'b0011);
	   7'b0001111 : selector = (sel[15:12]==4'b0011);
	   7'b0010000 : selector = (sel[3:0]  ==4'b1110);
	   7'b0010001 : selector = (sel[7:4]  ==4'b1110);
	   7'b0010010 : selector = (sel[11:8] ==4'b1110);
	   default :    selector = 1'b0;
	 endcase
`else
	 casez(sel)
	   23'b0000000????????????0101,
	   23'b0000001????????0101????,
	   23'b0000010????0101????????,
	   23'b00000110101????????????,
	   23'b0000100????????????1010,
	   23'b0000101????????1010????,
	   23'b0000110????1010????????,
	   23'b00001111010????????????,
	   23'b0001000????????????1100,
	   23'b0001001????????1100????,
	   23'b0001010????1100????????,
	   23'b00010111100????????????,
	   23'b0001100????????????0011,
	   23'b0001101????????0011????,
	   23'b0001110????0011????????,
	   23'b00011110011????????????,
	   23'b0010000????????????1110,
	   23'b0010001????????1110????,
	   23'b0010010????1110???????? :
	     selector = 1'b1;
	   default :
	     selector = 1'b0;
	 endcase
`endif
      end
   endfunction
   
`endif
endmodule
