#include DESIGN_HEADER

#include <cstdio>

int main()
{
  int i;
  int iter = 0;
  const UInt32 ONE = 0x1;
  const UInt32 ZERO = 0x0;
  UInt32 PAT[19];
  UInt32 val;
  UInt32 exp = 0;
  UInt32 errors = 0;
  
  PAT[0]  = 0x00000005;
  PAT[1]  = 0x00010050;
  PAT[2]  = 0x00020500;
  PAT[3]  = 0x00035000;
  PAT[4]  = 0x0004000A;
  PAT[5]  = 0x000500A0;
  PAT[6]  = 0x00060A00;
  PAT[7]  = 0x0007A000;
  PAT[8]  = 0x0008000C;
  PAT[9]  = 0x000900C0;
  PAT[10] = 0x000A0C00;
  PAT[11] = 0x000BC000;
  PAT[12] = 0x000C0003;
  PAT[13] = 0x000D0030;
  PAT[14] = 0x000E0300;
  PAT[15] = 0x000F3000;
  PAT[16] = 0x0010000E;
  PAT[17] = 0x001100E0;
  PAT[18] = 0x00120E00;
  
  CarbonObjectID *obj = DESIGN_CREATE(eCarbonFullDB, eCarbon_NoFlags);
  
  // CarbonWaveID *fsdb = carbonWaveInitFSDB(obj, "bug.fsdb", e1ns);
  //    carbonDumpVars(fsdb, 0, "tb");
  
  CarbonNetID *clk = carbonFindNet(obj, "top.clk");
  CarbonNetID *sel0 = carbonFindNet(obj, "top.sel0");
  CarbonNetID *sel1 = carbonFindNet(obj, "top.sel1");
  CarbonNetID *sel2 = carbonFindNet(obj, "top.sel2");
  CarbonNetID *sel3 = carbonFindNet(obj, "top.sel3");
  CarbonNetID *sel4 = carbonFindNet(obj, "top.sel4");
  CarbonNetID *sel5 = carbonFindNet(obj, "top.sel5");
  CarbonNetID *sel6 = carbonFindNet(obj, "top.sel6");
  CarbonNetID *sel7 = carbonFindNet(obj, "top.sel7");
  CarbonNetID *en0 = carbonFindNet(obj, "top.en0");
  CarbonNetID *en1 = carbonFindNet(obj, "top.en1");
  CarbonNetID *en2 = carbonFindNet(obj, "top.en2");
  CarbonNetID *en3 = carbonFindNet(obj, "top.en3");
  CarbonNetID *en4 = carbonFindNet(obj, "top.en4");
  CarbonNetID *en5 = carbonFindNet(obj, "top.en5");
  CarbonNetID *en6 = carbonFindNet(obj, "top.en6");
  CarbonNetID *en7 = carbonFindNet(obj, "top.en7");
  
  for (i=0; i<50000000; i=i+2)
    // for (i=0; i<50; i=i+2)
      {
	if ((i%1000000) == 0)
	  {
	    iter++;
	    carbonDeposit(obj, sel0, &PAT[(iter+0)%19], 0);
	    carbonDeposit(obj, sel1, &PAT[(iter+1)%19], 0);
	    carbonDeposit(obj, sel2, &PAT[(iter+2)%19], 0);
	    carbonDeposit(obj, sel3, &PAT[(iter+3)%19], 0);
	    carbonDeposit(obj, sel4, &PAT[(iter+4)%19], 0);
	    carbonDeposit(obj, sel5, &PAT[(iter+5)%19], 0);
	    carbonDeposit(obj, sel6, &PAT[(iter+6)%19], 0);
	    carbonDeposit(obj, sel7, &PAT[(iter+7)%19], 0);
	  }
	carbonDeposit(obj, clk, &ZERO, 0);
	carbonSchedule(obj, i);
	
	carbonDeposit(obj, clk, &ONE, 0);
	carbonSchedule(obj, i+1);
	/*
	  carbonExamine(obj, out1, &val, 0);
	  if (val != exp) {
	  printf("Time>%d: out1 should be %x but it was %x\n", i+1, exp, val);
	  ++errors;
	  }
	*/
      }

    carbonDestroy(&obj);

    return errors;
}
