module bug12286;

  genvar i;

  generate for(i=0; i<4; i=i+1) begin
    foo #(i) U_FOO();
  end
  
  endgenerate

endmodule // top


module foo;

  parameter PARAM = 0;
  
endmodule // foo
