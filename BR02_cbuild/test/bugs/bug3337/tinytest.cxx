#include "libtiny.h"

int main()
{
    const UInt32 ZERO = 0;
    const UInt32 ONE = 1;

    CarbonObjectID *obj = carbon_tiny_create(eCarbonFullDB, eCarbon_NoFlags);
    CarbonNetID *clk = carbonFindNet(obj, "bug.clk");
    CarbonNetID *en1 = carbonFindNet(obj, "bug.en1");
    CarbonNetID *en2 = carbonFindNet(obj, "bug.en2");
    CarbonWaveID *fsdb = carbonWaveInitFSDB(obj, "tiny.fsdb", e1ns);
    carbonDumpVars(fsdb, 0, "bug");
    CarbonTime t = 0;

    carbonDeposit(obj, clk, &ZERO, 0);
    carbonDeposit(obj, en1, &ONE, 0);
    carbonDeposit(obj, en2, &ZERO, 0);
    carbonSchedule(obj, t);
    ++t;
    carbonDeposit(obj, clk, &ONE, 0);
    carbonSchedule(obj, t);
    ++t;

    carbonDeposit(obj, clk, &ZERO, 0);
    carbonDeposit(obj, en1, &ZERO, 0);
    carbonSchedule(obj, t);
    ++t;
    carbonDeposit(obj, clk, &ONE, 0);
    carbonSchedule(obj, t);
    ++t;

    carbonDeposit(obj, clk, &ZERO, 0);
    carbonDeposit(obj, en2, &ONE, 0);
    carbonSchedule(obj, t);
    ++t;
    carbonDeposit(obj, clk, &ONE, 0);
    carbonSchedule(obj, t);
    ++t;

    carbonDeposit(obj, clk, &ZERO, 0);
    carbonSchedule(obj, t);
    ++t;
    carbonDeposit(obj, clk, &ONE, 0);
    carbonSchedule(obj, t);
    ++t;

    carbonDeposit(obj, clk, &ZERO, 0);
    carbonSchedule(obj, t);
    ++t;
    carbonDeposit(obj, clk, &ONE, 0);
    carbonSchedule(obj, t);
    ++t;

    carbonDestroy(&obj);
    return 0;
}
