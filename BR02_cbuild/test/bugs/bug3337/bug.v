module bug(clk, en1, en2);
   input clk, en1, en2;

   tri0 [15:0] data;
   foo foo(.clk(clk), .en1(en1), .en2(!en1 & en2),.data(data));

   mon mon(.clk(clk),
	   .datal(data[7:0]),
	   .datah(data[15:8]));

endmodule

module foo(clk, en1, en2,data);
   input clk, en1, en2;
   inout [15:0] data;

   wire [15:0] data;  // carbon observeSignal

   mod1 mod1(.clk(clk),
	     .en(en1),
	     .d15(data[15]),
	     .d14(data[14]),
	     .d13(data[13]),
	     .d12(data[12]),
	     .d11(data[11]),
	     .d10(data[10]),
	     .d9(data[9]),
	     .d8(data[8]),
	     .d7(data[7]),
	     .d6(data[6]),
	     .d5(data[5]),
	     .d4(data[4]),
	     .d3(data[3]),
	     .d2(data[2]),
	     .d1(data[1]),
	     .d0(data[0]));

   mod2 mod2(.clk(clk),
	     .en(en2),
	     .data(data));
endmodule

module mod1(clk, en, d15, d14, d13, d12, d11, d10, d9, d8, d7, d6, d5, d4, d3, d2, d1, d0);
   input clk, en;
   inout d15, d14, d13, d12, d11, d10, d9, d8, d7, d6, d5, d4, d3, d2, d1, d0;

   reg 	 en_reg;
   wire [15:0] data_out = 16'h1111;

   always @(posedge clk)
     en_reg <= en;

   assign      d15 = en_reg ? data_out[15] : 1'bz;
   assign      d14 = en_reg ? data_out[14] : 1'bz;
   assign      d13 = en_reg ? data_out[13] : 1'bz;
   assign      d12 = en_reg ? data_out[12] : 1'bz;
   assign      d11 = en_reg ? data_out[11] : 1'bz;
   assign      d10 = en_reg ? data_out[10] : 1'bz;
   assign      d9 = en_reg ? data_out[9] : 1'bz;
   assign      d8 = en_reg ? data_out[8] : 1'bz;
   assign      d7 = en_reg ? data_out[7] : 1'bz;
   assign      d6 = en_reg ? data_out[6] : 1'bz;
   assign      d5 = en_reg ? data_out[5] : 1'bz;
   assign      d4 = en_reg ? data_out[4] : 1'bz;
   assign      d3 = en_reg ? data_out[3] : 1'bz;
   assign      d2 = en_reg ? data_out[2] : 1'bz;
   assign      d1 = en_reg ? data_out[1] : 1'bz;
   assign      d0 = en_reg ? data_out[0] : 1'bz;

endmodule

module mod2(clk, en, data);
   input clk, en;
   inout [15:0] data;

   reg 	 en_reg;
   wire [15:0] data_out = 16'h2222;

   always @(posedge clk)
     en_reg <= en;

   assign      data = en_reg ? data_out : {16{1'bz}};
endmodule

module mon(clk, datah, datal);
   input clk;
   input [7:0] datah, datal;

   always @(posedge clk)
     $display("datah = %x, datal = %x", datah, datal);
endmodule
