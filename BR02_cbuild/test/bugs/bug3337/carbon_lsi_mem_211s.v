/*
 Carbon implementation of LSI's 2 port (1 read, 1 write) synchronous memory
 */

module carbon_lsi_mem_211s(clka, clkb, adra, adrb, dib, mea, meb, web, wemb, doa);
   parameter ADDR_WIDTH = 3;
   parameter DATA_WIDTH = 8;
   parameter DEPTH = 8;

   input     clka, clkb;
   input [ADDR_WIDTH-1:0] adra, adrb;
   input [DATA_WIDTH-1:0] dib;
   input 		  mea, meb;
   input 		  web;
   input [DATA_WIDTH-1:0] wemb;
   output [DATA_WIDTH-1:0] doa;
   
   reg [DATA_WIDTH-1:0] doa;
   reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];

   always @(posedge clka)
     if (mea)
       doa <= mem[adra];

   always @(posedge clkb)
     if (meb)
       if (web)
	 mem[adrb] <= (mem[adrb] & ~wemb) | (dib & wemb);

endmodule
