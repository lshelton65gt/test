module bug(clk,en1,en2);
   input clk,en1,en2;

   tri0 [3:0] data;

   mover mov1(clk,en1,        data[1:0],data[3:2]);
   mover mov2(clk,!en1 & en2,data[3:2],data[1:0]);

   always @(posedge clk)
     $display("data = %x", data);
endmodule

module mover(clk,en,a,b);
   input clk,en;
   output [1:0] a,b;
   assign a = en ? 2'b10 : 2'bz;
   assign b = en ? 2'b01 : 2'bz;
`ifdef WORKAROUND
   wire dummy = a & b;
`endif
endmodule
