// Attempts to reproduce problems with fold reported in bug6622.
module bug6622(clk, in1, in21, in22, in23, in3, en, out1, out2);
  input clk;
  input in1;
  input [31:0] in21;
  input [31:0] in22;
  input [31:0] in23;
  input [7:0]  in3;
  input        en;
  output [31:0] out1;
  output out2;

  reg [31:0]    sig1[0:5];
  reg [31:0]    sig2;
  reg [31:0]    sig3[0:5];

  reg           out2;

  integer       foridx;

  assign        out1 = sig1[2];

  always @(posedge clk)
    begin              
      sig1[0] <= in21;                
      sig2 <= in22;                   
      sig3[0] <= in23;

      for (foridx = 1; (foridx <= 5); foridx = (foridx + 1)) begin
        sig3[foridx] <= sig3[(foridx - 1)];
      end

      for (foridx = 1; (foridx <= 5); foridx = (foridx + 1)) begin
        sig1[foridx] <= sig1[(foridx - 1)];
      end

      if ((((in3[2] == 1'b1) & (in1 == 1'b0)) != 1'b0)) begin
        sig1[2] <= sig1[1];
      end
      else begin
        if ((((in3[2] == 1'b1) & (in1 == 1'b1)) != 1'b0)) begin
          // The if-else chain gets optimized where the if conditions looks like:
          //  (sig2[1] & (sig3[0][1] | !sig3[0][1]).
          // Fold then incorrectly folds it to:
          //  (sig2[1] & (|((~sig3[0]) & 0x2)))
          if ((((sig2[1] == 1'b1) & (sig3[0][1] == 1'b1)) != 1'b0)) begin // 11
            sig1[2] <= sig1[1];
          end
          else begin
            if ((((sig2[1] == 1'b0) & (sig3[0][1] == 1'b1)) != 1'b0)) begin // 01
              if ((en == 1'b0))
                sig1[2] <= sig2;     
              else
                sig1[2] <= sig3[1];  
            end
            else begin
              if ((((sig2[1] == 1'b1) & (sig3[0][1] == 1'b0)) != 1'b0)) begin // 10
                sig1[2] <= sig1[1];
              end
              else begin // 11
                if ((en == 1'b0))
                  sig1[2] <= sig2;      
                else
                  sig1[2] <= sig3[1];   
              end
            end
          end
        end
      end

      if (in3[2] == 1'b1) begin
        out2 <= sig2[1];
      end
      else begin
        // This if condition gets incorrectly folded to:
        //  ((sig3[0] & 0x2) == 0)
        if (sig3[0][1] & !sig3[0][1]) begin
          out2 <= sig2[0];
        end
        // This if condition gets incorrectly folded to:
        // ((sig3[1] & 0x2) == 0)
        else if (!sig3[1][1] & sig3[1][1]) begin
          out2 <= sig2[1];
        end
      end


      if (in3[3] == 1'b1) begin
        // This if condition gets incorrectly folded to:
        // (|(~sig3[2] & 0x2))
        if (!sig3[2][1] | sig3[2][1]) begin
          out2 <= sig2[3];
        end
        // This if condition incorrectly folds to:
        // ((sig3[2][1] & ((sig3[3] & 0x2) == 0)))
        else if (!sig3[3][1] & sig3[2][1] & sig3[3][1]) begin
          out2 <= sig2[5];
        end
      end

    end
endmodule // bug6622
