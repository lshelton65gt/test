-- February 2008
-- This file tests the visibility of a signal of array of boolean type..

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is
  type t_ar_bool is array (3 downto 2) of  boolean;
end FIXED_POINT_PKG;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity array_bool is
  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    in1  : in  boolean;
    out1 : out boolean
    );
end ;

architecture arch of array_bool is
  signal s_bool  : t_ar_bool;              
begin

  P1: process (rst, clk)
  begin  -- process P1
    if rst = '1' then
      s_bool <= (false, false);
    elsif clk'event and clk = '1' then    -- rising clock edge
      s_bool(2) <= in1;
      s_bool(3) <= s_bool(2);
    end if;
  end process P1;

  out1 <= s_bool(3);
end;
