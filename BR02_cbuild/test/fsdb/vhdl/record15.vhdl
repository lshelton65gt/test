-- Test passing a record to a procedure (output variable test)
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
      r3: std_logic;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity record15dut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end record15dut;

architecture a of record15dut is
  signal sigrec : rec := ( "00000000", "0000", '0' );

  procedure proc ( variable outrec : out rec; variable o1 : out std_logic;
                   b1, b2 : in std_logic ) is
    begin
      o1 := b1 or b2;
      outrec.r1 := (others => b1);
      outrec.r2 := (others => b2);
      outrec.r3 := b1 and b2;
    end proc;
begin

  p1: process (clk)
    -- note: the temps are here because we can't use signals in
    -- procedure output ports yet; this test was converted from using
    -- signals to variables.
    variable outtemp1, outtemp2 : rec;
    variable to1, to2 : std_logic;
  begin
    if clk'event and clk = '1' then
      proc( outtemp1, to1, b2 => recinport.r3, b1 => not recinport.r3 );
      recoutport1 <= outtemp1;
      recoutport1.r3 <= to1;
      proc( outtemp2, to2, recinport.r3, recinport.r2(2));
      sigrec <= outtemp2;
      recoutport2.r3 <= to2;
      recoutport2 <= sigrec;
    end if;
  end process p1;
end a;


library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity record15 is
  port (
    clk            : in  std_logic;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recinport_r3   : in  std_logic;
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0);
    recoutport1_r3 : out std_logic;
    recoutport2_r1 : out std_logic_vector(7 downto 0);
    recoutport2_r2 : out std_logic_vector(3 downto 0);
    recoutport2_r3 : out std_logic);
end record15;

architecture a of record15 is
  component record15dut is
                          port ( recinport : in rec
                                 ; recoutport1: out rec
                                 ; recoutport2: out rec
                                 ; clk: in std_logic
                                 );
  end component record15dut;

begin

  dut : record15dut port map (
    clk            => clk,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recinport.r3   => recinport_r3,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2,
    recoutport1.r3 => recoutport1_r3,
    recoutport2.r1 => recoutport2_r1,
    recoutport2.r2 => recoutport2_r2,
    recoutport2.r3 => recoutport2_r3);
  
end a;
