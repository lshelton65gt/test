-- April 2008
-- This file tests the enumeration type with character literals.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity enum_04 is
   port (in1  : in bit_vector(0 to 2);
         out1 : out bit_vector(0 to 2));
end enum_04;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

architecture arch of enum_04 is
   type    t_colors  is ('r', 'O', 'y', 'G', 'b', 'D', 'v', 'W');
   signal color_sig : t_colors := 'W';  -- carbon observeSignal
begin

  p1: process (in1)
    variable color_var : t_colors;
  begin  -- process p1
    
    case in1 is
      when "000" =>
        color_var := 'r';
      when "001" =>
        color_var := 'O';
      when "010" =>
        color_var := 'y';
      when "011" =>
        color_var := 'G';
      when "100" =>
        color_var := 'b';
      when "101" =>
        color_var := 'D';
      when "110" =>
        color_var := 'v';
      when "111" => 
        color_var := 'W';
    end case;
    color_sig <= color_var;
  end process p1;

  out1 <= in1;
end arch;


