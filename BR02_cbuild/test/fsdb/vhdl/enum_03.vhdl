-- March 2008
-- This file tests the fsdb dump of enumeration type, which has more than 256 elements.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
        

entity enum_03 is
   port (in_int  : in integer range 0 to 260;
         out_int : out integer range 0 to 260);
end enum_03;

architecture arch of enum_03 is
   type t_enum_big is (A,   B,  C,  D,  E,  F,  G,  H,  I,  J,  K,  L,  M,  N,  O,  P,  Q,  R,  S,  T,  U,  V,  W,  X,  Y,  Z,
                       AA, AB, AC, AD, AE, AF, AG, AH, AI, AJ, AK, AL, AM, AN, AO, AP, AQ, AR, AS, AT, AU, AV, AW, AX, AY, AZ,
                       BA, BB, BC, BD, BE, BF, BG, BH, BI, BJ, BK, BL, BM, BN, BO, BP, BQ, BR, BS, BT, BU, BV, BW, BX, BY, BZ,
                       CA, CB, CC, CD, CE, CF, CG, CH, CI, CJ, CK, CL, CM, CN, CO, CP, CQ, CR, CS, CT, CU, CV, CW, CX, CY, CZ,
                       DA, DB, DC, DD, DE, DF, DG, DH, DI, DJ, DK, DL, DM, DN, DO, DP, DQ, DR, DS, DT, DU, DV, DW, DX, DY, DZ,
                       EA, EB, EC, ED, EE, EF, EG, EH, EI, EJ, EK, EL, EM, EN, EO, EP, EQ, ER, ES, ET, EU, EV, EW, EX, EY, EZ,
                       FA, FB, FC, FD, FE, FF, FG, FH, FI, FJ, FK, FL, FM, FN, FO, FP, FQ, FR, FS, FT, FU, FV, FW, FX, FY, FZ,
                       GA, GB, GC, GD, GE, GF, GG, GH, GI, GJ, GK, GL, GM, GN, GO, GP, GQ, GR, GS, GT, GU, GV, GW, GX, GY, GZ,
                       HA, HB, HC, HD, HE, HF, HG, HH, HI, HJ, HK, HL, HM, HN, HO, HP, HQ, HR, HS, HT, HU, HV, HW, HX, HY, HZ,
                       IA, IB, IC, ID, IE, IF1,IG, IH, II, IJ, IK, IL, IM, IN1,IO, IP, IQ, IR, IS1,IT, IU, IV, IW, IX, IY, IZ,
                       ENDENUM
                      );
  signal enumSignal     : t_enum_big := ENDENUM;     -- carbon observeSignal
begin


  p1: process (in_int)
    variable en_var : t_enum_big;
  begin  -- process p1

  if in_int < 26 then
    en_var := A;
  elsif in_int < 52 then
    en_var := AA;
  elsif in_int < 78 then
    en_var := BA;
  elsif in_int < 104 then
    en_var := CA;
  elsif in_int < 130 then
    en_var := DA;
  elsif in_int < 156 then
    en_var := EA;
  elsif in_int < 182 then
    en_var := FA;
  elsif in_int < 208 then
    en_var := GA;
  elsif in_int < 234 then
    en_var := HA;
  elsif in_int < 260 then
    en_var := IA;
  else
    en_var := ENDENUM;
  end if;
  enumSignal <= en_var; 
  end process p1;
    
  p2: process (enumSignal)
  begin
    case enumSignal is
      when  A  =>  out_int <= 0;
      when  AA => out_int <= 26;
      when  BA => out_int <= 52;
      when  CA => out_int <= 78;
      when  DA => out_int <= 104;
      when  EA => out_int <= 130;
      when  FA => out_int <= 156;
      when  GA => out_int <= 182;
      when  HA => out_int <= 208;
      when  IA => out_int <= 234;
      when others => 
        out_int <= 260;
    end case;
  end process p2;

end arch;


