-- March 2008
-- This file tests the fsdb dump of characters and string
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library ieee;
use ieee.std_logic_1164.all;
entity character1 is
  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    out1 : out character
    );
end ;

architecture arch of character1 is
  signal my_str  : string(1 to 9) := "ABCDEFGHI"; -- carbon observeSignal
  signal my_char : character := 'Z';              -- carbon observeSignal
begin

  P1: process (rst, clk, my_char)
    variable position : natural;                      
  begin  -- process P1
    if rst = '1' then
      out1 <= my_char;
      position := 1;
    elsif clk'event and clk = '1' then    -- rising clock edge
      out1 <= my_str(position);
      if(position = 9) then
        position := 1;
      else
        position := position + 1;
      end if;
    end if;
  end process P1;
end;
