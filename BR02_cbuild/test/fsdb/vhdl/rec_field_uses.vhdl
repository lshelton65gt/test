-- This test exercises the getUses() method on NUCompositeFieldExpr. The
-- if statement conditions which use record fields achieve that.
library ieee;
use ieee.std_logic_1164.all;

entity rec_field_uses is
  
  port (
    en  : in  std_logic_vector(1 downto 0);
    in1  : in  std_logic_vector(7 downto 0);
    out1 : out std_logic_vector(3 downto 0));

end rec_field_uses;

architecture arch of rec_field_uses is
  type rec is record
                en   : std_logic;
                data : std_logic_vector(3 downto 0);
              end record;
  type recvec is array (natural range <>) of rec;
  
  constant const_rec : rec := (en => '0', data => "0000");
  signal sig_recvec : recvec(1 downto 0) := (others => const_rec);  -- carbon observeSignal
  
begin  -- arch

  proc: process (en, in1, sig_recvec)
  begin  -- process proc
    sig_recvec(0).en <= en(0);
    sig_recvec(0).data <= in1(3 downto 0);
    sig_recvec(1).en <= en(1);
    sig_recvec(1).data <= in1(7 downto 4);
    
    if ((sig_recvec(0).en = '1') and (sig_recvec(1).en = '1')) then -- tests getUses() on NUCompositeFieldExpr
      out1 <= sig_recvec(0).data and sig_recvec(1).data;
    end if;
  end process proc;

end arch;
