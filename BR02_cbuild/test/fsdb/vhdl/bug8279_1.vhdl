library ieee;
use ieee.std_logic_1164.all;

entity bug8279_1 is
  port (
    clk  : in  std_logic;
    cin  : in  character;
    bin  : in  boolean;
    cout : out character;
    bout : out boolean);
end bug8279_1;

architecture arch of bug8279_1 is

  type myrec is record
                  c : character;
                  b : boolean;
                end record;

  signal r : myrec;
  
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      r.c <= cin;
      r.b <= bin;
      cout <= r.c;
      bout <= r.b;
    end if;
  end process;
  
end arch;
