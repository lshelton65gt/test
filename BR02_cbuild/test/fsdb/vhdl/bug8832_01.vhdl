-- May 2008
-- This file tests the -vhdlCase preserve switch. Note that there are lower case a,
-- b signals and A,B field of the myrec record. CBUILD used to confuse the
-- names. The fsdb file shows the names correctly.
library ieee;
use ieee.std_logic_1164.all;

entity bug8832_01 is
  port (
    clk : in  std_logic;
    rst : in  std_logic;
    a   : in  std_logic;
    b   : in  std_logic;
    o   : out std_logic);
end bug8832_01;

architecture arch of bug8832_01 is

  type myrec is record
                  A : std_logic;
                  B : std_logic;
                end record;
  signal r : myrec := ('0', '0');
  
begin

  process (clk, rst)
  begin
    if rst = '0' then  
      o <= '0';
    elsif clk'event and clk = '1' then
      r.A <= a;
      r.B <= b;
      o <= r.A and r.B;
    end if;
  end process;
  
end arch;
