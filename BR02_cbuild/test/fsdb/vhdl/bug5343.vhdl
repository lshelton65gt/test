library IEEE;
use IEEE.std_logic_1164.all;

entity BUG5343 is 
  port (
    eq  : out std_logic;
    z	: out std_logic_vector(7 downto 0);
    a	: in  std_logic_vector(7 downto 0);
    clk	: in  std_logic
    );
end BUG5343;

architecture functional of BUG5343 is
  type REC is record
                x : std_logic_vector(7 downto 0);
                y : std_logic_vector(7 downto 0);
              end record;
  signal rec0 : REC := ((others=>'0'), (others=>'0'));
  signal rec1 : REC := ((others=>'0'), (others=>'0'));
begin

    process (clk)
    begin
      if (clk'event and clk = '1') then
        z <= rec1.y;
        rec1.y <= rec1.x;
        rec1.x <= rec0.y;
        rec0.y <= rec0.x;
        rec0.x <= a;

        if (rec0 = rec1) then
          eq <= '1';
        else
          eq <= '0';
        end if;
      end if;
    end process;

end functional;


