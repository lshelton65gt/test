-- This tests arrayed records, which makes memories of the record elements,
-- with all array directions being "downto".

library ieee;
--use ieee.numeric_bit.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_1164.all;


entity bug3331clks is
  port(clk : in std_logic ;
       in1: in std_logic_vector(174 downto 0); 
       --waddr_int : in integer range 0 to 1;
       waddr_int : in std_logic;
       junk1: out std_logic_vector(19 downto 11);
       junk2: out std_logic_vector(90 downto 10);
       junk3: out std_logic_vector(10 downto 0));
end;

architecture bug3331clks of bug3331clks is
  type h025risefiforecord is
    record
      addr                   : std_logic_vector( 119 downto 4);
      cmd                    : std_logic_vector(  65 downto 8);
      fmt                    : std_logic_vector(  14 downto 0);
    end record;
  type   h025risememtype is array(1 downto 0) of h025risefiforecord;
  signal data2align   : h025risememtype := (others => ((others => '0'), (others => '0'), (others => '0')));  -- carbon observeSignal

begin
  process (clk, in1, data2align)
  begin
    if (clk'event and clk = '1') then
      data2align(CONV_INTEGER(waddr_int)).addr(19 downto 14) <= in1(79 downto 74);
      data2align(CONV_INTEGER(waddr_int)).addr(13 downto 4) <= in1(93 downto 84);
      data2align(CONV_INTEGER(waddr_int)).addr(119 downto 20) <= in1(129 downto 30);
      data2align(CONV_INTEGER(waddr_int)).fmt <= in1(14 downto 0);
      junk1 <= data2align(CONV_INTEGER(waddr_int)).fmt(14 downto 6);
      junk2 <= data2align(CONV_INTEGER(waddr_int)).addr(113 downto 33);
      junk3 <= data2align(CONV_INTEGER(waddr_int)).addr(15 downto 5); 
    end if;
  end process;
end;
