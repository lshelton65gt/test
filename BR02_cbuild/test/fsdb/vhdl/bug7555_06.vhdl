-- September 2007
-- The function max_packet returns a record with array.
-- It is called like this: function_name.arr(i). 
-- 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use std.textio.all;

package pkg_test is

    type arr_int is array (0 to 1) of integer;
    type rec is
       record 
         arr      : arr_int;
       end record;
       
    function max_packet(a : integer; b : integer) return rec;
    
end pkg_test;

package body pkg_test is 
    
    function max_packet(a : integer; b : integer) return rec
    is
      variable Ret : rec;
    begin
          Ret.arr(0) := a;
          Ret.arr(1) := b;
       return Ret;
    end;
    
end pkg_test;    



library ieee;
use ieee.std_logic_1164.all;
use work.pkg_test.all;

entity bug7555_06 is
    port (clk	   : in	std_logic;
	  input_1  : in	integer;
	  input_2  : in	integer;
	  output_1 : out integer;
          output_2 : out integer);
end bug7555_06;

architecture behav of bug7555_06 is
  signal temp : arr_int; -- carbon observeSignal
begin

process(clk)
begin
   if (clk = '1' and clk'event) then
       temp <= max_packet(input_1, input_2 ).arr(0 to 1);   -- this line is tested
   end if;
end process;

  output_1 <= temp(0);
  output_2 <= temp(1);
  
  

end behav;
