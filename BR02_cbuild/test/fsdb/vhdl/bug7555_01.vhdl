-- September 2007
-- The function max_packet returns a record.
-- It is called like this: function_name.record_field. 
-- 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use std.textio.all;

package pkg_test is
    type  packet is
       record 
         a : std_logic_vector(3 downto 0);
         b : std_logic_vector(31 downto 0);
       end record;
       
    function max_packet(a : std_logic_vector(3 downto 0);
			b: std_logic_vector(31 downto 0)) return packet;
    
end pkg_test;

package body pkg_test is 
    
    function max_packet(a : std_logic_vector(3 downto 0);
			b : std_logic_vector(31 downto 0)) return packet
    is
      variable Ret_c : packet;
    begin
          Ret_c.a := a;
          Ret_c.b := b;
       return Ret_c;
    end;
    
end pkg_test;    



library ieee;
use ieee.std_logic_1164.all;
use work.pkg_test.all;

entity bug7555_01 is
    port (clk	   : in	 std_logic;
	  input_a  : in	 std_logic_vector(3 downto 0);
	  input_b  : in	 std_logic_vector(31 downto 0);
	  output_a : out std_logic_vector(3 downto 0);
	  output_b : out std_logic_vector(31 downto 0));
end bug7555_01;

architecture behav of bug7555_01 is
  signal result  : packet;
begin

process(clk)
  begin
    if (clk = '1' and clk'event) then
       result.a <= max_packet(input_a, input_b).a;  -- this line is tested
       result.b <= max_packet(input_a, input_b).b;  -- this line is tested
    end if;
  end process;

  output_a <= result.a;
  output_b <= result.b;

end behav;
