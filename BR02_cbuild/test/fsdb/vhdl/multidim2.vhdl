-- This tests the ability to populate arrays of arrays of records and to
-- reference the record fields.  It also points out problems in Aldec; it gets
-- REM incorrect.

entity multidim2 is
  port ( clk : in bit;
         in1 : in integer;
         o1, o2 : out integer );
end entity;

architecture arch of multidim2 is
  constant cNumClkDomain : integer := 8;
  type tRange is record
                   high : integer;
                   low : integer;
                 end record;
  type tClkRange is array (cNumClkDomain-1 downto 0) of tRange;
  type tClkRangeArr is array (natural range <>) of tClkRange;
  signal s : tClkRangeArr(1 to 2) := (others => (others => (0,0)));   -- carbon observeSignal
begin

  p1 : process ( clk )
  begin
    if clk'event and clk = '1' then  -- rising clock edge
      for i in s'range loop
        for j in tClkRange'range loop
          s(i)(j).high <= in1 rem 128;
          s(i)(j).low <= (in1 + 1) rem 128;
        end loop;
      end loop;
    end if;
  end process;

  p2 : process (clk)
    variable r1 : integer range tClkRange'range := tClkRange'left;
    variable r2 : integer range s'range := s'left;
  begin
    if clk'event and clk = '1' then  -- rising clock edge
      o1 <= s(r2)(r1).high;
      o2 <= s(r2)(r1).low;
      if r1 = 0 then
        r1 := 7;
      else
        r1 := r1 - 1;
      end if;
      if r2 = 1 then
        r2 := 2;
      else
        r2 := 1;
      end if;
    end if;
  end process p2;
  
end architecture;
