-- Tests concat of 3 records.
library ieee;
use ieee.std_logic_1164.all;

entity concat3 is
  
  port (
    in1  : in  std_logic;
    in2  : in  std_logic;
    in3  : in  std_logic;
    in4  : in  std_logic;
    in5  : in std_logic;
    in6  : in std_logic;
    out1 : out std_logic;
    out2 : out std_logic);

end concat3;

architecture arch of concat3 is
  
  type rec is record
                f1         : std_logic;
                f2         : std_logic;
              end record;
  type recvec is array (natural range<>) of rec;

  signal r1 : rec;
  signal r2 : rec;
  signal r3 : rec;
  signal rv : recvec(2 downto 0);
  
begin  -- arch

  r1 <= (in1, in2);
  r2 <= (in3, in4);
  r3 <= (in5, in6);

  rv <= r1 & r2 & r3;

  out1 <= rv(0).f1 xor rv(1).f1 xor rv(2).f1;
  out2 <= rv(0).f2 xor rv(1).f2 xor rv(2).f2;

end arch;
