-- Test port unconstrained type arrayed records concurrent assignment
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package p is

type tim_logic_vector is array (integer range <>) of bit;
  
type rec is record
    r1 : tim_logic_vector(7 downto 0);
    r2 : tim_logic_vector(3 downto 0);
end record;

end p;

use work.p.all;

entity record6 is
  port ( recinport : in rec
       ; recoutport: out rec
       ; clk: in bit
       );
end record6;

architecture a of record6 is
  signal localrec : rec;
begin

  localrec <= recinport;
  recoutport <= localrec;

end a;
