-- Feb 2008
-- This file is failing with user type population.
-- The problem is with array of enumerations

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
        
package base is

   type days is (mon, tue, wed, thr, fri, sat, sun);
   
   type tZtoN_enum is array(0 to 7) of days;

   type tZtoN_tZtoN_enum is array(0 to 7) of tZtoN_enum;

   function To_Days (param : std_logic_vector) return days;

   function To_StdLogicVector (param : days) return std_logic_vector;

end base;


package body base is

  function To_Days (param : std_logic_vector) return days is
    variable index : std_logic_vector(0 to 2) := param;
    variable result : days;
  begin  -- To_Days
    case index is
      when "000" => result := mon;
      when "001" => result := tue;
      when "010" => result := wed;
      when "011" => result := thr;
      when "100" => result := fri;
      when "101" => result := sat;
      when "110" => result := sun;
      when others => result := mon;
    end case;
    return result;
  end To_Days; 

  function To_StdLogicVector (param : days) return std_logic_vector is
    variable result : std_logic_vector(0 to 2);
  begin  -- To_StdLogicVector
    case param is
      when mon => result := "000";
      when tue => result := "001";
      when wed => result := "010";
      when thr => result := "011";
      when fri => result := "100";
      when sat => result := "101";
      when sun => result := "110";
    end case;
    return result;
  end To_StdLogicVector;

end base;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity bottom is
   port ( bin  : in tZtoN_tZtoN_enum;
          bout : out tZtoN_tZtoN_enum );
end bottom;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archbottom of bottom is

   signal s1 :tZtoN_tZtoN_enum;

begin

   s1 <= bin;

   bout <= s1;

end archbottom;



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity bug8271_1 is
   port ( pin  : in  tZtoN_tZtoN_enum;
          pout : out tZtoN_tZtoN_enum );
end bug8271_1;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archZtoN_ZtoN_enum of bug8271_1 is

   component bottom is
      port ( bin  : in  tZtoN_tZtoN_enum;
             bout : out tZtoN_tZtoN_enum);
   end component;

begin

   u1 : bottom port map ( bin  => pin,
                          bout => pout);

end archZtoN_ZtoN_enum;
