-- Test aggregate for nested array of records. The aggregate uses
-- different fields of nested record, allowing testing of aggregate
-- population and resynthesis.
library ieee;
use ieee.std_logic_1164.all;

entity aggregate_init12 is
  port ( clk, reset   : in std_logic;
         mid_nest1_b1 : in std_logic;
         mid_nest1_t2 : in std_logic_vector(3 downto 0);
         m2           : in std_logic;
         mid_nest2_b1 : in std_logic;
         mid_nest2_t2 : in std_logic_vector(3 downto 0);
         out_b1 : out std_logic;
         out_t2 : out std_logic_vector(3 downto 0);
         out_m2 : out std_logic);
end aggregate_init12;

architecture a of aggregate_init12 is
  -- bottomrec is 5 bits total
  type bottomrec is record
                      b1     : std_logic;
                      t2     : std_logic_vector(3 downto 0);
                    end record;

  -- midrec is 11 bits total
  type midrec is record
                   mid_nest1 : bottomrec;
                   m2        : std_logic;
                   mid_nest2 : bottomrec;
                 end record;

  type bundlerec is array(natural range<>) of midrec;
  constant const2 : midrec := (('1', "0100"), '1', ('0', "1010"));
  signal sigrec : bundlerec(0 to 3) := ((('0', "1110"), '1', ('1', "1001")), others => const2); -- carbon observeSignal
begin
  sigrec(1) <= ( m2 => sigrec(0).mid_nest2.b1,
                 others => sigrec(0).mid_nest1 );
  sigrec(0) <= ((mid_nest1_b1, mid_nest1_t2), m2, (mid_nest2_b1, mid_nest2_t2));
  out_b1 <= sigrec(0).mid_nest1.b1;
  out_t2 <= sigrec(1).mid_nest2.t2;
  out_m2 <= sigrec(0).m2;
end a;
