-- record field access in selected signal assignment
library ieee;
use ieee.std_logic_1164.all;

entity bug3820_4 is
  port(in1, in2 : std_logic_vector(0 to 1); out1 : out std_logic);
end;

architecture arch of bug3820_4 is
  type myrec is record
                  f1 : std_logic;
                  f2 : std_logic;
                end record;

  signal sig1 : myrec;  --  carbon observeSignal
  signal sig2 : myrec;  --  carbon observeSignal
  signal temp : myrec;  --  carbon observeSignal
begin
  sig1.f1 <= in1(0);
  sig1.f2 <= in1(1);
  sig2.f1 <= in2(0);
  sig2.f2 <= in2(1);
  temp.f1 <= sig1.f1 when in1(0)  = '1' else sig2.f1;
  with in1(0) select
    temp.f2 <= sig1.f2 when '1' , sig2.f2 when others;
  out1 <= temp.f1 and temp.f2;
end;

