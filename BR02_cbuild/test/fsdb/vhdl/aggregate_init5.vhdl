-- Test array aggregate initialization with a record array with named
-- association and OTHERS.  Test a positive unary operator, a range, and
-- a literal as named association.  Add a negative array bound in the
-- mix for fun, along with a unary op in a range.
library ieee;
use ieee.std_logic_1164.all;
entity aggregate_init5 is
  port (in1 : in  integer range -3 to 3;
        out1: out boolean;
        out2: out std_logic_vector(3 downto 0));
end aggregate_init5;

architecture rtl of aggregate_init5 is
  type ctype is record
    faddr : std_logic_vector(3 downto 0);
    en    : boolean;
  end record;
  type sconfig is array (integer Range <> ) of ctype;
  constant configv : ctype := ((others => '0'), false);
  constant cfg_std : sconfig(-3 to 3) := (
    -2 => (('0', others => '1'), true),
    -3 => configv,
    -1 to +2 => ("1010", true),
    +3 => (en => false, faddr => "1000"),
    others => configv
    );
begin
  out1 <= cfg_std(in1).en;
  out2 <= cfg_std(in1).faddr;
end rtl;
