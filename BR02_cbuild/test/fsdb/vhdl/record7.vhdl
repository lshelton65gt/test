-- Test port unconstrained type arrayed records, referencing individual
-- array members
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package p is

type tim_logic_vector is array (integer range <>) of bit;
  
type rec is record
    r1 : tim_logic_vector(7 downto 0);
    r2 : tim_logic_vector(3 downto 0);
end record;

type bundlerec is array(natural range<>) of rec;

end p;

use work.p.all;

entity record7 is
  port ( recinport : in rec
       ; recoutport: out rec
       ; clk: in bit
       );
end record7;

architecture a of record7 is
  signal arrayrec : bundlerec(0 to 1); -- carbon observeSignal
begin

  arrayrec(0) <= recinport;
  arrayrec(1) <= arrayrec(0);
  recoutport <= arrayrec(1);

end a;
