library ieee;
use ieee.std_logic_1164.all;

package pciGlobalDefs is
  constant pci_Data32BitBusWidth:     integer := 32;

  type bundle_pci32BitBus is
    record
      ad:             std_logic_vector (pci_Data32BitBusWidth-1 downto 0);
    end record;

  subtype accessType is std_logic_vector(1 downto 0);
  constant maxSonicsToPciRequests:            integer := 1;

  type bundle_sonicsPciRequest is
    record
      accessSpace:     accessType;
      firstByteEnable: std_logic_vector(3 downto 0);
    end record;
  type bundleVector_sonicsPciRequest is array(natural range<>) of bundle_sonicsPciRequest;end pciGlobalDefs;


library ieee;
use ieee.std_logic_1164.all;
use work.pciglobaldefs.all;

entity bug4456a is
  port (
    in1                : in  std_logic_vector(1 downto 0);
    accessSpace1       : in  accessType;
    firstByteEnable1   : in  std_logic_vector(3 downto 0);
    accessSpace0       : in  accessType;
    firstByteEnable0   : in  std_logic_vector(3 downto 0);
    ad                 : buffer std_logic_vector(pci_Data32BitBusWidth-1 downto 0));
end bug4456a;

architecture arch of bug4456a is
  signal sonicsRequest: bundleVector_sonicsPciRequest(maxSonicsToPciRequests downto 0); -- carbon observeSignal
  signal currentRequest: integer range maxSonicsToPciRequests downto 0;
  signal core2pad_pciBus : bundle_pci32BitBus := (others => (others => '0'));

  function genAddr10Bits (transactionInfo: bundle_sonicsPciRequest)
    return std_logic_vector is
  begin
    case transactionInfo.accessSpace is
      when "00" =>
        return "00";
      when "01" =>
        --  io does a full byte address on pci, stupid io 
        if transactionInfo.firstByteEnable(0) = '1' then
          return "00";
        elsif transactionInfo.firstByteEnable(1) = '1' then
          return "01";
        elsif transactionInfo.firstByteEnable(2) = '1' then
          return "10";
        else
          return "11";
        end if;
      when "10" =>
        return "00";
      when others =>
        return "01";
    end case;
  end function;
begin  -- arch

  sonicsRequest(1).accessSpace <= accessSpace1;
  sonicsRequest(0).accessSpace <= accessSpace0;
  sonicsRequest(1).firstByteEnable <= firstByteEnable1;
  sonicsRequest(0).firstByteEnable <= firstByteEnable0;
  ad(1 downto 0) <= core2pad_pciBus.ad(1 downto 0);
  ad(31 downto 2) <= (others => '0');
  
  currentRequest <= 0 when in1(1) = '0' else 1;
  
  p1: process( sonicsRequest )
  begin  -- process p1
    core2pad_pciBus.ad(1 downto 0) <= genAddr10Bits(sonicsRequest(currentRequest));
  end process p1; 

end arch;
