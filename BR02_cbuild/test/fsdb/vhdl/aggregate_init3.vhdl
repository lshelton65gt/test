-- Test array aggregate initialization with a record array with positional
-- association and OTHERS.
library ieee;
use ieee.std_logic_1164.all;
entity aggregate_init3 is
  port (in1 : in  integer range 0 to 6;
        out1: out boolean;
        out2: out std_logic_vector(3 downto 0));
end aggregate_init3;

architecture rtl of aggregate_init3 is
  type ctype is record
    faddr : std_logic_vector(3 downto 0);
    en    : boolean;
  end record;
  type sconfig is array (Natural Range <> ) of ctype;
  constant configv : ctype := ((others => '0'), false);
  constant cfg_std : sconfig(0 to 6) := (
    ("0111", true),
    ("1000", false),
    others => configv
    );
begin
  out1 <= cfg_std(in1).en;
  out2 <= cfg_std(in1).faddr;
end rtl;
