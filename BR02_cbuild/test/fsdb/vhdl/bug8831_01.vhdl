-- May 2008
-- This file tests the vhdlCase preserve switch. The record myrec contains fields
-- with names in upper case. The fsdb file shows the names correctly.
-- 
library ieee;
use ieee.std_logic_1164.all;

package mypack is
  type myrec is record
                  A : std_logic;
                  B : std_logic;
                end record;
end mypack;

library ieee;
use ieee.std_logic_1164.all;
library work;
use work.mypack.all;

entity bug8831_01 is
  port (
    clk : in  std_logic;
    rst : in  std_logic;
    i   : in  myrec;
    o   : out myrec);
end bug8831_01;

architecture arch of bug8831_01 is

  signal r : myrec := ('0', '0');
  
begin

  p1: process (clk, rst)
  begin  -- process p1
    if rst = '0' then  
      o.A <= '0';
      o.B <= '0';
    elsif clk'event and clk = '1' then  
      r <= i;
      o <= r;
    end if;
  end process p1;
  
 
end arch;

