-- Test basic population and manipulation of record fields

entity recordclock is
  port (
    clk,rst : in bit;
    in1  : in  bit_vector(7 downto 0)
    ;in2  : in  bit_vector(7 downto 0)
    ;outp : out bit_vector(7 downto 0)
    );
end;

architecture a of recordclock is

type rec is record
    r1 : bit_vector(7 downto 0);
    r2 : bit_vector(7 downto 0);
    r3, r4 : bit;
end record;

signal s1: rec;

begin

  s1.r3 <= clk;
  s1.r4 <= rst;
  
  u1: process (s1.r3,s1.r4)
  begin  -- process u1
    if s1.r4 = '0' then              -- asynchronous reset (active low)
      s1.r1 <= (others=>'0');
      s1.r2 <= (others=>'0');
    elsif s1.r3'event and s1.r3 = '1' then  -- rising clock edge
      s1.r1 <= in1;
      s1.r2 <= in2;
      outp <= s1.r1 and s1.r2;
    end if;
  end process u1;
end a;
