-- Test 3-d array creation when handling an array of records.  The gold file
-- was generated with MTI because Aldec segfaulted.
package p is
  type mem is array (0 to 7) of bit_vector(7 downto 0);
  type rec is record
                r1 : mem;
                r2 : bit_vector(3 downto 0);
              end record;
  type bundlerec is array(0 to 1) of rec;
end p;

use work.p.all;

entity dut is
  port ( recinport : in rec;            -- carbon observeSignal
         recoutport: out rec;           -- carbon observeSignal
         clk: in bit
       );
end;

architecture a of dut is
  signal arrayrec : bundlerec;          -- carbon observeSignal
begin

  p1: process (clk)
  begin  -- process p1
    if clk'event and clk = '1' then  -- rising clock edge
      arrayrec(0) <= recinport;
    end if;
  end process p1;
  
  p2: process (clk)
  begin  -- process p1
    if clk'event and clk = '0' then  -- rising clock edge
      arrayrec(1) <= arrayrec(0);
    end if;
  end process p2;
  
  p3: process (clk)
  begin  -- process p1
    if clk'event and clk = '1' then  -- rising clock edge
      recoutport <= arrayrec(1);
    end if;
  end process p3;
  
end a;


use work.p.all;

entity record10 is
  port (
    clk : in bit;
    recinport_r1  : in  bit_vector(63 downto 0);
    recinport_r2  : in  bit_vector(3 downto 0);
    recoutport_r1 : out bit_vector(63 downto 0);
    recoutport_r2 : out bit_vector(3 downto 0));
end record10;

architecture arch of record10 is
  component dut is
    port ( recinport : in rec
         ; recoutport: out rec
         ; clk: in bit
         );
  end component;

begin

  c1 : dut port map (
    clk              => clk,
    recinport.r1(0)  => recinport_r1(63 downto 56),
    recinport.r1(1)  => recinport_r1(55 downto 48),
    recinport.r1(2)  => recinport_r1(47 downto 40),
    recinport.r1(3)  => recinport_r1(39 downto 32),
    recinport.r1(4)  => recinport_r1(31 downto 24),
    recinport.r1(5)  => recinport_r1(23 downto 16),
    recinport.r1(6)  => recinport_r1(15 downto 8),
    recinport.r1(7)  => recinport_r1(7 downto 0),
    recinport.r2     => recinport_r2,

    recoutport.r1(0) => recoutport_r1(63 downto 56),
    recoutport.r1(1) => recoutport_r1(55 downto 48),
    recoutport.r1(2) => recoutport_r1(47 downto 40),
    recoutport.r1(3) => recoutport_r1(39 downto 32),
    recoutport.r1(4) => recoutport_r1(31 downto 24),
    recoutport.r1(5) => recoutport_r1(23 downto 16),
    recoutport.r1(6) => recoutport_r1(15 downto 8),
    recoutport.r1(7) => recoutport_r1(7 downto 0),
    recoutport.r2    => recoutport_r2);

end arch;
