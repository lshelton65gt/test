// Testing carbonDumpSizeLimit

#include "libdumpsizelimit2.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>

int main(int argc, char **argv)
{
  char fsdb_file_name[100];

  CarbonObjectID *obj = carbon_dumpsizelimit2_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID    *clk = carbonFindNet(obj, "dumpsizelimit2.clk");
  CarbonNetID    *rst = carbonFindNet(obj, "dumpsizelimit2.rst");

  CarbonUInt32 val;
  CarbonTime   t    = 0;
  bool         def  = (argc != 5);
  CarbonUInt32 idx  = def ? 0 : std::atoi(argv[2]);
  CarbonUInt32 size = def ? 0 : std::atoi(argv[4]);

  if(def)
    sprintf(fsdb_file_name, "dumpsizelimit2.run.cds.fsdb");
  else
    sprintf(fsdb_file_name, "dumpsizelimit2.run%d.cds.fsdb", idx);

  CarbonWaveID *wave = carbonWaveInitFSDB(obj, fsdb_file_name, e1ns);
  if(!def)
    carbonDumpSizeLimit(wave, size);
  carbonDumpVars(wave, 0, "dumpsizelimit2");

  val = 1;
  carbonDeposit(obj, rst, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 0;
  carbonDeposit(obj, rst, &val, 0);

  for(int i=0; i<5; i++) {
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  carbonDestroy(&obj);
  return 0;
}
