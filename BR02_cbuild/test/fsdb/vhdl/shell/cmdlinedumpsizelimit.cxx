// Testing -waveformDumpSizeLimit command line switch and carbonDumpSizeLimit() override

#include "libcmdlinedumpsizelimit.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>

int main(int argc, char **argv)
{
  char fsdb_file_name[100];

  CarbonObjectID *obj = carbon_cmdlinedumpsizelimit_create(eCarbonFullDB, eCarbon_NoFlags);
  CarbonNetID    *clk = carbonFindNet(obj, "cmdlinedumpsizelimit.clk");
  CarbonNetID    *rst = carbonFindNet(obj, "cmdlinedumpsizelimit.rst");

  CarbonUInt32 val;
  CarbonTime   t    = 0;
  bool         def  = (argc != 5);
  CarbonUInt32 idx  = def ? 0 : std::atoi(argv[2]);
  CarbonUInt32 size = def ? 0 : std::atoi(argv[4]);

  if(def)
    sprintf(fsdb_file_name, "cmdlinedumpsizelimit.run.cds.fsdb");
  else
    sprintf(fsdb_file_name, "cmdlinedumpsizelimit.run%d.cds.fsdb", idx);

  CarbonWaveID *wave = carbonWaveInitFSDB(obj, fsdb_file_name, e1ns);
  if(!def)
    carbonDumpSizeLimit(wave, size);
  carbonDumpVars(wave, 0, "cmdlinedumpsizelimit");

  val = 1;
  carbonDeposit(obj, rst, &val, 0);
  val = 0;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 1;
  carbonDeposit(obj, clk, &val, 0);
  carbonSchedule(obj, t++);
  val = 0;
  carbonDeposit(obj, rst, &val, 0);

  for(int i=0; i<5; i++) {
    val = 0;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
    val = 1;
    carbonDeposit(obj, clk, &val, 0);
    carbonSchedule(obj, t++);
  }

  carbonDestroy(&obj);
  return 0;
}
