-- Testcase for carbonDumpSizeLimit

library ieee;
use ieee.std_logic_1164.all;

package types is
 constant m:         integer := 1;      
 subtype  scalar1 is std_logic;                  -- std_logic
 subtype  scalar2 is integer;                    -- integer
 subtype  scalar3 is character;                  -- character
 subtype  array10 is std_logic_vector(0 to m);   -- std_logic_vector 
 type array11 is array (m downto 0) of scalar1;  -- array of std_logic 
 type array12 is array (0 to     m) of scalar2;  -- array of integer 
 type array13 is array (m downto 0) of scalar3;  -- array of character 
 type array20 is array (0 to     m) of array10;  -- array of std_logic_vector
 type array21 is array (m downto 0) of array11;  -- 2D array of std_logic
 type array22 is array (0 to     m) of array12;  -- 2D array of integer
 type array23 is array (m downto 0) of array13;  -- 2D array of character
 type recordb is record  -- bottom record
  item1b  : scalar1;     -- std_logic
  item2b  : scalar2;     -- integer
  item3b  : scalar3;     -- character
  item10b : array10;     -- std_logic_vector
  item11b : array11;     -- array of std_logic
  item12b : array12;     -- array of integer
  item13b : array13;     -- array of character
  item20b : array20;     -- array of std_logic_vector
  item21b : array21;     -- 2D array of std_logic
  item22b : array22;     -- 2D array of integer
  item23b : array23;     -- 2D array of character
 end record;
 type arrayrecord1b is array (0     to m) of recordb;       -- array of recordb
 type arrayrecord2b is array (m downto 0) of arrayrecord1b; -- 2D array of recordb
  type recordt is record       -- top record
   item1t     : scalar1;       -- std_logic
   item2t     : scalar2;       -- integer
   item3t     : scalar3;       -- character
   item10t    : array10;       -- std_logic_vector
   item11t    : array11;       -- array of std_logic
   item12t    : array12;       -- array of integer
   item13t    : array13;       -- array of character
   item20t    : array20;       -- array of std_logic_vector
   item21t    : array21;       -- 2D array of std_logic
   item22t    : array22;       -- 2D array of integer
   item23t    : array23;       -- 2D array of character
   itembrec1t : recordb;       -- bottom record
   itemarec1t : arrayrecord1b; -- array of bottom record
   itemarec2t : arrayrecord2b; -- 2D array of bottom record
  end record;
  type arrayrecord1t is array (m downto 0) of recordt;       -- array of recordt
  type arrayrecord2t is array (0 to     m) of arrayrecord1t; -- 2D array of recordt
end types;

library ieee;
use ieee.std_logic_1164.all;
use work.types.all;

entity dumpsizelimit2 is port (
    clk       : in  std_logic;
    rst       : in  std_logic;
    pi_s1     : in  scalar1;       -- std_logic 
    pi_s2     : in  scalar2;       -- integer 
    pi_s3     : in  scalar3;       -- character
    pi_a10    : in  array10;       -- std_logic_vector 
    pi_a11    : in  array11;       -- array of std_logic 
    pi_a12    : in  array12;       -- array of integer 
    pi_a13    : in  array13;       -- array of character
    pi_a20    : in  array20;       -- array of std_logic_vector
    pi_a21    : in  array21;       -- 2D array of std_logic
    pi_a22    : in  array22;       -- 2D array of integer
    pi_a23    : in  array23;       -- 2D array of character
    pi_brec1  : in  recordb;       -- bottom record
    pi_abrec1 : in  arrayrecord1b; -- array of bottom record
    pi_abrec2 : in  arrayrecord2b; -- 2D array of bottom record
    pi_trec1  : in  recordt;       -- top record
    pi_atrec1 : in  arrayrecord1t; -- array of top record
    pi_atrec2 : in  arrayrecord2t; -- 2D array of top record
    po_s1     : out scalar1;       -- std_logic
    po_s2     : out scalar2;       -- integer
    po_s3     : out scalar3;       -- character
    po_a10    : out array10;       -- std_logic_vector
    po_a11    : out array11;       -- array of std_logic
    po_a12    : out array12;       -- array of integer
    po_a13    : out array13;       -- array of character
    po_a20    : out array20;       -- array of std_logic_vector
    po_a21    : out array21;       -- 2D array of std_logic
    po_a22    : out array22;       -- 2D array of integer
    po_a23    : out array23;       -- 2D array of character
    po_brec1  : out recordb;       -- bottom record
    po_abrec1 : out arrayrecord1b; -- array of bottom record
    po_abrec2 : out arrayrecord2b; -- 2D array of bottom record
    po_trec1  : out recordt;       -- top record
    po_atrec1 : out arrayrecord1t; -- array of top record
    po_atrec2 : out arrayrecord2t);-- 2D array of top record
end dumpsizelimit2;

architecture rtl of dumpsizelimit2 is
  signal imm_s1     : scalar1;       -- carbon observeSignal
  signal imm_s2     : scalar2;       -- carbon observeSignal
  signal imm_s3     : scalar3;       -- carbon observeSignal
  signal imm_a10    : array10;       -- carbon observeSignal
  signal imm_a11    : array11;       -- carbon observeSignal
  signal imm_a12    : array12;       -- carbon observeSignal
  signal imm_a13    : array13;       -- carbon observeSignal
  signal imm_a20    : array20;       -- carbon observeSignal
  signal imm_a21    : array21;       -- carbon observeSignal
  signal imm_a22    : array22;       -- carbon observeSignal
  signal imm_a23    : array23;       -- carbon observeSignal
  signal imm_brec1  : recordb;       -- carbon observeSignal
  signal imm_abrec1 : arrayrecord1b; -- carbon observeSignal
  signal imm_abrec2 : arrayrecord2b; -- carbon observeSignal
  signal imm_trec1  : recordt;       -- carbon observeSignal
  signal imm_atrec1 : arrayrecord1t; -- carbon observeSignal
  signal imm_atrec2 : arrayrecord2t; -- carbon observeSignal
begin  -- rtl
  com: process (pi_s1, pi_s2, pi_s3,
                pi_a10, pi_a11, pi_a12, pi_a13,
                pi_a20, pi_a21, pi_a22, pi_a23,
                pi_brec1, pi_abrec1, pi_abrec2,
                pi_trec1, pi_atrec1, pi_atrec2)
  begin
    imm_s1     <= pi_s1;
    imm_s2     <= pi_s2;
    imm_s3     <= pi_s3;
    imm_a10    <= pi_a10;
    imm_a11    <= pi_a11;
    imm_a12    <= pi_a12;
    imm_a13    <= pi_a13;
    imm_a20    <= pi_a20;
    imm_a21    <= pi_a21;
    imm_a22    <= pi_a22;
    imm_a23    <= pi_a23;
    imm_brec1  <= pi_brec1;
    imm_abrec1 <= pi_abrec1;
    imm_abrec2 <= pi_abrec2;
    imm_trec1  <= pi_trec1;
    imm_atrec1 <= pi_atrec1;
    imm_atrec2 <= pi_atrec2;
  end process com;
  seq: process (clk, rst)
    variable init_scalar1       : scalar1       := '0';                      -- carbon observeSignal
    variable init_scalar2       : scalar2       := 0;                        -- carbon observeSignal
    variable init_scalar3       : scalar3       := '0';                      -- carbon observeSignal
    variable init_array10       : array10       := (others => init_scalar1); -- carbon observeSignal
    variable init_array11       : array11       := (others => init_scalar1); -- carbon observeSignal
    variable init_array12       : array12       := (others => init_scalar2); -- carbon observeSignal
    variable init_array13       : array13       := (others => init_scalar3); -- carbon observeSignal
    variable init_array20       : array20       := (others => init_array10); -- carbon observeSignal
    variable init_array21       : array21       := (others => init_array11); -- carbon observeSignal
    variable init_array22       : array22       := (others => init_array12); -- carbon observeSignal
    variable init_array23       : array23       := (others => init_array13); -- carbon observeSignal
    variable init_recordb       : recordb       := (init_scalar1, init_scalar2, init_scalar3,
                                                    init_array10, init_array11, init_array12, init_array13,
                                                    init_array20, init_array21, init_array22, init_array23);
                                                                                   -- carbon observeSignal
    variable init_arrayrecord1b : arrayrecord1b := (others => init_recordb);       -- carbon observeSignal
    variable init_arrayrecord2b : arrayrecord2b := (others => init_arrayrecord1b); -- carbon observeSignal
    variable init_recordt       : recordt       := (init_scalar1, init_scalar2, init_scalar3,
                                                    init_array10, init_array11, init_array12, init_array13,
                                                    init_array20, init_array21, init_array22, init_array23,
                                                    init_recordb, 
                                                    init_arrayrecord1b, init_arrayrecord2b);                                                                                                                -- carbon observeSignal
    variable init_arrayrecord1t : arrayrecord1t := (others => init_recordt);       -- carbon observeSignal
    variable init_arrayrecord2t : arrayrecord2t := (others => init_arrayrecord1t); -- carbon observeSignal
  begin  -- process seq
    if (rst = '1') then
      po_s1     <= init_scalar1;
      po_s2     <= init_scalar2;
      po_s3     <= init_scalar3;
      po_a10    <= init_array10;
      po_a11    <= init_array11;
      po_a12    <= init_array12;
      po_a13    <= init_array13;
      po_a20    <= init_array20;
      po_a21    <= init_array21;
      po_a22    <= init_array22;
      po_a23    <= init_array23;
      po_brec1  <= init_recordb;
      po_abrec1 <= init_arrayrecord1b;
      po_abrec2 <= init_arrayrecord2b;
      po_trec1  <= init_recordt;
      po_atrec1 <= init_arrayrecord1t;
      po_atrec2 <= init_arrayrecord2t;
    elsif (clk'event and clk = '1') then
      po_s1     <= imm_s1;
      po_s2     <= imm_s2;
      po_s3     <= imm_s3;
      po_a10    <= imm_a10;
      po_a11    <= imm_a11;
      po_a12    <= imm_a12;
      po_a13    <= imm_a13;
      po_a20    <= imm_a20;
      po_a21    <= imm_a21;
      po_a22    <= imm_a22;
      po_a23    <= imm_a23;
      po_brec1  <= imm_brec1;
      po_abrec1 <= imm_abrec1;
      po_abrec2 <= imm_abrec2;
      po_trec1  <= imm_trec1;
      po_atrec1 <= imm_atrec1;
      po_atrec2 <= imm_atrec2;
    end if;   
  end process seq;
end rtl;


