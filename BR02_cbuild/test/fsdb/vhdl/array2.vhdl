library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity array2 is
  port (
    clk,rst  : in  std_logic;
    in1      : in  std_logic_vector(3 downto 0);
    del_out  : out std_logic_vector(2 downto 0);
    out1     : out std_logic_vector(3 downto 0);
    out2     : out std_logic_vector(3 downto 0));
end;

architecture arch of array2 is
  type bf is array(7 downto 0) of std_logic_vector(3 downto 0);  
  signal buffer1 : bf := (others => (others => '0'));  -- carbon observeSignal
begin

  p1: process(rst, clk, in1)
    variable counter : integer := 0;
    variable delay : integer := 0;
  begin

    if(rst = '1') then
      out1 <= (others => '0');
      counter := 0;
      out2 <= (others => '0');
    elsif (clk'event and clk = '1') then 
      if(counter = 7) then
        delay := 0;
      else
        delay :=  counter + 1;
      end if;
      
      buffer1(counter) <= in1;
      out1 <= in1;
      out2 <= buffer1(delay);

      if(counter = 7) then
        counter := 0;
      else
        counter := counter + 1;
      end if;
      
    end if;

    del_out <= conv_std_logic_vector(delay, 3);
  end process;
  
end arch;

