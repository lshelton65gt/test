-- Test for nested record 2a.
library ieee;

use ieee.std_logic_1164.all;

package p is

  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;

  type rec_sl is
    record
      r2 : std_logic_vector(3 downto 0);
      r3 : std_logic;
    end record;

  type rec_int is
    record
      sl : rec_sl;
      i1 : integer;
      i2 : integer;
    end record;

  type rec_cb is
    record
      ch1,ch2 : character;
      b1 : bit;
      bv : bit_vector(3 downto 0);
    end record;

  type rec_all is
    record
      r1 : rec_int;
      r2 : rec_cb;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_nested2adut is
  port ( recinport : in rec
         ; intinport : in  integer
         ; clk: in std_logic
         );
end rec_nested2adut;

architecture a of rec_nested2adut is
  procedure proc ( variable iorec : inout rec_all;
                   b1 : in std_logic ) is
    variable xi : integer;
    variable xch : character;
  begin
    iorec.r1.sl.r2 := iorec.r1.sl.r3 & iorec.r1.sl.r2(2 downto 1) & b1;
    iorec.r1.sl.r3 := iorec.r1.sl.r2(2);
  end proc;
  
begin
  p1: process (clk, intinport)
    variable temp : rec_all;
  begin
    if clk'event and clk = '1' then
      proc(temp, recinport.r1(3));
    end if;
  end process p1;
end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_nested2a is
  port (
    clk            : in  std_logic;
    num            : in  integer;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0));
end rec_nested2a;

architecture a of rec_nested2a is
component rec_nested2adut is
  port ( recinport : in rec
         ; intinport : in integer
         ; clk: in std_logic
         );
end component rec_nested2adut;
begin
  dut : rec_nested2adut port map (
    clk            => clk,
    intinport      => num,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2);
end a;
