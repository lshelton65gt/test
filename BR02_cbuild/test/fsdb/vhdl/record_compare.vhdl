-- Test record equality/inequality operators:
--   record to record
--   record to named association
--   record to positional association
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is record
                r1 : std_logic_vector(7 downto 0);
                r2 : std_logic_vector(3 downto 0);
              end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity dut is
  port ( recinport : in rec
         ; clk: in std_logic
         ; comparevalue : out boolean
         ; comparevaluen : out boolean
         );
end dut;

architecture a of dut is
  constant constrec : rec := ("11001100", "1100");
begin
  comparevalue <= recinport = (r2 => "1010", r1 => "01010101") or boolean(constrec = ("11001100", "1100"));
  comparevaluen <= recinport /= recinport or recinport /= ("01010101", "1010");
end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity record_compare is
  port (
    rip_r1    : in  std_logic_vector(7 downto 0);
    rip_r2    : in  std_logic_vector(3 downto 0);
    comparevalue : out boolean;
    comparevaluen : out boolean;
    clk        : in  std_logic);
end record_compare;

architecture a of record_compare is
component dut is
  port ( recinport : in rec
         ; clk: in std_logic
         ; comparevalue : out boolean
         ; comparevaluen : out boolean
         );
end component dut;

begin

u1: dut port map (
  comparevalue => comparevalue,
  comparevaluen => comparevaluen,
  recinport.r1 => rip_r1,
  recinport.r2 => rip_r2,
  clk => clk);

end a;
