-- Test array aggregate initialization of 2 to 33 array range of records.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity aggregate_init19 is
  port (d0 : in std_logic;
        d1 : in std_logic;
        d2 : in std_logic;
        d3 : in std_logic;
        addr : in std_logic_vector(4 downto 0);
        out1: out std_logic);
end aggregate_init19;

architecture rtl of aggregate_init19 is
  type ctype is record
    data : std_logic;
    en : std_logic;
  end record;
  type sconfig is array (Natural Range <> ) of ctype;
  constant c0 : ctype := ('1', '0');
  constant c1 : ctype := ('0', '1');
  constant c2 : ctype := ('1', '0');
  constant c3 : ctype := ('0', '1');
  constant c4 : integer := 5;
  constant c5 : integer := 2;
  constant c6 : integer := c4 + c5;
  signal mem : sconfig(2 to 33) := (4 => c2, 3 => c1, 2 => c0,
                                    others => c3);  -- carbon observeSignal
  signal rec : ctype;                   -- carbon observeSignal
begin

  rec <= (d3, '1');
  mem <= ( c4 downto c5+1 => (d2, '0'), 10#2# | c6 | c4*c5 => (d1, '1'), 32 to 33 => (d0, '0'), others => rec);

  process(addr, mem)
    variable idx : integer;
  begin
    idx := to_integer(unsigned(addr));
    out1 <= mem(idx+2).data;
  end process;
end rtl;
