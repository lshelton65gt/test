-- March 2008
-- This file tests the fsdb dump of enumeration type.
-- The second enumeration signal was created to confirm
-- that both enum signals have the same fsdb type.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity enum_02 is
   port (in1  : in bit_vector(0 to 2);
         out1 : out bit_vector(0 to 2));
end enum_02;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

architecture arch of enum_02 is
   type days is (MON, TUE, WED, THR, FRI, SAT, SUN, WOW);
   type colors is (red, green, blue, yellow);
   subtype col_range is colors range  green to blue;
  signal enumSignal     : days := wow;      -- carbon observeSignal
  signal enumColors     : colors := red;    -- carbon observeSignal
  signal colRange      : col_range := blue; -- carbon observeSignal 
  signal enumSignalCopy : days := wow;      -- carbon observeSignal
begin


  p1: process (in1)
    variable en_var : days;
  begin  -- process p1
    
    case in1 is
      when "000" =>
        en_var := mon;
        enumColors <= red;
      when "001" =>
        en_var := tue;
        enumColors <= green;
        colRange <= green;
      when "010" =>
        en_var := wed;
        enumColors <= blue;
        colRange <= blue;
      when "011" =>
        en_var := thr;
        enumColors <= yellow;
      when "100" =>
        en_var := fri;
      when "101" =>
        en_var := sat;
      when "110" =>
        en_var := sun;
  when others => 
        en_var := wow;
    end case;
    enumSignal <= en_var;
  end process p1;
    
  p2: process (enumSignal)
  begin
    case enumSignal is
      when  mon =>
        out1 <= "001";
      when  tue =>
        out1 <= "010";
      when  wed =>
        out1 <= "011";
      when  thr =>
        out1 <= "100";
      when  fri =>
        out1 <= "101";
      when  sat =>
        out1 <= "110";
      when  sun =>
        out1 <= "111";
      when others => 
        out1 <= "000";
    end case;
  end process p2;

  enumSignalCopy <= enumSignal;
end arch;


