-- Test port arrayed record field access
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package p is

type tim_logic_vector is array (integer range <>) of bit;
  
type rec is record
    r1 : tim_logic_vector(7 downto 0);
    r2 : tim_logic_vector(3 downto 0);
end record;

type bundlerec is array(natural range<>) of rec;

end p;

use work.p.all;

entity record11 is
  port ( recinport : in rec
       ; recoutport1: out rec
       ; recoutport2: out rec
       ; clk: in bit
       );
end record11;

architecture a of record11 is
  signal arrayrec : bundlerec(0 to 1); -- carbon observeSignal
begin

 p1: process (clk)
 begin  -- process p1
   if clk'event and clk = '1' then  -- rising clock edge
       arrayrec(0).r1 <= recinport.r1;
       arrayrec(0).r2 <= recinport.r2;
   end if;
 end process p1;

p2: process (clk)
begin  -- process p1
  if clk'event and clk = '1' then  -- rising clock edge
      arrayrec(1).r1 <= recinport.r1;
      arrayrec(1).r2 <= recinport.r2;
  end if;
end process p2;

 p3: process( arrayrec )
 begin  -- process
     recoutport1 <= arrayrec(0);
     recoutport2.r1 <= arrayrec(1).r1;
     recoutport2.r2 <= arrayrec(1).r2;
 end process;

end a;
