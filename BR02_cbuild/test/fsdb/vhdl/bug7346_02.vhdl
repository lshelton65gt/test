-- May 2007
-- A signal mem_data is array of records declared in the package.
-- An entity uses the elements of the mem_data signal in the
-- left and right side assignment.
-- 
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is

  type complex_fixed_type is
    record
      real1    : integer;
      imag1    : integer;
    end record;

  type arr_rec is array (0 to 1) of complex_fixed_type;
  
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;


package FFT_CORE_SIG_PKG is
  signal mem_data       : arr_rec;
end FFT_CORE_SIG_PKG;

library ieee;
use ieee.std_logic_1164.all;
use work.FFT_CORE_SIG_PKG.all;
entity bug7346_02 is
  port (
    clock      : in  std_logic;
    out1       : out integer;
    out2       : out integer;
    out3       : out integer;
    out4       : out integer);
end bug7346_02;

architecture arch of bug7346_02 is
  signal temp1 : integer := 10; -- carbon observeSignal
  signal temp2 : integer := 20; -- carbon observeSignal
begin
  main: process (clock, mem_data)
  begin 
    if clock'event and clock = '1' then
      mem_data(0).real1 <= temp1;
      mem_data(0).imag1 <= temp2;
      mem_data(1).real1 <= temp1;
      mem_data(1).imag1 <= temp2;
    end if;

    out1 <= mem_data(0).real1;
    out2 <= mem_data(0).imag1;
    out3 <= mem_data(1).real1;
    out4 <= mem_data(1).imag1;
     
  end process;
end;
