-- September 07
-- This file tests an array of nested record used as a port for sub entity.
-- The nested record (not whole record) is used in the sub entity.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is
  type t_rec is
    record
      a : integer;
      b : integer;
    end record;

  type t_second is
    record
      data : t_rec;
    end record;

  type arr_rec is array (0 to 1) of t_second;
  
end FIXED_POINT_PKG;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity sub is
  port (
    insub   : in  t_rec;
    outsub  : out t_rec);
end sub;

architecture sub_arch of sub is
begin  -- sub_arch

  outsub <= insub;
end sub_arch;

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity bug7676_01 is
  port (
    in1  : in  integer;
    in2  : in  integer;
    out1 : out integer;
    out2 : out integer);
end bug7676_01;

architecture arch of bug7676_01 is
  signal comb : arr_rec; -- carbon observeSignal
  signal temp2 : t_rec; -- carbon observeSignal
begin
  
  comb(0).data.a <= in1;
  comb(0).data.b <= in2;

  
  s1 : entity work.sub 
    port map(
      insub  => comb(0).data,           -- this line is tested
      outsub => comb(1).data );         -- this line is tested

  temp2 <= comb(1).data;
  
  out1 <= temp2.a;
  out2 <= temp2.b;
  

end;
