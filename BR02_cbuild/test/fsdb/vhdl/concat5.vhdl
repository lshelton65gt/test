-- Tests concat of two records returned by two function calls.
library ieee;
use ieee.std_logic_1164.all;

entity concat5 is
  
  port (
    in1  : in  std_logic;
    in2  : in  std_logic;
    in3  : in  std_logic;
    in4  : in  std_logic;
    out1 : out std_logic;
    out2 : out std_logic);

end concat5;

architecture arch of concat5 is

  type rec is record
                f1         : std_logic;
                f2         : std_logic;
              end record;

  function constructRecord (in1 : std_logic; in2 : std_logic) return rec is
    variable frec : rec;
  begin  -- constructRecord
    frec := (in1, in2);
    return frec;
  end constructRecord;
  
  type recvec is array (natural range<>) of rec;
  signal rv : recvec(1 downto 0);
  
begin  -- arch

  rv <= constructRecord(in1, in2) & constructRecord(in3, in4);

  out1 <= rv(0).f1 xor rv(1).f1;
  out2 <= rv(0).f2 xor rv(1).f2;

end arch;
