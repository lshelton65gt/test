-- December 2007
-- The function max_packet returns an array of records, which contains an array
-- of records.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use std.textio.all;

package pkg_test is
    type  nested_rec is
      record
        b: integer;  
      end record; 

    type arr_rec is array (5 to 6) of nested_rec;
    type top_rec is
       record 
         a      : integer;
         b_inst : arr_rec;
       end record;

    type packet is array (2 to 3) of top_rec;
       
    function max_packet(a : integer; b : integer;
                        c : integer; d : integer;
                        e : integer; f : integer) return packet;
    
end pkg_test;

package body pkg_test is 
    
    function max_packet(a : integer; b : integer;
                        c : integer; d : integer;
                        e : integer; f : integer) return packet
    is
      variable Ret : packet;
    begin
          Ret(2).a := a;
          Ret(2).b_inst(5).b := b;
          Ret(2).b_inst(6).b := c;
          Ret(3).a := d;
          Ret(3).b_inst(5).b := e;
          Ret(3).b_inst(6).b := f;
       return Ret;
    end;
    
end pkg_test;    



library ieee;
use ieee.std_logic_1164.all;
use work.pkg_test.all;

entity bug8155_01 is
    port (clk	   : in	 std_logic;
          rst      : in  std_logic;
	  input_a  : in	 integer;
	  input_b  : in	 integer;
	  input_c  : in	 integer;
	  input_d  : in	 integer;
	  input_e  : in	 integer;
	  input_f  : in	 integer;
	  output_a : out integer;
	  output_b : out integer;
	  output_c : out integer;
	  output_d : out integer;
	  output_e : out integer;
          output_f : out integer);
end bug8155_01;

architecture behav of bug8155_01 is
  signal result  : packet; -- carbon observeSignal
begin

  process(clk, rst)
  begin
    if rst = '1' then
      result(2).a <= 0;
      result(2).b_inst(5).b <= 0;
      result(2).b_inst(6).b <= 0;
      result(3).a <= 0;
      result(3).b_inst(5).b <= 0;
      result(3).b_inst(6).b <= 0;
    elsif (clk = '1' and clk'event) then
       result <= max_packet(input_a, input_b, input_c,
                            input_d, input_e, input_f);                -- this line is tested
    end if;
  end process;

  output_a <= result(2).a;
  output_b <= result(2).b_inst(5).b;
  output_c <= result(2).b_inst(6).b;
  output_d <= result(3).a;
  output_e <= result(3).b_inst(5).b;
  output_f <= result(3).b_inst(6).b;
              
end behav;
