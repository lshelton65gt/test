-- March 2008
-- This file tests fsdb dump of vhdl block and generate construct.
-- 

entity sub is
  
  generic (
    num_bits : integer := 4);

  port (
    sub_in  : in  bit_vector(num_bits -1 downto 0);
    sub_out : out bit_vector(num_bits -1 downto 0));
end sub;

architecture arch_sub of sub is
  signal sub_temp : bit_vector(num_bits -1 downto 0);
begin
  sub_temp <= sub_in;
  sub_out <= sub_temp;
end arch_sub;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

entity bug8499_01 is
  port (
     top_in1  : in   bit_vector(7 downto 0);
     top_out1 : out  bit_vector(7 downto 0);
     top_in2  : in   bit_vector(7 downto 0);
     top_out2 : out  bit_vector(7 downto 0);
     top_in3  : in   bit_vector(7 downto 0);
     top_out3 : out  bit_vector(7 downto 0));
    
end bug8499_01;


architecture top_arch of bug8499_01 is
  component sub
  generic (
    num_bits : integer := 4);
  port (
    sub_in  : in  bit_vector(num_bits -1 downto 0);
    sub_out : out bit_vector(num_bits -1 downto 0));
  end component;
  
  constant num_bits_top : integer := 8;
begin

  gen_loop: for i in 0 to num_bits_top-1  generate
    signal gen1 : bit;          
  begin
    gen1 <= top_in1(i);
    top_out1(i) <= gen1;
  end generate gen_loop;


  myblock : block
    signal block1 : bit_vector(num_bits_top -1 downto 0);
  begin
    block1 <= top_in2;

    my_inner_block: block
      signal block2 : bit_vector(num_bits_top -1 downto 0);
    begin
      block2 <= block1;
      top_out2 <= block2;
    end block my_inner_block;
  end block myblock;
  

  ent_gen: for i_ent in 1 downto 0 generate
    signal gen_temp1 : bit_vector(num_bits_top/2 - 1 downto 0);  
    signal gen_temp2 : bit_vector(num_bits_top/2 - 1 downto 0);  
  begin

      gen_temp1 <=  top_in3((i_ent+1)*(num_bits_top/2) - 1 downto i_ent*num_bits_top/2);
      i_sub: sub
        port map (
          sub_in  => gen_temp1,
          sub_out => gen_temp2
        );
      
      top_out3((i_ent+1)*(num_bits_top/2) - 1 downto i_ent*num_bits_top/2) <= gen_temp2;
  end generate;

end top_arch;




