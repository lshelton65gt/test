library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug6374_2 is
  
  port (
    in1  : in  std_logic_vector(3 downto 0);
    in2  : in  std_logic_vector(3 downto 0);
    in3  : in  std_logic_vector(3 downto 0);
    in4  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0);
    out2 : out std_logic_vector(3 downto 0));

end bug6374_2;

architecture arch of bug6374_2 is

  type rec is record
                f1 : std_logic_vector(3 downto 0);
                f2 : std_logic_vector(3 downto 0);
              end record;

  function "*"(arg1 : rec; arg2 : rec) return rec is
    variable ret : rec := (others => (others => '0'));
  begin  -- *
    ret := ((arg1.f1 and arg2.f1), (arg1.f2 and arg2.f2));
    return ret;
  end "*";

begin  -- arch

  process (in1, in2, in3, in4)
    variable arg1 : rec := (others => (others => '0'));
    variable arg2 : rec := (others => (others => '0'));
    variable res  : rec := (others => (others => '0'));
  begin  -- process

    arg1 := (in1, in2);
    arg2 := (in3, in4);

    res := arg1 * arg2;

    out1 <= res.f1;
    out2 <= res.f2;
    
  end process;

end arch;
