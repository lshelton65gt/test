library ieee;
use ieee.std_logic_1164.all;

entity bug7669 is
  
  port (
    in1  : in  std_logic_vector(4 downto 0);
    clk  : in  std_logic;
    rst  : in  std_logic;
    out1 : out std_logic_vector(4 downto 0));

end bug7669;

architecture arch of bug7669 is

  type rec is record
                f1 : std_logic_vector(3 downto 0);
                f2 : std_logic;
              end record;
  type recary is array (natural range <>) of rec;

  type recaryrec is record
                      fary1 : recary(7 downto 0);
                    end record;

  type recaryrecary is array (natural range <>) of recaryrec;

  type recaryrecaryrec is record
                            fary2 : recaryrecary(1 downto 0);
                          end record;


  constant c1 : recaryrecaryrec :=  (fary2 => (others => (fary1 => (others => (f1 => "0000", f2 => '0')))));

  signal sig : recaryrecaryrec := c1;   -- carbon observeSignal
  signal sig1 : recaryrecaryrec := c1;  -- carbon observeSignal
  
begin  -- arch

  proc: process (clk, rst, sig)
  begin  -- process proc
    if rst = '0' then                   -- asynchronous reset (active low)
      sig <= c1;
      sig1 <= sig;
      out1 <= "00000";
    elsif clk'event and clk = '1' then  -- rising clock edge
      for s in 1 downto 0 loop
        for i in 7 downto 0 loop
          sig.fary2(s).fary1(i).f1 <= in1(3 downto 0);
          sig.fary2(s).fary1(i).f2 <= in1(4);
          sig1.fary2(s).fary1(i).f2 <= sig.fary2(s).fary1(i).f1(3);
          sig1.fary2(s).fary1(i).f1 <= sig.fary2(s).fary1(i).f1(2 downto 0) & sig.fary2(s).fary1(i).f2;
          out1 <= (sig1.fary2(s).fary1(i).f1 or sig.fary2(s).fary1(i).f1) & (sig1.fary2(s).fary1(i).f2 or sig.fary2(s).fary1(i).f2);
        end loop;  -- i
      end loop;  -- s
    end if;
  end process proc;

end arch;
