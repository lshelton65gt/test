-- nested levels of arrays of records of arrays
-- this design inspired by bug 7666, 
-- where warning message about out of range access indicated a problem.
-- Here is an example of two of the invalid warning messages
-- rec_arr5.vhd:64: Warning 3503: Part-select [3:0] out of range for net rhsr.rhs_table.rhs_array2[1:0]
-- rec_arr5.vhd:24 rec_arr5.rhsr.rhs_table.rhs_array2: Warning 4066: Range [3:1] is dead (cannot reach design outputs or observes).

library ieee;
use ieee.std_logic_1164.all;

package p is

  constant Data4Width : integer := 4;
  subtype Data4 is std_logic_vector(Data4Width-1 downto 0);
  constant Data16Width : integer := 16;
  subtype Data16 is std_logic_vector(Data16Width-1 downto 0);

  type Data4Array is array (NATURAL range<>) of Data4;
  type Data16Array is array (NATURAL range<>) of Data16;


  type rec_of_mixed is record
                         f1			: std_logic_vector(15 downto 0);
                         RHS_ARRAY1		: Data4Array(1 downto 0);
                         f2   			: std_logic_vector(15 downto 0);
                         RHS_ARRAY2            : Data16Array(3 downto 0);
                       end record;

  type T_array_of_rec_of_mixed is array(natural range<>) of rec_of_mixed;

  type T_RHSR is record
                   RHS_TABLE : T_array_of_rec_of_mixed(1 downto 0);
                 end record;

  type T_LHS_REC_OF_ARRAY is record 
                               LHS_GPIO                   : Data4Array(15 downto 0);
                             end record;

  type T_LHS_TABLE is array(NATURAL range <>) of T_LHS_REC_OF_ARRAY;

  type T_LHSR is record
                   LHS_TABLE : T_LHS_TABLE(3 downto 0); 
                 end record;

end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity mid is
  port ( din : in T_RHSR;               -- carbon observeSignal
         dout: out T_LHSR;              -- carbon observeSignal
         clk: in std_logic
       );
end mid;

architecture a of mid is
  signal RHSR : T_RHSR;                 -- carbon observeSignal
  signal LHSR : T_LHSR;                 -- carbon observeSignal

begin

 p1: process (clk)
 begin  -- process p1
   if clk'event and clk = '1' then  -- rising clock edge
     for coreNo in 0 to 3 loop
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(0)  <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(0)( 3 downto  0);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(1)  <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(0)( 7 downto  4);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(2)  <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(0)(11 downto  8);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(3)  <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(0)(15 downto 12);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(4)  <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(1)( 3 downto  0);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(5)  <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(1)( 7 downto  4);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(6)  <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(1)(11 downto  8);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(7)  <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(1)(15 downto 12);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(8)  <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(2)( 3 downto  0);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(9)  <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(2)( 7 downto  4);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(10) <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(2)(11 downto  8);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(11) <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(2)(15 downto 12);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(12) <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(3)( 3 downto  0);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(13) <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(3)( 7 downto  4);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(14) <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(3)(11 downto  8);
        LHSR.LHS_TABLE(coreNo).LHS_GPIO(15) <= RHSR.RHS_TABLE(coreNo/2).RHS_ARRAY2(3)(15 downto 12);                
     end loop;
   end if;
 end process p1;

 p2: process (din)
 begin  -- process
   RHSR <= din;
 end process;
 
-- p3: process( LHSR )
-- begin  -- process
     dout <= LHSR;
-- end process;

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_arr5 is

  port (
    clk                 : in  std_logic;
    f1                  : in  std_logic_vector(31 downto 0);
    rhs_array1          : in  std_logic_vector(15 downto 0);
    f2                  : in  std_logic_vector(31 downto 0);
    rhs_array2          : in  std_logic_vector(127 downto 0);
    lhs_table_gpio      : out std_logic_vector(255 downto 0));  -- carbon observeSignal
  
end rec_arr5;

architecture arch of rec_arr5 is

  component mid
    port (
      din  : in  T_RHSR;
      dout : out T_LHSR;
      clk  : in  std_logic);
  end component;
  
  signal din_sig : T_RHSR;
  signal dout_sig : T_LHSR;
  
begin  -- arch

  mid1 : mid port map (
    clk => clk,
    din => din_sig,
    dout => dout_sig);

  process (dout_sig)
  begin  -- process

    for i in 3 downto 0 loop
      for j in 15 downto 0 loop
        lhs_table_gpio((i*64 + j*4 + 3) downto (i*64 + j*4))
          <= dout_sig.lhs_table(i).lhs_gpio(j);
      end loop;  -- j
    end loop;  -- i
    
  end process;
    
  process (f1, f2, rhs_array1, rhs_array2)
  begin  -- process

    for i in 1 downto 0 loop
      din_sig.rhs_table(i).f1 <= f1(i*16+15 downto i*16);
      din_sig.rhs_table(i).f2 <= f2(i*16+15 downto i*16);
      for j in 1 downto 0 loop
        din_sig.rhs_table(i).rhs_array1(j) <= rhs_array1(i*8+j*4+3 downto i*8+j*4);
      end loop;  -- j
      for k in 3 downto 0 loop
        din_sig.rhs_table(i).rhs_array2(k) <= rhs_array2(i*64+k*16+15 downto i*64+k*16);
      end loop;  -- k
    end loop;  -- i
    
  end process;
  
end arch;
