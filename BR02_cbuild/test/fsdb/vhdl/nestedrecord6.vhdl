-- Test non-port unconstrained type arrayed nested records concurrent
-- assignment. Also test positional association of a nested record
-- subelement.
library ieee;
use ieee.std_logic_1164.all;

package pack is
  -- bottomrec is 5 bits total
  type bottomrec is record
                      b1     : std_logic;
                      t2     : std_logic_vector(3 downto 0);  -- reuse field name
                    end record;

  -- midrec is 11 bits total
  type midrec is record
                   mid_nest1 : bottomrec;
                   m2        : std_logic;
                   mid_nest2 : bottomrec;  -- multiple instance of the same
                                           -- record type
                 end record;
  -- toprec is 16 bits total
  type toprec is record
                   t1        : std_logic_vector(1 to 2);
                   top_nest  : midrec;
                   t4        : std_logic_vector(2 to 4);
                 end record;

  type bundlerec is array(natural range<>) of toprec;
end pack;


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity nestedrecord6dut is
  port ( clk, reset: in std_logic;
         recinport : in toprec;
         recoutport1: out toprec;
         recoutport2: out toprec );
end nestedrecord6dut;

architecture a of nestedrecord6dut is
  signal sigrec : bundlerec(0 to 1);    -- carbon observeSignal
begin
  sigrec(1) <= ( sigrec(0).t4(2 to 3),
                 recinport.top_nest,
                 sigrec(1).t1 & recinport.top_nest.mid_nest1.b1 );
  sigrec(0) <= recinport;
  recoutport1 <= sigrec(0);
  recoutport2 <= sigrec(1);
end a;


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;

entity nestedrecord6 is
  port (
    clk, reset     : in  std_logic;
    recinport_t1   : in  std_logic_vector(1 downto 0);
    recinport_t4   : in  std_logic_vector(2 downto 0);
    recinport_m2   : in  std_logic;
    recinport_1_b1 : in  std_logic;
    recinport_1_t2 : in  std_logic_vector(3 downto 0);
    recinport_2_b1 : in  std_logic;
    recinport_2_t2 : in  std_logic_vector(3 downto 0);
    
    recoutport1_t1 : out std_logic_vector(1 downto 0);
    recoutport1_t4 : out std_logic_vector(2 downto 0);
    recoutport1_m2   : out  std_logic;
    recoutport1_1_b1 : out  std_logic;
    recoutport1_1_t2 : out  std_logic_vector(3 downto 0);
    recoutport1_2_b1 : out  std_logic;
    recoutport1_2_t2 : out  std_logic_vector(3 downto 0);

    recoutport2_t1 : out std_logic_vector(1 downto 0);
    recoutport2_t4 : out std_logic_vector(2 downto 0);
    recoutport2_m2   : out  std_logic;
    recoutport2_1_b1 : out  std_logic;
    recoutport2_1_t2 : out  std_logic_vector(3 downto 0);
    recoutport2_2_b1 : out  std_logic;
    recoutport2_2_t2 : out  std_logic_vector(3 downto 0)
);
end nestedrecord6;

architecture a of nestedrecord6 is
  component nestedrecord6dut
    is
      port ( clk, reset: in std_logic;
             recinport : in toprec;
             recoutport1: out toprec;
             recoutport2: out toprec );
  end component nestedrecord6dut;

begin

  dut : nestedrecord6dut port map (
    clk            => clk,
    reset          => reset,
    recinport.t1   => recinport_t1,
    recinport.t4   => recinport_t4,
    recinport.top_nest.m2 => recinport_m2,
    recinport.top_nest.mid_nest1.b1 => recinport_1_b1,
    recinport.top_nest.mid_nest1.t2 => recinport_1_t2,
    recinport.top_nest.mid_nest2.b1 => recinport_2_b1,
    recinport.top_nest.mid_nest2.t2 => recinport_2_t2,
    
    recoutport1.t1 => recoutport1_t1,
    recoutport1.t4 => recoutport1_t4,
    recoutport1.top_nest.m2 => recoutport1_m2,
    recoutport1.top_nest.mid_nest1.b1 => recoutport1_1_b1,
    recoutport1.top_nest.mid_nest1.t2 => recoutport1_1_t2,
    recoutport1.top_nest.mid_nest2.b1 => recoutport1_2_b1,
    recoutport1.top_nest.mid_nest2.t2 => recoutport1_2_t2,
    
    recoutport2.t1 => recoutport2_t1,
    recoutport2.t4 => recoutport2_t4,
    recoutport2.top_nest.m2 => recoutport2_m2,
    recoutport2.top_nest.mid_nest1.b1 => recoutport2_1_b1,
    recoutport2.top_nest.mid_nest1.t2 => recoutport2_1_t2,
    recoutport2.top_nest.mid_nest2.b1 => recoutport2_2_b1,
    recoutport2.top_nest.mid_nest2.t2 => recoutport2_2_t2 );
  
end a;
