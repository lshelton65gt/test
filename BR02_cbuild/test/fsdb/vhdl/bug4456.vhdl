-- In order for this to compile in Aldec with main.vhdl, this conversion
-- function needs to be added to the set of conversion functions:

--  function to_accesstype (param : std_logic_vector) return accesstype is
--    variable retval : accesstype;
--  begin
--    case param(1 downto 0) is
--      when "00" => retval := mem;
--      when "01" => retval := io;
--      when "10" => retval := configType0;
--      when "11" => retval := configType1;
--      when others => null;
--    end case;
--    return retval;
--  end to_accesstype;

-- you also need to use package pciGlobalDefs and make a few other small edits
-- to main.vhdl to allow it to compile.

library ieee;
use ieee.std_logic_1164.all;

package pciGlobalDefs is
  constant pci_Data32BitBusWidth:     integer := 2;

  type bundle_pci32BitBus is
    record
      ad:             std_logic_vector (pci_Data32BitBusWidth-1 downto 0);
    end record;

  type accessType is (mem, io, configType0, configType1);
  constant maxSonicsToPciRequests:            integer := 1;

  type bundle_sonicsPciRequest is
    record
      accessSpace:     accessType;
      firstByteEnable: std_logic_vector(3 downto 0);
    end record;
  type bundleVector_sonicsPciRequest is array(natural range<>) of bundle_sonicsPciRequest;

  function to_accesstype (param : std_logic_vector) return accesstype;
  function to_stdlogicvector (param : accesstype) return std_logic_vector;

end pciGlobalDefs;

package body pciGlobalDefs is
  function to_accesstype (param : std_logic_vector) return accesstype is
  variable index : std_logic_vector(0 to 1) := param;
  variable retval : accesstype;
  begin
    case index is
      when "00" => retval := mem;
      when "01" => retval := io;
      when "10" => retval := configType0;
      when "11" => retval := configType1;
      when others => null;
    end case;
    return retval;
  end to_accesstype;

 function to_stdlogicvector (param : accesstype) return std_logic_vector is
  variable retval : std_logic_vector(0 to 1);
  begin
    case param is
      when mem => retval := "00";
      when io => retval := "01";
      when configType0 => retval := "10";
      when configType1 => retval := "11";
      when others => null;
    end case;
    return retval;
  end to_stdlogicvector;
end  pciGlobalDefs;


library ieee;
use ieee.std_logic_1164.all;
use work.pciglobaldefs.all;
use std.textio.all;

entity dut is
  port (
    in1: in std_logic_vector(1 downto 0) := "00";
    sonicsRequest:  in  bundleVector_sonicsPciRequest(maxSonicsToPciRequests downto 0);  -- carbon observeSignal
    core2pad_pciBus :  buffer  bundle_pci32BitBus);
end dut;

architecture arch of dut is

  signal currentRequest: integer range maxSonicsToPciRequests downto 0 := 0; -- carbon observeSignal
  
  function genAddr10Bits (transactionInfo: bundle_sonicsPciRequest)
    return std_logic_vector is
  begin
    case transactionInfo.accessSpace is
      when mem =>
        return "00";
      when io =>
        --  io does a full byte address on pci, stupid io 
        if transactionInfo.firstByteEnable(0) = '1' then
          return "00";
        elsif transactionInfo.firstByteEnable(1) = '1' then
          return "01";
        elsif transactionInfo.firstByteEnable(2) = '1' then
          return "10";
        else
          return "11";
        end if;
      when configType0 =>
        return "00";
      when configType1 =>
        return "01";
    end case;
  end function;
begin  -- arch

  currentRequest <= 0 when in1(1) = '0' else 1;
  
  p1: process( sonicsRequest, currentRequest )
  begin  -- process p1
    core2pad_pciBus.ad(1 downto 0) <= genAddr10Bits(sonicsRequest(currentRequest));
  end process p1; 

end arch;

library ieee;
use ieee.std_logic_1164.all;
use work.pciglobaldefs.all;

entity bug4456 is
  port (
    in1                : in  std_logic_vector(1 downto 0);
    accessSpace1       : in  accessType;
    firstByteEnable1   : in  std_logic_vector(3 downto 0);
    accessSpace0       : in  accessType;
    firstByteEnable0   : in  std_logic_vector(3 downto 0);
    ad                 : buffer std_logic_vector(pci_Data32BitBusWidth-1 downto 0));
end bug4456;

architecture arch of bug4456 is
  component dut is
                  port (
                    in1: in std_logic_vector(1 downto 0);
                    sonicsRequest:  in  bundleVector_sonicsPciRequest(maxSonicsToPciRequests downto 0);
                    core2pad_pciBus : buffer bundle_pci32BitBus);
  end component dut;

begin

  u1 : dut port map (
    in1                              => in1,
    sonicsRequest(1).accessSpace     => accessSpace1,
    sonicsRequest(1).firstByteEnable => firstByteEnable1,
    sonicsRequest(0).accessSpace     => accessSpace0,
    sonicsRequest(0).firstByteEnable => firstByteEnable0,
    core2pad_pciBus.ad               => ad);

end arch;
