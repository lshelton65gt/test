-- March 2008
-- This file tests that the temp reset net doesn't appear in fsdb file

library IEEE;
use IEEE.std_logic_1164.all;

entity bug8517_01 is
  
  port (
    hrstN     : in  std_logic;
    asyncrstN : in  std_logic;
    hclkbuf   : in  std_logic;
    in1       : in  std_logic_vector(3 downto 0);
    out1      : out std_logic_vector(3 downto 0));

end bug8517_01;


architecture arch of bug8517_01 is
begin  -- arch

  process (hrstN, asyncrstN, hclkbuf, in1)  
  begin
    if(hrstN = '0' or asyncrstN = '0') then
      out1 <= (others => '0');
    else
      if(hclkbuf'event and hclkbuf='1') then
         out1 <= in1;
      end if;
    end if;
  end process;
end arch;
