-- This tests the ability to populate arrays of arrays of records and to
-- do element assigns and array indexing with them, as well as full
-- record assigns.

entity multidim6 is
  port ( clk, rst : in bit;
         in1 : in bit_vector(16 downto 0);
         out1 : out bit_vector(33 downto 17) );
end entity;

architecture arch of multidim6 is
  type twobitmem is array (75 downto 72) of bit_vector(77 downto 76);
  type nested is record                              -- 12 bits total
                   one   : bit_vector(11 downto 9);  -- 3 bits
                   two   : bit;                      -- 1 bit
                   three : twobitmem;                -- 4x2=8 bits
                 end record;
  
  type tRange is record                            -- 17 bits total
                   high : bit_vector(3 downto 0);  -- 4 bits
                   low : nested;                   -- 12 bits
                   extra : bit;                    -- 1 bit
                 end record;
  
  type tClkRange is array (7 downto 0) of tRange;   -- 8x17 bits = 136 bits
  type tClkRangeArr is array (1 to 2) of tClkRange; -- 136x2=272 bits
  signal s : tClkRangeArr := (others => (others => ("0000", ("000", '0', (others => "00")), '0'))); -- carbon observeSignal
  signal rdaddr1 : integer range 1 to 2 := 1;
  signal wraddr1 : integer range 1 to 2 := 2;
  signal rdaddr2 : integer range 7 downto 0 := 7;
  signal wraddr2 : integer range 7 downto 0 := 6;
  
begin
  indexer: process (clk, rst)
  begin 
    if rst = '0' then
      rdaddr1 <= 1;
      rdaddr2 <= 7;
      wraddr1 <= 2;
      wraddr2 <= 6;
    elsif clk'event and clk = '1' then
      -- set up read addresses
      if rdaddr2 = 0 then
        rdaddr2 <= 7;
      else
        rdaddr2 <= rdaddr2 - 1;
      end if;
      if rdaddr1 = 1 then
        rdaddr1 <= 2;
      else
        rdaddr1 <= 1;
      end if;
      -- set up write addresses
      if wraddr2 = 0 then
        wraddr2 <= 7;
      else
        wraddr2 <= wraddr2 - 1;
      end if;
      if wraddr1 = 1 then
        wraddr1 <= 2;
      else
        wraddr1 <= 1;
      end if;      
    end if;
  end process indexer;
  
  reader : process ( clk )
  begin
    if clk'event and clk = '1' then
      s(wraddr1)(wraddr2).high <= in1(16 downto 13);
      s(wraddr1)(wraddr2).extra <= not in1(12);
      s(wraddr1)(wraddr2).low.one(11) <= in1(11) and in1(10);
      s(wraddr1)(wraddr2).low.one(10 downto 9) <= ((in1(11) xor in1(9)), (in1(10) or in1(9)));
      s(wraddr1)(wraddr2).low.two <= in1(8);
      s(wraddr1)(wraddr2).low.three(75) <= in1(7 downto 6);
      s(wraddr1)(wraddr2).low.three(74) <= not (in1(4), not in1(5));
      s(wraddr1)(wraddr2).low.three(73 downto 72) <= ((in1(0), in1(2)), (in1(3), in1(1)));
    end if;
  end process;

  writer: process (s)
    variable s1 : tClkRangeArr := (others => (others => ("0000", ("000", '0', (others => "00")), '0')));
  begin
    s1 := s;
    out1(33 downto 30) <= s1(rdaddr1)(rdaddr2).high;
    out1(29) <= s1(rdaddr1)(rdaddr2).extra;
    out1(28 downto 26) <= s1(rdaddr1)(rdaddr2).low.one;
    out1(25) <= s1(rdaddr1)(rdaddr2).low.two;
    out1(24 downto 23) <= s1(rdaddr1)(rdaddr2).low.three(75);
    out1(22 downto 21) <= s1(rdaddr1)(rdaddr2).low.three(74);
    out1(20 downto 19) <= s1(rdaddr1)(rdaddr2).low.three(73);
    out1(18 downto 17) <= s1(rdaddr1)(rdaddr2).low.three(72);
  end process;
  
end architecture;
