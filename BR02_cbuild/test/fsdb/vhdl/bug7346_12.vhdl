-- July 2007
-- The signals mem_data and mem_data2 are array of records.
-- They are defined in a package declaration. The entity (not top one) uses the
-- signals in the left and right side assignment. 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package FIXED_POINT_PKG is
  subtype fixed4_type is SIGNED (15 downto 0);
  type complex_fixed_type is
    record
      real1    : fixed4_type;
      imag1    : fixed4_type;
    end record;

  type arr_rec is array (0 to 1) of complex_fixed_type;
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
package FFT_CORE_SIG_PKG is
  signal mem_data      : arr_rec;
  signal mem_data2     : arr_rec;
end FFT_CORE_SIG_PKG;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity sub is
end sub;

architecture arch of sub is
begin
  mem_data2 <= mem_data;
end;




library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
use work.FFT_CORE_SIG_PKG.all;
entity bug7346_12 is
  port (
    in1        : in fixed4_type;
    in2        : in fixed4_type;
    in3        : in fixed4_type;
    in4        : in fixed4_type;
    out1       : out fixed4_type;
    out2       : out fixed4_type;
    out3       : out fixed4_type;
    out4       : out fixed4_type);
end bug7346_12;

architecture arch_top of bug7346_12 is
begin

  mem_data(0).real1 <= in1;             
  mem_data(0).imag1 <= in2;            
  mem_data(1).real1 <= in3;            
  mem_data(1).imag1 <= in4;          

  i_sub : entity work.sub;

  out1 <= mem_data2(0).real1;              
  out2 <= mem_data2(0).imag1;             
  out3 <= mem_data2(1).real1;               
  out4 <= mem_data2(1).imag1;                
end;
