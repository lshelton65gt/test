-- Test array of records waveform dump.

entity record16 is
  port (
    in1  : in  bit_vector(7 downto 0)
    ;in2  : in  bit_vector(7 downto 0)
    ;outp : out bit_vector(7 downto 0)
    );
end record16;

architecture a of record16 is

type rec1 is record
  f1 : bit;
  f2 : bit;
end record;

type rec is record
    r1 : bit_vector(7 downto 0);
    r2 : bit_vector(7 downto 0);
    r3 : rec1;
end record;

type recary is array (1 downto 0) of rec;

signal s1: recary := (others => (r1 => (others => '0'), r2 => (others => '0'), r3 => (f1 => '0', f2 => '0'))); -- carbon observeSignal

begin
  s1(0).r1 <= in1;
  s1(1).r2 <= in2;
  s1(0).r3.f1 <= in1(0);
  s1(1).r3.f2 <= in2(0);
  outp <= s1(0).r1 and s1(1).r2;
end a;
