-- Feb 2008
-- This file is failing with user type population.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
        
package base is

   type days is (mon, tue, wed, thr, fri, sat, sun);

   type tRec is record

      f1 : std_ulogic;

      f2 : bit_vector(0 to 7);

      f3 : natural;

      f4 : string(1 to 8);

      f5 : positive;

      f6 : days;

      f7 : character;

      f8 : unsigned(0 to 7);

      f9 : signed(0 to 7);

      f10 : boolean;

      f11 : integer;

      f12 : std_logic;

      f13 : bit;

      f14 : std_ulogic_vector(0 to 7);

      f15 : std_logic_vector(0 to 7);

   end record;

   type tZtoN_tRec is array(0 to 7) of tRec;

   type tZtoN_tZtoN_tRec is array(0 to 7) of tZtoN_tRec;

   function To_Days (param : std_logic_vector) return days;

   function To_StdLogicVector (param : days) return std_logic_vector;

end base;

package body base is

  function To_Days (param : std_logic_vector) return days is
    variable index : std_logic_vector(0 to 2) := param;
    variable result : days;
  begin  -- To_Days
    case index is
      when "000" => result := mon;
      when "001" => result := tue;
      when "010" => result := wed;
      when "011" => result := thr;
      when "100" => result := fri;
      when "101" => result := sat;
      when "110" => result := sun;
      when others => result := mon;
    end case;
    return result;
  end To_Days;  

  function To_StdLogicVector (param : days) return std_logic_vector is
    variable result : std_logic_vector(0 to 2);
  begin  -- To_StdLogicVector
    case param is
      when mon => result := "000";
      when tue => result := "001";
      when wed => result := "010";
      when thr => result := "011";
      when fri => result := "100";
      when sat => result := "101";
      when sun => result := "110";
    end case;
    return result;
  end To_StdLogicVector;
  
end base;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity bug8272_1 is
   port ( bin  : in tZtoN_tZtoN_tRec;
          bout : out tZtoN_tZtoN_tRec );
end bug8272_1;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archbottom of bug8272_1 is

   signal s1 :tZtoN_tZtoN_tRec;

begin

   s1 <= bin;

   bout <= s1;

end archbottom;



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity ZtoN_ZtoN_Rec is
   port ( pin  : in  tZtoN_tZtoN_tRec;
          pout : out tZtoN_tZtoN_tRec );
end ZtoN_ZtoN_Rec;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archZtoN_ZtoN_Rec of ZtoN_ZtoN_Rec is

   component bottom is
      port ( bin  : in  tZtoN_tZtoN_tRec;
             bout : out tZtoN_tZtoN_tRec);
   end component;

begin

   u1 : bottom port map ( bin  => pin,
                          bout => pout);

end archZtoN_ZtoN_Rec;
