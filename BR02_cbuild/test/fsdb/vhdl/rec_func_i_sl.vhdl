-- Test passing a record with a scalar to a function (input param only)
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec2 is
    record
      r2 : std_logic_vector(3 downto 0);
      b1 : std_logic;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_sldut is
  port ( recinport : in rec2
         ; recoutport1: out rec2
         ; recoutport2: out rec2
         ; clk: in std_logic
         );
end rec_func_i_sldut;

architecture a of rec_func_i_sldut is
  signal sigrec : rec2;

  function func2 ( inrec : rec2; b1,b2 : std_logic ) return std_logic_vector is
    variable outdata : std_logic_vector(7 downto 0);
  begin
    outdata := inrec.r2 & inrec.b1 & inrec.r2(3) & b1 & b2;
    return outdata;
  end func2;
begin

  p1: process (clk)
    variable result : std_logic_vector(7 downto 0);
  begin
    if clk'event and clk = '1' then
      result := func2 ((r2=>recinport.r2, b1 =>recinport.b1), recinport.b1,
                     (recinport.r2(3) or recinport.r2(0)));
      sigrec.b1 <= (result(2) xor result(3)) or (result(1) and result(0));
      sigrec.r2 <= result(7 downto 4);
    end if;
  end process p1;

  p3: process( sigrec )
  begin
    recoutport1 <= sigrec;
    recoutport2.b1 <= sigrec.b1;
    recoutport2.r2 <= sigrec.r2;
  end process;

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_sl is
  port (
    clk            : in  std_logic;
    recinport_b1   : in  std_logic;
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_b1 : out std_logic;
    recoutport1_r2 : out std_logic_vector(3 downto 0);
    recoutport2_b1 : out std_logic;
    recoutport2_r2 : out std_logic_vector(3 downto 0));
end rec_func_i_sl;

architecture a of rec_func_i_sl is
component rec_func_i_sldut is
  port ( recinport : in rec2
         ; recoutport1: out rec2
         ; recoutport2: out rec2
         ; clk: in std_logic
         );
end component rec_func_i_sldut;

begin

  dut : rec_func_i_sldut port map (
    clk            => clk,
    recinport.b1   => recinport_b1,
    recinport.r2   => recinport_r2,
    recoutport1.b1 => recoutport1_b1,
    recoutport1.r2 => recoutport1_r2,
    recoutport2.b1 => recoutport2_b1,
    recoutport2.r2 => recoutport2_r2);

end a;
