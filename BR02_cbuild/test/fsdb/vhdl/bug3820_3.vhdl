-- like bug3820.vhd but more complex, here we have a function that takes
-- a record as an argument
library ieee;
use ieee.std_logic_1164.all;

package bug3820_3_pak is

  type myrec is record
                  f1 : std_logic_vector(1 downto 0);
                  f2 : std_logic;
                end record;

  -- Conversion function for myrec_record
  function to_myrec(
    ftc_list : std_logic_vector(2 downto 0)
    ) return myrec;

  function myrec_allones(mr : myrec) return std_logic;

end bug3820_3_pak;

package body bug3820_3_pak is
  
  function to_myrec(
    ftc_list : std_logic_vector(2 downto 0)
    ) return myrec is 
    variable command           : myrec;
    alias part1    : std_logic_vector(1 downto 0) is ftc_list(1 downto 0);
    alias part2    : std_logic                    is ftc_list(2);
  begin
    command.f1    := part1;
    command.f2    := part2;
    return command;
  end to_myrec;

  function myrec_allones(mr : myrec) return std_logic is
  begin
    if (mr.f1 = b"11") and (mr.f2 = '1') then
      return '1';
    else
      return '0';
    end if;
  end function;

end bug3820_3_pak;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library work;
use work.bug3820_3_pak.all;

entity bug3820_3 is
  port(in1, in2 : std_logic_vector(2 downto 0); out1 : out std_logic);
end;

architecture arch of bug3820_3 is

  signal sig1 : myrec;   -- carbon observeSignal
  signal sig2 : myrec;   -- carbon observeSignal
begin
--  sig1 <= to_myrec(in1);
  sig1.f1(0) <= in1(0);
  sig1.f1(1) <= in1(1);
  sig1.f2 <= in1(2);
  sig2 <= sig1;
  out1 <= myrec_allones(sig2);
end;
