-- October 2007
-- This file tests a select statement with record's field assignment.
-- With version 1.5851 this design would produce an INFO_ASSERT about calling
-- old record method for new record implementation.

library ieee;
use ieee.std_logic_1164.all;

entity bug7672_02 is
  
  port (
    clk  : in  std_logic;
    in1  : in  std_logic_vector(3 downto 0);
    en   : in  std_logic_vector(1 downto 0);
    out1 : out std_logic_vector(1 downto 0));
  
end bug7672_02;

architecture arch of bug7672_02 is

  type rec is record
                f1 : std_logic;
                f2 : std_logic;
              end record;
  
  signal sig1 : rec := ('0', '0');
  signal sig2 : rec := ('0', '0');
  signal sig3 : rec := ('0', '0');
  
  
begin  -- arch

  proc1: process (clk)
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge
      sig1 <= (in1(0), in1(1));
      sig2 <= (in1(2), in1(3));
    end if;
  end process;

  out1 <= sig3.f1 & sig3.f2;

  -- The select statements below are tested
  with en select
    sig3.f1 <=
    sig1.f1 when "10",
    sig2.f1 when others;

  with en select
    sig3.f2 <=
    sig1.f2 when "10",
    sig2.f2 when others;

end arch;
