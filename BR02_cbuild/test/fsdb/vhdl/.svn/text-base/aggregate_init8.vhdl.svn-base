-- Test for array of record which contains a record array field
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity aggregate_init8 is
  port (sel : in std_logic_vector(1 downto 0);
        fsel : in std_logic_vector(2 downto 0);
        out1: out std_logic_vector(5 downto 0));
end aggregate_init8;

architecture rtl of aggregate_init8 is
  constant const1 : integer := 64;   
  constant const2 : integer := 2; 
  constant const3 : integer := 4; 

  subtype fint is integer range 0 to const1-1;

  type dtype is record
                  f11 : fint;
                  f12 : fint;
                end record;

  type dconfig is array (Natural Range <>) of dtype;

  type ctype is record
                  f1 : fint;
                  f2 : fint;
                  f3 : dconfig(const2-1 downto 0);
                  f4 : fint;
                end record;

  type sconfig is array (Natural Range <> ) of ctype;

  constant c1 : ctype := (3, 7, ((13, 21),(14, 22)), 27);
  constant c2 : ctype := (2, 6, ((11, 19),(12, 20)), 26);
  constant c3 : ctype := (1 , 5, ((9, 17),(10, 18)), 25);
  constant c4 : sconfig(const3-1 downto 0) := (0 => (4, 8, ((15, 23),(16, 24)), 28), 1 => c1, 2 => c2, others => c3);
  
  signal sig1 : sconfig(const3-1 downto 0);  -- carbon observeSignal
begin
  sig1 <= c4;

  process (fsel, sel, sig1)
    variable lsel : integer;
    variable lfsel : integer;
    variable lout  : fint;
  begin
    lfsel := to_integer(unsigned(fsel));
    lsel := to_integer(unsigned(sel));
    case lfsel is
      when 0 => lout := sig1(lsel).f1;
      when 1 => lout := sig1(lsel).f2;
      when 2 => lout := sig1(lsel).f3(0).f11;
      when 3 => lout := sig1(lsel).f3(0).f12;
      when 4 => lout := sig1(lsel).f3(1).f11;
      when 5 => lout := sig1(lsel).f3(1).f12;
      when 6 => lout := sig1(lsel).f4;
      when others => lout := 0;
    end case;
    out1 <= std_logic_vector(to_unsigned(lout, out1'length));
  end process;
end rtl;
