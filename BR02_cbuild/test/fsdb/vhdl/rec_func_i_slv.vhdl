-- Test passing a record to a function (input param only)
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_slvdut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end rec_func_i_slvdut;

architecture a of rec_func_i_slvdut is
  signal sigrec : rec;

  function func ( inrec : rec; b1 : std_logic  ) return std_logic_vector is
    variable outdata : std_logic_vector(11 downto 0);
  begin
    if b1 = '1' then
      outdata := inrec.r1 & inrec.r2 ;
    else      
      outdata := inrec.r2 & inrec.r1 ;
    end if;
    return outdata;
  end func;
begin

  p1: process (clk)
    variable result : std_logic_vector(11 downto 0);
  begin
    if clk'event and clk = '1' then
      result := func(recinport, recinport.r1(0));
      sigrec.r1 <= result(7 downto 0);
      sigrec.r2 <= result(11 downto 8);
    end if;
  end process p1;

  p3: process( sigrec )
  begin
    recoutport1 <= sigrec;
  end process;

end a;
library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_slv is
  port (
    clk            : in  std_logic;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0));end rec_func_i_slv;

architecture a of rec_func_i_slv is
component rec_func_i_slvdut is
  port ( recinport : in rec
         ; recoutport1: out rec
         ; clk: in std_logic
         );
end component rec_func_i_slvdut;

begin

  dut : rec_func_i_slvdut port map (
    clk            => clk,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2);
  
end a;

