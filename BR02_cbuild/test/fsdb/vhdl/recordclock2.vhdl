-- Test basic population and manipulation of record fields

entity recordclock2 is
  port (
    clk,rst : in bit;
    in1  : in  bit_vector(7 downto 0);
    in2  : in  bit_vector(7 downto 0);
    outp : out bit_vector(7 downto 0)
    );
end;

architecture a of recordclock2 is

type rec is record
    r1 : bit_vector(7 downto 0);
    r2 : bit_vector(7 downto 0);
    r3 : bit_vector(1 downto 0);
end record;

signal s1: rec;                         -- carbon observeSignal

begin

  s1.r3 <= (clk & rst);
  
  u1: process (s1.r3(1),s1.r3(0))
  begin  -- process u1
    if s1.r3(0) = '0' then
      s1.r1 <= (others=>'0');
      s1.r2 <= (others=>'0');
    elsif s1.r3(1)'event and s1.r3(1) = '1' then  -- rising clock edge
      s1.r1 <= in1;
      s1.r2 <= in2;
      outp <= s1.r1 and s1.r2;
    end if;
  end process u1;
end a;
