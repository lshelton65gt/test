-- Attempting to reproduce bug7804 :
-- ******CARBON INTERNAL ERROR*******

-- {Types.vhd:79}NUCompositeExpr(0x410377b0) [size=798, context=798]  : {}
-- /n/release/test/release-C2007_04_5481_0927-2007-09-27/src/localflow/VhPopulateExpr.cxx:2342
-- NU_ASSERT(prefix->getType() == NUExpr::eNUIdentRvalue) failed
-- /home/eda/carbon/current/bin/vspcompiler: line 42:  8279 Aborted

-- This test fails because we replace the selected name part of
-- indexed selected name with a constant. The population phase fails to
-- populate such a thing into nucleus. The fix is to avoid the replacement.
package p is

  attribute enum_encoding : string;
  type state is (mon, tue, wed, thu, fri, sat, sun);
  attribute enum_encoding of state : type is "000 001 010 011 100 101 111";

  type recstate is record
                     fst : state;
                   end record;
  
  type recstateary is array (3 downto 0) of recstate;
  type rec is record
                f1 : recstateary;
              end record;

  constant csat : recstate := (fst => sat);
  constant csun : recstate := (fst => sun);
  constant cmon : recstate := (fst => mon);
  constant ctue : recstate := (fst => tue);
  
  constant crec : rec := (f1 => (csat, csun, cmon, ctue));
  
end p;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

library work;
use work.p.all;

entity bug7804 is

  port (
    in1  : in  std_logic_vector(2 downto 0);
    idx  : in  std_logic_vector(1 downto 0);
    out1 : out std_logic_vector(2 downto 0));

end bug7804;

architecture arch of bug7804 is
begin  -- arch

  proc1: process (in1, idx)
    variable intidx : integer range 0 to 3 := 0;
  begin  -- process proc1
    intidx := conv_integer(unsigned(idx));
    case crec.f1(intidx).fst is
      when mon => out1 <= "000";
      when tue => out1 <= "001";
      when wed => out1 <= "010";
      when thu => out1 <= "011";
      when fri => out1 <= "100";
      when sat => out1 <= "101";
      when others => out1 <= "111";
    end case;
  end process proc1;

end arch;
