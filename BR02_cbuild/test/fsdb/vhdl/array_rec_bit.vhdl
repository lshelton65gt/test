-- Testing waveform dumping of arrays of records with bit fields,
-- because the fields are implemented as vectors.  This should use the
-- backdoor storage pointer with bitselect

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity array_rec_bit is
  port (
    clk  : in  std_logic;
    addr : in  std_logic_vector (3 downto 0);
    we   : in  std_logic;
    a    : in  std_logic;
    b    : in  std_logic;
    o    : out std_logic);
end array_rec_bit;

architecture arch of array_rec_bit is

  type myrec is record
                  x : std_logic;
                  y : std_logic;
                end record;
  type myarray is array (0 to 15) of myrec;
  signal arr : myarray := (others => ('0', '0'));
  signal addr_int : integer;  -- carbon observeSignal
  
begin
  
  addr_int <= conv_integer(unsigned(addr));

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        arr(addr_int).x <= a;
        arr(addr_int).y <= b;
      else
        o <= arr(addr_int).x and arr(addr_int).y;
      end if;
    end if;
  end process;

end arch;
