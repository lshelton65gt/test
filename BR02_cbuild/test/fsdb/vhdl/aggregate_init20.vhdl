-- Reproduces a bug seen with conexant design. The lines identified
-- with "-- bug" have record slice assigning to record array. This
-- resulted in an assert in composite resynthesis. The slice is
-- converted to an aggregate with memory range select converted to
-- per address memory selects but vector range select remaining
-- as it is. This caused aggregate resynthesis code to fail.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_1 is

  constant c_num	: natural := 4;
  constant c_width13	: natural := 13;
  constant c_width16	: natural := 16;
  type	rec is
    record
      f1		: signed(c_width13 - 1 downto 0);
      f2		: signed(c_width16 - 1 downto 0);
      f3		: std_ulogic;
    end record rec;

  type	rec_array	is array(natural range <>) of rec;
  type	t_sgn13_array is array(2*c_num - 1 downto 0) of signed(c_width13 - 1 downto 0);
  type	t_sgn16 is array(2*c_num - 1 downto 0) of signed(c_width16 - 1 downto 0);

end pkg_1;

package body pkg_1 is

end pkg_1;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.pkg_1.all;

entity unit is
  port(
    rst			: in	 std_ulogic;
    clk			: in	 std_ulogic;
    input		: in	 rec_array(1 downto 0);
    output              : out    rec_array(1 downto 0)
    );
end unit;

architecture arch of unit is

begin  -- arch

  process (clk, rst)
  begin  -- process
    if rst = '0' then                   -- asynchronous reset (active low)
      output <= (others => (to_signed(0, c_width13), to_signed(0, c_width16), '0'));
    elsif clk'event and clk = '1' then  -- rising clock edge
      output <= input;
    end if;
  end process;

end arch;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.pkg_1.all;

entity aggregate_init20 is
  port(
    rst			: in	 std_ulogic;
    clk			: in	 std_ulogic;
    en                  : in     std_ulogic;
    sgn13               : in     signed(12 downto 0);
    sgn16               : in     signed(15 downto 0);
    idx                 : in     unsigned(1 downto 0);
    outen               : out    std_ulogic;
    outsgn13            : out    signed(12 downto 0);
    outsgn16            : out    signed(15 downto 0)
    );
end aggregate_init20;

architecture arch of aggregate_init20 is

  signal s_input	: rec_array(2*c_num - 1 downto 0);
  signal s_output	: rec_array(2*c_num - 1 downto 0);
  signal s_sgn13	: t_sgn13_array;
  signal s_f3	        : std_ulogic_vector(c_num - 1 downto 0);
  signal s_sgn16	: t_sgn16;

  component unit is
                   port(
                     rst			: in	 std_ulogic;
                     clk			: in	 std_ulogic;
                     input		: in	 rec_array(1 downto 0);
                     output              : out    rec_array(1 downto 0)
                     );
  end component unit;

begin

  p_sgn13:
  process(clk, rst)
    variable index : integer := 0;
  begin
    if rst = '0' then
      s_sgn13 <= (others=>(others => '0'));
      s_sgn16 <= (others=>(others => '0'));
      s_f3 <= (others => '0');
      outen <= '0';
      outsgn13 <= (others => '0');
      outsgn16 <= (others => '0');
    elsif clk'event and clk = '1' then
      s_sgn13(index) <= sgn13;
      s_sgn13(index*2) <= sgn13;
      s_sgn16(index) <= sgn16;
      s_sgn16(index*2) <= sgn16;
      s_f3(index) <= en;
      index := to_integer(idx);
      outen <= s_output(index*2).f3 or s_output(index*2+1).f3;
      outsgn16 <= s_output(index*2).f2 or s_output(index*2+1).f2;
      outsgn13 <= s_output(index*2).f1 or s_output(index*2+1).f1;
    end if;
  end process;

  g_unit :
  For j in 0 to c_num - 1  generate
    s_input(j*2).f1		<= s_sgn13(j*2);
    s_input(j*2 + 1).f1	<= s_sgn13(j*2 + 1);
    s_input(j*2).f3		<= s_f3(j);
    s_input(j*2 + 1).f3	<= '0';
    s_input(j*2).f2		<= s_sgn16(j*2);
    s_input(j*2 + 1).f2	<= s_sgn16(j*2 + 1);

    u_unit :
      unit
        PORT map(
          clk			=> clk,
          rst			=> rst,
          input			=> s_input(j*2 + 1 downto j*2),
          output                => s_output(j*2 + 1 downto j*2)
          );
  end generate;
end arch;

