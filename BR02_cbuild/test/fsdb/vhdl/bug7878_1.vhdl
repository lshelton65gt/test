-- Test for part-select of a 2 dimensional record array.
-- This gives us varsels on field bit nets that become
-- memory nets after composite resynthesis. This test tests
-- varsels on memsels of fields.
library ieee;
use ieee.std_logic_1164.all;

entity bug7878_1 is

  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    ivec : in  std_logic_vector(9 downto 0);
    ovec : out std_logic_vector(9 downto 0));
  
end bug7878_1;

architecture arch of bug7878_1 is

  type rec is record
                f1             : std_ulogic;
              end record;

  type recArray is array (natural range <>) of rec;

  type rec1Array is array (1 to 2) of recArray(1 to 5);
  type rec2Array is array (1 to 2) of recArray(1 to 8);
  type rec3Array is array (1 to 2) of recArray(5 downto 1);
  type rec4Array is array (1 to 2) of recArray(1 to 9);

  signal sig1to5 : rec1Array := (others => (others => (f1 => '0')));-- carbon observeSignal
  signal sig1to8 : rec2Array := (others => (others => (f1 => '1')));-- carbon observeSignal
  signal sig5dt1 : rec3Array := (others => (others => (f1 => '1')));-- carbon observeSignal
  signal sig1to9 : rec4Array := (others => (others => (f1 => '0')));-- carbon observeSignal
  
begin  -- arch

  proc1: process (clk, rst)
  begin  -- process proc1
    if rst = '0' then                   -- asynchronous reset (active low)
      ovec <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- Constant index varsel on memsel lvalue
      sig1to5(1)(1) <= (f1 => ivec(0));
      sig1to5(1)(2) <= (f1 => ivec(1));
      sig1to5(1)(3) <= (f1 => ivec(2));
      sig1to5(1)(4) <= (f1 => ivec(3));
      sig1to5(1)(5) <= (f1 => ivec(4));
      -- Variable index varsel on memsel lvalue
      for j in 1 to 5 loop
        sig1to5(2)(j) <= (f1 => ivec(j+4));
      end loop;  -- j

      -- Constant range part-select on memsel lvalue and rvalue.
      sig1to8(1)(1 to 5) <= sig1to5(1);
      sig1to8(2)(1 to 5) <= sig1to5(2);
      sig5dt1(1) <= sig1to8(1)(1 to 5);
      sig5dt1(2) <= sig1to8(2)(1 to 5);

      -- Variable range part-select on memsel lvalue and rvalue.
      for i in 1 to 2 loop
         -- 2:3 <= 2:1, 4:5 <= 4:3
        sig1to9(1)(i*2 to i*2+1) <= sig5dt1(1)((i-1)*2+2 downto (i-1)*2+1);
        sig1to9(2)(i*2 to i*2+1) <= sig5dt1(2)((i-1)*2+2 downto (i-1)*2+1);
      end loop;  -- i

      -- Constant index varsel on memsel lvalue and rvalue.
      sig1to9(1)(1) <= sig5dt1(1)(5);
      sig1to9(2)(1) <= sig5dt1(2)(5);

      ovec(0) <= sig1to9(1)(5).f1;
      ovec(1) <= sig1to9(1)(4).f1;
      ovec(2) <= sig1to9(1)(3).f1;
      ovec(3) <= sig1to9(1)(2).f1;
      ovec(4) <= sig1to9(1)(1).f1;
      ovec(5) <= sig1to9(2)(5).f1;
      ovec(6) <= sig1to9(2)(4).f1;
      ovec(7) <= sig1to9(2)(3).f1;
      ovec(8) <= sig1to9(2)(2).f1;
      ovec(9) <= sig1to9(2)(1).f1;
    end if;
  end process proc1;

end arch;
