-- Simple record waveform dump test.

entity record1 is
  port (
    in1  : in  bit_vector(7 downto 0)
    ;in2  : in  bit_vector(7 downto 0)
    ;outp : out bit_vector(7 downto 0)
    );
end record1;

architecture a of record1 is

type rec is record
    r1 : bit_vector(7 downto 0);
    r2 : bit_vector(7 downto 0);
end record;

signal s1: rec;

begin
  s1.r1 <= in1;
  s1.r2 <= in2;
  outp <= s1.r1 and s1.r2;
end a;
