--This is a test for bug4691, where a) we did not support assignment from a
--record constant.  Fixing this issue exposed the fact that we dod not handle
--initial values at all for record types.  This test uses an initial value for
--a constant record.

library ieee;
use ieee.std_logic_1164.all;

package tmp is
type bundle_reg2txs is record
     reg_sel : std_logic_vector(1 downto 0);
     rd_en   : std_logic;
     rd_nxt  : std_logic;
end record;
end tmp;


library ieee;
use ieee.std_logic_1164.all;
use work.tmp.all;

entity ex1 is
    port(clk : in std_logic;
         reg2txs : out bundle_reg2txs := (reg_sel => "10",
                                           rd_en   => '0',
                                           rd_nxt  => '1')
    );
end ex1;

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.tmp.all;

architecture top of ex1 is
begin
proc: process(clk)

constant null_reg2txs : bundle_reg2txs := (reg_sel => "01",
                                           rd_en   => '0',
                                           rd_nxt  => '1');
begin
    reg2txs <= null_reg2txs;
end process proc;
end top;

library ieee;
use ieee.std_logic_1164.all;
use work.tmp.all;

entity bug4691 is
    port (
    clk     : in  std_logic;
    reg_sel : out std_logic_vector(1 downto 0);
    rd_en   : out std_logic;
    rd_nxt  : out std_logic);
end;

architecture arch of bug4691 is
component ex1 is
    port(clk : in std_logic;
         reg2txs : out bundle_reg2txs := (reg_sel => "11",
                                           rd_en   => '0',
                                           rd_nxt  => '1')
    );
end component ex1;

begin

  U1 : ex1 port map (
    clk             => clk,
    reg2txs.reg_sel => reg_sel,
    reg2txs.rd_en   => rd_en,
    reg2txs.rd_nxt  => rd_nxt);

end arch;
