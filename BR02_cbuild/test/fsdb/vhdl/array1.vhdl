-- An array of non-record test. 
library ieee;
use ieee.std_logic_1164.all;

entity array1 is
  
  port (
    clk  : in  std_logic;
    in1  : in  std_logic_vector(3 downto 0);
    out1 : out std_logic_vector(3 downto 0));

end array1;

architecture arch of array1 is

  type arr is array (3 downto 0) of std_logic;

  signal sig : arr := (others => '0');
  
begin  -- arch

  proc: process (clk)
  begin  -- process proc
    if clk'event and clk = '1' then  -- rising clock edge
      for i in 3 downto 0 loop
        sig(i) <= in1(i);
        out1(i) <= sig(i);
      end loop;  -- i
    end if;
  end process proc;

end arch;
