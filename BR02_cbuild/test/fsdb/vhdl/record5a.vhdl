-- Test record ports with records as the actual nets, both in and out.

package p is

  type rec is record
                r1 : bit_vector(7 downto 0);
                r2 : bit_vector(3 downto 0);
              end record;

end p;

use work.p.all;

entity dut is
  port ( recinport : in rec
         ; recoutport: out rec
         );
end dut;

architecture a of dut is
begin
  recoutport.r1 <= recinport.r1;
  recoutport.r2 <= recinport.r2;
end a;


use work.p.all;

entity record5a is
  port (
    rir1 : in  bit_vector(7 downto 0);
    rir2 : in  bit_vector(3 downto 0);
    ror1 : out bit_vector(7 downto 0);
    ror2 : out bit_vector(3 downto 0));
end;

architecture arch of record5a is
  component dut is
                  port ( recinport : in rec
                         ; recoutport: out rec
                         );
  end component dut;
  signal rr1, rr2 : rec := ((others => '0'), (others => '0'));

begin

  u1 : dut port map (rr1, rr2);
  rr1 <= (rir1, rir2);
  (ror1, ror2) <= rr2;
end arch;
