-- Test array of records with a collection of data types in the record.
-- This test was inspired by the broadcom testcase for bug4739(top: dot11aphy)
-- the place where populate gets this wrong is for:
--      recOfTableA.oneTableArray := tableA;  -- put a tableArray into a tableArray
-- see rec_arr4.vhd for a testcase that has just a tableArray on the LHS where
-- populate gets this correct

library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec_all is
    record
      r3 : std_logic;
      i1 : integer;
      slv8: std_logic_vector(7 downto 0);
    end record;

  type rec_out is
    record
      slv8: std_logic_vector(7 downto 0);
    end record;
  
  type tableArray is array(NATURAL range <>) of std_logic_vector(7 downto 0);

  type recType is record
     oneTableArray:              tableArray(8 downto 0); 
  end record;

end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

-- this is the outermost level 
entity rec_arr3 is
  port ( inport_r3 : in std_logic
         ; inport_i1 : in integer
         ; inport_slv8 : in std_logic_vector(7 downto 0)
         ; recoutport1_slv8: out std_logic_vector(7 downto 0)
         ; clk: in std_logic
         );
end rec_arr3;

architecture a of rec_arr3 is
  signal temp : std_logic_vector(7 downto 0) := (others => '1');
begin

  p1: process (clk)

    variable tableA:          tableArray(8 downto 0) := (others => (others => '0'));
    variable recOfTableA:     recType;  -- contains a single entry that is a tableArray
  begin
    if clk'event and clk = '1' then

      tableA(inport_i1 mod 9) := inport_slv8;  -- put a 8bit slv into one word of a tableArray

      recOfTableA.oneTableArray := tableA;  -- put a tableArray into a tableArray

      temp <= tableA(1);

      tableA := recOfTableA.oneTableArray;  -- put a tableArray from within a record into a tableArray

    end if;
  end process p1;

  recoutport1_slv8 <= temp;
end a;



