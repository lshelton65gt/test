-- Test passing a record to a function (input param only)
-- test for integer in record.
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec is
    record
      r1 : std_logic_vector(7 downto 0);
      r2 : std_logic_vector(3 downto 0);
    end record;

    type rec2 is
    record
      r1 : integer;
      r3 : integer;
      r2 : std_logic_vector(3 downto 0);
    end record;

end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_intdut is
  port ( recinport : in rec
         ; intinport : in  integer
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end rec_func_i_intdut;

architecture a of rec_func_i_intdut is
  signal sigrec : rec;

  function func1 ( inrec : rec; b1, b2 : std_logic ) return std_logic_vector is
    variable outdata : std_logic_vector(11 downto 0);
  begin
    outdata := inrec.r1 & inrec.r2;
    outdata(7) :=  b1;
    outdata(1) :=  b2;
    return outdata;
  end func1;
  function func2 ( inrec : rec2; b1 : std_logic ) return std_logic_vector is
    variable outdata : std_logic_vector(3 downto 0);
  begin
    if inrec.r1 = inrec.r3 and b1 = '1' then
      outdata := "01" & inrec.r2(1 downto 0);
    else
      outdata := "10" & inrec.r2(3 downto 2);
    end if;
    return outdata;
  end func2;
begin

  p1: process (clk, intinport)
    variable result : std_logic_vector(7 downto 0);
    variable r1 : std_logic_vector(3 downto 0);
    variable r3 : std_logic_vector(3 downto 0);
    variable r2 : std_logic;
  begin
    if clk'event and clk = '1' then
      r2 := recinport.r1(0);
      r3 := func2 ((r3=>intinport, r1=>(intinport+1),r2=>recinport.r2), r2);
      r1 := func2 ((r2=>r3, others=>15), recinport.r1(7));
      result := r3 & r1;
      r1 := func2 ((r2=>r1, r1=>15, r3=> intinport), recinport.r1(4));
      sigrec.r2 <= r1;
      sigrec.r1 <= result;
    end if;
  end process p1;

  p3: process( sigrec )
  begin
    recoutport1 <= sigrec;
    recoutport2.r1 <= sigrec.r1;
    recoutport2.r2 <= sigrec.r2;
  end process;

end a;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity rec_func_i_int is
  port (
    clk            : in  std_logic;
    num            : in  integer;
    recinport_r1   : in  std_logic_vector(7 downto 0);
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recoutport1_r1 : out std_logic_vector(7 downto 0);
    recoutport1_r2 : out std_logic_vector(3 downto 0);
    recoutport2_r1 : out std_logic_vector(7 downto 0);
    recoutport2_r2 : out std_logic_vector(3 downto 0));
end rec_func_i_int;

architecture a of rec_func_i_int is
component rec_func_i_intdut is
  port ( recinport : in rec
         ; intinport : in integer
         ; recoutport1: out rec
         ; recoutport2: out rec
         ; clk: in std_logic
         );
end component rec_func_i_intdut;

begin

  dut : rec_func_i_intdut port map (
    clk            => clk,
    intinport      => num,
    recinport.r1   => recinport_r1,
    recinport.r2   => recinport_r2,
    recoutport1.r1 => recoutport1_r1,
    recoutport1.r2 => recoutport1_r2,
    recoutport2.r1 => recoutport2_r1,
    recoutport2.r2 => recoutport2_r2);
  
end a;
