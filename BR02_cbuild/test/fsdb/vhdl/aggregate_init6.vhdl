-- Test array aggregate initialization with a record array with named
-- association, further wacky record aggregates inside it, and OTHERS.
library ieee;
use ieee.std_logic_1164.all;
entity aggregate_init6 is
  port (in1 : in  integer range 0 to 6;
        out1: out boolean;
        out2: out std_logic_vector(3 downto 0));
end aggregate_init6;

architecture rtl of aggregate_init6 is
  type ctype is record
    faddr : std_logic_vector(3 downto 0);
    en    : boolean;
  end record;
  type sconfig is array (integer Range <> ) of ctype;
  constant configv : ctype := ((others => '0'), false);
  constant cfg_std : sconfig(0 to 6) := (
    5 => configv,
    4 to 4 => ("1010", true),
    2 to 3 => (en => false, faddr => (2 => '0', 1 downto 0 => '1', others => '0')),
    1 => (('0', others => '1'), true),
    others => configv
    );
begin
  out1 <= cfg_std(in1).en;
  out2 <= cfg_std(in1).faddr;
end rtl;
