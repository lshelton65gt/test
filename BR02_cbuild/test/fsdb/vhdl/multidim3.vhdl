-- This tests the ability to populate arrays of arrays of records and to
-- do element assigns and array indexing with them, as well as full record assigns.

entity multidim3 is
  port ( clk : in bit;
         in1 : in bit_vector(3 downto 0);
         o1, o2 : out bit_vector(3 downto 0) );
end entity;

architecture arch of multidim3 is
  constant cNumClkDomain : integer := 8;
  type tRange is record
                   high : bit_vector(3 downto 0);
                   low : bit_vector(3 downto 0);
                 end record;
  type tClkRange is array (cNumClkDomain-1 downto 0) of tRange;
  type tClkRangeArr is array (natural range <>) of tClkRange;
  signal s : tClkRangeArr(1 to 2)  := (others => (others => ("0000","0000")));    -- carbon observeSignal
begin

  p1 : process ( clk )
  begin
    if clk'event and clk = '1' then  -- rising clock edge
      for i in s'range loop
        for j in tClkRange'range loop
          s(i)(j).high <= in1;
          s(i)(j).low <= not in1;
        end loop;
      end loop;
    end if;
  end process;

  p2: process (clk)
    variable r1 : integer range tClkRange'range := tClkRange'left;
    variable r2 : integer range s'range := s'left;
    variable s1 : tClkRangeArr(1 to 2); 
  begin
    if clk'event and clk = '1' then  -- rising clock edge
      s1 := s;
      o1 <= s1(r2)(r1).high;
      o2 <= s1(r2)(r1).low;
      if r1 = 0 then
        r1 := 7;
      else
        r1 := r1 - 1;
      end if;
      if r2 = 1 then
        r2 := 2;
      else
        r2 := 1;
      end if;
    end if;
  end process p2;
  
end architecture;
