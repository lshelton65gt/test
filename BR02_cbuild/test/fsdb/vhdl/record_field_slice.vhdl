-- testing the ability to take a slice of an unconstrained memory type inside a
-- record.
library ieee;
use ieee.std_logic_1164.all;

entity record_field_slice is
  port (in1  : in  std_logic_vector(15 downto 0);
        out1 : out std_logic_vector(31 downto 0));
end;

architecture synth of record_field_slice is
  type f1Array is array(NATURAL range <>) of std_logic_vector(7 downto 0);
  type rectype is
    record
      f1 : f1Array(15 downto 0);
    end record;
  type rec1type is
    record
      f1: f1Array(7 downto 0); 
      f2: f1Array(7 downto 0);
      f3: f1Array(7 downto 0);
    end record;

  signal recval: rectype;               -- carbon observeSignal 
  signal rec1val: rec1type;             -- carbon observeSignal

begin
  rec1val.f1(7 downto 0) <= (others => in1(15 downto 8));
  rec1val.f2(7 downto 0) <= (others => in1(15 downto 8));
  rec1val.f3(7 downto 0) <= (others => in1(15 downto 8));

  recval.f1(7 downto 0) <= rec1val.f3(7 downto 0);
  recval.f1(15 downto 8) <= rec1val.f2(7 downto 0);

  out1 <= recval.f1(6) & rec1val.f2(5) & rec1val.f1(2) & rec1val.f3(7);
end synth;
