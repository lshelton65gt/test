-- Test array aggregate initialization with range choice
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity aggregate_init16 is
  port (d0 : in std_logic_vector(3 downto 0);
        d1 : in std_logic_vector(3 downto 0);
        d2 : in std_logic_vector(3 downto 0);
        d3 : in std_logic_vector(3 downto 0);
        addr : in std_logic_vector(4 downto 0);
        out1: out std_logic_vector(3 downto 0));
end aggregate_init16;

architecture rtl of aggregate_init16 is
  type ctype is record
    data : std_logic_vector(3 downto 0);
    en : std_logic;
  end record;
  type sconfig is array (Natural Range <> ) of ctype;
  constant c0 : ctype := ("1000", '0');
  constant c1 : ctype := ("1001", '1');
  constant c2 : ctype := ("1010", '0');
  constant c3 : ctype := ("1011", '1');
  signal mem : sconfig(31 downto 0) := (2 => c2, 1 => c1, 0 => c0,
                                        others => c3); --carbon observeSignal
begin

  mem <= ( 16#0# => (d2, '0'), 3 downto 1 => (d1, '1'), 4 => (d0, '0'), others => (d3, '1'));

  process(addr, mem)
    variable idx : integer;
  begin
    idx := to_integer(unsigned(addr));
    out1 <= mem(idx).data;
  end process;
end rtl;
