-- December 2007
-- This file tests an assignment of variable index of array of records on both
-- LHS and RHS.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is

  
  type complex_fixed_type is
    record
      real1    : integer;
      imag1    : integer;
    end record;


  constant COUNT : integer := 1;
  type arr_rec is array (0 to COUNT) of complex_fixed_type;
  
end FIXED_POINT_PKG;

use work.FIXED_POINT_PKG.all;


library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity testArRec is
  port (
    clk        : in bit;
    in1        : in integer;
    in2        : in integer;
    out1       : out integer;
    out2       : out integer;
    out3       : out integer;
    out4       : out integer);
end;

architecture arch of testArRec is
  signal temp1 : arr_rec := ((others => 0), (others => 0));
  signal temp2 : arr_rec := ((others => 0), (others => 0));
begin  -- process p1

  p1: process (clk)
    variable t1, t2 : integer := 0;
    variable rec_temp1 : complex_fixed_type := (0, 0);
  begin  -- process p1
    if clk'event and clk = '1' then  
      for i in 0 to COUNT loop
        t1 := in1;
        t2 := in2;
        rec_temp1 := (t1, t2);
        temp1(i) <= rec_temp1;          -- this line is tested
      end loop;  -- i
    end if;
  end process p1;

  p2: process (clk)
  begin  -- process p1
    if clk'event and clk = '1' then  
      for i in 0 to COUNT loop
        temp2(i) <= temp1(i);           -- this line is tested
      end loop;  -- i
    end if;
  end process p2;


  out1 <= temp2(0).real1;       
  out2 <= temp2(0).imag1;      
  out3 <= temp2(1).real1;       
  out4 <= temp2(1).imag1;      
end;
