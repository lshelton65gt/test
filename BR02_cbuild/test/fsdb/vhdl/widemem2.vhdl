-- Testing waveform dumping of wide, sparse memories.
-- This should not use the backdoor storage pointer

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity widemem2 is
  port (
    clk  : in  std_logic;
    addr : in  std_logic_vector (3 downto 0);
    we   : in  std_logic;
    din  : in  std_logic_vector (79 downto 0);
    dout : out std_logic_vector (79 downto 0));
end widemem2;

architecture arch of widemem2 is

  type myarray is array (0 to 15) of std_logic_vector (79 downto 0);
  signal arr : myarray := (others => (others => '0'));
  signal addr_int : integer;  -- carbon observeSignal
  
begin
  
  addr_int <= conv_integer(unsigned(addr));

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        arr(addr_int) <= din;
      else
        dout <= arr(addr_int);
      end if;
    end if;
  end process;
  
end arch;
