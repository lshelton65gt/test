-- From monthly/fsdb/vhdl/MdtN
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
        
package base is

   type days is (mon, tue, wed, thr, fri, sat, sun);

   type tZtoN_std_ulogic is array(0 to 1) of std_ulogic;

   type tZtoN_bit_vector is array(0 to 1) of bit_vector(0 to 1);

   type tZtoN_natural is array(0 to 1) of natural;

   type tZtoN_string is array(0 to 1) of string(1 to 2);

   type tZtoN_positive is array(0 to 1) of positive;

   type tZtoN_enum is array(0 to 1) of days;

   type tZtoN_character is array(0 to 1) of character;

   type tZtoN_unsigned is array(0 to 1) of unsigned(0 to 1);

   type tZtoN_signed is array(0 to 1) of signed(0 to 1);

   type tZtoN_boolean is array(0 to 1) of boolean;

   type tZtoN_std_logic is array(0 to 1) of std_logic;

   type tZtoN_integer is array(0 to 1) of integer;

   type tZtoN_bit is array(0 to 1) of bit;

   type tZtoN_std_ulogic_vector is array(0 to 1) of std_ulogic_vector(0 to 1);

   type tZtoN_std_logic_vector is array(0 to 1) of std_logic_vector(0 to 1);

   type tRec is record

      f1 : std_ulogic;

      f2 : bit_vector(0 to 1);

      f3 : natural;

      f4 : string(1 to 2);

      f5 : positive;

      f6 : days;

      f7 : character;

      f8 : unsigned(0 to 1);

      f9 : signed(0 to 1);

      f10 : boolean;

      f11 : std_logic;

      f12 : integer;

      f13 : bit;

      f14 : std_ulogic_vector(0 to 1);

      f15 : std_logic_vector(0 to 1);

   end record;

   type tNdtZ_std_ulogic is array(1 downto 0) of std_ulogic;

   type tNdtZ_bit_vector is array(1 downto 0) of bit_vector(0 to 1);

   type tNdtZ_natural is array(1 downto 0) of natural;

   type tNdtZ_string is array(1 downto 0) of string(1 to 2);

   type tNdtZ_positive is array(1 downto 0) of positive;

   type tNdtZ_enum is array(1 downto 0) of days;

   type tNdtZ_character is array(1 downto 0) of character;

   type tNdtZ_unsigned is array(1 downto 0) of unsigned(0 to 1);

   type tNdtZ_signed is array(1 downto 0) of signed(0 to 1);

   type tNdtZ_boolean is array(1 downto 0) of boolean;

   type tNdtZ_std_logic is array(1 downto 0) of std_logic;

   type tNdtZ_integer is array(1 downto 0) of integer;

   type tNdtZ_bit is array(1 downto 0) of bit;

   type tNdtZ_std_ulogic_vector is array(1 downto 0) of std_ulogic_vector(0 to 1);

   type tNdtZ_std_logic_vector is array(1 downto 0) of std_logic_vector(0 to 1);

   type tMdtN_std_ulogic is array(3 downto 2) of std_ulogic;

   type tMdtN_bit_vector is array(3 downto 2) of bit_vector(0 to 1);

   type tMdtN_natural is array(3 downto 2) of natural;

   type tMdtN_string is array(3 downto 2) of string(1 to 2);

   type tMdtN_positive is array(3 downto 2) of positive;

   type tMdtN_enum is array(3 downto 2) of days;

   type tMdtN_character is array(3 downto 2) of character;

   type tMdtN_unsigned is array(3 downto 2) of unsigned(0 to 1);

   type tMdtN_signed is array(3 downto 2) of signed(0 to 1);

   type tMdtN_boolean is array(3 downto 2) of boolean;

   type tMdtN_std_logic is array(3 downto 2) of std_logic;

   type tMdtN_integer is array(3 downto 2) of integer;

   type tMdtN_bit is array(3 downto 2) of bit;

   type tMdtN_std_ulogic_vector is array(3 downto 2) of std_ulogic_vector(0 to 1);

   type tMdtN_std_logic_vector is array(3 downto 2) of std_logic_vector(0 to 1);

   type tNtoM_std_ulogic is array(2 to 3) of std_ulogic;

   type tNtoM_bit_vector is array(2 to 3) of bit_vector(0 to 1);

   type tNtoM_natural is array(2 to 3) of natural;

   type tNtoM_string is array(2 to 3) of string(1 to 2);

   type tNtoM_positive is array(2 to 3) of positive;

   type tNtoM_enum is array(2 to 3) of days;

   type tNtoM_character is array(2 to 3) of character;

   type tNtoM_unsigned is array(2 to 3) of unsigned(0 to 1);

   type tNtoM_signed is array(2 to 3) of signed(0 to 1);

   type tNtoM_boolean is array(2 to 3) of boolean;

   type tNtoM_std_logic is array(2 to 3) of std_logic;

   type tNtoM_integer is array(2 to 3) of integer;

   type tNtoM_bit is array(2 to 3) of bit;

   type tNtoM_std_ulogic_vector is array(2 to 3) of std_ulogic_vector(0 to 1);

   type tNtoM_std_logic_vector is array(2 to 3) of std_logic_vector(0 to 1);

   type tRec_Rec is record

      f1 : tZtoN_std_ulogic;

      f2 : tZtoN_bit_vector;

      f3 : tZtoN_natural;

      f4 : tZtoN_string;

      f5 : tZtoN_positive;

      f6 : tZtoN_enum;

      f7 : tZtoN_character;

      f8 : tZtoN_unsigned;

      f9 : tZtoN_signed;

      f10 : tZtoN_boolean;

      f11 : tZtoN_std_logic;

      f12 : tZtoN_integer;

      f13 : tZtoN_bit;

      f14 : tZtoN_std_ulogic_vector;

      f15 : tZtoN_std_logic_vector;

      f16 : tRec;

      f18 : tNdtZ_std_ulogic;

      f19 : tNdtZ_bit_vector;

      f20 : tNdtZ_natural;

      f21 : tNdtZ_string;

      f22 : tNdtZ_positive;

      f23 : tNdtZ_enum;

      f24 : tNdtZ_character;

      f25 : tNdtZ_unsigned;

      f26 : tNdtZ_signed;

      f27 : tNdtZ_boolean;

      f28 : tNdtZ_std_logic;

      f29 : tNdtZ_integer;

      f30 : tNdtZ_bit;

      f31 : tNdtZ_std_ulogic_vector;

      f32 : tNdtZ_std_logic_vector;

      f34 : tMdtN_std_ulogic;

      f35 : tMdtN_bit_vector;

      f36 : tMdtN_natural;

      f37 : tMdtN_string;

      f38 : tMdtN_positive;

      f39 : tMdtN_enum;

      f40 : tMdtN_character;

      f41 : tMdtN_unsigned;

      f42 : tMdtN_signed;

      f43 : tMdtN_boolean;

      f44 : tMdtN_std_logic;

      f45 : tMdtN_integer;

      f46 : tMdtN_bit;

      f47 : tMdtN_std_ulogic_vector;

      f48 : tMdtN_std_logic_vector;

      f50 : tNtoM_std_ulogic;

      f51 : tNtoM_bit_vector;

      f52 : tNtoM_natural;

      f53 : tNtoM_string;

      f54 : tNtoM_positive;

      f55 : tNtoM_enum;

      f56 : tNtoM_character;

      f57 : tNtoM_unsigned;

      f58 : tNtoM_signed;

      f59 : tNtoM_boolean;

      f60 : tNtoM_std_logic;

      f61 : tNtoM_integer;

      f62 : tNtoM_bit;

      f63 : tNtoM_std_ulogic_vector;

      f64 : tNtoM_std_logic_vector;

   end record;

   type tMdtN_tRec_Rec is array(3 downto 2) of tRec_Rec;

   function To_Days (param : std_logic_vector) return days;

   function To_StdLogicVector (param : days) return std_logic_vector;
   
end base;

package body base is

  function To_Days (param : std_logic_vector) return days is
    variable index : std_logic_vector(0 to 2) := param;
    variable result : days;
  begin  -- To_Days
    case index is
      when "000" => result := mon;
      when "001" => result := tue;
      when "010" => result := wed;
      when "011" => result := thr;
      when "100" => result := fri;
      when "101" => result := sat;
      when "110" => result := sun;
      when others => result := mon;
    end case;
    return result;
  end To_Days;  

  function To_StdLogicVector (param : days) return std_logic_vector is
    variable result : std_logic_vector(0 to 2);
  begin  -- To_StdLogicVector
    case param is
      when mon => result := "000";
      when tue => result := "001";
      when wed => result := "010";
      when thr => result := "011";
      when fri => result := "100";
      when sat => result := "101";
      when sun => result := "110";
    end case;
    return result;
  end To_StdLogicVector;
  
end base;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity bottom is
   port ( bin1  : in tMdtN_tRec_Rec;
          bin2  : in tMdtN_tRec_Rec;
          bout1 : out tMdtN_tRec_Rec;
          bout2 : out tMdtN_tRec_Rec );
end bottom;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archbottom of bottom is

   signal s1 : tMdtN_tRec_Rec;

   signal s2 : tMdtN_tRec_Rec; 
   
begin

   s1 <= bin1;

   s2 <= bin2;

   bout1 <= s1;

   bout2 <= s2;

end archbottom;



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity MdtN_Rec_Rec is
   port ( pin1  : in  tMdtN_tRec_Rec;
          pin2  : in  tMdtN_tRec_Rec;
          pout1 : out tMdtN_tRec_Rec;
          pout2 : out tMdtN_tRec_Rec );
end MdtN_Rec_Rec;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archMdtN_Rec_Rec of MdtN_Rec_Rec is

   component bottom is
      port ( bin1  : in  tMdtN_tRec_Rec;
             bin2  : in  tMdtN_tRec_Rec;
             bout1 : out tMdtN_tRec_Rec;
             bout2 : out tMdtN_tRec_Rec);
   end component;

begin

   u1 : bottom port map ( bin1  => pin1,
                          bin2  => pin2,
                          bout1 => pout1,
                          bout2 => pout2);

end archMdtN_Rec_Rec;

