library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
        
package base is

   type tMdtN_bit is array(3 downto 2) of bit;

   type tMdtN_tMdtN_bit is array(3 downto 2) of tMdtN_bit;

end base;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity bottom is
   port ( clk   : in std_logic;
          bin1  : in tMdtN_tMdtN_bit;
          bin2  : in tMdtN_tMdtN_bit;
          bin3  : in tMdtN_tMdtN_bit;
          bout1 : out tMdtN_tMdtN_bit;
          bout2 : out tMdtN_tMdtN_bit;
          bout3 : out tMdtN_tMdtN_bit);
end bottom;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archbottom of bottom is

   signal s1 : tMdtN_tMdtN_bit;

   signal s2 : tMdtN_tMdtN_bit;

   signal s3 : tMdtN_tMdtN_bit;

begin

   s1 <= bin1;

   s2 <= bin2;

   bout1 <= s1;

   bout2 <= s2;

   process(clk)
   begin
     if clk'event and clk = '1' then
        s3 <= bin3;
     end if;
   end process;

   bout3 <= s3;

end archbottom;



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity MdtN_MdtN_bit is
   port ( clk   : in  std_logic;
          pin1  : in  tMdtN_tMdtN_bit;
          pin2  : in  tMdtN_tMdtN_bit;
          pin3  : in  tMdtN_tMdtN_bit;
          pout1 : out tMdtN_tMdtN_bit;
          pout2 : out tMdtN_tMdtN_bit;
          pout3 : out tMdtN_tMdtN_bit);
end MdtN_MdtN_bit;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archMdtN_MdtN_bit of MdtN_MdtN_bit is

   component bottom is
      port ( clk   : in  std_logic;
             bin1  : in  tMdtN_tMdtN_bit;
             bin2  : in  tMdtN_tMdtN_bit;
             bin3  : in  tMdtN_tMdtN_bit;
             bout1 : out tMdtN_tMdtN_bit;
             bout2 : out tMdtN_tMdtN_bit;
             bout3 : out tMdtN_tMdtN_bit);
   end component;

begin

   u1 : bottom port map ( clk   => clk,
                          bin1  => pin1,
                          bin2  => pin2,
                          bin3  => pin3,
                          bout1 => pout1,
                          bout2 => pout2,
                          bout3 => pout3);

end archMdtN_MdtN_bit;
