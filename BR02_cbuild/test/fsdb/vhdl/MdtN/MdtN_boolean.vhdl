-- From monthly/fsdb/vhdl/MdtN
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
        
package base is

   type days is (mon, tue, wed, thr, fri, sat, sun);

   type tMdtN_boolean is array(7 downto 4) of boolean;

end base;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity bottom is
   port ( bin1  : in tMdtN_boolean;
          bin2  : in tMdtN_boolean;
          bout1 : out tMdtN_boolean;
          bout2 : out tMdtN_boolean );
end bottom;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archbottom of bottom is

   signal s1 : tMdtN_boolean;

   signal s2 : tMdtN_boolean;
   
begin

   s1 <= bin1;

   s2 <= bin2;

   bout1 <= s1;

   bout2 <= s2;

end archbottom;



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity MdtN_boolean is
   port ( pin1  : in  tMdtN_boolean;
          pin2  : in  tMdtN_boolean;
          pout1 : out tMdtN_boolean;
          pout2 : out tMdtN_boolean );
end MdtN_boolean;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archMdtN_boolean of MdtN_boolean is

   component bottom is
      port ( bin1  : in  tMdtN_boolean;
             bin2  : in  tMdtN_boolean;
             bout1 : out tMdtN_boolean;
             bout2 : out tMdtN_boolean);
   end component;

begin

   u1 : bottom port map ( bin1  => pin1,
                          bin2  => pin2,
                          bout1 => pout1,
                          bout2 => pout2);

end archMdtN_boolean;

