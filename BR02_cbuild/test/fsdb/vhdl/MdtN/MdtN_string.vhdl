
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
        
package base is

   type tMdtN_string is array(3 downto 2) of string(1 to 2);

end base;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity bottom is
   port ( clk   : in std_logic;
          bin1  : in tMdtN_string;
          bin2  : in tMdtN_string;
          bin3  : in tMdtN_string;
          bout1 : out tMdtN_string;
          bout2 : out tMdtN_string;
          bout3 : out tMdtN_string);
end bottom;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archbottom of bottom is

   signal s1 : tMdtN_string;

   signal s2 : tMdtN_string; 

   signal s3 : tMdtN_string := ("AB", "CD"); 

begin

   s1 <= bin1;

   s2 <= bin2;

   bout1 <= s1;

   bout2 <= s2;

   process(clk)
   begin
     if clk'event and clk = '1' then
        s3 <= bin3;
     end if;
   end process;

   bout3 <= s3;

end archbottom;



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity MdtN_string is
   port ( clk   : in  std_logic;
          pin1  : in  tMdtN_string;
          pin2  : in  tMdtN_string;
          pin3  : in  tMdtN_string;
          pout1 : out tMdtN_string;
          pout2 : out tMdtN_string;
          pout3 : out tMdtN_string);
end MdtN_string;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archMdtN_string of MdtN_string is

   component bottom is
      port ( clk   : in  std_logic;
             bin1  : in  tMdtN_string;
             bin2  : in  tMdtN_string;
             bin3  : in  tMdtN_string;
             bout1 : out tMdtN_string;
             bout2 : out tMdtN_string;
             bout3 : out tMdtN_string);
   end component;

begin

   u1 : bottom port map ( clk   => clk,
                          bin1  => pin1,
                          bin2  => pin2,
                          bin3  => pin3,
                          bout1 => pout1,
                          bout2 => pout2,
                          bout3 => pout3);

end archMdtN_string;

