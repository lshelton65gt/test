-- From monthly/fsdb/vhdl/MdtN
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
        
package base is

   type days is (mon, tue, wed, thr, fri, sat, sun);

   type tRec is record

      f1 : std_ulogic;

      f2 : bit_vector(0 to 1);

      f3 : natural;

      f4 : string(1 to 2);

      f5 : positive;

      f6 : days;

      f7 : character;

      f8 : unsigned(0 to 1);

      f9 : signed(0 to 1);

      f10 : boolean;

      f11 : std_logic;

      f12 : integer;

      f13 : bit;

      f14 : std_ulogic_vector(0 to 1);

      f15 : std_logic_vector(0 to 1);

   end record;

   type tMdtN_tRec is array(3 downto 2) of tRec;

   function To_Days (param : std_logic_vector) return days;

   function To_StdLogicVector (param : days) return std_logic_vector;
   
end base;

package body base is

  function To_Days (param : std_logic_vector) return days is
    variable index : std_logic_vector(0 to 2) := param;
    variable result : days;
  begin  -- To_Days
    case index is
      when "000" => result := mon;
      when "001" => result := tue;
      when "010" => result := wed;
      when "011" => result := thr;
      when "100" => result := fri;
      when "101" => result := sat;
      when "110" => result := sun;
      when others => result := mon;
    end case;
    return result;
  end To_Days;  

  function To_StdLogicVector (param : days) return std_logic_vector is
    variable result : std_logic_vector(0 to 2);
  begin  -- To_StdLogicVector
    case param is
      when mon => result := "000";
      when tue => result := "001";
      when wed => result := "010";
      when thr => result := "011";
      when fri => result := "100";
      when sat => result := "101";
      when sun => result := "110";
    end case;
    return result;
  end To_StdLogicVector;
  
end base;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity bottom is
   port ( bin1  : in tMdtN_tRec;
          bin2  : in tMdtN_tRec;
          bout1 : out tMdtN_tRec;
          bout2 : out tMdtN_tRec );
end bottom;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archbottom of bottom is

   signal s1 : tMdtN_tRec;

   signal s2 : tMdtN_tRec; 
   
begin

   s1 <= bin1;

   s2 <= bin2;

   bout1 <= s1;

   bout2 <= s2;

end archbottom;



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

entity MdtN_Rec is
   port ( pin1  : in  tMdtN_tRec;
          pin2  : in  tMdtN_tRec;
          pout1 : out tMdtN_tRec;
          pout2 : out tMdtN_tRec );
end MdtN_Rec;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.base.all;

architecture archMdtN_Rec of MdtN_Rec is

   component bottom is
      port ( bin1  : in  tMdtN_tRec;
             bin2  : in  tMdtN_tRec;
             bout1 : out tMdtN_tRec;
             bout2 : out tMdtN_tRec);
   end component;

begin

   u1 : bottom port map ( bin1  => pin1,
                          bin2  => pin2,
                          bout1 => pout1,
                          bout2 => pout2);

end archMdtN_Rec;

