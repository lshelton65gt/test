-- A nested record testcase for waveform dump.
library ieee;
use ieee.std_logic_1164.all;

package pack is
  -- bottomrec is 5 bits total
  type bottomrec is record
                      b1     : std_logic;
                      t2     : std_logic_vector(3 downto 0);  -- reuse field name
                    end record;
end pack;

library ieee;
use ieee.std_logic_1164.all;
use work.pack.bottomrec;

package pack1 is
  -- midrec is 11 bits total
  type midrec is record
                   mid_nest1 : bottomrec;
                   m2        : std_logic;
                   mid_nest2 : bottomrec;  -- multiple instance of the same
                                           -- record type
                 end record;
  -- toprec is 16 bits total
  type toprec is record
                   t1        : std_logic_vector(1 to 2);
                   top_nest  : midrec;
                   t4        : std_logic_vector(2 to 4);
                 end record;
end pack1;


library ieee;
use ieee.std_logic_1164.all;
use work.pack.all;
use work.pack1.all;

entity nestedrecord5dut is
  port ( clk, reset: in std_logic;
         recinport : in toprec;
         recoutport1: out toprec;
         recoutport2: out toprec );
end nestedrecord5dut;

architecture a of nestedrecord5dut is
  signal sigrec : toprec;
begin
  -- read full nested record
  sigrec <= recinport;
  -- write full nested record
  recoutport1 <= sigrec;
  -- read & write leaves
  recoutport2.t1 <= recinport.t4(2 to 3);
  recoutport2.t4 <= recinport.t1 & sigrec.top_nest.mid_nest2.b1;
  recoutport2.top_nest.m2 <= not sigrec.top_nest.m2;
  -- read & write subrecords
  recoutport2.top_nest.mid_nest1 <= recinport.top_nest.mid_nest2;
  recoutport2.top_nest.mid_nest2 <= sigrec.top_nest.mid_nest1;
end a;


library ieee;
use ieee.std_logic_1164.all;
use work.pack1.all;

entity nestedrecord5 is
  port (
    clk, reset     : in  std_logic;
    recinport_t1   : in  std_logic_vector(1 downto 0);
    recinport_t4   : in  std_logic_vector(2 downto 0);
    recinport_m2   : in  std_logic;
    recinport_1_b1 : in  std_logic;
    recinport_1_t2 : in  std_logic_vector(3 downto 0);
    recinport_2_b1 : in  std_logic;
    recinport_2_t2 : in  std_logic_vector(3 downto 0);
    
    recoutport1_t1 : out std_logic_vector(1 downto 0);
    recoutport1_t4 : out std_logic_vector(2 downto 0);
    recoutport1_m2   : out  std_logic;
    recoutport1_1_b1 : out  std_logic;
    recoutport1_1_t2 : out  std_logic_vector(3 downto 0);
    recoutport1_2_b1 : out  std_logic;
    recoutport1_2_t2 : out  std_logic_vector(3 downto 0);

    recoutport2_t1 : out std_logic_vector(1 downto 0);
    recoutport2_t4 : out std_logic_vector(2 downto 0);
    recoutport2_m2   : out  std_logic;
    recoutport2_1_b1 : out  std_logic;
    recoutport2_1_t2 : out  std_logic_vector(3 downto 0);
    recoutport2_2_b1 : out  std_logic;
    recoutport2_2_t2 : out  std_logic_vector(3 downto 0)
);
end nestedrecord5;

architecture a of nestedrecord5 is
  component nestedrecord5dut
    is
      port ( clk, reset: in std_logic;
             recinport : in toprec;
             recoutport1: out toprec;
             recoutport2: out toprec );
  end component nestedrecord5dut;

begin

  dut : nestedrecord5dut port map (
    clk            => clk,
    reset          => reset,
    recinport.t1   => recinport_t1,
    recinport.t4   => recinport_t4,
    recinport.top_nest.m2 => recinport_m2,
    recinport.top_nest.mid_nest1.b1 => recinport_1_b1,
    recinport.top_nest.mid_nest1.t2 => recinport_1_t2,
    recinport.top_nest.mid_nest2.b1 => recinport_2_b1,
    recinport.top_nest.mid_nest2.t2 => recinport_2_t2,
    
    recoutport1.t1 => recoutport1_t1,
    recoutport1.t4 => recoutport1_t4,
    recoutport1.top_nest.m2 => recoutport1_m2,
    recoutport1.top_nest.mid_nest1.b1 => recoutport1_1_b1,
    recoutport1.top_nest.mid_nest1.t2 => recoutport1_1_t2,
    recoutport1.top_nest.mid_nest2.b1 => recoutport1_2_b1,
    recoutport1.top_nest.mid_nest2.t2 => recoutport1_2_t2,
    
    recoutport2.t1 => recoutport2_t1,
    recoutport2.t4 => recoutport2_t4,
    recoutport2.top_nest.m2 => recoutport2_m2,
    recoutport2.top_nest.mid_nest1.b1 => recoutport2_1_b1,
    recoutport2.top_nest.mid_nest1.t2 => recoutport2_1_t2,
    recoutport2.top_nest.mid_nest2.b1 => recoutport2_2_b1,
    recoutport2.top_nest.mid_nest2.t2 => recoutport2_2_t2 );
  
end a;
