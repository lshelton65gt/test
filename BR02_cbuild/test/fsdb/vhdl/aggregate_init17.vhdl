-- Test array aggregate initialization with expressions for choices.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity aggregate_init17 is
  port (d0 : in std_logic_vector(3 downto 0);
        d1 : in std_logic_vector(3 downto 0);
        d2 : in std_logic_vector(3 downto 0);
        d3 : in std_logic_vector(3 downto 0);
        addr : in std_logic_vector(4 downto 0);
        out1: out std_logic_vector(3 downto 0));
end aggregate_init17;

architecture rtl of aggregate_init17 is
  type ctype is record
    data : std_logic_vector(3 downto 0);
    en : std_logic;
  end record;
  type sconfig is array (Natural Range <> ) of ctype;
  constant c0 : ctype := ("1000", '0');
  constant c1 : ctype := ("1001", '1');
  constant c2 : ctype := ("1010", '0');
  constant c3 : ctype := ("1011", '1');
  signal mem : sconfig(31 downto 0) := (2 => c2, 1 => c1, 0 => c0,
                                        others => c3); -- carbon observeSignal

  constant ci0 : integer := 2;
  constant ci1 : integer := 1;
begin

  mem <= ( ci0 downto ci1 => (d2, '0'), (ci0 + ci1) => (d1, '1'), d0'length => (d0, '0'), others => (d3, '1'));

  process(addr, mem)
    variable idx : integer;
  begin
    idx := to_integer(unsigned(addr));
    out1 <= mem(idx).data;
  end process;
end rtl;
