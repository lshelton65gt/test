library ieee;
use ieee.std_logic_1164.all;

entity bug11354_sub is
  port (
    clk : in  std_logic;
    en  : in  std_logic;
    i   : in  std_logic_vector (7 downto 0);
    o   : out std_logic_vector (7 downto 0));
end bug11354_sub;

architecture arch of bug11354_sub is
begin

  process (clk)
  begin
    if clk'event and clk = '1' then
      if en = '1' then
        o <= i;
      end if;
    end if;
  end process;

end arch;
