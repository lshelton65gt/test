library ieee;
use ieee.std_logic_1164.all;

entity vhdl_bottom is
  port ( bin  : in  std_logic;
         bout : out std_logic);
end vhdl_bottom;

architecture toparch of vhdl_bottom is

begin  -- toparch

  bout <= bin;

end toparch;
