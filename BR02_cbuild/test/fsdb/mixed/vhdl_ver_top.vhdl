library ieee;
use ieee.std_logic_1164.all;

entity vhdl_ver_top is
  port ( pin  : in  std_logic;
         pout : out std_logic);
end vhdl_ver_top;

architecture toparch of vhdl_ver_top is

  component ver_middle
    port (
      min  : in  std_logic;
      mout : out std_logic);
  end component;
  
begin  -- toparch

  m1 : ver_middle port map ( min  => pin,
                             mout => pout);
    

end toparch;
