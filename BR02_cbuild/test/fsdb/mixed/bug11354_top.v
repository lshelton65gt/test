module bug11354_top(clk, en, in1, in2, out1, out2);
   input clk, en;
   input [7:0] in1, in2;
   output [7:0] out1, out2;

   bug11354_sub sub1(.clk(clk),
                     .en(en),
                     .i(in1),
                     .o(out1));

   sub2 sub2(.clk(clk),
             .en(en),
             .in(in2),
             .out(out2));

endmodule

module sub2(clk, en, in, out);
   input clk, en;
   input [7:0] in;
   output [7:0] out;

   genvar       i;

   generate for (i = 0; i < 8; i = i + 1) begin : gen_flop
      flop flop(.clk(clk), .en(en), .in(in[i]), .out(out[i]));
   end
   endgenerate

endmodule

module flop(clk, en, in, out);
   input clk, en, in;
   output out;
   reg    out;

   always @(posedge clk)
     if (en)
       out <= in;
endmodule
