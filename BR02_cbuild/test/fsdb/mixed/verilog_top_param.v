// May 2008
module ver_top_param(clk, rst, in_dat, out_dat);
   parameter wiDth = 8;
  input   clk;
  input   rst;
  input[wiDth - 1:0]  in_dat;
  output[wiDth - 1:0] out_dat;

wire    dat_1l;
wire    dat_2l;
wire    dat_3l;
wire    dat_tmp;


// The width is written that way, to test that with -vhdlCase preserve
// switch, CBUILD does the mixed language boundary matching case insensitive way.
// Note that the name of the entity has different case as well.
vhdl_SUB #(.WiDtH(wiDth))  sub1(.clk(clk), .rst(rst), .din(in_dat), .dout(out_dat));

endmodule


