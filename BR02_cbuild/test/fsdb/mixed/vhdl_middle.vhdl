library ieee;
use ieee.std_logic_1164.all;

entity vhdl_middle is
  port ( min  : in  std_logic;
         mout : out std_logic);
end vhdl_middle;

architecture toparch of vhdl_middle is

  component ver_bottom
    port ( bin  : in  std_logic;
           bout : out std_logic);
  end component;
  
begin  -- toparch

  b1 : ver_bottom port map ( bin  => min,
                             bout => mout);
    

end toparch;
