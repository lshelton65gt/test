library IEEE;
use IEEE.std_logic_1164.all;

entity vhdl_sub is
  
  generic (
    Width : integer := 4);

  port (
    clk  : in  std_logic;
    rst  : in  std_logic;
    din  : in  std_logic_vector(Width-1 downto 0);
    dout : out std_logic_vector(Width-1 downto 0));

end vhdl_sub;

architecture vhdl_arch of vhdl_sub is

begin 

  p1: process (clk, rst)
  begin  -- process p1
    if rst = '0' then                   
      dout <= (others => '0');
    elsif clk'event and clk = '1' then  
      dout <= din;
    end if;
  end process p1;

end vhdl_arch;
