module ver_bottom(bin, bout);
   input bin;
   output bout;

   assign bout = bin;

endmodule
