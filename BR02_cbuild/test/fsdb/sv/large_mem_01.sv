// filename: test/fsdb/sv/large_mem_01.sv
// Description:  This test is used to check dumpsizelimit with SystemVerilog
// memories.  The memory should be too large to dump without setting
// -waveformDumpSizeLimit 0 (which is done in the large_mem_01_unlimited version
// of this test.

// we use a define for the top level module name so we can get different names for the same rtl
// this file is used for both large_mem_01 and large_mem_01_unlimited testcases
`define TOPNAME large_mem_01
module `TOPNAME(input clock, input [7:0] in1, in2, output reg [7:0] out1);

   reg [31:0] mem [0:10230];

   initial begin
      for (int jj = 0; jj < 1024; jj++)
	 begin
	    mem[jj] = jj;
	 end
   end
   always @(posedge clock)
     begin: main
	mem [in1] = {4{in2}};
       out1 = in1 & mem[in2];
     end
endmodule
