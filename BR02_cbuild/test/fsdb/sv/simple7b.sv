// test simple7b,    1 packed 1 unpacked dimensions
// checks wavedumpable objects that have multiple dimensions

module simple7b(in1, out1) ;
   input [2:0] in1;
   output [7:5] out1 [2:3];

   assign out1[2] = in1;
   assign out1[3] = ~in1;
   
endmodule
