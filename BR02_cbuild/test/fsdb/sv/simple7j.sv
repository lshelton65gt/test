// filename: test/fsdb/sv/simple7j.sv
// Description:  This test has a CARBON INTERNAL ERROR  with CMS v5.16.1
// it shows that popresynth is not quite right for conditional expressions  (the
// rhs of the assign is not correct)
// this does not have CARBON INTERNAL ERROR  if PA is 1


module simple7j #(parameter PA=8, parameter PB=10, parameter PC=4) (output [PA:0][PB:0] out1 [PC:0], input [2:0] ctrl);

   reg [PA:0][PB:0] mem [PC:0];

   assign       out1 = (ctrl == 3'b0) ? mem : mem ;
//   assign       out1 = mem ;
endmodule

