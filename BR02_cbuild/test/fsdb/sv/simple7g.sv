// filename: test/fsdb/sv/simple7g.sv
//                  4 packed dimensions, wide enough so it requires more than 1 word
// checks wavedumpable objects that have multiple dimensions

module simple7g(in1, out1) ;
   input [95:0] in1;
   output [14:12][11:8][7:0] out1; // should be 3*4*8 bits wide 96 bits

   assign out1 = in1;
   
endmodule
