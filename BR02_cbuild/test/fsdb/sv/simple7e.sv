// test simple7e,    1 packed 2 unpacked dimensions
// checks wavedumpable objects that have multiple dimensions

module simple7e(in1, out1) ;
   input [2:0] in1;
   output [10:8] out1 [2:3] [4:7];

   assign out1[2][4] = in1;
   assign out1[3][4] = ~in1;

   assign out1[2][5] = in1;
   assign out1[3][5] = ~in1;

   assign out1[2][6] = in1;
   assign out1[3][6] = ~in1;

   assign out1[2][7] = in1;
   assign out1[3][7] = ~in1;
   
endmodule
