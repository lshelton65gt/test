// filename: test/fsdb/sv/simple7i.sv
// Description:  This test has a CARBON INTERNAL ERROR with CMS v5.16.1
// it appears that the issue is caused by the implicit dimension for an array
// with multiple packed dimensions (and no unpacked dimensions) is not added to
// the read of a temporary created for port lowering.
// A specific example from libdesign.popresynth/simple7i.v:
//  assign out1[0][(7 * 10) +: 10] = $mr_lower_out_out1[0][7];1[9:0]; // simple7i.sv:14
// should probably be:
//  assign out1[0][(7 * 10) +: 10] = $mr_lower_out_out1[0][7];1[0:0][9:0]; // simple7i.sv:14

module simple7i(ctrl, in1, out1) ;
   parameter NBA = 10;
   parameter NPORTS = 8;
   input [2:0] ctrl;
   input  [NPORTS-1:0][NBA-1:0] in1;
   output [NPORTS-1:0][NBA-1:0] out1;


      for (genvar PB = 0; PB< NPORTS; PB++) begin: SEQ_INST
	 memc_core_sequencer u_core_sequencer(ctrl, in1[PB], out1[PB]);	// incorrect port connections
      end
   
endmodule


module memc_core_sequencer #(parameter NPORTS=8, parameter NBA=10) (
    input [2:0]                    ctrl,
    input [NBA-1:0] [NPORTS:0]     i_data,
    output reg [NPORTS:0][NBA-1:0] seq2axi_bsc_bnk_active);

   wire [NBA-1:0] [NPORTS:0] 	  w_bsc_bnk_active;

   for ( genvar bnkIdx = 0; bnkIdx  < 8; bnkIdx = bnkIdx+1) begin: bnkStatus
      memc_core_bank_status bank_status(.ctrl(ctrl), .o_bsc_actv(w_bsc_bnk_active[bnkIdx]), .i_data(i_data[bnkIdx]));
   end

   
   always @* begin
      for( int ii=0; ii < NBA; ii++) begin
	 for (int jj = 0; jj <= NPORTS; jj++) begin
	    seq2axi_bsc_bnk_active [jj][ii] = w_bsc_bnk_active [ii][jj];
	 end
      end
   end

   
endmodule

module memc_core_bank_status #(NPORTS=8) (input [2:0] ctrl, output reg [NPORTS:0]o_bsc_actv, input [NPORTS:0]i_data);
   
   wire 	[NPORTS:0] w1;	// note this is undriven
   always @(*)
      begin
	 case (ctrl)
	   // the logic here is just something that makes different assignments
	   // to o_bsc_actv
	   2'b00: o_bsc_actv = ~ i_data;
	   2'b01: o_bsc_actv = {NPORTS{1'b0}};
	   2'b10: o_bsc_actv = i_data | w1;
	   2'b11: o_bsc_actv = {i_data[0],i_data[NPORTS-1:1]};
	 endcase
      end

endmodule
