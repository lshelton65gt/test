// filename: test/fsdb/sv/simple7h.sv
// Description:  This test is a larger (and multiple levels of modules) version
// of simple7a.


module simple7h #(parameter NBA = 10, parameter NPORTS = 8)(
   input   [2:0] ctrl,
   input      [NPORTS:0] [NBA-1:0] in1, // note that in1 and out1 have total matching size, but do not have matching dimensions

   output reg [NBA-1:0]  [NPORTS:0] out1,
   output reg [(NPORTS*2):1] 	   out2,
   output reg [1:0] 		   out3,
    output reg 			   out4);
  
   wire      [NPORTS:0] [NBA-1:0] w1;

   memc_core #(.NBA(NBA),.NPORTS(NPORTS)) u1(.ctrl(ctrl), .in1(in1), .out1(w1));

	 
   always @(*) begin
      for (int ii = 0; ii < NBA; ii++) begin
	for (int jj = 0; jj < NPORTS; jj++) begin
	   out1[ii][jj] = w1[jj][ii];
	end
      end
   end

   always @(*) begin
      for (int ii = 1; ii <= 2; ii++) begin
	 out2[(NPORTS*(ii))-:NPORTS] = out1[ii];
      end
   end

   assign out3 = {out1[1][1], out2[2]};
   always @(*) begin
      out4 = w1[2][2];
   end

   
   
endmodule

module memc_core_axi #(parameter NPORTS=8, parameter NBA=10, parameter NHCC_AXI=4)
   (output [NPORTS:0][NBA-1:0] seq2axi_bsc_bnk_active [NHCC_AXI-1:0], input [2:0] ctrl);

   reg [NPORTS:0][NBA-1:0] seq2axi_bsc_bnk_active_m [NHCC_AXI-1:0];


   initial
     begin
	for (integer nba=0; nba <= NBA; nba++) begin
	   for (integer nhcc_axi=0; nhcc_axi<= NHCC_AXI; nhcc_axi++) begin
	      seq2axi_bsc_bnk_active_m[nhcc_axi][nba] = nhcc_axi*nba;
	   end
	end
     end

   assign       seq2axi_bsc_bnk_active = (ctrl == 0) ? seq2axi_bsc_bnk_active_m : seq2axi_bsc_bnk_active_m ;
endmodule

module memc_core(ctrl, in1, out1) ;
   parameter NBA = 10;
   parameter NPORTS = 8;
   parameter NHCC_AXI = 2;
   input [2:0] ctrl;
   input  [NPORTS:0][NBA-1:0] in1;
   output [NPORTS:0][NBA-1:0] out1;   

   wire [NPORTS:0][NBA-1:0] seq2axi_bsc_bnk_active [NHCC_AXI-1:0];

   memc_core_axi #(.NBA(NBA),.NPORTS(NPORTS), .NHCC_AXI(NHCC_AXI)) u_memc_core_axi(.seq2axi_bsc_bnk_active(seq2axi_bsc_bnk_active), .ctrl(ctrl));

   for (genvar g0 = 0; g0< NHCC_AXI; g0++) begin: SEQ_INST
      memc_core_sequencer #(.NBA(NBA),.NPORTS(NPORTS))u_core_sequencer(ctrl, in1, seq2axi_bsc_bnk_active[g0]);
   end

   assign out1 = seq2axi_bsc_bnk_active[ctrl];
endmodule


module memc_core_sequencer #(parameter NPORTS=8, parameter NBA=10) (
    input [2:0]                    ctrl,
    input [NBA-1:0] [NPORTS:0]     i_data,
    output reg [NPORTS:0][NBA-1:0] seq2axi_bsc_bnk_active);

   wire [NBA-1:0] [NPORTS:0] 	  w_bsc_bnk_active;

   for ( genvar bnkIdx = 0; bnkIdx  < 8; bnkIdx = bnkIdx+1) begin: bnkStatus
      memc_core_bank_status bank_status(.ctrl(ctrl), .o_bsc_actv(w_bsc_bnk_active[bnkIdx]), .i_data(i_data[bnkIdx]));
   end

   
   always @* begin
      for( int ii=0; ii < NBA; ii++) begin
	 for (int jj = 0; jj <= NPORTS; jj++) begin
	    seq2axi_bsc_bnk_active [jj][ii] = w_bsc_bnk_active [ii][jj];
	 end
      end
   end

   
endmodule

module memc_core_bank_status #(NPORTS=8) (input [2:0] ctrl, output reg [NPORTS:0]o_bsc_actv, input [NPORTS:0]i_data);
   
   wire 	[NPORTS:0] w1 = 0;
   always @(*)
      begin
	 case (ctrl)
	   2'b00: o_bsc_actv = ~ i_data;
	   2'b01: o_bsc_actv = {NPORTS{1'b0}};
	   2'b10: o_bsc_actv = i_data | w1;
	   2'b11: o_bsc_actv = {i_data[0],i_data[NPORTS-1:1]};
	 endcase
      end

endmodule
