// test simple7f,    2 packed 1 unpacked dimensions
// checks wavedumpable objects that have multiple dimensions

module simple7f(in1, out1) ;
   input [1:0] in1;
   output [10:8][2:3] out1 [4:7];

   assign out1[4][10] = in1;
   assign out1[4][ 9] = ~in1;
   assign out1[4][ 8] = in1;

   assign out1[5][10] = in1;
   assign out1[5][ 9] = ~in1;
   assign out1[5][ 8] = in1;

   assign out1[6][10] = in1;
   assign out1[6][ 9] = ~in1;
   assign out1[6][ 8] = in1;

   assign out1[7][10] = in1;
   assign out1[7][ 9] = ~in1;
   assign out1[7][ 8] = in1;

   
endmodule
