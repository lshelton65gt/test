// test simple7a,    2 packed dimensions
// checks wavedumpable objects that have multiple dimensions

module simple7a(in1, out1) ;
   input [3:0] in1;
   output [7:6][3:0] out1;

   assign out1 = {in1,(~in1)};
   
endmodule
