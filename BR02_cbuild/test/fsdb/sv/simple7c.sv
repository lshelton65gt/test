// test simple7c,    0 packed 2 unpacked dimensions
// checks wavedumpable objects that have multiple dimensions

module simple7c(in1, out1) ;
   input [5:0] in1;
   output out1 [2:3]  [7:5];

   assign out1[2][7] = in1[0];
   assign out1[2][6] = in1[1];
   assign out1[2][5] = in1[2];
   assign out1[3][7] = in1[3];
   assign out1[3][6] = in1[4];
   assign out1[3][5] = in1[5];
   
endmodule
