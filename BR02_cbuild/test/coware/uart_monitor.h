#include "systemc.h"
#include "scml.h"

SC_MODULE(monitor)
{
  sc_in<bool> txclk, rxclk;
  sc_in<bool>         reset;
  sc_in<bool>         ld_tx_data;
  sc_in<sc_uint<8> >  tx_data;
  sc_in<bool>         tx_enable;
  sc_in<bool>         tx_out;
  sc_in<bool>         tx_empty;
  sc_in<bool>         uld_rx_data;
  sc_in<bool>         rx_enable;
  sc_in<bool>         rx_in;
  sc_in<sc_uint<8> >  rx_data;
  sc_in<bool>         rx_empty;
  sc_in<sc_uint<4> >  rx_cnt;
  sc_in<bool>         rx_busy;
  sc_in<sc_uint<4> >  rx_sample_cnt;
  sc_in<bool>         rx_frame_err;
  sc_in<bool>         rx_ready;

  void prc_monitor();
  
  SC_CTOR(monitor) 
  {
    SC_METHOD(prc_monitor) 
    {
      sensitive << txclk << rxclk;
    }
  }
};
