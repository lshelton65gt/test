/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "systemc.h"
// Need to include the libscml.h file in order to get access to the Carbon model of 
// the scml and its API
#include "libuart.h"

SC_MODULE(uart_tb)
{
  sc_out<bool>        reset;
  sc_out<bool>        ld_tx_data;
  sc_out<sc_uint<8> > tx_data;
  sc_out<bool>        tx_enable;

  sc_in<bool>         tx_out;
  sc_in<bool>         tx_empty;


  sc_out<bool>        uld_rx_data;
  sc_out<bool>        rx_enable;
  sc_out<bool>        rx_in;
 
  sc_in<sc_uint<8> >  rx_data;
  sc_in<bool>         rx_empty;
	
  void test_driver();
  void log_msg(const char* msg);
  void transmit_data(unsigned int d);

  SC_CTOR(uart_tb)
  {
    SC_THREAD(test_driver);
  }
};
