/*****************************************************************************

 Copyright (c) 2007-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "libcounter.systemc.h"
#include "counter_tb.h"

int sc_main (int argc , char *argv[]) 
{

  sc_signal<bool>         rst;
  sc_signal<bool>         en;
  sc_signal<sc_uint<16> > c;

  sc_clock clk("clk", sc_time(3, SC_NS), 1.5, sc_time(0.0, SC_NS));

  counter COUNTER("COUNTER");
  COUNTER.reset(rst);
  COUNTER.clk(clk);
  COUNTER.enable(en);
  COUNTER.value(c);

  counter_tb COUNTER_TB("COUNTER_TB");
  COUNTER_TB.rst(rst);
  COUNTER_TB.en(en);
  COUNTER_TB.clk(clk);
  COUNTER_TB.val(c);
  
  sc_start(sc_time(0, SC_NS));
  sc_start(sc_time(10000, SC_NS));
  sc_stop();

  cout << "CARBON: Simulation exited at time " << sc_time_stamp().value() << endl;
  cout << "Last Counter Value: 0x" << hex << c << dec << endl;

  return 0;
}
