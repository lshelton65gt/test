// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "libmem1.systemc.h"
#include "check.h"
#include "scml_mem_tb.h"

#define MEM_SPACE_SIZE        256

int sc_main(int argc, char* argv[])
{
  sc_signal< sc_uint<64> >       rdata;
  sc_signal< sc_uint<8> >        raddr;
  sc_signal< bool >              wen;
  sc_signal< sc_uint<8> >        waddr;
  sc_signal< sc_uint<64> >       wdata;
  sc_clock clk("clk", sc_time(3, SC_NS), 1.5, sc_time(0.0, SC_NS));

  mem1 SCML("SCML");
  SCML.rdata(rdata);
  SCML.raddr(raddr);
  SCML.wen(wen);
  SCML.waddr(waddr);
  SCML.wdata(wdata);
  SCML.clk(clk);

  // An array to store the data read from the RTL memory
  unsigned long long rtlData[MEM_SPACE_SIZE];

  scml_mem_tb TB("TB", &rtlData[0], MEM_SPACE_SIZE);
  TB.rdata(rdata);
  TB.raddr(raddr);
  TB.wen(wen);
  TB.waddr(waddr);
  TB.wdata(wdata);
  TB.clk(clk);

  // init
  sc_start(sc_time(0, SC_NS));

  unsigned int errors = 0;

  // Test the mem32 memory with 32-bit, 16-bit, and 8-bit operations
  // Map: 0x0 - 0xF
  unsigned int pattern = 0xa0b1c2d3;
  scml_memory<unsigned int> mem32_gold("mem32_gold", scml_memsize(8));
  mem32_gold.set_addressing_mode(8); 
  errors += testMemory<unsigned int, 32, 32, 17, 0, 7>("mem32", SCML.m_mem32, mem32_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 0, 7>("mem32", SCML.m_mem32, mem32_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 0, 7>("mem32", SCML.m_mem32, mem32_gold, pattern);
  
  // Test the mem36 memory with 32-bit, 16-bit, and 8-bit operations
  // Note that this memory is 32 bits wide and takes a part of the
  // 36-bit wide RTL memory.
  // Map: 0x10 - 0x1F
  scml_memory<unsigned int> mem36_gold("mem36_gold", scml_memsize(8));
  mem36_gold.set_addressing_mode(8); 
  errors += testMemory<unsigned int, 32, 32, 17, 0, 7>("mem36", SCML.m_mem36, mem36_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 0, 7>("mem36", SCML.m_mem36, mem36_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 0, 7>("mem36", SCML.m_mem36, mem36_gold, pattern);
  
  // Test the mem40 memory with 40-bit, 32-bit, 16-bit, and 8-bit
  // operations Note that this memory is 64 bits wide and leaves a
  // hole between bits 40 and 63 because the RTL is 40 bits wide
  // Map: 0x20 - 0x2F
  scml_memory<unsigned long long> mem40_gold("mem40_gold", scml_memsize(8));
  mem40_gold.set_addressing_mode(8); 
  unsigned long long pattern64 = 0x00000078a0b1c2d3ULL;
  errors += testMemory<unsigned long long, 40, 40, 13, 0, 7>("mem40", SCML.m_mem40, mem40_gold, pattern);
  errors += testMemory<unsigned long long, 40, 32, 17, 0, 7>("mem40", SCML.m_mem40, mem40_gold, pattern);
  errors += testMemory<unsigned long long, 40, 16, 19, 0, 7>("mem40", SCML.m_mem40, mem40_gold, pattern);
  errors += testMemory<unsigned long long, 40, 8, 37, 0, 7>("mem40", SCML.m_mem40, mem40_gold, pattern);
  
  // Test the mem64 memory with 64, 32, 16, and 8-bit operations
  // Map: 0x30 - 0x3F
  scml_memory<unsigned long long> mem64_gold("mem64_gold", scml_memsize(8));
  mem64_gold.set_addressing_mode(8); 
  pattern64 = 0x12345678a0b1c2d3ULL;
  errors += testMemory<unsigned long long, 64, 64, 17, 0, 7>("mem64", SCML.m_mem64, mem64_gold, pattern64);
  errors += testMemory<unsigned long long, 64, 32, 19, 0, 7>("mem64", SCML.m_mem64, mem64_gold, pattern64);
  errors += testMemory<unsigned long long, 64, 16, 37, 0, 7>("mem64", SCML.m_mem64, mem64_gold, pattern64);
  errors += testMemory<unsigned long long, 64, 8, 39, 0, 7>("mem64", SCML.m_mem64, mem64_gold, pattern64);

  // Test the interleaved memory with 32, 16, and 8-bit operations
  // Map: 0x40 - 0x4F
  scml_memory<unsigned int> mem_inter_gold("mem_inter_gold", scml_memsize(8));
  mem_inter_gold.set_addressing_mode(8);
  errors += testMemory<unsigned int, 32, 32, 17, 0, 7>("mem_inter", SCML.m_mem_inter, mem_inter_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 0, 7>("mem_inter", SCML.m_mem_inter, mem_inter_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 0, 7>("mem_inter", SCML.m_mem_inter, mem_inter_gold, pattern);

  // Test the interleaved and cascaded memory with 32, 16, and 8-bit operations
  // Map: 0x50 - 0x5F
  scml_memory<unsigned int> mem_both_gold("mem_both_gold", scml_memsize(16));
  mem_both_gold.set_addressing_mode(8);
  errors += testMemory<unsigned int, 32, 32, 17, 0, 15>("mem_both", SCML.m_mem_both, mem_both_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 0, 15>("mem_both", SCML.m_mem_both, mem_both_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 0, 15>("mem_both", SCML.m_mem_both, mem_both_gold, pattern);

  // Test the a memory with holes with 32, 16, and 8-bit operations
  // Map: 0x60 - 0x6F
  scml_memory<unsigned int> mem_holes_gold("mem_holes_gold", scml_memsize(16));
  mem_holes_gold.set_addressing_mode(8);
  errors += testMemory<unsigned int, 32, 32, 17, 0, 3>("mem_holes", SCML.m_mem_holes, mem_holes_gold, pattern);
  errors += testMemory<unsigned int, 32, 32, 17, 8, 11>("mem_holes", SCML.m_mem_holes, mem_holes_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 0, 3>("mem_holes", SCML.m_mem_holes, mem_holes_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 8, 11>("mem_holes", SCML.m_mem_holes, mem_holes_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 0, 3>("mem_holes", SCML.m_mem_holes, mem_holes_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 8, 11>("mem_holes", SCML.m_mem_holes, mem_holes_gold, pattern);

  // Test a memory where the depth is reversed in the RTL [0:7]
  // Map: 0x70 - 0x7F
  scml_memory<unsigned int> mem32_rev_gold("mem32_rev_gold", scml_memsize(8));
  mem32_rev_gold.set_addressing_mode(8); 
  errors += testMemory<unsigned int, 32, 32, 17, 0, 7>("mem32_rev", SCML.m_mem32_rev, mem32_rev_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 0, 7>("mem32_rev", SCML.m_mem32_rev, mem32_rev_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 0, 7>("mem32_rev", SCML.m_mem32_rev, mem32_rev_gold, pattern);

  // Test a big endian memory - not yet really ready, settting the big endian flag causes it to fail
  // Map: 0x80 - 0x8F
  scml_memory<unsigned int> mem32_be_gold("mem32_be_gold", scml_memsize(8));
  mem32_be_gold.set_addressing_mode(8); 
  errors += testMemory<unsigned int, 32, 32, 17, 0, 7>("mem32_be", SCML.m_mem32_be, mem32_be_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 0, 7>("mem32_be", SCML.m_mem32_be, mem32_be_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 0, 7>("mem32_be", SCML.m_mem32_be, mem32_be_gold, pattern);

  // Test an 8-bit memory done as a 32-bit memory
  // Map: 0x90 - 0x9F
  scml_memory<unsigned int> mem8_gold("mem8_gold", scml_memsize(8));
  mem8_gold.set_addressing_mode(8); 
  errors += testMemory<unsigned int, 8, 8, 37, 0, 7>("mem8", SCML.m_mem8, mem8_gold, pattern);

  // Test an 16-bit memory done as a 32-bit memory
  // Map: 0xA0 - 0xAF
  scml_memory<unsigned int> mem16_gold("mem16_gold", scml_memsize(8));
  mem16_gold.set_addressing_mode(8); 
  errors += testMemory<unsigned int, 16, 16, 17, 0, 7>("mem16", SCML.m_mem16, mem16_gold, pattern);
  errors += testMemory<unsigned int, 16, 8, 19, 0, 7>("mem16", SCML.m_mem16, mem16_gold, pattern);

  // Test a byte swapped 32-bit memory
  // Map: 0xB0 - 0xBF
  scml_memory<unsigned int> mem32_swapb_gold("mem32_swapb_gold", scml_memsize(8));
  mem32_swapb_gold.set_addressing_mode(8); 
  errors += testMemory<unsigned int, 32, 32, 17, 0, 7>("mem32_swapb", SCML.m_mem32_swapb, mem32_swapb_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 0, 7>("mem32_swapb", SCML.m_mem32_swapb, mem32_swapb_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 0, 7>("mem32_swapb", SCML.m_mem32_swapb, mem32_swapb_gold, pattern);

  // Run the simulation. This will read the memory into our array. We
  // need to run for as many cycles as there are memory locations.
  sc_start(sc_time(3 * (MEM_SPACE_SIZE+1), SC_NS));
  sc_stop();

  // Check the results against the gold using the memory address maps
  errors += checkSimResults<unsigned int>("mem32", mem32_gold, 0x0, 8, rtlData);
  errors += checkSimResults<unsigned int>("mem36", mem36_gold, 0x10, 8, rtlData);
  errors += checkSimResults<unsigned long long>("mem40", mem40_gold, 0x20, 8, rtlData);
  errors += checkSimResults<unsigned long long>("mem64", mem64_gold, 0x30, 8, rtlData);
  errors += checkSimResults<unsigned int>("mem_inter", mem_inter_gold, 0x40, 8, rtlData);
  errors += checkSimResults<unsigned int>("mem_both", mem_both_gold, 0x50, 16, rtlData);
  errors += checkSimResults<unsigned int>("mem_holes", mem_holes_gold, 0x60, 16, rtlData);
  errors += checkSimResults<unsigned int>("mem32_rev", mem32_rev_gold, 0x70, 8, rtlData);
  errors += checkSimResults<unsigned int>("mem32_be", mem32_be_gold, 0x80, 8, rtlData);
  errors += checkSimResults<unsigned int>("mem8", mem8_gold, 0x90, 8, rtlData);
  errors += checkSimResults<unsigned int>("mem16", mem16_gold, 0xA0, 8, rtlData);
  errors += checkSimResults<unsigned int>("mem32_swapb", mem32_swapb_gold, 0xB0, 8, rtlData);

  // Results
  if (errors > 0) {
    cout << "CARBON: Simulation ended with " << errors << " errors." << endl;
  }
  cout << "CARBON: Simulation exited at time " << sc_time_stamp().value() << endl;
  return 0;

} // int sc_main
