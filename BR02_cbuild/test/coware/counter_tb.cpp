/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "counter_tb.h"

void counter_tb::entry_tb_clk()
{
  if (sc_time_stamp().value() < 1000)
  {
    en.write(0);
    rst.write(1);
  }
  else
  {
    rst.write(0);
    en.write(1);
  }

  cout << "CARBON: Time: " << sc_time_stamp().value() << " Value: 0x" << hex << val.read() << dec << endl;
}
