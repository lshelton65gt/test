/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "libfifo_s.systemc.h"
#include "scml_tb.h"

int sc_main (int argc , char *argv[]) 
{

  sc_signal<bool>         rst;
  sc_signal<sc_uint<32> > data_in;
  sc_signal<bool>         push;
  sc_signal<sc_uint<32> > data_out;
  sc_signal<bool>         pop;
  sc_signal<bool>         empty;
  sc_signal<bool>         full;

  sc_clock push_clk("push_clk", sc_time(3, SC_NS), 1.5, sc_time(0.0, SC_NS));
  sc_clock pop_clk ("pop_clk",  sc_time(2, SC_NS), 1.0, sc_time(0.0, SC_NS));

  fifo_seric SCML("SCML");
  SCML.rst(rst);
  SCML.push_clk(push_clk);
  SCML.data_in(data_in);
  SCML.push(push);
  SCML.pop_clk(pop_clk);
  SCML.data_out(data_out);
  SCML.pop(pop);
  SCML.empty(empty);
  SCML.full(full);


  scml_tb SCML_TB("SCML_TB");
  SCML_TB.rst(rst);
  SCML_TB.push_clk(push_clk);
  SCML_TB.data_in(data_in);
  SCML_TB.push(push);
  SCML_TB.pop_clk(pop_clk);
  SCML_TB.data_out(data_out);
  SCML_TB.pop(pop);
  SCML_TB.empty(empty);
  SCML_TB.full(full);


  SCML.m_Registers32_R1.write(0xaabbccdd);
  cout << "Read value: 0x" << hex << SCML.m_Registers32_R1.read() << dec << endl;

// Connect the Initiator to the Target port
  //SCML_TB.p_PV(SCML.p_REGS);

  
  sc_start(sc_time(0, SC_NS));
  sc_start(sc_time(50000, SC_NS));
  sc_stop();
  

  cout << "CARBON: Simulation exited at time " << sc_time_stamp().value() << endl;
  if (SCML_TB.error_count)
  {
    cout << "CARBON: " << SCML_TB.error_count << " errors found!!!" <<endl;
    return 1;
  } 
  else
  {
    cout << "CARBON: No errors detected." << endl;
    return 0;
  }

}
