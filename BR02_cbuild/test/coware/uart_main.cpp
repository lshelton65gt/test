/*****************************************************************************

 Copyright (c) 2007-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "libuart.systemc.h"
#include "uart_tb.h"
#include "uart_monitor.h"

static const char *modes[] = {"looking", "idle", "backoff", "disabled"};

static void mode_change_cb(CarbonObjectID* context,
                           void* userContext,
                           CarbonOnDemandMode from,
                           CarbonOnDemandMode to,
                           CarbonTime simTime)
{
  std::cout << "On-demand entering " << modes[to] << " mode at time " << std::dec << simTime << std::endl;
}

int sc_main (int argc , char *argv[]) 
{

  bool onDemand = false;
  bool record = false;
  bool playback = false;
  bool verboseReplay = false;
  for (int i = 0; i < argc; ++i) {
    if (strcmp(argv[i], "-onDemand") == 0) {
      onDemand = true;
    } else if (strcmp(argv[i], "-record") == 0) {
      record = true;
    } else if (strcmp(argv[i], "-playback") == 0) {
      playback = true;
    } else if (strcmp(argv[i], "-verboseReplay") == 0) {
      verboseReplay = true;
    }
  }

  sc_signal<bool>         reset;
  sc_signal<bool>         ld_tx_data;
  sc_signal<sc_uint<8> >  tx_data;
  sc_signal<bool>         tx_enable;
  sc_signal<bool>         tx_out;
  sc_signal<bool>         tx_empty;
  sc_signal<bool>         uld_rx_data;
  sc_signal<bool>         rx_enable;
  sc_signal<bool>         rx_in;
  sc_signal<sc_uint<8> >  rx_data;
  sc_signal<bool>         rx_empty;

  sc_clock txclk("txclk", 4, SC_NS, 0.5, 2, SC_NS, false);
  sc_clock rxclk("rxclk", 4, SC_NS, 0.5, 1, SC_NS, false);

  uart UART("UART");
  UART.reset(reset);
  UART.txclk(txclk);
  UART.ld_tx_data(ld_tx_data);
  UART.tx_data(tx_data);
  UART.tx_enable(tx_enable);
  UART.tx_out(tx_out);
  UART.tx_empty(tx_empty);
  UART.rxclk(rxclk);
  UART.uld_rx_data(uld_rx_data);
  UART.rx_enable(rx_enable);
  UART.rx_in(rx_in);
  UART.rx_data(rx_data);
  UART.rx_empty(rx_empty);
  
  uart_tb UARTTB("UARTTB");
  UARTTB.reset(reset);
  UARTTB.ld_tx_data(ld_tx_data);
  UARTTB.tx_data(tx_data);
  UARTTB.tx_enable(tx_enable);
  UARTTB.tx_out(tx_out);
  UARTTB.tx_empty(tx_empty);
  UARTTB.uld_rx_data(uld_rx_data);
  UARTTB.rx_enable(rx_enable);
  UARTTB.rx_in(rx_in);
  UARTTB.rx_data(rx_data);
  UARTTB.rx_empty(rx_empty);

  monitor UARTMON("UARTMonitor");
  UARTMON.reset(reset);
  UARTMON.txclk(txclk);
  UARTMON.ld_tx_data(ld_tx_data);
  UARTMON.tx_data(tx_data);
  UARTMON.tx_enable(tx_enable);
  UARTMON.tx_out(tx_out);
  UARTMON.tx_empty(tx_empty);
  UARTMON.rxclk(rxclk);
  UARTMON.uld_rx_data(uld_rx_data);
  UARTMON.rx_enable(rx_enable);
  UARTMON.rx_in(rx_in);
  UARTMON.rx_data(rx_data);
  UARTMON.rx_empty(rx_empty);
  UARTMON.rx_cnt(UART.rx_cnt);
  UARTMON.rx_frame_err(UART.rx_frame_err);
  UARTMON.rx_busy(UART.rx_busy);
  UARTMON.rx_sample_cnt(UART.rx_sample_cnt);
  UARTMON.rx_ready(UART.rx_ready);

  if (verboseReplay) {
    CarbonSys* sys = carbonGetSystem(NULL);
    carbonSystemSetVerboseReplay(sys, 1);
  }
  if (record) {
    carbonReplayRecordStart(UART.carbonModelHandle);
  }
  if (playback) {
    carbonReplayPlaybackStart(UART.carbonModelHandle);
  }
  if (onDemand) {
    carbonOnDemandSetBackoffStrategy(UART.carbonModelHandle, eCarbonOnDemandBackoffConstant);
    carbonOnDemandSetBackoffCount(UART.carbonModelHandle, 100);
    carbonOnDemandEnableStats(UART.carbonModelHandle);
    carbonOnDemandAddModeChangeCB(UART.carbonModelHandle, mode_change_cb, 0);
  }

  sc_start(sc_time(0, SC_NS));
  sc_start(sc_time(10000, SC_NS));
  sc_stop();

  cout << "CARBON: Simulation exited at time " << sc_time_stamp().value() << endl;

  return 0;
}
