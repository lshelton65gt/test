/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "systemc.h"
// Need to include the libscml.h file in order to get access to the Carbon model of 
// the scml and its API
#include "libfifo_s.h"

SC_MODULE(scml_tb)
{
  sc_out<bool>         rst;
  sc_in<bool>         push_clk;
  sc_out<sc_uint<32> > data_in;
  sc_signal<sc_uint<32> > next_data_in;
  sc_out<bool>         push;
  sc_in<bool>         pop_clk;
  sc_in<sc_uint<32> >  data_out;
  sc_out<bool>         pop;
  sc_in<bool>          empty;
  sc_in<bool>          full;
  unsigned int  x_data_out;
  int push_threshhold, pop_threshhold;
  int error_count;
	
  void entry_tb_pop_clk();
  void entry_tb_push_clk();

  SC_CTOR(scml_tb)
  {
    push_threshhold = (int)(RAND_MAX * .6);
    pop_threshhold = (int)(RAND_MAX * .1);
    SC_METHOD(entry_tb_pop_clk);
    sensitive << pop_clk.neg();
    SC_METHOD(entry_tb_push_clk);
    sensitive << push_clk.neg();
  }

};
