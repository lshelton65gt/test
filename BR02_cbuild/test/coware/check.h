// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _CHECK_H_
#define _CHECK_H_

template<typename DT, typename RT>
void checkRegister(RT& reg, const char* name, DT expected, bool disableCheck=false)
{
  // Read the value
  DT actual = reg.read();

  // If the caller asked to disable the check, stop now. This is to
  // work around CoWare bugs with 64-bit registers. The benefit of
  // doing the above read is that we still can check that Carbon
  // returns the right value because that shows up in the log.
  if (disableCheck) {
    return;
  }

  // Error check
  if (sizeof(DT) == 1) {
    cout << "Read " << name << " value: 0x" << hex << (unsigned int)actual << dec << endl;
  } else {
    cout << "Read " << name << " value: 0x" << hex << actual << dec << endl;
  }
  if (actual != expected) {
    if (sizeof(DT) == 1) {
      // Chars don't print right
      cout << "Error: " << name << " should be: 0x" << hex << (unsigned int)expected << dec << endl;
    } else {
      cout << "Error: " << name << " should be: 0x" << hex << expected << dec << endl;
    }
  }
}

template<typename DT>
void checkFullRegister(scml_memory<DT>& reg, const char* name, DT expected)
{
  bool disableCheck = (sizeof(DT) == sizeof(unsigned long long));
  checkRegister<DT, scml_memory<DT> >(reg, name, expected, disableCheck);
}

template<typename DT>
void checkField(scml_bitfield& regField, const char* name, DT expected,
                unsigned int highBit, unsigned int lowBit)
{
  // Compute the expected value for the field. We assume every register is 0 based
  unsigned int fieldLength = (highBit - lowBit + 1);
  DT mask = ((DT)-1 >> (sizeof(DT)*8 - fieldLength));
  unsigned int expectedField = (unsigned int)((expected >> lowBit) & mask);

  // Check the results
  bool disableCheck = (sizeof(DT) == sizeof(unsigned long long));
  checkRegister<unsigned int, scml_bitfield>(regField, name, expectedField, disableCheck);
}

template<typename _DT, unsigned int _memWidth, unsigned int _accessSize, unsigned int _multiplier,
         size_t _addrStart, size_t _addrEnd>
unsigned int testMemory(const char* memName, scml_memory<_DT>& mem, scml_memory<_DT>& gold, _DT pattern)
{
  // Perform write then read tests
  unsigned int errors = 0;
  for (size_t index = _addrStart; index <= _addrEnd; ++index) {
    // Figure out this access. It may be for a partial range
    unsigned int pieces = (sizeof(_DT)*8)/_accessSize;
    unsigned int offset = (index & (pieces-1)) * _accessSize;
    unsigned int msb = offset+_accessSize-1;
    unsigned int lsb = offset;
    if (msb >= _memWidth) {
      msb = _memWidth-1;
    }
    unsigned int accessSize = msb-lsb+1;

    // Write the data for the given access size
    _DT writeValue = (pattern + ((index + 1) * _multiplier));
    std::cout << "Writing(1) to " << memName << "[" << std::dec << index << "][" << msb << ":"
              << lsb << "] <= 0x" << std::hex << writeValue << std::dec << std::endl;
    mem.write(writeValue, index, accessSize, offset);
    gold.write(writeValue, index, accessSize, offset);

    // Read the data back using the access size and verify it
    _DT readValue = mem.read(index, accessSize, offset);
    _DT expected = gold.read(index, accessSize, offset);
    std::cout << "Read(1) " << memName << "[" << std::dec << index << "][" << msb << ":"
              << lsb << "] => 0x" << std::hex << readValue << std::dec << std::endl;
    if (readValue != expected) {
      std::cout << "Error: expecting value: 0x" << std::hex << expected << std::dec << std::endl;
      ++errors;
    }

    // Read the entire word and check it (unless we are writing the whole thing)
    if (accessSize != (sizeof(_DT)*8)) {
      _DT wholeExpected = gold.read(index);
      _DT wholevalue = mem.read(index);
      std::cout << "Read(1) " << memName << "[" << std::dec << index << "][" << (sizeof(_DT)*8)-1
                << ":0] => 0x" << std::hex << wholevalue << std::dec << std::endl;
      
      if (wholevalue != wholeExpected) {
        std::cout << "Error: expecting value: 0x" << std::hex << wholeExpected << std::dec << std::endl;
        ++errors;
      }
    }
  }

  // Perform write tests
  for (size_t index = _addrStart; index <= _addrEnd; ++index) {
    // Figure out the access
    unsigned int pieces = (sizeof(_DT)*8)/_accessSize;
    unsigned int offset = (index & (pieces-1)) * _accessSize;
    unsigned int msb = offset+_accessSize-1;
    unsigned int lsb = offset;
    if (msb >= _memWidth) {
      msb = _memWidth-1;
    }
    unsigned int accessSize = msb-lsb+1;

    // Write the data for the given access size
    _DT writeValue = (pattern + ((index + 1) * _multiplier * 3));
    std::cout << "Writing(2) to " << memName << "[" << std::dec << index << "][" << msb << ":"
              << lsb << "] <= 0x" << std::hex << writeValue << std::dec << std::endl;
    mem.write(writeValue, index, accessSize, offset);
    gold.write(writeValue, index, accessSize, offset);
  }

  // Perform read tests
  for (size_t index = _addrStart; index <= _addrEnd; ++index) {
    // Figure out the access
    unsigned int pieces = (sizeof(_DT)*8)/_accessSize;
    unsigned int offset = (index & (pieces-1)) * _accessSize;
    unsigned int msb = offset+_accessSize-1;
    unsigned int lsb = offset;
    if (msb >= _memWidth) {
      msb = _memWidth-1;
    }
    unsigned int accessSize = msb-lsb+1;

    // Read the data back using the access size and verify it
    _DT readValue = mem.read(index, accessSize, offset);
    _DT expected = gold.read(index, accessSize, offset);
    std::cout << "Read(2) " << memName << "[" << std::dec << index << "][" << msb << ":"
              << lsb << "] => 0x" << std::hex << readValue << std::dec << std::endl;
    if (readValue != expected) {
      std::cout << "Error: expecting value: 0x" << std::hex << expected << std::dec << std::endl;
      ++errors;
    }

    // Read the entire word and check it (unless we are writing the whole thing)
    if (accessSize != _memWidth) {
      _DT wholeExpected = gold.read(index);
      _DT wholevalue = mem.read(index);
      std::cout << "Read(2) " << memName << "[" << std::dec << index << "][" << _memWidth-1
                << ":0] => 0x" << std::hex << wholevalue << std::dec << std::endl;
      
      if (wholevalue != wholeExpected) {
        std::cout << "Error: expecting value: 0x" << std::hex << wholeExpected << std::dec << std::endl;
        ++errors;
      }
    }
  }
  return errors;
}

template<typename _DT>
int checkSimResults(const char* memName, scml_memory<_DT>& gold, unsigned int baseAddress, 
                     unsigned int memSize, unsigned long long* rtlData)
{
  unsigned int errors = 0;
  for (unsigned int i = 0; i < memSize; ++i) {
    unsigned long long memValue = (unsigned long long)gold.read(i);
    unsigned long long simValue = rtlData[baseAddress+i];
    if (memValue != simValue) {
      std::cout << "Error: " << memName << "[" << i << "]: sim/debug access mismatch: sim: 0x"
                << std::hex << simValue
                << ", debug: 0x" << memValue << std::dec << std::endl;
      ++errors;
    }
  }
  return errors;
}

#endif // _CHECK_H_
