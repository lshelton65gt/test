/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "systemc.h"
// Need to include the libscml.h file in order to get access to the Carbon model of 
// the scml and its API
#include "libcounter.h"

SC_MODULE(counter_tb)
{
  sc_out<bool>        rst;
  sc_in<bool>        clk;
  sc_out<bool>        en;
  sc_in<sc_uint<16> > val;
	
  void entry_tb_clk();

  SC_CTOR(counter_tb)
  {
    SC_METHOD(entry_tb_clk);
    sensitive << clk.neg();
  }
};
