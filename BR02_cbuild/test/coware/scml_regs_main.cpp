/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "libregs.systemc.h"
#include "check.h"

int sc_main (int argc , char *argv[]) 
{
  sc_clock clock("clock", sc_time(3, SC_NS), 1.5, sc_time(0.0, SC_NS));

  sc_signal<bool>         reset_n;
  sc_signal<bool>         wr;
  sc_signal<sc_uint<8> >  addr;
  sc_signal<sc_uint<64> > wrData;
  sc_signal<sc_uint<64> > rdData;

  sc_clock push_clk("push_clk", sc_time(3, SC_NS), 1.5, sc_time(0.0, SC_NS));
  sc_clock pop_clk ("pop_clk",  sc_time(2, SC_NS), 1.0, sc_time(0.0, SC_NS));

  regs SCML("SCML");

  SCML.reset_n ( reset_n );
  SCML.clock   ( clock );
  SCML.wr      ( wr );
  SCML.addr    ( addr );
  SCML.wrData  ( wrData );
  SCML.rdData  ( rdData );

  // Test unsigned char registers
  SCML.m_Registers8_reg8i.write(0xab);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8i, "reg8i", 0xab);
  SCML.m_Registers8_reg8j.write(0xcd);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8j, "reg8j", 0xcd);
  SCML.m_Registers8_reg8m_Value.write(0xa6);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8m, "reg8m", 0xa6);
  checkField<unsigned char>(SCML.m_Registers8_reg8m_Value, "reg8m.Value", 0xa6, 7, 0);
  SCML.m_Registers8_reg8n_Value.write(0xb7);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8n, "reg8n", 0xb7);
  checkField<unsigned char>(SCML.m_Registers8_reg8n_Value, "reg8n.Value", 0xb7, 7, 0);

  // Test unsigned short registers
  SCML.m_Registers16_reg16k.write(0x1234);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16k, "reg16k", 0x1234);
  SCML.m_Registers16_reg16l.write(0x5678);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16l, "reg16l", 0x5678);
  SCML.m_Registers16_reg16o_Value.write(0xb7c8);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16o, "reg16o", 0xb7c8);
  checkField<unsigned short>(SCML.m_Registers16_reg16o_Value, "reg16o.Value", 0xb7c8, 15, 0);
  SCML.m_Registers16_reg16p_Value.write(0xd9e0);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16p, "reg16p", 0xd9e0);
  checkField<unsigned short>(SCML.m_Registers16_reg16p_Value, "reg16p.Value", 0xd9e0, 15, 0);

  // Test unsigned int registers
  SCML.m_Registers32_reg32a.write(0xaaaaaaaa);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32a, "reg32a", 0xaaaaaaaa);
  SCML.m_Registers32_reg32b.write(0xbbbbbbbb);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32b, "reg32b", 0x0); // Read-only reg
  SCML.m_Registers32_reg32e_all.write(0xa5a5a5a5);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32e, "reg32e", 0xa5a5a5a5);
  checkField<unsigned int>(SCML.m_Registers32_reg32e_all, "reg32e.all", 0xa5a5a5a5, 31, 0);
  SCML.m_Registers32_reg32f_all.write(0x12345678);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32f, "reg32f", 0x12345678);
  checkField<unsigned int>(SCML.m_Registers32_reg32f_all, "reg32f.all", 0x12345678, 31, 0);

  // Test unsigned long long registers
  SCML.m_Registers64_reg64c.write(0xddddddddccccccccULL);
  checkFullRegister<unsigned long long>(SCML.m_Registers64_reg64c, "reg64c", 0xddddddddccccccccULL);
  SCML.m_Registers64_reg64d.write(0x99999999eeeeeeeeULL);
  checkFullRegister<unsigned long long>(SCML.m_Registers64_reg64d, "reg64d", 0x99999999eeeeeeeeULL);
  SCML.m_Registers64_reg64g.write(0x12341234abcdabcdULL);
  checkFullRegister<unsigned long long>(SCML.m_Registers64_reg64g, "reg64g", 0x123412345678abcdULL);
  SCML.m_Registers64_reg64h.write(0xcdcdabab12345678ULL);
  checkFullRegister<unsigned long long>(SCML.m_Registers64_reg64h, "reg64h", 0xcdcdabab12345678ULL);

  sc_start(sc_time(0, SC_NS));
  sc_start(sc_time(500, SC_NS));
  sc_stop();

  cout << "CARBON: Simulation exited at time " << sc_time_stamp().value() << endl;

  return 0;
}
