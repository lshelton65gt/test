
puts "Sourcing Carbon config file"
source libuart.coware.cmd

puts "Loading Carbon model"
load uart_tb.cpp uart_monitor.cpp libuart.systemc.cpp uart_main.cpp

puts "Running simulation"
simargs -record -verboseReplay

run

