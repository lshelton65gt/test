`define R32A_ADDR  8'h0
`define R32B_ADDR  8'h8
`define R64C_ADDR  8'h80
`define R64D_ADDR  8'hA0
`define R8I_ADDR   8'h4
`define R8J_ADDR   8'h5
`define R16K_ADDR  8'h6
`define R16L_ADDR  8'h7
`define M32E_ADDR  8'h10
`define M32F_ADDR  8'h11
`define M64G_ADDR  8'h12
`define M64H_ADDR  8'h13
`define M8M_ADDR   8'h14
`define M8N_ADDR   8'h15
`define M16O_ADDR  8'h16
`define M16P_ADDR  8'h17


module regs_bf
  (
   reset_n,
   clock,
   wr,
   addr,
   wrData,
   rdData
   );

   input         reset_n;
   input         clock;
   input         wr;
   input [ 7:0]  addr;
   input [63:0]  wrData;
   output [63:0] rdData;

   reg [63:0]    rdData;

   reg [31:0]    reg32a;
   reg [31:0]    reg32b;

   reg [63:0]    reg64c;
   reg [63:0]    reg64d;

   reg [7:0]     reg8i;
   reg [7:0]     reg8j;

   reg [15:0]    reg16k;
   reg [15:0]    reg16l;

   reg [7:0]     mem8 [1:0];
   reg [15:0]    mem16 [1:0];
   reg [31:0]    mem32 [1:0];
   reg [63:0]    mem64 [1:0];


   always @ (negedge reset_n or posedge clock)
     begin
        if(! reset_n) begin
           reg32a <= 0;
           reg32b <= 0;

           reg64c <= 0;
           reg64d <= 0;
        end else begin
           if(wr)
             // write
             if(addr == `R32A_ADDR)
               reg32a <= wrData[31:0];
             else if(addr == `R32B_ADDR)
               reg32b <= wrData[31:0];
             else if(addr == `R64C_ADDR)
               reg64c <= wrData;
             else if(addr == `R64D_ADDR)
               reg64d <= wrData;
             else if (addr == `M32E_ADDR)
               mem32[0] <= wrData[31:0];
             else if (addr == `M32F_ADDR)
               mem32[1] <= wrData[31:0];
             else if (addr == `M64G_ADDR)
               mem64[0] <= wrData[31:0];
             else if (addr == `M64H_ADDR)
               mem64[1] <= wrData[31:0];
             else if (addr == `R8I_ADDR)
               reg8i <= wrData[7:0];
             else if (addr == `R8J_ADDR)
               reg8j <= wrData[7:0];
             else if (addr == `R16K_ADDR)
               reg16k <= wrData[15:0];
             else if (addr == `R16L_ADDR)
               reg16l <= wrData[15:0];
             else if (addr == `M8M_ADDR)
               mem8[0] <= wrData[7:0];
             else if (addr == `M8N_ADDR)
               mem8[1] <= wrData[7:0];
             else if (addr == `M16O_ADDR)
               mem16[0] <= wrData[15:0];
             else if (addr == `M16P_ADDR)
               mem16[1] <= wrData[15:0];
             else
               ; // do nothing
           else
             // read
             if(addr == `R32A_ADDR)
               rdData <= reg32a;
             else if(addr == `R32B_ADDR)
               rdData <= reg32b;
             else if(addr == `R64C_ADDR)
               rdData <= reg64c;
             else if(addr == `R64D_ADDR)
               rdData <= reg64d;
             else if (addr == `M32E_ADDR)
               rdData <= {32'b0, mem32[0]};
             else if (addr == `M32F_ADDR)
               rdData <= {32'b0, mem32[1]};
             else if (addr == `M64G_ADDR)
               rdData <= mem64[0];
             else if (addr == `M64H_ADDR)
               rdData <= mem64[1];
             else if (addr == `R8I_ADDR)
               rdData <= {56'b0, reg8i};
             else if (addr == `R8J_ADDR)
               rdData <= {56'b0, reg8j};
             else if (addr == `R16K_ADDR)
               rdData <= {48'b0, reg16k};
             else if (addr == `R16L_ADDR)
               rdData <= {48'b0, reg16l};
             else if (addr == `M8M_ADDR)
               rdData <= {56'b0, mem8[0]};
             else if (addr == `M8N_ADDR)
               rdData <= {56'b0, mem8[1]};
             else if (addr == `M16O_ADDR)
               rdData <= {48'b0, mem16[0]};
             else if (addr == `M16P_ADDR)
               rdData <= {48'b0, mem16[1]};
             else
               rdData <= 0;
        end
     end


endmodule
