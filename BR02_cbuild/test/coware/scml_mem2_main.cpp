// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "libmem2.systemc.h"
#include "check.h"
#include "scml_mem_tb.h"

#define MEM_SPACE_SIZE  256

int sc_main(int argc, char* argv[])
{
  sc_signal< sc_uint<64> >      rdata;
  sc_signal< sc_uint<8> >       raddr;
  sc_signal< bool >             wen;
  sc_signal< sc_uint<8> >       waddr;
  sc_signal< sc_uint<64> >      wdata;
  sc_clock clk("clk", sc_time(3, SC_NS), 1.5, sc_time(0.0, SC_NS));

  mem2 SCML("SCML");
  SCML.rdata(rdata);
  SCML.raddr(raddr);
  SCML.waddr(waddr);
  SCML.wen(wen);
  SCML.wdata(wdata);
  SCML.clk(clk);

  // An array to store the data read from the RTL memory
  unsigned long long rtlData[MEM_SPACE_SIZE];

  scml_mem_tb TB("TB", &rtlData[0], MEM_SPACE_SIZE);
  TB.rdata(rdata);
  TB.raddr(raddr);
  TB.wen(wen);
  TB.waddr(waddr);
  TB.wdata(wdata);
  TB.clk(clk);

  // init
  sc_start(sc_time(0, SC_NS));

  unsigned int errors = 0;

  // Test the mem32 memory with 32-bit, 16-bit, and 8-bit operations
  // Map: 0x0 - 0xF
  unsigned int pattern = 0xa0b1c2d3;
  scml_memory<unsigned int> mem32_gold("mem32_gold", scml_memsize(8));
  mem32_gold.set_addressing_mode(8); 
  errors += testMemory<unsigned int, 32, 32, 17, 0, 7>("mem32", SCML.m_mem32, mem32_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 0, 7>("mem32", SCML.m_mem32, mem32_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 0, 7>("mem32", SCML.m_mem32, mem32_gold, pattern);
  
  // Test the mem36 memory with 32-bit, 16-bit, and 8-bit operations
  // Note that the RTL is 36 bits wide but we put it in a 32-bit memory
  // Map: 0x10 - 0x1F
  scml_memory<unsigned int> mem36_gold("mem36_gold", scml_memsize(8));
  mem36_gold.set_addressing_mode(8); 
  errors += testMemory<unsigned int, 32, 32, 17, 0, 7>("mem36", SCML.m_mem36, mem36_gold, pattern);
  errors += testMemory<unsigned int, 32, 16, 19, 0, 7>("mem36", SCML.m_mem36, mem36_gold, pattern);
  errors += testMemory<unsigned int, 32, 8, 37, 0, 7>("mem36", SCML.m_mem36, mem36_gold, pattern);
  
  // Test the mem64 memory with 64, 32, 16, and 8-bit operations
  // Map: 0x20 - 0x2F
  scml_memory<unsigned long long> mem64_gold("mem64_gold", scml_memsize(8));
  mem64_gold.set_addressing_mode(8); 
  unsigned long long pattern64 = 0x12345678a0b1c2d3ULL;
  errors += testMemory<unsigned long long, 64, 64, 17, 0, 7>("mem64", SCML.m_mem64, mem64_gold, pattern64);
  errors += testMemory<unsigned long long, 64, 32, 19, 0, 7>("mem64", SCML.m_mem64, mem64_gold, pattern64);
  errors += testMemory<unsigned long long, 64, 16, 37, 0, 7>("mem64", SCML.m_mem64, mem64_gold, pattern64);
  errors += testMemory<unsigned long long, 64, 8, 39, 0, 7>("mem64", SCML.m_mem64, mem64_gold, pattern64);

  sc_start(sc_time(3 *(MEM_SPACE_SIZE+1), SC_NS));
  sc_stop();

  // Check the results against the gold using the memory address maps
  errors += checkSimResults<unsigned int>("mem32", mem32_gold, 0x0, 8, rtlData);
  errors += checkSimResults<unsigned int>("mem36", mem36_gold, 0x10, 8, rtlData);
  errors += checkSimResults<unsigned long long>("mem64", mem64_gold, 0x20, 8, rtlData);

  // Results
  if (errors > 0) {
    cout << "CARBON: Simulation ended with " << errors << " errors." << endl;
  }
  cout << "CARBON: Simulation exited at time " << sc_time_stamp().value() << endl;

  return 0;  
} // int sc_main
