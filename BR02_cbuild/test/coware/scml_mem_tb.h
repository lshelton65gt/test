// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _SCML_MEM_TB_H_
#define _SCML_MEM_TB_H_

#include "systemc.h"

SC_MODULE(scml_mem_tb)
{
  sc_in< sc_uint<64> >  rdata;
  sc_out< sc_uint<8> >  raddr;
  sc_out< bool >        wen;
  sc_out< sc_uint<8> >  waddr;
  sc_out< sc_uint<64> > wdata;
  sc_in< bool >         clk;

  unsigned long long* mRtlData;
  unsigned int mMemSize;

  SC_HAS_PROCESS(scml_mem_tb);
  scml_mem_tb(sc_module_name name, unsigned long long* rtlData, unsigned int memSize) :
    mRtlData(rtlData), mMemSize(memSize)
  {
    SC_METHOD(run);
    sensitive << clk.neg();
  }
    
  void run()
  {
    // Fill in the data from the last read operation.
    unsigned int addr = raddr.read().to_uint();
    unsigned long long data = rdata.read().to_uint64();
    mRtlData[addr] = data;

    // Move to the next raddr
    raddr.write(addr + 1);
  }
}; // SC_MODULE

#endif // _SCML_MEM_TB_H_
