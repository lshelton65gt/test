//
// Simple 16 bit Verilog Counter

module counter(clk, reset, enable, value);
// Inputs
input clk;
input reset;
input enable;

// Outputs
output [15:0] value;

// Output Register
reg [15:0] value;

// Internal Register
reg [15:0] prev_value;
   
always @(posedge clk)
begin
   if (reset)
   begin
      value <= 0;
      prev_value = 0;
   end
   else if (enable)
   begin
      prev_value = value;
      value <= prev_value + 1;
   end
end

endmodule
