library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

package base is
  type subrec is record
     a : std_logic_vector (31 downto 0);
     b : std_logic;
  end record;
end package;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.base.all;

entity dbapi9 is
  port (
    clk   : in  std_logic;
    we    : in  std_logic;
    addr1 : in  std_logic_vector (1 downto 0);
    addr2 : in  std_logic_vector (2 downto 0);
    addr3 : in  std_logic_vector (3 downto 0);
    addr4 : in  std_logic_vector (1 downto 0);
    ain   : in  std_logic_vector (31 downto 0);
    bin   : in  std_logic;
    din   : in  std_logic_vector (15 downto 0);
    xin   : in  std_logic;
    yin   : in  std_logic_vector (31 downto 0);
    zin   : in  std_logic_vector (31 downto 0);
    aout  : out std_logic_vector (31 downto 0);
    bout  : out std_logic;
    cout  : out subrec;
    dout  : out std_logic_vector (15 downto 0);
    xout  : out std_logic;
    yout  : out std_logic_vector (31 downto 0);
    zout  : out std_logic_vector (31 downto 0));
end dbapi9;

architecture arch of dbapi9 is
  type midrec is record
                   c : subrec;
                   d : std_logic_vector(15 downto 0);  -- carbon scObserveSignal
                 end record;

  type subarray is array (3 downto 0) of midrec;

  type mymem is array (15 downto 0) of std_logic_vector (31 downto 0);

  type myrec is record
                  x : std_logic;
                  y : std_logic_vector (31 downto 0);
                  z : mymem;
                  r : subarray;
                end record;

  type myarray is array (3 downto 0, 7 downto 0) of myrec;

  signal arr : myarray;         -- carbon observeSignal
  signal addr1_int : integer;
  signal addr2_int : integer;
  signal addr3_int : integer;
  signal addr4_int : integer;

  signal mem       : mymem;     -- carbon observeSignal 
begin

  addr1_int <= conv_integer(unsigned(addr1));
  addr2_int <= conv_integer(unsigned(addr2));
  addr3_int <= conv_integer(unsigned(addr3));
  addr4_int <= conv_integer(unsigned(addr4));

  process (clk)
  begin
    if clk'event and clk = '1' then
      if we = '1' then
        arr(addr1_int, addr2_int).x <= xin;
        arr(addr1_int, addr2_int).y <= yin;
        arr(addr1_int, addr2_int).z(addr3_int) <= zin;
        arr(addr1_int, addr2_int).r(addr4_int).c.a <= ain;
        arr(addr1_int, addr2_int).r(addr4_int).c.b <= bin;
        arr(addr1_int, addr2_int).r(addr4_int).d <= din;
        mem(addr3_int) <= zin;
      else
        xout <= arr(addr1_int, addr2_int).x;
        yout <= arr(addr1_int, addr2_int).y;
        zout <= arr(addr1_int, addr2_int).z(addr3_int);
        aout <= arr(addr1_int, addr2_int).r(addr4_int).c.a;
        bout <= arr(addr1_int, addr2_int).r(addr4_int).c.b;
        dout <= arr(addr1_int, addr2_int).r(addr4_int).d;
        cout <= arr(addr1_int, addr2_int).r(addr4_int).c;
      end if;
    end if;
  end process;
  
end arch;
