/*****************************************************************************

 Copyright (c) 2003 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "uart_tb.h"

#define CYCLE(n) wait(n*8, SC_NS)

void uart_tb::log_msg(const char* msg)
{
  cout << "Time: " << sc_time_stamp() << ":: " << msg << endl;
}

void uart_tb::test_driver()
{
  reset.write(1); // Reset the circuit
  rx_in.write(1); // Clear Rx
  CYCLE(1);
  reset.write(0);
  CYCLE(1);

  transmit_data(0xaa);

  rx_enable.write(1); // Enable the receiver

  log_msg("rx_in=0");
  rx_in.write(0); // Sync
  CYCLE(8);


  log_msg("rx_in=1");
  rx_in.write(1); // Data
  CYCLE(1);

  log_msg("rx_in=0");
  rx_in.write(0); // Data
  CYCLE(1);

  log_msg("rx_in=1");
  rx_in.write(1); // Data
  CYCLE(1);

  log_msg("rx_in=0");
  rx_in.write(0); // Data
  CYCLE(1);

  log_msg("rx_in=1");
  rx_in.write(1); // Data
  CYCLE(1);

  log_msg("rx_in=0");
  rx_in.write(0); // Data
  CYCLE(1);

  log_msg("rx_in=1");
  rx_in.write(1); // Data
  CYCLE(1);

  log_msg("uld_rx_data=1");
  uld_rx_data.write(1);
  CYCLE(1);
  uld_rx_data.write(0);

}


void uart_tb::transmit_data(unsigned int val)
{
  ld_tx_data.write(1);
  tx_data.write(val);
  CYCLE(1);
  ld_tx_data.write(0);
  tx_enable.write(1);
  CYCLE(8);
  tx_enable.write(0);
}
