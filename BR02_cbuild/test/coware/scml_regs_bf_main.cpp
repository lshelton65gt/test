/*****************************************************************************

 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "libregs_bf.systemc.h"
#include "check.h"

int sc_main (int argc , char *argv[]) 
{
  sc_clock clock("clock", sc_time(3, SC_NS), 1.5, sc_time(0.0, SC_NS));

  sc_signal<bool>         reset_n;
  sc_signal<bool>         wr;
  sc_signal<sc_uint<8> >  addr;
  sc_signal<sc_uint<64> > wrData;
  sc_signal<sc_uint<64> > rdData;

  sc_clock push_clk("push_clk", sc_time(3, SC_NS), 1.5, sc_time(0.0, SC_NS));
  sc_clock pop_clk ("pop_clk",  sc_time(2, SC_NS), 1.0, sc_time(0.0, SC_NS));

  regs_bf SCML("SCML");

  SCML.reset_n ( reset_n );
  SCML.clock   ( clock );
  SCML.wr      ( wr );
  SCML.addr    ( addr );
  SCML.wrData  ( wrData );
  SCML.rdData  ( rdData );

  // reg32a is created as follows:
  //    [31]     Ena
  //    [30:24] Field4
  //    [23:16] Field3
  //    [15:12] Field2
  //    [11:4]  Field1
  //    [3:0]   Field0
  // Write the whole thing and read it back as a whole or as parts
  SCML.m_Registers32_reg32a.write(0xaaaaaaaa);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32a, "reg32a", 0xaaaaaaaa);
  checkField<unsigned int>(SCML.m_Registers32_reg32a_Ena, "reg32a.Ena", 0xaaaaaaaa, 31, 31);
  checkField<unsigned int>(SCML.m_Registers32_reg32a_Field4, "reg32a.Field4", 0xaaaaaaaa, 30, 24);
  checkField<unsigned int>(SCML.m_Registers32_reg32a_Field3, "reg32a.Field3", 0xaaaaaaaa, 23, 16);
  checkField<unsigned int>(SCML.m_Registers32_reg32a_Field2, "reg32a.Field2", 0xaaaaaaaa, 15, 12);
  checkField<unsigned int>(SCML.m_Registers32_reg32a_Field1, "reg32a.Field1", 0xaaaaaaaa, 11, 4);
  checkField<unsigned int>(SCML.m_Registers32_reg32a_Field0, "reg32a.Field0", 0xaaaaaaaa, 3, 0);

  // Write some pieces and read it back as a whole
  SCML.m_Registers32_reg32a_Field0.write(0xb);
  SCML.m_Registers32_reg32a_Field3.write(0xcd);
  SCML.m_Registers32_reg32a_Ena.write(0x0);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32a, "reg32a", 0x2acdaaab);

  // reg32b is created as follows:
  //    [31]     Ena
  //    [30:26] Field4
  //    [25:17] Field3
  //    [16:11] Field2
  //    [10:5]  Field1
  //    [4:0]   Field0
  // Write the whole thing and read it back as a whole or as parts
  SCML.m_Registers32_reg32b.write(0xbbbbbbbb);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32b, "reg32b", 0xbbbbbbbb);
  checkField<unsigned int>(SCML.m_Registers32_reg32b_Ena, "reg32b.Ena", 0xbbbbbbbb, 31, 31);
  checkField<unsigned int>(SCML.m_Registers32_reg32b_Field4, "reg32b.Field4", 0xbbbbbbbb, 30, 26);
  checkField<unsigned int>(SCML.m_Registers32_reg32b_Field3, "reg32b.Field3", 0xbbbbbbbb, 25, 17);
  checkField<unsigned int>(SCML.m_Registers32_reg32b_Field2, "reg32b.Field2", 0xbbbbbbbb, 16, 11);
  checkField<unsigned int>(SCML.m_Registers32_reg32b_Field1, "reg32b.Field1", 0xbbbbbbbb, 10, 5);
  checkField<unsigned int>(SCML.m_Registers32_reg32b_Field0, "reg32b.Field0", 0xbbbbbbbb, 4, 0);

  // Write some pieces and read it back as a whole
  SCML.m_Registers32_reg32b_Field1.write(0xb);
  SCML.m_Registers32_reg32b_Field4.write(0xcd);
  SCML.m_Registers32_reg32b_Ena.write(0x0);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32b, "reg32b", 0x37bbb97b);

  // reg64c is created as follows:
  //    [63]    Ena
  //    [62:32] Field4
  //    [31:28] Field3
  //    [27:24] Field2
  //    [23:8]  Field1
  //    [7:0]   Field0
  // Write the whole thing and read it back as a whole or as parts
  SCML.m_Registers64_reg64c.write(0xaaaaaaaaaaaaaaaaULL);
  checkFullRegister<unsigned long long>(SCML.m_Registers64_reg64c, "reg64c", 0xaaaaaaaaaaaaaaaaULL);
  checkField<unsigned long long>(SCML.m_Registers64_reg64c_Ena, "reg64c.Ena", 0xaaaaaaaaaaaaaaaaULL, 63, 63);
  checkField<unsigned long long>(SCML.m_Registers64_reg64c_Field4, "reg64c.Field4", 0xaaaaaaaaaaaaaaaaULL, 62, 32);
  checkField<unsigned long long>(SCML.m_Registers64_reg64c_Field3, "reg64c.Field3", 0xaaaaaaaaaaaaaaaaULL, 31, 28);
  checkField<unsigned long long>(SCML.m_Registers64_reg64c_Field2, "reg64c.Field2", 0xaaaaaaaaaaaaaaaaULL, 27, 24);
  checkField<unsigned long long>(SCML.m_Registers64_reg64c_Field1, "reg64c.Field1", 0xaaaaaaaaaaaaaaaaULL, 23, 8);
  checkField<unsigned long long>(SCML.m_Registers64_reg64c_Field0, "reg64c.Field0", 0xaaaaaaaaaaaaaaaaULL, 7, 0);
  // Write a part and read it back
  SCML.m_Registers64_reg64c_Field4.write(0x3bbbbbbb);
  checkFullRegister<unsigned long long>(SCML.m_Registers64_reg64c, "reg64c", 0xbbbbbbbbaaaaaaaaULL);


  // reg64d is created as follows:
  //    [63:32]    Upper
  //    [31:0]     Lower
  // Write parts and read it back
  SCML.m_Registers64_reg64d_Upper.write(0xcccccccc);
  SCML.m_Registers64_reg64d_Lower.write(0xaaaaaaaa);
  checkFullRegister<unsigned long long>(SCML.m_Registers64_reg64d, "reg64d", 0xccccccccaaaaaaaaULL);
  checkField<unsigned long long>(SCML.m_Registers64_reg64d_Lower, "reg64d.Lower", 0xccccccccaaaaaaaaULL, 31, 0);
  checkField<unsigned long long>(SCML.m_Registers64_reg64d_Upper, "reg64d.Upper", 0xccccccccaaaaaaaaULL, 63, 32);
  // Write the whole thing and read it back
  SCML.m_Registers64_reg64d.write(0xeeeeeeeeddddddddULL);
  checkField<unsigned long long>(SCML.m_Registers64_reg64d_Lower, "reg64d.Lower", 0xeeeeeeeeddddddddULL, 31, 0);
  checkField<unsigned long long>(SCML.m_Registers64_reg64d_Upper, "reg64d.Upper", 0xeeeeeeeeddddddddULL, 63, 32);

  // reg32e is defined as:
  // [31:16]    High
  // [15:0]     Low
  // Write the hole thing and read it back
  SCML.m_Registers32_reg32e.write(0xa5a5b6b6);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32e, "reg32e", 0xa5a5b6b6);
  checkField<unsigned int>(SCML.m_Registers32_reg32e_Low, "reg32e.Low", 0xa5a5b6b6, 15, 0);
  checkField(SCML.m_Registers32_reg32e_High, "reg32e.High", 0xa5a5b6b6, 31, 16);

  // Write a part and read it back
  SCML.m_Registers32_reg32e_High.write(0xc7c7);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32e, "reg32e", 0xc7c7b6b6);

  // reg32f is defined as:
  // [31:24]    Upper
  // [23:0]     Lower
  // Write the whole thing and read it back
  SCML.m_Registers32_reg32f.write(0x12345678);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32f, "reg32f", 0x12345678);
  checkField<unsigned int>(SCML.m_Registers32_reg32f_Lower, "reg32f.Lower", 0x12345678, 23, 0);
  checkField<unsigned int>(SCML.m_Registers32_reg32f_Upper, "reg32f.Upper", 0x12345678, 31, 24);

  // Write a part and read it back
  SCML.m_Registers32_reg32f_Upper.write(0xd8);
  checkFullRegister<unsigned int>(SCML.m_Registers32_reg32f, "reg32f", 0xd8345678);

  // reg64g is defined as:
  // [63:32]    Upper
  // [31:16]    Middle
  // [15:0]     Lower
  // Write the parts and read it back
  SCML.m_Registers64_reg64g_Lower.write(0xabcd);
  SCML.m_Registers64_reg64g_Middle.write(0x5678);
  SCML.m_Registers64_reg64g_Upper.write(0x12341234);
  checkFullRegister<unsigned long long>(SCML.m_Registers64_reg64g, "reg64g", 0x123412345678abcdULL);
  checkField<unsigned long long>(SCML.m_Registers64_reg64g_Lower, "reg64g.Lower", 0x123412345678abcdULL, 15, 0);
  checkField<unsigned long long>(SCML.m_Registers64_reg64g_Middle, "reg64g.Middle", 0x123412345678abcdULL, 31, 16);
  checkField<unsigned long long>(SCML.m_Registers64_reg64g_Upper, "reg64g.Upper", 0x123412345678abcdULL, 63, 32);

  // Write the whole thing and read it back
  SCML.m_Registers64_reg64g.write(0x123456789abcdef0ULL);
  checkField<unsigned long long>(SCML.m_Registers64_reg64g_Lower, "reg64g.Lower", 0x123456789abcdef0ULL, 15, 0);
  checkField<unsigned long long>(SCML.m_Registers64_reg64g_Middle, "reg64g.Middle", 0x123456789abcdef0ULL, 31, 16);
  checkField<unsigned long long>(SCML.m_Registers64_reg64g_Upper, "reg64g.Upper", 0x123456789abcdef0ULL, 63, 32);

  // reg64h is defined as:
  // [63:48]    Upper
  // [47:32]    Middle
  // [31:0]     Lower
  // Write the parts and read it back
  SCML.m_Registers64_reg64h_Lower.write(0x12345678);
  SCML.m_Registers64_reg64h_Middle.write(0xabcd);
  SCML.m_Registers64_reg64h_Upper.write(0xefef);
  checkFullRegister<unsigned long long>(SCML.m_Registers64_reg64h, "reg64h", 0xefefabcd12345678ULL);
  checkField<unsigned long long>(SCML.m_Registers64_reg64h_Lower, "reg64h.Lower", 0xefefabcd12345678ULL, 31, 0);
  checkField<unsigned long long>(SCML.m_Registers64_reg64h_Middle, "reg64h.Middle", 0xefefabcd12345678ULL, 47, 32);
  checkField<unsigned long long>(SCML.m_Registers64_reg64h_Upper, "reg64h.Upper", 0xefefabcd12345678ULL, 63, 48);

  // Write the whole thing and read it back
  SCML.m_Registers64_reg64h.write(0xababcdcdefef0101ULL);
  checkField<unsigned long long>(SCML.m_Registers64_reg64h_Lower, "reg64h.Lower", 0xababcdcdefef0101ULL, 31, 0);
  checkField<unsigned long long>(SCML.m_Registers64_reg64h_Middle, "reg64h.Middle", 0xababcdcdefef0101ULL, 47, 32);
  checkField<unsigned long long>(SCML.m_Registers64_reg64h_Upper, "reg64h.Upper", 0xababcdcdefef0101ULL, 63, 48);

  // reg8i is define as:
  // [7:4]      Upper
  // [3:0]      Lower
  // Write the whole thing and read it back
  SCML.m_Registers8_reg8i.write(0xab);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8i, "reg8i", 0xab);
  checkField<unsigned char>(SCML.m_Registers8_reg8i_Lower, "reg8i.Lower", 0xab, 3, 0);
  checkField<unsigned char>(SCML.m_Registers8_reg8i_Upper, "reg8i.Upper", 0xab, 7, 4);

  // Change a field and read it back
  SCML.m_Registers8_reg8i_Upper.write(0x1);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8i, "reg8i", 0x1b);

  // reg8j is define as:
  // [7:2]      Upper
  // [1:0]      Lower
  // Write the whole thing and read it back
  SCML.m_Registers8_reg8j.write(0xcd);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8j, "reg8j", 0xcd);
  checkField<unsigned char>(SCML.m_Registers8_reg8j_Lower, "reg8j.Lower", 0xcd, 1, 0);
  checkField<unsigned char>(SCML.m_Registers8_reg8j_Upper, "reg8j.Upper", 0xcd, 7, 2);

  // Write a part and read it back
  SCML.m_Registers8_reg8j_Lower.write(0x3);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8j, "reg8j", 0xcf);

  // reg16k is defined as:
  // [15:10]    Upper
  // [9:0]      Lower
  // Write the whole thing and read it back in parts and as a whole
  SCML.m_Registers16_reg16k.write(0x1234);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16k, "reg16k", 0x1234);
  checkField<unsigned short>(SCML.m_Registers16_reg16k_Lower, "reg16k.Lower", 0x1234, 9, 0);
  checkField<unsigned short>(SCML.m_Registers16_reg16k_Upper, "reg16k.Upper", 0x1234, 15, 10);

  // Write a part and read it back
  SCML.m_Registers16_reg16k_Upper.write(0x3a);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16k, "reg16k", 0xea34);

  // reg16l is defined as:
  // [15:6]    Upper
  // [5:0]      Lower
  // Write the whole thing and read it back in parts and as a whole
  SCML.m_Registers16_reg16l.write(0x5678);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16l, "reg16l", 0x5678);
  checkField<unsigned short>(SCML.m_Registers16_reg16l_Lower, "reg16l.Lower", 0x5678, 5, 0);
  checkField<unsigned short>(SCML.m_Registers16_reg16l_Upper, "reg16l.Upper", 0x5678, 15, 6);

  // Write a part and read it back
  SCML.m_Registers16_reg16l_Upper.write(0x3cc);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16l, "reg16l", 0xf338);

  // reg8m is define as:
  // [7:6]      Upper
  // [5:0]      Lower
  // Write the whole thing and read it back in parts and as a whole
  SCML.m_Registers8_reg8m.write(0xef);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8m, "reg8m", 0xef);
  checkField<unsigned char>(SCML.m_Registers8_reg8m_Lower, "reg8m.Lower", 0xef, 5, 0);
  checkField<unsigned char>(SCML.m_Registers8_reg8m_Upper, "reg8m.Upper", 0xef, 7, 6);

  // Write a part and read it back
  SCML.m_Registers8_reg8m_Lower.write(0x13);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8m, "reg8m", 0xd3);

  // reg8n is define as:
  // [7:7]      Upper
  // [6:0]      Lower
  // Write the whole thing and read it back in parts and as a whole
  SCML.m_Registers8_reg8n.write(0x92);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8n, "reg8n", 0x92);
  checkField<unsigned char>(SCML.m_Registers8_reg8n_Lower, "reg8n.Lower", 0x92, 6, 0);
  checkField<unsigned char>(SCML.m_Registers8_reg8n_Upper, "reg8n.Upper", 0x92, 7, 7);

  // Write a part and read it back
  SCML.m_Registers8_reg8n_Upper.write(0x0);
  checkFullRegister<unsigned char>(SCML.m_Registers8_reg8n, "reg8n", 0x12);

  // reg16o is defined as:
  // [15:14]    Upper
  // [13:0]     Lower
  // Write the whole thing and read it back in parts and as a whole
  SCML.m_Registers16_reg16o.write(0xab12);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16o, "reg16o", 0xab12);
  checkField<unsigned short>(SCML.m_Registers16_reg16o_Lower, "reg16o.Lower", 0xab12, 13, 0);
  checkField<unsigned short>(SCML.m_Registers16_reg16o_Upper, "reg16o.Upper", 0xab12, 15, 14);

  // Write a part and read it back
  SCML.m_Registers16_reg16o_Upper.write(0x0);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16o, "reg16o", 0x2b12);

  // reg16p is defined as:
  // [15:4]     Upper
  // [3:0]      Lower
  // Write the whole thing and read it back in parts and as a whole
  SCML.m_Registers16_reg16p.write(0xcd34);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16p, "reg16p", 0xcd34);
  checkField<unsigned short>(SCML.m_Registers16_reg16p_Lower, "reg16p.Lower", 0xcd34, 3, 0);
  checkField<unsigned short>(SCML.m_Registers16_reg16p_Upper, "reg16p.Upper", 0xcd34, 15, 4);

  // Write a part and read it back
  SCML.m_Registers16_reg16p_Upper.write(0x123);
  checkFullRegister<unsigned short>(SCML.m_Registers16_reg16p, "reg16p", 0x1234);

  sc_start(sc_time(0, SC_NS));
  sc_start(sc_time(500, SC_NS));
  sc_stop();

  cout << "CARBON: Simulation exited at time " << sc_time_stamp().value() << endl;

  return 0;
}
