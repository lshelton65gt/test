// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

int sc_main(int argc, char* argv[])
{
  sc_clock clock("clock", 3.0, SC_NS, 1.5, 0.0, SC_NS, true);
  sc_signal<bool> reset_n;

  sc_signal<bool > AWVALIDS;
  sc_signal<sc_uint<32> > AWADDRS;
  sc_signal<sc_uint<4> > AWLENS;
  sc_signal<sc_uint<3> > AWSIZES;
  sc_signal<sc_uint<2> > AWBURSTS;
  sc_signal<sc_uint<2> > AWLOCKS;
  sc_signal<sc_uint<4> > AWCACHES;
  sc_signal<sc_uint<3> > AWPROTS;
  sc_signal<sc_uint<4> > AWIDS;
  sc_signal<bool > AWREADYS;
  sc_signal<bool > WVALIDS;
  sc_signal<bool > WLASTS;
  sc_signal<sc_biguint<64> > WDATAS;
  sc_signal<sc_uint<8> > WSTRBS;
  sc_signal<sc_uint<4> > WIDS;
  sc_signal<bool > WREADYS;
  sc_signal<bool > BVALIDS;
  sc_signal<sc_uint<2> > BRESPS;
  sc_signal<sc_uint<4> > BIDS;
  sc_signal<bool > BREADYS;
  sc_signal<bool > ARVALIDS;
  sc_signal<sc_uint<32> > ARADDRS;
  sc_signal<sc_uint<4> > ARLENS;
  sc_signal<sc_uint<3> > ARSIZES;
  sc_signal<sc_uint<2> > ARBURSTS;
  sc_signal<sc_uint<2> > ARLOCKS;
  sc_signal<sc_uint<4> > ARCACHES;
  sc_signal<sc_uint<3> > ARPROTS;
  sc_signal<sc_uint<4> > ARIDS;
  sc_signal<bool > ARREADYS;
  sc_signal<bool > RVALIDS;
  sc_signal<bool > RLASTS;
  sc_signal<sc_biguint<64> > RDATAS;
  sc_signal<sc_uint<2> > RRESPS;
  sc_signal<sc_uint<4> > RIDS;
  sc_signal<bool > RREADYS;
  sc_signal<bool > AWVALIDM;
  sc_signal<sc_uint<32> > AWADDRM;
  sc_signal<sc_uint<4> > AWLENM;
  sc_signal<sc_uint<3> > AWSIZEM;
  sc_signal<sc_uint<2> > AWBURSTM;
  sc_signal<sc_uint<2> > AWLOCKM;
  sc_signal<sc_uint<4> > AWCACHEM;
  sc_signal<sc_uint<3> > AWPROTM;
  sc_signal<sc_uint<4> > AWIDM;
  sc_signal<bool > AWREADYM;
  sc_signal<bool > WVALIDM;
  sc_signal<bool > WLASTM;
  sc_signal<sc_biguint<64> > WDATAM;
  sc_signal<sc_uint<8> > WSTRBM;
  sc_signal<sc_uint<4> > WIDM;
  sc_signal<bool > WREADYM;
  sc_signal<bool > BVALIDM;
  sc_signal<sc_uint<2> > BRESPM;
  sc_signal<sc_uint<4> > BIDM;
  sc_signal<bool > BREADYM;
  sc_signal<bool > ARVALIDM;
  sc_signal<sc_uint<32> > ARADDRM;
  sc_signal<sc_uint<4> > ARLENM;
  sc_signal<sc_uint<3> > ARSIZEM;
  sc_signal<sc_uint<2> > ARBURSTM;
  sc_signal<sc_uint<2> > ARLOCKM;
  sc_signal<sc_uint<4> > ARCACHEM;
  sc_signal<sc_uint<3> > ARPROTM;
  sc_signal<sc_uint<4> > ARIDM;
  sc_signal<bool > ARREADYM;
  sc_signal<bool > RVALIDM;
  sc_signal<bool > RLASTM;
  sc_signal<sc_biguint<64> > RDATAM;
  sc_signal<sc_uint<2> > RRESPM;
  sc_signal<sc_uint<4> > RIDM;
  sc_signal<bool > RREADYM;

  AXITest SCML("SCML");

  SCML.ARESETn ( reset_n );
  SCML.ACLK   ( clock );
  SCML.AWVALIDS ( AWVALIDS );
  SCML.AWADDRS ( AWADDRS );
  SCML.AWLENS ( AWLENS );
  SCML.AWSIZES ( AWSIZES );
  SCML.AWBURSTS ( AWBURSTS );
  SCML.AWLOCKS ( AWLOCKS );
  SCML.AWCACHES ( AWCACHES );
  SCML.AWPROTS ( AWPROTS );
  SCML.AWIDS ( AWIDS );
  SCML.AWREADYS ( AWREADYS );
  SCML.WVALIDS ( WVALIDS );
  SCML.WLASTS ( WLASTS );
  SCML.WDATAS ( WDATAS );
  SCML.WSTRBS ( WSTRBS );
  SCML.WIDS ( WIDS );
  SCML.WREADYS ( WREADYS );
  SCML.BVALIDS ( BVALIDS );
  SCML.BRESPS ( BRESPS );
  SCML.BIDS ( BIDS );
  SCML.BREADYS ( BREADYS );
  SCML.ARVALIDS ( ARVALIDS );
  SCML.ARADDRS ( ARADDRS );
  SCML.ARLENS ( ARLENS );
  SCML.ARSIZES ( ARSIZES );
  SCML.ARBURSTS ( ARBURSTS );
  SCML.ARLOCKS ( ARLOCKS );
  SCML.ARCACHES ( ARCACHES );
  SCML.ARPROTS ( ARPROTS );
  SCML.ARIDS ( ARIDS );
  SCML.ARREADYS ( ARREADYS );
  SCML.RVALIDS ( RVALIDS );
  SCML.RLASTS ( RLASTS );
  SCML.RDATAS ( RDATAS );
  SCML.RRESPS ( RRESPS );
  SCML.RIDS ( RIDS );
  SCML.RREADYS ( RREADYS );
  SCML.AWVALIDM ( AWVALIDM );
  SCML.AWADDRM ( AWADDRM );
  SCML.AWLENM ( AWLENM );
  SCML.AWSIZEM ( AWSIZEM );
  SCML.AWBURSTM ( AWBURSTM );
  SCML.AWLOCKM ( AWLOCKM );
  SCML.AWCACHEM ( AWCACHEM );
  SCML.AWPROTM ( AWPROTM );
  SCML.AWIDM ( AWIDM );
  SCML.AWREADYM ( AWREADYM );
  SCML.WVALIDM ( WVALIDM );
  SCML.WLASTM ( WLASTM );
  SCML.WDATAM ( WDATAM );
  SCML.WSTRBM ( WSTRBM );
  SCML.WIDM ( WIDM );
  SCML.WREADYM ( WREADYM );
  SCML.BVALIDM ( BVALIDM );
  SCML.BRESPM ( BRESPM );
  SCML.BIDM ( BIDM );
  SCML.BREADYM ( BREADYM );
  SCML.ARVALIDM ( ARVALIDM );
  SCML.ARADDRM ( ARADDRM );
  SCML.ARLENM ( ARLENM );
  SCML.ARSIZEM ( ARSIZEM );
  SCML.ARBURSTM ( ARBURSTM );
  SCML.ARLOCKM ( ARLOCKM );
  SCML.ARCACHEM ( ARCACHEM );
  SCML.ARPROTM ( ARPROTM );
  SCML.ARIDM ( ARIDM );
  SCML.ARREADYM ( ARREADYM );
  SCML.RVALIDM ( RVALIDM );
  SCML.RLASTM ( RLASTM );
  SCML.RDATAM ( RDATAM );
  SCML.RRESPM ( RRESPM );
  SCML.RIDM ( RIDM );
  SCML.RREADYM ( RREADYM );

  sc_start(sc_time(0, SC_NS));
  sc_start(sc_time(500, SC_NS));
  sc_stop();
}
