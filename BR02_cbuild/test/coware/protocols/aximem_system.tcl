::pct::new_project

::pct::open_library axi_slave.xml 
::pct::open_library GFRBM 
::pct::open_library GenericIPlib 
::pct::open_library AXI_BL 


::pct::instantiate_block GenericIPlib:ClockGenerator . CLK
::pct::set_bounds CLK 164 66 100 100 

::pct::instantiate_block GenericIPlib:ResetGenerator . RST
::pct::set_bounds RST 508 60 100 100 

::pct::instantiate_bl_framework . i_BLFramework 
::pct::set_bounds i_BLFramework 705 341 26 100 

::pct::instantiate_block GFRBM:GFRBM . i_GFRBM 
::pct::set_param_value i_GFRBM "Extra properties" InputFile axi.stl
::pct::set_param_value i_GFRBM "Extra properties" MaxOutstandingResp 0
::pct::set_param_value i_GFRBM "Extra properties" MaxOutstandingRdResp 3
::pct::set_param_value i_GFRBM "Extra properties" MaxOutstandingWrResp 3
::pct::set_stub_block i_GFRBM/IRQ GenericIPlib:PinDrive  
::pct::set_bounds i_GFRBM 338 351 100 100 

::pct::instantiate_block axi_slave:axi_slave . i_axi_slave 
::pct::set_bounds i_axi_slave 924 343 100 107 

::pct::global_connect CLK/CLK i_BLFramework/Clk  
::pct::global_connect CLK/CLK i_GFRBM/CLK  
::pct::global_connect CLK/CLK i_axi_slave/ACLK  
::pct::global_connect RST/RST i_BLFramework/Rst  
::pct::global_connect RST/RST i_axi_slave/ARESETn  

::pct::create_connection C i_GFRBM/POST i_BLFramework/i_GFRBM_POST  
::pct::create_connection C_1 i_BLFramework/i_axi_slave_AXI i_axi_slave/axi_s32

::pct::set_address i_GFRBM/POST:i_axi_slave/axi_s32 0x0 

::BLWizard::generateFramework /HARDWARE/i_BLFramework AXI 32

::pct::create_simulation_build_project .
::pct::export_simulation 
::pct::new_project

::pct::open_library axi_slave.xml 
::pct::open_library GFRBM 
::pct::open_library GenericIPlib 
::pct::open_library AXI_BL 


::pct::instantiate_block GenericIPlib:ClockGenerator . CLK
::pct::set_bounds CLK 164 66 100 100 

::pct::instantiate_block GenericIPlib:ResetGenerator . RST
::pct::set_bounds RST 508 60 100 100 

::pct::instantiate_bl_framework . i_BLFramework 
::pct::set_bounds i_BLFramework 705 341 26 100 

::pct::instantiate_block GFRBM:GFRBM . i_GFRBM 
::pct::set_param_value i_GFRBM "Extra properties" InputFile axi.stl
::pct::set_param_value i_GFRBM "Extra properties" MaxOutstandingResp 0
::pct::set_param_value i_GFRBM "Extra properties" MaxOutstandingRdResp 3
::pct::set_param_value i_GFRBM "Extra properties" MaxOutstandingWrResp 3
::pct::set_stub_block i_GFRBM/IRQ GenericIPlib:PinDrive  
::pct::set_bounds i_GFRBM 338 351 100 100 

::pct::instantiate_block axi_slave:axi_slave . i_axi_slave 
::pct::set_bounds i_axi_slave 924 343 100 107 

::pct::global_connect CLK/CLK i_BLFramework/Clk  
::pct::global_connect CLK/CLK i_GFRBM/CLK  
::pct::global_connect CLK/CLK i_axi_slave/ACLK  
::pct::global_connect RST/RST i_BLFramework/Rst  
::pct::global_connect RST/RST i_axi_slave/ARESETn  

::pct::create_connection C i_GFRBM/POST i_BLFramework/i_GFRBM_POST  
::pct::create_connection C_1 i_BLFramework/i_axi_slave_AXI i_axi_slave/axi_s32

::pct::set_address i_GFRBM/POST:i_axi_slave/axi_s32 0x0 

::BLWizard::generateFramework /HARDWARE/i_BLFramework AXI 32

::pct::create_simulation_build_project .
::pct::export_simulation 
