//  memory tasks:
//    0 - clear mem 
//    1 - write_n mem, m_addr, m_data
//    2 - read_n mem, m_addr, output to data
//    3 - read_n burst, m_addr, m_data is num words
`define clear_memory  3'h0
`define write_memory  3'h1
`define read_memory   3'h2
`define read_burst    3'h3
`define write_burst   3'h4

//  states:
//    0000 - 0 ready
//    0001 - 1 start_clear
//    0010 - 2 finish_clear
//    0011 - 3 start_write
//    0100 - 4 finish_write
//    0101 - 5 start_read
//    0110 - 6 read_wait
//    0111 - 7 finish_read
//    1000 - 8 start_read_burst
//    1001 - 9 read_burst_wait
//    1010 - A finish_read_burst
//    1011 - B start_write_burst
//    1100 - C write_burst_wait
//    1101 - D finish_write_burst
`define ready        4'b0000
`define start_clear  4'b0001
`define finish_clear 4'b0010
`define start_write  4'b0011
`define finish_write 4'b0100
`define start_read   4'b0101
`define read_wait    4'b0110
`define finish_read  4'b0111
`define start_read_burst  4'b1000
`define read_burst_wait   4'b1001
`define finish_read_burst 4'b1010
`define start_write_burst  4'b1011
`define write_burst_wait   4'b1100
`define finish_write_burst 4'b1101

// Information for AXI-based signals
`define AXI_LEN_WIDTH            4


module memctl (
               // Interface to controller
               m_task, 
               m_addr, 
               m_data, 
               m_req, 
               read_data, 
               done,
               clk,
               rready,
               wvalid,
               awlen,
	       read_busy,
	       write_busy,

               // Interface to memory
               address, 
               data, 
               read_n,
               write_n,
               enable_n
               );

   // Interface to controller
   input  [2:0] m_task;    
   input [31:0] m_addr;   
   input [31:0] m_data;   
   input        m_req;
   output       read_data;
   output       done;
   input        clk;
   input        rready;  // protocol bit for AXI read data channel
   input        wvalid;  // protocol bit for AXI write data channel
   input [`AXI_LEN_WIDTH - 1:0] awlen;  // protocol bit for AXI write data channel
   output                       read_busy;
   output                       write_busy;

   // Interface to memory
   output [31:0]                address;
   output [31:0]                data;
   output                       read_n;
   output                       write_n;
   output                       enable_n;

   // Interface to controller
   reg                          done;
   reg                          read_data;
   reg                          read_busy;
   reg                          write_busy;

   // Interface to memory 
   reg [31:0]                   address;
   wire [31:0]                  data;
   reg                          read_n;
   reg                          write_n; 
   reg                          enable_n;

   // Registers used by internal state machine
   reg [3:0]                    state;
   reg [4:0]                    addr_counter;
   reg [31:0]                   write_data;

   // Initialize output ports
   initial
     begin
        read_n = 1;
        write_n = 1;
        enable_n = 1;
        read_busy = 0;
        write_busy = 0;
     end

   always @(posedge clk)
     read_data <= !read_n;

   assign data = (enable_n) ?  write_data : 32'bz ; 

   always @(posedge clk)
     case (state)
       `ready:
         begin
            done    <= 1'b0;
            if (m_req)
              begin
                 write_n <= 1'b1;
                 read_n  <= 1'b1;
                 case (m_task)
                   `clear_memory:
                     begin
                        enable_n   <= 1'b1;
                        address    <= 32'h00;
                        write_data <= 32'h00;
                        state      <= `start_clear;
                     end
		   
                   `write_memory:
                     begin
                        enable_n   <= 1'b1;
                        address    <= m_addr;
                        write_data <= m_data;
                        state      <= `start_write;
                     end
		   
                   `read_memory: 
                     begin
                        enable_n <= 1'b0;
                        address  <= m_addr;
                        state    <= `start_read;
                     end

                   `read_burst:
                     begin
                        enable_n     <= 1'b0;
                        address      <= m_addr;
                        addr_counter <= 5'b0;
                        state        <= `start_read_burst;
		        read_busy = 1;
                     end

                   `write_burst:
                     begin
                        enable_n     <= 1'b1;
                        address      <= m_addr;
                        addr_counter <= 5'b0;
                        state        <= `start_write_burst;
		        write_busy = 1;
                     end
                 endcase
              end
            else
	      begin
                 state <= `ready;
	         read_busy = 0;
	         write_busy = 0;
	      end
            end

       `start_clear:
         begin
            write_n <= 1'b0;
            state   <= `finish_clear;
         end

       `finish_clear:
         begin
            write_n <= 1'b1;
            if (address != 32'hFF)
              begin
                 address <= address + 32'h01;
                 state   <= `start_clear;
              end
            else  
              begin
                 write_data <= 32'hZZ;
                 done       <= 1'b1;
                 state      <= `ready;
              end
         end

       `start_write:
         begin
            write_n <= 1'b0;
            state   <= `finish_write;
         end

       `finish_write:
         begin
            write_n    <= 1'b1;
            write_data <= 32'hZZ;
            done       <= 1'b1;
            state   <= `ready;
         end

       `start_read:
         begin
            read_n <= 1'b0;
            state  <= `read_wait;
         end

       `read_wait:
         begin
            read_n <= 1'b0;
            state  <= `finish_read;
         end

       `finish_read:
         begin
            read_n   <= 1'b1;
            enable_n <= 1'b1;
            done     <= 1'b1;
            state    <= `ready;
         end

       `start_read_burst:
         begin
            read_n <= 1'b0;
            state  <= `read_burst_wait;
         end

       `read_burst_wait:
         begin
            read_n <= 1'b0;
            if (rready)
              state  <= `finish_read_burst;
            else
              state  <= `read_burst_wait;
         end

       `finish_read_burst:
         begin
            if ((m_data[4:0] != 0) && (addr_counter < (m_data[4:0] - 1)))
              begin
                 address      <= address + 32'h01;
                 addr_counter <= addr_counter + 5'h01;
                 read_n       <= 1'b1;
                 state        <= `start_read_burst;
              end
            else  
              begin
                 read_n   <= 1'b1;
                 enable_n <= 1'b1;
                 done     <= 1'b1;
                 state    <= `ready;
	         if (!m_req)
		   read_busy = 0;
              end
         end

       `start_write_burst:
         begin
            if (wvalid)
              state  <= `write_burst_wait;
            else
	      begin
                 state  <= `ready;
	         if (!m_req)
		   write_busy = 0;
	      end
         end

       `write_burst_wait:
         begin
            write_n <= 1'b0;
            write_data <= m_data;
            if (wvalid)
              state  <= `finish_write_burst;
            else
              state  <= `write_burst_wait;
         end

       `finish_write_burst:
         begin
            if ((awlen != 0) && (addr_counter < awlen))
              begin
                 address      <= address + 32'h01;
                 addr_counter <= addr_counter + 5'h01;
                 write_n      <= 1'b1;
                 state        <= `start_write_burst;
              end
            else  
              begin
                 write_n   <= 1'b1;
                 enable_n <= 1'b1;
                 done     <= 1'b1;
                 state    <= `ready;
	         if (!m_req)
		   write_busy = 0;
              end
         end
       default:
         state <= `ready;
     endcase

endmodule
//  memory tasks:
//    0 - clear mem 
//    1 - write_n mem, m_addr, m_data
//    2 - read_n mem, m_addr, output to data
//    3 - read_n burst, m_addr, m_data is num words
`define clear_memory  3'h0
`define write_memory  3'h1
`define read_memory   3'h2
`define read_burst    3'h3
`define write_burst   3'h4

//  states:
//    0000 - 0 ready
//    0001 - 1 start_clear
//    0010 - 2 finish_clear
//    0011 - 3 start_write
//    0100 - 4 finish_write
//    0101 - 5 start_read
//    0110 - 6 read_wait
//    0111 - 7 finish_read
//    1000 - 8 start_read_burst
//    1001 - 9 read_burst_wait
//    1010 - A finish_read_burst
//    1011 - B start_write_burst
//    1100 - C write_burst_wait
//    1101 - D finish_write_burst
`define ready        4'b0000
`define start_clear  4'b0001
`define finish_clear 4'b0010
`define start_write  4'b0011
`define finish_write 4'b0100
`define start_read   4'b0101
`define read_wait    4'b0110
`define finish_read  4'b0111
`define start_read_burst  4'b1000
`define read_burst_wait   4'b1001
`define finish_read_burst 4'b1010
`define start_write_burst  4'b1011
`define write_burst_wait   4'b1100
`define finish_write_burst 4'b1101

// Information for AXI-based signals
`define AXI_LEN_WIDTH            4


module memctl (
               // Interface to controller
               m_task, 
               m_addr, 
               m_data, 
               m_req, 
               read_data, 
               done,
               clk,
               rready,
               wvalid,
               awlen,
	       read_busy,
	       write_busy,

               // Interface to memory
               address, 
               data, 
               read_n,
               write_n,
               enable_n
               );

   // Interface to controller
   input  [2:0] m_task;    
   input [31:0] m_addr;   
   input [31:0] m_data;   
   input        m_req;
   output       read_data;
   output       done;
   input        clk;
   input        rready;  // protocol bit for AXI read data channel
   input        wvalid;  // protocol bit for AXI write data channel
   input [`AXI_LEN_WIDTH - 1:0] awlen;  // protocol bit for AXI write data channel
   output                       read_busy;
   output                       write_busy;

   // Interface to memory
   output [31:0]                address;
   output [31:0]                data;
   output                       read_n;
   output                       write_n;
   output                       enable_n;

   // Interface to controller
   reg                          done;
   reg                          read_data;
   reg                          read_busy;
   reg                          write_busy;

   // Interface to memory 
   reg [31:0]                   address;
   wire [31:0]                  data;
   reg                          read_n;
   reg                          write_n; 
   reg                          enable_n;

   // Registers used by internal state machine
   reg [3:0]                    state;
   reg [4:0]                    addr_counter;
   reg [31:0]                   write_data;

   // Initialize output ports
   initial
     begin
        read_n = 1;
        write_n = 1;
        enable_n = 1;
        read_busy = 0;
        write_busy = 0;
     end

   always @(posedge clk)
     read_data <= !read_n;

   assign data = (enable_n) ?  write_data : 32'bz ; 

   always @(posedge clk)
     case (state)
       `ready:
         begin
            done    <= 1'b0;
            if (m_req)
              begin
                 write_n <= 1'b1;
                 read_n  <= 1'b1;
                 case (m_task)
                   `clear_memory:
                     begin
                        enable_n   <= 1'b1;
                        address    <= 32'h00;
                        write_data <= 32'h00;
                        state      <= `start_clear;
                     end
		   
                   `write_memory:
                     begin
                        enable_n   <= 1'b1;
                        address    <= m_addr;
                        write_data <= m_data;
                        state      <= `start_write;
                     end
		   
                   `read_memory: 
                     begin
                        enable_n <= 1'b0;
                        address  <= m_addr;
                        state    <= `start_read;
                     end

                   `read_burst:
                     begin
                        enable_n     <= 1'b0;
                        address      <= m_addr;
                        addr_counter <= 5'b0;
                        state        <= `start_read_burst;
		        read_busy = 1;
                     end

                   `write_burst:
                     begin
                        enable_n     <= 1'b1;
                        address      <= m_addr;
                        addr_counter <= 5'b0;
                        state        <= `start_write_burst;
		        write_busy = 1;
                     end
                 endcase
              end
            else
	      begin
                 state <= `ready;
	         read_busy = 0;
	         write_busy = 0;
	      end
            end

       `start_clear:
         begin
            write_n <= 1'b0;
            state   <= `finish_clear;
         end

       `finish_clear:
         begin
            write_n <= 1'b1;
            if (address != 32'hFF)
              begin
                 address <= address + 32'h01;
                 state   <= `start_clear;
              end
            else  
              begin
                 write_data <= 32'hZZ;
                 done       <= 1'b1;
                 state      <= `ready;
              end
         end

       `start_write:
         begin
            write_n <= 1'b0;
            state   <= `finish_write;
         end

       `finish_write:
         begin
            write_n    <= 1'b1;
            write_data <= 32'hZZ;
            done       <= 1'b1;
            state   <= `ready;
         end

       `start_read:
         begin
            read_n <= 1'b0;
            state  <= `read_wait;
         end

       `read_wait:
         begin
            read_n <= 1'b0;
            state  <= `finish_read;
         end

       `finish_read:
         begin
            read_n   <= 1'b1;
            enable_n <= 1'b1;
            done     <= 1'b1;
            state    <= `ready;
         end

       `start_read_burst:
         begin
            read_n <= 1'b0;
            state  <= `read_burst_wait;
         end

       `read_burst_wait:
         begin
            read_n <= 1'b0;
            if (rready)
              state  <= `finish_read_burst;
            else
              state  <= `read_burst_wait;
         end

       `finish_read_burst:
         begin
            if ((m_data[4:0] != 0) && (addr_counter < (m_data[4:0] - 1)))
              begin
                 address      <= address + 32'h01;
                 addr_counter <= addr_counter + 5'h01;
                 read_n       <= 1'b1;
                 state        <= `start_read_burst;
              end
            else  
              begin
                 read_n   <= 1'b1;
                 enable_n <= 1'b1;
                 done     <= 1'b1;
                 state    <= `ready;
	         if (!m_req)
		   read_busy = 0;
              end
         end

       `start_write_burst:
         begin
            if (wvalid)
              state  <= `write_burst_wait;
            else
	      begin
                 state  <= `ready;
	         if (!m_req)
		   write_busy = 0;
	      end
         end

       `write_burst_wait:
         begin
            write_n <= 1'b0;
            write_data <= m_data;
            if (wvalid)
              state  <= `finish_write_burst;
            else
              state  <= `write_burst_wait;
         end

       `finish_write_burst:
         begin
            if ((awlen != 0) && (addr_counter < awlen))
              begin
                 address      <= address + 32'h01;
                 addr_counter <= addr_counter + 5'h01;
                 write_n      <= 1'b1;
                 state        <= `start_write_burst;
              end
            else  
              begin
                 write_n   <= 1'b1;
                 enable_n <= 1'b1;
                 done     <= 1'b1;
                 state    <= `ready;
	         if (!m_req)
		   write_busy = 0;
              end
         end
       default:
         state <= `ready;
     endcase

endmodule
