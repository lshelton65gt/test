`timescale 1 ps / 1 ps



// ====================================
// ====================================
//         Environment Constants
// ====================================
// ====================================

`define AXI_ADDR_WIDTH          32              
`define AXI_DATA_WIDTH          32
`define AXI_DATA_MAX_BIT        31             
`define AXI_ADDRESS_MAX_BIT     31              
`define AXI_ID_TAG_WIDTH         4
`define AXI_SIZE_WIDTH           3
`define AXI_LEN_WIDTH            4
`define AXI_BURST_WIDTH          2
`define AXI_LOCK_WIDTH           2
`define AXI_CACHE_WIDTH          4
`define AXI_PROT_WIDTH           3
`define AXI_RESP_WIDTH           2
`define AXI_WSTRB_WIDTH          4

`define DEFAULT_ARVALID_VALUE   1'b0
`define DEFAULT_ARADDR_VALUE    {`AXI_ADDR_WIDTH{1'b0}}
`define DEFAULT_ARREADY_VALUE   1'b0
`define DEFAULT_ARLEN_VALUE     {`AXI_LEN_WIDTH{1'b0}}
`define DEFAULT_ARSIZE_VALUE    {`AXI_SIZE_WIDTH{1'b0}}
`define DEFAULT_ARBURST_VALUE   {`AXI_BURST_WIDTH{1'b0}}
`define DEFAULT_ARLOCK_VALUE    {`AXI_LOCK_WIDTH{1'b0}}
`define DEFAULT_ARCACHE_VALUE   {`AXI_CACHE_WIDTH{1'b0}}
`define DEFAULT_ARPROT_VALUE    {`AXI_PROT_WIDTH{1'b0}}
`define DEFAULT_ARID_VALUE      {`AXI_ID_TAG_WIDTH{1'b0}}
`define DEFAULT_ARREADY_VALUE   1'b0

`define DEFAULT_AWVALID_VALUE   1'b0
`define DEFAULT_AWADDR_VALUE    {`AXI_ADDR_WIDTH{1'b0}}
`define DEFAULT_AWREADY_VALUE   1'b0
`define DEFAULT_AWLEN_VALUE     {`AXI_LEN_WIDTH{1'b0}}
`define DEFAULT_AWSIZE_VALUE    {`AXI_SIZE_WIDTH{1'b0}}
`define DEFAULT_AWBURST_VALUE   {`AXI_BURST_WIDTH{1'b0}}
`define DEFAULT_AWLOCK_VALUE    {`AXI_LOCK_WIDTH{1'b0}}
`define DEFAULT_AWCACHE_VALUE   {`AXI_CACHE_WIDTH{1'b0}}
`define DEFAULT_AWPROT_VALUE    {`AXI_PROT_WIDTH{1'b0}}
`define DEFAULT_AWID_VALUE      {`AXI_ID_TAG_WIDTH{1'b0}}
`define DEFAULT_AWREADY_VALUE   1'b0

`define DEFAULT_RVALID_VALUE    1'b0
`define DEFAULT_RLAST_VALUE     1'b0
`define DEFAULT_RDATA_VALUE     {`AXI_DATA_WIDTH{1'b0}}
`define DEFAULT_RRESP_VALUE     {`AXI_RESP_WIDTH{1'b0}}
`define DEFAULT_RID_VALUE       {`AXI_ID_TAG_WIDTH{1'b0}}
`define DEFAULT_RREADY_VALUE    1'b0

`define DEFAULT_WVALID_VALUE    1'b0
`define DEFAULT_WLAST_VALUE     1'b0
`define DEFAULT_WDATA_VALUE     {`AXI_DATA_WIDTH{1'b0}}
`define DEFAULT_WSTRB_VALUE     {`AXI_WSTRB_WIDTH{1'b0}}
`define DEFAULT_WID_VALUE       {`AXI_ID_TAG_WIDTH{1'b0}}
`define DEFAULT_WREADY_VALUE    1'b0

`define DEFAULT_BVALID_VALUE    1'b0
`define DEFAULT_BREADY_VALUE    1'b0
`define DEFAULT_BRESP_VALUE     {`AXI_RESP_WIDTH{1'b0}}
`define DEFAULT_BID_VALUE       {`AXI_ID_TAG_WIDTH{1'b0}}

`ifdef CARBON
 `define AW_STATE_START         2'b00
 `define AW_STATE_DROP_READY    2'b01
 `define AW_STATE_INIT_WRITE    2'b10

 `define W_STATE_START          2'b00
 `define W_STATE_GEN_READY          2'b01
 `define W_STATE_RESP           2'b10
 `define W_STATE_DROP_VALID     2'b11

 `define AR_STATE_START         2'b00
 `define AR_STATE_DROP_READY    2'b01
`endif


module axi_slave(
                 // The Global clock and reset
                 ACLK,    
                 ARESETn,

                 // The write address channel signals

                 AWVALID, // Write address valid
                 AWADDR,  // Write address
                 AWLEN,   // Write address burst length
                 AWSIZE,  // Write address burst size
                 AWBURST, // Write address burst type
                 AWLOCK,  // Write address lock type 
                 AWCACHE, // Write address cache type
                 AWPROT,  // Write address protection level
                 AWID,    // Write address ID tag
                 AWREADY, // Write address ready

                 // The read address channel signals

                 ARVALID, // Read address valid
                 ARADDR,  // Read address
                 ARLEN,   // Read address burst length
                 ARSIZE,  // Read address burst size
                 ARBURST, // Read address burst type
                 ARLOCK,  // Read address lock type 
                 ARCACHE, // Read address cache type
                 ARPROT,  // Read address protection level
                 ARID,    // Read address ID tag
                 ARREADY, // Read address ready

                 // The read channel signals

                 RREADY,  // Read ready
                 RVALID,  // Read valid
                 RLAST,   // Read last
                 RDATA,   // Read data
                 RRESP,   // Read response
                 RID,     // Read ID tag

                 // The write channel signals

                 WVALID,  // Write valid
                 WLAST,   // Write last
                 WDATA,   // Write data
                 WSTRB,   // Write strobes
                 WID,     // Write ID tag
                 WREADY,  // Write ready

                 // The write-resp channel signals

                 BREADY,  // Write response ready
                 BVALID,  // Write response valid
                 BRESP,   // Write response
                 BID);    // Reasponse ID


   input                          ACLK;     // Global clock signal
   input                          ARESETn;  // Global reset signal
   
   input                          AWVALID;  // Write address valid
   input [`AXI_ADDRESS_MAX_BIT:0] AWADDR;   // Write address
   input [`AXI_LEN_WIDTH - 1:0]   AWLEN;    // Write address burst length
   input [`AXI_SIZE_WIDTH - 1:0]  AWSIZE;   // Write address burst size
   input [`AXI_BURST_WIDTH - 1:0] AWBURST;  // Write address burst type
   input [`AXI_LOCK_WIDTH - 1:0]  AWLOCK;   // Write address lock type 
   input [`AXI_CACHE_WIDTH - 1:0] AWCACHE;  // Write address cache type
   input [`AXI_PROT_WIDTH - 1:0]  AWPROT;   // Write address protection level
   input [`AXI_ID_TAG_WIDTH -1:0] AWID;     // Write address ID tag
   output                         AWREADY;  // Write address ready
   
   input                          ARVALID;  // Read address valid
   input [`AXI_ADDRESS_MAX_BIT:0] ARADDR;   // Read address
   input [`AXI_LEN_WIDTH - 1:0]   ARLEN;    // Read address burst length
   input [`AXI_SIZE_WIDTH - 1:0]  ARSIZE;   // Read address burst size
   input [`AXI_BURST_WIDTH - 1:0] ARBURST;  // Read address burst type
   input [`AXI_LOCK_WIDTH - 1:0]  ARLOCK;   // Read address lock type 
   input [`AXI_CACHE_WIDTH - 1:0] ARCACHE;  // Read address cache type
   input [`AXI_PROT_WIDTH - 1:0]  ARPROT;   // Read address protection level
   input [`AXI_ID_TAG_WIDTH -1:0] ARID;     // Read address ID tag
   output                         ARREADY;  // Read address ready
   
   
   input                          RREADY;   // Read ready
   output                         RVALID;   // Read valid
   output                         RLAST;    // Read last
   output [`AXI_DATA_MAX_BIT:0]   RDATA;    // Read data
   output [`AXI_RESP_WIDTH - 1:0] RRESP;    // Read response
   output [`AXI_ID_TAG_WIDTH -1:0] RID;     // Read ID tag
   
   input                           WVALID;   // Write valid
   input                           WLAST;    // Write last
   input [`AXI_DATA_MAX_BIT:0]     WDATA;    // Write data
   input [`AXI_WSTRB_WIDTH - 1:0]  WSTRB;    // Write strobes
   input [`AXI_ID_TAG_WIDTH -1:0]  WID;      // Write ID tag
   output                          WREADY;   // Write ready
   
   input                           BREADY;   // Write ready
   output                          BVALID;   // Write response valid
   output [`AXI_RESP_WIDTH - 1:0]  BRESP;    // Write response
   output [`AXI_ID_TAG_WIDTH -1:0] BID;     // Reasponse ID
   


   wire                            ACLK;     // Global clock signal
   wire                            ARESETn;  // Global reset signal
   
   wire                            AWVALID;  // Write address valid
   wire [`AXI_ADDRESS_MAX_BIT:0]   AWADDR;   // Write address
   wire [`AXI_LEN_WIDTH - 1:0]     AWLEN;    // Write address burst length
   wire [`AXI_SIZE_WIDTH - 1:0]    AWSIZE;   // Write address burst size
   wire [`AXI_BURST_WIDTH - 1:0]   AWBURST;  // Write address burst type
   wire [`AXI_LOCK_WIDTH - 1:0]    AWLOCK;   // Write address lock type 
   wire [`AXI_CACHE_WIDTH - 1:0]   AWCACHE;  // Write address cache type
   wire [`AXI_PROT_WIDTH - 1:0]    AWPROT;   // Write address protection level
   wire [`AXI_ID_TAG_WIDTH - 1:0]  AWID;     // Write address ID tag
   wire                            AWREADY;  // Write address ready
   
   wire                            ARVALID;  // Read address valid
   wire [`AXI_ADDRESS_MAX_BIT:0]   ARADDR;   // Read address
   wire [`AXI_LEN_WIDTH - 1:0]     ARLEN;    // Read address burst length
   wire [`AXI_SIZE_WIDTH - 1:0]    ARSIZE;   // Read address burst size
   wire [`AXI_BURST_WIDTH - 1:0]   ARBURST;  // Read address burst type
   wire [`AXI_LOCK_WIDTH - 1:0]    ARLOCK;   // Read address lock type 
   wire [`AXI_CACHE_WIDTH - 1:0]   ARCACHE;  // Read address cache type
   wire [`AXI_PROT_WIDTH - 1:0]    ARPROT;   // Read address protection level
   wire [`AXI_ID_TAG_WIDTH - 1:0]  ARID;     // Read address ID tag
   wire                            ARREADY;  // Read address ready
   
   
   wire                            RREADY;   // Read ready
   wire                            RVALID;   // Read valid
   wire                            RLAST;    // Read last
   wire [`AXI_DATA_MAX_BIT:0]      RDATA;    // Read data
   wire [`AXI_RESP_WIDTH - 1:0]    RRESP;    // Read response
   wire [`AXI_ID_TAG_WIDTH - 1:0]  RID;      // Read ID tag
   
   wire                            WVALID;   // Write valid
   wire                            WLAST;    // Write last
   wire [`AXI_DATA_MAX_BIT:0]      WDATA;    // Write data
   wire [`AXI_WSTRB_WIDTH - 1:0]   WSTRB;    // Write strobes
   wire [`AXI_ID_TAG_WIDTH - 1:0]  WID;      // Write ID tag
   wire                            WREADY;   // Write ready
   
   wire                            BREADY;   // Write ready
   wire                            BVALID;   // Write response valid
   wire [`AXI_RESP_WIDTH - 1:0]    BRESP;    // Write response
   wire [`AXI_ID_TAG_WIDTH - 1:0]  BID;      // Reasponse ID

   reg                             awready_o;
   reg                             arready_o;
   reg                             rvalid_o; 
   reg                             rlast_o;
   reg [`AXI_DATA_MAX_BIT:0]       rdata_o;
   reg [`AXI_RESP_WIDTH - 1:0]     rresp_o;
   reg [`AXI_ID_TAG_WIDTH -1:0]    rid_o;
   reg                             wready_o;
   reg                             bvalid_o;
   reg [`AXI_RESP_WIDTH - 1:0]     bresp_o;
   reg [`AXI_ID_TAG_WIDTH -1:0]    bid_o;

   initial
     begin
        awready_o =  `DEFAULT_AWREADY_VALUE;
        arready_o =  `DEFAULT_ARREADY_VALUE;
        rvalid_o  =  `DEFAULT_RVALID_VALUE;
        rlast_o   =  `DEFAULT_RLAST_VALUE;
        rdata_o   =  `DEFAULT_RDATA_VALUE;
        rresp_o   =  `DEFAULT_RRESP_VALUE;
        rid_o     =  `DEFAULT_RID_VALUE;
        wready_o  =  `DEFAULT_WREADY_VALUE;
        bvalid_o  =  `DEFAULT_BVALID_VALUE;
        bresp_o   =  `DEFAULT_BRESP_VALUE;
        bid_o     =  `DEFAULT_BID_VALUE;
     end

   assign AWREADY = awready_o;
   assign ARREADY = arready_o;
   assign RVALID  = rvalid_o;
   assign RLAST   = rlast_o;
   assign RDATA   = rdata_o;
   assign RRESP   = rresp_o;
   assign RID     = rid_o;
   assign WREADY  = wready_o;
   assign BVALID  = bvalid_o;
   assign BRESP   = bresp_o;
   assign BID     = bid_o;

   //*********************
   // Internal Signals
   //*********************
   wire [31:0] address;
   wire [31:0] rdata;
   wire [31:0] wdata;
   wire        read_n;
   reg         old_read_n;
   wire        write_n;
   wire        enable_n;
   reg [2:0]   m_task;
   reg [31:0]  m_addr;
   reg [31:0]  m_data;
   reg         m_req;
   reg         arbitrate_read;
   reg         write_just_started;
   wire        read_data;
   wire        done;
   wire        read_busy;
   wire        write_busy;

   reg [`AXI_LEN_WIDTH - 1:0] awlen;    // Write address burst length for mem ctlr

   initial
     begin
	m_req     = 0;
	m_task    = 0;
	m_addr    = 0;
	m_data    = 0;
	arbitrate_read = 0;
	write_just_started = 0;
     end
   
   //*********************
   // AXI Bus Behavior
   //*********************

   always @(posedge ACLK)
     begin
        // avoid deadlock, pass the arbitration on to the other channel
        if (arbitrate_read)
	  arbitrate_read = !AWVALID || ARVALID;
        else
	  arbitrate_read = AWVALID || ARVALID;
     end
   
   // Receive a single write request for the memory slave 
   // Write Address Channel
`ifdef CARBON
   reg [2:0] aw_state;
   initial begin
      aw_state = `AW_STATE_START;
   end
`endif
   always @(posedge ACLK)
     begin
`ifdef CARBON
        case(aw_state)
          `AW_STATE_START:
            begin
               if (AWVALID && !read_busy && !arbitrate_read)
	         begin
	            m_task <= 4;
	            m_addr <= AWADDR;
	            awlen <= AWLEN;
	            awready_o <= 1;
	            write_just_started <= 1;
                    aw_state <= `AW_STATE_DROP_READY;
                 end
            end

          `AW_STATE_DROP_READY:
            begin
	       awready_o <= 0;
               if (done)
                 aw_state <= `AW_STATE_INIT_WRITE;
            end

          `AW_STATE_INIT_WRITE:
            begin
               arbitrate_read <= ARVALID;
               write_just_started <= 0;
               aw_state <= `AW_STATE_START;
            end
        endcase
`else
        if (AWVALID && !read_busy && !arbitrate_read)
	  begin
             m_task = 4;
             m_addr = AWADDR;
             awlen = AWLEN;
             awready_o = 1;
             write_just_started = 1;	   
             @(posedge ACLK);
             awready_o = 0;
             @(posedge done);
             write_just_started = 0;	   
             // process a read next if there is a valid address write transfer
             arbitrate_read = ARVALID;
          end
`endif
     end

   // Write Data channel
`ifdef CARBON
   reg [1:0] w_state;
   initial w_state = `W_STATE_START;
`endif
   always @(posedge ACLK)
     begin
`ifdef CARBON
        case (w_state)
          `W_STATE_START:
            begin
               if (WVALID && (write_just_started || write_busy))
	         begin
	            m_req <= 1;
	            m_data <= WDATA;
                    w_state <= `W_STATE_GEN_READY;
                 end
            end

          `W_STATE_GEN_READY:
            begin
               m_req <= 0;
	       wready_o <= ~write_n;
               if (done)
                 begin
	            // accept a single data item after write data is processed
	            bid_o <= WID;
                    w_state <= `W_STATE_RESP;
                 end
            end

          `W_STATE_RESP:
            begin
	       wready_o <= 0;
	       bresp_o <= 0;
	       bvalid_o <= 1;
               w_state <= `W_STATE_DROP_VALID;
            end

          `W_STATE_DROP_VALID:
            begin
               bvalid_o <= 0;
               w_state <= `W_STATE_START;
            end
        endcase
`else
        if (WVALID && (write_just_started || write_busy))
	  begin
	     m_req = 1;
	     m_data = WDATA;
	     @(posedge done);
	     // accept a single data item after write data is processed
	     wready_o = 1;
	     bid_o = WID;	    
	     @(posedge ACLK);
	     wready_o = 0;
	     bresp_o = 0;
	     bvalid_o = 1;
	     @(posedge ACLK);
	     bvalid_o = 0;
          end
`endif
     end
   
`ifdef CARBON
`else
   always @(negedge WVALID)
     begin
	m_req = 0;
     end
`endif
   
   //  always @( write_n)
   //   begin
   //       wready_o = write_n;
   //   end
   
   // Receive a single read request for the memory slave
`ifdef CARBON
   reg [1:0] ar_state;
   reg [31:0] m_index;
   initial ar_state = `AR_STATE_START;
`endif
   always @(posedge ACLK)
     begin
`ifdef CARBON
        case (ar_state)
          `AR_STATE_START:
            begin
               if (ARVALID && 
	           arbitrate_read &&
	           !write_busy && // there is no ongoing write transaction
	           !write_just_started)
	         begin 
	            m_task <= 3;
	            m_req <= 1;
	            m_addr <= ARADDR;
	            m_data <= (ARLEN + 1);
                    m_index <= 0;
	            arready_o <= 1;
	            rid_o <= ARID;
                    ar_state <= `AR_STATE_DROP_READY;
                 end
            end

          `AR_STATE_DROP_READY:
            begin
	       arready_o <= 0;
               if (m_index == m_data)
                 begin
	            arbitrate_read <= !AWVALID;
	            m_req <= 0;
                    ar_state <= `AR_STATE_START;
                 end                   
            end
        endcase
`else
        if (ARVALID && 
	    arbitrate_read &&
	    !write_busy && // there is no ongoing write transaction
	    !write_just_started)
	  begin 
	     m_task = 3;
	     m_req = 1;
	     m_addr = ARADDR;
	     m_data = (ARLEN + 1);
	     arready_o = 1;
	     rid_o = ARID;
	     @(posedge ACLK);
	     arready_o = 0;
	     @(posedge done);
	     // process a write next if there is a valid address write transfer
	     arbitrate_read = !AWVALID;
	     m_req = 0;
	     rlast_o = 1;
	     @(posedge ACLK);
	     rlast_o = 0;
	  end // if (ARVALID &&...
`endif
     end

   // Adding this kludge code to handle multiple toggles RREADY/RVALID sigs
`ifdef CARBON
   always @(posedge ACLK)
     begin
        if (~read_n && old_read_n)
          begin
             rvalid_o <= 1;
             rdata_o <= rdata;
             if ((m_index + 1) == m_data)
               rlast_o <= 1;
             else
               rlast_o <= 0;
          end
        else if (RREADY)
          begin 
             rvalid_o <= 0;
             rlast_o <= 0;
             m_index <= m_index + 1;
          end
        old_read_n <= read_n;
     end
`else
   always @(negedge read_n)
     begin
        rvalid_o = 1;
        rdata_o = rdata;
        @(posedge ACLK);
        rvalid_o = 0;
     end
`endif
   //*********************
   // Instances
   //*********************

   memory mem8x256 (
                    .clk(ACLK),
                    .rdata(rdata), 
                    .wdata(wdata), 
                    .address(address),
                    .read_n(read_n), 
                    .write_n(write_n),
                    .enable_n(enable_n)
                    );

   memctl mctl (
                .m_task(m_task), 
                .m_addr(m_addr), 
                .m_data(m_data),
                .m_req(m_req),
                .read_data(read_data), 
                .done(done),
                .clk(ACLK),
                .rready(RREADY),
                .wvalid(WVALID),
                .awlen(awlen),
	        .read_busy(read_busy),
	        .write_busy(write_busy),

                .address(address), 
                .data(wdata), 
                .read_n(read_n),
                .write_n(write_n),
                .enable_n(enable_n)
                );

endmodule

`timescale 1 ps / 1 ps



// ====================================
// ====================================
//         Environment Constants
// ====================================
// ====================================

`define AXI_ADDR_WIDTH          32              
`define AXI_DATA_WIDTH          32
`define AXI_DATA_MAX_BIT        31             
`define AXI_ADDRESS_MAX_BIT     31              
`define AXI_ID_TAG_WIDTH         4
`define AXI_SIZE_WIDTH           3
`define AXI_LEN_WIDTH            4
`define AXI_BURST_WIDTH          2
`define AXI_LOCK_WIDTH           2
`define AXI_CACHE_WIDTH          4
`define AXI_PROT_WIDTH           3
`define AXI_RESP_WIDTH           2
`define AXI_WSTRB_WIDTH          4

`define DEFAULT_ARVALID_VALUE   1'b0
`define DEFAULT_ARADDR_VALUE    {`AXI_ADDR_WIDTH{1'b0}}
`define DEFAULT_ARREADY_VALUE   1'b0
`define DEFAULT_ARLEN_VALUE     {`AXI_LEN_WIDTH{1'b0}}
`define DEFAULT_ARSIZE_VALUE    {`AXI_SIZE_WIDTH{1'b0}}
`define DEFAULT_ARBURST_VALUE   {`AXI_BURST_WIDTH{1'b0}}
`define DEFAULT_ARLOCK_VALUE    {`AXI_LOCK_WIDTH{1'b0}}
`define DEFAULT_ARCACHE_VALUE   {`AXI_CACHE_WIDTH{1'b0}}
`define DEFAULT_ARPROT_VALUE    {`AXI_PROT_WIDTH{1'b0}}
`define DEFAULT_ARID_VALUE      {`AXI_ID_TAG_WIDTH{1'b0}}
`define DEFAULT_ARREADY_VALUE   1'b0

`define DEFAULT_AWVALID_VALUE   1'b0
`define DEFAULT_AWADDR_VALUE    {`AXI_ADDR_WIDTH{1'b0}}
`define DEFAULT_AWREADY_VALUE   1'b0
`define DEFAULT_AWLEN_VALUE     {`AXI_LEN_WIDTH{1'b0}}
`define DEFAULT_AWSIZE_VALUE    {`AXI_SIZE_WIDTH{1'b0}}
`define DEFAULT_AWBURST_VALUE   {`AXI_BURST_WIDTH{1'b0}}
`define DEFAULT_AWLOCK_VALUE    {`AXI_LOCK_WIDTH{1'b0}}
`define DEFAULT_AWCACHE_VALUE   {`AXI_CACHE_WIDTH{1'b0}}
`define DEFAULT_AWPROT_VALUE    {`AXI_PROT_WIDTH{1'b0}}
`define DEFAULT_AWID_VALUE      {`AXI_ID_TAG_WIDTH{1'b0}}
`define DEFAULT_AWREADY_VALUE   1'b0

`define DEFAULT_RVALID_VALUE    1'b0
`define DEFAULT_RLAST_VALUE     1'b0
`define DEFAULT_RDATA_VALUE     {`AXI_DATA_WIDTH{1'b0}}
`define DEFAULT_RRESP_VALUE     {`AXI_RESP_WIDTH{1'b0}}
`define DEFAULT_RID_VALUE       {`AXI_ID_TAG_WIDTH{1'b0}}
`define DEFAULT_RREADY_VALUE    1'b0

`define DEFAULT_WVALID_VALUE    1'b0
`define DEFAULT_WLAST_VALUE     1'b0
`define DEFAULT_WDATA_VALUE     {`AXI_DATA_WIDTH{1'b0}}
`define DEFAULT_WSTRB_VALUE     {`AXI_WSTRB_WIDTH{1'b0}}
`define DEFAULT_WID_VALUE       {`AXI_ID_TAG_WIDTH{1'b0}}
`define DEFAULT_WREADY_VALUE    1'b0

`define DEFAULT_BVALID_VALUE    1'b0
`define DEFAULT_BREADY_VALUE    1'b0
`define DEFAULT_BRESP_VALUE     {`AXI_RESP_WIDTH{1'b0}}
`define DEFAULT_BID_VALUE       {`AXI_ID_TAG_WIDTH{1'b0}}

`ifdef CARBON
 `define AW_STATE_START         2'b00
 `define AW_STATE_DROP_READY    2'b01
 `define AW_STATE_INIT_WRITE    2'b10

 `define W_STATE_START          2'b00
 `define W_STATE_GEN_READY          2'b01
 `define W_STATE_RESP           2'b10
 `define W_STATE_DROP_VALID     2'b11

 `define AR_STATE_START         2'b00
 `define AR_STATE_DROP_READY    2'b01
`endif


module axi_slave(
                 // The Global clock and reset
                 ACLK,    
                 ARESETn,

                 // The write address channel signals

                 AWVALID, // Write address valid
                 AWADDR,  // Write address
                 AWLEN,   // Write address burst length
                 AWSIZE,  // Write address burst size
                 AWBURST, // Write address burst type
                 AWLOCK,  // Write address lock type 
                 AWCACHE, // Write address cache type
                 AWPROT,  // Write address protection level
                 AWID,    // Write address ID tag
                 AWREADY, // Write address ready

                 // The read address channel signals

                 ARVALID, // Read address valid
                 ARADDR,  // Read address
                 ARLEN,   // Read address burst length
                 ARSIZE,  // Read address burst size
                 ARBURST, // Read address burst type
                 ARLOCK,  // Read address lock type 
                 ARCACHE, // Read address cache type
                 ARPROT,  // Read address protection level
                 ARID,    // Read address ID tag
                 ARREADY, // Read address ready

                 // The read channel signals

                 RREADY,  // Read ready
                 RVALID,  // Read valid
                 RLAST,   // Read last
                 RDATA,   // Read data
                 RRESP,   // Read response
                 RID,     // Read ID tag

                 // The write channel signals

                 WVALID,  // Write valid
                 WLAST,   // Write last
                 WDATA,   // Write data
                 WSTRB,   // Write strobes
                 WID,     // Write ID tag
                 WREADY,  // Write ready

                 // The write-resp channel signals

                 BREADY,  // Write response ready
                 BVALID,  // Write response valid
                 BRESP,   // Write response
                 BID);    // Reasponse ID


   input                          ACLK;     // Global clock signal
   input                          ARESETn;  // Global reset signal
   
   input                          AWVALID;  // Write address valid
   input [`AXI_ADDRESS_MAX_BIT:0] AWADDR;   // Write address
   input [`AXI_LEN_WIDTH - 1:0]   AWLEN;    // Write address burst length
   input [`AXI_SIZE_WIDTH - 1:0]  AWSIZE;   // Write address burst size
   input [`AXI_BURST_WIDTH - 1:0] AWBURST;  // Write address burst type
   input [`AXI_LOCK_WIDTH - 1:0]  AWLOCK;   // Write address lock type 
   input [`AXI_CACHE_WIDTH - 1:0] AWCACHE;  // Write address cache type
   input [`AXI_PROT_WIDTH - 1:0]  AWPROT;   // Write address protection level
   input [`AXI_ID_TAG_WIDTH -1:0] AWID;     // Write address ID tag
   output                         AWREADY;  // Write address ready
   
   input                          ARVALID;  // Read address valid
   input [`AXI_ADDRESS_MAX_BIT:0] ARADDR;   // Read address
   input [`AXI_LEN_WIDTH - 1:0]   ARLEN;    // Read address burst length
   input [`AXI_SIZE_WIDTH - 1:0]  ARSIZE;   // Read address burst size
   input [`AXI_BURST_WIDTH - 1:0] ARBURST;  // Read address burst type
   input [`AXI_LOCK_WIDTH - 1:0]  ARLOCK;   // Read address lock type 
   input [`AXI_CACHE_WIDTH - 1:0] ARCACHE;  // Read address cache type
   input [`AXI_PROT_WIDTH - 1:0]  ARPROT;   // Read address protection level
   input [`AXI_ID_TAG_WIDTH -1:0] ARID;     // Read address ID tag
   output                         ARREADY;  // Read address ready
   
   
   input                          RREADY;   // Read ready
   output                         RVALID;   // Read valid
   output                         RLAST;    // Read last
   output [`AXI_DATA_MAX_BIT:0]   RDATA;    // Read data
   output [`AXI_RESP_WIDTH - 1:0] RRESP;    // Read response
   output [`AXI_ID_TAG_WIDTH -1:0] RID;     // Read ID tag
   
   input                           WVALID;   // Write valid
   input                           WLAST;    // Write last
   input [`AXI_DATA_MAX_BIT:0]     WDATA;    // Write data
   input [`AXI_WSTRB_WIDTH - 1:0]  WSTRB;    // Write strobes
   input [`AXI_ID_TAG_WIDTH -1:0]  WID;      // Write ID tag
   output                          WREADY;   // Write ready
   
   input                           BREADY;   // Write ready
   output                          BVALID;   // Write response valid
   output [`AXI_RESP_WIDTH - 1:0]  BRESP;    // Write response
   output [`AXI_ID_TAG_WIDTH -1:0] BID;     // Reasponse ID
   


   wire                            ACLK;     // Global clock signal
   wire                            ARESETn;  // Global reset signal
   
   wire                            AWVALID;  // Write address valid
   wire [`AXI_ADDRESS_MAX_BIT:0]   AWADDR;   // Write address
   wire [`AXI_LEN_WIDTH - 1:0]     AWLEN;    // Write address burst length
   wire [`AXI_SIZE_WIDTH - 1:0]    AWSIZE;   // Write address burst size
   wire [`AXI_BURST_WIDTH - 1:0]   AWBURST;  // Write address burst type
   wire [`AXI_LOCK_WIDTH - 1:0]    AWLOCK;   // Write address lock type 
   wire [`AXI_CACHE_WIDTH - 1:0]   AWCACHE;  // Write address cache type
   wire [`AXI_PROT_WIDTH - 1:0]    AWPROT;   // Write address protection level
   wire [`AXI_ID_TAG_WIDTH - 1:0]  AWID;     // Write address ID tag
   wire                            AWREADY;  // Write address ready
   
   wire                            ARVALID;  // Read address valid
   wire [`AXI_ADDRESS_MAX_BIT:0]   ARADDR;   // Read address
   wire [`AXI_LEN_WIDTH - 1:0]     ARLEN;    // Read address burst length
   wire [`AXI_SIZE_WIDTH - 1:0]    ARSIZE;   // Read address burst size
   wire [`AXI_BURST_WIDTH - 1:0]   ARBURST;  // Read address burst type
   wire [`AXI_LOCK_WIDTH - 1:0]    ARLOCK;   // Read address lock type 
   wire [`AXI_CACHE_WIDTH - 1:0]   ARCACHE;  // Read address cache type
   wire [`AXI_PROT_WIDTH - 1:0]    ARPROT;   // Read address protection level
   wire [`AXI_ID_TAG_WIDTH - 1:0]  ARID;     // Read address ID tag
   wire                            ARREADY;  // Read address ready
   
   
   wire                            RREADY;   // Read ready
   wire                            RVALID;   // Read valid
   wire                            RLAST;    // Read last
   wire [`AXI_DATA_MAX_BIT:0]      RDATA;    // Read data
   wire [`AXI_RESP_WIDTH - 1:0]    RRESP;    // Read response
   wire [`AXI_ID_TAG_WIDTH - 1:0]  RID;      // Read ID tag
   
   wire                            WVALID;   // Write valid
   wire                            WLAST;    // Write last
   wire [`AXI_DATA_MAX_BIT:0]      WDATA;    // Write data
   wire [`AXI_WSTRB_WIDTH - 1:0]   WSTRB;    // Write strobes
   wire [`AXI_ID_TAG_WIDTH - 1:0]  WID;      // Write ID tag
   wire                            WREADY;   // Write ready
   
   wire                            BREADY;   // Write ready
   wire                            BVALID;   // Write response valid
   wire [`AXI_RESP_WIDTH - 1:0]    BRESP;    // Write response
   wire [`AXI_ID_TAG_WIDTH - 1:0]  BID;      // Reasponse ID

   reg                             awready_o;
   reg                             arready_o;
   reg                             rvalid_o; 
   reg                             rlast_o;
   reg [`AXI_DATA_MAX_BIT:0]       rdata_o;
   reg [`AXI_RESP_WIDTH - 1:0]     rresp_o;
   reg [`AXI_ID_TAG_WIDTH -1:0]    rid_o;
   reg                             wready_o;
   reg                             bvalid_o;
   reg [`AXI_RESP_WIDTH - 1:0]     bresp_o;
   reg [`AXI_ID_TAG_WIDTH -1:0]    bid_o;

   initial
     begin
        awready_o =  `DEFAULT_AWREADY_VALUE;
        arready_o =  `DEFAULT_ARREADY_VALUE;
        rvalid_o  =  `DEFAULT_RVALID_VALUE;
        rlast_o   =  `DEFAULT_RLAST_VALUE;
        rdata_o   =  `DEFAULT_RDATA_VALUE;
        rresp_o   =  `DEFAULT_RRESP_VALUE;
        rid_o     =  `DEFAULT_RID_VALUE;
        wready_o  =  `DEFAULT_WREADY_VALUE;
        bvalid_o  =  `DEFAULT_BVALID_VALUE;
        bresp_o   =  `DEFAULT_BRESP_VALUE;
        bid_o     =  `DEFAULT_BID_VALUE;
     end

   assign AWREADY = awready_o;
   assign ARREADY = arready_o;
   assign RVALID  = rvalid_o;
   assign RLAST   = rlast_o;
   assign RDATA   = rdata_o;
   assign RRESP   = rresp_o;
   assign RID     = rid_o;
   assign WREADY  = wready_o;
   assign BVALID  = bvalid_o;
   assign BRESP   = bresp_o;
   assign BID     = bid_o;

   //*********************
   // Internal Signals
   //*********************
   wire [31:0] address;
   wire [31:0] rdata;
   wire [31:0] wdata;
   wire        read_n;
   reg         old_read_n;
   wire        write_n;
   wire        enable_n;
   reg [2:0]   m_task;
   reg [31:0]  m_addr;
   reg [31:0]  m_data;
   reg         m_req;
   reg         arbitrate_read;
   reg         write_just_started;
   wire        read_data;
   wire        done;
   wire        read_busy;
   wire        write_busy;

   reg [`AXI_LEN_WIDTH - 1:0] awlen;    // Write address burst length for mem ctlr

   initial
     begin
	m_req     = 0;
	m_task    = 0;
	m_addr    = 0;
	m_data    = 0;
	arbitrate_read = 0;
	write_just_started = 0;
     end
   
   //*********************
   // AXI Bus Behavior
   //*********************

   always @(posedge ACLK)
     begin
        // avoid deadlock, pass the arbitration on to the other channel
        if (arbitrate_read)
	  arbitrate_read = !AWVALID || ARVALID;
        else
	  arbitrate_read = AWVALID || ARVALID;
     end
   
   // Receive a single write request for the memory slave 
   // Write Address Channel
`ifdef CARBON
   reg [2:0] aw_state;
   initial begin
      aw_state = `AW_STATE_START;
   end
`endif
   always @(posedge ACLK)
     begin
`ifdef CARBON
        case(aw_state)
          `AW_STATE_START:
            begin
               if (AWVALID && !read_busy && !arbitrate_read)
	         begin
	            m_task <= 4;
	            m_addr <= AWADDR;
	            awlen <= AWLEN;
	            awready_o <= 1;
	            write_just_started <= 1;
                    aw_state <= `AW_STATE_DROP_READY;
                 end
            end

          `AW_STATE_DROP_READY:
            begin
	       awready_o <= 0;
               if (done)
                 aw_state <= `AW_STATE_INIT_WRITE;
            end

          `AW_STATE_INIT_WRITE:
            begin
               arbitrate_read <= ARVALID;
               write_just_started <= 0;
               aw_state <= `AW_STATE_START;
            end
        endcase
`else
        if (AWVALID && !read_busy && !arbitrate_read)
	  begin
             m_task = 4;
             m_addr = AWADDR;
             awlen = AWLEN;
             awready_o = 1;
             write_just_started = 1;	   
             @(posedge ACLK);
             awready_o = 0;
             @(posedge done);
             write_just_started = 0;	   
             // process a read next if there is a valid address write transfer
             arbitrate_read = ARVALID;
          end
`endif
     end

   // Write Data channel
`ifdef CARBON
   reg [1:0] w_state;
   initial w_state = `W_STATE_START;
`endif
   always @(posedge ACLK)
     begin
`ifdef CARBON
        case (w_state)
          `W_STATE_START:
            begin
               if (WVALID && (write_just_started || write_busy))
	         begin
	            m_req <= 1;
	            m_data <= WDATA;
                    w_state <= `W_STATE_GEN_READY;
                 end
            end

          `W_STATE_GEN_READY:
            begin
               m_req <= 0;
	       wready_o <= ~write_n;
               if (done)
                 begin
	            // accept a single data item after write data is processed
	            bid_o <= WID;
                    w_state <= `W_STATE_RESP;
                 end
            end

          `W_STATE_RESP:
            begin
	       wready_o <= 0;
	       bresp_o <= 0;
	       bvalid_o <= 1;
               w_state <= `W_STATE_DROP_VALID;
            end

          `W_STATE_DROP_VALID:
            begin
               bvalid_o <= 0;
               w_state <= `W_STATE_START;
            end
        endcase
`else
        if (WVALID && (write_just_started || write_busy))
	  begin
	     m_req = 1;
	     m_data = WDATA;
	     @(posedge done);
	     // accept a single data item after write data is processed
	     wready_o = 1;
	     bid_o = WID;	    
	     @(posedge ACLK);
	     wready_o = 0;
	     bresp_o = 0;
	     bvalid_o = 1;
	     @(posedge ACLK);
	     bvalid_o = 0;
          end
`endif
     end
   
`ifdef CARBON
`else
   always @(negedge WVALID)
     begin
	m_req = 0;
     end
`endif
   
   //  always @( write_n)
   //   begin
   //       wready_o = write_n;
   //   end
   
   // Receive a single read request for the memory slave
`ifdef CARBON
   reg [1:0] ar_state;
   reg [31:0] m_index;
   initial ar_state = `AR_STATE_START;
`endif
   always @(posedge ACLK)
     begin
`ifdef CARBON
        case (ar_state)
          `AR_STATE_START:
            begin
               if (ARVALID && 
	           arbitrate_read &&
	           !write_busy && // there is no ongoing write transaction
	           !write_just_started)
	         begin 
	            m_task <= 3;
	            m_req <= 1;
	            m_addr <= ARADDR;
	            m_data <= (ARLEN + 1);
                    m_index <= 0;
	            arready_o <= 1;
	            rid_o <= ARID;
                    ar_state <= `AR_STATE_DROP_READY;
                 end
            end

          `AR_STATE_DROP_READY:
            begin
	       arready_o <= 0;
               if (m_index == m_data)
                 begin
	            arbitrate_read <= !AWVALID;
	            m_req <= 0;
                    ar_state <= `AR_STATE_START;
                 end                   
            end
        endcase
`else
        if (ARVALID && 
	    arbitrate_read &&
	    !write_busy && // there is no ongoing write transaction
	    !write_just_started)
	  begin 
	     m_task = 3;
	     m_req = 1;
	     m_addr = ARADDR;
	     m_data = (ARLEN + 1);
	     arready_o = 1;
	     rid_o = ARID;
	     @(posedge ACLK);
	     arready_o = 0;
	     @(posedge done);
	     // process a write next if there is a valid address write transfer
	     arbitrate_read = !AWVALID;
	     m_req = 0;
	     rlast_o = 1;
	     @(posedge ACLK);
	     rlast_o = 0;
	  end // if (ARVALID &&...
`endif
     end

   // Adding this kludge code to handle multiple toggles RREADY/RVALID sigs
`ifdef CARBON
   always @(posedge ACLK)
     begin
        if (~read_n && old_read_n)
          begin
             rvalid_o <= 1;
             rdata_o <= rdata;
             if ((m_index + 1) == m_data)
               rlast_o <= 1;
             else
               rlast_o <= 0;
          end
        else if (RREADY)
          begin 
             rvalid_o <= 0;
             rlast_o <= 0;
             m_index <= m_index + 1;
          end
        old_read_n <= read_n;
     end
`else
   always @(negedge read_n)
     begin
        rvalid_o = 1;
        rdata_o = rdata;
        @(posedge ACLK);
        rvalid_o = 0;
     end
`endif
   //*********************
   // Instances
   //*********************

   memory mem8x256 (
                    .clk(ACLK),
                    .rdata(rdata), 
                    .wdata(wdata), 
                    .address(address),
                    .read_n(read_n), 
                    .write_n(write_n),
                    .enable_n(enable_n)
                    );

   memctl mctl (
                .m_task(m_task), 
                .m_addr(m_addr), 
                .m_data(m_data),
                .m_req(m_req),
                .read_data(read_data), 
                .done(done),
                .clk(ACLK),
                .rready(RREADY),
                .wvalid(WVALID),
                .awlen(awlen),
	        .read_busy(read_busy),
	        .write_busy(write_busy),

                .address(address), 
                .data(wdata), 
                .read_n(read_n),
                .write_n(write_n),
                .enable_n(enable_n)
                );

endmodule

