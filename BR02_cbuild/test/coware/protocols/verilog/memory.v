module memory (rdata, wdata, address, read_n, write_n, enable_n, clk);
   output [31:0] rdata;
   input [31:0]  wdata;
   input [31:0]  address;
   input         read_n, write_n, enable_n;
   input         clk;

`ifdef CARBON
`else
  function has_X;
    input   [31:0] bus;
    integer       i;

    begin
      has_X = 0;
      for (i = 0; i < 32; i = i + 1)
        if (bus[i] === 1'bX)
          has_X = 1;
    end
  endfunction

  function has_Z;
    input   [31:0] bus;
    integer       i;

    begin
      has_Z = 0;
      for (i = 0; i < 32; i = i + 1)
        if (bus[i] === 1'bZ)
          has_Z = 1;
    end
  endfunction
`endif
   
   parameter read_delay  = 0;
   parameter write_delay = 0;
   parameter MEM_SIZE = 65535;

   reg [31:0] membank [0:MEM_SIZE];
   integer    i;

`ifdef CARBON
   assign rdata = (enable_n || read_n) ? 32'b0 : membank[address[15:0]];

   always @(posedge clk)
     if (enable_n && ~write_n)
       membank[address[15:0]] <= #write_delay wdata;
`else

  // Initialize all memory values to 0
  initial
    begin
      for (i = 0; i < MEM_SIZE; i = i + 1)
       begin
         membank[i] = i;
       end
      data_reg = 32'h00000000;
    end

  always @(negedge read_n)
    data_reg <= #read_delay membank[address[15:0]];

  assign data = enable_n ? 31'bz : data_reg; 

  always @(negedge write_n)
    if (enable_n)
      membank[address[15:0]] <= #write_delay data;
`endif

endmodule
module memory (rdata, wdata, address, read_n, write_n, enable_n, clk);
   output [31:0] rdata;
   input [31:0]  wdata;
   input [31:0]  address;
   input         read_n, write_n, enable_n;
   input         clk;

`ifdef CARBON
`else
  function has_X;
    input   [31:0] bus;
    integer       i;

    begin
      has_X = 0;
      for (i = 0; i < 32; i = i + 1)
        if (bus[i] === 1'bX)
          has_X = 1;
    end
  endfunction

  function has_Z;
    input   [31:0] bus;
    integer       i;

    begin
      has_Z = 0;
      for (i = 0; i < 32; i = i + 1)
        if (bus[i] === 1'bZ)
          has_Z = 1;
    end
  endfunction
`endif
   
   parameter read_delay  = 0;
   parameter write_delay = 0;
   parameter MEM_SIZE = 65535;

   reg [31:0] membank [0:MEM_SIZE];
   integer    i;

`ifdef CARBON
   assign rdata = (enable_n || read_n) ? 32'b0 : membank[address[15:0]];

   always @(posedge clk)
     if (enable_n && ~write_n)
       membank[address[15:0]] <= #write_delay wdata;
`else

  // Initialize all memory values to 0
  initial
    begin
      for (i = 0; i < MEM_SIZE; i = i + 1)
       begin
         membank[i] = i;
       end
      data_reg = 32'h00000000;
    end

  always @(negedge read_n)
    data_reg <= #read_delay membank[address[15:0]];

  assign data = enable_n ? 31'bz : data_reg; 

  always @(negedge write_n)
    if (enable_n)
      membank[address[15:0]] <= #write_delay data;
`endif

endmodule
