module AXITest(ACLK, ARESETn,
               AWVALIDS, AWADDRS, AWLENS, AWSIZES, AWBURSTS, AWLOCKS, AWCACHES, AWPROTS, AWIDS, AWREADYS,
               WVALIDS, WLASTS, WDATAS, WSTRBS, WIDS, WREADYS,
               BVALIDS, BRESPS, BIDS, BREADYS,
               ARVALIDS, ARADDRS, ARLENS, ARSIZES, ARBURSTS, ARLOCKS, ARCACHES, ARPROTS, ARIDS, ARREADYS,
               RVALIDS, RLASTS, RDATAS, RRESPS, RIDS, RREADYS,
               AWVALIDM, AWADDRM, AWLENM, AWSIZEM, AWBURSTM, AWLOCKM, AWCACHEM, AWPROTM, AWIDM, AWREADYM,
               WVALIDM, WLASTM, WDATAM, WSTRBM, WIDM, WREADYM,
               BVALIDM, BRESPM, BIDM, BREADYM,
               ARVALIDM, ARADDRM, ARLENM, ARSIZEM, ARBURSTM, ARLOCKM, ARCACHEM, ARPROTM, ARIDM, ARREADYM,
               RVALIDM, RLASTM, RDATAM, RRESPM, RIDM, RREADYM);

   // Global
   input         ACLK;
   input         ARESETn;

   // Address write channel (slave)
   input         AWVALIDS;
   input [31:0]  AWADDRS;
   input [3:0]   AWLENS;
   input [2:0]   AWSIZES;
   input [1:0]   AWBURSTS;
   input [1:0]   AWLOCKS;
   input [3:0]   AWCACHES;
   input [2:0]   AWPROTS;
   input [3:0]   AWIDS;
   output        AWREADYS;

   // Write channel (slave)
   input         WVALIDS;
   input         WLASTS;
   input [63:0]  WDATAS;
   input [7:0]   WSTRBS;
   input [3:0]   WIDS;
   output        WREADYS;

   // Response channel (slave)
   output        BVALIDS;
   output [1:0]  BRESPS;
   output [3:0]  BIDS;
   input         BREADYS;

   // Address read channel (slave)
   input         ARVALIDS;
   input [31:0]  ARADDRS;
   input [3:0]   ARLENS;
   input [2:0]   ARSIZES;
   input [1:0]   ARBURSTS;
   input [1:0]   ARLOCKS;
   input [3:0]   ARCACHES;
   input [2:0]   ARPROTS;
   input [3:0]   ARIDS;
   output        ARREADYS;

   // Read Channel (slave)
   output        RVALIDS;
   output        RLASTS;
   output [63:0] RDATAS;
   output [1:0]  RRESPS;
   output [3:0]  RIDS;
   input         RREADYS;

   // Address write channel (master)
   output        AWVALIDM;
   output [31:0] AWADDRM;
   output [3:0]  AWLENM;
   output [2:0]  AWSIZEM;
   output [1:0]  AWBURSTM;
   output [1:0]  AWLOCKM;
   output [3:0]  AWCACHEM;
   output [2:0]  AWPROTM;
   output [3:0]  AWIDM;
   input         AWREADYM;

   // Write channel (master)
   output        WVALIDM;
   output        WLASTM;
   output [63:0] WDATAM;
   output [7:0]  WSTRBM;
   output [3:0]  WIDM;
   input         WREADYM;

   // Response channel (master)
   input         BVALIDM;
   input [1:0]   BRESPM;
   input [3:0]   BIDM;
   output        BREADYM;

   // Address read channel (master)
   output        ARVALIDM;
   output [31:0] ARADDRM;
   output [3:0]  ARLENM;
   output [2:0]  ARSIZEM;
   output [1:0]  ARBURSTM;
   output [1:0]  ARLOCKM;
   output [3:0]  ARCACHEM;
   output [2:0]  ARPROTM;
   output [3:0]  ARIDM;
   input         ARREADYM;

   // Read Channel (master)
   input         RVALIDM;
   input         RLASTM;
   input [63:0]  RDATAM;
   input [1:0]   RRESPM;
   input [3:0]   RIDM;
   output        RREADYM;

   // Address write channel outputs
   reg           AWVALIDM;
   reg [31:0]    AWADDRM;
   reg [3:0]     AWLENM;
   reg [2:0]     AWSIZEM;
   reg [1:0]     AWBURSTM;
   reg [1:0]     AWLOCKM;
   reg [3:0]     AWCACHEM;
   reg [2:0]     AWPROTM;
   reg [3:0]     AWIDM;
   reg           AWREADYS;

   // Write channel outputs
   reg           WVALIDM;
   reg           WLASTM;
   reg [63:0]    WDATAM;
   reg [7:0]     WSTRBM;
   reg [3:0]     WIDM;
   reg           WREADYS;

   // Response channel outputs
   reg           BVALIDS;
   reg [1:0]     BRESPS;
   reg [3:0]     BIDS;
   reg           BREADYM;

   // Address read channel outputs
   reg           ARVALIDM;
   reg [31:0]    ARADDRM;
   reg [3:0]     ARLENM;
   reg [2:0]     ARSIZEM;
   reg [1:0]     ARBURSTM;
   reg [1:0]     ARLOCKM;
   reg [3:0]     ARCACHEM;
   reg [2:0]     ARPROTM;
   reg [3:0]     ARIDM;
   reg           ARREADYS;

   // Read channel outputs
   reg           RVALIDS;
   reg           RLASTS;
   reg [63:0]    RDATAS;
   reg [1:0]     RRESPS;
   reg [3:0]     RIDS;
   reg           RREADYM;
   
   // Address write channel
   always @ (posedge ACLK or negedge ARESETn)
     begin
        if (!ARESETn)
          begin
             AWVALIDM <= 1'b0;
             AWADDRM <= 32'b0;
             AWLENM <= 4'b0;
             AWSIZEM <= 3'b0;
             AWBURSTM <= 2'b0;
             AWLOCKM <= 2'b0;
             AWCACHEM <= 4'b0;
             AWPROTM <= 3'b0;
             AWIDM <= 4'b0;
             AWREADYS <= 1'b0;
          end
        else
          begin
             AWVALIDM <= AWVALIDS;
             AWADDRM <= AWADDRS;
             AWLENM <= AWLENS;
             AWSIZEM <= AWSIZES;
             AWBURSTM <= AWBURSTS;
             AWLOCKM <= AWLOCKS;
             AWCACHEM <= AWCACHES;
             AWPROTM <= AWPROTS;
             AWIDM <= AWIDS;
             AWREADYS <= AWREADYM;
          end
     end

   // Write Channel
   always @ (posedge ACLK or negedge ARESETn)
     begin
        if (!ARESETn)
          begin
             WVALIDM <= 1'b0;
             WLASTM <= 1'b0;
             WDATAM <= 64'b0;
             WSTRBM <= 8'b0;
             WIDM <= 4'b0;
             WREADYS <= 1'b0;
          end
        else
          begin
             WVALIDM <= WVALIDS;
             WLASTM <= WLASTS;
             WDATAM <= WDATAS;
             WSTRBM <= WSTRBS;
             WIDM <= WIDS;
             WREADYS <= WREADYM;
          end
     end

   // Response channel
   always @ (posedge ACLK or negedge ARESETn)
     begin
        if (!ARESETn)
          begin
             BVALIDS <= 1'b0;
             BRESPS <= 2'b0;
             BIDS <= 4'b0;
             BREADYM <= 1'b0;
          end
        else
          begin
             BVALIDS <= BVALIDM;
             BRESPS <= BRESPM;
             BIDS <= BIDM;
             BREADYM <= BREADYS;
          end
     end

   // Address read channel
   always @ (posedge ACLK or negedge ARESETn)
     begin
        if (!ARESETn)
          begin
             ARVALIDM <= 1'b0;
             ARADDRM <= 32'b0;
             ARLENM <= 4'b0;
             ARSIZEM <= 3'b0;
             ARBURSTM <= 2'b0;
             ARLOCKM <= 2'b0;
             ARCACHEM <= 4'b0;
             ARPROTM <= 3'b0;
             ARIDM <= 4'b0;
             ARREADYS <= 1'b0;
          end
        else
          begin
             ARVALIDM <= ARVALIDS;
             ARADDRM <= ARADDRS;
             ARLENM <= ARLENS;
             ARSIZEM <= ARSIZES;
             ARBURSTM <= ARBURSTS;
             ARLOCKM <= ARLOCKS;
             ARCACHEM <= ARCACHES;
             ARPROTM <= ARPROTS;
             ARIDM <= ARIDS;
             ARREADYS <= ARREADYM;
          end
     end

   // Read Channel
   always @ (posedge ACLK or negedge ARESETn)
     begin
        if (!ARESETn)
          begin
             RVALIDS <= 1'b0;
             RLASTS <= 1'b0;
             RDATAS <= 64'b0;
             RRESPS <= 2'b0;
             RIDS <= 4'b0;
             RREADYM <= 1'b0;
          end
        else
          begin
             RVALIDS <= RVALIDM;
             RLASTS <= RLASTM;
             RDATAS <= RDATAM;
             RRESPS <= RRESPM;
             RIDS <= RIDM;
             RREADYM <= RREADYS;
          end
     end


endmodule
