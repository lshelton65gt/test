module mem1(rdata, raddr, wen, waddr, wdata, clk);
   output [63:0] rdata;
   input [7:0]   raddr;
   input         wen;
   input [7:0]   waddr;
   input [63:0]  wdata;
   input         clk;

   // Single bank memories
   reg [31:0]    mem32 [7:0]; // carbon observeSignal
   reg [35:0]    mem36 [7:0]; // carbon observeSignal
   reg [39:0]    mem40 [7:0]; // carbon observeSignal
   reg [63:0]    mem64 [7:0]; // carbon observeSignal
   reg [31:0]    mem32_rev[0:7]; // carbon observeSignal
   reg [0:31]    mem32_be[0:7]; // carbon observeSignal
   reg [7:0]     mem8[7:0];   // carbon observeSignal
   reg [15:0]    mem16[7:0];  // carbon observeSignal
   
   // Multiple bank memories
   reg [7:0]     mem8_0 [7:0]; //carbon observeSignal
   reg [7:0]     mem8_1 [7:0]; //carbon observeSignal
   reg [7:0]     mem8_2 [7:0]; //carbon observeSignal
   reg [7:0]     mem8_3 [7:0]; //carbon observeSignal

   // Multiple bank and rtl locator memories
   reg [15:0]    mem16_0 [7:0]; //carbon observeSignal
   reg [15:0]    mem16_1 [7:0]; //carbon observeSignal
   reg [15:0]    mem16_2 [7:0]; //carbon observeSignal
   reg [15:0]    mem16_3 [7:0]; //carbon observeSignal

   // Memory with wholes in it. bank0 is 0-3 and bank 2 is 8-11
   reg [31:0]    mem32_0[3:0]; // carbon observeSignal
   reg [31:0]    mem32_2[3:0]; // carbon observeSignal

   // Memory where the banks reverse the bits
   reg [31:0]    mem32_swapb[7:0]; // carbon observeSignal    

   // Write the appropriate memory
   always @ (posedge clk)
     if (wen)
       begin
          case(waddr[7:4])
            4'd0:
              mem32[waddr[2:0]] <= wdata[31:0];

            4'd1:
              mem36[waddr[2:0]] <= wdata[35:0];

            4'd2:
              mem40[waddr[2:0]] <= wdata[39:0];

            4'd3:
              mem64[waddr[2:0]] <= wdata[63:0];

            4'd4:
              begin
                 mem8_0[waddr[2:0]] <= wdata[7:0];
                 mem8_1[waddr[2:0]] <= wdata[15:8];
                 mem8_2[waddr[2:0]] <= wdata[23:16];
                 mem8_3[waddr[2:0]] <= wdata[31:24];
              end

            4'd5:
              if (waddr[3])
                begin
                   mem16_2[waddr[2:0]] <= wdata[15:0];
                   mem16_3[waddr[2:0]] <= wdata[31:16];
                end
              else
                begin
                   mem16_0[waddr[2:0]] <= wdata[15:0];
                   mem16_1[waddr[2:0]] <= wdata[31:16];
                end

            4'd6:
              case (waddr[3:2])
                2'd0:
                  mem32_0[waddr[1:0]] <= wdata[31:0];

                2'd2:
                  mem32_2[waddr[1:0]] <= wdata[31:0];

              endcase

            4'd7:
              // Write the memory in reverse because the mapping is opposite the storage
              mem32_rev[3'h7 - waddr[2:0]] <= wdata[31:0];

            4'd8:
              // Big endian memory. For now don't reverse the bits or bytes. We need to learn how customers
              // deal with big endian memories
              mem32_be[3'h7 - waddr[2:0]] <= wdata[31:0];

            4'd9:
              mem8[waddr[2:0]] <= wdata[7:0];
            
            4'd10:
              mem16[waddr[2:0]] <= wdata[15:0];
            
            4'd11:
              mem32_swapb[waddr[2:0]] <= {wdata[7:0], wdata[15:8], wdata[23:16], wdata[31:24]};
            
            default:
              ;
          endcase
       end

   // Read the memory
   reg [63:0] rdata;
   always @ (posedge clk)
     begin : read
        reg [63:0] value;
        case(raddr[7:4])
          4'd0:
            rdata <= {32'b0, mem32[raddr[2:0]]};

          4'd1:
            rdata <= {28'b0, mem36[raddr[2:0]]};

          4'd2:
            rdata <= {24'b0, mem40[raddr[2:0]]};

          4'd3:
            rdata <= mem64[raddr[2:0]];

          4'd4:
            begin
               rdata[7:0] <= mem8_0[raddr[2:0]];
               rdata[15:8] <= mem8_1[raddr[2:0]];
               rdata[23:16] <= mem8_2[raddr[2:0]];
               rdata[31:24] <= mem8_3[raddr[2:0]];
               rdata[63:32] <= 32'b0;
            end

          4'd5:
            if (raddr[3])
              begin
                 rdata[15:0] <= mem16_2[raddr[2:0]];
                 rdata[31:16] <= mem16_3[raddr[2:0]];
                 rdata[63:32] <= 32'b0;
              end
            else
              begin
                 rdata[15:0] <= mem16_0[raddr[2:0]];
                 rdata[31:16] <= mem16_1[raddr[2:0]];
                 rdata[63:32] <= 32'b0;
              end

          4'd6:
            case (raddr[3:2])
              2'd0:
                rdata <= {32'b0, mem32_0[raddr[1:0]]};

              2'd2:
                rdata <= {32'b0, mem32_2[raddr[1:0]]};

              default:
                rdata <= 64'b0;

            endcase

          4'd7:
            // Read the memory in reverse because the mapping is opposite the storage
            rdata <= {32'b0, mem32_rev[3'h7 - raddr[2:0]]};
          
          4'd8:
            // Big endian memory. For now don't reverse the bits or bytes. We need to learn how customers
            // deal with big endian memories
            rdata <= {32'b0, mem32_be[3'h7 - raddr[2:0]]};
            
          4'd9:
            rdata <= { 56'b0, mem8[raddr[2:0]]};
          
          4'd10:
            rdata <= { 48'b0, mem16[raddr[2:0]]};
          
          4'd11:
            begin
               value[31:0] = mem32_swapb[raddr[2:0]];
               rdata <= {32'b0, value[7:0], value[15:8], value[23:16], value[31:24]};
            end
          
          default:
            rdata <= 64'd0;
        endcase
     end
            
endmodule
   
