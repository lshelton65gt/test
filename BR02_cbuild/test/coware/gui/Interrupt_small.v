// *********************************************************************
// * Filename : Interrupt.v
// * Author: B. Maaref
// *
// * Copyright (c) 1996-2003
// * CoWare, Inc.
// * 2121 North First St.
// * San Jose, CA 95131
// * USA
// *
// * Purpose: verilog model of simple interrupt controller
// **********************************************************************
// HARDCODED for Demo
//  NR_IRQ = 32
`timescale 1ps/1ps

module Interrupt (

    HCLK,
    HRESETn,
    HSEL,
    HWRITE,
    HREADY,
    HPROT,
    HTRANS,
    HSIZE,
    HWDATA,
    HADDR,
    HREADYOUT,
    HRESP,
    HRDATA,
//    IntSource,
    IRQin0,
    IRQin1,
    IRQin2,
    p_nICINT);

  input         HCLK;
  input         HRESETn;
  input         HSEL;
  input         HWRITE;
  input         HREADY;
  input  [3:0]  HPROT;
  input   [1:0] HTRANS;
  input  [2:0]   HSIZE;
  input  [31:0]  HWDATA;
  input  [9:0]  HADDR;
  output        HREADYOUT;
  output [1:0]  HRESP;
  output [31:0] HRDATA;
//  input  [31:0]  IntSource;
  input 	IRQin0;
  input 	IRQin1;
  input 	IRQin2;
   
  output        p_nICINT;
  
  reg 		p_nICINT;
   wire 	HREADYOUT;
  wire   [1:0]   HRESP;
  reg   [31:0]  HRDATA; 
  reg   [31:0]  ICIntEnable;
  reg   [31:0]  Raw_INT_Interrupts; 
  reg   [1:0]  	RegHTRANS;
  reg   [9:0] 	RegHADDR;
  reg		RegHWRITE;   
  reg		RegHSEL; 
  reg		RegHSIZE;    
  reg   [31:0]  WriteData;
  reg   [31:0]  ReadData;
  wire [31:0] 	ICActiveLevel;
  wire [31:0] 	ICLevelEdge;

   wire [31:0] 	IntSource;
   assign      IntSource = {29'h1FFFFFFF, IRQin2, IRQin1, IRQin0};
   assign      ICActiveLevel = 32'h0;
   assign      ICLevelEdge = 32'h0;
   
   wire        resetn;
   assign      resetn = HRESETn;
   
   
   always @ (ICIntEnable or Raw_INT_Interrupts)
     begin :Evaluate_INT_interrupts
	if ( (ICIntEnable & Raw_INT_Interrupts) != 0 )
	  begin
	     p_nICINT = 1'b0;
	  end
	else
	  begin
	     p_nICINT = 1'b1;
	  end
     end

   assign HREADYOUT = 1'b1;
   assign HRESP = 2'b00;
   
   
   always @ (posedge HCLK or negedge resetn)
     begin :inter_select
	if (!resetn)
	  RegHSEL  <= 0;   
	else if (HREADY)
	  begin
	     RegHSEL <= HSEL;
	  end
     end


   always @ (posedge HCLK or negedge resetn)
     begin :bus_buffer
	if (!resetn)
	  begin
             RegHTRANS <= 0;
	     RegHADDR <= 0;
	     RegHWRITE <=  0; 
	     RegHSIZE <=  0; 
	  end
	else
	  begin
             if (HREADY & HSEL)
               begin
              	  RegHTRANS <= HTRANS;
		  RegHADDR <= HADDR;
		  RegHWRITE <=  HWRITE;     
		  RegHSIZE <=  HSIZE; 
               end
	  end
     end     

   always @ (posedge HCLK or negedge resetn )
     begin
	if (~resetn)
	  begin
	     ICIntEnable <= 0;
	  end
	else 
	  begin :bus_write
	     if (RegHSEL && (RegHTRANS == 2'b10 || RegHTRANS == 2'b11))
	       begin
		  if(RegHWRITE )
		    begin  
		       if (RegHADDR == 10'h0)
			 begin
      			    // Interrupt enable register
     			    ICIntEnable <= HWDATA;
      			 end
		    end
	       end
	  end
     end // always @ (posedge HCLK or negedge resetn )
   
   integer i;

   always @ (RegHADDR or RegHWRITE or RegHSEL or RegHTRANS or ICIntEnable or ICActiveLevel or ICLevelEdge or Raw_INT_Interrupts)
     begin : bus_read
	if (RegHSEL && (RegHTRANS == 2'b10 || RegHTRANS == 2'b11) )
	  begin
	     if(!RegHWRITE)
	       begin
		  case (RegHADDR)  		
    		    10'h0:
		      begin
      			 // ENABLE register
      			 HRDATA = ICIntEnable;
      		      end
		    
    		    10'h8:
		      begin
      			 //  Interrupt Active Level register
      			 HRDATA = ICActiveLevel;
      		      end
		    
    		    10'hC:
		      begin
      			 // Interrupt Level Edge register
      			 HRDATA = ICLevelEdge;
      		      end
		    
    		    10'h10:
		      begin
			 for (i=31; i>=0; i=i-1)
			   begin
			      if (Raw_INT_Interrupts[i] & ICIntEnable[i])
				HRDATA = 1'b1 << i;
			   end
      		      end
  		  endcase
	       end
	  end
	else
	  begin
	     HRDATA = 0;
	  end
     end

   wire [31:0] Interrupt_in;
   assign      Interrupt_in = ~IntSource;

   always @(posedge HCLK or negedge resetn)
     if (~resetn)
       begin
	  Raw_INT_Interrupts <= 0;
       end
     else 
       if ((RegHSEL && (RegHTRANS == 2'b10 || RegHTRANS == 2'b11))
	   && (RegHWRITE ) 
	   && (RegHADDR == 10'h4))
	 Raw_INT_Interrupts <= Raw_INT_Interrupts & ~(HWDATA);
       else
	 Raw_INT_Interrupts <= Interrupt_in;
   
endmodule
