module top(in, clk, out);
  output out;
  input  in, clk;
  reg    q;                     // carbon scObserveSignal
                                // carbon scDepositSignal

  always @(posedge clk)
    q <= in;
  assign out = ~q;
endmodule
