// *********************************************************************
// * Filename : Interrupt.v
// * Author: B. Maaref
// *
// * Copyright (c) 1996-2003
// * CoWare, Inc.
// * 2121 North First St.
// * San Jose, CA 95131
// * USA
// *
// * Purpose: verilog model of simple interrupt controller
// **********************************************************************
// HARDCODED for Demo
//  NR_IRQ = 32
`timescale 1ps/1ps

module Interrupt (

    HCLK,
    HRESETn,
    HSEL,
    HWRITE,
    HREADY,
    HPROT,
    HTRANS,
    HSIZE,
    HWDATA,
    HADDR,
    HREADYOUT,
    HRESP,
    HRDATA,
//    IntSource,
    IRQin0,
    IRQin1,
    IRQin2,
    p_nICINT);

  input         HCLK;
  input         HRESETn;
  input         HSEL;
  input         HWRITE;
  input         HREADY;
  input  [3:0]  HPROT;
  input   [1:0] HTRANS;
  input  [2:0]   HSIZE;
  input  [31:0]  HWDATA;
  input  [9:0]  HADDR;
  output        HREADYOUT;
  output [1:0]  HRESP;
  output [31:0] HRDATA;
//  input  [31:0]  IntSource;
  input 	IRQin0;
  input 	IRQin1;
  input 	IRQin2;
   
  output        p_nICINT;
  
  reg 		p_nICINT;
  reg          	HREADYOUT;
  reg   [1:0]   HRESP;
  reg   [31:0]  HRDATA; 
  reg   [31:0]  Raw_INT_Interrupts; 
  reg   [1:0]  	RegHTRANS;
  reg   [9:0] 	RegHADDR;
  reg		RegHWRITE;   
  reg		RegHSEL; 
  reg		RegHSIZE;    
  reg   [31:0]  WriteData;
  reg   [31:0]  ReadData;
  reg   [31:0]  ICIntEnable;
  reg [31:0] 	ICActiveLevel;
  reg [31:0] 	ICLevelEdge;
  reg [31:0] 	ICClear;

  integer  i;

   wire [31:0] IntSource;
   assign      IntSource = {~ICActiveLevel[31:3], IRQin2, IRQin1, IRQin0};
   
   reg 	       rst_active_level;
   initial rst_active_level = 0;
   
   wire        resetn;
   assign      resetn = rst_active_level ? ~HRESETn : HRESETn;
   
   reg 	       irq_active_level;
   initial irq_active_level = 0;
   
   always @ (ICIntEnable or Raw_INT_Interrupts)
     begin :Evaluate_INT_interrupts
	if ( (ICIntEnable & Raw_INT_Interrupts) != 0 )
	  begin
	     if (irq_active_level)
	       p_nICINT = 1'b1;
	     else 
	       p_nICINT = 1'b0;
	  end
	else
	  begin
	     if (irq_active_level)
	       p_nICINT = 1'b0;
	     else 
	       p_nICINT = 1'b1;
	  end
     end

   always @ (posedge HCLK or negedge resetn)
     begin :bus_respond
	if (!resetn)
	  begin
             HREADYOUT <= 1'b1 ;
             HRESP     <= 2'b00 ;
	  end
	else
	  begin
             HREADYOUT <= 1'b1;
             HRESP     <= 2'b00;
	  end
     end

   always @ (posedge HCLK or negedge resetn)
     begin :inter_select
	if (!resetn)
	  RegHSEL  <= 0;   
	else if (HREADY)
	  begin
	     RegHSEL <= HSEL;
	  end
     end


   always @ (posedge HCLK or negedge resetn)
     begin :bus_buffer
	if (!resetn)
	  begin
             RegHTRANS <= 0;
	     RegHADDR <= 0;
	     RegHWRITE <=  0; 
	     RegHSIZE <=  0; 
	  end
	else
	  begin
             if (HREADY & HSEL)
               begin
              	  RegHTRANS <= HTRANS;
		  RegHADDR <= HADDR;
		  RegHWRITE <=  HWRITE;     
		  RegHSIZE <=  HSIZE; 
               end
	  end
     end     
   


   always @ (posedge HCLK or negedge resetn )
     if (~resetn)
       begin
	  ICIntEnable <= 0;
	  ICActiveLevel <= 0;
	  ICLevelEdge <= 0;
	  ICClear <= 0;
       end
     else begin :bus_write
	if (RegHSEL && (RegHTRANS == 2'b10 || RegHTRANS == 2'b11))
	  begin
	     if(RegHWRITE )
	       begin  
  		  case(RegHADDR)  		
    		    10'h0:
		      begin
      			 // Interrupt enable register
     			 ICIntEnable <= HWDATA;
      		      end
    		    10'h4:
		      begin
      			 // Interrupt clear
			 ICClear <= HWDATA;
      		      end
    		    10'h8:
		      begin
      			 //  Interrupt Active Level register
      			 ICActiveLevel <= HWDATA;
      		      end
    		    10'hC:
		      begin
      			 // Interrupt Level Edge register
      			 ICLevelEdge <= HWDATA;
      		      end
    		    // 10'hC:
		    // begin
      		    // Interrupt IRQ Vector
      		    // end
  		  endcase  
	       end
	  end
     end

   reg prio_hw;
   initial prio_hw = 1'b1;
   
   always @ (RegHADDR or RegHWRITE or RegHSEL or RegHTRANS or ICIntEnable or ICActiveLevel or ICLevelEdge or Raw_INT_Interrupts)
     begin : bus_read
	if (RegHSEL && (RegHTRANS == 2'b10 || RegHTRANS == 2'b11) )
	  begin
	     if(!RegHWRITE)
	       begin
		  case (RegHADDR)  		
    		    10'h0:
		      begin
      			 // ENABLE register
      			 HRDATA = ICIntEnable;
      		      end
    		    10'h4:
		      begin
      			 // ENABLE register
      			 HRDATA = ICClear;
      		      end
    		    10'h8:
		      begin
      			 //  Interrupt Active Level register
      			 HRDATA = ICActiveLevel;
      		      end
		    
    		    10'hC:
		      begin
      			 // Interrupt Level Edge register
      			 HRDATA = ICLevelEdge;
      		      end
		    
    		    10'h10:
		      begin
      			 if (prio_hw)
			   for (i=0; i<32; i=i+1)
			     begin
				if (Raw_INT_Interrupts[i] & ICIntEnable[i])
				  HRDATA = 1'b1 << i;
			     end
			 else
			   HRDATA = Raw_INT_Interrupts & ICIntEnable;
      		      end
  		  endcase
	       end
	  end
	else
	  begin
	     HRDATA = 0;
	  end
     end

   reg [31:0] Interrupt_in;
   always @(ICActiveLevel or IntSource)
     for (i = 0; i < 32; i = i + 1)
       Interrupt_in[i] =  ICActiveLevel[i] ? IntSource[i] : ~IntSource[i];
   
   reg [1:0] state;
	    
   always @(posedge HCLK or negedge resetn)
     if (~resetn)
       begin
	  state <= 2'h0;
	  Raw_INT_Interrupts <= 0;
       end
     else for (i = 0; i<32; i = i + 1)
       if (ICLevelEdge[i])
	 begin
	    case (state)
	      2'h0: 
		if (Interrupt_in[i])
		  begin
		     Raw_INT_Interrupts[i] <= 1'b1;
		     state <= 2'h1;
		  end
	      2'h1:
		if (RegHSEL && (RegHTRANS == 2'b10 || RegHTRANS == 2'b11))
		  if(RegHWRITE )
		    if (RegHADDR == 10'h4)  
		      if (HWDATA[i] == 1'b1)
			begin
			   Raw_INT_Interrupts[i] <= 1'b0;
			   if (Interrupt_in[i] == 1'b0)
			     state <= 2'h0;
			   else
			     state <= 2'h2;
			end
	      2'h2:
		if (Interrupt_in[i] == 1'b0)
		  state <= 2'h0;
	    endcase
	 end
       else
	 if ((RegHSEL && (RegHTRANS == 2'b10 || RegHTRANS == 2'b11))
	      && (RegHWRITE ) 
	      && (RegHADDR == 10'h4)
	      && (HWDATA[i] == 1'b1)
	     )
	     Raw_INT_Interrupts[i] <= 1'b0;
	 else
	     Raw_INT_Interrupts[i] <= Interrupt_in[i];

endmodule
