#include "uart_monitor.h"

void monitor::prc_monitor()
{
  cout << "Time: " << sc_time_stamp() << ":: " << hex;
  cout << " reset: " << reset;
  cout << " ld_tx_data: " << ld_tx_data;
  cout << " tx_data: " << tx_data;
  cout << " tx_enable: " << tx_enable;
  cout << " tx_out: " << tx_out;
  cout << " tx_empty: " << tx_empty;
  cout << " rx_enable: " << rx_enable;
  cout << " rx_data " << rx_data;
  cout << " rx_in " << rx_in;
  cout << " rx_empty " << rx_empty;
  cout << " rx_cnt " << rx_cnt;
  cout << " rx_busy " << rx_busy;
  cout << " rx_frame_err " << rx_frame_err;
  cout << " rx_sample_cnt " << rx_sample_cnt;
  cout << " uld_rx_data " << uld_rx_data;
  cout << " rx_ready " << rx_ready;
  cout << dec << endl;
}
