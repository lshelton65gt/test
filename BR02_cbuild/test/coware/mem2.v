module mem2(rdata, raddr, waddr, wen, wdata, clk);
   output [63:0] rdata;
   input [7:0]   raddr;
   input         wen;
   input [7:0]   waddr;
   input [63:0]  wdata;
   input         clk;

   reg [31:0]    mem32 [7:0]; // carbon observeSignal
   reg [35:0]    mem36 [7:0]; // carbon observeSignal
   reg [63:0]    mem64 [7:0]; // carbon observeSignal

   // Write the appropriate memory
   always @ (posedge clk)
     if (wen)
       begin
          case(waddr[7:4])
            4'd0:
              mem32[waddr[2:0]] <= wdata[31:0];

            4'd1:
              mem36[waddr[2:0]] <= wdata[35:0];

            4'd2:
              mem64[waddr[2:0]] <= wdata[63:0];

            default:
              ;
          endcase
       end

   // Read the memory
   reg [63:0] rdata;
   always @ (posedge clk)
     begin
        case(raddr[7:4])
          4'd0:
            rdata <= {32'b0, mem32[raddr[2:0]]};

          4'd1:
            rdata <= {28'b0, mem36[raddr[2:0]]};

          4'd2:
            rdata <= mem64[raddr[2:0]];

          default:
            ;
        endcase
     end
            
endmodule
   
