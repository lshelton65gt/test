// Test flattening with hierrefs.
// Test fixup of a sibling when a niece is flattened into a sibling.
module top(o, d, we);
   output o;
   input  d,we;
   sibling1 sibling1 (o);
   sibling2 sibling2 (d, we);
endmodule

module sibling1(o);
   output o;
   assign o = sibling2.niece.r;
endmodule

module sibling2(d, we);
   input d, we;
   always @(we or d)
     if (we)
       niece.r = d;
   niece niece ();
endmodule

module niece();
   reg r;
endmodule
