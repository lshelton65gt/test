module a(o1, o2, i1, i2);
  output o1;
  output o2;
  input i1;
  input i2;

  // Output regs (2)
  reg                         o1;
  reg                         o2;

  // Initial Blocks
  initial                                          // task10.v:36
    // $block_task10_v_L36;1
    begin                                          // task10.v:36
      // Declarations
      // Statements
      o1 = 0;                                      // task10.v:36
    end
  initial                                          // task10.v:37
    // $block_task10_v_L37;1
    begin                                          // task10.v:37
      // Declarations
      // Statements
      o2 = 0;                                      // task10.v:37
    end
endmodule // a
module b_1();

  // Instances
  c                                                // task10.v:58
    C( );                                          // task10.v:48
endmodule // b_1
module b_2();

  // Instances
  c                                                // task10.v:58
    C( );                                          // task10.v:54
endmodule // b_2
module c();
endmodule // c
module top(out1, out2, in1, in2);
  output out1;
  output out2;
  input in1;
  input in2;

  // Local nets (4)
  wire                        t1;
  wire                        t2;
  wire                        t3;
  wire                        t4;

  // Continuous Assigns
  assign out1 = (t1 ^ t2);                         // task10.v:8
  assign out2 = (t3 ^ t4);                         // task10.v:9

  // Always Blocks
  always                                           // task10.v:38
    // $block_task10_v_L38;1
    begin                                          // task10.v:38
      doand(top.X.A.o1, top.X.A.i1, top.X.A.i2);
    end
  always                                           // task10.v:41
    // $block_task10_v_L41;1
    begin                                          // task10.v:41
      // Declarations
      // Statements
      do_or(top.X.A.o2, top.X.A.i1, top.X.A.i2);
    end
  always                                           // task10.v:38
    // $block_task10_v_L38;2
    begin                                          // task10.v:38
      // Declarations
      // Statements
      doand(top.Y.A.o1, top.Y.A.i1, top.Y.A.i2);
    end
  always                                           // task10.v:41
    // $block_task10_v_L41;2
    begin                                          // task10.v:41
      // Declarations
      // Statements
      do_or(top.Y.A.o2, top.Y.A.i1, top.Y.A.i2);
    end

  // Tasks
  task doand;
    output y;
    input a;
    input b;
    y = (a & b);                                     // task10.v:65
  endtask

  task do_or;
    output y;
    input a;
    input b;
    y = (a | b);                                     // task10.v:74
  endtask

  // Instances
  x                                                // task10.v:13
    X(                                             // task10.v:6
      .o1(t1),                                     // output
      .o2(t2),                                     // output
      .i1(in1),                                    // input
      .i2(in2));                                   // input
  y                                                // task10.v:22
    Y(                                             // task10.v:7
      .o1(t3),                                     // output
      .o2(t4),                                     // output
      .i1(in1),                                    // input
      .i2(in2));                                   // input
endmodule // top
module x(o1, o2, i1, i2);
  output o1;
  output o2;
  input i1;
  input i2;

  // Instances
  a                                                // task10.v:31
    A(                                             // task10.v:17
      .o1(o1),                                     // output
      .o2(o2),                                     // output
      .i1(i1),                                     // input
      .i2(i2));                                    // input
  b_1                                              // task10.v:46
    B( );                                          // task10.v:18
endmodule // x
module y(o1, o2, i1, i2);
  output o1;
  output o2;
  input i1;
  input i2;

  // Instances
  a                                                // task10.v:31
    A(                                             // task10.v:26
      .o1(o1),                                     // output
      .o2(o2),                                     // output
      .i1(i1),                                     // input
      .i2(i2));                                    // input
  b_2                                              // task10.v:52
    B( );                                          // task10.v:27
endmodule // y
