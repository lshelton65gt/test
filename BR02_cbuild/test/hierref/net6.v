   module top (out1, out2, in1, in2);
     output [1:0] out1, out2;
     input [1:0] in1, in2;

     child C1(out1, in1);
     child C2(out2, in2);

   endmodule

   module child (y, a);
     output [1:0] y;
     input [1:0] a;

     inv I1(a);
     outInv I2(y);

   endmodule

   module inv(i);
     input [1:0] i;
     reg [1:0] r;
     always @ (i)
       r = ~i;
   endmodule

   module outInv(o);
     output [1:0] o;

     assign o = I1.r;

   endmodule
