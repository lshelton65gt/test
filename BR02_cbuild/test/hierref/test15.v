// Test simple child task reference.
module top(o, data, clk, re, we, addr);
   output [1:0] o;
   input [1:0]  data;
   input [1:0]  addr;
   input        clk;
   input        re;
   input        we;

   reg [1:0]    o;
   
   always @(posedge clk)
     if (re)
       child.read_memory(addr, o);
     else if (we)
       child.write_memory(data, addr);

   child child ();
   
endmodule

module child();
   reg [1:0]    mem [3:0];
   integer    i;

   initial begin
     for (i = 0; i < 4; i = i + 1)
       mem[i] = 2'b0;
   end

   task read_memory;
      input [1:0] addr;
      output [1:0] o;
      o = mem[addr];
   endtask

   task write_memory;
      input [1:0] i;
      input [1:0] addr;
      mem[addr] = i;
   endtask
endmodule
