module top(o, data, clk, re, we, addr);
   output [1:0] o;
   input [1:0]  data;
   input [1:0]  addr;
   input        clk;
   input        re;
   input        we;

   reg [1:0]    o;
   reg [1:0]    mem [3:0];
   integer      i;
   
   initial begin
     for (i = 0; i < 4; i = i + 1)
       mem[i] = 2'b0;
   end

   always @(posedge clk)
     if (re)
       read_memory(o);
     else if (we)
       write_memory(data);

   task read_memory;
      output [1:0] o;
      o = mem[addr];
   endtask

   task write_memory;
      input [1:0] i;
      mem[addr] = i;
   endtask
endmodule
