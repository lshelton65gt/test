// Hierrefs: test multiple instantiations, different resolutions
module top();
  parent u1();
  parent u2();
endmodule

module parent();
  wire local1;
  wire local2;
  assign local1 = top.u1.child1.thenet;
  assign local2 = child1.thenet;
  child child1();
endmodule

module child();
  wire thenet;
endmodule
