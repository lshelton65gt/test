module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   sub1 S1 (out1, clk, in1);
   sub2 S2 (out2, clk, in2);

endmodule // top

module sub1 (out, clk, in);
   output out;
   input  clk, in;

   enable1 EN (1'b0);
   flop F1 (out, clk, in);

endmodule // sub

module sub2 (out, clk, in);
   output out;
   input  clk, in;

   enable2 EN (1'b1);
   flop F1 (out, clk, in);

endmodule // sub

module enable1 (en_in);
   input en_in;

   wire  en = en_in;

endmodule // enable


module enable2 (en_in);
   input en_in;

   wire  en = en_in;

endmodule // enable


module flop (q, clk, d);
   output q;
   input  clk, d;
   reg    q;

   wire en = EN.en;

   initial q = 0;
   always @ (posedge clk)
     if (en)
       q <= d;

endmodule // flop
