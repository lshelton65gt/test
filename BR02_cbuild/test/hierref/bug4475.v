module top(clk,in);
   input clk,in;
   // the presence of an additional wire caused a lookupElab assertion;
   // probably because the symtab lookup found a different net than
   // expected.
   wire  en; 
   forceunder fu(clk,in);
   connected connected(clk,fu.data);
endmodule

module forceunder(clk,in);
   input clk,in;
   reg 	 data; // carbon forceSignal
   initial data = 0;
   always @(posedge clk)
     data = in;
endmodule

module connected(clk,data);
   input clk;
   input data; // carbon observeSignal
endmodule
