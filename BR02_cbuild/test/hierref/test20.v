// bug 1932 - locally resolvable references within
// hierarchically referenced tasks.
module top(i1, i2, o);
   output o;
   reg    o;
   input  i1, i2;
   always @(i1 or i2)
     begin
        subroutines.t1(o);
     end
   subroutines subroutines();
endmodule

module subroutines;
   task t1;
      output o;
      o = child.i1 & child.i2;
   endtask
   child child();
endmodule

module child;
   wire i1;
   wire i2;
   assign i1 = top.i1;
   assign i2 = top.i2;
endmodule
