// Bug 2015 test case - test multiple possible resolutions with parameters.
module top(clk1, clk2, o1, o2, d1, d2);
   input clk1, clk2;
   output [31:0] o1;
   output [31:0] o2;
   input [31:0]  d1;
   input [31:0]  d2;
   child #(31) child ();
   middle1 middle1(clk1, clk2, o1, d1);
   middle2 middle2(clk1, clk2, o2, d2);
endmodule

module middle1(clk1, clk2, o, d);
   input clk1, clk2;
   input [31:0] d;
   output [31:0] o;
   reg [31:0]    o;
   
   always @(posedge clk1)
     child.q = d;
   always @(posedge clk2)
     o = child.q;
endmodule

module middle2(clk1, clk2, o, d);
   input clk1, clk2;
   output [31:0] o;
   input [31:0]  d;
   middle1 middle1(clk1, clk2, o, d);
   child #(31) child ();
endmodule

module child();
   parameter HIGHBIT = 3;
   reg [HIGHBIT:0] q;
endmodule
