// Test non-local function reads.
module top(clk, i1, i2, i3, o);
   input clk;
   input i1, i2, i3;
   output o;
   child child (clk, i1, i2, i3, o);
endmodule

module child(clk, i1, i2, i3, o);
   input clk;
   input i1, i2, i3;
   output o;
   reg    o;
   reg    w;

   always @(posedge clk)
     w = i1 & i2;

   always @(i3 or w)
     t(i3);

   task t;
      input i1;
      o = f(i1);
   endtask

   task t1;
      input i1;
      output o;
      o = f(i1);
   endtask

   function f;
      input i1;
      f = w & i1;
   endfunction
endmodule
