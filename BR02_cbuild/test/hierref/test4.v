// Hierrefs: test hier refs in one module (parent) that don't resolve to the same object, (considered inconsistent resolutions in interra flow)
module top(input clk, input i1, i2, output o1, o2);
   middle1 m1(clk, i1, i2, o1);
   middle2 m2(clk, i1, i2, o2);
endmodule

module middle1(input clk, input i1, i2, output o1);
   parent p(clk, o1);
   child1 c(i1);
endmodule

module middle2(input clk, input i1, i2, output o2);
   parent p(clk, o2);
   child2 c(i2);
endmodule

module parent(input clk, output reg o);
  wire local1;
  assign local1 = c.thenet;
   always @(posedge clk)
    o = local1;
endmodule

module child1(input i);
  wire thenet;
   assign thenet = i;
endmodule

module child2(input i);
  wire [3:0] thenet;
   assign thenet = i;
endmodule
