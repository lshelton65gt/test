module top(a,b);
   input a,b;
   
   storage storage();

   assign storage.data = a & b;
endmodule

module storage();
   wire data; // carbon observeSignal
endmodule
