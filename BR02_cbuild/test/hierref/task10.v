module top (out1, out2, in1, in2);
   output out1, out2;
   input  in1, in2;

   wire   t1, t2, t3, t4;
   x X(t1, t2, in1, in2);
   y Y(t3, t4, in1, in2);
   assign out1 = t1 ^ t2;
   assign out2 = t3 ^ t4;

endmodule // top

module x(o1, o2, i1, i2);
   output o1, o2;
   input  i1, i2;

   a A(o1, o2, i1, i2);
   b_1 B();

endmodule // x

module y(o1, o2, i1, i2);
   output o1, o2;
   input  i1, i2;

   a A(o1, o2, i1, i2);
   b_2 B();

endmodule // y

module a(o1, o2, i1, i2);
   output o1, o2;
   input  i1, i2;

   reg    o1, o2;
   initial o1 = 0;
   initial o2 = 0;
   always @ (i1 or i2)
     B.C.doand(o1, i1, i2);

   always @ (i1 or i2)
     B.C.do_or(o2, i1, i2);

endmodule // a

module b_1;

   c C();

endmodule // b

module b_2;

   c C();

endmodule // d

module c;

   task doand;
      output y;
      input a;
      input b;

      y = a & b;

   endtask // doand

   task do_or;
      output y;
      input a;
      input b;

      y = a | b;

   endtask // do_or

endmodule // c
