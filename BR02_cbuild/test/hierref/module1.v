module top (out, in);
   output out;
   input  in;

   sub S1 (out, in);

endmodule // top

module sub (y, a);
   output y;
   input  a;

   assign y = ~sub.a;

endmodule // sub
