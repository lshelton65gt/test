// this test is setup so that module 'child' references data.d that is resolved
// to two different nets, depending on where child was instantiated.

// this was not supported in the interra flow, it is supported in the verific flow

module top (input clk, input [1:0] i1, output o1, o2);
  middle middle(clk, i1, o1);
  child child(o2);
  topdata data(clk, i1);
endmodule

module middle(input clk, input [1:0] i1, output o);
  child child(o);
  middata data(clk, i1);
endmodule

module child(output o);
   wire w;
   assign w = data.d[0];
   assign o = w;
endmodule

module topdata(input clk, input [1:0] i);
  reg [1:0] d;
   always @(posedge clk)
     d = i;
endmodule

module middata(input clk, input [1:0] i);
  reg [0:1] d;
   always @(posedge clk)
     d = i;
endmodule
