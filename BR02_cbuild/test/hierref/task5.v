// Test simple child task reference.
module top(out1, out2, in1, in2, clk);
   output [1:0] out1, out2;
   input [1:0]  in1, in2;
   input        clk;
   
   task doand;
      output [1:0] o;
      input [1:0] i1, i2;
      o = i1 & i2;
   endtask // doand

   child child (out1, clk, in1, in2);
   
   reg [1:0]    out2;
   always @ (in1)
     child.doneg(out2, in1);
   
endmodule

module child(o1, clk, i1, i2);
   output [1:0] o1;
   input        clk;
   input  [1:0] i1, i2;

   reg [1:0] o1;
   always @(posedge clk)
     top.doand(o1, i1, i2);

   task doneg;
      output [1:0] o;
      input [1:0] i;
      o = ~i;
   endtask // doneg

endmodule
