// Hierrefs: test inconsistent task enables
module top(clk, rd_en, addr, data_in, data_out1, data_out2);
   input clk;
   input rd_en;
   wire wr_en = ~rd_en;
   input [4:0] addr;
   input [7:0] data_in;
   output [7:0] data_out1, data_out2;

   middle_one middle1(clk, rd_en, wr_en, addr, data_in, data_out1);
   middle_two middle2(clk, rd_en, wr_en, addr, data_in, data_out2);

endmodule


module middle_one(clk, rd_en, wr_en, addr, data_in, data_out);
   input clk;
   input rd_en;
   input wr_en;
   input [4:0] addr;
   input [7:0] data_in;
   output [7:0] data_out;

   parent parent(clk, rd_en, wr_en, addr, data_in, data_out);
   child_one child();
endmodule


module middle_two(clk, rd_en, wr_en, addr, data_in, data_out);
   input clk;
   input rd_en;
   input wr_en;
   input [4:0] addr;
   input [7:0] data_in;
   output [7:0] data_out;

   parent parent(clk, rd_en, wr_en, addr, data_in, data_out);
   child_two child();
endmodule


module parent(clk, rd_en, wr_en, addr, data_in, data_out);
   input clk;
   input rd_en;
   input wr_en;
   input [4:0] addr;
   input [7:0] data_in;
   output [7:0] data_out;
   reg [7:0] data_out;

   always @(posedge clk)
     begin
	if (rd_en)
	  child.do_read(addr, data_out);
	if (wr_en)
	  child.do_write(addr, data_in);
     end

endmodule


module child_one();
   reg [7:0] mem [31:0];
   integer   i;
   initial
      begin
	 for (i = 0; i < 32; i = i + 1)
	    begin
	       mem[i] = i;
	    end
      end

   task do_write;
      input [4:0] address;
      input [7:0] data;
      mem[address] = data;
   endtask
   task do_read;
      input [4:0] address;
      output [7:0] data;
      data = mem[address];
   endtask

endmodule


module child_two();
   reg [7:0] mem [31:0];
   integer   i;
   initial
      begin
	 for (i = 0; i < 32; i = i + 1)
	    begin
	       mem[i] = 32+i;
	    end
      end

   task do_write;
      input [4:0] address;
      input [7:0] data;
      mem[address] = data;
   endtask
   task do_read;
      input [4:0] address;
      output [7:0] data;
      data = mem[address];
   endtask

endmodule
