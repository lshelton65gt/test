module top (out, in, waddr, raddr, clk);
   output [1:0] out;
   input  [1:0] in, waddr, raddr;
   input        clk;

   assign       out = c.mem[raddr];
   child c(in, waddr, clk);

endmodule

module child(d, waddr, clk);
   input [1:0] d;
   input [1:0] waddr;
   input       clk;

   reg [1:0]    mem [3:0];
`ifdef VERILOGSIM
   integer     i;
   initial begin
      for (i = 0; i < 4; i = i + 1)
        mem[i] = 0;
   end
`endif

   always @ (posedge clk)
     mem[waddr] <= d;

endmodule
