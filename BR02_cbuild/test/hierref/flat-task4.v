// Test simple child task reference.
module top(out1, out2, in1, in2, clk);
   output [1:0] out1, out2;
   input [1:0]  in1, in2;
   input        clk;
   
   task doand;
      output [1:0] o;
      input [1:0] i1, i2;
      o = i1 & i2;
   endtask

   a a (out1, out2, in1, in2, clk);
   
endmodule

module a(out1,out2,in1,in2,clk);
   output [1:0] out1, out2;
   input [1:0]  in1, in2;
   input        clk;

   b b(out1,out2,in1,in2,clk);

endmodule

module b(out1,out2,in1,in2,clk);
   output [1:0] out1, out2;
   input [1:0]  in1, in2;
   input        clk;
   reg [1:0]    out1;

   assign out2 = ~in1;
   
   always @(posedge clk)
     top.doand(out1, in1, in2);

endmodule
