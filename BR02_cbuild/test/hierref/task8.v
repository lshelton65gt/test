module top (out1, out2, out3, in1, in2);
   output out1, out2, out3;
   input  in1, in2;

   sub1 a_b();
   sub2 a(out3, in1, in2);

   sub S1 (out1, out2, in1, in2);

endmodule // top

module sub1;
   task c;
      output o;
      input i1;
      input i2;

      o = i1 & i2;

   endtask // c
endmodule // sub1

module sub2(z, x, y);
   output z;
   input  x, y;

   sub1 b();

   assign z = x & y;

endmodule // sub2

module sub(o1, o2, i1, i2);
   output o1, o2;
   input  i1, i2;

   reg    o1, o2;
   initial begin
      o1 = 0;
      o2 = 0;
   end
   always @ (i1 or i2)
     begin
        a_b.c(o1, i1, i2);
        a.b.c(o2, i1, i2);
     end

endmodule // sub
