// Test flattening with hierrefs.
// Test a parent module having/not-having the same hierref as the child.
module top(o, data, we, re);
   output [3:0] o;
   input [3:0]  data;
   input [3:0]  we;
   input [3:0]  re;

   others others ();
   middle middle (o[2:0], data[2:0], we[2:0], re[2:0]);

   always @(we[3] or data[3])
     if (we[3])
       others.b = data[3];

   assign o[3] = re[3] ? others.b        : o[3];
endmodule

module middle(o, data, we, re);
   output [2:0] o;
   input  [2:0] data;
   input  [2:0] we;
   input  [2:0] re;

   bottom bottom (data[0], we[0]);

   always @(we[2] or data[2])
     if (we[2])
       others.a = data[2]; // Parent

   always @(we[1] or data[1])
     if (we[1])
       bottom.others.b = data[1]; // Child

   assign o[2] = re[2] ? others.a        : o[2]; // Parent
   assign o[1] = re[1] ? bottom.others.b : o[1]; // Child
   assign o[0] = re[0] ? bottom.others.a : o[0]; // Child
endmodule

module bottom(data, we);
   input  data;
   input  we;

   always @(we or data)
     if (we)
       others.a = data;

   others others ();
endmodule

module others();
   reg a;
   reg b;
endmodule
