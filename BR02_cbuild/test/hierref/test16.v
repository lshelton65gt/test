// Bug from EMC design - hierref to a port aliased net
module top(o, i1, i2, i3);
   input i1, i2, i3;
   output o;
   wire   w;
   wire   x;
   child1 child1 (w);
   child2 child2 (x, i3);
   child3 child3 ();
   assign o = w & x;
endmodule

module child1(o);
   output o;
endmodule

module child2(o, i3);
   input i3;
   output o;
   assign o = top.w & i3;
endmodule

module child3();
   assign top.child1.o = top.i1 & top.i2;
endmodule
