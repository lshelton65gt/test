// Test simple child task reference.
module top(out1, out2, clk, data1, data2, addr1, addr2);
   output [1:0] out1, out2;
   input        clk;
   input [1:0]  data1, data2, addr1, addr2;

   mem M1 ();
   always @ (posedge clk)
     M1.writemem(data1, addr1);
   reg [1:0]    out1;
   always @ (addr1 or data1)
     M1.readmem(out1, addr1);
     
   mem M2 ();
   sub S1 (out2, clk, data2, addr2);
   
endmodule // top

module sub(out, clk, data, addr);
   output [1:0] out;
   input        clk;
   input [1:0]  data, addr;

   always @ (posedge clk)
     top.M2.writemem(data, addr);
   reg [1:0]    out;
   always @ (addr or data)
     top.M2.readmem(out, addr);

endmodule // sub

module mem();

   reg [1:0] mem [3:0];

   task writemem;
      input [1:0] data;
      input [1:0] waddr;
      mem[waddr] <= data;
   endtask // writemem

   task readmem;
      output [1:0] data;
      input [1:0] raddr;
      data = mem[raddr];
   endtask // readmem
   
endmodule
