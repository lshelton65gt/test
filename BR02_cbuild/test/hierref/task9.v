module top (out1, out2, in1, in2);
   output out1, out2;
   input  in1, in2;

   x X(out1, in1, in2);
   y Y(out2, in1, in2);

endmodule // top

module x(o, i1, i2);
   output o;
   input  i1, i2;

   a A(o, i1, i2);
   b_1 B();

endmodule // x

module y(o, i1, i2);
   output o;
   input  i1, i2;

   a A(o, i1, i2);
   b_2 B();

endmodule // y

module a(o, i1, i2);
   output o;
   input  i1, i2;

   reg    o;
   initial o = 0;
   always @ (i1 or i2)
     B.C.doand(o, i1, i2);

endmodule // a

module b_1;

   c C();

endmodule // b

module b_2;

   c C();

endmodule // d

module c;

   task doand;
      output y;
      input a;
      input b;

      y = a & b;

   endtask // doand

endmodule // c
