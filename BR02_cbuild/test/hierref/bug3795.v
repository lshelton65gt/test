module top(outb,ena,a,b,c);
   output outb;
   input  ena;
   input  a,b,c;

   // make two mutex enables to avoid drive conflicts.
   wire   enb = ~ena;
   
   wire   cross;
   wrap_a wa();
   wrap_b wb();
   
   level3 l3(outb,cross,enb,b);
endmodule

module level3(out,cross,en,in);
   output out;
   inout  cross;
   input  en,in;
   sub s0(out,cross,en,in);
endmodule

module sub(out,cross,en,in);
   output out;
   inout  cross;
   input  en,in;
   assign cross = en ? in : 1'bz;
   assign out = ~cross;
endmodule

module wrap_a();
   // This driver was incorrectly eliminated because read/write
   // computation was not taking hier-ref reads into accounts when
   // determining if a bidirect port connection had readers.
   assign top.cross = top.ena ? top.a : 1'bz;
endmodule

module wrap_b();
   assign l3.s0.cross = l3.s0.en ? l3.s0.in : 1'bz;
   assign l3.s0.out = ~l3.s0.cross;
endmodule
