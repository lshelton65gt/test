// This test case finds a bug with hier refs to state update memories.

module top (out1, out2, raddr1, raddr2, waddr1, waddr2, data1, data2, clk);
   output [3:0] out1, out2;
   input [3:0]  raddr1, raddr2, waddr1, waddr2, data1, data2;
   input        clk;

   // Allocate the memories
   sub S1();
   sub S2();

   reg [3:0]    out1, out2;
   initial begin
      out1 = 0;
      out2 = 0;
   end
   always @ (posedge clk)
     begin
        S1.mem[waddr1] <= data1;
        out2 <= S2.mem[raddr2];
     end

   always @ (negedge clk)
     begin
        S2.mem[waddr2] <= data2;
        out1 <= S1.mem[raddr1];
     end

endmodule // top

module sub;

   reg [3:0] mem [15:0];

`ifdef VERILOGSIM
   integer     i;
   initial begin
      for (i = 0; i < 16; i = i + 1)
        mem[i] = 0;
   end
`endif

endmodule // sub

      
