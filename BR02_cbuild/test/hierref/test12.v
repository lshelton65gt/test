// Test memory hierrefs
module top(bus, we, re, addr);
   inout [3:0] bus;
   input        we, re;
   input [1:0]  addr;

   reg [1:0]    mem [3:0];

   child child ();
endmodule

module child();
   assign top.bus = top.re ? top.mem[top.addr] : 'bz;
   always @(top.we or top.addr or top.bus)
     if (top.we & ~top.re)
       top.mem[top.addr] = top.bus;
endmodule
