// Hierrefs: test simple refs, multiple names of same ref.
module parent();
  wire local1;
  wire local2;
  wire local3;
  assign local1 = child1.thenet;
  assign local2 = child2.thenet;
  assign local3 = parent.child2.thenet;
  child child1();
  child child2();
endmodule

module child();
   wire thenet;
endmodule
