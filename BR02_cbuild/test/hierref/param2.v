// bug 2015 test case - test hierarchical reference to parameterized nets.
module top(clk1, clk2, o, d);
   input clk1, clk2;
   input [31:0]  d;
   output [31:0] o;
   reg [31:0]    o;

   child #(31) child ();
   
   always @(posedge clk1)
     child.q = d;
   always @(posedge clk2)
     o = child.q;
endmodule

module child();
   parameter HIGHBIT = 3;
   reg [HIGHBIT:0] q;
endmodule
