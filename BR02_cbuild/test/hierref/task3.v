// Test simple child task reference.
module top(out1, out2, in1, in2, clk);
   output [1:0] out1, out2;
   input [1:0]  in1, in2;
   input        clk;
   
   task doand;
      output [1:0] o;
      input [1:0] i1, i2;
      o = i1 & i2;
   endtask

   child child (out1, out2, clk, in1, in2);
   
endmodule

module child(o1, o2, clk, i1, i2);
   output [1:0] o1, o2;
   input        clk;
   input  [1:0] i1, i2;

   assign o2 = ~i1;
   
   reg [1:0] o1;
   always @(posedge clk)
     top.doand(o1, i1, i2);

endmodule
