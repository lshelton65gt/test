   module top (out1, out2, in1, in2);
     output out1, out2;
     input in1, in2;

     child C1(out1, in1);
     child C2(out2, in2);

   endmodule

   module child (y, a);
     output y;
     input a;

     inv I1(a);
     outInv I2(y);

   endmodule

   module inv(i);
     input i;
     reg r;
     always @ (i)
       r = ~i;
   endmodule

   module outInv(o);
     output o;

     assign o = I1.r;

   endmodule
