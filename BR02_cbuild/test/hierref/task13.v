// Hier function call in a hier function call list.

module top(out, in, in2, clk);
   output [3:0] out;
   input [3:0] in, in2;
   input clk;

   brother male_child( out, in, in2, clk);
   sister female_child();
endmodule

module sister();

   function [3:0] add;
   input a;
   input b;
   begin
     add = a + b;
   end
   endfunction

endmodule

module brother( out, in, in2, clk );
   output [3:0] out;
   input [3:0] in, in2;
   reg [3:0] out;
   input clk;

   always@(posedge clk)
     doit;

   task doit;
   begin
     out = female_child.add( female_child.add(in,in2), in);
   end
   endtask
endmodule
