module top (out, in);
   output out;
   input  in;

   assign out = c.r;
   child c(in);

endmodule

module child(d);
   input d;

   reg   r;
   always @ (d)
     r = ~d;

endmodule
