// last mod: Mon Apr 24 11:27:45 2006
// filename: test/hierref/inst_array_01.v
// Description:  This test checks that we can properly handle a hierref to
// elements of an instance array.


module inst_array_01(en,a,b,out1,out2, out3);
   input en,a,b;
   output [1:0] out1, out2, out3;
   
   storage storage(out1);

   enabler enabler_inst(en,a,out2);

   wire 	en_n = ~en;

   one_enabler one_enabler_inst[1:0](en_n,b,out2);

   assign out3[0] = enabler_inst.one_enabler_inst[0].io;
   assign out3[1] = one_enabler_inst[1].io;

endmodule

module storage(o);
   output [1:0] o;
   assign o = enabler_inst.one_enabler_inst[1].io;
endmodule

module enabler(en,data,o);
   input en,data;
   inout [1:0] o;

   one_enabler one_enabler_inst[1:0](en,data,o);
endmodule

module one_enabler(en,data,io);
   input en,data;
   inout io;
   assign io = en ? data : 1'bz;
endmodule
