// Test hierrefs to pins
module top(clk, rst, i, en, o1, o2, io);
   input clk, rst, i, en;
   output o1, o2;
   reg    o1;
   inout  io;
   child child ();
endmodule

module child();
   always @(posedge top.clk or posedge top.rst)
     if (top.rst)
       top.o1 = 0;
     else
       top.o1 = top.i;
   assign top.io = top.en ? top.o1 : 'bz;
   assign top.o2 = top.io;
endmodule
