module top (out, clk, in);
   output [1:0] out;
   input        clk;
   input [1:0]  in;

   child c (out, clk);

endmodule

module child(q, clk);
   output [1:0] q;
   input  clk;

   reg    [1:0] q;
   always @ (posedge clk)
     q <= top.in;

endmodule
