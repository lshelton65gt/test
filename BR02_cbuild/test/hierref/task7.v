// Test simple child task reference.
module top(out, in1, in2, clk);
   output [3:0] out;
   input [1:0]  in1, in2;
   input        clk;
   
   reg [3:0]    tmp;
   initial tmp = 0;
   always @(posedge clk)
     child.doand(tmp, in1, in2);
   assign       out[1:0] = tmp[1:0];

   child child (out[3:2], in1);
   
endmodule

module child(o1, i);
   output [1:0] o1;
   input  [1:0] i;

   assign o1 = ~i;
   
   task doand;
      output [3:0] o;
      input [1:0] i1, i2;
      T1.doand(o[1:0], i1, i2);
   endtask

   tasks T1();

endmodule // child

module tasks;
   task doand;
      output [1:0] o;
      input [1:0] i1, i2;
      o = i1 & i2;
   endtask // doand
   
endmodule // tasks

   
