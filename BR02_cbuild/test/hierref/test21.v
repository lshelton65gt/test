// bug 2290 - hierrefs inside functions which are invoked from tasks

module top(clk, i1, i2, o);
   input clk;
   input i1, i2;
   output o;

   child1 child1 (i1, i2);
   child2 child2 (o, clk);
endmodule

module child1(i1, i2);
   input i1, i2;
   reg r;

   always @(i1 or i2)
     r = i1 & i2;
endmodule

module child2(o, clk);
   input clk;
   output o;
   reg    o;

   function f;
      input i;
      begin
         f = g(i);
      end
   endfunction
   
   function g;
      input i;
      begin
         g = top.child1.r;
      end
   endfunction
   
   task t;
      output o;
      begin
         o = f(1'b0);
      end
   endtask

   always @(posedge clk)
     begin
        t(o);
     end
endmodule
