module top (out, in);
   output out;
   input  in;

   sub1 S1 (out, in);

endmodule // top

module sub1 (y, a);
   output y;
   input  a;

   sub2 S1 (y);

endmodule // sub1

module sub2 (z);
   output z;
   assign z = ~sub1.a;

endmodule // sub2

