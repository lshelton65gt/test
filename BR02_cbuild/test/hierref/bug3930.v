module top(en,a,b,out1,out2);
   input en,a,b;
   output [1:0] out1;
   output [1:0] out2;
   
   storage storage(out1);

   enabler enabler(en,a,out2);

   wire 	en_n = ~en;
   one_enabler en_n0(en_n,b,out2[0]);
   one_enabler en_n1(en_n,b,out2[1]);
endmodule

module storage(o);
   output [1:0] o;
   assign o = enabler.o;
endmodule

module enabler(en,data,o);
   input en,data;
   inout [1:0] o;

   one_enabler en0(en,data,o[0]);
   one_enabler en1(en,data,o[1]);
endmodule

module one_enabler(en,data,io);
   input en,data;
   inout io;
   assign io = en ? data : 1'bz;
endmodule
