// Hierrefs: test different resolutions
module top();
   middle1 m1();
   middle2 m2();
endmodule

module middle1();
   parent p();
   child1 c();
endmodule

module middle2();
   parent p();
   child2 c();
endmodule

module parent();
  wire local1;
  assign local1 = c.thenet;
endmodule

module child1();
  wire thenet;
endmodule

module child2();
  wire thenet;
endmodule
