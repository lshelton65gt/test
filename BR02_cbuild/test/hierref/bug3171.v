module m (ck, a, d, q);
   input ck, d;
   input [7:0] a;

   output      q;

   mcore #(4) m (ck, a, d, q);

   wire 	       foo; // carbon observeSignal
   assign      foo = m.mem[a];   
   
   task smack;
      input data;
      input [7:0] address;
      
      begin
	 m.smack(data, address);	 
      end
   endtask // smack      
endmodule // m

module mcore (ck, a, d, q);
   parameter foo = 1;
   
   input ck, d;
   input [7:0] a;

   output      q;
   wire        q;
   reg 	       mem[255:0];
   assign      q = mem[a];
      
   always @(posedge ck) begin
      mem[a] <= d;      
   end

   task smack;
      input data;
      input [7:0] address;      
      begin
	 mem[address] = data;
      end
   endtask // smack
endmodule // m
