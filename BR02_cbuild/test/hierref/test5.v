// Hierrefs: test simple hierarchical task enables
module parent(clk, rd_en, wr_en, addr, data_in, data_out);
   input clk;
   input rd_en;
   input wr_en;
   input [4:0] addr;
   input [31:0] data_in;
   output [31:0] data_out;
   reg [31:0] data_out;

   child child1();

   always @(posedge clk)
     begin
	if (rd_en)
	  child1.do_read(addr, data_out);
	if (wr_en)
	  parent.child1.do_write(addr, data_in);
     end

endmodule


module child();
   reg [7:0] mem [31:0];

   integer    i;

   initial begin
     for (i = 0; i < 32; i = i + 1)
       mem[i] = 8'b0;
   end

   task do_write;
      input [4:0] address;
      input [31:0] data;
      mem[address] = data;
   endtask
   task do_read;
      input [4:0] address;
      output [31:0] data;
      data = mem[address];
   endtask

endmodule
