module top (out, in);
   output [1:0] out;
   input  [1:0] in;

   assign out = c.r;
   child c(in);

endmodule

module child(d);
   input [1:0] d;

   reg   [1:0] r;
   always @ (d)
     r = ~d;

endmodule
