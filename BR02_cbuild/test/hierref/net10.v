module top (out, in1, in2);
   output out;
   input  in1, in2;

   sub1 a_b(in1);
   sub2 a(in2);
   sub3 do(out);

endmodule // top

module sub1(c);
   input c;

endmodule // sub1

module sub2(b_c);
   input b_c;

endmodule // sub2

module sub3(o);
   output o;

   assign o = a_b.c & a.b_c;

endmodule // sub3
