// Hier function call in a for loop

module top(out, in, clk);
   output [3:0] out;
   input [3:0] in;
   input clk;

   brother male_child( out, in, clk);
   sister female_child();
endmodule

module sister();

   function [4:0] add;
   input [3:0]a;
   input [3:0]b;
   begin
     add = a + b;
   end
   endfunction

endmodule

module brother( out, in, clk );
   output [3:0] out;
   input [3:0] in;
   reg [3:0] out;
   input clk;
   integer i;
   initial
     out = 10;

   always@(posedge clk) begin
//      $display ("i: x, out: %d, in: %d, add: %d", out, in, female_child.add(out,in));
     for( i=0; i<female_child.add(out,in); i=i+1 ) begin
//      $display ("i: %d, out: %d, in: %d, add: %d", i,out, in, female_child.add(out,in));
       out = out + i;
     end
   end

endmodule
