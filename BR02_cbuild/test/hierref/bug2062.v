// bug2062 - hierarchical references occuring within named blocks.
module top(i1, i2, i3, clk, o);
   input i1, i2, i3, clk;
   output o;
   child child ();
endmodule

module child();
   reg q;
   always @(posedge top.clk)
     begin:named
        reg tmp;
        tmp = top.i1 & top.i2;
        q = tmp & top.i3;
     end
   assign   top.o = top.child.q;
endmodule
