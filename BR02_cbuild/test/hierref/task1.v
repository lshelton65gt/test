// Test simple child task reference.
module top(out1, out2, in1, in2, clk);
   output [1:0] out1, out2;
   input [1:0]  in1, in2;
   input        clk;
   reg [1:0]    out1;
   
   always @(posedge clk)
     child.doand(out1, in1, in2);

   child child (out2, in1);
   
endmodule

module child(o1, i);
   output [1:0] o1;
   input  [1:0] i;

   assign o1 = ~i;
   
   task doand;
      output [1:0] o;
      input [1:0] i1, i2;
      o = i1 & i2;
   endtask
endmodule
