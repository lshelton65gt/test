// This testcase had aliasing problems when flattening where one alias
// of top.cross would end up with a NULL storage ptr. This caused an
// incorrect rewrite when resolving flattened hierarchical references.
module top(outb,outc,en,a,b,c);
   output outb,outc;
   input [1:0] en; // single enable; three consumers are kept mutex to avoid drive conflicts.
   input  a,b,c;
   
   wire   cross;
   pullup(cross); // avoid 'x' on outputs.
   wrap_a wa();
   
   sub s0(outb,cross,en,2'b01,b);
   sub s1(outc,cross,en,2'b10,c);
endmodule

module sub(out,cross,en,val,in);
   output out;
   inout  cross;
   input [1:0] en,val;
   input in;
   wrap_b wb();
endmodule

module wrap_a();
   assign top.cross = (top.en==2'b11) ? top.a : 1'bz;
endmodule

module wrap_b();
   assign sub.cross = (sub.en==sub.val) ? sub.in : 1'bz;
   assign sub.out = ~sub.cross;
endmodule
