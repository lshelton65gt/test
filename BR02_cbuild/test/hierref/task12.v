// Relative path hierarchical reference with a parameterized module.

// This was a TFAIL test in interra flow with this explination:
// because we do not yet handle this case.
// The problem is that for relative hierarchical task references, we
// only elaborate the relative modules (not the whole path).
// Therefore, when a higher module overrides the parameter local
// to the task, we do not find the parameterized module name.
//
// See src/localflow/PopulateNet.cxx::findObjectResolution()

// This works fine in verific flow

module top(out, in, clk);
   output [3:0] out;
   input [3:0] in;
   input clk;

   dad #(3) pappy(out, in, clk);
endmodule

module dad(out, in, clk);
   output [3:0] out;
   input [3:0] in;
   input clk;
   parameter INCR_OVERRIDE = 1;

   brother male_child( clk );
   sister #(INCR_OVERRIDE) female_child( out, in );
endmodule

module brother( clk );
   input clk;

   always@(posedge clk)
     female_child.add;
endmodule

module sister( out, in );
   output [3:0] out;
   input [3:0] in;
   reg [3:0] out;

   initial
      begin
	 out = 0;
      end
   parameter INCR = 1;

   task add;
   begin
     out = out + in + INCR;
   end
   endtask
endmodule
