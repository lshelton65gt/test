module top (out1, out2, clk, clk_in1, clk_in2, clksel, in1, in2);
   output out1, out2, clk;
   input  clk_in1, clk_in2, clksel, in1, in2;

   clksub CLKSUB (clk, clk_in1, clk_in2, clksel);

   flop F1 (out1, in1);
   flop F2 (out2, in2);

endmodule // top

module clksub (clk, ci1, ci2, csel);
   output clk;
   input  ci1, ci2, csel;

   assign clk = csel ? ci1 : 1'bz;
   assign clk = ~csel ? ci2 : 1'bz;

endmodule // clksub

module flop (q, d);
   output q;
   input  d;

   wire   clk;
   assign clk = top.clk;

   reg    q;
   always @ (posedge clk)
     q <= d;

endmodule // flop
