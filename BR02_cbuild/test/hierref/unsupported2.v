module top(input clk, output out1, out2);
   middle middle(clk, out2);
   child child(clk, out1);
   topdata data();
endmodule

module middle(input clk, output o);
   child child(clk, o);
   middata data();
endmodule

module child(input clk, output o);
   reg r;
   initial
     r = 0;

   assign o = r;
   always @(posedge clk)
     begin
	data.t(r);
     end
endmodule

module topdata;
   task t;
      output o;
      o = 1'b0;
   endtask
endmodule

module middata;
   task t;
      inout o;
      o = ~o;
   endtask
endmodule
