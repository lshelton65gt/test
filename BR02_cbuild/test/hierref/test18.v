module top(i1, i2, o);
   input i1, i2;
   output o;
   sub S (i1);
   driver driver (top.S.r, top.middle.w);
   middle middle (o, i2);
endmodule

module sub (i);
   input i;
   wire  r;
   assign r = i;
endmodule // sub

module driver(i1, o);
   input i1;
   output o;
   assign o = i1;
endmodule

module middle(o, i);
   output o;
   input  i;
   wire w;
   bottom bottom (w, i, o);
endmodule

module bottom(i1, i2, o);
   input i1;
   input i2;
   output o;
   assign o = i1 & i2;
endmodule
