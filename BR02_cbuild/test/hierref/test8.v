module top(i1, i2, o1, i3, i4, o2, o3, clk, rst, idead);
   input i1, i2;
   output o1;
   input i3, i4;
   output o2;
   output o3;
   input  clk, rst;
   input  idead;

   reg [7:0]  o1_reg;

   function [7:0] compute;
      input i1;
      input i2;
      input [7:0] prev_val;
      begin
	 compute = prev_val;
	 compute[3] = i1 & i2;
      end
   endfunction

   task initialize;
      output [7:0] out;
      begin
	 out = 8'd0;
      end
   endtask

   initial
     begin : named1
	reg [7:0] tmp;
	tmp = 8'b11111111;
	o1_reg = tmp;
     end

   always @(posedge clk or posedge rst)
     begin
	if (rst)
	  begin
	     initialize(o1_reg);
	  end
	else
	  o1_reg = compute(i1, i2, o1_reg);
     end

   assign o1 = o1_reg;

   always @(posedge idead);

   child child (i3, i4, o2, o3);
   
endmodule


module child(i1, i2, o, io);
   input i1, i2;
   output o;
   inout io;
   and and1 (o, i1, i2);
   or or1 (io, o, i2);
endmodule
