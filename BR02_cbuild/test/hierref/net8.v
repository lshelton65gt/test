module top (out, clk, in);
   output [127:0] out;
   input        clk;
   input [127:0]  in;

   child c (out, clk);

endmodule

module child(q, clk);
   output [127:0] q;
   input  clk;

   reg    [127:0] q;
   always @ (posedge clk)
     q <= top.in;

endmodule
