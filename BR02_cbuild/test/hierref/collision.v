// testcase with two hierrefs needing pointers at the same point in the hierarchy.
module top(a,b,out1,out2);
   input a,b;
   output out1,out2;
   sub s1(a);
   sub s2(b);
   write w(out1,out2);
endmodule

module sub(in);
   input in;
endmodule

module write(out1,out2);
   output out1,out2;
   assign out1 = s1.in;
   assign out2 = s2.in;
endmodule
