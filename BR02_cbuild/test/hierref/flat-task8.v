// Test sibling task reference.
module top(out1, out2, out3, out4, in1, in2, clk);
   output [1:0] out1, out2, out3, out4;
   input [1:0]  in1, in2;
   input        clk;

   left  L (out1, out2, in1, in2, clk);
   right R (out3, out4, in1, in2, clk);
   
endmodule

module left(out1, out2, in1, in2, clk);
   output [1:0] out1, out2;
   input [1:0]  in1, in2;
   input        clk;
   
   a A (out1, in1, in2, clk);
   b X (out2, in1);
   
endmodule

module right(out1, out2, in1, in2, clk);
   output [1:0] out1, out2;
   input [1:0]  in1, in2;
   input        clk;
   
   a A (out1, in1, in2, clk);
   c X (out2, in1);
   
endmodule

module b(out,in);
   output [1:0] out;
   input [1:0]  in;
   
   d Y (out, in);
   
endmodule

module c(out,in);
   output [1:0] out;
   input [1:0]  in;
   
   d Y (out, in);
endmodule

module a(out1,in1,in2,clk);
   output [1:0] out1;
   input [1:0]  in1, in2;
   input        clk;
   reg [1:0] 	out1;
   
   always @(posedge clk)
     X.Y.doand(out1, in1, in2);

endmodule

module d(out,in);
   output [1:0] out;
   input [1:0]  in;

   assign out = ~in;
   
   task doand;
      output [1:0] o;
      input [1:0] i1, i2;
      o = i1 & i2;
   endtask

endmodule
