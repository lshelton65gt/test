module top (out, clk, in);
   output out;
   input  clk, in;

   child c (out, clk);

endmodule

module child(q, clk);
   output q;
   input  clk;

   reg    q;
   always @ (posedge clk)
     q <= top.in;

endmodule
