// Test simple child task reference.
module top(out1, out2, in1, in2, clk);
   output [1:0] out1, out2;
   input [1:0]  in1, in2;
   input        clk;
   
   a a (out1, in1, in2, clk);
   b b (out2, in1);
   
endmodule

module a(out1,in1,in2,clk);
   output [1:0] out1;
   input [1:0]  in1, in2;
   input        clk;
   reg [1:0] 	out1;
   
   always @(posedge clk)
     b.doand(out1, in1, in2);

endmodule

module b(out,in);
   output [1:0] out;
   input [1:0]  in;

   assign out = ~in;
   
   task doand;
      output [1:0] o;
      input [1:0] i1, i2;
      o = i1 & i2;
   endtask

endmodule
