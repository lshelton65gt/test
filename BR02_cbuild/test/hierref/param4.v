// bug 2015 test case - test hierarchical reference to multiply-instanced module.
module top(clk1, clk2, o1, o2, d1, d2);
   input clk1, clk2;
   input [31:0]  d1;
   input [31:0]  d2;
   output [31:0] o1;
   reg [31:0]    o1;
   output [31:0] o2;
   reg [31:0]    o2;

   child #(31) child1 ();
   child #(31) child2 ();
   
   always @(posedge clk1)
     child1.q = d1;
   always @(posedge clk2)
     o1 = child1.q;
   always @(posedge clk1)
     child2.q = d2;
   always @(posedge clk2)
     o2 = child2.q;
endmodule

module child();
   parameter HIGHBIT = 3;
   reg [HIGHBIT:0] q;
endmodule
