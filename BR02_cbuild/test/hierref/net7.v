module top (out, in);
   output [127:0] out;
   input  [127:0] in;

   assign out = c.r;
   child c(in);

endmodule

module child(d);
   input [127:0] d;

   reg   [127:0] r;
   always @ (d)
     r = ~d;

endmodule
