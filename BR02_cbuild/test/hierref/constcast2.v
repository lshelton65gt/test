module top (out, in);
   output out;
   input  in;

   wire   t1, t2;
   sub1 S1 (t1, in);
   sub3 S3 (t2, in);
   assign out = t1 ^ t2;
   
endmodule // top

module sub1 (y, a);
   output y;
   input  a;
   reg    y;
   initial y = 0;

   sub2 S2 (out);

endmodule // sub1

module sub2 (z);
   output z;
   assign z = ~S1.a;

endmodule // sub2

module sub3(y, i);
   output y;
   input  i;

   sub1a S1 (y, i);

endmodule // sub3

module sub1a(o, i);
   output o;
   input  i;

   wire   a = ~i;
   sub2 S2 (o);

endmodule // sub4
