module top(i1, i2, o);
   output o;
   reg    o;
   input  i1, i2;
   always @(i1 or i2)
     begin
        subroutines.t1(o);
     end
   subroutines subroutines();
endmodule

module subroutines;
   task t1;
      output o;
      o = top.i1 & top.i2;
   endtask
endmodule
