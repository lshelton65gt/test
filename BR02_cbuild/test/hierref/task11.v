// Relative path hierarchical reference with a parameterized module.


module top(out, in, clk);
   output [3:0] out;
   input [3:0] in;
   input clk;

   brother male_child( clk );
   sister #(5) female_child( out, in );
endmodule

module brother( clk );
   input clk;

   always@(posedge clk)
     female_child.add;
endmodule

module sister( out, in );
   output [3:0] out;
   input [3:0] in;
   reg [3:0] out;

   parameter INCR = 1;

   task add;
   begin
     out = out + in + INCR;
   end
   endtask

endmodule
