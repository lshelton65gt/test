// Test hierarchical references to a net which is driven
// as an output.  This showed up with mindspeed.
module top(i1, i2, i3, o1, o2);
   input i1, i2, i3;
   output o1, o2;

   child1 child1 (i1, i2, o1);
   child2 child2 (i3, o2);

endmodule

module child1(i1, i2, o);
   input i1, i2;
   output o;
   assign o = i1 & i2;
endmodule

module child2(i1, o);
   input i1;
   output o;
   assign o = top.o1 & i1;
endmodule
