// Test reading and writing to a hier ref.
module top(i, o, clk, re, we);
   input clk, re, we;
   input [7:0] i;
   output [7:0] o;
   child_reader cr (o, clk, re);
   child_writer cw (i, clk, we);
   child_data cd ();
endmodule

module child_reader(o, clk, re);
   output [7:0] o;
   input  clk, re;
   reg 	  [7:0] o;
   initial o = 0;
   always @(posedge clk)
     if (re)
       o <= cd.datum;
endmodule

module child_writer(i, clk, we);
   input [7:0] i;
   input  clk, we;
   always @(posedge clk)
     if (we)
       cd.datum <= i;
endmodule

module child_data();
   reg [7:0] datum;
   initial datum = 0;
endmodule
