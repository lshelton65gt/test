#!/usr/bin/perl
#
#
# determine if DDRx simulation output has parity wrt: reads/writes, RBC / BRC value, col, bank and row bit values.
# the result is 0 if there is parity (same) and 1 if not (different).

## this tool should be extended to do the following:
#   return 1 if the input file is empty (to indicate an error)
#   if there is an error then:
#         print an error message like "ERROR: read/write inconsistency"
#         report the line number and print the line where it failed so that the user can figure out where the failure was (which read/write)


if ($#ARGV != 0 ) {
  print "ERROR: wrong usage: $0 filename \n";
  exit;
}

$infilename=$ARGV[0];


# compare 2 arrays and report if they are different
sub compare_arrays  
{
  my ($first, $second) = @_;

  $length1 = @$first;
  $length2 = @$second;




  if ($length1 == 0)
  {
      print "ERROR: No Read Address encountered.\n";
      return 0;
  }

  if ($length2 == 0)
  {
      print "ERROR: No Write Address encountered.\n";
      return 0;
  }

  if ($length1 != $length2)
  {
      print "ERROR: Mismatch in $length1 reads and $length2 writes.\n";
  }

  for (my $i = 0; $i < @$first; $i++) 
  {
    if ($first->[$i] ne $second->[$i])
    {
      print "ERROR: Mismatch in line containing:  $first->[$i]";
      return 0;
    }
  }
  return 1;
}  


sub readTheFile () {
  $readString = "Read Address  ";
  $writeString = "Write Address ";
  $Failed = "Failed to open";
  $AlertMsg = "Alert";
  $Results = "STATUS";
  
  open FILE1, "<$infilename" or die "Cannot open $infilename!";
  
  # check for zero size file
  $filesize = -s $infilename;
  if ($filesize == 0)
  {
    print ("ERROR: File $infilename has zero size\n");
    exit(1);
  }
  
  my $line;
  while ($line = readline(FILE1)) 
  {
    if ($line =~ m/$Results/)
    {
        print "$line";
    }

    if ($line =~ m/$Failed/)
    {
        print "ERROR - $line";
        exit(1);
    }

    if ($line =~ m/$AlertMsg/)
    {
        print "ERROR - $line";
        exit(1);
    }

    if ($line =~ m/$readString/) 
    {
      push(@xx, "$'");
    }
    if ($line =~ m/$writeString/)
    {
      push(@x, "$'");                            
    }
}
close FILE1;
}


readTheFile ;

$result = compare_arrays \@xx,\@x ;

# files are the same
if ($result == 1) 
{

    exit(0);
}
else 
{
    print "ERROR: read/write inconsistency\n";
    exit(1);

}
