if you want to modify the test driver (like to make the data bus wider) here
are the steps you should take:

<you should do this with version SoCD 7.6 so that running variant tests will be
 compatible with the libraries that were available in 7.6 (The version used
 for testing when this test was first created).>



runsingle -setuponly test/DDRx
cd $CARBON_HOME/obj/Linux.product/test/DDRx/tools/test_driver/
edit ddrx_test_master.v
modelstudio Project_ddrx_test_master/Project_ddrx_test_master.carbon 
and click on recompile

you MUST then copy the generated .so files back to your sandbox
cp -i ./Project_ddrx_test_master/Linux/Default/SoCDesigner/libddrx_test_master.mx*.so $CARBON_HOME/test/DDRx/tools/test_driver/Project_ddrx_test_master/Linux/Default/SoCDesigner/


or do the same for lpddr2_test_master.v and project Project_lpddrx_test_master/Project_lpddrx_test_master.carbon 
