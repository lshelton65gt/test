// use this code to add SoCD parameters to the ccfg that are tied to internal registers,
// to use this file, just open it in modelstuido, then run the script,  you should only
// need to run this script if you have deleted the SoCD component and need to recreate it.
// only run it once since this script is not smart enough to keep from adding multiple copies
// of the parameters

var ccfg = Project.SoCDesigner.ccfg;

var hierPathToMemory = "lpddr2_test_master.";


addCurrentBitParameter(ccfg, hierPathToMemory + "carbon_current_ROW_BITS", "Row", "16", "|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|");
addCurrentBitParameter(ccfg, hierPathToMemory + "carbon_current_BA_BITS", "Bank", "3",  "|1|2|3|");
addCurrentBitParameter(ccfg, hierPathToMemory + "carbon_current_COL_BITS", "Column", "15", "|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|");
addCurrentBitParameter(ccfg, hierPathToMemory + "carbon_current_DATA_BITS", "Data", "8");

ccfg.write();


// adds a parameter for the ROW BANK or COLUMN internal configuration register  
function addCurrentBitParameter(topComponent, hierPathToRegister, typeName, defaultValue, enumChoices){
  var code;
  var paramName = typeName +"_Bits";

  topComponent.addParam(paramName, CcfgEnum.XtorString,  defaultValue, CcfgEnum.Init, "specify the number of bits in for the " + typeName + " segment of the address", defaultValue);
  // define necessary custom code for this parameter, (ties parameter value to internal register)
  code = "";
  code += "public:\n";
  code += "  CarbonNetID* m"+ paramName + ";\n";
  code += "private:\n";
  addCustomCode(topComponent, code, CcfgEnum.CompClass, CcfgEnum.Pre);

  code = "";
  code += "  m"+ paramName + " = NULL;\n";
  addCustomCode(topComponent, code, CcfgEnum.CompConstructor, CcfgEnum.Post);

  code = "";
  code += "  m"+ paramName + " = carbonFindNet(mCarbonObj, \"" + hierPathToRegister + "\");\n";
  code += "  if ( m"+ paramName + " ) {\n";
  code += "    CarbonUInt32 localValue;\n";
  code += "    eslapi::CASIConvertErrorCodes status = CASIConvertStringToValue((getTopComp()->getParameter(\"" + paramName + "\")),&localValue);\n";
  code += "    if (status == eslapi::CASIConvert_SUCCESS) {\n";
  code += "      carbonDeposit(mCarbonObj, m" + paramName + ", &localValue, NULL);\n";
  code += "    }\n";
  code += "  }\n";
  addCustomCode(topComponent, code, CcfgEnum.CompInit, CcfgEnum.Post);
}

function addCustomCode(component, code, section, position, genType)
{
  var customCodeObj = null;

  // See if this already exists
  for (var i = 0; i < component.getNumCustomCodes(); ++i) {
    var c = component.getCustomCode(i);
    if ((c.getSection() == section) && (c.getPosition() == position)) {
      customCodeObj = c;
      break;
    }
  }

  if (customCodeObj == null) {
    // Create a new one
    customCodeObj = component.addCustomCode(genType);
    customCodeObj.setSection(section);
    customCodeObj.setPosition(position);
    customCodeObj.setCode(code);
  } else {
    // Append the new code to what's already there
    var curCode = customCodeObj.getCode();
    customCodeObj.setCode(curCode + code);
  }
}
