`define DATASTORAGE_DEPTH 256
`define DATASTORAGE_WIDTH 512

// this test_driver is compatible with LPDDR2 and LPDDR3,
// when used with LPDDR2 the sdr_preamble signal can be ignored 
module lpddr2_test_master(/*AUTOARG*/
   // Outputs
   sdr_ca_ne, sdr_ca_pe, sdr_cke, sdr_cs_n, sdr_dm1_in, sdr_dm2_in,
   sdr_dq1_in, sdr_dq2_in, sdr_dq_valid_in,
   sdr_preamble, // only used in lpddr3
   // Inputs
   ck_t, sdr_dq1_out, sdr_dq2_out, sdr_dq_valid_out
   );

   // number of chip selects (ranks) and data width
   parameter CS_BITS = 4;  // determines number of cs_n and cke pins

   // max Data bits limited to this value:
   parameter DATA_BITS = 256;  // determines data bit width and number of DM and DQS pins (1 per byte)

   
   // calculated parameter
   parameter DM_BITS = ((DATA_BITS % 8) == 0 ) ? (DATA_BITS / 8) : (DATA_BITS / 8 ) + 1; 
`define PROGRAM_SIZE (((1024*1024)*2)-1)

   input                ck_t;

   // single data rate interface

   // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
   output reg 		      sdr_preamble;
   
   output reg [9:0] 	      sdr_ca_ne;
   output reg [9:0] 	      sdr_ca_pe;
   output reg [CS_BITS-1:0]   sdr_cke;
   output reg [CS_BITS-1:0]   sdr_cs_n;
   output reg [DM_BITS-1:0]   sdr_dm1_in;
   output reg [DM_BITS-1:0]   sdr_dm2_in;
   output reg [DATA_BITS-1:0] sdr_dq1_in;
   output reg [DATA_BITS-1:0] sdr_dq2_in;
   output reg 		      sdr_dq_valid_in;
   
   input [DATA_BITS-1:0]      sdr_dq1_out;
   input [DATA_BITS-1:0]      sdr_dq2_out;
   input 		      sdr_dq_valid_out;


   // actual bit widths used for device addressing - see JEDEC standard for specific device configurations
   reg [31:0] 		    carbon_current_BA_BITS;    // carbon depositSignal
                                                       // carbon observeSignal
   reg [31:0] 		    carbon_current_ROW_BITS;   // carbon depositSignal
                                                       // carbon observeSignal
   reg [31:0] 		    carbon_current_COL_BITS;   // carbon depositSignal
                                                       // carbon observeSignal
   wire [31:0] 		    carbon_current_BA_BITS_mask = (1 << carbon_current_BA_BITS)-1;
   wire [31:0] 		    carbon_current_ROW_BITS_mask = ( 1 << carbon_current_ROW_BITS)-1;
   wire [31:0] 		    carbon_current_COL_BITS_mask = ( 1 << carbon_current_COL_BITS)-1;

   // carbon_current_DATA_BITS is only used for the display of data
   reg [31:0] 		    carbon_current_DATA_BITS;    // carbon depositSignal
                                                       // carbon observeSignal
   wire [DATA_BITS-1:0]     carbon_current_DATA_BITS_mask = ( 1 << carbon_current_DATA_BITS)-1;

   // these are some variables to control the display of data/addresses
   reg 			    done_reported;
   reg 			    first_clock;
   reg 			    first_read;
   reg [31:0] 		    display32_a, display32_b ;
   reg [63:0] 		    display64_a, display64_b ;
   reg [127:0] 		    display128_a, display128_b ;
   reg [255:0] 		    display256_a, display256_b ;
   

   // ********************************************************************************
   // ********************************************************************************
   reg [33:0] 	    RBC_address; // the current RBC format address
   reg [33:0] 	    BRC_address; // the current BRC format address

   reg [15:0] 	    saved_row_addr [7:0]; 
   
   reg [31:0] 		      opcodes[0:`PROGRAM_SIZE]; // carbon observeSignal
			                                // carbon depositSignal
   reg [7:0] 		      data_byte;
   // data_storage: patterns of data that will be written to memory
   reg [`DATASTORAGE_WIDTH-1:0] data_storage[0:`DATASTORAGE_DEPTH-1];      // carbon observeSignal
			                                // carbon depositSignal
   // dm_storage: data masks (for byte access)
   reg [15:0] 		      dm_storage[0:15];         // carbon observeSignal
			                                // carbon depositSignal
	      
   reg [31:0] 		      pc;                       // carbon observeSignal
			                                // carbon depositSignal
   reg [31:0] 		      current_op;               // carbon observeSignal
			                                // carbon depositSignal
   reg [31:0] 		      next_op;                  // carbon observeSignal
			                                // carbon depositSignal
   reg [15:0] 		      repeat_count;             // carbon observeSignal
			                                // carbon depositSignal
   reg 			      test_done;                // carbon observeSignal
   
   
   // opcode fields
   wire [4:0] 		      opcode = current_op[31:27];                     // carbon observeSignal
   wire [2:0] 		      opcode_cs = current_op[26:24];                  // carbon observeSignal
   wire  		      opcode_precharge = current_op[19];              // carbon observeSignal
   wire [2:0] 		      opcode_ba = current_op[18:16];                  // carbon observeSignal
   wire [7:0] 		      opcode_ma = current_op[7:0];                    // carbon observeSignal
   wire [7:0] 		      opcode_op = current_op[15:8];                   // carbon observeSignal

   wire [15:0] 		      opcode_row = current_op[15:0];                  // carbon observeSignal
   wire [14:0] 		      opcode_col = current_op[14:0];                  // carbon observeSignal

   wire [15:0] 		      opcode_dq1_idx = current_op[7:0];               // carbon observeSignal 
   wire [15:0] 		      opcode_dq2_idx = current_op[15:8];              // carbon observeSignal
   wire [15:0] 		      opcode_dm1_idx = current_op[19:16];             // carbon observeSignal
   wire [15:0] 		      opcode_dm2_idx = current_op[23:20];             // carbon observeSignal
   
   wire [DATA_BITS-1:0]       current_dq1 = data_storage[opcode_dq1_idx];     // carbon observeSignal
   wire [DATA_BITS-1:0]       current_dq2 = data_storage[opcode_dq2_idx];     // carbon observeSignal
   wire [DM_BITS-1:0] 	      current_dm1 = dm_storage[opcode_dm1_idx];       // carbon observeSignal
   wire [DM_BITS-1:0] 	      current_dm2 = dm_storage[opcode_dm2_idx];       // carbon observeSignal
      
   
   parameter 
     OP_Idle = 0,  // repeat count field
       OP_PrechargeBank = 1,
       OP_PrechargeAll = 2,
       OP_WriteModeReg = 3,
       OP_Activate = 4,
       OP_ReadCmd = 5,
       OP_WriteCmd = 6,
       OP_WriteData = 7,
       OP_BstCmd = 8,
       OP_BstCmdWriteData = 9,
       OP_ReadModeReg = 10,
       OP_LPDDR3WritePreamble = 16, // overloaded with DDR3 Write preamble
       OP_Finish = 31;

   integer 		      i;
   // put default values in data and dm blocks
   initial
     begin
	for (i = 0; i < `DATASTORAGE_DEPTH; i = i + 1)
	  begin
	     data_byte = (i & 8'hff);
	     data_storage[i] = {`DATASTORAGE_WIDTH/8{data_byte}};
	  end

	dm_storage[0] = 16'h0000;
	dm_storage[15] = 16'hffff;

	$readmemh("test_sequence.memh", opcodes);
     end
   

   always @(posedge ck_t)
     begin
	current_op <= opcodes[pc];
	next_op <= opcodes[pc+1];
	
	pc <= pc + 1; // pc actually points to NEXT pc
     end

   always @(*) 
     begin
	case (opcode)
	  OP_Idle: begin
	     driveIdle;
	  end
	  OP_PrechargeBank: begin
	     drivePrechargeBank;
	  end
	  OP_PrechargeAll: begin
	     drivePrechargeAll;
	  end
	  OP_WriteModeReg: begin
	     driveWriteModeReg;
	  end
	  OP_ReadModeReg: begin
	     driveReadModeReg;
	  end
	  OP_Activate: begin
	     driveActivate;
	  end
	  OP_ReadCmd: begin
	     driveReadCmd;
	  end
	  OP_WriteCmd: begin
	     driveWriteCmd;
	  end
	  OP_WriteData: begin
	     driveWriteData;
	  end
	  OP_BstCmd: begin
	     driveBstCmd;
	  end
	  OP_BstCmdWriteData: begin
	     driveBstCmdWriteData;
	  end
	  OP_LPDDR3WritePreamble: begin
	     driveLPDDR3WritePreamble;
	  end
	  OP_Finish: begin
	     driveFinish;
	     test_done <= 1'b1;
	  end
	  default: begin
	     driveIdle;
	  end
	endcase
     end
   

   // ********************************************************************************
   task driveIdle;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= {CS_BITS{1'b1}};
	 sdr_ca_pe <= 10'h3ff;
	 sdr_ca_ne <= 10'h3ff;
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task drivePrechargeBank;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= (1 << opcode_cs)^{CS_BITS{1'b1}};
	 sdr_ca_pe <= { opcode_ba[2:0], 6'b001011 };
	 sdr_ca_ne <= 10'h0;
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task drivePrechargeAll;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= (1 << opcode_cs)^{CS_BITS{1'b1}};
	 sdr_ca_pe <= 10'b0000011011;
	 sdr_ca_ne <= 10'h0;
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task driveWriteModeReg;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= (1 << opcode_cs)^{CS_BITS{1'b1}};
	 sdr_ca_pe <= { opcode_ma[5:0], 4'h0 };
	 sdr_ca_ne <= { opcode_op[7:0], opcode_ma[7:6] };
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task driveReadModeReg;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= (1 << opcode_cs)^{CS_BITS{1'b1}};
	 sdr_ca_pe <= { opcode_ma[5:0], 4'h8 };
	 sdr_ca_ne <= { 8'h0, opcode_ma[7:6] };
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task driveActivate;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= (1 << opcode_cs)^{CS_BITS{1'b1}};
	 sdr_ca_pe <= { opcode_ba[2:0], opcode_row[12:8], 2'b10 };
	 sdr_ca_ne <= { opcode_row[14:13], opcode_row[7:0] };
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};
	 saved_row_addr[opcode_ba] = opcode_row; // save this row, associated with the opcode_ba value

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task driveReadCmd;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= (1 << opcode_cs)^{CS_BITS{1'b1}};
	 sdr_ca_pe <= { opcode_ba[2:0], opcode_col[2:1], 5'b00101 };  // NOTE: opcode_col[0] is not used!
	 sdr_ca_ne <= { opcode_col[11:3], opcode_precharge };
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};
	 // use the bank address as an index to look up the row address that was specified during activate
	 RBC_address <= ((saved_row_addr[opcode_ba]&carbon_current_ROW_BITS_mask) << (carbon_current_BA_BITS + carbon_current_COL_BITS) ) | ((opcode_ba&carbon_current_BA_BITS_mask)<<carbon_current_COL_BITS) | (opcode_col&carbon_current_COL_BITS_mask); 
	 BRC_address <= ((opcode_ba&carbon_current_BA_BITS_mask) << (carbon_current_ROW_BITS + carbon_current_COL_BITS) ) | ((saved_row_addr[opcode_ba]&carbon_current_ROW_BITS_mask)<<carbon_current_COL_BITS) | (opcode_col&carbon_current_COL_BITS_mask); 

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task driveWriteCmd;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= (1 << opcode_cs)^{CS_BITS{1'b1}};
	 sdr_ca_pe <= { opcode_ba[2:0], opcode_col[2:1], 5'b00001 };  // NOTE: opcode_col[0] is not used!
	 sdr_ca_ne <= { opcode_col[11:3], opcode_precharge };
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};
	 RBC_address <= ((saved_row_addr[opcode_ba]&carbon_current_ROW_BITS_mask) << (carbon_current_BA_BITS + carbon_current_COL_BITS) ) | ((opcode_ba&carbon_current_BA_BITS_mask)<<carbon_current_COL_BITS) | (opcode_col&carbon_current_COL_BITS_mask); 
	 BRC_address <= ((opcode_ba&carbon_current_BA_BITS_mask) << (carbon_current_ROW_BITS + carbon_current_COL_BITS) ) | ((saved_row_addr[opcode_ba]&carbon_current_ROW_BITS_mask)<<carbon_current_COL_BITS) | (opcode_col&carbon_current_COL_BITS_mask); 

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task driveBstCmd;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= (1 << opcode_cs)^{CS_BITS{1'b1}};
	 sdr_ca_pe <= 10'h3;
	 sdr_ca_ne <= 10'h0;
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task driveBstCmdWriteData;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= {CS_BITS{1'b1}};
	 sdr_ca_pe <= 10'h3;
	 sdr_ca_ne <= 10'h0;
	 sdr_dq_valid_in <= 1'b1;
	 sdr_dq1_in <= current_dq1;
	 sdr_dq2_in <= current_dq2;
	 sdr_dm1_in <= current_dm1;
	 sdr_dm2_in <= current_dm2;

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task driveWriteData;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= {CS_BITS{1'b1}};
	 sdr_ca_pe <= 10'h3ff;
	 sdr_ca_ne <= 10'h3ff;
	 sdr_dq_valid_in <= 1'b1;
	 sdr_dq1_in <= current_dq1;
	 sdr_dq2_in <= current_dq2;
	 sdr_dm1_in <= current_dm1;
	 sdr_dm2_in <= current_dm2;

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   task driveLPDDR3WritePreamble;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= {CS_BITS{1'b1}};
	 sdr_ca_pe <= 10'h3ff;
	 sdr_ca_ne <= 10'h3ff;
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};

 	 sdr_preamble <= 1'b1;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   task driveFinish;
      begin
	 sdr_cke <= {CS_BITS{1'b1}};
	 sdr_cs_n <= {CS_BITS{1'b1}};
	 sdr_ca_pe <= 10'h3ff;
	 sdr_ca_ne <= 10'h3ff;
	 sdr_dq_valid_in <= 1'b0;
	 sdr_dq1_in <= {DATA_BITS{1'b0}};
	 sdr_dq2_in <= {DATA_BITS{1'b0}};
	 sdr_dm1_in <= {DM_BITS{1'b0}};
	 sdr_dm2_in <= {DM_BITS{1'b0}};

 	 sdr_preamble <= 1'b0;	 // preamble signal is only needed for LPDDR3 where dqs is driven an extra cycle early
      end
   endtask
   
   // ********************************************************************************
   
   // How to best handle read data?  Just use async process to display read values as gold files?
   // This does not allow for self checking tests....

   initial
     begin
	done_reported = 0;
	first_clock = 1;
	first_read = 1;
     end

   always @(posedge ck_t)
      begin
	 if ( first_clock )
	    begin
	       first_clock = 0;
	       $display("@%d STATUS Address formats: RBC / BRC      COL_BITS: %2d BANK_BITS: %2d ROW_BITS: %2d, DataWidth: %3d", 
			$time, carbon_current_COL_BITS, carbon_current_BA_BITS, carbon_current_ROW_BITS, carbon_current_DATA_BITS);
	    end
	 if ( !done_reported && test_done )
	    begin
	       done_reported = 1;
	       $display("@%d STATUS Test finished.", $time);
	    end
	 if (sdr_dq_valid_in)
	   begin
	      if ( carbon_current_DATA_BITS <= 32 )
		begin
		   display32_a = sdr_dq1_in&carbon_current_DATA_BITS_mask;
		   display32_b = sdr_dq2_in&carbon_current_DATA_BITS_mask;
		   $display("@%d SDR Write Address 0x%09x/0x%09x Data 0x%08x 0x%08x", $time, RBC_address,BRC_address, display32_a, display32_b);
		end
	      else if ( carbon_current_DATA_BITS <= 64 )
		begin
		   display64_a = sdr_dq1_in&carbon_current_DATA_BITS_mask;
		   display64_b = sdr_dq2_in&carbon_current_DATA_BITS_mask;
		   $display("@%d SDR Write Address 0x%09x/0x%09x Data 0x%016x 0x%016x", $time, RBC_address,BRC_address, display64_a, display64_b);
		end
	      else if ( carbon_current_DATA_BITS <= 128 )
		begin
 		   display128_a = sdr_dq1_in&carbon_current_DATA_BITS_mask;
 		   display128_b = sdr_dq2_in&carbon_current_DATA_BITS_mask;
		   $display("@%d SDR Write Address 0x%09x/0x%09x Data 0x%032x 0x%032x", $time, RBC_address,BRC_address, display128_a, display128_b);
		end
	      else if ( carbon_current_DATA_BITS <= 256 )
		begin
 		   display256_a = sdr_dq1_in&carbon_current_DATA_BITS_mask;
 		   display256_b = sdr_dq2_in&carbon_current_DATA_BITS_mask;
		   $display("@%d SDR Write Address 0x%09x/0x%09x Data 0x%064x 0x%064x", $time, RBC_address,BRC_address, display256_a, display256_b);
		end
	      else
		begin
		   $display("@%d SDR Write Address 0x%09x/0x%09x Data 0x%0x 0x%0x", $time, RBC_address,BRC_address, sdr_dq1_in&carbon_current_DATA_BITS_mask, sdr_dq2_in&carbon_current_DATA_BITS_mask);
		end
	   end
      end
   
   always @(posedge ck_t)
      begin
	 if (sdr_dq_valid_out)
	   begin
	      if ( first_read )
		 begin
		    // ignore the very first read, prob because the model set sdr_dq_valid_out when it should not have been set
		    first_read = 0;
		 end
	      else if ( carbon_current_DATA_BITS <= 32 )	  
		begin
		   display32_a = sdr_dq1_out; // no masking here so we can see if memory sent invalid values
		   display32_b = sdr_dq2_out;
		   $display("@%d SDR Read Address  0x%09x/0x%09x Data 0x%08x 0x%08x", $time, RBC_address,BRC_address, display32_a, display32_b);
		end
	      else if ( carbon_current_DATA_BITS <= 64 )
		begin
		   display64_a = sdr_dq1_out;
		   display64_b = sdr_dq2_out;
		   $display("@%d SDR Read Address  0x%09x/0x%09x Data 0x%016x 0x%016x", $time, RBC_address,BRC_address, display64_a, display64_b);
		end
	      else if ( carbon_current_DATA_BITS <= 128 )
		begin
		   display128_a = sdr_dq1_out;
		   display128_b = sdr_dq2_out;
		   $display("@%d SDR Read Address  0x%09x/0x%09x Data 0x%032x 0x%032x", $time, RBC_address,BRC_address, display128_a, display128_b);
		end
	      else if ( carbon_current_DATA_BITS <= 256 )
		begin
		   display256_a = sdr_dq1_out;
		   display256_b = sdr_dq2_out;
		   $display("@%d SDR Read Address  0x%09x/0x%09x Data 0x%064x 0x%064x", $time, RBC_address,BRC_address, display256_a, display256_b);
		end
	      else
		begin
		   $display("@%d SDR Read Address  0x%09x/0x%09x Data 0x%0x 0x%0x", $time, RBC_address,BRC_address, sdr_dq1_out, sdr_dq2_out);
		end
	   end
      end

   
   
endmodule // lpddr2_test_master
