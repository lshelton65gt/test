#include "TestGen.h"
#include <iostream>


int main () {
  
  TestGen  test;

  // ********************************************************************************
  // ********************************************************************************
  // Main Test
  // Perform front door write -> front door read
  // This is performed in Little Endian mode, for across all CS, across all Blocks, all BA, BL, BT, CL settings 
  unsigned int row = 0;
  unsigned int col = 0;
  unsigned int MR_val, WL=1;
  unsigned long long data[8]; // this is ignored by write data cmd.

  int BL, BT, CL, CS, BA;

  // Configuration values for non-exhaustive testing
  const unsigned int cNumFixedConfigs = 8;
  const unsigned int cCLValues[cNumFixedConfigs] = { 2, 3, 4, 5, 6, 2, 3, 4 };
  const unsigned int cCSValues[cNumFixedConfigs] = { 0, 1, 2, 3, 3, 2, 1, 0 };
  const unsigned int cBAValues[cNumFixedConfigs] = { 0, 1, 2, 3, 4, 5, 6, 7 };

  // burst length 1-3 (BL2, BL4, BL8) << 0
  for (BL = 1; BL < 4; BL++) {
    // burst type - seq = 0, interleave = 1  << 3
    for (BT = 0; BT < 2; BT++) {
#ifdef EXHAUSTIVE_TESTING
      // cas latency 2-6 << 4
      for (CL = 2; CL < 7; CL++) {
#else
    for (unsigned int fixedPass = 0; fixedPass < cNumFixedConfigs; ++fixedPass) {
      CL = cCLValues[fixedPass];
      // We need to handle there being fewer that 4 chip selects and
      // fewer than 8 banks
      CS = cCSValues[fixedPass] % CS_BITS;
      BA = cBAValues[fixedPass] % (1 << BA_BITS);
#endif
	unsigned int counter = 0;  // counter used as unique value for each location 
	
#ifdef EXHAUSTIVE_TESTING
	for (CS = 0; CS < CS_BITS; CS++) {
#endif
	  // Program the model with the appropriate latencies and settings
	  MR_val = (CL << 4) | (BT << 3) | BL;
	  test.idle(5);
	  test.writeModeReg(CS, 0, MR_val);
	  test.idle(5);
	  
#ifdef EXHAUSTIVE_TESTING
	  // bank address - treated separately from "address" as it has implications in the model function
	  for (BA = 0; BA < (1<<BA_BITS); BA++) {
#endif
	    
	    // for each walking 1 address (starting at 8) (need to check each offset and wrap!)
	    for (unsigned int rc = 3; rc < ROW_BITS + COL_BITS; rc++) {
	      counter++; // start with 1 to avoid false matches with unused locations
	      row = (1 << rc) >> COL_BITS;
	      col = (1 << rc); // model will perform mask to limit to bottom COL_BITS
	      
	      // use debug access to pre-program a value (always perform burst 8)
	      // for each beat in a burst
	      for (int i = 0; i < (1 << BL); i++) {  
		data[i] = counter + i;
	      }

	      // perform front door write
	      test.activate(CS, BA, row);
	      test.idle(10);
	      test.writeCmd(CS, BA, col);
	      test.idle(WL);
	      if (BL == 1) {
		test.writeData(data[0], 0, data[1], 0);
	      } else if (BL == 2) {
		test.writeData(data[0], 0, data[1], 0);
		test.writeData(data[2], 0, data[3], 0);
	      } else if (BL == 3) {
		test.writeData(data[0], 0, data[1], 0);
		test.writeData(data[2], 0, data[3], 0);
		test.writeData(data[4], 0, data[5], 0);
		test.writeData(data[6], 0, data[7], 0);
	      }
	      test.idle(5); 
	      
	      // perform front door read and check data 
	      test.readCmd(CS, BA, col, true); // true means auto precharge
	      // not implemented per brian checkReadBurst((1 << BL), data);
              test.idle(10); // added due to above nyi
	      test.idle(10);
	    }
#ifdef EXHAUSTIVE_TESTING
	  }
	}
#endif
      }
    }
  }  

  // ********************************************************************************
  // ********************************************************************************

  test.idle(100);
  test.finish();
  test.idle(10);

//   cout << "Pass Count = " << std::dec << pass_count << std::endl;
//   cout << "Fail Count = " << std::dec << fail_count << std::endl;
//   if (fail_count) {
//     cout << "TEST FAILED" << std::endl;
//   } else {
//     cout << "TEST PASSED" << std::endl;
//   }

  return 0;

}
