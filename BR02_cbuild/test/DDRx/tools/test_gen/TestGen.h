#include <iostream>
#include <fstream>
#include <iomanip>

class TestGen
{
public:
  // ********************************************************************************
  // These are the primary methods to generate the opcodes used by the verilog state machine
  // ********************************************************************************

  TestGen();
  void idle(int count);
  void prechargeBank(unsigned int cs, unsigned int ba);
  void prechargeAll(unsigned int cs);
  void writeModeReg(unsigned int cs, unsigned int ba, unsigned int addr);
  void lpddr2WriteModeReg(unsigned int cs, unsigned char ma, unsigned char op);
  void lpddr2ReadModeReg(unsigned int cs, unsigned char ma);
  void activate(unsigned int cs, unsigned int ba, unsigned int row);
  void readCmd(unsigned int cs, unsigned int ba, unsigned int col, bool precharge = false);
  void writeCmd(unsigned int cs, unsigned int ba, unsigned int col, bool precharge = false);
  void writeData(unsigned long long data1, unsigned int dm1, unsigned long long data2, unsigned int dm2);
  void lpddr2BstCmd(unsigned int cs);
  void lpddr2BstCmdWriteData(unsigned long long data1, unsigned int dm1, unsigned long long data2, unsigned int dm2);
  void ddrBstCmd(unsigned int cs) {};  // TBD
  void ddrPrechargeAllWriteData() {}; // TBD
  void ddr3ResetActiveCKEActive() {}; // TBD
  void ddr3ResetActiveCKEInactive() {}; // TBD
  void ddr3ResetInactiveCKEActive() {}; // TBD
  void ddr3ResetInactiveCKEInactive() {}; // TBD
  void ddr3ReadCmd(unsigned int cs, unsigned int ba, unsigned int col, bool burst8, bool precharge = false);
  void ddr3WritePreamble();
  void ddr3WriteCmd(unsigned int cs, unsigned int ba, unsigned int col, bool burst8, bool precharge = false);
  void finish();
  void checkReadBurst(unsigned int burstSize, unsigned long long* dataArray) {}; // TBD
  void captureReadBurst(unsigned int burstSize, unsigned long long* dataArray) {}; // TBD

private:
  unsigned int pc;  // internal counter
  std::ofstream *ofs;
  void print_memh(unsigned int opcode);
};

  
