#include "TestGen.h"
#include <iostream>


int main () {
  
  TestGen  test;
  
  // ********************************************************************************
  // ********************************************************************************
  // Main Test
  // Perform front door write -> front door read
  // This is performed across all CS, across all Blocks, all BA, BL, BT, CL, AL settings 
  unsigned int row = 0;
  unsigned int col = 0;

  unsigned int CS, BA, WL, RL, BC;
  unsigned int BL, CL, RBT, AL, wrLvlEn, CWL, MPRen; 
  unsigned int CLv, ALv; 

  // Configuration values for non-exhaustive testing
  const unsigned int cNumFixedConfigs = 8;
  const unsigned int cCLValues[cNumFixedConfigs] = { 1, 2, 3, 4, 5, 6, 7, 1 };
  const unsigned int cALValues[cNumFixedConfigs] = { 0, 1, 2, 0, 1, 2, 0, 1 };
  const unsigned int cCWLValues[cNumFixedConfigs] = { 0, 1, 2, 3, 0, 1, 2, 3 };
  const unsigned int cCSValues[cNumFixedConfigs] = { 0, 1, 2, 3, 3, 2, 1, 0 };
  const unsigned int cBAValues[cNumFixedConfigs] = { 0, 1, 2, 3, 4, 5, 6, 7 };

  // write leveling enable (disabled for these tests)
  wrLvlEn = 0;
  // MPR operation (disabled for these tests)
  MPRen = 0;

  // Since write operations are ALWAYS sequential, this rd/wr test must only use RBT = 0
  // read burst type - nibble seq = 0, interleave
  RBT = 0;

  // burst length 0-2 (BL8, BC4/8, BC4) - here we use BC to go through all four cases, reducing down to three BL settings
  for (BC = 0; BC < 4; BC++) {
    if (BC == 3) 
      BL = 1;
    else
      BL = BC;

#ifdef EXHAUSTIVE_TESTING
    // cas latency 5-11 (CL setting + 4)
    for (CL = 1; CL < 8; CL++) {
      // additive latency 0, CL-1, or CL-2, respectively
      for (AL = 0; AL < 3; AL++) {
	// cas write latency 5 - 8 (CWL setting + 5)
	for (CWL = 0; CWL < 4; CWL++) {
#else
    for (unsigned int fixedPass = 0; fixedPass < cNumFixedConfigs; ++fixedPass) {
      CL = cCLValues[fixedPass];
      AL = cALValues[fixedPass];
      CWL = cCWLValues[fixedPass];
      // We need to handle there being fewer that 4 chip selects and
      // fewer than 8 banks
      CS = cCSValues[fixedPass] % CS_BITS;
      BA = cBAValues[fixedPass] % (1 << BA_BITS);
#endif
	  unsigned int counter = 0;  // counter used as unique value for each location 
#ifdef EXHAUSTIVE_TESTING
	  for (CS = 0; CS < CS_BITS; CS++) {
#endif
	    // Program the model with the appropriate latencies and settings
	    // Converted DDR3 specific methods to generic writeModeReg(cs, ba, addr) method
	    // test.writeDDR3ModeReg0(CS, BL, CL, RBT);
	    test.writeModeReg(CS, 0, (BL | ((CL & 0x8) << 2) | ((RBT & 1) << 3) | ((CL & 0x7) << 4)));
            test.idle(15);
	    // test.writeDDR3ModeReg1(CS, AL, wrLvlEn);
	    test.writeModeReg(CS, 1, ((AL & 0x3) << 3) | ((wrLvlEn & 1) << 7));
            test.idle(15);
	    // test.writeDDR3ModeReg2(CS, CWL);
	    test.writeModeReg(CS, 2, ((CWL & 0x7) << 3));
            test.idle(15);
	    // test.writeDDR3ModeReg3(CS, MPRen);
	    test.writeModeReg(CS, 3, ((MPRen & 0x1) << 2));
            test.idle(15);
	    CLv = CL +4;
	    if (AL == 0)
	      ALv = 0;
	    else if (AL == 1)
	      ALv = CLv - 1;
	    else 
	      ALv = CLv -2;
	    
	    WL = ALv + (CWL + 5);
	    RL = ALv + CLv;
	    
#ifdef EXHAUSTIVE_TESTING
	    // bank address - treated separately from "address" as it has implications in the model function
	    for (BA = 0; BA < (1<<BA_BITS); BA++) {
#endif
	      
	      // for each walking 1 address (starting at 8) (need to check each offset and wrap!)
	      for (unsigned int rc = 3; rc < ROW_BITS + COL_BITS; rc++) {
		counter++; // start with 1 to avoid false matches with unused locations
		row = (1 << rc) >> COL_BITS;
		col = (1 << rc); // ASSUME model will perform mask to limit to bottom COL_BITS
		
		unsigned long long data[8];

		// set up write data array
		for (int i = 0; i < ((BC&0x2)?4:8); i++) {
		  data[i] = counter + i;
		}
							  
		// perform front door write
		test.activate(CS, BA, row);
		test.idle(10);
		test.ddr3WriteCmd(CS, BA, col, (BC>>1)^1);  // no precharge
		test.idle(WL-1);
		test.ddr3WritePreamble();
		test.writeData(data[0], 0, data[1], 0);
		test.writeData(data[2], 0, data[3], 0);
		if (BC < 2) {
		  test.writeData(data[4], 0, data[5], 0);
		  test.writeData(data[6], 0, data[7], 0);
		}
		test.idle(5); 
		
		// now perform frontdoor read and check data
		test.ddr3ReadCmd(CS, BA, col, (BC>>1)^1, true); // true -> precharge
		// nyi per Brian checkReadBurst((BC < 2) ? 8 : 4, data);
		test.idle(10);  // based on above line
		test.idle(10); 
	      }
#ifdef EXHAUSTIVE_TESTING
	    }
	  }
	}
      }
#endif
    }
      // additional test for bug16691 case - back to back writes/reads across underlying verilog memory blocks 
      CS = 0;
      BL = 0;
      CL = 2;
      RBT = 0;
      AL = 0;
      wrLvlEn = 0;
      CWL = 0;
      MPRen = 0;
      for (CS = 0; CS < CS_BITS; CS++) {

	// Program the model with the appropriate latencies and settings
	// Converted DDR3 specific methods to generic writeModeReg(cs, ba, addr) method
	// test.writeDDR3ModeReg0(CS, BL, CL, RBT);
	test.writeModeReg(CS, 0, (BL | ((CL & 0x8) << 2) | ((RBT & 1) << 3) | ((CL & 0x7) << 4)));
	test.idle(15);
	// test.writeDDR3ModeReg1(CS, AL, wrLvlEn);
	test.writeModeReg(CS, 1, ((AL & 0x3) << 3) | ((wrLvlEn & 1) << 7));
	test.idle(15);
	// test.writeDDR3ModeReg2(CS, CWL);
	test.writeModeReg(CS, 2, ((CWL & 0x7) << 3));
	test.idle(15);
	// test.writeDDR3ModeReg3(CS, MPRen);
	test.writeModeReg(CS, 3, ((MPRen & 0x1) << 2));
	test.idle(15);
	CLv = CL +4;
	if (AL == 0)
	  ALv = 0;
	else if (AL == 1)
	  ALv = CLv - 1;
	else 
	  ALv = CLv -2;
	
	WL = ALv + (CWL + 5);
	RL = ALv + CLv;

	unsigned int counter = 0;  // counter used as unique value for each location 
	
	for (BA = 1; BA < (1<<BA_BITS); BA++) {
	  
	  unsigned long long data[8];
	  
	  // set up write data array
	  for (int i = 0; i < ((BC&0x2)?4:8); i++) {
	    data[i] = counter + i;
	  }
	  for (int i = 0; i < 2; i++) {
	    // vary the delay of the write data so the dqs is delayed
	    // perform front door write
	    test.activate(CS, BA, row);
	    test.idle(1);
	    test.activate(CS, 0, row);
	    test.idle(8);
	    test.ddr3WriteCmd(CS, BA, col, (BC>>1)^1);  // no precharge
	    test.idle(3);
	    test.ddr3WriteCmd(CS, 0, col+0x100, (BC>>1)^1);  // no precharge
	    test.idle(WL-5+i);
	    test.ddr3WritePreamble();
	    test.writeData(data[0], 0, data[1], 0);
	    test.writeData(data[2], 0, data[3], 0);
	    test.writeData(data[4], 0, data[5], 0);
	    test.writeData(data[6], 0, data[7], 0);
	    test.writeData(~data[0], 0, ~data[1], 0);
	    test.writeData(~data[2], 0, ~data[3], 0);
	    test.writeData(~data[4], 0, ~data[5], 0);
	    test.writeData(~data[6], 0, ~data[7], 0);
	    
	    test.idle(5); 
	    
	    // now perform frontdoor read and check data
	    test.ddr3ReadCmd(CS, BA, col, (BC>>1)^1, true); // true -> precharge
	    test.idle(3); 
	    test.ddr3ReadCmd(CS, 0, col+0x100, (BC>>1)^1, true); // true -> precharge
	    // nyi per Brian checkReadBurst((BC < 2) ? 8 : 4, data);
	    test.idle(10);  // based on above line
	    test.idle(20); 
	}
      }	
    }
  }

  // ********************************************************************************
  // ********************************************************************************

  test.idle(100);
  test.finish();
  test.idle(10);

//   cout << "Pass Count = " << std::dec << pass_count << std::endl;
//   cout << "Fail Count = " << std::dec << fail_count << std::endl;
//   if (fail_count) {
//     cout << "TEST FAILED" << std::endl;
//   } else {
//     cout << "TEST PASSED" << std::endl;
//   }

  return 0;

}
