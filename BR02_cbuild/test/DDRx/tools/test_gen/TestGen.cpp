#include "TestGen.h"

TestGen::TestGen() {
  pc = 0;
  ofs = new std::ofstream("test_sequence.memh", std::ios::out);
}

void
TestGen::print_memh(unsigned int opcode) {
  *ofs << "@" << std::setfill('0') << std::setw(8) << std::hex << pc << " " << std::setfill('0') << std::setw(8) << std::hex << opcode << std::endl;
  pc++;
}

// ********************************************************************************
void 
TestGen::idle(int count) {
  for (int i = 0; i < count; i++) {
    print_memh(0);  // idle opcode
  } 
}

void 
TestGen::prechargeBank(unsigned int cs, unsigned int ba) {
  unsigned int opcode = 1 << 27;
  opcode |= ((cs & 0x7) << 24);
  opcode |= ((ba & 0x7) << 16);
  print_memh(opcode);
}

void 
TestGen::prechargeAll(unsigned int cs) {
  unsigned int opcode = 2 << 27;
  opcode |= ((cs & 0x7) << 24);
  print_memh(opcode);
}

void 
TestGen::writeModeReg(unsigned int cs, unsigned int ba, unsigned int addr) {
  unsigned int opcode = 3 << 27;
  opcode |= ((cs & 0x7) << 24);
  opcode |= ((ba & 0x7)<< 16);
  opcode |= (addr & 0xffff);
  print_memh(opcode);
}

void 
TestGen::lpddr2WriteModeReg(unsigned int cs, unsigned char ma, unsigned char op) {
  unsigned int opcode = 3 << 27;
  opcode |= ((cs & 0x7) << 24);
  opcode |= ((op & 0xff) << 8);
  opcode |= (ma & 0xff);
  print_memh(opcode);
}

void 
TestGen::lpddr2ReadModeReg(unsigned int cs, unsigned char ma) {
  unsigned int opcode = 10 << 27;
  opcode |= ((cs & 0x7) << 24);
  opcode |= (ma & 0xff);
  print_memh(opcode);
}

void 
TestGen::activate(unsigned int cs, unsigned int ba, unsigned int row) {
  unsigned int opcode = 4 << 27;
  opcode |= ((cs & 0x7) << 24);
  opcode |= ((ba & 0x7)<< 16);
  opcode |= (row & 0xffff);
  print_memh(opcode);
}

void 
TestGen::readCmd(unsigned int cs, unsigned int ba, unsigned int col, bool precharge) {
  unsigned int opcode = 5 << 27;
  opcode |= ((cs & 0x7) << 24);
  opcode |= ((ba & 0x7)<< 16);
  opcode |= (col & 0x7fff);
  if (precharge)
    opcode |= (1 << 19);
  print_memh(opcode);
}

void 
TestGen::ddr3ReadCmd(unsigned int cs, unsigned int ba, unsigned int col, bool burst8, bool precharge) {
  unsigned int opcode = 15 << 27;
  opcode |= ((cs & 0x7) << 24);
  opcode |= ((ba & 0x7)<< 16);
  opcode |= (col & 0x7fff);
  if (precharge)
    opcode |= (1 << 19);
  if (burst8)
    opcode |= (1 << 20);
  print_memh(opcode);
}

void 
TestGen::writeCmd(unsigned int cs, unsigned int ba, unsigned int col, bool precharge) {
  unsigned int opcode = 6 << 27;
  opcode |= ((cs & 0x7) << 24);
  opcode |= ((ba & 0x7)<< 16);
  opcode |= (col & 0x7fff);
  if (precharge)
    opcode |= (1 << 19);
  print_memh(opcode);
}

void 
TestGen::ddr3WriteCmd(unsigned int cs, unsigned int ba, unsigned int col, bool burst8, bool precharge) {
  unsigned int opcode = 17 << 27;
  opcode |= ((cs & 0x7) << 24);
  opcode |= ((ba & 0x7)<< 16);
  opcode |= (col & 0x7fff);
  if (precharge)
    opcode |= (1 << 19);
  if (burst8)
    opcode |= (1 << 20);
  print_memh(opcode);
}

void 
TestGen::ddr3WritePreamble() {  // this is also LPDDR3 write preamble
  unsigned int opcode = 16 << 27;
  print_memh(opcode);
}

void 
TestGen::writeData(unsigned long long data1, unsigned int dm1, unsigned long long data2, unsigned int dm2) {
  unsigned int opcode = 7 << 27;

  // BSS this signature needs to change
  // Choose data based on lower bits of PC for now
  opcode |= ((((pc << 1) & 0xff) | 0) << 0); // dq1_idx 
  opcode |= ((((pc << 1) & 0xff) | 1) << 8); // dq2_idx 
  opcode |= ((0x0 & 0xf) << 16);  // dm1_idx = 0
  opcode |= ((0x0 & 0xf) << 20);  // dm2_idx = 0
  print_memh(opcode);
}

void 
TestGen::lpddr2BstCmd(unsigned int cs) {
  unsigned int opcode = 8 << 27;
  opcode |= ((cs & 0x7) << 24);
  print_memh(opcode);
}

void 
TestGen::lpddr2BstCmdWriteData(unsigned long long data1, unsigned int dm1, unsigned long long data2, unsigned int dm2) {
  unsigned int opcode = 9 << 27;

  // BSS this signature needs to change
  // Choose data based on lower bits of PC for now
  opcode |= ((((pc << 1) & 0xff) | 0) << 0); // dq1_idx 
  opcode |= ((((pc << 1) & 0xff) | 1) << 8); // dq2_idx 
  opcode |= ((0x0 & 0xf) << 16);  // dm1_idx = 0
  opcode |= ((0x0 & 0xf) << 20);  // dm2_idx = 0
  print_memh(opcode);
}


void
TestGen::finish() {
  unsigned int opcode = 0x1f << 27;
  print_memh(opcode);
}
