#include "TestGen.h"
#include <iostream>


int main () {
  
  TestGen  test;
  
//   // ********************************************************************************
//   // ********************************************************************************
//   // Main Test
//   // Perform front door write -> front door read
//   // This is performed in Little Endian mode, for across all CS, across all Blocks, all BA, BL, BT, CL, AL settings 
//   unsigned int row = 0;
//   unsigned int col = 0;
  
// //  int bytes = DM_BITS;  // This is the number of data bytes in the data bus
//   unsigned int MR_val, EMR1_val, RL, WL;
//   int BL, BT, CL, AL, CS, BA;

//   // burst length 2-3 (BL4, BL8) << 0
//   for (BL = 2; BL < 4; BL++) {
//     // burst type - seq = 0, interleave = 1  << 3
//     for (BT = 0; BT < 2; BT++) {
//       // cas latency 2-6 << 4
//       for (CL = 2; CL < 7; CL++) {
// 	// additive latency 0-5  << 3
// 	for (AL = 0; AL < 6; AL++) {
// 	  unsigned int counter = 0;  // counter used as unique value for each location 

// 	  for (CS = 0; CS < CS_BITS; CS++) {
// 	    // Program the model with the appropriate latencies and settings
// 	    RL = AL + CL;
// 	    WL = RL - 1;
// 	    MR_val = (CL << 4) | (BT << 3) | BL;
// 	    EMR1_val = AL << 3;
// 	    test.idle(5);
// 	    test.writeModeReg(CS, 0, MR_val);
// 	    test.idle(5);
// 	    test.writeModeReg(CS, 1, EMR1_val);
// 	    test.idle(5);
	  
// 	    // bank address - treated separately from "address" as it has implications in the model function
// 	    for (BA = 0; BA < (1<<BA_BITS); BA++) {
	      
// 	      // for each walking 1 address (starting at 8) (need to check each offset and wrap!)
// 	      for (unsigned int rc = 3; rc < ROW_BITS + COL_BITS; rc++) {
// 		counter++; // start with 1 to avoid false matches with unused locations
// 		row = (1 << rc) >> COL_BITS;
// 		col = (1 << rc); // model will perform mask to limit to bottom COL_BITS
		
// 		// use debug access to pre-program a value (always perform burst 8)
// 		unsigned long long data[8];
// 		// for each beat in a burst
// 		for (int i = 0; i < ((BL==3)?8:4); i++) {
// 		  data[i] = counter + i;
// 		}

// 		// perform frontdoor write 
// 		test.activate(CS, BA, row);
// 		test.idle(10);
// 		test.writeCmd(CS, BA, col);  // no precharge
// 		test.idle(WL);
// 		test.writeData(data[0], 0, data[1], 0);
// 		test.writeData(data[2], 0, data[3], 0);
// 		if (BL == 3) {
// 		  test.writeData(data[4], 0, data[5], 0);
// 		  test.writeData(data[6], 0, data[7], 0);
// 		}
// 		test.idle(5); 

// 		// now perform front door read and check the data 
// 		test.readCmd(CS, BA, col);
// 		test.checkReadBurst(((BL == 3) ? 8 : 4), data);
// 		test.idle(10);

// 		// Now precharge to get ready for next access
// 		test.prechargeAll(CS);
// 		test.idle(10);
// 	      }
// 	    }
// 	  }
// 	}
//       }
//     }
//   }


  // ********************************************************************************
  // ********************************************************************************
  // This set of tests does not use the CarbonDDRxModelInterface backdoor memory access capabilities
  // and therefore can be used with other models that do not provide it

  // ********************************************************************************
  // ********************************************************************************
  // Main Test
  // Perform front door write -> front door read
  // This is performed in Little Endian mode, for across all CS, across all Blocks, all BA, BL, BT, CL, AL settings 
  unsigned int row = 0;
  unsigned int col = 0;

  unsigned int CS, BA, BL, BT, RLWL;
  unsigned int RL, WL;

  unsigned int ADDED_DELAY;

  // burst length 3 (BL8)
  BL = 3;
  {
    // burst type sequential = 0
    BT = 0; 
    for (ADDED_DELAY = 0; ADDED_DELAY < 3; ADDED_DELAY++) 
    {
      // latency setting (combined)
      for (int i = 0; i < 14; i++) {
	switch (i) {
	case 0 : RLWL = 0x01; RL =  3; WL = 1; break;
	case 1 : RLWL = 0x04; RL =  6; WL = 3; break;
	case 2 : RLWL = 0x06; RL =  8; WL = 4; break;
	case 3 : RLWL = 0x07; RL =  9; WL = 5; break;
	case 4 : RLWL = 0x08; RL = 10; WL = 6; break;
	case 5 : RLWL = 0x09; RL = 11; WL = 6; break;
	case 6 : RLWL = 0x0a; RL = 12; WL = 6; break;

	case 7 : RLWL = 0x41; RL =  3; WL = 1; break;
	case 8 : RLWL = 0x44; RL =  6; WL = 3; break;
	case 9 : RLWL = 0x46; RL =  8; WL = 4; break;
	case 10: RLWL = 0x47; RL =  9; WL = 5; break;
	case 11: RLWL = 0x48; RL = 10; WL = 8; break;
	case 12: RLWL = 0x49; RL = 11; WL = 9; break;
	case 13: RLWL = 0x4a; RL = 12; WL = 9; break;
	}

	unsigned int counter = 0;  // counter used as unique value for each location 
	for (CS = 0; CS < CS_BITS; CS++) {
	  test.idle(10);
	  // Program the model with the appropriate latencies and settings
	  test.lpddr2WriteModeReg(CS, 0x01, ((BT << 3) | BL));
	  test.lpddr2WriteModeReg(CS, 0x02, RLWL);
	  
	  // bank address - treated separately from "address" as it has implications in the model function
	  for (BA = 0; BA < (1<<BA_BITS); BA++) {
	    
	    // for each walking 1 address (starting at 16) (need to check each offset and wrap!)
	    for (unsigned int rc = 4; rc < ROW_BITS + COL_BITS; rc++) {
	      counter++; // start with 1 to avoid false matches with unused locations
	      row = (1 << rc) >> COL_BITS;
	      col = (1 << rc) & (0xffffffff >> (32 - COL_BITS));
	      
	      // use debug access to pre-program a value (always perform burst 16)
	      // for each beat in a burst
// 	      for (int i = 0; i < (1 << BL); i++) {  
// 		data[i] = counter + i;
// 	      }

              unsigned long long data[8];
              // for each beat in a burst - burst length is always 8
              for (int i = 0; i < 8; i++) {
                data[i] = counter + i;
              }

	      // perform front door write
	      test.activate(CS, BA, row);
	      test.idle(10);
	      test.writeCmd(CS, BA, col);
	      test.idle(WL-1+ADDED_DELAY);
	      test.ddr3WritePreamble();  // this also serves for LPDDR3 
	      test.writeData(0, 0, 0, 0);
	      test.writeData(0, 0, 0, 0);
	      test.writeData(0, 0, 0, 0);
	      test.writeData(0, 0, 0, 0);
	      test.idle(5); 

	      // perform front door read and check data 
 	      test.readCmd(CS, BA, col, true);  // true parameter means auto-precharge
// 	      checkReadBurst((BL>3)?16:((BL>2)?8:4), data);
	      test.idle(20);
	    }
	  }
	}
      }
    }
    RLWL = 0x08; RL = 10; WL = 6;
    {
      // Test back to back writes (where 2nd write command's preamble is not needed)
      // Modified to use different Banks to replicate bug16691 since that should cause address to cross blocks
	unsigned int counter = 0;  // counter used as unique value for each location 
	for (CS = 0; CS < 1; CS++) {
	  test.idle(10);
	  // Program the model with the appropriate latencies and settings
	  test.lpddr2WriteModeReg(CS, 0x01, ((BT << 3) | BL));
	  test.lpddr2WriteModeReg(CS, 0x02, RLWL);
	  
	  // bank address - treated separately from "address" as it has implications in the model function
	  for (BA = 1; BA < (1<<BA_BITS); BA++) {
	    
	    // for each walking 1 address (starting at 16) (need to check each offset and wrap!)
	    for (unsigned int rc = 4; rc < ROW_BITS + COL_BITS; rc++) {
	      counter++; // start with 1 to avoid false matches with unused locations
	      row = (1 << rc) >> COL_BITS;
	      col = (1 << rc) & (0xffffffff >> (32 - COL_BITS));
	      
	      unsigned long long data[8];
	      // for each beat in a burst - burst length is always 8
	      for (int i = 0; i < 8; i++) {
		data[i] = counter + i;
	      }
	      
	      // perform front door write
	      test.activate(CS, BA, row);
	      test.activate(CS, 0, row);
	      test.idle(9);
	      test.writeCmd(CS, BA, col);
	      test.idle(3);
	      test.writeCmd(CS, 0, col+0x10); // use Bank 0 to cause underlying verilog block crossing to test bug16691
	      test.idle(1);
	      test.idle(2); // ADDED DELAY
	      test.ddr3WritePreamble();  // this also serves for LPDDR3 
	      test.writeData(0, 0, 0, 0);
	      test.writeData(0, 0, 0, 0);
	      test.writeData(0, 0, 0, 0);
	      test.writeData(0, 0, 0, 0);
	      test.writeData(0, 0, 0, 0);
	      test.writeData(0, 0, 0, 0);
	      test.writeData(0, 0, 0, 0);
	      test.writeData(0, 0, 0, 0);
	      test.idle(5); 
	      
	      // perform front door read and check data 
	      test.readCmd(CS, BA, col, true);  // true parameter means auto-precharge
	      test.idle(3);
	      test.readCmd(CS, 0, col+0x10, true);  // true parameter means auto-precharge
	      test.idle(20);
	    }
	  }
	}
      }
  }



  // ********************************************************************************
  // ********************************************************************************

  test.idle(100);
  test.finish();
  test.idle(10);

//   cout << "Pass Count = " << std::dec << pass_count << std::endl;
//   cout << "Fail Count = " << std::dec << fail_count << std::endl;
//   if (fail_count) {
//     cout << "TEST FAILED" << std::endl;
//   } else {
//     cout << "TEST PASSED" << std::endl;
//   }

  return 0;

}
