`timescale 1ps / 1ps
module carbon_memory_lpddr3_socd_wrapper(/*AUTOARG*/
   // Outputs
   sdr_dq_valid_out, sdr_dq2_out, sdr_dq1_out,
   // Inputs
   sdr_dq_valid_in, sdr_dq2_in, sdr_dq1_in, sdr_dm2_in, sdr_dm1_in,
   sdr_cs_n, sdr_cke, sdr_ca_pe, sdr_ca_ne, ck_t, sdr_preamble
   );

   // ********************************************************************************
   // PARAMETER CALCULATIONS REPLICATED FROM carbon_lpddr3 MODULE
   //

   // number of chip selects (ranks) and data width
   parameter CS_BITS = 4;  // determines number of cs_n and cke pins
   parameter DATA_BITS = 8;  // determines data bit width and number of DM and DQS pins (1 per byte)

   parameter MAX_ADDR_BITS = 29;

   // cycle accurate timing parameters
   
   // DQS_IN_DELAY: default assumes EARLY DQS input where DQS is high during first data transfer so it must
   // be delayed by 1/2 cycle to convert it to LATE DQS so it can be directly used as a clock
   // For "normal" JEDEC timing where DQS is already centered on the data, set DQS_IN_DELAY = 0
   parameter DQS_IN_DELAY = 1;

   // DQS_OUT_DELAY: default generates EARLY DQS timing which is the JEDEC standard.  To allow DQS to capture
   // data directly without further modification, it can be converted to LATE DQS timing by setting DQS_OUT_DELAY = 1
   parameter DQS_OUT_DELAY = 0;
   
   // Basic Configuration 4 setting - this parameter will define the reset value of 
   // Mode Register 8 which defines the I/O width, Density, and Type of memory
   parameter MR8_SETTING = 1;

   // Top level parameter to turn WRITE_PREAMBLE on and off
   parameter WRITE_PREAMBLE = 1;
   
   // calculated parameters
   parameter DM_BITS = ((DATA_BITS % 8) == 0 ) ? (DATA_BITS / 8) : (DATA_BITS / 8 ) + 1; 

   input                sdr_preamble;
   /*AUTOINPUT*/
   // Beginning of automatic inputs (from unused autoinst inputs)
   input		ck_t;			// To mem of carbon_lpddr3_sdram.v, ...
   input [9:0]		sdr_ca_ne;		// To intf of carbon_lpddr3_interface.v
   input [9:0]		sdr_ca_pe;		// To intf of carbon_lpddr3_interface.v
   input [CS_BITS-1:0]	sdr_cke;		// To intf of carbon_lpddr3_interface.v
   input [CS_BITS-1:0]	sdr_cs_n;		// To intf of carbon_lpddr3_interface.v
   input [DM_BITS-1:0]	sdr_dm1_in;		// To intf of carbon_lpddr3_interface.v
   input [DM_BITS-1:0]	sdr_dm2_in;		// To intf of carbon_lpddr3_interface.v
   input [DATA_BITS-1:0] sdr_dq1_in;		// To intf of carbon_lpddr3_interface.v
   input [DATA_BITS-1:0] sdr_dq2_in;		// To intf of carbon_lpddr3_interface.v
   input		sdr_dq_valid_in;	// To intf of carbon_lpddr3_interface.v
   // End of automatics
   /*AUTOOUTPUT*/
   // Beginning of automatic outputs (from unused autoinst outputs)
   output [DATA_BITS-1:0] sdr_dq1_out;		// From intf of carbon_lpddr3_interface.v
   output [DATA_BITS-1:0] sdr_dq2_out;		// From intf of carbon_lpddr3_interface.v
   output		sdr_dq_valid_out;	// From intf of carbon_lpddr3_interface.v
   // End of automatics
   
   // ********************************************************************************

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [9:0]		ca;			// From intf of carbon_lpddr3_interface.v
   wire [CS_BITS-1:0]	cke;			// From intf of carbon_lpddr3_interface.v
   wire [CS_BITS-1:0]	cs_n;			// From intf of carbon_lpddr3_interface.v
   wire [DM_BITS-1:0]	dm;			// From intf of carbon_lpddr3_interface.v
   wire [DATA_BITS-1:0]	dq;			// To/From mem of carbon_lpddr3_sdram.v, ...
   wire [DM_BITS-1:0]	dqs_c;			// To/From mem of carbon_lpddr3_sdram.v, ...
   wire [DM_BITS-1:0]	dqs_t;			// To/From mem of carbon_lpddr3_sdram.v, ...
   // End of automatics
   /*AUTOREG*/

   wire 		ck_c = ~ck_t;
   
   
   carbon_memory_lpddr3 #(.CS_BITS(CS_BITS),
			 .DATA_BITS(DATA_BITS),
			 .MAX_ADDR_BITS(MAX_ADDR_BITS),
			 .MR8_SETTING(MR8_SETTING),
			 .WRITE_PREAMBLE(WRITE_PREAMBLE),
			 .DQS_IN_DELAY(DQS_IN_DELAY),
			 .DQS_OUT_DELAY(DQS_OUT_DELAY)) mem(/*AUTOINST*/
							    // Inouts
							    .dq			(dq[DATA_BITS-1:0]),
							    .dqs_t		(dqs_t[DM_BITS-1:0]),
							    .dqs_c		(dqs_c[DM_BITS-1:0]),
							    // Inputs
							    .ck_t		(ck_t),
							    .ck_c		(ck_c),
							    .cke		(cke[CS_BITS-1:0]),
							    .cs_n		(cs_n[CS_BITS-1:0]),
							    .ca			(ca[9:0]),
							    .dm			(dm[DM_BITS-1:0]));


   carbon_lpddr3_interface #(.CS_BITS(CS_BITS),
			     .DATA_BITS(DATA_BITS),
			     .WRITE_PREAMBLE(WRITE_PREAMBLE),
			     .DQS_IN_DELAY(DQS_IN_DELAY),
			     .DQS_OUT_DELAY(DQS_OUT_DELAY)) intf(/*AUTOINST*/
								 // Outputs
								 .cke			(cke[CS_BITS-1:0]),
								 .cs_n			(cs_n[CS_BITS-1:0]),
								 .ca			(ca[9:0]),
								 .dm			(dm[DM_BITS-1:0]),
								 .sdr_dq1_out		(sdr_dq1_out[DATA_BITS-1:0]),
								 .sdr_dq2_out		(sdr_dq2_out[DATA_BITS-1:0]),
								 .sdr_dq_valid_out	(sdr_dq_valid_out),
								 // Inouts
								 .dqs_t			(dqs_t[DM_BITS-1:0]),
								 .dqs_c			(dqs_c[DM_BITS-1:0]),
								 .dq			(dq[DATA_BITS-1:0]),
								 // Inputs
								 .ck_t			(ck_t),
								 .sdr_cke		(sdr_cke[CS_BITS-1:0]),
								 .sdr_preamble          (sdr_preamble),
								 .sdr_cs_n		(sdr_cs_n[CS_BITS-1:0]),
								 .sdr_ca_pe		(sdr_ca_pe[9:0]),
								 .sdr_ca_ne		(sdr_ca_ne[9:0]),
								 .sdr_dq_valid_in	(sdr_dq_valid_in),
								 .sdr_dq1_in		(sdr_dq1_in[DATA_BITS-1:0]),
								 .sdr_dq2_in		(sdr_dq2_in[DATA_BITS-1:0]),
								 .sdr_dm1_in		(sdr_dm1_in[DM_BITS-1:0]),
								 .sdr_dm2_in		(sdr_dm2_in[DM_BITS-1:0]));
      
endmodule
