module carbon_lpddr3_interface(/*AUTOARG*/
   // Outputs
   cke, cs_n, ca, dm, sdr_dq1_out, sdr_dq2_out, sdr_dq_valid_out,
   // Inouts
   dqs_t, dqs_c, dq,
   // Inputs
   sdr_preamble,
   ck_t, sdr_cke, sdr_cs_n, sdr_ca_pe, sdr_ca_ne, sdr_dq_valid_in,
   sdr_dq1_in, sdr_dq2_in, sdr_dm1_in, sdr_dm2_in
   );

   // number of chip selects (ranks) and data width
   parameter CS_BITS = 4;  // determines number of cs_n and cke pins
   parameter DATA_BITS = 4;  // determines data bit width and number of DM and DQS pins (1 per byte)
   
   parameter WRITE_PREAMBLE = 1;
   // cycle accurate timing parameters
   
   // DQS_IN_DELAY: default assumes EARLY DQS input where DQS is high during first data transfer so it must
   // be delayed by 1/2 cycle to convert it to LATE DQS so it can be directly used as a clock
   // For "normal" JEDEC timing where DQS is already centered on the data, set DQS_IN_DELAY = 0
   parameter DQS_IN_DELAY = 1;

   // DQS_OUT_DELAY: default generates EARLY DQS timing which is the JEDEC standard.  To allow DQS to capture
   // data directly without further modification, it can be converted to LATE DQS timing by setting DQS_OUT_DELAY = 1
   parameter DQS_OUT_DELAY = 0;
   
   // calculated parameter
   parameter DM_BITS = ((DATA_BITS % 8) == 0 ) ? (DATA_BITS / 8) : (DATA_BITS / 8 ) + 1; 
   
   input                    ck_t;
   output reg [CS_BITS-1:0] cke;
   output reg [CS_BITS-1:0] cs_n;
   output reg [9:0] 	    ca;
   
   output reg [DM_BITS-1:0] dm;
   // from cycle based perspective all dqs strobes must have identical timing 
   // only a single DQS signal is use internally - vector form provided for convenience
   inout [DM_BITS-1:0] 	    dqs_t;
   inout [DM_BITS-1:0] 	    dqs_c; // unused
   inout [DATA_BITS-1:0]    dq; 
   
   // single data rate interface
   input [CS_BITS-1:0] 	    sdr_cke;
   input [CS_BITS-1:0] 	    sdr_cs_n;
   input [9:0] 		    sdr_ca_pe;
   input [9:0] 		    sdr_ca_ne;
   input                    sdr_preamble;
   
   input 		    sdr_dq_valid_in;
   input [DATA_BITS-1:0]    sdr_dq1_in;
   input [DATA_BITS-1:0]    sdr_dq2_in;
   input [DM_BITS-1:0] 	    sdr_dm1_in;
   input [DM_BITS-1:0] 	    sdr_dm2_in;
   
   output reg [DATA_BITS-1:0] sdr_dq1_out;
   output reg [DATA_BITS-1:0] sdr_dq2_out;
   output reg 		      sdr_dq_valid_out;
   

   // ********************************************************************************
   // ********************************************************************************

   // ****************************************
   // general signal timing (always posedge sampled same in both domains)
   // sample on posedge but launch on negedge (common practice in real phy)

   reg [CS_BITS-1:0] 		   sdr_cke_d;
   reg [CS_BITS-1:0] 		   sdr_cs_n_d;
   reg [9:0] 			   sdr_ca_pe_d;
   reg [9:0] 			   sdr_ca_ne_d;
   
   always @(posedge ck_t) begin
      sdr_cke_d <= sdr_cke;
      sdr_cs_n_d <= sdr_cs_n;

      sdr_ca_pe_d <= sdr_ca_pe;
      sdr_ca_ne_d <= sdr_ca_ne;

      ca <= sdr_ca_ne_d;
   end
	      
   always @(negedge ck_t) begin
      cke <= sdr_cke_d;
      cs_n <= sdr_cs_n_d;

      ca <= sdr_ca_pe_d;
   end
	      
   
   // ********************************************************************************
   // ********************************************************************************
   // Bidirectional signal generation and timing modification
   
   // generation of strobes using data techniques 
   reg 			pe_clk_div_2;
   reg 			ne_clk_div_2;
   reg 			dqs_clk;

   always @(posedge ck_t)
     pe_clk_div_2 <= ~pe_clk_div_2;
   
   always @(negedge ck_t)
     ne_clk_div_2 <= pe_clk_div_2;

   always @(*)
     dqs_clk <= pe_clk_div_2 ^ ne_clk_div_2; // this is a data signal that looks like the clock
   
   
   wire 		    dqs_out;
   reg 			    dqs_out_internal;

   parameter INTF_DQS_OUT_DELAY = (DQS_IN_DELAY == 0) ? 1 : 0;
   carbon_retiming_element #(1, INTF_DQS_OUT_DELAY) dqs_out_retimer(ck_t, dqs_out_internal, dqs_out);
   
   reg 			    write_preamble_enabled;
   initial
     begin
	write_preamble_enabled = WRITE_PREAMBLE;
     end
   
   wire 		    dqs_oe;
   reg 			    dqs_oe_internal;
   reg 			    dqs_preamble;
   assign dqs_oe = dqs_oe_internal | dqs_preamble; // dqs oe timing should not change, even though dqs out timing does by 1/2 cycle
   
   // NOTE:  Only a single dqs signal is used internally!
   // general form for generating internal write dqs (DM should provide final qualification of data)
   wire 		    dqs_in = |dqs_t & !dqs_oe; // ignore dqs that we generate

   // attempt to work around cbuild issue
   reg 			    dqs_in_reg;
   reg 			    dqs_in_reg_gate; // carbon depositSignal

   always @(*)
     dqs_in_reg <= dqs_in & !dqs_in_reg_gate;
   
   // alternate form using lsb for generating internal write dqs - just use a single bit
   // wire 		    dqs_in = dqs_t[0] & !dqs_oe; // ignore dqs that we generate
   wire 		    dqs_in_internal;
   parameter INTF_DQS_IN_DELAY = (DQS_OUT_DELAY == 0) ? 1 : 0;
   carbon_retiming_element #(1, INTF_DQS_IN_DELAY) dqs_in_retimer(ck_t, dqs_in_reg, dqs_in_internal);

   assign dqs_t = dqs_oe ? { DM_BITS {  dqs_out }} : { DM_BITS { 1'bz } };
   assign dqs_c = dqs_oe ? { DM_BITS { ~dqs_out }} : { DM_BITS { 1'bz } };
   pullup dqs_c_pullup[DM_BITS-1:0](dqs_c);
   
   reg [DATA_BITS-1:0] 	    dq_out;
   reg 			    dq_oe;
   wire [DATA_BITS-1:0]     dq_in = dq;
   assign dq = dq_oe ? dq_out : { DATA_BITS { 1'bz } };
   

   // ****************************************
   // dq input sampling 

   reg [DATA_BITS-1:0] 	    temp;
   reg [DATA_BITS-1:0] 	    sdr_dq1_out_internal;
   reg [DATA_BITS-1:0] 	    sdr_dq2_out_internal;
   reg 			    sdr_dq_valid_out_internal;
   reg [DATA_BITS-1:0] 	    sdr_dq1_out_internal2;
   reg [DATA_BITS-1:0] 	    sdr_dq2_out_internal2;
   reg 			    sdr_dq_valid_out_internal2;
   
   always @(posedge dqs_in_internal)
     temp <= dq_in;
   
   always @(negedge dqs_in_internal) begin
      sdr_dq1_out_internal <= temp;
      sdr_dq2_out_internal <= dq_in;
      sdr_dq_valid_out_internal <= 1;
   end
   

   always @(posedge ck_t) begin
      sdr_dq1_out_internal2 <= sdr_dq1_out_internal;
      sdr_dq2_out_internal2 <= sdr_dq2_out_internal;
      sdr_dq_valid_out_internal2 <= sdr_dq_valid_out_internal;

      sdr_dq_valid_out_internal <= 0;
   end
   
   always @(negedge ck_t) begin
      sdr_dq_valid_out <= sdr_dq_valid_out_internal2;
      if (sdr_dq_valid_out_internal2) begin
	 sdr_dq1_out <= sdr_dq1_out_internal2;
	 sdr_dq2_out <= sdr_dq2_out_internal2;
      end
   end
   
   // ****************************************
   // dq/dqs output generation

   // sample valid from sdr interface and generate dqs_out_internal and dq_out
   reg [DATA_BITS-1:0] sdr_dq1_in_d;
   reg [DATA_BITS-1:0] sdr_dq2_in_d;
   reg [DM_BITS-1:0] sdr_dm1_in_d;
   reg [DM_BITS-1:0] sdr_dm2_in_d;
   always @(posedge ck_t) begin
      sdr_dq1_in_d <= sdr_dq1_in;
      sdr_dq2_in_d <= sdr_dq2_in;
      sdr_dm1_in_d <= sdr_dm1_in;
      sdr_dm2_in_d <= sdr_dm2_in;
   end

   always @(posedge ck_t)
     dqs_oe_internal <= sdr_dq_valid_in;
   
   always @(posedge ck_t)
     dqs_preamble <= sdr_preamble & write_preamble_enabled;

   always @(*)
     dqs_out_internal <= dqs_clk & (dqs_oe_internal | dqs_preamble);

   always @(*)
     dq_out <= dqs_oe_internal ? (dqs_clk ? sdr_dq1_in_d:sdr_dq2_in_d) : 0;

   always @(*)
     dq_oe <= dqs_oe_internal;
   
   always @(*)
     dm <= dqs_oe_internal ? (dqs_clk ? sdr_dm1_in_d:sdr_dm2_in_d) : 0;

endmodule // carbon_ddr3_interface
