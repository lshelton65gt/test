`timescale 1ps / 1ps

module carbon_ddrx_interface(
   // Outputs
   cke, cs_n, ras_n, cas_n, we_n, ba, addr, dm, sdr_dq1_out,
   sdr_dq2_out, sdr_dq_valid_out,
   // Inouts
   dqs, dq,
   // Inputs
`ifdef CARBON_DDR3_MODEL
   sdr_preamble,
`endif
   ck, sdr_cke, sdr_cs_n, sdr_ras_n, sdr_cas_n, sdr_we_n, sdr_ba,
   sdr_addr, sdr_dq_valid_in, sdr_dq1_in, sdr_dq2_in, sdr_dm1_in,
   sdr_dm2_in
   );

   // number of chip selects (ranks) and data width
   parameter CS_BITS = 4;  // determines number of cs_n and cke pins
   parameter DATA_BITS = 4;  // determines data bit width and number of DM and DQS pins (1 per byte)
   
   // cycle accurate timing parameters
   
   // DQS_IN_DELAY: default assumes EARLY DQS input where DQS is high during first data transfer so it must
   // be delayed by 1/2 cycle to convert it to LATE DQS so it can be directly used as a clock
   // For "normal" JEDEC timing where DQS is already centered on the data, set DQS_IN_DELAY = 0
   parameter DQS_IN_DELAY = 1;

   // DQS_OUT_DELAY: default generates EARLY DQS timing which is the JEDEC standard.  To allow DQS to capture
   // data directly without further modification, it can be converted to LATE DQS timing by setting DQS_OUT_DELAY = 1
   parameter DQS_OUT_DELAY = 0;
   
   // calculated parameter
   parameter DM_BITS = ((DATA_BITS % 8) == 0 ) ? (DATA_BITS / 8) : (DATA_BITS / 8 ) + 1; 
   
   input                    ck;
   output reg [CS_BITS-1:0] cke;
   output reg [CS_BITS-1:0] cs_n;
   output reg 		    ras_n;
   output reg		    cas_n;
   output reg		    we_n;
   output reg [2:0] 	    ba; // maximum width supported - any unused bit should be tied to 0
   output reg [15:0] 	    addr; // maximum width supported - any unused bit should be tied to 0
   
   output reg [DM_BITS-1:0] dm;
   // from cycle based perspective all dqs strobes must have identical timing 
   // only a single DQS signal is use internally - vector form provided for convenience
   inout [DM_BITS-1:0] 	    dqs;
   inout [DATA_BITS-1:0]    dq; 
   
   // single data rate interface
   input [CS_BITS-1:0] 	    sdr_cke;
   input [CS_BITS-1:0] 	    sdr_cs_n;
   input 		    sdr_ras_n;
   input 		    sdr_cas_n;
   input 		    sdr_we_n;
   input [2:0] 		    sdr_ba;
   input [15:0] 	    sdr_addr;
`ifdef CARBON_DDR3_MODEL
   // preamble signal is only needed for DDR3 where dqs is driven an extra cycle early
   input                    sdr_preamble;
`endif   
   input 		    sdr_dq_valid_in;
   input [DATA_BITS-1:0]    sdr_dq1_in;
   input [DATA_BITS-1:0]    sdr_dq2_in;
   input [DM_BITS-1:0] 	    sdr_dm1_in;
   input [DM_BITS-1:0] 	    sdr_dm2_in;
   
   output reg [DATA_BITS-1:0] sdr_dq1_out;
   output reg [DATA_BITS-1:0] sdr_dq2_out;
   output reg 		      sdr_dq_valid_out;
   

   // ********************************************************************************
   // ********************************************************************************

   // ****************************************
   // general signal timing (always posedge sampled same in both domains)
   // sample on posedge but launch on negedge 

   reg [CS_BITS-1:0] 		   sdr_cke_d;
   reg [CS_BITS-1:0] 		   sdr_cs_n_d;
   reg 				   sdr_ras_n_d;
   reg 				   sdr_cas_n_d;
   reg 				   sdr_we_n_d;
   reg [2:0] 			   sdr_ba_d; // max width supported
   reg [15:0] 			   sdr_addr_d;

   always @(posedge ck)
     begin
	sdr_cke_d <= sdr_cke;
	sdr_cs_n_d <= sdr_cs_n;
	sdr_ras_n_d <= sdr_ras_n;
	sdr_cas_n_d <= sdr_cas_n;
	sdr_we_n_d <= sdr_we_n;
	sdr_ba_d <= sdr_ba;
	sdr_addr_d <= sdr_addr;
     end

   always @(negedge ck)
     begin
	cke <= sdr_cke_d;
	cs_n <= sdr_cs_n_d;
	ras_n <= sdr_ras_n_d;
	cas_n <= sdr_cas_n_d;
	we_n <= sdr_we_n_d;
	ba <= sdr_ba_d;
	addr <= sdr_addr_d;
     end

   // ********************************************************************************
   // ********************************************************************************
   // Bidirectional signal generation and timing modification
   
   wire 		    dqs_out;
   reg 			    dqs_out_internal;

   parameter INTF_DQS_OUT_DELAY = (DQS_IN_DELAY == 0) ? 1 : 0;
   carbon_retiming_element #(1, INTF_DQS_OUT_DELAY) dqs_out_retimer(ck, dqs_out_internal, dqs_out);
   
   wire 		    dqs_oe;
   reg 			    dqs_oe_internal;
`ifdef CARBON_DDR3_MODEL
   reg 			    dqs_preamble;
   assign dqs_oe = dqs_oe_internal | dqs_preamble; // dqs oe timing should not change, even though dqs out timing does by 1/2 cycle
`else
   assign dqs_oe = dqs_oe_internal; // dqs oe timing should not change, even though dqs out timing does by 1/2 cycle
`endif
   
   // NOTE:  Only a single dqs signal is used internally!
   // general form for generating internal write dqs (DM should provide final qualification of data)
   wire 		    dqs_in = |dqs & !dqs_oe; // ignore dqs that we generate
   // alternate form using lsb for generating internal write dqs - just use a single bit
   // wire 		    dqs_in = dqs[0] & !dqs_oe; // ignore dqs that we generate
   wire 		    dqs_in_internal;
   parameter INTF_DQS_IN_DELAY = (DQS_OUT_DELAY == 0) ? 1 : 0;
   carbon_retiming_element #(1, INTF_DQS_IN_DELAY) dqs_in_retimer(ck, dqs_in, dqs_in_internal);
   
   assign dqs = dqs_oe ? { DM_BITS { dqs_out }} : { DM_BITS { 1'bz } };
   
   
   reg [DATA_BITS-1:0] 	    dq_out;
   reg 			    dq_oe;
   wire [DATA_BITS-1:0]     dq_in = dq;
   assign dq = dq_oe ? dq_out : { DATA_BITS { 1'bz } };
   

   // ****************************************
   // dq input sampling 

   reg [DATA_BITS-1:0] 	    temp;
   reg [DATA_BITS-1:0] 	    sdr_dq1_out_internal;
   reg [DATA_BITS-1:0] 	    sdr_dq2_out_internal;
   reg 			    sdr_dq_valid_out_internal;
   reg [DATA_BITS-1:0] 	    sdr_dq1_out_internal2;
   reg [DATA_BITS-1:0] 	    sdr_dq2_out_internal2;
   reg 			    sdr_dq_valid_out_internal2;
   
   always @(posedge dqs_in_internal)
     temp <= dq_in;
   
   always @(negedge dqs_in_internal) begin
      sdr_dq1_out_internal <= temp;
      sdr_dq2_out_internal <= dq_in;
      sdr_dq_valid_out_internal <= 1;
   end
   

   always @(posedge ck) begin
      sdr_dq1_out_internal2 <= sdr_dq1_out_internal;
      sdr_dq2_out_internal2 <= sdr_dq2_out_internal;
      sdr_dq_valid_out_internal2 <= sdr_dq_valid_out_internal;

      sdr_dq_valid_out_internal <= 0;
   end
   
   always @(negedge ck) begin
      sdr_dq_valid_out <= sdr_dq_valid_out_internal2;
      if (sdr_dq_valid_out_internal2) begin
	 sdr_dq1_out <= sdr_dq1_out_internal2;
	 sdr_dq2_out <= sdr_dq2_out_internal2;
      end
   end
   
   // ****************************************
   // dq/dqs output generation

   // sample valid from sdr interface and generate dqs_out_internal and dq_out
   reg [DATA_BITS-1:0] sdr_dq1_in_d;
   reg [DATA_BITS-1:0] sdr_dq2_in_d;
   reg [DM_BITS-1:0] sdr_dm1_in_d;
   reg [DM_BITS-1:0] sdr_dm2_in_d;
   always @(posedge ck) begin
      sdr_dq1_in_d <= sdr_dq1_in;
      sdr_dq2_in_d <= sdr_dq2_in;
      sdr_dm1_in_d <= sdr_dm1_in;
      sdr_dm2_in_d <= sdr_dm2_in;
   end

   always @(posedge ck)
     dqs_oe_internal <= sdr_dq_valid_in;

`ifdef CARBON_DDR3_MODEL
   always @(posedge ck)
     dqs_preamble <= sdr_preamble;

   always @(*)
     dqs_out_internal <= ck & (dqs_oe_internal | dqs_preamble);
`else
   always @(*)
     dqs_out_internal <= ck & dqs_oe_internal;
`endif
   
   always @(*)
     dq_out <= dqs_oe_internal ? (ck ? sdr_dq1_in_d:sdr_dq2_in_d) : 0;

   always @(*)
     dq_oe <= dqs_oe_internal;
   
   always @(*)
     dm <= dqs_oe_internal ? (ck ? sdr_dm1_in_d:sdr_dm2_in_d) : 0;

endmodule // carbon_ddr3_interface
