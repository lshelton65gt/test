`timescale 1ps / 1ps

module carbon_memory_ddr_x2_socd_wrapper(
   // Outputs
   sdr_dq_valid_out, sdr_dq2_out, sdr_dq1_out,
   // Inputs
   sdr_we_n, sdr_ras_n, sdr_dq_valid_in, sdr_dq2_in, sdr_dq1_in,
   sdr_dm2_in, sdr_dm1_in, sdr_cs_n, sdr_cke, sdr_cas_n, sdr_ba,
   sdr_addr,
   sdr_preamble,
   sdr_reset_n,
   ck
   );

   // ********************************************************************************
   // PARAMETER CALCULATIONS REPLICATED FROM carbon_ddrx_sdram MODULE
   //

   // number of chip selects (ranks) and data width
   parameter CS_BITS = 1; //4;  // determines number of cs_n and cke pins
   parameter DATA_BITS = 8; // 4;  // determines data bit width and number of DM and DQS pins (1 per byte) *** PER CHIP ***
   parameter MAX_ADDR_BITS=20;
   
   // cycle accurate timing parameters
   
   // DQS_IN_DELAY: default assumes EARLY DQS input where DQS is high during first data transfer so it must
   // be delayed by 1/2 cycle to convert it to LATE DQS so it can be directly used as a clock
   // For "normal" JEDEC timing where DQS is already centered on the data, set DQS_IN_DELAY = 0
   parameter DQS_IN_DELAY = 1;

   // DQS_OUT_DELAY: default generates EARLY DQS timing which is the JEDEC standard.  To allow DQS to capture
   // data directly without further modification, it can be converted to LATE DQS timing by setting DQS_OUT_DELAY = 1
   parameter DQS_OUT_DELAY = 0;
   
   // calculated parameter
   parameter DM_BITS = ((DATA_BITS % 8) == 0 ) ? (DATA_BITS / 8) : (DATA_BITS / 8 ) + 1; 
   
   // ********************************************************************************
   
   input ck;
   input sdr_reset_n; // not used in DDR
   input sdr_preamble; // not used in DDR
   input [15:0] sdr_addr;
   input [2:0] sdr_ba;
   input sdr_cas_n;
   input [CS_BITS-1:0] sdr_cke;
   input [CS_BITS-1:0] sdr_cs_n;
   input [DM_BITS*2-1:0] sdr_dm1_in;
   input [DM_BITS*2-1:0] sdr_dm2_in;
   input [DATA_BITS*2-1:0] sdr_dq1_in;
   input [DATA_BITS*2-1:0] sdr_dq2_in;
   input sdr_dq_valid_in;
   input sdr_ras_n;
   input sdr_we_n;

   output [DATA_BITS*2-1:0] sdr_dq1_out;
   output [DATA_BITS*2-1:0] sdr_dq2_out;
   output sdr_dq_valid_out;
   
   // ********************************************************************************

   wire [15:0] addr;
   wire [2:0] ba;
   wire cas_n;
   wire [CS_BITS-1:0] cke;
   wire [CS_BITS-1:0] cs_n;
   wire [DM_BITS*2-1:0] dm;
   wire [DATA_BITS*2-1:0] dq;
   wire [DM_BITS*2-1:0] dqs;
   wire [DM_BITS*2-1:0] dqsbar;
   wire ras_n;
   wire we_n;

   wire ckbar = ~ck;
   
   carbon_memory_ddr #(.CS_BITS(CS_BITS),
                       .DATA_BITS(DATA_BITS),
                       .MAX_ADDR_BITS(MAX_ADDR_BITS),
                       .DQS_IN_DELAY(DQS_IN_DELAY),
                       .DQS_OUT_DELAY(DQS_OUT_DELAY)) mem0(
                                                           // Inouts
                                                           .dqs(dqs[DM_BITS-1:0]),
                                                           .dq(dq[DATA_BITS-1:0]),
                                                           // Inputs
                                                           .ck(ck),
                                                           .ckbar(ckbar),
                                                           .cke(cke[CS_BITS-1:0]),
                                                           .cs_n(cs_n[CS_BITS-1:0]),
                                                           .ras_n(ras_n),
                                                           .cas_n(cas_n),
                                                           .we_n(we_n),
                                                           .ba(ba[2:0]),
                                                           .addr(addr[15:0]),
                                                           .dm(dm[DM_BITS-1:0]));

   carbon_memory_ddr #(.CS_BITS(CS_BITS),
                       .DATA_BITS(DATA_BITS),
                       .MAX_ADDR_BITS(MAX_ADDR_BITS),
                       .DQS_IN_DELAY(DQS_IN_DELAY),
                       .DQS_OUT_DELAY(DQS_OUT_DELAY)) mem1(
                                                           // Inouts
                                                           .dqs(dqs[DM_BITS*2-1:DM_BITS]),
                                                           .dq(dq[DATA_BITS*2-1:DATA_BITS]),
                                                           // Inputs
                                                           .ck(ck),
                                                           .ckbar(ckbar),
                                                           .cke(cke[CS_BITS-1:0]),
                                                           .cs_n(cs_n[CS_BITS-1:0]),
                                                           .ras_n(ras_n),
                                                           .cas_n(cas_n),
                                                           .we_n(we_n),
                                                           .ba(ba[2:0]),
                                                           .addr(addr[15:0]),
                                                           .dm(dm[DM_BITS*2-1:DM_BITS]));

   carbon_ddrx_interface #(.CS_BITS(CS_BITS),
   .DATA_BITS(DATA_BITS*2),
   .DQS_IN_DELAY(DQS_IN_DELAY),
   .DQS_OUT_DELAY(DQS_OUT_DELAY)) intf(
       // Outputs
       .cke(cke[CS_BITS-1:0]),
       .cs_n(cs_n[CS_BITS-1:0]),
       .ras_n(ras_n),
       .cas_n(cas_n),
       .we_n(we_n),
       .ba(ba[2:0]),
       .addr(addr[15:0]),
       .dm(dm[DM_BITS*2-1:0]),
       .sdr_dq1_out(sdr_dq1_out[DATA_BITS*2-1:0]),
       .sdr_dq2_out(sdr_dq2_out[DATA_BITS*2-1:0]),
       .sdr_dq_valid_out(sdr_dq_valid_out),
       // Inouts
       .dqs(dqs[DM_BITS*2-1:0]),
       .dq(dq[DATA_BITS*2-1:0]),
       // Inputs
       .ck(ck),
       .sdr_cke(sdr_cke[CS_BITS-1:0]),
       .sdr_cs_n(sdr_cs_n[CS_BITS-1:0]),
       .sdr_ras_n(sdr_ras_n),
       .sdr_cas_n(sdr_cas_n),
       .sdr_we_n(sdr_we_n),
       .sdr_ba(sdr_ba[2:0]),
       .sdr_addr(sdr_addr[15:0]),
       .sdr_dq_valid_in(sdr_dq_valid_in),
       .sdr_dq1_in(sdr_dq1_in[DATA_BITS*2-1:0]),
       .sdr_dq2_in(sdr_dq2_in[DATA_BITS*2-1:0]),
       .sdr_dm1_in(sdr_dm1_in[DM_BITS*2-1:0]),
       .sdr_dm2_in(sdr_dm2_in[DM_BITS*2-1:0]));
      
endmodule
