int passed = 0;
int failed = 0;

int a_pe_val = 0;
int a_ne_val = 0;
int abi_n_pe_val = 0;
int abi_n_ne_val = 0;
int gddr5_value = 0;

int abi_enabled = 0;
int abi_active = 0;
int wdbi_enabled = 0;
int wdbi_active = 0;
int wdbi_mask = 0;
int rdbi_enabled = 0;
int rdbi_active = 0;
int rdbi_mask = 0;
int byteVar = 0;
int loopVar = 0;
int byteCnt = 0;
int loopCnt = 0;
int device = 0;

#define TESTER "TESTER"

#define CHECK_VALUE(msg, value, expected) \
if (value != expected) { \
failed++; \
message("INFO", "*** ERROR : Expected 0x%x, Actual 0x%x : %s ***", expected, value, msg); \
} else { \
passed++; \
message("INFO", "    PASSED: Expected 0x%x, Actual 0x%x : %s", expected, value, msg); \
}

#define FINAL_RESULTS \
if (failed == 0) { \
  message("INFO", "TEST PASSED with 0 Errors"); \
} else { \
  message("INFO", "TEST FAILED with %d Errors", failed); \
}

// ********************************************************************************
// Helper macros to set/get the values for a single device on the overall signal bus

// Signals where there is only one per device
#define SET_DEV_SLICE(device, signal, value) \
gddr5_value = CADIRegRead(TESTER, signal); \
gddr5_value = gddr5_value & ~(1 << device); \
gddr5_value = gddr5_value | (value << device); \
CADIRegWrite(TESTER, signal, gddr5_value);

#define GET_DEV_SLICE(device, signal, value) \
gddr5_value = CADIRegRead(TESTER, signal); \
gddr5_value = (gddr5_value & (1 << device)) >> device;


// Signals where there are 12 per device
#define SET_DEV_ADDR_SLICE(device, signal, value) \
gddr5_value = CADIRegRead(TESTER, signal); \
gddr5_value = gddr5_value & ~((0xfff) << (device*12)); \
gddr5_value = gddr5_value | (((0xfff) & value) << (device*12)); \
CADIRegWrite(TESTER, signal, gddr5_value);

#define GET_DEV_ADDR_SLICE(device, signal, value) \
gddr5_value = CADIRegRead(TESTER, signal); \
gddr5_value = (gddr5_value & ((0xfff) << (device*12))) >> (device*12);


// Signals where there are DEVICE_BYTE_LANES per device
#define SET_DEV_BYTE_SLICE(device, signal, value) \
gddr5_value = CADIRegRead(TESTER, signal); \
gddr5_value = gddr5_value & ~(((1 << DEVICE_BYTE_LANES)-1) << (device*DEVICE_BYTE_LANES)); \
gddr5_value = gddr5_value | ((((1 << DEVICE_BYTE_LANES)-1) & value) << (device*DEVICE_BYTE_LANES)); \
CADIRegWrite(TESTER, signal, gddr5_value); 

#define GET_DEV_BYTE_SLICE(device, signal, value) \
gddr5_value = CADIRegRead(TESTER, signal); \
gddr5_value = (gddr5_value & (((1 << DEVICE_BYTE_LANES)-1) << (device*DEVICE_BYTE_LANES))) >> (device*DEVICE_BYTE_LANES);


// Signals where there are 8*DEVICE_BYTE_LANES per device
#define SET_DEV_DATA_SLICE(device, signal, value) \
gddr5_value = CADIRegRead(TESTER, signal); \
gddr5_value = gddr5_value & ~(((1 << 8*DEVICE_BYTE_LANES)-1) << (device*DEVICE_BYTE_LANES*8)); \
gddr5_value = gddr5_value | ((((1 << 8*DEVICE_BYTE_LANES)-1) & value) << (device*DEVICE_BYTE_LANES*8)); \
CADIRegWrite(TESTER, signal, gddr5_value);

#define GET_DEV_DATA_SLICE(device, signal, value) \
gddr5_value = CADIRegRead(TESTER, signal); \
gddr5_value = gddr5_value & ((((1 << 8*DEVICE_BYTE_LANES)-1) << (device*DEVICE_BYTE_LANES*8)) >> (device*DEVICE_BYTE_LANES*8)); 

// ********************************************************************************

#define DUT_SET_ABI_ENABLE(value) \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
SET_ABI_ENABLE(device, value); \
}

#define SET_ABI_ENABLE(devNum, value) \
if (value != 0 ) { \
abi_enabled |= (value << devNum); \
} else { \
abi_enabled &= ~(value << devNum); \
}

#define DUT_SET_WDBI_ENABLE(value) \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
SET_WDBI_ENABLE(device, value); \
}

#define SET_WDBI_ENABLE(devNum, value) \
if (value != 0) { \
wdbi_enabled |= (value << devNum); \
} else { \
wdbi_enabled &= ~(value << devNum); \
}

#define DUT_SET_RDBI_ENABLE(value) \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
SET_RDBI_ENABLE(device, value); \
}

#define SET_RDBI_ENABLE(devNum, value) \
if (value != 0) { \
rdbi_enabled |= (value << devNum); \
} else { \
wdbi_enabled &= ~(value << devNum); \
}

#define CALC_ABI(devNum, value) \
abi_active = 0; \
if ((abi_enabled & (1 << devNum)) != 0) { \
loopCnt = 0; \
for (loopVar = 0; loopVar < 9; loopVar = loopVar + 1) { \
if ((value & (1 << loopVar)) == 0) { loopCnt++; } \
} \
if (loopCnt > 4) { \
abi_active = 1; \
}\
}\

#define CALC_WDBI(devNum, value) \
wdbi_active = 0; \
wdbi_mask = 0; \
if ((wdbi_enabled & (1 << devNum)) != 0) { \
  for (byteVar = 0; byteVar < DEVICE_BYTE_LANES; byteVar++) { \
    loopCnt = 0; \
    for (loopVar = 0; loopVar < 8; loopVar++) { \
      if ((value & (1 << (loopVar + (8 * byteVar)))) == 0) { loopCnt++; } \
    } \
    if (loopCnt > 4) { \
      wdbi_active |= (1 << byteVar); \
      wdbi_mask |= (0xff << (8*byteVar)); \
    } \
  } \
} 

#define EXECUTE(n) \
step(2*n);

// Set the tester outputs to know states that represent a disabled condition (CKE is not asserted)

#define DUT_DISABLE \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
DEV_DISABLE(device); \
}

#define DEV_DISABLE(devNum) \
SET_DEV_SLICE(devNum, "t_reset_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cke", 0x0); \
SET_DEV_SLICE(devNum, "t_sdr_cs_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cas_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_ras_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_we_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_ne", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_pe", 0x1); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_ne", 0x0); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_pe", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq1_dbi_n_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq2_dbi_n_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq3_dbi_n_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq4_dbi_n_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq1_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq2_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq3_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq4_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq_valid_in", 0x0); 

// Set the tester outputs to know states that represent enabled, but reset condition

#define DUT_RESET \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
DEV_RESET(device); \
}

#define DEV_RESET(devNum) \
SET_DEV_SLICE(devNum, "t_reset_n", 0x0); \
SET_DEV_SLICE(devNum, "t_sdr_cke", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cs_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cas_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_ras_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_we_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_ne", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_pe", 0x1); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_ne", 0x0); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_pe", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq1_dbi_n_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq2_dbi_n_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq3_dbi_n_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq4_dbi_n_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq1_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq2_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq3_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq4_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq_valid_in", 0x0); 

// Drive Write Data to the DUT - abi settings should be auto calculated, but always zero for now
// Note that other control signals are not driven by this macro and it does NOT advance time
// This allows the data to be driven independently of a command being issued, to allow true pipelining
// It also means it must be used in conjunction with another command to advance the time.

#define DUT_DRIVE_WRDATA(val1, val2, val3, val4) \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
DEV_DRIVE_WRDATA(device, \
                 val1 >> (8*DEVICE_BYTE_LANES*device), \
                 val2 >> (8*DEVICE_BYTE_LANES*device), \
                 val3 >> (8*DEVICE_BYTE_LANES*device), \
                 val4 >> (8*DEVICE_BYTE_LANES*device)); \
}

#define DEV_DRIVE_WRDATA(devNum, val1, val2, val3, val4) \
CALC_WDBI(devNum, val1); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq1_dbi_n_in", ~wdbi_active); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq1_in", val1 ^ wdbi_mask); \
CALC_WDBI(devNum, val2); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq2_dbi_n_in", ~wdbi_active); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq2_in", val2 ^ wdbi_mask); \
CALC_WDBI(devNum, val3); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq3_dbi_n_in", ~wdbi_active); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq3_in", val3 ^ wdbi_mask); \
CALC_WDBI(devNum, val4); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq4_dbi_n_in", ~wdbi_active); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq4_in", val4 ^ wdbi_mask); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq_valid_in", 0xffffffffffff); 


// Set the tester outputs to know states that represent enabled, but idle condition

#define DUT_IDLE \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
DEV_IDLE(device); \
}

#define DEV_IDLE(devNum) \
SET_DEV_SLICE(devNum, "t_reset_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cke", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cs_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cas_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_ras_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_we_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_ne", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_pe", 0x1); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_ne", 0x0); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_pe", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq1_dbi_n_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq2_dbi_n_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq3_dbi_n_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq4_dbi_n_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq1_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq2_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq3_in", 0x0); \
SET_DEV_DATA_SLICE(devNum, "t_sdr_dq4_in", 0x0); \
SET_DEV_BYTE_SLICE(devNum, "t_sdr_dq_valid_in", 0x0); 

// Drive the Mode Register Set Command 
#define DUT_MRS(ba, value) \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
DEV_MRS(device, ba, value); \
}

#define DEV_MRS( devNum, ba, value ) \
a_pe_val = (((ba & 1) >> 0) << 2) | \
	   (((ba & 2) >> 1) << 5) | \
	   (((ba & 4) >> 2) << 4) | \
	   (((ba & 8) >> 3) << 3) | \
	   (((value & 0x0100) >>  8) << 7) | \
	   (((value & 0x0200) >>  9) << 1) | \
	   (((value & 0x0400) >> 10) << 0) | \
	   (((value & 0x0800) >> 11) << 6) | \
	   (((value & 0x1000) >> 12) << 8); \
a_ne_val = value & 0xff; \
CALC_ABI(devNum, a_pe_val); \
if (abi_active != 0) { \
a_pe_val = ~a_pe_val & 0xfff; \
}\
abi_n_pe_val = 1 & ~abi_active; \
CALC_ABI(devNum, a_ne_val); \
if (abi_active != 0) { \
a_ne_val = ~a_ne_val & 0xfff; \
}\
abi_n_ne_val = 1 & ~abi_active; \
SET_DEV_SLICE(devNum, "t_reset_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cke",  0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cs_n",  0x0); \
SET_DEV_SLICE(devNum, "t_sdr_cas_n", 0x0); \
SET_DEV_SLICE(devNum, "t_sdr_ras_n", 0x0); \
SET_DEV_SLICE(devNum, "t_sdr_we_n",  0x0); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_ne", abi_n_ne_val); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_pe", abi_n_pe_val); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_pe", a_pe_val); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_ne", a_ne_val); 


// Drive the ACTIVE Command 

#define DUT_ACTIVE(ba, row) \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
DEV_ACTIVE(device, ba, row); \
}

#define DEV_ACTIVE( devNum, ba, row ) \
a_pe_val = (((ba & 1) >> 0) << 2) | \
	   (((ba & 2) >> 1) << 5) | \
	   (((ba & 4) >> 2) << 4) | \
	   (((ba & 8) >> 3) << 3) | \
	   (((row & 0x0100) >>  8) << 7) | \
	   (((row & 0x0200) >>  9) << 1) | \
	   (((row & 0x0400) >> 10) << 0) | \
	   (((row & 0x0800) >> 11) << 6) | \
	   (((row & 0x1000) >> 12) << 8); \
a_ne_val = row & 0xff; \
CALC_ABI(devNum, a_pe_val); \
if (abi_active != 0) { \
a_pe_val = ~a_pe_val & 0xfff; \
}\
abi_n_pe_val = 1 & ~abi_active; \
CALC_ABI(devNum, a_ne_val); \
if (abi_active != 0) { \
a_ne_val = ~a_ne_val & 0xfff; \
}\
abi_n_ne_val = 1 & ~abi_active; \
SET_DEV_SLICE(devNum, "t_reset_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cke",  0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cs_n",  0x0); \
SET_DEV_SLICE(devNum, "t_sdr_cas_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_ras_n", 0x0); \
SET_DEV_SLICE(devNum, "t_sdr_we_n",  0x1); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_ne", abi_n_ne_val); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_pe", abi_n_pe_val); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_pe", a_pe_val); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_ne", a_ne_val); 


// Drive the RD or RDA Command 
// The ap (auto precharge) setting determines whether RD or RDA is driven
#define DUT_RD(ba, col, ap) \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
DEV_RD(device, ba, col, ap); \
}

#define DEV_RD( devNum, ba, col, ap ) \
a_pe_val = (((ba & 1) >> 0) << 2) | \
	   (((ba & 2) >> 1) << 5) | \
	   (((ba & 4) >> 2) << 4) | \
	   (((ba & 8) >> 3) << 3) | \
	   (ap  << 7); \
a_ne_val = col & 0x7f; \
CALC_ABI(devNum, a_pe_val); \
if (abi_active != 0) { \
a_pe_val = ~a_pe_val & 0xfff; \
}\
abi_n_pe_val = 1 & ~abi_active; \
CALC_ABI(devNum, a_ne_val); \
if (abi_active != 0) { \
a_ne_val = ~a_ne_val & 0xfff; \
}\
abi_n_ne_val = 1 & ~abi_active; \
SET_DEV_SLICE(devNum, "t_reset_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cke",  0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cs_n",  0x0); \
SET_DEV_SLICE(devNum, "t_sdr_cas_n", 0x0); \
SET_DEV_SLICE(devNum, "t_sdr_ras_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_we_n",  0x1); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_ne", abi_n_ne_val); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_pe", abi_n_pe_val); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_pe", a_pe_val); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_ne", a_ne_val); 


// Drive the WOM or WOMA Command 
// The ap (auto precharge) setting determines whether WOM or WOMA is driven
#define DUT_WOM(ba, col, ap) \
for (device = 0; device < DEVICES_PER_RANK; device++) { \
DEV_WOM(device, ba, col, ap); \
}

#define DEV_WOM( devNum, ba, col, ap ) \
a_pe_val = (((ba & 1) >> 0) << 2) | \
	   (((ba & 2) >> 1) << 5) | \
	   (((ba & 4) >> 2) << 4) | \
	   (((ba & 8) >> 3) << 3) | \
	   (ap  << 7); \
a_ne_val = col & 0x7f; \
CALC_ABI(devNum, a_pe_val); \
if (abi_active != 0) { \
a_pe_val = ~a_pe_val & 0xfff; \
}\
abi_n_pe_val = 1 & ~abi_active; \
CALC_ABI(devNum, a_ne_val); \
if (abi_active != 0) { \
a_ne_val = ~a_ne_val & 0xfff; \
}\
abi_n_ne_val = 1 & ~abi_active; \
SET_DEV_SLICE(devNum, "t_reset_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cke",  0x1); \
SET_DEV_SLICE(devNum, "t_sdr_cs_n",  0x0); \
SET_DEV_SLICE(devNum, "t_sdr_cas_n", 0x0); \
SET_DEV_SLICE(devNum, "t_sdr_ras_n", 0x1); \
SET_DEV_SLICE(devNum, "t_sdr_we_n",  0x0); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_ne", abi_n_ne_val); \
SET_DEV_SLICE(devNum, "t_sdr_abi_n_pe", abi_n_pe_val); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_pe", a_pe_val); \
SET_DEV_ADDR_SLICE(devNum, "t_sdr_a_ne", a_ne_val); 


// ***************
// IDLE(10);	
// step(200);
// int i;
// for ( i = 0; i < 16; i++) {
//   MRS (i, 0x0101 * i, 1, 1);
//   IDLE(1);
// }
// MRS( 1, 0x400, 1, 1);
// for ( i = 0; i < 16; i++) {
//   MRS (i, 0x0101 * i, 0, 0);
//   IDLE(1);
// }
// step(100);
// // Clear the Mode Registers
// for ( i = 0; i < 16; i++) {
//   MRS (i, 0x0, 1, 1);
//   IDLE(1);
// }
// MRS (0, 0x1, 1, 1);  // set write latency to 1
// step(100);
// for ( i = 0; i < 16; i++) {
//   ACTIVE (i, 0x010 * i, 1, 1);
//   IDLE(1);
// }
// step(100);
// for ( i = 0; i < 16; i++) {
//   WOM (i, 0x100+(i<<3), 1, 1, 0);
//   IDLE(1);
//   DRIVE_WRDATA(i, i+0x100, i+0x200, i+0x300);
//   DRIVE_WRDATA(i+0x400, i+0x500, i+0x600, i+0x700);
//   IDLE(10);
// }
// step(100);
// for ( i = 0; i < 16; i++) {
//   RD (i, 0x100+(i<<3), 1, 1, 0);
//   IDLE(10);
// }
// step(100);
// for ( i = 0; i < 16; i++) {
//   RD (i, 0x100+(i<<3), 1, 1, 1);
//   IDLE(10);
// }

