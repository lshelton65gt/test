openSystem("./TestSys.mxp");
setParameter("DUT", "Waveform Format", "FSDB");
// setParameter("DUT", "Dump Waveforms", "true");
setParameter("TESTER", "Waveform Format", "FSDB");
// setParameter("TESTER", "Dump Waveforms", "true");
#include "./parameters.h"
#include "../../gddr5_macros.h"

int devNum;
int value;
int test_pattern = 0;

int abi = 0;
int wrLat;
int rdLat;
int ba = 0;
int row = 0;
int col = 0;
int ap = 1;
int i, j, addr, cadi_addr;
string msg;
int colOffset;
int blockAddr;

int x, y, vmask;

#define APPLY_VALUE_MASK(devNum) \
vmask = 0; \
for ( x = 0; x < DEVICE_BYTE_LANES; x++ ) { \
  vmask |= 0xff << (8*x); \
} \
vmask = vmask << (8*DEVICE_BYTE_LANES*devNum); \
value = value & vmask;

string regName;

// ********************************************************************************
// ********************************************************************************
// MRS command testing
// ********************************************************************************
// ********************************************************************************

// Since MxScript limits registers/memory width to 64 or less, limit the number of devices that we test 
int testDevices = 8/DEVICE_BYTE_LANES;
if (testDevices > DEVICES_PER_RANK) {
  testDevices = DEVICES_PER_RANK;
}

for ( devNum = 0; devNum < testDevices; devNum++ ) {
  
  DEV_IDLE(devNum);	
  EXECUTE(200);
  int i;
  // Perform basic Mode Register writes and check CADI reads 
  // (can't really test CADI Writes as there is no frontdoor read)
  for ( i = 0; i < 16; i++) {
    
    if (DEVICES_PER_RANK > 1) {
      regName = "CS0 Device " + (string)(devNum) + " MR" + (string)(i);
    } else {
      regName = "CS0 MR" + (string)(i);
    }


    test_pattern = 0xaaa;  // register are only 12 bits
    DEV_MRS (devNum, i, test_pattern); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(10);
    value = CADIRegRead("DUT.gddr5", regName); 

    if (i == 1) {
      // MRS1 has an autoclear bit in bit[11]
      test_pattern = test_pattern & 0x7ff;
    }
    CHECK_VALUE("CADI Reg Read: "+regName, value, test_pattern);

    test_pattern = 0x555;
    DEV_MRS (devNum, i, test_pattern); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(10);
    value = CADIRegRead("DUT.gddr5", regName); 
    CHECK_VALUE("CADI Reg Read: "+regName, value, test_pattern);

    test_pattern = 0x000;
    DEV_MRS (devNum, i, test_pattern); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(10);
    value = CADIRegRead("DUT.gddr5", regName); 
    CHECK_VALUE("CADI Reg Read: "+regName, value, test_pattern);
  }
  DEV_IDLE(devNum); EXECUTE(100);
}


// ********************************************************************************
// ********************************************************************************
// WOM (write) command testing
// ********************************************************************************
// ********************************************************************************

for ( wrLat = 1; wrLat < 8; wrLat++ ) {
  
  for ( devNum = 0; devNum < testDevices; devNum++ ) {
    
    DEV_IDLE(devNum);	
    EXECUTE(200);

    // Set Write Latency in mode register

    DEV_MRS (devNum, 0, wrLat & 0x7); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(10);

    // Activate bank

    DEV_ACTIVE(devNum, ba, row); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(10);

    // Send WOM command

    DEV_WOM(devNum, ba, col, ap); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(wrLat);  // Idle for the Write Latency

    
    // Drive data sequence that can be examined via CADI reads
    DEV_DRIVE_WRDATA(devNum, 0x1+devNum, 0x2+devNum, 0x3+devNum, 0x4+devNum); EXECUTE(1);
    DEV_DRIVE_WRDATA(devNum, 0x5+devNum, 0x6+devNum, 0x7+devNum, 0x8+devNum); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(10);

    for (i = 1; i < 9; i++) {
      addr = DEVICE_BYTE_LANES*devNum + DEVICES_PER_RANK*DEVICE_BYTE_LANES*(i-1);
      
      // Perform CADI Read to check data (this is a byte read)
      value = CADIMemRead("TESTER", "Memory", addr);

      sprintf(msg, "CADI Mem Read: Address 0x%x (device %d)", addr, devNum); 
      CHECK_VALUE(msg, value, (i+devNum));

      // clear value to continue testing
      CADIMemWrite("TESTER", "Memory", addr, 0);

    }
  }
  DEV_IDLE(devNum); EXECUTE(100);
}

// ********************************************************************************
// ********************************************************************************
// READ command and ABI/WDBI/RDBI testing
// ********************************************************************************
// ********************************************************************************
wrLat = 1;
for (abi = 0; abi < 2; abi++) {
  message("INFO", "=== Setting ABI = %d ===", abi);
for ( rdLat = 0; rdLat < 32; rdLat++ ) {

  message("INFO", "=== Setting rdLat = %d ===", rdLat);
  
  for ( devNum = 0; devNum < testDevices; devNum++ ) {
    
    DEV_IDLE(devNum);	
    EXECUTE(200);

    // Set Write and Read Latency in mode register

    DEV_MRS (devNum, 0, (((rdLat & 0xf) << 3) | (wrLat & 0x7))); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(10);

    // Set CAS (read) Latency Extra High Frequency in mode register 8
    DEV_MRS (devNum, 8, ((rdLat & 0x10) >> 4)); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(10);

    if (abi != 0) {
      // Set the ABI, WDBI, and RDBI bits in MR1
      DEV_MRS (devNum, 1, 0x700); EXECUTE(1);
      DEV_IDLE(devNum); EXECUTE(10);
      // Signal the test macros the ABI and WDBI modes are in use
      SET_ABI_ENABLE(devNum, 1);
      SET_WDBI_ENABLE(devNum, 1);
    }

    // Activate bank for write/read

    DEV_ACTIVE(devNum, ba, row); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(10);

    // Send WOM command

    DEV_WOM(devNum, ba, col, 0); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(wrLat);  // Idle for the Write Latency

    
    // Drive data sequence that can be examined via CADI reads
    DEV_DRIVE_WRDATA(devNum, 0x1+devNum, 0x2+devNum, 0x3+devNum, 0x4+devNum); EXECUTE(1);
    DEV_DRIVE_WRDATA(devNum, 0x5+devNum, 0x6+devNum, 0x7+devNum, 0x8+devNum); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(10);

    // Using CADI, check the values written
    for (i = 1; i < 9; i++) {
      cadi_addr = DEVICE_BYTE_LANES*devNum + DEVICES_PER_RANK*DEVICE_BYTE_LANES*(i-1);
      
      for (j = 0; j < DEVICE_BYTE_LANES; j++) {
	// Perform CADI Read to check data (this is a byte read)
	value = CADIMemRead("TESTER", "Memory", cadi_addr);
	
	sprintf(msg, "CADI Mem Read: Address 0x%x (device %d)", cadi_addr, devNum); 
	if (j == 0) {
	  CHECK_VALUE(msg, value, (i+devNum));
	} else {
	  CHECK_VALUE(msg, value, 0);
	}
	cadi_addr++;
      }
    }

    // Send READ command

    DEV_RD(devNum, ba, col, 0); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(rdLat+5+13);  // Idle for the Read Latency (actual value is setting + 5), 13 clocks for cmd delay out and data delay back into tester registers

    // Check the test data registers
    for (i = 1; i < 5; i++) {
      value = CADIRegRead("TESTER", "t_sdr_dq"+(string)(i)+"_out");
      // Since this is the entire bus, mask off the results so only this device is checked
      APPLY_VALUE_MASK(devNum);

      sprintf(msg, "Frontdoor Mem Read 0x%x (device %d)", 0, devNum);
      CHECK_VALUE(msg, value, ((i+devNum) << (8*DEVICE_BYTE_LANES*devNum)));
    }

    // Get the next data set
    EXECUTE(1);

    // Check the test data registers
    for (i = 1; i < 5; i++) {
      value = CADIRegRead("TESTER", "t_sdr_dq"+(string)(i)+"_out");
      // Since this is the entire bus, mask off the results so only this device is checked
      APPLY_VALUE_MASK(devNum);

      sprintf(msg, "Frontdoor Mem Read 0x%x (device %d)", 0, devNum);
      CHECK_VALUE(msg, value, ((i+devNum+4) << (8*DEVICE_BYTE_LANES*devNum)));
    }
    
    for (i = 1; i < 9; i++) {
      cadi_addr = DEVICE_BYTE_LANES*devNum + DEVICES_PER_RANK*DEVICE_BYTE_LANES*(i-1);
      
      // clear value to continue testing
      CADIMemWrite("TESTER", "Memory", cadi_addr, 0);
    }

    // Now reread them (frontdoor) and check they have been zeroed

    // Send READ command

    DEV_RD(devNum, ba, col, 1); EXECUTE(1);
    DEV_IDLE(devNum); EXECUTE(rdLat+5+13);  // Idle for the Read Latency (actual value is setting + 5), 13 clocks for cmd delay out and data delay back into tester registers

    // Check the test data registers
    for (i = 1; i < 5; i++) {
      value = CADIRegRead("TESTER", "t_sdr_dq"+(string)(i)+"_out");
      // Since this is the entire bus, mask off the results so only this device is checked
      APPLY_VALUE_MASK(devNum);

      sprintf(msg, "Frontdoor Mem Read 0x%x (device %d)", 0, devNum);
      CHECK_VALUE(msg, value, 0);
    }

    // Get the next data set
    EXECUTE(1);

    // Check the test data registers
    for (i = 1; i < 5; i++) {
      value = CADIRegRead("TESTER", "t_sdr_dq"+(string)(i)+"_out");
      // Since this is the entire bus, mask off the results so only this device is checked
      APPLY_VALUE_MASK(devNum);
      CHECK_VALUE(msg, value, 0);
    }
    
  }
  DEV_IDLE(devNum); EXECUTE(100);
}
}

// ********************************************************************************
// ********************************************************************************
// Write/Read Burst Wrap Testing for full bus width
// ********************************************************************************
// ********************************************************************************

wrLat = 1;
rdLat = 0;
abi = 0;

// Set Write and Read Latency in mode register
 
DUT_MRS(0, (((rdLat & 0xf) << 3) | (wrLat & 0x7))); EXECUTE(1);
 
DUT_IDLE; EXECUTE(10);
 
// Set CAS (read) Latency Extra High Frequency in mode register 8
DUT_MRS(8, ((rdLat & 0x10) >> 4)); EXECUTE(1);
 
DUT_IDLE; EXECUTE(10);
 
// Leave the ABI/RDBI/WDBI settings as-is - This means they are all turned on so this will check the CADI access of these as well

for (colOffset = 0; colOffset < 8; colOffset++) {
  message("INFO", "=== Burst Column Offset = %d ===", colOffset);
  // Perform burst access with the given offset to make sure it wraps appropriately
  DUT_IDLE; EXECUTE(200);

  // Activate bank for write/read
  
  DUT_ACTIVE(ba, row); EXECUTE(1);

  DUT_IDLE; EXECUTE(10);

  // Send WOM command
  
  DUT_WOM(ba, col + colOffset, 0); EXECUTE(1);
  DUT_IDLE; EXECUTE(wrLat);  // Idle for the Write Latency
  
  // Drive data sequence that can be examined via CADI reads
  DUT_DRIVE_WRDATA(0xffffffffffffffaa, 0xffffffffffffaaff, 0xffffffffffaaffff, 0xffffffffaaffffff); EXECUTE(1);
  DUT_DRIVE_WRDATA(0xffffffaaffffffff, 0xffffaaffffffffff, 0xffaaffffffffffff, 0xaaffffffffffffff); EXECUTE(1);
  DUT_IDLE; EXECUTE(15);

  // for every word in the burst
  for (j = 0; j < 8; j++) {
    // for every byte in the word
    for (i = 0; i < DEVICES_PER_RANK*DEVICE_BYTE_LANES; i++) {

      // Using CADI, check the values written for word width (while wrapping on burst boundaries)
      cadi_addr = col + (DEVICES_PER_RANK*DEVICE_BYTE_LANES)*((colOffset+j) & 0x7) + i;

      // Perform CADI Read to check data (this is a byte read)
      value = CADIMemRead("TESTER", "Memory", cadi_addr);
      
      sprintf(msg, "CADI Mem Read: Address 0x%x", cadi_addr); 
      if (j == i) {
	CHECK_VALUE(msg, value, 0xaa);
      } else {
	if (i > 8) {
	  // MxScript is limited to 64 bits (8 bytes) so anything above that should still be zero
	  CHECK_VALUE(msg, value, 0x0);
	} else {
	  CHECK_VALUE(msg, value, 0xff);
	}
      }
    }
  }
  
  // Send READ command
  
  DUT_RD( ba, col + colOffset, 1 ); EXECUTE(1);
  DUT_IDLE; EXECUTE(rdLat+5+13);  // Idle for the Read Latency (actual value is setting + 5), 13 clocks for cmd delay out and data delay back into tester registers
  
  // Check the test data registers
  for (i = 1; i < 5; i++) {
    value = CADIRegRead("TESTER", "t_sdr_dq"+(string)(i)+"_out");
    test_pattern = ~(0x55 << 8*(i - 1)) & ((1 << (8*DEVICE_BYTE_LANES*DEVICES_PER_RANK))-1);
    sprintf(msg, "A: Frontdoor Mem Read 0x%x", col + colOffset );
    CHECK_VALUE(msg, value, test_pattern);
  }
  
  // Get the next data set
  EXECUTE(1);
  
  // Check the test data registers
  for (i = 1; i < 5; i++) {
    value = CADIRegRead("TESTER", "t_sdr_dq"+(string)(i)+"_out");
    
    test_pattern = ~(0x55 << 8*(i + 3)) & ((1 << (8*DEVICE_BYTE_LANES*DEVICES_PER_RANK))-1);
    sprintf(msg, "B: Frontdoor Mem Read 0x%x", col + colOffset);
    CHECK_VALUE(msg, value, test_pattern);
  }
}

DUT_IDLE; EXECUTE(100);

// ********************************************************************************
// ********************************************************************************
// Write/Read different addresses (simple ba/row/col testing)
// ********************************************************************************
// ********************************************************************************

for (colOffset = 0; colOffset < 24; colOffset++) {

  addr = 1 << colOffset;
  message("INFO", "=== Testing Address 0x%x ===", addr);

  // fixed assignment of row/ba/col (RBC mode, with Row 13, Bank 4, Column 7)
  col =  addr & 0x7f;
  ba =  (addr >> 7 ) & 0xf;
  row = (addr >> 11 ) & 0x1fff;

  // Perform burst access with the given offset to make sure it behaves properly
  DUT_IDLE; EXECUTE(200);

  // Activate bank for write/read
  
  DUT_ACTIVE(ba, row); EXECUTE(1);

  DUT_IDLE; EXECUTE(10);

  // Send WOM command
  
  DUT_WOM(ba, col, 0); EXECUTE(1);
  DUT_IDLE; EXECUTE(wrLat);  // Idle for the Write Latency
  
  // Drive data sequence that can be examined via CADI reads
  DUT_DRIVE_WRDATA(0xffffffffffffffaa, 0xffffffffffffaaff, 0xffffffffffaaffff, 0xffffffffaaffffff); EXECUTE(1);
  DUT_DRIVE_WRDATA(0xffffffaaffffffff, 0xffffaaffffffffff, 0xffaaffffffffffff, 0xaaffffffffffffff); EXECUTE(1);
  DUT_IDLE; EXECUTE(15);

  // for every word in the burst
  for (j = 0; j < 8; j++) {
    // for every byte in the word
    for (i = 0; i < DEVICES_PER_RANK*DEVICE_BYTE_LANES; i++) {

      // Using CADI, check the values written for word width (while wrapping on burst boundaries)
      cadi_addr = ((((addr >> 3) << 3) + (((addr & 0x7)+j) & 0x7))*(DEVICES_PER_RANK*DEVICE_BYTE_LANES)) + i;

      // Perform CADI Read to check data (this is a byte read)
      value = CADIMemRead("TESTER", "Memory", cadi_addr);
      
      sprintf(msg, "CADI Mem Read: Address 0x%x", cadi_addr); 
      if (j == i) {
	CHECK_VALUE(msg, value, 0xaa);
      } else {
	if (i > 8) {
	  // MxScript is limited to 64 bits (8 bytes) so anything above that should still be zero
	  CHECK_VALUE(msg, value, 0x0);
	} else {
	  CHECK_VALUE(msg, value, 0xff);
	}
      }
    }
  }
  
  // Send READ command
  
  DUT_RD( ba, col, 1 ); EXECUTE(1);
  DUT_IDLE; EXECUTE(rdLat+5+13);  // Idle for the Read Latency (actual value is setting + 5), 13 clocks for cmd delay out and data delay back into tester registers
  
  // Check the test data registers
  for (i = 1; i < 5; i++) {
    value = CADIRegRead("TESTER", "t_sdr_dq"+(string)(i)+"_out");
    test_pattern = ~(0x55 << 8*(i - 1)) & ((1 << (8*DEVICE_BYTE_LANES*DEVICES_PER_RANK))-1);
    sprintf(msg, "A: Frontdoor Mem Read 0x%x", addr );
    CHECK_VALUE(msg, value, test_pattern);
  }
  
  // Get the next data set
  EXECUTE(1);
  
  // Check the test data registers
  for (i = 1; i < 5; i++) {
    value = CADIRegRead("TESTER", "t_sdr_dq"+(string)(i)+"_out");
    
    test_pattern = ~(0x55 << 8*(i + 3)) & ((1 << (8*DEVICE_BYTE_LANES*DEVICES_PER_RANK))-1);
    sprintf(msg, "B: Frontdoor Mem Read 0x%x", addr);
    CHECK_VALUE(msg, value, test_pattern);
  }
}

DUT_IDLE; EXECUTE(100);


FINAL_RESULTS;
closeSystem();
  
