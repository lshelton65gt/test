module carbon_gddr5_tester(/*AUTOARG*/
   // Outputs
   reset_n, sdr_a_ne, sdr_a_pe, sdr_abi_n_ne, sdr_abi_n_pe, sdr_cas_n,
   sdr_cke, sdr_cs_n, sdr_dq1_dbi_n_in, sdr_dq1_in, sdr_dq2_dbi_n_in,
   sdr_dq2_in, sdr_dq3_dbi_n_in, sdr_dq3_in, sdr_dq4_dbi_n_in,
   sdr_dq4_in, sdr_dq_valid_in, sdr_ras_n, sdr_we_n,
   // Inputs
   ck, wck, sdr_dq1_dbi_n_out, sdr_dq1_out, sdr_dq2_dbi_n_out,
   sdr_dq2_out, sdr_dq3_dbi_n_out, sdr_dq3_out, sdr_dq4_dbi_n_out,
   sdr_dq4_out, sdr_dq_valid_out, sdr_edc1, sdr_edc2, sdr_edc3,
   sdr_edc4
   );

   parameter DEVICE_BYTE_LANES = 4; //  determines data bit width (must be 2 or 4)
   parameter DEVICES_PER_RANK = 1;  //  determines data bit width 
   parameter MAX_ADDR_BITS = 20;    //  the maximum supported number of address bits

   // Calculated parameter
   parameter DATA_BITS = 8*DEVICE_BYTE_LANES*DEVICES_PER_RANK;
   

   // Beginning of automatic inputs (from unused autoinst inputs)
   input		ck;			
   output [DEVICES_PER_RANK-1:0]      reset_n;
   output [(12*DEVICES_PER_RANK)-1:0] sdr_a_ne;
   output [(12*DEVICES_PER_RANK)-1:0] sdr_a_pe;
   output [DEVICES_PER_RANK-1:0]      sdr_abi_n_ne;
   output [DEVICES_PER_RANK-1:0]      sdr_abi_n_pe;
   output [DEVICES_PER_RANK-1:0]      sdr_cas_n;
   output [DEVICES_PER_RANK-1:0]      sdr_cke;
   output [DEVICES_PER_RANK-1:0]      sdr_cs_n;
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	      sdr_dq1_dbi_n_in;
   output [DATA_BITS-1:0] 	      sdr_dq1_in;
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	      sdr_dq2_dbi_n_in;
   output [DATA_BITS-1:0] 	      sdr_dq2_in;
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	      sdr_dq3_dbi_n_in;
   output [DATA_BITS-1:0] 	      sdr_dq3_in;
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	      sdr_dq4_dbi_n_in;
   output [DATA_BITS-1:0] 	      sdr_dq4_in;
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0] sdr_dq_valid_in;
   output [DEVICES_PER_RANK-1:0]      sdr_ras_n;
   output [DEVICES_PER_RANK-1:0]      sdr_we_n;
   
   input 					     wck;
   // End of automatics
   

   // Beginning of automatic outputs (from unused autoinst outputs)
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq1_dbi_n_out;	// From intf of carbon_gddr5_interface.v
   input [DATA_BITS-1:0] sdr_dq1_out;		// From intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq2_dbi_n_out;	// From intf of carbon_gddr5_interface.v
   input [DATA_BITS-1:0] sdr_dq2_out;		// From intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq3_dbi_n_out;	// From intf of carbon_gddr5_interface.v
   input [DATA_BITS-1:0] sdr_dq3_out;		// From intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq4_dbi_n_out;	// From intf of carbon_gddr5_interface.v
   input [DATA_BITS-1:0] sdr_dq4_out;		// From intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0] sdr_dq_valid_out;// From intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_edc1;// From intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_edc2;// From intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_edc3;// From intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_edc4;// From intf of carbon_gddr5_interface.v
   // End of automatics

   reg [DEVICES_PER_RANK-1:0]		reset_n;		        
   reg [(12*DEVICES_PER_RANK)-1:0] 	sdr_a_ne;	
   reg [(12*DEVICES_PER_RANK)-1:0] 	sdr_a_pe;	
   reg [DEVICES_PER_RANK-1:0] 		sdr_abi_n_ne;	
   reg [DEVICES_PER_RANK-1:0] 		sdr_abi_n_pe;	
   reg [DEVICES_PER_RANK-1:0] 		sdr_cas_n;		
   reg [DEVICES_PER_RANK-1:0] 		sdr_cke;		
   reg [DEVICES_PER_RANK-1:0] 		sdr_cs_n;		
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	     sdr_dq1_dbi_n_in;	
   reg [DATA_BITS-1:0] 	     sdr_dq1_in;	
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	     sdr_dq2_dbi_n_in;	
   reg [DATA_BITS-1:0] 	     sdr_dq2_in;	
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	     sdr_dq3_dbi_n_in;	
   reg [DATA_BITS-1:0] 	     sdr_dq3_in;	
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	     sdr_dq4_dbi_n_in;	
   reg [DATA_BITS-1:0] 	     sdr_dq4_in;	
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0] sdr_dq_valid_in; 
   reg [DEVICES_PER_RANK-1:0] 			  sdr_ras_n;	  
   reg [DEVICES_PER_RANK-1:0] 			  sdr_we_n;	  



   
   reg [DEVICES_PER_RANK-1:0] 			  t_reset_n;		        // carbon depositSignal 
   // carbon observeSignal
   reg [(12*DEVICES_PER_RANK)-1:0] 		  t_sdr_a_ne;	// carbon depositSignal 
   // carbon observeSignal
   reg [(12*DEVICES_PER_RANK)-1:0] 		  t_sdr_a_pe;	// carbon depositSignal 
   // carbon observeSignal
   reg [DEVICES_PER_RANK-1:0] 			  t_sdr_abi_n_ne;	// carbon depositSignal 
   // carbon observeSignal
   reg [DEVICES_PER_RANK-1:0] 			  t_sdr_abi_n_pe;	// carbon depositSignal 
   // carbon observeSignal
   reg [DEVICES_PER_RANK-1:0] 			  t_sdr_cas_n;		// carbon depositSignal 
   // carbon observeSignal
   reg [DEVICES_PER_RANK-1:0] 			  t_sdr_cke;		// carbon depositSignal 
   // carbon observeSignal
   reg [DEVICES_PER_RANK-1:0] 			  t_sdr_cs_n;		// carbon depositSignal 
   // carbon observeSignal
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] t_sdr_dq1_dbi_n_in;	// carbon depositSignal 
   // carbon observeSignal
   reg [DATA_BITS-1:0] 				  t_sdr_dq1_in;	// carbon depositSignal 
   // carbon observeSignal
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] t_sdr_dq2_dbi_n_in;	// carbon depositSignal 
   // carbon observeSignal
   reg [DATA_BITS-1:0] 				  t_sdr_dq2_in;	// carbon depositSignal 
   // carbon observeSignal
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] t_sdr_dq3_dbi_n_in;	// carbon depositSignal 
   // carbon observeSignal
   reg [DATA_BITS-1:0] 				  t_sdr_dq3_in;	// carbon depositSignal 
   // carbon observeSignal
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] t_sdr_dq4_dbi_n_in;	// carbon depositSignal 
   // carbon observeSignal
   reg [DATA_BITS-1:0] 				  t_sdr_dq4_in;	// carbon depositSignal 
   // carbon observeSignal
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0] t_sdr_dq_valid_in;       // carbon depositSignal 
   // carbon observeSignal
   reg [DEVICES_PER_RANK-1:0] 			  t_sdr_ras_n;		// carbon depositSignal 
   // carbon observeSignal
   reg [DEVICES_PER_RANK-1:0] 			  t_sdr_we_n;		// carbon depositSignal 
   // carbon observeSignal

   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] t_sdr_dq1_dbi_n_out;     // carbon observeSignal
   reg [DATA_BITS-1:0] 				  t_sdr_dq1_out;	   // carbon observeSignal
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] t_sdr_dq2_dbi_n_out;	   // carbon observeSignal
   reg [DATA_BITS-1:0] 				  t_sdr_dq2_out;	   // carbon observeSignal
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] t_sdr_dq3_dbi_n_out;	   // carbon observeSignal
   reg [DATA_BITS-1:0] 				  t_sdr_dq3_out;	   // carbon observeSignal
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] t_sdr_dq4_dbi_n_out;	   // carbon observeSignal
   reg [DATA_BITS-1:0] 				  t_sdr_dq4_out;	   // carbon observeSignal
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0] t_sdr_dq_valid_out;      // carbon observeSignal
   

   always @(posedge wck) begin
      reset_n <= 	  t_reset_n;		        
      sdr_a_ne <= 	  t_sdr_a_ne;	
      sdr_a_pe <= 	  t_sdr_a_pe;	
      sdr_abi_n_ne <= 	  t_sdr_abi_n_ne;	
      sdr_abi_n_pe <= 	  t_sdr_abi_n_pe;	
      sdr_cas_n <= 	  t_sdr_cas_n;		
      sdr_cke <= 	  t_sdr_cke;		
      sdr_cs_n <= 	  t_sdr_cs_n;		
      sdr_dq1_dbi_n_in <= t_sdr_dq1_dbi_n_in;	
      sdr_dq1_in <= 	  t_sdr_dq1_in;	
      sdr_dq2_dbi_n_in <= t_sdr_dq2_dbi_n_in;	
      sdr_dq2_in <= 	  t_sdr_dq2_in;	
      sdr_dq3_dbi_n_in <= t_sdr_dq3_dbi_n_in;	
      sdr_dq3_in <= 	  t_sdr_dq3_in;	
      sdr_dq4_dbi_n_in <= t_sdr_dq4_dbi_n_in;	
      sdr_dq4_in <= 	  t_sdr_dq4_in;	
      sdr_dq_valid_in <=  t_sdr_dq_valid_in; 
      sdr_ras_n <= 	  t_sdr_ras_n;	  
      sdr_we_n <= 	  t_sdr_we_n;	  

      t_sdr_dq1_dbi_n_out <=    sdr_dq1_dbi_n_out;
      t_sdr_dq1_out <=	        sdr_dq1_out;	  
      t_sdr_dq2_dbi_n_out <=    sdr_dq2_dbi_n_out;
      t_sdr_dq2_out <=	        sdr_dq2_out;	  
      t_sdr_dq3_dbi_n_out <=    sdr_dq3_dbi_n_out;
      t_sdr_dq3_out <=	        sdr_dq3_out;	  
      t_sdr_dq4_dbi_n_out <=    sdr_dq4_dbi_n_out;
      t_sdr_dq4_out <=	        sdr_dq4_out;	  
      t_sdr_dq_valid_out <=     sdr_dq_valid_out; 
   end
   

   
   reg [31:0] DEVICE_BYTE_LANES_VALUE; //  carbon observeSignal
   reg [31:0] DEVICES_PER_RANK_VALUE;  //  carbon observeSignal
   reg [31:0] MAX_ADDR_BITS_VALUE;     //  carbon observeSignal

   initial
     begin
	DEVICE_BYTE_LANES_VALUE = 	DEVICE_BYTE_LANES; 
	DEVICES_PER_RANK_VALUE =  	DEVICES_PER_RANK;  
	MAX_ADDR_BITS_VALUE =     	MAX_ADDR_BITS;     
     end
   
   
endmodule // carbon_gddr5_tester


