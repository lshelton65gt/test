This Test_Driver is a simple mechanism for driving SoCD signals from CADI registers.
The Single Data Rate (SDR) interface to the GDDR5 memory test component is implemented here with all 
output signals connected to writeable CADI registers, and input signals are sampled and drive
readable CADI registers.  This allows tests to be written with MxScripts to drive the 
raw waveforms desired and sample any output from the device under test.

This is different mechanism than the other DDRx tests, requiring the tests to be written in MxScript.
