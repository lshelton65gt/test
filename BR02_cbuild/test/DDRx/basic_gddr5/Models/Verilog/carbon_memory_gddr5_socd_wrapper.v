`timescale 1ps / 1ps
module carbon_memory_gddr5_socd_wrapper(/*AUTOARG*/
   // Outputs
   sdr_edc4, sdr_edc3, sdr_edc2, sdr_edc1, sdr_dq_valid_out,
   sdr_dq4_out, sdr_dq4_dbi_n_out, sdr_dq3_out, sdr_dq3_dbi_n_out,
   sdr_dq2_out, sdr_dq2_dbi_n_out, sdr_dq1_out, sdr_dq1_dbi_n_out,
   // Inputs
   wck, sdr_we_n, sdr_ras_n, sdr_dq_valid_in, sdr_dq4_in,
   sdr_dq4_dbi_n_in, sdr_dq3_in, sdr_dq3_dbi_n_in, sdr_dq2_in,
   sdr_dq2_dbi_n_in, sdr_dq1_in, sdr_dq1_dbi_n_in, sdr_cs_n, sdr_cke,
   sdr_cas_n, sdr_abi_n_pe, sdr_abi_n_ne, sdr_a_pe, sdr_a_ne, reset_n,
   ck
   );

   // ********************************************************************************
   // PARAMETER CALCULATIONS REPLICATED FROM carbon_gddr5 MODULE
   //

   parameter DEVICE_BYTE_LANES = 4; //  determines data bit width (must be 2 or 4)
   parameter DEVICES_PER_RANK = 1;  //  determines data bit width 
   parameter MAX_ADDR_BITS = 24;    //  the maximum supported number of address bits

   // Calculated parameter
   parameter DATA_BITS = 8*DEVICE_BYTE_LANES*DEVICES_PER_RANK;
   

   /*AUTOINPUT*/
   // Beginning of automatic inputs (from unused autoinst inputs)
   input		ck;			// To mem of carbon_memory_gddr5.v, ...
   input [DEVICES_PER_RANK-1:0] reset_n;	// To mem of carbon_memory_gddr5.v, ...
   input [(12*DEVICES_PER_RANK)-1:0] sdr_a_ne;	// To intf of carbon_gddr5_interface.v
   input [(12*DEVICES_PER_RANK)-1:0] sdr_a_pe;	// To intf of carbon_gddr5_interface.v
   input [DEVICES_PER_RANK-1:0] sdr_abi_n_ne;	// To intf of carbon_gddr5_interface.v
   input [DEVICES_PER_RANK-1:0] sdr_abi_n_pe;	// To intf of carbon_gddr5_interface.v
   input [DEVICES_PER_RANK-1:0] sdr_cas_n;	// To intf of carbon_gddr5_interface.v
   input [DEVICES_PER_RANK-1:0] sdr_cke;	// To intf of carbon_gddr5_interface.v
   input [DEVICES_PER_RANK-1:0] sdr_cs_n;	// To intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq1_dbi_n_in;// To intf of carbon_gddr5_interface.v
   input [DATA_BITS-1:0] sdr_dq1_in;		// To intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq2_dbi_n_in;// To intf of carbon_gddr5_interface.v
   input [DATA_BITS-1:0] sdr_dq2_in;		// To intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq3_dbi_n_in;// To intf of carbon_gddr5_interface.v
   input [DATA_BITS-1:0] sdr_dq3_in;		// To intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq4_dbi_n_in;// To intf of carbon_gddr5_interface.v
   input [DATA_BITS-1:0] sdr_dq4_in;		// To intf of carbon_gddr5_interface.v
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0] sdr_dq_valid_in;// To intf of carbon_gddr5_interface.v
   input [DEVICES_PER_RANK-1:0] sdr_ras_n;	// To intf of carbon_gddr5_interface.v
   input [DEVICES_PER_RANK-1:0] sdr_we_n;	// To intf of carbon_gddr5_interface.v
   input		wck;			// To mem of carbon_memory_gddr5.v, ...
   // End of automatics
   /*AUTOOUTPUT*/
   // Beginning of automatic outputs (from unused autoinst outputs)
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq1_dbi_n_out;// From intf of carbon_gddr5_interface.v
   output [DATA_BITS-1:0] sdr_dq1_out;		// From intf of carbon_gddr5_interface.v
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq2_dbi_n_out;// From intf of carbon_gddr5_interface.v
   output [DATA_BITS-1:0] sdr_dq2_out;		// From intf of carbon_gddr5_interface.v
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq3_dbi_n_out;// From intf of carbon_gddr5_interface.v
   output [DATA_BITS-1:0] sdr_dq3_out;		// From intf of carbon_gddr5_interface.v
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_dq4_dbi_n_out;// From intf of carbon_gddr5_interface.v
   output [DATA_BITS-1:0] sdr_dq4_out;		// From intf of carbon_gddr5_interface.v
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0] sdr_dq_valid_out;// From intf of carbon_gddr5_interface.v
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_edc1;// From intf of carbon_gddr5_interface.v
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_edc2;// From intf of carbon_gddr5_interface.v
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_edc3;// From intf of carbon_gddr5_interface.v
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] sdr_edc4;// From intf of carbon_gddr5_interface.v
   // End of automatics
   
   // ********************************************************************************

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [(12*DEVICES_PER_RANK)-1:0] a;		// From intf of carbon_gddr5_interface.v
   wire [DEVICES_PER_RANK-1:0] abi_n;		// From intf of carbon_gddr5_interface.v
   wire [DEVICES_PER_RANK-1:0] cas_n;		// From intf of carbon_gddr5_interface.v
   wire [DEVICES_PER_RANK-1:0] cke;		// From intf of carbon_gddr5_interface.v
   wire [DEVICES_PER_RANK-1:0] cs_n;		// From intf of carbon_gddr5_interface.v
   wire [(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0] dbi_n;// To/From mem of carbon_memory_gddr5.v, ...
   wire [DATA_BITS-1:0]	dq;			// To/From mem of carbon_memory_gddr5.v, ..., Couldn't Merge
   wire [(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0] edc;// From mem of carbon_memory_gddr5.v
   wire [DEVICES_PER_RANK-1:0] ras_n;		// From intf of carbon_gddr5_interface.v
   wire [DEVICES_PER_RANK-1:0] we_n;		// From intf of carbon_gddr5_interface.v
   // End of automatics
   /*AUTOREG*/

   carbon_memory_gddr5 #(
			 .DEVICE_BYTE_LANES(DEVICE_BYTE_LANES),
			 .DEVICES_PER_RANK(DEVICES_PER_RANK),
			 .MAX_ADDR_BITS(MAX_ADDR_BITS)
			 ) mem(/*AUTOINST*/
			       // Outputs
			       .edc		(edc[(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0]),
			       // Inouts
			       .dq		(dq[(DEVICES_PER_RANK*8*DEVICE_BYTE_LANES)-1:0]),
			       .dbi_n		(dbi_n[(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0]),
			       // Inputs
			       .reset_n		(reset_n[DEVICES_PER_RANK-1:0]),
			       .ck		(ck),
			       .wck		(wck),
			       .ras_n		(ras_n[DEVICES_PER_RANK-1:0]),
			       .cas_n		(cas_n[DEVICES_PER_RANK-1:0]),
			       .we_n		(we_n[DEVICES_PER_RANK-1:0]),
			       .cke		(cke[DEVICES_PER_RANK-1:0]),
			       .cs_n		(cs_n[DEVICES_PER_RANK-1:0]),
			       .a		(a[(12*DEVICES_PER_RANK)-1:0]),
			       .abi_n		(abi_n[DEVICES_PER_RANK-1:0]));


   carbon_gddr5_interface #(
			    .DEVICE_BYTE_LANES(DEVICE_BYTE_LANES),
			    .DEVICES_PER_RANK(DEVICES_PER_RANK),
			    .MAX_ADDR_BITS(MAX_ADDR_BITS)
			    ) intf(/*AUTOINST*/
				   // Outputs
				   .ras_n		(ras_n[DEVICES_PER_RANK-1:0]),
				   .cas_n		(cas_n[DEVICES_PER_RANK-1:0]),
				   .we_n		(we_n[DEVICES_PER_RANK-1:0]),
				   .cke			(cke[DEVICES_PER_RANK-1:0]),
				   .cs_n		(cs_n[DEVICES_PER_RANK-1:0]),
				   .a			(a[(12*DEVICES_PER_RANK)-1:0]),
				   .abi_n		(abi_n[DEVICES_PER_RANK-1:0]),
				   .sdr_dq_valid_out	(sdr_dq_valid_out[(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0]),
				   .sdr_dq1_out		(sdr_dq1_out[DATA_BITS-1:0]),
				   .sdr_dq2_out		(sdr_dq2_out[DATA_BITS-1:0]),
				   .sdr_dq3_out		(sdr_dq3_out[DATA_BITS-1:0]),
				   .sdr_dq4_out		(sdr_dq4_out[DATA_BITS-1:0]),
				   .sdr_dq1_dbi_n_out	(sdr_dq1_dbi_n_out[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   .sdr_dq2_dbi_n_out	(sdr_dq2_dbi_n_out[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   .sdr_dq3_dbi_n_out	(sdr_dq3_dbi_n_out[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   .sdr_dq4_dbi_n_out	(sdr_dq4_dbi_n_out[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   .sdr_edc1		(sdr_edc1[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   .sdr_edc2		(sdr_edc2[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   .sdr_edc3		(sdr_edc3[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   .sdr_edc4		(sdr_edc4[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   // Inouts
				   .dq			(dq[DATA_BITS-1:0]),
				   .dbi_n		(dbi_n[(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0]),
				   // Inputs
				   .reset_n		(reset_n[DEVICES_PER_RANK-1:0]),
				   .ck			(ck),
				   .wck			(wck),
				   .edc			(edc[(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0]),
				   .sdr_ras_n		(sdr_ras_n[DEVICES_PER_RANK-1:0]),
				   .sdr_cas_n		(sdr_cas_n[DEVICES_PER_RANK-1:0]),
				   .sdr_we_n		(sdr_we_n[DEVICES_PER_RANK-1:0]),
				   .sdr_cke		(sdr_cke[DEVICES_PER_RANK-1:0]),
				   .sdr_cs_n		(sdr_cs_n[DEVICES_PER_RANK-1:0]),
				   .sdr_a_pe		(sdr_a_pe[(12*DEVICES_PER_RANK)-1:0]),
				   .sdr_a_ne		(sdr_a_ne[(12*DEVICES_PER_RANK)-1:0]),
				   .sdr_abi_n_pe	(sdr_abi_n_pe[DEVICES_PER_RANK-1:0]),
				   .sdr_abi_n_ne	(sdr_abi_n_ne[DEVICES_PER_RANK-1:0]),
				   .sdr_dq_valid_in	(sdr_dq_valid_in[(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0]),
				   .sdr_dq1_in		(sdr_dq1_in[DATA_BITS-1:0]),
				   .sdr_dq2_in		(sdr_dq2_in[DATA_BITS-1:0]),
				   .sdr_dq3_in		(sdr_dq3_in[DATA_BITS-1:0]),
				   .sdr_dq4_in		(sdr_dq4_in[DATA_BITS-1:0]),
				   .sdr_dq1_dbi_n_in	(sdr_dq1_dbi_n_in[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   .sdr_dq2_dbi_n_in	(sdr_dq2_dbi_n_in[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   .sdr_dq3_dbi_n_in	(sdr_dq3_dbi_n_in[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]),
				   .sdr_dq4_dbi_n_in	(sdr_dq4_dbi_n_in[(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]));
      
endmodule

// Local Variables:
// verilog-library-directories:("." "../../../../../src/carbonRTL/carbon_memory_gddr5")
// End:
