module carbon_gddr5_interface(/*AUTOARG*/
   // Outputs
   ras_n, cas_n, we_n, cke, cs_n, a, abi_n, sdr_dq_valid_out,
   sdr_dq1_out, sdr_dq2_out, sdr_dq3_out, sdr_dq4_out,
   sdr_dq1_dbi_n_out, sdr_dq2_dbi_n_out, sdr_dq3_dbi_n_out,
   sdr_dq4_dbi_n_out, sdr_edc1, sdr_edc2, sdr_edc3, sdr_edc4,
   // Inouts
   dq, dbi_n,
   // Inputs
   reset_n, ck, wck, edc, sdr_ras_n, sdr_cas_n, sdr_we_n, sdr_cke,
   sdr_cs_n, sdr_a_pe, sdr_a_ne, sdr_abi_n_pe, sdr_abi_n_ne,
   sdr_dq_valid_in, sdr_dq1_in, sdr_dq2_in, sdr_dq3_in, sdr_dq4_in,
   sdr_dq1_dbi_n_in, sdr_dq2_dbi_n_in, sdr_dq3_dbi_n_in,
   sdr_dq4_dbi_n_in
   );

   // number of chip selects (ranks) and data width
   parameter DEVICE_BYTE_LANES = 4;  // determines data bit width (must be 2 or 4)
   parameter DEVICES_PER_RANK = 1;  // determines data bit width 

   parameter MAX_ADDR_BITS = 24;  // the maximum supported number of address bits
                                 // see also the three registers carbon_current_BA_BITS carbon_current_ROW_BITS carbon_current_COL_BITS

   // Calculated parameter
   parameter DATA_BITS = DEVICES_PER_RANK*8*DEVICE_BYTE_LANES;


   // Signals COMMON to all "devices"
   input [DEVICES_PER_RANK-1:0]				    reset_n;
   input 						    ck;   
   input 						    wck;  

   // ********************************************************************************
   // Signals driven to DUT
   output reg [DEVICES_PER_RANK-1:0]			    ras_n;
   output reg [DEVICES_PER_RANK-1:0]			    cas_n;
   output reg [DEVICES_PER_RANK-1:0]			    we_n;
   output reg [DEVICES_PER_RANK-1:0]			    cke;
   output reg [DEVICES_PER_RANK-1:0]			    cs_n;
   output reg [(12*DEVICES_PER_RANK)-1:0] 		    a; // 12 = arbitrary maximum width supported - any unused bit should be tied to 0 -in reality this is only 8/9
   output reg [DEVICES_PER_RANK-1:0] 			    abi_n;
   inout [DATA_BITS-1:0] 				    dq; // carbon observeSignal
   inout [(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0] 	    dbi_n;
   input [(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0] 	    edc;
   

   // ********************************************************************************
   // Single data rate signals based on ck
   // pe = posedge sampled
   // ne = negedge sampled
   
   input [DEVICES_PER_RANK-1:0]				    sdr_ras_n;
   input [DEVICES_PER_RANK-1:0]				    sdr_cas_n;
   input [DEVICES_PER_RANK-1:0]				    sdr_we_n;
   input [DEVICES_PER_RANK-1:0]				    sdr_cke;
   input [DEVICES_PER_RANK-1:0]				    sdr_cs_n;
   input [(12*DEVICES_PER_RANK)-1:0] 			    sdr_a_pe; // 12 = arbitrary maximum width supported - any unused bit should be tied to 0 -in reality this is only 8/9
   input [(12*DEVICES_PER_RANK)-1:0] 			    sdr_a_ne; // 12 = arbitrary maximum width supported - any unused bit should be tied to 0 -in reality this is only 8/9
   input [DEVICES_PER_RANK-1:0] 			    sdr_abi_n_pe;
   input [DEVICES_PER_RANK-1:0] 			    sdr_abi_n_ne;

   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0] 	    sdr_dq_valid_in;
   input [DATA_BITS-1:0] 				    sdr_dq1_in;
   input [DATA_BITS-1:0] 				    sdr_dq2_in;
   input [DATA_BITS-1:0] 				    sdr_dq3_in;
   input [DATA_BITS-1:0] 				    sdr_dq4_in;
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	    sdr_dq1_dbi_n_in;
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	    sdr_dq2_dbi_n_in;
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	    sdr_dq3_dbi_n_in;
   input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0] 	    sdr_dq4_dbi_n_in;
 				    
   output [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0] 	    sdr_dq_valid_out;
   output reg [DATA_BITS-1:0] 				    sdr_dq1_out;
   output reg [DATA_BITS-1:0] 				    sdr_dq2_out;
   output reg [DATA_BITS-1:0] 				    sdr_dq3_out;
   output reg [DATA_BITS-1:0] 				    sdr_dq4_out;
   output reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]    sdr_dq1_dbi_n_out;
   output reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]    sdr_dq2_dbi_n_out;
   output reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]    sdr_dq3_dbi_n_out;
   output reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]    sdr_dq4_dbi_n_out;

   output reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]    sdr_edc1;
   output reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]    sdr_edc2;
   output reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]    sdr_edc3;
   output reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK)-1:0]    sdr_edc4;
   
   // ********************************************************************************
   assign sdr_dq_valid_out = 0;  // Test should know when to sample these...
   // ********************************************************************************

   // ****************************************
   // general signal timing (always posedge sampled same in both domains)
   // sample on posedge but launch on negedge (common practice in real phy)

   reg [DEVICES_PER_RANK-1:0]	   sdr_cke_d;
   reg [DEVICES_PER_RANK-1:0]	   sdr_cs_n_d;
   reg [DEVICES_PER_RANK-1:0]	   sdr_ras_n_d;
   reg [DEVICES_PER_RANK-1:0]	   sdr_cas_n_d;
   reg [DEVICES_PER_RANK-1:0]	   sdr_we_n_d;

   reg [(12*DEVICES_PER_RANK)-1:0] sdr_a_pe_d;
   reg [(12*DEVICES_PER_RANK)-1:0] sdr_a_ne_d;
   reg [DEVICES_PER_RANK-1:0] 	   sdr_abi_n_pe_d;
   reg [DEVICES_PER_RANK-1:0] 	   sdr_abi_n_ne_d;
   
   
   always @(posedge ck) begin
      sdr_cke_d <= sdr_cke;
      sdr_cs_n_d <= sdr_cs_n;
      sdr_ras_n_d <= sdr_ras_n;
      sdr_cas_n_d <= sdr_cas_n;
      sdr_we_n_d <= sdr_we_n;
      
      sdr_a_pe_d <= sdr_a_pe;
      sdr_a_ne_d <= sdr_a_ne;

      sdr_abi_n_pe_d <= sdr_abi_n_pe;
      sdr_abi_n_ne_d <= sdr_abi_n_ne;

      a <= sdr_a_ne_d;
      abi_n <= sdr_abi_n_ne_d;
   end
	      
   always @(negedge ck) begin
      cke <= sdr_cke_d;
      cs_n <= sdr_cs_n_d;
      ras_n <= sdr_ras_n_d;
      cas_n <= sdr_cas_n_d;
      we_n <= sdr_we_n_d;

      a <= sdr_a_pe_d;
      abi_n <= sdr_abi_n_pe_d;
   end
	      
   
   // ********************************************************************************
   // ********************************************************************************
   // Bidirectional signal generation and timing modification
   
/* -----\/----- EXCLUDED -----\/-----
   wire 		    dqs_out;
   reg 			    dqs_out_internal;

   parameter INTF_DQS_OUT_DELAY = (DQS_IN_DELAY == 0) ? 1 : 0;
   carbon_retiming_element #(1, INTF_DQS_OUT_DELAY) dqs_out_retimer(ck_t, dqs_out_internal, dqs_out);
   
   wire 		    dqs_oe;
   reg 			    dqs_oe_internal;
   assign dqs_oe = dqs_oe_internal; // dqs oe timing should not change, even though dqs out timing does by 1/2 cycle
   
   // NOTE:  Only a single dqs signal is used internally!
   // general form for generating internal write dqs (DM should provide final qualification of data)
   wire 		    dqs_in = |dqs_t & !dqs_oe; // ignore dqs that we generate
   // alternate form using lsb for generating internal write dqs - just use a single bit
   // wire 		    dqs_in = dqs_t[0] & !dqs_oe; // ignore dqs that we generate
   wire 		    dqs_in_internal;
   parameter INTF_DQS_IN_DELAY = (DQS_OUT_DELAY == 0) ? 1 : 0;
   carbon_retiming_element #(1, INTF_DQS_IN_DELAY) dqs_in_retimer(ck_t, dqs_in, dqs_in_internal);
   
   assign dqs_t = dqs_oe ? { DM_BITS {  dqs_out }} : { DM_BITS { 1'bz } };
   assign dqs_c = dqs_oe ? { DM_BITS { ~dqs_out }} : { DM_BITS { 1'bz } };
   pullup(dqs_c);
 -----/\----- EXCLUDED -----/\----- */
   
   reg [DATA_BITS-1:0] 	    dq_out;  // carbon observeSignal
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0]  dbi_n_out;
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0]  dq_oe;

   // sample valid from sdr interface and generate dqs_out_internal and dq_out
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0]  sdr_dq1_dbi_n_in_d;
   reg [DATA_BITS-1:0] 				   sdr_dq1_in_d;
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0]  sdr_dq2_dbi_n_in_d;
   reg [DATA_BITS-1:0] 				   sdr_dq2_in_d;
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0]  sdr_dq3_dbi_n_in_d;
   reg [DATA_BITS-1:0] 				   sdr_dq3_in_d;
   reg [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0]  sdr_dq4_dbi_n_in_d;
   reg [DATA_BITS-1:0] 				   sdr_dq4_in_d;
   
   wire [DATA_BITS-1:0]     dq_in = dq;
   wire [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0]  dbi_n_in = dbi_n;
   wire [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0]  edc_in = edc;
   
   always @(posedge ck) begin
      dq_oe <= sdr_dq_valid_in;
      sdr_dq1_dbi_n_in_d <= sdr_dq1_dbi_n_in;
      sdr_dq1_in_d <= sdr_dq1_in;
      sdr_dq2_dbi_n_in_d <= sdr_dq2_dbi_n_in;
      sdr_dq2_in_d <= sdr_dq2_in;
      sdr_dq3_dbi_n_in_d <= sdr_dq3_dbi_n_in;
      sdr_dq3_in_d <= sdr_dq3_in;
      sdr_dq4_dbi_n_in_d <= sdr_dq4_dbi_n_in;
      sdr_dq4_in_d <= sdr_dq4_in;
   end

   // This should be appropriately masked based on the sdr_valid_in masking, but for now, send whole data if any is valid
   assign dq    = |dq_oe ? dq_out    : {                           DATA_BITS { 1'bz } };
   assign dbi_n = |dq_oe ? dbi_n_out : { (DEVICE_BYTE_LANES*DEVICES_PER_RANK){ 1'bz } };
   
   // output phase selector
   // Based on ck and wck, one of four data values can be driven
 			
   wire mode = 1'b1; // indicates write data alignment
   
   always @(*) begin
      case ( {mode, ck, wck} )
	// Nominal timing
	3'b000: begin
	   dq_out <= sdr_dq1_in_d;
	   dbi_n_out <= sdr_dq1_dbi_n_in_d;
	end
	3'b011: begin
	   dq_out <= sdr_dq2_in_d;
	   dbi_n_out <= sdr_dq2_dbi_n_in_d;
	end
	3'b010: begin
	   dq_out <= sdr_dq3_in_d;
	   dbi_n_out <= sdr_dq3_dbi_n_in_d;
	end
	3'b001: begin
	   dq_out <= sdr_dq4_in_d;
	   dbi_n_out <= sdr_dq4_dbi_n_in_d;
	end
	// Delayed timing
	3'b111: begin
	   dq_out <= sdr_dq1_in_d;
	   dbi_n_out <= sdr_dq1_dbi_n_in_d;
	end
	3'b110: begin
	   dq_out <= sdr_dq2_in_d;
	   dbi_n_out <= sdr_dq2_dbi_n_in_d;
	end
	3'b101: begin
	   dq_out <= sdr_dq3_in_d;
	   dbi_n_out <= sdr_dq3_dbi_n_in_d;
	end
	3'b100: begin
	   dq_out <= sdr_dq4_in_d;
	   dbi_n_out <= sdr_dq4_dbi_n_in_d;
	end
      endcase
   end


   
   // ****************************************
   // dq input sampling 

   reg [DATA_BITS-1:0] 	    sdr_dq1_out_d;
   reg [DATA_BITS-1:0] 	    sdr_dq2_out_d;
   reg [DATA_BITS-1:0] 	    sdr_dq3_out_d;
   reg [DATA_BITS-1:0] 	    sdr_dq4_out_d;
   reg [DEVICE_BYTE_LANES*DEVICES_PER_RANK-1:0] sdr_dq1_dbi_n_out_d;
   reg [DEVICE_BYTE_LANES*DEVICES_PER_RANK-1:0] sdr_dq2_dbi_n_out_d;
   reg [DEVICE_BYTE_LANES*DEVICES_PER_RANK-1:0] sdr_dq3_dbi_n_out_d;
   reg [DEVICE_BYTE_LANES*DEVICES_PER_RANK-1:0] sdr_dq4_dbi_n_out_d;
   reg [DEVICE_BYTE_LANES*DEVICES_PER_RANK-1:0] sdr_edc1_d;
   reg [DEVICE_BYTE_LANES*DEVICES_PER_RANK-1:0] sdr_edc2_d;
   reg [DEVICE_BYTE_LANES*DEVICES_PER_RANK-1:0] sdr_edc3_d;
   reg [DEVICE_BYTE_LANES*DEVICES_PER_RANK-1:0] sdr_edc4_d;
   
   always @(negedge wck) begin
      if (ck) 
	begin
	   sdr_dq1_out_d <= dq_in;
	   sdr_dq1_dbi_n_out_d <= dbi_n_in;
	   sdr_edc1_d <= edc_in;
	end
      else
	begin
	   sdr_dq3_out_d <= dq_in;
	   sdr_dq3_dbi_n_out_d <= dbi_n_in;
	   sdr_edc3_d <= edc_in;
	end
   end
   

   always @(posedge wck) begin
      if (ck)
	begin
	   sdr_dq2_out_d <= dq_in;
	   sdr_dq2_dbi_n_out_d <= dbi_n_in;
	   sdr_edc2_d <= edc_in;
	end
      else
	begin
	   sdr_dq4_out_d <= dq_in;
	   sdr_dq4_dbi_n_out_d <= dbi_n_in;
	   sdr_edc4_d <= edc_in;
	end
   end

   function [DATA_BITS-1:0] get_dbi_mask;
      input [(DEVICE_BYTE_LANES*DEVICES_PER_RANK-1):0] dbi_n;
      integer 					       j;
      reg [DATA_BITS-1:0] 			       result;
      begin
	 get_dbi_mask = 0;
	 for ( j = 0; j < DEVICE_BYTE_LANES*DEVICES_PER_RANK; j = j + 1 ) 
	   if (~(dbi_n[j]))
	     get_dbi_mask = get_dbi_mask | (8'hff << (8*j));
      end
   endfunction
   
   // apply the dbi mask
   always @(posedge ck) begin
      sdr_dq1_dbi_n_out <= sdr_dq1_dbi_n_out_d;
      sdr_dq1_out <= sdr_dq1_out_d ^ get_dbi_mask(sdr_dq1_dbi_n_out_d);
      sdr_edc1 <= sdr_edc1_d;
      
      sdr_dq2_dbi_n_out <= sdr_dq2_dbi_n_out_d;
      sdr_dq2_out <= sdr_dq2_out_d ^ get_dbi_mask(sdr_dq2_dbi_n_out_d);
      sdr_edc2 <= sdr_edc2_d;

      sdr_dq3_dbi_n_out <= sdr_dq3_dbi_n_out_d;
      sdr_dq3_out <= sdr_dq3_out_d ^ get_dbi_mask(sdr_dq3_dbi_n_out_d);
      sdr_edc3 <= sdr_edc3_d;

      sdr_dq4_dbi_n_out <= dbi_n_in;
      sdr_dq4_out <= dq_in ^ get_dbi_mask(dbi_n_in);
      sdr_edc4 <= sdr_edc4_d;
   end

endmodule // carbon_gddr5_interface
