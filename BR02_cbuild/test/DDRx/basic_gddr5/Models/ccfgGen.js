// modelstudio -batch -script ccfgGen.js -squish  

var finalCcfg = new CarbonCfg();
var status = finalCcfg.readNoCheck(CcfgEnum.Arm, "initial.ccfg");

// ccfg.write("intermediate.ccfg");

// var finalCcfg = new CarbonCfg();
// var finalStatus = finalCcfg.readNoCheck(CcfgEnum.Arm, "intermediate.ccfg");

var subCcfg = finalCcfg.getSubComponent(0); // should only be one
var mem = subCcfg.getMemory(0); // should only be one
debug("Memory name = " + mem.name);
mem.setInitType(CcfgEnum.MemInitProgPreload);
mem.setProgPreloadEslPort("CarbonDebug");

// Add the embedded model subcomponent
finalCcfg.write("final.ccfg");


// Shutdown the application so modelstudio quits
Application.shutdown(0);
