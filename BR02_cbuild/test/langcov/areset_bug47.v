module bug (out2, out1, out0, in, reset, clk);
   input in, reset, clk;
   output out2, out1, out0;
   reg    out2, out1, out0;
   
   always @(negedge reset or negedge clk) 
     begin : foo0
        if (reset == 1'b0) 
          begin
             out0 <= 1'b0;
          end
        else
          begin
             out0 <= in;
          end
     end

   always @(negedge reset or negedge clk) 
     begin : foo1
        if (~reset) 
          begin
             out1 <= 1'b0;
          end
        else
          begin
             out1 <= in;
          end
     end
   always @(negedge reset or negedge clk) 
     begin : foo2
        if (!reset) 
          begin
             out2 <= 1'b0;
          end
        else
          begin
             out2 <= in;
          end
     end
endmodule


