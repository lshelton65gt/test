// last mod: Tue Jul 10 14:36:19 2007
// filename: test/langcov/tri5.v
// Description:  This test checks for the proper handling of bufif1/bufif0 with 
// incorrectly sized data and enable terms

module tri5(i1, e1, i4, e4, o1, o2, o3, o4, o5, o6, o7, o8);
   input i1, e1;
   input [3:0] i4, e4;
   output      o1, o2, o3, o4, o5, o6, o7, o8;

  bufif0 b1(o1, i1, e1);
  bufif0 b2(o2, i1, e4);	// enable too wide
  bufif0 b3(o3, i4, e4);	// data and enable too wide
  bufif0 b4(o4, i4, e1);	// data too wide
   
  bufif1 c1(o5, i1, e1);
  bufif1 c2(o6, i1, e4);	// enable too wide
  bufif1 c3(o7, i4, e4);	// data and enable too wide
  bufif1 c4(o8, i4, e1);	// data too wide
   

endmodule
