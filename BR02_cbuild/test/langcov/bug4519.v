// last mod: Thu May 26 18:54:54 2005
// filename: test/langcov/bug4519.v
// Description:  This test 


// Generated with: ./generator -n -1576847676 -s 50 -o candidate.v
// Using seed: 0xa20336c4
// Writing circuit of size 50 to file 'candidate.v'
module bug4519(clk, Ixt, _O, tXYX, uNrf, WtPJe, Ho, pG);
input clk;
output Ixt, _O, tXYX, uNrf, WtPJe, Ho, pG;

  // net delcarations
  wire clk;
  reg  [31:0] K;
  reg  [31:0] VDZNL;
  reg  [31:0] K_nX;
  reg  [31:0] AM;
  reg  [31:0] soNG;
  wire P;
  wire [126:63] Hp;
  wire [30:79] _dDt;
  reg   signed DS_fh;
  wire Vj;
  wire MxSFR;
  wire [88:24] B_n;
  wire [80:3] N;
  reg  [31:97] e;
  reg  [12:21] E;
  wire [78:5] RRjau;
  reg  EVp;
  wire OIfO;
  wire [48:62] ga;
  reg   signed [43:12] Ov;
  wire  signed [107:12] WbvKx;
  wire  signed [21:109] g;
  wire  signed [49:37] Hgz;
  wire [53:34] qgy;
  wire z;
  reg  fQtnz;
  wire [120:79] Jq;
  wire GxuhE;
  wire [7:108] zbHlv;
  wire [9:1] BX;
  wire  signed l;
  wire [1:14] kW;
  reg  [91:88] Pt;
  reg  [81:127] mAdeN;
  reg  [105:124] tt;
  wire [60:4] F;
  reg  [15:102] p;
  wire  signed [112:76] hL_j;
  wire Ixt;
  reg  [125:32] AeZo;
  wire  signed _O;
  wire [67:19] uWMU;
  reg  hzaMqr;
  wire [100:102] reYP;
  wire  signed [95:98] LHLT;
  wire [127:67] tXYX;
  wire  signed [110:106] QKKf;
  wire BkjNU;
  wire [1:63] u;
  reg   signed uNrf;
  wire  signed DcXt;
  reg  WtPJe;
  reg  [34:85] m;
  wire  signed [38:100] Ho;
  wire [16:113] pG;



  assign BkjNU = (((P ? p : (GxuhE<(((((119'haecb43cd<77'h22698c77) ? 32'h33b13e75 : F)>>((10'h4a+66'hdb1dfad2)==(|hzaMqr)))>>43'h4b11701)<=85'h62a73edf)))||(&(zbHlv<30'h2cdcca96)))==76'h676dab9c);


endmodule
