module task_example (a,b,c);
   input [7:0] a,b;
   output [7:0] c;
   reg [7:0]	c;
   task adder;
      input [7:0] a,b;
      output [7:0] adder;
      reg c;
      integer i;
      begin c = 0;
	 for (i = 0; i <= 7; i = i+1)
	   begin
	      adder[i] = a[i] ^ b[i] ^ c;
	      c = (a[i] & b[i]) | (a[i] & c) | (b[i] & c);
	   end
      end
   endtask
   always
     adder (a,b,c); //c is a reg
endmodule
