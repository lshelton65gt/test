// filename: test/langcov/function_auto.v
// Description:  This test uses two types of functions, automatic and
// non-automatic to make sure we can cover both.  Issue detected during
// useVerific testing. 


module function_auto(clock, in1, in2, out1, out2) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1, out2;
   reg [7:0] out1, out2;

 function automatic func1;
    input [7:0] a;
    input [7:0] b;
    reg 	r1;
    begin
    r1 = r1 | a;
    func1 = r1 | (a & b);
    end
  endfunction

 function func2;
    input [7:0] a;
    input [7:0] b;
    reg 	r1;
    begin
    r1 = r1 | a;
    func2 = r1 | (a & b);
    end
  endfunction
	       
   always @(posedge clock)
     begin: main
       out1 = func1(in1, in2);
       out2 = func2(in1, in2);
     end
endmodule
