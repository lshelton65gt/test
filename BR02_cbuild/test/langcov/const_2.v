// last mod: Mon Feb  7 16:37:56 2005
// filename: test/langcov/Display/const_2.v
// Description:  This test uses a series of signed and unsigned constants.


module const_2(clock, reset,
	       out4,
	       out8,
	       out16,
	       out32,
	       out64,
	       out128
	       );
   input clock;
   input reset;

   output [3:0] out4;
   reg [3:0] out4;

   output [7:0] out8;
   reg [7:0] out8;

   output [15:0] out16;
   reg [15:0] out16;

   output [31:0] out32;
   reg [31:0] out32;
   
   output [63:0] out64;
   reg [63:0] out64;

   output [127:0] out128;
   reg [127:0] out128;


   always @(posedge clock)
     begin: main
	out4 = -7;
	out8 = -15;
	out16 = -31;
	out32 = -63;
	out64 = -127;
	out128 = -255;
     end
endmodule
