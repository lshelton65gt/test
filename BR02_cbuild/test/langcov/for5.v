// simple bit-reorder with initializer outside the for block.
module top ( count, clk, in );
   output [4:0] count;
   reg [4:0] 	count;
   input 	clk;
   input [7:0] 	in;

   reg [7:0] 	tmp;

   always @ (posedge clk)
     begin
	tmp = in;
	for ( tmp = in ; tmp ; tmp = tmp >> 1 )
	  begin
	     if ( tmp[0] ) 
	       begin
		  count = count + 1;
	       end
	  end
     end // always @ (posedge clk)
      
endmodule // top
