/* Test some basic vector bit select on the lhs. */
module top(i1, i2, constbitsel, dynbitsel, partsel
          );
input [3:1] i1;
input [0:1] i2;

output [2:1] partsel;
output [0:3] dynbitsel;
reg [0:3] dynbitsel;
output [-3:-3] constbitsel;

assign partsel[2:1] = i1;

always
begin
  dynbitsel[i2] = i2;
end

assign constbitsel[-2-1] = i1[1];

endmodule
