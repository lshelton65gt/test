module top(out,a,b,c,d);
  output out;
  input a,b,c,d;
  reg out;
  task t;
    output o;
    input i1,i2;
    o = i1 & i2;
  endtask
  task xor_two;
    output o;
    input i1,i2;
    o = i1 ^ i2;
  endtask
  reg 	  t1,t2;
  always @(a or b) begin
    xor_two(t1,a,b);
    xor_two(t2,c,d);
    t(out,t1,t2);
  end
endmodule
