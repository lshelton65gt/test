
module top(a, b, c, clk);

   input c;
   output a, b;
   input  clk;
   reg    a;
   reg    b;

   initial a = 1'b1;
   
   task mytask;
      output t0;
      output t1;
      input tin;
      reg tmp;
      
      begin
         tmp = t0;
         t1 = tmp | tin;
         t0 = tin;
      end
   endtask // mytask

   always @(posedge clk)
     mytask(a, b, c);

endmodule // top
