#!/bin/bash
# This script is based on sandbox/test/write-test.run.template, see that file for additional comments

# 
# bug4290 fails with NU_ASSERT  errors because of $fopen function to task conversion
XTEXIT=" \
       bug4290 \
       "

# bug4290_c causes NC to issue an error for the empty parens in $random()
#                     and infinite loop when I remove the ().
#           causes Aldec to infinite-loop
#
XPASSNOSIM=" \
	bug4290_c \
"

# by default we expect the recursive testcases to fail.  But we also
# will test those cases with -recursiveTasks in which case we expect
# them to succeed.  recursive_1 is XPASS without needing -recursiveTasks
# because the recursive tasks are all dead.  We have adequate coverage
# of the error messages with recursive tasks exist with these tests
# below.
XPEXIT=" \
       tf11_f_2 \
       tf13_tf \
       recursive_2\
       factorial\
       facttask\
       fact_mutual\
       "

XPASS="
       tf1_t tf1_f \
       tf2_t tf2_f \
       tf3_f \
       tf4_f \
       tf5_f \
       tf6 \
       tf7_f \
       tf8_f \
       tf9_f \
       tf10_t \
       tf11_f \
       tf11_f_3 \
       tf12_t tf12_t_1 tf12_t_2 tf12_t_3 \
       tf14_t \
       bug3266 bug3266_b \
       reset_split reset_split2 \
       alias1 \
       unInitOutRead1 \
       unInitOutRead2 \
       unInitOutRead3 \
       unInitOutRead4 \
       unInitOutRead5 \
       unInitOutRead6 \
       unInitOutRead7 \
       recursive_1 \
       bug4290_b \
       badrandom \
       "

XPASS_RECURSE="\
       recursive_2\
       factorial\
       facttask\
       fact_mutual\
"

TRAILINGCBUILDOPTS=""

		###
		###   Define 'base' cbuild command
		###


CBUILD="cbuild -q"

# set TESTOUTPUT on windows so cbuilds don't use the same exec and lib names
if [ "$WINX" == 1 ]; then
  TESTOUTPUT=1
fi

# uncomment the following line to force each test to generate it's own output directory
#TESTOUTPUT=1

for Test in $XPASS_RECURSE; do
  if [ "$1" == "-quick" ] ; then
      echo "$CBUILD $Test.v -noFlatten -noMergeUnelab -recursiveTasks >& $Test.recurse.log"
  else
      echo "$CBUILD -testdriver $Test.v -recursiveTasks -o lib$Test.$STATEXT >& $Test.recurse.log"
      echo "runtest ./$Test.exe >& $Test.sim.log"
  fi
done

## for tf0_t and tf0_f we want a warning message to appear, but currently cbuild is silent (bug 2299)
echo "TDIFF $CBUILD  tf0_t.v >& tf0_t.cbld.log"
echo "TDIFF $CBUILD  tf0_f.v >& tf0_f.cbld.log"

############# below this line you should not need to modify this file

# pick up common routines, and write tests for standard variable lists
source $CARBON_HOME/test/write-test.common
f_writeTestsForStandardLists "$@ $TRAILINGCBUILDOPTS"


# and some extra tests

# Using an 'inline' directive on a task/function with hierarchical references produces a warning message.
echo "$CBUILD -verboseTFRewrite -directive htf.dir htf.v >& htf.cbld.log"

# Normal build without task/function inlining.
echo "$CBUILD -verboseTFRewrite tf.v >& tf.cbld.log"

# Build asking that all tasks get inlined.
echo "$CBUILD -verboseTFRewrite -inlineTasks tf.v >& tf-inlineTasks.cbld.log"

# Build with an inline directive.
echo "$CBUILD -verboseTFRewrite -directive inline.dir tf.v >& tf-inline.cbld.log"

# Build requesting that only singly called tasks get inlined.
echo "$CBUILD -verboseTFRewrite -inlineSingleTaskCalls tf.v >& tf-single-call.cbld.log"

# Build with a bad module directive.
echo "$CBUILD -verboseTFRewrite -directive bad-module.dir tf.v >& tf-bad-module.cbld.log"

# Build with a bad task directive.
echo "$CBUILD -verboseTFRewrite -directive bad-task.dir tf.v >& tf-bad-task.cbld.log"

# Build with an inline directive using a module-wildcard.
echo "$CBUILD -verboseTFRewrite -directive wild-module.dir tf.v >& tf-wild-module.cbld.log"

# Build with an inline directive using a task-wildcard.
echo "$CBUILD -verboseTFRewrite -directive wild-task.dir tf.v >& tf-wild-task.cbld.log"

# Build with a bad module wildcard.
echo "$CBUILD -verboseTFRewrite -directive bad-wild-module.dir tf.v >& tf-bad-wild-module.cbld.log"

# Build with a bad task wildcard.
echo "$CBUILD -verboseTFRewrite -directive bad-wild-task.dir tf.v >& tf-bad-wild-task.cbld.log"

# Build a testcase where task inlining occurs during buffer insertion.
echo "$CBUILD htf_buffer.v -noAtomize -enableOutputSysTasks -dumpBufferedSequentials >& htf_buffer.cbld.log"
