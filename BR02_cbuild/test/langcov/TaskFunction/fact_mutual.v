// Test mutally recursive hier-ref function version of factorial

module top(in, out, clk);
  input [4:0] in;
  output [63:0] out;
  input         clk;
  reg [63:0]    out;

  always @(posedge clk) begin
    out <= fact(in);
    //$display("fact(%d) == %d", in, out);
  end

  function [63:0] fact;
    input [4:0] in;
    if (in == 0)
      fact = 1;
    else
      fact = in * sub.fact(in - 1);
  endfunction

  sub sub();
endmodule // top

module sub;
  function [63:0] fact;
    input [4:0] in;
    if (in == 0)
      fact = 1;
    else
      fact = in * top.fact(in - 1);
  endfunction
endmodule // sub
