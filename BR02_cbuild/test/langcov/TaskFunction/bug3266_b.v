// last mod: Tue Nov 30 15:59:33 2004
// filename: test/langcov/TaskFunction/bug3266_b.v
// Description:  based on bug3266.v, this test has more function call
// indirection  in the for loop expressions.


module top(in,out);
   input [7:0] in;
   output [7:0] out;

   
   function [31:0] advance;
      input [31:0] in;
      input [31:0] value;
      advance = advance_sub(in+1, value);
   endfunction

   function [31:0] advance_sub;
      input [31:0] in;
      input [31:0] value;
      advance_sub = in + value -1;
   endfunction

   function [31:0] initialize;
      input [31:0] value;
      
      initialize = initialize_sub(value+1);
   endfunction

   function [31:0] initialize_sub;
      input [31:0] value;
      initialize_sub = value-1;
   endfunction

   function terminate_low;
      input [31:0] current;
      input [31:0] bound;
      terminate_low = (current < bound);
   endfunction
   function terminate_up;
      input [31:0] current;
      input [31:0] bound;
      terminate_up = (bound < current);
   endfunction

   integer 	   i;
   reg [7:0] 	   out;
   always @(in) begin
      out = 0;
      for (i = initialize(1);	// nested functions
	   (!terminate_low(i,0) && !terminate_up(i,8));
           i = advance(i,1))	// nested
	out[i] = in[i];
   end
endmodule
