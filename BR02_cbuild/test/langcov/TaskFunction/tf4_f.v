// last mod: Wed Jun  9 16:25:58 2004
// filename: test/langcov/TaskFunction/tf4_f.v
// Description:  This test is designed to test the handling of nested functions
// when converted to tasks.

module tf4_f(clock, in1, in2, in3, out);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   input [7:0] in3;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 0;
     end

   function [7:0] tf4_f;

      input [7:0] if1;
      input [7:0] if2;
      begin
	 tf4_f = if1 + if2;
      end
   endfunction

   function [7:0] tf4_g;

      input [7:0] ig1;
      begin
	 tf4_g = ig1 * ig1;
      end
   endfunction

   function [7:0] tf4_h;

      input [7:0] ih1;
      begin
	 tf4_h = ih1 + 10;
      end
   endfunction

   function [7:0] tf4_i;

      input [7:0] ii1;
      begin
	 tf4_i = ii1 + 1;
      end
   endfunction

   function [7:0] tf4_j;

      input [7:0] ij1;
      begin
	 tf4_j = ij1 - 1;
      end
   endfunction
   
   
   
      
   always @(posedge clock)
     begin: main
	// out =       ( ((in1 + 1)*(in1 + 1)) + ( (in2 -1) + 10 ) )
	out = tf4_f( tf4_g( tf4_i( in1 ) ), tf4_h( tf4_j( in2 ) ) );
     end
endmodule
