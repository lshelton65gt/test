// last mod: Fri Jun 11 05:59:40 2004
// filename: test/langcov/TaskFunction/tf8_f.v
// Description:  This test is designed to test the situation where  there are 2
// sub modules each with nested function definitions within (and the functions
// have identical names between the modules).  Looking for name conflicts


module tf8_f(clock, in1, in2, out1, out2);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   output [7:0] out1;
   output [7:0] out2;

   sub1 i1 (out1, in1, clock);
   sub2 i2 (out2, out1, clock);

endmodule


module sub1 (o, i, clock);
   output [7:0] o;
   input  [7:0] i;
   input 	clock;
   reg [7:0] o;

   initial
     o = 8'hAA;

   
   function [7:0] tf8_f_1;

      input [7:0] ia1;
      begin
	 tf8_f_1 = tf8_f_2(ia1);
      end
   endfunction

   function [7:0] tf8_f_2;

      input [7:0] ia2;
      begin
	 tf8_f_2 = ~ ia2;
      end
   endfunction
   
   
      
   always @(posedge clock)
     begin: main
	o = tf8_f_1(i);
     end
endmodule

module sub2 (o, i, clock);
   output [7:0] o;
   input  [7:0] i;
   input 	clock;
   reg [7:0] o;

   initial
     o = 8'hAA;

   
   function [7:0] tf8_f_1;

      input [7:0] ia1;
      begin
	 tf8_f_1 = tf8_f_2(ia1);
      end
   endfunction

   function [7:0] tf8_f_2;

      input [7:0] ia2;
      begin
	 tf8_f_2 = {ia2[3:0], ia2[7:4]};
      end
   endfunction
   
   
      
   always @(posedge clock)
     begin: main
	o = tf8_f_1(i);
     end
endmodule
