// last mod: Wed Jun  9 16:31:52 2004
// filename: test/langcov/TaskFunction/tf3_f.v
// Description:  This test is designed to test the handling of nested and
// multiple functions when converted to tasks.

module tf3_f(clock, in1, in2, in3, out1, out2, out3);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   input [7:0] in3;
   output [7:0] out1;
   output [7:0] out2;
   output [7:0] out3;
   reg [7:0] 	out1;
   reg [7:0] 	out2;
   reg [7:0] 	out3;

   initial
     begin
	out1 = 0;
	out2 = 0;
	out3 = 0;
     end

   function [7:0] tf3_and;

      input [7:0] ia1;
      input [7:0] ia2;
      begin
	 tf3_and = ia1 & ia2;
      end
   endfunction

   function [7:0] tf3_or;

      input [7:0] io1;
      input [7:0] io2;
      begin
	 tf3_or = io1 | io2;
      end
   endfunction
   
      
   always @(posedge clock)
     begin: main
	out1 = tf3_or  ( tf3_and(in1, in2), tf3_and(in2, in3) );
	out2 = tf3_and ( tf3_and(in1, in2), tf3_and(in2, in3) );
	out3 = tf3_and ( tf3_or(in1, in2),  tf3_or(in2, in3)  );
     end
endmodule
