module top(out,a,b);
  output out;
  input a,b;
  reg out;
  always @(a or b) begin
    d.t(out,a,b);
  end
  declarations d();
endmodule

module declarations();
  task t;
    output o;
    input i1,i2;
    o = i1 & i2;
  endtask
endmodule
