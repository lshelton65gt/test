// last mod: Wed Nov  3 13:30:10 2004
// filename: test/langcov/TaskFunction/tf11_f.v
// Description:  This test is designed to test the situation where a hierarhical
// function is called.  (inspired by bug2333)


module tf11_f(clock, in1, in2, out1);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   output [7:0] out1;

   function [7:0] tf11_f_1;

      input [7:0] ia1;
      begin
	 tf11_f_1 = ~ ia1;
      end
   endfunction

   sub1 i1 (out1, in2);

endmodule


module sub1 (o, i);
   output [7:0] o;
   input  [7:0] i;

   assign o = tf11_f.tf11_f_1(i);

endmodule


