// last mod: Fri Oct 14 15:10:53 2005
// filename: test/langcov/TaskFunction/recursive_2.v
// Description:  This test was inspired by some work at Cisco where cbuild got
// caught in an infinite loop while trying to remove a pair of mutually
// recursive tasks/functions.


module recursive_2(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
//   reg [7:0] out1;
   reg [7:0] tmp;
   wire [7:0] unused;
   

   task recursive_t_1;
      output [7:0] recursive_t_1_out;
      input [7:0] i1;
      input sel;
      reg [7:0] tf0x;
      begin
	 if (sel) 
	   recursive_t_1_out = i1;
	 else
	    begin
	       u1.recursive_t_2(recursive_t_1_out,i1,(!sel));
	    end
      end
   endtask

   mid u1(clock, in1, in2, out1);
   
endmodule

module mid(clock, in1, in2, out1);
   
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;
   reg [7:0] tmp;
   wire [7:0] unused;


   task recursive_t_2;
      output [7:0] recursive_t_2_out;
      input [7:0] i1;
      input sel;
      reg [7:0] tf0x;
      begin
  	 if (sel) 
  	   recursive_t_2_out = i1;
  	 else
  	   begin
  	      recursive_2.recursive_t_1(recursive_t_2_out,i1,(!sel));
  	   end
      end
   endtask

   always @(posedge clock)
     begin: main
	recursive_t_2(out1, in1, in2[0]);
     end

endmodule
