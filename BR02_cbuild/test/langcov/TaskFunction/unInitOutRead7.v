
module top(a, b, c, clk);

   input [1:0] c;
   output [1:0] a;
   output [1:0] b;
   input  clk;
   reg   [1:0]  a;
   reg [1:0]    b;
   
   initial a = 1;
   
   task mytask;
      output [1:0] t0;
      output [1:0] t1;
      input [1:0] tin;
      reg tmp;
      
      begin : stupid_cases
         t0[0] = 1'b0;
         
         case (tin)
           2'b00: t0[1] = t1[0];
           2'b01: t0[1] = tin[0];
           2'b10: t0[1] = 1'b1;
           2'b11: t0[1] = 1'b0;
         endcase // case(t1)
         
         t1 = {t0[0], tin};
         
      end
   endtask // mytask
   
   always @(posedge clk)
     mytask(a, b, c);

endmodule // top
