// last mod: Fri Jun 11 06:18:28 2004
// filename: test/langcov/TaskFunction/tf7_f.v
// Description:  This test is designed to test the situation where 
// nested functions are defined in a nested module.

module tf7_f(clock, in1, out);
   input clock;
   input [7:0] in1;
   output [7:0] out;

   sub i2 (out, in1, clock);

endmodule


module sub (o, i, clock);
   output [7:0] o;
   input  [7:0] i;
   input 	clock;
   reg [7:0] o;


   
   function [7:0] tf7_f_1;

      input [7:0] ia1;
      begin
	 tf7_f_1 = tf7_f_2(ia1);
      end
   endfunction

   function [7:0] tf7_f_2;

      input [7:0] ia2;
      begin
	 tf7_f_2 = ~ ia2;
      end
   endfunction
   
   
      
   always @(posedge clock)
     begin: main
	o = tf7_f_1(i);
     end
endmodule
