// last mod: Thu Jun 10 07:04:40 2004
// filename: test/langcov/TaskFunction/tf6.v
// Description:  This test is designed to test the situation where 
// a task calls nested functions.

module tf6(clock, in1, in2, in3, out);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   input [7:0] in3;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 0;
     end


   function [7:0] tf6_v;

      input [7:0] ia1;
      begin
	 tf6_v = ~ ia1;
      end
   endfunction
   
   function [7:0] tf6_a;

      input [7:0] ia1;
      input [7:0] ia2;
      begin
	 tf6_a = tf6_v(ia1) & ia2;
      end
   endfunction

   task tf6_b;
      output [7:0] o1;
      input [7:0] io1;
      input [7:0] io2;
      input [7:0] io3;
      reg [7:0]   temp;

      begin
	 temp = tf6_a( io1, io2 );
	 o1 = temp | io3;
      end
   endtask

   
      
   always @(posedge clock)
     begin: main
	tf6_b(out, in1, in2, in3);
     end
endmodule
