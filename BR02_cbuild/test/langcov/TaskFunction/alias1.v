// bug 4400 -- aliasing args of task gives wrong answers, because
// Verilog requires copy-in, copy-out semantics.  When the nets are
// wide we pass by const reference and aliasing messes us up.  Note
// that -inlineTasks gets us right answers.

module top(in1, out, clk);
  input [127:0] in1;
  input         clk;
  output [127:0] out;
  reg [127:0]    out;

  initial out = 128'b0;

  always @(posedge clk)
    mytask(out, out, in1);

  task mytask;
    output [127:0] o;
    input [127:0] i1, i2;

    begin
      o = i2;
      o = i1 ^ i2;
    end
  endtask // mytask
endmodule // top

    