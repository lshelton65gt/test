// last mod: Thu Feb 17 09:45:49 2005
// filename: test/langcov/TaskFunction/reset_split.v
// Description:  When created this test would NU_ASSERT because the
// local variables required for the output of the function (converted to a task)
// were not distributed to the proper block after the always block is split for
// reset handling.  Found while fixing bug 3843.

module reset_split ( clk, rst1, in, out, out2 );
   input clk;
   input rst1;
   input in;
   output out, out2;
   reg 	  out, out2;

   function  func1;
      input  i1;
      begin
	 func1 = ~i1;
      end
   endfunction 
   
   initial begin
      out = 1'b0;
      out2 = 1'b0;
   end

   always @(posedge clk or posedge rst1)
     if (rst1) begin
	out2 <= func1(1'b0);
     end else begin
	out <= func1(in);
	out2 <= in;
     end

endmodule
