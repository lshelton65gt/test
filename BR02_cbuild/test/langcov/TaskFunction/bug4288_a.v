// last mod: Wed Jun  1 14:07:16 2005
// filename: test/langcov/TaskFunction/bug4288_a.v
// Description:  This test is from test/hierref/task13.v, it contains a
// hierarchical function call that has a hierarchical function call in its arg list, and at
// one time (bug4288) the task_enables for the converted functions were placed
// int he wrong place.
// to see a segfault use -inlineTasks


module top(out, in, clk);
   output [3:0] out;
   input [3:0] in;
   input clk;

   brother male_child( out, in, clk);
   sister female_child();
endmodule

module sister();

   function add;
   input a;
   input b;
   begin
     add = a + b;
   end
   endfunction

endmodule

module brother( out, in, clk );
   output [3:0] out;
   input [3:0] in;
   reg [3:0] out;
   input clk;

   always@(posedge clk)
     doit;

   task doit;
   begin
     out = female_child.add( female_child.add(out,in), in);
   end
   endtask
endmodule
