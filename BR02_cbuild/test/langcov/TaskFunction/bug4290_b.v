// last mod: Mon May  2 12:45:33 2005
// filename: test/langcov/Display/bug4290_b.v
// Description:  This test would crash as of 04/22/05, a call to $random
// is made in a submodule, and there is no place to put the task created for
// this system function.  $random should be converted to a true task.


module top(handles);
   output [63:0] handles;
   makehandle h0(handles[31:0]);
   makehandle h1(handles[63:32]);
endmodule

module makehandle(handle);
   output [31:0] handle;
   assign 	 handle = $random();
endmodule
