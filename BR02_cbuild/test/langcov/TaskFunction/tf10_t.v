module top(a,b,out);
   input a,b;
   output out;
   reg 	  out;

   task t;
      input a,b;
      output o;
      begin :named
	 reg unused;
	 reg tand;
	 reg txor;
	 tand = a & b;
	 txor = a ^ b;
	 o = tand | txor;
	 if (0) 
	   begin :subnamed
	      reg foldunused;
              foldunused = !tand;
              o = foldunused | o;
	   end
      end
   endtask

   task unused_t;
      input a;
      begin 
         out = a;
      end
   endtask

   always @(a or b) 
     begin
	t(a,b,out);
     end
endmodule
