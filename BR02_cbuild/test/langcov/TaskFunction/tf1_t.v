// last mod: Wed Jun  9 07:40:17 2004
// filename: test/langcov/tf1_t.v
// Description:  This test, and tf1_f.v are functionally identical, this one
// uses verilog tasks, while tf1_f.v uses functions.


module top(clk, sel, i1, i2, o1, o2);
   output [8:0] o1;
   output [8:0] o2;
   reg [8:0] 	o1;
   reg [8:0] 	o2;
   input [8:0] 	i1;
   input [8:0] 	i2;
   input 	clk;
   input 	sel;

   initial
      begin
	 o1 = 1;
	 o2 = 1;
      end
   
   task tf1;
      output [8:0] otf1;
      input [8:0] ia;
      input [8:0] ib;
      input sel;
      reg [8:0] foo;
      begin
	 foo = ia + ib;
	 if (sel)
	   begin : nestednamed
	      reg [8:0] bar;
	      bar = foo;
	      otf1 = bar;
	   end
	 else
	   otf1 = foo;
      end
   endtask

   task tf2;
      output  [8:0] otf2;
      input [8:0] ic;
      otf2 = ic;
   endtask

   always @(posedge clk)
     begin
	tf1(o1, i1, i2, sel);
	tf2(o2, o1);
     end

endmodule
