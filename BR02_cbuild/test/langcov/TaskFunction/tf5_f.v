// last mod: Wed Jun  9 16:32:37 2004
// filename: test/langcov/TaskFunction/tf5_f.v
// Description:  This test is designed to test the handling of nested functions
// within a continious assignment.

module tf5_f(in1, in2, in3, out);
   input [7:0] in1;
   input [7:0] in2;
   input [7:0] in3;
   output [7:0] out;

   function [7:0] tf5_f;

      input [7:0] if1;
      input [7:0] if2;
      begin
	 tf5_f = if1 + if2;
      end
   endfunction

   function [7:0] tf5_g;

      input [7:0] ig1;
      begin
	 tf5_g = ig1 * ig1;
      end
   endfunction

   function [7:0] tf5_h;

      input [7:0] ih1;
      begin
	 tf5_h = ih1 + 10;
      end
   endfunction

   function [7:0] tf5_i;

      input [7:0] ii1;
      begin
	 tf5_i = ii1 + 1;
      end
   endfunction

   function [7:0] tf5_j;

      input [7:0] ij1;
      begin
	 tf5_j = ij1 - 1;
      end
   endfunction
   
   
   assign out = tf5_f( tf5_g( tf5_i( in1 ) ), tf5_h( tf5_j( in2 ) ) );

endmodule
