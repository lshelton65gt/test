
module top(a, b, c, clk);

   input c;
   output [7:0] a;
   output [7:0] b;
   input  clk;
   reg   [7:0]  a;
   reg [7:0]    b;
   
   initial a = 1;
   
   task mytask;
      output [7:0] t0;
      output [7:0] t1;
      input tin;
      reg tmp;
      integer i;
      
      begin
         for (i = 0; (i < 7) && (t0 < 3); i = i + 1)
           begin
              t0[i] = i & 1;
           end
      end
   endtask // mytask

   always @(posedge clk)
     mytask(a, b, c);

endmodule // top
