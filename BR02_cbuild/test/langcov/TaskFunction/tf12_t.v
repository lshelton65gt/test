// last mod: Fri Nov  5 11:42:32 2004
// filename: test/langcov/TaskFunction/tf12_t.v
// Description:  This test is designed to test the situation where a hierarhical
// task has a function call in it's argument list. (inspired by bug2333)


module tf12_t(clock, in1, in2, out1, out2);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   output [7:0] out1;
   output [7:0] out2;
   reg [7:0] out2;

   function [7:0] fct1;

      input [7:0] ia1;
      begin
	 fct1 = ~ ia1;
      end
   endfunction


   sub1 i1 (out1, in1);

   always @(posedge clock)
      begin
	 // here we have a hierarchical ref to a task, with a function call in
	 // its argument list
	 tf12_t.i1.task1(out2, fct1(in2));
      end

endmodule


module sub1 (o, i);
   output [7:0] o;
   input  [7:0] i;

   task task1;
      output [7:0] out;
      input  [7:0] in;
      out = ~in;
   endtask
      
   assign o = i;

endmodule


