// last mod: Mon May  2 13:38:55 2005
// filename: test/langcov/Display/bug4290.v
// Description:  This test would NU_ASSERT as of 04/22/05, a single file is opened
// twice, and the MCDs are assigned to a partselect of a primary output via a submodule.
// the problem is that we do not handle $fopen as a function and it is not quite
// converted to a task.

module top(handles);
   output [63:0] handles;
   
   makehandle h0(handles[31:0]);
   makehandle h1(handles[63:32]);
endmodule

module makehandle(handle);
   output [31:0] handle;
   assign 	 handle = $fopen("bug4290.dat");
endmodule
