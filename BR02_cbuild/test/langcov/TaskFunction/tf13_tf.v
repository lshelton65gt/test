// last mod: original file
// filename: test/langcov/TaskFunction/tf13_tf.v
// Description:  This test has a task and a function with the same name (in the
// same context)  it should fail with message about name conflict


module tf13_tf(clock, in1, in2, outf, outt);
   input clock;
   input [7:0] in1, in2;
   output [7:0] outf, outt;
   reg [7:0] 	outf, outt;

   initial
     begin
	outf = 77;
	outt = 55;
     end

   function [7:0] tf13;

      input [7:0] i1;
      input [7:0] i2;

      begin
	 tf13 = i1 & i2;
      end
   endfunction

   task tf13;
      output [7:0] tf13;
      input [7:0] i1;
      input [7:0] i2;

      begin
	 tf13 = i1 & i2;
      end
   endtask
   
      
   always @(posedge clock)
     begin: main
	outf = tf13(in1, in2);
	tf13(outt, in1, in2);
     end
endmodule
