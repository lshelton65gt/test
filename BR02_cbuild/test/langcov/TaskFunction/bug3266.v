// last mod: Tue Nov 30 15:58:11 2004
// filename: test/langcov/TaskFunction/bug3266.v
// Description:  At one time this test demonstrated that we did not handle
// function calls as part of the terminate expression in a for loop.


module top(in,out);
  input [7:0] in;
  output [7:0] out;

  function [31:0] advance;
    input [31:0] in;
    input [31:0] value;
    advance = in + value;
  endfunction

  function [31:0] initialize;
    input [31:0] value;
    initialize = value;
  endfunction

  function terminate;
    input [31:0] current;
    input [31:0] bound;
    terminate = (current >= bound);
  endfunction

  integer i;
  reg [7:0] out;
  always @(in) begin
    out = 0;
    for (i = initialize(0);
         !terminate(i,8);
         i = advance(i,1))
      out[i] = in[i];
  end
endmodule
