// last mod: Mon Jun  6 15:24:52 2005
// filename: test/langcov/TaskFunction/tf0_f.v
// Description:  This test is designed to show that we detect the situation
// where a task or function has an implicit latch (and we should generate a warning).


module tf0_f(clock, sel, in1, out);
   input clock;
   input sel;
   input [7:0] in1;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 77;
     end

   function [7:0] tf0;

      input [7:0] i1;
      input sel;
      reg [7:0] tf0x;
      begin
	 if (sel) 
	   tf0x = i1;		// note this function does not make an
				// assignment to its output
      end
   endfunction
   
      
   always @(posedge clock)
     begin: main
	out = tf0(in1, sel);
     end
endmodule
