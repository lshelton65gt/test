
module top(a, b, c, clk);

   input [1:0] c;
   output [1:0] a;
   output [1:0] b;
   input  clk;
   reg   [1:0]  a;
   reg [1:0]    b;
   
   initial a = 1;
   
   task mytask;
      output [1:0] t0;
      output [1:0] t1;
      input [1:0] tin;
      reg tmp;
      
      begin : stupid_cases
         case (t1)
           2'b00: t0 = 2'b11;
           2'b01: t0 = 2'b10;
           2'b10: t0 = 2'b00;
           2'b11: t0 = 2'b01;
         endcase // case(t1)

         case (tin)
           {1'b1, t1[0]}: t0 = 2'b01;
           {1'b0, t1[1]}: t0 = 2'b00;
         endcase // case(t1)
         
      end
   endtask // mytask
   
   always @(posedge clk)
     mytask(a, b, c);

endmodule // top
