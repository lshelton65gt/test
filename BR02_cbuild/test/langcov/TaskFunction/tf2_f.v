// last mod: Wed Jun  9 14:50:44 2004
// filename: test/langcov/TaskFunction/tf2_f.v
// Description:  This test is designed to test the handling of nested functions
// when converted to tasks.
// See tf2_t.v for equivalent design implemented with tasks

module tf2_f(clock, in1, in2, in3, out);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   input [7:0] in3;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 0;
     end

   function [7:0] tf2_and;

      input [7:0] ia1;
      input [7:0] ia2;
      begin
	 tf2_and = ia1 & ia2;
      end
   endfunction

   function [7:0] tf2_or;

      input [7:0] io1;
      input [7:0] io2;
      begin
	 tf2_or = io1 | io2;
      end
   endfunction
   
      
   always @(posedge clock)
     begin: main
	out = tf2_or ( tf2_and(in1, in2), tf2_and(in2, in3) );
     end
endmodule
