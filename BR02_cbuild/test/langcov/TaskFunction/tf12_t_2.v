// last mod: Wed Nov  3 07:40:07 2004
// filename: test/langcov/TaskFunction/tf12_t_2.v
// Description:  This test is designed to test the situation where a hierarhical
// task has a function call in it's argument list. in this case the function
// call is also hierarchical (inspired by bug2333)


module tf12_t(clock, in1, in2, out1, out2);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   output [7:0] out1;
   output [7:0] out2;
   reg [7:0] out2;
   wire [7:0] temp1;

   function [7:0] fct1;

      input [7:0] ia1;
      begin
	 fct1 = ~ ia1;
      end
   endfunction


   sub1 i1 (temp1, in1);
   sub2 i2 (out1, temp1);

   always @(posedge clock)
      begin
	 // here we have a hierarchical ref to a task, with a hierarchical
	 // function call in its argument list, 
	 tf12_t.i1.task1(out2, i2.fct2(in2));
      end

endmodule


module sub1 (o, i);
   output [7:0] o;
   input  [7:0] i;

   task task1;
      output [7:0] out;
      input  [7:0] in;
      out = ~in;
   endtask
      
   assign o = i;

endmodule


module sub2 (o, i);
   output [7:0] o;
   input  [7:0] i;

   function [7:0] fct2;

      input  [7:0] in;
      fct2 = ~in;
   endfunction
      
   assign o = i;

endmodule


