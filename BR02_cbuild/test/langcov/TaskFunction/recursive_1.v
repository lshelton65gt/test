// last mod: Sat Oct 15 10:15:52 2005
// filename: test/langcov/TaskFunction/recursive_1.v
// Description:  This test was inspired by some work at Cisco where cbuild got
// caught in an infinite loop while trying to remove a pair of mutually
// recursive tasks/functions.


module recursive_1(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;
   wire [7:0] unused;
   

   task dead_t_1;
      output [7:0] dead_t_1_out;
      input [7:0] i1;
      input sel;
      reg [7:0] tf0x;
      begin
	 if (sel) 
	   dead_t_1_out = i1;
	 else
	    begin
	       dead_t_2(dead_t_1_out,i1,(!sel));
	    end
      end
   endtask

   task dead_t_2;
      output [7:0] dead_t_2_out;
      input [7:0] i1;
      input sel;
      reg [7:0] tf0x;
      begin
  	 if (sel) 
  	   dead_t_2_out = i1;
  	 else
  	   begin
  	      dead_t_1(dead_t_2_out,i1,(!sel));
  	   end
      end
   endtask


   
   always @(posedge clock)
     begin: main
	out1 = in1 ^ in2;
     end
endmodule

