// BUG5424 - bad behavior for funny LHS assignment from $random

module top(c,a,b,d);
   input c;
   output [15:0] a;
   output [15:0] b;
   output [31:0] d;
   reg [31:0]    t;
   assign 	 {a,b} = $random();
   assign        d = t;         // top.d & top.t marked as forceSignal
   
   always @(posedge c)
     begin
        t[30:0] = $random();
        t[31] = 1;
     end
endmodule
