// Test recursive task implementation of factorial

module top(in, out);
  input [5:0] in;
  output [63:0] out;
  reg [63:0]    out;
  reg [63:0]    fact_n_minus_1;

  always @(in) begin
    fact(out, in);
    //$display("fact(%d) == %d", in, out);
  end


  task fact;
    output [63:0] out;
    input [5:0] in;
    if (in == 0)
      out = 1;
    else begin: block
      fact(fact_n_minus_1, in - 1); // recursive call
      out = in * fact_n_minus_1;
    end
  endtask
endmodule // top


  