// last mod: Wed Jun  9 10:55:11 2004
// filename: test/langcov/TaskFunction/tf0_t.v
// Description:  This test is designed to show that we detect the situation
// where a task or function has an implicit latch (and we should generate a warning).


module tf0_t(clock, sel, in1, out);
   input clock;
   input sel;
   input [7:0] in1;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 77;
     end

   task tf0;
      output [7:0] otf0;
      input [7:0] i1;
      input sel;
      reg [7:0] tf0x;
      begin
	 if (sel) 
	   tf0x = i1;		// note this task does not make an
				// assignment to its output
      end
   endtask
   
      
   always @(posedge clock)
     begin: main
	tf0(out, in1, sel);
     end
endmodule
