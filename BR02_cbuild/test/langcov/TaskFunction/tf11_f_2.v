// last mod: Fri Nov  5 08:33:52 2004
// filename: test/langcov/TaskFunction/tf11_f2.v
// Description:  This test is designed to test the situation where a hierarhical
// function is called and there are multiple resolutions for the function.  (inspired by bug2333)


module top(clock, in1, in2, in3, in4, in5, in6, out1, out2, out3);
   input clock;
   input [7:0] in1,in2,in3,in4, in5, in6;
   output [7:0] out1,out2, out3;


   m1 i1 (out1, in1, in2);
   m2 i2 (out2, in3, in4);
   m3 i3 (out3, in5, in6);
endmodule


module m1 (o, i1, i2);
   output [7:0] o;
   input  [7:0] i1;
   input  [7:0] i2;

   assign 	o = i3.fct(i1, i2);

endmodule

module m2 (o, in1, in2);
   output [7:0] o;
   input  [7:0] in1;
   input  [7:0] in2;

   wire [7:0] 	t;

   m1 i1 (t, in1, in2);
   m4 i3 (o, t, in2);

endmodule




module m3 (o, i1, i2);
   output [7:0] o;
   input  [7:0] i1;
   input  [7:0] i2;

   function [7:0] fct;

      input [7:0] ia1;
      input [7:0] ia2;
      begin
	 fct = ia1 | ia2;
      end
   endfunction

   assign o = ~i1 & ~ i2;

endmodule


module m4 (o, i1, i2);
   output [7:0] o;
   input  [7:0] i1;
   input  [7:0] i2;

   function [7:0] fct;

      input [7:0] ia1;
      input [7:0] ia2;
      begin
	 fct = ia1 & ia2;
      end
   endfunction

   assign o = ~i1 & ~ i2;

endmodule


