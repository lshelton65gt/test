// last mod: Mon May  2 12:46:08 2005
// filename: test/langcov/Display/bug4290_c.v
// as of 05/02/05 this gets a codegen error related to unterminated argument list invoking macro "WSET_SLICE"


module top(handles);
   output [63:0] handles;
   makehandle h0(handles[31:0]);
   makehandle h1(handles[63:32]);
endmodule

module makehandle(handle);
   output [31:0] handle;
   reg [31:0] 	 temp;

   assign 	 handle = temp;
   always
      temp =  $random();
endmodule
