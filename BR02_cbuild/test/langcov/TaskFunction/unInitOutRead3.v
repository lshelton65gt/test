
module top(a, b, c, clk);

   input c;
   output [7:0] a;
   output [7:0] b;
   input  clk;
   reg   [7:0]  a;
   reg [7:0]    b;
   
   initial a = 1;
   
   task mytask;
      output [7:0] t0;
      output [7:0] t1;
      input tin;
      reg tmp;
      
      begin
         if (tin) 
           begin
              tmp = t0[2];
              t1[0] = tmp;
              t0[1] = t1[0];
           end
         else if (t1[0] == 1'b1)
           begin
              t0 = 8'hff;
              t1 = 8'h00;
           end
         else
           begin
              t0 = 8'h00;
              t1 = 8'hff;
           end
      end
   endtask // mytask

   always @(posedge clk)
     mytask(a, b, c);

endmodule // top
