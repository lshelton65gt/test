// last mod: Fri Feb 18 17:13:09 2005
// filename: test/langcov/TaskFunction/reset_split2.v
// Description:  This test is designed to demonstrate a NU_ASSERT because the
// local variables required for the output of the function (converted to a task)
// are not distributed to the proper block after the always block is split for
// reset handling. (this is like reset_split.v but with a $stop that adds
// the control flow net to the mix)

// note the gold file for this was not verified with aldec, since stop's are
// called it will not simulate correctly, but the output matches
// reset_split.sim.log.gold  which is the same as this testcase without the
// $stop.

module reset_split2 ( clk, rst1, in, out, out2 );
   input clk;
   input rst1;
   input in;
   output out, out2;
   reg 	  out, out2;

   function  func1;
      input  i1;
      begin
	 func1 = ~i1;
      end
   endfunction 
   
   initial begin
      out = 1'b0;
      out2 = 1'b0;
   end

   always @(posedge clk or posedge rst1)
     if (rst1) begin
	out2 <= func1(1'b0);
     end else begin
	out <= func1(in);
	$stop;
	out2 <= in;
     end

endmodule
