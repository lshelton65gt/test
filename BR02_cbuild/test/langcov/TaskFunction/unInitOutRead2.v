
module top(a, b, c, clk);

   input c;
   output a, b;
   input  clk;
   reg    a;
   reg    b;

   initial a = 1'b1;
   
   task mytask;
      output t0;
      output t1;
      input tin;
      reg [1:0] tmp;
      
      begin
         tmp = {t0, t1};
         t1 = tmp[1] | tin;
         t0 = tmp[0] & tin;
      end
   endtask // mytask

   always @(posedge clk)
     mytask(a, b, c);

endmodule // top
