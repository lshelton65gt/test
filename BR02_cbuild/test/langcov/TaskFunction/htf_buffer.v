module top(clk,in,sel,c,q,d,a); // carbon disallowFlattening
   input in;
   input clk;
   input sel;
   input c;
   output q;
   reg 	  q;
   input  d;
   output a;

   reg 	  a,b;
   task t1;
      t2;
   endtask

   task t2;
      sub.bottom.t3;
   endtask

   always @(posedge clk) begin
      b = a;
      q = d;
   end

   always @(posedge clk)
     begin
	t1;
	if (sel) begin
	   a = b;
	end
     end

   sub sub();
endmodule

module sub(); // carbon flattenModuleContents
   bottom bottom();
endmodule

module bottom();
   reg value;
   task t3;
      value = top.in;
   endtask
   always @(value) begin
      $display(value);
   end
endmodule
