// last mod: Fri Jun 11 13:01:33 2004
// filename: test/langcov/TaskFunction/tf9_f.v
// Description:  This test is designed to test the situation where function
// calls are uses in the arg list of a module instantation.


module tf9_f(clock, in1, in2, out1);
   input clock;
   input [7:0] in1;
   input [7:0] in2;
   output [7:0] out1;

   function [7:0] tf9_f_1;

      input [7:0] ia1;
      begin
	 tf9_f_1 = ~ ia1;
      end
   endfunction

   
   sub1 i1 (out1, tf9_f_1(in1));

endmodule


module sub1 (o, i);
   output [7:0] o;
   input  [7:0] i;

   assign o = i;

endmodule
