// Test task inlining
module top(in1, in2, sel, sel2, out, clk,
	   out1, out2, out3, out4, out5,
	   clk1, clk2);
   input clk, clk1, clk2;
   input [3:0] in1;
   input [3:0] in2;
   input       sel, sel2;
   output [3:0] out;
   reg [3:0] 	out;
   output [3:0] out1;
   reg [3:0] 	out1;
   output [3:0] out2;
   reg [3:0] 	out2;
   output [3:0] out3;
   reg [3:0] 	out3;
   output [3:0] out4;
   reg [3:0] 	out4;
   output [3:0] out5;
   reg [3:0] 	out5;

   reg [3:0] 	foo;
   reg [3:0] 	bar;

   task t;
      begin
	 if (sel)
	   out <= in1;
	 else
	   out <= in2;
      end
   endtask // t

   task t1;
      input [3:0] i;
      output [3:0] o;
      inout [3:0] io;
      reg [3:0] r;
      begin
	 r = io + i;
	 out1 <= r;
	 io = out1;
	 o = r;
      end
   endtask // t1

   task t2;
      input [3:0] i1;
      input [3:0] i2;
      output [3:0] o;
      o <= i1 & i2;
   endtask // t2

   always @(posedge clk)
     begin
	t;
     end

   always @(posedge clk1)
     begin
	foo = in2;
	t1(in1, bar, foo);
	out2 = bar & foo;
     end

   initial
     begin
	t2(1'b0, 1'b0, out3);
     end

   always @(posedge clk2)
     begin
	t2(in1, in2, out3);
     end

   task t4;
      input [1:0] i1;
      out4 <= i1 & in2;
   endtask // t4

   task t5;
      out5 <= in1 & in2;
   endtask // t5

   function [2:1] fun;
      input [3:0] i;
      fun = i[3:2] & i[1:0];
   endfunction // foo
   
   task t3;
      input sel;
      integer i;
      begin
	 if (sel)
	   case (1'b0)
	     sel2: t4(fun(in1));
	     default: t4(fun(in1|in2));
	   endcase // case(1'b0)
	 else
	   t4(fun(in2));
	 for (i = 0; i < 1; i = i + 1)
	   begin
	      t5;
	   end
      end
   endtask // t3
   
   always @(in1 or in2)
     begin
	t3(sel2);
     end

endmodule // top
