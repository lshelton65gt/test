// Test dataflow through tasks
module top(sel, a1, b1, a2, b2, o1, o2,
	   selv, av, bv, ov, oio, oiov);

   input sel, a1, b1, a2, b2;
   output o1, o2, oio;
   reg o1, o2, oio;

   input selv;
   input [3:2] av;
   input [4:5] bv;
   output [79:78] ov;
   reg [79:78] ov;
   output [0:1] oiov;
   reg [0:1] oiov;

   always
     begin
	oio = sel;
	oiov[1] = sel;
	
	multimux(sel, a1, b1, a2, b2, o1, o2, oio);
	multimux(selv, av[3], bv[5], av[2], bv[4], ov[78], ov[79], oiov[1]);
     end
   
   task multimux;
      input sel;
      input d11, d21;
      input d12, d22;
      output o1, o2;
      inout oio;
      
      begin
	 if (oio)
	   oio = d11;
	 else
	   oio = d12;
	 
	 if (sel)
	   begin
	      o1 = d11;
	      o2 = d12;
	   end
	 else
	   begin
	      o1 = d21;
	      o2 = d22;
	   end
      end
   endtask // t1
   

endmodule // top
