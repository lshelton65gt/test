// Test tasks and out of scope references
module top(i1, i2, i3, o, o1);
   input i1, i2, i3;
   output o, o1;
   reg 	  r;
   reg 	  o;
   reg 	  o1;

   task t;
      begin
	 o = r & i2;
	 r = i3;
      end
   endtask // t

   task t1;
      t;
   endtask // t1

   task t3;
      o1 <= 1'b0;
   endtask // t3

   initial
     begin
	t3;
     end

   always @(i1 or i2)
     begin
	r = i1;
	t1;
     end

endmodule // top
