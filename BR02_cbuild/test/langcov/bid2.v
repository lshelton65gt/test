/* Test multiply-driven bids. */
module top( sel1, sel2, sel3, sel4, a, b, c, d,
            bus1, bus2, fakebus3
          );
inout bus1, bus2;
output fakebus3;
input a, b, c, d, sel1, sel2, sel3, sel4;
assign bus1 = sel1 ? a : 1'bz;
assign bus1 = sel2 ? b : 1'bz;
foo inst1 (bus1, sel3, sel4, c, d);
foo inst2 (bus2, sel3, sel4, c, d);
foo inst3 (fakebus3, sel3, sel4, c, d);
endmodule

module foo(bus, sel1, sel2, i1, i2);
inout bus;
input sel1, sel2, i1, i2;
assign bus = sel1 ? i1 : 1'bz;
assign bus = sel2 ? i2 : 1'bz;
endmodule
