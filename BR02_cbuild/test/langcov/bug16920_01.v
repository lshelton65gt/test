// filename: test/langcov/bug16920_01.v
// Description:  This test duplicates the issue of bug 16920. v5.16.1
// The problem is that partselects on a vector (reg) are handled differently than partselects on a parameter.
// expected output: test_passes is always true.

module bug16920_01(input clock, input [1:0] in1, output reg [7:0] out1, out2, out3, out4, output reg test_passes);
   parameter  CONST_ARRAY_PARAM = 24'h030201;

   reg [23:0] CONST_ARRAY_REG;
   initial
     CONST_ARRAY_REG = CONST_ARRAY_PARAM; // make value of vector match the parameter

   // all variants (settings of first 2 args) of this function are expected to produce the same result
   function integer calc_width;
      input   use_param;
      input   use_increasing;
      input [1:0] in1;  
      integer 	  JJ;  
      begin
	 calc_width = 0;
	 for (JJ=0; JJ< in1; JJ=JJ+1)
	   begin
	      if ( use_param )
		begin
		   if ( use_increasing )
		     calc_width = calc_width + CONST_ARRAY_PARAM[((JJ)*8) +: 8];
		   else
		     calc_width = calc_width + CONST_ARRAY_PARAM[((JJ+1)*8)-1 -: 8];
		end
	      else 
		begin
		   if ( use_increasing )
		     calc_width = calc_width + CONST_ARRAY_REG[((JJ)*8) +: 8];
		   else
		     calc_width = calc_width + CONST_ARRAY_REG[((JJ+1)*8)-1 -: 8];
		end
	   end
      end
   endfunction


   always @(posedge clock)
     begin: main
	out1 = calc_width(0, 0, in1); // use reg vector ( -: )
	out3 = calc_width(1, 0, in1); // use param vector ( -: )
	out2 = calc_width(0, 1, in1); // use reg vector ( +: )
	out4 = calc_width(1, 1, in1); // use param vector ( +: )
	test_passes = (out1 == out2) && (out2 == out3 ) && (out3 == out4); // expect this to always be true
     end
endmodule
