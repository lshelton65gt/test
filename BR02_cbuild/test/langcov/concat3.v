// last mod: Mon May 16 13:44:06 2005
// filename: test/langcov/concat3.v
// Description:  This test, inspired by bug 4459 tests concatentation of
// integers, which is invalid verilog but accepted by some simulators.


module concat3(clock, in1, out1);
   input clock;
   input [7:0] in1;
   output [72:0] out1;
   reg [72:0] out1;

   always @(posedge clock)
     begin: main
//	out1 = {1'b1,32'b0,32'b0,in1};
	out1 = {1'b1,0,0,in1};
     end
endmodule
