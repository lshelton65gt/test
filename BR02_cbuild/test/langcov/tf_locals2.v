// Test task/function locals semantics when the function is called more than once
// in the same module with different inputs.
module mylatch(o1, sel1, i1, o2, sel2, i2);
output o1;
input sel1, i1;
output o2;
input sel2, i2;

function latch;
input reset;
input i;
reg state;
begin
  if (reset)
    state = i;
  latch = state;
end
endfunction

assign o1 = latch(sel1, i1);
assign o2 = latch(sel2, i2);

endmodule
