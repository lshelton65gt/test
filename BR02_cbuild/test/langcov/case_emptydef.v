module bug (c,d);
   input c,d;
   reg 	 a;

   always @(posedge d) begin
      case (c)
	1'b1: a = 1'b1;
	default: ;
      endcase // case(c)
   end
endmodule // bug
