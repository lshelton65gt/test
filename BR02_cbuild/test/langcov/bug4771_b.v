// last mod: Wed Jul 27 18:20:51 2005
// filename: test/langcov/bug4771_b.v
// Description:  This test was inspired by bug 4771, where the problem was caused
// by an inconsistent calculation of the size of the output of the ?: operator
// Expected results: we expect that out1 and out2 are always identical.
// this is like bug4771_a.v but the size of in3 is 4 bits

module bug4771_b(clock, sel, in3, out1, out2);
   input clock, sel;
   input [3:0] in3;
   output [7:0] out1, out2;

   wire  [7:0] temp;

   // the following is equivalent to temp = sel ? 8'h10 : in3;
   assign 	temp = sel ? 1'b1 << 4 : in3;
   
   register r1 (clock, temp, out1);

   // at one time the instantiation of r2 was incorrectly converted to:
   // register r2 (.clk(clock), .in((sel!=1'b0) ? 8'b0 : in3), .out(out2));
   register r2 (.clk(clock), .in(sel ? 1'b1 << 4 : in3), .out(out2));
   
endmodule



module register (clk, in, out);

   input clk;
   input [7:0] in;
   output [7:0]out;
   reg 	  [7:0]out;
   
        
   always @(posedge clk)
     out = in;

endmodule
