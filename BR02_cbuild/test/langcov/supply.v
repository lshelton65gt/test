/* Test supply0/1 constructs. */
module top(sel, sel2, data,
           o1, o2, o3, o4, Z1, Z0);
   output Z1, Z0;
   output [2:0] o1;
   reg [2:0] o1;
   output o2;
   reg o2;
   output [1:0] o3;
   reg [1:0] o3;
   output o4;
   input sel, sel2;
   input [2:0] data;

   supply1 VCC;
   supply0 GND;

   supply0 [5:0] vect0;
   supply1 [1:0] vect1;

   buf (Z1, VCC);
   buf (Z0, GND);

   always
   begin
     if (sel) o1 = data;
     else if (sel2) o1 = vect0;
     else o1 = vect1;
   end

   always
   begin
     if (sel) o2 = data[1];
     else if (sel2) o2 = vect0[4];
     else o2 = vect1[sel];
   end

   always
   begin
     if (sel) o3 = data;
     else if (sel2) o3 = vect0[4:3];
     else o3 = vect1[1:1];
   end

   foo inst1 (o4, VCC, GND, sel);
endmodule


module foo(o, i1, i2, sel);
input i1, i2, sel;
output o;
assign o = sel ? i1 : i2;
endmodule
