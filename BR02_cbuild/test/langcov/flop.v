module top(i, o, i1, o1, clk);
input i, i1, clk;
output o, o1;
reg o;
reg b;

always
begin
@(negedge clk) o = i;
end

always
begin : myname
b = i1;
end

assign o1 = b;

endmodule
