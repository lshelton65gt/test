module top(n1, a1, a2, a3, e1);
   input a1, a2, e1, a3;
   output n1;

   assign n1 = a2 & a3;
   bar inst (n1, a1, e1);
endmodule

module bar(o, i, e);
   output o;
   input  i, e;
   assign o = e ? i : 1'bz;
endmodule
