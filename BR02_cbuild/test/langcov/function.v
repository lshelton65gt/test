module top(i1, i2, clk, sel, o1, o2);
output [8:0] o1;
output [8:0] o2;
reg [8:0] o1;
reg [8:0] o2;
input [8:0] i1;
input [8:0] i2;
input clk;
input sel;

function [7:0] plus;
input [8:0] a;
input [8:0] b;
input sel;
reg foo;
begin
foo = a + b;
if (sel)
  begin : nestednamed
    reg bar;
    bar = foo;
    plus = bar;
  end
end
endfunction

function [8:0] simpfun;
input [9:0] i;
simpfun = i;
endfunction

always @(posedge clk)
begin
o1 <= plus(i1, i2, sel);
o2 <= simpfun(o1);
end

endmodule
