// last mod: Tue Jun 27 08:20:44 2006
// filename: test/langcov/Size/resize_01.v
// Description:  This test checks for the proper resizing of operands of an expression


module resize_01(clock, in1, in2, in4, in5, in3, out1);
   input clock;
   input in1, in2, in4, in5;
   input [30:0] in3;
   output [6:0] out1;
   reg [6:0] out1;

   reg [6:0]	     a,b;

   always @(posedge clock)
     begin: main
	a = {in1,in1,in1,in1,in1,in1,in1};
	b = {in2,in2,in2,in2,in2,in2,in2};
	out1 = (((a + b) << in4) >> in5) + (in3[27:0] == 0);
     end
endmodule
