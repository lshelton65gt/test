// last mod: Thu Oct 12 10:06:45 2006
// filename: test/langcov/Size/shift_bug.v
// Description:  This test has a codegen error


module shift_bug(
	       input clock,
	       output reg signed [255:0] out8);
   
   reg [35:0] 	      shift;
   reg [255:0] 	      pattern;

   initial
      begin
	 shift = 36'h00000000F;
	 pattern = 256'hF37f0248237f0248337f0248437f0248537f0248637f0248737f0248837f0248;
      end
   
   always @(posedge clock)
     begin: main
	out8 = $signed(pattern) >>> shift;

	shift = shift << 1;	// and move the bitpattern to the left
     end
endmodule
