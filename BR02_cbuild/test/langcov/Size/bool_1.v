// last mod: Tue Jan 24 15:49:24 2006
// filename: test/langcov/Size/bool_1.v
// Description:  This test is used to check that we size the result of a boolean
// operation correctly.  We expect out1 == out2 and out1[0] == out3


module bool_1(clock, in1, in2, out1, out2, out3, out4);
   input clock;
   input in1, in2;
   output [1:0] out1, out2;
   output 	out3;
   output 	signed [1:0] out4;
   reg [1:0] out1, out2;
   reg 	     out3;
   reg 	signed [1:0] out4;
   reg [1:0] temp2;

   always @(posedge clock)
     begin: main
	out4 = $signed(in1 < in2);
	temp2 = (in1 < in2 ) << 1;
	out1 = temp2 >> 1;
	out2 = (in1 < in2);
	out3 = (in1 < in2);
     end
endmodule
