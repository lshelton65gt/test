// this testcase captures two issues, one reported in the bug, and
// the other found accidently.
//
// The one from the netEffect testcase in bug6842 was that
// NUExpr::makeCondition was failing to resize an expression
// whose determine-bit-size was 1.  This was a problem everywhere
// but was only showing up in inlined for loops with a ternary
// expression as a condition.
//
// While trying to reproduce that problem I found another one which
// is that NUTernaryOp population was resizing its first arg to the context
// size for the NUTernaryOp.  That's wrong, and in the case of a
// mod operator, was causing an assert on that resize.  The condition
// should not be resized to the context-size of the NUTernaryOp, as it
// is self-determined.  And in fact, it shouldn't be resized before
// makeCondition is applied.

module top(clk, in, out);
  input clk;
  input [7:0] in;
  output [31:0] out;
  reg [31:0]    out;

  task fact;
    integer i;
    begin
      out = 1;
      for (i = 1 ; (in % 2) ? (i < in) : (i < (in - 1)); i = i + 1)
        out = out * i;
    end
  endtask

  always @(posedge clk)
    fact;
endmodule

    

  
