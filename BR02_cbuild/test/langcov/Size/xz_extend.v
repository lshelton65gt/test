// Test assertion in LRM 2.5.1 that unsized constants with leading X or Z
// are extended to the size of the containing expression.
module top(i,j,q,d);
   input i;
   input [3:0] j;
   inout [34:0]  q;
   output [7:0] d;
   reg [7:0]    r;

   assign q = i ? 'bz0010 : 'bz110011;
   assign d = r;
   
   always @(i or j)
     begin
        casex ({j,j,j,j,j})  // 40 bit selector
          'bx01: r=1;
          'bx10: r=2;
          'bx11: r=3;
          'bx00: r=4;
        endcase
     end
   
endmodule
