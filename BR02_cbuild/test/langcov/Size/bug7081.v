// last mod: Mon Feb 26 16:45:08 2007
// filename: test/langcov/Size/bug7081.v
// Description:  This test demonstrates a problem found by S3.  With oldSizing
// this generates an internal error because it tries to resize the in1+i
// expression down to 4 bits


module bug7081(clock, in1, in2, out1);
   input clock;
   input [1:0] in1, in2;
   output [31:0] out1;
   reg [31:0] 	out1;
   integer 	i;
   reg [7:0] 	mem [3:0];

   initial
      begin
	 for(i = 0; i < 4; i = i + 1)
	   mem[i] = 8'b1111111;
      end
   always @(posedge clock)
     begin: main
	mem[in1][in2] = 1'b1;
	for(i = 1; i < 4; i = i + 1)
          mem[in1 + i][in2] = 1'b0; // note here that the expression in1+i may
				// evaluate to a value larger than the range
				// 3:0, it does NOT wrap around

	$display("mem[0]: %b", mem[0]);
	$display("mem[1]: %b", mem[1]);
	$display("mem[2]: %b", mem[2]);
	$display("mem[3]: %b", mem[3]);
	out1 = {mem[0],mem[1],mem[2],mem[3]};
     end
endmodule


