// filename: test/langcov/Size/mismatch_01.v
// Description:  This test was getting a CARBON INTERNAL ERROR in verific flow


module mismatch_01(clock, in1, in2, out1) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   sub i1(clock, in1, in2[2:0], out1) ; // connection to input #3 is wrong size
endmodule

module sub(clock, in1, in2, out1) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule
