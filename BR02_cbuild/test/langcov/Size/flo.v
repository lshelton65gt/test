// This testcase was confusing to the new sizing methodology.  There is
// an FLO (Find Last One) optimization in Codegen.  That creates code
// that calls an x86 assembly instruction that performs this casex logic
// quickly, but it can return -1.  The self-determined size of an FLO is
// a 32-bit operation.  

module test(vec, out);
  input [3:0] vec;
  output [3:0] out;
  reg [3:0]    out;

  always @(vec) begin
    casex (vec)
      4'b1xxx:  out = 4'b1000;
      4'b01xx:  out = 4'b0100;
      4'b001x:  out = 4'b0010;
      4'b0001:  out = 4'b0001;
      default:  out = 4'b0000;
    endcase
  end
endmodule
