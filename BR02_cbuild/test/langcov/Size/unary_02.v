// last mod: Thu Jan  4 16:15:28 2007
// filename: test/langcov/Size/unary_02.v
// Description:  This test checks that the size of an operand of an unary
// operator is resized before the application of that operator, in this case the
// operand of the unary inversion is a single bit of a vector.


module unary(clock, in1,out1);
   input clock;
   input [2:0] in1;
   output [31:0] out1;
   reg [31:0] out1;

   always @(posedge clock)
     begin: main
	out1 = ~(in1[1]) + 0;	// in1[1] should be zero padded before inversion
     end
endmodule
