// filename: test/langcov/Size/bug8280.v
// Description:  This test duplicates the problem seen in bug 8280, a carbon
// internal error caused by not handling the value of the negative index of ea_3
// properly when rewriting it due to memory rescope transformation.  See also
// bug 8242.

module bug8280 (input reset,input clk, output [7:0] m2bin);
   
   reg[7:0] ea_3[7:0]; // register array
   reg[3:0] gpc_2; // general purpose counter
   wire [7:0] 	      zero_element = 0;
   
   reg[7:0] m2bin; // multiplier inputs


   initial
     begin : init_block
	integer 	   i;

	for(i=0; i<=7; i=i+1) ea_3[i] = i ;  
     end

   always @(posedge clk or posedge reset)
     begin
	if (reset)
	  begin
	     gpc_2 = -1;
	  end
	else
	  begin
	     gpc_2 = gpc_2 + 1;
	  end
     end // always @ (posedge clk or posedge reset)
   
   always @(*)
     begin : resource_share
	reg[7:0] b_store[7:-1]; 
	reg [2:0] 	   idx_b_store; 
	integer 	   i;

	
        // If I uncomment the line below, it always works.
	//    gpc_2 = 0;
	// else if I comment either of these lines or both of these lines, 
	// vspcompiler works. If not, there is an error listed for 
	// the m2bin assignment line.
	for(i=0; i<=7; i=i+1) b_store[i] = ea_3[i] ;  
	b_store[-1] = zero_element; 
	
	
	idx_b_store = gpc_2;
	// ERROR: The line below is what is listed in the vspcompiler error
	m2bin = b_store[idx_b_store] ;

	// if you enable the following $display then in event simulators you
	// will get more lines displayed than with carbon (because of the @(*)
	// sensitivity list)
	//	$display("clk: %b, gpc_2: %d, m2bin: %d",clk,gpc_2,m2bin);
	
	
   end

 endmodule
