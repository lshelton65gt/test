// last mod: Wed Mar  1 12:48:07 2006
// filename: test/langcov/Size/shift_1.v
// Description:  This test tests a fold transformation that tries to resize an
// expression to a smaller size (found in the test of  bugs/bug5449/)


module shift_1(clock, a, out1);
   input clock;
   input [274:0] a;
   output [31:0] out1;
   reg [31:0] out1;


   always @(posedge clock)
     begin: main
	out1 = ((a[181:142] &40'hff00000000)>>32) << 18;
     end
endmodule
