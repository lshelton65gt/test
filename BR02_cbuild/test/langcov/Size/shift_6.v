// last mod: Thu Oct 12 09:37:08 2006
// filename: test/langcov/Size/shift_6.v
// Description:  This test checks the proper creation of a shift guard (when the
// shift amount is a long bitvector)
// 
// this test does L and R arith shifts on signed/unsigned values
// this test does L and R logical shifts on signed/unsigned values
// the amount of the shift is determined by a value that marches across a wide vector
// -testdriverVectorCount must be at least 146 to do a complete test with this design

module shift_6(
	       input clock,
	       output reg [35:0] 	      shift,
	       output reg [260:0] out1, out2, out3, out4,
	       output reg signed [260:0] out5, out6, out7, out8);
   
   reg [260:0] 	      pattern;

   initial
      begin
	 shift = 36'h000000081;	// this shift value is important to this test,
				// we need a value that will at some time fall
				// between 0x103 and 0xff, this value does once
				// it is shifted to the left by one position

	 // the pattern shifted is not important for this test
	 pattern = 260'hF37f0248237f0248337f0248437f0248537f0248637f0248737f0248837f02489;
      end
   
   always @(posedge clock)
     begin: main
	out1 = pattern << shift;
	out2 = pattern >> shift;
	out3 = pattern <<< shift;
	out4 = pattern >>> shift;
	out5 = $signed(pattern) << shift;
	out6 = $signed(pattern) >> shift;
	out7 = $signed(pattern) <<< shift;
	out8 = $signed(pattern) >>> shift;

	shift = shift << 1;	// and move the bitpattern to the left
     end
endmodule
