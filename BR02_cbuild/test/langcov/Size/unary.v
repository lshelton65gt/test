// last mod: Wed Dec 13 14:59:53 2006
// filename: test/langcov/Size/unary.v
// Description:  This test checks that the size of an operand of an unary
// operator is resized before the application of that operator


module unary(clock, in1,out1);
   input clock;
   input in1;
   output [31:0] out1;
   reg [31:0] out1;

   always @(posedge clock)
     begin: main
	out1 = ~in1 + 0;	// in1 should be zero padded before inversion
     end
endmodule
