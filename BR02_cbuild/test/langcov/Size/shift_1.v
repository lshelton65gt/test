// last mod: Wed Mar  1 12:48:07 2006
// filename: test/langcov/Size/shift_1.v
// Description:  This test tests a fold transformation that tries to resize an
// expression to a smaller size (found in the test of  bugs/bug5449/)


module shift_1(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [31:0] out1;
   reg [31:0] out1;
   reg [274:0] a;


   always @(posedge clock)
     begin: main
	a = {17{in1,in2}};
	out1 = ((a[181:142] &40'hff00000000)>>32) << 18;
     end
endmodule
