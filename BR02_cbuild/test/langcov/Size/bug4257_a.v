// last mod: Tue Feb 28 08:22:17 2006
// filename: test/langcov/Size/bug4257_a.v
// Description:  This test is a trimmed down version of test/bugs/bug4257/bug.v,
// at one time it showed a problem where we would limit the resizing of boolean
// results to always be a single bit.
// aldec says that Reg10 is always x, carbon says it is 0

module bug4257_a(clk,   Reg10);
input clk;
output  Reg10;

  // net delcarations
  wire clk;
  reg  [31:0] Reg1;
  reg  Reg2;
  wire [96:33] Reg3;
  reg  [16:98] Reg6;
  wire [122:76] Reg7;
  wire Reg10;


  always@(posedge clk)
  begin
     Reg2 = Reg2%15;
     Reg1 = 279470273*(Reg1 );
     Reg6[50 : 63] = Reg3[Reg1[21 : 9] +: 39]; // req
  end



  assign Reg7 = Reg1[2 : 0];  // req
  assign Reg10 = (((((((|(Reg6&&Reg2)) >({(~61'h0),Reg7})))<Reg1)))); // require

endmodule
