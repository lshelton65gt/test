// filename: test/langcov/Size/bug8242_01.v
// Description:  This testcase duplicates the internal error reported in bug 8242
// the problem is that the variable 'index_data' is a constant that contains a
// value with the MSB set, and this constant was used as an index into mem_real0
// that had been declared with a range of [0:0].  It produced a carbon internal error.
// the mem_realbig has a different problem, in that it thinks it is larger than
// 2**32 bits (it is not)

module bug8242_01(outrange1,outrange2,/* outrange3,*/inrange1,inrange2 /*,inrange3*/);
   output [15:0] outrange1,outrange2,/*outrange3,*/inrange1,inrange2 /* ,inrange3*/;

   reg [127:0]   mem_real0[0:0];
   reg [127:0]   mem_real_1[-1:-1];
//   reg [127:0]   mem_realbig[32'h80000001:32'h80000001];
   reg [15:0] 	 outrange1,outrange2,outrange3,inrange1,inrange2;
//   reg [15:0] inrange3;
   wire [31:0]   index_data;

   assign index_data = 32'h80000000; // MSB set and out of range

   always @(index_data) begin
      // the following memory index operations are within range
      mem_real0[0] = 0;
      mem_real_1[-1] = -1;
//      mem_realbig[32'h80000001] = 10;

      // the following memory index operations are outside of the declared range
      outrange1 = mem_real0[index_data][15:0];
      outrange2 = mem_real_1[index_data][15:0];
//      outrange3 = mem_realbig[index_data][15:0];

      // the following memory index operations are within the declared range
      inrange1 = mem_real0[index_data & 0][15:0];
      inrange2 = mem_real_1[index_data | 32'h7FFFFFFF][15:0];
//      inrange3 = mem_realbig[index_data | 1'b1][15:0];
   end
endmodule
