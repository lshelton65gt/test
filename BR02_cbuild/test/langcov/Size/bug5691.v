// last mod: Tue Jun 27 08:16:13 2006
// filename: test/langcov/Signed/bug5691.v
// Description:  This is the test from bug 5691, at one time it would assert in makeSizeExplicit


module bug5691 (sel, in, out0, out1);
  input sel;
  input [3:0] in;
  output [1:0] out0, out1;
  assign {out1, out0} = sel ? in : 64'bzz;
endmodule
