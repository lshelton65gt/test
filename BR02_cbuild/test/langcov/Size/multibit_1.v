// last mod: Tue Jan 24 16:53:19 2006
// filename: test/langcov/Size/multibit_1.v
// Description:  This test has multiple bits in RHS but not as large as LHS


module multibit_1(clock, in1, in2, in3, out1, out2);
   input clock;
   input [3:0] in1, in2, in3;
   output [7:0] out1, out2;
   reg [7:0] out1, out2;

   always @(posedge clock)
     begin: main
	out1 = (in1 < in2) + (in1 < in3);
	out2 = { (in1 < in2), (in2 < in1), (in1 < in3), (in3 < in1), (in2< in3)}; // 5 bits in the concat, remainder must be zero padded
     end
endmodule
