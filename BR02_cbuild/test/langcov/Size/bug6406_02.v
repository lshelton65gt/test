// filename: test/langcov/Size/bug6406_02.v
// Description:  This testcase shows that cbuild is incorrectly reporting that a
// memory is larger than 2**32 even though it is a single word.

module bug6406_02(outrange3, inrange3);
   output [127:0] outrange3, inrange3;

   reg [127:0]   mem_realbig[32'h80000001:32'h80000001]; // total of 128 bits, but cbuild says range is too large
//      reg [127:0]   mem_realbig[-1:-1];   // also 128 bits, but this works fine
//      reg [127:0]   mem_realbig[2147483649:2147483649]; // also 128 bits, but this also works fine
   
   reg [127:0] 	 outrange3,inrange3;
   wire [31:0]   index_data;

   assign index_data = 32'h80000000; // MSB set and out of range

   always @(index_data) begin
      // the following memory index operations are within range
      mem_realbig[32'h80000001] = 10;
//          mem_realbig[-1] = 10;

      // the following memory index operations are outside of the declared range
      outrange3 = mem_realbig[index_data];

      // the following memory index operations are within the declared range
      inrange3 = mem_realbig[index_data | 1'b1];
   end
endmodule
