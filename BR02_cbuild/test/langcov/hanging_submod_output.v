module top(a, e, out, i, o);
  input a, e, i;
  inout out;
  output o;

  assign out = e? a: 1'bz;
  wire   b;
  assign o = b & out;

  submod submod(i, out, b);
endmodule

module submod(i, out, b);
  input i;
  output out;                   // left hanging to replicate ATI chip situation
  output b;

  assign b = ~i;
endmodule

  