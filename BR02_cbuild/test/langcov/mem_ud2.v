module bug(in, out0, out1, clk, sel0, sel1);
   input sel0, sel1;
   input [5:0] in;
   input       clk;
   output [2:0] out0, out1;

   reg [2:0] 	mem [1:0];

   assign 	out1 = mem[sel1];
   assign 	out0 = mem[sel0];

  always @ (in) begin
    mem[sel0] = in[5:3];
    mem[sel1] = in[2:0];
  end
endmodule
