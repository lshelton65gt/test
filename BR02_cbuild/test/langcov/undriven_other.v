// Test undriven integers, time
module top(o1, o2, in1);
  output o1, o2;
  input [3:0] in1;
  integer i;
  time t;
  assign o1 = i + in1;
  assign o2 = t + in1;
endmodule
