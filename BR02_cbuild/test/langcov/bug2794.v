// last mod: Wed Mar 23 16:03:14 2005
// filename: test/memories/bug2794.v
// Description:  This test tests for proper argument order for the readmem
// system task.  We expect it to fail on the first readmemh, and currently it
// will fail on the second call, but see bug 772 for why it should not


module bug2794(in, out, clk);
   input [7:0] in;
   input       clk;
   output [7:0] out;

   reg [7:0] 	mem [0:15];
   reg [127:0] 	filename;

   initial
     begin
	// note that in the following line the arguments are in the wrong order
	$readmemh(mem, "bug2794.mem.dat");

	// note that in the following line the arguments are in the right order,
	// but the filename is via a register.
	filename = "bug2794.mem.dat";
	$readmemh(filename, mem);
     end

   assign out = mem[1];
endmodule

