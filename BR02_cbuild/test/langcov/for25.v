module bug(in1, 
	   in2,
	   out1,
	   out2);
   input [3:0]  in1;
   input [1:0]  in2;
   output [3:0] out1;
   output [1:0] out2;
   reg [3:0] 	out1;
   reg [1:0] 	out2;
   integer	i;

   always @(in1) begin
      out1 = in1;
      for (i = 0; (i < 4); i = (i + 1))
	if (in1[i] === 1'bx) 
	   out1[i] = 1'b0;
   end
   always @(in2) begin
      out2 = in2;
      for (i = 0; (i < 2); i = (i + 1))
	if (in2[i] === 1'bx) 
	   out2[i] = 1'b0;
   end
endmodule
