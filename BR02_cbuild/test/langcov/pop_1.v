// last mod: Thu Feb 10 17:16:17 2005
// filename: test/langcov/pop_1.v
// Description:  at one time this code would assert because we were trying to
// take a partselect of a scalar


module top(clock, in, outv);
   input clock;
   input [7:0] in;
   output [7:0] outv;
   reg [7:0] outv;
   reg 	     s;

   always @(posedge clock)
     begin: main
	s = in;
	outv = (s[1:0]); // part is out of range
     end
endmodule
