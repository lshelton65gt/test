// last mod: Mon Feb 13 18:13:38 2006
// filename: test/langcov/bitsel_2.v
// Description:  This test checks for the proper implementation of bitselects of vectors
// both in range and out of range, as well as the proper reporting of any
// out-of-range situations
// WE EXPECT WARNINGS TO BE PRINTED

module bitsel_2(input clock,
		input [7:0] in1,
		input [1:0] in2,
		output reg Ra,Rb,Rc,Rd,Re,Rf,Rg,Rh,
		output reg [1:0] Ri,Rj,Rk,
		output [1:0] Wa,Wb);

   wire 	[1:0]  w;
   reg 		[1:0]  r;

   assign 	       w = in1[0];

`ifdef CARBON   
   assign 	       Wa[0] = r; // in range, bitsel of scalar wire on LHS
   assign 	       Wb[2] = r; // out of range, bitsel of scalar wire on LHS
`else
   // the following should be equivalent to what carbon does for the above assign statements
   assign 	       Wa = {1'bz,r[0]};
   assign 	       Wb = 2'bz;
`endif

   always @(posedge clock)
     begin: main
	r = in1[1];
`ifdef CARBON
	Ra = w;			// valid
	Rb = r;			// valid
	Rc = w[0];		// in range, bitsel of scalar wire
	Rd = r[0];		// in range, bitsel of scalar reg
	Re = w[2];		// out of range, bitsel of scalar wire
	// for some reason the following out-of-range read of r does not cause a warning in cheetah
	Rf = r[2];		// out of range, bitsel of scalar reg
	Rg = w[in2];		// possible out of range, bitsel of scalar wire
	Rh = r[in2];		// possible out of range, bitsel of scalar reg
	Ri[0] = r;		// in range, bitsel of reg on LHS
	Rj[2] = r;		// out of range, bitsel of reg on LHS
	Rk[in2] = r;		// possible out of range, bitsel of reg on LHS
`else
	// the following should be equivalent to what carbon does for the above assignments
	Ra = w[0];
	Rb = r[0];
	Rc = w[0];
	Rd = r[0];
	Re = 1'bx;
	Rf = 1'bx;
	Rg = (in2 == 0) ? w[0] : (in2 == 1) ? w[1] : 1'bx;
	Rh = (in2 == 0) ? r[0] : (in2 == 1) ? r[1] : 1'bx;
	Ri = {1'bz,r[0]};
	Rj = 2'bz;
	Rk = (in2 == 0) ? {1'bz,r[0]} : (in2 == 1) ? {r[0],1'bz} : 2'bz;
`endif	
     end
endmodule
