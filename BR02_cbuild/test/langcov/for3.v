module top ( out, clk, in );
  output [7:0] out;
  reg [7:0] out;
  input clk;
  input [7:0] in;

  integer i;
  integer j;
  always @ (posedge clk)
  begin
    // out[i] = 1 if all in[7:i] are 1; done with a double-for
    // instead of vector-based collapsing
    for ( i = 0 ; i <= 7 ; i = i + 1 )
    begin
      out[i] = 0;
      for ( j = i ; j <= 7 ; j = j + 1 )
      begin
        out[i] = out[i] | in[j];
      end // for j
    end // for i
  end // always

endmodule // top
