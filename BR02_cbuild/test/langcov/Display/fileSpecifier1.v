// test the way that file specification works (a multi_channel_descriptor) with the $fdisplay system task
// in the output file we expect to see something like:
// in module top, opened file:           2


module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;
   integer     fd2;

   initial
     out = 3;
   
//   always @ (posedge clk1)
   initial
     begin
	fd2 = $fopen("fileSpecifier1.fd2.out");
        $fdisplay(fd2, "in module top, opened file: %d", fd2);
	$fclose(fd2);
     end
endmodule // top

