// test the way that format specifiers work in $display system task
// we expect to see something like:
// in module top,  out is: 3.<-note value then dot

module top (out, in1, in2, clk1, clk2);
   output [4:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [4:0] out;

//   always @ (posedge clk1)
   initial
     begin
	out = 13;
        $display("in module top,  %s %0d%s", "out is: ", out, ".<- note value then dot");
        $display("in module top,  %s %0d%0s", "out is: ", out, ".<- note value then dot");
        $display("in module top,  %s %4d%0s", "out is: ", out, ".<- note value then dot");
	    // the following has an incorrect format size field, the fraction
	    // part is ignored
        $display("in module top,  %s %1.4d%0s", "out is: ", out, ".<- note value then dot");
        $display("in module top,  %s %4.1d%0s", "out is: ", out, ".<- note value then dot");
        $display("in module top,  %s %0d%39s", "out is: ", out, ".<- note value then 2 spaces then dot");

     end
endmodule // top

