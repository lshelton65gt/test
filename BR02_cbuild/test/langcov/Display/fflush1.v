// test the $fflush system task, for both mcd and non-mcd usage
// note this test needs to be run with -2000 because $fflush is a verilog 2001 feature


module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;
   integer     fd;
   integer     fd2;
   integer     fd3;
   integer     fd4;

   initial
     out = 3;
   
//   always @ (posedge clk1)
   initial
     begin
	fd2 = $fopen("fflush1.fd2.out"); // mcd
	fd4 = $fopen("fflush1.fd4.out"); // mcd
	fd3 = $fopen("fflush1.fd3.out","w"); // non-mcd
        $fdisplay(fd2, "in module top, item 1, opened file: %d", fd2);
        $fdisplay(fd3, "in module top, item 1, opened file: %x", fd3);
        $fdisplay(fd4, "in module top, item 1, opened file: %d", fd4);
	$fflush;		// flush everything

        $fdisplay(fd2, "in module top, item 2, opened file: %d", fd2);
        $fdisplay(fd3, "in module top, item 2, opened file: %x", fd3);
        $fdisplay(fd4, "in module top, item 2, opened file: %d", fd4);
	$fflush (fd2);		// flush individually
	$fflush (fd3);
	$fflush (fd4);

	fd = fd2 | fd4;
        $fdisplay(fd,  "in module top, item 3, opened file: %d | %d", fd2, fd4);
        $fdisplay(fd3, "in module top, item 3, opened file: %x", fd3);
	$fflush (fd);		// flush with mcd
	$fflush (fd3);	
	
        $fdisplay(fd,  "in module top, item 4, opened file: %d | %d", fd2, fd4);
        $fdisplay(fd3, "in module top, item 4, opened file: %x", fd3);

	$fclose(fd);
	$fclose(fd3);
     end
endmodule // top

