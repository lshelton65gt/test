// test the way that format specifiers work in $display system task
// in this case we test %m

// I created the gold file for this using Aldec but then I had to
// modify the results. One reason is that when we run with Aldec it
// has a different top module (the test bench). The other reason is
// that the order of the displays is different. There is no ordering
// between intial blocks.  Finally aldec has one error.  Read the output it is
// self checking

module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   integer      ichar;

   initial
      begin
	 reg3 = 3;
	 reg2 = 2;
	 ichar = 100;
      end
   
//   always @ (posedge clk1)
   initial
     begin
	$display("The top module is: %m");
     end

   mid i1 (clk1,out);
endmodule // top

module mid (clk1,out);
   input clk1;
   output [1:0] out;
   reg 		blk3_done;
   reg [1:0] out_temp;
   
   initial
     begin
	blk3_done = 0;
	$display("Here we should display top.i1\n                     : %m");
     end

   always @(posedge clk1)
     begin:blk3
	if ( ~blk3_done )
	  $display("Here we should display top.i1.blk3\n                     : %m");
	if ( ~blk3_done )
	  top.i1.i2.mytask;
	blk3_done = 1;
     end

   bot i2 (clk1,out);
endmodule


module bot (clk1, out);
   input clk1;
   output [1:0] out;
   reg [1:0] 	out;

   reg 		blk2_done;
   reg 		blk1_done;


   task mytask;
      begin:intask
	 $display("Here we should display top.i1.i2.mytask.intask\n                     : %m");
      end
   endtask

   task myfunction;
      input unused;
      begin:infunction
	 $display("Here we should display top.i1.i2.myfunction.infunction\n                     : %m");
      end
   endtask
   
   initial
     begin
	blk1_done = 0;
	blk2_done = 0;
	$display("Here we should display top.i1.i2\n                     : %m");
     end

   initial out = 2'b00;
   always @(posedge clk1)
      begin:blk1
	 out = ~out;
	 if ( ~blk1_done )
	   $display("Here we should display top.i1.i2.blk1\n                     : %m");
	 if ( ~blk1_done )
	   mytask;
	 if ( ~blk1_done )
	   myfunction(0);
	 blk1_done = 1;
      end

   always @(posedge clk1)
      begin:blk2
	 if ( ~blk2_done )
	   $display("Here we should display top.i1.i2.blk2\n                     : %m");
	 if ( ~blk2_done )
	   mytask;
	 if ( ~blk1_done )
	   myfunction(1);
	 blk2_done = 1;
      end

endmodule
