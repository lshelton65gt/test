// last mod: Fri Jun 10 09:42:11 2005
// filename: test/langcov/Display/bug2565.v
// Description:  This test is the essence of the problem from bug 2565, at one
// time (before 1.3195) we were unable to resolve a hierarchical reference.
// This test must be run with -enableOutputSysTasks because that is what
// keeps the bottom always block alive


module top(in1);
   input in1;

   mid1 mid1();

   task top_task;
      begin
      end
   endtask

endmodule


module mid1;

   bottom bottom();

endmodule



module bottom;

   always @(top.in1) begin
      $display("a");
      top.top_task; // for some reason this could not be found (before v1.3195)
   end

endmodule






