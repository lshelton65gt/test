// last mod: Wed Mar 29 11:34:35 2006
// filename: test/langcov/Display/bug5853_b.v
// Description:  This test checks how we print strings using $display("%s", reg)
// where reg contains a character sequence with embedded NULL characters.
//
// The Verilog LRM states that leading zeros are not printed when a
// string stored in a register is $displayed with a %s format.
// We interpret the phrase "leading zeros" to mean leading NULL characters.
//
// The LRM is silent about how to handle non-leading NULL
// characters.
//
// Aldec and nc print any non-leading NULL characters as spaces.



module temp(input clock,
	    output reg [4*8:1] out4,
	    output reg [4*8:1] out4b,
	    output reg [4*8:1] out4c,
	    output reg [8*8:1] out8,
	    output reg [16*8:1] out16);

   initial
     begin
	out4  =  32'h00414243;	// leading null
	out4b =  32'h00410043;	// leading and embedded null
	out4c =  32'h00414300;	// leading and trailing null
	out8  =  64'h0041424344454647; // leading null
	out16 = 128'h00414243444546470041424344454647; // leading and embedded null
     end
   
   always @(posedge clock)
     begin: main
	$display("out4       hex:%x, %s", out4, out4);
	$display("out4b      hex:%x, %s", out4b, out4b);
	$display("out4c      hex:%x, %s", out4c, out4c);
	$display("out4cout4c hex:%x, %s", {2{out4c}}, {2{out4c}});
	$display("out8       hex:%x, %s", out8, out8);
	$display("out16      hex:%x, %s", out16, out16);
	// the following concat has both a leading and embedded null
	$display("out8out8   hex:%x, %s", {out8,out8}, {out8,out8});
	$display("");
     end
endmodule
