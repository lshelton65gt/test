// a splittable block that contains a $display sys task, aldec cannot be used to
// create gold files since the order of execution of combinational and
// sequential blocks is not defined

module top (clk1, clk2, in1, in2, out);
   input clk1, clk2;
   input in1;
   input [1:0] in2;
   output [1:0] out;

   reg [1:0] BS_c1;
   reg [1:0] c2;
   reg dummy;
   reg [1:0] c1;
   reg q;
   reg [1:0] out;

   initial q = 0;
   initial begin
      dummy = 0;
      c1 = 0;
   end
   initial begin
      BS_c1 = 0;
      c2 = 0;
   end


   always @ (posedge clk1)
     begin
	q = in1;
	$display($time,,"within posedge clk1         %b %b %b", c1, c2, q);
     end

   always @(posedge clk2)
     begin
	out = BS_c1 & c2;
      if ( c1[0] )
	$display($time,,"within posedge clk2         %b %b %b", c1, c2, q);
     end

   always @(q) begin
     {dummy,c1[1]} = {1'b0,q};
      $display  ($time,,"combinational logic @q      %b %b %b", c1, c2, q);
   end
   
   always @(q or c1 or c2 or in2) begin
      c1[0] = q;
      BS_c1 = c1;
      c2 = in2;
      if ( c2 )
	$display($time,,"combinational logic various %b %b %b", c1, c2, q);
     end
   

endmodule // top

   
