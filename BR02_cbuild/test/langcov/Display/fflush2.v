// test the $fflush system task, for error messages
// note this test needs to be run with -2000 because $fflush is a verilog 2001 feature


module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;
   integer     fd;
   integer     fd2;
   integer     fd3;
   integer     fd4;

   initial
     out = 3;
   
//   always @ (posedge clk1)
   initial
     begin
	fd2 = $fopen("fflush1.fd2.out"); // mcd
	fd4 = $fopen("fflush1.fd4.out"); // mcd
	fd3 = $fopen("fflush1.fd3.out","w"); // non-mcd
        $fdisplay(fd2, "in module top, item 1, opened file: %d", fd2);
        $fdisplay(fd3, "in module top, item 1, opened file: %x", fd3);
        $fdisplay(fd4, "in module top, item 1, opened file: %d", fd4);
	$fflush(fd2, fd3);		// wrong number of args;


	fd = fd2 | fd4;
	$fclose(fd);
	$fclose(fd3);
     end
endmodule // top

