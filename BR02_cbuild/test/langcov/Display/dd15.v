// last mod: Wed May 24 09:37:09 2006
// filename: test/langcov/Display/dd15.v
// Description:  This test is a simple test of display of signed numbers that
// are not stored in machine object sizes


module dd15(input clock,
	    input signed [1:0] inA_2, input [1:0] inB_2, output reg signed [1:0] outs_2, output reg [1:0] outu_2,
	    input signed [9:0] inA_10, input [9:0] inB_10, output reg signed [9:0] outs_10, output reg [9:0] outu_10,
	    input signed [33:0] inA_34, input [33:0] inB_34, output reg signed [33:0] outs_34, output reg [33:0] outu_34,
	    input signed [65:0] inA_66, input [65:0] inB_66, output reg signed [65:0] outs_66, output reg [65:0] outu_66

	    );

   reg 		      signed [1:0] regs_2;
   reg 		      signed [9:0] regs_10;
   reg 		      signed [33:0] regs_34;
   reg 		      signed [65:0] regs_66;
   always @(posedge clock)
     begin: main
	regs_2 = inB_2;
	outs_2 = inA_2 + regs_2;
	outu_2 = inA_2 + inB_2;
	$display ("inA_2(s) %d,%b,%o,%h inB_2(u) %d,%b,%o,%h regs_2(s) %d,%b,%o,0x%h, outs_2(s) %d outu_2(u) %d  no_format: ",
		  inA_2,inA_2,inA_2,inA_2, inB_2,inB_2,inB_2,inB_2, regs_2, regs_2,regs_2, regs_2, outs_2, outu_2, regs_2);

	regs_10 = inB_10;
	outs_10 = inA_10 + regs_10;
	outu_10 = inA_10 + inB_10;
	$display ("inA_10(s) %d,%b,%o,%h inB_10(u) %d,%b,%o,%h regs_10(s) %d,%b,%o,0x%h, outs_10(s) %d outu_10(u) %d  no_format: ",
		  inA_10,inA_10,inA_10,inA_10, inB_10, inB_10, inB_10, inB_10, regs_10, regs_10,regs_10, regs_10, outs_10, outu_10, regs_10);

	regs_34 = inB_34;
	outs_34 = inA_34 + regs_34;
	outu_34 = inA_34 + inB_34;
	$display ("inA_34(s) %d,%b,%o,%h inB_34(u) %d,%b,%o,%h regs_34(s) %d,%b,%o,0x%h, outs_34(s) %d outu_34(u) %d  no_format: ",
		  inA_34,inA_34,inA_34,inA_34, inB_34, inB_34, inB_34, inB_34, regs_34, regs_34,regs_34, regs_34, outs_34, outu_34, regs_34);

	regs_66 = inB_66;
	outs_66 = inA_66 + regs_66;
	outu_66 = inA_66 + inB_66;
	$display ("inA_66(s) %d,%b,%o,%h inB_66(u) %d,%b,%o,%h regs_66(s) %d,%b,%o,0x%h, outs_66(s) %d outu_66(u) %d  no_format: ",
		  inA_66,inA_66,inA_66,inA_66, inB_66, inB_66, inB_66, inB_66, regs_66, regs_66,regs_66, regs_66, outs_66, outu_66, regs_66);
	
     end
endmodule
