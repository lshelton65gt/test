// last mod: Mon Mar 27 07:53:40 2006
// filename: test/langcov/Display/bug3578.v
// Description:  This test is from bug 3578.  The original implementation for
// $display did not support format specifiers of the form %05d (where 5 could be
// any number)  This type of specifier is not defined in the LRM but it is
// supported by some simulators.
// we now allow the following specifiers:
// %d     // width for output is large enough to hold maximum possible value (LRM)
// %0d    // width for output is minimum number of characters required (LRM)
// the following are not defined by the LRM:
// %05d   // width for output is 5 (or what ever number specified) with fill
          // char set to zero.
// %00d   // width for output is minimu number of characters required (no fill required)
//
// format specifiers other than 'd' are supported but they have a fill character
// of zero by default.

// note that the aldec and fintronic simulators do not support this extension to the format specifier.

module bug(clk, in, out);
   input clk;
   input [63:0] in;
   output [63:0] out;

   reg [63:0] 	 out;

   initial
     $display("test string :%04d, :%4d, :%0d, :%d, :%00d,", 10, 10, 10, 10, 10);
   always @(posedge clk) begin
      out <= in;
// NOTE do not use 0x to display values in generated files, these appear as
// addresses to cdsDiff and thus are ignored.
      $display("in is hex:%04h", in);
   end
endmodule
