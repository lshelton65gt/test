// last mod: Fri May 26 14:10:04 2006
// filename: test/langcov/Display/bug3329.v
// Description:  This test demonstrates how rounding is handled for real numbers.


module bug3329(clock, out1);
   input clock;
   output [7:0] out1;
   reg [7:0] out1;

   real 	r1;
   integer 	i1;

   always @(posedge clock)
      begin
   	 r1 = 3.0;
	 i1 = 3.0;
	 $display("real: %f integer %d", r1, i1);
   	 r1 = 3.1;
	 i1 = 3.1;
	 $display("real: %f integer %d", r1, i1);
   	 r1 = 3.4;
	 i1 = 3.4;
	 $display("real: %f integer %d", r1, i1);
   	 r1 = 3.5;
	 i1 = 3.5;
	 $display("real: %f integer %d", r1, i1);
   	 r1 = -3.5;
	 i1 = -3.5;
	 $display("real: %f integer %d", r1, i1);
   	 r1 = 3.6;
	 i1 = 3.6;
	 $display("real: %f integer %d", r1, i1);
	 r1 = -2.0;
	 i1 = -2.0;
	 $display("real: %f integer %d", r1, i1);
	 r1 = -2.1;
	 i1 = -2.1;
	 $display("real: %f integer %d", r1, i1);
	 r1 = -2.4;
	 i1 = -2.4;
	 $display("real: %f integer %d", r1, i1);
	 r1 = -2.5;
	 i1 = -2.5;
	 $display("real: %f integer %d", r1, i1);
	 r1 = -2.6;
	 i1 = -2.6;
	 $display("real: %f integer %d", r1, i1);
	 // some numbers larger than 32 bits (4294967295==FFFFFFFF)
	 r1 = 5000000000.4;
	 i1 = 5000000000.4;
	 $display("real: %f integer %d", r1, i1);
	 r1 = 5000000000.5;
	 i1 = 5000000000.5;
	 $display("real: %f integer %d", r1, i1);
	 r1 = -5000000000.4;
	 i1 = -5000000000.4;
	 $display("real: %f integer %d", r1, i1);
	 r1 = -5000000000.5;
	 i1 = -5000000000.5;
	 $display("real: %f integer %d", r1, i1);

	 r1 = -5000000000.00005;
	 i1 = -5000000000.00005;
	 $display("real: %f integer %d", r1, i1);

	 r1 = -5000000000.000005;
	 i1 = -5000000000.000005;
	 $display("real: %f integer %d", r1, i1);

	 r1 = -500000000000.000005;
	 i1 = -500000000000.000005;
	 $display("real: %f integer %d", r1, i1);

	 // and the final values for these variables
	 r1 = -1.6;
	 i1 = -1.6;
	out1 = i1 + r1;
      end
endmodule
