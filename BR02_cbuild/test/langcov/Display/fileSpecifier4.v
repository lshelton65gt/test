// test the way that file specification (not MCD) works with the $fdisplay system task
// in the output file we expect to see something like:
// in module top, opened file: hex:80000003


module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;
   integer     fd3;

   initial
     out = 3;
   
//   always @ (posedge clk1)
   initial
     begin
	fd3 = $fopen("fileSpecifier4.fd3.out", "w");
// NOTE do not use 0x to display values in generated files, these appear as
// addresses to cdsDiff and thus are ignored.
        $fdisplay(fd3, "in module top, opened file: hex:%h", fd3);
	$fclose(fd3);
     end
endmodule // top

