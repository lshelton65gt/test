// test the way that format specifiers with null (empty) args work in $display
// system task

// to use this test file, run with verilog and just comment out any lines that
// cause a compilation or simulation error

module top (out );
output [1:0] out;
reg [1:0] 	out;
initial
   begin
     out = 2;
   end
   
initial
 begin
  // test1
  $display("test1 empty arg: space here? ->",,"<-end test1");
//  $display("test2 empty arg like a space (%%s)? here? ->%s<-",,"end test2");
//  $display("test3 empty arg like a space (%%c)? here? ->%c<-",,"end test3");
  $write  ("test4 empty arg at end, here? ->",); $display("<- test4");
  $display(,"test5 empty arg:space at beg of this line? end test5");

  $display("test6 empty arg: doublespace here? ->",,,"<-end test6");
//  $display("test7 2 empty args for (%%s)? doublespace? ->%s<-",,,"end test7");
 end
endmodule // top

