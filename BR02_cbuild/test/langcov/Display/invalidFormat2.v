// last mod: Tue Apr 26 12:09:58 2005
// filename: test/langcov/Display/invalidFormat2.v
// Description:  This test checks to see that we report a problem with the
// format string %7.6.5e or %7.6.5f or %7.6.5g and other invalid format strings

module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   reg [1:0] 	out;

   initial
      begin
	 reg3 = 3;
	 reg2 = 2;
	 out = 2;
      end
   
   initial
     begin
	// the following are invalid format strings
	$display(" 7.5 (format: 7.3.2 ) is:\t%d e%7.3.2f", 7.5);
	$display(" 7.5 (format: 7.3.2 ) is:\te%7.3.2e", 7.5);
	$display(" 7.5 (format: 7.3.2 ) is:\the%d %d %7.3.2g", 1, 7.5);

	$display(" 7 (format: +7+d ) is:\t%+7+d", 7);
	$display(" 7 (format: +7.+d ) is:\t%+7.+d", 7);
	$display(" 7 (format: +7-d ) is:\t%+7-d", 7);

     end

endmodule // top


