// This test tests $display and $time/$stime/$realtime, with and without %t formatting

module top (clk1);
   input clk1;

   always @ (posedge clk1)
     begin
        $display($time, "<=raw $time   >%t<=fmt $time", $time);
        $display($stime, "<=raw $stime   >%t<=fmt $stime", $stime);
        $display($realtime, "<=raw $realtime   >%t<=fmt $realtime", $realtime);
     end
endmodule // top

