// This test was generated with Aldec because for some reason it //
// does not print the Signal changed message. But all the signal values
// were correct so I think it is a script issue.

module top (signal, clk, in);
   output [1:0] signal;
   input        clk;
   input [1:0]  in;

   reg [1:0]    signal, signal_prev;

   always @ (posedge clk)
     signal <= in;

   always @(signal)
     if (signal != signal_prev) begin
        signal_prev = signal;
        $display("Signal changed.");
     end

endmodule

