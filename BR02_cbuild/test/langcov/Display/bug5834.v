// last mod: Mon Mar 20 16:58:22 2006
// filename: test/langcov/Display/bug5834.v
// Description:  This test is bug5834 where it was reported that \n was printed literally


module bug5834(clk,up,down,data_in,parity_out,carry_out,borrow_out, count_out);
  input clk,up,down;
  input [8:0] data_in;
  output parity_out, carry_out, borrow_out;
  output [8:0] count_out;

  reg parity_out, carry_out, borrow_out;
  reg [8:0] count_out;
  reg [8:0] count_nxt;
  reg [9:0] cnt_up, cnt_dn;
  reg load;
     reg   [31:0]    handle;
     reg   debug_on;

function [31:0] OpenLogFile;
     input [512*8:1] filename;
     reg   [31:0]    handle;
begin
     handle = $fopen (filename);
     if (handle == 0) begin
         $display ("Error: Could not open logfile %0s", filename);
         //$stop ;
     end
     OpenLogFile = handle;
end
endfunction


  initial begin
    handle = OpenLogFile("bug5834.out");
  end
  always @(posedge clk) 
  begin
    cnt_dn = count_out - 5;
    cnt_up = count_out + 3;
    load = 1;
    case ({up,down})
      0: count_nxt = data_in;
      1: count_nxt = cnt_dn;
      2: count_nxt = cnt_up;
      3: load = 0;
    endcase
    if(load) begin
      parity_out <= ^count_nxt;
      carry_out <= up & cnt_up[9];
      borrow_out <= down & cnt_dn[9];
      count_out <= count_nxt;
    end
//     $fwrite(handle,"abc %s def \n", "\n"); // the problem here is that the "\n"
				// is not reinterpreted as an escaped character
				// during output
    $fwrite(handle,"%b %b %s %b %b ",parity_out, carry_out, "\n",borrow_out, count_out);
  end
endmodule
