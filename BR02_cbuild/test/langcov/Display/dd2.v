/* Test support for $display like system tasks */
// test of different schedules
module top (clk, val0, val1, val2, out);
   input clk;
   input [1:0] val0;
   input [1:0] val1;
   input [1:0] val2;
   output [1:0] out;

   always @( val0 ) begin
      $display("DSP0: val0 changed"); // NOTE this is only run once by carbon (more often by a verilog simulator)
   end


   always @(posedge clk) begin
      $display("DSP1: clk changed"); // this will be run every posedge clk
   end
   mid i1 (clk, val1, val2, out);
   
   always @(val1) begin
      $display("DSP2: val1 or val2 changed %b %b", val1, val2); //run every change to val1 or val2
   end

endmodule
module mid (clk, val1, val2, out);
   input clk;
   input [1:0] val1;
   input [1:0] val2;
   output [1:0] out;
   reg [1:0] reg1;
   reg [1:0] out;

   initial reg1 = 0;
   
   always @(posedge clk) begin
      reg1 = val1 + reg1;
      out = reg1;
      $display("DSP3: clk changed in mid,  reg1= %b  val1= %b", reg1, val1); // run every posedge clk
   end
endmodule
