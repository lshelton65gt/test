// test the way that file names and type specification  work
// in this case the file names are stored in registers and are of different length, so they can be stored in
// UInt32, UInt64, and longer
// in the output files we expect to see something like:
// file 8o2: in module top, opened file: hex:00000002
// file 8ofd4: in module top, opened file: hex:00000004

module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;
   integer     fd2;
   integer     fd4;
   reg [3*8:1] fd2Name;
   reg [5*8:1] fd4Name;

//   always @ (posedge clk1)
   initial
     begin
	out = 3;
	fd2Name = "8o2";
	fd4Name = "8ofd4";
	fd2 = $fopen(fd2Name);
	fd4 = $fopen(fd4Name);
// NOTE do not use 0x to display values in generated files, these appear as
// addresses to cdsDiff and thus are ignored.
        $fdisplay(fd2, "in module top, opened file: hex:%h", fd2);
        $fdisplay(fd4, "in module top, opened file: hex:%h", fd4);
	$fclose(fd2);
	$fclose(fd4);
     end
endmodule // top

