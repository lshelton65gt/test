// last mod: Fri May 18 17:23:15 2007
// test the way that format specifiers work in $display system task
// in this case we test empty format specifiers
// $display()   should: 1) LRM: print a newline character, 2) NC: print a newline
// $display("") should: 1) LRM: print a newline character, 2) NC: print a newline
// $write()     should: 1) LRM: print nothing, 2) NC: print a single space
// $write("")   should: 1) LRM: print nothing, 2) NC: print nothing
// NOTE: gold file is from NC and does not match aldec.  Aldec matches the LRM

module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [1:0] 	out;

   
   always @ (posedge clk1)
//   initial
     begin
	out = 1;
	$display("start, out=", out);
	$display();
	$display("");
	$display("middle, this line should be preceded by 2 blank lines, out=", out);
	$write();
	$write("");
	$display("end, this line should be preceded by 0 blank lines and start with a single space, out=", out);
     end
endmodule // top

