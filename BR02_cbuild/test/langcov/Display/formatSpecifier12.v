// This test comes from bug3809 with some modifications. I added the
// $display's at the top level to test the unflattened $displays.
//
// Note that this test is different from the formatSpecifier10
// in that it has multiple instances of modules so it makes
// sure that it works correctly with flattening.

module top(clk1, clk2, reset1, reset2, out1, out2, out3, out4);
   input clk1, clk2, reset1, reset2;
   output [31:0] out1, out2, out3, out4;

   twocounter TW1 (clk1, clk2, reset1, reset2, out1, out2);
   twocounter TW2 (clk1, clk2, reset1, reset2, out3, out4);

   always @(posedge clk1)
     begin
        if (out1 == 1'b0)
          $display("(%m) out1 == 0", out1);
        if (out3 == 1'b0)
          $display("(%m) out1 == 0", out1);
     end

endmodule


// This demonstrates compiling and running a simple design with
// two counters driven by two clocks

module twocounter(clk1, clk2, reset1, reset2, out1, out2);
  input clk1, clk2, reset1, reset2;
  output [31:0] out1, out2;
//reg [31:0] out1, out2;

/**
  always @(posedge clk1)
    if (reset1)
      out1 <= 32'b0;
    else
      out1 <= out1 + 32'd1;

  always @(posedge clk2)
    if (reset2)
      out2 <= 32'b0;
    else
      out2 <= out2 + 32'd3;
**/

  onecounter #(1) cnt1 (clk1, reset1, out1);
  onecounter #(3) cnt2 (clk2, reset2, out2);

endmodule

module onecounter(clk, reset, out);
  input clk, reset;
  output [31:0] out;
  reg [31:0] out;

  parameter INC= 32'd5;

  initial out = 0;

  always @(posedge clk)
    if (reset)
      out <= 32'b0;
    else begin
       out <= out + INC;
       $display("(%m) RTL Checker Fired, %d", out); 
    end
endmodule

