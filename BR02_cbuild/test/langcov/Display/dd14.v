// last mod: Mon Jun 20 14:46:11 2005
// filename: test/langcov/Display/dd14.v
// Description:  This test is a $display of a string stored in a reg where arith
// is done on that value


module dd14(clk1, out);
   output [31:0] out;
   input 	 clk1;
   reg 	 [31:0] out;
   reg   [3:0] reg4;
   reg   [7:0] reg8;
   reg   [31:0] reg32;
   reg   [63:0] reg64;
   reg   [79:0] reg80;

   
   initial
     begin
	out = "abcd";
	reg4 = "abcd";
	reg8 = "abcd";
	reg32 = "abcd";
	reg64 = "abcdefgh";
	reg80 = "abcdefghij";

	$display("(abcd + 1)[3:0]",   " should be: ascii: ^E");
	$display("%s + 1",reg4, " actually:  ascii: %s", reg4+1);
	$display("abcd + \"C\" - \"A\"",   " should be: ascii: abcf");
        $display("%s + \"C\" - \"A\" ",reg4, " actually:  ascii: %s", reg4+"C"-"A");

	$display("(abcd + 1)[7:0]",   " should be: ascii: e");
	$display("%s + 1",reg8, " actually:  ascii: %s", reg8+1);
	$display("abcd + \"C\" - \"A\"",   " should be: ascii: abcf");
        $display("%s + \"C\" - \"A\" ",reg8, " actually:  ascii: %s", reg8+"C"-"A");
	
	$display("abcd + 1",   " should be: ascii: abce");
        $display("%s + 1",reg32, " actually:  ascii: %s", reg32+1);
	$display("abcd + \"C\" - \"A\"",   " should be: ascii: abcf");
        $display("%s + \"C\" - \"A\" ",reg32, " actually:  ascii: %s", reg32+"C"-"A");


     end
endmodule // top

