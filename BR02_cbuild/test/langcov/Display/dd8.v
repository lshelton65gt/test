// At one time this test demonstrated how carbon and verilog simulators generate different
// results when a $display shows more values than the block is sensitive to.
// we expect that there will be a difference between a verilog simulator and carbon simulator
// as of 04/04/05 carbon uses guards variable tests around blocks that contain
// $display operations, and so carbon and other simulatores more closely match.

module top (out, a1, a2);
   output [1:0] out;
   input a1;
   input a2;
   reg 	 [1:0] out;
   
   always @ (a1)
     begin
	out = {a1, a2};
        $display($time,,"value change: ", a1, a2 );
     end
endmodule // top

