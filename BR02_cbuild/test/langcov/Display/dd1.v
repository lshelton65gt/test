/* Test support for $display like system tasks */
module top (clk, out, address);
   input clk;
   input [2:0] address;
   output [3:0] out;
   reg [3:0] 	out;
   reg [3:0] mem1 [2:0];
   initial
      begin
	 mem1[0] = 0;
	 mem1[1] = 1;
	 mem1[2] = 2;
      end
   
   always @(posedge clk) begin
      $display("display text");
      $display($time,,"current time");
      out = mem1[address];
      $display("mem1[0] = %b", mem1[0]);

      mem1[0] = 7;
      $display("mem1[0] = %b", mem1[0]);
      out = mem1[address];      // keep it alive

      $display("mem1[1] = %b", mem1[1]);      
   end
   
endmodule
