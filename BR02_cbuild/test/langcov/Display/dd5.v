// simple example that shows problem  found by rsayde where the output sys task net was being removed

module top (out, in1, in2, clk);
   output out;
   input  in1, in2, clk;

   wire   rclk = clk & in1;
   sub S1 (out, rclk, in1, in2);

endmodule // top

module sub (out, clk, i1, i2);
   output out;
   input  clk, i1, i2;

   always @ (posedge clk)
     $display("from sub i1=", i1);

   assign out = i2;

endmodule // sub
