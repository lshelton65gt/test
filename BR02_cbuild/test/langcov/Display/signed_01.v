// last mod: Thu Nov 30 13:07:04 2006
// filename: test/langcov/Display/signed_01.v
// Description:  This test checks the proper display of signed numbers with
// sizes other than POD size (like 1 bit signed values)

module reg_vector_signed(clk);
   input clk;

   reg signed [1:1] s_r_1;
   reg signed [2:1] s_r_2;
   reg signed [4:1] s_r_4;
   reg signed [8:1] s_r_8;
   reg signed [9:1] s_r_9;
   reg signed [15:1] s_r_15;
   reg signed [16:1] s_r_16;
   reg signed [17:1] s_r_17;
   reg signed [33:1] s_r_33;
   
   initial
     begin
	s_r_1 = -1;
	s_r_2 = -1;
	s_r_4 = -1;
	s_r_8 = -1;
	s_r_9 = -1;
	s_r_15 = -1;
	s_r_16 = -1;
	s_r_17 = -1;
	s_r_33 = -1;
     end

   always @ (posedge clk)
      begin
	 s_r_1 = s_r_1 - 1;
	 s_r_2 = s_r_2 - 1;
	 s_r_4 = s_r_4 - 1;
	 s_r_8 = s_r_8 - 1;
	 s_r_9 = s_r_9 - 1;
	 s_r_15 = s_r_15 - 1;
	 s_r_16 = s_r_16 - 1;
	 s_r_17 = s_r_17 - 1;
	 s_r_33 = s_r_33 - 1;
      end

   always @ (negedge clk)
      begin
	 $display("s_r_1: %%h%h, %%o%o, %%b%b, %%d%d", s_r_1,s_r_1,s_r_1,s_r_1);
	 $display("s_r_2: %%h%h, %%o%o, %%b%b, %%d%d", s_r_2,s_r_2,s_r_2,s_r_2);
	 $display("s_r_4: %%h%h, %%o%o, %%b%b, %%d%d", s_r_4,s_r_4,s_r_4,s_r_4);
	 $display("s_r_8: %%h%h, %%o%o, %%b%b, %%d%d", s_r_8,s_r_8,s_r_8,s_r_8);
	 $display("s_r_9: %%h%h, %%o%o, %%b%b, %%d%d", s_r_9,s_r_9,s_r_9,s_r_9);
	 $display("s_r_15: %%h%h, %%o%o, %%b%b, %%d%d", s_r_15,s_r_15,s_r_15,s_r_15);
	 $display("s_r_16: %%h%h, %%o%o, %%b%b, %%d%d", s_r_16,s_r_16,s_r_16,s_r_16);
	 $display("s_r_17: %%h%h, %%o%o, %%b%b, %%d%d", s_r_17,s_r_17,s_r_17,s_r_17);
	 $display("s_r_33: %%h%h, %%o%o, %%b%b, %%d%d", s_r_33,s_r_33,s_r_33,s_r_33);
      end
endmodule


module signed_01(clock);
   input clock;
   reg_vector_signed  reg_vector_signed1(clock);
endmodule
