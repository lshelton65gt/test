module param_disp(clk, val);
  input clk;
  input val;

  display #("hello, world!   %d") u1(clk, val);
  display #("goodbye, world! %d") u2(!clk, val);
endmodule

module display(clk, val);
  input clk, val;
  parameter format = "format %d";

  always @(posedge clk)
    $display(format, val);
endmodule
