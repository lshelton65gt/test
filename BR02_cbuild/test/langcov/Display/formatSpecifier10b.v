// last mod: Wed Jul 13 16:37:22 2005
// filename: test/langcov/Display/formatSpecifier10b.v
// Description:  This test is used to check that we produce the proper warning
// messages if the user does not specify -enableOutputSysTasks but does use a
// noOutputSysTasks directive for one or modules.  We expect warnings.
// This test just uses formatSpecifier10.v with a copy of the directives from formatSpecifier10a


`include "formatSpecifier10.v"
