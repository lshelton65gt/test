// This test tests `timescale, $realtime, and $display

`timescale 1ns/1ns

module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   real  realtimereg;

   middle iMiddle(out, in1, in2, clk1, clk2);

   always @ (posedge clk1)
     begin
	realtimereg = $realtime;
        $display($realtime, "%t, %t module top %b", $realtime, realtimereg, {in1,in2});
     end
endmodule // top

`timescale 10us/1us

module middle (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg  reg_out;
   real  realtimereg;

   assign    out[0] = reg_out;
   
   bottom ibottom(out[1], in1, in2, clk2);
   
   always @(posedge clk1) begin
      reg_out <= in2[0];
      if ( in1 )
	 begin
	    realtimereg = $realtime;
            $display($realtime, "%t, %t module middle %b", $realtime, realtimereg, {in1,in2});
	 end
   end
   

endmodule // middle

`timescale 1ps/100fs

module bottom (out, in1, in2, clk2);
   output out;
   input in1;
   input [1:0] in2;
   input clk2;
   reg out;
   real  realtimereg;

   always @(posedge clk2) begin
      out <= in2[1];
      if ( in1 )
	begin
	   realtimereg = $realtime;
           $display($realtime, "%t, %t module bottom: %b", $realtime, realtimereg, {in1,in2});
	end
   end
   

endmodule // bottom
