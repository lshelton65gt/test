// test for invalid format/argument specifications

// null argument is not a string or char

// this testcase should fail

module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   reg [1:0] 	out;

//   always @ (posedge clk1)
   initial
     begin
	reg3 = 3;
	reg2 = 2;
	out = reg3 - reg2;
	$display("a null argument should result in a space in the output, but this shows that it is not really a string or char argument");
	$display("1->%d<-",,"done");
	$display("2->%s<-",,"done");
	$display("3->%c<-",,"done");
     end
endmodule // top

