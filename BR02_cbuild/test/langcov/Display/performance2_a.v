// last mod: Tue Oct  5 13:58:41 2004
// filename: test/langcov/Display/performance2_a.v
// Description:  This test attempts to dupliate the situation seen at sony where
// we are seeing low performance for printing of values


module performance2_a(clock, in1, in2, in3, in4);
   input clock;
   input [7:0] in1, in2, in3, in4;

   integer     fd;
   initial
     begin
	fd = $fopen("performance1_a.fd2.out");
     end

   always @(posedge clock)
     begin: main
	$fdisplay(fd, "%h%h%h%h", in1, in2, in3, in4);
     end
endmodule
