// test the interaction of $display, $stime, and `timescale

`timescale 10fs/10fs

module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;

   middle iMiddle(out, in1, in2, clk1, clk2);

   always @ (posedge clk1)
     begin
        $display($stime, "%t module top %b", $stime, {in1,in2});
     end
endmodule // top

`timescale 1ps/100fs

module middle (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg  reg_out;

   assign    out[0] = reg_out;
   
   bottom ibottom(out[1], in1, in2, clk2);
   
   always @(posedge clk1) begin
      reg_out <= in2[0];
      if ( in1 )
        $display($stime, "%t module middle %b", $stime, {in1,in2});
   end
   

endmodule // middle

`timescale 1ns/1ns

module bottom (out, in1, in2, clk2);
   output out;
   input in1;
   input [1:0] in2;
   input clk2;
   reg out;

   always @(posedge clk2) begin
      out <= in2[1];
      if ( in1 )
        $display($stime, "%t module bottom: %b", $stime, {in1,in2});
   end
   

endmodule // bottom
