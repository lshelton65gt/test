module top(x,y,d,q); // carbon disallowFlattening
   input x;
   input y;
   input d;
   output q;
   sub s0(x,y,d,,q);
endmodule

module sub(x,y,d,q,o);
   input x;
   input y;
   input d;
   output q;
   output o;
   reg 	  q;
   wire   c=0;
   wire   clk = x & !y;
   reg 	  corrupt;
   initial q = 0;
   always @(clk or c) begin
      corrupt = 0;
      if (clk) corrupt = 1;
      q = 1;
      if (corrupt==0) q = 0;
      if (c) $display("DANGER!\n");
   end
   assign o = x & y;
endmodule
