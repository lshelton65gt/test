// This test tests `timescale, $realtime, and $display

`timescale 10us/1us

module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;

   middle iMiddle(out, in1, in2, clk1, clk2);

   always @ (posedge clk1)
     begin
        $display($realtime, "%t module top %b", $realtime, {in1,in2});
     end
endmodule // top

`timescale 1ps/100fs

module middle (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg  reg_out;

   assign    out[0] = reg_out;
   
   bottom ibottom(out[1], in1, in2, clk2);
   
   always @(posedge clk1) begin
      reg_out <= in2[0];
      if ( in1 )
        $display($realtime, "%t module middle %b", $realtime, {in1,in2});
   end
   

endmodule // middle

`timescale 1ns/1ns

module bottom (out, in1, in2, clk2);
   output out;
   input in1;
   input [1:0] in2;
   input clk2;
   reg out;

   always @(posedge clk2) begin
      out <= in2[1];
      if ( in1 )
        $display($realtime, "%t module bottom: %b", $realtime, {in1,in2});
   end
   

endmodule // bottom
