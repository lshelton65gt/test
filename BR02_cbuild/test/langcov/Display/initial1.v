// last mod: Fri Apr 29 15:15:50 2005
// filename: test/langcov/Display/initial1.v
// Description:  This test shows that that displays in initial blocks work
// properly.  When created this testcase would not print the correct values for
// i1 from within the initial block.
// discovered during debugging of bug3329


module initial1(clock, out1);
   input clock;
   output [7:0] out1;
   reg [7:0] out1;

   integer 	i2;
   integer 	i1;

   initial
      begin
   	 i2 = 2;
	 i1 = 3;
	 $display("i1: %d i2: %d", i1, i2);
   	 i2 = 3;
	 i1 = 4;
	 $display("i1: %d i2: %d", i1, i2);
      end

   always @(posedge clock)
     begin: main
	out1 = i1;
     end
endmodule
