// This test tests `timescale, $time, and $display,
// it includes a check for when a time value is placed in a register

`timescale 1ns/1ns

module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg [63:0] timereg;

   middle iMiddle(out, in1, in2, clk1, clk2);

   always @ (posedge clk1)
     begin
	timereg = $time;	// copy to a register, then display later with %t
        $display($time, "%t, %t module top %b", $time, timereg, {in1,in2});
     end
endmodule // top

`timescale 10ns/1ns

module middle (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg  reg_out;
   reg [63:0] timereg;

   assign    out[0] = reg_out;
   
   bottom ibottom(out[1], in1, in2, clk2);
   
   always @(posedge clk1) begin
      reg_out <= in2[0];
      if ( in1 )
	begin
	   timereg = $time;
           $display($time, "%t, %t module middle %b", $time, timereg, {in1,in2});
	end
   end
   

endmodule // middle

`timescale 100ps/100ps

module bottom (out, in1, in2, clk2);
   output out;
   input in1;
   input [1:0] in2;
   input clk2;
   reg out;
   reg [63:0] timereg;

   always @(posedge clk2) begin
      out <= in2[1];
      if ( in1 )
	begin
	   timereg = $time;
           $display($time, "%t, %t module bottom: %b", $time, timereg, {in1,in2});
	end
   end
   

endmodule // bottom
