// test the way that format specifiers work in $display system task
// in this case we test special characters


module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   reg [1:0] 	out;

   initial
     begin
	reg3 = 3;
	reg2 = 2;
	out = reg3 - reg2;
	$write("start (out==%d)\n", out);
	$write("this line should contain <tab>, <percent>, <percent>, <backslash>, <char e>, <doublequote>, <newline> \t, %%, %0%, \\, \145, \",\n");
	 
	$display("the chars on this line should line up with the previous line starting with the tab char.........       \t, %%, %0%, \\, \145, \",\n");
	$display("\FOO This line should start with the string \"FOO\" not \"\\FOO\".");
	$display("\NEW This line should start with the string \"NEW\" not \"\\NEW\".");
	$display("\new This line should start with the string \"ew\" not \"\\new\" or \"new\".");

     end
endmodule // top

