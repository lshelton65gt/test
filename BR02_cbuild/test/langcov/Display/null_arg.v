module top(clk);
  input clk;

  always @(posedge clk) begin
    $display("one: %3b, two: %3b, three: %3b",1,2,,3);
  end
endmodule

