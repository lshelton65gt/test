// test the performance of multiple file specifiers and the $fdisplay system task

module top (out, in1, in2, clk1);
   output [1:0] out;
   input [1:0] in1;
   input [1:0] in2;
   input clk1;
   reg 	 [1:0] out;
   integer     fd2;
   integer     fd4;
   integer     fd;

   integer     count;
   
   initial
     out = 3;
   
   initial
     begin
	count = 0;
	fd2 = $fopen("fileSpecifier9.fd2.out");
	fd4 = $fopen("fileSpecifier9.fd4.out");
	fd = fd2 | fd4;
        $fdisplay(fd, "in module top, opened file: fd = %d", fd);
     end
   always @ (posedge clk1)
     begin
	count = count + 1;
	$fdisplay(fd, "count: %d, in1= %b, in2= %b", count, in1, in2);
     end
   
endmodule // top

