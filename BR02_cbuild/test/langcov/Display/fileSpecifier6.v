// test the way that having multiple files open works with the $fdisplay system task
// in both of the output files we expect to see something like:
// in module top, opened file: fd4 = hex:80000003
//  where the last char is either 3 or 4
// we close the fd3, then write a second line to both files but it should only
// appear in fd4:
// wrote this after fd3 was closed: fd4 = hex:80000004



module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;
   integer     fd3;
   integer     fd4;

   initial
     out = 3;
   
//   always @ (posedge clk1)
   initial
     begin
	fd3 = $fopen("fileSpecifier6.fd3.out","w");
	fd4 = $fopen("fileSpecifier6.fd4.out","w");
// NOTE do not use 0x to display values in generated files, these appear as
// addresses to cdsDiff and thus are ignored.
        $fdisplay(fd3, "in module top, opened file: fd3 = hex:%h", fd3);
        $fdisplay(fd4, "in module top, opened file: fd4 = hex:%h", fd4);
	$fclose(fd3);
        $fdisplay(fd3, "wrote this after fd3 was closed: fd3 = hex:%h", fd3);
        $fdisplay(fd4, "wrote this after fd3 was closed: fd4 = hex:%h", fd4);
     end
endmodule // top

