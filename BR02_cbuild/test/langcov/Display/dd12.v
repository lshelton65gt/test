/* Test support for $display like system tasks */
// test of $display in a verilog function
module top (clk, val0, val1, val2, out);
   input clk;
   input [1:0] val0;
   input [1:0] val1;
   input [1:0] val2;
   output [1:0] out;


   mid i1 (clk, val1, val2, out);
   
endmodule
module mid (clk, val1, val2, out);
   input clk;
   input [1:0] val1;
   input [1:0] val2;
   output [1:0] out;
   reg [1:0] reg1;
   reg [1:0] out;

   function showit;
      input [1:0] val1;
      input [1:0] reg1;

      begin
	 $display("DSP3: clk changed in mid,  reg1= %b  val1= %b", reg1, val1); // run every posedge clk

	 showit = reg1;
      end
   endfunction
   
   initial reg1 = 0;
   
   always @(posedge clk) begin
      reg1 = val1 + reg1;
      out = showit(reg1, val1);
      
   end
endmodule
