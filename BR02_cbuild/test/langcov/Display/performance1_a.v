// last mod: original file
// filename: test/langcov/Display/performance1_a.v
// Description:  This test is used to test the performance of writing strings
// with $display, see performance1_b.v for a test that produces the same output
// but uses fewer calls to VerilogOutFileSystem


module performance1_a(clock, reset);
   input clock;
   input reset;
   integer fd; 

   initial
     begin
	fd = $fopen("performance1_c.fd2.out");
     end

   always @(posedge clock)
     begin: main
	$fdisplay(fd, "Work", " consists", " of", " whatever", " a", " body", " is", " obliged", " to", " do.", " Play", " consists", " of", " whatever", " a", " body", " is", " not", " obliged", " to", " do.", " M.", " Twain");
     end
endmodule
