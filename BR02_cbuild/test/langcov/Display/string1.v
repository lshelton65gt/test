// testing of string handling
module string_test(out);
   output out;
   reg 	  out;
   reg [8*1:1] stringvar1;	// large enough to hold 1 chars UInt8
   reg [8*2:1] stringvar2;	// large enough to hold 2 chars UInt16
   reg [8*3:1] stringvar3;	// large enough to hold 3 chars UInt32
   reg [8*4:1] stringvar4;	// large enough to hold 4 chars UInt32
   reg [8*5:1] stringvar5;	// large enough to hold 5 chars UInt64
   reg [8*8:1] stringvar8;	// large enough to hold 8 chars UInt64
   reg [8*9:1] stringvar9;	// large enough to hold 9 chars BV
   reg [8*14:1] stringvar14;	// large enough to hold 14 chars BV
   reg [8*16:1] stringvar16;	// large enough to hold 16 chars BV
   reg [8*17:1] stringvar17;	// large enough to hold 17 chars BV
   reg [8*120:1] stringvar120;	// large enough to hold 120 chars BV
   initial
     begin
	out =1;
	stringvar1 = "";   // 0 chars
	// without format stringvar1 is displayed as decimal
// NOTE do not use 0x to display values in generated files, these appear as
// addresses to cdsDiff and thus are ignored.
	$display("%s is stored as hex:%h", stringvar1,stringvar1," displayed w/o format as: ",stringvar1);
	stringvar1 = {stringvar1,"-"};   // 1 chars
	$display("%s is stored as hex:%h", stringvar1,stringvar1," displayed w/o format as: ",stringvar1);
	stringvar1 = {stringvar1,"!"}; // now has 2 chars, LHS has only 1
	$display("%s is stored as hex:%h", stringvar1,stringvar1," displayed w/o format as: ",stringvar1);
	$display();

	stringvar2 = "a";   // 1 chars
	// without format stringvar2 is displayed as decimal
	$display("%s is stored as hex:%h", stringvar2,stringvar2," displayed w/o format as: ",stringvar2);
	stringvar2 = {stringvar2,"-"};   // 2 chars
	$display("%s is stored as hex:%h", stringvar2,stringvar2," displayed w/o format as: ",stringvar2);
	stringvar2 = {stringvar2,"!"}; // now has 3 chars, LHS has only 2
	$display("%s is stored as hex:%h", stringvar2,stringvar2," displayed w/o format as: ",stringvar2);
	$display();

	stringvar3 = "AB";   // 2 chars
	$display("%s is stored as hex:%h", stringvar3,stringvar3," displayed w/o format as: ",stringvar3);
	stringvar3 = {stringvar3,"-"};   // 3 chars
	$display("%s is stored as hex:%h", stringvar3,stringvar3," displayed w/o format as: ",stringvar3);
	stringvar3 = {stringvar3,"!"}; // now has 4 chars, LHS has only 3
	$display("%s is stored as hex:%h", stringvar3,stringvar3," displayed w/o format as: ",stringvar3);
	$display();

	stringvar4 = "ab";   // 2 chars
	// without format stringvar4 is displayed as decimal
	$display("%s is stored as hex:%h", stringvar4,stringvar4," displayed w/o format as: ",stringvar4);
	stringvar4 = {stringvar4,"c"};   // 3 chars
	$display("%s is stored as hex:%h", stringvar4,stringvar4," displayed w/o format as: ",stringvar4);
	stringvar4 = {stringvar4,"!"}; // now has 4 chars
	$display("%s is stored as hex:%h", stringvar4,stringvar4," displayed w/o format as: ",stringvar4);
	stringvar4 = {stringvar4,"+"}; // RHS is now 5 chars, LHS only 4
	$display("%s is stored as hex:%h", stringvar4,stringvar4," displayed w/o format as: ",stringvar4);
	$display();

	stringvar5 = "123";   // 3 chars
	// without format stringvar5 is displayed as decimal
	$display("%s is stored as hex:%h", stringvar5,stringvar5," displayed w/o format as: ",stringvar5);
	stringvar5 = {stringvar5,"4"};   // 4 chars
	$display("%s is stored as hex:%h", stringvar5,stringvar5," displayed w/o format as: ",stringvar5);
	stringvar5 = {stringvar5,"!"}; // now has 5 chars
	$display("%s is stored as hex:%h", stringvar5,stringvar5," displayed w/o format as: ",stringvar5);
	stringvar5 = {stringvar5,"+"}; // RHS is now 6 chars, LHS only 5
	$display("%s is stored as hex:%h", stringvar5,stringvar5," displayed w/o format as: ",stringvar5);
	$display();

	stringvar8 = "123456";   // 6 chars
	// without format stringvar8 is displayed as decimal
	$display("%s is stored as hex:%h", stringvar8,stringvar8," displayed w/o format as: ",stringvar8);
	stringvar8 = {stringvar8,"7"};   // 7 chars
	$display("%s is stored as hex:%h", stringvar8,stringvar8," displayed w/o format as: ",stringvar8);
	stringvar8 = {stringvar8,"!"}; // now has 8 chars
	$display("%s is stored as hex:%h", stringvar8,stringvar8," displayed w/o format as: ",stringvar8);
	stringvar8 = {stringvar8,"+"}; // RHS is now 9 chars, LHS only 8
	$display("%s is stored as hex:%h", stringvar8,stringvar8," displayed w/o format as: ",stringvar8);
	$display();

	stringvar9 = "1234567";   // 7 chars
	// without format stringvar9 is displayed as decimal
	$display("%s is stored as hex:%h", stringvar9,stringvar9," displayed w/o format as: ",stringvar9);
	stringvar9 = {stringvar9,"8"};   // 8 chars
	$display("%s is stored as hex:%h", stringvar9,stringvar9," displayed w/o format as: ",stringvar9);
	stringvar9 = {stringvar9,"!"}; // now has 9 chars
	$display("%s is stored as hex:%h", stringvar9,stringvar9," displayed w/o format as: ",stringvar9);
	stringvar9 = {stringvar9,"+"}; // RHS is now 10 chars, LHS only 9
	$display("%s is stored as hex:%h", stringvar9,stringvar9," displayed w/o format as: ",stringvar9);
	$display();
	

	stringvar14 = "Hello worl";   // 10 chars
	// without format stringvar14 is displayed as decimal
	$display("%s is stored as hex:%h", stringvar14,stringvar14," displayed w/o format as: ",stringvar14);
	stringvar14 = {stringvar14,"d"};   // 11 chars
	$display("%s is stored as hex:%h", stringvar14,stringvar14," displayed w/o format as: ",stringvar14);
	stringvar14 = {stringvar14,"!!!"}; // now has 14 chars
	$display("%s is stored as hex:%h", stringvar14,stringvar14," displayed w/o format as: ",stringvar14);
	stringvar14 = {stringvar14,"+"}; // RHS is now 15 chars, LHS only 14
	$display("%s is stored as hex:%h", stringvar14,stringvar14," displayed w/o format as: ",stringvar14);
	$display();


	stringvar16 = "odd test string";   // 15 chars
	// without format stringvar16 is displayed as decimal
	$display("%s is stored as hex:%h", stringvar16,stringvar16," displayed w/o format as: ",stringvar16);
	stringvar16 = {stringvar16,"!"};   // 16 chars
	$display("%s is stored as hex:%h", stringvar16,stringvar16," displayed w/o format as: ",stringvar16);
	stringvar16 = {stringvar16,"+"}; // RHS is now 17 chars, LHS only 15
	$display("%s is stored as hex:%h", stringvar16,stringvar16," displayed w/o format as: ",stringvar16);
	$display();


	stringvar17 = "even test string";   // 16 chars
	// without format stringvar17 is displayed as decimal
	$display("%s is stored as hex:%h", stringvar17,stringvar17," displayed w/o format as: ",stringvar17);
	stringvar17 = {stringvar17,"!"};   // 17 chars
	$display("%s is stored as hex:%h", stringvar17,stringvar17," displayed w/o format as: ",stringvar17);
	stringvar17 = {stringvar17,"+"}; // RHS is now 18 chars, LHS only 17
	$display("%s is stored as hex:%h", stringvar17,stringvar17," displayed w/o format as: ",stringvar17);
	$display();

	stringvar120 = "                                                                                                                       ";   // 119 chars
        stringvar120 = "Work consists of whatever a body is obliged to do.              Play consists of whatever a body is not obliged to do. "; 
	// without format stringvar120 is displayed as decimal
	$display("%s is stored as hex:%h", stringvar120,stringvar120," displayed w/o format as: ",stringvar120);
	stringvar120 = {stringvar120,"M"};   // 120 chars
	$display("%s is stored as hex:%h", stringvar120,stringvar120," displayed w/o format as: ",stringvar120);
	stringvar120 = {stringvar120," Twain"}; // RHS is now 126 chars, LHS only 120
	$display("%s is stored as hex:%h", stringvar120,stringvar120," displayed w/o format as: ",stringvar120);
	$display();
	
     end
endmodule
