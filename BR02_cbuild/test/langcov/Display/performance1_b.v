// last mod: Tue Sep 28 17:23:44 2004
// filename: test/langcov/Display/performance1_b.v
// Description:  This test is used to test the performance of writing strings
// with $fdisplay
// see also:
//  performance1_a.v same output but uses many more calls to VerilogOutFileSystem
//  performance1_b.v same output but uses single call to VerilogOutFileSystem
//  performance1_c.v same output to multiple files with single call to VerilogOutFileSystem



module performance1_b(clock, reset);
   input clock;
   input reset;
   integer fd; 

   initial
     begin
	fd = $fopen("performance1_c.fd2.out");
     end

   always @(posedge clock)
     begin: main
	$display("Work consists of whatever a body is obliged to do. Play consists of whatever a body is not obliged to do. M. Twain");
     end
endmodule
