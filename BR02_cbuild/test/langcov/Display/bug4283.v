// this test demonstrated that codegen for $display needed to mask off the dirty
// bits outside of the range within the partselect being printed.
// inspired by bug 4283
module test(c,w);
   input c;
   input [31:0] w;
   always @(posedge c) begin: foo
      reg [8:0] tmp;
      tmp = w[0+:9];
      // in the following line we expect to see pairs of identical values displayed
      $display("%0d %0d", w[0+:9], tmp, "   ", "%0b %0b", w[0+:9], tmp );
   end
endmodule
