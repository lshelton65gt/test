/* Test support for display of bitvectors (and parts of bitvectors) with the $display system task */
module top (clk, out, addressLong);
   input clk;
   input [100:0] addressLong;
   output [3:0] out;
   reg [3:0] 	out;
   reg [3:0] mem1 [7:0];
   reg [2:0] address;
   initial
      begin
	 mem1[0] = 0; mem1[1] = 1; mem1[2] = 2; mem1[3] = 3;
	 mem1[4] = 4; mem1[5] = 5; mem1[6] = 6; mem1[7] = 7;
      end


   always @(posedge clk) begin
      $display("addressLong[100:0]: %b\n", addressLong, "addressLong[68:1]:                                  %b\n", addressLong[68:1]);
      $display("addressLong[100:0]: %b\n", addressLong, "addressLong[75:1]:                           %b\n", addressLong[75:1]);
      $display("addressLong[100:0]: %b\n", addressLong, "addressLong[75:8]:                           %b\n", addressLong[75:8]);
      $display("addressLong[100:0]: %b\n", addressLong, "addressLong[99:34]:  %b\n", addressLong[99:34]);

      $display("addressLong[100:0]: %o\n", addressLong, "addressLong[70:3]:            %o\n", addressLong[70:3]);
      $display("addressLong[100:0]: %o\n", addressLong, "addressLong[77:3]:          %o\n", addressLong[77:3]);
      $display("addressLong[100:0]: %o\n", addressLong, "addressLong[77:9]:          %o\n", addressLong[77:9]);
      $display("addressLong[100:0]: %o\n", addressLong, "addressLong[99:33]: %o\n", addressLong[99:33]);

      $display("addressLong[100:0]: %b\n", addressLong, "addressLong[99:32]:  %b\n", addressLong[99:32]);
      $display("addressLong[100:0]: %b\n", addressLong, "addressLong[99:33]:  %b\n", addressLong[99:33]);
      
      address = addressLong[2:0];
      out = mem1[address];      // keep it alive
   end
   
endmodule
