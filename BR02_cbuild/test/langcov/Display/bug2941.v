// last mod: Mon Oct 25 14:52:06 2004
// filename: test/langcov/Display/bug2941.v
// Description:  This test duplicates the problem of bug2941, caused by the
// writing of a BVRef value.
// I found that the task does not even need to be called to cause the compile
// problem, but without it it is difficult to test that the proper code is
// generated. so for now it is used in the initial block.


module bug2941(clock, out);
   input clock;
   output out;
   reg 	  out;
   reg [255:0] string;
   
   task unusedfct;
      input	[255:0]	data;

      begin
	 // print whole line (32 chars)
	 $write("%0s\n", data);
	 // the following line prints a bitsel as a string (all but last char)
	 $write("%0s\n", data[255:8]);
      end
   endtask


   
   initial
     begin
	out = 0;
	string = "abcdefghijklmnopqrstuvwxyz012345"; // 32 chars long
	unusedfct(string);	// comment this line out if you like
     end
endmodule
