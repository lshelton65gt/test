// this test checks that the proper code is emitted for a display of a small
// partselect of a vector that is longer than 64 bits.  At one time it was not
// properly parenthesized.

module bug4436(val,out);
   input [71:0] val;
   output       out;
   reg          t;
   assign       out = t;
   always @ (val)
     begin
        $display (val[3:0]);
        t = val[0];
     end
   
        
   
endmodule
