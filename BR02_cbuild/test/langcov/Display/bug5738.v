// last mod: Tue Mar  7 14:35:40 2006
// filename: test/langcov/Display/bug5738.v
// Description:  This test demonstrates the problem defined in bug5738, where %c
// did not work for non integer values


module bug5738 (out, clk1);
   output  out;
   input 	clk1;

   reg [1:0] 	out;
   reg [31:0] 	reg32;
   reg [31:0] 	reg32_b;
   reg [7:0]    reg8;
   reg [63:0] 	reg64;
   reg [63:0] 	reg64_b;
   reg [65:0] 	reg66;
   reg [65:0] 	reg66_b;
   integer 	int1;

   initial out = 0;
   
   always @ (posedge clk1)
     begin
	reg8 =  100;
	reg32 = 101;
	reg32_b = 32'hAA65;     // this has 101 in the low 8 bits
	reg64 = 102;
	reg64_b = 64'hAA66;	// this has 102 in the low 8 bits
	reg66 = 103;
	reg66_b = 66'hAA67;	// this is has 103 in the low 8 bits
	int1  = 104;
	$display("reg8 (code %0d) as a %%c is displayed as: %c should be: d", reg8, reg8);
	$display("reg32 (code %0d) as a %%c is displayed as: %c should be: e", reg32, reg32);
	$display("reg32_b (code %0d) as a %%c is displayed as: %c should be: e", reg32_b, reg32_b);
	$display("reg64 (code %0d) as a %%c is displayed as: %c should be: f", reg64, reg64);
	$display("reg64_b (code %0d) as a %%c is displayed as: %c should be: f", reg64_b, reg64_b);
	$display("reg66 (code %0d) as a %%c is displayed as: %c should be: g", reg66, reg66);
	$display("reg66_b (code %0d) as a %%c is displayed as: %c should be: g", reg66_b, reg66_b);
	$display("int1 (code %0d) as a %%c is displayed as: %c should be: h", int1, int1);
     end

endmodule

