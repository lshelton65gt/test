// test how $time and fold interact (extracted from indus/memif_ctrl_central.v

module top (out,clk, do_late_act);
   output [9:0] out;
   input clk;
   input [3:0] do_late_act;
   reg 	 [9:0] out;

    always @ (posedge clk)
        if(|do_late_act)
		casex(do_late_act)
                4'b1xxx: out = $time;
                4'b01xx: out = $time;
                4'b001x: out = $time;
                4'b0001: out = $time;
		endcase
   
endmodule // top


