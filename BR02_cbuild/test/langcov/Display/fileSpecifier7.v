// test the way that file names and type specification  work
// in this case the file names and type strings are stored in registers
// in the output files we expect to see something like:
// in module top, opened file: hex:80000003


module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;
   integer     fd2;
   integer     fd3;
   integer     fd4;
   reg [22*8:1] fd2Name;
   reg [8:1] 	fd3Type;

//   always @ (posedge clk1)
   initial
     begin
	out = 3;
	fd2Name = "fileSpecifier7.fd2.out";
	fd2 = $fopen(fd2Name);
	fd3Type = "w";
	fd3 = $fopen("fileSpecifier7.fd3.out", fd3Type);
	fd4 = $fopen({"fileSpecifier7.fd",(3'b100+"0"),".out"}, "w");
// NOTE do not use 0x to display values in generated files, these appear as
// addresses to cdsDiff and thus are ignored.
        $fdisplay(fd2, "in module top, opened file: hex:%h", fd2);
        $fdisplay(fd3, "in module top, opened file: hex:%h", fd3);
        $fdisplay(fd4, "in module top, opened file: hex:%h", fd4);
	$fclose(fd2);
	$fclose(fd3);
	$fclose(fd4);
     end
endmodule // top

