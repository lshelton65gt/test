// test the way that multiple file specifiers work with the $fdisplay system task
// in this test we open 31 files
// in all of the output files we expect to see something like:
// in module top, opened file:           6
// we close the fd2, then write a second line to both files but it should only
// appear in fd4:
// wrote this after file 2 was closed: fd =           6



module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;
   integer     fd1;
   integer     fd2;
   integer     fd3;
   integer     fd4;
   integer     fd5;
   integer     fd6;
   integer     fd7;
   integer     fd8;
   integer     fd9;
   integer     fd10;
   integer     fd11;
   integer     fd12;
   integer     fd13;
   integer     fd14;
   integer     fd15;
   integer     fd16;
   integer     fd17;
   integer     fd18;
   integer     fd19;
   integer     fd20;
   integer     fd21;
   integer     fd22;
   integer     fd23;
   integer     fd24;
   integer     fd25;
   integer     fd26;
   integer     fd27;
   integer     fd28;
   integer     fd29;
   integer     fd30;
   integer     fd31;
   integer     j;
   integer     fd;
   integer     singlefd;
   
   initial
     out = 3;
   
//   always @ (posedge clk1)
   initial
     begin
	fd = 0;
	fd = fd | $fopen("fileSpecifier3.fd1.out");
        $fdisplay(fd, "in module top,  1 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd2.out");
        $fdisplay(fd, "in module top,  2 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd3.out");
        $fdisplay(fd, "in module top,  3 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd4.out");
        $fdisplay(fd, "in module top,  4 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd5.out");
        $fdisplay(fd, "in module top,  5 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd6.out");
        $fdisplay(fd, "in module top,  6 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd7.out");
        $fdisplay(fd, "in module top,  7 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd8.out");
        $fdisplay(fd, "in module top,  8 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd9.out");
        $fdisplay(fd, "in module top,  9 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd10.out");
        $fdisplay(fd, "in module top, 10 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd11.out");
        $fdisplay(fd, "in module top, 11 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd12.out");
        $fdisplay(fd, "in module top, 12 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd13.out");
        $fdisplay(fd, "in module top, 13 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd14.out");
        $fdisplay(fd, "in module top, 14 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd15.out");
        $fdisplay(fd, "in module top, 15 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd16.out");
        $fdisplay(fd, "in module top, 16 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd17.out");
        $fdisplay(fd, "in module top, 17 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd18.out");
        $fdisplay(fd, "in module top, 18 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd19.out");
        $fdisplay(fd, "in module top, 19 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd20.out");
        $fdisplay(fd, "in module top, 20 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd21.out");
        $fdisplay(fd, "in module top, 21 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd22.out");
        $fdisplay(fd, "in module top, 22 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd23.out");
        $fdisplay(fd, "in module top, 23 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd24.out");
        $fdisplay(fd, "in module top, 24 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd25.out");
        $fdisplay(fd, "in module top, 25 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd26.out");
        $fdisplay(fd, "in module top, 26 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd27.out");
        $fdisplay(fd, "in module top, 27 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd28.out");
        $fdisplay(fd, "in module top, 28 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd29.out");
        $fdisplay(fd, "in module top, 29 files open: fd = 32'%b", fd);
	fd = fd | $fopen("fileSpecifier3.fd30.out");
        $fdisplay(fd, "in module top, 30 files open: fd = 32'%b", fd);

	for (j = 1; j < 31; j = j + 1)
	   begin
	      singlefd = (1 << j);
	      fd = fd & (~ singlefd);
	      $fclose(singlefd);
              $fdisplay(fd, "in module top, closed file %2d: fd = 32'%b", j, fd);
	   end
        $fdisplay(fd, "you should never see this because it was written after all files were closed: fd = 32'%b", fd);	
     end
endmodule // top

