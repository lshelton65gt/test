// last mod: Wed Jul 13 16:42:08 2005
// filename: test/langcov/Display/formatSpecifier10d.v
// Description:  This test is used to check that we enable output systasks with
// just the enableOutputSysTasks directive (when the user does not specify -enableOutputSysTasks)
// enableOutputSysTasks directive for one or more modules.  We expect warnings
// only for $display in the 'mid' module
// This test just uses formatSpecifier10.v


`include "formatSpecifier10.v"
