// test the way that format specifiers work in $display system task
// in this case we test the proper handling of format specifiers that are
// incompatible with the arguments provided.
// we expect something like "# in module top,  %0d  45  611"

module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   reg [1:0] 	out;

   initial
     begin
	 reg3 = 3;
	 reg2 = 2;
	 out = reg3 - reg2;
	 $display("in module top,  %s %s %0d %s %0d", "%0d", reg3, "-", reg2, "=", out);
     end
endmodule // top

