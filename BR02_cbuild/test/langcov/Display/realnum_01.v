// last mod: Tue Nov 28 07:12:55 2006
// filename: test/langcov/Display/realnum_01.v
// Description:  This test checks for proper display of real numbers, as well as
// proper constant propagation


module realnum_01(clock, out1, out2);
   input clock;
   output  out1, out2;
   reg  out1, out2;
   real      r1, r2;


   initial
      begin
	 r1 = 239299329230617529590083.0;
	 r2 = 0.239299329230617529590083;
	 out1 = r1;
	 out2 = r2;
      end 
   always @(posedge clock)
     begin: main
	// note that in the following display the two values printed are not
	// equivalent, this shows rounding errors as values are displayed in %f format.
	$display("239299329230617529590083.0 == %f => %b",
		 239299329230617529590083.0,
		 (r1 == 239299329230617529590083.0));
	$display("0.239299329230617529590083 == %f => %b",
		 0.239299329230617529590083,
		 (r2 == 0.239299329230617529590083));
	r1 = r1 + 10;
	r2 = r2 + 10;

     end
endmodule
