// testing the sizing of expressions used in $display system tasks
// this example is similar to an example in the verilog lrm
// the single expression in the display statement should be resized to be 5 bits because d5 is that long
// the output should be:
// if sizing is correct you should see 01000 = 01000
module top(clk, in1, out);
   input clk, in1;
   output out;			// dummy output to avoid warning about no ports
   reg [3:0] a4,b4,c4;
   reg [4:0] d5;
   integer   i;
   initial begin
      a4 = 9;
      b4 = 8;
      c4 = 1;
      d5 = 17;
      i = 0;
   end
   assign out = a4;
   always @(posedge clk)
      begin
	 $display("if sizing is correct you should see a 5 bit value here(never 4 bits): %b", in1 ? (a4&b4) : d5);
	 $display("if sizing is correct you should see 00000000000000000000000000001000 = %b", (in1 | c4) ? (a4&b4) : (d5+i));
      end
endmodule
