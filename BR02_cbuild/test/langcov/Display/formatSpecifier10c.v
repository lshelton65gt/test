// last mod: Wed Jul 13 16:34:55 2005
// filename: test/langcov/Display/formatSpecifier10c.v
// Description:  This test is used to check that we produce the proper warning
// messages if the user specifies -enableOutputSysTasks and uses a
// enableOutputSysTasks directive for one or more modules.  We expect warnings.
// This test just uses formatSpecifier10.v with the directives from formatSpecifier10b


`include "formatSpecifier10.v"
