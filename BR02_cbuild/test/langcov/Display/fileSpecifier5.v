// test to make sure that the $fopen only handles 2 args at max.
// this testcase is expected to fail with error message about wrong number of arguments



module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;
   integer     fd2;
   integer     fd3;

   initial
     out = 3;
   
//   always @ (posedge clk1)
   initial
     begin
	fd2 = $fopen("fileSpecifier5.fd2.out", "w", "r");
	fd3 = $fopen("fileSpecifier5.fd3.out", "w+ra");	// this should fail because mode is too long
        $fdisplay(fd2, "in module top, opened file: %h", fd2);
	$fclose(fd2);
     end
endmodule // top

