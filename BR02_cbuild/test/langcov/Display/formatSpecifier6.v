// test the way that format specifiers work in $display system task
// in this case we test empty format specifiers
// $display() should print a blank line
// $write() should print nothing

module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   reg [1:0] 	out;

   initial
      begin
	 reg3 = 3;
	 reg2 = 2;
	 out = reg3 - reg2;
      end
   
   always @ (posedge clk1)
//   initial
     begin
	$write("start (out==%d)\n", out);
	$display();
	$write("middle, this line should be preceeded by 1 blank line (out==%d)\n", out);
	$write();
	$write("end, this line should be preceeded by 0 blank lines (out==%d)\n", out);
     end
endmodule // top

