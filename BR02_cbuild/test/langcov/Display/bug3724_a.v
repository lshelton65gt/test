// last mod: Wed Jan 26 09:52:43 2005
// filename: test/langcov/Display/bug3724_a.v
// Description:  This test is an extension of the one filed with bug 3724,
// The problem is that the file descriptor (first arg of $fwrite) was longer
// than 32 bits.  With v1.2740 cbuild would report a backend compile error.
// This testcase has been changed slightly so that it is simulatable, addition
// of the $fopen, and a demonstration that bits beyond 32 are ignored in the
// file descriptor argument of $fwrite.


module test(clk,in0,in1,in2,in3,in4,out);
   input clk,in0;
   input [1:0] in1;
   input [10:0] in2;
   input [15:0] in3;
   input [7:0] 	in4;
   output 	out;
   reg 		out;

   reg [64*8:1] fd2;		// much wider than needed, note 1 based
   reg [8:1] fd4;		// narrower than 32 bits

   initial
     begin
	fd2 = $fopen("bug3724_a.fd2.out");
	fd4 = $fopen("bug3724_a.fd4.out");
     end

   task dump;
      begin
	 $fwrite(fd2,"for fd2: %h, %h	// field= %d, line=%d, pixel=%d\n",fd2, in4,in1,in2,in3);
	 $fwrite(fd4,"for fd4: %h, %h	// field= %d, line=%d, pixel=%d\n",fd4, in4,in1,in2,in3);
      end
   endtask

   always @(posedge clk)
     begin
	dump;
	fd2[33] = 1'b1;		// modify the 33rd bit
	fd4[7] = 1'b1;		// modify bit 7
	dump;
	fd2[33] = 1'b0;		// restore the 33rd bit
	fd4[7] = 1'b0;		// restore bit 7
	out = in0;
     end
endmodule
