// test the way that format specifiers work in $display system task
// in this case we test number presentation


module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   reg [1:0] 	out;
   integer      ichar;
   reg 		signed [7:0] signed_sized;

   initial
      begin
	 reg3 = 3;
	 reg2 = 2;
	 out = reg3 - reg2;
	 ichar = 100;
	 signed_sized = -7;
      end
   
//   always @ (posedge clk1)
   initial
     begin
	$display("d15 (naturalsize) is        \tb%b, o%o, d%d, h%h", 15, 15, 15, 15);
	$display("d15 (compressed)  is             \tb%0b, o%0o, d%0d, h%0h", 15, 15, 15, 15);
	$display("d15 (compressed, leftaligned) is \tb%-0b, o%-0o, d%-0d, h%-0h", 15, 15, 15, 15);
	$display("hffffffff is:\t\tb%0b, o%0o, d%0d, h%0h u%0u", 32'hffffffff, 32'hffffffff, 32'hffffffff, 32'hffffffff, 32'hffffffff);
	$display("hfffffffff is:\tb%0b, o%0o, d%0d, h%0h", 36'hfffffffff, 36'hfffffffff, 36'hfffffffff, 36'hfffffffff);
	$display("bzz is:\tb%0b, o%0o, d%0d, h%0h", 2'bzz, 2'bzz, 2'bzz, 2'bzz);
	$display("bxxxxxxxx is:\tb%0b, u%0u z%0z", 2'bxxxxxxxx, 2'bxxxxxxxx, 2'bxxxxxxxx);
	$display("bzzzzzzzz is:\tb%0b, u%0u z%0z", 2'bzzzzzzzz, 2'bzzzzzzzz, 2'bzzzzzzzz);
	$display("bxz is:\tb%0b, u%0u z%0z", 2'bxz, 2'bxz, 2'bxz);
	$display("7.5 (0)is:\t\te%0e, f%0f, g%0g", 7.5, 7.5, 7.5);
	$display("7.5 (6.3 right) is:\te%6.3e, f%6.3f, g%6.3g", 7.5, 7.5, 7.5);
	$display("7.5 (6.3 left ) is:\te%-6.3e, f%-6.3f, g%-6.3g", 7.5, 7.5, 7.5);
	$display("-7 is:\tb%0b, o%o d%d h%h", -7, -7, -7, -7);
	$display("signed_sized = -7 is:\tb%0b, o%o d%d h%h",signed_sized,signed_sized,signed_sized,signed_sized);
	$display("The module is: %m");
	$display("The char code %0d as a %%c is displayed as: %c", ichar, ichar);
	// should also test %l %v %t here
     end

   mid i1 (clk1);
endmodule // top

module mid (clk1);
   input clk1;

   initial
     begin
	$display("The inner module is: %m, an undriven clk1 is: %b", clk1);
     end
endmodule

