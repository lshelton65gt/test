// test the way that format specifiers work in $display system task
// we expect to see something like:
// in module top,  3 - 2 = 1
// testing empty arg: there should be a space here -> <-

module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   reg [1:0] 	out;

//   initial
//      begin
//	 reg3 = 3;
//	 reg2 = 2;
//	 out = reg3 - reg2;
//      end
   
//   always @ (posedge clk1)
   initial
     begin
	reg3 = 3;
	reg2 = 2;
	out = reg3 - reg2;

	$display("in module top,  %0d %s %0d %s %0d", reg3, "-", reg2, "=", out);
	$display("testing empty arg: there should be a space here ->",,"<-");
     end
endmodule // top

