/* error message testing*/
module top (clk, out);
   input clk;
   output out;
   reg 	  out;
   initial
      begin
	 out = 1;
	 // The next line should report an error (during cbuild) about a missing argument
	 $display("test1 %s %s\n", "foo");
	 // The next line should report an error (during cbuild) about a truncated size specification
	 $display("test2 %0", "foo");
	 // So should this one...
	 $display("test3 %0.", "foo");
	 // This one reports that ' ' is an invalid format character
 	 $display("test4 %1 ", "foo");
      end
   always @(posedge clk)
     out = ~out;
endmodule
