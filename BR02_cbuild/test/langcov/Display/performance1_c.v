// last mod: original file
// filename: test/langcov/Display/performance1_c.v
// Description:  This test is used to test the performance of writing strings
// with $fdisplay,
// see also:
//  performance1_a.v same output but uses many more calls to VerilogOutFileSystem
//  performance1_b.v same output but uses single call to VerilogOutFileSystem
//  performance1_c.v same output to multiple files with single call to VerilogOutFileSystem


module performance1_c(clock, reset);
   input clock;
   input reset;
   integer fd; 

   initial
     begin
	fd = $fopen("performance1_c.fd2.out");
	fd = fd | $fopen("performance1_c.fd4.out");
	fd = fd | $fopen("performance1_c.fd8.out");
	fd = fd | $fopen("performance1_c.fd16.out");
	fd = fd | $fopen("performance1_c.fd32.out");
	fd = fd | $fopen("performance1_c.fd64.out");
	fd = fd | $fopen("performance1_c.fd128.out");
	fd = fd | $fopen("performance1_c.fd256.out");
	fd = fd | $fopen("performance1_c.fd512.out");
     end
   always @(posedge clock)
     begin: main
	$fdisplay(fd, "Work consists of whatever a body is obliged to do. Play consists of whatever a body is not obliged to do. M. Twain");
     end
endmodule
