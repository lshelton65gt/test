// Variation on formatSpecifer17.v, where the top level name is also
// an escaped identifier.  This tests that we are properly slashifying
// the branch instance names in the %m processing, before emitting them
// as a C literal.

// Note that we don't take the sim gold file from Aldec (though I
// looked at the diffs manually).  This is because Aldec sees the
// top level testbench as the top level, so it will generate different
// results for %m.

module top (clk1, clk2, reset1, reset2, out1, out2, out3, out4);
   input clk1, clk2, reset1, reset2;
   output [31:0] out1, out2, out3, out4;

   twocounter \cTW1[0]_req (clk1, clk2, reset1, reset2, out1, out2);
   twocounter \cTW2[1]_req (clk1, clk2, reset1, reset2, out3, out4);

   always @(posedge clk1)
     begin
        if (out1 == 1'b0)
          $display("(%m) out1 == 0", out1);
        if (out3 == 1'b0)
          $display("(%m) out1 == 0", out1);
     end

endmodule


// This demonstrates compiling and running a simple design with
// two counters driven by two clocks

module twocounter(clk1, clk2, reset1, reset2, out1, out2);
  input clk1, clk2, reset1, reset2;
  output [31:0] out1, out2;
//reg [31:0] out1, out2;

/**
  always @(posedge clk1)
    if (reset1)
      out1 <= 32'b0;
    else
      out1 <= out1 + 32'd1;

  always @(posedge clk2)
    if (reset2)
      out2 <= 32'b0;
    else
      out2 <= out2 + 32'd3;
**/

  onecounter #(1) cnt1 (clk1, reset1, out1);
  onecounter #(3) cnt2 (clk2, reset2, out2);

endmodule

module onecounter(clk, reset, out);
  input clk, reset;
  output [31:0] out;
  reg [31:0] out;

  parameter INC= 32'd5;

  initial out = 0;

  always @(posedge clk)
    if (reset)
      out <= 32'b0;
    else begin
       out <= out + INC;
       $display("(%m) RTL Checker Fired, %d", out); 
    end
endmodule

