// testcase dd7.v
// testing of the display of strings that were stored in registers

module top (clk1, out);
   output [31:0] out;
   input 	 clk1;
   reg 	 [31:0] out;
   reg   [31:0] reg32;
   reg   [63:0] reg64;
   reg   [79:0] reg80;

   
   initial
     begin
	out = "abcd";
	reg32 = "abcd";
	reg64 = "abcdefgh";
	reg80 = "abcdefghij";
	$display("%s", "even test string.");
	$display("%s", "odd test string.");

	$display("abcd",       " should be: dec: 1633837924");
        $display("%s",reg32,   " actually:  dec: ", reg32);
	$display("abcd",       " should be: hex: 61626364");
        $display("%s",reg32,   " actually:  hex: %h", reg32);
	$display("abcd",       " should be: oct: 14130461544");
        $display("%s",reg32,   " actually:  oct: %o", reg32);
	$display("abcd",       " should be: bin: 01100001011000100110001101100100");
        $display("%s",reg32,   " actually:  bin: %b\n", reg32);
	$display("");
	$display("abcdefgh",   " should be: dec:  7017280452245743464");
        $display("%s",reg64,   " actually:  dec: ", reg64);
	$display("abcdefgh",   " should be: hex: 6162636465666768");
        $display("%s",reg64,   " actually:  hex: %h", reg64);
	$display("abcdefgh",   " should be: oct: 0605423066214531463550");
        $display("%s",reg64,   " actually:  oct: %o", reg64);
	$display("abcdefgh",   " should be: bin: 0110000101100010011000110110010001100101011001100110011101101000");
        $display("%s",reg64,   " actually:  bin: %b", reg64);
	$display("");
	$display("abcdefghij", " should be: dec: 459884491718377043683690");
        $display("%s",reg80,   " actually:  dec:", reg80);
	$display("abcdefghij", " should be: hex: 6162636465666768696a");
        $display("%s",reg80,   " actually:  hex: %h", reg80);
	$display("abcdefghij", " should be: oct: 1413046154431263147320645521");
        $display("%s",reg80,   " actually:  oct: %o1", reg80);
	$display("abcdefghij", " should be: bin: 01100001011000100110001101100100011001010110011001100111011010000110100101101010");
        $display("%s",reg80,   " actually:  bin: %b", reg80);
     end
endmodule // top

