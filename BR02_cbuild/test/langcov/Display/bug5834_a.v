// last mod: Tue Mar 21 07:40:40 2006
// filename: test/langcov/Display/bug5834_a.v
// Description:  This test stresses the issue that appeared in 5834, that of
// using escaped strings in regs and printing them with $fwrite



module bug5834_a (clk1, out);
   input clk1;
   output out;
   reg 	  out;

   integer     fd2;
   integer     fd4, fd8, fd16, fd32, fd64;

   reg [15:0]   char_2;		// can store 2 char
   reg [15:0]   char_2a;		// can store 2 char
   reg [31:0]   char_4;
   reg [63:0]   char_8;
   reg [127:0]   char_16;
   reg [255:0] 	 char_32;
   reg [511:0] 	 char_64;

   initial
     out = 3;
   
//   always @ (posedge clk1)
   initial
     begin
	fd2 = $fopen("bug5834_a.fd2.out");
	fd4 = $fopen("bug5834_a.fd4.out");
	fd8 = $fopen("bug5834_a.fd8.out");
	fd16 = $fopen("bug5834_a.fd16.out");
	fd32 = $fopen("bug5834_a.fd32.out");
	fd64 = $fopen("bug5834_a.fd64.out");
	char_2 = "\t\n";		// this will store the \n
	char_2a = "\1\2";   	// here there is a \d at end of string
	char_4 = "\0077\8\9";	// first char is ^G second is 7 third is simply and 8 and last is 9 (ie the \8 is just the char 8) (aldec gets this wrong)
	char_8 = "a\ta\\a\"a\n";
	char_16 = "\154a\7a\ta\na\\\ta\\a\"a\n";
	char_32 = "a\7b\7c\7d\7e\7f\7g\7h\7\154a_a\ta\na\\\ta\\a\"a\n";
	char_64 = "\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n";	// {8{<tab>"test"<newline>}}
	    // NOTE do not use 0x as prefix for expected or actual patterns
	    // here, we use cdsDiff that converts such strings into <<address>>
	    // and thus ignores differences
        $fwrite(fd2, "\nexpected: char_2: hex:090a  .\t\n.");
        $fwrite(fd2, "\n  actual: char_2: hex:%h  .%s.", char_2, char_2);
        $fwrite(fd2, "\nexpected: char_2a: hex:0102  .\t\n.");
        $fwrite(fd2, "\n  actual: char_2a: hex:%h  .%s.", char_2a, char_2a);
	$fwrite(fd2, "\n");
	
        $fwrite(fd4, "\nexpected: char_4: hex:07373839  .789.");
        $fwrite(fd4, "\n  actual: char_4: hex:%h  .%s.", char_4, char_4);
	$fwrite(fd4, "\n");
	
        $fwrite(fd8, "\nexpected: char_8: hex:6109615c6122610a  .a\ta\\a\"a\n.");
        $fwrite(fd8, "\n  actual: char_8: hex:%h  .%s.", char_8, char_8);
	$fwrite(fd8, "\n");

        $fwrite(fd16, "\nexpected: char_16: hex:6c61076109610a615c09615c6122610a  .la\7a\ta\na\\\ta\\a\"a\n.");
        $fwrite(fd16, "\n  actual: char_16: hex:%h  .%s.", char_16, char_16);
	$fwrite(fd16, "\n");
	
        $fwrite(fd32, "\nexpected: char_32:  .a\7b\7c\7d\7e\7f\7g\7h\7\154a_a\ta\na\\\ta\\a\"a\n.");
        $fwrite(fd32, "\n  actual: char_32:  .%s.", char_32);
	$fwrite(fd32, "\n");

        $fwrite(fd64, "\nexpected: char_64:  .\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n\t\"\164\145\163\164\"\n.");
        $fwrite(fd64, "\n  actual: char_64:  .%s.", char_64);
	$fwrite(fd64, "\n");
     end
endmodule


