// test the interaction between format specifiers and null exprs
// inspired by bug 4254
// note that we do not match aldec but are somewhat close
// we do not match nc because it generates error messages in some cases

module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   reg [1:0] 	out;

   initial
      begin
	 reg3 = 3;
	 reg2 = 2;
	 out = reg3 - reg2;
      end
   
   initial
     begin
	$display("testing empty arg: there should be a space here ->",,"<-");
	$display("here we require a %%b arg .%b. and space specified", " ");
	$display("here we require a %%c arg .%c. and space specified", " ");
	$display("here we require a %%d arg .%d. and space specified", " ");
	$display("here we require a %%e arg .%e. and space specified", " ");
	$display("here we require a %%f arg .%f. and space specified", " ");
	$display("here we require a %%g arg .%g. and space specified", " ");
	$display("here we require a %%h arg .%h. and space specified", " ");
	$display("here we require a %%l arg .%l. and space specified", " ");
	$display("here we require a %%o arg .%o. and space specified", " ");
	$display("here we require a %%s arg .%s. and space specified", " ");
	$display("here we require a %%t arg .%t. and space specified", " ");
	$display("here we require a %%x arg .%x. and space specified", " ");
	// the following lines should generate a warning message  because null
	// argument supplied to a format specifier that normally consumes an argument
	$display("here we require a %%b arg .%b. and none  specified",); // <- note last arg is null
	$display("here we require a %%c arg .%c. and none  specified",);
	$display("here we require a %%d arg .%d. and none  specified",);
	$display("here we require a %%e arg .%e. and none  specified",);
	$display("here we require a %%f arg .%f. and none  specified",);
	$display("here we require a %%g arg .%g. and none  specified",);
	$display("here we require a %%h arg .%h. and none  specified",);
	$display("here we require a %%l arg .%l. and none  specified",);
	$display("here we require a %%o arg .%o. and none  specified",);
	$display("here we require a %%s arg .%s. and none  specified",);
	$display("here we require a %%t arg .%t. and none  specified",);
	$display("here we require a %%x arg .%x. and none  specified",);
     end
endmodule // top

