// last mod: Mon Feb 27 22:52:01 2006
// filename: test/langcov/Display/time4.v
// Description:  This test has $realtime divided by an integer, inspired by
// bug2209 and bug3179

`timescale 1ps/100fs

module time4(clk1);
   input clk1;

   always @ (posedge clk1)
     begin
        $display("$realtime/10 >", $realtime/10, "<    >%t<", $realtime/10);
     end
endmodule // top

