// last mod: Tue Feb 28 07:37:28 2006
// filename: test/langcov/Display/time3.v
// Description:  This test checks that %t works properly with all types of arguments
// currently fails because we do not get timescale info right bug 5234
`timescale 1ps/100fs

module top (clk1);
   input clk1;
   real  renum;
   integer innum;
   reg [10:0] sized;

   always @ (posedge clk1)
     begin
	renum = 3.145;
	innum = 10;
	sized = 25;
	
	$display("valuetype >                 raw<    >  with %%t formatting<");
	$display("----------------------------------------------------------");
        $display("$time     >", $time, "<    >%t<", $time);
	$display("real      >", renum, "<    >%t<", renum);
	$display("integer   >          ", innum, "<    >%t<", innum);
	$display("sized reg >                ", sized, "<    >%t<", sized);
     end
endmodule // top

