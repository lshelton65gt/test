// last mod: Thu May 25 08:33:53 2006
// filename: test/langcov/Display/formatSpecifier11.v
// Description:  This test uses a signed sized and unsigned sized variable in a
// $display, must be run with -2001


module formatSpecifier11(c, o);
   input     c;
   output    o;

   reg signed [3:0]  sindex4;
   reg        [3:0]  index4;

   reg signed [6:0]  sindex7;
   reg        [6:0]  index7;

   reg signed [7:0]  sindex8;
   reg        [7:0]  index8;

   reg signed [14:0]  sindex15;
   reg        [14:0]  index15;

   reg signed [15:0]  sindex16;
   reg        [15:0]  index16;

   reg signed [16:0]  sindex17;
   reg        [16:0]  index17;

   reg signed [31:0]  sindex32;
   reg        [31:0]  index32;

   reg signed [62:0]  sindex63;
   reg        [62:0]  index63;

   reg signed [63:0]  sindex64;
   reg        [63:0]  index64;

   reg signed [65:0]  sindex66;
   reg        [65:0]  index66;
   
   initial
     begin
	index4 = 7;
	$display ("index4: d%d b%b o%o h%h ", index4, index4, index4, index4);
	index4 = -7;
	$display ("index4: d%d b%b o%o h%h ", index4, index4, index4, index4);
	sindex4 = 7;
	$display ("sindex4: d%d b%b o%o h%h ", sindex4, sindex4, sindex4, sindex4);
	sindex4 = -7;
	$display ("sindex4: d%d b%b o%o h%h ", sindex4, sindex4, sindex4, sindex4);

	index7 = 14;
	$display ("index7: d%d b%b o%o h%h ", index7, index7, index7, index7);
	index7 = -14;
	$display ("index7: d%d b%b o%o h%h ", index7, index7, index7, index7);
	sindex7 = 14;
	$display ("sindex7: d%d b%b o%o h%h ", sindex7, sindex7, sindex7, sindex7);
	sindex7 = -14;
	$display ("sindex7: d%d b%b o%o h%h ", sindex7, sindex7, sindex7, sindex7);

	index8 = 15;
	$display ("index8: d%d b%b o%o h%h ", index8, index8, index8, index8);
	index8 = -15;
	$display ("index8: d%d b%b o%o h%h ", index8, index8, index8, index8);
	sindex8 = 15;
	$display ("sindex8: d%d b%b o%o h%h ", sindex8, sindex8, sindex8, sindex8);
	sindex8 = -15;
	$display ("sindex8: d%d b%b o%o h%h ", sindex8, sindex8, sindex8, sindex8);

	index15 = 30;
	$display ("index15: d%d b%b o%o h%h ", index15, index15, index15, index15);
	index15 = -30;
	$display ("index15: d%d b%b o%o h%h ", index15, index15, index15, index15);
	sindex15 = 30;
	$display ("sindex15: d%d b%b o%o h%h ", sindex15, sindex15, sindex15, sindex15);
	sindex15 = -30;
	$display ("sindex15: d%d b%b o%o h%h ", sindex15, sindex15, sindex15, sindex15);

	index16 = 31;
	$display ("index16: d%d b%b o%o h%h ", index16, index16, index16, index16);
	index16 = -31;
	$display ("index16: d%d b%b o%o h%h ", index16, index16, index16, index16);
	sindex16 = 31;
	$display ("sindex16: d%d b%b o%o h%h ", sindex16, sindex16, sindex16, sindex16);
	sindex16 = -31;
	$display ("sindex16: d%d b%b o%o h%h ", sindex16, sindex16, sindex16, sindex16);

	index17 = 32;
	$display ("index17: d%d b%b o%o h%h ", index17, index17, index17, index17);
	index17 = -32;
	$display ("index17: d%d b%b o%o h%h ", index17, index17, index17, index17);
	sindex17 = 32;
	$display ("sindex17: d%d b%b o%o h%h ", sindex17, sindex17, sindex17, sindex17);
	sindex17 = -32;
	$display ("sindex17: d%d b%b o%o h%h ", sindex17, sindex17, sindex17, sindex17);

	index32 = 63;
	$display ("index32: d%d b%b o%o h%h ", index32, index32, index32, index32);
	index32 = -63;
	$display ("index32: d%d b%b o%o h%h ", index32, index32, index32, index32);
	sindex32 = 63;
	$display ("sindex32: d%d b%b o%o h%h ", sindex32, sindex32, sindex32, sindex32);
	sindex32 = -63;
	$display ("sindex32: d%d b%b o%o h%h ", sindex32, sindex32, sindex32, sindex32);

	index63 = 126;
	$display ("index63: d%d b%b o%o h%h ", index63, index63, index63, index63);
	index63 = -126;
	$display ("index63: d%d b%b o%o h%h ", index63, index63, index63, index63);
	sindex63 = 126;
	$display ("sindex63: d%d b%b o%o h%h ", sindex63, sindex63, sindex63, sindex63);
	sindex63 = -126;
	$display ("sindex63: d%d b%b o%o h%h ", sindex63, sindex63, sindex63, sindex63);

	index64 = 127;
	$display ("index64: d%d b%b o%o h%h ", index64, index64, index64, index64);
	index64 = -127;
	$display ("index64: d%d b%b o%o h%h ", index64, index64, index64, index64);
	sindex64 = 127;
	$display ("sindex64: d%d b%b o%o h%h ", sindex64, sindex64, sindex64, sindex64);
	sindex64 = -127;
	$display ("sindex64: d%d b%b o%o h%h ", sindex64, sindex64, sindex64, sindex64);

	index66 = 255;
	$display ("index66: d%d b%b o%o h%h ", index66, index66, index66, index66);
	index66 = -255;
	$display ("index66: d%d b%b o%o h%h ", index66, index66, index66, index66);
	sindex66 = 255;
	$display ("sindex66: d%d b%b o%o h%h ", sindex66, sindex66, sindex66, sindex66);
	sindex66 = -255;
	$display ("sindex66: d%d b%b o%o h%h ", sindex66, sindex66, sindex66, sindex66);
	
     end
   always @(posedge c)
     begin 
        index16 = 7;
     end

   assign 	o = 1;
endmodule

