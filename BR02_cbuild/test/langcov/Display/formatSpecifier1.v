// test the way that format specifiers work in $display system task
// we expect to see something like:
// in module top,  out is: 3.<-note value then dot

module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;

//   always @ (posedge clk1)
   initial
     begin
	out = 3;
        $display("in module top,  %s", "out is: ", "%0d", out, ".<- note value then dot");
     end
endmodule // top

