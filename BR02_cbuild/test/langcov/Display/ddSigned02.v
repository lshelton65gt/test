// last mod: Fri Mar 31 15:14:32 2006
// filename: test/langcov/Display/ddSigned02.v
// Description:  This test is used to display signed values of different sizes.
// must be run with -2001 switch

module ddSigned02(input clock,
		  input signed [7:0] in8,
		  input signed [15:0] in16,
		  input signed [31:0] in32,
		  input signed [63:0] in64,
		  input signed [64:0] in65,
		  output reg signed [7:0] out,
		  output reg signed [7:0] out8,
		  output reg signed [15:0] out16,
		  output reg signed [31:0] out32,
		  output reg signed [63:0] out64,
		  output reg signed [64:0] out65);

   reg 			 signed [65:0] r1;

   initial
     begin
	r1 = 66'sb111111111111111111111111111111111100000000000000000000000000000011;
	$display ("r1: %d (hex:%h)", r1, r1);
     end

   always @(posedge clock)
     begin: main
	$display ("r1: %d (hex:%h)", r1, r1);
	out = r1;
	r1 = r1 + 1;
	out8 = in8;
	$display ("out8: %d (hex:%h)", out8, out8);
	out16 = in16;
	$display ("out16: %d (hex:%h)", out16, out16);
	out32 = in32;
	$display ("out32: %d (hex:%h)", out32, out32);
	out64 = in64;
	$display ("out64: %d (hex:%h)", out64, out64);
	out65 = in65;
	$display ("out65: %d (hex:%h)", out65, out65);
     end
endmodule
