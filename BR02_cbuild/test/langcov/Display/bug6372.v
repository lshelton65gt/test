// last mod: Wed Jul 19 16:06:29 2006
// filename: test/langcov/bug6372.v
// Description:  This test reports a sensitivity message about out1 (if run with
// -enableOutputSysTasks) but that message is unnecessary and confusing.


module bug6372(in1,out1);
  input [31:0] in1;
  output [32:0] out1;
  reg [32:0]    out1;
  
  always @(in1)
    begin
      out1 = in1;
      $display("%h %d", out1, out1);
    end
endmodule
  
