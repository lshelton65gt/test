// last mod: Mon Sep 18 16:19:17 2006
// filename: test/langcov/Display/formatSpecifier15.v
// Description:  This test checks for the proper operation of %e %f %g
// Gold file is from carbon.  There are slight differences between carbon and nc
// when the format specifier is of the form %6.0e  (where the fraction part is
// zero bits long).  


module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   reg [1:0] 	out;

   initial
      begin
	 reg3 = 3;
	 reg2 = 2;
	 out = 2;
      end
   
   initial
     begin
	// in the following, print a series of values twice so that we will see if any properties of print remain in effect after one value is printed

	// the following uses format %#.#.#e where # is a number, this is considered an invalid format specifier
	// $display(" 7.5 (format:    7.3.2           ) is:\te%7.3.2e, f%7.3.2f, g%7.3.2g\te%7.3.2e, f%7.3.2f, g%7.3.2g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);

	$display(" 7.5 (format:    0               ) is:\te%0e, f%0f, g%0g\te%0e, f%0f, g%0g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format  +7.3 showsign right) is:\te%+7.3e, f%+7.3f, g%+7.3g\te%+7.3e, f%+7.3f, g%+7.3g", 7.5, 7.5, 7.5,  7.5, 7.5, 7.5);
	$display("-7.5 (format: +7.3 showsign right) is:\te%+7.3e, f%+7.3f, g%+7.3g\te%+7.3e, f%+7.3f, g%+7.3g", -7.5, -7.5, -7.5, -7.5, -7.5, -7.5);
	$display(" 7.5 (format:  6.3          right) is:\te%6.3e, f%6.3f, g%6.3g\te%6.3e, f%6.3f, g%6.3g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format:  0.3          right) is:\te%0.3e, f%0.3f, g%0.3g\te%0.3e, f%0.3f, g%0.3g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format:  6.0          right) is:\te%6.0e, f%6.0f, g%6.0g\te%6.0e, f%6.0f, g%6.0g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format:  0.0          right) is:\te%0.0e, f%0.0f, g%0.0g\te%0.0e, f%0.0f, g%0.0g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format:               right) is:\te%e, f%f, g%g\te%e, f%f, g%g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: +6.3 showsign right) is:\te%+6.3e, f%+6.3f, g%+6.3g\te%+6.3e, f%+6.3f, g%+6.3g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: +0.3 showsign right) is:\te%+0.3e, f%+0.3f, g%+0.3g\te%+0.3e, f%+0.3f, g%+0.3g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: +6.0 showsign right) is:\te%+6.0e, f%+6.0f, g%+6.0g\te%+6.0e, f%+6.0f, g%+6.0g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: +0.0 showsign right) is:\te%+0.0e, f%+0.0f, g%+0.0g\te%+0.0e, f%+0.0f, g%+0.0g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: +    showsign right) is:\te%+e, f%+f, g%+g\te%+e, f%+f, g%+g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: -0.3 left          ) is:\te%-0.3e, f%-0.3f, g%-0.3g\te%-0.3e, f%-0.3f, g%-0.3g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: -6.0 left          ) is:\te%-6.0e, f%-6.0f, g%-6.0g\te%-6.0e, f%-6.0f, g%-6.0g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: -0.0 left          ) is:\te%-0.0e, f%-0.0f, g%-0.0g\te%-0.0e, f%-0.0f, g%-0.0g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: -    left          ) is:\te%e, f%f, g%g\te%e, f%f, g%g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: -6.3 left          ) is:\te%-6.3e, f%-6.3f, g%-6.3g\te%-6.3e, f%-6.3f, g%-6.3g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format  +0.3 showsign right) is:\te%+0.3e, f%+0.3f, g%+0.3g\te%+0.3e, f%+0.3f, g%+0.3g", 7.5, 7.5, 7.5,  7.5, 7.5, 7.5);
	$display(" 7.5 (format  +7.0 showsign right) is:\te%+7.0e, f%+7.0f, g%+7.0g\te%+7.0e, f%+7.0f, g%+7.0g", 7.5, 7.5, 7.5,  7.5, 7.5, 7.5);
	$display(" 7.5 (format  -0.3 left          ) is:\te%-0.3e, f%-0.3f, g%-0.3g\te%-0.3e, f%-0.3f, g%-0.3g", 7.5, 7.5, 7.5,  7.5, 7.5, 7.5);
	$display(" 7.5 (format  -7.0 left          ) is:\te%-7.0e, f%-7.0f, g%-7.0g\te%-7.0e, f%-7.0f, g%-7.0g", 7.5, 7.5, 7.5,  7.5, 7.5, 7.5);
	$display(" 7.5 (format  -0.0 left          ) is:\te%-0.0e, f%-0.0f, g%-0.0g\te%-0.0e, f%-0.0f, g%-0.0g", 7.5, 7.5, 7.5,  7.5, 7.5, 7.5);
	$display(" 7.5 (format  +0.0 showsign      ) is:\te%+0.0e, f%+0.0f, g%+0.0g\te%+0.0e, f%+0.0f, g%+0.0g", 7.5, 7.5, 7.5,  7.5, 7.5, 7.5);
	$display(" 7.5 (format  -7   left          ) is:\te%-0e, f%-0f, g%-0g\te%-0e, f%-0f, g%-0g", 7.5, 7.5, 7.5,  7.5, 7.5, 7.5);

     end

endmodule // top


