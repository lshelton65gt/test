// this file is the base for the flatten3*.v series
// test the interaction of flattening and file specifiers in the $fdisplay system task

module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   integer     fdtop;
   initial
     fdtop = $fopen("flatten3.fdtop.out");

   middle iMiddle(out, in1, in2, clk1, clk2);

   always @ (posedge clk1)
     begin
        $fdisplay(fdtop,"module top %b", {in1,in2});
     end
endmodule // top

   
module middle (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg  reg_out;

   integer     fdmiddle;
   initial
     fdmiddle = $fopen("flatten3.fdmiddle.out");

   
   assign    out[0] = reg_out;
   
   bottom ibottom(out, in1, in2, clk2);
   
   always @(posedge clk1) begin
      reg_out = in2;
      if ( in1 )
        $fdisplay(fdmiddle,"module middle %b", {in1,in2});
   end
   

endmodule // middle

module bottom (out, in1, in2, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk2;
   reg [1:0] out;
   integer     fdbottom;
   initial
     fdbottom = $fopen("flatten3.fdbottom.out");

   always @(posedge clk2) begin
      out[1] = in2;
      if ( in1 )
        $fdisplay(fdbottom,"module bottom: %b", {in1,in2});
   end
   

endmodule // bottom
