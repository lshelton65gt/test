// test how $display and fold interact (extracted from
// indus/memif_ctrl_central.v
// really a test of the operator== method for system tasks.

module top (out,clk, do_late_act);
   output [1:0] out;
   input clk;
   input [3:0] do_late_act;
   reg 	 [1:0] out;

    always @ (posedge clk)
      if(|do_late_act)
	case(do_late_act)
	  // only the first two branches of the case statement are
	  // identical, they should be folded
          4'b1000: $display($time,,"By-pass activation of virtual bank 2",);
          4'b0100: $display($time,,"By-pass activation of virtual bank 2",);
          4'b0010: $display($time,,"By-pass activation of virtual bank 1",);
          4'b0001: $display($time,,"By-pass activation of virtual bank 0",);
	endcase
   
   always @ (clk)
     begin
	out = do_late_act;
     end
endmodule // top


