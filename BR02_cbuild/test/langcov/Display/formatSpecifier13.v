// test the way that format specifiers work in $display system task
// in this case we test number presentation
// see formatspecifier8.v for formats that are not supported

module top (out, clk1);
   output [1:0] out;
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   reg [1:0] 	out;

   initial
      begin
	 reg3 = 3;
	 reg2 = 2;
	 out = 2;
      end
   
//   always @ (posedge clk1)
   initial
     begin
	$display("d15 (naturalsize) is        \tb%b, o%o, d%d, h%h", 15, 15, 15, 15);
	$display("d15 (compressed)  is             \tb%0b, o%0o, d%0d, h%0h", 15, 15, 15, 15);	    // currently we print leading zeros in %0b here, this is wrong
	$display("d15 (compressed, leftaligned) is \tb%-0b, o%-0o, d%-0d, h%-0h", 15, 15, 15, 15);
	$display("d15 (compressed, leftaligned) is \tb%-04b, o%-04o, d%-04d, h%-04h", 15, 15, 15, 15);
	$display("hffffffff is:\t\tb%0b, o%0o, d%0d, h%0h ", 32'hffffffff, 32'hffffffff, 32'hffffffff, 32'hffffffff);
	$display("hfffffffff is:\tb%0b, o%0o, d%0d, h%0h", 36'hfffffffff, 36'hfffffffff, 36'hfffffffff, 36'hfffffffff);
	$display("bzz is:\tb%0b, o%0o, d%0d, h%0h", 2'bzz, 2'bzz, 2'bzz, 2'bzz);
	// in the following, print a series of values twice so that we will see if any properties of print remain in effect after one value is printed
	$display(" 7.5 (format: 0)is:\t\t\te%0e, f%0f, g%0g\te%0e, f%0f, g%0g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format +7.3 showsign right) is:\te%+7.3e, f%+7.3f, g%+7.3g\te%+7.3e, f%+7.3f, g%+7.3g", 7.5, 7.5, 7.5,  7.5, 7.5, 7.5);
	$display("-7.5 (format: +7.3 showsign right) is:\te%+7.3e, f%+7.3f, g%+7.3g\te%+7.3e, f%+7.3f, g%+7.3g", -7.5, -7.5, -7.5, -7.5, -7.5, -7.5);
	$display(" 7.5 (format: 6.3 right) is:\t\te%6.3e, f%6.3f, g%6.3g\te%6.3e, f%6.3f, g%6.3g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display(" 7.5 (format: -6.3 left ) is:\t\te%-6.3e, f%-6.3f, g%-6.3g\te%-6.3e, f%-6.3f, g%-6.3g", 7.5, 7.5, 7.5, 7.5, 7.5, 7.5);
	$display("-7 is:\tb%0b, o%o d%d h%h", -7, -7, -7, -7);
	$display("The module is: %10m"); // allow width specification
     end

endmodule // top


