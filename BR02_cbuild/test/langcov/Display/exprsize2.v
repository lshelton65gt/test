// experiment with the size of displayed values and user specified format sizes.
module top(out, clk, data1);
   output out;
   input  clk;
   input  [4:0]data1;
   reg 	  out;

   reg 	  [15:0]reg16;
   reg 	  [31:0]reg32;



   initial
     begin
	out = 0;
	 reg16 = 16'b0000101010101010;
	 $display ("line0 16 bits  %%d.%d. \t%%0d.%0d. \t%%o.%o. %%0o.%0o. \t%%h.%h. %%0h.%0h.", reg16, reg16, reg16, reg16, reg16, reg16 );
	 reg16 = 16'b1010101010101010;
	 $display ("line1 16 bits  %%d.%d. \t%%0d.%0d. \t%%o.%o. %%0o.%0o. \t%%h.%h. %%0h.%0h.", reg16, reg16, reg16, reg16, reg16, reg16 );
	 $display(" ");
	 reg32 = 16'b0000101010101010;
	 $display ("line2 32 bits  %%d.%d. \t%%0d.%0d. \t%%o.%o. \t%%o.%0o. \t%%h.%h. %%0h.%0h.", reg32, reg32, reg32, reg32, reg32, reg32 );
	 reg32 = 16'b1010101010101010;
	 $display ("line3 32 bits  %%d.%d. \t%%0d.%0d. \t%%o.%o. \t%%0o.%0o. \t%%h.%h. %%0h.%0h.", reg32, reg32, reg32, reg32, reg32, reg32 );
	 reg32 = 32'b00001010101010100000101010101010;
	 $display ("line4 32 bits  %%d.%d. \t%%0d.%0d. \t%%o.%o. \t%%0o.%0o. \t%%h.%h. %%0h.%0h.", reg32, reg32, reg32, reg32, reg32, reg32 );
	 reg32 = 32'b10101010101010101010101010101010;
	 $display ("line5 32 bits  %%d.%d. \t%%0d.%0d. %%o.%o. \t%%0o.%0o. \t%%h.%h. %%0h.%0h.", reg32, reg32, reg32, reg32, reg32, reg32 );

	 reg32 = 32'b00001010101010100000101010101010;
	 $display ("line6 32 bits  %%d.%d. %%15d.%15d. %%o.%o. %%15o.%15o. %%h.%h. %%15h.%15h.", reg32, reg32, reg32, reg32, reg32, reg32 );
	 reg32 = 32'b10101010101010101010101010101010;
	 $display ("line7 32 bits  %%d.%d. %%15d.%15d. %%o.%o. %%15o.%15o. %%h.%h. %%15h.%15h.", reg32, reg32, reg32, reg32, reg32, reg32 );
      end

endmodule // top
