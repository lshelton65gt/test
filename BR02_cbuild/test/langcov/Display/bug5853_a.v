// last mod: Mon Mar 27 14:28:59 2006
// filename: test/langcov/Display/bug5853_a.v
// Description:  This test is a trimmed down version of bug5853.v


module bug5853_a(clock, in1, out1, out2);
   input clock;
   input in1;
   output out1;
   reg 	  out1;
   output [9*8:1] out2;
   reg [9*8:1] 	  out2;
   reg [4*8:1] 	  s1;
   reg [4*8:1] 	  s2;

   initial
     begin
	s1 = "ABC";
	s2 = "DEF";
	$display("%s\n", s1);
	$display("%s\n", s2);
	$display("%s\n", {s1,",",s2});
     end
   always @(posedge clock)
     begin: main
	out1 = in1;
	out2 = {s1,",",s2};
     end
endmodule
