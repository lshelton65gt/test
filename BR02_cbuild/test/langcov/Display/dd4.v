// an simpler version of dd3.v, splittable block that contains a $display sys
// task

module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;

   reg [1:0] BS_c1;
   reg [1:0] c2;
   reg dummy;
   reg [1:0] c1;
   reg q;
   reg [1:0] out;

   initial q = 0;
   initial begin
      dummy = 0;
      c1 = 0;
   end
   initial begin
      BS_c1 = 0;
      c2 = 0;
   end


   always @ (posedge clk1)
     begin
	q = in1;
	$display("d1 %b", c2);
     end

   always @(posedge clk2)
     begin
	out = BS_c1 & c2;
      if ( c1[0] )
	$display("d2 %b", BS_c1);
     end

   always @(q) begin
     {dummy,c1[1]} = {1'b0,q};
      $display("d3 %b", c1);
   end
   
   always @(q or c1 or c2 or in2) begin
      c1[0] = q;
      BS_c1 = c1;
      c2 = in2;
      if ( c2 )
	$display("d4 %b", c2);
     end
   

endmodule // top

   
