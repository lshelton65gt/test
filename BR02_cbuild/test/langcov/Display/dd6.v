/* testing of $display of a string parameter, similar to interra MISC151 testcase.
*/
 

module top (in1, out);

input  in1;
output out;
parameter param1 = "p_at_top";

mid #("p_at_inst") I1 (in1, out);

endmodule

module mid (in1, out);

input  in1;
output out;
parameter param1 = "p_local";

always @ (in1)
   $display ("param1=%s, in1=%b",param1, in1);

assign out = in1;

endmodule



