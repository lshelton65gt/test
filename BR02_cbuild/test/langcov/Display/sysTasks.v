/* simple testcase that tests the combination of $random $display and $stop */
/* send all output to stderr so that we do not have buffering problems */

module top (clk, in1, out, outR, outD, outS, outRD, outSD, outRS, outRSD);
   input clk;
   input in1;
   output out, outR, outD, outS, outRD, outSD, outRS, outRSD;
   reg    out, outR, outD, outS, outRD, outSD, outRS, outRSD;
   wire	  outR_w, outD_w, outS_w, outRD_w, outSD_w, outRS_w, outRSD_w;

   hasRand            i1(clk, in1, outR_w);
   hasDisplay         i2(clk, in1, outD_w);
   hasStop            i3(clk, in1, outS_w);
   hasRandDisplay     i4(clk, in1, outRD_w);
   hasStopDisplay     i5(clk, in1, outSD_w);
   hasRandStop        i6(clk, in1, outRS_w);
   hasRandDisplayStop i7(clk, in1, outRSD_w);

   initial
      begin
	 out = 0;
	 outR = 0;
	 outD = 0;
	 outS = 0;
	 outRD = 0;
	 outSD = 0;
	 outRS = 0;
	 outRSD = 0;
      end


   always @(posedge clk)
      begin
	 out = 1;
	 outR = outR_w;
	 outD = outD_w;
	 outS = outS_w;
	 outRD = outRD_w;
	 outSD = outSD_w;
	 outRS = outRS_w;
	 outRSD = outRSD_w;
      end

endmodule

module hasRand (clk, in1, out);
   input clk;
   input in1;
   output out;
   reg 	  out;

   always @(posedge clk)
      begin
	 out = $random;
      end
endmodule

module hasDisplay (clk, in1, out);
   input clk;
   input in1;
   output out;
   reg 	  out;

   initial
     out = 0;
   always @(posedge clk)
      begin
	 out = ~out;
	 $fdisplay(32'h80000002, "in hasDisplay outD: %h", out);
	 $fflush();
      end
endmodule

module hasStop (clk, in1, out);
   input clk;
   input in1;
   output out;
   reg 	  out;

   always @(posedge clk)
      begin
	 out = ~in1;
	 $stop();
	 $fflush();
      end
endmodule

module hasRandDisplay (clk, in1, out);
   input clk;
   input in1;
   output out;
   reg 	  out;

   always @(posedge clk)
      begin
	 $fdisplay(32'h80000002,"in hasRandDisplay: %h", $random);
	 $fflush();
	 out = $random;
	 $fflush();
      end
endmodule

module hasStopDisplay (clk, in1, out);
   input clk;
   input in1;
   output out;
   reg 	  out;

   always @(posedge clk)
      begin
	 out = ~in1;
	 $fdisplay(32'h80000002,"in hasStopDisplay: %h", out);
	 $stop();
      end
endmodule


module hasRandStop (clk, in1, out);
   input clk;
   input in1;
   output out;
   reg 	  out;

   initial
     out = 0;

   always @(posedge clk)
      begin
	 out = out | $random;
	 $stop();
	 $fflush();
      end
endmodule

module hasRandDisplayStop (clk, in1, out);
   input clk;
   input in1;
   output out;
   reg 	  out;

   always @(posedge clk)
      begin
	 $fdisplay(32'h80000002,"in hasRandDisplayStop: %h", $random);
	 out = in1;
	 $stop();
      end
endmodule
