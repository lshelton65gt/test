// last mod: original file
// filename: test/langcov/Display/fileSpecifier10.v
// Description:  This test tests to see if the conversion of functions to tasks
// causes an incorrect control flow situation. With v1.2774 this gets an
// assert in the generated code


module fileSpecifier10(clock, reset, out);
   input clock;
   input reset;
   output [31:0] out;
   reg [31:0] out;
   integer state;

   integer fd2;
   integer fd4;
   
   initial
      begin
	 state = 0;
	 // prime the file fileSpecifier10.fd2.out with a string
	 fd2 = $fopen("fileSpecifier10.test.out");
         $fdisplay(fd2, "in module top, opened file: %d", fd2);
	 $fclose(fd2);
      end

   always @(posedge clock)
     begin: main
	state = state +1;
	// first time through this, state will be 1, so we want to only open
	// fileSpecifier10.fd2.out, we should not be opening
	// fileSpecifier10.test.out, so it should always have the contents
	// defined in the initial block.
	fd4 = (state == 1) ? $fopen("fileSpecifier10.fd2.out") : $fopen("fileSpecifier10.test.out");
        $fdisplay(fd4, "in file: %d, state: %d", fd4, state);
	out = state;
     end
endmodule
