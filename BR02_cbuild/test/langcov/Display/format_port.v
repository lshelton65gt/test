module param_disp(clk, val);
  input clk;
  input val;

  display u1(clk,  "hello, world!   %d", val);
  display u2(!clk, "goodbye, world! %d", val);
endmodule

module display(clk, str, val);
  input clk, val;
  input [18*8 - 1:0] str;

  always @(posedge clk)
    $display(str, val);
endmodule
