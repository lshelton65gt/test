// test the way that multiple file specifiers work with the $fdisplay system task
// in both of the output files we expect to see something like:
// in module top, opened file:           6
// we close the fd2, then write a second line to both files but it should only
// appear in fd4:
// wrote this after file 2 was closed: fd =           6



module top (out, in1, in2, clk1, clk2);
   output [1:0] out;
   input in1;
   input [1:0] in2;
   input clk1, clk2;
   reg 	 [1:0] out;
   integer     fd2;
   integer     fd4;
//   reg [31:0]  fd;
   integer     fd;

   initial
     out = 3;
   
//   always @ (posedge clk1)
   initial
     begin
	fd2 = $fopen("fileSpecifier2.fd2.out");
	fd4 = $fopen("fileSpecifier2.fd4.out");
	fd = fd2 | fd4;
        $fdisplay(fd, "in module top, opened file: fd = %d", fd);
	$fclose(fd2);
        $fdisplay(fd, "wrote this after file 2 was closed: fd = %d", fd);	
     end
endmodule // top

