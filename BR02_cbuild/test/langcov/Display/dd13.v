// last mod: Tue Oct  5 09:08:18 2004
// filename: test/langcov/Display/dd13.v
// Description:  test the support for $display within a task (this task has no
// output so the $display must keep it alive)

module top (clk, out, addressLong);
   input clk;
   input [100:0] addressLong;
   output [3:0] out;
   reg [3:0] 	out;
   reg [3:0] mem1 [7:0];
   reg [2:0] address;

   task testtask;

      input	[3:0]		level;
      
      reg	[3:0]		level;
      begin
	 $display("testing, level = %0d\n", level);
      end
   endtask

   initial
      begin
	 mem1[0] = 0; mem1[1] = 1; mem1[2] = 2; mem1[3] = 3;
	 mem1[4] = 4; mem1[5] = 5; mem1[6] = 6; mem1[7] = 7;
      end


   always @(posedge clk) begin
      testtask(addressLong[3:0]);
      address = addressLong[2:0];
      out = mem1[address];      // keep it alive
   end
   
endmodule



