// filename: test/langcov/V2K/test1.v
// Description:  This test uses the ++ operator, requires -sverilog


module test1(clock, in1, in2, out1) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;
   reg [31:0] i;

   always @(posedge clock)
     begin: main
	out1 = 0;
	for ( i = 0; i < 7; i++ )
	   begin
              out1 = out1 + (in1[i] & in2[i]);
	   end
     end
endmodule
