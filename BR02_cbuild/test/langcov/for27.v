// testing of a FOR loop where the termination condition does not depend on loop
// variable, and the loop variable can be eliminated because it is not used
// inside the loop.  most bits of output are undriven

module top ( out, clk, in, doOneLoop );
   output [7:0] out;
   reg [7:0] 	out;
   input 	clk;
   input [7:0] 	in;
   input 	doOneLoop;
   

   reg 		keepGoing;
   integer 	i;
   always @ (posedge clk)
     begin
	keepGoing = doOneLoop;
	for (i=0 ; keepGoing != 1'b0 ; i=i+1  )
	  begin
	     keepGoing = ~keepGoing;
             out[1] = in[1];
	  end // for
     end // always

endmodule // top
