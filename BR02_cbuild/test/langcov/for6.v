module top ( out, clk, in );
   output [7:0] out;
   reg [7:0] 	out;
   input 	clk;
   input [7:0] 	in;
   
   integer 	i;
   
   always @ (posedge clk)
     begin
	out = 8'b0; // avoids any undriven/tristate problems
	for ( i = 0 ; i <= 7 ; i = i + 1 )
	  begin
             out[i] = in[i];
	  end
     end

endmodule // top
