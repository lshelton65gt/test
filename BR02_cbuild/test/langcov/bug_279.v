// Test assigns of z-only expressions.
module top(w, v, u, sel);
input sel;
output w;
output [2:0] v;
output [124:0] u;
pullup(w);
assign w = 1'bz;
assign v = sel ? 3'bzzz : 3'bzzz;
assign u = sel ? 125'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz : 125'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
endmodule
