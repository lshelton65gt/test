// Test nonblocking assigns in tasks and reordering
module top(i1, i2, o);
   input i1;
   input i2;
   output o;
   reg 	  o;

   task nonblock;
      input [1:0] a, b;
      output [1:0] o;
      reg [1:0] r;
      begin
	 r <= a + b;
	 o <= r;
      end
   endtask // nonblock

   always
     begin
	nonblock(i1, i2, o);
     end

endmodule // top
