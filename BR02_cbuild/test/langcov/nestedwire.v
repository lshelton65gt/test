/* Trivial test of nesting input wire connects */

module top(in1, out1);
	input in1;
	output out1;

	micro x(in1,out1);
endmodule

module micro(a,b);
	input a;
	output b;

	nano y(a,b);
endmodule

module nano(in, out);
	input in;
	output out;

	assign out = in;
endmodule
