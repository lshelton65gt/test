/* Test gate primitives. */
module top (i1, i2, i3,
            i4, i5,
            andprim1, andprim2, andprimarray, notprimarray,
            andprim, nandprim, orprim, norprim, xorprim, xnorprim, bufprim,
            notprim
           );

input i1, i2, i3;
input [1:4] i4;
input [0:3] i5;
output andprim1, andprim2, andprimarray, notprimarray;
output andprim, nandprim, orprim, norprim, xorprim, xnorprim, bufprim;
output notprim;

and(andprim1, i1, i2);
and(andprim2, i1, i2, i3);

and inst1 [3:0] (andprimarray, i4, i5);
not inst2 [3:0] (notprimarray, i4);


and(andprim, i1, i2);
nand(nandprim, i1, i2);
or(orprim, i1, i2);
nor(norprim, i1 ,i2);
xor(xorprim, i1, i2);
xnor(xnorprim, i1, i2);
buf(bufprim, i1);
not(notprim, i1);

endmodule
