module flop(clk, in, out);
   input clk, in;
   output out;

   wire   clk_d, in_d;
   reg 	  out;

   always @(posedge clk_d)
     out <= in_d;

   specify
      $setuphold(posedge clk, posedge in, 1, 1, , , , clk_d, in_d);
      $setuphold(posedge clk, negedge in, 1, 1, , , , clk_d, in_d);
   endspecify
   
endmodule // flop