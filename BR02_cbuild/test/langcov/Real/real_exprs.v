// last mod: Thu Apr 28 16:28:27 2005
// filename: test/langcov/real_exprs.v
// Description:  This test is to test the correct operation of expression types
// that include real values


module real_exprs(clock, real_whole, real_fraction, in_signed, in_unsigned,
		  out_real_whole, out_real_fraction, out_signed, out_unsigned);
   input clock;
   input [7:0] real_whole, real_fraction; // combined these define a real number
   input signed [7:0] in_signed;
   input        [7:0] in_unsigned;
   output integer      out_real_whole;
   output reg        [9:0] out_real_fraction;
   output reg signed [9:0] out_signed;
   output reg        [9:0] out_unsigned;

   real 	       real_input, real_internal;
   
  
   always @(posedge clock)
     begin: main
	real_input = real_whole + (real_fraction / 1000.0);

	real_internal = real_input + real_input;
	out_signed =    real_input + real_input;
	out_unsigned =  real_input + real_input;
	$display("R + R (%8.4f + %8.4f)  => real %10.4f, signed %d unsigned %d",real_input, real_input, real_internal, out_signed, out_unsigned);

	real_internal = real_input + in_signed;
	out_signed =    real_input + in_signed;
	out_unsigned =  real_input + in_signed;
	$display("R + S (%8.4f +     %d)  => real %10.4f, signed %d unsigned %d",real_input, in_signed,real_internal, out_signed, out_unsigned);

	real_internal = real_input + in_unsigned;
	out_signed =    real_input + in_unsigned;
	out_unsigned =  real_input + in_unsigned;
	$display("R + U (%8.4f +      %d)  => real %10.4f, signed %d unsigned %d",real_input, in_unsigned, real_internal, out_signed, out_unsigned);

	real_internal = in_signed + real_input;
	out_signed =    in_signed + real_input;
	out_unsigned =  in_signed + real_input;
	$display("S + R (    %d + %8.4f)  => real %10.4f, signed %d unsigned %d",in_signed, real_input, real_internal, out_signed, out_unsigned);

	real_internal = in_signed + in_signed;
	out_signed =    in_signed + in_signed;
	out_unsigned =  in_signed + in_signed;
	$display("S + S (    %d +     %d)  => real %10.4f, signed %d unsigned %d",in_signed, in_signed,real_internal, out_signed, out_unsigned);

	real_internal = in_signed + in_unsigned;
	out_signed =    in_signed + in_unsigned;
	out_unsigned =  in_signed + in_unsigned;
	$display("S + U (    %d +      %d)  => real %10.4f, signed %d unsigned %d",in_signed, in_unsigned, real_internal, out_signed, out_unsigned);

	real_internal = in_unsigned + real_input;
	out_signed =    in_unsigned + real_input;
	out_unsigned =  in_unsigned + real_input;
	$display("U + R (     %d + %8.4f)  => real %10.4f, signed %d unsigned %d",in_unsigned, real_input, real_internal, out_signed, out_unsigned);

	real_internal = in_unsigned + in_signed;
	out_signed =    in_unsigned + in_signed;
	out_unsigned =  in_unsigned + in_signed;
	$display("U + S (     %d +     %d)  => real %10.4f, signed %d unsigned %d",in_unsigned, in_signed,real_internal, out_signed, out_unsigned);

	real_internal = in_unsigned + in_unsigned;
	out_signed =    in_unsigned + in_unsigned;
	out_unsigned =  in_unsigned + in_unsigned;
	$display("U + U (     %d +      %d)  => real %10.4f, signed %d unsigned %d",in_unsigned, in_unsigned, real_internal, out_signed, out_unsigned);
	
	// convert to output
	out_real_whole = 0;
	out_real_whole = real_internal;	// get whole fraction (this is rounded)
	out_real_fraction = 0;
	out_real_fraction = (real_internal-out_real_whole)*1000;

     end
endmodule
