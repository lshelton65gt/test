// last mod: Wed May 31 09:04:50 2006
// filename: test/langcov/Real/bug2209_10.v
// Description:  This test checks the correct operation of bitstoreal with dirty bits
// from vectors of different sizes


module bug2209_10(clock, in1, in2, out1);
   input clock;
   input [15:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   real r;
   reg [31:0] i;
   reg [63:0] long_i;
   reg [127:0] long_long_i;


   always @(posedge clock)
     begin: main
	// convert a 15 bit parsel of a 32 bit number
	i = {in1,in2};
	r = $bitstoreal( i[7 +:15] );
	$display("r: %f", r);

	// convert a 50 bit parsel of a 64 bit number
	long_i = {i,in2,in1};
	r = $bitstoreal( long_i[10 +:50] );
	$display("r: %f", r);

	// convert a 80 bit parsel of a 128 bit number
	long_long_i = {i,in2,in1,in1,i,in2};
	r = $bitstoreal( long_long_i[20 +:80] );
	$display("r: %f", r);


	// convert a 15 bit parsel of a 32 bit number, partially out of range
	i = {in1,in2};
	r = $bitstoreal( i[20 +:15] );
	$display("r: %f", r);

	// convert a 50 bit parsel of a 64 bit number, partially out of range
	long_i = {i,in2,in1};
	r = $bitstoreal( long_i[20 +:50] );
	$display("r: %f", r);

	// convert a 80 bit parsel of a 128 bit number, partially out of range
	long_long_i = {i,in2,in1,in1,i,in2};
	r = $bitstoreal( long_long_i[50 +:80] );
	$display("r: %f", r);
	
	out1 = r;
     end
endmodule
