// last mod: Fri Jun  9 09:24:58 2006
// filename: test/langcov/Real/bug2209_9.v
// Description:  This test checks the correct operation of itor with dirty bits
// from vectors of different sizes


module bug2209_9(clock, in1, in2, out1);
   input clock;
   input [15:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   real r;
   reg [31:0] i;
   reg [63:0] long_i;
   reg [127:0] long_long_i;


   always @(posedge clock)
     begin: main
	// convert a 15 bit parsel of a 32 bit number
	i = {in1,in2};
	r = $itor( i[7 +:15] );
	$display("r1: %f", r);

	// convert a 50 bit parsel of a 64 bit number
	long_i = {i,(2+in2),(1+in1)};
	r = $itor( long_i[10 +:50] );
	$display("r2: %f", r);

	long_long_i = {i,in2,(1+in1),(in1+2),i,(in2+1)};

	// convert a 59 bit parsel of a 128 bit number
	r = $itor( long_long_i[25 +:59] );
	$display("r4: %f, long_long_i[25 +:59]: %d %x", r, long_long_i[25 +:59], long_long_i[25 +:59]);

	// convert a 80 bit parsel of a 128 bit number
	r = $itor( long_long_i[20 +:80] );
	$display("r5: %f, long_long_i[20 +:80]: %d %x", r, long_long_i[20 +:80], long_long_i[20 +:80]);

	
	out1 = r;

     end
endmodule
