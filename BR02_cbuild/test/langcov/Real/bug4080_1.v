// last mod: Tue May 30 15:41:32 2006
// filename: test/langcov/Real/bug4080_1.v
// Description:  This test contains the main feature of the problem reported in
// bug4080


module bug4080_1(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
	$display("realtime: %f", $realtime);
	if ( $realtime != 0 )
	   if ( $realtime == $time )
	     out1 = in1 & in2;
	   else
	     out1 = in1 | in2;
	else
	  out1 = 0;
     end
endmodule
