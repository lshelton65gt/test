// Test various real number constructs
// 1) real parameter
// 2) real & realtime net declarations
// 3) assignment to real & realtime nets from $realtime
// 4) real arithmetic
// 5) real comparison operators
// 6) blocking & nonblocking assignment of integer literals to a real variable

`timescale 1ns/1ns

module mt46v64m4(clk, data);
  parameter tRC = 600.12345;
  input clk;
  output data;
  reg data;

  real realout;
  real r1;
  real r2;
  realtime RC_chk3;

  initial begin
    RC_chk3 = 2.71828E-1;
    r1 = 1;
    realout = $realtime;
    r2 <= 2;
    RC_chk3 = $realtime;
  end

  always @(posedge clk)
  begin
    if (($realtime - RC_chk3) < tRC) begin
      r1 = 2;
      r2 <= r1;
       if (r1 > 10 )
	 r1 = 5.5;
       else
	 r1 = r1 + 1;
      $display( "%m : at time %t (r1: %f) ERROR: tRC violation during Activate bank",
                $realtime, r1);
    end
    else
    begin
    $display( "%m : r1: %f at time %t", r1, $realtime);
    realout = realout + 1.25;
    if (r1 > 10 )
      r1 = 5.5;
    else
      r1 = r1 + 1;
    r2 <= 7;
    end
    if (RC_chk3 < tRC + 3.14159 + r2) begin
      $display( "%m : at time %t Message 2",
                $realtime);
    end
    realout = $realtime;
  end

    always @(negedge clk )
     begin : outputassign
       if ( realout > 3.14159 )
         begin // after realtime is > pi output is one
           data <= 1'b1;
         end
       else
         begin
           data <= 1'b0;
         end
       end
endmodule
