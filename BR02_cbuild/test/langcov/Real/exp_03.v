// last mod: Fri Nov 17 10:18:28 2006
// filename: test/langcov/Real/exp_03.v
// Description:  This test checks for the proper conversion from unsigned to
// real operands (in1 and in2 in the two assignments).  We expect that out1 and
// out2 should always match, because they should be converted to real before the addition


module exp_03(clock, in1, in2, in3, out1, out2);
   input clock;
   input [2:0] in1;
   input [2:0] in2;
   input [1:0] in3;
   output [7:0] out1, out2;
   reg [7:0] out1, out2;

   always @(posedge clock)
     begin: main
       out1 = (in1 + in2 + 0) ** $signed(in3); // inclusion of +0 causes the
				// size of the addition to be 32 bits (before
				// conversion to real because of the signed
				// operand to the **
       out2 = (in1 + in2) ** $signed(in3); // here in1 and in2 should be
				// converted to real before addition.
     end
endmodule
