// last mod: Tue May 30 07:42:10 2006
// filename: test/langcov/Real/bug2209_7.v
// Description:  This test is a simple use of $rtoi and $itor with constants

module bug2209_7(clock, in1, in2, out1);
   input clock;
   input [31:0] in1, in2;
   output [31:0] out1;
   reg [31:0] out1;
   real      rb0, ra1, rb1;
   integer   ia1, ia2, ib1, ib64;
   reg [63:0] rega64, regb64;

   initial
     begin
	ia1 = 501;
	ia2 = 250;
	ra1 = $itor(ia1 - ia2);
	rega64 = $realtobits(ra1);

	rb0 = 1001.001;
	rb1 = 2001.0000002;
	ib1 = $rtoi(rb0 + rb1);
	ib64 = ib1;
	regb64 = $realtobits(rb0+rb1);
	$display("ia1: %d  ia2: %d diff:(r) %f  rb0: %f  rb1: %20.8f  ib1: %d, rega64: %b, ib64: %d, regb64: %b",
		      ia1,     ia2,        ra1,     rb0,     rb1,     ib1, rega64, ib64, regb64);

	ia1 = -501;
	ia2 = 250;
	ra1 = $itor(ia1 - ia2);

	rb0 = 1001.001;
	rb1 = -2001.0000002;
	ib1 = $rtoi(rb0 + rb1);
	ib64 = ib1;
	regb64 = $realtobits(rb0+rb1);
	$display("ia1: %d  ia2: %d diff:(r) %f  rb0: %f  rb1: %20.8f  ib1: %d, rega64: %b, ib64: %d, regb64: %b",
		      ia1,     ia2,        ra1,     rb0,     rb1,     ib1, rega64, ib64, regb64);


	ia1 = 501;
	ia2 = -250;
	ra1 = $itor(ia1 - ia2);

	rb0 = -1001.001;
	rb1 = 2001.0000002;
	ib1 = $rtoi(rb0 + rb1);
	ib64 = ib1;
	regb64 = $realtobits(rb0+rb1);
	$display("ia1: %d  ia2: %d diff:(r) %f  rb0: %f  rb1: %20.8f  ib1: %d, rega64: %b, ib64: %d, regb64: %b",
		      ia1,     ia2,        ra1,     rb0,     rb1,     ib1, rega64, ib64, regb64);

	ia1 = -501;
	ia2 = -250;
	ra1 = $itor(-501 - (-250));

	rb0 = -1001.001;
	rb1 = -2001.0000002;
	ib1 = $rtoi(-1001.001 + (-2001.0000002));
	ib64 = 0;
	regb64 = $realtobits(-1001.001 + (-2001.0000002));
	$display("ia1: %d  ia2: %d diff:(r) %f  rb0: %f  rb1: %20.8f  ib1: %d, rega64: %b, ib64: %d, regb64: %b",
		      ia1,     ia2,        ra1,     rb0,     rb1,     ib1, rega64, ib64, regb64);

	ia1 = 501;
	ia2 = -250;
	ra1 = $itor(501 - (-250));

	rb0 = -1001.001;
	rb1 = 2001.0000002;
	ib1 = $rtoi(-1001.001 + (2001.0000002));
	ib64 = 0;
	regb64 = $realtobits(-1001.001 + (2001.0000002));
	$display("ia1: %d  ia2: %d diff:(r) %f  rb0: %f  rb1: %20.8f  ib1: %d, rega64: %b, ib64: %d, regb64: %b",
		      ia1,     ia2,        ra1,     rb0,     rb1,     ib1, rega64, ib64, regb64);
	
	out1 = 0;
     end
endmodule
