// last mod: Wed May 17 16:05:01 2006
// filename: test/langcov/bug2209_1.v
// Description:  This test is a simple use of $rtoi and $itor


module bug2209_1(clock, in1, in2, out1);
   input clock;
   input [31:0] in1, in2;
   output [31:0] out1;
   reg [31:0] out1;
   real      r0, r1, r2;
   integer   i1;

   always @(posedge clock)
     begin: main
	r0 = in1 + in2;
	r1 = r0 / 1000.0;
	i1 = $rtoi(r1);
	r2 = $itor(i1+2);
	$display("in1: %d  in2: %d sum: %f  r1: %f  i1: %d  r2: %f",
		 in1, in2, in1+in2, r1, i1, r2);
	out1 = i1;
     end
endmodule
