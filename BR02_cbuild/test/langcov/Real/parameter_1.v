// last mod: Tue May 30 16:51:25 2006
// filename: test/langcov/Real/parameter_1.v
// Description:  This test checks the proper operation of parameters and reals


module parameter_1(input clock, input [7:0] in1, in2, output [7:0] out1);
   parameter t_r = -100.001;
   parameter t_i = -99;

   real      t_r1;
   integer   t_i1;

   reg [7:0] temp1;
   
   initial
      begin
	 t_r1 = -98.99;
	 t_i1 = -98;
      end
   
   always @(posedge clock)
     begin: main
	temp1 = {( t_r1 > t_r ), ( t_r1 > t_i ), (t_i1 > t_r), (t_i1 > t_i)};
	t_r1 = t_r1 + 100;
	t_i1 = t_i1 + 100;
     end

   mid #(10.01,9) i1(out1, temp1, $realtobits(t_r1), t_i1);
endmodule

module mid(output reg [7:0] out, input [7:0] in, input [63:0] t_r1, input signed [31:0] t_i);
   parameter m_r = 20.02;
   parameter m_i = 19;

   real      r1;

   always @(in, out, r1, t_i, t_r1)
      begin
	 r1 = $bitstoreal(t_r1);
	 $display ("out %b, in: %b, r1: %f",
		   out, in, r1);
	 out = {(r1 > m_r), (r1 > m_r), (t_i > m_r), (t_i > m_r), (in[3:0])};
      end
endmodule
