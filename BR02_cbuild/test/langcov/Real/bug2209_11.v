// last mod: Wed Jun  7 15:11:07 2006
// filename: test/langcov/Real/bug2209_11.v
// Description:  This test checks for proper operation of $roti and $itor
// it checks for results longer than an integer and uses both positive and
// negative values
// gold file from nc, aldec gets it wrong

module bug2209_11(clock, in1, in2, out1);
   input clock;
   input [31:0] in1, in2;
   output [31:0] out1;
   reg [31:0] out1;
   real      r1, r2, r3, r4;
   integer   i1;
   reg 	     signed [63:0] long_i1;
   reg 	     signed [66:0] extra_long_i1;

   always @(posedge clock)
     begin: main
	r1 = (in1 + in2) / 1000.0;


	i1 = $rtoi(r1);
	r1 = r1+1;
	long_i1 = $rtoi(r1);
	extra_long_i1 = $rtoi(r1+1);
	
	r2 = $itor(i1+2);
	r3 = $itor(long_i1);
	r4 = $itor(extra_long_i1);
	
	$display("in1: %d  in2: %d, i1: %h, long_i1: %h, extra_long_i1: %h, r1: %f,  r2: %f, r3: %f, r4, %f", 
		 in1, in2, i1, long_i1, extra_long_i1, r1, r2, r3, r4);

	// now switch to a negative real number

	r1 = -r1;
	
	i1 = $rtoi(r1);
	r1 = r1+1;
	long_i1 = $rtoi(r1);
	extra_long_i1 = $rtoi(r1+1);
	
	r2 = $itor(i1+2);
	r3 = $itor(long_i1);
	r4 = $itor(extra_long_i1);
	$display("in1: %d  in2: %d, i1: %h, long_i1: %h, extra_long_i1: %h, r1: %f,  r2: %f, r3: %f, r4, %f", 
		 in1, in2, i1, long_i1, extra_long_i1, r1, r2, r3, r4);

	
	out1 = i1;
     end
endmodule
