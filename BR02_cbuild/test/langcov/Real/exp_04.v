// last mod: Mon Nov 20 08:17:46 2006
// filename: test/langcov/Real/exp_04.v
// Description:  This test checks for proper coercion of operands when left op
// of ** is signed.  we expect all outputs to be equivalent.


module exp_04(clock, in1, in2, in3, out1, out2, out3);
   input clock;
   input [2:0] in1;
   input [2:0] in2;
   input [1:0] in3;
   output [7:0] out1, out2, out3;
   reg [7:0] 	out1, out2, out3;
   integer 	Stemp;

   always @(posedge clock)
     begin: main
	// inclusion of +0 causes the size of the addition to be 32 bits (before
	// conversion to real (because 0 (an integer) is considered signed)
	out1 = ($signed(in1) + $signed(in2) + 0) ** in3;

	// here in1 and in2 should be converted to real before addition.
	out2 = ($signed(in1) + $signed(in2)) ** in3;


	Stemp = $signed(in1) + $signed(in2);
	out3 = Stemp ** in3;	// Stemp should be converted to real.
     end
endmodule
