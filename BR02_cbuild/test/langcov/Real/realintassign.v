module top(clk);
input clk;

integer i, j;

always @(clk) begin
    i <= 1.2;
    i <= 1.8;
    j = 1.1;
    j = 1.7;
    j = 7.0;
end

endmodule
