// last mod: Tue Jun 13 11:56:03 2006
// filename: test/langcov/Real/bug2209_10c.v
// Description:  This test checks $bitstoreal using a 80 bit moving partselect
// of a constant.  The constant is setup so that the partselect will contain
// large negative numbers, small 


module bug2209_10c(clock, in1, in2, out1);
   input clock;
   input [15:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   real r;
   reg [127:0] long_long_i;
   integer     position;

   initial
     begin
	position = 16;
	long_long_i = {28'b1111111111111111111111111111,
		       84'b010000000000000000000000000000000000000000000000000000000000000000000000000000001001,
		       16'b1111111111111111};
     end

   always @(posedge clock)
     begin: main
	// convert a 80 bit parsel of a 128 bit number
	// in this case the position of the bit segement shifts right each time
	// the always block is executed
	r = $bitstoreal( long_long_i[position +:80] );
	$display("r3: %f, long_long_i[%d +:80]: %d %x", r, position,
		 long_long_i[position +:80], long_long_i[position +:80]);
	position = position + 1;
	if ( position+80  > 127 )
	  position = 0;

	
	out1 = r;

     end
endmodule
