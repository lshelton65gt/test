// last mod: Fri Jun  9 16:06:52 2006
// filename: test/langcov/Real/bug2209_9c.v
// Description:  This test checks the correct operation of itor with dirty bits
// from vectors of different sizes, in particular it uses a contrived pattern to
// check for the conversion to real of numbers larger than 64 bits that have
// many leading zeros


module bug2209_9c(clock, in1, in2, out1);
   input clock;
   input [15:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   real r;
   reg [31:0] i;
   reg [63:0] long_i;
   reg [127:0] long_long_i;
   integer     position;

   initial
     begin
	position = 18;
	long_long_i = {28'b1111111111111111111111111111,
		       80'b00000000000000000000000000000000000000000000000000000000000000100000000000000000,
		       20'b11111111111111111111};
     end

   always @(posedge clock)
     begin: main
	// convert a 80 bit parsel of a 128 bit number
	// in this case the position of the bit segement moves each time the
	// always block shifts
	r = $itor( long_long_i[position +:80] );
	$display("r3: %f, long_long_i[%d +:80]: %d %x", r, position,
		 long_long_i[position +:80], long_long_i[position +:80]);
	position = position + 1;
	if ( position+80  > 127 )
	  position = 0;

	
	out1 = r;

     end
endmodule
