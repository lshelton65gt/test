// last mod: Thu Dec 28 12:42:33 2006
// filename: test/langcov/Real/exp_01.v
// Description:  This test checks for the proper coercion of non-self determined
// operands to real before expression evaluation.
// currently fails with -newSizing, and works without, problem is when input are
// 3 2 2 and output is Sout1 (we intepret S3 as -2, aldec and nc handle it as 2)

`define OP1 **
`define OP2 **
`define SIZE1 2
`define SIZE2 2
`define SIZE3 2
`define SIZE4 2

module exp_01(
   input [`SIZE1-1:0] in1,
   input [`SIZE2-1:0] in2,
   input [`SIZE3-1:0] in3,
   output reg signed [`SIZE4-1:0] Sout1, // signed
   output reg        [`SIZE4-1:0] Uout2); // unsigned

   reg signed [`SIZE1-1:0] S1;
   reg        [`SIZE1-1:0] U1;
   reg signed [`SIZE2-1:0] S2;
   reg        [`SIZE2-1:0] U2;
   reg signed [`SIZE3-1:0] S3;
   reg        [`SIZE3-1:0] U3;

   
   always @(*)
     begin: main
	S1 = in1;
	U1 = in1;
	S2 = in2;
	U2 = in2;
	S3 = in3;
	U3 = in3;

	// in the following the addition/subtraction should be done as real, not
	// as 2 bits which is what we would see if the operands were not coerced
	// to reals
        Sout1 = (((U1+2'b11)-2'b11) `OP1  U2) `OP2 S3; // (((unsigned+3)-3) `OP unsigned) `OP signed

	// in the following the addition/subtraction is done in 2 bits (so loss
	// of msb) because all operands are 2 2 bits wide
        Uout2 = (((U1+2'b11)-2'b11) `OP1  U2) `OP2 U3; // (((unsigned+3)-3) `OP unsigned) `OP unsigned

	
     end
endmodule

