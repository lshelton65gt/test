module bug11531(clk, in, out);
   input clk, in;
   output out;

   reg    out;

   realtime   last_time;

   initial last_time = 0;
   
   always @(posedge clk) begin
      $display("Time since last clk: %t", $realtime - last_time);
      last_time = $time;
      out <= in;
   end
endmodule
