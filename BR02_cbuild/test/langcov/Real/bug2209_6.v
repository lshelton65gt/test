// last mod: Tue May 30 16:11:03 2006
// filename: test/langcov/Real/bug2209_6.v
// Description:  This test checks for the correct support of real variables
// some of the problems identified in bug 2209 are tested here


module bug2209_6(clock, out1);
   input clock;
   output [31:0] out1;
   reg [31:0] out1;

   real      r1;
   time      t1;
   integer   i1;

   initial
      begin
	r1 = 2;			// assign integer constant to a real variable
	i1 = 2.1;		// assign a real constant to a integer variable
	t1 = 2.9;		// assign a real constant to a time variable
      end

   always @(posedge clock)
     begin: main
	r1 = r1 + 1;		// arith of real variable with integer constant
	i1 = i1 - 2.1;		// arith operation of integer variable with real constant
	t1 = t1 + 2.9;		// arith operation of time variable with real constant
	$display ("correct   format spec r1 %3.2f, i1 %d, t1 %d", r1, i1, t1); // display with proper format characters
	$display ("incorrect format spec r1 %d,    i1 %3.2f, t1 %3.2f", r1, i1, t1); // this display requires conversion
	out1 = r1 + i1 - t1;	// arith operation of three types into a reg
	if ( r1 > 10 )		// compare real and integer constant
	  r1 = 1;
     end
endmodule
