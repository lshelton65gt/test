// last mod: Tue Jun 13 14:59:54 2006
// filename: test/langcov/Real/nan_1.v
// Description:  This test tries to create a not-a-number real value by putting a
// specific bitpattern into $bitstoreal


module nan_1(clock, out1, out2);
   input clock;
   output [63:0] out1;
   output signed [63:0] out2;
   reg [63:0] out1;
   reg signed [63:0] out2;
   real      r;
   reg [63:0] bitpattern;

   always @(posedge clock)
     begin: main
	bitpattern = 64'b0111111111110000000000000000000000000000000000000000000000000000;
	r = $bitstoreal(bitpattern);
	out1 = r;
	out2 = r;
	$display ("pattern: %b, real: %f, unsigned: %b, signed: %b", bitpattern, r, out1, out2);

	bitpattern = 64'b1111111111110000000000000000000000000000000000000000000000000000;
	r = $bitstoreal(bitpattern);
	out1 = r;
	out2 = r;
	$display ("pattern: %b, real: %f, unsigned: %b, signed: %b", bitpattern, r, out1, out2);

	bitpattern = 64'b0111111111110000000000000000000000000000000000000000000000000001;
	r = $bitstoreal(bitpattern);
	out1 = r;
	out2 = r;
	$display ("pattern: %b, real: %f, unsigned: %b, signed: %b", bitpattern, r, out1, out2);

	bitpattern = 64'b1111111111110000000000000000000000000000000000000000000000000001;
	r = $bitstoreal(bitpattern);
	out1 = r;
	out2 = r;
	$display ("pattern: %b, real: %f, unsigned: %b, signed: %b", bitpattern, r, out1, out2);
	
     end
endmodule
