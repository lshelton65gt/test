// last mod: Tue Jun  6 12:24:03 2006
// filename: test/langcov/Real/bug2209_9b.v
// Description:  This test checks the correct operation of itor with dirty bits
// from vectors of different sizes, in all cases part of each vector converted
// is out of range and thus should be x.
// gold results are from carbon as aldec and nc do not handle out-of-bounds properly.


module bug2209_9b(clock, in1, in2, out1);
   input clock;
   input [15:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   real r;
   reg [31:0] i;
   reg [63:0] long_i;
   reg [127:0] long_long_i;


   always @(posedge clock)
     begin: main
	// convert a 15 bit parsel of a 32 bit number, partially out of range (on high end)
	i = {in1,in2};
	r = $itor( i[20 +:15] );
	$display("i: %b, r: %f",(i[20 +:15]), r);

	// convert a 50 bit parsel of a 64 bit number, partially out of range (on high end)
	long_i = {i,in2,in1};
	r = $itor( long_i[20 +:50] );
	$display("i: %b, r: %f",(long_i[20 +:50]), r);

	// convert a 80 bit parsel of a 128 bit number, partially out of range (on high end)
	long_long_i = {i,in2,in1,in1,i,in2};
	r = $itor( long_long_i[50 +:80] );
	$display("i: %b, r: %f",(long_long_i[50 +:80]), r);

	// convert a 15 bit parsel of a 32 bit number, partially out of range (on low end)
	i = {in1,in2};
	r = $itor( i[10 -:15] );
	$display("i: %b, r: %f",(i[10 -:15]), r);

	// convert a 50 bit parsel of a 64 bit number, partially out of range (on low end)
	long_i = {i,in2,in1};
	r = $itor( long_i[30 -:50] );
	$display("i: %b, r: %f",(long_i[30 -:50]), r);

	// convert a 80 bit parsel of a 128 bit number, partially out of range (on low end)
	long_long_i = {i,in2,in1,in1,i,in2};
	r = $itor( long_long_i[50 -:80] );
	$display("i: %b, r: %f",(long_long_i[50 -:80]), r);
	
	out1 = r;
     end
endmodule
