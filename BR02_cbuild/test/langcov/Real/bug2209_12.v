// last mod: Wed Jun 14 08:14:49 2006
// filename: test/langcov/Real/bug2209_12.v
// Description:  This test checks that we get the same real to integer conversion
// when using the same bitpattern but in one case it is from a bitvector and in
// the other we use a BVref to get the bits.


module bug2209_12(clock, in1, out1);
   input clock;
   input in1;
   output [63:0] out1;
   reg [63:0] out1;
   real      r1, r2;
   wire [95:0] vec1;
   wire [127:0] vec2;

   
   assign  vec1 = {64'b0, 31'b1111111111111111111111111111111, in1};
   assign  vec2 = {64'b0, 31'b1111111111111111111111111111111, in1, 32'b0};

   always @(posedge clock)
     begin: main
	r1 = $itor( vec1 );
	r2 = $itor(vec2[32 +: 96]); // select the same bits that were in vec1
	$display ("vec1: %x, r1: %f, r2: %f", vec1, r1, r2);
	out1 = r1;
     end
endmodule
