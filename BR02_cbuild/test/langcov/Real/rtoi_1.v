// last mod: Fri Jun 16 09:10:07 2006
// filename: test/langcov/Real/foo1.v
// Description:  This test is simple test of $rtoi


module rtoi_1(clock, in1, in2);
   input clock;
   input [31:0] in1, in2;
   real      r, r2;
   integer   i;

   always @(posedge clock)
     begin: main
	r = (in1 * 1000.0) + (in2 * 0.0001);
	i = $rtoi(r);
	r2 = $itor(i);
	$display ("in1: %d, in2: %d, r: %f, i: %b rtoi: %b, r2: %f, $itor($rtoi(r)): %f", in1, in2, r, i, $rtoi(r), r2, $itor($rtoi(r)));
     end
endmodule
