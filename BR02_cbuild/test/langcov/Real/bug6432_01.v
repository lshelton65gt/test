// last mod: Mon Jul 31 22:10:57 2006
// filename: test/langcov/Real/bug6432_01.v
// Description:  This is the original test from bug6432
// the problem reported is that an error is reported:
// bug6432_01.v:24: Alert 20389:  Expression uses reals in an illegal context.
// this is correct for the following reason:
// any use of a signed number as an operand of the power operator (**) produces a real result
// the type of a parameter or localparam is defined by the type of the RHS
// you cannot use a real in the size declaration expression of a memory
// this problem can be fixed by making MEM_SIZE an integer
// localparam integer MEM_SIZE = 2 ** ADDRESS_SIZE ;

//module carbon_mem_1rw(RWA, WE, RE, CLK, FFI, FFO);
module bug6432_01(RWA, WE, RE, CLK, FFI, FFO);
   parameter ADDRESS_SIZE = 7;
   parameter WORD_SIZE = 96;
   localparam MEM_SIZE = 2 ** ADDRESS_SIZE;

   input [ADDRESS_SIZE-1:0] RWA;
   input 		    WE, RE, CLK;
   input [WORD_SIZE-1:0]    FFI;
   output [WORD_SIZE-1:0]   FFO;

   reg [WORD_SIZE-1:0] 	    mem [MEM_SIZE-1:0];

   reg [ADDRESS_SIZE-1:0]   RWA_L;

   initial
      begin
	 // the following should fail if MEM_SIZE is not a real value
	 $display("MEM_SIZE", $rtoi(MEM_SIZE));
      end
   always @ ( posedge CLK )
     if( WE )
       mem[RWA] <= mem[RWA];
   
   always @(posedge CLK)
     if (RE) RWA_L <= RWA ;
   
   assign 		    FFO = mem[RWA_L] ;



endmodule
