// last mod: Thu Jun 15 10:37:17 2006
// filename: test/langcov/Real/real3.v
// Description:  This test checks simple arith related to mixtures of real and integers.
// currently carbon has simulation mismatches for the first value of r_source

module real3(clock, in1, in2);
   input clock;
   input [31:0] in1, in2;

   real r;
   reg [127:0] long_long_i;
   real        r_source;


   always @(posedge clock)
     begin: main
	r_source = (in1 * 1000) + (in2 * 0.00001); // note that in1 is multiplied by 1000 not 1000.0
	$display ( "Test1: in1: %x, in2: %x, r_source: %f", in1, in2, r_source);

	r_source = (in1 * 1000.0) + (in2 * 0.00001);
	$display ( "Test2: in1: %x, in2: %x, r_source: %f", in1, in2, r_source);
     end
endmodule
