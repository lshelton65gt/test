// last mod: Thu Jun 15 15:48:05 2006
// filename: test/langcov/Real/identity_1.v
// Description:  This test checks that bitstoreal/realtobits and itor/rtoi are reversable
// out1[1] is not expected to be true (except for the value zero), but at one
// time carbon was getting the signedness wrong during the $itor conversion.
// gold file from carbon (aldec and nc do not agree on $itor($rtoi(r1))
//   carbon results were selected as being correct because we expect the
//   following equivalence tests to be true
//	int1 = $rtoi(r1);
//     int1 == $itor(int1) == $itor($rtoi(r1));


module identity_1(clock, in1, in2, out1);
   input clock;
   input [31:0] in1, in2;
   output [3:0] out1;
   reg [3:0] out1;
   reg [63:0] vec;
   integer    i1, int1;
   real       r1;

   initial
      begin
	 // start off with values that are zeros
	 r1 = 0.0;
	 vec = 0;
	 i1 = 0;
      end
   always @(posedge clock)
     begin: main
	out1[3] = ($bitstoreal($realtobits(r1)) == r1);
	out1[2] = ($realtobits($bitstoreal(vec)) == vec);
	out1[1] = ($itor($rtoi(r1)) == r1);
	out1[0] = ($rtoi($itor(i1)) == i1);
	$display("in1: %d, in2: %d, r1: %f, vec: 0x%h, i1: %d", in1, in2, r1, vec, i1);
	int1 = $rtoi(r1);
	$display("r1: %f, int1: %d, $itor($rtoi(r1)): %f, $itor(int1): %f", r1, int1, $itor($rtoi(r1)), $itor(int1));
	
	r1 = in1 + (in2 * 0.00001);
	i1 = in1 + in2;
	vec = {in1, in2};
     end
endmodule
