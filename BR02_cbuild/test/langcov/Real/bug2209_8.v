// last mod: Tue May 30 11:17:47 2006
// filename: test/langcov/Real/bug2209_8.v
// Description:  This test checks the proper operation of assignments to a 64
// bit reg from the output of bitstoreal for constants.


module bug2209_8(out1);
   output [7:0] out1;
   reg [7:0] out1;

   integer ib1;
   reg [63:0] rega64, regb64;
   real      r1, rtemp;
   initial
      begin
	 out1 = 0;

	 r1 = -2001.0000002 + 1001.001;
	 ib1 = $rtoi(r1);
	 rega64 = ib1;
	 regb64 = $bitstoreal(rega64); // note assignment of real to 64 bit reg
	 $display("r1: %d, rega64: %b, regb64: %b", r1, rega64, regb64);
	 rtemp = $bitstoreal(rega64); // note assignment of real to 64 bit reg
	 regb64 = rtemp;
	 $display("r1: %d, rega64: %b, regb64: %b", r1, rega64, regb64); // should be the same as above $display
      end
   
endmodule
