// last mod: Tue Aug  1 00:01:17 2006
// filename: test/langcov/Real/bug6432_03.v
// Description:  This is a test derived from bug6432
// run this with the directive warningMsg 20389 3085 and with -enableOutputSysTasks
// this produces an internal error as of 08/01/06

module bug6432_03(RWA, WE, RE, CLK, FFI, FFO);
   parameter ADDRESS_SIZE = 7;
   parameter WORD_SIZE = 96;
   localparam MEM_SIZE = 2 ** ADDRESS_SIZE;

   input [ADDRESS_SIZE-1:0] RWA;
   input 		    WE, RE, CLK;
   input [WORD_SIZE-1:0]    FFI;
   output [WORD_SIZE-1:0]   FFO;

   reg [WORD_SIZE-1:0] 	    mem [MEM_SIZE-1:0];

   reg [ADDRESS_SIZE-1:0]   RWA_L;

   initial
      begin
	 // the following should fail if MEM_SIZE is not a real value
	 $display("MEM_SIZE", $rtoi(MEM_SIZE));
      end
   always @ ( posedge CLK )
     if( WE )
       mem[RWA] <= mem[RWA];
   
   always @(posedge CLK)
     if (RE) RWA_L <= RWA ;
   
   assign 		    FFO = mem[RWA_L] ;



endmodule
