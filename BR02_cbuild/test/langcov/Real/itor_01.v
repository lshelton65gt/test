// last mod: Wed Nov 29 10:00:39 2006
// filename: test/langcov/Real/itor_01.v
// Description:  This test checks for the result of $itor(S2) for negative
// values of S2


module itor_01(clock, in2, out1);
   input clock;
   input [1:0] in2;
   output [31:0] out1;
   reg [31:0] 	 out1;
   reg 		 signed [1:0] S2;

   always @(posedge clock)
     begin: main
	S2 = in2;		// convert input to signed
	out1 = $itor(S2);	// convert 2 bit signed value to real, then back
				// to unsinged(out1), for negative values
	$display("$itor(S2): %f, out1: %b", $itor(S2), out1);
     end
endmodule
