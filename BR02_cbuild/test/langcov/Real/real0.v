// last mod: Tue May 16 22:06:08 2006
// filename: test/langcov/Real/real0.v
// Description:  This test checks for simple integer arith results being
// assigned to a real destination.


module real0(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] 	out1;
   real 	r0, r1, r2, r3;

   always @(posedge clock)
     begin: main
	out1 = in1 + in2;
	r0 =  in1 + in2;
	r1 = out1;
	r2 = r0 + 1.0;
	r3 = r1;
	$display("in1 %d, in2 %d, r0 %f, r1 %f, r2 %f, r3 %f", in1, in2, r0, r1, r2, r3);
     end
endmodule
