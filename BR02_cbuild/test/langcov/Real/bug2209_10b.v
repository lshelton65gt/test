// last mod: Thu Jun 15 10:10:17 2006
// filename: test/langcov/Real/bug2209_10b.v
// Description:  This test checks the proper operation of bits_to_real and
// real_to_bits using partselects, but the bitpatterns used by bits_to_real are
// always generated by a call to real_to_bits (unlike the patterns in bug2209_10.v


module bug2209_10b(clock, in1, in2, in3, out1);
   input clock;
   input [31:0] in1, in2;
   input [2:0] 	in3;
   output [7:0] out1;
   reg [7:0] out1;

   real r;
   reg [127:0] long_long_i;
   real        r_source;


   always @(posedge clock)
     begin: main
	r_source = (in1 * 1000.0) + (in2 * 0.00001);
	long_long_i = {in1,$realtobits(r_source),in2};
	r = $bitstoreal( long_long_i[32 +:64] );
	$display("in1: %x, in2: %x, r_source: %f, r_ %f, %x %x %x", in1, in2, r_source, r, long_long_i, $realtobits(r_source), long_long_i[32 +:64]);

	long_long_i = long_long_i << in3;
	r = $bitstoreal( long_long_i[(32+in3) +:64] );
	$display("in1: %x, in2: %x, r_source: %f, r_ %f, %x %x %x", in1, in2, r_source, r, long_long_i, $realtobits(r_source), long_long_i[32 +:64]);
	
	r_source = -r_source;
	long_long_i = {in1,$realtobits(r_source),in2};
	r = $bitstoreal( long_long_i[32 +:64] );
	$display("in1: %x, in2: %x, r_source: %f, r_ %f, %x %x %x", in1, in2, r_source, r, long_long_i, $realtobits(r_source), long_long_i[32 +:64]);

	long_long_i = long_long_i << in3;
	r = $bitstoreal( long_long_i[(32+in3) +:64] );
	$display("in1: %x, in2: %x, r_source: %f, r_ %f, %x %x %x", in1, in2, r_source, r, long_long_i, $realtobits(r_source), long_long_i[32 +:64]);

	out1 = r;
     end
endmodule
