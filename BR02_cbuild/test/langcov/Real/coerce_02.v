// last mod: Wed Nov 29 10:23:24 2006
// filename: test/langcov/Real/coerce_02.v
// Description:  This test checks that coercion is done in proper order for
// operands of unary operators


module coerce_02(clock, in1, in2, in3, out1);
   input clock;
   input [7:0] in1, in2;
   input [2:0] in3;
   output [7:0] out1;
   reg [7:0] out1;
   reg 	     signed [2:0] S3;

   always @(posedge clock)
     begin: main
	S3 = in3;
	out1 = (~in1) ** S3;	// coercion of in1 to real should be done before
				// inversion, and then then bitwise negation of
				// a real should be an error
     end
endmodule
