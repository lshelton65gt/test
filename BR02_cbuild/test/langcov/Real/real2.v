// last mod: Wed May 31 09:13:13 2006
// filename: test/langcov/Real/real2.v
// Description:  This test checks for the proper operation of real to integer
// assignments (especially with large negative reals)


module real2(input clock, input [59:0] in_filler, output reg [31:0] out1, out2);

   real      r0, r1, r2, r3;
   reg 	     signed [63:0] signed_p, signed_n;

   always @(posedge clock)
     begin: main
	// first build some large signed numbers, with even distribution of +/-
	signed_p = {4'b0000,in_filler}; // make a positive version
	r0 = signed_p;
	r1 = signed_p[1+:61];	// partselect, but has same MSB as signed_p
	signed_n = {4'b1111,in_filler}; // make a negative version
	r2 = signed_n;
	r3 = signed_n[1+:61];	// partselect, but has same MSB as signed_n
	$display("signed_p: %d, r0: %f, r1: %f,\nsigned_n %d, r2: %f, r3: %f, r4", signed_p, r0, r1, signed_n, r2, r3);
	
	out1 = r1;
	out2 = r3;
     end
endmodule
