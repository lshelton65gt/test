// last mod: Thu Aug 31 11:19:03 2006
// filename: test/langcov/Real/bug6432_04.v
// Description:  This is a test derived from bug6432
// run this with the directive warningMsg 20389
// this checks to see that by ignoring the message 20389, we still get the
// correct results

module bug6432_04(RWA, WE, RE, CLK, FFI, FFO);
   parameter ADDRESS_SIZE = 7;
   parameter WORD_SIZE = 96;
   localparam MEM_SIZE = 2 ** ADDRESS_SIZE;

   input [ADDRESS_SIZE-1:0] RWA;
   input 		    WE, RE, CLK;
   input [WORD_SIZE-1:0]    FFI;
   output [WORD_SIZE-1:0]   FFO;

   reg [WORD_SIZE-1:0] 	    mem [MEM_SIZE-1:0];

   reg [ADDRESS_SIZE-1:0]   RWA_L;
   integer 		    i;

   initial
      begin
	 for (i = 0; i < MEM_SIZE; i = i + 1)
	   mem[i] = i;
      end
   
   always @ ( posedge CLK )
     if( WE )
       mem[RWA] <= mem[RWA];
   
   always @(posedge CLK)
     if (RE) RWA_L <= RWA ;
   
   assign   FFO = mem[RWA_L] ;



endmodule
