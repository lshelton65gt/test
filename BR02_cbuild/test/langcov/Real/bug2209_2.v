// last mod: Fri May 19 09:56:03 2006
// filename: test/langcov/Real/bug2209_2.v
// Description:  This test is a simple use of $realtobits and $bitstoreal


module bug2209_2(input clock, input [7:0] in1, in2, output [63:0] out1);

   real r0, r1;
   reg [63:0] w1;

   always @(in1 or in2)
     begin
	r0 = in2;
	r0 = r0 * 0.0001;
	r1 = in1;
	r0 = r0 + r1;
	w1 = $realtobits(r0);
     end
   
   mid i1(clock, $realtobits(r1*0.001), w1, out1);

endmodule

module mid(input clock, input [63:0] v1, v2, output reg [63:0]out1);
   real r1, r2;


   always @(posedge clock)
     begin: main
	r1 = $bitstoreal(v1);
	r2 = $bitstoreal(v2);
	$display ( "v1 %b, b2 %b, r1 %f, r2 %f", v1, v2, r1, r2);
	out1 = r2 + r2;
     end
endmodule
