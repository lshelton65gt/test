// last mod: Tue May 30 15:50:34 2006
// filename: test/langcov/Real/but3329_1.v
// Description:  This test shows the problem from bug 3329, that of the display
// of rounding information for real numbers.


module but3329_1( out1);
   output [7:0] out1;
   reg [7:0] out1;

   real      r1;
   integer   i1;
   
   initial
      begin
   	 r1 = 3.4; i1 = 3.4;
	 $display("real: %f integer %d", r1, i1);
	 r1 = -3.5; i1 = -3.5;
	 $display("real: %f integer %d", r1, i1);
	 out1 = 0;
      end

endmodule

