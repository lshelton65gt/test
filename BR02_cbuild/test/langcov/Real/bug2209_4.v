// last mod: Tue May 23 11:12:05 2006
// filename: test/langcov/Real/bug2209_4.v
// Description:  This test checks the interaction of $realtobits and bitstoreal
// with a verilog task


module bug2209_4(in1, in2, sel, clk, out1);
   input [7:0] in1, in2;
   input       sel, clk;
   output [63:0] out1;
   reg [63:0] out1;
   real r1;

   task b2209_t;
      output  [63:0] out2;
      input [63:0] i1;
      input [7:0] i2;
      input sel;
      real rt;
      real rt2;
      begin
	 if (sel)
	    begin
	       rt2 = i2;
	       rt = $bitstoreal(i1);
	       rt = rt + rt2;
	       out2 = $realtobits(rt);
	    end
	 else
	    begin
	       out2 = i1;
	    end
      end
   endtask

   initial
     out1 = 64'b0101000000000000000000000000000000000000000000000000000000001010;
   
   always @(posedge clk)
      begin
	 r1 = in1;
	 r1 = (r1 * 0.0001);
	 b2209_t(out1, $realtobits(r1), in2, sel);
      end
endmodule
