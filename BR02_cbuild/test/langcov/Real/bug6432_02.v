// last mod: Tue Aug  1 00:00:31 2006
// filename: test/langcov/Real/bug6432_02.v
// Description:  This test was discovered while working bug6432_02.
// for some reason it hits a segfault in cheetah


module bug6432_02(clock, in1, in2, out1);
   defparam u1.bar1 = (2 ** 3);  // note this incorrectly is a ref to bar1 (which is a module)
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   bar1 u1(clock,in1,in2,out1);
endmodule

module bar1(clock, in1, in2, out1);
   defparam bar2 = 0;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule
