module top (in,out);
   parameter asize=2;
   parameter msize=4;
   parameter mcnt=128;

   input  [msize-1:0] in;
   output [msize-1:0] out;
   reg    [msize-1:0] out;

   reg [msize-1:0]    memory [mcnt-1:0];

   integer 	      i;
   
   always @ ( in )
     begin
	// push input on top; get output from bottom
	out <= memory[0];
	memory[mcnt-1] <= in;
	for ( i = 0 ; i < (mcnt-1) ; i = i + 1 )
	  begin
	     memory[i] <= memory[i+1];
	  end
     end
   
endmodule // top
