module top (out, clk, in);
   output [3:0] out;
   input 	clk;
   input [3:0] 	in;

   wire [7:0] 	tmp;
   assign 	tmp[3:0] = ~in[3:0];

   reg [3:0] 	out;
   always @ (posedge clk)
     out <= tmp[3:0] & tmp[7:5];

endmodule // top

   
