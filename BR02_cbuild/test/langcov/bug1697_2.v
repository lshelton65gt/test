// last mod: Tue May 24 11:53:33 2005
// filename: test/langcov/bug1697_2.v
// simulation gold file created with nc
// from bug 1697, in verilog the $setuphold calls in the specify block
// create an implicit connection between events and the delay signals
// (LRM 15.5.2)
// carbon should create an assignment for each of these
// in this case the clock to clk*_d is done with a bitselect,
// the in* to in*_d is done with a whole vector, a partselect and a bitselect


module bug1697_2(clk, in, out1, out2, out3);
   input  [2:0] clk, in;
   output [2:0] out3;
   output [1:0] out2;
   output       out1;

   wire 	clk0_d, clk1_d, clk2_d;
   wire   [2:0] in3_d;
   wire   [1:0] in2_d;
   wire         in1_d;
   reg    [2:0] out3;
   reg    [1:0] out2;
   reg          out1;


   always @(posedge clk0_d)
     out1 <= in1_d;

   always @(posedge clk1_d)
     out2 <= in2_d;

   always @(posedge clk2_d)
     out3 <= in3_d;

   // the following specify block causes the implicit creation of the following
   // connections:
   //  clk0_d = clk[0];
   //  clk1_d = clk[1];
   //  clk2_d = clk[2];

   //  in1_d = in[2];    // 1 bit by bitselect
   //  in2_d = in[2:1];  // 2 bits by partselect
   //  in3_d = in;       // all 3 bits

   specify
      $setuphold(posedge clk[0], posedge in, 1, 1, , , , clk0_d, in3_d);
      $setuphold(posedge clk[0], negedge in, 1, 1, , , , clk0_d, in3_d);

      $setuphold(posedge clk[1], posedge in[2:1], 1, 1, , , , clk1_d, in2_d);
      $setuphold(posedge clk[1], negedge in[2:1], 1, 1, , , , clk1_d, in2_d);

      $setuphold(posedge clk[2], posedge in[2], 1, 1, , , , clk2_d, in1_d);
      $setuphold(posedge clk[2], negedge in[2], 1, 1, , , , clk2_d, in1_d);
   endspecify
   
endmodule
