// This testcase verifies that we can do reset analysis on the bdd
// despite the fact that verilog sizes both 'reset' and '0' as 32-bits

module wreset(in, out, clk, reset);
  input in, clk, reset;
  output out;
  reg    out;

  always @(posedge clk or negedge reset) begin
    if (reset == 0)
      out <= 0;
    else
      out <= in;
  end
endmodule
