// last mod: Mon Jun 13 10:42:33 2005
// filename: test/langcov/bug4544.v
// Description:  This test at one time would get into an infinite loop in cheetah


 module top(out, i0, i1, sel);
    output [1:0] out;
    input  [1:0] i0, i1, sel;

    mux mux1(out[0], i0[0], i1[0], sel[0]);
    mux mux2(out[1], i0[1], i1[1], sel[1]);
  endmodule

  module mux(out, i0, i1, sel);
    output out;
    input  i0, i1, sel;
    reg    out;

    always @(i0 or i1 or sel) begin
      functions.mux(out, i0, i1);
    end

    functions functions();
  endmodule

  module functions;
    task mux;
      output out;
      input i0, i1;
      begin: muxscope
        reg t1, t2;
        subfuncs.and2(t1, i0, ~mux.sel);
        subfuncs.and2(t2, i1, mux.sel);
        subfuncs.or2(out, t1, t2);
      end
    endtask

    subfuncs subfuncs();
  endmodule

  module subfuncs;
    task and2;
      output out;
      input i0, i1;
      out = i0 & i1;
    endtask
    task or2;
      output out;
      input i0, i1;
      out = i0 | i1;
    endtask
  endmodule

