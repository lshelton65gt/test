/* Test named blocks. */
module top(a, b, c, d, e, sel, o);
  input a, b, c, d, e, sel;
  output o;
  reg r;

  always
  begin : named1
    r = a + b;
    begin : named2
      if (sel)
      begin : named3
        r = c;
      end
      else
      begin : named4
        reg z;
        z = d + e;
        r = z;
      end
     end
  end

  assign o = r;

endmodule
