// Test undriven nets
module top(o1, o2, o3, o4, o5, o6, io);
  output o1, o2, o3, o4, o5;
  output [3:0] o6;
  inout io;
  tri0 n;
  assign o2 = o1;
  assign o4 = n;
  assign o6 = 4'bzzzz;
  foo inst1 (o5);
endmodule

module foo (o);
  output o;
  pullup (o);
endmodule
