module dut(clk, in, out);
  input clk;
  input [7:0] in;
  output  [3:0] out;

  reg [3:0]     out;

  always @(posedge clk) begin
    out = getNibble(in);
  end

  function [3:0] getNibble (input [7:0] in);
    reg myReg [3:0];
    begin
      myReg[0] = in[0] & in[1];
      myReg[1] = in[2] ^ in[3];
      myReg[2] = in[4] & in[5];
      myReg[3] = in[6] | in[7];
      getNibble = {myReg[3], myReg[2], myReg[1], myReg[0]};
    end
  endfunction
endmodule
