// last mod: Wed Feb 15 09:14:04 2006
// filename: test/langcov/Ranges/range_1.v
// Description:  This test is an attempt to stress the range generation code, it
// does arithmetic in 126 bit vectors to address in1 that has a range that must
// be normalized.


module range_1(clock, in1, in2, in3, out1, out2);
   input clock;
   input [0:-7] in1;
   input       in2,in3;
   output [7:0] out1, out2;
   reg [7:0] out1, out2;

   reg [125:0] long1, long2, long3;

   always @(posedge clock)
     begin: main
	// the following 2 lines put a difference of 2 between long1/long2 up in bits 102 or so
	long1 = {{23{in3}},3'b010,{100{in2}}};
	long2 = {{23{in3}},3'b000,{100{in2}}};
	long3 = {126{in3}};
	out1 = in1[((long2-long1)>>100)&3'b111&long3]; // index is (in3 ? -2 : 0)
	out2 = in1[(((long2-long1)>>100)&3'b111&long3)-:2]; // partsel index is ((in3 ? -2 : 0) -:2)
     end
endmodule
