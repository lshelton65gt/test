// Test nested function non-local references and rewriting.
module top(i1, i2, i3, o1, o2);
   input [3:0] i1;
   input [3:0] i2;
   input [3:0] i3;
   output [3:0] o1;
   output [3:0] o2;

   reg [3:0] 	o1;
   reg [3:0] 	o2;

   reg [3:0] 	r1;
   reg [3:0] 	r2;

   function [3:0] f2;
      input a;
      begin
	 f2 = i1 + i2;
      end
   endfunction

   function [3:0] f1;
      input a;
      begin
	 f1 = f2(a);
      end
   endfunction

   function [3:0] f3;
      input a;
      begin
	 f3 = r1 + r2;
      end
   endfunction // f3

   function [3:0] f4;
      input a;
      begin
	 f4 = i1;
      end
   endfunction // f4
   

   always @(i1 or i2)
     begin
	r1 = i3;
	if ((f2(1'b0) == 0) || (f1(1'b0) == 0))
	  begin
	     r2 = i1;
	     o1 = f3(1'b0);
	  end
	else
	  begin
	     r2 = i2;
	     o1 = f3(1'b0);
	  end
     end // always @ (i1 or i2)

   always @(i1 or i2)
     begin : n
	integer i;
	o2 = 'b0;
	for (i = 0; i < f4(1'b0); i = i + f4(1'b1))
	  begin
	     o2[i] = i3[i];
	  end
     end

endmodule // top
