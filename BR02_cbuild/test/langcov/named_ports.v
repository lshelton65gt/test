/* Test named port connections. */
module top(o1, o2, select, i1, i2, i3, i4);
input [0:31] i1, i2, i3, i4;
output [0:31] o1, o2;
input select;
bar inst1 (.z(o1), .a(i1), .sel(select), .b(i2));
bar inst2 (select, i3, i4, o2);
endmodule



module bar (sel, a, b, z);
output [0:31] z;
input [0:31] a, b;
input sel;
assign z = sel ? a : b;
endmodule
