module arrays(in, out, a1, a2, a3, a4);
  input in;
  output out;
  input [7:0] a1, a2, a3, a4;

  // declares a memory mema of 256 8-bit registers. The indices are 0 to 255
  reg [7:0] mema[0:255]; 

  wire [7:0] w_array[0:5]; // declare array of wires

  always @(in or a1)
    mema[a1] = in;
  assign     w_array[2] = mema[5];
  wire [7:0] row = w_array[a4];
  assign     out = in | row[0];
endmodule

