// A test for part select of a word in the 3 dimensional
// memory. One of the dimension index is greater than 32
// bits, which cbuild should truncate to 32 bits, and
// issue a warning. This tests both rval and lval part
// selects.
module bug(clk, o, i, en, a, b);
   input clk;
   output [5:0] o;
   input [5:0]  i;
   input 	 en;
   input 	 a;
   input [63:0]  b;

   reg [6:0] foo[1:0][63:0];
   reg [5:0] o;
   
   always @(posedge clk)
     if (en)
       foo[a][b][5:0] <= i;
     else
       o <= foo[a][b][5:0];
endmodule

