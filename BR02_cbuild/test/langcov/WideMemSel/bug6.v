// A test for 2 dimensional memory where indices for one of
// the dimensions in rval and lval are greater than 32 bits.
// The index itself comes from a memory with words larger
// than 32 bits. The select of this memory is also larger than
// 32 bits. The cbuild truncates the indices to 32 bits,
// and issues a warning.
module bug(clk, o, i, en, a, b, c);
  input clk;
  output [5:0] o;
  input [5:0]  i;
  input  en;
  input  a;
  input [63:0] b;
  input [63:0] c;

  reg [7:0]    foo[63:0];
  reg [63:0]   sel[1:0][3:0];
  reg [5:0]    o;
   
  always @(posedge clk)
    if (en)
      foo[sel[a][c]] <= i;
    else
      o <= foo[sel[a][c]];
endmodule

