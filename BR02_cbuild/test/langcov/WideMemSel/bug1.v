// A test for 3 dimensional memory where indices for one of
// the dimensions in rval and lval are greater than 32 bits.
// The cbuild truncates the index to 32 bits, and issues a
// warning.
module bug(clk, o, i, en, a, b);
   input clk;
   output [5:0] o;
   input [5:0]  i;
   input 	 en;
   input 	 a;
   input [63:0]  b;

   reg [5:0] foo[1:0][63:0];
   reg [5:0] o;
   
   always @(posedge clk)
     if (en)
       foo[a][b] <= i;
     else
       o <= foo[a][b];
endmodule

