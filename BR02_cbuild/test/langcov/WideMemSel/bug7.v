// A test to check that we generate the right error
// message for memories with more than 3 dimensions
// for verilog.
module bug(clk, o, i, en, a, b, c);
  input clk;
  output [5:0] o;
  input [5:0]  i;
  input  en;
  input  a;
  input [63:0] b;
  input [3:0] c;

  reg [5:0]    foo[1:0][63:0][3:0];
  reg [5:0]    o;
   
  always @(posedge clk)
    if (en)
      foo[a][b][c] <= i;
    else
      o <= foo[a][b][c];
endmodule

