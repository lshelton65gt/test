// last mod: Mon Nov  7 08:51:24 2005
// filename: test/langcov/defparam3.v
// Description:  This test is a simple demonstration of the problem seen in bug
// 5276.  The values of level*_dat should all come from defparams not from
// instance or default values.  The gold file verified with aldec.


module defparam3(clock, dat, out1, out2, out3);
   input clock;
   input [7:0] dat;
   output [7:0] out1, out2, out3;

   defparam     u1.i1.b1.level4_dat = "defparam1";
   defparam     u1.i2.b1.level4_dat = "defparam2";
   defparam     u1.i3.b1.level4_dat = "defparam3";

   defparam     u1.i1.level3_dat = "defparam1";
   defparam     u1.i2.level3_dat = "defparam2";
   defparam     u1.i3.level3_dat = "defparam3";

   level2  u1(clock, dat, out1, out2, out3); // without this intermediate
				// hierarchy level, the correct values for
				// level*_dat are set by the above defparams

endmodule

module level2(clock, dat, out1, out2, out3);
   input clock;
   input [7:0] dat;
   output [7:0] out1, out2, out3;

   level3 #("instance1") i1(clock, dat, out1);
   level3 #("instance2") i2(clock, dat, out2);
   level3 #("instance3") i3(clock, dat, out3);

endmodule

module level3(clock, dat, out1);
   input clock;
   input [7:0] dat;
   output [7:0] out1;

   parameter [135:0] level3_dat = "defaultL3";
   
   initial $display ("%m level3_dat is: %s", level3_dat);

   level4 #("instance4") b1( clock, dat, out1);

endmodule

module level4(clock, dat, out1);
   input clock;
   input [7:0] dat;
   output [7:0] out1;
   reg [7:0] out1;

   parameter [135:0] level4_dat = "defaultL4";

   initial
     begin
	$display("%m level4_dat is: %s", level4_dat);
     end
   
   always @(posedge clock)
     begin
	out1 = dat;
     end
endmodule
