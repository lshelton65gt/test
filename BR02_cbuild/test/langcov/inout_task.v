// Test inouts with tasks
module top(clk, i, o, io);
   input clk;
   input i;
   output o;
   output  io;
   
   reg 	  io;
   reg 	  o;

task inout_ex;
input i;
inout io;
output o;
begin
  io <= i;
  o <= i;
end
endtask

always @(posedge clk)
begin
  inout_ex(i, io, o);
end

endmodule
