// Test name clashing in function rewrite
module top(i1, i2, o1);
   input i1;
   input i2;
   output o1;

   function g;
      input a;
      input b;
      begin
	 g = a | b | i2;
      end
   endfunction // g

   function f;
      input a;
      input b;
      reg i2;
      begin
	 i2 = b;
	 f = g(a, i2);
      end
   endfunction // f

   assign o1 = f(i1, 1'b0);

endmodule // top
