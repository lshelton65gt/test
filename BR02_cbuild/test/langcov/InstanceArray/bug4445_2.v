// last mod: Tue Aug  2 13:09:18 2005
// filename: test/langcov/InstanceArray/bug4445_1.v
// Description:  This test is from bug 4445, currently arrays of UDPs are not
// supported so this will generate a population error


module bug4445_2(clk, rst, in1, in2, in3, out1, out2, out3);
   input clk;
   input rst;
   input [7:0] in1;
   input [7:0] in2;
   input [7:0] in3;
   output [7:0] out1;
   output [7:0] out2;
   output [7:0] out3;

   UDP_DFFRP dff_u1[1:8] (out1, ~rst, in1, clk, 1'b0);
   UDP_DFFRP dff_u2[-5:2] (out2, ~rst, in2, clk, 1'b0);
   UDP_DFFRP dff_u3[-12:-19] (out3, ~rst, in3, clk, 1'b0);
   
endmodule

`ifdef CARBON_WORKAROUND
module UDP_DFFRP(Q,RB,D,CK,VIOL);
`else  // ifdef CARBON_WORKAROUND
primitive UDP_DFFRP(Q,RB,D,CK,VIOL);
`endif // ifdef CARBON_WORKAROUND
output Q;
reg Q;
input RB;
input D;
input CK;
input VIOL;
`ifdef CARBON_WORKAROUND
   always @(posedge CK or negedge RB)
     if (RB == 1'b0)
       Q <= 1'b0;
     else
       Q <= D;
endmodule
`else  // ifdef CARBON_WORKAROUND
table
//RB D CK  VIOL  Q
0    ? ?    ? :?:0;
1    0 r    ? :?:0;
1    1 r    ? :?:1;
?    0 (bx) ? :0:-;
1    1 (bx) ? :1:-;
?    ? (?0) ? :?:-;
1    1 (x1) ? :1:-;
1    0 (x1) ? :0:-;
x    0 r    ? :?:0;
(?x) ? ?    ? :1:x;
(?x) 1 x    ? :0:x;
(?x) x x    ? :0:x;
(?x) 0 x    ? :0:-;
(?x) ? b    ? :0:-;
?    * b    ? :?:-;
(?1) ? b    ? :?:-;
(?1) 0 ?    ? :?:-;
endtable
endprimitive 
`endif // ifdef CARBON_WORKAROUND
