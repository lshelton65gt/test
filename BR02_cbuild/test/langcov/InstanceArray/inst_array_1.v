// last mod: Wed May  4 16:18:57 2005
// filename: test/langcov/InstanceArray/inst_array_1.v
// Description:  This test contains an array of instances with formal
// ports of 4,2,1 bits wide.
// actual ports are full vectors and concats of the appropriate sizes


module inst_array_1(in1, in2, in8, out16, out8_A, out8_B);
   input in1;
   input [1:0] in2;
   input [7:0] in8;
   output [15:0] out16;
   output [7:0] out8_A, out8_B;

   submod instA[3:0] (.out_w4(out16), .in_w2(in8), .in_w1(in1));

   submod instB[3:0] (.out_w4({out8_A,out8_B}), .in_w2({in2,in2,in2,in2}), .in_w1(in1));

endmodule



module submod(out_w4, in_w2, in_w1);
   output [3:0] out_w4;
   input [1:0] 	in_w2;
   input 	in_w1;
   
   initial 
     $display("%m");

   assign 	 out_w4 = in_w2 << in_w1 ; // out_w4[3] is always 0
endmodule
