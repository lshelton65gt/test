// last mod: Fri May  6 10:07:24 2005
// filename: test/langcov/InstanceArray/bug4427_dut.v
// Description:  This test is 'dut.v' from bug 4427, it segfaulted at one time
// because of the way that connections were made to the instances.

module bug4427_dut(i,o,e);
   input [19:16] i;
   output [-2:-5] o;
   input e;

   pad p[1000:1003] (.i(i),.o(o));
   pad q[7:4] (.i(e),.o({p[1000].ena,
			 p[1001].ena,
			 p[1002].ena,
			 p[1003].ena}));
endmodule

module pad(i,o);
   input i;
   output o;
   wire ena;
   assign o = (ena) ? i : 1'b0;
endmodule
