// last mod: Tue May 17 10:44:01 2005
// filename: test/langcov/InstanceArray/inst_array_12.v
// Description:  This test is from EMC, see bug4430 for the passing case, see
// inst_array_11.v for a similar test

module inst_array_11;
   wire [31:0] a; // carbon observeSignal
                  // carbon depositSignal
   wire [31:0] b;

   submod submod1[3:0] (
		       .a (a[15:0])
		       );
endmodule

module submod (a);
   inout [3:0] a;
endmodule
