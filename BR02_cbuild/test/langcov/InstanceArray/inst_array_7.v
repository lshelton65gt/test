// last mod: Fri May  6 14:59:20 2005
// filename: test/langcov/InstanceArray/inst_array_7.v
// Description:  This test from mikeH, uses large negative numbers for the
// instance array range, and uses non-zero based ranges for the ports of top
// module, it is run with the directives depositSignal dut1.p[-1001].ena observeSignal dut1.p[-1002].i
// this testcase will not simulate with aldec, NC results are in inst_array_7.sim.log.ncgold


module inst_array_7(i,o,e);
   input [19:16] i;
   output [-2:-5] o;
   input e;

   pad p[-1000:-1003] (i,o);
endmodule

module pad(i,o);
   input i;
   output o;
   wire ena;
   assign o = (ena) ? i : 1'b0;
endmodule

