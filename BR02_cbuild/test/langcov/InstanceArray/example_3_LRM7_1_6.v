// last mod: Thu May  5 17:40:19 2005
// filename: test/langcov/InstanceArray/example_3_LRM7_1_6.v
// Description:  This test was inspired by example 3 from the verilog LRM
// section 7.1.6, here the portsizes do not match the actual argument widths, so
// there are partselects used in the connections


module example_3_LRM7_1_6 (busin, bushigh, buslow, enh, enl);
   input [15:0] busin;
   output [7:0] bushigh, buslow;
   input 	enh, enl;

`ifdef WORKAROUND   
   driver busar3 (busin[15:12], bushigh[7:4], enh);
   driver busar2 (busin[11:8], bushigh[3:0], enh);
   driver busar1 (busin[7:4], buslow[7:4], enl);
   driver busar0 (busin[3:0], buslow[3:0], enl);
`else
   driver busar[3:0] (.out({bushigh, buslow}), .in(busin),
		      .en({enh, enh, enl, enl}));
`endif

endmodule

module driver(in, out, en);
   input  [3:0] in;
   output [3:0] out;
   input 	en;

   assign       out = (en ? in : 4'b0); // use zero (instead of z) so aldec will
                                        // match carbon default
endmodule
