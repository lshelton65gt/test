module dut(i,o,e);
   input [4:0] i;
   output [4:0] o;
   input e;

   assign p[-3].ena = e;
   assign p[-2].ena = e;
   assign p[-1].ena = e;
   assign p[0].ena = e;
   assign p[1].ena = e;

   pad p[-3:1] (i,o);
endmodule

module pad(i,o);
   input i;
   output o;
   wire ena;
   assign o = (ena) ? i : 1'b0;
endmodule
