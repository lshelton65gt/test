// last mod: Wed May  4 22:07:54 2005
// filename: test/langcov/InstanceArray/example_1_LRM7_1_6.v
// Description:  This test was inspired by example 1 from LRM section 7.1.6


module example_1_LRM7_1_6(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   nand nand_array[1:8](out1, in1, in2);
   
endmodule
