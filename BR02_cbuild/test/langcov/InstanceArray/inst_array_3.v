// last mod: Wed May  4 16:47:36 2005
// filename: test/langcov/InstanceArray/inst_array_3.v
// Description:  This test contains an array of instances with formal
// ports of 32,2,1 bits wide.
// actual ports are full vector, partselect, and a bitselect


module inst_array_3(in1, out33, in35A, in3B);
   input in1;
   input [34:0] in35A;
   input [2:0]  in3B;
   output [32:0] out33;

   submod instA[3:0] (.out_w33(out33), .in_w32(in35A[32:1]), .in_w1(in3B[in1]));

endmodule



module submod(out_w33, in_w32, in_w1);
   output [32:0] out_w33;
   input [31:0] in_w32;
   input 	in_w1;

   initial 
     $display("%m");

   assign 	 out_w33 = in_w32 << in_w1 ;
endmodule
