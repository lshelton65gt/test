// last mod: Thu May  5 17:03:28 2005
// filename: test/langcov/InstanceArray/inst_array_6.v
// Description:  This test contains an array of instances with formal
// ports of 32,2,1 bits wide.
// actual ports are full vectors, and an expression that needs to be properly sized


module inst_array_6(in1, out32, in32A, in3B);
   input in1;
   input [31:0] in32A;
   input [2:0]  in3B;
   output [31:0] out32;

   // note in the following the arg for .in_w8 is the result of a sum between a
   // 32 bit value and a 3 bit value (this expression must be sized to 32 bits
   // for this test to work)
   submod instA[3:0] (.out_w8(out32), .in_w8(in32A+in3B), .in_w1(in1));

endmodule



module submod(out_w8, in_w8, in_w1);
   output [7:0] out_w8;
   input [7:0] in_w8;
   input 	in_w1;

   initial 
     $display("%m");

   // the following assign is 8 bits shifted by a value of up to 1
   assign 	 out_w8 = in_w8 << in_w1 ;
endmodule
