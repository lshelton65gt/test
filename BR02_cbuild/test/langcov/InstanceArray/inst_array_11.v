// last mod: Tue May 17 11:58:53 2005
// filename: test/langcov/InstanceArray/inst_array_11.v
// Description:  This test is from EMC, see bug4430 for the passing case, see
// inst_array_12.v for a similar test (output verified with nc)

module inst_array_11;
   wire [31:0] a;
   wire [31:0] b;

   submod submod1[3:0] (
		       .a (a[15:0])
		       );
endmodule

module submod (a);
   inout [3:0] a; // carbon observeSignal
                   // carbon depositSignal
endmodule
