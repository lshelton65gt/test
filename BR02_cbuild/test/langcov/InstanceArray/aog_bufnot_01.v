// last mod: Wed Jul 25 07:30:48 2007
// filename: test/langcov/InstanceArray/aog_bufnot_01.v
// Description:  This test checks for the proper handling of the buf and not
// logic gates used as an array of gates, and with multiple outputs.

// inspired by bug 7415

module aog_bufnot_01(i1, i3, i12, e1, i4, e4,
		     o1_a, o1_b, o1_c, o1_d, o1_e, o1_f,
		     o1_na, o1_nb, o1_nc, o1_nd, o1_ne, o1_nf,
		     o4_a, o4_b, o4_c, o4_d, o4_e, o4_f , o4_g, o4_h, o4_i, o4_j,
		     o4_na, o4_nb, o4_nc, o4_nd, o4_ne, o4_nf, o4_ng, o4_nh, o4_ni, o4_nj);
   input i1, e1;
   input [3:0] i4, e4;
   input [2:0] i3;
   input [11:0] i12;
   output      o1_a, o1_b, o1_c, o1_d, o1_e, o1_f,
	       o1_na, o1_nb, o1_nc, o1_nd, o1_ne, o1_nf ;
   output [3:0] o4_a, o4_b, o4_c, o4_d, o4_e, o4_f, o4_g, o4_h, o4_i, o4_j;
   output [3:0] o4_na, o4_nb, o4_nc, o4_nd, o4_ne, o4_nf, o4_ng, o4_nh, o4_ni, o4_nj;

   
   buf b1       (o1_a, o1_b, o1_c, i1);	// 3 outputs each 1 bit, data is 1 bit
   buf b2       (o1_d, o1_e, o1_f, i3);	// 3 outputs each 1 bit, data is 3 bit
   buf b13[3:0] (o4_a, o4_b, o4_c, i1);	// 3 outputs each 4 bit, data is 1 bit
   buf b15[3:0] (o4_d, o4_e, o4_f, i4);	// 3 outputs each 4 bit, data is 4 bit
//   buf b16[3:0] (o4_g, o4_h, o4_i, i12);	// 3 outputs each 4 bit, data is 12 bit (invalid verilog)
//   buf b16[3:0] (o4_g, o4_h, o4_i, i3);	// 3 outputs each 4 bit, data is 3 bit (invalid verilog)
   buf b16[3:0] (o4_g, o4_h, o4_i, o4_j, i4);	// 4 outputs each 4 bit, data is 4 bit
   
   not c1       (o1_na, o1_nb, o1_nc, i1);	// 3 outputs each 1 bit, data is 1 bit
   not c2       (o1_nd, o1_ne, o1_nf, i3);	// 3 outputs each 1 bit, data is 3 bit
   not c13[3:0] (o4_na, o4_nb, o4_nc, i1);	// 3 outputs each 4 bit, data is 1 bit
   not c15[3:0] (o4_nd, o4_ne, o4_nf, i4);	// 3 outputs each 4 bit, data is 4 bit
//   not c16[3:0] (o4_ng, o4_nh, o4_ni, i12);	// 3 outputs each 4 bit, data is 12 bit (invalid verilog)
//   not c16[3:0] (o4_ng, o4_nh, o4_ni, i3);	// 3 outputs each 4 bit, data is 3 bit (invalid verilog)
   buf c16[3:0] (o4_ng, o4_nh, o4_ni, o4_nj, i4);// 4 outputs each 4 bit, data is 4 bit
   
endmodule
