// last mod: Wed May  4 17:14:06 2005
// filename: test/langcov/InstanceArray/example_good_LRM7_1_5.v
// Description:  This test inspired by section 7.1.5, checks for a valid error messages


module example_good_LRM7_1_5(in1, in2, out1);
   input [3:0] in1, in2;
   output [3:0] out1;

       // the following should not generate an error msg
   nand #2 inand_good1[3:0] (out1, in1, in2), inand_good2[7:4] (out1, in1, in2); // uses different name

endmodule
