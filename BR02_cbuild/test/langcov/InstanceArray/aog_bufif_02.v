// last mod: Tue Jul 24 09:17:45 2007
// filename: test/langcov/InstanceArray/aog_bufif_02.v
// Description:  This test checks for the proper handling of bufif1/bufif0 in an
// array of gates situation.  In this test the outputs are a single bit wide,
// but each is used in an array of 4 instances.  In the cases where all args of
// the gate are 1 bit wide we expect non-x values.

// In the case where any of the second or third args to these bufif* gates is wider than 1 bit the
// output is undefined because the implementation will write to the same single output bit
// multiple times.  Gold files were created by running test with simulator and then manually
// inspecting the output bits that were driven multiple times.
// inspired by bug 7415

module aog_bufif_02(i1, e1, i4, e4, o1_a, o1_b, o1_c, o1_d, o1_e, o1_f, o1_g, o1_h);
   input i1, e1;
   input [3:0] i4, e4;
   output o1_a, o1_b, o1_c, o1_d, o1_e, o1_f, o1_g, o1_h;

  bufif0 b1[3:0] (o1_a, i1, e1);        // out is 1 bit, data is 1 bit, enable is 1 bit
  bufif0 b2[3:0] (o1_b, i1, e4);	// out is 1 bit, data is 1 bit, enable is 4 bit
  bufif0 b3[3:0] (o1_c, i4, e4);	// out is 1 bit, data is 4 bit, enable is 4 bit
  bufif0 b4[3:0] (o1_d, i4, e1);	// out is 1 bit, data is 4 bit, enable is 1 bit
   	 	       
  bufif1 b5[3:0] (o1_e, i1, e1);        // out is 1 bit, data is 1 bit, enable is 1 bit
  bufif1 b6[3:0] (o1_f, i1, e4);	// out is 1 bit, data is 1 bit, enable is 4 bit
  bufif1 b7[3:0] (o1_g, i4, e4);	// out is 1 bit, data is 4 bit, enable is 4 bit
  bufif1 b8[3:0] (o1_h, i4, e1);	// out is 1 bit, data is 4 bit, enable is 1 bit
   
   

endmodule
