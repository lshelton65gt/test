// last mod: Wed Apr 30 13:51:59 2008
// filename: test/langcov/InstanceArray/bug7547_02.v
// 
// Description:  This test was inspired by work done to duplicate a problem
// noticed in bug 7415 (where the width of an arg to an instance array was the
// wrong size). (bug7415_01.v)

// this is a contrived testcase, and is an attempt to make it self checking.
// The expected output:
// out1_A == out1_B == out1_C
// out2_A == out2_B == out2_C
// out3_A == out3_B == out3_C

module bug7547_02(out1_A, out2_A, out3_A, out1_B, out2_B, out3_B, out1_C, out2_C, out3_C, in, enable1, genable);
parameter DATAWIDTH = 3;
   inout [DATAWIDTH:1] out1_A, out1_B, out1_C;
   output              out2_A, out2_B, out2_C;
   output              out3_A, out3_B, out3_C;
   input [DATAWIDTH:1] in;
   input [DATAWIDTH:1] enable1;
   input genable;
   wire [DATAWIDTH:1] not_in, not_enable1;

   sub #(DATAWIDTH) i1(out1_A, out2_A, in, enable1, genable);
   sub #(DATAWIDTH) i2(out1_A, out3_A, ~in, ~enable1, genable);

   // the above two instances are equivalent to the following assigns and bufif1 instances
   // so we expect out1_A == out1_B
   assign out2_B = out1_B[1];
   bufif1(out1_B[3],in[3],(enable1[3]&genable));
   bufif1(out1_B[2],in[2],(enable1[2]&genable));
   bufif1(out1_B[1],in[1],(enable1[1]&genable));

   assign out3_B = out1_B[1];
   assign not_in = ~in;
   assign not_enable1 = ~enable1;
   bufif1(out1_B[3],not_in[3],(not_enable1[3]&genable));
   bufif1(out1_B[2],not_in[2],(not_enable1[2]&genable));
   bufif1(out1_B[1],not_in[1],(not_enable1[1]&genable));

   // they also should also be equivalent to the following
   // we expect out1_A == out1_B == out1_C
   assign out2_C = out1_C;
   assign out3_C = out1_C;
   bufif1 uC1[2:0](out1_C,in,(enable1&{3{genable}}));
   bufif1 uC2[2:0](out1_C,(~in),((~enable1)&{3{genable}}));
endmodule

module sub(dqs_out, dqs_out2, dqs_in, dqs_enable, out_enable);
   parameter DQS_BITS = 1;
   inout [DQS_BITS-1:0] dqs_out;
   output [DQS_BITS-1:0] dqs_out2;
   input [DQS_BITS-1:0] dqs_in;
   input  [DQS_BITS-1:0] dqs_enable;
   input 	       out_enable;

   assign 	       dqs_out2 = dqs_out;
   bufif1 buf_dqs [DQS_BITS-1:0] (dqs_out, dqs_in, dqs_enable & {DQS_BITS{out_enable}});
endmodule
