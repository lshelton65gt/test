// last mod: Thu May  5 07:58:34 2005
// filename: test/langcov/InstanceArray/bug4405_dut2.v
// Description:  This test is 'dut2' originally located in test/bugs/bug4405, it is
// now run from this directory (test/langcov/InstanceArray).


module bug4405_dut2(in, ena, pad, out);
   input [3:0] in;
   input [1:0] ena;
   inout [3:0] pad;
   output [3:0] out;

`ifdef WORKAROUND
   pad_bit p0 (in[0], ena, pad[0], out[0]);
   pad_bit p1 (in[1], ena, pad[1], out[1]);
   pad_bit p2 (in[2], ena, pad[2], out[2]);
   pad_bit p3 (in[3], ena, pad[3], out[3]);
`else
   pad_bit p[3:0] (in, ena, pad, out);
`endif

endmodule

module pad_bit(in, ena, pad, out);
   input in;
   input [1:0] ena;
   inout pad;
   output out;

   assign out = (ena[1]) ? pad : 1'bz;
   assign pad = (ena[0]) ? in : 1'bz;

endmodule
