// this is the test from test/bugs/bug4406/dut.v, the issue was there is a
// partselect in the actual args.  at one time this created an incorrect error
// message.

module dut(a, b, c);
   input [31:0] a;
   input [3:0] 	b;
   output [15:0] c;
   wire [31:0] a;
   wire [3:0]  b;
   wire [15:0] c;
`ifdef WORKAROUND
   submod submod0 (.a(a[3:0]),   .b(b), .c(c[3:0]));
   submod submod1 (.a(a[7:4]),   .b(b), .c(c[7:4]));
   submod submod2 (.a(a[11:8]),  .b(b), .c(c[11:8]));
   submod submod3 (.a(a[15:12]), .b(b), .c(c[15:12]));
`else  // ifdef WORKAROUND
   submod submod[3:0] (.a(a[15:0]), .b(b), .c(c));
`endif // ifdef WORKAROUND
endmodule

module submod (a, b, c);
   input [3:0] a;
   input [3:0] b;
   output [3:0] c;

   assign c = a & b;
endmodule
