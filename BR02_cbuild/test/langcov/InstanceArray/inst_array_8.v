// last mod: Mon May  9 12:09:18 2005
// filename: test/langcov/InstanceArray/inst_array_8.v
// Description:  This test puts partselect in a concat as formal args to arrayed
// instance, this arg must be partselected to connect to the instances
// this testcase will not simulate with aldec, nc gold in inst_array_8.sim.log.ncgold

module inst_array_8(i,o,c);
   input [63:0] i;
   output [63:0] o;
   input c;

   byte b[7:0] (.i(i),.o({o[31:0],o[63:32]}),.c(c));
endmodule

module byte(i,o,c);
   input [7:0] i;
   output [7:0] o;
   input c;
   flop f[7:0] (i,o,c);
endmodule

module flop(i,o,c);
   input i;
   output o;
   input c;
   reg o;
   always @(posedge c)
     o <= i;
endmodule
