// last mod: Thu Jul 26 09:26:25 2007
// filename: test/langcov/InstanceArray/aog_simple_03.v
// Description:  This test checks for the proper handling of simple logic gates
// (and/or/xor/buf/not) in an array of gates situation.
// This test is identical to aog_simple_01.v but the instance indices are
// reverse order and include negative values.
// inspired by bug 7415

module aog_simple_03(i1, e1, i4, e4,
		     o4_a, o4_b, o4_c, o4_d, o4_e, o4_f, o4_g, o4_h, o4_i, o4_j, o4_k, o4_l, o4_m, o4_n,
		     o4_na, o4_nb, o4_nc, o4_nd, o4_ne, o4_nf, o4_ng, o4_nh, o4_ni, o4_nj, o4_nk, o4_nl, o4_nm, o4_nn );
   input i1, e1;
   input [3:0] i4, e4;
   output [3:0] o4_a, o4_b, o4_c, o4_d, o4_e, o4_f, o4_g, o4_h, o4_i, o4_j, o4_k, o4_l, o4_m, o4_n;
   output [3:0] o4_na, o4_nb, o4_nc, o4_nd, o4_ne, o4_nf, o4_ng, o4_nh, o4_ni, o4_nj, o4_nk, o4_nl, o4_nm, o4_nn;

  and b1[-2:1] (o4_a, i1, e1);   // out is 4 bit, data is 1 bit, enable is 1 bit
  and b2[-1:2] (o4_b, i1, e4);	// out is 4 bit, data is 1 bit, enable is 4 bit
  and b3[0:3] (o4_c, i4, e4);	// out is 4 bit, data is 4 bit, enable is 4 bit
  and b4[1:4] (o4_d, i4, e1);	// out is 4 bit, data is 4 bit, enable is 1 bit
   	 	       
  or b5[-3:0] (o4_e, i1, e1);	// out is 4 bit, data is 1 bit, enable is 1 bit
  or b6[-4:-1] (o4_f, i1, e4);	// out is 4 bit, data is 1 bit, enable is 4 bit
  or b7[-1:-4] (o4_g, i4, e4);	// out is 4 bit, data is 4 bit, enable is 4 bit
  or b8[-2:1] (o4_h, i4, e1);	// out is 4 bit, data is 4 bit, enable is 1 bit

  xor b9[4:1] (o4_i, i1, e1);	// out is 4 bit, data is 1 bit, enable is 1 bit
  xor b10[5:2] (o4_j, i1, e4);	// out is 4 bit, data is 1 bit, enable is 4 bit
  xor b11[-2:1] (o4_k, i4, e4);	// out is 4 bit, data is 4 bit, enable is 4 bit
  xor b12[-2:1] (o4_l, i4, e1);	// out is 4 bit, data is 4 bit, enable is 1 bit

  buf b13[-2:1] (o4_m, i1);	// out is 4 bit, data is 1 bit
  buf b15[-2:1] (o4_n, i4);	// out is 4 bit, data is 4 bit
   
  nand c1[-2:1] (o4_na, i1, e1);   // out is 4 bit, data is 1 bit, enable is 1 bit
  nand c2[-2:1] (o4_nb, i1, e4);	// out is 4 bit, data is 1 bit, enable is 4 bit
  nand c3[-2:1] (o4_nc, i4, e4);	// out is 4 bit, data is 4 bit, enable is 4 bit
  nand c4[-2:1] (o4_nd, i4, e1);	// out is 4 bit, data is 4 bit, enable is 1 bit
   	 	       
  nor c5[-2:1] (o4_ne, i1, e1);	// out is 4 bit, data is 1 bit, enable is 1 bit
  nor c6[-2:1] (o4_nf, i1, e4);	// out is 4 bit, data is 1 bit, enable is 4 bit
  nor c7[-2:1] (o4_ng, i4, e4);	// out is 4 bit, data is 4 bit, enable is 4 bit
  nor c8[-2:1] (o4_nh, i4, e1);	// out is 4 bit, data is 4 bit, enable is 1 bit

  xnor c9[-2:1] (o4_ni, i1, e1);	// out is 4 bit, data is 1 bit, enable is 1 bit
  xnor c10[-2:1] (o4_nj, i1, e4);	// out is 4 bit, data is 1 bit, enable is 4 bit
  xnor c11[-2:1] (o4_nk, i4, e4);	// out is 4 bit, data is 4 bit, enable is 4 bit
  xnor c12[-2:1] (o4_nl, i4, e1);	// out is 4 bit, data is 4 bit, enable is 1 bit
   
  not c13[-2:1] (o4_nm, i1);	// out is 4 bit, data is 1 bit
  not c15[-2:1] (o4_nn, i4);	// out is 4 bit, data is 4 bit

endmodule
