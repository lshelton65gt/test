// last mod: Fri Jun 29 16:03:57 2007
// filename: test/langcov/InstanceArray/bug7415_01.v
// Description:  This test tries to duplicate a problem noticed in bug 7415
// where the width of an arg to an instance array was the wrong size.  Although
// it does not duplicate that problem a different problem was found where
// incorrect logic is created for the instance array

// This is a contrived testcase. Note that if genable is true then the inout
// called 'out' is driven in all cases (either via i1 or i2 because either
// enable1 or ~enable1 will be true)
module bug7415_01(out, out2, out3, in, enable1, genable);
parameter DATAWIDTH = 3;
   inout [DATAWIDTH:1] out;
   output      out2;
   output      out3;
   input [DATAWIDTH:1] in;
   input [DATAWIDTH:1] enable1;
   input genable;

   sub #(DATAWIDTH) i1(out, out2, in, enable1, genable);
   sub #(DATAWIDTH) i2(out, out3, ~in, ~enable1, genable);
   
endmodule

module sub(dqs_out, dqs_out2, dqs_in, dqs_enable, out_enable);
   parameter DQS_BITS = 1;
   inout [DQS_BITS-1:0] dqs_out;
   output [DQS_BITS-1:0] dqs_out2;
   input [DQS_BITS-1:0] dqs_in;
   input  [DQS_BITS-1:0] dqs_enable;
   input 	       out_enable;

   assign 	       dqs_out2 = dqs_out;
   bufif1 buf_dqs [DQS_BITS-1:0] (dqs_out, dqs_in, dqs_enable & {DQS_BITS{out_enable}});
endmodule
