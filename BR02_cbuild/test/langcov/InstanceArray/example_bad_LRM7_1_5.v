// last mod: Wed May  4 17:11:37 2005
// filename: test/langcov/InstanceArray/example_bad_LRM7_1_5.v
// Description:  This test inspired by section 7.1.5, checks for a valid error messages


module example_bad_LRM7_1_5(in1, in2, out1);
   input [7:0] in1, in2;
   output [7:0] out1;

       // the following should generate an error msg
   nand inand_bad[3:0] (out1, in1, in2), inand_bad[7:3] (out1, in1, in2); // attempt to use the same instance name over

endmodule
