// last mod: Mon Jul 23 15:38:36 2007
// filename: test/langcov/InstanceArray/aog_bufif_01.v
// Description:  This test checks for the proper handling of bufif1/bufif0 in an
// array of gates situation.

// inspired by bug 7415

module aog_bufif_01(i1, e1, i4, e4, o4_a, o4_b, o4_c, o4_d, o4_e, o4_f, o4_g, o4_h);
   input i1, e1;
   input [3:0] i4, e4;
   output [3:0] o4_a, o4_b, o4_c, o4_d, o4_e, o4_f, o4_g, o4_h;

  bufif0 b1[3:0] (o4_a, i1, e1);        // out is 4 bit, data is 1 bit, enable is 1 bit
  bufif0 b2[3:0] (o4_b, i1, e4);	// out is 4 bit, data is 1 bit, enable is 4 bit
  bufif0 b3[3:0] (o4_c, i4, e4);	// out is 4 bit, data is 4 bit, enable is 4 bit
  bufif0 b4[3:0] (o4_d, i4, e1);	// out is 4 bit, data is 4 bit, enable is 1 bit
   	 	       
  bufif1 b5[3:0] (o4_e, i1, e1);        // out is 4 bit, data is 1 bit, enable is 1 bit
  bufif1 b6[3:0] (o4_f, i1, e4);	// out is 4 bit, data is 1 bit, enable is 4 bit
  bufif1 b7[3:0] (o4_g, i4, e4);	// out is 4 bit, data is 4 bit, enable is 4 bit
  bufif1 b8[3:0] (o4_h, i4, e1);	// out is 4 bit, data is 4 bit, enable is 1 bit
   
   

endmodule
