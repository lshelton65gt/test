// last mod: Fri May  6 15:07:18 2005
// filename: test/langcov/InstanceArray/inst_array_10.v
// Description:  This test checks for the proper name of arrayed module instances

module inst_array_10(i,o,clk);
   input [3:0] i;
   output [3:0] o;
   input clk;
   flop f[2:-1] (i,o,clk);
endmodule

module flop(i,o,c);
   input i;
   output o;
   input c;
   reg o;
   always @(posedge c) begin
      o <= i;
      $display("Info (%m): o = %d", o);
   end
endmodule
