// last mod: Thu May 12 09:56:16 2005
// filename: test/langcov/InstanceArray/bug4427_dut3.v
// Description:  This test is 'dut3.v' from bug 4427, it segfaulted at one time
// because of the way that connections were made to the instances.

module bug4427_dut3(i,o);
   input [1:0] i;
   output [1:0] o;
   iocell io[1:0] (.i(i),.o(o[1:0]));
endmodule

module iocell(i,o);
   input i;
   output o;
   assign o=i;
endmodule
