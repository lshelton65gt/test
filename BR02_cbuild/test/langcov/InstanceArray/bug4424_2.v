// last mod: Thu May  5 22:38:38 2005
// filename: test/bugs/bug4424/dut2.v
// Description:  This test is a smaller version of dut from bug4424
// if I is non-negative then this testcase does not crash

`define I -1

module dut2(i,o,e);
   input  i;
   output o;
   input e;

   assign p[`I].ena = e;

   pad p[`I:`I] (i,o);		// creates a single instance with the name
				// p[`I],  if I is 0 the name is 'p[0]', if I is
				// -1 the name is 'p[-1]'
   
endmodule

module pad(i,o);
   input i;
   output o;
   wire ena;
   assign o = (ena) | i;
endmodule
