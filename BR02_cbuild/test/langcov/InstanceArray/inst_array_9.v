// last mod: Fri May  6 15:05:00 2005
// filename: test/langcov/InstanceArray/inst_array_9.v
// Description:  This test uses uses hierarchical refs in a concat as actual
// args to arrayed instance, the hier refs are to other instances of same
// arrayed instance.


module inst_array_9(i,o,e);
   input [3:0] i;
   output [3:0] o;
   input e;
   pad p[3:0] (.i(i), .o(o), .e({e,p[3].e,p[2].e,p[1].e}));
endmodule

module pad(i,o,e);
   input i;
   output o;
   input e;
   assign o = (e) ? i : 1'b0;
endmodule
