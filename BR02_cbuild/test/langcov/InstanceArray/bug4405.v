// last mod: Tue May  3 21:59:54 2005
// filename: test/langcov/bug4405.v
// Description:  This test checks for the correct port connections on instances created with arrays of instances specification.


module bug4405(clock, in32, in8, in4, out);
   input clock;
   input [31:0] in32;
   input [7:0] 	in8;
   input [3:0] 	in4;
   output [127:0] out;

   sub s[3:0] (.o(out), .in8(in32), .in2(in8), .in1(in4), .clock(clock));
endmodule

module sub(o,in8,in2, in1, clock);
   output [31:0] o;
   reg [31:0] o;
   input [7:0] 	 in8;
   input [1:0] 	 in2;
   input 	 in1;
   input 	 clock;

   always @(posedge clock)
     begin: main
	o = in8 * (in2 << in1);
     end
endmodule
