// last mod: Thu May  5 07:56:03 2005
// filename: test/langcov/InstanceArray/bug4405_dut.v
// Description:  This test is 'dut' that was placed in test/bugs/bug4405, it is
// now run from this directory

module bug4405_dut(a, b, c);
   input [15:0] a;
   input [3:0] 	b;
   output [15:0] c;
   wire [15:0] a;
   wire [3:0]  b;
   wire [15:0] c;
`ifdef WORKAROUND
   submod submod0 (.a(a[3:0]),   .b(b), .c(c[3:0]));
   submod submod1 (.a(a[7:4]),   .b(b), .c(c[7:4]));
   submod submod2 (.a(a[11:8]),  .b(b), .c(c[11:8]));
   submod submod3 (.a(a[15:12]), .b(b), .c(c[15:12]));
`else  // ifdef WORKAROUND
   submod submod[3:0] (.a(a), .b(b), .c(c));
`endif // ifdef WORKAROUND
endmodule

module submod (a, b, c);
   input [3:0] a;
   input [3:0] b;
   output [3:0] c;

   assign c = a & b;
endmodule
