// last mod: Wed May  4 15:10:44 2005
// filename: test/langcov/InstanceArray/inst_array_5.v
// Description:  This test contains an array of instances with formal
// ports of 4,2,1 bits wide.
// actual ports are full vectors and concats of the appropriate sizes
// like inst_array_4 but the declared ranges of the ports use negative values or
// are reversed.


module inst_array_5(in1, in2, in8, out16A, out16B, out16C, out4A, out4B);
   input in1;
   input [0:1] in2;
   input [1:8] in8;
   output [-6:9] out16A, out16B, out16C;
   output [2:-1]  out4A, out4B;

   submod instA[0:3] (.out_w4(out16A), .in_w2(in8), .in_w1(in1));

   submod instB[2:-1] (.out_w4(out16B), .in_w2(in8), .in_w1(in1));

   submod instC[-1:2] (.out_w4(out16C), .in_w2(in8), .in_w1(in1));

   submod instD[2:2]   (.out_w4(out4A), .in_w2(in2), .in_w1(in1));

   submod instE[-1:-1] (.out_w4(out4B), .in_w2(in2), .in_w1(in1));

endmodule



module submod(out_w4, in_w2, in_w1);
   output [1:-2] out_w4;
   input [0:1] 	in_w2;
   input 	in_w1;

   initial 
     $display("%m");
   
   assign 	 out_w4 = in_w2 << in_w1 ; // out_w4[1] is always 0
endmodule
