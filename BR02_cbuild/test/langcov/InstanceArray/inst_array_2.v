// last mod: Thu May  5 17:03:28 2005
// filename: test/langcov/InstanceArray/inst_array_2.v
// Description:  This test contains an array of instances with formal
// ports of 32,2,1 bits wide.
// actual ports are full vectors, and an expression that needs to be properly sized


module inst_array_2(in1, out33, in32A, in3B);
   input in1;
   input [31:0] in32A;
   input [2:0]  in3B;
   output [32:0] out33;

   // note in the following the arg for .in_w32 is the result of a sum between a
   // 32 bit value and a 3 bit value (this expression must be sized to 32 bits
   // for this test to work)
   submod instA[3:0] (.out_w33(out33), .in_w32(in32A+in3B), .in_w1(in1));

endmodule



module submod(out_w33, in_w32, in_w1);
   output [32:0] out_w33;
   input [31:0] in_w32;
   input 	in_w1;

   initial 
     $display("%m");

   assign 	 out_w33 = in_w32 << in_w1 ;
endmodule
