// last mod: Thu May  5 07:03:36 2005
// filename: test/langcov/InstanceArray/example_2_LRM7_1_6.v
// Description:  This test was inspired by LRM setion 7.1.6 example 2


module example_2_LRM7_1_6(ena, in1, in2, out1, out2);
   input ena;
   input [3:0] in1, in2;
   output [3:0] out1;
   output [3:0] out2;

   bufif0 ar[3:0] (out1, in1, ena);

   // and equivalent logic (out1 == out2)
   bufif0 ar3(out2[3], in1[3], ena);
   bufif0 ar2(out2[2], in1[2], ena);
   bufif0 ar1(out2[1], in2[1], ena);
   bufif0 ar0(out2[0], in2[0], ena);
endmodule
