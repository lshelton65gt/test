// last mod: Tue Jul 24 08:21:02 2007
// filename: test/langcov/InstanceArray/aog_simple_02.v
// Description:  This test checks for the proper handling of simple logic gates,
// where the lhs is not the width of the instance array.  Verilog only allows
// the width of the arguments of gate primitives to be either 1 bit or the
// number of instances in the array of instances.  In this example that number
// is 4.
// this example is kind of nonsense since for each line the single output bit is
// written by 4 assignments.  In the case where all args of the gate are 1 bit
// wide we expect non-x values.
//
// In the case where any of the second or third args to these bufif* gates is wider than 1 bit the
// output is undefined because the implementation will write to the same single output bit
// multiple times.  Gold files were created by running test with simulator and then manually
// inspecting the output bits that were driven multiple times.
// 
// Operators and/or/xor/buf/not are tested
// inspired by bug 7415

module aog_simple_02(i1, e1, i4, e4,
		     o1_a, o1_b, o1_c, o1_d, o1_e, o1_f, o1_g, o1_h, o1_i, o1_j, o1_k, o1_l, o1_m, o1_n,
		     o1_na, o1_nb, o1_nc, o1_nd, o1_ne, o1_nf, o1_ng, o1_nh, o1_ni, o1_nj, o1_nk, o1_nl, o1_nm, o1_nn );
   input i1, e1;
   input [3:0] i4, e4;
   output  o1_a, o1_b, o1_c, o1_d, o1_e, o1_f, o1_g, o1_h, o1_i, o1_j, o1_k, o1_l, o1_m, o1_n;
   output  o1_na, o1_nb, o1_nc, o1_nd, o1_ne, o1_nf, o1_ng, o1_nh, o1_ni, o1_nj, o1_nk, o1_nl, o1_nm, o1_nn;


   and b1[3:0] (o1_a, i1, e1);		// out is 1 bit, data is 1 bit, enable is 1 bit
   and b2[3:0] (o1_b, i1, e4);		// out is 1 bit, data is 1 bit, enable is 4 bit
   and b3[3:0] (o1_c, i4, e4);		// out is 1 bit, data is 4 bit, enable is 4 bit
   and b4[3:0] (o1_d, i4, e1);		// out is 1 bit, data is 4 bit, enable is 1 bit
   
   or b5[3:0] (o1_e, i1, e1);		// out is 1 bit, data is 1 bit, enable is 1 bit
   or b6[3:0] (o1_f, i1, e4);		// out is 1 bit, data is 1 bit, enable is 4 bit
   or b7[3:0] (o1_g, i4, e4);		// out is 1 bit, data is 4 bit, enable is 4 bit
   or b8[3:0] (o1_h, i4, e1);		// out is 1 bit, data is 4 bit, enable is 1 bit

   xor b9[3:0] (o1_i, i1, e1);		// out is 1 bit, data is 1 bit, enable is 1 bit
   xor b10[3:0] (o1_j, i1, e4);		// out is 1 bit, data is 1 bit, enable is 4 bit
   xor b11[3:0] (o1_k, i4, e4);		// out is 1 bit, data is 4 bit, enable is 4 bit
   xor b12[3:0] (o1_l, i4, e1);		// out is 1 bit, data is 4 bit, enable is 1 bit

   buf b13[3:0] (o1_m, i1);		// out is 1 bit, data is 1 bit
   buf b15[3:0] (o1_n, i4);		// out is 1 bit, data is 4 bit
   
   nand c1[3:0] (o1_na, i1, e1); 	// out is 1 bit, data is 1 bit, enable is 1 bit
   nand c2[3:0] (o1_nb, i1, e4);	// out is 1 bit, data is 1 bit, enable is 4 bit
   nand c3[3:0] (o1_nc, i4, e4);	// out is 1 bit, data is 4 bit, enable is 4 bit
   nand c4[3:0] (o1_nd, i4, e1);	// out is 1 bit, data is 4 bit, enable is 1 bit
   
   nor c5[3:0] (o1_ne, i1, e1);		// out is 1 bit, data is 1 bit, enable is 1 bit
   nor c6[3:0] (o1_nf, i1, e4);		// out is 1 bit, data is 1 bit, enable is 4 bit
   nor c7[3:0] (o1_ng, i4, e4);		// out is 1 bit, data is 4 bit, enable is 4 bit
   nor c8[3:0] (o1_nh, i4, e1);		// out is 1 bit, data is 4 bit, enable is 1 bit

   xnor c9[3:0] (o1_ni, i1, e1);	// out is 1 bit, data is 1 bit, enable is 1 bit
   xnor c10[3:0] (o1_nj, i1, e4);	// out is 1 bit, data is 1 bit, enable is 4 bit
   xnor c11[3:0] (o1_nk, i4, e4);	// out is 1 bit, data is 4 bit, enable is 4 bit
   xnor c12[3:0] (o1_nl, i4, e1);	// out is 1 bit, data is 4 bit, enable is 1 bit
   
   not c13[3:0] (o1_nm, i1);		// out is 1 bit, data is 1 bit
   not c15[3:0] (o1_nn, i4);		// out is 1 bit, data is 4 bit

endmodule
