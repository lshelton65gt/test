#include "libdut1.h"
#include <stdio.h>

int main(int argc, void* argv) {
   CarbonObjectID *obj;
   CarbonNetID *p0ena;
   CarbonNetID *p1ena;
   CarbonNetID *p2ena;
   CarbonNetID *p3ena;

   obj = carbon_dut1_create(eCarbonFullDB,eCarbon_NoFlags);
   p0ena = carbonFindNet(obj,"dut1.p[-1000].ena");
   p1ena = carbonFindNet(obj,"dut1.p[-1001].ena");
   p2ena = carbonFindNet(obj,"dut1.p[-1002].ena");
   p3ena = carbonFindNet(obj,"dut1.p[-1003].ena");

   printf("checking that nets from instance arrays are accessable from shell\n");

   carbonSchedule(obj,0);
   carbonDepositWord(obj,p0ena,1,0,0);
   carbonDepositWord(obj,p1ena,1,0,0);
   carbonDepositWord(obj,p2ena,1,0,0);
   carbonDepositWord(obj,p3ena,1,0,0);
   carbonSchedule(obj,0);

   carbonDestroy(&obj);
   printf("done\n");
   return 0;
}
