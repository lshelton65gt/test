module dut1(i,o,e);
   input [19:16] i;
   output [-2:-5] o;
   input e;

   pad p[-1000:-1003] (i,o);
endmodule

module pad(i,o);
   input i;
   output o;
   wire ena; // carbon depositSignal
   assign o = (ena) ? i : 1'b0;
endmodule
