module dut2(ao, clk, ai, en);
   output [7:0] ao;
   input 	clk;
   input [7:0] 	ai;
   input 	en;

   reg [7:0] 	ao_int;

   always @(posedge clk)
     ao_int <= ai;

   assign ao = (en) ? ao_int : 'bz;

   pullup p[7:0](ao);
endmodule
