#include "libdut2.h"
#include "assert.h"

int main(int argc, void* argv) {
   CarbonObjectID* obj;
   CarbonNetID* clk;
   CarbonNetID* en;
   CarbonNetID* in;
   CarbonNetID* out;
   UInt32 ONE;
   UInt32 ZERO;
   UInt32 errors;
   UInt32 in_val, en_val, out_val;

   obj = carbon_dut2_create(eCarbonFullDB, eCarbon_NoFlags);
   assert(obj);
   clk = carbonFindNet(obj, "dut2.clk");
   en  = carbonFindNet(obj, "dut2.en");
   in  = carbonFindNet(obj, "dut2.ai");
   out = carbonFindNet(obj, "dut2.ao");
   printf("checking that logic within instance arrays is correct\n");

   assert(clk);
   assert(en);
   assert(in);
   assert(out);


   ONE = 1;
   ZERO = 0;
   errors = 0;


   in_val = 0x75;
   en_val = 0x0;

   carbonDeposit(obj, en, &en_val, 0);
   carbonDeposit(obj, in, &in_val, 0);
   carbonDeposit(obj, clk, &ZERO, 0);
   carbonSchedule(obj, 0);
   carbonDeposit(obj, clk, &ONE, 0);
   carbonSchedule(obj, 1);
   carbonDeposit(obj, clk, &ZERO, 0);
   carbonSchedule(obj, 2);
   carbonExamine(obj, out, &out_val, 0);

   printf("Input 'in' = 0x%02X\n", in_val);
   printf("Enable 'en' = 0x%01X\n", en_val);
   printf("Output 'out' = 0x%02X\n", out_val);

   if (out_val == 0xFF) {
      printf("Success - output has correct value\n");
   } else {
      printf("Failure - output has wrong value\n");
      errors++;
   }

   return errors;
}
