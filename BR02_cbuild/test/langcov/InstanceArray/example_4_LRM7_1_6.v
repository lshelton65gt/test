// last mod: Thu May  5 17:27:13 2005
// filename: test/langcov/InstanceArray/example_4_LRM7_1_6.v
// Description:  This test was inspire3d by example 4 from the Verilog LRM,
// section 7.1.6, here module instances are chained together, and the module
// that is instantiated is parameterized.


module example_4_LRM7_1_6(in, out, clk);
   parameter M = 3, N = 4; // M=width,N=depth
   input [M-1:0] in;
   output [M-1:0] out;
   input 	  clk;
   wire [M*(N-1):1] t;
   // #(M) redefines the bits parameter for dffn
   // create p[1:N] columns of dffn rows (pipeline)
   dffn #(M) p[1:N] ({out, t}, {t, in}, clk);
endmodule

module dffn (q, d, clk);
   parameter bits = 1;
   input [bits-1:0] d;
   output [bits-1:0] q;
   input 	     clk ;
   DFF dff[bits-1:0] (q, d, clk); // create a row of D flip-flops
endmodule

module DFF (out, in, clk);
   output out;
   input  in, clk;

   reg 	  out;
   
   initial
     out = 0;

   always @(posedge clk)
     out = in;
endmodule
