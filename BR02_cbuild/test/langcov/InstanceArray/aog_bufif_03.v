// last mod: Tue Jul 24 14:12:51 2007
// filename: test/langcov/InstanceArray/aog_bufif_02.v
// Description:  This test checks for the proper handling of bufif1/bufif0 in an
// array of gates situation. In this example each line contains a sizing problem
// so that it should not be supported.  We expect errors to be reported with
// this testcase.

// inspired by bug 7415

module aog_bufif_03(i1, i2, e1, e2, i4, e4, o1_a, o1_b, o1_c, o1_d, o1_e, o1_f, o1_g, o1_h, o3_c, o3_g);
   input i1, e1;
   input [1:0] i2, e2;
   input [3:0] i4, e4;
   output o1_a, o1_b, o1_c, o1_d, o1_e, o1_f, o1_g, o1_h;
   output [2:0] o3_c, o3_g;

  bufif0 b1[3:0] (o1_a, i1, e2);        // out is 1 bit, data is 1 bit, enable is 2 bit (problem)
  bufif0 b2[3:0] (o1_b, i2, e4);	// out is 1 bit, data is 2 bit(problem), enable is 4 bit
  bufif0 b3[3:0] (o3_c, i4, e4);	// out is 3 bit(problem), data is 4 bit, enable is 4 bit
  bufif0 b4[3:0] (o1_d, i2, e2);	// out is 1 bit, data is 2 bit(problem), enable is 2 bit(problem)
   	 	       
  bufif1 b5[3:0] (o1_e, i1, e2);        // out is 1 bit, data is 1 bit, enable is 2 bit(problem)
  bufif1 b6[3:0] (o1_f, i2, e4);	// out is 1 bit, data is 2 bit(problem), enable is 4 bit
  bufif1 b7[3:0] (o3_g, i4, e4);	// out is 3 bit(problem), data is 4 bit, enable is 4 bit
  bufif1 b8[3:0] (o1_h, i2, e2);	// out is 1 bit, data is 2 bit(problem), enable is 2 bit(problem)
   
   

endmodule
