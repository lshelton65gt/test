// last mod: Tue May 17 11:54:44 2005
// filename: test/langcov/InstanceArray/bug4427_dut2.v
// Description:  This test is 'dut2.v' from bug 4427, it segfaulted at one time
// because of the way that connections were made to the instances.
// aldec does not support arrays of instances in a generate block
// nc crashes on this testcase

module bug4427_dut2(i,o,c);
   input [63:0] i;
   output [63:0] o;
   input c;

   generate
   genvar v;
   for (v=0; v<16; v=v+1) begin: u
      flop f[3:0] (.i(i[(4*v) +: 4]), .o(o[(4*v) +: 4]), .c(c));
   end
   endgenerate
endmodule

module flop(i,o,c);
   input i;
   output o;
   input c;
   reg o;
   always @(negedge c)
     o <= i;
endmodule
