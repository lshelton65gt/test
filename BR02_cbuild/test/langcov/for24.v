module top ( in, out );
   parameter cnt=60;

   input in;
   output out;
   reg    out;

   integer   i;

   reg [cnt-1:0] a;
   reg [cnt-1:0] b;
   reg [cnt-1:0] c;
   reg [cnt-1:0] d;
   reg [cnt-1:0] e;

   always @ ( in )
     begin
	// push input on top; get output from bottom
	out <= a[0];
	a[cnt-1] <= b[0];
	b[cnt-1] <= c[0];
	c[cnt-1] <= d[0];
	d[cnt-1] <= e[0];
	e[cnt-1] <= in;
	for ( i = 0 ; i < (cnt-1) ; i = i + 1 )
	  begin
	     a[i] <= a[i+1];
	     b[i] <= b[i+1];
	     c[i] <= c[i+1];
	     d[i] <= d[i+1];
	     e[i] <= e[i+1];
	  end
     end
endmodule // top
