// Similar to the "for" example from the VerilogLRM. Initializes a block..
module swap_or_reset ( out, clk, reset, in );
   output [31:0] out;
   reg [31:0] 	 out;
   
   input 	 reset;
   input 	 clk;
   input [31:0]  in;

   integer 	 i;
   
   always @(posedge clk)
     begin
	
	if (reset)
	  begin
	     for ( i = 0 ; i <= 31 ; i = i + 1 )
	       begin
		  out[i] = 0; // we could have assigned to a constant...
	       end
	  end
	else
	  begin
	     for ( i = 0 ; i <= 31 ; i = i + 1 )
	       begin
		  out[i] = in[31-i];
	       end
	  end // else: !if(reset)
     end // always @ (posedge clk or posedge reset)

endmodule // meminit

   

