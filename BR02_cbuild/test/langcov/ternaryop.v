/* Test ternary operators. */
module top(i1, i2, sel, o1
          );

input [3:4] i1;
input [-1:0] i2;
input [0:10] sel;
output [0:2] o1;

assign o1 = !(|sel) ? i1 : i2;

endmodule
