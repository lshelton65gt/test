// last mod: Wed Jun  8 10:29:20 2005
// filename: test/langcov/arrays.v
// Description:  This test is designed to test the ability to declare arrays of
// all types.  

module arrays(in8, in32, out, a1, a2, a3, a4);
   input [7:0] in8;
   input [7:0] in32;
   output [32:0] out;
   input [7:0] a1, a3, a4;
   input [2:0] a2;

   // declares a memory mema of 256 8-bit registers. The indices are 0 to 255
   reg [7:0]   mema[0:255];

   // declare an array of 16 integers
   integer     intarray [15:0];

   // declare array of wires
   wire [7:0]  w_array[0:5];

   always @(in8 or a1)
     mema[a1] = in8;

   always @(in32 or a2)
     intarray[a2] = in32;

   // create some data dependencies so that arrays stay alive
   assign      w_array[2] = mema[5];
   assign      w_array[3] = intarray[6];
   wire [7:0]  row = w_array[a4];
   assign      out = in8[1] | row[0] ;
endmodule


