// An example of an assign we cannot alias
// due to the relationship within the always block.
module top(a, e, sel, c, d, f, g);
   input sel, c, d, f, g;
   output a, e;

   reg 	a;
   reg 	b;
   reg 	e;
   always @(sel or c or b or d or f or g)
     begin
	if (sel)
	  begin
	     a = c;
	     e = b & f;
	  end
	else
	  begin
	     a = d;
	     e = b & g;
	  end
     end

   always b = a;
endmodule // top
