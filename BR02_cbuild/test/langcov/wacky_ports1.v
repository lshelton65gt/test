module top(out[7:4], out[3:0], in);
  output [7:0] out;
  input  [7:0] in;

  assign out = in;
endmodule
