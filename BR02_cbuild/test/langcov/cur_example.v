/* Design updated to have the current supported stuff. */
module top(i1, i2, o1, o2, i3, o3, clk);
input i1, i2, i3;
output o1, o2, o3;
input clk;
assign o1 = i1;
myassign inst1 (i2, o2, clk);
myassign inst2 (i3, o3, clk);
endmodule

module myassign(i, o, clk);
input i, clk;
output o;
reg o;
always @(posedge clk)
  begin
  o = i;
  end
endmodule
