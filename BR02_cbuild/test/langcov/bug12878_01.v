// July 2009
// MVV was failing with error 20537 - recursive localparams.
// It was fixed in MVV 2006.1.5.c.

`define CLK_SIZE 46 
`define CLK_MSB (`CLK_SIZE-1) 
`define CLK `CLK_MSB:0 

`define CLK_EBI        9

`define RTC_VBAT_MASK  1'b0
`define DAC_MASK           2'b10
`define AC_MASK 2'b01
`define ADC_MASK           2'b01
`define SPI_MASK           4'b0011    
`define USART_MASK         8'b00011111
`define TIM16_MASK         8'b00011111
`define TWI_MASK           4'b0101

`define ALWAYS_CLK_MASK   6'b111111

`define DIG_CLK_MASK      { (`ifdef AES_ENABLED 1'b1 `else 1'b0 `endif), \
                            (`ifdef EBI_ENABLED 1'b1 `else 1'b0 `endif), \
                            2'b11, \
                            (`ifdef DMA_ENABLED 1'b1 `else 1'b0 `endif) \
                            }
                      
`define ANAA_CLK_MASK   { (`DAC_MASK  == 2'b01) | (`DAC_MASK  == 2'b11), \
                          (`ADC_MASK == 2'b01) | (`ADC_MASK == 2'b11), \
                          (`AC_MASK == 2'b01) | (`AC_MASK == 2'b11) }
`define ANAB_CLK_MASK   { (`DAC_MASK  == 2'b10) | (`DAC_MASK  == 2'b11), \
                          (`ADC_MASK == 2'b10) | (`ADC_MASK == 2'b11), \
                          (`AC_MASK == 2'b10) | (`AC_MASK == 2'b11) }
  

`define TCCOMC_CLK_MASK   {  ((`TWI_MASK & 4'b0001) == 4'b0001),            \
                             ((`USART_MASK & 8'b00000010) == 8'b00000010),  \
                             ((`USART_MASK & 8'b00000001) == 8'b00000001),  \
                             ((`SPI_MASK &  4'b0001) == 4'b0001),           \
                             ((`TIM16_MASK & 8'b00000001) == 8'b00000001),  \
                             ((`TIM16_MASK & 8'b00000010) == 8'b00000010),  \
                             ((`TIM16_MASK & 8'b00000001) == 8'b00000001)}

`define TCCOMD_CLK_MASK   {  ((`TWI_MASK & 4'b0010) == 4'b0010),            \
                             ((`USART_MASK & 8'b00001000) == 8'b00001000),  \
                             ((`USART_MASK & 8'b00000100) == 8'b00000100),  \
                             ((`SPI_MASK &  4'b0010) == 4'b0010),           \
                             ((`TIM16_MASK & 8'b00000100) == 8'b00000100),  \
                             ((`TIM16_MASK & 8'b00001000) == 8'b00001000),  \
                             ((`TIM16_MASK & 8'b00000100) == 8'b00000100)}
                              
`define TCCOME_CLK_MASK   {  ((`TWI_MASK & 4'b0100) == 4'b0100),            \
                             ((`USART_MASK & 8'b00100000) == 8'b00100000),  \
                             ((`USART_MASK & 8'b00010000) == 8'b00010000),  \
                             ((`SPI_MASK &  4'b0100) == 4'b0100),           \
                             ((`TIM16_MASK & 8'b00010000) == 8'b00010000),  \
                             ((`TIM16_MASK & 8'b00100000) == 8'b00100000),  \
                             ((`TIM16_MASK & 8'b00010000) == 8'b00010000)}

`define TCCOMF_CLK_MASK   {  ((`TWI_MASK & 4'b1000) == 4'b1000),            \
                             ((`USART_MASK & 8'b10000000) == 8'b10000000),  \
                             ((`USART_MASK & 8'b01000000) == 8'b01000000),  \
                             ((`SPI_MASK &  4'b1000) == 4'b1000),           \
                             ((`TIM16_MASK & 8'b01000000) == 8'b01000000),  \
                             ((`TIM16_MASK & 8'b10000000) == 8'b10000000),  \
                             ((`TIM16_MASK & 8'b01000000) == 8'b01000000)}


`define CLK_1X_MASK {    (`RTC_VBAT_MASK),   \
                         (`TCCOMF_CLK_MASK), \
                         (`TCCOME_CLK_MASK), \
                         (`TCCOMD_CLK_MASK), \
                         (`TCCOMC_CLK_MASK), \
                         (`ANAB_CLK_MASK),   \
                         (`ANAA_CLK_MASK),   \
                         (`DIG_CLK_MASK),    \
                         (`ALWAYS_CLK_MASK) }   
               
`define CLK_2X_MASK `CLK_1X_MASK & \
                     (1<< `CLK_EBI)


module foo(clk, x);
   input [`CLK] clk;
   output [`CLK] x;
   
   localparam [`CLK] CLK_1X_MASK_G  = `CLK_1X_MASK; 
   localparam [`CLK] CLK_2X_MASK_G  = `CLK_2X_MASK;

endmodule // foo




