// design that allowed us to discover a problem with aliasing
// file stop_Ga2.v

module top (clk1, A, B, C, X, Y, Z );

   input clk1;
   input [3:0]    B, C,  X;
   output [3:0] A,          Y, Z; 
   reg [3:0]    A,          Y, Z;

   wire [3:0] Aw,     Yw, Zw; 

   initial
     begin
	A = 15;
	Y = 15;
	Z = 15;
     end


   m1 i1 (clk1, Aw, B, C );
   m3 i3 (clk1, X, Yw, Zw );

endmodule

module m1(clk1, A, B, C);

   input clk1;
   input [3:0]    B, C;
   output [3:0] A; 
   reg [3:0]    A; 
   
   always @(posedge clk1)
     begin
        A = B + C;
	
     end
endmodule

module m3(clk1, X, Y, Z );

   input clk1;
   input [3:0]  X;
   output [3:0] Y, Z; 
   reg [3:0]    Y, Z, T1; 

     always @(posedge clk1)
     begin
        Y = X;
	T1 = Y;			// we should allow the aliasing of T1 and Y
        Z = T1;
        $stop;

     end

endmodule
