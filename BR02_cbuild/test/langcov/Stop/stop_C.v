// basedesign for testing of execution flow and schedules related to $stop and $finish

module top (clk1, q1, q2, q3, fin, ctr);
   input clk1;
   output [3:0] q1, q2, q3;
   output 	fin;
   output [3:0]	ctr;
   reg [3:0] 	q1, q2, q3;
   reg [3:0] 	ctr;
   reg 		fin;

   initial
     begin
	q1 = 15;
	q2 = 15;
	q3 = 15;
	ctr = 0;
	fin = 0;
     end

   always @(posedge clk1)
     begin
	ctr = ctr + 1;

	q1 = ctr;
	if (ctr >= 1)
	  begin
	     $stop(0);		// within this we expect:
				// (q1 == ctr) && (q2 != ctr) && (q3 != ctr)
	  end

	q2 = ctr;

	if ( ctr > 5 )   	// finish after cycle 5
	  begin
	     $finish(2);	// within this we expect:
				// (q1 == ctr) && (q2 == ctr) && (q3 != ctr)

	     fin = 1;		// try to change a value after finish
	  end

	q3 = ctr;
     end
   

endmodule // top
