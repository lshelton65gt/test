// Testing of $stop statements when block-splitting is likely. If
// block splitting occurs on the 'main' block in bug3441_a.v, one or
// more of the $stop calls will observe an incorrect value on top.out.

#include "libbug3441_a.h"   /* generated header file */
#include <cstdio>
#include <cassert>
#include <cstring>


/* define a callback function for stop and finish */
void myControlCBFunction (CarbonObjectID* carbonObject,
                          CarbonControlType callbackType,
                          CarbonClientData /*data*/,
                          const char* verilogFilename,
                          int verilogLineNumber)
{

  CarbonNetID* top_fubar = carbonFindNet(carbonObject, "top.fubar");
  CarbonNetID* top_out = carbonFindNet(carbonObject, "top.out");
  UInt32 v_top_fubar;
  UInt32 v_top_out;
  carbonExamine(carbonObject, top_fubar, &v_top_fubar, NULL);
  carbonExamine(carbonObject, top_out, &v_top_out, NULL);

  // We have coordinated the testdriver below such that top.out will
  // get the value of top.fubar during the cycle when the $stop is
  // triggered. Further, the update to top.out _must_ occur before the
  // $stop.

  fprintf(stdout, "%s called,(%s:%d) top.fubar: %d, top.out: %d, testStatus: %s\n",
          ( (callbackType == eCarbonStop)? "$stop" : "$finish"),
          verilogFilename, verilogLineNumber,
          v_top_fubar, v_top_out,
          ( (v_top_fubar == v_top_out) ? "OK": "ERROR")
          );
}


// return true if execution should terminate
bool printStatusReport( CarbonStatus status )
{
  if ( status == eCarbon_ERROR )
  {
    fprintf(stdout, "Error seen, continue anyway\n");
  }
  if ( status == eCarbon_STOP )
  {
    fprintf(stdout, "Stop seen, continue anyway\n");
  }
  if ( status == eCarbon_FINISH )
  {
    fprintf(stdout, "Finish seen, exiting\n");
    return true;
  }
  return false;
}


/*! 
 * Run the schedule twice, triggering a posedge of the clock.
 * Returns true if simulation should terminate.
 */
bool schedulePosedgeClock(CarbonObjectID * dp, CarbonNetID * clock, CarbonTime * the_time)
{
  CarbonStatus status;

  (*the_time) += 5;
  
  // the cast to 'int' is necessary to avoid solaris endian issues
  fprintf(stdout, "\nstart of cycle at time: %d\n", (int)(*the_time));

  carbonDepositWord(dp, clock, 0, 0, 0);
  status = carbonSchedule(dp, *the_time);
  if ( printStatusReport(status) ) return true;

  (*the_time) += 5;

  carbonDepositWord(dp, clock, 1, 0, 0);
  status = carbonSchedule(dp, *the_time);
  if ( printStatusReport(status) ) return true;

  return false;
}

int main()
{
  int cycles;

  // the following line makes the stdout unbuffered so that it will
  // appear in the proper order relative to stderr msgs.  this is only
  // needed for simplify debugging of future problems
  setvbuf(stdout, NULL, _IONBF, 0);

  /* Instantiate a model */
  CarbonObjectID *dp = carbon_bug3441_a_create(eCarbonIODB, eCarbon_NoInit);

  fprintf(stdout, "START: bug3441_a \n");

  CarbonStatus status;

  /* setup callback */
  // now add a bunch of callbacks, we use the same function but unique id via the user-data
  /* CarbonRegisteredControlCBDataID* IDForStop1 =*/ carbonAdminAddControlCB(dp, myControlCBFunction, (CarbonClientData)1, eCarbonStop);
  /* CarbonRegisteredControlCBDataID* IDForFinish1 =*/ carbonAdminAddControlCB(dp, myControlCBFunction, (CarbonClientData)1, eCarbonFinish);

  status = carbonInitialize(dp, NULL, NULL, NULL);
  if ( printStatusReport(status) ) return 0;


  /* Get handles to all the I/O nets */

  // Inputs
  CarbonNetID* top_clock = carbonFindNet(dp, "top.clock");
  CarbonNetID* top_cond1 = carbonFindNet(dp, "top.cond1");
  CarbonNetID* top_cond2 = carbonFindNet(dp, "top.cond2");
  CarbonNetID* top_arg1  = carbonFindNet(dp, "top.arg1");
  CarbonNetID* top_arg2  = carbonFindNet(dp, "top.arg2");
  CarbonNetID* top_something = carbonFindNet(dp, "top.something");
  CarbonNetID* top_fubar     = carbonFindNet(dp, "top.fubar");

  // Outputs
  CarbonNetID* top_out  = carbonFindNet(dp, "top.out");
  CarbonNetID* top_out2 = carbonFindNet(dp, "top.out2");

  CarbonTime the_time = 0;
  if (schedulePosedgeClock(dp, top_clock, &the_time)) return 0;

  UInt32 v_top_fubar;
  UInt32 v_top_out;

  // Deposit values such that 'top.out' will get updated.
  carbonDepositWord(dp, top_cond1, 1, 0, 0);
  carbonDepositWord(dp, top_arg1,  5, 0, 0);
  carbonDepositWord(dp, top_arg2,  7, 0, 0);
  // Must be less than both top.arg1 and top.arg2.
  carbonDepositWord(dp, top_something, 3, 0, 0); 
  // The value that top.out will receive:
  v_top_fubar = 9;
  carbonDeposit(dp, top_fubar, &v_top_fubar, 0);

  // Schedule a posedge clock and check that top.out was updated.
  if (schedulePosedgeClock(dp, top_clock, &the_time)) return 0;
  carbonExamine(dp, top_out, &v_top_out, 0);
  
  fprintf(stdout, "Update of top.out: %s", (v_top_out==v_top_fubar) ? "OK" : "ERROR" );

  // Change the value of top.fubar and assert top.cond2, meaning the $stop will trigger.
  v_top_fubar = 11;
  carbonDeposit(dp, top_fubar, &v_top_fubar, 0);
  carbonDepositWord(dp, top_cond2, 1, 0, 0);

  if (schedulePosedgeClock(dp, top_clock, &the_time)) return 0;

  carbonDestroy(&dp);
  fprintf(stdout, "\nEND: bug3441_a \n");
  return 0;
}
