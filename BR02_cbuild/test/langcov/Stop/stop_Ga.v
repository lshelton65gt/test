// basedesign for testing of execution flow and schedules related to $stop and $finish
// in this case the use of $stop on different branches of if stmts divided among
// separate modules.
// file: stop_Ga.v,

module top (clk1, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z );

   input clk1;
   input [3:0]    B, C,     F,  H,  J,  L,  N,  P,  R,  T, V,  X;
   output [3:0] A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z; 
   reg [3:0]    A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z;

   wire [3:0] Aw,     Dw, Ew,  Gw,  Iw,  Kw,  Mw,  Ow,  Qw,  Sw,  Uw, Ww,  Yw, Zw; 

   initial
     begin
	A = 15;
	D = 15;
	E = 15;
	G = 15;
	I = 15;
	K = 15;
	M = 15;
	O = 15;
	Q = 15;
	S = 15;
	U = 15;
	W = 15;
	Y = 15;
	Z = 15;
     end


   m1 i1 (clk1, Aw, B, C, Dw, Ew, F, Gw, H, Iw, J, Kw);
   m2 i2 (clk1,     B,                                 L, Mw, N, Ow, P, Qw, R, Sw );
   m3 i3 (clk1,        C,                                                          T, Uw, V, Ww, X, Yw, Zw );

endmodule

module m1(clk1, A, B, C, D, E, F, G, H, I, J, K);

   input clk1;
   input [3:0]    B, C,     F,  H,  J;
   output [3:0] A,     D, E,  G,  I,  K; 
   reg [3:0] 	A,     D, E,  G,  I,  K; 
   
   always @(posedge clk1)
     begin
                    //     def   uses                     cflive_set
        A = B + C;  //     A    [B, C]                    [A]
        D = A + C;  //     D    [A, C]                    [A,D]
        $stop;      //     $cfn [A,D]                     [$cfn]   // redefines the live set
        E = D + C;  //     E    [D,C,$cfn]                [$cfn,E]
        G = F;      //     G    [F, $cfn]                 [$cfn,E,G]
        if (A[1])   //                                    [$cfn,E,G,I,K] // union of last cflive_list from each branch
                    //     I    [A,H,$cfn]
                    //     K    [A,J,$cfn]
	  
          I = H;    //     I    [H,$cfn]                  [$cfn,E,G,I]
        else
          K = J;    //     K    [J,$cfn]                  [$cfn,E,G,K]
	
     end
endmodule
module m2(clk1, B,  L, M, N, O, P, Q, R, S );

   input clk1;
   input [3:0]    B, L,  N,  P,  R;
   output [3:0]        M,  O,  Q,  S;
   reg [3:0]           M,  O,  Q,  S;

   always @(posedge clk1)
     begin
                    //     def   uses                     cflive_set
	
        if (B[0])   //                                    [$cfn,E,G,I,K,M,O,Q,S]
                    //     M    [L,B,$cfn]
                    //     O    [N,B,$cfn]
                    //     Q    [P,B,$cfn]
                    //     S    [R,B,$cfn]
	   
          begin
             M = L; //     M    [L,$cfn]                  [$cfn,E,G,I,K,M]
             O = N; //     O    [N,$cfn]                  [$cfn,E,G,I,K,M,O]
          end
        else
          begin
             Q = P; //     O    [P,$cfn]                  [$cfn,E,G,I,K,Q]
             S = R; //     S    [R,$cfn]                  [$cfn,E,G,I,K,Q,S]
          end

     end
endmodule
module m3(clk1,  C,                                                 T, U, V, W, X, Y, Z );

   input clk1;
   input [3:0]    C,                                    T, V,  X;
   output [3:0]                                           U, W,  Y, Z; 
   reg [3:0]                                              U, W,  Y, Z; 

      always @(posedge clk1)
     begin
                    //     def   uses                     cflive_set
        // in the following If stmt the $cfn is def'ed by the If stmt so the cflive_list only needs to contain $cfn
        if (C[2])   //                                    [$cfn]
	            //     U     [T,C,$cfn]                   
	            //     W     [V,C,$cfn] 
	            //     Y     [X,C,$cfn]
	            //     $cfn  [C,E,G,I,K,M,O,Q,S,T,$cfn]
          begin
             U = T; //     U     [T,$cfn]                [$cfn,E,G,I,K,M,O,Q,S,U]
             $stop; //     $cfn  [E,G,I,K,M,O,Q,S,U,$cfn]  [$cfn]
             Y = X; //     Y     [X,$cfn]                [$cfn,Y]
          end
        else
          begin
             W = V; //     W     [V,$cfn]                [$cfn,E,G,I,K,M,O,Q,S,W]
          end

        Z = Y;     //      Z     [Y,$cfn]                  [$cfn,Z]
        $stop;     //      $cfn  [U,W,Y,Z,$cfn]            [$cfn]

     end

endmodule // top
