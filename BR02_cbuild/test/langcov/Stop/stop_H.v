// basedesign for testing of execution flow and schedules related to $stop and $finish
// in this case the use of $stop on different branches of a case statement stop_H.v

module top (clk1, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z );

   input clk1;
   input [2:0]    B, C,     F,  H,  J,  L,  N,  P,  R,  T, V,  X;
   output [2:0] A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z; 
   reg [2:0]    A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z; 

// define DDUMP for help when generating a goldfile, comment it out otherwise
//`define DDUMP
`ifdef DDUMP
   task dumpData;
      input [31:0] line;
      begin
	 $display ("$stop called from file stop_H.v, line %0d, in scope: _, at time: %0d", line, $time);
	 $display (" A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z");
	 $display ("%0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d",
		   A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z);
      end
   endtask
`endif

   initial
     begin
	A = 15;
	D = 15;
	E = 15;
	G = 15;
	I = 15;
	K = 15;
	M = 15;
	O = 15;
	Q = 15;
	S = 15;
	U = 15;
	W = 15;
	Y = 15;
	Z = 15;
     end

   always @(posedge clk1)
     begin
				//  def   uses                     cflive_set
        A = B + C;		//    A  [B, C]                    [A]
        case (A)		//                                 [$cfn,A,D]
	                        //  $cfn [A]
	                        //    D  [A,C]
	  3'b000: D = A + C;	//    D  [A, C]                    [A,D]
	  3'b001: D = A - C;	//    D  [A, C]                    [A,D]
	  default:
	     begin
`ifdef DDUMP
		dumpData(57);
`else
		$stop;	        //  $cfn [A]                       [$cfn]   // redefines the live set
`endif
	     end
	endcase
        E = D + C;		//    E    [$cfn,C,D]              [$cfn,A,D,E]
`ifdef DDUMP
	dumpData(65);
`else
        $stop;                  //  $cfn   [$cfn,A,D,E]            [$cfn]   // redefines the live set
`endif
        G = F;			//    G    [$cfn,F]                [$cfn,G]
        case (A)    //                                             [$cfn,G,I,K] // union of last cflive_set from each branch
	                        //    I    [$cfn,A,H]
	                        //    K    [$cfn,A,J]
	  3'b111: I = H;        //    I    [$cfn,H]                [$cfn,G,I]
          3'b110: K = J;        //    K    [$cfn,J]                [$cfn,G,K]
	endcase
	
        case (B)                //                                 [$cfn,G,I,K] // union of last cflive_set from each branch and starting since not fullcase
	                        //  $cfn  [$cfn,B,G,I,K]
	  3'b000:
	     begin
`ifdef DDUMP
		dumpData(82);
`else
		$stop;	        //  $cfn  [$cfn,G,I,K]             [$cfn]   // redefines the live set
`endif
	     end
	endcase

	case (D)		//                                 [$cfn,GIKMOQSUWYZ] // union of last cflive_set from each branch,
	                                                                              // no need to union in the starting set since this is fullcase
	                        //  $cfn   [$cfn,D,G,I,K,T]
	                        //    M    [$cfn,D,L]
	                        //    O    [$cfn,D,N]
	                        //    Q    [$cfn,D,P]
	                        //    S    [$cfn,D,R]
	                        //    U    [$cfn,D,T]
	                        //    W    [$cfn,D,V]
	                        //    Y    [$cfn,D,X]
	                        //    Z    [$cfn,D,Y]
	  3'b000:
            begin
               M = L;	        //    M    [$cfn,L]                  [$cfn,G,I,K,M]
               O = N;           //    O    [$cfn,N]                  [$cfn,G,I,K,M,O]
            end
	  3'b001:
            begin
               Q = P;           //     Q    [$cfn,P]                  [$cfn,G,I,K,Q]
               S = R;	        //     S    [$cfn,R]                  [$cfn,G,I,K,Q,S]
            end
	  3'b010:
	    begin
	       U = T;           //     U     [$cfn,T]                [$cfn,G,I,K,U]
`ifdef DDUMP
	       dumpData(114);
`else
               $stop;           //     $cfn  [$cfn,G,I,K,U]          [$cfn]
`endif
               Y = X;           //     Y     [$cfn,X]                [$cfn,Y]
	    end
	  3'b011:
            begin
               W = V;           //     W     [$cfn,V]                [$cfn,G,I,K,W]
            end
	  3'b100:
	     begin
`ifdef DDUMP
		dumpData(127);
`else
		$stop;          //      $cfn  [$cfn,G,I,K]         [$cfn]	     
`endif
	     end
	  3'b101:
            Z = Y;              //      Z     [$cfn,Y]               [$cfn,G,I,K,Z]
	  3'b110:
	    Z = Y+1;		//      Z     [$cfn,Y]               [$cfn,G,I,K,Z]
	  3'b111:
	    Z = Y+2;		//      Z     [$cfn,Y]               [$cfn,G,I,K,Z]
        endcase
        A = B - A;              //     A    [$cfn,A,B]               [$cfn,GIKMOQSUWYZ,A]
`ifdef DDUMP
	dumpData(141);
`else
	$stop;			//   $cfn   [$cfn,AGIKMOQSUWYZ]      [$cfn]
`endif
	Z = Y;			//     Z    [$cfn,Y]                 [$cfn,Y]
     end
endmodule // top


`ifdef DDUMP
// if DDUMP is defined then this becomes the top level module, the equations for
// the input values are equivalent to the equations used by stop_H.2.cxx 
module stop_H_2_cxx(A, D, E, G, I, K, M, O, Q, S, U, W, Y, Z);
   output [3:0] A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z; 
//   reg [3:0] 	A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z;
   integer 	count;

   reg 		clock;
   reg [3:0] 	B, C,     F,  H,  J,  L,  N,  P,  R,  T, V,  X;

   initial
     begin
	clock = 0;
	B = 0;
	C = 1;
	F = 2;
	H = 3;
	J = 4;
	L = 5;
	N = 6;
	P = 7;
	R = 8;
	T = 9;
	V = 10;
	X = 11;
	for (count = 0; count < 10; count = count + 1)
	   begin
	      #5 clock = ! clock;
	      B = (B + 1) & 4'hf;
	      C = (C - 2) & 4'hf;
	      F = (F + C) & 4'hf;
	      H = (H * 2) & 4'hf;
	      J = (H ^ 5) & 4'hf;
	      L = (L + H) & 4'hf;
	      N = (N ^ L) & 4'hf;
	      P = (P + B) & 4'hf;
	      R = (R - B) & 4'hf;
	      T = (T ^ B) & 4'hf;
	      V = (V ^ 15) & 4'hf;
	      X = (~X) & 4'hf;
	      #5 clock = ! clock;
	   end
     end


   top i1 (clock, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z );
endmodule
`endif
