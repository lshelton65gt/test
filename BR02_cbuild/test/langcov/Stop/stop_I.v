// basedesign for testing of execution flow and schedules related to $stop and $finish
// in this case the use of $stop within a for loop

module top (clk1, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z );

   input clk1;
   input [2:0]    B, C,     F,  H,  J,  L,  N,  P,  R,  T, V,  X;
   output [2:0] A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z; 
   reg [2:0]    A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z;
   integer 	loopI;
// define DDUMP for help when generating a goldfile, comment it out otherwise
//`define DDUMP
`ifdef DDUMP
   task dumpData;
      input [31:0] line;
      begin
	 $display ("$stop called from file stop_I.v, line %0d, in scope: _, at time: %0d", line, $time);
	 $display (" A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z");
	 $display ("%0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d",
		   A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z);
      end
   endtask
`endif

   
   initial
     begin
	A = 15;
	D = 15;
	E = 15;
	G = 15;
	I = 15;
	K = 15;
	M = 15;
	O = 15;
	Q = 15;
	S = 15;
	U = 15;
	W = 15;
	Y = 15;
	Z = 15;
     end

   always @(posedge clk1)
     begin

	A = 0;
	for(loopI = 0; loopI < 2; loopI = loopI + 1) begin
	   A = A + B[loopI];
	end 

`ifdef DDUMP
	dumpData(55);
`else
	$stop;
`endif

	for(loopI = 0; loopI < 2; loopI = loopI + 1) begin
	   D = D + C[loopI];
`ifdef DDUMP
	dumpData(63);
`else
	   $stop;
`endif
	end
	E = F;

`ifdef DDUMP
	dumpData(71);
`else
	$stop;
`endif
     end
endmodule // top

`ifdef DDUMP
// if DDUMP is defined then this becomes the top level module, the equations for
// the input values are equivalent to the equations used by stop_I.2.cxx 
module stop_I_2_cxx(A, D, E, G, I, K, M, O, Q, S, U, W, Y, Z);
   output [3:0] A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z; 
//   reg [3:0] 	A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z;
   integer 	count;

   reg 		clock;
   reg [3:0] 	B, C,     F,  H,  J,  L,  N,  P,  R,  T, V,  X;

   initial
     begin
	clock = 0;
	B = 0;
	C = 1;
	F = 2;
	H = 3;
	J = 4;
	L = 5;
	N = 6;
	P = 7;
	R = 8;
	T = 9;
	V = 10;
	X = 11;
	for (count = 0; count < 10; count = count + 1)
	   begin
	      #5 clock = ! clock;
	      B = (B + 1) & 4'hf;
	      C = (C - 2) & 4'hf;
	      F = (F + C) & 4'hf;
	      H = (H * 2) & 4'hf;
	      J = (H ^ 5) & 4'hf;
	      L = (L + H) & 4'hf;
	      N = (N ^ L) & 4'hf;
	      P = (P + B) & 4'hf;
	      R = (R - B) & 4'hf;
	      T = (T ^ B) & 4'hf;
	      V = (V ^ 15) & 4'hf;
	      X = (~X) & 4'hf;
	      #5 clock = ! clock;
	   end
     end


   top i1 (clock, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z );
endmodule
`endif
