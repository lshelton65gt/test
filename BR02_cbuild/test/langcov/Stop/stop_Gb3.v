// basedesign for testing of execution flow and schedules related to $stop and $finish
// in this case the use of $stop on different branches of if stmts in diff
// modules, if flattened then this design had a code generation error
// file: stop_Gb3.v

module top (clk1, clk2, A );

   input clk1, clk2;
   output  A;
   reg  	A;
   wire  	Aw1;
   wire  	Aw2;

   always @(Aw1 or Aw2)
     A = Aw1 | Aw2;

   m1 i1 (clk1, Aw1);
   m1 i2 (clk2, Aw2);

endmodule

module m1(clk, out);
   input clk;
   output  out;
   reg  out;
   
   always @(posedge clk)
     begin
	$stop;
	out = 1'b1;
     end
endmodule
