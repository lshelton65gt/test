// testing of user-defined stop and finish callback functions

#include "libstop_A.h"   /* generated header file */
#include <cstdio>
#include <cassert>
#include <cstring>


/* define two variants of callback functions */

/* define a callback function for stop and finish */
void myControlCBFunctionA (CarbonObjectID* ,
                          CarbonControlType callbackType,
                          CarbonClientData data,
                          const char* verilogFilename,
                          int verilogLineNumber)
{
  fprintf(stdout, "in callback function A instance%d in  %s at %s:%d\n",
          *((CarbonUInt32*)data),
          ((callbackType == eCarbonStop ) ? "$stop" : "$finish"),
          verilogFilename, verilogLineNumber);
}

/* define a callback function for stop and finish */
void myControlCBFunctionB (CarbonObjectID* ,
                          CarbonControlType callbackType,
                          CarbonClientData data,
                          const char* verilogFilename,
                          int verilogLineNumber)
{
  fprintf(stdout, "in callback function B instance%d in  %s at %s:%d\n",
          *((CarbonUInt32*)data),
          ((callbackType == eCarbonStop ) ? "$stop" : "$finish"),
          verilogFilename, verilogLineNumber);
}


// return true if execution should terminate
bool printStatusReport( CarbonStatus status )
{
  if ( status == eCarbon_ERROR )
  {
    fprintf(stdout, "Error seen, continue anyway\n");
  }
  if ( status == eCarbon_STOP )
  {
    fprintf(stdout, "Stop seen, continue anyway\n");
  }
  if ( status == eCarbon_FINISH )
  {
    fprintf(stdout, "Finish seen, exiting\n");
    return true;
  }
  return false;
}


int main()
{
  int cycles;

  // the following line makes the stdout unbuffered so that it will
  // appear in the proper order relative to stderr msgs.  this is only
  // needed for simplify debugging of future problems
  setvbuf(stdout, NULL, _IONBF, 0);

  /* Instantiate a model */
  CarbonObjectID *dp =
    carbon_stop_A_create(eCarbonIODB, eCarbon_NoFlags);

  fprintf(stdout, "START: stop_A_2 \n");
  /* setup callback */
  // now add a bunch of callbacks, we use the same function but unique id via the user-data
  CarbonUInt32 ONE = 1;
  CarbonUInt32 TWO = 2;
  CarbonUInt32 THREE = 3;
  CarbonUInt32 FOUR = 4;
  CarbonUInt32 FIVE = 5;
  CarbonRegisteredControlCBDataID* IDForStop1 = carbonAdminAddControlCB(dp, myControlCBFunctionB, &ONE, eCarbonStop);
  CarbonRegisteredControlCBDataID* IDForFinish1 = carbonAdminAddControlCB(dp, myControlCBFunctionA, &ONE, eCarbonFinish);
  CarbonRegisteredControlCBDataID* IDForStop2 = carbonAdminAddControlCB(dp, myControlCBFunctionA, &TWO, eCarbonStop);
  CarbonRegisteredControlCBDataID* IDForStop3 = carbonAdminAddControlCB(dp, myControlCBFunctionB, &THREE, eCarbonStop);
  CarbonRegisteredControlCBDataID* IDForStop4 = carbonAdminAddControlCB(dp, myControlCBFunctionB, &FOUR, eCarbonStop);
  CarbonRegisteredControlCBDataID* IDForStop5 = carbonAdminAddControlCB(dp, myControlCBFunctionB, &FIVE, eCarbonStop);
  CarbonRegisteredControlCBDataID* IDForFinish2 = carbonAdminAddControlCB(dp, myControlCBFunctionB, &TWO, eCarbonFinish);
  
  if ( ( IDForStop1 == NULL ) ||
       ( IDForStop2 == NULL ) ||
       ( IDForStop3 == NULL ) ||
       ( IDForStop4 == NULL ) ||
       ( IDForStop5 == NULL ) ||
       ( IDForFinish1 == NULL ) ||
       ( IDForFinish2 == NULL )   ) {
    fprintf(stdout, "Error: unable to register a callback function \n");
  }



  /* Get handles to all the I/O nets */
  CarbonNetID* clk1 = carbonFindNet(dp, "top.clk1");

  CarbonStatus status;
  /* we need to simulate at least 7 posedges to be sure that things operate
   * correctly */
  for ( cycles = 0; cycles <= 7; ++cycles )
  {
    fprintf(stdout, "\nstart of cycle %d\n",cycles);
    carbonDepositWord(dp, clk1, 0, 0, 0);
    status = carbonSchedule(dp, (10 * cycles) + 0);
    if ( printStatusReport(status) ) break;

    carbonDepositWord(dp, clk1, 1, 0, 0);
    status = carbonSchedule(dp, (10 * cycles) + 5);
    if ( printStatusReport(status) ) break;

    // at each cycle we remove one of the registered callbacks to
    // make sure that removal also works
    switch (cycles)
    {
    case 1: { carbonAdminRemoveControlCB(dp, &IDForStop5); break; }
    case 2: { carbonAdminRemoveControlCB(dp, &IDForStop1); break; }
    case 3: { carbonAdminRemoveControlCB(dp, &IDForStop3); break; }
    case 4: { carbonAdminRemoveControlCB(dp, &IDForFinish1); break; }
    }
  }

  carbonDestroy(&dp);
  fprintf(stdout, "END: stop_A_2 \n");
  return 0;
}
