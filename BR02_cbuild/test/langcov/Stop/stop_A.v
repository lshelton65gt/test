// testcase to test userdefined and default $stop and $finish callbacks

module top (clk1, q1);
   input clk1;
   output [3:0] q1;
   reg [3:0] 	q1;
   reg [3:0] 	ctr;

   initial
     begin
	q1 = 0;
	ctr = 0;
     end

   always @(posedge clk1)
      begin
	 // we do not stop in first cycle
	 if ( ctr == 1 )
	   $stop(0);		// in second cycle no message about stop
	 else if (ctr > 1)
	   $stop;		// in remaining cycles we get a message
	 q1 = ctr;
	 ctr = ctr + 1;
	 if ( ctr > 5 )
	    $finish(2);		// finish after cycle 5
      end
   

endmodule // top
