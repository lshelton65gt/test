module top(clock, en, cond1, cond2, arg1, arg2, something, fubar, out, out2, out3);
   input clock, en, cond1, cond2;
   input [7:0] arg1, arg2, something, fubar;
   output [7:0] out, out2, out3;
   reg [7:0] 	out, out2, out3;
   reg [31:0] 	min2;

   task min;
      output [31:0] out;
      input [7:0] in1;
      input [7:0] in2;
      reg [31:0] out;

      begin
	 if ( in1 < in2 )
	   out = in1;
	 else
	   out = in2;
      end
   endtask

   // If we split 'dclk' from 'out' (both are in the clock tree),
   // copies of the $stop calls will end up in one or more of the
   // splits. 
   
   reg dclk;
   always @(posedge clock)
     begin: main
	if (cond1) begin
	   min(min2, arg1, arg2);
	   if (min2 >= something) begin
	      out = fubar;
	   end
	end
	if (cond2) begin
	   $stop;
	end
	if (cond1) begin
           dclk <= ~dclk | out[0];
	end
	if (cond2) begin
	   $stop;
	end
     end

   // Since 'dclk' is used to compute 'clk1' and both 'clk1' and 'out'
   // are used to compute 'clk2', we guarantee that a split for 'dclk'
   // will run before the split for 'out'.

   // This forced order ensures that if the above sequential block is
   // split, the $stop included with the 'dclk' computation will see
   // that 'out' has not yet been updated.

   // See the myControlCBFunction callback in bug3441_a.cxx.
   
   wire clk1 = dclk & en;
   wire clk2 = clk1 ^ out[0];

   always @ (posedge clk1)
     out2 <= arg1;

   always @ (posedge clk2)
     out3 <= arg1;

endmodule
