// testing of user-defined stop and finish callback functions
// we use fprintf to stderr so that error messages and normal output
// will be in sync

#include "libstop_G.h"   /* generated header file */
#include <cstdio>
#include <cassert>
#include <cstring>


/* define a callback function for stop and finish */
void myControlCBFunction (CarbonObjectID* carbonObject,
                          CarbonControlType callbackType,
                          CarbonClientData /*data*/,
                          const char* /*verilogFilename*/,
                          int /*verilogLineNumber*/)
{

  CarbonNetID* A = carbonFindNet(carbonObject, "top.A");
  CarbonNetID* B = carbonFindNet(carbonObject, "top.B");
  CarbonNetID* C = carbonFindNet(carbonObject, "top.C");
  CarbonNetID* D = carbonFindNet(carbonObject, "top.D");
  CarbonNetID* E = carbonFindNet(carbonObject, "top.E");
  CarbonNetID* F = carbonFindNet(carbonObject, "top.F");
  CarbonNetID* G = carbonFindNet(carbonObject, "top.G");
  CarbonNetID* H = carbonFindNet(carbonObject, "top.H");
  CarbonNetID* I = carbonFindNet(carbonObject, "top.I");
  CarbonNetID* J = carbonFindNet(carbonObject, "top.J");
  CarbonNetID* K = carbonFindNet(carbonObject, "top.K");
  CarbonNetID* L = carbonFindNet(carbonObject, "top.L");
  CarbonNetID* M = carbonFindNet(carbonObject, "top.M");
  CarbonNetID* N = carbonFindNet(carbonObject, "top.N");
  CarbonNetID* O = carbonFindNet(carbonObject, "top.O");
  CarbonNetID* P = carbonFindNet(carbonObject, "top.P");
  CarbonNetID* Q = carbonFindNet(carbonObject, "top.Q");
  CarbonNetID* R = carbonFindNet(carbonObject, "top.R");
  CarbonNetID* S = carbonFindNet(carbonObject, "top.S");
  CarbonNetID* T = carbonFindNet(carbonObject, "top.T");
  CarbonNetID* U = carbonFindNet(carbonObject, "top.U");
  CarbonNetID* V = carbonFindNet(carbonObject, "top.V");
  CarbonNetID* W = carbonFindNet(carbonObject, "top.W");
  CarbonNetID* X = carbonFindNet(carbonObject, "top.X");
  CarbonNetID* Y = carbonFindNet(carbonObject, "top.Y");
  CarbonNetID* Z = carbonFindNet(carbonObject, "top.Z");

  UInt32 v_A;
  UInt32 v_B;
  UInt32 v_C;
  UInt32 v_D;
  UInt32 v_E;
  UInt32 v_F;
  UInt32 v_G;
  UInt32 v_H;
  UInt32 v_I;
  UInt32 v_J;
  UInt32 v_K;
  UInt32 v_L;
  UInt32 v_M;
  UInt32 v_N;
  UInt32 v_O;
  UInt32 v_P;
  UInt32 v_Q;
  UInt32 v_R;
  UInt32 v_S;
  UInt32 v_T;
  UInt32 v_U;
  UInt32 v_V;
  UInt32 v_W;
  UInt32 v_X;
  UInt32 v_Y;
  UInt32 v_Z;

  carbonExamine(carbonObject, A, &v_A, NULL);
  carbonExamine(carbonObject, B, &v_B, NULL);
  carbonExamine(carbonObject, C, &v_C, NULL);
  carbonExamine(carbonObject, D, &v_D, NULL);
  carbonExamine(carbonObject, E, &v_E, NULL);
  carbonExamine(carbonObject, F, &v_F, NULL);
  carbonExamine(carbonObject, G, &v_G, NULL);
  carbonExamine(carbonObject, H, &v_H, NULL);
  carbonExamine(carbonObject, I, &v_I, NULL);
  carbonExamine(carbonObject, J, &v_J, NULL);
  carbonExamine(carbonObject, K, &v_K, NULL);
  carbonExamine(carbonObject, L, &v_L, NULL);
  carbonExamine(carbonObject, M, &v_M, NULL);
  carbonExamine(carbonObject, N, &v_N, NULL);
  carbonExamine(carbonObject, O, &v_O, NULL);
  carbonExamine(carbonObject, P, &v_P, NULL);
  carbonExamine(carbonObject, Q, &v_Q, NULL);
  carbonExamine(carbonObject, R, &v_R, NULL);
  carbonExamine(carbonObject, S, &v_S, NULL);
  carbonExamine(carbonObject, T, &v_T, NULL);
  carbonExamine(carbonObject, U, &v_U, NULL);
  carbonExamine(carbonObject, V, &v_V, NULL);
  carbonExamine(carbonObject, W, &v_W, NULL);
  carbonExamine(carbonObject, X, &v_X, NULL);
  carbonExamine(carbonObject, Y, &v_Y, NULL);
  carbonExamine(carbonObject, Z, &v_Z, NULL);




  fprintf(stdout, " A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z\n%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n",
          v_A, v_B, v_C, v_D, v_E, v_F, v_G, v_H, v_I, v_J, v_K, v_L, v_M, v_N, v_O, v_P, v_Q, v_R, v_S, v_T, v_U, v_V, v_W, v_X, v_Y, v_Z);
}


// return true if execution should terminate
bool printStatusReport( CarbonStatus status )
{
  if ( status == eCarbon_ERROR )
  {
    fprintf(stdout, "Error seen, continue anyway\n");
  }
  if ( status == eCarbon_STOP )
  {
    fprintf(stdout, "Stop seen, continue anyway\n");
  }
  if ( status == eCarbon_FINISH )
  {
    fprintf(stdout, "Finish seen, exiting\n");
    return true;
  }
  return false;
}


int main()
{
  int cycles;

  // the following line makes the stdout unbuffered so that it will
  // appear in the proper order relative to stderr msgs.  this is only
  // needed for simplify debugging of future problems
  setvbuf(stdout, NULL, _IONBF, 0);

  /* Instantiate a model */
  CarbonObjectID *dp =
    carbon_stop_G_create(eCarbonIODB, eCarbon_NoFlags);

  fprintf(stdout, "START: stop_G_2 \n");
  /* setup callback */
  // now add a bunch of callbacks, we use the same function but unique id via the user-data
  /* CarbonRegisteredControlCBDataID* IDForStop1 =*/ carbonAdminAddControlCB(dp, myControlCBFunction, (CarbonClientData)1, eCarbonStop);
  /* CarbonRegisteredControlCBDataID* IDForFinish1 =*/ carbonAdminAddControlCB(dp, myControlCBFunction, (CarbonClientData)1, eCarbonFinish);
  
  /* Get handles to all the I/O nets */
  CarbonNetID* clk1 = carbonFindNet(dp, "top.clk1");
  CarbonNetID* B = carbonFindNet(dp, "top.B");
  CarbonNetID* C = carbonFindNet(dp, "top.C");
  CarbonNetID* F = carbonFindNet(dp, "top.F");
  CarbonNetID* H = carbonFindNet(dp, "top.H");
  CarbonNetID* J = carbonFindNet(dp, "top.J");
  CarbonNetID* L = carbonFindNet(dp, "top.L");
  CarbonNetID* N = carbonFindNet(dp, "top.N");
  CarbonNetID* P = carbonFindNet(dp, "top.P");
  CarbonNetID* R = carbonFindNet(dp, "top.R");
  CarbonNetID* T = carbonFindNet(dp, "top.T");
  CarbonNetID* V = carbonFindNet(dp, "top.V");
  CarbonNetID* X = carbonFindNet(dp, "top.X");
  UInt32 v_B = 0;
  UInt32 v_C = 1;
  UInt32 v_F = 2;
  UInt32 v_H = 3;
  UInt32 v_J = 4;
  UInt32 v_L = 5;
  UInt32 v_N = 6;
  UInt32 v_P = 7;
  UInt32 v_R = 8;
  UInt32 v_T = 9;
  UInt32 v_V = 10;
  UInt32 v_X = 11;


  CarbonStatus status;
  /* we need to simulate at least 7 posedges to be sure that things operate
   * correctly */
  for ( cycles = 0; cycles <= 7; ++cycles )
  {
    fprintf(stdout, "\nstart of cycle %d\n",cycles);

    // the following is a random pattern of changes to the input values
    v_B = (v_B + 1) & 0xF;
    v_C = (v_C - 2) & 0xF;
    v_F = (v_F + v_C) & 0xF;
    v_H = (v_H * 2) & 0xF;
    v_J = (v_H ^ 5) & 0xF;
    v_L = (v_L + v_H) & 0xF;
    v_N = (v_N ^ v_L) & 0xF;
    v_P = (v_P + v_B) & 0xF;
    v_R = (v_R - v_B) & 0xF;
    v_T = (v_T ^ v_B) & 0xF;
    v_V = (v_V ^ 15) & 0xF;
    v_X = (~v_X) & 0xF;
    carbonDepositWord(dp, B, v_B, 0, 0);
    carbonDepositWord(dp, C, v_C, 0, 0);
    carbonDepositWord(dp, F, v_F, 0, 0);
    carbonDepositWord(dp, H, v_H, 0, 0);
    carbonDepositWord(dp, J, v_J, 0, 0);
    carbonDepositWord(dp, L, v_L, 0, 0);
    carbonDepositWord(dp, N, v_N, 0, 0);
    carbonDepositWord(dp, P, v_P, 0, 0);
    carbonDepositWord(dp, R, v_R, 0, 0);
    carbonDepositWord(dp, T, v_T, 0, 0);
    carbonDepositWord(dp, V, v_V, 0, 0);
    carbonDepositWord(dp, X, v_X, 0, 0);
    carbonDepositWord(dp, clk1, 0, 0, 0);
    status = carbonSchedule(dp, (10 * cycles) + 0);
    if ( printStatusReport(status) ) break;

    carbonDepositWord(dp, clk1, 1, 0, 0);
    status = carbonSchedule(dp, (10 * cycles) + 5);
    if ( printStatusReport(status) ) break;
  }

  carbonDestroy(&dp);
  fprintf(stdout, "END: stop_G_2 \n");
  return 0;
}
