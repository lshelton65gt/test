// simple problem that had a cbuild problem at one time.
// related to fact that O1 is not an output.

module top(O2);
   output[2:0] O2;
   reg [2:0] O2;
   reg [2:0] O1;
   integer    i;
   

   initial begin
      for(i = 0; i < 1; i = i + 1) begin
	 O1 = i[2:0];
      end
//      i = 0;
//      O1 = i[2:0];
//      $finish;
      O2 = O1;
   end
endmodule
     
