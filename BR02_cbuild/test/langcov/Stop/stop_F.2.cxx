// testing of user-defined stop and finish callback functions
// we use fprintf to stderr so that error messages and normal output
// will be in sync

#include "libstop_F.h"   /* generated header file */
#include <cstdio>
#include <cassert>
#include <cstring>


/* define a callback function for stop and finish */
void myControlCBFunction (CarbonObjectID* carbonObject,
                          CarbonControlType callbackType,
                          CarbonClientData /*data*/,
                          const char* verilogFilename,
                          int verilogLineNumber)
{

  CarbonNetID* ctr = carbonFindNet(carbonObject, "top.ctr");
  CarbonNetID* q1 = carbonFindNet(carbonObject, "top.q1");
  CarbonNetID* q2 = carbonFindNet(carbonObject, "top.q2");
  CarbonNetID* q3 = carbonFindNet(carbonObject, "top.q3");
  CarbonNetID* fin = carbonFindNet(carbonObject, "top.fin");
  UInt32 v_ctr;
  UInt32 v_q1;
  UInt32 v_q2;
  UInt32 v_q3;
  UInt32 v_fin;
  carbonExamine(carbonObject, ctr, &v_ctr, NULL);
  carbonExamine(carbonObject, q1, &v_q1, NULL);
  carbonExamine(carbonObject, q2, &v_q2, NULL);
  carbonExamine(carbonObject, q3, &v_q3, NULL);
  carbonExamine(carbonObject, fin, &v_fin, NULL);
  bool isStop = (callbackType == eCarbonStop);

  fprintf(stdout, "%s called,(%s:%d) ctr: %d, q1: %d, q2: %d, q3: %d, fin: %d \n",
          ( isStop ? "$stop" : "$finish"),
          verilogFilename, verilogLineNumber,
          v_ctr, v_q1, v_q2, v_q3, v_fin
          );
}


// return true if execution should terminate
bool printStatusReport( CarbonStatus status )
{
  if ( status == eCarbon_ERROR )
  {
    fprintf(stdout, "Error seen, continue anyway\n");
  }
  if ( status == eCarbon_STOP )
  {
    fprintf(stdout, "Stop seen, continue anyway\n");
  }
  if ( status == eCarbon_FINISH )
  {
    fprintf(stdout, "Finish seen, exiting\n");
    return true;
  }
  return false;
}


int main()
{
  int cycles;

  // the following line makes the stdout unbuffered so that it will
  // appear in the proper order relative to stderr msgs.  this is only
  // needed for simplify debugging of future problems
  setvbuf(stdout, NULL, _IONBF, 0);

  /* Instantiate a model */
  CarbonObjectID *dp =
    carbon_stop_F_create(eCarbonIODB, eCarbon_NoFlags);

  fprintf(stdout, "START: stop_F_2 \n");
  /* setup callback */
  // now add a bunch of callbacks, we use the same function but unique id via the user-data
  /*CarbonRegisteredControlCBDataID* IDForStop1 =*/ carbonAdminAddControlCB(dp, myControlCBFunction, (CarbonClientData)1, eCarbonStop);
  /*CarbonRegisteredControlCBDataID* IDForFinish1 =*/ carbonAdminAddControlCB(dp, myControlCBFunction, (CarbonClientData)1, eCarbonFinish);
  
  /* Get handles to all the I/O nets */
  CarbonNetID* clk1 = carbonFindNet(dp, "top.clk1");

  CarbonStatus status;
  /* we need to simulate at least 7 posedges to be sure that things operate
   * correctly */
  for ( cycles = 0; cycles <= 7; ++cycles )
  {
    fprintf(stdout, "\nstart of cycle %d\n",cycles);
    carbonDepositWord(dp, clk1, 0, 0, 0);
    status = carbonSchedule(dp, (10 * cycles) + 0);
    if ( printStatusReport(status) ) break;

    carbonDepositWord(dp, clk1, 1, 0, 0);
    status = carbonSchedule(dp, (10 * cycles) + 5);
    if ( printStatusReport(status) ) break;
  }

  carbonDestroy(&dp);
  fprintf(stdout, "END: stop_F_2 \n");
  return 0;
}
