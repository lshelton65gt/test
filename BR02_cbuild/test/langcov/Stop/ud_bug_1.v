// last mod: original file
// filename: test/langcov/Stop/bug_1.v
// Description:  This test pointed out a bug in cbuild during development of
// stop/finish support


module bug1 ( clk, addr, out );
   input clk;
   input addr;
   output [2:0] out;

   reg [2:0] 	a [2:0];
   reg [2:0] 	b [2:0];

   reg [2:0] 	A;
   reg [2:0] 	out;
   
   integer 	i;

   initial
     begin
	b[0] = 0;
	b[1] = 0;
	b[2] = 0;
     end
   task task_stop;
      output [2:0] to1;
      input [2:0] ti1;
      input [2:0] ti2;
      begin
	 if ( ti1[0] )
	   to1 = ti2;
	 else
	   $stop;
      end
   endtask
   
   always @(posedge clk) begin
      i = 0;
      task_stop( A, A, b[i]);
      a[i] = b[i];
      out = a[addr];
   end

   always @(posedge clk) begin
      task_stop( a[i], A, b[i]);
   end

endmodule
