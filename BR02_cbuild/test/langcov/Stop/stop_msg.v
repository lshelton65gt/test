// last mod: Wed Mar  2 08:21:38 2005
// filename: test/langcov/Stop/stop_msg.v
// Description:  This test just prints several $stop msg and then a $finish after a few clk cycles


module stop_msg(clock, out);
   input clock;
   output [7:0] out;


   mid i1(out, clock);
endmodule

module mid (out, clock);
   input clock;
   output [7:0] out;
   reg [7:0] out;
   
   initial
     out = 5;

   always @(posedge clock)
     begin: main
	out = out - 1 ;

	$stop(out);
	if ( out == 0 )
	  $finish(3);
     end
endmodule
