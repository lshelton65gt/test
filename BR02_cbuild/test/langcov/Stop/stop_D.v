// simple testcase used to debug problem with nucleus traversal

module top(in1, in2, out1);
   input    in1;
   input    in2;
   
   output   out1;
   reg 	    out1;

   always @(in2 or in1 ) begin

      if (in1)
	 out1 = in2;
      else
	$finish;
   end

endmodule
