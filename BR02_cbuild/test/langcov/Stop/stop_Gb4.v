// basedesign for testing of execution flow and schedules related to $stop and $finish
// in this case the use of $stop on different branches of if stmts in diff
// modules,  had a code generation error
// file: stop_Gb4.v

module top (clk1, clk2, A );

   input clk1, clk2;
   output  A;
   reg  	A;
   reg  	Aw1;
   reg  	Aw2;

   always @(Aw1 or Aw2)
     A = Aw1 | Aw2;

   always @(posedge clk1)
     begin
	$stop;
	Aw1 = 1'b1;
     end
   always @(posedge clk2)
     begin
	$stop;
	Aw2 = 1'b1;
     end

endmodule

