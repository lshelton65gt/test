// basedesign for testing of execution flow and schedules related to $stop and $finish

module top (clk1, q1, ctr);
   input clk1;
   output [3:0] q1;
   output [3:0]	ctr;
   reg [3:0] 	q1;
   reg [3:0] 	ctr;

   initial
     begin
	ctr = 0;
//	$stop;			// within this stop we should see q1 == 0
	q1 = 15;
	

     end

   always @(posedge clk1)
     begin
	// we do not stop in the first cycle
	ctr = ctr + 1;

	$stop;			// within this stop we should never see q1 == ctr
	
	q1 = ctr;

     end
   

endmodule // top
