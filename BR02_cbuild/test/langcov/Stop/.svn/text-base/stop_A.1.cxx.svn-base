// testing of default stop and finish callback functions

#include "libstop_A.h"   /* generated header file */
#include <cstdio>
#include <cassert>
#include <cstring>

// return true if execution should terminate
bool printStatusReport( CarbonStatus status )
{
  if ( status == eCarbon_ERROR )
  {
    fprintf(stdout, "Error seen, continue anyway\n");
  }
  if ( status == eCarbon_STOP )
  {
    fprintf(stdout, "Stop seen, continue anyway\n");
  }
  if ( status == eCarbon_FINISH )
  {
    fprintf(stdout, "Finish seen, exiting\n");
    return true;
  }
  return false;
}

int main()
{
  // the following line makes the stdout unbuffered so that it will
  // appear in the proper order relative to stderr msgs.  this is only
  // needed for simplify debugging of future problems
  setvbuf(stdout, NULL, _IONBF, 0);

  CarbonStatus status;
  int cycles;
  /* Instantiate a model */
  CarbonObjectID *dp =
    carbon_stop_A_create(eCarbonIODB, eCarbon_NoFlags);

  /* Get handles to all the I/O nets */
  CarbonNetID* clk1 = carbonFindNet(dp, "top.clk1");

  /* we need to see 7 posedges to be sure that things operate
   * correctly */
  for ( cycles = 0; cycles <= 7; ++cycles )
  {
    carbonDepositWord(dp, clk1, 0, 0, 0);
    status = carbonSchedule(dp, (10 * cycles) + 0);
    if ( printStatusReport(status) ) break;

    carbonDepositWord(dp, clk1, 1, 0, 0);
    status = carbonSchedule(dp, (10 * cycles) + 5);
    if ( printStatusReport(status) ) break;
  }

  carbonDestroy(&dp);
  return 0;
}
