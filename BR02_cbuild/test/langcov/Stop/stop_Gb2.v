// basedesign for testing of execution flow and schedules related to $stop and $finish
// in this case the use of $stop on different branches of if stmts in diff
// modules, if flattened then this design had a code generation error
// file: stop_Gb2.v

module top (clk1, A );

   input clk1;
   output [3:0] A;
   reg [3:0] 	A;

   initial
     begin
	A = 15;
     end


   m1 i1 (clk1);
   m1 i2 (clk1 );

endmodule

module m1(clk1);
   input clk1;
   
   always @(posedge clk1)
     begin
	$stop;
        $stop;
     end
endmodule
