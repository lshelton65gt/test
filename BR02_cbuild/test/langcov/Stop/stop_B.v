// basedesign for testing of execution flow and schedules related to $stop and $finish

module top (clk1, q1, q2, fin, ctr);
   input clk1;
   output [3:0] q1;
   output [3:0] q2;
   output 	fin;
   output 	ctr;
   reg [3:0] 	q1;
   reg [3:0] 	q2;
   reg [3:0] 	ctr;
   reg 		fin;

   initial
     begin
	q1 = 0;
	ctr = 0;
	fin = 0;
     end

   always @(posedge clk1)
     begin
	// we do not stop in the first cycle

	if (ctr >= 1)
	  begin
	     $display("before stop#1");
	     $stop(0);
	     $display("after stop#1");
	  end

	q1 = ctr;		// change a value after first stop

	if (ctr >= 1)
	  begin
	     $display("before stop#2, ctr: %d, q2: %d", ctr, q2);
	     $stop(0);
	     $display("after stop#2, ctr: %d, q2: %d", ctr, q2);
	  end
	q2 = ctr;		// change a value after second stop
	ctr = ctr + 1;
	if ( ctr > 5 )
	  begin
	     $finish(2);		// finish after cycle 5
	     fin = 1;		// try to change a value after finish
	  end
     end
   

endmodule // top
