// basedesign for testing of execution flow and schedules related to $stop and $finish
// in this case the use of $stop on different branches of if stmts. file: stop_G.v

module top (clk1, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z );

   input clk1;
   input [3:0]    B, C,     F,  H,  J,  L,  N,  P,  R,  T, V,  X;
   output [3:0] A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z; 
   reg [3:0]    A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z;
// define DDUMP for help when generating a goldfile, comment it out otherwise
//`define DDUMP
`ifdef DDUMP
   task dumpData;
      input [31:0] line;
      begin
	 $display ("$stop called from file stop_G.v, line %0d, in scope: _, at time: %0d", line, $time);
	 $display (" A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z");
	 $display ("%0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d %0d",
		   A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z);
      end
   endtask
`endif
   
   initial
     begin
	A = 15;
	D = 15;
	E = 15;
	G = 15;
	I = 15;
	K = 15;
	M = 15;
	O = 15;
	Q = 15;
	S = 15;
	U = 15;
	W = 15;
	Y = 15;
	Z = 15;
     end

   always @(posedge clk1)
     begin
                    //     def   uses                     cflive_set
        A = B + C;  //     A    [B, C]                    [A]
        D = A + C;  //     D    [A, C]                    [A,D]
`ifdef DDUMP
	dumpData(50);
`else
        $stop;      //     $cfn [A,D]                     [$cfn]   // redefines the live set
`endif
        E = D + C;  //     E    [D,C,$cfn]                [$cfn,E]
        G = F;      //     G    [F, $cfn]                 [$cfn,E,G]
        if (A[1])   //                                    [$cfn,E,G,I,K] // union of last cflive_list from each branch
                    //     I    [A[1],H,$cfn]
                    //     K    [A[1],J,$cfn]
	  
          I = H;    //     I    [H,$cfn]                  [$cfn,E,G,I]
        else
          K = J;    //     K    [J,$cfn]                  [$cfn,E,G,K]

	
        if (B[0])   //                                    [$cfn,E,G,I,K,M,O,Q,S]
                    //     M    [L,B[0],$cfn]
                    //     O    [N,B[0],$cfn]
                    //     Q    [P,B[0],$cfn]
                    //     S    [R,B[0],$cfn]
	   
          begin
             M = L; //     M    [L,$cfn]                  [$cfn,E,G,I,K,M]
             O = N; //     O    [N,$cfn]                  [$cfn,E,G,I,K,M,O]
          end
        else
          begin
             Q = P; //     O    [P,$cfn]                  [$cfn,E,G,I,K,Q]
             S = R; //     S    [R,$cfn]                  [$cfn,E,G,I,K,Q,S]
          end

        // in the following If stmt the $cfn is def'ed by the If stmt so the cflive_list only needs to contain $cfn
        if (C[2])   //                                    [$cfn]
	            //     U     [T,C[2],$cfn]
	            //     W     [V,C[2],$cfn] 
	            //     Y     [X,C[2],$cfn]
	            //     $cfn  [C[2],E,G,I,K,M,O,Q,S,T,$cfn]
          begin
             U = T; //     U     [T,$cfn]                [$cfn,E,G,I,K,M,O,Q,S,U]
`ifdef DDUMP
	dumpData(90);
`else
             $stop; //     $cfn  [E,G,I,K,M,O,Q,S,U,$cfn]  [$cfn]
`endif
             Y = X; //     Y     [X,$cfn]                [$cfn,Y]
          end
        else
          begin
             W = V; //     W     [V,$cfn]                [$cfn,E,G,I,K,M,O,Q,S,W]
          end

        Z = Y;     //      Z     [Y,$cfn]                  [$cfn,Z]
`ifdef DDUMP
	dumpData(103);
`else
        $stop;     //      $cfn  [U,W,Y,Z,$cfn]            [$cfn]
`endif
     end
endmodule // top

`ifdef DDUMP
// if DDUMP is defined then this becomes the top level module, the equations for
// the input values are equivalent to the equations used by stop_G.2.cxx 
module stop_G_2_cxx(A, D, E, G, I, K, M, O, Q, S, U, W, Y, Z);
   output [3:0] A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z; 
//   reg [3:0] 	A,     D, E,  G,  I,  K,  M,  O,  Q,  S,  U, W,  Y, Z;
   integer 	count;

   reg 		clock;
   reg [3:0] 	B, C,     F,  H,  J,  L,  N,  P,  R,  T, V,  X;

   initial
     begin
	clock = 0;
	B = 0;
	C = 1;
	F = 2;
	H = 3;
	J = 4;
	L = 5;
	N = 6;
	P = 7;
	R = 8;
	T = 9;
	V = 10;
	X = 11;
	for (count = 0; count < 10; count = count + 1)
	   begin
	      #5 clock = ! clock;
	      B = (B + 1) & 4'hf;
	      C = (C - 2) & 4'hf;
	      F = (F + C) & 4'hf;
	      H = (H * 2) & 4'hf;
	      J = (H ^ 5) & 4'hf;
	      L = (L + H) & 4'hf;
	      N = (N ^ L) & 4'hf;
	      P = (P + B) & 4'hf;
	      R = (R - B) & 4'hf;
	      T = (T ^ B) & 4'hf;
	      V = (V ^ 15) & 4'hf;
	      X = (~X) & 4'hf;
	      #5 clock = ! clock;
	   end
     end


   top i1 (clock, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z );
endmodule
`endif
