// testing of error messages related to user-defined stop and finish callback functions


#include "libstop_A.h"   /* generated header file */
#include <cstdio>
#include <cassert>
#include <cstring>


/* define a callback function for stop and finish */
void myControlCBFunction (CarbonObjectID* ,
                          CarbonControlType callbackType,
                          CarbonClientData data,
                          const char* verilogFilename,
                          int verilogLineNumber)
{
  fprintf(stdout,
          "in callback function A instance%d in  %s at %s:%d\n",
          *((CarbonUInt32*)data),
          ((callbackType == eCarbonStop ) ? "$stop" : "$finish"),
          verilogFilename, verilogLineNumber);
}


// return true if execution should terminate
bool printStatusReport( CarbonStatus status )
{
  if ( status == eCarbon_ERROR )
  {
    fprintf(stdout, "Error seen, continue anyway\n");
  }
  if ( status == eCarbon_STOP )
  {
    fprintf(stdout, "Stop seen, continue anyway\n");
  }
  if ( status == eCarbon_FINISH )
  {
    fprintf(stdout, "Finish seen, exiting\n");
    return true;
  }
  return false;
}


int main()
{
  int cycles;
  // the following line makes the stdout unbuffered so that it will
  // appear in the proper order relative to stderr msgs.  this is only
  // needed for simplify debugging of future problems
  setvbuf(stdout, NULL, _IONBF, 0);

  /* Instantiate a model */
  CarbonObjectID *dp =
    carbon_stop_A_create(eCarbonIODB, eCarbon_NoFlags);

  fprintf(stdout, "START: stop_A_3 \n");
  /* setup callback */
  CarbonUInt32 ONE = 1;
  fprintf(stdout, "On the next line we expect an error message about an invalid argument.\n" );
  CarbonRegisteredControlCBDataID* IDForInvalid1 = carbonAdminAddControlCB(dp, myControlCBFunction, &ONE, (CarbonControlType)99);
  if ( IDForInvalid1 != NULL ) {
    fprintf(stdout, "Error: an invalid callback function was registered \n");
  }

  fprintf(stdout, "On the next line we expect an error message about the first arg being invalid because it is NULL.\n" );
  CarbonRegisteredControlCBDataID* IDForInvalid2 = carbonAdminAddControlCB((CarbonObjectID*)(NULL), myControlCBFunction, &ONE, eCarbonStop);
  if ( IDForInvalid2 != NULL ) {
    fprintf(stdout, "Error: an invalid callback function was registered \n");
  }

  // the following test cannot be performed at this time,
  // currently there is no check for a valid value for a CarbonObjectID
  //  fprintf(stderr, "On the next line we expect an error message about the first arg being invalid.\n" );
  //  CarbonRegisteredControlCBDataID* IDForInvalid3 = carbonAdminAddControlCB((CarbonObjectID*)(&dp), myControlCBFunction, &ONE, eCarbonStop);

  // now add a bunch of callbacks, we use the same function but 
  CarbonRegisteredControlCBDataID* IDForStop1 = carbonAdminAddControlCB(dp, myControlCBFunction, &ONE, eCarbonStop);
  if ( IDForStop1 == NULL ) {
    fprintf(stdout, "Error: could not register a callback function\n");
  }

  CarbonRegisteredControlCBDataID* IDForFinish1 = carbonAdminAddControlCB(dp, myControlCBFunction, &ONE, eCarbonFinish);
  if ( IDForFinish1 == NULL ) {
    fprintf(stdout, "Error: could not register a callback function\n");
  }

  /* Get handles to all the I/O nets */
  CarbonNetID* clk1 = carbonFindNet(dp, "top.clk1");

  CarbonStatus status;
  /* we need to simulate at least 7 posedges in this testcase so that
   * we can see the error message when we attemt to call
   * carbon_schedule after $finish has been executed. */
  for ( cycles = 0; cycles <= 7; ++cycles )
  {
    fprintf(stdout, "\nstart of cycle %d\n",cycles);
    carbonDepositWord(dp, clk1, 0, 0, 0);
    // we expect an error from the following after a $finish has been simulated
    status = carbonSchedule(dp, (10 * cycles) + 0);
    printStatusReport(status);  // print report but continue even if $finsh seen
    carbonDepositWord(dp, clk1, 1, 0, 0);
    status = carbonSchedule(dp, (10 * cycles) + 5);
    printStatusReport(status);  // print report but continue even if $finsh seen
  }

  carbonDestroy(&dp);
  fprintf(stdout, "END: stop_A_3 \n");
  return 0;
}
