// basedesign for testing of execution flow and schedules related to $stop and $finish
// in this case the use of $stop in combinational blocks
// currently there is a problem in loop analysis that keeps us from having more
// than one $stop in each combinational block.

module top (clk1, q1, q2, q3, fin, ctr);
   input clk1;
   output [3:0] q1, q2, q3;
   output 	fin;
   output [3:0]	ctr;
   reg [3:0] 	q1, q2, q3;
   reg [3:0] 	ctr;
   reg 		fin;

   initial
     begin
	q1 = 15;
	q2 = 15;
	q3 = 15;
	ctr = 0;
	fin = 0;
     end

   always @(posedge clk1)
     begin
	ctr = ctr + 1;

	q1 = ctr;
	q2 = ctr+1;
	q3 = ctr+2;

	if ( ctr > 5 )
	  $finish;
     end

   always @(q1)
     begin
	if ( q1 == 1 )
	  $stop;
//	if ( q1 == 4 )
//	  $stop;
     end


   always @(q2)
     begin
	if ( q2 == 2 )
	  $stop;
//	if ( q2 == 5 )
//	  $stop;
      end

   always @(q3)
     begin
	if ( q3 == 4 )
	  $stop;
//	if ( q3 == 7 )
//	  $stop;
     end

endmodule // top
