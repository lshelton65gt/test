// last mod: Mon Aug  9 12:43:11 2004
// filename: test/langcov/Stop/split1.v
// Description:  This test shows how block splitting/mergeing works with control
// flow nets


module split1 (out1, out2, out3, clk, in1, in2, in3);
   output [3:0] out1, out2, out3;
   input 	clk;
   input  	in1, in2, in3;

   reg [3:0] 	out1,  out3;

//   wire [3:0] 	t1, t2, t3, t4;
   
   L2 i2 (t1, t2, in1, in3);

   L2 i3 (t3, t4, in2, in3);
   
   L2 i4 (t5, t6, in3, in2);

   assign
	 out2 = t5 & t6;
   
   always @ (posedge clk)
     begin
	$stop;
	out1 <= t1 & t2 & t3 & t4;
	$stop;
	out3 = out2;
     end

endmodule

module L2(t1, t3, in1, in3);
   output t1, t3;
   input  in1, in3;

   reg  t1, t3;

   L3 i1 (w1, w3, in1, in3);
   
   always @ (w1 or w3)
     begin
//	$stop;
	t1 = ~w1;
	$stop;
	t3 = ~w3;
     end
endmodule

module L3(out1, out2, in1, in2);
   output out1, out2;
   input  in1, in2;
   reg 	  out1 ;

   assign
	 out2 = ~in2;
   
   always @ (in1)
     begin
	$stop;
	out1 = in1;
	$stop;
     end
   
endmodule
