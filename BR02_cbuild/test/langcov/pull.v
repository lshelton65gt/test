module top(bid, sel, a);
inout bid;
input sel, a;
pullup (bid);
assign bid = sel ? a : 1'bz;
endmodule
