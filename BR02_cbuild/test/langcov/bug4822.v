// last mod: Tue Aug  2 15:47:10 2005
// filename: test/langcov/bug4822.v
// Description:  This test is the testcase from test/bugs/bug4822, combined into
// a single file for easy testing.  Normally this would be run without defining
// CARBON_UDP so that the monitor_udp version is used. (and currently fails
// because the UDP defines logic that contains a rising and falling edge on the
// same input signal.)


module bug4822(out, in1, in2, in3, in4);
   output out;
   input  in1;
   input  in2;
   input  in3;
   input  in4;

`ifdef CARBON_UDP
   monitor_rtl
`else
   monitor_udp
`endif
     m0 (out, in1, in2, in3, in4);

endmodule


`ifdef CARBON_UDP
module monitor_rtl(out, in1, in2, in3, in4);
   output out; reg out;
   input  in1;
   input  in2;
   input  in3;
   input  in4;

   initial out = 1'b0;
   always @(posedge in1)
     out = 1'b1;
   always @(negedge in1)
     out = 1'b1;
   always @(posedge in2)
     out = 1'b1;
   always @(negedge in2)
     out = 1'b1;
   always @(posedge in3)
     out = 1'b1;
   always @(negedge in3)
     out = 1'b1;
   always @(posedge in4)
     out = 1'b1;
   always @(negedge in4)
     out = 1'b1;

endmodule
`else
primitive monitor_udp(out, in1, in2, in3, in4);
   output out; reg out;
   input  in1;
   input  in2;
   input  in3;
   input  in4;

   table
   // in1 in2 in3 in4 | out0 |  out
      p   ?   ?   ?   : ?    :  1;
      n   ?   ?   ?   : ?    :  1;
      ?   p   ?   ?   : ?    :  1;
      ?   n   ?   ?   : ?    :  1;
      ?   ?   p   ?   : ?    :  1;
      ?   ?   n   ?   : ?    :  1;
      ?   ?   ?   p   : ?    :  1;
      ?   ?   ?   n   : ?    :  1;
   endtable
endprimitive
`endif
