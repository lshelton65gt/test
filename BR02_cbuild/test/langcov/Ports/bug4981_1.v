// last mod: Thu Sep 22 13:53:16 2005
// filename: test/langcov/Ports/bug4981_1.v
// Description:  Test the case where bits are split in the formal
// list of a module (wacky ports), but at the instantiation some of the bits are
// not connected.


module bug4981_1(b1,b2,b3,b4, pi1, pi2, pi3, out1, out2, out3, out4, out5, out6);
   input b1, b2, b3, b4;
   input [3:0] pi1;
   input [2:0] pi2;
   input       pi3;
   output [7:0] out1, out2, out3, out4, out5, out6;

   mid i1(b1,      ,    b3,    b4,     pi2, pi3, out1);	// mid in1[2] not driven
   mid i2(      , pi1[2] ,pi1[1],pi1[0], pi2, pi3, out2);// mid in1[3] not driven
   mid i3(pi1[3],        ,pi1[1],pi1[0], pi2, pi3, out3);// mid in1[2] not driven
   mid i4(pi1[3], pi1[2] ,      ,pi1[0], pi2, pi3, out4);// mid in1[1] not driven
   mid i5(pi1[3], pi1[2] ,pi1[1],      , pi2, pi3, out5);// mid in1[0] not driven
   mid i6(      ,        ,      ,      , pi2, pi3, out6);// mid in1[3:0] not driven

endmodule



module mid(in1[3], in1[2], in1[1], in1[0], in2, in3, out);
   input [3:0] in1;
   input [2:0] in2;
   input       in3;
   output [7:0] out;

   assign out = {in1,in2,in3};

endmodule
