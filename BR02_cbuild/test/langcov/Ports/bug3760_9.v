// last mod: Thu Oct  6 20:35:41 2005
// filename: test/langcov/Ports/bug3760_9.v
// Description:  Testing of a UDP with net expressions in the port list.  This
// is invalid verilog, you cannot have net expressions in the port list for UDPs


module bug3760_9(b1,b2,b3,b4, in1,  out1);
   input b1, b2, b3, b4;
   input [3:0] in1;
   output [7:0] out1;

   mid_udp i1(out1, b1,    b2,    b3,    b4);

endmodule



primitive mid_udp(out, in[3], in[2], in[1], in[0]);
   output out; reg out;
   input  [3:0] in;

   table
   // in1 in2 in3 in4 | out0 |  out
      p   ?   ?   ?   : ?    :  1;
      n   ?   ?   ?   : ?    :  1;
      ?   p   ?   ?   : ?    :  1;
      ?   n   ?   ?   : ?    :  1;
      ?   ?   p   ?   : ?    :  1;
      ?   ?   n   ?   : ?    :  1;
      ?   ?   ?   p   : ?    :  1;
      ?   ?   ?   n   : ?    :  1;
   endtable
endprimitive
