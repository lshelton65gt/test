// last mod: Fri Sep 23 15:56:42 2005
// filename: test/langcov/Ports/bug3760_8.v
// Description:  simple expressions in the formal port list of a module, similar
// to bug3760_1.v, but here the width of the formals is greater than a single bit


module bug3760_8(b1,b2,b3,b4, in1, in2, in3, out1, out2);
   input b1, b2, b3, b4;
   input [3:0] in1;
   input [2:0] in2;
   input       in3;
   output [7:0] out1;
   output [7:0] out2;

   mid i1(b1,    /* gap */,      b4,     in2, in3, out1);
   mid i2(in1[3],/* gap */,  in1[0],     in2, in3, out2);

endmodule



module mid(in1[3], in1[2:1], in1[0], in2, in3, out);
   input [3:0] in1;
   input [2:0] in2;
   input       in3;
   output [7:0] out;

   assign out = {in1,in2,in3};
   
endmodule
