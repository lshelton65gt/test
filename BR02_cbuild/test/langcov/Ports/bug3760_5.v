// last mod: Mon Sep 26 07:57:34 2005
// filename: test/langcov/Ports/bug3760_5.v
// Description:  This test has an instance array.  The module declaration has
// nets split into bits in the portlist.

module bug3760_5(clock, in1, out1);
   input clock;
   input [7:0] in1;
   output [7:0] out1;

   DFF iflop[3:0] (clock, {in1[7:4]}, {in1[3:0]}, {out1[7:4]}, {out1[3:0]});
endmodule

module DFF(clk, data[1], data[0], Q[1], Q[0]);
   input clk;
   input [1:0] data;
   output [1:0] Q;
   reg 	  [1:0] Q;

   always @(posedge clk)
     begin: main
	Q = data;
     end
endmodule

