// last mod: Mon Sep 26 17:34:36 2005
// filename: test/langcov/Ports/bug3760_11.v
// Description:  In this test the formals are adjacent but not in a continuous
// direction.  This should give an error message



module bug3760_11(b1,b2,b3,b4, b5, in1, in2, in3, out1, out2);
   input b1, b2, b3, b4, b5;
   input [4:0] in1;
   input [1:0] in2;
   input       in3;
   output [7:0] out1;
   output [7:0] out2;

   mid i1(b1,    b2,    b3,    b4,    b5, in2, in3, out1);
   mid i2(in1[4],in1[3],in1[2],in1[1],in1[0], in2, in3, out2);

endmodule



// in this module the net in1 is out of order in the port list
module mid(in1[3], in1[4], in1[2], in1[5], in1[1], in2, in3, out);
   input [5:1] in1;
   input [1:0] in2;
   input       in3;
   output [7:0] out;

   assign out = {in1,in2,in3};
   
endmodule
