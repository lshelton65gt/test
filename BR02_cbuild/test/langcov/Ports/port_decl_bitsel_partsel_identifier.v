// last mod: Fri Jan 28 11:36:59 2005
// filename: test/langcov/Ports/port_decl_bitsel_partsel_identifier.v
// Description:  This test has a module that uses a bitselect in the
// port declaration (both for input and output) where the expression is not
// equivalent to the full identifier (see also
// port_decl_bitsel_identifier.v )

module top(input clock,
	   input [2:0] in1,
	   output [3:0] out,
	   output out2);

   port_decl_bitsel_partsel_identifier i1(clock, in1, out, out2);

endmodule


    // port declaration list uses bitselect and partselect expresssions (that are NOT equivilant to
    // the full identifier)
module port_decl_bitsel_partsel_identifier(in1[1], {in1[3:2],in1[0]}, out, out[0]);
   input [3:0] in1;
   output [3:0] out;
   reg [3:0] 	out;

   always @(posedge in1[1])
     begin: main
	out = ~ in1;
     end
endmodule
