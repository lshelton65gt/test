// last mod: Mon Sep 26 08:08:57 2005
// filename: test/langcov/Ports/bug3760_6.v
// Description:  This test has an instance array.  The module declaration has
// nets split into bits in the portlist.  Nets are declared in an order reversed
// from those in bug3760_5.v and their order is reversed in the port list.

module bug3760_6(clock, in1, out1);
   input clock;
   input [7:0] in1;
   output [7:0] out1;

   DFF iflop[3:0] (clock, {in1[7:4]}, {in1[3:0]}, {out1[7:4]}, {out1[3:0]});
endmodule

module DFF(clk, data[0], data[1], Q[0], Q[1]);
   input clk;
   input [0:1] data;
   output [0:1] Q;
   reg 	  [0:1] Q;

   always @(posedge clk)
     begin: main
	Q = data;
     end
endmodule

