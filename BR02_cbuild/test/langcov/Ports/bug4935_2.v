// last mod: Fri Sep 30 08:27:43 2005
// filename: test/langcov/Ports/bug4935_2.v
// Description:  This test is from bug4935, where a module is declared with an
// unnamed formal port, and when instantiated there is no actual


module bug4935_2(in1, in2, out1);
   input [7:0] in1, in2;
   output [7:0] out1;

   mid u1(out1, in1, , in2 );	// a middle port is not connected
endmodule

module mid(out1, in1, , in2);	// a middle  port is not declared
   input [7:0] in1, in2;
   output [7:0] out1;

   assign 	out1 = in1 + in2;
endmodule

