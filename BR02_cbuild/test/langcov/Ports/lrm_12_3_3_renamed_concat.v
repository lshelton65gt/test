// last mod: Fri Jan 28 09:25:11 2005
// filename: test/langcov/Ports/lrm_12_3_3_renamed_concat.v
// Description:  This test is based on the example from section 12.3.3 of the
// LRM with only the 'renamed_concat' module


module top(in8, out8_f, out8_h);
   input [2:0] in8;
   output [1:0] out8_f;
   output 	out8_h;

   renamed_concat i5(in8, out8_f, out8_h);

endmodule


module renamed_concat (.a({b,c}), f, .g(h[1]));
   // Names �b�, �c�, �f�, �h� are defined inside the module.
   // Names �a�, �f�, �g� are defined for port connections.
   // Can use named port connections.
   input [1:0]  b;
   input        c;
   output [2:1] f;
   output [1:0] h;

   assign 	{h,f} = (b + c) * 1; // note that h[1] is carry bit, and that h[0]
                                    // is unused;
   
endmodule

