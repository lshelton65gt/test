// last mod: Fri Sep 23 11:16:34 2005
// filename: test/langcov/Ports/nets_split_on_port_2.v
// Description:  This test has many variants of nets split in the module
// declaration, all should be equivalent.


module nets_split_on_port_2(pi0, pi1, pi2, pi3, out3, out4);
   input [3:0] pi0;
   input [3:0] pi1;
   input [2:0] pi2;
   input       pi3;
   output [7:0] out3, out4;

   mid_C i3(pi0[3],pi0[2],pi0[1],pi0[0], pi2[2], pi2[1], pi2[0], pi3, out3[7], out3[6], out3[5], out3[4], out3[3], out3[2], out3[1], out3[0]);

   mid_D i4(pi0[3],pi0[2],pi0[1],pi0[0], pi2[2], pi2[1], pi2[0], pi3, out4[7], out4[6], out4[5], out4[4], out4[3], out4[2], out4[1], out4[0]);   
endmodule


    // in this module the declared nets have more bits than those that appear in the port list.(unused on the left)
module mid_C(in1[3], in1[2], in1[1], in1[0], in2[2], in2[1], in2[0], in3, out[7], out[6], out[5], out[4], out[3], out[2], out[1], out[0]);
   input [4:0] in1;
   input [3:0] in2;
   input       in3;
   output [8:0] out;

   assign out = {in1,in2[2:0],in3};
   
endmodule

    // in this module the declared nets have more bits than those that appear in the port list.(unused on the right)
module mid_D(in1[4], in1[3], in1[2], in1[1], in2[3], in2[2], in2[1], in3, out[8], out[7], out[6], out[5], out[4], out[3], out[2], out[1]);
   input [4:0] in1;
   input [3:0] in2;
   input       in3;
   output [8:0] out;

   assign out[8:1] = {in1[4:1],in2[3:1],in3};
   
endmodule
