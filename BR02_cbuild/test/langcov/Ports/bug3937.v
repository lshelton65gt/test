// last mod: Tue Oct 18 23:23:15 2005
// filename: test/bugs/bug3937/tiny.v
// Description:  This test demonstrates the crash in bug 3937


module tiny (out2);
   output out2;

   mid u1 (.out1(undef1.addr[22:2]));
   
endmodule



module mid (out1); 
output	[22:2]	out1; 

endmodule 

