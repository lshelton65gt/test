// last mod: Fri Sep 23 10:10:41 2005
// filename: test/langcov/Ports/bug3760_4.v
// Description:  This test has a ranged instance and one of the input ports is disconnected


module bug3760_4(clock, in1, out1);
   input clock;
   input [7:0] in1;
   output [7:0] out1;

   DFF iflop[7:0] (.clk(clock), .data(in1), .reset(), .Q(out1));
endmodule

module DFF(clk,data,reset,Q);
   input clk,data, reset;
   output Q;
   reg 	  Q;

   always @(posedge clk)
     begin: main
	Q = data;
     end
endmodule

