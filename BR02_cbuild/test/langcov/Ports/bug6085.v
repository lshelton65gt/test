// last mod: Thu Jun  8 14:37:43 2006
// filename: test/langcov/Ports/bug6085.v
// Description:  This test is the simple test from bug 6085
// requires -2001 switch


module bug6085(  input ck, input [31:0] d, output [31:0] q );
   reg [31:0] q;		// this line is invalid verilog but is supported by many vendors
   always @(posedge ck) begin
      q <= d;
   end
endmodule
