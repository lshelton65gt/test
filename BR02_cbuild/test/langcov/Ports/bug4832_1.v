// last mod: Mon Oct 10 11:29:11 2005
// filename: test/langcov/Ports/bug4832_1.v
// Description:  This test demonstrates how we handle explicit and explicit
// missing connections to ports in in a verilog module.


module bug4832_1(in1, in2, in3, out1, out2, out3, out4, out5, out6, out7, out8,
		 out9, out10, out11, out12);
   input [3:0] in1, in2, in3;
   output [3:0] out1, out2, out3, out4, out5, out6, out7, out8, out9, out10,
		out11, out12;

   mid1 u1(out1, in1, in2, in3); // all ports connected
   mid1 u2(out2, in1, in2);      // implicit missing port connection
   mid1 u3(out3, in1, in2,);     // explicit missing connection for a port

   mid1 u4(.out(out4), .a(in1), .b(in2), .xtra(in3)); // all ports connected
   mid1 u6(.out(out5), .a(in1), .b(in2)); // one port is implicitly not connected
   mid1 u5(.out(out6), .a(in1), .b(in2), .xtra()); // all ports named, one is explicitly connected to nothing

   mid2 u7(out7, in1, in2, in3); // all ports connected
   mid2 u8(out8, in1, in2);      // implicit missing port connection
   mid2 u9(out9, in1, in2,);     // explicit missing connection for a port
   mid2 u10(.out(out10), .a(in1), .b(in2), .c(in3)); // all ports connected
   mid2 u12(.out(out11), .a(in1), .b(in2));       // one port is implicitly not connected
   mid2 u11(.out(out12), .a(in1), .b(in2), .c()); // all ports named, one is explicitly connected to nothing
   
endmodule


module mid1(out, a, b, xtra);
   input  [3:0] a, b, xtra;	// note that xtra is not used
   output [3:0] out;

   assign out = a + b;

endmodule


module mid2(out, a, b, c);
   input  [3:0] a, b, c;
   output [3:0] out;

   assign out = a + b + c;
endmodule


