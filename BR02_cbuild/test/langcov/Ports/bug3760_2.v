// last mod: Fri Sep 23 08:41:41 2005
// filename: test/langcov/Ports/bug3760_2.v
// Description:  This test is from bug3760, where a net appears in the port list
// more than once, and not all bits of the declared input appear in the port list.
// note that this test will not match simulation because carbon will put 'a' on
// the port list as a vector. As if the module was declared as:
// module bug3760_2(a[4:0], out1);

// in order to simulate this to get gold results you must define the macro
// SIMULATE, which provides an alternate top level module that reverses the bits
// so that you will get the same results as carbon.


//`define SIMULATE
`ifdef SIMULATE
module bug3760_2(a,out1);
   input [0:4] a;
   output [2:0] out1;
   s_bug3760_2 inst(a[3],a[2],a[1],out1);
endmodule

module s_bug3760_2(a[1], a[2], a[3], out1);
`else
module bug3760_2(a[1], a[2], a[3], out1);   
`endif   
   input [4:0] a;
   output [2:0] out1;

   assign 	out1 = a[3:1];
   
endmodule
