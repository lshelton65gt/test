// last mod: Fri Jan 28 11:38:09 2005
// filename: test/langcov/Ports/port_decl_bitsel_identifier.v
// Description:  This test has a module that uses a constant bitselect in the
// port declaration (both for input and output) where the expression is
// equivalent to the full identifier (see also
// port_decl_bitsel_identifier_2.v)

module top(input clock,
	   input [3:0] in1,
	   output [3:0] out,
	   output out2);

   port_decl_bitsel_identifier i1(clock, in1, out, out2);

endmodule


    // port declaration list uses bitselect expresssions (that are equivilant to
    // the full identifier)
module port_decl_bitsel_identifier(clock[1], in1, out, out2[0]);
   input [1:1] clock;
   input [3:0] in1;
   output [3:0] out;
   output [0:0] out2;
   reg [3:0] 	out;
   reg [0:0] 	out2;

   always @(posedge clock)
     begin: main
	out = ~ in1;
	out2 = in1[1];
     end
endmodule
