// last mod: Fri Jan 28 09:21:29 2005
// filename: test/langcov/Ports/lrm_12_3_3_complex_ports.v
// Description:  This test is based on the example from section 12.3.3 of the
// LRM, with only the 'complex_ports' module included.


module top(in5, out5);
   input [3:0] in5;
   output [3:0] out5;

   complex_ports i2(in5, out5);
   
endmodule

module complex_ports ({c,d}, .e(f)); // Nets {c,d} receive the first
   // port bits. Name �f� is declared inside the module.
   // Name �e� is defined outside the module.
   // Can�t use named port connections of first port.
   input [2:0] c;
   input       d;
   output [3:0] f;

   assign 	c = f+1;
   assign       d = ^f;
endmodule
