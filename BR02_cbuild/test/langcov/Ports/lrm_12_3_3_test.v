// last mod: Fri Jan 28 08:58:12 2005
// filename: test/langcov/Ports/lrm_12_3_3_test.v
// Description:  This test is based on the example from section 12.3.3 of the
// LRM, with only the 'test' module included.



module top(in1, in2, in3, in4, out1, out2, out3, out4);
   input        [7:0] in1;
   input [7:0] 	      in2;
   input 	      signed [7:0] in3;
   input 	      signed [7:0] in4;
   output [7:0]       out1;
   output [7:0]       out2;
   output 	      signed [7:0] out3;
   output 	      signed [7:0] out4;


   test i1(in1, in2, in3, in4, out1, out2, out3, out4);

endmodule

module test(a,b,c,d,e,f,g,h);
   input [7:0] a; // no explicit declaration - net is unsigned
   input [7:0] b;
   input       signed [7:0] c;
   input       signed [7:0] d; // no explicit net declaration - net is signed
   output [7:0] e; // no explicit declaration - net is unsigned
   output [7:0] f;
   output 	signed [7:0] g;
   output 	signed [7:0] h; // no explicit net declaration - net is signed
   wire 	signed [7:0] b; // port b inherits signed attribute from net decl.
   wire [7:0] 	c; // net c inherits signed attribute from port
   reg 		signed [7:0] f; // port f inherits signed attribute from reg decl.
   reg [7:0] 	g; // reg g inherits signed attribute from port

   initial
     begin
	f = 0;
	g = 0;
     end
   assign e = a + c;
   assign h = c + d;
   always @(posedge a[0])
     f = b + d;
   always @(posedge a[0])
     g = a + b;
endmodule

