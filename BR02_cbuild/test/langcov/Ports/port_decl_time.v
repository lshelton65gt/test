// last mod: Fri Mar 25 10:50:10 2005
// filename: test/langcov/Ports/port_decl_time.v
// Description:  This test was inspired by bug 1866
//   tests a top level port declared as time variable
// if you use -testdriver and aldec to create a gold sim file you must remove
// the #1 delay from main.v

module port_decl_time(clock, in1, in2, out1, out2);
   input clock;
   input [31:0] in1, in2;
   output 	time out1;	// this is legal verilog
   output 	time out2;	// this is legal verilog
//   output 	realtime out3;   // this is not valid verilog, it generates a cheetah message about incompatible definition

   always @(posedge clock)
     begin: main
	out1 = in1 & in2;
	out2 = $time;
//	out3 = $time;
     end
endmodule
