// last mod: Fri Jan 28 09:34:28 2005
// filename: test/langcov/Ports/lrm_12_3_3.v
// Description:  This test is based on the example from section 12.3.3 of the
// LRM, currently we do not run this full testcase, instead see the split out
// versions: lrm_12_3_3_*.v



module top(in1, in2, in3, in4, out1, out2, out3, out4,
	   in5, out5,
	   in6, out6,
	   in7, out7,
	   in8, out8_f, out8_h,
	   in9, out9
	   );
   input [7:0] in1;
   input [7:0] in2;
   input       signed [7:0] in3;
   input       signed [7:0] in4;
   input [3:0] in5;
   input [3:0] in6;
   input [3:0] in7;
   input [2:0] in8;
   input       in9;
   output [7:0] out1;
   output [7:0] out2;
   output 	signed [7:0] out3;
   output 	signed [7:0] out4;
   output [3:0] out5;
   output [3:0] out6;
   output [3:0] out7;
   output [1:0] out8_f;
   output 	out8_h;
   output 	out9;


   test i1(in1, in2, in3, in4, out1, out2, out3, out4);

   complex_ports i2(in5, out5);
   
   split_ports i3(in6, out6);

   same_port i4(out7,in7);

   renamed_concat i5(in8, out8_f, out8_h);

   same_input i6(in9, out9);	// portdirection does not matter.
   
endmodule

module test(a,b,c,d,e,f,g,h);
   input [7:0] a; // no explicit declaration - net is unsigned
   input [7:0] b;
   input       signed [7:0] c;
   input       signed [7:0] d; // no explicit net declaration - net is signed
   output [7:0] e; // no explicit declaration - net is unsigned
   output [7:0] f;
   output 	signed [7:0] g;
   output 	signed [7:0] h; // no explicit net declaration - net is signed
   wire 	signed [7:0] b; // port b inherits signed attribute from net decl.
   wire [7:0] 	c; // net c inherits signed attribute from port
   reg 		signed [7:0] f; // port f inherits signed attribute from reg decl.
   reg [7:0] 	g; // reg g inherits signed attribute from port
endmodule


module complex_ports ({c,d}, .e(f)); // Nets {c,d} receive the first
   // port bits. Name f is declared inside the module.
   // Name e is defined outside the module.
   // Cant use named port connections of first port.
   input [2:0] c;
   input       d;
   output [3:0] f;

   assign 	c = f+1;
   assign       d = ^f;
endmodule

module split_ports (a[7:4], a[3:0]); // First port is upper 4 bits of
   // a.
   // Second port is lower 4 bits of a.
   // Cant use named port connections because
   // of part-select port a.
   inout [7:0] a;
   assign      a[3:0] = a[7:4] + 2;
endmodule
		  
module same_port (.a(i), .b(i)); // Name i is declared inside the
   // module as a inout port. Names a and b are
   // defined for port connections.
   inout [3:0] i;
   assign      i[3:2] = i[1:0] + 1;
endmodule

module renamed_concat (.a({b,c}), f, .g(h[1]));
   // Names b, c, f, h are defined inside the module.
   // Names a, f, g are defined for port connections.
   // Can use named port connections.
   input [1:0]  b;
   input        c;
   output [2:1] f;
   output [1:0] h;

   assign 	{h,f} = (b + c) * 1; // note that h[1] is carry bit, and that h[0]
                                    // is unused;
   
endmodule

module same_input (a,a);
   input a; // This is legal. The inputs are tied together.
endmodule
