// last mod: Fri Jan 28 14:55:06 2005
// filename: test/langcov/Ports/multi_use.v
// Description:  This test uses the same net in multiple positions of the port
// declaration (input-input, input-output, output-output) (idea from bug98)


module top(input i1,
	   input i2,
	   input i3,
	   output o1,
	   output o2,
	   output o3);
   
   multi_use inst1(i1, i2, i3, o1, o2, o3);
   
endmodule

    // two input ports are connected together internally
    // two inout ports are connected together internally
    // two output ports are connected together internally
module multi_use(in1, in1, io1, io1, out1, out1);
   input in1;
   inout io1;
   output out1;

   assign out1 = io1;
   assign io1 = in1;

endmodule


