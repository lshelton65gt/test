// last mod: Fri Sep 30 14:01:54 2005
// filename: test/langcov/Ports/bug4935_3.v
// Description:  This test is from bug4935, where a module is declared with
// multiple adjacent unnamed formal ports, and when instantiated there is no actual


module bug4935_3(in1, in2, out1);
   input [7:0] in1, in2;
   output [7:0] out1;

   mid u1(out1, in1, , , in2 );	// middle ports are not connected
endmodule

module mid(out1, in1, , , in2);	//  middle ports are not declared
   input [7:0] in1, in2;
   output [7:0] out1;

   assign 	out1 = in1 + in2;
endmodule

