// filename: test/langcov/Ports/portsize_1.v
// Description:  This test is intended to check how port connections between
// formal and actuals of different sizes should work.
// The lrm says that port connections are supposed to work like a continuous
// assignment.
//    12.3.9.2(LRM 2005) "Each port connection shall be a continuous
//    assignment of source to sink, where one connected item shall be a
//    signal source and the other shall be a signal sink."
//
// So it is expected that when ports are connected to miss-matching actuals that
// zero or sign extension is performed.

// as of 06/25/08 mti does not seem to get this correct, in particular out10B
// and out10Bwire do not match at posedge clocks.

// gold sim file is from VCS (MTI gets this wrong)

module portsize_1(clock, in1, in6A, in6B, out10A, out10Awire, out10B, out10Bwire);
   input clock;
   input in1;
   input [5:0] in6A;
   input [5:0] in6B;
   output [9:0] out10A;
   output [9:0] out10B;
   output [9:0] out10Awire;
   output [9:0] out10Bwire;

   wire [4:0] 	temp5;
   wire [7:0] 	temp8;

   assign
     temp5 = in6A[4:0] + in1 ;
   assign
     out10Awire = temp5;	// should be equivalent to out10A @posedge clock
   
   assign
      temp8 = in6B + in1;
   assign 
      out10Bwire = temp8;	// should be equivalent to out10B @posedge clock

   sub_small U1(clock, in1, in6A, out10A);

   sub_large U2(clock, in1, in6B, out10B);
   
endmodule

// module with data ports 5 bits
module sub_small (clock, in1, in5, out5);
   input clock;
   input in1;
   input [4:0] in5;
   output [4:0] out5;
   reg [4:0] out5;
   always @(posedge clock)
     begin: main
	out5 = in5 + in1;
	$display("in5: 0x%x, in1: 0x%x, out5: 0x%x", in5, in1, out5);
     end
endmodule 

// module with data ports of 8 and 10 bits
module sub_large(clock, in1, in8, out8);
   input clock;
   input in1;
   input [7:0] in8;
   output [7:0] out8;
   reg [7:0] out8;
   always @(posedge clock)
     begin: main
	out8 = in8 + in1;
	$display("in8: 0x%x, in1: 0x%x, out8: 0x%x", in8, in1, out8);
     end
endmodule 
