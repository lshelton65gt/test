// last mod: Fri Sep 30 08:27:43 2005
// filename: test/langcov/Ports/bug4935_4.v
// Description:  This test is from bug4935, where a module is declared with
// multiple adjacent unnamed formal ports, and when instantiated there is no
// actual, and the actual has a port that is split 


module bug4935_4(in1, in2, out1);
   input [7:0] in1, in2;
   output [7:0] out1;

   mid u1(out1, in1[7], in1[6], in1[5], in1[4], in1[3], in1[2], in1[1], in1[0], , , in2[7], in2[6], in2[5], in2[4], in2[3], in2[2], in2[1], in2[0] );	// middle ports are not connected
endmodule

module mid(out1, in1[7], in1[6], in1[5], in1[4], in1[3], in1[2], in1[1], in1[0], , , in2[7], in2[6], in2[5], in2[4], in2[3], in2[2], in2[1], in2[0]);	//  middle ports are not declared
   input [7:0] in1, in2;
   output [7:0] out1;

   assign 	out1 = in1 + in2;
endmodule

