// last mod: Thu Sep 22 14:33:05 2005
// filename: test/langcov/Ports/bug4981_2.v
// Description:  This test 


module bug4981_1(b1,b2,b3,b4, pi1, pi2, pi3, out1, out2, out3, out4, out5, out6);
   input b1, b2, b3, b4;
   input [3:0] pi1;
   input [2:0] pi2;
   input       pi3;
   output [7:0] out1, out2, out3, out4, out5, out6;

   mid i1(    b1,      b2,     b3,     b4, pi2, pi3, out1[7], out1[6], out1[5], out1[4], out1[3], out1[2], out1[1], out1[0]);
   mid i2(pi1[3],  pi1[2], pi1[1], pi1[0], pi2, pi3, out2[7], out2[6], out2[5], out2[4], out2[3], out2[2], out2[1], out2[0]);
   mid i3(pi1[3],  pi1[2], pi1[1], pi1[0], pi2, pi3, out3[7],       , out3[5], out3[4], out3[3], out3[2], out3[1], out3[0]);

endmodule



module mid(in1[3], in1[2], in1[1], in1[0], in2, in3, out[7], out[6], out[5], out[4], out[3], out[2], out[1], out[0]);

   input [3:0] in1;
   input [2:0] in2;
   input       in3;
   output [7:0] out;

   assign out = {in1,in2,in3};

endmodule
