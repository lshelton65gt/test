// last mod: Mon Oct  3 16:39:54 2005
// filename: test/langcov/Ports/parameter_1.v
// Description:  This test tests for messages printed when port sizes are mismatched


module parameter_1(in1, in2, out1, out2);
   input [7:0] in1, in2;
   output [7:0] out1, out2;

   mid u1(out1, in1);
   mid #3 u2(out2[7:4], in2[3:0]);
   mid #3 u3(out2, in2);
endmodule


module mid(out, in);
   parameter msb=7;
   output [msb:0] out;
   input [msb:0] in;

   assign 	 out = in;

endmodule
