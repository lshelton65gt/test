// last mod: Fri Oct  4 13:50:35 2013
// filename: test/langcov/Ports/bug3760_10.v
// Description:  This test checks what happens when there are part selects in
// the module declaration but the actuals connected to them are incorrect sizes.
// We expect that the parts of the concat will need to be truncated and padded
// as necessary.
// in addition we expect messages from the parser about the connections to the
// first three ports of 'mid'. All 3 all incorrectly sized for both instances,
// thus we expect 6 messages.


module bug3760_10(b2,b3, in1, in2, in3, out1, out2);
   input [1:0] b2;
   input [2:0] b3;
   input [4:0] in1;
   input [2:0] in2;
   input       in3;
   output [7:0] out1;
   output [7:0] out2;

   mid i1(b3,    b2,     in2, in3, out1); // first and third args are too long, second is two short
   mid i2(in1[4:2],in1[1:0], in2, in3, out2);  // first and third args are too long, second is two short

endmodule



module mid(in1[4:3], in1[2:0], in2, in3, out);
   input [4:0] in1;
   input [1:0] in2;
   input       in3;
   output [7:0] out;

   assign out = {in1,in2,in3};
   
endmodule
