// filename: test/langcov/Ports/bug14514_0.v
// Description:  This test contains a signed/unsigned port mismatch
// but there is no size mismatch, see also bug14514_1.v and bug14514_2.v


module bug14514_0(t_umula, t_umulb, t_uresultA, t_smula, t_smulb, t_sresultA, t_uresultB ); // carbon disallowFlattening
   output [31:0] t_uresultA;
   input [31:0] t_umula; 
   input [15:0] t_umulb; 
   output signed [31:0] t_sresultA;
   input signed [15:0] t_smula; 
   input signed [15:0] t_smulb; 
   output [31:0] t_uresultB;

   mid midUnSigned(t_umula[15:0], t_umulb, t_uresultA); // all unsigned args

   // for some reason if you include the following line then the original bug (14514) was not hit, so we leave this commented out
//   mid midSigned(t_smula, t_smulb, t_sresultA);	// all signed args

   mid midMixed(t_umula[31:16], $unsigned(t_smulb), t_uresultB); // two args unsigned

endmodule
module mid(m_umula, m_umulb, m_uresult); // carbon disallowFlattening
   output  [31:0] m_uresult;
   input  [15:0] m_umula; 
   input  [15:0] m_umulb; 

   bottom ubot(m_umula, m_umulb, m_uresult); // all unsigned args

endmodule

module bottom(smula, smulb, sresult); // carbon disallowFlattening
   output signed [31:0] sresult;
   input signed [15:0] smula; 
   input signed [15:0] smulb; 

   assign                 sresult = smula * smulb; 

endmodule
