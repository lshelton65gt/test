// last mod: Thu Feb 22 15:36:10 2007
// filename: test/langcov/Ports/bug7062.v
// Description:  This test checks to see how substute module works when the port list of the instantion is incorrect. 
// try this test with directives:
// substituteModule do_not_use portsBegin c i1 i2 o1 portsEnd use_me portsBegin C I1 I2 O1 portsEnd
// warningMsg 20282

// at one time we would get an internal error with this testcase
// we expect all output bits to be z becaue there will be no connection to them
// due to incorrect case of port names in instance specification

module bug7062(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   do_not_use i1(.C(clock),.I1(in1),.I2(in2),.O1(out1)); // note the formal port names are uppercase which DOES NOT match with module do_not_use
endmodule

module do_not_use(c, i1, i2, o1);
   input c;
   input [7:0] i1, i2;
   output [7:0] o1;
   reg [7:0] o1;

   always @(posedge c)
     begin: main
       o1 = i1 | i2;
     end
endmodule



module use_me(C, I1, I2, O1);
   input C;
   input [7:0] I1, I2;
   output [7:0] O1;
   reg [7:0] O1;

   always @(posedge C)
     begin: main
       O1 = I1 | I2;
     end
endmodule

