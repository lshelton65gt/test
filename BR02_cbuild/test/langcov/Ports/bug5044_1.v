// last mod: Fri Sep 23 15:56:04 2005
// filename: test/langcov/Ports/bug5044_1.v
// Description:  This test is like bug5044, where a vector is declared in the
// port list using a bit order in reverse of the order specified in the
// declaration of the vector.  This should be a verilog syntax error


module bug5044_1(clock, in1, in2, out1, out2);
   input clock;
   input [3:0] in1, in2;
   output [7:0] out1, out2;

//   assign 	out2 = {in1[0:3], in2[0:3]};  // attempt to reference the bits of in1/in2 in reverse order will also generate alert
   
   mid i1 (in1, in2[3:1], in3, out1);
   
endmodule


module mid(in1[0:3], in2, in3, out); // note here that the first port_reference (in1[0:3] is specifed using
				     // a range in a reverse order from the declaration.
   input [3:0] in1;
   input [2:0] in2;
   input       in3;
   output [7:0] out;

   assign out = {in1,in2,in3};
   
endmodule
