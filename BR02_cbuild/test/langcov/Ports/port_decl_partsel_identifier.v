// last mod: Thu Feb  3 12:20:47 2005
// filename: test/langcov/Ports/port_decl_partsel_identifier.v
// Description:  This test has a module that uses a constant partselect in the
// port declaration (both for input and output), the partselects are the full
// width of the identifier


module top(input clock,
	   input [3:0] in1,
	   output [3:0] out,
	   output out2);

   port_decl_partsel_identifier i1(clock, in1, out, out2);

endmodule



module port_decl_partsel_identifier(clock, in1[3:0], out[3:0], out2);
   input [1:1] clock;
   input [3:0] in1;
   output [3:0] out;
   output [0:0] out2;
   reg [3:0] 	out;
   reg [0:0] 	out2;

   always @(posedge clock)
     begin: main
	out = ~ in1;
	out2 = in1[1];
     end
endmodule
