// last mod: Fri Jan 28 09:16:49 2005
// filename: test/langcov/Ports/lrm_12_3_3_split_ports.v
// Description:  This test is based on the example from section 12.3.3 of the
// LRM, with only the split_ports module


module top(in5, out5);
   input [3:0] in5;
   output [3:0] out5;

   split_ports i2(in5, out5);
   
   
endmodule

module split_ports (a[7:4], a[3:0]); // First port is upper 4 bits of �a�.
                                     // Second port is lower 4 bits of �a�.
                                     // Can�t use named port connections because
                                     // of part-select port �a�.
   inout [7:0] a;
   assign      a[3:0] = a[7:4] + 2;
endmodule
