// last mod: Sun Feb 16 17:22:40 2014
// filename: test/langcov/Ports/bug4935_6.v
// Description:  In this test an a formal is not defined, and it is left
// unconnected by an actual net, this is possible due to use of named port
// connection style


module bug4935_6(in1, in2, out1);
   input [7:0] in1, in2;
   output [7:0] out1;
   wire [7:0] 	unused;

   mid u1(.out1(out1), .in1(in1), .in2(in2) );
endmodule

module mid(out1, in1, , in2);	// a middle  port is not declared
   input [7:0] in1, in2;
   output [7:0] out1;

   assign 	out1 = in1 + in2;
endmodule

