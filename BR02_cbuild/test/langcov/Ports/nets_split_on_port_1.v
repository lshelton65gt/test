// last mod: Fri Sep 23 16:27:49 2005
// filename: test/langcov/Ports/nets_split_on_port_1.v
// Description:  This test has many variants of nets split in the module
// declaration, all should be equivalent.


module nets_split_on_port_1(pi0, pi1, pi2, pi3, out1, out2);
   input [3:0] pi0;
   input [3:0] pi1;
   input [2:0] pi2;
   input       pi3;
   output [7:0] out1, out2;

   mid_A i1(pi0[3],pi0[2],pi0[1],pi0[0], pi2[2], pi2[1], pi2[0], pi3, out1[7], out1[6], out1[5], out1[4], out1[3], out1[2], out1[1], out1[0]);
   mid_B i2(pi0[3:2],     pi0[1],pi0[0], pi2[2], pi2[1:0],       pi3, out2[7], out2[6], out2[5:3],                 out2[2], out2[1], out2[0]);
endmodule



module mid_A(in1[3], in1[2], in1[1], in1[0], in2[2], in2[1], in2[0], in3, out[7], out[6], out[5], out[4], out[3], out[2], out[1], out[0]);
   input [3:0] in1;
   input [2:0] in2;
   input       in3;
   output [7:0] out;

   assign out = {in1,in2,in3};
   
endmodule

module mid_B(in1[3:2], in1[1], in1[0], in2[2], in2[1:0], in3, out[7], out[6], out[5:3], out[2], out[1], out[0]);
   input [3:0] in1;
   input [2:0] in2;
   input       in3;
   output [7:0] out;

   assign out = {in1,in2,in3};
   
endmodule




