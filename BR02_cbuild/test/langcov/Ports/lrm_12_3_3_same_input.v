// last mod: Fri Jan 28 09:25:43 2005
// filename: test/langcov/Ports/lrm_12_3_3_same_input.v
// Description:  This test is based on the example from section 12.3.3 of the
// LRM with only the 'same_input' module


module top(in9, out9);
   input       in9;
   output 	out9;


   same_input i6(in9, out9);	// portdirection does not matter.
   
endmodule


module same_input (a,a);
   input a; // This is legal. The inputs are tied together.
endmodule
