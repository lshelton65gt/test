// last mod: Fri Jan 28 11:12:30 2005
// filename: test/langcov/Ports/port_decl_identifier.v
// Description:  This test is a simple module declaration with an identifier


module top(input clock,
	   input [3:0] in1,
	   output [3:0] out);

   port_decl_identifier i1(clock, in1, out);

endmodule

module port_decl_identifier(clock, in1, out);
   input clock;
   input [3:0] in1;
   output [3:0] out;
   reg [3:0] out;

   always @(posedge clock)
     begin: main
	out = | in1;
     end
endmodule
