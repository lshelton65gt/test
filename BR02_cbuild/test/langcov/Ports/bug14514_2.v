// filename: test/langcov/Ports/bug14514_2.v
// Description:  This test contains a signed/unsigned port mismatch, and the
// size of the ports do not match
// for example where all ports need to be extended see bug14514_1.v
// for example without size mismatch see also bug14514_0.v

// this example never showed the cbuild crash seen in bug14514 it was just
// created for completeness.  However this example does show some simulation
// mismatches, that only appear if all three instances of the module mid are
// included in the design.

module bug14514_2(t_umula, t_umulb, t_uresultA, t_smula, t_smulb, t_sresultA, t_uresultB ); // carbon disallowFlattening
   output [25:0] t_uresultA;
   input [20:0] t_umula; 
   input [20:0] t_umulb; 
   output signed [25:0] t_sresultA;
   input signed [20:0] t_smula; 
   input signed [20:0] t_smulb; 
   output [25:0] t_uresultB;

   mid midUnSigned(t_umula, t_umulb, t_uresultA); // all unsigned args, all need to be truncated 

   mid midSigned(t_smula, t_smulb, t_sresultA);	// all signed args, all need to be truncated

   mid midMixed(t_umula, t_smulb, t_uresultB); // two args unsigned, all need to be truncated

endmodule
module mid(smula, smulb, sresult); // carbon disallowFlattening
   output signed [31:0] sresult;
   input signed [15:0] smula; 
   input signed [15:0] smulb; 

   assign                 sresult = smula | smulb; 

endmodule

