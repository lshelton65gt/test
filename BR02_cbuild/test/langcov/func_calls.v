// Test function call scenarios (bug 347)
module top(r, a, b);
   input [3:0] a;
   input [3:0] b;
   output [2:0]   r;
   reg [2:0]   r;
   
   
   always @(a or b)
     begin
	r = bar(a, b);
     end
   
   function [43:0] bar;
      input [3:0] a;
      input [3:0] b;
      bar = foo(b, a);
   endfunction // bar

   function [155:0] baz;
      input [3:0] a;
      input [3:0] b;
      baz = a + b;
   endfunction // baz

   function [4:0] foo;
      input [3:0] a;
      input [3:0] b;
      foo = baz(b, a);
   endfunction // foo

endmodule // top
