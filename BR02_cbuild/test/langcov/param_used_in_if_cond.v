// filename: test/langcov/param_used_in_if_cond.v
// Description:  This test demonstrated a problem related to sizing when a
// parameter was used as the condition in an if statement.
// this test must be run with -v2k


module param_used_in_if_cond #(parameter msb = 7,
			       cond1 = 0)
   (
   input clock,
   input [msb:0] in1,
   input [msb:0] in2,
    output reg [msb:0] out1);

   always @(posedge clock)
     begin: main
	if ( cond1 )
	  out1 = in1 & in2;
     end
endmodule
