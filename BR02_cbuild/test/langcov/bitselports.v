/* Test of bit and part selects in ports. */
module top(in1, in2,
           out1, out2
          );

input [3:0] in1, in2;
output [0:2] out1, out2;

foo bitsel (in1[3], in2[0], out1[0]);
bar partsel (in1[3:2], in2[2:1], out2[0:1]);

endmodule


module foo(a, b, z);
output z;
input a;
input [1:1] b;
assign z = a & b;
endmodule


module bar(a, b, z);
output [-3:-4] z;
input [1:0] a, b;
assign z = a | b;
endmodule
