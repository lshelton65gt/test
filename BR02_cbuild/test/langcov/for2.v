module top ( out, clk, take, in );
  output [7:0] out;
  reg [7:0] out;
  input clk;
  input [7:0] take;
  input [7:0] in;

  always @ (posedge clk)
  begin :break
    integer i;
    for ( i = 0 ; i <= 7 ; i = i + 1 )
      begin :continue
        if ( take[i] == 1 ) 
          out[i] = in[i];
      
      end // continue
  end // break

endmodule // top
