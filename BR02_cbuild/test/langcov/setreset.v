/* Test set and reset. */
module top(set, reset, q, q1, ld, d, ld_in, clk);
input set, reset, d, ld, ld_in, clk;
output q, q1;
reg q, q1;
always @(posedge clk or posedge reset or posedge set)
begin
  if (reset)
    q <= 0;
  else if (set)
    q <= 1;
  else
    q <= d;
end


  always @ (posedge clk or posedge set or posedge reset or posedge ld)
    if (ld)
      q1 = ld_in;
    else if (reset)
      q1 = 0;
    else if (set)
      q1 = 1;
    else
      q1 = d;
endmodule
