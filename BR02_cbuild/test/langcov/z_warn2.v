module top(n1, a1);
   input a1;
   output n1;

   assign n1 = a1;
   fab inst (n1);
endmodule

module fab(o);
   output o;
endmodule
