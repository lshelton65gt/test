// September 2008
// This file tests the specparam.

module bug6693_01(clk, in1, out1, out2);
   specify
    specparam t1 = 10;
   endspecify

   specparam t2 = 5.0;
   
   input   clk;
   input[3:0]  in1;
   output  out1, out2;
   reg     out1, out2;	   
   reg     a;
   reg     b;
   real    stime;

  always @(posedge clk)
  begin
    if (in1 < t1) 
      out1 <= 1'b0;
    else 
      out1 <= 1'b1;

    if (in1 < t2) 
      out2 <= 1'b0;
    else 
      out2 <= 1'b1;
  end
    

endmodule
