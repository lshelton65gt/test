/* Asynchronous reset positive tests. */
module top(d, reset_val, reset_val2, clock, reset, reset2,
           baz, blah, e, foo, bar,
           o
          );
input d, reset_val, reset_val2;
input baz, blah, e, foo, bar;
input clock, reset, reset2;
reg q;
output o;

reg r;

assign o = q;

   /*
   // Example 1 - Valid asynchronous reset.
   */
   always @(posedge reset or negedge clock)
   begin
     if (reset)
       q = reset_val;
     else
       q = d;
   end


   /*
   // Example 2 - Valid multiple asynchronous resets.
   */
   always @(posedge reset or negedge clock or negedge reset2)
   begin
     if (reset2 == 1'b0)
       q = reset_val2;
     else if (reset)
       q = reset_val;
     else
       q = d;
   end

   /*
   // Example 3 - Valid reset with additional conditions on the if statement.
   */
   always @(posedge reset or negedge clock)
   begin
     if (reset == 1'b1)
       q = reset_val;
     else if (foo)
       q = baz;
     else if (bar)
       q = blah;
     else
       q = d;
   end


   /*
   // Example 4 - Valid reset with only one register being reset, but 2 registers
   // being clocked.
   */
   always @(negedge reset2 or negedge clock)
   begin
     if (~reset2)
       q = reset_val;
     else
     begin
       r = e;
       q = d;
     end
   end

endmodule
