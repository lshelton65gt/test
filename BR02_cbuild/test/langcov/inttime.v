/* Test preliminary support of integer and time. */
module top(CLK, o);
   input CLK;
   output [63:0] o;
   
   integer A;
   reg [31:0] B;
   time c;

   always @(posedge CLK) begin
      B = 2;
      A = B;
      c = B;
   end

   assign o = c + A;
     
endmodule
