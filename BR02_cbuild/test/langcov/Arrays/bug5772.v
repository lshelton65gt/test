// last mod: Fri Mar 10 10:11:50 2006
// filename: test/langcov/Arrays/bug5772.v
// Description:  This test is the essence of bug 5772, the declaration of the
// 'bytes' array in the function was not supported at one time.


module bug5772(clock, in1,  out1);
   input clock;
   input [47:0] in1;
   output [6:0] out1;
   reg [6:0] out1;

   function [6:0] crc6;
      input [47:0] sa;
      integer i,j;
      parameter CRC32_POLY = 32'hedb88320;
      reg [31:0] temp;
      reg [31:0] c;
      reg [7:0] bytes [7:0]; // <--- LOOK
      begin
	 temp = 32'hffffffff;
	 bytes[0] = sa[7:0];
	 bytes[1] = sa[15:8];
	 bytes[2] = sa[23:16];
	 bytes[3] = sa[31:24];
	 bytes[4] = sa[39:32];
	 bytes[5] = sa[47:40];
	 for (i=0;i<6;i=i+1)
	   begin
	      //      c = (temp & 8'hff) ^ bytes[i];
	      c = {24'b0,temp[7:0]} ^ {24'b0,bytes[i]};
	      for (j=0;j<8;j=j+1)
		begin
		   c = c[0] ? ((c >> 1) ^ CRC32_POLY) : (c >> 1);
		end
	      temp = (temp >> 8) ^ c;
	   end
	 crc6 = temp[6:0];
      end
   endfunction

   
   always @(posedge clock)
     begin: main
	out1 = crc6(in1);
     end
endmodule
