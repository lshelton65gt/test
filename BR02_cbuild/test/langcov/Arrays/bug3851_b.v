// last mod: Fri Mar 18 06:55:24 2005
// filename: test/langcov/bug3851_b.v
// Description:  Inspired by bug3851, here we currently get a internal error
// because of driver analysis.
// once this test works the description for this testcse will be:
// 2D verilog array,  read from dynamic location with bitselect
//                    write to static location with bitselect


module bug3851_b(clock, in, add, bit, out);
   input clock, in;
   input [3:0] add;
   input [2:0] bit;
   output [7:0] out;

   reg [7:0] mem [15:0];

   integer   i;
   initial
      begin
	 
	 for(i = 0; i < 16; i = i + 1)
	   mem[i] = i;
      end

   assign out = mem[add][bit];

   // this single line always block will be converted to an assign statment with
   // a dynamic bitselect
   always @(in)
     begin
	mem[3][bit] = in ? in : 1'bz;
     end
endmodule
