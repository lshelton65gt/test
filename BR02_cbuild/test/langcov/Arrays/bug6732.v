// last mod: Tue Nov 21 14:56:10 2006
// filename: test/langcov/Arrays/bug6732.v
// Description:  At one time this test would fail with an error
// message saying that the array 'mem' was larger than 2^32 bits,
// it was exactly 2^32 bits.
// 

module bug6732();
   parameter twoTo32 = 33'b100000000000000000000000000000000;
   parameter twoTo31 = (1 << 31);
   parameter LEN = twoTo31 - 1;
   reg [1:0] mem [0:(twoTo31-1)];	// exactly 2**32 total bits

   reg [1:0] mem_ok [0:(twoTo31-2)];    // exactly (2**32)-2 total bits

   
   reg [1:twoTo32] vector_tobig;	// exactly (2**32) bits
   reg [0:twoTo32-1] vector_tobig2;   	// exactly (2**32) bits
   reg [1:twoTo31] vector_ok;   	// exactly (2**32) bits

endmodule
