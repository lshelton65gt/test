// filename: test/langcov/bug14755.v
// Description:  This test duplicates the issue seen in bug 14755.  The issue
// was that during population of the block F1_fn the cheetah object for array
// asid0 (type VE_NETARRAY) was passed to veVariableGetParentTfArg() that only
// takes objects of type VE_VARIABLE. This resulted in an error message from the
// parser, and a non-zero exit. 

module bug14755(clk, all_a, sel, F1_val); 

   input clk;
   input [359:0] all_a;
   input [3:0] 	 sel;
   output [29:0] F1_val;


   reg [29:0] 	 F1_val;


   function [29:0] F1;
      input [359:0] all_a;
      input [3:0]   sel;
      begin : F1_fn
         reg [9:0]     asid0[11:0];	 
         integer       i;

         for (i=0; i<12; i=i+1) begin
            asid0[i] = all_a[i*30 +: 10];
         end
         F1 = asid0[sel];
      end
   endfunction


   always @(posedge clk)
     begin
	F1_val <= F1( all_a,  sel);
     end


endmodule
