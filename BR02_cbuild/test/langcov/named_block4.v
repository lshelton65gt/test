/* More named blocks. */
module top(clk, i1, i2, sel,
           o1, o2
          );

input clk;
input i1, i2, sel;
output o1, o2;
reg o1, o2;

always @(posedge clk)
if (sel)
begin
  o1 = i1;
end
else
  o1 = i2;

always
begin
if (sel)
  begin : named1
  reg temp;
  temp = i1;
  o2 = temp;
  end
else
  begin
    begin : named2
    reg temp;
    temp = i2;
    o2 = temp;
    end
  end
end

endmodule
