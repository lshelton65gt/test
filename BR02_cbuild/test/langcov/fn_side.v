// Test function side effects
module top(i1, i2, o1, o2);
   input i1, i2;
   output o1, o2;

   reg 	  o1;

   function [3:0] f;
      input a, b;
      begin
	 o1 = a & b;
	 f = a | b;
      end
   endfunction // f

   assign   o2 = f(i1, i2);

endmodule // top
