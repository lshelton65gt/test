module top ( a, b, in, clk );
   input clk;
   input [2:0] in;
   output [2:0] a;
   output [2:0] b;
   reg [2:0] 	a;
   reg [2:0] 	b;

   integer 	i;
   
   always @(posedge clk)
     begin
	for ( i = 0 ; i <= 2 ; i = i + 1 )
	  begin
	     a[i] <= in[i];
	     b[i] = a[i];
	  end
     end
endmodule // top
