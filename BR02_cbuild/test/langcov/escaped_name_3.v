// last mod: Mon Dec  5 16:24:05 2005
// filename: test/langcov/escaped_name_3.v
// Description:  This test has only escaped names,
// with -Wc -g.

// gold file from aldec, main.v required minor editing because of escaped name

module escaped_name_3(\1clock1 , \1in1 , \1in2 , \1out1 , \1out2 );
   input \1clock1 ;
   input [7:0] \1in1 , \1in2 ;
   output [7:0] \1out1 ;
   output [7:0] \1out2 ;
   reg [7:0] \1out2 ;

   \1mid  \1u1 (\1clock1 , \1in1 , \1in2 , \1out1 );
endmodule


module \1mid (\2clock1 , \2in1 , \2in2 , \2out1 );
   input \2clock1 ;
   input [7:0] \2in1 , \2in2 ;
   output [7:0] \2out1 ;

   bot u1(\2clock1 , \2in1 , \2in2 , \2out1 );
endmodule


module bot (clock1 , in1 , in2 , out1 );
   input clock1 ;
   input [7:0] in1 , in2 ;
   output [7:0] out1 ;
   reg [7:0] out1 ;

   always @(posedge clock1 )
     begin: main 
       out1  = in1 & in2 ;
	escaped_name_3.\1out2 = ~out1;
     end
endmodule
