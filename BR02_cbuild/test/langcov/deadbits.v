module top(i1, i3, o1, o2);
  input i1, i3;
  output o1, o2;

  wire [3:0] vec;

  // Bit 0 is totally dead.  There is no driver, no load.

  // Bit 1 is totally live, and it livens the 'vec' so we need to include it in waveforms
  assign     o1 = vec[1];
  assign     vec[1] = i1;

  // Bit 2 is Z.  There is no driver, there is a load.
  assign     o2 = vec[2];

  // Bit 3 starts out live but becomes dead because there is a driver, but there
  // is no load, so we will dead-logic-eliminate this assign
  assign     vec[3] = i3;
endmodule
