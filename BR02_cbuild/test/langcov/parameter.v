
module pcheck(in, vecin1, vecin2, bigout, out1, out2, out3, out4, out5, out6, out7, vecout1, vecout2, clk);
   input in, clk;
   output out1, out2;
   output out3, out4, out5, out6, out7;
   output [19:0] vecout1;
   input [19:0] vecin1;
   output [5:0] vecout2;
   input [5:0] vecin2;

   output [127:0] bigout;
   reg [127:0] bigout;

   always @(posedge clk) bigout = {128{1'b1}};

   // These are all the same and should only yield 1 module
   foo foo1 (clk, in, out1);
   foo #(1) foo2 (clk, in, out2);
   foo #(1,1'b1, 40'hf000000001) foo3 (clk, in, out3);
   // Instance where the sizes are greater than the bounds, have unequal 
   // values but still as a result of the bounds result in the default module
   foo #(10'b1111100001, 1'b1, 44'hff000000001) foo7(clk, in, out7);

   // This looks the same but is different, because the second param is not bounded
   foo #(1,1) foo4 (clk, in, out4);
   
   // just third is different
   foo #(5'b1, 1'b1, 1) foo5 (clk, in, out5);

   // all 3 are different, using expressions
   foo #(2'b1 << 1, 8'hf0 & 6'b111000, 1 - 0) foo6 (clk, in, out6);

   bar #(20, 1, 20'hfffff) bar1(clk, vecin1, vecout1);
   bar #(6, 1, 4'hf) bar2(clk, vecin2, vecout2);
   
endmodule   

module foo (clk, in, out);
   input in, clk;
   output out;
   reg 	  out;
   
   parameter [4:0] rangeParam = 1'b1;
   parameter anyParam = 1'b1;
        
   parameter [39:0] bigParam = 40'hf000000000 + 1'b1;

   always @(posedge clk)
     out = ((rangeParam | in) <= bigParam) + anyParam;

endmodule

module bar (clk, in, out);
   parameter msb = 10;
   parameter lsb = 0;
   parameter [msb:lsb] vec = 0;
   parameter           baddiv = msb/lsb;
   
            
   input [msb:lsb]     in;
   
   input                clk;
   output [msb:lsb]     out;
   
   reg  [msb:lsb]       out;

   always @(posedge clk)
     out <= in + baddiv;
endmodule // bar
