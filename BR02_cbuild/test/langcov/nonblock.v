/* Test non-blocking assigns. */
module top(i1, i2, i3, i4, sel,
           o1, o2, o3, o4, o5, o6, o7);
input i1, i2, sel;
output o1, o2, o3, o4;
output [3:5] o5, o6;
output [0:2] o7;
input [0:2] i3, i4;
reg tmp;
reg r1;
reg r2;
reg r3;
reg r4;
reg [1:3] r5, r6;
reg [0:2] o7;

assign o1 = r1;
assign o2 = r2;
assign o3 = r3;
assign o4 = r4;
assign o5 = r5;
assign o6 = r6;

always
begin
  tmp = i1 & i2;
  r3 <= i1 | i2;
  if (sel)
    r1 <= i1;
  r2 <= tmp;
  tmp = i2;
  r3 <= i1 ^ i2;
end

always
begin
  r4 <= i1 | i2;
end

always
begin
  r5 <= i3;
  r6 <= r5;
end

always
begin
  if (sel)
    o7[0] <= i1;
  o7[2] <= i2;
end

endmodule
