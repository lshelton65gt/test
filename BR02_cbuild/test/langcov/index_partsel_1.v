// last mod: Fri Nov 11 14:55:05 2005
// filename: test/langcov/index_partsel_1.v
// Description:  This test shows that cbuld does/did not get indexed partselect
// values correct for some numbers longer than 64 bits.
// ALDEC puts ONE more blank in the field for %d when printing the decimal digits
// than Carbon does.
// bug 5307
module index_partsel_1 (clk, out);
   input clk;
   reg [7:0] sel;
   output [64:0] 	 out;
   reg [64:0] 	 out;
   integer 	 i;
   reg [64:0] 	 out_downto;
   reg [0:64] 	 out_to;

   
   always @(posedge clk)
     begin
	for ( i = 0; i < 64; i = i+4 )
	   begin
              out_downto = 65'b0;
	      out_downto[i +: 32] = 32'b11001101111011110000000000000001;
	      $display(" out_downto[%d +:32]: %b",i, out_downto);
           end
	for ( i = 0; i < 64; i = i+4 )
	   begin
              out_to = 65'b0;
	      out_to[i +: 32] = 32'b11001101111011110000000000000001;
	      $display("     out_to[%d +:32]: %b",i, out_to);
           end
	out = out_downto ^ out_to;
     end
endmodule // index_partsel_1


