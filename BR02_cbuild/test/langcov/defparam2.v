// last mod: Fri Jan 13 15:35:37 2006
// filename: test/langcov/defparam2.v
// see bug 4976
// Description:  This test is an extension of defparam1.v with an attemted
// workaround.  However just defining the extra defparams does not cause the
// proper uniqufication. for example only 2 versions of bottom are created for
// this example (we expected 3).



module defparam2(clock, rw, addr, dat, out1, out2, out3);
   input clock, rw;
   input [3:0] addr;
   input [7:0] dat;
   output [7:0] out1, out2, out3;

   defparam 	i1.i1.b1.bottom_fn = "bottom1";
   defparam 	i1.i2.b1.bottom_fn = "bottom2";
   defparam 	i1.i3.b1.bottom_fn = "bottom3";

   defparam 	i1.foo = 2;
   defparam 	i1.i1.mid_fn = "mid1";
   defparam 	i1.i2.mid_fn = "mid2";
   defparam 	i1.i3.mid_fn = "mid3";

   midtop i1(clock, rw, addr, dat, out1, out2, out3);

endmodule

module midtop(clock, rw, addr, dat, out1, out2, out3);
   input clock, rw;
   input [3:0] addr;
   input [7:0] dat;
   output [7:0] out1, out2, out3;

   parameter [7:0] foo = 0;
   
   mid i1(clock, rw, addr, dat, out1);
   mid i2(clock, rw, addr, dat, out2);
   mid i3(clock, rw, addr, dat, out3);

endmodule

module mid(clock, rw, addr, dat, out1);
   input clock, rw;
   input [3:0] addr;
   input [7:0] dat;
   output [7:0] out1;

   parameter [127:0] mid_fn = "mem0.dat";
   
   initial $display ("at %m, mid_fn is: %s", mid_fn);

   bottom b1( clock, rw, addr, dat, out1);

endmodule

module bottom(clock, rw, addr, dat, out1);
   input clock, rw;
   input [3:0] addr;
   input [7:0] dat;
   output [7:0] out1;
   reg [7:0] out1;
   reg [7:0] mem [0:15];

   parameter [127:0] bottom_fn = "bottom0";

   initial
     begin
	$display("at %m, bottom_fn is: %s", bottom_fn);
     end
   
   always @(posedge clock)
     begin
	if (rw)
	  mem[addr] = dat;
	else
	  out1 = mem[addr];
     end
endmodule
