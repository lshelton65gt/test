// Aldec and carbon handle this differently? BUG5433
// variable init syntax is only permitted at MODULE level
// See notes at bottom for justification of Verific gold file
module top(clk,x, y, z, q);
   input clk;
   output x,y,z,q;
   wire [63:0] q;
   wire [31:0] z;
   wire [31:0] y;

   reg [7:0]   r = 8'hca;
   time        t = 1 ;
   integer     i = 55;
   real        f = 2.5;
   
   always @(posedge clk)
     begin
        r = r >> 1;
        t = $time - t;
        i = i + 1;
        f = f * f;
     end
   
   assign      x = | r;
   assign      y = i;
   assign      z = f;
   assign      q = t;
endmodule

// This test requires a Verific gold file because
// the Interra flow has a bug in it's treatment of
// time variables. Specifically, it treats time
// as a signed quantity, when in fact it is
// unsigned. The Verific flow correctly recognizes
// that time is unsigned.
//
// As a consequence, the Verific flow correctly
// aliases 't' and 'q' (with 'q' being the master); whereas
// the Interra flow fails to alias the two.
// As a result, the flags on 't' and 'q' are slightly
// different between the two flows.
//
// As discussed above, the Interra flow incorrectly 
// puts the 'signed' flag on 't'.
// Because 't' is aliased to 'q' in the Verific flow, 
// the 'read' flag on 't' is placed on 'q' instead, 
// the 'aliased' flag appears on 't' (instead of 'q'), 
// and the 'read' and 'written' flags are placed 
// on 'q' instead of 't'.
