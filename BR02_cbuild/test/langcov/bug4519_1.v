// last mod: Thu May 26 17:47:14 2005
// filename: test/langcov/bug4519_1.v
// Description:  This test is a trimmed down version of bug4519.v


module bug4519_1(out, in1);
   output out;
   input  in1;

  assign out = (  (32'haecb43cd<0) ? 1'b0 : in1) ;

//  > cheetah ve_expr
//    ((32'haecb43cd < 0) ? 1'b0 : in1): CONDOP
//  > p VeExprGetValueType(ve_expr)
//    $14 = 2
//  > p/d LOCALLY_STATIC        // this is wrong, it appears that a signed  compare is being done
//    $16 = 2
//  > p VeExprGetEvaluateType( ve_expr )
//    $22 = 2     // (this is VE_EXPREVAL_BINARY)
//  > p VeExprEvaluateValueInString(ve_expr, &flagXZ, 1)
//    $23 = 0     // 0 here is null, meaning that it was not a constant
//

endmodule
