// Simple minded sanity test for variable initializers, and
// signedness.
// Note: This test has a separate Verific gold file, because
// the signedness on time nets is computed incorrectly
// in the Interra flow.
module top(clk, r_o, t_o, i_o, f_o, r_ow, t_ow, i_ow, f_ow);
   input clk;
   output r_o, t_o, i_o, f_o;
   output r_ow, t_ow, i_ow, f_ow;
   wire [31:0] r_o;
   wire [63:0] t_o;
   wire [31:0] i_o;
   wire [63:0] f_o;  

   wire [63:0]  r_ow;
   wire [127:0] t_ow;
   wire [63:0]  i_ow;
   wire [127:0] f_ow;  
   
   reg [31:0]   r = 8'hca;
   time         t = 1 ;
   integer      i = -55;
   real         f = 2.5;

   assign r_o = r;
   assign r_ow = r;
   assign t_o = t;
   assign t_ow = t;
   assign i_o = i;
   assign i_ow = i;
   assign f_o = f;   
   assign f_ow = f;
endmodule
