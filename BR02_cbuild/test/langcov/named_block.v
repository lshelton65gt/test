/* Test named blocks. */
module top(j, k, l, sel,
           o
          );
input [3:0] j, k, l;
input sel;
output [3:0] o;
reg [3:0] i;
reg [3:0] m;

  assign o = m;

  always
  begin : foo
    reg [2:5] r;
    i = j + k;
    if (sel)
      i = l;
    r = i;
    m = r;
  end
endmodule
