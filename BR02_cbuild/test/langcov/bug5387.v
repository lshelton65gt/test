// last mod: Wed Nov 30 19:07:23 2005
// filename: test/langcov/bug5387.v
// Description:  This test caused a segfault at one time.
// It is expected to exit with an error about multiple top-level modules

module bug5387;
endmodule

module bar;
endmodule
