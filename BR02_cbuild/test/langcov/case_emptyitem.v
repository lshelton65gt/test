module bug (c,d);
   input c,d;
   reg 	 a;

   always @(posedge d) begin
      case (c)
	1'b1: ;
	default: a = 1'b1;
      endcase // case(c)
   end
endmodule // bug
