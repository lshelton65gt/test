

module mux21 (Q, A, B, SL);
   output Q;
   input  A, B, SL;

   initial
     $display("in %m");

   assign Q = SL ? A : B;

endmodule

