// this is a module that no one should use
module do_not_use_me(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   initial
     $display("This line from %m should never appear");

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule
