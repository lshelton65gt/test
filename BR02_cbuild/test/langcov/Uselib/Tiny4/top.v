// last mod: Mon Nov 28 09:37:00 2005
// filename: test/langcov/Uselib/Tiny4/top.v
// Description:  This test was used to demonstrate a problem that appeared when a CEM file
// contained a resetall directive.  At the time it looked like the use of uselib
// somehow was involved.  Since I had spent the time building another testcase
// that used uselibs in a new way I have added the test here.

// HOWEVER this test currently fails, but should not.
// An error is reported that mux21_wrap has no timescale directive.
// This is caused by the way that we generate .cem files, if a file that
// is .cem'ed contains a resetall anywhere in the file we insert the
// resetall at the beginning of the file, this is wrong. (bug 5366)

// The only timescale directive in this whole design hierarchy is the following one:
`timescale 1ns/1ps

module top(sel, in1, in2, out1, out2, out3, out4);
   input sel;
   input  in1, in2;
   output  out1, out2, out3, out4;

`uselib file=lib1/mux21.v

   mux21 u1 (out1,in1, in2, sel);

// now specify a new uselib, where the first two items in the list have modules
// but those modules are not used in this design.  The third item (lib2) is a
// protected module, with .cem protection, and the source for that module
// contains a resetall so any modules processed after it should fail
// because they have not active timescale directive.  Because cheetah processes
// missing modules in alphabetic order no other modules fall after mux21_wrap
`uselib file=libNothing/do_not_use_me_2.ve \
        dir=libNothing libext=.v \
        dir=lib2 libext=.v 

   mux21_wrap u2 (out2, in1, in2, sel);

`uselib dir=lib3 libext=.v 

   mux21 u3 (out3, in1, in2, sel);

`uselib dir=lib4 libext=.v 

   mux21 u4 (out3, in1, in2, sel);
   
   
endmodule

