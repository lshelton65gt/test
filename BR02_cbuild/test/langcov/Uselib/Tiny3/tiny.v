// last mod: Mon Nov 28 11:54:35 2005
// filename: test/bugs/bug5093/tiny.v
// Description:  At one time this test demonstrated a crash in cheetah when you have a
// module and a udp with the same name and the use of each is selected by
// carefully defined uselib directives

// this particular test would pass  if you put the lines between the //1 comments
// after the line with the //2 comment
// after the lines with the //2 comments

// I could not get aldec to simulate this correctly, so I manually defined
// modules with different names, and used aldec to create the gold file


module tiny(sel, in1, in2, out1, out2);
   input sel;
   input  in1, in2;
   output  out1, out2;

//1   
`uselib dir=modules libext=.v
   mux21 u2 (out2,in1, in2, sel);
//1   

`uselib dir=udps libext=.v
   mux21 u1 (out1,in1, in2, sel);
//2

endmodule

