// last mod: Fri Jun 16 10:27:50 2006
// filename: test/bugs/bug5093/tiny.v
// Description:  This test demonstrates a crash in cheetah when you have a
// module and a udp with the same name and the use of each is selected by
// carefully defined uselib directives.  Note that this design has a cbuild
// error because the last `uselib clears the search path and so mux21 cannot be found

`uselib dir=udps libext=.v 

module tiny(sel, in1, in2, out1, out2);
   input sel;
   input  in1, in2;
   output  out1, out2;

   mux21 u1 (out1,in1, in2, sel);
`uselib file=lib/mid.v

   mid u2 (sel, in1, in2, out2);
`uselib

   mux21 u3 (out1,in1, in2, sel);   
endmodule

