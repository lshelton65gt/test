// last mod: Fri Jun 16 10:10:45 2006
// filename: test/bugs/bug5093/tiny_2.v
// Description:  This test checks the proper operation of uselib, there are 2
// instances, one of a module and one of a udp, both the udp and module have the
// same same name (but have different functions).  The proper one is selected by
// the use of uselib directives
// gold file from nc (aldec gets this wrong)

`uselib dir=udps libext=.v 

module tiny(sel, in1, in2, out1, out2);
   input sel;
   input  in1, in2;
   output  out1, out2;

   // mux21 is found in the udps directory
   mux21 u1 (out1,in1, in2, sel);


`uselib file=lib/mid.v
   // find mid in lib/mid.v, it selects mux21 from the modules directory
   mid u2 (sel, in1, in2, out2);
endmodule

