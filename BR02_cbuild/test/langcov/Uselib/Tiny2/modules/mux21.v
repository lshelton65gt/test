

module mux21 (Q, A, B, SL);
   output Q;
   input  A, B, SL;

   assign Q = SL ? A : B;

endmodule

