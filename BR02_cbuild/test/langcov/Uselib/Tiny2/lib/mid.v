`uselib dir=modules libext=.v

module mid(sel, in1, in2, out1);
   input sel;
   input  in1, in2;
   output  out1;

`resetall
  
   mux21  u1 (out1,in1, in2, sel);

endmodule
`uselib

