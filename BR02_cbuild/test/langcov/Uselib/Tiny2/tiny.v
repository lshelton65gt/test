// last mod: Mon Nov 28 11:42:58 2005
// filename: test/bugs/bug5093/tiny.v
// Description:  This test once demonstrated a crash in cheetah when you have a
// module and a udp with the same name and the use of each is selected by
// carefully ordered uselib directives


// I was unable to get aldec to simulate this correctly, so I rewrote the design
// so it selected the correct modules (each given a unique name), and then
// simulated to create the gold file.


module tiny(sel, in1, in2, out1, out2);
   input sel;
   input  in1, in2;
   output  out1, out2;

`uselib file=modules/mux21.v
	    
   mux21 u1 (out1,in1, in2, sel);

`uselib dir=udps libext=.v 
   mux21 u2 (out2, in1, in2, sel);

   
endmodule

