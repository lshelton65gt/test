

module TOPXB_RTM (IN1);
   input IN1;

  MODULE2 u1 (.FD(LSI_VSS));

endmodule



module MODULE2 (FD);
   input FD;
   MODULE3 I4 (.Z(JTAGAZ), .D0(A), .D1(JTAGA), .S(JTAGAS));
endmodule

