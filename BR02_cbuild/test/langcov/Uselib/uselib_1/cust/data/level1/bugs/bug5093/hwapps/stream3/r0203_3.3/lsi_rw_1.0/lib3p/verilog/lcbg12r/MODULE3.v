`resetall

`timescale 1ns/100ps
module MODULE3(Z,D0,D1,S);
  output  Z;
  input   D0, D1, S;

  buf #(0.001) ( Z, ZT);
  LSI_MUX21 M1 (ZT, D0, D1, S);



endmodule
