`timescale 1ns/100ps

module LSI_MUX21 (Q, A, B, SL);
   output Q;
   input  A, B, SL;

   assign Q = SL ? B : A;

endmodule // LSI_MUX21

