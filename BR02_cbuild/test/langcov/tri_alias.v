/* Test tristate aliasing */
module top(bi, a1, a2, e1, e2, out);
   inout bi;
   input a1, a2, e1, e2;
   output out;
   wire internal;

   tribuf u1(bi, a1, e1);       // marks bi as a primary tristate
   tribuf u2(internal, a2, e2); // internal is also marked primary tristate
   or(out, internal, bi);    
 endmodule

 module tribuf(out, in, ena);
   output out;
   input in, ena;
   assign out = ena? in: 1'bz;
 endmodule
