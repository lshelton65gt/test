// Test big memories
`define DEPTH 1048576
`define DEPTH_BITS 20
module top(rd, wr, clk, in, out, addr);
   input rd, wr, clk;
   input [31:0] in;
   output [31:0] out;
   reg [31:0] out;
   input [`DEPTH_BITS - 1:0] addr;

   reg [31:0] mem [`DEPTH - 1:0];
   initial
     begin : named
	integer i;
	for (i = 0; i < `DEPTH; i = i + 1)
	  mem[i] = 0;
     end

   always @(posedge clk)
     begin
	if (rd)
	  out = mem[addr];
     end
   
   always @(posedge clk)
     begin
	if (wr)
	  mem[addr] = in;
     end
   
endmodule
