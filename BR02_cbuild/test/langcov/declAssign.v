module top(in1,in2,out);
input in1,in2;
output out;

wire outWire=in1?in2:0;
assign out=outWire;

endmodule
