module iopad(pad,in,out,en,pullen);
  inout pad;
  output in;
  input out;
  input en;
  input pullen;

  reg pullup_driver;
  wire buf_pad;

  always @(pullen)
    if (pullen)
      pullup_driver = 1'b1;
    else
      pullup_driver = 1'bz;

  assign pad = en ? out: 1'bz;
  assign (weak0,weak1) pad = pullup_driver;
  assign in = pad;

  //pmos (pad,buf_pad,1'b0);

endmodule
