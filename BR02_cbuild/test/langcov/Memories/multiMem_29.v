/* This test case test the result of an continuous assignments , where      *
 * multi-dimensional array word select is used in the RHS with constant and *
 * variable indices.                                                        */

module multiMem_1(addr1,addr2,addr3,clk,data1, data2, data3, data4, data5);
   input  addr1;
   input [0:1] addr2;
   input [0:1] addr3;
   input  clk;
   output [0:31] data1;
   output [0:31] data2;
   output [0:31] data3;
   output [0:31] data4;
   output [0:31] data5;

   integer loop_index_1;
   integer loop_index_2;
   reg signed  [0:31] array3 [0:3][0:1];

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 3; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 0; loop_index_2 <= 1; loop_index_2 = loop_index_2 + 1) 
       begin 
       array3[loop_index_1][loop_index_2] = addr2 + 10 * loop_index_2 + loop_index_1;
       end
     end
end

   assign data1 = array3[addr2][addr1];
   assign data2 = array3[addr1][addr1];
   assign data3 = array3[addr3][addr1];
   assign data4 = array3[addr3][addr1];
   assign data5 = array3[2][1];

endmodule

