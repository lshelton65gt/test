/* Concatenation of multi-dimensional array word select elements passed as port*
 * connection to a module and the module output is used to populate the top    *
 * module's output port. multi-dim parent element (array3 [0:1][4:6]) has      *
 * non-zero base range (like 6:3 instead of 3:0).                              */

module matrox_11 (in00, in01, in02, in03, in10, in11, in12, in13, out00, out01, out02, out03, out10, out11, out12, out13);

   input  [7:0] in00, in01, in02, in03, in10, in11, in12, in13;
   output [7:0] out00, out01, out02, out03, out10, out11, out12, out13;

   wire [7:0] aindata[1:0][6:3];
   wire [7:0] aoutdata[1:0][12:9];


   assign aindata[0][6] = in00;
   assign aindata[0][5] = in01;
   assign aindata[0][4] = in02;
   assign aindata[0][3] = in03;
   assign aindata[1][6] = in10;
   assign aindata[1][5] = in11;
   assign aindata[1][4] = in12;
   assign aindata[1][3] = in13;

   bottom B({aindata[0][6], aindata[0][5], aindata[0][4], aindata[0][3]}, 
           {aoutdata[0][12], aoutdata[0][11], aoutdata[0][10], aoutdata[0][9]});  

   assign out00 = aoutdata[0][12];
   assign out01 = aoutdata[0][11];
   assign out02 = aoutdata[0][10];
   assign out03 = aoutdata[0][9];
   assign out10 = aoutdata[0][12];
   assign out11 = aoutdata[0][11];
   assign out12 = aoutdata[0][10];
   assign out13 = aoutdata[0][9];
   
endmodule


module bottom(indata, outdata);

   input  [31:0]  indata;
   output [31:0]  outdata;

   assign  outdata = indata;
endmodule

