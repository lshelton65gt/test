/* Concatenation of multi-dimensional array word select elements passed as port*
 * connection to a task and the task output is used to populate the top        *
 * module's output port.                                                       */

module multiMem_15 (in00, in01, in02, in03, in10, in11, in12, in13, out00, out01, out02, out03, out10, out11, out12, out13);

   input  [7:0] in00, in01, in02, in03, in10, in11, in12, in13;
   output [7:0] out00, out01, out02, out03, out10, out11, out12, out13;

   wire [7:0] aindata[1:0][3:0];
   reg [7:0] aoutdata[1:0][3:0];

   

   assign aindata[0][3] = in00;
   assign aindata[0][2] = in01;
   assign aindata[0][1] = in02;
   assign aindata[0][0] = in03;
   assign aindata[1][3] = in10;
   assign aindata[1][2] = in11;
   assign aindata[1][1] = in12;
   assign aindata[1][0] = in13;

always @(aindata[0][0])
   bottom({aindata[0][3], aindata[0][2], aindata[0][1], aindata[0][0]}, 
            {aoutdata[0][3], aoutdata[0][2], aoutdata[0][1], aoutdata[0][0]});  

   assign out00 = aoutdata[0][3];
   assign out01 = aoutdata[0][2];
   assign out02 = aoutdata[0][1];
   assign out03 = aoutdata[0][0];
   assign out10 = aoutdata[0][3];
   assign out11 = aoutdata[0][2];
   assign out12 = aoutdata[0][1];
   assign out13 = aoutdata[0][0];
   
task bottom;
   input  [31:0]  indata;
   output [31:0]  outdata;
begin
   outdata = indata;
end
endtask
endmodule

