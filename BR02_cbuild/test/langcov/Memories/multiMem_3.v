/* This test case tests multi-dimensional array word select passed as port *
 * connection and also it is populated from the master module.             */

module multiMem_3(addr1,addr2,clk,data,data1);
   input  addr1;
   input [0:1] addr2;
   input  clk;
   output [0:1] data;
   output [0:1] data1;

   integer loop_index_1;
   integer loop_index_2;
   reg [0:1] array3 [0:1][0:3];

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 0; loop_index_2 <= 3; loop_index_2 = loop_index_2 + 1)
       begin
       if (loop_index_1 == 0) begin
         if (loop_index_2 == 1) begin
           array3[loop_index_1][loop_index_2][0] = addr2 + 2'b00;
           array3[loop_index_1][loop_index_2][1] = addr2 + 2'b11;
         end
         else if (loop_index_2 == 2) begin
           array3[loop_index_1][loop_index_2] = addr2 + 2'b10 + addr1;
           array3[loop_index_1][loop_index_2] = addr2 + 2'b01;
         end
         else
           array3[loop_index_1][loop_index_2][0] = addr2 + 2'b11;
           array3[loop_index_1][loop_index_2][1] = addr2 + 2'b01;
       end
       else if (loop_index_1 == 1) begin
         if (loop_index_2 == 1) begin
           array3[loop_index_1][loop_index_2][0] = addr2 + 2'b10 + addr1;
           array3[loop_index_1][loop_index_2][1] = addr2 + 2'b01;
         end
         else if (loop_index_2 == 2) begin
           array3[loop_index_1][loop_index_2] = addr2 + 2'b11;
           array3[loop_index_1][loop_index_2] = addr2 + 2'b10;
         end
         else
           array3[loop_index_1][loop_index_2] = addr2 + 2'b11 + addr1;
           array3[loop_index_1][loop_index_2] = addr2 + 2'b01;
         end
       end
     end
end

   bot I(array3[addr1][addr2], data);
   bot I1(array3[addr1][addr1], data1);
endmodule

module bot (in,out);
    input [1:0] in;
    output [1:0] out;

    assign out = in;
endmodule
  
