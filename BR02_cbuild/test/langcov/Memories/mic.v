/* bug 5269 -- problem related to vector inference  */

module multiMem_1(addr1,addr2,clk,data);
   input  addr1;
   input [0:31] addr2;
   input  clk;
   output [0:127] data;

   integer loop_index_1;
   integer loop_index_2;
   reg [0:31] array3 [7:6];

always @(posedge clk)
begin
   for (loop_index_1 = 6; loop_index_1 <= 7; loop_index_1 = loop_index_1 + 1)
     begin
       array3[loop_index_1] = addr2;
     end
end

   assign data[0:31] = array3[7];
   assign data[32:63] = array3[7];
   assign data[64:95] = array3[6];
   assign data[96:127] = array3[6];

endmodule

