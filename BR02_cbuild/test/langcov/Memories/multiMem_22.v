/* This test case tests 

    a.  multi-dimensional array word select is in LHS.
    b.  multi-dimensional array word select is in RHS.
    c.  multi-dimensional array word select is populated by a scope variable.
    d.  multi-dimensional array net range is determined by parameter.
*/

module multiMem_4(addr1,addr2,clk,data, data1);
   input  addr1;
   input [0:2] addr2;
   input  clk;
   output [0:2] data;
   output [0:2] data1;

   parameter RANGE = 2;
   integer loop_index_1;
   integer loop_index_2;
   reg [0:RANGE] array3 [0:1][0:2];

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 0; loop_index_2 <= 2; loop_index_2 = loop_index_2 + 1) 
       begin 
       array3[loop_index_1][loop_index_2] = addr2 + loop_index_1 * 10 + loop_index_2;
       end
     end
end

   bot #(RANGE)I(array3[addr1][1+addr1], addr1, data, data1);
endmodule


module bot (in, addr, out, out1);
    parameter RANGE = 4;
    input  [RANGE:0] in;
    input  addr;
    output [RANGE:0] out;
    output [RANGE:0] out1;

    wire [0:RANGE] array3 [0:1][0:2];
    assign array3[0][0] = multiMem_4.addr2;
    assign array3[0][1] = multiMem_4.addr1 + multiMem_4.addr2;
    assign array3[0][2] = multiMem_4.addr1 + multiMem_4.addr2 + addr;
    assign array3[1][0] =  multiMem_4.addr2 + RANGE + addr;
    assign array3[1][1] =  multiMem_4.addr2 + RANGE + addr + 1'b1;
    assign array3[1][2] =  multiMem_4.addr2 + RANGE + addr + 2'b11;
    assign out = array3[addr][0] + array3[addr][addr + 1];
    assign out1 = {array3[addr][addr][addr], array3[addr][1+addr][1], array3[addr][addr][1+addr]};
endmodule

