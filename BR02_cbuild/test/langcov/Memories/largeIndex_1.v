// last mod: Tue Aug 29 13:39:54 2006
// filename: test/langcov/Memories/largeIndex_1.v
// Description:  This test is an attempt to define and use a memory that has an
// index range of 8 words but requires more than 32 bits to access. (related to
// bug 5836).  This example has a parser alert because the declared range
// exceeds 32 bit capacity.  But that alert is changed to a warning.


module largeIndex_1(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] 	out1;
   reg [7:0] 	mem [4294967397:4294967390]; // 8 bits and 8 words
   reg [2:0] 	addr;
   initial
     begin
	mem[4294967397] = 0;
	mem[4294967396] = 1;
	mem[4294967395] = 2;
	mem[4294967394] = 3;
	mem[4294967393] = 4;
	mem[4294967392] = 5;
	mem[4294967391] = 6;
	mem[4294967390] = 7;
     end
   always @(posedge clock)
     begin: main
	out1 = mem[in1];
     end
endmodule
