/* Multi-dimensional array word select element is passed as port connection to* 
 * the task.                                                                  */

module multiMem_13(addr1,addr2,clk,data);
   input  addr1;
   input  addr2;
   input  clk;
   output data;

   reg data;

   integer loop_index_1;
   integer loop_index_2;
   reg  array3 [0:1][6:4];

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 4; loop_index_2 <= 6; loop_index_2 = loop_index_2 + 1) 
       begin 
       array3[loop_index_1][loop_index_2] = addr2;
       end
     end
end

   always @(array3[addr1][4+addr2])
     AssignTask(array3[addr1][4+addr2], data);

task AssignTask;

input  in;
output out;
begin
   out = in;
end
endtask
endmodule
