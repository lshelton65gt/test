// March 2009
// This file tests four dimensional memories.

module bug12151_01(clk, rst,
		   in1, in2, in3, in4, out1, out2, out3, out4, out5, out6, out7, out8);
   input rst, clk;
   input[3:0]   in1;
   input[3:0]   in2;
   input[3:0]   in3;
   input[3:0]   in4;
   output    out1, out2, out3, out4, out5, out6, out7, out8;
   reg[3:0]  out1, out2, out3, out4, out5, out6, out7, out8;
   reg[14:11]   mem1[8:7][6:5][4:3];

   integer 	i, j, k, m, n;
   
   always @(posedge clk or posedge rst)
     begin
	if (rst)
	  begin
            for (i = 7; i <= 8; i = i+1) begin 
	      for (j = 5; j <= 6; j = j+1) begin
	        for (k = 3; k <= 4; k = k+1) begin
		  if (k == 3)
		    // This is part select
  		    mem1[i][j][k] <= 4'h0;
		  else
		    for (m = 11; m <= 14; m = m+1) begin
		       // This is bit select
		       mem1[i][j][k][m] <= 1'b1; 
		    end
   	        end
              end
	    end
	    out1 <= 4'h0;
	    out2 <= 4'h0;
	    out3 <= 4'h0;
	    out4 <= 4'h0;
	    out5 <= 4'h0;
	    out6 <= 4'h0;
	    out7 <= 4'h0;
	    out8 <= 4'h0;
	  end
	else
	  begin
	     mem1[7][5][3] <= in1;
	     mem1[7][5][4] <= ~in1;
	     mem1[7][6][3] <= in2;
	     mem1[7][6][4] <= ~in2;
	     mem1[8][5][3] <= in3;
	     mem1[8][5][4] <= ~in3;
	     mem1[8][6][3] <= in4;
	     mem1[8][6][4] <= ~in4;

	     // This is part select
	     out1 <= mem1[7][5][3];
	     out2 <= mem1[7][5][4];
	     out3 <= mem1[7][6][3];
	     out4 <= mem1[7][6][4];
	     out5 <= mem1[8][5][3];
	     out6 <= mem1[8][5][4];
	     out7 <= mem1[8][6][3];

  	     // This is bit select
	     for (n = 11; n <= 14; n = n+1) begin
		out8[n-11] <= mem1[8][6][3][n]; 
	     end
	  end
     end

endmodule			  
