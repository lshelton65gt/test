/* Multi-dimensional array part/bit select elements drives the output port of
 * the design. This is identical to multiMem_24.v except that the range of the
 * array aoutdata was changed to be 1 based instead of 0 based (both in decl and
 * usage.  This should not affect the simulation results, they should be 
 * identical to multiMem_24.v case
 */ 
module matrox_11 (in00, in01, in02, in03, in10, in11, in12, in13, out00, out01, out02, out03, out10, out11, out12, out13);

   input  [7:0] in00, in01, in02, in03, in10, in11, in12, in13;
   output [7:0] out00, out01, out02, out03, out10, out11, out12, out13;

   wire [7:0] aindata[1:0][3:0];
   wire [8:1] aoutdata[2:1][4:1];


   assign aindata[0][3] = in00;
   assign aindata[0][2] = in01;
   assign aindata[0][1] = in02;
   assign aindata[0][0] = in03;
   assign aindata[1][3] = in10;
   assign aindata[1][2] = in11;
   assign aindata[1][1] = in12;
   assign aindata[1][0] = in13;

   bottom B({aindata[0][3], aindata[0][2], aindata[0][1], aindata[0][0]}, 
            {aoutdata[1][4], aoutdata[1][3], aoutdata[1][2], aoutdata[1][1]});

   assign out00[4:0] = aoutdata[1][4][5:1];
   assign out00[7:5] = aoutdata[1][4][8:6];
   assign out01 = aoutdata[1][3];
   assign out02 = aoutdata[1][2];
   assign out03 = aoutdata[1][1];
   assign out10 = aoutdata[1][4];
   assign out11 = aoutdata[1][3];
   assign out12 = aoutdata[1][2];
   assign out13 = aoutdata[1][1];
   
endmodule


module bottom(indata, outdata);

   input  [31:0]  indata;
   output [31:0]  outdata;

   assign  outdata = indata;
endmodule

