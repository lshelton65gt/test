// last mod: Wed Apr  5 13:22:14 2006
// filename: test/langcov/Memories/largeIndex_3n.v
// Description:  This test has a multidimensional array with large index expressions
// this test is somewhat like largeIndex_3.v but memory indices are negative

module largeIndex_3n(input clock, input signed [7:0] in1, in2, output reg [7:0] out1, out2, out3, out4);


   reg [7:0] mem [-1023:0] [-511:0];
   integer   i, j;
   reg [7:0] temp; 

   initial
     begin
	for ( i = 0; i > -1024; i = i - 1 )
	  begin
	     for ( j = 0; j > -512; j = j - 1 )
		begin
		   temp = i*j;
		   $display("mem[%d][%d] = %h", i, j, temp);
		   mem[i][j] = i*j;
		end
	  end
     end
   
   always @(posedge clock)
     begin: main
	$display("in1: %d  in2: %d",in1, in2);
	out1 = mem[in1][in2];
	out2 = mem[{32'b0,in1}][{32'b0,in2}]; // index exprs are longer than 32 bits but have same value as for out1
	out3 = mem[{32'h00000000,in1[0],8'b0}][{32'h00000000,in2[0],7'h00}]; // index exprs are longer than 32 bits
	out4 = mem[{32'h80000000,in1[0],8'b0}][{32'h80000000,in2[0],7'h00}]; // index exprs are longer than 32 bits, but if truncated should be same as for out3	
     end
endmodule
