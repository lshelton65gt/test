/* Multi dimensional array bit select used in both LHS and RHS. Here the  *
 * parent element is a vector of (9 to 8) range.                          */

module multiMem_17(addr1,addr2,clk,data);
   input  addr1;
   input [0:1] addr2;
   input  clk;
   output [0:1] data;

   wire [0:1] data1;
   integer loop_index_1;
   integer loop_index_2;
   reg [9:8] array3 [0:1][5:6];

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 5; loop_index_2 <= 6; loop_index_2 = loop_index_2 + 1) 
       begin 
       array3[loop_index_1][loop_index_2][9] = addr2[0];
       array3[loop_index_1][loop_index_2][8] = addr2[1];
       end
     end
end

   assign data = {array3[0][5][9], array3[0][5][8]};
   assign data1 = array3[0][5];

   always @(data or data1)
   begin
     if (data == data1)
        $display ("PASSED");
     else
        $display ("FAILED");
   end
endmodule

