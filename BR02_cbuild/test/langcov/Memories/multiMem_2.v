/* Multi dimensional array bit select used in both LHS and RHS and multi  *
 * dimensional array bit select is used in the RHS.                       */

module multiMem_2(addr1,addr2,clk,data1, data2);
   input  addr1;
   input [0:1] addr2;
   input  clk;
   output [0:1] data1;
   output [0:1] data2;

   integer loop_index_1;
   integer loop_index_2;
   reg [0:1] array3 [0:1][0:3];

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 0; loop_index_2 <= 3; loop_index_2 = loop_index_2 + 1) 
       begin 
       if (loop_index_1 == 0) begin
         if (loop_index_2 == 1) begin
           array3[loop_index_1][loop_index_2][0] = addr2 + 2'b00;
           array3[loop_index_1][loop_index_2][1] = addr2 + 2'b11;
         end
         else if (loop_index_2 == 2) begin
           array3[loop_index_1][loop_index_2][0] = addr2 + 2'b10 + addr1;
           array3[loop_index_1][loop_index_2][1] = addr2 + 2'b01;
         end
         else
           array3[loop_index_1][loop_index_2][0] = addr2 + 2'b11;
           array3[loop_index_1][loop_index_2][1] = addr2 + 2'b01;
       end 
       else if (loop_index_1 == 1) begin
         if (loop_index_2 == 1) begin
           array3[loop_index_1][loop_index_2][0] = addr2 + 2'b10 + addr1;
           array3[loop_index_1][loop_index_2][1] = addr2 + 2'b01;
         end
         else if (loop_index_2 == 2) begin
           array3[loop_index_1][loop_index_2][0] = addr2 + 2'b11;
           array3[loop_index_1][loop_index_2][1] = addr2 + 2'b10;
         end
         else
           array3[loop_index_1][loop_index_2][0] = addr2 + 2'b11 + addr1;
           array3[loop_index_1][loop_index_2][1] = addr2 + 2'b01;
         end 
       end
     end
end

   assign data1[0] = array3[addr1][addr1][0];
   assign data1[1] = array3[addr1][addr2][1];

   assign {data2[1], data2[0]} = array3[addr1][addr1];
endmodule

