module top(in, out);

input in;
output out;

time time_array [1:0][0:1][0:1];

assign out = in;

endmodule

