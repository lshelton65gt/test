/* This test case test the result of an continuous assignments where    * 
 * multi-dimensional array bit select is used in the RHS here parent    *
 * multi-dim element has non-zero base range (array3 [6:5][6:4] -- here *
 * it is 6:4).                                                          */

module multiMem_5(addr1,addr2,clk,data,data1);
   input  addr1;
   input [0:1] addr2;
   input  clk;
   output [0:4] data;
   output [0:4] data1;

   integer loop_index_1;
   integer loop_index_2;
   integer count ;
   reg [0:4] array3 [6:5][4:6];

always @(posedge clk)
begin
   count = 0;
   for (loop_index_1 = 5; loop_index_1 <= 6; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 6; loop_index_2 >= 4; loop_index_2 = loop_index_2 - 1) 
       begin 
       array3[loop_index_1][loop_index_2] = addr2 + count;
       count = count + 1;
       end
     end
end
   assign data = array3[5+addr1][addr1 + 4];
   assign data1 = array3[5+addr1][addr1 + 5];
endmodule
