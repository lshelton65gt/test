/* Concatenation of multi-dimensional array word select elements passed as port*
 * connection to a function and the func output is used to populate the top    *
 * module's output port.                                                       */

module multiMem_16 (in00, in01, in02, in03, in10, in11, in12, in13, out00, out01, out02, out03, out10, out11, out12, out13);

   input  [7:0] in00, in01, in02, in03, in10, in11, in12, in13;
   output [7:0] out00, out01, out02, out03, out10, out11, out12, out13;

   wire [7:0] aindata[1:0][3:0];

   reg [31:0] out;  

   assign aindata[0][3] = in00;
   assign aindata[0][2] = in01;
   assign aindata[0][1] = in02;
   assign aindata[0][0] = in03;
   assign aindata[1][3] = in10;
   assign aindata[1][2] = in11;
   assign aindata[1][1] = in12;
   assign aindata[1][0] = in13;

always @(aindata[0][0])
   out = bottom({aindata[0][3], aindata[0][2], aindata[0][1], aindata[0][0]});  

   assign out00 = out[31:24];
   assign out01 = out[23:16];
   assign out02 = out[15:8];
   assign out03 = out[7:0];
   assign out10 = out[31:24];
   assign out11 = out[23:16];
   assign out12 = out[15:8];
   assign out13 = out[7:0];
   
function bottom;
   input  [31:0]  indata;
begin
   bottom = indata;
end
endfunction
endmodule

