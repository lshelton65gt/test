// March 2009
// This file tests four dimensional memories.

module bug12151_02(clk, rst,
		   in1, in2, in3, in4, out1, out2, out3, out4, out5, out6, out7, out8);
   input rst, clk;
   input[3:0]   in1;
   input[3:0]   in2;
   input[3:0]   in3;
   input[3:0]   in4;
   output       out1, out2, out3, out4, out5, out6, out7, out8;
   reg[3:0]     out1, out2, out3, out4, out5, out6, out7, out8;
   reg       mem1[8:7][6:5][4:3][14:11];

   integer 	i, j, k, m, n, p;
   
   always @(posedge clk or posedge rst)
     begin
	if (rst)
	  begin
            for (i = 7; i <= 8; i = i+1) begin 
	      for (j = 5; j <= 6; j = j+1) begin
	        for (k = 3; k <= 4; k = k+1) begin
	          for (m = 11; m <= 14; m = m+1) begin
  		    mem1[i][j][k][m] <= 1'h0;
		    end
   	        end
              end
	    end
	    out1 <= 4'h0;
	    out2 <= 4'h0;
	    out3 <= 4'h0;
	    out4 <= 4'h0;
	    out5 <= 4'h0;
	    out6 <= 4'h0;
	    out7 <= 4'h0;
	    out8 <= 4'h0;
	  end
	else
	  begin
	     for (n = 11; n <= 14; n = n+1) begin
		mem1[7][5][3][n] <= in1[n-11];
		mem1[7][5][4][n] <= ~in1[n-11];
		mem1[7][6][3][n] <= in2[n-11];
		mem1[7][6][4][n] <= ~in2[n-11];
		mem1[8][5][3][n] <= in3[n-11];
		mem1[8][5][4][n] <= ~in3[n-11];
		mem1[8][6][3][n] <= in4[n-11];
		mem1[8][6][4][n] <= ~in4[n-11];
	     end

	     for (p = 11; p <= 14; p = p+1) begin
		out1[p-11] <= mem1[7][5][3][p];
		out2[p-11] <= mem1[7][5][4][p];
		out3[p-11] <= mem1[7][6][3][p];
		out4[p-11] <= mem1[7][6][4][p];
		out5[p-11] <= mem1[8][5][3][p];
		out6[p-11] <= mem1[8][5][4][p];
		out7[p-11] <= mem1[8][6][3][p];
		out8[p-11] <= mem1[8][6][4][p];
	     end


	  end
     end

endmodule			  
