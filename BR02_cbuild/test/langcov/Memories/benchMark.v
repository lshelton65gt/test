/* This test case test the result of an continuous assignments , where   *
 * multi-dimensional array word select is used in the RHS.                */
module multiMem_1(addr1,addr2,clk,data);
   input  addr1;
   input [0:31] addr2;
   input  clk;
   output [0:127] data;

   integer loop_index_1;
   integer loop_index_2;
   reg [0:31] array3 [0:1][0:2];

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 0; loop_index_2 <= 2; loop_index_2 = loop_index_2 + 1) 
       begin 
       array3[loop_index_1][loop_index_2] = addr2 + loop_index_1 + loop_index_2;
       end
     end
end

   assign data[0:31] = array3[0][0];
   assign data[32:63] = array3[0][2];
   assign data[64:95] = array3[1][0];
   assign data[96:127] = array3[1][2];

endmodule

