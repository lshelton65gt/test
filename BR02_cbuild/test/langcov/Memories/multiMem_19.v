/* Multi dimensional array bit select used in both LHS and RHS. Here the  *
 * parent element is a vector of (0 to 3) range.                          */

module multiMem_19(addr1,addr2,clk,data);
   input  addr1;
   input [0:3] addr2;
   input  clk;
   output [0:3] data;

   wire [0:3] data1;
   integer loop_index_1;
   integer loop_index_2;
   reg [0:3] array3 [0:1][9:6];

initial
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 9; loop_index_2 >= 6; loop_index_2 = loop_index_2 - 1)
       begin
       array3[loop_index_1][loop_index_2][0] <= 0;
       array3[loop_index_1][loop_index_2][1] <= 0;
       array3[loop_index_1][loop_index_2][2] <= 0;
       array3[loop_index_1][loop_index_2][3] <= 0;
       end
     end

end

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 9; loop_index_2 >= 6; loop_index_2 = loop_index_2 - 1) 
       begin 
       array3[loop_index_1][loop_index_2][0] <= addr2[0];
       array3[loop_index_1][loop_index_2][1] <= addr2[1];
       array3[loop_index_1][loop_index_2][2] <= addr2[2];
       array3[loop_index_1][loop_index_2][3] <= addr2[3];
       end
     end
end

   assign data = {array3[0][6][0], array3[0][7][1], array3[0][8][2], array3[0][9][3]};
   assign data1 = array3[0][8];

   always @(posedge clk)
   begin
     if (data == data1)
        $display ("PASSED");
     else
        $display ("FAILED");
   end
endmodule

