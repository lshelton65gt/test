/* Multi dimensional array bit select used in RHS. Here the               *
 * parent element is a vector of (0 to 2) range.                          */

module multiMem_18(addr1,addr2,clk,data);
   input  addr1;
   input [0:2] addr2;
   input  clk;
   output [0:2] data;

   wire [0:2] data1;
   integer loop_index_1;
   integer loop_index_2;
   reg [0:2] array3 [0:1][5:6];

initial
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 5; loop_index_2 >= 6; loop_index_2 = loop_index_2 + 1)
       begin
       array3[loop_index_1][loop_index_2][0] <= 0;
       array3[loop_index_1][loop_index_2][1] <= 0;
       array3[loop_index_1][loop_index_2][2] <= 0;
       end
     end

end

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 5; loop_index_2 <= 6; loop_index_2 = loop_index_2 + 1) 
       begin 
       array3[loop_index_1][loop_index_2] <= addr2 + 3'b011 + loop_index_1 + loop_index_2;
       end
     end
end


   assign data = {array3[addr1][5 + addr1][0], array3[0][5 + addr1][1], array3[addr1][5 + addr1][2]};
   assign data1 = array3[addr1][5 + addr1];

   always @(posedge clk)
   begin
     if (data == data1)
        $display ("PASSED");
     else
        $display ("FAILED");
   end
endmodule

