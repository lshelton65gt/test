// last mod: Mon Nov  7 07:02:31 2005
// filename: test/langcov/Memories/aldec8_package.v
// Description:  This test was reported to aldec 11/04/05, it is aldec tracking
// number 11725

// description in this test the aldec simulation results at time 5 are incorrect.
// In the output of the simulation of this design with riviera we get:
//        5 clk: 1, addr1: 1, addr2: 01, data1: 00, data2: 01
// I think we should get:
//        5 clk: 1, addr1: 1, addr2: 01, data1: 10, data2: 01

// currently this testcase cannot be run with carbon (waiting for 3D memory support)

module aldec8_package(output [0:1] data1,
		      output [0:1] data2);


   reg 				   addr1;
   reg [0:1] 			   addr2;
   reg 				   clk;

   
   initial
      begin
	 clk = 1'b0;
	 addr1 = 1'b0;
	 addr2 = 2'b11;
	 #1 $display ($time, " clk: %b, addr1: %b, addr2: %b, data1: %b, data2: %b", clk, addr1, addr2, data1, data2);
	 #1 clk = 1'b1;
	 #1 $display ($time, " clk: %b, addr1: %b, addr2: %b, data1: %b, data2: %b", clk, addr1, addr2, data1, data2);
	 #1  addr1 = 1'b1;
	 addr2 = 2'b01;
	 #1 $display ($time, " clk: %b, addr1: %b, addr2: %b, data1: %b, data2: %b", clk, addr1, addr2, data1, data2);
      end

   dut u1(addr1, addr2, clk, data1, data2);
   
endmodule

module dut(addr1,addr2,clk,data1, data2);
  input  addr1;
  input [0:1] addr2;
  input  clk;
  output [0:1] data1;
  output [0:1] data2;

  integer loop_index_1;
  integer loop_index_2;
  reg [0:1] array3 [0:1][0:3];

always @(posedge clk)
begin
  for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
    begin
    for (loop_index_2 = 0; loop_index_2 <= 3; loop_index_2 = loop_index_2 + 1)
      begin
      if (loop_index_1 == 0) begin
        if (loop_index_2 == 1) begin
          array3[loop_index_1][loop_index_2][0] = addr2 + 2'b00;
          array3[loop_index_1][loop_index_2][1] = addr2 + 2'b11;
        end
        else if (loop_index_2 == 2) begin
          array3[loop_index_1][loop_index_2][0] = addr2 + 2'b10 + addr1;
          array3[loop_index_1][loop_index_2][1] = addr2 + 2'b01;
        end
        else
          array3[loop_index_1][loop_index_2][0] = addr2 + 2'b11;
          array3[loop_index_1][loop_index_2][1] = addr2 + 2'b01;
      end
      else if (loop_index_1 == 1) begin
        if (loop_index_2 == 1) begin
          array3[loop_index_1][loop_index_2][0] = addr2 + 2'b10 + addr1;
          array3[loop_index_1][loop_index_2][1] = addr2 + 2'b01;
        end
        else if (loop_index_2 == 2) begin
          array3[loop_index_1][loop_index_2][0] = addr2 + 2'b11;
          array3[loop_index_1][loop_index_2][1] = addr2 + 2'b10;
        end
        else
          array3[loop_index_1][loop_index_2][0] = addr2 + 2'b11 + addr1;
          array3[loop_index_1][loop_index_2][1] = addr2 + 2'b01;
        end
      end
    end
end

  assign data1[0] = array3[addr1][addr1][0];
  assign data1[1] = array3[addr1][addr2][1];

  assign {data2[1], data2[0]} = array3[addr1][addr1];
endmodule
