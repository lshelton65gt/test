/* Multi dimensional array population (by a net)  and the array bit select is * 
 * used for conditional branching.                                            */

module matrox_6 (clk, data, outp);
   input  clk;
   input  data;
   output outp;

   reg [3:0] array_of_8_array_of_4_array_of_4[7:0][3:0];

   integer loop_index_1;
   integer loop_index_2;
   integer loop_index_3;
   reg     v;

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 7; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 0; loop_index_2 <= 3; loop_index_2 = loop_index_2 + 1) 
       begin 
         array_of_8_array_of_4_array_of_4[loop_index_1][loop_index_2] = data;
       end
     end
end

always @(posedge clk, data)
begin
   
   v = 1'b0;
   for (loop_index_1 = 0; loop_index_1 <= 7; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 0; loop_index_2 <= 3; loop_index_2 = loop_index_2 + 1) 
       begin 
       for (loop_index_3 = 0; loop_index_3 <= 3; loop_index_3 = loop_index_3 + 1)
         begin
           if ( array_of_8_array_of_4_array_of_4[loop_index_1][loop_index_2][loop_index_3] != data)
             v = 1'b0;
         end
       end
     end
end
   assign outp = v;

endmodule

