// filename: size01.v
// Description:  This test demonstrates a simulation mismatch we saw at Customer 1 (constant range).


module size01(clock, in1, in3, out1) ;
   parameter WIDTH10 = 10;
   parameter WIDTH3 = 3;
   input clock;
   input  in1;
   input       in3;
   output [13:0] out1;
   reg [13:0] out1;
   reg [2:0] reg1 [7:0];

   wire [13:0] wire1;
   wire [10:0] wire3 [1:0];
   genvar      g1;

   assign wire1 = {1'b1, ( wire3[1][WIDTH10-1:0]), 3'b0};	   
   
   always @(posedge clock)
     begin: main
	out1 = wire1;
     end
endmodule
