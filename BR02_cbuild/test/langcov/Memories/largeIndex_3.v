// last mod: Tue Apr  4 09:39:55 2006
// filename: test/langcov/Memories/largeIndex_3.v
// Description:  This test has a multidimensional array with large index expressions


module largeIndex_3(input clock, input [7:0] in1, in2, output reg [7:0] out1, out2, out3, out4);


   reg [7:0] mem [1023:0] [512:0];
   integer   i, j;

   initial
     begin
	for ( i = 0; i < 1024; i = i + 1 )
	  begin
	     for ( j = 0; j < 512; j = j + 1 )
	       mem[i][j] = i*j;
	  end
     end
   
   always @(posedge clock)
     begin: main
	out1 = mem[in1][in2];
	out2 = mem[{32'b0,in1}][{32'b0,in2}]; // index exprs are longer than 32 bits but have same value as for out1
	out3 = mem[{32'h00000000,in1[0],8'b0}][{32'h00000000,in2[0],7'h00}]; // index exprs are longer than 32 bits
	out4 = mem[{32'h80000000,in1[0],8'b0}][{32'h80000000,in2[0],7'h00}]; // index exprs are longer than 32 bits, but if truncated should be same as for out3	
     end
endmodule
