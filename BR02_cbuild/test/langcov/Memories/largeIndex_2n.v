// last mod: Wed Apr  5 13:06:02 2006
// filename: test/langcov/Memories/largeIndex_2n.v
// Description:  This test has a normal size memory but the index expressions
// used to address the memory are of different sizes, some larger than 32 bits.


module largeIndex_2n(input clock,
		     input signed [9:0] in1, in2,
		     output reg [7:0] out1,out2,out3,out4,out5);

   reg [7:0] mem [-1023:0];
   integer   i;
   initial
     begin
	for (i = -1023; i <= 0; i = i + 1 )
	  mem[i] = i;
     end

   always @(posedge clock)
     begin: main
	out1 = mem[in1];
	out2 = mem[in1+33'b1]; // index expression is 33 bits
	out3 = mem[{32'b0,in1}];	// index expression is 40 bits, but same value as for out1
	out4 = mem[{in2,in1,in2,in1}];	// index expression is 40 bits
	out5 = mem[(in1[0] ? ((in1+33'b0)<<in2) : ((in2+66'b0)<<in1))]; // index expression is 66 bits
     end
endmodule
