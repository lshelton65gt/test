/* Continuous assignment's LHS and RHS contains concatenation of           * 
 * multi-dimensional array word selects.                                   */

module matrox_11 (in00, in01, in02, in03, in10, in11, in12, in13, out00, out01, out02, out03, out10, out11, out12, out13);

   input  [7:0] in00, in01, in02, in03, in10, in11, in12, in13;
   output [7:0] out00, out01, out02, out03, out10, out11, out12, out13;

   wire [7:0] aindata[1:0][6:3];
   wire [7:0] aoutdata[1:0][12:9];


   assign aindata[0][6] = in00;
   assign aindata[0][5] = in01;
   assign aindata[0][4] = in02;
   assign aindata[0][3] = in03;
   assign aindata[1][6] = in10;
   assign aindata[1][5] = in11;
   assign aindata[1][4] = in12;
   assign aindata[1][3] = in13;

   assign {aoutdata[0][12], aoutdata[0][11], aoutdata[0][10], aoutdata[0][9]} 
       =   {aindata[0][6], aindata[0][5], aindata[0][4], aindata[0][3]}; 
   assign {aoutdata[1][12], aoutdata[1][11], aoutdata[1][10], aoutdata[1][9]} 
      = {aindata[1][6], aindata[1][5], aindata[1][4], aindata[1][3]};  

   assign {out00, out01, out02, out03} = {aoutdata[0][12], aoutdata[0][11], aoutdata[0][10], aoutdata[0][9]};
   assign {out10, out11, out12, out13} = {aoutdata[1][12], aoutdata[1][11], aoutdata[1][10], aoutdata[1][9]};
   
endmodule

