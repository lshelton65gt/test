/* Multi dimensional array bit select used in both LHS and RHS. Here the  *
 * parent element is a vector of (0 to 3) range. Here concatenation of    *
 * bit selects are populated from scope variable.                         */

module multiMem_20(addr1,addr2,clk,data, data1);
   input  addr1;
   input [0:3] addr2;
   input  clk;
   output [0:3] data;

   output [0:3] data1;
   integer loop_index_1;
   integer loop_index_2;
   reg [0:3] array3 [0:1][9:6];

always @(posedge clk)
begin
   for (loop_index_1 = 0; loop_index_1 <= 1; loop_index_1 = loop_index_1 + 1)
     begin
     for (loop_index_2 = 9; loop_index_2 >= 6; loop_index_2 = loop_index_2 - 1) 
       begin 
       array3[loop_index_1][loop_index_2][0] = addr2[0] | loop_index_1;
       array3[loop_index_1][loop_index_2][1] = addr2[1] & loop_index_2;
       array3[loop_index_1][loop_index_2][2] = addr2[2] | loop_index_2;
       array3[loop_index_1][loop_index_2][3] = addr2[3] & loop_index_1;
       end
     end
end

   assign data1 = array3[0][8];

   BOTTOM B(data, {array3[0][6][0], array3[0][7][1], array3[0][8][2], array3[0][9][3]});
endmodule

module BOTTOM(out, in);

    output [0:3] out;
    input [0:3] in;

    wire [0:3] array3 [0:1][8:5];

    assign array3[0][8] = multiMem_20.data1;
    assign array3[0][7] = multiMem_20.addr2;
    assign array3[0][6] = multiMem_20.addr2 + in;
    assign array3[0][5] = multiMem_20.addr2 + in + multiMem_20.data1;
    assign out = {array3[0][8][0], array3[0][7][1], array3[0][6][2], array3[0][5][3]};
endmodule
