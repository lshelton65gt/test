// last mod: Tue Aug 29 10:37:57 2006
// filename: test/langcov/Memories/scalar2DArray.v
// Description:  This test defines a 2D array of scalar values.


module scalar2DArray(clock, in1, in2, add1, add2, out1);
   input clock;
   input [7:0] in1, in2;
   input [1:0] add1;
   input [2:0] add2;
   output [7:0] out1;
   reg [7:0] out1;
   reg 	     mem[3:0] [7:0];
   reg 	     temp;
   integer   i, j;
   initial
      begin
	 for ( i = 0; i <= 3; i = i + 1)
	   for ( j = 0; j <= 7; j = j + 1)
	     mem[i][j] = i[0];
      end

   always @(posedge clock)
     begin: main
	t1(add1,add2,temp);
       out1 = mem[add1][add2] & temp;
	$display("main out1: %b", out1);
     end

   task t1;
      input [1:0] add1;
      input [2:0] add2;
      output out;
      reg 	     local_mem[3:0] [7:0];
      integer i,j;
      begin
	 for ( i = 0; i <= 3; i = i + 1)
	   for ( j = 0; j <= 7; j = j + 1)
	     local_mem[i][j] = i[1];

	 out = local_mem[add1][add2];
	 $display("task out: %b, local_mem[1][7]: %b, add1: %d, add2: %d", out, local_mem[1][7], add1, add2);
      end
   endtask
   
endmodule
