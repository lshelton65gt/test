module top (io, out, clk, in);
   inout [7:0]  io;
   output [3:0] out;
   input 	clk;
   input [3:0] 	in;

   pullup P1 [7:0] (io);
   assign 	io[3:0] = ~in[3:0];

   reg [3:0] 	out;
   always @ (posedge clk)
     out <= io[3:0] & io[7:5];

endmodule // top

   
