// last mod: Tue Jan 16 10:11:03 2007
// filename: test/langcov/verilogCase/casez9.v
// Description:  This test checks to see how casex stmt labels are handled if
// they are defined by parameters.



module casez9(clock, in1, state, out1);
   input clock;
   input [7:0] in1;
   output out1;
   reg 	  out1;
   output    [3:0] state;
   reg 	     [3:0] state;
   wire 		   temp;
   wire      [3:0] new_state;

   initial
      begin
	 state = 0;
      end

   mid U1( state, in1, temp, new_state );
   
   always @(posedge clock)
     begin: main
	state = new_state;
	out1 = temp;
     end
endmodule


module mid(sel,in,out, newSel);
   parameter STATE0 = 0;
   parameter STATE1 = 1;
   parameter STATE2 = 2;
   parameter STATE3 = 3;
   parameter STATE4 = 4;
   parameter STATE5 = 5;
   parameter STATE6 = 6;
   parameter STATE7 = 7;
   parameter STATE8 = 8;
   parameter STATEN1 = 32'sb???????????????????????????11111;
   parameter STATEN8 = 32'sb???????????????????????????11000;

  input [3:0] sel;
  input [7:0] in;
  output out;
   output [3:0] newSel;
   reg 		out;
   reg    [3:0] newSel;
   

   initial
     begin
	out = 0;
	newSel = 0;
     end
   
  always @(sel or in)
    casez(sel)
      // a branch with an unreachable label
      STATEN1: begin out = in[1]; newSel = STATEN8; $display("this line should never be executed, newSel: %d", newSel); end
      // a branch with an unreachable label
      STATEN8: begin out = in[1]; newSel = STATEN1; $display("this line should never be executed, newSel: %d", newSel); end
      // the following branches have rechable labels
      STATE0: begin out = in[0]; newSel = 7; end
      STATE1: begin out = in[1]; newSel = 0; end
      STATE2: begin out = in[3]; newSel = 6; end
      STATE3: begin out = in[2]; newSel = 2; end
      STATE4: begin out = in[7]; newSel = 8; end
      STATE5: begin out = in[6]; newSel = 3; end
      STATE6: begin out = in[4]; newSel = 4; end
      STATE7: begin out = in[5]; newSel = 5; end
      STATE8: begin out = in[1]; newSel = 1; end
    endcase
endmodule
