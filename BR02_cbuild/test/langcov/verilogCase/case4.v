module top(sel,in,out);
  input [3:0] sel;
  input [3:0] in;
  output out;
  reg out;

  always @(sel or in)
    case(sel)
      4'bx0z0: out = in[0];
      4'bx0z1: out = in[1];
      4'bz1z0: out = in[2];
      4'bx1x1: out = in[3];
      default: out = 1'b0;
    endcase
endmodule
