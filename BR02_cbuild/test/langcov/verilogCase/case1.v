module top(sel,in,out);
  input [1:0] sel;
  input [3:0] in;
  output out;
  reg out;

  always @(sel or in)
    case(sel)
      2'bxx: out = in[0];
      default: out = 1'b0;
    endcase
endmodule
