module top(in,out);
  input [1:0] in;
  output out;
  reg out;

  always @(in)
    casex(3'b10x)
      3'b101: out = in[0];
      3'b100: out = in[1];
      default: out = 1'b0;
    endcase
endmodule
