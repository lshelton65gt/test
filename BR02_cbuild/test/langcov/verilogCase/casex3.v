module top(sel,in,out);
  input [1:0] sel;
  input [3:0] in;
  output out;
  reg out;

  always @(sel or in)
    casex(sel)
      2'bzz: out = 1'b1;
      2'b00: out = in[0];
      2'b01: out = in[1];
      2'b10: out = in[2];
      2'b11: out = in[3];
      default: out = 1'b0;
    endcase
endmodule
