module top(sel,in,out);
  input [1:0] sel;
  input [3:0] in;
  output out;
  reg out;

  always @(sel or in)
    case(2'b10)
      2'bxx: out = in[0];
      2'b01: out = in[1];
      3'b010: out = in[2];
      2'b011: out = in[3];
      4'b0000: out = in[0];
      default: out = 1'b0;
    endcase
endmodule
