module top(sel,in,out);
  input [1:0] sel;
  input [3:0] in;
  output out;
  reg out;

  always @(sel or in)
    casex(sel)
      1'bx: out = in[0];
      6'bzzx001: out = in[1];
      3'b010: out = in[2];
      4'bz011: out = in[3];
      4'b1011: out = 1'b1;
      default: out = 1'b0;
    endcase
endmodule
