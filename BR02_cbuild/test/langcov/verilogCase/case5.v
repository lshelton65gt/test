module top(sel,in,out);
  input [1:0] sel;
  input [3:0] in;
  output out;
  reg out;

  always @(sel or in)
    case(sel)
      1'b0: out = in[0];
      6'b000001: out = in[1];
      3'b010: out = in[2];
      4'b0011: out = in[3];
      4'b1011: out = 1'b1;
      default: out = 1'b0;
    endcase
endmodule
