module top(item1,item2,in,out);
  input [3:0] in;
  input [2:0] item1;
  input [2:0] item2;
  output out;
  reg out;

  always @(item1 or item2 or in)
    casex(3'b10x)
      item1: out = in[0];
      item2: out = in[1];
      3'b110: out = in[2];
      default: out = in[3];
    endcase
endmodule
