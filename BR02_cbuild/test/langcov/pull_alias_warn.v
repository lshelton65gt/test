/* Pullup/down warnings coming from elaboration */
module top(io1, o, en, data);
  inout io1;
  input en, data;
  output o;

  // pullup/down on same net
  pullup(io1);
  bar inst1 (io1, en, data);

  // pullup on a trireg
  trireg reg1;
  baz inst2 (reg1, en, data);

  // pulldown on a trireg
  wire reg2;
  pulldown (reg2);
  caz inst3 (reg2, en, data);

  // pullup on a tri0
  tri0 reg3;
  baz inst4 (reg3, en, data);

  // pulldown on a tri1
  tri1 reg4;
  foo inst5 (reg4, en, data);

  // Make sure the nets are alive
  assign o = reg1 & reg3 & reg2 & io1 & reg4;
endmodule

module bar(io, e, d);
  inout io;
  input e, d;
  foo inst1 (io, e, d);
endmodule

module foo(io, e, d);
  inout io;
  input e, d;
  pulldown (io);
  assign io = e ? d : 1'bz;
endmodule

module baz(o, e, d);
  output o;
  input e, d;
  pullup (o);
  assign o = e ? d : 1'bz;
endmodule

module caz(o, e, d);
  output o;
  input e, d;
  trireg o;
  assign o = e ? d : 1'bz;
endmodule
