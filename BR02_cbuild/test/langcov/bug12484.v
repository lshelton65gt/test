// The problem here is that the part select in[BITS::11]
// is actually reversed from the original net range direction.
// This makes the range null, and an error should be issued.
module bug12484(in, out);
   parameter BITS = 9;

   input [31:0] in;
   output [BITS-1:0] out;

   reg [BITS-1:0]    out;
   always @(in) begin
      if (BITS <= 10) begin
         out[BITS-1:0] = in[BITS-1:0];
      end
      else begin
         out[9:0] = in[9:0];
         out[BITS-1:10] = in[BITS:11]; // Null range
      end
   end
endmodule
