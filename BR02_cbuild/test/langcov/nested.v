module top(in, out);
  input in;
  output out;
  sub sub(.a(in), .z(out));
endmodule

module sub(a, z);
  output z;
  input a;
  reg z;

  always @(a)
    z = a;
endmodule

  