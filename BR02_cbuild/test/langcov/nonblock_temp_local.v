/* Test nonblocking assign temps and block locals. */
module top(i, clk,
           o
          );

input clk;

input [2:0] i;
reg [3:0] r, a, b;
output [1:0] o;

always @(posedge clk)
begin : named1
  begin : named2
    a <= r;
  end
  r = i;
  begin : named3
    b <= a + r;
  end
end

endmodule
