/* Test warning of unsupported system tasks */
module aBug (clk);
   input clk;
   always @(posedge clk) begin
      $display("time to dump core");
   end
   
endmodule
