module top (out, in);
   output [31:0] out;
   input [31:0]  in;

   sub1 S1();
   sub2 S2(out, in);
endmodule // top


module sub2 (out, in);
   output [31:0] out;
   input [31:0]  in;

   task initval;
      begin
         S1.value = 32;
      end
   endtask // initval
 
   integer       i;
   reg [31:0]    out;
   always @ (in)
     begin
        initval;
        for (i = 0; i < S1.value; i = i + 1) begin
           out[i] = in[i];
        end
     end

endmodule // top

module sub1();

   integer value;

endmodule // sub
