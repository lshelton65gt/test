/* Test binary operators. */
module top (plus, minus, mult, div, mod, eq, neq, trieq, trineq, logand, logor,
            lt, lte, gt, gte, bitand, bitor, bitxor, bitxnor, rshift, lshift,
            i1, i2);

input [-2:1] i1, i2;
output [0:-3] plus, minus, mult, div, mod, eq, neq, trieq, trineq, logand, logor;
output [55:58] lt, lte, gt, gte, bitand, bitor, bitxor, bitxnor, rshift, lshift;

assign plus = i1 + i2;
assign minus = i1 - i2;
assign mult = i1 * i2;
assign div = i1 / i2;
assign mod = i1 % i2;
assign eq = i1 == i2;
assign neq = i1 != i2;
assign trieq = i1 === i2;
assign trineq = i1 !== i2;
assign logand = i1 && i2;
assign logor = i1 || i2;
assign lt = i1 < i2;
assign lte = i1 <= i2;
assign gt = i1 > i2;
assign gte = i1 >= i2;
assign bitand = i1 & i2;
assign bitor = i1 | i2;
assign bitxor = i1 ^ i2;
assign bitxnor = i1 ~^ i2;
assign rshift = i1 >> i2;
assign lshift = i1 << i2;
endmodule
