module top ( out, in, clk );
   input clk;
   input [7:0] in;
   output [7:0] out;
   reg [7:0] 	out;

   integer 	i;
   
   always @(posedge clk)
     begin
	out[0] <= in[7];
	for ( i = 1 ; i < 7 ; i = i + 1 )
	  begin
	     out[i] <= in[i];
	  end
	out[7] <= in[0];
     end
endmodule // top
