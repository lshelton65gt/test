// DEcember 2007
// This file tests arrays of supply0/1 nets.
// CBUILD doesn't support these type of nets. It fails with CARBON_INTERNAL_ERROR. 
// This test case should be compiled with -2000 switch.

module bug8127_01 (clk);
  input clk;

  supply0[2:0]  arraySupply0 [0:3];
  supply1[2:0]  arraySupply1 [0:3];


  supply0  bitArraySupply0   [0:3];
  supply1  bitArraySupply1   [0:3];
   
endmodule
