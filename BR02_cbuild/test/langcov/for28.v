// Test initial blocks with big for loops
// originally we did not unroll for loops in initial blocks,
//  now the problem is that if we do not unroll the loop (because it is too big)
//   then we will set up a flow within the initial block and this causes scheduling problems.

// this testcase will work fine if LOOPLENGTH is 10.
`define LOOPLENGTH 1048576
`define WIDTH 2
module top(clk, out);
   input clk;
   output [31:0] out;
   reg [31:0] out;
   reg [31:0] temp;
   integer    i;

   initial
     begin 
	for (i = 0; i < `LOOPLENGTH; i = i + 1)
	   begin
	      temp = i;
	   end
	out = temp;
     end

   always @(posedge clk)
     begin
	out = 0;
     end
   
endmodule
