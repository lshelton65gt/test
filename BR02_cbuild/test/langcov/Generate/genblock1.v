// last mod: Tue Oct 26 17:09:36 2004
// filename: test/langcov/Generate/genblock1.v
// Description:  This test contains a generate block that contains some
// expressions, including an if stmt that will be handled as a generate_if
// from bug2939

module genblock1(a, b, c);
   input a, b;
   output c;

   parameter OP = "AND";

   generate begin
      if (OP == "AND") begin
	 assign c = a & b;
      end
      else if (OP == "OR") begin
	 assign c = a | b;
      end
      else 
	assign c = 0;
   end
   endgenerate
endmodule
