// last mod: Wed Mar 14 15:58:16 2007
// filename: test/langcov/Generate/gencase11.v
// Description:  This test was reported to be a problem by AMCC (bug7122)
// the issue is that cheetah complains that foo2, foo3 and foo4 are not
// defined, but they are not needed.
// aldec has problems with this test, but if you comment out the unused branches
// of the generate then you can get aldec to simulate

module gencase11(clk, w_in1, in1, in2, out1, w_out1);
   input clk;
   input w_in1;
   input [7:0] in1, in2;
   output [7:0] out1;
   output 	w_out1;
   reg [7:0] out1;

parameter TARGET_TECH = "FOO1";
 

generate
   case ("FOO1")

            "FOO1":  foo1 foo1 (
                        .clkin(clk),
                        .in1  (w_in1),
                        .out1 (w_out1)
                        );
 

            "FOO2":  foo2 foo2 (
                        .clkin(clk),
                        .in1  (w_in1),
                        .out1 (w_out1)
                        );

 

            "FOO3":  foo3  foo3 (
                        .clkin(clk),
                        .in1  (w_in1),
                        .out1 (w_out1)
                        );

 

            default: foo4 foo4 (
                        .clkin(clk),
                        .in1  (w_in1),
                        .out1 (w_out1)
                        );

   endcase
endgenerate

   
   always @(posedge clk)
     begin: main
       out1 = in1 & in2;
     end
endmodule


module foo1(input clkin, input in1, output reg out1);

   initial
     out1 = 0;
   
   always @(posedge clkin)
     begin: main
       out1 = in1;
     end
endmodule
