// filename: test/langcov/Generate/bug16152.v
// Description:  This test has an infinite loop in the second generate loop.
// In order to find the identical problem in bug 16152 it was necessary to run
// cbuild in the debugger.  Now this kind of problem is automatically detected.


module bug16152 (bin, gray);
  parameter SIZE = 8;                  // this module is parameterizable
  output [SIZE-1:0] bin;
  input [SIZE-1:0] gray;

  genvar i;

  reg foo;

  generate
   for ( i = 0; i < 7; i = i + 1)
     begin:this_is_the_label
        if ( SIZE == 7 )
	  begin:this_is_the_label
	     assign bin[i] = ^gray[SIZE-1:i];
	  end
     end
    if ( SIZE == 8 )
       begin
	  wire [SIZE-1:0] localvar;
	  for ( i = 0; i < 8; i =  + 1)	// the error is on this line, where i is not incremented for each iteration
	    begin:this_is_the_label
	       assign localvar[i] = ^gray[SIZE-1:i];
	    end
	  assign      bin = localvar;
       end
  endgenerate

endmodule
