// testing various uses of named generate blocks
// testcase test/langcov/Generate/genenamedblocks2.v
// here we have:
//   two named generate blocks with the same name (this is expected to create a conflict)
module top(clk, in1, in2, out1, out2);
   input clk;
   input [31:0] in1, in2;
   output [31:0] out1, out2;

   parameter 	 A = 1;
   parameter 	 B = 1;

   generate begin: out1_block
      if (A) begin
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in1;
         assign out1 = outreg;
      end
      else begin
         assign out1 = 32'h0;
      end
   end
   endgenerate
      
   generate begin: out1_block
      if (B) begin
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in2;
         assign out2 = outreg;
      end
      else begin
         assign out2 = 32'h0;
      end
   end
   endgenerate
      
endmodule

