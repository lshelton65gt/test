// filename: bug16445_b.v
// Description:  This test was derived from bug16445, but here the test is setup
// to check that the proper value for the partselect of i is NOT THE SAME for
// the two instances of foo_ctl.  So we do not expect that out1 and out2 are
// identical. but with the added $display we will see the parameter value in the
// text output.


module bug16445_b(clock, in1, out1, out2) ;
   input clock;
   input [5:0] in1;
   output [5:0] out1;
   output [5:0] out2;

   genvar    i;
   generate for (i=3; i<6; i=i+1) begin : gen_foo_ctl
      foo_ctl #(i) u_foo_a(clock, in1[i],out1[i]);      // this type of parameter specification does create 3 different modules (as expected)
      foo_ctl #(i[2:1]) u_foo_b(clock, in1[i],out2[i]);	// this type of parameter specification fails to create 3 different modules
   end
   endgenerate
   

endmodule

module foo_ctl #(parameter [1:0] FOO_ID = 2'b00)(input clock, input in1, output out1);
   reg outr;

   assign out1 = outr;
   always @(posedge clock)
     begin: main
	$display("%m FOO_ID: %d", FOO_ID);
	if ( FOO_ID == 2'b0) 
	  outr = 0;
	else if ( FOO_ID == 2'b1) 
	  outr = in1;
	else if ( FOO_ID == 2'b10) 
	  outr = !in1;
     end
endmodule

