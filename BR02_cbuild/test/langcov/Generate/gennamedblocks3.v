// testing various uses of named generate blocks
// testcase test/langcov/Generate/genenamedblocks3.v
// here we have:
//   two un-named generate blocks that declare a reg with the same name (this is expected to create a conflict)
module top(clk, in1, in2, out1, out2);
   input clk;
   input [31:0] in1, in2;
   output [31:0] out1, out2;

   parameter 	 A = 1;
   parameter 	 B = 1;

   generate begin
      if (A) begin : block_A
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in1;
         assign out1 = outreg;
      end
      else begin
         assign out1 = 32'h0;
      end
   end
   endgenerate
      
   generate begin
      if (B) begin : block_B
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in2;
         assign out2 = outreg;
      end
      else begin
         assign out2 = 32'h0;
      end
   end
   endgenerate
      
endmodule

