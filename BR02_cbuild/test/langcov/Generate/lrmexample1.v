// This is Example 1 of section 12.1.3.2 of the 2001 Verilog LRM.  It's a for
// generate statement that generates continuous assignments.


module gray2bin1 (bin, gray);
       parameter SIZE = 8;                  // this module is parameterizable
       output [SIZE-1:0] bin;
       input [SIZE-1:0] gray;
       genvar i;
       generate for (i=0; i<SIZE; i=i+1) begin:bit
          assign bin[i] = ^gray[SIZE-1:i];
       end endgenerate
   endmodule
