module test(clk, rstn, din1, din2, dout);
parameter DIN_SIZE = 8;
input clk, rstn;
input [DIN_SIZE-1:0] din1, din2;
output [(DIN_SIZE*2)-1:0] dout;

wire [DIN_SIZE-1:0] dreg1, dreg2;

genvar w;
generate for (w=0;w<DIN_SIZE/4;w=w+1) begin : gen_bank1
 fflop4  w_fflop (.clk(clk), .rstn(rstn), .d(din1[(4*w)+3:(4*w)]),.q(dreg1[(4*w)+3:(4*w)]) );
   end
endgenerate


genvar u;
generate for (u=0;u<DIN_SIZE/4;u=u+1) begin :gen_bank2
 fflop4  u_fflop (.clk(clk), .rstn(rstn), .d(din2[(4*u)+3:(4*u)]),.q(dreg2[(4*u)+3:(4*u)]) );
   end
endgenerate


genvar v;
generate for (v=0;v<DIN_SIZE;v=v+1) begin : dout_gen
      assign dout[(v*2)+1:(v*2)] = {dreg1[v],dreg2[v]};
  end
endgenerate


endmodule



module fflop4 (clk, rstn, d, q);
parameter DSIZE = 4;
input clk, rstn;
input [DSIZE-1:0] d;
output [DSIZE-1:0] q;



genvar w;
generate for (w=0;w<DSIZE;w=w+1) begin : gen_fflop1
 fflop1  w_fflop1 (.clk(clk), .rstn(rstn), .d(d[w]),.q(q[w]) );
   end
endgenerate

/*/
reg [DSIZE-1:0] q;
always @(posedge clk or negedge rstn)
  begin
   if (!rstn)
        q <= {DSIZE{1'b0}};
   else
        q <= d;
  end
*/

endmodule


module fflop1 (clk, rstn, d, q);
input clk, rstn;
input d;
output q;

reg q;

always @(posedge clk or negedge rstn)
  begin
   if (!rstn)
        q <= 1'b0;
   else
        q <= d;
  end


endmodule

