// generate if case testing continuous assign to a bitselect

module GENERATE12(a,b,c,d,sum);
  input [0:7] a,b,c,d; 
  output [0:7] sum;
  localparam I = 6, J = 8;

  generate
   if (I<7)
    begin:bit1
      assign sum[I] = a[I] && b[I];
    end
    if (J<7)
    begin:bit2
      assign sum[J] = c[J] && d[J];
    end
  endgenerate
endmodule
         
