// generate if test performing a module instantiation

module and22(a,b,sum);
  input a,b;
  output sum;
  assign sum = a && b;
endmodule

module GENERATE14(a,b,sum);
  parameter idx =8;
  parameter a_width = 8, b_width = 8;
  input [0:a_width-1] a,b; 
  output [0:a_width-1] sum;
  genvar I;
  generate  
    if (idx < 8)
      and22 u1 (a, b, sum);
  endgenerate
endmodule
