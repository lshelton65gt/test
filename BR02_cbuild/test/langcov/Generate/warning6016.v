// This reproduces a bug seen with many of the modelkit tests, running with -writeGuiDB
// results in errors like this:
// DeclarationScopeElab(0xb77e1a10) :  DeclarationScope(0xb7a1ee78)  BranchNode(0xb77d3138)
// Warning 6016: Cannot identify the unelaboratored symbol for top.g_j[0]

module top #(parameter cpu = 1) (input wire [7:0] a, input wire clk, output wire [7:0] b);

   reg foo;

   genvar j;
  generate
    for (j=0; j<cpu ; j=j+1)
      begin : g_j
         child u1 (a, clk, b);
      end
  endgenerate

endmodule

module child #(parameter cpu = 1) (input wire [7:0] a, input wire clk, output wire [7:0] b);

   reg foo;
   
   assign b = a;

endmodule
