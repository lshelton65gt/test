// last mod: Wed Apr  6 22:36:50 2005
// filename: test/langcov/Generate/bug4184.v
// Description:  This test demonstrates the problem reported in bug 4184, 
//  Could not find net in named declaration scope u1[1]:  top.RTL_DEBUG


module top  (input clk);

  reg RTL_DEBUG; // carbon depositSignal

  mid inst(clk);

endmodule


module mid (input clk);

//   wire localRTL_DEBUG;                    // workaround
//   assign localRTL_DEBUG = top.RTL_DEBUG;  // workaround

   generate 
   begin: gen4
      genvar g;
      for (g=0; g<2;g=g+1) begin: u1
         
         always @(posedge clk) 
           begin
	      if (top.RTL_DEBUG)
//	      if (localRTL_DEBUG)            // workaround
		$display( "in generate loop# %d.", g);
           end
      end // block: u1
   end // block: gen4

   endgenerate

   
endmodule
