// last mod: Wed Jan 12 09:01:04 2005
// filename: test/langcov/Generate/genvar1.v
// Description:  This test was inspired by a test from bug 2937, a genvar is
// used to create a parameter of a module


module genvar1(in, out);
   input [4:0] in;
   output [4:0]out;


   genvar g;
   generate
   for(g = 0; g < 5; g = g+1) begin: block1
      mid #(.NUMBITS(g)) i_mid ( out, in); // instantiates block1[0].i_mid block1[1].i_mid block1[2].i_mid block1[3].i_mid block1[4].i_mid
   end
   endgenerate
endmodule

module mid(out, in);
   parameter NUMBITS = 7;

   output [4:0]out;
   input  [4:0]in;

   assign out[NUMBITS] = |in[NUMBITS:0];
endmodule
