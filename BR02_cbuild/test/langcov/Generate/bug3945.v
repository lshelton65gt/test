// last mod: Mon Jul  2 07:24:28 2012
// filename: test/langcov/Generate/bug3945.v
// Description:  This test is the example from bug3945, we expect to see the
// names of the generate blocks as u_block_and_gate and u_block_or_gate
// or the full pathnames as: bug3945.u_block_or_gate.u_or_gate


module bug3945 (do, di, clk);
parameter GATE = 1'b0;
parameter AND_GATE = 1'b1;
parameter OR_GATE = 1'b0;
input [1:0] di;
input clk;
output do;

wire do;

generate
  case ({GATE, GATE})
    {1'b1, AND_GATE}:
    begin : u_block_and_gate
        and_gate u_and_gate (do, di[0], di[1]);
    end

    {1'b0, OR_GATE}:
    begin : u_block_or_gate
        or_gate u_or_gate (do, di[0], di[1]);
    end

    default:
    begin
      assign do = ~di[0];
    end

  endcase
endgenerate
endmodule

module and_gate (z, a, b);
input a, b;
output z;
wire z;

   initial
     $display("%m");

   assign z = a & b;

endmodule

module or_gate (z, a, b);
input a, b;
output z;
wire z;

   initial
     $display("%m");

   assign z = a | b;

endmodule
