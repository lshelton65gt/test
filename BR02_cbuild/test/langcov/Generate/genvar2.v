// last mod: Tue Jan 11 13:46:51 2005
// filename: test/langcov/Generate/genvar2.v
// Description:  In this test we try to partselect a genvar.

module genvar2(
	       input clk,
	       output reg [7:0] out);
   generate 
      begin: block1

         genvar g1;


         for (g1=5; g1<6;g1=g1+1) begin: block2 // one iteration

	    initial begin
	       $display("genvar test: g1: %b, g1[1:0]: %b", g1, g1[1:0]); 
	    end

	    always @(posedge clk) 
	       out = g1[2:1];	// try to do partselect of genvar
         end

      end
   endgenerate
endmodule
   
