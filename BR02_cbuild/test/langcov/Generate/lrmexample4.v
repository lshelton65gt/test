// This is Example 4 of section 12.1.3.2 of the 2001 Verilog LRM.  It's
// a generated ripple adder with the net declaration inside of the
// generate loop.


module addergen1 (co, sum, a, b, ci);
   parameter SIZE = 4;
   output [SIZE-1:0] sum;
   output                     co;
   input    [SIZE-1:0] a, b;
   input                      ci;
   wire     [SIZE       :0] c;
   genvar                     i;
   assign c[0] = ci;
   // Generated instance names are:
   // xor gates: bit[0].g1 bit[1].g1 bit[2].g1 bit[3].g1
   // bit[0].g2 bit[1].g2 bit[2].g2 bit[3].g2
   // and gates: bit[0].g3 bit[1].g3 bit[2].g3 bit[3].g3
   //                    bit[0].g4 bit[1].g4 bit[2].g4 bit[3].g4
   // or     gates: bit[0].g5 bit[1].g5 bit[2].g5 bit[3].g5
   // Generated instances are connected with
   // generated nets: bit[0].t1 bit[1].t1 bit[2].t1 bit[3].t1
   //                           bit[0].t2 bit[1].t2 bit[2].t2 bit[3].t2
   //                           bit[0].t3 bit[1].t3 bit[2].t3 bit[3].t3
   generate
      for(i=0; i<SIZE; i=i+1) begin:bit
        wire       t1, t2, t3; // generated net declaration
        xor g1 (             t1, a[i], b[i]);
        xor g2 ( sum[i],             t1, c[i]);
        and g3 (              t2, a[i], b[i]);
        and g4 (              t3, t1, c[i]);
        or     g5 ( c[i+1],          t2,      t3);
      end
   endgenerate
   assign co = c[SIZE];
endmodule
