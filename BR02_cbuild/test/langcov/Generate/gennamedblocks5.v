// testing various uses of named generate blocks
// testcase test/langcov/Generate/genenamedblocks5.v
// here we have:
//   one unnamed generate block and one named, the named one uses the same name
//   that carbon will use for the unnamed block (this is expected to create a conflict)
module top(clk, in1, in2, out1, out2);
   input clk;
   input [31:0] in1, in2;
   output [31:0] out1, out2;

   parameter 	 A = 1;
   parameter 	 B = 1;

          // the following has an unnamed block, which should be assigned a name
       // that does not conflict with the user defined name below
   generate begin
      if (B) begin: ifblk
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in2;
         assign out2 = outreg;
      end
      else begin
         assign out2 = 32'h0;
      end
   end
   endgenerate
      
       // the following named block happens to use the same name that carbon
       // will create for a unnamed block
   generate begin: genblk1
      if (A) begin: ifblk
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in1;
         assign out1 = outreg;
      end
      else begin
         assign out1 = 32'h0;
      end
   end
   endgenerate

endmodule

