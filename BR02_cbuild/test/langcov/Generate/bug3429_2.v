// filename: test/langcov/Generate/bug3429_2.v
// Description:  This is test bug2.v from bug 3429, it has two instances of the
// same module that have different names
// see also bug3429_1.v for a similar example except that the instance names are identical.

module bug(clk, in1, in2, out1, out2);
   input clk;
   input [31:0] in1, in2;
   output [31:0] out1, out2;

   parameter 	 A = 1;
   parameter 	 B = 1;

   generate begin: out1_block
      if (A) begin
	 flop flop1(.clk(clk), .in(in1), .out(out1));
      end
      else begin
         assign out1 = 32'h0;
      end
   end
   endgenerate
      
   generate begin: out2_block
      if (B) begin
	 flop flop2(.clk(clk), .in(in2), .out(out2));
      end
      else begin
         assign out2 = 32'h0;
      end
   end
   endgenerate
      
endmodule

module flop(clk, in, out);
   input clk;
   input [31:0] in;
   output [31:0] out;

   reg [31:0] 	 out;

   always @(posedge clk)
     out <= in;
endmodule
