
// Tests uniquification of parameterized modules instantiating other 
// modules in a generate statement
// This particular test has to deal with two different branches in an 
// if-generate within a for-generate. gscopefor 0 thru 6 should all 
// uniquify to the same scope.

module top(out1, out2, in1, in2);

   output [13:0] out1;
   output [1:0]  out2;
   input [13:0]  in1;
   input [1:0]   in2;
   
   defparam top.u1.MyParam = 2;
   defparam top.u2.MyParam = 1;

   mid u1(out1[13:7], out2[1], in1[13:7], in2[1]);
   mid #(4) u2(out1[6:0], out2[0], in1[6:0], in2[0]); // 4 should be overridden by the defparam
endmodule

module mid(out1, out2, in1, in2);

   output [6:0] out1;
   output       out2;
   input [6:0]  in1;
   input        in2;

   parameter    MyParam = 0;
   
   genvar i;
   generate
   for (i = 0; i < 7; i = i + 1)
     begin : gscopefor
        if (MyParam == 1)
          begin : gscopeif1
             sub t1(out1[i], in1[i]);
          end
        if (MyParam == 2)
          begin : gscopeif2
             sub2 f4(out1[i], in1[i]);
          end
     end
   endgenerate

   sub t2(out2, in2);
endmodule // mid

module sub(z, a);
   output z;
   input  a;

   assign z = a;
endmodule // sub

module sub2(z, a);
   output z;
   input  a;

   assign z = ~a;
endmodule   