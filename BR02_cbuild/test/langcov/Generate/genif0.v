// Simple generate if use, adapted from the LRM examples

module gray2bin1 (bin, gray);
  parameter SIZE = 8;                  // this module is parameterizable
  output [SIZE-1:0] bin;
  input [SIZE-1:0] gray;

  genvar i;

  reg foo;

  generate
    if ( SIZE == 7 )
    begin:this_is_the_label
      assign bin[0] = ^gray[SIZE-1:0];
      assign bin[1] = ^gray[SIZE-1:1];
      assign bin[2] = ^gray[SIZE-1:2];
      assign bin[3] = ^gray[SIZE-1:3];
      assign bin[4] = ^gray[SIZE-1:4];
      assign bin[5] = ^gray[SIZE-1:5];
      assign bin[6] = ^gray[SIZE-1:6];
    end
    if ( SIZE == 8 )
    begin:this_is_the_label
      assign bin[0] = ^gray[SIZE-1:0];
      assign bin[1] = ^gray[SIZE-1:1];
      assign bin[2] = ^gray[SIZE-1:2];
      assign bin[3] = ^gray[SIZE-1:3];
      assign bin[4] = ^gray[SIZE-1:4];
      assign bin[5] = ^gray[SIZE-1:5];
      assign bin[6] = ^gray[SIZE-1:6];
      assign bin[7] = ^gray[SIZE-1:7];
    end
  endgenerate

endmodule
