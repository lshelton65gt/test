// last mod: Thu Dec 30 08:35:01 2004
// filename: test/langcov/Generate/genfor6.v
// Description:  This test shows shows that the generate_if processing is
// correct by only selecting a single branch of the if stmts.
// At one time we would not get this correct.
// we should see:
// genfor6.v:13 top.out: Warning 4022: Net has undriven slice [2:1]
// genfor6.v:13 top.out: Warning 4021: Net has undriven slice [7:4]


module top (out, in);
   parameter SIZE = 8;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;

   genvar i;

   reg 		     foo;

   generate
   begin:this_is_the_label
      for ( i = 0; i < 1; i = i + 1) // this loop only sets i to 0
	begin: loop1
	   if ( i < 3 )		// since i is always 0 the THEN branch is the true branch
	     assign out[0] = ^in[SIZE-1:i];
	   else
	     assign out[1] = |in[SIZE-1:i];

	end
      for ( i = 3; i < 4; i = i + 1) // this loop only sets i to 3
	begin: loop2
	   if ( i < 3 )		// since i is always 3 the ELSE branch is the true branch
	     assign out[2] = ^in[SIZE-1:i];
	   else
	     assign out[3] = |in[SIZE-1:i];
	end
   end
   endgenerate

endmodule
