// last mod: Thu Jan 27 08:46:46 2005
// filename: test/langcov/Generate/genvar3.v
// Description:  This test has a bitselect of a genvar, at one time this did not work


module genvar3(clk, in, out);
   input clk;
   input [4:0] in;
   output [3:0] out;
   reg [3:0] out;

   generate 
   begin: block1
      genvar g1;

      for (g1=32'h5aaaaaaa; g1<32'h5aaaaaab;g1=g1+1) begin: block2 // one iteration

	 initial begin
		// do static bitselect
	    $display("genvar test: g1: %b, g1[1]: %b, g1[0]: %b", g1, g1[1], g1[0]); 
	 end

	 
	 always @(posedge clk)
	    begin
	       out = g1[in];	// do dynamic bitselect of genvar
	    end
      end

   end
   endgenerate
endmodule
