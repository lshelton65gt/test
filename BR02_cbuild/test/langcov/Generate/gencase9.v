// last mod: Tue Mar  1 16:04:33 2005
// filename: test/langcov/Generate/gencase9.v
// Description:  similar to gencase2.v This test shows shows that the generate_case processing is
// correct by selecting several of a case statement.  In this testcase the case
// labels and the selector are longer than 32 bits.  Note that this crashes
// other simulators so this was converted to a self checking testcase, the
// self_check vector should always be 4'b1111!

module top (gen_out0, gen_out1, gen_out2, gen_out3,
	    man_out0, man_out1, man_out2, man_out3,
	    self_check, in);
   parameter SIZE = 4;
   output [SIZE-1:0] gen_out0, gen_out1,gen_out2, gen_out3;
   output [SIZE-1:0] man_out0, man_out1,man_out2, man_out3;
   output [3:0]      self_check;
   input [SIZE-1:0]  in;

       // we expect the self_check vector to always be 4'b1111;
   assign 	     self_check[0] = gen_out0 == man_out0;
   assign 	     self_check[1] = gen_out1 == man_out1;
   assign 	     self_check[2] = gen_out2 == man_out2;
   assign 	     self_check[3] = gen_out3 == man_out3;

   mid_gen #(1'b1,0) i0( gen_out0, in);
   mid_gen #(1'b1,1) i1( gen_out1, in);
   mid_gen #(1'b1,2) i2( gen_out2, in);
   mid_gen #(1'b1,3) i3( gen_out3, in);


       // the mid_man_* modules are manual implementations of what we expect
       // from the generate expansion
   mid_man_0 i4( man_out0, in);
   mid_man_1 i5( man_out1, in);
   mid_man_2 i6( man_out2, in);
   mid_man_3 i7( man_out3, in);

endmodule

module mid_gen (out, in);
   parameter ONE = 1'b1;
   parameter TYPE = 0;
   parameter SIZE = 4;
   parameter P_A = 0;
   parameter P_B = 1;
   parameter P_C = 2;
   parameter P_D = 3;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;

   genvar i;

   reg 		     foo;

   generate
   begin:this_is_the_gen_label
      for ( i = 0; i < SIZE; i = i + 1) // sets i to 0, 1, 2 ... SIZE-1
	begin:this_is_for_label
	   case ( {ONE,TYPE} )
	     {1'b0,32'd0}  : assign out[i] = 0; // this will not be selected by generate
	     {1'b1,P_A} : assign out[i] = ^in[SIZE-1:i];
	     {1'b0,32'd1}  : assign out[i] = 1; // this will not be selected by generate
	     {1'b1,P_B} : assign out[i] = |in[SIZE-1:i];
	     
	     {1'b1,P_C} : assign out[i] = &in[SIZE-1:i];
	     {1'b1,P_D} : assign out[i] = ~|in[SIZE-1:i];
	   endcase
	end
   end
   endgenerate

endmodule


module mid_man_0 (out, in);
   parameter SIZE = 4;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;

   assign 	     out[0] = ^in[SIZE-1:0];
   assign 	     out[1] = ^in[SIZE-1:1];
   assign 	     out[2] = ^in[SIZE-1:2];
   assign 	     out[3] = ^in[SIZE-1:3];

endmodule



module mid_man_1 (out, in);
   parameter SIZE = 4;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;


   assign 	     out[0] = |in[SIZE-1:0];
   assign 	     out[1] = |in[SIZE-1:1];
   assign 	     out[2] = |in[SIZE-1:2];
   assign 	     out[3] = |in[SIZE-1:3];

endmodule

module mid_man_2 (out, in);
   parameter SIZE = 4;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;


   assign 	     out[0] = &in[SIZE-1:0];
   assign 	     out[1] = &in[SIZE-1:1];
   assign 	     out[2] = &in[SIZE-1:2];
   assign 	     out[3] = &in[SIZE-1:3];

endmodule

module mid_man_3 (out, in);
   parameter SIZE = 4;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;


   assign 	     out[0] = ~|in[SIZE-1:0];
   assign 	     out[1] = ~|in[SIZE-1:1];
   assign 	     out[2] = ~|in[SIZE-1:2];
   assign 	     out[3] = ~|in[SIZE-1:3];

endmodule
