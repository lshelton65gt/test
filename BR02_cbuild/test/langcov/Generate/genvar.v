// This is taken from example 1 of section 12.1.3.2 of the 2001 Verilog
// LRM.  Test the proper handling of a genvar with no generate
// statements.


module gray2bin1 (bin, gray);
       output [7:0] bin;
       input [7:0] gray;

       genvar i;

       assign bin = gray;
   endmodule
