// last mod: Thu Dec 30 13:57:01 2004
// filename: test/langcov/Generate/gencase6.v
// Description:  This test shows shows that the generate_case processing is
// correct by only selecting a some of the branches of the case statement. It
// has negative tags
// we should see:
// gencase6.v:13 top.out: Warning 4021: Net has undriven slice [1:0]
// gencase6.v:13 top.out: Warning 4021: Net has undriven slice [5:4]
// note that this testcase does not run in aldec as of12/30/04,
// but I did verify that we have correct gold outputs (cloutier)
module top (out, in, cond);
   parameter SIZE = 6;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;
   input [2:0]	     cond;

   genvar i;

   reg 		     foo;

   generate
   begin:this_is_the_label
      for ( i = 2; i < 4; i = i + 1) // sets i to ONLY:  2, 3
	begin:block2
	   case ( i-3 )
	     -2 : assign out[i] = ^in[SIZE-1:i];
	     -1 : assign out[i] = |in[SIZE-1:i]; // i == 2
	      0 : assign out[i] = &in[SIZE-1:i]; // i == 3
	      1 : assign out[i] = ~|in[SIZE-1:i];
	   endcase
	end
   end
   endgenerate

endmodule
