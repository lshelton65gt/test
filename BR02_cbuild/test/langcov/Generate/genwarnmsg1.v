// last mod: Tue Jun  7 16:39:40 2005
// filename: test/langcov/Generate/genwarnmsg1.v
// Description:  This test is used to check for warning messages related to
// generate support


module genwarnmsg1(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   generate
   begin: block1
      mid i1(clock, in1, in2, out1);
   end
   endgenerate
endmodule

module mid(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;
   reg [7:0] t;
   always @(posedge clock)
     begin: main
	t = in1 | in2;
	out1 = in1 & in2 & t[7:4];
     end
endmodule
