
// Tests uniquification of parameterized modules within generate 
// instantiations.
// This particular blindly generates instances, but defparams change the 
// modules that are being instantiated.

module top(out1, out2, in1, in2);

   output [13:0] out1;
   output [1:0]  out2;
   input [13:0]  in1;
   input [1:0]   in2;
   
   defparam top.u1.gscopefor[2].t1.Mode = 1;
   defparam top.u2.gscopefor[6].t1.Mode = 3;

   mid u1(out1[13:7], out2[1], in1[13:7], in2[1]);
   mid u2(out1[6:0], out2[0], in1[6:0], in2[0]); 
endmodule

module mid(out1, out2, in1, in2);

   output [6:0] out1;
   output       out2;
   input [6:0]  in1;
   input        in2;

   genvar i;
   generate
   for (i = 0; i < 7; i = i + 1)
     begin : gscopefor
        sub t1(out1[i], in1[i]);
     end
   endgenerate

   sub t2(out2, in2);
endmodule // mid

module sub(z, a);
   output z;
   input  a;

   parameter Mode = 2;

   generate
   if (Mode == 0)
     begin: mode0
        assign z = 1'b0;
     end
   else if (Mode == 1)
     begin: mode1
        assign z = ~a;
     end
   else if (Mode == 2)
     begin: mode2
        assign z = a;
     end
   else if (Mode == 3)
     begin: mode3
        assign z = 1'b1;
     end
   endgenerate
endmodule // sub
