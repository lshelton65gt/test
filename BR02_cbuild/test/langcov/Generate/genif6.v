// Simple generate if use, inspired by testing for bug3947
// the condition expression is longer than 32 bits but the low 32 bits are
// always 0, should select only the THEN of first if and the ELSE of the second if

module top(clk);
//  output [3:0] bin1, bin2;
  input clk;
   mid #(2,0) i1(clk);
   mid #(0,0) i2(clk);
endmodule
module mid (clk);
  parameter P1 = 2;
  parameter P2 = 2;
//  output [3:0] bin;
  input clk;

  generate
    if ( {P1,P2} )
    begin:this_is_the_then_label
       always @(posedge clk)
	 begin
	    //assign bin = gray + 1;
	    $display("true {P1,P2} {%d,%d}", P1, P2);
	 end
    end
    else
    begin:this_is_the_else_label
       always @(posedge clk)
	 begin
       //assign bin = gray + 2;
       $display("false {P1,P2} {%d,%d}", P1, P2);
	 end
    end
       
    if ( {P1-2,P2})
    begin:this_is_the_then_label2
       always @(posedge clk)
	 begin
       //assign bin = gray + 3;
       $display("true {P1-2,P2} {%d,%d}", P1-2, P2);
	 end
    end
   else
    begin:this_is_the_else_label2
       always @(posedge clk)
	 begin
       // assign bin = gray + 4;
       $display("false {P1-2,P2} {%d,%d}", P1-2, P2);
	 end
    end
  endgenerate

endmodule
