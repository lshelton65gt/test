// Description:  This test seems to duplicate the problem of bug 14714, seen at the customer site.
// The issue seems to be related to multiple generate blocks that instantiate the same sub module.


`define WIDTH  1
module bug14714_b(clkV, resetV, in1, out1, in2, out2);
   input [`WIDTH-1:0] clkV;
   input [`WIDTH-1:0] resetV;
   input [`WIDTH-1:0] in1;
   output [`WIDTH-1:0] out1;
   input [`WIDTH-1:0] 	in2;
   output [`WIDTH-1:0] out2;
   reg [`WIDTH-1:0] 	out2;
   reg [`WIDTH-1:0] 	out1;

   wire [`WIDTH-1:0] 	temp1;
   wire [`WIDTH-1:0] 	temp2;
   genvar 	n;

   generate
      for (n=0; n<`WIDTH; n=n+1) begin : block1
	 submod u1 (.in_p       (in1[n]), .out_p      (temp1[n]));
	 always @(posedge clkV[n] or negedge resetV[n])
	   begin
              if (!resetV[n]) out1[n] <= 0;
              else            out1[n] <= temp1[n];
	   end
      end
   endgenerate


   generate
      for (n=0; n<`WIDTH; n=n+1) begin : block2
	 submod u2 (.in_p       (in2[n]), .out_p      (temp2[n]));
	 always @(posedge clkV[n] or negedge resetV[n])
	   begin
              if (!resetV[n]) out2[n] <= 0;
              else            out2[n] <= temp2[n];
	   end
      end
   endgenerate
   

endmodule
module submod (in_p, out_p);

   input    in_p;
   output  out_p;

   assign out_p = in_p;
endmodule

