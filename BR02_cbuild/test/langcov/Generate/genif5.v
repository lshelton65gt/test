// last mod: Fri Sep  9 22:02:39 2005
// filename: test/langcov/Generate/genif5.v
// Description:  This test shows shows that the generate_if processing is
// correct by only selecting a single branch of the if stmt.


module and22(a,b,sum);
  input [7:0] a,b;
  output [7:0] sum;
  assign sum = a & b;
endmodule

module GENERATE14(a,b,sum);
  parameter idx =8;
  parameter a_width = 8, b_width = 8;
  input [0:a_width-1] a,b; 
  output [0:a_width-1] sum;
  genvar I;
  generate  
    if (idx < 8)
      ; // null then branch will not be selected 
      else 
	and22 u1 (a, b, sum);	// will be selected
  endgenerate
endmodule
