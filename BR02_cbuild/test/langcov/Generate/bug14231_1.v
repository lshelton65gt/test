// filename: test/langcov/Generate/bug14231_1
// Description:  This test shows the problem of bug14231, where a module
// instance within a conditional generate block does not generate the proper
// design hierarchy and so waveform dumping produces the wrong name


module bug14231_1(clk, in1, in2, out1, out2);
   input clk;
   input [7:0] in1, in2;
   output [7:0] out1;
   output  out2;

   parameter 	 A = 4;
   parameter 	 B = 1;


   mid #(A, B) U1(clk, in1, in2, out1, out2);
      
endmodule


module mid(clk, in1, in2, out1, out2);
   input clk;
   input [7:0] in1, in2;
   output [7:0] out1;
   output  out2;

   parameter 	NUMCPUS = 1;
   parameter    OTHERCPUS = 0;   

   genvar 	i;
   generate begin: out1_block
      for ( i = 0; i < NUMCPUS; i = i+1) begin
	 flop vflop(.clk(clk), .in(in1[i]), .out(out1[i]));
      end
      assign out1[7:NUMCPUS] = 0;
   end
   endgenerate
      
   generate begin: out2_block
      if (OTHERCPUS) begin
	 wire outreg;
	 flop uflop(.clk(clk), .in(in2[0]), .out(outreg));
         assign out2 = outreg;
      end
      else begin
         assign out2 = 0;
      end
   end
   endgenerate


endmodule



module flop(clk, in, out);
   input clk;
   input [0:0] in;
   output [0:0] out;

   reg [0:0] 	 out;

   initial
     $display("%m");
   
   always @(posedge clk)
     out <= in;
endmodule
