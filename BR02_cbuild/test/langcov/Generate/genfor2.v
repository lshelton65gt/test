// last mod: original file
// filename: test/langcov/Generate/genfor2.v
// Description:  This test is like genfor1.v except the if and the for
// are reversed. Same functionality.


module gray2bin1 (bin, gray);
  parameter SIZE = 8;                  // this module is parameterizable
  output [SIZE-1:0] bin;
  input [SIZE-1:0] gray;

  genvar i;

  reg foo;

  generate
   if ( SIZE == 7 )
     for ( i = 0; i < 7; i = i + 1)
       begin:this_is_the_label
	  assign bin[i] = ^gray[SIZE-1:i];
       end
   
   if ( SIZE == 8 )
     begin
	wire [SIZE-1:0] localvar;
	  for ( i = 0; i < 8; i = i + 1)
	    begin:this_is_the_label
	       assign localvar[i] = ^gray[SIZE-1:i];
	    end
	assign        bin = localvar;
     end
   endgenerate
   
endmodule
