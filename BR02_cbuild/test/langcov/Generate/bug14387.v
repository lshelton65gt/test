// Test case to demonstrate the issue found in bug 14387.
// The issue was that cbuild would exit with Error 43, saying there was an
// error, but it would not list any other errors or point to a problem.
//
// This was caused by the use of the localparam within a generate block.
//
// testcase test/langcov/Generate/bug14387.v


module bug14387(clk, in1, out1);
   input clk;
   input [31:0] in1;
   output [31:0] out1;

   parameter 	 A = 1;
   localparam FOO = 20; // a value that is never used

   generate begin: out1_block
      // use localparam within a generate block, (this was not supported before this bug)
      // and then use the localparam to make sure that that the proper value (31) is actually used
      localparam FOO = A + 30;
      if (A) begin: nested1_block
	 reg [FOO:0] outreg;
	 always @(posedge clk)
	   outreg <= in1;
         assign out1 = outreg;
      end
      else begin
         assign out1 = 32'h0;
      end
   end
   endgenerate

endmodule

