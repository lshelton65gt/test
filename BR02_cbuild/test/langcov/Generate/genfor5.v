// last mod: Wed Dec 29 14:33:47 2004
// filename: test/langcov/Generate/genfor5.v
// Description:  This test has a generate for loop that contains an if stmt that
// uses the genvar as a condition.


module genfor5 (out, in);
   parameter SIZE = 8;                  // this module is parameterizable
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;

   genvar i;

   reg 		     foo;

   generate
   for ( i = 0; i < SIZE; i = i + 1)
     begin:this_is_the_label
	if ( i < 3 )
	  assign out[i] = ^in[SIZE-1:i];
	else
	  assign out[i] = |in[SIZE-1:i];
     end
   endgenerate

endmodule
