// last mod: Tue Jan 11 17:01:13 2005
// filename: test/langcov/Generate/genvar4.v
// Description:  In this test we try to do an out-of-range partselect of a
// genvar., it should generate an error message
//
// Aldec will generate x's because it was out of range
// Do an xz-diff and if that passes then use the
// Carbon output as the gold.

module genvar2(
	       input clk,
	       output reg [7:0] out);
   generate 
      begin: block1

         genvar g1;


         for (g1=10; g1<11;g1=g1+1) begin: block2 // one iteration

	    initial begin
	       $display("genvar test: g1: %b, g1[35:32]: %b", g1, g1[35:32]); 
	    end

	    always @(posedge clk) 
	       out = g1[35:32];	// out of range
         end

      end
   endgenerate
endmodule
   
