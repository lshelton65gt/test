module gclk(in, out, clk, ena);
  input [8191:0] in;
  output [8191:0] out;
  reg [8191:0] out;
  input           clk;
  input [12:0]    ena;

  genvar i;

  generate
    for ( i = 0; i < 8192; i = i + 1)
      begin: this_is_the_label
        wire gclk;
        assign gclk = clk & (ena == i);
        always @(posedge gclk)
          out[i] <= in[i];
      end
  endgenerate
endmodule
