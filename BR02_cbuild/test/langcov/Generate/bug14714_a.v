// Description:  This test is an attempt to duplicate the problem in bug 14714,
// it generated the same internal error, but it was not exactly the problem seen
// at the customer site (as verified by webex session).

// once the internal error was fixed this example still will not compile because
// the negedge reset in the sensitivity list does not match the reset condition
// in the always block.  cbuild is correctly reporting that this is a failure to
// compile. so this test should be PEXIT

module bug14714_a(clock, reset, in1, out1);
   input [7:0] clock;
   input [7:0] reset;
   input [7:0] in1;
   output [7:0] out1;
   reg [7:0] out1;

   wire [7:0] 	temp;
   genvar 	n;
  generate 
    for (n=0; n<8; n=n+1) begin : inst_sync_ctl_tbt_cmd_done
      myreg #(1) uReg
        (.clk        (clock[n]),
         .reset_n    (reset[n]),
         .clear      (1'b0),
         .in_p       (temp[n]),
         .out_p      (temp[n]));

      always @(posedge clock[n] or negedge reset) begin  // at one time this line would create an internal error message
        if (!reset[n]) out1[n] <= 0;
        else           out1[n] <= temp[n];
      end
    end
  endgenerate
endmodule


module myreg(clk, reset_n, clear, in_p, out_p);
   parameter WIDTH = 3;
   input [WIDTH-1:0] clk;
   input [WIDTH-1:0] reset_n;
   input [WIDTH-1:0] clear;
   input [WIDTH-1:0] in_p;
   output [WIDTH-1:0] out_p;
   reg [WIDTH-1:0] out_p;

   always @(posedge clk)
     begin: main
	out_p = in_p;
     end

endmodule
