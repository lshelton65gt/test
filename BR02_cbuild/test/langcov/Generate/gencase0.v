// generate case with a module instantiation

module and22(a,b,sum);
  input [0:1] a,b;
  output [0:1] sum;
  assign sum = a & b;
endmodule

module GENERATE19(a,b,sum);
  parameter idx = 4;
  parameter a_width = 4, b_width = 4;
  input [0:a_width-1] a,b; 
  output [0:a_width-1] sum;
  genvar I;
  generate  
    case (idx) 
      1 : and22 u1 (a[0:1], b[0:1], sum[0:1]);
      default: and22 u2 (a[2:3], b[2:3], sum[2:3]);
    endcase
  endgenerate
endmodule
