// last mod: Thu Dec 30 12:53:12 2004
// filename: test/langcov/Generate/gencase3.v
// Description:  This test shows shows that the generate_case processing is
// correct by selecting all of the branches of the case statement. but
// incomplete coverage for vector out.
// we should see:
// gencase3.v:13 top.out: Warning 4022: Net has undriven bit [0]
// gencase3.v:13 top.out: Warning 4022: Net has undriven bit [5]


module top (out, in, cond);
   parameter SIZE = 6;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;
   input [2:0]	     cond;

   genvar i;

   reg 		     foo;

   generate
   begin:this_is_the_label
      for ( i = 0; i < SIZE; i = i + 1) // sets i to 0, 1, 2 ... SIZE-1
	begin:block2
	   case ( i )
	     1 : assign out[i] = |in[SIZE-1:i];
	     2 : assign out[i] = &in[SIZE-1:i];
	     3 : assign out[i] = ~|in[SIZE-1:i];
	     4 : assign out[i] = ^in[SIZE-1:i];
	   endcase
	end
   end
   endgenerate

endmodule
