// last mod: Fri Mar  4 10:05:42 2005
// filename: test/langcov/Generate/genmem.v
// Description:  This test is used to check the naming convention for generate
// blocks, and the accessibility of nets declared within those blocks.  Run this
// with the directives:
// observeSignal top.mem1.style1.bk1.data2
// observeSignal top.mem0.genblk0.bk0.data2
// 


// use a generate block to select the style of memory to create,
// in this test both styles happen to be logically equivalent, but have different names
`define MEM_WIDTH 32
`define MEM_DEPTH 32
`define MEM_ADDR_BITS 5


module top (out0, out1, clk, wen, data, waddr, raddr);
   output [`MEM_WIDTH-1:0]    out0, out1;
   input 		      clk, wen;
   input [`MEM_WIDTH-1:0]     data;
   input [`MEM_ADDR_BITS-1:0] waddr, raddr;
   initial
      begin
	// $display(top.mem1.style1.reg_array_s1[0]); // this works in nc

	// $display(top.mem1.style1.reg_array_s1[0]); // this should work in carbon
	 
	// $display(top.mem0.genblk1.reg_array_s0[0]);// this is most likely the name in nc
      end

       // to avoid defining initial blocks to initalize the memory we use same
       // address for read and write
   gen_mem #(1) mem1(out1, clk, wen, data, raddr, raddr);
   gen_mem      mem0(out0, clk, wen, data, raddr, raddr);
endmodule


module gen_mem (out, clk, wen, data, waddr, raddr);
   output [`MEM_WIDTH-1:0]    out;
   input 		      clk, wen;
   input [`MEM_WIDTH-1:0]     data;
   input [`MEM_ADDR_BITS-1:0] waddr, raddr;

   parameter 		      STYLE = 0;
   
   generate
      case (STYLE)
	1:
	  begin:style1
	     // This is the memory
	     reg [`MEM_WIDTH-1:0]       reg_array_s1 [`MEM_DEPTH-1:0];

	     always @ (negedge clk)
		   $display("%m"); // nc says this is: top.mem1.style1
	     // Write the memory
	     always @ (negedge clk)
		begin:bk1
		   reg  [`MEM_WIDTH-1:0]     data2;
		   data2 = ~data;
		   if (wen)
		     reg_array_s1[waddr] = data2;
		end

             // Read the memory
             assign  out = reg_array_s1[raddr];
          end
	default:
	  begin             // has no name
	     // This is the memory
	     reg [`MEM_WIDTH-1:0]       reg_array_s0 [`MEM_DEPTH-1:0];

	     // Write the memory
	     always @ (negedge clk)
		   $display("%m"); // nc says this is: top.mem0.genblk1

	     always @ (negedge clk)
		begin:bk0
		   reg  [`MEM_WIDTH-1:0]     data2;
		   data2 = ~data;
		   if (wen)
		     reg_array_s0[waddr] = data2;
		end 
             // Read the memory
             assign out = reg_array_s0[raddr];
          end
      endcase
   endgenerate
   
endmodule


