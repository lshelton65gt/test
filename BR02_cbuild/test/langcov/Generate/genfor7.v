// last mod: Fri Dec  2 08:28:03 2005
// filename: test/langcov/Generate/genfor7.v
// Description:  This test was identified by josh, here a wire is declared
// within a generate FOR loop(gclk), and there is an initilization of the wire
// on the declaration line.  At one time this was not properly populated.
// If the initilization is on a different line then it was properly populated.


module genfor7(clock, in1, in2, ena, out1);
   input clock, ena;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] 	out1;

   initial
     out1 = 0;
   
   genvar i;
   generate
   for ( i  = 0; i <= 7; i = i + 1 )
     begin: loop_label
	wire 	gclk = clock & ena;

	always @(posedge gclk)
	  begin: main
	     out1[i] = in1[i] & in2[i];
	  end
     end
   endgenerate
endmodule
