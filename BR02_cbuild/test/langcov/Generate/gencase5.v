// last mod: Thu Dec 30 13:07:58 2004
// filename: test/langcov/Generate/gencase5.v
// Description:  This test shows shows that the generate_case processing is
// correct by only selecting a some of the branches of the case statement.
// we should see:
// gencase5.v:13 top.out: Warning 4021: Net has undriven slice [2:0]
// gencase5.v:13 top.out: Warning 4022: Net has undriven bit [5]



module top (out, in, cond);
   parameter SIZE = 6;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;
   input [2:0]	     cond;

   genvar i, j;

   reg 		     foo;

   generate
   begin:this_is_the_label
      for ( i = 3; i < 5; i = i + 1) // sets i to ONLY:  3, 4
	begin:blocki
	   for ( j = 1; j < 3; j = j + 1) // sets j to ONLY: 1 and 2
	      begin:blockj
		 case ( i-j )		// i-j is always in range: 1:3
		   1 : assign out[i-j] = |in[SIZE-1:i-j];
		   2 : assign out[i-j] = &in[SIZE-1:i-j];
		   3 : assign out[i-j] = ~|in[SIZE-1:i-j];
		   4 : assign out[i-j] = ^in[SIZE-1:i-j];
		 endcase
	      end
	end
   end
   endgenerate

endmodule
