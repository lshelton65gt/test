// last mod: Tue Mar  1 10:07:47 2005
// filename: test/langcov/Generate/gencase10.v
// Description:  similar to gencase9.v This test shows shows that the generate_case processing is
// correct by selecting all branches of a case statement.  in this case the case
// labels are longer than 32 bits, but not the same width as the case selector

module top (out0, out1, out2, out3, in);
   parameter SIZE = 4;
   output [SIZE-1:0] out0, out1,out2, out3;
   input [SIZE-1:0]  in;

   mid #(2'b1,0) i0( out0, in);	// parameter widths control selector of generate-case
   mid #(2'b1,1) i1( out1, in);
   mid #(2'b1,2) i2( out2, in);
   mid #(2'b1,3) i3( out3, in);
endmodule

module mid (out, in);
   parameter ONE = 1'b1;
   parameter TYPE = 0;
   parameter SIZE = 4;
   parameter P_A = 0;
   parameter P_B = 1;
   parameter P_C = 2;
   parameter P_D = 3;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;

   genvar i;

   reg 		     foo;

   generate
   begin:this_is_the_gen_label
      for ( i = 0; i < SIZE; i = i + 1) // sets i to 0, 1, 2 ... SIZE-1
	begin:this_is_for_label
	   case ( {ONE,TYPE} )	// selector width is controlled by parameter value width
	     {1'b0,32'd0}  : assign out[i] = 0; // this will not be selected by generate
	     {1'b1,P_A} : assign out[i] = ^in[SIZE-1:i];
	     {1'b0,32'd1}  : assign out[i] = 1; // this will not be selected by generate
	     {1'b1,P_B} : assign out[i] = |in[SIZE-1:i];
	     
	     {1'b1,P_C} : assign out[i] = &in[SIZE-1:i];
	     {1'b1,P_D} : assign out[i] = ~|in[SIZE-1:i];
	   endcase
	end
   end
   endgenerate

endmodule
