// last mod: Thu Dec 30 08:55:57 2004
// filename: test/langcov/Generate/gencase2.v
// Description:  This test shows shows that the generate_case processing is
// correct by selecting all branches of a case statement.

module top (out, in, cond);
   parameter SIZE = 4;
   output [SIZE-1:0] out;
   input [SIZE-1:0]  in;
   input [2:0] 	     cond;

   genvar i;

   reg 		     foo;

   generate
   begin:this_is_the_gen_label
      for ( i = 0; i < SIZE; i = i + 1) // sets i to 0, 1, 2 ... SIZE-1
	begin:this_is_for_label
	   case ( i )
	     0 : assign out[i] = ^in[SIZE-1:i];
	     1 : assign out[i] = |in[SIZE-1:i];
	     2 : assign out[i] = &in[SIZE-1:i];
	     3 : assign out[i] = ~|in[SIZE-1:i];
	   endcase
	end
   end
   endgenerate

endmodule
