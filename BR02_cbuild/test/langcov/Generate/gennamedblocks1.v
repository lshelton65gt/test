// testing the variations of named generate blocks
// testcase test/langcov/Generate/genenamedblocks1.v
// here we have:
//   a    named generate block with a nested    named generate block
//   a    named generate block with a nested un-named generate block
//   a un-named generate block with a nested    named generate block
//   a un-named generate block with a nested un-named generate block

module top(clk, in1, in2, in3, in4, out1, out2, out3, out4);
   input clk;
   input [31:0] in1, in2, in3, in4;
   output [31:0] out1, out2, out3, out4;

   parameter 	 A = 1;
   parameter 	 B = 1;
   parameter 	 C = 1;
   parameter 	 D = 1;

   // named with nested named
   generate begin: out1_block
      if (A) begin: nested1_block
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in1;
         assign out1 = outreg;
      end
      else begin
         assign out1 = 32'h0;
      end
   end
   endgenerate

   // named with nested unnamed
   generate begin: out2_block
      if (B) begin
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in2;
         assign out2 = outreg;
      end
      else begin
         assign out2 = 32'h0;
      end
   end
   endgenerate

   // unnamed with nested named
   generate begin
      if (C) begin: nested1_block
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in3;
         assign out3 = outreg;
      end
      else begin
         assign out3 = 32'h0;
      end
   end
   endgenerate

   // unnamed with nested unnamed
   generate begin
      if (D) begin
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in4;
         assign out4 = outreg;
      end
      else begin
         assign out4 = 32'h0;
      end
   end
   endgenerate
      
endmodule

