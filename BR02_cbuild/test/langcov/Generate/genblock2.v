// last mod: Wed Oct 27 15:24:16 2004
// filename: test/langcov/Generate/genblock2.v
// Description:  This test is bug2.v from bug2939, (installed here because it is
// a generate issue)


module genblock2(a, b, c);
   input a, b;
   output c;

   bug #("OR") bug(.a(a), .b(b), .c(c));

endmodule

module bug(a, b, c);
   input a, b;
   output c;

   parameter OP = "AND";

   generate
      if (OP == "AND") begin
	 assign c = a & b;
      end
      else if (OP == "OR") begin
	 assign c = a | b;
      end
   endgenerate
endmodule

