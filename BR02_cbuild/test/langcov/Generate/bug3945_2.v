// last mod: Mon Jul  2 07:26:10 2012
// filename: test/langcov/Generate/bug3945_2.v
// Description:  This test is the example from bug3945, with the modification
// that the generate block is unnamed we expect to see the
// names of the generate blocks as genblk0 and genblk1
// or the full pathname to the OR_GATE as: bug3945_2.genblk1.u_or_gate


module bug3945_2 (do, di, clk);
parameter GATE = 1'b0;
parameter AND_GATE = 1'b1;
parameter OR_GATE = 1'b0;
input [1:0] di;
input clk;
output do;

wire do;

generate
  case ({GATE, GATE})
    {1'b1, AND_GATE}:
    begin  // unnamed
        and_gate u_and_gate (do, di[0], di[1]);
    end

    {1'b0, OR_GATE}:
    begin  // unnamed
        or_gate u_or_gate (do, di[0], di[1]);
    end

    default:
    begin
      assign do = ~di[0];
    end

  endcase
endgenerate
endmodule

module and_gate (z, a, b);
input a, b;
output z;
wire z;

   initial
     $display("%m");

   assign z = a & b;

endmodule

module or_gate (z, a, b);
input a, b;
output z;
wire z;

   initial
     $display("%m");

   assign z = a | b;

endmodule
