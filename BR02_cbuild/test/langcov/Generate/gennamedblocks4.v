// testing the variations of named generate blocks
// testcase test/langcov/Generate/genenamedblocks4.v
// here we have:
//   a    named generate block with a task declaration
//   a un-named generate block with a task declaration


module top(clk, in1, in2, out1, out2);
   input clk;
   input [31:0] in1, in2;
   output reg [31:0] out1, out2;

   parameter 	 A = 1;
   parameter 	 B = 1;

   // named with task declaration
   generate begin: out1_block
      if (A) begin
	 task tsk1;
         output [7:0] otsk0;
         input [7:0] i1;
         begin
	     otsk0 = i1;
          end
         endtask

	 always @(in1)
	   tsk1(out1, in1);
      end
      else begin
	 always @(in1) 
	   out1 = 32'h0;
      end
   end
   endgenerate

   // unnamed with task declaration
   generate begin
      if (B) begin
	 task tsk2;
         output [7:0] otsk0;
         input [7:0] i1;
         begin
	     otsk0 = i1;
          end
         endtask
	 
	 always @(in1)
	   tsk2(out2, in2);
      end
      else begin
	 always @(in1) 
           out2 = 32'h0;
      end
   end
   endgenerate
endmodule

