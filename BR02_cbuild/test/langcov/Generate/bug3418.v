// this testcase is from bug3418, the problem was that there was a name conflict
// between the outreg from the out1_block and the out2_block at one
// time. (because we did not create named contexts for their declarations)
module bug3418(clk, in1, in2, out1, out2);
   input clk;
   input [31:0] in1, in2;
   output [31:0] out1, out2;

   parameter 	 A = 1;
   parameter 	 B = 1;

   generate begin: out1_block
      if (A) begin
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in1;
         assign out1 = outreg;
      end
      else begin
         assign out1 = 32'h0;
      end
   end
   endgenerate
      
   generate begin: out2_block
      if (B) begin
	 reg [31:0] outreg;
	 always @(posedge clk)
	   outreg <= in2;
         assign out2 = outreg;
      end
      else begin
         assign out2 = 32'h0;
      end
   end
   endgenerate
      
endmodule

