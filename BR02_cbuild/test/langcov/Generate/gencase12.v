// last mod: Wed Mar 14 15:54:49 2007
// filename: test/langcov/Generate/gencase12.v
// Description:  This test checks to make sure that we properly report a missing
// module that is found to be missing within an active generate case branch.


module gencase12(clk, w_in1, in1, in2, out1, w_out1);
   input clk;
   input w_in1;
   input [7:0] in1, in2;
   output [7:0] out1;
   output 	w_out1;
   reg [7:0] out1;

parameter TARGET_TECH = "FOO1";
 

generate
   case ("FOO1")

            "FOO1":  \#%fooA foo1 (   // note fooA is undefined 
                        .clkin(clk),
                        .in1  (w_in1),
                        .out1 (w_out1)
                        );
 

            "FOO2":  foo1 foo2 (// this and  all branchs below are unused
                        .clkin(clk),
                        .in1  (w_in1),
                        .out1 (w_out1)
                        );

 

            "FOO3":  foo3  foo3 (
                        .clkin(clk),
                        .in1  (w_in1),
                        .out1 (w_out1)
                        );

 

            default: foo4 foo4 (
                        .clkin(clk),
                        .in1  (w_in1),
                        .out1 (w_out1)
                        );

   endcase
endgenerate

   
   always @(posedge clk)
     begin: main
       out1 = in1 & in2;
     end
endmodule


module foo1(input clkin, input in1, output reg out1);

   initial
     out1 = 0;
   
   always @(posedge clkin)
     begin: main
       out1 = in1;
     end
endmodule
