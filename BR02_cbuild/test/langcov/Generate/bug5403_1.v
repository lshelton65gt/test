// last mod: Mon Dec 19 17:08:51 2005
// filename: test/langcov/Generate/bug5403_1.v
// Description:  This test is an attempt to duplicate the issue found in bug5403
// if you run with the begin/end of the generate for commented out then this would 
// crash in cheetah, must be run with -2001 


module mod1 (output reg [2-1:0]       done);

wire [5:0]         internal_wire;


genvar n;

generate
begin
   for (n=0; n<2; n=n+1)
//      begin
        assign internal_wire[(n*3)+:3] = n[2:0];
//      end
end
endgenerate

endmodule
module mod2 (output wire foo);
   localparam addr0_width      = 30;

   reg [(addr0_width*(1<<2))-1:0] addr0;
   reg [7:0] wire2;
   
   wire [1:0]                       internal_wire;

   always @* begin
           wire2 = addr0[addr0_width +: 1] ;
   end

endmodule
