// last mod: Tue Oct 26 17:03:54 2004
// filename: test/langcov/Generate/genif4.v
// Description:  THIS IS NOT A GENERATE_IF

// The if is not handled as a generate_if because of the surrounding always block.


module genif4(a,b,c);
   input a, b;
   output c;
   reg 	  cr;
   parameter OP = "AND";
   
   generate
      begin      // generate_block
	 assign c = cr;
	 always @(*)
              begin
		 if ( a )	// this does not need to be a constant because it is not a generate_if
	           cr = a & b;
	      end
      end
   endgenerate
endmodule
