module top(i1, i2, i3, clk, o1_1, o2_1, o1_2, o2_2, o1_3, o2_3);
output [8:0] o1_1;
output [8:0] o2_1;
output [8:0] o1_2;
output [8:0] o2_2;
output [8:0] o1_3;
output [8:0] o2_3;
reg [8:0] o1_1;
reg [8:0] o2_1;
reg [8:0] o1_2;
reg [8:0] o2_2;
reg [8:0] o1_3;
reg [8:0] o2_3;
input [8:0] i1;
input [8:0] i2;
input [8:0] i3;
input clk;

task doplus;
  output [8:0] o;
  input [8:0] a;
  input [8:0] b;
  o <= a + b;
endtask

always @(posedge clk)
begin
  doplus(o1_1, i1, i2);
  o2_1 = o1_1 + i3;
end

always @(posedge clk)
begin
  o1_2 <= i1 + i2;
  o2_2 = o1_2 + i3;
end

always @(posedge clk)
begin
  begin : inlined_doplus
    reg [8:0] task_tmp_a;
    reg [8:0] task_tmp_b;
    reg [8:0] task_tmp_o;
    task_tmp_a = i1;
    task_tmp_b = i2;
    task_tmp_o <= task_tmp_a + task_tmp_b;
    o1_3 = task_tmp_o;
  end
  o2_3 = o1_3 + i3;
end

endmodule
