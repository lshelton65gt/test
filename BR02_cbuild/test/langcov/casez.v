module top (out1, out2, out3, out4, out5, out6, clk, in1, in2, in3, in4, sel);
   output [3:0] out1, out2, out3, out4, out5, out6;
   input 	clk;
   input [3:0] 	in1, in2, in3, in4;
   input [1:0] 	sel;


   // Flop the inputs
   reg [3:0] 	rin1, rin2, rin3, rin4, rsel;
   always @ (posedge clk)
     begin
	rsel = sel;
	rin1 = in1;
	rin2 = in2;
	rin3 = in3;
	rin4 = in4;
     end

   // Case 1, fully defined
   reg [3:0] cout1, out1;
   always @ (rsel or rin1 or rin2 or rin4)
     begin
	casez (rsel)
	  2'b0z: cout1 = rin1;
	  2'bz0: cout1 = rin2;
	  2'b11: cout1 = rin4;
	endcase // casez(rsel)
     end
   always @ (posedge clk)
     out1 = cout1;

   // Case 2, partially defined
   reg [3:0] cout2, out2;
   always @ (rsel or rin1 or rin3)
     begin
	casez (rsel)
	  2'b?1: cout2 = rin1;
	  2'b1?: cout2 = rin3;
	endcase // case(sel)
     end
   always @ (posedge clk)
     out2 = cout2;
   
   // Case 3, partially defined with default
   reg [3:0] cout3, out3;
   always @ (rsel or rin1 or rin3 or rin4)
     begin
	casez (rsel)
	  2'b?0: cout3 = rin1;
	  2'b0z: cout3 = rin3;
	  default: cout3 = rin4;
	endcase // case(sel)
     end
   always @ (posedge clk)
     out3 = cout3;
   
   // Case 4, many expressions per item
   reg [3:0] cout4, out4;
   always @ (rsel or rin1 or rin2)
     begin
	casez (rsel)
	  2'b0x,
	  2'bz0:
	    cout4 = rin1;
	  2'b11:
	    cout4 = rin2;
	endcase // case(rsel)
     end // always @ (rsel or rin1 or rin2 or rin3 or rin4)
   always @ (posedge clk)
     out4 = cout4;

   // Case 5, partial constant and variable items
//   reg [3:0] cout5, out5;
//   always @ (rin1 or rin2 or rin3 or rin4)
//     begin
//	casez (rin1)
//	  {2'b0z,rsel}: cout5 = rin2;
//	  {2'bz0,rsel}: cout5 = rin3;
//	  {2'b11,rsel}: cout5 = rin4;
//	endcase // casez(rin1)
//     end
//   always @ (posedge clk)
//     out5 = cout5;

endmodule // top

