/* Test aliasing. */
module top(i1, i2, i3, o1, o2, o3, o_multdrive, o_multdrive2);
input i1, i2, i3;
output o1, o2, o3, o_multdrive, o_multdrive2;
wire w, v, x;
wire a, b, c;
wor o_multdrive;
wor o_multdrive2;

assign w = i1 & i2;
assign o1 = w;

assign v = i1 & i2;
assign x = v;
assign o_multdrive = x;
assign o_multdrive = i3;

assign a = i1 | i2;
assign b = a;
assign c = a;
assign o2 = b & c;

foo inst1 (i1, i2, i3, o3);
bar inst2 (i1, i2, o_multdrive2);
assign o_multdrive2 = i3;

endmodule

module foo(i1, i2, i3, o);
input i1, i2, i3;
wor i2;
output o;
/* assign i2 = i3; */
assign o = i1 ^ i2;
endmodule

module bar(i1, i2, o);
input i1, i2;
output o;
assign o = i1 & i2;
endmodule
