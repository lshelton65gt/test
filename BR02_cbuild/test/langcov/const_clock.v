module top(clk, in1, in2, in3, in4, out1, out2, out3);
  input clk, in1, in2, in3, in4;
  output out1, out2, out3;
  reg out1, out2, out3, q, clk3;
  supply0 clk1;

  xor (clk2, in1, in1);

  initial begin
    out1 = 0;
    out2 = 0;
    out3 = 0;
  end

  always @(posedge clk1)
    out1 <= in1;

  always @(posedge clk2)
    out2 <= in2;

  always @(posedge clk)
    q <= in3;

  always @(q or out2)
    clk3 <= q ^ out2;

  always @(posedge clk3)
    out3 <= in4;
endmodule
