/* Test non-blocking assigns which need temps. */
module top(i1, i2, sel,
           o1, o2, o3, o4, o5, o6
          );

input i1, i2, sel;
output o1, o2, o3, o4, o5, o6;

reg a, b, c, d, e, f, r, s, t;

assign o1 = a;
assign o2 = b;
assign o3 = c;
assign o4 = d;
assign o5 = e;
assign o6 = f;

always
begin
  a <= r;
  r = i1;
  b <= a + r;
end

always
begin
  if (sel)
    c <= s;
  s = i2;
  d <= c + s;
end

   always
     begin
	if (i1)
	  begin
	     case (sel)
	       1'b0: e <= t;
	       1'b1: e <= ~t;
	     endcase // case(sel)
	     t = i1;
	  end
	f <= e + t;
     end

endmodule
