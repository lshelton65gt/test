// Test functions and latches
module top(i, sel, o);
input i, sel;
output o;

function fun;
input i, sel;
reg [3:0] a;
begin
  a[2:0] = a[2:0] + i;
  if (sel)
    fun = a[0];
end
endfunction

assign o = fun(i, sel);

endmodule
