/* Test assigns in declarations. */
module top(i1, i2, o, o1);
input i1, i2;
output [0:1] o1;
output o;
reg o;

wire w = i1 & i2;
wire [0:1] v = i1 + i2;

always
begin
o = w;
end

endmodule
