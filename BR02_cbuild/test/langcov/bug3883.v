// last mod: Wed Mar  9 14:46:05 2005
// filename: test/langcov/bug3883.v
// Description:  This test checks the error message that cheetah generates when
// it encounters a 2001 construct when it is not run with -v2001


module dut(in, out);
   input in;
   output out;
   parameter PARAM1 = 1'b1;

   generate case (PARAM1)
	      1'b0: begin
		 assign out = in;
	      end
	      default: begin
		 assign out = ~in;
	      end
	    endcase
   endgenerate
endmodule
