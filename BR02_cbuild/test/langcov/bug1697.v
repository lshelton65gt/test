// from bug 1697, in verilog the two $setuphold calls in the specify block
// create an implicit connection between clk and clk_d and between in and
// in_d. (LRM 15.5.2)
// carbon should create an assignment for each of these

module flop(clk, in, out);
   input clk, in;
   output out;

   wire   clk_d, in_d;
   reg 	  out;

   always @(posedge clk_d)
     out <= in_d;

// the following two assigns are created implicitly by the $setuphold system calls.   
//   assign clk_d = clk;
//   assign in_d = in;
   specify
      $setuphold(posedge clk, posedge in, 1, 1, , , , clk_d, in_d);
      $setuphold(posedge clk, negedge in, 1, 1, , , , clk_d, in_d);
   endspecify
   
endmodule // flop
