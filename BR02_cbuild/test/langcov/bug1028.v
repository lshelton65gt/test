module top(in, out0, out1);
   input [31:0] in;
   output [3:0] out0;
   output [4:0] out1;
   reg [3:0] 	out0;
   reg [4:0] 	out1;

   function [1:0] csa;
      input [2:0] in;
	 case (in)
	   3'h0: csa = 2'h0;
	   3'h1, 3'h2, 3'h4: csa = 2'h1;
	   3'h3, 3'h6, 3'h5: csa = 2'h2;
	   3'h7: csa = 2'h3;
	   default: csa = 2'hx;
	 endcase // case(in)
   endfunction

   function [1:0] ha;
      input [1:0] in;
	 case (in)
	   2'h0: ha = 2'h0;
	   2'h1, 2'h2: ha = 2'h1;
	   2'h3: ha = 2'h2;
	   default: ha = 2'hx;
	 endcase // case(in)
   endfunction

   function [2:0] wal7;
      input [6:0] in;
      reg a0, b0, a1, b1, c1;
      begin
	 {a1, a0} = csa(in[2:0]);
	 {b1, b0} = csa(in[5:3]);
	 {c1, wal7[0]} = csa({in[6], b0, a0});
	 wal7[2:1] = csa({c1, b1, a1});
      end
   endfunction

   task sum32;
      input [31:0] in;
      output [3:0] addin0;
      output [4:0] addin1;
      reg [2:0] a, b, c, d;
      reg a0, a1, b1, c1, d1;
      reg a2, b2, c2, d2, a3, b3;
      begin
	 a = wal7(in[6:0]);
	 b = wal7(in[13:7]);
	 c = wal7(in[20:14]);
	 d = wal7(in[27:21]);
	 {a1, a0} = csa(in[30:28]);
	 {b1, addin1[0]} = csa({in[31], a0, d[0]});
	 {c1, addin0[0]} = csa({a[0], b[0], c[0]});
	 {a2, addin0[1]} = csa({a[1], b[1], c[1]});
	 {b2, d1} = csa({d[1], a1, b1});
	 {a3, c2} = csa({a[2], b[2], c[2]});
	 {d2, addin1[1]} = ha({c1, d1});
	 {b3, addin0[2]} = csa({a2, c2, d2});
	 {addin1[3], addin1[2]} = ha({d[2], b2});
	 {addin1[4], addin0[3]} = ha({a3, b3});
      end
   endtask

   always @(in)
     begin
	sum32(in, out0, out1);
     end

endmodule
