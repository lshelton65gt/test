// filename: test/event_01.v
// Description:  This test shows that verific flow does not properly ignore event expressions
// within combinational block, inspired by bug1458

module event_01 (A, B, OUT1);
   input [ 7: 0] A, B;
   output        OUT1;
   reg 		 OUT1;

   always
     begin : BLOCK1
	OUT1 = (A === B);
	@(A or B);
     end

endmodule

