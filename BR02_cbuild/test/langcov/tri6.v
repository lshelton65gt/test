// last mod: Mon Jul 23 07:58:05 2007
// filename: test/langcov/tri6.v
// Description:  This test checks to see that the use of bufif1/bufif0 with
// incorrectly sized enable terms still work properly.

module tri6(i1, e2, i4, e3, e5, o1
	    //, o4, p4
	    );
   input i1;
   input [1:0] e2;
   input [3:0] i4;
   input [2:0] e3;
   input [4:0] e5;
   
   output      o1;
//   output [3:0] o4, p4;

  bufif1(o1, i1, e2);		// the enable here should be 1 bit
//  bufif0(o4, i4, e5);		// the enable here should be 4 bits
//  bufif0(p4, i4, e3);		// the enable here should be 4 bits

endmodule
