// Test unconnected ports
module top(in1, in2, o1, o2);
   input in1;
   input in2;
   output o1;
   output o2;
   foo inst1 (,in1, in2, o1,,o2,);
endmodule // top

module foo (,i1, i2, o1,,o2,);
   input i1, i2;
   output o1, o2;
   assign o1 = i1 & i2;
   assign o2 = i1 | i2;
endmodule // foo
