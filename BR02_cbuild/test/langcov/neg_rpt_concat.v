// This example is from Sony

module top(indat);

input  [2:0] indat;
wire [13:0] sig14b;
wire [12:0] sig13b;


mod1 #(13,4,14) mod1(
                .a              (sig14b),
                .b              ({1'b0,indat}),
                .x              (sig13b)
                );



endmodule

module mod1 (a, b, x);
    //synopsys template
    parameter		A_WL = 8, B_WL = 8, X_WL = 9;

    input [A_WL-1:0]	a;
    input [B_WL-1:0]	b;
    output [X_WL-1:0]	x;
    
    parameter		DIFFBA = B_WL - A_WL + 1;

    assign x = func_add_s(a, b);

    function [X_WL-1:0]		func_add_s;
   	input [A_WL-1:0]	a; 
   	input [B_WL-1:0]	b; 
	begin
                // Note that as instantiated, DIFFBA will be -8
	    	func_add_s = {{DIFFBA{a[A_WL-1]}}, a} + {b[B_WL-1], b};
    	end
    endfunction
endmodule
