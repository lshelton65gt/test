// for this this udp  no output expression is created so we do not support it.
//   The test is expected to fail.
module dut(go_to_x, setup_hold_viol, timing_viol, timing_viol_ok);
   output go_to_x;
   input  setup_hold_viol, timing_viol, timing_viol_ok;
   udp1 u1( go_to_x, setup_hold_viol, timing_viol, timing_viol_ok);
endmodule
primitive udp1 ( go_to_x, setup_hold_viol, timing_viol, timing_viol_ok);
  output go_to_x; reg go_to_x;
  input setup_hold_viol, timing_viol, timing_viol_ok;

   table
    // d0 d1 d2 : q0 : q
       // setup/hold VIOLATION
       *  ?  ? :  0 :  1;
       *  ?  ? :  1 :  0;
       *  ?  ? :  x :  0;
       // timing violation
       ?  *  x :  0 :  1;
       ?  *  x :  1 :  0;
       ?  *  x :  x :  0;
       ?  *  b :  ? :  -;
       // ignore changes on 'OK' inputs
       ?  ?  * :  ? :  -;
   endtable
endprimitive
