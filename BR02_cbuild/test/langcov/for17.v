module top ( out, in, clk );
   input clk;
   input [2:0] in;
   output [2:0] out;
   reg [2:0] 	out;
   integer 	i;
   
   always @(in or clk)
     begin
	for (i = 0; i <= 2; i = i + 1)
	  if ((i != 2) || clk)
            out[i] = in[i];
     end
endmodule // top
