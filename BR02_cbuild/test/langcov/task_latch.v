// Test task latch warnings

module top(i, sel, o);
   input i;
   input sel;
   output o;
   reg 	  o;

   always
     begin
	latch(i, sel, o);
     end

   task latch;
      input d;
      input sel;
      output o;
      reg r;
      begin
	 if (sel)
	   begin
	      r = d;
	   end
	 o = r;
      end
   endtask // t1
   
endmodule // top
