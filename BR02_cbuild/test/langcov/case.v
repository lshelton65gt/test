module top (out1, out2, out3, out4, out5, out6, clk, in1, in2, in3, in4, sel);
   output [3:0] out1, out2, out3, out4, out5, out6;
   input 	clk;
   input [3:0] 	in1, in2, in3, in4;
   input [1:0] 	sel;


   // Flop the inputs
   reg [3:0] 	rin1, rin2, rin3, rin4, rsel;
   always @ (posedge clk)
     begin
	rsel = sel;
	rin1 = in1;
	rin2 = in2;
	rin3 = in3;
	rin4 = in4;
     end

   // Case 1, fully defined
   reg [3:0] cout1, out1;
   always @ (rsel or rin1 or rin2 or rin3 or rin4)
     begin
	case (rsel)
	  2'b00: cout1 = rin1;
	  2'b01: cout1 = rin2;
	  2'b10: cout1 = rin3;
	  2'b11: cout1 = rin4;
	endcase // case(sel)
     end
   always @ (posedge clk)
     out1 = cout1;

   // Case 2, partially defined
   reg [3:0] cout2, out2;
   always @ (rsel or rin1 or rin2 or rin3 or rin4)
     begin
	case (rsel)
	  2'b00: cout2 = rin1;
	  2'b10: cout2 = rin3;
	endcase // case(sel)
     end
   always @ (posedge clk)
     out2 = cout2;
   
   // Case 3, partially defined with default
   reg [3:0] cout3, out3;
   always @ (rsel or rin1 or rin2 or rin3 or rin4)
     begin
	case (rsel)
	  2'b00: cout3 = rin1;
	  2'b10: cout3 = rin3;
	  default: cout3 = rin4;
	endcase // case(sel)
     end
   always @ (posedge clk)
     out3 = cout3;
   
   // Case 4, fully defined with two outputs and non-blocking statements
   reg [3:0] cout4, cout5, out4, out5;
   always @ (rsel or rin1 or rin2 or rin3 or rin4)
     begin
	case (rsel)
	  2'b00:
	    begin
	       cout4 <= rin1;
	       cout5 <= cout4;
	    end
	  2'b01:
	    begin
	       cout4 <= rin2;
	       cout5 <= cout4;
	    end
	  2'b10:
	    begin
	       cout4 <= rin3;
	       cout5 <= cout4;
	    end
	  2'b11:
	    begin
	       cout4 <= rin4;
	       cout5 <= cout4;
	    end
	endcase // case(sel)
     end // always @ (rsel or rin1 or rin2 or rin3 or rin4)
   always @ (posedge clk)
     begin
	out4 = cout4;
	out5 = cout5;
     end

   // Case 5, variable case items
   reg [3:0] cout6, out6;
   always @ (rsel or rin1 or rin2 or rin3 or rin4)
     begin
	case (rsel)
	  rin1[1:0],
	  rin2[1:0]:
	    cout6 = rin3;
	  rin1[3:2],
	  rin2[3:2]:
	    cout6 = rin4;
	endcase // case(rsel)
     end // always @ (rsel or rin1 or rin2 or rin3 or rin4)
   always @ (posedge clk)
     out6 = cout6;

endmodule // top

