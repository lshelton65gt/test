// Test parameter/function interaction

module top(i1, i2, i3, i4, o1, o2, o3, o4);
input [2:0] i1;
input [2:0] i2;
input [9:0] i3;
input [9:0] i4;
output [2:0] o1;
output [2:0] o2;
output [9:0] o3;
output [9:0] o4;

bar #(3,3) inst1 (i1, i2, o1);
bar inst2 (i1, i2, o2);
bar #(10,10) inst3 (i3, i4, o3);
bar #(10,10) inst4 (i3, i4, o4);

endmodule

module bar(i1, i2, o);
  parameter O_WIDTH = 3;
  parameter I_WIDTH = 3;

  input [I_WIDTH-1:0] i1;
  input [I_WIDTH-1:0] i2;
  output [O_WIDTH-1:0] o;

  function [O_WIDTH-1:0] fun;
    input [I_WIDTH-1:0] i1;
    input [I_WIDTH-1:0] i2;
    begin : named
      reg [I_WIDTH-1:0] r;
      r = i1 + i2;
      fun = r;
    end
  endfunction

  assign o = fun(i1,i2);
endmodule
