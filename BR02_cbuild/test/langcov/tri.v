/* Test tristates. */
module top(en, en2, i, o, iv, ih1, ih2,
           ov, oh, oj, ok, ol
          );
input en, i, en2;
input [3:0] iv;
input [3:0] ih1;
input [3:0] ih2;
output [3:0] ov;
output [3:0] oh;
output [3:0] oj;
output [3:0] ok;
output [3:0] ol;
output o;
trireg reg_o;
assign reg_o = en ? i : 1'bz;
assign o = reg_o;
assign ov = en ? iv : 4'bz;
assign oh = en ? ih1 : 4'bz;
foo inst1 (en2, ih2, oh);
foo inst2 (en2, ih2, oj);
bar inst3 (en2, ih2, ok);

assign ol = oj | oh;
endmodule

module foo(en, i, o);
input en;
input [3:0] i;
output [3:0] o;
assign o = ~en ? 4'bz : i;
endmodule

module bar(en, i, o);
input en;
input [3:0] i;
output [3:0] o;
assign o = en ? i : 4'bz;
endmodule
