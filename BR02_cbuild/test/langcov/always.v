/* Test simple always blocks.  Also test chained assigns. */
module top(in1, in2, out1, out2, out3);
input in1, in2;
output out1, out2, out3;

alway inst1 (in1, out1);
myinst inst2 (in2, out2, out3);

endmodule


module myinst(in, out1, out2);
input in;
output out1, out2;

assign out1 = in;
assign out2 = out1;

endmodule

module alway(in1, out1);
input in1;
output out1;
reg reg1, reg2, out1;

always
begin
  reg1 = in1;
  reg2 = reg1;
end

always
begin
out1 = reg2;
end

endmodule
