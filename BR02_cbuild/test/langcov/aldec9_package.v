// last mod: Mon Nov  7 07:13:28 2005
// filename: test/langcov/aldec9_package.v
// Description:  This test shows that aldec (riviera) does not simulate the "+:"
// type of partselct on the left hand side for numbers longer than 64 bits
// reported to aldec 11/07/05

module aldec9_package ( out);
   reg [7:0] sel;
   output [0:64] out;
   reg [0:64] 	 out;
   
   initial begin
      out = 0;
      #10 sel = 8'b00000000;
      $display($time, " %b %b",sel, out);
      $display("should be:        10 00000000 00000000000000000000000000000000000000000000000000000000000000000");
      $display("\n");
      #10 sel = 8'b01000000;
      $display($time, " %b %b",sel, out);
      $display("should be:        20 01000000 00000000000000000000000000000000000000000000000000000000000000001");
      $display("\n");
      #10 sel = 8'b00100000;
      $display($time, " %b %b",sel, out);
      $display("should be:        30 00100000 00000000000000000000000000000000110011011110111100000000000000010");
      $display("\n");
      #10 sel = 8'b00000010;
      $display($time, " %b %b",sel, out);
      $display("should be:        40 00000010 00110011011110111100000000000000010000000000000000000000000000000");
   end
   
   always @(sel)
     begin
        out = 65'b0;
	out[sel +: 32] = 32'b11001101111011110000000000000001;
        

     end
endmodule // aldec9_package


