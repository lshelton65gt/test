module top(a1, a2, c1, c2, d, o1, o2);
input a1, a2, c1, c2, d;
output o1,o2;
wire o1;
wand b;
assign b = a1 | a2;
assign b = c1 | c2;
assign o1 = b | d;
assign o2 = b;
endmodule
