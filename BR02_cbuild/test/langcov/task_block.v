// This is a narrowed-down testcase which showed up with -inlineTasks,
// the issue was that task inlining was creating locals in nested blocks.
module bug(gen, correct_n, datain, chkin, err_detect, err_multpl, dataout, 
	chkout);

	parameter		width		= 32;
	parameter		chkbits		= 7;
	parameter		synd_sel	= 0;

	input			gen;
	input			correct_n;
	input	[(width - 1):0]	datain;
	input	[(chkbits - 1):0]
				chkin;
	output			err_detect;
	output			err_multpl;
	output	[(width - 1):0]	dataout;
	output	[(chkbits - 1):0]
				chkout;


	function [30:0] foo;

	input reg
		[30:0]		in1;
	input reg
		[30:0]		in2;
	    foo = 1;
	endfunction

   	integer			bar;
	integer			sel;
	integer			r;
	integer			s;
	integer			q;
	integer			t;

	initial begin
	      for (bar = 0; ((bar < 10) ); bar = (bar + 1)) begin
		    if (sel < 8)
		      r = foo(s, sel);
		    else
			bar = (t + q);
	      end
	    end
endmodule
