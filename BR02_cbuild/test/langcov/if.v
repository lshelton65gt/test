/* Test if statements. */
module top(a, b, o, o1, sel1, sel2, c
          );

input a, b, sel1, sel2;
input c;
output o, o1;
reg o;
reg tmp;

assign o1 = tmp;

always
begin
  o = c;
  tmp = c;
  if (sel1)
  begin
    tmp = b;
    begin
      if (sel2)
        o = a;
    end
    tmp = tmp + a;
  end
  else
  begin
    o = b;
    tmp = tmp;
  end
end

endmodule
