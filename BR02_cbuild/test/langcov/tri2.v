/* Test tristates - hierarchy. */
module top(i1, e1, i2, e2, i3, e3,
           o1
          );
  input i1, e1, i2, e2, i3, e3;
  output o1;
  wire w;
  bufif1(w, i1, e1);
  foo inst1 (w, o1, i2, e2, i3, e3);
endmodule

module foo(a, o, a1, ae1, a2, ae2);
  input a, a1, ae1, a2, ae2;
  output o;
  wire a;
  wire o;
  assign o = a;
  notif0(o, a1, ae1);
  bar inst1 (o, a2, ae2);
endmodule

module bar(o, b, be);
  input b, be;
  output o;
  assign o = be ? 1'bz : b;
endmodule
