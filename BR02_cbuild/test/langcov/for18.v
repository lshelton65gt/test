// this module needed an iterative UD algorithm to properly compute
// the dependencies for out[].

module top ( in, out, sel );
   input sel;
   
   input [2:0] in;
   output [2:0] out;
   reg [2:0] 	out;

   reg 		sav;

   always @(in or sel)
     begin :forblk
	integer i;
	sav = 0;
	for ( i = 0 ; i <= 2 ; i = i + 1 )
	  begin
	     out[i] = sav;
	     if ( sel )
	       sav = in[i];
	  end
     end
endmodule // top
