// last mod: Thu Jan  5 09:36:46 2006
// filename: test/langcov/Gigantic/inst_generate_3000.v
// Description:  This test creates 3000 instances via a generate for

module instance_3000(clock, in1, in2, out);
   input clock;
   input [2999:0] in1, in2;
   output [2999:0] out;

  genvar i;
  generate
   for ( i = 0; i < 3000; i = i + 1)
     begin:this_is_the_label
	m u(clock, out[i], in1[i], in2[i]);
     end
  endgenerate
endmodule

module m(clock, out, in1, in2);
   input clock;
   input in1, in2;
   output out;
   reg out;

   always @(posedge clock)
     begin: main
       out = in1 & in2;
     end

endmodule
