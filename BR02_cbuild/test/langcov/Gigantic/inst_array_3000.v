// last mod: Wed Jan  4 15:25:00 2006
// filename: test/langcov/InstanceArray/inst_array_3000.v
// Description:  This test has 3000 instances.  


module inst_array_3000(clock, in1, in2, out1);
   input clock;
   input [2999:0] in1, in2;
   output [2999:0] out1;

   m nand_array[1:3000](out1, in1, in2);
   
endmodule
module m(out,in1,in2);
   input in1, in2;
   output out;

   assign out = in1 & ! in2;
endmodule
