/* Basic bidirect test. */
module top( sel1, sel2, sel3, a, b, c,
            bus, o
          );
inout bus;
output o;
input a, b, c, sel1, sel2, sel3;
assign bus = sel1 ? a : 1'bz;
assign bus = sel2 ? b : 1'bz;
foo inst1 (bus, sel3, c, o);
endmodule

module foo(bus, sel, a, o);
inout bus;
input sel, a;
output o;
assign o = sel ? bus : a;
endmodule
