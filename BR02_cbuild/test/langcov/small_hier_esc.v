// file: small_hier_esc.v
// created: Fri Nov  7 11:26:59 2003
// author: cloutier
// General topic: a small version of medium_hier.v, used to examine how clocks
// within hierarchy are handled.
// this particular version has escaped names at the lower levels, at one time
// this would not compile



module L0 ( data_out, clk_h, data_in, clk);
   input       data_in;
   input       clk;
   output      data_out;
   output      clk_h; 		// divided clock

   wire        data_u1_L0;
   wire        data_u2_L0;
   wire        data_u3_L0;
   
   wire        clk_h_u1_L0;
   wire        clk_h_u2_L0;
   wire        \clk_h_u3.L0 ;
   
   CDU  u1_L0 ( data_u1_L0,  clk_h_u1_L0, data_in,    clk         );
   CDU  u2_L0 ( data_u2_L0,  clk_h_u2_L0, data_u1_L0, clk_h_u1_L0 );
   CDU  u3_L0 ( data_u3_L0,  \clk_h_u3.L0 , data_u2_L0, clk_h_u2_L0 );
   CDU  u4_L0 ( data_out,    clk_h,       data_u3_L0, \clk_h_u3.L0 );
endmodule // L0


// clock divisor unit (division and consumption (via scan like chain) )
module CDU( data_out, \CKL_H.dot , data_in, \EEE.CLK );
   input       data_in;
   input       \EEE.CLK ;
   output      data_out;
   output      \CKL_H.dot ; 		// divided clock

   wire        data_temp;

   CD   cd1 ( \CKL_H.dot , \EEE.CLK  );
   
   R1   r1 ( data_temp, data_in,   \EEE.CLK    );
   R1   r2 ( data_out,  data_temp, \CKL_H.dot  );
   
endmodule // CDU

// clock divider
module CD ( out, \DDD[2]  );
   input \DDD[2] ;
   output out;
   reg    out;

   initial out = 0;
   
   always @(posedge \DDD[2] )
      out <= ~out;
endmodule // cd

// simple register   
module R1 (out, in, \CCC[1] );
   input       in;
   input       \CCC[1] ;
   output      out;
   reg         out;

   initial out = 0;
   
   always @(posedge \CCC[1] )
     out <= in;
endmodule // R1

