// Test function non-local references and rewriting.
module top(i1, i2, i3, o1, o2, o3);
   input [3:0] i1;
   input [3:0] i2;
   input [3:0] i3;
   output [3:0] o1;
   output [3:0] o2;
   output [3:0] o3;
   reg [3:0] 	o2;
   reg [3:0] 	o3;

   function adder;
      input [3:0] i2;
      begin
	 adder = i1 + i2;
      end
   endfunction // adder

   assign o1 = adder(i1);

   always @(i1 or i2 or i3) {o2[{adder(i2)}]} = i3;

   always @(i1 or i2 or i3)
     begin
	case (4'b1)
	  adder(i3) : o3 = 4'b0;
	  default : o3 = 4'b1111;
	endcase
     end

endmodule // top
