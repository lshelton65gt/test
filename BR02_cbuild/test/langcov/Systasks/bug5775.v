// signedness issues with $time.  Gold file from Carbon as clock lsbs don't
// track between carbon and aldec
module top(c, cmp);
   input c;
   output cmp;

   reg t;
   assign cmp = t;
   always @(posedge c)
     begin
        // $display("time is %d", $time);
        t = ($time == 64'sd200);
     end
   
   always @(negedge c)
     t = ($time == 64'd200);
endmodule

