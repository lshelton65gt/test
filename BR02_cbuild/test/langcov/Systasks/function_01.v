// last mod: last mod: Thu Aug  3 14:59:18 2006
// filename: test/langcov/Systasks/function_01.v
// Description:  This test checks to see if you can call a system function
// ($unsigned()) from within a function used to define a parameter, or in a
// constant context


module function_01(clock, in1, in2, out1);
   input clock;
   input signed [31:0] in1, in2;
   output [10:0] out1;


   defparam 		      U1.MSB = make_unsigned(-10);
   function [31:0] make_unsigned;
      input signed [31:0] val;
      begin
	 make_unsigned = $unsigned(val);
      end
   endfunction

   mid U1(clock, in1, in2, out1);
endmodule

module mid(clock, in1, in2, out1);
   parameter 		      MSB = 1;
   parameter 		      LSB = $unsigned(0);
   input clock;
   input signed [31:LSB] in1, in2;
   output [MSB:LSB] out1;
   reg [MSB:LSB] out1;

   
   always @(posedge clock)
     begin
       out1 = in1 + in2;
     end
endmodule
