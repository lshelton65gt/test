// last mod: Mon Dec  5 16:38:35 2005
// filename: test/langcov/Systasks/bug5406_1.v
// Description:  At one time this test had an internal error during population
// caused by no statement list being defined for the current context


module bug5406_1(a);
   output [7:0] a;
   
   assign     a = $random;
endmodule
