// last mod: Mon Mar  6 16:17:52 2006
// filename: test/langcov/Systasks/setuphold_1.v
// Description:  This test was inspired by a converstaion with Ron K.  The issue
// here is that the nets that the $setuphold task makes an implied connection to
// are not defined.  This is a cheetah error.


module setuphold_1(q, ck, d);
   output q;
   input  ck, d;
   wire	  q, ck, d;
   reg 	  q_b;
   // wire   ck_b;
   // wire   d_b;

   buf(q, q_b);
   always @(posedge ck)
     q_b = d;

   specify
      $setuphold(posedge ck, posedge d, 1, 1, , , , ck_b, d_b);
      $setuphold(posedge ck, negedge d, 1, 1, , , , ck_b, d_b);
   endspecify

endmodule
