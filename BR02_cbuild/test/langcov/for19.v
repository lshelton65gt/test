// this module needed an iterative UD algorithm to properly compute
// the dependencies for out[].

module top ( in, out, sel );
   input [2:0] sel;
   input [2:0] in;

   output [2:0] out;
   reg [2:0] 	out;

   reg 		sav_a;
   reg 		sav_b;

   always @(in or sel)
     begin :forblk
	integer i;
	sav_a = 0;
	sav_b = 0;
	for ( i = 0 ; i <= 2 ; i = i + 1 )
	  begin
	     out[i] = sav_a;
	     if ( sel[0] )
	       begin
		  sav_a = sav_b;
		  if ( sel[1] )
		    sav_b = in[i];
	       end
	  end
     end
endmodule // top
