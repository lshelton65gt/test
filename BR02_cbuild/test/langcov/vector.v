/* Test some basic vector stuff. */
module top(i1, i2, i3, constbitsel, dynbitsel, partsel,
	 negsel1, negsel2, negsel3
          );
input [3:1] i1;
input [0:1] i2;
input [-3:-1] i3;

output constbitsel;
output dynbitsel;
output partsel;
output negsel1;
output negsel2;
output negsel3;

assign partsel = i1[2:1];
assign dynbitsel = i1[i2];
assign constbitsel = i1[2];
assign negsel1 = i3[-3];
//assign negsel2 = i3[-2'b11];
assign negsel3 = i3[55-56];

endmodule
