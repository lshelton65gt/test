// last mod: Wed Dec  7 13:16:09 2005
// filename: test/langcov/escaped_name_1.v
// Description:  This test has an escaped name, and at one time got a codegen error when run
// with -Wc -g  (a backslash space would be inserted at the end of a line when
// these switches were use).
// found while trying to fix test/mixed-lang/beacon_VHDL_top_p_ns/SpecialInstances7

// gold file from aldec, main.v required minor editing because of escaped name
module escaped_name_1(\1clock1\ , in1, in2, out1 );
   input \1clock1\ ;
   input [7:0] in1, in2;
   output [7:0] out1 ;
   reg [7:0] out1 ;

   always @(posedge \1clock1\ )
     begin: main
       out1  = in1 & in2;
     end
endmodule
