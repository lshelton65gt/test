// filename: test/langcov/time_signed.v
// Description:  This test checks to see if a time variable is signed or unsigned
// in interra flow they are incorrectly marked as signed, while in verific flow
// they are marked as unsigned.


module time_signed(clock, in1, out1) ;
   input clock;
   input signed [7:0] in1;
   output signed [64:0] out1;
   reg signed    [64:0] out1;
   time        t = -1 ;

   always @(posedge clock)
     begin: main
	out1 = t + in1;
     end
endmodule
