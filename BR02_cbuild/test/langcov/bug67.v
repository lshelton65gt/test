// Test of parameterized delays
module aBug (A, B, C);
   parameter DLY = 50;   
   
   input A, C;
   output B;
   reg    B;
 
   always @(posedge C) begin
      #50 B = A;     // Works
      B = #50 A;     // Works
      B = #(DLY)A;   // Works
      #(DLY) B = A;  // Don't work
   end
endmodule // aBug
