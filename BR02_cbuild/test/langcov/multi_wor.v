/* Test multiple drivers for wired-or resolution. */
module top(a1, a2, c1, c2, d, e, f, o1, o2, sel);
input a1, a2, c1, c2, d;
input [1:2] e, f;
input [0:1] sel;
output o1;
output o2;
wire o1;
wire b;
wor [0:3] o2;
assign o2[0] = a1 | a2;

foo inst1 (o2[1], d);
bar inst2 (o2[2:3], e, f);

assign b = c1;
assign b = c2;
assign o1 = b & o2[sel];

endmodule


module foo (z, a);
input a;
output z;
assign z = a;
endmodule


module bar (z, a, b);
input [0:1] a, b;
output [0:1] z;
assign z = a & b;
endmodule
