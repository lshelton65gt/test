module bug(in, out0, out1, clk);
   input [5:0] in;
   input       clk;
   output [2:0] out0, out1;

   reg [2:0] 	mem [1:0];

   assign 	out1 = mem[1];
   assign 	out0 = mem[0];

  always @ (in) begin
    mem[0] = in[5:3];
    mem[1] = in[2:0];
  end
endmodule
