// last mod: Wed Feb  8 10:48:49 2006
// filename: test/langcov/Unsigned/pedge_1.v
// Description:  This test is to check the size used for a vector in a posedge expression


module pedge_1(in1, in2, out1);
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   initial
     out1 = 8'b01010101;
   
   always @(posedge in1)
     begin: main
       out1 = in2;
     end
endmodule
