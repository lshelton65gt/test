// last mod: Wed Feb  8 10:19:57 2006
// filename: test/langcov/Unsigned/un_1.v
// Description:  This test checks that resizing is done before a unary not is performed.
// we expect out2 and x2 to be either 001 or 110, never 000 or 111.

module un_1(clock, in1, out3, x3);
   input clock;
   input in1;
   output [2:0] out3, x3;
   reg [2:0] 	out3;

   assign 	x3 = ~(in1);   // should resize in1 to 3 bits before bitwise not

   always @(posedge clock)
     begin: main
        out3 = ~(in1);   // should resize in1 to 3 bits before bitwise not
     end
endmodule
