// last mod: Wed May 10 14:42:32 2006
// filename: test/langcov/bug5693_1.v
// Description:  This test at one time would create an internal error in cbuild
// for the cases where we did an out of range partselect of a scalar or a single
// bit vector.
// gold file by inspection since bit/partselects of scalars is invalid in aldec

module bug5693_1(in1, in2, in3,  out0, out1, out2, out3, out4);
   input in1;
   input [0:0] in2;
   input [1:0] in3;
   
   output      out0, out1;
   output [1:0] out2, out3, out4;

   assign       out0 = in1[0 : 0];   // partselect of scalar (in range)
   assign 	out1 = in1[2 : 1];   // partselect of a scalar (completely out of range)
   assign       out2 = in2[2 : 1];   // partselect of single bit vector (completely out of range)
   assign 	out3 = in3[2 : 1];   // partselect of a vector (part out of range)
   assign       out4 = in3[4 : 2];   // partselect of vector (completely out of range)
endmodule
