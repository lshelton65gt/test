// looping with a non-1 step & non-blocking assigns
module skip ( out, in, clk );
   parameter smlen=4;
   parameter cnt=16;
   parameter biglen=cnt*smlen;
   
   input [biglen-1:0] in;
   input 	   clk;
   output [smlen-1:0] out;
   reg [smlen-1:0]    out;
   
   always @( posedge clk )
     begin :outblk
	integer i;
	for ( i = 0 ; i < biglen ; i = i + cnt )
	  begin :inblk
	     out[i] <= in[i];
	  end
     end

endmodule // skip

