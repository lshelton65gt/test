module search ( clk, op, in, addr, out );
   parameter asize=5;
   parameter msize=32; // 2**5
   parameter mcnt=128;

   input     clk;
   
   input  [1:0] op;
   
   input  [asize-1:0] addr;

   input  [msize-1:0] in;
   output [msize-1:0] out;
   reg    [msize-1:0] out;
   
   reg    [msize-1:0] sav;

   reg [msize-1:0]    memory [mcnt-1:0];
   
   integer 	      i;
   
   always @ ( posedge clk )
     begin
	case ( op )
	  2'b00: // reset everything
	    begin
	       for ( i = 0 ; i < mcnt ; i = i + 1 )
		 memory[i] <= 0;
	       out <= sav;
	    end

	  2'b01: // read
	    begin
	       sav <= memory[ addr ];
	       out <= memory[ addr ];
	    end
	  
	  2'b10: // write
	    begin
	       memory[ addr ] <= in;
	       out <= sav;
	    end
	  
	  2'b11: // read and write
	    begin
	       sav <= in;
	       memory[ addr ] <= sav;
	       out <= memory[ addr ];
	    end
	  
	endcase // case( op )
     end
   
endmodule // search
