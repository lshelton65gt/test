module top(in1, out1, out2, out3);
  input in1;
  output out1, out2, out3;
  reg    out3;

  child1 u1(out1);
  child1 u2(out2);
  child2 u3();

  always @(in1) begin
    u1.in1 = in1;
    u3.r = in1;
    out3 = u3.r;
  end
endmodule

module child1(out);
  output out;
  reg    in1;
  assign out = in1;
endmodule

module child2();
  reg r;
endmodule
