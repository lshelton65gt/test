module top(in1, ena1, in2, ena2, xorout);
   input in1, ena1, in2, ena2;
   output xorout;
   wire up, down;
   tri hang;
   trireg keep;
   wand nwand, twand;
   wor nwor, twor;

   assign up = ena1? in1: 1'bz;
   assign up = ena2? in2: 1'bz;
   pullup(up);

   assign down = ena1? in1: 1'bz;
   assign down = ena2? in2: 1'bz;
   pulldown(down);

   assign hang = ena1? in1: 1'bz;
   assign hang = ena2? in2: 1'bz;

   assign keep = ena1? in1: 1'bz;
   assign keep = ena2? in2: 1'bz;

   assign nwor = in1;
   assign nwor = in2;

   assign nwand = in1;
   assign nwand = in2;

   assign twor = ena1? in1: 1'bz;
   assign twor = ena2? in2: 1'bz;

   assign twand = ena1? in1: 1'bz;
   assign twand = ena2? in2: 1'bz;

   assign xorout = up ^ down ^ hang ^ keep ^ nwor ^ nwand ^ twor ^ twand;
 endmodule
