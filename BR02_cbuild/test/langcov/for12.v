module top ( a, clk );
   input clk;
   inout [7:0] a;
   reg [7:0]   tmp;

   integer 	i;

   assign 	a = tmp;
   
   always @(posedge clk)
     begin
	tmp[0] <= a[7];
	tmp[7] <= a[0];
	for ( i = 1 ; i < 7 ; i = i + 1 )
	  begin
	     tmp[7-i] <= a[i];
	  end
     end
   
endmodule // top
