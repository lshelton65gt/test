module concat(in1,in2,out,o2,o3);
input in1, in2;
output [1:0] out;
output [3:0] o2;
output [8:0] o3;

assign o2 = {1'b1, 2'b10, 1'b0};

assign o3 = {4{1'b1,1'b0}};

assign out={in1,in2};

endmodule
