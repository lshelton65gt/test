/* Test tristates - hierarchy. */
module top(i1, e1, i2, e2, i3, e3,
           o1, o2
          );
  input i1, e1, i2, e2, i3, e3;
  output o1, o2;
  wire w1, w2;
  buf(w1, i1);
  bufif1(w2, i1, e1);
  bar inst1(w1, o1, i2, e2);
  bar inst2(w2, o2, i3, e3);
endmodule

module bar(a, o, b, be);
  input b, be, a;
  output o;
  assign o = a;
  assign o = be ? 1'bz : b;
endmodule
