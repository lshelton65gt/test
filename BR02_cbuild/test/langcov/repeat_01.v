// filename: /home/cds/cloutier/Wrushc_b01/examples/repeat_01.v
// Description:  This test duplicates a problem seen at customer site, where parameter is used as repeat count in a declaration assignment to a wire.
// with r1356 this exits with ERROR: repeat_01.v:12, Unsupported not integer repeat expression


module repeat_01 #(parameter par1=2) (input clock,
				      input [par1:0] in1,
                                      input [par1:0] in2,
                                      output [par1:0] out1);


   genvar    i;
   wire  [par1:0]    temp = {par1{in1[par1]}} & in2[par1:0];

   assign out1 = temp;
endmodule
