// last mod: Thu Jun  1 15:28:21 2006
// filename: test/langcov/defparam4.v
// Description:  This test is just the small testcase from bug 6157
// it would crash if you ran with the directive:
//  warningMsg 20391
// the problem is that the line
//     defparam ubot_1.I_width_x=8;
// specifies a name that does not exist

module defparam4(o,i);
   output [7:0] o;
   input [7:0] 	i;
   mid umid_1(o,i);
endmodule

module mid(o,i);
   output [7:0] o;
   input [7:0] 	i;
   defparam ubot_1.I_width_x=8;
   defparam ubot_1.O_width=8;
   bot ubot_1(o,i);
endmodule

module bot(o,i);
   parameter I_width=2;
   parameter O_width=2;
   output [O_width-1:0] o;
   input [I_width-1:0] 	i;
   assign o = {O_width{&i}};
endmodule
