// filename: size01.v
// Description:  This test demonstrates a simulation mismatch we saw at Customer 1.
// here the parameter14 was incorrectly used as the value 33 when used as the concat multiplier

module param01(clock, in1, in3, out1) ;
   parameter WIDTH14 = 33'b1110;
   input clock;
   input  in1;
   input       in3;
   output [35:0] out1;
   reg [35:0] out1;
   
   always @(posedge clock)
     begin: main
	out1 = ({WIDTH14{in1}} );
     end
endmodule
