// Test parameter/task interaction

module top(i1_1, i2_1, i3_1, o_1,
	   i1_2, i2_2, i3_2, o_2);
   input [4:0] i1_1, i2_1, i3_1;
   input [45:0] i1_2, i2_2, i3_2;
   output [4:0] o_1;
   output [45:0] o_2;

   bar #(4) inst1 (i1_1, i2_1, i3_1, o_1);
   bar #(45) inst2 (i1_2, i2_2, i3_2, o_2);

endmodule // top


module bar(in1, in2, in3, o1);
   parameter PARAM = 4;

   input [PARAM:0] in1, in2, in3;
   output [PARAM:0] o1;

   reg [PARAM:0] o1;

   always
     begin
	t2(in1, in2, in3, o1);
     end
   
   task t2;
      input [0:PARAM] i1, i2, i3;
      output [0:PARAM] o;
      begin : named
	 reg [0:PARAM] r;
	 begin
	    r = i3;
	    t1(i1, i2, r);
	    o = r;
	 end
      end
   endtask // t2
   task t1;
      input [0:PARAM] i1, i2;
      inout [0:PARAM] io;
      begin : named
	 reg [PARAM:0] r;
	 r = i1 + io;
	 io = r + i2;
      end
   endtask // t1
   
endmodule // bar
