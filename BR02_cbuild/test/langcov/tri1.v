/* Test tristates - various constructs. */
module top(i1, i2, i3, e1, e2, e3, e4, e5, e6,
           o1, o2, o3, o4, o5, o6, o7, o8, o9,
           o10, o11
          );
  input i1, i2, e1, e2, e3, e4, e5, e6;
  input [3:0] i3;
  output o1, o2, o3, o4, o5, o6, o7, o9, o10, o11;
  output [3:0] o8;

  bufif1(o1, i1, e1);
  bufif0(o1, i2, e2);
  notif1(o2, i1, e1);
  notif0(o3, i2, e2);
  nmos(o4, i1, e1);
  rnmos(o5, i1, e1);
  pmos(o6, i1, e1);
  rpmos(o7, i1, e1);
  cmos(o9, i1, e3, e4);
  rcmos(o10, i2, e5, e6);
  and(strong1, highz0) aInst (o11, i1, i2);
   
  nmos foo[3:0] (o8, i3, e1);

endmodule
