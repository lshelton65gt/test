// last mod: Thu Jun  8 08:36:40 2006
// filename: test/langcov/bug6184.v
// Description:  This test is a variation on  test/interra/not_sup/INT_RG 
// except that it checks that the width of int1 is populated correctly (as 32 bits)
// by using a shift to examine the MSB.  Before 06/08/06 cbuild incorrectly
// populated the 33 bits specified in the declaration
// gold file from nc

module bug6184(in1,in2,in3,out1);
input [31:0] in1,in2,in3;
output [32:0] out1;
reg [32:0] out1;
integer [32:0] int1;
integer int2;

   always @(in1 or in2 or in3)
   begin
     int1 = in1 + in2 + in3;
      int2  = int1 >> 1;
      
      out1 = int2 + 1;
  end
endmodule
  

