// Test enabled drivers of various sizes.
module top(io, io1, io2, i, i1, i2, en);
inout [62:0] io;
inout [63:0] io1;
inout [64:0] io2;
input [62:0] i;
input [63:0] i1;
inout [64:0] i2;
input en;

assign io = en ? i : 63'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign io1 = en ? i1 : 64'b0zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign io1 = en ? i1 : 64'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign io2 = en ? i2 : 65'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
endmodule
