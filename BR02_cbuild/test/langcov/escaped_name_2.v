// last mod: Wed Dec  7 13:20:23 2005
// filename: test/langcov/escaped_name_2.v
// Description:  This test has an escaped name, it is similar to escaped_name_1
// and we run it with -Wc -g because those switches were requied for
// escaped_name_1 to cause a problem.

// gold file from aldec, main.v required minor editing because of escaped name

module escaped_name_2(\1clock1 , in1, in2, out1 );
   input \1clock1 ;
   input [7:0] in1, in2;
   output [7:0] out1 ;
   reg [7:0] out1 ;

   always @(posedge \1clock1 )
     begin: main
       out1  = in1 & in2;
     end
endmodule
