/* Test named blocks. */
module top(o, a, b, sel, c, d, clk, rst);
output [2:1] o;
input [0:1] a, b, c, d;
input sel, clk, rst;

foo foo(o, a, b, sel, c, d, clk, rst);

endmodule


module foo(o, a, b, sel, c, d, clk, rst);
output [2:1] o;
reg [2:1] o;
input [0:1] a, b, c, d;
input sel, clk, rst;

always @(posedge clk)
begin : named1
if (!rst)
begin
  o = a + b;
end
else
begin : named2
  integer temp;
  if (sel)
    temp = c;
  else
  begin : named3
    temp = d + 3;
  end
  o = temp;
end
end

endmodule
