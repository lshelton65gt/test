/* Test unary operators. */
module top(plus, minus, lognot, bitneg,
	   redand, rednand, redor, rednor, redxor, redxnor,
           i1
          );
output [0:9] plus, minus, lognot, bitneg;
output redand, rednand, redor, rednor, redxor, redxnor;
input [0:10] i1;

assign plus = +i1;
assign minus = -i1;
assign lognot = !i1;
assign bitneg = ~i1;
assign redand = &i1;
assign rednand = ~&i1;
assign redor = |i1;
assign rednor = ~|i1;
assign redxor = ^i1;
assign redxnor = ~^i1;

endmodule
