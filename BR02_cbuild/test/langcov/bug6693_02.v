// September 2008
// This file tests use of specparam.
// This file is based on the customer test case.
module a(clk, out1);
   specparam t1 = 1000;
   input   clk;
   output  out1;
   reg 	   out1; 
   real    stime;

   initial out1 = 1'b0;

   always @(posedge clk)
    if ((clk === 1'b1) && ($time - stime >= t1)) begin
       out1  <=      1'b1;   stime   <=      $time;
    end
endmodule
