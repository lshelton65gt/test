// last mod: Mon Feb 13 18:30:14 2006
// filename: test/langcov/bitsel_1.v
// Description:  This test checks for the proper implementation of bitselects of scalars
// both in range and out of range, as well as the proper reporting of any
// out-of-range situations
// WE EXPECT WARNINGS TO BE PRINTED

module bitsel_1(input clock,
		input [7:0] in1,
		input [1:0] in2,
		output reg Ra,Rb,Rc,Rd,Re,Rf,Rg,Rh,Ri,Rj,Rk,
		output Wa,Wb);

   wire 	       w;
   reg 		       r;

   assign 	       w = in1[0];

`ifdef CARBON   
   assign 	       Wa[0] = r; // in range, bitsel of scalar wire on LHS
   assign 	       Wb[1] = r; // out of range, bitsel of scalar wire on LHS
`else
   // the following should be equivalent to what carbon does for the above assign statements
   assign 	       Wa = r;
   assign 	       Wb = 1'bx;
`endif

   always @(posedge clock)
     begin: main
	r = in1[1];
`ifdef CARBON
	Ra = w;			// valid
	Rb = r;			// valid
	Rc = w[0];		// in range, bitsel of scalar wire
	Rd = r[0];		// in range, bitsel of scalar reg
	Re = w[1];		// out of range, bitsel of scalar wire
	Rf = r[1];		// out of range, bitsel of scalar reg
	Rg = w[in2];		// possible out of range, bitsel of scalar wire
	Rh = r[in2];		// possible out of range, bitsel of scalar reg
	Ri[0] = r;		// in range, bitsel of reg on LHS
	Rj[1] = r;		// out of range, bitsel of reg on LHS
	Rk[in2] = r;		// possible out of range, bitsel of reg on LHS
`else
	// the following should be equivalent to what carbon does for the above assignments
	Ra = w;
	Rb = r;
	Rc = w;
	Rd = r;
	Re = 1'bx;
	Rf = 1'bx;
	Rg = in2 ? 1'bx : w;
	Rh = in2 ? 1'bx : r;
	Ri = r;
	Rj = 1'bx;
	if ( in2 )
	  Rk = 1'bx;
	else
	  Rk = r;

`endif	
     end
endmodule
