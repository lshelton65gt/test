// last mod: Mon Nov 28 13:06:13 2005
// filename: test/langcov/Timescale/ts3.v
// Description:  This test has not timescale directive, and uses a submodule
// with a resetall at the top.  No warning about missing timescale should be
// created



module ts3(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   sub_r_m u1(clock, in1, in2, out1);
endmodule

