// last mod: Mon Nov 28 13:08:03 2005
// filename: test/langcov/Timescale/neg_ts2.v
// Description:  This test has no timescale at top, but uses sub module with
// timescale directive at top.  This should generate a warning


module neg_ts2(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   sub_t_m u1(clock, in1, in2, out1);

endmodule
