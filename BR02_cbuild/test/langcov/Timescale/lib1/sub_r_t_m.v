// last mod: Mon Nov 28 13:23:37 2005
// filename: test/langcov/Timescale/lib1/sub_m_r.v
// Description:  This file is used as a sub module for tests one level up.
// this file has a resetall, followed by a timescale, followed by a module sub_r_t_m


`resetall
`timescale 1ns/1ps
  
module sub_r_t_m(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule

