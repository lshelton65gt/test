// last mod: Mon Nov 28 13:23:22 2005
// filename: test/langcov/Timescale/lib1/sub_m_r.v
// Description:  This file is used as a sub module for tests one level up.
// this file has module sub_m_r defined followed by a resetall


module sub_m_r(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule

`resetall

