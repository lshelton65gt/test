// last mod: Mon Nov 28 13:24:23 2005
// filename: test/langcov/Timescale/lib1/sub_t_m.v
// Description:  This file is used as a sub module for tests one level up.
// this file has a timescale, followed by a module sub_t_m


`timescale 1ns/1ps
  
module sub_t_m(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule

