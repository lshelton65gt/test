// last mod: Mon Nov 28 13:22:23 2005
// filename: test/langcov/Timescale/lib1/sub_r_m.v
// Description:  This test is used as a sub module for designs one level up, it
// has a resetall followed by the module sub_r_m

`resetall
  
module sub_r_m(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule
