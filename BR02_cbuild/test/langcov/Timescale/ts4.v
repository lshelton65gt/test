// last mod: Mon Nov 28 13:25:13 2005
// filename: test/langcov/Timescale/ts4.v
// Description:  This test has timescale directive at top, and uses a submodule
// with a resetall, then timescale at the top.  No warning about missing timescale should be
// created


`timescale 1ns/1ps

module ts4(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   sub_r_t_m u1(clock, in1, in2, out1);
endmodule

