// last mod: Wed Feb 22 23:17:23 2006
// filename: test/langcov/Timescale/bug5664.v
// Description:  This is the test from bug 5664


`define TIMESCALE1 1ns/10fs
`timescale `TIMESCALE1

 module bug5664(out, in);
   output out;
   input in;
   assign out=in;
 endmodule
