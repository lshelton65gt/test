// last mod: Mon Nov 28 12:51:54 2005
// filename: test/langcov/Timescale/neg_ts1.v
// Description:  This test should report a warning about a missing timescale
// directive. It has a timescale at the top and uses a
// module from a file that has resetall at the top of that file

`timescale 1ns/1ps

module neg_ts1(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   sub_r_m u1(clock, in1, in2, out1);
endmodule

