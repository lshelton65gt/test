// last mod: Mon Nov 28 12:48:55 2005
// filename: test/langcov/Timescale/ts1.v
// Description:  This test should pass, it has a timescale at the top and uses a
// module that has a resetall at the end of the file with the resetall

`timescale 1ns/1ps

module ts1(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   sub_m_r u1(clock, in1, in2, out1);
endmodule

