// last mod: Mon Nov 28 13:04:11 2005
// filename: test/langcov/Timescale/ts2.v
// Description:  This test has no timescale directive at the top, but is
// otherwise like ts1.v.  No warning about missing timescale directives should
// be generated.

module ts2(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   sub_m_r u1(clock, in1, in2, out1);
endmodule

