/* Test whole-identifier code. */
module top(i1, i2, i3, sel, data,
           o1, o2, o3, o4, o5
          );
input sel, data;
output [3:0] o1, o2;
output [2:2] o3;
output [1:1] o4;
output [3:0] o5;
input [0:3] i1, i2;
input [1:0] i3;

foo inst1 (o1[3:0], i1[0:3], i2);
foo inst2 ({o2}, i1, {i2[0:3]});
bar inst3 (o3[2:2], i1[0:0], i2[1]);
foo inst4 (o5, {2{i3}}, {1{i2}});

assign o4[1:1] = sel ? data : 1'bz;

endmodule

module foo(o, i1, i2);
output [0:3] o;
input [3:0] i1, i2;
assign o = i1 & i2;
endmodule

module bar(o, i1, i2);
output o;
input i1,i2;
assign o = i1 | i2;
endmodule

