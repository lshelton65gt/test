/* This is an excerpt of the proto.  Handle multiple drivers through
 * bit selections through hierarchy.
 */

// The 32-bit mux implemented in this file is intended to provide 
// hierarchy to the design in a purely combinational fashion.

// 32-bit mux implemented as 2 16-bit slices
module mux32(in1, in2, sel, out);
   input [31:0] in1, in2;
   input 	sel;
   output [31:0] out;

   mux16 muxa (in1[15: 0], in2[15: 0], sel, out[15: 0]);  
   mux16 muxb (in1[31:16], in2[31:16], sel, out[31:16]);

endmodule

// 16-bit mux implemented as 4 4-bit slices
module mux16(in1, in2, sel, out);
  input [15:0] in1, in2;
  input sel;
  output [15:0] out;

  mux4 muxa (in1[ 3: 0], in2[ 3: 0], sel, out[ 3: 0]);  
  mux4 muxb (in1[ 7: 4], in2[ 7: 4], sel, out[ 7: 4]);
  mux4 muxc (in1[11: 8], in2[11: 8], sel, out[11: 8]);
  mux4 muxd (in1[15:12], in2[15:12], sel, out[15:12]);
endmodule

// 4-bit mux implemented as 4 1-bit slices
module mux4(in1, in2, sel, out);
  input [3:0] in1, in2;
  input sel;
  output [3:0] out;

  mux1 muxa (.in1(in1[0]), .in2(in2[0]), .sel(sel), .out(out[0]));  
  mux1 muxb (.in1(in1[1]), .in2(in2[1]), .sel(sel), .out(out[1]));  
  mux1 muxc (.in1(in1[2]), .in2(in2[2]), .sel(sel), .out(out[2]));  
  mux1 muxd (.in1(in1[3]), .in2(in2[3]), .sel(sel), .out(out[3]));  
endmodule

// 1-bit mux implemented with gates
module mux1 (sel, in1, in2, out);
  input in1, in2, sel;
  output out;

  not (selb, sel);
  and (temp1, selb, in1);
  and (temp2,  sel, in2);
  or  (out, temp1, temp2);

endmodule
