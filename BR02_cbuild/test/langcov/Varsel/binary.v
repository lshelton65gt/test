/*
 * Related to bug 7901.
 * Checking that "dirty" bits don't make their way
 * into the final result after the shift right.
 */
module binary(in1, in2, in3,Sout_y);
   input [8:0] in1;
   input [8:0] in2;
   input [2:0] in3;
   output [7:0] Sout_y;
   
   assign 	Sout_y = ((in1 + in2) >> in3); 

endmodule // binary
