// last mod: Tue Mar  7 08:37:21 2006
// filename: test/langcov/Varsel/foo.v
// Description:  This test is a check for correct indexed partselect on LHS,
// since sel is only 7 bits long it has a range of 0 to 127, so the assignment
// to out[sel +: 128] is always partially in range. So some bits must always be
// assigned.
// currently 03/07/06 carbon (bug5733), aldec, nc all get this wrong.

module varsel5( out, sel, in1 );
   input [6:0] 	sel;
   input [31:0] in1;
   output [0:127] out;
   reg [0:127] 	out;

   initial
      begin
	 out=0;
	 // print a header to help in the debuging of output
	 $display("s  in1");
	 $display("e           00001111222233334444555566667777");
	 $display("l           048A048A048A048A048A048A048A048A");
      end
   always @(sel or in1)
     begin
        out[sel +: 128] = in1;
     end
endmodule
