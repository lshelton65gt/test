// testcase to see how part select assignments should work

// this test is setup so that out1 and out2 should be identical after each
// simulation cycle.  gold file is manually generated



module vector ( Isel, sel, out1, out2 );
   input [1:0] 	  Isel;
   output [0:63]  out1, out2;
   reg [0:63] 	  out1, out2;
   output [5:0]   sel;
   reg [5:0] 	  sel;
   reg [31:0] 	  in1;

   initial
     begin
	out1 = 0;
	out2 = 0;
	in1 = 32'hBFFFFFFD;
     end

   
   always @(Isel or in1)
     begin
	out1 = 0;
	out2 = 0;
	sel = ((Isel == 2'b00) ? 0 : (Isel == 2'b01) ? 2 : (Isel == 2'b10) ? 32 : 1);
	
	if ( sel == 0 )
	  begin
             out1[sel +: 64] = in1;
             out2[ 0 : 63] = in1;
	  end
	else if ( sel == 2 )
	  begin
             out1[sel +: 62] = in1;
	     out2[ 2 : 63] = in1;
	  end
	else if ( sel == 32 )
	  begin
             out1[sel +: 32] = in1;
	     out2[ 32 : 63] = in1;
	  end
	else
	  begin
             out1 = 64'b1;
	     out2 = 64'b1;
	  end
     end
endmodule
