// testcase to see how part select assignments should work

// aldec and nc get this wrong
// this test is setup so that out1 and out2 should be identical after each
// simulation cycle.  gold file is manually generated


module vector ( Isel, sel, out1, out2 );
   input [1:0] 	  Isel;
   output [0:127] out1, out2;
   reg [0:127] 	  out1, out2;
   reg [31:0] 	  in1;
   output [6:0] 	  sel;
   reg [6:0] 	  sel;
   initial
     begin
	out1 = 0;
	out2 = 0;
	in1 = 32'hBFFFFFFD;
     end

   
   always @ (Isel or in1)
     begin
	out1 = 0;
	out2 = 0;
	sel = ((Isel == 2'b00) ? 0 : (Isel == 2'b01) ? 2 : (Isel == 2'b10) ? 32 : 1);
	
	if ( sel == 0 )
	  begin
             out1[sel +: 128] = in1; // should be 128'hBFFFFFFD;
             out2[ 0 : 127] = in1;   // out2 should match out1
	  end
	else if ( sel == 2 )
	  begin
             out1[sel +: 124] = in1; // should be 124'hBFFFFFFD;
	     out2[ 2 : 127] = in1;   // out2 should match out1
	  end
	else if ( sel == 32 )
	  begin
             out1[sel +: 96] = in1; // should be 96'h3FFFFFFD;
	     out2[ 32 : 127] = in1;   // out2 should match out1
	  end
	else
	  begin
	     out1 = 128'b0;
	     out2 = 128'b0;
	  end
     end
endmodule
