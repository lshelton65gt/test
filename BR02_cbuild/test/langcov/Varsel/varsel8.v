// testcase to see how part select assignments should work

// aldec gets this wrong, and so does nc

module vector ( Isel, sel, out1, out2 );
   input [1:0] 	  Isel;
   output [0:127] out1, out2;
   reg [0:127] 	out1, out2;
   output [6:0]   sel;
   reg [6:0] 	sel;
   reg [31:0] in1;

   initial
      begin
	 out1 = 0;
	 out2 = 0;
	 in1 = 32'hBFFFFFFD;
      end

      
   always @(Isel or in1)
     begin
	out1 = 0;
	out2 = 0;
	sel = ((Isel == 2'b00) ? 0 : (Isel == 2'b01) ? 2 : (Isel == 2'b10) ? 32 : 1);

	if ( sel == 0 )
	  begin
          out1[sel +: 128] = in1; // should write bits 0:127 with {96'b0,in1}
          out2[ 0 : 127] = in1;
	  end
	else if ( sel == 2 )
	  begin
             out1[sel +: 128] = in1; // should write bits 2:127 with {96'b0,in1}[127:2]
	     out2[ 2 : 127] = in1[31:2];
	  end
	else if ( sel == 32 )
	   begin
              out1[sel +: 128] = in1; // should write bits 32:127 with {96'b0,in1}[127:32]
	      out2[ 32 : 127] = 0;
	   end
	else
	  begin
	     out1 = 128'bx;
	     out2 = 128'bx;
	  end
     end
endmodule
