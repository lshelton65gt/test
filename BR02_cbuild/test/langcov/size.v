/* Test some sizing.  By no means is this exhaustive. */
module top (i1, i2, i3, i4, o1, o2, sel, o3, o4, o5, o6, o7, o8);
input [0:2] i1, i2;
input [0:1] i3;
input i4;
input sel;
output [0:3] o1;
output [0:4] o2;
output o3;
output [0:1] o4;
output [0:4] o5;
output [0:4] o6;
output [0:2] o7;
output o8;

assign o1 = (i1 & i2) | (i3 + i4);
assign o2 = sel ? i1 : i3;
assign o3 = (i1 == 3'b011);
assign o4 = (i3 == 'b01100);
assign o5 = ((i3 + i1) | (i1 == (i2+i3+(-1))));
assign o6 = ((i3 + i1) | (i1 ^ (i2+i3+1)));
assign o7 = i3 + i1 + (4'b0101 - 4'b0100);
assign o8 = (i3 == ('b01000 + 3'b001));

endmodule
