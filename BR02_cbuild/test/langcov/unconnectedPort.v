module top(clk);
input clk;

//Unconnected d AND q
flop u0(.d(), .clk(clk) );
endmodule

module flop(d,clk,q);
input d,clk;
output q;
reg q;

always @(posedge clk)
  q <= d;

endmodule
