module top(i1, i2, i3, i4, i8, i9_1, i9_2, i9_3,
	   sel, sel1, sel2, clk, rst,
	   o1, o2, o3, o4, o5, o6, o7, o8, o9, o9_3, o10, o11);
   input i1, i2, i4, i8, i9_1, i9_2, i9_3;
   input rst, sel, clk;
   input sel1, sel2;
   input [2:1] i3;
   wire  w1;
   output o1, o2, o4, o5, o6, o7, o8, o9, o9_3;
   output [1:0] o3;
   reg [1:0] 	o3;
   output [1:0] o10;
   reg [1:0] 	o10;
   output 	o11;
   reg 		o11;
   assign w1 = i1;
   assign o1 = w1;

   reg 	  w2;
   always @(i2)
     begin
	if (sel)
	  w2 = i2;
     end
   assign o2 = w2;

   wire   clk1;
   assign clk1 = clk;

   reg [2:1] w3;

   always @(posedge clk1)
     w3 = i3;

   // Should not alias because different ranges
   always @(w3)
     o3 = w3;

   // Aliasing with dead assign fixup
   reg 	     w4;
   reg 	     w5;
   always @(posedge clk1)
     begin
	if (~sel)
	  begin
	     w4 = i4 | i3;
	     w5 = w4;
	     w4 = i4 & i3;
	  end
     end
   assign o4 = w4;
   assign o5 = w5;


   // Test aliasing with multiple drivers (reset, clk)
   reg 	  w6;
   always @(posedge clk or negedge rst)
     begin
	if (~rst)
	  w6 = 1'b0;
	else
	  w6 = i4;
     end
   assign o6 = w6;

   // Make sure don't alias something with multiple drivers
   wire   w7_1 = sel1 ? i1 : 1'bz;
   wire   w7_2 = (sel2&!sel1) ? i2 : 1'bz;
   assign o7 = w7_1;
   assign o7 = w7_2;

   // Don't alias inputs and outputs directly, yet
   assign o8 = i8;

   // Test instantiations
   wire   w_o9;
   wire   w_i9_2;
   wire   w_i9_3;
   assign o9 = w_o9;
   bar inst1 (i9_1, w_i9_2, w_i9_3, w_o9);
   assign w_i9_2 = i9_2;
   assign w_i9_3 = i9_3;
   assign o9_3 = w_i9_3 & i1;

   // Test always blocks
   reg [1:0]	  w10;
   reg [1:0] 	  tmp10; 	  
   always @(sel or i3 or i1 or i2)
     begin
	if (sel)
	  tmp10 = i3;
	else
	  tmp10 = i3 ^ {i1,i2};
	w10 = tmp10;
	o10[1] = w10[1];
     end
   always
     o10[0] = w10[0];

   wire w11 = i1 & i2;
   always @(w11)
     begin
	o11 = w11;
     end
endmodule // top


module bar(i1, i2, i3, o1);
   input i1, i2, i3;
   output o1;
   wire   w1;
   assign w1 = i1 & i2;
   assign o1 = w1;
endmodule // bar
