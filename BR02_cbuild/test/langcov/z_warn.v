/* Test warnings of z/non-z drivers. */
module top(n1, n2, n3, n4, a1, a2, e1, e2, bid);
input a1, a2, e1, e2;
inout bid;
output n1, n2, n3, n4;

  // Simple case
  bufif1(n1, a1, e1);
  buf(n2, n1);
  bufif1(n2, a2, e2);

  // Test bidis
  foo inst1(bid, a1, e1, a2);

  // baz is a non-z driver, can only detect through elaboration
  bufif0(n3, a1, e1);
  bar inst2(n3, a2, e2);
  baz inst3(n3, a2, e2);

  // test inputs, can only detect through elaboration
  fab inst4(n1, n4);
  fab inst5(a1, n4);
  bar inst6(n4, a2, e2);
endmodule


module foo(bidi, a, e, b);
inout bidi;
input a, e, b;
assign bidi = a ? e : 1'bz;
assign bidi = b;
endmodule


module bar(o, i, e);
output o;
input i, e;
assign o = e ? i : 1'bz;
endmodule


module baz(o, i, e);
output o;
input i, e;
assign o = e ? i : 1'b0;
endmodule


module fab(i, o);
input i;
output o;
assign o = i;
endmodule
