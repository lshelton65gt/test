// last mod: Mon Feb  7 17:22:37 2005
// filename: test/langcov/Display/const_1.v
// Description:  This test uses a signed large number


module const_1(clock, reset, out);
   input clock;
   input reset;
   output [63:0] out;
   reg [63:0] out;


   always @(posedge clock)
     begin: main
	out = -127;
     end
endmodule
