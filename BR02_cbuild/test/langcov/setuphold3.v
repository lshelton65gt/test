// to check handling of some variants $setup and $hold sys calls
// bug5306
module foo(clk, in, out);
   input clk, in;
   output out;
   reg 	  out;

   reg 	  notifier;
   wire   in_n;
   wire   clk_d, in_d;

   not n0(in_n, in);

   always @(posedge clk) begin
      //$display("CLOCK");
      out <= in_n;
   end

   specify


      specparam tSetup = 10,
      tHold = 15;
      
      $setup(in_n, posedge clk, tSetup);
      $hold(in_n, posedge clk, tHold);
      $setuphold(posedge clk, in_n, 1, 1);
      $recovery(posedge clk, posedge in, 1);
//      $removal (posedge clk,posedge clk &&& (in_n == 1'b1),0:0:0,notifier);
      $recrem(posedge clk, posedge in, 1, 2);

      $skew(posedge clk, clk_d, 0.1);
//      $timeskew(posedge clk, posedge clk_d, 0.1);
//      $fullskew(posedge clk, negedge in, 0.1, 0.1, notifier);
      $width( posedge clk,0.4219,0,notifier);
      $period(posedge clk, 10);
      $nochange(posedge clk, negedge clk_d, 0.1, 0.1);

   endspecify
endmodule // foo
