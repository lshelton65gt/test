module top ( out, in, clk );
   input clk;
   input in;
   output [2:0] out;
   reg [2:0] 	out;

   reg 		save;
   
   integer 	i;

   
   always @(posedge clk)
     begin
	for ( i = 0 ; i <= 2 ; i = i + 1 )
	  begin :forblk
	     reg sel;
	     sel = i[0] ^ save;
	     if ( sel )
	       begin
		  out[i] <= in;
		  save = out[i];
	       end
	  end
     end
endmodule // top
