// last mod: Thu May 26 08:42:50 2005
// filename: test/langcov/bug1697_3.v
// These simulation results were checked by nc, aldec does not get this right.
// 
// In this testcase we are checking to see that the correct size check is done
// for the two sides of the implict connection.  If the event and delayed signal
// are not of the same size then the connection should not be created, and a
// note is printed reporting the mismatch.
// see also test/langcov/bug1697_*.v
// NOTE: this test is designed to report a simulation mismatch if a connection
// is incorrectly created, and thus creating multiple drivers for a single
// output. In cbuild if there are multiple drivers we currently use the original
// line numbers to select the wining driver.  Thus the order of statements is
// important in this testcase.  To protect this testcase from future changes in
// this resolution function, each situation that we want to test for the
// non-creation of a implicit connection has two variants, one where the
// to-be-created implicit connection comes first, and a second where the
// not-to-be-connected implicit connections comes first. This way this testcase
// always contains a test that will check for any ordering of the resolution function.

module bug1697_3(clk, in, out_A_1, out_A_2, out_B_1, out_B_2);
   input [2:0] clk;
   input [4:0] in;
   output [4:0] out_A_1, out_A_2;
   output [4:0] out_B_1, out_B_2;

   wire 	clk_d;
   wire 	clk_d_unused;
   wire  [4:3] in_A_1_d, in_A_2_d;
   reg 	 [4:0] out_A_1, out_A_2;

   wire 	clk_B_1_d;
   wire 	clk_B_2_d;
   reg 	 [4:0] out_B_1, out_B_2;
   wire  [4:3] in_B_1_d, in_B_2_d;

   always @(posedge clk_d)
     out_A_1 <= in_A_1_d;

   always @(posedge clk_d)
     out_A_2 <= in_A_2_d;

   specify
      // TEST_A check that an implicit connection is not created when the data
      // event and the delayed data are of different sizes
      // test 1, first we specify the $setuphold with the implicit
      // connection that will NOT be created, then the $setuphold with the
      // implicit connection that will be created.
      
      // in these first two $setuphold stmts the size of the data and delay data
      // are different so no connection is made, the size of the clock and delay
      // clock are the same but the delayed clock is unused in this design
      $setuphold(posedge clk[2], posedge in[2:0], 1, 1, , , , clk_d_unused, in_A_1_d);
      $setuphold(posedge clk[2], negedge in[2:0], 1, 1, , , , clk_d_unused, in_A_1_d);

      // in these next two $setuphold stmts the sizes match for the data and
      // delay data so a connection is made, note it is to the same delay data
      // (in_A_1_d) as used in the previous two $setuphold so there would be a
      // multiple driver msg generated if the previous lines had incorrectly
      // created a connection.
      $setuphold(posedge clk[0], posedge in[4:3], 1, 1, , , , clk_d, in_A_1_d);
      $setuphold(posedge clk[0], negedge in[4:3], 1, 1, , , , clk_d, in_A_1_d);


      // test 2, first we specify the $setuphold with the implicit
      // connection that will be created, then the $setuphold with the
      // implicit connection that will NOT be created.

      // in these next two $setuphold stmts the sizes match for the data and
      // delay data so a connection is made, note it is to the same delay data
      // (in_A_2_d) as used in the following two $setuphold so there will be a
      // multiple driver msg generated if these two lines had incorrectly
      // created a connection.
      $setuphold(posedge clk[0], posedge in[4:3], 1, 1, , , , clk_d, in_A_2_d);
      $setuphold(posedge clk[0], negedge in[4:3], 1, 1, , , , clk_d, in_A_2_d);
      // in these two $setuphold stmts the size of the data and delay data
      // are different so no connection is made, the size of the clock and delay
      // clock are the same but the delayed clock is unused in this design
      $setuphold(posedge clk[2], posedge in[2:0], 1, 1, , , , clk_d_unused, in_A_2_d);
      $setuphold(posedge clk[2], negedge in[2:0], 1, 1, , , , clk_d_unused, in_A_2_d);

   endspecify




   // TEST_B check that an implicit connection is not created when the clock
   // event and the delayed clock are of different sizes
   always @(posedge clk_B_1_d)
     out_B_1 <= in_B_1_d;

   always @(posedge clk_B_2_d)
     out_B_2 <= in_B_2_d;

   specify
      // test 1, first we specify the $setuphold with the implicit
      // connection that will NOT be created, then the $setuphold with the
      // implicit connection that will be created.
      
      // in these first two $setuphold stmts the size of the clock and delay clock
      // are different so no connection is made, the size of the data and delay
      // data are the same
      $setuphold(posedge clk[2:1], posedge in[4:3], 1, 1, , , , clk_B_1_d, in_B_1_d);
      $setuphold(posedge clk[2:1], negedge in[4:3], 1, 1, , , , clk_B_1_d, in_B_1_d);

      // in these next two $setuphold stmts the sizes match for the clock and
      // delay clock so a connection is made, note it is to the same delay clock
      // (clk_B_1_d) as used in the previous two $setuphold so there would be a
      // multiple driver msg generated if the previous lines had incorrectly
      // created a connection.
      $setuphold(posedge clk[0], posedge in[4:3], 1, 1, , , , clk_B_1_d, in_B_1_d);
      $setuphold(posedge clk[0], negedge in[4:3], 1, 1, , , , clk_B_1_d, in_B_1_d);


      // test 2, first we specify the $setuphold with the implicit
      // connection that will be created, then the $setuphold with the
      // implicit connection that will NOT be created.

      // in these next two $setuphold stmts the sizes match for the clock and
      // delay clock so a connection is made, note it is to the same delay clock
      // (clk_B_2_d) as used in the next two $setuphold so there would be a
      // multiple driver msg generated if the next two lines incorrectly
      // create a connection.
      $setuphold(posedge clk[0], posedge in[4:3], 1, 1, , , , clk_B_2_d, in_B_2_d);
      $setuphold(posedge clk[0], negedge in[4:3], 1, 1, , , , clk_B_2_d, in_B_2_d);
      // in these two $setuphold stmts the size of the clock and delay clock
      // are different so no connection is made, the size of the data and delay
      // data are the same
      $setuphold(posedge clk[2:1], posedge in[4:3], 1, 1, , , , clk_B_2_d, in_B_2_d);
      $setuphold(posedge clk[2:1], negedge in[4:3], 1, 1, , , , clk_B_2_d, in_B_2_d);

   endspecify
   


   
endmodule
