// last mod: Thu Sep  8 17:54:08 2005
// filename: test/langcov/defparam1.v
// see bug 4976
// Description:  This test duplicates a problem seen at skyworks where a
// defparam is used to define the filenames used by lower level readmemh.
// It turns out that the problem is related to defparams not readmemh and
// this test has been converted to use $display to demonstrate the problem.
// It appears that the problem is that the module bottom is not being uniquified
// as it should be because the module mid is not seen as unique.  If you force
// the mid module to be unique (by uncommenting the defparams for i1.mid_fn,
// i2.mid_fn and i3.mid_fn, then cbuild will correctly uniquify the bottom
// modules.



module defparam1(clock, rw, addr, dat, out1, out2, out3);
   input clock, rw;
   input [3:0] addr;
   input [7:0] dat;
   output [7:0] out1, out2, out3;

   defparam 	i1.b1.bottom_fn = "bottom1";
   defparam 	i2.b1.bottom_fn = "bottom2";
   defparam 	i3.b1.bottom_fn = "bottom3";

//   defparam 	i1.mid_fn = "mid1";
//   defparam 	i2.mid_fn = "mid2";
//   defparam 	i3.mid_fn = "mid3";

   mid i1(clock, rw, addr, dat, out1);
   mid i2(clock, rw, addr, dat, out2);
   mid i3(clock, rw, addr, dat, out3);

endmodule

module mid(clock, rw, addr, dat, out1);
   input clock, rw;
   input [3:0] addr;
   input [7:0] dat;
   output [7:0] out1;

   parameter [127:0] mid_fn = "mem0.dat";
   
   initial $display ("mid_fn is: %s", mid_fn);

   bottom b1( clock, rw, addr, dat, out1);

endmodule

module bottom(clock, rw, addr, dat, out1);
   input clock, rw;
   input [3:0] addr;
   input [7:0] dat;
   output [7:0] out1;
   reg [7:0] out1;
   reg [7:0] mem [0:15];

   parameter [127:0] bottom_fn = "bottom0";

   initial
     begin
	$display("bottom_fn is: %s", bottom_fn);
     end
   
   always @(posedge clock)
     begin
	if (rw)
	  mem[addr] = dat;
	else
	  out1 = mem[addr];
     end
endmodule
