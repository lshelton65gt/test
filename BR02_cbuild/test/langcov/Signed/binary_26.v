// last mod: Wed Jun 22 22:37:51 2005
// filename: test/langcov/Signed/binary_26.v
// Description:  This test is like binary25 but the operands are in the largest
// to smallest 
// gold file checked with nc, (aldec gets some outputs wrong)

`define OP1 +
`define OP2 -
`define SIZE1 4
`define SIZE2 3
`define SIZE3 2
`define SIZE4 10

`include "binary.v"
