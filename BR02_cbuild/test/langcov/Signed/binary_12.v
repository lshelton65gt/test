// last mod: Wed Jun 22 11:11:01 2005
// filename: test/langcov/Signed/binary_12.v
// Description:  This test checks signed and unsigned operands of ==
// binary_12.sim.log.gold checked with NC

`define OP1 ==
`define OP2 ==
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"
