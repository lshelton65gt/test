// last mod: Wed Jun 22 11:18:13 2005
// filename: test/langcov/Signed/binary_23.v
// Description:  This test checks signed and unsigned operands of >>>
// binary_23.sim.log.gold checked with finsim - aldec and NC both
// give bogus results for:
// 	 out10 = 3'sd4 >>> 0   => carbon, finsim say 10'b1111111100
//				  aldec, nc say      10'b0000000100

`define OP1 >>>
`define OP2 >>>
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"
