// last mod: Fri Feb 17 08:41:04 2006
// filename: test/langcov/Signed/fold_1.v
// Description:  This test was inspired by the fix for binary_F_K_10_1_1_1.v
// here we try experiments with different types of single bit signed operands to
// the && operator.  Several of these were broken as of 1.4257


module fold_1(
	      input  isA, isB, isC,       // input, scalar
	      input  [2:0] ivD, ivE, ivF, // input, vector
	      output reg signed [4:0] Sout_a, Sout_b, Sout_c, Sout_d, Sout_e, Sout_f );

   reg signed  SsB, SsC;	// signed scalar reg
   reg signed [2:0] SvE, SvF;	// signed vector reg

   
   always @(*)
     begin: main
	SsB = isB;
	SsC = isC;
	Sout_a = $signed(isA) && $signed(isB);
	Sout_b = $unsigned(isB) && $unsigned(isC);
	Sout_c = $signed(isB!=1'b0 ) && $signed(isC!=1'b0);

        Sout_d =         SsB          &&         SsC;
	Sout_e =        (SsB != 1'b0) &&        (SsC != 1'b0);
	Sout_f = $signed(SsB != 1'b0) && $signed(SsC != 1'b0);

	SvE = ivE;
	SvF = ivF;
	Sout_a = $signed(ivD) && $signed(ivE);
	Sout_b = $unsigned(ivE) && $unsigned(ivF);
	Sout_c = $signed(ivE!=1'b0 ) && $signed(ivF!=1'b0);

        Sout_d =         SvE          &&         SvF;
	Sout_e =        (SvE != 3'b0) &&        (SvF != 3'b0);
	Sout_f = $signed(SvE != 3'b0) && $signed(SvF != 3'b0);

     end
endmodule

