// filename: test/langcov/Signed/Signed_gigantic/binary_K_M_129_65_10_10.v
// test something of the form: 
// O129 = I_65 && J_10 >> K_10
// F G H I J  K  L M  N  O  P   Q
// + * / & == && > >> << ** >>> <<<
`define OP1 &&
`define OP2 >>
`define SIZE1 65
`define SIZE2 10
`define SIZE3 10
`define SIZE4 129


`include "binary.v"
