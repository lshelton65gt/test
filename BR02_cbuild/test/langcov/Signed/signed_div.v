// This testcase yields a large number of these alerts:
// signed_arith_OP.v:109: Alert 2015: Expressions involving operator / with operand widths greater than 64 bits are only handled correctly if the righthand operand is a positive power of 2.  Otherwise, you could get an incorrect result.

// Because of this limitation, I have not event attempted to demote the
// alert and run simulation tests.  We should probably make a signed_div.v
// that restricts its right-hand operands to a power of 2.

`define OP /
`include "signed_arith_OP.v"
