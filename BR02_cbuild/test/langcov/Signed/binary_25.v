// last mod: Wed Jun 22 22:21:56 2005
// filename: test/langcov/Signed/binary_25.v
// Description:  This test does 10bits = 2bits + 3bits - 4bits
// gold file checked with nc (aldec gets some outputs wrong)

`define OP1 +
`define OP2 -
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"
