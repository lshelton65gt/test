// last mod: Wed Jun 22 11:27:53 2005
// filename: test/langcov/Signed/signed_1.v
// Description:  This test checks the $signed operator, it is setup to
// be self checking, all outputs should be 1 (verified with nc)  aldec gets some
// of this wrong


module signed_1(
		input [1:0] in2,
		input [2:0] in3,
		input [3:0] in4,
		output reg  check_a, check_b, check_c, check_d,
		            check_e, check_f, check_g, check_h,
		            check_i, check_j, check_k, check_l,
		            check_m, check_n, check_o, check_p,
		            check_q, check_r, check_s, check_t,
		            check_u, check_v, check_w);

   reg signed [1:0] S2;
   reg        [1:0] U2;
   reg signed [2:0] S3;
   reg        [2:0] U3;
   reg signed [3:0] S4;
   reg        [3:0] U4;
   reg signed [9:0] S_implicit10_a, S_implicit10_b, S_implicit10_c, S_implicit10_d,
                    S_implicit10_e, S_implicit10_f, S_implicit10_g, S_implicit10_h,
		    S_implicit10_i, S_implicit10_j, S_implicit10_k, S_implicit10_l,
		    S_implicit10_m, S_implicit10_n, S_implicit10_o, S_implicit10_p,
		    S_implicit10_q, S_implicit10_r, S_implicit10_s, S_implicit10_t,
                    S_implicit10_u, S_implicit10_v, S_implicit10_w;
   reg signed [9:0] S_explicit10_a, S_explicit10_b, S_explicit10_c, S_explicit10_d,
                    S_explicit10_e, S_explicit10_f, S_explicit10_g, S_explicit10_h,
		    S_explicit10_i, S_explicit10_j, S_explicit10_k, S_explicit10_l,
		    S_explicit10_m, S_explicit10_n, S_explicit10_o, S_explicit10_p,
		    S_explicit10_q, S_explicit10_r, S_explicit10_s, S_explicit10_t,
                    S_explicit10_u, S_explicit10_v, S_explicit10_w;

   initial
     $display("i2 i3  i4abcdefghijklmnopqrstuvw");

   always @(*)
     begin: main
	S2 = in2;
	U2 = in2;
	S3 = in3;
	U3 = in3;
	S4 = in4;
	U4 = in4;

	S_implicit10_a =  U2 + U3 + U4;
	S_explicit10_a =  $unsigned(U2) + $unsigned(U3) + $unsigned(U4);
	check_a = S_implicit10_a == S_explicit10_a;
	
	S_implicit10_b =  U2 + U3 + U4;
	S_explicit10_b =  $unsigned(U2) + $unsigned(U3) + U4;
	check_b = S_implicit10_b == S_explicit10_b;

	S_implicit10_c =  U2 + U3 + U4;
	S_explicit10_c =  $unsigned(U2) + U3 + U4;
	check_c = S_implicit10_c == S_explicit10_c;

	S_implicit10_d =  S2 + S3 + S4;
	S_explicit10_d =  $signed(S2) + $signed(S3) + $signed(S4);
	check_d = S_implicit10_d == S_explicit10_d;

	S_implicit10_e =  S2 + S3 + S4;
	S_explicit10_e =  $signed(S2) + $signed(S3) + S4;
	check_e = S_implicit10_e == S_explicit10_e;

	S_implicit10_f =  S2 + S3 + S4;
	S_explicit10_f =  $signed(S2) + S3 + S4;
	check_f = S_implicit10_f == S_explicit10_f;

	S_implicit10_g =  U2 + U3 + U4;
	S_explicit10_g = $unsigned(S2) + $unsigned(S3) + $unsigned(S4);
	check_g = S_implicit10_g == S_explicit10_g;

	S_implicit10_h =  U2 + U3 + U4;
	S_explicit10_h =  $unsigned(S2) + $unsigned(S3) + U4;
	check_h = S_implicit10_h == S_explicit10_h;

	S_implicit10_i = U2 + U3 + U4;
	S_explicit10_i = $unsigned(S2) + U3 + U4;
	check_i = S_implicit10_i == S_explicit10_i;

	S_implicit10_j = U2 + U3 + U4;
	S_explicit10_j = U2 + U3 + $unsigned(S4);
	check_j = S_implicit10_j == S_explicit10_j;

	S_implicit10_k = U2 + U3 + U4;
	S_explicit10_k = U2 + U3 + S4;
	check_k = S_implicit10_k == S_explicit10_k;

	// in this case using $signed with U4 should be a no-op, and all
	// operands should be handled as if unsigned
	S_implicit10_l =  U2 + U3 + U4;
	S_explicit10_l =  U2 + U3 + $signed(U4);
	check_l = S_implicit10_l == S_explicit10_l;

	// in this case using $signed with U3 should be a no-op, and all
	// operands should be handled as unsigned
	S_implicit10_m =  U2 + U3 + U4;
	S_explicit10_m =  $signed(U2) + U3 + $signed(U4);
	check_m = S_implicit10_m == S_explicit10_m;

	S_implicit10_n =  S2 + S3 + S4;
	S_explicit10_n = $signed(U2) + $signed(U3) + $signed(U4);
	check_n = S_implicit10_n == S_explicit10_n;

	S_implicit10_o =  S2 + S3 + S4;
	S_explicit10_o =  $signed(U2) + S3 + $signed(U4);
	check_o = S_implicit10_o == S_explicit10_o;

	S_implicit10_p =  S2 + S3 + S4;
	S_explicit10_p = $signed(U2) + S3 + S4;
	check_p = S_implicit10_p == S_explicit10_p;

	S_implicit10_q = S2 + S3 + S4;
	S_explicit10_q = S2 + S3 + $signed(U4);
	check_q = S_implicit10_q == S_explicit10_q;

	S_implicit10_r = S2 + S3 + S4;
	S_explicit10_r = S2 + S3 + $signed($unsigned($signed(U4)));
	check_r = S_implicit10_r == S_explicit10_r;

	S_implicit10_s = S2 + S3 + S4;
	S_explicit10_s = $signed($unsigned($signed(U2))) + S3 + S4;
	check_s = S_implicit10_s == S_explicit10_s;

	S_implicit10_t = S2 + S3 + S4;
	S_explicit10_t = S2 + $signed($unsigned(S3)) + S4;
	check_t = S_implicit10_t == S_explicit10_t;

	S_implicit10_u = S2 + U3 + S4;
	S_explicit10_u = S2 + $unsigned($signed($unsigned(S3))) + S4;
	check_u = S_implicit10_u == S_explicit10_u;

	S_implicit10_v = S2 + U3 + S4;
	S_explicit10_v = S2 + $unsigned($signed($unsigned(U3))) + S4;
	check_v = S_implicit10_v == S_explicit10_v;

	S_implicit10_w = S2 + (U3 + S4);
	S_explicit10_w = S2 + ($unsigned(S3) + S4);
	check_w = S_implicit10_w == S_explicit10_w;

     end
endmodule


// nc says all outputs are 1
