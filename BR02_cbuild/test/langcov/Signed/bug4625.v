// assert generating extend of bitvector.

module top(A,B);
   input signed [71:0] A;
   inout signed [90:0] B;

   assign B = A + B;
endmodule
