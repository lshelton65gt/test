// last mod: Tue Jun 14 11:36:42 2005
// filename: test/langcov/Signed/binary_1_exp.v
// Description:  This test checks the rules for automatic determination of the
// signed/unsigned property of operands of binary operators, it is actually a
// copy of binary_1.v with a parallel set of expressions where explicit
// conversion of operands (using the verilog rules) has been added, then the
// output is just the xor of the two results
// this test is similar to binary_1.v but $unsigned is added to explicitly
// convert terms to unsigned following the rules of verilog.  Simulation results
// should be identical to binary_1.v
// binary_1.sim.log.gold checked with aldec and nc
// note  this testcase is self checking, all outputs should be 1 (verified with
// nc) (aldec does not match)

module binary_1(
		input [1:0] in2,
		input [2:0] in3,
		input [3:0] in4,
		output reg  check_a, check_b, check_c, check_d,
		            check_e, check_f, check_g, check_h,
		            check_i, check_j, check_k, check_l,
		            check_m, check_n, check_o, check_p,
		            check_q, check_r, check_s, check_t,
		            check_u, check_v, check_w);

   reg signed [1:0] S2;
   reg        [1:0] U2;
   reg signed [2:0] S3;
   reg        [2:0] U3;
   reg signed [3:0] S4;
   reg        [3:0] U4;
   reg signed [9:0] S_implicit10_a, S_implicit10_b, S_implicit10_c, S_implicit10_d,
                    S_implicit10_e, S_implicit10_f, S_implicit10_g, S_implicit10_h,
		    S_implicit10_i, S_implicit10_j, S_implicit10_k, S_implicit10_l,
		    S_implicit10_m, S_implicit10_n, S_implicit10_o, S_implicit10_p,
		    S_implicit10_q, S_implicit10_r, S_implicit10_s, S_implicit10_t,
                    S_implicit10_u, S_implicit10_v, S_implicit10_w;
   reg signed [9:0] S_explicit10_a, S_explicit10_b, S_explicit10_c, S_explicit10_d,
                    S_explicit10_e, S_explicit10_f, S_explicit10_g, S_explicit10_h,
		    S_explicit10_i, S_explicit10_j, S_explicit10_k, S_explicit10_l,
		    S_explicit10_m, S_explicit10_n, S_explicit10_o, S_explicit10_p,
		    S_explicit10_q, S_explicit10_r, S_explicit10_s, S_explicit10_t,
                    S_explicit10_u, S_explicit10_v, S_explicit10_w;
   initial
     $display("i2 i3  i4abcdefghijklmnopqrstuvw");

   always @(*)
     begin: main
	S2 = in2;
	U2 = in2;
	S3 = in3;
	U3 = in3;
	S4 = in4;
	U4 = in4;

	S_implicit10_a =        S3  + U4;	// signed + unsigned
	S_explicit10_a =        $unsigned(S3)  + U4;	// signed + unsigned
	check_a = S_implicit10_a == S_explicit10_a;
	
	S_implicit10_b =        S3  + S4;	// signed + signed
	S_explicit10_b =        S3  + S4;	// signed + signed
	check_b = S_implicit10_b == S_explicit10_b;

	S_implicit10_c = (S2 +  S3) + S4;	// (signed + signed) + signed
	S_explicit10_c = (S2 +  S3) + S4;	// (signed + signed) + signed
	check_c = S_implicit10_c == S_explicit10_c;

	S_implicit10_d =  S2 + (S3  + S4);	// signed + (signed + signed)
	S_explicit10_d =  S2 + (S3  + S4);	// signed + (signed + signed)
	check_d = S_implicit10_d == S_explicit10_d;

	S_implicit10_e =  S2 +  S3  + S4;	// signed + signed + signed
	S_explicit10_e =  S2 +  S3  + S4;	// signed + signed + signed
	check_e = S_implicit10_e == S_explicit10_e;

	S_implicit10_f =  S2 + (S3  + U4);	// signed + (signed + unsigned)
	S_explicit10_f =  $unsigned(S2) + ($unsigned(S3)  + U4);	// signed + (signed + unsigned)
	check_f = S_implicit10_f == S_explicit10_f;

	S_implicit10_g = (S2 +  S3) + U4;	// (signed + signed) + unsigned
	S_explicit10_g = ($unsigned(S2) +  $unsigned(S3)) + U4;	// (signed + signed) + unsigned
	check_g = S_implicit10_g == S_explicit10_g;

	S_implicit10_h =  S2 +  S3  + U4;	// signed + signed + unsigned
	S_explicit10_h =  $unsigned(S2) +  $unsigned(S3)  + U4;	// signed + signed + unsigned
	check_h = S_implicit10_h == S_explicit10_h;

	S_implicit10_i =  U2 + (S3  + S4);	// unsigned + (signed + signed)
	S_explicit10_i =  U2 + ($unsigned(S3)  + $unsigned(S4));	// unsigned + (signed + signed)
	check_i = S_implicit10_i == S_explicit10_i;

	S_implicit10_j = (U2 +  S3) + S4;	// (unsigned + signed) + signed
	S_explicit10_j = (U2 +  $unsigned(S3)) + $unsigned(S4);	// (unsigned + signed) + signed
	check_j = S_implicit10_j == S_explicit10_j;

	S_implicit10_k =  U2 +  S3  + S4;	// unsigned + signed + signed
	S_explicit10_k =  U2 +  $unsigned(S3)  + $unsigned(S4);	// unsigned + signed + signed
	check_k = S_implicit10_k == S_explicit10_k;

	S_implicit10_l =  S2 + (U3  + S4);	// signed + (unsigned + signed)
	S_explicit10_l =  $unsigned(S2) + (U3  + $unsigned(S4));	// signed + (unsigned + signed)
	check_l = S_implicit10_l == S_explicit10_l;

	S_implicit10_m = (S2 +  U3) + S4;	// (signed + unsigned) + signed
	S_explicit10_m = ($unsigned(S2) +  U3) + $unsigned(S4);	// (signed + unsigned) + signed
	check_m = S_implicit10_m == S_explicit10_m;

	S_implicit10_n =  S2 +  U3  + S4;	// signed + unsigned + signed
	S_explicit10_n =  $unsigned(S2) +  U3  + $unsigned(S4);	// signed + unsigned + signed
	check_n = S_implicit10_n == S_explicit10_n;

	S_implicit10_o =  S2 + (U3  + U4);	// signed + (unsigned + unsigned)
	S_explicit10_o =  $unsigned(S2) + (U3  + U4);	// signed + (unsigned + unsigned)
	check_o = S_implicit10_o == S_explicit10_o;

	S_implicit10_p = (S2 +  U3) + U4;	// (signed + unsigned) + unsigned
	S_explicit10_p = ($unsigned(S2) +  U3) + U4;	// (signed + unsigned) + unsigned
	check_p = S_implicit10_p == S_explicit10_p;

	S_implicit10_q =  S2 +  U3  + U4;	// signed + unsigned + unsigned
	S_explicit10_q =  $unsigned(S2) +  U3  + U4;	// signed + unsigned + unsigned
	check_q = S_implicit10_q == S_explicit10_q;

	S_implicit10_r =  U2 + (U3  + S4);	// unsigned + (unsigned + signed)
	S_explicit10_r =  U2 + (U3  + $unsigned(S4));	// unsigned + (unsigned + signed)
	check_r = S_implicit10_r == S_explicit10_r;

	S_implicit10_s = (U2 +  U3) + S4;	// (unsigned + unsigned) + signed
	S_explicit10_s = (U2 +  U3) + $unsigned(S4);	// (unsigned + unsigned) + signed
	check_s = S_implicit10_s == S_explicit10_s;

	S_implicit10_t =  U2 +  U3  + S4;	// unsigned + unsigned + signed
	S_explicit10_t =  U2 +  U3  + $unsigned(S4);	// unsigned + unsigned + signed
	check_t = S_implicit10_t == S_explicit10_t;

	S_implicit10_u =  U2 + (U3  + U4);	// unsigned + (unsigned + unsigned)
	S_explicit10_u =  U2 + (U3  + U4);	// unsigned + (unsigned + unsigned)
	check_u = S_implicit10_u == S_explicit10_u;

	S_implicit10_v = (U2 +  U3) + U4;	// (unsigned + unsigned) + unsigned
	S_explicit10_v = (U2 +  U3) + U4;	// (unsigned + unsigned) + unsigned
	check_v = S_implicit10_v == S_explicit10_v;

	S_implicit10_w =  U2 +  U3  + U4;	// unsigned + unsigned + unsigned
	S_explicit10_w =  U2 +  U3  + U4;	// unsigned + unsigned + unsigned
	check_w = S_implicit10_w == S_explicit10_w;

     end
endmodule


// nc says all outputs are 1
