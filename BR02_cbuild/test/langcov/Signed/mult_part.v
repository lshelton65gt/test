// This testcase fails to compile, as logged in bug6805

module top(clk, in, i, j, out);
  input clk;
  input signed [529:0] in;
  input [7:0]   i, j;
  reg signed [264:0] out;
  output signed [264:0] out;

  initial out = 0;

  always @(posedge clk)
    out <= $signed(in[i +: 265]) * $signed(in[j +: 265]);
endmodule
