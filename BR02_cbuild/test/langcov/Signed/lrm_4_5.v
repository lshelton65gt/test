// last mod: Wed Jun 22 16:45:42 2005
// filename: test/langcov/Signed/lrm_4_5.v
// Description:  This test was inspired by an example in section 4.5 of lrm
// goldfile checked with aldec


module lrm_4_5(output reg [7:0] Uout1, Uout2, Uout3,
	       output reg signed [7:0] Sout1, Sout2, Sout3);

   initial
      begin
	 Uout1 = $unsigned(-4);	// lrm says this should be equivalent to: Uout1 = 4'b1100 (but cloutier thinks the LRM comment is wrong)
	 Uout2 = $unsigned(4);
	 Uout3 = 4'b1100;

	 Sout1 = $signed(4'b1100); // lrm says this should be equivalent to: Sout1 = -4;
	 Sout2 = $signed(4'b0100);
	 Sout3 = -4;
      end

endmodule
