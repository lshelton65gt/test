// last mod: Fri Jul  1 16:49:31 2005
// filename: test/langcov/Signed/size_7.v
// Description: this test is an attempt to duplicate a codegen problem seen in vhdl
// where we have the following intermediate verilog
//
// assign v_localresult = (((v_operanda  EXT  33)  EXT  66) * ((v_operandb  EXT  33)  EXT  66)); // mult9.vhdl:17
//
// currently this test has a simulation mismatch, and it appears that the
// populated verilog is incorrect, we see:
//       Sout_a = ((Sin66 + ({(Sin1  EXT  33)}  EXT  33)) * (Sin66 + ({(Sin2  EXT  33)}  EXT  33))); // size_7.v:20
// I think we should be seeing:
//       Sout_a = ((Sin66 + ({(Sin1  EXT  33)}  EXT  66)) * (Sin66 + ({(Sin2  EXT  33)}  EXT  66))); // size_7.v:20
// gold file was checked by nc and aldec.


module size_7(input signed [32:0] Sin1, Sin2,
	    output reg signed [65:0] Sout_a);


   wire signed [65:0] Sin66;

   assign      Sin66 = 0;
   
   always @(*)
     begin: main

	Sout_a = ( Sin66 + $signed({$signed(Sin1)})) * (Sin66 + $signed({$signed(Sin2)}));
// the following is another attempt at this
//	Sout_a = (  $signed($signed({$signed(Sin1)}) + Sin66)) *  ($signed($signed({$signed(Sin2)}) + Sin66));
     end
endmodule
