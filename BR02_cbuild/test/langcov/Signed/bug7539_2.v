// For a more detailed analysis of the issues tested by this bug, see bug 8072.
// Also, bug7539.v does the same things but with sized constants
module bug7539 (clk, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10);
   input clk;
   output [64:0] x1;
   output [64:0] x2;
   output [64:0] x3;
   output [8:0] x4;
   output [8:0] x5;
   output [7:0] x6;
   output [7:0] x7;
   output [31:0] x8;
   output [31:0] x9;
   output [31:0] x10;
   reg signed [64:0] x1;
   reg signed [64:0] x2;
   reg        [64:0] x3;
   reg signed [8:0] x4;
   reg        [8:0] x5;
   reg signed [7:0] x6;
   reg        [7:0] x7;
   reg signed [31:0] x8;
   reg        [31:0] x9;
   reg signed [31:0] x10;
   always @(posedge clk)
     begin
       x1  = 'bz;
       x2  = 'sb10z;
       x3  = 'b10z;
       x4  = 'b10z;
       x5  = 'b10z;
       x6  = 'b10z;
       x7  = 'b10z;
       x8  = 'bz;
       x9  = 'sbz;
       x10 = 'sbz;
     end
endmodule
