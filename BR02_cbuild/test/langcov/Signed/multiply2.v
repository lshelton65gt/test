// This testcase is similar to multiply.v, but I didn't observe that
// it was broken.  It doesn't have constants, and bug6795 was in the
// constant propagation (actually boiling down to carbon_signed_multiply).

module top(clk, num, w32);
  input clk;
  input [3:0] num;
  output signed [31:0] w32;
    
  reg signed [31:0] u32, v32, w32;
  reg signed [63:0] u64, v64, w64;
  reg signed [126:0] u127, v127, w127;
  reg signed [127:0] u128, v128, w128;

  always @(posedge clk) begin
    u32 = num - 8;              // gets a number between -8 and 7
    v32 = u32;
    w32 = u32 * v32;
    u64 = u32;
    v64 = u32;
    w64 = u64 * v64;
    u127 = u32;
    v127 = u32;
    w127 = u127 * v127;
    u128 = u32;
    v128 = u32;
    w128 = u128 * v128;
`ifdef CARBON
    $display("u32=%d, w32=%d, w64=%d, w127=%d, w128=%d",
             u32, w32, w64, w127, w128);
`else
    $display("CARBON: u32=%d, w32=%d, w64=%d, w127=%d, w128=%d",
             u32, w32, w64, w127, w128);
`endif
  end
endmodule
