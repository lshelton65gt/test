// last mod: Thu Jul  5 15:28:30 2007
// filename: test/langcov/Signed/bug7455_01.v
// Description:  This test checks for proper right shift of signed numbers


module bug7455_01(in1, in2, out7, out8);
   input signed [6:0] in1, in2;
   output [6:0] out7;
   output [7:0] out8;
   reg [6:0] 	out7;
   reg [7:0] 	out8;

   always @(in1)
     begin: main
	out7 = in1 >> 1;	// LHS and left op are same size
	out8 = in2 >> 1;	// LHS and left op are diff size, left op needs to be sign extended first 
     end
endmodule
