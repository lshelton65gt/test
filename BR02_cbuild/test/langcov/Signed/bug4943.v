// bug4943 - codegen overload trouble

module top(clk,out);
   input clk;
   output out;
   reg signed[127:0] out;
   always@(posedge clk)
     out = (out << 8) + 1;
endmodule
