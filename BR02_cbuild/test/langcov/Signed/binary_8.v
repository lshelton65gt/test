// last mod: Wed Jun 22 11:10:14 2005
// filename: test/langcov/Signed/binary_8.v
// Description:  This test checks signed and unsigned operands of bitwise xor
// binary_8.sim.log.gold checked with NC, differs with ALDEC

`define OP1 ^
`define OP2 ^
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"
