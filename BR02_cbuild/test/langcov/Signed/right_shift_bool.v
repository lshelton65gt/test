module top(s1,s2,s3,s4);
   input signed [64:0] s1;
   input signed [9:0] s2, s3;
   output signed [128:0] s4;

   assign s4 = (s1 && s2) >> s3;
endmodule
