// filename: test/langcov/Signed/bug15862_48.v
// Description:  This test was inspired by bug 15862, it is contrived so that
// the if condition has a high chance of resulting in a somewhat equal
// distribution of true and false values with just a small number of random inputs.


`define WIDTH 48

module bug15862_48(clock, in1, in2, out1);
   input clock;
   input [1:0] in1;
   input in2;
   output out1;
   reg out1;

   reg signed [`WIDTH-1:0] t1;

   always @(posedge clock)
     begin: main
	t1 = {in1, {`WIDTH-2{in2}}};	// assemble the test vector from two inputs, so that there is a high chance that we get both positive and negative values
	if ( t1 > 32'sh80000000 )
	  out1 = 1;
	else
	  out1 = 0;
     end
endmodule
