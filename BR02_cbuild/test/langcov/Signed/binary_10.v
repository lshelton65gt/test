// last mod: Wed Jun 22 11:10:45 2005
// filename: test/langcov/Signed/binary_10.v
// Description:  This test checks signed and unsigned operands of addition
// binary_10.sim.log.gold checked with NC [finsim disagrees]

`define OP1 ===
`define OP2 ===
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"
