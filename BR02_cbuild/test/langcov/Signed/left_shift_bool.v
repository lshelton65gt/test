// left-shift-bool
module top(a,b,c);
   output signed [9:0] c;
   input  signed [9:0] a,b;

   assign c = ( a > 32) << b;
endmodule

