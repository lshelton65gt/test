// last mod: Tue Jan 31 22:12:55 2006
// filename: test/langcov/Signed/extension_1.v
// Description:  This test checks for proper sign and zero extensions


module extension_1(clock, in1, in2, in3, out1, out2, out3);
   input clock;
   input signed [4:0] in1, in2, in3;
   output [9:0] out1, out2, out3;
   reg [9:0] 	out1, out2, out3;

   always @(posedge clock)
     begin: main
	out1 = (in1 < in2);  // expect this to be 0 extended to 10 bits
	out2 = (in1 < in2) + in3; // expect in3 to be 0 extended to 10 bits
	out3 = $signed(in1 < in2) + in3; // expect in3 to be sign extended to 10 bits
     end
endmodule
