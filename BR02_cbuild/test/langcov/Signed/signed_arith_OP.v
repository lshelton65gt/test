// corner case test for signed arithmetic operations
//    different size operands, ranging from 1 bit thru bitvector sizes
// Requires V2k flag

`ifndef CONVERT
 `define CONVERT
`endif

module top(i,o);
   input [63:0] i;
   output [3813:0] o;

   // operands
   wire signed          i1 = i[0];
   wire signed [6:0] i7 = i[7:1];
   wire signed [7:0] i8 = i[7:0];
   wire signed [30:0]i31 = i[30:0];
   wire signed [31:0]i32 = i[31:0];
   wire signed [32:0]i33 = i[32:0];
   wire signed [62:0]i63 = i[62:0];
   wire signed [63:0]i64=i;
   wire signed [64:0]i65 = {i[31:0], 1'b0, i[63:32]};

   assign o = {
               `CONVERT(i1 `OP i1),
               `CONVERT(i1 `OP i7),
               `CONVERT(i1 `OP i8),
               `CONVERT(i1 `OP i31),
               `CONVERT(i1 `OP i32),
               `CONVERT(i1 `OP i33),
               `CONVERT(i1 `OP i63),
               `CONVERT(i1 `OP i64),
               `CONVERT(i1 `OP i65),

               `CONVERT(i7 `OP i1),
               `CONVERT(i7 `OP i7),
               `CONVERT(i7 `OP i8),
               `CONVERT(i7 `OP i31),
               `CONVERT(i7 `OP i32),
               `CONVERT(i7 `OP i33),
               `CONVERT(i7 `OP i63),
               `CONVERT(i7 `OP i64),
               `CONVERT(i7 `OP i65),

               `CONVERT(i8 `OP i1),
               `CONVERT(i8 `OP i7),
               `CONVERT(i8 `OP i8),
               `CONVERT(i8 `OP i31),
               `CONVERT(i8 `OP i32),
               `CONVERT(i8 `OP i33),
               `CONVERT(i8 `OP i63),
               `CONVERT(i8 `OP i64),
               `CONVERT(i8 `OP i65),

               `CONVERT(i31 `OP i1),
               `CONVERT(i31 `OP i7),
               `CONVERT(i31 `OP i8),
               `CONVERT(i31 `OP i31),
               `CONVERT(i31 `OP i32),
               `CONVERT(i31 `OP i33),
               `CONVERT(i31 `OP i63),
               `CONVERT(i31 `OP i64),
               `CONVERT(i31 `OP i65),

               `CONVERT(i32 `OP i1),
               `CONVERT(i32 `OP i7),
               `CONVERT(i32 `OP i8),
               `CONVERT(i32 `OP i31),
               `CONVERT(i32 `OP i32),
               `CONVERT(i32 `OP i33),
               `CONVERT(i32 `OP i63),
               `CONVERT(i32 `OP i64),
               `CONVERT(i32 `OP i65),

               `CONVERT(i33 `OP i1),
               `CONVERT(i33 `OP i7),
               `CONVERT(i33 `OP i8),
               `CONVERT(i33 `OP i31),
               `CONVERT(i33 `OP i32),
               `CONVERT(i33 `OP i33),
               `CONVERT(i33 `OP i63),
               `CONVERT(i33 `OP i64),
               `CONVERT(i33 `OP i65),

               `CONVERT(i63 `OP i1),
               `CONVERT(i63 `OP i7),
               `CONVERT(i63 `OP i8),
               `CONVERT(i63 `OP i31),
               `CONVERT(i63 `OP i32),
               `CONVERT(i63 `OP i33),
               `CONVERT(i63 `OP i63),
               `CONVERT(i63 `OP i64),
               `CONVERT(i63 `OP i65),

               `CONVERT(i64 `OP i1),
               `CONVERT(i64 `OP i7),
               `CONVERT(i64 `OP i8),
               `CONVERT(i64 `OP i31),
               `CONVERT(i64 `OP i32),
               `CONVERT(i64 `OP i33),
               `CONVERT(i64 `OP i63),
               `CONVERT(i64 `OP i64),
               `CONVERT(i64 `OP i65),

               `CONVERT(i65 `OP i1),
               `CONVERT(i65 `OP i7),
               `CONVERT(i65 `OP i8),
               `CONVERT(i65 `OP i31),
               `CONVERT(i65 `OP i32),
               `CONVERT(i65 `OP i33),
               `CONVERT(i65 `OP i63),
               `CONVERT(i65 `OP i64),
               `CONVERT(i65 `OP i65)
               };
endmodule
