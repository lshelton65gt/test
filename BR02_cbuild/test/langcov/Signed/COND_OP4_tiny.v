// This test was inspired by test/interra/expr1/COND_OP4
// At one time carbon would get this wrong.



module COND_OP4_tiny(clk, in6,out0);
input clk;
input [31:0] in6;
output [32:0] out0;
reg [32:0] out0;
integer int16; 

  always @(posedge clk)
  begin
     int16=in6;
     out0 = (int16 >> 4);
  end
endmodule
    
