// last mod: Fri Jul  1 11:27:42 2005
// filename: test/langcov/Signed/size.v
// Description:  This file is the base design file for the size__* series
// you must define `OP1,   `SIZE1 `SIZE2 and `SIZE3 to use it

module size(input signed [`SIZE1-1:0] Sin1,
	    input signed [`SIZE2-1:0] Sin2,
	    output reg signed [`SIZE3-1:0] Sout_a,Sout_b,Sout_c,Sout_d,Sout_e,Sout_f,Sout_g,Sout_h,Sout_i);

   wire [`SIZE1-1:0] Uin1;
   wire [`SIZE2-1:0] Uin2;

   assign 	    Uin1 = Sin1;
   assign 	    Uin2 = Sin2;
   
   always @(*)
     begin: main
	Sout_a = Sin1;
	Sout_b = $signed(Sin1);
	Sout_c = $signed(Uin1);
	
	Sout_d = Sin2;
	Sout_e = $signed(Sin2);
	Sout_f = $signed(Uin2);
	
	Sout_g = Sin1 `OP1 Sin2;
	Sout_h = $signed(Sin1) `OP1 $signed(Sin2);
	Sout_i = $signed(Uin1) `OP1 $signed(Uin2);

     end
endmodule
