// last mod: Tue Jul 12 11:03:18 2005
// filename: test/langcov/Signed/lrm_4_4_2a.v
// Description:  This test was inspired by the prose in section 4.4.2 of LRM
// concerning the sizing of operands
// gold file checked with aldec and NC

module lrm_4_4_2a(input [15:0] in1, in2,
		  output reg [15:0] Uout1, Uout2, Uout3, Uout4,
		  output reg signed [15:0] Sout1, Sout2, Sout3, Sout4);

   reg signed [15:0] Sin1, Sin2;
   reg        [15:0] Uin1, Uin2;

   always @(*)
     begin
	Sin1 = in1;
	Sin2 = in2;
	Uin1 = in1;
	Uin2 = in2;
	
	Uout1 = (Uin1 + Uin2) >> 1;
	Uout2 = (Uin1 + Uin2) >> 1'b1; // should be same as Uout1
	Uout3 = (Uin1 + Uin2 + 0) >> 1;
	Uout4 = (Uin1 + Uin2 + 0) >> 1'b1; // should be same as Uout3

	Sout1 = (Sin1 + Sin2) >> 1;
	Sout2 = (Sin1 + Sin2) >> 1'b1; // should be same as Sout1
	Sout3 = (Sin1 + Sin2 + 0) >> 1;
	Sout4 = (Sin1 + Sin2 + 0) >> 1'b1; // should be same as Sout3

     end
endmodule
