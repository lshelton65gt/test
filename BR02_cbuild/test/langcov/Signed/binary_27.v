// last mod: Tue Jun 21 09:09:10 2005
// filename: test/langcov/Signed/binary_27.v
// Description:  In this test we have 32bits = 32bits + 32bits - 32bits


`define OP1 +
`define OP2 -
`define SIZE1 32
`define SIZE2 32
`define SIZE3 32
`define SIZE4 33

`include "binary.v"
