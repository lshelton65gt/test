// last mod: Fri Jul  1 13:50:21 2005
// filename: test/langcov/Signed/size_5.v
// Description:  This test checks that we can handle multiple size signed expressions.
// assign a long and a short signed value to a long signed value
// gold file checked with nc (aldec gets wrong values)

`define OP1 &
`define SIZE1 8
`define SIZE2 65
`define SIZE3 66

`include "size.v"

