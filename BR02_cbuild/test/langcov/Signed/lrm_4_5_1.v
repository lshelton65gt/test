// last mod: Wed Jun 22 16:38:52 2005
// filename: test/langcov/Signed/lrm_4_5_1.v
// Description:  This test was inspired by lrm section 4.5.1
// aldec gets this wrong, nc is correct and produces this:
//  0000000000000000 0000000011111011 1111111111111011 \
//  000000000000000000000000000000000 \
//  000000000000000000000000011111011 \
//  111111111111111111111111111111011 \

module lrm_4_5_1(output reg [15:0] out1, out2, out3,
		 output reg [33:0] out4, out5, out6);

   reg signed [7:0] Sb;
   integer Ib;
   initial
      begin
	 Sb = -5;
	 Ib = -5;
	 out1 = Sb[2]; 	 // bitselects are unsigned, so zero extend
	 out2 = Sb[7:0]; // a partselect is always unsigned, so is zero extended to fill
	 out3 = Sb;      // Sb is signed, so sign extended

	 out4 = Ib[2];   // bitselects are unsigned, so zero extend
	 out5 = Ib[7:0]; // a partselect is always unsigned, so is zero extended to fill
	 out6 = Ib;      // Ib is signed, so sign extended
	 
      end
endmodule
