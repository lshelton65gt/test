// last mod: Wed Nov 22 07:44:06 2006
// filename: test/langcov/Signed/real_02.v
// Description:  This test checks for proper coercion of operands of ** to real
// we expect that out1 out2, and out3 are always equivalent
// aldec and nc match each other but get this wrong, gold file from carbon

module real_02(clock, in1, in2, in3, out1, out2, out3);
   input clock;
   input [2:0] in1;
   input [2:0] in2;
   input [1:0] in3;
   output [7:0] out1, out2, out3;
   reg [7:0] 	out1, out2, out3;
   reg 		signed [4:0] stemp;

   always @(posedge clock)
     begin: main
	// in the following three assignments to out? since the left operand of ** is signed then the
	// type of the expression is real, the non-self determined operands
	// should be coreced to real before they are sized, so we should see
	// that out1, out2, out3 are always equivalent
	out1 = ($signed(in1) + $signed(in2)) ** in3;

	out2 = ($itor(in1) + $itor(in2)) ** in3;

	stemp = in1 + in2;
	out3 =  stemp ** in3;
     end
endmodule
