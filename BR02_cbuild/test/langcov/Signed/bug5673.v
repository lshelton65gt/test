module fpu_add12 ( co, sum, a, b, cin ) ;
	input	[11:0] 	a ;
	input	[11:0] 	b ;
	input			cin ;
	output			co ;
	output	[11:0]	sum ;

	assign {co, sum } = a + b + cin ;

endmodule

module top(a,b,cin, sum, co, checker);
   input [11:0] a,b;
   output [11:0] sum;
   input         cin;
   output        co, checker;

   fpu_add12 adder(co, sum, a, b, !cin); // Fool makeSizeExplicit

   // Also test that direct specification of a logical in the expression crashes too.
   assign        checker = (a+b+!cin) == {co, sum};
endmodule
