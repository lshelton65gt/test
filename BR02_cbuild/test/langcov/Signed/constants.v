// last mod: Tue Jan  3 14:53:50 2006
// filename: test/langcov/Signed/constants.v
// Description:  This test has many signed constants


module constants(clock, 
		 out0,   out1, out2, out3, out4, out5, out6, out7, out8, 
		 out20, out21, out22, out23, out24, out25, out26, out27, out28, 
		 out30, out31, out32, out33, out34, out35, out36, out37, out38

		 );
   input clock;
   output [7:0] out0,   out1, out2, out3, out4, out5, out6, out7, out8;
   output signed [7:0] out20, out21, out22, out23, out24, out25, out26, out27, out28;
   output signed [31:0] out30, out31, out32, out33, out34, out35, out36, out37, out38;
   reg        [7:0]  out0,   out1, out2, out3, out4, out5, out6, out7, out8;
   reg signed [7:0]  out20, out21, out22, out23, out24, out25, out26, out27, out28;
   reg signed [31:0] out30, out31, out32, out33, out34, out35, out36, out37, out38;


   always @(posedge clock)
     begin: main


	 out4 = 12;		//  An unsized, unbased integer (e.g., 12)
	out24 = 12;
	out34 = 12;
	 out4 = -12;		//  An unsized, unbased integer (e.g., 12)
	out24 = -12;
	out34 = -12;

	 out5 = 'd12; 		// An unsized, based integer (e.g., �d12, �sd12)
	out25 = 'd12;
	out35 = 'd12;
	 out6 = 'sd12; 		// An unsized, based integer (e.g., �d12, �sd12)
	out26 = 'sd12;
	out36 = 'sd12;

         out7 = 16'd12;		// A sized, based integer (e.g., 16�d12, 16�sd12)
	out27 = 16'd12;
	out37 = 16'd12;
         out8 = 16'sd12;	// A sized, based integer (e.g., 16�d12, 16�sd12)
	out28 = 16'sd12;
	out38 = 16'sd12;

	
	// the following are from LRM 4.1.3
	out0 = -12 / 3;
	out20 = -12 / 3;
	out30 = -12 / 3;      // The result is -4.

	out1 = -'d 12 / 3;
	out21 = -'d 12 / 3;
	out31 = -'d 12 / 3;   // The result is 1431655761.

	out2 = -'sd 12 / 3;
	out22 = -'sd 12 / 3;
	out32 = -'sd 12 / 3;  // The result is -4.

	out3 = -4'sd12 / 3;
	out23 = -4'sd12 / 3;
	out33 = -4'sd12 / 3; // -4'sd12 is the negative of the 4-bit
	                      // quantity 1100, which is -4. -(-4) = 4.
                              // The result is 1.

     end
endmodule



