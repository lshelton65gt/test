// last mod: Fri Nov 17 14:42:14 2006
// filename: test/langcov/Signed/real_03.v
// Description:  This test tests automatic coercision of operands to real
// because one of the operands of ** is signed.  We expect that Sout_v and
// int_out will always be equal.


`define OP1 **
`define OP2 **
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

module real_03(
   input [`SIZE1-1:0] in1,
   input [`SIZE2-1:0] in2,
   input [`SIZE3-1:0] in3,
   output reg signed [`SIZE4-1:0]
		  Sout_v,
output reg signed [`SIZE4-1:0] int_out

		  );

   reg signed [`SIZE1-1:0] S1;
   reg        [`SIZE1-1:0] U1;
   reg signed [`SIZE2-1:0] S2;
   reg        [`SIZE2-1:0] U2;
   reg signed [`SIZE3-1:0] S3;
   reg        [`SIZE3-1:0] U3;

   
   always @(*)
     begin: main
	S1 = in1;
	U1 = in1;
	S2 = in2;
	U2 = in2;
	S3 = in3;
	U3 = in3;
	
        Sout_v = (U1 `OP1  U2) `OP2 S3; // (unsigned `OP unsigned) `OP signed
        int_out = $rtoi(($itor(U1) `OP1  U2) `OP2 S3); // (unsigned `OP unsigned) `OP signed

	
     end
endmodule

