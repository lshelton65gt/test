// last mod: Fri Jan 20 08:32:27 2006
// filename: test/langcov/Signed/wide_1.v
// Description:  This test checks for proper sign extension to wide variables
// discovered while reviewing the results for test/fold/signedfold.v


module wide_1(a, b, c, wideInput, out1);
   input a, b, c;
   input signed [65:0] wideInput;
   output out1;

   wire signed [65:0] wide;
   assign wide = a;

   assign out1 = ((~wide) < wideInput) ? b : c;
endmodule
