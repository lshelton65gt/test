// test of proper extensions of bitselects

module top (a,o1,o2,o3, selfcheck);
   input signed[3:0] a;
   output [7:0] o1, o2;
   output       selfcheck;
   output [65:0] o3;
   assign       o1 = $signed(a[2]); // should SIGN EXTEND and produce either 255 or 0
   assign       o2 = a[2];      // should ZERO extend, producing either 1 or 0
   assign       o3 = $signed(a[2]); // should sign-extend
   
   // SHOULD ALWAYS BE TRUE
   assign       selfcheck = ((o2[7:1] == 0) && (&o1 == |o1)
                             &&  (|o3[65:1] == o3[0]));
   
endmodule
