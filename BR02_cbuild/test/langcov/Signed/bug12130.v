// filename: test/langcov/Signed/bug12130.v
// Description:  This test duplicates the issue in bug 12130 but here we just
// use verilog.


module bug12130(clock, in1, in2, out1);
   input clock;
   input [159:0] in1, in2;
   output [1:0] out1;
   reg [1:0] out1;

   always @(posedge clock)
     begin: main
	if ($signed(in1[9:0]) > $signed(in2[9:0]))
	  out1[0] = 1'b1;
	else
	  out1[0] = 1'b0;

	if ($signed(in1[39:30]) > $signed(in2[39:30]))  // this line was being converted to an unsigned compare
	  out1[1] = 1'b1;
	else
	  out1[1] = 1'b0;
     end
endmodule
