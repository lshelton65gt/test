// last mod: Wed Feb 15 14:02:54 2006
// filename: test/langcov/Signed/div_1.v   // this is for bug 5662
// Description:  This test was discovered by
// test/langcov/Signed/Signed_gigantic/binary_H_L_10_1_1_1.v
// with version this would get a NU_ASSERT
//
// it tests something of the form: 
// O10 = I_1 / J_1 > K_1
// F G H I J  K  L M  N  O  P   Q
// + * / & == && > >> << ** >>> <<<

`define OP1 /
`define OP2 >
`define SIZE1 1
`define SIZE2 1
`define SIZE3 1
`define SIZE4 10

module div_1(
   input [`SIZE1-1:0] in1,
   input [`SIZE2-1:0] in2,
   input [`SIZE3-1:0] in3,
   output reg signed [`SIZE4-1:0]
	                   Sout_a, Sout_b, Sout_c, Sout_d,
                           Sout_e, Sout_f, Sout_g, Sout_h,
		           Sout_i, Sout_j, Sout_k, Sout_l,
		           Sout_m, Sout_n, Sout_o, Sout_p,
		           Sout_q, Sout_r, Sout_s, Sout_t,
		           Sout_u, Sout_v, Sout_w, Sout_x,
		           Sout_y, Sout_z);

   reg signed [`SIZE1-1:0] S1;
   reg        [`SIZE1-1:0] U1;
   reg signed [`SIZE2-1:0] S2;
   reg        [`SIZE2-1:0] U2;
   reg signed [`SIZE3-1:0] S3;
   reg        [`SIZE3-1:0] U3;

///   initial
///      begin:ini
///	 reg [3:0] op1;
///	 reg [3:0] op2;
///	 op1 = "`OP1";
///	 op2 = "`OP2";
///
///	 $display("`OP1=%s", op1 + "A");
///      end
   
   always @(*)
     begin: main
	S1 = in1;
	U1 = in1;
	S2 = in2;
	U2 = in2;
	S3 = in3;
	U3 = in3;
	
        Sout_a =        S2  `OP2 U3;    // signed `OP unsigned
        Sout_b =        S2  `OP2 S3;    // signed `OP signed

        Sout_c = (S1 `OP1  S2) `OP2 S3; // (signed `OP signed) `OP signed
        Sout_d =  S1 `OP1 (S2  `OP2 S3);// signed `OP (signed `OP signed)
        Sout_e =  S1 `OP1  S2  `OP2 S3; // signed `OP signed `OP signed

        Sout_f =  S1 `OP1 (S2  `OP2 U3);// signed `OP (signed `OP unsigned)
        Sout_g = (S1 `OP1  S2) `OP2 U3; // (signed `OP signed) `OP unsigned
        Sout_h =  S1 `OP1  S2  `OP2 U3; // signed `OP signed `OP unsigned

        Sout_i =  S1 `OP1 (U2  `OP2 S3);// signed `OP (unsigned `OP signed)
        Sout_j = (S1 `OP1  U2) `OP2 S3; // (signed `OP unsigned) `OP signed
        Sout_k =  S1 `OP1  U2  `OP2 S3; // signed `OP unsigned `OP signed

        Sout_l =  S1 `OP1 (U2  `OP2 U3);// signed `OP (unsigned `OP unsigned)
        Sout_m = (S1 `OP1  U2) `OP2 U3; // (signed `OP unsigned) `OP unsigned
        Sout_n =  S1 `OP1  U2  `OP2 U3; // signed `OP unsigned `OP unsigned

        Sout_o =  U1 `OP1 (S2  `OP2 S3);// unsigned `OP (signed `OP signed)
        Sout_p = (U1 `OP1  S2) `OP2 S3; // (unsigned `OP signed) `OP signed
        Sout_q =  U1 `OP1  S2  `OP2 S3; // unsigned `OP signed `OP signed

        Sout_r =  U1 `OP1 (S2  `OP2 U3);// unsigned `OP (signed `OP unsigned)
        Sout_s = (U1 `OP1  S2) `OP2 U3; // (unsigned `OP signed) `OP unsigned
        Sout_t =  U1 `OP1  S2  `OP2 U3; // unsigned `OP signed `OP unsigned
	
        Sout_u =  U1 `OP1 (U2  `OP2 S3);// unsigned `OP (unsigned `OP signed)
        Sout_v = (U1 `OP1  U2) `OP2 S3; // (unsigned `OP unsigned) `OP signed
        Sout_w =  U1 `OP1  U2  `OP2 S3; // unsigned `OP unsigned `OP signed

        Sout_x =  U1 `OP1 (U2  `OP2 U3);// unsigned `OP (unsigned `OP unsigned)
        Sout_y = (U1 `OP1  U2) `OP2 U3; // (unsigned `OP unsigned) `OP unsigned
        Sout_z =  U1 `OP1  U2  `OP2 U3; // unsigned `OP unsigned `OP unsigned

	
     end
endmodule
