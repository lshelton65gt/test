// last mod: Fri Jul  1 11:25:29 2005
// filename: test/langcov/Signed/size_1.v
// Description:  This test checks that we can handle multiple size signed expressions.
// assign a short signed value to a short signed value
// gold file checked with aldec and nc

`define OP1 &
`define SIZE1 8
`define SIZE2 8
`define SIZE3 8

`include "size.v"

