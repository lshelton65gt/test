// last mod: Mon Nov 20 15:56:20 2006
// filename: test/langcov/Signed/real_07.v
// Description:  This test checks for proper calculation of real values.
// In this testcase all terms used in each expression calculations have the
// value 7 (some signed some unsigned, but always the value 7 (never -1))
//
// so we have different forms of the following expressions:
//  1:  (7 ** 7)
//  2:  ((7 ** 7) ** 7)
// the questions are:
//   what is the result?
//   what is the result size?
//   what is the result type?
//   what is the size of intermediate terms?
// here the first section of $display uses an unsigned term as the LOP for a **
// ** (with a value of 7), in the second section a signed term is used (has the
// same value of 7) so the results should be the same.

// as of 11/21/06 gold file hand created based on our current understanding of verilog
module real_07(clock);
   input clock;
   reg 	     signed [3:0] s4;
   reg [2:0] u3;
   reg [3:0] u4;
   
   initial
      begin
	 s4 = 4'b0111;
	 u4 = 4'b0111;
	 u3 = 3'b111;
      end

   always @(posedge clock)
     begin: main
	// in each case show values as %f and self determined expressions


	// a value of 7 for the following could be considered correct: (3'b111**3'b111)[2:0] == 7
	$display("(3'b111 ** 3'b111) = %%f:%f, self determined: ",(3'b111 ** 3'b111),(3'b111 ** 3'b111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((3'b111 ** 3'b111) ** 4'sb0111) = %%f:%f, self determined: ",((3'b111 ** 3'b111) ** 4'sb0111),((3'b111 ** 3'b111) ** 4'sb0111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((u3 ** u3) ** s4) = %%f:%f, self determined: ",((u3 ** u3) ** s4),((u3 ** u3) ** s4));

	// the following should be coerced to real (signed operand) and result in 823543.0
	$display("(4'sb0111 ** 3'b111) = %%f:%f, self determined: ",(4'sb0111 ** 3'b111),(4'sb0111 ** 3'b111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((4'sb0111 ** 3'b111) ** 4'sb0111) = %%f:%f, self determined: ",((4'sb0111 ** 3'b111) ** 4'sb0111),((4'sb0111 ** 3'b111) ** 4'sb0111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((s4 ** u3) ** s4) = %%f:%f, self determined: ",((s4 ** u3) ** s4),((s4 ** u3) ** s4));

	// the following should be coerced to real (signed operand) and result in 823543.0
	$display("(4'sb0111 ** 4'b0111) = %%f:%f, self determined: ",(4'sb0111 ** 4'b0111),(4'sb0111 ** 4'b0111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((4'sb0111 ** 4'b0111) ** 4'b0111) = %%f:%f, self determined: ",((4'sb0111 ** 4'b0111) ** 4'b0111),((4'sb0111 ** 4'b0111) ** 4'b0111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((s4 ** u4) ** u4) = %%f:%f, self determined: ",((s4 ** u4) ** u4),((s4 ** u4) ** u4));

	// a value of 7 for the following could be considered correct: (4'b0111**3'b111)[3:0] == 7
	$display("(4'b0111 ** 3'b111) = %%f:%f, self determined: ",(4'b0111 ** 3'b111),(4'b0111 ** 3'b111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((4'b0111 ** 3'b111) ** 4'sb0111) = %%f:%f, self determined: ",((4'b0111 ** 3'b111) ** 4'sb0111),((4'b0111 ** 3'b111) ** 4'sb0111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((u4 ** u3) ** s4) = %%f:%f, self determined: ",((u4 ** u3) ** s4),((u4 ** u3) ** s4));
     end
endmodule
