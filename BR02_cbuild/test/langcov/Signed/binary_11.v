// last mod: Wed Jun 22 10:06:00 2005
// filename: test/langcov/Signed/binary_11.v
// Description:  This test checks signed and unsigned operands of !==
// binary_11.sim.log.gold checked with aldec 

`define OP1 !==
`define OP2 !==
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"

