// corner case test for signed comparisons
//    different size operands, ranging from 1 bit thru bitvector sizes
// Requires V2k flag

module top(i,o);
   input [63:0] i;
   output [80:0] o;

   // operands
   wire signed          i1 = i[0];
   wire signed [6:0] i7 = i[7:1];
   wire signed [7:0] i8 = i[7:0];
   wire signed [30:0]i31 = i[30:0];
   wire signed [31:0]i32 = i[31:0];
   wire signed [32:0]i33 = i[32:0];
   wire signed [62:0]i63 = i[62:0];
   wire signed [63:0]i64=i;
   wire signed [64:0]i65 = {i[31:0], 1'b0, i[63:32]};

   // outputs
   assign     o[0] = (i1 `OP i1);
   assign     o[1] = (i1 `OP i7);
   assign     o[2] = (i1 `OP i8);
   assign     o[3] = (i1 `OP i31);
   assign     o[4] = (i1 `OP i32);
   assign     o[5] = (i1 `OP i33);
   assign     o[6] = (i1 `OP i63);
   assign     o[7] = (i1 `OP i64);
   assign     o[8] = (i1 `OP i65);

   assign     o[9] = (i7 `OP i1);
   assign     o[10] = (i7 `OP i7);
   assign     o[11] = (i7 `OP i8);
   assign     o[12] = (i7 `OP i31);
   assign     o[13] = (i7 `OP i32);
   assign     o[14] = (i7 `OP i33);
   assign     o[15] = (i7 `OP i63);
   assign     o[16] = (i7 `OP i64);
   assign     o[17] = (i7 `OP i65);

   assign     o[18] = (i8 `OP i1);
   assign     o[19] = (i8 `OP i7);
   assign     o[20] = (i8 `OP i8);
   assign     o[21] = (i8 `OP i31);
   assign     o[22] = (i8 `OP i32);
   assign     o[23] = (i8 `OP i33);
   assign     o[24] = (i8 `OP i63);
   assign     o[25] = (i8 `OP i64);
   assign     o[26] = (i8 `OP i65);

   assign     o[27] = (i31 `OP i1);
   assign     o[28] = (i31 `OP i7);
   assign     o[29] = (i31 `OP i8);
   assign     o[30] = (i31 `OP i31);
   assign     o[31] = (i31 `OP i32);
   assign     o[32] = (i31 `OP i33);
   assign     o[33] = (i31 `OP i63);
   assign     o[34] = (i31 `OP i64);
   assign     o[35] = (i31 `OP i65);

   assign     o[36] = (i32 `OP i1);
   assign     o[37] = (i32 `OP i7);
   assign     o[38] = (i32 `OP i8);
   assign     o[39] = (i32 `OP i31);
   assign     o[40] = (i32 `OP i32);
   assign     o[41] = (i32 `OP i33);
   assign     o[42] = (i32 `OP i63);
   assign     o[43] = (i32 `OP i64);
   assign     o[44] = (i32 `OP i65);

   assign     o[45] = (i33 `OP i1);
   assign     o[46] = (i33 `OP i7);
   assign     o[47] = (i33 `OP i8);
   assign     o[48] = (i33 `OP i31);
   assign     o[49] = (i33 `OP i32);
   assign     o[50] = (i33 `OP i33);
   assign     o[51] = (i33 `OP i63);
   assign     o[52] = (i33 `OP i64);
   assign     o[53] = (i33 `OP i65);

   assign     o[54] = (i63 `OP i1);
   assign     o[55] = (i63 `OP i7);
   assign     o[56] = (i63 `OP i8);
   assign     o[57] = (i63 `OP i31);
   assign     o[58] = (i63 `OP i32);
   assign     o[59] = (i63 `OP i33);
   assign     o[60] = (i63 `OP i63);
   assign     o[61] = (i63 `OP i64);
   assign     o[62] = (i63 `OP i65);

   assign     o[63] = (i64 `OP i1);
   assign     o[64] = (i64 `OP i7);
   assign     o[65] = (i64 `OP i8);
   assign     o[66] = (i64 `OP i31);
   assign     o[67] = (i64 `OP i32);
   assign     o[68] = (i64 `OP i33);
   assign     o[69] = (i64 `OP i63);
   assign     o[70] = (i64 `OP i64);
   assign     o[71] = (i64 `OP i65);

   assign     o[72] = (i65 `OP i1);
   assign     o[73] = (i65 `OP i7);
   assign     o[74] = (i65 `OP i8);
   assign     o[75] = (i65 `OP i31);
   assign     o[76] = (i65 `OP i32);
   assign     o[77] = (i65 `OP i33);
   assign     o[78] = (i65 `OP i63);
   assign     o[79] = (i65 `OP i64);
   assign     o[80] = (i65 `OP i65);
endmodule
