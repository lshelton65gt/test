// last mod: Thu Feb 16 12:42:53 2006
// filename: test/langcov/Signed/signed_3.v
// Description:  This test checks for proper sign extension from the $signed function


module signed_3(
		input in1, 
		output reg signed [5:0] out1, out2, out3, out4);
   reg 		       signed  s1;

   always @(*)
     begin: main
	s1 = in1;
	out1 = in1;
	out2 = $unsigned(s1);
	
	out3 = s1;
	out4 = $signed(in1);
     end
endmodule
