// last mod: Mon Nov 20 12:07:59 2006
// filename: test/langcov/Signed/real_05.v
// Description:  This test checks for proper rounding of real values
// we should see r1 change, and the result of the comparison should be true in
// first clock cycle and  then later false.  Also we expect that Sout1 and Sout2
// are equivalent in first clock, and then different in other clock cycles.
// this test also checks for proper constant generation/evaluation
// currently fails, gold is from aldec but it may not be correct
module real_05(clock, Sout1, Sout2);
   input clock;
   output signed [9:0] Sout1, Sout2;
   reg signed [9:0] Sout1, Sout2;
   real      r1;

   reg 	     signed [3:0] s4;
   reg [1:0] u2;
   reg [2:0] u3;
   
   initial
      begin
	 r1 = 239299329230617526140928.0;
	 s4 = 4'b0111;
	 u2 = 2'b11;
	 u3 = 3'b111;
      end

   always @(posedge clock)
     begin: main
	$display("((3'b011 ** 4'b0111) ** 4'sb0111) = %f",((3'b011 ** 4'b0111) ** 4'sb0111));
	$display("pre  r1 %f ~= %f => %b",
		 r1, ((3'b011 ** 4'b0111) ** 4'sb0111), (-1.0 < (r1 - ((3'b011 ** 4'b0111) ** 4'sb0111)) < 1.0));
	Sout1 = r1;
	Sout2 = (3'b011 ** 4'b0111) ** 4'sb0111;
	r1 = r1 * 100.0;
     end
endmodule
