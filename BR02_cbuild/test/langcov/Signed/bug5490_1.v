// last mod: Tue Jan  3 06:29:09 2006
// filename: test/langcov/Signed/bug5490_1.v
// Description:  This test is from bug 5490, with variation where function
// returns a single bit that is also signed.  The results should be identical to
// those of bug5409.v

module bug5490_1(out, in1, in2);
   output [23:0] out;
   input [11:0]  in1, in2;

   reg [23:0] 	 out;
   reg  	 out_s;
   reg [22:0] 	 out_r;
   
   function signed f_signed_mult
      (input signed [11:0] data_0,
       input signed [11:0] data_1);
      reg signed [23:0] temp;
       begin
          temp = data_0 * data_1;
	  f_signed_mult = temp[23];
       end
   endfunction

   always @(in1 or in2)
      begin
	 out_s = f_signed_mult($signed(in1), $signed(in2));
	 out_r = $signed(in1)*$signed(in2);
	 out = {out_s,out_r};
      end

endmodule
