// last mod: Fri Jul  1 11:31:39 2005
// filename: test/langcov/Signed/size_2.v
// Description:  This test assigns short signed values to a long signed
// destination.
// gold file checked with nc, (aldec gets wrong results)


`define OP1 &
`define SIZE1 8
`define SIZE2 8
`define SIZE3 66

`include "size.v"

