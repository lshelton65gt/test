// last mod: Mon Apr  9 22:09:11 2007
// filename: test/langcov/Signed/fold_constants.v
// Description:  This test was created while debugging a gcc problem.  The real
// issue was that there was a section of fold in cbuild that was untested (sExtendConstant).
// In this example I create several variations on signed division of constant
// values that are only recognized as constants after fold has processed the
// expressions. Each pair of outputs has a positive and negative version of the
// same pair of constant values.


module fold_constants(in1, in2, Sout_a, Sout_b, Sout_c, Sout_d,Sout_e,
		      Sout_f,Sout_g, Sout_h, Sout_i, Sout_j, Sout_k, Sout_l, Sout_m, Sout_n );
   input [1:0] in1;
   input [2:0] in2;
   output [9:0] Sout_a, Sout_b, Sout_c, Sout_d,Sout_e, Sout_f,Sout_g, Sout_h;
   output [31:0] Sout_i, Sout_j;
   output [62:0] Sout_k, Sout_l;
   output [63:0] Sout_m, Sout_n;

   reg 		signed   [9:0] Sout_a, Sout_b, Sout_c, Sout_d,Sout_e, Sout_f,Sout_g, Sout_h;
   reg 		signed   [31:0] Sout_i, Sout_j;
   reg 		signed   [62:0] Sout_k, Sout_l; 		
   reg 		signed   [63:0] Sout_m, Sout_n;

   reg 		signed   [1:0]          S1;
   reg 		signed   [2:0]         S2;

   always @( in1 or in2 )
     begin
	S1 = in1;
	S2 = in2;
	Sout_a = ($signed(13'h0000 | {(S1^S1),(S2^S2)}) / 13'sd22); 
	Sout_b = ($signed(13'h1000 | {(S1^S1),(S2^S2)}) / 13'sd22); 

	Sout_c = ($signed(10'h000 | {(S1^S1),(S2^S2)}) / 10'sd22); 
	Sout_d = ($signed(10'h200 | {(S1^S1),(S2^S2)}) / 10'sd22); 

	Sout_e = ($signed(9'h000 | {(S1^S1),(S2^S2)}) / 9'sd22); 
	Sout_f = ($signed(9'h100 | {(S1^S1),(S2^S2)}) / 9'sd22); 

	Sout_g = ($signed(4'h0 | {(S1[1:0]^S1[1:0]),(S2[1:0]^S2[1:0])}) / 4'sd12); 
	Sout_h = ($signed(4'h8 | {(S1[1:0]^S1[1:0]),(S2[1:0]^S2[1:0])}) / 4'sd12); 

	Sout_i = ($signed(32'h00000000 | {(S1^S1),(S2^S2)}) / 22); 
	Sout_j = ($signed(32'h80000000 | {(S1^S1),(S2^S2)}) / 22); 

	Sout_k = ($signed(63'h0000000000000000 | {(S1^S1),(S2^S2)}) / 63'sd22); 
	Sout_l = ($signed(63'h4000000000000000 | {(S1^S1),(S2^S2)}) / 63'sd22); 

	Sout_m = ($signed(64'h0000000000000000 | {(S1^S1),(S2^S2)}) / 64'sd22); 
	Sout_n = ($signed(64'h8000000000000000 | {(S1^S1),(S2^S2)}) / 64'sd22); 
	
     end
endmodule
