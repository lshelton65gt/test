// last mod: Fri Nov 17 17:19:50 2006
// filename: test/langcov/Signed/real_04.v
// Description:  This test checks for proper sign extension of an operand
// coerced to real.  We expect Sout_a and Sout_b to be equivalent

`define OP1 **
`define OP2 **
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

module real_04(
   input [`SIZE1-1:0] in1,
   input [`SIZE2-1:0] in2,
   input [`SIZE3-1:0] in3,
   output reg signed [`SIZE4-1:0] Sout_a, Sout_b);

   reg signed [`SIZE1-1:0] S1;
   reg        [`SIZE1-1:0] U1;
   reg signed [`SIZE2-1:0] S2;
   reg        [`SIZE2-1:0] U2;
   reg signed [`SIZE3-1:0] S3;
   reg        [`SIZE3-1:0] U3;

   
   always @(*)
     begin: main
	S1 = in1;
	U1 = in1;
	S2 = in2;
	U2 = in2;
	S3 = in3;
	U3 = in3;
	
        Sout_a =   S2 `OP2 U3;
        Sout_b = $rtoi($itor(S2)) `OP2 U3;
	
     end
endmodule

