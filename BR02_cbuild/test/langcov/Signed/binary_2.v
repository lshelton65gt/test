// last mod: Tue Jun 21 14:06:54 2005
// filename: test/langcov/Signed/binary_2.v
// Description:  This test checks signed and unsigned operands of subtraction
// binary_2.sim.log.gold checked with aldec and nc

`define OP1 -
`define OP2 -
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"
