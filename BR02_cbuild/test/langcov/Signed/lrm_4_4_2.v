// last mod: Tue Jun 14 10:34:57 2005
// filename: test/langcov/Signed/lrm_4_4_2.v
// Description:  This test was inspired by the example lrm section 4.4.2
// extensions. gold file checked with nc, aldec produces incorrect results


module lrm_4_4_2(output reg out);
   
   reg [3:0] a,b;
   reg [1:0] c;
   reg [4:0] d;
   reg signed [3:0] sa,sb;
   reg signed [1:0] sc;
   reg signed [4:0] sd;

   initial begin
      a = 9;
      b = 8;
      c = 1;
      d = -1;
      sa = 9;
      sb = 8;
      sc = 1;
      sd = -1;
      $display("answer (unsigned) = %b", c ? (a&b) : d);
      $display("answer (signed)   = %b", sc ? (sa&sb) : sd);
      out = 1;			// just to keep design alive
   end
endmodule

// expected output (NC)
//the $display statement will display:
//answer (unsigned) = 01000
//answer (signed)   = 11000
//1
