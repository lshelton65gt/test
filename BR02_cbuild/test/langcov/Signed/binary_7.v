// last mod: Wed Jun 22 11:09:59 2005
// filename: test/langcov/Signed/binary_7.v
// Description:  This test checks signed and unsigned operands of bitwise OR
// binary_7.sim.log.gold checked with NC, differs with Aldec

`define OP1 |
`define OP2 |
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"
