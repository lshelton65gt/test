// last mod: Wed Nov 22 08:02:05 2006
// filename: test/langcov/Signed/real_06.v
// Description:  This test checks for proper calculation of real values
// it is a good example of how constants should be properly evaluated during
// population.
// On mainline this test asserts.
// With -newSizing this test has incorrect simultation results.
// see also real_07.v where all operands have the same value.
module real_06(clock);
   input clock;
   reg 	     signed [3:0] s4;
   reg [1:0] u2;
   reg [2:0] u3;
   
   initial
      begin
	 s4 = 4'b0111;
	 u2 = 2'b11;
	 u3 = 3'b111;
      end

   always @(posedge clock)
     begin: main
	// show values as %f and self determined

	// a value of 3 for the following could be considered correct: (2'b11 ** 3'b111)[1:0] == 3
	$display("(2'b11 ** 3'b111) = %%f:%f, self determined: ",(2'b11 ** 3'b111),(2'b11 ** 3'b111));
	// the following should be coerced to real (signed operand) and result in a value of 239299329230617529590083.0
	$display("((2'b11 ** 3'b111) ** 4'sb0111) = %%f:%f, self determined: ",((2'b11 ** 3'b111) ** 4'sb0111),((2'b11 ** 3'b111) ** 4'sb0111));
	// the following should be coerced to real (signed operand) and result in a value of 239299329230617529590083.0
	$display("((u2 ** u3) ** s4) = %%f:%f, self determined: ",((u2 ** u3) ** s4),((u2 ** u3) ** s4));

	// a value of 7 for the following could be considered correct: (3'b111 ** 3'b111)[2:0] == 7
	$display("(3'b111 ** 3'b111) = %%f:%f, self determined: ",(3'b111 ** 3'b111),(3'b111 ** 3'b111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((3'b111 ** 3'b111) ** 4'sb0111) = %%f:%f, self determined: ",((3'b111 ** 3'b111) ** 4'sb0111),((3'b111 ** 3'b111) ** 4'sb0111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((u3 ** u3) ** s4) = %%f:%f, self determined: ",((u3 ** u3) ** s4),((u3 ** u3) ** s4));

	// the following should be coerced to real (signed operand) and result in 823543.0
	$display("(4'sb0111 ** 3'b111) = %%f:%f, self determined: ",(4'sb0111 ** 3'b111),(4'sb0111 ** 3'b111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((4'sb0111 ** 3'b111) ** 4'sb0111) = %%f:%f, self determined: ",((4'sb0111 ** 3'b111) ** 4'sb0111),((4'sb0111 ** 3'b111) ** 4'sb0111));
	// the following should be coerced to real (signed operand) and result in 2.569235775210589e+41
	$display("((s4 ** u3) ** s4) = %%f:%f, self determined: ",((s4 ** u3) ** s4),((s4 ** u3) ** s4));
     end
endmodule
