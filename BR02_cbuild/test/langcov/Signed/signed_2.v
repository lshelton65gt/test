// last mod: Thu Jun 23 10:04:51 2005
// filename: test/langcov/Signed/signed_2.v
// Description:  This test checks the $signed operator, it is setup to
// be self checking, all outputs are expected to be 1 (verified with nc)


module signed_2(
		input [1:0] in2,
		input [2:0] in3,
		input [3:0] in4,
		output reg  check_a, check_b, check_c, check_d,
		            check_e, check_f, check_g, check_h,
		            check_i, check_j, check_k, check_l,
		            check_m, check_n, check_o, check_p,
		            check_q, check_r, check_s, check_t,
		            check_u, check_v);

   reg signed [1:0] S2;
   reg        [1:0] U2;
   reg signed [2:0] S3;
   reg        [2:0] U3;
   reg signed [3:0] S4;
   reg        [3:0] U4;
   reg signed [9:0] S_implicit10_a, S_implicit10_b, S_implicit10_c, S_implicit10_d,
                    S_implicit10_e, S_implicit10_f, S_implicit10_g, S_implicit10_h,
		    S_implicit10_i, S_implicit10_j, S_implicit10_k, S_implicit10_l,
		    S_implicit10_m, S_implicit10_n, S_implicit10_o, S_implicit10_p,
		    S_implicit10_q, S_implicit10_r, S_implicit10_s, S_implicit10_t,
                    S_implicit10_u, S_implicit10_v;
   reg signed [9:0] S_explicit10_a, S_explicit10_b, S_explicit10_c, S_explicit10_d,
                    S_explicit10_e, S_explicit10_f, S_explicit10_g, S_explicit10_h,
		    S_explicit10_i, S_explicit10_j, S_explicit10_k, S_explicit10_l,
		    S_explicit10_m, S_explicit10_n, S_explicit10_o, S_explicit10_p,
		    S_explicit10_q, S_explicit10_r, S_explicit10_s, S_explicit10_t,
                    S_explicit10_u, S_explicit10_v;

   initial
     $display("i2 i3  i4abcdefghijklmnopqrstuv");

   always @(*)
     begin: main
	S2 = in2;
	U2 = in2;
	S3 = in3;
	U3 = in3;
	S4 = in4;
	U4 = in4;

	if (           U2  <           U3  ) S_implicit10_a = 1'b1; else S_implicit10_a = 1'b0;
	if ( $unsigned(U2) < $unsigned(U3) ) S_explicit10_a = 1'b1; else S_explicit10_a = 1'b0;
	check_a = S_implicit10_a == S_explicit10_a;


	if (           U2  <           U3 ) S_implicit10_b = 1'b1; else S_implicit10_b = 1'b0;
	if ( $unsigned(U2) <           U3 ) S_explicit10_b = 1'b1; else S_explicit10_b = 1'b0;
	check_b = S_implicit10_b == S_explicit10_b;

	if (           U2  <           U3  ) S_implicit10_c = 1'b1; else S_implicit10_c = 1'b0;
	if (           U2  < $unsigned(U3) ) S_explicit10_c = 1'b1; else S_explicit10_c = 1'b0;
	check_c = S_implicit10_c == S_explicit10_c;

	if (         S2  <         S3  ) S_implicit10_d = 1'b1; else S_implicit10_d = 1'b0;
	if ( $signed(S2) < $signed(S3) ) S_explicit10_d = 1'b1; else S_explicit10_d = 1'b0;
	check_d = S_implicit10_d == S_explicit10_d;

	if (         S2  <         S3  ) S_implicit10_e = 1'b1; else S_implicit10_e = 1'b0;
	if (         S2  < $signed(S3) ) S_explicit10_e = 1'b1; else S_explicit10_e = 1'b0;
	check_e = S_implicit10_e == S_explicit10_e;

	if (         S2  <         S3  ) S_implicit10_f = 1'b1; else S_implicit10_f = 1'b0;
	if ( $signed(S2) <         S3  ) S_explicit10_f = 1'b1; else S_explicit10_f = 1'b0;
	check_f = S_implicit10_f == S_explicit10_f;

	if (         S2  <         S3  ) S_implicit10_g = 1'b1; else S_implicit10_g = 1'b0;
	if ( $signed(U2) < $signed(U3) ) S_explicit10_g = 1'b1; else S_explicit10_g = 1'b0;
	check_g = S_implicit10_g == S_explicit10_g;

	if (         S2  <         S3  ) S_implicit10_h = 1'b1; else S_implicit10_h = 1'b0;
	if (         S2  < $signed(U3) ) S_explicit10_h = 1'b1; else S_explicit10_h = 1'b0;
	check_h = S_implicit10_h == S_explicit10_h;

	if (         S2  <         S3  ) S_implicit10_i = 1'b1; else S_implicit10_i = 1'b0;
	if ( $signed(U2) <         S3  ) S_explicit10_i = 1'b1; else S_explicit10_i = 1'b0;
	check_i = S_implicit10_i == S_explicit10_i;

	if (                   S2   <                   S3   ) S_implicit10_j = 1'b1; else S_implicit10_j = 1'b0;
	if ( $signed($unsigned(S2)) < $signed($unsigned(S3)) ) S_explicit10_j = 1'b1; else S_explicit10_j = 1'b0;
	check_j = S_implicit10_j == S_explicit10_j;

	if (                           S2    <                           S3   ) S_implicit10_k = 1'b1; else S_implicit10_k = 1'b0;
	if ( $signed($unsigned($signed(U2))) < $signed($unsigned($signed(S3)))) S_explicit10_k = 1'b1; else S_explicit10_k = 1'b0;
	check_k = S_implicit10_k == S_explicit10_k;


	if (           U2  >           U3  ) S_implicit10_l = 1'b1; else S_implicit10_l = 1'b0;
	if ( $unsigned(U2) > $unsigned(U3) ) S_explicit10_l = 1'b1; else S_explicit10_l = 1'b0;
	check_l = S_implicit10_l == S_explicit10_l;


	if (           U2  >           U3 ) S_implicit10_m = 1'b1; else S_implicit10_m = 1'b0;
	if ( $unsigned(U2) >           U3 ) S_explicit10_m = 1'b1; else S_explicit10_m = 1'b0;
	check_m = S_implicit10_m == S_explicit10_m;

	if (           U2  >           U3  ) S_implicit10_n = 1'b1; else S_implicit10_n = 1'b0;
	if (           U2  > $unsigned(U3) ) S_explicit10_n = 1'b1; else S_explicit10_n = 1'b0;
	check_n = S_implicit10_n == S_explicit10_n;

	if (         S2  >         S3  ) S_implicit10_o = 1'b1; else S_implicit10_o = 1'b0;
	if ( $signed(S2) > $signed(S3) ) S_explicit10_o = 1'b1; else S_explicit10_o = 1'b0;
	check_o = S_implicit10_o == S_explicit10_o;

	if (         S2  >         S3  ) S_implicit10_p = 1'b1; else S_implicit10_p = 1'b0;
	if (         S2  > $signed(S3) ) S_explicit10_p = 1'b1; else S_explicit10_p = 1'b0;
	check_p = S_implicit10_p == S_explicit10_p;

	if (         S2  >         S3  ) S_implicit10_q = 1'b1; else S_implicit10_q = 1'b0;
	if ( $signed(S2) >         S3  ) S_explicit10_q = 1'b1; else S_explicit10_q = 1'b0;
	check_q = S_implicit10_q == S_explicit10_q;

	if (         S2  >         S3  ) S_implicit10_r = 1'b1; else S_implicit10_r = 1'b0;
	if ( $signed(U2) > $signed(U3) ) S_explicit10_r = 1'b1; else S_explicit10_r = 1'b0;
	check_r = S_implicit10_r == S_explicit10_r;

	if (         S2  >         S3  ) S_implicit10_s = 1'b1; else S_implicit10_s = 1'b0;
	if (         S2  > $signed(U3) ) S_explicit10_s = 1'b1; else S_explicit10_s = 1'b0;
	check_s = S_implicit10_s == S_explicit10_s;

	if (         S2  >         S3  ) S_implicit10_t = 1'b1; else S_implicit10_t = 1'b0;
	if ( $signed(U2) >         S3  ) S_explicit10_t = 1'b1; else S_explicit10_t = 1'b0;
	check_t = S_implicit10_t == S_explicit10_t;

	if (                   S2   >                   S3   ) S_implicit10_u = 1'b1; else S_implicit10_u = 1'b0;
	if ( $signed($unsigned(S2)) > $signed($unsigned(S3)) ) S_explicit10_u = 1'b1; else S_explicit10_u = 1'b0;
	check_u = S_implicit10_u == S_explicit10_u;

	if (                           S2    >                           S3   ) S_implicit10_v = 1'b1; else S_implicit10_v = 1'b0;
	if ( $signed($unsigned($signed(U2))) > $signed($unsigned($signed(S3)))) S_explicit10_v = 1'b1; else S_explicit10_v = 1'b0;
	check_v = S_implicit10_v == S_explicit10_v;
	



     end
endmodule


// nc says all outputs are 1
