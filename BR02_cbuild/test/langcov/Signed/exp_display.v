// Trivial test of ** operator semantics
// ALDEC and CARBON DIFFER IN ONLY 1 BIT when a is 0x0e when computing (32'd28 ** 8'sd14).
//   All other simulations are identical. [Weird!]

// ALDEC and NC disagree in 7 of 256 inputs, where NC gets ZERO for the U**S cases
// a==21, 17, 29, 14, 19 

module top(input c);
   
   always @(posedge c)
     begin: hack
        reg [31:0] o;
        reg [63:0] x;
        reg [7:0] i;
        realtime  r;
   
        for(i=0; i<20; i=i+1)
          begin: inner
             reg signed [7:0] j;
             r = r + 1.0;
             j = i;
             o = (j + i) ** i;
             x = (j + i) ** j;
             $display("i = %h (r**j)=%f (i+j)**i=%h round((i+j)**j)=%h", i, (r)**j, o, x);
          end
     end
endmodule
