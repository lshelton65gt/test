// last mod: Wed Jun 22 11:19:09 2005
// filename: test/langcov/Signed/binary_24.v
// Description:  This test checks signed and unsigned operands of <<<
// binary_24.sim.log.gold checked with aldec 

`define OP1 <<<
`define OP2 <<<
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"
