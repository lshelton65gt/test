// This testcase replicates bug6795, which ultimatey was that our signed 
// multiply function was broken.
//
// be sure to cover all cases of (S,S),(S,U),(U,S),(U,U), over several
// different sized ints

module top(clk, w32);
  input clk;
  output signed [31:0] w32;
    
  reg signed [31:0] u32, v32, w32;
  reg signed [63:0] u64, v64, w64;
  reg signed [127:0] u128, v128, w128;
  reg signed [126:0] u127, v127, w127;

  always @(posedge clk) begin
    u32 = -5;
    v32 = -5;
    w32 = u32 * v32;
    u64 = -5;
    v64 = -5;
    w64 = u64 * v64;
    u128 = -5;
    v128 = -5;
    w128 = u128 * v128;
    u127 = -5;
    v127 = -5;
    w127 = u127 * v127;
`ifdef CARBON
    $display("w32=%d, w64=%d, w127=%d, w128=%d",
             w32, w64, w127, w128);
`else
    $display("CARBON: w32=%d, w64=%d, w127=%d, w128=%d",
             w32, w64, w127, w128);
`endif
    u32 = -3;
    v32 = 6;
    w32 = u32 * v32;
    u64 = -3;
    v64 = 6;
    w64 = u64 * v64;
    u128 = -3;
    v128 = 6;
    w128 = u128 * v128;
    u127 = -3;
    v127 = 6;
    w127 = u127 * v127;
`ifdef CARBON
    $display("w32=%d, w64=%d, w127=%d, w128=%d",
             w32, w64, w127, w128);
`else
    $display("CARBON: w32=%d, w64=%d, w127=%d, w128=%d",
             w32, w64, w127, w128);
`endif
    u32 = 10;
    v32 = -4;
    w32 = u32 * v32;
    u64 = 10;
    v64 = -4;
    w64 = u64 * v64;
    u128 = 10;
    v128 = -4;
    w128 = u128 * v128;
    u127 = 10;
    v127 = -4;
    w127 = u127 * v127;
`ifdef CARBON
    $display("w32=%d, w64=%d, w127=%d, w128=%d",
             w32, w64, w127, w128);
`else
    $display("CARBON: w32=%d, w64=%d, w127=%d, w128=%d",
             w32, w64, w127, w128);
`endif
    u32 = 7;
    v32 = 9;
    w32 = u32 * v32;
    u64 = 7;
    v64 = 9;
    w64 = u64 * v64;
    u128 = 7;
    v128 = 9;
    w128 = u128 * v128;
    u127 = 7;
    v127 = 9;
    w127 = u127 * v127;
`ifdef CARBON
    $display("w32=%d, w64=%d, w127=%d, w128=%d",
             w32, w64, w127, w128);
`else
    $display("CARBON: w32=%d, w64=%d, w127=%d, w128=%d",
             w32, w64, w127, w128);
`endif
  end
endmodule
