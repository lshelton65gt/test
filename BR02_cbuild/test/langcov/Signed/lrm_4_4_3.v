// last mod: Tue Jun 14 11:07:22 2005
// filename: test/langcov/Signed/lrm_4_4_3.v
// Description:  This test was inspired by the example from LRM section 4.4.3
// gold files checked with aldec and nc

module lrm_4_4_3(output reg out);

   reg [3:0] a;
   reg [5:0] b;
   reg [15:0] c;

   reg signed [3:0] Sa;
   reg signed [5:0] Sb;
   reg signed [15:0] Sc;

   initial
     begin
	a = 4'hF;
	b = 6'ha;
	Sa = 4'hF;
	Sb = 6'ha;
	
	$display("a*b=%b", a*b); // expression size is self determined
	c = {a*b}; // expression a*b is self determined due to {}
	$display("a*b=%b", c);
	c = a*b; // expression size is determined by c
	$display("c  =%b", c);

	$display("Sa*Sb=%b", Sa*Sb); // expression size is self determined
	Sc = {Sa*Sb}; // expression a*b is self determined due to {}
	$display("Sa*Sb=%b", Sc);
	Sc = Sa*Sb; // expression size is determined by c
	$display("Sc   =%b", Sc);

	
	out = 1;
     end
endmodule

// expected output (NC):
//a*b=010110
//a*b=0000000000010110
//c  =0000000010010110
//Sa*Sb=110110
//Sa*Sb=0000000000110110
//Sc   =1111111111110110
//1
