// Test relational operator optimizations
// with version 1.5060 cbuild got some of this wrong bug6676

module small_signed_relop(a,b, o);
   parameter WIDTH=4;
   input signed [WIDTH-1:0]a;
   input signed [WIDTH-1:0]b;
   output signed  [0:5] o;

   assign     o[0]= a < b;
   assign     o[1]= a <= b;
   assign     o[2]= a > b;
   assign     o[3]= a >= b;
   assign     o[4]= a == b;
   assign     o[5]= a != b;
endmodule


module top(o);
   output [0:17] o;

   small_signed_relop #(4)  ss1 (4'b1111,               4'b1000,             o[0:5]);
   small_signed_relop #(4)  ss2 (4'b0111,               4'b1000,             o[6:11]);
   small_signed_relop #(16) ss3 (16'b1111111111111111, 16'b1000000000000000, o[12:17]);
   
endmodule
   
