// last mod: Tue Jan  3 06:29:09 2006
// filename: test/langcov/Signed/bug5490.v
// Description:  This test is from bug 5490

module bug5490(out, in1, in2);
   output [23:0] out;
   input [11:0]  in1, in2;

   reg [23:0] 	 out;
   
   function signed [23:0] f_signed_mult
      (input signed [11:0] data_0,
       input signed [11:0] data_1);

       begin
          f_signed_mult = data_0 * data_1;
       end
   endfunction

   always @(in1 or in2)
     out = f_signed_mult($signed(in1), $signed(in2));

endmodule
