// last mod: Tue Jun 14 08:24:21 2005
// filename: test/langcov/Signed/binary_1.v
// Description:  This test checks signed and unsigned operands of addition
// binary_1.sim.log.gold checked with NC.  [finsim and aldec disagree]

`define OP1 +
`define OP2 +
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"
