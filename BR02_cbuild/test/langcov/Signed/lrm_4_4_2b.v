// last mod: Tue Jul 12 11:25:46 2005
// filename: test/langcov/Signed/lrm_4_4_2_b.v
// Description:  This test was inspired by a discussion of section 4.4.2 of LRM
// concerning the sizing of operands
// gold file checked with aldec and NC


module lrm_4_4_2_b(input [14:0] in1, in2,
		   output reg [15:0] Uout1, Uout2, Uout3, Uout4,
		   output reg signed [15:0] Sout1, Sout2, Sout3, Sout4);

   reg signed [14:0] Sin1, Sin2;
   reg        [14:0] Uin1, Uin2;
   
   always @(*)
     begin: main
	Sin1 = in1;
	Sin2 = in2;
	Uin1 = in1;
	Uin2 = in2;

	Uout1 = (Uin1 + Uin2) >> 1; // size of RHS should be calculated in 16
				// (max (size(Uout1),size(Uin1),size(Uin2)))
				// bits, then logical shifted right so msb of
				// Uout1 is always 0
	Uout2 = (Uin1 + Uin2) >> 1'b1; // should be same as Uout1

	Uout3 = (Uin1 + Uin2) >>> 1;	// size of RHS should be calcuated in 16
				// bits, then arith shifted right, but since
				// operands are unsigned the msb is always 0
	Uout4 = (Uin1 + Uin2) >>> 1'b1; // should be same as Uout3

	Sout1 = (Sin1 + Sin2)>> 1;  // size of RHS should be calculated in 16
				// bits, then shifted right so msb is zero 
	Sout2 = (Sin1 + Sin2) >> 1'b1; // should be same as Sout1
	Sout3 = (Sin1 + Sin2) >>> 1;  // size of RSH should be calculated in 16
				// bits, then arith shifted right so the msb is
				// a duplicate of bit [15]
	Sout4 = (Sin1 + Sin2) >>> 1'b1; // should be same as Sout3

     end
endmodule
