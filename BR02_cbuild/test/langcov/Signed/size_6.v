// last mod: Fri Jul  1 14:10:35 2005
// filename: test/langcov/Signed/size_6.v
// Description:  This test assigns from integers to a long signed output
// Gold file checked with nc and aldec


module size_6(input signed [31:0] Sin1, Sin2,
	    output reg signed [65:0] Sout_a,Sout_b,Sout_c,Sout_d,Sout_e,Sout_f,Sout_g,Sout_h,Sout_i);

   integer 	   Int1;
   integer 	   Int2;

   wire [31:0] Uin1;
   wire [31:0] Uin2;

   assign      Uin1 = Int1;
   assign      Uin2 = Int2;
   
   
   always @(*)
     begin: main
	Int1 = Sin1;
	Int2 = Sin2;
	Sout_a = Int1;
	Sout_b = $signed(Int1);
	Sout_c = $signed(Uin1);
	
	Sout_d = Int2;
	Sout_e = $signed(Int2);
	Sout_f = $signed(Uin2);
	
	Sout_g = Int1 & Int2;
	Sout_h = $signed(Int1) & $signed(Int2);
	Sout_i = $signed(Uin1) & $signed(Uin2);

     end
endmodule
