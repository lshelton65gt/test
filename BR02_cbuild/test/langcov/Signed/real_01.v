// last mod: Wed Nov 15 15:45:36 2006
// filename: test/langcov/Signed/real_01.v
// Description:  This test checks for proper coercion of operands of ** to real


module real_01(clock, in1, in2, in3, out1, out2);
   input clock;
   input [2:0] in1;
   input [2:0] in2;
   input [1:0] in3;
   output [7:0] out1, out2;
   reg [7:0] 	out1, out2;

   always @(posedge clock)
     begin: main
	// in the following since one of the operands of ** is signed then the
	// type of the expression is real, the non-self determined operands are
	// coreced to real before they are sized, so we should see that out1 and
	// out2 are always equivalent
	out1 = (in1 + in2) ** $signed(in3);

	out2 = ($itor(in1) + $itor(in2)) ** $signed(in3);
     end
endmodule
