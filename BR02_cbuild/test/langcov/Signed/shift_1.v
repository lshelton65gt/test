// last mod: Thu Jul 28 14:20:26 2005
// filename: test/langcov/Signed/shift_1.v
// Description:  This test demonstrates that with version 2004.12 aldec gets the
// shift results wrong,  With 2005.04 aldec gets the correct results.
// carbon and nc get the same results.  Gold files from nc.


module shift_1(input signed [2:0] s3,
	       input signed [3:0] s4,
	       output [9:0] r1,
	       output [9:0] r2);
   wire  [3:0] u4;
   assign u4 = s4;
   assign r1 = s3 >>> s4;
   assign r2 = s3 >> u4;
   
endmodule
