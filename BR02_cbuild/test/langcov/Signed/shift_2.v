// last mod: Mon Aug  1 11:27:48 2005
// filename: test/langcov/Signed/shift_2.v
// Description:  This test tests the shifting of signed values, and intermediate
// results.  Aldec and NC agree on the results.  At one time carbon got wrong
// answer because cheetah provides the wrong signed-ness for $signed(c) 
// this is/was interra bug 9348, bugzilla bug 4813


// test verilog shift optimizations
module shift_2(a,b,c,x);
   input [2:0] a,b;
   input [31:0] c;
   output  x;

   assign       x = ^(($signed(c) >>> a) >>> b);
endmodule
