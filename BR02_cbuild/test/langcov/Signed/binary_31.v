// last mod: Tue Sep  6 16:39:58 2005
// filename: test/langcov/Signed/binary_28.v
// Description:  In this test we have 32bits = 128bits + 32bits + 32bits


`define OP1 +
`define OP2 +
`define SIZE1 128
`define SIZE2 32
`define SIZE3 32
`define SIZE4 128

`include "binary.v"
