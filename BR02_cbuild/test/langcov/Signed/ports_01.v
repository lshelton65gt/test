// last mod: Fri Jun 30 14:34:57 2006
// filename: test/langcov/Signed/ports_01.v
// Description:  This test checks that an assignment across a port does not
// carry the signed property with it.


module ports_01(clock, in1, in2, out1, out2, sout1, sout2);
   input clock;
   input [15:0] in1, in2;
   output [15:0] out1, out2;
   output signed [15:0] sout1, sout2;

   mid U1 ( clock, in1[7:0], in2[7:0], out1, out2, sout1, sout2);
endmodule

module mid(clock, in1, sin2, outa, souta, outb, soutb);
   input clock;
   input [7:0] in1;
   input signed [7:0] sin2;
   output [7:0] outa, outb;
   reg [7:0] outa, outb;
   output signed [7:0] souta, soutb;
   reg signed [7:0] souta, soutb;
   
   always @(posedge clock)
     begin: main
	outa = in1 + sin2;	// unsigned result
	souta = $signed(in1) + sin2; // signed result
	outb = in1 + sin2;	// unsigned result
	soutb = $signed(in1) + sin2; // signed result
     end
endmodule

