// last mod: Fri Jun 17 09:50:17 2005
// filename: test/langcov/Signed/binary_13.v
// Description:  This test checks signed and unsigned operands of !=
// binary_13.sim.log.gold checked with aldec 

`define OP1 !=
`define OP2 !=
`define SIZE1 2
`define SIZE2 3
`define SIZE3 4
`define SIZE4 10

`include "binary.v"
