/* Test handling of delay values. */
module top(d, clk, en, i1, i2, o1, o2, o3, o4);
input d, clk, en, i1, i2;
output o2, o1, o3, o4;

reg q;
reg o1;

assign o2 = q;

always @(negedge clk)
  if (en) #0 q = d;

always
begin
 o1 <= #0 i1;
end

and #(2:3:4) inst1(o3, i1, i2);
and #(5) inst2(o4, i1, i2);

endmodule
