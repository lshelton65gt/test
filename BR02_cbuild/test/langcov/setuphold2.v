module foo(clk, in, out);
   input clk, in;
   output out;
   reg 	  out;

   wire   in_n;

   not n0(in_n, in);

   always @(posedge clk) begin
      //$display("CLOCK");
      out <= in_n;
   end

   specify
      specparam tSetup = 10,
     tHold = 15;
      
      $setup(in_n, posedge clk, tSetup);
      $hold(in_n, posedge clk, tHold);
      
   endspecify
endmodule // foo
