// from bug 324, 
// in the following specify block the "parallel connection" module path is defined by (d=>q)
// (section 14.2.5 of LRM).  Verilog requires that the source and dest of a parallel
// connection have the same number of bits.
// you could run this with +suppress+437
// note that without this suppression cheetah correctly reports an error:
// bug324.v:10: Error 20437:  Size mismatch between input and output port for parallel connect.
// cheetah parses specify blocks even if you think carbon should ignore them,
// but see bug 1697 for why specify blocks cannot be ignored.


module spec(d, q);
  input [3:0] d;
  output q;

  specify
    specparam FOO = 1;
    (d => q) = (FOO, FOO);
  endspecify

  assign q = d[0];

endmodule
