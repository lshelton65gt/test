// last mod: Fri Mar 18 15:19:50 2005
// filename: test/langcov/Disable/init1.v
// Description:  This test is a single initial block that disables part of
// itself, unlike init1.v there are no duplicated block names


module init1(clock, out1);
   input clock;
   output out1;
   reg 	  out1;

   initial
     begin: main1
	$display("A");
	begin: l2
	   $display("B");
	   begin: main2
	      $display("C");
	      out1 = 1;
	      disable main2;
	      $display("D");
	   end
	   $display("E");
	   disable main1;
	   $display("F");
	end
	$display("G");
     end
endmodule

// results aldec and nc
// A
// B
// C
// E
