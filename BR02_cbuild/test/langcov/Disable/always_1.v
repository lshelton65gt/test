// last mod: Wed Mar  8 13:49:08 2006
// filename: test/langcov/Disable/always_1.v
// Description:  This test has a disable statement in a combinational always
// block, the target is a different combinational block.  BUG 5754


module always_1(outa, outb, outc, outd, rega, regb, regc, regd);
   output outa, outb, outc, outd;
   input rega, regb, regc, regd;
   reg outa, outb, outc, outd;

   always @(regb or rega)
     begin : block1
	outb = regb;
	disable block2;
	outa = rega;
     end

   always @(regc or regd)
     begin : block2
	outc = regc;
	disable block1;
	outd = regd;
     end
   
endmodule
