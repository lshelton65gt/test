module double_nest(out, in1, in2);
  output [31:0] out;
  input [3:0]  in1, in2;
  reg [31:0]    out;

  integer       i, j;

  always @(in1 or in2)
  begin: b1
    out = 0;
    for (i = 0; i < 256; i = i + 1)
      begin: b2
        for (j = 0; j < 256; j = j + 1) begin
          if (j == in1)
            disable b1;
          out = out + 1;
          if (j == in2)
            disable b2;
          out = out + 1;
        end
      end
  end
endmodule
