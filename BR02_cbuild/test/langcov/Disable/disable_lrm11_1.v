// last mod: Wed Mar  9 17:22:16 2005
// filename: test/langcov/Disable/disable_lrm11_1.v
// Description:  This test is based on the LRM section 11 example 1.
// a local block disables itself, regc should be undriven


// Note, a Verilog simulator may give regc a value of 'x' but
// Carbon will say it's a 'z' because it's live and undriven by
// the model.  With -coercePorts it might become a bidirect, in
// which case the vectors may need to be regenerated

module disable_lrm11_1(clock, rega, regb, regc);
   input clock;
   input regb;
   output rega, regc;
   reg rega, regc;

   always @(posedge clock)
     begin : block_name
	rega = regb;
	disable block_name;
	regc = rega; // this assignment will never execute
     end

endmodule
