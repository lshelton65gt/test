// last mod: Wed Apr 13 14:37:39 2005
// filename: test/langcov/Disable/disable_task5.v
//
// Description:  Like disable_task2, but makes the two inputs
//               to task2() inverted from one another.  Carbon
//		 will run this testcase incorrectly at the moment,
//		 because we will abort task2 even when the target
//		 of the disable is not in the call-chain
//		 Unfortunately disable_task5 doesn't give us this error
//		 because the first time we hit task2 is from task1,
//		 and the second time we hit task2 we do not validate
//		 its break targets.  disable_task4 is similar but reports
//		 an appropriate error.


module disable_task5(clock, in, in2, out);
   input clock;
   input in, in2;
   output out;
   reg out;

   task task1;
      output out1;
      input in;

      reg temp1;
      begin
	 temp1 = ~in;
	 $display("this line from task1 should always be printed", in);
	 task2(out1, in, temp1);
	 $display("this line from task1 must never be printed", in);
      end
   endtask

   task task2;
      output out2;
      input in1;
      input in2;

      begin
	 $display("this line from task2 should always be printed", in2);
	 // disable the task that called the current task, 
	 // so the current task is also disabled,
         out2 = 1'b1;
	 if (in1 | in2)  // caller has set in2 = ~in1 
	   disable task1; 
	 $display("this line from task2 must never be printed", in2);
	 out2 = 1'b0; // this line must never be executed, so out is always 1
      end
   endtask
   
	 
   always @(posedge clock)
     begin: main
       task1(out, in);
       task2(out, in, ~in);
     end
endmodule
