// last mod: Wed Mar 16 13:51:20 2005
// filename: test/langcov/Disable/hier1.v
// Description:  This test disables a local always block but uses a hierarchical
// reference to that block.


module hier1(clock, in1, out);
   input clock;
   input in1;
   output out;

   mid i1(clock, in1, out);
endmodule

module mid (clock, in, out);
   input clock, in;
   output out;

   reg 	  temp;
   initial
     temp = 1;

   assign out = temp;
   always @(posedge clock)
     begin: blk_mid
	if (in)
	  disable hier1.i1.blk_mid; // disables a local block with a
                                    // hierarchical reference to that block.
	temp = in; // since temp starts at 0, and we only execute this line if
		   // in is zero, the output of this module will start at 1 and
		   // change (and stay at zero)
     end
endmodule
