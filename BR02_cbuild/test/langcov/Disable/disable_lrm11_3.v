// last mod: Wed Mar  9 17:28:58 2005
// filename: test/langcov/Disable/disable_lrm11_3.v
// Description:  This test is from the LRM chapter 11, example 3
// here a task has a disable that causes early exit from the task.


module disable_lrm11_3(clock, in1, out);
   input clock;
   input in1;
   output out;
   reg 	  out;
   
   task proc_a;
      output out;
      input a;
      reg  out;
      begin
	 $display("start task");

	 if (a == 0)
	   disable proc_a; // return if true

	 $display("in task, this line should only be seen if a is 1, here a is: %b",a);
	 out = a;
      end
   endtask

   always @(posedge clock)
     begin: main
	proc_a(out, in1);
     end
endmodule
