// last mod: Fri Jun  3 08:18:55 2005
// filename: test/langcov/Disable/init3.v
// Description:  This test is a single initial block that disables part of
// itself, this is like init1 but there are no duplicate names of blocks to
// confuse the issue.  Gold results are shown at end of this file


module init3(clock, out1);
   input clock;
   output out1;
   reg 	  out1;

   initial
     begin: main1
	$display("A");
	begin: l2
	   $display("B");
	   begin: main2
	      $display("C");
	      out1 = 1;
	      disable main2;
	      $display("D");
	   end
	   $display("E");
	   disable main2;      // disables 'main2' (a sibling) (see if it can find
			       // the proper name)
	   $display("F");
	end
	$display("G");
     end
endmodule

// these results have been verified with  aldec and nc, note F and G displayed
// A
// B
// C
// E
// F
// G
