module nested(clk, in, stop1, stop2, stop21,
              o1, o2, stopped1, stopped2, stopped21);
  input clk, in;
  input [3:0] stop1, stop2, stop21;
  output [15:0] o1, o2;
  output        stopped1, stopped2, stopped21;
  reg [15:0] o1, o2;
  reg        stopped1, stopped2, stopped21;

  integer    i, j;

  always @(posedge clk) begin
    o1 = 0;
    stopped1 = 1'b0;
    stopped2 = 1'b0;
    stopped21 = 1'b0;

    begin: loop1
      for (i = 0; i < 16; i = i + 1) begin
        case (i)
          stop1: begin
            stopped1 = 1'b1;
            disable loop1;
          end
        endcase

        begin: loop2
          for (j = 0; j < 16; j = j + 1) begin
            case (j)
              stop2: begin
                stopped2 = 1'b1;
                disable loop2;
              end
              stop21: begin
                stopped21 = 1'b1;
                disable loop1;
              end
            endcase
            o1 = o1 + 1;
          end
        end
      end
    end

    o2 = in;
  end
endmodule
