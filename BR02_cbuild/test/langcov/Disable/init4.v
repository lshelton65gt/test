// last mod: Fri Jun  3 08:14:18 2005
// filename: test/langcov/Disable/init4.v
// Description:  This test is a single initial block that disables part of
// itself, note that there are two blocks named 'main' and both are disabled at
// different points.  Gold results are shown at end of this file


module init4(clock, out1);
   input clock;
   output out1;
   reg 	  out1;

   initial
     begin: main              // upper-main
	$display("A");
	begin: l2
	   $display("B");
	   disable main;      // no-op in NC & Aldec, upper-main in Carbon
	   begin: main        // note the name 'main' is reused!
	      $display("C");
	      out1 = 1;
	      disable main;   // disables the lower 'main'
	      $display("D");
	   end
	   $display("E");
	   disable main;      // disables the upper 'main'
	   $display("F");
	end
	$display("G");
     end
endmodule

// these results have been verified with  aldec and nc, note F and G displayed
// A
// B
// C
// E
// F
// G
