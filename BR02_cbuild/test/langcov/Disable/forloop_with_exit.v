module forloop_with_exit(clk, in1, out1);
  input clk;
  input [7:0] in1;
  output [7:0] out1;
  reg [7:0]    out1;

  reg [7:0]      v;
  integer        i, j;

  always @(posedge clk)  begin
    v = 0;
    for (i = 0; i < 4; i = i + 1) begin: l1
      for (j = 0; j < 4; j = j + 1) begin: l2
        if (v == 8)
          disable l2;
        else
          v = v + j;
      end
      v = v + i;
    end
    out1 <= in1 + v;
  end
endmodule

