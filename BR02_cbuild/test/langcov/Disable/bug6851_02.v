// last mod: Tue Dec 19 11:24:14 2006
// filename: test/langcov/Disable/bug6851_02.v
// Description:  This test is a variation of bug6851_01
// The difference is that the block being disabled here is one level lower in
// block hierarhcy than what is seen in bug6851_01
// This is not supported because the disable is trying to disable a non parent block.


module bug6851_02 (ena1, ena2, OUT);
   input  ena1, ena2;
   output OUT;

   reg 	  Pm, Wm;
   reg 	  PM;

   wire   OUT =  (PM === 0) ? Wm : Pm;     

   always @(posedge OUT)
     if (ena1) begin: p0
	if ( ena2 ) begin: p1
	   Wm <= # 1 1'b0;
	end
     end


   always @(ena1) begin
      if (ena1 === 0) begin: p2
	if ( ena2 ) begin: p3
   	   disable p3;		// this shows that a nested block can be found
   	   disable p0.p1;	// this cannot be found because it is not a parent of the current stmt
	end
	end
   end
endmodule




