// last mod: Fri Jun  3 08:07:20 2005
// filename: test/langcov/Disable/function1.v
// Description:  This test has a nested set of functions, the bottom function
// disables the block of the bottom-level function in the call chain.


module function1(clock, sel, in1, out);
   input clock;
   input sel;
   input [7:0] in1;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 77;
     end

   function [7:0] fct_top;
      input [7:0] i1;
      input sel;
      begin: fblock_top
	 fct_top = fct_mid(i1, sel);
      end
   endfunction

   function [7:0] fct_mid;
      input [7:0] i1;
      input sel;
      begin: fblock_mid
	 fct_mid = fct_bot(i1, sel);
      end
   endfunction

   function [7:0] fct_bot;
      input [7:0] i1;
      input sel;
      begin: fblock_bot
	 if (sel)
	   disable fblock_bot;
	 fct_bot = i1;		// note this function sometimes does not make an
				// assignment to its output
      end
   endfunction
   
      
   always @(posedge clock)
     begin: main
	out = fct_top(in1, sel);
     end
endmodule
