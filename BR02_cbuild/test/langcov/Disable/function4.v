// last mod: Wed Mar 16 15:05:31 2005
// filename: test/langcov/Disable/function4.v
// Description:  This test has function that disables the block that surrounds
// the function call.  The LRM says
//     "In cases where a disable statement within a function disables a block or a
//      task that called the function, the behavior is undefined."



module function4(clock, sel, in1, out);
   input clock;
   input sel;
   input [7:0] in1;
   output [7:0] out;
   reg [7:0] 	out;

   initial
     begin
	out = 77;
     end

   function [7:0] fct_top;
      input [7:0] i1;
      input sel;
      begin: fblock_bot
	 if (sel)
	   disable main;
	 fct_top = i1;		// note this function sometimes does not make an
				// assignment to its output
      end
   endfunction
   
      
   always @(posedge clock)
     begin: main
	out = fct_top(in1, sel);
     end
endmodule
