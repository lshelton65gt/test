// last mod: Fri Mar 18 14:29:57 2005
// filename: test/langcov/Disable/for1.v
// Description:  This test contains a disable within a for loop that disables
// the surrounding always block.  at each clock out should increment by 15


module for1(clock, reset, out);
   input clock;
   input reset;
   output [31:0] out;
   reg [31:0] 	 out;
   integer 	 i;
   initial
     begin
	out = 0;
     end
   always @(posedge clock)
     begin: main
	begin: forblock
	   for (i = 0; i < 10; i = i + 1)
	     begin
		out = out + i;
		if (i > 4)
		  disable main;	// always block exit
		if (i > 5)
		   begin
		      disable forblock; // loop exit (never executed because always
		                        // block exit above happens first)
		      $display("1 this line should never be executed!");
		   end
	     end
	end

	// the following lines are never executed
	$display("2 this line should never be executed!");
	out = out + 100;
	disable main;
	out = out + 1000;
     end
endmodule
