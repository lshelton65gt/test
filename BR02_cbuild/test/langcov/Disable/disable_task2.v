// last mod: Mon Apr 11 16:39:25 2005
// filename: test/langcov/Disable/disable_task2.v
//
// Description:  This test has a nested set of tasks, where the lowest level
// disables the upper level task.  It is self checking in that there are
// $displays that clearly state they should never be seen in the output
// and out is always X.  This is like disable_task1 except here the lowest level
// task is enabled both via task1 and directly from the top.  in both cases
// task2 will disable task1.
//


module disable_task2(clock, in, in2, out1, out2);
   input clock;
   input in, in2;
   output out1, out2;
   reg out1, out2;

   task task1;
      output out;
      input in;

      reg temp1;
      begin
	 temp1 = ~in;
	 $display("this line from task1 should always be printed", in);
	 task2(out, in, temp1);
	 $display("this line from task1 must never be printed", in);
      end
   endtask

   task task2;
      output out;
      input in1;
      input in2;

      reg temp1;
      begin
	 $display("this line from task2 should always be printed", in2);
	 // disable the task that called the current task, 
	 // so the current task is also disabled,  
	 if (in1 | in2)  // caller has set in2 = ~in1
	    begin
	       disable task1;
	       disable task2;
	    end
	 $display("this line from task2 must never be printed", in2);
	 out = in1; // this line must never be executed, so out is always undriven
      end
   endtask
   
	 
   always @(posedge clock)
     begin: main
       task1(out1, in);
       task2(out2, in2, ~in2);
     end
endmodule
