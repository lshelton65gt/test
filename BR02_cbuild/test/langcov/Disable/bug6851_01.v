// last mod: Tue Dec 19 11:24:14 2006
// filename: test/langcov/Disable/bug6851_01.v
// Description:  This test is a trimmed down version of the testcase provided in
// bug6851.  The problem is that the disable refers to a block that is not
// within the parent hierarchy of the disable stmt.  This is not supported
// because the disable is trying to disable a non parent block.


module bug6851_01 (ena1, OUT);
   input  ena1;
   output OUT;

   reg 	  Pm, Wm;
   reg 	  PM;

   wire   OUT =  (PM === 0) ? Wm : Pm;     

   always @(posedge OUT)
     if (ena1) begin: p0
	Wm <= # 1 1'b0;
     end


   always @(ena1) begin
      if (ena1 === 0) 
	begin
	   disable p0;
	end
   end
endmodule




