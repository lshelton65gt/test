// There was some covered-set-clearing issues in BreakResynth.cxx
// which caused some regression issues when dead tasks that called
// other dead tasks caused an assertion failure
// This is like dead_task2.v but with the functions in the opposite
// order

module mybuf(a, b);
  input a;
  output b;

  assign b = a;

    function [31:0] crc32_24atm;
        input [31:0] 
            crc;
        input [23:0] 
            inp;
        integer 
            i;
        begin
            crc32_24atm = crc;
            for(i = 0; i < 24; i = i + 1)
                crc32_24atm = crc32_1(crc32_24atm, inp[23 - i]);
        end
    endfunction

    function [31:0] crc32_1;
        input [31:0] 
            crc;
        input 
            B;
        reg 
            C;
        begin
            C = crc[31] ^ B;
            crc32_1 = {crc[30:0], 1'b0} ^ ({32{C}} & 32'b100110000010001110110110111);
        end
    endfunction
endmodule
