// last mod: Wed Mar  9 17:25:29 2005
// filename: test/langcov/Disable/disable_lrm11_2.v
// Description:  This test is based on the LRM section 11 example 2
// a local block disables itself


module disable_lrm11_2(clock, a, out);
   input clock;
   input a;
   output [1:0] out;
   reg [1:0]    out;

   always @(posedge clock)
      begin
	 $display("step1");
	 begin : block_name
	    $display("step2");

            out = 2'b00;
	    if (a == 0)
	      disable block_name;
	
            out = 2'b01;
	    $display("step3, this should never be seen if a = 0, a is: %b", a);
	 end // end of named block
	 
	 // continue with code following named block
         out = 2'b10;
	 $display("step4");
      end
endmodule
