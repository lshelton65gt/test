// last mod: Fri Mar 18 14:40:36 2005
// filename: test/langcov/Disable/for3.v
// Description:  This test contains a disable within a for loop that disables
// the named block within the for loop.  This is a loop continue.
// At each clock 'out' should increment by 45


module for3(clock, reset, out);
   input clock;
   input reset;
   output [31:0] out;
   reg [31:0] 	 out;
   integer 	 i;
   initial
     begin
	out = 0;
     end
   always @(posedge clock)
     begin: main
	begin: forblock
	   for (i = 0; i < 10; i = i + 1)
	     begin:forloop
		out = out + i;
		if (i > 5)
		   begin
		      disable forloop; // loop continue
		      $display("1 this line should never be executed!");
		   end
	     end
	end

	if (i > 4)
	  disable main;	// always block exit
	
	// the following lines are never executed
	$display("2 this line should never be executed!");
	out = out + 100;
	disable main;
	out = out + 1000;
     end
endmodule
