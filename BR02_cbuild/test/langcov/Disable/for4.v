// last mod: Wed Sep 13 08:47:00 2006
// filename: test/langcov/Disable/for1.v
// Description:  This test contains a disable within a for loop that disables
// the surrounding always block.  at each clock out should increment by 21


module for4(clock, reset, out);
   input clock;
   input reset;
   output [31:0] out;
   reg [31:0] 	 out;
   integer 	 i;
   initial
     begin
	out = 0;
     end
   always @(posedge clock)
      begin: forblock
         for (i = 0; i < 10; i = i + 1)
           begin
              out = out + i;
              if (i > 5)
                 begin
                    disable forblock; // loop exit (never executed because always
                                      // block exit above happens first)
                    $display("1 this line should never be executed!");
                 end
           end
      end
endmodule
