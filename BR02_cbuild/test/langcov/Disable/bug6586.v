// This is Josh's simplified version of MarkK's bug testcase, where
// the bogus results are visible on a primary output so it works with
// testdriver.  I also left test/bugs/bug6586 enabled in
// test/bugs/test.run.group6500

module bug(clk, cmp, loopvar, accum);
  input clk;
  input [3:0] cmp;
  output [4:0] loopvar;
  output [5:0] accum;
  reg [4:0]    loopvar;
  reg [5:0]    accum;

  always @(posedge clk) begin : loop
    accum = 0;
    for (loopvar = 0; loopvar < 16; loopvar = loopvar + 1) begin
      if (loopvar == cmp)
        disable loop;
      accum = accum + cmp;
    end
  end
endmodule
