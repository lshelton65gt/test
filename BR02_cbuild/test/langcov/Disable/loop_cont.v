module double_nest(out, in1);
  output [31:0] out;
  input [7:0]  in1;
  reg [31:0]    out;

  integer       i;

  always @(in1) begin
    out = 0;
    for (i = 0; i < 256; i = i + 1)
      begin: cont
        out = out + 1;
        if (i == in1)
          disable cont;
        out = out + 1;
      end
  end
endmodule
