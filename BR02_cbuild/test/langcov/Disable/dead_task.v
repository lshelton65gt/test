module m(i, o, clk);
  input i, clk;
  output o;
  reg    o;

  // This function is not called from anywhere, but it needs
  // to be resynthesized anyway
   function [7:0] f1;

      input [66-1:0]     in0;
      input [66-1:0]     in1;
      integer                   i;
      begin: loop
         f1 = 65;
         for (i=65; i>=0; i=i-1)
           begin
              if (in0[i] != in1[i])
		begin
                   f1 = i;
                   disable loop;
		end
           end
      end
   endfunction

  // put a flop in just so testdriver has something to do and doesn't warn
  always @(posedge clk)
    o <= i;
endmodule
