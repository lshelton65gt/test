// last mod: Wed Apr 13 14:36:54 2005
// filename: test/langcov/Disable/disable_task4.v
//
// Description:  Like disable_task2, but makes the two inputs
//               to task2() inverted from one another.  Carbon
//		 will error out on this testcase because the
//		 only call to task2 references a task that is
//		 not in the call-chain.  Since we don't want the
//		 overhead of detecting which tasks are in the call-chain
//		 then I think this is appropriate behavior for us.
//		 Unfortunately disable_task5 doesn't give us this error
//		 because the first time we hit task2 is from task1,
//		 and the second time we hit task2 we do not validate
//		 its break targets.


module disable_task4(clock, in, in2, out);
   input clock;
   input in, in2;
   output out;
   reg out;

   task task1;
      output out1;
      input in;

      reg temp1;
      begin
	 temp1 = ~in;
	 $display("this line from task1 should always be printed", in);
	 task2(out1, in, temp1);
	 $display("this line from task1 must never be printed", in);
      end
   endtask

   task task2;
      output out2;
      input in1;
      input in2;

      begin
	 $display("this line from task2 should always be printed", in2);
	 // disable the task that called the current task, 
	 // so the current task is also disabled,
         out2 = 1'b1;
	 if (in1 | in2)  // caller has set in2 = ~in1 
	   disable task1; 
	 $display("this line from task2 must never be printed", in2);
	 out2 = 1'b0; // this line must never be executed, so out is always 1
      end
   endtask
   
	 
   always @(posedge clock)
     begin: main
       //task1(out, in);
       task2(out, in, ~in);
     end
endmodule
