// last mod: Mon Apr 11 17:24:34 2005
// filename: test/langcov/Disable/hier2.v
// Description:  This test disables a remote always block via a hierarchical
// reference to that block.  actually both bottom blocks disable the sister block


module hier2(clock, in1, in2, out1, out2);
   input clock;
   input in1, in2;
   output out1, out2;

   mid i1(clock, in1, in2, out1, out2);
endmodule


module mid(clock, in1, in2, out1, out2);
   input clock, in1, in2;
   output out1, out2;

   bottom1 inst1(clock, in1, out1);
   bottom2 inst2(clock, in2, out2);
   
endmodule


module bottom1 (clock, in, out);
   input clock, in;
   output out;

   reg 	  temp;
   initial
     temp = 1;

   assign out = temp;
   always @(posedge clock)
     begin: blk1_bottom
	if (in)
	  disable hier2.i1.inst2.blk2_bottom; // disables a block via hier ref
	temp = in; // since temp starts at 0, and we should only execute this
		   // line if in is zero (because we will be remotely disabled),
		   // the output of this module will start at 1 and change to
		   // zero and stay at zero
     end
endmodule

module bottom2 (clock, in, out);
   input clock, in;
   output out;

   reg 	  temp;
   initial
     temp = 1;

   assign out = temp;
   always @(posedge clock)
     begin: blk2_bottom
	if (in)
	  disable hier2.i1.inst1.blk1_bottom; // disables a block via hier ref
	temp = in; // since temp starts at 0, and we should only execute this
		   // line if in is zero (because we will be remotely disabled),
		   // the output of this module will start at 1 and change to
		   // zero and stay at zero
     end
endmodule
