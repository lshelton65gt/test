// last mod: Wed Sep 13 14:39:04 2006
// filename: test/langcov/Disable/for6.v
// Description:  This test checks to see that the loop variable calculations are
// done properly when there is a disable of the begin/end block within the loop


module for6(clock, reset, out);
   input clock;
   input reset;
   output [31:0] out;
   reg [31:0] 	 out;
   integer 	 i;
   initial
     begin
	out = 0;
     end
   always @(posedge clock)
     begin: main
	begin: forblock
	   for (i = 0; i < 10; i = i + 1)
	     begin:forloop
		if (i > 7)
		   begin
		      disable forblock;	// loop exit
		   end
		if (i > 5)
		   begin
		      disable forloop; // loop continue
		      $display("1 this line should never be executed!");
		   end
		out = out + i;
	     end
	end

        if (i == 8)
          begin
	     // we expect that i is exactly 8 when we get here
            $display("Correct: loop variable i is 8");
          end
	else
	   begin
            $display("Error: the loop variable i is %d (not the expected value of 8)", i);
	   end

	if (i > 4)
	  disable main;	// always block exit
	
	// the following lines are never executed
	$display("2 this line should never be executed!");
	out = out + 100;
	disable main;
	out = out + 1000;
     end
endmodule
