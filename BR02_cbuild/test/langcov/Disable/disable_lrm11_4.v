// last mod: Wed Mar  9 17:32:42 2005
// filename: test/langcov/Disable/disable_lrm11_4.v
// Description:  This test was inspired by the LRM chapter 11, example 4, since
// we do not support implicit state machines it was converted to a form we do support.
// a disable is used to implement a continue and a next for a FOR loop


module disable_lrm11_4(clock, count, n, val, a, b, out);
   input clock;
   input [3:0] n;
   input       a, b;
   input [7:0] count, val;
   output [7:0] out;

   reg [7:0] 	out;

   always @(posedge clock)
     begin : brk
	integer i;
	out = val;
	for (i = 0; i < n; i = i+1)
	  begin : cont
	     out = out+1;
	     if (a == 0) // "continue" loop
	       disable cont;
	     out = out+1;
	     if (a == b) // "break" from loop
	       disable brk;
	     out = out+1;
	  end
     end

endmodule

