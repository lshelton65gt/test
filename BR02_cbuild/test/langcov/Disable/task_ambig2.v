// Description:  This test tries to legalize task_ambig.v by giving
// a hierarchical path down from the module, which disambiguates it.
// This is legal Verilog but Carbon cannot handle it presently, and
// we give an error message.


module task_ambig(clock, in, in2, out);
   input clock;
   input in, in2;
   output out;
   reg out;

   task task2;
      output out;
      input in1;
      input in2;

      reg temp1;
      begin
	 $display("this line from task2 should always be printed", in2);
	 // disable the task that called the current task, 
	 // so the current task is also disabled,  
	 if (in1 | in2)  // caller has set in2 = ~in1 
	   disable outer1.target; 
	 $display("this line from task2 must never be printed", in2);
	 out = in1; // this line must never be executed, so out is always undriven
      end
   endtask

   always @(posedge clock)
     begin: outer1
       begin: target
         task2(out, in, in2);
       end
     end

   always @(posedge clock)
     begin: outer2
       begin: target
         task2(out, in, in2);
       end
     end
endmodule
