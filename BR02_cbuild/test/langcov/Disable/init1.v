// last mod: Fri Mar 18 15:36:07 2005
// filename: test/langcov/Disable/init1.v
// Description:  This test is a single initial block that disables part of
// itself, note that there are two blocks named 'main' and both are disabled at
// different points.  Gold results are shown at end of this file


module init1(clock, out1);
   input clock;
   output out1;
   reg 	  out1;

   initial
     begin: main
	$display("A");
	begin: l2
	   $display("B");
	   begin: main        // note the name 'main' is reused!
	      $display("C");
	      out1 = 1;
	      disable main;   // disables the lower 'main'
	      $display("D");
	   end
	   $display("E");
	   disable main;      // disables the upper 'main'
	   $display("F");
	end
	$display("G");
     end
endmodule

// these results have been verified with  aldec and nc, note F and G displayed
// A
// B
// C
// E
// F
// G
