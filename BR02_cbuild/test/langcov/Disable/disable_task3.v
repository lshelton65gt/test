// last mod: Fri Jun  3 08:00:40 2005
// filename: test/langcov/Disable/disable_task3.v
//
// Description:  This test has a nested set of tasks, where the lowest level
// disables the upper level task.  It is self checking in that there are
// $displays that clearly state they should never be seen in the output
// and out is always X.
//      like disable_task1 but the disable stmt is not a task but for a block in
//      the task that enabled the lower level task  (currently fails) 
//


module disable_task3(clock, in, in2, out1, out2);
   input clock;
   input in, in2;
   output out1, out2;
   reg out1, out2;

   task task1a;
      output out;
      input in;

      reg temp1;
      begin: b1
	 temp1 = ~in;
	 $display("this line from task1a should always be printed", in);
	 task2(out, in, temp1);
	 $display("this line from task1a must never be printed", in);
      end
   endtask

   task task1b;
      output out;
      input in;

      reg temp1;
      begin: b1
	 temp1 = ~in;
	 $display("this line from task1b should always be printed", in);
	 task2(out, in, temp1);
	 $display("this line from task1b must never be printed in:%b temp1:%b", in, temp1);
      end
   endtask

   task task2;
      output out;
      input in1;
      input in2;

      reg temp1;
      begin
	 $display("this line from task2 should always be printed", in2);
	 // disable the block within the task that called the current task, 
	 // so the current task is also disabled,  
	 if (in1 | in2)  // caller has set in2 = ~in1
           begin
	      disable task1a.b1;
              disable task1b.b1;
           end
	 $display("this line from task2 must never be printed. in1:%b in2:%b", in1, in2);
	 out = in1; // this line must never be executed, so out is always undriven
      end
   endtask
   
	 
   always @(posedge clock)
     begin
       task1a(out1, in);
       task1b(out2, in);
     end
endmodule
