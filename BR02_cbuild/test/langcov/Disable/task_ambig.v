// Description:  This test tries to create an ambiguous 'disable' in
// a subtask that reaches up to the blcok enclosing the task enable.
// We tried to use nested blocks to create an ambiguous inner block
// name.  But according to Cheetah this is not legal Verilog and we
// do not parse it successfully.  You can't specify a nested block
// from the task.  You have to specify the task hierarchically down
// from the module.


module task_ambig(clock, in, in2, out);
   input clock;
   input in, in2;
   output out;
   reg out;

   task task2;
      output out;
      input in1;
      input in2;

      reg temp1;
      begin
	 $display("this line from task2 should always be printed", in2);
	 // disable the task that called the current task, 
	 // so the current task is also disabled,  
	 if (in1 | in2)  // caller has set in2 = ~in1 
	   disable target; 
	 $display("this line from task2 must never be printed", in2);
	 out = in1; // this line must never be executed, so out is always undriven
      end
   endtask

   always @(posedge clock)
     begin: outer1
       begin: target
         task2(out, in, in2);
       end
     end

   always @(posedge clock)
     begin: outer2
       begin: target
         task2(out, in, in2);
       end
     end
endmodule
