// last mod: Wed Mar 16 14:29:14 2005
// filename: test/langcov/Disable/task5.v
// Description:  This test has a task that disables a block outside of the task,
// but within the same module.  The task is enabled from a different module via
// a hierarchical reference.


module task5(clock, sel, in1, out, out2);
   input clock;
   input sel;
   input [7:0] in1;
   output [7:0] out, out2;
   reg [7:0] out2;

   initial
     out2 = 0;
   
   task t1;
      output out;
      input sel;
      input in;

      begin: tb1
	 if ( sel ) 
           disable ab1;		// disable block outside of task
	 out = in;
      end
   endtask

   mid i1(out, sel, in1);

   always@(posedge clock)
     begin: ab1
        out2 = out2 +1;
     end
endmodule

module mid(out, sel, in);
   input sel;
   input [7:0] in;
   output [7:0] out;
   reg [7:0] out;

   always @(in)
      begin: ab2
	 t1(out[1],sel,in[1]); // enable a task defined in higher module
	 out[0] = 0;
	 out[7:2] = 0;
      end
endmodule
