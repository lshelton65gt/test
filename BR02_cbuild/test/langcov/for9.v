module top ( out1, out2, in, clk );
   input clk;
   input [7:0] in;
   output [7:0] out1;
   reg [7:0] 	out1;
   output [7:0] out2;
   reg [7:0] 	out2;

   integer 	i;
   
   always @(posedge clk)
     begin
	out1[0] <= in[7];
	for ( i = 1 ; i < 7 ; i = i + 1 )
	  begin
	     out2[i] = in[i];
	  end
	out1[7] <= in[0];
     end
endmodule // top
