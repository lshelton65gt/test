// Test case stmts folding down to defaults
module top(in1, in2, in3, inx, out1, out2, out3, outx, clk);
   input in1, in2, in3, inx, clk;
   output out1, out2, out3, outx;
   bottom b0 (3'b000, in1, in2, in3, inx, out1, out2, out3, outx, clk);
   bottom b1 (3'b010, in1, in2, in3, inx, out1, out2, out3, outx, clk);
   bottom b2 (3'b100, in1, in2, in3, inx, out1, out2, out3, outx, clk);
   bottom b3 (3'b001, in1, in2, in3, inx, out1, out2, out3, outx, clk);
endmodule


module bottom(sel, in1, in2, in3, inx, out1, out2, out3, outx, clk);
   input in1, in2, in3, inx, clk;
   output out1, out2, out3, outx;
   reg 	  out1, out2, out3, outx;
   input [2:0] sel;
   always @(posedge clk)
     case (sel)
       3'b000: out1 = in1;
       3'b010: out2 = in2;
       3'b100: out3 = in3;
       default: outx = inx;
     endcase
endmodule
