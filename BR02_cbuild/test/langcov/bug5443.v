// last mod: Mon Dec 19 10:17:48 2005
// filename: test/langcov/bug5443.v
// Description:  This test once had an error in cheetah because the infer_mux
// directive mentioned "mux" which was both a block and a module name.


module bug5443(s1, s2, in0, in1, in2, out1);
   input s1, s2;
   input [1:0] in0, in1, in2;
   output [1:0] out1;

   muxwrap #(2) u1 (out1, in0, in1, in2, s1, s2);
endmodule

module muxwrap(o, d0, d1, d2, s0, s1);
parameter size = 1;
output [size-1:0] o;
input [size-1:0] d0, d1, d2;
input s0, s1;
    wire [1:0] s = s0 ? 2'd0 : s1 ? 2'd1 : 2'd2;
    mux #(size) i0(o, d0, d1, d2, s);
endmodule


module mux(o, d0, d1, d2, s);
   parameter size = 32;

   output [size-1:0] o;
   input [size-1:0]  d0;
   input [size-1:0]  d1;
   input [size-1:0]  d2;
   input [1:0] 	     s;
   reg [size-1:0]    o;


   // synopsys infer_mux "mux"
   always @(s or d0 or d1 or d2) begin: mux
      case (s)		
	2'b00:
	  o = d0;
	2'b01:
	  o = d1;
	2'b10:
	  o = d2;
	default:
	  o = {size{1'bx}};
      endcase
   end

endmodule

