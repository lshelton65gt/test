// last mod: Thu Jun 24 12:13:08 2004
// filename: test/langcov/Case/bug2324b.v
// Description:  This test from bug2324, a casestmt that could have a simpler
// implementation. This is just a decode of the sel input. (alternative is show
// here also)


module bug2324b(sel, out1, out2, ok);
   input [3:0] sel;
   output [15:0] out1, out2;
   output 	 ok;
   reg [15:0] 	 out1, out2;

   always @(sel)
     begin
	case (sel)		// mindspeed implementation
	  4'b0:
	    out1 = 16'b1;
	  4'b1:
	    out1 = 16'h0002;
	  4'h2:
	    out1 = 16'h0004;
	  4'h3:
	    out1 = 16'h0008;
	  4'h4:
	    out1 = 16'h0010;
	  4'h5:
	    out1 = 16'h0020;
	  4'h6:
	    out1 = 16'h0040;
	  4'h7:
	    out1 = 16'h0080;
	  4'h8:
	    out1 = 16'h0100;
	  4'h9:
	    out1 = 16'h0200;
	  4'ha:
	    out1 = 16'h0400;
	  4'hb:
	    out1 = 16'h0800;
	  4'hc:
	    out1 = 16'h1000;
	  4'hd:
	    out1 = 16'h2000;
	  4'he:
	    out1 = 16'h4000;
	  4'hf:
	    out1 = 16'b1000000000000000;
	endcase
     end


   always @(sel) begin
      out2 = 1<<sel;		// alternative implementation
   end

   assign
     ok = out1 == out2;		// this should always be true

endmodule
