// simple case stmt, some branches of case are explicit, some covered by
// default,  case tags are integers, selector is 4 bits
// author: josh
// last mod: 06/22/04

module casestmt(sel, i0, i1, i2, i3, i4, def, out);
  input [3:0] sel, i0, i1, i2, i3, i4, def;
  output [3:0] out;
  reg [3:0]    out;
  always @(sel or i0 or i1 or i2 or i3 or i4 or def)
    case (sel)
      0: out = i0;
      1: out = i1;
      2: out = i2;
      3: out = i3;
      4: out = i4;
      default: out = def;
    endcase
endmodule
