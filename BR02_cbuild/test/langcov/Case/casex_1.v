// casex with small (2 bit) selector, output is simply:  the decimal value of
// the most significant bit of the selector.
// test: test/langcov/Case/casex_1.v
// author: cloutier
// last mod: 06/22/04

module casex_1(sel, out);
  input [3:0] sel;
  output [7:0] out;
  reg [7:0]    out;

  always @(sel)
     casex(sel)   	// synopsys full_case parallel_case
          4'b1???: out =  8;
          4'b01??: out =  4;
          4'b001?: out =  2;
          4'b0001: out =  1;
          4'b0000: out =  0;
     endcase
endmodule
