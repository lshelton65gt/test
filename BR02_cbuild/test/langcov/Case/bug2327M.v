// last mod: Thu Jun 24 14:58:08 2004
// filename: test/langcov/Case/bug2327M.v
// Description:  This test is from bug 2327, a medium fully specified case, with constant RHS


module bug2327(clock, sel, out);
   input clock;
   input [2:0] sel;
   output [15:0] out;
   reg [15:0] 	 out;

always @(posedge clock)
  begin: main
     case (sel)
       3'b000: out = 1234;
       3'b001: out = 9384;
       3'b010: out = 2498;
       3'b011: out = 0933;

       3'b100: out = 0123;
       3'b101: out = 0938;
       3'b110: out = 0249;
       3'b111: out = 0093;
     endcase
  end
endmodule
