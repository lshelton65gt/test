// last mod: Fri Dec  8 11:02:41 2006
// filename: test/langcov/Case/casecat_04.v
// Description:  This test contains a case statement, the
// casestmt has full_case specified, but the case labels are integers, and one
// possible case label is not specified (0), the question is: is the full_case property maintined?
// The size of the integer tags should not cause the case to lose its fully
// specified property.  In addition we should not generate code to check that
// {sel1,sel0} > 3, it never can be
// gold file from aldec/nc

module casescat_04(sel1, sel0, idat, out);
   input [3:0] idat;
   input       sel1, sel0;
   wire       sel1_in, sel0_in;
   output [3:0] out;
   reg [3:0] 	out_temp;

   // if input is out of range, then change to don't care value
   assign 	out = (! sel1 && !sel0 ) ? 2'bxx : out_temp;
   
   always @(sel1 or sel0 or idat)
    case ({sel1,sel0}) // carbon full_case
      1: out_temp = idat + 5;
      2: out_temp = idat + 7;
      3: out_temp = idat + 11;
    endcase
endmodule
