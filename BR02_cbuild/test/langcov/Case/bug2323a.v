// last mod: Tue Jul  6 17:18:49 2004
// filename: test/langcov/Case/bug2323a.v
// Description:  This test inspired by exmaple from mindspeed where a casestmt is
// being used to implement a one hot detector (but in this case the  RHS are not
// all the same)
//    you might want a  parallel_case directive here although it does not appear
//    to be necessary.

module bug2323a(sel, in0, in1, in2, in3, in4, in5, out);
   input [5:0] sel;
   input in0, in1, in2, in3, in4, in5;
   output out;
   reg 	  out;

   always @(sel or in0 or in1 or in2 or in3 or in4 or in5)

     begin
	case (sel)
	  6'b1:
	    out = in0;
	  6'd2:
	    out = in1;
	  6'd4:
	    out = in2;
	  6'd8:
	    out = in3;
	  6'd16:
	    out = in4;
	  6'd32:
	    out = in5;
	  default:
	    out = 0;
	endcase
   end

endmodule
