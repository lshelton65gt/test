// case stmt with a concat of 2 bits for the selector expression, case tags are
// integers.  Setup so that case stmt is fully specified, and that there is one
// branch that is never possible

// author: cloutier
// last mod: Fri Dec  1 13:35:33 2006

module casecat_03(sel1, sel0, idat, out);
   input [3:0] idat;
   input       sel1, sel0;
   output [3:0] out;
   reg [3:0] 	out;
  always @(sel1 or sel0 or idat)
    case ({sel1,sel0})
      2'b01: out = idat + 1;
      2'b10: out = idat + 3;
      2'b11: out = idat + 5;
      2'b00: out = idat + 11;
      3'b100: out = idat + 19;	// this can never be selected
    endcase
endmodule
