// last mod: Tue Jul  6 17:24:08 2004
// filename: test/langcov/Case/bug2323b.v
// Description:  This test inspired by mindspeed, has a case stmt with a
// selector that is used as a one-hot.  The RHS of the case branches select bits
// from a vector where the bitposition of the single bit in the case label is
// the index in the RHS array.


module bug2323b(sel, in, out);
   input [5:0] sel;
   input [5:0] in;
   output out;
   reg 	  out;


   always @(sel or in)
     begin : in_mux5
	case (sel)
	  6'b1: begin
	     out = in[0];
	  end
	  6'd2: begin
	     out = in[1];
	  end
	  6'd4: begin
	     out = in[2];
	  end
	  6'd8: begin
	     out = in[3];
	  end
	  6'd16: begin
	     out = in[4];
	  end
	  6'd32: begin
	     out = in[5];
	  end
	  default: begin
	     out = 0;
	  end
	endcase
     end
	
endmodule
