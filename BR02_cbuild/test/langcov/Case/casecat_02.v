// case stmt with a concat of 2 bits for the selector expression, case tags are
// integers.  Testcase is designed so that case stmt is fully specified, and
// that there is one branch that is never possible

// author: cloutier
// last mod: Wed Dec  6 09:48:01 2006

module casestmt_02(sel1, sel0, idat, out);
   input [3:0] idat;
   input       sel1, sel0;
   output [3:0] out;
   reg [3:0] 	out;
  always @(sel1 or sel0 or idat)
    case ({sel1,sel0}) // carbon full_case
      1: out = idat + 1;
      2: out = idat + 3;
      3: out = idat + 5;
      4: out = idat + 7;	// this branch can never be selected
      default: out = idat + 11;	// this branch is for {sel1,sel0}==0
    endcase
endmodule
