// November 2008
// The issue is with case statement with don't cares. CBUILD moves around the case
// items, which causes simulation mismatch. Verilog LRM says that case items
// should be handled in the same sequence, as they appear in the design.

module bug11668_01(sel, in1, in2, out1);
   input[19:0] sel;
   input[20:0] in1;
   input[20:0] in2;
   output[20:0] out1;
   
function [1:0] vlc_decode_def;

      input [19:0] code;
      input [20:0] bsscmk;
      input [20:0] bssemk;

      begin

  casez(code[18:3])

   16'b1???????????????:  vlc_decode_def={bsscmk[1],bssemk[1]};  
   16'b0001????????????:  vlc_decode_def={bsscmk[1],bssemk[1]};  
   16'b0???????????????:  vlc_decode_def={bsscmk[4],bssemk[4]};  
   16'b0011????????????:  vlc_decode_def={bsscmk[1],bssemk[1]};  
   16'b0101????????????:  vlc_decode_def={bsscmk[1],bssemk[1]}; 
   16'b0111????????????:  vlc_decode_def={bsscmk[1],bssemk[1]};  

   default:		   vlc_decode_def={1'bx,1'bx};

  endcase

      end

endfunction // vlc_decode

   assign out1 = vlc_decode_def(sel,in1[5:0],in2[5:0]);

endmodule
