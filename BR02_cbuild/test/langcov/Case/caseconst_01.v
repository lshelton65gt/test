// last mod: Mon Aug  6 07:05:53 2007
// filename: test/langcov/Case/caseconst_01.v
// Description:  This test is for an example found while working on bug 7364, a
// case stmt with a constant select expression that was zero. Here we test zero
// one and two

module caseconst_01 (out3, out4, out5, out6, clk, in1, in2, in3, in4);
   output [3:0] out3, out4, out5, out6;
   input 	clk;
   input [3:0] 	in1, in2, in3, in4;
   reg [3:0] cout3, out3;
   reg [3:0] 	rin1, rin2, rin3, rin4;
   reg [3:0] cout4, cout5, out4, out5;
   reg [3:0] cout6, out6;

   initial
     begin
	out3 = 0;
	out4 = 0;
	out5 = 0;
	out6 = 0;
	cout3 = 0;
	cout4 = 0;
	cout5 = 0;
	cout6 = 0;
	rin1 = 0;
	rin2 = 0;
	rin3 = 0;
	rin4 = 0;

     end
   // Flop the inputs
   always @ (posedge clk)
     begin
	rin1 = in1;
	rin2 = in2;
	rin3 = in3;
	rin4 = in4;
     end

   
   // Case with constant select expression of zero
   always @ (rin1 or rin3 or rin4)
     begin
	case (2'b0)
	  2'b00: cout3 = rin1;
	  2'b10: cout3 = rin3;
	  default: cout3 = rin4;
	endcase
     end
   always @ (posedge clk)
     out3 = cout3;
   
   // Case with constant select expression of one
   always @ ( rin1 or rin2 or rin3 or rin4)
     begin
	case (2'b01)
	  2'b00:
	    begin
	       cout4 = rin1;
	       cout5 = cout4;
	    end
	  2'b01:
	    begin
	       cout4 = rin2;
	       cout5 = cout4;
	    end
	  2'b10:
	    begin
	       cout4 = rin3;
	       cout5 = cout4;
	    end
	  2'b11:
	    begin
	       cout4 = rin4;
	       cout5 = cout4;
	    end
	endcase
     end
   always @ (posedge clk)
     begin
	out4 = cout4;
	out5 = cout5;
     end

   // case with constant select expression of two
   always @ (rin1 or rin2 or rin3 or rin4)
     begin
	case (2'b10)
	  2'b00:
	    begin
	       cout6 = rin1;
	    end
	  2'b01:
	    begin
	       cout6 = rin2;
	    end
	  2'b10:
	    begin
	       cout6 = rin3;
	    end
	  2'b11:
	    begin
	       cout6 = rin4;
	    end
	endcase
     end
   always @ (posedge clk)
     out6 = cout6;

endmodule // top

