// last mod: Thu Jun 24 11:57:34 2004
// filename: test/langcov/Case/bug2324a.v
// Description:  This test is from bug2324, a case statement that is used to
// generate a mask, the function transformation is straightforward (like a
// simple shift)  mask = ~( 4'b1111 << x[1:0] );


module bug2324a(clock, x, mask1, mask2);
   input clock;
   input [3:0] x;
   output [2:0] mask1, mask2;
   reg [2:0] 	mask1, mask2;

   always @(posedge clock)
     begin
	case (x[1:0])
	  0: mask1 = 3'b0;
	  1: mask1 = 3'b1;
	  2: mask1 = 3'b11;
	  3: mask1 = 3'b111;
	endcase 
     end
   
   always @(posedge clock)
     begin
	mask2 = ~( 4'b1111 << x[1:0] );
     end
endmodule
