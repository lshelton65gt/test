// last mod: original file
// filename: test/langcov/Case/bug2328.v
// Description:  This test is from bug 2328, a case statement used to implement
//               rotation of a vector.


module bug2328(clock, i, cnt, o);
   input clock;
   input [3:0] i;
   input [1:0] cnt;
   output [3:0] o;
   reg [3:0] 	o;

always @(posedge clock)
  begin: main
     case (cnt)
       0: o = i;
       1: o = {i[0], i[3], i[2], i[1]};
       2: o = {i[1], i[0], i[3], i[2]};
       3: o = {i[2], i[1], i[0], i[3]};
     endcase
  end
endmodule
