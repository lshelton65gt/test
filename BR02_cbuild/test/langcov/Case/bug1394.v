// last mod: Wed Jun  1 17:13:21 2005
// filename: test/langcov/Case/bug1394.v
// Description:  This test is similar to designs we found in mindspeed, where we
// should recognize the casex stmt as a priority encoding and not compare all
// values in order to detect that no bits are set in pri


module bug1394(clock, pri, out);
   input        clock;
   input [3:0]  pri;
   output [2:0] out;
   reg    [2:0] out;

always @(posedge clock)
  begin: main

     out = 3;
     casex(pri)
       4'bxxx1: out = 0;
       4'bxx10: out = 1;
       4'bx100: out = 1;
       4'b1000: out = 2;
     endcase

 
  end
endmodule
