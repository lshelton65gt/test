// last mod: Fri Dec  8 10:54:37 2006
// filename: test/langcov/Case/casecat_05.v
// Description:  This test contains a case statement, the
// casestmt DOES NOT have full_case specified, but the case labels are integers, the
// question is: can this be correctly recognized as a fully specified case stmt?
// The size of the integer tags should not prohibit the recognition of the fully
// specifed case   In addition we should not generate code to check that
// {sel1,sel0} > 3, it never can be.


module casescat_05(sel1, sel0, idat, out);
   input [3:0] idat;
   input       sel1, sel0;
   output [3:0] out;
   reg [3:0] 	out;
   always @(sel1 or sel0 or idat)
    case ({sel1,sel0})
      0: out = idat + 3;
      1: out = idat + 5;
      2: out = idat + 7;
      3: out = idat + 11;
    endcase
endmodule
