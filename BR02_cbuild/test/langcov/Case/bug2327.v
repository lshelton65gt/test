// last mod: original file
// filename: test/langcov/Case/bug2327.v
// Description:  This test is from bug 2327, fully specified case, with constant RHS


module bug2327(clock, foo, out);
   input clock;
   input [1:0] foo;
   output [15:0] out;
   reg [15:0] 	 out;

always @(posedge clock)
  begin: main
     case (foo)
       2'b00: out = 1234;
       2'b01: out = 9384;
       2'b10: out = 2498;
       2'b11: out = 0933;
     endcase
  end
endmodule
