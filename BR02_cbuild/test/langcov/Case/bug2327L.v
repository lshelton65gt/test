// last mod: original file
// filename: test/langcov/Case/bug2327L.v
// Description:  This test is from bug 2327, a large fully specified case, with
// constant RHS stmts


module bug2327(clock, sel, out);
   input clock;
   input [3:0] sel;
   output [15:0] out;
   reg [15:0] 	 out;

always @(posedge clock)
  begin: main
     case (sel)
       4'b0000: out = 1234;
       4'b0001: out = 9384;
       4'b0010: out = 2498;
       4'b0011: out = 0933;
       
       4'b0100: out = 0123;
       4'b0101: out = 0938;
       4'b0110: out = 0249;
       4'b0111: out = 0093;
       
       4'b1000: out = 0012;
       4'b1001: out = 0093;
       4'b1010: out = 0024;
       4'b1011: out = 0009;

       4'b1100: out = 0001;
       4'b1101: out = 0009;
       4'b1110: out = 0002;
       4'b1111: out = 0000;
     endcase
  end
endmodule
