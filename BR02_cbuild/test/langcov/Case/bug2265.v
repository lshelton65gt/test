// last mod: original file
// filename: test/langcov/Case/bug2265.v
// Description:  This test from bug 2265, where a case stmt represents a simple expression.


module bug2265(clock, sel1,sel2,sel3, out1, out2, ok);
   input clock;
   input sel1, sel2, sel3;
   output out1, out2, ok;
   reg 	  out1, out2;

   always @(posedge clock)
     begin: main1		// original implementation
	case({sel1, sel2, sel3}) // synopsys full_case
          1,
          7,
          2,
          4:
            out1 = 1;         
          3,
          5,
          0,
          6:
            out1 = 0;       
	endcase
     end

   always @(posedge clock)
     begin: main2
	out2 = sel1 ^ sel2 ^ sel3; // simpler implementation
     end

   assign ok = out1 == out2;	// this should always be 1
endmodule
