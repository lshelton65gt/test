// last mod: Tue Jul  6 17:24:31 2004
// filename: test/langcov/Case/bug2323c.v
// Description:  Inspired by mindspeed, This test is a fullcase where selector
// is useds as one-hot-or-all-cold identifier, the RHS are all constants and for
// the one-hot section it is the bit position of the one-hot-bit, for the
// all-cold section (default) it is the constant 7.  This type of case stmt was
// seen in several places in Mindspeed.


module bug2323c(clock, sel, out);
   input clock;
   input [5:0] sel;
   output [3:0] out;
   reg [3:0] 	out;

   always @(posedge clock)
     begin: main
     	case (sel)
	  6'b1:
	    out =  3'b0;
	  6'd2:
	    out =  3'b1;
	  6'd4:
	    out =  3'h2;
	  6'd8:
	    out =  3'h3;
	  6'd16:
	    out =  3'b100;
	  6'd32:
	    out =  3'b101;
	  default:
	    out =  3'b111;
	endcase

     end
endmodule
