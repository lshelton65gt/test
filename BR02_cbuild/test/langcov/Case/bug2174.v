// last mod: original file
// filename: test/langcov/Case/bug2174.v
// Description:  This test shows how we generate code in the situation where the
// selector is scrambled, when it could have been implemented by using a simple
// vector for the selector and just reworking the case tags.  Using the scrambled
// selector requires 18 additional instructions to prepare the selector.


module bug2174(clock, sel, out1, out2, ok);
   input clock;
   input [3:0] sel;
   output      ok;
   output [3:0] out1, out2;
   reg [3:0] 	out1, out2;

   always @(posedge clock)
     begin: main1
	case ({sel[2],sel[0],sel[1],sel[3]})
	  4'b0000: out1 = 0;
	  4'b0001: out1 = 1;
	  4'b0010: out1 = 2;
	  4'b0011: out1 = 3;
	  4'b0100: out1 = 4;
	  4'b0101: out1 = 5;
	  4'b0110: out1 = 6;
	  4'b0111: out1 = 7;
	  4'b1000: out1 = 8;
	  4'b1001: out1 = 9;
	  4'b1010: out1 = 10;
	  4'b1011: out1 = 11;
	  4'b1100: out1 = 12;
	  4'b1101: out1 = 13;
	  4'b1110: out1 = 14;
	  4'b1111: out1 = 15;
	endcase
     end


       // here is a version of equivalent logic with the selector in simple form
       // and the case tags changed to match the selector order.
   always @(posedge clock)
     begin: main2
	case (sel)
	  4'b0000: out2 = 0;
	  4'b1000: out2 = 1;
	  4'b0010: out2 = 2;
	  4'b1010: out2 = 3;
	  4'b0001: out2 = 4;
	  4'b1001: out2 = 5;
	  4'b0011: out2 = 6;
	  4'b1011: out2 = 7;
	  4'b0100: out2 = 8;
	  4'b1100: out2 = 9;
	  4'b0110: out2 = 10;
	  4'b1110: out2 = 11;
	  4'b0101: out2 = 12;
	  4'b1101: out2 = 13;
	  4'b0111: out2 = 14;
	  4'b1111: out2 = 15;
	endcase
     end

   assign ok = out1 == out2;	// this should always be true

endmodule
