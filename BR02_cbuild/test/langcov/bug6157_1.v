// last mod: Wed Jun 14 11:23:28 2006
// filename: test/langcov/bug6157_1.v
// Description:  This test duplicates the problem that is described in bug6157
// the issue here is that if you have a hierarchical ref to an instance and that
// instance has a missing definition, and you change message 20391 to a warning,
// then you get a segfault.  You should also get an Error about an
// undefined module definition.
// this test is run with two directives
// warningMsg 20391
// warningMsg 43003
// warningMsg 20252
// 43003 tries to demote an error to a warning (which should fail)
// 20391 demotes an alert to a warning that with MVV-2004.1.18.b causes a segfault

// NOTE see also defparam4 for another bug found in bug6157

module bug6157_1(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   mid i1 (out1, in1, in2);
endmodule
module mid(out1, in1, in2);
   output [7:0] out1;
   input [7:0] 	in1, in2;

   defparam 	i2.net1 = 1'b1;
   
   missing_module i2(out1, in1, in2);
endmodule

