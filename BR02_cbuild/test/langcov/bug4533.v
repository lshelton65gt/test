// last mod: Fri Jun 10 09:21:14 2005
// filename: test/langcov/bug4533.v
// Description:  This test has an undefined parameter that is used in a delay
// statement.  This caused a crash in cheetah.  Interra bug #9094
// it should produce an error message about undefined 'delay'



module bug4533( clk, sel, Q );
   input     clk;
   input     sel;
   output    Q;
   reg       Q;
//   parameter delay = 1; // if this is uncommented this test works fine
   
   always @(clk)
     begin
	if(sel==0)
	  begin
	     Q=1'b1 ;
	     #(delay) ;
	  end
	else
	  begin
	     Q=1'b0 ;
	  end
     end
endmodule



