/* Pulldown design from spec */
module top(bi, in, ena, out);
inout bi;
input in, ena;
output out;
assign bi = ena ? in : 1'bz;
pulldown(bi);
assign out = ~bi;
endmodule
