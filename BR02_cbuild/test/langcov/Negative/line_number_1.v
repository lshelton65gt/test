// last mod: Mon Jan 30 16:55:01 2006
// filename: test/langcov/Negative/line_number_1.v
// Description:  At one time this test would point to the wrong line number in
// an error mesage.  The line that should be reported is indicated with <<-- line
// this is from bug 5595

`timescale 1ps/1ps
module line_number_1 (
	    clk,             
	    resetN,          
	    in1,
	    in2,
	    in3
	    );
   input		clk;
   input		resetN;
   input		in1;
   input [1:0] 		in2;
   input		in3;
   reg [3:0] 		reg1;
   reg 			reg2;


   always @ (posedge clk or negedge resetN)  // <<--
     begin
	if (in3)
	  begin
	     reg2 <= in1;
	  end
     end

   always @ (posedge clk or negedge resetN)
     begin
	if (!resetN)
	  begin
             reg1 <= 0;
	  end
	else
	  begin
             if (in3 & in1)
               begin
              	  reg1 <= in2;
               end
	  end
     end     
   
endmodule
