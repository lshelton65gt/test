// last mod: Mon Nov 27 05:58:19 2006
// filename: test/langcov/Negative/bug6620.v
// Description:  This test duplicates the issue from bug 6620, where the wrong
// line is reported when edges and levels are mixed in the sensitivity list



module bug6620(Clk, Reset_N, in1, in2, out1);

   input             Clk;
   input             Reset_N;
   input             in1;
   input             in2;

   output            out1;

   reg               out1;

   reg [15:0] 	     reg1;

   reg               EXTERNAL_CONTROL1; // This reg is forced externally from configure.v
   reg               EXTERNAL_CONTROL2; // This reg is forced externally from configure.v

   initial
      begin
	 EXTERNAL_CONTROL1 = 0;
	 EXTERNAL_CONTROL2 = 0;
      end
   always @( posedge Clk or negedge Reset_N ) begin
      if ( !Reset_N ) begin
	 reg1    = 0;
      end
      else begin
	 reg1 = reg1 + 1;
      end
   end

   // the following block contains edges and levels in the sensitivity list (unsupported)
   // any warning should point to it, not some other line that uses EXTERNAL_CONTROL1
   always @( posedge in2 or negedge Reset_N or EXTERNAL_CONTROL1 ) begin
      if ( EXTERNAL_CONTROL1 == 1 ) begin    // once EXTERNAL_CONTROL1 is ignored
				             // (by warning, then this if stmt does
				             // not match anything in the sensitivity list,
				             // and so we get an error message.
         out1 = 1;
      end
      else if ( !Reset_N ) begin
         out1 = 0;
      end
      else if ( !in2 ) begin
         out1 = 0;
      end
   end

   always @( in1 or Reset_N ) begin
      if ( Reset_N & in1 ) begin
	 if ( EXTERNAL_CONTROL2 == 0 ) begin
	    out1 = 1;
	 end
      end
   end
endmodule
