// last mod: Tue Jan 17 14:38:16 2006
// filename: test/langcov/Negative/no_such_module_1.v
// Description:  This test tries to use an instance of a undefined module, the
// module has an escaped name.


module no_such_module_1(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   \#%dut u1(out1, in1, in2, clock);
endmodule
