// last mod: Mon Dec 12 09:50:52 2005
// filename: test/langcov/bug5360.v
// Description:  This test would segfault in a cheetah routine when run from
// cbuild, and a standalone application.  Must be run with the command line
// switch -override


module bug5360 (i1, i2, o1);
        input i1, i2;
        output o1;

        Q_BUF i0 (.Z(o1), .A(i1), .B(i2));
endmodule 


module AND2 (o,ob,i0,i1);
	output o,ob;
	input i0,i1;

	nand  (ob,i0 ,i1 );
	not  (o,ob);

endmodule
// duplicate copy of AND2
module AND2 (o,ob,i0,i1);
	output o,ob;
	input i0,i1;

	nand  (ob,i0 ,i1 );
	not  (o,ob);

endmodule

module Q_BUF(Z,A,B);
   input A, B;
   output Z;

   assign Z = A;
endmodule 
