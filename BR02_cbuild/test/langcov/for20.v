// this module needed an iterative UD algorithm to properly compute
// the dependencies for out[].

module top ( in, out );
   
   input [127:0] in;
   output [127:0] out;
   reg [127:0] 	out;

   reg 		sava;
   reg 		savb;
   reg 		savc;
   reg 		savd;
   reg 		save;
   reg 		savf;
   reg 		savg;
   reg 		savh;

   always @(in)
     begin :forblk
	integer i;
   	sava=0;
   	savb=0;
   	savc=0;
   	savd=0;
   	save=0;
   	savf=0;
   	savg=0;
   	savh=0;
	for ( i = 0 ; i <= 127 ; i = i + 1 )
	  begin
	     out[i] = sava;
	     sava = savb;
	     savb = savc;
	     savc = savd;
	     savd = save;
	     save = savf;
	     savf = savg;
	     savg = savh;
	     savh = in[i];
	  end
     end
endmodule // top
