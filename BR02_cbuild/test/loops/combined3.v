// testcase which needs repeated application of loop code motion and
// distribution prior to unrolling.
module motion(clk, rst, sel1, sel2, sel3, a_in, b_in, c_in, d_in, out);
   parameter WIDTH = 512;
   
   input clk;
   input rst;
   input sel1, sel2, sel3;
   input [WIDTH-1:0] a_in;
   input [WIDTH-1:0] b_in;
   input [WIDTH-1:0] c_in;
   input [WIDTH-1:0] d_in;
   output [WIDTH-1:0] out;

   reg [WIDTH-1:0] out;
   reg [WIDTH-1:0] a;
   reg [WIDTH-1:0] b;
   reg [WIDTH-1:0] c;
   reg [WIDTH-1:0] d;

   integer     i;
   
   always @(a_in or b_in or c_in or d_in) begin

      for (i=0; i<WIDTH; i=i+1) begin
	 if (sel1) begin
	    if (sel2) begin
	       a[i] <= a_in[i];
	       b[i] <= b_in[i];
	    end else begin
	       a[i] <= b_in[i];
	       b[i] <= a_in[i];
	    end
	    c[i] <= c_in[i];
	 end else begin
	    if (sel3) begin
	       a[i] <= b_in[i];
	       b[i] <= c_in[i];
	    end else begin
	       a[i] <= c_in[i];
	       b[i] <= b_in[i];
	    end
	    c[i] <= a_in[i];
	 end
	 d[i] <= d_in[i];
      end
   end
   
   always @(posedge clk) begin
      if (rst) 
	out <= 0;
      else
	out <= a | b | c | d;
   end

endmodule
