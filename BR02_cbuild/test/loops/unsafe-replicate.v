module cw000504_compelm(clk,r,ensyncdet,syncchar,syncmask,s);
   input clk;
   input [0:9] r;
   input       ensyncdet;
   input [0:9] syncchar;
   input [0:9] syncmask;
   output      s;

   wire        sp,sn;
   reg [0:9]   stp,stn;
   integer     i;

   assign      sp = stp[0]&stp[1]&stp[2]&stp[3]&stp[4]&stp[5]&
	       stp[6]&stp[7]&stp[8]&stp[9]&ensyncdet;

   assign      sn = stn[0]&stn[1]&stn[2]&stn[3]&stn[4]&stn[5]&
	       stn[6]&stn[7]&stn[8]&stn[9]&ensyncdet;

   reg 	       s;
   initial s=0;
   always @(posedge clk) s = sp||sn;

   initial stn=0;
   initial stp=0;
   always @(r or syncchar or syncmask)
     begin
	for (i=0; i<=9; i=i+1)
	  begin
	     if (syncmask[i])
	       if (r[i] == syncchar[i])
		 begin
		    stp[i] = stn[i];
		    stn[i] = 0;
		 end
	       else 
		 begin
		    stp[i] = stn[i];
		    stn[i] = 1;
		 end
	     else
	       begin
		  stp[i] = stn[i];
		  stn[i] = 1;
	       end
	  end
     end 
endmodule
