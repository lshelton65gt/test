// testcase which cannot have loop distribution occur
module distribute(clk, rst, a, b, out);
   parameter WIDTH = 512;
   
   input clk;
   input rst;
   input [WIDTH-1:0] a;
   input [WIDTH-1:0] b;
   output [WIDTH-1:0] out;

   reg [WIDTH-1:0] out;
   reg [WIDTH-1:0] c;
   reg [WIDTH-1:0] d;

   integer     i;

   initial begin
      c = 0;
      d = 0;
      out = 0;
   end
   
   always @(posedge clk) begin
      // the loop cannot be replicated for each stmt because d[i] uses
      // a value not yet computed.
      for (i=0; i<WIDTH-1; i=i+1) begin
	 c[i] = a[i];
	 d[i] = b[i] & c[i+1];
      end
      // update the tail.
      c[WIDTH-1] = a[WIDTH-1];
      d[WIDTH-1] = b[WIDTH-1];
      out = c ^ d;
   end
   
endmodule
