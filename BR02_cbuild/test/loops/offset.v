module big(clk,in,out);
   input clk;
   input in;
   output out;

   reg [73:10] storage;

   initial storage = 0;

   assign  out = storage[73];

   integer i;
   always @(posedge clk) begin
      storage[10] <= in;
      for (i=73; i>10;i=i-1)
	storage[i] <= storage[i-1];
   end
endmodule
