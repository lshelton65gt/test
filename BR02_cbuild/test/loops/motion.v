// testcase which needs loop code motion prior to unrolling.
module motion(clk, rst, sel, a, b, out);
   parameter WIDTH = 512;
   
   input clk;
   input rst;
   input sel;
   input [WIDTH-1:0] a;
   input [WIDTH-1:0] b;
   output [WIDTH-1:0] out;

   reg [WIDTH-1:0] out;
   reg [WIDTH-1:0] c;

   integer     i;
   
   always @(a or b) begin
      // the if() statement should be moved outside the loop.
      for (i=0; i<WIDTH; i=i+1) 
	 if (sel)
	   c[i] <= a[i];
	 else
	   c[i] <= b[i];
   end
   
   always @(posedge clk) begin
      if (rst) 
	out <= 0;
      else
	out <= c;
   end

endmodule
