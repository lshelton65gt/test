// testcase which cannot have loop distribution occur
module doubledip(clk, smallsel, bigsel, a, b, out);
   parameter WIDTH = 256;
   
   input clk;
   input [1:0] smallsel;
   input [(WIDTH/2)-1:0] bigsel;

   input [WIDTH-1:0] a;
   input [WIDTH-1:0] b;
   output [WIDTH-1:0] out;

   reg [WIDTH-1:0] out;
   reg [WIDTH-1:0] c;
   reg [WIDTH-1:0] d;

   integer     i;
   integer     j;

   initial begin
      c = 0;
      d = 0;
      out = 0;
   end
   
   always @(posedge clk) begin
      // the loop cannot be replicated for each stmt because d[i] uses
      // a value not yet computed.
      for (i=0; i<2; i=i+1) begin
	 for (j=0; j<(WIDTH/2); j=j+1) begin
	    if (smallsel[i])
	      if (bigsel[j]) 
		c[i*(WIDTH/2)+j] = a[i*(WIDTH/2)+j];
	      else
		c[i*(WIDTH/2)+j] = b[i*(WIDTH/2)+j];
	    else
	      c[i*(WIDTH/2)+j] = a[i*(WIDTH/2)+j] & b[i*(WIDTH/2)+j];
	 end
      end
      for (i=0; i<2; i=i+1) begin
	 for (j=0; j<(WIDTH/2); j=j+1) begin
	    d[i*(WIDTH/2)+j] = a[i*(WIDTH/2)+j];
	 end
      end
      out = c|d;
   end
endmodule
