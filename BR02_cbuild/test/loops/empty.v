module broken(clk,in,out);
   input clk;
   input in;
   output out;
   reg 	  out;

   task flush_check;
      integer r;
      integer flush_reqid;
      integer q_id;
      integer q_stat_value;
      integer status;

      begin
	 // Optimizing this loop could remove drivers. If we do not
	 // recompute UD for the task, it will be incorrect, leading
	 // to a failure when creating UnelabFlow.
	 
	 for (flush_reqid = 0; flush_reqid <= 4'hf; flush_reqid = flush_reqid+1)
	   for (q_id = flush_reqid; q_id <= {4'h8, flush_reqid[3:0]}; q_id = q_id + 8'h10)
	     begin
		//$q_exam(q_id, 1, q_stat_value, status);
		if ((q_stat_value > 0) && (status === 0)) begin
		   begin
		      begin
			 //$write("[%0t] %%W:%sreg_common_bfm.v:0077: Queue not empty with queue id: %h\n",
			 //$time, broken.in, q_id);
		      end
		   end
		end
	     end
      end
   endtask

   initial out = 0;
   always @(posedge clk)
     begin
	out <= in;
     end
endmodule
