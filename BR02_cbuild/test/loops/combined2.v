// testcase which needs loop code motion then distribution prior to unrolling.
module motion(clk, rst, sel, a, b, c, d, out);
   parameter WIDTH = 512;
   
   input clk;
   input rst;
   input sel;
   input [WIDTH-1:0] a;
   input [WIDTH-1:0] b;
   input [WIDTH-1:0] c;
   input [WIDTH-1:0] d;
   output [WIDTH-1:0] out;

   reg [WIDTH-1:0] out;
   reg [WIDTH-1:0] e;
   reg [WIDTH-1:0] f;

   integer     i;
   
   always @(a or b) begin
      // the if() statement should be moved outside the loop.
      // the loop should be replicated for each internal stmt
      for (i=0; i<WIDTH; i=i+1) 
	 if (sel) begin
	    e[i] <= a[i];
	    f[i] <= b[i];
	 end else begin
	    e[i] <= c[i];
	    f[i] <= d[i];
	 end
   end
   
   always @(posedge clk) begin
      if (rst) 
	out <= 0;
      else
	out <= e | f;
   end

endmodule
