module top(a,b,x,y);
   input [127:0] a,b;
   output [127:0] x,y;
   reg [127:0] 	  x,y;

   integer 	  i;
   always @(a or b)
     begin
	for (i=0 ; i<128 ; i=i+1) 
	  begin
	     {x[i],y[i]} = {a[i],b[i]};
	  end
     end
endmodule
