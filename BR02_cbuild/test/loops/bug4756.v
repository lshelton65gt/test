// Testcase which exposed that we were incorrectly computing UD for
// loops with multiple, overlapping defs for the same net.

// This test is not simulated. It will infinitely loop if are_we_done is ever 0.

module top(clk,write,select,addr1,addr2,addr3,addr4,are_we_done,out1,out2);
   input clk,write,select;
   input [13:0] addr1,addr2,addr3,addr4;
   input 	are_we_done;
   output [13:0] out1,out2;
   reg [13:0] out1,out2;

   parameter  WIDTH=1<<3;//14;
   reg [13:0] mem [0:WIDTH-1];
   reg [13:0] prev [0:WIDTH-1];

   reg 		 done;
   always @(posedge clk)
     begin
	while (done == 0) begin
	   if (select) begin
	      if (write) begin
		 out1 = mem[addr1];
	      end
	      
	      mem[addr2] = mem[0];

	      prev[mem[0]] = addr2;

	      mem[0] = addr2;
	      
	      done = are_we_done;
	   end else begin
	      if (write) begin
		 out2 = mem[addr3];
	      end
	      
	      mem[addr2] = mem[0];

	      prev[mem[0]] = addr4;

	      mem[0] = addr2;
	      
	      done = are_we_done;
	   end
	   if (done == 0) begin
	      $display("DANGER WILL ROBINSON");
	   end
	end
     end
   
endmodule
