// This testcase has some simulation diffs -- it has
// conflicting non-blocking assigns to o1, and I think
// Verilog semantics are that the second assign should
// win, and Carbon is letting the first one win.  Actually
// if you look at the generated C++ verilog, the assign to
// o1 is given as a case with many duplicate entries.

module top (i1, ck, i1v, i2v, o1, o3);
   input i1, ck;
   input [127:0] i1v;
   input [3:0]   i2v;

   output        o1;
   output        o3;

   reg           o1, o3;

   reg           r1, r2, r3;
   reg [31:0]    r1v;

   initial begin
     r1 = 0;
     r2 = 0;
     r3 = 0;
     r1v = 0;
     o1 = 1;                    // needed for simulation match
     o3 = 0;
   end

   always @(posedge ck) begin :stuff
      integer I,K;
      reg     lr1;

      // Bug 3814 documents a problem where replication of
      // control statements was not triggering a UD update. This
      // was only a problem if the containing loop did not
      // unroll. This testcase was constructed such that
      // unrolling of the outer loop does not occur. 
      
      // This particular testcase failed to unroll with the
      // C2004_12_2659_p1 release, causing the problem. On the
      // mainline (version 1.2805), the loop was unrolled. The
      // bug3814-2 testcase reproduced the problem with version
      // 1.2805.

      for(I=0; I < 4; I = I + 1) begin
         lr1 = i1;
         if(i2v[I]) begin
            for (K = 0; K < 16; K = K+1) begin
               r1v[K] = i1v[K];
            end
         end

         if (i1) r1 <= 0;

         if (lr1) r2 <= 1;
         else if (i1) r2 <= 0;

         if (i1) o1 <= 0;
         if (i1) o1 <= 1;

         if (!i1) begin
            if (r3 == 1'b1)
              o3 <= r3;
            else if (r3 == 1'b1)
              o3 <= r3 | i1;
         end

      end // for (I=0; I < 4; I = I + 1)

   end // always @ (posedge ck)
endmodule // top
