module named_loop(oDOUT, FIFO, READG);
   // parameter iWIDTH = 256;
   // parameter iDEPTH = 32;
   parameter iWIDTH = 8;
   parameter iDEPTH = 16;
   output [iWIDTH-1:0] oDOUT;
   input [(iWIDTH*iDEPTH)-1:0] FIFO;
   input [iDEPTH-1:0] 	       READG;

   reg [iWIDTH-1:0] 	       oDOUT;

   always @(FIFO or READG)
     begin :xhdl_10
	integer i;
        for(i = 0; i <= (iWIDTH - 1); i = i + 1)
          begin
             begin : xhdl_11
		integer j;
		for(j = 0; j <= (iDEPTH - 1); j = j + 1)
		  begin
                     //        oDOUT(i) := oDOUT(i) or (FIFO(j)(i) and READG(j));
                     oDOUT[i] = oDOUT[i] | (FIFO[(j * iWIDTH) + i] & READG[j]); 
		  end
             end // :xhdl_11
          end
     end // :xhdl_10
endmodule
