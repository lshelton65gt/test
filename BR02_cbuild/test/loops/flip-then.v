module flip(clk,in,wsel,sel,out);
   input clk;
   input [127:0] in;
   input [127:0] wsel;
   input 	 sel;
   output [127:0] out;

   reg [127:0] 	  out;
   integer 	  i;

   initial out = 0;
   always @(posedge clk)
     begin
	for (i=0; i<128; i=i+1)
	  begin
	     if (wsel[i]) 
	       begin
		  if (sel) 
		    begin
		       out[i] = in[i];
		    end
		  else
		    begin
		       out[i] = ~in[i];
		    end
	       end
	     else
	       begin
		  out[i] = ~out[i];
	       end
	  end
     end
endmodule

