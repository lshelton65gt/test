module top(o, clk, iv);
   output o;
   input  clk;
   input  [0:10] iv;
   integer i, j;
   reg     o;
   always @(posedge clk)
     begin
        i = 0;
        while (i < 10)
          i = i + 1;
        j = i;
        o = 1'b1;
        while (j > 0)
          begin
             o = o & iv[j];
             j = j - 2;
          end
     end
endmodule
