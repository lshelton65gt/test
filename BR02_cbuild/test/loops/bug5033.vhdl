library IEEE;
   use IEEE.std_logic_1164.all;
   use IEEE.std_logic_unsigned.all;
   use IEEE.std_logic_arith.all;

package mypack is
        constant TLBDEPTH : integer := 32;
        constant NUMENTRIESMAX : integer := 32;
        type bitarray is array (0 to TLBDEPTH-1) of std_logic_vector(NUMENTRIESMAX-1 downto 0);
type DRVBankEntry is array (NUMENTRIESMAX-1 downto 0) of std_logic_vector(1 downto 0);
type DRVBank is array (0 to TLBDEPTH-1) of DRVBankEntry;
end mypack;

library IEEE;
   use IEEE.std_logic_1164.all;
   use IEEE.std_logic_unsigned.all;
   use IEEE.std_logic_arith.all;
library work;
use work.mypack.all;
   entity bug5033 is
   port (
      mfeclkbuf             : in     std_logic;
      mferstN               : in     std_logic;
      drvlatchref0, drvlatchdirty0                     : in bitarray;
      drvlatchref2, drvlatchdirty2                     : in bitarray;
      drvin0, drvin2                                   : in DRVBankEntry;
      drv                                              : buffer DRVBank
         );
      end bug5033;


architecture functional of bug5033 is

  begin
    
DRVREGISTERS : process (mfeclkbuf, mferstN)
   begin
      if (mfeclkbuf'event and mfeclkbuf = '1') then
         for i in 0 to TLBDEPTH-1 loop
            for j in 0 to NUMENTRIESMAX-1 loop
               if (drvlatchref0(i)(j) = '1') then
                  drv(i)(j)(1) <= drvin0(j)(1);
               elsif (drvlatchref2(i)(j) = '1') then
                  drv(i)(j)(1) <= drvin2(j)(1);
               end if;

               if (drvlatchdirty0(i)(j) = '1') then
                  drv(i)(j)(0) <= drvin0(j)(0);
               elsif (drvlatchdirty2(i)(j) = '1') then
                  drv(i)(j)(0) <= drvin2(j)(0);
               end if;
            end loop;
         end loop;   
      end if;
   end process;
end functional;
