module bug905(clk,sel,in,out);
   parameter WIDTH=32;
   input     clk;
   input     sel;
   input [WIDTH-1:0] in;
   output [WIDTH-1:0] out;
   reg [WIDTH-1:0] out;

   integer 	   i;
   always @(posedge clk) begin
      for(i = 0; i < WIDTH; i = i + 1) begin
	 case (sel)
	   1'b0:
             out = 0;
	   1'b1:
             out[i] = in[i];
	 endcase
      end
   end
endmodule

