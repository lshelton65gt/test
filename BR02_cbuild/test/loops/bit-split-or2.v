module top (clk, wen, sel, in, out);
   parameter WIDTH = 4;

   input clk;
   input wen;
   input [WIDTH-1:0] sel;
   input [WIDTH-1:0] in;
   output [WIDTH-1:0] out;
   reg [WIDTH-1:0] out;

   integer 	   i;
   
   initial out = 0;
   always @(posedge clk) begin
      for (i=0; i<WIDTH; i=i+1) begin
	 if (sel[i] | wen)
	   out[i] = in[i];
	 else
	   out[i] = ~in[i];
      end
   end
endmodule
