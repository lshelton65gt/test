// testcase which needs loop distribution then code motion prior to unrolling.
module motion(clk, rst, sel, a, b, out);
   parameter WIDTH = 2;

   input clk;
   input rst;
   input sel;
   input [WIDTH-1:0] a;
   input [WIDTH-1:0] b;
   output out;

   reg [WIDTH-1:0] out;
   reg [WIDTH-1:0] c;
   reg [WIDTH-1:0] d;

   integer     i;
   
   always @(a or b) begin
      // the loop should be replicated for each stmt
      // the if() statement should be moved outside the loop.
      for (i=0; i<WIDTH; i=i+1) begin
	 if (sel)
	   c[i] <= a[i];
	 else
	   c[i] <= b[i];
	 d[i] <= a[i] & b[i];
      end
   end
   
   always @(posedge clk) begin
      if (rst) 
	out <= 0;
      else
	out <= c | d;
   end

endmodule
