// This loop cannot be distributed. If we separate the two definitions
// of 'out', some bits may end up with incorrect values.
module top(clk,in,out);
   input clk;
   input [31:0] in;
   output [31:0] out;
   reg [31:0] 	 out;

   integer 	 i;
   initial out = 0;
   always @(posedge clk) begin
      for (i=10; i<31; i=i+1) begin
	 out[i-10] = in[i];
	 out[i] = in[i];
      end
   end
endmodule
