module top(o, o1, clk);
   output [31:0] o;
   reg [31:0]    o;
   output [31:0] o1;
   reg [31:0]    o1;
   input         clk;
   integer       i;

   function [31:0] foo;
      input [31:0] i;
      begin
         foo = 10 + i;
         $display("called foo");
      end
   endfunction
   
   always @(posedge clk)
     begin
        o = 0;
        repeat (10)
          o = o + 1;
     end
   always @(posedge clk)
     begin
        o1 = 0;
        repeat (foo(1))
          begin
             repeat (0)
               o1 = o1 + 1;
             o1 = o1 + 1;
          end
     end
endmodule
