module top (clk, ena, enb, enc, sela, selb, selc, in, out);
   parameter WIDTH = 4;

   input clk;
   input ena, enb, enc;
   input [WIDTH-1:0] sela, selb, selc;
   input [WIDTH-1:0] in;
   output [WIDTH-1:0] out;
   reg [WIDTH-1:0] out;

   integer 	   i;
   
   initial out = 0;
   always @(posedge clk) begin
      for (i=0; i<WIDTH; i=i+1) begin
	 if ( ena && (sela[i] || enb) ) begin
	    out[i] = in[i];
	 end else begin
	    if ( enb || (selb[i] && enc && selc[i])) begin
	       out[i] = ~in[i];
	    end else begin
	       out[i] = in[i];
	    end
	 end
      end
   end
endmodule
