// author: cloutier
// created: 12/12/03
// testing the handling of boundaries of loops defined with large constants with small values.
// for loop unrolling code should see that the number of iterations is limited
// to 7, at one time we were seeing an assert that the number of iterations is > 1024
// looking at the code this appears to be a problem with the comparison of the
// integer i (signed) and the constant (unsigned) 157'h07.

module top ( out, clk, in );
  output [7:0] out;
  reg [7:0] out;
  input clk;
  input [7:0] in;

  integer i;
  always @ (posedge clk)
  begin
    for ( i = 0 ; i <= 157'h07 ; i = i + 1 )
      begin
        out[i] = in[i];
      end // for
  end // always

endmodule // top
