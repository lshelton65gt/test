module top(clk,reset,a,b,c,d,e,f,g,h,j,k,l,m);
   parameter WIDTH=128;
   
   input clk;
   input reset;
   output [WIDTH-1:0] a;
   input 	  b;
   input [6:0] 	  c;
   input       d,e,f,g,h,j,k;
   input [WIDTH-1:0] l, m;
   
   integer i;
   reg [127:0] a;
   always @(posedge clk)
     begin
	if (~reset)
	  begin
	     for (i = 0; i < WIDTH; i = i + 1)
	       a[i]  <= 1'b0;
	  end
	else
	  begin
	     for (i = 0; i < WIDTH; i = i + 1) begin
		if (b && (i == c[6:0]) && (d || !e))
		  a[i]  <= 1'b0;
		else if (f && g)
		  begin
		     if (!h)
		       begin
			  if (!j)
			    begin
			       if (k)
				 a[i]  <= (l[i] | m[i]);
			    end
			  else
			    a[i]  <= (l[i] | m[i]);
		       end
		  end
		else
		  a[i]  <= (l[i] | m[i]);
	     end
	  end
     end
endmodule

