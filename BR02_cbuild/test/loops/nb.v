module markk(clk,en,wren,depth);
  input clk;
  input [1:0] en,wren;
  output [1:0] depth;
  reg [1:0] depth;
  integer jj;

  initial depth = 0;
  always @(posedge clk) begin
    for (jj=0; jj<2; jj=jj+1) begin
      depth[jj] <= 1'b0;
      if (en[jj]) begin
        if (wren[jj]) begin
          depth[jj] <= 1'b1;
        end
      end
    end
  end
endmodule
