// testcase which cannot have loop distribution occur
module distribute(clk, rst, a, b, out);
   parameter WIDTH = 512;
   
   input clk;
   input rst;
   input [WIDTH-1:0] a;
   input [WIDTH-1:0] b;
   output [WIDTH-1:0] out;

   reg [WIDTH-1:0] out;
   reg [WIDTH-1:0] c;
   reg [WIDTH-1:0] d;

   integer     i;
   
   always @(a or b) begin
      // update the head.
      d[0] = b[WIDTH-1];
      // the loop cannot be replicated for each stmt because d[i] uses
      // a value not yet computed.

      // This is a variation of the failure example Fig 20.23 on pg
      // 693 of Advanced Compiler Design & Implementation (Muchnick)
      for (i=0; i<WIDTH-1; i=i+1) begin
	 c[i] = a[i] & d[i];
	 d[i+1] = b[i];
      end
      // update the tail.
      c[WIDTH-1] = a[i] & d[WIDTH-1];
   end
   
   always @(posedge clk) begin
      if (rst) 
	out <= 0;
      else
	out <= c ^ d;
   end

endmodule
