// testcase which needs loop distribution prior to unrolling.
module distribute(clk, rst, a, b, out);
   parameter WIDTH = 512;
   
   input clk;
   input rst;
   input [WIDTH-1:0] a;
   input [WIDTH-1:0] b;
   output [WIDTH-1:0] out;

   reg [WIDTH-1:0] out;
   reg [WIDTH-1:0] c;
   reg [WIDTH-1:0] d;

   integer     i;
   
   always @(a or b) begin
      // update the head.
      c[0] = a[0];
      d[0] = b[0] & c[0];
      // the loop should be replicated for each stmt. this is safe to
      // distribute because d[i] only uses previously computed values.
      for (i=1; i<WIDTH; i=i+1) begin
	 c[i] = a[i];
	 d[i] = b[i] & c[i] & c[i-1];
      end
   end
   
   always @(posedge clk) begin
      if (rst) 
	out <= 0;
      else
	out <= c ^ d;
   end

endmodule
