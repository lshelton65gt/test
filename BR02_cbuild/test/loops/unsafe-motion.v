// testcase cannot have loop code motion occur
module motion(clk, rst, sel, a, b, out);
   parameter SELWIDTH = 16;
   parameter REPEAT   = 32;
   parameter WIDTH = SELWIDTH*REPEAT;

   input clk;
   input rst;
   input [SELWIDTH-1:0] sel;
   input [WIDTH-1:0] a;
   input [WIDTH-1:0] b;
   output [WIDTH-1:0] out;

   reg [WIDTH-1:0] out;
   reg [WIDTH-1:0] c;
   reg [WIDTH-1:0] d;

   integer     i;
   
   always @(a or b) begin
      // the if() statement uses the index variable.
      for (i=0; i<WIDTH; i=i+1) 
	 if (sel[i%SELWIDTH]) begin
	    c[i] <= a[i];
	    d[i] <= b[i];
	 end else begin
	    c[i] <= b[i];
	    d[i] <= a[i];
	 end
   end
   
   always @(posedge clk) begin
      if (rst) 
	out <= 0;
      else
	out <= c | d;
   end

endmodule
