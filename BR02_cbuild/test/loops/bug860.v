module bug (
		 iICLK,
		 iOCLK,
		 iIRST,
		 iREQI,
		 iDI,
		 oDO,
		 iWRPTR,
		 iREADG
);
   parameter iWIDTH  = 321;
   parameter iDEPTH  = 16;
   parameter iDLOG  = 4;

   input iICLK; 
   input iOCLK; 
   input iIRST;
   input iREQI; 
   input[(iWIDTH - 1):0] iDI; 
   output[(iWIDTH - 1):0] oDO; 
   input[(iDLOG - 1):0] iWRPTR; 
   input[(iDEPTH - 1):0] iREADG; 

   reg[(iDEPTH * iWIDTH - 1):0] FIFO; 
   reg[(iWIDTH - 1):0] FIFODO; 
   reg [(iDEPTH - 1):0]  READG; 
   reg[(iDEPTH - 1):0] FIFOCE;
   reg IRSTr; 
   reg[(iDEPTH - 1):0] FIFOE; 
   wire[(iWIDTH - 1):0] FIFODI; 
   wire[(iDLOG - 1):0] WRPTR; 
   wire INCWRPTR; 
   wire[(iWIDTH - 1):0] IRSTrbus; 
   wire[(iWIDTH - 1):0] oDO;
 
   assign INCWRPTR = iREQI ; 
   assign WRPTR = iWRPTR; 
   assign FIFODI = iDI & ~IRSTrbus ; 
   assign IRSTrbus = {iWIDTH{IRSTr}} ; 
   assign oDO = FIFODO ; 

   always @(posedge iOCLK) 
   begin : oclk_reg
      READG <= iREADG ; 
   end 

   always @(posedge iICLK) 
   begin : iclk_rreg
      IRSTr <= iIRST ; 
   end 

   always @(FIFOE or INCWRPTR or IRSTr)
   begin : WRITEENABLE_PROCESS
      begin : xhdl_14
         integer i;
         for(i = 0; i <= (iDEPTH - 1); i = i + 1)
         begin
            FIFOCE[i]  = (FIFOE[i] & INCWRPTR) | IRSTr ; 
         end
      end 
   end 

   always @(WRPTR)
   begin : WRITEDECODE_PROCESS
      FIFOE  = 0 ; 
      FIFOE[WRPTR]  = 1'b1 ; 
   end 

`ifdef CARBONX
   reg [(iDEPTH * iWIDTH - 1):0] mask; 
`endif
   always @(posedge iICLK) 
   begin : WRITEFIFO_PROCESS
      begin : xhdl_6
         integer i;
         for(i = (iDEPTH - 1); i >= 0; i = i - 1)
         begin
            if ((FIFOCE[i]) == 1'b1)
            begin
               begin : xhdl_7
`ifdef CARBONX
		  mask = ~(({iWIDTH{1'b1}})<<(i*iWIDTH));
		  FIFO <= (FIFO & mask) | (FIFODI<<(i*iWIDTH));
`else
                  integer j;
                  for(j = 0; j <= (iWIDTH - 1); j = j + 1)
                  begin
                     FIFO[((i + 1) * iWIDTH) - 1 - j] <= FIFODI[iWIDTH - 1 - j] ; 
                  end
`endif
               end 
            end 
         end
      end 
   end 

   always @(FIFO or READG)
   begin : READFIFO_PROCESS
      reg[(iWIDTH - 1):0] oDOUT; 
      begin : xhdl_9
         integer i;
         for(i = 0; i <= (iWIDTH - 1); i = i + 1)
         begin
            oDOUT[i] = 1'b0; 
         end
      end 
      begin : xhdl_10
         integer i;
         for(i = 0; i <= (iWIDTH - 1); i = i + 1)
         begin
            begin : xhdl_11
               integer j;
               for(j = 0; j <= (iDEPTH - 1); j = j + 1)
               begin
                  //        oDOUT(i) := oDOUT(i) or (FIFO(j)(i) and READG(j));
                  oDOUT[i] = oDOUT[i] | (FIFO[(j * iWIDTH) + i] & READG[j]); 
               end
            end 
         end
      end 
      FIFODO  = oDOUT ; 
   end 
endmodule
