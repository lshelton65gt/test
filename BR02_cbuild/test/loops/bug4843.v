`define     ECSM_PORT2SPS_NUM_OF_GE_CH             2
`define ECSM_SPG_NUM            64
`define     ECSM_PORT2SPS_NUM_OF_FE_CH             14
`define ECSM_SPGBUS_WIDTH       6
`define NP3705_CSM_UCDWIDTH     64
`define NP3705_ECSM_UCAWIDTH    20
`define     ECSM_GE_CHBUS_WIDTH                     1
`define     ECSM_FE_CHBUS_WIDTH                     4

module np3705_ecsm  (
                        csm_clk,
                        csm_rst_b,
                     xdist_if_bank_rd,
                     xdist_if_bank_add,
                     xdist_if_bank_data,
                     fi0a_p2spg_v,
                     fi0b_p2spg_v,
                     spi4_p2spg_v,
                     hi_p2spg_v,
                     lb_p2spg_v,
                     fi1_p2spg_v,
                     fi0a_ecsm_full_s,
                     fi0b_ecsm_full_s,
                     ge_csm_full_s,
                     fe14_csm_full_s,
                     fe_p2spg_v,
                     ge_p2spg_v,
                     HI_P2SPG,
                     FI1_P2SPG,
                     FI0a_P2SPG,
                     LB_P2SPG,
                     FI0b_P2SPG,
                     SPI4_P2SPG,
                     spi4_csm_full_s,
                     xdist_spg_xstate_v,
                     OUT
                     );
   input                                             csm_clk;
   input                                             csm_rst_b;    
   input                                             xdist_if_bank_rd;
   input [`NP3705_ECSM_UCAWIDTH          -1  :   0]  xdist_if_bank_add;
   input [`NP3705_CSM_UCDWIDTH           -1  :   0]  xdist_if_bank_data;
   input                                             fi0a_p2spg_v; 
   input                                             fi0b_p2spg_v; 
   input                                             spi4_p2spg_v;
   input                                             fi0a_ecsm_full_s; 
   input                                             fi0b_ecsm_full_s; 
   input                                             lb_p2spg_v;
   input                                             hi_p2spg_v;
   input                                             fi1_p2spg_v;
   input                                             spi4_csm_full_s;
   input                                             xdist_spg_xstate_v;
   input [`ECSM_PORT2SPS_NUM_OF_GE_CH -1 :       0]  ge_csm_full_s; 
   input [`ECSM_PORT2SPS_NUM_OF_FE_CH -1 :       0]  fe14_csm_full_s;
   input [`ECSM_PORT2SPS_NUM_OF_FE_CH -1 :       0]  fe_p2spg_v;
   input [`ECSM_PORT2SPS_NUM_OF_GE_CH -1 :       0]  ge_p2spg_v;
   input [`ECSM_SPGBUS_WIDTH -1  :   0]              HI_P2SPG;
   input [`ECSM_SPGBUS_WIDTH -1  :   0]              FI1_P2SPG;
   input [`ECSM_SPGBUS_WIDTH -1  :   0]              FI0a_P2SPG; 
   input [`ECSM_SPGBUS_WIDTH -1  :   0]              LB_P2SPG;
   input [`ECSM_SPGBUS_WIDTH -1  :   0]              FI0b_P2SPG; 
   input [`ECSM_SPGBUS_WIDTH -1  :   0]              SPI4_P2SPG;
   output [`ECSM_SPG_NUM      -1  :   0]             OUT;
   reg [`ECSM_SPG_NUM      -1  :   0]                SPG_XON;
   reg [`ECSM_SPGBUS_WIDTH -1  :   0]                GE_P2SPG    [`ECSM_PORT2SPS_NUM_OF_GE_CH -1 :0];
   reg [`ECSM_SPGBUS_WIDTH -1  :   0]                FE_P2SPG    [`ECSM_PORT2SPS_NUM_OF_FE_CH -1 :0];
   integer        i,j,k;
   parameter      TP          = 1;

   initial SPG_XON = 0;
   
   always @(posedge csm_clk or negedge csm_rst_b)
    if (~csm_rst_b) begin
        for (i = 0; i < `ECSM_PORT2SPS_NUM_OF_GE_CH  ; i = i + 1)
           GE_P2SPG[i] <= #TP {`ECSM_SPGBUS_WIDTH{1'b0}};
    end
    else 
      GE_P2SPG[xdist_if_bank_add[`ECSM_GE_CHBUS_WIDTH -1 :0]] <= #TP xdist_if_bank_data[`ECSM_SPGBUS_WIDTH -1:0];

   always @(posedge csm_clk or negedge csm_rst_b)
    if (~csm_rst_b) begin
        for (j = 0; j < `ECSM_PORT2SPS_NUM_OF_FE_CH  ; j = j + 1)
           FE_P2SPG[j] <= #TP {`ECSM_SPGBUS_WIDTH{1'b0}};
    end
    else 
      FE_P2SPG[xdist_if_bank_add[`ECSM_FE_CHBUS_WIDTH -1 :0]] <= #TP xdist_if_bank_data[`ECSM_SPGBUS_WIDTH -1:0];


   always @(posedge csm_clk or negedge csm_rst_b)
    if (~csm_rst_b) SPG_XON<= #TP {`ECSM_SPG_NUM{1'b1}};
    else  if (xdist_spg_xstate_v & !xdist_if_bank_rd) SPG_XON <= #TP xdist_if_bank_data;
      else for (k = 0; k < `ECSM_SPG_NUM  ; k = k + 1)  begin
            if  (k==SPI4_P2SPG & spi4_p2spg_v)   SPG_XON[k]      <= #TP !spi4_csm_full_s;

	    else if  (k==FI0a_P2SPG  & fi0a_p2spg_v)    SPG_XON[k]       <= #TP !fi0a_ecsm_full_s; 
	    else if  (k==FI0b_P2SPG  & fi0b_p2spg_v)    SPG_XON[k]       <= #TP !fi0b_ecsm_full_s;
            else if  (k==FI1_P2SPG  & fi1_p2spg_v)    SPG_XON[k]       <= #TP fi0a_ecsm_full_s;
            else if  (k==HI_P2SPG  & hi_p2spg_v)     SPG_XON[k]        <= #TP fi0b_ecsm_full_s;
            else if  (k==LB_P2SPG  & lb_p2spg_v)     SPG_XON[k]        <= #TP !fi0b_ecsm_full_s;
            else if  (k==GE_P2SPG[0]  & ge_p2spg_v[0])  SPG_XON[k]     <= #TP !ge_csm_full_s[0];
            else if  (k==GE_P2SPG[1]  & ge_p2spg_v[1])  SPG_XON[k]     <= #TP !ge_csm_full_s[1];
            else if  (k==FE_P2SPG[0]  & fe_p2spg_v[0])  SPG_XON[k]     <= #TP !fe14_csm_full_s[0];
            else if  (k==FE_P2SPG[1]  & fe_p2spg_v[1])  SPG_XON[k]     <= #TP !fe14_csm_full_s[1];
            else if  (k==FE_P2SPG[2]  & fe_p2spg_v[2])  SPG_XON[k]     <= #TP !fe14_csm_full_s[2];
            else if  (k==FE_P2SPG[3]  & fe_p2spg_v[3])  SPG_XON[k]     <= #TP !fe14_csm_full_s[3];
            else if  (k==FE_P2SPG[4]  & fe_p2spg_v[4])  SPG_XON[k]     <= #TP !fe14_csm_full_s[4];
            else if  (k==FE_P2SPG[5]  & fe_p2spg_v[5])  SPG_XON[k]     <= #TP !fe14_csm_full_s[5];
            else if  (k==FE_P2SPG[6]  & fe_p2spg_v[6])  SPG_XON[k]     <= #TP !fe14_csm_full_s[6];
            else if  (k==FE_P2SPG[7]  & fe_p2spg_v[7])  SPG_XON[k]     <= #TP !fe14_csm_full_s[7];
            else if  (k==FE_P2SPG[8]  & fe_p2spg_v[8])  SPG_XON[k]     <= #TP !fe14_csm_full_s[8];
            else if  (k==FE_P2SPG[9]  & fe_p2spg_v[9])  SPG_XON[k]     <= #TP !fe14_csm_full_s[9];
            else if  (k==FE_P2SPG[10]  & fe_p2spg_v[10]) SPG_XON[k]    <= #TP !fe14_csm_full_s[10];
            else if  (k==FE_P2SPG[11]  & fe_p2spg_v[11]) SPG_XON[k]    <= #TP !fe14_csm_full_s[11];
            else if  (k==FE_P2SPG[12]  & fe_p2spg_v[12]) SPG_XON[k]    <= #TP !fe14_csm_full_s[12];
            else if  (k==FE_P2SPG[13]  & fe_p2spg_v[13]) SPG_XON[k]    <= #TP !fe14_csm_full_s[13];
        
           end
   assign OUT = SPG_XON;
endmodule
