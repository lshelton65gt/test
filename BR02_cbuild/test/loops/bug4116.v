module foobar(dirtymsk, nindexmsk);
   input [3:0] dirtymsk;
   output [3:0] nindexmsk;
   reg [3:0] 	nindexmsk;
   reg          ena;
   integer      i;

   always @(dirtymsk) begin
      nindexmsk = 4'hf;
      ena = 1'b1;
      for (i = 0; ena && (i <= 3); i = i + 1) begin
	 nindexmsk[i] = 1'b0;
	 if ((dirtymsk[i] == 1'b1))
           ena = 1'b0;
      end
   end
endmodule
