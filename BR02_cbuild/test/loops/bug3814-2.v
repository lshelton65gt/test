module big(clk,ctl,rin,in,out,o2);
   input clk;
   input [2047:0] ctl;
   input in;
   input [127:0] rin;
   output out;
   output o2;

   reg [2047:0] storage;
   reg [31:0] 	r;

   integer i,j;

   initial storage = 0;

   assign  out = storage[2047];
   assign  o2 = |j;

   always @(posedge clk) begin
      storage[0] = in;
      for (i=2047; i>0;i=i-1)
	begin
	   // Bug 3814 documents a problem where replication of
	   // control statements was not triggering a UD update. This
	   // was only a problem if the containing loop did not
	   // unroll. This testcase was constructed such that
	   // unrolling of the outer loop does not occur.
	   
	   if(ctl[i]) begin
	      for (j=0;j<16;j=j+1)
		r[j] = rin[j];
           end

           storage[i] = storage[i-1] & (|r);
	end
   end
endmodule
