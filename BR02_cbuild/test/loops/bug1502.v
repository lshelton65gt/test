// DO NOT let the for() loop contained in this module be unrolled.
module never_unroll (clk,in,hit,address);
   input clk;
   input [511:0] in;
   output 	 hit;
   output [8:0]  address;
   reg [9:0] 	 i;
   reg [511:0] 	 in_ff;
   reg 		 hit_ff;
   reg [8:0] 	 address_int;
   reg [8:0] 	 address_int_ff;
   wire [8:0] 	 address_ff = address_int_ff;
   assign 	 hit = hit_ff;
   assign 	 address = address_ff;
   always @(posedge clk) begin : blk
      in_ff  <= in;
      hit_ff <= |in_ff;
      address_int = 9'h000;
      for ( i = 0 ; i < 512 ; i = i + 1 ) begin
	 if (in_ff[i] === 1'bx) begin
	    if(i[8] === 1'b1) address_int[8] = 1'bx;
	    if(i[7] === 1'b1) address_int[7] = 1'bx;
	    if(i[6] === 1'b1) address_int[6] = 1'bx;
	    if(i[5] === 1'b1) address_int[5] = 1'bx;
	    if(i[4] === 1'b1) address_int[4] = 1'bx;
	    if(i[3] === 1'b1) address_int[3] = 1'bx;
	    if(i[2] === 1'b1) address_int[2] = 1'bx;
	    if(i[1] === 1'b1) address_int[1] = 1'bx;
	    if(i[0] === 1'b1) address_int[0] = 1'bx;
	 end
	 if (in_ff[i] === 1'b1) begin
	    if (address_int[8] !== 1'bx) address_int[8] = address_int[8] | i[8];
	    if (address_int[7] !== 1'bx) address_int[7] = address_int[7] | i[7];
	    if (address_int[6] !== 1'bx) address_int[6] = address_int[6] | i[6];
	    if (address_int[5] !== 1'bx) address_int[5] = address_int[5] | i[5];
	    if (address_int[4] !== 1'bx) address_int[4] = address_int[4] | i[4];
	    if (address_int[3] !== 1'bx) address_int[3] = address_int[3] | i[3];
	    if (address_int[2] !== 1'bx) address_int[2] = address_int[2] | i[2];
	    if (address_int[1] !== 1'bx) address_int[1] = address_int[1] | i[1];
	    if (address_int[0] !== 1'bx) address_int[0] = address_int[0] | i[0];
	 end
      end
      address_int_ff <= address_int;
   end   
endmodule
