module dfs2 (a, b, c, d, e, clk, rst, op);
    input a;  
    input b;  
    input c;  
    input d;  
    input e;
    input clk;
    input rst;
    output op;
    
   wire    level11, level12, level21, level31, level13, level22, level32;
   wire level41;
   reg 	   level4_q;
   reg 	   RF;
   
   
   
   and level11_inst(level11, a, b);
   or level12_inst(level12, c, d);

   xor level21_inst(level21, level11, d);

   and level31_inst(level31, level21, level12);

   xor level13_inst(level13, e, level12);
   and level22_inst(level22, level21, level13);
   or level32_inst(level32, level22, RF);

   always  @(posedge clk or negedge rst)
    begin
	    if (~rst)
            begin
	            level4_q <= 1'b0;
                RF <= 1'b0;
            end
	    else
            begin
	            level4_q <= level31;
                RF <= level32;
            end
    end
   
   and level41_inst(level41, level4_q, level31);
   or (op, level41, RF);
    
endmodule
