/* 
* Simple nwk to test the abc interface.
*/

module same_fanin_po (aIn, bIn, op1, op2, op3);
   input aIn,bIn;
   output op1, op2, op3;

   wire tmp1, tmp2;
   wire result1;
   wire result2;
   wire result3;

   assign tmp1 = aIn & bIn;
   assign tmp2 = aIn | bIn;
   assign result1 = tmp1 ^ tmp2;
   assign op1 = result1;
   assign op2 = op1;

   assign result2 = result1 ^ aIn;
   assign result3 = result1 ^ bIn;

   assign op3 = result2 & result3;

endmodule // same_fanin_po
