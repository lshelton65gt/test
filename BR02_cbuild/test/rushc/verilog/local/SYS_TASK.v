/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/

// Display system task
// Section 15.5.4 of the test plan

module SYS_TASK(in1,in2,out1);
input in1,in2;
output out1;
reg out1;

  always @(in1 or in2)
  begin
    $display("Signals = ",in1,in2,out1);
    //$display("Signals = ");
    out1 = in1 & in2;
  end
endmodule
