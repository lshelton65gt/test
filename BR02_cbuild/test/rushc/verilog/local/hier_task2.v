// Test simple child task reference.
module top(out, in1, in2, clk);
    output out;
    input in1, in2, clk;
    reg tmp1;
    reg tmp2;
    initial tmp1 = 0;
    initial tmp2 = 0;
    always @(posedge clk)
    begin
        child1.doand(tmp1, in1, in2);
        child1.doand(tmp2, in2, tmp1);
    end
    child child1(out, tmp2);
endmodule

module child(o1, i);
   output o1;
   input  i;

   assign o1 = ~i;
   
   task doand;
      output o;
      input i1, i2;
      o = i1 & i2;
   endtask
endmodule // child

