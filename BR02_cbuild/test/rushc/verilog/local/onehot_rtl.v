/*********************************************************/
// MODULE:		One-Hot state machine
//
// FILE NAME:	onehot_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module shows a state machine
// implemented using one-hot encoding.
//
// NOTES:
/*********************************************************/

// DEFINES
`define DEL	1				// Clock-to-output delay. Zero
							// time delays can be confusing
							// and sometimes cause problems.

// TOP MODULE
module state_machine(
		clock,
		reset_n,
		wr,
		rd,
		ready,
		out_en,
		write_en,
		ack);

// INPUTS
input		clock;	   	// State machine clock
input		reset_n;   	// Active low, synchronous reset
input		wr;	   		// Write command from processor
input		rd;   		// Read command from processor
input		ready;		// Ready signal from memory device

// OUTPUTS
output		out_en;		// Output enable to memory
output		write_en;	// Write enable to memory
output		ack;		// Acknowledge signal to processor

// INOUTS

// SIGNAL DECLARATIONS
wire		clock;
wire		reset_n;
wire		wr;
wire		rd;
wire		ready;
reg			out_en;
reg			write_en;
reg			ack;

reg  [3:0]	mem_state;	// synthesis state_machine

// PARAMETERS
parameter[3:0]			// State machine states
	IDLE_STATE	= 4'b0001,
	WRITE_STATE	= 4'b0010,
	READ1_STATE	= 4'b0100,
	READ2_STATE	= 4'b1000;
    //4'b0111 <- any state with more than 1 bit set is unreachable.
    //"one hot" finite state machine.

parameter[3:0]			// State machine cases
	IDLE_CASE	= 4'bxxx1,
	WRITE_CASE	= 4'bxx1x,
	READ1_CASE	= 4'bx1xx,
	READ2_CASE	= 4'b1xxx;

// ASSIGN STATEMENTS

// MAIN CODE

// Look at the rising edge of clock for state transitions
always @(posedge clock or negedge reset_n) begin : fsm
	if (~reset_n)
		mem_state <= #`DEL IDLE_STATE;
	else begin
						   	// use parallel_case directive to
						   	// to show that all states are
						   	// mutually exclusive
							// use full_case directive to
							// show that any undefined states
							// are don't cares
							// use casex statement to show that
							// some bit positions are don't cares

		casex (mem_state)  	// synthesis parallel_case full_case
			IDLE_CASE:	begin
				if (wr == 1'b1)
					mem_state <= #`DEL WRITE_STATE;
				else if (rd == 1'b1)
					mem_state <= #`DEL READ1_STATE;
			end
			WRITE_CASE:	begin
				mem_state <= #`DEL IDLE_STATE;
			end
			READ1_CASE:	begin
				if (ready == 1'b1)
					mem_state <= #`DEL READ2_STATE;
			end
			READ2_CASE:	begin
				mem_state <= #`DEL IDLE_STATE;
			end
		endcase
	end
end			// fsm

// Look at changes in the state to determine outputs
always @(mem_state) begin : outputs

						   	// use parallel_case directive
						   	// to show that all states are
						   	// mutually exclusive
							// use full_case directive to
							// show that any undefined states
							// are don't cares
							// use casex statement to show that
							// some bit positions are don't cares

	casex (mem_state)		// synthesis parallel_case full_case
		IDLE_CASE:	begin
			out_en = 1'b0;
			write_en = 1'b0;
			ack = 1'b0;
		end
		WRITE_CASE:	begin
			out_en = 1'b0;
			write_en = 1'b1;
			ack = 1'b1;
		end
		READ1_CASE:	begin
			out_en = 1'b1;
			write_en = 1'b0;
			ack = 1'b0;
		end
		READ2_CASE:	begin
			out_en = 1'b1;
			write_en = 1'b0;
			ack = 1'b1;
		end
	endcase
end			// outputs
endmodule		// state_machine
