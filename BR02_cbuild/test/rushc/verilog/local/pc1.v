/*
 * Small network to test const propogation
 */

module pc1 (a, b, op);
   input a, b;
   
   output op;

   wire X1;
   wire X2;
   

   assign X1 = a | b;
   assign X2 = a | ~b;
   assign op = X1 | X2;

endmodule // pc1

   
