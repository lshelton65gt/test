/*********************************************************/
// MODULE:		Synchronous SRAM Controller
//
// FILE NAME:	ssramcon_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module implements a controller for an
// Synchronous SRAM.
//
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.
`define WAIT 2		// Number of wait cycles needed
					// Flow-through SSRAMs require
					// 	1 wait cycles
					// Pipelined SSRAMs require
					// 	2 wait cycle

// TOP MODULE
module ssram_control(
		clock,
		reset,
		as,
		ack);

// INPUTS
input				clock;	   	// State machine clock
input				reset;		// Reset
input				as;			// Address strobe

// OUTPUTS
output				ack;		// Acknowledge to processor

// INOUTS

// SIGNAL DECLARATIONS
wire				clock;
wire				reset;
wire				as;
wire				ack;

reg  [`WAIT-1:0]	shift_reg;	// Shift register

// PARAMETERS

// ASSIGN STATEMENTS
// Create the acknowledge signal
assign ack = shift_reg[`WAIT-1];

// MAIN CODE

// Look at the rising edge of clock for state transitions
always @(posedge clock or posedge reset) begin
	if (reset) begin
		 shift_reg <= #`DEL 0;
	end
	else begin
		shift_reg <= #`DEL shift_reg << 1;
		shift_reg[0] <= #`DEL as;
	end
end
endmodule		// ssram_control