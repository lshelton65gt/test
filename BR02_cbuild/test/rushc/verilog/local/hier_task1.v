// Test simple child task reference.
module top(out, in1, in2, clk);
    output out;
    input in1, in2, clk;
    reg tmp;
    initial tmp = 0;
    always @(posedge clk)
        child1.doand(tmp, in1, in2);
    child child1(out, tmp);
endmodule

module child(o1, i);
   output o1;
   input  i;

   assign o1 = ~i;
   
   task doand;
      output o;
      input i1, i2;
      o = i1 & i2;
   endtask
endmodule // child

