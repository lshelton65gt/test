/*
 Small network to test cut on
 */

module cuts_structural_equivalence_check_adder (in_a, in_b, clk, rst, op);

   input [2 : 0] in_a;
   input [2 : 0] in_b;
   input clk;
   input rst;
   
   output [2 : 0] op;

   reg [2 : 0] Register_Q; //the register q output
   wire	[2 : 0 ] Register_D; //the register d input
	  
   always @(posedge clk or negedge rst)
     begin
	if (~rst)
	  Register_Q <= 3'b0;
	else
	  Register_Q <= Register_D;
     end

   assign op = Register_Q;

   assign Register_D = in_a + in_b;

endmodule // cut1

   
