/*
 Small network to test cut on
 */

module cuts_classes_check_1 (in_a, in_b, clk, rst, op);

   input [7 : 0] in_a;
   input [7 : 0] in_b;
   input clk;
   input rst;
   
   output [7 : 0] op;

   reg [7 : 0] Register_Q; //the register q output
   wire	[7 : 0 ] Register_D; //the register d input
	  
   always @(posedge clk or negedge rst)
     begin
	if (~rst)
	  Register_Q <= 8'b0;
	else
	  Register_Q <= Register_D;
     end

   assign op = Register_Q;

   assign Register_D = in_a & in_b;

endmodule // cut1

   
