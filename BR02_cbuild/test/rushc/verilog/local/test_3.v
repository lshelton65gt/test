module test_3 (clk, rst, a_in, b_in, ip, Out);

input a_in, b_in, ip;
input clk, rst;

output Out;

wire RegDriver, candidate_1, candidate_2, candidate_3;

assign candidate_3 = ip ? ip : 1'b0;
or(candidate_1, candidate_3, candidate_2);
or(candidate_2, a_in, b_in);

reg Register;

assign RegDriver = (candidate_1 & Register);

always @(posedge clk or negedge rst)
begin
    if (! rst)
        Register <= 1'b0;
    else
        Register <= RegDriver;
end

assign Out = Register;

endmodule

