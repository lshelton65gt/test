/*
 Small network to test cut on
 */

module cuts_structural_equivalence_check_1 (in_a, in_b, clk, rst, op);

   input [1 : 0] in_a;
   input [1 : 0] in_b;
   input clk;
   input rst;
   
   output [1 : 0] op;

   reg [1 : 0] Register_Q; //the register q output
   wire	[1 : 0 ] Register_D; //the register d input
	  
   always @(posedge clk or negedge rst)
     begin
	if (~rst)
	  Register_Q <= 2'b0;
	else
	  Register_Q <= Register_D;
     end

   assign op = Register_Q;

   assign Register_D = in_a & in_b;

endmodule // cut1

   
