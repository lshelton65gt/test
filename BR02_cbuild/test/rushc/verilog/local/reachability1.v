module reachability1 (CLK,	RST, Register2_op, Register1_op, Register0_op);
							 input CLK;
							 input RST;
							 output Register2_op, Register1_op, Register0_op;
							 
							 reg Register2,Register1,Register0;
							 wire d2,d1,d0;
							 
							 //assign the outputs
							 assign Register2_op = Register2;
							 assign Register1_op = Register1;
							 assign Register0_op = Register0;
							 
							 //create the state machine:
							 //we need to check Register2, Register1, Register0 registers.
							 always @(posedge CLK )
							 begin
							 if (~RST)
							 begin
							 Register2<=0;
							 Register1 <= 0;
							 Register0 <= 0;
							 end
							 else
							 begin
							 Register2 <= d2;
							 Register1 <= d1;
							 Register0 <= d0;
							 end
	end
							 
							assign d0 = ~Register2 & ~Register1 & ~Register0 | ~Register2 & Register1 & ~Register0 | Register2 & Register1 & ~Register0;
											
							assign d1 = ~Register2 & ~Register1 & ~Register0 | ~Register2 & Register1 & ~Register0 | Register2 & ~Register0;
							assign d2 = ~Register2 & Register1 & Register0 | Register2 & ~Register0; 
endmodule
			
