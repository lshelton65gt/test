/*
 Small network to test cut on
 */

module cut1(a,b,c,d,e,clk,rst,op);
   input a,b,c,d,e,clk;
   input rst;
   
   output op;

   reg 	  op_q; //the register q output
   wire	  op_d; //the register d input
	  
   always @(posedge clk or negedge rst)
     begin
	if (~rst)
	  op_q <= 1'b0;
	else
	  op_q <= op_d;
     end

   assign op = op_q ^ (d & e);
   assign op_d = a & b & c;
endmodule // cut1

   
