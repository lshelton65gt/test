/*
 Small network to test cnf generation.
 */

module cnf2(a, b, c, d, out);
   input a, b, c, d;
   output out;

   assign out = a | ((a | b) | (c | d));

endmodule // cut1

   
