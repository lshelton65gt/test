/* 
* Simple nwk to test the abc interface.
*/

module dff (clk, set, aIn, bIn, op1, op2);
    input clk, set;
    input aIn, bIn;
    output op1, op2;

    wire Tmp1;
    reg RegFirst;
    
    assign Tmp1 = aIn & bIn;
    
   //asynchronous set
    always  @(posedge clk or set)
        begin
            if (set)
                begin
                RegFirst <= 1'b1;
                end
            else
                begin
                RegFirst <= Tmp1;
                end
        end

    
    assign op1 = RegFirst | aIn;
    assign op2 = Tmp1;

endmodule // simple_ff
