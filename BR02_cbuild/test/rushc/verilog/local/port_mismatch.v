// filename: test/rushc/verilog/port_mismatch.v
// Description:  This test has a port size mismatch 
// as of revision 950 this would not populate

module port_mismatch(clock1, clock2, clock3, in1, in2, out1);
   input clock1, clock2, clock3;// carbon observeSignal
   input [7:0] in1, in2;
   output [8:0] out1;

   foo u_foo(clock1, clock2, clock3, in1, , out1);
endmodule

module foo(clock1, clock2, clock3, in1, , out1);
   input clock1, clock2, clock3;
   input [7:0] in1;
   wire [7:0] in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock1)
     begin: sub
	reg [7:0]  main_reg;
	begin: sub
	   reg [7:0]  sub_reg;
	   begin: bottom
	      sub_reg = in1 & in2;
	   end
	   main_reg = sub_reg;
        end
	out1 = main_reg;
     end
endmodule
