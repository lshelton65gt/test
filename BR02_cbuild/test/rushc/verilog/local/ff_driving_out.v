/* 
* Simple nwk to test the abc interface.
*/

module ff_driving_out (clk, rst, aIn, bIn, op1);
    input clk, rst;
    input aIn, bIn;
    output op1;

    wire Tmp1;
    reg Reg1;
    
    always  @(posedge clk or negedge rst)
        begin
            if (~rst)
                begin
                Reg1 <= 1'b0;
                end
            else
                begin
                Reg1 <= aIn & bIn;
                end
        end

    
    assign op1 = Reg1;

endmodule // ff_driving_out
