/*
 Small network to test cnf generation.
 */

module CnfMux(a, b, selector, outA, outB);
    input selector;
    input a, b;
    output outA, outB;


    assign muxWire = selector ? a : b;
    assign outA = muxWire ^ a;
    assign outB = muxWire ^ b;

endmodule

   
