
`define RST_VALUE      (1'b1)
`define RST_EVENT      posedge

`define IDLE	2'd0

`define CFETCH	2'd1

module abc_simple (clk, rst, state, en, icqmem_cycstb_i, valid, icqmem_ci_i, read, icram_we);

    input clk, rst, state;

    input en, icqmem_cycstb_i, valid, icqmem_ci_i;

    output read;

    output [3:0] icram_we;

    reg	cache_inhibit;

    assign read = (en & icqmem_cycstb_i) | (!valid & state);

    assign icram_we[3] = read & !cache_inhibit;

    assign icram_we[2] = icram_we[3];

    assign icram_we[1] = icram_we[3];

    assign icram_we[0] = icram_we[3];

    always @(posedge clk or `RST_EVENT rst) begin
      if (rst == `RST_VALUE) begin
	    cache_inhibit <=  1'b0;
        end
      else
	    case (state)
	        `IDLE :
	            if (en & icqmem_cycstb_i) begin
	                cache_inhibit <=  icqmem_ci_i;
	                end
	            else 
	                cache_inhibit <=  1'b0;
	        `CFETCH:
                if (icqmem_cycstb_i & icqmem_ci_i) begin
	                cache_inhibit <=  1'b1;
                    end
                else
	                cache_inhibit <=  1'b0;
	    endcase
    end

endmodule
