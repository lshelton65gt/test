module param_instantiation(a0, o0);

input a0;
output o0;

parameter e = 0;

defparam c1.s = 1;
defparam c2.s = 2; 
defparam c1.gc1.k = 3;
defparam c1.gc2.k = 4;
defparam c2.gc1.k = 5;
defparam c2.gc2.k = 6;

child c1(a0, o0);
child c2(a0, o0);

endmodule

module child(ca0, co0);

input ca0;
output co0;

parameter s = 0;

gchild gc1(ca0, co0);
gchild gc2(ca0, co0);

endmodule

module gchild(gca0, gco0);

input gca0;
output gco0;

parameter k = 0;

assign gco0 = gca0;

endmodule
