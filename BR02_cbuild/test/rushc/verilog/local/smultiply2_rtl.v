/*********************************************************/
// MODULE:		signed integer multiplier
//
// FILE NAME:	smultiply2_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module defines a multiplier of signed
// integers. This code uses a large ROM to generate each
// product.
//
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.
`define OP_BITS 4	// Number of bits in each operand

// TOP MODULE
module SignedMultiply(
		clk,
		reset,
		a,
		b,
		multiply_en,
		product,
		valid);

// INPUTS
input					clk;			// Clock
input					reset;			// Reset input
input [`OP_BITS-1:0]	a;				// Multiplicand
input [`OP_BITS-1:0]	b;				// Multiplier
input					multiply_en;	// Multiply enable input

// OUTPUTS
output [2*`OP_BITS-1:0]	product;		// Product
output					valid;			// Is output valid yet?

// INOUTS

// SIGNAL DECLARATIONS
wire					clk;
wire					reset;
wire [`OP_BITS-1:0]		a;
wire [`OP_BITS-1:0]		b;
wire					multiply_en;
reg  [2*`OP_BITS-1:0]	product;
reg						valid;

wire [2*`OP_BITS-1:0]	romout;			// Output of ROM

// PARAMETERS

// ASSIGN STATEMENTS

// MAIN CODE

// Instantiate the ROM
Sm_Rom Rom(
	.address({a,b}),
	.out(romout));

// Look at the rising edge of the clock
// and the rising edge of reset
always @(posedge reset or posedge clk) begin
	if (reset)
		valid <= #`DEL 1'b0;
	else if (multiply_en)
	   	valid <= #`DEL 1'b1;
end

// Look at the rising edge of the clock
always @(posedge clk) begin
	if (multiply_en)
		product <= #`DEL romout;
end
endmodule		// SignedMultiply

// SUBMODULES
`include "sm_rom.v"
