/*********************************************************/
// MODULE:		unsigned integer multiplier
//
// FILE NAME:	umultiply1_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module defines a multiplier of
// unsigned integers. This code uses a brute force
// combinatorial method for generating the product.
//
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.
`define OP_BITS 4	// Number of bits in each operand

// TOP MODULE
module UnsignedMultiply(
		clk,
		reset,
		a,
		b,
		multiply_en,
		product,
		valid);

// INPUTS
input					clk;			// Clock
input					reset;			// Reset input
input [`OP_BITS-1:0]	a;				// Multiplicand
input [`OP_BITS-1:0]	b;				// Multiplier
input					multiply_en;	// Multiply enable input

// OUTPUTS
output [2*`OP_BITS-1:0]	product;		// Product
output					valid;			// Is the output valid?

// INOUTS

// SIGNAL DECLARATIONS
wire					clk;
wire					reset;
wire [`OP_BITS-1:0]		a;
wire [`OP_BITS-1:0]		b;
wire					multiply_en;
reg  [2*`OP_BITS-1:0]	product;
reg						valid;

// PARAMETERS

// ASSIGN STATEMENTS

// MAIN CODE

// Look at the rising edge of the clock
// and the rising edge of reset
always @(posedge reset or posedge clk) begin
	if (reset)
		valid <= #`DEL 1'b0;
	else if (multiply_en)
	   	valid <= #`DEL 1'b1;
end

// Look at the rising edge of the clock
always @(posedge clk) begin
	if (multiply_en)
		product <= #`DEL a*b;
end
endmodule		// UnsignedMultiply
