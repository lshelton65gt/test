/* 
* Test case for nucleus.
*/

module and2  (aIn, bIn, op1);
   input aIn,bIn;
   output op1;

   assign op1 = aIn & bIn;

endmodule 
