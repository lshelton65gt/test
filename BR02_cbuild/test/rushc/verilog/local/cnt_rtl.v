/*********************************************************/
// MODULE:		up/down counter
//
// FILE NAME:	cnt_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module defines an up/down counter with
// asynchronous set and reset inputs, and synchronous load,
// up/down control, and count enable
//
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.
`define BITS 8		// Number of bits in counter

// TOP MODULE
module	Counter(
		clk,
		in,
		reset_n,
		preset_n,
		load,
		up_down,
		count_en,
		out,
		carry_out);

// INPUTS
input				clk;		// Clock
input [`BITS-1:0]	in;			// Input
input				reset_n;	// Active low,
								// asynchronous reset
input				preset_n;	// Active low,
								// asynchronous preset
input				load;		// Synchronous load input
input				up_down;	// Synchronous up/down control
input				count_en;	// Synchronous count
								// enable control

// OUTPUTS
output [`BITS-1:0]	out;		// Output
output				carry_out;	// Carry output

// INOUTS

// SIGNAL DECLARATIONS
wire				clk;
wire [`BITS-1:0]	in;
wire				reset_n;
wire				preset_n;
wire				load;
wire				up_down;
wire				count_en;
reg  [`BITS-1:0]   	out;
reg					carry_up;
reg					carry_dn;
wire				carry_out;	// The carry out is generated
								// from two registers for
								// better clock to output
								// timing

// PARAMETERS

// ASSIGN STATEMENTS
assign #`DEL carry_out = up_down ? carry_up : carry_dn;

// MAIN CODE

// Look at the rising edge of clock for state transitions
always @(posedge clk or negedge reset_n or negedge preset_n) begin

	carry_up <= #`DEL 1'b0;
	carry_dn <= #`DEL 1'b0;

	if (~reset_n) begin
		// This is the reset condition.
		out <= #`DEL `BITS'h0;
		carry_dn <= #`DEL 1'b1;
	end
	else if (~preset_n) begin
		// This is the preset condition. Note that in this
		// implementation, the reset has priority over preset
		out <= #`DEL ~`BITS'h0;
		carry_up <= #`DEL 1'b1;
	end
	else if (load) begin
		// In this implementation, load has priority over
		// count enable
		out <= #`DEL in;
		if (in == ~`BITS'h0)
			carry_up <= #`DEL 1'b1;
		else if (in == `BITS'h0)
			carry_dn <= #`DEL 1'b1;
	end
	else if (count_en) begin
		if (up_down) begin
			out <= #`DEL out+1;
			if (out == ~`BITS'h1)
				carry_up <= #`DEL 1'b1;
		end
		else begin
			out <= #`DEL out-1;
			if (out == `BITS'h1)
				carry_dn <= #`DEL 1'b1;
		end
	end
end
endmodule		// Counter
