// filename: test/rushc/verilog/unsupported_1.v
// Description:  This test checks to see that we do not get a crash if $display
// is used without -enableOutputSysTasks, so this test must be run without that switch


module unsupported_1(clock, in1, in2, out1);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
	$display(out1);
     end
endmodule
