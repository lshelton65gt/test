/*********************************************************/
// MODULE:		unsigned integer multiplier
//
// FILE NAME:	umultiply3_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module defines a multiplier of
// unsigned integers. This code uses a shift register and
// partial sums.
//
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.
`define OP_BITS 4	// Number of bits in each operand
`define OP_SIZE 2	// Number of bits required to
					// represent the OP_BITS constant

// TOP MODULE
module UnsignedMultiply(
		clk,
		reset,
		a,
		b,
		multiply_en,
		product,
		valid);

// INPUTS
input					clk;			// Clock
input					reset;			// Reset input
input [`OP_BITS-1:0]	a;				// Multiplicand
input [`OP_BITS-1:0]	b;				// Multiplier
input					multiply_en;	// Multiply enable input

// OUTPUTS
output [2*`OP_BITS-1:0]	product;		// Product
output					valid;			// Is the output valid?

// INOUTS

// SIGNAL DECLARATIONS
wire					clk;
wire					reset;
wire [`OP_BITS-1:0]		a;
wire [`OP_BITS-1:0]		b;
wire					multiply_en;
reg  [2*`OP_BITS-1:0]	product;
wire					valid;

reg  [`OP_SIZE-1:0]		count;			// Count the number
										// of bit shifts

// PARAMETERS

// ASSIGN STATEMENTS
assign valid = (count == 0) ? 1'b1 : 1'b0;

// MAIN CODE

// Look at the rising edge of the clock
// and the rising edge of reset
always @(posedge reset or posedge clk) begin
	if (reset) begin
		count <= #`DEL `OP_SIZE'b0;
	end
	else begin
		if (multiply_en && valid) begin
			// Put the count at all ones - the maximum
			count <= #`DEL ~`OP_SIZE'b0;
		end
		else if (count)
			count <= #`DEL count - 1;
	end
end

// Look at the rising edge of the clock
always @(posedge clk) begin
	if (multiply_en & valid) begin
		if (b[`OP_BITS-1]) begin
			// If the MSB of the multiplier is 1,
			// load the multiplicand into the product
			product <= #`DEL a;
		end
		else begin
			// If the MSB of the multiplier is 0,
			// load 0 into the product
			product <= #`DEL 0;
		end
	end
	else if (count) begin
		if (b[count-1]) begin
			// If this bit of the multiplier is 1,
			// shift the product left and add the
			// multiplicand
			product <= #`DEL (product << 1) + a;
		end
		else begin
			// If this bit of the multiplier is 0,
			// just shift the product left
			product <= #`DEL product << 1;
		end
	end
end
endmodule		// UnsignedMultiply
