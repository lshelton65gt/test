/* 
* Simple nwk to test the abc interface.
*/

module test (aIn, bIn, op1, op2);
   input aIn, bIn;
   output op1, op2;

   wire tmp1, tmp2;
   assign op1 = aIn | bIn;
   assign op2 = op1;

endmodule // test
