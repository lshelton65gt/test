module port1(in1, in2, out1);
input [7:0] in1, in2;
output [7:0] out1;
mid u1(.in2 (in2), .out1 (out1), .in1 (in1)  );
endmodule


module mid (input [7:0] in1, input [7:0] in2, output [7:0] out1);
assign                 out1 = in1 + in2;
endmodule
