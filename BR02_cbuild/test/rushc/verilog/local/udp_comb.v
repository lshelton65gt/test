module no_inst_name(in1, in2, out1) ;
   input  in1, in2;
   output  out1;
   subudp (out1, in1, in2) ;             // udps don't need an instance name

endmodule

 
primitive subudp(out1, in1, in2) ;
   output  out1;
   input  in1, in2;

   table
      0 0 : 1;
      1 1 : 0;
      x x : 0;
   endtable
endprimitive
