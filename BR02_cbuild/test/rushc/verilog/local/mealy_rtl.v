/*********************************************************/
// MODULE:		Mealy state machine
//
// FILE NAME:	mealy_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module shows a state machine
// implementation.
//
// NOTES: This is a Mealy model of a state machine, since
// the outputs are defined by the current state.
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.

// TOP MODULE
module state_machine(
		clock,
		reset_n,
		wr,
		rd,
		ready,
		out_en,
		write_en,
		ack);

// INPUTS
input		clock;	   	// State machine clock
input		reset_n;   	// Active low, synchronous reset
input		wr;	   		// Write command from processor
input		rd;   		// Read command from processor
input		ready;		// Ready signal from memory device

// OUTPUTS
output		out_en;		// Output enable to memory
output		write_en;	// Write enable to memory
output		ack;		// Acknowledge signal to processor

// INOUTS

// SIGNAL DECLARATIONS
wire		clock;
wire		reset_n;
wire		wr;
wire		rd;
wire		ready;
reg			out_en;
reg			write_en;
reg			ack;

reg  [1:0]	mem_state;	// synthesis state_machine

// PARAMETERS
parameter[1:0]			// State machine states
	IDLE	= 0,
	WRITE	= 1,
	READ	= 2;

// ASSIGN STATEMENTS

// MAIN CODE

// Look at the rising edge of clock for state transitions
always @(posedge clock or negedge reset_n) begin : fsm
	if (~reset_n) begin
		mem_state <= #`DEL IDLE;
		out_en <= #`DEL 1'b0;
		write_en <= #`DEL 1'b0;
		ack <= #`DEL 1'b0;
	end
	else begin
						   	// use parallel_case directive
						   	// to show that all states are
						   	// mutually exclusive

		case (mem_state)  	// synthesis parallel_case
			IDLE:	begin
				// Do not do anything if we are
				// acknowledging a previous access
				if (~ack && (wr == 1'b1)) begin
					mem_state <= #`DEL WRITE;
					out_en <= #`DEL 1'b0;
					write_en <= #`DEL 1'b1;
					ack <= #`DEL 1'b1;
				end
				else if (~ack && (rd == 1'b1)) begin
					mem_state <= #`DEL READ;
					out_en <= #`DEL 1'b1;
					write_en <= #`DEL 1'b0;
					ack <= #`DEL 1'b0;
				end
				else begin
					out_en <= #`DEL 1'b0;
					write_en <= #`DEL 1'b0;
					ack <= #`DEL 1'b0;
				end
			end
			WRITE:	begin
				mem_state <= #`DEL IDLE;
				out_en <= #`DEL 1'b0;
				write_en <= #`DEL 1'b0;
				ack <= #`DEL 1'b0;
			end
			READ:	begin
				if (ready == 1'b1) begin
					mem_state <= #`DEL IDLE;
					out_en <= #`DEL 1'b1;
					write_en <= #`DEL 1'b0;
					ack <= #`DEL 1'b1;
				end
				else begin
					out_en <= #`DEL 1'b1;
					write_en <= #`DEL 1'b0;
					ack <= #`DEL 1'b0;
				end
			end
			default: begin	// Since not all states are defined,
							// include a default state to
							// optimize synthesis
				mem_state <= #`DEL 2'bxx;
				out_en <= #`DEL 1'bx;
				write_en <= #`DEL 1'bx;
				ack <= #`DEL 1'bx;
			end
		endcase
	end
end		// fsm
endmodule		// state_machine
