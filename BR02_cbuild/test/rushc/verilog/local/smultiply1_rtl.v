/*********************************************************/
// MODULE:		signed integer multiplier
//
// FILE NAME:	smultiply1_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module defines a multiplier of signed
// integers.  This code uses a brute force combinatorial
// method for generating the product.
//
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.
`define OP_BITS 4	// Number of bits in each operand

// TOP MODULE
module SignedMultiply(
		clk,
		reset,
		a,
		b,
		multiply_en,
		product,
		valid);

// INPUTS
input					clk;			// Clock
input					reset;			// Reset input
input [`OP_BITS-1:0]	a;				// Multiplicand
input [`OP_BITS-1:0]	b;				// Multiplier
input					multiply_en;	// Multiply enable input

// OUTPUTS
output [2*`OP_BITS-1:0]	product;		// Product
output					valid;			// Is output valid yet?

// INOUTS

// SIGNAL DECLARATIONS
wire					clk;
wire					reset;
wire [`OP_BITS-1:0]		a;
wire [`OP_BITS-1:0]		b;
wire					multiply_en;
reg  [2*`OP_BITS-1:0]	product;
reg						valid;

reg  [`OP_BITS-1:0]		a_abs;			// Absolute value of a
reg  [`OP_BITS-1:0]		b_abs;			// Absolute value of b

// PARAMETERS

// ASSIGN STATEMENTS

// MAIN CODE

// Look at the rising edge of the clock
// and the rising edge of reset
always @(posedge reset or posedge clk) begin
	if (reset)
		valid <= #`DEL 1'b0;
	else if (multiply_en)
		valid <= #`DEL 1'b1;
end

// Look at the rising edge of the clock
always @(posedge clk) begin
	if (multiply_en) begin
		// Look at the sign bits, then multiply
		// their absolute values and negate the
		// result if necessary
		case ({a[`OP_BITS-1],b[`OP_BITS-1]})
			2'b00: begin
				product <= #`DEL a*b;
			end
			2'b01: begin
				b_abs = ~b + 1;
				product <= #`DEL ~(a*b_abs)+1;
			end
			2'b10: begin
				a_abs = ~a + 1;
				product <= #`DEL ~(a_abs*b)+1;
			end
			2'b11: begin
				a_abs = ~a + 1;
				b_abs = ~b + 1;
				product <= #`DEL a_abs*b_abs;
			end
		endcase
	end
end
endmodule		// SignedMultiply
