/*********************************************************/
// MODULE:		encryption and decryption
//
// FILE NAME:	encr_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module defines a data
// encrypter/decrypter
//
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.

					// These are good tap values for 2 to 32 bits
`define TAP2	2'b11
`define TAP3	3'b101
`define TAP4	4'b1001
`define TAP5	5'b10010
`define TAP6	6'b100001
`define TAP7	7'b1000001
`define TAP8	8'b10001110
`define TAP9	9'b100001000
`define TAP10	10'b1000000100
`define TAP11	11'b10000000010
`define TAP12	12'b100000101001
`define TAP13	13'b1000000001101
`define TAP14	14'b10000000010101
`define TAP15	15'b100000000000001
`define TAP16	16'b1000000000010110
`define TAP17	17'b10000000000000100
`define TAP18	18'b100000000001000000
`define TAP19	19'b1000000000000010011
`define TAP20	20'b10000000000000000100
`define TAP21	21'b100000000000000000010
`define TAP22	22'b1000000000000000000001
`define TAP23	23'b10000000000000000010000
`define TAP24	24'b100000000000000000001101
`define TAP25	25'b1000000000000000000000100
`define TAP26	26'b10000000000000000000100011
`define TAP27	27'b100000000000000000000010011
`define TAP28	28'b1000000000000000000000000100
`define TAP29	29'b10000000000000000000000000010
`define TAP30	30'b100000000000000000000000101001
`define TAP31	31'b1000000000000000000000000000100
`define TAP32	32'b10000000000000000000000001100010

`define WORD_SZ 8  	// Number of bits per word
`define TAPS `TAP8	// This must be the taps for the 
					// number of bits specified above

// TOP MODULE
module	Encrypt(
		clk,
		load,
		key,
		data_in,
		data_out);

// INPUTS
input					clk;		// Clock
input					load;		// Load the key
input [`WORD_SZ-1:0]	key;		// Encryption key
input [`WORD_SZ-1:0]	data_in;   	// Input data

// OUTPUTS
output [`WORD_SZ-1:0] 	data_out;	// Encrypted data

// INOUTS

// SIGNAL DECLARATIONS
wire				clk;
wire				load;
wire [`WORD_SZ-1:0]	key;
wire [`WORD_SZ-1:0]	data_in;
wire [`WORD_SZ-1:0]	data_out;

reg  [`WORD_SZ-1:0]	lfsr;			// LFSR data

// PARAMETERS

// ASSIGN STATEMENTS
									// Encrypt the data
assign #`DEL data_out = lfsr ^ data_in;

// MAIN CODE

// Look at the rising edge of clock for state transitions
always @(posedge clk) begin
	if (load) begin
		// Load the key into the LFSR
		lfsr <= #`DEL key;
	end
	else begin
		// Create the next random word for encryption

		// Shift all of the bits left
		lfsr[`WORD_SZ-1:1] <= #`DEL lfsr[`WORD_SZ-2:0];

		// Create the new bit 0
		lfsr[0] <= #`DEL ^(lfsr & `TAPS);
	end
end
endmodule		// Encrypt
