/*********************************************************/
// MODULE:		adder
//
// FILE NAME:	add_rtl.v
// VERSION:		1.0
// DATE:		January 1, 1999
// AUTHOR:		Bob Zeidman, Zeidman Consulting
// 
// CODE TYPE:	Register Transfer Level
//
// DESCRIPTION:	This module defines an adder with
// synchronous add enable and reset inputs. When the adder
// is synchronously reset, the outputs go to zero and the
// valid signal is asserted on the next clock cycle. When
// the add enable input is asserted and the valid output is
// asserted during the same clock cycle, the adder begins
// adding. When the valid output signal is again asserted
// on a subsequent clock cycle, the new output is correct.
// Note that the inputs must be held steady from the cycle
// during which the add enable input is asserted until the
// cycle during which the valid output signal is asserted.
//
/*********************************************************/

// DEFINES
`define DEL	1		// Clock-to-output delay. Zero
					// time delays can be confusing
					// and sometimes cause problems.

// TOP MODULE
module Adder(
		clk,
		a,
		b,
		reset_n,
		add_en,
		out,
		cout,
		valid);

// INPUTS
input			clk;		// Clock
input [31:0]	a;			// 32-bit A input
input [31:0]	b;			// 32-bit B input
input			reset_n;	// Active low, synchronous reset
input			add_en;		// Synchronous add enable control

// OUTPUTS
output [31:0]	out;		// 32-bit output
output 			cout;		// Carry output
output 			valid;		// Is the output valid yet?

// INOUTS

// SIGNAL DECLARATIONS
wire			clk;
wire [31:0]		a;
wire [31:0]		b;
wire			reset_n;
wire			add_en;
wire [31:0]		out;
wire 			cout;
wire 			valid;
wire [7:0]		cout4;		// Carry output of 4-bit adder
reg  [2:0]		valid_cnt;	// Counter to determine when the
							// output is valid

// PARAMETERS

// ASSIGN STATEMENTS
assign #`DEL cout = cout4[7];
assign #`DEL valid = ~|valid_cnt;

// MAIN CODE

// Instantiate eight 4-bit adders
Adder_4bit Add0(
		.clk(clk),
		.a(a[3:0]),
		.b(b[3:0]),
		.cin(1'b0),
		.reset_n(reset_n),
		.add_en(add_en),
		.out(out[3:0]),
		.cout(cout4[0]));

Adder_4bit Add1(
		.clk(clk),
		.a(a[7:4]),
		.b(b[7:4]),
		.cin(cout4[0]),
		.reset_n(reset_n),
		.add_en(add_en),
		.out(out[7:4]),
		.cout(cout4[1]));

Adder_4bit Add2(
		.clk(clk),
		.a(a[11:8]),
		.b(b[11:8]),
		.cin(cout4[1]),
		.reset_n(reset_n),
		.add_en(add_en),
		.out(out[11:8]),
		.cout(cout4[2]));

Adder_4bit Add3(
		.clk(clk),
		.a(a[15:12]),
		.b(b[15:12]),
		.cin(cout4[2]),
		.reset_n(reset_n),
		.add_en(add_en),
		.out(out[15:12]),
		.cout(cout4[3]));

Adder_4bit Add4(
		.clk(clk),
		.a(a[19:16]),
		.b(b[19:16]),
		.cin(cout4[3]),
		.reset_n(reset_n),
		.add_en(add_en),
		.out(out[19:16]),
		.cout(cout4[4]));

Adder_4bit Add5(
		.clk(clk),
		.a(a[23:20]),
		.b(b[23:20]),
		.cin(cout4[4]),
		.reset_n(reset_n),
		.add_en(add_en),
		.out(out[23:20]),
		.cout(cout4[5]));

Adder_4bit Add6(
		.clk(clk),
		.a(a[27:24]),
		.b(b[27:24]),
		.cin(cout4[5]),
		.reset_n(reset_n),
		.add_en(add_en),
		.out(out[27:24]),
		.cout(cout4[6]));

Adder_4bit Add7(
		.clk(clk),
		.a(a[31:28]),
		.b(b[31:28]),
		.cin(cout4[6]),
		.reset_n(reset_n),
		.add_en(add_en),
		.out(out[31:28]),
		.cout(cout4[7]));

// Look at the rising edge of the clock
always @(posedge clk) begin
	if (~reset_n) begin
		// Initialize the valid counter
		valid_cnt <= #`DEL 3'h0;
	end
	else if (((valid_cnt == 3'h0) && (add_en == 1'b1)) ||
		  (valid_cnt != 3'h0)) begin
		// Increment the valid counter
		// if valid and add_en are asserted
		// or if valid is not asserted
		valid_cnt <= #`DEL valid_cnt + 1;
	end
end
endmodule		// Adder

// SUB MODULE
module	Adder_4bit(
		clk,
		a,
		b,
		reset_n,
		add_en,
		cin,
		out,
		cout);

// INPUTS
input			clk;		// Clock
input [3:0]		a;			// 4-bit A input
input [3:0]		b;			// 4-bit B input
input			cin;		// Carry in
input			reset_n;	// Active low, synchronous reset
input			add_en;		// Synchronous add enable control

// OUTPUTS
output [3:0]	out;		// 4-bit output
output 			cout;		// Carry output

// INOUTS

// SIGNAL DECLARATIONS
wire			clk;
wire [3:0]		a;
wire [3:0]		b;
wire			cin;
wire			reset_n;
wire			add_en;
reg  [3:0]		out;
reg 			cout;

// ASSIGN STATEMENTS

// PARAMETERS

// MAIN CODE

// Look at the rising edge of the clock
always @(posedge clk) begin
	if (~reset_n) begin
		{cout,out} <= #`DEL 33'h00000000;
	end
	else if (add_en) begin
		{cout,out} <= #`DEL a+b+cin;
	end
end
endmodule			// Adder_4bit
