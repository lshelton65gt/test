// filename: test/rushc/verilog/beacon_verilog2001/carbon_FUNCTION1.v
// Description:  This test is derived from the file
//  verilog2001/ignore/FUNCTION/FUNCTION1/FUNCTION1
// that file had several inorrect issues, it did not declare the proper width
// for depth, and it addressed address_bus with the wrong range.

module carbon_FUNCTION1(address_bus,out1);
   output out1;
   parameter RAM_SIZE = 1024;
   //NOTE: Vector widths can be calculated using complex constant function
   //defined below.
   input [clogb2(RAM_SIZE)-1:0] address_bus;
   function integer clogb2;
      input [31:0] 		depth;
      integer 			i;
      begin
         clogb2 = 1;
         for (i =0; 2**i < depth ; i = i+1)
           clogb2 = i+1;
         $display(" i= %d,clogb2= %b\n",i,clogb2);
      end
   endfunction
   assign out1 = address_bus[1:0];
endmodule
