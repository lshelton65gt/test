
// (c) Copyright Electra Design Automation / Verific Design Automation.
// Use, disclosure or distribution is prohibited
// without prior written permission of Electra Design Automation / Verific Design Automation.

// TYPE: SYNTHESIZABLE

// TESTING FEATURE(S): This design uses always_comb block to introduce procedural statements.
// always_comb block introduces combinational logic.

module test(input [7:0] in1, in2, output reg [7:0] out);

reg [7:0] r1, r2;

always_comb
begin
    r1 = in1[7-:8] ^ in2[0+:8];
    r2 = r1 & (in1 | in2);
    out = r1 | r2;
end

endmodule

