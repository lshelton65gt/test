LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

entity vh is
  port (
    clk : in std_ulogic;
    in1 : in std_ulogic;
    out1: out std_ulogic
    );
end vh;

architecture test_arch of vh is
begin
  process(clk)
  begin
    if (clk'event and clk = '1') then
      out1 <= in1;
    end if;
  end process;
end test_arch;
