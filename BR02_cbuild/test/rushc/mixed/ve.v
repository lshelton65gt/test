module ve(clk, in, out);

  input clk, in;
  output out;

  vh test_vh(.clk(clk), .in1(in), .out1(out));

endmodule
