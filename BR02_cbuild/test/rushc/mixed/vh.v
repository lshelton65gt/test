
module vh(clk, in1, out1);

  input clk, in1;
  output out1;
  reg out1;

  always @(posedge clk)
  begin
      out1 <= in1;
  end

endmodule
