#!/bin/bash
USE_GOLD_SIM_FILES=1

#
#   This is the script that writes a new 'test.run' command
#    file for this testsuite
#
#   This is run by the test harness *every* time this testsuite is
#   run.  It is invoked like this:
#		./write-test.run  >  test.run
#

# in this directory we check for CARBON_INTERNAL_ERROR in any log file, none should exist
DO_CHECK_FOR_CARBON_INTERNAL_ERROR=1

		###
		###   Define the testlists
		###

# XPASS_*  tests are passing
# XPASS_QUICK is run when -quick specified, XPASS_QUICK + XPASS_SLOW is run if -quick is not specified,  move tests to XPASS_SLOW when you want to run them less often
XPASS_QUICK=" \
"
# Steve Lim 2013-10-30: moved proc_call103 and proc_call103_bv from XTSKIP to XPASS_SLOW. I have
#                       added their sim.log.gold.gz files.

XPASS_SLOW=" \
sequential/if21/if21 \
sequential/if23/if23 \
sequential/case1/case1 \
sequential/case11/case11 \
sequential/case14/case14 \
sequential/case17/case17 \
sequential/case18/case18 \
sequential/case18_bv/case18_bv \
sequential/case19/case19 \
sequential/case2/case2 \
sequential/case20/case20 \
sequential/case200/case200 \
sequential/case200_bv/case200_bv \
sequential/case201/case201 \
sequential/case203/case203 \
sequential/case204/case204 \
sequential/case205/case205 \
sequential/case21/case21 \
sequential/case22/case22 \
sequential/case25/case25 \
sequential/case3/case3 \
sequential/case4/case4 \
sequential/case5/case5 \
sequential/case6/case6 \
sequential/case8/case8 \
sequential/case_stat13/case_stat13 \
sequential/case_stat14/case_stat14 \
sequential/case_stat15/case_stat15 \
sequential/case_stat16/case_stat16 \
sequential/case_stat17/case_stat17 \
sequential/case_stat18/case_stat18 \
sequential/case_stat19/case_stat19 \
sequential/case_stat20/case_stat20 \
sequential/case_stat21/case_stat21 \
sequential/case_stat22/case_stat22 \
sequential/exit1/exit1 \
sequential/for10/for10 \
sequential/for11/for11 \
sequential/for12/for12 \
sequential/for13/for13 \
sequential/for14/for14 \
sequential/for15/for15 \
sequential/for16/for16 \
sequential/for17/for17 \
sequential/for18/for18 \
sequential/for19/for19 \
sequential/for2/for2 \
sequential/for3/for3 \
sequential/for4/for4 \
sequential/for5/for5 \
sequential/for6/for6 \
sequential/for7/for7 \
sequential/for8/for8 \
sequential/for9/for9 \
sequential/for_loop100/for_loop100 \
sequential/if10/if10 \
sequential/if11/if11 \
sequential/if7/if7 \
sequential/if8/if8 \
sequential/if12/if12 \
sequential/if29/if29 \
sequential/if150/if150 \
sequential/if151/if151 \
sequential/if16/if16 \
sequential/if17/if17 \
sequential/if18/if18 \
sequential/if19/if19 \
sequential/if19_bv/if19_bv \
sequential/if20/if20 \
sequential/if20_bv/if20_bv \
sequential/if22/if22 \
sequential/if24/if24 \
sequential/if24_bv/if24_bv \
sequential/if25/if25 \
sequential/if27/if27 \
sequential/if4/if4 \
sequential/if9/if9 \
sequential/loop12/loop12 \
sequential/loop13/loop13 \
sequential/next1/next1 \
sequential/null1/null1 \
sequential/proc_call100/proc_call100 \
sequential/proc_call101/proc_call101 \
sequential/proc_call102/proc_call102 \
sequential/proc_call102_bv/proc_call102_bv \
sequential/proc_call103/proc_call103 \
sequential/proc_call103_bv/proc_call103_bv \
sequential/procedure1/procedure1 \
sequential/procedure10/procedure10 \
sequential/procedure2/procedure2 \
sequential/procedure2_bv/procedure2_bv \
sequential/procedure3/procedure3 \
sequential/procedure4/procedure4 \
sequential/sig_asgn1/sig_asgn1 \
sequential/sig_asgn1_bv/sig_asgn1_bv \
sequential/signal1/signal1 \
sequential/signal10/signal10 \
sequential/signal11/signal11 \
sequential/signal12/signal12 \
sequential/signal13/signal13 \
sequential/signal1_bv/signal1_bv \
sequential/signal2/signal2 \
sequential/signal3/signal3 \
sequential/signal4/signal4 \
sequential/signal5/signal5 \
sequential/signal5_bv/signal5_bv \
sequential/signal7/signal7 \
sequential/signal8/signal8 \
sequential/signal9/signal9 \
sequential/signal9_bv/signal9_bv \
sequential/var_asgn1/var_asgn1 \
sequential/variable1/variable1 \
sequential/variable10/variable10 \
sequential/variable11/variable11 \
sequential/variable12/variable12 \
sequential/variable13/variable13 \
sequential/variable1_bv/variable1_bv \
sequential/variable2/variable2 \
sequential/variable3/variable3 \
sequential/variable4/variable4 \
sequential/variable5/variable5 \
sequential/variable5_bv/variable5_bv \
sequential/variable6/variable6 \
sequential/variable8/variable8 \
sequential/variable9/variable9 \
sequential/variable9_bv/variable9_bv \
sequential/wait1/wait1 \
sequential/wait2/wait2 \
sequential/wait3/wait3 \
sequential/variable7/variable7 \
sequential/procedure13/procedure13 \
sequential/procedure12/procedure12 \
sequential/case12/case12 \
"

# these tests use some feature that keeps them from running with testdriver
XPASSNOSIM_QUICK=" \
"
XPASSNOSIM_SLOW=" \
"


# Steve Lim 2013-12-17 These have multiple wait statements and Verific reports
# there as unsupported.
XTEXIT_QUICK=" \
sequential/if1/if1 \
sequential/if1_bv/if1_bv \
"

XTEXIT_SLOW=" \
"

# sequential/for20/for20  is a bad testcase filename is for20, but defines for23 in file
# sequential/loop3_bv/loop3_bv  and many others use multiple wait stmts in a process (a construct not supported)

# This group is for tests that are not supported, or for testcases that are bad
XPEXIT_QUICK=" \
sequential/loop3/loop3 \
"

# Steve Lim 2013-10-30 Moved case15, case15_bv,case16,case16_bv,loop10, loop10_bv, loop11 from XTSKIP
# Steve Lim 2013-10-30 Moved loop1, loop1_bv, loop2, loop2_bv from XTEXIT
#            
XPEXIT_SLOW=" \
sequential/loop1/loop1 \
sequential/loop1_bv/loop1_bv \
sequential/loop2/loop2 \
sequential/loop2_bv/loop2_bv \
sequential/case15/case15 \
sequential/case15_bv/case15_bv \
sequential/case16/case16 \
sequential/case16_bv/case16_bv \
sequential/case10/case10 \
sequential/case10_bv/case10_bv \
sequential/for20/for20 \
sequential/loop3_bv/loop3_bv \
sequential/loop4/loop4 \
sequential/loop4_bv/loop4_bv \
sequential/loop5/loop5 \
sequential/loop5_bv/loop5_bv \
sequential/loop6/loop6 \
sequential/loop6_bv/loop6_bv \
sequential/loop7/loop7 \
sequential/loop7_bv/loop7_bv \
sequential/loop8/loop8 \
sequential/loop8_bv/loop8_bv \
sequential/loop9/loop9 \
sequential/loop9_bv/loop9_bv \
sequential/loop10/loop10 \
sequential/loop10_bv/loop10_bv \
sequential/loop11/loop11 \
sequential/loop11_bv/loop11_bv \
sequential/if14/if14 \
sequential/if14_bv/if14_bv \
sequential/if15/if15 \
sequential/if152/if152 \
sequential/if152_bv/if152_bv \
sequential/if15_bv/if15_bv \
sequential/while01/while01 \
sequential/while010/while010 \
sequential/while010_bv/while010_bv \
sequential/while011/while011 \
sequential/while011_bv/while011_bv \
sequential/while012/while012 \
sequential/while012_bv/while012_bv \
sequential/while013/while013 \
sequential/while013_bv/while013_bv \
sequential/while01_bv/while01_bv \
sequential/while02/while02 \
sequential/while02_bv/while02_bv \
sequential/while03_bv/while03_bv \
sequential/while03/while03 \
sequential/while04/while04 \
sequential/while04_bv/while04_bv \
sequential/while05/while05 \
sequential/while05_bv/while05_bv \
sequential/while06/while06 \
sequential/while06_bv/while06_bv \
sequential/while07/while07 \
sequential/while07_bv/while07_bv \
sequential/while08/while08 \
sequential/while08_bv/while08_bv \
sequential/while09/while09 \
sequential/while09_bv/while09_bv \
sequential/while1/while1 \
sequential/while11/while11 \
sequential/while11_bv/while11_bv \
sequential/while1_bv/while1_bv \
sequential/while5/while5 \
sequential/while5_bv/while5_bv \
sequential/while7/while7 \
sequential/while8/while8 \
sequential/while9/while9 \
sequential/while9_bv/while9_bv \
"

# These tests currently have a simulation difference (between verific and interra)
XTSIMDIFF_QUICK=" \
"

XTSIMDIFF_SLOW=" \
"


# the XPASS_SIMONLY tests are run with verific parser and the simulation results are compared with a gold sim file.  
# This group is used for testcases that do not match simulation results from Interra flow because interra flow is incorrect
XPASS_SIMONLY_QUICK="\
"

XPASS_SIMONLY_SLOW="\
"

# sequential/if26/if26 seems to have a backend compilation error in interra flow.  This needs to be investigated (bug16441)
# sequential/case15/case15 not supported   multiple wait statements
# sequential/loop1/loop1  infinite loop during population
# sequential/loop10/loop10  infinite loop during population
# sequential/loop1_bv/loop1_bv  infinite loop during population
# sequential/loop2/loop2 infinite loop during population

# Steve Lim 2013-11-01 Update to the above comments:
# if26 still has the backend compilation error being tracked in bug16441.
# case15,loop1,loop10,loop1_bv,loop2 no longer have an infinite loop problem
# - these are negative tests that fail on unsupported VHDL constructs (multiple
#   wait statements in a process or wait statement in a loop); their 
#   verific.cbld.log.gold files had been updated and these had been moved to
#   XPEXIT.

XTSKIP="\
sequential/if26/if26 \
"


# XTSHOULD_FAIL_VERIFIC_* is a list of tests that exits non-zero from Interra flow because either bad testcase or unsupported construct, currently the Verific flow does not produce an error message but it should.
XTSHOULD_FAIL_VERIFIC_QUICK="\
"

XTSHOULD_FAIL_VERIFIC_SLOW="\
"

HDLExt="vhdl"
source $CARBON_HOME/test/rushc/Common/write-test.common

			### end-of-file  ###
