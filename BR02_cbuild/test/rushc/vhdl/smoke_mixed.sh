#!/bin/tcsh -f

# NOTES
# Tests should always be run from vhdl directory (for vhdl libraries path)
# Currently supported only verilog on top case
# test files should be separated by ; like 
#       set test1 = "../verilog/mixed.v;./sig_var.vhd"
#       set test2 = "../verilog/other.v;./simple.vhd"
# tests variable should be updated to include more test variables separated by space, like
#       set tests = "${test1} ${test2} ${test3}"
set test1 = "../verilog/mixed.v;./sig_var.vhd"
set tests = "${test1}"

setenv CARBON_BIN product
foreach i (${tests})
    set ctest=`echo ${i} | sed "s/;/ /g"`
    rm -f design.exe design_verific.exe design_interra.exe interra.txt verific.txt 
    cbuild -q ${ctest} -useVerific -testdriverDumpVCD >& /dev/null
    mv design.exe design_verific.exe
    ./design_verific.exe >& verific.txt
    rm design.exe -f
    cbuild -q ${ctest} -testdriverDumpVCD >& /dev/null
    mv design.exe design_interra.exe
    ./design_interra.exe >& interra.txt
    diff -q ./verific.txt ./interra.txt >& /dev/null
    if ($status != 0) then
        echo "${ctest} - FAIL"
    else
        echo "${ctest} - PASS"
    endif
end
