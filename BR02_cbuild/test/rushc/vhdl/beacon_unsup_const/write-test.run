#!/bin/bash
#USE_GOLD_SIM_FILES=1

#
#   This is the script that writes a new 'test.run' command
#    file for this testsuite
#
#   This is run by the test harness *every* time this testsuite is
#   run.  It is invoked like this:
#		./write-test.run  >  test.run
#
# in this directory we check for CARBON_INTERNAL_ERROR in any log file, none should exist
DO_CHECK_FOR_CARBON_INTERNAL_ERROR=1

		###
		###   Define the testlists
		###

# XPASS_*  tests are passing
# XPASS_QUICK is run when -quick specified, XPASS_QUICK + XPASS_SLOW is run if -quick is not specified,  move tests to XPASS_SLOW when you want to run them less often
XPASS_QUICK=" \
"

XPASS_SLOW=" \
unsup_const/access_type/access_type \
unsup_const/attrib_spec4/attrib_spec4 \
unsup_const/attrib_spec5/attrib_spec5 \
unsup_const/block1/block1 \
unsup_const/clock3/clock3 \
unsup_const/float_type/float_type \
unsup_const/func5/func5 \
unsup_const/func6/func6 \
unsup_const/incomp_type/incomp_type \
unsup_const/label1/label1 \
unsup_const/label2/label2 \
unsup_const/label4/label4 \
unsup_const/label5/label5 \
unsup_const/label6/label6 \
unsup_const/label7/label7 \
unsup_const/label8/label8 \
unsup_const/misc19/misc19 \
unsup_const/mult_dim_array/mult_dim_array \
unsup_const/pre_attrb1/pre_attrb1 \
unsup_const/textio/textio \
unsup_const/wait_on1/wait_on1 \
unsup_const/while14/while14 \
unsup_const/while15/while15 \
unsup_const/while_loop/while_loop \
"

# these tests use some feature that keeps them from running with testdriver
XPASSNOSIM_QUICK=" \
"
XPASSNOSIM_SLOW=" \
"


# The XTEXIT tests are currently broken,
XTEXIT_QUICK=" \
"

XTEXIT_SLOW=" \
unsup_const/time_type/time_type \
"
# This group is for tests that are not supported
XPEXIT_QUICK=" \
"

XPEXIT_SLOW=" \
unsup_const/attrib1/attrib1 \
unsup_const/attrib2/attrib2 \
unsup_const/attrib3/attrib3 \
unsup_const/clock1/clock1 \
unsup_const/clock2/clock2 \
unsup_const/conc_block18/conc_block18 \
unsup_const/deffered_const/deffered_const \
unsup_const/dis_spec/dis_spec \
unsup_const/event_process1/event_process1 \
unsup_const/flip_flop11/flip_flop11 \
unsup_const/flip_flop26/flip_flop26 \
unsup_const/flip_flop27/flip_flop27 \
unsup_const/flip_flop31/flip_flop31 \
unsup_const/flip_flop32/flip_flop32 \
unsup_const/flip_flop34/flip_flop34 \
unsup_const/func4/func4 \
unsup_const/gen_no_default/gen_no_default \
unsup_const/linkage1/linkage1 \
unsup_const/misc13/misc13 \
unsup_const/misc33/misc33 \
unsup_const/misc36/misc36 \
unsup_const/misc37/misc37 \
unsup_const/misc38/misc38 \
unsup_const/null_wave/null_wave \
unsup_const/pre_attrb3/pre_attrb3 \
unsup_const/pre_attrb4/pre_attrb4 \
unsup_const/sel_asgn14/sel_asgn14 \
unsup_const/unconst_generic1/unconst_generic1 \
"

# These tests currently have a simulation difference (between verific and interra)
XTSIMDIFF_QUICK=" \
"


XTSIMDIFF_SLOW=" \
"


# the XPASS_SIMONLY tests are run with verific parser and the simulation results are compared with a gold sim file.  
# This group is used for testcases that do not match simulation results from Interra flow because interra flow is incorrect
XPASS_SIMONLY_QUICK="\
"

XPASS_SIMONLY_SLOW="\
"


# XTSHOULD_FAIL_VERIFIC_* are tests that fail(non-zero exit) in interra flow and should fail in verific flow but currently do not report an error message (or return a zero status) in verific flow.
# unsup_const/file_type/file_type  does not yet report problem with unsup_const.file_type.vhdl:16: Error 30499:  Using 1076-1987 syntax for file declaration
XTSHOULD_FAIL_VERIFIC_SLOW="\
unsup_const/file_type/file_type \
"


# the XPSHOULD_FAIL_VERIFIC are tests that correct exit with non-zero status from verific flow, they require a gold file
# unsup_const/severity_level1/severity_level1 verific correctly reports that you cannot use 'severity_level' as a signal type name, interra flow seems to ignore this issue
XPSHOULD_FAIL_VERIFIC_QUICK=" \
"

XPSHOULD_FAIL_VERIFIC_SLOW=" \
unsup_const/severity_level1/severity_level1 \
"

#The tests in this group were incorrectly missing from this file, someone should run them to assign them to the correct test list
XTSKIP="\
unsup_const/allocators/allocators \
unsup_const/attributes8/attributes8 \
unsup_const/clock_lst_val/clock_lst_val \
unsup_const/func3/func3 \
unsup_const/label3/label3 \
unsup_const/label9/label9 \
unsup_const/misc35/misc35 \
unsup_const/misc6/misc6 \
unsup_const/now_func/now_func \
unsup_const/pre_attrb2/pre_attrb2 \
unsup_const/sig1/sig1 \
unsup_const/use_attb/use_attb \
"

HDLExt="vhdl"
source $CARBON_HOME/test/rushc/Common/write-test.common

			### end-of-file  ###

