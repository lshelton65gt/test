-- Test of dynamic elaboration involving uniquification of function
entity ufunc5 is
  port (IN1  : bit;
        OUT1 : out bit_vector(7 downto 0));
end;
architecture ufunc5 of ufunc5 is
  signal w : integer := 7;
  function getbits(n : integer; inbit : bit) return bit_vector is
     variable v : bit_vector(n downto 0);
  begin
     if (n = 7) then
       for i in v'range loop
         v(i) := inbit;
       end loop;
     else
       v := (others => '1');
     end if;
     return v;
  end;
begin
  process(IN1, w)
    variable v : integer;
  begin
    if (w = 7) then
      v := w;
    else
      v := w+1;
    end if;
    OUT1 <= getbits(v, IN1) and getbits(v-1, IN1);
  end process;
end;
