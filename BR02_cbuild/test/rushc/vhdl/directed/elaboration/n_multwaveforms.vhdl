-- Negative test of multiple waveforms in conditional signal assignment statement.
entity n_multwaveforms is
  port(IN1, IN2 : bit;
       OUT1 : out bit);
end;
architecture n_multwaveforms of n_multwaveforms is
begin
  OUT1 <= IN1 after 1 ns, IN2 after 2 ns;
end;
