-- Test of dynamic elaboration involving uniquification of function
entity ufunc2 is
  port (IN1  : bit;
        OUT1 : out bit_vector(7 downto 0));
end;
architecture ufunc2 of ufunc2 is
  constant w : integer := 7;
  function foo(n : integer; inbit : bit) return bit_vector is
     variable v : bit_vector(n downto 0);
  begin
     if (n = 7) then
       for i in v'range loop
         v(i) := inbit;
       end loop;
     else
       v := (others => '1');
     end if;
     return v;
  end;
begin
  process(IN1)
    variable v : integer;
  begin
    v := w;
    OUT1 <= foo(v, IN1) and foo(v-1, IN1);
  end process;
end;
