-- Test dynamic elaboration involving uniquification of a function
entity ufunc is
  port (IN1  : bit;
        OUT1 : out bit;
        OUT2 : out bit);
end;
architecture ufunc of ufunc is
  function getbit(n : integer; inbit : bit) return bit is
     variable v : bit_vector(n downto 0);
  begin
     if (n = 7) then
       v(n) := inbit;
       v(0) := not inbit;
     else
       v(n) := inbit;
       v(0) := inbit;
     end if;
     return (v(n) nand v(0));
  end;
begin
  process(IN1)
    variable v : integer;
  begin
    v := 7;
    OUT1 <= getbit(v, IN1);
    OUT2 <= getbit(v-1, IN1);
  end process;
end;
