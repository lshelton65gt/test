-- Test dynamic elaboration involving uniquification of a function;
-- based on ufunc
entity ufunc3 is
  port (IN1  : bit;
        OUT1 : out bit;
        OUT2 : out bit);
end;
architecture ufunc3 of ufunc3 is
  constant w : integer := 7;
  function getbit(n : integer; inbit : bit) return bit is
     variable v : bit_vector(n downto 0);
  begin
     if (n = 7) then
       v(n) := inbit;
       v(0) := not inbit;
     else
       v(n) := inbit;
       v(0) := inbit;
     end if;
     return (v(n) nand v(0));
  end;
begin
  process(IN1)
    variable v1 : integer;
    variable v2 : integer;
  begin
    if (v1 = w) then
      v2 := 7;
    else
      v2 := w;
    end if;
    OUT1 <= getbit(v2, IN1);
    OUT2 <= getbit(v2-1, IN1);
  end process;
end;
