-- Reproduce customer 2 issue 23
-- Output port connects to an 'actual' that is a slice of an
-- array causes an error in arraySliceWithConstIndexes

library ieee;
use ieee.std_logic_1164.all;
-- use ieee.numeric_std.all;
package types is
 type val_array is array (integer range <>) of std_logic_vector(31 downto 0);
end;

library ieee;
use ieee.std_logic_1164.all;
use work.types.all;

entity child is
port(
      in1 : in std_logic_vector(31 downto 0);
      in2 : in std_logic_vector(31 downto 0);
      out1 : out val_array(1 downto 0)
    );
end child;

architecture rtl of child is
begin
    out1(0) <= in1;    
    out1(1) <= in2;    
end architecture;

library ieee;
use ieee.std_logic_1164.all;
use work.types.all;

entity issue23 is
port( 
      clk : in std_logic;
      sel : in std_logic;
      a : in std_logic_vector(31 downto 0);
      b : in std_logic_vector(31 downto 0);
      dout : out std_logic_vector(31 downto 0)
     );
end issue23;

architecture rtl of issue23 is

signal va: val_array(1 downto 0);

begin

   process (clk, sel) is
   begin
     if (clk'event and clk = '1') then
       if (sel = '0') then
          dout <= va(0);
       else
          dout <= va(1);
       end if;
     end if;
   end process;

   inst1: entity work.child(rtl) port map (a, b, va(1 downto 0));

end architecture;

