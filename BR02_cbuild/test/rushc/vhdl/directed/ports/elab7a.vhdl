-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- part select & bit select in port map

-- modified by carbon to demonstrate problem with port ordering (that 
-- the order of portconnections needs to be established by the forma arg), see also misc.elab7.vhdl

library ieee; use ieee.std_logic_1164.all;
 
entity leaf1 is
	port(in1 : in std_logic_vector(2 downto 0);
	out1 : out std_logic_vector(2 downto 0));
end leaf1;

architecture behav of leaf1 is
begin
	out1 <= in1;
end behav;

library ieee;
use ieee.std_logic_1164.ALL;

entity elab7a is
	port( data_in : in std_logic_vector(0 to 2);
		  data_out : out std_logic_vector(0 to 2));
end elab7a;
 
architecture elab7a of elab7a is
 
  component leaf1
      port (in1 : in std_logic_vector(2 downto 0 );
            out1 : out std_logic_vector(2 downto 0 ));
  end component;
 
begin

  c1: leaf1 port map (in1(0) => data_in(0),                 --note here that the formal (in1) is not listed in the declaration order
                                                            --and the actual (data_in) is listed so it matches the non-declaration order of in1
                      in1(2 downto 1) => data_in(1 to 2),

                      out1(0) => data_out(2),
                      out1(2 downto 1) => data_out(0 to 1));

end elab7a;
