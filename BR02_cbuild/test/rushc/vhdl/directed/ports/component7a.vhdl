-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

--testPlan Ver. 3.00
--3.8.7 : Component Declaration having array of array.

-- modified version of:
-- test/rushc/vhdl/beacon_types_decls/types_decls.component7.vhdl


-- modified by carbon so that the multi dimensional array on port of
-- component7a_1 is broken up to either slices of the array or slices of
-- an element of the array.  In most cases the slices/elements are not listed in
-- the declaration order of the formal,  This test checks to make 
-- sure that we create the proper portconnections.  Simulation output should be
-- identical to types_decls.component7.vhdl

library IEEE;
use IEEE.std_logic_1164.all;
package CONV_PACK_component7a is
 subtype arr1 is std_logic_vector(1 to 4);
 type arr2 is array(1 to 4) of arr1;
end CONV_PACK_component7a;

library IEEE;
use IEEE.std_logic_1164.all;
use work.CONV_PACK_component7a.all;

entity component7a_1 is
 port (	in1 : arr1;
		in2 : arr2;
		op1 : out arr2 );
end component7a_1;

 architecture component7a_1 of component7a_1 is
 begin
  process(in1,in2)
  begin
   for index in 1 to in2'length loop
    op1(index) <= in2(index) nor in1;
   end loop;
  end process;
 end ;

use work.CONV_PACK_component7a.all;
entity component7a is
 port ( in1,in2,in3 : arr2;
		in4 : arr1;
		op14,op24,op34 : out arr2);
end component7a;

 architecture component7a of component7a is

  component component7a_1
   port (in1 : arr1; in2 : arr2; op1 : out arr2);
  end component;

  for all : component7a_1 use entity work.component7a_1(component7a_1);

  begin
   C1 : component7a_1 port map (in1 => in4,
                               in2 => in1,
                               op1(2) => op14(2),
                               op1(1) => op14(1), --note that order of formal is not listed here in declaration order of formal
                               op1(4) => op14(4), --note that order of formal is not listed here in declaration order of formal
                               op1(3) => op14(3)
                               );
   C2 : component7a_1 port map (in1 => in4,
                               in2 => in2,
                               op1(3 to 4) => op24(3 to 4), --note that this slice is not listed in portmap in the declaration order of formal
                               op1(1 to 2) => op24(1 to 2)
                               );

   
   C3 : component7a_1 port map (in1 => in4,
                               in2 => in3,
                               op1(2 to 4) => op34(2 to 4),
                               op1(1)(2 to 4) => op14(1)(2 to 4),
                               op1(1)(1) => op14(1)(1)
                               );
  end component7a;
