-- contrived testcase to try all possible port direction connections (downto or
-- to) for both inputs and outputs.

library ieee; use ieee.std_logic_1164.all;
 
entity leaf_up is 
  -- vectors are 0 to 4
  port(in1 : in std_logic_vector(0 to 4);
       out1 : out std_logic_vector(0 to 4));
end leaf_up;

architecture behav of leaf_up is
begin
	out1 <= in1;
end behav;


library ieee; use ieee.std_logic_1164.all;
 
entity leaf_dn is 
  -- vectors are 4 downto 0
  port(in1 : in std_logic_vector(4 downto 0);
       out1 : out std_logic_vector(4 downto 0));
end leaf_dn;

architecture behav of leaf_dn is
begin
	out1 <= in1;
end behav;



library ieee;
use ieee.std_logic_1164.ALL;

entity port_order_01 is
	port( data_in_up : in std_logic_vector(0 to 4);
              data_in_dn : in std_logic_vector(4 downto 0);
              data_out_up_01 : out std_logic_vector(0 to 4);
              data_out_dn_01 : out std_logic_vector(4 downto 0);
              data_out_up_02 : out std_logic_vector(0 to 4);
              data_out_dn_02 : out std_logic_vector(4 downto 0)
              );
end port_order_01;
 
architecture port_order_01 of port_order_01 is
 
  component leaf_up
      port (in1 : in std_logic_vector(0 to 4 );
            out1 : out std_logic_vector(0 to 4 ));
  end component;

  component leaf_dn
      port (in1 : in std_logic_vector(4 downto 0 );
            out1 : out std_logic_vector(4 downto 0 ));
  end component;
  
begin

  c1 : leaf_dn port map (in1(4)          => data_in_dn(4),
                         in1(1 downto 0) => data_in_dn(1 downto 0),
                         in1(3 downto 2) => data_in_dn(3 downto 2),

                         out1(4)          => data_out_dn_01(4),
                         out1(3 downto 2) => data_out_dn_01(3 downto 2),
                         out1(1 downto 0) => data_out_dn_01(1 downto 0)
                         );


  c2 : leaf_up port map (in1(0)      => data_in_dn(4),
                         in1(1 to 2) => data_in_dn(3 downto 2),
                         in1(3 to 4) => data_in_dn(1 downto 0),

                         out1(0)      => data_out_dn_02(4),
                         out1(1 to 2) => data_out_dn_02(3 downto 2),
                         out1(3 to 4) => data_out_dn_02(1 downto 0)
                         );


  c3 : leaf_up port map (in1(0)      => data_in_up(0),
                         in1(1 to 2) => data_in_up(1 to 2),
                         in1(3 to 4) => data_in_up(3 to 4),

                         out1(0)      => data_out_up_01(0),
                         out1(1 to 2) => data_out_up_01(1 to 2),
                         out1(3 to 4) => data_out_up_01(3 to 4)
                         );

  c4 : leaf_dn port map (in1(4)          => data_in_up(0),
                         in1(3 downto 2) => data_in_up(1 to 2),
                         in1(1 downto 0) => data_in_up(3 to 4),

                         out1(4)          => data_out_up_02(0),
                         out1(3 downto 2) => data_out_up_02(1 to 2),
                         out1(1 downto 0) => data_out_up_02(3 to 4)
                         );

  

end port_order_01;
