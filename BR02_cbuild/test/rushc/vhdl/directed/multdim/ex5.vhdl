library ieee;
use ieee.std_logic_1164.all;
package p_ex5 is
  type T2 is array ( 0 to 4 ) of std_logic_vector(7 downto 0);
  type T5 is array ( 0 to 3, 0 to 2 ) of T2;
end package;
use work.p_ex5.all;
entity ex5 is
   port (in1 : T5;
         out1 : out T5);
end entity;
architecture ex5 of ex5 is
begin
  out1 <= in1;
end architecture;
