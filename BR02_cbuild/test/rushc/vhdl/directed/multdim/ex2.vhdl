library ieee;
use ieee.std_logic_1164.all;
package p_ex2 is
  type T2 is array ( 0 to 4 ) of std_logic_vector(7 downto 0);
end package;
use work.p_ex2.all;
entity ex2 is
   port (in1 : T2;
         out1 : out T2);
end entity;
architecture ex2 of ex2 is
begin
  process(in1)
    variable v : T2;
  begin
    for i in v'range loop
       v(i) := in1(i);
    end loop;
    for j in out1'range loop
      for k in 7 downto 0 loop
        out1(j)(k) <= v(j)(k);
      end loop;
    end loop;
  end process;
end architecture;
