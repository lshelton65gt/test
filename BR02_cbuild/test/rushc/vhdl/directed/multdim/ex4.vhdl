library ieee;
use ieee.std_logic_1164.all;
package p_ex4 is
 type T1 is array(0 to 3) of std_logic;
 type T4 is array(0 to 3) of T1;
end package;

use work.p_ex4.all;
entity ex4 is
  port(in1: T4;
       out1  : out T4
      );
end ex4;


architecture ex4 of ex4 is
begin
  process(in1)
     variable v : T4;
  begin
     for i in T4'range loop
        v(i) := in1(i);
     end loop;
     out1 <= v;
  end process;
end;
   
