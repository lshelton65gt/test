package p_ex7 is
		type time_unit_enum is (u_fs,  u_ps,  u_ns,  u_us,  u_ms,  u_sec, u_min, u_hr);
		type time_unit_name_array is array (time_unit_enum) of string(1 to 3);
		constant time_unit_names: time_unit_name_array
			:= ("fs ", "ps ", "ns ", "us ", "ms ", "sec", "min", "hr ");
end p_ex7;

use WORK.p_ex7.all;

entity ex7 is
	port( 
		in1: in time_unit_enum;
		out1 : out string(1 to 3)
	    );
end ex7;

architecture ex7 of ex7 is
begin
	out1 <= time_unit_names(in1);
end ex7;
