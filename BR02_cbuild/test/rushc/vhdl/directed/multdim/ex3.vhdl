library ieee;
use ieee.std_logic_1164.all;
package p_ex3 is
  type T3 is array ( 0 to 3, 0 to 2 ) of std_ulogic;
end package;
use work.p_ex3.all;
entity ex3 is
   port (in1 : T3;
         out1 : out T3);
end entity;
architecture ex3 of ex3 is
begin
  out1 <= in1;
end architecture;
