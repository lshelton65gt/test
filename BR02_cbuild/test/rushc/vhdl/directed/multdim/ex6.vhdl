library ieee;
use ieee.std_logic_1164.all;
package p_ex6 is
  type T6 is array (positive range <>, natural range <>) of integer;
end package;
use work.p_ex6.all;
entity ex6 is
   port (in1 : T6(1 to 2,1 to 4);
         out1 : out T6(1 to 2, 0 to 3));
end entity;
architecture ex6 of ex6 is
begin
  out1 <= in1;
end architecture;
