package ex9_pack is
   subtype rsub is bit_vector(3 downto 0);
   type relem is array (1 downto 0) of rsub;
   type rec is record
		f : relem;
   end record;
   type vec2 is array (1 downto 0) of rec;
   type vec3 is array (1 downto 0) of vec2;
end ex9_pack;

use WORK.ex9_pack.all;

entity ex9 is
	port( 
R : rec;
		I : vec3;
		J, K, M, N : integer range 0 to 1;
		out1 : out bit; out2 : out rec
	    );
end ex9;

architecture arch_ex9 of ex9 is
begin
   process (R,I, J, K, M, N)
   function fn(in1:bit_vector; idx:integer range 0 to 1) return BIT is
			begin
			return in1(idx);
			end;
   begin
	   out1 <= fn(I(J)(K).f(M)(1 downto 0),N) ;
out2 <= R;
   end process;
end arch_ex9;
