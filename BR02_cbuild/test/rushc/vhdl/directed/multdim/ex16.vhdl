package p_ex16 is
  subtype vec128 is bit_vector(127 downto 0);
  type big is array(127 downto 0) of vec128;
end package;

use work.p_ex16.all;
entity ex16 is
  port(in1,in2: big;
       out1  : out big
      );
end ex16;


architecture ex16 of ex16 is
begin
  process(in1,in2)
   variable var: big;
  begin
   for i in big'range loop
     for j in 127 downto 0  loop
         var(i)(j):=in1(i)(j) xor in2(i)(j); 
     end loop;
   end loop;
   for i in big'range loop
     for j in 127 downto 0  loop
         out1(i)(j) <= var(j)(i);
     end loop;
   end loop;
  end process;
end;
   
