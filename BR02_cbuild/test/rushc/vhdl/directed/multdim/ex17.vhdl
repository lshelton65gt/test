package p_ex17 is
 type elem is array(3 downto 0) of bit;
 type big is array(2 downto 0) of elem;
end package;

use work.p_ex17.all;
entity ex17 is
  port(in1: big;
       out1  : out big
      );
end ex17;


architecture ex17 of ex17 is
begin
  process(in1)
  begin
      out1 <= in1;
  end process;
end;
   
