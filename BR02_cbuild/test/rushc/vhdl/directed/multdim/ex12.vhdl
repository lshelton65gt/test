package p_ex5 is
 type L is array(0 to 4, 0 to 5) of bit;
 type T is array(0 to 1, 0 to 2, 0 to 3) of L;
end package;

use work.p_ex5.all;
entity ex5 is
  port(in1: T;
       out1  : out T
      );
end ex5;


architecture ex5 of ex5 is
begin
  process(in1)
  begin
     --for i in 0 to 7 loop
       --for j in 0 to 1 loop
         --for k in 0 to 2 loop
           --out1(i,j,k) <= in1(i,j,k);
         --end loop;
       --end loop;
      --end loop;
  out1 <= in1;
  end process;
end;
   
