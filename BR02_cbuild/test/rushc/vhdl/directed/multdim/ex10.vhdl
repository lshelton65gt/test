package p_ex3 is
 type S is array(0 to 4) of bit_vector(7 downto 0);
 type T is array(0 to 3, 0 to 2) of S;
end package;

use work.p_ex3.all;
entity ex3 is
  port(in1: T;
       out1  : out T
      );
end ex3;


architecture ex3 of ex3 is
begin
  process(in1)
  begin
   out1 <= in1;
  end process;
end;
   
