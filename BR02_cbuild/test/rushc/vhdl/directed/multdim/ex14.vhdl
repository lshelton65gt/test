package p_ex14 is
  --type big is array(3 downto 0, 3 downto 0) of bit;
  type big is array(3 downto 0) of bit;
end package;

use work.p_ex14.all;
entity ex14 is
  port(in1,in2: big;
       out1  : out big
      );
end ex14;


architecture ex14 of ex14 is
begin
  process(in1,in2)
   variable var: big;
  begin
   for i in big'range loop
     for j in 3 downto 0  loop
	 --var(i,j):=in1(i,j) xor in2(i,j); 
         var(i):=in1(i) xor in2(j); 
     end loop;
   end loop;
   for i in big'range loop
     for j in 3 downto 0  loop
         --out1(i,j) <= var(j,i);
         out1(j) <= var(i);
     end loop;
   end loop;
  end process;
end;
   
