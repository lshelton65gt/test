package p_ex4 is
 type T is array(0 to 3, 0 to 2, 0 to 1) of bit_vector(7 downto 0);
end package;

use work.p_ex4.all;
entity ex4 is
  port(in1: T;
       out1  : out T
      );
end ex4;


architecture ex4 of ex4 is
begin
  process(in1)
  begin
     --for i in 0 to 7 loop
       --for j in 0 to 1 loop
         --for k in 0 to 2 loop
           --out1(i,j,k) <= in1(i,j,k);
         --end loop;
       --end loop;
      --end loop;
  out1 <= in1;
  end process;
end;
   
