-- Note this is a basic test. The type is not multi-dimensional.
-- We test handling of the 9-value logic type std_logic; we should not generate a multi-bit net
-- it is a bit net.
library ieee;
use ieee.std_logic_1164.all;
package p_ex1 is
 type T is array(0 to 3) of std_logic;
end package;

use work.p_ex1.all;
entity ex1 is
  port(in1: T;
       out1  : out T
      );
end ex1;


architecture ex1 of ex1 is
begin
  process(in1)
     variable v : T;
  begin
      v := in1;
      for i in T'range loop
        out1(i) <= v(i);
      end loop;
  end process;
end;
   
