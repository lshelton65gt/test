library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

--package containg User defined unconstrained array of element type small_int
package conv_pack_ex8 is
 type myarray is array(integer Range <>) of small_int;
end conv_pack_ex8 ;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use work.conv_pack_ex8.myarray;
entity ex8 is
 generic ( N : integer := 5);
 port ( in1 : in myarray(1 to N);
		in2 : in small_int;
		op1 : out myarray(1 to N));
end ex8;

 architecture ex8 of ex8 is

  function f1(a : myarray(1 to N);b:small_int) return myarray is
   variable var1 : myarray(1 to N);
  begin
   for i in 1 to N loop
	var1(i) := a(i) * b;
   end loop;
   return var1;
  end f1;

 begin -- start of the architecture body
   op1 <= f1(in1,in2);
 end ex8;
