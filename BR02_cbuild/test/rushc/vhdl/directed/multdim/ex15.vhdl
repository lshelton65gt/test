-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

-- Multidimensional array
--15.11 of test plan version 3.0

entity ex15 is
  port(in1,in2: integer;
       output : out integer
      );
end ex15;


architecture ex15 of ex15 is
  type arr is array(0 to 1,0 to 2,0 to 3) of integer;
  --type arr is array(0 to 3) of integer;
begin
  process(in1,in2)
   variable var: arr;
   variable var1: integer;
  begin
   for i in 0 to 1 loop
     for j in 0 to 3 loop
         var(i,0,j):=in1+i+j;
         --var(i):=in1+i+j;
     end loop;
   end loop;
    var1:=0;
    for i in 0 to 1 loop   
     for j in 0 to 3 loop
       var1:= var1+var(i,0,j);
       --var1:= var1+var(j);
      end loop;
   end loop;
   output<= var1;
  end process;
end;
   
