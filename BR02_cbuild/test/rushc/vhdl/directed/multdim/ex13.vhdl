package p_ex6 is
 type L is array(0 to 6, 0 to 1) of bit_vector(9 downto 1);
 type T is array(0 to 3, 0 to 2, 0 to 4) of L;
end package;

use work.p_ex6.all;
entity ex6 is
  port(in1: T;
       out1  : out T
      );
end ex6;


architecture ex6 of ex6 is
begin
  process(in1)
  begin
     --for i in 0 to 7 loop
       --for j in 0 to 1 loop
         --for k in 0 to 2 loop
           --out1(i,j,k) <= in1(i,j,k);
         --end loop;
       --end loop;
      --end loop;
  out1 <= in1;
  end process;
end;
   
