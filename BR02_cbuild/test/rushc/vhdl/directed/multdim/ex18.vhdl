package p_ex18 is
  type big is array(3 downto 0, 3 downto 0) of bit;
end package;

use work.p_ex18.all;
entity ex18 is
  port(in1,in2: big;
       out1  : out big
      );
end ex18;


architecture ex18 of ex18 is
begin
  process(in1,in2)
   variable var: big;
  begin
   for i in big'range loop
     for j in big'range  loop
	 var(i,j):=in1(i,j) xor in2(i,j); 
     end loop;
   end loop;
   for i in big'range loop
     for j in big'range  loop
         out1(i,j) <= var(j,i);
     end loop;
   end loop;
  end process;
end;
   
