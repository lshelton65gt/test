-- Negative test: process sensitivity list missing IN2 and S. The signals are passed to
-- a procedure.
entity n_proc is
  port (IN1, IN2 : bit;
        OUT1 : out bit);
end;
architecture n_proc of n_proc is
  signal S : bit;
  procedure p(INP1 : bit; INP2 : bit; signal S : out bit) is
  begin
     if (INP1 = '1') then
        S <= IN2;
     else
        S <= '0';
     end if;
  end;
begin
  process(IN1)
  begin
    p(IN1, IN2, S);
    OUT1 <= S;
  end process;
end;
