-- Negative test: process sensitity list missing IN2. The signal is passed to a function.
entity n_func is
  port (IN1, IN2 : bit;
        OUT1 : out bit);
end;
architecture n_func of n_func is
  signal S : bit;
  function f(INP1 : bit; INP2 : bit) return bit is
    variable S : bit;
  begin
     if (INP1 = '1') then
        S := INP2;
     else
        S := '0';
     end if;
     return S;
  end;
begin
  process(S, IN1)
  begin
    S <= f(IN1, IN2);
    OUT1 <= S;
  end process;
end;
