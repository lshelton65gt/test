-- Basic test of clocked process (using wait clk'event construct) and sensitivity list:
-- checks (signals read under clock edge condition need not be in sensitivity list
entity waitclk is
  port (RST, CLK, IN1, IN2 : bit;
        OUT1 : out boolean);
end;
architecture waitclk of waitclk is
  signal S1 : bit;
  signal S2 : bit;
begin
  S1 <= IN1 or IN2;
  process
  begin
    wait until CLK'event and CLK='1' ;
    S2 <= IN2 or IN1;
  end process;
  process(S1, S2)
  begin
    OUT1 <= (S1 = S2);
  end process;
end;
