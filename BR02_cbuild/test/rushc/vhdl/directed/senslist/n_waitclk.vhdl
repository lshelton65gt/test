-- Negative test of process having wait statement and sensitivity list
entity n_waitclk is
  port (RST, CLK, IN1, IN2 : bit;
        OUT1 : out boolean);
end;
architecture n_waitclk of n_waitclk is
  signal S1 : bit;
  signal S2 : bit;
begin
  S1 <= IN1 or IN2;
  process(CLK)
  begin
    wait until CLK'event and CLK='1' ;
    S2 <= IN2 or IN1;
  end process;
  process(S1, S2)
  begin
    OUT1 <= (S1 = S2);
  end process;
end;
