-- Negative test of process having wait statement and sensitivity list. Based on n_waitclk but
-- with a different sensitivity list 
entity n_waitclk2 is
  port (RST, CLK, IN1, IN2 : bit;
        OUT1 : out boolean);
end;
architecture n_waitclk2 of n_waitclk2 is
  signal S1 : bit;
  signal S2 : bit;
begin
  S1 <= IN1 or IN2;
  process(IN1, IN2)
  begin
    wait until CLK'event and CLK='1' ;
    S2 <= IN2 or IN1;
  end process;
  process(S1, S2)
  begin
    OUT1 <= (S1 = S2);
  end process;
end;
