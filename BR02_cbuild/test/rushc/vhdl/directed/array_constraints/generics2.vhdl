-- Test instantation of an entity with ports sized by a generic.
-- Its ports will be constrained by the component instantiation.
entity module is
    generic (SIZE : natural := 8);
    port (IN1 : bit_vector(SIZE-1 downto 0);
         OUT1 : out bit_vector(SIZE-1 downto 0));
end;
architecture module of module is
begin
  OUT1 <= IN1;
end;
use work.module;
entity generics2 is
  port (IN1 : bit_vector(11 downto 0);
        OUT1 : out bit_vector(11 downto 0));
end;
architecture generics2 of generics2 is
  component module 
    generic (SIZE : natural := 8);
    port (IN1 : bit_vector(SIZE-1 downto 0);
        OUT1 : out bit_vector(SIZE-1 downto 0));
  end component;
  for U1: module use entity work.module;
  signal S_IN : bit_vector(11 downto 0);
  signal S_OUT : bit_vector(11 downto 0);
begin 
  S_IN <= IN1;
  OUT1 <= S_OUT;
  U1:module generic map(12) port map(S_IN,S_OUT);
end;
