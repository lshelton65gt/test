-- Negative test for array signal with a null range
entity null_range is
  port (IN1 : bit_vector(11 downto 0);
        OUT1 : out bit_vector(11 downto 0));
end;
architecture null_range of null_range is
  signal S : bit_vector(0 downto 11);
begin 
  S <= IN1;
  OUT1 <= IN1 nor S;
end;
