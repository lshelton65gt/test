-- Test instantation of an entity with unconstrained ports.
-- Its ports will be constrained by the component instantiation using
-- signals.
entity module is
  port (IN1 : bit_vector;
        OUT1 : out bit_vector);
end;
architecture module of module is
begin
  OUT1 <= IN1;
end;
use work.module;
entity uncons_ports2 is
  port (IN1 : bit_vector(11 downto 0);
        OUT1 : out bit_vector(11 downto 0));
end;
architecture uncons_ports2 of uncons_ports2 is
  component module 
    port (IN1 : bit_vector;
          OUT1 : out bit_vector);
  end component;
  for U1: module use entity work.module;
  signal S_IN : bit_vector(11 downto 0);
  signal S_OUT : bit_vector(11 downto 0);
begin 
  S_IN <= IN1;
  OUT1 <= S_OUT;
  U1:module port map(S_IN, S_OUT);
end;
