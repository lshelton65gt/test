-- Test multiple instantations of an entity with unconstrained ports.
-- The ports of the different instances are constrained by their component
-- instantiations with different sizes.
entity module is
  port (IN1 : bit_vector;
        OUT1 : out bit_vector);
end;
architecture module of module is
begin
  OUT1 <= IN1;
end;
use work.module;
entity uncons_ports3 is
  port (IN1 : bit_vector(11 downto 0);
        IN2 : bit_vector(13 downto 0);
        OUT1 : out bit_vector(11 downto 0);
        OUT2 : out bit_vector(13 downto 0));
end;
architecture uncons_ports3 of uncons_ports3 is
  component module 
    port (IN1 : bit_vector;
        OUT1 : out bit_vector);
  end component;
  for all: module use entity work.module;
begin 
  U1:module port map(IN1,OUT1);
  U2:module port map(IN2,OUT2);
end;
