-- Test instantation of an entity with unconstrained ports.
-- Its ports will be constrained by the component instantiation.
entity module is
  port (IN1 : bit_vector;
        OUT1 : out bit_vector);
end;
architecture module of module is
begin
  OUT1 <= IN1;
end;
use work.module;
entity uncons_ports is
  port (IN1 : bit_vector(11 downto 0);
        OUT1 : out bit_vector(11 downto 0));
end;
architecture uncons_ports of uncons_ports is
  component module 
    port (IN1 : bit_vector;
        OUT1 : out bit_vector);
  end component;
  for U1: module use entity work.module;
begin 
  U1:module port map(IN1,OUT1);
end;
