-- Negative test for a signal's range constraint that depends on a port
entity dynamic_elab is
  port (SIZE : natural range 0 to 12;
        IN1 : bit_vector(11 downto 0);
        OUT1 : out bit_vector(11 downto 0));
end;
architecture dynamic_elab of dynamic_elab is
  signal S : bit_vector(SIZE-1 downto 0);
begin 
  OUT1 <= IN1 and S;
end;
