package package_n1 is
 type enum is (alpha,bravo,charlie,delta,echo,foxtrot,hunter,india,juliet);
end package_n1;

use work.package_n1.all;
entity n1 is
 port ( in1 : enum;
		in2 : enum;
		op1 : out enum;
		op2 : out enum);
end n1;

 architecture n1 of n1 is
 begin
  process(in1,in2)
  begin
   if ((in1 = alpha) and (in2 = juliet)) then
     op1 <= delta; op2 <= echo;
   else 
     if ((in1 = delta) and (in2 = echo)) then
       op1 <= foxtrot; op2 <= bravo;
     else 
       if ((in1 = charlie) and (in2 = hunter)) then
         op2 <= india; op1 <= india;
       else
         op1 <= juliet; op2 <= hunter;
       end if;
     end if;
   end if;
  end process;
 end ;

