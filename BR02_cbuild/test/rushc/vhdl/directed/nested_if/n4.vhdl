package package_n4 is
 type enum is (alpha,bravo,charlie,delta,echo,foxtrot,hunter,india,juliet);
end package_n4;

use work.package_n4.all;
entity n4 is
 port ( in1 : enum;
		in2 : enum;
		op1 : out enum;
		op2 : out enum);
end n4;

 architecture n4 of n4 is
 begin
  process(in1,in2)
  begin
   if ((in1 = alpha) and (in2 = juliet)) then
     op1 <= delta; op2 <= echo;
   elsif ((in1 = delta) and (in2 = echo)) then
       op1 <= foxtrot; op2 <= bravo;
   elsif ((in1 = charlie) and (in2 = hunter)) then
       op2 <= india; op1 <= india;
   elsif (true) then
       op1 <= juliet; op2 <= hunter;
   end if;
  end process;
 end ;

