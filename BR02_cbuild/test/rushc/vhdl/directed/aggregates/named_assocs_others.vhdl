-- Basic test of an aggregate with named associations with an others
entity named_assocs_others is
  port(in1,in2, in3 : bit;
       out1 : out bit_vector(3 downto 0));
end;
architecture named_assocs_others of named_assocs_others is
begin
  out1 <= (0 => in1, 2 => in2, 1 => in3, others => '0'); 
end;
