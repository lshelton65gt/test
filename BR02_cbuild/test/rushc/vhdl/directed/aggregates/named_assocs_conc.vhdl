-- Basic test of aggregate expression combined with a concatenation
entity named_assocs_conc is
  port(in1,in2, in3 : bit;
       out1 : out bit_vector(3 downto 0));
end;
architecture named_assocs_conc of named_assocs_conc is
begin
  out1 <= (3 => in3, 1 => in1, 2 => '0') & "0";
end;
