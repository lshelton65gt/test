-- Negative test of an aggregate with mixed positional and named associations
entity n_mixed_assocs is
  port(in1,in2, in3 : bit;
       out1 : out bit_vector(3 downto 0));
end;
architecture n_mixed_assocs of n_mixed_assocs is
begin
  out1 <= (in1, in2, 3 => in3, 2 => '0'); 
end;
