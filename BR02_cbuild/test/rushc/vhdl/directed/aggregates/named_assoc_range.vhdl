-- Basic test of an aggregate with a named element associations including one with a with a range
entity named_assoc_range is
  port(in1,in2, in3 : bit;
       out1 : out bit_vector(3 downto 0));
end;
architecture named_assoc_range of named_assoc_range is
begin
  out1 <= (0 to 2 => in1, 3 => in3); 
end;
