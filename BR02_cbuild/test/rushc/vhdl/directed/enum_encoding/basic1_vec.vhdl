-- Basic test of enum_encoding attribute using only 1 vectors
package p_basic1_vec is
  type utype is (A, B, C, D);
  attribute ENUM_ENCODING : string;
  attribute ENUM_ENCODING of utype : type is "11 11 11 11";
end;
use work.p_basic1_vec.all;
entity basic1_vec is
  port(out1, out2, out3, out4 : out utype);
end;
architecture basic1_vec of basic1_vec is
begin
  out1 <= A;
  out2 <= B;
  out3 <= C;
  out4 <= D;
end;
