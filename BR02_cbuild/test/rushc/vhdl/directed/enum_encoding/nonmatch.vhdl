-- Test for enum_encoding attribute with non-matching number of encodings (6) and enum
-- literals (3) in the enumeration type. 
-- Verific ignores the attribute and uses a sequential encoding while Interra uses the
-- first 3 patterns.

package p_nonmatch is
  type utype is (A, B, C);
  attribute ENUM_ENCODING : string;
  attribute ENUM_ENCODING of utype : type is "00 00 01 10 11 ZZ";
end;
use work.p_nonmatch.all;
entity nonmatch is
  port(out1, out2, out3 : out utype);
end;
architecture nonmatch of nonmatch is
begin
  out1 <= A;
  out2 <= B;
  out3 <= C;
end;
