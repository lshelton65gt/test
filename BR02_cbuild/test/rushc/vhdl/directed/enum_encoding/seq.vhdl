-- Basic test of enum_encoding attribute using sequenital encoding
package p_seq is
  type utype is (A, B, C, D);
  attribute ENUM_ENCODING : string;
  attribute ENUM_ENCODING of utype : type is "00 01 10 11";
end;
use work.p_seq.all;
entity seq is
  port(out1, out2, out3, out4 : out utype);
end;
architecture seq of seq is
  constant v1 : utype := A;
  constant v2 : utype := B;
  constant v3 : utype := C;
  constant v4 : utype := D;
begin
  out1 <= v1;
  out2 <= v2;
  out3 <= v3;
  out4 <= v4;
end;
