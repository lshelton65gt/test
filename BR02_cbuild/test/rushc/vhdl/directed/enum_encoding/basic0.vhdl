-- Basic test of enum_encoding attribute using only 0 values
package p_basic0 is
  type utype is (A, B, C, D);
  attribute ENUM_ENCODING : string;
  attribute ENUM_ENCODING of utype : type is "0 0 0 0";
end;
use work.p_basic0.all;
entity basic0 is
  port(out1, out2, out3, out4 : out utype);
end;
architecture basic0 of basic0 is
begin
  out1 <= A;
  out2 <= B;
  out3 <= C;
  out4 <= D;
end;
