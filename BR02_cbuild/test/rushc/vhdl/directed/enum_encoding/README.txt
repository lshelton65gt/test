ENUM_ENCODING is an attribute of type string specified on a user-defined enumeration type that defines the bit patterns (i.e. the encoding) for each literal in the enumeration.

They bit patterns must use only the following character literals from the STD_LOGIC type in package std_logic_1164:  U', '0', '1', 'Z', '-'.  'D' may be used instead of '-' (for compatibility with synthesis tools).
