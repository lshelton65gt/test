-- Test of enum_encoding attribute whre the attribute declaration and specification
-- come after object declarations (This is an error in certain EDA tools)
package p_pre is
  type utype is (A, B, C, D);
  constant v1 : utype := A;
  constant v2 : utype := B;
  constant v3 : utype := C;
  constant v4 : utype := D;
  attribute ENUM_ENCODING : string;
  attribute ENUM_ENCODING of utype : type is "00 10 11 01";
end;
use work.p_pre.all;
entity pre is
  port(out1, out2, out3, out4 : out utype);
end;
architecture pre of pre is
begin
  out1 <= v1;
  out2 <= v2;
  out3 <= v3;
  out4 <= v4;
end;
