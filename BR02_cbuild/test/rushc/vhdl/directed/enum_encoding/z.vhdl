-- Test enum_encoding attribute using Z values
package p_z is
  type utype is (A, B, C);
  attribute ENUM_ENCODING : string;
  attribute ENUM_ENCODING of utype : type is "0 1 Z";
end;
use work.p_z.all;
entity z is
  port(sel : natural range 0 to 3;
       in1 : utype;
       out1 : out utype);
end;
architecture z of z is
begin
  out1 <= A when sel=0 else B when sel=1 else C when sel=2 else in1;
end;
