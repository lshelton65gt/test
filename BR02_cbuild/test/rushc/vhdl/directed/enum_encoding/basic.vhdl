-- Basic test for enum_encoding attribute 

package p_basic is
  type utype is (A, B, C, D, E, F);
  attribute ENUM_ENCODING : string;
  attribute ENUM_ENCODING of utype : type is "00 00 01 10 11 ZZ";
end;
use work.p_basic.all;
entity basic is
  port(out1, out2, out3, out4, out5, out6 : out utype);
end;
architecture basic of basic is
begin
  out1 <= A;
  out2 <= B;
  out3 <= C;
  out4 <= D;
  out5 <= E;
  out6 <= F;
end;
