-- Basic test of enum_encoding attribute using only 1 values
package p_basic1 is
  type utype is (A, B, C, D);
  attribute ENUM_ENCODING : string;
  attribute ENUM_ENCODING of utype : type is "1 1 1 1";
end;
use work.p_basic1.all;
entity basic1 is
  port(out1, out2, out3, out4 : out utype);
end;
architecture basic1 of basic1 is
begin
  out1 <= A;
  out2 <= B;
  out3 <= C;
  out4 <= D;
end;
