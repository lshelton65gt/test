-- Basic test of enum_encoding attribute using only 0 vectors
package p_basic0_vec is
  type utype is (A, B, C, D);
  attribute ENUM_ENCODING : string;
  attribute ENUM_ENCODING of utype : type is "00 00 00 00";
end;
use work.p_basic0_vec.all;
entity basic0_vec is
  port(out1, out2, out3, out4 : out utype);
end;
architecture basic0_vec of basic0_vec is
begin
  out1 <= A;
  out2 <= B;
  out3 <= C;
  out4 <= D;
end;
