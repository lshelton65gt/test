-- Negative test of & (concatenation) operator on arrays with non-matching sizes in assignment
entity n_concrec is
port(
     IN1, IN2 : bit_vector(0 to 3);
     OUT1 : out bit_vector(0 to 9));
end;
architecture n_concrec of n_concrec is
begin
  OUT1 <= IN1 & IN2; -- non matching sizes LHS is 10, RHS is 8
end;
