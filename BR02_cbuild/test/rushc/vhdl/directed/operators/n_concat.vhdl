-- Negative test of & operator on arrays (non-matching sizes)
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity n_concat is
port(
     BV1, BV2 : bit_vector(3 downto 0);
     SLV1, SLV2 : std_logic_vector(3 downto 0);
     IN1,IN2 : signed(3 downto 0); 
     UIN1,UIN2 : unsigned(3 downto 0); 
     OUT_S : out signed(15 downto 0);
     OUT_U : out unsigned(15 downto 0);
     OUT_BV : out bit_vector(15 downto 0);
     OUT_SLV : out std_logic_vector(15 downto 0));
end;
architecture n_concat of n_concat is
begin
  OUT_S <= IN1 & IN2;  
  OUT_U <= UIN1 & UIN2; 
  OUT_BV <= BV1 & BV2 ;
  OUT_SLV <= SLV1 & SLV2 ;
end;
