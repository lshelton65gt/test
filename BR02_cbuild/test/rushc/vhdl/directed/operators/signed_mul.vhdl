-- Test numeric_std * operator on signed and unsigned arrays
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity signed_mul is
port(
     IN1,IN2 : signed(3 downto 0); 
     UIN1,UIN2 : unsigned(3 downto 0); 
     OUT1 : out signed(8 downto 0);
     OUT2 : out unsigned(8 downto 0);
     S1, S2 : out boolean);
end;
architecture signed_mul of signed_mul is
begin
  OUT1 <= IN1 * IN2; 
  OUT2 <= UIN1 * UIN2; 
  process(IN1,IN2,UIN1,UIN2)
    variable sout : signed(8 downto 0);
    variable uout : unsigned(8 downto 0);
    variable sh_out : signed(8 downto 0);
    variable sh_uout : unsigned(8 downto 0);
  begin
    sout := IN1 * 4; -- IN1 is left shifted 2 
    uout := UIN1 * 8; -- UIN1 is left shifted 3 
    sh_out := IN1 sll 2;
    sh_uout := UIN1 sll 3;
    S1 <= (sout /= sh_out);
    S2 <= (uout /= sh_uout);
  end process;
end;
