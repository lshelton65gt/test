-- Test & operator on arrays
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity concat is
port(
     BV1, BV2 : bit_vector(3 downto 0);
     SLV1, SLV2 : std_logic_vector(3 downto 0);
     IN1,IN2 : signed(3 downto 0); 
     UIN1,UIN2 : unsigned(3 downto 0); 
     OUT_S : out signed(15 downto 0);
     OUT_U : out unsigned(15 downto 0);
     OUT_BV : out bit_vector(15 downto 0);
     OUT_SLV : out std_logic_vector(15 downto 0));
end;
architecture concat of concat is
  constant PADDING_S : signed(7 downto 0) := "00001111";
  constant PADDING_U : unsigned(7 downto 0) := "00001111";
  constant PADDING_BV : bit_vector(7 downto 0) := "00001111";
  constant PADDING_SLV : std_logic_vector(7 downto 0) := "00001111";
   
begin
  OUT_S <= PADDING_S & IN1 & IN2; 
  OUT_U <= PADDING_U & UIN1 & UIN2; 
  OUT_BV <= BV1 & BV2 & PADDING_BV; 
  OUT_SLV <= SLV1 & SLV2 & PADDING_SLV; 
end;
