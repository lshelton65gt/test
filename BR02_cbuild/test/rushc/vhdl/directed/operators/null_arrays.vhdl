-- Test relational operators on null arrays
package p_null_arrays is
  type outarray is array(natural range <> ) of boolean;
end;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.p_null_arrays.all;
entity null_arrays is
port(NULL1 : signed(3 to 0); -- null array
     NULL2 : signed(2 to 0); -- null array
     IN1 : signed(3 downto 0); -- non null array
     OUT1 : out outarray(1 to 6);
     OUT2 : out outarray(1 to 6);
     OUT3 : out outarray(1 to 6);
     OUT4 : out outarray(1 to 6);
     AOUT1 : out signed(4 downto 0);
     AOUT2 : out signed(4 downto 0));
end;
architecture null_arrays of null_arrays is
begin
  -- Test with both operands being null arrays
  OUT1(1) <= NULL1 = NULL2;  -- always true
  OUT1(2) <= NULL1 /= NULL2;  -- always false
  OUT1(3) <= NULL1 < NULL2;  -- always false
  OUT1(4) <= NULL1 <= NULL2;   -- always true
  OUT1(5) <= NULL1 > NULL2;  -- always false
  OUT1(6) <= NULL1 >= NULL2;  -- always true

  -- Same as above but with operands switched (we don't expect this to make a difference)
  OUT2(1) <= NULL2 = NULL1; -- always true
  OUT2(2) <= NULL2 /= NULL1;  -- always false
  OUT2(3) <= NULL2 < NULL1;  -- always false
  OUT2(4) <= NULL2 <= NULL1; -- always true 
  OUT2(5) <= NULL2 > NULL1;-- always false 
  OUT2(6) <= NULL2 >= NULL1;-- always true 

 -- Left operand is a non-null array 
  OUT3(1) <= IN1 = NULL1; -- always false
  OUT3(2) <= IN1 /= NULL1; -- always true
  OUT3(3) <= IN1 < NULL1; -- always false
  OUT3(4) <= IN1 <= NULL1;   -- always false
  OUT3(5) <= IN1 > NULL1;  -- always true
  OUT3(6) <= IN1 >= NULL1; -- always true

  -- Right operand is a non-null array 
  OUT4(1) <= NULL1 = IN1; -- always false
  OUT4(2) <= NULL1 /= IN1; -- always true
  OUT4(3) <= NULL1 < IN1; -- always true
  OUT4(4) <= NULL1 <= IN1; -- always true
  OUT4(5) <= NULL1 > IN1; -- always false
  OUT4(6) <= NULL1 >= IN1; -- always false

  AOUT1 <= NULL1 + NULL2;
  AOUT2 <= NULL1 - NULL2;
end;
