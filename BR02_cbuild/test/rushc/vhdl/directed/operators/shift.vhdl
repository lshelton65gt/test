-- Basic tests on shift operators
package p_shift is
  type outarray is array(natural range <>) of boolean;
end;
library ieee;
use ieee.numeric_std.all;
use work.p_shift.all;
entity shift is
port(IN1, IN2 : bit_vector(7 downto 0);
     SIN1, SIN2 : signed(7 downto 0);
     OUT1 : out outarray(1 to 4));
end;
architecture shift of shift is
  constant z : bit_vector(7 downto 0) := "00000000";
  constant zmaskL : bit_vector(7 downto 0) := "00001111";
  constant zmaskH : bit_vector(7 downto 0) := "11110000";
  constant z_s : signed(7 downto 0) := "00000000";
  constant zmaskL_s : signed(7 downto 0) := "00001111";
  constant zmaskH_s : signed(7 downto 0) := "11110000";
  
begin
  OUT1(1) <= z = (IN1 sll 8);
  OUT1(2) <= (IN1 sll 2) = (((zmaskL and IN1) or (zmaskH and IN1)) sll 2);
  OUT1(3) <= z_s = (SIN1 sll 8);
  OUT1(4) <= (SIN1 srl 4) = (((zmaskL_s and SIN1) or (zmaskH_s and SIN1)) srl 4);
end;
