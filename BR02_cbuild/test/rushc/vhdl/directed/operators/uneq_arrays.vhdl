-- Test relational operator on unequal-size arrays
entity uneq_arrays is
port(IN1 : bit_vector(3 downto 0);
     IN2 : bit_vector(1 downto 0);
     OUT1, OUT2, OUT3, OUT4, OUT5, OUT6, OUT7, OUT8 : out boolean);
end;
architecture uneq_arrays of uneq_arrays is
begin
  OUT1 <= IN1 = IN2;
  OUT2 <= IN2 > IN1;
  OUT3 <= IN2 < IN1;
  OUT4 <= IN1 /= IN2;
  OUT5 <= IN1 /= IN1;
  OUT6 <= IN1 = IN1;
  OUT7 <= IN2 <= IN1;
  OUT8 <= IN2 >= IN1;
end;
