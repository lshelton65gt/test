-- Test numeric_std + and - operators on signed and unsigned arrays
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity signed_add is
port(
     IN1,IN2 : signed(3 downto 0); 
     UIN1,UIN2 : unsigned(3 downto 0); 
     OUT1 : out signed(2 downto 0);
     OUT2 : out signed(4 downto 0);
     OUT3 : out unsigned(4 downto 0);
     u1_msb, u2_msb, s1_msb, s2_msb : out std_logic);
end;
architecture signed_add of signed_add is
begin

  OUT1 <= IN1 + IN2; -- IN1 and IN2 should be truncated to 3 bits
  OUT2 <= IN1 + IN2; -- IN1 and IN2 should be sign-extended

  process(UIN1, UIN2, IN1, IN2)
    variable u1 : unsigned(4 downto 0);
    variable u2 : unsigned(4 downto 0);
    variable s1 : signed(4 downto 0) := "11111";
    variable s2 : signed(4 downto 0) := "11111";
  begin
    -- IN1 and IN2 should be zero-extended
    u1 := 0 + UIN1; 
    u2 := 0 + UIN2; 
    if (IN1 /= 0) then
       s1 := 0 - abs(IN1); 
    end if;
    if (IN2 /= 0) then
      s2 := 0 - abs(IN2); 
    end if;
    u1_msb <= u1(4); -- always 0 
    u2_msb <= u2(4); -- always 0
    s1_msb <= s1(4); -- always 1 
    s2_msb <= s2(4); -- always 1
    OUT3 <= u1 + u2;
  end process;
end;
