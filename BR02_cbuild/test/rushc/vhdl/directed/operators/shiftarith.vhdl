-- Basic tests on arithmetic shift operators
package p_shiftarith is
  type outarray is array(natural range <>) of boolean;
end;
use work.p_shiftarith.all;
entity shiftarith is
port(IN1, IN2 : bit_vector(7 downto 0);
     OUT1 : out outarray(1 to 3));
end;
architecture shiftarith of shiftarith is
  constant z : bit_vector(7 downto 0) := "00000000";
  constant zmaskL : bit_vector(7 downto 0) := "00001111";
  constant zmaskH : bit_vector(7 downto 0) := "11110000";
  
begin
  OUT1(1) <= z = (IN1 sla 8);
  OUT1(2) <= z = (IN1 sra 8);
  OUT1(3) <= (IN1 sla 2) = (((zmaskL and IN1) or (zmaskH and IN1)) sla 2);
end;
