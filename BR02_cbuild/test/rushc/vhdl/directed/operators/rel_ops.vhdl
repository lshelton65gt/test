-- Basic tests of relational operators
entity rel_ops is
port(IN1 : bit_vector(3 downto 0);
     IN2 : bit_vector(3 downto 0);
     OUT1,OUT2,OUT3,OUT4,OUT5,OUT6,OUT7,OUT8 : out boolean);
end;
architecture rel_ops of rel_ops is
begin
  OUT1 <= IN1 = IN2;
  OUT2 <= IN1 /= IN2;
  OUT3 <= IN1 > IN2;
  OUT4 <= IN1 < IN2;
  OUT5 <= IN1 < IN2;
  OUT6 <= IN1 >= IN2;
  OUT7 <= IN1 <= IN2 and IN2 /= "1111" and IN1 /= "0000";
  OUT8 <= IN1 <= IN2 or IN1 = "1111" or IN2 = "0000";
end;
