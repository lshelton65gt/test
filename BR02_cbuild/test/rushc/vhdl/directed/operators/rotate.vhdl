-- Basic tests on rotate operators
package p_rotate is
  type outarray is array(natural range <>) of boolean;
end;
library ieee;
use ieee.numeric_std.all;
use work.p_rotate.all;
entity rotate is
port(IN1, IN2 : bit_vector(7 downto 0);
     SIN1, SIN2 : signed(7 downto 0);
     OUT1 : out outarray(1 to 5));
end;
architecture rotate of rotate is
  constant z : bit_vector(7 downto 0) := "11111110";
  signal S : bit_vector(7 downto 0);
begin
  OUT1(1) <= IN1 = (IN1 rol 8);
  OUT1(2) <= IN2 = (IN2 ror 8);
  OUT1(3) <= SIN2 = ((SIN2 rol 8) ror 8);
  OUT1(4) <= (IN1 rol 1) = (IN1 ror 1);
  S <= (IN1 rol 1);
  OUT1(5) <= S(0) = IN1(7);
end;
