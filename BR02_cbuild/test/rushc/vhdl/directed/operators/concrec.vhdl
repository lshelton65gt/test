-- Test & (concatenation) operator on arrays of records
package p_concrec is
   type rec is record
     a : bit;
     b : boolean;
     c : integer range -4 to 3;
   end record;
   type carray is array(natural range <>) of rec;
end;
use work.p_concrec.all;
entity concrec is
port(
     IN1, IN2 : carray(0 to 3);
     OUT1 : out carray(0 to 9));
end;
architecture concrec of concrec is
  constant PADDING : carray(1 downto 0) := (('0', false, 0), ('1', true, 1));
   
begin
  OUT1 <= PADDING & IN1 & IN2;
end;
