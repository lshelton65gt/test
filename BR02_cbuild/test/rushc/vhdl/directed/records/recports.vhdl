package p_recports is
  type rec is record
    A : bit;
    B : bit;
  end record;
end;
use work.p_recports.all;
entity bot is
  port(IN1, IN2 : rec; 
       OUT1     : out rec);
end;
architecture bot of bot is
begin
  OUT1.A <= IN1.B or IN2.A; 
  OUT1.B <= IN1.A or IN2.B; 
end;

use work.p_recports.all;
entity recports is
  port(IN1,IN2 : rec; 
       OUT1: out rec);
end;
architecture recports of recports is
  component bot is
    port(IN1,IN2 : rec; 
         OUT1: out rec);
  end component;
  type arr is array(1 to 2) of rec;
  signal S : arr;
  signal R1 : rec;
  signal R2 : rec;
  --for all:bot use entity work.bot(bot);
begin
  S(1) <= IN1;
  S(2) <= IN2;
  R1 <= S(1);
  R2 <= S(2);
  u1:bot port map(R1, R2,OUT1);
end;

