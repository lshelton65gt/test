package p_frec is
   type REC is record
      pos : bit;
      neg : bit;
   end record;
end package;

use work.p_frec.all;
entity frec is
   port (OUT1 : out REC);
end entity;

architecture frec of frec is
  function func(A : bit; B:bit) return REC is
    variable v,y : REC;
    constant z : REC := ('0','0');
  begin
    if ((A and B) = '1') then
      v.pos := '1';
      v.neg := '0';
      return v;
    elsif ((A nand B) = '1') then
      y.pos := '0';
      y.neg := '1';
      return y;
    end if;
    return z;
  end;
  procedure proc (signal O : out REC) is
  begin
   O.pos <= func('1', '1').pos;
   O.neg <= func('0', '0').neg;
  end;
begin
   --OUT1.pos <= func('1', '1').pos;
   --OUT1.neg <= func('0', '0').neg;
   proc(OUT1);
end;
