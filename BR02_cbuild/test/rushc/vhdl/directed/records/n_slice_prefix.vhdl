-- Negative test of selected name using slice name as prefix
package p_n_slice_prefix is
   type REC is record
      A : bit;
      B : bit_vector(7 downto 0);
      C : bit_vector(15 downto 0);
   end record;

   type T is array(1 to 8 ) of REC;
end package;

use work.p_n_slice_prefix.all;
entity n_slice_prefix is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture n_slice_prefix of n_slice_prefix is
begin
  process (IN1)
  begin
     -- A slice of an array of records is not a record
     OUT1(1 to 7).C <= IN1(2 to 8).C;
  end process;
end;
