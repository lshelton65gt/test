-- Basic test of arrays of records
package p_array_of_rec is
   type REC is record
      A : bit;
      B : bit_vector(7 downto 0);
   end record;

   type T is array(1 to 8 ) of REC;
end package;

use work.p_array_of_rec.all;
entity array_of_rec is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture array_of_rec of array_of_rec is
begin
  process (IN1)
  begin
   OUT1 <= IN1;
  end process;
end;
