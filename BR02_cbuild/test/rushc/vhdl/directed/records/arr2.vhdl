-- Two-dimensional arrays of records
package p_arr2 is
   type REC is record
      A : bit;
      B : bit_vector(7 downto 0);
   end record;
   type T2 is array(3 to 5, 4 to 6) of REC;
   type T is array(1 to 3, 2 to 4) of T2;
end package;

use work.p_arr2.all;
entity arr2 is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture arr2 of arr2 is
begin
  process (IN1)
  begin
     OUT1 <= IN1;
  end process;
end;
