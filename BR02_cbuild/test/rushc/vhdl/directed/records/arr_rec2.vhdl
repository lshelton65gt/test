-- Basic test of arrays of records with element that is an array of records
-- and selected name whose prefix is an indexed name
package p_arr_rec2 is
   type REC is record
      A : bit;
      B : bit;
   end record;
   type T is array(1 to 2) of REC;
end package;

use work.p_arr_rec2.all;
entity arr_rec2 is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture arr_rec2 of arr_rec2 is
begin
  process (IN1)
  begin
   OUT1(1).A <= IN1(1).A and IN1(2).A;
   OUT1(1).B <= IN1(1).B and IN1(2).B;
   OUT1(2).A <= IN1(2).A and IN1(1).A;
   OUT1(2).B <= IN1(2).B and IN1(1).B;
  end process;
end;
