-- Test instantiation of component with record ports
package p_cell is
   type REC is record
      A : bit;
      B : bit;
   end record;

   --type T is array(1 to 1) of REC;
end package;

use work.p_cell.all;
entity cell is
   port (IN1 : REC;
         OUT1 : out REC);
end entity;

architecture cell of cell is
begin
  process (IN1)
  begin
    OUT1.A <= IN1.A;
    OUT1.B <= IN1.B;
  end process;
end;

use work.p_cell.all;
use work.all;
entity inst_recports is
   port (A : REC;
         B : out REC);
end entity;
architecture inst_recports of inst_recports is
  component cell is
   port (IN1 : REC;
         OUT1 : out REC);
  end component;
  for U1:cell use entity work.cell(cell);
begin
  U1: cell port map (A, B);
end;
