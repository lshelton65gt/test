-- Basic test of selected names
package p_selected_name is
   type T is record
      e_bit : bit;
      e_bv : bit_vector(7 downto 0);
      e_int : integer range 0 to 31;
   end record;
end package;

use work.p_selected_name.all;
entity selected_name is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture selected_name of selected_name is
begin
  process (IN1)
  begin
   OUT1.e_bit <= IN1.e_bit;
   OUT1.e_bv <= IN1.e_bv;
   OUT1.e_int <= IN1.e_int;
  end process;
end;
