-- Test selected name of an element of an array of records. A slice name is used which is then
-- indexed with a constant.
package p_selname3 is
   type REC is record
      A : bit;
      B : bit;
   end record;

   type T is array(positive range <>) of REC;
   subtype ST is T(1 to 4);
end package;

use work.p_selname3.all;
entity selname3 is
   port (IN1 : T(1 to 8);
         OUT1 : out T(1 to 4));
end entity;

architecture selname3 of selname3 is
begin
  process (IN1)
    variable V : ST;
  begin
    V := IN1(1 to 4);
    for i in 1 to 8 loop
       OUT1(i).A <= V(1).A;
       OUT1(i).B <= V(1).B;
    end loop;
  end process;
end;
