-- Basic test of arrays of records with element that is an array of records
package p_arr_rec is
   type REC1 is record
      A : bit;
      B : bit_vector(7 downto 0);
   end record;
  type ARREC is array (0 to 2) of REC1;
   type REC2 is record
      A : ARREC;
      B : bit_vector(7 downto 0);
   end record;
   type T is array(1 to 8 ) of REC2;
end package;

use work.p_arr_rec.all;
entity arr_rec is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture arr_rec of arr_rec is
begin
  process (IN1)
  begin
   OUT1 <= IN1;
  end process;
end;
