-- Basic of indexed name of record element
package p_indexed_elem is
   type REC is record
      e_bit : bit;
      e_bv : bit_vector(7 downto 0);
      e_int : integer range 0 to 15;
   end record;
end package;

use work.p_indexed_elem.all;
entity indexed_elem is
   port (IN1 : REC;
         OUT1 : out REC);
end entity;

architecture indexed_elem of indexed_elem is
begin
   process (IN1)
   begin
     OUT1.e_bit <= IN1.e_bit;
     for i in 7 downto 0 loop
       OUT1.e_bv(i) <= IN1.e_bv(i);
     end loop;
     OUT1.e_int <= IN1.e_int;
   end process;
end;
