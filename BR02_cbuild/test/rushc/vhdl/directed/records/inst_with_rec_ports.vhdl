-- Basic test of component instantiation of entity with record ports
package p_inst_with_rec_ports is
   type REC is record
      e_bit : bit;
      e_bv : bit_vector(7 downto 0);
      e_int : integer range 0 to 15;
   end record;
end package;

use work.p_inst_with_rec_ports.all;
entity sub is
   port (IN1 : REC;
         OUT1 : out REC);
end entity;

architecture sub of sub is
begin
   OUT1.e_bit <= IN1.e_bit;
   OUT1.e_bv <= IN1.e_bv;
   OUT1.e_int <= IN1.e_int;
end;
use work.sub;
use work.p_inst_with_rec_ports.all;
entity inst_with_rec_ports is
   port (IN1 : REC;
         OUT1 : out REC);
end entity;

architecture inst_with_rec_ports of inst_with_rec_ports is
  component sub is
   port (IN1 : REC;
         OUT1 : out REC);
  end component;
  for U1:sub use entity work.sub(sub);
begin
  U1: sub port map(IN1,OUT1);
end;
