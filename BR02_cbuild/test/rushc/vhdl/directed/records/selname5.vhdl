-- Test selected name of an element of an array of records
package p_selname5 is
   type REC is record
      A : bit;
      B : bit;
   end record;

   type T is array(1 to 2) of REC;
end package;

use work.p_selname5.all;
entity selname5 is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture selname5 of selname5 is
begin
  process (IN1)
  begin
   for i in T'range loop
     OUT1(i).A <= IN1(i).A and IN1(i).B;
     OUT1(i).B <= IN1(i).B xor IN1(i).A;
   end loop;
  end process;
end;
