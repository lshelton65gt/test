-- Basic test of arrays of records of records
package p_rec_of_rec is
   type REC1 is record
      A : bit;
      B : bit_vector(7 downto 0);
   end record;
   type REC2 is record
      C : REC1;
      D : bit_vector(7 downto 0);
   end record;
   type T is array(1 to 8 ) of REC2;
end package;

use work.p_rec_of_rec.all;
entity rec_of_rec is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture rec_of_rec of rec_of_rec is
begin
  process (IN1)
  begin
   OUT1 <= IN1;
  end process;
end;
