-- Test multilevel nesting of records and selected name
package p_rec0 is
   type REC is record
      C : bit;
   end record;
   type R2 is record
      B : REC;
   end record;
end package;

use work.p_rec0.all;
entity rec0 is
   port (IN1 : R2;
         OUT1 : out R2);
end entity;

architecture rec0 of rec0 is
begin
  process (IN1)
  begin
    OUT1.B.C <= IN1.B.C;
  end process;
end;
