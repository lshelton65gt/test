-- Test multilevel nesting of records, array of such and indexed and selected names
package p_rec1 is
   type REC is record
      A : bit;
      B : bit;
   end record;
   type R2 is record
      A : bit;
      B : REC;
   end record;
   type T is array(1 to 2) of R2;
end package;

use work.p_rec1.all;
entity rec1 is
   port (IN1 : R2;
         OUT1, OUT2 : out R2);
end entity;

architecture rec1 of rec1 is
begin
  process (IN1)
  begin
   --for i in T'range loop
    -- OUT1(i).A <= IN1(i).A;
     --OUT1(i).B.A <= IN1(i).B.A;
     --OUT1(i).B.B <= IN1(i).B.B;
   --end loop;
     OUT1.A <= IN1.A;
    OUT2.A <= IN1.A;
    OUT1.B.A <= IN1.B.A;
    OUT2.B.A <= IN1.B.A;
    OUT1.B.B <= IN1.B.B;
  end process;
end;
