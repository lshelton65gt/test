-- Negative test of recursive records 
package p_n_recursive_rec is
   type REC1;
   type REC2;
   type REC2 is record
      A : bit;
      B : bit_vector(7 downto 0);
      C : REC1;
   end record;
   type REC1 is record
      A : bit;
      B : bit_vector(7 downto 0);
      C: REC2;
   end record;
   type T is record
      C : REC1;
      D : bit_vector(7 downto 0);
      E : REC2;
   end record;
end package;

use work.p_n_recursive_rec.all;
entity n_recursive_rec is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture n_recursive_rec of n_recursive_rec is
begin
  process (IN1)
  begin
   OUT1 <= IN1;
  end process;
end;
