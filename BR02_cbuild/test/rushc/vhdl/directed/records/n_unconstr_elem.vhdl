-- Negative test of records with unconstrained elements
package p_n_unconstr_elem is
   type REC is record
      e_bit : bit;
      e_bv : bit_vector;
      e_int : integer;
   end record;
end package;

use work.p_n_unconstr_elem.all;
entity n_unconstr_elem is
   port (IN1 : bit;
         OUT1 : out bit);
end entity;

architecture n_unconstr_elem of n_unconstr_elem is
   signal S : REC;
begin
   OUT1 <= IN1;
end;
