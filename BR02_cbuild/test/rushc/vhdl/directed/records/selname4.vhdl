-- Arrays of multilevel nested records
package p_selname4 is
   type R1 is record
      A : bit;
   end record;
   type R2 is record
      A : bit;
      B : R1;
   end record;
   type R3 is record
      A : bit;
      B : R1;
      C : R2;
   end record;

   type T is array(1 to 8) of R3;
end package;

use work.p_selname4.all;
entity selname4 is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture selname4 of selname4 is
begin
  process (IN1)
    variable V : T;
  begin
    V := IN1;
    for i in 1 to 8 loop
       OUT1(i).A <= V(i).A or V(i).B.A;
       OUT1(i).B.A <= V(i).B.A or V(i).C.A;
       OUT1(i).C.A <= V(i).A or V(i).C.B.A;
       OUT1(i).C.B.A <= V(i).A or V(i).C.B.A or V(i).B.A;
    end loop;
  end process;
end;
