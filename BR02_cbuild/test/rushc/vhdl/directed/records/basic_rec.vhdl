-- Basic record test
package p_basic_rec is
   type REC is record
      e_bit : bit;
      e_bv : bit_vector(7 downto 0);
      e_int : integer range 0 to 15;
   end record;
end package;

use work.p_basic_rec.all;
entity basic_rec is
   port (IN1 : REC;
         OUT1 : out REC);
end entity;

architecture basic_rec of basic_rec is
begin
   OUT1.e_bit <= IN1.e_bit;
   OUT1.e_bv <= IN1.e_bv;
   OUT1.e_int <= IN1.e_int;
end;
