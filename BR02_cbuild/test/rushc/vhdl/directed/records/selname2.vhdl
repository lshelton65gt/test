-- Test selected name of an element of an array of records. The indexed names use constant indices.
package p_selname2 is
   type REC is record
      A : bit;
      B : bit;
   end record;

   type T is array(1 to 2) of REC;
end package;

use work.p_selname2.all;
entity selname2 is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture selname2 of selname2 is
begin
  process (IN1)
  begin
    OUT1(1).A <= IN1(1).A;
    OUT1(1).B <= IN1(1).B;
    OUT1(2).A <= IN1(2).A;
    OUT1(2).B <= IN1(2).B;
  end process;
end;
