-- Test selected name of an element of an array of records. The indexed names use a loop variable.
package p_selname1 is
   type REC is record
      A : bit;
      B : bit;
   end record;

   type T is array(1 to 2) of REC;
end package;

use work.p_selname1.all;
entity selname1 is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture selname1 of selname1 is
begin
  process (IN1)
  begin
   for i in T'range loop
     OUT1(i).A <= IN1(i).A;
     OUT1(i).B <= IN1(i).B;
   end loop;
  end process;
end;
