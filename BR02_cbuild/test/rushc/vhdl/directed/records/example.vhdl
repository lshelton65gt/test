-- Example used in Steve Lim's email 2013-11-25
-- "Proposed changes to V2N processing of records"
package p_example is
   type R4 is record
      A : bit;
   end record;
   type T4 is array(natural range <>) of R4;
   type R5 is record
      A : T4(1 to 4);
   end record;
end package;

use work.p_example.all;
entity example is
   port (IN1 : T4(1 to 4);
         OUT1 : out R5);
end entity;

architecture example of example is
begin
  process (IN1)
  begin
   for i in 1 to 4 loop
     OUT1.A(i).A <= IN1(i).A;
   end loop;
  end process;
end;
