-- Test function that returns a record.
package p_funcrec is
   type REC is record
      A : bit;
      B : bit;
   end record;
   type T is array(1 to 2) of REC;
end package;

use work.p_funcrec.all;
entity funcrec is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture funcrec of funcrec is
  function func(A : bit; B:bit) return REC is
    variable v : REC;
  begin
    v.A := A;
    v.B := B;
    return v;
  end;
begin
  process (IN1)
  begin
   OUT1(1).A <= func('1', '0').A;
   OUT1(1).B <= func('0', '1').B;
   OUT1(2).A <= IN1(2).A and IN1(1).A;
   OUT1(2).B <= IN1(1).B and IN1(2).B;
  end process;
end;
