-- Test multilevel nesting of records, array of such and indexed and selected names
package p_rec3 is
   type REC1 is record
      A : bit;
      B : bit;
   end record;
   type REC2 is record
      A : bit;
      B : bit;
      C : REC1;
   end record;
   type REC3 is record
      A : bit;
      B : bit;
      C : REC2;
   end record;

   type T is array(1 to 2) of REC3;
end package;

use work.p_rec3.all;
entity rec3 is
   port (IN1 : T;
         OUT1 : out T);
end entity;

architecture rec3 of rec3 is
begin
  process (IN1)
  begin
   for i in T'range loop
     OUT1(i).A <= IN1(i).A;
     OUT1(i).B <= IN1(i).B;
     OUT1(i).C.A <= IN1(i).A;
     OUT1(i).C.B <= IN1(i).B;
     OUT1(i).C.C.A <= IN1(i).A;
     OUT1(i).C.C.B <= IN1(i).B;
   end loop;
  end process;
end;
