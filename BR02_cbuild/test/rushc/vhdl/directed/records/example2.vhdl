-- Example used in 2013-11-23 Steve Lim's email
-- "Proposed changes to V2N processing of records"
package p_example2 is
   type R1 is record
      A : bit;
      B : bit_vector(7 downto 0);
   end record;
   type R6 is record
      A : bit;
      B : R1;
   end record;
end package;

use work.p_example2.all;
entity example2 is
   port (IN1 : R6;
         OUT1 : out R6);
end entity;

architecture example2 of example2 is
begin
  process (IN1)
  begin
   OUT1.A <= IN1.A;
   OUT1.B.A <= IN1.B.A;
   OUT1.B.B <= IN1.B.B;
  end process;
end;
