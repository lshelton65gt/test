module top (in1, in2, in3, out1, out2, out3);
   input in1, in2, in3;
   output out1, out2, out3;   

   wire [2:0] tmp;

   assign     out1 = tmp[0];
   assign     out2 = tmp[1];
   assign     out3 = tmp[2];  
   
   foo f (in1, in2, in3, tmp);
   
endmodule // foo


module foo (in1, in2, in3, out1);
   input in1, in2, in3;
   output [2:0] out1;

// Mimic the tie that happens from the directives
`ifdef EVENTSIM
   assign      (supply1, supply0) in2 = 1'b1;
`endif
   
   assign       out1 = {in3, in2, in1};
endmodule // foo

