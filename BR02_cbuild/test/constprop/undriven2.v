module top (out, in);
   output out;
   input  in;

   reg    r1;
`ifdef EVENTSIM
   initial r1 = 0;
`endif
   
   assign out = ~r1 & in;

endmodule
