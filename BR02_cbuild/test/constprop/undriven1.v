module top (out, in);
  output out;
  input in;

  reg r1;
  initial r1 = 1;
  assign out = r1 & in;

endmodule
