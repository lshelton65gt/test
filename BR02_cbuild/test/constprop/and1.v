module top (out1, out2, in1, in2, clki, rst);
   output out1, out2;
   input  in1, in2, clki, rst;

   reg    r1;
   initial r1 = 0;
   always @ (posedge clki or posedge rst)
     if (rst)
       r1 <= 0;
     else
       r1 <= in1;

   wire   clk;
   sub S1 (clk, r1, clki, 1'b0);
   reg    out1;
   initial out1 = 0;
   always @ (posedge clk)
     out1 <= in2;

   reg    out2;
   initial out2 = 0;
   always @ (posedge clki)
     out2 <= r1;

endmodule // top

module sub (y, a, b, c);
   output y;
   input  a, b, c;

   and (y, a, b, c);

endmodule // sub
