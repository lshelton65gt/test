/*
 Make sure constants don't propagate from always-blocks through
 task enables into tasks.  This is a variant on the testcase from
 bug4828, that didn't fail prior to bug4828 being fixed, but we want
 to make sure it continues not failing.
*/

module bug4828(out, clk);
output [4:0] out;
input  clk;
reg [4:0] data;
reg [4:0] out;

initial out = 5'b0;           //  help sim gold files match

task inc;
begin
  data = data + 1'b1;
end
endtask

always @(negedge clk) begin
  data = 0;
  inc;
  out = data;
end

endmodule


