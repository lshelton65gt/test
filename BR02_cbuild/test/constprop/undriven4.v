module top (out, in);
   output out;
   input  in;

   tri1  r1;
   assign out = r1 & in;

endmodule
