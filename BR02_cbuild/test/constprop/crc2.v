module crc(hec_data_out, data_in);
  input	[7:0]		data_in;
  output [3:0]          hec_data_out;
  
  reg			feed;
  reg [3:0]             crc_data;
  integer               i;
  
  assign #(2)           hec_data_out = crc_data;
  
  always @(data_in) begin
    crc_data = 0;
    for (i = 7; (i >= 0); i = (i - 1)) begin
      feed = (crc_data[3] ^ data_in[i]);
      crc_data[3] = crc_data[2];
      crc_data[2] = (feed ^ crc_data[1]);
      crc_data[1] = (feed ^ crc_data[0]);
      crc_data[0] = feed;
    end
  end
endmodule
