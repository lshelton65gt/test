
module bug4828(out, clk);
output [4:0] out;
input  clk;
reg [4:0] data;
reg [4:0] out;

initial out = 5'b0;           //  help sim gold files match

task start_inc;
begin
  data = 1'b0;
  inc;
end
endtask

task inc;
begin
  data = data + 1'b1;
end
endtask

always @(negedge clk) begin
  start_inc;
  inc;
  out = data;
end

endmodule


