module top (out, in);
  output out;
  input in;

  sub1 S1 (out, in);
  sub2 S2 (out, in);

endmodule

module sub1 (o1, i1);
  output o1;
  input i1;

  assign o1 = i1;

endmodule

module sub2 (o2, i2);
  output o2;
  input i2;

  assign o2 = i2;

endmodule
