// This is like tienet19.v except the tied port is an inout.

module top (out, clk, d, en);
   output out;
   input  clk, d, en;

   // Create a derived clock
   wire   gclk;
   clksub CS (gclk, clk, en & out);

   // Create an alias of the derived clock
   sub S(gclk);

   // Use that clock
   reg    out;
   initial out = 0;
   always @ (posedge gclk)
     out <= d;

endmodule

module clksub(gclk, clk, en);
   output gclk;
   input  clk, en;

   assign gclk = clk & en;

endmodule

module sub (clkout);
   inout clkout;

`ifdef EVENTSIM
   assign (supply1, supply0) clkout = 1'b0;
`endif
endmodule
