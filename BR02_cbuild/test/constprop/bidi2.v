module top (out1, out2, out3, out4);
   output out1, out2, out3, out4;

   // Constant is driven from inside. out1 = 1, out2 = 0
   buf1 P1 (out2, out1);

   // Constant is driven from outside, out3 = 0, out4 = 1
   assign out3 = 1'b0;
   buf2 P2 (out4, out3);

endmodule

module buf1 (out, io);
   output out;
   inout  io;

   assign io = 1'b1;
   assign out = ~io;

endmodule

module buf2 (out, io);
   output out;
   inout  io;

   assign out = ~io;

endmodule

