// This test case tries to reproduce bug8168, a hang in constant
// propagation but it does not do so. But it did find that port
// connections where traversed and excessive number of times when
// creating expressions for v000045 in sub2.

module top (out1, out2, out3, out4, clk, in1, in2, in3, in4);
   output [65:0] out1, out2, out3, out4;
   input         clk;
   input [63:0]  in1, in2, in3, in4;

   wire [63:0]   c1, c2, c3, c4;

   sub1 S1 (out1, out2, out3, out4, {c1[31], c1}, {c2[31], c2},
            {c3[31], c3}, {c4[31], c4}   );

   sub3 S2 (c1, c2, c3, c4, clk, in1, in2, in3, in4);

endmodule

module sub1 (out1, out2, out3, out4, in1, in2, in3, in4);
   output [65:0] out1, out2, out3, out4;
   input [64:0]  in1, in2, in3, in4;

   sub2 S1 (out1, in1, in2, 1'b0);
   sub2 S2 (out2, in2, in3, 1'b0);
   sub2 S3 (out3, in3, in4, 1'b0);
   sub2 S4 (out4, in4, in1, 1'b0);

endmodule


module sub2 (out, v000043, v000052, v025346);
   output [65:0] out;
   input [64:0]  v000043, v000052;
   input          v025346;

   reg [65:0]    v031187;
   reg [63:0]    v013066;
   reg [63:0]    v000045;

   always @ (v000052 or v025346 or v000043)
     // $block_v031186;2
     begin                                        // ./f000000/f000001/f000793/f006074/f006075/f011624.v:90
        // Declarations
        // Statements
        v031187 = (~{1'b0,v000052});                          // ./f000000/f000001/f000793/f006074/f006075/f011624.v:91
        v013066[0] = (~v025346);                            // ./f000000/f000001/f000793/f006074/f006075/f011624.v:90
        v013066[1] = (((v000043[0] & v031187[0]) | (v031187[0] & v013066[0])) | (v000043[0] & v013066[0])); // ./f000000/f000001/f000793/f006074/f006075/f011624.v:94
        v013066[2] = (((v000043[1] & v031187[1]) | (v031187[1] & v013066[1])) | (v000043[1] & v013066[1])); // ./f000000/f000001/f000793/f006074/f006075/f011624.v:94
        v000045[0] = ((v000043[0] ^ v031187[0]) ^ v013066[0]);          // ./f000000/f000001/f000793/f006074/f006075/f011624.v:93
        v000045[1] = ((v000043[1] ^ v031187[1]) ^ v013066[1]);          // ./f000000/f000001/f000793/f006074/f006075/f011624.v:93
     end

   assign out = { v000045, v000052 };

endmodule

module sub3 (o1, o2, o3, o4, clk, i1, i2, i3, i4);
   output [63:0] o1, o2, o3, o4;
   input         clk;
   input [63:0]  i1, i2, i3, i4;

   sub4 S1 [63:0] (o1, clk, i1);
   sub4 S2 [63:0] (o2, clk, i2);
   sub4 S3 [63:0] (o3, clk, i3);
   sub4 S4 [63:0] (o4, clk, i4);

endmodule

module sub4 (q, clk, d);
   output q;
   input  clk;
   input  d;

   reg    q;
   always @ (posedge clk)
     q <= d;

endmodule
