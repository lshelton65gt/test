// Test tie net using a constant that is too big (truncate)
module top (out, clk, in, en);
   output out;
   input  clk, in;
   input [1:0] en;

`ifdef EVENTSIM
   sub S1 (out, clk, in, 3'h7);
`else
   sub S1 (out, clk, in, en);
`endif

endmodule // top

module sub (out, clk, in, en);
   output out;
   input  clk, in;
   input [1:0] en;

   reg    out;
   initial begin
      out = 0;
   end

   wire [1:0] dis = ~en;
   
   always @ (posedge clk)
     if (dis == 2'b11)
       out <= in;

endmodule // top
