module top (out, in, en);
   output out;
   input  in, en;

   wire   c1;
   sub S1 (c1, in, en, 1'b1);
   assign out = ~c1;

endmodule // top

module sub (out, a, b, in);
   output out;
   input  a, b, in;

   bidi B1 (in, a, 1'b0);
   assign out = a ? (b & in): 1'b1;

endmodule // sub

module bidi(io, i, e);
   inout io;
   input i, e;

   assign io = e ? i : 1'bz;

endmodule // bidi

   
