module top (out, in);
   output out;
   input  in;

   wire    r1;
   
   assign out = r1 & in;

   sub S1 (r1);

endmodule

module sub (o1, i1);
   output o1;
   input  i1;

`ifdef EVENTSIM
   tri0   i1;
`endif

   assign o1 = ~i1;

endmodule // sub
