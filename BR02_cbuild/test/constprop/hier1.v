// This is test case that fixed a propagation bug through port
// connections with multiple drivers (initial, posedge clk and posedge
// rst).

module top (out, clk, in);
   output out;
   input  clk, in;

   wire   r1;
   flop F1 (r1, clk, 1'b1, in);
   assign out = ~r1;

endmodule // top

module flop (q, clk, rst, d);
   output q;
   input  clk, rst, d;
   reg    q;

   initial q = 0;
   always @ (posedge clk or posedge rst)
     if (rst)
       q <= 0;
     else
       q <= d;

endmodule // flop

   
