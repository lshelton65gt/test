module top (out, s0, s1, s2, s3);
   output [3:0] out;
   input        s0, s1, s2, s3;

   sub S1 (out, s0, s1, s2, s3, 4'h0, 4'h0, 4'h0, 4'h0);

endmodule // top


module sub (out, s0, s1, s2, s3, d0, d1, d2, d3);
   output [3:0] out;
   input        s0, s1, s2, s3;
   input [3:0]  d0, d1, d2, d3;

   assign        out =
                 {4 { s0 } } & d0 |
                 {4 { s1 } } & d1 |
                 {4 { s2 } } & d2 |
                 {4 { s3 } } & d3;

endmodule // top
