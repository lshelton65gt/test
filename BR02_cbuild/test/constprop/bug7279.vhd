
library IEEE;
use IEEE.std_logic_1164.all;

entity bug7279 is 
    port(clk		: in  STD_LOGIC;
	async_reset	: in  STD_LOGIC; 
	control_reg_we	: in  STD_LOGIC; 
	write_dbus	: in  STD_LOGIC_VECTOR (15 downto 0);
	out_ctrl	: out STD_LOGIC_VECTOR (4 downto 0);
	out_bus		: out STD_LOGIC_VECTOR (15 downto 0) );
end bug7279;


architecture rtl of  bug7279 is

	SIGNAL control_15       : std_logic; 
	SIGNAL control_14       : std_logic; 
	SIGNAL control_13       : std_logic; 
	SIGNAL control_12       : std_logic; 
	SIGNAL control_7        : std_logic; 
	SIGNAL control_reg      : std_logic_vector(15 DOWNTO 0) := (OTHERS => '0');

begin

	PROCESS(clk,async_reset)
	BEGIN
	 IF (async_reset = '0') THEN
	   control_reg(15 DOWNTO 12) <= (OTHERS => '0');
	   control_reg( 8 DOWNTO  6) <= (OTHERS => '0');
	 ELSIF (clk'EVENT AND clk = '1') THEN
	   IF (control_reg_we = '1') THEN
	     control_reg(15 DOWNTO 12) <= write_dbus(15 DOWNTO 12);
	     control_reg( 8 DOWNTO  6) <= write_dbus( 8 DOWNTO  6);
	   END IF;
	 END IF;
	END PROCESS;

--	control_reg(11 DOWNTO 9) <= (OTHERS => '0');
--	control_reg(5 DOWNTO 0) <= (OTHERS => '0');

	control_15      <= control_reg(15); 
	control_14      <= control_reg(14); 
	control_13      <= control_reg(13);
	control_12      <= control_reg(12);
	control_7       <= control_reg(7);


	PROCESS(clk,async_reset)
	BEGIN
	 IF (async_reset = '0') THEN
	   out_bus( 15 DOWNTO  0) <= (OTHERS => '0');
	 ELSIF (clk'EVENT AND clk = '1') THEN
	   IF (control_reg_we = '1') THEN
	     out_bus(15 DOWNTO 0) <= control_reg (15 DOWNTO 0);
	     out_ctrl(4) <= control_15;
	     out_ctrl(3) <= control_14;
	     out_ctrl(2) <= control_13;
	     out_ctrl(1) <= control_12;
	     out_ctrl(0) <= control_7;
	   END IF;
	 END IF;
	END PROCESS;

end rtl;

