module top(loc16, o1, o2);
   output [3:0] o1, o2;
   input [1:0] loc16;

   bottom bottom (o1, o2, loc16);
endmodule

module bottom(naccen, out, loc16);
   output [3:0] naccen;
   output [3:0] out;
   input [1:0]  loc16;
   reg [3:0]    naccen, out;

   // Due to some test driver issues, we run an extra cycle with
   // Carbon and loc16 = 0 at the start.
   initial naccen = 1;

   always @(loc16)
     begin
        naccen[loc16] = 1'b1;
        out = 4'b0101;
     end
endmodule
