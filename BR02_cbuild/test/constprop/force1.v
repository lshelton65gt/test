module top (out, clk, in);
   output out;
   input  clk, in;

   reg    out;
   reg    ren;
   initial begin
      out = 0;
      ren = 0;
   end
   
   wire en = 1;

   wire dis = ~en;
   
   always @ (posedge clk)
     if (~dis)
       out <= in;

endmodule // top
