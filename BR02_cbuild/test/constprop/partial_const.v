module top(clk, in1, in2, out);
  input clk;
  input [7:0] in1, in2;
  output [8:0] out;
  reg [8:0]    out;

  wire [8:0]   temp;
  assign       temp[8] = 1'b0;  // found by constant propagation
  assign       temp[7:0] = in1 ^ in2;  // should be rescoped into always block

  always @(posedge clk) begin
    out <= ~temp;
  end
endmodule
