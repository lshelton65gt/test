// Simulation is now enabled yet because we can handle z drivers in always
// blocks correctly, at least the ones here.
//
// This test found a bug in constant propagation because it was letting
// dead elaborated flow remove an opportunity.
module foo(PAD, in, out, PAD2, in2, out2, en2, en);
   input out, en;
   output in;
   inout  PAD;

   input  out2, en2;
   output in2;
   inout  PAD2;

   wire   dead;
   
   LVIOOUT_PD FOO(.din(out), .oen(en), .pad(PAD) );
   LVIOOUT_PD FOO2(.din(out2), .oen(en2), .pad(PAD2) );
   LVIOOUT_PD FOO3(.din(out2), .oen(en2), .pad(dead));
endmodule

module LVIOOUT_PD(din, oen, pad, vdd, vss, vd33, vsspst);
   input din;
   input oen;
   inout pad;
   inout vdd; // carbon tieNet 1'b1
   inout vss; // carbon tieNet 1'b0
   inout vd33; // carbon tieNet 1'b1
   inout vsspst; // carbon tieNet 1'b0

`ifdef CARBON
`define vdd vdd
`define vss vss
`define vd33 vd33
`define vsspst vsspst
`else

// Let Aldec know about the tie-nets

`define vdd 1'b1
`define vss 1'b0
`define vd33 1'b1
`define vsspst 1'b0
`endif


   // Local nets (2)
   reg 	 pad_driver;
   wire  power_valid;
   // Continuous Assigns
   assign pad = pad_driver;

   assign power_valid = ((((`vdd == 1'b1) && (`vss == 1'b0)) && (`vd33 == 1'b1)) && (`vsspst == 1'b0));
   always @ (power_valid or din or oen)
     begin                                         
	if ((((!power_valid) || 0) || 0)) begin    
	   pad_driver = 1'bx;
	end
	else begin
	   if ((!oen)) begin                       
	      if ((0 || 0)) begin                  
		 pad_driver = 1'bx;
	      end
	      else begin
		 pad_driver = din;
	      end
	   end
	   else begin
	      pad_driver = 1'bz;
	   end // else: !if((!oen))
	end // else: !if((((!power_valid) || 0) || 0))
     end // always begin
endmodule // LVIOOUT_PD

