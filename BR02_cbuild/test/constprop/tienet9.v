// Test tie net on net with multiple bit assigns in the same block
module top (out, clk, in, en);
   output out;
   input  clk, in;
   input [1:0] en;

   sub S1 (out, clk, in, en);

endmodule // top

module sub (out, clk, in, en);
   output out;
   input  clk, in;
   input [1:0] en;

   reg    out;
   initial begin
      out = 0;
   end

`ifdef EVENTSIM
   wire [1:0] dis = 2'b10;
`else
    reg [1:0] dis;
   always @ (en)
     begin
        dis[0] = ~en[1];
        dis[1] = ~en[0];
     end
`endif
   
   always @ (posedge clk)
     if (dis == 2'b10)
       out <= in;

endmodule // top
