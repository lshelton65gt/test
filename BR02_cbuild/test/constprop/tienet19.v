// This test is related to bug6213. It allows tieNets to
// propagate up the hierarchy to make up for some missing
// simplification in global constant propagation. Global
// constant propagation currently doesn't prune multiple
// drivers of a net if one driver is at tie strength.

module top (out, clk, d, en);
   output out;
   input  clk, d, en;

   // Create a derived clock
   wire   gclk;
   clksub CS (gclk, clk, en & out);

   // Create an alias of the derived clock
   sub S(gclk);

   // Use that clock
   reg    out;
   initial out = 0;
   always @ (posedge gclk)
     out <= d;

endmodule

module clksub(gclk, clk, en);
   output gclk;
   input  clk, en;

   assign gclk = clk & en;

endmodule

module sub (clkout);
   output clkout;

`ifdef EVENTSIM
   assign (supply1, supply0) clkout = 1'b0;
`endif
endmodule
