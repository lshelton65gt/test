module top (out1, out2, in1, in2);
   output out1, out2;
   input  in1, in2;

   sub S1 (out1, in1, 1'b0, 1'b0);
   sub S2 (out2, in2, 1'b0, 1'b0);

endmodule // top

module sub (out, d1, d2, d3);
   output out;
   input  d1, d2, d3;

   wire	  c1, c2;
   doand A1 (c1, d1, d2);
   doand A2 (c2, d1, d3);
   assign out = c1 ^ c2;

endmodule // sub

   
module doand(y, a, b);
   output y;
   input  a, b;

   assign y = a & b;

endmodule // doand
