module top (out, clk, in, sel);
   output [31:0] out;
   input         clk;
   input [31:0]  in;
   input         sel;

   reg [31:0]    out;
   reg [31:0]    tmp [0:1];

   initial begin
      tmp[0] = 0;
   end


   always @ (posedge clk)
     begin
        out <= in & ~tmp[sel];
     end

endmodule
