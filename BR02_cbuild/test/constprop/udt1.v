module top(clk);
   input clk;
   wire  c1, c2, t1, t2;

   dut DUT (c1, c2, t1, t2, 1'b0);

   always @ (posedge clk)
     $udt(t1, t2, c1, c2);
   
endmodule // top

module dut (out1, out2, in1, in2, en);
   output out1, out2;
   input  in1, in2, en;

   assign out1 = in1 & en;
   assign out2 = in2 & en;

endmodule // dut
