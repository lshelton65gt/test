module top (out, clk, in);
   output out;
   input  clk, in;

   reg  r1;
   initial r1 = 0;
   always @ (posedge clk)
     r1 <= in;
   
  assign out = ~r1 & in;

endmodule
