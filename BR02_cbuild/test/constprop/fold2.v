module top (out, in);
   output out;
   input  in;

   reg 	  a, b, c, out;
   always @ (in)
     a = in & 0;
   always @ (a)
     b = ~a;
   always @ (a or b)
     c = a ^ b;
   always @ (c)
     out = ~c;
   
endmodule // top

