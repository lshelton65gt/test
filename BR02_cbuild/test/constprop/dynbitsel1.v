module top(loc16, o);
   output [3:0] o;
   input [1:0] loc16;

   bottom bottom (o, loc16);
endmodule

module bottom(naccen, loc16);
   output [3:0] naccen;
   reg [3:0] naccen;
   input [1:0] loc16;

   // Due to some test driver issues, we run an extra cycle with
   // Carbon and loc16 = 0 at the start.
   initial naccen = 1;
   
   always @(loc16)
     begin
        naccen[loc16] = 1'b1;
     end
endmodule
