module top (out, clk, in);
   output [3:0] out;
   input        clk;
   input [3:0]  in;

   wire         rst = 0;

   reg [3:0]    out;
   always @ (posedge clk or negedge rst)
     if (~rst)
       if (1'b1) begin
          out <= 4'h5;
       end
       else begin
          out <= 4'ha;
       end
     else
       out <= in;

endmodule // top
