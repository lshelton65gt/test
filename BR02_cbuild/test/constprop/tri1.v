module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   reg 	  r1, r2;
   always @ (posedge clk)
     begin
	r1 <= in1;
	r2 <= in2;
     end

   wire bus;
   assign bus = r1 ? r2 : 1'bz;
   sub S1 (bus, 1'b0, r1);

   assign out1 = bus;
   assign out2 = r1 ^ r2;

endmodule

module sub (io, en, in);
   inout io;
   input en, in;

   assign io = en ? in : 1'bz;

endmodule
