module top(in, cond, out, out2);
  input in, cond;
  output out, out2;
  reg    out, out2;

  always @(out) begin
    out2 = out;
    out = 1'b0;
  end


  always @(cond or in) begin
    if (!cond)
      out = in;
  end

endmodule
