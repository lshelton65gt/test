module top (out, in, mask, clk);
   output [3:0] out;
   input [3:0]  in;
   input [3:0]  mask;
   input        clk;

   reg [3:0]    out;
   reg [1:0]    x;
   integer      i;

   initial out = 4'b0;
   always @ (posedge clk)
     begin
        x = 0;
        for (i = 0; i < 4; i = i + 1) begin
           if(mask[i]) begin
              out[x] = in[i];
           end
           x = x + 1;
        end
     end

endmodule
