module top (out, clk, in1, in2);
   output out;
   input  clk;
   input  in1, in2;

   reg    r1, r2;
   always @ (posedge clk)
     begin
        r1 <= in1;
        r2 <= in2;
     end

   wire c1;
   wire c2 = 1'b1;
   sub1 S1 (c1, ~c2);
   wire c3;
   sub2 S2 (c3, r1, r2, c1);

   reg  out;
   always @ (posedge clk)
     out <= ~c3;

endmodule // top

module sub1 (b, a);
   output b;
   input  a;

   assign b = ~a;

endmodule // sub1

module sub2 (z, w, x, y);
   output z;
   input  w, x, y;

   reg   t1, t2;
   always @ (w or y)
     t1 = w & ~y;
   always @ (x or y)
     t2 = w | y;

   assign z = t1 ^ t2;

endmodule // sub2

  
