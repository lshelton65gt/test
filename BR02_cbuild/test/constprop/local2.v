// This test exercises the RELocalConstProp code. It propagates
// constants within a block when possible. This test case makes
// sure we propagate partial constants correctly.
//
// Currently we don't handle partial constants
module top (out1, out2, out3, out4, out5, out6, in1, in2);
   output [1:0] out1, out2, out3, out4, out5, out6;
   input  [1:0] in1, in2;

   reg [1:0]    out1, out2, out3, out4, out5, out6;

   always @ (in1 or in2)
     begin : sub
        reg [1:0] tmp;
        tmp = 2'b01;
        if (tmp[0])
          out1 = in1;
        else
          out1 = in2;
        tmp = 2'b10;
        if (tmp[1])
          out2 = in1;
        else
          out2 = in2;
        tmp = in1;
        if (tmp)
          begin
             tmp[0] = 1'b0;
             out3 = tmp[0] ? in1 : in2;
          end
        else
          begin
             tmp[1] = 1'b0;
             out3 = tmp[1] ? in2 : in1;
          end
        out4 = tmp;
        out5 = in2 ^ tmp;
        tmp = 2'b00;
        out6[tmp[0]] = in1[0];
        out6[1] = in1[1];
     end

endmodule
