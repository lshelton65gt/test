// Same as state1 but we put a depositSignal on top.out
module top (out, clk, in);
   output out;
   input  clk, in;

   wire    en = 1'b0;

   // This is one flop treated as a constant
   reg    q;
   wire   gclk = clk & en;
   initial q = 0;
   always @(posedge gclk)
       q <= in;

   // This is not treated as a constant because of a depositSignal
   wire clk2 = q | en;
   reg  out;
   initial out = 0;
   always @ (posedge clk2)
     out <= in;

endmodule // sub
