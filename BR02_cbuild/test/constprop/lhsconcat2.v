module top (out1, out2, out3, out4, out5, out6);
   output out1, out2, out3, out4, out5, out6;

   sub S1 (out1, out2, out3, 3'b101);
   sub S2 (out4, out5, out6, 3'b010);

endmodule

module sub (o1, o2, o3, in);
   output o1, o2, o3;
   input [2:0] in;

   assign {o1, o2, o3} = in;

endmodule
