

// xxxx

module      tinyH_vtoc                     // TOP LEVEL
  (
    output wire [(12-1):0]     clk, input wire [(12-1):0] clkin  ); // Clk vector observed OK here.
   
   tinyH_synt
     i_synt
       (
        .clkin (clkin), 
        // Outputs
        .clk     (clk[(12-1):0])  // Clock vector from tinyH_synt below
        //.....
        );
endmodule // tinyH_vtoc

module tinyH_synt
  (
   //...
    output wire [(12-1):0]  clk, input wire [(12-1):0] clkin);
   //...
   
   clkctrl     i_clkctrl
    (
     
     // Outputs
        .clk      (clk[(12-1):0]),     // Clock vector comes from here...
     .clkin (clkin)
     //..
       );
   
   cpuif_tinyH
     i_cpuif
       (                
                        // ...  
                        
                        // Inputs
        .clk_cpu_c      (clk[11])    // Clock for the CPU/PC
                        //...
                        );
    foo
      foo
        (                
        // ...  
                         
                         // Inputs
                         .clk      (clk[0])    // Clock for the CPU/PC
        //...
                         );
endmodule // tinyH_synt


module cpuif_tinyH
    (
    input  wire    clk_cpu_c );// Marked CONSTANT in hierarchy browser.
   avr8L   i_core
     (
        // Inputs
      .clk_c        (clk_cpu_c)
      // ...
       );
endmodule //cpuif_tinyH

module avr8L
   (
    input  wire    clk_c  // Marked CONSTANT in hierarchy browser.
    // ...
   );
   
   avr8L_inst    i_inst(
        .clk_c     (clk_c)
                        
                        );
endmodule //avr8L

module foo
  (
    input  wire    clk  
   );
   reg [31:0]      out;
   
   
   always @(posedge clk) begin
      out <= out+32'd1; 
   end

endmodule

module avr8L_inst
(
     input wire clk_c    // Marked CONSTANT in hierarchy browser.
     //...
);
   reg [31:0] pc; 
   
   //...
   always @(posedge clk_c) begin
        // Program counter manipulations here.
      pc <= pc+32'd1; 
   end

endmodule //avr8L_inst

module  clkctrl (output wire [(12-1):0] clk, input wire [(12-1):0] clkin   
                 );

/* -----\/----- EXCLUDED -----\/-----
   wire clk_0;
   wire clk_11;
 -----/\----- EXCLUDED -----/\----- */
   
/* -----\/----- EXCLUDED -----\/-----
   wire clk_foo;
 -----/\----- EXCLUDED -----/\----- */

/* -----\/----- EXCLUDED -----\/-----
   assign clk_0 = clk_foo;
   assign clk[0] = clk_0;
   assign clk[11] = clk_11;
 -----/\----- EXCLUDED -----/\----- */


   assign clk[0] = clkin[0];

   assign clk[9:1] = 1'b0;

//   assign clk[10] = 1'b0;  // Comment this out to make it fail.
 
   assign clk[11]= clkin[11];
    
   
endmodule
