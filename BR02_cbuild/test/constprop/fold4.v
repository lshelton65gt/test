module top (out, in);
   output out;
   input  in;

   reg 	  a, b, c, out;
   always @ (in)
     begin
	a = in & 0;
	b = ~a;
	if (in)
	  a = 0;
	else
	  a = 1;
	c = a ^ b;
	out = ~c;
     end
   
endmodule // top

