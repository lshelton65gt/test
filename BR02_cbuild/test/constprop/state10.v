module top (out, rst, in);
   output [3:0] out;
   input        rst;
   input [3:0]  in;

   wire         clk = 0;

   reg [3:0]    out;
   always @ (posedge clk or negedge rst)
     if (~rst)
       out <= 4'h5;
     else
       out <= in;

endmodule // top
