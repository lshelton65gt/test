module top (out1, out2, clk, in1, in2, in3, in4);
   output out1, out2;
   input  clk, in1, in2, in3, in4;

   cycle C1 (out1, out2, clk, in1, in2, in3, in4);

endmodule


module cycle (vom, vop, clk, gd, vi_m, vi_p, vp);
    output vom;
    output vop;
    input clk;
    input gd;
    input vi_m;
    input vi_p;
    input vp;
    reg vop,vom;
    always @(clk or vi_p or vi_m)
        if (clk === 0)
            begin
               vop <= 1'b1;
               vom <= 1'b1;
            end
        else if (clk)
            begin
               vop <= vi_p;
               vom <= vi_m;
            end
endmodule

