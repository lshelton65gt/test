module top (out1, out2, clk, cond);
   output out1, out2;
   input  clk, cond;
   reg 	  out1, out2;
   reg 	  a, b;

   initial begin
	a = 1;
	b = 1;
   end

   always @ (cond)
     begin
	a = 0;
	if (cond)
	  b = ~a;
	a = 1;
     end

  always @ (posedge clk)
    begin
       out1 <= a;
       out2 <= b;
    end

endmodule // top

      
