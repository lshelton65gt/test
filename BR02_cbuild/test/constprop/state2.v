// Same as state1 but we use different initial values.
module top (out, clk, in);
   output out;
   input  clk, in;

   reg    en;

   sub S1 (out, clk, in, 1'b0);

endmodule // top

module sub (out, clk, d, en);
   output out;
   input  clk, d, en;

   reg    q;
   wire   gclk = clk & en;
   initial q = 1;
   always @(posedge gclk)
       q <= d;

   wire clk2 = q | en;
   reg  out;
   initial out = 1;
   always @ (posedge clk2)
     out <= d;

endmodule // sub
