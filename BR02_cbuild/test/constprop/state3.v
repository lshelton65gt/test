// Same as state1 but we use async reset in one of the inactive flops.
module top (out, clk, rst, in);
   output out;
   input  clk, rst, in;

   reg    en;

   sub S1 (out, clk, rst, in, 1'b0);

endmodule // top

module sub (out, clk, rst, d, en);
   output out;
   input  clk, rst, d, en;

   reg    q;
   wire   gclk = clk & en;
   wire   trst = rst & en;
   initial q = 0;
   always @(posedge gclk)
     q <= d;

   wire clk2 = q | en;
   reg  out;
   initial out = 0;
   always @ (posedge clk2 or posedge trst)
     if (trst)
       out <= 0;
     else
       out <= d;

endmodule // sub
