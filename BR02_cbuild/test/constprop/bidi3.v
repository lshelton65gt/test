module top (out1, out2, out3, out4);
   output out1, out2, out3, out4;

   // Constant is driven from inside. out1 = 1, out2 = 0
   pad P1 (out2, out1, 1'b1, 1'b1);

   // Constant is driven from outside, out3 = 0, out4 = 1
   pad P2 (out4, out3, 1'b1, 1'b0);
   assign out3 = 1'b0;

endmodule

module pad(out, io, in, en);
   output out;
   inout  io;
   input  in, en;

   assign io = en ? in : 1'bz;
   assign out = ~io;
   
endmodule

