// Same as state1 but we break up the net into many flops for various bits
module top (out, clk, in);
   output [3:0] out;
   input        clk;
   input [3:0]  in;

   reg    en;

   sub S1 (out, clk, 1'b0, in);

endmodule // top

module sub (out, clk, en, d);
   output [3:0] out;
   input  clk, en;
   input [3:0] d;

   reg    q;
   wire   gclk = clk & en;
   initial q = 0;
   always @(posedge gclk)
       q <= d;

   wire clk2 = q | en;
   reg  [3:0] out;
   initial out = 4'b1010;
   always @ (posedge clk2)
     out[0] <= d[3];

   always @ (posedge clk2)
     out[2:1] <= d[1:0];

   always @ (posedge clk2)
     out[3] <= d[2];

endmodule // sub
