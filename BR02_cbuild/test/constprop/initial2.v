// Simple test case where we treat the output of a flop as a constant.
module top (out, clk, in);
   output out;
   input  clk, in;

   wire    en;

   sub1 S1 (out, clk, in, en);
   sub2 S2 (en);
   sub3 S3 (en);

endmodule // top

module sub1 (out, clk, d, en);
   output out;
   input  clk, d, en;

   // This is one flop treated as a constant
   reg    q;
   wire   gclk = clk & en;
   initial q = 0;
   always @(posedge gclk)
       q <= d;

   // This is another flop that is treated as a constant because its
   // clock pin has the above flop in it which is a constant.
   wire clk2 = q | en;
   reg  out;
   initial out = 0;
   always @ (posedge clk2)
     out <= d;

endmodule // sub1

module sub2 (out);
   output out;
   reg    out;

   initial out = 0;

endmodule // sub2

module sub3 (out);
   output out;
   reg    out;

   initial out = 1;

endmodule // sub3

