module top (out, in);
   output [3:0] out;
   input [3:0]  in;

   reg [3:0]    out;
   reg [3:0]    r1;          
   reg [1:0]    x;
   integer      i;

   always @ (in)
     begin
        x = 1;
        r1[0] = 1'b1;
        for (i = 1; i < 4; i = i + 1) begin
           r1[x] = r1[x-1] ^ 1;
           x = x + 1;
        end
     end

   always @ (r1 or in)
     out = r1 & in;

endmodule
