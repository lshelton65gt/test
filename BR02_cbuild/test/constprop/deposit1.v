module top (out, clk, in);
   output out;
   input  clk, in;

   reg    out;
   reg    en;
   initial begin
      out = 0;
      en = 0;
   end
   
   wire dis = ~en;
   
   always @ (posedge clk)
     if (~dis)
       out <= in;

endmodule // top
