// Simple test case where we treat the output of a flop as a constant.
module top (out1, out2, clk, in);
   output out1, out2;
   input  clk, in;

   wire    [1:0] en;

   sub1 S1_1 (out1, clk, in, en[0]);
   sub1 S1_2 (out2, clk, in, en[1]);
   sub2 S2 (en);
   sub3 S3 (en);

endmodule // top

module sub1 (out, clk, d, en);
   output out;
   input  clk, d, en;

   // This is one flop treated as a constant
   reg    out;
   wire   gclk = clk & en;
   initial out = 0;
   always @(posedge gclk)
       out <= d;

endmodule // sub1

module sub2 (out);
   output [1:0] out;
   reg    [1:0] out;

   initial out = 2'b10;

endmodule // sub2

module sub3 (out);
   output [1:0] out;
   reg    [1:0] out;

   initial out = 2'b00;

endmodule // sub3

