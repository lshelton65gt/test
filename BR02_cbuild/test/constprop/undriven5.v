module top (out, in);
   output out;
   input  in;

   wire   r1;
   
   assign out = ~r1 & in;

   sub S1 (r1);

endmodule

module sub (o1);
   output o1;

endmodule // sub
