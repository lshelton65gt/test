module top (out1, out2, out3, out4, in1);
   output out1, out2, out3, out4;
   input  in1;


   assign out1 = ~in1;
   assign {out2, out3, out4} = 3'b101;

endmodule // top

   
