module top (out1, out2, clk, in1, in2, en);
   output out1, out2;
   input  clk, in1, in2, en;

   reg    r1, r2;
   always @ (posedge clk)
     begin
        r1 <= in1;
        r2 <= in2;
     end

   wire d1, d2;
   sub S1 (d1, d2, r1, en);
   sub S2 (d2, d1, r2, en);

   flop F1 (out1, clk, d1);
   flop F2 (out2, clk, d2);
   
`ifdef EVENTSIM
   assign (supply1, supply0) d1 = 1;
   initial begin
      r1 = 0;
      r2 = 0;
   end
`endif

endmodule // top

module sub (b1, b2, in, en);
   output b1, b2;
   input  in, en;

   assign b1 = en ? in : 1'bz;
   assign b2 = ~en ? ~in : 1'bz;

endmodule // sub

module flop (q, clk, d);
   output q;
   input  clk, d;
   reg    q;

   initial q = 0;
   always @ (posedge clk)
     q <= d;

endmodule // flop
