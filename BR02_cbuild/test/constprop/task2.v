module top (out, in);
   output [7:0] out;
   input  [7:0] in;

   task t1;
      output [7:0] o;
      input [7:0] i;
      reg len;
      begin
         len = 1'b1;
         if (len == 1)
           o = i;
      end
   endtask

   reg [7:0] out;
   always @ (in)
     t1(out, in);

endmodule
