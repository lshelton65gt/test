// This test has different combinations of flops that could never
// change. This is because their initial value matches all the
// values that could be stored as they run.
//
// The test contains both flops that can be propagated and some that
// can't be propagated.
module top (out1, out2, out3, out4, clk, rst);
   output [1:0] out1, out2, out3, out4;
   input  clk, rst;
   reg [1:0] out1, out2, out3, out4;

   // No initial value so we choose 0
   always @ (posedge clk or posedge rst)
     begin
        if (rst)
          out1 <= 2'b00;
        else
          out1 <= 2'b00;
     end

   // Initial value matches flopped value
   initial out2 = 2'b10;
   always @ (posedge clk or posedge rst)
     begin
        if (rst)
          out2 <= 2'b10;
        else
          out2 <= 2'b10;
     end

   // Initial value does not match flopped value
   initial out3 = 2'b01;
   always @ (posedge clk or posedge rst)
     begin
        if (rst)
          out3 <= 2'b11;
        else
          out3 <= 2'b11;
     end

   // No initial value but the flopped value is not zero
   always @ (posedge clk or posedge rst)
     begin
        if (rst)
          out4 <= 2'b01;
        else
          out4 <= 2'b01;
     end

endmodule
