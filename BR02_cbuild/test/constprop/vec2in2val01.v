module top (in1, in2, in3, out1, out2, out3);
   input [1:0] in1, in2, in3;
   output [1:0] out1, out2, out3;   

   wire [5:0] tmp;

   assign     out1 = tmp[1:0];
   assign     out2 = tmp[3:2];
   assign     out3 = tmp[5:4];  
   
   foo f (in1, in2, in3, tmp);
   
endmodule // foo


module foo (in1, in2, in3, out1);
   input [1:0] in1, in2, in3;
   output [5:0] out1;

// Mimic the tie that happens from the directives
`ifdef EVENTSIM
   assign      (supply1, supply0) in2 = 2'b01;
`endif
   
   assign       out1 = {in3, in2, in1};
endmodule // foo

