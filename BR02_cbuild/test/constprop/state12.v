module bug (in, crap_in, clk, out1, out2);
   input in, crap_in;
   input clk;
   output out1, out2;


   reg    crap_clk;
   initial crap_clk = 0;
   
   flop flop1 (in, crap_in, clk, crap_clk, out1);
   flop flop2 (in, crap_in, clk, 1'b0, out2);

endmodule

module flop (in, crap_in, clk, crap_clk, out);
   input in, crap_in, clk, crap_clk;
   output out;
   reg    out;
   
   initial out = 1;
   always @(posedge clk or posedge crap_clk)
     if (crap_clk)
       out <= crap_in;
     else
       out <= in;
endmodule
