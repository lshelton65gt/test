module top (out1, out2, out3, out4, out5, out6, clk, in1, in2, in3, en);
   output out1, out2, out3, out4, out5, out6, clk;
   input  in1, in2, in3, en;

   reg    out1;
   initial out1 = 0;
   always @ (posedge clk)
     out1 <= in1;

   sub0 S0 (clk, 1'b1);
   sub1 S1 (out2, clk, in2);
   sub2 S2 (out4, out3_, en, in3, 1'b1);

   // I don't think this testcase is trying to test port coercion, but
   // If we don't have this inverter in the path of out3, then it gets
   // coerced to an inout, and that produces Xs in the Aldec testdriver
   // simulation, which propagate poorly through the assign to 'out' in
   // sub2.
   assign  out3 = out3_;

   reg    r1;
   initial r1 = 0;
   assign out5 = r1;
   sub2 S3 (out6, out5, 1'b1, 1'b1, 1'b1);

endmodule

module sub0(out, en);
   output out;
   input  en;

endmodule

module sub1 (q, clk, d);
   output q;
   input  clk, d;

   reg    q;
   initial q = 0;
   always @ (posedge clk)
     q <= d;

   wire   undriven;
   wire   dummy;
   sub2 S2 (dummy, clk, 1'b1, 1'b0, undriven);

endmodule

module sub2 (out, io, en, in, tn);
   output out;
   inout io;
   input en, in, tn;

   // avoid mismatches with aldec due to propagation of Z through io's fanout
   pulldown(io);

   wire  enn, tn_enn;
   assign enn = ~en;
   assign tn_enn = (~(tn & enn));
   assign io = (!tn_enn) ? in : 1'bz;
   assign out = (io === 1'b1) ? 1'b0 : 1'b1;

endmodule
