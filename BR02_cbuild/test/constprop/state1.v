// Simple test case where we treat the output of a flop as a constant.
module top (out, clk, in);
   output out;
   input  clk, in;

   reg    en;

   sub S1 (out, clk, in, 1'b0);

endmodule // top

module sub (out, clk, d, en);
   output out;
   input  clk, d, en;

   // This is one flop treated as a constant
   reg    q;
   wire   gclk = clk & en;
   initial q = 0;
   always @(posedge gclk)
       q <= d;

   // This is another flop that is treated as a constant because its
   // clock pin has the above flop in it which is a constant.
   wire clk2 = q | en;
   reg  out;
   initial out = 0;
   always @ (posedge clk2)
     out <= d;

endmodule // sub
