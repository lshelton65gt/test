module top (out, clk, in, tout, tclk, trst, tin);
   output out;
   input  clk, in;
   output tout;
   input  tclk, trst, tin;

`ifdef EVENTSIM
   sub S1 (out, clk, in, tout, 1'b0, 1'b0, 1'b0);
`else
   sub S1 (out, clk, in, tout, tclk, trst, tin);
`endif

endmodule // top

module sub (out, clk, in, tout, tclk, trst, tin);
   output out;
   input  clk, in;
   output tout;
   input  tclk, trst, tin;

   reg    reset;
   wire   gtclk = tclk & ~trst;
   initial reset = 0;
   always @ (posedge gtclk)
     reset <= trst;

   reg    tout;
   initial tout = 0;
   always @ (posedge gtclk or posedge reset)
     if (reset)
       tout <= 1'b0;
     else
       tout <= tin;

   reg    out;
   initial out = 0;
   always @ (posedge clk)
     out <= in;

endmodule // top
