// This test exercises the RELocalConstProp code. It propagates
// constants within a block when possible.
module top (out1, out2, out3, out4, out5, out6, out7, out8, out9, in1, in2);
   output out1, out2, out3, out4, out5, out6, out7, out8, out9;
   input  in1, in2;

   reg    out1, out2, out3, out4, out5, out6, out7, out8, out9;

   always @ (in1 or in2)
     begin : sub
        reg tmp;

        // Test if condition being optimized to 0
        tmp = 1'b0;
        if (tmp)
          out1 = in1;
        else
          out1 = in2;

        // Test if condition being optimized to 1
        tmp = 1'b1;
        if (tmp)
          out2 = in1;
        else
          out2 = in2;
        
        // Test tmp overriden inside nesting and getting optimized
        tmp = in1;
        if (tmp)
          begin
             tmp = 1'b0;
             out3 = tmp ? in1 : in2;
          end
        else
          begin
             tmp = 1'b0;
             out3 = tmp ? in2 : in1;
          end

        // At this point tmp should be a constant so out4 and out5
        // should get optimized by the above if/then/else.
        out4 = tmp;
        out5 = in2 ^ tmp;

        // Test to make sure if we only write the constant in the then that
        // it does not get optimized
        tmp = in2;
        if (in1)
          tmp = 1'b0;
        out6 = tmp | in2;

        // Test to make sure if we only write the constant in the else that
        // it does not get optimized
        if (in2)
          ;
        else
          tmp = 1'b1;
        out7 = tmp & in1;

        // In this case we should optimize because the then and preexisting
        // values of tmp are the same
        tmp = 1'b1;
        if (in2)
          tmp = 1'b1;
        out8 = tmp & in1;

        // In this case we should not optimize because the else and
        // preexisting values of tmp are not the same
        tmp = 1'b0;
        if (in2)
          ;
        else
          tmp = 1'b1;
        out9 = tmp & in1;
     end

endmodule

