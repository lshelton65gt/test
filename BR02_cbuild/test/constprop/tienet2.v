// Test tie net on a top module input port
module top (out, clk, in, en);
`ifdef EVENTSIM
   output out;
   input  clk, in;
   output en;
   wire   en = 1'b1;
`else
   output out;
   input  clk, in, en;
`endif

   sub S1 (out, clk, in, en);

endmodule // top

module sub (out, clk, in, en);
   output out;
   input  clk, in, en;

   reg    out;
   initial begin
      out = 0;
   end

   wire dis = ~en;
   always @ (posedge clk)
     if (~dis)
       out <= in;

endmodule // top
