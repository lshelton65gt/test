module top(in, cond, out, out2);
  input in, cond;
  output out, out2;
  reg    out2, tmp;

  always @(in) begin
    tmp = in;
    out2 = tmp;
    tmp = 1'b0;
  end
  assign out = ~tmp;

endmodule
