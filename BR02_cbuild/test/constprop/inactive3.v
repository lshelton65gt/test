module top (out, clk, in, rst, tout, tclk, trst, tin, dout);
   output out;
   input  clk, in, rst;
   output tout;
   input  tclk, trst, tin;
   output dout;

`ifdef EVENTSIM
   sub S1 (out, clk, in, rst, tout, 1'b0, 1'b0, 1'b0, dout);
`else
   sub S1 (out, clk, in, rst, tout, tclk, trst, tin, dout);
`endif

endmodule // top


module sub (out, clk, in, rst, tout, tclk, trst, tin, dout);
   output out;
   input  clk, in, rst;
   output tout;
   input  tclk, trst, tin;
   output dout;

   reg    out;
   reg    reset;
   initial begin
      out = 0;
      reset = 0;
   end
   always @ (posedge clk or posedge rst or posedge reset)
     if (reset)
       out <= tin;
     else if (rst)
       out <= 1'b0;
     else
       out <= in;

   wire   gtclk = tclk & ~trst;
   always @ (posedge gtclk)
     reset <= trst;

   reg    tout;
   initial tout = 0;
   always @ (posedge gtclk or posedge reset)
     if (reset)
       tout <= 1'b0;
     else
       tout <= tin;

   assign dout = out & in;

endmodule // top
