module top (out, clk, d);
   output out;
   input  clk, d;

   reg    out;
   initial out = 0;
   always @ (clk or d)
     if (clk != 0)
       if (clk != 0)
         if (clk != 0)
           if (clk != 0)
             out <= d;

endmodule
