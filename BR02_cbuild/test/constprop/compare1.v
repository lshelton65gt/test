// This case found a bug because we didn't handle the initial block in
// the code that detects if we have a valid LHS. Because of this it
// failed to optimize.

module top (out, clk, in);
   output out;
   input  clk;
   input [1:0] in;

   wire        rst = 0;
   reg [1:0]   state;
   initial state = 0;
   always @ (posedge clk or negedge rst)
     if (~rst)
       state = 2'b0;
     else
       state = in;

   assign      out = (state == 2'b01) ? 1'b1 : 1'b0;

endmodule // top
