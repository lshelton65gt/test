module bug(outa, outb, ina1, ina2, inb1, inb2);
   output outa, outb;
   input  ina1, ina2, inb1, inb2;

   assign outa = sel(1'b0, ina1, ina2);
   assign outb = sel(1'b1, ina1, ina2);
   
   function sel;
      input select;
      input in1;
      input in2;

      begin
	 if (select)
	   sel = in2;
	 else
	   sel = in1;
      end
   endfunction
endmodule
