// Another multi assign case but we break it up twice. This exercises the
// recursive break up of a constant.
module top (out, clk, in, en);
   output out;
   input  clk, in;
   input [3:0] en;

   sub S1 (out, clk, in, en);

endmodule // top

module sub (out, clk, in, en);
   output out;
   input  clk, in;
   input [3:0] en;

   reg    out;
   initial begin
      out = 0;
   end

`ifdef EVENTSIM
   wire [3:0] dis = 4'b1010;
`else
    reg [3:0] dis;
   always @ (en)
     begin
        begin : one
           dis[0] = ~en[1];
           dis[1] = ~en[0];
        end
        begin : two
           dis[2] = ~en[3];
           dis[3] = ~en[2];
        end
     end
`endif
   
   always @ (posedge clk)
     if (dis == 4'b1010)
       out <= in;

endmodule // top
