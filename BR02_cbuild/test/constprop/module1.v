module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   sub S1 (out1, clk, in1, 1'b0, 1'b1);
   sub S2 (out2, clk, in2, 1'b0, 1'b0);

endmodule // top

module sub (q, clk, d, set, rst);
   output q;
   input  clk, d, set, rst;

   reg 	  q;
   always @ (posedge clk)
     if (set)
       q <= 1'b1;
     else if (rst)
       q <= 1'b0;
     else
       q <= d;

endmodule // sub

   
