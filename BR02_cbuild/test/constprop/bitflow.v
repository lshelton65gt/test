module bitflow(p, q, r, x);
  input p, r;
  input [1:0] q;
  output [1:0] x;
  reg [3:0]         v;
  reg [1:0]         x;

  always @(p or q or r) begin
    v[0] = p;
    v[2:1] = q;
    v[3] = r;
    x = v[3:2];
  end
endmodule
