// Test tie net on a top module clock input port
module top (out, clk, in);
`ifdef EVENTSIM
   output out;
   input  in;
   output clk;
   wire    clk = 1'b0;
`else
   output out;
   input  clk, in;
`endif

   sub S1 (out, clk, in);

endmodule // top

module sub (out, clk, in);
   output out;
   input  clk, in;

   reg    out;
   initial begin
      out = 0;
   end

   always @ (posedge clk)
     out <= in;

endmodule // top
