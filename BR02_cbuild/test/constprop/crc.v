module hec_crc8_2(hec_data_out, data_in);

	input	[31:0]		data_in;
	output	[7:0]		hec_data_out;

	reg			feed;
	reg	[7:0]		crc_data;
	integer			i;

	assign #(2) hec_data_out = crc_data;

	always @(data_in) begin
	  crc_data = 0;
	  for (i = 31; (i >= 0); i = (i - 1)) begin
	    feed = (crc_data[7] ^ data_in[i]);
	    crc_data[7] = crc_data[6];
	    crc_data[6] = crc_data[5];
	    crc_data[5] = crc_data[4];
	    crc_data[4] = crc_data[3];
	    crc_data[3] = crc_data[2];
	    crc_data[2] = (feed ^ crc_data[1]);
	    crc_data[1] = (feed ^ crc_data[0]);
	    crc_data[0] = feed;
	  end
	end
endmodule
