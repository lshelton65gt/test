/*
 Make sure that nets def'd by tasks are not seen as constants from
 earlier drivers.  This is a variant on the testcase from
 bug4828, that didn't fail prior to bug4828 being fixed, but we want
 to make sure it continues not failing.
*/


module bug4828(out, in, ena, clk);
  output [4:0] out;
  input        clk, ena;
  input [4:0] in;
  reg [4:0]   data;
  reg [4:0]   out;

  initial out = 5'b0;           //  help sim gold files match

  reg [5:0]     tmp;

  task def_tmp;
    if (ena)    // def, but to not kill 'tmp'
      tmp = in;
  endtask

  always @(negedge clk) begin
    tmp = 1'b0;
    def_tmp;
    out = tmp;
  end

endmodule
