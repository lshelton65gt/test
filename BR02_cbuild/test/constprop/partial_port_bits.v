module top(clk, in1, in2, out1, out2, out3, out4, out5);
   input        clk;
   input [7:0]  in1, in2;
   output [8:0] out1, out2, out3, out4, out5;
   reg [8:0]    out1;

   wire [8:0]   temp;
   assign       temp[8] = 1'b0;  // found by constant propagation
   assign       temp[7:0] = in1 ^ in2;  // should be rescoped into always block

   always @(posedge clk) begin
      out1 <= ~temp;
   end

   sub S1 (temp, clk, out2);
`ifdef EVENTSIM
   sub S2 ({2'b0, in2[3:0],in1[2:0]}, clk, out3);
`else
   sub S2 ({in2[3:0],in1[2:0]}, clk, out3);
`endif
   sub S3 ({in2[2:0], in1[2:0], 3'b0}, clk, out4);

   sub S4 ({in2[2:0], 3'b1, in1[2:0]}, clk, out5);
   
endmodule

module sub(d, clk, q);
   input [8:0]  d;
   input        clk;
   output [8:0] q;
   reg [8:0]    q;

   always @ (posedge clk)
     q <= d;

endmodule
