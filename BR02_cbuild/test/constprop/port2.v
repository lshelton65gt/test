// This test found issues with constant propagating through ports
// where the constant was spread across multiple drivers that
// form a concat

module top (out1, out2, clk, in1, in2);
   output [3:0] out1, out2;
   input        clk;
   input [3:0]  in1, in2;

   wire [3:0]   r1;
   sub1 S1 (r1, clk, 1'b1, in1);
   assign       out1 = ~r1;

   wire [3:0]   r2;
   sub2 S2 (r2, clk, 1'b1, in2);
   assign       out2 = ~r2;

endmodule

module sub1 (q, clk, rst, d);
   output [3:0] q;
   input        clk, rst;
   input [3:0]  d;

   reg [3:0]    q;

   always @ (posedge clk or posedge rst)
     if (rst)
       q[1:0] <= 2'b01;
     else
       q[1:0] <= d[1:0];

   always @ (posedge clk or posedge rst)
     if (rst)
       q[3:2] <= 2'b10;
     else
       q[3:2] <= d[3:2];

endmodule

module sub2 (q, clk, rst, d);
   output [3:0] q;
   input        clk, rst;
   input [3:0]  d;

   reg [3:0]    q;

   always @ (posedge clk or posedge rst)
     if (rst)
       q[0] <= 1'b1;
     else
       q[0] <= d[0];

   always @ (posedge clk or posedge rst)
     if (rst)
       q[2:1] <= 2'b10;
     else
       q[2:1] <= d[2:1];

   always @ (posedge clk or posedge rst)
     if (rst)
       q[3] <= 1'b0;
     else
       q[3] <= d[3];

endmodule
