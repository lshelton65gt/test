module bug(clk, a, b, z);
   input clk;
   input [14:0] a, b;
   output [2:0] z;

   reg [2:0] 	y, z;

   always @(posedge clk)
     z <= y;

   // Testcase that needs thorough constant propagation to recognize
   // that 'y' is not a latch.
   
   always @(a or b) begin : foo
      integer index;
      integer offset;
      integer i;

      index = 0;
      offset = 0;
      y[0] = 1'b1;

      for (i = 0; i < 15; i = i + 1) begin
	 if (offset == 5) begin
	    offset = 0;
	    index = index + 1;
	    y[index] = 1'b1;
	 end
	 
	 if (a[i] != b[i])
	   y[index] = 1'b0;

	 offset = offset + 1;
      end
   end
endmodule
