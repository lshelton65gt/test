// Same as tienet7.v but we reverse the bits just to make sure we get
// it right.
module top (out, clk, in, en);
   output out;
   input  clk, in;
   input [1:0] en;

   sub S1 (out, clk, in, en);

endmodule // top

module sub (out, clk, in, en);
   output out;
   input  clk, in;
   input [1:0] en;

   reg    out;
   initial begin
      out = 0;
   end

`ifdef EVENTSIM
   wire [1:0] dis = 2'b10;
`else
   reg [1:0] dis;
   always @ (en)
     dis[0] = ~en[1];
   always @ (en)
     dis[1] = ~en[0];
`endif
   
   always @ (posedge clk)
     if (dis == 2'b10)
       out <= in;

endmodule // top
