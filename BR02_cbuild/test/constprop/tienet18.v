// Test tie net on a sub module clock input port
module top (out1, out2, clk, in1, in2);
   output [3:0] out1;
   output [1:0] out2;
   input        clk;
   input [3:0]  in1;
   input [1:0]  in2;

`ifdef EVENTSIM
   sub S1 (out1, 1'b0, in1);
   sub #(2) S2 (out2, 1'b0, in2);
`else
   sub S1 (out1, clk, in1);
   sub #(2) S2 (out2, clk, in2);
`endif

endmodule // top

module sub (out, clk, in);
   parameter size = 4;
   output [size-1:0] out;
   input             clk;
   input [size-1:0]  in;

   reg [size-1:0]    out;
   initial begin
      out = 0;
   end

   always @ (posedge clk)
     out <= in;

endmodule // top
