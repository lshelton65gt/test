// This test exercises the RELocalConstProp code. It propagates
// constants within a block when possible. This test case
// deals with case statements

module top (out1, out2, out3, out4, out5, out6, out7, out8, out9, in1, in2);
   output out1, out2, out3, out4, out5, out6, out7, out8, out9;
   input  in1, in2;

   reg    out1, out2, out3, out4, out5, out6, out7, out8, out9;

   always @ (in1 or in2)
     begin : sub
        reg tmp, dummy;

        // Test case condition being optimized to 0
        tmp = 1'b0;
        case (tmp)
          1'b1:
            out1 = in1;
          1'b0:
            out1 = in2;
        endcase

        // Test case condition being optimized to 1
        tmp = 1'b1;
        case (tmp)
          1'b0:
            out2 = in1;
          1'b1:
            out2 = in2;
        endcase
        
        // Test tmp overriden inside nesting and getting optimized
        tmp = in1;
        case (tmp)
          1'b0:
            begin
               tmp = 1'b0;
               out3 = tmp ? in1 : in2;
            end
          1'b1:
            begin
               tmp = 1'b0;
               out3 = tmp ? in2 : in1;
            end
        endcase

        // At this point tmp should be a constant so out4 and out5
        // should get optimized by the above if/then/else.
        out4 = tmp;
        out5 = in2 ^ tmp;

        // Test to make sure if we only write the constant in one case item
        // it does not get optimized
        tmp = in2;
        case (in1)
          1'b0:
            tmp = 1'b0;
        endcase
        out6 = tmp | in2;

        // Test to make sure if we only write the constant in one case item
        // it does not get optimized
        case (in2)
          1'b1:
            tmp = 1'b1;
          1'b0:
            dummy = 1'b0;
        endcase
        out7 = tmp & in1;

        // In this case we should optimize because the case item
        // and preexisting values of tmp are the same
        tmp = 1'b1;
        case (in2)
          1'b1:
            tmp = 1'b1;
        endcase
        out8 = tmp & in1;

        // In this case we should not optimize because the case item and
        // preexisting values of tmp are not the same
        tmp = 1'b0;
        case (in2)
          1'b1:
            tmp = 1'b1;
        endcase
        out9 = tmp & in1;
     end

endmodule


