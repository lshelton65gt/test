// Test tie net on an undriven scalar net
module top (out, clk, in);
   output out;
   input  clk, in;

   reg    out;
   initial begin
      out = 0;
   end

`ifdef EVENTSIM
   wire en = 1;
`else
   reg  en;
`endif
   
   wire dis = ~en;
   
   always @ (posedge clk)
     if (~dis)
       out <= in;

endmodule // top
