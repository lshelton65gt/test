module dut(i, o);
   input i;
   output o;

   wire   ena1, ena2, ena_a, ena_b, i_ena, o_ena;
   wire   t1, t2, t3, t4, t5;

`ifdef EVENTSIM
   assign ena1 = 1'b1;
   assign ena2 = 1'b1;
   assign ena_a = 1'b1;
   assign ena_b = 1'b1;
   assign i_ena = 1'b1;
   assign o_ena = 1'b1;
`endif

   assign t1 = (ena1)  ? i  : 1'b0;
   assign t2 = (ena2)  ? t1 : 1'b0;
   assign t3 = (ena_a)  ? t2 : 1'b0;
   assign t4 = (ena_b) ? t3 : 1'b0;
   assign t5 = (i_ena) ? t4 : 1'b0;
   assign o  = (o_ena) ? t5 : 1'b0;
endmodule
