module top (out1, out2, raddr, ren, clk, waddr, wen, wdata);
   output [7:0] out1, out2;
   input [3:0] 	raddr;
   input 	ren, clk;
   input [3:0] 	waddr;
   input 	wen;
   input [7:0] 	wdata;

   sub S1 (out1, raddr, 1'b1, clk, waddr, wen, wdata);
   sub S2 (out2, raddr, 1'b1, clk, waddr, 1'b1, wdata);

endmodule // top

module sub (out, raddr, ren, clk, waddr, wen, wdata);
   output [7:0] out;
   input [3:0] 	raddr;
   input 	ren, clk;
   input [3:0] 	waddr;
   input 	wen;
   input [7:0] 	wdata;

   reg [7:0] 	mem [15:0];
   
   assign 	out = ren ? mem[raddr] : 7'b0;

   always @ (posedge clk)
     if (wen)
       mem[waddr] <= wdata;

`ifdef EVENTSIM
   integer 	i;
   initial begin
      for (i = 0; i < 16; i = i + 1)
	mem[i] = 0;
   end
`endif

endmodule // top
