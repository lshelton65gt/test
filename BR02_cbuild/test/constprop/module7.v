module top (out1, out2, clk, in1, in2, en1, en2);
   output [3:0] out1, out2;
   input 	clk, en1, en2;
   input [3:0] 	in1, in2;

   reg [3:0] 	r1, r2;
   always @ (posedge clk)
     begin
	r1 <= in1;
	r2 <= in2;
     end

   sub S1 (out1, r1[1:0], r1[3:2], en1, 1'b1);
   sub S2 (out2, r2[1:0], r2[3:2], en2, 1'b1);

   assign out1[1:0] = ~en1 ? 2'b0 : 2'bz;
   assign out2[1:0] = ~en2 ? 2'b0 : 2'bz;

endmodule // top

module sub (out, in1, in2, en1, en2);
   output [3:0] out;
   input [1:0] 	in1, in2;
   input 	en1, en2;

   assign 	out[1:0] = en1 ? in1 : 2'bz;
   assign 	out[3:2] = en2 ? in2 : 2'bz;

endmodule // sub
