module top (out1, out2, clk, dis, test, in1, in2);
   output [2:0] out1, out2;
   input        clk;
   input [2:0]  in1, in2;
`ifdef EVENTSIM
   output       dis;
   wire         dis = 1'b1;
   output [2:0] test;
   wire [2:0]   test = 3'b0;
`else
   input        dis;
   input [2:0]  test;
`endif

   wire [3:0]   gtest;
   sub1 S1 (gtest[2:0], test, dis, 3'h3);

   wire         tclk, trst;
   sub2 S2 (tclk, trst, gtest[2:0]);

   reg [2:0]    out1, out2;
   initial out1 = 3'b0;
   always @ (posedge clk)
     out1 <= in1;
   initial out2 = 3'b0;
   always @ (posedge tclk or posedge trst)
     begin
        if (trst)
          out2 <= 4'b0;
        else
          out2 <= in2;
     end

endmodule

module sub1 (gtest, test, dis, other);
   output [2:0] gtest;
   input [2:0]  test;
   input        dis;
   input [2:0]  other;

   assign       gtest = dis ? test : other;

endmodule

module sub2 (tclk, trst, test);
   output tclk, trst;
   input  [2:0] test;

   assign tclk = (test == 3'h1) || (test == 3'h5);
   assign trst = (test == 3'h7);

endmodule
