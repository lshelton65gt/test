// A simple case where the output of a case statement is a constant
// with a default.

module top (out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   reg    r1, r2;
   always @ (posedge clk)
     begin
        r1 <= in1;
        r2 <= in2;
     end

   wire [1:0] sel = 2'b00;

   reg r3;
   always @ (in1 or in2 or sel)
     case (sel) 
       2'b00: begin
          r3 = 1'b1;
       end

       2'b01: begin
          r3 = 1'b0;
       end

       2'b10: begin
          r3 = r1;
       end

       default: begin
          r3 = r2;

       end
     endcase // case(sel)

   assign out = ~r3;

endmodule // top


       
