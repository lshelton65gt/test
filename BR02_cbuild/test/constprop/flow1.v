module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   reg 	  r1, r2;
   always @ (posedge clk)
     begin
	r1 <= in1;
	r2 <= in2;
     end

   wire c1, c2;
   sub S1 (c1, c2, r1, r2, 1'b0, 1'b1);

   assign out1 = c1 ^ c2;
   assign out2 = c1 & c2;

endmodule // top

module sub (x, y, a, b, c, d);
   output x, y;
   input  a, b, c, d;

   reg   t1, t2, x, y;
   always @ (a or b or c or d)
     begin
	t1 = c;
	if (a)
	  x = t1;
	t1 = d;
	if (b)
	  x = t1;
	t1 = c | d;
	t2 = c ^ d;
	case ({a, b})
	  2'b00:
	    y = t1;
	  2'b01:
	    y = t2;
	  2'b10:
	    y = t1;
	  2'b11:
	    y = t2;
	endcase // case({a, b})
     end // always @ (a or b or c or d)

endmodule // sub
