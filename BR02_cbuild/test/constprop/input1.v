module top (out, in1, in2);
   output out;
   input  in1, in2;

   sub S1 (out, in1, in2, 1'b1);

endmodule // top

module sub (y, a, b, c);
   output y;
   input  a, b, c;

   assign c = 1'b1;
   assign y = a & b & c;

endmodule // sub
