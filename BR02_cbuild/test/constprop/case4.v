// A simple case where the output of a case statement is a constant
// with a default.

module top (out, clk, in1, in2);
   output out;
   input  clk, in1, in2;

   reg    r1, r2;
   always @ (posedge clk)
     begin
        r1 <= in1;
        r2 <= in2;
     end

   wire [127:0] sel = 128'h0;

   reg r3;
   always @ (in1 or in2 or sel)
     case (sel) 
       128'h0: begin
          r3 = 1'b1;
       end

       128'h1: begin
          r3 = 1'b0;
       end

       default: begin
          r3 = r2;

       end

       128'h2: begin
          r3 = r1;
       end

     endcase // case(sel)

   assign out = ~r3;

endmodule // top


       
