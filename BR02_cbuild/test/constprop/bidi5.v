module top(z, a, e);
   output [1:0] z;
   input [1:0]  a, e;

   assign       z[0] = e[0] ? a[0] : 1'bz;
   assign       z[1] = e[1] ? a[1] : 1'bz;
   assign       z[0] = ~e[0] ? ~a[1] : 1'bz;
   assign       z[1] = ~e[1] ? ~a[0] : 1'bz;

   ppull0 u3(z[0]);
   ppull1 u4(z[1]);
endmodule

module ppull1(z);
   output z;
   tri1   z;
endmodule // pull1

module ppull0(z);
   output z;
   tri0   z;
endmodule // pull1

