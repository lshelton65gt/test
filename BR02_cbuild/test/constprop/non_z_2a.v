module tc_io ( test_enable, test_select, scan_mode, xphy_rd_a, phy_emu_rd_a, SI, SO, xphy_td_b, emu_phy_td_b);

   input test_enable;
   input [2:0] test_select;
   output scan_mode;
   inout [3:0] xphy_rd_a;
   output [3:0] phy_emu_rd_a;
   input [67:64] SO;
   output [69:68] SI;
   inout [3:0] xphy_td_b;
   input [3:0] emu_phy_td_b;


   wire scan_oeL;

   wire kscan_mode;
   wire nkscan_mode;
   wire [2:0] ts ;
   wire te;
 
   wire scan_ie;

   assign scan_mode         = nkscan_mode | kscan_mode;
   assign nkscan_mode       = (te & (ts == 3'b000)) ? 1'b1 : 1'b0;
   assign kscan_mode        = (te & (ts == 3'b001)) ? 1'b1 : 1'b0;

   assign scan_oeL = ~scan_mode;
   assign scan_ie  =  scan_mode;

   BD33LV_12_PD_A  test_select_0_PAD (.PAD(test_select[0]),        .DOUT(ts[0]),   .DIN(1'b0),     .OEN(1'b1));
   BD33LV_12_PD_A  test_select_1_PAD (.PAD(test_select[1]),        .DOUT(ts[1]),   .DIN(1'b0),     .OEN(1'b1));
   BD33LV_12_PD_A  test_select_2_PAD (.PAD(test_select[2]),        .DOUT(ts[2]),   .DIN(1'b0),     .OEN(1'b1));

   BD33LV_12_PD_A  test_enable_PAD (.PAD(test_enable),     .DOUT(te),      .DIN(1'b0),     .OEN(1'b1));

   TCM13_H3F_MFIO_GP xphy_rd_a_0_PAD (.PAD(xphy_rd_a[0]), .DOUT(phy_emu_rd_a[0]), .DIN(SO[64]), .OEN(scan_oeL));
   TCM13_H3F_MFIO_GP xphy_rd_a_1_PAD (.PAD(xphy_rd_a[1]), .DOUT(phy_emu_rd_a[1]), .DIN(SO[65]), .OEN(scan_oeL));
   TCM13_H3F_MFIO_GP xphy_rd_a_2_PAD (.PAD(xphy_rd_a[2]), .DOUT(phy_emu_rd_a[2]), .DIN(SO[66]), .OEN(scan_oeL));
   TCM13_H3F_MFIO_GP xphy_rd_a_3_PAD (.PAD(xphy_rd_a[3]), .DOUT(phy_emu_rd_a[3]), .DIN(SO[67]), .OEN(scan_oeL));


   TCM13_H3F_MFIO_GP xphy_td_b_0_PAD (.PAD(xphy_td_b[0]), .DOUT(SI[68]), .DIN(emu_phy_td_b[0]),  .OEN(scan_ie));
   TCM13_H3F_MFIO_GP xphy_td_b_1_PAD (.PAD(xphy_td_b[1]), .DOUT(SI[69]), .DIN(emu_phy_td_b[1]),  .OEN(scan_ie));

   TCM13_H3F_MFIO_GP xphy_td_b_2_PAD (.PAD(xphy_td_b[2]), .DIN(emu_phy_td_b[2]),  .OEN(1'b0));
   TCM13_H3F_MFIO_GP xphy_td_b_3_PAD (.PAD(xphy_td_b[3]), .DIN(emu_phy_td_b[3]),  .OEN(1'b0));

endmodule


module tb ( clk, din, dout, emu_phy_td_b, scan_mode, phy_emu_rd_a);
input clk, din;
input [3:0] emu_phy_td_b;
output dout;
output scan_mode;
output [3:0] phy_emu_rd_a;

wire [3:0] xphy_rd_a;
wire [3:0] xphy_td_b;
wire [67:64] SO;
wire [69:68] SI;
assign xphy_rd_a = xphy_td_b;

reg [2:0] test_select;
reg test_enable;

   reg dout;
   initial begin
      test_select = 3'b000;
      test_enable = 0;   
   end

   always @(posedge clk)
   begin
            dout <= din;
   end


   tc_io tc_io ( .test_enable(test_enable), .test_select(test_select), .scan_mode(scan_mode), 
                 .xphy_rd_a(xphy_rd_a), .phy_emu_rd_a(phy_emu_rd_a), .SO(SO), .SI(SI),
                 .xphy_td_b(xphy_td_b), .emu_phy_td_b(emu_phy_td_b));

endmodule


module TCM13_H3F_MFIO_GP ( DIN, OEN, DOUT, PAD );
	input	DIN;
	input	OEN;
	output	DOUT;
        inout   PAD;



tcm13_mfio_gp_out i1 ( .in (DIN), .oen (OEN), .pad (PAD) );
tcm13_mfio_gp_in  i2 ( .out (DOUT), .pad (PAD) );


endmodule


module tcm13_mfio_gp_out ( in, oen, pad );
	input in;
	input oen;
	inout pad;


	bufif0 ( pad, in, oen );

endmodule


module tcm13_mfio_gp_in ( out, pad );
    input pad;
	output out;


	buf ( out, pad );

endmodule

module LVIOOUT_PD (
	din,
	oen,
	pad
);

input  din;
input  oen;
inout  pad;
	
wire    pad_driver;
tri0   pad = pad_driver;
	
	bufif0 ( pad_driver, din, oen );

endmodule



module LVIOIN (
	pad,
	dout
);

inout  pad;
output dout;
			
	buf ( dout, pad );
			
endmodule



module BD33LV_12_PD_A (PAD, DIN, DOUT, OEN );
   inout  PAD;
   input  DIN;
   output DOUT;
   input  OEN;
   
   LVIOOUT_PD LVIOOUT_PD  ( 
			    .din     ( DIN ),
			    .oen     ( OEN ),
			    .pad     ( PAD )
			    );
   
   LVIOIN LVIOIN (
		  .pad     ( PAD ),
		  .dout    ( DOUT )
		  );
   
endmodule

