module top (out, in);
   output out;
   input  in;

   wire   clk = 0;
   wire   rst = 0;

   wire   reg1;
   regs R1 (reg1, clk, rst, in);
   sub S1 (out, reg1);

endmodule // top

module regs (q, clk, rst, d);
   output q;
   input  clk, rst, d;

   flop F1 (q, clk, rst, d);

endmodule // regs

module flop(q, clk, rst, d);
   output q;
   input  clk, rst, d;

   reg    q;
   initial q = 0;
   always @ (posedge clk or posedge rst)
     if (rst)
       q <= 0;
     else
       q <= d;

endmodule // flop

module sub (o, i);
   output o;
   input  i;

   assign o = ~i;

endmodule // sub
