module top (out, clk, in);
   output [3:0] out;
   input        clk;
   input [3:0]  in;

   wire         rst = 1;

   reg [3:0]    out;
   always @ (posedge clk or negedge rst)
     if (~rst)
       out <= 4'h5;
     else
       out <= in;

endmodule // top
