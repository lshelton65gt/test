// Test tie net on a sub module clock input port
module top (out, clk, in);
   output out;
   input  clk, in;

`ifdef EVENTSIM
   sub S1 (out, 1'b0, in);
`else
   sub S1 (out, clk, in);
`endif

endmodule // top

module sub (out, clk, in);
   output out;
   input  clk, in;

   reg    out;
   initial begin
      out = 0;
   end

   always @ (posedge clk)
     out <= in;

endmodule // top
