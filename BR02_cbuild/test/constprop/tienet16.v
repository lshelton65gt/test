// Test tie net on a top module input port
module top (out1, out2, out3, out4, sel1, sel2, sel3, sel4, in1, in2);
`ifdef EVENTSIM
   output [3:0]  out1, out2, out3, out4;
   input [3:0]   in1, in2;
   output [31:0] sel1;
   output [3:0]  sel2;
   output [15:0] sel3;
   output [7:0]  sel4;
   wire [31:0]   sel1 = 32'h2;
   wire [3:0]    sel2 = 4'h2;
   wire [15:0]   sel3 = 16'b010011100;
   wire [7:0]    sel4 = 8'b10010010;
`else
   output [3:0] out1, out2, out3, out4;
   input [31:0] sel1;
   input [3:0]  sel2;
   input [15:0] sel3;
   input [7:0]  sel4;
   input [3:0]  in1, in2;
`endif

   reg [3:0]    out1, out2, out3, out4;
   always @ (sel1 or in1 or in2)
     begin
        if (sel1 == 32'h2)
          out1 = in1;
        else
          out1 = in2;

        if (sel2 == 4'h2)
          out2 = in2;
        else
          out2 = in1;

        if (sel3 == 16'b010011100)
          out3 = in1;
        else
          out3 = in2;

        if (sel4 == 8'b10010010)
          out4 = in2;
        else
          out4 = in1;
     end

endmodule // top
