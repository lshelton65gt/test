module top (out1, out2, in1, in2);
   output [3:0] out1, out2;
   input [3:0] 	in1, in2;

   wire [3:0] 	c1, c2;
   sub S1 ({c1, c2}, {in1, in2});
   assign 	out1 = { (c1 == 4'h1), (c1 == 4'h2), (c1 == 4'h4),
		   (c1 == 4'h8) };
   assign out2 = { (c2 == 4'h1), (c2 == 4'h2), (c2 == 4'h4),
		   (c2 == 4'h8) };

endmodule // top


module sub (out, in);
   output [7:0] out;
   input [7:0] 	in;

   assign 	out = in;

endmodule // sub

