// Same as state4 but with async reset
module top (out, clk, rst, in);
   output [3:0] out;
   input        clk, rst;
   input [3:0]  in;

   reg    en;

   sub S1 (out, clk, rst, 1'b0, in);

endmodule // top

module sub (out, clk, rst, en, d);
   output [3:0] out;
   input  clk, rst, en;
   input [3:0] d;

   reg    q;
   wire   gclk = clk & en;
   wire   trst = rst & en;
   initial q = 0;
   always @(posedge gclk)
     q <= d;

   wire clk2 = q | en;
   reg  [3:0] out;
   initial out = 4'b1010;
   always @ (posedge clk2 or posedge trst)
     if (trst)
       out[0] <= 0;
     else
       out[0] <= d[3];

   always @ (posedge clk2 or posedge trst)
     if (trst)
       out[2:1] <= 0;
     else
       out[2:1] <= d[1:0];

   always @ (posedge clk2 or posedge trst)
     if (trst)
       out[3] <= 0;
     else
       out[3] <= d[2];

endmodule // sub
