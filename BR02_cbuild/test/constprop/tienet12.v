// Make sure that if the net is undriven and a top output that we tie
// the output value as well.
module top (out, en, clk, in);
   output out, en;
   input  clk, in;

// Other simulators may not have this directive so we tie it in the
// HDL for them.
`ifdef EVENTSIM
   assign en = 1'b1;
`endif
   sub S1 (out, clk, in, en);

endmodule // top

module sub (out, clk, in, en);
   output out;
   input  clk, in, en;

   reg    out;
   initial begin
      out = 0;
   end

   wire dis = ~en;
   always @ (posedge clk)
     if (~dis)
       out <= in;

endmodule // top
