module top (out, in);
   output [3:0] out;
   input [3:0]  in;

   reg [3:0]    out;
   reg [1:0]    x;
   integer      i;

   always @ (in)
     begin
        x = 0;
        for (i = 0; i < 4; i = i + 1) begin
           out[x] = in[x];
           x = x + 1;
        end
     end

endmodule

        
