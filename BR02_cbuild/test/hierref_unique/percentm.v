// test the way that format specifiers work in $display system task
// in this case we test %m

// I created the gold file for this using Aldec but then I had to
// modify the results. One reason is that when we run with Aldec it
// has a different top module (the test bench). The other reason is
// that the order of the displays is different. There is no ordering
// between intial blocks.  Finally aldec has one error.  Read the output it is
// self checking

module top (out, clk1, keep_live);
   output [1:0] out;
   output       keep_live;      // kludge to work around bug4671
   input 	clk1;
   reg [2:0] 	reg3;
   reg [2:0] 	reg2;
   integer      ichar;

   integer      state;
   reg [1:0] 	out;


   initial
      begin
	 reg3 = 3;
	 reg2 = 2;
	 ichar = 100;
      end
   
//   always @ (posedge clk1)
   initial
     begin
	$display("The top module is: %m");
     end

   mid i1 (clk1, keep_live);

   initial out = 2'b00;

   always @(posedge clk1)
      begin:blk1
	 out = ~out;
	 if ( state == 0 ) begin
	   $display("Here we should display top.blk1\n                     : %m");
	   i1.i2.mytask;
	   i1.i2.myfunction(0);
	   state = 1;
         end
      end

   always @(posedge clk1)
      begin:blk2
	 if ( state == 1) begin
	   $display("Here we should display top.blk2\n                     : %m");
	   i1.i2.mytask;
	   i1.i2.myfunction(1);
           state = 2;
         end
      end

   initial
     begin
	state = 0;
	$display("Here we should display top\n                     : %m");
     end
endmodule // top

module mid (clk1, keep_live);
   input clk1;
   reg [1:0] out_temp;
   output     keep_live;
   
   initial
     begin
	$display("Here we should display top.i1\n                     : %m");
     end

   always @(posedge clk1)
     begin:blk3
	if (top.state == 2) begin
	  $display("Here we should display top.i1.blk3\n                     : %m");
	  top.i1.i2.mytask;
          top.state = 3;
        end
     end

   bot i2 (clk1, keep_live);
endmodule


module bot (clk1, keep_live);
   input clk1;
   output keep_live;

   assign  keep_live = ~clk1;

   task mytask;
      begin:intask
	 $display("Here we should display top.i1.i2.mytask.intask\n                     : %m");
      end
   endtask

   task myfunction;
      input unused;
      begin:infunction
	 $display("Here we should display top.i1.i2.myfunction.infunction\n                     : %m");
      end
   endtask
   
endmodule
