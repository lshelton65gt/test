module top(in, out, clk);
  input clk;
  input [5:0] in;
  output signed [31:0] out;

  sub sub(in, out, clk);
endmodule

module sub(in, out, clk);
  input clk;
  input [5:0] in;
  output signed [31:0] out;
  reg signed [31:0]    out;

  reg signed [31:0]    mem [0:63];
  reg           init;
  initial init = 1'b0;
  

  always @(posedge clk) begin
    if (init == 1'b0) begin
      init = 1'b1;
      initialize;
    end
    out = mem[in];
  end

  task initialize;
    $readmemh("mem.dat", mem);
  endtask
endmodule
