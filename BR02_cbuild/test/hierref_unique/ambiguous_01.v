// filename: test/hierref_unique/ambiguous_01.v
// Description:  This test 


module ambiguous_01(clock, in1, out1) ;
   input clock;
   input [7:0] in1;
   output  out1;		// to keep testdriver happy

   assign out1 = 0;
   MID mid(clock, in1);
   B_up  b(clock,in1);
   
endmodule

module MID(input clock, input [7:0] in1) ;

   B_dn  b(clock,in1);
endmodule

module B_up(input clock, input [7:0] in1) ;

   task action;
      input [7:0] in;
      output [7:0] o;
      o = in + 1;
   endtask
   
   C c(clock, in1);
endmodule

module B_dn(input clock, input [7:0] in1) ;
   task action;
      input [7:0] in;
      output [7:0] o;
      o = in - 1;
   endtask

   C c(clock, in1);
endmodule



module C(input clock, input [7:0] in1) ;

   reg [7:0] o;
   always @(posedge clock)
      begin
	 b.action(in1, o);
	 $display("module b1_?: in1: %d, o: %d", in1, o);
      end
endmodule
