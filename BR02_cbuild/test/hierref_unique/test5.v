// Hierarchical reference test
// Test multiple levels of indirection with hierarchically invoked
// tasks, where the references are all relative.
module top(clk, clk1, i1, i2, o1, o2);
   input clk, clk1, i1, i2;
   output o1, o2;
   state1 state ();
   tasks3 tasks ();
   writer writer (clk, i1,i2);
   middle1 middle1 (clk1, i1, i2);
   middle2 middle2 (clk, i1, i2);

   assign o1 = state.state;
   assign o2 = middle2.state.state;
endmodule

module writer(clk, i1, i2);
   input clk, i1, i2;
   always @(posedge clk)
     tasks1.t(i1, i2);
   tasks1 tasks1 ();
endmodule

module middle1(clk, i1, i2);
   input clk, i1, i2;
   writer writer (clk, i1, i2);
   tasks2 tasks ();
   tasks3 tasks_final ();
endmodule

module middle2(clk, i1, i2);
   input clk, i1, i2;
   writer writer (clk, i1, i2);
   tasks2 tasks ();
   tasks3 tasks_final ();
   state2 state ();
endmodule

module state1();
   reg state;
endmodule

module state2();
   reg state;
endmodule

module tasks1();
   task t;
      input i1;
      input i2;
      begin
         tasks.t(i1, i2);
      end
   endtask
endmodule

module tasks2();
   task t;
      input i1;
      input i2;
      begin
         tasks_final.t(~i1, i2);
      end
   endtask
endmodule

module tasks3();
   task t;
      input i1;
      input i2;
      begin
         state.state = i1 & i2;
      end
   endtask
endmodule
