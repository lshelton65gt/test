// Hierarchical reference test
// Test hierarchical reference to net which results in different
// bit size depending on the elaboration.
module top(clk,
           in1, in2,
           q0, q1);
   input clk;
   input in1, in2;
   output [7:0] q0;
   output [7:0] q1;

   procedures0 proc();
   middle middle(clk, in1, in2, q0);
   child child(clk, in1, in2, q1);
endmodule

module middle(clk, in1, in2, q);
   input clk;
   input in1, in2;
   output [7:0] q;

   procedures1 proc();
   child child(clk, in1, in2, q);
endmodule

module child(clk, in1, in2, q);
   input clk;
   input in1, in2;
   output [7:0] q;
   always @(posedge clk)
     begin
        proc.state = in1 + in2;
     end
   assign q = proc.state;
endmodule

module procedures0();
   reg state;
endmodule

module procedures1();
   reg [3:0] state;
endmodule
