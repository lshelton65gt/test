// Run this testcase with -promoteHierTasks

module top(a, b, c, d);
  input a, b, c;
  output d;

  sub sub(a, b, c, d);
endmodule

module sub(a, b, c, d);
  input a, b, c;
  output d;
  reg    d;

  leaf leaf(a, b, c);

  // Verific flow will populate the task hierref enable (leaf.myxor(a, leaf.myxor(r, c))) under assign_d task, and so it will correctly reference parent scope variable 'r'
  // Interra flow will populate an extra always block in 'sub' module and will move task hierref enable (leaf.myxor(a, leaf.myxor(r, c))) under extra always block. This implementation is buggy, as there will be no 'r' var to be referenced, and it will result to a segfault in later phases. 
  task assign_d;
    reg r;
    begin
        r = b; 
        d = leaf.myxor(a, leaf.myxor(r, c));
    end
  endtask
endmodule

module leaf(a, b, c);
  input a, b, c;

  // This function will not be promoted
  function myxor;
    input a, b;
    myxor = a ^ b;
  endfunction

  // This always-block will be promoted to 'top'
  always @(a or b or c)
    sub.assign_d;
endmodule
