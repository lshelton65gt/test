//  This test used to fail when -multiLevelHierTasks switch was
//  on.  A fix was added to

module ms_fixup(out, clk);
output out;
input clk;

sister s1(data);
brother b1(out, clk);

endmodule

module sister(data);
output data;
reg data;

endmodule

module brother(out, clk);
output out;
input clk;
reg out;

task superfly;
  out = ms_fixup.data;
endtask

always@(posedge clk)
begin
  superfly;
end

endmodule
