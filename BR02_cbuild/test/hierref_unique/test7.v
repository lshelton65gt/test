module top(in, out);
  input in;
  output [1:0] out;

  sub sub(in, out);

  function [1:0] xfer;
    input in;
    xfer = {in,in};
  endfunction // xfer
endmodule // top

module sub(in, out);
  input in;
  output [1:0] out;
  reg [1:0]    out;

  always @(in) begin
    out = top.xfer(in);
  end
endmodule // sub
