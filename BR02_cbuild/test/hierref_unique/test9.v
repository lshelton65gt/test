// Hierarchical reference test
// Test multiple levels of hierarchical task invocation.
// In this test, the caller and callee have an inconsistent
// hierarchical relationship in the instantiations.
module top(clk1, clk2, d1, d2, d3, d4, q1, q2, accum);
   input clk1, clk2, accum;
   input d1, d2, d3, d4;
   output q1, q2;
   procedures0 proc(accum);
   procedures1 proc1();
   child child (clk1, d1, d2, q1);
   middle middle (clk2, d3, d4, q2);

   always @(posedge clk1)
     proc.assigner(child.state, child.q);
   always @(posedge clk2)
     proc.assigner(middle.child.state, middle.child.q);
endmodule

module middle(clk, d1, d2, q);
   input clk;
   input d1, d2;
   output q;
   procedures1 proc1();
   child child (clk, d1, d2, q);
endmodule

module child(clk, d1, d2, q);
   input clk;
   input d1, d2;
   output q;
   reg    q;
   reg    state;
//   always @(posedge clk)
//     proc1.caller(state, q);
   always @(d1 or d2)
     proc1.setvalue(d1, d2, state);
endmodule

module procedures0(accum);
   input accum;
   task assigner;
      input i;
      output o;
      o = inverter(i);
   endtask

   function inverter;
     input i;
     inverter = ~i | accum;
   endfunction
endmodule

module procedures1();
   task setvalue;
      input i1, i2;
      output o;
      o = i1 & i2;
   endtask
endmodule
