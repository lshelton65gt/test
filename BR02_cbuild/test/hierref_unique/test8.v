// Hierarchical reference test


/*
  Add test for that has call-tree

    hierTaskEnable()
      task()
        hierTaskEnable()
          task()
            localFunctionCall()
              function()
                module-scoped variable reference

  If you demote 3088 to a warning, then you get a error 3084 with
  this testcase:

  test8.v:xxx child.proc1.accum: Error 3084: Unable to resolve this hierarchical reference within child
*/

module top(clk1, clk2, d1, d2, d3, d4, q1, q2, accum, mask);
   input clk1, clk2, accum, mask;
   input d1, d2, d3, d4;
   output q1, q2;
   procedures0 proc(accum);
   procedures1 proc1(mask);
   child child (clk1, d1, d2, q1);
   middle middle (clk2, d3, d4, q2, mask);
endmodule

module middle(clk, d1, d2, q, mask);
   input clk, mask;
   input d1, d2;
   output q;
   procedures1 proc1(mask);
   child child (clk, d1, d2, q);
endmodule

module child(clk, d1, d2, q);
   input clk;
   input d1, d2;
   output q;
   reg    q;
   reg    state;
   always @(posedge clk)
     proc1.caller(state, q);
   always @(d1 or d2)
     proc1.setvalue(d1, d2, state);
endmodule

module procedures0(accum);
   input accum;
   task assigner;
      input i;
      output o;
      o = inverter(i);
   endtask

   function inverter;
     input i;
     inverter = ~i | accum;
   endfunction
endmodule

module procedures1(mask);
  input mask;
   task caller;
      input i;
      output o;
      proc.assigner(i & mask, o);
   endtask
   task setvalue;
      input i1, i2;
      output o;
      o = i1 & i2;
   endtask
endmodule
