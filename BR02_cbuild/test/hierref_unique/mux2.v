module top(out, i0, i1, sel);
  output [1:0] out;          
  input  [1:0] i0, i1, sel;  
  mux mux1(out[0], i0[0], i1[0], sel[0]);
  mux mux2(out[1], i0[1], i1[1], sel[1]);
endmodule

module mux(out, i0, i1, sel);
  output out;                
  input  i0, i1, sel;        
  reg    out;                

  always @(i0 or i1 or sel)
    functions.mux(out, i0, i1);

  functions functions(sel);   
endmodule                     

module functions(sel);        
  input sel;                  
  task mux;
    output out; input i0, i1;
    begin: muxscope
      reg t1, t2;
      subfuncs.and2(t1, i0, ~sel);
      subfuncs.and2(t2, i1, sel);
      subfuncs.or2(out, t1, t2);
    end
  endtask

  subfuncs subfuncs();        
endmodule                     

module subfuncs;
  task and2;    
    output out;
    input i0, i1;
    out = i0 & i1;
  endtask         
  task or2;       
    output out;
    input i0, i1;
    out = i0 | i1;
  endtask
endmodule
