// last mod: Tue Feb 28 14:07:18 2006
// filename: test/hierref_unique/bug2137.v
// Description:  This test is from bug 2137, where at one time we got:
// Error 3063: Hierarchical reference resolves to different net ranges in different instantiations
// with version 1.4319 we get an internal error

module bug2137(a);
   input a;
  
   foo #(1) f1(a);
   foo #(2) f2(a);

   always@(a) f1.c.bar;
   always@(a) f2.c.bar;
      
endmodule // top

module foo(a);
   parameter w = 10;

   input a;

   child #(w) c(a);
   
endmodule // foo

module child(a);   
   parameter w = 10;

   input a;

   reg [w:0] r;
   reg [w:0]	     data;

   task bar;
      begin
	 c.r = c.data;	 
      end
   endtask // bar      
endmodule

