// Hierarchical reference test
// Test multiple levels of hierarchical task invocation.
// In this test, the caller and callee have a consistent
// hierarchical relationship in all instantiations.
module top(clk1, clk2, d1, d2, d3, d4, q1, q2);
   input clk1, clk2;
   input d1, d2, d3, d4;
   output q1, q2;
   procedures proc();
   child child (clk1, d1, d2, q1);
   middle middle (clk2, d3, d4, q2);
endmodule

module middle(clk, d1, d2, q);
   input clk;
   input d1, d2;
   output q;
   procedures proc();
   child child (clk, d1, d2, q);
endmodule

module child(clk, d1, d2, q);
   input clk;
   input d1, d2;
   output q;
   reg    q;
   always @(posedge clk)
     proc.caller(q);
   always @(d1 or d2)
     proc.setvalue(d1 & d2);
endmodule

module procedures();
   reg state;
   task caller;
      output o;
      proc.assigner(o);
   endtask
   task assigner;
      output o;
      o = state;
   endtask
   task setvalue;
      input i;
      state = i;
   endtask
endmodule
