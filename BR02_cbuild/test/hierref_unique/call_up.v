module top(in1, in2, out, clk);
  input in1, in2, clk;
  output out;
  reg    out;

  wire   tmp2 = ~in2;

  task invert;
    if (1'b0 == 1'b0)
      out = ~in1;
    else
      out = tmp2;
  endtask

  sub sub(clk);
endmodule

module sub(clk);
  input clk;

  always @(posedge clk)
    do_it;

  not(clk_, clk);
  always @(negedge clk_)
    do_it;

  task do_it;
    top.invert;
  endtask
endmodule
