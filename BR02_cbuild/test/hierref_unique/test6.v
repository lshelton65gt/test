module top(in, out, clk);
  input in, clk;
  output [1:0] out;

  sub sub(in, out, clk);

  function [1:0] xfer;
    input in;
    xfer = {in,in};
  endfunction // xfer
endmodule // top

module sub(in, out, clk);
  input in, clk;
  output [1:0] out;
  reg [1:0]    out;

  always @(posedge clk)
    out = top.xfer(in);
endmodule // sub
