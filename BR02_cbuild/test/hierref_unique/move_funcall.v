// Run this testcase with -promoteHierTasks

module top(a, b, c, d);
  input a, b, c;
  output d;

  sub sub(a, b, c, d);
endmodule

module sub(a, b, c, d);
  input a, b, c;
  output d;
  reg    d;

  leaf leaf(a, b, c);

  // This task will be promoted to 'top', but
  // there will be an always-blocks left behind
  // for leaf.myxor(b,c) which references variables
  // declared in task assign.d
  task assign_d;
    d = leaf.myxor(a, leaf.myxor(b, c));
  endtask
endmodule

module leaf(a, b, c);
  input a, b, c;

  // This function will not be promoted
  function myxor;
    input a, b;
    myxor = a ^ b;
  endfunction

  // This always-block will be promoted to 'top'
  always @(a or b or c)
    sub.assign_d;
endmodule
