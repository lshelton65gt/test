// Hierarchical reference test
// Test multiple levels of hierarchical task invocation.
// In this test, the caller and callee have an inconsistent
// hierarchical relationship in the instantiations.
// Additionally, child calls different tasks depending on its
// elaboration.
module top(clk1, clk2, d1, d2, d3, d4, q1, q2);
   input clk1, clk2;
   input d1, d2, d3, d4;
   output q1, q2;
   procedures0 proc();
   procedures1 proc1();
   child child (clk1, d1, d2, q1);
   middle middle (clk2, d3, d4, q2);
endmodule

module middle(clk, d1, d2, q);
   input clk;
   input d1, d2;
   output q;
   procedures2 proc1();
   child child (clk, d1, d2, q);
endmodule

module child(clk, d1, d2, q);
   input clk;
   input d1, d2;
   output q;
   reg    q;
   reg    state;
   always @(posedge clk)
     proc1.caller(state, q);
   always @(d1 or d2)
     proc1.setvalue(d1, d2, state);
endmodule

module procedures0();
   task assigner;
      input i;
      output o;
      o = i;
   endtask
endmodule

module procedures1();
   task caller;
      input i;
      output o;
      proc.assigner(i, o);
   endtask
   task setvalue;
      input i1, i2;
      output o;
      o = i1 & i2;
   endtask
endmodule

module procedures2();
   task caller;
      input i;
      output o;
      proc.assigner(i, o);
   endtask
   task setvalue;
      input i1, i2;
      output o;
      o = i1 | i2;
   endtask
endmodule
