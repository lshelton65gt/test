module top(out, i0, i1, sel);
  output [1:0] out;          
  input [1:0]  i0, i1;
  input sel;  
  mux mux1(out[0], i0[0], i1[0]);
  mux mux2(out[1], i0[1], i1[1]);
endmodule

module mux(out, i0, i1);
  output out;                
  input  i0, i1;        
  reg    out;                

  always @(i0 or i1 or top.sel)
    functions.mux(out, i0, i1);

  functions functions();   
endmodule                     

module functions;        
  task mux;
    output out; input i0, i1;
    begin: muxscope
      reg t1, t2;
      subfuncs.and2(t1, i0, ~top.sel);
      subfuncs.and2(t2, i1, top.sel);
      subfuncs.or2(out, t1, t2);
    end
  endtask

  subfuncs subfuncs();        
endmodule                     

module subfuncs;
  task and2;    
    output out;
    input i0, i1;
    out = i0 & i1;
  endtask         
  task or2;       
    output out;
    input i0, i1;
    out = i0 | i1;
  endtask
endmodule
