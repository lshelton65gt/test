-- March 2008
-- This file tests the user type population for all the signals.
-- Before this commit the user type for signals of the sub2.bottom1
-- weren't populated. 

package base is
  subtype myType is bit;
end base;

use work.base.all;

entity bug8493_01 is
   port (
          pin1  : in  myType;
          pout1 : out myType;
          pin2  : in  myType;
          pout2 : out myType
          );
end;


use work.base.all;

architecture top of bug8493_01 is

component sub
  port (
    i : in myType;
    o : out myType
    );
end component;

begin

  sub1 : sub port map (pin1, pout1);
  sub2 : sub port map (pin2, pout2);

end top;

use work.base.all;
entity sub is
  port (
    i : in myType;
    o : out myType
    );
end sub;

use work.base.all;
architecture arch of sub is

component bottom
  port (
    i : in myType;
    o : out myType
    );
end component;

begin

  bottom1 : bottom port map (i, o);
  
end arch;


use work.base.all;
entity bottom is
  port (
    i : in myType;
    o : out myType
    );
end bottom;

use work.base.all;
architecture arch of bottom is

begin

  o <= i;
  
end arch;
