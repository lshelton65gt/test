-- February 2008
-- This file tests the user type population for enumaration, character and string types.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vis3 is
   port (in1  : in bit_vector(0 to 2);
         out1 : out bit_vector(0 to 2));
end vis3;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

architecture arch of vis3 is
  type    days    is (mon, tue, wed, thr, fri, sat, sun, wow);
  type    colors  is ('r', 'o', 'y', 'g', 'b', 'd', 'v', 'w');
  subtype weekend is days range sat to wow;
  type state      is (up, down, left, right);
  attribute enum_encoding : string;
  attribute enum_encoding of state : type is "001100 000011 111000 000111";
  
  signal enumSignal : days := wow;                -- carbon observeSignal
  signal workWeekSignal : days range mon to fri;  -- carbon observeSignal
  signal weekendEnumSignal : weekend := wow;      -- carbon observeSignal
  signal colorEnumSignal : colors := 'w';         -- carbon observeSignal
  signal charSignal : character := 'A';           -- carbon observeSignal
  subtype str_t is string(1 to 8);
  signal stringSignal : str_t := "ABCDEFGH";      -- carbon observeSignal
  signal stateMachine : state := up;              -- carbon observeSignal 
begin


  p1: process (in1)
    variable en_var : days;
    variable workWeek_var : days;
    variable weekend_var : weekend;
    variable color_var : colors;
  begin  -- process p1
    
    case in1 is
      when "000" =>
        en_var := mon;
        workWeek_var := mon;
        color_var := 'r';
      when "001" =>
        en_var := tue;
        workWeek_var := tue;
        color_var := 'o';
      when "010" =>
        en_var := wed;
        workWeek_var := wed;
        color_var := 'y';
      when "011" =>
        en_var := thr;
        workWeek_var := thr;
        color_var := 'g';
      when "100" =>
        en_var := fri;
        workWeek_var := fri;
        color_var := 'b';
      when "101" =>
        en_var := sat;
        weekend_var := sat;
        color_var := 'd';
      when "110" =>
        en_var := sun;
        weekend_var := sun;
        color_var := 'v';
  when others => 
        en_var := wow;
        color_var := 'w';
    end case;
    enumSignal <= en_var;
    workWeekSignal <= workWeek_var;
    weekendEnumSignal <= weekend_var;
    colorEnumSignal <= color_var;
  end process p1;
    
  p2: process (enumSignal)
  begin
    case enumSignal is
      when  mon =>
        out1 <= "001";
      when  tue =>
        out1 <= "010";
      when  wed =>
        out1 <= "011";
      when  thr =>
        out1 <= "100";
      when  fri =>
        out1 <= "101";
      when  sat =>
        out1 <= "110";
      when  sun =>
        out1 <= "111";
      when others => 
        out1 <= "000";
    end case;
  end process p2;
end arch;


