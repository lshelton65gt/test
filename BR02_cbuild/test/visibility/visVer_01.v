// Dec. 2007
// This file tests the user type population for reg, wire, integer, time and real.
// There are two mode of operation in Cheetah, with -2000, -2001, -v2k switch, 
// and without. In 2000 version additional types are supported. This file is used 
// for both modes. Translate_on/off is used to remove the lines for the not 2000
// mode.

module bug (clk);
  input clk;

   reg [3:1] vectorReg;       // carbon observeSignal
   wire [1:3] vectorWire;     // carbon observeSignal
   reg  [4:1] arrayReg [8:1]; // carbon observeSignal

   integer netInt;            // carbon observeSignal
   real    netReal;           // carbon observeSignal
   time    netTime;           // carbon observeSignal

   integer arrayInt [3:1];    // carbon observeSignal
   time    arrayTime[3:0];    // carbon observeSignal
   
// carbon translate_off
   wire  [3:0] arrayWire [7:0];       // carbon observeSignal

   reg  [3:0] array2Reg  [1:0][7:0];  // carbon observeSignal
   wire [3:0] array2Wire [1:0][7:0];  // carbon observeSignal
// carbon translate_on

   reg        bitArrayReg [7:0];      // carbon observeSignal

// carbon translate_off
   wire       bitArrayWire[7:0];      // carbon observeSignal
   reg        bitArray2reg[1:0][7:0]; // carbon observeSignal
   wire       bitArray2wire[1:0][7:0];// carbon observeSignal
// carbon translate_on

endmodule //Bug

