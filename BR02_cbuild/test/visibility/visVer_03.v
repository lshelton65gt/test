// Dec. 2007
// This file tests population of user types of integer, real and time nets.
module integer1 (clk, rst, in1, out1);
  input clk;
  input rst;
  input in1;
  output out1;

  integer  counter; // carbon observeSignal
  real     tempR;    // carbon observeSignal
  time     tempT;   // carbon observeSignal
  integer  out1;

   
  always @(posedge clk or posedge rst) 
  begin
    if (rst)
      begin
	counter <= 0; 
        out1 <= 0;
	tempR <= 0.0;
	 tempT <= 0; 
      end 
    else
      begin
	 counter <= counter + 1;
	 tempR <= tempR + 1.0;
	 out1 <= in1;
	 tempT <= tempT + 1;
      end
  end

endmodule 

