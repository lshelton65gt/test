-- Tests for save/restore of following types:
-- 1. Arrays
-- 2. Records
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity vis2 is
  port (
    clk : in std_logic;                 
    i : in  std_logic_vector(31 downto 0);
    o  : out std_logic_vector(31 downto 0));
end vis2;

architecture arch of vis2 is

  -- array of arrays
  type typ1 is array (3 downto 0) of std_logic;
  type typ2 is array(natural range <>) of typ1;  -- unconstrained array
  type typ3 is array(0 to 1) of typ2(2 downto 0);
  type typ4 is array(1 to 4) of typ3;
  -- multi-dimensional array
  type typ5 is array(3 downto 0, 1 downto 0, 0 to 1, 1 to 4) of std_ulogic;
  -- record
  type rec is record
                f1 : typ4;
                f2 : typ5;
              end record;
  -- array of array of records
  type typ6 is array(1 to 3) of rec;
  type typ7 is array(4 downto 2) of typ6;
  -- multi-dimensional array of array of records
  type typ8 is array(2 to 3, 5 downto 2) of typ6;
  -- nested record
  type nrec is record
                 f1 : typ6;
                 f2 : rec;
               end record;
  -- multi-dimensional array of nested record
  type typ9 is array(3 downto 0, 0 to 2) of nrec;
  type typ10 is array(integer range <>) of nrec;

  signal sig1 : rec;
  signal sig2 : typ2(2 downto 0);                   -- carbon observeSignal
  signal sig3 : nrec;
  signal sig4 : typ4;
  signal sig5 : typ5;
  signal sig6 : typ6;
  signal sig7 : typ7;
  signal sig8 : typ8;
  signal sig9 : typ9;
  signal sig10 : typ10(1 to 3);
  signal sig11 : std_ulogic_vector(1 to 5);

  signal idx3dt0 : integer range 3 downto 0 := 0;
  signal idx1dt0 : integer range 1 downto 0 := 0;
  signal idx0to1 : integer range 0 to 1 := 0;
  signal idx1to4 : integer range 1 to 4 := 1;

  constant c1 : std_logic_vector := "10101010101010101010100";
  
begin  -- arch

  proc: process (clk)
  begin -- process proc
    if clk'event and clk = '1' then
      sig1 <= (f1 => (others => (others => (others => (3 => i(2), 2 => i(3), 1 => i(0), 0 => i(1))))),
               f2 => (others => (others => (others => (4 => i(2), 3 => i(3), 2 => i(4), 1 => i(20))))));
      sig2 <= (2 => (3 => i(5), 2 => i(6), 1 => i(8), 0 => i(7)),
               others => (3 => i(9), 2 => i(11), 1 => i(10), 0 => i(12)));
      sig3 <= (f1 => (others => sig1), f2 => sig1);
      sig4 <= (others => (others => (others => (3 => i(13), 2 => i(15), 1 => i(14), 0 => i(16)))));
      sig5 <= (others => (others => (others => (4 => i(17), 3 => i(19), 2 => i(18), 1 => i(19)))));
      sig6 <= (others => sig1);
      sig7 <= (others => sig6);
      sig8 <= (others => (others => sig6));
      sig9 <= (others => (others => sig3));
      sig10 <= (others => sig3);

      idx3dt0 <= conv_integer(unsigned(i(31 downto 30)));
      idx1dt0 <= conv_integer(unsigned(i(26 downto 26)));
      idx0to1 <= conv_integer(unsigned(i(23 downto 23)));
      idx1to4 <= conv_integer(unsigned(i(22 downto 21))) + 1;

      o <= c1 & sig5(idx3dt0, idx1dt0, idx0to1, idx1to4) & sig4(idx1to4)(idx0to1)(idx1dt0)(idx3dt0) &
           sig3.f1(3).f1(idx1to4)(idx0to1)(idx1dt0)(idx3dt0) & sig2(idx1dt0)(idx3dt0) & sig6(3).f2(idx3dt0, idx1dt0, idx0to1, idx1to4) &
           sig7(4)(3).f2(idx3dt0, idx1dt0, idx0to1, idx1to4) & sig8(2,4)(3).f2(idx3dt0, idx1dt0, idx0to1, idx1to4) &
           sig9(2,1).f1(3).f1(idx1to4)(idx0to1)(idx1dt0)(idx3dt0) & sig10(2).f1(3).f1(idx1to4)(idx0to1)(idx1dt0)(idx3dt0);
--   o <= "1010101010101010101010101010100" & sig1.f1(idx1to4)(idx0to7)(idx1dt0)(idx3dt0);
    end if;
  end process proc;
end arch;
