-- Tests for save/restore of following types:
-- 1. Basic data types.
-- 2. Subtypes
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity vis1 is
  port (
   clk : in std_logic;
    i : in  std_logic_vector(31 downto 0);
    o  : out std_logic_vector(31 downto 0));
end vis1;

architecture arch of vis1 is

  subtype typ1 is std_logic_vector(31 downto 0);
  subtype typ2 is std_logic_vector(4 downto 0);
  subtype typ3 is integer range -16 to 15;
  subtype typ4 is natural range 0 to 31;
  subtype typ5 is typ3 range 0 to 12;
  subtype typ6 is typ4 range 0 to 15;
  subtype typ7 is integer;
  subtype typ8 is natural;
  
  subtype typ9 is std_logic_vector(0 to 0);
  subtype typ10 is std_logic;

  subtype typ11 is signed;
  subtype typ12 is unsigned;
  
  signal sig1 : typ1;                   -- carbon observeSignal
  signal sig2 : typ2;                   -- carbon observeSignal
  signal sig3 : typ3;                   -- carbon observeSignal
  signal sig4 : typ4;                   -- carbon observeSignal
  signal sig5 : typ5;                   -- carbon observeSignal
  signal sig6 : typ6;                   -- carbon observeSignal
  signal sig7 : typ7;                   -- carbon observeSignal
  signal sig8 : typ8;                   -- carbon observeSignal
  signal sig9 : typ9;                   -- carbon observeSignal
  signal sig10 : typ10;                 -- carbon observeSignal
  signal sig11 : typ11(31 downto 0);    -- carbon observeSignal
  signal sig12 : typ12(31 downto 0);    -- carbon observeSignal

  constant c1 : std_logic_vector(24 downto 0) := (others => '1');
  
begin  -- arch

  proc: process (clk)
  begin  -- process proc
    if clk'event and clk = '1' then  -- rising clock edge
      sig1 <= i;
      sig2 <= i(4 downto 0);

      sig3 <= conv_integer(signed(sig2));
      sig4 <= conv_integer(unsigned(sig2));

      sig5 <= conv_integer(unsigned(sig2(2 downto 0)));
      sig6 <= conv_integer(unsigned(sig2(3 downto 0)));

      sig7 <= conv_integer(sig11);
      sig8 <= conv_integer(sig12);

      sig9 <= i(sig4 downto sig4);
      sig10 <= i(sig4);
  
      sig11 <= signed(sig1);
      sig12 <= unsigned(sig1);

      o <= sig1 xor (c1 & sig2 & sig9 & sig10) xor std_logic_vector(sig11)
           xor std_logic_vector(sig12) xor std_logic_vector(conv_signed(sig7,32))
           xor std_logic_vector(conv_unsigned(sig8,32))
           xor (c1(13 downto 0) & std_logic_vector(conv_signed(sig3,5)) &
                std_logic_vector(conv_unsigned(sig4,5)) &
                std_logic_vector(conv_unsigned(sig5,4)) &
                std_logic_vector(conv_unsigned(sig6,4)));
      
    end if;
  end process proc;
  
end arch;
