library ieee;
use ieee.std_logic_1164.all;

package defs is

  type t0 is array (natural range <>) of std_logic;
  subtype t1 is t0 (0 to 7);

  type t2 is array (natural range <>, natural range <>) of std_logic;
  subtype t3 is t2 (0 to 7, 0 to 7);

  subtype t4 is integer range 0 to 1024;
  subtype t5 is integer;
  
end defs;


library ieee;
use ieee.std_logic_1164.all;
use work.defs.all;

entity vis4 is
  
  port ( pin1  : in  t0(0 to 7);
         pin2  : in  t1;
         pin3  : in  t2(0 to 7, 0 to 7);
         pin4  : in  t3;
         pin5  : in  t4;
         pin6  : in  t5 range 1024 to 2048;
         pout1 : out t0(0 to 7);
         pout2 : out t1;
         pout3 : out t2(0 to 7, 0 to 7);
         pout4 : out t3;
         pout5 : out t4;
         pout6 : out t5 range 1024 to 2048);

end vis4;

architecture arch of vis4 is

begin  -- arch

  pout1 <= pin1;
  pout2 <= pin2;
  pout3 <= pin3;
  pout4 <= pin4;
  pout5 <= pin5;
  pout6 <= pin6;

end arch;
