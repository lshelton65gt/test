// Dec. 2007
// This file tests different user types in Verilog.
// There are two mode of operation in Cheetah, with -2000, -2001, -v2k switch, 
// and without. In 2000 version additional types are supported. This file is used 
// for both modes. Translate_on/off is used to remove the lines for the not 2000
// mode.

// The commented out supply nets should be commented in after bug 8127 is fixed.

module bug (clk);
  input clk;

   reg     netReg;
   wire    netWire;
   wand    netWand;
   wire    netWor;
   tri     netTri;
   tri0    netTri0;
   tri1    netTri1;
   triand  netTriAnd;
   trior   netTriOr;
   trireg  netTriReg;
   supply0 netSupply0;
   supply1 netSupply1;

   integer netInt;
   time    netTime;
 


   reg    [2:0]  vectorReg;
   wire   [2:0]  vectorWire;
   wand   [2:0]  vectorWand;
   wor    [2:0]  vectorWor;
   tri    [2:0]  vectorTri;
   tri0   [2:0]  vectorTri0;
   tri1   [2:0]  vectorTri1;
   triand [2:0]  vectorTriAnd;
   trior  [2:0]  vectorTriOr;
   trireg [2:0]  vectorTriReg;
   supply0[2:0]  vectorSupply0;
   supply1[2:0]  vectorSupply1;


   reg    [2:0]  arrayReg     [0:3];
   reg           bitArrayReg  [0:3];

   
// carbon translate_off
   wire   [2:0]  arrayWire    [0:3];
   wand   [2:0]  arrayWand    [0:3];
   wor    [2:0]  arrayWor     [0:3];
   tri    [2:0]  arrayTri     [0:3];
   tri0   [2:0]  arrayTri0    [0:3];
   tri1   [2:0]  arrayTri1    [0:3];
   triand [2:0]  arrayTriAnd  [0:3];
   trior  [2:0]  arrayTriOr   [0:3];
   trireg [2:0]  arrayTriReg  [0:3];
//   supply0[2:0]  arraySupply0 [0:3];
//   supply1[2:0]  arraySupply1 [0:3];

   wire     bitArrayWire      [0:3];
   wand     bitArrayWand      [0:3];
   wor      bitArrayWor       [0:3];
   tri      bitArrayTri       [0:3];
   tri0     bitArrayTri0      [0:3];
   tri1     bitArrayTri1      [0:3];
   triand   bitArrayTriAnd    [0:3];
   trior    bitArrayTriOr     [0:3];
   trireg   bitArrayTriReg    [0:3];
//   supply0  bitArraySupply0   [0:3];
//   supply1  bitArraySupply1   [0:3];
   
// carbon translate_on


endmodule //Bug

