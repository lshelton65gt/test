module top(a, b, out1);
  input a;
  input [1:0] b;
  output [1:0] out1;

  sub #(0) sub(a, b, out1);
endmodule

module sub(a, b, out1);
  input a;
  input [1:0] b;
  output [1:0] out1;
  parameter    rpt = 1;

  assign       out1 = ~{rpt{a}};
endmodule
