module top(a, b, out1, out2);
  input a;
  input [1:0] b;
  output [1:0] out1, out2;

  sub #(0) sub(a, b, out1, out2);
endmodule

module sub(a, b, out1, out2);
  input a;
  input [1:0] b;
  output [1:0] out1, out2;
  parameter    rpt = 1;// note that the parameter is set to 0 at the module instantiation above, thus rpt is 0

  assign       out1 = {{rpt{a}},b};
  assign       out2 = {rpt{a}} + b;
endmodule
