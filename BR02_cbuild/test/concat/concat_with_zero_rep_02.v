// filename: test/concat/concat_with_zero_rep_02.v
// Description:  This test should be run with: -topLevelParam rep=0 and -enableIgnoreZeroReplicationConcatItem


module concat_with_zero_rep_02(clock, in1, in2, out1, out2) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   output [7:0] out2;
   parameter rep = 4;
   
   sub #(0) u1(clock, in1, in2, out1) ;
   sub #(2) u2(clock, in1, in2, out2) ;
   
endmodule

module sub(clock, in1, in2, outA) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] outA;
   reg [7:0] outA;
   parameter rep = 4;

   always @(posedge clock)
     begin: main
       outA = {in1,{rep{1'b1}}} & in2; // if rep is 0 then this is in1 & in2 
     end
endmodule
