module top(a, b, out);
  input [1:0] a, b;
  output [392:0] out;
  
  assign         out = {100{a,b}}; // need to truncate
endmodule
