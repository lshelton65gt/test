module top(a, b, out1, out2);
  input a, b;
  output [1:0] out1, out2;

  assign       out1 = {{0{a}},b};
  assign       out2 = {0{a}} + b;
endmodule
