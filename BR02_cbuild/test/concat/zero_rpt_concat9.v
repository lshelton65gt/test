// file: zero_rpt_concat9, here a replication operator is used as a RHS of an
// assignment (instead of being within a concat)

module top(a, b, out1);
  input a;
  input [1:0] b;
  output [1:0] out1;

  sub #(0) sub(a, b, out1);
endmodule

module sub(a, b, out1);
  input a;
  input [1:0] b;
  output [1:0] out1;
  parameter    rpt = 0;

   assign       out1 = {rpt{
			  {1{1'b1}},
			  {rpt{a}} // this reduces to null if systemVerilog
			 }
		       };
endmodule
