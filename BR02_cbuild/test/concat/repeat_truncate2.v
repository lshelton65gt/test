module top(out);
  output [392:0] out;
  
  assign         out = {100{3'b11,3'b00}}; // need to truncate
endmodule
