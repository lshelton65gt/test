module mux2(clk,sel,in1,in2,out);
   input sel,clk;
   input [7:0] in1;
   input [7:0] in2;
   output [7:0] out;

   reg [7:0] 	temp;

   assign 	out = temp;
   always @(posedge clk)
     temp = ({8{sel}} & in1) | ({8{!sel}} & in2);
endmodule //mux2

module mux(sel,in1,in2,in3,in4,clk,out);
   input in1,in2,in3,in4;
   input [2:0] sel;
   input clk;
   reg tmp;
   output out;

   assign out=tmp;
   
   always @(posedge clk)
     tmp = ({3{(sel==0)}} & in1) |
	   ({3{(sel==1)}} & in2) |
	   ({3{(sel==2)}} & in3) |
	   ({3{(sel==3)}} & in4);

   // Not mutually exclusive - use |= assignment mux, please!
   always @(negedge clk)
     tmp = ( {3{(sel==0)}} & in1) |
	   ( {3{(sel>3)}} & in2) |
	   ( {3{(sel ==4)}} & in3);
   

endmodule // mux

module aludecode(clk, instr, a, b, alu);
   input clk;
   input [7:0] instr;
   input [31:0] a;
   input [31:0] b;
   output [31:0] alu;

   reg [31:0] 	 buffer;

   always @(posedge clk)
     buffer = ({32{(instr[2:0] == 0)}} & (a+b) |
	       {32{(instr[2:0] == 1)}} & (a-b) |
	       {32{(instr[2:0] == 2)}} & (a*b) |
	       {32{(instr[2:0] == 3)}} & (a|b) |
	       {32{(instr[2:0] == 4)}} & (a&b) |
	       {32{(instr[2:0] == 5)}} & (a^b) |
	       {32{(instr[2:0] == 6)}} & (a/b) |
	       {32{(instr[2:0] == 7)}} & a);

   assign 	 alu = buffer;
endmodule // aludecode

module top(clk,sel, a, b,c, acc);
   input clk,sel;
   input [7:0] a;
   output [7:0] b;
   output 	c;
   wire [31:0] 	oreg;
   output [31:0] acc;
   
   assign 	 acc = oreg;
   
   mux2 m0(clk, sel, a, ~a, b);

   mux m1(a[2:0], a[0], a[1], a[2], a[3], clk, c);
   aludecode mycpu(clk, a, {a,b,a,b}, {a,a,b,b}, oreg);
   
endmodule
