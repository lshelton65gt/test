// filename: test/concat/concat_with_zero_rep_01.v
// Description:  This test should be run with: -topLevelParam rep=0 and -enableIgnoreZeroReplicationConcatItem


module concat_with_zero_rep_01(clock, in1, in2, out1) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;
   parameter rep = 4;
   parameter rep2 = 0;
   parameter  temp1 = rep - rep2 + 1;
   always @(posedge clock)
     begin: main
       out1 = {in1,{rep{1'b1}}} & in2; // if rep is 0 then this is in1 & in2 
     end
endmodule
