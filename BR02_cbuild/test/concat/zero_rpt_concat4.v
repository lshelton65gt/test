module top(a, b, out1);
  input a;
  input b;
  output [2:0] out1;

  sub #(0) sub(a, b, out1);
endmodule

module sub(a, b, out1);
  input a;
  input b;
  output [2:0] out1;
  parameter    rpt = 1;

  assign       out1 = {1'b1,{rpt{a}},b};
endmodule
