// filename: test/concat/concat_with_zero_rep_03.v
// Description:  This test should be run with: -topLevelParam rep=0 and -enableIgnoreZeroReplicationConcatItem
// similar to concat_with_zero_rep_01 but here there is {in1,{0{{0{1'b0}}}}}
// the second term here is invalid because the concat is completely empty, thus
// this is a negative test

module concat_with_zero_rep_01(clock, in1, in2, out1) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;
   parameter rep = 4;
      parameter rep2 = 0;
   parameter  temp1 = rep - rep2 + 1;
   always @(posedge clock)
     begin: main
       out1 = {in1,{temp1{{rep-rep2{1'b1}}}}} & in2; // if rep, and rep2 are equal then this is invalid because temp1 is 1, but the content of the  item it is repeating is empty
     end
endmodule
