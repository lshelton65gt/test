module top(out, in, sel);
  output [21+3 : 0] out;
  reg [21+3 : 0] out;
  input [10+1 : 0] in;
  input            sel;

  always @(sel or in)
    case (sel)
      1'b0:
        out = {{10+1{1'b0}}, in, 2'b00};
      1'b1:
	out = {{10+2{1'b0}}, in, 1'b0};
    endcase
endmodule

        