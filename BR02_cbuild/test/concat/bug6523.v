// last mod: Fri Sep  8 09:36:46 2006
// filename: test/concat/bug6523.v
// Description:  This test demonstrates the problem documented in bug 6523, at
// one time this would get an error in the codegen phase caused by the fact that
// assignment has a concat on the RHS and a dynamic partselect on the LHS.


module bug6523(pixcnt,data_fa,data_fb,dout);
   input [3:0] pixcnt;
   input [63:0] data_fa;
   input [63:0] data_fb;
   output [1023:0] dout;

   sub sub(pixcnt,{data_fb,data_fa},dout[511:256]);
endmodule
module sub(pixcnt,data_f,data);
   input [3:0] pixcnt;
   input [127:0] data_f;
   output [255:0] data;
   reg [255:0] 	  data;

   always begin
      data[(pixcnt*16) +: 128] <= data_f;
   end
endmodule
