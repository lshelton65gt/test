module mux(sel,out);
   input [2:0] sel;
   output [2:0] out;

  assign        out = {3{sel==0}};
endmodule // mux
