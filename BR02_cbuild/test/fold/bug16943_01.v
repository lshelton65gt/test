// filename: test/fold/bug16820_01.v
// Description:  This test duplicates the problem seen in bug 16943.  In the
// Fold code the pattern recognition of the FLZ (find last zero) pattern was
// incorrect in that it was not checking that the data values were changing in
// the same direction as the bit index. Instead it only checked the boundary
// conditions: (1) the bit position and value at index 3, and (2) the default
// value).
// From these two checks it concluded that the FLZ pattern had been found.
// With the fix for bug16820 the code has been changed to check all index
// values, all data values and that each is incremented by 1 and in matching
// directions. 
// see also bug16820_02 and bug16820_03 for additional similar tests.
module bug16820_01(clk, i,o);
   input        clk;
   input [7:0] 	i;
   output [3:0] o;

   reg [3:0] 	temp;

   assign       o = temp;
   
   always @(negedge clk)
     begin
	temp =  ( i[6] == 0 ) ? 3'd0 :
		( i[5] == 0 ) ? 3'd1 :
		( i[4] == 0 ) ? 3'd2 :
		( i[3] == 0 ) ? 3'd3 :
		( i[2] == 0 ) ? 3'd4 :
		( i[1] == 0 ) ? 3'd5 :
		( i[0] == 0 ) ? 3'd6 : 3'd7;

     end
endmodule
