// Test of lvalue partselect folding
// failing testcase from codegen/partsel.v
// carbon, aldec, nc and mti disagree on the results of this test.
// carbon only differs in 6 bits
// In particular fold vector[2:-4] = ~vector[2:-4] => vector[2:0] = ~vector[2:0];
//
// NC and Aldec are broken on this (as is MTI)
//
// This example writes to partially out-of-range part-selects - the LRM (4.2.1) says that the
// in range parts are to be written, NC doesn't.  ATI doesn't correctly handle the x's in the
// out-of-range pieces..
//
// Gold results for this test come from Carbon!


module probe(c,i,o);
   parameter vwidth=64;          // vector width to access
   parameter iwidth=7;         // index expression width
   input     c;
   input [31:0] i;        // Initial value
   output     [vwidth-1:0]  o;        // computed result

   reg [vwidth-1: 0] vector = 0;

   reg signed [iwidth-1:0]  index;
   always @(posedge c)
     begin : varsel
        integer j;
        for(j = vwidth; j >= 32; j = j - 32)
          vector = (vector << 32) | i;

        for (j = -7; j < (vwidth+7); j = j+3)
          begin : v2
             integer k;
             index = j;
             vector[(index) +:7] = ~vector[(index) +: 7];
             k = k ^(^index);
          end
     end

   assign o = vector;
endmodule // probe
