// test simple cond-expr reductions

module condexpr(sel, in, out1, out2);
  input sel, in;
  output out1, out2;

  assign out1 = sel? in: ~in;   // -> ~(sel ^ in)
  assign out2 = sel? ~in: in;	// -> (sel ^ in)
endmodule
