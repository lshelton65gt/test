// bug7311 test fails due to a fold defect where statements with out-of-bound
// select varsel lvalues are converted to 0 select lvalue ..instead of removing
// it which leads to simulation errors. This is a contrived test.
module test
  (
   clk,
   reset,

   sel,
   state,
   req,
   bnk,

   en,
   mem_out
   );

  parameter max       = 5;
  parameter ONE = 1;
  parameter TWO = 2;
  parameter THREE = 3;
  parameter FOUR = 4;
  parameter FIVE = 5;

  parameter one = 5;
  parameter two = 4;
  parameter three = 3;
  parameter four = 2;
  parameter five = 1;

  input     clk, reset;

  input [1:0] sel;
  input       req;
  input [1:0] bnk;
  input [2:0] state;

  output [1:max] en;
  output [1:0]     mem_out;

  reg [1:0]        bnk_hld[1:max];
  reg [1:max]    en;
  reg [1:0]   mem_out;

  integer          i1, i2;

  initial begin
      en     <= {max{1'b0}};
      for(i1=1; i1<(max+1); i1=i1+1) bnk_hld[i1] <= 2'd0;
  end


  always @(posedge clk or negedge reset) begin
    if (!reset) begin
      en     <= {max{1'b0}};
      for(i1=1; i1<(max+1); i1=i1+1) bnk_hld[i1] <= 2'd0;
    end
    else begin
      bnk_hld[1] <= (state==ONE) ? sel : bnk;
      en[1]  <= req;
      for(i1=1; i1<6; i1=i1+1) begin
        bnk_hld[i1+1] <= bnk_hld[i1] | sel ;
        en[i1+1]  <= en[i1];
      end
      if (state == ONE)
        mem_out <= bnk_hld[one];
      else if (state == TWO)
        mem_out <= bnk_hld[two];
      else if (state == THREE)
        mem_out <= bnk_hld[three];
      else if (state == FOUR)
        mem_out <= bnk_hld[four];
      else
        mem_out <= bnk_hld[five];
    end
  end


endmodule
