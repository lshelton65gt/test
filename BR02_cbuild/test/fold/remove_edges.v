module top(i, o, iclk, phase);
  input iclk;
  input [1:0] phase;
  input [26:0] i;
  output [26:0] o;

  reg           clk, rst, pre;
  initial begin
    clk = 1'b0;
    rst = 1'b0;
    pre = 1'b0;
  end

  // generate changes on clk,rst,pre from a random phase counter, to
  // ensure that we only have one of these toggle at a time.  This avoids
  // hard-to-resolve races in Aldec and allows us to match its behavior
  // without stress.
  always @(posedge iclk)
    case (phase)
      2'b00: clk = !clk;
      2'b01: rst = !rst;
      2'b10: pre = !pre;
    endcase

  dff u0(i[0],   o[0],  clk, rst, pre);
  dff u1(i[1],   o[1],  clk, rst, 1'b0);
  dff u2(i[2],   o[2],  clk, rst, 1'b1);
  dff u3(i[3],   o[3],  clk, 1'b0, pre);
  dff u4(i[4],   o[4],  clk, 1'b1, pre);
  dff u5(i[5],   o[5],  1'b0, rst, pre);
  dff u6(i[6],   o[6],  1'b1, rst, pre);
  dff u7(i[7],   o[7],  clk, 1'b0, 1'b0);
  dff u8(i[8],   o[8],  clk, 1'b0, 1'b1);
  dff u9(i[9],   o[9],  clk, 1'b1, 1'b0);
  dff u10(i[10], o[10], clk, 1'b1, 1'b1);
  dff u11(i[11], o[11], 1'b0, rst, 1'b0);
  dff u12(i[12], o[12], 1'b0, rst, 1'b1);
  dff u13(i[13], o[13], 1'b1, rst, 1'b0);
  dff u14(i[14], o[14], 1'b1, rst, 1'b1);
  dff u15(i[15], o[15], 1'b0, 1'b0, pre);
  dff u16(i[16], o[16], 1'b0, 1'b1, pre);
  dff u17(i[17], o[17], 1'b1, 1'b0, pre);
  dff u18(i[18], o[18], 1'b1, 1'b1, pre);
  dff u19(i[19], o[19], 1'b0, 1'b0, 1'b0);
  dff u20(i[20], o[20], 1'b0, 1'b1, 1'b0);
  dff u21(i[21], o[21], 1'b1, 1'b0, 1'b0);
  dff u22(i[22], o[22], 1'b1, 1'b1, 1'b0);
  dff u23(i[23], o[23], 1'b0, 1'b0, 1'b1);
  dff u24(i[24], o[24], 1'b0, 1'b1, 1'b1);
  dff u25(i[25], o[25], 1'b1, 1'b0, 1'b1);
  dff u26(i[26], o[26], 1'b1, 1'b1, 1'b1);
endmodule

module dff(i, o, clk, rst, pre);
  input i, clk, rst, pre;
  output o;

  reg    o;

  initial o = 1'b0;

`ifdef CARBON
  always @(posedge clk or posedge rst or posedge pre)
`else
  reg prev_clk;
  initial prev_clk = 1'b0;     // vhdl-style reset/preset-flop helps replicate
  always @(clk or rst or pre)  // HW behavior on negedge rst with pre==1
`endif
  begin
    if (rst)
      o <= 1'b0;
    else if (pre)
      o <= 1'b1;
`ifdef CARBON
    else 
      o <= i;
`else
    else if ((prev_clk != clk) && clk)
      o <= i;
    prev_clk <= clk;
`endif
  end
endmodule

