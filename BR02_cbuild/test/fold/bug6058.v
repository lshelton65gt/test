module top(enable, in21, tmp22);
   input  [20:0] in21;
   input         enable;
   output [21:0]   tmp22;
   assign tmp22 = enable ? (~in21 + 1) : in21;
endmodule

