// test findone/findzero via case statements.
module findonestmt(i,o,k,m,n,c,c2);
   input [7:0] i;
   output [3:0] o;
   output [3:0] k,m,n;
   input        c,c2;

   reg [3:0]    rk, ro,rm,rn;
   assign       o = ro;
   assign       k = rk;
   
always @(negedge c)
  begin
     casex (i)
       8'b0xxxxxxx: ro=7;        // FLZ
       8'bx0xxxxxx: ro=6;
       8'bxx0xxxxx: ro=5;
       8'bxxx0xxxx: ro=4;
       8'bxxxx0xxx: ro=3;
       8'bxxxxx0xx: ro=2;
       8'bxxxxxx0x: ro=1;
       8'bxxxxxxx0: ro=0;
       default: ro=15;
     endcase

     casex(i)                   // FFO
       8'bxxxxxxx1: rk=0;
       8'bxxxxxx1x: rk=1;
       8'bxxxxx1xx: rk=2;
       8'bxxxx1xxx: rk=3;
       8'bxxx1xxxx: rk=4;
       8'bxx1xxxxx: rk=5;
       8'bx1xxxxxx: rk=6;
       8'b1xxxxxxx: rk=7;
       default: rk=15;
     endcase
     
  end

always @(posedge c)
  begin
     casex (i)
       8'b1xxxxxxx: ro=7;        // FLO
       8'bx1xxxxxx: ro=6;
       8'bxx1xxxxx: ro=5;
       8'bxxx1xxxx: ro=4;
       8'bxxxx1xxx: ro=3;
       8'bxxxxx1xx: ro=2;
       8'bxxxxxx1x: ro=1;
       8'bxxxxxxx1: ro=0;
       default: ro=15;
     endcase

     casex(i)                   // FFZ
       8'bxxxxxxx0: rk=0;
       8'bxxxxxx0x: rk=1;
       8'bxxxxx0xx: rk=2;
       8'bxxxx0xxx: rk=3;
       8'bxxx0xxxx: rk=4;
       8'bxx0xxxxx: rk=5;
       8'bx0xxxxxx: rk=6;
       8'b0xxxxxxx: rk=7;
       default: rk=15;
     endcase
     
  end
   
always @(posedge c2)
  begin   
     casex(i)
       8'b0xxxxxxx: ro=7;       // FLZ - note no default!
       8'bx0xxxxxx: ro=6;
       8'bxx0xxxxx: ro=5;
       8'bxxx0xxxx: ro=4;
       8'bxxxx0xxx: ro=3;
       8'bxxxxx0xx: ro=2;
       8'bxxxxxx0x: ro=1;
       8'bxxxxxxx0: ro=0;
     endcase

     casex(i)                   // FFO - note no default!
       8'bxxxxxxx1: rk=0;
       8'bxxxxxx1x: rk=1;
       8'bxxxxx1xx: rk=2;
       8'bxxxx1xxx: rk=3;
       8'bxxx1xxxx: rk=4;
       8'bxx1xxxxx: rk=5;
       8'bx1xxxxxx: rk=6;
       8'b1xxxxxxx: rk=7;
     endcase
  end

// partially specified or explicit default
always @(negedge c2)
  begin
     casex(i)
       8'b0xxxxxxx: ro=7;       // FLZ - note no default!
       8'bx0xxxxxx: ro=6;       // AND partially specified
       8'bxx0xxxxx: ro=5;
       8'bxxx0xxxx: ro=4;
     endcase

     casex(i)                   // FFO - note empty "default"
       8'bxxxxxxx1: rk=0;
       8'bxxxxxx1x: rk=1;
       8'bxxxxx1xx: rk=2;
       8'bxxxx1xxx: rk=3;
       8'bxxx1xxxx: rk=4;
       8'bxx1xxxxx: rk=5;
       8'bx1xxxxxx: rk=6;
       8'b1xxxxxxx: rk=7;
       8'b00000000: rk=-1;      // Should convert to case with -1 alternative?
     endcase
  end

   always @(posedge c) begin    // bug5515
      casex(i)
        8'b1xxxxxxx: rm = 8;   // FLO(i)+1
        8'b01xxxxxx: rm = 7;
        8'b001xxxxx: rm = 6;
        8'b0001xxxx: rm = 5;
        8'b00001xxx: rm = 4;
        8'b000001xx: rm = 3;
        8'b0000001x: rm = 2;
        8'b00000001: rm = 1;
        default: rm = 0;
      endcase
   end
   always @(negedge c) begin
      casex(i)
        8'b00000001: rm = 0;   // FFO
        8'b0000001x: rm = 1;
        8'b000001xx: rm = 2;
        8'b00001xxx: rm = 3;
        8'b0001xxxx: rm = 4;
        8'b001xxxxx: rm = 5;
        8'b01xxxxxx: rm = 6;
        8'b1xxxxxxx: rm = 7;
        default: rm = 4'b1111;
      endcase
   end
   always @(posedge c) begin
      casex(i)
        8'b0xxxxxxx: rn = 7;   // FL0
        8'b10xxxxxx: rn = 6;
        8'b110xxxxx: rn = 5;
        8'b1110xxxx: rn = 4;
        8'b11110xxx: rn = 3;
        8'b111110xx: rn = 2;
        8'b1111110x: rn = 1;
        8'b11111110: rn = 0;
        default: rn = 4'b1111;
      endcase
   end
   always @(negedge c) begin
      casex(i)                  // FFZ
        8'b11111110: rn = 0;
        8'b1111110x: rn = 1;
        8'b111110xx: rn = 2;
        8'b11110xxx: rn = 3;
        8'b1110xxxx: rn = 4;
        8'b110xxxxx: rn = 5;
        8'b10xxxxxx: rn = 6;
        8'b0xxxxxxx: rn = 7;
        default: rn = 4'b1111;
      endcase
   end

   assign n = rn;
   assign m = rm;
   
endmodule
