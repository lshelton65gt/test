// Some tests of signed expression folding Verilog 2001
module top(a,i,j,o);
   input a;
   input signed [7:0] i;
   input signed [7:0] j;

   output [10:0] o;

   assign       o[0] = a ? 1: 0;  // bug 4501
   assign       o[1] = a ? 0 : 1; // try some variations too
   assign       o[2] = ($signed(a) + i) < 0; // Want signed arithmetic for this
   assign       o[3] = $signed(a ? a: !a) < 0; // Should always be true
   assign       o[4] = ($signed (a) & 1) != -1;
   assign       o[5] = ^(i[3:0] + j[3:0]); // Should NOT BE sign-extended, do 4 bit computations
   assign       o[6] = i > $signed(1);
   assign       o[7] = i > $signed(i==j);
   assign       o[8] = ((a? $unsigned(i) : $unsigned(j)) & 511) > 255; // Should be false
   assign       o[9] = (~i) ? 1'b1 : 1'b0;

   wire         signed [65:0] wide;
   assign       wide = a;
   assign       o[10] = (~wide) ? i[0] : j[0];
endmodule
