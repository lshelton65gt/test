// test boolean idents
module boolident (a,b,c);
   input [3:0] a,b;
   output [23:0] c;

   assign        c[0+:4] = a &(a|b);
   assign        c[4+:4] = a | (a&b);
   assign        c[8+:4] = a | (a^b);
   assign        c[12+:4] = a & (a^b);
   assign        c[16+:4] = a ^(a|b);
   assign        c[20+:4] = a ^(a&b);
endmodule
