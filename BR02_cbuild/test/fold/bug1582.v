module mem_crash(clk,rst,a,b,x,y,z);
   input clk;
   input rst;
   input a,b;
   output x,y,z;
   reg    x,y,z;
   wire   sel1 = 1'b0;
   wire   sel2 = 1'b0;
   wire   a_ = ~a;
   wire   b_ = ~b;
   always @(posedge clk  or negedge rst)
      if((!rst )) begin
         x <= 1'b0;
         y <= 1'b0;
         z <= 1'b0;
      end else begin
         if(sel1) begin
         end else begin
            if(sel2) begin
               x <= a_;
               y <= b_;
            end else begin
	       if (!sel1)
		 begin : inner
		    x <= b_;
		 end
	       
	       y <= a_;
            end
            z <= a_ | b;
         end
      end
endmodule
