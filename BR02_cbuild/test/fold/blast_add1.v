// Generated with: ./rangen -s 158 -a -n 0x3b98f006 -o /work/aron/build/bug6517/obj/Linux.product/test/rangen/rangen/testcases.082806/cbld/segfault1.v
// Using seed: 0x3b98f006
// Writing acyclic circuit of size 158 to file '/work/aron/build/bug6517/obj/Linux.product/test/rangen/rangen/testcases.082806/cbld/segfault1.v'
// Module count = 8

// module JyOB
// size = 9 with 2 ports
module JyOB(XsY, dbc);
output XsY;
input dbc;

reg XsY;
wire [8:16] dbc;
reg [17:115] XlG;
reg [5:6] uZoh;
wire [1:1] j;
reg eHc;
wire [9:15] BZL;
reg dR;
Cyg Vfq ({j[1],j[1],BZL[11],j[1],BZL[11],BZL[11]}, dbc[11], {dbc[11],dbc[11],dbc[11]}, /* unconnected */, {dbc[11],dbc[10:11],dbc[10:11],dbc[11],dbc[11],dbc[10:11],dbc[10:11],dbc[10:11],dbc[10:11],dbc[11]}, /* unconnected */, {dbc[11],dbc[11],dbc[11],dbc[10:11]});

aD qjGW (j[1], {j[1],dbc[11],j[1],j[1],j[1]});

aD YmxKm (j[1], {j[1],dbc[11],BZL[11],BZL[11],BZL[11]});

aD fA (BZL[11], {BZL[11],j[1],dbc[11],dbc[11],j[1]});

endmodule

// module Cyg
// size = 60 with 7 ports
module Cyg(SOEUL, qbH, Gi, XtA, iBT, Fd, b);
output SOEUL;
input qbH;
input Gi;
inout XtA;
input iBT;
inout Fd;
input b;

reg [1:6] SOEUL;
wire qbH;
wire [6:8] Gi;
wire [30:87] XtA;
wire [9:24] iBT;
wire [1:4] Fd;
wire [12:16] b;
reg [9:30] NBt;
reg [3:3] frrR;
wire [1:6] afsB;
reg [6:7] dZUu;
reg [30:83] Ejr;
reg [5:6] EVt;
wire [12:19] LYbsO;
reg PlX;
reg U;
reg [24:27] oQYX;
reg [20:24] f;
wire [13:13] Iyqh;
reg [1:3] ypy;
reg br;
reg [3:7] ut;
reg HYA;
reg lFbn;
reg [1:23] TiVsl;
reg [2:3] CGMGv;
wire pXd;
reg t_u;
reg [15:26] VjC;
reg [14:87] g;
reg [0:3] so;
reg [21:23] qKxaO;
wire [0:7] eH;
reg [19:21] R_WN;
wire as;
reg [2:4] jzPM;
reg [15:29] NWDc;
reg [5:22] jhhUj;
reg [0:3] P;
wire Ze;
wire [1:5] pHCjg;
wire [6:24] jaOau;
wire D;
wire [3:4] euHEr;
reg [6:6] JBLP;
wire [21:48] zFdPZ;
reg [10:17] EGtCt;
reg [48:84] SGb;
reg [23:26] yKI;
reg [2:4] EkV [0:6]; // memory
reg [7:14] KWG;
always @(qbH)
if (qbH)
begin
br = {Gi[7:8],XtA[31:52]};
g[41:79] = (Fd[2:4]);
end

assign Fd[1:3] = ~((((~g[41:79]))));

assign zFdPZ[22:34] = b[13:14];

assign zFdPZ[22:34] = Fd[2:4]^Fd[2:4];

rvdz InqQe ({Fd[2:4],br,Gi[7:8],Fd[2:4],qbH,iBT[14:21],b[13:14],br,4'b1111}, {Fd[2:4],qbH,iBT[14:21],qbH,qbH,Fd[2:4],br,10'b1110100011}, {qbH,qbH,b[13:14],br}, br);

always @(negedge &g[41:79])
begin
begin
br = ((~qbH));
end
end

assign LYbsO[13:19] = ~b[13:14];

rvdz MS ({br,Fd[2:4],br,Gi[7:8],br,Fd[2:4],b[13:14],br,11'b01110101111}, {br,XtA[31:52],br,Fd[2:4],qbH}, {Gi[7:8],br,br,1'b1}, br);

always @(negedge qbH)
begin
begin
frrR[3] = ~b[13:14];
TiVsl[9:11] = ~9'b100100110;
end
end

always @(negedge frrR[3])
begin :qtVV
reg [3:76] IqxtU;
reg [7:17] Q;
reg [2:25] tS [5:6]; // memory
reg [10:16] RKNTB [23:27]; // memory

begin
PlX = 1'b0;
qKxaO[21:22] = (~iBT[14:21]);
end
end

always @(negedge &XtA[31:52])
begin
begin
f[21:24] = ~Fd[2:4];
end
end

aD gJLgZ (Iyqh[13], {frrR[3],f[21:24]});

always @(negedge &Fd[2:4])
begin
begin
SGb[56:60] = ~qKxaO[21:22]|br;
end
end

always @(negedge &f[21:24])
begin :rERoU
reg vnXVr;
reg AVyFB;
reg [2:6] yoDz;

begin
end
end

always @(qbH)
if (qbH)
begin
f[21:24] = TiVsl[9:11];
end
else
begin
end

always @(posedge &Gi[7:8])
begin
begin
if (iBT[14:21] == 8'b01001000)
begin
if (b[13:14] == 2'b11)
begin
lFbn = ~qbH;
end
else
begin
oQYX[24:25] = TiVsl[9:11]&Iyqh[13];
SOEUL[6] = ~frrR[3];
end
end
end
end

assign XtA[57:69] = ~SOEUL[6]|PlX;

aD X (D, {oQYX[24:25],Fd[2:4]});

always @(&SGb[56:60])
if (SGb[56:60])
begin
NBt[25:27] = Gi[7:8];
end

always @(posedge &NBt[25:27])
begin
begin
VjC[15] = ~(NBt[25:27]^Iyqh[13]);
end
end

always @(negedge &iBT[14:21])
begin
begin
end
end

always @(posedge &oQYX[24:25])
begin
begin
jhhUj[15:19] = br;
end
end

always @(negedge &b[13:14])
begin
begin
lFbn = ~lFbn;
end
end

assign Fd[1:3] = ~(~(g[41:79]));

always @(negedge lFbn)
begin :JQyb
reg [14:29] An;
reg tY;

begin
end
end

always @(posedge br)
begin
begin
TiVsl[9:11] = 9'b110101010;
end
end

assign LYbsO[13:19] = 9'b110000000;

always @(frrR[3])
if (~frrR[3])
begin
g[41:79] = 4'b1011;
end
else
begin
end

always @(negedge VjC[15])
begin
begin
Ejr[70:81] = ~(br&XtA[31:52]);
end
end

always @(negedge br)
begin :PzR
reg IMnt;
reg [28:29] M;

begin
VjC[15] = ~g[41:79];
end
end

assign eH[0] = br;

assign pXd = ~(jhhUj[15:19]&b[13:14]);

always @(posedge &b[13:14])
begin :Ij
reg [6:10] maCs;
reg IlpBD;

begin
EVt[5:6] = ~(~g[41:79]);
end
end

aD SF (pXd, SGb[56:60]);

always @(negedge &TiVsl[9:11])
begin
begin
jzPM[3] = ~3'b001;
end
end

assign as = Ejr[70:81];

always @(posedge &SGb[56:60])
begin
begin
Ejr[38:74] = ~(~(~pXd|jhhUj[15:19]) + ({br,lFbn}));
end
end

always @(&f[21:24])
if (f[21:24])
begin
Ejr[38:74] = ~g[41:79];
end
else
begin
br = XtA[31:52];
end

qMb v ({LYbsO[13:19],as,D,zFdPZ[24:39],pXd,Ze,pXd,pXd,eH[0],eH[1:3],XtA[60:84],LYbsO[12:18],LYbsO[13:19],LYbsO[13:19],Fd[1:3]}, {SOEUL[6],frrR[3],lFbn,Gi[7:8],VjC[15],qbH,qKxaO[21:22],jhhUj[15:19],VjC[15],11'b10110110111}, {VjC[15],frrR[3],pXd,br});

assign LYbsO[12:18] = ((br|LYbsO[12:18])) + (Fd[2:4]|pXd);

always @(negedge as)
begin
begin
if (Fd[2:4] == 3'b011)
begin
NBt[11:13] = Iyqh[13]|D;
end
end
end

always @(negedge eH[0])
begin :mWmGv
reg [0:3] DUX;
reg [6:25] pql;
reg [0:1] L;
reg [5:7] BY [3:7]; // memory
reg [17:19] Mfu;

begin
NBt[16:28] = TiVsl[9:11];
ypy[1] = ~SOEUL[6];
end
end

always @(posedge &g[41:79])
begin
begin
g[41:79] = ~{PlX,PlX};
end
end

always @(posedge br)
begin
begin
ypy[1] = ~pXd^NBt[25:27];
$display("module: %s, value: %b",jhhUj[15:19])
;end
end

endmodule

// module aD
// size = 0 with 2 ports
module aD(KdVz, OC);
output KdVz;
input OC;

reg KdVz;
wire [68:72] OC;
wire x;
endmodule

// module rvdz
// size = 55 with 4 ports
module rvdz(jcj, lCijq, AFZ, bzxXN);
input jcj;
input lCijq;
input AFZ;
input bzxXN;

wire [93:117] jcj;
wire [2:29] lCijq;
wire [21:25] AFZ;
wire bzxXN;
reg Qrh;
wire [0:24] ryvY;
reg [0:5] jl;
wire [4:9] csZ;
reg [3:5] V;
reg [4:7] U_As_;
wire [7:14] XzTR;
reg [9:31] _Kj;
wire [30:94] gzIFS;
reg A;
reg [4:6] XFM;
reg [1:6] FWLCh;
wire [10:105] yIY_g;
wire [12:19] jOvRk;
reg yyBJ;
reg tq;
reg g;
reg [3:5] UPT;
wire [3:4] ypHo;
reg _rLg;
wire [1:2] rOCm;
reg QHC;
wire [3:5] CIFt;
reg xNq;
reg [3:7] T;
wire [1:7] s;
reg [5:30] Ft;
reg [2:3] M;
reg sT;
reg [49:99] truPX;
reg [5:6] kYy;
wire [21:22] u;
reg [4:5] LSsZ;
wire [5:48] YIrRO;
reg crrn;
reg [1:3] itzz;
reg BLZ;
reg P_PnV;
reg [4:6] N;
reg [3:6] LanlD;
reg [4:6] ypj;
reg L;
wire tUIv;
reg [0:106] K;
Pe Oo ({tUIv,ypHo[3],ryvY[13:18],XzTR[10:13],XzTR[10:13]}, AFZ[21:25], lCijq[28], bzxXN);

assign tUIv = {ryvY[13:18],lCijq[28]};

assign ryvY[4:14] = ~jcj[107:109];

always @(negedge &XzTR[10:13])
begin
begin
end
end

wire nIo = tUIv;
always @(negedge lCijq[28] or negedge nIo)
if (~nIo)
begin
begin
jl[3:4] = ~XzTR[10:13]^bzxXN;
end
end
else
begin
begin
N[4] = XzTR[10:13];
end
end

Pe _Kmt ({XzTR[10:13],csZ[6:9],ryvY[13:18],tUIv,tUIv}, AFZ[21:25], N[4], N[4]);

always @(&ryvY[13:18])
if (~ryvY[13:18])
begin
LanlD[6] = lCijq[28];
end
else
begin
A = AFZ[21:25]^LanlD[6];
end

z_Zc Boa (tUIv, ypHo[3], {bzxXN,N[4],N[4],XzTR[10:13],ypHo[3],tUIv,tUIv});

always @(A)
if (A)
begin
kYy[6] = XzTR[10:13]^LanlD[6];
end
else
begin
K[12:41] = ~tUIv;
end

assign CIFt[3] = (jl[3:4] != 2'b11) ? A^A : 1'bz;

assign XzTR[10:13] = (ryvY[13:18] != 6'b111011) ? ~(~tUIv) + (lCijq[28]) : 4'bz;

always @(negedge &jl[3:4])
begin :Gr
reg [23:24] bJNOU [27:30]; // memory
reg [12:27] WY;
reg [1:5] ms;
reg Bn;

begin
for (XFM[4]=1'b0; XFM[4]>1'b0; XFM[4]=XFM[4]-1'b0)
begin
QHC = ~tUIv;
end
end
end

Pe LpXYd ({s[4:5],tUIv,s[4:5],CIFt[3],ypHo[3],gzIFS[73:79],rOCm[1:2]}, AFZ[21:25], ypHo[3], N[4]);

always @(negedge ypHo[3])
begin :lcCQ
reg [1:6] Xi;
reg [1:2] oIaT;

begin
FWLCh[3:5] = 4'b0010;
end
end

wire oosV = &FWLCh[3:5];
always @(negedge tUIv or posedge oosV)
if (oosV)
begin
begin
U_As_[4:6] = (~lCijq[28]^rOCm[1:2]);
end
end
else
begin
begin
g = ~(~{kYy[6],CIFt[3]});
end
end

always @(negedge ypHo[3])
begin
begin
end
end

always @(negedge &U_As_[4:6])
begin
begin
_Kj[22:26] = ryvY[13:18];
end
end

qMb _S ({yIY_g[65:93],ryvY[4:14],rOCm[1:2],ryvY[13:18],csZ[6:9],gzIFS[73:79],ypHo[3],gzIFS[73:79],csZ[6:9],tUIv,gzIFS[73:79],rOCm[1:2],ypHo[3]}, {LanlD[6],XzTR[10:13],tUIv,FWLCh[3:5],ypHo[3],kYy[6],tUIv,tUIv,s[4:5],csZ[6:9],7'b1101011}, {tUIv,s[4:5],lCijq[28]});

z_Zc gV (u[22], CIFt[3], {tUIv,tUIv,gzIFS[73:79],ypHo[3]});

always @(posedge tUIv)
begin
begin
truPX[80:94] = ~FWLCh[3:5];
end
end

always @(posedge tUIv)
begin
begin
g = ~N[4]^A;
g = (~ypHo[3]) + (~ypHo[3]);
end
end

endmodule

// module Pe
// size = 4 with 4 ports
module Pe(IA, OaY_, HZ, YWCo);
output IA;
input OaY_;
input HZ;
input YWCo;

reg [9:24] IA;
wire [2:6] OaY_;
wire HZ;
wire YWCo;
reg [10:30] kxarV;
reg J;
reg [5:7] O;
wire [3:5] OeS;
assign OeS[5] = (HZ != 1'b1) ? HZ : 1'bz;

always @(posedge HZ)
begin
begin
if (HZ == 1'b0)
begin
J = YWCo;
end
else
begin
O[5:6] = ~(8'b01100110);
end
end
end

wire Uu = J;
always @(negedge &OaY_[2:4] or negedge Uu)
if (~Uu)
begin
begin
J = YWCo|O[5:6];
end
end
else
begin
begin
IA[22:24] = J;
if (J == 1'b0)
begin
IA[22:24] = J^IA[22:24];
end
end
end

endmodule

// module z_Zc
// size = 1 with 3 ports
module z_Zc(xsD, pcyNl, Gh);
output xsD;
output pcyNl;
input Gh;

reg xsD;
reg pcyNl;
wire [6:15] Gh;
reg [8:24] YAkiY;
reg WKo;
reg [2:2] vHAqq;
reg [9:26] I;
wire [3:4] qlf;
wire eX = &Gh[9:14];
always @(negedge &Gh[7:14] or posedge eX)
if (eX)
begin
begin
YAkiY[11:24] = (~(Gh[7:14]));
end
end
else
begin
begin
YAkiY[11:24] = YAkiY[11:24]&Gh[7:14];
I[14:25] = ~YAkiY[11:24];
end
end

endmodule

// module qMb
// size = 2 with 3 ports
module qMb(b, py, q);
output b;
input py;
input q;

reg [40:121] b;
wire [3:28] py;
wire [2:5] q;
reg [3:7] Xlv;
wire [0:7] ZFt;
always @(posedge &py[13:14])
begin
begin
end
end

always @(&py[7:10])
if (py[7:10])
begin
end
else
begin
Xlv[4:6] = ~py[13:14];
end

always @(negedge &q[3:4])
begin :N
reg lI;
reg [57:98] UEO [10:79]; // memory

begin
end
end

always @(&q[3:4])
if (q[3:4])
begin
b[87:103] = q[3:4];
end

endmodule
