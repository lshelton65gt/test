// A version of bug7300.v, modified to test concats. Fold used to incorrectly
// fold {A,B} <= {A,B} statement to  a null statement resulting in
// incorrect simulation. This has been fixed now.
module bug7300_concat(clk, reset, in1, in2, out1, out2, outerr);
  input clk, reset;
  input [3:0] in1;
  input [3:0] in2;
  output [7:0] out1;
  output [7:0] out2;
  output [7:0] outerr;

  integer      i;

  reg [7:0]    sig1;
  reg [7:0]    sig2;
  reg [7:0]    err;

  assign       out1 = sig1;
  assign       out2 = sig2;
  assign       outerr = err;

  initial begin
    sig1 = 8'h0;
    sig2 = 8'h0;
  end

  always @(posedge clk) begin
    if (reset) begin
      sig1 <= 8'had;
    end
    else begin
      for (i = 0; i < 3; i=i+1) begin
        if (in1[i] == 1) begin
          {sig1[i],sig1[i+4]} <= {sig1[i+1],sig1[i+4+1]};
          if (in2[i] == 1) begin
            {sig1[i],sig1[i+4]} <= {sig1[0],sig1[4]};
          end
        end
      end
    end
  end

  always @(posedge clk) begin
    if (reset) begin
      sig2 <= 8'had;
    end
    else begin
      for (i = 0; i < 3; i=i+1) begin
        if (in1[i] == 1) begin
          if (in2[i] == 0)
            {sig2[i],sig2[i+4]} <= {sig2[i+1],sig2[i+4+1]};
          else
            {sig2[i],sig2[i+4]} <= {sig2[0],sig2[4]};
        end
      end
    end
  end

  always @(posedge clk) begin
    for (i = 0; i < 8; i=i+1) begin
      if (sig1[i] == sig2[i])
        err[i] <= 0;
      else
        err[i] <= 1;
    end
  end

endmodule
