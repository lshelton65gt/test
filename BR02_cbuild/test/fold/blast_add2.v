module top(out1, out0, in1, in2, ena);
  output out1, out0;
  input        in1, in2, ena;

  assign       {out1, out0} = ena ? (in1 + in2) : 2'bzz;
endmodule
