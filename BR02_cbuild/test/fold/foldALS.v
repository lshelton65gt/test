// test simplifications of arithmetic left shifts.

module top(a,b,c,o,z,p,q,w,x,ws);
   input signed [31:0] a;
   input signed [31:0] b;
   input signed [31:0]c;
   output signed [31:0] o;
   wire signed [31:0] 	 ta;
   wire signed [31:0] 	 tb;
   wire signed [31:0] 	 tc;
   output z,p,q;
   output [7:0]  w;
   
   shifter s1(a, 8'd32, ta);		// should always be zero
   shifter s2(b, 8'd31, tb);		// one-bit of significance
   shifter s3(c, 8'd0, tc);		// should optimize away shift completely   
   wire signed [63:0] 	 bigger;
   assign 	 bigger = a <<< 32; // Don't optimize this one
   
   assign 	 o = ta ^ tb ^ tc ^ bigger[31:0] ^ bigger[63:32];

   reg           signed ones=1'd1;
   
   assign 	 z = 0 <<< a;   // 0 <<< k

   assign 	 p = (a[0] <<< 99) + 100'b0; 	 // a[0] ? 100'b100...:100'b0
   assign 	 q = (a <<< 32) + 1;  // should always be 1

   // test corrected bitsizes
   output [3:0]  x;
   assign        x[0] = (a[7:0] <<< 4'd8) < 17'h10000;
   assign        x[1] = ($signed(a[3:0]) <<< b[1:0]) < 128;

   assign        w = (ones <<< a);

   output [31:0] ws;
   assign        ws = {a,b,c} <<<  a; // Poke at resizing AND effectiveBitSize() problems
endmodule

module shifter(v,c,r);
   input signed [31:0] v;
   input [7:0] 	c;
   output signed [31:0] r;

   assign 	 r = v <<< c;
endmodule
