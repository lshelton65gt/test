// Reproduces bug7300. Fold used to incorrectly fold A <= A statement to 
// a null statement resulting in incorrect simulation. This has been
// fixed now.
module bug7300(clk, reset, in1, in2, out1, out2, outerr);
  input clk, reset;
  input [3:0] in1;
  input [3:0] in2;
  output [3:0] out1;
  output [3:0] out2;
  output [3:0] outerr;

  integer      i;

  reg [3:0]    sig1;
  reg [3:0]    sig2;
  reg [3:0]    err;

  assign       out1 = sig1;
  assign       out2 = sig2;
  assign       outerr = err;

  initial begin
    sig1 = 4'h0;
    sig2 = 4'h0;
  end

  always @(posedge clk) begin
    if (reset) begin
      sig1 <= 4'ha;
    end
    else begin
      for (i = 0; i < 3; i=i+1) begin
        if (in1[i] == 1) begin
          sig1[i] <= sig1[i+1];
          if (in2[i] == 1) begin
            sig1[i] <= sig1[0];
          end
        end
      end
    end
  end

  always @(posedge clk) begin
    if (reset) begin
      sig2 <= 4'ha;
    end
    else begin
      for (i = 0; i < 3; i=i+1) begin
        if (in1[i] == 1) begin
          if (in2[i] == 0)
            sig2[i] <= sig2[i+1];
          else
            sig2[i] <= sig2[0];
        end
      end
    end
  end

  always @(posedge clk) begin
    for (i = 0; i < 4; i=i+1) begin
      if (sig1[i] == sig2[i])
        err[i] <= 0;
      else
        err[i] <= 1;
    end
  end

endmodule
