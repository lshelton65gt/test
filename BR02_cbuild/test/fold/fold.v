// test constant folding..

module iftest(val,o);
   output o;
   input  val;
   wire	  a=0;
   reg    b;

   always @(val)
     if(a==0)
       begin
       b = val;			// Fold away else clause
       end
     else
       begin
       b = ~val;
       end

   assign o = b;
endmodule

module ifnot(val,o);
   output o;
   input  val;
   wire	  a=0;
   reg    b;

   always @(val)
     if(a!=0)
       b = val;			// Fold away then clause
     else
       b = ~val;

   assign o = b;
endmodule

module caseconst(val,o);
   output o;
   input  val;
   wire [2:0]  a=3;
   reg 	  b;
   assign o=b;

   always @(val)
     case(a)			// Fold away all but one action
       3'b011: b = val;
       3'b001: b = 0;
       3'b010: b = 1;
       default: b = !val;
     endcase // case(val)
endmodule

module notinvert(a,b,o);
   output o;
   input  a,b;

   assign o= !(a==b);		// Should xform to !=
endmodule // notinvert

module notrelop(a,b,o);
   output o;
   input  a,b;

   assign o = !(a>b);
endmodule // notrelop



module popcount(i,o);
   input [0:31] i;
   output [5:0] o;

   assign o = i[0] +i[1] +i[2] +i[3] +i[4] +i[5] +i[6] +i[7] +i[8] +i[9]
     +i[10] +i[11] +i[12] +i[13] +i[14] +i[15] +i[16] +i[17] +i[18] +i[19]
     +i[20] +i[21] +i[22] +i[23] +i[24] +i[25] +i[26] +i[27] +i[28] +i[29]
     +i[30] +i[31];
endmodule // popcount

module condsimp(c,i,o);
   input [31:0] i;
   input 	c;
   output [1:0] o;
   
   reg [1:0] 	r;
   assign 	o = r;
   
   always @(posedge c)
     begin
	if (!i[0])
	  r = 1;
	else
	  r = 2;
     end

   always @(negedge c)
     begin
	if (i != 5)
	  r = 0;
	else
	  r = 3;
     end
endmodule // condsimp

module ifcase(c, i, o);
   input [31:0] i;
   input 	c;
   output 	o;

   reg [31:0] 	x;

   assign 	o = ^x;

   always @(posedge c)
     begin
	if (i[7:0]==0)
	  x = 1;
	else if (i[7:0] ==2)
	  x = 3;
	else if (i[7:0] == 3 || i[7:0] == 4 || i[7:0] == 0) // dup
	  x = 7;
	else if (i[7:0])	// warn of duplicate here
	  x = 8;
	else
	  x = 15;
     end // always @ (posedge c)

   // Don't case-translate this one
   always @(negedge c)
     begin
	if (i[7:0]==0)
	  x = 1;
	else if (i[7:0] ==2 || i[7:0] == 2) // warn of duplicate here too
	  x = 3;
	else if (i[7:0] == 3 || i[15:0] == 4) // Not identical NUNetRef!
	  x = 7;
	else
	  x = 15;
     end // always @ (posedge c)
endmodule // ifcase

module foldcase(c,i,j);
   input c, i;
   output j;
   reg 	  r,s;
   assign j = r|s;
   
   always @(posedge c)
     begin
	case (i)
	  1'b0: r=i;
	  1'b1: r=i;
	  default: r=i;
	endcase
     end

   always @(negedge c)
     begin
	case (i+1)		// Don't fold this one
	  1'b0: s=0;		
	  1'b1: s=i;		// combine these two case actions?
	  default: s=i;
	endcase
     end
endmodule // foldcase

module bigrelationals(val, ccs);
   output [3:0] ccs;
   input [156:0] val;

   // Fold was punting compares with things larger than 64 bits.  
   assign 	 ccs[0] = (val > 10);
   assign        ccs[1] = (val >= 10);
   assign        ccs[2] = (val < -10);
   assign        ccs[3] = (val <= -10);
endmodule

module top(o,i,j, v);
   output [26:0] o;
   input [31:0]  v;
   input 	 i,j;
   wire 	 w=1;
   iftest a(i, o[0]);
   ifnot b(i, o[1]);
   caseconst c(i,o[2]);
   notinvert d(i, j, o[3]);
   notrelop e(i,j, o[4]);
   popcount f(v, o[10:5]);
   condsimp g(i,v, o[12:11]);
   ifcase h(i,v, o[13]);
   foldcase k(i,j, o[14]);

   bigrelationals br1(157'h7, o[22:19]);
   bigrelationals br2(-157'd10, o[26:23]);
   
   assign 	 o[15] = (w)? 1: 0;
   assign 	 o[16] = (!w) ? i: j;
   assign 	 o[17] = (~v + 1) == -v;

   wire 	 [1:0] clkvec = v[0] ? {i,j}: {~i,~j};
   wire 	 myclk = clkvec[0];

   reg 		 r;
   initial r=0;
   
   always @(posedge myclk)
     begin : labeled
     r = 1;
     end
   
   assign 	 o[18] = r;
endmodule // top


