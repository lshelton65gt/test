// filename: test/fold/bug16943_09 .v
// Description:  This test checks the FFO (find first one) detection for ascending values
// see also bug16943_03.v 
// expected output: status == 1

module bug16943_09(clk, i,o1, o2, status);
   input        clk;
   input [7:0] 	i;
   output [3:0] o1, o2;
   output 	status;

   reg [3:0] 	temp_1, temp_2;

   assign       o1 = temp_1;
   assign       o2 = temp_2;
   assign status = ( temp_1 == temp_2);
   
   always @(negedge clk)
     begin
	casex (i)
	  8'bxxxxxxx1: temp_1=6;
	  8'bxxxxxx1x: temp_1=5;
	  8'bxxxxx1xx: temp_1=4;
	  8'bxxxx1xxx: temp_1=3;
	  8'bxxx1xxxx: temp_1=2;
	  8'bxx1xxxxx: temp_1=1;
	  8'bx1xxxxxx: temp_1=0;
	  default:     temp_1=7;
	endcase

	temp_2 = ( i[0] == 1 ) ? 3'd6 :
		 ( i[1] == 1 ) ? 3'd5 :
		 ( i[2] == 1 ) ? 3'd4 :
		 ( i[3] == 1 ) ? 3'd3 :
		 ( i[4] == 1 ) ? 3'd2 :
		 ( i[5] == 1 ) ? 3'd1 :
		 ( i[6] == 1 ) ? 3'd0 : 3'd7;

     end
endmodule
