//
// Just like repeat99, but compile with -noConcatRewrite to make sure that
// LUT generation works correctly.
module top(i, o);
  output [197:0] o;
  input [31:0]  i;
  assign        o[197:0] = {99{i[1:0]}};
endmodule
