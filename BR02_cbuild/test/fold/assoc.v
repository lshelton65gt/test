// Test associative operators
module simple(i,j,k, x,y,z);
   input [7:0] i,j,k;
   output [7:0] x,y,z;

   assign       x = (i + 7 + (j + 8) + (-i) + k);
   assign       y = i & j & k & ~j;
   assign       z = i | j | k | ~i;
endmodule

module messy(i,j,k, o);
   input [7:0] i,j,k;
   output [7:0] o;

   assign       o = (k + j + i) + (-i) + (-j) + (-k);
endmodule


module top(a,b,c,o,p,q,f,g,h,j);
   input [7:0] a,b,c;
   output [31:0] o;
   output        p,q,f,g,h,j;   

   simple one(a,b,c,o[7:0], o[15:8], o[23:16]);
   messy two(a,b,c,o[31:24]);

   assign        p = ^(a + b + (~a) + (-b)); // ^(255 + 0)
   assign        q = ^(a ^ b ^ (!a) ^ (~a) ^ (!a)); // don't let !a confuse us
   assign        f = ^(a + a + b + (-a) + (-a) + (~b)); // should be ^(8'hff), aka 0
   assign        g = ^(a * 5 * b * (-a) * c * 10); // should be (a*b*c * (-a) * 50)
   assign        h = ^(( a & b & c & a & b & c) // should be ^(a&b&c + (a^b^c) + (a|b|c))
                       + (a ^ b ^ c ^ a ^ b ^ c)
                       + (a | b | c | a | b | c));
   assign        j = (~a[b-5'd5] & b[0] & (~(|c)) & (a < 8'd52)) ? 1'b1 : 1'b0;
endmodule
