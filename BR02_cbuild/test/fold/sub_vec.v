// Contrived testcase to show that - -> ^ conversion results in
// vectorizability

module top(out, a, b);
  input [7:0] a, b;
  output [7:0] out;

  reg [7:0]    out;
  integer      i;

  always @(a or b)
    for (i = 0; i < 8; i = i + 1)
      out[i] = a[i] - b[i];
endmodule

  