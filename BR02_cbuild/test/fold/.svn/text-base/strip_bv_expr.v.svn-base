module bv(out1, out2,  out3, out4, out5, out6, out7, out8, a128, b64, c32);
  output [127:0] out1, out2, out3, out4;
  output [31:0]  out5;
  output [63:0]  out6, out7, out8;
  input [127:0]  a128;
  input [63:0]   b64;
  input [31:0]   c32;

  // break this up into a concatenation 32-bit ^ operations.
  // ultimately, after concatRewrite processes it, we get:
  //   out1[127:96] = a128[127:96];
  //   out1[95:64] = a128[95:64]
  //   out1[63:32] = (a128[63:32] ^ b64[63:32]);
  //   out1[31:0] = ((a128[31:0] ^ b64[31:0]) ^ c32)
  //
  // These statements wind up getting put in different schedules
  // based on input sensitivities.
  assign         out1 = a128 ^ b64 ^ c32;

  // This one doesn't really get folded.  With new sizing, we get:
  //   out2 = (a128 - {64'h0000000000000000,b64});  // strip_bv_expr.v:8
  assign         out2 = a128 - b64;

  // This one gets split into 32-bit chunks, and the high 64 bits are
  // all 0, so we get:
  //   out3[127:64] = 64'h0000000000000000;
  //   out3[31:0] = (a128[31:0] & b64[31:0]);
  //   out3[63:32] = (a128[63:32] & b64[63:32]);
  // This is nice because the high 64-bits can be scheduled once at
  // the beginning of time, rather than copying 0s into all those bits
  // every cycle.
  assign         out3 = a128 & b64;

  // In this one, we first must stribute the ~ across the wide vector.
  // Then concatRewrite will split it up and we get to do the constant
  // assign once in the initial schedule:
  //    out4[127:96] = (~a128[127:96]);
  //    out4[95:0] = 96'h000000000000000000000000;
  assign	 out4 = ~{a128[127:96],96'hffffffffffffffffffffffff};

  // We do *not* want to distrubte the ~ in this case:
  assign         out5 = ~{a128[1:0],b64[1:0],c32[1:0],26'b0};

  // We *do* want to distribute the ~ in these cases:
  assign         out6 = ~{16'h0000, ~a128[47:0]};
  assign         out7 = ~{16'h0000, ~a128[15:0],b64[31:0]};

  // We do *not* want the ~ distributed here:
  assign         out8 = ~{16'h0000, a128[15:0],b64[31:0]};
endmodule

// Note, we run this with -noVectorInference to avoid having it recombine
// our 32-bit chunks as it sees fit.  We are surgically testing the
// FoldI::stripMineConcat, and don't want vectorization to undo that.
//
// fold on constants in concats still undoes part of this effort by
// recombining adjacent 32-bit constants into 64 bit constants, etc.
//
// It's OK in the product for that to happen because back-end strip-mining
// will make it all happen on 32-bit boundaries once again.  But I want
// to test this in isolation.
