// test case from mediatek that found a bug in the fold popcount
// optimization.
module test10( hclk_ck, preset_rstb, reg_max_adr);
   input hclk_ck, preset_rstb;
   output [7:0] reg_max_adr;
   wire         reg_chn_en, reg_dw32_en, reg_chn0_ban, reg_dual_rank_en;
   wire [1:0]   reg_chn0_col, reg_chn0_row;
   wire [5:0]   chn0_max_bit;
   reg [7:0]    chn0_max_adr;
   reg [7:0]    reg_max_adr;

   // Timer to wait 16 cycles
   reg [3:0]    timer;
   always @ (posedge hclk_ck or negedge preset_rstb)
     if (~preset_rstb)
       timer <= 4'h0;
     else
       timer = timer + 1;

   // Put the value in a flop that causes the bug. It was easier to do
   // this and use testdriver rather than make test/fold handle a C++
   // testbench. The problem value gets held for 16 cycles
   reg [31:0]   reg_cona;
   always @ (posedge hclk_ck or negedge preset_rstb)
     if (~preset_rstb)
       reg_cona <= 32'h1300133;
     else if (timer == 4'hF)
       reg_cona <= reg_cona + 1;

   assign       reg_chn_en = 1'b0;
   assign       reg_dw32_en = reg_cona[1];
   assign       reg_chn0_col = reg_cona[5:4];
   assign       reg_chn0_ban = reg_cona[8];
   assign       reg_chn0_row = reg_cona[13:12];
   assign       reg_dual_rank_en = reg_cona[17] & ~reg_chn_en;
   
   assign       chn0_max_bit = (reg_dual_rank_en) ? 
                                 ({5'b0, reg_dw32_en} + {4'b0, reg_chn0_col} + {5'b0, reg_chn0_ban} + {4'b0, reg_chn0_row} + 6'd24):
                                   ({5'b0, reg_dw32_en} + {4'b0, reg_chn0_col} + {5'b0, reg_chn0_ban} + {4'b0, reg_chn0_row} + 6'd23);
   
   always @(chn0_max_bit)
     begin
        case(chn0_max_bit)
          6'd27 : chn0_max_adr = 8'h8;
          6'd28 : chn0_max_adr = 8'h10;
        endcase
     end

   always @(posedge hclk_ck or negedge preset_rstb)
     begin
        if(~preset_rstb)
          reg_max_adr <= 8'h0;
        else
          reg_max_adr <= chn0_max_adr;
     end

endmodule

