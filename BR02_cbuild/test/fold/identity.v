// test zero identity folds
module zeroident(a,b,plus,minus,ander,orer, foldadd, otherids);
   input [15:0] a;
   input [15:0] b;

   output [16:0] plus;
   output [16:0] minus;

   output [15:0] ander;
   output [15:0] orer;
   output [16:0] foldadd;
   output [11:0]  otherids;

   assign 	 plus = a + b + 0; // Must do 17-bit or greater add
   assign 	 minus = a - b - 0;

   
   assign 	 orer = a | 0; // should not fold this one

   assign 	 ander= b & 16'hffff; // fold this one

   // We should fold this, but Cheetah doesn't return a 16'b0
   // when it parses this expression, it returns a 17'b0, and
   // we're dead since we think the constant wants to maintain
   // extra precision.
   
//   assign 	 foldadd = (a + b) + 16'b0; // and this one too
   assign 	 foldadd = (a + b) + 1'b0; // and this one too

   // identities
   assign 	 otherids = { (a > a), // bunch of zeros
			      (a < a),
			      ((a&a)!=a),
			      ((a|a)!=a),
			      ((a-a)!=0),
			      ((a%a)!=0),
			      ((a^a)==0), // always true
			      (a>=a),
			      (a <= a),
			      ((!(!a)) == (a != 0)),
			      ((!(!b[0])) == b[0]),
                              ((~(~a))==a)
                            };

endmodule

   

   
