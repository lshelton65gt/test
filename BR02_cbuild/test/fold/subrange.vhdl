-- Test simple vhdl subrange compares
-- Interesting cases:
--   s11 <= POS11
--   s11 <= POS12
--   s11 <  POS11
--   s11 <  POS12
--   s11 >= POS11
--   s11 >= POS12
--   s11 >  POS11
--   s11 >  POS12

entity subrange is
  port (s11 : in integer range 0 to 2047;
        result : out bit_vector(13 downto 0)
        );
end;

architecture arch of subrange is
  constant pos10 : integer := 1023;
  constant pos11 : integer := 2047;
  constant pos12 : integer := 2049;
begin
  fold: process(s11)
    type boolean_vector is array (natural range <>) of boolean;
    variable relop : boolean_vector(13 downto 0);
  begin
    relop(0) := (s11 <= pos11);
    relop(1) := (s11 <= pos12);
    
    relop(2) := (s11 < pos11);
    relop(3) := (s11 < pos12);

    relop(4) := (s11 >= pos11);
    relop(5) := (s11 >= pos12);

    relop(6) := (s11 > pos11);
    relop(7) := (s11 > pos12);

    relop(8) := (s11 <= pos10);
    relop(9) := (s11 < pos10);
    relop(10):= (s11 >= pos10);
    relop(11):= (s11 > pos10);

    relop(12):= (s11 = pos12);
    relop(13):= (s11 /= pos12);
    for i in 13 downto 0 loop
      if relop(i) = true then
        result(i) <= '1';
      else
        result(i) <= '0';
      end if;
    end loop;

  end process fold;

end architecture arch;
