// last mod: Thu Mar 17 16:18:43 2005
// filename: test/langcov/bug3851.v
// Description:  This test at one time created a segfault. NC, (MTI || Aldec),  Carbon sim mismatch


module top(d1,ck,q);

   input [19:0] d1;
   input [3:0]	 ck;
   output [39:0] q;

   foo f0 ( .in1(d1[ck +: 20]), .idx(ck), .o(q[0 +: 20]));
   foo f1 ( .in1(d1[ck +: 20]), .idx(ck), .o(q[20 +: 20]));

endmodule // top

module foo(in1, idx, o);
   input [19:0] in1;
   input [3:0] 	idx;
   output [19:0] o;

   reg [19:0] 	 o_r;
   initial o_r = 0;
   always @(in1) 
     o_r [idx +: 4] = in1[12 +: 4] ;
   assign o = o_r;
endmodule
