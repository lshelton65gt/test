// This testcase comes from bug4120.
//
// This snippet of a randomly generated testcase caused wrong answers in
// a sandbox, because changes in that sandbox exposed a latent bug where
//      (wide_nonzero_const && variable)
// was being folded to
//      wide_nonzero_const
// when it should have been folded to
//      1'b1
// So this testcase captures the details
//
// The original testcase had sim-results, which I suspect are inherently
// bogus.  I think they must have been created by golding the cbuild output.
//
// Note that the expression after the == folds to 1'b1 (that's what this
// testcase is really looking for), and is tested in simulation in
// wide_nonzero_const2.v. 
//
// But look at the part before the == sign.  &mIOa is only going to be
// 1 if all 9 bits of mIOa are 1, which is going to happen only very
// occassionally with random vectors.  The dynamic index into m is,
// if you look at waveforms, always going to be WAY out of bounds.  I'm
// seing index sequences of 896,1566,1899,1055....I found this by breaking
// out that index expression into a separate 32 bit variable and looking
// at the waveform for it.
//
// In Carbon, that's still an out-of-bounds memory read, and valgrind
// complains.  Aldec gives a 0/1 answer and I don't know where it gets it.
// NC says X which I think is right.  So I'm not going to check in a
// sim gold file for this case, just for wide_nonzero_const2.v
module top(IOa, m, OM, E, mWIA, H, frPb, UDj, nuxy, xe);
  input  [119:127] IOa;
  input [116:9] m;
  input  [125:115] OM;
  input [124:92] E;
  input [110:110] mWIA;
  input H;
  input  frPb;
  input [26:54] nuxy;
  input [18:34] UDj;

  output xe;

  assign xe = (((&IOa)
                || m[(((UDj[23:33]
                        ^ (((nuxy
                             & (127'h00000000efcda402
                                + OM[124:121]))
                            && {1'b0,96'h00000000d8973976,UDj,(E[100:92]
                                                               ^ H)})
                           & 1))
                       - 9)
                      + 9)
                     +: 69])
               == (95'h00000000afda3bf6
                   && (((mWIA[110]
                         != frPb)
                        << m[112:97])
                       | 95'h00000000a6c8e695)));
endmodule
