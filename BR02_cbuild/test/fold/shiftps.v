// folding a funny shift...

module top(v,s,o);
   input [31:0] v;
   input [4:0]  s;
   output [6:0] o;
   assign       o = (v >> (s-3)) & 255;

endmodule
                        
