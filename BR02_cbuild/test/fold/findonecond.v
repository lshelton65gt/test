// test findone via simple statements.
module findonestmt(i,o,k);
   input [7:0] i;
   output [4:0] o;
   output [4:0] k;

   // would like to see "o = FLO(i)
   assign o = i[7] ? 7 : i[6] ? 6 : i[5] ? 5 : i[4] ? 4 : i[3] ? 3 : i[2] ? 2 : i[1] ? 1 : i[0] ? 0 : -1;
   assign k = i[0]?0: i[1]?1: i[2]?2: i[3]?3: i[4]?4: i[5]?5: i[6]?6: i[7]?7: -1;
   
endmodule

// use findone with flattened instance to test FLZ also
module top(a,b,c,selA,selB,outFO,outFZ);
   output [9:0]c;
   input [7:0] a;
   output [9:0] b;

   findonestmt  wantflo(a,b[4:0],c[4:0]);
   findonestmt  wantflz(~a, b[9:5], c[9:5]);

   // from panasonic test
   output [3:0] outFO, outFZ;
   input [12:0] selA,selB;
   // These two DON'T transform, because the values can't be adjusted like a
   // case stmts can.
   assign outFO = (selA[0]) ? 4'hC:
          (selA[1]) ? 4'hB:
          (selA[2]) ? 4'hA:
          (selA[3]) ? 4'h9:
          (selA[4]) ? 4'h8:
          (selA[5]) ? 4'h7:
          (selA[6]) ? 4'h6:
          (selA[7]) ? 4'h5:
          (selA[8]) ? 4'h4:
          (selA[9]) ? 4'h3:
          (selA[10]) ? 4'h2:
          (selA[11]) ? 4'h1:
          (selA[12]) ? 4'h0: 4'hF;

   assign outFZ = (!selB[0]) ? 4'hC:
          (!selB[1]) ? 4'hB:
          (!selB[2]) ? 4'hA:
          (!selB[3]) ? 4'h9:
          (!selB[4]) ? 4'h8:
          (!selB[5]) ? 4'h7:
          (!selB[6]) ? 4'h6:
          (!selB[7]) ? 4'h5:
          (!selB[8]) ? 4'h4:
          (!selB[9]) ? 4'h3:
          (!selB[10]) ? 4'h2:
          (!selB[11]) ? 4'h1:
          (!selB[12]) ? 4'h0: 4'hF;
endmodule

   
