// test simple xor reductions

module simp(x,y,o,clock);
   input [31:0] x;
   input [31:0] y;
   input 	clock;
   reg [2:0] r; 
   output [4:0] o;
	assign o[2:0] = r;

   always @(negedge clock)
     begin
	r[1] = x[0] & y[0] & x[1] & y[1] & x[2] & x[3] & y[2] & y[4];

	r[2] = x[0] | y[0] | x[1] | y[1] | x[2] | x[3] | y[2] | y[4];
     end

   assign o[3] = (x ^ x);       // always zero
   assign o[4] = (x ~^ x);      // always 1
endmodule // simp

   
