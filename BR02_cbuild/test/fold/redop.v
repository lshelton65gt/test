// Test reduction operator optimizations

module top(a,b,o);
   input [63:0]a;
   input [0:63]b;
   output [26:0] o;

   //////////////////////////////////////
   // tests that shouldn't parallelize

   // Already aligned, no size reduction and only 1 bit per word.
   assign 	o[0] = a[0] | a[32] | b[31] | b[63];
   assign       o[1] = a[0] & a[32] & b[31] & b[63];

   assign 	o[2] = a[0] ^ a[32] ^ b[1] ^ b[31]; 	
   assign       o[3] = a[0] + a[32] + b[1] + b[2] + b[32];

   //////////////////////////////////
   // test that should parallelize

   // This has enough non-aligned bits to be useful..
   // throw in some duplicates to test reduction elimination code.
   
   assign 	o[4] = a[0] | a[1] | a[2] | b[31] | b[32] | b[32];
   assign       o[5] = a[0] & a[1] & a[2] & b[32] & b[32];
   assign 	o[6] = a[0] ^ a[33] ^ b[1] ^ b[33] ^ a[33] ^ a[33]; 
   assign       o[7] = a[0] + a[32] + b[1] + b[2] + b[32];

   // Reject as parallel because of repeated operand
   assign       o[8] = a[0] + a[32] + b[1] + b[2] + b[32] + b[32];
   assign       o[9] = a[0] ~^ a[1] ~^ a[3] ~^ a[5]; // reject because of ~^
   
   // BDD-optimizable reductions
   assign        o[10] = &{a[0],1'b1};   // optimize to buffer
   assign        o[11] = &{a[0],1'b0};   // optimize to 0
   assign        o[12] = |{a[0],1'b1};   // optimize to 1
   assign        o[13] = |{a[0],1'b0};   // optimize to a buffer
   assign        o[14] = ~&{a[0],1'b1};  // optimize to inverter
   assign        o[15] = ~&{a[0],1'b0};  // optimize to 1
   assign        o[16] = ~|{a[0],1'b1};  // optimize to 0
   assign        o[17] = ~|{a[0],1'b0};  // optimize to inverter

   // These four don't get optimzed nicely in the back end yet:
   assign        o[18] = &{a[0],a[1]};   // optimize to (a&0x3)==0x3
   assign        o[19] = |{a[0],a[1]};   // optimize to (a&0x3)!=0
   assign        o[20] = ~&{a[0],a[1]};  // optimize to (a&0x3)!=0x3
   assign        o[21] = ~|{a[0],a[1]};  // optimize to (a&0x3)==0

   // This is equiv to a line from dsp16k that caused an error in my first
   // try at adding these redunction operators, due to a failure to set
   // the size on the NUConst for the vector of 1s for reduction-and
   assign        o[22] = ~((~|a[5:3]) | &a[5:3]);

   // New folds involving ~
   
   assign        o[23] = &(~a[7:0]); // a[7:0] == 0
   assign        o[24] = |(~a[15:8]); // a[15:8] != 255
   assign        o[25] = ~|(~a);      // => &a
   assign        o[26] = ~&(~a);      // => |a
   
endmodule
