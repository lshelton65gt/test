//bug5810 - was incorrectly transforming this to FLO(sel[0:-12]).
// It works now because we reject it for ?: transform, but accept it
// after reduce changes it to an IF-THEN-ELSE.
//
module test( clk, sel, out );
   input clk;
   input [12:0] sel;
   output [3:0] out;
   
   reg [3:0] out;
		     
   initial begin
      out = 4'h0;
   end
   
   always @(posedge clk) begin
      out <= (sel[0]) ? 4'hC:
     	     (sel[1]) ? 4'hB:
             (sel[2]) ? 4'hA:
             (sel[3]) ? 4'h9:
             (sel[4]) ? 4'h8:
             (sel[5]) ? 4'h7:
             (sel[6]) ? 4'h6:
             (sel[7]) ? 4'h5:
             (sel[8]) ? 4'h4:
             (sel[9]) ? 4'h3:
             (sel[10]) ? 4'h2:
             (sel[11]) ? 4'h1:
             (sel[12]) ? 4'h0: 4'hF;
   end
endmodule
