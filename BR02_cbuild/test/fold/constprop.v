module test(in0, out1,A,B,C);
input in0;
output out1;
   output A,B,C;

   assign A = B;
   assign B = C;
   assign C = 0;

assign out1 = A + B + C;
assign out1 = in0;
endmodule
