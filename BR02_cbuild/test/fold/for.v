// Test infinite-loop for statement
module fornow(i,o,c);
   input i, c;
   output o;
   wire   o;
   reg r;
   assign o = r;
   integer count;

   always @(posedge c)
     begin
        r = 0;
        for (count = 0; 0; count = count+1) // fold this one away
          r = !r;
     end

   always @(negedge c)
     begin
        r = 0;
        for (count = 0; 1; count = count+1) // warn about infinite loop
          r = !r;
     end
   
endmodule
