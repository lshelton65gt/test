// Test empty scope removal
// This test simulates differently in Aldec & NC unless the -noFixClocksAsData
// flag is passed to cbuild.
//
module top(i,x);
   input i;
   output [4:0] x;
   reg [4:0] o;
   
   always @(posedge i)
     begin : container
        begin: empty1
        end
        begin : empty2
        end
        begin : hasdecl
           integer j;
           j = i;
           o[0] = !j;
        end
        begin : hassamedecl
           integer j;
           j = !i;
           o[1] = j;
        end
        begin : deepernest
           begin : empty3
           end
           begin : empty4
           end
           o[2] = i;
        end
        begin : nesthide
           begin : hasdecl
              integer j;
              j = i+1;
              o[3] = j;
           end
        end
        begin : nesthide2
           begin : hasdecl
              integer j;
              j = i*4;
              o[4] = j;
           end
        end
     end
   assign x = o;
endmodule

   
