/*
 
  Al writes in a review for Josh:

 >  Folded :{random5.v:189}
 >         :(119'h000000000000000000000000000000 != (-q[22:14]))
 > -       :((-q[22:14]) != 119'h000000000000000000000000000000)
 > +       :((-q[22:14]) != 9'h000)
 > 
 > Here's an example that I think produces a different result if q[22:14] 
 > is all ones, and
 > we generate a 119 bit value for -q[22:14].  Then we get
 > 
 >          119'h200 != 119'h0
 > 
 > as opposed to the 9 bit compare where the carry gets lost and we have 
 > 9'b0 != 9'b0.
 > 
 > 
 
This testcase captures the issue (and caught a bug before commit).
*/


module top(a, out);
  input [2:0] a;
  output      out;

  assign      out = (-a == 0);
endmodule
