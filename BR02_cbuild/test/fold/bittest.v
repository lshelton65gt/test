module bittest(clk,a,b,o);
   output o;
   input [3:0] b;
   input       a;

   input       clk;
   reg 	       result;

   always @(posedge clk)
     if (a==1)
       result = a;
     else
       result = !a;

   assign      o = result;

   always @(negedge clk)
     if(b == 0)
       result = b;
     else
       result = !b;
endmodule
