module bug(a,b,c,out);
  input a,b,c;
  output out;
  reg out;

  always @(a or b or c) 
    case({a,b,c}) 
    0: out = 0;
    1: out = 1;
    2: out = 1;
    3: out = 0;
    4: out = 1;
    5: out = 0;
    6: out = 0;
    7: out = 1;
    endcase
endmodule
