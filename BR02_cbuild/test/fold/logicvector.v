// test logical expression vector inferencing

module top(i,a,b,c,d,e,f,g);
   input [7:0] i;
   output      a,b,c,d,e,f,g;

   // Want a = (i[3:0] == 0);
   assign      a = (i[0] == 0) && (i[1]==0) && (i[2] == 0) && (i[3]==0);

   // Want b = (i[3:0] == 15)
   assign      b = (i[0] == 1) && (i[1]==1) && (i[2] == 1) && (i[3]==1);

   // Want c = (i[3:0] == 4'b0110)
   assign      c = (i[0] != 1) && (i[1]!=0) && (i[2] == 1) && (i[3]==0);

   // want d = (i[3:0] != 0)
   assign      d = (i[0] == 1) || (i[1]==1) || (i[2] == 1) || (i[3]==1);

   // want e = (i == 8'b11110000')
   assign e = i[0]==0 && i[4]==1 && i[1]==0 && i[5]==1 && i[2]==0
          && i[6]==1 && i[3]==0 && i[7]==1;

   // want f = (i & (128|32|64|16)) == 0
   assign f = i[7]==0 && i[5]==0 && i[6]==0 && i[4] == 0;

   // Want g = (i!=0)
   assign g = i[0] | i[1] | i[2] | i[3] | i[4] | i[5] | i[6] | i[7];
endmodule
               
