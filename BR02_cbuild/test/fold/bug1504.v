// Test effectiveBitSize() to see that we get right answers for a mix
// of foldable/non-foldable cases.
//

module foo (clk, in, out);
   input in, clk;
   output out;
   reg 	  out;
   
   parameter [4:0] rangeParam = 1'b1;
   parameter anyParam = 1'b1;
        
   parameter [39:0] bigParam = 40'hf000000000 + 1'b1;

   always @(posedge clk)
     out = ((rangeParam | in) <= bigParam) + anyParam;

   // silly, but what the heck...
   always @(negedge clk)
     out = ((rangeParam | in) <= bigParam) + anyParam;
endmodule

module random(a,b,o);
   input [7:0] a;
   input [3:0] b;

   output  [23:0] o;


   
   assign      o[0] = (9'h100 > (a+b)); // non-const

   assign      o[1] = (1024 > (a+b));   // const (can't be bigger than 9 bits)
   assign      o[2] = (512 > (a&b)); // const
   assign      o[3] = (32'H80000000 > (a<<b));      // const 255 << 16
   assign      o[4] = (128 > (a%128)); // const
   assign      o[5] = (16'h8000 > (a*b)); // const

   assign      o[6] = (1024 > (a-b)); // nonconst

   assign      o[7] = (8'h80 > (a&b));  //const

   assign      o[8] = (8'h80 > (a|b)); // non-const

   assign      o[9] = (9'h100 > (a|b));// const

   assign      o[10] = (8'h80 > (a^&b)); // non-const
   assign      o[11] = (9'h100 > (-a));	// non constant

   assign      o[12] = (2 > (~a)); // const, because a gets widened to 32 bits before ~
   assign      o[13] = (13'h1000 > {a,b}); //const
   assign      o[14] = (9'h100 > (a>>b));// const
   assign      o[15] = (8'hff >= (a>>b)); // how smart are we?

   assign      o[16] = (8'h80 > ((b>3) ? a : b)); // non-const

   assign      o[17] = (512 > ((b>3) ? a : b));// const
   assign      o[18] = (512 > (a^b)); // const
   assign      o[19] = (8'h80 > a[6:0]); // const

   assign      o[20] = (512 > (a + 0));	// should be const[FIXME]

   foo other(a[0], b[0], o[21]);

   assign      o[22] = (2 > (^a));// const
   assign      o[23] = (2 > (!a));// const
   
endmodule
