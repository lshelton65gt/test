// casex infinite fold loop
module top(clk, mcid, en, ena, a, enb, b, enc, c, x);
   input [2:0] mcid;
   input       en, ena, a, enb, b, enc, c,clk;
   output      x;
   reg         o;
   assign      x = o;
   always @(posedge clk)
     begin
        casex({mcid == 3'b0, mcid==3'b111, en, ena&a, enb&b, enc&c})
          6'b1x1xxx,
          6'b1x01xx,
          6'b1x001x,
          6'b1x0001,
          6'b11xxx,
          6'b101xx,
          6'b1001x,
          6'b10001:
            o = 1'b1;
          default
            o = 1'b0;
        endcase
     end
endmodule
