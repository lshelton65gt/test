module dominator(i1, i2, i3, out_or, out_and);
  input i1, i2, i3;
  output out_or, out_and;

  // Generate a zero that will be tough for Fold to find on its own,
  // but will be easy for the BDD package to find.
  `define TRICKY_ZERO (i1 & (~i1 | (~i1 & i2)))
  `define TRICKY_ONE (i1 | (~i1 | (~i1 & i2)))
  `define INVALID_BDD (i2 + i3)

  assign out_or = `TRICKY_ONE | `INVALID_BDD;
  assign out_and = `TRICKY_ZERO & `INVALID_BDD;
endmodule
