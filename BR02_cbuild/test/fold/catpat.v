// test concat pattern match collapse
module top(input [7:0] a, input [7:0] b, output o);
   reg [127:0] tab;

   always @(a or b)
     begin
        tab = {a,b,b,a,
               a,b,b,a,
               a,a,b,b,
               a,a,b,b};  // like to see {{2{a,{2{b}},a}},{2{{2{a},{2{b}}}}}}
     end
   
   assign   o = ^tab;
endmodule
