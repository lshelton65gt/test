// This variation just takes the fold that should go to 1 and makes
// sure we get that.

module top(m, mWIA, frPb, out);
  input [116:9] m;
  input [110:110] mWIA;
  input           frPb;
  output          out;

  assign out = (95'h00000000afda3bf6
                && (((mWIA[110]
                      != frPb)
                     << m[112:97])
                    | 95'h00000000a6c8e695));
endmodule
