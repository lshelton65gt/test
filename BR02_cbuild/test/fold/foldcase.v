module foldcase(c,i,j);
   input c, i;
   output j;
   reg 	  r,s;
   assign j = r|s;
   
   always @(posedge c)
     begin
	case (i)
	  1'b0: r=i;
	  1'b1: r=i;
	  default: r=i;
	endcase
     end

   always @(negedge c)
     begin
	case (i+1)		// Don't fold this one
	  1'b0: s=0;		
	  1'b1: s=i;		// combine these two case actions?
	  default: s=i;
	endcase
     end
endmodule // foldcase
