// Test folding {r,r,r} => {3{r}}, et al.

module top(a,b,w,x,y,z);
   input a;
   input [1:0] b;
   output [15:0] w,x,y,z;

   assign       w = {a,a,a,a,a,a,a,a,b,b,b,b};
   assign       x = {a,b,a,b,a,b,a,b,a,b,a,b};
   assign       y = {a,b,b,a,a,b,b,a,b,b};

   // Make sure we handle repeat counts when we collapse
   assign       z = {b,{2{a,a}},{2{a,a}},b,{2{a}},{2{a}}};
endmodule

