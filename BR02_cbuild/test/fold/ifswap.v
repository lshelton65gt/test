// test that const <op> var is canonicalized.

module swap(c,i,o, w, x);
   input [3:0] i;
   input       c;
   output      o;
   output      w;
   output [3:0] x;

   assign o = (1 < i[3:0]) ? i[0]: i[3];	// convert to i > 1

   assign x = 4'b0010;

   reg [3:0] r;
   
   always @(posedge c)
     begin
	if (x < 7)
	  r = i;
	else
	  r = 0;
     end
endmodule

