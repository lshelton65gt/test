// Test logical operators

module subor(x,y, flag, out);
   input [5:0] x,y;
   input       flag;
   output[1:0]      out;
   assign      out[0] = x || flag;
   assign      out[1] = (x * y) || flag;
endmodule

module suband(x,y, flag, out);
   input [5:0] x,y;
   input       flag;
   output [1:0] out;

   assign       out[0] = x && flag;
   assign       out[1] = (x + y) && flag;       
endmodule

module top(a, b, out);
   input [5:0] a, b;
   output [18:0] out;

   suband sub0(a,b, 1'b1, out[1:0]);
   suband sub1(a,b, 1'b0, out[3:2]);
   subor sub2(a,b, 1'b1, out[5:4]);
   subor sub3(a,b, 1'b0, out[7:6]);

   suband sub4(a,6'b1, a!=0, out[9:8]);
   suband sub5(a,6'b0, a!=0, out[11:10]);
   subor  sub6(6'b1, b, b>3, out[13:12]);
   subor  sub7(6'b0, b, b>3, out[15:14]);

   // test idempotency
   assign        out[16] = a && a;
   assign        out[17] = b || b;
   assign        out[18] = (a * b) && (a * b);        
endmodule
