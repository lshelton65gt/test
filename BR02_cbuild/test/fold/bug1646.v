module fold(clk, add_sgn, ocs_add_mat_mask, alg0_idx, o);
   input clk;
   input add_sgn;
   input [41:0] ocs_add_mat_mask;
   input [6:0] 	alg0_idx;
   output [6:0] o;
   reg [6:0] 	o;
   
   wire [6:0] 	alg1_idx;
   reg [6:0] 	alg_idx;

   wire [42:0] 	__alg1_idx_6;
   assign 	__alg1_idx_6 = ocs_add_mat_mask[41:0]<<0;
   assign 	alg1_idx[6] = 0;
   wire [42:0] 	__alg1_idx_5;
   assign 	__alg1_idx_5 = __alg1_idx_6<<(64&(0-alg1_idx[6]));
   assign 	alg1_idx[5] = (& __alg1_idx_5[41:10]);
   wire [42:0] 	__alg1_idx_4;
   assign 	__alg1_idx_4 = __alg1_idx_5<<(32&(0-alg1_idx[5]));
   assign 	alg1_idx[4] = (& __alg1_idx_4[41:26]);
   wire [42:0] 	__alg1_idx_3;
   assign 	__alg1_idx_3 = __alg1_idx_4<<(16&(0-alg1_idx[4]));
   assign 	alg1_idx[3] = (& __alg1_idx_3[41:34]);
   wire [42:0] 	__alg1_idx_2;
   assign 	__alg1_idx_2 = __alg1_idx_3<<(8&(0-alg1_idx[3]));
   assign 	alg1_idx[2] = (& __alg1_idx_2[41:38]);
   wire [42:0] 	__alg1_idx_1;
   assign 	__alg1_idx_1 = __alg1_idx_2<<(4&(0-alg1_idx[2]));
   assign 	alg1_idx[1] = (& __alg1_idx_1[41:40]);
   wire [42:0] 	__alg1_idx_0;
   assign 	__alg1_idx_0 = __alg1_idx_1<<(2&(0-alg1_idx[1]));
   assign 	alg1_idx[0] = (& __alg1_idx_0[41:41]);
   always @ (add_sgn  or alg1_idx or alg0_idx) begin
      case (add_sgn)
	1'd1: alg_idx = (alg1_idx[6:0]);
	1'd0: alg_idx = (alg0_idx[6:0]);
	default: alg_idx = 7'bx;
      endcase
   end

   initial o = 0;
   always @(posedge clk) begin
      o <= ~alg_idx;
   end
endmodule
