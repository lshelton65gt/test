// Test commutativity,  identities and zeros

module commute_mul(a,b,c,d,e);
   input [31:0] a;
   input [31:0] b;

   output [31:0] c;
   output [31:0] d;
   output [31:0] e;

   assign 	 c = 1*a;
   assign        d = 0*a;
   assign        e = (1<<31) * (1<<31);
endmodule

module c_div(a,b,c,d,e);
   input [31:0] a;
   
   input [31:0] b;

   output [31:0] c;
   output [31:0] d;
   output [31:0] e;

   assign 	 c = a/1;
   assign        d = 0/a;
   assign        e = a/(-1);
endmodule

module commute_and(a,b,c,d,e);
   input [31:0] a;
   input [31:0] b;

   output [31:0] c;
   output [31:0] d;
   output [31:0] e;

   assign 	 c = 32'hffffffff & a;
   assign        d = 0 & a;
   assign        e = 1 & a;	// not folded
endmodule

module commute_or(a,b,c,d,e);
   input [31:0] a;
   input [31:0] b;

   output [31:0] c;
   output [31:0] d;
   output [31:0] e;

   assign 	 c = 1 | a;
   assign        d = 0 | a;
   assign        e = 32'hffffffff | a;
endmodule


module top(a,b,o);
   output [31:0] o;
   input [31:0]  a;
   input [31:0]  b;
   
   wire [31:0] 	 x;
   wire [31:0] 	 y;
   wire [31:0] 	 z;

   commute_or A(a,b, x, y, z);

   wire [31:0] 	 x1;
   wire [31:0] 	 y1;
   wire [31:0] 	 z1;

   commute_and B(a, b, x1, y1, z1);

   wire [31:0] 	 x2;
   wire [31:0] 	 y2;
   wire [31:0] 	 z2;
   c_div C(a, b, x2,y2,z2);

   wire [31:0] 	 x3;
   wire [31:0] 	 y3;
   wire [31:0] 	 z3;
   commute_mul D(a,b,x3,y3,z3);

   assign 	 o = x ^ y ^ z ^ x1 ^y1 ^z1 ^x2 ^y2 ^z2 ^x3 ^y3 ^z3;
endmodule
