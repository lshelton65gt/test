module top(a, b, c, sum1, carry1, sum2, carry2);
  input a, b, c;
  output sum1, carry1, sum2, carry2;

  assign {carry1, sum1} = a + b;
  assign {carry2, sum2} = a + b + c;
endmodule
