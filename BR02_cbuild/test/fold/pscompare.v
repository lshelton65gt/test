// test partsel compare optimizations
module top(i, j, o);
   input [63:0] i, j;
   output [11:0] o;

   wire [127:0]   bv;

   assign        bv = {i,j};

   assign        o[0] = i[7+:30] == j[7+:30]; // fold
   assign        o[1] = i[32+:8] > j[0+:8]; // nofold (word aligned already
   assign        o[2] = $signed(i[33 +: 15]) <= 15'sd2300;  // fold
   assign        o[3] = $signed(i[7+:30]) < $signed(j[7+:30]); // no fold

   assign        o[4] = bv[16+:64] >= bv[48+:64]; // no fold transform
   assign        o[5] = bv[16+:32] >= bv[48+:32]; // fold
   assign        o[6] = $signed(i[8 +: 24]) < $signed(j[8+:24]); // fold
   assign        o[7] = $signed(bv[65 +: 7]) > $signed(i[33+:7]); // fold
            
   assign        o[8] = i[7:0] || i[15:8] || i[23:16] || i[31:24];  // want |(i[31:0])
   assign        o[9] = i[3+:18] || j[3+:18]; // fold
   assign        o[10] = bv[63+:12] || i[7+:17]; // fold
   assign        o[11] = i[7:0] && j[12+:20] && bv[60+:12]; // fold
endmodule
                              
