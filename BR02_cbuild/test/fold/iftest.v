// test constant folding..

module iftest(val,o);
   output o;
   input  val;
   wire	  a=0;
   reg    b;
   always @(val)
     if(a==0)
       begin
       b = val;			// Fold away else clause
       end
     else
       begin
       b = ~val;
       end

   assign o = b;
endmodule

module ifone(i,j,c,o);
   input i,j,c;
   output o;

   reg          r;
   assign       o = r;
   always @(posedge c)
     begin
        if (i)
          r = r^j;
        else if (j)
          r = r^j;
        else
          r = 0;
     end
endmodule

module iftwo(i,j,c,o);
   input i,j,c;
   output o;

   reg          r;
   assign       o = r;
   always @(posedge c)
     begin                      // convert to if (i || !j) r=1 else r=0;
        if (i)
          r = !r;
        else if (j)
          r = 0;
        else
          r = !r;
     end
   
endmodule

module ifthree(i,j,c,o);
   input i,j,c;
   output o;

   reg          r;
   assign       o = r;
   always @(posedge c)
     begin 
        if (i)
          if (j)
            r = !r;
          else
            r = 0;
        else
          r = !r;
     end
   
endmodule

module iffour(i,j,c,o);
   input i,j,c;
   output o;

   reg          r;
   assign       o = r;
   always @(posedge c)
     begin 
        if (i)
          if (j)
            r = 0;
          else
            r = !r;
        else
          r = !r;
     end
   
endmodule



module top(i,j,c,o);
   input i,j,c;
   output [4:0] o;

   
   iftest condfold(i,o[0]);
   ifone a(i,j,c,o[1]);
   iftwo b(i,j,c,o[2]);
   ifthree xc(i,j,c,o[3]);
   iffour d(i,j,c,o[4]);
   
endmodule
