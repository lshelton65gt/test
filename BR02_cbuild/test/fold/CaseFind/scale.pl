#!/bin/sh
# -*-Perl-*-
PATH=$CARBON_HOME/bin:$PATH
LANG=C
export LANG
exec perl -w -S -x -- "$0" "$@"
#!perl

#
# Script used to generate casez/casex testcases.
#

use strict;
use Getopt::Long;

my $output_bits = 3; # use 3 output bits; 2^3 select bits.
my $descending  = 0; # emit ascending case stmt.
my $value       = 1; # use '1' as the diagonal value.
my $wrap        = 0; # construct a top-level wrapper module.
my $instances   = 5; # when wrapping, use 5 instances.
my $fillx       = 0; # fill with 'x', not the inverted value.

my $option_result = GetOptions("bits=i"     => \$output_bits,
			       "descending" => \$descending,
			       "value=i"    => \$value,
			       "wrap"       => \$wrap,
			       "instances"  => \$instances,
			       "fillx"      => \$fillx);

# We do no error checking. Sorry.

my $fill_value = $fillx ? "x" : ($value=="1" ? "0" : "1");
my $selector_bits  = 2**$output_bits;

print "// module to find the first one.\n";
print "module finder$selector_bits(clk,sel,in,out);\n";
print "input clk;\n";
printf "  input [%d:0] sel;\n", ($selector_bits-1);
printf "  input [%d:0] in;\n", ($output_bits-1);
printf "  output [%d:0] out;\n", ($output_bits-1);
printf "  reg [%d:0] out;\n", ($output_bits-1);
print "initial out = 0;\n";
print "always @(posedge clk) begin\n";
print "casex(sel)\n";
for (my $i = 0; $i < $selector_bits; ++$i) {
    my $leading_bits = $selector_bits - $i - 1;
    my $trailing_bits = $selector_bits - $leading_bits - 1;

    print "${selector_bits}'b";

    if ($descending) {
	print $fill_value x $trailing_bits;
    } else { 
	print "x" x $leading_bits;
    }
    print $value ? "1" : "0";
    if ($descending) {
	print "x" x $leading_bits;
    } else { 
	print $fill_value x $trailing_bits;
    }

    # assign out.
    print ": out = in & $i;\n";
}
print "default: out = 0;\n";
print "endcase\n";
print "end\n";
print "endmodule\n";
print "\n";

if ($wrap) {
    # Optionally, create a top-level wrapper module.
    print "// wrap it all up.\n";
    print "module top(clk,sel,in,out);\n";
    print "  input clk;\n";
    printf "  input [%d:0] sel;\n", ($selector_bits-1);
    printf "  input [%d:0] in;\n", ($output_bits-1);
    printf "  output [%d:0] out;\n", ($output_bits-1);

    for (my $i = 0; $i < $instances; ++$i) {
	printf "  wire [%d:0] stage_%d_out;\n", ($output_bits-1), $i;
    }

    print "finder$selector_bits stage0find ( clk, sel, in, stage_0_out );\n";

    for (my $i = 1; $i < $instances; ++$i) {
	printf "finder%d stage%dfind ( clk, sel, stage_%d_out, stage_%d_out );\n", $selector_bits, $i, $i-1, $i;
    }

    printf "finder%d stage%dfind ( clk, sel, stage_%d_out, out );\n", $selector_bits, $instances, $instances-1, $instances;

    print "endmodule\n";
}
