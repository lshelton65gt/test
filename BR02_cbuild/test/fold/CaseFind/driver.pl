#!/bin/sh
# -*-Perl-*-
PATH=$CARBON_HOME/bin:$PATH
LANG=C
export LANG
exec perl -w -S -x -- "$0" "$@"
#!perl

# Kickoff script to check compile-time for a class of casex
# statements. The "scale.pl" script is used to generate the testcases.

# Change these variables to control which output-bit widths are
# tested. The selector width will be 2^output_bit.
my $MIN_OUTPUT_BITS=1;
my $MAX_OUTPUT_BITS=10;

sub drive_bits($$$)
{
    my ($use_x, $descending, $value) = @_;
    my $Args = "-wrap -value $value";
    my $BaseName = "case_%02d_" . $value;
    if ($use_x) {
	$BaseName .= "_x";
	$Args .= " -fillx";
    }
    if ($descending) {
	$BaseName .= "_desc";
	$Args .= " -descending";
    }
    my $Echo=0;			# print the commands.
    my $Run=1;			# run the commands.
    for (my $i=$MIN_OUTPUT_BITS; $i<=$MAX_OUTPUT_BITS; ++$i) {
	my $Root = sprintf($BaseName,$i);
	my $Verilog = $Root . ".v";
	my $Log = $Root . ".cbld.log";

	my @Commands = ("./scale.pl -bits $i $Args > $Verilog",
			"\$CARBON_HOME/bin/cbuild -q -phaseStats -nocc $Verilog > $Log",
			"tail -1 $Log");
	if ($Echo) {
	    foreach my $Command (@Commands) { print $Command . "\n"; }
	}
	if ($Run) {
	    print "$Root: ";
	    foreach my $Command (@Commands) {
		system($Command);
	    }
	}
    }
}

for (my $use_x=0; $use_x<2; ++$use_x) {
    for (my $descending=0; $descending<2; ++$descending) {
	for (my $value=0; $value<2; ++$value) {
	    drive_bits($use_x,$descending,$value);
	    print "\n";
	}
    }
}
