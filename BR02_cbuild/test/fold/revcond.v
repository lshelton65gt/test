module revcond(c, i, o);
   input [31:0] i;
   input 	c;
   output 	o;

   reg [31:0] 	x;

   assign 	o = ^x;

   always @(posedge c)
     begin
	if (i[7:0])
	  x = 1;
	else
	  x = 15;
     end 

endmodule // ifcase
