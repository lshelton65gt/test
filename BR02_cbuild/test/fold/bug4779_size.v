/* 
 Testcase associated with bug4779; trying to test case transformations
 with various sizing mismatches.
 */

module top(
	   sel1, sel2, sel3, sel4, sel5, sel6, sel7,
	   a,b,
	   out1,out2,out3,out4,out5,out6,out7
	   );
   input [1:0] sel1;
   input [3:0] sel2;
   input [3:0] sel3;
   input [3:0] sel4;
   input [3:0] sel5;
   input [3:0] sel6;
   input [5:0] sel7;
   input       a,b;
   output      out1;
   output      out2;
   output      out3;
   output      out4;
   output      out5;
   output      out6;
   output      out7;

 large_conditions i1(sel1,a,b,out1);
 small_conditions i2(sel2,a,b,out2);
 small_conditions i3(sel3,a,b,out3);
 large_conditions_dont_care i4(sel4,a,b,out4);
 small_conditions_dont_care i5(sel5,a,b,out5);
 small_unaligned_dont_care i6(sel6,a,b,out6);
 large_unaligned_dont_care i7(sel7,a,b,out7);

endmodule

module large_conditions(sel,a,b,out);
   input [1:0] sel;
   input       a,b;
   output      out;
   reg 	       out;
   always @(sel or a or b)
     begin
	casex(sel)
	  3'b00x,
	  3'b11x: out = a;
	  default: out = b;
	endcase
     end
endmodule

module small_conditions(sel,a,b,out);
   input [3:0] sel;
   input       a,b;
   output      out;
   reg 	       out;
   always @(sel or a or b)
     begin
	casex(sel)
	  3'b00x,
	  3'b11x: out = a;
	  default: out = b;
	endcase
     end
endmodule

module large_conditions_dont_care(sel,a,b,out);
   input [3:0] sel;
   input       a,b;
   output      out;
   reg 	       out;
   always @(sel or a or b)
     begin
	casex(sel)
	  7'bxxxxxxx:  out = a;
	  default: out = b;
	endcase
     end
endmodule

module small_conditions_dont_care(sel,a,b,out);
   input [3:0] sel;
   input       a,b;
   output      out;
   reg 	       out;
   always @(sel or a or b)
     begin
	casex(sel)
	  3'bxxx:  out = a;
	  default: out = b;
	endcase
     end
endmodule


module small_unaligned_dont_care(sel,a,b,out);
   input [3:0] sel;
   input       a,b;
   output      out;
   reg 	       out;
   always @(sel or a or b)
     begin
	casex(sel)
	  5'bx11xx:  out = a;
	  default: out = b;
	endcase
     end
endmodule

module large_unaligned_dont_care(sel,a,b,out);
   input [5:0] sel;
   input       a,b;
   output      out;
   reg 	       out;
   always @(sel or a or b)
     begin
	casex(sel)
	  5'bx111x: out = a;
	  default: out = b;
	endcase
     end
endmodule
