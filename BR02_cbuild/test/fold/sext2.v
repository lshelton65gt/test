// test sign extension

module sextrandom(i,o);
   input [4:0] i;
   output [13:0] o;

   assign 	 o = { {9{i[4]}}, i};
endmodule

module sextaligned(i,o);
   input [7:0]i;
   output [15:0] o;

   assign 	 o = {{8{i[7]}}, i};
endmodule

module sextspecial(i,o,x);
   input [31:0] i;
   output [31:0] o;
   output [31:0] x;
   assign 	 o={{16{i[15]}}, i[15:0]};

   // same thing
   assign 	 x = {{17{i[15]}}, i[14:0]};
endmodule

module top(i,o14, o16, o32,e);
   input [31:0] i;
   output 	e;
   output [31:0] o32;
   output [15:0] o16;
   output [13:0] o14;
   

   wire [31:0] 	 cmp;

   sextrandom A(i[4:0], o14);
   sextaligned B(i[23:16], o16);
   sextspecial C(i, o32, cmp);

   assign 	 e = (cmp == o32);
endmodule

   
