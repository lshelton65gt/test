// test preservation of user-named blocks

module names(i, o, c);
   input [7:0]i;
   output o;
   input  c;
   reg 	  t;
   wire 	  flag;
   
   assign flag=0;
   
   assign o = t;
   always @(posedge c)
     if (flag)
       begin:usernamedead
	  reg r;
	  t = r;
	  r = ^i;

       end
     else
       t = 0;

   always @(negedge c)
     if (!flag)
       begin:usernamelive
	  reg r;
	  t = r;
	  r = ^i;

       end
     else
       t = 0;

endmodule
