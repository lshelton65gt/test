module top(a, b, d, e, f, out1, out2, out3, out4);
  input [31:0] a, b;
  input        d, e, f;
  output       out1, out2, out3, out4;

  assign       out1 = d & (a == b) | ~d; // requires (a==b) gets a leaf BDD
  assign       out2 = d & (e == f) | ~d; // can turn e==f into ~(e^f)

  // Depend on clever BDD leaf optimizations to simplify this to (d | (a != b))
  assign       out3 = d & (a == b) | !(a == b);

  // In this one, the outer-most expression is not BDD-able, because
  // it's a multi-bit operation.  But it's got BDD-able sub-parts,
  // which ought to get optimized
  assign       out4 = (b[2] ? 0:
                       b[1] ? 0:
                       b[0] ? 0: d);
endmodule
