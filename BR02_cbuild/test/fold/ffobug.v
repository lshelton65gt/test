// ffo bug out of noah test

module noah(i,o,c);
   input [1:0] i;
   output [7:0] o;
   input        c;

   reg [7:0]    r;
   assign       o = r;
   
   always @(posedge c)
     begin
        case (i) //   // synopsys parallel_case full_case 
          2'b01: r = 2'b01;
          2'b10: r = 2'b10;
//       2'b11:
//       2'b00:
          default:  r = 2'b00;
        endcase
     end
endmodule


