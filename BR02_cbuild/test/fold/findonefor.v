// test findone via simple statements.


module findonefor(i,o,x,c);
   input [7:0] i;
   output [4:0] o,x;
   
   reg [4:0]    ro,rx;
   input        c;
   assign       o = ro;
   assign       x = rx;
       
   // Without disable stmt, can't terminate early on these finds...
   integer      k;
   integer      m;
   always @(posedge c)
     begin: FLOZ
        rx = -1;
        for (k=0; k<8; k=k+1)
          if (i[k])
            rx = k;
          else
            rx = rx;

        ro = -1;
        for(k=0; k<8; k=k+1)
          if(!i[k])
            ro = k;
          else
            ro = ro;
     end

   always@(negedge c)
     begin: FFOZ
        ro = -1;
        rx = -1;
        for (m=7; m >=0; m=m-1)
          if (i[m])
            ro = m;
          else
            rx = m;
            
     end
   
endmodule
