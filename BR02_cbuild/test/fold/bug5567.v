// bug5567 - extra ZXT
module top(sel,in,out);
   input [1:0] sel;
   input [7:0] in;
   output [4:0] out;
   reg [4:0] 	out;
   always @(in or sel) begin
      case(sel)
	2'b00: out[1:0] = in[1:0] & 2'b00;
	2'b01: out[2:1] = in[3:2] & 2'b01;
	2'b10: out[3:2] = in[5:4] & 2'b10;
	2'b11: out[4:3] = in[7:6] & 2'b11;
      endcase
   end
endmodule
