// Tests of find-bit that should NOT result in a FF/FL because
// of flaws in the patterns.
module testA( clk, sel, out );
   input clk;
   input [12:0] sel;
   output [3:0] out;
   
   reg [3:0] out;
                   
   initial begin
      out = 4'h0;
   end
   
   always @(posedge clk) begin
      out <= (sel[0]) ? 4'hC:
             (sel[1]) ? 4'hB:
             (sel[2]) ? 4'hA:
             (sel[3]) ? 4'h9:
             (sel[4]) ? 4'h8:
             (sel[5]) ? 4'h6: // note out of order value
             (sel[6]) ? 4'h7:
             (sel[7]) ? 4'h5:
             (sel[8]) ? 4'h4:
             (sel[9]) ? 4'h3:
             (sel[10]) ? 4'h2:
             (sel[11]) ? 4'h1:
             (sel[12]) ? 4'h0: 4'hF;
   end
endmodule

module testB( clk, sel, out );
   input clk;
   input [12:0] sel;
   output [3:0] out;
   
   reg [3:0] out;
                   
   initial begin
      out = 4'h0;
   end
   
   always @(posedge clk) begin
      out <= (sel[0]) ? 4'hC:
             (sel[1]) ? 4'hB:
             (sel[2]) ? 4'hA:
             (sel[3]) ? 4'h9:
             (sel[4]) ? 4'h8:
             (sel[6]) ? 4'h6: // note out of order test, but assignments same
                              // as bug5810.v, this should NOT convert to FFO
             (sel[5]) ? 4'h7:
             (sel[7]) ? 4'h5:
             (sel[8]) ? 4'h4:
             (sel[9]) ? 4'h3:
             (sel[10]) ? 4'h2:
             (sel[11]) ? 4'h1:
             (sel[12]) ? 4'h0: 4'hF;
   end
endmodule

module testC(clk, sel, out );
   input clk;
   input [12:0] sel;
   output [3:0] out;

   reg [3:0]    out;
   initial begin
      out = 4'h0;
   end
   
   always @(posedge clk) begin
      out <= (sel[12]) ? 4'hC:
             (sel[11]) ? 4'hB:
             (sel[10]) ? 4'hA:
             (sel[9]) ? 4'h9:
             (sel[8]) ? 4'h8:
             (sel[7]) ? 4'h6: // note out of order value
             (sel[6]) ? 4'h7:
             (sel[5]) ? 4'h5:
             (sel[4]) ? 4'h4:
             (sel[3]) ? 4'h3:
             (sel[2]) ? 4'h2:
             (sel[1]) ? 4'h1:
             (sel[0]) ? 4'h0: 4'hF;

   end
endmodule

module testD(clk, sel, out );
   input clk;
   input [12:0] sel;
   output [3:0] out;

   reg [3:0]    out;
   initial begin
      out = 4'h0;
   end

   always @(posedge clk) begin
      out <= (sel[12]) ? 4'hC:
             (sel[11]) ? 4'hB:
             (sel[10]) ? 4'hA:
             (sel[9]) ? 4'h9:
             (sel[8]) ? 4'h8:
             (sel[6]) ? 4'h7: // note out of order test
             (sel[7]) ? 4'h6:
             (sel[5]) ? 4'h5:
             (sel[4]) ? 4'h4:
             (sel[3]) ? 4'h3:
             (sel[2]) ? 4'h2:
             (sel[1]) ? 4'h1:
             (sel[0]) ? 4'h0: 4'hF;
   end
endmodule

module top(clk, sel, out );
   input clk;
   input [12:0] sel;
   output [15:0] out;

   testA a(clk, sel, out[3:0]);
   testB b(clk, sel, out[7:4]);
   testC c(clk, sel, out[11:8]);
   testD d(clk, sel, out[15:12]);

endmodule

