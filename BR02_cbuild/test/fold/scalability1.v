module top( digr_sum_e,
        digr_p_err_e_0_1_masked_out, digr_ecc_ce_e_0_1_masked_out,
        digr_ecc_ue_e_0_1_masked_out, digr_overflow_e_0_1_masked_out,
        digr_p_err_e_1_1_masked_out, digr_ecc_ce_e_1_1_masked_out,
        digr_ecc_ue_e_1_1_masked_out, digr_overflow_e_1_1_masked_out,
        digr_p_err_e_2_1_masked_out, digr_ecc_ce_e_2_1_masked_out,
        digr_ecc_ue_e_2_1_masked_out, digr_overflow_e_2_1_masked_out,
        digr_p_err_e_3_1_masked_out, digr_ecc_ce_e_3_1_masked_out,
        digr_ecc_ue_e_3_1_masked_out, digr_overflow_e_3_1_masked_out,
        digr_p_err_e_4_1_masked_out, digr_ecc_ce_e_4_1_masked_out,
        digr_ecc_ue_e_4_1_masked_out, digr_overflow_e_4_1_masked_out,
        digr_p_err_e_5_1_masked_out, digr_ecc_ce_e_5_1_masked_out,
        digr_ecc_ue_e_5_1_masked_out, digr_overflow_e_5_1_masked_out,
        digr_p_err_e_6_1_masked_out, digr_ecc_ce_e_6_1_masked_out,
        digr_ecc_ue_e_6_1_masked_out, digr_overflow_e_6_1_masked_out,
        digr_p_err_e_7_1_masked_out, digr_ecc_ce_e_7_1_masked_out,
        digr_ecc_ue_e_7_1_masked_out, digr_overflow_e_7_1_masked_out,
        digr_p_err_e_8_1_masked_out, digr_ecc_ce_e_8_1_masked_out,
        digr_ecc_ue_e_8_1_masked_out, digr_overflow_e_8_1_masked_out,
        digr_p_err_e_9_1_masked_out, digr_ecc_ce_e_9_1_masked_out,
        digr_ecc_ue_e_9_1_masked_out, digr_overflow_e_9_1_masked_out,
        digr_p_err_e_10_1_masked_out, digr_ecc_ce_e_10_1_masked_out,
        digr_ecc_ue_e_10_1_masked_out, digr_overflow_e_10_1_masked_out,
        digr_p_err_e_11_1_masked_out, digr_ecc_ce_e_11_1_masked_out,
        digr_ecc_ue_e_11_1_masked_out, digr_overflow_e_11_1_masked_out,
        digr_p_err_e_12_1_masked_out, digr_ecc_ce_e_12_1_masked_out,
        digr_ecc_ue_e_12_1_masked_out, digr_overflow_e_12_1_masked_out,
        digr_p_err_e_13_1_masked_out, digr_ecc_ce_e_13_1_masked_out,
        digr_ecc_ue_e_13_1_masked_out, digr_overflow_e_13_1_masked_out,
        digr_p_err_e_14_1_masked_out, digr_ecc_ce_e_14_1_masked_out,
        digr_ecc_ue_e_14_1_masked_out, digr_overflow_e_14_1_masked_out,
        digr_p_err_e_15_1_masked_out, digr_ecc_ce_e_15_1_masked_out,
        digr_ecc_ue_e_15_1_masked_out, digr_overflow_e_15_1_masked_out,
        digr_p_err_e_16_1_masked_out, digr_ecc_ce_e_16_1_masked_out,
        digr_ecc_ue_e_16_1_masked_out, digr_overflow_e_16_1_masked_out,
        digr_p_err_e_17_1_masked_out, digr_ecc_ce_e_17_1_masked_out,
        digr_ecc_ue_e_17_1_masked_out, digr_overflow_e_17_1_masked_out,
        digr_p_err_e_18_1_masked_out, digr_ecc_ce_e_18_1_masked_out,
        digr_ecc_ue_e_18_1_masked_out, digr_overflow_e_18_1_masked_out,
        digr_p_err_e_19_1_masked_out, digr_ecc_ce_e_19_1_masked_out,
        digr_ecc_ue_e_19_1_masked_out, digr_overflow_e_19_1_masked_out,
        digr_p_err_e_20_1_masked_out, digr_ecc_ce_e_20_1_masked_out,
        digr_ecc_ue_e_20_1_masked_out, digr_overflow_e_20_1_masked_out,
        digr_p_err_e_21_1_masked_out, digr_ecc_ce_e_21_1_masked_out,
        digr_ecc_ue_e_21_1_masked_out, digr_overflow_e_21_1_masked_out,
        digr_p_err_e_22_1_masked_out, digr_ecc_ce_e_22_1_masked_out,
        digr_ecc_ue_e_22_1_masked_out, digr_overflow_e_22_1_masked_out,
        digr_p_err_e_23_1_masked_out, digr_ecc_ce_e_23_1_masked_out,
        digr_ecc_ue_e_23_1_masked_out, digr_overflow_e_23_1_masked_out,
        digr_p_err_e_0_2_masked_out, digr_ecc_ce_e_0_2_masked_out,
        digr_ecc_ue_e_0_2_masked_out, digr_overflow_e_0_2_masked_out,
        digr_p_err_e_1_2_masked_out, digr_ecc_ce_e_1_2_masked_out,
        digr_ecc_ue_e_1_2_masked_out, digr_overflow_e_1_2_masked_out,
        digr_p_err_e_2_2_masked_out, digr_ecc_ce_e_2_2_masked_out,
        digr_ecc_ue_e_2_2_masked_out, digr_overflow_e_2_2_masked_out,
        digr_p_err_e_3_2_masked_out, digr_ecc_ce_e_3_2_masked_out,
        digr_ecc_ue_e_3_2_masked_out, digr_overflow_e_3_2_masked_out,
        digr_p_err_e_4_2_masked_out, digr_ecc_ce_e_4_2_masked_out,
        digr_ecc_ue_e_4_2_masked_out, digr_overflow_e_4_2_masked_out,
        digr_p_err_e_5_2_masked_out, digr_ecc_ce_e_5_2_masked_out,
        digr_ecc_ue_e_5_2_masked_out, digr_overflow_e_5_2_masked_out,
        digr_p_err_e_6_2_masked_out, digr_ecc_ce_e_6_2_masked_out,
        digr_ecc_ue_e_6_2_masked_out, digr_overflow_e_6_2_masked_out,
        digr_p_err_e_7_2_masked_out, digr_ecc_ce_e_7_2_masked_out,
        digr_ecc_ue_e_7_2_masked_out, digr_overflow_e_7_2_masked_out,
        digr_p_err_e_8_2_masked_out, digr_ecc_ce_e_8_2_masked_out,
        digr_ecc_ue_e_8_2_masked_out, digr_overflow_e_8_2_masked_out,
        digr_p_err_e_9_2_masked_out, digr_ecc_ce_e_9_2_masked_out,
        digr_ecc_ue_e_9_2_masked_out, digr_overflow_e_9_2_masked_out,
        digr_p_err_e_10_2_masked_out, digr_ecc_ce_e_10_2_masked_out,
        digr_ecc_ue_e_10_2_masked_out, digr_overflow_e_10_2_masked_out,
        digr_p_err_e_11_2_masked_out, digr_ecc_ce_e_11_2_masked_out,
        digr_ecc_ue_e_11_2_masked_out, digr_overflow_e_11_2_masked_out,
        digr_p_err_e_12_2_masked_out, digr_ecc_ce_e_12_2_masked_out,
        digr_ecc_ue_e_12_2_masked_out, digr_overflow_e_12_2_masked_out,
        digr_p_err_e_13_2_masked_out, digr_ecc_ce_e_13_2_masked_out,
        digr_ecc_ue_e_13_2_masked_out, digr_overflow_e_13_2_masked_out,
        digr_p_err_e_14_2_masked_out, digr_ecc_ce_e_14_2_masked_out,
        digr_ecc_ue_e_14_2_masked_out, digr_overflow_e_14_2_masked_out,
        digr_p_err_e_15_2_masked_out, digr_ecc_ce_e_15_2_masked_out,
        digr_ecc_ue_e_15_2_masked_out, digr_overflow_e_15_2_masked_out,
        digr_p_err_e_16_2_masked_out, digr_ecc_ce_e_16_2_masked_out,
        digr_ecc_ue_e_16_2_masked_out, digr_overflow_e_16_2_masked_out,
        digr_p_err_e_17_2_masked_out, digr_ecc_ce_e_17_2_masked_out,
        digr_ecc_ue_e_17_2_masked_out, digr_overflow_e_17_2_masked_out,
        digr_p_err_e_18_2_masked_out, digr_ecc_ce_e_18_2_masked_out,
        digr_ecc_ue_e_18_2_masked_out, digr_overflow_e_18_2_masked_out,
        digr_p_err_e_19_2_masked_out, digr_ecc_ce_e_19_2_masked_out,
        digr_ecc_ue_e_19_2_masked_out, digr_overflow_e_19_2_masked_out,
        digr_p_err_e_20_2_masked_out, digr_ecc_ce_e_20_2_masked_out,
        digr_ecc_ue_e_20_2_masked_out, digr_overflow_e_20_2_masked_out,
        digr_p_err_e_21_2_masked_out, digr_ecc_ce_e_21_2_masked_out,
        digr_ecc_ue_e_21_2_masked_out, digr_overflow_e_21_2_masked_out,
        digr_p_err_e_22_2_masked_out, digr_ecc_ce_e_22_2_masked_out,
        digr_ecc_ue_e_22_2_masked_out, digr_overflow_e_22_2_masked_out,
        digr_p_err_e_23_2_masked_out, digr_ecc_ce_e_23_2_masked_out,
        digr_ecc_ue_e_23_2_masked_out, digr_overflow_e_23_2_masked_out,
        digr_p_err_e_0_3_masked_out, digr_ecc_ce_e_0_3_masked_out,
        digr_ecc_ue_e_0_3_masked_out, digr_overflow_e_0_3_masked_out,
        digr_p_err_e_1_3_masked_out, digr_ecc_ce_e_1_3_masked_out,
        digr_ecc_ue_e_1_3_masked_out, digr_overflow_e_1_3_masked_out,
        digr_p_err_e_2_3_masked_out, digr_ecc_ce_e_2_3_masked_out,
        digr_ecc_ue_e_2_3_masked_out, digr_overflow_e_2_3_masked_out,
        digr_p_err_e_3_3_masked_out, digr_ecc_ce_e_3_3_masked_out,
        digr_ecc_ue_e_3_3_masked_out, digr_overflow_e_3_3_masked_out,
        digr_p_err_e_4_3_masked_out, digr_ecc_ce_e_4_3_masked_out,
        digr_ecc_ue_e_4_3_masked_out, digr_overflow_e_4_3_masked_out,
        digr_p_err_e_5_3_masked_out, digr_ecc_ce_e_5_3_masked_out,
        digr_ecc_ue_e_5_3_masked_out, digr_overflow_e_5_3_masked_out,
        digr_p_err_e_6_3_masked_out, digr_ecc_ce_e_6_3_masked_out,
        digr_ecc_ue_e_6_3_masked_out, digr_overflow_e_6_3_masked_out,
        digr_p_err_e_7_3_masked_out, digr_ecc_ce_e_7_3_masked_out,
        digr_ecc_ue_e_7_3_masked_out, digr_overflow_e_7_3_masked_out,
        digr_p_err_e_8_3_masked_out, digr_ecc_ce_e_8_3_masked_out,
        digr_ecc_ue_e_8_3_masked_out, digr_overflow_e_8_3_masked_out,
        digr_p_err_e_9_3_masked_out, digr_ecc_ce_e_9_3_masked_out,
        digr_ecc_ue_e_9_3_masked_out, digr_overflow_e_9_3_masked_out,
        digr_p_err_e_10_3_masked_out, digr_ecc_ce_e_10_3_masked_out,
        digr_ecc_ue_e_10_3_masked_out, digr_overflow_e_10_3_masked_out,
        digr_p_err_e_11_3_masked_out, digr_ecc_ce_e_11_3_masked_out,
        digr_ecc_ue_e_11_3_masked_out, digr_overflow_e_11_3_masked_out,
        digr_p_err_e_12_3_masked_out, digr_ecc_ce_e_12_3_masked_out,
        digr_ecc_ue_e_12_3_masked_out, digr_overflow_e_12_3_masked_out,
        digr_p_err_e_13_3_masked_out, digr_ecc_ce_e_13_3_masked_out,
        digr_ecc_ue_e_13_3_masked_out, digr_overflow_e_13_3_masked_out,
        digr_p_err_e_14_3_masked_out, digr_ecc_ce_e_14_3_masked_out,
        digr_ecc_ue_e_14_3_masked_out, digr_overflow_e_14_3_masked_out,
        digr_p_err_e_15_3_masked_out, digr_ecc_ce_e_15_3_masked_out,
        digr_ecc_ue_e_15_3_masked_out, digr_overflow_e_15_3_masked_out,
        digr_p_err_e_16_3_masked_out, digr_ecc_ce_e_16_3_masked_out,
        digr_ecc_ue_e_16_3_masked_out, digr_overflow_e_16_3_masked_out,
        digr_p_err_e_17_3_masked_out, digr_ecc_ce_e_17_3_masked_out,
        digr_ecc_ue_e_17_3_masked_out, digr_overflow_e_17_3_masked_out,
        digr_p_err_e_18_3_masked_out, digr_ecc_ce_e_18_3_masked_out,
        digr_ecc_ue_e_18_3_masked_out, digr_overflow_e_18_3_masked_out,
        digr_p_err_e_19_3_masked_out, digr_ecc_ce_e_19_3_masked_out,
        digr_ecc_ue_e_19_3_masked_out, digr_overflow_e_19_3_masked_out,
        digr_p_err_e_20_3_masked_out, digr_ecc_ce_e_20_3_masked_out,
        digr_ecc_ue_e_20_3_masked_out, digr_overflow_e_20_3_masked_out,
        digr_p_err_e_21_3_masked_out, digr_ecc_ce_e_21_3_masked_out,
        digr_ecc_ue_e_21_3_masked_out, digr_overflow_e_21_3_masked_out,
        digr_p_err_e_22_3_masked_out, digr_ecc_ce_e_22_3_masked_out,
        digr_ecc_ue_e_22_3_masked_out, digr_overflow_e_22_3_masked_out,
        digr_p_err_e_23_3_masked_out, digr_ecc_ce_e_23_3_masked_out,
        digr_ecc_ue_e_23_3_masked_out, digr_overflow_e_23_3_masked_out,
        digr_p_err_e_0_4_masked_out, digr_ecc_ce_e_0_4_masked_out,
        digr_ecc_ue_e_0_4_masked_out, digr_overflow_e_0_4_masked_out,
        digr_p_err_e_1_4_masked_out, digr_ecc_ce_e_1_4_masked_out,
        digr_ecc_ue_e_1_4_masked_out, digr_overflow_e_1_4_masked_out,
        digr_p_err_e_2_4_masked_out, digr_ecc_ce_e_2_4_masked_out,
        digr_ecc_ue_e_2_4_masked_out, digr_overflow_e_2_4_masked_out,
        digr_p_err_e_3_4_masked_out, digr_ecc_ce_e_3_4_masked_out,
        digr_ecc_ue_e_3_4_masked_out, digr_overflow_e_3_4_masked_out,
        digr_p_err_e_4_4_masked_out, digr_ecc_ce_e_4_4_masked_out,
        digr_ecc_ue_e_4_4_masked_out, digr_overflow_e_4_4_masked_out,
        digr_p_err_e_5_4_masked_out, digr_ecc_ce_e_5_4_masked_out,
        digr_ecc_ue_e_5_4_masked_out, digr_overflow_e_5_4_masked_out,
        digr_p_err_e_6_4_masked_out, digr_ecc_ce_e_6_4_masked_out,
        digr_ecc_ue_e_6_4_masked_out, digr_overflow_e_6_4_masked_out,
        digr_p_err_e_7_4_masked_out, digr_ecc_ce_e_7_4_masked_out,
        digr_ecc_ue_e_7_4_masked_out, digr_overflow_e_7_4_masked_out,
        digr_p_err_e_8_4_masked_out, digr_ecc_ce_e_8_4_masked_out,
        digr_ecc_ue_e_8_4_masked_out, digr_overflow_e_8_4_masked_out,
        digr_p_err_e_9_4_masked_out, digr_ecc_ce_e_9_4_masked_out,
        digr_ecc_ue_e_9_4_masked_out, digr_overflow_e_9_4_masked_out,
        digr_p_err_e_10_4_masked_out, digr_ecc_ce_e_10_4_masked_out,
        digr_ecc_ue_e_10_4_masked_out, digr_overflow_e_10_4_masked_out,
        digr_p_err_e_11_4_masked_out, digr_ecc_ce_e_11_4_masked_out,
        digr_ecc_ue_e_11_4_masked_out, digr_overflow_e_11_4_masked_out,
        digr_p_err_e_12_4_masked_out, digr_ecc_ce_e_12_4_masked_out,
        digr_ecc_ue_e_12_4_masked_out, digr_overflow_e_12_4_masked_out,
        digr_p_err_e_13_4_masked_out, digr_ecc_ce_e_13_4_masked_out,
        digr_ecc_ue_e_13_4_masked_out, digr_overflow_e_13_4_masked_out,
        digr_p_err_e_14_4_masked_out, digr_ecc_ce_e_14_4_masked_out,
        digr_ecc_ue_e_14_4_masked_out, digr_overflow_e_14_4_masked_out,
        digr_p_err_e_15_4_masked_out, digr_ecc_ce_e_15_4_masked_out,
        digr_ecc_ue_e_15_4_masked_out, digr_overflow_e_15_4_masked_out,
        digr_p_err_e_16_4_masked_out, digr_ecc_ce_e_16_4_masked_out,
        digr_ecc_ue_e_16_4_masked_out, digr_overflow_e_16_4_masked_out,
        digr_p_err_e_17_4_masked_out, digr_ecc_ce_e_17_4_masked_out,
        digr_ecc_ue_e_17_4_masked_out, digr_overflow_e_17_4_masked_out,
        digr_p_err_e_18_4_masked_out, digr_ecc_ce_e_18_4_masked_out,
        digr_ecc_ue_e_18_4_masked_out, digr_overflow_e_18_4_masked_out,
        digr_p_err_e_19_4_masked_out, digr_ecc_ce_e_19_4_masked_out,
        digr_ecc_ue_e_19_4_masked_out, digr_overflow_e_19_4_masked_out,
        digr_p_err_e_20_4_masked_out, digr_ecc_ce_e_20_4_masked_out,
        digr_ecc_ue_e_20_4_masked_out, digr_overflow_e_20_4_masked_out,
        digr_p_err_e_21_4_masked_out, digr_ecc_ce_e_21_4_masked_out,
        digr_ecc_ue_e_21_4_masked_out, digr_overflow_e_21_4_masked_out,
        digr_p_err_e_22_4_masked_out, digr_ecc_ce_e_22_4_masked_out,
        digr_ecc_ue_e_22_4_masked_out, digr_overflow_e_22_4_masked_out,
        digr_p_err_e_23_4_masked_out, digr_ecc_ce_e_23_4_masked_out,
        digr_ecc_ue_e_23_4_masked_out, digr_overflow_e_23_4_masked_out,
        digr_p_err_e_0_5_masked_out, digr_ecc_ce_e_0_5_masked_out,
        digr_ecc_ue_e_0_5_masked_out, digr_overflow_e_0_5_masked_out,
        digr_p_err_e_1_5_masked_out, digr_ecc_ce_e_1_5_masked_out,
        digr_ecc_ue_e_1_5_masked_out, digr_overflow_e_1_5_masked_out,
        digr_p_err_e_2_5_masked_out, digr_ecc_ce_e_2_5_masked_out,
        digr_ecc_ue_e_2_5_masked_out, digr_overflow_e_2_5_masked_out,
        digr_p_err_e_3_5_masked_out, digr_ecc_ce_e_3_5_masked_out,
        digr_ecc_ue_e_3_5_masked_out, digr_overflow_e_3_5_masked_out,
        digr_p_err_e_4_5_masked_out, digr_ecc_ce_e_4_5_masked_out,
        digr_ecc_ue_e_4_5_masked_out, digr_overflow_e_4_5_masked_out,
        digr_p_err_e_5_5_masked_out, digr_ecc_ce_e_5_5_masked_out,
        digr_ecc_ue_e_5_5_masked_out, digr_overflow_e_5_5_masked_out,
        digr_p_err_e_6_5_masked_out, digr_ecc_ce_e_6_5_masked_out,
        digr_ecc_ue_e_6_5_masked_out, digr_overflow_e_6_5_masked_out,
        digr_p_err_e_7_5_masked_out, digr_ecc_ce_e_7_5_masked_out,
        digr_ecc_ue_e_7_5_masked_out, digr_overflow_e_7_5_masked_out,
        digr_p_err_e_8_5_masked_out, digr_ecc_ce_e_8_5_masked_out,
        digr_ecc_ue_e_8_5_masked_out, digr_overflow_e_8_5_masked_out,
        digr_p_err_e_9_5_masked_out, digr_ecc_ce_e_9_5_masked_out,
        digr_ecc_ue_e_9_5_masked_out, digr_overflow_e_9_5_masked_out,
        digr_p_err_e_10_5_masked_out, digr_ecc_ce_e_10_5_masked_out,
        digr_ecc_ue_e_10_5_masked_out, digr_overflow_e_10_5_masked_out,
        digr_p_err_e_11_5_masked_out, digr_ecc_ce_e_11_5_masked_out,
        digr_ecc_ue_e_11_5_masked_out, digr_overflow_e_11_5_masked_out,
        digr_p_err_e_12_5_masked_out, digr_ecc_ce_e_12_5_masked_out,
        digr_ecc_ue_e_12_5_masked_out, digr_overflow_e_12_5_masked_out,
        digr_p_err_e_13_5_masked_out, digr_ecc_ce_e_13_5_masked_out,
        digr_ecc_ue_e_13_5_masked_out, digr_overflow_e_13_5_masked_out,
        digr_p_err_e_14_5_masked_out, digr_ecc_ce_e_14_5_masked_out,
        digr_ecc_ue_e_14_5_masked_out, digr_overflow_e_14_5_masked_out,
        digr_p_err_e_15_5_masked_out, digr_ecc_ce_e_15_5_masked_out,
        digr_ecc_ue_e_15_5_masked_out, digr_overflow_e_15_5_masked_out,
        digr_p_err_e_16_5_masked_out, digr_ecc_ce_e_16_5_masked_out,
        digr_ecc_ue_e_16_5_masked_out, digr_overflow_e_16_5_masked_out,
        digr_p_err_e_17_5_masked_out, digr_ecc_ce_e_17_5_masked_out,
        digr_ecc_ue_e_17_5_masked_out, digr_overflow_e_17_5_masked_out,
        digr_p_err_e_18_5_masked_out, digr_ecc_ce_e_18_5_masked_out,
        digr_ecc_ue_e_18_5_masked_out, digr_overflow_e_18_5_masked_out,
        digr_p_err_e_19_5_masked_out, digr_ecc_ce_e_19_5_masked_out,
        digr_ecc_ue_e_19_5_masked_out, digr_overflow_e_19_5_masked_out,
        digr_p_err_e_20_5_masked_out, digr_ecc_ce_e_20_5_masked_out,
        digr_ecc_ue_e_20_5_masked_out, digr_overflow_e_20_5_masked_out,
        digr_p_err_e_21_5_masked_out, digr_ecc_ce_e_21_5_masked_out,
        digr_ecc_ue_e_21_5_masked_out, digr_overflow_e_21_5_masked_out,
        digr_p_err_e_22_5_masked_out, digr_ecc_ce_e_22_5_masked_out,
        digr_ecc_ue_e_22_5_masked_out, digr_overflow_e_22_5_masked_out,
        digr_p_err_e_23_5_masked_out, digr_ecc_ce_e_23_5_masked_out,
        digr_ecc_ue_e_23_5_masked_out, digr_overflow_e_23_5_masked_out,
        digr_p_err_e_0_6_masked_out, digr_ecc_ce_e_0_6_masked_out,
        digr_ecc_ue_e_0_6_masked_out, digr_overflow_e_0_6_masked_out,
        digr_p_err_e_1_6_masked_out, digr_ecc_ce_e_1_6_masked_out,
        digr_ecc_ue_e_1_6_masked_out, digr_overflow_e_1_6_masked_out,
        digr_p_err_e_2_6_masked_out, digr_ecc_ce_e_2_6_masked_out,
        digr_ecc_ue_e_2_6_masked_out, digr_overflow_e_2_6_masked_out,
        digr_p_err_e_3_6_masked_out, digr_ecc_ce_e_3_6_masked_out,
        digr_ecc_ue_e_3_6_masked_out, digr_overflow_e_3_6_masked_out,
        digr_p_err_e_4_6_masked_out, digr_ecc_ce_e_4_6_masked_out,
        digr_ecc_ue_e_4_6_masked_out, digr_overflow_e_4_6_masked_out,
        digr_p_err_e_5_6_masked_out, digr_ecc_ce_e_5_6_masked_out,
        digr_ecc_ue_e_5_6_masked_out, digr_overflow_e_5_6_masked_out,
        digr_p_err_e_6_6_masked_out, digr_ecc_ce_e_6_6_masked_out,
        digr_ecc_ue_e_6_6_masked_out, digr_overflow_e_6_6_masked_out,
        digr_p_err_e_7_6_masked_out, digr_ecc_ce_e_7_6_masked_out,
        digr_ecc_ue_e_7_6_masked_out, digr_overflow_e_7_6_masked_out,
        digr_p_err_e_8_6_masked_out, digr_ecc_ce_e_8_6_masked_out,
        digr_ecc_ue_e_8_6_masked_out, digr_overflow_e_8_6_masked_out,
        digr_p_err_e_9_6_masked_out, digr_ecc_ce_e_9_6_masked_out,
        digr_ecc_ue_e_9_6_masked_out, digr_overflow_e_9_6_masked_out,
        digr_p_err_e_10_6_masked_out, digr_ecc_ce_e_10_6_masked_out,
        digr_ecc_ue_e_10_6_masked_out, digr_overflow_e_10_6_masked_out,
        digr_p_err_e_11_6_masked_out, digr_ecc_ce_e_11_6_masked_out,
        digr_ecc_ue_e_11_6_masked_out, digr_overflow_e_11_6_masked_out,
        digr_p_err_e_12_6_masked_out, digr_ecc_ce_e_12_6_masked_out,
        digr_ecc_ue_e_12_6_masked_out, digr_overflow_e_12_6_masked_out,
        digr_p_err_e_13_6_masked_out, digr_ecc_ce_e_13_6_masked_out,
        digr_ecc_ue_e_13_6_masked_out, digr_overflow_e_13_6_masked_out,
        digr_p_err_e_14_6_masked_out, digr_ecc_ce_e_14_6_masked_out,
        digr_ecc_ue_e_14_6_masked_out, digr_overflow_e_14_6_masked_out,
        digr_p_err_e_15_6_masked_out, digr_ecc_ce_e_15_6_masked_out,
        digr_ecc_ue_e_15_6_masked_out, digr_overflow_e_15_6_masked_out,
        digr_p_err_e_16_6_masked_out, digr_ecc_ce_e_16_6_masked_out,
        digr_ecc_ue_e_16_6_masked_out, digr_overflow_e_16_6_masked_out,
        digr_p_err_e_17_6_masked_out, digr_ecc_ce_e_17_6_masked_out,
        digr_ecc_ue_e_17_6_masked_out, digr_overflow_e_17_6_masked_out,
        digr_p_err_e_18_6_masked_out, digr_ecc_ce_e_18_6_masked_out,
        digr_ecc_ue_e_18_6_masked_out, digr_overflow_e_18_6_masked_out,
        digr_p_err_e_19_6_masked_out, digr_ecc_ce_e_19_6_masked_out,
        digr_ecc_ue_e_19_6_masked_out, digr_overflow_e_19_6_masked_out,
        digr_p_err_e_20_6_masked_out, digr_ecc_ce_e_20_6_masked_out,
        digr_ecc_ue_e_20_6_masked_out, digr_overflow_e_20_6_masked_out,
        digr_p_err_e_21_6_masked_out, digr_ecc_ce_e_21_6_masked_out,
        digr_ecc_ue_e_21_6_masked_out, digr_overflow_e_21_6_masked_out,
        digr_p_err_e_22_6_masked_out, digr_ecc_ce_e_22_6_masked_out,
        digr_ecc_ue_e_22_6_masked_out, digr_overflow_e_22_6_masked_out,
        digr_p_err_e_23_6_masked_out, digr_ecc_ce_e_23_6_masked_out,
        digr_ecc_ue_e_23_6_masked_out, digr_overflow_e_23_6_masked_out,
        digr_p_err_e_0_7_masked_out, digr_ecc_ce_e_0_7_masked_out,
        digr_ecc_ue_e_0_7_masked_out, digr_overflow_e_0_7_masked_out,
        digr_p_err_e_1_7_masked_out, digr_ecc_ce_e_1_7_masked_out,
        digr_ecc_ue_e_1_7_masked_out, digr_overflow_e_1_7_masked_out,
        digr_p_err_e_2_7_masked_out, digr_ecc_ce_e_2_7_masked_out,
        digr_ecc_ue_e_2_7_masked_out, digr_overflow_e_2_7_masked_out,
        digr_p_err_e_3_7_masked_out, digr_ecc_ce_e_3_7_masked_out,
        digr_ecc_ue_e_3_7_masked_out, digr_overflow_e_3_7_masked_out,
        digr_p_err_e_4_7_masked_out, digr_ecc_ce_e_4_7_masked_out,
        digr_ecc_ue_e_4_7_masked_out, digr_overflow_e_4_7_masked_out,
        digr_p_err_e_5_7_masked_out, digr_ecc_ce_e_5_7_masked_out,
        digr_ecc_ue_e_5_7_masked_out, digr_overflow_e_5_7_masked_out,
        digr_p_err_e_6_7_masked_out, digr_ecc_ce_e_6_7_masked_out,
        digr_ecc_ue_e_6_7_masked_out, digr_overflow_e_6_7_masked_out,
        digr_p_err_e_7_7_masked_out, digr_ecc_ce_e_7_7_masked_out,
        digr_ecc_ue_e_7_7_masked_out, digr_overflow_e_7_7_masked_out,
        digr_p_err_e_8_7_masked_out, digr_ecc_ce_e_8_7_masked_out,
        digr_ecc_ue_e_8_7_masked_out, digr_overflow_e_8_7_masked_out,
        digr_p_err_e_9_7_masked_out, digr_ecc_ce_e_9_7_masked_out,
        digr_ecc_ue_e_9_7_masked_out, digr_overflow_e_9_7_masked_out,
        digr_p_err_e_10_7_masked_out, digr_ecc_ce_e_10_7_masked_out,
        digr_ecc_ue_e_10_7_masked_out, digr_overflow_e_10_7_masked_out,
        digr_p_err_e_11_7_masked_out, digr_ecc_ce_e_11_7_masked_out,
        digr_ecc_ue_e_11_7_masked_out, digr_overflow_e_11_7_masked_out,
        digr_p_err_e_12_7_masked_out, digr_ecc_ce_e_12_7_masked_out,
        digr_ecc_ue_e_12_7_masked_out, digr_overflow_e_12_7_masked_out,
        digr_p_err_e_13_7_masked_out, digr_ecc_ce_e_13_7_masked_out,
        digr_ecc_ue_e_13_7_masked_out, digr_overflow_e_13_7_masked_out,
        digr_p_err_e_14_7_masked_out, digr_ecc_ce_e_14_7_masked_out,
        digr_ecc_ue_e_14_7_masked_out, digr_overflow_e_14_7_masked_out,
        digr_p_err_e_15_7_masked_out, digr_ecc_ce_e_15_7_masked_out,
        digr_ecc_ue_e_15_7_masked_out, digr_overflow_e_15_7_masked_out,
        digr_p_err_e_16_7_masked_out, digr_ecc_ce_e_16_7_masked_out,
        digr_ecc_ue_e_16_7_masked_out, digr_overflow_e_16_7_masked_out,
        digr_p_err_e_17_7_masked_out, digr_ecc_ce_e_17_7_masked_out,
        digr_ecc_ue_e_17_7_masked_out, digr_overflow_e_17_7_masked_out,
        digr_p_err_e_18_7_masked_out, digr_ecc_ce_e_18_7_masked_out,
        digr_ecc_ue_e_18_7_masked_out, digr_overflow_e_18_7_masked_out,
        digr_p_err_e_19_7_masked_out, digr_ecc_ce_e_19_7_masked_out,
        digr_ecc_ue_e_19_7_masked_out, digr_overflow_e_19_7_masked_out,
        digr_p_err_e_20_7_masked_out, digr_ecc_ce_e_20_7_masked_out,
        digr_ecc_ue_e_20_7_masked_out, digr_overflow_e_20_7_masked_out,
        digr_p_err_e_21_7_masked_out, digr_ecc_ce_e_21_7_masked_out,
        digr_ecc_ue_e_21_7_masked_out, digr_overflow_e_21_7_masked_out,
        digr_p_err_e_22_7_masked_out, digr_ecc_ce_e_22_7_masked_out,
        digr_ecc_ue_e_22_7_masked_out, digr_overflow_e_22_7_masked_out,
        digr_p_err_e_23_7_masked_out, digr_ecc_ce_e_23_7_masked_out,
        digr_ecc_ue_e_23_7_masked_out, digr_overflow_e_23_7_masked_out,
        digr_p_err_e_0_8_masked_out, digr_ecc_ce_e_0_8_masked_out,
        digr_ecc_ue_e_0_8_masked_out, digr_overflow_e_0_8_masked_out,
        digr_p_err_e_1_8_masked_out, digr_ecc_ce_e_1_8_masked_out,
        digr_ecc_ue_e_1_8_masked_out, digr_overflow_e_1_8_masked_out,
        digr_p_err_e_2_8_masked_out, digr_ecc_ce_e_2_8_masked_out,
        digr_ecc_ue_e_2_8_masked_out, digr_overflow_e_2_8_masked_out,
        digr_p_err_e_3_8_masked_out, digr_ecc_ce_e_3_8_masked_out,
        digr_ecc_ue_e_3_8_masked_out, digr_overflow_e_3_8_masked_out,
        digr_p_err_e_4_8_masked_out, digr_ecc_ce_e_4_8_masked_out,
        digr_ecc_ue_e_4_8_masked_out, digr_overflow_e_4_8_masked_out,
        digr_p_err_e_5_8_masked_out, digr_ecc_ce_e_5_8_masked_out,
        digr_ecc_ue_e_5_8_masked_out, digr_overflow_e_5_8_masked_out,
        digr_p_err_e_6_8_masked_out, digr_ecc_ce_e_6_8_masked_out,
        digr_ecc_ue_e_6_8_masked_out, digr_overflow_e_6_8_masked_out,
        digr_p_err_e_7_8_masked_out, digr_ecc_ce_e_7_8_masked_out,
        digr_ecc_ue_e_7_8_masked_out, digr_overflow_e_7_8_masked_out,
        digr_p_err_e_8_8_masked_out, digr_ecc_ce_e_8_8_masked_out,
        digr_ecc_ue_e_8_8_masked_out, digr_overflow_e_8_8_masked_out,
        digr_p_err_e_9_8_masked_out, digr_ecc_ce_e_9_8_masked_out,
        digr_ecc_ue_e_9_8_masked_out, digr_overflow_e_9_8_masked_out,
        digr_p_err_e_10_8_masked_out, digr_ecc_ce_e_10_8_masked_out,
        digr_ecc_ue_e_10_8_masked_out, digr_overflow_e_10_8_masked_out,
        digr_p_err_e_11_8_masked_out, digr_ecc_ce_e_11_8_masked_out,
        digr_ecc_ue_e_11_8_masked_out, digr_overflow_e_11_8_masked_out,
        digr_p_err_e_12_8_masked_out, digr_ecc_ce_e_12_8_masked_out,
        digr_ecc_ue_e_12_8_masked_out, digr_overflow_e_12_8_masked_out,
        digr_p_err_e_13_8_masked_out, digr_ecc_ce_e_13_8_masked_out,
        digr_ecc_ue_e_13_8_masked_out, digr_overflow_e_13_8_masked_out,
        digr_p_err_e_14_8_masked_out, digr_ecc_ce_e_14_8_masked_out,
        digr_ecc_ue_e_14_8_masked_out, digr_overflow_e_14_8_masked_out,
        digr_p_err_e_15_8_masked_out, digr_ecc_ce_e_15_8_masked_out,
        digr_ecc_ue_e_15_8_masked_out, digr_overflow_e_15_8_masked_out,
        digr_p_err_e_16_8_masked_out, digr_ecc_ce_e_16_8_masked_out,
        digr_ecc_ue_e_16_8_masked_out, digr_overflow_e_16_8_masked_out,
        digr_p_err_e_17_8_masked_out, digr_ecc_ce_e_17_8_masked_out,
        digr_ecc_ue_e_17_8_masked_out, digr_overflow_e_17_8_masked_out,
        digr_p_err_e_18_8_masked_out, digr_ecc_ce_e_18_8_masked_out,
        digr_ecc_ue_e_18_8_masked_out, digr_overflow_e_18_8_masked_out,
        digr_p_err_e_19_8_masked_out, digr_ecc_ce_e_19_8_masked_out,
        digr_ecc_ue_e_19_8_masked_out, digr_overflow_e_19_8_masked_out,
        digr_p_err_e_20_8_masked_out, digr_ecc_ce_e_20_8_masked_out,
        digr_ecc_ue_e_20_8_masked_out, digr_overflow_e_20_8_masked_out,
        digr_p_err_e_21_8_masked_out, digr_ecc_ce_e_21_8_masked_out,
        digr_ecc_ue_e_21_8_masked_out, digr_overflow_e_21_8_masked_out,
        digr_p_err_e_22_8_masked_out, digr_ecc_ce_e_22_8_masked_out,
        digr_ecc_ue_e_22_8_masked_out, digr_overflow_e_22_8_masked_out,
        digr_p_err_e_23_8_masked_out, digr_ecc_ce_e_23_8_masked_out,
        digr_ecc_ue_e_23_8_masked_out, digr_overflow_e_23_8_masked_out);

  input digr_p_err_e_0_1_masked_out, digr_ecc_ce_e_0_1_masked_out;
  input digr_ecc_ue_e_0_1_masked_out, digr_overflow_e_0_1_masked_out;
  input digr_p_err_e_1_1_masked_out, digr_ecc_ce_e_1_1_masked_out;
  input digr_ecc_ue_e_1_1_masked_out, digr_overflow_e_1_1_masked_out;
  input digr_p_err_e_2_1_masked_out, digr_ecc_ce_e_2_1_masked_out;
  input digr_ecc_ue_e_2_1_masked_out, digr_overflow_e_2_1_masked_out;
  input digr_p_err_e_3_1_masked_out, digr_ecc_ce_e_3_1_masked_out;
  input digr_ecc_ue_e_3_1_masked_out, digr_overflow_e_3_1_masked_out;
  input digr_p_err_e_4_1_masked_out, digr_ecc_ce_e_4_1_masked_out;
  input digr_ecc_ue_e_4_1_masked_out, digr_overflow_e_4_1_masked_out;
  input digr_p_err_e_5_1_masked_out, digr_ecc_ce_e_5_1_masked_out;
  input digr_ecc_ue_e_5_1_masked_out, digr_overflow_e_5_1_masked_out;
  input digr_p_err_e_6_1_masked_out, digr_ecc_ce_e_6_1_masked_out;
  input digr_ecc_ue_e_6_1_masked_out, digr_overflow_e_6_1_masked_out;
  input digr_p_err_e_7_1_masked_out, digr_ecc_ce_e_7_1_masked_out;
  input digr_ecc_ue_e_7_1_masked_out, digr_overflow_e_7_1_masked_out;
  input digr_p_err_e_8_1_masked_out, digr_ecc_ce_e_8_1_masked_out;
  input digr_ecc_ue_e_8_1_masked_out, digr_overflow_e_8_1_masked_out;
  input digr_p_err_e_9_1_masked_out, digr_ecc_ce_e_9_1_masked_out;
  input digr_ecc_ue_e_9_1_masked_out, digr_overflow_e_9_1_masked_out;
  input digr_p_err_e_10_1_masked_out, digr_ecc_ce_e_10_1_masked_out;
  input digr_ecc_ue_e_10_1_masked_out, digr_overflow_e_10_1_masked_out;
  input digr_p_err_e_11_1_masked_out, digr_ecc_ce_e_11_1_masked_out;
  input digr_ecc_ue_e_11_1_masked_out, digr_overflow_e_11_1_masked_out;
  input digr_p_err_e_12_1_masked_out, digr_ecc_ce_e_12_1_masked_out;
  input digr_ecc_ue_e_12_1_masked_out, digr_overflow_e_12_1_masked_out;
  input digr_p_err_e_13_1_masked_out, digr_ecc_ce_e_13_1_masked_out;
  input digr_ecc_ue_e_13_1_masked_out, digr_overflow_e_13_1_masked_out;
  input digr_p_err_e_14_1_masked_out, digr_ecc_ce_e_14_1_masked_out;
  input digr_ecc_ue_e_14_1_masked_out, digr_overflow_e_14_1_masked_out;
  input digr_p_err_e_15_1_masked_out, digr_ecc_ce_e_15_1_masked_out;
  input digr_ecc_ue_e_15_1_masked_out, digr_overflow_e_15_1_masked_out;
  input digr_p_err_e_16_1_masked_out, digr_ecc_ce_e_16_1_masked_out;
  input digr_ecc_ue_e_16_1_masked_out, digr_overflow_e_16_1_masked_out;
  input digr_p_err_e_17_1_masked_out, digr_ecc_ce_e_17_1_masked_out;
  input digr_ecc_ue_e_17_1_masked_out, digr_overflow_e_17_1_masked_out;
  input digr_p_err_e_18_1_masked_out, digr_ecc_ce_e_18_1_masked_out;
  input digr_ecc_ue_e_18_1_masked_out, digr_overflow_e_18_1_masked_out;
  input digr_p_err_e_19_1_masked_out, digr_ecc_ce_e_19_1_masked_out;
  input digr_ecc_ue_e_19_1_masked_out, digr_overflow_e_19_1_masked_out;
  input digr_p_err_e_20_1_masked_out, digr_ecc_ce_e_20_1_masked_out;
  input digr_ecc_ue_e_20_1_masked_out, digr_overflow_e_20_1_masked_out;
  input digr_p_err_e_21_1_masked_out, digr_ecc_ce_e_21_1_masked_out;
  input digr_ecc_ue_e_21_1_masked_out, digr_overflow_e_21_1_masked_out;
  input digr_p_err_e_22_1_masked_out, digr_ecc_ce_e_22_1_masked_out;
  input digr_ecc_ue_e_22_1_masked_out, digr_overflow_e_22_1_masked_out;
  input digr_p_err_e_23_1_masked_out, digr_ecc_ce_e_23_1_masked_out;
  input digr_ecc_ue_e_23_1_masked_out, digr_overflow_e_23_1_masked_out;
  input digr_p_err_e_0_2_masked_out, digr_ecc_ce_e_0_2_masked_out;
  input digr_ecc_ue_e_0_2_masked_out, digr_overflow_e_0_2_masked_out;
  input digr_p_err_e_1_2_masked_out, digr_ecc_ce_e_1_2_masked_out;
  input digr_ecc_ue_e_1_2_masked_out, digr_overflow_e_1_2_masked_out;
  input digr_p_err_e_2_2_masked_out, digr_ecc_ce_e_2_2_masked_out;
  input digr_ecc_ue_e_2_2_masked_out, digr_overflow_e_2_2_masked_out;
  input digr_p_err_e_3_2_masked_out, digr_ecc_ce_e_3_2_masked_out;
  input digr_ecc_ue_e_3_2_masked_out, digr_overflow_e_3_2_masked_out;
  input digr_p_err_e_4_2_masked_out, digr_ecc_ce_e_4_2_masked_out;
  input digr_ecc_ue_e_4_2_masked_out, digr_overflow_e_4_2_masked_out;
  input digr_p_err_e_5_2_masked_out, digr_ecc_ce_e_5_2_masked_out;
  input digr_ecc_ue_e_5_2_masked_out, digr_overflow_e_5_2_masked_out;
  input digr_p_err_e_6_2_masked_out, digr_ecc_ce_e_6_2_masked_out;
  input digr_ecc_ue_e_6_2_masked_out, digr_overflow_e_6_2_masked_out;
  input digr_p_err_e_7_2_masked_out, digr_ecc_ce_e_7_2_masked_out;
  input digr_ecc_ue_e_7_2_masked_out, digr_overflow_e_7_2_masked_out;
  input digr_p_err_e_8_2_masked_out, digr_ecc_ce_e_8_2_masked_out;
  input digr_ecc_ue_e_8_2_masked_out, digr_overflow_e_8_2_masked_out;
  input digr_p_err_e_9_2_masked_out, digr_ecc_ce_e_9_2_masked_out;
  input digr_ecc_ue_e_9_2_masked_out, digr_overflow_e_9_2_masked_out;
  input digr_p_err_e_10_2_masked_out, digr_ecc_ce_e_10_2_masked_out;
  input digr_ecc_ue_e_10_2_masked_out, digr_overflow_e_10_2_masked_out;
  input digr_p_err_e_11_2_masked_out, digr_ecc_ce_e_11_2_masked_out;
  input digr_ecc_ue_e_11_2_masked_out, digr_overflow_e_11_2_masked_out;
  input digr_p_err_e_12_2_masked_out, digr_ecc_ce_e_12_2_masked_out;
  input digr_ecc_ue_e_12_2_masked_out, digr_overflow_e_12_2_masked_out;
  input digr_p_err_e_13_2_masked_out, digr_ecc_ce_e_13_2_masked_out;
  input digr_ecc_ue_e_13_2_masked_out, digr_overflow_e_13_2_masked_out;
  input digr_p_err_e_14_2_masked_out, digr_ecc_ce_e_14_2_masked_out;
  input digr_ecc_ue_e_14_2_masked_out, digr_overflow_e_14_2_masked_out;
  input digr_p_err_e_15_2_masked_out, digr_ecc_ce_e_15_2_masked_out;
  input digr_ecc_ue_e_15_2_masked_out, digr_overflow_e_15_2_masked_out;
  input digr_p_err_e_16_2_masked_out, digr_ecc_ce_e_16_2_masked_out;
  input digr_ecc_ue_e_16_2_masked_out, digr_overflow_e_16_2_masked_out;
  input digr_p_err_e_17_2_masked_out, digr_ecc_ce_e_17_2_masked_out;
  input digr_ecc_ue_e_17_2_masked_out, digr_overflow_e_17_2_masked_out;
  input digr_p_err_e_18_2_masked_out, digr_ecc_ce_e_18_2_masked_out;
  input digr_ecc_ue_e_18_2_masked_out, digr_overflow_e_18_2_masked_out;
  input digr_p_err_e_19_2_masked_out, digr_ecc_ce_e_19_2_masked_out;
  input digr_ecc_ue_e_19_2_masked_out, digr_overflow_e_19_2_masked_out;
  input digr_p_err_e_20_2_masked_out, digr_ecc_ce_e_20_2_masked_out;
  input digr_ecc_ue_e_20_2_masked_out, digr_overflow_e_20_2_masked_out;
  input digr_p_err_e_21_2_masked_out, digr_ecc_ce_e_21_2_masked_out;
  input digr_ecc_ue_e_21_2_masked_out, digr_overflow_e_21_2_masked_out;
  input digr_p_err_e_22_2_masked_out, digr_ecc_ce_e_22_2_masked_out;
  input digr_ecc_ue_e_22_2_masked_out, digr_overflow_e_22_2_masked_out;
  input digr_p_err_e_23_2_masked_out, digr_ecc_ce_e_23_2_masked_out;
  input digr_ecc_ue_e_23_2_masked_out, digr_overflow_e_23_2_masked_out;
  input digr_p_err_e_0_3_masked_out, digr_ecc_ce_e_0_3_masked_out;
  input digr_ecc_ue_e_0_3_masked_out, digr_overflow_e_0_3_masked_out;
  input digr_p_err_e_1_3_masked_out, digr_ecc_ce_e_1_3_masked_out;
  input digr_ecc_ue_e_1_3_masked_out, digr_overflow_e_1_3_masked_out;
  input digr_p_err_e_2_3_masked_out, digr_ecc_ce_e_2_3_masked_out;
  input digr_ecc_ue_e_2_3_masked_out, digr_overflow_e_2_3_masked_out;
  input digr_p_err_e_3_3_masked_out, digr_ecc_ce_e_3_3_masked_out;
  input digr_ecc_ue_e_3_3_masked_out, digr_overflow_e_3_3_masked_out;
  input digr_p_err_e_4_3_masked_out, digr_ecc_ce_e_4_3_masked_out;
  input digr_ecc_ue_e_4_3_masked_out, digr_overflow_e_4_3_masked_out;
  input digr_p_err_e_5_3_masked_out, digr_ecc_ce_e_5_3_masked_out;
  input digr_ecc_ue_e_5_3_masked_out, digr_overflow_e_5_3_masked_out;
  input digr_p_err_e_6_3_masked_out, digr_ecc_ce_e_6_3_masked_out;
  input digr_ecc_ue_e_6_3_masked_out, digr_overflow_e_6_3_masked_out;
  input digr_p_err_e_7_3_masked_out, digr_ecc_ce_e_7_3_masked_out;
  input digr_ecc_ue_e_7_3_masked_out, digr_overflow_e_7_3_masked_out;
  input digr_p_err_e_8_3_masked_out, digr_ecc_ce_e_8_3_masked_out;
  input digr_ecc_ue_e_8_3_masked_out, digr_overflow_e_8_3_masked_out;
  input digr_p_err_e_9_3_masked_out, digr_ecc_ce_e_9_3_masked_out;
  input digr_ecc_ue_e_9_3_masked_out, digr_overflow_e_9_3_masked_out;
  input digr_p_err_e_10_3_masked_out, digr_ecc_ce_e_10_3_masked_out;
  input digr_ecc_ue_e_10_3_masked_out, digr_overflow_e_10_3_masked_out;
  input digr_p_err_e_11_3_masked_out, digr_ecc_ce_e_11_3_masked_out;
  input digr_ecc_ue_e_11_3_masked_out, digr_overflow_e_11_3_masked_out;
  input digr_p_err_e_12_3_masked_out, digr_ecc_ce_e_12_3_masked_out;
  input digr_ecc_ue_e_12_3_masked_out, digr_overflow_e_12_3_masked_out;
  input digr_p_err_e_13_3_masked_out, digr_ecc_ce_e_13_3_masked_out;
  input digr_ecc_ue_e_13_3_masked_out, digr_overflow_e_13_3_masked_out;
  input digr_p_err_e_14_3_masked_out, digr_ecc_ce_e_14_3_masked_out;
  input digr_ecc_ue_e_14_3_masked_out, digr_overflow_e_14_3_masked_out;
  input digr_p_err_e_15_3_masked_out, digr_ecc_ce_e_15_3_masked_out;
  input digr_ecc_ue_e_15_3_masked_out, digr_overflow_e_15_3_masked_out;
  input digr_p_err_e_16_3_masked_out, digr_ecc_ce_e_16_3_masked_out;
  input digr_ecc_ue_e_16_3_masked_out, digr_overflow_e_16_3_masked_out;
  input digr_p_err_e_17_3_masked_out, digr_ecc_ce_e_17_3_masked_out;
  input digr_ecc_ue_e_17_3_masked_out, digr_overflow_e_17_3_masked_out;
  input digr_p_err_e_18_3_masked_out, digr_ecc_ce_e_18_3_masked_out;
  input digr_ecc_ue_e_18_3_masked_out, digr_overflow_e_18_3_masked_out;
  input digr_p_err_e_19_3_masked_out, digr_ecc_ce_e_19_3_masked_out;
  input digr_ecc_ue_e_19_3_masked_out, digr_overflow_e_19_3_masked_out;
  input digr_p_err_e_20_3_masked_out, digr_ecc_ce_e_20_3_masked_out;
  input digr_ecc_ue_e_20_3_masked_out, digr_overflow_e_20_3_masked_out;
  input digr_p_err_e_21_3_masked_out, digr_ecc_ce_e_21_3_masked_out;
  input digr_ecc_ue_e_21_3_masked_out, digr_overflow_e_21_3_masked_out;
  input digr_p_err_e_22_3_masked_out, digr_ecc_ce_e_22_3_masked_out;
  input digr_ecc_ue_e_22_3_masked_out, digr_overflow_e_22_3_masked_out;
  input digr_p_err_e_23_3_masked_out, digr_ecc_ce_e_23_3_masked_out;
  input digr_ecc_ue_e_23_3_masked_out, digr_overflow_e_23_3_masked_out;
  input digr_p_err_e_0_4_masked_out, digr_ecc_ce_e_0_4_masked_out;
  input digr_ecc_ue_e_0_4_masked_out, digr_overflow_e_0_4_masked_out;
  input digr_p_err_e_1_4_masked_out, digr_ecc_ce_e_1_4_masked_out;
  input digr_ecc_ue_e_1_4_masked_out, digr_overflow_e_1_4_masked_out;
  input digr_p_err_e_2_4_masked_out, digr_ecc_ce_e_2_4_masked_out;
  input digr_ecc_ue_e_2_4_masked_out, digr_overflow_e_2_4_masked_out;
  input digr_p_err_e_3_4_masked_out, digr_ecc_ce_e_3_4_masked_out;
  input digr_ecc_ue_e_3_4_masked_out, digr_overflow_e_3_4_masked_out;
  input digr_p_err_e_4_4_masked_out, digr_ecc_ce_e_4_4_masked_out;
  input digr_ecc_ue_e_4_4_masked_out, digr_overflow_e_4_4_masked_out;
  input digr_p_err_e_5_4_masked_out, digr_ecc_ce_e_5_4_masked_out;
  input digr_ecc_ue_e_5_4_masked_out, digr_overflow_e_5_4_masked_out;
  input digr_p_err_e_6_4_masked_out, digr_ecc_ce_e_6_4_masked_out;
  input digr_ecc_ue_e_6_4_masked_out, digr_overflow_e_6_4_masked_out;
  input digr_p_err_e_7_4_masked_out, digr_ecc_ce_e_7_4_masked_out;
  input digr_ecc_ue_e_7_4_masked_out, digr_overflow_e_7_4_masked_out;
  input digr_p_err_e_8_4_masked_out, digr_ecc_ce_e_8_4_masked_out;
  input digr_ecc_ue_e_8_4_masked_out, digr_overflow_e_8_4_masked_out;
  input digr_p_err_e_9_4_masked_out, digr_ecc_ce_e_9_4_masked_out;
  input digr_ecc_ue_e_9_4_masked_out, digr_overflow_e_9_4_masked_out;
  input digr_p_err_e_10_4_masked_out, digr_ecc_ce_e_10_4_masked_out;
  input digr_ecc_ue_e_10_4_masked_out, digr_overflow_e_10_4_masked_out;
  input digr_p_err_e_11_4_masked_out, digr_ecc_ce_e_11_4_masked_out;
  input digr_ecc_ue_e_11_4_masked_out, digr_overflow_e_11_4_masked_out;
  input digr_p_err_e_12_4_masked_out, digr_ecc_ce_e_12_4_masked_out;
  input digr_ecc_ue_e_12_4_masked_out, digr_overflow_e_12_4_masked_out;
  input digr_p_err_e_13_4_masked_out, digr_ecc_ce_e_13_4_masked_out;
  input digr_ecc_ue_e_13_4_masked_out, digr_overflow_e_13_4_masked_out;
  input digr_p_err_e_14_4_masked_out, digr_ecc_ce_e_14_4_masked_out;
  input digr_ecc_ue_e_14_4_masked_out, digr_overflow_e_14_4_masked_out;
  input digr_p_err_e_15_4_masked_out, digr_ecc_ce_e_15_4_masked_out;
  input digr_ecc_ue_e_15_4_masked_out, digr_overflow_e_15_4_masked_out;
  input digr_p_err_e_16_4_masked_out, digr_ecc_ce_e_16_4_masked_out;
  input digr_ecc_ue_e_16_4_masked_out, digr_overflow_e_16_4_masked_out;
  input digr_p_err_e_17_4_masked_out, digr_ecc_ce_e_17_4_masked_out;
  input digr_ecc_ue_e_17_4_masked_out, digr_overflow_e_17_4_masked_out;
  input digr_p_err_e_18_4_masked_out, digr_ecc_ce_e_18_4_masked_out;
  input digr_ecc_ue_e_18_4_masked_out, digr_overflow_e_18_4_masked_out;
  input digr_p_err_e_19_4_masked_out, digr_ecc_ce_e_19_4_masked_out;
  input digr_ecc_ue_e_19_4_masked_out, digr_overflow_e_19_4_masked_out;
  input digr_p_err_e_20_4_masked_out, digr_ecc_ce_e_20_4_masked_out;
  input digr_ecc_ue_e_20_4_masked_out, digr_overflow_e_20_4_masked_out;
  input digr_p_err_e_21_4_masked_out, digr_ecc_ce_e_21_4_masked_out;
  input digr_ecc_ue_e_21_4_masked_out, digr_overflow_e_21_4_masked_out;
  input digr_p_err_e_22_4_masked_out, digr_ecc_ce_e_22_4_masked_out;
  input digr_ecc_ue_e_22_4_masked_out, digr_overflow_e_22_4_masked_out;
  input digr_p_err_e_23_4_masked_out, digr_ecc_ce_e_23_4_masked_out;
  input digr_ecc_ue_e_23_4_masked_out, digr_overflow_e_23_4_masked_out;
  input digr_p_err_e_0_5_masked_out, digr_ecc_ce_e_0_5_masked_out;
  input digr_ecc_ue_e_0_5_masked_out, digr_overflow_e_0_5_masked_out;
  input digr_p_err_e_1_5_masked_out, digr_ecc_ce_e_1_5_masked_out;
  input digr_ecc_ue_e_1_5_masked_out, digr_overflow_e_1_5_masked_out;
  input digr_p_err_e_2_5_masked_out, digr_ecc_ce_e_2_5_masked_out;
  input digr_ecc_ue_e_2_5_masked_out, digr_overflow_e_2_5_masked_out;
  input digr_p_err_e_3_5_masked_out, digr_ecc_ce_e_3_5_masked_out;
  input digr_ecc_ue_e_3_5_masked_out, digr_overflow_e_3_5_masked_out;
  input digr_p_err_e_4_5_masked_out, digr_ecc_ce_e_4_5_masked_out;
  input digr_ecc_ue_e_4_5_masked_out, digr_overflow_e_4_5_masked_out;
  input digr_p_err_e_5_5_masked_out, digr_ecc_ce_e_5_5_masked_out;
  input digr_ecc_ue_e_5_5_masked_out, digr_overflow_e_5_5_masked_out;
  input digr_p_err_e_6_5_masked_out, digr_ecc_ce_e_6_5_masked_out;
  input digr_ecc_ue_e_6_5_masked_out, digr_overflow_e_6_5_masked_out;
  input digr_p_err_e_7_5_masked_out, digr_ecc_ce_e_7_5_masked_out;
  input digr_ecc_ue_e_7_5_masked_out, digr_overflow_e_7_5_masked_out;
  input digr_p_err_e_8_5_masked_out, digr_ecc_ce_e_8_5_masked_out;
  input digr_ecc_ue_e_8_5_masked_out, digr_overflow_e_8_5_masked_out;
  input digr_p_err_e_9_5_masked_out, digr_ecc_ce_e_9_5_masked_out;
  input digr_ecc_ue_e_9_5_masked_out, digr_overflow_e_9_5_masked_out;
  input digr_p_err_e_10_5_masked_out, digr_ecc_ce_e_10_5_masked_out;
  input digr_ecc_ue_e_10_5_masked_out, digr_overflow_e_10_5_masked_out;
  input digr_p_err_e_11_5_masked_out, digr_ecc_ce_e_11_5_masked_out;
  input digr_ecc_ue_e_11_5_masked_out, digr_overflow_e_11_5_masked_out;
  input digr_p_err_e_12_5_masked_out, digr_ecc_ce_e_12_5_masked_out;
  input digr_ecc_ue_e_12_5_masked_out, digr_overflow_e_12_5_masked_out;
  input digr_p_err_e_13_5_masked_out, digr_ecc_ce_e_13_5_masked_out;
  input digr_ecc_ue_e_13_5_masked_out, digr_overflow_e_13_5_masked_out;
  input digr_p_err_e_14_5_masked_out, digr_ecc_ce_e_14_5_masked_out;
  input digr_ecc_ue_e_14_5_masked_out, digr_overflow_e_14_5_masked_out;
  input digr_p_err_e_15_5_masked_out, digr_ecc_ce_e_15_5_masked_out;
  input digr_ecc_ue_e_15_5_masked_out, digr_overflow_e_15_5_masked_out;
  input digr_p_err_e_16_5_masked_out, digr_ecc_ce_e_16_5_masked_out;
  input digr_ecc_ue_e_16_5_masked_out, digr_overflow_e_16_5_masked_out;
  input digr_p_err_e_17_5_masked_out, digr_ecc_ce_e_17_5_masked_out;
  input digr_ecc_ue_e_17_5_masked_out, digr_overflow_e_17_5_masked_out;
  input digr_p_err_e_18_5_masked_out, digr_ecc_ce_e_18_5_masked_out;
  input digr_ecc_ue_e_18_5_masked_out, digr_overflow_e_18_5_masked_out;
  input digr_p_err_e_19_5_masked_out, digr_ecc_ce_e_19_5_masked_out;
  input digr_ecc_ue_e_19_5_masked_out, digr_overflow_e_19_5_masked_out;
  input digr_p_err_e_20_5_masked_out, digr_ecc_ce_e_20_5_masked_out;
  input digr_ecc_ue_e_20_5_masked_out, digr_overflow_e_20_5_masked_out;
  input digr_p_err_e_21_5_masked_out, digr_ecc_ce_e_21_5_masked_out;
  input digr_ecc_ue_e_21_5_masked_out, digr_overflow_e_21_5_masked_out;
  input digr_p_err_e_22_5_masked_out, digr_ecc_ce_e_22_5_masked_out;
  input digr_ecc_ue_e_22_5_masked_out, digr_overflow_e_22_5_masked_out;
  input digr_p_err_e_23_5_masked_out, digr_ecc_ce_e_23_5_masked_out;
  input digr_ecc_ue_e_23_5_masked_out, digr_overflow_e_23_5_masked_out;
  input digr_p_err_e_0_6_masked_out, digr_ecc_ce_e_0_6_masked_out;
  input digr_ecc_ue_e_0_6_masked_out, digr_overflow_e_0_6_masked_out;
  input digr_p_err_e_1_6_masked_out, digr_ecc_ce_e_1_6_masked_out;
  input digr_ecc_ue_e_1_6_masked_out, digr_overflow_e_1_6_masked_out;
  input digr_p_err_e_2_6_masked_out, digr_ecc_ce_e_2_6_masked_out;
  input digr_ecc_ue_e_2_6_masked_out, digr_overflow_e_2_6_masked_out;
  input digr_p_err_e_3_6_masked_out, digr_ecc_ce_e_3_6_masked_out;
  input digr_ecc_ue_e_3_6_masked_out, digr_overflow_e_3_6_masked_out;
  input digr_p_err_e_4_6_masked_out, digr_ecc_ce_e_4_6_masked_out;
  input digr_ecc_ue_e_4_6_masked_out, digr_overflow_e_4_6_masked_out;
  input digr_p_err_e_5_6_masked_out, digr_ecc_ce_e_5_6_masked_out;
  input digr_ecc_ue_e_5_6_masked_out, digr_overflow_e_5_6_masked_out;
  input digr_p_err_e_6_6_masked_out, digr_ecc_ce_e_6_6_masked_out;
  input digr_ecc_ue_e_6_6_masked_out, digr_overflow_e_6_6_masked_out;
  input digr_p_err_e_7_6_masked_out, digr_ecc_ce_e_7_6_masked_out;
  input digr_ecc_ue_e_7_6_masked_out, digr_overflow_e_7_6_masked_out;
  input digr_p_err_e_8_6_masked_out, digr_ecc_ce_e_8_6_masked_out;
  input digr_ecc_ue_e_8_6_masked_out, digr_overflow_e_8_6_masked_out;
  input digr_p_err_e_9_6_masked_out, digr_ecc_ce_e_9_6_masked_out;
  input digr_ecc_ue_e_9_6_masked_out, digr_overflow_e_9_6_masked_out;
  input digr_p_err_e_10_6_masked_out, digr_ecc_ce_e_10_6_masked_out;
  input digr_ecc_ue_e_10_6_masked_out, digr_overflow_e_10_6_masked_out;
  input digr_p_err_e_11_6_masked_out, digr_ecc_ce_e_11_6_masked_out;
  input digr_ecc_ue_e_11_6_masked_out, digr_overflow_e_11_6_masked_out;
  input digr_p_err_e_12_6_masked_out, digr_ecc_ce_e_12_6_masked_out;
  input digr_ecc_ue_e_12_6_masked_out, digr_overflow_e_12_6_masked_out;
  input digr_p_err_e_13_6_masked_out, digr_ecc_ce_e_13_6_masked_out;
  input digr_ecc_ue_e_13_6_masked_out, digr_overflow_e_13_6_masked_out;
  input digr_p_err_e_14_6_masked_out, digr_ecc_ce_e_14_6_masked_out;
  input digr_ecc_ue_e_14_6_masked_out, digr_overflow_e_14_6_masked_out;
  input digr_p_err_e_15_6_masked_out, digr_ecc_ce_e_15_6_masked_out;
  input digr_ecc_ue_e_15_6_masked_out, digr_overflow_e_15_6_masked_out;
  input digr_p_err_e_16_6_masked_out, digr_ecc_ce_e_16_6_masked_out;
  input digr_ecc_ue_e_16_6_masked_out, digr_overflow_e_16_6_masked_out;
  input digr_p_err_e_17_6_masked_out, digr_ecc_ce_e_17_6_masked_out;
  input digr_ecc_ue_e_17_6_masked_out, digr_overflow_e_17_6_masked_out;
  input digr_p_err_e_18_6_masked_out, digr_ecc_ce_e_18_6_masked_out;
  input digr_ecc_ue_e_18_6_masked_out, digr_overflow_e_18_6_masked_out;
  input digr_p_err_e_19_6_masked_out, digr_ecc_ce_e_19_6_masked_out;
  input digr_ecc_ue_e_19_6_masked_out, digr_overflow_e_19_6_masked_out;
  input digr_p_err_e_20_6_masked_out, digr_ecc_ce_e_20_6_masked_out;
  input digr_ecc_ue_e_20_6_masked_out, digr_overflow_e_20_6_masked_out;
  input digr_p_err_e_21_6_masked_out, digr_ecc_ce_e_21_6_masked_out;
  input digr_ecc_ue_e_21_6_masked_out, digr_overflow_e_21_6_masked_out;
  input digr_p_err_e_22_6_masked_out, digr_ecc_ce_e_22_6_masked_out;
  input digr_ecc_ue_e_22_6_masked_out, digr_overflow_e_22_6_masked_out;
  input digr_p_err_e_23_6_masked_out, digr_ecc_ce_e_23_6_masked_out;
  input digr_ecc_ue_e_23_6_masked_out, digr_overflow_e_23_6_masked_out;
  input digr_p_err_e_0_7_masked_out, digr_ecc_ce_e_0_7_masked_out;
  input digr_ecc_ue_e_0_7_masked_out, digr_overflow_e_0_7_masked_out;
  input digr_p_err_e_1_7_masked_out, digr_ecc_ce_e_1_7_masked_out;
  input digr_ecc_ue_e_1_7_masked_out, digr_overflow_e_1_7_masked_out;
  input digr_p_err_e_2_7_masked_out, digr_ecc_ce_e_2_7_masked_out;
  input digr_ecc_ue_e_2_7_masked_out, digr_overflow_e_2_7_masked_out;
  input digr_p_err_e_3_7_masked_out, digr_ecc_ce_e_3_7_masked_out;
  input digr_ecc_ue_e_3_7_masked_out, digr_overflow_e_3_7_masked_out;
  input digr_p_err_e_4_7_masked_out, digr_ecc_ce_e_4_7_masked_out;
  input digr_ecc_ue_e_4_7_masked_out, digr_overflow_e_4_7_masked_out;
  input digr_p_err_e_5_7_masked_out, digr_ecc_ce_e_5_7_masked_out;
  input digr_ecc_ue_e_5_7_masked_out, digr_overflow_e_5_7_masked_out;
  input digr_p_err_e_6_7_masked_out, digr_ecc_ce_e_6_7_masked_out;
  input digr_ecc_ue_e_6_7_masked_out, digr_overflow_e_6_7_masked_out;
  input digr_p_err_e_7_7_masked_out, digr_ecc_ce_e_7_7_masked_out;
  input digr_ecc_ue_e_7_7_masked_out, digr_overflow_e_7_7_masked_out;
  input digr_p_err_e_8_7_masked_out, digr_ecc_ce_e_8_7_masked_out;
  input digr_ecc_ue_e_8_7_masked_out, digr_overflow_e_8_7_masked_out;
  input digr_p_err_e_9_7_masked_out, digr_ecc_ce_e_9_7_masked_out;
  input digr_ecc_ue_e_9_7_masked_out, digr_overflow_e_9_7_masked_out;
  input digr_p_err_e_10_7_masked_out, digr_ecc_ce_e_10_7_masked_out;
  input digr_ecc_ue_e_10_7_masked_out, digr_overflow_e_10_7_masked_out;
  input digr_p_err_e_11_7_masked_out, digr_ecc_ce_e_11_7_masked_out;
  input digr_ecc_ue_e_11_7_masked_out, digr_overflow_e_11_7_masked_out;
  input digr_p_err_e_12_7_masked_out, digr_ecc_ce_e_12_7_masked_out;
  input digr_ecc_ue_e_12_7_masked_out, digr_overflow_e_12_7_masked_out;
  input digr_p_err_e_13_7_masked_out, digr_ecc_ce_e_13_7_masked_out;
  input digr_ecc_ue_e_13_7_masked_out, digr_overflow_e_13_7_masked_out;
  input digr_p_err_e_14_7_masked_out, digr_ecc_ce_e_14_7_masked_out;
  input digr_ecc_ue_e_14_7_masked_out, digr_overflow_e_14_7_masked_out;
  input digr_p_err_e_15_7_masked_out, digr_ecc_ce_e_15_7_masked_out;
  input digr_ecc_ue_e_15_7_masked_out, digr_overflow_e_15_7_masked_out;
  input digr_p_err_e_16_7_masked_out, digr_ecc_ce_e_16_7_masked_out;
  input digr_ecc_ue_e_16_7_masked_out, digr_overflow_e_16_7_masked_out;
  input digr_p_err_e_17_7_masked_out, digr_ecc_ce_e_17_7_masked_out;
  input digr_ecc_ue_e_17_7_masked_out, digr_overflow_e_17_7_masked_out;
  input digr_p_err_e_18_7_masked_out, digr_ecc_ce_e_18_7_masked_out;
  input digr_ecc_ue_e_18_7_masked_out, digr_overflow_e_18_7_masked_out;
  input digr_p_err_e_19_7_masked_out, digr_ecc_ce_e_19_7_masked_out;
  input digr_ecc_ue_e_19_7_masked_out, digr_overflow_e_19_7_masked_out;
  input digr_p_err_e_20_7_masked_out, digr_ecc_ce_e_20_7_masked_out;
  input digr_ecc_ue_e_20_7_masked_out, digr_overflow_e_20_7_masked_out;
  input digr_p_err_e_21_7_masked_out, digr_ecc_ce_e_21_7_masked_out;
  input digr_ecc_ue_e_21_7_masked_out, digr_overflow_e_21_7_masked_out;
  input digr_p_err_e_22_7_masked_out, digr_ecc_ce_e_22_7_masked_out;
  input digr_ecc_ue_e_22_7_masked_out, digr_overflow_e_22_7_masked_out;
  input digr_p_err_e_23_7_masked_out, digr_ecc_ce_e_23_7_masked_out;
  input digr_ecc_ue_e_23_7_masked_out, digr_overflow_e_23_7_masked_out;
  input digr_p_err_e_0_8_masked_out, digr_ecc_ce_e_0_8_masked_out;
  input digr_ecc_ue_e_0_8_masked_out, digr_overflow_e_0_8_masked_out;
  input digr_p_err_e_1_8_masked_out, digr_ecc_ce_e_1_8_masked_out;
  input digr_ecc_ue_e_1_8_masked_out, digr_overflow_e_1_8_masked_out;
  input digr_p_err_e_2_8_masked_out, digr_ecc_ce_e_2_8_masked_out;
  input digr_ecc_ue_e_2_8_masked_out, digr_overflow_e_2_8_masked_out;
  input digr_p_err_e_3_8_masked_out, digr_ecc_ce_e_3_8_masked_out;
  input digr_ecc_ue_e_3_8_masked_out, digr_overflow_e_3_8_masked_out;
  input digr_p_err_e_4_8_masked_out, digr_ecc_ce_e_4_8_masked_out;
  input digr_ecc_ue_e_4_8_masked_out, digr_overflow_e_4_8_masked_out;
  input digr_p_err_e_5_8_masked_out, digr_ecc_ce_e_5_8_masked_out;
  input digr_ecc_ue_e_5_8_masked_out, digr_overflow_e_5_8_masked_out;
  input digr_p_err_e_6_8_masked_out, digr_ecc_ce_e_6_8_masked_out;
  input digr_ecc_ue_e_6_8_masked_out, digr_overflow_e_6_8_masked_out;
  input digr_p_err_e_7_8_masked_out, digr_ecc_ce_e_7_8_masked_out;
  input digr_ecc_ue_e_7_8_masked_out, digr_overflow_e_7_8_masked_out;
  input digr_p_err_e_8_8_masked_out, digr_ecc_ce_e_8_8_masked_out;
  input digr_ecc_ue_e_8_8_masked_out, digr_overflow_e_8_8_masked_out;
  input digr_p_err_e_9_8_masked_out, digr_ecc_ce_e_9_8_masked_out;
  input digr_ecc_ue_e_9_8_masked_out, digr_overflow_e_9_8_masked_out;
  input digr_p_err_e_10_8_masked_out, digr_ecc_ce_e_10_8_masked_out;
  input digr_ecc_ue_e_10_8_masked_out, digr_overflow_e_10_8_masked_out;
  input digr_p_err_e_11_8_masked_out, digr_ecc_ce_e_11_8_masked_out;
  input digr_ecc_ue_e_11_8_masked_out, digr_overflow_e_11_8_masked_out;
  input digr_p_err_e_12_8_masked_out, digr_ecc_ce_e_12_8_masked_out;
  input digr_ecc_ue_e_12_8_masked_out, digr_overflow_e_12_8_masked_out;
  input digr_p_err_e_13_8_masked_out, digr_ecc_ce_e_13_8_masked_out;
  input digr_ecc_ue_e_13_8_masked_out, digr_overflow_e_13_8_masked_out;
  input digr_p_err_e_14_8_masked_out, digr_ecc_ce_e_14_8_masked_out;
  input digr_ecc_ue_e_14_8_masked_out, digr_overflow_e_14_8_masked_out;
  input digr_p_err_e_15_8_masked_out, digr_ecc_ce_e_15_8_masked_out;
  input digr_ecc_ue_e_15_8_masked_out, digr_overflow_e_15_8_masked_out;
  input digr_p_err_e_16_8_masked_out, digr_ecc_ce_e_16_8_masked_out;
  input digr_ecc_ue_e_16_8_masked_out, digr_overflow_e_16_8_masked_out;
  input digr_p_err_e_17_8_masked_out, digr_ecc_ce_e_17_8_masked_out;
  input digr_ecc_ue_e_17_8_masked_out, digr_overflow_e_17_8_masked_out;
  input digr_p_err_e_18_8_masked_out, digr_ecc_ce_e_18_8_masked_out;
  input digr_ecc_ue_e_18_8_masked_out, digr_overflow_e_18_8_masked_out;
  input digr_p_err_e_19_8_masked_out, digr_ecc_ce_e_19_8_masked_out;
  input digr_ecc_ue_e_19_8_masked_out, digr_overflow_e_19_8_masked_out;
  input digr_p_err_e_20_8_masked_out, digr_ecc_ce_e_20_8_masked_out;
  input digr_ecc_ue_e_20_8_masked_out, digr_overflow_e_20_8_masked_out;
  input digr_p_err_e_21_8_masked_out, digr_ecc_ce_e_21_8_masked_out;
  input digr_ecc_ue_e_21_8_masked_out, digr_overflow_e_21_8_masked_out;
  input digr_p_err_e_22_8_masked_out, digr_ecc_ce_e_22_8_masked_out;
  input digr_ecc_ue_e_22_8_masked_out, digr_overflow_e_22_8_masked_out;
  input digr_p_err_e_23_8_masked_out, digr_ecc_ce_e_23_8_masked_out;
  input digr_ecc_ue_e_23_8_masked_out, digr_overflow_e_23_8_masked_out;
  output digr_sum_e;

assign digr_sum_e = ( digr_p_err_e_0_1_masked_out || digr_ecc_ce_e_0_1_masked_out
                    || digr_ecc_ue_e_0_1_masked_out || digr_overflow_e_0_1_masked_out
                    || digr_p_err_e_1_1_masked_out || digr_ecc_ce_e_1_1_masked_out
                    || digr_ecc_ue_e_1_1_masked_out || digr_overflow_e_1_1_masked_out
                    || digr_p_err_e_2_1_masked_out || digr_ecc_ce_e_2_1_masked_out
                    || digr_ecc_ue_e_2_1_masked_out || digr_overflow_e_2_1_masked_out
                    || digr_p_err_e_3_1_masked_out || digr_ecc_ce_e_3_1_masked_out
                    || digr_ecc_ue_e_3_1_masked_out || digr_overflow_e_3_1_masked_out
                    || digr_p_err_e_4_1_masked_out || digr_ecc_ce_e_4_1_masked_out
                    || digr_ecc_ue_e_4_1_masked_out || digr_overflow_e_4_1_masked_out
                    || digr_p_err_e_5_1_masked_out || digr_ecc_ce_e_5_1_masked_out
                    || digr_ecc_ue_e_5_1_masked_out || digr_overflow_e_5_1_masked_out
                    || digr_p_err_e_6_1_masked_out || digr_ecc_ce_e_6_1_masked_out
                    || digr_ecc_ue_e_6_1_masked_out || digr_overflow_e_6_1_masked_out
                    || digr_p_err_e_7_1_masked_out || digr_ecc_ce_e_7_1_masked_out
                    || digr_ecc_ue_e_7_1_masked_out || digr_overflow_e_7_1_masked_out
                    || digr_p_err_e_8_1_masked_out || digr_ecc_ce_e_8_1_masked_out
                    || digr_ecc_ue_e_8_1_masked_out || digr_overflow_e_8_1_masked_out
                    || digr_p_err_e_9_1_masked_out || digr_ecc_ce_e_9_1_masked_out
                    || digr_ecc_ue_e_9_1_masked_out || digr_overflow_e_9_1_masked_out
                    || digr_p_err_e_10_1_masked_out || digr_ecc_ce_e_10_1_masked_out
                    || digr_ecc_ue_e_10_1_masked_out || digr_overflow_e_10_1_masked_out
                    || digr_p_err_e_11_1_masked_out || digr_ecc_ce_e_11_1_masked_out
                    || digr_ecc_ue_e_11_1_masked_out || digr_overflow_e_11_1_masked_out
                    || digr_p_err_e_12_1_masked_out || digr_ecc_ce_e_12_1_masked_out
                    || digr_ecc_ue_e_12_1_masked_out || digr_overflow_e_12_1_masked_out
                    || digr_p_err_e_13_1_masked_out || digr_ecc_ce_e_13_1_masked_out
                    || digr_ecc_ue_e_13_1_masked_out || digr_overflow_e_13_1_masked_out
                    || digr_p_err_e_14_1_masked_out || digr_ecc_ce_e_14_1_masked_out
                    || digr_ecc_ue_e_14_1_masked_out || digr_overflow_e_14_1_masked_out
                    || digr_p_err_e_15_1_masked_out || digr_ecc_ce_e_15_1_masked_out
                    || digr_ecc_ue_e_15_1_masked_out || digr_overflow_e_15_1_masked_out
                    || digr_p_err_e_16_1_masked_out || digr_ecc_ce_e_16_1_masked_out
                    || digr_ecc_ue_e_16_1_masked_out || digr_overflow_e_16_1_masked_out
                    || digr_p_err_e_17_1_masked_out || digr_ecc_ce_e_17_1_masked_out
                    || digr_ecc_ue_e_17_1_masked_out || digr_overflow_e_17_1_masked_out
                    || digr_p_err_e_18_1_masked_out || digr_ecc_ce_e_18_1_masked_out
                    || digr_ecc_ue_e_18_1_masked_out || digr_overflow_e_18_1_masked_out
                    || digr_p_err_e_19_1_masked_out || digr_ecc_ce_e_19_1_masked_out
                    || digr_ecc_ue_e_19_1_masked_out || digr_overflow_e_19_1_masked_out
                    || digr_p_err_e_20_1_masked_out || digr_ecc_ce_e_20_1_masked_out
                    || digr_ecc_ue_e_20_1_masked_out || digr_overflow_e_20_1_masked_out
                    || digr_p_err_e_21_1_masked_out || digr_ecc_ce_e_21_1_masked_out
                    || digr_ecc_ue_e_21_1_masked_out || digr_overflow_e_21_1_masked_out
                    || digr_p_err_e_22_1_masked_out || digr_ecc_ce_e_22_1_masked_out
                    || digr_ecc_ue_e_22_1_masked_out || digr_overflow_e_22_1_masked_out
                    || digr_p_err_e_23_1_masked_out || digr_ecc_ce_e_23_1_masked_out
                    || digr_ecc_ue_e_23_1_masked_out || digr_overflow_e_23_1_masked_out
                    || digr_p_err_e_0_2_masked_out || digr_ecc_ce_e_0_2_masked_out
                    || digr_ecc_ue_e_0_2_masked_out || digr_overflow_e_0_2_masked_out
                    || digr_p_err_e_1_2_masked_out || digr_ecc_ce_e_1_2_masked_out
                    || digr_ecc_ue_e_1_2_masked_out || digr_overflow_e_1_2_masked_out
                    || digr_p_err_e_2_2_masked_out || digr_ecc_ce_e_2_2_masked_out
                    || digr_ecc_ue_e_2_2_masked_out || digr_overflow_e_2_2_masked_out
                    || digr_p_err_e_3_2_masked_out || digr_ecc_ce_e_3_2_masked_out
                    || digr_ecc_ue_e_3_2_masked_out || digr_overflow_e_3_2_masked_out
                    || digr_p_err_e_4_2_masked_out || digr_ecc_ce_e_4_2_masked_out
                    || digr_ecc_ue_e_4_2_masked_out || digr_overflow_e_4_2_masked_out
                    || digr_p_err_e_5_2_masked_out || digr_ecc_ce_e_5_2_masked_out
                    || digr_ecc_ue_e_5_2_masked_out || digr_overflow_e_5_2_masked_out
                    || digr_p_err_e_6_2_masked_out || digr_ecc_ce_e_6_2_masked_out
                    || digr_ecc_ue_e_6_2_masked_out || digr_overflow_e_6_2_masked_out
                    || digr_p_err_e_7_2_masked_out || digr_ecc_ce_e_7_2_masked_out
                    || digr_ecc_ue_e_7_2_masked_out || digr_overflow_e_7_2_masked_out
                    || digr_p_err_e_8_2_masked_out || digr_ecc_ce_e_8_2_masked_out
                    || digr_ecc_ue_e_8_2_masked_out || digr_overflow_e_8_2_masked_out
                    || digr_p_err_e_9_2_masked_out || digr_ecc_ce_e_9_2_masked_out
                    || digr_ecc_ue_e_9_2_masked_out || digr_overflow_e_9_2_masked_out
                    || digr_p_err_e_10_2_masked_out || digr_ecc_ce_e_10_2_masked_out
                    || digr_ecc_ue_e_10_2_masked_out || digr_overflow_e_10_2_masked_out
                    || digr_p_err_e_11_2_masked_out || digr_ecc_ce_e_11_2_masked_out
                    || digr_ecc_ue_e_11_2_masked_out || digr_overflow_e_11_2_masked_out
                    || digr_p_err_e_12_2_masked_out || digr_ecc_ce_e_12_2_masked_out
                    || digr_ecc_ue_e_12_2_masked_out || digr_overflow_e_12_2_masked_out
                    || digr_p_err_e_13_2_masked_out || digr_ecc_ce_e_13_2_masked_out
                    || digr_ecc_ue_e_13_2_masked_out || digr_overflow_e_13_2_masked_out
                    || digr_p_err_e_14_2_masked_out || digr_ecc_ce_e_14_2_masked_out
                    || digr_ecc_ue_e_14_2_masked_out || digr_overflow_e_14_2_masked_out
                    || digr_p_err_e_15_2_masked_out || digr_ecc_ce_e_15_2_masked_out
                    || digr_ecc_ue_e_15_2_masked_out || digr_overflow_e_15_2_masked_out
                    || digr_p_err_e_16_2_masked_out || digr_ecc_ce_e_16_2_masked_out
                    || digr_ecc_ue_e_16_2_masked_out || digr_overflow_e_16_2_masked_out
                    || digr_p_err_e_17_2_masked_out || digr_ecc_ce_e_17_2_masked_out
                    || digr_ecc_ue_e_17_2_masked_out || digr_overflow_e_17_2_masked_out
                    || digr_p_err_e_18_2_masked_out || digr_ecc_ce_e_18_2_masked_out
                    || digr_ecc_ue_e_18_2_masked_out || digr_overflow_e_18_2_masked_out
                    || digr_p_err_e_19_2_masked_out || digr_ecc_ce_e_19_2_masked_out
                    || digr_ecc_ue_e_19_2_masked_out || digr_overflow_e_19_2_masked_out
                    || digr_p_err_e_20_2_masked_out || digr_ecc_ce_e_20_2_masked_out
                    || digr_ecc_ue_e_20_2_masked_out || digr_overflow_e_20_2_masked_out
                    || digr_p_err_e_21_2_masked_out || digr_ecc_ce_e_21_2_masked_out
                    || digr_ecc_ue_e_21_2_masked_out || digr_overflow_e_21_2_masked_out
                    || digr_p_err_e_22_2_masked_out || digr_ecc_ce_e_22_2_masked_out
                    || digr_ecc_ue_e_22_2_masked_out || digr_overflow_e_22_2_masked_out
                    || digr_p_err_e_23_2_masked_out || digr_ecc_ce_e_23_2_masked_out
                    || digr_ecc_ue_e_23_2_masked_out || digr_overflow_e_23_2_masked_out
                    || digr_p_err_e_0_3_masked_out || digr_ecc_ce_e_0_3_masked_out
                    || digr_ecc_ue_e_0_3_masked_out || digr_overflow_e_0_3_masked_out
                    || digr_p_err_e_1_3_masked_out || digr_ecc_ce_e_1_3_masked_out
                    || digr_ecc_ue_e_1_3_masked_out || digr_overflow_e_1_3_masked_out
                    || digr_p_err_e_2_3_masked_out || digr_ecc_ce_e_2_3_masked_out
                    || digr_ecc_ue_e_2_3_masked_out || digr_overflow_e_2_3_masked_out
                    || digr_p_err_e_3_3_masked_out || digr_ecc_ce_e_3_3_masked_out
                    || digr_ecc_ue_e_3_3_masked_out || digr_overflow_e_3_3_masked_out
                    || digr_p_err_e_4_3_masked_out || digr_ecc_ce_e_4_3_masked_out
                    || digr_ecc_ue_e_4_3_masked_out || digr_overflow_e_4_3_masked_out
                    || digr_p_err_e_5_3_masked_out || digr_ecc_ce_e_5_3_masked_out
                    || digr_ecc_ue_e_5_3_masked_out || digr_overflow_e_5_3_masked_out
                    || digr_p_err_e_6_3_masked_out || digr_ecc_ce_e_6_3_masked_out
                    || digr_ecc_ue_e_6_3_masked_out || digr_overflow_e_6_3_masked_out
                    || digr_p_err_e_7_3_masked_out || digr_ecc_ce_e_7_3_masked_out
                    || digr_ecc_ue_e_7_3_masked_out || digr_overflow_e_7_3_masked_out
                    || digr_p_err_e_8_3_masked_out || digr_ecc_ce_e_8_3_masked_out
                    || digr_ecc_ue_e_8_3_masked_out || digr_overflow_e_8_3_masked_out
                    || digr_p_err_e_9_3_masked_out || digr_ecc_ce_e_9_3_masked_out
                    || digr_ecc_ue_e_9_3_masked_out || digr_overflow_e_9_3_masked_out
                    || digr_p_err_e_10_3_masked_out || digr_ecc_ce_e_10_3_masked_out
                    || digr_ecc_ue_e_10_3_masked_out || digr_overflow_e_10_3_masked_out
                    || digr_p_err_e_11_3_masked_out || digr_ecc_ce_e_11_3_masked_out
                    || digr_ecc_ue_e_11_3_masked_out || digr_overflow_e_11_3_masked_out
                    || digr_p_err_e_12_3_masked_out || digr_ecc_ce_e_12_3_masked_out
                    || digr_ecc_ue_e_12_3_masked_out || digr_overflow_e_12_3_masked_out
                    || digr_p_err_e_13_3_masked_out || digr_ecc_ce_e_13_3_masked_out
                    || digr_ecc_ue_e_13_3_masked_out || digr_overflow_e_13_3_masked_out
                    || digr_p_err_e_14_3_masked_out || digr_ecc_ce_e_14_3_masked_out
                    || digr_ecc_ue_e_14_3_masked_out || digr_overflow_e_14_3_masked_out
                    || digr_p_err_e_15_3_masked_out || digr_ecc_ce_e_15_3_masked_out
                    || digr_ecc_ue_e_15_3_masked_out || digr_overflow_e_15_3_masked_out
                    || digr_p_err_e_16_3_masked_out || digr_ecc_ce_e_16_3_masked_out
                    || digr_ecc_ue_e_16_3_masked_out || digr_overflow_e_16_3_masked_out
                    || digr_p_err_e_17_3_masked_out || digr_ecc_ce_e_17_3_masked_out
                    || digr_ecc_ue_e_17_3_masked_out || digr_overflow_e_17_3_masked_out
                    || digr_p_err_e_18_3_masked_out || digr_ecc_ce_e_18_3_masked_out
                    || digr_ecc_ue_e_18_3_masked_out || digr_overflow_e_18_3_masked_out
                    || digr_p_err_e_19_3_masked_out || digr_ecc_ce_e_19_3_masked_out
                    || digr_ecc_ue_e_19_3_masked_out || digr_overflow_e_19_3_masked_out
                    || digr_p_err_e_20_3_masked_out || digr_ecc_ce_e_20_3_masked_out
                    || digr_ecc_ue_e_20_3_masked_out || digr_overflow_e_20_3_masked_out
                    || digr_p_err_e_21_3_masked_out || digr_ecc_ce_e_21_3_masked_out
                    || digr_ecc_ue_e_21_3_masked_out || digr_overflow_e_21_3_masked_out
                    || digr_p_err_e_22_3_masked_out || digr_ecc_ce_e_22_3_masked_out
                    || digr_ecc_ue_e_22_3_masked_out || digr_overflow_e_22_3_masked_out
                    || digr_p_err_e_23_3_masked_out || digr_ecc_ce_e_23_3_masked_out
                    || digr_ecc_ue_e_23_3_masked_out || digr_overflow_e_23_3_masked_out
                    || digr_p_err_e_0_4_masked_out || digr_ecc_ce_e_0_4_masked_out
                    || digr_ecc_ue_e_0_4_masked_out || digr_overflow_e_0_4_masked_out
                    || digr_p_err_e_1_4_masked_out || digr_ecc_ce_e_1_4_masked_out
                    || digr_ecc_ue_e_1_4_masked_out || digr_overflow_e_1_4_masked_out
                    || digr_p_err_e_2_4_masked_out || digr_ecc_ce_e_2_4_masked_out
                    || digr_ecc_ue_e_2_4_masked_out || digr_overflow_e_2_4_masked_out
                    || digr_p_err_e_3_4_masked_out || digr_ecc_ce_e_3_4_masked_out
                    || digr_ecc_ue_e_3_4_masked_out || digr_overflow_e_3_4_masked_out
                    || digr_p_err_e_4_4_masked_out || digr_ecc_ce_e_4_4_masked_out
                    || digr_ecc_ue_e_4_4_masked_out || digr_overflow_e_4_4_masked_out
                    || digr_p_err_e_5_4_masked_out || digr_ecc_ce_e_5_4_masked_out
                    || digr_ecc_ue_e_5_4_masked_out || digr_overflow_e_5_4_masked_out
                    || digr_p_err_e_6_4_masked_out || digr_ecc_ce_e_6_4_masked_out
                    || digr_ecc_ue_e_6_4_masked_out || digr_overflow_e_6_4_masked_out
                    || digr_p_err_e_7_4_masked_out || digr_ecc_ce_e_7_4_masked_out
                    || digr_ecc_ue_e_7_4_masked_out || digr_overflow_e_7_4_masked_out
                    || digr_p_err_e_8_4_masked_out || digr_ecc_ce_e_8_4_masked_out
                    || digr_ecc_ue_e_8_4_masked_out || digr_overflow_e_8_4_masked_out
                    || digr_p_err_e_9_4_masked_out || digr_ecc_ce_e_9_4_masked_out
                    || digr_ecc_ue_e_9_4_masked_out || digr_overflow_e_9_4_masked_out
                    || digr_p_err_e_10_4_masked_out || digr_ecc_ce_e_10_4_masked_out
                    || digr_ecc_ue_e_10_4_masked_out || digr_overflow_e_10_4_masked_out
                    || digr_p_err_e_11_4_masked_out || digr_ecc_ce_e_11_4_masked_out
                    || digr_ecc_ue_e_11_4_masked_out || digr_overflow_e_11_4_masked_out
                    || digr_p_err_e_12_4_masked_out || digr_ecc_ce_e_12_4_masked_out
                    || digr_ecc_ue_e_12_4_masked_out || digr_overflow_e_12_4_masked_out
                    || digr_p_err_e_13_4_masked_out || digr_ecc_ce_e_13_4_masked_out
                    || digr_ecc_ue_e_13_4_masked_out || digr_overflow_e_13_4_masked_out
                    || digr_p_err_e_14_4_masked_out || digr_ecc_ce_e_14_4_masked_out
                    || digr_ecc_ue_e_14_4_masked_out || digr_overflow_e_14_4_masked_out
                    || digr_p_err_e_15_4_masked_out || digr_ecc_ce_e_15_4_masked_out
                    || digr_ecc_ue_e_15_4_masked_out || digr_overflow_e_15_4_masked_out
                    || digr_p_err_e_16_4_masked_out || digr_ecc_ce_e_16_4_masked_out
                    || digr_ecc_ue_e_16_4_masked_out || digr_overflow_e_16_4_masked_out
                    || digr_p_err_e_17_4_masked_out || digr_ecc_ce_e_17_4_masked_out
                    || digr_ecc_ue_e_17_4_masked_out || digr_overflow_e_17_4_masked_out
                    || digr_p_err_e_18_4_masked_out || digr_ecc_ce_e_18_4_masked_out
                    || digr_ecc_ue_e_18_4_masked_out || digr_overflow_e_18_4_masked_out
                    || digr_p_err_e_19_4_masked_out || digr_ecc_ce_e_19_4_masked_out
                    || digr_ecc_ue_e_19_4_masked_out || digr_overflow_e_19_4_masked_out
                    || digr_p_err_e_20_4_masked_out || digr_ecc_ce_e_20_4_masked_out
                    || digr_ecc_ue_e_20_4_masked_out || digr_overflow_e_20_4_masked_out
                    || digr_p_err_e_21_4_masked_out || digr_ecc_ce_e_21_4_masked_out
                    || digr_ecc_ue_e_21_4_masked_out || digr_overflow_e_21_4_masked_out
                    || digr_p_err_e_22_4_masked_out || digr_ecc_ce_e_22_4_masked_out
                    || digr_ecc_ue_e_22_4_masked_out || digr_overflow_e_22_4_masked_out
                    || digr_p_err_e_23_4_masked_out || digr_ecc_ce_e_23_4_masked_out
                    || digr_ecc_ue_e_23_4_masked_out || digr_overflow_e_23_4_masked_out
                    || digr_p_err_e_0_5_masked_out || digr_ecc_ce_e_0_5_masked_out
                    || digr_ecc_ue_e_0_5_masked_out || digr_overflow_e_0_5_masked_out
                    || digr_p_err_e_1_5_masked_out || digr_ecc_ce_e_1_5_masked_out
                    || digr_ecc_ue_e_1_5_masked_out || digr_overflow_e_1_5_masked_out
                    || digr_p_err_e_2_5_masked_out || digr_ecc_ce_e_2_5_masked_out
                    || digr_ecc_ue_e_2_5_masked_out || digr_overflow_e_2_5_masked_out
                    || digr_p_err_e_3_5_masked_out || digr_ecc_ce_e_3_5_masked_out
                    || digr_ecc_ue_e_3_5_masked_out || digr_overflow_e_3_5_masked_out
                    || digr_p_err_e_4_5_masked_out || digr_ecc_ce_e_4_5_masked_out
                    || digr_ecc_ue_e_4_5_masked_out || digr_overflow_e_4_5_masked_out
                    || digr_p_err_e_5_5_masked_out || digr_ecc_ce_e_5_5_masked_out
                    || digr_ecc_ue_e_5_5_masked_out || digr_overflow_e_5_5_masked_out
                    || digr_p_err_e_6_5_masked_out || digr_ecc_ce_e_6_5_masked_out
                    || digr_ecc_ue_e_6_5_masked_out || digr_overflow_e_6_5_masked_out
                    || digr_p_err_e_7_5_masked_out || digr_ecc_ce_e_7_5_masked_out
                    || digr_ecc_ue_e_7_5_masked_out || digr_overflow_e_7_5_masked_out
                    || digr_p_err_e_8_5_masked_out || digr_ecc_ce_e_8_5_masked_out
                    || digr_ecc_ue_e_8_5_masked_out || digr_overflow_e_8_5_masked_out
                    || digr_p_err_e_9_5_masked_out || digr_ecc_ce_e_9_5_masked_out
                    || digr_ecc_ue_e_9_5_masked_out || digr_overflow_e_9_5_masked_out
                    || digr_p_err_e_10_5_masked_out || digr_ecc_ce_e_10_5_masked_out
                    || digr_ecc_ue_e_10_5_masked_out || digr_overflow_e_10_5_masked_out
                    || digr_p_err_e_11_5_masked_out || digr_ecc_ce_e_11_5_masked_out
                    || digr_ecc_ue_e_11_5_masked_out || digr_overflow_e_11_5_masked_out
                    || digr_p_err_e_12_5_masked_out || digr_ecc_ce_e_12_5_masked_out
                    || digr_ecc_ue_e_12_5_masked_out || digr_overflow_e_12_5_masked_out
                    || digr_p_err_e_13_5_masked_out || digr_ecc_ce_e_13_5_masked_out
                    || digr_ecc_ue_e_13_5_masked_out || digr_overflow_e_13_5_masked_out
                    || digr_p_err_e_14_5_masked_out || digr_ecc_ce_e_14_5_masked_out
                    || digr_ecc_ue_e_14_5_masked_out || digr_overflow_e_14_5_masked_out
                    || digr_p_err_e_15_5_masked_out || digr_ecc_ce_e_15_5_masked_out
                    || digr_ecc_ue_e_15_5_masked_out || digr_overflow_e_15_5_masked_out
                    || digr_p_err_e_16_5_masked_out || digr_ecc_ce_e_16_5_masked_out
                    || digr_ecc_ue_e_16_5_masked_out || digr_overflow_e_16_5_masked_out
                    || digr_p_err_e_17_5_masked_out || digr_ecc_ce_e_17_5_masked_out
                    || digr_ecc_ue_e_17_5_masked_out || digr_overflow_e_17_5_masked_out
                    || digr_p_err_e_18_5_masked_out || digr_ecc_ce_e_18_5_masked_out
                    || digr_ecc_ue_e_18_5_masked_out || digr_overflow_e_18_5_masked_out
                    || digr_p_err_e_19_5_masked_out || digr_ecc_ce_e_19_5_masked_out
                    || digr_ecc_ue_e_19_5_masked_out || digr_overflow_e_19_5_masked_out
                    || digr_p_err_e_20_5_masked_out || digr_ecc_ce_e_20_5_masked_out
                    || digr_ecc_ue_e_20_5_masked_out || digr_overflow_e_20_5_masked_out
                    || digr_p_err_e_21_5_masked_out || digr_ecc_ce_e_21_5_masked_out
                    || digr_ecc_ue_e_21_5_masked_out || digr_overflow_e_21_5_masked_out
                    || digr_p_err_e_22_5_masked_out || digr_ecc_ce_e_22_5_masked_out
                    || digr_ecc_ue_e_22_5_masked_out || digr_overflow_e_22_5_masked_out
                    || digr_p_err_e_23_5_masked_out || digr_ecc_ce_e_23_5_masked_out
                    || digr_ecc_ue_e_23_5_masked_out || digr_overflow_e_23_5_masked_out
                    || digr_p_err_e_0_6_masked_out || digr_ecc_ce_e_0_6_masked_out
                    || digr_ecc_ue_e_0_6_masked_out || digr_overflow_e_0_6_masked_out
                    || digr_p_err_e_1_6_masked_out || digr_ecc_ce_e_1_6_masked_out
                    || digr_ecc_ue_e_1_6_masked_out || digr_overflow_e_1_6_masked_out
                    || digr_p_err_e_2_6_masked_out || digr_ecc_ce_e_2_6_masked_out
                    || digr_ecc_ue_e_2_6_masked_out || digr_overflow_e_2_6_masked_out
                    || digr_p_err_e_3_6_masked_out || digr_ecc_ce_e_3_6_masked_out
                    || digr_ecc_ue_e_3_6_masked_out || digr_overflow_e_3_6_masked_out
                    || digr_p_err_e_4_6_masked_out || digr_ecc_ce_e_4_6_masked_out
                    || digr_ecc_ue_e_4_6_masked_out || digr_overflow_e_4_6_masked_out
                    || digr_p_err_e_5_6_masked_out || digr_ecc_ce_e_5_6_masked_out
                    || digr_ecc_ue_e_5_6_masked_out || digr_overflow_e_5_6_masked_out
                    || digr_p_err_e_6_6_masked_out || digr_ecc_ce_e_6_6_masked_out
                    || digr_ecc_ue_e_6_6_masked_out || digr_overflow_e_6_6_masked_out
                    || digr_p_err_e_7_6_masked_out || digr_ecc_ce_e_7_6_masked_out
                    || digr_ecc_ue_e_7_6_masked_out || digr_overflow_e_7_6_masked_out
                    || digr_p_err_e_8_6_masked_out || digr_ecc_ce_e_8_6_masked_out
                    || digr_ecc_ue_e_8_6_masked_out || digr_overflow_e_8_6_masked_out
                    || digr_p_err_e_9_6_masked_out || digr_ecc_ce_e_9_6_masked_out
                    || digr_ecc_ue_e_9_6_masked_out || digr_overflow_e_9_6_masked_out
                    || digr_p_err_e_10_6_masked_out || digr_ecc_ce_e_10_6_masked_out
                    || digr_ecc_ue_e_10_6_masked_out || digr_overflow_e_10_6_masked_out
                    || digr_p_err_e_11_6_masked_out || digr_ecc_ce_e_11_6_masked_out
                    || digr_ecc_ue_e_11_6_masked_out || digr_overflow_e_11_6_masked_out
                    || digr_p_err_e_12_6_masked_out || digr_ecc_ce_e_12_6_masked_out
                    || digr_ecc_ue_e_12_6_masked_out || digr_overflow_e_12_6_masked_out
                    || digr_p_err_e_13_6_masked_out || digr_ecc_ce_e_13_6_masked_out
                    || digr_ecc_ue_e_13_6_masked_out || digr_overflow_e_13_6_masked_out
                    || digr_p_err_e_14_6_masked_out || digr_ecc_ce_e_14_6_masked_out
                    || digr_ecc_ue_e_14_6_masked_out || digr_overflow_e_14_6_masked_out
                    || digr_p_err_e_15_6_masked_out || digr_ecc_ce_e_15_6_masked_out
                    || digr_ecc_ue_e_15_6_masked_out || digr_overflow_e_15_6_masked_out
                    || digr_p_err_e_16_6_masked_out || digr_ecc_ce_e_16_6_masked_out
                    || digr_ecc_ue_e_16_6_masked_out || digr_overflow_e_16_6_masked_out
                    || digr_p_err_e_17_6_masked_out || digr_ecc_ce_e_17_6_masked_out
                    || digr_ecc_ue_e_17_6_masked_out || digr_overflow_e_17_6_masked_out
                    || digr_p_err_e_18_6_masked_out || digr_ecc_ce_e_18_6_masked_out
                    || digr_ecc_ue_e_18_6_masked_out || digr_overflow_e_18_6_masked_out
                    || digr_p_err_e_19_6_masked_out || digr_ecc_ce_e_19_6_masked_out
                    || digr_ecc_ue_e_19_6_masked_out || digr_overflow_e_19_6_masked_out
                    || digr_p_err_e_20_6_masked_out || digr_ecc_ce_e_20_6_masked_out
                    || digr_ecc_ue_e_20_6_masked_out || digr_overflow_e_20_6_masked_out
                    || digr_p_err_e_21_6_masked_out || digr_ecc_ce_e_21_6_masked_out
                    || digr_ecc_ue_e_21_6_masked_out || digr_overflow_e_21_6_masked_out
                    || digr_p_err_e_22_6_masked_out || digr_ecc_ce_e_22_6_masked_out
                    || digr_ecc_ue_e_22_6_masked_out || digr_overflow_e_22_6_masked_out
                    || digr_p_err_e_23_6_masked_out || digr_ecc_ce_e_23_6_masked_out
                    || digr_ecc_ue_e_23_6_masked_out || digr_overflow_e_23_6_masked_out
                    || digr_p_err_e_0_7_masked_out || digr_ecc_ce_e_0_7_masked_out
                    || digr_ecc_ue_e_0_7_masked_out || digr_overflow_e_0_7_masked_out
                    || digr_p_err_e_1_7_masked_out || digr_ecc_ce_e_1_7_masked_out
                    || digr_ecc_ue_e_1_7_masked_out || digr_overflow_e_1_7_masked_out
                    || digr_p_err_e_2_7_masked_out || digr_ecc_ce_e_2_7_masked_out
                    || digr_ecc_ue_e_2_7_masked_out || digr_overflow_e_2_7_masked_out
                    || digr_p_err_e_3_7_masked_out || digr_ecc_ce_e_3_7_masked_out
                    || digr_ecc_ue_e_3_7_masked_out || digr_overflow_e_3_7_masked_out
                    || digr_p_err_e_4_7_masked_out || digr_ecc_ce_e_4_7_masked_out
                    || digr_ecc_ue_e_4_7_masked_out || digr_overflow_e_4_7_masked_out
                    || digr_p_err_e_5_7_masked_out || digr_ecc_ce_e_5_7_masked_out
                    || digr_ecc_ue_e_5_7_masked_out || digr_overflow_e_5_7_masked_out
                    || digr_p_err_e_6_7_masked_out || digr_ecc_ce_e_6_7_masked_out
                    || digr_ecc_ue_e_6_7_masked_out || digr_overflow_e_6_7_masked_out
                    || digr_p_err_e_7_7_masked_out || digr_ecc_ce_e_7_7_masked_out
                    || digr_ecc_ue_e_7_7_masked_out || digr_overflow_e_7_7_masked_out
                    || digr_p_err_e_8_7_masked_out || digr_ecc_ce_e_8_7_masked_out
                    || digr_ecc_ue_e_8_7_masked_out || digr_overflow_e_8_7_masked_out
                    || digr_p_err_e_9_7_masked_out || digr_ecc_ce_e_9_7_masked_out
                    || digr_ecc_ue_e_9_7_masked_out || digr_overflow_e_9_7_masked_out
                    || digr_p_err_e_10_7_masked_out || digr_ecc_ce_e_10_7_masked_out
                    || digr_ecc_ue_e_10_7_masked_out || digr_overflow_e_10_7_masked_out
                    || digr_p_err_e_11_7_masked_out || digr_ecc_ce_e_11_7_masked_out
                    || digr_ecc_ue_e_11_7_masked_out || digr_overflow_e_11_7_masked_out
                    || digr_p_err_e_12_7_masked_out || digr_ecc_ce_e_12_7_masked_out
                    || digr_ecc_ue_e_12_7_masked_out || digr_overflow_e_12_7_masked_out
                    || digr_p_err_e_13_7_masked_out || digr_ecc_ce_e_13_7_masked_out
                    || digr_ecc_ue_e_13_7_masked_out || digr_overflow_e_13_7_masked_out
                    || digr_p_err_e_14_7_masked_out || digr_ecc_ce_e_14_7_masked_out
                    || digr_ecc_ue_e_14_7_masked_out || digr_overflow_e_14_7_masked_out
                    || digr_p_err_e_15_7_masked_out || digr_ecc_ce_e_15_7_masked_out
                    || digr_ecc_ue_e_15_7_masked_out || digr_overflow_e_15_7_masked_out
                    || digr_p_err_e_16_7_masked_out || digr_ecc_ce_e_16_7_masked_out
                    || digr_ecc_ue_e_16_7_masked_out || digr_overflow_e_16_7_masked_out
                    || digr_p_err_e_17_7_masked_out || digr_ecc_ce_e_17_7_masked_out
                    || digr_ecc_ue_e_17_7_masked_out || digr_overflow_e_17_7_masked_out
                    || digr_p_err_e_18_7_masked_out || digr_ecc_ce_e_18_7_masked_out
                    || digr_ecc_ue_e_18_7_masked_out || digr_overflow_e_18_7_masked_out
                    || digr_p_err_e_19_7_masked_out || digr_ecc_ce_e_19_7_masked_out
                    || digr_ecc_ue_e_19_7_masked_out || digr_overflow_e_19_7_masked_out
                    || digr_p_err_e_20_7_masked_out || digr_ecc_ce_e_20_7_masked_out
                    || digr_ecc_ue_e_20_7_masked_out || digr_overflow_e_20_7_masked_out
                    || digr_p_err_e_21_7_masked_out || digr_ecc_ce_e_21_7_masked_out
                    || digr_ecc_ue_e_21_7_masked_out || digr_overflow_e_21_7_masked_out
                    || digr_p_err_e_22_7_masked_out || digr_ecc_ce_e_22_7_masked_out
                    || digr_ecc_ue_e_22_7_masked_out || digr_overflow_e_22_7_masked_out
                    || digr_p_err_e_23_7_masked_out || digr_ecc_ce_e_23_7_masked_out
                    || digr_ecc_ue_e_23_7_masked_out || digr_overflow_e_23_7_masked_out
                    || digr_p_err_e_0_8_masked_out || digr_ecc_ce_e_0_8_masked_out
                    || digr_ecc_ue_e_0_8_masked_out || digr_overflow_e_0_8_masked_out
                    || digr_p_err_e_1_8_masked_out || digr_ecc_ce_e_1_8_masked_out
                    || digr_ecc_ue_e_1_8_masked_out || digr_overflow_e_1_8_masked_out
                    || digr_p_err_e_2_8_masked_out || digr_ecc_ce_e_2_8_masked_out
                    || digr_ecc_ue_e_2_8_masked_out || digr_overflow_e_2_8_masked_out
                    || digr_p_err_e_3_8_masked_out || digr_ecc_ce_e_3_8_masked_out
                    || digr_ecc_ue_e_3_8_masked_out || digr_overflow_e_3_8_masked_out
                    || digr_p_err_e_4_8_masked_out || digr_ecc_ce_e_4_8_masked_out
                    || digr_ecc_ue_e_4_8_masked_out || digr_overflow_e_4_8_masked_out
                    || digr_p_err_e_5_8_masked_out || digr_ecc_ce_e_5_8_masked_out
                    || digr_ecc_ue_e_5_8_masked_out || digr_overflow_e_5_8_masked_out
                    || digr_p_err_e_6_8_masked_out || digr_ecc_ce_e_6_8_masked_out
                    || digr_ecc_ue_e_6_8_masked_out || digr_overflow_e_6_8_masked_out
                    || digr_p_err_e_7_8_masked_out || digr_ecc_ce_e_7_8_masked_out
                    || digr_ecc_ue_e_7_8_masked_out || digr_overflow_e_7_8_masked_out
                    || digr_p_err_e_8_8_masked_out || digr_ecc_ce_e_8_8_masked_out
                    || digr_ecc_ue_e_8_8_masked_out || digr_overflow_e_8_8_masked_out
                    || digr_p_err_e_9_8_masked_out || digr_ecc_ce_e_9_8_masked_out
                    || digr_ecc_ue_e_9_8_masked_out || digr_overflow_e_9_8_masked_out
                    || digr_p_err_e_10_8_masked_out || digr_ecc_ce_e_10_8_masked_out
                    || digr_ecc_ue_e_10_8_masked_out || digr_overflow_e_10_8_masked_out
                    || digr_p_err_e_11_8_masked_out || digr_ecc_ce_e_11_8_masked_out
                    || digr_ecc_ue_e_11_8_masked_out || digr_overflow_e_11_8_masked_out
                    || digr_p_err_e_12_8_masked_out || digr_ecc_ce_e_12_8_masked_out
                    || digr_ecc_ue_e_12_8_masked_out || digr_overflow_e_12_8_masked_out
                    || digr_p_err_e_13_8_masked_out || digr_ecc_ce_e_13_8_masked_out
                    || digr_ecc_ue_e_13_8_masked_out || digr_overflow_e_13_8_masked_out
                    || digr_p_err_e_14_8_masked_out || digr_ecc_ce_e_14_8_masked_out
                    || digr_ecc_ue_e_14_8_masked_out || digr_overflow_e_14_8_masked_out
                    || digr_p_err_e_15_8_masked_out || digr_ecc_ce_e_15_8_masked_out
                    || digr_ecc_ue_e_15_8_masked_out || digr_overflow_e_15_8_masked_out
                    || digr_p_err_e_16_8_masked_out || digr_ecc_ce_e_16_8_masked_out
                    || digr_ecc_ue_e_16_8_masked_out || digr_overflow_e_16_8_masked_out
                    || digr_p_err_e_17_8_masked_out || digr_ecc_ce_e_17_8_masked_out
                    || digr_ecc_ue_e_17_8_masked_out || digr_overflow_e_17_8_masked_out
                    || digr_p_err_e_18_8_masked_out || digr_ecc_ce_e_18_8_masked_out
                    || digr_ecc_ue_e_18_8_masked_out || digr_overflow_e_18_8_masked_out
                    || digr_p_err_e_19_8_masked_out || digr_ecc_ce_e_19_8_masked_out
                    || digr_ecc_ue_e_19_8_masked_out || digr_overflow_e_19_8_masked_out
                    || digr_p_err_e_20_8_masked_out || digr_ecc_ce_e_20_8_masked_out
                    || digr_ecc_ue_e_20_8_masked_out || digr_overflow_e_20_8_masked_out
                    || digr_p_err_e_21_8_masked_out || digr_ecc_ce_e_21_8_masked_out
                    || digr_ecc_ue_e_21_8_masked_out || digr_overflow_e_21_8_masked_out
                    || digr_p_err_e_22_8_masked_out || digr_ecc_ce_e_22_8_masked_out
                    || digr_ecc_ue_e_22_8_masked_out || digr_overflow_e_22_8_masked_out
                    || digr_p_err_e_23_8_masked_out || digr_ecc_ce_e_23_8_masked_out
                    || digr_ecc_ue_e_23_8_masked_out || digr_overflow_e_23_8_masked_out);
endmodule
