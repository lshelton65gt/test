library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug6039 is
  port ( clk : in std_logic;
        data : in std_logic_vector(44 downto 0);
      output : out std_logic );
end bug6039;

architecture bug6039 of bug6039 is
   signal rdhzvis  : std_logic;
   signal index    : std_logic_vector(7 downto 0);
   signal q        : std_logic := '0';
   constant NLOC : integer := 128;
begin

   index <= data(43 downto 36); -- data'left-1 downto data'left-1-7); 
   rdhzvis <= '1' when (unsigned(index)>=NLOC) else '0';
   output <= q;

   process(clk)
     begin
       if (clk'event and clk='1') then
         q <= rdhzvis;
       end if;
   end process;
end bug6039;
