// Test folding of a forloop

module forloop(shiftin, shiftout, c, clk);
   input [31:0] shiftin;
   input [4:0] 	c;
   input 	clk;
   output [31:0] shiftout;
   reg [31:0] 	 sreg;

   assign 	shiftout = sreg;

   wire 	j = 1'b1;
   
   integer 	i;   
   always @(posedge clk)
     begin:nb
	sreg = shiftin;
	for (i=0; i<c; i=i+1)
	  sreg = sreg << j;	// substitute '1' for 'j'
     end

   // Add a test of folding a for loop (something we don't do yet..
   always @(negedge clk)
     begin
	for(i=j; i < 1; i=i+j)	// Fold this loop to nothing if we're smart
	  sreg = sreg + i;
     end
   
endmodule


   
