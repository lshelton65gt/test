// test complements

module comp(i,o);
   input [7:0] i;
   output [7:0] o;

   assign       o[0] = ~(~i[0]);
   assign       o[1] = !(~i[1]);
   assign       o[2] = ~(!i[2]);
   assign       o[3] = !(!i[3]);
   assign       o[4] = ~(!i);
   assign       o[5] = !(~i);
   assign       o[6] = !(!i);
   assign       o[7] = ^(~(~i));
endmodule
