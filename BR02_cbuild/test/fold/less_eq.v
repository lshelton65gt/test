module less_eq(a, b, out1, out2, out3, out4, out5, out6, out7, out8,
               out11, out12, out13, out14, out15, out16, out17, out18);
  input [7:0] a, b;
  output      out1, out2, out3, out4, out5, out6, out7, out8;
  output      out11, out12, out13, out14, out15, out16, out17, out18;

  assign      out1 = (((a+b) < 16'h3) | ((a+b) == 16'h3));
  // this should simplify first to
  //                ((a+b) <= 16'h3);
  // and then to
  //  		    (((a+b) & ~(16'h3)) == 16'h0)


  // similarly
  assign      out2 = (((a+b) == 16'h3) | ((a+b) < 16'h3));
  assign      out3 = (((a+b) == 16'h4) | ((a+b) > 16'h4));
  assign      out4 = (((a+b) > 16'h4)  | ((a+b) == 16'h4));

  assign      out5 = ((16'h3 > (a+b)) | (16'h3 == (a+b)));
  assign      out6 = ((16'h3 == (a+b)) | (16'h3 > (a+b)));
  assign      out7 = ((16'h4 == (a+b)) | (16'h4 < (a+b)));
  assign      out8 = ((16'h4 < (a+b))  | (16'h4 == (a+b)));

  assign      out11 = (((a+b) < 16'h3) || ((a+b) == 16'h3));
  assign      out12 = (((a+b) == 16'h3) || ((a+b) < 16'h3));
  assign      out13 = (((a+b) == 16'h4) || ((a+b) > 16'h4));
  assign      out14 = (((a+b) > 16'h4)  || ((a+b) == 16'h4));

  assign      out15 = ((16'h3 > (a+b)) || (16'h3 == (a+b)));
  assign      out16 = ((16'h3 == (a+b)) || (16'h3 > (a+b)));
  assign      out17 = ((16'h4 == (a+b)) || (16'h4 < (a+b)));
  assign      out18 = ((16'h4 < (a+b))  || (16'h4 == (a+b)));

endmodule
