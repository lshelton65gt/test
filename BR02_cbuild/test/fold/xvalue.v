// test folding x values...
module top(a,b,e,r,t,x,y,zed, k,j,g,h,WAX,PLUS,w,modval,divval,xsub,xdiv);
   input e;
   input [3:0] a,b;
   output [3:0] r,t,x,y;
   output [3:0]       zed,WAX, PLUS;
   output             w,k,j,g,h, modval,divval, xsub, xdiv;
   
   assign       r = e ? a : 4'bxxxx; // fold to a
   assign       t = e ? 4'bxxxx : b; // fold to b
   assign       x = 1'bx ? a : b*a + 1; //  fold to false
   assign       y = e ? 4'bzzzz : b; // becomes tristate

   // This one is funny.  Because part of zed is tristated, the whole
   // thing is tristated, You would expect this to fold to
   // 	zed[1:0] = e?a[1:0] : 2'bzz;
   //   zed[3:2]=a[3:2];
   // but LFTristate sees that second assignment and constructs
   //   zed[3:2] = 1?a[3:2]: 2'bzz;
   // so we get the proper strongly driven bits set.
   //
   assign       zed = e ? a : 4'bxxzz;

   reg         c=0;
   assign       w = c;

   always @(posedge e)
     begin
	// This doesn't compare with aldec.  It doesn't seem
	// to make c an 'x' when we have the normal if-then-else
	// but we do compare for the x = 1'bx ? a : b*a+1
        // Aldec leaves w as a zero, implying that it really does
        // delete the whole if-then-else rather than simulating one
        // branch....
       if (1'bx)
         c =1'b1;                 
       else
         c = ^(a ^ b);          // Fold to this
     end

   assign k = a[0] & 1'bx;
   assign j = a[1] | 1'bx;
   assign g = a[2] ^ 1'bx;
   assign h = ~1'bx;
   assign WAX[0] = a[0] > 1'bx;
   assign WAX[1] = ^{1'b1, 1'b0, 1'bx};
   assign WAX[2] = !1'bx;
   assign WAX[3] = 1'bx == 1'bx;
   assign PLUS = (a + 4'bxxxx) + 8;

   reg[7:0] zero = 0;
   assign modval = a % zero;
   assign divval = a / zero;
   assign xsub = (8'hxx - 8'h1) == 8'h0;
   assign xdiv = ((8'hxx) / 8'd2) == 8'd1;
endmodule
