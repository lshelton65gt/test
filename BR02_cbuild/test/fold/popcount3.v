module popcount2(out1, out2, out3, out4, in);

   output [15:0] out1, out2, out3, out4;
   input [18:0]  in;

   assign        out1 = in[15:0] + in[16] + in[17] + in[18];
   assign        out2 = in[0] + in[1] + in[2] + in[18:3];
   assign        out3 = in[0] + in[1] + in[17:2] + in[18];
   assign        out4 = in[0] + in[16:1] + in[17] + in[18];

endmodule
