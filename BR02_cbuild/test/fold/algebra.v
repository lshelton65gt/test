// This tests that the arithmetic simplifications in fold/FoldIArith.cxx
// work.  For now, those transformations are turned off because they have
// bad effects on strip-mining and vectorization, in the tests:
//    test/codegen/bug5506.v  -- size diff -- stripming
//    test/assign/vectorize.v -- cbuild diff -- vectorization.  I think the
//        testcase in test/assign is vector-04.v.

module top(clk, a, b, out1, out2, out3, out4);
  input clk;
  input [31:0] a, b;
  output [31:0] out1, out2, out3, out4;
  reg [31:0]    out1, out2, out3, out4;

  always @(posedge clk) begin
    out1 = ((a + 31) - a) + 1;
    out2 = (2*(4*a + 31) - a) + 1;
    out3 = (2*(4*a + 31) - a) + 1 - b - (b + 5);
  end
  
endmodule

                