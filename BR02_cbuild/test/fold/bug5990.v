// test case collapse?
module top(clk,a,b,c,d, v);
   input clk;
   input [3:0] a,b,c,d;
   output [3:0] v;

   reg [3:0]    t;
   assign       v = t;
   always @(posedge clk) begin
      case (2'h3)
        2'h0: t = a;
        2'h1: t = b;
        2'h2: t = c;
        2'h3: t = d;
      endcase
   end
endmodule
