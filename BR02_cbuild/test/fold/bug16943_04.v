// filename: test/fold/bug16943_04.v
// Description:  This test checks the FFZ detection for decending values
// see also bug16943_02.v 
// expected output: status == 1

module bug16943_04(clk, i,o1, o2, status);
   input        clk;
   input [7:0] 	i;
   output [3:0] o1, o2;
   output 	status;

   reg [3:0] 	temp_1, temp_2;

   assign       o1 = temp_1;
   assign       o2 = temp_2;
   assign status = ( temp_1 == temp_2);
   
   always @(negedge clk)
     begin
	casex (i)
	  8'bxxxxxxx0: temp_1=0;
	  8'bxxxxxx0x: temp_1=1;
	  8'bxxxxx0xx: temp_1=2;
	  8'bxxxx0xxx: temp_1=3;
	  8'bxxx0xxxx: temp_1=4;
	  8'bxx0xxxxx: temp_1=5;
	  8'bx0xxxxxx: temp_1=6;
	  default:     temp_1=7;
	endcase

	temp_2 =  ( i[0] == 0 ) ? 3'd0 :
		  ( i[1] == 0 ) ? 3'd1 :
		  ( i[2] == 0 ) ? 3'd2 :
		  ( i[3] == 0 ) ? 3'd3 :
		  ( i[4] == 0 ) ? 3'd4 :
		  ( i[5] == 0 ) ? 3'd5 :
		  ( i[6] == 0 ) ? 3'd6 : 3'd7;

     end
endmodule
