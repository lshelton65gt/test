// This testcase was extracted from s3/columbia/carbon/cwtb/tb_c3d,
// and it tripped me up when changing expression-folding to use
// managed expressions because the context-size of 'a' and 'b'
// NUIdentRvalues changes from 32 to 1 after a-b==0 turns into
// a==b, but resize of managed expressions resists that.

module top(a, b, c, d, u, v, out1, out2, out3);
  input a, b, c, d;
  input [1:0] u, v;
  output out1, out2, out3;

  assign out1 = ((((a - b) + -1) + 1) == 0);
  assign out2 = (a - b) == 0;
  assign out3 = (({1'b0,a} | (({1'b0,b} & {1'b0,c}) & (~{1'b0,d}))) |
                 (u & (v == 2'h1)));
endmodule
