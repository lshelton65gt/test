// when you add a test to this file you need to increment MSB
`define MSB 47
module consteval(i,r,a,b,c);
   output [`MSB:0] r;
   input 	 i;
   
   reg [`MSB:0]	 o;
   input [31:0] a;
   input [31:0] b;
   input [149:0] c;

   assign r = o;
   
   // These will be constant propagated
   always @(posedge i)
     begin
     o[0] = (a+b) == 17;
     o[1] = (a-b) == 7;
     o[2] = (b-a) == -7;
     o[3] = (a*4) == 48;	// Only powers of two supported
     o[4] = (a/8) == 1;		// ""
     o[5] = (b/8) == 0;
     o[6] = (b % 8) == b;
     o[7] = (a % 4) == 0;
     o[8] = (a==b) == 1'b0;
     o[9] = (a!=b) == 1'b1;
     o[9] = (a === b) == 1'b0;
     o[10] = (a !== b) == 1'b1;
     o[11] = (a && b) == 1'b1;
     o[12] = (a || b) == 1'b1;

     o[13] = (a < b) == 1'b0;
     o[14] = (b < a) == 1'b1;
     o[15] = (a <= b) == 1'b0;
     o[16] = (a > b) == 1'b1;
     o[17] = (a >= b) == 1'b1;
     o[18] = (a & b) == 4;
     o[19] = ~(a & b) == (~(12 & 5));
     o[20] = (a|b) == 13;
     o[21] = ~(a|b) == ~(12|5);
     o[22] = (a^b) == 9;
     o[23] = (a~^b) == ~(12^5);
     o[24] = (a << b) == (12*32);
     o[25] = (a >> b) == 0;
     o[26] = (a >> 1) == 6;

	// Unary operators
	o[27] = (~a) == (12 ^ -1);
	o[28] = (!(a==b)) == 1'b1;
	o[29] = (!(!a) == 1'b1);
	o[30] = (-b[0] == 1'b1);// negating single bit is identity
	o[31] = (-a == -12);
	o[32] = (!a == 1'b0);
	o[33] = (&a == 1'b0);
	o[34] = (~&a == 1'b1);
	o[35] = (|a == 1'b1);
	o[36] = (~|b == 1'b0);
	o[37] = (^a == 1'b0);
	o[38] = (~^a == 1'b1);
	
	o[39] = (a[6]&64 == 1'b0); // should always reduce to a zero
	o[40] = (a[1]&1 == 1'b1);  // should always reduce to a zero
	o[41] = (a[1] == 2);	// perverse and ALWAYS false (NCVerilog issues a warning!)
	
	o[42] = ((b == 5) != 2);// perverse and always true  ( "" )

        //WARNING: ALDEC gets this next test wrong (NCVERILOG AGREES WITH
        // CARBON...
	o[43] = ((16 & (-b[0])) != 0);  // compute at runtime (BUG1510)

	o[44] = ((a === 0) == !a);  // (a===0) should reduce to !a ->result true
	o[45] = ((b !== 0) == (b != 0));  // (b!==0) should reduce to b ->result true
	o[46] = ((c === 0) == (~|c));  // (c===0) should reduce to !c ->result true (this currently fails 03/05/04)
	o[47] = ((c !== 0) == (|c));  // (c!==0) should reduce to c ->result true 
     end
   
endmodule
   

module top(clk, c_short, r);
   output [`MSB:0] r;
   input 	 clk;
   input [1:0] c_short;

   // the following uses a concat to produce a long input from a short input
   // this makes the simulation log file easier to read
   consteval doit(clk, r, 32'd12, 32'd5, {75{c_short}});
endmodule

   
