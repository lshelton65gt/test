// test sign extension

module sext(i,o,w,x);
   input [31:0] i;
   output [31:0] o;
   output [31:0] w;
   output [31:0] x;

   assign 	 o = { {16{i[15]}}, i[15:0]}; //{ 16{i[15]==1}, i[15:0]};
   assign 	 w = { {16{i[31]}}, i[31:16]};
   assign 	 x = { {24{i[7]}}, i[7:0]}; 	 
endmodule // sext
