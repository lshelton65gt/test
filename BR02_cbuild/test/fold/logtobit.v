// Test of logical -> bitwise conversion
module top(i1, i2, i3, o1, o2, o3);
   input i1, i2;
   input [1:0] i3;
   output      o1, o2;
   output [1:0] o3;
   assign o1 = i1 && i2;
   assign o2 = i1 && i3;
   assign o3 = (i1 || i2) + i3;
endmodule
