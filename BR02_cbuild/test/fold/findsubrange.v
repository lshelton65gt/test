// test that we can find the bits in a subrange of an input value

module find(i,o);
   input [63:0] i;
   output [2:0] o;
   reg [2:0]    r;
   assign        o = r;
   always@(i)                   // should see FLO(i[32:28])
     if (i[32])
       r = 5;
     else if (i[31])
       r = 4;
     else if (i[30])
       r = 3;
     else if (i[29])
       r = 2;
     else if (i[28])
       r = 1;
     else
       r = 0;
endmodule
