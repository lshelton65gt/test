// casex test pattern that shouldn't fold into FF/FL
module top(input [2:0] i, output [7:0] o);

   reg [7:0] x;

   assign    o = x;
   
   always @(i)
     casex(i)
       3'b011: x=2;
       3'b10?: x=1;
       3'b110: x=0;
       3'b111: x=255;
     endcase
endmodule

                
