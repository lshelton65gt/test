// collapse constant concatenations.
// collapse nested concats
module concat(o,i,x,y,z,a,q);
   input i;
   input [7:0] x;
   output [8:0] y;
   output [4:0] z;
   output [0:79] o;
   output        a;

   assign 	 o[0:63] = {i, {16'hf0f0, 32'b0}, 32'b1};

   assign 	 o[64:71] = {8{i}};

   assign        y = {x};       // remove concat...
   assign        z = {x[5:0]};  // here too
   assign        a = {8'b0, x} < 384; // gcc was warning about CTCE compare

   output [3:0]  q;
   assign        q = {x[1:0] == 3, x[1:0] == 2, x[1:0] == 1, x[1:0] == 0};

   assign        o[72:79] = {{x[7:4]}, {x[3:0]}};
endmodule
