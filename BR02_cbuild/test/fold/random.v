// random misc. tests

module random(i,o,e,b,c);
   output b;
   input i,e,c;
   output o;

   assign b = e ? i : 0;

   function xorit;
      input x;
      xorit = x ^ 1;
   endfunction
   
   task wild;
      input j;
      output k;

      k = xorit(j);
   endtask

   reg 	     r;
   initial 
     begin
	r=0;
     end
   
   
   always @(posedge c)
     begin: nblk
	integer z;
	z = z + 1;
	
	wild(i,r);
     end

   assign o = r;
endmodule

