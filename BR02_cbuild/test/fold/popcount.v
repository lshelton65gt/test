// recognize and transform into popcount
module popcount(multi_phy, phy_enable, plrx_adr_lines, bigv, bigc, pc);

   input [31:0] phy_enable; input  [65:0] bigv;
   input [7:0]  plrx_adr_lines;
   
   output [31:0] multi_phy; output [7:0] bigc, pc;
   reg [31:0]    multi_phy;

   integer       indx;

   // Slow popcount...
   always @(phy_enable) begin
      multi_phy = 0;
      for (indx = 0; (indx < (1 << plrx_adr_lines)); indx = (indx + 1))
	multi_phy = (multi_phy + phy_enable[indx]);
   end

   reg [7:0] bigc, pc;
   always @(bigv) begin:bv
      integer c;
      bigc = 0;
      for (c = 0; c < 65; c = c+1) // full count
        bigc = bigc + bigv[c];

      pc = 0;
      for (c=12; c < 57; c = c+1) // bvref count
        pc = pc + bigv[c];
   end
   
endmodule

