// the purpose of this testcase is to prove that we don't fold away
// the "negedge nreset" block because nreset is forcible.  The sim
// results are fragile, though, because Aldec sees the a posedge
// clk on the first cycle and immediately clocks "!in" into "out".
//
// I think Carbon tries to mimic that behavior so that sim-golds are
// easier to diff, but it doesn't seem to do it for "out" which has
// a live async-reset.  It does do it for "out2".  I've worked around
// the problem in a very fragile way because I happen to know our
// random test-vector generator will have in=1 on the first cycle,
// so I can get Carbon to match the Aldec behavior by using !in in
// the clock-block portion of the sequential block.

module top(clk, in, out, out2);
  input clk, in;
  output out, out2;
  reg    out, out2;

  // just because nreset has a single continuous driver doesn't
  // mean that it can be eliminated.  It also has a force applied.
  wire   nreset = 1'b1;         // carbon forceSignal

  always @(posedge clk or negedge nreset) begin
    if (~nreset)
      out <= 1'b0;
    else
      out <= !in;
  end

  // this one has no 'force' so it can be folded away
  wire nreset2 = 1'b1;
  always @(posedge clk or negedge nreset2) begin
    if (~nreset2)
      out2 <= 1'b0;
    else
      out2 <= !in;
  end
endmodule
