module dummy(i,o);
   input i;
   output o;
   assign o= 1;
endmodule // dummy

module top(a,b);
   input a;
   output b;
   wire 	  r,s;
 	  
//   dummy X(1'b1, s);
   dummy Y(1'b1, r);

   assign b =  r;
endmodule // top

   
