module top(gross, io_reg, gr_e_p, gr_u_p, gr_m_p, gr_lm, gr_e_s, gr_u_s,
           gr_m_s, ucode, err_reg);
  input [686:0] io_reg;
  input         gr_e_p, gr_u_p, gr_m_p, gr_lm, gr_e_s, gr_u_s, gr_m_s,
                ucode, err_reg;
  output        gross;

  assign gross = io_reg[116] | gr_e_p | gr_u_p | gr_m_p | gr_lm | gr_e_s |
         gr_u_s | gr_m_s | ucode | io_reg[312] | io_reg[492] | err_reg;
endmodule
