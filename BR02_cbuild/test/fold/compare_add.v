/*
 
  Al writes in a review for Josh:

What happens for something like:

         reg [15:0] x,y;

         ....
        if ( (x+y) == 0) ....

Verilog semantics says that "x+y" is computed as a 32 bit result and 
compared against zero.  But
its self-determined size is 16.    As far as I can tell, your code 
doesn't handle this.  I think if
you added a check that

              e1->effectiveBitSize() <= e1->determineBitSize()

which implies that there are no overflows or excess bits in the result....

 
This testcase captures the issue (and caught a bug before commit).
*/


module top(a, b, out);
  input [1:0] a, b;
  output      out;

  assign      out = ((a + b) == 0);
endmodule
