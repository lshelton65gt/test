// Test partsel optimizations

module partsel(i,j,o,x,y);
   input [7:0]i;
   input [0:0]j;

   output      o,x;
   output [24:0] y;
   

   assign      o = i[3:3];
   assign      x = j[0:0];

   // masking optimizations too...

   assign      y[0] = i[3:0] & 3;
   assign      y[2:1] = i & 7;
   assign      y[3] = i[4] & 255;
   assign      y[7:4] = i[7:4] & 7;

   ps partfold(i[0], y[8]);

   wire [255:0] zeros = 256'b0;
   wire [255:0] ones = {256{1'b1}};
   wire [7:0]   randv = 8'hce;
   
   assign       y[9] = randv[3];  // Fold to constant assign
   assign       y[10] = zeros[i]; // zero for any
   assign       y[11] = ones[i];  // one for any

   wire         [3:0] shifty;   // BUG2434
   assign             shifty = {i[3], i[2:0]<<1};
   assign             y[12] = shifty[2:0];

   wire [5:0]         ifsel;
   assign             ifsel = (i ? 9'd511 : 9'd0);
   assign             y[16:13] = ifsel[4:1]; // 255 or 0
   
   sub sub1(i[3:0],y[20:17]);
   sub sub2(i[7:4],y[24:21]);

endmodule


// test folding partselect expressions
module ps(i,o);
   input i;
   output o;

   wire [3:0] r = 4'b1000;

   assign     o = (r[3:2] == 2'b10) ? i : ~i;

endmodule

module sub(in,out);
   input [3:0] in;
   output [3:0] out;
   reg [3:0] out;

   integer     i;
   always @(in) begin
      for (i=0;i<4;i=i+1) begin
         out[i] = ~in[i];
      end
   end
endmodule
