// Contrived testcase to show that + -> ^ conversion results in
// vectorizability

module top(out, a, b, sum, carry);
  input [7:0] a, b;
  output [7:0] out, sum, carry;
  reg [7:0]    out, sum, carry;
  integer      i;

  always @(a or b)
    for (i = 0; i < 8; i = i + 1) begin
      out[i] = a[i] + b[i];
      {carry[i],sum[i]} = a[i] + b[i];
    end
endmodule

  