// filename: test/fold/bug16943_03.v
// Description:  This test combines problems seen in bug 16943 and 16953
// the value for temp_1 is for bug 16953
// the value for temp_2 is for bug 16943
// The idea here was to express the same behavior with a casex and a nested chain of ternary operators.
// Expected output: temp_1 and temp_2 should always be equivalent, and status to always be 1
//  the assignment to temp_2, this should be recognized as a FFZ
//  the casex condition expression should use a FFZ

// This test differes from 16943_02 in that the output values are decending,
// this particular example should have worked before bug16943 was fixed (except
// that bug16953 also interfered with the correct operation.
module bug16943_03(clk, i,o1, o2, status);
   input        clk;
   input [7:0] 	i;
   output [3:0] o1, o2;
   output 	status;

   reg [3:0] 	temp_1, temp_2;

   assign       o1 = temp_1;
   assign       o2 = temp_2;
   assign status = ( temp_1 == temp_2);
   
   always @(negedge clk)
     begin
	casex (i)
	  8'bx0xxxxxx: temp_1=6;
	  8'bxx0xxxxx: temp_1=5;
	  8'bxxx0xxxx: temp_1=4;
	  8'bxxxx0xxx: temp_1=3;
	  8'bxxxxx0xx: temp_1=2;
	  8'bxxxxxx0x: temp_1=1;
	  8'bxxxxxxx0: temp_1=0;
	  default:     temp_1=7;
	endcase

	temp_2 =  ( i[6] == 0 ) ? 3'd6 :
		( i[5] == 0 ) ? 3'd5 :
		( i[4] == 0 ) ? 3'd4 :
		( i[3] == 0 ) ? 3'd3 :
		( i[2] == 0 ) ? 3'd2 :
		( i[1] == 0 ) ? 3'd1 :
		( i[0] == 0 ) ? 3'd0 : 3'd7;

     end
endmodule
