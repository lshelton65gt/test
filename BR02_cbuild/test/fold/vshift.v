// test verilog shift optimizations
module top(a,b,c,x);
   input [2:0] a,b;
   input [31:0] c;
   output [7:0] x;

   assign       x[0] = ^((c << a) << b);
   assign       x[1] = ^((c >> a) >> b);
   assign       x[2] = ^(($signed(c) >>> a) >>> b);
   assign       x[3] = ^(($signed(c) <<< 3 ) <<< -3);
   assign       x[4] = ^((c >> 4'd8) >> -5'sd8); // treated as unsigned!!!
   assign       x[5] = ^((c >> a) >> 5'd16);
   assign       x[6] = ^((c << a) << 5'd16);
   assign       x[7] = ^((c << 3'd7) << b);
endmodule
