// test simplifications of rightshifts.

module top(a,b,c,o,x,w,z);
   input [31:0]a;
   input [31:0] b;
   input [31:0]c;
   output [31:0] o;
   output [127:0] x;
   output [31:0]  w;
   wire [31:0] 	 ta;
   wire [31:0] 	 tb;
   wire [31:0] 	 tc;
   
   wire [63:0]   td;
   assign        td = {a,b};
   assign        w = td >> 32;  // Should be 'a'
   
   shifter s1(a, 8'd32, ta);		// should always be zero
   shifter s2(b, 8'd31, tb);		// one-bit of significance
   shifter s3(c, 8'd0, tc);		// should optimize away shift completely

   assign 	 o = a ^ b ^ c;

   // partselect optimizations
   assign x[15:0] = a[1:0] & 16'h00ff; // should do small partsel extended
   assign x[31:16] = b[23:16] & 16'hffff; // just small partsel
   assign x[63:32] = ({ta,tb} >> 16) & 32'hffffffff;
   assign x[64+16:64] = (a >> 16);
   assign x[127:81] = (b[30:4] >>8);

   output [3:1] z;
   assign        z[1] = (a[7:0] >> b[1:0]) < 128; // Can't decide at compile time
   assign        z[2] = (a[7:0] >> 4) < 32; // Always true
   assign        z[3] = ({8'b0,a[7:0]} >> 8) == 0; // always true


endmodule

module shifter(v,c,r);
   input [31:0] v;
   input [7:0] 	c;
   output [31:0] r;

   assign 	 r = v >> c;
endmodule
