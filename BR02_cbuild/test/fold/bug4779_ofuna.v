/*
 This testcase is derived from the decode module within the
 Toshiba/Ofuna design.

 While working on case statement optimization as part of bug 4779,
 this module exhibited a simulation mismatch. This was due to the fact
 that we were not correctly combining case elements with shared
 constant conditions.
 
 For example, we were incorrectly transforming the case statement:
 
 case(sel)                case(sel[2])
 3'b01x,            ===>  1'b0: case(sel[1:0])
 3'b0x1:  out = a;              2'b1x:   out = a;
 default: out = b;              default: out = b;
 endcase                        endcase
                          1'b0: case(sel[1:0])
                                2'bx1:   out = a;
                                default: out = b;
                                endcase
                          default: out = b;
                          endcase
 
 The repetition of the 1'b0 condition means that one of the two
 conditions will never fire. This is wrong. The correct transformation
 is:

 case(sel)                case(sel[2])
 3'b01x,            ===>  1'b0: case(sel[1:0])
 3'b0x1:  out = a;              2'b1x, 2'bx1: out = a;
 default: out = b;              default: out = b;
 endcase                        endcase
                          default: out = b;
                          endcase
 */

`define	abx	10'h0_3a		
`define aby	10'h1_3a
`define	addd	10'b00_11??_0011, 10'h1_e3
`define	subd	10'b00_10??_0011, 10'h1_a3
`define	asld	10'h0_05 		
`define	lsrd	10'h0_04 		
`define cpd	10'b10_10??_0011, 10'h3_a3
`define	cpx	10'b00_10??_1100, 10'h3_ac
`define cpy	10'b01_10??_1100, 10'h2_ac
`define	ins	10'h0_31		
`define	inx	10'h0_08		
`define iny	10'h1_08
`define	des	10'h0_34		
`define	dex	10'h0_09		
`define dey	10'h1_09
`define	tsx	10'h0_30		
`define tsy	10'h1_30
`define	txs	10'h0_35		
`define tys	10'h1_35
`define	jsr	10'h0_9d, 10'h0_ad, 10'h0_bd, 10'h1_ad
`define	bsr	10'b00_1000_1101	
`define	pshx	10'h0_3c		
`define pshy	10'h1_3c
`define	pulx	10'h0_38		
`define puly	10'h1_38
`define	xgdx	10'b00_1000_1111	
`define xgdy	10'h1_8f

`define	asl	10'b00_011?_1000, 10'h1_68
`define	asr	10'b00_011?_0111, 10'h1_67
`define	lsr	10'b00_011?_0100, 10'h1_64
`define	clr	10'b00_011?_1111, 10'h1_6f
`define	com	10'b00_011?_0011, 10'h1_63
`define	dec	10'b00_011?_1010, 10'h1_6a
`define	inc	10'b00_011?_1100, 10'h1_6c
`define	neg	10'b00_011?_0000, 10'h1_60
`define	rol	10'b00_011?_1001, 10'h1_69
`define	ror	10'b00_011?_0110, 10'h1_66

module top(ir,page,double_alu,write_back);
   input [7:0]	ir;
   input [1:0] 	page;
   output	double_alu, write_back;
   reg 		double_alu, write_back;
   initial double_alu = 0;
   initial write_back = 0;
   always @(page or ir) begin
      // fails
    casez({ page, ir })								// synopsys parallel_case
      `abx, `aby, 
      `addd, `subd, 
      `asld, `lsrd, 
      `cpd, `cpx, `cpy, 
      `ins, `inx, `iny, 
      `des, `dex, `dey, 
      `tsx, `tsy, `txs, `tys, 
      `jsr, `bsr, 
      `pshx, `pshy, 
      `pulx, `puly, 
      `xgdx, `xgdy : double_alu = 1;
      default : double_alu = 0;
    endcase
      // fails
    casez({ page, ir })
      `asl, `asr, `lsr, 
      `clr, `com,
      `dec, `inc, `neg, 
      `rol, `ror : write_back = 1;
      default : write_back = 0;
    endcase
   end
endmodule
