// Test folding of named blocks

module nb(i,c,o);
   input i,c;
   output o;

   wire    r;
   assign r = 0;                // propagatable

   reg   w;
   assign o = w;
   
   always @(posedge c)
     begin
        if (r)
          begin: badblock        // Can't delete - it declares a local net
             reg q;
             q = !q;
             w = q;
          end
        else
          w = 1;

        if (r & i)
          begin: okblock
             w = !i;
          end
     end

   
endmodule

             
