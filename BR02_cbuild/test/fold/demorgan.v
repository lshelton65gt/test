module demorgan(a,b,c,d,o);
   input a,b;
   input [7:0] c, d;
   
   output [14:0] o;
   
   assign o[0] = ((~a) & (~b)) == (~(a | b));
   assign o[1] = ((~c) | (~d)) == (~(c & d));
   assign o[2] = (~((~a) & (~b))) == ( a | b);
   assign o[3] = (~((~a) | (~b))) == ( a & b );

   assign o[4] = ((!a) && (!b)) == (!(a || b));
   assign o[5] = ((!c) || (!d)) == (!(c && d));
   assign o[6] = (!((!a) && (!b))) == ( a || b);
   assign o[7] = (!((!a) || (!b))) == ( a && b );

   assign o[8] = (~((~a) ^ (b))) == (a ^ b); 
   assign o[9] = (~((a) ^ (~b))) == (a ^ b);

   assign o[10] = ((~a) ^ (~b)) == (a ^ b);

   assign o[11] = (a ^ (~a)) == 1'b1;
   assign o[12] = ((~a) ^ a) == 1'b1;
   assign o[13] = (!c[0] & !(|c[7:1]));
   assign o[14] = (!d[7] | !(&d[6:0]));
endmodule


