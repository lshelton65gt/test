
module mixedconst(i, clk, out1, out2);
   input i, clk;
   output out1;
   output [9:0] out2;
   reg          out1;
   
   assign       out2 = 10'b1xxzz0011z;
   
   always @(posedge clk)
     out1 <= i;
endmodule // mixedconst

