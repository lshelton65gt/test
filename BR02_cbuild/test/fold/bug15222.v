// filename: test/fold/bug15222.v
// Description:  This test duplicates the problem seen in bug 15222
// the issue was that a fold operation on multiple right shifts, that converted
// the shift into a part select was not taking into consideration that the total
// number of shift positions might not be expressible as the sum of the
// individual shift positions unless the width of the expression was enlarged.
// This was a sizing problem within the fold optimization.
//
// assume that in3 and in4 are the same size
// old: Folded :(in1 >> in3)[in4 +: 19]
//             :in1[(in3 + in4) +: 19]   // note that the sum in3+in4 could overflow 
// new: Folded :(in1 >> in3)[in4 +: 19]
//             :in1[({1'b0,in3} + {1'b0,in4}) +: 19]

module bug15222 (in1, in3, in4, out1);

   input wire [28:0]         in1;
   input wire [1:0] 	     in3;
   input wire [1:0] 	     in4;
   output wire [18:0] 	     out1;

   assign out1 = (((in1 >> in3) >> in4) );

endmodule
