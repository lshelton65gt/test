// test simplifications of left shifts.

module top(a,b,c,d,o,z,p,q,w,x,ws,bs,o_d);
   input [31:0]a;
   input [31:0] b;
   input [31:0] c;
   input [31:0] d;
   output [31:0] o, bs;
   output [9:0]  o_d;
   wire [31:0] 	 ta;
   wire [31:0] 	 tb;
   wire [31:0] 	 tc;
   wire [131:100] 	 td;
   output 	 z,p,q;
   output [7:0]  w;
   
   shifter s1(a, 8'd32, ta);		// should always be zero
   shifter s2(b, 8'd31, tb);		// one-bit of significance
   shifter s3(c, 8'd0, tc);		// should optimize away shift completely   
   shifter s4(d, 8'd8, td);

   assign 	 o_d = td[129:120]; // if we flatten this partselect the output of
				    // module then we get a non-normalized partselect

   wire [63:0] 	 bigger;
   assign 	 bigger = a << 32; // Don't optimize this one
   
   assign 	 o = a ^ b ^ c ^ bigger[31:0] ^ bigger[63:32];


   assign 	 z = 0 << a;   // 0 << k
   assign 	 p = (a[0] << 99) + 100'b0; 	 // a[0] ? 100'b100...:100'b0
   assign 	 q = (a<<32) + 1;  // should always be 1

   // test corrected bitsizes
   assign 	 w = (a[1] << 3) + 8'b1; // (a[1]?16:0) + 8'b1
   output [1:0]  x;
   assign        x[0] = (a[7:0] << 4'd8) < 17'h10000; // Always true
   assign        x[1] = (a[3:0] << b[1:0]) < 128; // Always true

   output [31:0] ws;
   assign        ws = {a,b,c} <<  a; // Poke at resizing AND effectiveBitSize() problems
   assign        bs = {a[15:0],16'h1234} << 16; // fold to 32'h12340000

endmodule

module shifter(v,c,r);
   input [31:0] v;
   input [7:0] 	c;
   output [31:0] r;

   assign 	 r = v << c;
endmodule
