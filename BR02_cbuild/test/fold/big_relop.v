module pan(bv, i64, i32, out1, out2, out3, out4, out5, out6);
  input [255:0] bv;
  input [63:0] i64;
  input [31:0] i32;
  output        out1, out2, out3, out4, out5, out6;

  assign        out1 = bv[0] & bv[200]; // way too big a span
  assign        out2 = bv[30] & bv[34]; // small span, covers two words
  assign        out3 = bv[31] & bv[32]; // adjacent, covers two words
  assign        out4 = bv[66] & bv[90]; // span is small, all in one word
  assign        out5 = i64[10] & i64[60]; // big span, but all in a UInt64
  assign        out6 = i32[3] & i32[30]; // small span, small word
endmodule
