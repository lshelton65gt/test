module top(i, o);
  output [197:0] o;
  input [31:0]  i;
  assign        o[197:0] = {99{i[1:0]}};
endmodule
