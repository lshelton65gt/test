// associative folding with z's

module top(i,o);
   input [3:0] i;
   output      o;

   assign      o = i[0] | 1'bz | i[1] | i[2] | 1'bz | i[3];
endmodule
