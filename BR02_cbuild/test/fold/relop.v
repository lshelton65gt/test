// Test relational operator optimizations
module relop(a,b, o);
   input [3:0]a;
   input [3:0]b;
   output [5:0] o;

   assign     o[0]= a < b;
   assign     o[1]= a <= b;
   assign     o[2]= a > b;
   assign     o[3]= a >= b;
   assign     o[4]= a == b;
   assign     o[5]= a != b;
endmodule

module srelop(a,b,o);
   input [31:0]a;
   input [31:0]b;
   integer     ia;
   integer     ib;
   output [5:0] o;

   always @(a or b)
     begin
	ia = a;
	ib = b;
     end
 	
   assign     o[0]= ia < ib;
   assign     o[1]= ia <= ib;
   assign     o[2]= ia > ib;
   assign     o[3]= ia >= ib;
   assign     o[4]= ia == ib;
   assign     o[5]= ia != ib;
endmodule

module canon(a,o);
   input [7:0] a;
   output[5:0] o;

   assign      o[0] = !(3 >= a);
   assign      o[1] = !(9 > a);
   assign      o[2] = !(12 < a);
   assign      o[3] = !(5 <= a);
   assign      o[4] = !(7 != a);
   assign      o[5] = !(8 == a);
endmodule

module top(a,b,o);
   output [120:0] o;
   input [5:0] 	 a;
   input [5:0] 	 b;
   
   relop one( 4'd5, 4'd4, o[5:0]);
   relop two( 4'd4, 4'd5, o[11:6]);
   relop three( 4'd4, 4'd4, o[17:12]);
   relop frr( 4'd0, 4'd4, o[23:18]);
   relop fiv( 4'd0, 4'd0, o[29:24]);
   relop six( 4'd15, 4'd16, o[35:30]);

   
   assign 	 o[36] = a == 128; // Always false because 'a' zero extends.
   assign 	 o[37] = 64 != b; // Always true because 'b' zero extends.
   assign 	 o[38] = (-1 == {33{1'b1}}); // not true because -1 is zero extended

   srelop s1 (5, 4, o[44:39]);
   srelop s2 (4, 5, o[50:45]);
   srelop s3 (-4, -4, o[56:51]);
   srelop s4 (0, -12, o[62:57]);
   srelop s5 (-12, -10, o[68:63]);
   srelop s6 (-20, 5, o[74:69]);
   canon s7 ({2'b0, a}, o[80:75]);
   srelop s8 (6, 6, o[108:103]);

   // the trick of subtracting and looking at the sign bit is very
   // dicy when two numbers differ only in the sign bit
   srelop s9  (32'h80001000, 32'h00001000, o[114:109]);
   srelop s10 (32'h00001000, 32'h80001000, o[120:115]);

   assign 	 o[81] = (a < 64);
   assign 	 o[82] = (a <= 63);
   assign 	 o[83] = (a > 63);
   assign 	 o[84] = (a >= 64); 	 
   assign        o[85] = (~a == 8'd10);
   assign        o[86] = (~a != 10); // note resizing!
   assign        o[87] = (~a == 10);
   assign        o[88] = ((a+8'd22) == 8'd55); // no overflow to worry about
   assign        o[89] = (a+127) == 128; // possible overflow here!
   assign        o[90] = (a-8'd22) != 8'd0;
   assign        o[91] = (a*12) == 12; // no fold here yet...
   assign        o[92] = 64'b0 + (a << 63) + 64'd128 == 64'd128;
   assign        o[93] = {64'b0, a, 58'b0} + 128'hfedcba98765432100123456789abcdef != 128'hfedcba98765432100123456789abcdef;
   assign        o[94] = (64-a) != 0; // should fold to true because a can't be more than 63.
   assign        o[95] = (a ^ 65) != 0; // don't do this one yet?
   assign        o[96] = !(a ? (64-a) : b);
   assign        o[97] = ^(a?a : b);
   assign        o[98] = -(a ? a : b); // fold into ?: because minus is reducing
   assign        o[99] = ((&a) | 5) < 6; // TRUE
   assign        o[100] = ((|a) & 6) == 6; //  FALSE
   assign        o[101] = (a[1:0] & -4) == 0; // TRUE
   assign        o[102] = (b[2:0] | 6) < 6; // evaluate at runtime, can't fold!
endmodule
   
