module casetest(a,b,out,clk);
   input a;
   input b;
   input clk;
   reg [31:0] c;
   output [31:0] out;

   assign     out = c;

   always @(posedge clk)
     begin
	case({a,b})
	  2'b00: c = 0;
	  2'b01: c = 1;
	  2'b10: c = 2;
	  2'b11: c = 3;
	endcase // case({a,b})
     end
   always @(negedge clk)
     begin
        case ({a,b})            // remove case and leave just body...
          0: c=0;
          1: c=0;
          2: c=0;
          default: c=0;
        endcase
     end
endmodule

module top(out0, out1,clk);
   output [31:0] out1;
   output [31:0] out0;
	
   input  clk;

   casetest zero(1'b0,1'b0, out0, clk);
   casetest ones(1'b0,1'b1, out1, clk);
	  
endmodule // top
