// test constant propagation
module dummy(i,o);
   input i;
   output o;
   assign o= 1;
endmodule // dummy

module top(a,b,o);
   input a;
   output b;
   output o;
   wire 	  r,s;
   reg 		  c; 	  
//   dummy X(1'b1, s);
   dummy Y(1'b1, r);

   assign b =  r;
   assign o = c;
   
   reg 	  temp;
   always @(posedge a)
     begin
	temp = 1'b1;

	c = temp ^ 1'b1;
     end
   
endmodule // top

   
