// test lhs concat optimizations
module top(j,i,o,x,y);
   input j;
   input [8:0] i;
   output [15:0] o;
   output        x,y;

   assign        {x,o[15],o[14],o[13], o[12:10], {o[9],o[8]}} = i;
   assign        {o[7:4], y, o[3], o[2], o[1],o[0]} = i;
endmodule
