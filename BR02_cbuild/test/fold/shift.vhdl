-- Test simple vhdl shift fold optimizations */
-- Interesting cases:
--      -1 sra k => -1
--      -1 sla k => -1
--      x srl -5 => x sll 5
--      x sll -5 => x srl 5
--      x srl 5  => x >> 5
--      x sll 5  => x << 5
--      x sra 5  => x >>> 5

entity shift is
  port (in1: in bit_vector(7 downto 0);
        sc: in integer;
        a : out bit_vector(7 downto 0);
        b : out bit_vector(7 downto 0);
        c : out bit_vector(7 downto 0);
        d : out bit_vector(7 downto 0);
        e : out bit_vector(7 downto 0);
        f : out bit_vector(7 downto 0);
        g : out bit_vector(7 downto 0);
        h : out bit_vector(7 downto 0);
        i : out bit_vector(7 downto 0);

        j : out bit_vector(7 downto 0);
        k : out bit_vector(7 downto 0);
        l : out bit_vector(7 downto 0);
        m : out bit_vector(7 downto 0);
        n : out bit_vector(7 downto 0);
        o : out bit_vector(7 downto 0);

        p : out bit_vector(7 downto 0);
        q : out bit_vector(7 downto 0);
        r : out bit_vector(7 downto 0);
        s : out bit_vector(7 downto 0);
        t : out bit_vector(7 downto 0);
        u : out bit_vector(7 downto 0)
        );
end;

architecture arch of shift is
begin
  a <= B"11111111" sra sc;                      -- -1
  b <= B"11111111" sla sc;                      -- -1
  c <= in1 srl -5;                      -- in1 sll 5
  d <= in1 sll -5;                      -- in srl 5
  e <= in1 sll 5;                       --in << 5
  f <= in1 sra 5;                       --in >>> 5
  g <= in1 srl 5;                       --in >> 5
  h <= (in1  sll 2) sll 3;              -- in << 5
  i <= (in1 srl 3) srl 2;               -- in >> 5

  j <= (in1 sll -2) sll 3;              --becomes (in1 srl 2) sll 3, no folds
  k <= (in1 srl -3) srl 2;
  l <= (in1 sll 2) sll -3;
  m <= (in1 srl 3) srl -2;
  n <= (in1 sll -2) sll -3;
  o <= (in1 srl -3) srl -2;

  p <= (in1 sla -2) sla 3;              --becomes (in1 srl 2) sll 3, no folds
  q <= (in1 sra -3) sra 2;
  r <= (in1 sla 2) sla -3;
  s <= (in1 sra 3) sra -2;
  t <= (in1 sla -2) sla -3;
  u <= (in1 sra -3) sra -2;

end;
