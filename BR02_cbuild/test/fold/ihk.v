// Generated with: ./generator -n -1003796213 -s 50 -o candidate.v
// Using seed: 0xc42b490b
// Writing circuit of size 50 to file 'candidate.v'
module top(clk, O, sVIf, lMFJg, mqq, IA, YeXPT, J, b, IhK);
input clk;
   output IhK;
output O, sVIf, lMFJg, mqq, IA, YeXPT, J, b;

  // net delcarations
  wire clk;
  reg  [31:0] WV;
  reg  [31:0] CRTsT;
  reg  [31:0] K;
  reg  [31:0] f;
  reg  [31:0] Ozb;
  wire [1:81] ng;
  reg   signed JAUM;
  reg  [21:40] OffeP;
  wire  signed mWT;
  wire  signed [74:68] PZcM;
  wire  signed [61:114] HN;
  wire Uefai;
  wire [21:52] IjyA;
  wire [71:22] p;
  reg  [106:46] WjDN;
  wire [109:118] k;
  wire [51:105] e;
  wire [20:125] _kth;
  wire  signed [116:126] xEe;
  reg  [109:100] rm;
  wire IhK;
  wire [96:52] y;
  wire  signed z;
  wire  signed D;
  reg  [71:25] apuPC;
  wire  signed iOz;
  wire atr;
  wire [56:48] SvHbp;
  wire O;
  reg   signed [51:43] xRi;
  wire sMB;
  wire XGjNk;
  wire [33:30] MvTDv;
  wire [102:124] zt;
  wire  signed [82:86] sVIf;
  wire  signed MCV;
  wire [66:32] eZIT;
  reg   signed LhZ;
  reg   signed [10:56] lMFJg;
  reg  [44:110] mqq;
  reg   signed E;
  wire  signed [54:38] az;
  wire UptL;
  wire [68:125] rxAZ;
  wire [78:104] UETiB;
  wire [99:44] U;
  reg  [32:111] iPC;
  reg   signed IA;
  reg  [22:79] Jfb;
  wire [89:9] EkHpG;
  reg  xdAjn;
  wire [38:27] YeXPT;
  wire  signed [126:82] uYtIL;
  wire  signed [68:54] J;
  wire [102:110] b;


  initial begin
    f = 32'h42f56118;
  end
  always@(posedge clk)
  begin
    f = 279470273*(f % 15) - 102913196 * (f / 15);
  end

  // assignments
  assign ng[53 : 61] = CRTsT[31 : 0];

  always@(posedge clk)
  begin
    JAUM = 93'h2542cf24;
  end

   always@(posedge clk)
  begin
    OffeP[36 : 36] = (^CRTsT[(^((97'he9761927<=(((102'hf07a31b9+7'h5c)-(ng[f[14 : 13] +: 63]|78'h5e805e0d))<<(JAUM^(124'h8ab3437c+Ozb[18 : 16]))))!=JAUM)) +: 14]);
  end

  assign PZcM[68 : 68] = ((ng^WV[26 : 9])<(~Ozb[31 : 0]));

  assign IjyA[50 : 52] = (PZcM[74 : 68]*33'h9f4fc1b3);

  assign p[71 : 23] = (!f[24 : 24]);

  always@(posedge clk)
  begin
    WjDN[49 : 49] = CRTsT[6 : 0];
  end

  assign k[114 : 117] = (K==(((-120'h25bf87d0) ? (((~((~(2'h0==60'h4edb14ed))*((-CRTsT[31 : 0])>=JAUM)))^(-(((-CRTsT[29 : 3])!=(116'h72dd092c<Uefai))-((Uefai>>IjyA)<p))))&&((109'h95a1c8b9>72'h78dd5053)|Ozb)) : ((IjyA[37 : 52]!=((|Uefai)>=(65'h497fce0e||IjyA))) ? OffeP : Ozb))>=(&(OffeP>62'hd4fc3237))));

  assign e[89 : 103] = mWT;

  assign IhK = e[((|61'hd9e19717)>>(~f[(^(OffeP<=PZcM[71 : 68])) -: 24])) -: 11];
endmodule
