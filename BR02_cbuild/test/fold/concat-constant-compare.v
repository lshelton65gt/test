module top(a,b,c,out);
  input signed [7:0] a,b,c;
  output out;

  // compare a concat to a larger constant.
  assign out = ( {a,b,c} == 32'hff804020 );
endmodule
