// more associative tests
module top(stat, x,y,z,a,b,c);
   input [15:0] stat;
   output       x,y,z,a,b,c;

   assign       x = (& stat[8:5]) & (| stat[4:3]);
   assign       y = (&stat[8:5]) & !stat[4] & (& (~stat[3:0]));
   assign       z = (^stat[8:5]) ^ stat[4] ^ (^stat[3:0]);
   // Fold into ^(2{stat[0+:2] | stat[2+:2] | ... stat[14+:2])
   assign       a = ^( {2{stat[0+:2]}} | {2{stat[2+:2]}} | {2{stat[4+:2]}}
                       | {2{stat[6+:2]}} | {2{stat[8+:2]}}
                       | {2{stat[10+:2]}} | {2{stat[12+:2]}} | {2{stat[14+:2]}});
   // don't collapse this one - the repeat count of the concat's differ
   assign       b = ^( {2{stat[5]}} |  {stat[1:0]});
   // don't collapse this one - the number of terms in the concats isn't one
   assign       c = ^({stat[0], stat[7]} | {stat [5:4]});
endmodule
