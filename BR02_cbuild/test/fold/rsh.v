// bit-select not folded..

module top(e,i,f);
   input [3:0] f;
   input [4:0] e;
   output      i;

   assign      i = e[(1 >> ~f[e -:24]) -: 11];
endmodule
