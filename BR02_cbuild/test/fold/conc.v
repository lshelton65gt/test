module top(out, in);
  output out;
  input [12:0] in;

  assign       out = |((~{3'h0,in}) & 16'h0360);
endmodule
