entity nor_boolean is 
  port(in1,in2 : boolean;
       output : out boolean);
end;

architecture nor_boolean of nor_boolean is
begin
  process(in1, in2) 
  begin
    if in1 then 
      output <= true nor in2;
    else
      output <= not (false nor in2);
    end if;
--	wait on in1,in2;
  end process;
end;
