module top(s);
output s;

wire [29:54] s;

RXEqr jQPFK (,);

QaRi QngVe ({s[45:49],s[45:49]}, );


endmodule

module QaRi(uMbh, VS);
output uMbh;
input VS;

reg [3:12] uMbh;
wire [3:21] VS;

reg [3:7] jE;
reg cK;
reg nK;
reg [0:12] VXRW;
reg [3:6] tiiCc;

y c (, , , uMbh[5:7]);

always @(&uMbh[4:11])
  if (uMbh[4:11])
    cK = 1'b0;

always @(cK)
  if (cK)
    VXRW[1:8] = 4'b1000;

always @(tiiCc[4])
  if (tiiCc[4])
    VXRW[1:8] = 8'hff;

always @(nK)
  if (~nK)
    uMbh[3:12] <= ~VXRW[1:8];

endmodule

module RXEqr(T, u);
inout T;
input u;

wire T;
wire [2:3] u;

reg YIgR;
wire [61:70] Xg;

y t (, , , Xg[61:63]);

assign Xg[63] = Xg[61:63]^YIgR;

y VUYXS (YIgR, , ,);

endmodule

module y(Pm, kE, EPLVF, E); // carbon disallowFlattening
input Pm;
inout kE;
input EPLVF;
input E;

wire Pm;
wire [5:7] kE;
wire EPLVF;
wire [4:6] E;

reg EA;
reg CsU;
reg Gcg;
reg Eltd; // carbon observeSignal

always @(negedge Gcg)
  CsU <= 1'b1;

always @(posedge CsU)
  Gcg = 1'b0;

c kUDlG ({Gcg,Pm},);

always @(negedge E[4])
  Eltd = EA;

always @(posedge CsU)
  EA = 1'b0;

endmodule


module c(lISh, iGiWE);
input lISh;
input iGiWE;

wire [15:22] lISh;
wire [3:6] iGiWE;

endmodule

