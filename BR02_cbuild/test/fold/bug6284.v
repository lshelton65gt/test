// Check that partselects of extended ! fold away EXT and deal with sign extensions
// correctly.

module top(i,o);
  input i;
  output[1:0] o;
  integer s_i,s_j;
  reg [1:0] o;
  always @(i) begin
    s_i = !i;
    o[0] = s_i[0];
    s_j = $signed(!i);
     o[1] = s_j[3];             // signed should still yield i[0]
  end
endmodule
