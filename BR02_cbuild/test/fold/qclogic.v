// convert a?b:0 and friends

module qc(a,b,x,y,c,d,e,f,g,h,R,S,T,U,V,W,X,Y,Z, badone,YAQC,PV);
   input [7:0]x;
   input [7:0]y;
   input      a,b;
   output c,d,e,f,h;
   output [7:0] g,W;
   output       R,S, T,U,V,X,Y,Z;
   output [2:0]       badone;

   assign c = a?b:1'b0;
   assign d = a?1'b0:b;
   assign e = a?b:1'b1;
   assign f = a?1'b1:b;

   // tests that should NOT fold
   assign g = x?y:1'b0;

   // test that should fold if we are smart
   assign h = a?b:0;		//  '0' widens 'b' to 32 bits

   // test that could be simplified
   assign R = (x[2] ? 0:
               x[1] ? 0:
               x[0] ? 0: a);           // out = en[2:0] ? 0 : a;

   assign S = (x[2] ? 0:
               x[1] ? 1:
               x[0] ? 0: b);    // out = (en[2:0] == 3'b010 ? 0 : b);


   // These three tests simulate differently on Aldec and NC
   // because of the way 'x' is treated.  I've looked carefully
   // at the differences and believe they are okay.
   // Simplify because of x
   assign T = (a & 1'bx);       // 0
   assign U = (b | 1'bx);       // 1

   // should be (1'bx ^ 1'bx) ? ^x : 0 => 1'bx ? ^x : 0 => 0
   assign V = ((~1'bx) ^ 1'bx) ? ^x : (x + 8'bxxxxxxxx);

   assign W = -(a ? x : 8'b0);
   assign X = !((x > 15) ?  1'b1:1'b0);

   assign Y = (a ? 1:0) & b;
   assign Z = (a ? x:y) < 128;

   // this does a LOT of folding if we're not careful about order
   assign badone[0] = (x[0] ? 1: 0) | (x[1]? 1:0) | (x[2] ? 1:0) | (x[3] ? 1:0)
   | (x[4]? 1:0) | (x[5]?1:0) | (x[6]?1:0) | (x[7]?1:0);

   // This one should NOT fold
   assign badone[2:1] = (x[0] ? 2:y[1:0]) | (x[1]? 2:y[1:0]) | (x[2] ? 2:y[1:0]) | (x[3] ? 2:y[1:0])
   | (x[4]? 2:y[1:0]) | (x[5]?2:y[1:0]) | (x[6]?2:y[1:0]) | (x[7]?2:y[1:0]);

   inout             YAQC;
   assign YAQC = (a ? 1 : (!a ? 0 :YAQC)); // bug2814  - should reduce to YAQC=a

   output [7:0] PV;
   assign       PV[0] = (a & (a ? x[1] : x[0])); // => a & x[1]
   assign       PV[1] = (x!=0) && ((x!=0) ? x : y);
   assign       PV[2] = (b | (b ? x[2] : x[3]));
   assign       PV[3] = (x==0) || ((x==0) ? x[2] : x[3]);
   assign       PV[4] = (a ^ (a ? x[2] : y[3]));
   assign       PV[5] = (a ? x[7] : y[7]) & a; // check swapped operands too.
   assign       PV[6] = ^((a ? x : y) & (b ? 8'hff : 8'h00)); // bug5625
   assign       PV[7] = ^(((a < b)?x : y) | ((b > a)? y : x));
endmodule
