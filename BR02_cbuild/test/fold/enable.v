// test enable folding
module et(i,e,o);
   input i;
   input e;
   output o;

   assign o = e ? i : 1'bz;
endmodule

module top(a,ena,b,c,clk);
   input [7:0] a, ena;
   input       clk;
   output [7:0] b,c;

   et a0(a[0], 1'b0, b[0]);
   et a1(a[1], 1'b1, b[1]);
   et a2(a[2], 1'bz, b[2]);
   et a3(a[3], ena[3], b[3]);
   et a4(a[4], ena[4], b[4]);
   et a5(a[5], ena[5], b[5]);
   et a6(a[6], 1'b0, b[6]);
   et a7(a[7], 1'b0, b[7]);
   reg [7:0]    r;
   assign       c = r;
   always @(posedge clk) begin
      r = (ena==ena) ? a : 8'bz;// fold to tristate assignment
   end
   
endmodule
