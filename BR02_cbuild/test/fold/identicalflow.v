// test multiple identical flow values...

module mflow(clk,a,b,c);
   input [7:0] a;
   input [7:0] b;
   input       clk;
   output      c;

   reg [7:0]   r;
   reg [7:0]   z;  

   initial r=1;

   assign      c = r;
always @(posedge clk)
   begin
      r = 1;   
   end

   always @(negedge clk)
     begin
	r = 1;
     end

   always @(a)
     begin
	z = r + 1;
	  
     end
   
endmodule // mflow
