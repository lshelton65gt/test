module top(in, in2, out1, out2);
  output [31:0] out1;
  output [127:0] out2;
  input [7:0]   in;
  input         in2;

  // ideally in[3]<<3 should fold to in&0x8 to avoid shifting in & out of
  // 1st position, but I took that Fold out of FoldIExpr.cxx and put it
  // in Logic2LUT.cxx (commented out for now) because of concerns about
  // changing the determined bit size.
  assign        out1 = in[3] << 3;

  // This should *not* be converted to a ?BV:BV, and it was being converted,
  // due to a messed up demorgan in FoldIExpr.cxx.  To make sure it doesn't
  // go back, I'm putting it in here as an explicit test.
  assign        out2 = in2 << 40;
endmodule
