// folding pairwise merges of relational compares
// generate all combinations of
//          (a relop1 b) & (a relop2 b)
//          (a relop1 b) | (a relop2 b)
//
// to test merging things like:
//       (a < b) | (a==b)  => (a <= b)
//       (a < b) & (a==b)  => 1'b0


module top(input[7:0] a, input [7:0] b,
           output oand0,
           output oor0,
           output oand1,
           output oor1,
         output oand2,
        output oor2,
         output oand3,
        output oor3,
         output oand4,
        output oor4,
         output oand5,
        output oor5,
         output oand6,
        output oor6,
         output oand7,
        output oor7,
         output oand8,
        output oor8,
         output oand9,
        output oor9,
         output oand10,
        output oor10,
         output oand11,
        output oor11,
         output oand12,
        output oor12,
         output oand13,
        output oor13,
         output oand14,
        output oor14,
         output oand15,
        output oor15,
         output oand16,
        output oor16,
         output oand17,
        output oor17,
         output oand18,
        output oor18,
         output oand19,
        output oor19,
         output oand20,
        output oor20,
         output oand21,
        output oor21,
         output oand22,
        output oor22,
         output oand23,
        output oor23,
         output oand24,
        output oor24,
         output oand25,
        output oor25,
         output oand26,
        output oor26,
         output oand27,
        output oor27,
         output oand28,
        output oor28,
         output oand29,
        output oor29,
         output oand30,
        output oor30,
         output oand31,
        output oor31,
         output oand32,
        output oor32,
         output oand33,
        output oor33,
         output oand34,
        output oor34,
         output oand35,
        output oor35,
         output oand36,
        output oor36,
         output oand37,
        output oor37,
         output oand38,
        output oor38,
         output oand39,
        output oor39,
         output oand40,
        output oor40,
         output oand41,
        output oor41,
         output oand42,
        output oor42,
         output oand43,
        output oor43,
         output oand44,
        output oor44,
         output oand45,
        output oor45,
         output oand46,
        output oor46,
         output oand47,
        output oor47,
         output oand48,
        output oor48,
         output oand49,
        output oor49,
         output oand50,
        output oor50,
         output oand51,
        output oor51,
         output oand52,
        output oor52,
         output oand53,
        output oor53,
         output oand54,
        output oor54,
         output oand55,
        output oor55,
         output oand56,
        output oor56,
         output oand57,
        output oor57,
         output oand58,
        output oor58,
         output oand59,
        output oor59,
         output oand60,
        output oor60,
         output oand61,
        output oor61,
         output oand62,
        output oor62,
         output oand63,
        output oor63,
         output oand64,
        output oor64,
         output oand65,
        output oor65,
         output oand66,
        output oor66,
         output oand67,
        output oor67,
         output oand68,
        output oor68,
         output oand69,
        output oor69,
         output oand70,
        output oor70,
         output oand71,
        output oor71,
         output oand72,
        output oor72,
         output oand73,
        output oor73,
         output oand74,
        output oor74,
         output oand75,
        output oor75,
         output oand76,
        output oor76,
         output oand77,
        output oor77,
         output oand78,
        output oor78,
         output oand79,
        output oor79,
         output oand80,
        output oor80);
           
assign oand0 = (a == b) & (a == b);
assign oor0  = (a == b) | (a == b);
assign oand1 = (a == b) & (a != b);
assign oor1  = (a == b) | (a != b);
assign oand2 = (a == b) & (a < b);
assign oor2  = (a == b) | (a < b);
assign oand3 = (a == b) & (a <= b);
assign oor3  = (a == b) | (a <= b);
assign oand4 = (a == b) & (a > b);
assign oor4  = (a == b) | (a > b);
assign oand5 = (a == b) & (a >= b);
assign oor5  = (a == b) | (a >= b);
assign oand6 = (a == b) & ($signed(a) < $signed(b));
assign oor6  = (a == b) | ($signed(a) < $signed(b));
assign oand7 = (a == b) & ($signed(a) <= $signed(b));
assign oor7  = (a == b) | ($signed(a) <= $signed(b));
assign oand8 = (a == b) & ($signed(a) > $signed(b));
assign oor8  = (a == b) | ($signed(a) > $signed(b));
assign oand9 = (a == b) & ($signed(a) >= $signed(b));
assign oor9  = (a == b) | ($signed(a) >= $signed(b));
assign oand10 = (a != b) & (a == b);
assign oor10  = (a != b) | (a == b);
assign oand11 = (a != b) & (a != b);
assign oor11  = (a != b) | (a != b);
assign oand12 = (a != b) & (a < b);
assign oor12  = (a != b) | (a < b);
assign oand13 = (a != b) & (a <= b);
assign oor13  = (a != b) | (a <= b);
assign oand14 = (a != b) & (a > b);
assign oor14  = (a != b) | (a > b);
assign oand15 = (a != b) & (a >= b);
assign oor15  = (a != b) | (a >= b);
assign oand16 = (a != b) & ($signed(a) < $signed(b));
assign oor16  = (a != b) | ($signed(a) < $signed(b));
assign oand17 = (a != b) & ($signed(a) <= $signed(b));
assign oor17  = (a != b) | ($signed(a) <= $signed(b));
assign oand18 = (a != b) & ($signed(a) > $signed(b));
assign oor18  = (a != b) | ($signed(a) > $signed(b));
assign oand19 = (a != b) & ($signed(a) >= $signed(b));
assign oor19  = (a != b) | ($signed(a) >= $signed(b));
assign oand20 = (a < b) & (a == b);
assign oor20  = (a < b) | (a == b);
assign oand21 = (a < b) & (a != b);
assign oor21  = (a < b) | (a != b);
assign oand22 = (a < b) & (a < b);
assign oor22  = (a < b) | (a < b);
assign oand23 = (a < b) & (a <= b);
assign oor23  = (a < b) | (a <= b);
assign oand24 = (a < b) & (a > b);
assign oor24  = (a < b) | (a > b);
assign oand25 = (a < b) & (a >= b);
assign oor25  = (a < b) | (a >= b);
assign oand26 = (a < b) & ($signed(a) < $signed(b));
assign oor26  = (a < b) | ($signed(a) < $signed(b));
assign oand27 = (a < b) & ($signed(a) <= $signed(b));
assign oor27  = (a < b) | ($signed(a) <= $signed(b));
assign oand28 = (a < b) & ($signed(a) > $signed(b));
assign oor28  = (a < b) | ($signed(a) > $signed(b));
assign oand29 = (a < b) & ($signed(a) >= $signed(b));
assign oor29  = (a < b) | ($signed(a) >= $signed(b));
assign oand30 = (a <= b) & (a == b);
assign oor30  = (a <= b) | (a == b);
assign oand31 = (a <= b) & (a != b);
assign oor31  = (a <= b) | (a != b);
assign oand32 = (a <= b) & (a < b);
assign oor32  = (a <= b) | (a < b);
assign oand33 = (a <= b) & (a <= b);
assign oor33  = (a <= b) | (a <= b);
assign oand34 = (a <= b) & (a > b);
assign oor34  = (a <= b) | (a > b);
assign oand35 = (a <= b) & (a >= b);
assign oor35  = (a <= b) | (a >= b);
assign oand36 = (a <= b) & ($signed(a) < $signed(b));
assign oor36  = (a <= b) | ($signed(a) < $signed(b));
assign oand37 = (a <= b) & ($signed(a) <= $signed(b));
assign oor37  = (a <= b) | ($signed(a) <= $signed(b));
assign oand38 = (a <= b) & ($signed(a) > $signed(b));
assign oor38  = (a <= b) | ($signed(a) > $signed(b));
assign oand39 = (a <= b) & ($signed(a) >= $signed(b));
assign oor39  = (a <= b) | ($signed(a) >= $signed(b));
assign oand40 = (a > b) & (a == b);
assign oor40  = (a > b) | (a == b);
assign oand41 = (a > b) & (a != b);
assign oor41  = (a > b) | (a != b);
assign oand42 = (a > b) & (a < b);
assign oor42  = (a > b) | (a < b);
assign oand43 = (a > b) & (a <= b);
assign oor43  = (a > b) | (a <= b);
assign oand44 = (a > b) & (a > b);
assign oor44  = (a > b) | (a > b);
assign oand45 = (a > b) & (a >= b);
assign oor45  = (a > b) | (a >= b);
assign oand46 = (a > b) & ($signed(a) < $signed(b));
assign oor46  = (a > b) | ($signed(a) < $signed(b));
assign oand47 = (a > b) & ($signed(a) <= $signed(b));
assign oor47  = (a > b) | ($signed(a) <= $signed(b));
assign oand48 = (a > b) & ($signed(a) > $signed(b));
assign oor48  = (a > b) | ($signed(a) > $signed(b));
assign oand49 = (a > b) & ($signed(a) >= $signed(b));
assign oor49  = (a > b) | ($signed(a) >= $signed(b));
assign oand50 = (a >= b) & (a == b);
assign oor50  = (a >= b) | (a == b);
assign oand51 = (a >= b) & (a != b);
assign oor51  = (a >= b) | (a != b);
assign oand52 = (a >= b) & (a < b);
assign oor52  = (a >= b) | (a < b);
assign oand53 = (a >= b) & (a <= b);
assign oor53  = (a >= b) | (a <= b);
assign oand54 = (a >= b) & (a > b);
assign oor54  = (a >= b) | (a > b);
assign oand55 = (a >= b) & (a >= b);
assign oor55  = (a >= b) | (a >= b);
assign oand56 = (a >= b) & ($signed(a) < $signed(b));
assign oor56  = (a >= b) | ($signed(a) < $signed(b));
assign oand57 = (a >= b) & ($signed(a) <= $signed(b));
assign oor57  = (a >= b) | ($signed(a) <= $signed(b));
assign oand58 = (a >= b) & ($signed(a) > $signed(b));
assign oor58  = (a >= b) | ($signed(a) > $signed(b));
assign oand59 = (a >= b) & ($signed(a) >= $signed(b));
assign oor59  = (a >= b) | ($signed(a) >= $signed(b));
assign oand60 = ($signed(a) < $signed(b)) & (a == b);
assign oor60  = ($signed(a) < $signed(b)) | (a == b);
assign oand61 = ($signed(a) < $signed(b)) & (a != b);
assign oor61  = ($signed(a) < $signed(b)) | (a != b);
assign oand62 = ($signed(a) < $signed(b)) & (a < b);
assign oor62  = ($signed(a) < $signed(b)) | (a < b);
assign oand63 = ($signed(a) < $signed(b)) & (a <= b);
assign oor63  = ($signed(a) < $signed(b)) | (a <= b);
assign oand64 = ($signed(a) < $signed(b)) & (a > b);
assign oor64  = ($signed(a) < $signed(b)) | (a > b);
assign oand65 = ($signed(a) < $signed(b)) & (a >= b);
assign oor65  = ($signed(a) < $signed(b)) | (a >= b);
assign oand66 = ($signed(a) < $signed(b)) & ($signed(a) < $signed(b));
assign oor66  = ($signed(a) < $signed(b)) | ($signed(a) < $signed(b));
assign oand67 = ($signed(a) < $signed(b)) & ($signed(a) <= $signed(b));
assign oor67  = ($signed(a) < $signed(b)) | ($signed(a) <= $signed(b));
assign oand68 = ($signed(a) < $signed(b)) & ($signed(a) > $signed(b));
assign oor68  = ($signed(a) < $signed(b)) | ($signed(a) > $signed(b));
assign oand69 = ($signed(a) < $signed(b)) & ($signed(a) >= $signed(b));
assign oor69  = ($signed(a) < $signed(b)) | ($signed(a) >= $signed(b));
assign oand70 = ($signed(a) <= $signed(b)) & (a == b);
assign oor70  = ($signed(a) <= $signed(b)) | (a == b);
assign oand71 = ($signed(a) <= $signed(b)) & (a != b);
assign oor71  = ($signed(a) <= $signed(b)) | (a != b);
assign oand72 = ($signed(a) <= $signed(b)) & (a < b);
assign oor72  = ($signed(a) <= $signed(b)) | (a < b);
assign oand73 = ($signed(a) <= $signed(b)) & (a <= b);
assign oor73  = ($signed(a) <= $signed(b)) | (a <= b);
assign oand74 = ($signed(a) <= $signed(b)) & (a > b);
assign oor74  = ($signed(a) <= $signed(b)) | (a > b);
assign oand75 = ($signed(a) <= $signed(b)) & (a >= b);
assign oor75  = ($signed(a) <= $signed(b)) | (a >= b);
assign oand76 = ($signed(a) <= $signed(b)) & ($signed(a) < $signed(b));
assign oor76  = ($signed(a) <= $signed(b)) | ($signed(a) < $signed(b));
assign oand77 = ($signed(a) <= $signed(b)) & ($signed(a) <= $signed(b));
assign oor77  = ($signed(a) <= $signed(b)) | ($signed(a) <= $signed(b));
assign oand78 = ($signed(a) <= $signed(b)) & ($signed(a) > $signed(b));
assign oor78  = ($signed(a) <= $signed(b)) | ($signed(a) > $signed(b));
assign oand79 = ($signed(a) <= $signed(b)) & ($signed(a) >= $signed(b));
assign oor79  = ($signed(a) <= $signed(b)) | ($signed(a) >= $signed(b));
assign oand80 = ($signed(a) > $signed(b)) & (a == b);
assign oor80  = ($signed(a) > $signed(b)) | (a == b);
assign oand81 = ($signed(a) > $signed(b)) & (a != b);
assign oor81  = ($signed(a) > $signed(b)) | (a != b);
assign oand82 = ($signed(a) > $signed(b)) & (a < b);
assign oor82  = ($signed(a) > $signed(b)) | (a < b);
assign oand83 = ($signed(a) > $signed(b)) & (a <= b);
assign oor83  = ($signed(a) > $signed(b)) | (a <= b);
assign oand84 = ($signed(a) > $signed(b)) & (a > b);
assign oor84  = ($signed(a) > $signed(b)) | (a > b);
assign oand85 = ($signed(a) > $signed(b)) & (a >= b);
assign oor85  = ($signed(a) > $signed(b)) | (a >= b);
assign oand86 = ($signed(a) > $signed(b)) & ($signed(a) < $signed(b));
assign oor86  = ($signed(a) > $signed(b)) | ($signed(a) < $signed(b));
assign oand87 = ($signed(a) > $signed(b)) & ($signed(a) <= $signed(b));
assign oor87  = ($signed(a) > $signed(b)) | ($signed(a) <= $signed(b));
assign oand88 = ($signed(a) > $signed(b)) & ($signed(a) > $signed(b));
assign oor88  = ($signed(a) > $signed(b)) | ($signed(a) > $signed(b));
assign oand89 = ($signed(a) > $signed(b)) & ($signed(a) >= $signed(b));
assign oor89  = ($signed(a) > $signed(b)) | ($signed(a) >= $signed(b));
assign oand90 = ($signed(a) >= $signed(b)) & (a == b);
assign oor90  = ($signed(a) >= $signed(b)) | (a == b);
assign oand91 = ($signed(a) >= $signed(b)) & (a != b);
assign oor91  = ($signed(a) >= $signed(b)) | (a != b);
assign oand92 = ($signed(a) >= $signed(b)) & (a < b);
assign oor92  = ($signed(a) >= $signed(b)) | (a < b);
assign oand93 = ($signed(a) >= $signed(b)) & (a <= b);
assign oor93  = ($signed(a) >= $signed(b)) | (a <= b);
assign oand94 = ($signed(a) >= $signed(b)) & (a > b);
assign oor94  = ($signed(a) >= $signed(b)) | (a > b);
assign oand95 = ($signed(a) >= $signed(b)) & (a >= b);
assign oor95  = ($signed(a) >= $signed(b)) | (a >= b);
assign oand96 = ($signed(a) >= $signed(b)) & ($signed(a) < $signed(b));
assign oor96  = ($signed(a) >= $signed(b)) | ($signed(a) < $signed(b));
assign oand97 = ($signed(a) >= $signed(b)) & ($signed(a) <= $signed(b));
assign oor97  = ($signed(a) >= $signed(b)) | ($signed(a) <= $signed(b));
assign oand98 = ($signed(a) >= $signed(b)) & ($signed(a) > $signed(b));
assign oor98  = ($signed(a) >= $signed(b)) | ($signed(a) > $signed(b));
assign oand99 = ($signed(a) >= $signed(b)) & ($signed(a) >= $signed(b));
assign oor99  = ($signed(a) >= $signed(b)) | ($signed(a) >= $signed(b));

endmodule

       
        
