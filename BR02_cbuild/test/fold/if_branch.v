// Test of reducing branches in if stmts by moving
// constant assigns out of the if stmt when possible.
module top(clk, cond1, cond2, cond3, a1, a2, a3, a4, b);
   input clk, cond1, cond2, cond3, b;
   output a1, a2, a3, a4;
   reg    a1, a2, a3, a4;
   always @(posedge clk)
     begin
        if (cond1)
          a1 = 0;
        else
          a1 = b;
     end
   always @(posedge clk)
     begin
        if (cond2)
          a2 = b;
        else
          a2 = 0;
     end
   always @(posedge clk)
     begin
        if (a3)
          a3 = b;
        else
          a3 = 0;
     end
   always @(posedge clk)
     begin
        if (cond3)
          a4 = b | a4;
        else
          a4 = 0;
     end
endmodule
