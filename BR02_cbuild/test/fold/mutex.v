module top(out, p1rsbm, p2wt, p1cmd, p1rsdata, p1renderrectov, p1rsindexgo);
  output out;
  input  p2wt, p1rsbm;
  input [2:0] p1cmd;
  input [31:0] p1rsdata;
  input        p1renderrectov;
  input [7:0]  p1rsindexgo;
  
  assign out = ((p1rsbm &
                 (p2wt & ((p1cmd == 2'h2) &
                          ((p1rsindexgo == 8'hf0) &
                           (((p1rsdata & 100663296) == 33554432) &
                            ((p1rsdata & 4261412864) == 3221225472)))))) |
                (p1renderrectov & ((p1rsindexgo != 8'hf0) |
                                   (!(p1rsbm &
                                      (p2wt & (p1cmd == 2'h2))))))); // ../../../customer/vhdl/dmavdpath.vhdl:2153
endmodule

