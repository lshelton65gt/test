// test findone via simple statements.
module findonestmt(i,o,k);
   input [7:0] i;
   output [4:0] o;
   output [4:0] k;

   reg [4:0]    ro, rk;
   assign       o = ro;
   assign       k = rk;
             

   // would like to see "o = FLO(i)"

   always @(i)
     if (i[7])
       ro = 7;
     else if (i[6])
       ro = 6;
     else if (i[5])
       ro = 5;
     else if (i[4])
       ro = 4;
     else if (i[3])
       ro = 3;
     else if (i[2])
       ro = 2;
     else if (i[1])
       ro = 1;
     else if (i[0])
       ro = 0;
     else
       ro = -1;
     
   always @(i)
     // Do a subrange... make sure we end up with adjust case indices...
     if (i[3])
       rk = 0;
     else if (i[4])
       rk = 1;
     else if (i[5])
       rk = 2;
     else if (i[6])
       rk = 3;
     else if (i[7])
       rk = 4;
     else
       rk = -1;
   
endmodule
