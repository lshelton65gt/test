// fold was not smart enough to fully analyze the ~ in the shift
// but codegen was, leading to codegen emitting code that GCC decided
// was redundant and warned about shift-count problems.


module top( tLI, Y);
   output  tLI;
   input   Y;

   assign          tLI = 1'b1 << (39'ha60f61e5 -
                                  (Y <<  (~(
                                           (114'h1742237b >> Y)
                                           ^ 117'hcc4daf44)))
                                  );
endmodule
