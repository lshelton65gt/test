module foo(out);
  output [7:0] out;

  integer      i, j, k, l;
  initial begin
    i = -42000;
    j =  2000000000;
    k =  400000000;
    l = -2000000000;
  end
  wire         one = 1'b1;
  wire [3:0]   mix = 4'b01xz;
  wire [31:0]  x = 42;

  assign       out = i + one + mix + x + j + k + l;
endmodule
