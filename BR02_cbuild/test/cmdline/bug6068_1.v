// last mod: Thu May 25 13:12:58 2006
// filename: test/cmdline/bug6068_1.v
// Description:  This test is used to check that a -synth_prefix cmd line
// argument works, this design will not cbuild unless it is run with the args
// -synth_prefix nonsense_sy -synth_prefix synthesis
// 


module bug6068_1(clock, in1, in2, out1);  // nonsense_sy enableOutputSysTasks
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   // check that the default prefix is supported
// carbon translate_off
   input [7:0] out1;		// this line (if processed) will cause a syntax error
// carbon translate_on   

   // check that a non-standard synthesis prefix works when specified on command line
// nonsense_sy translate_off
   input [7:0] out1;		// this line (if processed) will cause a syntax error
// nonsense_sy translate_on   


   // check that what the user might have expected as the default prefix, must
   // be specified on the command line
// synthesis translate_off
   input [7:0] out1;		// this line (if processed) will cause a syntax error
// synthesis translate_on
   
   reg [7:0] out1;		// has_typo_in_directive observe_Signal

   initial
     $display("foo");

   always @(posedge clock)
     begin: main
	case (in1)		// nonsense_sy full_case
	  7'b0:
	    out1 = in1 & in2;
	  7'b1:
	    out1 = in1 | in2;
	endcase
	     
     end
endmodule
