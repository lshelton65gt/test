`MODULE_TOP(clk, a, b);
  output a;
  input clk, b;
  reg a;
  always @(posedge clk)
    a <= b;
endmodule
