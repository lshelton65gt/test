// filename: test/cmdline/lib_subdir_01.v
// Description:  This test is used to check that the -y command line can accept
// double slash within the name (matches Verilog XL capabilities)
// run this test with the command cbuild -q -y .//Lib_subdir lib_subdir_01.v


module lib_subdir_01(clock, in1, in2, out1) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;

   bottom u1(clock, in1, in2, out1) ;
   
endmodule
