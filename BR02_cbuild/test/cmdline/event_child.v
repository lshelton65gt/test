module child (input wire a, output wire b);

   event foo;

   assign b = a;

endmodule
