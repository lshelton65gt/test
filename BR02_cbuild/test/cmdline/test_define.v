// This test verifies that a macro can expand to a quoted string with
// embedded blanks. `D should expand to "foo bar". If it expands
// to an unquoted string, e.g. foo bar, then the parse will fail
// with syntax errors.

module top(i,o);

   input i;
   output o;

   initial $display("Value of D = (%s)\n", `D);

   assign o = i;

endmodule
