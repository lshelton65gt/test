module word(i,o);
   input [31:0] i;
   output [31:0] o;

   assign 	 o[31:16] = i[15:0];
endmodule // word
