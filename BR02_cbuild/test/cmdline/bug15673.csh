#!/bin/csh -f
# this cbuild command is executed via this script so that we have complete control over how strings in environment variables are escaped 
# there was a problem with getting this to work properly in the test.run file, and we echo CARBON_CBUILD_ARGS to make sure that $ENV_VAR_3 is not expanded by the shell but instead expanded by cbuild
# by including both a -f and a -directive in the CARBON_CBUILD_ARGS this duplicates the issue seen in bug 12024
setenv ENV_VAR_1 .
setenv ENV_VAR_2 .
setenv ENV_VAR_3 .
setenv CARBON_CBUILD_ARGS '-f $ENV_VAR_3/cmd_3_15673.f -directive bot2.dir'
echo "CARBON_CBUILD_ARGS is:"
printenv | grep CARBON_CBUILD_ARGS
cbuild -q
