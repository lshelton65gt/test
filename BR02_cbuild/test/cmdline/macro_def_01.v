// filename: test/cmdline/macro_def_01.v
// Description:  This test checks that a macro, defined on the command line,
// works properly.  Note that the issue occurs when the macro is defined with no
// value, and it is followed by what appears to be a value. e.g.: +define+TP+7
// The manual says that this defines two variables 'TP' and '7'
// the second variable is nonsense and is ignored by the interra flow.

// inspired by test/bugs/bug5832 (a customer testcase)

// run this test with the command cbuild -q +define+TP+7 macro_def_01.v -enableOutputSystasks
// with verific flow this reports: macro_def_01.v:24: Alert 51158: use of undefined macro TP

//`define TP          // it is expected that +define+TP on the command line is equivalent to this line (when uncommented)
`define LOCAL_TP `TP

module macro_def_01(clock, in1, in2, out1) ;
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   initial
     begin
	// at this point we expect that LOCAL_TP is defined
`ifdef LOCAL_TP	
       $display("PASSED: macro LOCAL_TP is defined");
`else
       $display("FAILED: macro LOCAL_TP is NOT defined");
`endif	
     end
   always @(posedge clock)
     begin: main
       out1 = `LOCAL_TP in1 & in2;
     end
endmodule
