module top3(in1, out1);
   input in1;
   output out1;

   wire first = in1;
   wire second = in1;
   wire third = in1;

   assign out1 = in1;
   
endmodule
