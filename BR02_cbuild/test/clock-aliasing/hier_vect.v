// Mimic a failure seen in S3 when an expression in one level
// of hierarchy is based on a slice of a vector defined in another level
// of hierarchy.

module top(in, out, clkvec, addr, we, mclk);
  input in, we, mclk;
  input [1:0] clkvec, addr;
  output      out;

  reg [1:0]   mem[0:3];

  always @(posedge mclk)
    if (we)
      mem[addr] = clkvec;

  sub sub(in, out, mem[0]);
endmodule



module sub(in, out, clkvec);
  input in;
  input [1:0] clkvec;
  output      out;
  reg         out;
  wire        clk = clkvec[0] ^ clkvec[1];

  initial out = 0;
  always @(posedge clk)
    out <= in;
endmodule
