// last mod: Fri Jun 10 16:22:36 2005
// filename: test/clock-aliasing/bug4453_a.v
// Description:  This test was inspired by the clock aliasing problem of
// bug4453, sub.clk is forcable, and it is assigned to  bug4453_a.clk so it
// might be aliased together, but they should not since the forceSignal could
// write to it.  with 1.3328 we would get a backend compile error



module bug4453_a(i1, i2, o1, o2);
  input i1, i2;
  output o1, o2;
  reg    o1;

  wire   clk;

  always @(posedge clk)
    o1 <= i1;

  sub sub(i2, o2);
endmodule

module sub(i2, o2);
  input i2;
  output o2;
  reg    o2;
  wire   clk;                   // carbon forceSignal
  assign bug4453_a.clk = clk;
  always @(posedge clk)
    o2 <= i2;
endmodule
