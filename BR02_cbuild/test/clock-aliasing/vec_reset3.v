// bug6225 -- using dynamic bit-select as an async reset, but the
// dynamic expr is def'd in the seq always block, so it's *not* safe
// to substitute, and this testcase yields an error.
//
// Note that to get past a simple one-if-statement synthesizability
// check, I had to make this testcase have an async preset

//Sensitivity list of always block has vector that consists Async signal and sync signal.
module tc(clk_in, async_reset, async_preset, out, reset_sel_in, preset_sel_in);
  input		clk_in, async_reset, async_preset, reset_sel_in, preset_sel_in;
  output [31:0] out;
  
  reg [1:0]    reset, preset;
  reg          clk, reset_sel, preset_sel;
  reg [31:0]    out;
  
  initial begin
    reset = 0;
    preset = 0;
    reset_sel = 0;
    clk = 0;
  end

  // these delay statements force Aldec to use the same input-flow model
  // as Carbon, where at the periphery, data is faster than clock, and
  // clock is faster than reset
  always @(clk_in)
    #1 clk = clk_in;
  always @(async_reset or reset_sel_in) begin
    #3   // a delay of only 2 yields an event race due to delay in testdriver
      reset_sel = reset_sel_in;
      reset[reset_sel] = async_reset;
  end
  always @(async_preset or preset_sel_in) begin
    #5   // a delay of only 2 yields an event race due to delay in testdriver
      preset_sel = preset_sel_in;
      preset[preset_sel] = async_preset;
  end


  always @(posedge clk or
`ifdef CARBON
           posedge
`endif
           reset[reset_sel] or posedge preset[preset_sel]) begin
    if (reset[reset_sel] == 1'b1) begin
      out <= 32'b0;
    end else if (preset[preset_sel])
      out <= 32'hffff0000;
    else
      out <= out + 32'd1;
  end
endmodule
