// last mod: Wed Aug 11 12:29:26 2004
// filename: test/clock-aliasing/bug2556.v
// Description:  This file is the base of two different testcases: 
// (bug2556.v, bug2556a.v)

// This is a testcase where we did not discover the proper clock aliases:
// 
// % cbuild -dumpClockAliases bug2556.v
// 1.      MASTER: top.clk
//         Alias:  top.u1.clk
//         Alias:  top.u1.rst        <<<<<
// 2.      MASTER: top.rst
// 
// This is INCORRECT. The top.u1.rst net should not be aliased to the clocks.
// 
// % cbuild -dumpClockAliases +define+GOODCLOCKS bug.v
// 1.      MASTER: top.clk
//         Alias:  top.u1.clk
// 2.      MASTER: top.rst
//         Alias:  top.u1.rst
// 
// This is the correct aliasing.


module top(ctl_in, in1, in2, out1, out2);
   input [1:0] ctl_in;
   input       in1, in2;
   output      out1, out2;

   wire       clk = ctl_in[0];
   wire       rst = ctl_in[1];

   reg [1:0]  ctl;
   reg [2:0]  ctl1;
   always @(ctl_in) 
     begin
`ifdef GOODCLOCKS
	ctl[0] = rst;
	ctl[1] = clk;
`else
	ctl[0] = ctl_in[1];
        ctl[1] = ctl_in[0];
`endif
	ctl1 = {1'b0,ctl};
     end
   
   sub u1(ctl1, in1, out1);
   
   reg 	       out2;

   wire #1     rst_delay = rst; // delay added to allow aldec to match carbon
   always @(posedge clk or posedge rst_delay)
     if (rst_delay)
       out2 <= 0;
     else
       out2 <= in2;
endmodule // top

module sub(sctl, sin, sout);
   input [2:0] sctl;
   input       sin;
   output      sout;
   reg         sout;

   wire        sclk = sctl[1];
   wire        srst = sctl[0];

   wire #1     srst_delay = srst; // delay added to allow aldec to match carbon
   always @(posedge sclk or posedge srst_delay)
     if (srst_delay)
       sout <= 0;
     else
       sout <= sin;
endmodule // sub
