module bug(async_rst_l, clk, out, inThing);   
   input          async_rst_l;
   input 	  clk;
   input          inThing;
   
   output 	  out;
   
   reg 		  out;
   reg 		  async_rst;
   
  initial out = 1;

   always @(async_rst_l) begin
     #2 async_rst = (~async_rst_l);
   end

   // posedge async_rst seems to be the problem here
   always @(posedge clk or posedge async_rst) begin
      if (async_rst) begin
	 out <= 1'b1;
      end
      else
	begin
	    out <= inThing;
	end
   end

endmodule // bug

