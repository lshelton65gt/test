// filename: bid_clock_nodir.v
// This is a variant of bid_clock.v, but without an 'input' directive.
// (which has been deprecated).

module top(clka, clkb, data, en, rst, o1, o2);
   input clka;
   input clkb;
   input data;
   input rst; // this was once an inout, that was converted to an input with the now deprecated 'input' directive
   input en;
   output o1;
   output o2;

//  pullup(rst);

  reg     rst_buf;
  initial rst_buf = 0;
  always @(rst)
    #1 rst_buf = rst;

`ifdef WHICH
   wire   local_rst;
   pad pad (rst_buf, local_rst);
`endif

   flop1 flop1 (data, 1'b1, clka, en, o1);

`ifdef WHICH
   flop2 flop2 (data, local_rst, clka, o2);
`else
   flop2 flop2 (data, rst_buf, clka, o2);
`endif

endmodule // top

`ifdef WHICH
module pad(rst, local_rst);
   inout rst;
   output local_rst;
   assign local_rst = rst;
endmodule // pad
`endif

module flop1(d, r, c, en, o);
   input d, r, c, en;
   reg 	 q;
   output o;
   initial q = 1;
   always @(posedge c or negedge r)
     begin
	if (!r)
	  q <= 1'b0;
	else
	  q <= en ? d : q;
     end
   assign o = q;
   
endmodule // flop1


module flop2(d, r, c, o);
   input d, r, c;
   output o;
   reg 	  q;
   initial q = 1;
   always @(posedge c or negedge r)
     begin
	if (!r)
	  q <= 1'b0;
	else
	  q <= d;
     end
   assign o = q;
endmodule // flop2
