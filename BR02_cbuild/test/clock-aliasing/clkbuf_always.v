module clkbuf(in, out, clk);
  input in, clk;
  output out;

  reg    q1, q2, out;
  reg    clk1, clk2, clk_;

  initial begin
    q1 = 0;
    q2 = 0;
    out = 0;
  end
  always @(clk or clk1 or clk_) begin
    clk1 = clk;
    clk_ = ~clk1;
    clk2 = ~clk_;
  end
  always @(posedge clk)
    q1 <= in;

  always @(posedge clk1)
    q2 <= q1;

  always @(posedge clk2)
    out <= q2;
endmodule // clkbuf
