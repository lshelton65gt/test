// last mod: Wed Mar  9 16:30:19 2005
// filename: test/clock-aliasing/syncreset.v
// Description:  This test is like test/vhdl/syncreset/floped.vhd but is verilog
// and demonstrates that a confusing error message is printed about a clock that
// is driven by a flop that has a sync reset.


module syncreset(clk, ext_reset, in0, in1, in2, out1, out2);
   input clk;
   input ext_reset;
   input in0, in1, in2;
   output out1, out2;
   wire   derived_clk;

   flop i1(clk,          in0,   ext_reset,  derived_clk); // drive derived_clk from flop that has a sync reset

   flop i2(clk,          in1, derived_clk,         out1); // use derived_clk as a sync_reset signal

   flop i3(derived_clk,  in2,        1'b0,         out2); // use derived_clk as a clock

   
endmodule

module flop(clock, in, sync_reset, out);
   input clock;
   input sync_reset;
   input in;
   output out;
   reg 	  out;

   always @(posedge clock)
      begin
	 if ( sync_reset )
	   out = 0;
	 else
	   out = in;
      end
endmodule


