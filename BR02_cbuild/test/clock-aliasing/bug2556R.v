// last mod: Wed Aug 25 15:55:30 2004
// filename: test/clock-aliasing/bug2556R.v
// Description:  This file is the base of two different testcases: 
// (bug2556R.v, bug2556Ra.v)

// this is similar to bug2556 except that the bits in ctl have been reversed to
// make sure that we are not dependent upon clk being in one of the
// positions. (we were at one time.)


module top(ctl_in, in1, in2, out1, out2);
   input [1:0] ctl_in;
   input       in1, in2;
   output      out1, out2;

   wire       clk = ctl_in[0];
   wire       rst = ctl_in[1];

   reg [1:0]  ctl;
   reg [2:0]  ctl1;
   always @(ctl_in) 
     begin
`ifdef GOODCLOCKS
	ctl[1] = rst;
	ctl[0] = clk;
`else
	ctl[1] = ctl_in[1];
        ctl[0] = ctl_in[0];
`endif
	ctl1 = {1'b0,ctl};
     end
   
   sub u1(ctl1, in1, out1);
   
   reg 	       out2;

   wire #1     rst_delay = rst; // delay added to allow aldec to match carbon
   always @(posedge clk or posedge rst_delay)
     if (rst_delay)
       out2 <= 0;
     else
       out2 <= in2;
endmodule // top

module sub(sctl, sin, sout);
   input [2:0] sctl;
   input       sin;
   output      sout;
   reg         sout;

   wire        sclk = sctl[0];
   wire        srst = sctl[1];

   wire #1     srst_delay = srst; // delay added to allow aldec to match carbon
   always @(posedge sclk or posedge srst_delay)
     if (srst_delay)
       sout <= 0;
     else
       sout <= sin;
endmodule // sub
