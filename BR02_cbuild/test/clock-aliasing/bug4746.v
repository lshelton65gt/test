// bug4746 - Test for following clocks through a vector when the
// vector has partial drivers.
module top(clks_a, in1, in2, out1, out2);
  input [1:0] clks_a;
  input in1;
  input in2;
  output out1;
  output out2;

  // Output regs (1)
  reg                         out2;

  // Local nets (6)
  wire         [0:1]          relay;
  wire                        clk;
  wire                        rst;
  wire                        rst_delay;
  /*temp*/ reg [2:0]          lower_1;

  // Continuous Assigns
  assign clk = clks_a[1];                          // vector_ati5.v:11
  assign rst = clks_a[0];                          // vector_ati5.v:12
  initial lower_1[0] = 1'b0; // vector_ati5.v:16

  // Always Blocks
  always @(posedge rst)                            // vector_ati5.v:22
    // $block_vector_ati5_v_L22;1
    begin                                          // vector_ati5.v:22
      // Declarations
      // Statements
      out2 = 1'b0;                                 // vector_ati5.v:22
    end
  always @(posedge clk)                            // vector_ati5.v:24
    // $block_vector_ati5_v_L21;1
    begin                                          // vector_ati5.v:24
      // Declarations
      // Statements
      out2 = in2;                                  // vector_ati5.v:24
    end
  always @(clks_a)                                          // vector_ati5.v:16
    // $block_vector_ati5_v_L16;5
    begin :named                                         // vector_ati5.v:16
      // Declarations
      reg [1:0] rescope_1;
      // Statements
      rescope_1[0] = clks_a[1];             // vector_ati5.v:14
      rescope_1[1] = clks_a[0];             // vector_ati5.v:13
      lower_1[2:1] = rescope_1; // vector_ati5.v:16
    end

  // Instances
  sub                                              // vector_ati5.v:37
    u1(                                            // vector_ati5.v:16
      .ctl(lower_1),        // input
      .in(in1),                                    // input
      .out(out1));                                 // output
endmodule // top


module sub(ctl, in, out);
  input [0:2] ctl;
  input in;
  output out;

  // Output regs (1)
  reg                         out;

  // Local nets (2)
  wire                        clk;
  wire                        rst;

  // Continuous Assigns
  assign clk = ctl[1];                             // vector_ati5.v:43
  assign rst = ctl[0];                             // vector_ati5.v:45

  // Always Blocks
  always @(posedge rst)                            // vector_ati5.v:49
    // $block_vector_ati5_v_L49;1
    begin                                          // vector_ati5.v:49
      // Declarations
      // Statements
      out = 1'b0;                                  // vector_ati5.v:49
    end
  always @(posedge clk)                            // vector_ati5.v:51
    // $block_vector_ati5_v_L47;1
    begin                                          // vector_ati5.v:51
      // Declarations
      // Statements
      out = in;                                    // vector_ati5.v:51
    end
endmodule // sub
