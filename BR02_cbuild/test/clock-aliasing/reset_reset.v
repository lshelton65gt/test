module bug(clk1, clk2, rst, a, b, t, y, z);
   input clk1;
   input clk2;
   input rst;
   input a;
   input b;
   input t;
   output y;
   output z;

   wire   int_rst1;
   wire   int_rst2;

   rst_sync rst1(.clk(clk1),
		 .rst_in(rst),
		 .test(t),
		 .rst_out(int_rst1));
   rst_sync rst2(.clk(clk2),
		 .rst_in(rst),
		 .test(t),
		 .rst_out(int_rst2));

   and2 and2(.clk(clk1),
	     .rst(int_rst1),
	     .a(a),
	     .b(b),
	     .z(y));

   or2 or2(.clk(clk2),
	   .rst(int_rst2),
	   .a(a),
	   .b(b),
	   .z(z));

endmodule

module rst_sync(clk, rst_in, test, rst_out);
   input clk;
   input rst_in;
   input test;
   output rst_out;

   reg 	  rst_d;
   reg 	  rst_out_pretest;

   always @(posedge clk)
     rst_d <= rst_in;

   initial rst_out_pretest = 0;

   always @(posedge clk or negedge rst_in)
     if (~rst_in)
       rst_out_pretest <= 1'b0;
     else
       rst_out_pretest <= rst_d;

   assign rst_out = rst_out_pretest | test;
   
endmodule

module and2(clk, rst, a, b, z);
   input clk;
   input rst;
   input a;
   input b;
   output z;
   reg 	  z;

   initial z = 0;

   always @(posedge clk or negedge rst)
     if (~rst)
       z <= 1'b0;
     else
       z <= a & b;
endmodule

module or2(clk, rst, a, b, z);
   input clk;
   input rst;
   input a;
   input b;
   output z;
   reg 	  z;

   initial z = 0;

   always @(posedge clk or negedge rst)
     if (~rst)
       z <= 1'b0;
     else
       z <= a | b;
endmodule

