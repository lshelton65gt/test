module top(clk, in, test_en, test_rst_, out);
  input			clk;
  input [1:0]		in;
  input			test_en;
  input			test_rst_;
  output [1:0]          out;

  foo u1(clk, in[0], test_en, test_rst_, out[0]);
  foo u2(clk, in[1], test_en, test_rst_, out[1]);
endmodule

module foo (clk, in, test_en, test_rst_, out);
  input			clk;
  input			in;
  input			test_en;
  input			test_rst_;
  output                out;
  
  wire			rst_;
  reg			out;
  reg			in_d1r;
  reg			out_e1;
  
  assign                rst_ = (test_en ? test_rst_ : in);
  
  always @(posedge clk or negedge rst_) begin
    if (~rst_) begin
      in_d1r <= 1'b0;
      out_e1 <= 1'b0;
    end
    else
      begin
	in_d1r <= in;
	out_e1 <= in_d1r;
      end
  end
  always @(posedge clk) begin
    out <= out_e1;
  end
endmodule 

