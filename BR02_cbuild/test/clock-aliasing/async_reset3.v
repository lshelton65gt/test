// bug7702: In this testcase we used to issue a spurious warning about
// multiple drivers.  See bug4478.  This is now fixed.  But we still
// have a problem that we do not detect that iclk1 and iclk2 are equivalent.
// 
// This is becasue the "funky reset blocks" that are
// synthesized by localflow/Reset.cxx are inserted in the priority chain.
// We can't currently prove they are equivalent because expression synthesis
// fails, becasue of latch-like constructs like:
//     if (r2) 
//       iclk2 = i2;
//     else
//       if (r3)
//         iclk2 = i3;
// We successfully identify funky reset blocks in clock analysis.  We could
// potentially discard them from the equivalence check because they are
// really synthesized from the other blocks.  But Richard things that's too
// aggressive because latch conversion may create other funky reset blocks
// that are not safe to ignore for clock equivalence.

module top(in1, in2, i1, i2, i3, i4, r1, r2, r3, out1, out2);
  input in1, in2, i1, i2, i3, i4, r1, r2, r3;
  output out1, out2;
  reg    out1, out2, iclk1, iclk2;

  always @(posedge r1 or posedge r2 or posedge r3)
    if (r1)
      iclk1 <= i1;
    else if (r2)
      iclk1 <= i2;
    else if (r3)
      iclk1 <= i3;
    else
      iclk1 <= i4;

  always @(posedge r1 or posedge r2 or posedge r3)
    if (r1)
      iclk2 <= i1;
    else if (r2)
      iclk2 <= i2;
    else if (r3)
      iclk2 <= i3;
    else
      iclk2 <= i4;

  always @(posedge iclk1)
    out1 <= in1;

  always @(posedge iclk2)
    out2 <= in2;
endmodule
