module bug (fastclk, ireset, in, out, iclk);
   input fastclk;
   input iclk;
   input [3:0] ireset;
   input [3:0] in;
   output [3:0] out;
   wire [0:3] 	reset_a,  reset_b;
   reg clk;
   reg [3:0]    reset;

   always @(posedge fastclk) begin
     reset <= ireset;
     clk <= iclk;
   end

   core core (reset_a, reset);
   
   assign reset_b[0] = reset_a[0];
   assign reset_b[1] = reset_a[1];
   assign reset_b[2] = reset_a[2];
   assign reset_b[3] = reset_a[3];

   ser ser0 (reset_b[0], in[0], out[0], clk);
   ser ser1 (reset_b[1], in[1], out[1], clk);
   ser ser2 (reset_b[2], in[2], out[2], clk);
   ser ser3 (reset_b[3], in[3], out[3], clk);
   
endmodule // bug

module core (reset_a, reset);
   input [3:0] reset;
   output [0:3] reset_a;
   wire [ 3:0]   reset_cat;
   wire 	 reset_s3, reset_s2, reset_s1, reset_s0;

   mux mux (reset_a, reset_cat);

   assign 	 reset_cat = ({reset_s3, reset_s2, reset_s1, reset_s0});

   slice slice0 (reset_s3, reset[0]);
   slice slice1 (reset_s2, reset[1]);
   slice slice2 (reset_s1, reset[2]);
   slice slice3 (reset_s0, reset[3]);
endmodule

module slice (out,in);
   input in;
   output out;
   assign out = in;
endmodule // slice

module mux (reset_a, reset_cat);
   input [3:0] reset_cat;
   output [0:3] reset_a;
   wire [3:0] 	 reset_muxout;

   assign reset_muxout    = reset_cat;
   assign reset_a      = ({reset_muxout[0], reset_muxout[1], reset_muxout[2], reset_muxout[3]});

endmodule
   
module ser(reset, in, out, clk);
   input reset, in, clk;
   output out;
   reg 	  out;
  reg     rst;

  always @(reset)
    #1 rst = reset;

   always @(posedge clk or posedge rst)
     if (rst)
       out <= 1'b1;
     else
       out <= in;
   
endmodule
