// In the AMCC 3450, there is a lot of equivalence being proved in the
// derived clock tree.  This has the potential to cause some pain when
// the flow-dependencies don't point us toward the canonical paths.
// there is a computedClks assertion in DerivedClockSchedules.cxx that
// comes up because when following the canonical clocks, we find a
// derived clock that hasn't been computed yet.

module top(clk, c1, c2, i1, i2, i3, clk2, o1, o2, o3);
  input clk, c1, c2, i1, i2, i3, clk2;
  output o1, o2, o3;

  reg    dclk1, dclk2;          // these are equivalent
  reg    o1, o2, o3;

  always @(posedge clk)
    dclk1 = ~dclk1;
  always @(posedge clk)
    dclk2 = ~dclk2;

  not(clk2_, clk2);
  
  always @(posedge dclk1)
    o1 <= i1;
  always @(posedge dclk2)
    o2 <= i2;
  always @(posedge clk2_)
    o3 <= i3;

endmodule // top
