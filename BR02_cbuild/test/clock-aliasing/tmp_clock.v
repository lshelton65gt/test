// filename: test/clock-aliasing/tmp_clock.v
// Description:  This test demonstrates a problem with population of the hc
// signal (use verific_interra_compare.csh script to debug)
// note that the gold simulation results for this test were manually checked
// against mti results. For some reason carbon handles hc[11] as a clock
// (because of the first always block) but this signal is also used to control a
// latch (for out1) 

module tmp_clock(hc, in1, in2, out1, out2) ;
   input [11:11] hc;
   input [7:0] in1, in2;
   output [7:0] out1, out2;
   reg [7:0] out1, out2;

   always @(posedge hc[11] )
     out2 = in1;
   
   always @(hc[11] or in1 or in2)
     begin: main
	if ( ~hc[11]) 
	  out1 = in1 & in2;
     end
endmodule
