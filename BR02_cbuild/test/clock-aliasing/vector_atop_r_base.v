// test packaging up a clock in a vector in a scan chain element
// and cascading a few of them 
// based on vector_atop.v, but in this example only the reset is placed in a
// vector (the clock is passed outside of the vector)

// this file is the base of the vector_atop_r*.v series

module top(clks_a, in1, in2, in3, in4, in5, out1, out2, out3, out4, out5, clk);

   input [1:0] clks_a;
  input in1, in2, in3, in4, in5, clk;
  output out1, out2, out3, out4, out5;

  wire [1:0] ctl;
  wire [1:0] ctl1;

  wire       rst = clks_a[0];
//   assign    ctl1[0] = 1'b0;  // why does non inclusion of this line cause clk analysis to not recognize equivalent reset signals?
   assign    ctl1[1] = rst;

  wire [1:0] ctl2, ctl3, ctl4, ctl5, ctl6;

  sub u1({1'b0, ctl1}, in1, out1, ctl2, clk);
  sub u2({1'b0, ctl2}, in2, out2, ctl3, clk);
  sub u3({1'b0, ctl3}, in3, out3, ctl4, clk);
  sub u4({1'b0, ctl3}, in4, out4, ctl5, clk);
  sub u5({1'b0, ctl3}, in5, out5, ctl6, clk);
endmodule // top

module sub(ctl, in, out, ctlout, clk);
  input [2:0] ctl;
  input       in, clk;
  output      out;
  output [1:0] ctlout;
  reg         out;

  buf #(2) (rst, ctl[1]);

  always @(posedge clk or posedge rst) begin
`ifdef MTI                      // deglitch zero-delay event ordering
    #1 if (clk || rst) begin
`endif
      if (rst)
        out <= 0;
      else
        out <= in;
`ifdef MTI
    end
`endif
  end

   assign      ctlout[1] = rst;
   assign      ctlout[0] = 1'b0;
endmodule // sub
