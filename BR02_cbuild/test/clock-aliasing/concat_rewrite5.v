// This case originated in bug4805.

module top(ctl_in, in1, in2, in3, out1, out2, out3);
  input [1:0] ctl_in;
  input in1;
  input in2, in3;
  output out1;
  output out2, out3;

  // Output regs (1)
  reg                         out2;

  // Local nets (5)
  wire                        clk;
  wire                        rst;
  reg          [1:0]          ctl;
  reg          [2:0]          ctl1;
  wire                        rst_delay;

  // Continuous Assigns
  assign clk = ctl_in[0];                          // bug2556R.v:16
  assign rst = ctl_in[1];                          // bug2556R.v:17

  // Always Blocks
  always @(clk or rst)
    // $block_bug2556R_v_L21;1
    begin: named                                          // bug2556R.v:21
      // Declarations
      reg [1:0] rescope_1;
      // Statements
      rescope_1[0] = clk;                     // bug2556R.v:25
      rescope_1[1] = rst;                     // bug2556R.v:24
      ctl1[2] = 1'b0;                              // bug2556R.v:30
      ctl1[1:0] = rescope_1;                  // bug2556R.v:30
    end
  always @(posedge rst)                            // bug2556R.v:40
    // $block_bug2556R_v_L40;1
    begin                                          // bug2556R.v:40
      // Declarations
      // Statements
      out2 = 1'b0;                                // bug2556R.v:40
    end
  always @(posedge clk)                            // bug2556R.v:42
    // $block_bug2556R_v_L39;1
    begin                                          // bug2556R.v:42
      // Declarations
      // Statements
      out2 = in2;                                  // bug2556R.v:42
    end

  // Instances
  sub                                              // bug2556R.v:45
    u1(                                            // bug2556R.v:33
      .sctl(ctl1),                                 // input
      .sin(in1),                                   // input
      .sout(out1));                                // output
  // Instances
  sub                                              // bug2556R.v:45
    u3(                                            // bug2556R.v:33
      .sctl(ctl1),                                 // input
      .sin(in3),                                   // input
      .sout(out3));                                // output
endmodule // top
module sub(sctl, sin, sout);
  input [2:0] sctl;
  input sin;
  output sout;

  // Output regs (1)
  reg                         sout;

  // Local nets (3)
  wire                        sclk;
  wire                        srst;
  wire                        srst_delay;

  // Continuous Assigns
  assign sclk = sctl[0];                           // bug2556R.v:51
  assign srst = sctl[1];                           // bug2556R.v:52

  // Always Blocks
  always @(posedge srst)                           // bug2556R.v:57
    // $block_bug2556R_v_L57;1
    begin                                          // bug2556R.v:57
      // Declarations
      // Statements
      sout = 1'b0;                                // bug2556R.v:57
    end
  always @(posedge sclk)                           // bug2556R.v:59
    // $block_bug2556R_v_L55;1
    begin                                          // bug2556R.v:59
      // Declarations
      // Statements
      sout = sin;                                  // bug2556R.v:59
    end
endmodule // sub
