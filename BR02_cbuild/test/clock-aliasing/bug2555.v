// last mod: Thu Aug 12 10:03:12 2004
// filename: test/clock-aliasing/bug2555.v
// Description:  This file is the base of two different testcases: 
// (bug2555.v, bug2555a.v) they should all get the same results


// testcase from bug2555, this single testcase identified at least 2 problems
// 
// The top.s0.clk and top.s1.clk nets should be aliases. The top.s0.rst and
// top.s1.rst nets should also be aliases.
// 
// % cbuild -dumpClockAliases bug.v
// 1.      MASTER: top.s0.clk
// 2.      MASTER: (inactive) top.s1.clk
// 3.      MASTER: top.s0.rst
// 4.      MASTER: top.s1.rst
// 
// No aliases were discovered. Further, top.s1.clk is marked inactive!
// 
// % cbuild -dumpClockAliases +define+GOODCLOCKS bug.v
// 1.      MASTER: top.s0.clk
//         Alias:  top.s1.clk
// 2.      MASTER: top.s0.rst
// 3.      MASTER: top.s1.rst
// 
// We found the clock aliases, but missed the resets.
//
// in bug2555 there was also a report of a problem related to finding the resets
// but not the clocks, but that problem was due to an inconsistency in the
// logic for the clocks in the testcase.  This problem has been corrected in
// this file and so that part of the testing has been removed.
// 
// in both cases we SHOULD find the same set of aliases.


// a note about getting simulation results.  I was unable to introduce the
// necessary delays that would allow carbon and aldac to get the same results,
// the problem is that clt_in and hold are all used to create clock edges.  I
// was able to match aldec results except in the case where
// ctl[1](1->0), hold(0->1) and in0(0->1), in this case aldec would see the
// rising edge on clk and load the new value for in0, while carbon would see the
// old value for in0.  Simply putting a delay on in0 caused different problems
// so I solved this issue by manual analysis of the .vlog file.
module top(ctl_in,hold,in0,in1,out0,out1);
   input [1:0] ctl_in;
   input       hold,in0,in1;
   output      out0,out1;
//   wire        clk = ctl_in[1];
//   wire        rst = ctl_in[0];
   wire [1:0]  ctl0;
   assign #3     ctl0[0] = ctl_in[1];	// delay added to keep aldec results in sync with carbon
   assign #2     ctl0[1] = ctl_in[0];	// delay added to keep aldec results in sync with carbon
   wire [1:0]  ctl1;
   wire [1:0]  ctl2;
   sub s0({1'b0,ctl0},in0,out0,ctl1,hold);
   sub s1({1'b0,ctl1},in1,out1,ctl2,hold);
endmodule

module sub(ctl,in,out,ctlout,hold);
   input [2:0] ctl;
   input       in;
   output      out;
   output [1:0] ctlout;
   input 	hold;

   wire 	clk = ctl[1] & hold;
   wire 	rst = ctl[0];

   reg 		out;

   always @(posedge clk or posedge rst)
     begin
	if (rst) out = 0;
	else     out = in;
     end

   reg [1:0] ctlout;
   always @(ctl or clk or hold) begin
`ifdef GOODCLOCKS
      ctlout[0] = ctl[0];
      ctlout[1] = clk;
`else
      ctlout[0] = ctl[0];
      ctlout[1] = ctl[1] & hold;
`endif
   end
endmodule
