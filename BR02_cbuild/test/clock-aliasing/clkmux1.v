module top(clk, d, q0, sel);
   input clk, sel;
   input [1:0] d;
   output [1:0] q0;
   wire [1:0] 	 clk_vec;
   reg           qsel, clk2;
   
   initial begin
     qsel = 0;
     clk2 = 0;
   end

   // divide down clk.
   always @(posedge clk)
     clk2 <= ~clk2;

   // sel should only change on negedge clk to avoid a glitch
   // and an NC mismatch with Carbon
   always @(negedge clk)
     qsel <= sel;

   assign clk_vec = qsel ? 
	  {~clk2, ~clk2} :
	  {clk2, clk2};
   flop u0 (clk_vec[0], d[0], q0[0]);
   flop u1 (clk_vec[1], d[1], q0[1]);
endmodule

module flop (clk, d, q);
   input clk, d;
   output q;
   reg 	  q;

   initial q = 0;

   always @(posedge clk)
     q <= d;
endmodule
