module sun_clk_mux(clk,z,a,b,s);
  output  z;
  input   a, b, s, clk;

  // keep other input quiet when it's not in use
  wire a_quiet, b_quiet;
  //CLKAND2P  and_gate (.Z(b_quiet), .A(b), .B(s));
  //CLKOR2P   or_gate  (.Z(a_quiet), .A(a), .B(s));
  assign b_quiet = b & s;
  assign a_quiet = a | s;
  reg    z;
  initial z = 0;


//  CLKMUX21P gates (.Z(z), .A(a), .B(b), .S(s));
//`ifdef CARBON
//   assign z = s ? b : a ;
//`else
   always @(negedge clk)
     z <= s ? b_quiet : a_quiet ;
//`endif
endmodule

module top(clk, ic1, ic2, isel, ii1, ii2, o1, o2);
  input clk, ic1, ic2, isel, ii1, ii2;
  output o1, o2;
  reg    o1, o2, c1, c2, sel, i1, i2;

  initial begin
    c1 <= 1'b0;
    c2 <= 1'b0;
    sel <= 1'b0;
    i1 <= 1'b0;
    i2 <= 1'b0;
    o1 <= 1'b0;
    o2 <= 1'b0;
  end

  always @(posedge clk) begin
    i1 <= ii1;
    i2 <= ii2;
    c1 <= ic1;
    c2 <= ic2;
    sel <= isel;
  end

  sun_clk_mux m1(clk, clk1, c1, c2, sel);
  sun_clk_mux m2(clk, clk2, c1, c2, sel); // should be proved equiv.

  always @(posedge clk1)
    o1 <= i1;
  always @(posedge clk2)
    o2 <= i2;
endmodule
