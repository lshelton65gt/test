module clkbuf(in, out, clk);
  input in, clk;
  output out;

  reg    q1, q2, out;
  wire    clk1, dead_clk, clk_;

  initial begin
    q1 = 0;
    q2 = 0;
    out = 0;
  end

  assign clk1 = clk;
  assign clk_ = ~clk1;
  assign dead_clk = ~clk_;

  always @(posedge clk)
    q1 <= in;

  always @(posedge clk1)
    q2 <= q1;

  always @(posedge clk1)
    out <= q2;
endmodule // clkbuf
