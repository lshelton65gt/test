module top (fclk, iclk, out1, out2, iin1, iin2);
   output out1, out2;
   inout  fclk;
   input  iclk, iin1, iin2;
   reg    fclkreg, clk, in1, in2;

   tri1   rclk;
   assign rclk = clk;

   initial begin
     clk = 0;
     in1 = 0;
     in2 = 0;
   end

   pulldown(fclk);

   always @(posedge fclk) begin
     clk <= iclk;
     in1 <= iin1;
     in2 <= iin2;
   end

   dut DUT (out1, out2, clk, rclk, in1, in2);
   
endmodule // top

module dut (out1, out2, clk1, clk2, in1, in2);
   output out1, out2;
   input  clk1;
   inout  clk2;
   input  in1, in2;

   reg    out1, out2, gclk;
   initial begin
     gclk = 0;
     out1 = 0;
     out2 = 0;
   end

   buf #(3) (in1_delay, in1);
   always @ (posedge clk1)
     out1 <= in1_delay;

   always @ (clk2 or in1)
     gclk = clk2 & in1;
   buf #(3) (in2_delay, in2);
   
   always @ (posedge gclk)
     out2 <= in2_delay;

endmodule
