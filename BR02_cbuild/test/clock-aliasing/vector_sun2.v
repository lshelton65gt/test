module bug (clk, d, q0, q1, sel);
   input clk, sel;
   input [1:0] d;
   output [1:0] q0, q1;
   wire [0:15] 	 clk_vec;
   reg [1:0]     q1;

   initial q1 = 2'b0;
   
   assign clk_vec[0:7] = {clk, ~clk, 1'b1, 1'b0, 1'b1, 1'b0, 1'b1, 1'b0};
   assign clk_vec[8:15] = clk_vec[0:7];

   flop u8 (clk_vec[8], d[0], q0[0]);
   flop u9 (clk_vec[9], d[1], q0[1]);

   always @(posedge clk_vec[8])
     q1[0] <= d[0];

   always @(posedge clk_vec[9])
     q1[1] <= d[1];
endmodule

module flop (clk, d, q);
   input clk, d;
   output q;
   reg 	  q;

   initial q = 0;
   always @(posedge clk)
     q <= d;

endmodule

