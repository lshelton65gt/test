// last mod: Wed Aug 25 17:04:37 2004
// filename: test/clock-aliasing/bug2573b.v
// Description:  This is like bug2573a but a different bit of cvec is selected
// to make sure that this testcase works even when something other than the lsb
// of a vector is used as a clock

module bug2573( pwrdown, in, out, cin);
   input pwrdown, in, cin;
   output out;
   reg 	  out;

   wire [4:0] cvec;	// one bit of this needs to be used as a clock

   stram stram(cvec, pwrdown, cin);
   initial out = 0;

   
   always @(posedge cvec[1])
     begin
	out = in;
     end

endmodule


module stram(cvec, pwrdown, cin);
   
   output  [4:0]     	cvec ;	// data out
   input 		pwrdown;// Power Down
   input                cin;
   
   

`ifdef THISWORKS
   // FYI: this alternate definition for the latch works fine
   reg [4:0] 		cvec;		
   initial
     cvec = 0;
   always @(pwrdown)
      begin
	 if ( !pwrdown )
	   cvec = cin;
      end
`else
   // this definition for cvec causes the infinite recursion
   assign 		cvec     = (pwrdown )?cvec:{cin,1'b0};// make cvec a latch
`endif
   
   
   
endmodule

