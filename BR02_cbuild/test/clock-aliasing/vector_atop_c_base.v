// test packaging up a clock in a vector in a scan chain element
// and cascading a few of them 
// based on vector_atop_r.v, but in this example the reset has been removed, and
// at one time the clock was not recognized as being equivalent in both instances.

// this file is the base of the vector_atop_c*.v series

module top(clk, in1, in2, out1, out2);
   input       clk;
   input       in1, in2;
   output      out1, out2;

   wire [1:0]  ctl1;

   //   assign    ctl1[0] = 1'b0;  // at one time having an undriven bit in ctl1
   //   caused clk analysis to be unable to recognize that the clocks in sub1
   //   and sub2 should be aliased
   assign      ctl1[1] = clk;

   wire [1:0]  ctl2, ctl3, ctl4;

   sub1 u1({1'b0, ctl1}, in1, out1, ctl2);
   sub2 u2({1'b0, ctl2}, in2, out2, ctl3);
endmodule // top

module sub1(ctl, in, out, ctlout);
   input [2:0] ctl;
   input       in;
   output      out;
   output [1:0] ctlout;
   reg 		out;
   wire 	clk;

   assign 	clk = ctl[1];

   always @(posedge clk) begin
      out <= in;
   end

//   assign ctlout = {clk,1'b0}; // doing a full assign causes correct recognition of clocks
   assign      ctlout[1] = clk;
   assign      ctlout[0] = 1'b0;
endmodule // sub


module sub2(ctl, in, out, ctlout);
   input [2:0] ctl;
   input       in;
   output      out;
   output [1:0] ctlout;
   reg 		out;
   wire 	clk;

   assign 	clk = ctl[1];

   always @(posedge clk) begin
      out <= in;
   end

//   assign ctlout = {clk,1'b0}; // doing a full assign causes correct recognition of clocks
   assign      ctlout[1] = clk;
   assign      ctlout[0] = 1'b0;
endmodule // sub
