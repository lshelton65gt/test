module top(clk, c, i1, i2, o1, o2);
  input clk, c, i1, i2;
  output o1, o2;

  reg    c1, c2, o1, o2;
  wire [2:0] five = 3'b101;
  wire [1:0] two = five[2:1];
  wire       zero = two[0];
  wire [1:0] onev = five[1:0];
  wire       one = onev[0];


  initial begin
    c1 = 0;
    c2 = 0;
  end

  always @(posedge clk)
    c1 <= ~(c1 & one);

  always @(posedge clk)
    c2 <= ~(c2 | zero);

  always @(posedge c1)
    o1 <= i1;

  always @(posedge c2)
    o2 <= i2;
endmodule
