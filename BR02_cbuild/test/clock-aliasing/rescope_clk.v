// This is a testcase to make sure that we don't issue a spurious
// undriven-in-clk-path warning (1015).  This testcase is from
// bug3333/bug1.v
//
// The directives file both defines the c-model and promotes the
// warning to an error.

module top (out, clk, in, clk_in);
  output [31:0] out;
  input [31:0]  in;
  input         clk, clk_in;

  // Make a derived clock with a task.
  reg [31:0]    out;
  reg           dclk, tin1;
  
  initial begin
    out = 0;
    dclk = 0;
  end

  always @ (posedge clk) begin
    neg(tin1, clk_in);
    dclk <= ~dclk & tin1;
  end
  
  always @ (posedge dclk)
    out <= in;
  
  task neg;
    output tin1;
    input in1;
    
    tin1 = ~in1;
  endtask
endmodule // top

