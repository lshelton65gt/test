// file: vector_rjc1a.v
// created: 10/13/03
// author: cloutier
// General topic: A Clock that is part of a vector at top level is split at
//                lower level, do we alias them? 
// specific for this test: In this test three instance of sub1 are used, so we
//                         should merge the clocks.  In this example the main
//                         clock passes through a register before being used in
//                         sub1.
// desired result: the three clocks within instances of sub1 should be aliased

module top(wide_clk, clk1, in1, in2, in3, out1, out2, out3);
   input [7:0] wide_clk;
   input       clk1;
   input       in1, in2, in3;
   output      out1, out2, out3;

   wire [1:0]  ctl;

//   wire [7:0]  wide_clk_int;
//   assign      wide_clk_int = wide_clk;

   reg [7:0] wide_clk_int;

   initial wide_clk_int = 8'b0;

   always @(posedge clk1)
     wide_clk_int <= wide_clk;
   
   

  sub1 u1 (wide_clk_int, in1, out1);
  sub1 u2 (wide_clk_int, in2, out2);
  sub1 u3 (wide_clk_int, in3, out3);
endmodule // top

module sub1(wide_clk, in, out);
  input [7:0] wide_clk;
  input       in;
  output      out;
  reg         out;

  wire        clk = wide_clk[1];
  initial out = 1'b0;

  always @(posedge clk)
      out <= in;
endmodule // sub

