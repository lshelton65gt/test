module clkbuf(in, out, clk, cin);
  input in, clk, cin;
  output out;

  reg    q1, q2, q3, q4, out, clk2, clk3, clk1, clk_;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
    out = 0;
    clk2 = 0;
    clk3 = 0;
    clk1 = 0;
    clk_ = 1;
  end

  wire  [2:0]   add = 3'b0;

  always @(posedge clk) begin
    clk1 <= cin;
    clk_ <= ~clk1;

    // In this test, we have two equivalent clock trees, clk2 and clk3,
    // where the trees are partially non-BDD-able,
    clk2 <= ~clk_ + add;
    clk3 <= clk1 + add;
  end

  always @(posedge clk1)
    q1 <= in;

  always @(posedge clk_)
    q2 <= q1;

  always @(negedge clk2)
    q3 <= q2;

  always @(posedge clk3)
    q4 <= q3;

  always @(posedge clk2)
    out <= q4;
endmodule // clkbuf
