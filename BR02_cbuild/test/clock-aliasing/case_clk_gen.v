module top(i, o, iclk, phase);
  input iclk, phase;
  input [1:0] i;
  output [1:0] o;

  reg           clk1, clk2;
  reg [1:0]     o;

  initial begin
    clk1 = 1'b0;
    clk2 = 1'b0;
    o = 2'b0;
  end

  // bug7335: clock analysis was incorrectly following nesting
  // through case-statements where variables were only def'd in one branch.
  // I don't think this would have been a problem if either clk1 or clk2
  // was def'd in more than one branch.
  always @(posedge iclk)
    case (phase)
      1'b0: clk1 = !clk1;
      1'b1: clk2 = !clk2;
    endcase

  always @(posedge clk1)
    o[0] <= i[0];

  always @(posedge clk2)
    o[1] <= i[1];
endmodule
