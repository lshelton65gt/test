module top(out1, out2, c0, c1, sel, in1, in2);
  output out1, out2;
  input  c0, c1, sel, in1, in2;

  reg    clk1, clk2, out1, out2;
  initial begin
    clk1 = 0;
    clk2 = 0;
    out1 = 0;
    out2 = 0;
  end

  always @(c0 or c1 or sel)
    case (sel)
      1'b0: clk1 = c0;
      1'b1: clk1 = c1;
    endcase

  always @(c0 or c1 or sel)
    case (sel)
      1'b0: clk2 = c0;
      1'b1: clk2 = c1;
    endcase

  always @(posedge clk1)
    out1 <= in1;
  always @(posedge clk2)
    out2 <= in2;
endmodule
