module foo(i1, i2, e1, e2, out);
  input i1, i2, e1, e2;
  inout out;

  assign out = e1? i1: 1'bz;
  assign out = e2? i2: 1'bz;
endmodule
