// This is copied from test/bugs/bug4430.  It has a real multi-driver in the
// clock path, so we should warn about it.

// We are not trying to do sim diffs here, as random verilog is not
// good for diffs against 4-state simulators.  So there is no sim gold file.

// Generated with: ./rangen -s 72 -n 0x3520d660 -o testcases.04.26.2005/assert7.v
// Using seed: 0x3520d660
// Writing cyclic circuit of size 72 to file 'testcases.04.26.2005/assert7.v'
// Module count = 7

// module ELsTE
// size = 72 with 5 ports
module ELsTE(W, OH, xS, qX, z);
input W;
input OH;
input xS;
input qX;
output z;

wire W;
wire [2:30] OH;
wire xS;
wire qX;
reg [22:65] z;
reg [0:7] IK;
reg [0:20] ZpiVi;
wire [11:29] oZT;
wire cpT;
reg [3:3] KA;
wire [0:4] viVa;
reg Tf;
reg [17:89] TzyLS;
reg [0:2] eR;
reg xvRp;
reg [0:7] efl;
reg X;
reg [4:7] oM;
wire xTi;
reg [2:3] Q;
reg GJNvm;
reg vCfOz;
reg vLZMO;
reg [5:7] rUq;
reg [5:26] Xqn;
reg [13:17] aJWR;
reg [1:16] ukld_;
reg nhYt;
reg [18:125] XVE;
wire Tgf;
wire [0:2] gKPN;
reg [11:14] a;
reg Uc;
wire OSt;
wire [15:17] AQdXC;
reg Lfqan;
reg [10:15] v;
reg [4:4] Vtisz;
reg [25:26] aNHl;
reg [0:5] lDQWW;
wire [18:22] DsAPT;
reg [13:23] LnlOP;
wire VMdX;
reg [72:76] uL;
reg [2:4] bC;
reg [24:27] lxUa;
reg Ym;
reg CK;
reg eaAB;
reg [2:4] lxOvQ;
reg sghtK;
reg [21:26] V;
reg [2:5] AY;
reg lWcHr;
reg [1:6] M;
reg dM;
reg EtU;
reg [2:2] baRYU;
reg [2:6] ioy;
reg [4:16] JmYM;
reg CM;
reg pPD;
reg xbt;
reg css;
wire ps;
reg EW;
reg ufAxT;
reg [10:27] e;
IFa c ({v[12:14],X,KA[3],V[23:26],KA[3],lxOvQ[2:4],vCfOz,1'b0}, {Tgf,KA[3],AQdXC[16],vCfOz});

assign DsAPT[19:21] = Tgf;

always @(negedge xbt)
begin
begin
uL[73] = X^DsAPT[19:21];
vCfOz = ~LnlOP[17:18];
end
end

always @(negedge vCfOz)
begin :iLW
reg [13:23] J;
reg [10:25] mnBL;
reg sBN;
reg o;
reg [8:9] FbxC;

begin
lWcHr = xbt;
end
end

always @(posedge cpT)
begin
begin
Uc = ~4'b0110;
end
end

always @(posedge vCfOz)
begin
begin
CK = ~1'b0;
end
end

always @(posedge &lxOvQ[2:4])
begin
begin
JmYM[11:13] = ~(~uL[73]);
end
end

always @(negedge AQdXC[16])
begin
begin
efl[1:7] <= ~7'b0011101;
end
end

always @(uL[73])
if (uL[73])
begin
if (TzyLS[38:67] == 30'b110001000101110010110100110110)
begin
z[26:65] = (~5'b01011);
end
end
else
begin
end

always @(negedge &XVE[45:79])
begin :oaa
reg [87:103] uLzo;
reg PCe;

begin
for (e[11:12]=2'b11; e[11:12]>2'b11; e[11:12]=e[11:12]+2'b10)
begin
CM = 9'b001000111;
baRYU[2] <= XVE[45:79]|efl[1:7];
end
end
end

assign cpT = ~CM;

always @(negedge &z[22:31])
begin :Slna
reg WWl;
reg [12:19] qy_Ir;
reg [2:4] HYj;
reg [0:2] jZV;
reg [0:2] j_FtG;

begin
M[1:3] <= (6'b000100);
end
end

always @(cpT)
if (~cpT)
begin
css <= ~(~6'b100111);
end

always @(posedge &ukld_[2:9])
begin
begin
end
end

wire GS = &efl[1:7];
always @(posedge &JmYM[4:15] or negedge GS)
if (~GS)
begin
begin
rUq[5:6] = ~nhYt|AQdXC[16];
end
end
else
begin
begin
efl[1:7] <= qX;
end
end

always @(posedge CK)
begin :QiTu
reg [7:14] WKI;
reg [2:3] cuSs;
reg [1:3] UBMM;

begin
WKI[8:10] = ~lWcHr^V[23:26];
end
end

assign DsAPT[19:21] = JmYM[4:15];

always @(posedge vCfOz)
begin
begin
case (W)
1'd0:
begin
xvRp = baRYU[2];
X = 2'b00;
end
1'd1:
begin
case (cpT)
1'd0:
begin
efl[0:5] <= ((((baRYU[2]) + (~xvRp))));
end
1'd1:
begin
LnlOP[14:23] = efl[1:7]^xvRp;
end
default:
begin
baRYU[2] <= ~LnlOP[14:23]^OH[12:24];
end
endcase
end
default:
begin
lxOvQ[2] = ~((ukld_[5:15]));
end
endcase
end
end

assign gKPN[1] = ~(ufAxT) + (W^ukld_[2:9]);

assign viVa[1:4] = xvRp;

assign cpT = z[29:35]^lDQWW[2:5];

always @(&XVE[45:79])
if (~XVE[45:79])
begin
KA[3] = ~vCfOz;
end

always @(posedge &efl[4:6])
begin
begin
lDQWW[4:5] = ~efl[1:7];
end
end

always @(posedge AQdXC[16])
begin
begin
end
end

assign xTi = ~AQdXC[16];

always @(negedge &z[45:55])
begin
begin
lxOvQ[2] = (lDQWW[2:5]);
end
end

assign VMdX = ~M[1:3]^efl[1:7];

assign Tgf = W;

always @(&DsAPT[19:21])
if (~DsAPT[19:21])
begin
nhYt = ~(~ufAxT&xS);
end

assign gKPN[1] = ~z[26:65];

assign xTi = ukld_[4:10];

always @(posedge css)
begin
begin
M[1:3] = ~(~lxOvQ[2:4]);
end
end

always @(X)
if (X)
begin
oM[7] <= ukld_[5:15];
end
else
begin
bC[2:3] = ~(lWcHr);
end

always @(negedge xbt)
begin :sA
reg [0:2] IO;
reg [2:7] WS;
reg [2:4] TrOE;
reg [2:4] lmuer;
reg mFr;

begin
Ym <= ~AQdXC[16];
end
end

always @(&rUq[5:6])
if (rUq[5:6])
begin
XVE[19:35] = ~KA[3]^JmYM[4:15];
end

always @(posedge &JmYM[11:13])
begin
begin
Lfqan = ~(OH[12:24]^nhYt) + (~JmYM[11:13]&baRYU[2]);
dM = ~v[12:14];
end
end

always @(posedge &efl[4:6])
begin
begin
Xqn[10:15] <= ~Ym;
end
end

always @(Lfqan)
if (~Lfqan)
begin
case (efl[0:5])
6'd0:
begin
a[12:14] <= KA[3];
end
6'd1:
begin
ZpiVi[0:3] = ~efl[4:6];
end
6'd2:
begin
uL[73] = (~z[29:35]);
end
6'd3:
begin
vCfOz = ~AQdXC[16];
end
6'd4:
begin
Q[2:3] = ~OH[12:24]^oM[7];
end
6'd5:
begin
css = lWcHr|W;
end
6'd6:
begin
lWcHr = ~(~Uc&dM) + (~xvRp);
end
6'd7:
begin
if (e[11:12] == 2'b11)
begin
M[1:3] <= (~OH[12:24]^lWcHr);
end
end
6'd8:
begin
Uc <= baRYU[2];
end
6'd9:
begin
TzyLS[26:55] = ~ioy[5:6];
end
default:
begin
if (qX == 1'b0)
begin
X = ~6'b001000;
end
end
endcase
end
else
begin
Xqn[10:15] = ~e[11:12];
end

always @(&LnlOP[14:23])
if (~LnlOP[14:23])
begin
end
else
begin
Vtisz[4] = ~4'b1011;
end

always @(posedge X)
begin
begin
JmYM[11:13] = ~(efl[1:7]) + (~nhYt);
end
end

assign oZT[15:16] = (xbt != 1'b0) ? ~eaAB^LnlOP[17:18] : 2'bz;

always @(negedge baRYU[2])
begin
begin
TzyLS[26:55] = ukld_[2:9]^LnlOP[17:18];
end
end

assign cpT = ~(efl[1:7]);

always @(negedge &LnlOP[14:23])
begin
begin
a[13] = ~JmYM[11:13];
end
end

always @(posedge a[13])
begin
begin
baRYU[2] = ~JmYM[4:15];
end
end

assign xTi = ~(~Q[2:3]);

always @(&OH[12:24])
if (OH[12:24])
begin
efl[0:5] = ~efl[0:5]^qX;
end

always @(posedge &V[23:26])
begin
begin
JmYM[13:14] = ~vCfOz^XVE[19:35];
end
end

assign cpT = ~Xqn[10:15];

always @(negedge AQdXC[16])
begin
begin
baRYU[2] = ~Lfqan;
end
end

wire RIprt = Lfqan;
always @(posedge lxOvQ[2] or negedge RIprt)
if (~RIprt)
begin
begin
ufAxT <= ~xvRp^W;
end
end
else
begin :BDDR
reg [15:21] lr;

begin
xbt = Uc;
end
end

endmodule

// module IFa
// size = 1 with 2 ports
module IFa(hxHe, eWqX);
input hxHe;
input eWqX;

wire [14:28] hxHe;
wire [15:18] eWqX;
reg [1:7] _CF;
reg [5:30] G_f;
reg [4:31] YO;
reg [3:9] Ar;
reg [19:22] ee;
reg [5:5] RDA;
reg [37:94] ur;
reg [0:7] uKo;
reg f;
reg L_yGS;
reg [1:7] GICjm;
reg [3:6] tFZ;
reg [1:4] _QZVB;
wire HKhBa;
always @(&hxHe[15:18])
if (hxHe[15:18])
begin
L_yGS = ~hxHe[15:18];
end

endmodule
