// test packaging up a clock in a vector

module top(iclk, rst, clk1, ena1, iin, out, iin2, out2);
  input       iin, rst, iclk, clk1, ena1, iin2;
  output      out, out2;

  reg [1:0] ctl;
  reg        out2, ena, clk, u1_clk, clock, u1_rst, out;
  reg        in, in2, d_u1_rst, d_rst, d_in, d_in2;

  initial begin
    ctl = 0;
    out2 = 0;
    ena = 0;
    clk = 0;
    u1_clk = 0;
    clock = 0;
    u1_rst = 0;
    out = 0;
    in = 0;
    in2 = 0;
    ctl = 2'b11;
    d_u1_rst = 0;
    d_rst = 0;
    d_in = in;
    d_in2 = in2;
  end

  // clock in the internal clock signals so that they
  // don't glitch in XL when computing the gold file
  always @(posedge iclk) begin
    clk <= clk1;
    ena <= ena1;
    ctl[1] <= clk & ena;
    clock <= ctl[1];
    u1_clk <= ctl[1];
    ctl[0] <= rst;
    u1_rst <= ctl[0];
    in <= iin;
    in2 <= iin2;
  end

  always @(u1_rst)
    #1 d_u1_rst = u1_rst;
  always @(in)
    #2 d_in = in;

  always @(posedge u1_clk or posedge d_u1_rst)
    if (d_u1_rst)
      out <= 0;
    else
      out <= d_in;

  always @(rst)
    #1 d_rst = rst;
  always @(in2)
    #2 d_in2 = in2;

  always @(posedge clock or posedge d_rst)
    if (d_rst)
      out2 <= 0;
    else
      out2 <= d_in2;
endmodule // top
