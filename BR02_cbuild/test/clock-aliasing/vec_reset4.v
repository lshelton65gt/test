// bug6225 -- using static bit-select as an async reset

//Sensitivity list of always block has vector that consists Async signal and sync signal.
module tc(clk_in, async_reset, async_preset, out);
  input		clk_in, async_reset, async_preset;
  output [31:0] out;
  
  reg [1:0]    reset;
  reg          clk;
  reg [31:0]    out;
  
  // these delay statements force Aldec to use the same input-flow model
  // as Carbon, where at the periphery, data is faster than clock, and
  // clock is faster than reset
  always @(clk_in)
    #1 clk = clk_in;
  always @(async_reset)
    #5 reset[0] = async_reset;
  always @(async_preset)
    #3 reset[1] = async_preset;

  always @(posedge clk or posedge reset[0] or posedge reset[1]) begin
    if (reset[0] == 1'b1) //reset[0] is asynchronous reset
      out <= 32'b0;
    else
      begin
	if(reset[1] == 1'b1) //reset[1] is asynchronous preset
	  out <= 32'hffff0000;
	else
	  out <= out + 32'd1;
      end
  end
  
endmodule
