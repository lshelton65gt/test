// last mod: Wed Aug 25 17:05:37 2004
// filename: test/clock-aliasing/bug2573.v
// Description:  This test duplicates a problem found in test/cust/picojava.
// with version 1.2029 this goes into an infinite recursion and eventually
// fails.

module bug2573( pwrdown, out);
   input pwrdown;
   output out;
   reg 	  out;
   initial out = 0;

   wire [4:0] xdo;	// one bit of this needs to be used as a clock

   stram stram(xdo, pwrdown);

   
   always @(posedge xdo[0])
     begin
	out = pwrdown;
     end

endmodule


module stram(xdo, pwrdown);
   
   output  [4:0]     	xdo ;	// data out
   input 		pwrdown;// Power Down
   
   

`ifdef THISWORKS
   // FYI: this alternate definition for the latch works fine
   reg [4:0] 		xdo;		
   initial
     xdo = 0;
   always @(pwrdown)
      begin
	 if ( !pwrdown )
	   xdo = 0;
      end
`else
   // this definition for xdo causes the infinite recursion
   assign 		xdo     = (pwrdown )?xdo:0;// make xdo a latch
`endif
   
   
   
endmodule

