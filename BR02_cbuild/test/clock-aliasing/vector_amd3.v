// test packaging up a clock in a vector

module top(iclk, rst, clk1, ena1, in, out, in2, out2, ena2);
  input       in, rst, iclk, clk1, ena1, in2, ena2;
  output      out, out2;

  wire [1:0] ctl;
  wire       clock = ctl[1];
  reg        out2, ena, clk;

  assign     ctl[0] = ena2? rst: 1'bz;
  assign     ctl[1] = ena2? (clk & ena): 1'bz;
  assign     ctl[0] = (~ena2)? 1'b1: 1'bz; // pullup
  assign     ctl[1] = (~ena2)? 1'b1: 1'bz; // pullup

  // clock in the internal clock signals so that they
  // don't glitch in XL when computing the gold file
  always @(posedge iclk) begin
    clk <= clk1;
    ena <= ena1;
  end

  sub u1 (ctl, in, out);
  always @(posedge clock)
    out2 <= in2;
endmodule // top

module sub(ctl, in, out);
  input [1:0] ctl;
  input       in;
  output      out;
  reg         out;

  wire        clk = ctl[1];
  buf #(1) (rst, ctl[0]);

  always @(posedge clk or posedge rst)
    if (rst)
      out <= 0;
    else
      out <= in;
endmodule // sub
