// test packaging up a clock in a vector in a scan chain element
// and cascading a few of them  (see vector_atop for simpler version of this test)

module top(clks_a, in1, in2, in3, in4, in5, out1, out2, out3, out4, out5, hold);
  input [21:20] clks_a;
  input in1, in2, in3, in4, in5, hold;
  output out1, out2, out3, out4, out5;

  wire [31:30] ctl;
  wire [41:40] relay;

  wire       clk = clks_a[21];
  wire       rst = clks_a[20];
  assign     relay[40] = rst;
  assign     relay[41] = clk;

  wire [51:50] ctl2, ctl3, ctl4, ctl5, ctl6;

  sub u1({1'b0, relay}, in1, out1, ctl2, hold);
  sub u2({1'b0, ctl2}, in2, out2, ctl3, hold);
  sub u3({1'b0, ctl3}, in3, out3, ctl4, hold);
  sub u4({1'b0, ctl3}, in4, out4, ctl5, hold);
  sub u5({1'b0, ctl3}, in5, out5, ctl6, hold);
endmodule // top

module sub(ctl, in, out, ctlout, hold);
  input [62:60] ctl;
  input       in, hold;
  output      out;
  output [71:70] ctlout;
  reg         out;

  buf #(1) (clk, ctl[61] & hold);
  buf #(2) (rst, ctl[60]);

  always @(posedge clk or posedge rst)
    if (rst)
      out <= 0;
    else
      out <= in;

  assign      ctlout[71] = clk;
  assign      ctlout[70] = rst;
endmodule // sub
