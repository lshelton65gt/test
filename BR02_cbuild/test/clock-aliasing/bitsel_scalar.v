// This example was manually culled from one of Jeff Newbern's
// randomly generated testcases.

module top(i1, i2, clkin, o1, o2);
  input i1, i2, clkin;
  output o1, o2;
  reg    o1, o2;

  initial begin
    o1 = 0;
    o2 = 0;
  end

  wire [76:117] clkvec;

  assign        clkvec[78:114] = clkin;

  // Doing expression synthesis on this causes us to take
  // clkin[22] when building the clock expression.  This is
  // legal verilog but yields an an implementation-dependent
  // result.  In the case of Carbon, that result will be 0,
  // so this clock will be labeled "(inactive)"
  always @(posedge clkvec[92])
    o1 <= i1;

  // But if we are taking bit 0 (clkvec[114]) then the 
  // canonical clock is equivalent to clkin.
  always @(posedge clkvec[114])
    o2 <= i2;
endmodule
