module clkbuf(a,x);
   input a;
   output x;
   reg 	  x,stage1,stage2;

  initial begin
    x = 0;
    stage1 = 0;
    stage2 = 0;
  end

   always @(a)
     stage1 = ~a;
   always @(stage1)
     stage2 = ~stage1;
   always @(stage2)
     x = stage2;
endmodule

module flop(clk,d,q);
   input clk,d;
   output q;
   reg    q;

  initial q = 0;

   always @(posedge clk)
     q <= d;
endmodule

module top(clk,d1,d2,sel1,sel2,q1,q2);
   input clk,d1,d2,sel1,sel2;
   output q1,q2;

   flop clkdiv(clk,~clkdiv2,clkdiv2);
   clkbuf buf1(clkdiv2,clkdiv2_1);
   clkbuf buf2(clkdiv2,clkdiv2_2);
   flop clkdiv2div1(clkdiv2_1,~clkdiv4_1,clkdiv4_1);
   flop clkdiv2div2(clkdiv2_2,~clkdiv4_2,clkdiv4_2);

   reg 	  clk1,buf3_stage1,buf3_stage2;
   initial begin
     clk1 = 0;
     buf3_stage1 = 0;
     buf3_stage2 = 0;
   end
   reg 	  preclk1;
   initial preclk1 = 0;
   always @(sel1 or clkdiv4_1) begin
      #2 preclk1 = sel1?1'b0:clkdiv4_1;
      buf3_stage1 = ~preclk1;
   end
   always @(buf3_stage1)
     buf3_stage2 = ~buf3_stage1;
   always @(buf3_stage2)
     clk1 = buf3_stage2;

  reg 	 clk2,buf4_stage1,buf4_stage2;
  initial begin
    clk2 = 0;
    buf4_stage1 = 0;
    buf4_stage2 = 0;
  end

`ifdef CARBON
    reg 	  preclk2;
    initial preclk2 = 0;
    always @(sel2 or clkdiv4_2) begin
      #1 preclk2 = sel2?1'b0:clkdiv4_2;
      buf4_stage1 = ~preclk2;
    end
`else
  // for gold file generation, mimic the collapseClock directive
  always @(buf3_stage1)
    buf4_stage1 = buf3_stage1;
`endif

   always @(buf4_stage1)
     buf4_stage2 = ~buf4_stage1;
   always @(buf4_stage2)
     clk2 = buf4_stage2;

   flop flop1(clk1,d1,q1);
   flop flop2(clk2,d2,q2);
endmodule
