// last mod: Wed Aug 25 17:13:47 2004
// filename: test/clock-aliasing/bug2573a.v
// Description:  This test duplicates a problem found in test/cust/picojava.
// with version 1.2029 this goes into an infinite recursion and eventually
// fails.  This is like bug2573 where we saw the infinite recursion but but the
// outputs did not change.  This (bug2573a.v) is adjusted so that the output
// changes value during simulation.

module bug2573( pwrdown, in, out, cin);
   input pwrdown, in, cin;
   output out;
   reg 	  out;

   wire [4:0] cvec;	// one bit of this needs to be used as a clock

   stram stram(cvec, pwrdown, cin);
   initial out = 0;

   
   always @(posedge cvec[0])
     begin
	out = in;
     end

endmodule


module stram(cvec, pwrdown, cin);
   
   output  [4:0]     	cvec ;	// data out
   input 		pwrdown;// Power Down
   input                cin;
   
   

`ifdef THISWORKS
   // FYI: this alternate definition for the latch works fine
   reg [4:0] 		cvec;		
   initial
     cvec = 0;
   always @(pwrdown)
      begin
	 if ( !pwrdown )
	   cvec = cin;
      end
`else
   // this definition for cvec causes the infinite recursion
   assign 		cvec     = (pwrdown )?cvec:cin;// make cvec a latch
`endif
   
   
   
endmodule

