// test packaging up a clock in a vector
// Same as the original but the vector's are reversed

module top(clks_a, in1, in2, out1, out2);
  input [1:0] clks_a;
  input in1, in2;
  output out1, out2;

  wire [0:1] relay;

  wire       clk = clks_a[1];
  wire       rst = clks_a[0];
  assign     relay[0] = rst;
  assign     relay[1] = clk;

  sub u1({relay, 1'b0}, in1, out1);

  reg        out2;
  buf #(1) (rst_delay, rst);
  always @(posedge clk or posedge rst_delay)
    if (rst_delay)
      out2 <= 0;
    else
      out2 <= in2;
endmodule // top

/*
module mid(ctl, in, out);
  input [2:0] ctl;
  input       in;
  output      out;

  sub sub(ctl[1:0], in, out);
endmodule
*/

module sub(ctl, in, out);
  input [0:2] ctl;
  input       in;
  output      out;
  reg         out;

  buf(clk, ctl[1]);
  //wire        clk = ctl[1];
  buf #(1) (rst, ctl[0]);

  always @(posedge clk or posedge rst)
    if (rst)
      out <= 0;
    else
      out <= in;
endmodule // sub
