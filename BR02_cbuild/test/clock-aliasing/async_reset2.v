// This testcase shows that we successfully disocver that iclk2 and iclk1 are
// equivalent.  Note that in async_reset3.v and async_reset4.v we do not
// detect equivalence.  Wee those testcases for details.

module top(in1, in2, i1, i2, i3, r1, r2, out1, out2);
  input in1, in2, i1, i2, i3, r1, r2;
  output out1, out2;
  reg    out1, out2, iclk1, iclk2;

  always @(posedge r1 or posedge r2)
    if (r1)
      iclk1 <= i1;
    else if (r2)
      iclk1 <= i2;
    else
      iclk1 <= i3;

  always @(posedge r1 or posedge r2)
    if (r1)
      iclk2 <= i1;
    else if (r2)
      iclk2 <= i2;
    else
      iclk2 <= i3;

  always @(posedge iclk1)
    out1 <= in1;

  always @(posedge iclk2)
    out2 <= in2;
endmodule
