// file: clkalias1_dep_driver.v
// general topic: testing related to interaction of clock aliasing and force/deposit directives
// specific topic: a design that has equivalent clocks should alias them even
//                 when there is a directive to make one of the drivers
//                 depositable
// this case should be run with the directive depositSignal top.clk

// expected results: top.clk and top.clk2 will be aliased
// expected results: top.clk2a and top.clk2b will be equiv

`include "clkalias1_base.v"
