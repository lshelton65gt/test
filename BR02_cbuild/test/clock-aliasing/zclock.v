// This tries to minimally reproduce the issue in bug1599, which is
// that a clock-buffer instance that is connected to a primary
// bidirect causes all other instances of that clock-buffer to be
// labelled as primary bidirects.  This causes some confusion in
// the shell, which shows all the other clocks as Z because the
// strength fields are turned off.  

module top(c1, c2, i1, i2, o1, o2);
  inout c1;
  input c2, i1, i2;
  output o1, o2;

  reg    o1, o2;
  initial begin
    o1 = 0;
    o2 = 0;
  end

  pulldown(c1);

  clkbuf cb1(c1, clk1);
  clkbuf cb2(c2, clk2);

  always @(posedge clk1)
    o1 <= i1;
  always @(posedge clk2)
    o2 <= i2;
endmodule

module clkbuf(in, out);
  input in;
  output out;

  assign out = in;
endmodule
