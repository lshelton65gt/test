// This is copied from test/bugs/bug2370/bug.v.  It has a real multiple driver
// in the clock path.

// We are not trying to do sim diffs here, as random verilog is not
// good for diffs against 4-state simulators.  So there is no sim gold file.

// Generated with: ../../rangen -n 0x733c25f5
// Using seed: 0x733c25f5
// Writing cyclic circuit of size 100 to file '733c25f5.v'
// Module count = 1

// module s
// size = 100 with 6 ports
module s(t_v, ccoW, _C, KciH, vzYS, v);
output t_v;
output ccoW;
input _C;
input KciH;
input vzYS;
output v;

reg t_v;
reg ccoW;
wire [87:118] _C;
wire [0:6] KciH;
wire [4:5] vzYS;
reg [47:108] v;
wire [46:118] jVtR;
reg [2:3] UW;
reg [2:4] W;
wire U;
wire [0:4] Yci;
wire [0:7] CEj;
reg [6:7] vfK;
wire [10:23] ABJc;
reg [1:6] Wpk;
wire [13:23] OeTn;
reg [2:4] KXcZ;
reg [9:21] Y;
wire S;
reg [3:7] hOV;
reg s__;
reg [38:84] TyIrL;
reg [1:22] hh;
wire [1:4] mI;
wire [2:7] t;
reg rs;
wire [17:19] joXp;
reg [36:111] oZ;
reg ZT;
reg O;
reg Cmcr;
wire [3:6] XOtM;
reg X;
reg ac;
wire [2:6] AWFOB;
wire [1:7] LUIX;
reg Ex;
reg NVbl;
reg JMkd;
reg [3:23] EPxI;
reg JU;
reg [1:1] M;
wire [3:4] Ece;
reg _HQN;
reg nBPCB;
wire InXk;
reg xf;
reg [1:2] Mxi;
reg [4:6] rN;
reg [3:4] vsu;
wire zFNi;
reg [1:6] uuji_;
wire LqCI;
reg [24:26] EVa;
reg a;
reg [5:7] GSS;
wire [0:5] KsA;
wire [94:105] XdI;
reg [0:1] z;
wire [2:5] WV;
reg [45:71] uM;
reg [17:27] WumDE;
reg [108:113] LDu;
reg u;
reg [2:12] xT;
reg [1:7] n;
reg [25:25] DBKth;
reg [51:74] RdE;
reg Ljen;
reg ItLKU;
wire [9:25] gIAO;
reg h;
reg [0:5] jCQ;
reg [2:7] Dxs;
wire [7:31] UcFcSV;
wire [3:5] Kk;
reg [0:3] BtEs;
reg KRb;
reg tBtfB;
reg [20:21] lVR;
reg [24:29] fAPT;
reg [5:6] dO;
reg [1:4] FqkG;
reg [4:5] e_N;
reg Neq;
reg [3:21] fmE;
reg lkS;
wire [22:22] ZU;
assign CEj[0:6] = ~(rN[5:6]|JMkd);

always @(posedge &rN[5:6])
fAPT[25:28] = ~EVa[25:26]^OeTn[14:16];

always @(negedge Dxs[5])
vsu[3:4] = M[1];

assign gIAO[9:18] = (~JMkd^EVa[25:26]);

assign t[6:7] = oZ[85:108];

assign WV[3] = Neq;

assign ZU[22] = mI[2:3]^s__;

always @(&FqkG[1:4])
if (~FqkG[1:4])
ccoW = JMkd|AWFOB[6];

always @(negedge &KciH[3:6])
uM[45:60] = a^KciH[3:6];

always @(&hOV[3:7])
if (hOV[3:7])
oZ[48:98] = jVtR[55:118];

assign WV[5] = ~gIAO[12:22];

assign jVtR[73:116] = vsu[3:4]|jVtR[47:103];

assign LUIX[1:2] = jCQ[4];

always @(posedge &oZ[44:52])
W[2] = ~xT[7:11];

assign Yci[1:2] = vzYS[5]^rN[5:6];

always @(Ex)
if (~Ex)
v[54:91] = (tBtfB);

always @(&gIAO[9:21])
if (~gIAO[9:21])
Cmcr = ~joXp[18:19]^NVbl;

assign CEj[1:2] = ~(~U);

always @(a)
if (a)
Ljen = ~s__;

assign gIAO[9:18] = (vsu[3:4] != 0) ? _C[110:111]|oZ[85:108] : 10'bz;

always @(posedge tBtfB)
O <= mI[2:3]&KXcZ[2:3];

always @(&hOV[3:7])
if (~hOV[3:7])
u <= FqkG[1:4]|n[4:6];

assign KsA[1] = ~mI[2:3];

always @(&_C[102:111])
if (_C[102:111])
O = hh[8:9];

always @(AWFOB[6])
if (AWFOB[6])
uM[60:62] = oZ[85:108]|oZ[44:52];

assign LqCI = ~uM[63:66]^a;

always @(negedge &FqkG[1:4])
uM[45:67] = FqkG[1:4];

always @(AWFOB[6])
if (~AWFOB[6])
_HQN = ((jCQ[4]));

always @(posedge JMkd)
Ljen = (~xT[7:11]^OeTn[14:16]);

always @(posedge &rN[5:6])
uuji_[1:2] = ~LDu[111:113]|Neq;

always @(negedge &KsA[1:5])
z[1] = LDu[111:113];

assign OeTn[14:22] = ~(~(xT[7:11]));

always @(posedge &_C[110:111])
Neq <= (~FqkG[1:4]);

assign LqCI = ~KsA[1:5];

assign U = ~(~(~FqkG[1:4]));

always @(posedge &mI[2:3])
TyIrL[58:84] <= n[4:6];

always @(&mI[2:3])
if (mI[2:3])
oZ[48:98] = ~NVbl;

assign LqCI = ~jVtR[47:103]|X;

always @(negedge AWFOB[6])
xT[8:10] <= Dxs[5]^FqkG[1:4];

always @(negedge tBtfB)
u = mI[1:2];

assign U = ~(~vzYS[5]);

always @(posedge &gIAO[9:21])
hh[6:8] = ~gIAO[10:23];

always @(posedge a)
O = (X&jCQ[4]);

assign XdI[98:101] = ~gIAO[10:23]&jCQ[4];

always @(&FqkG[1:4])
if (FqkG[1:4])
_HQN = ~(~(~jVtR[47:103]^joXp[18:19]));

assign S = M[1]&xT[11:12];

always @(posedge &hh[8:9])
TyIrL[58:84] = ~X;

always @(&xT[7:11])
if (xT[7:11])
v[54:91] <= Ex;

always @(negedge &rN[5:6])
e_N[4:5] = jVtR[55:118];

always @(tBtfB)
if (tBtfB)
rs = ~tBtfB&oZ[85:108];

always @(posedge a)
NVbl = rN[5:6]&oZ[55:62];

always @(&rN[5:6])
if (rN[5:6])
v[54:91] <= _C[102:111];

always @(posedge &FqkG[1:4])
JU <= EVa[25:26]|AWFOB[6];

always @(negedge &gIAO[12:22])
ac = vzYS[5]|xT[11:12];

assign WV[5] = n[4:6];

assign ZU[22] = ~(LDu[111:113]);

assign S = (EVa[25:26] != 0) ? (KciH[3:6]) : 1'bz;

always @(negedge &hh[8:9])
Mxi[2] = FqkG[1:4]^KciH[3:6];

assign zFNi = s__;

always @(&vsu[3:4])
if (~vsu[3:4])
z[1] = ~vzYS[5]|gIAO[9:21];

always @(negedge &mI[2:3])
EVa[24] = ~(~NVbl^a);

always @(posedge U)
JMkd = ~a;

always @(posedge s__)
LDu[110:113] = ~xT[11:12]^gIAO[12:22];

always @(U)
if (~U)
nBPCB = oZ[55:62];

always @(&jVtR[55:118])
if (~jVtR[55:118])
s__ = ~NVbl;

always @(negedge &KciH[3:6])
oZ[71:92] <= ~xT[7:11];

assign KsA[1] = (_C[102:111] != 0) ? (oZ[55:62]&mI[1:2]) : 1'bz;

assign OeTn[15:20] = EVa[25:26]^hh[8:9];

assign OeTn[14:22] = (~(_C[102:111]|hOV[3:7]));

assign UcFcSV[20:26] = ~a^OeTn[14:16];

assign jVtR[73:116] = (KXcZ[2:3] != 0) ? ~rN[5:6] : 44'bz;

always @(posedge &KXcZ[2:3])
z[1] <= ~_C[102:111];

always @(negedge s__)
xT[8:10] <= KciH[3:6];

assign t[6:7] = hh[11:18]&U;

always @(negedge &KsA[1:5])
vfK[6] <= ~KXcZ[2:3]^oZ[85:108];

always @(Neq)
if (~Neq)
rN[6] = s__;

always @(jCQ[4])
if (jCQ[4])
GSS[5:6] = gIAO[10:23];

always @(posedge &hOV[3:7])
Dxs[3:5] = (~n[4:6]);

assign OeTn[14:22] = ~(~EVa[25:26]);

always @(&FqkG[1:4])
if (~FqkG[1:4])
s__ = jCQ[4]^hh[11:18];

always @(NVbl)
if (~NVbl)
DBKth[25] = ~Ex;

always @(&mI[2:3])
if (mI[2:3])
uM[45:60] = ~tBtfB;

always @(&rN[5:6])
if (~rN[5:6])
Wpk[3:4] = ~mI[2:3];

always @(U)
if (~U)
rs = ~vsu[3:4]&xT[7:11];

always @(&hh[8:9])
if (~hh[8:9])
TyIrL[58:84] = hOV[3:7];

always @(vzYS[5])
if (~vzYS[5])
KRb = ~KciH[3:6]|Dxs[5];

always @(negedge JMkd)
hh[11:19] = mI[1:2];

always @(&KXcZ[2:3])
if (~KXcZ[2:3])
Y[15:18] = hOV[3:7]&KXcZ[2:3];

always @(&OeTn[14:16])
if (~OeTn[14:16])
GSS[5:6] = ~(gIAO[9:21]^X);

always @(negedge JMkd)
jCQ[1:5] = ~FqkG[1:4]^xT[11:12];

assign gIAO[9:18] = ~FqkG[2:4]|M[1];

always @(negedge &joXp[18:19])
nBPCB = ~jVtR[94:117]|_C[102:111];

always @(negedge &FqkG[1:4])
ccoW = mI[1:2]^joXp[18:19];

always @(&gIAO[12:22])
if (~gIAO[12:22])
W[2] <= ~tBtfB;

assign XdI[98:101] = rN[5:6]&vsu[3:4];

assign gIAO[9:18] = jVtR[55:118];

always @(&KciH[3:6])
if (KciH[3:6])
RdE[69:72] = KsA[1:5]^jCQ[4];

always @(posedge &oZ[55:62])
vfK[6] = ~hh[11:18]^s__;

assign LqCI = (~LDu[111:113]^gIAO[10:23]);

assign WV[3] = ~KXcZ[2:3];

endmodule
