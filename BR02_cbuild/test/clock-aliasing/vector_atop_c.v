// test packaging up a clock in a vector in a scan chain element
// and cascading a few of them 
// based on vector_atop_r.v, but in this example the reset has been removed, and
// at one time the clock was not recognized as being equivalent in both instances.

`include "vector_atop_c_base.v"
