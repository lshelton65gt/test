// Generated with: ../../rangen -n 0xf81c36f5
// Using seed: 0xf81c36f5
// Writing cyclic circuit of size 100 to file 'f81c36f5.v'
// Module count = 1

// module EMb
// size = 100 with 4 ports
module EMb(IoBcc, X, Ot, gBh);
  output IoBcc;
  input  X;
  output Ot;
  input  gBh;
  
  reg    IoBcc;
  initial IoBcc = 0;
  wire [10:16] X;
  wire [12:18] Ot;
  wire [0:6]   gBh;
  reg [24:29]  nxmAS;
  initial nxmAS = 0;
  reg          cMStW;
  reg          yj;
  initial yj = 0;
  reg          pbEn;
  reg [2:7]    VBkSe;
  initial VBkSe = 0;
  reg [6:6]    XsAtX;
  reg [53:99]  uCaPD;
  initial uCaPD = 0;
  reg [4:10]   br;
  wire         ADnOF;
  reg [3:7]    dfu;
  initial dfu = 0;
  reg [15:26]  J;
  reg [0:23]   EkCa;
  initial EkCa = 0;
  reg          YbyPE;
  wire [1:4]   BINk;
  reg          gzUQm;
  initial gzUQm = 0;
  reg [2:2]    BFS;
  reg [9:11]   x;
  initial x = 0;
  reg          z;
  reg [2:9]    CHLCL;
  initial CHLCL = 0;
  reg          usaq;
  wire [1:6]   _DA;
  reg [1:22]   WL;
  initial WL = 0;
  reg [5:17]   G;
  reg [17:28]  Mj;
  initial Mj = 0;
  reg [10:13]  hk;
  reg          Fp;
  initial Fp = 0;
  reg [0:1]    Ojda;
  reg [0:22]   t;
  initial t = 0;
  reg [3:5]    THyO;
  reg [2:6]    d;
  initial d = 0;
  reg          kYiJ;
  reg [0:7]    CEs;
  initial CEs = 0;
  reg [1:2]    NtFne;
  reg [10:15]  UAp;
  initial UAp = 0;
  reg [3:4]    YhkYt;
  reg [0:4]    V;
  initial V = 0;
  reg [21:124] Q;
  reg [93:102] MIVP;
  initial MIVP = 0;
  reg [1:3]    kC;
  wire [87:100] mXAW;
  reg           PbXQK;
  initial PbXQK = 0;
  reg [13:22]   fSjmy;
  reg           ei;
  initial ei = 0;
  reg [1:6]     F;
  reg           qXood;
  initial qXood = 0;
  reg           CFxf;
  reg           SbOzNu;
  initial SbOzNu = 0;
  wire [1:1]    j;
  wire          FIe;
  wire [6:16]   e;
  wire [1:4]    R;
  reg           _Ej;
  initial _Ej = 0;
  reg           Nm;
  reg [0:5]     Tslj;
  initial Tslj = 0;
  reg [7:31]    Ub;
  wire [12:22]  MMA;
  wire [4:7]    s_;
  wire          _VpZ;
  reg [9:31]    yM_T_;
  initial yM_T_ = 0;
  reg [0:3]     Oijbm;
  wire [1:2]    BcHM;
  reg [0:3]     Cg;
  initial Cg = 0;
  reg           pF;
  reg           pLG;
  initial pLG = 0;
  reg [3:7]     EEVv;
  reg [1:23]    Kb;
  initial Kb = 0;
  reg [1:2]     c;
  wire          TSKgA;
  wire [2:6]    XPo_t;
  reg [16:26]   S;
  initial S = 0;
  reg [4:5]     fudr;
  reg [3:3]     PgX;
  initial PgX = 0;
  reg           HHqNO;
  reg           UpDT;
  initial UpDT = 0;
  assign        MMA[15:19] = ~R[2:3];
  
  always @(posedge &gBh[0:3])
    YbyPE = ~THyO[4:5];
  
  always @(negedge &_DA[3:6])
    usaq = ~dfu[6:7]^Ot[15:18];
  
  assign        BINk[1] = ~Ot[15:16];
  
  always @(posedge &R[2:3])
    MIVP[97:98] = YhkYt[3];
  
  always @(VBkSe[3])
    if (~VBkSe[3])
      UAp[13:15] = FIe^Mj[19:23];
  
  always @(negedge &Ot[15:18])
    EkCa[15:22] <= X[11:15]|EEVv[5:6];
  
  always @(negedge &Mj[19:23])
    CEs[4:5] = EEVv[5:6]^Ot[15:18];
  
  always @(negedge &Ot[15:18])
    YhkYt[3:4] = ((dfu[6:7]^FIe));
  
  assign        Ot[15:18] = ~t[15:18]&Ot[13:14];
  
  always @(&XPo_t[2:6])
    if (XPo_t[2:6])
      IoBcc = R[2:3]|THyO[4:5];
  
  always @(&_DA[3:6])
    if (_DA[3:6])
      XsAtX[6] <= Ot[13:14];
  
  always @(posedge hk[11])
    EkCa[7:21] <= ~Ot[15:18];
  
  assign        j[1] = (FIe != 0) ? (~gBh[0:3]^PgX[3]) : 1'bz;
  
  always @(posedge &t[15:18])
    CHLCL[6:9] = gBh[0:3];
  
  assign        BcHM[2] = BINk[1:2]&Ot[15:18];
  
  always @(&BINk[1:2])
    if (~BINk[1:2])
      usaq = cMStW;
  
  always @(FIe)
    if (~FIe)
      x[9:11] = ~Ot[15:18];
  
  always @(negedge yM_T_[22])
    Fp <= cMStW|X[11:15];
  
  assign        ADnOF = ~BINk[1:2]|gBh[0:3];
  
  always @(&THyO[4:5])
    if (~THyO[4:5])
      EkCa[6:19] = ~FIe;
  
  assign        j[1] = ~Ot[15:18]|EEVv[5:6];
  
  always @(VBkSe[3])
    if (VBkSe[3])
      usaq <= ~x[9:10];
  
  always @(negedge &Mj[19:23])
    CEs[1:3] = (~V[0:2]);
  
  always @(posedge XsAtX[6])
    Fp = ~qXood;
  
  assign        BcHM[2] = ~(~(PgX[3]&YhkYt[3]));
  
  always @(negedge &X[13:15])
    EEVv[3:5] = ~XPo_t[2:6];
  
  always @(hk[11])
    if (~hk[11])
      F[3:6] = x[9:10]&EEVv[3:6];
  
  always @(&Ot[15:18])
    if (~Ot[15:18])
      nxmAS[26:27] = ~THyO[4:5]&Mj[19:23];
  
  always @(&Ot[13:14])
    if (Ot[13:14])
      G[6:8] = ~Cg[0:1]^PgX[3];
  
  always @(posedge &Mj[19:23])
    hk[11:13] = ~_VpZ^FIe;
  
  always @(&XPo_t[2:6])
    if (~XPo_t[2:6])
      EEVv[3:5] <= ~Mj[19:23]&R[2:3];
  
  always @(&_DA[3:6])
    if (_DA[3:6])
      NtFne[1:2] = ~PgX[3];
  
  assign        Ot[15:16] = ~qXood;
  
  assign        R[2:4] = (Ot[15:18]);
  
  always @(&EEVv[5:6])
    if (~EEVv[5:6])
      Q[99:124] = XPo_t[2:6]^EEVv[3:6];
  
  always @(negedge &EEVv[3:6])
    Ojda[0] = ~R[2:3]^Ot[15:18];
  
  always @(negedge &THyO[4:5])
    EkCa[15:22] = ~EEVv[3:6]^Ot[13:14];
  
  always @(negedge &gBh[0:3])
    yM_T_[24:26] = Ot[15:16]^F[1:5];
  
  always @(posedge &X[11:15])
    MIVP[97:98] = dfu[6:7];
  
  always @(&F[1:5])
    if (F[1:5])
      kYiJ <= qXood;
  
  always @(&EEVv[3:6])
    if (~EEVv[3:6])
      gzUQm = _DA[3:6];
  
  always @(negedge &Ot[15:18])
    PbXQK = ~EEVv[3:6]&gBh[0:3];
  
  assign        Ot[15:18] = ~Mj[19:23];
  
  always @(qXood)
    if (~qXood)
      z <= EEVv[5:6]^IoBcc;
  
  always @(&Mj[19:23])
    if (~Mj[19:23])
      HHqNO = ~XPo_t[2:6]^_DA[3:6];
  
  always @(posedge &Ot[15:16])
    t[6:22] <= ~(~(~(~F[1:5]^R[2:3])));
  
  always @(negedge UAp[10])
    fSjmy[14:15] = Cg[0:1]&V[0:2];
  
  always @(&X[11:15])
    if (X[11:15])
      Tslj[2:5] = gBh[0:3]^BINk[1:2];
  
  assign        FIe = _DA[3:6];
  
  always @(&_DA[3:6])
    if (_DA[3:6])
      Kb[3:13] = ~_DA[3:6];
  
  always @(&R[2:3])
    if (~R[2:3])
      Oijbm[0:1] = ~X[11:15];
  
  always @(&X[11:15])
    if (~X[11:15])
      usaq = ~(~X[13:15]);
  
  always @(&X[13:15])
    if (~X[13:15])
      Tslj[2:5] = (~yM_T_[22]&hk[11]);
  
  always @(&EEVv[3:6])
    if (EEVv[3:6])
      EEVv[3:5] <= (~Ot[13:14]&t[15:18]);
  
  always @(posedge &F[1:5])
    UAp[13:15] <= ~_DA[3:6];
  
  assign        j[1] = R[2:3];
  
  always @(_VpZ)
    if (_VpZ)
      d[2:4] = ~PgX[3]|EEVv[5:6];
  
  assign        mXAW[93:98] = (Ot[15:18] != 0) ? PgX[3]&THyO[4:5] : 6'bz;
  
  always @(&dfu[6:7])
    if (~dfu[6:7])
      kYiJ = ~Ot[15:18]|Ot[15:18];
  
  always @(&Ot[15:18])
    if (Ot[15:18])
      z = ~VBkSe[3];
  
  always @(posedge &_DA[3:6])
    SbOzNu = EEVv[3:6];
  
  always @(negedge UAp[10])
    Kb[18:19] <= ~(~_DA[3:6]);
  
  always @(&t[15:18])
    if (t[15:18])
      CEs[1:3] = ~_VpZ^cMStW;
  
  assign        mXAW[95:97] = (x[9:10] != 0) ? ~_DA[3:6]&Ot[15:18] : 3'bz;
  
  assign        mXAW[93:98] = (_DA[3:6] != 0) ? ~X[11:15]^IoBcc : 6'bz;
  
  always @(posedge &Mj[19:23])
    NtFne[1:2] = ~YhkYt[3]^_DA[3:6];
  
  always @(negedge qXood)
    hk[11:13] = ~UAp[10]|R[2:3];
  
  always @(negedge &EEVv[5:6])
    ei = EEVv[5:6]|THyO[4:5];
  
  always @(posedge _VpZ)
    EkCa[7:21] = Cg[0:1];
  
  always @(&EEVv[3:6])
    if (~EEVv[3:6])
      EEVv[4] = gBh[0:3]&gBh[0:3];
  
  always @(posedge &x[9:10])
    Ub[12:31] = ~BINk[1:2]^XPo_t[2:6];
  
  always @(FIe)
    if (~FIe)
      XsAtX[6] = IoBcc&Mj[19:23];
  
  always @(&Ot[13:14])
    if (Ot[13:14])
      EEVv[6:7] <= ~X[11:15];
  
  always @(negedge &x[9:10])
    pLG = (Mj[19:23]);
  
  always @(posedge YhkYt[3])
    F[3:6] = (X[11:15]);
  
  assign        Ot[15:16] = (~Ot[15:16]&UAp[10]);
  
  always @(&x[9:10])
    if (~x[9:10])
      MIVP[93] <= ~gBh[0:3]&XsAtX[6];
  
  always @(&BINk[1:2])
    if (~BINk[1:2])
      dfu[3:5] = ~PgX[3];
  
  always @(posedge PgX[3])
    c[1:2] <= dfu[6:7]^EEVv[3:6];
  
  always @(negedge &Ot[15:16])
    G[6:8] = ~yM_T_[22];
  
  always @(negedge &Ot[15:18])
    NtFne[1:2] = ~PgX[3];
  
  assign        MMA[14:20] = YhkYt[3]^THyO[4:5];
  
  assign        TSKgA = ~F[1:5];
  
  always @(posedge yM_T_[22])
    Fp = qXood&BINk[1:2];
  
  assign        e[12:14] = IoBcc^Ot[15:18];
  
  assign        TSKgA = qXood|x[9:10];
  
  always @(negedge UAp[10])
    Q[99:124] <= PgX[3];
  
  always @(&Ot[13:14])
    if (Ot[13:14])
      kYiJ = ~yM_T_[22];
  
  assign        MMA[15:19] = ~XPo_t[2:6];
  
  assign        BINk[2:4] = (Ot[15:18] != 0) ? ~t[15:18]^Ot[15:18] : 3'bz;
  
  assign        TSKgA = ~hk[11]^Ot[13:14];
  
  always @(&Mj[19:23])
    if (Mj[19:23])
      t[5:11] = ~((X[11:15]^Ot[13:14]));
  
  always @(&Mj[19:23])
    if (Mj[19:23])
      Mj[17:25] = X[11:15]|hk[11];
  
  assign        Ot[13:18] = ~EEVv[3:6]^EEVv[5:6];
  
  always @(posedge &Ot[15:18])
    Fp = yM_T_[22]^_DA[3:6];
  
  always @(VBkSe[3])
    if (VBkSe[3])
      NtFne[1:2] <= _VpZ;
  
  assign        e[12:14] = IoBcc^EEVv[3:6];
  
  always @(posedge VBkSe[3])
    CFxf = ~(Ot[13:14]&R[2:3]);
  
  assign        XPo_t[3:5] = ~(~(X[11:15]));
  
endmodule
