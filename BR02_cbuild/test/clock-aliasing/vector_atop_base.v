// test packaging up a clock in a vector in a scan chain element
// and cascading a few of them  (see vector_ati3 for simpler version of this
// test) (also see vector_atop_{r,c}.v for other simpler versions of this)


module top(clks_a, in1, in2, in3, in4, in5, out1, out2, out3, out4, out5, hold);
  input [1:0] clks_a;
  input in1, in2, in3, in4, in5, hold;
  output out1, out2, out3, out4, out5;

  wire [1:0] ctl;
  wire [1:0] relay;

  wire       clk = clks_a[1];
  wire       rst = clks_a[0];
  assign     relay[0] = rst;
  assign     relay[1] = clk;

  wire [1:0] ctl2, ctl3, ctl4, ctl5, ctl6;

  sub u1({1'b0, relay}, in1, out1, ctl2, hold);
  sub u2({1'b0, ctl2}, in2, out2, ctl3, hold);
  sub u3({1'b0, ctl3}, in3, out3, ctl4, hold);
  sub u4({1'b0, ctl3}, in4, out4, ctl5, hold);
  sub u5({1'b0, ctl3}, in5, out5, ctl6, hold);
endmodule // top

module sub(ctl, in, out, ctlout, hold);
  input [2:0] ctl;
  input       in, hold;
  output      out;
  output [1:0] ctlout;
  reg         out;

  buf(clk, ctl[1] & hold);
  //wire        clk = ctl[1];
  buf #(2) (rst, ctl[0]);

  always @(posedge clk or posedge rst) begin
`ifdef MTI                      // deglitch zero-delay event ordering
    #1 if (clk || rst) begin
`endif
      if (rst)
        out <= 0;
      else
        out <= in;
`ifdef MTI
    end
`endif
  end

  assign      ctlout[1] = clk;
  assign      ctlout[0] = rst;
endmodule // sub
