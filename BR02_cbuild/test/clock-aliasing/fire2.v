// From sun fire design.
// Run with -noFlatten

module top(clk, q, d);
   input clk;
   input [3:0] d;
   output [3:0] q;
   sub1 sub1 ( .clk_a(clk), .clk_b(clk), .clk_c(clk), .clk_d(clk), .q(q), .d(d) );
endmodule

module sub1(clk_a, clk_b, clk_c, clk_d, q, d);
   input clk_a, clk_b, clk_c, clk_d;
   output [3:0] q;
   input [3:0]  d;
   wire [3:0]   clkvec;

   wire [3:0]   lower_temp;
   assign lower_temp[0] = clk_d;
   assign lower_temp[1] = clk_c;
   assign lower_temp[2] = clk_b;
   assign lower_temp[3] = clk_a;
   clkwrap clkwrap ( .clkvecin(lower_temp), .clkvecout(clkvec) );

   sub2 sub2 ( .clkvec(clkvec), .q(q), .d(d) );
endmodule

module clkwrap ( clkvecin, clkvecout );
   input [3:0] clkvecin;
   output [3:0] clkvecout;
   assign       clkvecout = clkvecin;
endmodule

module sub2(clkvec, q, d);
   input [3:0] clkvec;
   output [3:0] q;
   input [3:0]  d;
   wire         clk0, clk1, clk2, clk3;

   sub3 sub3 (clkvec[1:0], q[1:0], d[1:0]);
   sub3 sub3_1 (clkvec[3:2], q[3:2], d[3:2]);
endmodule

module sub3(clkvec, q, d);
   input [1:0] clkvec;
   output [1:0] q;
   input [1:0]  d;
   flop flop0 (clkvec[0], q[0], d[0]);
   flop flop1 (clkvec[1], q[1], d[1]);
endmodule

module flop(clk, q, d);
   input clk;
   output q;
   input  d;
   reg    q;
   always @(posedge clk)
     q <= d;
endmodule
