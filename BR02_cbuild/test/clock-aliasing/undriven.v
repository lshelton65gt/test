module foo(i1, i2, e1, e2, out);
  input i1, i2, e1, e2;
  inout out;

  wire  u1, u2;
  assign out = e1? i1: 1'bz;
  assign out = e2? i2: 1'bz;
  assign out = u1? u2: 1'bz;
endmodule
