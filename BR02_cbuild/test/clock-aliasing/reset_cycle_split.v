module bug(clk,in,in2,out,rst,oscillation_blocker);
  input clk,in,in2,rst;
  output out, oscillation_blocker;
  reg q1,q1p,q2,q2p,reset,q3p,out;

  initial begin
    q1 = 0;
    q1p = 0;
    q2 = 1;
    q2p = 0;
    reset = 0;
    q3p = 0;
    out = 0;
  end

  wire anded_rst = reset & rst;
  assign oscillation_blocker = q3p & ~q2p & q1p;

  always @(posedge clk)
    q1p <= in & anded_rst;
  always @(anded_rst or q1p)
    q1 = q1p & anded_rst;

  always @(posedge clk)
    q2p <= ~q1;
  always @(q1 or q2p)
    q2 = q2p | ~q1;

  always @(posedge clk)
    q3p <= q2;
  always @(q2 or q3p or oscillation_blocker)
    #4 reset = q2 & q3p & ~oscillation_blocker;

  always @(posedge clk or negedge reset)
    if (~reset)
      out <= 0;
    else
      out <= in2;
endmodule
