module top(c1, i1, i2, q1, q2);
  input c1, i1, i2;
  output q1, q2;

  reg    clk1, clk2, o1, o2, q1, q2;
/*
  initial begin
    clk1 = 0;
    clk2 = 0;
    o1 = 0;
    o2 = 0;
    q1 = 0;
    q2 = 0;
    clka = 0;
    clkb = 0;
  end
*/

  // clk1 & clk2 will be aliased, clk1 will be the master due to
  // alphabetic compare
  always @(posedge c1)
    clk1 <= ~clk1;
  always @(posedge c1)
    clk2 <= ~clk2;

  // clka will be the master, but it's derived off clk2, so
  // clk1 will be a dead master with a live alias
  wire clka = ~clk2;
  wire clkb = ~clk1;

  always @(posedge c1 or posedge clka)
    if (clka)
      q1 <= 0;
    else
      q1 <= i1;
  always @(posedge c1 or posedge clkb)
    if (clkb)
      q2 <= 0;
    else
      q2 <= i2;
endmodule
