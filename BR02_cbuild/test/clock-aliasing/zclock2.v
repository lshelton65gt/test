// Try to minimally reproduce an issue seen in ati-new/io while
// working on the new allocator.  A buffer driving a primary bidirect
// also drives a clock-net that gets aliased to a primary input.
// That aliasing process causes the primary-z flag to propagate
// to the primary clock, but that's actually not needed.  This
// has ill effects in ati-new/io, due to an input spontaneously
// becoming a bidirect.
//
// Now this case compiles without errors even if primary-z gets
// propagated to input 'clk', but ati-new/io doesn't.  Don't know
// why that is, but at least this regression can prove that
// we don't let the primary-z flag propagate via a clock alias.

module top(clk, i1, i2, i3, o1, o2, o3);
`ifdef CARBON
  inout o1;
`else
  output o1;
`endif
  input clk, i1, i2, i3;
  output o2, o3;

  reg    o2, o3;
  initial begin
    o2 = 0;
    o3 = 0;
  end

`ifdef CARBON
  assign o1 = i2? i1: 1'bz;
`endif
  clkbuf b1(i1, o1); // this makes clkbuf.out be primary-z

  clkbuf b2(clk, buffered_clk);// this makes buffered_clk primary-z

  always @(posedge clk)
    o2 <= i2;
  always @(posedge buffered_clk)
    o3 <= i3;
endmodule

module clkbuf(in, out);
  input in;
  output out;

  assign out = in;
endmodule
