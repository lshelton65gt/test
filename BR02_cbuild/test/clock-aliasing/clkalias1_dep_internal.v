// file: clkalias1_dep_internal.v
// general topic: testing related to interaction of clock aliasing and force/deposit directives
// specific topic: a design that has equivalent clocks should not equiv them
//                 when there is a directive to make one of the internal clocks
//                 depositable
// this case should be run with the directive depositSignal top.clk2a

// expected results: top.clk2a and top.clk2b will not be equiv
// expected results: top.clk and top.clk2 will be aliased


// note that currently it is nearly impossible to generate a gold simulation
// file using an alternate verilog simulatior such as aldec or fintronic because
// the testdriver generation code does not know how to disconnect the in-design
// driver for the depositable clock, and it does not know that it should only
// deposit to the derived clock on negedge of the the clock that generates the
// derived clock (cloutier)

// addendum: I fixed a bug with depositable clocks which changed the
// gold files for this test case. The gold files still don't match the
// aldec results because of differences in how we handle deposits. But 
// I compared the VCD data before and after my change and the new VCD file
// looks better. But since this was a manual process, I could have missed
// something. (rsayde)

`include "clkalias1_base.v"
