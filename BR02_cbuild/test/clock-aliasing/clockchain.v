module top(out, in, clk);
  output out;
  input  in, clk;
  reg    out;

  not(c1, clk);
  not(c2, c1);
  not(c3, c2);
  not(c4, c3);

  always @(posedge c4)
    out <= in;
endmodule // top
