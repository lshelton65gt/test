/* Copyright (c) 1997 Interra, Inc.
   Use, disclosure or distribution is prohibited 
   without prior written permission of Interra Inc.
*/
// bit select on a vector clk
// Section 18 of the test plan

module MISC181(in1,in2,clk,out1);
input in1,in2;
input [1:0] clk;
output out1;
reg out1;
  initial out1 = 0;

always @(posedge clk[0])
begin
 out1 <= in1 & in2;
end

endmodule
