// In this test, clk1 and clk2 are derived from a common input
// through a construct that clock equivalence cannot handle.
// We prefer this to making two separate inputs so that Verilog XL
// gets the same answer.

module clkdiv(in, in2, in3, out, dclk1_, dclk2_, clk);
  input in, in2, in3, clk;
  output out, dclk1_, dclk2_;

  reg    q1, q2, q3, q4, out, out2, out3;
  reg    dclk1, dclk2;
  reg    clk1, clk2;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
    out = 0;
    dclk1 = 0;
    dclk2 = 0;
    out2 = 0;
    out3 = 0;
    clk1 = 0;
    clk2 = 0;
  end

  // This rather contrived buffer cannot currently be traversed
  // by clock aliasing because it has an if statement.  Hopefully
  // we will nail this someday.
  always @(clk) begin
    if (clk) begin
      clk1 = 1;
      clk2 = 1;
    end
    else begin
      clk1 = 0;
      clk2 = 0;
    end
  end

  assign dclk1_ = ~dclk1;
  assign dclk2_ = ~dclk2;

  always @(posedge clk1)
    dclk1 <= dclk1_;

  always @(posedge clk2)
    dclk2 <= dclk2_;

  always @(posedge dclk1)
    q1 <= in;

  always @(posedge dclk2)
    q2 <= q1;

  always @(posedge dclk1)
    q3 <= q2;

  always @(posedge dclk2)
    q4 <= q3;

  always @(posedge dclk1)
    out <= q4;
endmodule // clkdiv

