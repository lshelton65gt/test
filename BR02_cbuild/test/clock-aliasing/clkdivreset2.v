module clkdiv(in, out, reset, clk);
  input in, clk, reset;
  output out;

  reg    q1, q2, q3, q4, out;
  reg    clk2a, clk2b, clk4a, clk4b;
  wire    clk1, clk2, clk_;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
    out = 0;
    clk2a = 0;
    clk2b = 0;
    clk4a = 0;
    clk4b = 0;
  end // initial begin

  assign clk1 = clk;
  assign clk_ = ~clk1;
  assign clk2 = ~clk_;

  always @(posedge clk)
    clk2a <= ~(clk2a | reset);

  always @(posedge clk2)
    clk2b <= ~(reset | clk2b);

  always @(posedge clk2a)
    clk4a <= ~(reset | clk4a);

  always @(posedge clk2b)
    clk4b <= ~(reset | clk4b);

  always @(posedge clk4a)
    q1 <= in;

  always @(posedge clk4b)
    q2 <= q1;

  always @(posedge clk4a)
    q3 <= q2;

  always @(posedge clk4b)
    q4 <= q3;

  always @(posedge clk4a)
    out <= q4;
endmodule // clkdiv

