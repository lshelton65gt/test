module top(clk, ena, i1, i2, i3, o1, o2, o3);
  input clk, ena, i1, i2, i3;
  output o1, o2, o3;

  flop1 u1(clk, ena, i1, o1);
  flop2 u2(clk, ena, i2, o2, i3, o3);
endmodule

module flop1(clk, ena, i, o);
  input clk, ena, i;
  output o;

  wire   clk_ena = clk & ena;
  reg    q;
  initial q = 0;
  always @(posedge clk_ena)
    q <= i;

  assign o = q & clk_ena;       // avoid aliasing clk_ena
endmodule

module flop2(clk, ena, i, o, i3, o3);
  input clk, ena, i, i3;
  output o, o3;

  wire   clk_ena = clk & ena;
  reg    q;
  initial q = 0;
  always @(posedge clk_ena)
    q <= i;

  assign o = q & clk_ena;       // avoid aliasing clk_ena

  flop3 u3(clk_ena, i3, o3);
endmodule

module flop3(clk_ena, i, o);
  input clk_ena, i;
  output o;

  reg    q;
  initial q = 0;
  always @(posedge clk_ena)
    q <= i;

  assign o = q & clk_ena;       // avoid aliasing clk_ena
endmodule
