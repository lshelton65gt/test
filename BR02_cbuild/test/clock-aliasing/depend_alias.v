// This replicates a problem found in customer test-cases while trying to
// make clock aliasing more aggressive.  I find two clocks that are aliased
// together, but the subservient one is needed to compute another derived
// clock, so it can't be aliased -- it needs to survive.
module top(clk, ena, i1, i2, i3, o1, o2, o3);
  input clk, ena, i1, i2, i3;
  output o1, o2, o3;

  reg    o1, clk1;
  initial begin
    o1 = 0;
    clk1 = 0;
  end

  always @(posedge clk)
    clk1 <= ena;

  always @(posedge clk1)
    o1 <= i1;

  alias_and_derive u1(clk, ena, i2, i3, o2, o3);
endmodule

module alias_and_derive(clk, ena, i2, i3, o2, o3);
  input clk, ena, i2, i3;
  output o2, o3;

  reg    clk2, clk3, o2, o3;  // clk2 will get aliased, but is needed for clk3

  initial begin
    clk2 = 0;
    clk3 = 0;
    o2 = 0;
    o3 = 0;
  end

  always @(posedge clk)
    clk2 <= ena;                // will be equivalenced to top.clk1

  always @(posedge clk2)
    o2 <= i2;

  always @(posedge clk)
    clk3 <= clk2;

  always @(posedge clk3)
    o3 <= i3;
endmodule
