// This testcase comes from bug1705.  What happens is that a pulldown
// driver is automatically synthesized for top.ds, and the code in
// ClkAnalVector.cxx that traverses through bit-selects to find
// assignments find that, but misses the enabled driver, because
// enabled drivers are not assignments, and that code is only looking
// for assignments.
//
// To run this example correctly, the code must invalidate its assignment
// traversal in the presence of a driver it does not understand.  This
// testcase was moved to the clock-aliasing bucket to help maximize
// the chance that regressing clock equivalance correctness would be caught
// in one test.


module top(clk, in1, in2, en1, en2, out1, out2);
   input clk;
   input [1:0] in1;
   input [1:0] in2;
   input en1, en2;
   output [1:0] out1;
   output [1:0] out2;

   wire [1:0] 	d;
   wire [1:0] 	ds;
   
   bug1 bug1a(.clk(clk), .in(in1), .en(en1), .out(out1), .d(d), .ds(ds));

   bug2 bug2a(.clk(clk), .in(in2), .en(en2), .out(out2), .d(d), .ds(ds));
   
endmodule // top

module bug1(clk, in, en, out, d, ds);
   input clk;
   input [1:0] in;
   input       en;
   output [1:0] out;
   inout [1:0] 	d;
   inout [1:0] ds;

   wire   [1:0] d_in, ds_in, en_d, in_d;

   bug1slice slice0(.clk(clk), .ds(ds_in[0]), .in(in[0]), .in_d(in_d[0]), .en(en), .en_d(en_d[0]), .d_in(d_in[0]), .out(out[0]));
   bug1slice slice1(.clk(clk), .ds(ds_in[1]), .in(in[1]), .in_d(in_d[1]), .en(en), .en_d(en_d[1]), .d_in(d_in[1]), .out(out[1]));
   
   assign d[0] = en_d[0] ? in_d[0] : 1'bz;
   assign d[1] = en_d[1] ? in_d[1] : 1'bz;
   assign ds[0] = en_d[0] ? clk : 1'bz;
   assign ds[1] = en_d[1] ? clk : 1'bz;
   assign d_in = d;
   assign ds_in = ds;

endmodule // bug

module bug1slice(clk, ds, in, in_d, en, en_d, d_in, out);
   input clk, ds, in, en, d_in;
   output in_d, en_d, out;
   reg in_d, en_d, out;

   initial begin
     in_d = 0;
     en_d = 0;
     out = 0;
   end

   always @(posedge clk) begin
      in_d <= in;
      en_d <= en;
   end

   always @(posedge ds)
     if (~en)
       out <= d_in;
endmodule
   

module bug2(clk, in, en, out, d, ds);
   input clk;
   input [1:0] in;
   input       en;
   output [1:0] out;
   inout [1:0] 	d;
   inout [1:0] ds;

   wire   d_en, ds_en, ds_out, ds_in;
   wire   [1:0] d_out, d_in;

   cmodel cmodel(.clk(clk),
		 .in(in),
		 .en(en),
		 .out(out),
		 .d_in(d_in),
		 .ds_in(ds_in),
		 .d_out(d_out),
		 .ds_out(ds_out),
		 .d_en(d_en),
		 .ds_en(ds_en));
   
   assign d = d_en ? d_out : 2'bz;
   assign ds = ds_en ? {2{ds_out}} : 2'bz;
   assign d_in = d;
   assign ds_in = ds[0];
   
endmodule // bug

module cmodel(clk,
	      in,
	      en,
	      out,
	      d_in,
	      ds_in,
	      d_out,
	      ds_out,
	      d_en,
	      ds_en);
   input clk, en, ds_in;
   input [1:0] in, d_in;
   output [1:0] out, d_out;
   output ds_out, d_en, ds_en;

endmodule // cmodel
