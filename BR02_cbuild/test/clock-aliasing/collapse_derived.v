module clkbuf(in1, in2, out1, out2, clk1, clk2);
  input in1, in2, clk1, clk2;
  output out1, out2;
  reg    dclk1, dclk2, ddclk1, ddclk2, out1, out2;

  initial begin
    dclk1 = 0;
    dclk2 = 0;
    ddclk1 = 0;
    ddclk2 = 0;
    out1 = 0;
    out2 = 0;
  end

  // First level of derived clocks
  always @(posedge clk1)
    dclk1 <= ~dclk1;
  always @(posedge clk2)
    dclk2 <= ~dclk2;

  // Second level of derived clocks
  always @(posedge dclk1)
    ddclk1 <= ~ddclk1;
  always @(posedge dclk2)
    ddclk2 <= ~ddclk2;

  // ddclk1 and ddclk2 will be collapsed together via a directive,
  // making the flop that drives dclk2, which is an alias master,
  // be unreachable

  always @(posedge ddclk1)
    out1 <= in1;
  always @(posedge ddclk2)
    out2 <= in2;
endmodule // clkbuf

