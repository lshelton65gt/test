module top(cin, hin, out, rst, clk, cld, hld, rcnt, rsto);
   input [31:0] cin, hin;
   input 	cld, hld, clk, rst;
   output [31:0] out;
   output [3:0]  rcnt;
   output 	 rsto;
   
   wire [31:0] 	 tmp1, tmp2, tmp3;
   assign 	 rsto = rst;
   
   reg4 reg4_0 (cin, hin, tmp1, rst, clk, cld, hld);
   reg4 reg4_1 (tmp1, cin, tmp2, rst, clk, cld, hld);
   reg4 reg4_2 (tmp2, tmp1, tmp3, rst, clk, cld, hld);
   reg4 reg4_3 (tmp3, tmp2, out, rst, clk, cld, hld);

   rcounter rcounter (rcnt, clk, rst);
   
endmodule

module rcounter (rcnt, clk, rst);
   input clk, rst;
   output [3:0] rcnt;
   reg [3:0] 	rcnt;

  initial  rcnt = 0;

   always @(posedge clk)
     if (rst)
       rcnt <= rcnt + 1;
	     
endmodule

module reg4(cin, hin, out, rst, clk, cld, hld);
   input [31:0] cin, hin;
   input 	cld, hld, clk, rst;
   output [31:0] out;

   reg [31:0] 	 out, tmp1, tmp2, tmp3;

  initial begin
    out = 0;
    tmp1 = 0;
    tmp2 = 0;
    tmp3 = 0;
  end

   always @(posedge clk)
     begin
	if (rst)
	  tmp1 <= 1'b0;
	else if (cld)
	  begin
	     tmp1 <= cin;
	  end
	else if (hld)
	  tmp1 <= hin;
     end

   always @(posedge clk)
     begin
	if (rst)
	  tmp2 <= 1'b1;
	else if (cld)
	  begin
	     tmp2 <= ((~tmp1) & (~{32{hld}}) & tmp2) |
		     ((~tmp1) & ({32{hld}}) & cin);
	  end
	else if (hld)
	  tmp2 <= cin;
     end
   always @(posedge clk)
     begin
	if (rst)
	  tmp3 <= 1'b1;
	else if (cld)
	  begin
	     tmp3 <= (tmp2 | 
		      (({32{hld}}) & tmp1) |
		      ((~{32{hld}}) & tmp3));
	  end
	else if (hld)
	  tmp3 <= tmp1;
     end
   always @(posedge clk)
     begin
	if (rst)
	  tmp1 <= 1'b0;
	else if (cld)
	  begin
	     out[10:0] <= tmp3;
	     out[21:11] <= ((~tmp3[21:11]) & (~{11{hld}}) & out[21:11]) |
			   ((~tmp3[21:11]) & ({11{hld}}) & tmp2[21:11]);
	     out[31:22] <= (tmp3[31:22] | 
			    (({32{hld}}) & tmp2[31:22]) |
			    ((~{32{hld}}) & out[31:22]));
	  end
	else if (hld)
	  out <= tmp2;
     end
endmodule

