// file: clkalias1.v
// general topic: testing related to interaction of clock aliasing and force/deposit directives
// specific topic: this is the baseline testcase of the clkalias1* series,
//     it is included to make sure that aliasing is working with no directives.
// expected results: top.clk2a and top.clk2b will be equiv
// expected results: top.clk and top.clk2 will be aliased



`include "clkalias1_base.v"
