module top(ctl_in, hold, in0, in1, out0, out1);
  input [1:0] ctl_in;
  input hold;
  input in0;
  input in1;
  output out0;
  output out1;

  // Local nets (7)
  wire         [1:0]          ctl0;
  wire         [1:0]          ctl1;
  wire         [1:0]          ctl2;
  /*temp*/ reg [2:0]          lower_1;
  /*temp*/ wire [2:0]          lower_2;

  // Continuous Assigns
  always lower_1[2] = 1'b0;  // bug2555.v:55
  assign lower_2[1] = ctl1[1];
  assign lower_2[0] = ctl1[0];
  assign lower_2[2] = 1'b0;  // bug2555.v:56

  // Always Blocks
  always @(ctl_in)
    // $block_bug2555_v_L55;5
    begin :named                                         // bug2555.v:55
      // Declarations
      reg [1:0] rescope_1;
      // Statements
      rescope_1[1] = ctl_in[1];              // bug2555.v:51
      rescope_1[0] = ctl_in[0];              // bug2555.v:52
      lower_1[0] = rescope_1[1];
      lower_1[1] = rescope_1[0];
    end

  // Instances
  sub                                              // bug2555.v:59
    s0(                                            // bug2555.v:55
      .ctl(lower_1),         // input
      .in(in0),                                    // input
      .out(out0),                                  // output
      .ctlout(ctl1),                               // output
      .hold(hold));                                // input
  sub                                              // bug2555.v:59
    s1(                                            // bug2555.v:56
      .ctl(lower_2),         // input
      .in(in1),                                    // input
      .out(out1),                                  // output
      .ctlout(ctl2),                               // output
      .hold(hold));                                // input
endmodule // top

module sub(ctl, in, out, ctlout, hold);
  input [2:0] ctl;
  input in;
  output out;
  output [1:0] ctlout;
  input hold;

  // Output regs (2)
  reg                         out;
  reg          [1:0]          ctlout;

  // Local nets (2)
  wire                        clk;
  wire                        rst;



  // Continuous Assigns
  assign clk = (ctl[1] & hold);                    // bug2555.v:66
  assign rst = ctl[0];                             // bug2555.v:67

  // Always Blocks
  always @(posedge rst or posedge clk)
    if (rst)
      out <= 1'b0;                                  // bug2555.v:73
    else
      out <= in;                                    // bug2555.v:74

  always @(ctl or hold)
    // $block_bug2555_v_L78;1
    begin                                          // bug2555.v:78
      // Declarations
      // Statements
      ctlout[0] = ctl[0];                          // bug2555.v:83
      ctlout[1] = (ctl[1] & hold);                 // bug2555.v:84
    end
endmodule // sub
