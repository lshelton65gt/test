// test packaging up a clock in a vector in a scan chain element
// and cascading a few of them

module top(clks_a, in1, in2, in3, in4, in5, out1, out2, out3, out4, out5);
  input [1:0] clks_a;
  input in1, in2, in3, in4, in5;
  output out1, out2, out3, out4, out5;

  wire [1:0] ctl;
  wire [1:0] relay;

  wire       clk = clks_a[1];
  wire       rst = clks_a[0];
  assign     relay[0] = rst;
  assign     relay[1] = clk;

  wire [1:0] ctl2, ctl3, ctl4, ctl5, ctl6;

  sub u1({1'b0, relay}, in1, out1, ctl2);
  sub u2({1'b0, ctl2}, in2, out2, ctl3);
  sub u3({1'b0, ctl3}, in3, out3, ctl4);
  sub u4({1'b0, ctl3}, in4, out4, ctl5);
  sub u5({1'b0, ctl3}, in5, out5, ctl6);
endmodule // top

/*
module mid(ctl, in, out);
  input [2:0] ctl;
  input       in;
  output      out;

  sub sub(ctl[1:0], in, out);
endmodule
*/

module sub(ctl, in, out, ctlout);
  input [2:0] ctl;
  input       in;
  output      out;
  output [1:0] ctlout;
  reg         out;

  buf(clk, ctl[1]);
  //wire        clk = ctl[1];
  buf #(1) (rst, ctl[0]);

  always @(posedge clk or posedge rst)
    if (rst)
      out <= 0;
    else
      out <= in;

  assign      ctlout[1] = clk;
  assign      ctlout[0] = rst;
endmodule // sub
