// This is a copy of bug 1050, put in the clock analysis bucket.
// It must be run with -g or -noVectorInferencing.  See module mux
// for what we are really trying to do here.

module bug (ireset, iin, out, iclk, fclk);
   input iclk, fclk;
   input [3:0] ireset, iin;
   output [3:0] out;
   wire [0:3] 	reset_a,  reset_b;
   reg          clk;
   reg [3:0]     reset, in;

   initial begin
     clk = 0;
     reset = 0;
     in = 0;
   end

   always @(posedge fclk)
     clk <= iclk;
   always @(negedge fclk) begin
     reset <= ireset;
     in <= iin;
   end

   core core (reset_a, reset);
   
   assign reset_b[0] = reset_a[0];
   assign reset_b[1] = reset_a[1];
   assign reset_b[2] = reset_a[2];
   assign reset_b[3] = reset_a[3];

   ser ser0 (reset_b[0], in[0], out[0], clk);
   ser ser1 (reset_b[1], in[1], out[1], clk);
   ser ser2 (reset_b[2], in[2], out[2], clk);
   ser ser3 (reset_b[3], in[3], out[3], clk);
   
endmodule // bug

module core (reset_a, reset);
   input [3:0] reset;
   output [0:3] reset_a;
   wire [ 4:1]   reset_cat;
   wire 	 reset_s3, reset_s2, reset_s1, reset_s0;

   mux mux (reset_a, reset_cat);

   assign 	 reset_cat = ({reset_s3, reset_s2, reset_s1, reset_s0});

   slice slice0 (reset_s3, reset[0]);
   slice slice1 (reset_s2, reset[1]);
   slice slice2 (reset_s1, reset[2]);
   slice slice3 (reset_s0, reset[3]);
endmodule

module slice (out,in);
   input in;
   output out;
   assign out = in;
endmodule // slice

module mux (reset_a, reset_cat);
   input [3:0] reset_cat;
   output [0:3] reset_a;
   wire [6:3] 	 reset_muxout;

   // This assignment causes a change of indices when expression
   // synthesis for reset_a translates reset_muxout to reset_cat,
   // triggering a hard-to-get-to clause in ClkAnalVector.cxx.
   assign reset_muxout    = reset_cat;
   assign reset_a      = ({reset_muxout[3], reset_muxout[4],
                           reset_muxout[5], reset_muxout[6]});

endmodule
   
module ser(reset, in, out, clk);
   input reset, in, clk;
   output out;
   reg 	  out;

   reg     rst_delay;
   initial rst_delay = 1'b0;
   always @(reset)
     #1 rst_delay = reset;

   always @(posedge clk or posedge rst_delay)
     if (rst_delay)
       out <= 1'b1;
     else
       out <= in;
   
endmodule
