// This testcase was provided by Joe 7/18/05, based on some intermediate 
// output of concat-rewrite hacking he's doing for Cisco.  For reasons
// I don't have time to explore right now, both Aldec & Finsim hang
// on this example.  I'm guessing there is some sort of unstable combinational
// cycle, but none is obvious to me.  Don't believe the .vlog.log.gold file -- 
// it comes from cbuild
module top(clks_a, in1, in2, in3, in4, in5, out1, out2, out3, out4, out5, 
    hold);
  input [1:0] clks_a;
  input in1;
  input in2;
  input in3;
  input in4;
  input in5;
  output out1;
  output out2;
  output out3;
  output out4;
  output out5;
  input hold;

  // Local nets (19)
  wire         [1:0]          ctl;
  wire         [1:0]          relay;
  wire                        clk;
  wire                        rst;
  wire         [1:0]          ctl2;
  wire         [1:0]          ctl3;
  wire         [1:0]          ctl4;
  wire         [1:0]          ctl5;
  wire         [1:0]          ctl6;
  /*temp*/ reg [2:0]          tmp_relay;
  /*temp*/ wire [2:0]          tmp_lower_1;
  /*temp*/ wire [2:0]          tmp_lower_2;
  /*temp*/ wire [2:0]          tmp_lower_3;
  /*temp*/ wire [2:0]          tmp_lower_4;

  // Continuous Assigns
  always tmp_relay[2] = 1'b0; // vector_atop_base.v:21
  assign tmp_lower_1[1:0] = ctl2; // vector_atop_base.v:22
  assign tmp_lower_1[2] = 1'b0;  // vector_atop_base.v:22
  assign tmp_lower_2[1:0] = ctl3; // vector_atop_base.v:23
  assign tmp_lower_2[2] = 1'b0;  // vector_atop_base.v:23
  assign tmp_lower_3[1:0] = ctl3; // vector_atop_base.v:24
  assign tmp_lower_3[2] = 1'b0;  // vector_atop_base.v:24
  assign tmp_lower_4[1:0] = ctl3; // vector_atop_base.v:25
  assign tmp_lower_4[2] = 1'b0;  // vector_atop_base.v:25

  // Always Blocks
  always @(clks_a)
    // $block_vector_atop_base_v_L21;5
    begin                                          // vector_atop_base.v:21
      // Declarations
      // Statements
      tmp_relay[1:0] = clks_a; // vector_atop_base.v:21
    end

  // Instances
  sub                                              // vector_atop_base.v:28
    u1(                                            // vector_atop_base.v:21
      .ctl(tmp_relay),        // input
      .in(in1),                                    // input
      .out(out1),                                  // output
      .ctlout(ctl2),                               // output
      .hold(hold));                                // input
  sub                                              // vector_atop_base.v:28
    u2(                                            // vector_atop_base.v:22
      .ctl(tmp_lower_1),         // input
      .in(in2),                                    // input
      .out(out2),                                  // output
      .ctlout(ctl3),                               // output
      .hold(hold));                                // input
  sub                                              // vector_atop_base.v:28
    u3(                                            // vector_atop_base.v:23
      .ctl(tmp_lower_2),         // input
      .in(in3),                                    // input
      .out(out3),                                  // output
      .ctlout(ctl4),                               // output
      .hold(hold));                                // input
  sub                                              // vector_atop_base.v:28
    u4(                                            // vector_atop_base.v:24
      .ctl(tmp_lower_3),         // input
      .in(in4),                                    // input
      .out(out4),                                  // output
      .ctlout(ctl5),                               // output
      .hold(hold));                                // input
  sub                                              // vector_atop_base.v:28
    u5(                                            // vector_atop_base.v:25
      .ctl(tmp_lower_4),         // input
      .in(in5),                                    // input
      .out(out5),                                  // output
      .ctlout(ctl6),                               // output
      .hold(hold));                                // input
endmodule // top


module sub(ctl, in, out, ctlout, hold);
  input [2:0] ctl;
  input in;
  output out;
  output [1:0] ctlout;
  input hold;

  // Output regs (1)
  reg                         out;

  // Local nets (2)
  wire                        clk;
  wire                        rst;



  // Continuous Assigns
  assign ctlout[1] = clk;                          // vector_atop_base.v:52
  assign ctlout[0] = ctl[0];                       // vector_atop_base.v:53
  assign clk = (ctl[1] & hold);                    // vector_atop_base.v:35
  buf #(1) (rst, ctl[0]);                             // vector_atop_base.v:37

  // Always Blocks
  always @(posedge rst or posedge clk)
    if (rst)
      out <= 1'b0;
    else
      out <= in;                                    // vector_atop_base.v:46
endmodule // sub
