module top(out, in, clk, cclk, gate, d, q, d2, q2);
  output out, q, q2;
  input  in, clk, cclk, gate, d, d2;
  reg    out, q2;

  clkgate clkgate(clk_, clk, gate, cclk, d, q);
  wire   clk2 = ~clk_;          // clk2 is canonical clock
  always @(posedge clk2)
    out <= in;

  or(clk3, clk2, gate);

  always @(posedge clk3)
    q2 <= d2;
endmodule // top

module clkgate(clk_, clk, gate, cclk, d, q);
  output clk_, q;
  input  clk, gate, cclk, d;
  reg    clk1, gate1, q;
  
  always @(posedge cclk) begin
    clk1 <= clk;
    gate1 <= gate1;
  end

  or(clk_or_gate, clk1, gate1);
  not(clk_, clk_or_gate);

  always @(posedge clk_or_gate)
    q <= d;
endmodule // clkgate

  