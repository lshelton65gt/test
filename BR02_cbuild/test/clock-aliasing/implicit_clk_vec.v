module foo(clk, in1, in2, in3, in4, out1, out2, out3, out4);
   input [1:0] clk;
   input in1, in2, in3, in4;
   output out1, out2, out3, out4;
   reg 	  out1, out2, out3, out4;

   always @(posedge clk) begin
      out1 <= in1;
   end

   always @(posedge clk) begin
      out2 <= in2;
   end

   always @(posedge clk[1]) begin
      out3 <= in3;
   end

   always @(posedge clk[1]) begin
      out4 <= in4;
   end

endmodule // foo
