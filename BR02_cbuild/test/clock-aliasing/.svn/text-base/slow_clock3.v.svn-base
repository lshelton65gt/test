// What this test is intended to check for?  Is it a test of clock
// equivalence where clk and del_clk should not be made clock aliases?
// Or is it a test of the way that del_clk and rst can race?
// I could not get this test to match aldec even by changing 
//  always @(posedge del_clk or posedge rst)
// to
//  always @(posedge del_clk or rst)

// and I note that this test is different from slow_clock* because there is no
// slowClock directive and it differs from slow_clock in the assignment to rst
// (as a declaration equation instead of a continuous assign (is that a V2k feature?))



`timescale 1ns/1ns

module top(clk, out, in);
  input clk;
  input [31:0] in;
  output [31:0] out;
  reg [31:0]    out;
  reg del_clk;
  reg [3:0]    tmp;

  initial begin
    tmp = 4'b0;
    out = 1'b0;
    del_clk = 1'b0;
  end

  always @(posedge clk)
    tmp <= tmp + 1;

  // Create a predictable async reset that will race with clock
  wire         rst = tmp == 4'b0000;

  // slow down the clock for Aldec
  always @(clk)
    #1 del_clk = clk;

  always @(posedge del_clk or posedge rst)
    if (rst)
      out <= 0;
    else
      out <= in;

  // Clock equivalence should not merge together a slow clock and
  // a normal clock
endmodule
