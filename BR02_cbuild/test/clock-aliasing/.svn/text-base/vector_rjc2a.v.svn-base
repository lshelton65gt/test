// file: vector_rjc2a.v
// created: 10/13/03
// author: cloutier
// General topic: A Clock that is part of a vector at top level is split at
//                lower level, do we alias them? 
// specific for this test: In this test three different instance of sub* are 
//                         used, each selecting a different bit of wide_clk.
//                         In this example the main
//                         clock passes through a register before being used in
//                         sub*.
// desired result: the three clocks within instances of sub* should NOT be aliased

module top(wide_clk, clk1, in1, in2, in3, out1, out2, out3);
   input [7:0] wide_clk;
   input       clk1;
   input       in1, in2, in3;
   output      out1, out2, out3;

   wire [1:0]  ctl;


   reg [7:0] wide_clk_int;
   always @(posedge clk1)
     wide_clk_int <= wide_clk;
   
   

  sub1 u1 (wide_clk_int, in1, out1);
  sub2 u2 (wide_clk_int, in2, out2);
  sub3 u3 (wide_clk_int, in3, out3);
endmodule // top

module sub1(wide_clk, in, out);
  input [7:0] wide_clk;
  input       in;
  output      out;
  reg         out;

  wire        clk = wide_clk[1];

   initial out = 1;
   always @(posedge clk)
     out <= in;
endmodule // sub1

module sub2(wide_clk, in, out);
  input [7:0] wide_clk;
  input       in;
  output      out;
  reg         out;

  wire        clk = wide_clk[2];

   initial out = 1;
   always @(posedge clk)
     out <= in;
endmodule // sub2

module sub3(wide_clk, in, out);
  input [7:0] wide_clk;
  input       in;
  output      out;
  reg         out;

  wire        clk = wide_clk[3];

   initial out = 1;
   always @(posedge clk)
     out <= in;
endmodule // sub3



