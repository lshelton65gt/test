// this testcase from bug2583 demonstrates how we did not find the alias between:
// top.u1.clk and top.u2.clk, and it would mark top.u2.clk as inactive because
// a mistake had been made in tracking the clock back to PIs (a constant 0 was
// found).
// to see this problem you should run with: -noVectorInference -dumpClockAliases

module top(ctl1, in1, in2, out1, out2);
  input [1:0] ctl1;
  input in1,in2;
  output out1,out2;

  wire [1:0] ctl2;
  wire [1:0] ctl3;

  wire [2:0] lowered_sub1_ctl = {1'b1,ctl1};
  wire [2:0] lowered_sub2_ctl = {1'b0,ctl2};

  sub u1( .ctl_in(lowered_sub1_ctl),
          .in(in1), .out(out1),
          .ctl_out(ctl2) );
  sub u2( .ctl_in(lowered_sub2_ctl),
          .in(in2), .out(out2),
          .ctl_out(ctl3) );
endmodule

module sub(ctl_in, in, out, ctl_out);
  input [2:0] ctl_in;
  input in;
  output out;
  output [1:0] ctl_out;

  wire clk = ctl_in[1];
  wire #1 rst = ctl_in[0];	// delay to match verilog simulation

  reg out;
  always @(posedge clk or posedge rst)
  begin
    if (rst)
      out = 0;
    else
      out = in;
  end

  reg [1:0] ctl_out;
  always @(ctl_in)
  begin
//     ctl_out = ctl_in;
    ctl_out[0] = ctl_in[0];
    ctl_out[1] = ctl_in[1];
  end
endmodule
