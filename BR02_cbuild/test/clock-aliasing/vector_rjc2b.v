// file: vector_rjc2b.v
// created: 10/13/03
// author: cloutier
// General topic: A Clock that is part of a vector at top level, then
//                bitselected at the top level and distributed to lower level,
//                do we alias them? 
// specific for this test: In this test three different instance of sub* are used.
//                         The clock is divided and split at the top level,
//                         selecting different bits of the vector
// desired result: the three clocks within instances of sub1 should NOT be aliased

module top(v_clk, clk1, in1, in2, in3, out1, out2, out3);
   input [7:0] v_clk;
   input       clk1;
   input       in1, in2, in3;
   output      out1, out2, out3;

   wire [1:0]  ctl;

   reg  s_clk_int1;
   reg  s_clk_int2;
   reg  s_clk_int3;
   
   always @(posedge clk1)
     s_clk_int1 <= v_clk[0];

   always @(posedge clk1)
     s_clk_int2 <= v_clk[1];

   always @(posedge clk1)
     s_clk_int3 <= v_clk[2];
   
   

  sub1 u1 (s_clk_int1, in1, out1);
  sub2 u2 (s_clk_int2, in2, out2);
  sub3 u3 (s_clk_int3, in3, out3);
endmodule // top

module sub1(clk, in, out);
   input       clk;
   input       in;
   output      out;
   reg         out;

   initial out = 1;
   always @(posedge clk)
     out <= in;
endmodule // sub1

module sub2(clk, in, out);
   input       clk;
   input       in;
   output      out;
   reg         out;

   initial out = 1;
   always @(posedge clk)
     out <= in;
endmodule // sub2

module sub3(clk, in, out);
   input       clk;
   input       in;
   output      out;
   reg         out;

   initial out = 1;
   always @(posedge clk)
     out <= in;
endmodule // sub3

