module clkdiv(in, out, clk);
  input in, clk;
  output out;

  reg    q1, q2, q3, q4, out;
  reg    clk2a, clk2b;
  wire    clk1, clk2, clk_;

  assign clk1 = clk;
  assign clk_ = ~clk1;
  assign clk2 = ~clk_;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
    out = 0;
    clk2a = 0;
    clk2b = 0;
  end

  always @(posedge clk)
    clk2a <= ~clk2a;

  always @(posedge clk2)
    clk2b <= ~clk2b;

  always @(posedge clk2a)
    q1 <= in;

  always @(posedge clk2b)
    q2 <= q1;

  always @(posedge clk2a)
    q3 <= q2;

  always @(posedge clk2b)
    q4 <= q3;

  always @(posedge clk2a)
    out <= q4;
endmodule // clkdiv

