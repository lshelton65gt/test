// file: clkalias1_dep_driver_walias.v
// general topic: testing related to interaction of clock aliasing and force/deposit directives
// specific topic: a design that has equivalent clocks should alias them when
//                 there is a directive to make both the driver and the
//                 internal clock an alias and a deposit directive on the driver
// this case should be run with the directives depositSignal top.clk
//                                             collapseClock top.clk top.clk2

// expected results: top.clk and top.clk2 will be aliased
// expected results: top.clk2a and top.clk2b will be equiv


`include "clkalias1_base.v"
