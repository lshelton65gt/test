// last mod: Tue Feb 14 09:29:20 2006
// filename: test/clock-aliasing/reset_priority.v
// Description:  This test shows that we are too pessimistic during clock
// analysis. Because of the 4th always block shown in comments below the routine
// SCHClkAnal::getPriorityOrder will say that there is no async reset when there
// really is.
// This testcase was inspired by a code review of a change in clock-analysis,
// see bug 5658  or comment in SCHClkAnal::getPriorityOrder for more details.


module reset_priority(clk, d, rst1, rst2, q);
   input clk;
   input  d, rst1, rst2;
   output  q;
   reg  q;


 always @ (posedge clk or negedge rst1 or negedge rst2)
    begin
      if (~rst1)
        q <= 0;
      else if (~rst2)
        q <= 1;
      else
        q <= d;
    end
   
// // In this case we create 4 always blocks which look like:
//
// // priority is negedge rst2 block
// always @ (posedge clk) 
//    q <= d;
//
// // clock block is posedge clk, priority is negedge rst1
// always @ (negedge rst2) 
//    q <= 1;
//
// // clock block is posedge clk, priority is NULL
// always @ (negedge rst1) 
//    q <= 0;
//
// // clock block is posedge clk, priority is NULL
// always @ (posedge rst1) 
//    if (~rst2)
//       q <= 1;
//
endmodule
