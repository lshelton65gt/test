// This testcase mimics an issue found at Sandburst, reported as bug2415.
// We have a complexity of asynchronous-resets in the clock path that,
// for some reason, causes the clock divider code to give up, and not
// explore the logic cone fully.  Alias-clocks then gets over-anxious
// and misses some disqualifying fanout that had no BDDs.

module top(i1, i2, i3, o1, o2, o3, clkout, fclk, sclk, rst,
           i1a, o1a, i2a, o2a, i3a, o3a);
  input i1, i2, i3, fclk, sclk, rst, i1a, i2a, i3a;
  output o1, o2, o3, clkout, o1a, o2a, o3a;

  clkgen cg1(fclk, sclk, clk1, i1, o1, rst, i1a, o1a);
  clkgen cg2(fclk, sclk, clk2, i2, o2, rst, i2a, o2a);
  clkgen cg3(fclk, sclk, clk3, i3, o3, rst, i3a, o3a);

  assign clkout = ~clk2;
endmodule

module clkgen(fclk, sclk, clk, i, o, rst, i2, o2);
  input fclk, sclk, i, rst, i2;
  output clk, o, o2;
  reg    clk_, o, clk, clk__, clk___, clk1, clk2, o2;

  initial begin
    clk_ = 0;
    clk__ = 0;
    clk___ = 0;
    clk = 0;
    o = 0;
    o2 = 0;
    clk1 = 0;
    clk2 = 0;
  end

  always @(posedge fclk)
    clk1 <= sclk;
  always @(posedge fclk)
    clk2 <= clk1;

  not(sclk_, clk2);
  always @(posedge fclk) begin
    if (!rst) begin
      clk <= 0;
      clk_ <= 0;
      clk__ <= 0;
      clk___ <= 0;
    end else begin
      clk_ <= sclk_;
      clk__ <= clk_;
      clk___ <= clk__;
      clk <= clk___;
    end
  end

  always @(posedge clk)
    o <= i;
  always @(posedge clk2)
    o2 <= i2;
endmodule
