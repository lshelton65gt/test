// This is a copy of test/cycle-dumping/test1.v.  It was copied here to validate
// that we handle a tristate driven clock correctly.  'reset' has only one driver
// in the Verilog code, although we will synthesize a pulldown for it.  This
// should not be reported as a multiply driven net (warning 1003).
//
// We are not trying to do sim diffs here, as a reset==z is not good for diffs
// against 4-state simulators.  So there is no sim gold file.

module top(clk, en, in, out);
input clk, en, in;
output out;

wire clk;
wire en;
wire [31:0] in;
wire [31:0] out;

wire reset;
reg dclk;
reg [31:0] a;
reg [31:0] b;
reg [31:0] c;

assign reset = en ? c[0] : 1'bz;

always @(posedge clk or negedge reset)
  if (~reset)
    dclk = 1'b0;
  else
    dclk = clk;

always@(dclk or b or in)
  if (dclk)
    a = b ^ in;

always@(dclk or a or c)
  if (~dclk)
    b = a ^ c;

always@(clk or a or b)
  if (clk)
    c = ~a;
  else
    c = ~b;
  
assign out = ~c;

endmodule
