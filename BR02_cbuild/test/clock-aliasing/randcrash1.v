// Generated with: ../../rangen -n 0xaa86234e
// Using seed: 0xaa86234e
// Writing cyclic circuit of size 100 to file 'aa86234e.v'
// Module count = 1

// module hx
// size = 100 with 11 ports
module hx(MXD, AEep, h, YHqZ, d_NGt, G, CCRd, gy, W, n, dPt, vout);
  input MXD;
  input AEep;
  input h;
  output YHqZ;
  output d_NGt;
  input  G;
  inout  CCRd;
  output gy;
  output W;
  output n;
  output dPt;
  output vout;
  reg    vout;
  
  wire [65:83] MXD;
  wire [3:13]  AEep;
  wire [3:6]   h;
  reg [19:26]  YHqZ;
  reg          d_NGt;
  wire [13:16] G;
  wire [0:2]   CCRd;
  reg [8:16]   gy;
  reg [0:17]   W;
  wire         n;
  reg          dPt;
  wire [1:4]   Ta;
  wire         Jir;
  wire [1:30]  jlWFu;
  reg          nDOi;
  reg [31:59]  lI;
  reg          HjADh;
  reg [1:7]    gN;
  reg          U;
  reg [17:24]  b;
  reg          RMkUI;
  reg          c_D;
  wire [6:6]   f;
  wire [9:15]  asrq;
  wire         Wab;
  reg [0:4]    _sS;
  reg          V;
  reg          ji;
  wire [1:27]  PNJNS;
  reg [1:6]    EryzZ;
  wire [3:18]  ytO_A;
  reg          HJP;
  reg [0:6]    a;
  reg          ENK;
  reg [24:31]  PAuXn;
  reg          UxKH;
  reg          H;
  reg          v_t;
  reg [0:2]    zib;
  reg [0:12]   lX;
  wire [24:26] yk;
  wire [3:6]   qy;
  reg [7:7]    jgltM;
  wire [4:7]   YYyGP;
  reg [2:3]    Xqq;
  reg [17:18]  efWB;
  reg [21:23]  kT;
  reg          CbdG;
  reg          pdFv;
  wire         MyPpX;
  reg [2:3]    TtyM;
  reg [3:23]   _;
  reg          Gnxa;
  reg          LIh;
  reg          Kjk;
  reg [0:3]    sV_Y;
  reg          ugt;
  wire         YiZa;
  wire [76:117] MkYth;
  reg           XXWSt;
  reg           hfL;
  reg [103:119] RjtS;
  wire [27:30]  PXZJ;
  reg           qD;
  reg [20:25]   yH;
  reg [2:3]     vrtlv;
  reg           F;
  wire [5:6]    eF;
  reg [0:3]     khtO;
  reg [4:5]     j;
  reg           PbVON;
  reg [5:5]     kN;
  reg           MteS;
  reg [7:16]    nD;
  reg           Dr;
  reg           Oajfoe;
  wire [35:74]  e;
  reg [3:4]     oNo;
  reg           PZT;
  reg           kodDT;
  reg [4:13]    C;
  reg [4:6]     AQ;
  reg [9:28]    Aa;
  always @(CCRd[0])
    if (~CCRd[0])
      zib[0:1] <= efWB[18]&jlWFu[21:30];
  
  always @(ENK)
    if (ENK)
      Kjk <= V;
  
  assign        CCRd[0] = ~MkYth[92];
  
  always @(negedge &jlWFu[12:25])
    gN[6] = ~v_t;
  
  always @(&yk[24:25])
    if (yk[24:25])
      gN[6] = ~Dr^Dr;
  
  always @(posedge Dr)
    kT[22] <= jlWFu[15:16];
  
  always @(&khtO[1:3])
    if (khtO[1:3])
      Xqq[2:3] <= gy[11:12];
  
  always @(&AEep[8:11])
    if (~AEep[8:11])
      dPt = ~sV_Y[1:3]&eF[5:6];
  
  assign        MyPpX = ~ENK;
  
  always @(negedge PZT)
    U = gy[11:12];
  
  always @(posedge G[16])
    CbdG = jlWFu[15:16];
  
  always @(posedge MkYth[92])
    YHqZ[25:26] = PNJNS[10:21];
  
  always @(&zib[1:2])
    if (zib[1:2])
      vrtlv[3] = ~YHqZ[24];
  
  always @(negedge &sV_Y[1:3])
    PAuXn[24:31] = ~eF[5:6]^gy[11:13];
  
  always @(LIh)
    if (~LIh)
      lX[0:5] = ~gy[11:12]&MkYth[92];
  
  assign        f[6] = YHqZ[21:26]^MkYth[92];
  
  always @(posedge &zib[1:2])
    Aa[11:16] = XXWSt;
  
  always @(efWB[18])
    if (~efWB[18])
      TtyM[2] = ~YiZa;
  
  always @(posedge &Aa[26:28])
    _[8:23] = ~jlWFu[15:16];
  
  always @(d_NGt)
    if (d_NGt)
      lI[39:56] = (~YHqZ[21:26]|jlWFu[15:16]);
  
  always @(negedge v_t)
    PAuXn[24:31] = sV_Y[1:3]^PNJNS[10:21];
  
  always @(&jlWFu[3:6])
    if (~jlWFu[3:6])
      _sS[1:2] = ~qy[4:5]|CCRd[0];
  
  always @(&jlWFu[1:15])
    if (~jlWFu[1:15])
      gN[4:7] = ~G[16];
  
  always @(&jlWFu[21:30])
    if (jlWFu[21:30])
      Aa[9:13] <= (YHqZ[21:26]);
  
  always @(&AEep[8:11])
    if (~AEep[8:11])
      Aa[11:16] = ~jlWFu[8:17]^khtO[1:3];
  
  always @(ugt)
    if (~ugt)
      kodDT <= zib[1:2]^f[6];
  
  always @(h[5])
    if (~h[5])
      j[5] <= ~PNJNS[11:18];
  
  assign        f[6] = jlWFu[21:26]^efWB[18];
  
  always @(posedge &khtO[1:3])
    U = (~jlWFu[21:30]);
  
  always @(&CCRd[0:1])
    if (~CCRd[0:1])
      C[6:9] = ~LIh^gy[11:16];
  
  always @(&PNJNS[11:18])
    if (~PNJNS[11:18])
      PAuXn[25:31] = gy[11:16];
  
  always @(posedge &khtO[1:3])
    yH[20:21] <= ~(~CCRd[1:2]&h[5]);
  
  assign        jlWFu[17:25] = ~gy[11:13];
  
  always @(posedge LIh)
    U = (~CCRd[0]|yk[24:25]);
  
  always @(negedge &jlWFu[3:6])
    Aa[10:20] = yk[24:25]^jlWFu[1:15];
  
  assign        n = ~CCRd[0]|LIh;
  
  always @(ENK)
    if (~ENK)
      oNo[3:4] <= ~yk[24:25];
  
  always @(posedge v_t)
    oNo[3:4] <= jlWFu[15:16]^Aa[26:28];
  
  always @(posedge &YYyGP[5:7])
    ugt = ~MXD[72:75]^jlWFu[12:25];
  
  always @(posedge &AEep[3:5])
    MteS = ~(~efWB[18]);
  
  assign        YYyGP[4:5] = RMkUI;
  
  assign        asrq[12:13] = khtO[1:3]^Aa[26:28];
  
  assign        jlWFu[19:23] = (j[4]^V);
  
  always @(Dr)
    if (~Dr)
      _[17:23] = CCRd[1:2];
  
  always @(&jlWFu[3:6])
    if (jlWFu[3:6])
      kT[22] = qy[4:5];
  
  assign        jlWFu[9:30] = CCRd[0:1];
  
  always @(posedge &khtO[1:3])
    _[8:23] <= G[16]^efWB[18];
  
  always @(&CCRd[1:2])
    if (CCRd[1:2])
      RMkUI = h[5]^G[16];
  
  always @(MkYth[92])
    if (~MkYth[92])
      Aa[10:20] = ~gy[11:13]|PZT;
  
  assign        MkYth[78:114] = V;
  
  always @(&gy[11:13])
    if (gy[11:13])
      yH[20:21] <= ~PNJNS[18:26]^asrq[12];
  
  always @(negedge CbdG)
    d_NGt = CCRd[0]^gy[11:12];
  
  assign        qy[5] = ~EryzZ[1:6];
  
  always @(negedge &CCRd[0:1])
    gN[6] <= ~Dr&ENK;
  
  always @(posedge YiZa)
    ugt = ~jlWFu[21:26]^khtO[1:3];
  
  assign        qy[3:5] = AEep[3:9];
  
  always @(negedge &jlWFu[3:6])
    MteS = ~jlWFu[12:25]^YHqZ[24];
  
  assign        e[47:51] = CCRd[1:2]&CbdG;
  
  assign        ytO_A[8:12] = ~gy[11:12]|U;
  
  assign        jlWFu[11:12] = d_NGt^YYyGP[5:7];
  
  always @(posedge V)
    Gnxa <= MXD[72:75];
  
  assign        MyPpX = U;
  
  always @(posedge &sV_Y[1:3])
    hfL = AEep[8:11];
  
  assign        MkYth[86:107] = PNJNS[10:21];
  
  always @(negedge &jlWFu[8:17])
    sV_Y[2:3] <= ~AEep[3:5]^PNJNS[22:27];
  
  always @(&jlWFu[15:16])
    if (jlWFu[15:16])
      zib[0:1] = ~(CCRd[0]);
  
  always @(&AEep[3:5])
    if (AEep[3:5])
      Oajfoe = ~(~YiZa&CCRd[1:2]);
  
  always @(posedge &eF[5:6])
    efWB[18] = ~pdFv;
  
  always @(negedge &AEep[8:11])
    LIh = AEep[8:11]&MXD[72:75];
  
  assign        MkYth[78:114] = jlWFu[1:15]^sV_Y[1:3];
  
  always @(&MXD[72:75])
    if (~MXD[72:75])
      ugt = ~((~YHqZ[19:20]^jlWFu[21:30]));
  
  always @(negedge &MXD[72:75])
    Aa[10:20] <= YHqZ[24]&YiZa;
  
  always @(negedge efWB[18])
    Kjk = f[6]^G[16];
  
  always @(pdFv)
    if (~pdFv)
      Aa[11:19] <= ~(gy[11:12]^YYyGP[5:7]);
  
  always @(&jlWFu[21:30])
    if (jlWFu[21:30])
      Aa[9:13] <= ENK;
  
  always @(posedge &sV_Y[1:3])
    khtO[0] = MXD[72:75]|PNJNS[22:27];
  
  always @(&MXD[72:75])
    if (MXD[72:75])
      b[20] = (~CCRd[0:1]);
  
  always @(posedge &eF[5:6])
    Aa[9:13] = ~yk[24:25];
  
  always @(&jlWFu[1:13])
    if (jlWFu[1:13])
      XXWSt <= CCRd[0:1];
  
  always @(posedge MkYth[92])
    Gnxa <= jlWFu[3:6];
  
  always @(posedge j[4])
    Xqq[2:3] <= (~YHqZ[19:20]);
  
  always @(&jlWFu[21:26])
    if (jlWFu[21:26])
      PAuXn[25:31] = YYyGP[5:7]|YYyGP[5:7];
  
  assign        jlWFu[3:8] = (~CbdG&qy[4:5]);
  
  assign        asrq[12:13] = jlWFu[1:13]&CCRd[0];
  
  assign        YiZa = YHqZ[19:20]&RMkUI;
  
  assign        PNJNS[21:25] = PNJNS[10:21];
  
  always @(asrq[12])
    if (~asrq[12])
      V = (YYyGP[5:7]^f[6]);
  
  assign        PXZJ[29:30] = (asrq[12] != 0) ? ~PNJNS[11:18] : 2'bz;
  
  assign        MyPpX = ~G[16]&jlWFu[8:17];
  
  assign        ytO_A[7:11] = ~(~sV_Y[1:3]|PNJNS[18:26]);
  
  always @(&PNJNS[22:27])
    if (~PNJNS[22:27])
      PAuXn[24:31] <= ((PNJNS[18:26]|j[4]));
  
  always @(&jlWFu[21:30])
    if (~jlWFu[21:30])
      H <= ~jlWFu[1:13]&jlWFu[12:25];
  
  assign        Jir = ~AEep[8:11];
  
  assign        e[47:51] = ~ENK^Aa[26:28];
  
  always @(ugt)
    if (~ugt)
      TtyM[2] = jlWFu[1:15]|jlWFu[3:6];
  
  always @(negedge f[6])
    a[2:6] <= ~d_NGt&jlWFu[8:17];
  
  always @(negedge ENK)
    Gnxa <= U^qy[4:5];
  
  assign        PNJNS[9:11] = ~khtO[1:3]&YHqZ[19:20];
  
  always @(negedge &AEep[3:5])
    b[20] = ~asrq[12];
  
  assign        PXZJ[29:30] = jlWFu[8:17]&CCRd[0:1];
  
  always @(posedge MkYth[114])
    vout <= MXD;

endmodule
