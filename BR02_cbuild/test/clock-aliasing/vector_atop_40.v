// This testcase is representative of a degredation from 28 clocks to 40 clocks
// in atop that occurred after BR_3597.  bug5507.

module atop(resettx, tstclkl, tmd_pllpwrdn, pwrdn, in1, out1, in2, out2);
  input resettx, tstclkl, tmd_pllpwrdn, pwrdn;
  input       in1, in2;
  output      out1, out2;

  wire [1:0] rxpwrdn;
  pwrdn_gen pwrdn_gen(pwrdn, pwrdn_0, pwrdn_1);
  assign     rxpwrdn[0] = pwrdn_0;
  assign     rxpwrdn[1] = pwrdn_1;
  CDR CDR(resettx, tstclkl, tmd_pllpwrdn, rxpwrdn, in1, out1, in2, out2);
endmodule

module pwrdn_gen(pwrdn, pwrdn_0, pwrdn_1);
  input pwrdn;
  output pwrdn_0, pwrdn_1;
  assign pwrdn_0 = pwrdn;
  assign pwrdn_1 = pwrdn;
endmodule

module CDR(RESETTX, TSTCLK, PLLPWRDN, RXPWRDN, in1, out1, in2, out2);
  input RESETTX, TSTCLK, PLLPWRDN, in1, in2;
  input [1:0] RXPWRDN;
  output      out1, out2;

  wire [1:0] PWRDN;
  wire        pwrdn_1 = PWRDN[0];
  wire        pwrdn_2 = PWRDN[1];

  assign PWRDN = (PLLPWRDN ? 8'hff : RXPWRDN);     // /cust/data/regression-copies/stargen/aruba/rev1/rtl/cdr622m8BY_rtl.v:999

  RX_SLICE RX1(RESETTX, TSTCLK, pwrdn_1, in1, out1);
  RX_SLICE RX2(RESETTX, TSTCLK, pwrdn_2, in2, out2);
endmodule

module RX_SLICE(RESET, HCK, PWRDN, in, out);
  input RESET, PWRDN, HCK, in;
  output out;
  reg    out, cds_PWRDN;

  always @(negedge HCK)                            // /cust/data/regression-copies/stargen/aruba/rev1/vlib/RX_SLICE.v:196
  // $block_RX_SLICE_v_L196;3
  begin                                          // /cust/data/regression-copies/stargen/aruba/rev1/vlib/RX_SLICE.v:196
    // Declarations
    // Statements
    cds_PWRDN <= PWRDN;                           // /cust/data/regression-copies/stargen/aruba/rev1/vlib/RX_SLICE.v:196
  end

   wire carbon_reset;		// insert delay to avoid glitch in aldec when RESET and
				// cds_PWRDN change at same time
  assign #2 carbon_reset = (RESET | cds_PWRDN);       // /cust/data/regression-copies/stargen/aruba/rev1/vlib/RX_SLICE.v:204

  always @(posedge carbon_reset)
    out <= in;
endmodule
