// In this example, clock analysis fails because of the way expression
// resynthesis works, finding all the driving flow for the nets used
// in an expression.  That makes the split off async reset drivers
// look like multiple drivers, and ESPopulateExpr::createNetExpr gives
// up.

module top(in, out, clk, ena, rst);
  input in, clk, ena, rst;
  output out;

  reg    q1, q2, out;

  initial begin
    q1 = 0;
    q2 = 0;
    out = 0;
  end

  clkgen u1(clk1, clk, ena, rst);
  clkgen u2(clk2, clk, ena, rst);
  clkgen u3(clk3, clk, ena, rst);

  always @(posedge clk1)
    q1 <= in;

  always @(posedge clk2)
    q2 <= q1;

  always @(posedge clk3)
    out <= q2;
endmodule // clkbuf

module clkgen(out, clk, ena, rst);
  output out;
  input  clk, ena, rst;
  reg    out, divided;

  always @(posedge clk or posedge rst)
    if (rst)
      divided <= 0;
    else
      divided <= ~divided;

  always @(posedge divided or posedge rst)
    if (rst)
      out <= 0;
    else
      out <= ena;
endmodule

