module ena_reset(in, out, out2, clk, rst, ena);
  input in, clk, rst, ena;
  output out, out2;
  reg    out, out2;

  initial begin
    out = 0;
    out2 = 0;
  end

  always @(posedge clk or posedge rst) begin
    if (rst)
      out <= 0;
    else begin
      if (ena)
        out <= in;
      out2 <= in;
    end
  end
endmodule

module top(i1, i2, i3, i4, o1, o2, o3, o4, clkin, clk, rst, ena);
  input i1, i2, i3, i4, clk, rst, ena, clkin;
  output o1, o2, o3, o4;
  reg    o1, o2, o3, o4;

  initial begin
    o1 = 0;
    o2 = 0;
    o3 = 0;
    o4 = 0;
  end

  ena_reset er1(clkin, clk1, clk2, clk, rst, ena);
  ena_reset er2(clkin, clk3, clk4, clk, rst, ena);

  always @(posedge clk1)
    o1 <= i1;

  always @(posedge clk2)
    o2 <= i2;

  always @(posedge clk3)
    o3 <= i3;

  always @(posedge clk4)
    o4 <= i4;
endmodule
