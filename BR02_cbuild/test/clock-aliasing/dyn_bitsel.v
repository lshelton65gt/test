module top(iclk, iclks, iclksel, clk, i1, i2, i3, i4, i5, i6,
           o1, o2, o3, o4, o5, o6);
  input iclk;
  input [7:0] iclks;
  input [2:0] iclksel;
  input       i1, i2, i3, i4, i5, i6, clk;
  output      o1, o2, o3, o4, o5, o6;
  reg         o1, o2, o3, o4, o5, o6, clk5, clk6;
  reg [7:0]   clks;
  reg [2:0]   clksel;

  wire        clk1 = clks[clksel];
  wire        clk2 = clks[clksel];

  wire [7:0] clks2 = {8{clk}};
  wire 	     clk3 = clks2[clksel];
  wire 	     clk4 = clks2[clksel];

  initial begin
    o5 = 0;
    o6 = 0;
  end

  // register the clock-tree inputs so they don't glitch in XL
  always @(posedge iclk) begin
    clks <= iclks;
    clksel <= iclksel;
  end

  always @(posedge iclk) begin
    clk5 <= clks[clksel];
    clk6 <= clks[clksel];
  end

  always @(posedge clk1)
    o1 <= i1;
  always @(posedge clk2)
    o2 <= i2;
  always @(posedge clk3)
    o3 <= i3;
  always @(posedge clk4)
    o4 <= i4;
  always @(posedge clk5)
    o5 <= i5;
  always @(posedge clk6)
    o6 <= i6;
endmodule
