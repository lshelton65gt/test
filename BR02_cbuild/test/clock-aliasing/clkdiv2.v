module clkdiv(in, in2, in3, out, clk2a_, clk2b_, clk);
  input in, in2, in3, clk;
  output out, clk2a_, clk2b_;

  reg    q1, q2, q3, q4, out, out2, out3;
  reg    clk2a, clk2b;
  wire    clk1, clk2, clk_;

  assign clk1 = clk;
  assign clk_ = ~clk1;
  assign clk2 = ~clk_;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
    out = 0;
    clk2a = 0;
    clk2b = 0;
    out2 = 0;
    out3 = 0;
  end

  assign clk2a_ = ~clk2a;
  assign clk2b_ = ~clk2b;

  always @(posedge clk)
    clk2a <= clk2a_;

  always @(posedge clk2)
    clk2b <= clk2b_;

  always @(posedge clk2a)
    q1 <= in;

  always @(posedge clk2b)
    q2 <= q1;

  always @(posedge clk2a)
    q3 <= q2;

  always @(posedge clk2b)
    q4 <= q3;

  always @(posedge clk2a)
    out <= q4;

/*
  always @(negedge clk2a_)
    out2 = in2;
    
  always @(negedge clk2b_)
    out3 = in3;
*/
endmodule // clkdiv

