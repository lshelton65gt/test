module top(clk, c, i1, i2, o1, o2);
  input clk, c, i1, i2;
  output o1, o2;

  reg    c1, c2, o1, o2;
  integer neg1;
  wire [32:0] zeros = 33'b0;
  wire [32:0] ones = zeros ^ neg1; // one should be sign-extended
  wire        one = ones[32];

  always neg1  = -1;

  initial begin
    c1 = 0;
    c2 = 0;
  end

  always @(posedge clk)
    c1 <= ~c1;

  always @(posedge clk)
    c2 <= ~(c2 & one);

  always @(posedge c1)
    o1 <= i1;

  always @(posedge c2)
    o2 <= i2;
endmodule
