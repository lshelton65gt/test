// file: vector_rjc2.v
// created: 10/13/03
// author: cloutier
// General topic: A Clock that is part of a vector at top level is split at
//                lower level, do we alias them? 
// specific for this test: In this test three different instances of sub* are
//                         used, each selecting a different bit of wide_clk.
// desired result: The clocks should not be merged.
// vector clock is primary input, different bits selected at registers using it,
// so the clccks should not be merged
module top(wide_clk, clk1, in1, in2, in3, out1, out2, out3);
   input [7:0] wide_clk;
   input       clk1;
   input       in1, in2, in3;
   output      out1, out2, out3;

   wire [1:0]  ctl;

   wire [7:0]  wide_clk_int;
   assign      wide_clk_int = wide_clk;

//   reg [7:0] wide_clk_int;
//   always @(posedge clk1)
//     wide_clk_int <= wide_clk;
   
   

  sub1 u1 (wide_clk_int, in1, out1);
  sub2 u2 (wide_clk_int, in2, out2);
  sub3 u3 (wide_clk_int, in3, out3);
endmodule // top

// sub1 uses bit 1 of wide_clk as it's clock
module sub1(wide_clk, in, out);
  input [7:0] wide_clk;
  input       in;
  output      out;
  reg         out;

  initial     out = 1'b0;
  wire        clk = wide_clk[1];

  always @(posedge clk)
      out <= in;
endmodule // sub

// sub2 uses bit 2 of wide_clk as it's clock
module sub2(wide_clk, in, out);
  input [7:0] wide_clk;
  input       in;
  output      out;
  reg         out;

  initial     out = 1'b0;
  wire        clk = wide_clk[2];

  always @(posedge clk)
      out <= in;
endmodule // sub

// sub3 uses bit 3 of wide_clk as it's clock
module sub3(wide_clk, in, out);
  input [7:0] wide_clk;
  input       in;
  output      out;
  reg         out;

  initial     out = 1'b0;
  wire        clk = wide_clk[3];

  always @(posedge clk)
      out <= in;
endmodule // sub
