// test packaging up a clock in a vector

module top(clks, in1, in2, out1, out2);
  input [1:0] clks;
  input in1, in2;
  output out1, out2;

  wire [1:0] ctl;
  sub u1(clks, in1, out1);
  sub u2(clks, in2, out2);
endmodule // top

module sub(ctl, in, out);
  input [1:0] ctl;
  input       in;
  output      out;
  reg         out;

  wire        clk = ctl[1];
  buf #(1) (rst, ctl[0]);

  always @(posedge clk or posedge rst)
    if (rst)
      out <= 0;
    else
      out <= in;
endmodule // sub
