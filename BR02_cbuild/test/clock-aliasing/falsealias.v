// This mimics a situation in pogo where two reset signals are
// marked as equivalent when they are slightly different in that
// one of them has a resettable flop in its fanin and the other
// one has a non-resettable flop.
module top(clk, in, rin, rclk, rrst, out1, out2);
  input clk, in, rin, rclk, rrst;
  output out1, out2;
  reg    out1, out2, reset1, reset2;

  always @(posedge clk or posedge reset1)
    if (reset1)
      out1 <= 0;
    else
      out1 <= in;

  always @(posedge clk or posedge reset2)
    if (reset2)
      out2 <= 0;
    else
      out2 <= in;


  always @(posedge rclk)
    reset1 <= rin;

  always @(posedge rclk or posedge rrst)
    if (rrst)
      reset2 <= 0;
    else
      reset2 <= rin;
endmodule // top


