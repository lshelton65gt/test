module top(clk, clkin, in1, in2, out1, out2, clkout);
  input  clk, clkin, in1, in2;
  output out1, out2, clkout;
  reg    clkout, clk2, out1, out2;

  initial begin
    clkout = 0;
    clk2 = 0;
    out1 = 0;
    out2 = 0;
  end

  always @(posedge clk) begin
    clkout = clkin;
    clk2 = clkout;
  end

  always @(posedge clkout)
    out1 <= in1;

  always @(posedge clk2)
    out2 <= in1;
endmodule
