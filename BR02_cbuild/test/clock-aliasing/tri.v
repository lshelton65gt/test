module foo(i1, i2, e1, e2, out);
  input i1, i2, e1, e2;
  output out;

  wire   temp;
  assign temp = e1? i1: 1'bz;
  assign temp = e2? i2: 1'bz;
  buf(out, temp);
endmodule
