module top (q1, q2, q3, q4, clk, hold, d1, d2, d3, d4);
   output q1, q2, q3, q4;
   input  clk, hold, d1, d2, d3, d4;

   // Create four clocks with muxes
   wire   gclk1, gclk2, gclk3, gclk4;
   assign gclk1 = !hold && clk;
   assign gclk2 = ~gclk1;
   assign gclk3 = hold || ~clk;
   assign gclk4 = ~gclk3;

   // Use the clocks
   reg 	  q1, q2, q3, q4;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
  end

   always @ (posedge gclk1)
     q1 <= d1;
   always @ (posedge gclk2)
     q2 <= d2;
   always @ (posedge gclk3)
     q3 <= d3;
   always @ (posedge gclk4)
     q4 <= d4;

endmodule // top
