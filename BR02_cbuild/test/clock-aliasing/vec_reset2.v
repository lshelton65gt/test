// bug6225 -- using dynamic bit-select as an async reset, but the
// dynamic expr is not def'd in the seq always block, so it's safe
// to substitute

//Sensitivity list of always block has vector that consists Async signal and sync signal.
module tc(clk_in, async_reset, sync_reset, out, reset_sel_in);
  input		clk_in, async_reset, sync_reset, reset_sel_in;
  output [31:0] out;
  
  reg [1:0]    reset;
  reg          clk, reset_sel;
  reg [31:0]    out;
  
  initial begin
    reset = 0;
    reset_sel = 0;
    clk = 0;
  end

  // these delay statements force Aldec to use the same input-flow model
  // as Carbon, where at the periphery, data is faster than clock, and
  // clock is faster than reset
  always @(clk_in)
    #1 clk = clk_in;
  always @(async_reset or reset_sel_in) begin
    #3   // a delay of only 2 yields an event race due to delay in testdriver
      reset_sel = reset_sel_in;
      reset[reset_sel] = async_reset;
  end


  always @(posedge clk or posedge reset[reset_sel]) begin
    if (reset[reset_sel] == 1'b1) //reset[0] is asynchronous reset
      out <= 32'b0;
    else
      begin
	if(sync_reset)
	  out <= 32'hffff0000;
	else
	  out <= out + 32'd1;
      end
  end
  
endmodule
