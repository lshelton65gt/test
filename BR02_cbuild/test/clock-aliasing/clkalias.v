module top (fclk, q1, q2, q3, q4, iclk, ihold, d1, d2, d3, d4);
   output q1, q2, q3, q4;
   input  fclk, iclk, ihold, d1, d2, d3, d4;

   // Create four clocks with muxes
   reg   gclk1, gclk3, clk, hold;

   always @(posedge fclk) begin
     clk <= iclk;
     hold <= ihold;
   end

   always @(negedge fclk) begin
     gclk1 <= hold ? 1'b0 : clk;
     gclk3 <= hold ? 1'b1 : ~clk;
   end

   wire gclk2 = ~gclk1;
   wire gclk4 = ~gclk3;

   // Use the clocks
   reg 	  q1, q2, q3, q4;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
    gclk1 = 0;
    gclk3 = 0;
    clk = 0;
    hold = 0;
  end

   always @ (posedge gclk1)
     q1 <= d1;
   always @ (posedge gclk2)
     q2 <= d2;
   always @ (posedge gclk3)
     q3 <= d3;
   always @ (posedge gclk4)
     q4 <= d4;

endmodule // top
