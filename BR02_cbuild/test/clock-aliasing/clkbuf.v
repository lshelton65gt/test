module clkbuf(in, out, clk);
  input in, clk;
  output out;

  reg    q1, q2, out;
  wire    clk1, clk2, clk_;

  initial begin
    q1 = 0;
    q2 = 0;
    out = 0;
  end

  assign clk1 = clk;
  assign clk_ = ~clk1;
  assign clk2 = ~clk_;

  always @(posedge clk)
    q1 <= in;

  always @(posedge clk1)
    q2 <= q1;

  always @(posedge clk2)
    out <= q2;
endmodule // clkbuf
