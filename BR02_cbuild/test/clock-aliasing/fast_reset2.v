`timescale 1ns/1ns

module top(clk, out, in);
  input clk;
  input [31:0] in;
  output [31:0] out;
  reg [31:0]    out;
  reg del_clk, del_rst;
  reg [3:0]    tmp;
  integer      cycle;           // avoids sim-difference due to startup glitch 

  initial begin
    tmp = 4'b0;
    out = 1'b0;
    del_clk = 1'b0;
    del_rst = 1'b0;
    cycle = 0;
  end

  always @(posedge clk)
    tmp <= tmp + 1;

  // Create a predictable async reset that will race with clock
  wire rst = tmp == 4'b0111;
  wire         pre;             // carbon asyncReset
                                // carbon fastReset
  assign pre = tmp == 4'b0000;

  // slow down the clock for Aldec, so that 'pre' deasserts quicker
  always @(clk)
    #1 del_clk = clk;

  // slow down the async reset even more
  always @(rst)
    #2 del_rst = rst;

  always @(posedge del_clk or posedge del_rst)
    if (del_rst)
      out <= 0;      
    else if (pre)
      out <= 1;
    else if (cycle > 1)
      out <= in;

  always @(posedge del_clk)
    cycle <= cycle + 1;

endmodule
