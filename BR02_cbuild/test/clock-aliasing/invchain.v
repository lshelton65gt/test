// This testcase mimics a situation found in
// test/cust/matrox/sunex/carbon/cwtb/mem_grp,
// where an inverter chain is formed through a vector, and
// various bits along that inverter-chain are tapped off
// and used as clocks

module top(clkin, i0, i1, i2, i3, o0, o1, o2, o3);
  input clkin, i0, i1, i2, i3;
  output o0, o1, o2, o3;

  wire [3:0] clkvec;

  inv u0(clkvec[0], clkin);
  inv u1(clkvec[1], clkvec[0]);
  inv u2(clkvec[2], clkvec[1]);
  inv u3(clkvec[3], clkvec[2]);

  flop f0(o0, i0, clkvec[0]);
  flop f1(o1, i1, clkvec[1]);
  flop f2(o2, i2, clkvec[2]);
  flop f3(o3, i3, clkvec[3]);

endmodule

module inv(z, a);
  output z;
  input  a;



  assign z = ~a;
endmodule

module flop(q, d, cp);
  output q;
  input  d, cp;
  reg    q;



  always @(posedge cp)
    q <= d;
endmodule
