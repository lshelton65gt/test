// file: vector_rjc1c.v
// created: 10/13/03
// author: cloutier
// General topic: A Clock that is part of a vector at top level, then
//                bitselected within a submodule and distributed to lower level,
//                do we alias these clocks? 
// specific for this test: In this test threee instance of sub1 are used, so we
//                         should merge the clocks.  In this case the clock is
//                         divided and split at a sub module level.
// desired result: the three clocks within instances of sub1 should be aliased

module top(v_clk, clk1, in1, in2, in3, out1, out2, out3);
   input [7:0] v_clk;
   input       clk1;
   input       in1, in2, in3;
   output      out1, out2, out3;

   wire [1:0]  ctl;

  clk_div_split ucs1 ( v_clk, clk1, s_clk_int1 );
  clk_div_split ucs2 ( v_clk, clk1, s_clk_int2 );
  clk_div_split ucs3 ( v_clk, clk1, s_clk_int3 );

  sub1 u1 (s_clk_int1, in1, out1);
  sub1 u2 (s_clk_int2, in2, out2);
  sub1 u3 (s_clk_int3, in3, out3);
endmodule // top

module sub1(clk, in, out);
   input       clk;
   input       in;
   output      out;
   reg         out;

   initial out = 1;
   always @(posedge clk)
     out <= in;
endmodule // sub


// vector clock in
// scalar clock out
// clock divider within
module clk_div_split ( v_clk, clk1, out_clk );
   input [7:0] v_clk;
   input clk1;
   output      out_clk;

   reg out_clk;
   wire clk1;

   initial out_clk = 0;

   always @(posedge clk1)
     out_clk <= v_clk[0];
endmodule // clk_div_split
