// This is a derived clock aliasing issue that is exposed by
// running btop, and (hopefully) faithfully reproduced here.

module top(iclk, itclk, irclk, iclksel, clkena_, i1, i2, o1, o2);
  input iclk, itclk, irclk, iclksel, clkena_, i1, i2;
  output o1, o2;
  reg    o1, o2, tclk, rclk, clksel;

  initial begin
    #1
    o1 = 0;
    o2 = 0;
    tclk = 0;
    rclk = 0;
    clksel = 0;
  end

  always @(posedge iclk) begin
    tclk <= itclk;
    rclk <= irclk;
    clksel <= iclksel;
  end

  clkgen u1(tclk, rclk, clksel, clkena_, clk1);
  clkgen u2(tclk, rclk, clksel, clkena_, clk2);

  always @(posedge clk1)
    o1 <= i1;
  always @(posedge clk2)
    o2 <= i2;
endmodule // top

module clkgen(tclk, rclk, clksel, clkena_, clk);
  input tclk, rclk, clksel, clkena_;
  output clk;
  reg    muxout, clkena_delay, orout;

  initial begin
    #1
    muxout = 0;
    clkena_delay = 0;
    orout = 0;
  end

  always @(clksel or tclk or rclk)
    if (clksel)
      muxout = tclk;
    else
      muxout = rclk;

  always @(posedge muxout)
    clkena_delay <= clkena_;

  always @(muxout or clkena_delay)
    orout = muxout | clkena_delay;

  inverter inv(clk, orout);
endmodule // clkgen

module inverter(z, a);
  output z;
  input  a;
  not(z, a);
endmodule // inverter
