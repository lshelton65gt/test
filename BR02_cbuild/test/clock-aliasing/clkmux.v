module bug (clk, d, q0, q1, sel);
   input clk, sel;
   input [15:0] d;
   output [15:0] q0, q1;
   wire [15:0] 	 clk_vec;
   reg [15:0] 	 q1;
   reg           qsel;
   
   always @(posedge clk)
     qsel <= sel;

   assign clk_vec = qsel ? 
	  {~clk, ~clk, ~clk, ~clk, ~clk, ~clk, ~clk, ~clk,
	   ~clk, ~clk, ~clk, ~clk, ~clk, ~clk, ~clk, ~clk} :
	  {clk, clk, clk, clk, clk, clk, clk, clk,
	   clk, clk, clk, clk, clk, clk, clk, clk};
/*
   flop u0 (clk, d[0], q0[0]);
   flop u1 (clk, d[1], q0[1]);
   flop u2 (clk, d[2], q0[2]);
   flop u3 (clk, d[3], q0[3]);
   flop u4 (clk, d[4], q0[4]);
   flop u5 (clk, d[5], q0[5]);
   flop u6 (clk, d[6], q0[6]);
   flop u7 (clk, d[7], q0[7]);
   flop u8 (clk, d[8], q0[8]);
   flop u9 (clk, d[9], q0[9]);
   flop u10 (clk, d[10], q0[10]);
   flop u11 (clk, d[11], q0[11]);
   flop u12 (clk, d[12], q0[12]);
   flop u13 (clk, d[13], q0[13]);
   flop u14 (clk, d[14], q0[14]);
   flop u15 (clk, d[15], q0[15]);
*/
   flop u0 (clk_vec[0], d[0], q0[0]);
   flop u1 (clk_vec[1], d[1], q0[1]);
   flop u2 (clk_vec[2], d[2], q0[2]);
   flop u3 (clk_vec[3], d[3], q0[3]);
   flop u4 (clk_vec[4], d[4], q0[4]);
   flop u5 (clk_vec[5], d[5], q0[5]);
   flop u6 (clk_vec[6], d[6], q0[6]);
   flop u7 (clk_vec[7], d[7], q0[7]);
   flop u8 (clk_vec[8], d[8], q0[8]);
   flop u9 (clk_vec[9], d[9], q0[9]);
   flop u10 (clk_vec[10], d[10], q0[10]);
   flop u11 (clk_vec[11], d[11], q0[11]);
   flop u12 (clk_vec[12], d[12], q0[12]);
   flop u13 (clk_vec[13], d[13], q0[13]);
   flop u14 (clk_vec[14], d[14], q0[14]);
   flop u15 (clk_vec[15], d[15], q0[15]);

   always @(posedge clk)
     q1 <= d;

endmodule

module flop (clk, d, q);
   input clk, d;
   output q;
   reg 	  q;

   always @(posedge clk)
     q <= d;
endmodule
