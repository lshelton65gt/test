module top(out, c0, c1, sel, in);
  output out;
  input  c0, c1, sel, in;

  reg    clk, out;
  initial begin
    clk = 0;
    out = 0;
  end

  always @(c0 or c1 or sel)
    case (sel)
      1'b0: clk = c0;
      1'b1: clk = c1;
    endcase

  always @(posedge clk)
    out <= in;
endmodule
