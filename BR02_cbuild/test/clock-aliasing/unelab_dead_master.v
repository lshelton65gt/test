// Try to mimic a situation in ati/chip where a clock master is
// unelaboratedly dead, causing a sanity-elab assert

module top(in1, in2, in3, out1, out2, out3, undriv2, undriv3);
  input in1, in2, in3;
  output out1, out2, undriv2, undriv3, out3;

  undriv_out1 u1(undriv1, out1, in1);
  undriv_out2 u2(undriv2, out2, in2);
  undriv_out1 u3(undriv3, out3, in3);
  pullup(undriv2);
endmodule

module undriv_out1(undriv, out, in);
  output undriv, out;
  input  in;

  undriv_out2 u1(undriv, tmp, in);
  or(out, undriv, tmp);
endmodule

module undriv_out2(undriv, out, in);
  output undriv, out;
  input  in;

  assign out = ~in;
endmodule
