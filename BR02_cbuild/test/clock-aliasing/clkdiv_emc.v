module top(in1, in2, out1, out2, pre, clk, c1, c2);
  input in1, in2, pre, clk, c1, c2;
  output out1, out2;

  // We are challenged by EMC's clock tree, which includes
  // flops which differ only by async reset
  reg    q1, q2, q3, q4, out1, out2;

  initial begin
    q1 = 0;
    q2 = 0;
    q3 = 0;
    q4 = 0;
    out1 = 0;
    out2 = 0;
  end

  always @(posedge clk or posedge pre)
    if (pre)
      q1 <= 1;
    else
      q1 <= c1;

  always @(posedge clk or posedge pre)
    if (pre)
      q2 <= 1;
    else
      q2 <= c2;

  
  // Now make dividers based on q1 and q2.  They should be
  // seen as different
  always @(negedge clk) begin
    q3 <= ~q1;
    q4 <= ~q2;
  end

  always @(posedge q3)
    out1 <= in1;

  always @(posedge q4)
    out2 <= in2;
endmodule

