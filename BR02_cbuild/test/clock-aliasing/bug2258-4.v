// This is copied from test/schedule/schedule1/clkalways_confict.v.  It has
// a real multiple driver in the clock path and it should warn.

// We are not trying to do sim diffs here, as reset=x is not
// good for diffs against 4-state simulators.  So there is no sim gold file.

module bug(async_rst_l, clk, out, inThing);   
   input          async_rst_l;
   input 	  clk;
   input          inThing;
   
   output 	  out;
   
   reg 		  out;
   reg 		  async_rst;
   
   always @(async_rst_l) begin
      async_rst = async_rst_l;
   end

   always @(async_rst_l) begin
      async_rst = (~async_rst_l);
   end

   // posedge async_rst seems to be the problem here
   always @(posedge clk or posedge async_rst) begin
      if (async_rst) begin
	 out <= 1'b1;
      end
      else
	begin
	    out <= inThing;
	end
   end

endmodule // bug

