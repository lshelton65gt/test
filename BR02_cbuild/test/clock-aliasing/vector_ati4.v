// test packaging up a clock in a vector in a scan chain element
// and cascading a few of them

module top(clk, rst, in, out);
  input clk, rst, in;
  output out;

  wire [1:0] ctl1;
  reg        out;

  initial out = 0;

  assign ctl1[1] = ~clk;
  assign ctl1[0] = ~rst;

  always @(posedge ctl1[1])
    out <= in;
endmodule // top
