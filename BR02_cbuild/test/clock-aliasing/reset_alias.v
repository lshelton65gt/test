module BUFX12 (A, Y);
  input A;
  output Y;
  assign Y = A;
endmodule 

module DFFRHQX4 ( CK, D, RN, Q );
  input CK;
  input D;
  input RN;
  output Q;
  reg Q;
  initial Q = 0;
  always @ (posedge CK or negedge RN)
  begin
    if (~RN)
      Q <= 1'b0;
    else
      Q <= #0.5 D;
  end
endmodule

module reset_sync (i_clk, i_reset_, i_test_en, o_reset_);
  input i_clk; 
  input i_reset_;
  input i_test_en;
  output o_reset_;
  wire n0, n1, reset_;
  reg o_reset_pretest, i_reset_p1, o_reset_;
  initial begin
    o_reset_pretest = 0;
    i_reset_p1 = 0;
    o_reset_ = 0;
  end

  always @(posedge i_clk)
  begin
    i_reset_p1 <= i_reset_;
  end
  always @(posedge i_clk)
  begin
    o_reset_pretest <= i_reset_p1;
  end
  always
    #1 o_reset_ = o_reset_pretest | i_test_en;

endmodule 


module clkbufs(in,out1,out2);
  input in;
  output out1,out2;
  BUFX12 U0(in,out1);
  BUFX12 U1(in,out2);
endmodule

module top(clk,rst,q1,q2);
  input clk,rst;
  output q1,q2;

  clkbufs clkbufs_inst(clk,clk1,clk2);
  BUFX12 rst1_buf(rst,rst1);
  BUFX12 rst2_buf(rst,rst2);
  reset_sync reset_sync1(clk1,rst1,1'b0,rst1_synced);
  reset_sync reset_sync2(clk2,rst2,1'b0,rst2_synced);
  DFFRHQX4 flop1(clk1,~q1,rst1_synced,q1);
  DFFRHQX4 flop2(clk2,~q2,rst2_synced,q2);

endmodule
