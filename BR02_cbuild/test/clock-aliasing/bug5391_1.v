// last mod: Mon Dec  5 21:17:08 2005
// filename: test/clock-aliasing/bug5391_1.v
// Description:  This test is an attempt to duplicate the issue seen in bug5391,
// where the master of a reset is changed to an inverted reset.  

`define MSB 2

module bug5391_1(clock, in1, in2, out1, rst);
   input clock, rst;
   input [`MSB:0] in1, in2;
   output [`MSB:0] out1;
  reg    dclk1, dclk2;          // these are equivalent

  always @(posedge clock)
    dclk1 = ~(dclk1 | rst);
  always @(posedge clock)
    dclk2 = ~(dclk2 | rst);

   
   m0 u0 (~dclk1, in1, in2, out1[0]);
   m1 a1 (dclk2, in1, in2, out1[1]);
   m2 u2 (~clock, in1, in2, out1[2]);

endmodule


module m0(clock, in1, in2, out1);
   input clock;
   input [`MSB:0] in1, in2;
   output out1;
   reg out1;
   initial out1 = 1'b0;

   always @(posedge clock)
     begin: main
       out1 = in1[0] & in2[0];
     end

endmodule

module m1(clock, in1, in2, out1);
   input clock;
   input [`MSB:0] in1, in2;
   output out1;
   wire 	   clk;

   buf(clk, clock);

   m1_1 u1 (clk, in1, in2, out1);
endmodule


module m1_1(clock, in1, in2, out1);
   input clock;
   input [`MSB:0] in1, in2;
   output out1;
   reg out1;
   initial out1 = 1'b0;

   always @(posedge clock)
     begin: main
       out1 = in1[1] & in2[1];
     end

endmodule


module m2(clock, in1, in2, out1);
   input clock;
   input [`MSB:0] in1, in2;
   output out1;
   reg out1;
   initial out1 = 1'b0;

   always @(posedge clock)
     begin: main
       out1 = in1[2] & in2[2];
     end

endmodule


