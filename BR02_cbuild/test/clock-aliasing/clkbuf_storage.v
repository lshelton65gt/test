// This tests an issue where we process the clock-divider net top.clk3
// first, given a divider clock of top.u3.divided.  Then we process
// top.clk2, and discover its divider, top.u2.divided, is equivalent
// to top.u3.divided, and has a better name.  The dividers get aliased,
// but we will think top.clk2 is different from top.clk3 because we
// were hashing on the NUNetElab* of the divider-clock, which are
// different but equivalent nets.
//
// The fix is to hash on the BDD of the divider-clock rather than
// the NUNetElab* of the divider-clock.

module top(in, out, clk, ena);
  input in, clk, ena;
  output out;

  reg    q1, q2, out;

  initial begin
    q1 = 0;
    q2 = 0;
    out = 0;
  end

  clkgen u1(clk1, clk, ena);
  clkgen u2(clk2, clk, ena);
  clkgen u3(clk3, clk, ena);

  always @(posedge clk1)
    q1 <= in;

  always @(posedge clk2)
    q2 <= q1;

  always @(posedge clk3)
    out <= q2;
endmodule // clkbuf

module clkgen(out, clk, ena);
  output out;
  input  clk, ena;
  reg    out, divided;

  always @(posedge clk)
    divided <= ~divided;

  always @(posedge divided)
    out <= ena;
endmodule
