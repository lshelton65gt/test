// last mod: Wed Sep 28 15:38:32 2005
// filename: test/clock-aliasing/bug5071.v
// Description:  This test would segfault at one time (09/28/05)

module bug5071(x, clk);
   inout x;
   input clk;
   
   
   wire [1:0] x;			// note x[0] never driven
   wire       clk;
   
   
   reg 	      a;
   reg 	      b;
   
   
   always @(negedge clk)
     a = x[0];
   
   
   always @(negedge a)
     b = 1'b1;
   
   
   assign     x[1] = ~b;
   
   
   sub S0 ({x,a});
   
   
endmodule


module sub(Q);
   input Q;
   
   
   wire [2:0] Q;
   
   
endmodule
