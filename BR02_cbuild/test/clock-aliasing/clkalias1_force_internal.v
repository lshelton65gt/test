// file: clkalias1_force_internal.v
// general topic: testing related to interaction of clock aliasing and force/deposit directives
// specific topic: a design that has equivalent clocks should not equiv them
//                 when there is a directive to make one of the internal clocks
//                 forcable
// this case should be run with the directive forceSignal top.clk2a

// expected results: top.clk2a and top.clk2b will not be equiv
// expected results: top.clk and top.clk2 will be aliased


`include "clkalias1_base.v"
