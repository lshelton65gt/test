// this test showed a bug in clock aliasing, where we would incorrectly alias
// rst_n[0] with rst_n[1] (because of a problem in handling partselects in the
// aliasing code)
// expected results are that the bits of rst_n are NOT aliased together.
module bug2548(clk, global_rst, rst_in, rst_sel, in1, in2, out1, out2);
   input clk;
   input global_rst;
   input [1:0] rst_in;
   input       rst_sel;
   input       in1, in2;
   output      out1, out2;

   wire [1:0]  rst_int, rst_n;
   
   rst_gen rst_gen(.clk(clk), .rst(global_rst), .in(rst_in), .sel(rst_sel), .out(rst_int));

   inverter interver[1:0] (.in(rst_int), .out(rst_n));
   
   flop flop1(.clk(clk), .rsti_n(rst_n[0]), .in(in1), .out(out1));
   flop flop2(.clk(clk), .rsti_n(rst_n[1]), .in(in2), .out(out2));
   
endmodule // bug

module rst_gen(clk, rst, in, sel, out);
   input clk, rst;
   input [1:0] in;
   input       sel;
   output [1:0] out;
   reg [1:0] out;

   always @(posedge clk or posedge rst)
     if (rst)
       out <= 2'b11;
     else if (sel)
       out <= in;
   
endmodule // rst_gen

module flop(clk, rsti_n, in, out);
   input clk, rsti_n, in;
   output out;
   reg 	  out;

   always @(posedge clk or negedge rsti_n)
     if (~rsti_n)
       out <= 1'b0;
     else
       out <= in;

endmodule // flop

module inverter(in, out);
   input in;
   output out;

   assign out = ~in;
endmodule
   
