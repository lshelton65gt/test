// I accidently found a clock aliasing problem with this testcase, so I
// copied it into test/clock-aliasing.  The dynamic bitselect code in
// clock aliasing was ignoring the actual select-net and using the
// bit-select *variable* as the select.  For example,
//
//   wire [3:0] var;
//   wire [1:0] sel;
//
//   assign clk = var[sel];
//
// We would create this expression:
//   assign clk = (var[0] && (var==0)) |
//                (var[1] && (var==1)) |
//                (var[2] && (var==2)) |
//                (var[3] && (var==3));
// when we need
//   assign clk = (var[0] && (sel==0)) |
//                (var[1] && (sel==1)) |
//                (var[2] && (sel==2)) |
//                (var[3] && (sel==3));


module foo(ck,d1, d2, q, s, a);
   input ck;
   output [255:0] q;
   input          a;

   reg [255:0]    m [1:0];
   initial begin
     m[0] = 256'b0;
     m[1] = 256'b0;
   end

   input [7:0]    s;
   input [255:0]  d1;
   input          d2;

   wire           ckgated;

   assign         q = m[a];


   always @(posedge ckgated)begin
      m[a] <= d1;
   end

   ckg ckg1(d1, ck, ckgated, s);


endmodule // foo

module ckg (d1, ck, cko, s);
   input [255:0] d1;
   input         ck;

   input [7:0]   s;
   output        cko;

   assign        cko =  ck & d1[s];
endmodule
