#ifndef COMPATIBILITY
#include "widemem1.h"
#else
#include "libwidemem1.h"
#endif

#include <iostream>
#include <iomanip>
#include <cstring>

// please do not use "using namespace std"
#ifndef COMPATIBILITY
#define DESIGN_DB eCarbonIODB
#define DESIGN_CREATE carbon_design_create
#else
#define DESIGN_DB eCarbonFullDB
#define DESIGN_CREATE carbon_widemem1_create
#endif

int main()
{
  unsigned long i;
  UInt32 val;
  CarbonTime simTime;
  UInt32 bval[3];
  bval[2] = 0;
  bval[1] = 0;
  bval[0] = 0;

  std::cout << "addr\toutput" << std::endl;
  const char* ws = "\t";
  CarbonObjectID* hdl = DESIGN_CREATE(DESIGN_DB, eCarbon_NoFlags);

  CarbonNetID* m_clk = carbonFindNet(hdl, "top.clk");
  CarbonNetID* m_in_en = carbonFindNet(hdl, "top.in_en");
  CarbonNetID* m_in_addr = carbonFindNet(hdl, "top.in_addr");
  CarbonNetID* m_in = carbonFindNet(hdl, "top.in");
  CarbonNetID* m_out = carbonFindNet(hdl, "top.out");
  CarbonNetID* m_out_addr = carbonFindNet(hdl, "top.out_addr");;

  if (! m_clk || ! m_in_en || ! m_in_addr || ! m_in || ! m_out_addr || ! m_out)
    return 1;

  // Write the memory first
  val = 1;
  carbonDeposit(hdl, m_in_en, &val, 0);
  simTime = 0;
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    bval[0] = i + 1;
    bval[1] = i + 1;
    bval[2] = i & 1;
    carbonDeposit(hdl, m_in, bval, 0);
    val = i;
    carbonDeposit(hdl, m_in_addr, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;                   
  }
    
  // Now read the memory
  std::cout << std::hex << std::setfill('0');
  val = 0;
  carbonDeposit(hdl, m_in_en, &val, 0);
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    val = i;
    carbonDeposit(hdl, m_out_addr, &val, 0);
    carbonSchedule(hdl, simTime);
    ++simTime;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);

    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 outAddrBuf;
    UInt32 outBuf[3];
    carbonExamine(hdl, m_out_addr, &outAddrBuf, 0);
    carbonExamine(hdl, m_out, outBuf, 0);

    std::cout << outAddrBuf << ws
              << std::setw(1) << outBuf[2]
              << std::setw(8) << outBuf[1]
              << std::setw(8) << outBuf[0] << std::endl;
  }

  // Let's test to make sure the ordering between read/write edge
  // triggered operations is correct. We shouldn't see the read data
  val = 1;
  carbonDeposit(hdl, m_in_en, &val, 0);
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    bval[0] = i + 2;
    bval[1] = i + 3;
    bval[2] = (i ^ 1) & 1;
    carbonDeposit(hdl, m_in, bval, 0);
    val = i;
    carbonDeposit(hdl, m_in_addr, &val, 0);
    carbonDeposit(hdl, m_out_addr, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;                   

    UInt32 outAddrBuf;
    UInt32 outBuf[3];
    carbonExamine(hdl, m_out_addr, &outAddrBuf, 0);
    carbonExamine(hdl, m_out, outBuf, 0);

    std::cout << outAddrBuf << ws
              << std::setw(1) << outBuf[2]
              << std::setw(8) << outBuf[1]
              << std::setw(8) << outBuf[0] << std::endl;
  }

  // And read it again to make sure the last write worked.
  val = 0;
  carbonDeposit(hdl, m_in_en, &val, 0);
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    val = i;
    carbonDeposit(hdl, m_out_addr, &val, 0);
    carbonSchedule(hdl, simTime);
    ++simTime;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);

    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 outAddrBuf;
    UInt32 outBuf[3];
    carbonExamine(hdl, m_out_addr, &outAddrBuf, 0);
    carbonExamine(hdl, m_out, outBuf, 0);

    std::cout << outAddrBuf << ws
              << std::setw(1) << outBuf[2]
              << std::setw(8) << outBuf[1]
              << std::setw(8) << outBuf[0] << std::endl;
  }

  carbonDestroy(&hdl);
  return 0;
}
