module top (out, clk, out_addr, in, in_en, in_addr);
   output [7:0] out;
   input 	clk, in_en;
   input [7:0] 	in;
   input [6:0]  out_addr, in_addr;

   // Two memories
   reg [7:0] 	mem1 [127:0];
   reg [7:0] 	mem2 [0:127];

   // Sequential blocks in a cycle
   reg [7:0] 	rdata1, rdata2;
   always @ (posedge clk)
     begin
	rdata2 = mem1[out_addr];
	if (in_en)
	  mem1[in_addr] = in;
     end
   always @ (posedge clk)
     begin
	rdata1 = mem2[out_addr];
	if (in_en)
	  mem2[in_addr] = in;
     end

   assign out = rdata1 & rdata2;

endmodule // top
