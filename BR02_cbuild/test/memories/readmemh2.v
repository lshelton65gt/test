module top (out, clk, out_addr, in, in_en, in_addr);
   output [33:0] out;
   input 	clk, in_en;
   input [33:0] 	in;
   input [6:0]  out_addr, in_addr;

   // Funny width memory
   reg [33:0] 	mem [127:0];

   initial begin
      $readmemh("readmemh2.txt", mem);
   end

   // Memory write
   always @ (posedge clk)
     if (in_en)
       mem[in_addr] <= in;

   // Memory read
   reg [33:0] 	out;
   always @ (posedge clk)
     out <= mem[out_addr];

endmodule // top

  
