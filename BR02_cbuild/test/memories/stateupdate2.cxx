#include "stateupdate2.h"

#include <iostream>

// please do not use "using namespace std"

int main()
{
  int i;
  UInt32 val;
  CarbonTime simTime;

  std::cout << "addr\toutput" << std::endl;
  const char* ws = "\t";
  CarbonObjectID* hdl = carbon_design_create(eCarbonIODB, eCarbon_NoFlags);

  CarbonNetID* m_clk = carbonFindNet(hdl, "top.clk");
  CarbonNetID* m_wen1 = carbonFindNet(hdl, "top.wen1");
  CarbonNetID* m_wen2 = carbonFindNet(hdl, "top.wen2");
  CarbonNetID* m_waddr1 = carbonFindNet(hdl, "top.waddr1");
  CarbonNetID* m_waddr2 = carbonFindNet(hdl, "top.waddr2");

  CarbonNetID* m_in1 = carbonFindNet(hdl, "top.in1");
  CarbonNetID* m_in2 = carbonFindNet(hdl, "top.in2");
  CarbonNetID* m_out = carbonFindNet(hdl, "top.out");
  CarbonNetID* m_raddr = carbonFindNet(hdl, "top.raddr");;

  if (! (m_raddr && m_out && m_in2 && m_in1 && m_waddr2 && m_waddr1 && m_wen2 && m_wen1 && m_clk))
    return 1;

  // Write the memory first
  val = 1;
  carbonDeposit(hdl, m_wen1, &val, 0);
  val = 0;
  carbonDeposit(hdl, m_wen2, &val, 0);
  carbonDeposit(hdl, m_waddr2, &val, 0);
  carbonDeposit(hdl, m_raddr, &val, 0);
  simTime = 0;
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);
  
    val = i + 1;
    carbonDeposit(hdl, m_in1, &val, 0);
    val = i;
    carbonDeposit(hdl, m_waddr1, &val, 0);
    
    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;
  }
    
  // Now read the memory
  val = 0;
  carbonDeposit(hdl, m_wen1, &val, 0);
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);
  
    val = i;
    carbonDeposit(hdl, m_raddr, &val, 0);
    
    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 raddrBuf, outBuf;
    carbonExamine(hdl, m_raddr, &raddrBuf, 0);
    carbonExamine(hdl, m_out, &outBuf, 0);

    std::cout << raddrBuf << ws
              << outBuf << ws << std::endl;
  }

  // Let's test to make sure the ordering between read/write edge
  // triggered operations is correct. We shouldn't see the read data
  val = 1;
  carbonDeposit(hdl, m_wen1, &val, 0);
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);
  
    val = i + 2;
    carbonDeposit(hdl, m_in1, &val, 0);
    val = i;
    carbonDeposit(hdl, m_waddr1, &val, 0);
    carbonDeposit(hdl, m_raddr, &val, 0);
    
    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 raddrBuf, outBuf;
    carbonExamine(hdl, m_raddr, &raddrBuf, 0);
    carbonExamine(hdl, m_out, &outBuf, 0);

    std::cout << raddrBuf << ws
              << outBuf << ws << std::endl;
  }

  // And read it again to make sure the last write worked.
  val = 0;
  carbonDeposit(hdl, m_wen1, &val, 0);
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);
  
    val = i;
    carbonDeposit(hdl, m_raddr, &val, 0);
    
    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 raddrBuf, outBuf;
    carbonExamine(hdl, m_raddr, &raddrBuf, 0);
    carbonExamine(hdl, m_out, &outBuf, 0);

    std::cout << raddrBuf << ws
              << outBuf << ws << std::endl;
  }

  // Now write through two ports at the same time
  val = 1;
  carbonDeposit(hdl, m_wen1, &val, 0);
  carbonDeposit(hdl, m_wen2, &val, 0);
  for (i = 0; i < 64; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);
  
    val = i + 3;
    carbonDeposit(hdl, m_in1, &val, 0);
    val = i + 67;
    carbonDeposit(hdl, m_in2, &val, 0);
    val = i;
    carbonDeposit(hdl, m_waddr1, &val, 0);
    val = i + 64;
    carbonDeposit(hdl, m_waddr2, &val, 0);
    
    carbonSchedule(hdl, simTime);
    simTime++;
    
    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;
  }

  val = 0;
  carbonDeposit(hdl, m_wen1, &val, 0);
  carbonDeposit(hdl, m_wen2, &val, 0);
  // Now read the memory again
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);
  
    val = i;
    carbonDeposit(hdl, m_raddr, &val, 0);
    
    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 raddrBuf, outBuf;
    carbonExamine(hdl, m_raddr, &raddrBuf, 0);
    carbonExamine(hdl, m_out, &outBuf, 0);

    std::cout << raddrBuf << ws
              << outBuf << ws << std::endl;
  }

  carbonDestroy(&hdl);
  return 0;
}
