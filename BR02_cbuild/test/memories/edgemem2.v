module top (out, clk, out_addr, in, in_en, in_addr);
   output [7:0] out;
   input 	clk, in_en;
   input [6:0] 	out_addr, in_addr;
   input [7:0] 	in;

   reg [7:0] 	mem [127:0];

   // Memory write
   always @ (posedge clk)
     if (in_en)
       mem[in_addr] = in;

   // Memory read
   wire [7:0] 	out;
   assign out = mem[out_addr];

endmodule // top
