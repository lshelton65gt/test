// testcase from bug 6254, there are were two issues here
// 1. if ADDR_WIDTH is 28 then we would error out with memory being too large
// 2. with ADDR_WIDTH at 26 we get sparse memories but not at 27

module mem(clk, addr, we, wmask, din, dout);
   parameter ADDR_WIDTH = 28;
   parameter DEPTH = 1 << ADDR_WIDTH;

   input     clk;
   input [ADDR_WIDTH-1:0] addr;
   input                  we;
   input [2:0]            wmask;
   input [63:0]           din;
   output [63:0]          dout;

   reg [15:0]             mem0 [0:DEPTH-1]; // DEPTH words (16 bits each word)
   reg [15:0]             mem1 [1:DEPTH]; // DEPTH words (16 bits each word)
   reg [31:0] 		  mem2 [0:3];

   always @(posedge clk)
     if (we) begin
        if (wmask[0])
          mem0[addr] <= din[15:0];
        if (wmask[1])
          mem1[addr] <= din[31:16];
	if (wmask[2])
	  mem2[addr[1:0]] <= din[63:32];
     end

   assign dout = {mem2[addr[1:0]],mem1[addr],mem0[addr]};
endmodule
