module top (out, clk, out_addr, in, in_en, in_addr);
   output [7:0] out;
   input 	clk, in_en;
   input [7:0] 	in;
   input [7:0]  out_addr, in_addr;

   // Reversed range and non-zero start
   reg [7:0] 	mem [128:255];

   initial begin
      $readmemh("readmemh3.txt", mem);
   end

   // Memory write
   always @ (posedge clk)
     if (in_en)
       mem[in_addr] <= in;

   // Memory read
   reg [7:0] 	out;
   always @ (posedge clk)
     out <= mem[out_addr];

endmodule // top

  
