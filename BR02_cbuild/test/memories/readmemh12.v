// last mod: Fri Mar 25 09:53:27 2005
// filename: test/memories/readmemh12.v
// Description:  This test loads a memory via a hierarchical reference, and then
// reads and writes it via hierarchical refs to check that values were loaded properly.

module top (out, clk, out_addr, in, in_en, in_addr);
   output [7:0] out;
   input 	clk, in_en;
   input [7:0] 	in;
   input [6:0]  out_addr, in_addr;


   midread   imidread(out, clk, out_addr);
   midwrite imidwrite(clk, in, in_en, in_addr);


endmodule // top

  
module midread (out, clk, out_addr);
   output [7:0] out;
   input 	clk;
   input [6:0]  out_addr;

   reader ireader(out, clk, out_addr);
endmodule

module reader(out, clk, out_addr);
   output [7:0] out;
   input 	clk;
   input [6:0]  out_addr;

   initial begin
      $readmemh("readmemh12.txt", top.imidwrite.mem);
   end
   
   // Memory read
   reg [7:0] 	out;
   always @ (posedge clk)
     out <= top.imidwrite.mem[out_addr];

endmodule


module midwrite(clk, in, in_en, in_addr);
   input 	clk, in_en;
   input [7:0] 	in;
   input [6:0]  in_addr;

   reg [7:0] 	mem [127:0];	// the memory is located here

   writer iwriter(clk, in, in_en, in_addr);
endmodule

module writer(clk, in, in_en, in_addr);
   input 	clk, in_en;
   input [7:0] 	in;
   input [6:0]  in_addr;


   // Memory write
   always @ (posedge clk)
     if (in_en)
       top.imidwrite.mem[in_addr] <= in;

endmodule
