#ifndef COMPATIBILITY
#include "readmemb3.h"
#else
#include "libreadmemb3.h"
#endif
#include <iostream>
#include <iomanip>

// please do not use "using namespace std"
#ifndef COMPATIBILITY
#define DESIGN_DB eCarbonIODB
#define DESIGN_CREATE carbon_design_create
#else
#define DESIGN_DB eCarbonFullDB
#define DESIGN_CREATE carbon_readmemb3_create
#endif

int main()
{
  int i;
  UInt32 val;
  UInt32 bval[2];
  bval[1] = 0;
  bval[0] = 0;

  CarbonTime simTime;

  std::cout << "addr\toutput" << std::endl;
  const char* ws = "\t";
  CarbonObjectID* hdl = DESIGN_CREATE(DESIGN_DB, eCarbon_NoFlags);

  CarbonNetID* m_clk = carbonFindNet(hdl, "top.clk");
  CarbonNetID* m_in_en = carbonFindNet(hdl, "top.in_en");
  CarbonNetID* m_in_addr = carbonFindNet(hdl, "top.in_addr");
  CarbonNetID* m_in = carbonFindNet(hdl, "top.in");
  CarbonNetID* m_out = carbonFindNet(hdl, "top.out");
  CarbonNetID* m_out_addr = carbonFindNet(hdl, "top.out_addr");;

  if (! m_clk || ! m_in_en || ! m_in_addr || ! m_in || ! m_out_addr || ! m_out)
    return 1;

  // Read the memory
  val = 0;
  carbonDeposit(hdl, m_in_en, &val, 0);
  simTime = 0;
  std::cout << std::hex << std::setfill('0');
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    val = i;
    carbonDeposit(hdl, m_out_addr, &val, 0);
    carbonSchedule(hdl, simTime);
    ++simTime;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);

    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 outAddrBuf;
    UInt32 outBufTmp[2];
    carbonExamine(hdl, m_out_addr, &outAddrBuf, 0);
    carbonExamine(hdl, m_out, outBufTmp, 0);

    UInt64 outBuf = outBufTmp[1];
    outBuf <<= 32;
    outBuf += outBufTmp[0];

    std::cout << std::setw(2) << outAddrBuf << ws
              << std::setw(16) << outBuf << std::endl;
  }
  
  // Overwrite the readmem data
  val = 1;
  carbonDeposit(hdl, m_in_en, &val, 0);
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    bval[1] = 0x12345678;
    bval[0] = 0x9ABCD000 + i + 1;
    carbonDeposit(hdl, m_in, bval, 0);

    val = i;
    carbonDeposit(hdl, m_in_addr, &val, 0);

    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;                   
  }
    
  // Now read the memory again
  val = 0;
  carbonDeposit(hdl, m_in_en, &val, 0);
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    val = i;
    carbonDeposit(hdl, m_out_addr, &val, 0);
    carbonSchedule(hdl, simTime);
    ++simTime;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);

    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 outAddrBuf;
    UInt32 outBufTmp[2];
    carbonExamine(hdl, m_out_addr, &outAddrBuf, 0);
    carbonExamine(hdl, m_out, outBufTmp, 0);

    UInt64 outBuf = outBufTmp[1];
    outBuf <<= 32;
    outBuf += outBufTmp[0];

    std::cout << std::setw(2) << outAddrBuf << ws
              << std::setw(16) << outBuf << std::endl;
  }
  
  carbonDestroy(&hdl);
  return 0;
}
