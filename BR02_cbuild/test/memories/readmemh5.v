module top (out, clk, out_addr, in, in_en, in_addr);
   output [7:0] out;
   input 	clk, in_en;
   input [7:0] 	in;
   input [6:0]  out_addr, in_addr;

   reg [7:0] 	mem [127:0];

   initial begin
      // Reverse reading
      $readmemh("readmemh5.txt", mem, 0, 127);
   end

   // Memory write
   always @ (posedge clk)
     if (in_en)
       mem[in_addr] <= in;

   // Memory read
   reg [7:0] 	out;
   always @ (posedge clk)
     out <= mem[out_addr];

endmodule // top

  
