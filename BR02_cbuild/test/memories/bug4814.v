//`define MEM_SIZE 16777216
`define MEM_SIZE   1000000

module mem_init(out, clk, reset);
output [7:0] out;
input clk;
input reset;
reg [7:0] out;

reg [7:0] mem [0:`MEM_SIZE-1];
integer i;

reg init_done;

task init;
begin
  for( i=0; i < `MEM_SIZE; i=i+1 ) begin
    mem[i] <= 0;
  end
  init_done = 1;
end
endtask

initial begin
  init_done = 0;
end

always@( posedge clk ) begin
  if( init_done == 0 )
    init;

  mem[1] <= mem[0];
  out=mem[0];
end

endmodule
