#include "stateupdate4.h"

#include <iostream>
#include <cstring>

// please do not use "using namespace std"

int main()
{
  int i;
  UInt32 val;
  CarbonTime simTime;

  std::cout << "addr\toutput" << std::endl;
  const char* ws = "\t";
  CarbonObjectID* hdl = carbon_design_create(eCarbonIODB, eCarbon_NoFlags);

  CarbonNetID* m_clk = carbonFindNet(hdl, "top.clk");
  CarbonNetID* m_in_en = carbonFindNet(hdl, "top.in_en");
  CarbonNetID* m_in_addr = carbonFindNet(hdl, "top.in_addr");
  CarbonNetID* m_in = carbonFindNet(hdl, "top.in");
  CarbonNetID* m_out1 = carbonFindNet(hdl, "top.out1");
  CarbonNetID* m_out2 = carbonFindNet(hdl, "top.out2");
  CarbonNetID* m_out_addr = carbonFindNet(hdl, "top.out_addr");;

  if (! m_clk || ! m_in_en || ! m_in_addr || ! m_in || ! m_out_addr || ! m_out1 || ! m_out2)
    return 1;

  // Write the memory first
  val = 1;
  carbonDeposit(hdl, m_in_en, &val, 0);
  simTime = 0;
  for (i = 0; i < 128; i += 2)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);
    val = i + 1;
    carbonDeposit(hdl, m_in, &val, 0);
    val = i;
    carbonDeposit(hdl, m_in_addr, &val, 0);

    carbonSchedule(hdl, simTime);
    simTime++;
    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;                   
  }
    
  // Now read the memory
  val = 0;
  carbonDeposit(hdl, m_in_en, &val, 0);
  for (i = 0; i < 128; i += 2)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    val = i;
    carbonDeposit(hdl, m_out_addr, &val, 0);
    carbonSchedule(hdl, simTime);
    ++simTime;
    
    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);

    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 outAddrBuf, out1Buf, out2Buf;
    carbonExamine(hdl, m_out_addr, &outAddrBuf, 0);
    carbonExamine(hdl, m_out1, &out1Buf, 0);
    carbonExamine(hdl, m_out2, &out2Buf, 0);

    std::cout << outAddrBuf << ws << out1Buf << "\n";
    std::cout << outAddrBuf+1 << ws << out2Buf << "\n";
  }

  // Let's test to make sure the ordering between read/write edge
  // triggered operations is correct. We shouldn't see the read data
  val = 1;
  carbonDeposit(hdl, m_in_en, &val, 0);
  for (i = 0; i < 128; i += 2)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    val = i + 2;
    carbonDeposit(hdl, m_in, &val, 0);

    val = i;
    carbonDeposit(hdl, m_in_addr, &val, 0);
    carbonDeposit(hdl, m_out_addr, &val, 0);

    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 outAddrBuf, out1Buf, out2Buf;
    carbonExamine(hdl, m_out_addr, &outAddrBuf, 0);
    carbonExamine(hdl, m_out1, &out1Buf, 0);
    carbonExamine(hdl, m_out2, &out2Buf, 0);

    std::cout << outAddrBuf << ws << out1Buf << "\n";
    std::cout << outAddrBuf+1 << ws << out2Buf << "\n";
  }

  // And read it again to make sure the last write worked.
  val = 0;
  carbonDeposit(hdl, m_in_en, &val, 0);
  for (i = 0; i < 128; i += 2)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    val = i;
    carbonDeposit(hdl, m_out_addr, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 outAddrBuf, out1Buf, out2Buf;
    carbonExamine(hdl, m_out_addr, &outAddrBuf, 0);
    carbonExamine(hdl, m_out1, &out1Buf, 0);
    carbonExamine(hdl, m_out2, &out2Buf, 0);

    std::cout << outAddrBuf << ws << out1Buf << "\n";
    std::cout << outAddrBuf+1 << ws << out2Buf << "\n";
  }

  carbonDestroy(&hdl);
  return 0;
}
