// filename: test/memories/readmemh_order_02.v
// Description:  This test checks to see what order is used by readmemh when the
// memory is declared in different address orders and the data file does not
// contain addresses.
// The Verilog LRM states that when doing a readmemh and no address is specified
// in the readmemh command or in the data file then the load should start at the
// left address and proceed to the right address.
// it has been observed that mti and nc both ignore this, and instead
// start at the low address (-4 in this testcase) and proceed to the high
// address.
// aldec appears to start at location 0 and proceed to the high address, and
// thus is even more wrong.

// gold file is from nc and mti (NOTE aldec results are wrong but happen to match carbon as of Mon May 14 11:04:57 2007)

// this is for bug7321
// see also readmemh_order_01.v  for the range 0:7

module readmemh_order_02(clk, addr, out1, out2);
   input signed [2:0] addr;
   input clk;
   
   output [2:0] out1, out2;
   reg [2:0]    out1, out2;
   
   reg [2:0] mem_downto [3:-4];
   reg [2:0] mem_to [-4:3];

   initial
      begin
	 // use the same memory data file for both memories.
	 // Since the data file does not contain addresses the LRM says that the
	 // first data value should be loaded into the left most address, thus
	 // we expect that (mem_downto[N] != mem_to[N]).
	 $readmemh("readmemh_order_02.dat", mem_downto); // loc -4 should have the value 7
	 $readmemh("readmemh_order_02.dat", mem_to);     // loc -4 should have the value 0
      end

   always @(posedge clk)
      begin
	 out1 = mem_downto[addr];
	 out2 = mem_to[addr];
      end
endmodule
