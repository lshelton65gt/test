module top (out1, out2, clk, out_addr1, out_addr2, in, in_en, in_addr);
   output [7:0] out1, out2;
   input 	clk, in_en;
   input [6:0] 	out_addr1, out_addr2, in_addr;
   input [7:0] 	in;

   reg [7:0] 	mem [127:0];

   // Memory write
   always @ (posedge clk)
     if (in_en)
       mem[in_addr] <= in;

   // Memory read
   reg [7:0] 	out1;
   always @ (posedge clk)
     out1 <= mem[out_addr1];

   // Memory read
   assign out2 = mem[out_addr2];

endmodule // top

  
