// filename: test/memories/readmemh_order_01.v
// Description:  This test checks to see what order is used by readmemh when the
// memory is declared in different address orders and the data file does not
// contain addresses.
// The Verilog LRM states that when doing a readmemh and no address is specified
// in the readmemh command or in the data file then the load should start at the
// left address and proceed to the right address.
// it has been observed that aldec, mti and nc all ignore this, and instead
// start at location 0 and proceed to the high address.

// this is for bug7321

// see also readmemh_order_02.v for the range [3:-4]

module readmemh_order_01(clk, addr, out1, out2);
   input [2:0] addr;
   input clk;
   
   output [2:0] out1, out2;
   reg [2:0]    out1, out2;
   
   reg [2:0] mem_downto [7:0];
   reg [2:0] mem_to [0:7];

   initial
      begin
	 // use the same memory data file for both memories.
	 // Since the data file does not contain addresses the LRM says that the
	 // first data value should be loaded into the left most address, thus
	 // we expect that (mem_downto[N] != mem_to[N]).
	 // the fact is that aldec, mti, and nc all load the memory starting at
	 // location 0, ignoring the LRM standard.
	 $readmemh("readmemh_order_01.dat", mem_downto); // loc 0 should have the value 7
	 $readmemh("readmemh_order_01.dat", mem_to);     // loc 0 should have the value 0
      end

   always @(posedge clk)
      begin
	 out1 = mem_downto[addr];
	 out2 = mem_to[addr];
      end
endmodule
