module top(clk,a,b,a1,a2,d1,d2,out);
   parameter N=256;
   input     clk,a,b;
   input [7:0] a1,a2;
   input [31:0] d1,d2;
   output    [31:0] out;
   integer   i;
   reg [31:0] m1 [N-1:0];
   reg [31:0] m2 [N-1:0];
   always @(posedge clk) begin
      if (a) begin
	 for (i=0;i<N;i=i+1) begin
	    m1[i]<=32'b0;
	 end
      end else begin
	 m2[a1] <= d1;
	 m1[a1] <= m2[a1];
      end
      if (b) begin
	 m1[a2] <= d2;
	 m2[a2] <= m1[a2];
      end else begin
	 for (i=0;i<N;i=i+1) begin
	    m2[i]<=32'b0;
	 end
      end
   end

   assign out = m1[a1] | m2[a2];
endmodule
