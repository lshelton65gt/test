/*
 This tests that the memory file reader does not use global variables. 
 A re-entrant implementation of the lexer was created by Mark S. 
 
 The runtime gold file for this test was created using aldec with the 
 'too many addresses' warning added so it would compare.
 
 The main.cxx of testdriver uses an api function which uses the readmem reader.
 This happens after initialization of the design, during which an incomplete 
 readmem is done on the memory. The non-reentrant lex kept information in the 
 global buffer which ended up in the data read in by main.cxx. The re-entrant 
 lex properly cleans up after itself.
 */
module top(in, clk, out);
   input [2:0] in;
   input clk;
   
   output [2:0] out;
   reg [2:0]    out;
   
   reg [2:0] mem [7:4];

   initial
     $readmemh("bug4492.dat", mem, 4, 6);

   always @(posedge clk)
     out <= mem[in];
endmodule // top
