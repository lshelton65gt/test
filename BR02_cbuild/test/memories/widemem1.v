module top (out, clk, out_addr, in, in_en, in_addr);
   output [64:0] out;
   input 	clk, in_en;
   input [64:0] 	in;
   input [6:0]  out_addr, in_addr;

   reg [64:0] 	mem [127:0];

   // Memory write
   always @ (posedge clk)
     if (in_en)
       mem[in_addr] <= in;

   // Memory read
   reg [64:0] 	out;
   always @ (posedge clk)
     out <= mem[out_addr];

endmodule // top

  
