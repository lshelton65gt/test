module top (out1, out2, clk, out_addr, in, in_en, in_addr);
   output [7:0] out1, out2;
   input 	clk, in_en;
   input [7:0] 	in;
   input [6:0]  out_addr, in_addr;

   // Two memories
   reg [7:0] 	mem1 [127:0];
   reg [7:0] 	mem2 [0:127];

   // Sequential blocks in a cycle
   reg [7:0] 	rdata1, rdata2;
   reg [7:0] 	rdata3, rdata4;
   integer 	i;
   always @ (posedge clk)
     begin
	if (in_en)
	  for (i = 0; i < 2; i = i + 1)
	    mem1[in_addr+i] = in + i;
	rdata1 = mem2[out_addr];
	rdata3 = mem2[out_addr+1];
     end
   always @ (posedge clk)
     begin
	if (in_en)
	  for (i = 0; i < 2; i = i + 1)
	    mem2[in_addr+i] = in + i;
	rdata2 = mem1[out_addr];
	rdata4 = mem1[out_addr+1];
     end

   assign out1 = rdata1 & rdata2;
   assign out2 = rdata3 & rdata4;

endmodule // top
