module top (out1, out2, clk, out_addr1, out_addr2, in1, in2, in_en1, in_en2,
	    in_addr1, in_addr2);
   output [7:0] out1, out2;
   input 	clk, in_en1, in_en2;
   input [6:0] 	out_addr1, out_addr2, in_addr1, in_addr2;
   input [7:0] 	in1, in2;

   reg [7:0] 	mem [127:0];

   // Memory write
   always @ (posedge clk)
     if (in_en1)
       mem[in_addr1] <= in1;

   // Memory write
   always @ (posedge clk)
     if (in_en2)
       mem[in_addr2] <= in2;

   // Memory read
   reg [7:0] 	out1;
   always @ (posedge clk)
     out1 <= mem[out_addr1];

   // Memory read
   assign out2 = mem[out_addr2];

endmodule // top

  
