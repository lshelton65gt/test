module top(clk,small_raddr,small_waddr,small_data,small_out,big_raddr,big_waddr,big_data,big_out);
   input clk;

   parameter SMALL_ADDR_BITS = 12;
   parameter SMALL_DATA_BITS = 128;
   parameter SMALL_MEM_ROWS  = (1 << SMALL_ADDR_BITS);

   parameter BIG_ADDR_BITS = 16;
   parameter BIG_DATA_BITS = 1024;
   parameter BIG_MEM_ROWS  = (1 << BIG_ADDR_BITS);

   input [(SMALL_ADDR_BITS-1):0] small_raddr;
   input [(SMALL_ADDR_BITS-1):0] small_waddr;
   input [(SMALL_DATA_BITS-1):0] small_data;
   output [(SMALL_DATA_BITS-1):0] small_out;
   input [(BIG_ADDR_BITS-1):0] big_raddr;
   input [(BIG_ADDR_BITS-1):0] big_waddr;
   input [(BIG_DATA_BITS-1):0] big_data;
   output [(BIG_DATA_BITS-1):0] big_out;

   // expect non-sparse by default.
   reg [(SMALL_DATA_BITS-1):0] small_mem [0:(SMALL_MEM_ROWS-1)];

   // expect sparse by default.
   reg [(BIG_DATA_BITS-1):0] big_mem [0:(BIG_MEM_ROWS-1)];

   always @(posedge clk)
     small_mem[small_waddr] = small_data;
   always @(posedge clk)
     big_mem[big_waddr] = big_data;

   assign small_out = small_mem[small_raddr];
   assign big_out = big_mem[big_raddr];
endmodule
