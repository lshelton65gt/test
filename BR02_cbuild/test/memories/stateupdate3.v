module top (out, clk, out_addr, in, in_en, in_addr);
   output [7:0] out;
   input 	clk, in_en;
   input [7:0] 	in;
   input [7:0]  out_addr, in_addr;

   // Two memories
   reg [7:0] 	mem1 [255:128];
   reg [7:0] 	mem2 [128:255];

   // Sequential blocks in a cycle
   reg [7:0] 	rdata1, rdata2;
   always @ (posedge clk)
     begin
	if (in_en)
	  mem1[in_addr] = in;
	rdata1 = mem2[out_addr];
     end
   always @ (posedge clk)
     begin
	if (in_en)
	  mem2[in_addr] = in;
	rdata2 = mem1[out_addr];
     end

   assign out = rdata1 & rdata2;

endmodule // top
