// filename: test/memories/mem1.v
// Description:  This test is a test that detected a sizing problem in the verific flow.
// self determined size for the index expression on the LHS was wrong.


module mem1(clock, dat1, in1, in2, out1) ;
   input clock;
   input [34:0] dat1;
   input [1:0] in1, in2;
   output [34:0] out1;
   reg [34:0] out1 ;
   reg [34:0] mem1 [7:0] ;	// note address range is 7:0, which requires 3 bits

   always @(posedge clock)
     begin: main
	mem1[in1 + in2 + 5'b0] = dat1; // calculate address using 5 bits
	out1 = 	mem1[in1 + in2 ]; // calcualte address using 3 bits
     end
endmodule
