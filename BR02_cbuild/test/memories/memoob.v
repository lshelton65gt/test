// test memory out-of-bounds

module memcost(r,w,d,a,o);
   input r,w;
   input [31:0] d;
   input [31:0]  a;
   output [31:0] o;
   reg [31:0]    o;
   reg [31:0]    mem [400000:400001]; // scary offset
   initial
     begin
        mem [400000] = 32'hdeadbeef;
        mem [400001] = 32'hfeedface;
     end
   

   always @(posedge r)
     o = mem[a];

   always @(posedge w)
     mem[a] = d;
endmodule

module top(c,i,j,o,x);
   input c;
   input [3:0] i;
   input [31:0] j;
   output [31:0] o,x;
   memcost mymem(c, !c, j, 400001-i, o); // some OOB, some inbound
   memcost crash(c, !c, j, -32'd1, x); // way out-of-bounds!
endmodule

   
