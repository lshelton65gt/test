module top (out, clk, out_addr, in, in_en, in_addr);
   output [31:0] out;
   input 	 clk, in_en;
   input [7:0]  in;
   input [5:0] 	 in_addr, out_addr;

   reg [31:0] 	 mem [63:0];

   // Truncate the writes to a memory
   always @ (posedge clk)
     if (in_en)
       mem[in_addr] = in;

   // Zero extend the reads from a memory
   assign 	 out = mem[out_addr];

endmodule // top
