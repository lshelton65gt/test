// last mod: Wed Jul 26 10:10:08 2006
// filename: test/memories/bug6254_2.v
// Description:  This test shows that carbon does not support memories that have
// a large address space but are still smaller than 2**24 bits  (bug 6406)
// SECTION 3.10 of the LRM says about the minimum array size:
//    Implementations may limit the maximum size of an array, but they shall at least be 16777216 (2**24).


module bug6254_2(clock, addr, data, out3);
   input clock;
   input [511:0] data;
   input [26:1] addr;
   output [511:0] out3;
   reg [511:0] 	 out3;

   reg [511:0]            memE [1:(1<<23)];

   always @(posedge clock)
     begin: main
	memE[addr+1] = data;
	out3 = {memE[addr+1]};
     end
endmodule
