module top (out, clk, out_addr, in, in_en, in_addr);
   output [63:0] out;
   input 	clk, in_en;
   input [63:0] 	in;
   input [6:0]  out_addr, in_addr;

   reg [63:0] 	mem [127:0];

   initial begin
      $readmemb("readmemb3.txt", mem);
   end

   // Memory write
   always @ (posedge clk)
     if (in_en)
       mem[in_addr] <= in;

   // Memory read
   reg [63:0] 	out;
   always @ (posedge clk)
     out <= mem[out_addr];

endmodule // top

  
