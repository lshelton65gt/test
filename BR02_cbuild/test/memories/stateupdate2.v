module top (out, clk, raddr, in1, in2, wen1, wen2, waddr1, waddr2);
   output [7:0] out;
   input 	clk, wen1, wen2;
   input [7:0] 	in1, in2;
   input [6:0]  raddr, waddr1, waddr2;

   // Two memories
   reg [7:0] 	mem1 [127:0];
   reg [7:0] 	mem2 [0:127];

   // Sequential blocks in a cycle
   reg [7:0] 	rdata1, rdata2;
   always @ (posedge clk)
     begin
	if (wen1)
	  mem1[waddr1] = in1;
	rdata1 = mem2[raddr];
     end
   always @ (posedge clk)
     begin
	if (wen1)
	  mem2[waddr1] = in1;
	rdata2 = mem1[raddr];
     end

   // Add a couple of other memory write blocks. One of these will be
   // double buffered because of the cycle above
   always @ (posedge clk)
     if (wen2)
       mem2[waddr2] = in2;
   always @ (posedge clk)
     if (wen2)
       mem1[waddr2] = in2;

   // Read the data back out, it should be identical between both memories
   assign out = rdata1 & rdata2;

endmodule // top
