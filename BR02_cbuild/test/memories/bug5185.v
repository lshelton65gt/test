module top(d0, d1,
           clk, rd, wr0, wr1, ctl,
           o0, o1);
   input [63:0] d0, d1;
   input        clk, rd, wr0, wr1;
   output [63:0] o0, o1;
   reg [63:0]    o0, o1;
   output [7:0] ctl;
   reg [7:0]    ctl;

   reg [63:0] mem_out [1:0];

   reg [32:0] mem_in [3:0];

   integer    i;

   initial ctl = 0;
   
   always @(posedge clk)
     begin
        {mem_in[1],mem_in[0]} <= d0;
        {mem_in[3],mem_in[2]} <= d1;
     end

   always @(posedge clk)
     begin
        if (wr0)
          begin
             mem_out[0][63:32] <= mem_in[1];
             mem_out[0][31:0] <= mem_in[0];
             ctl = ctl + 1;
          end
        if (rd)
          begin
             ctl = ctl + 1;
             o0 = mem_out[0];
             o1 = mem_out[1];
          end
        if (wr1)
          begin
             ctl = ctl + 1;
             mem_out[1][63:32] <= mem_in[3];
             mem_out[1][31:0] <= mem_in[2];
          end
     end
endmodule
