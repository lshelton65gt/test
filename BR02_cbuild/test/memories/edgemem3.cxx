#include "edgemem3.h"

#include <iostream>

// please do not use "using namespace std"

int main()
{
  int i;
  UInt32 val;
  CarbonTime simTime;

  std::cout << "addr1\toutput1\taddr2\toutput2" << std::endl;
  const char* ws = "\t";
  CarbonObjectID* hdl = carbon_design_create(eCarbonIODB, eCarbon_NoFlags);

  CarbonNetID* m_clk = carbonFindNet(hdl, "top.clk");
  CarbonNetID* m_in_en = carbonFindNet(hdl, "top.in_en");
  CarbonNetID* m_in_addr = carbonFindNet(hdl, "top.in_addr");
  CarbonNetID* m_in = carbonFindNet(hdl, "top.in");
  CarbonNetID* m_out1 = carbonFindNet(hdl, "top.out1");
  CarbonNetID* m_out2 = carbonFindNet(hdl, "top.out2");
  CarbonNetID* m_out_addr1 = carbonFindNet(hdl, "top.out_addr1");;
  CarbonNetID* m_out_addr2 = carbonFindNet(hdl, "top.out_addr2");;

  if (! m_clk || ! m_in_en || ! m_in_addr || ! m_in || ! m_out_addr1 || ! m_out_addr2 || ! m_out1 || ! m_out2)
    return 1;

  // Write the memory first
  val = 1;
  carbonDeposit(hdl, m_in_en, &val, 0);
  simTime = 0;
  for (i = 0; i < 128; ++i)
  {
    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    val = i + 1;
    carbonDeposit(hdl, m_in, &val, 0);
    val = i;
    carbonDeposit(hdl, m_in_addr, &val, 0);

    carbonSchedule(hdl, simTime);
    simTime++;
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;                   
  }
    
  // Now read the memory from both ports in reverse directions
  val = 0;
  carbonDeposit(hdl, m_in_en, &val, 0);
  for (i = 0; i < 128; ++i)
  {
    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    val = i;
    carbonDeposit(hdl, m_out_addr1, &val, 0);
    val = 127 - i;
    carbonDeposit(hdl, m_out_addr2, &val, 0);
    carbonSchedule(hdl, simTime);
    ++simTime;

    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 outAddr1Buf, outAddr2Buf, out1Buf, out2Buf;
    carbonExamine(hdl, m_out_addr1, &outAddr1Buf, 0);
    carbonExamine(hdl, m_out1, &out1Buf, 0);
    carbonExamine(hdl, m_out_addr2, &outAddr2Buf, 0);
    carbonExamine(hdl, m_out2, &out2Buf, 0);

    std::cout << outAddr1Buf << ws
              << out1Buf << ws
              << outAddr2Buf << ws
              << out2Buf << ws << std::endl;
  }

  // Let's test to make sure the ordering between read/write operations
  // is correct. The edge triggered read should see the old data and the
  // continous assignment should see the new data.
  val = 1;
  carbonDeposit(hdl, m_in_en, &val, 0);
  for (i = 0; i < 128; ++i)
  {
    val = 0;
    carbonDeposit(hdl, m_clk, &val, 0);

    val = i + 2;
    carbonDeposit(hdl, m_in, &val, 0);

    val = i;
    carbonDeposit(hdl, m_in_addr, &val, 0);
    carbonDeposit(hdl, m_out_addr1, &val, 0); 
    carbonDeposit(hdl, m_out_addr2, &val, 0); 

    carbonSchedule(hdl, simTime);
    simTime++;

    val = 1;
    carbonDeposit(hdl, m_clk, &val, 0);
    carbonSchedule(hdl, simTime);
    simTime++;

    UInt32 outAddr1Buf, outAddr2Buf, out1Buf, out2Buf;
    carbonExamine(hdl, m_out_addr1, &outAddr1Buf, 0);
    carbonExamine(hdl, m_out1, &out1Buf, 0);
    carbonExamine(hdl, m_out_addr2, &outAddr2Buf, 0);
    carbonExamine(hdl, m_out2, &out2Buf, 0);

    std::cout << outAddr1Buf << ws
              << out1Buf << ws
              << outAddr2Buf << ws
              << out2Buf << ws << std::endl;
  }

  carbonDestroy(&hdl);
  return 0;
}
