LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

-- not_test6a translate_off
library vh_and_test6a;
-- not_test6a translate_on

entity top is
  port (
    clk : in std_ulogic;
    in1 : in std_logic_vector(31 downto 0);
    in2 : in std_logic_vector(31 downto 0);
    out_top: out std_logic_vector(31 downto 0)
    );
end top;

architecture test_arch of top is

  component vhand
    port (
      clk       : in  std_ulogic;
      in1       : in  std_logic_vector(15 downto 0);
      in2       : in  std_logic_vector(15 downto 0);
      out_vhand : out std_logic_vector(15 downto 0));
  end component;

  component vhor
    port (
      clk      : in  std_ulogic;
      in1      : in  std_logic_vector(15 downto 0);
      in2      : in  std_logic_vector(15 downto 0);
      out_vhor : out std_logic_vector(15 downto 0));
  end component;
  
begin

  vh1 : vhand port map (
    clk       => clk,
    in1       => in1(15 downto 0),
    in2       => in2(15 downto 0),
    out_vhand => out_top(15 downto 0));

  vh2 : vhor port map (
    clk      => clk,
    in1      => in1(31 downto 16),
    in2      => in2(31 downto 16),
    out_vhor => out_top(31 downto 16));

end test_arch;
