module upDown1(clk,up,down,data_in,parity_out,carry_out,borrow_out, count_out);
  input clk,up,down;
  input [8:0] data_in;
  output parity_out, carry_out, borrow_out;
  output [8:0] count_out;

  reg parity_out, carry_out, borrow_out;
  reg [8:0] count_out;
  reg [8:0] count_nxt;
  reg [9:0] cnt_up, cnt_dn;
  reg load;

  always @(posedge clk) 
  begin
    cnt_dn = count_out - 5;
    cnt_up = count_out + 3;
    load = 1;
    case ({up,down})
      0: count_nxt = data_in;
      1: count_nxt = cnt_dn;
      2: count_nxt = cnt_up;
      3: load = 0;
    endcase
    if(load) begin
      parity_out <= ^count_nxt;
      carry_out <= up & cnt_up[9];
      borrow_out <= down & cnt_dn[9];
      count_out <= count_nxt;
    end
  end
endmodule
