LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

entity vh1 is
  port (
    clk : in std_ulogic;
    in1 : in std_logic_vector(15 downto 0);
    in2 : in std_logic_vector(15 downto 0);
    out_vh: out std_logic_vector(15 downto 0)
    );
end vh1;

architecture test_arch of vh1 is

  component veor1
    port (
      clk      : in  std_ulogic;
      in1      : in  std_logic_vector(15 downto 0);
      in2      : in  std_logic_vector(15 downto 0);
      out_veor : out std_logic_vector(15 downto 0));
  end component;
  
begin

  veor : veor1 port map (
    clk      => clk,
    in1      => in1,
    in2      => in2,
    out_veor => out_vh);

end test_arch;
