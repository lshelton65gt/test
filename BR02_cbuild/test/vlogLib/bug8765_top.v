module top_v(pin, pout);
   input  pin;
   output pout;

   wire   tmp;

   foo_v f0(pin, tmp);
   bar_v b0(tmp, pout);

endmodule
