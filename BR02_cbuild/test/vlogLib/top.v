`timescale 1ps / 1fs

module top (clk, w1_in1, w1_in2, w1_out1, w1_out2, w2_in1, w2_in2, w2_out1, w2_out2);
input clk;
input w1_in1;
input w1_in2;
output w1_out1;
output w1_out2;
input w2_in1;
input w2_in2;
output w2_out1;
output w2_out2;


 	mid1 u1_mid1 (
  		.clk(clk),
  		.w_in1  (w1_in1),
  		.w_in2  (w1_in2),
		.w_out1 (w1_out1),
		.w_out2 (w1_out2)
		);

 	mid2 u1_mid2 (
  		.clk(clk),
  		.w_in1  (w2_in1),
  		.w_in2  (w2_in2),
		.w_out1 (w2_out1),
		.w_out2 (w2_out2)
		);

endmodule
