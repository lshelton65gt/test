# cleanup
rm -rf ve_* vh_* mylib*

# Test 1
# gold run
#cbuild -q test1.v ve.v vh.vhdl -testdriver -o libtest1.a >& test1.cbld.log.gold
#runtest ./test1.exe >& test1.sim.log.gold

# top level verilog module in a verilog file

############################################################################
#                               IMPORTANT
# A logical library can not have different physical lib anymore.
# Now there is no difference between verilog library and vhdl library.
# Chaning test to conform with this.
############################################################################

## currently many tests are VTSKIP'ed here because mixed language support is incomplete

# a. compiled separately into separate physical and logical libraries
cbuild -q -compileLibOnly -vhdlLib work:./mylib_test1a vh.vhdl >& test1a.1.cbld.log
cbuild -q -compileLibOnly -vlogLib work:./mylib_test1a ve.v >& test1a.2.cbld.log
cbuild -q -vlogLib work:./mylib_test1a -vhdlLib work:./mylib_test1a -vlogTop top test1.v -testdriver -o libtest1a.a >& test1a.3.cbld.log
runtest ./test1a.exe >& test1a.sim.log

# b. compiled together into separate physical and logical libraries
cbuild -q -compileLibOnly -vhdlLib work:./mylib_test1b vh.vhdl -vlogLib work:./mylib_test1b ve.v >& test1b.1.cbld.log
cbuild -q -vhdlLib work:./mylib_test1b -vlogLib work:./mylib_test1b test1.v -vlogTop top -testdriver -o libtest1b.a >& test1b.2.cbld.log
runtest ./test1b.exe >& test1b.sim.log

# top level verilog module in a verilog library

# c. compiled into one physical and logical library
cbuild -q -compileLibOnly -vhdlLib work:./vh_test1c vh.vhdl -vlogLib work:./vh_test1c test1.v >& test1c.1.cbld.log
cbuild -q -vlogLib work:./vh_test1c -vhdlLib work:./vh_test1c -vlogTop top ve.v -testdriver -o libtest1c.a >& test1c.2.cbld.log
runtest ./test1c.exe >& test1c.sim.log

# d. compile into one physical and separate logical libraries
VTSKIP cbuild -q -compileLibOnly -vhdlLib vh_work:./vh_test1d vh.vhdl -vlogLib ve_work:./vh_test1d test1.v >& test1d.1.cbld.log
VTSKIP cbuild -q -vhdlLib ve_work:./vh_test1d -vlogLib vh_work:./vh_test1d -vlogTop top ve.v -testdriver -o libtest1d.a >& test1d.2.cbld.log
VTSKIP runtest ./test1d.exe >& test1d.sim.log

# top level verilog module in default verilog library

# e. default verilog and vhdl library
cbuild -q -compileLibOnly vh.vhdl test1.v -o libtest1e.a >& test1e.1.cbld.log
cbuild -q -vlogLib WORK:./libtest1e.WORK -vhdlLib WORK:./libtest1e.WORK -vlogTop top ve.v -testdriver -o libtest1e.a >& test1e.2.cbld.log
runtest ./test1e.exe >& test1e.sim.log

# f. default paths for verilog and vhdl library
cbuild -q -compileLibOnly -vhdlLib vh_test1f vh.vhdl -vlogLib ve_test1f test1.v -o libtest1f.a >& test1f.1.cbld.log
cbuild -q -vhdlLib vh_test1f:libtest1f.vh_test1f -vlogLib ve_test1f:libtest1f.ve_test1f ve.v -vlogTop top -testdriver -o libtest1f.a >& test1f.2.cbld.log
runtest ./test1f.exe >& test1f.sim.log

# Test 2
# gold run
#cbuild -q test2.v vh1.vhdl vh2.vhdl veor1.v vexor1.v ve1.v -vlogTop top -testdriver -o libtest2.a >& test2.cbld.log.gold
#runtest ./test2.exe >& test2.sim.log.gold

# Test for multiple verilog libraries on read.

# a. All compiled in one go
cbuild -q -compileLibOnly -vhdlLib vh1_work:./vh_test2a vh1.vhdl -vhdlLib vh2_work:./vh_test2a vh2.vhdl -vlogLib ve_work:./ve_test2a test2.v veor1.v vexor1.v >& test2a.1.cbld.log
cbuild -q -vhdlLib vh1_work:./vh_test2a -vhdlLib vh2_work:./vh_test2a -vlogLib ve_work:./ve_test2a ve1.v -vlogTop top -testdriver -o libtest2a.a >& test2a.2.cbld.log 
runtest ./test2a.exe >& test2a.sim.log

# b. All compiled separately into separate libraries
cbuild -q -compileLibOnly -vhdlLib vh1_work:./vh_vh1_test2b vh1.vhdl >& test2b.1.cbld.log
cbuild -q -compileLibOnly -vhdlLib vh2_work:./vh_vh2_test2b vh2.vhdl >& test2b.2.cbld.log
cbuild -q -compileLibOnly -vlogLib ve1_work:./ve_ve1_test2b ve1.v >& test2b.3.cbld.log
cbuild -q -compileLibOnly -vlogLib veor1_work:./ve_veor1_test2b veor1.v >& test2b.4.cbld.log
cbuild -q -compileLibOnly -vlogLib vexor1_work:./ve_vexor1_test2b vexor1.v >& test2b.5.cbld.log
cbuild -q -compileLibOnly -vlogLib top_work:./ve_top_test2b test2.v >& test2b.6.cbld.log
cbuild -q -vhdlLib vh1_work:./vh_vh1_test2b -vhdlLib vh2_work:./vh_vh2_test2b -vlogLib ve1_work:./ve_ve1_test2b -vlogLib veor1_work:./ve_veor1_test2b -vlogLib vexor1_work:./ve_vexor1_test2b -vlogLib top_work:./ve_top_test2b -vlogTop top -testdriver -o libtest2b.a >& test2b.6.cbld.log
runtest ./test2b.exe >& test2b.sim.log

# c. A library specified multiple times
VTSKIP cbuild -q -vhdlLib vh1_work:./vh_vh1_test2b -vhdlLib vh2_work:./vh_vh2_test2b -vlogLib ve1_work:./ve_ve1_test2b -vlogLib top_work:./ve_top_test2b -vlogLib veor1_work:./ve_veor1_test2b -vlogLib top_work:./ve_top_test2b -vlogLib vexor1_work:./ve_vexor1_test2b -vlogLib top_work:./ve_top_test2b -vlogTop top -testdriver -o libtest2b.a >& test2c.1.cbld.log
VTSKIP runtest ./test2b.exe >& test2c.sim.log

# Test 3
# gold run
#cbuild -q test3.vhdl vh1.vhdl vh2.vhdl veor1.v vexor1.v ve1.v -vhdlTop top -testdriver -o libtest3.a >& test3.cbld.log.gold
#runtest ./test3.exe >& test3.sim.log.gold

# a. Verilog module instantiated in a top module VHDL file
# Currently mixed language designs do not work with enableCSElab
cbuild -disableCSElab -q -compileLibOnly -vhdlLib vh1_work:./vh_test3a vh1.vhdl -vhdlLib vh2_work:./vh_test3a vh2.vhdl -vlogLib ve_work:./ve_test3a ve1.v veor1.v vexor1.v >& test3a.1.cbld.log
cbuild -disableCSElab -q -vhdlLib vh1_work:./vh_test3a -vhdlLib vh2_work:./vh_test3a test3.vhdl -vlogLib ve_work:./ve_test3a -vhdlTop top -testdriver -o libtest3a.a >& test3a.2.cbld.log
runtest ./test3a.exe >& test3a.sim.log


# b. Verilog module instantiated in a top module VHDL library
# Currently mixed language designs do not work with enableCSElab
cbuild -disableCSElab -q -compileLibOnly -vhdlLib vh1_work:./vh_test3a vh1.vhdl -vhdlLib vh2_work:./vh_test3a test3.vhdl -vlogLib ve_work:./ve_test3a ve1.v veor1.v vexor1.v  >& test3b.1.cbld.log
cbuild -disableCSElab -q -vhdlLib vh1_work:./vh_test3a -vhdlLib vh2_work:./vh_test3a vh2.vhdl -vlogLib ve_work:./ve_test3a -vhdlTop top -testdriver -o libtest3b.a >& test3b.2.cbld.log
runtest ./test3b.exe >& test3b.sim.log

# Test 4

# a. No verilog file specified in compile only mode
# first test cbuild - it should fail.
# here explicitly calling CARBON_HOME/bin/cbuild prevents 'runsingle -vsp' from substituting cbuild with vspcompiler
# this test has 2 lines, one for verific flow where the test properly reports missing hdl files, and one for Interra flow where the test incorrectly is silent about missing hld files (bug 7942)

# bug 7942 tracks the fact that this test used to work but doesnt anymore (with Interra flow), so for now the return status is inverted with expectfail 
VTSKIP PEXIT expectfail $CARBON_HOME/bin/cbuild -q -compileLibOnly -o libtest4a.a >& test4a.cbld.log

# This correctly works with -useVerific, so we skip this test if running with Interra flow
IPSKIP PEXIT           $CARBON_HOME/bin/cbuild -q -compileLibOnly -o libtest4a.a >& test4a.cbld.log

# then test vspcompiler, since 'runsingle -vsp' will not do it. this should pass
# we do not care about the vspcompiler command so verific flow just skips this test
VTSKIP vspcompiler -q -compileLibOnly -o libtest4a.a >& test4a.1.cbld.log


# b. More than one verilog library specified for compile only mode
cbuild -q -vlogLib ve_1_work -vlogLib ve_2_work test4.v -vlogTop top -o libtest4b.a >& test4b.1.cbld.log 

# c. Same Verilog module appearing in multiple libraries
cbuild -q -compileLibOnly -vlogLib ve_1_test4c test4.v -o libtest4c.a >& test4c.1.cbld.log
cbuild -q -compileLibOnly -vlogLib ve_2_test4c test4.v -o libtest4c.a >& test4c.2.cbld.log
cbuild -q -vlogLib ve_1_test4c -vlogLib ve_2_test4c -vlogTop top -o libtest4c.a >& test4c.3.cbld.log

# d. Same verilog module appearing in a library and a file
cbuild -q -compileLibOnly -vlogLib ve_test4d test4.v -o libtest4d.a >& test4d.1.cbld.log
cbuild -q -vlogLib ve_test4d -vlogTop top -o libtest4d.a >& test4d.2.cbld.log

# e. No verilog file or library specified for verilog
PEXIT cbuild -q -o libtest4e.a >& test4e.1.cbld.log

# f. Verilog top should be specified when using libraries
PEXIT cbuild -q -vlogLib ve_test4d -o libtest4f.a >& test4f.1.cbld.log

# Test 5
# gold run
#cbuild -q test5.v veor1.v vexor1.v -testdriver -o libtest5.a >& test5.cbld.log.gold
#runtest ./test5.exe >& test5.sim.log.gold

# Verilog only tests

# a. Compiled individually with top in non-work library. This should fail.
cbuild -q -vlogLib ve_or_test5a veor1.v -compileLibOnly -o libtest5a.a >& test5a.1.cbld.log
cbuild -q -vlogLib ve_top_test5a test5.v -compileLibOnly -o libtest5a.a  >& test5a.2.cbld.log
PEXIT cbuild -q -vlogLib ve_top_test5a -vlogLib ve_or_test5a vexor1.v -vlogTop top -testdriver -o libtest5a.a >& test5a.3.cbld.log

# b. Compiled together with top in work library
cbuild -q -vlogLib ve_test5b veor1.v test5.v -compileLibOnly -o libtest5b.a >& test5b.1.cbld.log
cbuild -q -vlogLib ve_test5b vexor1.v -vlogTop top -testdriver -o libtest5b.a >& test5b.2.cbld.log
runtest ./test5b.exe >& test5b.sim.log

# Test 6

# VHDL only tests

# a. Compiled individually with top in non-work library. This should fail.
cbuild -q -vhdlLib vh_and_test6a vhand.vhdl -compileLibOnly -o libtest6a.a >& test6a.1.cbld.log
cbuild -q -vhdlLib vh_and_test6a -vhdlLib vh_top_test6a test6.vhdl  -compileLibOnly -o libtest6a.a >& test6a.2.cbld.log
PEXIT cbuild -q -vhdlLib vh_top_test6a -vhdlLib vh_and_test6a vhor.vhdl -vhdlTop top -o libtest6a.a >& test6a.3.cbld.log
# Compiled individually with top in work library. This should pass.
cbuild -q -vhdlLib vh_and_test6a -vhdlLib vh_top_test6a vhor.vhdl -vhdlTop top -testdriver -o libtest6a.a >& test6a.4.cbld.log
runtest ./test6a.exe >& test6a.sim.log

# b. Compile together with top in work library
cbuild -q -vhdlLib vh_test6b test6.vhdl vhor.vhdl -compileLibOnly -o libtest6b.a  -vhdl_synth_prefix not_test6a >& test6b.1.cbld.log
cbuild -q -vhdlLib vh_test6b vhand.vhdl -vhdlTop top -testdriver -o libtest6b.a >& test6b.2.cbld.log
runtest ./test6b.exe >& test6b.sim.log

# c. Compile together with top in vhdl file
cbuild -q -vhdlLib vh_test6c vhor.vhdl vhand.vhdl -compileLibOnly -o libtest6c.a >& test6c.1.cbld.log
cbuild -q -vhdlLib vh_test6c test6.vhdl -vhdlTop top -testdriver -o libtest6c.a  -vhdl_synth_prefix not_test6a >& test6c.2.cbld.log
runtest ./test6c.exe >& test6c.sim.log

# bug 7588
cbuild -q -compileLibOnly -vlogLib lib1 mid1.v foo1.v foo2.v  >& bug7588.1.cbld.log
cbuild -q -compileLibOnly -vlogLib lib2 mid2.v  >& bug7588.2.cbld.log
cbuild -q -vlogLib lib1 -vlogLib lib2 -testdriver -vlogTop top top.v >& bug7588.3.cbld.log
runtest ./design.exe >& bug7588.sim.log

# Bug 8765
# a. Verilog on top
cbuild -f bug8765.cmd >& bug8765a.1.cbld.log
cbuild -testdriver -vlogTop top_v bug8765_top.v >& bug8765a.2.cbld.log
runtest ./design.exe >& bug8765a.sim.log 

# b. VHDL on top
cbuild -testdriver -vhdlTop top_vhdl bug8765_top.vhdl >& bug8765b.1.cbld.log
runtest ./design.exe >& bug8765b.sim.log

## This test used to pass (prior to revision 1381), but was elaborating the wrong top. It was elaborating the module in counter.v ('upDown1'), 
## rather than 'upDown in counter.vhd. Now, it is taking an assertion error here:
## VerificVhdlExpressionWalker.cpp:652 INFO_ASSERT(formal) failed

# Bug 12146
rm -rf lib*
VTSKIP cbuild -f cbuild.cmd.broken -verboseHierarchy >& bug12146.cbld.log
VTSKIP cat libdesign.hierarchy >& bug12146.libdesign.hierarchy

