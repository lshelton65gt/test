module top (clk, in1, in2, out_top);
  input clk;
  input [31:0] in1, in2;

  output [31:0] out_top;

  wire [31:0]   out_top;

  veor1 or1(.clk(clk), .in1(in1[15:0]), .in2(in2[15:0]), .out_veor(out_top[15:0]));
  vexor1 xor1(.clk(clk), .in1(in1[31:16]), .in2(in2[31:16]), .out_vexor(out_top[31:16]));

endmodule
