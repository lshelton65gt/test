LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

entity vh2 is
  port (
    clk : in std_ulogic;
    in1 : in std_logic_vector(15 downto 0);
    in2 : in std_logic_vector(15 downto 0);
    out_vh: out std_logic_vector(15 downto 0)
    );
end vh2;

architecture test_arch of vh2 is

  component vexor1
    port (
      clk       : in  std_ulogic;
      in1       : in  std_logic_vector(15 downto 0);
      in2       : in  std_logic_vector(15 downto 0);
      out_vexor : out std_logic_vector(15 downto 0));
  end component;
  
begin

  vexor : vexor1 port map (
    clk       => clk,
    in1       => in1,
    in2       => in2,
    out_vexor => out_vh);

end test_arch;
