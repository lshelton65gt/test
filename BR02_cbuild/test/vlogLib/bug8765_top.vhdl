library ieee;
use ieee.std_logic_1164.all;

entity top_vhdl is
  port (pin  : in  std_logic;
        pout : out std_logic);
end top_vhdl;

architecture toparch of top_vhdl is
  component foo_vhdl 
    port (pin  : in  std_logic;
          pout : out std_logic);
  end component;
  component bar_vhdl 
    port (pin  : in  std_logic;
          pout : out std_logic);
  end component;
  signal tmp : std_logic;
begin  -- toparch

  f0: foo_vhdl port map (pin, tmp);
  b0: bar_vhdl port map (tmp, pout);

end toparch;
