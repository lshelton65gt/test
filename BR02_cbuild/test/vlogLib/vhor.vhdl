LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

entity vhor is
  port (
    clk : in std_ulogic;
    in1 : in std_logic_vector(15 downto 0);
    in2 : in std_logic_vector(15 downto 0);
    out_vhor : out std_logic_vector(15 downto 0)
    );
end vhor;

architecture test_arch of vhor is
begin
  process (clk)
  begin
    if (clk'event and clk = '1') then
      out_vhor <= in1 or in2;
    end if;
  end process;
end test_arch;
