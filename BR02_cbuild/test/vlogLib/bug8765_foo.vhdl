library ieee;
use ieee.std_logic_1164.all;

entity foo_vhdl is
  port (pin  : in  std_logic;
        pout : out std_logic);
end foo_vhdl;

architecture toparch of foo_vhdl is
begin  -- toparch

  pout <= pin;

end toparch;
