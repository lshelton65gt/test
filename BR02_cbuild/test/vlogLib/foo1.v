module foo1 (clkin, in1, out1);
input clkin;
input in1;
output out1;

reg out1;

always @(clkin)
	out1 <= in1;
endmodule
