module vexor1(clk, in1, in2, out_vexor);
  input clk;
  input [15:0] in1, in2;

  output [15:0] out_vexor;

  reg [15:0]    out_vexor;

  always @(posedge clk)
    out_vexor <= in1 ^ in2;

endmodule
