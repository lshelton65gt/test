module ve1(clk, in1, in2, out_ve);
  input clk;
  input [31:0] in1, in2;

  output [31:0] out_ve;

  wire [31:0]   out_ve;

  vh1 test_vh1(.clk(clk), .in1(in1[15:0]), .in2(in2[15:0]), .out_vh(out_ve[15:0]));
  vh2 test_vh2(.clk(clk), .in1(in1[31:16]), .in2(in2[31:16]), .out_vh(out_ve[31:16]));

endmodule
