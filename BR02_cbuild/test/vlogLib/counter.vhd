library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
library mylib;
use mylib.cntlib.all;
                                                                                      
entity upDown is
    port (
        clk        : in std_logic;
        up         : in std_logic;
        down       : in std_logic;
        data_in    : in std_logic_vector(8 downto 0);
        parity_out : out std_logic;
        carry_out  : out std_logic;
        borrow_out : out std_logic;
        count_out  : out std_logic_vector(8 downto 0));
end upDown;
                                                                                      
architecture rtl of upDown is
   signal count  : std_logic_vector(8 downto 0);

begin

  upDown_process : PROCESS (clk)
     variable count_nxt : std_logic_vector(8 downto 0);
     variable cnt_up    : std_logic_vector(9 downto 0);
     variable cnt_dn    : std_logic_vector(9 downto 0);
     variable load      : std_logic;
     variable updown     : std_logic_vector(1 downto 0);
  begin

    if clk'EVENT AND clk = '1' then

       cnt_dn := count - 5;
       cnt_up := count + 3;
       load := '1';
       updown := up & down;
   
       case (updown) is
         when "00" => count_nxt := data_in;
         when "01" => count_nxt := cnt_dn(8 downto 0);
         when "10" => count_nxt := cnt_up(8 downto 0);
         when "11" => load := '0';
         when others => NULL;
       end case;
   
       if (load = '1') then
         parity_out <= REDUCE_XOR(count_nxt);
         carry_out <= up AND cnt_up(9);
         borrow_out <= down AND cnt_dn(9);
         count <= count_nxt;
       end if;

    end if;

  end process upDown_process;

  count_out <= count;

end rtl;
