LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

entity vhand is
  port (
    clk : in std_ulogic;
    in1 : in std_logic_vector(15 downto 0);
    in2 : in std_logic_vector(15 downto 0);
    out_vhand : out std_logic_vector(15 downto 0)
    );
end vhand;

architecture test_arch of vhand is
begin
  process (clk)
  begin
    if (clk'event and clk = '1') then
      out_vhand <= in1 and in2;
    end if;
  end process;
end test_arch;
