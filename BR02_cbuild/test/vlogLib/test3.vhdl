LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

entity top is
  port (
    clk : in std_ulogic;
    in1 : in std_logic_vector(31 downto 0);
    in2 : in std_logic_vector(31 downto 0);
    out_top: out std_logic_vector(31 downto 0)
    );
end top;

architecture test_arch of top is

  component ve1
    port (
      clk      : in  std_ulogic;
      in1      : in  std_logic_vector(31 downto 0);
      in2      : in  std_logic_vector(31 downto 0);
      out_ve   : out std_logic_vector(31 downto 0));
  end component;
  
begin

  ve : ve1 port map (
    clk      => clk,
    in1      => in1,
    in2      => in2,
    out_ve => out_top);

end test_arch;
