module foo_v(pin, pout);
   input  pin;
   output pout;

   assign pout = pin;

endmodule
