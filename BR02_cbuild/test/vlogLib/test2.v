module top (clk, in1, in2, out_top);
  input clk;
  input [31:0] in1, in2;

  output [31:0] out_top;

  wire [31:0]   out_top;

  ve1 test_ve1(clk, in1, in2, out_top);

endmodule
