module veor1(clk, in1, in2, out_veor);
  input clk;
  input [15:0] in1, in2;

  output [15:0] out_veor;

  reg [15:0]    out_veor;

  always @(posedge clk)
    out_veor <= in1 | in2;

endmodule
