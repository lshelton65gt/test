library ieee;
use ieee.std_logic_1164.all;

entity bar_vhdl is
  port (pin  : in  std_logic;
        pout : out std_logic);
end bar_vhdl;

architecture toparch of bar_vhdl is
begin  -- toparch

  pout <= pin;

end toparch;
