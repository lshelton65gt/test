// Examples of valid clock gating
module top (q1, q2, q3, q4, clk, en1i, en2i, en3i, en4i, d1, d2, d3, d4);
   output q1, q2, q3, q4;
   input  clk, en1i, en2i, en3i, en4i, d1, d2, d3, d4;

   reg    q1, q2, q3, q4;
   reg    en1, en2, en3, en4;
   initial begin
      q1 = 0;
      q2 = 0;
      q3 = 0;
      q4 = 0;
      en1 = 0;
      en2 = 0;
      en3 = 0;
      en4 = 0;
   end

   // Flop the enable inputs to help match with Aldec
   always @ (posedge clk)
     begin
        en1 <= en1i;
        en2 <= en2i;
        en3 <= en3i;
        en4 <= en4i;
     end

   wire gclk1, gclk2, gclk3, gclk4;
   
   clkgate CG1 (gclk1, clk, en1);
   always @ (posedge gclk1)
     q1 <= d1;
   

   clkgate CG2 (gclk2, clk, en2);
   always @ (posedge gclk2)
     q2 <= d2;
   

   clkgate CG3 (gclk3, clk, en3);
   always @ (posedge gclk3)
     q3 <= d3;

   clkgate CG4 (gclk4, clk, en4);
   always @ (posedge gclk4)
     q4 <= d4;

endmodule

module clkgate (gclk, clk, eni);
   output gclk;
   input  clk, eni;

   wire eno;
   assign eno = clk ? eno : eni;
   assign gclk = clk & eno;

endmodule
