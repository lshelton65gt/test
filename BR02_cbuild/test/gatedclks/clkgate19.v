// Examples of valid/invalid clock gated latches.
// This is a thorough set of combinationals of simple gated clocks
//
// More complex examples could include inverted gated clocks.
module top (q1, q2, q3, q4, q5, q6, q7, q8,
            q9, q10, q11, q12, q13, q14, q15, q16,
            clk, en0i, en2i, en4i, en6i,
            d1, d2, d3, d4, d5, d6, d7, d8,
            d9, d10, d11, d12, d13, d14, d15, d16);
   output q1, q2, q3, q4, q5, q6, q7, q8,
          q9, q10, q11, q12, q13, q14, q15, q16;
   input  clk, en0i, en2i, en4i, en6i,
          d1, d2, d3, d4, d5, d6, d7, d8,
          d9, d10, d11, d12, d13, d14, d15, d16;

   reg    q1, q2, q3, q4, q5, q6, q7, q8;
   reg    q9, q10, q11, q12, q13, q14, q15, q16;
   reg    en0, en1, en2, en3, en4, en5, en6, en7;
   reg    en8, en9, en10, en11, en12, en13, en14, en15;
   reg    en16, en17, en18, en19, en20, en21, en22, en23;
   reg    en24, en25, en26, en27, en28, en29, en30, en31;
   initial begin
      q1 = 0;
      q2 = 0;
      q3 = 0;
      q4 = 0;
      q5 = 0;
      q6 = 0;
      q7 = 0;
      q8 = 0;
      q9 = 0;
      q10 = 0;
      q11 = 0;
      q12 = 0;
      q13 = 0;
      q14 = 0;
      q15 = 0;
      q16 = 0;
      en0 = 0;
      en1 = 0;
      en2 = 0;
      en3 = 0;
      en4 = 0;
      en5 = 0;
      en6 = 0;
      en7 = 0;
      en8 = 0;
      en9 = 0;
      en10 = 0;
      en11 = 0;
      en12 = 0;
      en13 = 0;
      en14 = 0;
      en15 = 0;
      en16 = 0;
      en17 = 0;
      en18 = 0;
      en19 = 0;
      en20 = 0;
      en21 = 0;
      en22 = 0;
      en23 = 0;
      en24 = 0;
      en25 = 0;
      en26 = 0;
      en27 = 0;
      en28 = 0;
      en29 = 0;
      en30 = 0;
      en31 = 0;
   end

   // Flop the enable inputs to help match with Aldec
   always @ (posedge clk)
     begin
        en0 <= en0i;
        en2 <= en2i;
        en4 <= en4i;
        en6 <= en6i;
        en8 <= ~en0i;
        en10 <= ~en2i;
        en12 <= ~en4i;
        en14 <= ~en6i;
        en16 <= en0i ^ en6i;
        en18 <= en2i ^ en4i;
        en20 <= en4i ^ ~en2i;
        en22 <= en6i ^ ~en0i;
        en24 <= en0i;
        en26 <= en2i;
        en28 <= en4i;
        en30 <= en6i;
     end

   // Create an inverted version of clock to make things difficult
   wire clk_;
   assign clk_ = ~clk;

   // Variation clk, clk, & ==> illegal
   always @ (clk or en0)
     if (clk)
       en1 <= en0;
   wire   gclk1;
   assign #1 gclk1 = clk & en1;
   always @ (negedge gclk1)
     q1 <= d1;

   // Variation clk, clk, | ==> legal
   always @ (clk or en2)
     if (clk)
       en3 <= en2;
   wire   gclk2;
   assign #1 gclk2 = clk | en3;
   always @ (negedge gclk2)
     q2 <= d2;
   
   // Variation clk, ~clk, & ==>  legal
   always @ (clk or en4)
     if (~clk)
       en5 <= en4;
   wire   gclk3;
   assign #1 gclk3 = clk & en5;
   always @ (negedge gclk3)
     q3 <= d3;

   // Variation clk, ~clk, | ==> illegal
   always @ (clk or en6)
     if (~clk)
       en7 <= en6;
   wire   gclk4;
   assign #1 gclk4 = clk | en7;
   always @ (negedge gclk4)
     q4 <= d4;

   // Variation ~clk, clk, & ==> legal
   always @ (clk or en8)
     if (clk)
       en9 <= en8;
   wire   gclk5;
   assign #1 gclk5 = ~clk & en9;
   always @ (negedge gclk5)
     q5 <= d5;
   
   // Variation ~clk, clk, | ==> illegal
   always @ (clk or en10)
     if (clk)
       en11 <= en10;
   wire   gclk6;
   assign #1 gclk6 = ~clk | en11;
   always @ (negedge gclk6)
     q6 <= d6;

   // Variation ~clk, ~clk, & ==> illegal
   always @ (clk or en12)
     if (~clk)
       en13 <= en12;
   wire   gclk7;
   assign #1 gclk7 = ~clk & en13;
   always @ (negedge gclk7)
     q7 <= d7;
   
   // Variation ~clk, ~clk, | ==> legal
   always @ (clk or en14)
     if (~clk)
       en15 <= en14;
   wire   gclk8;
   assign #1 gclk8 = ~clk | en15;
   always @ (negedge gclk8)
     q8 <= d8;

   // Variation clk, clk_, & ==> legal
   always @ (clk_ or en16)
     if (clk_)
       en17 <= en16;
   wire   gclk9;
   assign #1 gclk9 = clk & en17;
   always @ (negedge gclk9)
     q9 <= d9;

   // Variation clk, clk_, | ==> illegal
   always @ (clk_ or en18)
     if (clk_)
       en19 <= en18;
   wire   gclk10;
   assign #1 gclk10 = clk | en19;
   always @ (negedge gclk10)
     q10 <= d10;
   
   // Variation clk, ~clk_, & ==> illegal
   always @ (clk_ or en20)
     if (~clk_)
       en21 <= en20;
   wire   gclk11;
   assign #1 gclk11 = clk & en21;
   always @ (negedge gclk11)
     q11 <= d11;

   // Variation clk, ~clk_, | ==> legal
   always @ (clk_ or en22)
     if (~clk_)
       en23 <= en22;
   wire   gclk12;
   assign #1 gclk12 = clk | en23;
   always @ (negedge gclk12)
     q12 <= d12;

   // Variation ~clk, clk_, & ==> illegal
   always @ (clk_ or en24)
     if (clk_)
       en25 <= en24;
   wire   gclk13;
   assign #1 gclk13 = ~clk & en25;
   always @ (negedge gclk13)
     q13 <= d13;
   
   // Variation ~clk, clk_, | ==> legal
   always @ (clk_ or en26)
     if (clk_)
       en27 <= en26;
   wire   gclk14;
   assign #1 gclk14 = ~clk | en27;
   always @ (negedge gclk14)
     q6 <= d6;

   // Variation ~clk, ~clk_, & ==> legal
   always @ (clk_ or en28)
     if (~clk_)
       en29 <= en28;
   wire   gclk15;
   assign #1 gclk15 = ~clk & en29;
   always @ (negedge gclk15)
     q15 <= d15;
   
   // Variation ~clk, ~clk_, | ==> illegal
   always @ (clk_ or en30)
     if (~clk_)
       en31 <= en30;
   wire   gclk16;
   assign #1 gclk16 = ~clk | en31;
   always @ (negedge gclk16)
     q16 <= d16;

endmodule
