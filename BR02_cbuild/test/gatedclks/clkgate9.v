// Test some invalid conversion

module top (out1, out2, out3, clk, en1, en2, en3, d1, d2, d3);
   output out1, out2, out3;
   input  clk, en1, en2, en3, d1, d2, d3;

   reg    out1, out2, out3, en4, en5, en6;
   initial begin
      out1 = 0;
      out2 = 0;
      out3 = 0;
      en4 = 0;
      en5 = 0;
      en6 = 0;
   end

   // Create non-exclusive enable
   always @ (posedge clk)
     en4 <= en1;
   wire gclk1;
   assign #1 gclk1 = clk & en4; // delay is needed for an Aldec glitch
   always @ (posedge gclk1)
     out1 <= d1;

   // Create different clocked enables
   reg    dclk1, dclk2;
   initial begin
      dclk1 = 0;
      dclk2 = 0;
   end
   always @ (posedge clk)
     dclk1 <= ~dclk1;
   always @ (posedge clk)
     dclk2 <= ~dclk2;
   always @ (negedge dclk1)
     en5 <= en2;
   always @ (negedge dclk2)
     en6 <= en3;
   wire gclk2;
   assign gclk2 = dclk1 & en5 & en6;
   always @ (posedge gclk2)
     out2 <= d2;

   // And a second one so the test is not order dependent
   wire   gclk3;
   assign gclk3 = dclk2 & en5 & en6;
   always @ (posedge gclk3)
     out3 <= d3;

endmodule
