module x(ZyR, _StSK, cz);
input ZyR;
output _StSK;
output cz;

wire [27:28] ZyR;
wire [5:7] _StSK;
reg [9:51] cz;
wire [6:7] axR;

mkNm D (/* unconnected */, {axR[6:7],_StSK[5:7],axR[6:7],_StSK[5:7],_StSK[6:7],_StSK[6:7],_StSK[5:7]}, {_StSK[6:7],axR[6:7]});

endmodule

module mkNm(HlB, NoJ, XnDY);
output HlB;
output NoJ;
output XnDY;

reg HlB;
reg [9:25] NoJ;
wire [1:4] XnDY;

always @(&XnDY[1:3])
if (~XnDY[1:3])
begin
HlB = NoJ[15:21];
end

always @(negedge &XnDY[1:3])
begin
NoJ[10:19] = ~(HlB);
end

always @(posedge &NoJ[10:19])
begin
begin
HlB = ~HlB;
end
end

always @(negedge &NoJ[10:19])
begin
NoJ[12:25] = 1'b0;
end

assign XnDY[1:3] = ~HlB;

endmodule
