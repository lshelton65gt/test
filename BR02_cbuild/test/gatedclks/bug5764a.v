// Generated with: ./rangen -s 70 -n 0x4526eca9 -o /w/xenon/rsayde/build/rangen/obj/Linux.product/rangen/test/rangen/rangen/testcases.030906/cbld/assert2.v
// Using seed: 0x4526eca9
// Writing cyclic circuit of size 70 to file '/w/xenon/rsayde/build/rangen/obj/Linux.product/rangen/test/rangen/rangen/testcases.030906/cbld/assert2.v'
// Module count = 8

// module DiK
// size = 38 with 8 ports
module DiK(TdcO, aTmj, e, gZKLO, RgCj, F, WZDTj, N);
input TdcO;
input aTmj;
input e;
inout gZKLO;
input RgCj;
output F;
input WZDTj;
output N;

wire [7:23] TdcO;
wire aTmj;
wire [4:7] e;
wire [10:29] gZKLO;
wire [11:27] RgCj;
reg [4:17] F;
wire WZDTj;
reg [15:30] N;
wire [12:20] J;
reg [7:30] SiM;
reg n;
reg [14:22] GLM;
reg [24:28] rtm;
reg [4:12] spy;
reg FxD;
wire [0:23] NXZM;
wire [30:100] ngSg;
reg [1:1] rkB [28:96]; // memory
reg hWtzp;
reg RmmBa;
reg [11:32] ktqDN;
reg [4:6] Ab;
wire [2:3] uEBjA;
reg [36:81] EXzDx;
reg [4:30] Y;
wire fpBuL;
reg [0:20] kLgt;
reg [13:27] PA;
reg [11:27] oGFN;
wire x;
wire [13:20] gxa;
always @(negedge &e[4:5])
begin
begin
N[19:22] = ~RgCj[17];
end
end

assign gZKLO[22:27] = (aTmj != 1'b0) ? ~e[4:5]^N[19:22] : 6'bz;

assign NXZM[2:9] = (x);

assign fpBuL = (x != 1'b1) ? TdcO[22]^F[5:15] : 1'bz;

assign J[13:20] = N[19:22];

assign gZKLO[22:27] = ~(gZKLO[10:12]);

always @(negedge &EXzDx[73:76])
begin
begin
rtm[27:28] = ~RgCj[17]|RgCj[15:24];
end
end

cTXP eIUhZ ({TdcO[11:18],TdcO[22],aTmj,WZDTj}, {TdcO[22],TdcO[22]}, fpBuL, TdcO[22], {RgCj[17],WZDTj}, TdcO[11:18]);

always @(posedge aTmj)
begin :Ug
reg [7:17] Cxlj;
reg [15:30] nWL;
reg [0:21] pitY [3:3]; // memory
reg [36:51] tOi;

begin
end
end

always @(&TdcO[11:18])
if (~TdcO[11:18])
begin
ktqDN[12:13] = x;
GLM[17:22] <= F[5:15];
end

cTXP PyKbd ({WZDTj,TdcO[22],N[19:22],F[14:15],aTmj,e[4:5]}, ktqDN[12:13], fpBuL, TdcO[22], F[14:15], TdcO[11:18]);

cTXP s ({RgCj[17],RgCj[15:24]}, ktqDN[12:13], fpBuL, aTmj, ktqDN[12:13], TdcO[11:18]);

always @(posedge &e[4:5])
begin
begin
N[16:18] = ~rtm[27:28];
end
end

cTXP xdf ({F[14:15],e[4:5],x,N[19:22],x,TdcO[22]}, ktqDN[12:13], fpBuL, x, rtm[27:28], {TdcO[22],WZDTj,N[19:22],x,aTmj});

assign gxa[15:16] = TdcO[11:18];

assign NXZM[2:9] = x;

always @(posedge TdcO[22])
begin
begin
Y[12:25] = RgCj[15:24]&F[5:15];
end
end

always @(&gZKLO[10:12])
if (~gZKLO[10:12])
begin
end

assign NXZM[12:22] = ~N[16:18]^TdcO[22];

always @(posedge &rtm[27:28])
begin
begin
oGFN[12:18] = ~(~x);
end
end

always @(posedge &RgCj[15:24])
begin :rgCgB
reg [0:5] f;

begin
N[16:18] = x;
end
end

always @(&EXzDx[73:76])
if (EXzDx[73:76])
begin
end

jj kK (x, x, {e[4:5],RgCj[17],TdcO[11:18],TdcO[11:18],RgCj[17]}, aTmj);

always @(negedge aTmj)
begin
begin
N[16:18] = WZDTj;
end
end

always @(&N[16:18])
if (~N[16:18])
begin
rtm[25] = 5'b01011;
end

assign NXZM[2:9] = ~6'b111010;

always @(posedge &N[19:22])
begin
begin
rkB[91]/* memuse */ = ~3'b110;
end
end

assign gxa[15:16] = (e[4:5] != 2'b01) ? 8'b00111101 : 2'bz;

always @(posedge &ktqDN[12:13])
begin :FeML
reg [12:80] mtg;

begin
kLgt[3:13] = 9'b010000001;
end
end

always @(&ktqDN[12:13])
if (ktqDN[12:13])
begin
spy[5:7] = (~{x,N[16:18]});
end
else
begin
rtm[27:28] = N[16:18];
end

always @(&GLM[17:22])
if (~GLM[17:22])
begin
GLM[17:20] = ~F[5:15]&N[16:18];
end
else
begin
GLM[17:22] = ktqDN[12:13]|oGFN[12:18];
spy[6:7] = ~{F[14:15],N[16:18]};
end

always @(posedge &F[14:15])
begin
begin
PA[13:15] = 7'b1010100;
end
end

always @(&Y[12:25])
if (~Y[12:25])
begin
FxD = kLgt[3:13];
end

always @(posedge rkB[91]/* memuse */)
begin
begin
FxD <= ~aTmj;
rkB[62]/* memuse */ = ~PA[13:15]^TdcO[11:18];
end
end

assign ngSg[37:75] = ~gZKLO[10:12];

always @(negedge &TdcO[11:18])
begin
begin
FxD = ~ktqDN[12:13];
end
end

always @(negedge &rtm[27:28])
begin :xlHNP

begin
end
end

always @(rkB[62]/* memuse */)
if (rkB[62]/* memuse */)
begin
if (aTmj == 1'b1)
begin
n = ~rkB[91]/* memuse */|ktqDN[12:13];
end
else
begin
GLM[17:20] <= spy[6:7];
rtm[25] = ~rkB[91]/* memuse */|GLM[17:22];
end
end

endmodule

// module cTXP
// size = 32 with 6 ports
module cTXP(ax, Njqi, VL, rctr_, xcKaj, AJ);
input ax;
input Njqi;
output VL;
input rctr_;
input xcKaj;
input AJ;

wire [8:18] ax;
wire [5:6] Njqi;
reg VL;
wire rctr_;
wire [0:1] xcKaj;
wire [0:7] AJ;
reg [4:7] t;
reg [1:5] oXi;
reg [4:26] Xhx;
wire G;
reg [0:0] RyOOD;
reg [46:74] Lm;
reg [5:14] j [2:4]; // memory
reg [4:6] KqiuY;
reg UG;
wire [1:5] PTaHJ;
reg qGUFe;
reg [0:4] oHqsO;
reg [4:6] vzb;
reg JQ;
reg [11:29] hyYB;
reg [18:28] Xz;
reg [7:94] bH_Zq;
reg [0:1] XNTO;
reg [2:3] xoJ;
reg _TkmK;
reg ga;
reg [1:6] Yfe_B;
reg xZ;
reg [1:4] HhI;
reg WsGCo;
reg [8:19] U;
c J ({oXi[1:3],oXi[1:3],AJ[5],xcKaj[0],rctr_,Njqi[5:6],xcKaj[0],Njqi[5:6],AJ[5],_TkmK,5'b10010}, rctr_, {Yfe_B[6],UG,t[6]}, PTaHJ[2]);

always @(&Njqi[5:6])
if (Njqi[5:6])
begin
Yfe_B[3:5] = j[4]/* memuse */;
end
else
begin
vzb[6] = ~xcKaj[0];
end

assign G = (ax[9:18]);

always @(negedge t[6])
begin
begin
HhI[4] = 2'b11;
end
end

always @(negedge Yfe_B[6])
begin
begin
JQ = (Yfe_B[1:4]);
end
end

always @(posedge &ax[9:18])
begin
begin
ga = ~AJ[5];
end
end

always @(&oXi[1:3])
if (~oXi[1:3])
begin
WsGCo = ax[9:18]|Yfe_B[1:4];
end

always @(posedge rctr_)
begin
begin
end
end

always @(negedge Yfe_B[6])
begin :GYQvl
reg [3:6] Y;
reg [1:5] X;
reg [7:34] WNX;

begin
_TkmK = Yfe_B[3:5];
end
end

always @(&Yfe_B[3:5])
if (~Yfe_B[3:5])
begin
bH_Zq[40:46] = ~ga^j[4]/* memuse */;
end

assign PTaHJ[2] = xcKaj[0];

always @(posedge HhI[4])
begin
begin
oXi[1:3] = ~(~oXi[1:3]);
end
end

always @(negedge &oXi[1:3])
begin :Utd
reg [3:5] E;
reg [3:4] DsY;
reg [18:30] q [6:23]; // memory

begin
if (rctr_ == 1'b0)
begin
RyOOD[0] = 6'b001000;
end
end
end

always @(xcKaj[0])
if (~xcKaj[0])
begin
VL = ~(~ga) + (~_TkmK);
end

always @(JQ)
if (~JQ)
begin
hyYB[15] = ~oXi[1:3]^vzb[6];
end
else
begin
_TkmK = ~(ax[15:18]);
end

always @(hyYB[15])
if (hyYB[15])
begin
XNTO[1] = ~RyOOD[0]&Yfe_B[1:4];
end

always @(JQ)
if (JQ)
begin
end

always @(posedge ga)
begin
begin
Yfe_B[3:5] = ~rctr_;
end
end

assign G = ax[15:18]^j[4]/* memuse */;

always @(posedge &Yfe_B[1:4])
begin :aO
reg [4:13] x;
reg [1:2] o;
reg [20:28] bc;
reg [55:114] fHS;

begin
Yfe_B[1] <= ~(~j[4]/* memuse */) + (vzb[6]);
end
end

always @(posedge XNTO[1])
begin
begin
oXi[3:4] = ((Njqi[5:6]) + (~t[6]));
end
end

always @(negedge &oXi[1:3])
begin
begin
j[4]/* memuse */ = ~vzb[6];
end
end

always @(negedge JQ)
begin
begin
KqiuY[5:6] = ax[9:18];
xoJ[2] = ~(~((1'b0))) + (rctr_^Yfe_B[6]);
end
end

always @(negedge Yfe_B[6])
begin
begin
_TkmK = ~j[4]/* memuse */|rctr_;
end
end

always @(&bH_Zq[40:46])
if (~bH_Zq[40:46])
begin
end

always @(posedge AJ[5])
begin :phXn

begin
Xz[26] = (~Yfe_B[6]);
end
end

always @(posedge t[6])
begin
begin
hyYB[15] = vzb[6];
end
end

always @(&Njqi[5:6])
if (Njqi[5:6])
begin
oHqsO[0:3] = (bH_Zq[40:46]) + (~Yfe_B[1]&_TkmK);
end
else
begin
vzb[6] <= 8'b01111001;
end

always @(posedge &KqiuY[5:6])
begin :TXsT

begin
qGUFe = ~3'b101;
end
end

always @(posedge RyOOD[0])
begin
begin
U[9:17] <= ~9'b001000001;
end
end

always @(negedge t[6])
begin
begin
Yfe_B[1] = ~j[4]/* memuse */;
end
end

endmodule

// module jj
// size = 5 with 4 ports
module jj(ygNv, regDO, s, z);
input ygNv;
input regDO;
input s;
input z;

wire ygNv;
wire regDO;
wire [10:29] s;
wire z;
reg [2:2] If [0:27]; // memory
reg at;
reg [2:23] WQ;
reg aXno;
reg [25:116] f;
reg LiH;
reg A;
wire [18:19] E;
always @(&f[58:65])
if (~f[58:65])
begin
if (regDO == 1'b1)
begin
f[28:84] = ~f[58:65];
end
end

assign E[18:19] = s[19:22]&If[0]/* memuse */;

assign E[18:19] = ~(~If[0]/* memuse */&If[0]/* memuse */);

always @(posedge ygNv)
begin
begin
A = If[0]/* memuse */;
end
end

always @(z)
if (z)
begin
If[12]/* memuse */ = If[0]/* memuse */^f[28:84];
end
else
begin
f[61:78] = ~If[0]/* memuse */&f[58:65];
end

endmodule

// module c
// size = 5 with 4 ports
module c(C, J, XKlY_, A);
input C;
input J;
input XKlY_;
output A;

wire [1:21] C;
wire J;
wire [19:21] XKlY_;
reg A;
reg WtkSB;
reg [14:28] UB;
reg [1:4] zQGT;
reg [0:14] u;
wire [3:5] nqM;
always @(negedge &XKlY_[19:20])
begin
begin
WtkSB = J;
end
end

always @(nqM[3])
if (~nqM[3])
begin
A = ~(~nqM[3]^C[10]);
end

always @(negedge C[10])
begin
begin
end
end

assign nqM[3:5] = ~J;

always @(&XKlY_[19:20])
if (XKlY_[19:20])
begin
A = (A);
end
else
begin
u[0:5] = 5'b01111;
end

endmodule
