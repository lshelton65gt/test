// This is a test case from Panasonic that checks if we handle 
// converting gated clock flops to enabled flops when the
// gate-condition latch has a bit select enable

module  ck2 (out1, out2,sen,cken,cki, d);
   output  out1, out2;
   input   sen,cken,cki, d;
   wire    cko;

   reg     rsen, rcken;
   initial begin
      rsen = 0;
      rcken = 0;
   end
   always @ (posedge cki)
     begin
        rsen <= sen;
        rcken <= cken;
     end
   
   or      or_1   (n1out,rcken,rsen);
   not     not_2  (n2out,cki);
   lat_q   lat_q_3(n3q,n1out,n2out);
   and     and_4  (cko,cki,n3q);

   reg  out1, out2;
   initial begin
      out1 = 0;
      out2 = 0;
   end
   always @ (posedge cko)
     out1 <= d;
   always @ (posedge cki)
     out2 <= d;
endmodule

module lat_q (q,d,g) ;
   output q ;
   input  d,g ;
   reg    q ;
   initial q = 0;
   always @(g or d) begin
      if      (  g ) q <= d;
      else           q <= q;
   end
endmodule

