module top (out, clk, eni, d);
   output out;
   input  clk, eni, d;
   reg    out;
   initial out = 1;

   // Create a cycle for the enable latch
   wire   n1, n2, en;
   assign n1 = (clk & n2);
   assign n2 = (eni & n1);
   assign en = ~n1;

   // Create a gated clock and use it
   wire   gclk;
   assign gclk = clk & en;
   always @ (posedge gclk)
     out <= d;

endmodule

   
