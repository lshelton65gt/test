// This is a variant of clkgate10.v but with more vectors
// introduced to test the ability to handle bit selects.

module top (q1, q2, q3, q4, clk, eni, d1, d2, d3, d4);
   output      q1, q2, q3, q4;
   input       clk;
   input [3:0] eni;
   input       d1, d2, d3, d4;

   reg    q1, q2, q3, q4;
   reg [3:0] en;
   initial begin
      q1 = 0;
      q2 = 0;
      q3 = 0;
      q4 = 0;
      en = 0;
   end

   // Flop the enable inputs to help match with Aldec
   always @ (posedge clk)
     begin
        en <= eni;
     end

   wire [3:0] gclk;
   wire gclk1, gclk2, gclk3, gclk4;
   
   clkgate CG1 (gclk[0], clk, en[0]);
   assign gclk1 = gclk[0];
   always @ (posedge gclk1)
     q1 <= d1;
   

   clkgate CG2 (gclk[1], clk, en[1]);
   assign gclk2 = gclk[1];
   always @ (posedge gclk2)
     q2 <= d2;
   

   clkgate CG3 (gclk[2], clk, en[2]);
   assign gclk3 = gclk[2];
   always @ (posedge gclk3)
     q3 <= d3;

   clkgate CG4 (gclk[3], clk, en[3]);
   assign gclk4 = gclk[3];
   always @ (posedge gclk4)
     q4 <= d4;

endmodule

module clkgate (gclk, clk, eni);
   output gclk;
   input  clk, eni;

   wire eno;
   assign eno = clk ? eno : eni;
   assign gclk = clk & eno;

endmodule
