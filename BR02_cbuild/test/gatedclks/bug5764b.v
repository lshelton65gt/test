// Generated with: ./rangen -s 7 -a -n 0xc172ebc6 -o /w/uranium/aron/build/clean/obj/Linux.product/test/rangen/rangen/testcases.042006/cbld/assert7.v
// Using seed: 0xc172ebc6
// Writing acyclic circuit of size 7 to file '/w/uranium/aron/build/clean/obj/Linux.product/test/rangen/rangen/testcases.042006/cbld/assert7.v'
// Module count = 8

// module BUz
// size = 7 with 4 ports
module BUz(upd, b, R, u);
inout upd;
input b;
input R;
output u;

wire [2:5] upd;
wire b;
wire R;
wire [3:7] u;
reg [7:7] RMq;
wire chTx;
reg [29:29] KtG;
reg [15:20] P;
assign upd[2:5] = ~R;

assign chTx = upd[3];

always @(negedge upd[3])
begin
begin
P[17:20] = R^b;
end
end

T Ej ({R,b,b,P[17:20],upd[3]}, b);

always @(negedge upd[3])
begin
begin
P[19:20] = ~4'b0000;
end
end

IQ bexif ({P[19:20],b,P[19:20],upd[3],P[17:20],b,P[17:20],upd[3],P[19:20],b,8'b10100100}, /* unconnected */, {upd[2:5],upd[2:5],chTx,u[7],chTx,u[7],u[7],u[7],chTx,u[7],chTx,chTx});

endmodule

// module T
// size = 2 with 2 ports
module T(pO, QWIQ);
input pO;
input QWIQ;

wire [24:31] pO;
wire QWIQ;
reg [57:63] naGu;
reg olNB;
reg [3:16] IQEyt;
reg [10:68] AO;
reg [5:11] WTT;
reg [1:7] t;
reg [4:6] tqyf;
reg [2:6] HmUee;
reg FQ;
reg [0:2] Q;
reg vOt_;
reg [4:20] yGdEa;
reg KV;
reg [8:11] bESF;
reg [4:23] e;
reg Hyl;
reg ijxn;
reg [4:6] vAM;
reg [58:68] ti;
reg c;
wire [15:107] sml;
always @(negedge &pO[24:31])
begin
begin
yGdEa[11:20] = ~QWIQ;
end
end

always @(&yGdEa[11:20])
if (yGdEa[11:20])
begin
vAM[5:6] = QWIQ;
end

endmodule

// module IQ
// size = 1 with 3 ports
module IQ(ZAroD, yGRdD, QOIZr);
input ZAroD;
inout yGRdD;
output QOIZr;

wire [1:27] ZAroD;
wire [5:7] yGRdD;
reg [3:20] QOIZr;
always @(posedge &ZAroD[1:13])
begin :kUPkf
reg [4:5] qox;
reg [0:10] ykSt;

begin
QOIZr[3:17] = ~ZAroD[1:13];
end
end

endmodule
