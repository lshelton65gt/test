// This is a test case from Panasonic that checks if we handle
// converting latches to flops when the latch has a bit select
// enable

module  ck2 (cko,sen,cken,cki);
   output  cko;
   input   sen,cken,cki;

   reg     rsen, rcken;
   initial begin
      rsen = 0;
      rcken = 0;
   end
   always @ (posedge cki)
     begin
        rsen <= sen;
        rcken <= cken;
     end
   
   or      or_1   (n1out,rcken,rsen);
   not     not_2  (n2out,cki);
   lat_q   lat_q_3(n3q,n1out,n2out);
   and     and_4  (cko,cki,n3q);
endmodule

module lat_q (q,d,g) ;
   output q ;
   input  d,g ;
   reg    q ;
   initial q = 0;
   always @(g or d) begin
      if      (  g ) q <= d;
      else           q <= q;
   end
endmodule

