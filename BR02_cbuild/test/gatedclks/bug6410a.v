module top(in, out);
   input [27:28] in;
   output [2:2]  out;

   reg [2:2] 	 out;

   wire 		 gclk1;
   wire 		 gclk2;
   reg [9:25] 	 flatten_D_NoJ;
   reg [1:4] 	 flatten_D_XnDY;
   reg 		 lower_1;
   wire [5:7] 	 _StSK;
   reg [6:7] 	 axR;

   always 	 axR[6] = (!lower_1);
   assign 	 gclk1 = axR[6];
   assign 	 gclk2 = (axR[7] & (out & axR[6]));
   always
     begin
	if (axR[6]) begin
	end
	else begin
           lower_1 = 1'b1;
	end
     end
   always @(negedge gclk1)
     begin
	axR[7] = 1'b1;
     end
   always @(negedge gclk1)
     begin
	out = 1'b1;
     end
   always @(posedge gclk2)
     begin
	lower_1 = (!lower_1);
     end
   always @(negedge gclk2)
     begin
	axR = 2'h0;
     end
   always @(negedge gclk2)
     begin
	out = 1'b0;
     end
endmodule
