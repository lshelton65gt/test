// Examples of valid clock gating but invert the clocks and enable
// and the clock output

module top (q1, q2, q3, q4, clk, en0i, en2i, en4i, en6i, d1, d2, d3, d4);
   output q1, q2, q3, q4;
   input  clk, en0i, en2i, en4i, en6i, d1, d2, d3, d4;

   reg    q1, q2, q3, q4;
   reg    en0, en1, en2, en3, en4, en5, en6, en7;
   initial begin
      q1 = 0;
      q2 = 0;
      q3 = 0;
      q4 = 0;
      en0 = 0;
      en1 = 0;
      en2 = 0;
      en3 = 0;
      en4 = 0;
      en5 = 0;
      en6 = 0;
      en7 = 0;
   end

   // Flop the enable inputs to help match with Aldec
   always @ (posedge clk)
     begin
        en0 <= en0i;
        en2 <= en2i;
        en4 <= en4i;
        en6 <= en6i;
     end

   // Example 2
   always @ (negedge clk)
     en1 <= en0;

   wire   gclk1, gclk1_;
   assign gclk1 = ~clk | ~en1;
   assign gclk1_ = ~gclk1;

   always @ (posedge gclk1_)
     q1 <= d1;
   

   // Example 3
   always @ (posedge clk)
     en3 <= en2;

   wire   gclk2, gclk2_;
   assign gclk2 = ~clk & ~en3;
   assign gclk2_ = ~gclk2;

   always @ (negedge gclk2_)
     q2 <= d2;
   

   // Example 4
   always @ (clk or en4)
     if (clk)
       en5 <= en4;

   wire   gclk3, gclk3_;
   assign gclk3 = ~clk & ~en5;
   assign gclk3_ = ~gclk3;

   always @ (negedge gclk3_)
     q3 <= d3;


   // Example 5
   always @ (clk or en6)
     if (~clk)
       en7 <= en6;

   wire   gclk4, gclk4_;
   assign gclk4 = ~clk | ~en7;
   assign gclk4_ = ~gclk4;

   always @ (posedge gclk4_)
     q4 <= d4;

endmodule
