// Same as clkgate7 (nested clock gating) but changing and order
module top (q1, q2, q3, q4, clk, en0, en2, en4, en6, d1, d2, d3, d4);
   output q1, q2, q3, q4;
   input  clk, en0, en2, en4, en6, d1, d2, d3, d4;

   reg    q1, q2, q3, q4;
   reg    en1, en3, en5, en7;
   initial begin
      q1 = 0;
      q2 = 0;
      q3 = 0;
      q4 = 0;
      en1 = 0;
      en3 = 0;
      en5 = 0;
      en7 = 0;
   end

   always @ (clk or en0)
     if (~clk)
       en1 <= en0;

   wire   gclk1;
   assign gclk1 = en1 & clk;

   always @ (posedge gclk1)
     q1 <= d1;
   

   always @ (gclk1 or en2)
     if (~gclk1)
       en3 <= en2;

   wire   gclk2;
   assign gclk2 =  gclk1 & en3;

   always @ (posedge gclk2)
     q2 <= d2;
   

   always @ (gclk2 or en4)
     if (~gclk2)
       en5 <= en4;

   wire   gclk3;
   assign gclk3 = en5 & gclk2;

   always @ (posedge gclk3)
     q3 <= d3;


   always @ (gclk3 or en6)
     if (~gclk3)
       en7 <= en6;

   wire   gclk4;
   assign gclk4 = gclk3 & en7;

   always @ (posedge gclk4)
     q4 <= d4;

endmodule
