// This test case require addition expression synthesis for the no 
// gated clock case to convert gclk2's fanin latch. Expressions had
// to be created for gclk2 and tmpclk2.
//
// This came from Ceva with the local analysis breakup change.

module top (out1, out2, out3, out4, clk, d1, d2, en1, en2);
   output [3:0] out1, out2, out3, out4;
   input [3:0]  clk, d1, d2;
   input        en1, en2;

   wire [1:0]   iclk;
   wire         tmpclk1, tmpclk2, gclk1, gclk2;
   reg          ren1, ren2;
   reg          cen1, cen2;          
   reg [3:0]    out1, out2, out3, out4;

   initial begin
      out1 = 0;
      out2 = 0;
      out3 = 0;
      out4 = 0;
      ren1 = 0;
      ren2 = 0;
   end

   // Create clocked versions of the enables so that we get predictable
   //  behavior with an event based simulator.
   assign       tmpclk1 = clk[1];
   always @ (posedge tmpclk1)
     cen1 <= en1;
   always @ (negedge tmpclk1)
     cen2 <= en2;

   // Delays are to make sure we see stable ren1 and clk[1] 
   // in an event based simulator.
   assign #1      gclk1 = ren1 & clk[1];

   always @ (tmpclk1 or cen1)
     if (~tmpclk1)
       ren1 <= cen1;

   assign       iclk[1] = gclk1;
   assign       tmpclk2 = iclk[1];

   // Delays are to make sure we see stable ren2 and iclk[1]
   // in an event based simulator
   assign #1    gclk2 = ren2 & iclk[1];

   always @ (tmpclk2 or cen2)
     if (~tmpclk2)
       ren2 <= cen2;

   always @ (posedge tmpclk1)
     out1 <= d1;

   always @ (posedge gclk1)
     out2 <= d2;

   always @ (posedge tmpclk2)
     out3 <= ~d1;

   always @ (posedge gclk2)
     out4 <= ~d2;

endmodule
