// Test case with an enable that will never load.
// This caused problems with the gated clock optimization.

module top (out1, out2, clk, d1, d2, en);
   output out1, out2;
   input  clk, d1, d2, en;

   sub S1 (out1, clk, en, clk, d1);
   sub S2 (out2, clk, en, 1'b0, d2);

endmodule

module sub (q, clk, en, enclk, d);
   output q;
   input  clk, en, enclk, d;

   reg    q, eno;
   initial begin
      q = 0;
      eno = 0;
   end
   always @ (posedge enclk)
     eno <= en;
   wire   gclk;
   assign #1 gclk = clk | eno;

   always @ (posedge gclk)
     q <= d;

endmodule
