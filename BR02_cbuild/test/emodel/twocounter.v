// This demonstrates compiling and running a simple design with
// two counters driven by two clocks

module twocounter(clk1, clk2, reset1, reset2, out1, out2);
  input clk1, clk2, reset1, reset2;
  output [31:0] out1, out2;

  counter u1(clk1, reset1, 32'd1, out1);
  counter u2(clk2, reset2, 32'd3, out2);
endmodule
