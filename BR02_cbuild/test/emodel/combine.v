// combine two IP blocks from different vendors

module combine(in1, in2, out1, out2);
  input in1, in2;
  output out1, out2;

  top top(out1, in1);
  expose_dir expose_dir(out2, in2);
endmodule
