// this file gets pulled into combine.v via -v libcemlib.cem

module c_e_m_lib(out, in);
  output out;
  input  in;

  not(out, a);
  not(a, b);
  not(b, c);
  not(c, d);
  not(d, in);
endmodule
