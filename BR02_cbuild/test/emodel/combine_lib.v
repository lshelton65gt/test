// combine two IP blocks from different vendors

module combine(in1, in2, out1, out2, in3, out3);
  input in1, in2, in3;
  output out1, out2, out3;

  top top(out1, in1);
  expose_dir expose_dir(out2, in2);
  c_e_m_lib c_e_m_lib(out3, in3);
endmodule
