module counter(clk, reset, delta, out);
  input clk, reset;
  output [31:0] out;
  input [31:0]  delta;
  reg [31:0] out;

  always @(posedge clk)
    if (reset)
      out <= 32'b0;
    else
      out <= out + delta;
endmodule
