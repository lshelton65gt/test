module caselatch(clk,sel,out);
  input clk;
  input [1:0] sel;
  output [1:0] out; 
  reg [1:0] out;
  reg [1:0] tmp;

  always @(sel)
    case (sel)// carbon parallel_case full_case
      2'b00: tmp = 2'b01;
      2'b10: tmp = 2'b11;
      2'b11: tmp = 2'b00;
    endcase

  always @(posedge clk)
    out <= tmp;
endmodule

