module expose_dir(out, in);
  output out;
  input  in;

  not(out, a);
  not(a, b);
  not(b, c);
  not(c, d);
  not(d, in);
endmodule
