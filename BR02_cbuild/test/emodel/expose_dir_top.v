module top(out, in);
  output out;
  input  in;

  expose_dir expose_dir(out, in);
endmodule
