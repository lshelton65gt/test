#!/usr/bin/python2.2
##
##
## Copyright (c) 2003 Carbon Design Systems, Inc.
##
##


import setup_util
import os

##
## Do anything that needs to be done to set up the customer area.
## This usually includes creating it and setting up links to the RTL
## source and waveform files. 
##
## This function is passed two arguments:
##
##   template_root -- The directory containing the template files for
##                    the customer design framework.
##   design_root   -- The value of whatever was passed via the -r
##                    option on the command line.
##
def setup_customer_area(template_root, design_root):
    customer_area = os.path.join("..", "customer")
    setup_util.create_directory(customer_area)
    setup_util.link_file(os.path.join(design_root, "rtl"), os.path.join(customer_area, "proj_lmi_rtl_1_port/SRC/HDL/verilog"))
    #setup_util.link_file(os.path.join(design_root, "gold.vcd"), os.path.join(customer_area, "gold.vcd"))
    #setup_util.copy_file(os.path.join(design_root, "notes.txt"), os.path.join(customer_area, "notes.txt"))


##
## Do anything that needs to be done to set up the carbon area.  For
## example, if the paths in a "-f file" need to be updated, that
## should be done here.
##
## This function is passed two arguments:
##
##   template_root -- The directory containing the template files for
##                    the customer design framework.
##   design_root   -- The value of whatever was passed via the -r
##                    option on the command line.
##
def setup_carbon_area(template_root, design_root):
    pass
