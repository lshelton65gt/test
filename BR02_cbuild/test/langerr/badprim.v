/* Currently unsupported primitives. */
module top(i1, i2, e1, e2,
           otran1, otran2, ortran1, ortran2,
           otranif0, ortranif0, otranif1, ortranif1
          );
input i1, i2, e1, e2;
output otran1, otran2, ortran1, ortran2, otranif0,
       ortranif0, otranif1, ortranif1;

  tran(otran1, otran2);
  rtran(ortran1, ortran2);
  tranif0(otranif0, i1, e1);
  rtranif0(ortranif0, i1, e2);
  tranif1(otranif1, i1, e1);
  rtranif1(otranif1, i1, e1);

endmodule
