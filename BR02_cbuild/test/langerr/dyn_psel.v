module top(sel, big_vect, little_vect, dword, nibble);
  input [31:0] big_vect;
  input [0:31] little_vect;
  input [31:0]  sel;
  output [63:0] dword;
  output [3:0]  nibble;

  reg [63:0] dword;
  reg [3:0]  nibble;

  // The first four if statements show the identity between the
  // two part select constructs. The last one shows an indexable nature.

  always @(*) begin
    nibble = big_vect[7:4];
    if ( big_vect[0 +:8] == big_vect[7 : 0]) begin end
    if (little_vect[0 +:8] == little_vect[0 : 7]) begin end
    if ( big_vect[15 -:8] == big_vect[15 : 8]) begin end
    if (little_vect[15 -:8] == little_vect[8 :15]) begin end

    // Replace the byte selected.
    if (sel >0 && sel < 8)
      dword[8*sel +:8] = big_vect[7:0];
  end
endmodule
