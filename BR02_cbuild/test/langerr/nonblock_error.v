/* Test non-blocking assigns for which temporaries are necessary. */
module top(i1, i2, i3, sel,
           o1, o2, o3
          );

input i1, i2, i3, sel;
output o1, o2;
output [0:1] o3;
reg a, b;
reg [0:1] c;

assign o1 = a;
assign o2 = b;
assign o3 = c;

always
begin
  a = i1;
  b = i2;
  a <= b;
  b <= a;
end

always
begin
 c[0] <= i1;
 c[0] = i1 & i2;
 if (sel)
  c[0] <= i3;
 else
  c[0] = i2;
end

endmodule
