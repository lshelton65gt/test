// Test task/function recursion
module top(i, o, clk);
input [8:0] i;
output [8:0] o;
reg [8:0] o;
input clk;

function [8:0] factorial;
input [8:0] i;
begin
  if (i == 1)
    factorial = 1;
  else
    factorial = i * factorial(i-1);
end
endfunction

always @(posedge clk)
begin
  o = factorial(i);
end
endmodule
