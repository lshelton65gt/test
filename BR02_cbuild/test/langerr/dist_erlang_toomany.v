module top(clk, out);
  input clk;
  output [31:0] out;
  reg [31:0]    out;
  integer       seed;

  initial begin
    seed = 99;
  end

  always @(posedge clk)
    out <= $dist_erlang(seed, 10, 1000, 0);
endmodule
