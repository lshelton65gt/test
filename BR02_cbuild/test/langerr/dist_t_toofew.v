module top(clk, out);
  input clk;
  output [31:0] out;
  reg [31:0]    out;
  integer       seed;

  initial begin
    seed = 8921;
  end

  always @(posedge clk)
    out <= $dist_t();
endmodule
