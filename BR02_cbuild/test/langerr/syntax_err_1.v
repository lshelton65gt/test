// last mod: Fri Feb 17 11:21:00 2006
// filename: test/langerr/syntax_err_1.v
// Description:  This test has a syntax error, we want to see the proper error
// number reported by cheetah.


module syntax_err_1(clock, in1, in2, out1);
   input clock              // here is the error (no ;)
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule
