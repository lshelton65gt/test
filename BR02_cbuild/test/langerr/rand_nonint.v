module top(clk, out);
  input clk;
  output [31:0] out;
  reg [31:0]    out;

  reg [7:0]          seed;           // not 32 bits

  initial seed = 50;

  // No seed is specified -- must find the correct seed in the model
  always @(posedge clk)
    out <= $random(seed);
endmodule
