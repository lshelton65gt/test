module ashift(in, out1, out2);
  input [7:0] in;
  output [7:0] out1, out2;

  assign       out1 = in <<< 3;
  assign       out2 = in >>> 4;
endmodule

  