/* Asynchronous reset error conditions. */
module top(d, reset_val, clock, reset,
           o
          );
input d, reset_val;
input clock, reset;
reg q;
output o;

reg foo, bar;
reg reset2;
reg reset_tmp;

assign o = q;

   /*
   // Example 5 - Invalid asynchronous reset; circuit is never reset.
   */
   always @(posedge reset or negedge clock)
   begin
     if (reset)
       q = reset_val;
     q = d;
   end

   /*
   // Example 6 - Invalid asynchronous reset; if stmt not used.
   */
   always @(posedge reset or negedge clock)
   begin
     q = reset ? reset_val : d;
   end

   /*
   // Example 7 - Invalid asynchronous reset; wrong polarity checked.
   */
   always @(posedge reset or negedge clock)
   begin
     if (~reset)
       q = reset_val;
     else
       q = d;
   end

   /*
   // Example 8 - Invalid asynchronous reset; too many unused edge
   // nets (either 'clock' or 'reset2' is the clock; cannot decide).
   */
   always @(posedge reset or negedge clock or negedge reset2)
   begin
     if (reset)
       q = reset_val;
     else
       q = d;
   end

   /*
   // Example 9 - Invalid asynchronous reset; a statement preceeds
   // the if statement.
   */
   always @(posedge reset or negedge clock)
   begin
     reset_tmp = 1'b0;
     if (reset)
       q = reset_tmp;
     else
       q = d;
   end

   /*
   // Example 10 - Invalid asynchronous reset; a statement follows
   // the if statement.
   */
   always @(posedge reset or negedge clock)
   begin
     if (reset & !clock)
       q = reset_val;
     else
       q = d;
     foo = bar;
   end

endmodule
