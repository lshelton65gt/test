module adder8 (sum,c0,a,b,cin);
  input [7:0] a, b;
  output [7:0] sum;
  output [0:0] c0;
  input [0:0]  cin;
  assign       {c0, sum} = a + b + cin;
endmodule

module adder_32bit(a,b,sum,cin_in,c0_out);
  input [31:0]a, b;
  output [31:0] sum;
  input         cin_in;
  output        c0_out;
  wire [4:0]    c0;
  assign        c0[0] = cin_in;
  generate genvar i;
    for (i=0; i<=3; i = i +1) begin : u
      adder8 add (sum [8*i+7 : 8*i],
                  c0[i+1],
                  a [8*i+7 : 8*i],
                  b[8*i+7:8*i],
                  c0[i]);
    end
  endgenerate

  assign c0_out = c0[4];
endmodule
