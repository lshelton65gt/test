/* Test trireg with ports. */
module top(bid, in, out);
  inout bid;
  input in;
  output out;
  trireg bid;
  trireg in;
  trireg out;
  foo inst1 (bid, in, out);
endmodule

module foo(b, i, o);
  inout b;
  input i;
  output o;
  trireg b;
  trireg i;
  trireg o;
endmodule
