// Test multiple top level modules.

module top3(i, o);
input i;
output o;
assign o = i;
endmodule

module top1(i, o);
input i;
output o;
assign o = i;
endmodule

module top2(i, o);
input i;
output o;
assign o = i;
top3 inst1 (i,o);
endmodule
