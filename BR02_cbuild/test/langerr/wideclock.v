module top(in, out, c1, c2);
  input in;
  input [2:0] c1, c2;
  output      out;
  reg         out;

  always @(posedge c1 + c2)
    out <= in;
endmodule
