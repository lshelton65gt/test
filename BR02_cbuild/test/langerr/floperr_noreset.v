// Current error msg:
// message2.v:8: Error 3006: Always block cannot reset
module top(clk, rst, q, d);
   input clk, rst, d;
   output q;
   reg    q;

   always @(posedge clk or negedge rst)
     begin
          q <= d;
     end
   
endmodule
