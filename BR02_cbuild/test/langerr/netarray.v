module arrays;
  // declares a memory mema of 256 8-bit registers. The indices are 0 to 255
  reg [7:0] mema[0:255]; 

  // declare a two dimensional array of one bit registers
  reg       arrayb[7:0][0:255];

  wire w_array[7:0][5:0]; // declare array of wires
  integer inta[1:64]; // an array of 64 integer values
  time    chng_hist[1:1000]; // an array of 1000 time values
  integer t_index;

  initial begin
    mema[1] = 0; //Assigns 0 to the second element of mema
    arrayb[1][0] = 0; // Assigns 0 to the bit referenced by indices [1][0]
    inta[4] = 33559; // Assign decimal number to integer in array

    // Assign current simulation time to element addressed by integer index
    chng_hist[t_index] = $time;
  end
endmodule

