// Current error msg:
// message1.v:10: Error 3008: Conditional does not match an edge
module top(clk, rst1, rst2, q, d);
   input clk, rst1, rst2, d;
   output q;
   reg    q;

   always @(posedge clk or negedge rst1)
     begin
        if (rst1 ^ rst2)
          q <= 0;
        else
          q <= d;
     end
   
endmodule
