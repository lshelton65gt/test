// Test task recursion
module top(i, o, clk);
   input [8:0] i;
   output [8:0] o;
   reg [8:0] 	o;
   input 	clk;

   task factorial;
      input [8:0] i;
      output [8:0] o;
      reg [8:0] otemp;
      begin
	 if (i == 1)
	   o = 1;
	 else
	   begin
	      factorial(i, otemp);
	      o = i * otemp;
	   end
      end
   endtask

always @(posedge clk)
  begin
     factorial(i, o);
  end
endmodule
