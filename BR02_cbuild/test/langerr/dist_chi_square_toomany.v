module top(clk, out);
  input clk;
  output [31:0] out;
  reg [31:0]    out;
  integer       seed;

  initial begin
    seed = 99;
  end

  always @(posedge clk)
    out <= $dist_chi_square(seed, 50000, 1);
endmodule
