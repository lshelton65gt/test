/* Test pullup/down warnings. */
module top();

trireg wire_trireg1;
trireg wire_trireg2;
tri0 wire_tri0_1;
tri0 wire_tri0_2;
tri1 wire_tri1_1;
tri1 wire_tri1_2;
wire conflict;
wire [3:0] wire_vect;

// Warning conditions
pullup(wire_trireg1);
pulldown(wire_trireg2);
pullup(wire_tri0_1);
pulldown(wire_tri1_1);

pullup(conflict);
pulldown(conflict);

// Need no warning
pulldown(wire_tri0_2);
pullup(wire_tri1_2);

// Not yet supported
pullup(wire_vect[0]);
pulldown foo[0:2] (wire_vect[3:1]);

endmodule
