// This example was originally written for an on-demand example.  But
// when playing around with the ModelStudio GUI I stumbled on an
// assertion in the compiler when an asyncReset directive is applied
// to a vector net.  I reported this as compiler bug 7836, and isolated
// the testcase outside the GUI.
//
// This file is compiled with two directives files.  Both of which
// claim that count.debug_counter is an async reset, which it cannot
// be.  One of them also demotes the new alert message to a warning to
// verify that the compile succeeds.

module count(clk, reset, hold, ena_debug, out, out2);
  input clk, reset, hold, ena_debug;
  output [31:0] out, out2;
  reg [31:0]    out, out2;
  reg [31:0]    debug_counter;  // carbon observeSignal
  reg [2:0]     state;

  initial begin
    debug_counter = 0;
    state = 3'h0;
  end

  always @(posedge clk) begin : scope
    // This loop is meant just to slow down the model without introducing
    // any new state that on-demand would detect as acyclic.  The loop
    // can't be (easily) unrolled and accelerated because the loop bounds
    // depend on primary-input "hold".
    integer i;
    out2 = 0;
    for (i = 0; i < 32'hffff - hold; i = i + 1)
      out2 = out2 + 32'd1;

    // compute output value based on reset and current state
    if (reset)
      out <= 32'b0;
    else if (!hold && (state == 3'h2))
      out <= out + 32'd1;

    // Simple state-machine next-state logic.  3-bit state wraps around.
    state <= state + 3'h1;
  end

  // this debug counter auto-increments and will prevent automatic on-demand
  // idle detection unless
  always @(posedge clk)
    if (ena_debug)
      debug_counter = debug_counter + 1;
endmodule
