module top(sel,a,b,out);
   input sel;
   input a,b;
   output out;

   MUX2 MUX2_0(out,sel,a,b);
endmodule

primitive MUX2(Q,S,A,B);
   output Q;
   input  S, A, B;
   table
   // S A B Q
      0 0 ? : 0;
      0 1 ? : 1;
      1 ? 0 : 0;
      1 ? 1 : 1;
   endtable
endprimitive
