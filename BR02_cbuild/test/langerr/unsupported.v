module top();
   wire io1, io2, io3;
   wire clk;
   integer i;
   integer a;

   reg [31:0] addr;
   reg [7:0]  data;

   always @(posedge clk)
     begin
	i = 1;
	a = 0;
	while ( i > 0 )
	  begin
	     a = a + 1;
	     i = i - 1;
	  end
	foo.bar = a;
     end

   always @(posedge clk)
     foo.do_write(addr, data);
   
   rtranif1(io1, io2, io3);
   
   foo foo ();

endmodule


module foo();
   integer bar;
   real    baz;
   reg [31:0] mem[7:0];
   task do_write;
      input [7:0] addr;
      input [31:0] data;
      begin
	 mem[addr] = data;
      end
   endtask
endmodule
