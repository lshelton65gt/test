module top(o1, repeat_count, clk);
   output [31:0] o1;
   reg [31:0]    o1;
   input [63:0]  repeat_count;
   input         clk;

   always @(posedge clk)
     begin
        o1 = 0;
        repeat (repeat_count)
          begin
             repeat (0)
               o1 = o1 + 1;
             o1 = o1 + 1;
          end
     end
endmodule
