// Test driven primary inputs
module top(i, a1, e1, e2, o, i2, i3, i4);
  input i, a1, e1, e2, i3, i4;
  input [1:0] i2;
  output o;
  assign i = o;
  assign i2[1] = a1 + e1;
  assign i2[0] = a1 + e2;
  bar inst1 (i3, a1, e2);
  foo inst2 (i4, a1, e2);
endmodule

module bar(o, i1, i2);
  output o;
  input i1, i2;
  assign o = i1 + i2;
endmodule

module foo(io, e, d);
  inout io;
  input e, d;
  assign io = e ? d : 1'bz;
endmodule
