// last mod: Fri Feb 17 11:22:50 2006
// filename: test/langerr/syntax_err_2.v
// Description:  This test has a syntax error, we try to change this error to a
// warning,(but cannot),  we want to see the proper error message reported by cheetah.

module syntax_err_2(clock, in1, in2, out1);
   input clock              // here is the error (no ;)
   input [7:0] in1, in2;
   output [7:0] out1;
   reg [7:0] out1;

   always @(posedge clock)
     begin: main
       out1 = in1 & in2;
     end
endmodule
