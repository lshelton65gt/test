module top(clk, out);
  input clk;
  output [31:0] out;
  reg [31:0]    out;
  integer       seed;

  initial begin
    seed = 53;
  end

  always @(posedge clk)
    out <= $dist_uniform(seed, -100, 100, 5);
endmodule
