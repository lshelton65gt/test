library IEEE;
use IEEE.std_logic_1164.all;
use work.MYPACKAGE.all;

entity index18_wrapper_top is
    port ( port1 : in std_logic_vector(0 to 0);
           port2 : out std_logic_vector(0 to 0));
end entity index18_wrapper_top;


architecture arch of index18_wrapper_top is
component index18_top is
                          port ( port1 : in matrix;
                                 port2: out std_logic_vector(0 to 0));
  end component index18_top;
signal temp1 : matrix;
begin

    temp1(0) <= port1;

    i_dut2 : index18_top port map (temp1, port2);

end architecture arch;
