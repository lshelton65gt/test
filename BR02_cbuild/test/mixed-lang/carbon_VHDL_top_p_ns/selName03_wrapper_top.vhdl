library IEEE;
use IEEE.std_logic_1164.all;
use work.MYPACKAGE.all;

entity selName03_wrapper_top is
    port (infield : in std_logic_vector(0 to 7);
          myfield : in std_logic_vector(0 to 31) ;
          outfield  : out std_logic_vector(0 to 7));
end;


architecture arch of selName03_wrapper_top   is
  component selName03_top is
                          port ( port1 : in myrec;
                                 port2: out rec);
  end component selName03_top;
signal temp1 : myrec;
signal temp2 : rec;
begin

temp1.rec1.field1 <= infield;
temp1.myfield1 <= myfield;
outfield <= temp2.field1;

dut : selName03_top port map (port1 => temp1, port2 => temp2);
end;

