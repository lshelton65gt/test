library IEEE;
use IEEE.std_logic_1164.all;
use work.MYPACKAGE.all;

entity selName04_wrapper_top is
    generic ( g1 : integer := 16;
              g2 : integer := 1);
    port (infield : in std_logic_vector(0 to 7);
          myfield : in std_logic_vector(0 to 31) ;
          outfield  : out std_logic_vector(0 to 7));
end;


architecture arch of selName04_wrapper_top   is
  component selName04_top is
    generic ( g1 : integer := 16;
              g2 : integer := 1);
                          port ( port1 : in myrec;
                                 port2: out rec);
  end component selName04_top;
signal temp1 : myrec;
signal temp2 : rec;
begin

temp1.rec1.field1 <= infield;
temp1.myfield1 <= myfield;
outfield <= temp2.field1;

dut : selName04_top generic map ( g1 => 16, g2 => 1) port map (port1 => temp1, port2 => temp2);
end;
