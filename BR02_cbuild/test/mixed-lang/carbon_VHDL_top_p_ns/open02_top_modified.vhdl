-- in this modification, the port maping by name has had the order of the
-- mapping changed so that it does not match the positional mapping.
-- open02_top works fine, this is just a variation that needs testing.
-- last mod: Fri Oct 27 14:33:36 2006
-- ****************************************************************
-- *********  Copyright (c) 2003, 2006 Interra Systems, Inc. ************
-- *********  Use,  disclosure  or  distribution   is  ************
-- *********  prohibited   without  prior     written  ************
-- *********  permission  of  Interra  Systems,  Inc.  ************
-- ****************************************************************


-- ** Purpose   :  open port in entity instantiation(named).

-- ** TestPlan  :  1.1.9.4.2

-- ** Status    :  SIMULATION_SHOULD_PASS 

--**Assumptions :  NO

-- ** Date      :  02/14/2003 
-- ****************************************************************


library IEEE; 
use IEEE.std_logic_1164.all; 

entity open02_top is 
    port( in1  : in  std_logic;
		  out1 : out std_logic);
end entity open02_top; 

architecture arch of open02_top is 
    signal sig : std_logic;
begin 
	 sig <= in1;

     -- named association. 
     i0_dut : entity work.dut 
            port map ( A => sig);


end architecture arch; 
