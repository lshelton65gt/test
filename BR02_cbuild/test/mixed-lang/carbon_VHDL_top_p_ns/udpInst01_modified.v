// last mod: Tue Dec  6 11:41:06 2005
// filename: test/mixed-lang/carbon_VHDL_top_p_ns/udpInst01.v
// Description:  This is a variation of
// /tools/beacon/Mixed-2003.1.3/testcase/VHDL_top/positive/nosandwich/udpInst/udpInst01/udpInst01.v
// that is supported by carbon, note that this version is NOT sequential
// also note that aldec does not support calling a udp from vhdl


// define ALDEC if you want to simulate this with aldec
//`define ALDEC 1  
`ifndef ALDEC
primitive dut(z, a);
  output z;
  input a;

  table
  // a z
     0:1;
     1:0;
  endtable

endprimitive

`else

module dut(z, a);
   output z;
   input  a;

   assign z = ~ a;
endmodule
`endif
