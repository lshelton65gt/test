module top(sel, in0, in1, in2, in3, out);
   input [1:0] sel;
   input       in0, in1, in2, in3;
   output      out;

   mux mux (sel, {in0, in1, in2, in3}, out);
endmodule


module mux(sel, in, out);
   input [1:0] sel;
   input [3:0] in;
   output      out;
   reg 	       out;
   always @(sel or in)
     begin
	out = in[sel];
     end
endmodule
