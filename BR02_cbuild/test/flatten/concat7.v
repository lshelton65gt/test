module top(sel_in, out, in,
	   out1, out2, out3, out4, out5, out6, out7, out8, out9, out10,
	   in1, in2, sel2, sel3, clk);
   input [1:0] sel_in;
   output [3:0] out;
   reg [3:0] 	out;
   reg [1:0] 	sel;
   input 	in;
   output 	out1, out2, out3, out4;
   reg 		out1, out2, out3, out4;
   output [1:0] out5, out6;
   reg [1:0] 	out5, out6;
   output 	out7, out8, out9, out10;
   input 	in1, in2, sel2, sel3, clk;
   integer 	i;

   always @(sel or in or sel_in) {out[sel], sel} = {in, sel_in};
   assign {out7, out8} = {2{in1 & in2}};
   assign {out9, out10} = {in1, in2};

   always @(posedge clk)
     begin
	if (sel2)
	  begin
	     {out1, {out2}} = {in1, {in2}};
	  end
	case (sel3)
	  1'b0: {out3, out4} <= {in1, in2};
	  1'b1: {out4, out3} <= {in1, in2};
	endcase
	for (i = 0; i < 2; i = i + 1)
	  {out5[i], out6[i]} = {in1, in2};
     end
endmodule
