module case_top ( i1,i2,o1,o2 );
   input i1,i2;
   output o1;
   output o2;

   mycase CASE1 (o1,i1,i2);
   mycase CASE2 (o2,i2,i1);

endmodule // top

module mycase ( o, i1, i2 );
   input i1,i2;
   output o;
   reg 	  o;
   
   always @(i1 or i2)
     begin
	case ( {i1,i2} )
	  2'b00,
	  2'b10,
	  2'b01:
	    o = 1'b0;
	  2'b11:
	    o = 1'b1;
	endcase // case( {i1,i2} )
     end
   
endmodule // or
