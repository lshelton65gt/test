module function_top ( i1,i2,o1 );
   input i1,i2;
   output o1;

   myor OR1 (o1,i1,i2);

endmodule // top

module myor ( o, i1, i2 );
   input i1,i2;
   output o;
   reg 	  o;
   
   function or_func;
      input a;
      input b;
      or_func = a | b;
   endfunction // or_func

   always @(i1 or i2)
     o = or_func(i1,i2);
   
endmodule // or
