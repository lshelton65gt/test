module task_top ( i1,i2,o1 );
   input i1,i2;
   output o1;

   myor OR1 (o1,i1,i2);

endmodule // top

module myor ( o, i1, i2 );
   input i1,i2;
   output o;
   reg 	  o;
   
   task or_task;
      output o;
      input a;
      input b;
      o = a | b;
   endtask // or_func

   always @(i1 or i2)
     or_task(o,i1,i2);
   
endmodule // or
