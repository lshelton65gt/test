module bits_top(clk,rst,in,out);
   input clk;
   input rst;
   input [1:0] in;
   output [1:0] out;

   sub u0 (clk,rst,in[0],out[0]);
   sub u1 (clk,rst,in[1],out[1]);
endmodule

module sub(clk,rst,in,out);
   input clk;
   input rst;
   input [0:0] in;
   output [0:0] out;

   reg [0:0] 	out;

   always @(posedge clk) begin
      if (rst) 
	out[0:0] <= 1'b0;
      else
	out[0] <= in[0] | in[0:0];
   end
endmodule
