// toplevel too "large" to flatten into.
module big_top ( i, o, x, y );
   input [5:0] i;
   output      o;
   big big (i, o);

   input [5:0] x;
   output      y;

   wire [5:0]  t;

   // arbitrary bit order to prevent vectorization.
   assign      t[0] = x[5];
   assign      t[1] = x[2];
   assign      t[2] = x[4];
   assign      t[3] = x[1];
   assign      t[4] = x[3];
   assign      t[5] = x[0];

   assign      y = |t;

endmodule // top

// submodule too "large" to flatten up.
module big ( i, o );
   input [5:0] i;
   output      o;

   wire [5:0]  t;

   // arbitrary bit order to prevent vectorization.
   assign      t[0] = i[5];
   assign      t[1] = i[2];
   assign      t[2] = i[4];
   assign      t[3] = i[1];
   assign      t[4] = i[3];
   assign      t[5] = i[0];

   assign      o = |t;
endmodule // big
