module part_top ( i, j, x,y,z );
   input [5:0] i;
   input [2:0] j;
   output x,y,z;
   sub sub1 ( i[2:0], x );
   sub sub2 ( i[5:3], y );
   sub sub3 ( j, z );
endmodule // top

module sub ( i, o );
   input [1:3] i;
   output o;
   assign o = i[1] | i[2:3];
endmodule // sub
