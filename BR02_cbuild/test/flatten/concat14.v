module top(clk1, clk2, in3, in2, in1, in0, out3, out2, out1, out0);
   input clk1, clk2;
   input in3, in2, in1, in0;
   output out3, out2, out1, out0;
   reg 	  out3, out2, out1, out0;
   reg [3:0] mem [0:0];
   always @(posedge clk1)
     begin
	mem[0] = { in3, in2, in1, in0 };
     end
   always @(posedge clk2)
     begin
	{out3, out2, out1, out0} = mem[0];
     end
endmodule
