module integer_top (clk,in1,in2,out1);
   input clk;
   input [31:0] in1,in2;
   output [32:0] out1;
   integer int1,int2;
   always @(in1 or in2)
     begin
	int1 = in1;
	int2 = in2;
     end
   mod inst1 (clk,int1,int2,out1);
endmodule

module mod(clk,in1,in2,out);
   input clk;
   input [31:0] in1,in2;
   output [32:0] out;
   reg [32:0] out;
   initial out = 0;
   always @(posedge clk)
     out <= in1 + in2;
endmodule
