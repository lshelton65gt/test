module two_bit_output_top ( i,j, o );
   input i,j;
   output [1:0] o;
   sub sub1 ( i, o[0] );
   sub sub2 ( j, o[1] );
endmodule // top

module sub ( i, o );
   input i;
   output o;
   assign o = ~i;
endmodule // sub
