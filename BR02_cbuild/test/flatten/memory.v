module memory_top ( clk,i1,i2,value,out );
   input clk;
   input i1,i2;
   input [1:0] value;
   output out;

   memmod MEM (clk,i1,i2,value,out);

endmodule // top

module memmod ( clk,i1,i2,val,o );
   input clk;
   input i1,i2;
   input [1:0] val;
   output o;
   reg 	  o;
   
   reg [1:0] mem [1:0];
   initial begin
      mem[0] = 0;
      mem[1] = 0;
   end

   always @(i1 or val)
     mem[i1] = val;

   always @(posedge clk)
     o = mem[i2];
   
endmodule // or
