// testcase to show that flattening can inline the 'chooser' module
// without lowering the '~in' port actual.
module top(clk,bin,in,addr,bout,out);
   input clk;
   input bin;
   input [15:0] in;
   input [3:0] 	addr;
   output 	bout;
   output 	out;

   chooser chooser ( .clk(clk), .in(~in), .addr(addr), .out(out) );

   bhooser bhooser ( .clk(clk), .in(!bin), .out(bout) );
endmodule

module chooser(clk,in,addr,out);
   input clk;
   input [15:0] in;
   input [3:0] 	addr;
   output 	out;
   reg 		out;
   initial out = 1'b0;
   always @(posedge clk) begin
      out = in[addr];
   end
endmodule

module bhooser(clk,in,out);
   input clk;
   input in;
   output out;
   reg 		out;
   initial out = 1'b0;
   always @(posedge clk) begin
      out = !in;
   end
endmodule
