module top(clk, out1, out2, in1, in2, in3);
   input clk;
   output [2:5] out1;
   output [10:0] out2;
   input [0:3]	in1;
   input [3:0]	in2;
   input [3:0]	in3;
   bar bar (clk, {out1[4], out2[9:6], out1[2:3], out2[4:0]}, {in1, in2, in3});
endmodule

module bar(clk, out, in);
   input clk;
   output [0:11] out;
   reg [0:11] 	 out;
   input [11:0] in;

   always @(posedge clk)
     begin
	out[0:2] = in[11:9];
	out[2:4] = in[8:7];
	out[5:11] = in[6:0];
     end
endmodule
