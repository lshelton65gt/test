// testcase driven by Bug 1337 -- do not create aliases for
// task/function internals.
module top(clk,sel,write,out);
   input clk;

   input [0:0] sel;
   input [1:0] write;
   output [1:0] out;

   sub s0 ( clk, sel, write, out );
endmodule

module sub(clk,sel,write,out);
   input clk;

   input [0:0] sel;
   input [1:0] write;
   output [1:0] out;

   reg [1:0] out;

   reg [1:0] mem [1:0];

   task mem_task;
      output [1:0] o;
      input [0:0] addr;
      input [1:0] val;
      begin
	 o = mem[addr];
	 mem[addr] = val;
      end
   endtask
   
   initial begin
      mem[0] = 0;
      mem[1] = 0;
      out = 0;
   end
   
   always @(posedge clk) begin
      mem_task(out,sel,write);
   end
   
endmodule
