module top(clk, in0, in1, in2, in3, out0, out1, out2, out3);
   input clk;
   input in0, in1, in2, in3;
   output out0, out1, out2, out3;

   flop flop (clk, {in0, in1, in2, in3}, {out0, out1, out2, out3});
endmodule


module flop(clk, d, q);
   input clk;
   input [3:0] d;
   output [3:0] q;
   reg [3:0] 	q;
   always @(posedge clk)
     begin
	q <= d;
     end
endmodule
