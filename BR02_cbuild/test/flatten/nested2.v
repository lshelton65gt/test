module nested2_top ( i1,i2,o1,o2 );
   input i1,i2;
   output o1,o2;

   myor OR1 (o1,i1,i2);
   myor OR2 (o2,i2,i1);

endmodule // top

module myor ( o, i1, i2 );
   input i1,i2;
   output o;
   reg 	  o;
   
   always @(i1 or i2)
     begin :outie
	reg a;
	a=i1;
	begin :midie
	   reg b;
	   b=i2;
	   begin :innie
	      reg c;
	      c = a | b;
	      o = c;
	   end
	end
     end
   
   
endmodule // or
