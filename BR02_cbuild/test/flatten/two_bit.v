module two_bit_top ( i, o );
   input [1:0] i;
   output [1:0] o;
   sub sub1 ( i[0], o[0] );
   sub sub2 ( i[1], o[1] );
endmodule // top

module sub ( i, o );
   input i;
   output o;
   assign o = ~i;
endmodule // sub
