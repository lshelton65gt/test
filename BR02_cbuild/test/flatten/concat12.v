module top(in0, in1, in3, sel, clk, rst, out);
   input clk, rst;
   input [1:0] sel;
   input [15:0] in0;
   input [15:0] in1;
   input [15:0] in3;
   output [15:0] out;

   flop flop (clk, rst, sel, {in3, 16'h0002, in1}, in0, out);
endmodule


module flop(clk, rst, sel, data1, data2, out);
   input clk;
   input rst;
   input [1:0] sel;
   input [47:0] data1;
   input [15:0] data2;
   output [15:0] out;
   reg [15:0] 	 out;
   reg [65:2] 	 r;

   always @(posedge clk)
     begin
	if (rst)
	  begin
	     r = 0;
	  end
	else
	  begin
	     r[17:2] = data2;
	     r[65:18] = data1;
	  end
	case (sel)
	  2'b00: out = r[17:2];
	  2'b01: out = r[33:18];
	  2'b10: out = r[49:34];
	  2'b11: out = r[65:50];
	endcase
     end

endmodule
