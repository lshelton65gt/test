module top(sel, in, in0, in1, in2, in3, out, out0, out1, out2, out3);
   input [1:0] sel;
   input       in0, in1, in2, in3;
   input       in;       
   output      out;
   output      out0, out1, out2, out3;

   mux mux (sel, {in0, in1, in2, in3}, out);
   demux demux (sel, in, {out0, out1, out2, out3});
endmodule


module demux(sel, in, out);
   input [1:0] sel;
   input       in;
   output [3:0] out;
   reg [3:0] 	out;
   always @(sel or in)
     begin
	out[sel] = in;
     end
endmodule

module mux(sel, in, out);
   input [1:0] sel;
   input [0:3] in;
   output      out;
   reg 	       out;
   always @(sel or in)
     begin
	out = in[sel];
     end
endmodule
