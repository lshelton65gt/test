module const_top ( clk, en, data, out1, out2 );
   input clk;
   input en;
   input [1:0] data;
   output out1;
   output [1:0] out2;

   sub sub ( clk, out1, 2'b10, 2'b10 );
   ena ena ( out2, en, data, 2'b0z );
endmodule // top

module sub ( clk, out, val1, val2 );
   input clk;
   output out;
   input [0:1] val1;
   input [1:0] val2;

   reg 	       out;
	     
   always @(posedge clk) begin :kablooie
      reg t1;
      reg t2;
      reg t3;
      reg t4;
      t1 =  (val1[0]^val2[0]);
      t2 =  (val1[1]^val2[1]);
      t3 =  (val1[0]&val2[1]);
      t4 = ~(val1[1]|val2[0]);
      out = t1 & t2 & t3 & t4;
   end
   
endmodule

module ena ( out, en, d1, d2 );
   output [1:0] out;
   input  en;
   input  [1:0] d1;
   input  [1:0] d2;
   assign 	out = en ? d1 : d2;
endmodule
