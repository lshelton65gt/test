/*
 * Testcase to check that we properly dump VCD when flattening only
 * removes one instance. The flattening thresholds should be
 * configured such that s0 is flattened but s1 is not.
*/
module rescope_top (out,a,clk);
   output out;
   input a;
   input clk;

   wire  b;
   wire  d;
   sub s0(b,a);
   sub s1(d,a);

   reg 	 c;
   always @(b)
     c = ~b;

   reg 	 out;
   always @(posedge clk)
     out <= c & d;
   
endmodule

module sub (out,in);
   output out;
   input  in;
   reg 	  out;
   always @(in)
     out = ~in;
endmodule
