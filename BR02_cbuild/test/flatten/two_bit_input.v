module two_bit_input_top ( i, x, y );
   input [1:0] i;
   output      x,y;
   sub sub1 ( i[0], x );
   sub sub2 ( i[1], y );
endmodule // top

module sub ( i, o );
   input i;
   output o;
   assign o = ~i;
endmodule // sub
