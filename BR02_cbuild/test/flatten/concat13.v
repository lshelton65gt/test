module top(clk, rst, in0, in1, out0, out1);
   input clk;
   input rst;
   input [127:0] in0;
   input [31:0]  in1;
   output [127:0] out0;
   reg [127:0] 	 out0;
   output [31:0] out1;
   reg [31:0] 	 out1;

   always @(posedge clk)
     begin
	if (rst)
	  begin
	     out0 = 128'hxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
	     out1 = 32'hxxxxxxxx;
	  end
	else
	  begin
	     out0 = in0;
	     out1 = in1;
	  end
     end
   
endmodule
