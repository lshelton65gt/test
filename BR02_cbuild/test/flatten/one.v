module one_top ( i, o );
   input i;
   output o;
   sub sub ( i, o );
endmodule // top

module sub ( i, o );
   input i;
   output o;
   assign o = ~i;
endmodule // sub
