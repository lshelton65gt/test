// unit test for name preservation.

module names ( in, out, clk, rst, out2 );
   input [1:0] in;
   input clk;
   input rst;
   output [1:0] out;
   output [1:0] out2;

   wire [1:0] tin;
   assign tin = in;		// assignment alias -- input

   wire [1:0] tout;
   assign out = tout;		// assignment alias -- output

   // pass in a constant -- internally, this net is protected.
   sub sub ( .out(tout), .in(tin), .cin(2'b00), .clk(clk), .rst(rst) );

   // Create a second instance so we don't constant propagate
   sub sub2 ( .out(out2), .in(~tin), .cin({in, 1'b0}), .clk(~clk), .rst(~rst) );

endmodule

module sub ( out, in, cin, clk, rst ); 	// flattened port aliases
   output [1:0] out;
   input  [1:0] in;
   input  [1:0] cin;
   input  clk;
   input  rst;

   reg [1:0] delay;
   initial 
     delay = 0;
   always @(posedge clk) 
     delay = in;

   // 1. make sure we preserve names through partsels.
   // 2. the 'dout' output is intentionally disconnected -- it is
   //    user-observed (see names.dir) -- we should see its name 
   //    persist.
   subsub subsub ( .out(out[1:0]), .dout(), .in(delay[1:0]), .cin(cin), .clk(clk), .rst(rst) );
endmodule // sub

module subsub ( out, dout, in, cin, clk, rst ); // second-level flattened port aliases
   output [1:0] out;
   output [1:0] dout;
   input  [1:0] in;
   input  [1:0] cin;
   input  clk;
   input  rst;
   
   reg [1:0] out;
   reg [1:0] hold;

   initial 
     begin
	out  = 0;
	hold = 0;
     end
   
   function [1:0] fun;
      input [1:0] sel; // aliasing within a function
      input [1:0] old;
      fun = sel ? old : 2'b11;
   endfunction // endfunction

   always @(posedge clk)
     begin :name1
	reg [1:0] r; // aliasing within named block; r is defined as a flop, not temporary
	if (rst) begin
	   out = cin;
	   r = 2'b00;
	end else begin
	   hold = fun(in,cin) | hold;
	   if (hold) begin
	      r = ~in;
	   end
	   out = fun(r,hold);
	end
     end

   assign dout = ~out;
endmodule
