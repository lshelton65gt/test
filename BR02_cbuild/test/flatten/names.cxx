#include "libnames.h"
#include <cassert>
#include <iostream>

static const int MaxAliasGroups = 10;
static const int MaxSynonyms = 3;

int main(void)
{
  struct Aliases {
    char * representative;
    char * synonyms[MaxSynonyms];
  };

  Aliases aliases[MaxAliasGroups] = 
    { 
      { "names.in", { "names.tin", "names.sub.in", } },
      { "names.out", { "names.tout", "names.sub.out", "names.sub.subsub.out" } },
      { "names.clk", { "names.sub.clk", "names.sub.subsub.clk" } },
      { "names.rst", { "names.sub.rst", "names.sub.subsub.rst" } },
      { "names.sub.delay", { "names.sub.subsub.in" } },
      { "names.sub.subsub.hold", {} },
      { "names.sub.cin", { "names.sub.subsub.cin" } },
      { "names.sub.subsub.dout", {} },
      { "names.out2", { "names.sub2.out", "names.sub2.subsub.out" } },
      { "names.sub.subsub.name1.r", {} },
#if 0
      // Internal signals are not visible through the CPP API.
      { "names.sub.fun.fun", { "names.sub_fun.fun" } },
      { "names.sub.fun.sel", { "names.sub_fun.sel" } },
      { "names.sub.fun.old", { "names.sub_fun.old" } } 
#endif
    };

  CarbonObjectID * model = carbon_names_create(eCarbonFullDB, eCarbon_NoFlags);
  assert(model);

  std::cout << std::endl;
  std::cout << "***" << std::endl;
  std::cout << "*** Testing that the following groups of nets exist and are aliased:" << std::endl;
  std::cout << "***" << std::endl << std::endl;

  int status = 0;
  for (int i=0;i<MaxAliasGroups;++i) {
    const char * repName = aliases[i].representative;
    if (! repName) continue;

    CarbonNetID * repNet = carbonFindNet(model,repName);

    std::cout << repName;
    if (! repNet) { // these nets should exist.
      status = 1;
      std::cout << " *MISSING*";
    }
    std::cout << std::endl;

    for (int j=0;j<MaxSynonyms;++j) {
      const char * name = aliases[i].synonyms[j];
      if (! name) continue;

      CarbonNetID * net = carbonFindNet(model,name);

      std::cout << "    " << name;
      if (! net) { // these nets should exist.
	status = 1;
	std::cout << " *MISSING*";
      }
      std::cout << std::endl;

    }
  }

  std::cout << std::endl << "***" << std::endl;
  if (status) {
    std::cout << "*** Errors occurred." << std::endl;
  } else { 
    std::cout << "*** Success." << std::endl;
  }
  std::cout << "***" << std::endl;

  return(status);
}
