module CXEj(Ff, UH, E, rMBr, yvJ);
inout Ff;
input UH;
inout E;
output rMBr;
inout yvJ;

wire Ff;
wire UH;
wire E;
reg [1:8] rMBr;
wire [17:21] yvJ;

reg [11:15] kVi;

Jthu FX ({UH,UH,yvJ[19:20],UH,kVi[13:14],E,kVi[13:14],E,kVi[13:14],yvJ[19:20],8'b11010011}, UH, kVi[13:14], UH);

endmodule


module Jthu(ZDEoD, H, khg, sXL);
input ZDEoD;
input H;
input khg;
input sXL;

wire [6:28] ZDEoD;
wire [25:25] H;
wire [3:4] khg;
wire sXL;

wire [1:3] btcUH;
wire [0:3] tV;

zmkfS a (H[25], {btcUH[2:3]}, /* unconnected */);

zmkfS ttvhc (H[25], {tV[0:1]}, /* unconnected */);

tCB_Y SlrKu (H[25], {btcUH[2:3],H[25],btcUH[2:3],khg[4],khg[4]});

endmodule


module zmkfS(Y_v, Shd, GvtXk);
input Y_v;
output Shd;
inout GvtXk;

wire Y_v;
wire [3:5] Shd;

assign Shd[4:5] = ~Y_v;

endmodule


module tCB_Y(ztNr, IMKX);
input ztNr;
input IMKX;

wire ztNr;
wire [0:6] IMKX;

reg [0:7] sPx;

always @(negedge ztNr)
  sPx[2] = IMKX[2:3];

endmodule
