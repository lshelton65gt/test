module top(clk, rst,
	   in3, in2, in1, in0,
	   out3, out2, out1, out0,
	   in3_25, in2_27, in1_29, in0_31,
	   out3_25, out2_27, out1_29, out0_31
	   );
   input clk, rst;
   input in3, in2, in1, in0;
   output out3, out2, out1, out0;
   input  [24:0] in3_25;
   input  [26:0] in2_27;
   input  [28:0] in1_29;
   input  [30:0] in0_31;
   output  [24:0] out3_25;
   output  [26:0] out2_27;
   output  [28:0] out1_29;
   output  [30:0] out0_31;

   flop1 flop1 (clk,
		rst,
		{in3, in2, in1, in0},
		{out3, out2, out1, out0}
		);
   flop2 flop2 (clk,
		rst,
		{in3_25, in2_27, in1_29, in0_31},
		{out3_25, out2_27, out1_29, out0_31}
		);
endmodule

module flop1(clk, rst, in, out);
   input clk, rst;
   input [3:0] in;
   output [3:0] out;
   reg [3:0] 	out;

   always @(posedge clk)
     begin
	if (rst)
	  out <= 4'b1010;
	else
	  out <= in;
     end
endmodule

module flop2(clk, rst, in, out);
   input clk, rst;
   input [111:0] in;
   output [111:0] out;
   reg [111:0]    out;

   always @(posedge clk)
     begin
	if (rst)
	  out <= 112'hba9876543210fedcba9876543210;
	else
	  out <= in;
     end
endmodule
