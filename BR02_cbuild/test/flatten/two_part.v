module two_part_top ( i, x,y );
   input [3:0] i;
   output x,y;
   sub sub1 ( i[1:0], x );
   sub sub2 ( i[3:2], y );
endmodule // top

module sub ( i, o );
   input [1:0] i;
   output o;
   assign o = |i;
endmodule // sub
