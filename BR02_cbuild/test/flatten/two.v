module two_top ( i1,i2, o1,o2 );
   input i1,i2;
   output o1,o2;
   sub sub1 ( i1, o1 );
   sub sub2 ( i2, o2 );
endmodule // top

module sub ( i, o );
   input i;
   output o;
   assign o = ~i;
endmodule // sub
