module top(clk,en1,en2,raddr1,raddr2,waddr1,waddr2,data1,data2,out1,out2);
   input clk,en1,en2;
   input [3:0] raddr1,raddr2,waddr1,waddr2,data1,data2;
   output [3:0] out1,out2;
   reg [3:0] out1,out2;

   sub S1();
   sub S2();

   always @(posedge clk) begin
      if (en1) begin
	 S1.mem[waddr1] <= data1;
      end else begin
	 S1.mem[waddr1] <= S2.mem[raddr1];
      end
      out1 <= S1.mem[raddr1];
      if (en2) begin
	 S2.mem[waddr2] <= data2;
      end else begin
	 S2.mem[waddr2] <= S1.mem[raddr2];
      end
      out2 <= S2.mem[raddr2];
   end
endmodule

module sub;
   reg [3:0] mem [15:0];
endmodule
