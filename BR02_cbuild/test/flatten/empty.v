module empty_top (clk,rst,in,out);
   input clk,rst;
   input in;
   output out;
   reg 	  out;
   
   // instances with empty port lists (bug946)
   silly s0();
   silly s1();
   silly s2();

   always @(posedge clk) begin
      if (rst)
	out <= 0;
      else
	out <= in;
   end
endmodule

module silly(o,i);
   input i;
   output o;
   assign o = i;
endmodule
