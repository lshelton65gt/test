module constants_top (clk,i,o);
   input clk;
   input i;
   output o;
   reg o;
   always @(posedge clk) o <= i;
   DFFRHQX4 spare_ff0 (.CK(1'b0), .D(1'b0), .RN(1'b0), .Q());
   NAND2X4 spare_nand2_0 (.A(1'b0), .B(1'b0), .Y());
   INVX4 spare_inv_6 (.A(1'b0), .Y());
   CLKBUFX12 spare_bufx120 (.A(1'b0), .Y());
   CLKBUFX1 spare_bufx10 (.A(1'b0), .Y());
   CLKINVX8 clk_inv0 (.A(1'b0), .Y());
   MXI2X4 spare_muxi20 (.A(1'b0), .B(1'b0), .S0(1'b0), .Y());
   MX2X4 spare_mux20 (.A(1'b0), .B(1'b0), .S0(1'b0), .Y());
   MX4X4 spare_mux40 (.A(1'b0), .B(1'b0), .C(1'b0), .D(1'b0), .S0(1'b0), .S1(1'b0), .Y());
endmodule

module DFFRHQX4 ( CK, D, RN, Q );
   input CK;
   input D;
   input RN;
   output Q;
   reg 	  Q;
   always @ (posedge CK or negedge RN)
     begin
	if (~RN)
	  Q <= 1'b0;
	else
	  Q <= #0.5 D;
     end
endmodule

module NAND2X4 (Y, A, B);
   output Y;
   input  A, B;
   nand (Y, A, B);
endmodule 

module INVX4 (Y, A);
   input A;
   output Y;
   assign Y = ~A;
endmodule

module CLKBUFX12 (A, Y);
   input A;
   output Y;
   assign Y = A;
endmodule 

module CLKBUFX1 (A, Y);
   input A;
   output Y;
   assign Y = A;
endmodule 

module CLKINVX8 (A, Y);
   input A;
   output Y;
   assign Y = ~A;
endmodule 

module MXI2X4 (A, B, S0, Y);
   input A, B, S0;
   output Y;
   assign Y = (~S0) ? ~A: ~B;
endmodule 

module MX2X4 (A, B, S0, Y);
   input A, B, S0;
   output Y;
   assign Y = (~S0) ? A: B;
endmodule

module MX4X4 (A, B, C, D, S0, S1, Y);
   input A, B, C, D, S0, S1;
   output Y;
   reg 	  muxout;
   always @ (A or B or C or D or S0 or S1)
     begin 
	case({S1,S0})
	  2'b00: muxout = A;
	  2'b01: muxout = B;
	  2'b10: muxout = C;
	  2'b11: muxout = D;
	endcase
     end
   assign Y = muxout;
endmodule
