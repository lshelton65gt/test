// toplevel small enough to flatten into.
module up_top ( i1, i2, o1, o2 );
   input [5:0] i1;
   input [5:0] i2;
   output      o1;
   output      o2;
   big big1 (i1, o1);

   // the second instance will not be flattened because the composed
   // module would be too large.
   big big2 (i2, o2);
endmodule // top

// submodule too "large" to flatten up.
module big ( i, o );
   input [5:0] i;
   output      o;

   wire [5:0]  t;

   // arbitrary bit order to prevent vectorization.
   assign      t[0] = i[5];
   assign      t[1] = i[2];
   assign      t[2] = i[4];
   assign      t[3] = i[1];
   assign      t[4] = i[3];
   assign      t[5] = i[0];

   assign      o = |t;
endmodule // big
