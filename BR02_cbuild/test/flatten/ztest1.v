module top(clk, d0, d1, d2, d3, sel, o);
   input clk, sel;
   input d0, d1, d2, d3;
   output o;
   reg 	  o;
   
   flop f0 (clk, q0, d0, 1'b1);
   flop f1 (clk, q1, d1, 1'b1);
   flop f2 (clk, q2, d2, 1'b1);
   flop f3 (clk, q3, d3, 1'b0);

   always @(sel or q0 or q1 or q2 or q3)
     begin
	case (sel)
	  2'd0: o = q0;
	  2'd1: o = q1;
	  2'd2: o = q2;
	  2'd3: o = q3;
	endcase
     end
endmodule

module flop(clk, q, d, oe);
   input clk, oe, d;
   output q;
   reg 	  state;
   always @(posedge clk)
     state <= d;
   assign q = oe ? ~state : 1'bz;
endmodule
