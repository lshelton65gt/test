module top(rst, clk, sel, in0, in1, in2, in3, out);
   input rst, clk;
   input [1:0] sel;
   input [39:0] in0;
   input [39:0] in1;
   input [39:0] in2;
   input [39:0] in3;
   output [39:0] out;
   vector_mux vmux (rst, clk, sel, {in3, in2, in1, in0}, out);
endmodule

module vector_mux(rst, clk, sel, data_in, data_out);
   input rst, clk;
   input [1:0] sel;
   input [159:0] data_in;
   output [39:0] data_out;
   reg [39:0] 	 data_out;

   reg [159:0] 	 mux_vect;
 
   initial data_out = 0;

   always @(posedge clk)
     begin
	if (rst)
	  mux_vect = 160'h0000000004000000000300000000020000000001;
//	  mux_vect = 160'h0123456789123456789a23456789ab3456789abc;
	else
	  begin
	     mux_vect[39:0] = data_in[39:0];
	     mux_vect[79:40] = data_in[79:40];
	     mux_vect[119:80] = data_in[119:80];
	     mux_vect[159:120] = data_in[159:120];
	  end
	case (sel)
	  0: data_out = mux_vect[39:0];
	  1: data_out = mux_vect[79:40];
	  2: data_out = mux_vect[119:80];
	  3: data_out = mux_vect[159:120];
	endcase
     end
endmodule
