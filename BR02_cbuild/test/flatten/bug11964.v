// filename: test/flatten/bug11964.v
// Description: This test duplicates the problem found in a MIPS design, where an assert
//              was being issued because the same hierarchical reference was being added
//              twice to the top level scope. The problem was that the flattening process
//              was not recognizing that all the hierarchical references to the same
//              net where indeed identical, so duplicated were being created unnessarily.
//              The reason for this is that during flattening the module containing the
//              register being referenced hierarchically by other parts of the design had
//              been flattened into the parent module, resulting in a node that preserved
//              the original hierarchical of the register. The search algorithm didn't take
//              that into account, so it was never finding the flattened reference. The fix
//              was to recognize this situtation and extend the searching into the node.
//              This problem was already encountered in bug 8336, but never fixed until now.
//
`define TOP_MODULE_CFG bug11694.T1.M1

module bug11694(t_in, t_out);
   input [0:2] t_in;
   output [0:2] t_out;
   
   middle1 T1(t_in[0], t_out[0]);
   middle2 T2(t_in[1], t_out[1]);
   middle3 T3(t_in[2], t_out[2]);
   
endmodule

// This scope, after flattening, contains a symbol table node that includes
// the original hierarchy, like this: "M1.REG1".
module middle1(m_in, m_out);
   input m_in;
   output m_out;

   wire   temp;
   
   bottom1 M1(m_in, temp);
   failed  M2(temp, m_out);
   
endmodule

// This module is prevented from being flattened by the "disallowFlattening" directive.
// This in turn prevents module middle1 from getting flattened.
module failed(f_in, f_out);
   input f_in;
   output f_out;

   assign f_out = f_in;
   
endmodule


module middle2(m_in, m_out);
   input m_in;
   output m_out;

   pbottom2 M2(m_in, m_out);
      
endmodule

module middle3(m_in, m_out);
   input m_in;
   output m_out;

   pbottom3 M3(m_in, m_out);
   
endmodule

// This module is flattened into the parent module, middle1.
module bottom1(b_in, b_out);
   input b_in;
   output b_out;

   reg 	  REG1;

   initial
     REG1 = 1'b0;
   
   assign b_out = b_in;

endmodule

module pbottom2(b_in, b_out);
   input b_in;
   output b_out;

   bottom2 pb2(b_in, b_out);

endmodule

module pbottom3(b_in, b_out);
   input b_in;
   output b_out;

   bottom3 pb3(b_in, b_out);

endmodule


module bottom2(b_in, b_out);
   input b_in;
   output b_out;

   assign b_out = (`TOP_MODULE_CFG.REG1 == 1'b0) ? b_in : 0;

endmodule

module bottom3(b_in, b_out);
   input b_in;
   output b_out;

   assign b_out = (`TOP_MODULE_CFG.REG1 == 1'b0) ? b_in : 0;

endmodule
