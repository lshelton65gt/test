module top(rst, clk, sel, in0, in1, in2, in3, out);
   input rst, clk;
   input [1:0] sel;
   input [39:0] in0;
   input [39:0] in1;
   input [39:0] in2;
   input [39:0] in3;
   output [39:0] out;
   vector_mux vmux (rst, clk, sel, {in3, in2, in1, in0}, out);
endmodule

module vector_mux(rst, clk, sel, data_in, data_out);
   input rst, clk;
   input [1:0] sel;
   input [159:0] data_in;
   output [39:0] data_out;
   reg [39:0] 	 data_out;

   reg [159:0] 	 mux_vect;
 
   initial data_out = 0;

   always @(posedge clk)
     begin
	case (sel)
	  0:
	    begin
	       mux_vect[39:0] = data_in[39:0];
	       if (rst)
		 begin
		    mux_vect[39:0] = 40'h0000000001;
		    data_out = mux_vect[39:0];
		 end
	       else
		 begin
		    data_out = mux_vect[39:0];
		 end
	    end
	  1:
	    begin
	       mux_vect[79:40] = data_in[79:40];
	       if (rst)
		 begin
		    mux_vect[79:40] = 40'h0000000002;
		    data_out = mux_vect[79:40];
		 end
	       else
		 begin
		    data_out = mux_vect[79:40];
		 end
	    end
	  2:
	    begin
	       mux_vect[119:80] = data_in[119:80];
	       if (rst)
		 begin
		    mux_vect[119:80] = 40'h0000000003;
		    data_out = mux_vect[119:80];
		 end
	       else
		 begin
		    data_out = mux_vect[119:80];
		 end
	    end
	  3:
	    begin
	       mux_vect[159:120] = data_in[159:120];
	       if (rst)
		 begin
		    mux_vect[159:120] = 40'h0000000003;
		    data_out = mux_vect[159:120];
		 end
	       else
		 begin
		    data_out = mux_vect[159:120];
		 end
	    end
	endcase
     end
endmodule
