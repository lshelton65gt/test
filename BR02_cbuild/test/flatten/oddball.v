module top(clk,in,out);
  input clk;
  input in;
  output out;

  reg [1:0] mem [1:0];

  initial begin
    mem[0] = 0;
    mem[1] = 0;
  end

  sub s0 ( clk, in, 0, mem[0], out );
endmodule

module sub(clk,in,val,mval,out);
  input clk;
  input in;
  input val;
  input [1:0] mval;
  output out;

  reg out;

  initial out = 0;
  always @(posedge clk) begin
    if (val) 
      out = val | (|mval);
    else
      out = in;
  end
endmodule
