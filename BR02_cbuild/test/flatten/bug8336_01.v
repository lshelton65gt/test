// filename: test/flatten/bug8336_01.v
// Description:  This test duplicates the internal error seen in bug 8336.  The
// issue seems to be related to the fact that there are two hierrefs in
// reg_trace_0 (that is instanced twice) the two refs use two different paths to
// get to the same module, one path is prohibited from flattening (in this case
// with disallowFlattening (in the customer test it is probably not flattened
// because of size).

// as of 1.6301 with CDB (debug) this test ends with an internal error.
//
//******CARBON INTERNAL ERROR*******
//
//{bug8336_01.v:20}BitNetHierRef(0x40275880) : $href_level1.level2a.level3.loc_reg; : Scope(bug8336_01) : primary aliased allocated z read 
///home/cds/cloutier/W8336/src/nucleus/NUScope.cxx:155 NU_ASSERT(mNetHash.find(name) == mNetHash.end()) failed

// without CDB (product) the error is:
//
//******CARBON INTERNAL ERROR*******
//
//{bug8336_01.v:20}BitNetHierRef(0x402c5940) : $href_level1.level2a.level3.loc_reg; : Scope(bug8336_01) : primary aliased allocated read hierref-read hierref-written 
///home/cds/cloutier/Wmaint7/src/nucleus/NUNet.cxx:2117 NU_ASSERT(cmp != 0) failed


module bug8336_01(clock, in1, in2, out1, out2);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1, out2;
 
   level1 level1(clock, in1, in2, out1, out2);
 
 
   reg_trace_0 reg_trace_0();
   reg_trace_0  pc_trace_0();
   
 
endmodule
 
module reg_trace_0();		// carbon enableOutputSysTasks
   wire w1 = bug8336_01.level1.level2a.level3.loc_reg;
   wire w2 = bug8336_01.level1.level2b.level3.loc_reg;
 
   always @(posedge bug8336_01.clock)
      begin
	 $display("reg", w1, w2);
      end
endmodule


module level1(clock, in1, in2, out1, out2);
   input clock;
   input [7:0] in1, in2;
   output [7:0] out1, out2;
   
   level2a level2a(clock, in1, out1);
   level2b level2b(clock, in2, out2);
 
endmodule


module level2a(clock, in1, out1);
   input clock;
   input [7:0] in1;
   output [7:0] out1;
   
   level3 level3(clock, in1, out1);
 
endmodule

module level2b(clock, in1, out1); // carbon disallowFlattening
   input clock;
   input [7:0] in1;
   output [7:0] out1;
   
   level3 level3(clock, in1, out1);
 
endmodule
 

module level3(clock, inA, out1); 
   input clock;
   input [7:0] inA;
   output [7:0] out1;
   reg [7:0] out1;
 
   reg      loc_reg;
   wire     loc_wire;

   assign loc_wire = inA[3];
 
   always @(posedge clock)
     begin: main
	{loc_reg,out1} = inA + loc_wire;
     end
endmodule
 

