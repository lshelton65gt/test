module top(out, in);
  output out;
  input in;

  // Local nets (2)
  wire         [0:15]         clk_vec;
  wire         [10:25]        c3;

  // Continuous Assigns
  assign out = c3[10];                             // bselpsel1.v:7
  assign clk_vec[1:15] = 15'h002a;                 // bselpsel1.v:5
  assign clk_vec[0] = in;                          // bselpsel1.v:5
  assign c3[10:17] = clk_vec[0:7];                 // bselpsel1.v:6
endmodule // top
