module top(out, in);
  output out;
  input  in;

  wire [0:15] clk_vec = {in, 15'h2a};
  wire [30:37]  c1 = clk_vec[0:7];
  wire [50:57]  c2 = clk_vec[8:15];
  wire [10:25]  c3 = {c1, c2};
  assign        out = c3[10];   // should be "in"
endmodule
