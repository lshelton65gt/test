module top (out, clk, in, hold);
   output out;
   input  clk, in, hold;

   wire   dclk = clk & ~hold;

   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top
