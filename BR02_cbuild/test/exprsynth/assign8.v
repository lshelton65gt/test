module top (out1, out2, clk, in, hold1, hold2);
   output out1, out2;
   input  clk, in, hold1, hold2;

   wire [1:0] all_hold = {hold1, hold2};
   wire       no_hold = & all_hold;
   wire       dclk = clk & no_hold;

   reg 	  out1;
   always @ (posedge dclk)
     out1 <= in;
   assign out2 = no_hold;

endmodule // top
