module top (out, in1, in2);
   output out;
   input [-1:0] in1, in2;

   wire [-1:0] 	c = { in1[-1] & in2[-1], in1[0] & in2[0] };
   assign 	out = c[-1] | c[0];

endmodule // top

   
   
