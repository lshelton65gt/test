module top (out, clk, in, hold);
   output out;
   input  clk, in;
   input [1:0] hold;

   wire   dclk = clk & ~hold[0];

   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top
