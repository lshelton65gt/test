module top (out1, out2, out3, out4, in1, in2, in3, in4);
   output [31:0] out1, out2, out3, out4;
   input [31:0]  in1, in2, in3, in4;
   reg [31:0]    out1, out2, out3, out4;

   reg [31:0]    r1, r2;
   integer       i1, i2;
   always @ (in1 or in2 or in3 or in4)
     begin
        // Go from signed to unsigned
        r1 = -1;
        r2 = -8;
        out1 = r1 & in1;
        out2 = r2 ^ in2;

        // Go from unsigned to signed
        i1 = r1;
        i2 = r2;
        out3 = {(i1 < i2), in3[30:0]};
        out4 = {(i1 > i2), in4[30:0]};
     end

endmodule

        
