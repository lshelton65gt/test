module top (out, in);
   output [1:0] out;
   input [1:0] 	in;
   
   wire [1:0] 	c1;
   assign 	{ c1[0], c1[1] } = in;
   assign 	out = { c1[0] == 1'b0, c1[1] == 1'b1};

endmodule // top
