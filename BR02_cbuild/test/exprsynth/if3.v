module top (out, clk, in, sel);
   output out;
   input  clk;
   input  in, sel;
   
   reg 	  dclk;
   always @ (clk or sel)
     if (sel)
       dclk = clk;

   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top
