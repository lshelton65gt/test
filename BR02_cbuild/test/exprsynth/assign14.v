module top( id2_23_, a );
   input [33:0] a;
   output [1:0] id2_23_;
   wire [8:0] 	id1;
   wire [23:0] 	id4;
   wire 	id3_nzero, id1_nzero, a_7_, a_nzero;
   wire 	id2_24_;
   wire [1:0] 	id2_23_;
   wire [33:0] 	const_26_0x0_;
   wire 	const_27_0_, const_28_0_;
   reg [33:0] 	id2;

   assign 	const_26_0x0_= 1024'h0;
   assign 	id1= (a[9:0]);
   assign 	const_27_0_= 1024'h0;
   assign 	const_28_0_= 1024'h0;
   assign 	id4= (a[33:0]);
   assign 	id1_nzero= ((|id1[8:0]));
   assign 	a_7_= ((|id4[23:0]));
   assign 	id3_nzero = a[1];
   assign 	a_nzero= (id3_nzero|a_7_);
   assign 	id2_24_= a[0];
   assign 	id2_23_={ a_nzero,id2_24_};

   always @ (id2_23_[1:0]  or const_26_0x0_ or a or const_27_0_ or const_28_0_)
     begin
      case (id2_23_[1:0])
	2'd3: id2 = (const_26_0x0_[33:0]);
	2'd2: id2 = (a[33:0]);
	2'd1: id2 = (const_27_0_);
	2'd0: id2 = (const_28_0_);
	default: id2 = 34'bx;
      endcase
   end

endmodule
