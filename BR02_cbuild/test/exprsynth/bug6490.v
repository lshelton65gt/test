// Generated with: ./generator -b  -n -363691954 -s 50 -o candidate.v
// Using seed: 0xea52804e
// Writing circuit of size 50 to file 'candidate.v'
module top(clk, qFZn, vh, DoQ, d, KgWjm, QZdG, bYKNHV, BUDd, i, vT, G, kZNv, zdaLe, t);
input clk;
output qFZn, vh, DoQ, d, KgWjm, QZdG, bYKNHV, BUDd, i, vT, G, kZNv, zdaLe, t;

  // net declarations
  wire clk;
  reg  [31:0] Iab=0;
  reg  [31:0] mctK=0;
  reg  [31:0] vgBV=0;
  reg  [31:0] nlch=0;
  reg  [31:0] Nfd=0;
  wire  signed [64:50] zHt;
  reg   signed [6:116] QgLi=0;
  wire  signed [75:71] x;
  wire  signed U;
  reg  [123:57] DLBrM=0;
  reg  Z=0;
  wire _;
  reg  [41:19] R=0;
  wire [29:123] cp;
  wire [56:14] OABt;
  wire [43:124] Vx;
  reg  [55:104] MycL=0;
  reg   signed pzR=0;
  reg  sZXz=0;
  wire [18:41] e;
  reg  [62:7] aR=0;
  reg  [97:64] dR=0;
  wire  signed Xa;
  wire qFZn;
  wire OiZ;
  wire  signed rg;
  wire  signed vh;
  wire  signed [51:108] igOiq;
  wire [81:94] ZNyI;
  wire M;
  wire  signed ptR;
  wire JkXt;
  reg  [52:123] BZDqS=0;
  wire [12:13] DoQ;
  wire [91:61] JIhPX;
  reg  [56:39] b=0;
  wire T;
  wire [41:86] KSfDs;
  wire  signed [73:55] u;
  wire [17:107] d;
  reg  LB=0;
  wire [51:105] BJ;
  wire [8:63] Vd;
  wire  signed [22:79] KgWjm;
  wire [41:72] mCN;
  reg  [30:37] avMJx=0;
  wire  signed QZdG;
  reg  [43:61] bYKNHV=0;
  wire BUDd;
  wire [86:27] i;
  wire [122:94] vT;
  reg   signed [81:52] G=0;
  reg  [2:16] kZNv=0;
  wire  signed [23:26] zdaLe;
  wire [112:63] t;


  // data generators
  initial begin
    Iab = 32'h5c725a5c;
  end
  always@(posedge clk)
  begin
    Iab = 279470273*(Iab % 15) - 102913196 * (Iab / 15);
  end

  initial begin
    mctK = 32'h5eb3b15a;
  end
  always@(posedge clk)
  begin
    mctK = 279470273*(mctK % 15) - 102913196 * (mctK / 15);
  end

  initial begin
    vgBV = 32'h40cbb3d1;
  end
  always@(posedge clk)
  begin
    vgBV = 279470273*(vgBV % 15) - 102913196 * (vgBV / 15);
  end

  initial begin
    nlch = 32'h544fd34c;
  end
  always@(posedge clk)
  begin
    nlch = 279470273*(nlch % 15) - 102913196 * (nlch / 15);
  end

  initial begin
    Nfd = 32'h74d96cdb;
  end
  always@(posedge clk)
  begin
    Nfd = 279470273*(Nfd % 15) - 102913196 * (Nfd / 15);
  end


  // assignments
  assign zHt[54 : 52] = (Nfd!=(Iab[16 : 15]*mctK[(~((((vgBV[31 : 25]==11'h528)&(43'he5e6dd31+29'h12be3f7e))!=(|(49'h59e44a67>>24'h93c6dd))) ? 10'h22c : (|Nfd[22 : 3]))) +: 21]));

  always@(posedge clk)
  begin
    QgLi = 68'h8a1653f5;
  end

  assign x = ((zHt[mctK +: 15]>>QgLi)*Nfd);

  assign U = zHt[53 : 53];

  always@(posedge clk)
  begin
    DLBrM[122 : 78] = 119'h16e9ce69;
  end

  always@(posedge clk)
  begin
    Z = QgLi[100 : 103];
  end

  assign _ = (~zHt[64 : 50]);

  always@(posedge clk)
  begin
    R[29 : 22] = zHt;
  end

  assign cp = ((90'hfbed427a&&({({(nlch[22 : 2]*DLBrM[({U,(Iab[14 : 4]<30'hcf864b8),115'hf487c517,Iab}) -: 32]),(-(((~124'h2e495a5f)*((101'h3b8dd58f>=110'h8f71a56b)<=zHt))<<(Z<<((~Z) ? (U||90'hd844ab34) : (71'h449561==mctK)))))})}))^110'h52d8c3ee);

  assign OABt[23 : 16] = _;

  assign Vx = (((105'hd5abbceb==(OABt[39 : 32]<=(!((((Nfd^Iab[18 : 15])&(113'h642bae59&90'h203de2b))*120'h4b9096fa)|QgLi))))&&(|30'h2c424ac7))^20'ha68f9);

  always@(posedge clk)
  begin
    MycL[55 : 104] = DLBrM[70 : 63];
  end

  always@(posedge clk)
  begin
    pzR = (16'h8d4d>=zHt[60 : 58]);
  end

  always@(posedge clk)
  begin
    sZXz = (R>>pzR);
  end

  assign e[36 : 41] = ((((((^_)^Z)&(pzR&(&(nlch[29 : 29]|(&68'ha55770e1)))))<<vgBV[31 : 0])&(((QgLi[6 : 116]||60'h6dad6b48)&&Z)>MycL))==cp);

  always@(posedge clk)
  begin
    aR = (MycL||(e^(((((((79'hfdf42ff-12'h872)+(Vx[43 : 103] ? 64'h9458b840 : x)) ? ((!72'h3ecb0828)>(72'hd8205e62 ? 77'hfae3b325 : x)) : Vx)+(vgBV==77'h48c4b7c6))>(((&R[33 : 33])||((Z-cp)<=({105'he4264064,42'h7c2efc39})))!=(mctK[4 : 0]&&(nlch[31 : 0]<=(48'hc5ca5545 ? MycL : 81'ha2403657)))))<=MycL[103 : 103])!=(96'h2d25f444-((&_)<=120'h5ba6bb9)))));
  end

  always@(posedge clk)
  begin
    dR[93 : 68] = Z;
  end

  assign Xa = (&(71'h3309549e||U));

  assign qFZn = mctK;

  assign OiZ = (({Z,(-Nfd[31 : 0]),x[73 : 71]})>Nfd[8 : 5]);

  assign rg = ((8'h22^U)||aR[(110'hb22c8a02||59'hcb5a38a5) +: 31]);

  assign vh = (((((x ? OiZ : pzR)-(56'h6e961e67-((({66'h3b381003,Nfd[mctK[29 : 26] +: 2],(74'h4244669c^43'h57959b7d)}) ? (|dR[97 : 97]) : 81'h8e2e9cd2)+(&((23'h3cb5eb+e[21 : 27])*(^122'hfad2a298))))))<<Iab)+(Nfd[31 : 0]>cp[92 : 115]))&&(55'he4e0a643-vgBV[DLBrM[92 : 59] +: 27]));

  assign igOiq = (^OABt[56 : 14]);

  assign ZNyI = 63'h9a032f32;

  assign M = 40'h2e3cffa6;

  assign ptR = (-pzR);

  assign JkXt = Vx;

  always@(posedge clk)
  begin
    BZDqS = ((((((89'h2c20d34f*88'h8eb055f4)>=igOiq[(JkXt==pzR) -: 41])<=120'h1535a787) ? (17'h1f889<R[34 : 20]) : 82'hadcbf371)!=mctK[28 : 1])+cp[29 : 123]);
  end

  assign DoQ[12 : 13] = (((((86'h74786462!=Iab)<<JkXt)^(aR==((|((96'h59afc38>=rg)^(-nlch[zHt[50 : 50] +: 9])))^(10'h26d^((^nlch)*((~7'h7f)^(112'ha98a14db>=108'he8a1e6b8)))))))>=(vgBV[2 : 1] ? QgLi[29 : 81] : ((OABt[(zHt[64 : 63]&(108'h2430d5a1|125'hfe66e20a)) +: 24]-(((38'h41dafb8b^x)==(JkXt|73'hdf50af01))<<(((89'hd0d90d8e>>39'hdc4b7e9c)||2'h2)!=((121'h79081484<vgBV[31 : 0])>>R))))|47'h57b51ac9)))|R[32 : 26]);

  assign JIhPX = x;

  always@(posedge clk)
  begin
    b[56 : 39] = (JIhPX*35'h5b04c969);
  end

  assign T = rg;

  assign KSfDs[83 : 86] = (BZDqS ? (((M<=28'h4fd83d0)&e[33 : 41])||(BZDqS||nlch)) : (U+((64'h9c5ea162|(x[74 : 74]*91'h7b13d10))!=({7'h7d,124'h4bc70d31,(7'hd*T)}))));

  assign u[73 : 60] = ((-((JIhPX[88 : 79]<<R)<74'hb7b60ff3))<=U);

  assign d[69 : 88] = (112'h8582f0ab^sZXz);

  always@(posedge clk)
  begin
    LB = (87'h91912e0e^((89'ha23e0ab8&(^M))^94'hb88d3f63));
  end

  assign BJ = (~((&ZNyI[81 : 81])^_));

  assign Vd = ((&(~Xa))^67'hb9bc4137);

  assign KgWjm = (BJ<=(Vx^((({R[30 : 22],105'h113cec34})+40'hc4cb3d1d)&&cp)));

  assign mCN[70 : 71] = ((105'h9bb48de9>=ptR)<<cp[36 : 36]);

  always@(posedge clk)
  begin
    avMJx[32 : 35] = Nfd;
  end

  assign QZdG = Nfd[(zHt[55 : 52]*(({(Vd[50 : 57]>>(112'h93780268|((nlch!=34'ha5a5034)^(|75'hd3fcebde)))),avMJx,JkXt})^(|(R ? MycL : (^((82'h2434a6e8 ? 45'h25f3afec : _)!=zHt)))))) -: 7];

  always@(posedge clk)
  begin
    bYKNHV = (-(|(-((((&Iab[7 : 3])&&u[73 : 55])&&(dR[97 : 64]|(((igOiq[51 : 108]>121'h2d88e7d4)&(-3'h4))<=_)))-(nlch[30 : 30]-75'h1113e0da)))));
  end

  assign BUDd = (((23'h676c00<<(((mCN&&Xa)==(4'h9>>((~(25'h5f5017 ? 104'h76f9af6f : 77'hc3b5c86a))==((~zHt[64 : 50])^(u[66 : 64]<<24'hd13902)))))==((^KSfDs[41 : 86])&98'habd2cefa)))-Vx)||66'h22a2838c);

  assign i[36 : 35] = ((&nlch[31 : 0])<=(68'hf7dbe4c7<OABt[37 : 29]));

  assign vT[102 : 97] = 14'h2915;

  always@(posedge clk)
  begin
    G = ((mctK[30 : 26]==(^(^(^(-86'h2bf1605e)))))^OABt[56 : 15]);
  end

  always@(posedge clk)
  begin
    kZNv[2 : 16] = b;
  end

  assign zdaLe = (M!=ZNyI);

  assign t[91 : 90] = ((&pzR)-LB);

endmodule

