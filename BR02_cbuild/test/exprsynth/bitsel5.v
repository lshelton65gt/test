module top (out, in1, in2);
   output out;
   input [0:-1] in1, in2;

   wire [0:-1] 	c = { in1[-1] & in2[-1], in1[0] & in2[0] };
   assign 	out = c[-1] | c[0];

endmodule // top

   
   
