module top (out, clk, hold, in);
   output [1:0] out;
   input 	clk, hold;
   input [1:0] 	in;

   reg [1:0] 	dclk;
   always @ (clk or hold)
     if (hold)
       begin
	  dclk[0] = 1'b1;
	  dclk[1] = 1'b0;
       end
     else
       begin
	  dclk[0] = clk;
	  dclk[1] = clk;
       end

   reg [1:0] out;
   wire      clk1 = dclk[0];
   wire      clk2 = dclk[1];
   always @ (posedge clk1)
     out[0] <= in[0];
   always @ (posedge clk2)
     out[1] <= in[1];

endmodule // top
