module top (out1, out2, clk, in, sel);
   output out1, out2;
   input  clk;
   input  in, sel;
   reg 	  dclk;
   reg 	  out1;
   reg 	  out2;
   
   always @ (clk or sel)
     if (sel)
       begin
	  dclk = clk;
	  out2 = clk;
       end
     else
       begin
	  dclk = ~clk;
       end

   always @ (posedge dclk)
     out1 <= in;

endmodule // top
