module top (out, clk, in, hold);
   output [1:0] out;
   input [1:0] 	clk, in, hold;

   wire [1:0] 	no_hold;
   assign 	no_hold[0] = ~hold[1];
   assign 	no_hold[1] = ~hold[0];
   wire 	dclk1 = clk[0] & (no_hold != 1'b0);
   wire 	dclk2 = clk[1] & (no_hold != 1'b1);

   reg [1:0] 	out;
   always @ (posedge dclk1)
     out[0] <= in[0];
   always @ (posedge dclk2)
     out[1] <= in[1];

endmodule // top
