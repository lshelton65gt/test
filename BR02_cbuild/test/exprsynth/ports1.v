// This test is for mis-sized ports
module top(out1, out2, out3, out4, out5, out6, in1, in2, in3, in4, in5, in6);
   output [127:0] out1;
   output [63:0]  out2;
   output [31:0]  out3;
   output [15:0]  out4;
   output [7:0]   out5;
   output [3:0]   out6;
   input [127:0]  in1;
   input [63:0]   in2;
   input [31:0]   in3;
   input [15:0]   in4;
   input [7:0]    in5;
   input [3:0]    in6;

   // Make a bunch of instances where we give a smaller output port
   wire [95:0]    c1;
   wire [47:0]    c2;
   wire [23:0]    c3;
   wire [11:0]    c4;
   wire [5:0]     c5;
   wire [1:0]     c6;
   sub1 S1_smallo (c1, in1);
   sub2 S2_smallo (c2, in2);
   sub3 S3_smallo (c3, in3);
   sub4 S4_smallo (c4, in4);
   sub5 S5_smallo (c5, in5);
   sub6 S6_smallo (c6, in6);

   // Make a bunch of instances where we give a larger output port
   wire [159:0]   c7;
   wire [95:0]    c8;
   wire [47:0]    c9;
   wire [23:0]    c10;
   wire [11:0]    c11;
   wire [5:0]     c12;
   sub1 S1_largeo (c7, in1);
   sub2 S2_largeo (c8, in2);
   sub3 S3_largeo (c9, in3);
   sub4 S4_largeo (c10, in4);
   sub5 S5_largeo (c11, in5);
   sub6 S6_largeo (c12, in6);
   
   // Make a bunch of instances where we give a smaller input port
   wire [127:0]   c13;
   wire [63:0]    c14;
   wire [31:0]    c15;
   wire [15:0]    c16;
   wire [7:0]     c17;
   wire [3:0]     c18;
   sub1 S1_smalli (c13, c1);
   sub2 S2_smalli (c14, c2);
   sub3 S3_smalli (c15, c3);
   sub4 S4_smalli (c16, c4);
   sub5 S5_smalli (c17, c5);
   sub6 S6_smalli (c18, c6);

   // Make a bunch of instances where we give a larger input port
   wire [127:0]   c19;
   wire [63:0]    c20;
   wire [31:0]    c21;
   wire [15:0]    c22;
   wire [7:0]     c23;
   wire [3:0]     c24;
   sub1 S1_largei (c19, c7);
   sub2 S2_largei (c20, c8);
   sub3 S3_largei (c21, c9);
   sub4 S4_largei (c22, c10);
   sub5 S5_largei (c23, c11);
   sub6 S6_largei (c24, c12);

   // Make the outputs live
   assign         out1 = c13 ^ c19;
   assign         out2 = c14 ^ c20;
   assign         out3 = c15 ^ c21;
   assign         out4 = c16 ^ c22;
   assign         out5 = c17 ^ c23;
   assign         out6 = c18 ^ c24;
   
endmodule

module sub1 (o1, i1);
   output [127:0] o1;
   input [127:0]  i1;

   assign         o1 = ~i1;

endmodule

module sub2 (o2, i2);
   output [63:0] o2;
   input [63:0]  i2;

   assign        o2 = ~i2;

endmodule

module sub3 (o3, i3);
   output [31:0] o3;
   input [31:0]  i3;

   assign        o3 = ~i3;

endmodule

module sub4 (o4, i4);
   output [15:0] o4;
   input [15:0]  i4;

   assign        o4 = ~i4;

endmodule

module sub5 (o5, i5);
   output [7:0] o5;
   input [7:0]  i5;

   assign        o5 = ~i5;

endmodule

module sub6 (o6, i6);
   output [3:0] o6;
   input [3:0]  i6;

   assign        o6 = ~i6;

endmodule
