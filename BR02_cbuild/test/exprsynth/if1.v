module top (out, clk, in, hold);
   output out;
   input  clk, in, hold;

   reg 	  dclk;
   always @ (clk or hold)
     if (hold)
       dclk = 1'b1;
     else
       dclk = clk;

   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top
