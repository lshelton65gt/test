module top (out, clk, in, sel);
   output out;
   input [1:0] clk;
   input  in, sel;
   
   reg 	  dclk;
   always @ (clk or sel)
     if (sel)
       dclk = clk[0];
     else
       dclk = clk[1];

   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top
