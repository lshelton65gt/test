module top (out, in1, in2, en1, en2);
   output out;
   input  in1, in2, en1, en2;

   wire   bus;
   sub S1 (bus, in1, en1);
   sub S2 (bus, in2, en2);

   assign out = ~bus;

endmodule // top

module sub (out, in, en);
   output out;
   input  in, en;

   assign out = en ? in : 1'bz;

endmodule // sub

   
