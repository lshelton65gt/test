module top (out, in1, in2);
   output [3:0] out;
   input [3:0] in1, in2;

   wire [7:0]  a = { in1, in2 };
   wire [7:0]  b = { in2, in1 };
   wire [3:0]  c1 = a[7:4] & b[7:4];
   wire [3:0]  c2 = a[3:0] | b[3:0];
   assign      out = c1 | c2;

endmodule // top

   
   
