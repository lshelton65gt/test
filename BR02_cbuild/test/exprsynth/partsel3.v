module top (out, in1, in2);
   output [1:0] out;
   input [3:0] in1, in2;

   wire [7:4]  a = { in1[1:0], in2[3:2] };
   wire [4:7]  b = { in2[1:0], in1[3:2] };
   wire [1:0]  c1 = a[5:4] & b[4:5];
   wire [1:0]  c2 = a[7:6] & b[6:7];
   assign      out = c1 | c2;

endmodule // top

   
   
