module top(out, in);
  output out;
  input  in;

  wire [0:15] clk_vec = {in, 15'h2a};
  wire [10:25] c3 = {clk_vec[0:7], clk_vec[8:15]};
  assign        out = c3[10];   // should be "in"
endmodule
