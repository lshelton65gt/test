// last mod: Thu May 19 17:05:23 2005
// filename: test/exprsynth/bug4472.v
// Description:  This test is a trimmed down version of test/rangen/fold/random1.v
// it would NU_ASSERT at one time, note the bitselect of the scalar A1

module top(out);
   output out;

   wire   clk;
   reg 	  A1;

   initial
     begin
	A1 = 0;
     end

   assign out = (&((A1[0]) != (A1[1]))  ); // try to bitselect a scalar

endmodule
