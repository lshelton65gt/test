module top (out, in1, in2);
   output [1:0] out;
   input [3:0] in1, in2;

   wire [1:0]  a1 = in1[1:0];
   wire [1:0]  a2 = in1[3:2];
   wire [1:0]  b1 = in2[1:0];
   wire [1:0]  b2 = in2[3:2];
   wire [1:0]  c1 = a1 & b1;
   wire [1:0]  c2 = a2 & b2;
   assign      out = c1 | c2;

endmodule // top

   
   
