module top (out, clk, in, hold1, hold2);
   output out;
   input  clk, in, hold1, hold2;

   reg 	  dclk;
   always @ (clk or hold1 or hold2)
     if (hold1)
       dclk = 1'b1;
     else
       begin
	  if (hold2)
	    dclk = 1'b0;
	  else
	    dclk = clk;
       end
   
   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top
