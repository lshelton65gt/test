module top (out, in1, in2);
   output out;
   input [3:4] in1, in2;

   wire [1:2]  c = { in1[4] & in2[4], in1[3] & in2[3] };
   assign      out = c[2] | c[1];

endmodule // top

   
   
