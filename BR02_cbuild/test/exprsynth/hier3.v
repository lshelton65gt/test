module top (out, in1, in2);
   output out;
   input  in1, in2;

   wire   c1, c2, c3;
   sub S1 (c1, in1, in2);
   sub S2 (c2, ~in1, ~in2);
   sub S3 (c3, ~c1, c2);

   assign out = ~c3;

endmodule // top

module sub (out, a, b);
   output out;
   input  a, b;

   assign out = a & b;

endmodule // sub

   
