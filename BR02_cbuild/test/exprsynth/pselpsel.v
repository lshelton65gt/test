module top(zero, clk);
  output [7:0] zero;
  input  clk;

  wire [0:15] clk_vec;
  assign      clk_vec[0:7] = {clk, 7'h2a};
  assign      clk_vec[8:15] = clk_vec[0:7];
  assign      zero = clk_vec[0:7] - clk_vec[8:15];
endmodule
