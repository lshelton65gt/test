module top (out, clk, in, hold1, hold2);
   output out;
   input  clk, in, hold1, hold2;

   wire [1:0] all_hold = {hold1, hold2};
   wire       no_hold = (all_hold == 2'b00);
   wire       dclk = clk & no_hold;

   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top
