module top(in, out);
  input [15:0] in;
  output [8:0] out;

  sub sub(in, out);
endmodule

module sub(sub_in, sub_out);
  input [115:100] sub_in;
  output [8:0]    sub_out;

  assign          sub_out = sub_in[115:108]; // note that RHS is 8 bits, LHS is 9 bits
endmodule
