module top (out1, out2, in1, in2);
   output [3:0] out1, out2;
   input [3:0] 	in1, in2;

   wire [3:0] 	c1, c2;
   assign 	{ c1, c2 } = { in1[1:0], in2[1:0], in1[3:2], in2[3:2] };
   assign 	out1 = c1 & 4'ha;
   assign 	out2 = c2 ^ 4'h5;
   
endmodule // top
