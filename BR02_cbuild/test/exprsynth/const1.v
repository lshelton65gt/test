module top (out, clk, in, hold);
   output out;
   input  clk, in, hold;

   sub S1 (dclk, clk, hold, 1'b1);

   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top

module sub (clkout, clkin, hold, val);
   output clkout;
   input  clkin, hold, val;

   wire   clkout = clkin & ~hold & val;

endmodule
   
