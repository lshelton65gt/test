module top(out, in);
  output out;
  input  in;

  wire [0:15] clk_vec = {in, 15'h2a};
  wire [30:15] c1 = clk_vec;
  assign        out = c1[30];   // should be "in"
endmodule
