module top (out1, out2, clk, in, hold);
   output out1, out2;
   input  clk, in, hold;

   reg 	  dclk, no_hold;
   always @ (clk or hold)
     begin
	no_hold = ~hold;
	dclk = clk & no_hold;
     end

   reg 	  out1;
   always @ (posedge dclk)
     out1 <= in;
   assign out2 = no_hold;

endmodule // top
