module extend(in, out);
  input in;
  output [2:0] out;

  wire         tmp = ~in;
  assign       out = tmp;       // should extend 1 to 001
endmodule
