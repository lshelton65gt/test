module top (out, in1, in2);
   output out;
   input [1:0] in1, in2;

   wire        a1 = in1[0];
   wire        a2 = in1[1];
   wire        b1 = in2[0];
   wire        b2 = in2[1];
   wire        c1 = a1 & b1;
   wire        c2 = a2 & b2;
   assign      out = c1 | c2;

endmodule // top

   
   
