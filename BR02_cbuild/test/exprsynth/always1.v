module top (out, clk, in, hold);
   output out;
   input  clk, in, hold;

   reg 	  dclk;
   always @ (clk or hold)
     dclk = clk & ~hold;

   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top
