module top (out1, out2, in1, in2);
   output [3:0] out1, out2;
   input [3:0] 	in1, in2;

   wire [3:0] 	c1, c2;
   sub S1 ({c1, c2}, {in1, in2}, {in2, in1});

   assign 	out1 = ~c1;
   assign 	out2 = ~c2;

endmodule // top

module sub (z, x, y);
   output [7:0] z;
   input [7:0] 	x, y;

   assign 	z = x ^ y;

endmodule // sub
