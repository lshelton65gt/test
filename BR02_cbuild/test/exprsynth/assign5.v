module top (out, clk, in, hold);
   output out;
   input  clk, in, hold;

   wire   dclk = hold ? clk : 1'b0;

   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top
