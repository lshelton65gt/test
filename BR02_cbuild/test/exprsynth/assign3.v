module top (out, clk, in, hold);
   output out;
   input  clk, in, hold;

   wire   no_hold = ~hold;
   wire   dclk = clk & no_hold;

   reg 	  out;
   always @ (posedge dclk)
     out <= in;

endmodule // top
