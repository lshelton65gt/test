`protect
// Here's a block that'll be protected in the .vp file.
`endprotect

module myflop(d, clk, q);

   input d;
   input clk;
   output q;
   reg    q;

   always @(posedge clk)
     q <= d;

endmodule // myflop
