# This test is derived from part of test/licensing.  Since that test
# runs only once a day, some of the testing is duplicated here.  If
# you add something here you probably should also add it there.

# Create .cem file
vspcompiler -q -cemgen myflop myflop.v >& cemgen.log

# Create normal .vp file
rm -f myflop.vp
# -generateCleartextSource doesn't make sense here, but test it anyway.
vspcompiler -q -generateCleartextSource +protect myflop.v >& protect.log

# Create +protectHDLOnly file into .vpHDLOnly
rm -f myflop.vpHDLOnly
vspcompiler -q -generateCleartextSource +protect.vpHDLOnly +protectHDLOnly myflop.v >& protectHDLOnly.log

# Use -generateCleartextSource on a normal unencrypted file.
vspcompiler -q -generateCleartextSource myflop.v >& myflop.v.gencleartextsource.log

# Verify that generated code is in fact cleartext.
file .carbon.libdesign/schedule.cxx | grep -i ascii >& grepascii.log

# Don't use -generateCleartextSource on a normal unencrypted file.
vspcompiler -q myflop.v >& myflop.v.genencryptedsource.log

# Verify that generated code is in encrypted.
file .carbon.libdesign/schedule.cxx | grep -v -i ascii >& grepnotascii.log

# Make model from .vp file
VTSKIP vspcompiler -q myflop.vp >& myflop.cbld.log

# Make model with -generateCleartextSource from regular .vp file.  This
# should always fail.
VTSKIP PEXIT vspcompiler -q -generateCleartextSource myflop.vp >& myflop.gencleartextsource.log

# Make model with -noEncrypt from regular .vp file.  This should pass.
VTSKIP vspcompiler -q -noEncrypt myflop.vp >& myflop.noencrypt.log

# Make model with -generateCleartextSource from .vpHDLOnly file.  This
# should pass.
VTSKIP vspcompiler -q -generateCleartextSource myflop.vpHDLOnly >& myflop.vpHDLOnly.gencleartextsource.log

# Make model from .cem file, should always pass.
VTSKIP vspcompiler -q libdesign.cem >& cem.log

# Make model with -generateCleartextSource from .cem file.  This should
# always fail.
VTSKIP PEXIT vspcompiler -q -generateCleartextSource libdesign.cem >& cem.gencleartextsource.log

# Make model with -noEncrypt from .cem file.
VTSKIP vspcompiler -q -noEncrypt libdesign.cem >& cem.noencrypt.log

# Test nonsensical specification of +protectHDLOnly without +protect
vspcompiler -q myflop.v +protectHDLOnly >& optionRequiresOption.log
