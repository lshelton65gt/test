// In this varation, we include rom.v, but we set the constant factory back
// to the default (it is overridden in write-test.run).  This way we should
// get no congruence.  Look at rom3.cmdOpts

`include "rom.v"
