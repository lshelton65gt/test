// This testcase should not show any congruence because as of now
// we don't have the mechanism built in to turn varsel ConstantRange
// into input ports, because there is no NUConst*.  We can do this
// at a later date.

module SUBMOD(datain,clk,dataout);

parameter param = 4'd1;

input [3:0] datain;
input clk;

output dataout;
reg    dataout;

always @ (posedge clk)
	dataout = datain [param - 1];

endmodule

module PARAM1(in1, in2, clk, out1, out2);
input [3:0] in1, in2;
input clk;

output out1, out2;
wire out1, out2;

SUBMOD #(2)
inst1(.datain(in1),.clk(clk),.dataout(out1));

SUBMOD #(3)
inst2(.datain(in2),.clk(clk),.dataout(out2));

endmodule


