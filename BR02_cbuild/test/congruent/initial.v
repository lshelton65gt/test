
module top (in, out1, out2, out3);
   output out1, out2, out3;
   input  in;

   sub1 s1 (out1);
   sub2 s2 (out2);
  assign  out3 = in;

endmodule // top

// sub1 and sub2 look really congruent to me.  But unfortunately if we change
// constants in initial blocks into port nets, cbuild drops the whole assign
// statement and gets the wrong answer.  :(

module sub1 (out);
   output out;
   reg    out;

   initial out = 0;

endmodule

module sub2 (out);
   output out;
   reg    out;

   initial out = 1;

endmodule
