// This testcase illustrates that two ROMs are not equivalent if they
// do not read the same data files.  We had a problem with this because
// (a) NUReadmemX does not keep its args as expressions, and so the
//     NUDesignWalker did not reach them
// (b) An explicit callback for NUReadmemX is never called, because
//     NUDesignWalker was broken in that respect
// This is from bug5363

module bug(clk, addr, data1, data2);
   input clk;
   input [7:0] addr;
   output [15:0] data1, data2;

   rom #("rom1.dat") rom1(.clk(clk), .addr(addr), .data(data1));
   rom #("rom2.dat") rom2(.clk(clk), .addr(addr), .data(data2));
endmodule

module rom(clk, addr, data);
   parameter init_file = "";
   input clk;
   input [7:0] addr;
   output [15:0] data;
   reg [15:0] data;

   reg [15:0] mem [0:255];

   initial
     $readmemh(init_file, mem);

   always @(posedge clk)
     data <= mem[addr];
endmodule
