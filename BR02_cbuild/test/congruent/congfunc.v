module top(i1, i2, o1, o2, clk);
  input i1, i2, clk;
  output o1, o2;

  function inv1;
    input in;
    inv1 = ~in;
  endfunction // inv1

  function inv2;
    input in;
    inv2 = ~in;
  endfunction // inv1

  reg     o1, o2;
  always @(posedge clk) begin
    o1 <= inv1(i1);
    o2 <= inv2(i2);
  end
endmodule // top
