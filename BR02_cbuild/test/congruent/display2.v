module param_disp(clk, val);
  input clk;
  input val;

  display1 u1(clk, val);
  display2 u2(!clk, val);
endmodule

module display1(clk, val);
  input clk, val;

  always @(posedge clk)
    $display("hello, world!   %d", val);
endmodule

module display2(clk, val);
  input clk, val;

  always @(posedge clk)
    $display("goodbye, world! %d", val);
endmodule
