module top(rst, clk, out1, out2);
  input rst, clk;
  output [1:0] out1, out2;

  mid1 mid1(rst, clk, out1);
  mid2 mid2(rst, clk, out2);
endmodule

module mid1(rst, clk, out);
  input rst, clk;
  output [1:0] out;

  sub1 sub(rst, clk, out);
endmodule

module mid2(rst, clk, out);
  input rst, clk;
  output [1:0] out;

  sub2 sub(rst, clk, out);
endmodule

module sub1(rst, clk, out);
  input rst, clk;
  output [1:0] out;
  reg [1:0]    out;

  always @(posedge clk)
    if (rst)
      out <= 2'b00;
    else
      out <= 2'b11;
endmodule

module sub2(rst, clk, out);
  input rst, clk;
  output [1:0] out;
  reg [1:0]    out;

  always @(posedge clk)
    if (rst)
      out <= 2'b00;
    else
      out <= 2'b10;
endmodule
