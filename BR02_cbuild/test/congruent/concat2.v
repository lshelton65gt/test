// This started out as a negative case to ensure that {a,{b,c}} was
// not congruent to {{a,b},c}, but I think in LocalAnalysis, Fold
// will canonicalize the expressions to {a,b,c}, and they *will*
// be congruent, so now it's a positive testcase.

module top(i1, i2, i3, o1, o2);
  input i1, i2, i3;
  output [7:0] o1, o2;

  sub1 sub1(i1, i2, i3, o1);
  sub2 sub2(i1, i2, i3, o2);
endmodule

module sub1(i1, i2, i3, o);
  input i1, i2, i3;
  output [7:0] o;

  assign       o = {i1,{i2,i3}};
endmodule

module sub2(i1, i2, i3, o);
  input i1, i2, i3;
  output [7:0] o;

  assign       o = {{i1,i2},i3};
endmodule

