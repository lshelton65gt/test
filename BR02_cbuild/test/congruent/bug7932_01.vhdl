-- November 2007
-- This file used to segfault on congruency testing with cbuild versions 1.5481.2.201
-- and  1.5923. Now it's fixed.
-- This test case has two entities sub1 and sub2, which are identical. Both are
-- instantiated. The second one get's replaced with first one on congruency
-- checking phase.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package FIXED_POINT_PKG is

  type test is array (natural range <>) of std_logic_vector(2 downto 0);
  type complex_fixed_type is
    record
      test1 : test(3 to 4);
    end record;

end FIXED_POINT_PKG;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity sub1 is
  port (
    first      : in complex_fixed_type;
    out1       : out std_logic_vector  (2 downto 0);
    out2       : out std_logic_vector  (2 downto 0));
end;

architecture arch_sub1 of sub1 is
begin
  out1 <= first.test1(3);
  out2 <= first.test1(4);
end;


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity sub2 is
  port (
    first      : in complex_fixed_type;
    out1       : out std_logic_vector  (2 downto 0);
    out2       : out std_logic_vector  (2 downto 0));
end;

architecture arch_sub2 of sub2 is
begin
  out1 <= first.test1(3);
  out2 <= first.test1(4);
end;


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.FIXED_POINT_PKG.all;
entity bug7932_01 is
  port (
    in1        : in  std_logic_vector  (2 downto 0);
    in2        : in  std_logic_vector  (2 downto 0);
    out1       : out std_logic_vector  (2 downto 0);
    out2       : out std_logic_vector  (2 downto 0);
    out3       : out std_logic_vector  (2 downto 0);
    out4       : out std_logic_vector  (2 downto 0));
end;

architecture arch of bug7932_01 is
  signal rec_arr    : complex_fixed_type;
begin

  rec_arr.test1(3) <= in1;
  rec_arr.test1(4) <= in2;
  
  i1 : entity sub1 
    port map (
      first => rec_arr,
      out1  => out1,
      out2  => out2);
      
  i2 : entity sub2 
    port map (
      first => rec_arr,
      out1  => out3,
      out2  => out4);
      
end;
