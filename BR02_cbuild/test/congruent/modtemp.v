// In this example, there will be some module-scoped temps generated
// for the function call return-values.  We should find congruence between
// sub1 and sub2.

module top(i1, i2, i3, i4, o1, o2);
  input i1, i2, i3, i4;
  output o1, o2;

  sub1 sub1(o1, i1, i2);
  sub2 sub2(o2, i3, i4);
endmodule

module sub1(o, i1, i2);
  output o;
  input  i1, i2;

  function inv;
    input i;
    inv = ~i;
  endfunction

  assign  o = inv(i1) ^ inv(i2);
endmodule

module sub2(o, i1, i2);
  output o;
  input  i1, i2;

  function inv;
    input i;
    inv = ~i;
  endfunction

  assign  o = inv(i1) ^ inv(i2);
endmodule
