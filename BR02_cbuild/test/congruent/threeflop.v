// Note -- the sim gold file for this comes from cbuild because the 
// noAsyncResets directive changes the simulation semantics

module flop1 ( clk, rst1, rst2, rst3, in, out );
   input clk;
   input rst1;
   input rst2;
   input rst3;
   input in;
   output out;
   reg 	  out;

   initial begin
      out = 1'b0;
   end

   always @(posedge clk or posedge rst1 or posedge rst2 or posedge rst3)
     if (rst1)
       out <= 1'b0;
     else if (rst2)
       out <= out;
     else if (rst3)
       out <= 1'b1;
     else
       out <= in;

endmodule

module flop2 ( clk, rst1, rst2, rst3, in, out );
   input clk;
   input rst1;
   input rst2;
   input rst3;
   input in;
   output out;
   reg 	  out;

   // carbon noAsyncResets

   initial begin
      out = 1'b0;
   end

   always @(posedge clk or posedge rst1 or posedge rst2 or posedge rst3)
     if (rst1)
       out <= 1'b0;
     else if (rst2)
       out <= out;
     else if (rst3)
       out <= 1'b1;
     else
       out <= in;

endmodule

module flop3 ( clk, rst1, rst2, rst3, in, out );
   input clk;
   input rst1;
   input rst2;
   input rst3;
   input in;
   output out;
   reg 	  out;

   initial begin
      out = 1'b0;
   end

   always @(posedge clk or posedge rst1 or posedge rst2 or posedge rst3)
     if (rst1)
       out <= 1'b0;
     else if (rst2)
       out <= out;
     else if (rst3)
       out <= 1'b1;
     else
       out <= in;

endmodule

module top ( clk, rst1, rst2, rst3, in, out );
   input clk;
   input rst1;
   input rst2;
   input rst3;
   input [2:0] in;
   output [2:0] out;

  flop1 flop1(clk, rst1, rst2, rst3, in[0], out[0]);
  flop2 flop2(clk, rst1, rst2, rst3, in[1], out[1]);
  flop3 flop3(clk, rst1, rst2, rst3, in[2], out[2]);
endmodule

