module top(i1, i2, o1, o2);
  input i1, i2;
  output o1, o2;
  reg    o1, o2;

  task inv1;
    input i;
    output o;
    o = ~i;
  endtask

  task inv2;
    input i;
    output o;
    o = ~i;
  endtask

  always @(i1 or i2) begin
    inv1(i1, o1);
    inv2(i2, o2);
  end
endmodule
