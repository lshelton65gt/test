module top(out,in1,in2);
   output out;
   input in1,in2;

   wire a,b;
   referenced r(in1,a);
   unreferenced u(in2,b);

   assign r.out = ~r.in ? 1'b1 : 1'bz;
   assign out = r.out | b;
endmodule

module referenced(in,out);
   input in;
   output out;
   assign out = in ? 1'b1 : 1'bz;
endmodule

module unreferenced(in,out);
   input in;
   output out;
   assign out = in ? 1'b1 : 1'bz;
endmodule
