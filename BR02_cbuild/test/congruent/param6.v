module f(ck, d, q, addr_in);
   parameter SIZE = 32;
   parameter ADDR = 16'hc;

   input     ck;
   input [SIZE-1:0] d;
   input [2:0] 	    addr_in;

   output [SIZE-1:0] q;
   reg [SIZE-1:0]    q;

   always @(posedge ck) begin
     if (ADDR[addr_in+:8] == 0) begin
       q <= d;	 
     end
   end

endmodule // f

module top (ck, d1, d2, d3, d4, q1, q2, q3, q4, addr1, addr2, addr3, addr4);
  input ck;
  input [31:0] d1, d2;
  input [23:0] d3, d4;
  output [31:0] q1, q2;
  output [23:0] q3, q4;
  input [2:0]   addr1, addr2, addr3, addr4;

  f #(32, 16'h0) f1(ck, d1, q1, addr1);
  f #(32, 16'h1) f2(ck, d2, q2, addr2);
  f #(24, 16'h3) f3(ck, d3, q3, addr3);
  f #(24, 16'h4) f4(ck, d4, q4, addr4);
endmodule // top
