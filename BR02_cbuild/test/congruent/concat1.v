// This started out as a negative testcase to ensure we didn't claim congruence
// on modules that differed only by a concat repeat count.  It turns out that
// after folding this module, we wind up with expressions i?3:0 and i?7:0, which
// *are* congruent after factoring out the constant.  It's still the negative
// testcase we wanted because if we fail to discern the repeat-counts we will
// converge sub1&sub2 early, without factoring out constants.

// Update, this test no longer is congruent with new sizing. If
// fold were improved, it might be a good congruent test again.

module top(i1, i2, o1, o2);
  input i1, i2;
  output [7:0] o1, o2;

  sub1 sub1(i1, o1);
  sub2 sub2(i2, o2);
endmodule

module sub1(i, o);
  input i;
  output [7:0] o;

  assign       o = {2{i}};
endmodule

module sub2(i, o);
  input i;
  output [7:0] o;

  assign       o = {3{i}};
endmodule

