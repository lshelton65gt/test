module top(i1, i2, o1, o2, clk);
  input i1, i2, clk;
  output o1, o2;

  inv inv1(i1, o1, clk);
  inv inv2(i2, o2, clk);
endmodule // top

module inv(i, o, clk);
  input i, clk;
  output o;

  function inv1;
    input in;
    inv1 = ~in;
  endfunction // inv1

  reg     o;
  always @(posedge clk) begin
    o <= inv1(i);
  end
endmodule // top
