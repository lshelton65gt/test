// Originally conceived of as a positive testcase for congruence, this has 
// become a negative testcase, because the congruence-with-constant-factoring
// now occurs *after* folding.  The folding of the parameterized 'sub' and
// 'sub2' variants causes them to look different, and no longer be congruent.
//
// This is morally justified on the grounds that Fold sometimes finds dramatic
// wins and it's better not to block with congruence that loses the constants.
//
// This was found because of perf-tests on mindspeed m27480, where a module that
// was mostly a ROM, but had a very large number of instructions, looked like
// a good Congruence candidate based on heuristics.  But removing all those
// constants eliminated a lot of good Folds and left us with a smaller but slower
// model.

module top(in1, in2, out1, out2);
  input [7:0] in1, in2;
  output [7:0] out1, out2;

  sub #(0,0,0) u0(in1[0], in2[0], out1[0], out2[0]);
  sub #(0,0,1) u1(in1[1], in2[1], out1[1], out2[1]);
  sub #(0,1,0) u2(in1[2], in2[2], out1[2], out2[2]);
  sub #(0,1,1) u3(in1[3], in2[3], out1[3], out2[3]);
  sub #(1,0,0) u4(in1[4], in2[4], out1[4], out2[4]);
  sub #(1,0,1) u5(in1[5], in2[5], out1[5], out2[5]);
  sub #(1,1,0) u6(in1[6], in2[6], out1[6], out2[6]);
  sub2 #(1,1,1) u7(in1[7], in2[7], out1[7], out2[7]);
endmodule

module sub(in1, in2, out1, out2);
  input in1, in2;
  output out1, out2;

  parameter p1 = 1'b0;
  parameter p2 = 1'b0;
  parameter p3 = 1'b0;

  assign out1 = (p1 & (in1 & in2)) ^
                (p2 & (in1 | in2)) ^
                (p3 & (in1 ^ in2));
  assign out2 = (p1 & ~(in1 & in2)) ^
                (p2 & ~(in1 | in2)) ^
                (p3 & ~(in1 ^ in2));
endmodule

// This module is almost the same, but not quite.
module sub2(in1, in2, out1, out2);
  input in1, in2;
  output out1, out2;

  parameter p1 = 1'b0;
  parameter p2 = 1'b0;
  parameter p3 = 1'b0;

  assign out1 = (p1 & (in1 | in2)) ^
                (p2 & (in1 & in2)) ^
                (p3 & (in1 ^ in2));
  assign out2 = (p1 & ~(in1 & in2)) ^
                (p2 & ~(in1 | in2)) ^
                (p3 & ~(in1 ^ in2));
endmodule
