// In this testcase, rom1 and rom2 are congruent, but it's a bad idea
// to converge them because the weight of the parameterization is worse
// than the weight of the replicated module body.
//
// This test case is used as rom.v and rom3.v. In rom.v we set
// -congruencyParamCostLimit to 0 which means an unlimited number
// of parameters are allowed on so rom1 and rom2 are congruent.
// In rom3.v the default value is used so the modules are not
// congruent.

module top(in, out1, out2);
  input [3:0] in;
  output [31:0] out1, out2;

  rom1 r1(in, out1);
  rom2 r2(in, out2);
endmodule

module rom1(in, out);
  input [3:0] in;
  output [31:0] out;
  reg [31:0] out;

  always @(in)
    case (in)
      4'h0: out = 5;
      4'h1: out = 7;
      4'h2: out = 9;
      4'h3: out = 12;
      4'h4: out = 13;
      4'h5: out = 15;
      4'h6: out = 17;
      4'h7: out = 19;
      4'h8: out = 21;
      4'h9: out = 23;
      4'ha: out = 25;
      4'hb: out = 27;
      4'hc: out = 29;
      4'hd: out = 31;
      4'he: out = 33;
      4'hf: out = 35;
    endcase
endmodule


module rom2(in, out);
  input [3:0] in;
  output [31:0] out;
  reg [31:0] out;

  always @(in)
    case (in)
      4'h0: out = 15;
      4'h1: out = 17;
      4'h2: out = 19;
      4'h3: out = 111;
      4'h4: out = 113;
      4'h5: out = 115;
      4'h6: out = 117;
      4'h7: out = 119;
      4'h8: out = 121;
      4'h9: out = 123;
      4'ha: out = 125;
      4'hb: out = 127;
      4'hc: out = 129;
      4'hd: out = 131;
      4'he: out = 133;
      4'hf: out = 135;
    endcase
endmodule
