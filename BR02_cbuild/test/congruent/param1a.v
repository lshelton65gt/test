// simpler version of param1.v for debug.  Interesting because
// there are two parameters & constants that are the same across
// all the instanceseof 'sub', so we only need one parameter

module top(in1, in2, out1, out2);
  input [2:0] in1, in2;
  output [2:0] out1, out2;

  sub #(0,0,0) u0(in1[0], in2[0], out1[0], out2[0]);
  sub #(0,0,1) u1(in1[1], in2[1], out1[1], out2[1]);
  sub2 #(1,1,1) u7(in1[2], in2[2], out1[2], out2[2]);
endmodule

module sub(in1, in2, out1, out2);
  input in1, in2;
  output out1, out2;

  parameter p1 = 1'b0;
  parameter p2 = 1'b0;
  parameter p3 = 1'b0;

  assign out1 = (p1 & (in1 & in2)) ^
                (p2 & (in1 | in2)) ^
                (p3 & (in1 ^ in2));
  assign out2 = (p1 & ~(in1 & in2)) ^
                (p2 & ~(in1 | in2)) ^
                (p3 & ~(in1 ^ in2));
endmodule

// This module is almost the same, but not quite.
module sub2(in1, in2, out1, out2);
  input in1, in2;
  output out1, out2;

  parameter p1 = 1'b0;
  parameter p2 = 1'b0;
  parameter p3 = 1'b0;

  assign out1 = (p1 & (in1 | in2)) ^
                (p2 & (in1 & in2)) ^
                (p3 & (in1 ^ in2));
  assign out2 = (p1 & ~(in1 & in2)) ^
                (p2 & ~(in1 | in2)) ^
                (p3 & ~(in1 ^ in2));
endmodule
