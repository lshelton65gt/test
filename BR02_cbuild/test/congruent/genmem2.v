// last mod: Wed Sep 12 08:05:16 2012
// filename: test/congruent/genmem2.v  
// Description:  This test is used to check how congruence works when it
// interacts with generate blocks.
// in this example a generate case stmt is used to select between two
// implementations of a memory.  The implementations are logically equivalent and no
// observeSignals are used. Note that botmem1 and botmem2 are identical except
// for the following factors:
//  1 they have different names
//  2 the value of the HASLOGIC parameter is inverted
//  3 the testing of the HASLOGIC value is inverted
//  So we expect that the logic for both is reduced to identical unnamed
//  generate blocks, and so we expect that they will be recognized as contruent.

`define MEM_WIDTH 32
`define MEM_DEPTH 32
`define MEM_ADDR_BITS 5


module top (out0, out1, clk, wen, data, waddr, raddr);
   output [`MEM_WIDTH-1:0]    out0, out1;
   input 		      clk, wen;
   input [`MEM_WIDTH-1:0]     data;
   input [`MEM_ADDR_BITS-1:0] waddr, raddr;

   initial
      begin
//	 $display(top.mem0.bk0.data2);
//	 $display(top.mem1.bk0.data2);
	// $display(top.mem1.style1.reg_array_s1[0]); // this works in nc

	// $display(top.mem1.style1.reg_array_s1[0]); // this should work in carbon
	 
       // $display(top.mem0.genblk1.reg_array_s0[0]);// this is most likely the name in nc
      end

       // to avoid defining initial blocks to initalize the memory we use same
       // address for read and write
   gen_mem #(1) mem1(out1, clk, wen, data, raddr, raddr);
   gen_mem      mem0(out0, clk, wen, data, raddr, raddr);
endmodule


module gen_mem (out, clk, wen, data, waddr, raddr);
   output [`MEM_WIDTH-1:0]    out;
   input 		      clk, wen;
   input [`MEM_WIDTH-1:0]     data;
   input [`MEM_ADDR_BITS-1:0] waddr, raddr;

   parameter 		      STYLE = 0;

//	     botmem u1(out, clk, wen, data, waddr, raddr);   
    generate
       case (STYLE)
 	1:
 	  begin:n
 	     botmem1 u1(out, clk, wen, data, waddr, raddr);
           end
 	default:
 	  begin:n
 	     botmem2 u1(out, clk, wen, data, waddr, raddr);
           end
       endcase
   endgenerate
   
endmodule


module botmem1(out, clk, wen, data, waddr, raddr);
   output [`MEM_WIDTH-1:0]    out;
   input 		      clk, wen;
   input [`MEM_WIDTH-1:0]     data;
   input [`MEM_ADDR_BITS-1:0] waddr, raddr;

   parameter 		      HASLOGIC = 1;
   // This is the memory
   reg [`MEM_WIDTH-1:0]       reg_array_s0 [`MEM_DEPTH-1:0];

   if (HASLOGIC )
	  begin			// unnamed generate block
             integer                     i;
             initial begin
		$display("TOP %m");
               for (i = 0; i < `MEM_DEPTH; i = i + 1)
                 reg_array_s0[i] = 1'b0;
             end

	     always @ (negedge clk)
		   $display("%m");
	     // Write the memory
	     always @ (negedge clk)
		begin:bk0
		   reg  [`MEM_WIDTH-1:0]     data2;
		   data2 = ~data;
		   if (wen)
		     reg_array_s0[waddr] = data2;
		end

             // Read the memory
             assign  out = reg_array_s0[raddr];
          end
endmodule

module botmem2(out, clk, wen, data, waddr, raddr);
   output [`MEM_WIDTH-1:0]    out;
   input 		      clk, wen;
   input [`MEM_WIDTH-1:0]     data;
   input [`MEM_ADDR_BITS-1:0] waddr, raddr;

   parameter 		      HASLOGIC = 0;
   // This is the memory
   reg [`MEM_WIDTH-1:0]       reg_array_s0 [`MEM_DEPTH-1:0];

   if ( ! HASLOGIC )
	  begin			// unnamed generate block
             integer                     i;
             initial begin
		$display("TOP %m");
               for (i = 0; i < `MEM_DEPTH; i = i + 1)
                 reg_array_s0[i] = 1'b0;
             end

	     always @ (negedge clk)
		   $display("%m");
	     // Write the memory
	     always @ (negedge clk)
		begin:bk0
		   reg  [`MEM_WIDTH-1:0]     data2;
		   data2 = ~data;
		   if (wen)
		     reg_array_s0[waddr] = data2;
		end

             // Read the memory
             assign  out = reg_array_s0[raddr];
          end
endmodule
