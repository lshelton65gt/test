// Test that function locals in a nested scope don't prevent congruence

module top(i1, i2, o1, o2, clk);
  input i1, i2, clk;
  output o1, o2;

  function inv1;
    input in;
    begin :bar
      reg baz;
      baz = ~in;
      inv1 = baz;
    end
  endfunction // inv1

  function inv2;
    input i;
    begin :foo
      reg glob;
      glob = ~i;
      inv2 = glob;
    end
  endfunction // inv1

  reg     o1, o2;
  always @(posedge clk) begin
    o1 <= inv1(i1);
    o2 <= inv2(i2);
  end
endmodule // top
