// This test ensures that we pay attention to lvalues when examining modules
// for congruence, which was broken till 8/15/05

module top(o1, o2, o3, o4, i1, i2, i3, i4);
  output o1, o2, o3, o4;
  input  i1, i2, i3, i4;

  sub1 sub1(o1, o2, i1, i2);
  sub2 sub2(o3, o4, i3, i4);
endmodule

module sub1(o1, o2, i1, i2);
  output o1, o2;
  input  i1, i2;

  assign o1 = i1;
  assign o2 = i2;
endmodule

module sub2(o1, o2, i1, i2);
  output o1, o2;
  input  i1, i2;

  assign o2 = i1;
  assign o1 = i2;
endmodule
