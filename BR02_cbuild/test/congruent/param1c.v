// This version of param1b.v retains congruence, because hiding the
// logical operators in tasks avoids us folding them and leaves them
// congruent.

module top(in1, in2, out1, out2);
  input [6:0] in1, in2;
  output [6:0] out1, out2;

  sub #(0,0,0) u0(in1[0], in2[0], out1[0], out2[0]);
  sub #(0,0,1) u1(in1[1], in2[1], out1[1], out2[1]);
  sub #(0,1,0) u2(in1[2], in2[2], out1[2], out2[2]);
  sub #(0,1,1) u3(in1[3], in2[3], out1[3], out2[3]);
  sub #(1,0,0) u4(in1[4], in2[4], out1[4], out2[4]);
  sub #(1,0,1) u5(in1[5], in2[5], out1[5], out2[5]);
  sub #(1,1,0) u6(in1[6], in2[6], out1[6], out2[6]);
endmodule

module sub(in1, in2, out1, out2);
  input in1, in2;
  output out1, out2;

  parameter p1 = 1'b0;
  parameter p2 = 1'b0;
  parameter p3 = 1'b0;

  reg      t1, t2, t3, t4, t5, t6;
  reg      t8, t9, t10, t11, t12, t13;
  reg      out1, out2;

  always @(in1 or in2) begin
    and2(t1, in1, in2);
    and2(t2, t1,  p1);
    or2(t3, in1, in2);
    and2(t4, t3,  p2);
    xor2(t5, in1, in2);
    and2(t6, t5,  p3);
    xor3(out1, t2, t4, t6);

    not1(t8, t1);
    and2(t9, p1, t8);
    not1(t10, t3);
    and2(t11, p2, t10);
    not1(t12, t5);
    and2(t13, p3, t12);
    xor3(out2, t9, t11, t13);
  end

  task and2;
    output z;
    input  a, b;

    z = a & b;
  endtask

  task not1;
    output z;
    input  a;

    z = ~a;
  endtask

  task or2;
    output z;
    input  a, b;

    z = a | b;
  endtask

  task xor2;
    output z;
    input  a, b;

    z = a ^ b;
  endtask

  task xor3;
    output z;
    input  a, b, c;

    z = a ^ b ^ c;
  endtask
endmodule

