module top (out1, out2, in1, in2);
   output [7:0] out1, out2;
   input [7:0] 	in1, in2;

   sub #(8'h8) S1 (out1, in1);
   sub #(8'h9) S2 (out2, in2);

endmodule // top

module sub (out, in);
   output [7:0] out;
   input [7:0] 	in;
   parameter 	val=8'h0;

   assign 	out = in & val;

endmodule // sub
