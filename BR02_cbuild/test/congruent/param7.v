module top ( clk, d, q1, q2);


   parameter WIDTH = 32;
   parameter ONES = 32'b1111_1111_1111_1111_1111_1111_1111_1111;
   parameter ALT =  32'h0101_0101_0101_0101_1111_0000_1111_0000;

   input     clk;
   input [WIDTH-1:0] d;
   output [WIDTH-1:0] q1, q2;

   flop #( WIDTH, ONES) F1 ( .clock( clk ), .d(d), .q(q1));
   flop #( WIDTH, ALT)  F2 ( .clock( clk ), .d(d), .q(q2));

endmodule // top


module flop (clock, d,q);
   parameter  SIZE = 32;
   parameter  MASK = 32'b1111_1111_1111_1111_1111_1111_1111_1111;

   input 	    clock;
   input [SIZE-1:0] 	     d;
   output [SIZE-1:0] 	     q;
   reg [SIZE-1:0] 	     q;

   initial q = 0;

   integer 		     i;

   always @ (posedge clock)// or negedge resetn)
     for (i= 0; i < 32; i=i+1)
       if (MASK[i])
         begin
            q[i] <= d[i];
         end
endmodule
