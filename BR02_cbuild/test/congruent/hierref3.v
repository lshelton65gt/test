module top(o1,o2,i1,i2);
   output o1,o2;
   input i1,i2;

   foo fooie(o1,i1);
   bar kablooie(o2,i2);
   sub foo(i2);
endmodule

module foo(out,in);
   output out;
   input in;
   assign out = in & foo.in;
endmodule

module bar(out,in);
   output out;
   input in;
   assign out = in & foo.in;
endmodule

module sub(in);
   input in;
endmodule
