module top(i1, i2, o1, o2, diff, clk);
  input clk;
  input [7:0] i1, i2;
  output [7:0] o1, o2;
  output       diff;

  function [7:0] minus1;
    input in1, in2;
    minus1 = in1 - in2;
  endfunction

  function [7:0] minus2;
    input in1, in2;
    minus2 = in2 - in1;
  endfunction

  reg [7:0]     o1, o2;
  always @(posedge clk) begin
    o1 <= minus1(i1, i2);
    o2 <= minus2(i2, i1);
  end

  assign diff = o1 == o2;       // should always be 1
endmodule // top
