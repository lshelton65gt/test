// Expect flop1 & flop2 will be merged, but flop3 and flop4 will
// remain distinct due to incompatible timescales.

`timescale 10ps/10ps

module flop1 ( clk, rst1, rst2, rst3, in, out );
   input clk;
   input rst1;
   input rst2;
   input rst3;
   input in;
   output out;
   reg 	  out;

   initial begin
      out = 1'b0;
   end

   always @(posedge clk or posedge rst1 or posedge rst2 or posedge rst3)
     if (rst1)
       out <= 1'b0;
     else if (rst2)
       out <= out;
     else if (rst3)
       out <= 1'b1;
     else
       out <= in;

endmodule

module flop2 ( clk, rst1, rst2, rst3, in, out );
   input clk;
   input rst1;
   input rst2;
   input rst3;
   input in;
   output out;
   reg 	  out;

   initial begin
      out = 1'b0;
   end

   always @(posedge clk or posedge rst1 or posedge rst2 or posedge rst3)
     if (rst1)
       out <= 1'b0;
     else if (rst2)
       out <= out;
     else if (rst3)
       out <= 1'b1;
     else
       out <= in;

endmodule

`timescale 100ps/10ps

module flop3 ( clk, rst1, rst2, rst3, in, out );
   input clk;
   input rst1;
   input rst2;
   input rst3;
   input in;
   output out;
   reg 	  out;

   initial begin
      out = 1'b0;
   end

   always @(posedge clk or posedge rst1 or posedge rst2 or posedge rst3)
     if (rst1)
       out <= 1'b0;
     else if (rst2)
       out <= out;
     else if (rst3)
       out <= 1'b1;
     else
       out <= in;

endmodule

`timescale 100ps/100ps

module flop4 ( clk, rst1, rst2, rst3, in, out );
   input clk;
   input rst1;
   input rst2;
   input rst3;
   input in;
   output out;
   reg 	  out;

   initial begin
      out = 1'b0;
   end

   always @(posedge clk or posedge rst1 or posedge rst2 or posedge rst3)
     if (rst1)
       out <= 1'b0;
     else if (rst2)
       out <= out;
     else if (rst3)
       out <= 1'b1;
     else
       out <= in;

endmodule

module top ( clk, rst1, rst2, rst3, in, out );
   input clk;
   input rst1;
   input rst2;
   input rst3;
   input [3:0] in;
   output [3:0] out;

  flop1 flop1(clk, rst1, rst2, rst3, in[0], out[0]);
  flop2 flop2(clk, rst1, rst2, rst3, in[1], out[1]);
  flop3 flop3(clk, rst1, rst2, rst3, in[2], out[2]);
  flop4 flop4(clk, rst1, rst2, rst3, in[3], out[3]);
endmodule

