// this testcase should not show any congruence because
// although the two flops look very similar, they differ
// by an edge sensitivity, and we don't know how to factor
// that out using a port.  Actually we *could* factor it
// out by xoring the reset with an input parameter, but
// that's beyond our scope right now!


module DJK1RS1 (reset, clk, j, k, q);

   input reset; 
   input clk; 
   input j; 
   input k; 
   output q; 
   reg q;

   always @(posedge clk or posedge reset)
   begin : dffr
      if (reset == 1'b1)
      begin
         q <= 1'b0 ; 
      end
      else
      begin
         if (j == 1'b1 & k == 1'b0)
         begin
            q <= 1'b1 ; 
         end
         else if (j == 1'b0 & k == 1'b1)
         begin
            q <= 1'b0 ; 
         end
         else if (j == 1'b1 & k == 1'b1)
         begin
            q <= 1'b0 ; // not really JK
         end 
      end 
   end 
endmodule

module DJK1RS2 (reset, clk, j, k, q);

   input reset; 
   input clk; 
   input j; 
   input k; 
   output q; 
   reg q;

   always @(posedge clk or negedge reset)
   begin : dffr
      if (reset == 1'b0)
      begin
         q <= 1'b0 ; 
      end
      else
      begin
         if (j == 1'b1 & k == 1'b0)
         begin
            q <= 1'b1 ; 
         end
         else if (j == 1'b0 & k == 1'b1)
         begin
            q <= 1'b0 ; 
         end
         else if (j == 1'b1 & k == 1'b1)
         begin
            q <= 1'b0 ; // not really JK
         end 
      end 
   end 
endmodule

module top(reset, clk, j, k, q1, q2);

   input reset; 
   input clk; 
   input j; 
   input k; 
   output q1, q2;

  DJK1RS1 u1(reset, clk, j, k, q1);
  DJK1RS2 u2(reset, clk, j, k, q2);
endmodule
