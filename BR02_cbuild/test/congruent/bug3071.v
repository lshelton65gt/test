module foo(clk, in1, in2, out1, out2);
   input clk;
   input [31:0] in1, in2;
   output [31:0] out1, out2;

   bar #(8) bar1(.clk(clk), .out(out1), .in(in1));
   bar #('b1000) bar2(.clk(clk), .out(out2), .in(in2));

endmodule

module bar(clk, in, out);
   parameter INCR = 1;

   input clk;
   input [31:0] in;
   output [31:0] out;
   reg [31:0] out;

   always @(posedge clk)
     out <= in + INCR;
endmodule


