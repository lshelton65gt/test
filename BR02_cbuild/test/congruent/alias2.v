// We should not find these two modules congruent because
// the aliases have different names, and so visibility will be affected
//
// In this case, mybuf1 and mybuf2 should be congruent, but inv1 and inv2
// will not be due to the different instance paths


module top(i1, i2, o1, o2);
  input i1, i2;
  output o1, o2;

  // carbon disallowFlattening

  inv1 inv1(i1, o1);
  inv2 inv2(i2, o2);
endmodule

module inv1(i, o);
  input i;
  output o;

  // carbon disallowFlattening

  mybuf1 mybuf1(tmp, i);
  assign o = ~tmp;
endmodule

module mybuf1(out, in);
  output out;
  input  in;

  // carbon flattenModule

  wire tmp1 = in;
  assign out = tmp1;
endmodule

module inv2(i, o);
  input i;
  output o;

  // carbon disallowFlattening

  mybuf2 mybuf2(tmp, i);        // different instance names
  assign o = ~tmp;
endmodule

module mybuf2(out, in);
  output out;
  input  in;

  // carbon flattenModule

  wire tmp1 = in;  // same as mybuf1, so congruence will occur
  assign out = tmp1;
endmodule

