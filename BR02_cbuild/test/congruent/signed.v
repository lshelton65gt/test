// Demonstrate troubles with SIGNED constants becoming unsigned nets.

module SUBMOD(datain,clk,dataout);
   parameter param = 32'd1;
   input signed [31:0]  datain;
   
   input  clk;

   output dataout;
   reg    dataout;

   always @ (posedge clk)
     dataout = (param <= datain) ? ^datain : |datain;   

endmodule

module SIGNEDPARAM(in1, in2, clk, out1, out2);
   input signed [31:0] in1, in2;
input clk;

output out1, out2;
wire out1, out2;

SUBMOD #(2)
inst1(.datain(in1),.clk(clk),.dataout(out1));

SUBMOD #(0)
inst2(.datain(in2),.clk(clk),.dataout(out2));

endmodule


