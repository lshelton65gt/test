// This version of param1.v should remain congruent, because hiding the
// logical operators in unflattened instances will avoid us folding them
// and leave them congruent.
//
// Unfortunately this doesn't work yet because the temp-names created for
// lowered ports differ, and we require nets to have the same names, even
// temps.  That kills the deal for this one.  This should be fixed by
// allowing temp nets to have different names if they have the same BaseIndex.
//
// param1c.v attempts to solve this problem by embedding the logic in tasks
// where it will not be subject to port-lowering or constant propgation

module top(in1, in2, out1, out2);
  input [7:0] in1, in2;
  output [7:0] out1, out2;

  sub #(1'b0,1'b0,1'b0) u0(in1[0], in2[0], out1[0], out2[0]);
  sub #(1'b0,1'b0,1'b1) u1(in1[1], in2[1], out1[1], out2[1]);
  sub #(1'b0,1'b1,1'b0) u2(in1[2], in2[2], out1[2], out2[2]);
  sub #(1'b0,1'b1,1'b1) u3(in1[3], in2[3], out1[3], out2[3]);
  sub #(1'b1,1'b0,1'b0) u4(in1[4], in2[4], out1[4], out2[4]);
  sub #(1'b1,1'b0,1'b1) u5(in1[5], in2[5], out1[5], out2[5]);
  sub #(1'b1,1'b1,1'b0) u6(in1[6], in2[6], out1[6], out2[6]);
  sub2 #(1'b1,1'b1,1'b1) u7(in1[7], in2[7], out1[7], out2[7]);
endmodule

module sub(in1, in2, out1, out2);
  input in1, in2;
  output out1, out2;

  parameter p1 = 1'b0;
  parameter p2 = 1'b0;
  parameter p3 = 1'b0;

  and2 u1(t1, in1, in2);
  and2 u2(t2, t1,  p1);
  or2  u3(t3, in1, in2);
  and2 u4(t4, t3,  p2);
  xor2 u5(t5, in1, in2);
  and2 u6(t6, t5,  p3);
  xor3 u7(out1, t2, t4, t6);

  not1 u8(t8, t1);
  and2 u9(t9, p1, t8);
  not1 u10(t10, t3);
  and2 u11(t11, p2, t10);
  not1 u12(t12, t5);
  and2 u13(t13, p3, t12);
  xor3 u14(out2, t9, t11, t13);
endmodule

// This module is almost the same, but not quite.
module sub2(in1, in2, out1, out2);
  input in1, in2;
  output out1, out2;

  parameter p1 = 1'b0;
  parameter p2 = 1'b0;
  parameter p3 = 1'b0;

  assign out1 = (p1 & (in1 | in2)) ^
                (p2 & (in1 & in2)) ^
                (p3 & (in1 ^ in2));
  assign out2 = (p1 & ~(in1 & in2)) ^
                (p2 & ~(in1 | in2)) ^
                (p3 & ~(in1 ^ in2));
endmodule

module and2(z, a, b);
  output z;
  input  a, b;

  assign z = a & b;
endmodule

module not1(z, a);
  output z;
  input  a;

  assign z = ~a;
endmodule

module or2(z, a, b);
  output z;
  input  a, b;

  assign z = a | b;
endmodule

module xor2(z, a, b);
  output z;
  input  a, b;

  assign z = a ^ b;
endmodule

module xor3(z, a, b, c);
  output z;
  input  a, b, c;

  assign z = a ^ b ^ c;
endmodule
