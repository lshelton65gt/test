module top (in, out1, out2, out3);
   output out1, out2, out3;
   input  in;

   sub1 s1 (out1);
   sub2 s2 (out2);
  assign  out3 = in;

endmodule // top

// In this variation, there are initial blocks, but the constants are the
// same, so they don't have to be factored out, and we so sub1 and
// sub2 are congruent

module sub1 (out);
   output out;
   reg    out;

   initial out = 0;

endmodule

module sub2 (out);
   output out;
   reg    out;

   initial out = 0;

endmodule
