module bug6039b(pixswapcntgt0,pixswapcnt_in);
   output [3:0] pixswapcntgt0;
   input [11:0] pixswapcnt_in;

   reg [2:0] 	pixswapcnt[3:0];
   always @(pixswapcnt_in) begin
      pixswapcnt[0] = pixswapcnt_in[2:0];
      pixswapcnt[1] = pixswapcnt_in[5:3];
      pixswapcnt[2] = pixswapcnt_in[8:6];
      pixswapcnt[3] = pixswapcnt_in[11:9];
   end
   assign 	pixswapcntgt0[0] = ((~pixswapcnt[0][2]) & (pixswapcnt[0][1] | pixswapcnt[0][0]));
   assign 	pixswapcntgt0[1] = ((~pixswapcnt[1][2]) & (pixswapcnt[1][1] | pixswapcnt[1][0]));
   assign 	pixswapcntgt0[2] = ((~pixswapcnt[2][2]) & (pixswapcnt[2][1] | pixswapcnt[2][0]));
   assign 	pixswapcntgt0[3] = ((~pixswapcnt[3][2]) & (pixswapcnt[3][1] | pixswapcnt[3][0]));
endmodule
