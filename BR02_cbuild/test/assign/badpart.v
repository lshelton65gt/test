// test indexed partsel with out-range...
// This test yields different results with aldec, mti, nc, carbon

// module to read and write a -: bit range
module BElsb(i,j,o,x);
   input [31:0] i;
   input [4:0]  j;
   output [15:0] o,x;
   reg [15:0]    t;
   assign        o = i[j-:16];
   assign        x = t;
   always @(i or j)
     t[j-:16] = i;
endmodule

// module to read and write a +: bit range
module BEmsb(i,j,o,x);
   input [31:0] i;
   input [4:0]  j;
   output [15:0] o,x;
   reg [15:0]    t;
   assign        o = i[j+:16];
   assign        x = t;
   always @(i or j)
     t[j+:16] = i;
endmodule

// Now the same thing but with little-endian vectors
// module to read and write a -: bit range
module LElsb(i,j,o,x);
   input [0:31] i;
   input [4:0]  j;
   output [0:15] o,x;
   reg [0:15]    t;
   assign        o = i[j-:16];
   assign        x = t;
   always @(i or j)
     t[j-:16] = i;
endmodule

// module to read and write a +: bit range
module LEmsb(i,j,o,x);
   input [0:31] i;
   input [4:0]  j;
   output [0:15] o,x;
   reg [0:15]    t;
   assign        o = i[j+:16];
   assign        x = t;
   always @(i or j)
     t[j+:16] = i;
endmodule

// Now the driver

module top(value, index, bel, belx, bem, bemx, lel, lelx, lem, lemx);
   input [31:0] value;
   input [4:0]  index;          // 0..31 index into value

   output [15:0] bel, bem, lel, lem;
   output [15:0] belx,bemx,lelx,lemx;

   BElsb a(value, index, bel, belx);
   LElsb b(value, index, lel, lelx);
   BEmsb c(value, index, bem, bemx);
   LEmsb d(value, index, lem, lemx);
endmodule

