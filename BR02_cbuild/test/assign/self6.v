// self-referencing inference opportunity. this one could succeed, but
// we probably won't handle it initially.
module vector ( out, in, clk );
   input [0:3] in;
   input       clk;
   output [0:3] out;

   // self-reference this vector, but an index which hasn't yet
   // been computed. safe to collapse.

   // Unfortunately, our bit-level analysis does not work well in this
   // situation -- cbuild thinks there's a cycle. We should /not/
   // collapse until bug 399 is resolved.
   assign out[3] = out[2] & in[3];
   assign out[2] = out[1] & in[2];
   assign out[1] = out[0] & in[1];
   assign out[0] = in[0];

endmodule // vector
