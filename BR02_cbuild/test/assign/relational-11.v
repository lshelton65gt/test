module top (t, h, b, x);
  input [1:0] t, h, b;
  output [1:0] x;
  assign       x[1] = (t[1] != h[1]) & b[1];
  assign       x[0] = (t[0] != h[0]) & b[0];
endmodule // top
