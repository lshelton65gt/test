// last mod: Thu Dec 15 12:09:24 2005
// filename: test/assign/.:source/bug5458.v
// Description:  This test is the original test from bug5458, at one time it
// would NU_ASSERT

module bug5458 (
             reset,             
             fclk,              
             addr,              
             xaddr,             
             scl,               
             sda,               
             status,            
             data,              
             dataValid,         
             dataIn             
             );
   

   input [6:0]   addr;
   input [7:0]   xaddr;
   input         reset;
   input         fclk;
   inout         scl;
   inout         sda;
   input [63:0]  dataIn;

   output [63:0] data;          
   output        dataValid;     
   
   output [7:0]  status;        

   wire [6:0]    addr;
   wire [7:0]    xaddr;
   wire          reset;
   wire          fclk;
   wire          scl;
   wire          sda;           

   reg [3:0]     cpuState;      
   
   reg [2:0]     raddr;
   reg           sReadAddressValid, sWriteAddressValid;

   wire [7:0]    status;        

   reg [63:0]    data;          
   wire          dataValid;     
   reg           dv0, dv1;

   reg [7:0]     dataByte;      
   reg [7:0]     xmitByte;      
   
   reg [7:0]     rdata;         
   
   reg [2:0]     i2cAddr;
   reg [7:0]     i2cDi;
   reg           i2cNwr;
   wire          i2cIntr;
   wire [7:0]    i2cDa;
   reg [7:0]     i2cCntr;
   reg [7:0]     i2cStatus;

   
   wire          isda;
   wire          iscl;
   wire          osda;
   wire          oscl;
   wire          i2cReset;
   wire          i2cClk;

   reg           hwReset;
   reg           busError;          
   reg [7:0]     unsupportedStatus; 

   
   assign        scl = oscl ? 1'bz : 1'b0;
   assign        sda = osda ? 1'bz : 1'b0;
   
   assign        isda = sda;
   assign        iscl = scl;

   assign        i2cReset = hwReset;
   assign        dataValid = dv0 & ~dv1;
   assign        status = i2cStatus;

   
   always @(posedge fclk)
       dv1 <= dv0;
   
   always @(posedge i2cClk or posedge reset)
     begin
        if (reset)
          hwReset <= 1'b1;
        else
          hwReset <= 1'b0;
     end 

   
   always @(posedge fclk)
     begin
        if (i2cNwr)
          case (i2cAddr)
            3'b010:
              i2cCntr <= i2cDa;
            3'b011:
              i2cStatus <= i2cDa;
          endcase 
     end                        

   

always @(posedge fclk or posedge hwReset)
  begin
     if (hwReset)
       begin
          sReadAddressValid <= 0;
          sWriteAddressValid <= 0;
          readReg(3'b011);
          dv0 <= 0;
          busError <= 0;
          unsupportedStatus <= 0;

          nextCpuState(4'b0010);

       end 
     else

       
       if (!i2cNwr)
         i2cNwr <= 1'b1;
       else
          case (cpuState)
            4'b0000:
              begin
                 rdata <= i2cDa;
                 readReg(3'b011);
                 
                 if (i2cIntr)
                   
                   begin
                      
                      readReg(3'b011);
                      nextCpuState(4'b0110);
                   end 
              end 
            
            4'b0110:
              begin
                 
                 readReg(3'b001);
                 nextCpuState(4'b0111);
              end 

            4'b0111:
              begin
                 
                 casex (i2cStatus)
                   
                   'h00:        
                     begin
                        busError <= 1;
                        sWriteAddressValid <= 0;
                        sReadAddressValid <= 0;

                        
                        writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000 | 8'b0000_0100 | 8'b0001_0000);
                        nextCpuState(4'b1010);
                     end 
                   
                   'h60:        
                     begin
                        dv0 <= 0;
                        dataByte <= 0;
                        sWriteAddressValid <= 1;
                        sReadAddressValid <= 0;
                        data <= 64'b0; 

                        
                        writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000 | 8'b0000_0100);
                        nextCpuState(4'b1010);
                     end 
                   
                   'h80:        
                     begin
                        
                        loadData(dataByte);

                        dataByte <= dataByte + 8'h01;

                        
                        writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000 | 8'b0000_0100);
                        nextCpuState(4'b1010);
                     end 
                   
                   'hA0:        
                     begin
                        dv0 <= sWriteAddressValid;
                        sWriteAddressValid <= 0;
                        sReadAddressValid <= 0;

                        
                        writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000 | 8'b0000_0100);
                        nextCpuState(4'b1010);
                     end 
                   
                   'hA8:        
                     begin
                        sReadAddressValid <= 1;
                        xmitByte <= 0;
                        nextCpuState(4'b1000);
                     end

                   'hB8:        
                     begin
                        nextCpuState(4'b1000);
                     end 
                       
                   'hC0:        
                     begin
                        sReadAddressValid <= 0;

                        
                        writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000 | 8'b0000_0100);
                        nextCpuState(4'b1010);
                     end 

                   'hC8:        
                     begin
                        sReadAddressValid <= 0;

                        
                        writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000 | 8'b0000_0100);
                        nextCpuState(4'b1010);
                     end 

                   'hF8:        
                     begin

                        
                        writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000 | 8'b0000_0100);
                        nextCpuState(4'b1010);
                     end 
                     
                   default:     
                     begin
                        sWriteAddressValid <= 0;
                        sReadAddressValid <= 0;
                        unsupportedStatus <= i2cStatus;

                        
                        writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000 | 8'b0000_0100);
                        nextCpuState(4'b1010);
                     end 
                   
                 endcase 
                 
              end
            
            4'b1010:
              begin
                 
                 if (!i2cIntr)
                   nextCpuState(4'b0000);
              end
            
            4'b1000:
              begin
                 
                 writeXmitData(xmitByte);
                 xmitByte <= xmitByte + 1;

                 nextCpuState(4'b1001);
              end 
            
            4'b1001:
              begin
                 
                 if (xmitByte == 8'h08) 
                   writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000);
                 else
                   writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000 | 8'b0000_0100);
                 
                 nextCpuState(4'b1010);
              end
            
            
            4'b0010:
              begin
                 
                 writeReg(3'b000, {addr, 1'b0});
                 
                 nextCpuState(4'b0011);
              end
            
            4'b0011:
              begin
                 
                 writeReg(3'b100, xaddr);

                 nextCpuState(4'b0100);
              end
            
            4'b0100:
              begin
                 
                 writeReg(3'b010, 8'b1000_0000 | 8'b0100_0000 | 8'b0000_0100);

                 nextCpuState(4'b0101);
              end
            
            4'b0101:
              begin
                 
                 writeReg(3'b011, {1'b0, 4'h5, 3'h2});

                 nextCpuState(4'b0000);
              end
            
          endcase 
  end 

 
 
 

   
   task writeReg;
      input [2:0] addr;
      input [7:0] data;
      begin
         i2cAddr <= addr;
         i2cDi <= data;
         i2cNwr <= 1'b0;
      end
   endtask 

   
   task readReg;
      input [2:0] addr;
      begin
         i2cAddr <= addr;
         i2cNwr <= 1'b1;
      end
   endtask 

   
   task loadData;
      input [7:0] byte;
      begin
         case (dataByte)
           8'h00:
             data[7:0] <= i2cDa;
           8'h01:
             data[15:8] <= i2cDa;
           8'h02:
             data[23:16] <= i2cDa;
           8'h03:
             data[31:24] <= i2cDa;
           8'h04:
             data[39:32] <= i2cDa;
           8'h05:
             data[47:40] <= i2cDa;
           8'h06:
             data[55:48] <= i2cDa;
           8'h07:
             data[63:56] <= i2cDa;
         endcase 

      end
   endtask 

   task nextCpuState;
      input [3:0] state;
      begin
         cpuState <= state;
      end
   endtask 

   
   task writeXmitData;
      input [7:0] byte;
      reg [7:0] xmitData;
      begin
         case (byte)
           8'h00:
             xmitData <= dataIn[7:0];
           8'h01:
             xmitData <= dataIn[15:8];
           8'h02:
             xmitData <= dataIn[23:16];
           8'h03:
             xmitData <= dataIn[31:24];
           8'h04:
             xmitData <= dataIn[39:32];
           8'h05:
             xmitData <= dataIn[47:40];
           8'h06:
             xmitData <= dataIn[55:48];
           8'h07:
             xmitData <= dataIn[63:56];
         endcase 
         
         writeReg(3'b001, xmitData);
      end
   endtask 

endmodule
