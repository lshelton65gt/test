module offset ( out, in, sel );
   input [0:3] in;
   input       sel;
   output [1:4] out;
   reg [1:4] 	out;
   
   always @(in or sel)
     begin
	out[1] = in[0] & sel;
	out[2] = in[1] & sel;
	out[3] = in[2] & sel;
	out[4] = in[3] & sel;
     end
endmodule // offset
