// test bitfields with positive offset, such that nonexisting bits are always
// msbs

module top(i,o);
   parameter WORD = 32;
   parameter FIELD = 16;
   
   input [WORD-1:0] i;
   output[WORD-1:0] o;

   reg [WORD-1:0]   temp;

   assign       o = temp;
   always @(i)
     begin: blk
        integer k;
        reg [FIELD-1:0] value;
        reg [WORD-1:0] hold;
        for(k=0; k < WORD; k=k+1)
          begin
             value = i[k+:FIELD];
             hold[k+:FIELD] = value;

             // set flag if value we read is different from what we wrote
             temp[k] = (hold[k+:FIELD] != value);
          end
     end
endmodule
