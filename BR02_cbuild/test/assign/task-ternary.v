module vector ( out, in1, in2, sel );
   input [0:3] in1;
   input [0:3] in2;
   input 	sel;
   
   output [0:3] out;
   reg [0:3] 	out;

   task silly;
      output [0:3] o;
      input sel;
      input [0:3] in1, in2;
      begin
	 o[3] = sel ? in1[3] : in2[3];
	 o[2] = sel ? in1[2] : in2[2];
	 o[1] = sel ? in1[1] : in2[1];
	 o[0] = sel ? in1[0] : in2[0];
      end
   endtask // silly

   always @(sel or in1 or in2 )
     begin
	silly(out,sel,in1,in2);
     end

endmodule // vector
