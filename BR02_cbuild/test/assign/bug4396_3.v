// Test case for bug 4396 - test that
// tristate drivers with Z in concats are handled.

module top(sel0, sel1, d0, d1, d3, io);
   input sel0, sel1;
   input [1:0] d0;
   input [1:0] d1;
   input       d3;
   inout [2:0] io;

   assign io[0] = sel0 ? d0[0] : 1'bz;
   assign io[1] = sel0 ? d0[1] : 1'bz;
   assign io[2:0] = sel1 ? d1[1:0] : {1'bz,{2{1'bz}}};
endmodule
