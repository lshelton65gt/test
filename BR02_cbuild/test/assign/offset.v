module offset ( out, in, sel );
   input [1:4] in;
   input       sel;
   output [0:3] out;
   reg [0:3] 	out;
   
   always @(in or sel)
     begin
	out[0] = in[1] & sel;
	out[1] = in[2] & sel;
	out[2] = in[3] & sel;
	out[3] = in[4] & sel;
     end
endmodule // offset
