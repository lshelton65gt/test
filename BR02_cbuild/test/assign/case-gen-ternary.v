module ternary ( clk,sel,a,b,out );
   input clk;
   input [1:0] sel;
   input [1:0] a,b;
   output [1:0] out;
   reg [1:0] 	out;

   initial out = 0;
   always @(posedge clk) begin
      case (sel[0])
	1'b0: out[0] = a[0];
	1'b1: out[0] = b[0];
      endcase
      case (sel[1])
	1'b0: out[1] = a[1];
	1'b1: out[1] = b[1];
      endcase
   end
endmodule
