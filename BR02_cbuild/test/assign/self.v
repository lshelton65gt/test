// self-referencing inference opportunity. this one should succeed.
module vector ( out, in, clk );
   input [0:3] in;
   input       clk;
   output [0:3] out;
   reg [0:3] 	out;

   initial out = 0; // for simulation.
   
   always @( posedge clk )
     begin
	// self-reference this vector, but to the current index.
	// safe to collapse.
	out[0] = out[0] & in[0];
	out[1] = out[1] & in[1];
	out[2] = out[2] & in[2];
	out[3] = out[3] & in[3];
     end

endmodule // vector
