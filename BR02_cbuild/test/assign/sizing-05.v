module top (a, x);
  input [3:0] a;
  output [3:0] x;
  assign       x [1:0] = $signed (a [1:0]);
  assign       x [3:2] = $signed (a [3:2]);
endmodule // top
