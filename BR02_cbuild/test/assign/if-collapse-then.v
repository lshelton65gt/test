module top (clk, sel, Y);
  input clk;
  input sel;
  reg [14:0] X;
  input [14:0] Y;
  always @(posedge clk )
   begin 
    if (sel)
      begin 
      X [1 : 0 ]<= X [1 : 0 ];
      X [3 : 2 ]<= X [3 : 2 ];
      X [5 : 4 ]<= X [5 : 4 ];
      X [7 : 6 ]<= X [7 : 6 ];
      X [9 : 8 ]<= X [9 : 8 ];
      X [11 : 10 ]<= X [11 : 10 ];
      X [13 : 12 ]<= X [13 : 12 ];
      X [14 ]<= X [14 ];
      end 
    else begin 
      X [1 : 0 ]<= Y [1 : 0 ];
      X [3 : 2 ]<= X [1 : 0 ];
      X [5 : 4 ]<= X [3 : 2 ];
      X [7 : 6 ]<= X [5 : 4 ];
      X [9 : 8 ]<= X [7 : 6 ];
      X [11 : 10 ]<= X [9 : 8 ];
      X [13 : 12 ]<= X [11 : 10 ];
      X [14 ]<= X [12 ];
    end 
  end 
endmodule
