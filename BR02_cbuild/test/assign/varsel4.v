// another big case
module vector ( out, in1, sel );
   input [127:0] in1;
   input [6:0] 	sel;
   output [0:127] out;
   reg [0:127] 	out;

   initial out=0;
   always @(sel or in1)
     begin
        out[sel +: 128] = in1[sel +: 128];
     end
endmodule // vector
