module test (in, sel, out);
  input [15:0] in;
  input [1:0]  sel;
  output [3:0] out;
  assign       out [3] = in [sel+4];
  assign       out [2] = in [sel+3];
  assign       out [1] = in [sel+2];
  assign       out [0] = in [sel+1];
endmodule // test
