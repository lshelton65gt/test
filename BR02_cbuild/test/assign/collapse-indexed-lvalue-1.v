module top (clk,y,mode_reg,x);
  input clk;
  input [31:0] 	y;
  output [127:0] x;
  reg [127:0] x;
  integer    i, j;
  input [9:0] mode_reg;
  //reg [9:0]  mode_reg;
  integer    burst_length;
  reg [31:0] temp_cmd; 			     
  always @(clk) 
    begin
      for (i = 0; i < 2 + mode_reg[6:4] - 2; i = i + 1)
	begin
	  temp_cmd = {{16{1'b0}}, {15{1'b0}}, 1'b1};
	  for (j = 0; j < 32; j = j + 1)
	    x [32*i + j] = y [j];
	end
    end
endmodule
