module concat (a, x);
  input [7:0] a;
  output [7:0] x;
  assign x [3:0] = a[3:0];
  assign x [7:4] = a[3:0];
endmodule
