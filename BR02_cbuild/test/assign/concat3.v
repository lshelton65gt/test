module test (clk, data, out);
  input clk;
  input [7:0] data;
  output [31:0] out;
  assign 	out [15:0] = {2{data}};
  assign 	out [31:16] = {2{data}};
endmodule
