// self-referencing inference opportunity. this one should fail.
module vector ( out, in, clk );
   input [0:3] in;
   input       clk;
   output [0:3] out;
   reg [0:3] 	out;

   always @( posedge clk )
     begin
	// self-referencing this vector, on an index which has already
	// been computed -- out[3] depends on the new computation of all other
	// out[i]. unsafe to collapse.
	out[0] = in[0];
	out[1] = out[0] & in[1];
	out[2] = out[1] & in[2];
	out[3] = out[2] & in[3];
     end

endmodule // vector
