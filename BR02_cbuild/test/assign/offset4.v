module vector ( out, in1, in2, sel );
   input [10:13] in1;
   input [10:13] in2;
   input 	sel;
   output [0:3] out;
   reg [0:3] 	out;

   always @(sel or in1 or in2)
     begin
    	out[0] = sel ? in1[10] : in2[10];
    	out[1] = sel ? in1[11] : in2[11];
    	out[2] = sel ? in1[12] : in2[12];
    	out[3] = sel ? in1[13] : in2[13];
     end
endmodule // vector
