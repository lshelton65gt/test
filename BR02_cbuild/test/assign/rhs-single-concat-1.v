module test (in, sel, out);
  input [15:0] in;
  input [1:0]  sel;
  output [3:0] out;
  assign      out = {in [sel+3], in [sel+2], in [sel+1], in [sel+0]};
endmodule // test
