module top (clk, a0, c, out);
  input clk;
  input [1:0] a0, c;
  output [3:0] out;
  reg [3:0]    out;
  reg [1:0]   mem [3:0];
  initial
    begin
      mem [0] = 0;
      mem [1] = 0;
      mem [2] = 0;
      mem [3] = 0;
    end
  always @(posedge clk)
    begin
      out [3:2] = c;
      out [1:0] = mem [a0];
    end
  always @(negedge clk)
    begin
      mem [a0] = c;
    end
endmodule
  
