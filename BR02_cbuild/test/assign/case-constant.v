module vector (out0, out1, sel);
  input [1:0] sel;
  output [3:0] out0, out1;
  reg [3:0] out0, out1;
  always @(sel)
    begin
      case (sel)
        2'b00: out0 = 4'b0000;
        2'b01: out0 = 4'b0001;
        2'b10: out0 = 4'b0010;
	2'b11: out0 = 4'b0011;
      endcase
      case (sel)
        2'b00: out1 = 4'b0001;
        2'b01: out1 = 4'b0001;
        2'b10: out1 = 4'b0001;
	2'b11: out1 = 4'b0001;
      endcase
    end
endmodule
