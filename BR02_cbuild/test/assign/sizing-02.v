module top (a, x);
  output [4:0] x;
  input [3:0]  a;
  assign       x [1:0] = a [1:0];
  assign       x [4:2] = {1'b0, a [3:2]};
endmodule // top

  
