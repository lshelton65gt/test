module vector ( out, in );
   input [0:3] in;
   output [0:3] out;
   reg [0:3] 	out;

   always @( in )
     begin
	begin :silly
	   out[3] = in[3];
	   out[2] = in[2];
	   out[1] = in[1];
	   out[0] = in[0];
	end
     end

endmodule // vector
