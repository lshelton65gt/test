module top (clk, a, b, x, y);
  input [31:0] a;
  input [31:0] b;
  input        clk;
  output [31:0] x;
  output [31:0] y;
  reg [9999:0] 	m;
  reg [9999:0] 	n;
  initial m = 0;
  initial n = 0;
  always @(posedge clk)
    begin
      if (a > b) 
	begin
	  m = a;
	  n = b;
	end
      else
	begin
	  m = b;
	  n = a;
	end
    end
  assign x = m;
  assign y = n;
endmodule // top
