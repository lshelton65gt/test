// Test case for bug 4396 - test that un-rewritable
// tristate drivers with Z in concats are detected.
//
// Note that the sim gold file for this testcase, checked in
// as revision 1.1 date: 2005/05/04 14:44:23;  author: joe,
// is incorrect according to Aldec & xz-diff, mismatching 0 vs 1.
//
// This new sim gold file is from cbuild after its handling of this
// construct was corrected.  It has X diffs from the aldec generated
// goldfile, but matches all Z,0,1, and passes xz-diff.

module top(sel0, sel1, d0, d1, d3, io);
   input sel0, sel1;
   input [1:0] d0;
   input [1:0] d1;
   input       d3;
   inout [2:0] io;

   assign io[0] = sel0 ? d0[0] : 1'bz;
   assign io[1] = sel0 ? d0[1] : 1'bz;
   assign io[2:0] = sel1 ? d1[1:0] : {d3,2'bzz};
endmodule
