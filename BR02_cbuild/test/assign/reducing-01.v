module top (p, q, r, a);
  input [11:0] p, q, r;
  output [1:0] a;
  assign a[1] = (!(|p) & q[11]);
  assign a[0] = (!(|p) & q[10]);
endmodule
