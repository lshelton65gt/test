// self-referencing inference opportunity. this one should fail.
module vector ( out, in, clk );
   input [0:3] in;
   input       clk;
   output [0:3] out;

   // self-referencing this vector, on an index which has already
   // been computed -- out[3] depends on the new computation of all other
   // out[i]. unsafe to collapse.
   assign out[0] = in[0];
   assign out[1] = out[0] & in[1];
   assign out[2] = out[1] & in[2];
   assign out[3] = out[2] & in[3];

endmodule // vector
