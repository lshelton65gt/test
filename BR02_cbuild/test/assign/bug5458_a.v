// last mod: Wed Dec 14 22:11:14 2005
// filename: test/assign/bug5458_a.v
// Description:  This test was trimmed down from the test from bug 5458. At one
// time it would hit a NU_ASSERT

module tc (fclk, data, dataIn);
   input         fclk;
   input [7:0] 	 dataIn;
   output 	 data;

   reg [7:0] 	 xmitByte;
   reg           i2cNwr;
   reg           hwReset;



   always @(posedge fclk or posedge hwReset)
     begin
	if (hwReset)
	  begin
	  end
	else
	  begin
             writeXmitData(xmitByte);
             xmitByte = xmitByte + 1;
	  end
     end


   task writeXmitData;
      input [7:0] byte;
      reg [7:0] xmitData;
      begin
         xmitData <= dataIn[7:0];
	 i2cNwr <= 1'b0;
      end
   endtask

endmodule
