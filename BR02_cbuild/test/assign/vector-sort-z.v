// from bug616
module vector ( out1, out2, sel, in );
   input [0:3] in;
   input       sel;
   output [0:3] out1;
   output [0:3] out2;

   assign out1[0] = sel ? in[0] : 1'bz;
   assign out2[0] = sel ? in[0] : 1'bz;
   assign out1[1] = sel ? in[1] : 1'bz;
   assign out2[1] = sel ? in[1] : 1'bz;
   assign out1[2] = sel ? in[2] : 1'bz;
   assign out2[2] = sel ? in[2] : 1'bz;
   assign out1[3] = sel ? in[3] : 1'bz;
   assign out2[3] = sel ? in[3] : 1'bz;

endmodule // vector
