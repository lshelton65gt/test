module top (clk, command, current, mode);
  input clk;
  input [33:0] current;
  input [9:0]  mode;
  output [305:0] command;
  reg [305:0] command;
  reg [31:0] i;

  initial command = 0;
  
  always @(*)
    begin
      for (i = 0; (i < 1); i = (i + 1)) begin
        command[(((i + mode[6:4]) + -1) * 34)] = current[0];
	command[(((i + mode[6:4]) + -1) * 34)+1] = current[1]; 
        command[(((i + mode[6:4]) + -1) * 34)+2] = current[2]; 
        command[(((i + mode[6:4]) + -1) * 34)+3] = current[3]; 
        command[(((i + mode[6:4]) + -1) * 34)+4] = current[4]; 
        command[(((i + mode[6:4]) + -1) * 34)+5] = current[5]; 
        command[(((i + mode[6:4]) + -1) * 34)+6] = current[6]; 
        command[(((i + mode[6:4]) + -1) * 34)+7] = current[7]; 
        command[(((i + mode[6:4]) + -1) * 34)+8] = current[8]; 
        command[(((i + mode[6:4]) + -1) * 34)+9] = current[9]; 
        command[(((i + mode[6:4]) + -1) * 34)+10] = current[10];
        command[(((i + mode[6:4]) + -1) * 34)+11] = current[11];
        command[(((i + mode[6:4]) + -1) * 34)+12] = current[12];
        command[(((i + mode[6:4]) + -1) * 34)+13] = current[13];
        command[(((i + mode[6:4]) + -1) * 34)+14] = current[14];
        command[(((i + mode[6:4]) + -1) * 34)+15] = current[15];
        command[(((i + mode[6:4]) + -1) * 34)+16] = current[16];
        command[(((i + mode[6:4]) + -1) * 34)+17] = current[17];
        command[(((i + mode[6:4]) + -1) * 34)+18] = current[18];
        command[(((i + mode[6:4]) + -1) * 34)+19] = current[19];
        command[(((i + mode[6:4]) + -1) * 34)+20] = current[20];
        command[(((i + mode[6:4]) + -1) * 34)+21] = current[21];
        command[(((i + mode[6:4]) + -1) * 34)+22] = current[22];
        command[(((i + mode[6:4]) + -1) * 34)+23] = current[23];
        command[(((i + mode[6:4]) + -1) * 34)+24] = current[24];
        command[(((i + mode[6:4]) + -1) * 34)+25] = current[25];
        command[(((i + mode[6:4]) + -1) * 34)+26] = current[26];
        command[(((i + mode[6:4]) + -1) * 34)+27] = current[27];
        command[(((i + mode[6:4]) + -1) * 34)+28] = current[28];
        command[(((i + mode[6:4]) + -1) * 34)+29] = current[29];
        command[(((i + mode[6:4]) + -1) * 34)+30] = current[30];
        command[(((i + mode[6:4]) + -1) * 34)+31] = current[31];
        command[(((i + mode[6:4]) + -1) * 34)+32] = current[32];
        command[(((i + mode[6:4]) + -1) * 34)+33] = current[33];
      end
    end
endmodule // top
