module top (data, checkword);
  input [7:0] data;
  output [3:0] checkword;
  assign checkword[0] = (|(data & 8'hb7));
  assign checkword[1] = (|(data & 8'hb7));
  assign checkword[2] = (|(data & 8'h8e));
  assign checkword[3] = (|(data & 8'h8e));
endmodule // top
