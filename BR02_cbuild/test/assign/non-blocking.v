module nonblock ( in, out, clk, sel );
   input [3:0] in;
   input clk;
   input [2:0] sel;
   output [3:0] out;
   reg [3:0] out;

   initial out = 0;

   always @(posedge clk)
     begin
	case(sel) 
	  3'b000: begin
	     out[3] <= out[3] & in[3];
	     out[2] <= out[2] & in[2];
	     out[1] <= out[1] & in[1];
	     out[0] <= out[0] & in[0];
	  end
	  3'b001: begin
	     out[3] <= out[3] & in[0];
	     out[2] <= out[2] & in[3];
	     out[1] <= out[1] & in[2];
	     out[0] <= out[0] & in[1];
	  end
	  3'b010 : begin
	     out[3] <= out[0] & in[0];
	     out[2] <= out[3] & in[3];
	     out[1] <= out[2] & in[2];
	     out[0] <= out[1] & in[1];
	  end
	  3'b011 : begin
	     out[3] <= out[2] & in[0];
	     out[2] <= out[1] & in[3];
	     out[1] <= out[0] & in[2];
	     out[0] <= out[3] & in[1];
	  end
	  3'b100: begin
	     out[0] <= out[0] & in[0];
	     out[1] <= out[1] & in[1];
	     out[2] <= out[2] & in[2];
	     out[3] <= out[3] & in[3];
	  end
	  3'b101: begin
	     out[0] <= out[0] & in[3];
	     out[1] <= out[1] & in[0];
	     out[2] <= out[2] & in[1];
	     out[3] <= out[3] & in[2];
	  end
	  3'b110 : begin
	     out[0] <= out[3] & in[3];
	     out[1] <= out[0] & in[0];
	     out[2] <= out[1] & in[1];
	     out[3] <= out[2] & in[2];
	  end
	  3'b111 : begin
	     out[0] <= out[3] & in[1];
	     out[1] <= out[0] & in[2];
	     out[2] <= out[1] & in[3];
	     out[3] <= out[2] & in[0];
	  end
	endcase // case(sel)
     end
   
endmodule // portInf
