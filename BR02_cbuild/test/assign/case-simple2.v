module vector ( out, sel, in );
   input [0:3] in;
   input       sel;
   output [0:3] out;
   reg [0:3] 	out;

   always @( sel or in )
     begin
	case (sel)
	  1'b0:
	    begin
	       out[3] = 1'b0;
	       out[2] = 1'b0;
	       out[1] = 1'b0;
	       out[0] = 1'b0;
	    end
	  1'b1:
	    begin
	       out[3] = in[3];
	       out[2] = in[2];
	       out[1] = in[1];
	       out[0] = in[0];
	    end
	endcase // case(sel)
     end // always @ ( sel or in )
endmodule // vector
