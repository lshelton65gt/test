// self-referencing inference opportunity. this one should succeed.
module vector ( out, in, clk );
   input [0:3] in;
   input       clk;
   output [0:3] out;
   reg [0:3] 	out;

   initial out = 0; // for simulation.
   
   always @( posedge clk )
     begin
	// self-reference this vector, but an index which hasn't yet
	// been computed. safe to collapse.
	out[3] = out[2] & in[3];
	out[2] = out[1] & in[2];
	out[1] = out[0] & in[1];
	out[0] = in[0];
     end

endmodule // vector
