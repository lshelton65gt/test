// from bug616
module vector ( out, sel, in );
   input [0:3] in;
   input       sel;
   output [0:3] out;

   assign out[0] = sel ? in[0] : 1'bz;
   assign out[1] = sel ? in[1] : 1'bz;
   assign out[2] = sel ? in[2] : 1'bz;
   assign out[3] = sel ? in[3] : 1'bz;

endmodule // vector
