module top (a, x);
  input [3:0] a;
  output [5:0] x;
  assign       x [2:0] = $signed (a [1:0]);
  assign       x [5:3] = $signed (a [3:2]);
endmodule // top
