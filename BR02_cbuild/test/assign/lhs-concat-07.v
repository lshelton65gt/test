module test (a0, a1, a2, a3, b, x0, x1, x2, x3);
  input a0, a1, a2, a3;
  input [3:0] b;
  output      x0, x1, x2, x3;
  assign      x0 = a0 & b [0];
  assign      x1 = a1 & b [1];
  assign      x2 = a2 & b [2];
  assign      x3 = a3 & b [3];
endmodule // test
