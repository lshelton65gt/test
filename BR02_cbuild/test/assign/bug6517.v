module top (clk, sel, out);
  input clk, sel;
  output [3:0] out;
  reg [1:0] mem [7:0];
  initial begin
    mem [0] = 8'h11;
    mem [1] = 8'h00;
    mem [2] = 8'h22;
    mem [3] = 8'h00;
    mem [4] = 8'h00;
    mem [5] = 8'h00;
    mem [6] = 8'h00;
    mem [7] = 8'h00;
  end
  always @(posedge clk) begin
    case (sel)
      0: mem [0] = mem [0];
      1: mem [1] = mem [2];
    endcase
  end
  assign out [1:0] = mem [0];
  assign out [3:2] = mem [1];
endmodule // top
