// Another variation on bug 4396, where the driver of the
// partial-tristate expression is harder to split.  We could support
// this properly if we automatically create a temp for (d1+d0) in
// LFTristate::splitExpr, but since we Alert out on it, it's probably
// easy enough to work around in the field, and obscure enough that
// we shouldn't have to worry about it (famout last words!)
//
// Note that if we demote the alert we will generate code that does
// not convert the last assignment into an enabled driver, and will
// thus stomp valid values written by the first two assignments
// even if sel1 is 0.

module top(sel0, sel1, d0, d1, d3, io);
   input sel0, sel1;
   input [1:0] d0;
   input [3:0] d1;
   input       d3;
   inout [3:0] io;

   assign io[0] = sel0 ? d0[0] : 1'bz;
   assign io[1] = sel0 ? d0[1] : 1'bz;
   assign io[3:0] = sel1 ? (d1 + d0) : {d3,d3,2'bzz};
endmodule
