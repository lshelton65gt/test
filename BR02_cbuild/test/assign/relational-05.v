module top (a, b, p, q, r, s, t, u, v, w, x, y);
  input  		a;
  input [1:0] 		b;
  output  		p, q, r, s, t, u, v, w, x, y;
  assign 	        p = (a == 4'b1) & b[0];
  assign 		q = (a == 4'b1) & b[1];
  assign 		r = b[0] & (a == 4'b1);
  assign 		s = b[1] & (a == 4'b1);
  assign 	        t = (a > 4'b1) & b[0];
  assign 		u = (a > 4'b1) & b[1];
  assign 		v = b[0] & (a > 4'b0);
  assign 		w = b[1] & (a > 4'b0);
  assign 		x = b[0] & (a != 4'b1);
  assign 		y = b[1] & (a != 4'b1);
endmodule // top
