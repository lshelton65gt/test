module top (clk, a, b, op, x);
  input clk;
  input [1:0] op;
  output [7:0] x;
  input [7:0] a;
  input [1:0] b;
  reg [7:0]   mem1 [3:0];
  reg [7:0]   mem2 [3:0];
  reg [7:0]   tmp;
  assign      x = tmp;
  initial
    begin
      tmp = 0;
      mem1 [0] = 8'b0;
      mem1 [1] = 8'b0;
      mem1 [2] = 8'b0;
      mem1 [3] = 8'b0;
    end
  always @(posedge clk) 
    begin
      case (op)
	2'b00: mem1 [b] = a;
	2'b01: mem2 [b] = a;
	2'b10: tmp = mem1 [b];
	2'b11:
	  begin
	    mem1 [b] [7:4] = mem2 [b][3:0];
	    mem1 [b] [3:0] = mem2 [b][3:0];
	  end
	endcase
    end
endmodule // top
