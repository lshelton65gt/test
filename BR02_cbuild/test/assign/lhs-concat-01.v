module test (in, out0, out1);
  input [1:0] in;
  output      out0, out1;
  assign out0 = in [0];
  assign out1 = in [1];
endmodule // test
