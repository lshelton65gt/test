module top(sel,a,b,c,d,w,x,y,z);
   input [1:0] sel;
   input [31:0] a;
   input [127:0] b;
   input [31:0] c;
   input [31:0] d;
   output [31:0] w;
   output [31:0] x;
   output [31:0] y;
   output [31:0] z;

   reg [31:0] w;
   reg [31:0] x;
   reg [31:0] y;
   reg [31:0] z;

   // If we don't rewrite these concats, we'll be stuck with nasty
   // BitVector temporaries in our generated code.
   always @(sel or a or b) begin
      case(sel)
        0: {w,x,y,z} = {4{a}};
        1: {w,x,y,z} = b & {4{a}};
        2: {w,x,y,z} = {a,c,{2{d}}};
        3: {w,x,y,z} = {a,{2{c}},d};
      endcase
   end
endmodule
