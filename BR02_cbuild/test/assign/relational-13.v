module top (t1, t0, h1, h0, x);
  input [1:0] t1, t0, h1, h0;
  output [3:0] x;
  assign       x[1:0] = t1 != h1;
  assign       x[3:2] = t0 != h0;
endmodule // top
