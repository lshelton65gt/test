module vector ( out, in1, in2 );
   input [0:3] in1;
   input [0:3] in2;
   output [0:3] out;
   reg [0:3] 	out;

   function [0:3] silly;
      input [0:3] a, b;
      begin
	 silly[3] = a[3] & b[3];
	 silly[2] = a[2] & b[2];
	 silly[1] = a[1] & b[1];
	 silly[0] = a[0] & b[0];
      end
   endfunction // silly
   
   always @( in1 or in2 )
     begin
	out = silly(in1, in2);
     end

endmodule // vector
