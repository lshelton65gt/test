module test (in0, in1, out);
  input [1:0] in0;
  input [1:0] in1;
  output [3:0] out;
  assign       out [1:0] = in0;
  assign       out [3:2] = in1;
endmodule // test
