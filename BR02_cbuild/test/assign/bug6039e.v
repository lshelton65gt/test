module bug6039b (clk, out, in);
  input clk;
  output [1:0] out;
  input [1:0] in;
  reg [2:0]    mem [1:0];
  always @(posedge clk)
    begin
      mem [1][1] = in [0];
      mem [1][2] = in [1];
    end
  assign out [0] = mem [1][1];
  assign out [1] = mem [1][2];
endmodule
