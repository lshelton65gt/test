module top (clk, a, b, c, x, y, z);
  input clk;
  input [3:0] a, b;
  input [7:0] c;
  output [3:0] x, y;
  output [7:0] z;
  assign x = a & b & c [3:0];
  assign y = a & b & c [7:0];
  assign z = {y,x};
endmodule // top
