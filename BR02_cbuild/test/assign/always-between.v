module vector ( out, in, sel );
   input [0:3] in;
   input       sel;
   output [0:3] out;
   reg [0:3] 	out;

   initial out = 0; // for simulation.
   
   always @( in or sel )
     begin
	// some non-assignment between two bit-level operations is cannot be collapsed.
	out[0] = in[0];
	if (sel)
	  out[2] = in[2];
	else
	  out[3] = in[3];
	out[1] = in[1];
     end

endmodule // vector
