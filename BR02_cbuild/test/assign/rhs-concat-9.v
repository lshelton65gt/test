module top(a, b, c, d, e, f, x);
  input a, b, c, d, e, f;
  output [3:0] x;
  assign       x [1] = a & b;
  assign       x [2] = c & d;
  assign       x [3] = e & ~f;      
endmodule

