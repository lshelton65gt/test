module vector ( out, in );
   input [0:3] in;
   output [0:3] out;
   reg [0:3] 	out;

   function [0:3] silly;
      input [0:3] a;
      begin
	 silly[3] = a[3];
	 silly[2] = a[2];
	 silly[1] = a[1];
	 silly[0] = a[0];
      end
   endfunction // silly
   
   always @( in )
     begin
	out = silly(in);
     end

endmodule // vector
