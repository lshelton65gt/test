module test (a0, a1, a2, b, x0, x1, x2);
  input a0, a1, a2;
  input [2:0] b;
  output      x0, x1, x2;
  assign      x0 = b [1];
  assign      x1 = b [2];
  assign      x2 = b [2];      
endmodule // test
