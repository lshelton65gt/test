module concat(en,data,left_wide,right_wide,o3,o2,o1);
   output o3,o2,o1;
   output [1:0] left_wide;
   output [1:0] right_wide;
   input 	data;
   input 	en;
   wire 	en_n = ~en;
   assign 	{o3,left_wide,o2,right_wide,o1} = (en_n==0) ? {7{data}} : {7{1'bz}};
endmodule
