module top (clk, a, b, x, y, z);
  input clk;
  input [3:0] a, b;
  output [3:0] x, y;
  output [7:0] z;
  assign x = a & b;
  assign y = a & b;
  assign z = {y,x};
endmodule // top
