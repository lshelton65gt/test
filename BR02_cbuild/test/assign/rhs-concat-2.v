module test (in0, in1, in2, in3, out);
  input [1:0] in0;
  input [1:0] in1;
  input [1:0] in2;
  input [1:0] in3;
  output [7:0] out;
  assign       out [1:0] = in0;
  assign       out [3:2] = in1;
  assign       out [7:6] = in3;
endmodule // test
