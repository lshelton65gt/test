module top (clk,in,out1,out2);
  input clk,in;
  output [3:0] out1;
  output [3:0] out2;
  reg [3:0]    out1;
  reg [3:0]    out2;
  always @(posedge clk)
    begin
      out1[1:0] = in;
      out1[3:2] = in;
      out2[0] = in;
      out2[1] = in;
      out2[3:2] = in;
    end
endmodule
