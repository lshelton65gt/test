module top(c,r,d,x);
   input c;
   input r;
   input d;
   output [2:0] x;
   reg [2:0] x;

   initial x = 3'b000;
   
   // Two options:
   // 1. recognize & recode this as x = x << 1;
   // 2. change ConcatRewrite to rewrite and order the statements:
   //    x[2] = x[1];
   //    x[1] = x[0];
   //    x[0] = 1'b0;
   always @(posedge c) begin
      if (r) begin
	 {x[0],x[1],x[2]} = {1'b0,x[0],x[1]};
      end else begin
	 {x[0],x[1],x[2]} = {d,x[0],x[1]};
      end
   end
endmodule
