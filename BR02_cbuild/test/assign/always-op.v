module vector ( out, in1, in2 );
   input [0:3] in1;
   input [0:3] in2;
   output [0:3] out;
   reg [0:3] 	out;

   always @( in1 or in2 )
     begin
	out[0] = in1[0] & in2[0];
	out[1] = in1[1] & in2[1];
	out[2] = in1[2] & in2[2];
	out[3] = in1[3] & in2[3];
     end

endmodule // vector
