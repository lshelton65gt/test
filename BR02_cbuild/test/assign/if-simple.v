module vector ( out, sel, in );
   input [0:3] in;
   input       sel;
   output [0:3] out;
   reg [0:3] 	out;

   always @( sel or in )
     begin
	if (sel) begin
	   out[0] = in[0];
	   out[1] = in[1];
	   out[2] = in[2];
	   out[3] = in[3];
	end
     end

endmodule // vector
