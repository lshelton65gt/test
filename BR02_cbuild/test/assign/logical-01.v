module top (ena, in, out);
  input ena;
  input [1:0] in;
  output [1:0] out;
  assign       out[0] = in[0] && ena;
  assign       out[1] = in[1] && ena;
endmodule // top
