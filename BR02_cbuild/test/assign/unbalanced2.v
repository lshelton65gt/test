module top(r2d_lan_ra_in,o); // carbon disallowFlattening
   input [11:0] r2d_lan_ra_in;
   output [11:0] o;
   sub sub(.i({1'b1,r2d_lan_ra_in[10:7],1'b0,r2d_lan_ra_in[5:1],1'b0}),.o(o));
endmodule

module sub(i,o);
   input [11:0] i;
   output [11:0] o;
   assign o = i;
endmodule
