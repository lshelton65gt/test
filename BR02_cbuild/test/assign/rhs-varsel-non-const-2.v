module top(clk, in, idx, out);
  input clk;
  input [15:0] in;
  input [2:0] idx;
  output [7:0] out;

  assign out [0] = in [idx+0];
  assign out [1] = in [idx+1];
  assign out [2] = in [idx+2];
  assign out [3] = in [idx+3];
  assign out [4] = in [idx+4];
  assign out [5] = in [idx+5];
  assign out [6] = in [idx+6];
  assign out [7] = in [idx+7];

endmodule
