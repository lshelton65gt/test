module vector (in, idx, out);
  input [7:0] in;
  input [1:0] idx;
  output [3:0] out;
  assign out[0] = in[idx[1:0]+0];
  assign out[1] = in[idx[1:0]+1];
  assign out[2] = in[idx[1:0]+2];
  assign out[3] = in[idx[1:0]+3];
endmodule // vector
