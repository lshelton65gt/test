// This test doesn't actually fold the vector because we don't handle
// && in inference and fold doesn't convert && with a single bit to &.
// There's commented out code in reduce/Inference.cxx and code enabled
// under  -foldBitTest in reduce/Fold.cxx that handles this.  But turning
// that code on causes problems in clock analysis as well as making the
// emc test fail

module vector ( out, sel, in1, in2 );
   input [0:3] in1;
   input [0:3] in2;
   input       sel;
   output [0:3] out;
   reg [0:3] 	out;

   always @( sel or in1 or in2 )
     begin
	if (sel) begin
	   out[0] = in1[0] && in2[0];
	   out[1] = in1[1] && in2[1];
	   out[2] = in1[2] && in2[2];
	   out[3] = in1[3] && in2[3];
	end
     end
endmodule // vector
