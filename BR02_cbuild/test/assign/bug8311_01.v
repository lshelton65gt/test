module top (clk, a, x);
  input clk;
  input [31:0] a;
  output [31:0] x;
  reg signed [31:0] r;
  assign x = r;
  initial
    r = 32'b0;
  always @(negedge clk)
    r = a;
  always @(posedge clk)
    r = r >>> 1;
endmodule
