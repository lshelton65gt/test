// test bitfields with negative offset, such that nonexisting bits are always
// lsbs

module top(i,o);
   parameter WORD=32,
             FIELD=16;
   
   input [0:WORD-1] i;
   output[0:WORD-1] o;

   reg [0:WORD-1]   temp;

   assign       o = temp;
   always @(i)
     begin: blk
        integer k;
        reg [0:FIELD-1] value;
        reg [0:WORD-1] hold;
        for(k=0; k<WORD; k=k+1)
          begin
             value = i[k-:FIELD];
             hold[k-:FIELD] = value;

             // set flag if value we read is different from what we wrote
             temp[k] = (hold[k-:FIELD] != value);
          end
     end
endmodule
