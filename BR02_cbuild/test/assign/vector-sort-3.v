// from bug616
module top (hclk, a, out);
  input hclk;
  input [2:0] a;
  output [2:0] out;
  register out_reg(.clk(hclk), 
		   .a({a[2], a[0], a[1]}),
		   .z({out[2], out [0], out[1]}));
endmodule

module register (clk, a, z);
 input clk;
 input [2:0] a;
 output [2:0] z;
 reg [2:0] z;
 always @( posedge clk)
   z = a;
endmodule

