module top (a, b, x);
  output [4:0] x;
  input [3:0]  a;
  input [3:0]  b;
  assign       x [1:0] = a [1:0] & b [1:0];
  assign       x [4:2] = {0, a [3:2] & b [4:2]};
endmodule // top

  
