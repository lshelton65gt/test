module top (clk, sel, in, out);
  input clk;
  input [1:0] sel;
  input [1:0] in;
  output [1:0] out;
  reg [1:0]    out;
  reg [1:0] mem [3:0];
  initial
    begin
      mem [0] = 2'b00;
      mem [1] = 2'b00;
      mem [2] = 2'b00;
      mem [3] = 2'b00;
      out = 3'b11;
    end
  always @(posedge clk)
    begin
      mem [sel] = in;
    end
  always @(negedge clk)
    begin
      out [0] = mem [sel][1:0] == 2'b01 ? 1 : 0;
      out [1] = mem [sel][1:0] == 2'b01 ? 1 : 0;
    end
endmodule // top
