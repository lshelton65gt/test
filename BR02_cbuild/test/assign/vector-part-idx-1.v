module vector (in, idx, out);
  input [0:3] in;
  input [0:3] idx;
  output [0:3] out;
  assign out[0] = in[idx[0:1]];
  assign out[1] = in[idx[0:1]];
  assign out[2] = in[idx[0:1]];
  assign out[3] = in[idx[0:1]];
endmodule // vector
