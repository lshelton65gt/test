// from bug616
module vector ( out1, out2, in );
   input [0:3] in;
   output [0:3] out1;
   output [0:3] out2;

   assign out1[0] = in[0];
   assign out2[0] = in[0];
   assign out1[1] = in[1];
   assign out2[1] = in[1];
   assign out1[2] = in[2];
   assign out2[2] = in[2];
   assign out1[3] = in[3];
   assign out2[3] = in[3];

endmodule // vector
