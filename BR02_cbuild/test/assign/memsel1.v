module test (clk, addr, out);
  input clk;
  input [6:0] addr;
  reg [7:0] mem [127:0];
  output [7:0] out;
  reg [7:0] out;
  always @(clk) begin
    out [0] = mem [addr] [0];
    out [1] = mem [addr] [1];
    out [2] = mem [addr] [2];
    out [3] = mem [addr] [3];
  end
endmodule
