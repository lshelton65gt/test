module top (a, b, c);
  input [3:0] a;
  output [7:0] b, c;
  reg [7:0] 	b, c;
  always @(a)
    begin
      b [7] = a [3];
      b [6] = a [3];
      b [5] = a [3];
      b [4] = a [3];
      b [3] = a [3];
      b [2] = a [3];
      b [1] = a [3];
      b [0] = a [3];
    end
  always @(a)
    begin
      c [7:6] = a [1:0];
      c [5:4] = a [1:0];
      c [3:2] = a [1:0];
      c [1:0] = a [1:0];
    end
endmodule // top
