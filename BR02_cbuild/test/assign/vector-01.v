module top (a, b, c);
  input [63:0] a;
  output [63:0] b, c;
  reg [63:0] 	b, c;
  always @(a)
    begin
      b [63:32] = a [31:0];
      b [31:0] = a [31:0];
    end
  always @(a)
    begin
      c [63:32] = a [63:32];
      c [31:0] = a [63:32];
    end
endmodule // top
