module top (a, b, u, v, w, x, y);
  input 		a;
  input [3:0] 		b;
  output [3:0] 		u, v, w, x, y;
  assign 	        u[1:0] = (a == 1'b1) & b[1:0];
  assign 		u[3:2] = (a == 1'b1) & b[3:2];
  assign 		v[1:0] = b[1:0] & (a == 1'b1);
  assign 		v[3:2] = b[3:2] & (a == 1'b1);
  assign 	        w[1:0] = (a > 1'b1) & b[1:0];
  assign 		w[3:2] = (a > 1'b1) & b[3:2];
  assign 		x[1:0] = b[1:0] & (a > 1'b0);
  assign 		x[3:2] = b[3:2] & (a > 1'b0);
  assign 		y[1:0] = b[1:0] & (a != 1'b1);
  assign 		y[3:2] = b[3:2] & (a != 1'b1);
endmodule // top
