module vector ( out, in );
   input [0:3] in;
   output [0:3] out;
   reg [0:3] 	out;

   task silly;
      output [0:3] o;
      input [0:3] a;
      begin
	 o[0] = a[0];
	 o[1] = a[1];
	 o[2] = a[2];
	 o[3] = a[3];
      end
   endtask // silly
   
   always @( in )
     begin
	silly(out,in);
     end

endmodule // vector
