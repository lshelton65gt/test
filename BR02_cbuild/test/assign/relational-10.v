module test(clk, in0, in1, out);
  input clk;
  input [1:0] in0;
  input [1:0] in1;
  output [3:0] out;

  reg [3:0]          memorybv_mem;

  assign out[1:0] = ((memorybv_mem[1:0] == 2'h0) ? 2'h3 : 2'h0);
  assign out[3:2] = ((memorybv_mem[3:2] == 2'h0) ? 2'h3 : 2'h0);

  initial memorybv_mem = 3'b000;
  
  always @(posedge clk)
    begin
      memorybv_mem[1:0] = in0;
    end
endmodule
