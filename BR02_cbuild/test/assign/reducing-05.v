module top (p, a, b, c);
  input [3:0] p;
  output [1:0] a, b, c;
  assign       a[1] = |p;
  assign       a[0] = |p;
  assign       b [1] = &p;
  assign       b [0] = &p;
  assign       c [1] = ^p;
  assign       c [0] = ^p;
endmodule
