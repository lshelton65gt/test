module ternary ( clk,asel,bsel,csel,a,b,out );
   input clk;
   input [1:0] asel;
   input [1:0] bsel;
   input       csel;
   input [1:0] a,b;
   output [1:0] out;
   reg [1:0] 	out;

   initial out = 0;
   integer i;
   always @(posedge clk) begin
    for (i=0;i<=1;i=i+1)
      if (asel[i])
	out[i] = 1'b0;
      else
      case (csel)
	1'b0: if (bsel[i]) out[i] = a[i];
	1'b1: if (bsel[i]) out[i] = b[i];
      endcase
   end
endmodule
