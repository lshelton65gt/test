module varsel ( out, in, sel );
   input [1:4] in;
   input       sel;
   output [0:3] out;
   reg [0:3] 	out;
   
   always @(in or sel)
     begin:named
        integer i;
        for (i = 0; i<4; i=i+2)
          out[i +: 2] = in[(i+1) +: 2] & {sel,sel};
     end
endmodule // varsel
