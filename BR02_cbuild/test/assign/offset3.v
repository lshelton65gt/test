module vector ( out, in1, in2, sel );
   input [0:3] in1;
   input [0:3] in2;
   input 	sel;
   output [10:13] out;
   reg [10:13] 	out;

   always @(sel or in1 or in2)
     begin
    	out[10] = sel ? in1[0] : in2[0];
    	out[11] = sel ? in1[1] : in2[1];
    	out[12] = sel ? in1[2] : in2[2];
    	out[13] = sel ? in1[3] : in2[3];
     end
endmodule // vector
