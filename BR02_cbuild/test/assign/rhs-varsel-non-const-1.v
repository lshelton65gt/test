module top(clk, in, idx, out);
  input clk;
  input [7:0] in;
  input [2:0] idx;
  output [7:0] out;

  assign out [0] = in [idx];
  assign out [1] = in [idx];
  assign out [2] = in [idx];
  assign out [3] = in [idx];
  assign out [4] = in [idx];
  assign out [5] = in [idx];
  assign out [6] = in [idx];
  assign out [7] = in [idx];

endmodule
