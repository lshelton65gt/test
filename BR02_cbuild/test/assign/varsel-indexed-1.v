module top (clk, current, mode, command);
  input clk;
  input [3:0] current;
  input [1:0]  mode;
  output [31:0] command;
  reg [31:0] command;
  reg [31:0] i;
  initial command = 0;
  always @(posedge clk)
    begin
      for (i = 0; i < 2; i = i + 1) begin
        command[4*(i + mode[1:0])] = current[0];
	command[4*(i + mode[1:0])+1] = current[1]; 
        command[4*(i + mode[1:0])+2] = current[2]; 
        command[4*(i + mode[1:0])+3] = current[3]; 
      end
  end
endmodule // top
