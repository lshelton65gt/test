module top(sel,out);
   input [3:0] sel;
   output [3:0] out;

   assign 	out[0] = sel[0] ? 1'b0 : 1'b1;
   assign 	out[1] = sel[1] ? 1'b1 : 1'b0;
   assign 	out[2] = sel[2] ? 1'b0 : 1'b1;
   assign 	out[3] = sel[3] ? 1'b1 : 1'b0;
endmodule
