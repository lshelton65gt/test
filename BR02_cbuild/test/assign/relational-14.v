module top (t0, t1, t2, h0, h1, h2, b, x);
  input t0, t1, t2, h0, h1, h2;
  input [1:0] b;
  output [2:0] x;
  assign       x[2] = (t2 != h2) & b[1];
  assign       x[1] = (t1 != h1) & b[1];
  assign       x[0] = (t0 != h0) & b[1];
endmodule
