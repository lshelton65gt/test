module top (a1, a0, b1, b0, c1, c0, x);
  input [1:0] a1, a0, b1, b0, c1, c0;
  output [3:0] x;
  assign       x[1:0] = (a1 != b1) & c1;
  assign       x[3:2] = (a0 != b0) & c0;
endmodule // top
