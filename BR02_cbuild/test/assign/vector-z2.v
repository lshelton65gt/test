// from bug616
module vector ( out, in );
   input [0:3] in;
   output [0:3] out;

   // these statements cannot be vectorized -- we don't know how to
   // handle dynamic indexing and Z.
   assign out[0] = in[0] ? 1'b0 : 1'bz;
   assign out[1] = in[1] ? 1'b0 : 1'bz;
   assign out[2] = in[2] ? 1'b0 : 1'bz;
   assign out[3] = in[3] ? 1'b0 : 1'bz;

endmodule // vector
