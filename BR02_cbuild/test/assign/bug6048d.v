module bug(clk,sel1,sel2,a,b,c,d,e,f,g,h,i,j,k,l,out);
   input clk;
   input sel1,sel2;
   input a,b,c,d,e,f;
   input g,h,i,j,k,l;
   output [5:0] out;
   reg [5:0] out;

   always @(posedge clk) begin
      out = { (sel1 ? a : b), (sel2 ? g : h),
	      (sel1 ? c : d), (sel2 ? i : j),
	      (sel1 ? e : f), (sel2 ? k : l) };
   end
endmodule
