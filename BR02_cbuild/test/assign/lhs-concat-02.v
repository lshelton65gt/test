module test (a0, a1, b, x0, x1);
  input a0, a1;
  input [1:0] b;
  output      x0, x1;
  assign      x0 = a0 & b [0];
  assign      x1 = a1 & b [1];
endmodule // test
