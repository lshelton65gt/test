module top (a, x);
  output [3:0] x;
  input [3:0]  a;
  assign       x [1:0] = a [1:0] & 3;
  assign       x [3:2] = a [3:2] & 2;
endmodule // top

  
