module top(a,b,x);
   input a;
   input [1:0] b;
    output [3:0] x;
   assign x = {b,a,b};
endmodule

