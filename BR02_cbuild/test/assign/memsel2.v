module test (clk, out);
  input clk;
  reg [7:0] mem [0:-127];
  output [7:0] out;
  reg [7:0] out;
  initial mem [-1] = 8'b01010101;
  always @(clk) begin
    out [0] = mem [-1] [0];
    out [1] = mem [-1] [1];
    out [2] = mem [-1] [2];
    out [3] = mem [-1] [3];
  end
endmodule
