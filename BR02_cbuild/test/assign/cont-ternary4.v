module vector ( out1, out2, in1, in2, sel );
   input [0:3] in1;
   input [0:3] in2;
   input 	sel;
   output [0:3] out1;
   output [0:3] out2;

   assign 	out2[3] = sel ? in1[3] : in2[3];
   assign 	out2[2] = sel ? in1[2] : in2[2];
   assign 	out2[1] = sel ? in1[1] : in2[1];
   assign 	out2[0] = sel ? in1[0] : in2[0];

   assign 	out1[3] = sel ? in1[3] : in2[3];
   assign 	out1[2] = sel ? in1[2] : in2[2];
   assign 	out1[1] = sel ? in1[1] : in2[1];
   assign 	out1[0] = sel ? in1[0] : in2[0];
endmodule // vector
