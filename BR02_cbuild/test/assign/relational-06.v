module top (a, b, u, v, w, x, y);
  input  		a;
  input [1:0] 		b;
  output [1:0] 		u, v, w, x, y;
  assign 	        u[0] = (a == 4'b1) & b[0];
  assign 		u[1] = (a == 4'b1) & b[1];
  assign 		v[0] = b[0] & (a == 4'b1);
  assign 		v[1] = b[1] & (a == 4'b1);
  assign 	        w[0] = (a > 4'b1) & b[0];
  assign 		w[1] = (a > 4'b1) & b[1];
  assign 		x[0] = b[0] & (a > 4'b0);
  assign 		x[1] = b[1] & (a > 4'b0);
  assign 		y[0] = b[0] & (a != 4'b1);
  assign 		y[1] = b[1] & (a != 4'b1);
endmodule // top
