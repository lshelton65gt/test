module vector ( out, in1, in2, sel, rst );
   input [0:3] in1;
   input [0:3] in2;
   input 	sel;
   input 	rst;
   
   output [0:3] out;
   reg [0:3] 	out;

   always @(rst or sel or in1 or in2)
     begin
	if (rst) begin
	   out[0] = 1'b0;
	   out[1] = 1'b0;
	   out[2] = 1'b0;
	   out[3] = 1'b0;
	end else begin
	   out[0] = sel ? in1[0] : in2[0];
	   out[1] = sel ? in1[1] : in2[1];
	   out[2] = sel ? in1[2] : in2[2];
	   out[3] = sel ? in1[3] : in2[3];
	end // else: !if(rst)
     end

endmodule // vector
