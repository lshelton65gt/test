module vector ( out, in, other );
   input [3:0] in;
   input [3:0] other;
   output [3:0] out;
   reg [3:0] 	out;

   always @( in or other )
     begin
	out[0] = in[0] ^ other[1] ^ other[2];
	out[1] = in[1] ^ other[1] ^ other[2];
	out[2] = in[2] ^ other[1] ^ other[2];
	out[3] = in[3] ^ other[1] ^ other[2];
     end

endmodule // vector
