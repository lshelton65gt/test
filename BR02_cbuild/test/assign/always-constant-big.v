module vectorize(A,B,C,D,int_out);
   parameter size=256;
   input [size-1:0] A,B,C;
   input [7:0] D;
   output [size-1:0] int_out;
   reg [size-1:0] int_out;
   integer 	  i;   
   always @(A or B or C or D or i) begin
      for (i=0;i<size;i=i+1)
	int_out[i] = ( ( A[i] &&  B[i] &&  C[i] && D[7]) ||
		       ( A[i] &&  B[i] && !C[i] && D[6]) ||
		       ( A[i] && !B[i] &&  C[i] && D[5]) ||
		       ( A[i] && !B[i] && !C[i] && D[4]) ||
		       (!A[i] &&  B[i] &&  C[i] && D[3]) ||
		       (!A[i] &&  B[i] && !C[i] && D[2]) ||
		       (!A[i] && !B[i] &&  C[i] && D[1]) ||
		       (!A[i] && !B[i] && !C[i] && D[0]) );
   end
endmodule
