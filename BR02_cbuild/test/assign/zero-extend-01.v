module binary_1(in1, in2, in3, Sout_x, Sout_y);
  input [1:0] in1;
  input [1:0] in2;
  input [1:0] in3;
  output      reg signed [1:0] Sout_x, Sout_y;
  reg [1:0]   U1;
  reg [1:0]   U2;
  reg [1:0]   U3;
  always @(*)
    begin: main
      U1 = in1;
      U2 = in2;
      U3 = in3;
      Sout_y = (U1 ===  U2) === U3;
      Sout_x =  U1 ===  U2  === U3;
     end
endmodule
