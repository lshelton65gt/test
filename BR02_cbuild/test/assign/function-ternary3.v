module vector ( out, in1, in2, sel );
   input [0:3] in1;
   input [0:3] in2;
   input [0:3] sel;
   
   output [0:3] out;
   reg [0:3] 	out;

   function [0:3] silly;
      input [0:3] sel;
      input [0:3] in1, in2;
      begin
	 silly[0] = sel[0] ? in1[0] : in2[0];
	 silly[1] = sel[1] ? in1[1] : in2[1];
	 silly[2] = sel[2] ? in1[2] : in2[2];
	 silly[3] = sel[3] ? in1[3] : in2[3];
      end
   endfunction // silly

   always @(sel or in1 or in2 )
     begin
	out = silly(sel,in1,in2);
     end

endmodule // vector
