module test (clk, in, out);
  input clk;
  input [7:0] in;
  output [7:0] out;
  reg [1:0]    mem [3:0];
  reg [7:0]    outreg;
  assign       out = outreg;
  initial begin
    outreg = 8'b0;
    mem [0] = 2'b0;
    mem [1] = 2'b0;
    mem [2] = 2'b0;
    mem [3] = 2'b0;
  end
  always @(negedge clk) begin
    mem [0] = in [1:0];
    mem [1] = in [3:2];
    mem [2] = in [5:4];
    mem [3] = in [7:6];
  end
  always @(posedge clk) begin
    outreg [1:0] = mem [0];
    outreg [3:2] = mem [1];
    outreg [5:4] = mem [2];
    outreg [7:6] = mem [3];
  end
endmodule // test
