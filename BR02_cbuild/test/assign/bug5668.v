module top(a,b,c,d,i);
  output a,b,c,d;
  input [3:0] i;
  assign { a, {b, {c, d} } } = i;
endmodule
