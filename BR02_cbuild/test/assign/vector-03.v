// from bug616
module top (a, b, x);
  input [7:0] a,b;
  output [7:0] x;
  assign x [7] = a [3];
  assign x [6] = a [2];
  assign x [5] = a [1];
  assign x [4] = a [0];
  assign x [3] = b [7] & a [3];
  assign x [2] = b [6] & a [2];
  assign x [1] = b [5] & a [1];
  assign x [0] = b [5] & a [0];
endmodule // top
