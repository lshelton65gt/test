// from bug616
module vector ( out1, out2, sel1, sel2, in );
   input [0:3] in;
   input sel1;
   input sel2;
   output [0:3] out1;
   output [0:3] out2;

   assign out1[0] = sel1 ? in[0] : 1'bz;
   assign out2[0] = sel2 ? in[0] : 1'bz;
   assign out1[1] = sel1 ? in[1] : 1'bz;
   assign out2[1] = sel2 ? in[1] : 1'bz;
   assign out1[2] = sel1 ? in[2] : 1'bz;
   assign out2[2] = sel2 ? in[2] : 1'bz;
   assign out1[3] = sel1 ? in[3] : 1'bz;
   assign out2[3] = sel2 ? in[3] : 1'bz;

endmodule // vector
