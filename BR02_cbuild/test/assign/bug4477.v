// astute test case?

module top(c,h,f,ostate);
   input h,c;
   input [7:0] f;
   output [55:0] ostate;

   reg [55:0]    state;

   assign       ostate = state;
   always @(posedge c)
     begin
        case ({h,f[7]})
          2'b00: state = 1<<54;
          2'b01: state = 1<<53;
          2'b10: state = 1<<52;
          2'b11: state = 1<<51;
        endcase
     end
endmodule
