module top (a, b, p, q, r, s, t, u, v, w, x, y);
  input 		a;
  input [3:0] 		b;
  output [1:0] 		p, q, r, s, t, u, v, w, x, y;
  assign 	        p = (a == 1'b1) & b[1:0];
  assign 		q = (a == 1'b1) & b[3:2];
  assign 		r = b[1:0] & (a == 1'b1);
  assign 		s = b[3:2] & (a == 1'b1);
  assign 	        t = (a > 1'b1) & b[1:0];
  assign 		u = (a > 1'b1) & b[3:2];
  assign 		v = b[1:0] & (a > 1'b0);
  assign 		w = b[3:2] & (a > 1'b0);
  assign 		x = b[1:0] & (a != 4'b1);
  assign 		y = b[3:2] & (a != 4'b1);
endmodule // top
