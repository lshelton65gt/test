`include "fullpart_be.v"

module bv(i,o);
   input [127:0] i;
   output [127:0] o;

   top #(128,65) bvX (i,o);
endmodule
