module concat(en,data,data0,o6,o5,o4,o3,o2,o1,o_wide);
   output o6,o5,o4,o3,o2,o1;
   output [1:0] o_wide;
   input [7:0] 	data;
   input 	data0;
   input 	en;
   wire 	en_n = ~en;
   assign 	{o6,o5,o4,o3,o2,o1,o_wide} = (en_n==0) ? 8'bzzzzzzzz : {data[7:1],data0};
endmodule
