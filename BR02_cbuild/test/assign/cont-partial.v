module vector ( out, in1, in2, sel );
   input [0:3] in1;
   input [0:3] in2;
   input 	sel;
   output [0:3] out;

   assign 	out[0] = sel ? in1[0] : in2[0];
   assign 	out[1] = sel ? in1[1] : in2[1];
   assign 	out[2] = sel ? in1[2] : in2[2];
   assign 	out[3] = (!sel) ? in1[3] : in2[3];

endmodule // vector
