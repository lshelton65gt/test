module top(p2cmdfifordata,xout,xout2);
   input         [63:0]         p2cmdfifordata;
   output         [15:0]          xout;
   output         [4:0]          xout2;
   sub s0(p2cmdfifordata,xout,xout2);
endmodule

module sub(p2cmdfifordata,xout,xout2);
  input         [63:0]         p2cmdfifordata;
  output         [15:0]          xout;
  output         [4:0]          xout2;
  wire         [63:0]         p2cmdfifordata;
  wire         [1:0]          p2cmd;
  wire         [2:0]          p2dw0swiz;
  wire         [2:0]          p2dw1swiz;
  wire         [2:0]          p2dw2swiz;
  wire         [2:0]          p2dw3swiz;
  wire         [3:0]          p2dwexp;
  wire [4:0] 		      p2dwstart;
  assign p2cmd = p2cmdfifordata[52:51];
  assign p2dwstart = ((p2cmd == 2'h3) ? p2cmdfifordata[33:29] : 5'h00);
  assign p2dw0swiz = ((p2cmdfifordata[52:51] == 2'h3) ? p2cmdfifordata[36:34] : 3'h0);
  assign p2dw1swiz = ((p2cmdfifordata[52:51] == 2'h3) ? p2cmdfifordata[39:37] : 3'h1);
  assign p2dw2swiz = ((p2cmdfifordata[52:51] == 2'h3) ? p2cmdfifordata[42:40] : 3'h1);
  assign p2dw3swiz = ((p2cmdfifordata[52:51] == 2'h3) ? p2cmdfifordata[45:43] : 3'h1);
  assign p2dwexp = ((p2cmdfifordata[52:51] == 2'h3) ? p2cmdfifordata[49:46] : 4'h0);

  assign xout = {p2dwexp,p2dw3swiz,p2dw2swiz,p2dw1swiz,p2dw0swiz};
  assign xout2 = ~p2dwstart;
endmodule
