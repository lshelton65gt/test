module top (clk_dsp, trap_abort_instr, excep_instr, cache_stg0, instr_stg1, instr_buf_out, sel_instr_src_next, instr_stg0);
  input clk_dsp;
  input [31: 0] cache_stg0, instr_stg1, instr_buf_out;
  output [31:0] excep_instr;
  output [31:0]   instr_stg0;
  input 	  trap_abort_instr;
  input [3:0] 	  sel_instr_src_next;
  assign excep_instr[31: 0] = {8'hc4, trap_abort_instr, 23'h00_0000};
  vector_mux_sel_reg #(32, 4, 2, 16)  u_instr_mux
    (
    .clk      (   clk_dsp                     ),
    .operand  ( { excep_instr       [31: 0],
                  cache_stg0        [31: 0],
                  instr_stg1        [31: 0],
                  instr_buf_out     [31: 0] } ),
    .load_sel (   1'b1                        ),
    .sel_next (   sel_instr_src_next[ 1: 0]   ),
    .stall    (   1'b0                        ),
    .result   (   instr_stg0        [31: 0]   )
    );
endmodule

module vector_mux_sel_reg
  (
  clk      ,
  load_sel ,
  operand  ,
  sel_next ,
  stall    ,
  result
  );
  parameter N = 32;
  parameter M = 4;
  parameter Q = 2;
  parameter S = 4;
  parameter W = N*(1<<Q);
  input           clk;
  input           load_sel;
  input [N*M-1:0] operand;
  input [  Q-1:0] sel_next;
  input           stall;
  output [N-1:0] result;
  reg [    (1<<Q)-1:0] mux_word;
  reg [(N*(1<<Q))-1:0] padded_operand;
  reg [         N-1:0] result;
  reg [         Q-1:0] sel_word;
  reg [       S*Q-1:0] sel_reg;
  reg [       N*Q-1:0] sel_vector;
  integer i;
  integer j;
  integer k;
  integer t;
  integer u;
  initial
    if (((N/S) * S) != N)
    begin
      $display("Error!  Mux with registered selects has invalid parameters at %m!");
      $display("        N=%0d, S=%0d", N, S);
      $finish;
    end
  always @(posedge clk)
    if (~stall & load_sel)
      for (u=0; u<Q; u=u+1)
        for (t=0; t<S; t=t+1)
          sel_reg[(u*S)+t] <= sel_next[u];
  always @(operand or sel_reg)
  begin
    padded_operand          = {{W{1'b0}}};
    padded_operand[M*N-1:0] = operand;
    for (k=0; k<Q; k=k+1)
      for (j=0; j<S; j=j+1)
        for (i=0; i<(N/S); i=i+1)
          sel_vector[(k*N)+(j*(N/S))+i] = sel_reg[(k*S)+j];
    for (j=0; j<N; j=j+1)
    begin
      for (i=0; i<M; i=i+1)
        mux_word[i] = padded_operand[i*N+j];
      for (i=0; i<Q; i=i+1)
        sel_word[i] = sel_vector[i*N+j];
      result[j] = mux_word[sel_word];
    end
    `ifdef CHECK_MUX
    if (sel_word >= M)
    begin
      $display("Warning!  Mux select hex:%h exceeds parameterized maximum hex:%0h", sel_word, (M-1));
      $display("          %m\n");
    end
    `endif
  end
endmodule

