// Test case for bug 4396 - test that concatenations of Z
// are handled by tristate driver analysis.

module top(testmode, ktst_iddq_mode, clk, o);
   input [4:0] testmode;
   input       ktst_iddq_mode;
   input       clk;
   output [1:0] o;
   reg [1:0]    o;

   wire [4:0]  pi_testmode;

   bottom bottom (testmode[4:0], pi_testmode, ktst_iddq_mode);

   always @(posedge clk)
     begin
        casez({pi_testmode[4:0]})
          5'b00000: o = 2'd0;
          5'b01100: o = 2'd1;
          default: o = 2'd3;
        endcase
     end
endmodule

module bottom(testmode, pi_testmode, ktst_iddq_mode);
   input ktst_iddq_mode;
   input [4:0] testmode;
   output  [4:0] pi_testmode;

   
        PDDWDGZ u_testmode0(
                .PAD                            (testmode[0]), 
                .C                              (pi_testmode[0]), 
                .REN                            (ktst_iddq_mode));
        PDDWDGZ u_testmode1(
                .PAD                            (testmode[1]), 
                .C                              (pi_testmode[1]), 
                .REN                            (ktst_iddq_mode));
         PDDWDGZ u_testmode2(
                 .PAD                            (testmode[2]), 
                 .C                              (pi_testmode[2]), 
                 .REN                            (ktst_iddq_mode));
        PDDWDGZ u_testmode3(
                .PAD                            (testmode[3]), 
                .C                              (pi_testmode[3]), 
                .REN                            (ktst_iddq_mode));
   assign        pi_testmode[4] = u_testmode3.pull ? 0 : 1'bz;
endmodule

module PDDWDGZ(PAD, C, REN);

        parameter               PullTime        = 100000;

        input                   PAD;
        input                   REN;
        output                  C;

        reg                     lastPAD;
        reg                     pull;
        wire                    C_buf;
        wire                    RE;

        specify

        (PAD => C) = (0, 0);
        endspecify

        bufif1 (weak0, weak1) (C_buf, 1'b0, pull);
        not (RE, REN);
        buf (C, C_buf);
        pmos (C_buf, PAD, 1'b0);

        always @(PAD or RE) begin
          if ((PAD === 1'bz) && RE) begin
            if (lastPAD === 1'b0) begin
              pull = 1;
            end
            else begin
              pull = #(PullTime) 1;
            end
          end
          else begin
            pull = 0;
          end
          lastPAD = PAD;
        end
endmodule
