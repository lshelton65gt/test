// last mod: Mon Apr  9 09:22:23 2007
// filename: test/assign/bug7220_a.v
// Description:  This test has a varied history,
// First it would produce an internal error due to sizing when run with
// -newSizing.  This issue was fixed with v1.5535
// it was also noticed that this test had a simulation mismatch with -oldSizing
// at that time.  The fix for the internal error also fixed the -oldSizing
// issue.  Now this testcase works fine with -newSizing and -oldSizing


module bug7220_a(clk, sel, a, b, x);
  input clk;
  input [3:0] sel;
  input [6:0] a;
  input [2:0] b;
  output [3:0] x;
  reg [3:0]    x;
  always @(posedge clk)
    case(sel[3:2])
      2'b00: x = a[6:3] + (b[2] +|b[1:0]);
      2'b01: x = a[5:2] + (b[2] +|b[1:0]);
      2'b10: x = a[4:1] + (b[2] +|b[1:0]);
      2'b11: x = a[3:0] + (b[2] +|b[1:0]);
    endcase
endmodule
