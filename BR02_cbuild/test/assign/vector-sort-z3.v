// from bug616
module vector ( out1, out2, in );
   input [0:3] in;
   output [0:3] out1;
   output [0:3] out2;

   // these statements cannot be vectorized -- we don't know how to
   // handle dynamic indexing and Z.
   assign out1[0] = in[0] ? 1'b0 : 1'bz;
   assign out2[0] = in[0] ? 1'b0 : 1'bz;
   assign out1[1] = in[1] ? 1'b0 : 1'bz;
   assign out2[1] = in[1] ? 1'b0 : 1'bz;
   assign out1[2] = in[2] ? 1'b0 : 1'bz;
   assign out2[2] = in[2] ? 1'b0 : 1'bz;
   assign out1[3] = in[3] ? 1'b0 : 1'bz;
   assign out2[3] = in[3] ? 1'b0 : 1'bz;

endmodule // vector
