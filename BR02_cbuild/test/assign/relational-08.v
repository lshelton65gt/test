module top (a, b, x);
  input [1:0]  		a;
  input [6:0] 		b;
  output [6:0] 		x;

  assign 		x[1:0] = (a == 4'b1) & b [1:0];
  assign 		x[3:2] = (a == 4'b1) & b [3:2];
  assign 		x[6:4] = (a == 2'b1) & b [6:4];
endmodule // top
