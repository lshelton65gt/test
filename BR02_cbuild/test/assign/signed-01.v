// When a constant term appears in vectorisable
// assignment right-hand sides, the vectorised
// statement will include a constant constructed
// from the pieces. This test is checking that construction
// for that flavour of integer term in the
// vectorisable expressions. 

module top (a, x);
  output [3:0] x;
  input [3:0]  a;
  assign       x [1:0] = a [1:0] & 3;
  assign       x [3:2] = a [3:2] & 6;
endmodule // top

  
