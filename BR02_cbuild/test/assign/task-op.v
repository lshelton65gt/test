module vector ( out, in1, in2 );
   input [0:3] in1;
   input [0:3] in2;
   output [0:3] out;
   reg [0:3] 	out;

   task silly;
      output [0:3] o;
      input [0:3] a, b;
      begin
	 o[0] = a[0] & b[0];
	 o[1] = a[1] & b[1];
	 o[2] = a[2] & b[2];
	 o[3] = a[3] & b[3];
      end
   endtask // silly
   
   always @( in1 or in2 )
     begin
	silly(out, in1, in2);
     end

endmodule // vector
