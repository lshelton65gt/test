module top (clk, a, b, x, y);
  input clk;
  input [3:0] a, b;
  output [3:0] x, y;
  assign x = a & b;
  assign y = a & b;
endmodule // top
