// last mod: Thu Dec 15 14:58:58 2005
// filename: test/assign/bug5458_c.v
// Description:  This test was inspired by the test from bug 5458. 


module tc (fclk, data, dataIn);
   input         fclk;
   input [7:0] 	 dataIn;
   output [7:0]  data;
   reg           hwReset;

   reg [7:0] 	 xmitData;
   reg           i2cNwr;


   assign 	 data = xmitData;

   always @(posedge fclk or posedge hwReset)
     begin
	toptask(hwReset);
     end

   task toptask;
      input hwReset;
      reg [7:0] 	 xmitByte;
      
      begin
	 if (hwReset)
	  begin
	     resetFlop(hwReset, xmitByte);
	  end
	else
	  begin
             writeXmitData(xmitByte);
             xmitByte = xmitByte + 1;
	  end

      end
   endtask

   task writeXmitData;
      input [7:0] byte;
      begin

         xmitData <= dataIn[7:0];
	 i2cNwr <= 1'b0;
      end
   endtask

   task resetFlop;
      input hwReset;
      output [7:0] 	 xmitByte;
//      reg [7:0] 	 xmitByte;

      begin
	 xmitByte <= 0;
	 xmitData <= 0;
      end
   endtask
endmodule
