module top (clk, a, b, x, y);
  input clk;
  input [3:0] a;
  input [7:0] b;
  output [3:0] x, y;
  assign       x = b [3:0];
  assign       y = b [7:3];
endmodule // top
