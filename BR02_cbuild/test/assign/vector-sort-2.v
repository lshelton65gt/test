// from bug616
module vector ( out1, out2, in );
   input [0:1] in;
   output [0:1] out1;
   output [0:1] out2;

   assign out1[0] = in[0];
   assign out2[0] = in[0];
   assign out1[1] = in[1];
   assign out2[1] = in[1];

endmodule // vector
