module dynsize_write(a, b, sel, out);
	input[31:0]     a;
	input[23:0]     b;
	input[2:0]      sel;
	output[31:0]    out;
	assign out [15:8]  = (sel[0] ? b[15:8]  : a[15:8]);
	assign out [7:0]   = (sel[0] ? b[7:0]   : a[7:0]);
endmodule
