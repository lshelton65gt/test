package apackage is
  type intarray2   is array ( 3 downto 0 ) of integer;
  type intarray2x2 is array ( 0  to 3 )    of intarray2;
  constant tbl : intarray2x2 := ( ( 0, 1, 2, 3 ), ( 4, 5, 6, 7 ),
                                  ( 8, 9,10,11 ), (12,13,14,15 ) );
end apackage;

library ieee;
use ieee.std_logic_1164.all;

entity bug5409 is
  port (
    d1 : in std_logic_vector(1 downto 0);
    d2 : in std_logic_vector(1 downto 0);
    tbl_read : out std_logic_vector(32 downto 0)
    );
end bug5409;

library ieee;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.apackage.all;

architecture arch of bug5409 is
begin
  proc: process (d1, d2)
    variable tmp  : integer;
    variable isel : integer range 0 to 3;
    variable jsel : integer range 0 to 3;
    variable itbl : intarray2;
  begin
    isel := conv_integer(d1);
    jsel := conv_integer(d2);
    -- memory-to-memory copy; creates concat which needs transforming.
    itbl := tbl(isel);
    tmp := itbl(jsel);
    tbl_read <= conv_std_logic_vector(tmp,32);
  end process proc;
end arch;
