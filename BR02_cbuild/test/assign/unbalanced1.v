module
top(MEA,ra_CaptureWR,ra_UpdateWR,ra_ShiftWR,TieNet_ra_SelectWIR,ra_en_ret,ra_smart,ra_WRSTN,ra_WSI,ra_WRCK,ra_rst_sms,ra_awt,r2d_aric_ra_in,ra_wrstn_out);
output
MEA,ra_CaptureWR,ra_UpdateWR,ra_ShiftWR,TieNet_ra_SelectWIR,ra_en_ret,ra_smart,ra_WRSTN,ra_WSI,ra_WRCK,ra_rst_sms,ra_awt;
input r2d_aric_ra_in,ra_wrstn_out;

wire [11:0] r2d_aric_ra_in;

assign
{MEA,ra_CaptureWR,ra_UpdateWR,ra_ShiftWR,TieNet_ra_SelectWIR,ra_en_ret,ra_smart,ra_WRSTN,ra_WSI,ra_WRCK,ra_rst_sms,ra_awt} = {r2d_aric_ra_in[11:5],ra_wrstn_out,r2d_aric_ra_in[3:0]};

endmodule

