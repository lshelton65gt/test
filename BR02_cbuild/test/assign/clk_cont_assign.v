// filename: clk_cont_assign.v
// Description:  This test demonstrates a difference between interra and verific
// flow. 



module clk_cont_assign(clock1, in1, out1) ;
   input  clock1;
   input  in1;
   output out1;
   reg 	  out1;
   reg [1:0] state;

   initial
     state = 0;

   always @(posedge clock1)
     begin: clock_blk
	state = state + in1;
     end
// the following block was populated incorrectly
   always @state
     begin: continuous_blk
	case (state)
	  2'b00: out1 = 0;
	  2'b01: out1 = 0;
	  2'b10: out1 = 1;
	  2'b11: out1 = 1;
	  default: out1 = 0;
	endcase 
     end

   
endmodule
