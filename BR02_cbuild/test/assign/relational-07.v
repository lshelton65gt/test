module top (a, b, x);
  input [3:0]  		a;
  input [3:0] 		b;
  output [3:0] 		x;
  assign 		x[1:0] = (a [1:0] == 2'b1) & b [1:0];
  assign 		x[3:2] = (a [3:2] == 2'b1) & b [3:2];
endmodule // top
