`include "fullpart_le.v"

module bv(i,o);
   input [0:127] i;
   output [0:127] o;

   top #(128,65) bvX (i,o);
endmodule
