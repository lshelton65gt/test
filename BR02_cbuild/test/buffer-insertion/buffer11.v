module top (out1, out2, clk, in1, in2);
   output out1, out2;
   input  clk, in1, in2;

   reg    s1, s2;
   reg    out1, out2;
   initial begin
      s1 = 0;
      s2 = 0;
      out1 = 0;
      out2 = 0;
   end
   
   task one;
      begin
         s1 = ~out1;
      end
   endtask // one

   task two;
      begin
         s2 = ~out2;
      end
   endtask // two

   // Make a cycle of two tasks
   always @ (posedge clk)
     begin
        two;
        out1 <= s2 ^ in1;
     end
   always @ (posedge clk)
     begin
        one;
        out2 <= s1 ^ in2;
     end

   
endmodule // top
