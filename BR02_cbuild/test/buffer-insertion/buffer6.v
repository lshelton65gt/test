module top (out1, out2, in1, in2, clk, en1, en2);
   output [3:0] out1, out2;
   input [3:0]  in1, in2;
   input        clk, en1, en2;

   task incraddr;
      output [3:0] addr;
      input [3:0] src;
      begin
         addr = src + 1;
      end
   endtask

   reg [3:0] out1, out2;
   reg [6:0] addr;
   initial begin
      out1 = 0;
      out2 = 0;
      addr = 0;
   end

   always @ (posedge clk)
     begin
        if (en1)
          incraddr(addr[3:0], in1);
        out1 = addr[3:0] + 1;
     end

   always @ (posedge clk)
     begin
        if (en2)
          incraddr(addr[6:3], in2);
        out2 = addr[6:3] + 1;
     end

endmodule
