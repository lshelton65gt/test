module top (out1, out2, out3, in1, in2, in3, sel, clk);
   output [1:0] out1, out2, out3;
   input [1:0]  in1, in2, in3;
   input        sel, clk;

   reg [1:0]    out1;
   initial out1 = 0;
   always @ (posedge clk)
     out1 <= in1;

   sub S1 (out2, out3, in2, in3, sel, clk, clk);

endmodule

module sub (o1, o2, i1, i2, sel, clk, pll_clk);
   output [1:0] o1, o2;
   input [1:0]  i1, i2;
   input        sel, clk, pll_clk;

   wire [1:0]   debug_bus = { i1[0], pll_clk };

   reg [1:0]    o1, o2;
   initial begin
      o1 = 0;
      o2 = 0;
   end
   always @ (posedge clk)
     begin
        t1(o1, i1);
        t2(o2, i2, sel, debug_bus);
     end

   task t1;
      output [1:0] out;
      input [1:0] in;

      t3(out, in);
   endtask

   task t2;
      output [1:0] out;
      input [1:0] in;
      input sel;
      input [1:0] debug_bus;
      if (sel)
        t3(out, in);
      else
        t3(out, debug_bus);
   endtask

   task t3;
      output out;
      input in;

      t4(out, in & debug_bus);
   endtask

   task t4;
      output o;
      input i;

      o = i & sel;
   endtask
   

endmodule
