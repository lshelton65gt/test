// This test duplicates the problem seen at Panasonic (bug 7073) where
// there was an incorrect simulation result that could be corrected by
// dumping waveforms.  The cause of the problem was that there was
// a flop that computes a derived clock, and this flop was such that
// a buffer needed to be inserted in its data input because to fix
// a schedueling conflig. Because the buffer insertion was being done
// after schedules were calculated, the output of the buffer was not
// being marked to force it to be transition scheduled.

// In this example clk3 is the derived clock, it is the result of a
// combo of out1 and out2.  out1 and out2 are setup so that one or the
// other must have a buffer inserted in the data input to allow for
// proper timing (because out1 is driven by a clk1/clk2 chain of
// flops, while out2 is driven by a clk2/clk1 chain of flops).  This
// creates the situation where a buffer is inserted in the data input
// of a derived clock.

module top (out1, out2, out3, clk1, rst1, rst2, d1, d2);
   output out1, out2, out3;
   input  clk1, rst1, rst2, d1, d2;

   reg    r1, r2, out1, out2, out3;

   // Use an inverted clock so that we get data flowing from one
   // schedule call to the next between two DCL flops. But we also
   // have to turn off clock equivalence, otherwise, these would
   // get aliased and the buffer insertion would be smart enough
   // to avoid the problem.
   wire   clk2 = ~clk1;

   initial begin
      r1 = 0;
      r2 = 0;
      out1 = 0;
      out2 = 0;
      out3 = 0;
   end

   always @ (posedge clk1 or negedge rst1)
     if (~rst1)
       r1 <= 1'b0;
     else
       r1 <= d1;
   always @ (posedge clk2 or negedge rst1)
     if (~rst1)
       out1 <= 1'b0;
     else
       out1 <= r1;
   
   always @ (posedge clk2 or negedge rst1)
     if (~rst1)
       r2 <= 1'b0;
     else
       r2 <= d2;
   always @ (posedge clk1 or negedge rst1)
     if (~rst1)
       out2 <= 1'b0;
     else
       out2 <= r2;

   wire   clk3 = out1 ^ out2 ^ r1 ^ r2;
   always @ (posedge clk3)
     out3 <= ~d1;

   
endmodule
