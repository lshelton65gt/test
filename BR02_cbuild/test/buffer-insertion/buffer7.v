module top (out1, out2, out3, in1, in2, in3, sel, clk);
   output [1:0] out1, out2, out3;
   input [1:0]  in1, in2, in3;
   input        sel, clk;

   reg [1:0]    out1;
   initial out1 = 0;
   
   always @ (posedge clk)
     out1 <= in1;

   sub S1 (out2, out3, in2, in3, sel, clk, clk);

endmodule

module sub (o1, o2, i1, i2, sel, clk, pll_clk);
   output [1:0] o1, o2;
   input [1:0]  i1, i2;
   input        sel, clk, pll_clk;

   wire [1:0]   debug_bus;

   assign       debug_bus = { i1[0], pll_clk };

   reg [1:0]    o1, o2;
   initial begin
      o1 = 0;
      o2 = 0;
   end
   always @ (posedge clk)
     begin
        o1 <= i1;
        if (sel)
          o2 <= i2;
        else
          o2 <= debug_bus;
     end

endmodule
