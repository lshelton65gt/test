module top (out1, out2, out3, clk, in1, in2);
   output [3:0] out1, out2, out3;
   input        clk;
   input [3:0]  in1, in2;

   // Make a self cycle derived clock
   reg [3:0]    en;
   wire         gclk;
   initial en = 0;
   always @ (posedge gclk)
     en[1] <= in1[1];
   always @ (in1)
     en[0] = in1[0];
   assign       gclk = clk & (en[0] ^ en[1]);

   // Use the other bits of the enable in normal logic
   reg [3:0]    out1, out2, out3;
   initial begin
      out1 = 0;
      out2 = 0;
      out3 = 4'hf;
   end
   always @ (posedge gclk)
     begin
        en[3:2] = in1[1:0];
        out1 <= en;
        out2 <= in2;
     end

   always @ (en)
     out3 = ~en;
   
endmodule // top
