module top (out1, clk1, clk2, in1, in2);
   output [3:0] out1;
   input        clk1, clk2;
   input        in1, in2;

   reg [3:0]    r1, r2;
   initial begin
      r1 = 0;
      r2 = 0;
   end

   always @ (posedge clk1)
     r1[0] <= ~r2[3];
   always @ (posedge clk1)
     r2[3] <= in1 ^ r1[0];

   always @ (posedge clk2)
     r1[3] <= in2 ^ r2[0];
   always @ (posedge clk2)
     r2[0] <= ~r1[3];

   assign       out1 = r1 ^ r2;

endmodule
