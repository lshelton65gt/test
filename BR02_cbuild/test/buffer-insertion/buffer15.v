// This case makes sure we don't make flow live

module top (out1, out2, out3, clk, in1, in2, in3);
   output out1, out2, out3;
   input  clk, in1, in2, in3;

   sub S1 (out1, out2, clk, in1, in2, in3);
   sub S2 (out3, dummy, clk, in1, in2, in3);

endmodule


module sub (outa, outb, clk, in1, in2, in3);
   output outa, outb;
   input  clk, in1, in2, in3;

   reg [1:0] r1, r2;
   initial begin
      r1 = 0;
      r2 = 0;
   end

   always @ (posedge clk)
     r1[1] <= r2[1] ^ in1;

   always @ (posedge clk)
     r2[1] <= r1[1] ^ in2;

   // dead flop in second instance
   always @ (posedge clk)
     r1[0] <= in3;

   assign outa = r2[1];
   assign outb = r1[0];

endmodule
