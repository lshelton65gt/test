module top (q1, q2, clk, d);
   output q1, q2;
   input  clk, d;

   reg    q1, q2;
   initial begin
      q1 = 0;
      q2 = 0;
   end
   
   always @ (posedge clk)
     q1 <= q2 ^ d;

   always @ (posedge clk)
     copy;

   task copy;
      q2 = q1;
   endtask


endmodule

