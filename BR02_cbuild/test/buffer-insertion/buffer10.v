module top (out1, out2, clk, in1, in2, in3);
   output out1, out2;
   input  clk, in1, in2, in3;

   reg    r1, r2, r3;

   initial begin
      r1 = 0;
      r2 = 0;
      r3 = 0;
   end
   
   always @ (posedge clk)
     begin
        r1 <= in1 ^ out2;
        r2 <= in2;
     end

   // Create some clock logic
   always @ (negedge clk)
     r3 <= in3;
   wire gclk = clk & r3;

   // A case with clock logic flowing in
   sub S1 (out1, gclk, r3, r1, r2);

   // A case with no input flow, but the input flow net has an alias.
   sub S2 (out2, clk, r1, r1, r2);

endmodule // top

module sub (q, clk, sel, d1, d2);
   output q;
   input  clk, sel, d1, d2;
   reg    q;

   initial q = 0;

   always @ (posedge clk)
     if (sel)
       q <= d1;
     else
       q <= d2;

endmodule // sub
