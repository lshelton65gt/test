module top (out, clk, in1, in2);
   output [7:0] out;
   input        clk;
   input [7:0]  in1, in2;

   reg [7:0]    r3;
   reg [7:0]    r2, c1;

   initial begin
      r2 = 0;
      r3 = 0;
      c1 = 0;
   end

   // This block defs the live portion of the net that should get buffered
   always @ (posedge clk)
     begin
        if (~in2[0])
          begin
             c1[3:0] = in1[3:0] ^ in2[3:0];
             r3 <= {~c1[3:0], r2[7:4]};
          end
     end

   // This block uses and defs the net that should get buffered.
   always @ (posedge clk)
     begin
        if (in2[0])
          begin
             c1[7:4] = in1[7:4] ^ in2[7:4];
             r2 <= ~c1;
          end
     end

   assign out = r3 ^ r2;

endmodule

     
