module top (out1, clk, in1, in2);
   output [1:0] out1;
   input        clk;
   input        in1, in2;

   reg [1:0]    r1, r2, r3;
   initial begin
      r1 = 0;
      r2 = 0;
      r3 = 0;
   end

   always @ (posedge clk)
     r1[0] <= ~r2[1] ^ r3[1];
   always @ (posedge clk)
     r2[1] <= in1 ^ r1[0];

   always @ (posedge clk)
     r1[1] <= in2 ^ r2[0] ^ r3[0];
   always @ (posedge clk)
     r2[0] <= ~r1[1];

   always @ (posedge clk)
     r3 <= ~r1;

   assign       out1 = r1 ^ r2 ^ r3;

endmodule
