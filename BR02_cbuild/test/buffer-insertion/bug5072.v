module top(x, z);
input x;
inout z;
                                                                               
                                     
wire [0:4] x;
wire z;
                                                                               
                                     
reg [11:27] a;
reg [1:28] b;
reg [8:31] mem [15:29]; // memory
                                                                               
                                     
sub S0 (, {b[3:24],z,a[15:22],z,a[15:22],z,mem[27],a[15:22],z});
                                                                               
                                     
sub S1 ({a[15:22],z} ,);
                                                                               
                                     
always @(posedge x[0])
  a[15:22] = b[3:24];
                                                                               
                                     
wire clk = &b[3:24];
always @(negedge z or posedge clk)
  if (clk)
    b[3:24] = ~b[3:24];
                                                                               
                                     
endmodule
                                                                               
                                     
module sub(in, unused);
input in;
input unused;
                                                                               
                                     
wire [1:29] in;
wire [31:126] unused;
                                                                               
                                     
wire [5:31] c; // carbon observeSignal
                                                                               
                                     
assign c[5:31] = in[5:22];
                                                                               
                                     
endmodule
