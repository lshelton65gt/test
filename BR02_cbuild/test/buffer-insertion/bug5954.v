// Generated with: ./rangen -s 101 -n 0x94cb79c2 -o /w/uranium/aron/build/clean/obj/Linux.product/test/rangen/rangen/testcases.042006/cbld/assert5.v
// Using seed: 0x94cb79c2
// Writing cyclic circuit of size 101 to file '/w/uranium/aron/build/clean/obj/Linux.product/test/rangen/rangen/testcases.042006/cbld/assert5.v'
// Module count = 12

// module gbt
// size = 17 with 6 ports
module gbt(mA, Sft, OStrI, ezVLX, mrpp, G);
input mA;
output Sft;
input OStrI;
output ezVLX;
input mrpp;
input G;

wire [6:7] mA;
wire [6:7] Sft;
wire [2:22] OStrI;
wire ezVLX;
wire [0:7] mrpp;
wire G;
wire [21:24] U;
reg [5:26] ZF;
reg [0:4] Wq;
reg [2:4] MbXL;
reg [2:2] OjrX;
reg [28:28] uHR;
reg HdI;
reg [0:31] eeGQy;
wire Hvck;
tnqhv uB ({OStrI[22],OStrI[22],G}, {OStrI[22],OStrI[22],G}, {OStrI[7:16],OStrI[7:16],G}, mA[6], {ezVLX,Sft[6:7]}, {G,OStrI[22],OStrI[22]});

always @(posedge G)
begin
begin
OjrX[2] = ~G|OStrI[22];
uHR[28] = mA[6];
end
end

tnqhv fq ({mA[6],OStrI[22],uHR[28]}, {uHR[28],G,mA[6]}, {OjrX[2],OjrX[2],OStrI[7:16],OjrX[2],OjrX[2],G,OStrI[22],OStrI[22],4'b1100}, G, {ezVLX,Hvck,Hvck}, {uHR[28],uHR[28],mA[6]});

YBctF g ({Hvck,Sft[6:7],Hvck,U[23:24],Hvck,Sft[6:7],U[23:24],Sft[6:7]}, OjrX[2], {mrpp[0:6],mA[6],G,OStrI[22],OjrX[2],G,mrpp[0:6],OStrI[7:16],OjrX[2],mrpp[0:6],61'b0110111000101101100000111010011110110110111101000000001101011}, ezVLX, ezVLX);

endmodule

// module tnqhv
// size = 13 with 6 ports
module tnqhv(BYH, R_NMF, gpvm, dXP, WjbhA, o);
input BYH;
input R_NMF;
input gpvm;
input dXP;
output WjbhA;
input o;

wire [27:29] BYH;
wire [3:5] R_NMF;
wire [72:92] gpvm;
wire dXP;
reg [2:4] WjbhA;
wire [0:2] o;
wire [4:26] P;
reg Si;
reg gCh;
reg [3:8] g;
reg sgZu;
wire [3:14] YN;
reg [3:5] nvO;
reg [7:11] LO;
YBctF Gl ({YN[4:5],YN[4:5],P[22:24],YN[4:5],P[22:24]}, dXP, {P[4:26],YN[7:14],P[4:26],YN[9:11],g[6],P[4:26],g[6],o[1:2],gpvm[78:84],7'b0010101}, /* unconnected */, /* unconnected */);

YBctF z ({YN[4:5],YN[4:5],YN[8:12],YN[4:5],YN[4:5]}, dXP, {R_NMF[5],BYH[28:29],R_NMF[5],YN[7:14],gpvm[78:84],g[6],YN[9:11],dXP,YN[7:14],dXP,65'b01010101101011001010000111111010011110101110010110001101000010001}, /* unconnected */, /* unconnected */);

KWXj eayGB ({YN[9:11],g[6],BYH[28:29]}, {P[22:24]}, g[6]);

always @(negedge &YN[9:11])
begin
begin
sgZu = YN[9:11]^g[6];
end
end

Il qet (R_NMF[5], {dXP,sgZu,sgZu,P[12:26],R_NMF[5],dXP,o[1:2],dXP,2'b00});

eY ertW_ (g[6], YN[8:12]);

endmodule

// module YBctF
// size = 46 with 5 ports
module YBctF(UBDfp, jYfTY, X, srvAf, c);
output UBDfp;
input jYfTY;
input X;
inout srvAf;
inout c;

reg [9:21] UBDfp;
wire jYfTY;
wire [29:126] X;
wire [3:3] srvAf;
wire c;
reg [41:107] dPY;
reg jYfte;
reg gu;
reg OfO_;
reg [1:5] IHb;
reg [1:8] E;
wire [20:26] VKFZ;
wire [19:22] eaKoq;
wire [0:15] oXa;
reg [1:4] b [0:5]; // memory
reg [0:3] NP [0:20]; // memory
wire [1:1] QjTH;
reg vT;
reg hMuJ;
reg [7:7] r_F;
reg [21:26] pGiME;
reg [6:31] S;
reg Bec;
reg [1:2] qv;
reg ufCW;
wire [3:37] FlX;
reg [20:29] IXl [5:7]; // memory
wire [83:122] EWf_;
reg [28:31] VTO;
reg [5:13] an;
wire nvXI;
reg [4:6] ygIL [19:24]; // memory
reg xZ;
reg NTdP;
reg [0:2] oq [3:6]; // memory
cc n ({eaKoq[19:21],UBDfp[17:19],E[4:5],eaKoq[19:21],QjTH[1],jYfte,3'b001}, {NTdP,ygIL[19]/* memuse */,E[4:5]});

T _oXDA ({jYfTY,QjTH[1]}, /* unconnected */);

assign QjTH[1] = (dPY[41:48] != 8'b00011100) ? ~X[35:108] : 1'bz;

always @(NTdP)
if (NTdP)
begin
end
else
begin
ufCW = ~(~(FlX[18:34])) + (~vT^QjTH[1]);
end

T tYe ({jYfTY,NTdP}, /* unconnected */);

eY YeT (srvAf[3], VKFZ[22:26]);

always @(&UBDfp[10:12])
if (~UBDfp[10:12])
begin
vT = ygIL[19]/* memuse */;
end
else
begin
end

always @(negedge vT)
begin
begin
OfO_ = dPY[82:94];
end
end

always @(negedge c)
begin
begin
r_F[7] = ~(~(vT) + ((~dPY[41:48])));
VTO[28:31] = ~FlX[18:34];
end
end

cc xKio (dPY[52:67], {ygIL[22]/* memuse */,jYfte,NTdP,srvAf[3]});

assign EWf_[111:119] = ~UBDfp[10:12];

always @(&dPY[81:85])
if (~dPY[81:85])
begin
hMuJ = jYfte|srvAf[3];
end
else
begin
NP[7]/* memuse */ = ~dPY[82:94]^UBDfp[10:12];
end

always @(posedge &FlX[18:34])
begin
begin
UBDfp[12:13] = ~(oq[3]/* memuse */^dPY[81:85]);
end
end

always @(posedge &dPY[75:107])
begin :LzT
reg [6:9] ryd;
reg [11:23] vFG;

begin
oq[4]/* memuse */ = ~ygIL[19]/* memuse */;
end
end

always @(&FlX[18:34])
if (~FlX[18:34])
begin
E[4:8] = ~UBDfp[12:13]|c;
end
else
begin
S[24:27] = ygIL[19]/* memuse */;
end

assign c = ~eaKoq[19:21]^oq[4]/* memuse */;

always @(negedge &oq[4]/* memuse */)
begin
begin
UBDfp[10:19] = ~dPY[64:85];
end
end

always @(negedge ufCW)
begin
begin
VTO[30:31] = (vT);
dPY[54:81] = UBDfp[10:12]&r_F[7];
end
end

always @(posedge &UBDfp[10:19])
begin
begin
end
end

always @(negedge &VTO[29:31])
begin
begin
Bec = QjTH[1];
UBDfp[12:13] = (VTO[29:31]);
end
end

always @(&NP[13]/* memuse */)
if (~NP[13]/* memuse */)
begin
UBDfp[10:19] = (3'b010);
end
else
begin
hMuJ = ~dPY[73:104];
end

assign eaKoq[20:21] = IXl[7]/* memuse */;

cc tHS ({dPY[67:79],srvAf[3],srvAf[3],jYfte}, {oq[4]/* memuse */,jYfte,vT,vT});

RHfPC saXM ({ygIL[22]/* memuse */,NP[7]/* memuse */,UBDfp[10:12],E[4:8]}, vT);

RHfPC rx ({srvAf[3],UBDfp[10:19],UBDfp[10:12],1'b0}, jYfTY);

assign VKFZ[22:26] = (VTO[28:31]);

always @(negedge vT)
begin
begin
VTO[30:31] = ~VTO[30:31]^jYfte;
end
end

always @(posedge &E[4:5])
begin
begin
for (VTO[30:31]=2'b01; VTO[30:31]>2'b10; VTO[30:31]=VTO[30:31]+2'b11)
begin
IXl[7]/* memuse */ = (UBDfp[10:19]^oq[4]/* memuse */) + ((X[35:108]));
end
end
end

always @(&dPY[43:83])
if (dPY[43:83])
begin
end
else
begin
dPY[51:83] = ~NTdP;
end

NGzcNe BpCFJ (hMuJ, {srvAf[3],EWf_[111:119],nvXI,srvAf[3],c,nvXI,eaKoq[20:21],c,FlX[4:5],nvXI}, /* unconnected */, ufCW);

always @(posedge &VTO[28:31])
begin :rVBpX
reg [6:7] UsXXs;
reg Zft;
reg [3:4] EmStb;
reg [16:19] FbmH;
reg [2:5] K;

begin
FbmH[16:18] = 4'b1110;
end
end

always @(negedge vT)
begin
begin
E[6] = 9'b110001111;
end
end

wire F = c;
always @(negedge &VTO[30:31] or negedge F)
if (~F)
begin
begin
S[18:24] = (3'b100);
end
end
else
begin
begin
UBDfp[10:19] = ~VTO[29:31];
end
end

always @(negedge &dPY[79:91])
begin :H
reg [1:1] O;
reg [37:55] Ee;
reg Jumf;

begin
ygIL[23]/* memuse */ = ~OfO_&dPY[52:67];
end
end

always @(negedge c)
begin
begin
NP[7]/* memuse */ = ~oq[3]/* memuse */;
r_F[7] = ~UBDfp[10:12]^r_F[7];
end
end

endmodule

// module Il
// size = 0 with 2 ports
module Il(u, qmBd);
input u;
input qmBd;

wire [3:3] u;
wire [2:26] qmBd;
reg KYSdC;
reg az;
reg [15:22] v;
reg [5:25] VS;
wire [2:70] tGQp;
endmodule

// module RHfPC
// size = 5 with 2 ports
module RHfPC(DmE, vKkIq);
input DmE;
input vKkIq;

wire [16:30] DmE;
wire vKkIq;
wire J;
wire DSpjG;
reg PjBe;
reg [0:29] FQ;
wire H;
assign J = 5'b01100;

assign J = ~DmE[22:27]^DmE[16:26];

assign J = (~(~DmE[22:27]));

always @(DmE[27])
if (~DmE[27])
begin
PjBe = ~DmE[27];
end

always @(DSpjG)
if (DSpjG)
begin
PjBe = 4'b1000;
end

endmodule

// module eY
// size = 3 with 2 ports
module eY(vUpWl, KK);
input vUpWl;
output KK;

wire [3:3] vUpWl;
wire [3:7] KK;
reg e;
wire RhK;
assign RhK = ~6'b010010;

assign KK[5:6] = ~vUpWl[3]^RhK;

assign KK[5:6] = (((vUpWl[3])));

endmodule

// module cc
// size = 2 with 2 ports
module cc(q, uUC);
input q;
input uUC;

wire [0:15] q;
wire [0:5] uUC;
reg [18:28] Qtmy;
reg [0:0] eVyPn [16:100]; // memory
wire h;
always @(posedge &uUC[0:1])
begin
begin
eVyPn[56]/* memuse */ = ~q[7:14];
eVyPn[54]/* memuse */ = ~(6'b111001);
Qtmy[21:28] = ~(eVyPn[56]/* memuse */);
end
end

endmodule

// module KWXj
// size = 5 with 3 ports
module KWXj(sdSb, r, WRDmx);
input sdSb;
output r;
input WRDmx;

wire [1:6] sdSb;
reg [3:6] r;
wire WRDmx;
reg [3:7] g;
reg MAV;
reg [3:25] qdC;
reg [1:7] YJX;
reg rT;
reg [4:5] ai;
reg [2:3] ujMm;
wire [16:31] STVf;
always @(negedge &r[3:5])
begin
begin
YJX[1:7] = WRDmx;
end
end

always @(posedge &STVf[22:30])
begin :LPWl
reg [7:63] azbpZ [25:29]; // memory
reg [1:3] Ghp;
reg [30:64] sHDt;
reg OAg;

begin
Ghp[1:2] = STVf[17:31];
end
end

always @(&r[3:5])
if (r[3:5])
begin
g[4:6] = YJX[5:6];
end

assign STVf[18:26] = ~r[3:5]^g[4:6];

always @(&sdSb[4:5])
if (sdSb[4:5])
begin
end

assign STVf[18:26] = ~YJX[1:7];

endmodule

// module NGzcNe
// size = 4 with 4 ports
module NGzcNe(BTS, qd, x, l);
input BTS;
output qd;
inout x;
input l;

wire BTS;
reg [7:26] qd;
wire [102:112] x;
wire l;
wire ivrv;
reg [2:3] Wo;
assign ivrv = Wo[2];

assign ivrv = ~{x[102:105],l};

assign ivrv = 5'b01101;

assign x[105:107] = 10'b0110100010;

endmodule

// module T
// size = 4 with 2 ports
module T(NN, nekF);
input NN;
inout nekF;

wire [0:1] NN;
wire [9:15] nekF;
reg HNZ;
wire [5:22] YLLqA;
assign YLLqA[12:16] = ~(nekF[9:10]);

always @(posedge &nekF[9:10])
begin :OjW
reg CTk;
reg [14:29] ZcFM;
reg [98:126] EdUzE [1:1]; // memory
reg tou;
reg [8:27] y;

begin
tou = ~HNZ|nekF[12:13];
end
end

assign YLLqA[12:16] = nekF[9:10]&NN[0:1];

assign YLLqA[12:16] = (~nekF[12:13]);

endmodule
