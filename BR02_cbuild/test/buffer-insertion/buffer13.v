module top (out1, out2, clk, d, irst);
   output out1, out2;
   input  clk, d, irst;

   wire   rstinv;
   assign rstinv = !irst;

   reg    rst1, rst2, out1, out2;
   initial begin
      rst1 = 0;
      rst2 = 0;
      out1 = 0;
      out2 = 0;
   end
   
   always @ (negedge clk)
     begin
        rst1 <= rstinv;
        rst2 <= rst1;
        out1 <= rst2;
     end

   wire rst;
   assign rst = rst2 & rst1;

   always @ (posedge clk or negedge rst)
     if (~rst)
       out2 <= 1'b0;
     else
       out2 <= d;

endmodule

