`define PARAM_N 8
`define PARAM_M 4
`define PARAM_Q 2

module vector_mux 
  (
  operand ,
  sel     ,

  result
  );

  parameter N = `PARAM_N;
  parameter M = `PARAM_M;
  parameter Q = `PARAM_Q;
  parameter W = N*(1<<Q);

  input [N*M-1:0] operand;
  input [  Q-1:0] sel;

  output [N-1:0] result;

  reg [         N-1:0] mux_word;
  reg [(N*(1<<Q))-1:0] padded_operand;
  reg [         N-1:0] result;

  reg [N-1:0] mux_table [0:M-1];

  integer i;
  integer j;

  always @(operand or sel)
  begin
    padded_operand          = {{W{1'b0}}};

    padded_operand[M*N-1:0] = operand;

    for (j=0; j<M; j=j+1)
    begin

      for (i=0; i<N; i=i+1)
        mux_word[i] = padded_operand[j*N+i];

      mux_table[j] = mux_word;
    
    end

    result = mux_table [sel];

  end

endmodule

