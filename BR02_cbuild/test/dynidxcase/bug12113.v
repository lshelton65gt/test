// reproducer for bug 12113 - DynIdxToCase conversion was xforming
// dynamic indexes where the dynamic index variable was a memory net,
// and creating illegal tree, causing an assert.
module top(b, a_0, a_1, clk, clk_0, clk_1, q_0, q_1);
   input clk, clk_0, clk_1;
   input [1:0] a_0, a_1;
   output      q_0, q_1;
   reg         q_0, q_1;
   input [1:0] b;

   reg [1:0]   m [1:0];
   
   always @(posedge clk)
     begin
        q_0 = b[m[0]];
        q_1 = b[m[1]];
     end

   always @(posedge clk_0)
     begin
        m[0] = a_0;
     end

   always @(posedge clk_1)
     begin
        m[1] = a_1;
     end
endmodule
