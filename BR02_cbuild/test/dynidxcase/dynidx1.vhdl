library IEEE;
   use IEEE.std_logic_1164.all;
   use IEEE.std_logic_arith.all;

      entity dynidx1 is
      port (
         mfeclkbuf            : in     std_logic;
         mfeclkbuf2            : in     std_logic;
         mferstN              : in     std_logic;
      encoderout             : in integer range 0 to 7-1;
      fliporder              : in std_logic;
         arbsel_ch            : in     std_logic_vector(1 downto 0);
               o : out std_logic
      );
   end dynidx1;

architecture functional of dynidx1 is
      constant MAXNBRMUXIN          : integer := 7;
      signal encoderin2order        : integer range 0 to MAXNBRMUXIN-1;
      signal encoderin3order        : integer range 0 to MAXNBRMUXIN-1;
      signal encoderin4order        : integer range 0 to MAXNBRMUXIN-1;
      signal encoderin5order        : integer range 0 to MAXNBRMUXIN-1;
      signal encoderin6order        : integer range 0 to MAXNBRMUXIN-1;
      type REQID     is (DEM, VIN, VM0WR, VM0RD, VM1, VM2, VMRBC);
      type ENCMUXSELTYPE is array (0 to MAXNBRMUXIN-1) of REQID;
      signal encodermuxsel          : ENCMUXSELTYPE;

      signal muxinit : ENCMUXSELTYPE;   -- carbon observeSignal
begin

  outassign:
    process (mfeclkbuf)
      begin
        if (mfeclkbuf'event and mfeclkbuf = '1') then
          if (encodermuxsel(6) = VIN) then
            o <= '1';
            else
              o <= '0';
          end if;
        end if;
      end process outassign;
  
   arbmuxsel : 
      process (arbsel_ch, encoderin2order, encoderin3order, encoderin4order, encoderin5order, encoderin6order)
--         variable muxinit : ENCMUXSELTYPE;
      begin
--         muxinit := (DEM, VIN, VM0WR, VM0RD, VM1, VM2, VMRBC);  -- Default
         muxinit <= (DEM, VIN, VM0WR, VM0RD, VM1, VM2, VMRBC);  -- Default
         encodermuxsel <= muxinit;  -- Default

         case arbsel_ch is
            when "00"     =>
              encodermuxsel(0) <= muxinit(0);
                             encodermuxsel(1) <= muxinit(1);
                             encodermuxsel(2) <= muxinit(encoderin2order);
                             encodermuxsel(3) <= muxinit(encoderin3order);
                             encodermuxsel(4) <= muxinit(encoderin4order);
                             encodermuxsel(5) <= muxinit(encoderin5order);
                             encodermuxsel(6) <= muxinit(encoderin6order);
            when "01"     =>
              encodermuxsel(0) <= muxinit(1);
                             encodermuxsel(1) <= muxinit(0);
                             encodermuxsel(2) <= muxinit(encoderin2order);
                             encodermuxsel(3) <= muxinit(encoderin3order);
                             encodermuxsel(4) <= muxinit(encoderin4order);
                             encodermuxsel(5) <= muxinit(encoderin5order);
                             encodermuxsel(6) <= muxinit(encoderin6order);
            when "10"     =>
              encodermuxsel(0) <= muxinit(0);
                             encodermuxsel(1) <= muxinit(1);
                             encodermuxsel(2) <= muxinit(2);
                             encodermuxsel(3) <= muxinit(3);
                             encodermuxsel(4) <= muxinit(4);
                             encodermuxsel(5) <= muxinit(5);
                             encodermuxsel(6) <= muxinit(6);
            when "11"     =>
              encodermuxsel(0) <= muxinit(1);
                             encodermuxsel(1) <= muxinit(0);
                             encodermuxsel(2) <= muxinit(2);
                             encodermuxsel(3) <= muxinit(3);
                             encodermuxsel(4) <= muxinit(4);
                             encodermuxsel(5) <= muxinit(5);
                             encodermuxsel(6) <= muxinit(6);
            when others   => null;
         end case;
      end process arbmuxsel;
   arbmuxreg2 : 
      process (mferstN, mfeclkbuf2)
      begin
         if (mferstN = '0') then
            encoderin2order <= 2;
            encoderin3order <= 3;
            encoderin4order <= 4;
            encoderin5order <= 5;
            encoderin6order <= 6;
         else
            if (mfeclkbuf2'event and mfeclkbuf2 = '1') then
               if (fliporder = '1') then
                  if (encoderout = 2) then  -- 000, 001, 100
                     encoderin2order <= encoderin3order;
                     encoderin3order <= encoderin4order;
                     encoderin4order <= encoderin5order;
                     encoderin5order <= encoderin6order;
                     encoderin6order <= encoderin2order;
                  elsif (encoderout = 3) then
                     encoderin3order <= encoderin4order;
                     encoderin4order <= encoderin5order;
                     encoderin5order <= encoderin6order;
                     encoderin6order <= encoderin3order;
                  elsif (encoderout = 4) then
                     encoderin4order <= encoderin5order;
                     encoderin5order <= encoderin6order;
                     encoderin6order <= encoderin4order;
                  elsif (encoderout = 5) then
                     encoderin5order <= encoderin6order;
                     encoderin6order <= encoderin5order;
                  end if;
               end if;
            end if;
         end if;
      end process arbmuxreg2;
end functional;
