import Carbon.tools

hint = '''\
    If you have added a new python file to Carbon/tools, it should define
    methods name(), brief(), and run(). In addition you must list this python
    command in one of the two lists described below for executables in
    CARBON_HOME/bin.

    See src/python/Carbon/tools/version.py for a simple example.


    If you have added new executable to CARBON_HOME/bin, then you need to
    edit src/python/Carbon/tools/__init__.py in one of two places:

    1) If your executable is something that users will be running from
       the command line, you should add a brief description of it to
       class _BinTool, member _descriptions, at the top of the file.

    2) If your executable is not run directly by users, then add it to
       the 'hiddenNames' list.  For examples, see carbon_arch, carbon_exec.
'''

for tool in Carbon.tools.tools():
  try:
    name = tool.name()
  except:
    print 'Error getting name for tool'
    print hint
    name = '*** unknown ***'
  print 'tool:', name

  try:
    brief = tool.brief()
    if len(brief.strip()) == 0:
      raise
  except:
    print 'Error getting brief description for tool', name
    print hint

  names = dir(tool)
  if not 'run' in names:
    print 'No run method for tool', name
    print hint
