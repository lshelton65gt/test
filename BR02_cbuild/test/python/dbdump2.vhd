-- declare different vhdl types to test carbon dbdump.py and
-- the Python API.

library ieee;
use ieee.std_logic_1164.all;

entity declarations is
  port (
    out1 : out real;
    out2 : out std_logic;
    out3 : out std_logic_vector(31 downto 0);
    out4 : out integer;
    clk  : in std_logic;
    in1  : in real;
    in2  : in std_logic;
    in3  : in std_logic_vector(31 downto 0);
    in4  : in integer
    );
end;

architecture top of declarations is
begin
  process (clk)
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge
      out3 <= in3;
    end if;
  end process;

  out1 <= in1 * 17.0;

  out2 <= not in2;

  out4 <= in4 + 1;
end top;
