// This test creates a bunch of different Verilog declaration
// types and sizes to test that part of the Carbon Python API.

module top(out1, out2, out3, out4, out5, clk, in1);
   output [63:0] out1;
   output [31:0]  out2, out3, out5;
   output         out4;
   input          clk;
   input          in1;

   time           time1 = 1;
   integer        int1 = 12;
   real           float1 = 34.5;
   reg            out4;
   
   always @ (posedge clk)
     begin
        out4 <= in1;
        time1 = $time;
        int1 = int1 + 1;
        float1 = float1 * 17;
     end

   assign out1 = time1;
   assign out2 = int1;
   assign out3 = float1;
   assign out5 = out3 + in1;

endmodule


   
