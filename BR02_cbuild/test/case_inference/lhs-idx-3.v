module test (clk, sel, out);
  input clk;
  input [1:0] sel;
  output [10:2] out;
  reg [10:2]   out;
  always @(clk)
    case (sel)
      // lsb = index * 2 + 2
      0: out [3:2] = 2'b00;
      1: out [5:4] = 2'b01;
      2: out [7:6] = 2'b10;
      3: out [10:8] = 2'b11;
    endcase // case(sel)
endmodule // test
