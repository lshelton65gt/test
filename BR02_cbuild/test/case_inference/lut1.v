module top(i, o);
   input [1:0] i;
   output [4:0] o;
   reg [4:0]    o;

   always @(i)
     begin
        case (i)
          2'b00: o = 4'b0001;
          2'b01: o = 4'b0010;
          2'b10: o = 4'b0100;
          2'b11: o = 4'b1000;
        endcase
     end
endmodule
