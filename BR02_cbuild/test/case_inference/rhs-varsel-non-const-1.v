module test(in, sel, idx, out);
  input sel;
  input [3:0] idx;
  input [31:0] in;
  output      out;
  reg 	      out;
  always @(*) begin
    case (sel)
      1'b0: out = in [idx + 0];
      1'b1: out = in [idx + 1];
    endcase
  end
endmodule
