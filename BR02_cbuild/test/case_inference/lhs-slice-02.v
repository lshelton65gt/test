module top (clk, sel, in, out);
  input clk;
  input [1:0] sel;
  input [31:0] in;
  output [0:31] out;
  reg [0:31] 	out;
  always @(posedge clk) begin
    out [0] = 1'b0;
    out [16:31] = 16'b0;
    case (sel)
      2'b00: out [1:15] = in [14:0];
      2'b01: out [1:15] = in [15:1];
      2'b10: out [1:15] = in [16:2];
      2'b11: out [1:15] = in [17:3];
    endcase
  end
endmodule
