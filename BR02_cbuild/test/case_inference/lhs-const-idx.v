module vector ( out, sel, in1, in2);
   input [3:0] in1;
   input [6:0] in2;
   input [1:0] sel;
   output      out;
   reg [1:0]   rsp;
   always @( sel or in1 or in2)
     begin
	case (sel)
	  2'b0:
	    rsp[1] = in1[0]&in2[1]&in2[3];
	  2'b1:
	    rsp[1] = in1[1]&in2[2]&in2[4];
	  2'h2:
	    rsp[1] = in1[2]&in2[3]&in2[5];
	  2'h3:
	    rsp[1] = in1[3]&in2[4]&in2[6];
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = rsp[1];
endmodule // vector
