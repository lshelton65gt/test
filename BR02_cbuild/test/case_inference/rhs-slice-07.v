module top (clk, sel, in, out);
  input clk;
  input [1:0] sel;
  input [31:0] in;
  output [3:0] out;
  reg [3:0] 	out;
  always @(posedge clk) begin
    case (sel)
      2'b00: out = in [3:0];
      2'b01: out = in [6:3];
      2'b10: out = in [9:6];
      2'b11: out = in [12:9];
    endcase
  end
endmodule
