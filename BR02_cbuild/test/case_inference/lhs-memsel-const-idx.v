module vecotr ( out, sel );
   input [1:0] sel;
   output [31:0] out;
   reg [31:0] 	rsp[3:0];
   always @( sel )
     begin
	case (sel)
	  2'b0:
	    {rsp[0]} = 10;
	  2'b1:
	    {rsp[0]} = 11;
	  2'h2:
	    {rsp[0]} = 12;
	  2'h3:
	    {rsp[0]} = 13;
	endcase // case(sel)
     end // always @ ( sel)

   assign out = rsp[0];
endmodule // vector
