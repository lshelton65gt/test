module vector ( out, sel, in1, in2);
   input [3:0] in1;
   input [6:0] in2;
   input [0:1] sel;
   output [3:0] out;
   reg [3:0] 	rsp;
   always @( sel or in1 or in2 )
     begin
	rsp = 0;
	case (sel)
	  2'b0:
	    rsp[0] = in1[0]&in2[1]&in2[3];
	  2'b1:
	    rsp[1] = in1[1]&in2[2]&in2[4];
	  2'h2:
	    rsp[2] = in1[2]&in2[3]&in2[5];
	  2'h3:
	    rsp[3] = in1[3]&in2[4]&in2[6];
	endcase // case(sel)
     end // always @ ( sel or in1 or in2)
   assign out = rsp;
endmodule // vector
