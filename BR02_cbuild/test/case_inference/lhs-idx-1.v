module test (clk, sel, out);
  input clk;
  input [1:0] sel;
  output [7:0] out;
  reg [7:0]   out;
  always @(clk)
    case (sel)
      // lsb = index * 2
      0: out [1:0] = 2'b00;
      1: out [3:2] = 2'b01;
      2: out [5:4] = 2'b10;
      3: out [7:6] = 2'b11;
    endcase // case(sel)
endmodule // test
