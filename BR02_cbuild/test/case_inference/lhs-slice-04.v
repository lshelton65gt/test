module top (clk, sel, in, out);
  input clk;
  input [1:0] sel;
  input [31:0] in;
  output [31:0] out;
  reg [31:0] 	out;
  always @(posedge clk) begin
    out [0] = 1'b0;
    out [31:19] = 13'b0;
    case (sel)
      2'b00: out [15:1] = in [14:0];
      2'b01: out [16:2] = in [15:1];
      2'b10: out [17:3] = in [16:2];
      2'b11: out [18:4] = in [17:3];
    endcase
  end
endmodule
