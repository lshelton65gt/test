module vecotr ( out, sel, in1, in2 );
   input [3:0] in1;
   input [3:0] in2;
   input [1:0] sel;
   output [1:0] out;
   reg 	       rsp1;
   reg 	       rsp2;
   always @( sel or in1 )
     begin
	case (sel)
	  2'b0:
	    {rsp1,rsp2} = in1;
	  2'b1:
	    {rsp1,rsp2} = in1;
	  2'h2:
	    {rsp1,rsp2} = in1;
	  2'h3:
	    {rsp1,rsp2} = in1;
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = {rsp1,rsp2};
endmodule // vector
