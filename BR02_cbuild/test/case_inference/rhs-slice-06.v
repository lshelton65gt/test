module top (clk, sel, in, out);
  input clk;
  input [1:0] sel;
  input [0:31] in;
  output [14:0] out;
  reg [14:0] 	out;
  always @(posedge clk) begin
    case (sel)
      2'b00: out = in [0:14];
      2'b01: out = in [1:15];
      2'b10: out = in [2:16];
      2'b11: out = in [3:17];
    endcase
  end
endmodule
