module vecotr ( arbSt, TrhffMet, loc_PadGnt5_L, BusArbClk, HighestReq, currentReq_CN, NewWinner, extReq_C, heedNewWinner_C, out);
   input 	BusArbClk;
   input [3:0] 	HighestReq;
   input 	currentReq_CN;
   input 	NewWinner;
   input 	extReq_C;
   input	TrhffMet;
   input	loc_PadGnt5_L;
   input 	heedNewWinner_C;
   input [2:0] 	arbSt;
   reg [2:0] 	arbSt_P;
   reg 		parkGnt5_C;
   output [2:0] out;

   always @(arbSt or BusArbClk or HighestReq or currentReq_CN or NewWinner
	    or extReq_C or HighestReq or TrhffMet or loc_PadGnt5_L or 
	    heedNewWinner_C) begin
      parkGnt5_C = ((~loc_PadGnt5_L) & ((HighestReq == 4'b1111) | 
					(HighestReq == 4'b0101)));
      case (arbSt)
	3'b0:
	  case (((BusArbClk & (HighestReq != 4'b1111)) & TrhffMet))
	    1'b0:
	      arbSt_P = 3'b0;
	    1'b1:
	      arbSt_P = 3'b1;
	      endcase
	3'b1: begin
	   case ((((~currentReq_CN) & (~NewWinner)) | parkGnt5_C))
	     1'b0:
	       arbSt_P = 3'b0;
	     1'b1:
	       arbSt_P = 3'b1;
	   endcase
	end
	3'b010:
	  case (((NewWinner & extReq_C) & heedNewWinner_C))
	    1'b0:
	      arbSt_P = 3'b010;
	    1'b1:
	      arbSt_P = 3'b011;
	  endcase
	default:
	  arbSt_P = 3'b011;
      endcase
   end
   assign out = arbSt_P;
endmodule
