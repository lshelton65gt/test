module vector ( out, sel, in1, in2);
   input [7:1] in1;
   input [6:0] in2;
   input [0:1] sel;
   output   out;
   reg 	    rsp;
   always @( sel or in1 or in2)
     begin
	case (sel)
	  2'b0:
	    rsp = in1[1];
	  2'b1:
	    rsp = in1[3];
	  2'h2:
	    rsp = in1[5];
	  2'h3:
	    rsp = in1[7];
	endcase // case(sel)
     end // always @ ( sel or in1 or in2)
   assign out = rsp;
endmodule // vector
