module vector ( out, sel, in1);
   input [3:0] in1;
   input [1:0] sel;
   output [3:0]  out;
   reg 	[3:0]	rsp;
   always @( sel or in1 )
     begin
	case (sel)
	  2'b0:
	    rsp = in1[0]+1;
	  2'b1:
	    rsp = in1[1]+2;
	  2'h2:
	    rsp = in1[2]+3;
	  2'h3:
	    rsp = in1[3]+4;
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = rsp;
endmodule // vector
