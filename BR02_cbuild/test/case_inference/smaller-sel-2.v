module top (sel, out);
  input [2:0] sel;
  output [8:0] out;
  reg [8:0] out;
  always @(*)
    case (sel)
      // selector is three bits, expression is nine bit.
      3'b000: out = 9'sd7;
      3'b001: out = (9'h100);
      3'b010: out = (9'h101);
      3'b011: out = (9'h102);
      3'b100: out = (9'h103);
      3'b101: out = (9'h104);
      3'b110: out = (9'h105);
      3'b111: out = (9'h106);
    endcase
endmodule
