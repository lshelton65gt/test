module test (clk, sel, out);
  input clk;
  input [1:0] sel;
  reg [7:0] mem [0:-127];
  output [7:0] out;
  reg [7:0] out;
  initial mem [-1] = 8'b01010101;
  always @(clk) begin
    case (sel)
      2'b00: out = mem [-1];
      2'b01: out = mem [-1];
      2'b10: out = mem [-1];
      2'b11: out = mem [-1];
    endcase
  end
endmodule
