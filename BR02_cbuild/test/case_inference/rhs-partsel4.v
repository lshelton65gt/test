module top (sel,addr,next);
  input             sel;
  input [15:0]      addr;
  output [16:2]      next;
  reg [16:2] 	     next;
  always @(sel)
    begin
      case (sel)
        1'b0:    next[16:2] = addr[15:2];
        1'b1:    next[16:2] = addr[15:2];
      endcase 
    end
endmodule
