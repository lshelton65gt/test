`timescale 1ns / 10 ps
module uv_alu_2cmp( nfixo, nfix );
   input [27:0] nfix;
   output [27:0] nfixo;
   wire 	 test_zero_2_;
   wire 	 const_3_0x1_;
   reg [27:0] 	 nfixo_5_;
   reg 		 test_zero;

   // const_3_0x1 prevents case inferencing from happening the first time, 
   // after constant prop (and Elaboration), the case inferencing would 
   // occur, but the created temp would not be elaborated correctly
   // (NUNet::lookupElab complains.) So, case inference is disabled after
   // Elaboration in schedule.
   assign 	 const_3_0x1_= 1024'h1;
   assign 	 test_zero_2_= (nfix[27:0] == (29'b0<<0));

   always @ (test_zero_2_  or const_3_0x1_ ) begin
      case (test_zero_2_)
	1'd1: test_zero = (const_3_0x1_);
	1'd0: test_zero = 1024'h0;
      endcase
   end

   always @ (test_zero) begin
      case (test_zero)
	1'd1: nfixo_5_ = 1024'h0;
	1'd0: nfixo_5_ = 1024'h1;
      endcase
   end

   assign nfixo = nfixo_5_[27:0];

endmodule
