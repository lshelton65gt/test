module top (clk, sel, in, out);
  input clk;
  input [1:0] sel;
  input [31:0] in;
  output [14:0] out;
  reg [14:0] 	out;
  always @(posedge clk) begin
    case (sel)
      2'b00: out = in [14:0];
      2'b01: out = in [15:1];
      2'b10: out = in [16:2];
      2'b11: out = in [17:3];
    endcase
  end
endmodule
