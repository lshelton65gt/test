module vecotr ( out, sel );
   input [1:0] sel;
   output [31:0] out;
   reg [31:0] 	rsp[3:0];
   integer 	i;
   initial
     begin
	for (i = 0; i < 4; i=i+1)
	  rsp[i] = 0;
     end
   always @( sel )
     begin
	case (sel)
	  2'b0:
	    rsp[0] = 10;
	  2'b1:
	    rsp[1] = 11;
	  2'h2:
	    rsp[2] = 12;
	  2'h3:
	    rsp[3] = 13;
	endcase // case(sel)
     end // always @ ( sel)

   assign out = rsp[0] || rsp[1] || rsp[2] || rsp[3];
endmodule // vector
