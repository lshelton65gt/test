module top (clk, sel, in, out);
  input clk;
  input [1:0] sel;
  input [0:31] in;
  output [3:0] out;
  reg [3:0] 	out;
  always @(posedge clk) begin
    case (sel)
      2'b00: out = in [2:5];
      2'b01: out = in [5:8];
      2'b10: out = in [8:11];
      2'b11: out = in [11:14];
    endcase
  end
endmodule
