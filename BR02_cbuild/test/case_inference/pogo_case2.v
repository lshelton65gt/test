module vecotr ( loc_PadGnt5_L, HighestReq, currentReq_CN, NewWinner, out);
   input [3:0] 	HighestReq;
   input 	currentReq_CN;
   input	loc_PadGnt5_L;
   input 	NewWinner;
   reg [2:0] 	arbSt_P;
   reg 		parkGnt5_C;
   output [2:0] out;

   always @(HighestReq or currentReq_CN or NewWinner
	    or HighestReq or loc_PadGnt5_L)
     begin
      parkGnt5_C = ((~loc_PadGnt5_L) & ((HighestReq == 4'b1111) | 
					(HighestReq == 4'b0101)));
      case ((((~currentReq_CN) & (~NewWinner)) | parkGnt5_C))
	     1'b0:
	       arbSt_P = 3'b0;
	     1'b1:
	       arbSt_P = 3'b1;
      endcase
     end
   assign out = arbSt_P;
endmodule
