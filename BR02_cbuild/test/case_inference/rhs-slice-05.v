module top (clk, sel, in, out);
  input clk;
  input [1:0] sel;
  input [0:31] in;
  output [0:14] out;
  reg [0:14] 	out;
  always @(posedge clk) begin
    case (sel)
      2'b00: out = in [14:28];
      2'b01: out = in [15:29];
      2'b10: out = in [16:30];
      2'b11: out = in [17:31];
    endcase
  end
endmodule
