module vecotr ( out, sel, in1, in2 );
   input [4:0] in1;
   input [3:0] in2;
   input [1:0] sel;
   output [2:0] out;
   reg [2:0] 	rsp1;
   always @( sel or in1 or in2)
     begin
	case (sel)
	  2'b0:
	    rsp1 = {in1[0],in2[0],in1[1]};
	  2'b1:
	    rsp1 = {in1[1],in2[1],in1[2]};
	  2'h2:
	    rsp1 = {in1[2],in2[2],in1[3]};
	  2'h3:
	    rsp1 = {in1[3],in2[3],in1[4]};
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = rsp1;
endmodule // vector
