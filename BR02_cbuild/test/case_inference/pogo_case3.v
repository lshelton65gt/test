module vecotr ( arbSt, TrhffMet, loc_PadGnt5_L, BusArbClk, HighestReq, currentReq_CN, NewWinner, extReq_C, heedNewWinner_C, out);
   input 	BusArbClk;
   input [3:0] 	HighestReq;
   input 	currentReq_CN;
   input 	NewWinner;
   input 	extReq_C;
   input	TrhffMet;
   input	loc_PadGnt5_L;
   input 	heedNewWinner_C;
   input [2:0] 	arbSt;
   reg [2:0] 	arbSt_P;
   reg 		parkGnt5_C;
   output [2:0] out;

   always @(currentReq_CN or NewWinner or extReq_C or heedNewWinner_C) 
     begin
      case (((NewWinner & extReq_C) & heedNewWinner_C))
	1'b0:
	  arbSt_P = 3'b010;
	1'b1:
	  arbSt_P = 3'b011;
      endcase
     end
   assign out = arbSt_P;
endmodule
