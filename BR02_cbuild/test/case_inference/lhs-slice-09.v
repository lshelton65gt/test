module top (clk, sel, in, out);
  input clk;
  input [1:0] sel;
  input [31:0] in;
  output [0:31] out;
  reg [0:31] 	out;
  always @(posedge clk) begin
    out [0:1] = 2'b0;
    out [15:31] = 17'b0;
    case (sel)
      2'b00: out [2:5] = in [14:0];
      2'b01: out [5:8] = in [15:1];
      2'b10: out [8:11] = in [16:2];
      2'b11: out [11:14] = in [17:3];
    endcase
  end
endmodule
