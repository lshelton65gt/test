module vector ( out, sel, in1, in2, cond);
   input [1:0] in1;
   input [1:0] in2;
   input  cond;
   input  sel;
   output out;
   reg 	  rsp;
   always @( sel or in1 or cond or in2 )
     begin
	case (sel)
	  2'b0:
	    rsp = (cond==0) ? in1[0] : in2[0];
	  2'b1:
	    rsp = (cond==1) ? in1[1] : in2[1];
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = rsp;
endmodule // vector
