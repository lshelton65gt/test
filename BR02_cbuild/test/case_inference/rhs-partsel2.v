module top (clk, sel, in, out);
  input clk;
  input sel;
  input [31:0] in;
  output [14:0] out;
  reg [14:0] 	out;
  always @(posedge clk) begin
    case (sel)
      1'b1: out = in [30:16];
      1'b0: out = in [29:15];
    endcase
  end
endmodule
