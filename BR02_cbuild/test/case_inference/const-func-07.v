// last mod: Thu Dec 21 10:29:55 2006
// filename: test/case_inference/const-func-07.v
// Description:  This test is like const-func-01.v but here the case selector
// has been shifted left by 1 bit (so lsb is always 0) and the case labels have
// also been shifted.  Should produce the same results as cons-func-01.v

module top (clk, sel, x);
  input clk;
  input [1:0] sel;
  output [3:0] x;
  reg [3:0]    x;
  always @(posedge clk)
    case ({1'b1,sel}<<1)
      3'b000: x = 4'b0001;
      3'b010: x = 4'b0010;
      3'b100: x = 4'b0100;
      3'b110: x = 4'b1000;
    endcase // case(sel)
endmodule // top
