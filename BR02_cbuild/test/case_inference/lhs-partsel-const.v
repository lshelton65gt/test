module vecotr ( out, sel );
   input [1:0] sel;
   output [4:0] out;
   reg [4:0] 	rsp1;
   always @( sel )
     begin
	rsp1 = 0;
	case (sel)
	  2'b0:
	    {rsp1[2:1]} = 2'b0;
	  2'b1:
	    {rsp1[2:1]} = 2'b1;
	  2'h2:
	    {rsp1[2:1]} = 2'h2;
	  2'h3:
	    {rsp1[2:1]} = 2'h3;
	endcase // case(sel)
     end // always @ ( sel)
   assign out = rsp1;
endmodule // vector
