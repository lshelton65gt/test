module top (clk, sel, x);
  input clk;
  input sel;
  output [3:0] x;
  reg [3:0] x;
  always @(posedge clk)
    case (sel)
      1'b0: x = 4'b0000;
      1'b1: x = 4'b0101;
    endcase
endmodule // top
