module vector ( out, sel, in1, in2, cond);
   input [3:0] in1;
   input [6:0] in2;
   input       cond;
   input [0:1] sel;
   output   out;
   reg      tmp;
   reg 	    rsp;
   always @( sel or in1 or in2 or cond)
     begin
	case (sel)
	  2'b0:
	    rsp = cond ? in1[0] : in2[1];
	  2'b1:
	    rsp = cond ? in1[1] : in2[2];
	  2'h2:
	    rsp = cond ? in1[2] : in2[3];
	  2'h3:
	    rsp = cond ? in1[3] : in2[4];
	endcase // case(sel)
     end // always @ ( sel or in1 or in2 or cond)
   assign out = rsp;
endmodule // vector
