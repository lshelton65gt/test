module vector ( out, sel, a);
   input [1:0] sel;
   input [31:0] a;
   output [31:0] out;
   reg [31:0] rsp[3:0];
   reg [31:0] rsp2;
   integer     i;

   always @( a or sel )
     begin
	for (i=0; i<4; i=i+1)
	  rsp[i] = a + i;
	case (sel)
	  2'b0:
	    rsp2 = rsp[0];
	  2'b1:
	    rsp2 = rsp[1];
	  2'h2:
	    rsp2 = rsp[2];
	  2'h3:
	    rsp2 = rsp[3];
	endcase // case(sel)
     end // always @ ( sel)

   assign out = rsp2;
endmodule // vector
