module top (clk, sel, x);
  input clk;
  input [6:0] sel;
  output [3:0] x;
  reg [3:0] x;
  always @(posedge clk)
    case (sel [5:2] == 4'b0011)
      1'b0: x = 4'h2;
      1'b1: x = 4'h5;
    endcase
endmodule // top
