module vector ( out, sel, in1);
   input [3:0] in1;
   input [0:1] sel;
   output [5:0] out;
   reg [5:0] 	rsp;
   always @( sel or in1 )
     begin
	rsp = 0;
	case (sel)
	  2'b0:
	    rsp[2] = in1[0];
	  2'b1:
	    rsp[3] = in1[1];
	  2'h2:
	    rsp[4] = in1[2];
	  2'h3:
	    rsp[5] = in1[3];
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = {rsp[5:2],sel};
endmodule // vector
