module test (clk, sel, out);
  input clk;
  input sel;
  output [3:0] out;
  reg [3:0]    out;

  function [3:0] tri_case;
    input cnd1;
    case (cnd1)
      1'b1: tri_case = 4'bz01Z;
      1'b0: tri_case = 4'b11z0;
    endcase
  endfunction

  always @(clk)
    out = tri_case (sel);
endmodule 

