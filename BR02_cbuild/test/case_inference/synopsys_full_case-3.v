module test (sel, clk, in, out);
  input sel;
  input clk;
  input [1:0] in;
  output [1:0] out;
  reg [1:0]    out;
  always @(clk) begin
    case (sel) // carbon full_case
      0: out = 2'b10;
    endcase
  end
endmodule // test
