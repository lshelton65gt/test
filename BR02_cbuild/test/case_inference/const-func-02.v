module top (clk, sel, x);
  input clk;
  input [5:0] sel;
  output [3:0] x;
  reg [3:0]    x;
  always @(posedge clk)
    case (sel == 5'h30)
      1'b0: x = 3'h1;
      1'b1: x = 3'h6;
    endcase // case(sel)
endmodule // top
