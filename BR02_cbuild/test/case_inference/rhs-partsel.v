module vecotr ( out, sel, in1 );
   input [4:0] in1;
   input [1:0] sel;
   output [4:0] out;
   reg [1:0] 	rsp1;
   reg 	       rsp2;
   always @( sel or in1 )
     begin
	case (sel)
	  2'b0:
	    rsp1 = {in1[1:0]};
	  2'b1:
	    rsp1 = {in1[2:1]};
	  2'h2:
	    rsp1 = {in1[3:2]};
	  2'h3:
	    rsp1 = {in1[4:3]};
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = rsp1;
endmodule // vector
