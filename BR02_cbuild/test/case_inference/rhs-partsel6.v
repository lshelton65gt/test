module top (clk, sel, in, out);
  input clk;
  input sel;
  input [0:31] in;
  output [14:0] out;
  reg [14:0] 	out;
  always @(posedge clk) begin
    case (sel)
      // case_multiple = 1
      // case_offset = 0
      1'b0: out = in [17:31]; // rvalue range = {mMsb = 14, mLsb = 0}
      1'b1: out = in [16:30]; // rvalue range = {mMsb = 15, mLsb = 1}
    endcase
  end
endmodule
