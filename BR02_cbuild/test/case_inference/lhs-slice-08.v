module top (clk, sel, in, out);
  input clk;
  input [1:0] sel;
  input [31:0] in;
  output [31:0] out;
  reg [31:0] 	out;
  always @(posedge clk) begin
    out [1:0] = 2'b0;
    out [31:15] = 17'b0;
    case (sel)
      2'b00: out [5:2] = in [14:0];
      2'b01: out [8:5] = in [15:1];
      2'b10: out [11:8] = in [16:2];
      2'b11: out [14:11] = in [17:3];
    endcase
  end
endmodule
