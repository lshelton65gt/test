module vector ( out, sel, in1, in2);
   input [3:0] in1;
   input [3:0] in2;
   input [1:0] sel;
   output[7:0]   out;
   reg[7:0] 	    rsp;
   always @( sel or in1 )
     begin
	rsp = 0;
	case (sel)
	  2'b0:
	    rsp[0] = in1[0];
	  2'b1:
	    rsp[1] = in1[1];
	  2'h2:
	    rsp[2] = in1[2];
	  2'h3:
	    rsp[3] = in1[3];
	endcase // case(sel)
     end // always @ ( sel or in1)
   always @( sel or in2 )
     begin
	case (sel)
	  2'b0:
	    rsp[4] = in2[0];
	  2'b1:
	    rsp[5] = in2[1];
	  2'h2:
	    rsp[6] = in2[2];
	  2'h3:
	    rsp[7] = in2[3];
	endcase // case(sel)
     end // always @ ( sel or in2)
   assign out = rsp;
endmodule // vector
