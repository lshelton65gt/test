module vecotr ( out, sel, in1, in2 );
   input [3:0] in1;
   input [3:0] in2;
   input [1:0] sel;
   output [1:0] out;
   reg [1:0] 	rsp1;
   always @( sel or in1 )
     begin
	case (sel)
	  2'b0:
	    rsp1 = {in1[0],in2[0]};
	  2'b1:
	    rsp1 = {in1[1],in2[1]};
	  2'h2:
	    rsp1 = {in1[2],in2[2]};
	  2'h3:
	    rsp1 = {in1[3],in2[3]};
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = rsp1;
endmodule // vector
