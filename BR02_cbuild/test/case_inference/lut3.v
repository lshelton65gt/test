// bug 3911 - test differing sizes of the rhs
// get resized correctly.
module bug(clk, num, msg);
   input clk;
   input [7:0] num;

   output [255:0] msg;
   reg [255:0] msg;

   parameter   MSG_0 = "zero";
   parameter   MSG_1 = "one";
   parameter   MSG_2 = "two";
   parameter   MSG_3 = "some big string 3";
   parameter   MSG_4 = "some big string 4";
   parameter   MSG_5 = "some big string 5";
   parameter   MSG_6 = "some big string 6";
   parameter   MSG_7 = "some big string 7";
   parameter   MSG_8 = "some big string 8";
   parameter   MSG_9 = "some big string 9";
   parameter   MSG_10 = "some big string 10";
   parameter   MSG_11 = "some big string 11";
   parameter   MSG_12 = "some big string 12";
   parameter   MSG_13 = "some big string 13";
   parameter   MSG_14 = "some big string 14";
   parameter   MSG_15 = "some big string 15";
   parameter   MSG_16 = "some big string 16";
   parameter   MSG_17 = "some big string 17";
   parameter   MSG_18 = "some big string 18";
   parameter   MSG_19 = "some big string 19";
   parameter   MSG_20 = "some big string 20";
   parameter   MSG_21 = "some big string 21";
   parameter   MSG_22 = "some big string 22";
   parameter   MSG_23 = "some big string 23";
   parameter   MSG_24 = "some big string 24";
   parameter   MSG_25 = "some big string 25";
   parameter   MSG_26 = "some big string 26";
   parameter   MSG_27 = "some big string 27";
   parameter   MSG_28 = "some big string 28";
   parameter   MSG_29 = "some big string 29";
   parameter   MSG_30 = "some big string 00";
   parameter   MSG_31 = "some big string 31";

   always @(posedge clk) begin
      case (num[4:0])
	5'd0: msg  <= MSG_0;
	5'd1: msg  <= MSG_1;
	5'd2: msg  <= MSG_2;
	5'd3: msg  <= MSG_3;
	5'd4: msg  <= MSG_4;
	5'd5: msg  <= MSG_5;
	5'd6: msg  <= MSG_6;
	5'd7: msg  <= MSG_7;
	5'd8: msg  <= MSG_8;
	5'd9: msg  <= MSG_9;
	5'd10: msg <= MSG_10;
	5'd11: msg <= MSG_11;
	5'd12: msg <= MSG_12;
	5'd13: msg <= MSG_13;
	5'd14: msg <= MSG_14;
	5'd15: msg <= MSG_15;
	5'd16: msg <= MSG_16;
	5'd17: msg <= MSG_17;
	5'd18: msg <= MSG_18;
	5'd19: msg <= MSG_19;
	5'd20: msg <= MSG_20;
	5'd21: msg <= MSG_21;
	5'd22: msg <= MSG_22;
	5'd23: msg <= MSG_23;
	5'd24: msg <= MSG_24;
	5'd25: msg <= MSG_25;
	5'd26: msg <= MSG_26;
	5'd27: msg <= MSG_27;
	5'd28: msg <= MSG_28;
	5'd29: msg <= MSG_29;
	5'd30: msg <= MSG_30;
	5'd31: msg <= MSG_31;
      endcase
   end

endmodule

