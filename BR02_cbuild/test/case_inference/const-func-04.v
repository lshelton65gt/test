module top (clk, sel, x);
  input clk;
  input [4:0] sel;
  output [3:0] x;
  reg [3:0] x;
  always @(posedge clk)
    case (sel [3:2])
      2'b00: x = 4'b0001;
      2'b01: x = 4'b0010;
      2'b10: x = 4'b0100;
      2'b11: x = 4'b1000;
    endcase // case(sel)
endmodule // top
