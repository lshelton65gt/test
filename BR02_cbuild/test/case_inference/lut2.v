module top(i, o);
   input [1:0] i;
   output [74:0] o;
   reg [74:0]    o;

   always @(i)
     begin
        case (i)
          2'b00: o = 74'd30;
          2'b01: o = 74'd40;
          2'b10: o = 74'd50;
          2'b11: o = 74'd61;
        endcase
     end
endmodule
