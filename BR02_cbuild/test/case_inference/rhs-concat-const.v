module vecotr ( out, sel, in1, in2 );
   input  in1;
   input [1:0] in2;
   input [1:0] sel;
   output [1:0] out;
   reg [1:0] 	rsp1;
   reg 	       rsp2;
   always @( sel or in1 or in2)
     begin
	case (sel)
	  2'b0:
	    rsp1 = {in1,in2[0]};
	  2'b1:
	    rsp1 = {in1,in2[0]};
	  2'h2:
	    rsp1 = {in1,in2[0]};
	  2'h3:
	    rsp1 = {in1,in2[0]};
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = rsp1;
endmodule // vector
