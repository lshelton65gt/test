module test (sel, clk, in, out);
  input sel;
  input clk;
  input [1:0] in;
  output [1:0] out;
  reg [1:0]    out;
  initial out = 0;
  always @(clk) begin
    case (sel) // carbon full_case
      0: out [0] = in [0];
    endcase
  end
endmodule // test
