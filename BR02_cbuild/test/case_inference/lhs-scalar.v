module vecotr ( out, sel, in1 );
   input [3:0] in1;
   input [1:0] sel;
   output      out;
   reg 	       rsp;
   always @( sel or in1 )
     begin
	case (sel)
	  2'b0:
	    rsp = in1[0];
	  2'b1:
	    rsp = in1[1];
	  2'h2:
	    rsp = in1[2];
	  2'h3:
	    rsp = in1[3];
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = rsp;
endmodule // vector
