module top (clk, y, sel, x, out);
  input clk;
  input [7:0] y;
  input [1:0] x;
  output [3:0] out;
  reg [3:0]    out;
  input        sel;
  always @(clk)
    case (sel)
      1'b0: out = x[0] ? 4'h1 : 4'h0;
      1'b1: out = x[1] ? 4'h1 : 4'h1;
    endcase // case(sel)
endmodule // top
