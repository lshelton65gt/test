module top (clk, sel, in, out);
  input clk;
  input sel;
  input [0:31] in;
  output [14:0] out;
  reg [14:0] 	out;
  always @(posedge clk) begin
    case (sel)
      // case_multiple = -1
      // case_offset = 2
      1'b1: out = in [16:30]; // rvalue range = {mMsb = 15, mLsb = 1}
      1'b0: out = in [15:29]; // rvalue range = {mMsb = 16, mLsb = 2}
    endcase
  end
endmodule
