module vecotr ( currentReq_CN, out);
   input 	currentReq_CN;
   reg [2:0] 	arbSt_P;
   output [2:0] out;

   always @(currentReq_CN)

     begin
      case (~currentReq_CN) 
	     1'b0:
	       arbSt_P = 3'b0;
	     1'b1:
	       arbSt_P = 3'b1;
      endcase
     end
   assign out = arbSt_P;
endmodule
