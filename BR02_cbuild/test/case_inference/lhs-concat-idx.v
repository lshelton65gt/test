module vecotr ( out, sel, in1 );
   input [1:0] in1;
   input [1:0] sel;
   output [3:0] out;
   reg [3:0] 	rsp1;
   reg [4:0] 	rsp2;
   always @( sel or in1 )
     begin
	rsp1 = 0;
	rsp2 = 0;
	case (sel)
	  2'b0:
	    {rsp1[0],rsp1[3]} = in1;
	  2'b1:
	    {rsp1[1],rsp1[3]} = in1;
	  2'h2:
	    {rsp1[2],rsp1[3]} = in1;
	  2'h3:
	    {rsp1[3],rsp1[3]} = in1;
	endcase // case(sel)
     end // always @ ( sel or in1)
   assign out = rsp1;
endmodule // vector
