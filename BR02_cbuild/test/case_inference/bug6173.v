`define DATAWORD_MSB 31
module mux4(mux_in,selected_src1,selected_src2,alu_result_logic);
  input  [3:0] mux_in; 
  input  [`DATAWORD_MSB:0] selected_src1; 
  input  [`DATAWORD_MSB:0] selected_src2; 
  output [`DATAWORD_MSB:0] alu_result_logic; 

  reg [`DATAWORD_MSB:0] alu_result_logic_int; 
  reg [1:0] data_sel; 

  integer I;
  always @(selected_src1 or selected_src2 or mux_in)
  begin
      for(I = 0; I <= `DATAWORD_MSB; I = I + 1)
      begin
        data_sel = {selected_src1[I], selected_src2[I]};
`ifdef ORIGINAL
        case (data_sel)
          2'b00 :   alu_result_logic_int[I] = mux_in[0];
          2'b01 :   alu_result_logic_int[I] = mux_in[1];
          2'b10 :   alu_result_logic_int[I] = mux_in[2];
          2'b11 :   alu_result_logic_int[I] = mux_in[3];
	  // redundant default.
          //default : alu_result_logic_int[I] = mux_in[0];
        endcase
`else
        alu_result_logic_int[I] = mux_in[data_sel];
`endif
      end
  end 

  assign alu_result_logic = alu_result_logic_int;
endmodule
