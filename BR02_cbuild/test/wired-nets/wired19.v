
module top(clk, out,out2, c, d, f);
   input c, d, clk, f;
   output out, out2;
   wor   w;
   reg   q;

   sub s1(w, out, q, c, clk);
   
   always @(posedge clk)
     begin
        q <= d;
     end

   assign out2 = w;
   assign w = f;
   
endmodule // top

module sub(w, out, q, c, clk);
   output w;
   output out;
   input  q, c, clk;
   wire   w;
   
   assign w = s2.wreg;
   
   sub2 s2(out, q, c, clk);
   
endmodule // sub

module sub2(out, q, c, clk);
   output out;
   input  q, c, clk;

   reg    wreg;
   
   assign out = q & wreg;

   always @(negedge clk)
     begin
        wreg = c;
     end
endmodule // s2
