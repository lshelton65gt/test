
module top(out1, out2, out3, out4, in1, in2, in3, in4);
   output out1, out2, out3, out4;
   input  in1, in2, in3, in4;
   
   SUBC subc(out1, out2, in1, in2);
   SUBD subd(out3, out4, in3, in4);
endmodule // top

module SUBC(out1, out2, in1, in2);
   output out1, out2;
   input  in1, in2;
   
   SUBA suba(out1, in1);
   SUBB subb(out2, in2);
endmodule // SUBC

module SUBD(out1, out2, in1, in2);
   output out1, out2;
   input  in1, in2;

   SUBA suba(out1, in1);
   SUBE subb(out2, in2);
endmodule // SUBD

module SUBA(out, in);
   output out;
   input  in;
   
   assign subb.wired = out;
   assign out = in;
endmodule // SUBA

module SUBB(out, in);
   output out;
   input  in;

   wand wired;
   assign wired = in;
   assign out = wired;
   
endmodule // SUBB

module SUBE(out, in);
   output out;
   input  in;

   wand   wired;
   assign out = wired;
   assign wired = ~in;
endmodule // SUBB
