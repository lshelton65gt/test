
module top(clk, out,out2, c, d, f);
   input c, d, clk, f;
   output out, out2;
   wor   w;
   reg   q;

   sub s1(out, q, c, clk);
   
   always @(posedge clk)
     begin
        q <= d;
     end

   assign out2 = w;
   assign top.s1.w = f;  // *** driver 1 of top.s1.w
   
endmodule // top

module sub(out, q, c, clk);
   output out;
   input  q, c, clk;
   wire   w;

   assign top.w = w;
   
   assign w = s2.wreg;   // *** driver 2 of top.s1.w, gives X in sim gold

   sub2 s2(out, q, c, clk);
   
endmodule // sub

module sub2(out, q, c, clk);
   output out;
   input  q, c, clk;

   reg    wreg;
   
   // After flattening, continuous assignment aliasing over-aggressively
   // aliases top.s1.w to top.s1.s2.wreg, which means 'out' sees the
   // conflicted driver and gets the wrong answer.  In Aldec, 'wreg' and
   // 'out' will not be affected by driver 1 (top.s1.w=f), so we mismatch.
   //
   // Two action items for this:
   //   1. assignment aliasing should avoid aliasing conflicting non-Z
   //      drivers
   //   2. RENetWarning should warn on conflicting non-Z drivers.  It
   //      probably can't issue an Alert because of redundant driver
   //      scenarios, although it's possible that functional aliasing
   //      may alleviate some of those.
   
   assign out = q & wreg;

   always @(negedge clk)
     begin
        wreg = c;
     end
endmodule // s2
