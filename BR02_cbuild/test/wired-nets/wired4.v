
module top(g, out, out2, out3, c, d, f);
   input c, d, g, f;
   output out, out2, out3;
   wire   out, out2;
   wor    out3;
   
   sub s1(out, c);
   sub s2(out2, d);
   sub s3(out3, f);
   
   assign out3 = g;
   assign out2 = g;
   assign out = g;
   
endmodule // top

module sub(out, in);
   output out;
   input  in;

   assign out = in;
endmodule // sub

