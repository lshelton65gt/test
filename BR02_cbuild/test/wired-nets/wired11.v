
module top(clk, out1, out2, d, f, en1, en2);
   input d, clk, f, en1, en2;
   output out1, out2;
   wor   w;
   reg   q;

   always @(posedge clk)
     begin
        q <= d;
     end

   assign {out2, out1} = {q,w};
   assign w = 1'bz;
   assign w = en1 ? 1'b1 : en2 ? f : 1'b0;
   assign w = q;   
endmodule // top

