
module top(clk, out, d, f, g, en1, en2);
   input [1:0] d;
   input clk, f, g, en1, en2;
   output out;
   triand [3:0]  w;
   reg  [1:0] q;

   always @(posedge clk)
     begin
        q <= d;
     end

   assign out = w;
   assign w[0] = en1 ? 1'b0 : en2 ? f : 1'b1;
   assign w[1] = g;
   assign w[3:2] = q;   
endmodule // top

