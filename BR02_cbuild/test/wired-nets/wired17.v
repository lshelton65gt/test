
module top(clk,out2, c, d, f);
   input c, d, clk, f;
   output out2;
   wor   w;
   reg   q;


   function myfunc;
      input in, in2;

      myfunc = in ^ in2;
   endfunction // myfunc
   
   always @(posedge clk)
     begin
        q <= d;
     end

   assign out2 = w;
   assign w = myfunc(c, d);
   assign w = f;
   
endmodule // top
