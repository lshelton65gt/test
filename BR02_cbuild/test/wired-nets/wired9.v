
module top(clk, out, f, en1, en2);
   input clk, f, en1, en2;
   inout out;
   wor    out;

   assign out = en1 ? 1'b1 : en2 ? f : 1'bz;
endmodule // top

