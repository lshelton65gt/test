
module top(clk, out, d, f);
   input d, clk, f;
   output out;
   wor   w;
   reg   q;

   sub s1(w, q);
   
   always @(posedge clk)
     begin
        q <= d;
     end

   assign out = w;
   assign w = f;
   
endmodule // top

module sub(w, q);
   output w;
   input  q;
   
   wire w;
   reg    out;
   
   assign w = q;
   
endmodule // sub
