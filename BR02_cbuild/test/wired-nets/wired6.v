
module top(clk, out, d, f, en);
   input d, clk, f, en;
   output out;
   wor   w;
   reg   q;

   always @(posedge clk)
     begin
        q <= d;
     end

   assign out = w;
   assign w = en ? f : 0;
   assign w = q;   
endmodule // top

