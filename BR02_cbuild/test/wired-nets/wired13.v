
module top(out1, out2, d, f, en1, en2);
   input d, f, en1, en2;
   output out1, out2;
   wor [1:0]  w;


   assign {out2, out1} = w;
   assign w = {en2 ? 1'b0 : d,en1 ? 1'b1 : en2 ? f : 1'b0};
endmodule // top

