
module top(clk, out, d, f, en1, en2);
   input d, clk, f, en1, en2;
   output out;
   
   wor   w;
   reg   q;

   always @(posedge clk)
     begin
        q <= d;
     end

   assign out = w;
   assign w = en1 ? 1'b1 : en2 ? f : 1'bz;
   assign w = q;   
endmodule // top

