
module top(clk, out, d, f);
   input d, clk, f;
   output out;
   trior    w;
   reg   q;

   always @(posedge clk)
     begin
        q <= d;
     end

   assign out = w;
   assign w = f;
   assign w = q;   
endmodule // top

