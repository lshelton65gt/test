
module top(clk, out, out2, out3, c, d, f);
   input c, d, clk, f;
   output out, out2, out3;
   wire   w;

   sub s1(w, out, q, c, clk);

   sub2 s2(w, d, out3);
   
   assign out2 = w;
   assign w = f;
   
endmodule // top

module sub(w, out, q, c, clk);
   output w;
   output out;
   input  q, c, clk;
   
   wire w;
   reg  tmp;
   
   reg    out;
   
   always @(negedge clk)
     begin
        tmp = q;
        out = ~tmp;
        tmp = c;
     end
   
   assign w = tmp;
   
endmodule // sub

module sub2(q, d, out3);
   inout q;
   input d;
   output out3;

   wor   q;
   
   assign q = d;
   assign out3 = q;

endmodule // sub2
