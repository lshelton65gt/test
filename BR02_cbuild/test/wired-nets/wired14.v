
module lhsWired(wo, out, in, in2);
   input [1:2] in;
   input       in2;
   
   output out;

   output wo;
   wor    wo;
   wand   wa;
   
   assign {wa, wo} = in;
   assign wa = in2;
   assign wo = wa ^ in;
   assign out = wa | wo;
endmodule // wiredTask
