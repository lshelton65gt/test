primitive udp_rslatn_out_ (out_, r_, s_, NOTIFIER);
   output out_;  
   input  r_, s_, NOTIFIER;
   reg    out_;

   table

// r_  s_  NOT : Qt : Qt+1
// 
  (?1) 1   ?   : ?  :  -  ; // no change
   1  (?1) ?   : ?  :  -  ; // no change
  (?0) 1   ?   : ?  :  1  ; // reset
   0  (?1) ?   : ?  :  1  ; // reset
  (?1) 0   ?   : ?  :  0  ; // set
   1  (?0) ?   : ?  :  0  ; // set
  (?0) 0   ?   : ?  :  1  ; // unused state
   0  (?0) ?   : ?  :  1  ; // unused state
  (?1) x   ?   : 0  :  0  ; // reduced pessimism
   1  (?x) ?   : 0  :  0  ; // reduced pessimism
  (?x) 1   ?   : 1  :  1  ; // reduced pessimism
   x  (?1) ?   : 1  :  1  ; // reduced pessimism
   ?   ?   *   : ?  :  x  ; // any notifier changed

   endtable
endprimitive // udp_rslatn_out_

module top (out_, r_, s_, NOTIFIER);
  output out_;  
  input  r_, s_, NOTIFIER;
  udp_rslatn_out_ (out_, r_, s_, NOTIFIER);
endmodule // top
