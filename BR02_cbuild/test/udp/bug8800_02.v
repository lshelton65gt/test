// This test case was derived from bug8800.
//
// VCS appears to do the wrong thing with this udp:
//
// VCS gives the results (this is bug8800_01-01.vcs.gold):
//
// D C R S n Q0 Q1
// 1 1 0 1 0    0
// 1 1 0 0 1 0  x
// 1 0 1 1 1 x  1
//
// There does not seem to be any entry in the table that
// sets Q to 1 for these inputs...

module top (D, C, R, S, notifier, Q);
  input D, C, R, S, notifier;
  output Q;
  DLSFQ2_2 (Q, D, C, R, S, notifier);
endmodule

primitive DLSFQ2_2 ( Q, D, C, R, S, notifier );
    output Q;
    reg  Q;
    input  D,C,R,S,notifier;

    table
    // D    C    R    S notifier  : Qtn    :  Qtn+1

     (?1)   1    1    1    ?     :  ?        :   1;
       1  (?1)   1    1    ?     :  ?        :   1;
       1    1  (?1)   1    ?     :  ?        :   1;
       1    1    1  (?1)   ?     :  ?        :   1;
     (?0)   1    1    1    ?     :  ?        :   0;
       0  (?1)   1    1    ?     :  ?        :   0;
       0    1  (?1)   1    ?     :  ?        :   0;
       0    1    1  (?1)   ?     :  ?        :   0;
       ?    0   (?1)  1    ?     :  ?        :   -;
       ?    0    1   (?1)  ?     :  ?        :   -;
       1  (?x)   1    1    ?     :  1        :   -;
       0  (?x)   1    1    ?     :  0        :   -;
      (?1)  x    1    1    ?     :  1        :   -;
      (?0)  x    1    1    ?     :  0        :   -;
       0    x   (?1)  1    ?     :  0        :   -; // added 9/22/94
       1    x    1   (?1)  ?     :  1        :   -; // added 9/22/94
       *    0    1    1    ?     :  ?        :   -;

       0    0    x    1    ?     :  0        :   -; // added 3/12/91
       1    0    x    1    ?     :  0        :   -; // added 9/28/94
       0    1    x    1    ?     :  ?        :   0;
       0    x    x    1    ?     :  0        :   -;
       x    0    x    1    ?     :  0        :   -;  
     
       1    0    1    x    ?     :  1        :   -; // added 4/12/95
       0    0    1    x    ?     :  1        :   -; // added 4/12/95
       1    1    1    x    ?     :  ?        :   1; // added 4/12/95
       1    x    1    x    ?     :  1        :   -; // added 4/12/95
       x    0    1    x    ?     :  1        :   -; // added 4/12/95
     
       ?    ?    1    0    ?     :  ?        :   1;
       ?    ?    0    1    ?     :  ?        :   0;

//       ?    ?    0    0    ?     :  ?        :   1; // set & reset
//       ?    ?    0    x    ?     :  ?        :   x; // set & reset
//       ?    ?    x    0    ?     :  ?        :   1; // set & reset
       ?    ?    0    0    ?     :  ?        :   x; // set & reset
       ?    ?    0    x    ?     :  ?        :   x; // set & reset
       ?    ?    x    0    ?     :  ?        :   x; // set & reset
      
       b  (?0)   1    1    ?     :  ?        :   -;
       ?    ?    ?    ?    *     :  ?        :   x; // Output an x if the
                                                    //notifier changes

    endtable
endprimitive

