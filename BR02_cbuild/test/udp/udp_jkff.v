primitive udp_jkff (out, j, k, clk, clr_, set_, NOTIFIER);
   output out;  
   input  j, k, clk, clr_, set_, NOTIFIER;
   reg    out;

   table

// j  k  clk  clr_   set_  NOT  : Qt : Qt+1
//       
   0  0   r    1      1     ?   : ?  :  -  ; // output remains same
   0  1   r    ?      1     ?   : ?  :  0  ; // clock in 0
   1  0   r    1      ?     ?   : ?  :  1  ; // clock in 1
   1  1   r    ?      1     ?   : 1  :  0  ; // clock in 0
   1  1   r    1      ?     ?   : 0  :  1  ; // clock in 1
   ?  0   *    1      ?     ?   : 1  :  1  ; // reduce pessimism
   0  ?   *    ?      1     ?   : 0  :  0  ; // reduce pessimism
   ?  ?   f    ?      ?     ?   : ?  :  -  ; // no changes on negedge clk
   *  ?   b    ?      ?     ?   : ?  :  -  ; // no changes when j switches
   *  0   x    1      ?     ?   : 1  :  1  ; // no changes when j switches
   ?  *   b    ?      ?     ?   : ?  :  -  ; // no changes when k switches
   0  *   x    ?      1     ?   : 0  :  0  ; // no changes when k switches
   ?  ?   ?    ?      0     ?   : ?  :  1  ; // set output
   ?  ?   b    1      *     ?   : 1  :  1  ; // cover all transistions on set_
   ?  0   x    1      *     ?   : 1  :  1  ; // cover all transistions on set_
   ?  ?   ?    0      1     ?   : ?  :  0  ; // reset output
   ?  ?   b    *      1     ?   : 0  :  0  ; // cover all transistions on clr_
   0  ?   x    *      1     ?   : 0  :  0  ; // cover all transistions on clr_
   ?  ?   ?    ?      ?     *   : ?  :  x  ; // any notifier change

   endtable
endprimitive // udp_jkff

module top (out, j, k, clk, clr_, set_, NOTIFIER);
  output out;  
  input  j, k, clk, clr_, set_, NOTIFIER;
  udp_jkff JK (out, j, k, clk, clr_, set_, NOTIFIER);
endmodule
