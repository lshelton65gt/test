// User-Defined Primitives

// COMPASS Design Automation

/*****************************************************************************************/
// NOTIFIER feature inserted by RAVI for generating 'X' on outputs during
// timing check violations like setup,hold,width.   10/12/92.

// Positive edge triggered D FF w/ => active low set and clr; 
//                                 => q set to high when simultaneous set and clr;
//
primitive UDP_DFF(q, d, cp, set, clr, notify);

   output q;
   input d, cp, set, clr, notify;
   reg q;
   initial q = 1'b1;
   table
   //                      n
   //                      o
   //             s    c   t    p    
   //        c    e    l   i    r
   //   d    p    t    r   f    e     q
   	
	1    r    1    1   ?  : ? :   1 ;
	1    r    x    1   ?  : ? :   1 ;
	0    r    x    1   ?  : ? :   x ; // new 1-30-2002
//	?    ?    x    1   ?  : 1 :   1 ; // removed 2-19-2002
	?    0    x    1   ?  : 1 :   1 ; // new 2-19-2002
	?    x    1    1   ?  : ? :   x ; // new 1-14-93

	0    r    1    1   ?  : ? :   0 ;
	0    r    1    x   ?  : ? :   0 ;
	1    r    1    x   ?  : ? :   x ; // new 1-30-2002
//	?    ?    1    x   ?  : 0 :   0 ; // removed 2-19-2002
	?    0    1    x   ?  : 0 :   0 ; // new 2-19-2002

	1  (x1)   1    1   ?  : 1 :   1 ;
//	1  (0x)   1    1   ?  : 1 :   1 ; removed 1-14-93
	0  (x1)   1    1   ?  : 0 :   0 ;
//	0  (0x)   1    1   ?  : 0 :   0 ; removed 1-14-93
	?  (?x)   ?    ?   ?  : ? :   x ; // new 1-14-93
	
	?    ?    0    1   ?  : ? :   1 ;
	?    ?    1    0   ?  : ? :   0 ;
	?    ?    0    0   ?  : ? :   0 ;

	?  (?0)   1    1   ?  : ? :   - ;
//	?  (1x)   1    1   ?  : ? :   - ; removed 1-14-93
	*    ?    ?    ?   ?  : ? :   - ;

	?    ?  (?1)   ?   ?  : ? :   - ;
	?    ?    ?  (?1)  ?  : ? :   - ;

	?    ?    ?    ?   *  : ? :   x ;// output x on any notifier change
       

   endtable

endprimitive

