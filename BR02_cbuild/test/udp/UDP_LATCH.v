// User-Defined Primitives

// COMPASS Design Automation

//modified by Penny Wang from Cadence primitives;

primitive UDP_LATCH (Q, D, G, SB, RB, NOTI_REG); 

    output Q; 
    reg    Q;                               
    input  D,               // DATA
           G,               // CLOCK
           SB,              // SET ACTIVE LOW
           RB,              // CLEAR ACTIVE LOW
           NOTI_REG;        // NOTIFY_REG
// FUNCTION : POSITIVE LEVEL SENSITIVE D-TYPE LATCH WITH ACTIVE LOW
//            ASYNCHRONOUS SET AND RESET.  ( Q OUTPUT UDP ).


   table
     //  D       G      SB    RB   NOTI_REG       : Qtn :   Qtn+1
                                  
         *       0      1     1     ?             :  ?  :    -   ;
                                  
         ?       ?      0     1     ?             :  ?  :    1   ; // asynchro set
     
         ?       ?      ?     0     ?             :  ?  :    0   ; 
                                                                    // occurs 
         ?      (?0)    1     1     ?             :  ?  :    -   ;  //AB
         ?      (1x)    1     1     ?             :  ?  :    -   ;  //AB
                                  
         0      (0x)    1     1     ?             :  0  :    0   ;
         1      (0x)    1     1     ?             :  1  :    1   ;
         0      (x1)    1     1     ?             :  ?  :    0   ;
         1      (x1)    1     1     ?             :  ?  :    1   ;
                                  
        (?0)     1      1     1     ?             :  ?  :    0   ;
        (?1)     1      1     1     ?             :  ?  :    1   ; 
         0      (01)    1     1     ?             :  ?  :    0   ;
         1      (01)    1     1     ?             :  ?  :    1   ; 
                                  
//       0       ?      1     x     ?             :  0  :    0   ; // Reducing pessimism.//AB
       
         ?       0      1   (?x)    ?             :  0  :    0   ;   // Reducing pessimism.//AB
         ?       0    (?1)    x     ?             :  0  :    0   ;   // Reducing pessimism.//AB
         *       0      1     x     ?             :  0  :    0   ;   // Reducing pessimism//AB

        (?0)     x      1     x     ?             :  0  :    0   ;   // Reducing pessimism.//AB
         0       x      1     *     ?             :  0  :    0   ;   // Reducing pessimism.//AB
         0      (0x)    1     x     ?             :  0  :    0   ;   // Reducing pessimism.//AB
         0       x    (?1)    x     ?             :  0  :    0   ;   // Reducing pessimism.//AB

         0      (?1)    1     x     ?             :  ?  :    0   ; // Reducing pessimism.
        (?0)     1      1     x     ?             :  ?  :    0   ; // Reducing pessimism.
         0       1      1   (?x)    ?             :  ?  :    0   ; // Reducing pessimism.//AB
         0       1    (?1)    x     ?             :  ?  :    0   ; // Reducing pessimism.//AB
     
//       1       ?      x     1     ?             :  1  :    1   ; // Reducing pessimism.
        
         ?       0    (?x)    1     ?             :  1  :    1   ;   // Reducing pessimism.//AB
         ?       0      x   (?1)    ?             :  1  :    1   ;   // Reducing pessimism.//AB
         *       0      x     1     ?             :  1  :    1   ;   // Reducing pessimism//AB
 
        (?1)     x      x     1     ?             :  1  :    1   ;   // Reducing pessimism.//AB
         1       x      *     1     ?             :  1  :    1   ;   // Reducing pessimism.//AB
         1      (0x)    x     1     ?             :  1  :    1   ;   // Reducing pessimism.//AB
         1       x      x   (?1)    ?             :  1  :    1   ;   // Reducing pessimism.//AB

      
         1      (?1)    x     1     ?             :  ?  :    1   ; // Reducing pessimism.
        (?1)     1      x     1     ?             :  ?  :    1   ; // Reducing pessimism.
         1       1      x   (?1)    ?             :  ?  :    1   ; // Reducing pessimism.//AB
         1       1    (?x)    1     ?             :  ?  :    1   ; // Reducing pessimism.//AB
                                  
         ?       0      1   (?1)    ?             :  ?  :    -   ;   // ignore edge on clear
         0       1      1   (?1)    ?             :  ?  :    0   ;   // pessimism .
         1       1      1   (?1)    ?             :  ?  :    1   ;  

                                  
         ?       0    (?1)    1     ?             :  ?  :    -   ;   // ignore edge on set
         0       1    (?1)    1     ?             :  ?  :    0   ;   // pessimism .
         1       1    (?1)    1     ?             :  ?  :    1   ;  

        (?1)     x      1     1     ?             :  1  :    1   ; // Reducing pessimism.
        (?0)     x      1     1     ?             :  0  :    0   ; // Reducing pessimism.
                                  
         ?       ?      ?     ?     *             :  ?  :    x   ;

	 1	(10)	x     1     ?		  :  ?  :    1   ; // Notice by Welkin.01/30/01
	 0	(10)	1     x     ?		  :  ?  :    0   ; // Notice by Welkin.01/30/01

   endtable

endprimitive


/*****************************************************************************************/


module top (Q, D, G, SB, RB, NOTI_REG); 

  output Q; 
  input  D, G, SB, RB, NOTI_REG;
  UDP_LATCH (Q, D, G, SB, RB, NOTI_REG);
endmodule // top

