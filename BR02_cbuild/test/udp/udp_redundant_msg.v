// last mod: Fri Feb  3 18:37:26 2006
// filename: test/udp/udp_redundant_msg.v
// Description:  This test was found in test/bugs/bug588,
// with version 1.4227 it incorrectly reports:
// udp_redundant_msg.v:22: Note 4009: Redundant conditional test of e == 0 detected.


module udp_redundant_msg(in, ck, e, out);

   input [1:0] in;
   input ck;
   input       e;

   output [1:0] out;
   

   UDP_dffe C0 ( out[0], ck, e, in[0]);
   UDP_dffe C1 ( out[1], ck, e, in[1]);

endmodule

primitive UDP_dffe (q,  cp, e, d);
   output                q;
   reg 	            q;
   input cp, e, d;
   table
        (01) 1  1 : ? :  1 ;
        (01) 1  0 : ? :  0 ;
         *   0  ? : ? :  - ;
         *   ?  1 : 1 :  - ;
         *   ?  0 : 0 :  - ;
        (1x) ?  ? : ? :  - ;
        (?0) ?  ? : ? :  - ;
         ?   ?  * : ? :  - ;
         ?   *  ? : ? :  - ;
    endtable
endprimitive

