primitive udp_bmx (out, x2, a, s, m1, m0);
   output out;  
   input   x2, a, s, m1, m0;

   table

// x2 a  s m1 m0 :  out
//
   0  1  1  ?  ? :  0;
   0  1  0  0  ? :  1;
   0  1  0  1  ? :  0;
   0  0  1  0  ? :  0;
   0  0  1  1  ? :  1;
   0  0  0  ?  ? :  x;
   1  1  1  ?  ? :  0;
   1  1  0  ?  0 :  1;
   1  1  0  ?  1 :  0;
   1  0  1  ?  0 :  0;
   1  0  1  ?  1 :  1;
   1  0  0  ?  ? :  x;

   endtable
endprimitive // udp_bmx
