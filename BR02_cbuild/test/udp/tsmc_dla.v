primitive tsmc_dla (q, d, e, cdn, sdn, notifier);
   output q;
   input d, e, cdn, sdn, notifier;
   reg q;
   table
// d  e cdn sdn  noti : qt : qt+1
//
   1  1   1   1   ?   : ?  :  1  ; // Latch 1
   0  1   1   1   ?   : ?  :  0  ; // Latch 0
   0 (10) 1   1   ?   : ?  :  0  ; // Latch 0 after falling edge
   1 (10) 1   1   ?   : ?  :  1  ; // Latch 1 after falling edge
   *  0   ?   ?   ?   : ?  :  -  ; // no changes
   ?  ?   ?   0   ?   : ?  :  1  ; // preset to 1
   ?  0   1   *   ?   : 1  :  1  ;
   1  ?   1   *   ?   : 1  :  1  ;
   1  *   1   ?   ?   : 1  :  1  ;
   ?  ?   0   1   ?   : ?  :  0  ; // reset to 0
   ?  0   *   1   ?   : 0  :  0  ;
   0  ?   *   1   ?   : 0  :  0  ;
   0  *   ?   1   ?   : 0  :  0  ;
   ?  ?   ?   ?   *   : ?  :  x  ; // toggle notifier
   endtable
endprimitive

module top (q, d, e, cdn, sdn, notifier);
  output q;
  input   d, e, cdn, sdn, notifier;
  tsmc_dla (q, d, e, cdn, sdn, notifier);
endmodule // top

  
