// filename: test/udp/udp_mxtronics_0.v
// Description:  This test shows a UDP that is incorrectly supported by carbon,
// probably because of the * in the hold colum, line 4, we should at least
// report that this is not supported.


module udp_mxtronics_0(out, in, hold, clr, set, notify);

   output out;
   input in;
   input hold;
   input clr;
   input set;
   input notify;
   
   udp_tlat C0 ( out, in, hold, clr, set, notify);

endmodule

primitive udp_tlat (out, in, hold, clr_, set_, NOTIFIER);
    output out;
    reg   out;
    input  in, hold, clr_, set_, NOTIFIER;
    table
       //  in, hold, clr_, set_, NOTIFIER  out  outnew
            1     0     1     ?         ? :  ? :     1 ;
            0     0     ?     1         ? :  ? :     0 ;
            1     *     1     ?         ? :  1 :     1 ;
            0     *     ?     1         ? :  0 :     0 ;
            *     1     ?     ?         ? :  ? :     - ;
            ?     ?     ?     0         ? :  ? :     1 ;
            ?     1     1     *         ? :  1 :     1 ;
            1     ?     1     *         ? :  1 :     1 ;
            ?     ?     0     1         ? :  ? :     0 ;
            ?     1     *     1         ? :  0 :     0 ;
            0     ?     *     1         ? :  0 :     0 ;
            ?     ?     ?     ?         * :  ? :     x ;
    endtable
endprimitive
