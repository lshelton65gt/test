module comb1( out, a, b, c, d );
output out;
input a;
input b;
input c;
input d;

  comb_udp1( out, a, b, c, d );
endmodule

primitive comb_udp1( out, a, b, c, d );
output out;
input a;
input b;
input c;
input d;

table
//  a b c d : out
    ? ? 1 0 : 0;
    ? x 1 1 : 1;
    0 1 ? B : x;
    1 0 ? B : 1;
    b ? 0 1 : 1;
endtable
endprimitive

