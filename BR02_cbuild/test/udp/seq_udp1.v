// last mod: Wed Dec  7 07:30:44 2005
// filename: test/udp/seq_udp1.v
// Description:  This is the simplest sequential UDP
// currently unsupported because no expression is recognized for the udp 


module seq_udp1(in1, out1);
   input  in1;
   output  out1;

   dut u1(out1, in1);
   
endmodule

primitive dut(z, a);
  output z;
  reg z;
  input a;

  table
     ?:0:0;
  endtable

endprimitive
