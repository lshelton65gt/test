primitive udp_etlat (out, in, hold, clr_, set_, enable, NOTIFIER);
   output out;  
   input  in, hold, clr_, set_, enable, NOTIFIER;
   reg    out;

   table

// in  hold  clr_   set_  enable NOT  : Qt : Qt+1
//
   ?  ?   ?   ?   ?  *   : ?  :  x  ; // any notifier changed
   1  0   1   1   1  ?   : ?  :  1  ; // 
   0  0   1   1   1  ?   : ?  :  0  ; // 
//   ?  0   1   1   0  ?   : ?  :  -  ; // not enabled
//   ?  1   1   1   1  ?   : ?  :  -  ; // not enabled
   ?  r   1   1   0  ?   : ?  :  -  ; // clock in 0
   ?  1   1   1   f  ?   : ?  :  -  ; // clock in 0
   ?  f   1   1   0  ?   : ?  :  -  ; // clock in 0
   ?  1   1   1   r  ?   : ?  :  -  ; // clock in 0
   0  r   1   1   1  ?   : ?  :  0  ; // clock in 0
   1  r   1   1   1  ?   : ?  :  1  ; // clock in 1
   0  0   1   1   f  ?   : ?  :  0  ; // clock in 0
   1  0   1   1   f  ?   : ?  :  1  ; // clock in 1
   1  *   1   ?   1  ?   : 1  :  1  ; // reduce pessimism
   0  *   ?   1   1  ?   : 0  :  0  ; // reduce pessimism
   1  0   1   ?   *  ?   : 1  :  1  ; // reduce pessimism
   0  0   ?   1   *  ?   : 0  :  0  ; // reduce pessimism
   *  1   ?   ?   ?  ?   : ?  :  -  ; // no changes when in switches
   *  ?   ?   ?   0  ?   : ?  :  -  ; // no changes when in switches
   ?  ?   ?   0   ?  ?   : ?  :  1  ; // set output
   ?  1   1   *   ?  ?   : 1  :  1  ; // cover all transistions on set_
   ?  ?   1   *   0  ?   : 1  :  1  ; // cover all transistions on set_
   1  ?   1   *   ?  ?   : 1  :  1  ; // cover all transistions on set_
   ?  ?   0   1   ?  ?   : ?  :  0  ; // reset output
   ?  1   *   1   ?  ?   : 0  :  0  ; // cover all transistions on clr_
   ?  ?   *   1   0  ?   : 0  :  0  ; // cover all transistions on clr_
   0  ?   *   1   ?  ?   : 0  :  0  ; // cover all transistions on clr_

   endtable
endprimitive // udp_etlat
