-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

--testPlan Ver. 3.00
--19.1.15 : type_conversion function To_StdULogic : 

library IEEE;
use IEEE.std_logic_1164.all;

entity type_conv2_bv is
 port ( in1 : bit;
		in2 : bit;
		op1 : out std_ulogic;
		op2 : out std_ulogic);
end type_conv2_bv;

 architecture type_conv2_bv of type_conv2_bv is
  constant con1 :std_ulogic:= '1';
 begin
  op1 <= con1 and to_stdulogic(in1);
  op2 <= con1 xor to_stdulogic(in2);
 end type_conv2_bv;


