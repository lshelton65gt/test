// Testdriver test for a SystemVerilog design with ports that have
// 0 packed dimensions, and 1 unpacked dimension 
// on ports.

module top (
input a [3:0],
output b [3:0]
);

assign b[3] = ~a[3];
assign b[2] = ~a[2];
assign b[1] = ~a[1];
assign b[0] = ~a[0];

endmodule
