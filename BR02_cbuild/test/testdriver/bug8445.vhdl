library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bug8445 is
  port (
    en : in std_logic_vector (2 downto 0);
    data : in std_logic_vector (2 downto 0);
    io : inout std_logic_vector (2 downto 0);
    o : out std_logic_vector (2 downto 0)
    );
end bug8445;


architecture arch of bug8445 is
component sub
  port (
    en : in std_logic;
    data : in std_logic;
    io : inout std_logic;
    o : out std_logic
    );
end component;
begin

    sub0 : sub
    port map (
      en => en(0),
      data => data(0),
      io => io(0),
      o => o(0)
      );

    sub1 : sub
    port map (
      en => en(1),
      data => data(1),
      io => io(1),
      o => o(1)
      );

    sub2 : sub
    port map (
      en => en(2),
      data => data(2),
      io => io(2),
      o => o(2)
      );
end arch;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity sub is
  port (
    en : in std_logic;
    data : in std_logic;
    io : inout std_logic;
    o : out std_logic
    );
end sub;

architecture arch of sub is

begin

  io <= data when (en = '1') else 'Z';
  o <= io;
  
end arch;
