// Verify testdriver support for a nested observed signal.
module bottom (
input wire clk,
input wire reset,
input byte a [3:0],
output byte b [3:0],
output byte inc
);

byte foo; // carbon observeSignal

always @(posedge clk) 
begin
   if (reset == 1)
   begin
      foo = 0;     
      b[3] = 0;
      b[2] = 0;
      b[1] = 0;
      b[0] = 0;
   end
   else
   begin
      b[3] = ~a[3];
      b[2] = ~a[2];
      b[1] = ~a[1];
      b[0] = ~a[0];
      foo = a[3] + a[2] + a[1] + a[0];
   end
end

assign inc = foo + 1;

endmodule

module top (
input wire clk,
input wire reset,
input byte a [3:0],
output byte b [3:0],
output byte inc
);

bottom i1 (.clk(clk), .reset(reset), .a(a), .b(b), .inc(inc));

endmodule
