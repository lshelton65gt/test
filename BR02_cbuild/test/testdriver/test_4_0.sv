module top (
input bit [0:0][1:0][3:2][5:4] a,
output bit [0:0][1:0][3:2][5:4] b
);

assign b = ~a;

endmodule
