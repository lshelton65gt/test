library ieee;
use ieee.std_logic_1164.all;

entity case_in_func1 is
 port ( op : std_logic_vector(1 downto 0);
         in1 : integer range 1000 downto -100;
         in2 : integer range 1000 downto -100;
         output : out integer
       );
end;

architecture case_in_func1 of case_in_func1 is
 function func
   ( signal op  : std_logic_vector(1 downto 0);
     signal in1 : integer range 1000 downto -100;
     signal in2 : integer range 1000 downto -100
   ) return integer is
 begin
   case op is
    when "00" => return in1 + in2 ;
    when "01" => return in1 - in2 ; 
    when "10" => return in1 * in2;
    when "11" => return in1 / 2 ;
    when others => return 0;
   end case ;
 end ;
begin
  output <= func(op,in1,in2);
end ;
    
