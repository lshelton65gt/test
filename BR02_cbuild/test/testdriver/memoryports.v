module memoryports(clock, in1, in20, in21, in22, in23, out0);
   input        clock;
   input  [7:0] in1, in20, in21, in22, in23;
   output [7:0] out0;  
   reg    [7:0] out0;
   
   reg    [7:0] out1;            // carbon observeSignal
   reg    [7:0] out2 [3:0];      // carbon observeSignal
   reg    [7:0] out3 [3:0][3:0]; // carbon observeSignal
 
   always @(posedge clock)
     begin: main
        out0       = in1;
        out1       = in1;
	out2[0]    = in1 & in20;
        out2[1]    = in1 & in21;
        out2[2]    = in1 & in22; 
        out2[3]    = in1 & in23;
        out3[0][0] = in1 & in20;
        out3[0][1] = in1 & in21;
        out3[0][2] = in1 & in22;
        out3[0][3] = in1 & in23;
        out3[1][0] = in1 & in21;
        out3[1][1] = in1 & in22;
        out3[1][2] = in1 & in23;
        out3[1][3] = in1 & in20;
        out3[2][0] = in1 & in22;
        out3[2][1] = in1 & in23;
        out3[2][2] = in1 & in20;
        out3[2][3] = in1 & in21;
        out3[3][0] = in1 & in23;
        out3[3][1] = in1 & in20;
        out3[3][2] = in1 & in21;
        out3[3][3] = in1 & in22;	
     end
endmodule
