library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity slvtop is
  
  port (
    int1 : in  std_logic_vector(31 downto 0);
    int2 : out std_logic_vector(31 downto 0));

end slvtop;

architecture a of slvtop is
  signal s : signed(31 downto 0);
begin

  s <= signed(int1) + 42;
  int2 <= std_logic_vector(s);

end architecture;
