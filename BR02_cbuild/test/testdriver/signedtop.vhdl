library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

entity signedtop is
  
  port (
    iNt1 : in  signed(31 downto 0);
    inT2 : out signed(31 downto 0));

end signedtop;

architecture a of signedtop is

begin

  int2 <= int1 + 42;

end architecture;
