entity inttop is
  
  port (
    int1 : in  integer;
    int2 : out integer);

end inttop;

architecture a of inttop is

begin

  int2 <= int1 + 42;

end architecture;
