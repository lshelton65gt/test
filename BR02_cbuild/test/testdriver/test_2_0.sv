module top (
input bit [1:0][2:1] a,
output bit [1:0][2:1] b
);

assign b = ~a;

endmodule
