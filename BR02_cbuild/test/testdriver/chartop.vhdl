-- Test passing a record to a function (output variable test)
library ieee;
use ieee.std_logic_1164.all;

package p is
  type rec_char is
    record
      r2 : std_logic_vector(3 downto 0);
      r3 : std_logic;
      ch1,ch2 : character;
    end record;
end p;

library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity chartop_dut is
  port ( recinport : in rec_char
         ; recoutport1: out rec_char
         ; recoutport2: out rec_char
         ; clk: in std_logic
         );
end chartop_dut;

architecture a of chartop_dut is
  signal sigrec  : rec_char := ( "0000",'0','x','x');
  signal sigrec2 : rec_char := ( "0000",'0','x','x');

  procedure proc ( variable outrec : out rec_char;
                   variable o1 : out std_logic;
                   inrec : rec_char;
                   b1, b2 : in std_logic ) is
  begin
    o1 := b1 or b2;
    outrec.r2 := inrec.r2(1 downto 0)& inrec.r2(3 downto 2);
    outrec.r3 := inrec.r3 and (b1 or b2);
    outrec.ch2 := inrec.ch1;
    outrec.ch1 := inrec.ch2;
  end proc;


  begin

  p1: process (clk)
    -- note: the temps are here because we can't use signals in
    -- procedure output ports yet; this test was converted from using
    -- signals to variables.
    variable temp1, temp2 : rec_char;
    variable to1, to2 : std_logic;
    variable b1, b2 : bit;
--    variable bv : bit_vector(7 downto 0);

  begin
    if clk'event and clk = '1' then
      proc( temp1, to1, recinport, b2 => recinport.r3, b1 => not recinport.r3 );
      recoutport1 <= temp1;
      recoutport1.r3 <= to1;

      proc( temp2, to2, (temp1.r2,
                         recinport.r3 and temp1.r3,
                         recinport.ch2,
                         recinport.ch1),
            temp1.r3,
            recinport.r3);
      
      sigrec <= temp2;
      recoutport2 <= sigrec;
      recoutport2.r3 <= to2;
    end if;
  end process p1;
end a;


library ieee;
use ieee.std_logic_1164.all;
use work.p.all;

entity chartop is
  port (
    clk            : in  std_logic;
    recinport_r2   : in  std_logic_vector(3 downto 0);
    recinport_r3   : in  std_logic;
    recinport_ch1  : in  character;
    recinport_ch2  : in  character;
    recoutport1_r2   : out  std_logic_vector(3 downto 0);
    recoutport1_r3   : out  std_logic;
    recoutport1_ch1  : out  character;
    recoutport1_ch2  : out  character;
    recoutport2_r2   : out  std_logic_vector(3 downto 0);
    recoutport2_r3   : out  std_logic;
    recoutport2_ch1  : out  character;
    recoutport2_ch2  : out  character);
end chartop;

architecture a of chartop is
  component chartop_dut is
                          port ( recinport : in rec_char
                                 ; recoutport1: out rec_char
                                 ; recoutport2: out rec_char
                                 ; clk: in std_logic
                                 );
  end component chartop_dut;

begin

  dut : chartop_dut port map (
    clk              => clk,
    recinport.r2     => recinport_r2,
    recinport.r3     => recinport_r3,
    recinport.ch1    => recinport_ch1,
    recinport.ch2    => recinport_ch2,
    recoutport1.r2   => recoutport1_r2,
    recoutport1.r3   => recoutport1_r3,
    recoutport1.ch1  => recoutport1_ch1,
    recoutport1.ch2  => recoutport1_ch2,
    recoutport2.r2   => recoutport2_r2,
    recoutport2.r3   => recoutport2_r3,
    recoutport2.ch1  => recoutport2_ch1,
    recoutport2.ch2  => recoutport2_ch2);
end a;
