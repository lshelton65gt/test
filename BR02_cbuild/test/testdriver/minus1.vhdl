-- Copyright (c) 2002 Interra Systems, Inc.
-- Use, disclosure or distribution is prohibited without
-- prior written permission of Interra Systems, Inc.

--19.10.16.1 of TestPlan
--"-" operator (signed, std_ulogic return signed)

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity minus1 is
 port ( in1 : signed(1 to 1);
		in2 : std_ulogic;
		op1 : out signed(1 to 1));
end minus1;

 architecture minus1 of minus1 is
 begin
  op1 <= in1 - in2;
 end;
