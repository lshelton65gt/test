// Test for multiple unpacked dimensions on a port
module top (
input bit [1:0] a [7:8][4:3][2:1],
output bit [1:0] b [7:8][4:3][2:1]
);

assign b = a;

endmodule
