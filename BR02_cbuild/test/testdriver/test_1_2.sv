module top (
input bit [1:0] a [4:3][2:1],
input logic din,
output bit [1:0] b [4:3][2:1],
output logic dout
);


assign b[4][2] = ~a[4][2];
assign b[4][1] = ~a[4][1];
assign b[3][2] = ~a[3][2];
assign b[3][1] = ~a[3][1];
assign dout = ~din;

endmodule
