library ieee;
use ieee.std_logic_1164.all;

package base is

  subtype bits is UX01 range '0' to '1'; -- derived from IEEE UX01

  type UX10 is ( 'U', 'X', '1', '0'); 

  subtype bits2 is UX10 range '1' to '0';

  function To_Bits2 (in1 : std_logic_vector(0 to 1)) return bits2;
  function To_StdLogicVector (in1 : bits2) return std_logic_vector;  

end base;

package body base is

  function To_Bits2 (in1 : std_logic_vector(0 to 1)) return bits2 is
  begin  -- To_Bits
    case in1 is
      when "10" => return '1';
      when "11" => return '0';
      when others => null;
    end case;
    return '0';    
  end To_Bits2;

  function To_StdLogicVector (in1 : bits2) return std_logic_vector is
  begin  -- To_StdLogicVector
    case in1 is
      when '0' => return "11";
      when '1' => return "10";
      when others => null;
    end case;
    return "00";
  end To_StdLogicVector;

end base;

library ieee;
use ieee.std_logic_1164.all;
use work.base.all;

entity top is
  
  port ( pin  : in  bits;
         pin2 : in  bits2;
         pout : out bits;
         pout2 : out bits2);

end top;

architecture toparch of top is

begin  -- toparch

  pout <= pin;
  pout2 <= pin2;

end toparch;

