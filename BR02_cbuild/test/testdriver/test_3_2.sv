module top (
input  bit [0:1][2:1][5:6] a [4:3][7:8],
output bit [0:1][2:1][5:6] b [4:3][7:8]
);


assign b[4][7] = ~a[4][7];
assign b[3][7] = ~a[3][7];
assign b[4][8] = ~a[4][8];
assign b[3][8] = ~a[3][8];

endmodule
