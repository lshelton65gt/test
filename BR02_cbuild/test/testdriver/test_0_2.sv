// Testdriver test for a SystemVerilog design with ports that have
// 0 packed dimensions, and 2 unpacked dimension 
// on ports.
module top (
input logic a [1:0][2:1],
output logic b [1:0][2:1]
);


assign b[1][2] = ~a[1][2];
assign b[1][1] = ~a[1][1];
assign b[0][2] = ~a[0][2];
assign b[0][1] = ~a[0][1];

endmodule
