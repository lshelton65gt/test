-- This test tests bits on primary inputs and outputs

entity bittop is
  
  port (
    in1, in2 : in bit;
    out1 : out bit);

end bittop;

architecture a of bittop is
begin
  out1 <= in1 and in2;

end architecture;
