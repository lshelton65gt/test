entity buf is
  
  port (
    in1  : in     bit;
    out1 : buffer bit);

end buf;


architecture arch of buf is

begin

  out1 <= out1 and in1;

end arch;



