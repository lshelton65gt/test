entity intrange is
  
  port (
    in1 : in integer range 7 to 43;
    in2 : in integer range -7 to 9;
    in3 : in integer;
    in4 : in positive range 1 to 3;
    in5 : in positive;
    in6 : in natural;
    in7 : in natural range 11 to 15;
    out1 : out integer);

end intrange;


architecture arch of intrange is

begin

  out1 <= in1 + in2 + in3 + in4 + in5 + in6 + in7;

end arch;
