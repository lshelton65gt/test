// Testdriver test for a SystemVerilog design with ports that have
// 1 packed dimension, and 1 unpacked dimension 
// on ports.

module top (
input wire [7:0] a [3:0],
output reg [7:0] b [3:0]
);


always @(*) begin
   b[3] = ~a[3];
   b[2] = ~a[2];
   b[1] = ~a[1];
   b[0] = ~a[0];
end

endmodule
