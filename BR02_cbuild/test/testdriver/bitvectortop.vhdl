library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_bit.all;

entity bitvectortop is
  port (
    int1 : in  bit_vector(31 downto 0);
    int2 : out bit_vector(31 downto 0));
end bitvectortop;

architecture a of bitvectortop is
  signal s : signed(31 downto 0);
begin

  s <= signed(int1) + 42;
  int2 <= bit_vector(s);

end architecture;
