-- filename: test/testdriver/bug8453_03.vhdl
-- Description:  This test demonstrates a runtime problem with testdriver when
-- run with -disableCompositeTypeDump
-- the runtime error is:
-- Error 5042: bug8453_03.s_indexarray is a memory and not a design net. A design net name is expected.
-- design.exe: tmp.main.cxx:3474: int main(int, char**): Assertion `m_bug8453_03_s_indexarray' failed.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg is
  type     t_DataArray is array(natural range <>) OF std_ulogic_vector(18 - 1 downto 0);
  type     t_IndexArray is array(natural range <>) OF natural range 0 to 31;
end pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkg.all;

entity bug8453_03 is
  port (
    clock    : in  std_logic;
    in1, in2, in3, in4 : in  std_ulogic_vector(18 - 1 downto 0);
    s_IndexArray : in  t_IndexArray(0 to 3);
    sel : in std_ulogic_vector(2 downto 0);
    dataArray : buffer t_DataArray(0 to 27);
    out1     : out natural range 0 to 4);
end;

architecture arch of bug8453_03 is
  signal s_WtrMarkDelta : natural range 0 to 4;

begin
  main: process (clock)
    variable v_count   : natural range 0 to 4;
  begin
    if clock'event and clock = '1' then 
      v_count         := 0;
      dataArray(s_IndexArray(0)) <= in1;
      dataArray(s_IndexArray(1)) <= in2;
      dataArray(s_IndexArray(2)) <= in3;
      dataArray(s_IndexArray(3)) <= in4;
      case sel is
        when "001"  =>
          for i in 0 to 3 loop
            if (signed(dataArray(s_IndexArray(i))(18 - 1 downto 18 - 2)) /= 0 and
                signed(dataArray(s_IndexArray(i))(18 - 1 downto 18 - 2)) /= - 1)
            then
              v_count := v_count + 1;
            end if;
          end loop;
        when "010"  =>
          for i in 0 to 3 loop
            if (signed(dataArray(s_IndexArray(i))(18 - 1 downto 18 - 3)) /= 0 and
                signed(dataArray(s_IndexArray(i))(18 - 1 downto 18 - 3)) /= - 1)
            then
              v_count := v_count + 1;
            end if;
          end loop;
        when others =>                  -- "000"  => 
          v_count     := 0;
      end case;
      s_WtrMarkDelta <= v_count;
      

      out1 <= s_WtrMarkDelta;
    end if;
  end process;
end;
