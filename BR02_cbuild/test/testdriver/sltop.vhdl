-- This test tests single-bit std_logic on primary inputs and outputs

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sltop is
  
  port (
    in1, in2 : in std_logic;
    out1 : out std_logic);

end sltop;

architecture a of sltop is
begin
  out1 <= in1 and in2;

end architecture;
