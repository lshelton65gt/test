module top (
input bit [1:0][2:1] a [4:3],
output bit [1:0][2:1] b [4:3]
);


assign b[4] = ~a[4];
assign b[3] = ~a[3];

endmodule
