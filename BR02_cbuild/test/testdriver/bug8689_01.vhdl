-- Test for array of record which contains a record field
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bug8689_01 is
  port (sel : in std_logic_vector(1 downto 0);
        fsel : in std_logic_vector(1 downto 0);
        out1: out std_logic_vector(4 downto 0));
end bug8689_01;

architecture rtl of bug8689_01 is
  constant const1 : integer := 32;   
  constant const2 : integer := 4; 

  subtype fint is integer range 0 to const1-1;
  
  type dtype is record
                  f11 : fint;
                  f12 : fint;
                end record;

  type ctype is record
                  f1 : fint;
                  f2 : fint;
                  f3 : dtype;
                end record;

  type sconfig is array (Natural Range <> ) of ctype;

  constant c1 : ctype := (4, 8, (12, 16));
  constant c2 : sconfig(0 to const2-1) := ((1, 5, (9, 13)), (2, 6, (10, 14)), (3, 7, (11, 15)), others => c1);
  
  signal sig1 : sconfig(0 to const2-1);  -- carbon observeSignal
begin

  sig1 <= c2;

  process (fsel, sel, sig1)
    variable lsel : integer;
    variable lfsel : integer;
    variable lout : fint;
  begin
    lfsel := to_integer(unsigned(fsel));
    lsel := to_integer(unsigned(sel));
    case lfsel is
      when 0 => lout := sig1(lsel).f1;
      when 1 => lout := sig1(lsel).f2;
      when 2 => lout := sig1(lsel).f3.f11;
      when 3 => lout := sig1(lsel).f3.f12;
      when others => lout := 0;
    end case;
    out1 <= std_logic_vector(to_unsigned(lout, out1'length)); 
  end process;
end rtl;
