module top(clk, we, addr, in, out);
input clk, we, addr, in;
output out;

wire clk;
wire we;
wire [1:0] addr;
wire [7:0] in;
wire [7:0] out;

reg [7:0] mem [0:3];

always @(posedge clk)
begin
  if (we)
  begin
`ifdef NO_ARRAYSEL
    mem[addr] = { in[3:0], in[7:4] };
`else
    // we should be able to collapse this to mem[addr] = {in[3:0],in[7;4]};
    mem[addr][3:0] = in[7:4];
    mem[addr][7:4] = in[3:0];
`endif
  end
end

assign out = mem[addr];

endmodule
