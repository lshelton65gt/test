module top(clk, row, bit, we, in, out);
input clk, row, bit, we, in;
output out;

wire clk;
wire [3:0] row;
wire [2:0] bit;
wire we;
wire in;
wire out;

reg [7:0] mem [0:15]; // memory of 16 bytes

`ifdef NO_ARRAYSEL
reg [7:0] temp;
`endif

always@(posedge clk)
begin
  if (we)
  begin
`ifdef NO_ARRAYSEL
    temp = mem[row];
    temp[bit] = in;
    mem[row] = temp;
`else
    mem[row][bit] = in;
`endif
  end
end

assign out = (mem[row] >> bit) & 1'b1;

endmodule
