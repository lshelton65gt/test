module top(clk, row, bit, we, in, out);
input clk, row, bit, we, in;
output out;

wire clk;
wire [3:0] row;
wire [2:0] bit;
wire we;
wire in;
wire out;

reg [7:0] mema [0:15]; // memory of 16 bytes
reg [7:0] memb [0:15]; // memory of 16 bytes

reg [7:0] temp;

always@(posedge clk)
begin
  if (we)
  begin
`ifdef NO_ARRAYSEL
    temp = mema[row];
    temp[bit] = in;
    mema[row] = temp;
`else
    mema[row][bit] = in;
`endif
    temp = memb[row];
    temp[bit] = in;
    memb[row] = temp;
  end
  if (mema[row] != memb[row])
    $display("Miscompare mema (%h) vs. memb (%h) at addr=%d", mema[row], memb[row], row);
end

`ifdef NO_ARRAYSEL
assign out = (mema[row] >> bit) & 1'b1;
`else
assign out = mema[row][bit];
`endif

endmodule
