module top(clk, in, sel, out);
input clk, in, sel;
output out;

wire clk;
wire [3:0] in;
wire [1:0] sel;
wire [3:0] out;

reg [3:0] mem [3:0];

`ifdef NO_ARRAYSEL
reg [3:0] temp;
`endif

always@(posedge clk)
begin
`ifdef NO_ARRAYSEL
  temp = mem[sel];
  temp[sel] = in[sel];
  mem[sel] = temp;
`else
  // case stmt is equivalent to mem[sel][sel] = in[sel];
  case(sel)
   0: mem[0][0] = in[0];
   1: mem[1][1] = in[1];
   2: mem[2][2] = in[2];
   3: mem[3][3] = in[3];
  endcase
`endif
end

assign out = mem[sel];

endmodule
