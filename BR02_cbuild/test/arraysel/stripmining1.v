module top(clk, addr, byte, in, out);
input clk, addr, byte, in;
output out;

wire [3:0] addr;
wire [4:0] byte;
wire [7:0] in;
wire [7:0] out;

reg [127:0] mem [0:7];

reg [127:0] mask;
reg [127:0] data;

always @(posedge clk)
begin
  // equivalent to mem[byte*8 +: 8] = in;
  mask = 8'hff << (byte * 8);
  data = in << (byte * 8);
  mem[addr] = ((~mask) & mem[addr]) | (mask & data);
end

assign out = mem[addr][byte*8 +: 8];

endmodule
