module top(clk, bit, we, in, out);
input clk, bit, we, in;
output out;

wire clk;
wire [4:0] bit;
wire we;
wire in;
wire out;

reg [0:31] vec0;
reg [31:0] vec1;
reg [0:31] vec2;
reg [31:0] vec3;

reg [31:0] temp;

initial
begin
  vec0 = 0;
  vec1 = 0;
  vec2 = 0;
  vec3 = 0;
end

always @(posedge clk)
begin
  if (we)
  begin
    temp = vec0;
    temp[31-bit] = in;
    vec0 = temp;

    temp = vec1;
    temp[bit] = in;
    vec1 = temp;

    vec2[bit] = in;

    vec3[bit] = in;
  end
  // vec0[bit] is a common-sub-expression that should be identified
  if (vec0[bit] != vec1[bit])
    $display("Miscompare vec0 (%b) vs. vec1 (%b) at bit=%d", vec0[bit], vec1[bit], bit);
  if (vec0[bit] != vec2[bit])
    $display("Miscompare vec0 (%b) vs. vec2 (%b) at bit=%d", vec0[bit], vec2[bit], bit);
  if (vec0[bit] != vec3[bit])
    $display("Miscompare vec0 (%b) vs. vec3 (%b) at bit=%d", vec0[bit], vec3[bit], bit);
end

assign out = vec0[bit];

endmodule
