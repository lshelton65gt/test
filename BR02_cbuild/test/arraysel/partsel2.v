module top(clk, row, byte, we, in, out);
input clk, row, byte, we, in;
output out;

wire clk;
wire [3:0] row;
wire [1:0] byte;
wire we;
wire [7:0] in;
wire [7:0] out;

reg [31:0] mema [0:15];  // memory of 16 32-bit words
reg [31:0] memb [0:15];  // memory of 16 32-bit words

reg [31:0] temp;

always@(posedge clk)
begin
  if (we)
  begin
`ifdef NO_ARRAYSEL
    temp = mema[row];
    temp[8*byte +: 8] = in;
    mema[row] = temp;
`else
    mema[row][8*byte +: 8] = in;
`endif
    temp = memb[row];
    temp[8*byte +: 8] = in;
    memb[row] = temp;
  end
`ifdef NO_ARRAYSEL
  temp = memb[row];
`else
  // This should be recognized and folded into a single row access
  temp = {memb[row][31:24],memb[row][23:16],memb[row][15:8],memb[row][7:0]};
`endif
  if (mema[row] != temp)
    $display("Miscompare mema (%h) vs. memb (%h) at addr=%d", mema[row], temp, row);
end

`ifdef NO_ARRAYSEL
assign out = (mema[row] >> (8 * byte)) & 8'hff;
`else
assign out = mema[row][8*byte +: 8];
`endif

endmodule
