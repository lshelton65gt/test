module top(clk, in, addr, sel, out);
input clk, in, addr, sel;
output out;

wire clk;
wire [3:0] in;
wire [2:0] addr;
wire [1:0] sel;
reg  [3:0] out;

reg [3:0] mem [0:7];

`ifdef NO_ARRAYSEL
reg [3:0] temp;
`endif

always@(posedge clk)
begin
`ifdef NO_ARRAYSEL
  temp = mem[addr];
  out = temp[sel];
`else
  // should be able to infer out = mem[addr][sel]
  case(sel)
   0: out = mem[addr][0];
   1: out = mem[addr][1];
   2: out = mem[addr][2];
   3: out = mem[addr][3];
  endcase
`endif
end

always @(negedge clk)
begin
  mem[addr] = sel;
end

endmodule
