module top(clk, in, addr, sel, out);
input clk, in, addr, sel;
output out;

wire clk;
wire [7:0] in;
wire [2:0] addr;
wire [1:0] sel;
wire [7:0] out;

reg [31:0] mem [0:7];
reg [31:0] mask;

always@(posedge clk)
begin
  // should be able to infer mem[addr][sel*8 :+ 8] = in;
  mask = (8'hff << (sel*8));
  mem[addr] = (~mask & mem[addr]) | (in << (sel*8));
end

assign out = mem[addr];

endmodule
