module top(clk, reset, out);
input clk, reset;
output out;

wire clk;
wire reset;
wire [3:0] out;

reg [3:0] addr;
reg [3:0] last;
initial
begin
  addr = 0;
  last = 0;
end

reg [3:0] mem [0:15];
integer i;

always @(posedge clk)
begin
  if (reset)
  begin
`ifdef NO_ARRAYSEL
    mem[addr] = 4'h0;
`else
    for (i=0; i<=3; i=i+1)
    begin
      mem[addr][i] = 1'b0;
    end
`endif
  end
  else
  begin
    last <= mem[addr];
    mem[addr] = last + addr + 1;    
    addr = last;
  end
end

assign out = mem[addr];

endmodule
