module top(clk, in, addr, sel, out);
input clk, in, addr, sel;
output out;

wire clk;
wire [3:0] in;
wire [2:0] addr;
wire [1:0] sel;
wire [3:0] out;

reg [3:0] mem [0:7];

reg [3:0] temp;

always@(posedge clk)
begin
  // should be able to infer mem[addr][sel] = in[sel]
  // and eliminate the temp
  temp = mem[addr];
  temp[sel] = in[sel];
  mem[addr] = temp;
end

assign out = mem[addr];

endmodule
