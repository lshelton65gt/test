module top(clk, row, byte, we, in, out);
input clk, row, byte, we, in;
output out;

wire clk;
wire [3:0] row;
wire [7:0] byte;
wire we;
wire [7:0] in;
wire [7:0] out;

reg [2047:0] mema [0:15]; // memory of 16 256-byte words
reg [2047:0] memb [0:15]; // memory of 16 256-byte words

reg [2047:0] temp;

always@(posedge clk)
begin
  if (we)
  begin
`ifdef NO_ARRAYSEL
    temp = mema[row];
    temp[8*byte +: 8] = in;
    mema[row] = temp;
`else
    mema[row][8*byte +: 8] = in;
`endif
    temp = memb[row];
    temp[8*byte +: 8] = in;
    memb[row] = temp;
  end
  if (mema[row] != memb[row])
    $display("Miscompare mema (%h) vs. memb (%h) at addr=%d", mema[row], memb[row], row);
end

`ifdef NO_ARRAYSEL
assign out = (mema[row] >> (8*byte)) & 8'hff;
`else
assign out = mema[row][8*byte +: 8];
`endif

endmodule
