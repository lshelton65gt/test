module top(clk, addr, bit, we, in, out);
input clk, addr, bit, we, in;
output out;

wire clk;
wire [7:0] addr;
wire [4:0] bit;
wire we;
wire in;
wire out;

reg [0:31] mema [0:255];
reg [31:0] memb [0:255];
reg [0:31] memc [255:0];
reg [31:0] memd [255:0];

reg [31:0] temp;

always @(posedge clk)
begin
  if (we)
  begin
    temp = mema[addr];
    temp[31-bit] = in;
    mema[addr] = temp;

    temp = memb[addr];
    temp[bit] = in;
    memb[addr] = temp;

    memc[addr][bit] = in;

    memd[addr][bit] = in;
  end
  // mema[addr][bit] is a common-sub-expression that should be identified
  if (mema[addr][bit] != memb[addr][bit])
    $display("Miscompare mema (%b) vs. memb (%b) at addr=%d bit=%d", mema[addr][bit], memb[addr][bit], addr, bit);
  if (mema[addr][bit] != memc[addr][bit])
    $display("Miscompare mema (%b) vs. memc (%b) at addr=%d bit=%d", mema[addr][bit], memc[addr][bit], addr, bit);
  if (mema[addr][bit] != memd[addr][bit])
    $display("Miscompare mema (%b) vs. memd (%b) at addr=%d bit=%d", mema[addr][bit], memd[addr][bit], addr, bit);
end

assign out = mema[addr][bit];

endmodule
