module top(clk, addr1, addr2, from, to, out);
input clk, addr1, addr2, from, to;
output out;

wire clk;
wire [3:0] addr1;
wire [3:0] addr2;
wire [1:0] from;
wire [1:0] to;
wire [3:0] out;

reg [3:0] mem [0:15];

wire out1;
wire out2;

`ifdef NO_ARRAYSEL
wire port1 = (mem[addr1] >> from) & 1'b1;
wire port2 = (mem[addr2] >> from) & 1'b1;
child C1(clk, port1, out1);
child C2(clk, port2, out2);
`else
child C1(clk, mem[addr1][from], out1);
child C2(clk, mem[addr2][from], out2);
`endif

always @(addr1 or out1)
  mem[addr1][to] = out1;

always @(addr2 or out2)
  mem[addr2][to] = out2;

assign out = mem[addr1];

endmodule

module child(clk, in, out);
input clk, in;
output out;

wire clk;
wire in;
reg out;

always @(posedge clk)
  out = ~in;

endmodule
