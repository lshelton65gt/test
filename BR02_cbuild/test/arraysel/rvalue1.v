module top(clk, row, bit, we, in, out);
input clk, row, bit, we, in;
output out;

wire clk;
wire [3:0] row;
wire [2:0] bit;
wire we;
wire in;
wire out;

reg [7:0] mem [0:15]; // memory of 16 bytes

reg [7:0] temp;

always@(posedge clk)
begin
  if (we)
  begin
    temp = mem[row];
    temp[bit] = in;
    mem[row] = temp;
  end
end

`ifdef NO_ARRAYSEL
assign out = (mem[row] >> bit) & 1'b1;
`else
assign out = mem[row][bit];
`endif

endmodule
