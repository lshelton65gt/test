module top(clk, in, addr, sel, out);
input clk, in, addr, sel;
output out;

wire clk;
wire in;
wire [2:0] addr;
wire [1:0] sel;
reg out;

reg [3:0] mem [0:7];

reg [3:0] temp;

always@(posedge clk)
begin
`ifdef NO_ARRAYSEL
  temp = mem[addr];
  temp[sel] = in;
  mem[addr] = temp;  
`else
  mem[addr][sel] = in;
`endif
end

always@(negedge clk)
begin
  // should be able to infer out = mem[addr][sel];
  temp = mem[addr];
  out = temp[sel];
end

endmodule
