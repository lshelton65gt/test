module top(clk, reset, out);
input clk, reset;
output out;

wire clk;
wire reset;
wire [3:0] out;

reg [3:0] addr;
reg [3:0] last;

reg [3:0] mem  [0:15];
reg [3:0] mem2 [0:15];

integer i;

initial
begin
  addr = 0;
  last = 0;
  for (i = 0; i < 16; i = i + 1) begin
    mem[i] = 4'b0;
    mem2[i] = 4'b0;
  end
end


always @(posedge clk)
begin
  if (reset)
  begin
`ifdef NO_ARRAYSEL
    mem[addr] = mem[addr] ^ mem2[addr];
    mem2[addr] = 4'hf;
`else
    for (i=0; i<=3; i=i+1)
    begin
      mem[addr][i] = mem[addr][i] ^ mem2[addr][i];
    end
    for (i=0; i<=3; i=i+1)
    begin
      mem2[addr][i] = 1'b1;
    end
`endif
  end
  else
  begin
    last <= mem[addr] ^ mem2[addr];
    mem[addr] = last + addr + 1;
    mem2[addr] = last - addr - 1;
    addr = last;
  end
end

assign out = mem[addr];

endmodule
