module top(clk, in, sel, out);
input clk, in, sel;
output out;

wire [7:0] in;
wire [2:0] sel;
reg  [7:0] state;

wire [7:0] out = state;

always@(posedge clk)
  state[sel] = ~in[sel];

endmodule

