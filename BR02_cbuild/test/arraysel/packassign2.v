module top(in1, in2, out);
input in1, in2;
output out;

wire [15:0] in1;
wire [15:0] in2;
wire [15:0] out;

wire [15:0] mem [0:1];

`ifdef NO_ARRAYSEL
assign mem[0] = in1;
`else
// this should be fully packed into assign mem[0] = in1;
assign mem[0][0] = in1[0];
assign mem[0][15] = in1[15];
assign mem[0][1] = in1[1];
assign mem[0][14:13] = in1[14:13];
assign mem[0][5:2] = in1[5:2];
assign mem[0][10] = in1[10];
assign mem[0][8] = in1[8];
assign mem[0][12:11] = in1[12:11];
assign mem[0][9] = in1[9];
assign mem[0][7] = in1[7];
assign mem[0][6] = in1[6];
`endif

`ifdef NO_ARRAYSEL
assign mem[1] = in2;
`else
// this should be fully packed into mem[1] = in2;
assign mem[1][7:3] = in2[7:3];
assign mem[1][15:14] = in2[15:14];
assign mem[1][1] = in2[1];
assign mem[1][13] = in2[13];
assign mem[1][0] = in2[0];
assign mem[1][12:9] = in2[12:9];
assign mem[1][8] = in2[8];
assign mem[1][2] = in2[2];
`endif

`ifdef NO_ARRAYSEL
assign out = mem[0] ^ mem[1];
`else
// this should be inferenced into out = mem[0] ^ mem[1];
assign out[0] = mem[0][0] ^ mem[1][0];
assign out[5:3] = mem[0][5:3] ^ mem[1][5:3];
assign out[2] = mem[0][2] ^ mem[1][2];
assign out[8] = mem[0][8] ^ mem[1][8];
assign out[7:6] = mem[0][7:6] ^ mem[1][7:6];
assign out[1] = mem[0][1] ^ mem[1][1];
assign out[15:9] = mem[0][15:9] ^ mem[1][15:9];
`endif

endmodule
