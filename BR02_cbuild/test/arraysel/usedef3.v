module top(clk, reset, out);
input clk, reset;
output out;

wire clk;
wire reset;
wire [3:0] out;

reg [3:0] addr;
reg [3:0] last;
initial
begin
  addr = 0;
  last = 0;
end

reg [3:0] mem [0:15];
integer i;

always @(posedge clk)
begin
  if (reset)
  begin
`ifdef NO_ARRAYSEL
    mem[addr] = 4'hd;
    mem[addr ^ 1'b1] = 4'h3;
`else
    // These four assignments are NOT killed by the fifth
    mem[addr][0] = 1'b1;
    mem[addr][1] = ~mem[addr][0];
    mem[addr][2] = reset;
    mem[addr][3] = mem[addr][2];
    mem[addr ^ 1'b1][3:0] = 4'h3;
`endif
  end
  else
  begin
    last <= mem[addr];
    mem[addr] = last + addr + 1;    
    addr = last;
  end
end

assign out = mem[addr];

endmodule
