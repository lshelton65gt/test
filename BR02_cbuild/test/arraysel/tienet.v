module top(clk, in, addr, out);
input clk, in, addr;
output out;

wire clk;
wire [31:0] in;
wire [4:0] addr;
wire out;

sub S1 (clk, 32'h1234abcd, addr, out);

endmodule

module sub(clk, in, addr, out);
input clk, in, addr;
output out;

wire clk;
wire [31:0] in;
wire [4:0] addr;

reg out;

always @(posedge clk)
  out = in[addr];

endmodule
