module top(clk, row, bit, we, in, out);
input clk, row, bit, we, in;
output out;

wire clk;
wire [3:0] row;
wire [2:0] bit;
wire we;
wire in;
wire out;

reg [7:0] mema [0:15]; // memory of 16 bytes
reg [7:0] memb [0:15]; // memory of 16 bytes

reg [7:0] temp;

always@(posedge clk)
begin
  if (we)
  begin
`ifdef NO_ARRAYSEL
    temp = mema[row];
    temp[bit] = in;
    mema[row] = temp;
    temp = memb[row];
    temp[bit] = in;
    memb[row] = temp;
`else
    mema[row][bit] = in;
    memb[row][bit] = in;
`endif
  end
`ifdef NO_ARRAYSEL
  temp = memb[row];
`else
  // This should be recognized and folded into a single row access
  temp = {memb[row][7],memb[row][6],memb[row][5],memb[row][4],memb[row][3],memb[row][2],memb[row][1],memb[row][0]};
`endif
  if (mema[row] != temp)
    $display("Miscompare mema (%h) vs. memb (%h) at addr=%d", mema[row], temp, row);
end

`ifdef NO_ARRAYSEL
assign out = (mema[row] >> bit) & 1'b1;
`else
assign out = mema[row][bit];
`endif

endmodule
