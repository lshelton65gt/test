module identity(out,in);
   output [15:0] out;
   input [15:0]  in;
   assign 	 out = in;
endmodule

module top(a, b, out);
   input [7:0] a;
   output [7:0] b;
   output [15:0] out;
   
   assign 	 b = ~a;
   identity i0(out,{a,b});
endmodule
