// filename: test/coerce/bug12192_1.v
// Description:  This is a trimmed down version of bug12192.v
// observeSignal directive *.* is used on the top level clock caused it to be
// coerced to an inout


module bug12192_1 (vec);
   output [2:0]     vec; // carbon observeSignal

   mod2 I1(.vec     (vec));
   
endmodule


module  mod2 ( vec);
   output [2:0] vec;

   wire 	 vec_0;  // undriven
   wire 	 vec_1;  // undriven
   wire 	 vec_2; // undriven
   
   assign vec[0] = vec_0;
//   assign vec[1] = vec_1;  // at least one bit in the middle must be undriven
   assign vec[2] = vec_2;
   
endmodule

