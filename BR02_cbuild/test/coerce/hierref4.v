module top(out,read,en1,en2,in1,in2);
   output out; // should become an inout.
   output read;
   input  en1,en2;
   input  in1,in2;

   assign read = ~out;
   wrap w0(en1,in1);
   wrap w1(en2,in2);

endmodule

module wrap(en,in);
   input en,in;
   // hierarchical weak driver.
   assign top.out = en ? in : 1'bz;
endmodule
