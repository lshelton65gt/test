module top(o,io,en,data);
   output o; // coerce to inout.
   inout io;
   input en,data;
   sub sub(o,io,en,data);
   wrapo  wo();
   wrapio wio();
endmodule
module sub(o,io,en,data);
   output o; // coerce to inout.
   inout io;
   input en,data;

endmodule // sub

module wrapo();
   // weak hierarchical writer
   assign sub.o = sub.en ? sub.data : 1'bz;
endmodule

module wrapio();
   // weak hierarchical writer with hierarchical reader
   assign sub.io = sub.en ? sub.o : 1'bz;
endmodule
