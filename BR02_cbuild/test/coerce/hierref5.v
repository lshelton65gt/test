module top(out,en,in1,in2);
   output out; 
   input  en;
   input  in1; // should become inout (internal writer)
   input  in2;

   assign out = ~in1;
   wrap w0(en,in2);

endmodule

module wrap(en,in);
   input en,in;
   // weak hierarchical writer.
   assign top.in1 = en ? in : 1'bz;
endmodule
