module top(out,read,en1,en2,in1,in2);
   output out; // should become inout.
   output read;
   input  en1,en2;
   input  in1,in2;

   mid m0 (out, read, en1, en2, in1, in2);
endmodule
   
module mid(out,read,en1,en2,in1,in2);
   output out; // should become inout.
   output read;
   input  en1,en2;
   input  in1,in2;

   assign out = en1 ? in1 : 1'bz;

   sub s0 (out, read, en2, in2);
endmodule

module sub(out,read,en,in);
   output out; // needs to become inout.
   output read;
   input  en;
   input  in;

   assign read = ~out;
   assign out = en ? in : 1'bz;
endmodule
