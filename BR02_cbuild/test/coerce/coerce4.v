module top(out,read,en1,en2,in1,in2);
   output out; // should become an inout.
   output read;
   input  en1,en2;
   input  in1,in2;

   assign read = ~out;
   assign out = en1 ? in1 : 1'bz;
   assign out = en2 ? in2 : 1'bz;

endmodule
