module top(outb,outc,ena,enb,enc,a,b,c);
   output outb,outc;
   input  ena,enb,enc;
   input  a,b,c;

   wire   cross;
   wrap_a wa();
   wrap_b wb();
   wrap_c wc();
   
   level0 l0b(outb,cross,enb,b);
   level0 l0c(outc,cross,enc,c);
endmodule

module level0(out,cross,en,in);
   output out;
   input  cross;
   input  en,in;
   level1 l1(out,cross,en,in);
endmodule

module level1(out,cross,en,in);
   output out;
   input  cross;
   input  en,in;
   level2 l2(out,cross,en,in);
endmodule

module level2(out,cross,en,in);
   output out;
   input  cross;
   input  en,in;
   level3 l3(out,cross,en,in);
endmodule

module level3(out,cross,en,in);
   output out;
   input  cross;
   input  en,in;
   sub s0(out,cross,en,in);
endmodule

module sub(out,cross,en,in);
   output out;
   input  cross;
   input  en,in;
   assign cross = en ? in : 1'bz;
   assign out = ~cross;
endmodule

module wrap_a();
   assign top.cross = top.ena ? top.a : 1'bz;
endmodule

module wrap_b();
   assign l0b.l1.l2.l3.s0.cross = l0b.l1.l2.l3.s0.en ? l0b.l1.l2.l3.s0.in : 1'bz;
   assign l0b.l1.l2.l3.s0.out = ~l0b.l1.l2.l3.s0.cross;
endmodule

module wrap_c();
   assign l0c.l1.l2.l3.s0.cross = l0c.l1.l2.l3.s0.en ? l0c.l1.l2.l3.s0.in : 1'bz;
   assign l0c.l1.l2.l3.s0.out = ~l0c.l1.l2.l3.s0.cross;
endmodule
