// Hierarchical reference testcase for port coercion. In this example,
// all ports have been declared with the correct direction. No port
// coercion should occur.

module top(in,out);
   input in;
   output out;
   bfm bfm(.in(in),.out(top.a.data));
   a a(out);
endmodule

module bfm(in,out);
   input in;
   output out;
   assign out = ~in;
endmodule

module a(data);
   output data;
endmodule
