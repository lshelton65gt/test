module top(o,io,in,en,data);
   output o;
   inout io;
   input in; // coerce to inout.
   input en,data;
   sub sub(o,io,in,en,data);
endmodule

module sub(o,io,in,en,data);
   output o;
   inout io;
   input in; // coerce to inout.
   input en,data;

   wrapin win(en,data);
   wrapio wio(en);

   assign o = in & io;
endmodule // sub

module wrapin(en,in);
   input en,in;
   // weak hierarchical writer.
   assign top.sub.in = en ? in : 1'bz;
endmodule

module wrapio(en);
   input en;
   // weak hierarchical writer with hierarchical read.
   assign top.sub.io = en ? top.sub.in : 1'bz;
endmodule
