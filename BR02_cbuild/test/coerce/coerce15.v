module top(outb,outc,ena,enb,enc,a,b,c);
   output outb,outc;
   input  ena,enb,enc;
   input  a,b,c;
   
   wire cross = ena ? a : 1'bz;

   sub s0(outb,cross,enb,b);
   sub s1(outc,cross,enc,c);
endmodule

module sub(out,cross,en,in);
   output out;
   input  cross;
   input  en,in;
   assign cross = en ? in : 1'bz;
   assign out = ~cross;
endmodule
