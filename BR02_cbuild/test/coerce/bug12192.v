// filename: test/coerce/bug12192.v
// Description:  This test is from the attachment of bug 12192.
// the main problem is:
//  0. an observeSignal directive used on the top level net caused it to be
//     coerced to an inout (bug 12192)
// there are several additional problems here:
//  1. for some reason the in-line observeSignal on clk does not
//     demonstrate this problem but the same directive in an external file does
//     cause this problem. (bug 12821)
//  2. cbuild should report that clk_foo is undriven, or that clk[0] is
//  undriven. (bug 12823)


module      bug12192                     // TOP LEVEL
  (
   // Clk vector observed OK here.
    output wire [(12-1):0]     clk // carbon observeSignal 
   );
   tinyH_synt
     i_synt
       (
        // Outputs
        .clk     (clk[(12-1):0])  // Clock vector from tinyH_synt below
        //.....
	);
endmodule // bug12192

module tinyH_synt
  (
   //...
    output wire [(12-1):0]  clk);
   //...

   clkctrl     i_clkctrl
     (
      
      // Outputs
      .clk      (clk[(12-1):0])    // Clock vector comes from here...
      //..
      );

   cpuif_tinyH
     i_cpuif
       (                
			// ...  

			// Inputs
			.clk_cpu_c      (clk[11])    // Clock for the CPU/PC
			//...
			);
   foo
     foo
       (                
			// ...  

			// Inputs
			.clk      (clk[0])    // Clock for the CPU/PC
			//...
			);
endmodule // tinyH_synt


module cpuif_tinyH
  (
    input  wire    clk_cpu_c );// Marked CONSTANT in hierarchy browser.
   avr8L   i_core
     (
      // Inputs
      .clk_c        (clk_cpu_c)
      // ...
      );
endmodule //cpuif_tinyH

module avr8L
  (
    input  wire    clk_c  // Marked CONSTANT in hierarchy browser.
   // ...
   );

   avr8L_inst    i_inst(
			.clk_c     (clk_c)
      
			);
endmodule //avr8L

module foo
  (
    input  wire    clk  
   );
   reg [31:0] 	   out;
   
   
   always @(posedge clk) begin
      out <= out+32'd1; 
   end

endmodule

module avr8L_inst
  (
    input wire clk_c    // Marked CONSTANT in hierarchy browser.
   //...
   );
   reg [31:0]  pc; 
   
   //...
   always @(posedge clk_c) begin
      // Program counter manipulations here.
      pc <= pc+32'd1; 
   end

endmodule //avr8L_inst

module  clkctrl (output wire [(12-1):0] clk  
                 );

   wire clk_0;
   wire clk_11;
   
   wire clk_foo;

   assign clk_0 = clk_foo;
   assign clk[0] = clk_0;
   assign clk[11] = clk_11;
   
endmodule

