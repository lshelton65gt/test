module top(clk,in,out);
   input clk;
   input [5:0] in;
   output [5:0] out;
   reg [5:0] 	out;

   pullup p1 [5:0](in);
   
   always @(posedge clk)
     out <= in;
endmodule
