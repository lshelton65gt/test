module top(clk,in,out);
   input clk;
   input [5:0] in;
   output [5:0] out;
   reg [5:0] 	out;

   wrap w0();
   
   always @(posedge clk)
     out <= in;
endmodule

module wrap();
   // hierarchical pullup.
   pullup p1 [5:0](top.in);
endmodule
