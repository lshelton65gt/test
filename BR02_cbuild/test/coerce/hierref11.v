module top(o1,o2,en,a,b,c,d,e);
   output o1,o2; 
   input  en;
   input  a; // should become inout (submodule writer)
   input  b,c,d;
   input  e; // should become inout (submodule writer)

   // instance causes 'a' and 'sub.in1' to become inout.
   sub s0(.out(o1),.en(en),.in1(a),.in2(d));

   // instance with complex expression on coerced port (in1)
   sub s1(.out(o2),.en(en),.in1(b&c),.in2(d));

   // instance with disconnected coerced port (in1)
   sub s2(.out(o3),.en(en),.in1(),.in2(d));

   // instance with disconnected output
   sub s3(.out(),.en(en),.in1(e),.in2(d));
endmodule

module sub(out,en,in1,in2);
   output out;
   input  en;
   input  in1; // should become inout (internal writer)
   input  in2;
   
   assign out = ~in1;

   wrap w0(en,in2);
endmodule

module wrap(en,in);
   input en,in;
   // weak hierarchical writer.
   assign sub.in1 = en ? in : 1'bz;
endmodule
