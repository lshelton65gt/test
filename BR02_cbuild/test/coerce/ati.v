module io(vddci,o);
   inout vddci;
   output o;

   io_tt io_tt(vddci,o);
endmodule

module io_tt(vddci,o);
   inout vddci;
   output o;
   
   pad pad(vddci);
   buf_wrap buf_wrap(vddci,o);
endmodule

module buf_wrap(vddci,o);
   inout vddci;
   output o;

   my_buf my_buf(vddci,o);
endmodule

module my_buf(i,o);
   input i;
   output o;
   assign o = i;
endmodule

module pad(io);
   inout io;
endmodule
