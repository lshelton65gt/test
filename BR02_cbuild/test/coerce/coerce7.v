module top(out,en,in1,in2);
   output out; 
   input  en;
   input  in1; // should become inout (submodule writer)
   input  in2;

   left  l0(en,in1,in2);
   right r0(out,in1);
endmodule

module left(en,in1,in2);
   input en;
   input in1; // should become inout (internal writer)
   input in2;
   
   assign in1 = en ? in2 : 1'bz;
endmodule

module right(out,in);
   input in; // should remain input.
   output out;
   assign out = ~in;
endmodule
