module identity(out,in);
   output [15:0] out;
   input [15:0]  in;
   assign 	 out = in;
endmodule

module top(a, b, out);
   input [7:0] a;
   input [7:0] b;
   output [15:0] out;
   
   identity i0(out,{a,b});
endmodule
