module top(clk,in,out);
   input clk;
   input in;
   output out;

   x x0 (clk,in,out);
endmodule

module x(clk,in,out);
   input clk;
   input in;
   output out;

   y y0 (clk,in,out);
endmodule

module y(clk,in,out);
   input clk;
   input in;
   output out;

   z z0 (clk,in,out);
endmodule

module z(clk,in,out);
   input clk;
   input in;
   output out;
   reg 	  out;
   initial out = 0;
   always @(posedge clk)
     out <= out ^ in;

endmodule
