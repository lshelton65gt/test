module bigflop(clk,rst,wen,wdata,q);
   input clk;
   input rst;
   input [3:0] wen;
   input [3:0] wdata;
   output [3:0] q;

   // coercion needs to understand that bit-selects don't need sibling
   // propagation.
   
   flop f0(clk,rst,1'b0,wen[0],wdata[0],q[0]);
   flop f1(clk,rst,1'b1,wen[1],wdata[1],q[1]);
   flop f2(clk,rst,1'b0,wen[2],wdata[2],q[2]);
   flop f3(clk,rst,1'b1,wen[3],wdata[3],q[3]);
endmodule

module flop(clk,rst,rdata,wen,wdata,q);
   input clk;
   input rst;
   input rdata;
   input wen;
   input wdata;
   output q;
   reg 	  q;
   always @(posedge clk) begin
      if (rst) begin 
	 q = rdata;
      end else begin
	 if (wen) begin
	    q = wdata;
	 end else begin
	    q = q;
	 end
      end
   end
endmodule
