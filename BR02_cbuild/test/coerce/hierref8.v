module top(out,en,in1,in2);
   output out; 
   input  en;
   input  in1; // should become inout (submodule writer)
   input  in2;

   sub s0(out,en,in1,in2);
endmodule

module sub(out,en,in1,in2);
   output out;
   input  en;
   input  in1; // should become inout (internal writer)
   input  in2;
   
   assign out = ~in1;

   wrap w0(en,in2);
endmodule

module wrap(en,in);
   input en,in;
   assign top.s0.in1 = en ? in : 1'bz;
endmodule
