module top(i, o, en, o2);
  input i, en;
  output o, o2;
  wire w;
  child2 child2 (w, i, o, en);
  child1 child1 (w, o2);
endmodule

module child1(w, o2);
  output w;
  output o2;
  assign o2 = w;
endmodule

module child2(w, i, o, en);
  output w;
  input i, en;
  output o;
  assign o = w;
  assign w = en ? i : 'bz;
endmodule
