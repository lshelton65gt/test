module mod1 (
    ck
  , i_oe, i_ie
  , ot0 , in0, io0
  , ot1 , in1, io1
);
  input  ck;
  input  [31:0] i_oe, i_ie;
  reg    [31:0] oe,   ie;
  output ot0; input  in0, io0;
  output ot1; input  in1, io1;

  always @(posedge ck) begin
    oe <= i_oe;
    ie <= i_ie;
  end

  pad0 i0 (io0, ot0 , in0, oe[0], ie[0]);
  pad1 i1 (io1, ot1 , in1, oe[1], ie[1]);

endmodule

module pad0 ( io, ot , in , oe , ie );
  inout  io;
  output ot; input  in, oe, ie;
  supply0 gnd;
  bufif0  i1 (w_io, in  , oe );
   pmos    i2 (io  , w_io, gnd);
  and     i3 (ot  , ie  , io );
endmodule

module pad1 ( io, ot , in , oe , ie );
  inout  io;
  output ot; input  in, oe, ie;
  supply0 gnd;
  bufif0  i1 (io  , in  , oe );
  and     i3 (ot  , ie  , io );
endmodule // pad1
