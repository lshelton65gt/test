module top(out,en,in1,in2);
   output out; 
   input  en;
   input  in1; // should become inout (submodule writer)
   input  in2;

   assign out = ~in1;

   sub s0(en,in1,in2);
endmodule

module sub(en,in1,in2);
   input en;
   input in1; // should become inout (internal writer)
   input in2;
   
   assign in1 = en ? in2 : 1'bz;
endmodule
