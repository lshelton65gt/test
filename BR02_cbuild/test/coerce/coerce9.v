module top(o1,o2,en,in1,in2);
   output o1,o2; 
   input  en;
   input  in1; // should become inout (submodule writer)
   input  in2;

   left  l0(o1,en,in1,in2);
   right r0(o2,in1);
endmodule

module left(out,en,in1,in2);
   output out;
   input en;
   input in1; // should become inout (internal writer)
   input in2;
   
   assign in1 = en ? in2 : 1'bz;
   assign out = in1 & in2;
endmodule

module right(out,in);
   input in; // should remain input.
   output out;
   assign out = ~in;
endmodule
