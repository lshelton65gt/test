module top(i, o, en);
  input i, en;
  output o;
  wire w;
  child2 child2 (w, i, o, en);
  child1 child1 (w);
endmodule

module child1(w);
  output w;
  pullup(w);
endmodule

module child2(w, i, o, en);
  output w;
  input i, en;
  output o;
  assign o = w;
  assign w = en ? i : 'bz;
endmodule
