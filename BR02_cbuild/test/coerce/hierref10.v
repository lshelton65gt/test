module top(o1,o2,en,in1,in2);
   output o1,o2; 
   input  en;
   input  in1; // should become inout (submodule writer)
   input  in2;

   assign o1 = ~in1;

   sub s0(o2,en,in1,in2);
endmodule

module sub(out,en,in1,in2);
   output out;
   input en;
   input in1; // should become inout (internal writer)
   input in2;
   
   assign out = in1 & in2;

   wrap w0(en,in2);
endmodule

module wrap(en,in);
   input en,in;
   // weak hierarchical writer
   assign top.s0.in1 = en ? in : 1'bz;
endmodule
