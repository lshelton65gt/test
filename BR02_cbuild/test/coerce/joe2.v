module top(i, o, en1, en2);
  input i, en1, en2;
  output o;
  wire w;
  child1 child1 (w, i, en1);
  child2 child2 (w, i, o, en2);
endmodule

module child1(w, i, en);
  input i, en;
  output w;
  assign w = en ? i : 'bz;
endmodule

module child2(w, i, o, en);
  output w;
  input i, en;
  output o;
  assign o = w;
  assign w = en ? i : 'bz;
endmodule
