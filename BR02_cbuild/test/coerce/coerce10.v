module top(o1,o2,en,in1,in2);
   output o1,o2; 
   input  en;
   input  in1; // should become inout (submodule writer)
   input  in2;

   assign o1 = ~in1;

   sub s0(o2,en,in1,in2);
endmodule

module sub(out,en,in1,in2);
   output out;
   input en;
   input in1; // should become inout (internal writer)
   input in2;
   
   assign in1 = en ? in2 : 1'bz;
   assign out = in1 & in2;
endmodule
