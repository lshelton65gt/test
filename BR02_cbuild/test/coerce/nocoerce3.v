module top(out,z_out,in);
   output out;
   output z_out; // leave disconnected.
   input  in;
   assign out = ~in;
endmodule
