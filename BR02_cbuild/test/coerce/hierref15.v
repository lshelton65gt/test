module top(outb,outc,ena,enb,enc,a,b,c);
   output outb,outc;
   input  ena,enb,enc;
   input  a,b,c;
   
   wire   cross;
   wrap_a wa();
   
   sub s0(outb,cross,enb,b);
   sub s1(outc,cross,enc,c);
endmodule

module sub(out,cross,en,in);
   output out;
   input  cross;
   input  en,in;
   wrap_b wb();
endmodule

module wrap_a();
   assign top.cross = top.ena ? top.a : 1'bz;
endmodule

module wrap_b();
   assign sub.cross = sub.en ? sub.in : 1'bz;
   assign sub.out = ~sub.cross;
endmodule
