module top(o,io,en,data);
   output o; // coerce to inout.
   inout io;
   input en,data;
   sub sub(o,io,en,data);
endmodule
module sub(o,io,en,data);
   output o; // coerce to inout.
   inout io;
   input en,data;

   assign o = en ? data : 1'bz;
   assign io = en ? o : 1'bz;
endmodule // sub
