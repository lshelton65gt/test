// testcase for directive-controlled flattening.
module top(clk,in,out1,out2,out3,out4);
   input clk;
   input [15:0] in;
   output [15:0] out1,out2,out3,out4;

   flatten_into_top u0(clk,in,out1);
   flatten_children u1(clk,in,out2);
   flatten_normally u2(clk,in,out3);
   buffer16         u3(clk,in,out4);
endmodule

module flatten_into_top(clk,in,out);
   input clk;
   input [15:0] in;
   output [15:0] out;
   flop4 f0(clk,in[3:0],out[3:0]);
   flop4 f1(clk,in[7:4],out[7:4]);
   flop4 f2(clk,in[11:8],out[11:8]);
   flop4 f3(clk,in[15:12],out[15:12]);
endmodule

module flatten_children(clk,in,out);
   input clk;
   input [15:0] in;
   output [15:0] out;
   flop4 f0(clk,in[3:0],out[3:0]);
   flop4 f1(clk,in[7:4],out[7:4]);
   flop4 f2(clk,in[11:8],out[11:8]);
   flop4 f3(clk,in[15:12],out[15:12]);
endmodule

module flatten_normally(clk,in,out);
   input clk;
   input [15:0] in;
   output [15:0] out;
   buffer4 b0(clk,in[3:0],out[3:0]);
   buffer4 b1(clk,in[7:4],out[7:4]);
   flop4 f0(clk,in[11:8],out[11:8]);
   flop4 f1(clk,in[15:12],out[15:12]);
endmodule

module buffer16(clk,in,out);
   input clk;
   input [15:0] in;
   output [15:0] out;
   buffer4 b0(clk,in[3:0],out[3:0]);
   buffer4 b1(clk,in[7:4],out[7:4]);
   buffer4 b2(clk,in[11:8],out[11:8]);
   buffer4 b3(clk,in[15:12],out[15:12]);
endmodule

module buffer4(clk,in,out);
   input clk;
   input [3:0] in;
   output [3:0] out;
   assign 	 out = in;
endmodule

module flop4(clk,in,out);
   input clk;
   input [3:0] in;
   output [3:0] out;
   reg [3:0] out;

   reg [3:0] q;
   always @(posedge clk) begin
      q[0] = in[3];
      q[1] = in[1];
      q[2] = in[0];
      q[3] = in[2];
   end

   always @(posedge clk) begin
      out[0] = q[3];
      out[1] = q[1];
      out[2] = q[0];
      out[3] = q[2];
   end
endmodule
