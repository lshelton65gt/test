module top(clk,in,out);
   input clk;
   input [3:0] in;
   output [3:0] out;
   sub s0(clk,in,out);
endmodule

module sub(clk,in,out);
   input clk;
   input [3:0] in;
   output [3:0] out;

   reg [3:0] q;
   always @(posedge clk) begin
      q[0] = in[3];
      q[1] = in[1];
      q[2] = in[0];
      q[3] = in[2];
   end
   
   reg [3:0] out;
   always @(posedge clk) begin
      out[0] = q[3];
      out[1] = q[1];
      out[2] = q[0];
      out[3] = q[2];
   end
   
endmodule
