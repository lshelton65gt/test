#!/usr/bin/perl -w
use strict;

my $MSB   = 31;
my $LSB   = 0;
my $LIMIT = 20;
my $INSTANCES = 12000 / $LIMIT;

print "module child(in,out);
input [$MSB:$LSB] in;
output [$MSB:$LSB] out;
";
for (my $i = 0; $i < $LIMIT; ++$i) {
    printf ("reg [%d:%d] stage_%d;\n", $MSB, $LSB, $i);
}
printf ("reg [%d:%d] out;\n", $MSB, $LSB);

print "always @(in) begin\n";
printf ("stage_0 = ~ in;\n");
for (my $i = 1; $i < $LIMIT; ++$i) {
    printf ("stage_%d = ~ stage_%d;\n", $i, $i-1);
}
printf ("out = ~ stage_%d;\n", $LIMIT - 1);
print "end\n";
print "endmodule\n";
print "\n";

print "module top(in,out);
input [$MSB:$LSB] in;
output [$MSB:$LSB] out;
";

for (my $i = 0; $i < $INSTANCES; ++$i) {
    print "wire [$MSB:$LSB] stage_$i;\n";
}

print "child child0(.in(in),.out(stage_0));\n";
for (my $i = 1; $i < $INSTANCES; ++$i) {
    printf ("child child%d(.in(stage_%d),.out(stage_%d));\n", $i, $i-1, $i);
}
printf ("child childOUT(.in(stage_%d),.out(out));\n", $INSTANCES-1);

print "endmodule\n";

