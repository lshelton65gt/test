// testcase for directive-controlled flattening.
module top(clk,in,out1,out2,out3,out4);
   input clk;
   input [15:0] in;
   output [15:0] out1,out2,out3,out4;

   flatten_into_top u0(clk,in,out1);
   flatten_children u1(clk,in,out2);
   flatten_normally u2(clk,in,out3);
   buffer #(16)     u3(clk,in,out4);
endmodule

module flatten_into_top(clk,in,out);
   input clk;
   input [15:0] in;
   output [15:0] out;
   flop #(4) f0(clk,in[3:0],out[3:0]);
   flop #(4) f1(clk,in[7:4],out[7:4]);
   flop #(4) f2(clk,in[11:8],out[11:8]);
   flop #(4) f3(clk,in[15:12],out[15:12]);
endmodule

module flatten_children(clk,in,out);
   input clk;
   input [15:0] in;
   output [15:0] out;
   flop #(4) f0(clk,in[3:0],out[3:0]);
   flop #(4) f1(clk,in[7:4],out[7:4]);
   flop #(4) f2(clk,in[11:8],out[11:8]);
   flop #(4) f3(clk,in[15:12],out[15:12]);
endmodule

module flatten_normally(clk,in,out);
   input clk;
   input [15:0] in;
   output [15:0] out;
   buffer #(4) b0(clk,in[3:0],out[3:0]);
   buffer #(4) b1(clk,in[7:4],out[7:4]);
   flop #(4) f0(clk,in[11:8],out[11:8]);
   flop #(4) f1(clk,in[15:12],out[15:12]);
endmodule

module buffer(clk,in,out);
   input clk;
   parameter width = 1;
   input [width - 1:0] in;
   output [width - 1:0] out;
   assign                out = in;
endmodule

module flop(clk,in,out);
   parameter width = 1;
   input clk;
   input [width - 1:0] in;
   output [width - 1:0] out;
   reg [width - 1:0] out;

   reg [1:width] q;
   always @(posedge clk)
     q <= in;

   always @(posedge clk)
     out <= q;
endmodule
