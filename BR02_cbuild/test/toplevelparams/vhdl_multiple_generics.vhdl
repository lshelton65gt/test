-- Description: Multiple -topLevelParam are specified, changing the values
--              of both the generics in the top level entity.
--
library ieee;
use ieee.std_logic_1164.all;

entity bottom is
  
  generic (BSIZE1 : integer := 7;
           BSIZE2 : integer := 15);

  port (bin1  : in  std_logic_vector(BSIZE1 downto 0);
        bin2  : in  std_logic_vector(BSIZE2 downto 0);
        bout1 : out std_logic_vector(BSIZE1 downto 0);
        bout2 : out std_logic_vector(BSIZE2 downto 0));

end bottom;

architecture bottomarch of bottom is

begin  -- bottomarch

  bout1 <= bin1;
  bout2 <= bin2;

end bottomarch;

library ieee;
use ieee.std_logic_1164.all;

entity vhdl_multiple_generics is
  
  generic (TSIZE1 : integer := 7;
           TSIZE2 : integer := 15);

  port (tin1  : in  std_logic_vector(TSIZE1 downto 0);
        tin2  : in  std_logic_vector(TSIZE2 downto 0);
        tout1 : out std_logic_vector(TSIZE1 downto 0);
        tout2 : out std_logic_vector(TSIZE2 downto 0));

end vhdl_multiple_generics;

architecture toparch of vhdl_multiple_generics is

  component bottom
    generic (BSIZE1 : integer;
             BSIZE2 : integer);
    port (bin1  : in  std_logic_vector(BSIZE1 downto 0);
          bin2  : in  std_logic_vector(BSIZE2 downto 0);
          bout1 : out std_logic_vector(BSIZE1 downto 0);
          bout2 : out std_logic_vector(BSIZE2 downto 0));
  end component;
  
begin  -- toparch

  U1 : bottom generic map (BSIZE1 => TSIZE1,
                           BSIZE2 => TSIZE2)
              port map (bin1  => tin1,
                        bin2  => tin2,
                        bout1 => tout1,
                        bout2 => tout2);

  
end toparch;
