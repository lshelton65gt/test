-- Description: The top level generic STD value was replaced by a string
--              that contains spaces.
--
library ieee;
use ieee.std_logic_1164.all;

entity bottom is
  
  generic (BSIZE : integer := 7);

  port (bin  : in  std_logic_vector(BSIZE downto 0);
        bout : out std_logic_vector(BSIZE downto 0));

end bottom;

architecture bottomarch of bottom is

begin  -- bottomarch

  bout <= bin;

end bottomarch;

library ieee;
use ieee.std_logic_1164.all;

entity vhdl_string_with_spaces is
  
  generic (TSIZE : integer := 7;
           STR   : string(1 to 12)  := "nospaceshere");

  port (tin  : in  std_logic_vector(TSIZE downto 0);
        tout : out std_logic_vector(TSIZE downto 0));

end vhdl_string_with_spaces;

architecture toparch of vhdl_string_with_spaces is

  component bottom
    generic (BSIZE : integer);
    port (bin  : in  std_logic_vector(BSIZE downto 0);
          bout : out std_logic_vector(BSIZE downto 0));
  end component;
  
begin  -- toparch

  U1 : bottom generic map (BSIZE => TSIZE)
              port map (bin  => tin,
                        bout => tout);

  
end toparch;
