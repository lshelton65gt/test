-- Description: Using -topLevelParam TSIZE=, the value of TSIZE was replaced
--              with a NULL pointer. This will cause cbuild to fail, because
--              TSIZE is actually used in the design.
--
library ieee;
use ieee.std_logic_1164.all;

entity bottom is
  
  generic (BSIZE : integer := 7);

  port (bin  : in  std_logic_vector(BSIZE downto 0);
        bout : out std_logic_vector(BSIZE downto 0));

end bottom;

architecture bottomarch of bottom is

begin  -- bottomarch

  bout <= bin;

end bottomarch;

library ieee;
use ieee.std_logic_1164.all;

entity vhdl_empty_generic is
  
  generic (TSIZE : integer := 7);

  port (tin  : in  std_logic_vector(TSIZE downto 0);
        tout : out std_logic_vector(TSIZE downto 0));

end vhdl_empty_generic;

architecture toparch of vhdl_empty_generic is

  component bottom
    generic (BSIZE : integer);
    port (bin  : in  std_logic_vector(BSIZE downto 0);
          bout : out std_logic_vector(BSIZE downto 0));
  end component;
  
begin  -- toparch

  U1 : bottom generic map (BSIZE => TSIZE)
              port map (bin  => tin,
                        bout => tout);

  
end toparch;

