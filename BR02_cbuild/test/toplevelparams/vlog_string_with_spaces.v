module vlog_string_with_spaces(tin, tout);
   parameter STR="no spaces";
   parameter TSIZE=7;
   input [0:TSIZE] tin;
   output [0:TSIZE] tout;

   vlog_bottom #(TSIZE) u1(tin, tout);

endmodule

module vlog_bottom(bin, bout);
   parameter BSIZE=3;
   input [0:BSIZE] bin;
   output [0:BSIZE] bout;

   assign bout = bin;

endmodule
