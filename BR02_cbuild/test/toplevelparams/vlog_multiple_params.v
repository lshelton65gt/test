module vlog_multiple_params(tin1, tin2, tout1, tout2);
   parameter TSIZE1=7;
   parameter TSIZE2=15;
   input [0:TSIZE1] tin1;
   input [0:TSIZE2] tin2;
   output [0:TSIZE1] tout1;
   output [0:TSIZE2] tout2;

   vlog_bottom #(TSIZE1,TSIZE2) u1(tin1, tin2, tout1, tout2);

endmodule

module vlog_bottom(bin1, bin2, bout1, bout2);
   parameter BSIZE1=3;
   parameter BSIZE2=7;
   input [0:BSIZE1] bin1;
   input [0:BSIZE2] bin2;
   output [0:BSIZE1] bout1;
   output [0:BSIZE2] bout2;

   assign bout1 = bin1;
   assign bout2 = bin2;

endmodule
