-- Description: The top level entity has no generics, so using -topLevelParam
--              results in a FATAL error.
--
library ieee;
use ieee.std_logic_1164.all;

entity bottom is
  
  generic (BSIZE : integer := 7);

  port (bin  : in  std_logic_vector(BSIZE downto 0);
        bout : out std_logic_vector(BSIZE downto 0));

end bottom;

architecture bottomarch of bottom is

begin  -- bottomarch

  bout <= bin;

end bottomarch;

library ieee;
use ieee.std_logic_1164.all;

entity vhdl_no_parameters is
  
  port (tin  : in  std_logic_vector(7 downto 0);
        tout : out std_logic_vector(7 downto 0));

end vhdl_no_parameters;

architecture toparch of vhdl_no_parameters is

  component bottom
    generic (BSIZE : integer);
    port (bin  : in  std_logic_vector(BSIZE downto 0);
          bout : out std_logic_vector(BSIZE downto 0));
  end component;
  
begin  -- toparch

  U1 : bottom generic map (BSIZE => 7)
              port map (bin  => tin,
                        bout => tout);

  
end toparch;
