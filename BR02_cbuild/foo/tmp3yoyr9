#include "logger/BladeLogger.h"
#include "sim/CommandLine.h"

#include <algorithm>
#include <iomanip>
#include <iostream>

using namespace std;
#ifdef WIN32
extern __declspec(dllimport) bool gLogBreakpointHitMessage; // defined in
                                                            // maxcorelib
                                                            // mxsim/Common.cpp
#else
extern bool gLogBreakpointHitMessage; // defined in maxcorelib mxsim/Common.cpp
#endif

/*! Checks that the -numBlades and -numSegments command line arguments are sane
**
** If either is set (non-zero) then they must be identical, or the other must
** be zero.
**
** \note this check is modifying so that they end up being the same
*/
bool checkSegmentsAndBladesCounts(BladeRunnerOptions &bladeOptions)
{
  // if either is 0 we are fine - make them equal to the non-zero one
  if (bladeOptions.numberBlades == 0 || bladeOptions.numberSegments == 0)
  {
    bladeOptions.numberBlades =
        max(bladeOptions.numberBlades, bladeOptions.numberSegments);
    bladeOptions.numberSegments = bladeOptions.numberBlades;
    return true;
  }

  // Both are non-zero, must be the same
  if (bladeOptions.numberBlades == bladeOptions.numberSegments) return true;

  // both were non-zer0, but they are different
  LOG_ERR << "One of number of segments or number of blades must be zero, or "
             "they must match";
  LOG_ERR << dec << "Number of Blades: " << bladeOptions.numberBlades;
  LOG_ERR << dec << "Number of Segments: " << bladeOptions.numberSegments;
  return false;
}

bool checkOptionsFile(const BladeRunnerOptions &bladeOptions)
{
  return true;
}

std::ostream &operator<<(std::ostream &os, const BladeRunnerOptions opt)
{
  os << "BladeRunnerOptions:" << std::dec
     << "\nnumberSegments = " << opt.numberSegments
     << "\ncurrentSegment = " << opt.currentSegment
     << "\nprintHelp = " << std::boolalpha << opt.printHelp
     << "\nnumberBlades = " << opt.numberBlades
     << "\nexitAfter = " << opt.exitAfter
     << "\nsegmentsDirectory = " << opt.segmentsDirectory
     << "\nconfigFileName = " << opt.configFileName
     << "\napplicationsFileName = " << opt.applicationsFileName;
  return os;
}

BladeRunnerOptions commandLine(int argc, const char *argv[])
{

  BladeRunnerOptions bladeOptions;
  /*
 ** Setup and parse command line
 */
  po::variables_map options;
  po::options_description commandOptions;
  commandOptions.add_options()(
      "help,h", po::value<bool>(&bladeOptions.printHelp)->default_value(false),
      "Print this help message")(
      "numBlades", po::value<int>(&bladeOptions.numberBlades)->default_value(0),
      "Number Of Blades to create")(
      "numSegments",
      po::value<int>(&bladeOptions.numberSegments)->default_value(0),
      "Number Of Segments to create")(
      "logBreakpointHit",
      po::value<bool>(&gLogBreakpointHitMessage)->default_value(false),
      "Emit log message for every breakpoint.  Reduces performance "
      "signifcantly!")(
      "exitAfter", po::value<int>(&bladeOptions.exitAfter)->default_value(0),
      "Exit Creation after creating checkpoint N")(
      "segment",
      po::value<int>(&bladeOptions.currentSegment)->default_value(-1),
      "Segment number to run in CA mode")(
      "segmentsDir", po::value<std::string>(&bladeOptions.segmentsDirectory),
      "Top level segments directory")
      // This needs to also be a default for a floating arg
      ("config", po::value<std::string>(&bladeOptions.configFileName),
       "Path to configuration file")(
          "applications",
          po::value<std::string>(&bladeOptions.applicationsFileName),
          "Applications file");

  po::positional_options_description pd;
  pd.add("config", 1);

  // parse
  po::store(po::command_line_parser(argc, argv)
                .options(commandOptions)
                .positional(pd)
                .style(po::command_line_style::unix_style |
                       po::command_line_style::allow_long_disguise)
                .run(),
            options);
  po::notify(options);

  if (bladeOptions.printHelp)
  {
    std::cout << commandOptions; // << pd;
  }

  // Checks to add
  // options file must exist
  bool validated = true;
  validated = checkSegmentsAndBladesCounts(bladeOptions) && validated;
  validated = checkOptionsFile(bladeOptions) && validated;

  bladeOptions.optionsValid = validated;

  return bladeOptions;
}
