#ifdef CARBON_SPEEDCC
#include "codegen/SysIncludes.h"
#include "util/c_memmanager.h"
#else
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "carbon/c_memmanager.h"
#endif
#include "libmemtest.h"

// boolean to control simulation. If not ok, simulation will stop
static CarbonStatus sSimulateStatus = eCarbon_OK;

// Entry point for calltree --toggle-collect=simulate --collect-state=no
extern "C" void simulate(CarbonObjectID* hdl, const CarbonTime time) {
  sSimulateStatus = carbonSchedule(hdl, time);
  // Let $stop through
  if (sSimulateStatus == eCarbon_STOP) sSimulateStatus = eCarbon_OK;
}

// control callback for $finish
static void sControlFinishCB(CarbonObjectID*, CarbonControlType, CarbonClientData, const char*, int) {
  sSimulateStatus = eCarbon_FINISH;
}

// our own version of abs, so we don't have to rely on std namespace
#define CABS(e) ((e)>=0?(e):-(e))

int main(int argc, char **argv)
{
  int doPrint = 1;
  int repeatCount = 1;
  int arg = 1;
  CarbonObjectID *hdl;
  CarbonMemFileID* memFile;
  CarbonTime time = 0;
  SInt64 firstAddress, lastAddress;
  while (argc > arg) {
    if (strcmp(argv[arg], "-noprint") == 0) {
      doPrint = 0;
      ++arg;
    }
    else if (strcmp(argv[arg], "-repeat") == 0) {
      char* endp;
      ++arg;
      repeatCount = strtol(argv[arg], &endp, 10);
      ++arg;
      if ((repeatCount == 0) || (*endp != '\0')) {
        fprintf(stderr, "Invalid args\n");
        exit(1);
      }
    }
    else {
      fprintf(stderr, "Invalid argument: %s\n", argv[arg]);
      exit(1);
    }
  }

  CarbonNetID * m_ce;
  CarbonNetID * m_we;
  CarbonNetID * m_addr;
  CarbonNetID * m_data;

#define INITIALIZE(INIT) \
  hdl = carbon_memtest_create(eCarbonIODB, eCarbon_NoInit); \
  carbonChangeMsgSeverity(hdl, 5065, eCarbonMsgSuppress); \
  CarbonRegisteredControlCBDataID* IDForFinish1 = carbonAdminAddControlCB(hdl, sControlFinishCB, NULL, eCarbonFinish); \
  if (IDForFinish1 == NULL) { \
    fprintf(stderr, "Error: unable to register $finish callback function \n"); \
    return 1; \
  } \
  if (INIT) carbonInitialize(hdl, NULL, NULL, NULL); \
\
  m_ce = carbonFindNet(hdl, "single_port_ram.ce"); \
  assert(m_ce); \
  m_we = carbonFindNet(hdl, "single_port_ram.we"); \
  assert(m_we); \
  m_addr = carbonFindNet(hdl, "single_port_ram.addr"); \
  assert(m_addr); \
  m_data = carbonFindNet(hdl, "single_port_ram.data"); \
  assert(m_data); \


  INITIALIZE(true);
  memFile = carbonReadMemFile(hdl, "memtest.test.vectors", eCarbonHex, 70, 0);
  if (! memFile) return 1;
  if (carbonMemFileGetFirstAndLastAddrs(memFile, &firstAddress, &lastAddress) != eCarbon_OK) return 1;
  CarbonUInt32 numRows = CABS(lastAddress - firstAddress) + 1;

  CarbonUInt32* m_ce_cds_carbonnetdata = (CarbonUInt32*) carbonmem_malloc((sizeof(CarbonUInt32)) * 1 * numRows);
  CarbonUInt32* m_ce_cds_carbonnetdata_begin = m_ce_cds_carbonnetdata;
  if (carbonMemFilePopulateArray(memFile, m_ce_cds_carbonnetdata, 1 * numRows, 69, 1) != eCarbon_OK) return 1;

  CarbonUInt32* m_we_cds_carbonnetdata = (CarbonUInt32*) carbonmem_malloc((sizeof(CarbonUInt32)) * 1 * numRows);
  CarbonUInt32* m_we_cds_carbonnetdata_begin = m_we_cds_carbonnetdata;
  if (carbonMemFilePopulateArray(memFile, m_we_cds_carbonnetdata, 1 * numRows, 68, 1) != eCarbon_OK) return 1;

  CarbonUInt32* m_addr_cds_carbonnetdata = (CarbonUInt32*) carbonmem_malloc((sizeof(CarbonUInt32)) * 1 * numRows);
  CarbonUInt32* m_addr_cds_carbonnetdata_begin = m_addr_cds_carbonnetdata;
  if (carbonMemFilePopulateArray(memFile, m_addr_cds_carbonnetdata, 1 * numRows, 64, 4) != eCarbon_OK) return 1;

  CarbonUInt32* m_data_cds_tmp_data = (CarbonUInt32*) carbonmem_malloc((sizeof(CarbonUInt32)) * 1 * numRows);
  CarbonUInt32* m_data_cds_tmp_data_begin = m_data_cds_tmp_data;
  if (carbonMemFilePopulateArray(memFile, m_data_cds_tmp_data, 1 * numRows, 32, 32) != eCarbon_OK) return 1;

  CarbonUInt32* m_data_cds_tmp_enable = (CarbonUInt32*) carbonmem_malloc((sizeof(CarbonUInt32)) * 1 * numRows);
  CarbonUInt32* m_data_cds_tmp_enable_begin = m_data_cds_tmp_enable;
  if (carbonMemFilePopulateArray(memFile, m_data_cds_tmp_enable, 1 * numRows, 0, 32) != eCarbon_OK) return 1;
  carbonFreeMemFileID(&memFile);

  char* s = NULL;
  const size_t strSize = 33;
  if (doPrint) 
    s = CARBON_ALLOC_VEC(char, strSize);


  for (int repeat = 0; repeat < repeatCount; ++repeat) {
    for(SInt64 i = firstAddress; (i <= lastAddress) && (sSimulateStatus == eCarbon_OK); ++i)
    {
      carbonDeposit(hdl, m_ce, m_ce_cds_carbonnetdata, 0);
      m_ce_cds_carbonnetdata += 1;
      carbonDeposit(hdl, m_we, m_we_cds_carbonnetdata, 0);
      m_we_cds_carbonnetdata += 1;
      carbonDeposit(hdl, m_addr, m_addr_cds_carbonnetdata, 0);
      m_addr_cds_carbonnetdata += 1;
      carbonDeposit(hdl, m_data, m_data_cds_tmp_data, m_data_cds_tmp_enable);

      simulate(hdl, time);
      time += 100; 

      if (!doPrint) continue;
      carbonFormat(hdl, m_ce, s, strSize, eCarbonBin);
      fputs(s, stdout);
      carbonFormat(hdl, m_we, s, strSize, eCarbonBin);
      fputs(s, stdout);
      carbonFormat(hdl, m_addr, s, strSize, eCarbonBin);
      fputs(s, stdout);
      carbonFormatGenericArray(s, strSize, m_data_cds_tmp_data, 32, 0, eCarbonBin);
      fputs(s, stdout);
      carbonFormatGenericArray(s, strSize, m_data_cds_tmp_enable, 32, 0, eCarbonBin);
      fputs(s, stdout);
      carbonFormat(hdl, m_data, s, strSize, eCarbonBin);
      fputs(s, stdout);
      fputc('\n', stdout);
      fflush(stdout);

      m_data_cds_tmp_data += 1;
      m_data_cds_tmp_enable += 1;
    }

    m_ce_cds_carbonnetdata = m_ce_cds_carbonnetdata_begin;
    m_we_cds_carbonnetdata = m_we_cds_carbonnetdata_begin;
    m_addr_cds_carbonnetdata = m_addr_cds_carbonnetdata_begin;
    m_data_cds_tmp_data = m_data_cds_tmp_data_begin;
    m_data_cds_tmp_enable = m_data_cds_tmp_enable_begin;
  }
  carbonDestroy(&hdl);
  carbonmem_free(m_ce_cds_carbonnetdata_begin);
  carbonmem_free(m_we_cds_carbonnetdata_begin);
  carbonmem_free(m_addr_cds_carbonnetdata_begin);
  carbonmem_free(m_data_cds_tmp_data_begin);
  carbonmem_free(m_data_cds_tmp_enable_begin);
  if (s)
    CARBON_FREE_VEC(s, char, strSize);
  return 0;
}
