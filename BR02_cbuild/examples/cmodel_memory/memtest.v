// This demonstrates compiling and running a simple design with
// a single port RAM

// Single ported RAM with a bidirectional data bus.  We cannot represent
// a bidirectional bus with a C Model so we split the bus in the Verilog
// model, and use the write-enable as an indication of whether we are
// going to internally drive the bus or not
module single_port_ram(ce, we, addr, data);
  input ce, we;
  input [3:0] addr;             // make it shallow so we can test it thoroughly
  inout [31:0] data;            // no reason to make it too narrow

  // Read data bus
  wire          out_enable = ce & !we;
  wire [31:0]   read_data;

  // Drive the bidirectional bus
  assign        data = out_enable ? read_data : 32'bz;

  // instantiate the read model
  single_port_ram_read read(ce & !we, addr, read_data);

  // instantiate the write model
  single_port_ram_write write(ce & we, addr, data);
endmodule

// c model for the read half
module single_port_ram_read(oe, addr, data);
  input oe;
  input [3:0] addr;
  output [31:0] data;
endmodule

// c model for the write half
module single_port_ram_write(we, addr, data);
  input we;
  input [3:0] addr;
  input [31:0] data;
endmodule
