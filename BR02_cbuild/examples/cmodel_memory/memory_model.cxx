/* C-model implementation of increment module */

#include "cds_libmemtest_single_port_ram_write.h"
#include "cds_libmemtest_single_port_ram_read.h"
#include <map>
#include <string>
#include <assert.h>
#include <cstring>

//! Class to keep the state of the memory.
/*!
 *! Note that this is shared between the read-model and the write-model
 */
class MemoryModel {
public:
# define NUM_ADDRS 16

  MemoryModel() {
    memset(mData, 0, NUM_ADDRS * sizeof(UInt32));
  };
  ~MemoryModel() {}
  
  UInt32 mData[NUM_ADDRS];
};

typedef std::map<std::string,MemoryModel*> ModelMap;
static ModelMap* sModelMap = NULL;

void* cds_single_port_ram_write_create(int numParams,
                                       CModelParam* cmodelParams,
                                       const char* inst)
{
  if (sModelMap == NULL) {
    sModelMap = new ModelMap;
  }

  // This routine will either be called with a.b.c.read or a.b.c.write,
  // because that's how the instances in single_port_ram model is defined.
  // We want to return the same MemoryModel for these two C models, so
  // we chop the ".read" or ".write" off the instance path and look it
  // up in a map.  
  std::string inst_string(inst);
  size_t dot = inst_string.find_last_of('.');
  if (dot != std::string::npos) {
    inst_string.erase(dot);
  }
  ModelMap::iterator p = sModelMap->find(inst_string);


  MemoryModel* mm = NULL;

  if (p != sModelMap->end()) {
    // If we find a match we can associate the existing model
    // with the new half, and also erase it from the map -- we should not
    // see it again.  
    mm = p->second;
    sModelMap->erase(p);        // clear the map as we go
  }

  else {
    // If we don't find a match then this is the first half
    // that's getting initialized, so we have to create the memory model
    // and put it in the map so the other half finds it when it initializes
    mm = new MemoryModel;
    (*sModelMap)[inst_string] = mm;
  }
  return mm;
}

void* cds_single_port_ram_read_create(int numParams,
                                      CModelParam* cmodelParams,
                                      const char* inst)
{
  return cds_single_port_ram_write_create(numParams, cmodelParams, inst);
}

void cds_single_port_ram_write_misc(void* hndl,
                                    CarbonCModelReason reason,
                                    void* cmodelData)
{
}

void cds_single_port_ram_read_misc(void* hndl,
                                   CarbonCModelReason reason,
                                   void* cmodelData)
{
}

void cds_single_port_ram_read_run(void* hndl,
                                  CDSsingle_port_ram_readContext context,
                                  const CarbonUInt32* oe,   // Input, size = 1
                                  const CarbonUInt32* addr, // Input, size = 4
                                  CarbonUInt32* data        // Output, size = 32
  )
{
  if (oe[0]) {
    MemoryModel*  mm = (MemoryModel*) hndl;
    *data = mm->mData[addr[0]];
  }
}

void cds_single_port_ram_write_run(void* hndl,
                                   CDSsingle_port_ram_writeContext context,
                                   const CarbonUInt32* we,   // Input, size = 1
                                   const CarbonUInt32* addr, // Input, size = 4
                                   const CarbonUInt32* data  // Input, size = 32
  )
{
  if (we[0]) {
    MemoryModel*  mm = (MemoryModel*) hndl;
    mm->mData[addr[0]] = *data;
  }
}

void cds_single_port_ram_write_destroy(void* hndl)
{
  MemoryModel* mm = (MemoryModel*) hndl;
  delete mm;
  assert(sModelMap->empty());
}

void cds_single_port_ram_read_destroy(void* hndl)
{
  // Only need destroy in the write-side
  assert(sModelMap->empty());
}
