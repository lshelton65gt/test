// -*- mode: c++ -*-

#include "carbon/carbon_capi.h"
#include "AhbSlaveModel.h"
#include "xactors/ahb_xtor/carbonXAhb.h"


class AhbTb
{
public:
  AhbTb();
  // Constructor

  virtual ~AhbTb();
  // Destructor

  bool setup();
  // Sets up the model and the transactor

  bool evaluate(CarbonTime currTime);
  bool evaluate();
  // Evaluates the test function, transactor and the Carbon model

  virtual void testFunction()= 0;
  // Test function - to be implemented by the test

  virtual void reportTransaction(AhbTransaction * trans)=0;
  // Report Transaction Method, should be defined by test

  static void reportTransactionCB(AhbTransaction * trans, void * clientData);
  // Report Transaction Callback function for master transactor

  static void setDefaultResponseCB(AhbTransaction * trans, void * clientData);
  // Set Default Response Callback function for slave transactor

  static void setResponseCB(AhbTransaction* trans, void* clientData);
  // Set Response Callback function for slave transactor

  static void readResponseCB(CarbonUInt64 addr, CarbonUInt32 * data, void *);
  static void writeResponseCB(CarbonUInt64 addr, CarbonUInt32 * data, void*);


protected:
  // Member variables
  CarbonObjectID * mCarbonID;
  AhbAbsXtor *  mMasterTrans;
  AhbAbsXtor *   mSlaveTrans;
  AhbSlaveModel *  mSlaveModel;
  bool             mTestDone;

  CarbonWaveID *   mWaveID;
};
