// -*- mode: c++ -*-

#ifndef AHBTEST_H
#define AHBTEST_H

#include <iostream>

#include "AhbTb.h"
#include "xactors/ahb_xtor/carbonXAhb.h"
//#include "carbonXAhb.h"


class AhbTest : public AhbTb
{
public:
  AhbTest();
  virtual ~AhbTest();

  void testFunction();
  // test function needs to be implemented here

  void reportTransaction(AhbTransaction * trans);
  // Report Transaction Method, should be defined by test

  CarbonUInt32 getErrors() const  {
    std::cout << "Test had " << mErrorCount << " errors." << std::endl;
    return mErrorCount;
  }

private:
  void write(CarbonUInt32 addr, CarbonUInt32 data);
  void read(CarbonUInt32 addr);

  void write(AhbTransConfig_t trans_cfg);
  void read(AhbTransConfig_t trans_cfg);

  // Test function go through these states sequentially
  enum TestState_t {
    eStart,
    eWriteMem,
    eWaitWriteDone,
    eReadMem,
    eWaitReadDone,

    eWriteMemInc4,
    eWaitWriteDoneInc4,
    eReadMemInc4,
    eWaitReadDoneInc4,

    eWriteMemWrap4,
    eWaitWriteDoneWrap4,
    eReadMemWrap4,
    eWaitReadDoneWrap4,

    eWriteMemInc4NB,
    eWaitWriteDoneInc4NB,
    eReadMemInc4NB,
    eWaitReadDoneInc4NB,

    eWriteMemWrap4NB,
    eWaitWriteDoneWrap4NB,
    eReadMemWrap4NB,
    eWaitReadDoneWrap4NB,

    eWriteMemIncrSlaveWait3,
    eWaitWriteDoneIncrSlaveWait3,
    eReadMemIncrSlaveWait3,
    eWaitReadDoneIncrSlaveWait3,

    eWriteMemIncr2SlaveWait3,
    eWaitWriteDoneIncr2SlaveWait3,
    eReadMemIncr2SlaveWait3,
    eWaitReadDoneIncr2SlaveWait3,

    eWriteMemIncr4MasterBusy3,
    eWaitWriteDoneIncr4MasterBusy3,
    eReadMemIncr4MasterBusy3,
    eWaitReadDoneIncr4MasterBusy3,

    eTestDone
  };

  TestState_t mTestState;

  /* Used to keep track of received transactions */
  CarbonUInt32     mTransCount;         // pending transaction counts
  AhbTransaction * mLastReceivedTrans;
  bool             mTransReceived;

  CarbonUInt32     mErrorCount;
  // Keep track of errors in the test
};


#endif
