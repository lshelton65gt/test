// -*- mode: c++ -*-

#include "AhbTest.h"


using namespace std;

AhbTest::AhbTest()
  : mTestState(eStart),
    mTransCount(0),
    mErrorCount(0)
{ }

AhbTest::~AhbTest()
{
  carbonDumpFlush(mWaveID);

  //if (mLastReceivedTrans != NULL)
  //  carbonXAhbTransDestroy(mLastReceivedTrans);
}

void AhbTest::testFunction()
{
  switch(mTestState) {
  case eStart :
    cout << "\teStart -" << endl;
    mTransCount = 0;
    mTestState  = eWriteMem;
    break;
  case eWriteMem :
    cout << "\teWriteMem -" << endl;

    // Create a write transaction
    write(0x400, 0x12345678);

    // Go to wait for writes state
    mTestState = eWaitWriteDone;
    break;
  case eWaitWriteDone :
    cout << "\teWaitWriteDone -" << endl;

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eReadMem;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }

    mTransReceived = false; // Clear receive flag
    break;
  case eReadMem :
    cout << "\teReadMem -" << endl;

    // Read the addresses written earlier, and see if they match
    read(0x400);

    // Go to wait for reades state
    mTestState = eWaitReadDone;
    break;
  case eWaitReadDone :
    cout << "\teWaitReadDone -" << endl;

    // When a read transaction is done, check the result
    if(mTransReceived) 
      {
	// retrieve trans addr and data
	CarbonUInt64 addr = carbonXAhbTransGetAddress(mLastReceivedTrans);
	CarbonUInt32 data = ((CarbonUInt32 *)carbonXAhbTransGetDataPtr(mLastReceivedTrans))[0];

	switch(addr) 
	  {
	  case 0x400:
	    if(data != 0x12345678) {
	      cout << "Error: Read data mismatch for addr " << hex << addr
		   << " Expected: 0x12345678 Received: " << data << dec << endl;
	      ++mErrorCount;
	    }
	    break;

	  case 0x404:
	    if(data != 0x2345) {
	      cout << "Error: Read data mismatch for addr " << hex << addr
		   << " Expected: 0x2345 Received: " << data << dec << endl;
	      ++mErrorCount;
	    }
	    break;

	  case 0x408:
	    if(data != 0x3456) {
	      cout << "Error: Read data mismatch for addr " << hex << addr
		   << " Expected: 0x3456 Received: " << data << dec << endl;
	      ++mErrorCount;
	    }
	    break;
	  }

	mTransReceived = false;
      }

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eWriteMemInc4;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }
    break;

  case eWriteMemInc4:
    cout << "\teWriteMemInc4 -" << endl;

    AhbTransConfig_t trans_cfg;

    CarbonUInt32 data[4];
    data[0] = 0x11223344;
    data[1] = 0x55667788;
    data[2] = 0x99AABBCC;
    data[3] = 0xDDEEFF67;

    // Setup the write inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_WRITE;
    trans_cfg.addr      = 0x400;
    trans_cfg.data_ptr  = (CarbonUInt8*) & data;
    trans_cfg.length    = 16;
    trans_cfg.burstType = CarbonXAhbTrans_INCR4;
    trans_cfg.size      = CarbonXAhbTrans_SIZE32;
    trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    write(trans_cfg);

    // Go to wait for writes state
    mTestState = eWaitWriteDoneInc4;

    break;

  case eWaitWriteDoneInc4 :
    cout << "\teWaitWriteDoneInc4 -" << endl;

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eReadMemInc4;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }

    mTransReceived = false; // Clear receive flag
    break;

  case eReadMemInc4 :
    cout << "\teReadMemInc4 -" << endl;

    //AhbTransConfig_t trans_cfg;

    // Setup the read inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_READ;
    trans_cfg.addr      = 0x400;
    trans_cfg.data_ptr  = NULL;
    trans_cfg.length    = 16;
    trans_cfg.burstType = CarbonXAhbTrans_INCR4;
    trans_cfg.size      = CarbonXAhbTrans_SIZE32;
    trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    read(trans_cfg);

    // Go to wait for reades state
    mTestState = eWaitReadDoneInc4;
    break;

  case eWaitReadDoneInc4 :
    cout << "\teWaitReadDoneInc4 -" << endl;

    // When a read transaction is done, check the result
    if(mTransReceived)
      {
	// retrieve trans addr and data
	CarbonUInt64 addr = carbonXAhbTransGetAddress(mLastReceivedTrans);
	CarbonUInt8 * data_ptr = carbonXAhbTransGetDataPtr(mLastReceivedTrans);

	// 1st data item
	CarbonUInt32 data = ((CarbonUInt32 *)(data_ptr))[0];
	if (data != 0x11223344)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr
		 << " Expected: 0x11223344 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 2nd data item
	data = ((CarbonUInt32 *)(data_ptr))[1];
	if (data != 0x55667788)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 4
		 << " Expected: 0x55667788 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 3rd data item
	data = ((CarbonUInt32 *)(data_ptr))[2];
	if (data != 0x99aabbcc)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 8
		 << " Expected: 0x99aabbcc Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 4th data item
	data = ((CarbonUInt32 *)(data_ptr))[3];
	if (data != 0xddeeff67)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 12
		 << " Expected: 0xddeeff67 Received: " << data << dec << endl;
	    cout << hex << ((CarbonUInt32 *)(data_ptr))[4] << dec << endl;
	    ++mErrorCount;
	  }

	mTransReceived = false;
      }

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eWriteMemWrap4;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }
    break;

  case eWriteMemWrap4:
    cout << "\teWriteMemWrap4 -" << endl;

    data[0] = 0x1a1b1c1d;
    data[1] = 0x2a2b2c2d;
    data[2] = 0x3a3b3c3d;
    data[3] = 0x4a4b4c4d;

    // Setup the write inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_WRITE;
    trans_cfg.addr      = 0x408;
    trans_cfg.data_ptr  = (CarbonUInt8*) & data;
    trans_cfg.length    = 16;
    trans_cfg.burstType = CarbonXAhbTrans_WRAP4;
    trans_cfg.size      = CarbonXAhbTrans_SIZE32;
    trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    write(trans_cfg);

    // Go to wait for writes state
    mTestState = eWaitWriteDoneWrap4;

    break;

  case eWaitWriteDoneWrap4 :
    cout << "\teWaitWriteDoneWrap4 -" << endl;

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eReadMemWrap4;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }

    mTransReceived = false; // Clear receive flag
    break;

  case eReadMemWrap4 :
    cout << "\teReadMemWrap4 -" << endl;

    //AhbTransConfig_t trans_cfg;

    // Setup the read inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_READ;
    trans_cfg.addr      = 0x404;
    trans_cfg.data_ptr  = NULL;
    trans_cfg.length    = 16;
    trans_cfg.burstType = CarbonXAhbTrans_WRAP4;
    trans_cfg.size      = CarbonXAhbTrans_SIZE32;
    trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    read(trans_cfg);

    // Go to wait for reades state
    mTestState = eWaitReadDoneWrap4;
    break;

  case eWaitReadDoneWrap4 :
    cout << "\teWaitReadDoneWrap4 -" << endl;

    // When a read transaction is done, check the result
    if(mTransReceived)
      {
	// retrieve trans addr and data
	CarbonUInt64 addr = carbonXAhbTransGetAddress(mLastReceivedTrans);
	CarbonUInt8 * data_ptr = carbonXAhbTransGetDataPtr(mLastReceivedTrans);

	// 1st data item
	CarbonUInt32 data = ((CarbonUInt32 *)(data_ptr))[0];
	if (data != 0x4a4b4c4d)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr
		 << " Expected: 0x4a4b4c4d Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 2nd data item
	data = ((CarbonUInt32 *)(data_ptr))[1];
	if (data != 0x1a1b1c1d)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 4
		 << " Expected: 0x1a1b1c1d Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 3rd data item
	data = ((CarbonUInt32 *)(data_ptr))[2];
	if (data != 0x2a2b2c2d)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 8
		 << " Expected: 0x2a2b2c2d Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 4th data item
	data = ((CarbonUInt32 *)(data_ptr))[3];
	if (data != 0x3a3b3c3d)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 12
		 << " Expected: 0x3a3b3c3d Received: " << data << dec << endl;
	    cout << hex << ((CarbonUInt32 *)(data_ptr))[4] << dec << endl;
	    ++mErrorCount;
	  }

	mTransReceived = false;
      }

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eWriteMemInc4NB;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }
    break;

  case eWriteMemInc4NB:
    cout << "\teWriteMemInc4NB -" << endl;

    data[0] = 0x20211011;
    data[1] = 0x40413031;
    data[2] = 0xdeadbeef;
    data[3] = 0xdeadbeef;

    // Setup the write inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_WRITE;
    trans_cfg.addr      = 0x400;
    trans_cfg.data_ptr  = (CarbonUInt8*) & data;
    trans_cfg.length    = 8;
    trans_cfg.burstType = CarbonXAhbTrans_INCR4;
    trans_cfg.size      = CarbonXAhbTrans_SIZE16;
    trans_cfg.locked    = CarbonXAhbTrans_LOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    write(trans_cfg);

    // Go to wait for writes state
    mTestState = eWaitWriteDoneInc4NB;

    break;

  case eWaitWriteDoneInc4NB :
    cout << "\teWaitWriteDoneInc4NB -" << endl;

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eReadMemInc4NB;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }

    mTransReceived = false; // Clear receive flag
    break;

  case eReadMemInc4NB :
    cout << "\teReadMemInc4NB -" << endl;

    //AhbTransConfig_t trans_cfg;

    // Setup the read inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_READ;
    trans_cfg.addr      = 0x400;
    trans_cfg.data_ptr  = NULL;
    trans_cfg.length    = 8;
    trans_cfg.burstType = CarbonXAhbTrans_INCR4;
    trans_cfg.size      = CarbonXAhbTrans_SIZE16;
    trans_cfg.locked    = CarbonXAhbTrans_LOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    read(trans_cfg);

    // Go to wait for reades state
    mTestState = eWaitReadDoneInc4NB;
    break;

  case eWaitReadDoneInc4NB :
    cout << "\teWaitReadDoneInc4NB -" << endl;

    // When a read transaction is done, check the result
    if(mTransReceived)
      {
	// retrieve trans addr and data
	CarbonUInt64 addr = carbonXAhbTransGetAddress(mLastReceivedTrans);
	CarbonUInt8 * data_ptr = carbonXAhbTransGetDataPtr(mLastReceivedTrans);

	// 1st data item
	CarbonUInt32 data = ((CarbonUInt32 *)(data_ptr))[0];
	if (data != 0x20211011)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr
		 << " Expected: 0x20211011 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 2nd data item
	data = ((CarbonUInt32 *)(data_ptr))[1];
	if (data != 0x40413031)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 4
		 << " Expected: 0x40413031 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	mTransReceived = false;
      }

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eWriteMemWrap4NB;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }
    break;

  case eWriteMemWrap4NB:
    cout << "\teWriteMemWrap4NB -" << endl;

    data[0] = 0x60615051;
    data[1] = 0x80817071;
    data[2] = 0xdeadbeef;
    data[3] = 0xdeadbeef;

    // Setup the write inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_WRITE;
    trans_cfg.addr      = 0x406;
    trans_cfg.data_ptr  = (CarbonUInt8*) & data;
    trans_cfg.length    = 8;
    trans_cfg.burstType = CarbonXAhbTrans_WRAP4;
    trans_cfg.size      = CarbonXAhbTrans_SIZE16;
    trans_cfg.locked    = CarbonXAhbTrans_LOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    write(trans_cfg);

    // Go to wait for writes state
    mTestState = eWaitWriteDoneWrap4NB;

    break;

  case eWaitWriteDoneWrap4NB :
    cout << "\teWaitWriteDoneWrap4 -" << endl;

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eReadMemWrap4NB;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }

    mTransReceived = false; // Clear receive flag
    break;

  case eReadMemWrap4NB :
    cout << "\teReadMemWrap4NB -" << endl;

    //AhbTransConfig_t trans_cfg;

    // Setup the read inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_READ;
    trans_cfg.addr      = 0x402;
    trans_cfg.data_ptr  = NULL;
    trans_cfg.length    = 8;
    trans_cfg.burstType = CarbonXAhbTrans_WRAP4;
    trans_cfg.size      = CarbonXAhbTrans_SIZE16;
    trans_cfg.locked    = CarbonXAhbTrans_LOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    read(trans_cfg);

    // Go to wait for reades state
    mTestState = eWaitReadDoneWrap4NB;
    break;

  case eWaitReadDoneWrap4NB :
    cout << "\teWaitReadDoneWrap4NB -" << endl;

    // When a read transaction is done, check the result
    if(mTransReceived)
      {
	// retrieve trans addr and data
	CarbonUInt64 addr = carbonXAhbTransGetAddress(mLastReceivedTrans);
	CarbonUInt8 * data_ptr = carbonXAhbTransGetDataPtr(mLastReceivedTrans);

	// 1st data item
	CarbonUInt32 data = ((CarbonUInt32 *)(data_ptr))[0];
	if (data != 0x80817071)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr
		 << " Expected: 0x80817071 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 2nd data item
	data = ((CarbonUInt32 *)(data_ptr))[1];
	if (data != 0x60615051)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 4
		 << " Expected: 0x60615051 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	mTransReceived = false;
      }

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eWriteMemIncrSlaveWait3;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }
    break;

  case eWriteMemIncrSlaveWait3:
    cout << "\teWriteMemIncrSlaveWait3 -" << endl;

    data[0] = 0x13121110;
    data[1] = 0x23222120;
    data[2] = 0xdeadbeef;
    data[3] = 0xdeadbeef;

    // Setup the write inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_WRITE;
    trans_cfg.addr      = 0x200;
    trans_cfg.data_ptr  = (CarbonUInt8*) & data;
    trans_cfg.length    = 4;
    trans_cfg.burstType = CarbonXAhbTrans_INCR;
    trans_cfg.size      = CarbonXAhbTrans_SIZE32;
    trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 3;  // Slave wait mode, actually not set(used) by master xtor

    // Create a burst write transaction
    write(trans_cfg);

    // Go to wait for writes state
    mTestState = eWaitWriteDoneIncrSlaveWait3;

    break;

  case eWaitWriteDoneIncrSlaveWait3 :
    cout << "\teWaitWriteDoneIncrSlaveWait3 -" << endl;

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eReadMemIncrSlaveWait3;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }

    mTransReceived = false; // Clear receive flag
    break;

  case eReadMemIncrSlaveWait3 :
    cout << "\teReadMemIncrSlaveWait3 -" << endl;

    //AhbTransConfig_t trans_cfg;

    // Setup the read inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_READ;
    trans_cfg.addr      = 0x200;
    trans_cfg.data_ptr  = NULL;
    trans_cfg.length    = 4;
    trans_cfg.burstType = CarbonXAhbTrans_INCR;
    trans_cfg.size      = CarbonXAhbTrans_SIZE32;
    trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    read(trans_cfg);

    // Go to wait for reades state
    mTestState = eWaitReadDoneIncrSlaveWait3;
    break;

  case eWaitReadDoneIncrSlaveWait3 :
    cout << "\teWaitReadDoneIncrSlaveWait3 -" << endl;

    // When a read transaction is done, check the result
    if(mTransReceived)
      {
	// retrieve trans addr and data
	CarbonUInt64 addr = carbonXAhbTransGetAddress(mLastReceivedTrans);
	CarbonUInt8 * data_ptr = carbonXAhbTransGetDataPtr(mLastReceivedTrans);

	// 1st data item
	CarbonUInt32 data = ((CarbonUInt32 *)(data_ptr))[0];
	if (data != 0x13121110)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr
		 << " Expected: 0x13121110 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	mTransReceived = false;
      }

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eWriteMemIncr2SlaveWait3;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }
    break;

  case eWriteMemIncr2SlaveWait3:
    cout << "\teWriteMemIncr2SlaveWait3 -" << endl;

    data[0] = 0x63626160;
    data[1] = 0x73727170;
    data[2] = 0xdeadbeef;
    data[3] = 0xdeadbeef;

    // Setup the write inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_WRITE;
    trans_cfg.addr      = 0x200;
    trans_cfg.data_ptr  = (CarbonUInt8*) & data;
    trans_cfg.length    = 8;
    trans_cfg.burstType = CarbonXAhbTrans_INCR;
    trans_cfg.size      = CarbonXAhbTrans_SIZE32;
    trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 3;  // Slave wait mode, actually not set(used) by master xtor

    // Create a burst write transaction
    write(trans_cfg);

    // Go to wait for writes state
    mTestState = eWaitWriteDoneIncr2SlaveWait3;

    break;

  case eWaitWriteDoneIncr2SlaveWait3 :
    cout << "\teWaitWriteDoneIncr2SlaveWait3 -" << endl;

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eReadMemIncr2SlaveWait3;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }

    mTransReceived = false; // Clear receive flag
    break;

  case eReadMemIncr2SlaveWait3 :
    cout << "\teReadMemIncr2SlaveWait3 -" << endl;

    //AhbTransConfig_t trans_cfg;

    // Setup the read inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_READ;
    trans_cfg.addr      = 0x200;
    trans_cfg.data_ptr  = NULL;
    trans_cfg.length    = 8;
    trans_cfg.burstType = CarbonXAhbTrans_INCR;
    trans_cfg.size      = CarbonXAhbTrans_SIZE32;
    trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
    trans_cfg.master_wait_states = 0;
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    read(trans_cfg);

    // Go to wait for reades state
    mTestState = eWaitReadDoneIncr2SlaveWait3;
    break;

  case eWaitReadDoneIncr2SlaveWait3 :
    cout << "\teWaitReadDoneIncr2SlaveWait3 -" << endl;

    // When a read transaction is done, check the result
    if(mTransReceived)
      {
	// retrieve trans addr and data
	CarbonUInt64 addr = carbonXAhbTransGetAddress(mLastReceivedTrans);
	CarbonUInt8 * data_ptr = carbonXAhbTransGetDataPtr(mLastReceivedTrans);

	// 1st data item
	CarbonUInt32 data = ((CarbonUInt32 *)(data_ptr))[0];
	if (data != 0x63626160)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr
		 << " Expected: 0x63626160 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 2nd data item
	data = ((CarbonUInt32 *)(data_ptr))[1];
	if (data != 0x73727170)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 4
		 << " Expected: 0x73727170 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	mTransReceived = false;
      }

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eWriteMemIncr4MasterBusy3;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }
    break;

  case eWriteMemIncr4MasterBusy3:
    cout << "\teWriteMemIncr4MasterBusy3 -" << endl;

    data[0] = 0xa3a2a1a0;
    data[1] = 0xb3b2b1b0;
    data[2] = 0xc3c2c1c0;
    data[3] = 0xd3d2d1d0;

    // Setup the write inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_WRITE;
    trans_cfg.addr      = 0x300;
    trans_cfg.data_ptr  = (CarbonUInt8*) & data;
    trans_cfg.length    = 16;
    trans_cfg.burstType = CarbonXAhbTrans_INCR4;
    trans_cfg.size      = CarbonXAhbTrans_SIZE32;
    trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
    trans_cfg.master_wait_states = 3;  // Master busy mode
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    write(trans_cfg);

    // Go to wait for writes state
    mTestState = eWaitWriteDoneIncr4MasterBusy3;

    break;

  case eWaitWriteDoneIncr4MasterBusy3 :
    cout << "\teWaitWriteDoneIncr4MasterBusy3 -" << endl;

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eReadMemIncr4MasterBusy3;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }

    mTransReceived = false; // Clear receive flag
    break;

  case eReadMemIncr4MasterBusy3 :
    cout << "\teReadMemIncr4MasterBusy3 -" << endl;

    //AhbTransConfig_t trans_cfg;

    // Setup the read inc4 transaction
    trans_cfg.access    = CarbonXAhbTrans_READ;
    trans_cfg.addr      = 0x300;
    trans_cfg.data_ptr  = NULL;
    trans_cfg.length    = 16;
    trans_cfg.burstType = CarbonXAhbTrans_INCR4;
    trans_cfg.size      = CarbonXAhbTrans_SIZE32;
    trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
    trans_cfg.master_wait_states = 3;  // Master busy mode
    trans_cfg.slave_wait_states  = 0;

    // Create a burst write transaction
    read(trans_cfg);

    // Go to wait for reades state
    mTestState = eWaitReadDoneIncr4MasterBusy3;
    break;

  case eWaitReadDoneIncr4MasterBusy3 :
    cout << "\teWaitReadDoneIncr4MasterBusy3 -" << endl;

    // When a read transaction is done, check the result
    if(mTransReceived)
      {
	// retrieve trans addr and data
	CarbonUInt64 addr = carbonXAhbTransGetAddress(mLastReceivedTrans);
	CarbonUInt8 * data_ptr = carbonXAhbTransGetDataPtr(mLastReceivedTrans);

	// 1st data item
	CarbonUInt32 data = ((CarbonUInt32 *)(data_ptr))[0];
	if (data != 0xa3a2a1a0)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr
		 << " Expected: 0xa3a2a1a0 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 2nd data item
	data = ((CarbonUInt32 *)(data_ptr))[1];
	if (data != 0xb3b2b1b0)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 4
		 << " Expected: 0xb3b2b1b0 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 3rd data item
	data = ((CarbonUInt32 *)(data_ptr))[2];
	if (data != 0xc3c2c1c0)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 4
		 << " Expected: 0xc3c2c1c0 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	// 4th data item
	data = ((CarbonUInt32 *)(data_ptr))[3];
	if (data != 0xd3d2d1d0)
	  {
	    cout << "Error: Read data mismatch for addr " << hex << addr + 4
		 << " Expected: 0xd3d2d1d0 Received: " << data << dec << endl;
	    ++mErrorCount;
	  }

	mTransReceived = false;
      }

    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0)
      {
	mTestState = eTestDone;

	carbonXAhbTransDestroy(mLastReceivedTrans);
      }
    break;

  case eTestDone :
    cout << "\tTest is done!" << endl;
    mTestDone = true; // Flag that test is done
    break;
  default:
    break;
  }
}

void AhbTest::write(CarbonUInt32 addr, CarbonUInt32 data)
{
  // Setup the write transaction & print out a description for it
  AhbTransConfig_t ahb_trans_cfg;

  ahb_trans_cfg.access    = CarbonXAhbTrans_WRITE;
  ahb_trans_cfg.addr      = addr;
  ahb_trans_cfg.data_ptr  = (CarbonUInt8*) & data;
  ahb_trans_cfg.length    = 4;
  ahb_trans_cfg.burstType = CarbonXAhbTrans_SINGLE;
  ahb_trans_cfg.size      = CarbonXAhbTrans_SIZE32;
  ahb_trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
  ahb_trans_cfg.master_wait_states = 0;
  ahb_trans_cfg.slave_wait_states  = 0;

  AhbTransaction * trans = carbonXAhbTransCreate(ahb_trans_cfg);

  if (trans == NULL)
    cout << "write(addrs, data) failed!" << endl;

  // Once the transaction is created it can be queued to the transactor with the startTransaction method
  carbonXAhbStartNewTransaction(mMasterTrans, trans);

  // Increment, the transaction count, so the test can keep track of outstanding transactions. 
  ++ mTransCount;
}

void AhbTest::write(AhbTransConfig_t trans_cfg)
{

  AhbTransaction * trans = carbonXAhbTransCreate(trans_cfg);

  if (trans == NULL)
    cout << "write(trans) failed!" << endl;

  // Once the transaction is created it can be queued to the transactor with the startTransaction method
  carbonXAhbStartNewTransaction(mMasterTrans, trans);

  // Increment, the transaction count, so the test can keep track of outstanding transactions. 
  ++ mTransCount;
}


// Function to create a read transaction
void AhbTest::read(CarbonUInt32 addr)
{
  // Setup the read transaction & print out a description for it
  AhbTransConfig_t ahb_trans_cfg;

  ahb_trans_cfg.access    = CarbonXAhbTrans_READ;
  ahb_trans_cfg.addr      = addr;
  ahb_trans_cfg.data_ptr  = NULL;
  ahb_trans_cfg.length    = 4;
  ahb_trans_cfg.burstType = CarbonXAhbTrans_SINGLE;
  ahb_trans_cfg.size      = CarbonXAhbTrans_SIZE32;
  ahb_trans_cfg.locked    = CarbonXAhbTrans_UNLOCKED;
  ahb_trans_cfg.master_wait_states = 0;
  ahb_trans_cfg.slave_wait_states  = 0;

  AhbTransaction * trans = carbonXAhbTransCreate(ahb_trans_cfg);

  if (trans == NULL)
    cout << "read(addr) failed!" << endl;

  // Once the transaction is created it can be queued to the transactor with the startTransaction method
  carbonXAhbStartNewTransaction(mMasterTrans, trans);

  ++mTransCount;
}

void AhbTest::read(AhbTransConfig_t trans_cfg)
{

  AhbTransaction * trans = carbonXAhbTransCreate(trans_cfg);

  if (trans == NULL)
    cout << "read(trans) failed!" << endl;

  // Once the transaction is created it can be queued to the transactor with the startTransaction method
  carbonXAhbStartNewTransaction(mMasterTrans, trans);

  // Increment, the transaction count, so the test can keep track of outstanding transactions. 
  ++ mTransCount;
}


void AhbTest::reportTransaction(AhbTransaction * trans)
{
  cout << "AhbTest::reportTransaction(AhbTransaction * trans)" << endl;

  if (carbonXAhbTransGetAccessType(trans) == CarbonXAhbTrans_WRITE) {
    std::cout << "Write transaction done." << std::endl;
  }
  else {
    std::cout << "Read " << ((CarbonUInt32 *)carbonXAhbTransGetDataPtr(trans))[0] 
	      << " from addr " << carbonXAhbTransGetAddress(trans) << std::endl;
  }

  // Store transaction, and indicate reception
  mLastReceivedTrans = carbonXAhbTransCreateCopy(trans);
  mTransReceived     = true;

  // Decrement outstanding transaction count;
  --mTransCount;

  // Clean up transaction
  carbonXAhbTransDestroy(trans);
}

int main(int argc, char* argv[])
{
  (void)argc; (void)argv;
  
  // Setup testbench
  AhbTest test;  
  test.setup();

  // Run until evaulates indicates that it has nothing left to do
  bool cont = true;
  CarbonTime simTime = 0;
  for(simTime = 0; cont && simTime < 1600; simTime += 10) {
    cout << "************** " << simTime << " *************" << endl;
    cont = test.evaluate(simTime);
    cout << "---------------------------" << endl;
  }
  // Check if we timed out
  if(simTime == 1600) {
    cout << "Error: Simulation timed out before test was done!" << endl;
    return 1;
  }

  return test.getErrors();
}
