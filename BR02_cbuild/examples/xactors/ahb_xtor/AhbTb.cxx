// -*- mode: c++ -*-

#include <iostream>

#include "AhbTb.h"
#include "libahb_wire.h"


using namespace std;

AhbTb::AhbTb()
  : mTestDone(false)
{
}

AhbTb::~AhbTb()
{
  //  carbonDumpFlush(mWaveID);
  cout << "AhbTb::~AhbTb() " << endl;

  carbonXAhbDestroy(mMasterTrans);
  carbonXAhbDestroy(mSlaveTrans);
  delete mSlaveModel;

  carbonDestroy(&mCarbonID);
}

bool AhbTb::setup()
{
  // Setup Carbon model
  mCarbonID = carbon_ahb_wire_create(eCarbonFullDB, eCarbon_NoFlags);

  if(mCarbonID == 0){
    std::cout << "Carbon model creation failed." << std::endl;
    return false;
  }

  mSlaveModel = new AhbSlaveModel(mSlaveTrans);

  // Setup the master transactor
  {
    // set up an Ahb master xtor cfg
    AhbConfig_t  xtor_cfg;
    xtor_cfg.xtor_name        = "AhbMasterXtor";
    xtor_cfg.xtor_type        = CarbonXAhb_Master;
    xtor_cfg.addr_width       = 32;
    xtor_cfg.data_width       = 32;
    xtor_cfg.trans_queue_size = 256;
    xtor_cfg.dcd_addr_width   = 0; // not used by master
    xtor_cfg.ahb_lite         = false;

    mMasterTrans = carbonXAhbCreate(xtor_cfg, reportTransactionCB, NULL, NULL, this);

    // Connect the transactor to the Carbon model
    CarbonXInterconnectNameNamePair masterConnectPair[] =
      {
	{"hReset_n", "ahb_wire.hReset_n"},
	{"hClk",     "ahb_wire.hClk"},
	{"hReady",   "ahb_wire.hReadyM"},
	{"hResp",    "ahb_wire.hRespM"},
	{"hRData",   "ahb_wire.hRDataM"},
	{"hBusReq",  "ahb_wire.hBusReq"},
	{"hGrant",   "ahb_wire.hGrant"},
	{"hLock",    "ahb_wire.hLockM"},
	{"hTrans",   "ahb_wire.hTransM"},
	{"hAddr",    "ahb_wire.hAddrM"},
	{"hWrite",   "ahb_wire.hWriteM"},
	{"hSize",    "ahb_wire.hSizeM"},
	{"hBurst",   "ahb_wire.hBurstM"},
	{"hProt",    "ahb_wire.hProtM"},
	{"hWData",   "ahb_wire.hWDataM"},
	{ NULL, NULL }
    };

    bool success = carbonXAhbConnectByModelSignalName(mMasterTrans, masterConnectPair, mCarbonID);

    // Check that connection was successful
    if(!success) {
      std::cout << "AHB Master transactor connection failed." << std::endl;
      return false;
    }
  }

  // Setup the slave transactor
  {
    // set up an Ahb slave xtor cfg
    AhbConfig_t  xtor_cfg;
    xtor_cfg.xtor_name        = "AhbSlaveXtor";
    xtor_cfg.xtor_type        = CarbonXAhb_Slave;
    xtor_cfg.addr_width       = 32;
    xtor_cfg.data_width       = 32;
    xtor_cfg.trans_queue_size = 0;   // not used by slave
    xtor_cfg.dcd_addr_width   = 20;

    mSlaveTrans = carbonXAhbCreate(xtor_cfg, NULL, setDefaultResponseCB, setResponseCB, mSlaveModel);

    // Connect the transactor to the Carbon model
    CarbonXInterconnectNameNamePair slaveConnectPair[] =
      {
	{"hReset_n",  "ahb_wire.hReset_n"},
	{"hClk",      "ahb_wire.hClk"},
	{"hSel",      "ahb_wire.hSel"},
	{"hAddr",     "ahb_wire.hAddr"},
	{"hWrite",    "ahb_wire.hWrite"},
	{"hTrans",    "ahb_wire.hTrans"},
	{"hSize",     "ahb_wire.hSize"},
	{"hBurst",    "ahb_wire.hBurst"},
	{"hWData",    "ahb_wire.hWData"},
	{"hReadyIn",  "ahb_wire.hReadyIn"},
	{"hMaster",   "ahb_wire.hMaster"},
	{"hMastLock", "ahb_wire.hMastLock"},
	{"hReadyOut", "ahb_wire.hReadyOut"},
	{"hResp",     "ahb_wire.hResp"},
	{"hRData",    "ahb_wire.hRData"},
	{"hSplitx",   "ahb_wire.hSplitx"},
	{ NULL, NULL }
      };

    bool success = carbonXAhbConnectByModelSignalName(mSlaveTrans, slaveConnectPair, mCarbonID);

    // Check that connection was successful
    if(!success) {
      std::cout << "AHB Slave transactor connection failed." << std::endl;
      return false;
    }
  }


  // Setup Wave dumping
  mWaveID = carbonWaveInitVCD(mCarbonID, "waves.vcd", e1ns);
  carbonDumpVars(mWaveID, 0, "ahb_wire");

  // Now if we made it this far we have successfully setup the environment
  return true;
}

bool AhbTb::evaluate(CarbonTime currTime)
{
  // Execute test and transactor
  testFunction();

  // Note the timing of the calls here. All transactors should be evaluated
  // before their output signals are driven out. This way the transactors works
  // as if their outputs are registers. I.e, the transactor is reading their
  // inputs before the edge, but updating the outputs after the edge, and the order
  // of the evaluates stay independent.

  // Refresh inputs from Carbon Model
  carbonXAhbRefreshInputs(mMasterTrans);
  carbonXAhbRefreshInputs(mSlaveTrans);


  // Evaluate transactors.
  carbonXAhbEvaluate(mMasterTrans);
  carbonXAhbEvaluate(mSlaveTrans);


  carbonXAhbRefreshOutputs(mMasterTrans);
  carbonXAhbRefreshOutputs(mSlaveTrans);

  cout << "carbonSchedule: " << currTime << endl;
  carbonSchedule(mCarbonID, currTime);


  // Evaluate Timer Model
  mSlaveModel->evaluate();

  // We want to continue to evaluate until the test is done
  if(mTestDone) return false;
  else          return true;
}

//! Report Transaction Callback function for master transactor
void AhbTb::reportTransactionCB(AhbTransaction* trans, void* clientData)
{
  AhbTb* that = reinterpret_cast<AhbTb*>(clientData);
  that->reportTransaction(trans);
}

//! Set Default Response Callback function for slave transactor
void AhbTb::setDefaultResponseCB(AhbTransaction* trans, void* clientData)
{
  AhbSlaveModel* that = reinterpret_cast<AhbSlaveModel*>(clientData);
  that->setDefaultResponse(trans);
}
  
//! Set Default Response Callback function for slave transactor
void AhbTb::setResponseCB(AhbTransaction* trans, void* clientData)
{
  AhbSlaveModel* that = reinterpret_cast<AhbSlaveModel*>(clientData);
  that->setResponse(trans);
}
