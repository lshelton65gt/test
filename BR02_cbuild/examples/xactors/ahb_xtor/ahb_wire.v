module ahb_wire (
  hReset_n,
  hClk,

  // ahb_wire <-> ahb target
  hSel,
  hAddr,
  hWrite,
  hTrans,
  hSize,
  hBurst,
  hWData, 
  hReadyIn,
  hMaster,
  hMastLock,

  hReadyOut,
  hResp,
  hRData,
  hSplitx,

  // ahb initiator <-> ahb_wire
  hReadyM,
  hRespM,
  hRDataM,

  hGrant,
  hBusReq,

  hLockM,
  hAddrM,
  hWriteM,
  hTransM,
  hSizeM,
  hBurstM,
  hWDataM,
  hProtM
);

input         hReset_n;
input         hClk;

// ahb_wire <-> ahb target
output         hSel;
output [31:0]  hAddr;
output         hWrite;
output [ 1:0]  hTrans;
output [ 2:0]  hSize;
output [ 2:0]  hBurst;
output [31:0]  hWData;
output         hReadyIn;
output [ 3:0]  hMaster;
output         hMastLock;

input          hReadyOut;
input  [ 1:0]  hResp;
input  [31:0]  hRData;
input  [15:0]  hSplitx;

// ahb initiator <-> ahb_wire
output         hReadyM;
output [ 1:0]  hRespM;
output [31:0]  hRDataM;

output         hGrant;
input          hBusReq;

input          hLockM;
input  [31:0]  hAddrM;
input          hWriteM;
input  [ 1:0]  hTransM;
input  [ 2:0]  hSizeM;
input  [ 2:0]  hBurstM;
input  [31:0]  hWDataM;
input  [ 3:0]  hProtM;

parameter IDLE   = 2'h0;
parameter BUSy   = 2'h1;
parameter NONSEQ = 2'h2;
parameter SEQ    = 2'h3;

parameter OKAY   = 2'h0;
parameter ERROR  = 2'h1;
parameter RETRY  = 2'h2;
parameter SPLIT  = 2'h3;

reg            hGrant;
reg    [ 3:0]  master;
reg    [ 3:0]  split_master;
reg            split;
wire           grant;

reg    [15:0]  count;


//assign hSel     = (hAddr[31:10] == 22'h200000);
assign hSel     = hTransM != 0;
assign hAddr    = hAddrM;
assign hWrite   = hWriteM;
assign hTrans   = hTransM;
assign hSize    = hSizeM;
assign hBurst   = hBurstM;
assign hWData   = hWDataM;
assign hReadyIn = hReadyOut;

assign hReadyM = hReadyOut;
assign hRespM  = hResp;
assign hRDataM = hRData;

assign grant = split ? 0 : hBusReq;

reg grant2;
reg grant3;

always @ (count or hSel or hTrans or hAddr or hBurst or hBusReq)
begin
  if (count < 3)
    grant3 = 0;
  else if (hAddr[3:0] == 8 && count == 3)
    grant3 = 0;
  else if (hTrans == SEQ && hBurst == 5)
    grant3 = 1;
  else
    grant3 = hBusReq;
end

always @ (hBusReq or hBurst or hSel or count)
begin
  if (count < 2)
    grant2 = 0;
  else if (hSel && (hBurst == 0))
    grant2 = 0;
  else if (hBusReq)
    grant2 = 1;
  else
    grant2 = 0;
end

always @ (count or hBusReq or grant or grant2 or grant3)
begin
  if (count == 1)
     hGrant = grant;
  else if (count == 2)
     hGrant = grant2;
  else if (count == 3)
     hGrant = grant3;
end

always @ (hTrans or hGrant or hReadyM or hAddr)
begin
  if (hTrans == NONSEQ && hGrant && hReadyM)
    master = hAddr >> 8;
end

always @ (hRespM or hSplitx or master or split_master or hTrans or count or hReadyM or hBurst)
begin
  if (hTrans == IDLE && count == 0)
    begin
      count = 1;
      split = 0;
    end
  else if (hRespM == ERROR && hReadyM)
    count = 2;
  else if (hTrans == NONSEQ && hBurst == 5)
    count = 3;
  else if (hRespM == SPLIT)
    begin
      split = 1;
      split_master = master;
    end
  else if (hSplitx[split_master] == 1)
    split = 0;
end

assign hMastLock = 0;
assign hMaster = master;


endmodule
