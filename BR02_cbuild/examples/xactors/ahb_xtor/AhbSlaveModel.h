// -*- mode: c++ -*-

#ifndef AHBSLAVEMODEL_H
#define AHBSLAVEMODEL_H


#include "carbon/carbon_capi.h"


#define AHB_MEMORY_BASE    0x400
#define AHB_MEMORY_SIZE    0x400
#define AHB_ADDR_SIZE         10


class AhbTransaction;
class AhbAbsXtor;

class AhbSlaveModel
{
public:
  AhbSlaveModel(AhbAbsXtor * xtor);
  ~AhbSlaveModel();

  void setDefaultResponse(AhbTransaction * trans);
  // This is the Set Default Response callback for the APB3_Slave transactor.

  void setResponse(AhbTransaction * trans);
  // This is the Set Response callback for the APB3_Slave transactor.

  void write(CarbonUInt64 addr, CarbonUInt32 * data);
  void read(CarbonUInt64 addr, CarbonUInt32 * data);

  void evaluate();
  // Evaluate method should be called every cycle

private:
  void write(CarbonUInt64 addr, CarbonUInt8 * data, CarbonUInt32 byte_size);
  void read(CarbonUInt64 addr, CarbonUInt8 * data, CarbonUInt32 byte_size);

  CarbonUInt8 * _memory;
  // memory space for this model

  AhbAbsXtor * _xtor;
  // xtor to which this model is attached to

  CarbonUInt32 _status;
  // this is here to set up slave wait states for testing purpose
};


#endif
