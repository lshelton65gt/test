// -*- mode: c++ -*-

#include <iostream>

#include "AhbSlaveModel.h"
#include "xactors/ahb_xtor/carbonXAhb.h"

using namespace std;


AhbSlaveModel::AhbSlaveModel(AhbAbsXtor * xtor)
  : _xtor (xtor),
    _status (0)
{
  _memory = new CarbonUInt8[0x800];
}

AhbSlaveModel::~AhbSlaveModel()
{
  delete [] _memory;
}


void AhbSlaveModel::evaluate()
{
  cout << "AhbSlaveModel::evaluate(): To be supplied by user if any!" << endl;
}

// This is called when the slave xtor detects starting of a transaction
void AhbSlaveModel::setDefaultResponse(AhbTransaction * trans)
{
  cout << "AhbSlaveModel::setDefaultResponse(AhbTransaction * trans): TBD" << endl;

  CarbonUInt64  addr        = carbonXAhbTransGetAddress(trans);
  CarbonUInt8 * data_ptr    = carbonXAhbTransGetDataPtr(trans);
  CarbonUInt32  data_length = carbonXAhbTransGetExpDataLength(trans);

  CarbonUInt64 alignment_mask    = carbonXAhbTransGetAlignmentMask(trans);
  CarbonUInt64 start_base_addr   = addr & alignment_mask;
  CarbonUInt64 start_offset_addr = addr & (~ alignment_mask);

  if ((carbonXAhbTransGetBurstType(trans) == CarbonXAhbTrans_INCR))
    {
      carbonXAhbTransSetSlaveWaitStates(trans, 3);

      _status ++;
    }



  // for read trans, fill in the READ DATA according to read request appropriately
  if (carbonXAhbTransGetAccessType(trans) == CarbonXAhbTrans_READ)
    {
      switch (carbonXAhbTransGetBurstType(trans))
	{
	case CarbonXAhbTrans_SINGLE:
	case CarbonXAhbTrans_INCR:
	case CarbonXAhbTrans_INCR4:
	case CarbonXAhbTrans_INCR8:
	case CarbonXAhbTrans_INCR16:
	  for(CarbonUInt32 i = 0; i < data_length; i++)
	    data_ptr[i] = _memory[addr + i];
	  break;

	case CarbonXAhbTrans_WRAP4:
	case CarbonXAhbTrans_WRAP8:
	case CarbonXAhbTrans_WRAP16:
	  for(CarbonUInt32 i = 0; i < data_length; i++)
	    data_ptr[i] = _memory[start_base_addr + ((start_offset_addr + i) & (~ alignment_mask))];
	  break;

	default:
	  cout << "ERROR: Unsupported burst type - " << carbonXAhbTransGetBurstType(trans) << endl;
	  break;
	}
    }

  // nothing to be done for a write trans
}

// This is called when slave ended a transaction on AHB bus.
void AhbSlaveModel::setResponse(AhbTransaction* trans)
{
  cout << "AhbSlaveModel::setResponse(AhbTransaction* trans)" << endl;

  CarbonUInt64  addr        = carbonXAhbTransGetAddress(trans);
  CarbonUInt8 * data_ptr    = carbonXAhbTransGetDataPtr(trans);
  CarbonUInt32  data_length = carbonXAhbTransGetDataLength(trans);

  CarbonUInt64 alignment_mask    = carbonXAhbTransGetAlignmentMask(trans);
  CarbonUInt64 start_base_addr   = addr & alignment_mask;
  CarbonUInt64 start_offset_addr = addr & (~ alignment_mask);

  // When READ trans completes, read data stored in trans structure have been
  //  transferred on AHB bus.

  // When WRITE completes, write data need to be off-loaded to slave model memory
  if (carbonXAhbTransGetAccessType(trans) == CarbonXAhbTrans_WRITE)
    {
      switch (carbonXAhbTransGetBurstType(trans))
	{
	case CarbonXAhbTrans_SINGLE:
	case CarbonXAhbTrans_INCR:
	case CarbonXAhbTrans_INCR4:
	case CarbonXAhbTrans_INCR8:
	case CarbonXAhbTrans_INCR16:
	  for(CarbonUInt32 i = 0; i < data_length; i++)
	    _memory[addr + i] = data_ptr[i];
	  break;

	case CarbonXAhbTrans_WRAP4:
	case CarbonXAhbTrans_WRAP8:
	case CarbonXAhbTrans_WRAP16:
	  for(CarbonUInt32 i = 0; i < data_length; i++)
	    {
	      _memory[start_base_addr + ((start_offset_addr + i) & (~ alignment_mask))] = data_ptr[i];
	    }
	  break;

	default:
	  cout << "ERROR: Unsupported burst type - " << carbonXAhbTransGetBurstType(trans) << endl;
	  break;
	}
    }
}
