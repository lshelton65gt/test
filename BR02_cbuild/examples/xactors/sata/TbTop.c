#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "xactors/sata/carbonXSataLL.h"
#include "libTbTop.h"

static void reportTransCb(CarbonXSataLLTrans*, void*);
static void hostResponseCb(CarbonXSataLLTrans*, void*);
static void deviceResponseCb(CarbonXSataLLTrans*, void*);

int main()
{
    int i;
    CarbonTime t = 0;
    int clocktick = 0;
    /*
    CarbonUInt32 one = 1;
    CarbonUInt32 zero = 0;
    */
    CarbonNetID *SystemClock;
    CarbonWaveID *vcd;

    CarbonXSataLLID host, device;
    CarbonXSataLLTrans *trans;
    CarbonUInt32 data[32];

    /* create glue logic for back-to-back transactor connection */
    CarbonObjectID *TbTop =
      carbon_TbTop_create(eCarbonIODB, eCarbon_NoFlags);
    if (TbTop == NULL)
    {
        printf("ERROR: Unable to create Carbon model ...\n");
        exit(EXIT_FAILURE);
    }
    vcd = carbonWaveInitVCD(TbTop, "TbTop.vcd", e1us);

    /* Get handles to system clock */
    SystemClock = carbonFindNet(TbTop, "TbTop.SystemClock");
    assert(SystemClock);

    /* create SATA LL Host Transactor */
    host = carbonXSataLLCreate("HOST", CarbonXSataLL_Host, 5,
            &reportTransCb, NULL, &hostResponseCb, NULL, NULL, NULL, &host);
    if (host == NULL)
    {
        printf("ERROR: Unable to create SATA LL Host Transactor ...\n");
        exit(EXIT_FAILURE);
    }

    /* create SATA LL Device Transactor */
    device = carbonXSataLLCreate("DEVICE", CarbonXSataLL_Device, 5,
            &reportTransCb, NULL, &deviceResponseCb, NULL, NULL, NULL, &device);
    if (device == NULL)
    {
        printf("ERROR: Unable to create SATA LL Device Transactor ...\n");
        exit(EXIT_FAILURE);
    }

    /* connect host transactor to Carbon Model */
    {
        CarbonXInterconnectNameNamePair hostConnectn[] = {
            { "COMRESET", "Host_COMRESET" },
            { "COMWAKE", "Host_COMWAKE" },
            { "PhyRdy", "Host_PhyRdy" },
            { "DeviceDetect", "Host_DeviceDetect" },
            { "PhyInternalError", "Host_PhyInternalError" },
            { "COMMA", "Host_COMMA" },
            { "PARTIAL", "Host_PARTIAL" },
            { "SLUMBER", "Host_SLUMBER" },
            { "DATAOUT", "Host_DATAOUT" },
            { "DATAIN", "Host_DATAIN" },
            { NULL /* NULL here indicates end of list */, NULL } };
        carbonXSataLLConnectByModelSignalName(host, hostConnectn,
            "TbTop", TbTop);
    }
    /* connect device transactor to Carbon Model */
    {
        CarbonXInterconnectNameNamePair deviceConnectn[] = {
            { "COMRESET", "Device_COMRESET" },
            { "COMWAKE", "Device_COMWAKE" },
            { "PhyRdy", "Device_PhyRdy" },
            { "DeviceDetect", "Device_DeviceDetect" },
            { "PhyInternalError", "Device_PhyInternalError" },
            { "COMMA", "Device_COMMA" },
            { "PARTIAL", "Device_PARTIAL" },
            { "SLUMBER", "Device_SLUMBER" },
            { "DATAOUT", "Device_DATAOUT" },
            { "DATAIN", "Device_DATAIN" },
            { NULL /* NULL here indicates end of list */, NULL } };
        carbonXSataLLConnectByModelSignalName(device, deviceConnectn,
            "TbTop", TbTop);
    }

    /* start some transactions */
    trans = carbonXSataLLTransCreate();
    carbonXSataLLTransSetType(trans, CarbonXSataLLTrans_Frame);
    for (i = 0; i < 10; i++)
        data[i] = i;
    carbonXSataLLTransSetFrameData(trans, 10, data);
    printf("**** HOST: Starting a frame transaction with data 0 .. 9.\n");
    carbonXSataLLStartNewTransaction(host, trans);

    /* Dump all nets */
    carbonDumpVars (vcd, 1, "TbTop");

    /* simulation */
    for (t = 0; t < 40000; t += 10)
    {
        if ((t % 100) == 0)
        {
            CarbonUInt32 val;
            ++clocktick;
            val = clocktick & 1;
            carbonDeposit(TbTop, SystemClock, &val, NULL);

            if (val) /* at posedge of clock */
            {
                /* refreshes input Carbon Model connections of
                 * host and device transactors */
                carbonXSataLLRefreshInputs(host, t);
                carbonXSataLLRefreshInputs(device, t);

                /* evaluate host and device transactors */
                carbonXSataLLEvaluate(host, t);
                carbonXSataLLEvaluate(device, t);

                carbonSchedule(TbTop, t);

                /* refreshes output Carbon Model connections of
                 * host and device transactors */
                carbonXSataLLRefreshOutputs(host, t);
                carbonXSataLLRefreshOutputs(device, t);
            }
            else
                carbonSchedule(TbTop, t);
        }
        else
            carbonSchedule(TbTop, t);
    }

    assert (carbonDumpFlush (vcd) == eCarbon_OK);
    carbonDestroy(&TbTop);
    carbonXSataLLDestroy(host);
    carbonXSataLLDestroy(device);
    return 0;
}

void reportTransCb(CarbonXSataLLTrans *txTrans, void *userData)
{
    CarbonXSataLLID xtor = *((CarbonXSataLLID*) userData);
    if (carbonXSataLLTransGetType(txTrans) == CarbonXSataLLTrans_Frame)
    {
        if (carbonXSataLLTransGetTransStatus(txTrans) ==
                CarbonXSataLLTransStatus_Ok)
        {
            printf("**** %s: Frame transmission successful.\n",
                    carbonXSataLLGetName(xtor));
        }
        else
        {
            printf("**** %s: Error in frame transmission.\n",
                    carbonXSataLLGetName(xtor));
        }
    }
    carbonXSataLLTransDestroy(txTrans);
}

void responseCb(int transmitBack, CarbonXSataLLTrans *rxTrans, void *userData)
{
    CarbonXSataLLID xtor = *((CarbonXSataLLID*) userData);
    if (carbonXSataLLTransGetType(rxTrans) == CarbonXSataLLTrans_Frame)
    {
        if (carbonXSataLLTransGetTransStatus(rxTrans) ==
                CarbonXSataLLTransStatus_Ok)
        {
            CarbonUInt32 i;
            CarbonUInt32 size = carbonXSataLLTransGetFrameSize(rxTrans);
            CarbonUInt32 *data = carbonXSataLLTransGetFrameData(rxTrans);
            printf("**** %s: Received frame of size %d.\n",
                    carbonXSataLLGetName(xtor), size);
            for (i = 0; i < size; i++)
            {
                printf("Received %d data: %d\n", i, data[i]);
            }
            if (transmitBack)
            {
                CarbonXSataLLTrans *txTrans = carbonXSataLLTransCreate();
                carbonXSataLLTransSetType(txTrans, CarbonXSataLLTrans_Frame);
                carbonXSataLLTransSetFrameData(txTrans, size, data);
                carbonXSataLLStartNewTransaction(xtor, txTrans);
            }
        }
        else
        {
            printf("**** %s: Error in frame reception.\n",
                    carbonXSataLLGetName(xtor));
        }
    }
}

void hostResponseCb(CarbonXSataLLTrans *rxTrans, void *userData)
{
    responseCb(0, rxTrans, userData);
}

void deviceResponseCb(CarbonXSataLLTrans *rxTrans, void *userData)
{
    responseCb(1, rxTrans, userData);
}

