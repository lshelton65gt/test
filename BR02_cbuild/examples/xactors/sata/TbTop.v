/**** Glue Logic connecting Host and Device Transactors ****/
module TbTop(SystemClock,
        /* Host Connections */
        Host_COMRESET,
        Host_COMWAKE,
        Host_PhyRdy,
        Host_DeviceDetect,
        Host_PhyInternalError,
        Host_COMMA,
        Host_PARTIAL,
        Host_SLUMBER,
        Host_DATAOUT,
        Host_DATAIN,
        /* Device Connections */
        Device_COMRESET,
        Device_COMWAKE,
        Device_PhyRdy,
        Device_DeviceDetect,
        Device_PhyInternalError,
        Device_COMMA,
        Device_PARTIAL,
        Device_SLUMBER,
        Device_DATAOUT,
        Device_DATAIN);

input SystemClock;

input Host_COMRESET;
input Host_COMWAKE;
input Host_PhyRdy;
input Host_DeviceDetect;
input Host_PhyInternalError;
input Host_COMMA;
output Host_PARTIAL;
output Host_SLUMBER;
input [9:0] Host_DATAOUT;
output [9:0] Host_DATAIN;

input Device_COMRESET;
input Device_COMWAKE;
input Device_PhyRdy;
input Device_DeviceDetect;
input Device_PhyInternalError;
input Device_COMMA;
output Device_PARTIAL;
output Device_SLUMBER;
input [9:0] Device_DATAOUT;
output [9:0] Device_DATAIN;

assign Host_DATAIN = Device_DATAOUT;
assign Device_DATAIN = Host_DATAOUT;

assign Host_PARTIAL = 1'b0;
assign Host_SLUMBER = 1'b0;
assign Device_PARTIAL = 1'b0;
assign Device_SLUMBER = 1'b0;

endmodule

