`timescale 1ns/100ps

module ddrTb;

   wire [15:0] dq;     // inout
   wire [1:0]  dqs;    // inout
   wire [12:0] addr;   // input to carbonx_ddrsdram           // output from carbonx_ddrsdram_ctrl
   wire [1:0]  ba;     // input to carbonx_ddrsdram           // output from carbonx_ddrsdram_ctrl
   wire        cke;    // input to carbonx_ddrsdram           // output from carbonx_ddrsdram_ctrl
   wire        cs_n;   // input to carbonx_ddrsdram           // output from carbonx_ddrsdram_ctrl
   wire        cas_n;  // input to carbonx_ddrsdram           // output from carbonx_ddrsdram_ctrl
   wire        ras_n;  // input to carbonx_ddrsdram           // output from carbonx_ddrsdram_ctrl
   wire        we_n;   // input to carbonx_ddrsdram           // output from carbonx_ddrsdram_ctrl
   wire [1:0]  dm;     // input to carbonx_ddrsdram           // output from carbonx_ddrsdram_ctrl

   //***************************************************************************
   //                     ddr_ctl  Ports Definitions
   //***************************************************************************  
   wire        reset_n;         // carbon depositSignal
               
   wire        clk;
   wire        clk_n = ~clk;    // input to carbonx_ddrsdram

   // -----------------------------------------------   
   carbonx_ddrsdram #(256, 16) DDR_1 
     ( .dq(dq),
       .dqs(dqs),
       .addr(addr),
       .ba(ba),
       .clk(clk),
       .clk_n(clk_n),
       .cke(cke),
       .cs_n(cs_n),
       .cas_n(cas_n),
       .ras_n(ras_n),
       .we_n(we_n),
       .dm(dm)
       );

   carbonx_ddrsdram_ctrl DDRCTL_1 
     ( .clk(clk),            // System signals
       .reset_n(reset_n),
      
       .dq(dq),             // DDR SDRAM interface
       .dqs(dqs), 
       .addr(addr), 
       .ba(ba), 
       .cke(cke), 
       .cs_n(cs_n), 
       .cas_n(cas_n), 
       .ras_n(ras_n), 
       .we_n(we_n), 
       .dm(dm)
       );
   
endmodule
