//
// test for DDR memory transactor
//

#include "libdesign.h"   /* generated header file */
#include "xactors/xactors.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <time.h>
#include <iostream>

#define SIM_MAX_TICKS 10000L


#include <string>
#include <map>



/*
** These are some utility functions
*/
void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time);
void printHeader(char*);
void passfail(void);
void master_func(CarbonMemoryID*);
CarbonUInt32 backdoorRead(CarbonMemoryID * mem, CarbonUInt32 ba, CarbonUInt32 ra, CarbonUInt32 ca);
void backdoorWrite(CarbonMemoryID * mem, CarbonUInt32 addr, CarbonUInt32 data);

const int CLOCK_CYCLE = 10;
int myERROR = 0;
CarbonUInt32 ONE  = 1;
CarbonUInt32 ZERO = 0;

const int rstTime = 500;    // number of clocks to assert reset
 
// Main function
int main(int argc, char *argv[]) 
{

  printHeader("DDR (16Meg x 16)  testbench -- starting ");

  // Create Sim Infrastructure.  Master, Clocks, and Reset

  // Handle to Carbon Model
  CarbonObjectID* model = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  
  printf("Start writing VCD file\n");
  CarbonWaveID* wave = carbonWaveInitVCD(model, "ddrTb.vcd", e1ns);
  if(carbonDumpVars(wave, 6, "ddrTb") != eCarbon_OK)    assert(0);

  // get verilog side Clock & Reset signals
  CarbonNetID*    clock  = carbonFindNet(model, "ddrTb.DDRCTL_1.ddr_clk");
  CarbonNetID*    resetn = carbonFindNet(model, "ddrTb.reset_n"); 
  CarbonMemoryID* mem    = carbonFindMemory(model, "ddrTb.DDR_1.ram_array");
  
  printf("main: asserting reset_n for %d clocks\n", rstTime);
  assert(resetn != NULL);
  carbonDeposit(model, resetn, &ZERO, NULL);
  
  long numTicks = 0;
  CarbonTime time = 0;
  for (int i=0; i<rstTime; i++)
    {
      run(model, clock, time);
      
    }
  
  numTicks = time / CLOCK_CYCLE;
  printf("main: DEASSERTING reset_n at cycle %d\n", numTicks);
  carbonDeposit(model, resetn, &ONE, NULL);
  
  printf("\n****** Starting Simulation ******\n");
 
  // Connect an SVC control function to a specific SVC instance
  carbonXAttach("DDRCTL_1", (CarbonXAttachFuncT)master_func,                   1, mem);
  
  tbp_set_max_error_count  (1);
  
  while (    (numTicks < SIM_MAX_TICKS) 
	     && (!carbonXCheckSignalState(TEST_COMPLETE))  )
    {
      run(model, clock, time);
      numTicks = time / CLOCK_CYCLE;
      
      //if (      (numTicks < 1000) 
      //	|| ( (numTicks > 1000) && (numTicks <= 10000) && ((numTicks % 1000) == 0)  )
      //	|| ( (numTicks > 10000) && ((numTicks % 10000) == 0)  )
      //	)  
	//printf("Main:  cycle %4d\n", numTicks);    
    }
  
  if (numTicks < SIM_MAX_TICKS)
    printf("============= main:  TEST_COMPLETE detected ===============\n");
  else
    printf(" === Error: Simulation exceeded time limit of %-6d ticks (SIM_MAX_TICKS)\n\n",
	   SIM_MAX_TICKS);  
  
  
  if (carbonDumpFlush(wave) != eCarbon_OK) assert(0);
  
  
  printf( "DDR (16Meg x 16) Simulation complete at cycle: %d ",
	  (time / CLOCK_CYCLE) );
  
  passfail();
  carbonDestroy(&model);
  
}    // end of main



//  ******
//    Run       run clock one clock period.
//  ******
void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time)
{
  carbonDeposit(model, clock, &ZERO, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
  carbonDeposit(model, clock, &ONE, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
}



/*
 *  print a header line that includes the time of day
 *
 */
void printHeader(char *strInfo)
{
  time_t      curtime          = time (NULL);          // Get the current time.
  struct tm  *loctime          = localtime (&curtime); // Convert it to local time representation.  
  char       *dtString         = asctime(loctime);     // convert to ascii string
  dtString[strlen(dtString)-1] = '\0';                 // remove the \n at the end of dtString
  printf("======= %s  at %s =========\n\n", strInfo, dtString);
}


/****  D A T A     C O M P A R E     function  ****/

bool DataCompare( CarbonUInt64 dataR, CarbonUInt64 dataX )
{
   if (dataX == dataR)
     { printf("Received data: %08llx EQUALS expected data: %08llx\n", dataR, dataX);
       return true;
     }
   else
     { MSG_Error("ERROR Received data: %08llx NOT equal to expected data: %08llx\n", dataR, dataX); 
       myERROR++;
       return false;
     }
}


bool DataCompare( CarbonUInt64 dataR, CarbonUInt64 dataX, CarbonUInt32 Addr )
{
   if (dataX == dataR)
     { printf("Addr[0x%0x]: %08llx EQUALS expected data: %08llx\n", Addr, dataR, dataX);
       return true;
     }
   else
     { MSG_Error("Addr[0x%0x] = %08llx NOT equal to expected data: %08llx\n", Addr, dataR, dataX); 
       myERROR++;
       return false;
     }
}

void passfail(void)
{

                   printf ("\n\n**********\n");
     if (myERROR == 0) printf ("** PASS **\n");
     else              printf ("** FAIL **   error count: %d\n", myERROR);
                       printf ("**********\n");
}                        



void master_func(CarbonMemoryID* mem) {

  u_int64 foo;
  u_int16 read16;
  u_int32 ba, ra, ca;

  CarbonUInt32 dataPatArray[4];
  CarbonUInt32 dataReadArray[4];
  CarbonUInt32 addrArray;

  printf("Starting master_func\n");
  
  // Need to wait for a while so that the memory model transactor is out of reset
  carbonXIdle(150);


  printf("***\n*** CAS Latency 2, Burst Length 4\n***\n");

  // Initializing DDR SDRAM
  carbonXDdrsdramCtrlSetModeReg( (2<<4) | (0<<3) | 2); // CAS LAT 2, Sequential mode, Burst Length 4

  CarbonUInt64 dataPat1 = 0x1111222233334444LL;
  CarbonUInt32 addr1    = 0x320;
  carbonXDdrsdramCtrlWrite(addr1, dataPat1);
  printf("master_func: carbonXDdrsdramCtrlWrite[0x%0x] = %llx\n", addr1, dataPat1);  

  foo = carbonXDdrsdramCtrlRead(addr1);
  DataCompare(foo, dataPat1, addr1);

  CarbonUInt64 dataPat2 = 0x5555666677778888LL;
  CarbonUInt32 addr2    = 0x4320;     // shadow block ?

  carbonXDdrsdramCtrlWrite(addr1 , dataPat2);
  carbonXDdrsdramCtrlWrite(addr2 , dataPat1);
  
  foo = carbonXDdrsdramCtrlRead(addr1);
  DataCompare(foo, dataPat2);

  foo = carbonXDdrsdramCtrlRead(addr2);
  DataCompare(foo, dataPat1);
  
  // Check Backdoor Read
  // Calculate Bank, Row and Column addresses
  ba = (addr1 >> 22) & 0x3;
  ra = (addr1 >> 9 ) & 0x1fff;
  ca = addr1 & 0x1ff;
  
  // Backdoor read 16 bits at a time and compare data
  foo = 0;
  for(int i = 0; i < 4; i++) {
    read16 = backdoorRead(mem, ba, ra, ca+i);
    DataCompare(read16, (dataPat2 >> (16*i)) & 0xffff, addr1+i);
  }

  dataPatArray[3] = 0x456789AB;
  dataPatArray[2] = 0x3456789A;
  dataPatArray[1] = 0x23456789;
  dataPatArray[0] = 0x12345678;
  dataReadArray[3] = 0;
  dataReadArray[2] = 0;
  dataReadArray[1] = 0;
  dataReadArray[0] = 0;

  addrArray = 0x120;
  carbonXWriteArray32(addrArray, dataPatArray, 8);
  printf("master_func: carbonXWriteArray32[0x%0x] = %x, %x, %x, %x\n", addrArray, dataPatArray[3], dataPatArray[2], dataPatArray[1], dataPatArray[0]);  
  
  read16 = backdoorRead(mem, 0, 0, 0x120);
  printf("master_func:  backdoor read, addr=0x120, data=%x\n", read16);
  if (read16 != (dataPatArray[0] & 0xffff)) MSG_Error("master_func: Backdoor read error\n");
  read16 = backdoorRead(mem, 0, 0, 0x121);
  printf("master_func:  backdoor read, addr=0x121, data=%x\n", read16);
  if (read16 != ((dataPatArray[0]>>16) & 0xffff)) MSG_Error("master_func: Backdoor read error\n");
  read16 = backdoorRead(mem, 0, 0, 0x122);
  printf("master_func:  backdoor read, addr=0x122, data=%x\n", read16);
  if (read16 != (dataPatArray[1] & 0xffff)) MSG_Error("master_func: Backdoor read error\n");
  read16 = backdoorRead(mem, 0, 0, 0x123);
  printf("master_func:  backdoor read, addr=0x123, data=%x\n", read16);
  if (read16 != ((dataPatArray[1]>>16) & 0xffff)) MSG_Error("master_func: Backdoor read error\n");

  carbonXReadArray32(addrArray, dataReadArray, 8);
  printf("master_func: carbonXReadArray32[0x%0x] = %x, %x, %x, %x\n", addrArray, dataReadArray[3], dataReadArray[2], dataReadArray[1], dataReadArray[0]);  
  if (dataReadArray[0] != dataPatArray[0]) MSG_Error("master_func: frontdoor read error\n");
  if (dataReadArray[1] != dataPatArray[1]) MSG_Error("master_func: frontdoor read error\n");




  carbonXSetSignal(TEST_COMPLETE);

  printf("master_func:  signaled test complete\n");
  printf("master_func:  end of function\n");

}

CarbonUInt32 backdoorRead(CarbonMemoryID * mem, CarbonUInt32 ba, CarbonUInt32 ra, CarbonUInt32 ca) {
  CarbonUInt32 addr = (ba << 22) | (ra << 9) | ca;
  CarbonUInt32 ram_data = carbonExamineMemoryWord(mem, addr, 0);
  return ram_data;
}

void backdoorWrite(CarbonMemoryID * mem, CarbonUInt32 addr, CarbonUInt32 data) {
  carbonDepositMemoryWord(mem, addr, data, 0);
}
