/* DDR memory test using CarbonSim library */

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "libdesign.h"   /* generated header file */


#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimClock.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimMemory.h"
#include "carbonsim/CarbonSimStep.h"
#include "carbonsim/CarbonSimObjectInstance.h"


#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <time.h>

#include "carbon/xactors.h"

#define SIM_MAX_TICKS 10000L

#define SIM_Read32(addr) SparseMemReadWord32(mem, addr)
#define SIM_Write32(addr, data) SparseMemWriteWord32(mem, addr, data)


/*
** These are some utility functions
*/
void createClock(CarbonSimMaster*, int, const char*, int initVal = 0);
void bindNet(CarbonSimMaster*, CarbonSimFunctionGen*, const char*);
void printHeader(char*);
void master_func(void);
void passfail(void);

const int CLOCK_CYCLE = 20;
int myERROR = 0;

// Main function
int main(int argc, char *argv[]) 
{
  CarbonUInt32 ONE  = 1;
  CarbonUInt32 ZERO = 0;

  const int rstTime = 100;    // number of clocks to assert reset
 
  printHeader("DDR (16Meg x 16)  testbench -- starting ");

  // Create Sim Infrastructure.  Master, Clocks, and Reset
  // Handle to main simulation object
  CarbonSimMaster* master = CarbonSimMaster::create();
  assert(master);
  
    CarbonSimObjectType*   objType = master->addObjectType("ddrTb", carbon_design_create);	
    CarbonSimObjectInstance* tbObj = master->addInstance(objType, "ddrTb");

    printf("Start writing VCD file\n");
    CarbonWaveID* wave = carbonWaveInitVCD(tbObj->getCarbonObject(), "ddrTb.vcd", e1ns);
    if(carbonDumpVars(wave, 6, "ddrTb") != eCarbon_OK)    assert(0);

    
	
    // Clocks
    createClock(master, CLOCK_CYCLE, "ddrTb.clk", 0);

    // Reset_n
    printf("main: asserting reset_n for %d clocks\n", rstTime);
    CarbonSimNet* resetn = master->findNet("ddrTb.reset_n");
    assert(resetn != NULL);
    resetn->deposit(&ZERO);

    long numTicks = 0;

    for (int i=0; i<rstTime; i++)
      {
        master->run(CLOCK_CYCLE);
	numTicks = master->getTick() / CLOCK_CYCLE;
      }

    printf("main: DEASSERTING reset_n at cycle %d\n", numTicks);
    resetn->deposit(&ONE);

    printf("\n****** Starting Simulation ******\n");

    // Connect an SVC control function to a specific SVC instance

    SIM_Attach("DDRCTL_1", master_func, 0);
    tbp_set_max_error_count  (1);


    while (    (numTicks < SIM_MAX_TICKS) 
               && (!SIM_CheckSignalState(TEST_COMPLETE))  )
      {
	master->run(CLOCK_CYCLE);
	numTicks = master->getTick() / CLOCK_CYCLE;
        
	if (      (numTicks < 1000) 
             || ( (numTicks > 1000) && (numTicks <= 10000) && ((numTicks % 1000) == 0)  )
             || ( (numTicks > 10000) && ((numTicks % 10000) == 0)  )
           )  
            printf("Main:  cycle %4d\n", numTicks);    
      }

    if (numTicks < SIM_MAX_TICKS)
      printf("============= main:  TEST_COMPLETE detected ===============\n");
    else
      printf(" === Error: Simulation exceeded time limit of %-6d ticks (SIM_MAX_TICKS)\n\n",
	     SIM_MAX_TICKS);  
    

    if (carbonDumpFlush(wave) != eCarbon_OK) assert(0);


    printf( "DDR (16Meg x 16) Simulation complete at cycle: %d ",
            (master->getTick() / CLOCK_CYCLE) );
    
    passfail();
    
    delete master;
}

/*
 *  print a header line that includes the time of day
 *
 */
void printHeader(char *strInfo)
{
  time_t      curtime          = time (NULL);          // Get the current time.
  struct tm  *loctime          = localtime (&curtime); // Convert it to local time representation.  
  char       *dtString         = asctime(loctime);     // convert to ascii string
  dtString[strlen(dtString)-1] = '\0';                 // remove the \n at the end of dtString
  printf("======= %s  at %s =========\n\n", strInfo, dtString);
}



/*
** Create a clock, bind the net, add it to the master.
** Clocks will have duty cycle = per / 2
*/
void createClock(CarbonSimMaster* master, int cycle, const char* name, int initVal) {
    CarbonSimClock *clk = new CarbonSimClock(cycle, cycle / 2, initVal);
    master->addClock(clk);
    bindNet(master, clk, name);
}

/*
** bind a net name to a function generator (clock)
** looks up the net, checks the pointer is valid, and does the binding
**
** Should just be used once per bound signal at the begining of the test
*/
void bindNet(CarbonSimMaster* m, CarbonSimFunctionGen* ck, const char* name) {
    CarbonSimNet* n = m->findNet(name);
    assert (n);
    ck->bindSignal(n);
}


/****  D A T A     C O M P A R E     function  ****/

bool DataCompare( uint32 dataR, uint32 dataX )
{
   if (dataX == dataR)
     { printf("Received data: %08x EQUALS expected data: %08x\n", dataR, dataX);
       return true;
     }
   else
     { MSG_Error("ERROR Received data: %08x NOT equal to expected data: %08x\n", dataR, dataX); 
       myERROR++;
       return false;
     }
}


bool DataCompare( uint32 dataR, uint32 dataX, uint32 Addr )
{
   if (dataX == dataR)
     { printf("SIM_Read32[0x%0x]: %08x EQUALS expected data: %08x\n", Addr, dataR, dataX);
       return true;
     }
   else
     { MSG_Error("SIM_Read32[0x%0x] = %08x NOT equal to expected data: %08x\n", Addr, dataR, dataX); 
       myERROR++;
       return false;
     }
}

void passfail(void)
{

                   printf ("\n\n**********\n");
     if (myERROR == 0) printf ("** PASS **\n");
     else              printf ("** FAIL **   error count: %d\n", myERROR);
                       printf ("**********\n");
}                        



void master_func(void) {
  u_int32 foo;
  sparseMem_t mem = SparseMemCreateNew();

  printf("Starting master_func\n");

  SIM_Idle(10);
  printf("master_func:  Idled for 10 cycles; start write\n");

  uint32 dataPat1 = 0x12345678;
  uint32 addr1    = 0x320;
  SIM_Write32(addr1, dataPat1);
  printf("master_func: SIM_Write32[0x%0x] = %0x\n", addr1, dataPat1);  

  foo = SIM_Read32(addr1);
  DataCompare(foo, dataPat1, addr1);

   
  foo = SIM_CsRead(0);
  printf("master_func:  SIM_CsRead(0) =  %08x\n", foo);

  SIM_CsWrite(0, 0x24);
  printf("master_func: SIM_CsWrite(0, 0x24)\n");

  foo = SIM_CsRead(0);
  printf("masterfunc: SIM_CsRead(0) =  %08x\n", foo); 
  DataCompare(foo, 0x24);
   

  uint32 dataPat2 = 0x87654321;
  uint32 addr2    = 0x4320;     // shadow block ?

  SparseMemWriteWord32(mem, addr1 , dataPat2);
  SparseMemWriteWord32(mem, addr2 , dataPat1);
  
  foo = SparseMemReadWord32(mem, addr1);
  DataCompare(foo, dataPat2);

  foo = SparseMemReadWord32(mem, addr2);
  DataCompare(foo, dataPat1);


  // Burst operation & refresh operation ?

  SIM_Idle(10);

  SIM_SetSignal(TEST_COMPLETE);

  printf("master_func:  signaled test complete\n");
  printf("master_func:  end of function\n");

}
