//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
`timescale 1ns / 100 ps

`define DWIDTH 31:0

module xgmiiTb;

//   X G M I I

  wire         sys_clk;           // carbon depositSignal
  wire         sys_reset_l;       // carbon depositSignal

  //  rx xactor   (all inputs)
   wire [`DWIDTH] TXD;       // inputs to gmiiRX
   wire [3:0]     TXC;
   wire           TX_CLK;


  //  tx xactor  (all OUTPUTS)
  wire             RX_CLK; // Receive clock - 125MHz
  wire       [3:0] RXC;    // Receive error
  wire   [`DWIDTH] RXD;    // Receive data


carbonx_enetxgmii_tx
 xmitter
    (sys_clk,   
     sys_reset_l,
 
     RXD,       // output [31:0]       
     RXC,       // output  [3:0]
     RX_CLK     // output

   );


enet_xover
 crossover
   (    
     RXD,       
     RXC,       
     RX_CLK,    

     TXD,      
     TXC,   
     TX_CLK 
   );


carbonx_enetxgmii_rx
  receiver
    (sys_clk,   
     sys_reset_l,
     TXD,      
     TXC,   
     TX_CLK 

   );
  
 endmodule   

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 module enet_xover(
     RXD,       
     RXC,       
     RX_CLK,    

     TXD,      
     TXC, 
     TX_CLK 
   );

 // MII signals - receiver (rx) xactor
   output  [`DWIDTH]  TXD;       // inputs to gmiiRX
   output  [3:0]      TXC;   
   output             TX_CLK;

  // outputs from Transmitter (named RX*) are INPUTS to crossover.
   input             RX_CLK;   // Receive clock - 125MHz
   input [3:0]       RXC;       // Receive control
   input [`DWIDTH]   RXD;      // Receive dat 

  // Transmitter output (named RX*) are INPUTS here.
  // They are output as RX signals (named TX*)
  //  to become INPUTS to Receiver module

   assign            TXD = RXD;     
   assign            TXC = RXC;
   assign            TX_CLK = RX_CLK;


 // always @(TXD)
 //    $display("@ TXD: %0h", TXD);

endmodule
