/*****************************************************************************

 Copyright (c) 2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "xactors/axi/carbonXAxi.h"
#include "libTbTop.h"

#ifndef DATA_BUS_WIDTH
#define DATA_BUS_WIDTH 32 /* Default is 32 bit data bus */
#endif

/* Global variable to hold current simulation time */
CarbonTime t = 0;

/* Forward declarations */
static void setDefaultResponseCb(CarbonXAxiTrans*, void*);
static void setResponseCb(CarbonXAxiTrans*, void*);
static void reportTransCb(CarbonXAxiTrans*, void*);

/* Set the clocks for which indefinite wait will remain active */
static CarbonUInt32 readIndefiniteWaitClocks = 50;
static CarbonUInt32 writeIndefiniteWaitClocks = 50;

/* Main environment */
int main()
{
    int clocktick = 0;

    CarbonXAxiID MasterXtor, SlaveXtor;
    CarbonXAxiTrans* trans;
    CarbonObjectID *TbTop;
    CarbonNetID *SystemClock;
    CarbonWaveID *vcd;

    CarbonXAxiConfig *config = carbonXAxiConfigCreate();
    carbonXAxiConfigSetDataBusWidth(config,
            (CarbonXAxiDataBusWidth) DATA_BUS_WIDTH);

    /* create AXI Transactor MasterXtor */
    MasterXtor = carbonXAxiCreate("MASTER", CarbonXAxi_Master, config, 5,
            &reportTransCb, &setDefaultResponseCb, &setResponseCb, NULL, NULL,
            NULL, &MasterXtor);
    if (MasterXtor == NULL)
    {
        printf("ERROR: Unable to create AXI Transactor ...\n");
        exit(EXIT_FAILURE);
    }

    /* create AXI Transactor SlaveXtor */
    SlaveXtor = carbonXAxiCreate("SLAVE", CarbonXAxi_Slave, config, 5,
            &reportTransCb, &setDefaultResponseCb, &setResponseCb, NULL, NULL,
            NULL, &SlaveXtor);
    if (SlaveXtor == NULL)
    {
        printf("ERROR: Unable to create AXI Transactor ...\n");
        exit(EXIT_FAILURE);
    }
    carbonXAxiConfigDestroy(config);

    /* create VHM glue logic for back-to-back transactor connection */
    TbTop = carbon_TbTop_create(eCarbonIODB, eCarbon_NoFlags);
    if (TbTop == NULL)
    {
        printf("ERROR: Unable to create Carbon model ...\n");
        exit(EXIT_FAILURE);
    }
    vcd = carbonWaveInitVCD(TbTop, "TbTop.vcd", e1us);

    /* Get handles to system clock */
    SystemClock = carbonFindNet(TbTop, "TbTop.SystemClock");
    assert(SystemClock);

    /* connect MasterXtor to VHM */
    {
        CarbonXInterconnectNameNamePair MasterXtorConnection[] = {
            /* Write Address Channel */
            { "AWID",    "MASTER_AWID"   },
            { "AWADDR",  "MASTER_AWADDR" },
            { "AWLEN",   "MASTER_AWLEN"  },
            { "AWSIZE",  "MASTER_AWSIZE" },
            { "AWBURST", "MASTER_AWBURST"},
            { "AWLOCK",  "MASTER_AWLOCK" },
            { "AWCACHE", "MASTER_AWCACHE"},
            { "AWPROT",  "MASTER_AWPROT" },
            { "AWVALID", "MASTER_AWVALID"},
            { "AWREADY", "MASTER_AWREADY"},
            /* Write Data Channel */
            { "WID",     "MASTER_WID"    },
            { "WDATA",   "MASTER_WDATA"  },
            { "WSTRB",   "MASTER_WSTRB"  },
            { "WLAST",   "MASTER_WLAST"  },
            { "WVALID",  "MASTER_WVALID" },
            { "WREADY",  "MASTER_WREADY" },
            /* Write Response Channel */
            { "BID",     "MASTER_BID"    },
            { "BRESP",   "MASTER_BRESP"  },
            { "BVALID",  "MASTER_BVALID" },
            { "BREADY",  "MASTER_BREADY" },
            /* Read Address Channel */
            { "ARID",    "MASTER_ARID"   },
            { "ARADDR",  "MASTER_ARADDR" },
            { "ARLEN",   "MASTER_ARLEN"  },
            { "ARSIZE",  "MASTER_ARSIZE" },
            { "ARBURST", "MASTER_ARBURST"},
            { "ARLOCK",  "MASTER_ARLOCK" },
            { "ARCACHE", "MASTER_ARCACHE"},
            { "ARPROT",  "MASTER_ARPROT" },
            { "ARVALID", "MASTER_ARVALID"},
            { "ARREADY", "MASTER_ARREADY"},
            /* Read Data Channel */
            { "RID",     "MASTER_RID"    },
            { "RDATA",   "MASTER_RDATA"  },
            { "RRESP",   "MASTER_RRESP"  },
            { "RLAST",   "MASTER_RLAST"  },
            { "RVALID",  "MASTER_RVALID" },
            { "RREADY",  "MASTER_RREADY" },
            { NULL /* NULL here indicates end of list */, NULL } };
        carbonXAxiConnectByModelSignalName(MasterXtor, MasterXtorConnection,
                "TbTop", TbTop);
    }

    /* connect SlaveXtor to VHM */
    {
        CarbonXInterconnectNameNamePair SlaveXtorConnection[] = {
            /* Write Address Channel */
            { "AWID",    "SLAVE_AWID"    },
            { "AWADDR",  "SLAVE_AWADDR"  },
            { "AWLEN",   "SLAVE_AWLEN"   },
            { "AWSIZE",  "SLAVE_AWSIZE"  },
            { "AWBURST", "SLAVE_AWBURST" },
            { "AWLOCK",  "SLAVE_AWLOCK"  },
            { "AWCACHE", "SLAVE_AWCACHE" },
            { "AWPROT",  "SLAVE_AWPROT"  },
            { "AWVALID", "SLAVE_AWVALID" },
            { "AWREADY", "SLAVE_AWREADY" },
            /* Write Data Channel */
            { "WID",     "SLAVE_WID"     },
            { "WDATA",   "SLAVE_WDATA"   },
            { "WSTRB",   "SLAVE_WSTRB"   },
            { "WLAST",   "SLAVE_WLAST"   },
            { "WVALID",  "SLAVE_WVALID"  },
            { "WREADY",  "SLAVE_WREADY"  },
            /* Write Response Channel */
            { "BID",     "SLAVE_BID"     },
            { "BRESP",   "SLAVE_BRESP"   },
            { "BVALID",  "SLAVE_BVALID"  },
            { "BREADY",  "SLAVE_BREADY"  },
            /* Read Address Channel */
            { "ARID",    "SLAVE_ARID"    },
            { "ARADDR",  "SLAVE_ARADDR"  },
            { "ARLEN",   "SLAVE_ARLEN"   },
            { "ARSIZE",  "SLAVE_ARSIZE"  },
            { "ARBURST", "SLAVE_ARBURST" },
            { "ARLOCK",  "SLAVE_ARLOCK"  },
            { "ARCACHE", "SLAVE_ARCACHE" },
            { "ARPROT",  "SLAVE_ARPROT"  },
            { "ARVALID", "SLAVE_ARVALID" },
            { "ARREADY", "SLAVE_ARREADY" },
            /* Read Data Channel */
            { "RID",     "SLAVE_RID"     },
            { "RDATA",   "SLAVE_RDATA"   },
            { "RRESP",   "SLAVE_RRESP"   },
            { "RLAST",   "SLAVE_RLAST"   },
            { "RVALID",  "SLAVE_RVALID"  },
            { "RREADY",  "SLAVE_RREADY"  },
            { NULL /* NULL here indicates end of list */, NULL } };
        carbonXAxiConnectByModelSignalName(SlaveXtor, SlaveXtorConnection,
                "TbTop", TbTop);
    }

    /* Dump all nets */
    carbonDumpVars(vcd, 1, "TbTop");

    /* Starting READ transaction with ID 1 */
    {
        CarbonUInt32 wait[4] = { 3, 1, 2, 0 };
        trans = carbonXAxiTransCreate(1, CarbonXAxiTrans_READ);
        carbonXAxiTransSetStartAddress(trans, 10);
        carbonXAxiTransSetBurstLength(trans, 4);
        carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
        carbonXAxiTransSetDataWaitStates(trans, 4, wait);

        printf("MASTER: Starting read transaction %u ...\n",
            carbonXAxiTransGetId(trans));
        carbonXAxiStartNewTransaction(MasterXtor, trans);
    }
    /* Starting WRITE transaction with ID 1 */
    {
        unsigned int i;
        CarbonUInt32 dataSize = 32;
        CarbonUInt8 data[32];
        CarbonUInt1 strb[32];
        CarbonUInt32 wait[4] = { 5, 1, 2, 0 };
        for (i = 0; i < dataSize; i++)
        {
            data[i] = i + 1;
            strb[i] = 1;
        }
        trans = carbonXAxiTransCreate(1, CarbonXAxiTrans_WRITE);
        carbonXAxiTransSetStartAddress(trans, 10);
        carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_4);
        carbonXAxiTransSetBurstLength(trans, 4);
        carbonXAxiTransSetData(trans, dataSize, data);
        carbonXAxiTransSetWriteStrobe(trans, dataSize, strb);
        carbonXAxiTransSetDataWaitStates(trans, 4, wait);

        printf("MASTER: Starting write transaction %u ...\n",
            carbonXAxiTransGetId(trans));
        carbonXAxiStartNewTransaction(MasterXtor, trans);
    }
    /* Starting READ transaction with ID 2 */
    {
        CarbonUInt32 wait[4] = { 3, 1, 2, 0 };
        trans = carbonXAxiTransCreate(2, CarbonXAxiTrans_READ);
        carbonXAxiTransSetStartAddress(trans, 10);
        carbonXAxiTransSetBurstLength(trans, 4);
        carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
        carbonXAxiTransSetDataWaitStates(trans, 4, wait);

        printf("MASTER: Starting read transaction %u ...\n",
            carbonXAxiTransGetId(trans));
        carbonXAxiStartNewTransaction(MasterXtor, trans);
    }
    /* Starting WRITE transaction with ID 2 */
    {
        unsigned int i;
        CarbonUInt32 dataSize = 12;
        CarbonUInt8 data[12];
        CarbonUInt1 strb[12];
        CarbonUInt32 wait[4] = { 0, 1, 0, 3 };
        for (i = 0; i < dataSize; i++)
        {
            data[i] = i + 1;
            strb[i] = 1;
        }
        trans = carbonXAxiTransCreate(2, CarbonXAxiTrans_WRITE);
        carbonXAxiTransSetStartAddress(trans, 20);
        carbonXAxiTransSetBurstLength(trans, 6);
        carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
        carbonXAxiTransSetData(trans, dataSize, data);
        carbonXAxiTransSetWriteStrobe(trans, dataSize, strb);
        carbonXAxiTransSetDataWaitStates(trans, 4, wait);

        printf("MASTER: Starting write transaction %u ...\n",
            carbonXAxiTransGetId(trans));
        carbonXAxiStartNewTransaction(MasterXtor, trans);
    }
    /* simulation */
    for (t = 0; t < 1000; t += 1)
    {
        if ((t % 5) == 0)
        {
            CarbonUInt32 val;
            ++clocktick;
            val = clocktick & 1;
            carbonDeposit(TbTop, SystemClock, &val, NULL);

            if (val) /* at posedge of clock */
            {
                /* refreshes input VHM connections of
                 * master and slave transactors */
                carbonXAxiRefreshInputs(MasterXtor, t);
                carbonXAxiRefreshInputs(SlaveXtor, t);

                /* evaluate master and slave transactors */
                carbonXAxiEvaluate(MasterXtor, t);
                carbonXAxiEvaluate(SlaveXtor, t);

                carbonSchedule(TbTop, t);

                /* refreshes output VHM connections of
                 * master and slave transactors */
                carbonXAxiRefreshOutputs(MasterXtor, t);
                carbonXAxiRefreshOutputs(SlaveXtor, t);
            }
            else
                carbonSchedule(TbTop, t);
        }
        else
            carbonSchedule(TbTop, t);
    }

    assert (carbonDumpFlush(vcd) == eCarbon_OK);
    carbonXAxiDestroy(MasterXtor);
    carbonXAxiDestroy(SlaveXtor);

    return 0;
}

void setDefaultResponseCb(CarbonXAxiTrans* rxTrans, void* stimulusInstance)
{
    CarbonXAxiID xtor = *((CarbonXAxiID*) stimulusInstance);
    const char *xtorName = carbonXAxiGetName(xtor);
    CarbonXAxiTransType transType = carbonXAxiTransGetType(rxTrans);
    CarbonUInt32 transId = carbonXAxiTransGetId(rxTrans);
    if (transType == CarbonXAxiTrans_WRITE)
    {
        CarbonUInt32 wait[4] = { 2, 0, 1, 2 };
        printf("%s[%d]: Write transaction %u started.\n", xtorName, (int) t,
            transId);
        carbonXAxiTransSetDataWaitStates(rxTrans, 4, wait);
    }
    else if (transType == CarbonXAxiTrans_READ)
    {
        /* Check whether indefinite wait flag is set on rxTrans */
        if (!carbonXAxiTransGetWaitIndefiniteStatus(rxTrans))
        {
            CarbonUInt32 wait[4] = { 2, 0, 1, 1 };
            printf("%s[%d]: Read transaction %u started.\n", xtorName, (int) t,
                transId);
            carbonXAxiTransSetDataWaitStates(rxTrans, 4, wait);
            /* Setting indefinite wait to a particular transaction */
            if (transId == 1)
            {
                printf("%s[%d]: Setting Indefinite Wait in read "
                       "transaction %u.\n", xtorName, (int) t, transId);
                carbonXAxiTransWaitIndefinitely(rxTrans, 1);
            }
        }
        else if (transId == 1)
        {
            if (!readIndefiniteWaitClocks)
            {
                /* Removing indefinite wait from the particular transaction */
                printf("%s[%d]: Removing Indefinite Wait from read "
                       "transaction %u.\n", xtorName, (int) t, transId);
                carbonXAxiTransWaitIndefinitely(rxTrans, 0);
            }
            else
                readIndefiniteWaitClocks--;
        }
    }
    else
    {
        printf("%s[%d]: ERROR: Unknown transaction\n", xtorName, (int) t);
    }
}

void setResponseCb(CarbonXAxiTrans* rxTrans, void* stimulusInstance)
{
    CarbonXAxiID xtor = *((CarbonXAxiID*) stimulusInstance);
    const char *xtorName = carbonXAxiGetName(xtor);
    CarbonXAxiTransType transType = carbonXAxiTransGetType(rxTrans);
    CarbonUInt32 transId = carbonXAxiTransGetId(rxTrans);
    if (transType == CarbonXAxiTrans_WRITE)
    {
        /* Check whether indefinite wait flag is set on rxTrans */
        if (!carbonXAxiTransGetWaitIndefiniteStatus(rxTrans))
        {
            printf("%s[%d]: Write transaction %u received.\n", xtorName, (int)t,
                transId);
            carbonXAxiTransDump(rxTrans);
            carbonXAxiTransSetWriteResponse(rxTrans, CarbonXAxiResponse_OKAY);
            /* Setting indefinite wait to a particular write transaction */
            if (transId == 1)
            {
                printf("%s[%d]: Setting Indefinite Wait in write "
                       "transaction %u.\n", xtorName, (int) t, transId);
                carbonXAxiTransWaitIndefinitely(rxTrans, 1);
            }
        }
        else if (transId == 1)
        {
            if (!writeIndefiniteWaitClocks)
            {
                /* Removing indefinite wait from the particular transaction */
                printf("%s[%d]: Removing Indefinite Wait from write "
                       "transaction %u.\n", xtorName, (int) t, transId);
                carbonXAxiTransWaitIndefinitely(rxTrans, 0);
            }
            else
                writeIndefiniteWaitClocks--;
        }
    }
    else if (transType == CarbonXAxiTrans_READ)
    {
        const CarbonUInt8 data[8]  = { 41, 72, 93, 74, 15, 60, 47, 98 };
        CarbonXAxiResponse resp[8] = {CarbonXAxiResponse_OKAY,
            CarbonXAxiResponse_SLVERR };
        printf("%s[%d]: Read transaction %u received.\n", xtorName, (int) t,
            transId);
        carbonXAxiTransDump(rxTrans);
        carbonXAxiTransSetData(rxTrans, 8, data);
        carbonXAxiTransSetReadResponse(rxTrans, 8, resp);
    }
    else
    {
        printf("%s[%d]: ERROR: Unknown transaction\n", xtorName, (int) t);
    }
}

void reportTransCb(CarbonXAxiTrans *txTrans, void *stimulusInstance)
{
    CarbonXAxiID xtor = *((CarbonXAxiID*) stimulusInstance);
    const char *xtorName = carbonXAxiGetName(xtor);
    CarbonXAxiTransType transType = carbonXAxiTransGetType(txTrans);
    if (transType == CarbonXAxiTrans_WRITE)
    {
        char *responseStr;
        printf("%s[%d]: Write transaction %u complete.\n", xtorName, (int) t,
            carbonXAxiTransGetId(txTrans));
        switch (carbonXAxiTransGetWriteResponse(txTrans))
        {
            case CarbonXAxiResponse_OKAY:   responseStr = "OKAY"; break;
            case CarbonXAxiResponse_EXOKAY: responseStr = "EXOKAY"; break;
            case CarbonXAxiResponse_SLVERR: responseStr = "SLVERR"; break;
            case CarbonXAxiResponse_DECERR: responseStr = "DECERR"; break;

        }
        printf("Response received = %s\n", responseStr);
    }
    else if (transType == CarbonXAxiTrans_READ)
    {
        printf("%s[%d]: Read transaction %u complete.\n", xtorName, (int) t,
            carbonXAxiTransGetId(txTrans));
        carbonXAxiTransDump(txTrans);
    }
    else
    {
        printf("%s[%d]: ERROR: Unknown transaction\n", xtorName, (int) t);
    }
    carbonXAxiTransDestroy(txTrans);
}
