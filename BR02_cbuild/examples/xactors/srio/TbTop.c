// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "xactors/srio/carbonXSrio.h"
#include "libTbTop.h"

/* Forward declarations */
static void reportTransCb(CarbonXSrioTrans*, void*);
static void setResponseCb(CarbonXSrioTrans*, void*);

/* Global variables */
CarbonTime t = 0;

/* This program will create and connect two SRIO transactors (A and B )
 * back-to-back. Then a NREAD transaction will be sent from transactor A.
 * When transactor B will receive this transaction, it will show the details
 * in console and send back a response to A. Upon reception of the response,
 * transactor A will show the response data and other related info. */
int main()
{
    int clocktick = 0;

    CarbonXSrioID xtorA, xtorB;
    CarbonObjectID *TbTop;
    CarbonNetID *SystemClock;
    CarbonWaveID *vcd;

    /* Create transactor configurations */
    CarbonXSrioConfig *config = carbonXSrioConfigCreate();
    /* carbonXSrioConfigSet1xMode(config, 1); */

    /* create SRIO Transactor xtorA */
    xtorA = carbonXSrioCreate("XtorA", config, 5,
            reportTransCb, NULL, setResponseCb, NULL, NULL, NULL, &xtorA);
    if (xtorA == NULL)
    {
        printf("ERROR: Unable to create SRIO Transactor ...\n");
        exit(EXIT_FAILURE);
    }

    /* create SRIO Transactor xtorB */
    xtorB = carbonXSrioCreate("XtorB", config, 5,
            reportTransCb, NULL, setResponseCb, NULL, NULL, NULL, &xtorB);
    if (xtorB == NULL)
    {
        printf("ERROR: Unable to create SRIO Transactor ...\n");
        exit(EXIT_FAILURE);
    }
    carbonXSrioConfigDestroy(config);

    /* create glue logic Carbon Model for back-to-back transactor connection */
    TbTop = carbon_TbTop_create(eCarbonIODB, eCarbon_NoFlags);
    if (TbTop == NULL)
    {
        printf("ERROR: Unable to create Carbon model ...\n");
        exit(EXIT_FAILURE);
    }
    vcd = carbonWaveInitVCD(TbTop, "TbTop.vcd", e1us);

    /* Get handles to system clock */
    SystemClock = carbonFindNet(TbTop, "TbTop.SystemClock");
    assert(SystemClock);

    /* connect xtorA to Carbon Model */
    {
        CarbonXInterconnectNameNamePair xtorAConnection[] = {
            { "TLane0", "XtorA_TLane0" },
            { "TLane1", "XtorA_TLane1" },
            { "TLane2", "XtorA_TLane2" },
            { "TLane3", "XtorA_TLane3" },
            { "RLane0", "XtorA_RLane0" },
            { "RLane1", "XtorA_RLane1" },
            { "RLane2", "XtorA_RLane2" },
            { "RLane3", "XtorA_RLane3" },
            { NULL /* NULL here indicates end of list */, NULL } };
        carbonXSrioConnectByModelSignalName(xtorA, xtorAConnection, "TbTop",
                                            TbTop);
    }

    /* connect xtorB to Carbon Model */
    {
        CarbonXInterconnectNameNamePair xtorBConnection[] = {
            { "TLane0", "XtorB_TLane0" },
            { "TLane1", "XtorB_TLane1" },
            { "TLane2", "XtorB_TLane2" },
            { "TLane3", "XtorB_TLane3" },
            { "RLane0", "XtorB_RLane0" },
            { "RLane1", "XtorB_RLane1" },
            { "RLane2", "XtorB_RLane2" },
            { "RLane3", "XtorB_RLane3" },
            { NULL /* NULL here indicates end of list */, NULL } };
        carbonXSrioConnectByModelSignalName(xtorB, xtorBConnection, "TbTop",
                                            TbTop);
    }

    /* Dump all nets */
    carbonDumpVars(vcd, 1, "TbTop");

    /* starting few transactions */
    {
        CarbonXSrioIoRequest ioReq;
        CarbonXSrioTrans *trans;
        ioReq.reqType = CarbonXSrioIoRequest_NREAD;
        ioReq.address = 100 << 3;
        ioReq.extAddress = 0;
        ioReq.xamsbs = 0;
        ioReq.wdptr = 1;
        ioReq.rdSize = 12; // 8 DWORDs
        ioReq.wrSize = 0;
        ioReq.dataSize = 0;
        ioReq.data = NULL;
        ioReq.transId = 20;
        ioReq.srcId = 1;
        ioReq.destId = 3;
        ioReq.prio = 0;

        trans = carbonXSrioCreateIoRequestTrans(&ioReq);
        carbonXSrioStartNewTransaction(xtorA, trans);
    }

    /* simulation */
    srand(1); /* to equalize the randomization between Carbon Model and callback modes */
    for (t = 0; t < 50000; t += 1)
    {
        if ((t % 5) == 0)
        {
            CarbonUInt32 val;
            ++clocktick;
            val = clocktick & 1;
            carbonDeposit(TbTop, SystemClock, &val, NULL);

            if (val) /* at posedge of clock */
            {
                /* refreshes input Carbon Model connections of
                 * host and device transactors */
                carbonXSrioRefreshInputs(xtorA, t);
                carbonXSrioRefreshInputs(xtorB, t);

                /* evaluate host and device transactors */
                carbonXSrioEvaluate(xtorA, t);
                carbonXSrioEvaluate(xtorB, t);

                carbonSchedule(TbTop, t);

                /* refreshes output Carbon Model connections of
                 * host and device transactors */
                carbonXSrioRefreshOutputs(xtorA, t);
                carbonXSrioRefreshOutputs(xtorB, t);
            }
            else
                carbonSchedule(TbTop, t);
        }
        else
            carbonSchedule(TbTop, t);
    }

    assert (carbonDumpFlush(vcd) == eCarbon_OK);
    carbonXSrioDestroy(xtorA);
    carbonXSrioDestroy(xtorB);

    return 0;
}

/* This callback will be called after transaction has been pushed to transactor
 * for processing. In this callback we shall monitor the transaction that
 * has been accepted by transactor and can be deleted */
void reportTransCb(CarbonXSrioTrans *txTrans, void *stimulusInstance)
{
    CarbonXSrioID xtor = *((CarbonXSrioID*) stimulusInstance);
    const char *xtorName = carbonXSrioGetName(xtor);
    switch(carbonXSrioGetTransType(txTrans))
    {
        case CarbonXSrioTrans_IO_REQUEST :
        {
            CarbonXSrioIoRequest *ioReq =
                carbonXSrioGetIoRequest(txTrans);
            CarbonUInt8 transId = ioReq->transId;
            switch(ioReq->reqType)
            {
                case CarbonXSrioIoRequest_NREAD :
                {
                    printf("%s[%d]: Transmitted NREAD IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break; 
                }
                case CarbonXSrioIoRequest_NWRITE :
                {    
                    printf("%s[%d]: Transmitted NWRITE IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_SWRITE :
                {    
                    printf("%s[%d]: Transmitted SWRITE IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_NWRITE_R :
                {    
                    printf("%s[%d]: Transmitted NWRITE_R IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_INC :
                {    
                    printf("%s[%d]: Transmitted ATOMIC_INC IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_DEC :
                {    
                    printf("%s[%d]: Transmitted ATOMIC_DEC IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_TSWP :
                {    
                    printf("%s[%d]: Transmitted ATOMIC_TSWP IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_SET :
                {    
                    printf("%s[%d]: Transmitted ATOMIC_SET IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_CLR :
                {    
                    printf("%s[%d]: Transmitted ATOMIC_CLR IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_SWAP :
                {    
                    printf("%s[%d]: Transmitted ATOMIC_SWAP IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                default :
                {
                    assert(0);
                }
            }
            carbonXSrioDestroyIoRequest(ioReq);
            break;
        }
        case CarbonXSrioTrans_MESSAGE :
        {
            CarbonXSrioMessage *msgReq = carbonXSrioGetMessage(txTrans);
            printf("%s[%d]: Transmitted DATA Message Request.\n",
                        xtorName, (int) t);
            carbonXSrioDestroyMessage(msgReq);
            break;
        }
        case CarbonXSrioTrans_DOORBELL :
        {
            CarbonXSrioDoorbell *dbReq = carbonXSrioGetDoorbell(txTrans);
            printf("%s[%d]: Transmitted DOORBELL Request.\n",
                        xtorName, (int) t);
            carbonXSrioDestroyDoorbell(dbReq);
            break;
        }
        case CarbonXSrioTrans_RESPONSE :
        {
            CarbonXSrioResponse *resp = carbonXSrioGetResponse(txTrans);
            printf("%s[%d]: Transmitted RESPONSE.\n",
                        xtorName, (int) t);
            carbonXSrioDestroyResponse(resp);
            break;
        }
        default :
        {
            assert(0);
        }
    }
    /* delete the transmitted transaction */
    carbonXSrioTransDestroy(txTrans);
}

/* Function to view IO request structure */
void printIoRequest(CarbonXSrioIoRequest *ioReq)
{
    printf("IO REQUEST:\n");
    printf("    address = %d.%d.%d\n",
            ioReq->xamsbs, ioReq->extAddress, ioReq->address);
    printf("    wdptr = %d, rdSize / wrSize = %d\n",
            ioReq->wdptr,
            (ioReq->rdSize > 0) ? ioReq->rdSize : ioReq->wrSize);
    printf("    Trans ID = %d, Src ID = %d, Dest ID = %d\n",
            ioReq->transId, ioReq->srcId, ioReq->destId);
    printf("    Data size = %d DWORDs.\n", ioReq->dataSize);
    if (ioReq->data != NULL)
    {
        unsigned int i;
        for(i = 0; i < ioReq->dataSize; i++)
        printf("    DWORD[%d] = %d.%d\n", i,
                ioReq->data[i].msWord, ioReq->data[i].lsWord);
    }
    printf("\n");
}

/* This callback will be called when an incomming transaction will be received
 * by the transactor. In this callback the incomming transaction will be 
 * decompiled in console and a response will be sent back */
void setResponseCb(CarbonXSrioTrans* rxTrans, void* stimulusInstance)
{
    CarbonXSrioID xtor = *((CarbonXSrioID*) stimulusInstance);
    const char *xtorName = carbonXSrioGetName(xtor);
    switch(carbonXSrioGetTransType(rxTrans))
    {
        case CarbonXSrioTrans_IO_REQUEST :
        {
            CarbonXSrioIoRequest *ioReq = carbonXSrioGetIoRequest(rxTrans);
            CarbonUInt8 transId = ioReq->transId;
            switch(ioReq->reqType)
            {
                case CarbonXSrioIoRequest_NREAD :
                {
                    printf("%s[%d]: Received NREAD IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    /* decompile incomming NREAD request */
                    printIoRequest(ioReq);
                    /* send a response transaction back */
                    {
                        int i; CarbonXSrioDword data[32];
                        CarbonUInt6 dataSize;
                        CarbonXSrioResponse resp;
                        CarbonXSrioTrans *trans;
                        dataSize = carbonXSrioGetIoRequestDataSize(ioReq);
                        for (i = 0; i < dataSize; i++)
                        {
                            data[i].msWord = data[i].lsWord = i;
                        }
                        resp.transaction = 0x8;
                        resp.status = CarbonXResponseStatus_DONE;
                        resp.dataSize = dataSize;
                        resp.data = data;
                        resp.transId = transId;
                        resp.srcId = ioReq->destId;
                        resp.destId = ioReq->srcId;
                        resp.prio = ioReq->prio;
                        assert(trans = carbonXSrioCreateResponseTrans(&resp));
                        carbonXSrioStartNewTransaction(xtor, trans);
                    }
                    break; 
                }
                case CarbonXSrioIoRequest_NWRITE :
                {    
                    printf("%s[%d]: Received NWRITE IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_SWRITE :
                {    
                    printf("%s[%d]: Received SWRITE IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_NWRITE_R :
                {    
                    printf("%s[%d]: Received NWRITE_R IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_INC :
                {    
                    printf("%s[%d]: Received ATOMIC_INC IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_DEC :
                {    
                    printf("%s[%d]: Received ATOMIC_DEC IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_TSWP :
                {    
                    printf("%s[%d]: Received ATOMIC_TSWP IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_SET :
                {    
                    printf("%s[%d]: Received ATOMIC_SET IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_CLR :
                {    
                    printf("%s[%d]: Received ATOMIC_CLR IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                case CarbonXSrioIoRequest_ATOMIC_SWAP :
                {    
                    printf("%s[%d]: Received ATOMIC_SWAP IO Request no. %d.\n",
                           xtorName, (int) t, transId);
                    break;
                }
                default :
                {
                    printf("%s[%d]: Received UNKNOWN type of IO Request.\n",
                        xtorName, (int) t);
                    break;
                }
            }
            carbonXSrioDestroyIoRequest(ioReq);
            break;
        }
        case CarbonXSrioTrans_MESSAGE :
        {
            CarbonXSrioMessage *msgReq = carbonXSrioGetMessage(rxTrans);
            printf("%s[%d]: Received DATA Message Request.\n",
                        xtorName, (int) t);
            carbonXSrioDestroyMessage(msgReq);
            break;
        }
        case CarbonXSrioTrans_DOORBELL :
        {
            CarbonXSrioDoorbell *dbReq = carbonXSrioGetDoorbell(rxTrans);
            printf("%s[%d]: Received DOORBELL Request.\n",
                        xtorName, (int) t);
            carbonXSrioDestroyDoorbell(dbReq);
            break;
        }
        case CarbonXSrioTrans_RESPONSE :
        {
            CarbonXSrioResponse *resp = carbonXSrioGetResponse(rxTrans);
            printf("%s[%d]: Received RESPONSE.\n",
                        xtorName, (int) t);
            if(resp->status == CarbonXResponseStatus_DONE)
            {
                int i;
                printf("RESPONSE:\n");
                printf("    Trans ID = %d, Src ID = %d, Dest ID = %d\n",
                        resp->transId, resp->srcId, resp->destId);
                printf("    Data size = %d DWORDs.\n", resp->dataSize);
                for(i = 0; i < resp->dataSize; i++) 
                {
                    printf("    DWORD[%d] = %d.%d\n", i,
                            resp->data[i].msWord, resp->data[i].lsWord);
                }
                printf("\n");
            }
            carbonXSrioDestroyResponse(resp);
            break;
        }
        default :
        {
            printf("%s[%d]: Received UNKNOWN Type of Request.\n",
                        xtorName, (int) t);
        }
    }
}
