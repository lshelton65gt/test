/**** Glue Logic connecting XtorA and XtorB Transactors ****/
module TbTop(SystemClock,
        /* XtorA Connections */
        XtorA_TLane0,
        XtorA_TLane1,
        XtorA_TLane2,
        XtorA_TLane3,
        XtorA_RLane0,
        XtorA_RLane1,
        XtorA_RLane2,
        XtorA_RLane3,
        /* XtorB Connections */
        XtorB_TLane0,
        XtorB_TLane1,
        XtorB_TLane2,
        XtorB_TLane3,
        XtorB_RLane0,
        XtorB_RLane1,
        XtorB_RLane2,
        XtorB_RLane3);

input SystemClock;

input [9 : 0] XtorA_TLane0;
input [9 : 0] XtorA_TLane1;
input [9 : 0] XtorA_TLane2;
input [9 : 0] XtorA_TLane3;
input [9 : 0] XtorB_TLane0;
input [9 : 0] XtorB_TLane1;
input [9 : 0] XtorB_TLane2;
input [9 : 0] XtorB_TLane3;

output [9 : 0] XtorA_RLane0;
output [9 : 0] XtorA_RLane1;
output [9 : 0] XtorA_RLane2;
output [9 : 0] XtorA_RLane3;
output [9 : 0] XtorB_RLane0;
output [9 : 0] XtorB_RLane1;
output [9 : 0] XtorB_RLane2;
output [9 : 0] XtorB_RLane3;

/* Xtor connectivities */
assign XtorA_RLane0 = XtorB_TLane0;
assign XtorA_RLane1 = XtorB_TLane1;
assign XtorA_RLane2 = XtorB_TLane2;
assign XtorA_RLane3 = XtorB_TLane3;
assign XtorB_RLane0 = XtorA_TLane0;
assign XtorB_RLane1 = XtorA_TLane1;
assign XtorB_RLane2 = XtorA_TLane2;
assign XtorB_RLane3 = XtorA_TLane3;

endmodule

