//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
`timescale 1ns/100ps

`define BFM0  spi3_tx0

module spi3Tb;
   
wire    clk;	  // carbon depositSignal
wire    reset_l;  // carbon depositSignal


wire 	[3:0] tb_dtpa;
wire		tb_stpa;
wire	  tb_ptpa;
wire 	[7:0]	tb_tadr;
wire 	[31:0]	tb_tdat;
wire		tb_tsop;
wire		tb_tenbn;
wire		tb_tprty;
wire	[1:0]	tb_tmod;
wire 		tb_teop;
wire		tb_terr;
wire  	tb_tsx;

wire 	[31:0] tb_rdat;
wire		tb_rsop;
wire		tb_renbn;
wire		tb_rprty;
wire	[1:0]	tb_rmod;
wire 		tb_reop;
wire		tb_rerr;
wire		tb_rsx;
wire    tb_rval;

`ifdef NOT_CARBON
  reg clk1;
  initial clk1 = 0;
  always #10 clk1 = ~clk1;
   
  assign clk = clk1;
`endif

carbonx_spi3_lnktx spi3_tx0 (
	.reset_l(reset_l),
        .tadr(tb_tadr),
        .tdat(tb_tdat),
        .tsop(tb_tsop),
        .tenbn(tb_tenbn),
        .tfclk(clk),
        .dtpa(tb_dtpa),
      	.stpa(tb_stpa),
        .ptpa(tb_ptpa),
        .tprty(tb_tprty),
        .tmod(tb_tmod),
        .teop(tb_teop),
        .terr(tb_terr),
        .tsx(tb_tsx)
);        

carbonx_spi3_lnkrx spi3_rx0 (
        .reset_l(reset_l),
        .rdat(tb_rdat),
        .rprty(tb_rprty),
        .rsop(tb_rsop),
        .renbn(tb_renbn),
        .rval(tb_rval),  
        .rfclk(clk),
        .rmod(tb_rmod[1:0]),
	      .reop(tb_reop),
        .rerr(tb_rerr),
	      .rsx(tb_rsx)
);


spi3_phy dut0 (
        .reset_l(reset_l),

        .tfclk(clk),
        .tenb(tb_tenbn),
        .tdat(tb_tdat),
        .tmod(tb_tmod),
        .tprty(tb_tprty),
        .tsop(tb_tsop),
        .teop(tb_teop),
        .terr(tb_terr),
        .tsx(tb_tsx),
        .dtpa(tb_dtpa),
        .stpa(tb_stpa),
        .ptpa(tb_ptpa),
        .tadr(tb_tadr),

        .rfclk(clk),
        .renb(tb_renbn),               
        .rdat(tb_rdat),
        .rmod(tb_rmod),
        .rprty(tb_rprty),
        .rval(tb_rval),
        .rsop(tb_rsop),
        .reop(tb_reop),
        .rerr(tb_rerr),
        .rsx(tb_rsx)         
);

endmodule
