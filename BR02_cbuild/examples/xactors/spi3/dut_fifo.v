//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
module dut_fifo(  
                  reset_l,
                  wclk,
                  rclk,
                  we,
                  re,
                  clear_l,
                  wr_data,
                  rd_data,
                  full_l,
                  af_l,
                  empty_l
                            );
input reset_l;
input wclk;
input rclk;
input we;
input re;
input clear_l;
input [37:0] wr_data;
output [37:0] rd_data;
output full_l;
output af_l;
output empty_l;


reg full_l;
reg af_l;
reg empty_l;
reg last_l;
reg first_l;

reg  [4:0] cnt;
reg  [3:0] rd_ptr;
reg  [3:0] wr_ptr;

wire   re_l  = !(re && empty_l);
wire   we_l = !we;

memory mem(.clk(wclk),
           .we_l(we_l),
           .rd_addr(rd_ptr),
           .wr_addr(wr_ptr),
           .data_in(wr_data),
           .data_out(rd_data)
           ); 

always @(posedge wclk or negedge reset_l)
  begin
    if(!reset_l) begin
      cnt    <= 0;
      rd_ptr <= 0;
      wr_ptr <= 0;
    end
    else begin
      if(~clear_l) begin
        cnt    <= 0;
        rd_ptr <= 0;
        wr_ptr <= 0;
      end
      else begin
        if(~we_l) 
          wr_ptr <= wr_ptr + 1;
        if(~re_l)
          rd_ptr <= rd_ptr + 1;
        if(~we_l && re_l && full_l) 
          cnt <= cnt + 1;
        else if(we_l && ~re_l && empty_l)
          cnt <= cnt - 1;
      end
    end
  end

always @(posedge rclk or negedge reset_l)
  begin
    if(!reset_l)
      empty_l <= 1'b0;
    else begin
      if(clear_l==1'b1) begin
        if(empty_l==1'b0 && we_l==1'b0)
           empty_l <= 1'b1;
         else if(first_l==1'b0 && re_l==1'b0 && we_l==1'b1)
           empty_l <= 1'b0;
      end
      else
        empty_l <= 1'b0;
    end
  end
 
always @(posedge rclk or negedge reset_l)
  begin
    if(!reset_l)
      first_l <= 1'b1;
    else begin
      if(clear_l==1'b1) begin
        if((empty_l==1'b0 && we_l==1'b0) ||
           (cnt==2 && re_l==1'b0 && we_l==1'b1))
          first_l <= 1'b0;
        else if (first_l==1'b0 && (we_l ^ re_l))
          first_l <= 1'b1;
      end
      else begin 
        first_l <= 1'b1;
      end
    end   
  end   

always @(posedge wclk or negedge reset_l)
  begin
    if(!reset_l)
      last_l <= 1'b1;
    else begin
      if(clear_l==1'b1) begin
        if ((full_l==1'b0 && re_l==1'b0)  ||
            (cnt == 14 && we_l==1'b0 && re_l==1'b1))  
          last_l <= 1'b0;
        else if(last_l==1'b0 && (re_l ^ we_l) )
          last_l <= 1'b1;
      end
      else
        last_l <= 1'b1;
    end
  end       

always @(posedge wclk or negedge reset_l)
  begin
    if(!reset_l)
      af_l <= 1'b1;
    else begin
      if(clear_l==1'b1) begin
        if ((full_l==1'b0 && re_l==1'b0)  ||
            (cnt >= 8) )
          af_l <= 1'b0;
        else
          af_l <= 1'b1;
      end
      else
        af_l <= 1'b1;
    end
  end       

always @(posedge wclk or negedge reset_l)
  begin
    if(!reset_l)
      full_l <= 1'b1;
    else begin
      if(clear_l==1'b1)  begin
        if (last_l==1'b0 && we_l==1'b0 && re_l==1'b1) begin
          full_l <= 1'b0;
          $display("ERROR -- SPI-3 DUT Fifo Overflow");
        end
        else if(full_l==1'b0 && re_l==1'b0)
          full_l <= 1'b1;
      end
      else
        full_l <= 1'b1;
    end
  end
       
endmodule

module memory( clk,
               we_l,
               wr_addr,
               rd_addr,
               data_in,
               data_out
               );
  input clk;
  input we_l;
  input  [3:0]  wr_addr;
  input  [3:0]  rd_addr;
  input  [37:0] data_in;
  output [37:0] data_out;
  wire   [37:0] data_out;  
  reg    [37:0] storage [0:15];
  assign data_out  = storage[rd_addr];

  always @(posedge clk)
  begin
    if(we_l==1'b0)
      storage[wr_addr] <= data_in;
  end

endmodule


