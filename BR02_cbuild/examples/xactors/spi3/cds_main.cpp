
// SPI-3 transactor test

 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 //  Copyright 2005-2007 Carbon Design Systems, Inc.  All Rights Reserved. 
 //  Portions of this software code are licensed to Carbon Design Systems 
 //  and are protected by copyrights of its licensors."
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


//
//   include files
//
#include "libdesign.h"
#include "carbon/c_memmanager.h"
// #include "xactors/xactors.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <time.h>   // Only used for debug -- can be removed


#include <stdlib.h>
#include <string.h>
#include "xactors/carbonX.h"

extern "C" {
#include "xactors/spi3/carbonXSpi3LnkTx.h"
#include "xactors/spi3/carbonXSpi3LnkRx.h"
}

#define NUM_PORTS       4
#define PORT_ID         0
#define NUM_PKTS        100

#define TRANSMITTER   "spi3_tx0"
#define RECEIVER      "spi3_rx0"

#define TEST_COMPLETE "test_complete"

#define Dbug printf


int    myERROR = 0;

CarbonUInt32 ONE  = 1;  // need pointers to values
CarbonUInt32 ZERO = 0;  // for deposit function
const int CLOCK_CYCLE = 20; 

#define SIM_MAX_TICKS 25000L

/*
 * structures to help with testing 
 *
 */

typedef struct {
  
  u_int8  port_addr;
  u_int32 size_in_bytes;
  u_int8  *pkt_ptr;
  
  u_int32 bytes_sent;
  u_int32 segments_sent;
  
  u_int32 bytes_rcvd;
  u_int32 segments_rcvd;

}  SPI3_TEST_PKT ;

typedef struct {
  u_int32   num_ports;
  u_int32   port_pkt_cnt[256];
  u_int32   port_addrs[256];
  u_int32   data_max_t;

  u_int32   word_err;
  u_int32   bit_err;
} SPI3_TEST_PARAMS;



//
// Transactor library routine
//
extern "C" int  tbp_get_error_count(void);
extern "C" void tbp_print_error_status(void);


//
// local functions - in this file
//

static void transmitter_func(SPI3_TEST_PARAMS *test_parms, SPI3_TEST_PKT **pkt_queues);
static void receiver_func(SPI3_TEST_PARAMS *test_parms, SPI3_TEST_PKT **pkt_queues);
static SPI3_TEST_PKT ** BuildPortDesc(SPI3_TEST_PARAMS * test_parms);

SPI3_TEST_PKT spi3_test_create_packet(u_int8 port, u_int32 size, u_int8 payload_start_char);
                                            
s_int32 spi3_test_get_port_idx(u_int32 port_addr, u_int32 num_pkts, SPI3_TEST_PKT **pkt_queues);
s_int32 spi3_test_check_all_rcvd(SPI3_TEST_PARAMS *tp, SPI3_TEST_PKT **pkt_queues);


/*
 *  These are some utility functions
 */

void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time);
void printHeader(char*);
int  passfail(void);


// *************
//     Main   
// *************

int main()
{
    u_int32 i;
    SPI3_TEST_PARAMS * test_parms = 
                 (SPI3_TEST_PARAMS*)carbonmem_malloc(sizeof(SPI3_TEST_PARAMS));
    SPI3_TEST_PKT ** pkt_queues;
  
    printHeader("SPI-3 (dtpa port0) testbench starting");

    // Create Sim Infrastructure.  testBench, Clocks, and Reset

    // Handle to Carbon Model
    Dbug("\n============== create carbon testBench  ==============\n");	
    CarbonObjectID* model = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);

    Dbug("=============== Start writing VCD file ===================\n");
    CarbonWaveID* wave = carbonWaveInitVCD(model, "spi3Tb.vcd", e1ns);
    int wavStat = carbonDumpVars(wave, 6, "spi3Tb");
    assert(wavStat == eCarbon_OK);

    // Clocks & Reset
    CarbonNetID* clock  = carbonFindNet(model, "spi3Tb.clk");
    CarbonNetID* resetn = carbonFindNet(model, "spi3Tb.reset_l");

    // Assert Reset for a few clocks
    //
    assert(resetn != NULL);
    Dbug("============== Asserting Reset ==============\n");
    carbonDeposit(model, resetn, &ZERO, NULL);

    CarbonTime time = 0;
    Dbug("============== Run 100 ticks  ==============\n");
    for (int i=0; i<100; i++) run(model, clock, time);
	   
    Dbug("============== Deasserting Reset ==============\n");
    carbonDeposit(model, resetn, &ONE, NULL);

    // Set number of ports in the test
    test_parms->num_ports   = NUM_PORTS;
    test_parms->data_max_t  = 50000;

    // Set number of Packets per port
    for(i = 0; i < test_parms->num_ports; i++)
      test_parms->port_pkt_cnt[i] = NUM_PKTS;

    // Set Port Addresses
    for(i = 0; i < test_parms->num_ports; i++)
      test_parms->port_addrs[i] = i;
    
    // Build Packet Queues
    pkt_queues = BuildPortDesc(test_parms);
  
    // Start Attached Function Contexts
    MSG_Milestone("carbonXAttach transmitter function\n");
    carbonXAttach(TRANSMITTER,  (CarbonXAttachFuncT)transmitter_func, 2, test_parms, pkt_queues);

    MSG_Milestone("carbonXAttach receiver function\n");  
    carbonXAttach(RECEIVER,  (CarbonXAttachFuncT)receiver_func,    2, test_parms, pkt_queues);
  
    Dbug("=============   run clock until TEST_COMPLETE  ========  \n");
  
    long numTicks = 0;

    while (    (numTicks < SIM_MAX_TICKS) 
            && (!carbonXCheckSignalState(TEST_COMPLETE))  )
      {
	run(model, clock, time);
	numTicks = time / CLOCK_CYCLE;
      
 	if ((numTicks % 100) == 0)  printf("Main:  cycle %4d\n", numTicks);    
      }

    if (numTicks < SIM_MAX_TICKS)
      Dbug("============= main:  TEST_COMPLETE detected ===============\n");
    else
      MSG_Error(" === Simulation exceeded time limit of %-6d ticks (SIM_MAX_TICKS)\n\n",
                SIM_MAX_TICKS);  

    if(carbonDumpFlush(wave) != eCarbon_OK)    assert(0);
    carbonXIdle(20);
    printf("\n\nmain: Simulation complete at cycle: %d\n", time / CLOCK_CYCLE);
    carbonmem_dealloc(test_parms, sizeof(SPI3_TEST_PARAMS));
    carbonDestroy(&model);

    exit (passfail());

}    // end of main()



/*
 *  print a header line that includes the time of day
 *
 */
void printHeader(char *strInfo)
{
  time_t      curtime          = time (NULL);          // Get the current time.
  struct tm  *loctime          = localtime (&curtime); // Convert it to local time representation.  
  char       *dtString         = asctime(loctime);     // convert to ascii string
  dtString[strlen(dtString)-1] = '\0';                 // remove the \n at the end of dtString
  printf("======= %s  at %s =========\n\n", strInfo, dtString);
}


//  ******
//    Run       run clock one clock period.
//  ******
void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time)
{
  carbonDeposit(model, clock, &ZERO, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
  carbonDeposit(model, clock, &ONE, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
}

// -------------------

int passfail(void)
{
  myERROR += tbp_get_error_count();
                    printf ("\n\n**********\n");
  if (myERROR == 0) printf     ("** PASS **\n");
  else              printf     ("** FAIL **   error count: %d\n", myERROR);
                    printf     ("**********\n");

  return myERROR;
}                        

//
// Alternate TBP style 
//
// int passfail(void)
// {
//   int errors;
//   tbp_print_error_status();
//   return tbp_get_error_count();
// }

// -----------------------------------------------------------


static void transmitter_func(SPI3_TEST_PARAMS *test_parms, SPI3_TEST_PKT **pkt_queues){
    
  u_int8 *        pktPtr;
  s_int32         pktSize;
  u_int32         pkt_cnt[256];
  SPI3_TEST_PKT * curr_pkt;
  u_int32         sop;
  u_int32         port_idx;
  u_int32         bytes_sent;
  u_int32         all_pkts_sent;
  s_int32         cycles;


  // Set One Port at a time operation
  //carbonXSpi3LnkTxWrConfig(CARBONXSPI3LNKTX_ONEPORT);
     
  // Clean All Packet Counts
  memset(pkt_cnt, 0, 256*sizeof(u_int32));
   
  // Until all packets are sent
  port_idx = PORT_ID;
  all_pkts_sent = 0;
  
  while(!all_pkts_sent){
    
    curr_pkt = &pkt_queues[port_idx][pkt_cnt[port_idx]];

    // Check if it's the first segment of the Packet
    if(curr_pkt->bytes_sent == 0) {
      bytes_sent = 0;  
      sop = 1;
    }
    else
      sop = 0;

    // Calculate Packet Size, and Pointer
    pktSize = curr_pkt->size_in_bytes - curr_pkt->bytes_sent;
    pktPtr  = curr_pkt->pkt_ptr + curr_pkt->bytes_sent;
    // Send Packet Segment
    MSG_Milestone("Sending Segment for Port %d, Packet %d, Packet_size = %d\n",
                  curr_pkt->port_addr, pkt_cnt[port_idx], pktSize);

    bytes_sent = carbonXSpi3LnkTxPkt(pktPtr, pktSize, curr_pkt->port_addr, sop);
    curr_pkt->bytes_sent += bytes_sent;

    MSG_Milestone("bytes_sent = %d, curr.packet bytes_sent = %d, packet size = %d\n",
                   bytes_sent, curr_pkt->bytes_sent, curr_pkt->size_in_bytes);
    
    // Check if the whole packet is sent and in that case update the packet count
    if(curr_pkt->bytes_sent == curr_pkt->size_in_bytes) {
      pkt_cnt[port_idx]++;
    }
    else if(curr_pkt->bytes_sent > curr_pkt->size_in_bytes) {
      MSG_Panic("Sent more bytes than packet is big. Either test or Environment is broken.\n");
    }
    
    MSG_Milestone("Check if Port %d has packets available. pkt_cnt = %d, total_cnt = %d\n", 
	  	           port_idx, pkt_cnt[port_idx], test_parms->port_pkt_cnt[port_idx]);
    // Check if there are any packets to send on this port
    if(pkt_cnt[port_idx] == test_parms->port_pkt_cnt[port_idx])
      all_pkts_sent = 1;

    //carbonXIdle(1);
  }
  
  // Now we wait for all packets to be received by the Sink Transactor
  cycles = 0;
  while(!spi3_test_check_all_rcvd(test_parms, pkt_queues) && cycles < 10000) {
      carbonXIdle(10);
      cycles += 10;
  }
  
  if(cycles >= 10000) {
    MSG_Error("Timeout occured when waiting for all packets to be received.\n");
  }

  MSG_Milestone("Ran %d Idle Cycles.\n", cycles);
  
  carbonXSetSignal(TEST_COMPLETE);
  MSG_Milestone("%s: Finish Tx \n", __FUNCTION__);
}


static void receiver_func(SPI3_TEST_PARAMS *test_parms, SPI3_TEST_PKT **pkt_queues){
  
  u_int8          pkt_buf[1000];
  u_int32         i;
  u_int32         size, sop, eop, port;
  s_int32         cycles;
  u_int32         pkt_cnt[256];
  SPI3_TEST_PKT * curr_pkt;
  u_int32         port_idx;

  MSG_Milestone("%s: Start Rx \n", __FUNCTION__);
  
  /* This function sets the TX transactor Configuration Register
     First argument must be equal to the number of following arguments.
     Each following argument corresponds to the bit to be set */
  // Set RENB_DISABLE = 1
  //spi3_set_rx_config_reg(2, RENB_DISABLE, 10);
  
  // Clean All Packet Counts
  memset(pkt_cnt, 0, 256*sizeof(u_int32));

  MSG_Milestone("%s: after mmset cal \n", __FUNCTION__);

  while(1) {

    size = 1000;
    // Get A Packet from the Sink Transactor

    s_int32* p_size = (s_int32*)&size;
    s_int32* p_sop = (s_int32*)&sop;
    s_int32* p_eop = (s_int32*)&eop;
    s_int32* p_port = (s_int32*)&port;
    
    //  cycles = carbonXSpi3LnkRxPkt (pkt_buf, &size, &sop, &eop, &port);
    cycles = carbonXSpi3LnkRxPkt (pkt_buf, p_size, p_sop, p_eop, p_port);

    MSG_Milestone("RxPkt size=%d, sop=%d, eop=%d, port=%d\n", size, sop, eop, port);
    
    // Get the Current packet for this port from the queue
    port_idx = spi3_test_get_port_idx(port, test_parms->num_ports, pkt_queues);
    if(port_idx == -1){
      MSG_Error("Received packet on undefined Port %d\n", port);
    }
    else if (size != 0) {
      curr_pkt = &pkt_queues[port_idx][pkt_cnt[port_idx]];
      
      // Check Port
      if(port != PORT_ID)
        MSG_Error("Received wrong port value: expected port %d, actual port %d\n", PORT_ID, port);

      // Check packet size
      if((curr_pkt->bytes_rcvd + size) > curr_pkt->size_in_bytes) {
	    MSG_Error("Received more bytes than packet size for packet %d on Port %d: Expected size %d, actual size %d\n",
	               pkt_cnt[port_idx], port, curr_pkt->size_in_bytes, curr_pkt->bytes_rcvd + size);
      }
      
      // Check SOP
      if(sop && curr_pkt->segments_rcvd > 0)
	    MSG_Error("Received SOP for Port %d, Packet %d, but partial packet already received.\n",
		           port, pkt_cnt[port_idx]);
      
      // Check EOP
      if(eop && (curr_pkt->bytes_rcvd + size) < curr_pkt->size_in_bytes) {
	    MSG_Error("Received EOP for packet %d on Port %d, but has not yet got all the bytes.\n",
		          pkt_cnt[port_idx], port); 
      }
      
      // Check Payload
      for(i = 0; i < size; i++) {
	    if(pkt_buf[i] != curr_pkt->pkt_ptr[curr_pkt->bytes_rcvd+i]) {
	      MSG_Error("Wrong Data found on Port %d, Packet %d, Byte %d. Expected %x, Actual %x\n",
		             port, pkt_cnt[port_idx], curr_pkt->bytes_rcvd+i, curr_pkt->pkt_ptr[curr_pkt->bytes_rcvd+i], pkt_buf[i]);
	    }
      }
      
      // Update Packet Status
      curr_pkt->bytes_rcvd += size;
      curr_pkt->segments_rcvd++;
      if(eop) pkt_cnt[port_idx]++;

      MSG_Milestone("Received Packet Segment for Port %d, Size = %d, SOP %d, EOP %d, Received %d of %d bytes.\n",
		             port, size, sop, eop, curr_pkt->bytes_rcvd, curr_pkt->size_in_bytes);
    }
  }

  MSG_Milestone("%s: Finish Rx \n", __FUNCTION__);
}



//
//  b u i l d    d e s c r i p t o r
//----------------------------------------------------------------------
/*
 * This function builds a list of descriptors 
 * onto a named queue for use by a transmit function 
 */ 
static SPI3_TEST_PKT * build_descriptor_list(u_int32 pkt_count, u_int32 port) {
  
  u_int32 i;
  SPI3_TEST_PKT *pkt_list;

  // Allocate Space for The Packet list
  pkt_list = (SPI3_TEST_PKT *) calloc (pkt_count, sizeof(SPI3_TEST_PKT));

  for(i = 0;  i < pkt_count; i++) {
    pkt_list[i] = spi3_test_create_packet(port, 100+i, 35+i);
  }
  
  return pkt_list;
}


/* Generating desc list */
static SPI3_TEST_PKT ** BuildPortDesc(SPI3_TEST_PARAMS * test_parms) {

  u_int32 port;
  SPI3_TEST_PKT **port_list;
  
  // Allocate Space for The Packet Queues
  port_list = (SPI3_TEST_PKT **) calloc (test_parms->num_ports, sizeof(SPI3_TEST_PKT *));

  // Call a function to build a list of packets for each port
  for (port=0; port < test_parms->num_ports; port++)
    port_list[port] = build_descriptor_list(test_parms->port_pkt_cnt[port], port);

  return port_list;
}

// -----------------------------------------------------------
//    spi3 test support routines
// -----------------------------------------------------------

SPI3_TEST_PKT spi3_test_create_packet(u_int8 port, u_int32 size, u_int8 payload_start_char) {

  SPI3_TEST_PKT pkts;
  u_int8 *      pkt_ptr;
  u_int32       i;

  // Allocate Memory for the Packet
  if( NULL == (pkt_ptr = (u_int8*)carbonmem_malloc(size *sizeof(u_int8) ))) {
    MSG_Panic("Malloc failed when trying to allocated memory for packet data.\n");
  }
  
  // Fill Packet Data with Incrementing Bytes
  for(i = 0; i < size; i++) {
    pkt_ptr[i] = payload_start_char + i;
  }
  
  // Fill Out Structure
  pkts.pkt_ptr       = pkt_ptr;
  pkts.port_addr     = port;
  pkts.size_in_bytes = size;
  pkts.bytes_sent    = 0;
  pkts.segments_sent = 0;
  pkts.bytes_rcvd    = 0;
  pkts.segments_rcvd = 0;

  return pkts;
}


s_int32 spi3_test_get_port_idx(u_int32 port_addr, u_int32 num_pkts, SPI3_TEST_PKT **pkt_queues) {
  
  s_int32 i;
  
  for(i = 0; i < num_pkts; i++) {
    if(pkt_queues[i]->port_addr == port_addr)
      return i;
  }

  return -1;

}

s_int32 spi3_test_check_all_rcvd(SPI3_TEST_PARAMS *tp, SPI3_TEST_PKT **pkt_queues) {
  
  u_int32        port_idx, pkts;
  SPI3_TEST_PKT *curr_pkt;

  for(port_idx = 0 ; port_idx < tp->num_ports; port_idx++) {
    for(pkts = 0; pkts < tp->port_pkt_cnt[port_idx]; pkts++){
      curr_pkt = &pkt_queues[port_idx][pkts];
      if(curr_pkt->bytes_rcvd < curr_pkt->bytes_sent)
	    return 0;
    }
  }
  
  // If we get here means that all packets were received
  return 1;
}



// -- eof --
