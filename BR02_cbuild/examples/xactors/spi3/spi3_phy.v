//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module spi3_phy (

  reset_l,
                    
  tfclk,
  tenb,
  tdat,
  tmod,
  tprty,
  tsop,
  teop,
  terr,
  tsx,
  dtpa,
  stpa,
  ptpa,
  tadr,        

  rfclk,
  renb,
  rdat,
  rmod,
  rprty,
  rval,
  rsop,
  reop,
  rerr,
  rsx
);

// Ingress (transmit)
input reset_l, tfclk;
input tenb, tprty, tsop, teop, terr, tsx;
input [31:0] tdat;
input  [1:0] tmod;
input  [7:0] tadr;
 
output  [3:0] dtpa;
output  stpa, ptpa;

// Egress (receive)
input rfclk;
input renb;

output [31:0] rdat;
output  [1:0] rmod;
output rprty, rval, rsop, reop, rerr, rsx;



reg stpa, ptpa;
reg [31:0] rdat;
reg [1:0] rmod;
reg rprty, rsop, reop, rerr;
reg rval;

wire        renb0, renb1, renb2, renb3;
wire [31:0] rdat0, rdat1, rdat2, rdat3;
wire  [1:0] rmod0, rmod1, rmod2, rmod3;
wire        rprty0, rprty1, rprty2, rprty3;
wire        rval0, rval1, rval2, rval3;
wire        rsop0, rsop1, rsop2, rsop3;
wire        reop0, reop1, reop2, reop3;
wire        rerr0, rerr1, rerr2, rerr3;

wire       tenb0, tenb1, tenb2, tenb3;
wire [1:0] stpa_Sel;
wire [1:0] ptpa_Sel;
wire [3:0] rec_Sel;
wire       rval_any;

reg        tsx_r, rsx_r;
reg [31:0] tdat_r;
reg  [7:0] tsx_addr, tsx_addr_r;
wire [7:0] rsx_addr;
reg  [7:0] counter;

wire one_port, addr_mode;
assign one_port  = spi3_tx0.one_port;
assign addr_mode = spi3_tx0.addr_mode;

//always
//begin
//  if (spi3_tx0.tx_config[11] == 1) begin
//   `define SMALL_FIFO_SIZE;
//  end
//end
   

// Extract in-band address
always @(negedge rfclk or negedge reset_l)
begin
  if (!reset_l)
    tsx_addr <= 8'h0;
  else if (tsx && addr_mode==0)
    tsx_addr <= tdat[7:0];
  else if (tsx && addr_mode==1)
    tsx_addr <= 8'h0;
  else
    tsx_addr <= tsx_addr;    
end 

// Generate write enables for FIFOs
assign tenb0 = !(!tenb  && (tsx_addr == 8'h0));
assign tenb1 = !(!tenb  && (tsx_addr == 8'h1));
assign tenb2 = !(!tenb  && (tsx_addr == 8'h2));
assign tenb3 = !(!tenb  && (tsx_addr == 8'h3));


// Generate STPA and PTPA signals
assign stpa_Sel = tsx_addr[1:0];

always @(stpa_Sel or dtpa[0] or dtpa[1] or dtpa[2] or dtpa[3])
  case(stpa_Sel)
    2'b00: stpa = dtpa[0];
    2'b01: stpa = dtpa[1];
    2'b10: stpa = dtpa[2];
    2'b11: stpa = dtpa[3];
  endcase

assign ptpa_Sel = tadr[1:0];

always @(ptpa_Sel or dtpa[0] or dtpa[1] or dtpa[2] or dtpa[3])
  case(ptpa_Sel)
    2'b00: ptpa = dtpa[0];
    2'b01: ptpa = dtpa[1];
    2'b10: ptpa = dtpa[2];
    2'b11: ptpa = dtpa[3];
    default: ptpa = dtpa[0];
  endcase
 

/******************************
  Generate Receve Signals
******************************/

always @(posedge rfclk or negedge reset_l)
begin
  if (!reset_l) begin
    tsx_r  <= 1'b0;
    tdat_r <= 32'b0;
    tsx_addr_r <= 1'b0;
  end
  else begin
    tsx_r  <= tsx;
    tdat_r <= tdat;
    tsx_addr_r <= tsx_addr;
  end
end

always @(posedge rfclk or negedge reset_l)
begin
  if (!reset_l)
    counter <= 8'hfd;
  //else if (counter == 8'h10)  // @@@Temporarily halt counter
  //  counter <= counter;
  else
    if (renb == 0)  // only increment when receive is enabled
      counter <= counter + 1;
end


// Generate RSX end internal RENBs
always @(posedge rfclk or negedge reset_l)
begin
    if (!reset_l)
      rsx_r = 1'b0;
    else if ((counter[5:0] == 6'h3f) && (renb == 1'b0))
      rsx_r = 1'b1;
    else
      rsx_r = 1'b0;  
end


//  one_port creates feedback from TSX to RSX
assign rsx      = addr_mode ? 1'b0 : one_port ? tsx_r : rsx_r;
assign rsx_addr = addr_mode ? 8'h0 : one_port ? tsx_addr_r : {6'h0, counter[7:6]};
assign rec_Sel  = addr_mode ? {1'b0, rval_any, 2'b00} :
                  one_port  ? {tsx_r, rval_any, rsx_addr[1:0]} : {rsx, rval_any, counter[7:6]}; 

assign renb0 = !(!renb && (rsx_addr == 8'h0) && !rsx);
assign renb1 = !(!renb && (rsx_addr == 8'h1) && !rsx);
assign renb2 = !(!renb && (rsx_addr == 8'h2) && !rsx);
assign renb3 = !(!renb && (rsx_addr == 8'h3) && !rsx);

assign rval_any = addr_mode ? rval0 :
                              ((rval0 && rsx_addr==0) || (rval1 && rsx_addr==1) ||
                               (rval2 && rsx_addr==2) || (rval3 && rsx_addr==3));
                               
 
// Generate all other outputs
always @(rec_Sel or rdat0  or rdat1  or rdat2  or rdat3  or 
                    rmod0  or rmod1  or rmod2  or rmod3  or
                    rprty0 or rprty1 or rprty2 or rprty3 or
                    rval0  or rval1  or rval2  or rval3  or
                    rsop0  or rsop1  or rsop2  or rsop3  or
                    reop0  or reop1  or reop2  or reop3  or
                    rerr0  or rerr1  or rerr2  or rerr3  or rsx_addr )

  if (rec_Sel[3] == 1) begin  // Assert in-band address
                         rdat = {24'h000, rsx_addr}; rmod = 2'b00;  rprty = ~(^{24'h000, rsx_addr}); rval = 1'b0;
                         rsop = 1'b0;  reop = 1'b0;  rerr = 1'b0;
                       end
  else if (rec_Sel[3:2] == 2'b00) begin   // Supress x-es on the DUT output
                                    rdat = 32'h0000; rmod = 2'b00;  rprty = 1'b1; rval = 1'b0;
                                    rsop = 1'b0;  reop = 1'b0;   rerr = 1'b0;
                                  end
  else case(rec_Sel)
    4'b0100: begin
               rdat = rdat0; rmod = rmod0; rprty = rprty0;   rval = rval0;
               rsop = rsop0; reop = reop0; rerr = rerr0;
             end
    4'b0101: begin
               rdat = rdat1; rmod = rmod1; rprty = rprty1;   rval = rval1;
               rsop = rsop1; reop = reop1; rerr = rerr1;
             end
    4'b0110: begin
               rdat = rdat2; rmod = rmod2; rprty = rprty2;   rval = rval2;
               rsop = rsop2; reop = reop2; rerr = rerr2;
             end
    4'b0111: begin
               rdat = rdat3; rmod = rmod3; rprty = rprty3;   rval = rval3;
               rsop = rsop3; reop = reop3; rerr = rerr3;
             end
  endcase

dut_fifo    fifo0 (
   .wclk(tfclk),
   .rclk(tfclk),
  .reset_l(reset_l),
  .clear_l(reset_l),
  .wr_data({tmod,tprty,tsop,teop,terr,tdat}),
  .we(!tenb0),
  .re(!renb0),

  .rd_data({rmod0,rprty0,rsop0,reop0,rerr0,rdat0}),
  .full_l(F_FullN0),
  .af_l(dtpa[0]),
  .empty_l(rval0)
);

dut_fifo    fifo1 (
   .wclk(tfclk),
   .rclk(tfclk),
  .reset_l(reset_l),
  .clear_l(reset_l),
  .wr_data({tmod,tprty,tsop,teop,terr,tdat}),
  .we(!tenb1),
  .re(!renb1),

  .rd_data({rmod1,rprty1,rsop1,reop1,rerr1,rdat1}),
  .full_l(F_FullN1),
  .af_l(dtpa[1]),
  .empty_l(rval1)
);

dut_fifo    fifo2 (
   .wclk(tfclk),
   .rclk(tfclk),
  .reset_l(reset_l),
  .clear_l(reset_l),
  .wr_data({tmod,tprty,tsop,teop,terr,tdat}),
  .we(!tenb2),
  .re(!renb2),

  .rd_data({rmod2,rprty2,rsop2,reop2,rerr2,rdat2}),
  .full_l(F_FullN2),
  .af_l(dtpa[2]),
  .empty_l(rval2)
);

dut_fifo    fifo3 (
   .wclk(tfclk),
   .rclk(tfclk),
  .reset_l(reset_l),
  .clear_l(reset_l),
  .wr_data({tmod,tprty,tsop,teop,terr,tdat}),
  .we(!tenb3),
  .re(!renb3),

  .rd_data({rmod3,rprty3,rsop3,reop3,rerr3,rdat3}),
  .full_l(F_FullN3),
  .af_l(dtpa[3]),
  .empty_l(rval3)
);

endmodule
