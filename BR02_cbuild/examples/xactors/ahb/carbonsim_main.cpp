/* C testbench for AHB transactor */

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#include "libdesign.h"   /* generated header file */


//   Transactor Header files

extern "C" {
#include "ahb_master_support.h"
#include "ahb_slave_support.h"
#include "ahb_monitor_support.h"
}


// CarbonSim  library

#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimClock.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimMemory.h"
#include "carbonsim/CarbonSimStep.h"
#include "carbonsim/CarbonSimObjectInstance.h"
#include "tbp_user.h"


// Carbon "C" library

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <time.h>   // Only used for debug -- can be removed

// Set to 1 for printing debug info, 0 for no printing
//
#if   1
 #define Dbug(s) printf(s)
#else
 #define Dbug(s) 
#endif

// local synch signal
//
#define MASTER_DONE "masterdone"
#define MASTER2_DONE "master2done"
#define MS2_DONE "ms2done"
#define SLAVE_PROCEED "slaveproceed"
#define SLAVE_DONE "slavedone"
#define MON_DONE  "mondone"


/*
** These are some utility functions
*/
void createClock(CarbonSimMaster*, int, const char*, int initVal = 0);
void bindNet(CarbonSimMaster*, CarbonSimFunctionGen*, const char*);

void master_func(void);
void slave_func(void) ;
void monitor_func(void);

const int CLOCK_CYCLE = 20;

int  myERROR = 0;

// Main function
int main(int argc, char *argv[]) {

    time_t curtime = time (NULL);                // Get the current time.
    struct tm  *loctime = localtime (&curtime);  // Convert it to local time representation.  
     
    Dbug("\n\n===== cds_main starting at  ");
    Dbug( asctime(loctime) );                    // convert time to ascii string 

    CarbonUInt32 ONE  = 1;
    CarbonUInt32 ZERO = 0;
 
    // Create Sim Infrastructure.  Master, Clocks, and Reset

    // Handle to Carbon Model
    CarbonSimMaster* master = CarbonSimMaster::create();
    assert(master);
  
    // CarbonSimObjectType*   objType = master->addObjectType("ahbTb", carbon_ahb_create);
    CarbonSimObjectType*   objType = master->addObjectType("ahbTb", carbon_design_create);
    Dbug("\n============== carbon AHB create ==============\n\n");	
    CarbonSimObjectInstance* tbObj = master->addInstance(objType, "ahbTb");
    Dbug("============== carbon AHB instantiate ==============\n\n");

    printf("Start writing VCD file\n");
    CarbonWaveID* wave = carbonWaveInitVCD(tbObj->getCarbonObject(), "ahbTb.vcd", e1ns);
    if(carbonDumpVars(wave, 6, "ahbTb") != eCarbon_OK) {
      assert(0);
    }
    //carbonDumpVars(wave, 15, "AhbTb.xx");

    // Clocks
    Dbug("============== create AHB HCLK  ==============\n");
    createClock(master, CLOCK_CYCLE, "ahbTb.HCLK", 0);
    Dbug("============== AHB HCLK created ==============\n\n");


    // Assert Reset for a few clocks
    //
    CarbonSimNet* resetn = master->findNet("ahbTb.HRESETn");
    assert(resetn != NULL);
    Dbug("============== Asserting Reset ==============\n\n");
    resetn->deposit(&ZERO);

    Dbug("============== Run 100 ticks  ==============\n\n");
    for (int i=0; i<100; i++)
	    master->run(CLOCK_CYCLE);

    Dbug("============== Deasserting Reset ==============\n\n");
      resetn->deposit(&ONE); 
      
    printf("\n****** Starting Simulation ******\n");

    Dbug("============= spawning MASTER1_XTOR thread ===============\n");
         SIM_Attach("MASTER1_XTOR", master_func, 0);
         tbp_set_max_error_count  (1);
	
    Dbug("============= spawning SLAVE1_XTOR thread ===============\n");
         SIM_Attach("SLAVE1_XTOR", slave_func, 0);

    Dbug("============= spawning MONITOR_XTOR thread ===============\n");
         SIM_Attach("MONITOR_XTOR", monitor_func, 0);

    CarbonUInt32 numTicks;

     while (!SIM_CheckSignalState(MASTER_DONE)) {
      master->run(CLOCK_CYCLE);
      numTicks = master->getTick() / CLOCK_CYCLE;

      printf("Main:  cycle %4d\n", numTicks);
    }

    Dbug("============= main:  MASTER_DONE detected ===============\n");

    Dbug("\n === main     :  Waiting for MON_DONE\n");
    SIM_WaitSignal(MON_DONE, 10000000);    // Wait for master to signal DONE
    Dbug("\n === main     :  Received  MON_DONE\n");

    if(carbonDumpFlush(wave) != eCarbon_OK) {
      assert(0);
    }


    SIM_Idle(100);
  
    SIM_SetSignal(TEST_COMPLETE);   
    Dbug("\n === main   :  signaled test complete\n");

    if (myERROR == 0) printf ("** PASS **\n");
    else            printf ("** FAIL **   error count: %d\n", myERROR);

    printf("\n\nmain: Simulation complete at cycle: %d\n", master->getTick() / CLOCK_CYCLE);
    
    delete master;
}

/*
** Create a clock, bind the net, add it to the master.
** Clocks will have duty cycle = per / 2
*/
void createClock(CarbonSimMaster* master, int cycle, const char* name, int initVal) {
    CarbonSimClock *clk = new CarbonSimClock(cycle, cycle / 2, initVal);
    master->addClock(clk);
    bindNet(master, clk, name);
}

/*
** bind a net name to a function generator (clock)
** looks up the net, checks the pointer is valid, and does the binding
**
** Should just be used once per bound signal at the begining of the test
*/
void bindNet(CarbonSimMaster* m, CarbonSimFunctionGen* ck, const char* name) {
    CarbonSimNet* n = m->findNet(name);
    assert (n);
    ck->bindSignal(n);
}

/****  D A T A     C O M P A R E     function  ****/

bool DataCompare( uint32 dataR, uint32 dataX )
{
   if (dataX == dataR)
     { printf("Received data: %08x EQUALS expected data: %08x\n", dataR, dataX);
       return true;
     }
   else
     { MSG_Error("Received data: %08x NOT equal to expected data: %08x\n", dataR, dataX); 
       myERROR++;
       return false;
     }
}


bool ReadExpected( uint32 Addr, uint32 dataX)
{
   return ( DataCompare( SIM_Read32(Addr), dataX)  );
}


/**********************************************
 *           M A S T E R
 ********************************************** 
 *
 *  This function runs in parrallel to any others started in main()
 */

void master_func(void) {
  u_int32 foo;

  printf("\n>>>>>>>> Starting master_func <<<<<<<<<<\n");

  SIM_Idle(10);
  Dbug("\n>>>master_func:  Idled for 10 cycles; start write\n");

  SIM_CsWrite(0,0);            // Write default value to master config reg
  Dbug("\n>>>master_func:  Back from a CS Write(0, 0)\n");
  SIM_Idle(9);
  Dbug("\n>>>master_func:  Idled(9) - do AHB_SetProtection(0x11)\n");
  AHB_SetProtection(0x0011);   // set HPROT value
  SIM_Idle(8);
  Dbug("\n>>>master_func:  Idled(8) - do AHB_SetBurstConfig(2,1,0)\n");
  AHB_SetBurstConfig(2, 1, 0); // 32 bit, INCR, no lock
  SIM_Idle(7);
  Dbug("\n>>>master_func:  signaling SLAVE_PROCEED\n");
  SIM_SetSignal(SLAVE_PROCEED);    

  SIM_Idle(6);
  uint32 dataPat1 = 0x12345678;
  printf("\n>>>master_func:  Idled(6) - do SIM_Write32(0x320, %x08)\n", dataPat1);

  SIM_Write32(0x320, dataPat1);
  Dbug("\n>>>master_func:  Back from write; do a SIM_CsRead(0)\n");

  foo = SIM_CsRead(0);
  printf("\n>>>master_func:  Back from a CsRead(0); data= %08x\n", foo);

  SIM_CsWrite(0, 0x24);
  Dbug("\n>>>master_func:  back from a CsWrite(0,0x24)\n");

  foo = SIM_CsRead(0);
  printf("\n>>>master_func:  Back from a CsRead(0); data= %08x\n", foo);

  foo = SIM_Read32 (0x320);
  DataCompare(foo, dataPat1);

  uint32 blockPattern[] = {
	  0xdeadbeef,
	  0xb16b00b5,
	  0x91ce0a55,
	  0xbad0da7a,
	  0xcafef00d };


  Dbug("\n>>>master_func: Writing 4 32 bit words to 0x500 - 0x50f\n");

  for (uint32 i=0x500, j=0; j<4; i += 4, j++)
      	SIM_Write32(i, blockPattern[j]);  // Mem[0x500 - 0x50f] <- BlockPattern[0-3]
			

  Dbug("\n>>>master_func: checking (Read back) same 4 32 bit words\n");
  for (uint32 i=0x500, j=0; j<4; i += 4, j++)
        ReadExpected(i, blockPattern[j]);

  Dbug("\n>>>master_func: checking (reverse order Read back) same 4 32 bit words\n");
  for (int i=0x50c, j=3; j> -1; i -= 4, j--)
        ReadExpected(i, blockPattern[j]);

  // -----------------------
  // u_int32 AHB_BurstRead  (u_int32 nbits, u_int32 beats, u_int32 incr, 
  //		              u_int32 lock,  u_int32 address, u_int32 *rdata);
  // ----------------------
  uint32  burstBuffer[4];	
  
  Dbug("\n>>>master_func: Burst Read same 4 32 bit words starting at 0x500\n");
  AHB_BurstRead(32, 4, 1, 0, 0x500, &burstBuffer[0]);

  Dbug("\n>>>master_func: Check block read agrees with expected pattern\n");
  for (int j=0; j<4;  j++)
        DataCompare(burstBuffer[j], blockPattern[j]);


  SIM_Idle(4);
  SIM_SetSignal(MASTER2_DONE);  Dbug("\n>>>master_func:  signaled MASTER2_DONE (to Monitor)\n");

  Dbug("\n>>>master_func:  WAITING for MONITOR_DONE\n");
  SIM_WaitSignal(MONITOR_DONE, 1000000L);

  SIM_SetSignal(MS2_DONE);      Dbug("\n>>>master_func:  signaled MS2_DONE\n");
  SIM_SetSignal(MASTER_DONE);   Dbug("\n>>>master_func:  signaled MASTER_DONE\n");

  printf("\n>>>master_func:  end of function\n");

}

/**********************************************
 *           S L A V E
 **********************************************/
/*
  AHB_SlaveCfgCmd( 0,  // Enable User Resume
		   0,  // Split Count
		   0,  // Slave Response
		   0,  // Slave Response Count
		   0   // Wait Count
		 );
**********************/

void slave_func(void) {
  printf("\n<<<<<<<<<<< Starting SLAVE func  >>>>>>>>>>>\n");


  Dbug("\n<<< slave_func:  waiting for SLAVE_PROCEED signal\n");
  SIM_WaitSignal(SLAVE_PROCEED, 10000000L);   

  Dbug("\n<<< slave_func:  Received SLAVE_PROCEED - doing AHB_SetOkResponse(1)\n");
  AHB_SetOkResponse(1);  // 

  Dbug("\n<<< slave_func:  Waiting for MS2_DONE\n");
  SIM_WaitSignal(MS2_DONE, 10000000);    // Wait for master to signal DONE

  Dbug("\n<<< slave_func:  Received MS2_DONE, setting SLAVE_DONE\n");
  SIM_SetSignal(SLAVE_DONE);

  printf("\n<<< slave_func:  end of function\n");
}


/**********************************************
 *           M O N I T O R
 **********************************************/

void monitor_func(void) {
	
  printf("\nStarting MONITOR func\n");
  AHB_resetMonCounters();          
                                            // Track statistics until...
  printf("\nmonitor_func:  waiting for MASTER2_DONE\n");
  SIM_WaitSignal(MASTER2_DONE, 10000000);    // Wait for master to signal DONE

  printf("\nmonitor_func:  received MASTER2_DONE -- take 40 cycles to read monitor registers\n");
  // AHB_MonRegs_T *monRegs = AHB_readMonCounters();
  // AHB_printMonCounters(monRegs);
  AHB_printMonCounters(AHB_readMonCounters());
  SIM_Idle(5);
  printf("\nmonitor_func:  signaling MON_DONE\n");
  SIM_SetSignal(MON_DONE);
  SIM_SetSignal(MONITOR_DONE);

  printf("\nmonitor_func:  end of function\n");

}


/*** end of file **/
