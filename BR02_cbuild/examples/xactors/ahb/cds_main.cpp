
// test for AHB transactor 

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005-2007 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#include "libdesign.h"   /* generated header file */

// #include "xactors/xactors.h"

 #ifdef __cplusplus
extern "C" {
 #endif
 
  #include "xactors/carbonX.h"
  #include "xactors/ahb/carbonXAhbMaster.h"
  #include "xactors/ahb/carbonXAhbMonitor.h"
  #include "xactors/ahb/carbonXAhbSlave.h"

 #ifdef __cplusplus
}
 #endif


#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>


// Set to 1 for printing debug info, 0 for no printing
//
#if   1
//  #define Dbug(s) printf(s)
 #define Dbug MSG_Printf 
#else
 #define Dbug(s) 
#endif

// local synch signal
//
#define MASTER_DONE "masterdone"
#define MONITOR_DUMP "monitordump"
#define MON_DONE  "mondone"

/*
** These are some utility functions
*/

void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time);
void printHeader(char *strInfo);

void master_func(void);
void slave_func(void) ;
void monitor_func(void);


const int CLOCK_CYCLE = 20;

int    myERROR = 0;
CarbonUInt32 ONE  = 1;
CarbonUInt32 ZERO = 0;


// *************
//     Main   
// *************

int main(int argc, char *argv[]) {

    printHeader(" Carbon AHB transactor "); 
                  
    // Create Sim Infrastructure.  Master, Clocks, and Reset

    // Handle to Carbon Model
    MSG_Printf("\n============== carbon design create AHB  ==============\n\n");	
    CarbonObjectID* model = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  
     
    MSG_Milestone("Start writing VCD file\n");
    CarbonWaveID* wave = carbonWaveInitVCD(model, "ahbTb.vcd", e1ns);
    int wavStat = carbonDumpVars(wave, 6, "ahbTb");
    assert (wavStat == eCarbon_OK);                 // Kill sim if Dumpvars fails
    

    // get verilog side Clock & Reset signals
    
    CarbonNetID* clock  = carbonFindNet(model, "ahbTb.HCLK");
    CarbonNetID* resetn = carbonFindNet(model, "ahbTb.HRESETn");

    // Assert Reset for a few clocks
    //
    MSG_Printf("============== Asserting Reset ==============\n\n");
    carbonDeposit(model, resetn, &ZERO, NULL);

    Dbug("============== Run 100 Clocks  ==============\n\n");
    CarbonTime time = 0;
    for (int i=0; i<100; i++) run(model, clock, time);

    Dbug("============== Deasserting Reset ==============\n\n");
    carbonDeposit(model, resetn, &ONE, NULL);
      
    MSG_Printf("\n****** Starting Simulation ******\n");

    Dbug("============= spawning MASTER1_XTOR thread ===============\n");
         carbonXAttach("MASTER1_XTOR", (CarbonXAttachFuncT)master_func, 0);
         tbp_set_max_error_count  (1000);  // (1);
	
    Dbug("============= spawning SLAVE1_XTOR thread ===============\n");
         carbonXAttach("SLAVE1_XTOR", (CarbonXAttachFuncT)slave_func, 0);

    Dbug("============= spawning MONITOR_XTOR thread ===============\n");
         carbonXAttach("MONITOR_XTOR", (CarbonXAttachFuncT)monitor_func, 0);


         // Now that we have spawned off the threads for MASTER, SLAVE and MONITOR
         // sit here running the clock until the MASTER signals us he is done.

    CarbonUInt32 numTicks;

    while (!carbonXCheckSignalState(MASTER_DONE)) {
      run(model, clock, time);
      numTicks = time / CLOCK_CYCLE;

      printf("Main:  cycle %4d\n", numTicks);
    }

    MSG_Debug("============= main:  MASTER_DONE detected ===============\n");

    Dbug("\n === main     :  Waiting for MON_DONE\n");
    carbonXWaitSignal(MON_DONE, 10000000);    // Wait for master to signal DONE
    Dbug("\n === main     :  Received  MON_DONE\n");

    if(carbonDumpFlush(wave) != eCarbon_OK) {
      assert(0);
    }


    carbonXIdle(100);
  
    carbonXSetSignal(TEST_COMPLETE);   
    Dbug("\n === main   :  signaled test complete\n");

    // print PASS / FAIL banner

    printf ("\n\n**********\n");
    if (myERROR == 0) {printf ("** PASS **\n");}
    else              {printf ("** FAIL **   error count: %d\n", myERROR);}
    printf ("**********\n");

    MSG_Milestone("\n\nmain: Simulation complete at cycle: %d\n", time / CLOCK_CYCLE);
    
    carbonDestroy(&model);
}


//  ******
//    Run       run clock one clock period.
//  ******
void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time)
{
  carbonDeposit(model, clock, &ZERO, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
  carbonDeposit(model, clock, &ONE, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
}


#include <time.h>   // Only used for printHeader
void printHeader(char *strInfo)
{
  time_t      curtime          = time (NULL);          // Get the current time.
  struct tm  *loctime          = localtime (&curtime); // Convert it to local time representation.  
  char       *dtString         = asctime(loctime);     // convert to ascii string
  dtString[strlen(dtString)-1] = '\0';                 // remove the \n at the end of dtString
  printf("======= %s -- Starting at %s =========\n\n", strInfo, dtString);
}



/****  D A T A     C O M P A R E     function  ****/

bool DataCompare( CarbonUInt32 dataR, CarbonUInt32 dataX )
{
   if (dataX == dataR)
     { printf("Received data: 0x%08x EQUALS expected data: 0x%08x\n", dataR, dataX);
       return true;
     }
   else
     { MSG_Error("Received data: 0x%08x NOT equal to expected data: 0x%08x\n", dataR, dataX); 
       myERROR++;
       return false;
     }
}


bool ReadExpected( CarbonUInt32 Addr, CarbonUInt32 dataX)
{
   return ( DataCompare( carbonXRead32(Addr), dataX)  );
}



/**********************************************
 *           M A S T E R
 ********************************************** 
 *
 *  This function runs in parrallel to any others started in main()
 */

void master_func(void) {
  u_int32 foo;

  printf("\n>>>>>>>> Starting master_func <<<<<<<<<<\n");

  carbonXCsWrite(0,0);            // Write default value to master config reg
  
// .........................................................
// Set HPROT value. 
// This affects ALL types of transfers 
// hprot: protection (as defined by AHB spec) (Config bits [3:0]) 
//                           3 2 1 0 
// ---------------+-+-+-----+-------+ 
//   config reg   |-|-|- - -| hprot | 
// ---------------+-+-+-----+-------+ 
// .........................................................
//
  carbonXAhbSetProtection(0x0011);   // set HPROT value


// ...................................................................
// Set Burst parameters (Use this with SIM_Read/Write) 
// 
// incr:  1: INCR or 0: WRAP burst.                     (Config bit 8) 
// lock:  1: Locked burst requested; 0: Don't Lock      (Config bit 7) 
// hsize: 0: 8-bit;  1: 16-bit,  2: 32-bit transfer     (Config bits [6:4]) 
// 
// This values are ONLY used by SIM_Read/Write. 
//                 8 7 6 5 4 3 2 1 0 
// ---------------+-+-+-----+-------+ 
//   config reg   |I|L|size |- - - -| 
// ---------------+-+-+-----+-------+ 
// .....................................................................
//
  carbonXAhbSetBurstConfig(2, 1, 0); // 32 bit, INCR, no lock
 
  CarbonUInt32 dataPat1 = 0x12345678;
  carbonXWrite32(0x320, dataPat1);
  
  foo = carbonXCsRead(0);
  printf("\n>>>master_func:  CsRead(0) data= 0x%08x\n", foo);

  carbonXCsWrite(0, 0x24);

  foo = carbonXCsRead(0);
  DataCompare(foo, 0x24);

  foo = carbonXRead32 (0x320);
  DataCompare(foo, dataPat1);

  CarbonUInt32 blockPattern[] = {
	  0xdeadbeef,
	  0xb16b00b5,
	  0x91ce0a55,
	  0xbad0da7a,
	  0xcafef00d };


  Dbug("\n>>>master_func: Writing 4 32 bit words to 0x500 - 0x50f\n");

  for (CarbonUInt32 i=0x500, j=0; j<4; i += 4, j++)
      	carbonXWrite32(i, blockPattern[j]);     // Mem[0x500 - 0x50f] <- BlockPattern[0-3]
			

  Dbug("\n>>>master_func: checking (Read back) same 4 32 bit words\n");
  for (CarbonUInt32 i=0x500, j=0; j<4; i += 4, j++)
        ReadExpected(i, blockPattern[j]);

  Dbug("\n>>>master_func: checking (reverse order Read back) same 4 32 bit words\n");
  for (int i=0x50c, j=3; j> -1; i -= 4, j--)
        ReadExpected(i, blockPattern[j]);

  // -----------------------
  // u_int32 carbonxAhbBurstRead  (u_int32 nbits, u_int32 beats, u_int32 incr, 
  //		              u_int32 lock,  u_int32 address, u_int32 *rdata);
  // ----------------------
  CarbonUInt32  burstBuffer[4];	
  
  Dbug("\n>>>master_func: Burst Read same 4 32 bit words starting at 0x500\n");
  carbonXAhbBurstRead(32, 4, 1, 0, 0x500, &burstBuffer[0]);

  Dbug("\n>>>master_func: Check block read agrees with expected pattern\n");
  for (int j=0; j<4;  j++)
        DataCompare(burstBuffer[j], blockPattern[j]);


  carbonXIdle(4);
  carbonXSetSignal(MONITOR_DUMP);  Dbug("\n>>>master_func:  signaled MONITOR_DUMP (to Monitor)\n");

  Dbug("\n>>>master_func:  WAITING for MON_DONE\n");
  carbonXWaitSignal(MON_DONE, 1000000L);

  carbonXSetSignal(MASTER_DONE);   Dbug("\n>>>master_func:  signaled MASTER_DONE\n");

  printf("\n>>>master_func:  end of function\n");
}


/**********************************************
 *           S L A V E
 **********************************************/
/*
  carbonXAhbSlaveCfgCmd( 0,  // Enable User Resume
		   0,  // Split Count
		   0,  // Slave Response
		   0,  // Slave Response Count
		   0   // Wait Count
		 );
**********************/

void slave_func(void) {
  printf("\n<<<<<<<<<<< Starting SLAVE func  >>>>>>>>>>>\n");


  carbonXCsWrite(SLAVE_CFG_REG, carbonXAhbSetOkResponse(1));

  printf("\n<<< slave_func:  end of function\n");
}


/**********************************************
 *           M O N I T O R
 *
 *  The monitor collects statistics from the executing AHB bus operations.
 * 
 *  After the Master is done excersizing the bus it takes 40 clocks
 *  for the monitor to read out its  registers.
 *  
 *  So, after MASTER_DONE is signalled we can read the statistics
 *  and signal MONITOR_DONE to indicate that is complete.
 **********************************************/

void monitor_func(void) {
	
  printf("\nStarting MONITOR func -- initializing all counters\n");
  carbonXAhbresetMonCounters();          
                                            // Track statistics until...
  Dbug("\nmonitor_func:  waiting for MASTER to signal MONITOR_DUMP\n");
  carbonXWaitSignal(MONITOR_DUMP, 10000000);    // Wait for master to signal DUMP

  Dbug("\nmonitor_func:  received MONITOR_DUMP signal -- take 40 cycles to read monitor registers\n");
  carbonXAhbMonRegs_T *monRegs = carbonXAhbreadMonCounters();
  carbonXAhbprintMonCounters(monRegs);

  carbonXIdle(5);
  printf("\nmonitor_func:  signaling MON_DONE\n");
  carbonXSetSignal(MON_DONE);

  printf("\nmonitor_func:  end of function\n");

}


/*** end of file **/
