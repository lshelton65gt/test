`timescale 1 ns / 1 ns

/*------------------------
 *  Testbench to compile transactors with Carbon Design environment
 *
 *  The following models are instantiated at this top level:
 *
 *       ahb_master_xtor
 *       ahb_slave_xtor
 *       ahb_monitor_xtor.v
 *       ahb_arbiter_xtor.v

 *
 **************************************************************************
 */



module ahbTb;

`include "carbonx_ahb_params.vh"  // Global Parameters for AHB models

 parameter NUM_MASTERS = 4;

  //-////////////////////////////////////////////////////////////////////////////
  // A H B   B U S   S i g n a l s
  //-////////////////////////////////////////////////////////////////////////////


  wire  [31:0] 		  HADDR;    // Address Bus
  wire  [31:0] 	 	  HRDATA;   // Read data bus

  wire  		  HWRITE;   // Read or write transfer
  wire  [31:0] 		  HWDATA;   // Write data bus

  wire  [1:0] 		  HTRANS;   // Type of transer
  wire  [2:0] 		  HSIZE;    // Size of a transfer in each beat
  wire  [2:0] 		  HBURST;   // Burst type
  wire  [3:0]             HPROT;     // Protection Control

  wire   [3:0] 		  HMASTER;  // Active master which owns the BUS
  wire                    HMASTLOCK; // Locked transfer

  wire [NUM_MASTERS-1:0]  HSEL;     // HSELx;   // slave selected

  reg     		  HRESETn; // carbon depositSignal
  reg  		  	  HCLK;    // carbon depositSignal

  wire  		  HINTERRUPT;

  wire    		  HREADY;   // Slave ready for transfer/ insert wait cycles.
  wire    [1:0] 	  HRESP;    // Slave response (OKAY,ERROR,RETRY,SPLIT) 

  wire [NUM_MASTERS-1:0]  HSPLIT;  //  Split Resume, driven by test side 

 
  //-////////////////////////////////////////////////////////////////////////////
  // A H B   Arbiter   I n t e r f a c e
  //-////////////////////////////////////////////////////////////////////////////
  //    
                                           // from Master to Arbiter
     wire  [NUM_MASTERS-1:0]    HBUSREQ;   // carbon observeSignal  
     wire  [NUM_MASTERS-1:0]    HLOCK;     // from Master to Arbiter
     wire  [NUM_MASTERS-1:0]    HGRANT;    // from Arbiter to Master

     wire   [3:0] 		HMaster_d; // from Arbiter to monitor		   

  // wire  [NUM_MASTERS-1:0]    HSPLIT;    // from Slave  to Arbiter
  //
  // input                      HCLK;
  // input 		        HRESETn;	     
  // input 		        HREADY;    // from Slave  to Arbiter
  // input [1:0] 		HRESP;     // from Slave  to Arbiter
  // input [2:0] 		HBURST;    // from Master to Arbiter
  // input [1:0] 		HTRANS;    // from Master to Arbiter
  //
  // output [3:0] 		HMASTER;
  // output 		        HMASTLOCK;


  //-////////////////////////////////////////////////////////////////////////////
  // A H B   MASTER   I n t e r f a c e
  //-////////////////////////////////////////////////////////////////////////////

  wire   	          // Not used - oHBUSREQ, // outputs
	  		  oHLOCK,
			  oHWRITE;
  wire  [1:0] 		  oHTRANS;
  wire  [2:0] 		  oHSIZE,
			  oHBURST;
  wire  [3:0] 		  oHPROT;
  wire [31:0] 	          oHADDR;
  wire [31:0] 	          oHWDATA;


  reg    driveData;

  wire    selM1 = HGRANT[0];  // When arbiter grants, this master drives the bus  

 always @(posedge HCLK)
    driveData <= HGRANT[0] & oHWRITE;

  assign HADDR     = selM1 ? oHADDR  : 32'hZZZZZZZZ;
  assign HTRANS    = selM1 ? oHTRANS :  2'bzz;
  assign HSIZE     = selM1 ? oHSIZE  :  3'bzzz;
  assign HPROT     = selM1 ? oHPROT  :  4'bzzzz;
  assign HBURST    = selM1 ? oHBURST :  1'bz;
  // assign HMASTLOCK = selM1 ? oHLOCK  :  1'bz;  // Driven by arbiter 

  assign HWRITE    = selM1 ? oHWRITE :  1'bz;
  assign HWDATA    = driveData ? oHWDATA : 32'hZZZZZZZZ;

   assign HBUSREQ[NUM_MASTERS-1:1] = 3'b000;
   assign HLOCK[NUM_MASTERS-1:1]   = 3'b000;
   assign HINTERRUPT   = 1'b0;  
   
 initial 
   $display("ahbTb.v:  NUM_MASTERS = %d", NUM_MASTERS);


 // --------  Slave Address Select Decode  ------

    assign HSEL[0] = (HADDR[31:13] == 0);
    assign HSEL[1] = (HADDR[31:13] == 1);
    assign HSEL[2] = (HADDR[31:13] == 2);
    assign HSEL[3] = (HADDR[31:13] == 3);
    

// --------------------------------------------------------------------

carbonx_ahb_master
    MASTER1_XTOR (
  // Outputs
	 HBUSREQ[0], // oHBUSREQ,
	 HLOCK[0],   // oHLOCK,
	 oHWRITE,
	 oHTRANS,
	 oHSIZE,
	 oHBURST, 
	 oHPROT, 
 	 oHADDR,
	 oHWDATA, 
  // Inputs
	 HGRANT[0],   // HGRANT 
	 HREADY, 
	 HRESETn,
	 HCLK, 
	 HRESP,
	 HRDATA,
	 HINTERRUPT
        );


// --------------------------------------------------------------
//
// parameter ADDR_WIDTH = 10;            // Slave memory address width. Default 1Kx8bit
   parameter ADDR_WIDTH = 12;            // Slave memory address width. Default 4Kx8bit

carbonx_ahb_slave
    SLAVE1_XTOR (

  // Outputs
	 HREADY,
	 HRESP,
	 HRDATA,
	 HSPLIT, // HSPLITx, -- [NUM_MASTERS-1:0]
  // Inputs
	 HSEL[0],   // HSELx - Just 1 bit
	 HADDR,
	 HWDATA,
	 HWRITE,
	 HTRANS,
	 HSIZE,
	 HBURST,
	 HRESETn,
	 HCLK,
 
	 HMASTER,
	 HMASTLOCK,
	 HPROT
        );


// --------------------------------------------------------------------

carbonx_ahb_monitor
   MONITOR_XTOR (

	 HWRITE,  // ALL monitored ports are Inputs	
	 HTRANS,	
	 HSIZE,
	 HPROT,
	 HADDR,
	 HWDATA,
	 HRDATA,
	 HBUSREQ, 
 	 HLOCK,
	 HSPLIT,
	 HREADY,
 	 HRESP,
 	 HBURST,
 	 HGRANT,
 	 HMASTER,
 	 HMASTLOCK,
 	 HSEL,
	 HRESETn,
	 HCLK
      );

// --------------------------------------------------------------------

carbonx_ahb_arbiter
   ARBITER_XTOR (
 		 HCLK,		
		 HRESETn,	       
		 HBUSREQ, 
  		 HLOCK,
		 HSPLIT,
		 HREADY,
		 HRESP,
		 HBURST,
		 HTRANS,
		 HGRANT, 
		 HMASTER,
		 HMASTLOCK,
		 HMaster_d );

 //-//////////////////////////////////////////////////////////////////////////////

  reg [31:0]  CycleNum;  // carbon observeSignal
  initial CycleNum <= 0;
  always @(posedge HCLK) CycleNum <= CycleNum + 1;

 //-//////////////////////////////////////////////////////////////////////////////


 //-//////////////////////////////////////////////////////////////////////////////
  //                                                            8 7 6 5 4 3 2 1 0
  //                                            ---------------+-+-+-----+-------+
  //  config reg                                               |I|L| size|  hprot|
  //                                            ---------------+-+-+-----+-------+

//   ------------- End of Test Bench --------------

endmodule  
