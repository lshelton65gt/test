// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __TestMaster_h__
#define __TestMaster_h__

#include <systemc.h>
#include "tlm.h"
#include "xactors/systemc/CarbonXEnetIfs.h"

SC_MODULE(TestMaster) {
  
  // Constructor
  SC_CTOR(TestMaster);
  
  // Destructor
  ~TestMaster();

  // Port Declarations
  sc_port<tlm::tlm_master_if<CarbonXTransReqT, CarbonXTransRespT>  > tx_port;
  sc_port<tlm::tlm_master_if<CarbonXTransReqT, CarbonXTransRespT>  > rx_port;
  sc_in<bool> config_done;
  

  // Processes
  void txProcess();
  void rxProcess();
  void txStatus();
  
 private:

  // Member variables
  unsigned int   mPktCount;
  ENET_PKT_T   * mPktList;
  unsigned int   mPktsRecvd;
  unsigned int   mPktsSent;
  bool           mTxInitialized;
  bool           mRxInitialized;

  static const UInt32 interface = ENET_GMII; 
  static const UInt32 mTxPktLen = 65; 

  // Member Methods
  void buildDescriptorList();

};
    
#endif
