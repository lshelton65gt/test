/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "TestMaster.h"
using namespace std;
using namespace tlm;

TestMaster::TestMaster(sc_module_name inst) : 
  sc_module(inst),
  tx_port("tx_port"),
  rx_port("rx_port")
{
  
  SC_THREAD(txProcess);
  sensitive << config_done.pos();
  dont_initialize();

  SC_THREAD(txStatus);

  SC_THREAD(rxProcess);
  sensitive << config_done.pos();
  dont_initialize();
  
  // Default values
  mPktsRecvd     = 0;
  mPktsSent      = 0;
  mPktCount      = 10;
  mTxInitialized = false;
  mRxInitialized = false;

  // Build Descriptor List
  mPktList = new ENET_PKT_T[mPktCount];
  assert(mPktList);
  buildDescriptorList();
}

void TestMaster::txProcess() {
  
  CarbonXEnetTransReqT trans;
  u_int32 size, ipg = 12; // Set IPG to 12 as it's safe for all Interfaces

  if(!mTxInitialized) {
    
    if(!mTxInitialized) {
      cout << "Starting txProcess" << endl;
      
      // Configure Transactors
      trans.csWrite(ENET_FRTR_CONFIG, ENET_FCS_ENABLE | interface);
      tx_port->put(trans);
      
      mTxInitialized = true;
    }
  }

  for (; mPktsSent < mPktCount; mPktsSent++) {
    // Build Packet from Descriptor
    trans.buildPacket(mPktList[mPktsSent]);
    trans.setTransId(mPktsSent);
    
    // Send Packet
    tx_port->put(trans);
  }

}



void TestMaster::txStatus() {

  CarbonXEnetTransRespT response;

  while(tx_port->nb_get(response)) {
    cout << "Transaction:"      << hex << response.getTransId() 
         << " started at time " << dec << response.getStartTime()
	 << " Ending at " << response.getEndTime() << endl;
  }
  wait(tx_port->ok_to_get());
}



void TestMaster::rxProcess(){
  
  CarbonXEnetTransReqT   request;
  CarbonXEnetTransRespT  response;

  if(!mRxInitialized) {
    cout << "Starting rxProcess" << endl;

    // Mark Transactions and Config Transactions so we can through them away
    request.setTransId(0x80000000);

    // Configure Transactors
    request.csWrite(ENET_FRTR_CONFIG, ENET_FCS_ENABLE | interface);
    rx_port->nb_put(request);
    
    // Tell Transactor to start receiving packets
    request.readArray32Req(0, 0); // Hdl side will return size
    request.setRepeatCnt(0);      // Repeat receiving Transactions until further notice

    // Mark Transaction so we recognize it
    request.setTransId(0x1234);

    rx_port->nb_put(request);

    mRxInitialized = true;
  }
  
  
  // Check all Packets currently available
  for(; mPktsRecvd < mPktCount;) {
    rx_port->get(response); 
    cout << "rxprocess: Got Response with ID: 0x" 
         << hex << response.getTransId() << endl;

    if(response.getTransId() == 0x1234) {
      cout << "rxprocess: Checking Packet number "
           << dec << mPktsRecvd << endl;
      response.checkPacket(mPktList[mPktsRecvd++]);
    }
  }

  // When all packets are received, end simulation
  cout << endl << "Pkts received = "   << dec << mPktsRecvd 
       << " Total Packets Expected = " << mPktCount << endl;

  if(mPktsRecvd >= mPktCount){

     u_int32 numErrs = tbp_get_error_count();
                       cout << "**********" << endl;
     if (numErrs == 0) cout << "** PASS **" << endl;
     else              cout << "** FAIL **" << endl;
                       cout << "**********" << endl;

     cout << "All packets received -- Ending simulation" << endl;
     sc_stop();
  }  
}

//----------------------------------------------------------------------
// This function builds a list of descriptors
// onto a named queue for use by a transmit function
//----------------------------------------------------------------------
void TestMaster::buildDescriptorList(){
  u_int32 i, j;
  ENET_PKT_T enet;
 
  cout << "build_descriptor_list(): creating " << dec << mPktCount << " packets." << endl;

  // Create descriptors, fill in fields and add them to the tx queue
  for(i=0; i < mPktCount; i++) {
    
    cout << "Building packet " << dec << i << " ... " << endl;

    // enable the ethernet header and fill in the fields
    enet.sa = 0xdeadfacefeedLL;
    enet.da = 0x112233445566LL;
    enet.typelength = mTxPktLen +i;
    enet.pl_size    = mTxPktLen + i;    

    // Disable the VLAN Header
    enet.vlan.enable = 0;

    // Allocate Memory for payload buffer
    enet.payload = new u_int8[mTxPktLen +i];
    assert(enet.payload);
 
    // There is only a pdu in this example, so just fill in the
    // pdu fields that interest us.
    for (j = 0; j < mTxPktLen +i; j+= 1) enet.payload[j] = j;
    enet.ipg = 5;
    mPktList[i] = enet;

  } //--end of for(i=0; i<pkt_count; i++)

}


TestMaster::~TestMaster() {
  delete mPktList;

  if(mPktsRecvd < mPktCount) {
    MSG_Error("Didn't receive all packets. Received %d, Expected %d\n", mPktsRecvd, mPktCount);
  }
}
