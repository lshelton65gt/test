
// Ethernet GMII SystemC / transactor Verification Module
/***************************************************************************
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 //  Copyright 2006-2007 Carbon Design Systems, Inc.  All Rights Reserved. 
 //  Portions of this software code are licensed to Carbon Design Systems 
 //  and are protected by copyrights of its licensors.
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * ***************************************************************************
 * *
 * * This file contains the functions required to pass ethernet 
 * * encapsulated packets from a tx transactor to an rx transactor.
 * *
 * * This test transmits 10 good ethernet packets from the enet tx xactor
 * * and receives 10 good ethernet packets at the enet rx xactor.
 * * The rx xactor checks the validity of the received packet.
 * *
 * ****************************************************************************/
//
//   include files
//

#include "systemc.h"
#include "libdesign.systemc.h"
#include "xactors/carbonX.h"
#include "TestMaster.h"

#include "xactors/systemc/CarbonXTransLoopSysC.h"
#include "xactors/systemc/CarbonXTlmFifo.h"
#include "xactors/systemc/CarbonXTlmReqRspChannel.h"

#include <cassert>

using namespace std;
using namespace tlm;


extern "C" void tbp_print_error_status(void);
extern "C" int  tbp_get_error_count(void);

#define TRANSMITTER "xmitter"
#define RECEIVER "receiver"

#define SIM_MAX_TICKS 10000L

//
// local functions - in this file
//
static void build_descriptor_list(ENET_PKT_T *pkt_list, u_int32 pkt_count);


// -------------------- TestBench --------------------

SC_MODULE(testbench)
{
  sc_clock                              clk;

  sc_clock         	                sys_clk;     // enetTb.sys_clk
  sc_signal<bool> 	                sys_reset_l; // enetTb.sys_reset_l
  sc_signal<bool>                       config_done;
  
  // Test Module
  TestMaster                            *test_master;
 
  // Ethernet TX Channel
  CarbonXTransLoopSysC                  tx_enet_xtor;
  CarbonXTlmReqRspChannel<CarbonXTransReqT, CarbonXTransRespT> tx_req_resp;

  // Ethernet RX Channel
  CarbonXTransLoopSysC                  rx_enet_xtor;
  CarbonXTlmReqRspChannel<CarbonXTransReqT, CarbonXTransRespT> rx_req_resp;
  
  // Carbon Model Object
  gmiiTb *pEnetTb;

  void tConfigure(void);
  void tPrint(void);

  testbench(sc_module_name name);
  ~testbench();

  SC_HAS_PROCESS(testbench);
};

testbench::testbench(sc_module_name name)
: sc_module(name)
, sys_clk("sys_clk", 20, 0.5, 0, false)
, sys_reset_l("sys_reset_l")
, config_done("config_done")
, tx_enet_xtor("tx_enet_xtor", TRANSMITTER)
, tx_req_resp("tx_req_resp")
, rx_enet_xtor("rx_enet_xtor", RECEIVER)
, rx_req_resp("rx_req_resp")
{
  cout << name << ":gmii SystemC Ethernet testbench starting" << endl;

  // Create Sim Infrastructure.  testBench, Clocks, and Reset
  // Handle to main simulation object
  cout << "\n============== create carbon testBench  ==============\n" << endl;	
  pEnetTb = new gmiiTb("pEnetTb");
  assert(pEnetTb);
  test_master = new TestMaster("test_master");

  // Hook up clk and reset to the Carbon Model
  pEnetTb->sys_clk(sys_clk);
  pEnetTb->sys_reset_l(sys_reset_l); 
    
  // Connect the SystemC Tx Transactor objects
  test_master->tx_port(tx_req_resp.master_export);
  tx_enet_xtor.trans_port(tx_req_resp.slave_export);
  
  // Connect the SystemC Rx Transactor objects
  test_master->rx_port(rx_req_resp.master_export);
  rx_enet_xtor.trans_port(rx_req_resp.slave_export);

  test_master->config_done(config_done);

  cout << "=============== Start writing VCD file ===================\n" << endl;

  // wave dumping
  // ------------
  pEnetTb->carbonSCWaveInitVCD("carbon.vcd", SC_NS);
  pEnetTb->carbonSCDumpVars();

  // Setup Threads
  // -------------
  SC_THREAD(tConfigure);
  SC_THREAD(tPrint);

}


// Destructor

testbench::~testbench()
{
  pEnetTb->carbonSCDumpFlush();
  delete pEnetTb;
  tbp_print_error_status();
}


void testbench::tConfigure(void)
{
  // Assert Reset for a few clocks
  //
  cout << "============== Asserting Reset ==============\n" << endl;
  sys_reset_l.write(false); // assert

  cout << "============== Run 100 ticks  ==============\n" << endl;
  wait( sys_clk.period() * 100 ); 
	   
  cout << "============== Deasserting Reset ==============\n" << endl;
  sys_reset_l.write(true); // release

  wait( sys_clk.period() * 10 ); 

  config_done.write(true);
}


void testbench::tPrint(void)
{
  for( long numTicks= 0; numTicks < SIM_MAX_TICKS ; ) 
    wait( sys_clk.period() * 100 ); 

  
  cout << " === Error: Simulation exceeded time"
       << " limit of " << SIM_MAX_TICKS << " ticks (SIM_MAX_TICKS)" << endl << endl;
}



// *****************
//   SystemC  Main   
// *****************

int sc_main (int argc , char *argv[]) 
{
  testbench *tb;
  tb = new testbench("Gmii_Ethernet_Testbench");

  sc_start();

  delete tb;
  return 0;
}

// -- eof --
