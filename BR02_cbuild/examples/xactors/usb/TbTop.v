/**** UTMI Level 0 Glue Logic connecting Host and Device Transactors ****/

module UtmiBus(SystemClock,
        /* Phy Ports */
        PHY_Reset,
        PHY_DataBus16_8,
        PHY_SuspendM,
        PHY_DataInLo,
        PHY_DataInHi,
        PHY_TXValid,
        PHY_TXValidH,
        PHY_TXReady,
        PHY_DataOutLo,
        PHY_DataOutHi,
        PHY_RXValid,
        PHY_RXValidH,
        PHY_RXActive,
        PHY_RXError,
        PHY_XcvrSelect,
        PHY_TermSelect,
        PHY_OpMode,
        PHY_LineState,

        /* Link Ports */
        LINK_Reset,
        LINK_DataBus16_8,
        LINK_SuspendM,
        LINK_DataInLo,
        LINK_DataInHi,
        LINK_TXValid,
        LINK_TXValidH,
        LINK_TXReady,
        LINK_DataOutLo,
        LINK_DataOutHi,
        LINK_RXValid,
        LINK_RXValidH,
        LINK_RXActive,
        LINK_RXError,
        LINK_XcvrSelect,
        LINK_TermSelect,
        LINK_OpMode,
        LINK_LineState
        );

input SystemClock;

/* Phy ports */
output PHY_Reset;
output PHY_DataBus16_8;
output PHY_SuspendM;
output [7 : 0] PHY_DataInLo;
output [7 : 0] PHY_DataInHi;
output PHY_TXValid;
output PHY_TXValidH;
input  PHY_TXReady;
input  [7 : 0] PHY_DataOutLo;
input  [7 : 0] PHY_DataOutHi;
input  PHY_RXValid;
input  PHY_RXValidH;
input  PHY_RXActive;
input  PHY_RXError;
output PHY_XcvrSelect;
output PHY_TermSelect;
output [1 : 0] PHY_OpMode;
input  [1 : 0] PHY_LineState;

/* Link ports */
input  LINK_Reset;
input  LINK_DataBus16_8;
input  LINK_SuspendM;
input  [7 : 0] LINK_DataInLo;
input  [7 : 0] LINK_DataInHi;
input  LINK_TXValid;
input  LINK_TXValidH;
output LINK_TXReady;
output [7 : 0] LINK_DataOutLo;
output [7 : 0] LINK_DataOutHi;
output LINK_RXValid;
output LINK_RXValidH;
output LINK_RXActive;
output LINK_RXError;
input  LINK_XcvrSelect;
input  LINK_TermSelect;
input  [1 : 0] LINK_OpMode;
output [1 : 0] LINK_LineState;

/* signal assignments */
assign PHY_Reset = LINK_Reset;
assign PHY_DataBus16_8 = LINK_DataBus16_8;
assign PHY_SuspendM = LINK_SuspendM;
assign PHY_DataInLo = LINK_DataInLo;
assign PHY_DataInHi = LINK_DataInHi;
assign PHY_TXValid = LINK_TXValid;
assign PHY_TXValidH = LINK_TXValidH;
assign LINK_TXReady = PHY_TXReady;
assign LINK_DataOutLo = PHY_DataOutLo;
assign LINK_DataOutHi = PHY_DataOutHi;
assign LINK_RXValid = PHY_RXValid;
assign LINK_RXValidH = PHY_RXValidH;
assign LINK_RXActive = PHY_RXActive;
assign LINK_RXError = PHY_RXError;
assign PHY_XcvrSelect = LINK_XcvrSelect;
assign PHY_TermSelect = LINK_TermSelect;
assign PHY_OpMode = LINK_OpMode;
assign LINK_LineState = PHY_LineState;

endmodule
