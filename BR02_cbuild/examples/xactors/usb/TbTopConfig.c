/*****************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include <stdio.h>
#include "carbon/c_memmanager.h"
#include "TbTopConfig.h"

/* Configuration parameters at host */
CarbonUInt7 deviceAddress = 0;
CarbonUInt8 numConfigs = 0;
CarbonXUsbConfigurationDescriptor *configDescriptorList[256];

extern CarbonTime t;

/*******************************************************************************
 * Sets following device descriptor to device configuration
 *
 * Device Configuration:
 * - Device Speed = Full Speed
 * - Total Number of Configurations = 1
 *       Configuration Number = 1
 *       Configuration Length = 0x27
 * - Total Number of Interfaces = 1
 * - Total Number of Pipes = 4
 * - Default Control Pipe EP0:
 *       Max Packet Size = 32 Bytes
 * - Bulk OUT Pipe EP1:
 *       Endpoint Address = 1
 *       Max Packet Size = 32 Bytes
 * - Bulk IN Pipe EP1:
 *       Endpoint Address = 1
 *       Max Packet Size = 8 Bytes
 * - Control Pipe EP2:
 *       Endpoint Address = 2
 *       Max Packet Size = 16 Bytes
 ******************************************************************************/
CarbonUInt32 setDeviceConfiguration(CarbonXUsbConfig *config)
{
    CarbonUInt8 *bytes = NULL;

    /* configuring device descriptor */
    {
        /* creating device descriptor for Transactor DeviceXtor */
        CarbonXUsbDeviceDescriptor *deviceDesc = (CarbonXUsbDeviceDescriptor*)
            carbonmem_malloc(sizeof(CarbonXUsbDeviceDescriptor));
        deviceDesc->bcdUSB = 0x200;
        deviceDesc->bDeviceClass = 0x0;
        deviceDesc->bDeviceSubClass = 0x0;
        deviceDesc->bDeviceProtocol = 0x0;
        deviceDesc->bMaxPacketSize0 = 32;
        deviceDesc->idVendor = 0x58F;
        deviceDesc->idProduct = 0x6387;
        deviceDesc->bcdDevice = 0x141;
        deviceDesc->iManufacturer = 0x1;
        deviceDesc->iProduct = 0x2;
        deviceDesc->iSerialNumber = 0x3;
        deviceDesc->bNumConfigurations = 0x1;

        /* creating byte array from device descriptor */
        bytes = carbonXUsbCreateBytesFromDeviceDescriptor(deviceDesc);

        /* check if the descriptor is OK */
        if (carbonXUsbCheckDescriptor(bytes, CarbonXUsbSpeed_FS) == 0)
        {
            printf("ERROR in device descriptor\n");
            carbonXUsbDestroyDeviceDescriptor(deviceDesc);
            carbonmem_free(bytes);
            return 0;
        }

        /* registering device descriptor to the configuration */
        carbonXUsbConfigSetDeviceDescriptor(config, bytes);

        /* destroying device descriptor structure */
        carbonXUsbDestroyDeviceDescriptor(deviceDesc);

        /* destroying device descriptor array */
        carbonmem_free(bytes);
    }

    /* configuring configuration descriptor */
    {
        CarbonXUsbInterfaceDescriptor *intfDesc;
        CarbonXUsbEndpointDescriptor *ep1, *ep2, *ep3;

        /* creating configuration descriptor for Transactor DeviceXtor */
        CarbonXUsbConfigurationDescriptor *configDesc
            = (CarbonXUsbConfigurationDescriptor*)
            carbonmem_malloc(sizeof(CarbonXUsbConfigurationDescriptor));
        configDesc->wTotalLength = 0x27;
        configDesc->bNumInterfaces = 0x1;
        configDesc->bConfigurationValue = 0x1;
        configDesc->iConfiguration = 0x0;
        configDesc->bmAttributes = 0x80;     /* Bus_Powered */
        configDesc->bMaxPower = 0x32;
        configDesc->intfDescriptor = (CarbonXUsbInterfaceDescriptor**)
            carbonmem_malloc(sizeof(CarbonXUsbInterfaceDescriptor*));

        /* creating interface descriptor */
        configDesc->intfDescriptor[0] = (CarbonXUsbInterfaceDescriptor*)
            carbonmem_malloc(sizeof(CarbonXUsbInterfaceDescriptor));
        intfDesc = configDesc->intfDescriptor[0];
        intfDesc->bInterfaceNumber = 0x0;
        intfDesc->bAlternateSetting = 0x0;
        intfDesc->bNumEndpoints = 0x3;
        intfDesc->bInterfaceClass = 0x8;     /* Mass Storage */
        intfDesc->bInterfaceSubClass = 0x6;  /* SCSI Transparent Command Set */
        intfDesc->bInterfaceProtocol = 0x50; /* Bulk-Only Transport */
        intfDesc->iInterface = 0x0;
        intfDesc->epDescriptor = (CarbonXUsbEndpointDescriptor**)
            carbonmem_malloc(3 * sizeof(CarbonXUsbEndpointDescriptor*));

        /* creating endpoint descriptor */
        intfDesc->epDescriptor[0] = (CarbonXUsbEndpointDescriptor*)
            carbonmem_malloc(sizeof(CarbonXUsbEndpointDescriptor));
        intfDesc->epDescriptor[1] = (CarbonXUsbEndpointDescriptor*)
            carbonmem_malloc(sizeof(CarbonXUsbEndpointDescriptor));
        intfDesc->epDescriptor[2] = (CarbonXUsbEndpointDescriptor*)
            carbonmem_malloc(sizeof(CarbonXUsbEndpointDescriptor));
        ep1 = intfDesc->epDescriptor[0];
        ep2 = intfDesc->epDescriptor[1];
        ep3 = intfDesc->epDescriptor[2];

        /* endpoint descriptor for Bulk OUT */
        ep1->bEndpointAddress = 0x1;  /* OUT */
        ep1->bmAttributes = 0x2;      /* USB_ENDPOINT_TYPE_BULK */
        ep1->wMaxPacketSize = 32;
        ep1->bInterval = 0x0;

        /* endpoint descriptor for Bulk IN  */
        ep2->bEndpointAddress = 0x81; /* IN */
        ep2->bmAttributes = 0x2;      /* USB_ENDPOINT_TYPE_BULK */
        ep2->wMaxPacketSize = 8;
        ep2->bInterval = 0x0;

        /* control endpoint descriptor */
        ep3->bEndpointAddress = 0x2;  /* direction immaterial for control ep */
        ep3->bmAttributes = 0x0;      /* USB_ENDPOINT_TYPE_CONTROL */
        ep3->wMaxPacketSize = 16;
        ep3->bInterval = 0x0;

        /* creating bytes from configuration descriptor */
        bytes = carbonXUsbCreateBytesFromConfigurationDescriptor(configDesc);

        /* destroying configuration descriptor structure */
        carbonXUsbDestroyConfigurationDescriptor(configDesc);

        /* check if the descriptor is OK */
        if (carbonXUsbCheckDescriptor(bytes, CarbonXUsbSpeed_FS) == 0)
        {
            printf("ERROR in configuration descriptor\n");
            carbonXUsbConfigAddConfigDescriptor(config, bytes);
            carbonmem_free(bytes);
            return 0;
        }

        /* adding configuration descriptor to the configuration */
        carbonXUsbConfigAddConfigDescriptor(config, bytes);

        /* destroying configuration descriptor array */
        carbonmem_free(bytes);
    }
    return 1;
}

/* Creates GetDescriptor transaction */
CarbonXUsbTrans *createGetDescriptorTrans(CarbonXUsbPipe *pipe,
        CarbonXUsbDescriptorType descriptorType, CarbonUInt8 index)
{
    CarbonXUsbTrans *trans = carbonXUsbTransCreate(pipe);
    carbonXUsbTransSetRequestType(trans, 0x80);
    carbonXUsbTransSetRequest(trans, 6);
    carbonXUsbTransSetRequestValue(trans, descriptorType << 8 | index);
    carbonXUsbTransSetRequestIndex(trans, 0);
    carbonXUsbTransSetRequestLength(trans,
            descriptorType == CarbonXUsbDescriptor_DEVICE ? 18 : 0xFFFF);
    return trans;
}

/* Creates GetDescriptor transaction */
CarbonXUsbTrans *createSetConfigurationTrans(CarbonXUsbPipe *pipe,
        CarbonUInt8 configIndex)
{
    CarbonXUsbTrans *trans = carbonXUsbTransCreate(pipe);
    carbonXUsbTransSetRequestType(trans, 0x00);
    carbonXUsbTransSetRequest(trans, 9);
    carbonXUsbTransSetRequestValue(trans, configIndex);
    carbonXUsbTransSetRequestIndex(trans, 0);
    carbonXUsbTransSetRequestLength(trans, 0);
    return trans;
}

/* Process configuration transaction at host
 * Returns '1' if transaction is processed */
CarbonUInt32 processConfigTransAtHost(CarbonXUsbID HostXtor,
        CarbonXUsbTrans* trans)
{
    int i;
    const char *xtorName = carbonXUsbGetName(HostXtor);
    CarbonXUsbPipe *defaultControlPipe = carbonXUsbTransGetPipe(trans);
    /* if the pipe is not default control pipe, do not process */
    if (carbonXUsbPipeGetEpNumber(defaultControlPipe) != 0)
        return 0;
    /* check for different standard device requests and process */
    switch (carbonXUsbTransGetRequestType(trans) << 8 |
            carbonXUsbTransGetRequest(trans))
    {
        case 0x0005: /* SetAddress */
        {
            printf("%s[%d]: Bus enumeration complete.\n", xtorName, (int) t);
            /* save the device address */
            deviceAddress = carbonXUsbTransGetRequestValue(trans) & 0x7F;
            /* after bus enumeration, get the device descriptor */
            trans = createGetDescriptorTrans(defaultControlPipe,
                    CarbonXUsbDescriptor_DEVICE, 0);
            carbonXUsbStartNewTransaction(HostXtor, trans);
            break;
        }
        case 0x8006: /* GetDescriptor */
        {
            CarbonXUsbDescriptorType descriptorType;

            /* before bus enumeration, do not process GetDescriptor */
            if (carbonXUsbPipeGetDeviceAddress(defaultControlPipe) == 0)
                return 1;

            descriptorType =
                (carbonXUsbTransGetRequestValue(trans) >> 8) & 0xFF;
            switch (descriptorType)
            {
                /* when device descriptor is received, extract number of
                 * configuration descriptors, and get all configuration
                 * descriptors */
                case CarbonXUsbDescriptor_DEVICE:
                {
                    CarbonXUsbDeviceDescriptor *deviceDescriptor;

                    printf("%s[%d]: GetDescriptor(Device) complete.\n",
                            xtorName, (int) t);

                    /* extract number of configuration descriptors */
                    deviceDescriptor = carbonXUsbGetDeviceDescriptorFromBytes(
                                carbonXUsbTransGetData(trans));
                    numConfigs = deviceDescriptor->bNumConfigurations;
                    carbonXUsbDestroyDeviceDescriptor(deviceDescriptor);

                    /* get all configuration descriptors */
                    for (i = 0; i < numConfigs; i++)
                    {
                        trans = createGetDescriptorTrans(defaultControlPipe,
                                CarbonXUsbDescriptor_CONFIGURATION, i + 1);
                        carbonXUsbStartNewTransaction(HostXtor, trans);

                        configDescriptorList[i] = NULL;
                    }
                    break;
                }
                /* configuration descriptor received, save all descriptors */
                case CarbonXUsbDescriptor_CONFIGURATION:
                {
                    /* save all descriptors */
                    CarbonUInt8 configIndex =
                        carbonXUsbTransGetRequestValue(trans) & 0xFF;
                    printf("%s[%d]: GetDescriptor(Config %d) complete.\n",
                            xtorName, (int) t, configIndex);
                    for (i = 0; i < numConfigs; i++)
                    {
                        if (configDescriptorList[i] == NULL)
                        {
                            configDescriptorList[i] =
                                carbonXUsbGetConfigurationDescriptorFromBytes(
                                        carbonXUsbTransGetData(trans));
                            break;
                        }
                    }

                    /* when all config descriptors are retrieved, start
                     * SetConfiguration */
                    if (i == numConfigs - 1) /* i.e. all configs are obtained */
                    {
                        CarbonUInt16 ActiveConfigNumber = 1;
                        trans = createSetConfigurationTrans(defaultControlPipe,
                                ActiveConfigNumber & 0xFF);
                        carbonXUsbStartNewTransaction(HostXtor, trans);
                    }
                    break;
                }
                default: return 0;
            }
            break;
        }
        case 0x0009: /* SetConfiguration */
        {
            CarbonUInt16 activeConfigNumber =
                carbonXUsbTransGetRequestValue(trans);
            CarbonXUsbConfigurationDescriptor *activeConfig =
                configDescriptorList[activeConfigNumber - 1];
            printf("%s[%d]: SetConfiguration(%d) complete.\n",
                    xtorName, (int) t, activeConfigNumber);
            for (i = 0; i < activeConfig->bNumInterfaces; i++)
            {
                int j;
                for (j = 0; j < activeConfig->intfDescriptor[i]->bNumEndpoints;
                        j++)
                {
                    CarbonUInt8 *epDescriptor =
                        carbonXUsbCreateBytesFromEndpointDescriptor(
                                activeConfig->intfDescriptor[i]
                                ->epDescriptor[j]);
                    carbonXUsbCreatePipe(HostXtor, deviceAddress, epDescriptor);
                    carbonmem_free(epDescriptor);
                }
            }
            return 1;
            break;
        }
        default: return 0;
    }
    return 1;
}

/* Deletes all configuration parameters */
void destroyConfigParameters()
{
    int i;
    for (i = 0; i < numConfigs && configDescriptorList[i] != NULL; i++)
        carbonXUsbDestroyConfigurationDescriptor(configDescriptorList[i]);
}
