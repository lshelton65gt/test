/*****************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "xactors/usb/carbonXUsb.h"
#include "TbTopConfig.h"
#include "libTbTop.h"

/* Globals */
CarbonTime t = 0;
static void startTransactions(CarbonXUsbID HostXtor);
static void deviceSetDefaultResponseCb(CarbonXUsbTrans*, void*);
static void deviceSetResponseCb(CarbonXUsbTrans*, void*);
static void hostSetResponseCb(CarbonXUsbTrans*, void*);
static void hostReportTransactionCb(CarbonXUsbTrans*, void*);
static void hostReportCommandCb(CarbonXUsbCommand, void*);
static unsigned int transCount = 0;
extern CarbonUInt32 CarbonXUsbMaxMemory;

int main()
{
    int clocktick = 0;

    CarbonXUsbID HostXtor, DeviceXtor;
    CarbonObjectID *TbTop;
    CarbonNetID *SystemClock;
    CarbonWaveID *vcd;

    CarbonXUsbConfig *config = carbonXUsbConfigCreate();
    /* carbonXUsbConfigSetUtmiDataBusWidth(config,
            CarbonXUsbUtmiDataBusWidth_16); */

    /* Create USB Transactor HostXtor */
    carbonXUsbConfigSetIntfSide(config, CarbonXUsbIntfSide_LINK);
    HostXtor = carbonXUsbCreate("HOST", CarbonXUsb_Host, config,
            &hostReportTransactionCb, NULL, &hostSetResponseCb,
            NULL, &hostReportCommandCb, NULL, NULL, &HostXtor);
    if (HostXtor == NULL)
    {
        printf("ERROR: Unable to create USB Transactor ...\n");
        exit(EXIT_FAILURE);
    }

    /* Create USB Transactor DeviceXtor */
    carbonXUsbConfigSetIntfSide(config, CarbonXUsbIntfSide_PHY);
    if (!setDeviceConfiguration(config))
        exit(EXIT_FAILURE);
    DeviceXtor = carbonXUsbCreate("DEVICE", CarbonXUsb_Device, config,
            NULL, &deviceSetDefaultResponseCb, &deviceSetResponseCb,
            NULL, NULL, NULL, NULL, &DeviceXtor);
    if (DeviceXtor == NULL)
    {
        printf("ERROR: Unable to create USB Transactor ...\n");
        exit(EXIT_FAILURE);
    }
    carbonXUsbConfigDestroy(config);

    /* Create Carbon model glue logic for back-to-back transactor connection */
    TbTop = carbon_TbTop_create(eCarbonIODB, eCarbon_NoFlags);
    if (TbTop == NULL)
    {
        printf("ERROR: Unable to create Carbon model ...\n");
        exit(EXIT_FAILURE);
    }
    vcd = carbonWaveInitVCD(TbTop, "TbTop.vcd", e1ns);

    /* Get handles to system clock */
    SystemClock = carbonFindNet(TbTop, "UtmiBus.SystemClock");
    assert(SystemClock);

    {
        /* PHY Xtor connection to Carbon model */
        CarbonXInterconnectNameNamePair PhyXtorConnection[] = {
            { "Reset",       "PHY_Reset"       },
            { "DataBus16_8", "PHY_DataBus16_8" },
            { "SuspendM",    "PHY_SuspendM"    },
            { "DataInLo",    "PHY_DataInLo"    },
            { "DataInHi",    "PHY_DataInHi"    },
            { "TXValid",     "PHY_TXValid"     },
            { "TXValidH",    "PHY_TXValidH"    },
            { "TXReady",     "PHY_TXReady"     },
            { "DataOutLo",   "PHY_DataOutLo"   },
            { "DataOutHi",   "PHY_DataOutHi"   },
            { "RXValid",     "PHY_RXValid"     },
            { "RXValidH",    "PHY_RXValidH"    },
            { "RXActive",    "PHY_RXActive"    },
            { "RXError",     "PHY_RXError"     },
            { "XcvrSelect",  "PHY_XcvrSelect"  },
            { "TermSelect",  "PHY_TermSelect"  },
            { "OpMode",      "PHY_OpMode"      },
            { "LineState",   "PHY_LineState"   },
            { NULL /* NULL here indicates end of list */, NULL } };

        /* LINK Xtor connection to Carbon model */
        CarbonXInterconnectNameNamePair LinkXtorConnection[] = {
            { "Reset",       "LINK_Reset"       },
            { "DataBus16_8", "LINK_DataBus16_8" },
            { "SuspendM",    "LINK_SuspendM"    },
            { "DataInLo",    "LINK_DataInLo"    },
            { "DataInHi",    "LINK_DataInHi"    },
            { "TXValid",     "LINK_TXValid"     },
            { "TXValidH",    "LINK_TXValidH"    },
            { "TXReady",     "LINK_TXReady"     },
            { "DataOutLo",   "LINK_DataOutLo"   },
            { "DataOutHi",   "LINK_DataOutHi"   },
            { "RXValid",     "LINK_RXValid"     },
            { "RXValidH",    "LINK_RXValidH"    },
            { "RXActive",    "LINK_RXActive"    },
            { "RXError",     "LINK_RXError"     },
            { "XcvrSelect",  "LINK_XcvrSelect"  },
            { "TermSelect",  "LINK_TermSelect"  },
            { "OpMode",      "LINK_OpMode"      },
            { "LineState",   "LINK_LineState"   },
            { NULL /* NULL here indicates end of list */, NULL } };
        carbonXUsbConnectByModelSignalName(HostXtor, LinkXtorConnection,
                "UtmiBus", TbTop);
        carbonXUsbConnectByModelSignalName(DeviceXtor, PhyXtorConnection,
                "UtmiBus", TbTop);
    }

    /* Dump all nets */
    carbonDumpVars(vcd, 1, "UtmiBus");

    /* Device attachment for PHY device */
    carbonXUsbStartCommand(DeviceXtor, CarbonXUsbCommand_DeviceAttach);

    /* Simulation */
    for (t = 0; t < 1000000; t += 1)
    {
        if ((t % 5) == 0)
        {
            CarbonUInt32 val;
            ++clocktick;
            val = clocktick & 1;
            carbonDeposit(TbTop, SystemClock, &val, NULL);

            if (val) /* At posedge of clock */
            {
                /* Refreshes input Carbon model connections of
                 * master and slave transactors */
                carbonXUsbRefreshInputs(HostXtor, t);
                carbonXUsbRefreshInputs(DeviceXtor, t);

                /* Evaluate master and slave transactors */
                carbonXUsbEvaluate(HostXtor, t);
                carbonXUsbEvaluate(DeviceXtor, t);

                carbonSchedule(TbTop, t);

                /* Refreshes output Carbon model connections of
                 * master and slave transactors */
                carbonXUsbRefreshOutputs(HostXtor, t);
                carbonXUsbRefreshOutputs(DeviceXtor, t);
            }
            else
                carbonSchedule(TbTop, t);
        }
        else
            carbonSchedule(TbTop, t);
    }

    assert (carbonDumpFlush(vcd) == eCarbon_OK);
    carbonXUsbDestroy(HostXtor);
    carbonXUsbDestroy(DeviceXtor);
    destroyConfigParameters();

    return 0;
}

void startTransactions(CarbonXUsbID HostXtor)
{
    CarbonUInt32 pipeCount = carbonXUsbGetCurrentPipeCount(HostXtor);
    CarbonXUsbPipe * const * pipeList = carbonXUsbGetCurrentPipes(HostXtor);
    unsigned int i;
    for (i = 0; i < pipeCount; i++)
    {
        CarbonXUsbTrans* trans;
        CarbonXUsbPipe *pipe = pipeList[i];
        switch (carbonXUsbPipeGetType(pipe))
        {
            case CarbonXUsbPipe_CONTROL:
            {
                /* No transaction are pushed to control pipe */
                if (carbonXUsbPipeGetEpNumber(pipe) == 0)
                    break;
                {
                    int i;
                    CarbonUInt8 data[20];
                    CarbonXUsbTransRetry retryStates[] = { { 1, 3 }, { 3, 6 } };
                    for (i = 0; i < 20; i++)
                        data[i] = i;

                    /* Out control transfer */
                    trans = carbonXUsbTransCreate(pipe);
                    carbonXUsbTransSetRequestType(trans, 0x00);
                    carbonXUsbTransSetRequestLength(trans, 20);
                    carbonXUsbTransSetData(trans, 20, data);
                    carbonXUsbTransSetRetryStates(trans, 2, retryStates);

                    printf("HOST: Starting an Out Control transaction "
                            "with 20 data bytes ...\n");
                    carbonXUsbStartNewTransaction(HostXtor, trans);
                }
                {
                    /* In control transfer */
                    CarbonXUsbTransRetry retryStates[] = { { 1, 3 },
                        { 2, 2 }, { 4, 2 } };
                    trans = carbonXUsbTransCreate(pipe);
                    carbonXUsbTransSetRequestType(trans, 0x80);
                    carbonXUsbTransSetRequestLength(trans, 20);
                    carbonXUsbTransSetRetryStates(trans, 2, retryStates);

                    printf("HOST: Starting an In Control transaction "
                            "with 20 data bytes ...\n");
                    carbonXUsbStartNewTransaction(HostXtor, trans);
                }
                break;
            }
            case CarbonXUsbPipe_BULK:
            {
                if (carbonXUsbPipeGetDirection(pipe) ==
                        CarbonXUsbPipeDirection_OUTPUT)
                {
                    int i;
                    CarbonUInt8 data[50];
                    CarbonXUsbTransRetry retryStates[] = { { 1, 3 }, { 3, 6 } };
                    for (i = 0; i < 50; i++)
                        data[i] = i;

                    /* Out control transfer */
                    trans = carbonXUsbTransCreate(pipe);
                    carbonXUsbTransSetData(trans, 50, data);
                    carbonXUsbTransSetRetryStates(trans, 2, retryStates);

                    printf("HOST: Starting an Out Bulk transaction "
                            "with 50 data bytes ...\n");
                    carbonXUsbStartNewTransaction(HostXtor, trans);
                }
                else /* Bulk Input */
                {
                    /* In control transfer */
                    CarbonXUsbTransRetry retryStates[] = { { 1, 3 },
                        { 2, 2 }, { 4, 2 } };
                    trans = carbonXUsbTransCreate(pipe);
                    carbonXUsbTransSetRetryStates(trans, 2, retryStates);

                    printf("HOST: Starting an In Bulk transaction "
                            "with 30 data bytes ...\n");
                    carbonXUsbStartNewTransaction(HostXtor, trans);
                }
                break;
            }
            default: break;
        }
    }
}

void deviceSetDefaultResponseCb(CarbonXUsbTrans* trans, void* stimulusInstance)
{
    CarbonXUsbID xtor = *((CarbonXUsbID*) stimulusInstance);
    const char *xtorName = carbonXUsbGetName(xtor);
    printf("%s[%d]: Receiving transaction ...\n", xtorName, (int) t);

    /* For input control transfer that requires data, provide data */
    if (carbonXUsbPipeGetType(carbonXUsbTransGetPipe(trans)) ==
            CarbonXUsbPipe_CONTROL) /* Control transfer */
    {
        if ((carbonXUsbTransGetRequestType(trans) >> 7) && /* Input direction */
            carbonXUsbTransGetRequestLength(trans) > 0)
        {
            int i;
            CarbonUInt8 data[100];
            for (i = 0; i < carbonXUsbTransGetRequestLength(trans); i++)
                data[i] = 100 - i;

            printf("Providing data for input transaction ...\n");
            carbonXUsbTransSetData(trans,
                    carbonXUsbTransGetRequestLength(trans), data);
        }
    }
    else if (carbonXUsbPipeGetDirection(carbonXUsbTransGetPipe(trans)) ==
            CarbonXUsbPipeDirection_INPUT) /* For other input transfers */
    {
        int i;
        CarbonUInt8 data[30];
        for (i = 0; i < 30; i++)
            data[i] = 30 - i;

        printf("Providing data for input transaction ...\n");
        carbonXUsbTransSetData(trans, 30, data);
    }

    /* Provide retry states */
    {
        CarbonXUsbTransRetry retryStates[] = { { 1, 5 }, { 2, 3 } };
        carbonXUsbTransSetRetryStates(trans, 2, retryStates);
        /* carbonXUsbTransSetStallState(trans, 2); */
    }
}

void deviceSetResponseCb(CarbonXUsbTrans* trans, void* stimulusInstance)
{
    CarbonXUsbID xtor = *((CarbonXUsbID*) stimulusInstance);
    const char *xtorName = carbonXUsbGetName(xtor);
    char *statusStr = "SUCCESS";
    printf("%s[%d]: Transaction received.\n", xtorName, (int) t);
    carbonXUsbTransDump(trans);
    printf("Status responded = %s\n", statusStr);

    if (++transCount % 2 == 0)
        printf("\n");
}

void hostSetResponseCb(CarbonXUsbTrans* trans, void* stimulusInstance)
{
    CarbonXUsbID xtor = *((CarbonXUsbID*) stimulusInstance);
    if (!processConfigTransAtHost(xtor, trans))
    {
        // Receive Interrupt or Isochronous transfer status

        if (++transCount % 2 == 0)
            printf("\n");
    }
}

void hostReportTransactionCb(CarbonXUsbTrans *trans, void *stimulusInstance)
{
    CarbonXUsbID xtor = *((CarbonXUsbID*) stimulusInstance);
    if (processConfigTransAtHost(xtor, trans))
    {
        /* After SetConfiguration, start new transactions */
        if (carbonXUsbTransGetRequestType(trans) == 0 &&
                carbonXUsbTransGetRequest(trans) == 9)
        {
            startTransactions(xtor);
        }
    }
    else
    {
        const char *xtorName = carbonXUsbGetName(xtor);
        CarbonXUsbStatusType status = carbonXUsbTransGetStatus(trans);
        char *statusStr = "SUCCESS";
        if (status == CarbonXUsbStatus_HALT)
            statusStr = "HALT";
        printf("%s[%d]: Transaction complete.\n", xtorName, (int) t);
        carbonXUsbTransDump(trans);
        printf("Status received = %s\n", statusStr);

        if (++transCount % 2 == 0)
            printf("\n");
    }
    carbonXUsbTransDestroy(trans);
}

void hostReportCommandCb(CarbonXUsbCommand command, void *stimulusInstance)
{
    CarbonXUsbID xtor = *((CarbonXUsbID*) stimulusInstance);
    switch (command)
    {
        case CarbonXUsbCommand_DeviceAttach:
        {
            /* Start bus enumeration for newly attached device */
            carbonXUsbStartBusEnumeration(xtor);
            break;
        }
        default: break;
    }
}
