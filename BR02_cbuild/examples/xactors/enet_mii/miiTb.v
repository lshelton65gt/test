//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
`timescale 1ns / 100 ps

`define DWIDTH 3:0

module miiTb;


  wire         sys_clk;           // carbon depositSignal
  wire         sys_reset_l;       // carbon depositSignal

  // MII signals - rx xactor
  wire  [`DWIDTH]  TXD;       // inputs to gmiiRX
  wire             TX_ER;
  wire             TX_EN;
  wire             TX_CLK;

  wire             CRS;       // outputs from gmiiRX
  wire             COL;


  // Primary IO  -- tx xactor  (all OUTPUTS)
  wire             RX_CLK;   // Receive clock - 125MHz
  wire             RX_DV;    // Receive data valid
  wire             RX_ER;    // Receive error
  wire   [`DWIDTH] RXD;      // Receive data


carbonx_enetmii_tx
 xmitter
    (sys_clk,   
     sys_reset_l,
 
     RXD,       // output [3:0]       
     RX_DV,     // output
     RX_ER,     // output
     RX_CLK     // output

   );


enet_xover
 crossover
   (    
     RXD,       
     RX_DV,     
     RX_ER,     
     RX_CLK,    

     TXD,      
     TX_ER, 
     TX_EN, 
     CRS, 
     COL, 
     TX_CLK 
   );


carbonx_enetmii_rx
  receiver
    (sys_clk,   
     sys_reset_l,
     TXD,      
     TX_ER, 
     TX_EN, 
     CRS, 
     COL, 
     TX_CLK 

   );
  
 endmodule   

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 module enet_xover(
     RXD,       
     RX_DV,     
     RX_ER,     
     RX_CLK,    

     TXD,      
     TX_ER, 
     TX_EN, 
     CRS, 
     COL, 
     TX_CLK 
   );

 // MII signals - receiver (rx) xactor
  output  [`DWIDTH]  TXD;       // inputs to gmiiRX
  output             TX_ER;
  output             TX_EN;
  output             TX_CLK;

  input              CRS;       // outputs from gmiiRX
  input              COL;


  // outputs from Transmitter (named RX*) are INPUTS to crossover.
  input             RX_CLK;   // Receive clock - 125MHz
  input             RX_DV;    // Receive data valid
  input             RX_ER;    // Receive error
  input   [`DWIDTH] RXD;      // Receive dat 

  // Transmitter output (named RX*) are INPUTS here.
  // They are output as RX signals (named TX*)
  //  to become INPUTS to Receiver module

  assign  TXD = RXD;     
  assign  TX_ER = RX_ER;
  assign  TX_EN = RX_DV;

  assign  TX_CLK = RX_CLK;


 //  always @(TXD)
 //     $display("@ TXD: %0h", TXD);

endmodule
