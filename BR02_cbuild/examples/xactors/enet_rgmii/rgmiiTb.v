//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
`timescale 1ns / 100 ps

module rgmiiTb (
                sys_clk,        // ports are automatically designated
                sys_reset_l     // Carbon despositsignal
               );

   input          sys_clk;
   input          sys_reset_l;


// include "enet_bfm.vh"
   
   wire           sys_clk; 
   wire           sys_reset_l;          

  // RGMII signals - rx xactor
   wire     [3:0] TXD;       // inputs to rgmiiRX
   wire           TX_ER;
   wire           TX_EN;
   wire           TX_CLK;
   
   wire           CRS;       // outputs from rgmiiRX
   wire           COL;
   


  // Primary IO  -- tx xactor  (all OUTPUTS)
   wire           RX_CLK;   // Receive clock - 125MHz
   wire           RX_DV;    // Receive data valid
   wire           RX_ER;    // Receive error
   wire     [3:0] RXD;      // Receive data



carbonx_enetrgmii_tx
 xmitter
    (sys_clk,   
     sys_reset_l,
 
     RXD,       // output
     RX_CTL,    // output
     RX_CLK     // output

   );


enet_xover
 crossover
   (    
     RXD,       
     RX_CTL,     
     RX_CLK,    
     TXD,      
     TX_CTL, 
     TX_CLK 
   );


carbonx_enetrgmii_rx
  receiver
    (sys_clk,   
     sys_reset_l,
     TXD,      
     TX_CTL, 
     TX_CLK 

   );
  
 endmodule   

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    Acts like a crossover ethernet cable -- connects output to input
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 module enet_xover(
     RXD,       
     RX_CTL,     
     RX_CLK,    
     TXD,      
     TX_CTL, 
     TX_CLK 
   );

 // RGMII signals - receiver (rx) xactor
   output      [3:0]  TXD;       // inputs to rgmiiRX
   output             TX_CTL;
   output             TX_CLK;

  // outputs from Transmitter (named RX*) are INPUTS to crossover.
   input              RX_CLK;   // 
   input              RX_CTL;    // 
   input     [3:0]    RXD;      // 

   reg [3:0]          data_h;
   reg [3:0]          data_l;
   reg                ctl_h;
   reg                ctl_l;

   // Transmitter output (named RX*) are INPUTS here.
   // They are output as RX signals (named TX*)
   //  to become INPUTS to Receiver module

   // We need to switch the edges that the data is driven on to be able
   // to connect the two transactors since the transmitter transactor
   // is driving the data on the other to make up for that we cannot model
   // a delay on the clock. The receiver transactor also samples the data on the other
   // edge for the same reason.
   always @(posedge RX_CLK) begin
      data_l <= RXD;
      ctl_l  <= RX_CTL;
   end
   
   always @(negedge RX_CLK) begin
      data_h <= RXD;
      ctl_h  <= RX_CTL;
   end

   assign  TX_CLK = RX_CLK;
   assign  TXD    = RX_CLK ? data_l : data_h;     
   assign  TX_CTL = RX_CLK ? ctl_l  : ctl_h;
   
endmodule
