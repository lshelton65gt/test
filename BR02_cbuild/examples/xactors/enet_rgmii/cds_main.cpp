
// Ethernet RGMII transactor test

 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 //  Copyright 2005-2007 Carbon Design Systems, Inc.  All Rights Reserved. 
 //  Portions of this software code are licensed to Carbon Design Systems 
 //  and are protected by copyrights of its licensors."
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 /***************************************************************************
  *
  * Description: This file contains the functions required to
  * pass ethernet encapsulated packets from each tx transactor to
  * the rx transactor.
  * This test transmits and receives 10 good ethernet packets and
  * Check the validity of the received packet.
  *
  **************************************************************************/
//
//   include files
//
  #include "libdesign.h"


#include "carbon/c_memmanager.h"
#include "xactors/xactors.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <time.h>   // Only used for debug -- can be removed


// Set to 1 for printing debug info, 0 for no printing
//
#if   1
 #define Dbug(s) printf(s)
#else
 #define Dbug(s) 
#endif

#if 1
 # define ToDo(s) printf(s)
#else
 #define ToDo(s)
#endif


static u_int32 interface = ENET_RGMII_MODE;
int    myERROR = 0;

CarbonUInt32 ONE  = 1;  // need pointers to values
CarbonUInt32 ZERO = 0;  // for deposit function
const int CLOCK_CYCLE = 20; 

// Maximum Packet Size
#define  ENET_MAX_PKT_SIZE 65536

#define TX_PORTS_TO_USE 1
#define RX_PORTS_TO_USE 1

// Total number of packets
#define TX_NUM_PKTS 10

// Transmit packet length
#define TX_PACKET_LEN 64

#define TRANSMITTER "xmitter"
#define RECEIVER "receiver"

#define SIM_MAX_TICKS 10000L

//
// Transactor library routine
//
extern "C" int  tbp_get_error_count(void);
extern "C" void tbp_print_error_status(void);


//
// local functions - in this file
//
static void build_descriptor_list(ENET_PKT_T *pkt_list, u_int32 pkt_count);
static void check_packet(ENET_PKT_T desc_pkt, u_int8 *act_pkt, u_int32 act_size);
static void xmitter_func(ENET_PKT_T *pkt_list, u_int32 size);
static void receiver_func(ENET_PKT_T *pkt_list, u_int32 size);


/*
 *  These are some utility functions
 */

void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time);
void printHeader(char*);
int  passfail(void);


// *************
//     Main   
// *************

int main()
{
    ENET_PKT_T *pkt_list;

    printHeader("rgmii Ethernet testbench starting");

    // Create Sim Infrastructure.  testBench, Clocks, and Reset

    // Handle to Carbon Model
    Dbug("\n============== create carbon testBench  ==============\n");	
    CarbonObjectID* model = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);

    Dbug("=============== Start writing VCD file ===================\n");
    CarbonWaveID* wave = carbonWaveInitVCD(model, "rgmiiTb.vcd", e1ns);
    int wavStat = carbonDumpVars(wave, 6, "rgmiiTb");
    assert(wavStat == eCarbon_OK);

    // Clocks & Reset
    CarbonNetID* clock  = carbonFindNet(model, "rgmiiTb.sys_clk");
    CarbonNetID* resetn = carbonFindNet(model, "rgmiiTb.sys_reset_l");

    // Assert Reset for a few clocks
    //
    assert(resetn != NULL);
    Dbug("============== Asserting Reset ==============\n");
    carbonDeposit(model, resetn, &ZERO, NULL);

    CarbonTime time = 0;
    Dbug("============== Run 100 ticks  ==============\n");
    for (int i=0; i<100; i++) run(model, clock, time);
	   
    Dbug("============== Deasserting Reset ==============\n");
    carbonDeposit(model, resetn, &ONE, NULL);
      
    // --------------------------------------------------------
    // Setting up the command Queues
    // --------------------------------------------------------
    // Call a local (static) function to build a list of
    // descriptors onto the tx_queue of TX transactor
      
    pkt_list = (ENET_PKT_T *)carbonmem_malloc(TX_NUM_PKTS * sizeof(ENET_PKT_T));
    build_descriptor_list(pkt_list, TX_NUM_PKTS);
      
    //
    // Attach transactors to their respective test functions.
    //  Pass the descriptor list to both functions.
       
    MSG_Milestone("carbonXAttach transmitter function\n");
    carbonXAttach(TRANSMITTER,(CarbonXAttachFuncT)xmitter_func,     2, pkt_list, TX_NUM_PKTS);
      
    MSG_Milestone("carbonXAttach receiver function\n");
    carbonXAttach(RECEIVER,    (CarbonXAttachFuncT)receiver_func,    2, pkt_list, TX_NUM_PKTS);
     
    Dbug("=============   run clock until TEST_COMPLETE  ========  \n");

    long numTicks = 0;

    while (    (numTicks < SIM_MAX_TICKS) 
            && (!carbonXCheckSignalState(TEST_COMPLETE))  )
      {
	run(model, clock, time);
	numTicks = time / CLOCK_CYCLE;
      
 	if ((numTicks % 100) == 0)  printf("Main:  cycle %4d\n", numTicks);    
      }

    if (numTicks < SIM_MAX_TICKS)
      Dbug("============= main:  TEST_COMPLETE detected ===============\n");
    else
      MSG_Error(" === Simulation exceeded time limit of %-6d ticks (SIM_MAX_TICKS)\n\n",
	     SIM_MAX_TICKS);  

   
    if(carbonDumpFlush(wave) != eCarbon_OK)    assert(0);
    carbonXIdle(20);
    printf("\n\nmain: Simulation complete at cycle: %d\n", time / CLOCK_CYCLE);
    carbonDestroy(&model);

    exit (passfail());

}    // end of main()



/*
 *  print a header line that includes the time of day
 *
 */
void printHeader(char *strInfo)
{
  time_t      curtime          = time (NULL);          // Get the current time.
  struct tm  *loctime          = localtime (&curtime); // Convert it to local time representation.  
  char       *dtString         = asctime(loctime);     // convert to ascii string
  dtString[strlen(dtString)-1] = '\0';                 // remove the \n at the end of dtString
  printf("======= %s  at %s =========\n\n", strInfo, dtString);
}


//  ******
//    Run       run clock one clock period.
//  ******
void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time)
{
  carbonDeposit(model, clock, &ZERO, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
  carbonDeposit(model, clock, &ONE, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
}

// -------------------

int passfail(void)
{
  myERROR += tbp_get_error_count();
                    printf ("\n\n**********\n");
  if (myERROR == 0) printf     ("** PASS **\n");
  else              printf     ("** FAIL **   error count: %d\n", myERROR);
                    printf     ("**********\n");

  return myERROR;
}                        

//
// Alternate TBP style 
//
// int passfail(void)
// {
//   int errors;
//   tbp_print_error_status();
//   return tbp_get_error_count();
// }




//
//  b u i l d    d e s c r i p t o r
//----------------------------------------------------------------------
// This function builds a list of descriptors
// onto a named queue for use by a transmit function
//----------------------------------------------------------------------
static void build_descriptor_list(ENET_PKT_T *pkt_list, u_int32 pkt_count){
  u_int32 i, j;
  ENET_PKT_T enet;
 
  printf("build_descriptor_list(): creating %d packets\n", pkt_count);

  // Create descriptors, fill in fields and add them to the tx queue
  for(i=0; i<pkt_count; i++) {
   
    // enable the ethernet header and fill in the fields
    enet.sa = 0xdeadfacefeedLL;
    enet.da = 0x112233445566LL;
    enet.typelength = TX_PACKET_LEN +i;
    enet.pl_size    = TX_PACKET_LEN + i;    

    // Disable the VLAN Header
    enet.vlan.enable = 0;
    enet.payload = (u_int8 *)carbonmem_malloc((TX_PACKET_LEN +i));
 
    // There is only a pdu in this example, so just fill in the
    // pdu fields that interest us.
    for (j = 0; j < TX_PACKET_LEN +i; j++) enet.payload[j] = 0x01020304+j;
    enet.ipg = 5;
    pkt_list[i] = enet;

  } //--end of for(i=0; i<pkt_count; i++)
}


static void check_packet(ENET_PKT_T desc_pkt, u_int8 *act_pkt, u_int32 act_size)
{
  u_int8 *exp_pkt;
  u_int32 exp_size;
  u_int32 i;
  BOOL found_error = FALSE;
  u_int32 first_error = 0;
  
  exp_pkt = (u_int8 *)carbonmem_malloc(65536 * sizeof(exp_pkt));
  // Build the expected packet from the Descriptor
  exp_size = carbonXEnetBuildPacket(&desc_pkt, exp_pkt);
  MSG_Printf("Check Packet: Expecting packet size: %d", exp_size);
  if(exp_size != act_size) {
    MSG_Printf("\n");
    MSG_Error("Expected and Actual Packet Size differ. Expected = %d, Actual %d.\n", exp_size, act_size) ;
   }
  else {
    for(i = 0; i < exp_size; i++) {
      if(exp_pkt[i] != act_pkt[i]) {
	found_error = TRUE;
	first_error = i;
	break;
      }
    }
  }
  if(found_error) {
    MSG_Printf("\n");
    MSG_Error("Mismatch found in packet. First error on byte: %d\n",
	      first_error);
    MSG_Printf("Expected Packet.");

    for(i = 0; i < exp_size; i++) {
      if(i%8 == 0) MSG_Printf("\n");
      MSG_Printf("%3x", exp_pkt[i]);
    }
    MSG_Printf("\n");
    MSG_Printf("Actual Packet.");
    for(i = 0; i < act_size; i++) {
      if(i%8 == 0) MSG_Printf("\n");
      MSG_Printf("%3x", act_pkt[i]);
    }
    MSG_Printf("\n");
  }
  else MSG_Printf(" : Passed!\n");
}




/******************** T R A N S M I T T E R *******************************/

static void xmitter_func(ENET_PKT_T *pkt_list, u_int32 size) {
  
  printf(">>>>>>>>>>>>>Starting transmitter_func. pkt_list = 0x%x Size = %d\n", pkt_list, size);

  u_int32 i;
  u_int32 cside_tx_config = 0;  // tx_config : shadow of hdl_register maintained on the c-side

  MSG_Milestone("Starting transmitter_func. pkt_list = 0x%x Size = %d\n", pkt_list, size);

  // --------------------------------------------------------
  // Tx config
  // --------------------------------------------------------
  // Set the interface to RGMII
  cside_tx_config = (cside_tx_config & 0xf);
  
  // Enable CRC/FCS generation by the tx_Xactor
  cside_tx_config = ((cside_tx_config & ~0x8) | ENET_FCS_ENABLE);
  MSG_Milestone("Doing CS Write to Addr: %x, Data: %x\n", ENET_FRTR_CONFIG,
		cside_tx_config);
  carbonXCsWrite(ENET_FRTR_CONFIG, cside_tx_config);
 
  // --------------------------------------------------------
  // Sending Packets
  // --------------------------------------------------------
  for(i = 0; i < size; i++){
    MSG_Milestone("transmitting Packet # %3d\n", i+1);
    carbonXEnetWrite(&pkt_list[i]);
  }
  // Done!
  MSG_Milestone("transmitter_func: end of function\n");

}

  
/****************  R E C E I V E R  ***********************/

static void receiver_func(ENET_PKT_T *pkt_list, u_int32 size) {
  u_int32 i;
  u_int8 curr_pkt[65536];
  u_int32 curr_size;

  // tr_config : shadow of hdl_register maintained on the c-side
  u_int32 cside_rx_config = 0;

  printf("<<<<<<<<<<<<<<<<< Starting Receiver: pkt_list= 0x%x  Size = %d\n", pkt_list, size);
  MSG_Milestone("Starting Receiver func.\n");
  // --------------------------------------------------------
  // Rx config
  // --------------------------------------------------------
  // Set the interface to RGMII
  cside_rx_config = (cside_rx_config & 0xf) | interface;

  Dbug("<<<<<<< receiver: doing carbonXCsWrite(ENET_FRTR_CONFIG, cside_rx_config)\n");  
  // Enable CRC/FCS generation by the rx_Xactor
  cside_rx_config = ((cside_rx_config & ~0x8) | ENET_FCS_ENABLE);
  carbonXCsWrite(ENET_FRTR_CONFIG, cside_rx_config);

  Dbug("<<<<<<< receiver: Check Packets\n");
  // --------------------------------------------------------
  // Receive and Check Packets
  // --------------------------------------------------------
  for(i = 0; i < size; i++) {
    // Get packet from receiver
    MSG_Milestone(" Checking Packet # %3d\n\n", i+1);
    curr_size = carbonXEnetReceivePacket(curr_pkt);
    // Check packet against expected
    check_packet(pkt_list[i], curr_pkt, curr_size);
  }
   
  MSG_Milestone("  All Packets received, signal test complete\n");
  carbonXSetSignal(TEST_COMPLETE);

  MSG_Milestone("receiver_func: end of function\n");  

}

// -- eof --
