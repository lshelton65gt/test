// 1 ATM, 1 PHY with 4 ports, direct status
 
`timescale 1 ns / 1 ps

`define DATAW 16
 
module utopia2Tb;     // Utopia2 - 1 Phy - 4 Ports (u2_1phy_4port_tb)

  wire              reset_n;   // carbon depositSignal

  wire              txclk; 
  wire [`DATAW-1:0] txdat;
  wire              txprty;
  wire              txsoc;
  wire              txenb_n;
	wire [4:0]        txaddr;
  wire [3:0]        txclav;    // txclav[0] = txfull_n
	 
  wire              rxclk; 
  wire [`DATAW-1:0] rxdat;
  wire              rxprty;
  wire              rxsoc;
  wire              rxenb_n;
	wire [4:0]        rxaddr;
  wire [3:0]        rxclav;    // rxclav[0] = rxempty_n

	 
	defparam AtmTx.DATAW = `DATAW;
	defparam AtmRx.DATAW = `DATAW;
	 
	defparam PhyTx0.DATAW = `DATAW;
	defparam PhyRx0.DATAW = `DATAW;
	 
`ifdef NOT_CARBON
   // 16-bit - 50.0 MHz, 8-bit - 33.0 MHz
   parameter freq = (`DATAW == 16) ? 50.0 : 33.3; // MHz
	 defparam  AtmTx.TXCLK_PERIOD = 1000.0/freq;    // ns 
	 defparam  AtmRx.RXCLK_PERIOD = 1000.0/freq;    // ns
`endif	 

   
  carbonx_utopia2_atmtx AtmTx( 
                          .iResetn           ( reset_n ),
                          .oTxClk            ( txclk ),
                          .oTxData           ( txdat ),
                          .oTxPrty           ( txprty ),
                          .oTxSOC            ( txsoc ),
                          .oTxEnbn           ( txenb_n ),
                          .oTxAddr           ( txaddr ),
                          .iTxFulln_TxClav0  ( txclav[0] ),
	                  .iTxClav3_1        ( txclav[3:1] )
                         );

  carbonx_utopia2_phytx PhyTx0( 
                          .iResetn           ( reset_n ),
                          .iTxClk            ( txclk ),
                          .iTxData           ( txdat ),
                          .iTxPrty           ( txprty ),
                          .iTxSOC            ( txsoc ),
                          .iTxEnbn           ( txenb_n ),
                          .iTxAddr           ( txaddr ),
                          .oTxFulln_TxClav0  ( txclav[0] ),
	                  .oTxClav3_1        ( txclav[3:1] )
                         );

  carbonx_utopia2_atmrx AtmRx( 
                          .iResetn           ( reset_n ),
                          .oRxClk            ( rxclk ),
                          .iRxData           ( rxdat ),
                          .iRxPrty           ( rxprty ),
                          .iRxSOC            ( rxsoc ),
                          .oRxEnbn           ( rxenb_n ),
                          .oRxAddr           ( rxaddr ),
                          .iRxEmptyn_RxClav0 ( rxclav[0] ),
	                  .iRxClav3_1        ( rxclav[3:1] )
                         );

  carbonx_utopia2_phyrx PhyRx0( 
                          .iResetn           ( reset_n ),
                          .iRxClk            ( rxclk ),
                          .oRxData           ( rxdat ),
                          .oRxPrty           ( rxprty ),
                          .oRxSOC            ( rxsoc ),
                          .iRxEnbn           ( rxenb_n ),
                          .iRxAddr           ( rxaddr ),
                          .oRxEmptyn_RxClav0 ( rxclav[0] ),
	                  .oRxClav3_1        ( rxclav[3:1] )
                         );

 endmodule // u2_1phy_4port_tb
	 
	 

