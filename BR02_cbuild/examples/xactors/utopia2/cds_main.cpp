
// UTOPIA-2 transactor test

 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 //  Copyright 2005-2007 Carbon Design Systems, Inc.  All Rights Reserved. 
 //  Portions of this software code are licensed to Carbon Design Systems 
 //  and are protected by copyrights of its licensors."
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


//
//   include files
//
#include "libdesign.h"
#include "carbon/c_memmanager.h"
// #include "xactors/xactors.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>



#include <stdlib.h>
#include <string.h>


extern "C" {
 #include "xactors/carbonX.h"
 #include "xactors/utopia2/carbonXUtopia2AtmTx.h"
 #include "xactors/utopia2/carbonXUtopia2PhyTx.h"

 #include "xactors/utopia2/carbonXUtopia2AtmRx.h"  // Currently not used - needed for compile
 #include "xactors/utopia2/carbonXUtopia2PhyRx.h"  // currently not used - needed for compile

}

#define carbonXWaitForSignal(s,n) carbonXSetWaitTest(s,n*1000)

#define TEST_COMPLETE "test_complete"
#define Dbug printf

 #define UTOPIA2_ATMTX   "AtmTx"
 #define UTOPIA2_ATMRX   "AtmRx"
 #define UTOPIA2_PHYTX_0 "PhyTx0"
 #define UTOPIA2_PHYRX_0 "PhyRx0"


typedef struct {
  
  u_int8  port_addr;
  u_int8  *cell_buf;
  
  u_int32 cell_sent;
  u_int32 cell_rcvd;

}  UTOPIA2_TEST_CELL ;


typedef struct {
  u_int32   num_ports;
  u_int32   port_cell_cnt[256];
  u_int32   port_addrs[256];

} UTOPIA2_TEST_PARAMS;



int    myERROR = 0;

CarbonUInt32 ONE  = 1;  // need pointers to values
CarbonUInt32 ZERO = 0;  // for deposit function

const int CLOCK_CYCLE = 20;  // 50 Mhz = 20 ns 

#define SIM_MAX_TICKS 25000L

//
// Transactor library routine
//
extern "C" int  tbp_get_error_count(void);
extern "C" void tbp_print_error_status(void);


//
// local functions - in this file
//

static void transmitter_func(UTOPIA2_TEST_PARAMS *test_parms, UTOPIA2_TEST_CELL **cell_queues);
static void receiver_func(UTOPIA2_TEST_PARAMS *test_parms, UTOPIA2_TEST_CELL **cell_queues);
static UTOPIA2_TEST_CELL ** BuildPortDesc(UTOPIA2_TEST_PARAMS test_parms);

UTOPIA2_TEST_CELL utopia2_test_create_cell(u_int8 port, u_int32 size, u_int8 payload_start_char);
s_int32 utopia2_test_get_port_idx(u_int32 port_addr, u_int32 num_cells, UTOPIA2_TEST_CELL **cell_queues);
s_int32 utopia2_test_check_all_rcvd(UTOPIA2_TEST_PARAMS *tp, UTOPIA2_TEST_CELL **cell_queues);
u_int32 utopia2_test_get_next_port_to_send(u_int32               last_port_idx, 
					   UTOPIA2_TEST_PARAMS * test_parms, 
					   UTOPIA2_TEST_CELL **  cell_queues,
					   u_int32 *             cell_cnt,
					   u_int32 *             all_cells_sent);
u_int32 utopia2_test_get_next_port_to_rcv(u_int32               last_port_idx, 
					  UTOPIA2_TEST_PARAMS * test_parms, 
					  UTOPIA2_TEST_CELL **  cell_queues,
					  u_int32 *             cell_cnt,
					  u_int32 *             all_cells_sent);

/*
 *  These are some utility functions
 */

void run2(CarbonObjectID* model, CarbonNetID* clock1, CarbonNetID* clock2, CarbonTime& time);
void printHeader(char*);
int  passfail(void);


// *************
//     Main   
// *************

int main()
{
    u_int32 i;
    UTOPIA2_TEST_PARAMS *test_parms;
    UTOPIA2_TEST_CELL ** cell_queues;

    printHeader("utopia-2 testbench starting");

    // Create Sim Infrastructure.  testBench, Clocks, and Reset

    // Handle to Carbon Model
    Dbug("\n============== create carbon testBench  ==============\n");	
    CarbonObjectID* model = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);

    Dbug("=============== Start writing VCD file ===================\n");
    CarbonWaveID* wave = carbonWaveInitVCD(model, "utopia2Tb.vcd", e1ns);
    int wavStat = carbonDumpVars(wave, 6, "utopia2Tb");
    assert(wavStat == eCarbon_OK);

    // Clocks & Reset
    CarbonNetID* txclk  = carbonFindNet(model, "utopia2Tb.AtmTx.oTxClk");
    CarbonNetID* rxclk  = carbonFindNet(model, "utopia2Tb.AtmRx.oRxClk");
    assert(txclk != NULL);
    assert(rxclk != NULL);

    CarbonNetID* resetn = carbonFindNet(model, "utopia2Tb.reset_n");
    assert(resetn != NULL);

    // Assert Reset for a few clocks
    //
    Dbug("============== Asserting Reset ==============\n");
    carbonDeposit(model, resetn, &ZERO, NULL);

    CarbonTime time = 0;
    Dbug("============== Run 100 ticks  ==============\n");
    for (int i=0; i<100; i++) run2(model, txclk, rxclk, time);
	   
    Dbug("============== Deasserting Reset ==============\n");
    carbonDeposit(model, resetn, &ONE, NULL);


  // Allocate Memory for the Test Params
    test_parms = (UTOPIA2_TEST_PARAMS*)carbonmem_malloc(sizeof(UTOPIA2_TEST_PARAMS));
    if(test_parms == NULL) MSG_Panic("Failed to allocate memory.\n");

  // Set number of ports in the test
    test_parms->num_ports = 1;

  // Set number of Cells per port
    for(i = 0; i < test_parms->num_ports; i++) {
      test_parms->port_cell_cnt[i] = 50;
    }

  // Set Port Addresses
    for(i = 0; i < test_parms->num_ports; i++)
      test_parms->port_addrs[i] = i;
  
  // Build Cell Queues
    cell_queues = BuildPortDesc(*test_parms);
  
  // Start Transactor Threads
    carbonXAttach(UTOPIA2_ATMTX,   (CarbonXAttachFuncT)transmitter_func, 2, test_parms, cell_queues);
    carbonXAttach(UTOPIA2_PHYTX_0, (CarbonXAttachFuncT)receiver_func,    2, test_parms, cell_queues);


    Dbug("=============   run clock until TEST_COMPLETE  ========  \n");
  
    long numTicks = 0;

    while (    (numTicks < SIM_MAX_TICKS) 
            && (!carbonXCheckSignalState(TEST_COMPLETE))  )
      {
	run2(model, txclk, rxclk, time);
	numTicks = time / CLOCK_CYCLE;
      
 	if ((numTicks % 100) == 0)  printf("Main:  cycle %4d\n", numTicks);    
      }

    if (numTicks < SIM_MAX_TICKS)
      Dbug("============= main:  TEST_COMPLETE detected ===============\n");
    else
      MSG_Error(" === Simulation exceeded time limit of %-6d ticks (SIM_MAX_TICKS)\n\n",
                SIM_MAX_TICKS);  

    assert( carbonDumpFlush(wave) == eCarbon_OK);
    carbonXIdle(20);
    printf("\n\nmain: Simulation complete at cycle: %d\n", time / CLOCK_CYCLE);
    carbonmem_dealloc(test_parms, sizeof(UTOPIA2_TEST_PARAMS));
    carbonDestroy(&model);

    exit (passfail());

}    // end of main()



/*
 *  print a header line that includes the time of day
 *
 */
#include <time.h>   // Only used for printHeader
void printHeader(char *strInfo)
{
  time_t      curtime          = time (NULL);          // Get the current time.
  struct tm  *loctime          = localtime (&curtime); // Convert it to local time representation.  
  char       *dtString         = asctime(loctime);     // convert to ascii string
  dtString[strlen(dtString)-1] = '\0';                 // remove the \n at the end of dtString
  printf("======= %s  at %s =========\n\n", strInfo, dtString);
}


//  ******
//    Run       run clock one clock period.
//  ******
void run2(CarbonObjectID* model, CarbonNetID* clock1, CarbonNetID* clock2, CarbonTime& time)
{
  carbonDeposit(model, clock1, &ZERO, NULL);
  carbonDeposit(model, clock2, &ZERO, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);

  carbonDeposit(model, clock1, &ONE, NULL);
  carbonDeposit(model, clock2, &ONE, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
}

// -------------------

int passfail(void)
{
  myERROR += tbp_get_error_count();
                    printf ("\n\n**********\n");
  if (myERROR == 0) printf     ("** PASS **\n");
  else              printf     ("** FAIL **   error count: %d\n", myERROR);
                    printf     ("**********\n");

  return myERROR;
}                        

//
// Alternate TBP style 
//
// int passfail(void)
// {
//   int errors;
//   tbp_print_error_status();
//   return tbp_get_error_count();
// }

// -----------------------------------------------------------


static void transmitter_func(UTOPIA2_TEST_PARAMS *test_parms, UTOPIA2_TEST_CELL **cell_queues){
  
  u_int32         cell_cnt[31];
  UTOPIA2_TEST_CELL * curr_cell;
  u_int32         port_idx;
  u_int32         all_cells_sent;
  s_int32         cycles;

  // Clean All Cell Counts
  memset(cell_cnt, 0, 31*sizeof(u_int32));
  
  // Wait a few cycles
  carbonXIdle(20);
  MSG_Milestone("Ran %d Idle Cycles.\n", 20);
  
  // Until all cells are sent
  port_idx = 0; all_cells_sent = 0;
  while(!all_cells_sent){
    
    // Get the current cell to send
    curr_cell = &cell_queues[port_idx][cell_cnt[port_idx]];

    // Send Cell
    carbonXUtopia2AtmTxSendCell(curr_cell->port_addr, curr_cell->cell_buf);
    
    // Mark cell as sent and update cell count
    curr_cell->cell_sent = 1;
    cell_cnt[port_idx]++;
    
    // Report Progress for every 5 cells sent
    if( ((cell_cnt[port_idx] % 5) == 0 || cell_cnt[port_idx] == test_parms->port_cell_cnt[port_idx]))
      MSG_Milestone("Port %2d has sent %d total cells.\n", test_parms->port_addrs[port_idx], cell_cnt[port_idx]);

    // Goto the Next Port in Order, and check if there are cells to be sent
    port_idx = utopia2_test_get_next_port_to_send(port_idx, test_parms, cell_queues, cell_cnt, &all_cells_sent);

  }
  
  // Now we wait for all cells to be received by the Sink Transactor
  cycles = 0;
  while(!utopia2_test_check_all_rcvd(test_parms, cell_queues) && cycles < 1000) {
    cycles += 10;
    carbonXIdle(10);
  }
  if(cycles >= 1000) {
    MSG_Error("Timeout occured when waiting for all cells to be received.\n");
  }

  MSG_Milestone("Ran %d Idle Cycles.\n", cycles);
  
  carbonXSetSignal(TEST_COMPLETE);
  MSG_Milestone("%s: Finish Tx \n", __FUNCTION__);
}

static void receiver_func(UTOPIA2_TEST_PARAMS *test_parms, UTOPIA2_TEST_CELL **cell_queues){
  
  u_int8          cell_buf[54];
  u_int32         i;
  u_int32         port;
  u_int32         cell_cnt[256];
  UTOPIA2_TEST_CELL * curr_cell;
  u_int32         port_idx;
  s_int32         fifo_depth[5] = {0,0,0,0,0};

  s_int32         max_fifo_depth[1] = {108};
  s_int32         full_wm[1] = {53};

  MSG_Milestone("%s: Start Rx \n", __FUNCTION__);
  
  // Clean All Cell Counts
  memset(cell_cnt, 0, 54*sizeof(u_int32));
  
  // Setup Port Map
  for(i = 0; i < test_parms->num_ports; i++)
    carbonXUtopia2PhyTxSetPortAddr(i, test_parms->port_addrs[i]);

  // Set initial Port status
  carbonXUtopia2PhyTxSetStatus(0, 1);

  while(1) {
    
    // Get A Cell from the Sink Transactor
    port = carbonXUtopia2PhyTxReceiveCell(cell_buf);

    MSG_Milestone("port = %d\n", port);
    for (i=0; i<carbonXUtopia2PhyTxGetCellSize(); i+=6) 
      MSG_Milestone("data = %x %x %x %x %x %x\n",
		    cell_buf[i],
		    cell_buf[i+1],
		    cell_buf[i+2],
		    cell_buf[i+3],
		    cell_buf[i+4],
		    cell_buf[i+5]);

    // Get the Current cell for this port from the queue
    port_idx = utopia2_test_get_port_idx(port, test_parms->num_ports, cell_queues);

    curr_cell = &cell_queues[port_idx][cell_cnt[port_idx]];
	
    // Check Payload
    for(i = 0; i < carbonXUtopia2PhyTxGetCellSize(); i++) {
      if(cell_buf[i] != curr_cell->cell_buf[i]) {
	MSG_Error("Wrong Data found on Port %d, Cell %d, Byte %d. Expected %x, Actual %x\n",
		  port, cell_cnt[port_idx], i, curr_cell->cell_buf[i], cell_buf[i]);
      }
    }
	
    // Check that our virtual FIFO is not overflowed
    fifo_depth[port] += carbonXUtopia2PhyTxGetCellSize();
    if(fifo_depth[port] > max_fifo_depth[port])
      MSG_Error("Virtual Fifo overflowed for port %d. Max Fifo Depth = %d, Actual depth = %d.\n", 
		port, max_fifo_depth[port], fifo_depth[port]);  
    
    // Read a little bit out of each fifo for each segment read.
    for(i = 0; i < test_parms->num_ports; i++){
      fifo_depth[i] -= carbonXUtopia2PhyTxGetCellSize(); if(fifo_depth[i] < 0) fifo_depth[i] = 0;
      //MSG_Milestone("Fifo Depth for port %d = %d. Cycles = %d\n", i, fifo_depth[i], cycles); 
    }
    
    // Now Update Fifo Status
    for(i = 0; i < test_parms->num_ports; i++) {
      if(fifo_depth[i] > full_wm[i]) {
	carbonXUtopia2PhyTxSetStatus(i, 0);
      }
      else {
	carbonXUtopia2PhyTxSetStatus(i, 1);
      }
    }

    // Update Cell Status
    curr_cell->cell_rcvd = 1;
    cell_cnt[port_idx]++;
    
    // Report Progress for every 5 cells received
    if( ((cell_cnt[port_idx] % 5) == 0 || cell_cnt[port_idx] == test_parms->port_cell_cnt[port_idx]))
      MSG_Milestone("Port %2d has received %d total cells.\n", test_parms->port_addrs[port_idx], cell_cnt[port_idx]);
    
  }

  MSG_Milestone("%s: Finish Rx \n", __FUNCTION__);
}

/*
 * This function builds a list of descriptors 
 * onto a named queue for use by a transmit function 
 */ 
static UTOPIA2_TEST_CELL * build_descriptor_list(u_int32 cell_count, u_int32 port) {
  
  u_int32 i;
  UTOPIA2_TEST_CELL *cell_list;

  // Allocate Space for The Cell list
  cell_list = (UTOPIA2_TEST_CELL *) calloc (cell_count, sizeof(UTOPIA2_TEST_CELL));

  for(i = 0;  i < cell_count; i++) {
    cell_list[i] = utopia2_test_create_cell(port, 100+i, 35+i);
  }
  
  return cell_list;
}

/* Adding Vports and generating desc list */
static UTOPIA2_TEST_CELL ** BuildPortDesc(UTOPIA2_TEST_PARAMS test_parms) {

  u_int32 port;
  UTOPIA2_TEST_CELL **port_list;

  // Allocate Space for The Cell Queues
  port_list = (UTOPIA2_TEST_CELL **) calloc (test_parms.num_ports, sizeof(UTOPIA2_TEST_CELL *));

  // Call a function to build a list of cells for each port
  for (port=0; port < test_parms.num_ports; port++)
    port_list[port] = build_descriptor_list(test_parms.port_cell_cnt[port], port);

  return port_list;
}


// -----------------------------------------------------------



UTOPIA2_TEST_CELL utopia2_test_create_cell(u_int8 port, u_int32 size, u_int8 payload_start_char) {

  UTOPIA2_TEST_CELL cells;
  u_int8 *      cell_ptr;
  u_int32       i;

  // Allocate Memory for the Packet
  if( NULL == (cell_ptr = (u_int8*)carbonmem_malloc(size *sizeof(u_int8) ))) {
    MSG_Panic("Malloc failed when trying to allocated memory for packet data.\n");
  }
  
  // Fill Packet Data with Incrementing Bytes
  for(i = 0; i < size; i++) {
    cell_ptr[i] = payload_start_char + i;
  }
  
  // Fill Out Structure
  cells.cell_buf      = cell_ptr;
  cells.port_addr     = port;
  cells.cell_sent     = 0;
  cells.cell_rcvd     = 0;

  return cells;
}

s_int32 utopia2_test_get_port_idx(u_int32 port_addr, u_int32 num_cells, UTOPIA2_TEST_CELL **cell_queues) {
  
  s_int32 i;

  for(i = 0; i < num_cells; i++) {
    if(cell_queues[i]->port_addr == port_addr)
      return i;
  }

  return -1;

}

s_int32 utopia2_test_check_all_rcvd(UTOPIA2_TEST_PARAMS *tp, UTOPIA2_TEST_CELL **cell_queues) {
  
  u_int32        port_idx, cells;
  UTOPIA2_TEST_CELL *curr_cell;

  for(port_idx = 0 ; port_idx < tp->num_ports; port_idx++) {
    for(cells = 0; cells < tp->port_cell_cnt[port_idx]; cells++){
      curr_cell = &cell_queues[port_idx][cells];
      if(curr_cell->cell_rcvd == 0)
	return 0;
    }
  }
  
  // If we get here means that all packets were received
  return 1;
}

u_int32 utopia2_test_get_next_port_to_send(u_int32            last_port_idx, 
					UTOPIA2_TEST_PARAMS * test_parms, 
					UTOPIA2_TEST_CELL **   cell_queues,
					u_int32 *             cell_cnt,
					u_int32 *             all_cells_sent) {
  
  u_int32  cell_avail, credits_avail;
  u_int32  port_idx;
  
  // Goto the Next Port in Order, and check if there are packets to be sent
  port_idx       = last_port_idx;
  *all_cells_sent = 1;
  cell_avail      = 0;
  credits_avail  = 0;
  while ((credits_avail == 0) || (cell_avail == 0)) {
    do {
      cell_avail     = 0;
      credits_avail     = 0;
      port_idx++;
      if(port_idx >= test_parms->num_ports) port_idx = 0;
      
      // Check if there are any packets to send on this port
      if(cell_cnt[port_idx] < test_parms->port_cell_cnt[port_idx]) {
	cell_avail = 1;
	*all_cells_sent = 0;
      }
      if (carbonXUtopia2AtmTxGetStatus(test_parms->port_addrs[port_idx])) {
	credits_avail = 1;
      }
    } while((port_idx != last_port_idx) && 
	    (!cell_avail || !credits_avail));
    
    // If we went though all ports without finding any data to send, we're done
    if ((port_idx == last_port_idx) && 
	!*all_cells_sent &&
	!(cell_avail && credits_avail)) {
      carbonXIdle(10);
    }
    if (*all_cells_sent == 1) break;
  }
  
  return port_idx;
}

  
u_int32 utopia2_test_get_next_port_to_rcv(u_int32            last_port_idx, 
					  UTOPIA2_TEST_PARAMS * test_parms, 
					  UTOPIA2_TEST_CELL **   cell_queues,
					  u_int32 *             cell_cnt,
					  u_int32 *             all_cells_rcvd) {
  
  u_int32  cell_avail, credits_avail;
  u_int32  port_idx;
  u_int32  i;

  // Check if we have data that is not received
  *all_cells_rcvd = 1;
  for(i = 0; i < test_parms->num_ports; i++) {
    if(cell_cnt[i] < test_parms->port_cell_cnt[i])
      *all_cells_rcvd = 0;
  }
  if(*all_cells_rcvd == 1) {
    port_idx = last_port_idx;
    MSG_Milestone("All Cells received.\n");
  }

  else {

    // Goto the Next Port in Order, and check if there are packets to be sent
    port_idx       = last_port_idx;
    cell_avail     = 0;
    while (cell_avail == 0) {

      do {
	cell_avail     = 0;
	credits_avail     = 0;
	port_idx++;

	if(port_idx >= test_parms->num_ports) port_idx = 0;
	
	//MSG_Milestone("Checking Avalable data on port %d... %s.\n", test_parms->port_addrs[port_idx], 
	//      carbonXUtopia2AtmRxGetStatus(test_parms->port_addrs[port_idx]) ? "Yes" : "No");
 
	if(carbonXUtopia2AtmRxGetStatus(test_parms->port_addrs[port_idx]))
	  cell_avail = 1;

      } while( (port_idx != last_port_idx) && !cell_avail);
      
      // If we went though all ports without finding a port with available data,
      // wait for a while and try again
      if ( (port_idx == last_port_idx) && !cell_avail) {
	MSG_Milestone("Waiting for data to be available.\n");
	carbonXIdle(10);
      }
    }
  }

  return port_idx;
}



// -- eof --
