// SPI-4.2 transactor test

 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 //  Copyright 2005-2007 Carbon Design Systems, Inc.  All Rights Reserved. 
 //  Portions of this software code are licensed to Carbon Design Systems 
 //  and are protected by copyrights of its licensors."
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#include "libdesign.h"
#include "xactors/xactors.h"
#include "carbon/c_memmanager.h"

#include <stdlib.h>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>



// #define MSG_Debug MSG_Printf

#define DISPLAYINITIALPACKETS FALSE

#define SPI4_SRC   "pl4tx0"
#define SPI4_SNK   "pl4rx0"

#define TEST_COMPLETE "test_complete"
#define SIM_MAX_TICKS 10000L
#define FSDB FALSE


//
// Transactor library routine
//
extern "C" int  tbp_get_error_count(void);
extern "C" void tbp_print_error_status(void);

//
// local functions - in this file
//

static void transmitter_func();
static void receiver_func();
static void spi4_simple_test(void);

/*
 *  These are some utility functions
 */
void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time);
void run2(CarbonObjectID* model, CarbonNetID* clock, CarbonNetID* clock2, int phaseDiff, CarbonTime& time);
void run2freq(CarbonObjectID* model, CarbonNetID* clock, CarbonNetID* clock2, int freqDiff, CarbonTime& time);
void printHeader(char*);
int  passfail(void);


//  Global Data

int    myERROR = 0;

CarbonUInt32 ONE  = 1;  // need pointers to values
CarbonUInt32 ZERO = 0;  // for deposit function
//
// default clock cycle values for SPI-4 are:
// dclock: 0.8 ns (625Mhz)   sclock = 8 * dclock: 6.4 ns (78.1 Mhz)
//
const int CLOCK_CYCLE = 8;  // == 0.8 ns (by definition a unit of 1 is 0.1 ns)



// *************
//     Main   
// *************

int main()
{
    printHeader("SPI-4.2 testbench starting");

    // Create Sim Infrastructure.  testBench, Clocks, and Reset

    // Handle to Carbon Model
    MSG_Debug("\n============== create carbon testBench  ==============\n");	
    CarbonObjectID* model = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);

    CarbonWaveID* wave;
 
    if (!FSDB) {
        MSG_Debug("=============== Start VCD dumpfile ===================\n");
        wave = carbonWaveInitVCD(model, "spi4.vcd", e1ns);
       
    } else {
        MSG_Debug("=============== Start FSDB dumpfile ===================\n");
        wave = carbonWaveInitFSDB(model, "spi4.fsdb", e1ns);
    }

    CarbonStatus wavStat = carbonDumpVars(wave, 6, "spi4Tb");
    assert(wavStat == eCarbon_OK);


    // Clocks & Reset
    CarbonNetID* dclock  = carbonFindNet(model, "spi4Tb.pl4tx0.dclk");
    assert(dclock);

    CarbonNetID* sclock  = carbonFindNet(model, "spi4Tb.pl4rx0.sclk");
    assert(sclock);

    //  initialize clocks to 0. Set clock Frequency ratio
    //
    // dclk runs 8 times faster than sclk, or same speed as sclk
    //  default values:    dclk = 6.4 ns   sclk = 0.8 ns (625 Mhz)
    //
    int dsclkRatio = 8;  // 8 : 1 frequency ratio 
    carbonDeposit(model, dclock, &ZERO, NULL);
    carbonDeposit(model, sclock, &ZERO, NULL); 

    CarbonNetID* resetn = carbonFindNet(model, "spi4Tb.reset_n");
    assert(resetn);

    // Assert Reset for a few clocks
    //
    MSG_Debug("============== Asserting Reset ==============\n");
    carbonDeposit(model, resetn, &ZERO, NULL);

    CarbonTime time = 0;
    MSG_Debug("============== Run 32 dclocks (4 sclocks)  ==============\n");
    for (int i=0; i<32; i++) run2freq(model, dclock, sclock, dsclkRatio, time);
	   
    MSG_Debug("============== Deasserting Reset ==============\n");
    carbonDeposit(model, resetn, &ONE, NULL);
      
    //
    // Attach transactors to their respective test functions.
    //  
    spi4_simple_test();

    MSG_Debug("=============   run clock until TEST_COMPLETE  ========  \n");

    long numTicks = 0;

    while (    (numTicks < SIM_MAX_TICKS) 
            && (!carbonXCheckSignalState(TEST_COMPLETE))  )
      {
	run2freq(model, dclock, sclock, dsclkRatio, time);
	numTicks = time / CLOCK_CYCLE;
      
 	if (    ((numTicks % 100) == 0)
             || (numTicks < 100) 
           )
          {
             printf("Main:  cycle %4d\n", numTicks);    
             carbonDumpFlush(wave);
          }
      }

    if (numTicks < SIM_MAX_TICKS)
      MSG_Debug("============= main:  TEST_COMPLETE detected ===============\n");
    else {
      // error count incremented by MSG_Error. Available through tbp_get_error_count()
      MSG_Error(" === Simulation exceeded time limit of %-6d ticks (SIM_MAX_TICKS)\n\n",
	     SIM_MAX_TICKS);  
    }
    if(carbonDumpFlush(wave) != eCarbon_OK)    assert(0);
    carbonXIdle(20);
    printf("\n\nmain: Simulation complete at cycle: %d\n", time / CLOCK_CYCLE);
    passfail();
    carbonDestroy(&model);

}    // end of main()



/*
 *  print a header line that includes the time of day
 *
 */

#include <time.h>   // time.h is only used in printHeader

void printHeader(char *strInfo)
{
  time_t      curtime          = time (NULL);          // Get the current time.
  struct tm  *loctime          = localtime (&curtime); // Convert it to local time representation.  
  char       *dtString         = asctime(loctime);     // convert to ascii string
  dtString[strlen(dtString)-1] = '\0';                 // remove the \n at the end of dtString
  printf("======= %s  at %s =========\n\n", strInfo, dtString);
}


//  ******
//    Run       run clock one clock period.
//  ******
//
// CarbonNetID* clock1, clock2  // CarbonFindNet( "verilog clock signal names")
// int phaseDiff                // Phase difference between clock1 and clock2
// CarbonTime&                  // Simulation time variable
//

void run(CarbonObjectID* model, CarbonNetID* clock1, CarbonTime& time)
{
  time += (CLOCK_CYCLE / 2);
  carbonDeposit(model, clock1, &ZERO, NULL);
  carbonSchedule(model, time);

  time += (CLOCK_CYCLE / 2);
  carbonDeposit(model, clock1, &ONE, NULL);
  carbonSchedule(model, time);
}

//  ******
//    Run2       run two clocks one clock period. 
//  ******                                        
//  
//   A phase difference between 0 and CLOCK_CYCLE may be specified 
//
// CarbonNetID* clock1, clock2  // CarbonFindNet( "verilog clock signal names")
// int phaseDiff                // Phase difference between clock1 and clock2
// CarbonTime&                  // Simulation time variable
//

void run2(CarbonObjectID* model, 
         CarbonNetID* clock1, CarbonNetID* clock2, int phaseDiff, CarbonTime& time)
{
 if (phaseDiff >= CLOCK_CYCLE) phaseDiff = phaseDiff % CLOCK_CYCLE;

 if (phaseDiff == 0)
   {
     time += (CLOCK_CYCLE / 2);
     carbonDeposit(model, clock1, &ZERO, NULL);
     carbonDeposit(model, clock2, &ZERO, NULL);
     carbonSchedule(model, time);
     
     time += (CLOCK_CYCLE / 2);
     carbonDeposit(model, clock1, &ONE, NULL);
     carbonDeposit(model, clock2, &ONE, NULL);
     carbonSchedule(model, time);
   }
 else
   {
     int halfPD = phaseDiff / 2;

     time += (CLOCK_CYCLE / 2) - halfPD;
     carbonDeposit(model, clock1, &ZERO, NULL);
     carbonSchedule(model, time);
     
     time += halfPD;
     carbonDeposit(model, clock2, &ZERO, NULL);
     carbonSchedule(model, time);
     
     time += (CLOCK_CYCLE / 2) - halfPD;
     carbonDeposit(model, clock1, &ONE, NULL);
     carbonSchedule(model, time);
     
     time += halfPD;
     carbonDeposit(model, clock2, &ONE, NULL);
     carbonSchedule(model, time);
   }
}

// ********
// run2freq 
// ********
//
//  run 2 clocks with one clock being a frequency multiple of the other
//
//  (NOTE: A frequency multiple of 0 is nonsense, so is treated the same as
//   a multiple of 1 - ie clocks run the same frequency).
//
//   fastClock runs <freqMult> times faster than slowClock
//

CarbonUInt32 fastClkCt = 0;
// CarbonUInt32 fastClkState = 0;
CarbonUInt32 slowClkCt = 0;


void run2freq(CarbonObjectID* model, 
         CarbonNetID* fastClock, CarbonNetID* slowClock, int freqMult, CarbonTime& time)
{
  CarbonUInt32 slowClkState;

  if (freqMult <= 1) return( run2(model, fastClock, slowClock, 0, time));

  fastClkCt++;
  int halfPer   = freqMult / 2;
  bool evenFreq = ((freqMult & 0x00000001) == 0);

  // Always run a complete fastClock cycle.
  // foreach tick of fastClock, decide if it is time to transition slowClock
  //

  //  first "half" of slow clock period   both clocks transition  
  if (fastClkCt % freqMult == 0)               
    {                                       
      slowClkState = ++slowClkCt & 0x00000001;

      time += (CLOCK_CYCLE / 2);
      carbonDeposit(model, fastClock, &ZERO, NULL);
      if (!evenFreq)
          carbonDeposit(model, slowClock, &slowClkState, NULL);
      carbonSchedule(model, time);
     
      time += (CLOCK_CYCLE / 2);
      carbonDeposit(model, fastClock, &ONE, NULL);
      if (evenFreq)
         carbonDeposit(model, slowClock, &slowClkState, NULL);
      carbonSchedule(model, time);
    }
  else if (fastClkCt % freqMult == halfPer)               // 2nd half of slower clock period
    {                                                  // 
      slowClkState = ++slowClkCt & 0x00000001;

      time += (CLOCK_CYCLE / 2);
      carbonDeposit(model, fastClock, &ZERO, NULL);
      carbonSchedule(model, time);
     
      time += (CLOCK_CYCLE / 2);
      carbonDeposit(model, fastClock, &ONE, NULL);
      carbonDeposit(model, slowClock, &slowClkState, NULL);
      carbonSchedule(model, time);
    }
  else  
    run(model, fastClock, time);

  // NOTE: The above if..else statement needs to be written like this
  //       to account for the case of freqMult being an ODD value


}  // end run2freq


// -------------------

int passfail(void)
{
  myERROR += tbp_get_error_count();
                    printf ("\n\n**********\n");
  if (myERROR == 0) printf     ("** PASS **\n");
  else              printf     ("** FAIL **   error count: %d\n", myERROR);
                    printf     ("**********\n");

  return myERROR;
}                        

//
// Alternate TBP style 
//
// int passfail(void)
// {
//   int errors;
//   tbp_print_error_status();
//   return tbp_get_error_count();
// }


// -------------------
void   spi4_simple_test() {
// -------------------

  MSG_Milestone("%s: attaching transactor %s -> transmitter_func \n", __FUNCTION__,SPI4_SRC);
  carbonXAttach(SPI4_SRC, transmitter_func, 0);
  MSG_Milestone("%s: attaching transactor %s -> receiver_func \n", __FUNCTION__,SPI4_SNK);
  carbonXAttach(SPI4_SNK, receiver_func,    0);
}


// ------------------------
//  t r a n s m i t t  e r
// ------------------------
static void transmitter_func(){
  
  s_int32 i;
  u_int8 pkt [3000];
  u_int8 * pktPtr = pkt;
  s_int32 sentBytes;
  s_int32 pktSize;

  MSG_Milestone("%s: Config Tx \n", __FUNCTION__);
  // Configure Transactor

  carbonXSpi4SrcStartup(16,        // calendar length
		  1,         // calendar m
		500,       // data_max_t
		  1,         // alpha
		  5,         // num valid dip-2 to sync with rx
		  1,         // num invalid dip-2 to lose sync with rx
		  0);        // fifo_max_t

  for (i=0; i<16; i++) {
    carbonXSpi4SrcSetCalPortMap(i, 255);
    carbonXSpi4SrcSetCalMaxB1(255, 15);
    carbonXSpi4SrcSetCalMaxB2(255, 5);
  }

  // Wait a few cycles
  MSG_Milestone("Ran %d Idle Cycles.\n", carbonXSpi4SrcIdle(20));

  for (i=0; i<3000; i++) pkt[i] = i;

  pktSize = 310;

  sentBytes = carbonXSpi4SrcPkt(pktPtr, pktSize, 255, 1);
  MSG_Milestone("Sent %d bytes.\n", sentBytes);

  pktPtr  += sentBytes;
  pktSize -= sentBytes;

  sentBytes = carbonXSpi4SrcPkt(pktPtr, pktSize, 255, 0);
  MSG_Milestone("Sent %d bytes.\n", sentBytes);

  MSG_Milestone("Ran %d Idle Cycles.\n", carbonXSpi4SrcIdle(200));

  carbonXSetSignal(TEST_COMPLETE);
  MSG_Milestone("%s: Finish Tx \n", __FUNCTION__);
}


// ----------------
//  r e c e i v e r
// ----------------

static void receiver_func(){
  
  u_int16 data_buf[1000];
  u_int8 pkt_buf[1000];
  s_int32 size, sop, eop, port, cycles;
  u_int32 i;
  u_int16 pre_cw, post_cw;
  u_int32 ctrl_cnt;

  MSG_Milestone("%s: Start Rx \n", __FUNCTION__);

  carbonXSpi4SnkStartup( 16,     // calendar length
		    1,      // calendar m
		  500,    // data_max_t
		    1,      // alpha
		   10,     // num of valid training to sync with tx
		    1);     // num of invalid dip-4 to lose sync with tx


  for (i=0; i<16; i++) {
    carbonXSpi4SnkSetCalPortMap(i, 255);
  }
  carbonXSpi4SnkSetCalPortState(255, 0);
  carbonXSpi4SnkSetCal();

  while(1) {
    
    size = 1000;
    cycles = carbonXSpi4SnkPkt( pkt_buf,
			  &size,
			  &sop,
			  &eop,
			  &port );
    
    MSG_Milestone("RX Found a packet, size=%d, sop=%d, eop=%d, port=%d\n",
		  size, sop, eop, port);
    for (i=0; i<size; i++) {
      MSG_Milestone("PacketData[%d] = %x\n",i, pkt_buf[i]);
    }
  }

  MSG_Milestone("%s: Finish Rx \n", __FUNCTION__);
}

// -- eof --
