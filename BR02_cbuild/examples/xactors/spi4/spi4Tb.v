
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


/**********************************************************
 ** SPI4/5 TX/RX transactor Testbench
 **
 ** Simply hook the rx and tx together, and let C side verify data flow
 **********************************************************/

`timescale 1 ps / 1 ps

`define LVDS_MODE 1

module spi4Tb;
   // Local wires and registers  -- Carbon will control reset from cds_main.cpp

   wire        reset_n;  // carbon depositSignal

   wire        tdclk, rdclk;
   wire        tsclk, rsclk;
   wire        tctl,  rctl;
   wire [15:0] tdat,  rdat;
   wire [1:0]  tstat, rstat;
	 

   defparam  pl4tx0.lvds = `LVDS_MODE;
   defparam  pl4rx0.lvds = `LVDS_MODE;

	// Clock generators
	// internal dclk   is 2X tdclk
	// internal sclk   is dclk or dclk/8
	// in LVDS mode:  tdclk = dclk/2; rsclk = sclk/2 = tdclk
	// in LVTTL mode: tdclk = dclk/2; rsclk = sclk   = tdclk/4

//
// Carbon controls dclk & sclk with deposits from module cds_main.cpp  
//
// if NOT a Carbon simulation, then define the following parameters
//
`ifdef NOT_CARBON
         	
                // dclk is 625.0 MHz, tdclk is 312.5 MHz
	 parameter dfreq        = 625.0; // MHz 
	 parameter sfreq        = (`LVDS_MODE == 1) ? dfreq : dfreq / 8.0; // MHz 
	 
	 defparam  pl4tx0.DFREQ = dfreq;
	 defparam  pl4rx0.SFREQ = sfreq;
`endif	 
	 	
  // instance of the posphy_tx xactor
  carbonx_spi4_src pl4tx0 (                        // dclk  internal, 625 MHz
		    .reset_n   (reset_n),    // input
		    .sclk_in   (tsclk),      // input
		    .stat_in   (tstat),      // input
		    .dclk_out  (tdclk),      // output,   generated from dclk, 312.5 MHz
		    .dat_out   (tdat),       // output
		    .ctl_out   (tctl)        // output
		    );
 
  // instance of the posphy_rx xactor
  carbonx_spi4_snk pl4rx0 (
		                             // sclk  internal, 625 or 625/8 = 78.125 MHz
		    .reset_n   (reset_n),    // input
		    .dclk_in   (rdclk),      // input
		    .dat_in    (rdat),       // input
		    .ctl_in    (rctl),       // input
		    .sclk_out  (rsclk),      // output,   generated from sclk, 312.5 MHz or 312.5/8 = 39.0625 MHz
		    .stat_out  (rstat)       // output
		    );

  spi4_loopback dut (
	      .tdclk     (tdclk),      // in:  tx data clock 
	      .tdat      (tdat),       // in:  tx data bus
	      .tctl      (tctl),       // in:  tx control signal
	      .tsclk     (tsclk),      // out
	      .tstat     (tstat),      // out
 
	      .rsclk     (rsclk),      // in
	      .rstat     (rstat),      // in
	      .rdclk     (rdclk),      // out: rx data clock
	      .rdat      (rdat),       // out: rx data bus
	      .rctl      (rctl)        // out: rx control signal
        );
 
endmodule // spi4Tb


/**************************************
 *  spi4  L O O P B A C K   device    *
 **************************************/

module spi4_loopback 
  (
   tdclk,            // tx data clock 
   tdat,             // tx data bus
   tctl,             // tx control signal
   tsclk,
   tstat,
 
   rsclk,
   rstat,
   rdclk,            // rx data clock
   rdat,             // rx data bus
   rctl              // rx control signal
   );
 
  input          	tdclk;       // tx data clock
  input          	tctl;        // tx control bit
  input  [15:0]  	tdat;        // tx data bus

  output         tsclk;       // tx status clock
  output [1:0]   tstat;       // tx status bits
         
  input         	rsclk;       // rx status clock
  input  [1:0]   	rstat;       // rx status bits
         
  output         rdclk;       // rx data clock
  output         rctl;        // rx control bit
  output [15:0]  rdat;        // rx data bus

        // loopback INs to OUTs
         
  assign rdclk  = tdclk;
  assign rctl   = tctl;
  assign rdat   = tdat;
         
  assign  tsclk = rsclk;
  assign  tstat = rstat;
         
         
endmodule // spi4_loopback

//  --- end of file: spi4Tb.v -----

