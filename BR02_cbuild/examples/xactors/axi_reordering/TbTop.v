/**** VHM Glue Logic connecting master and slave Transactors ****/
`ifndef DATA_BUS_WIDTH
`define DATA_BUS_WIDTH 32
`endif
`define STRB_BUS_WIDTH (`DATA_BUS_WIDTH / 8 + (`DATA_BUS_WIDTH % 8 != 0))
`define ID_BUS_WIDTH 4
module TbTop(SystemClock,
        /* Master Ports */
        /* Write Address Channel */
        MASTER_AWID,
        MASTER_AWADDR,
        MASTER_AWLEN,
        MASTER_AWSIZE,
        MASTER_AWBURST,
        MASTER_AWLOCK,
        MASTER_AWCACHE,
        MASTER_AWPROT,
        MASTER_AWVALID,
        MASTER_AWREADY,
        /* Write Data Channel */
        MASTER_WID,
        MASTER_WDATA,
        MASTER_WSTRB,
        MASTER_WLAST,
        MASTER_WVALID,
        MASTER_WREADY,
        /* Write Response Channel */
        MASTER_BID,
        MASTER_BRESP,
        MASTER_BVALID,
        MASTER_BREADY,
        /* Read Address Channel */
        MASTER_ARID,
        MASTER_ARADDR,
        MASTER_ARLEN,
        MASTER_ARSIZE,
        MASTER_ARBURST,
        MASTER_ARLOCK,
        MASTER_ARCACHE,
        MASTER_ARPROT,
        MASTER_ARVALID,
        MASTER_ARREADY,
        /* Read Data Channel */
        MASTER_RID,
        MASTER_RDATA,
        MASTER_RRESP,
        MASTER_RLAST,
        MASTER_RVALID,
        MASTER_RREADY,

        /* Slave Connections */
        /* Write Address Channel */
        SLAVE_AWID,
        SLAVE_AWADDR,
        SLAVE_AWLEN,
        SLAVE_AWSIZE,
        SLAVE_AWBURST,
        SLAVE_AWLOCK,
        SLAVE_AWCACHE,
        SLAVE_AWPROT,
        SLAVE_AWVALID,
        SLAVE_AWREADY,
        /* Write Data Channel */
        SLAVE_WID,
        SLAVE_WDATA,
        SLAVE_WSTRB,
        SLAVE_WLAST,
        SLAVE_WVALID,
        SLAVE_WREADY,
        /* Write Response Channel */
        SLAVE_BID,
        SLAVE_BRESP,
        SLAVE_BVALID,
        SLAVE_BREADY,
        /* Read Address Channel */
        SLAVE_ARID,
        SLAVE_ARADDR,
        SLAVE_ARLEN,
        SLAVE_ARSIZE,
        SLAVE_ARBURST,
        SLAVE_ARLOCK,
        SLAVE_ARCACHE,
        SLAVE_ARPROT,
        SLAVE_ARVALID,
        SLAVE_ARREADY,
        /* Read Data Channel */
        SLAVE_RID,
        SLAVE_RDATA,
        SLAVE_RRESP,
        SLAVE_RLAST,
        SLAVE_RVALID,
        SLAVE_RREADY
        );
input SystemClock;
/* Master ports*/
/* Write Address Channel */
input  [`ID_BUS_WIDTH - 1 : 0] MASTER_AWID;
input  [31 : 0] MASTER_AWADDR;
input  [3 : 0] MASTER_AWLEN;
input  [2 : 0] MASTER_AWSIZE;
input  [1 : 0] MASTER_AWBURST;
input  [1 : 0] MASTER_AWLOCK;
input  [3 : 0] MASTER_AWCACHE;
input  [2 : 0] MASTER_AWPROT;
input  MASTER_AWVALID;
output MASTER_AWREADY;
/* Write Data Channel */
input  [`ID_BUS_WIDTH - 1 : 0] MASTER_WID;
input  [`DATA_BUS_WIDTH - 1 : 0] MASTER_WDATA;
input  [`STRB_BUS_WIDTH - 1 : 0] MASTER_WSTRB;
input  MASTER_WLAST;
input  MASTER_WVALID;
output MASTER_WREADY;
/* Write Response Channel */
output [`ID_BUS_WIDTH - 1 : 0] MASTER_BID;
output [1 : 0] MASTER_BRESP;
output MASTER_BVALID;
input  MASTER_BREADY;
/* Read Address Channel */
input  [`ID_BUS_WIDTH - 1 : 0] MASTER_ARID;
input  [31 : 0] MASTER_ARADDR;
input  [3 : 0] MASTER_ARLEN;
input  [2 : 0] MASTER_ARSIZE;
input  [1 : 0] MASTER_ARBURST;
input  [1 : 0] MASTER_ARLOCK;
input  [3 : 0] MASTER_ARCACHE;
input  [2 : 0] MASTER_ARPROT;
input  MASTER_ARVALID;
output MASTER_ARREADY;
/* Read Data Channel */
output [`ID_BUS_WIDTH - 1 : 0] MASTER_RID;
output [`DATA_BUS_WIDTH - 1 : 0] MASTER_RDATA;
output [1 : 0] MASTER_RRESP;
output MASTER_RLAST;
output MASTER_RVALID;
input  MASTER_RREADY;

/* Slave Ports */
/* Write Address Channel */
output [`ID_BUS_WIDTH - 1 : 0] SLAVE_AWID;
output [31 : 0] SLAVE_AWADDR;
output [3 : 0] SLAVE_AWLEN;
output [2 : 0] SLAVE_AWSIZE;
output [1 : 0] SLAVE_AWBURST;
output [1 : 0] SLAVE_AWLOCK;
output [3 : 0] SLAVE_AWCACHE;
output [2 : 0] SLAVE_AWPROT;
output SLAVE_AWVALID;
input  SLAVE_AWREADY;
/* Write Data Channel */
output [`ID_BUS_WIDTH - 1 : 0] SLAVE_WID;
output [`DATA_BUS_WIDTH - 1 : 0] SLAVE_WDATA;
output [`STRB_BUS_WIDTH - 1 : 0] SLAVE_WSTRB;
output SLAVE_WLAST;
output SLAVE_WVALID;
input  SLAVE_WREADY;
/* Write Response Channel */
input  [`ID_BUS_WIDTH - 1 : 0] SLAVE_BID;
input  [1 : 0] SLAVE_BRESP;
input  SLAVE_BVALID;
output SLAVE_BREADY;
/* Read Address Channel */
output [`ID_BUS_WIDTH - 1 : 0] SLAVE_ARID;
output [31 : 0] SLAVE_ARADDR;
output [3 : 0] SLAVE_ARLEN;
output [2 : 0] SLAVE_ARSIZE;
output [1 : 0] SLAVE_ARBURST;
output [1 : 0] SLAVE_ARLOCK;
output [3 : 0] SLAVE_ARCACHE;
output [2 : 0] SLAVE_ARPROT;
output SLAVE_ARVALID;
input  SLAVE_ARREADY;
/* Read Data Channel */
input  [`ID_BUS_WIDTH - 1 : 0] SLAVE_RID;
input  [`DATA_BUS_WIDTH - 1 : 0] SLAVE_RDATA;
input  [1 : 0] SLAVE_RRESP;
input  SLAVE_RLAST;
input  SLAVE_RVALID;
output SLAVE_RREADY;

/* Xtor connectivities */
/* Write Address Channel */
assign SLAVE_AWID = MASTER_AWID;
assign SLAVE_AWADDR = MASTER_AWADDR;
assign SLAVE_AWLEN = MASTER_AWLEN;
assign SLAVE_AWSIZE = MASTER_AWSIZE;
assign SLAVE_AWBURST = MASTER_AWBURST;
assign SLAVE_AWLOCK = MASTER_AWLOCK;
assign SLAVE_AWCACHE = MASTER_AWCACHE;
assign SLAVE_AWPROT = MASTER_AWPROT;
assign SLAVE_AWVALID = MASTER_AWVALID;
assign MASTER_AWREADY = SLAVE_AWREADY;
/* Write Data Channel */
assign SLAVE_WID = MASTER_WID;
assign SLAVE_WDATA = MASTER_WDATA;
assign SLAVE_WSTRB = MASTER_WSTRB;
assign SLAVE_WLAST = MASTER_WLAST;
assign SLAVE_WVALID = MASTER_WVALID;
assign MASTER_WREADY = SLAVE_WREADY;
/* Write Response Channel */
assign MASTER_BID = SLAVE_BID;
assign MASTER_BRESP = SLAVE_BRESP;
assign MASTER_BVALID = SLAVE_BVALID;
assign SLAVE_BREADY = MASTER_BREADY;
/* Read Address Channel */
assign SLAVE_ARID = MASTER_ARID;
assign SLAVE_ARADDR = MASTER_ARADDR;
assign SLAVE_ARLEN = MASTER_ARLEN;
assign SLAVE_ARSIZE = MASTER_ARSIZE;
assign SLAVE_ARBURST = MASTER_ARBURST;
assign SLAVE_ARLOCK = MASTER_ARLOCK;
assign SLAVE_ARCACHE = MASTER_ARCACHE;
assign SLAVE_ARPROT = MASTER_ARPROT;
assign SLAVE_ARVALID = MASTER_ARVALID;
assign MASTER_ARREADY = SLAVE_ARREADY;
/* Read Data Channel */
assign MASTER_RID = SLAVE_RID;
assign MASTER_RDATA = SLAVE_RDATA;
assign MASTER_RRESP = SLAVE_RRESP;
assign MASTER_RLAST = SLAVE_RLAST;
assign MASTER_RVALID = SLAVE_RVALID;
assign SLAVE_RREADY = MASTER_RREADY;

endmodule

