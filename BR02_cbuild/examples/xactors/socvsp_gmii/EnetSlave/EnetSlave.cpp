/*
 *    Class implementation of the MaxSim component <EnetSlave>
 *
 *    This code has been generated by the MaxSim(tm) Component Wizard.
 *    Copyright (c) 2004-2005 by ARM Limited, 2000-2004 by AXYS Design Automation Inc., Irvine, CA, USA and AXYS GmbH, Herzongenrath
 *
 */

#include "EnetSlave.h"

#include <stdio.h>

#include "ENET_TS.h"



EnetSlave::EnetSlave(sc_mx_m_base* c, const string &s) : sc_mx_module(c, s)
{
    initComplete = false;

    ENET_TSlave = new ENET_TS( this );
    registerPort( ENET_TSlave, "ENET" );

    // Clocking the components
    SC_MX_CLOCKED();
    // Register ourself as a clock slave port.
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");




    // Place parameter definitions here:
    defineParameter( "Enable Debug Messages", "false", MX_PARAM_BOOL);

}

EnetSlave::~EnetSlave()
{

    delete ENET_TSlave;

}

void 
EnetSlave::communicate()
{  
    // the following message will be printed only in the debug version of MxExplorer
#ifdef _DEBUG_
    if (p_enableDbgMsg == true)
    {
    // NOTE: printing this message reduces the performance significantly !!! 
        message(MX_MSG_DEBUG,"Executing communicate function");
    }
#endif

    // TODO:  Add your communicate code here.
    // ...
 }

void 
EnetSlave::update()
{
    // the following message will be printed only in the debug version of MxExplorer
#ifdef _DEBUG_
    if (p_enableDbgMsg == true)
    {
        message(MX_MSG_DEBUG,"Executing update function");
    }
#endif

    // TODO:  Add your update code here.
    // ...
}

void 
EnetSlave::init()
{
	
    // Initialize ourself for save/restore.
    initStream( this );

    // Allocate memory and initialize data here:
    // ...
  
    // Call the base class after this has been initialized.
    sc_mx_module::init();

    // Set a flag that init stage has been completed
    initComplete = true;
}

void
EnetSlave::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
    // Add your reset behavior here:
    // ...

    // Call the base class after this has been reset.
    sc_mx_module::reset(level, filelist);
}

void 
EnetSlave::terminate()
{
    // Call the base class first.
    sc_mx_module::terminate();


    // Free memory which has been allocated in init stage
    // ...
}

void
EnetSlave::setParameter( const string &name, const string &value )
{
    MxConvertErrorCodes status = MxConvert_SUCCESS;

    if (name == "Enable Debug Messages")
    {
        status = MxConvertStringToValue( value, &p_enableDbgMsg  );
    }

    if ( status == MxConvert_SUCCESS )
    {
        sc_mx_module::setParameter(name, value);
    }
    else
    {
        message( MX_MSG_WARNING, "EnetSlave::setParameter: Illegal value <%s> "
		 "passed for parameter <%s>. Assignment ignored.", value.c_str(), name.c_str() );
    }
}

string
EnetSlave::getProperty( MxPropertyType property )
{
    string description; 
    switch ( property ) 
    {    
	 case MX_PROP_LOADFILE_EXTENSION:
		return "";
	 case MX_PROP_REPORT_FILE_EXT:
		return "yes";
	 case MX_PROP_COMPONENT_TYPE:
		return "Other"; 
	 case MX_PROP_COMPONENT_VERSION:
		return "0.1";
	 case MX_PROP_MSG_PREPEND_NAME:
		return "yes"; 
	 case MX_PROP_DESCRIPTION:
		description = "Dummy Slave for Enet Transactions";
		return description + " Compiled on " + __DATE__ + ", " + __TIME__; 
	 case MX_PROP_MXDI_SUPPORT:
		return "no";
	 case MX_PROP_SAVE_RESTORE:
		return "yes";
	 default:
		return "";
    }
    return "";
}

string
EnetSlave::getName(void)
{
    return "EnetSlave";
}


bool
EnetSlave::saveData( MxODataStream &data )
{
    // TODO:  Add your save code here.
    // This shows how the example state variable is saved.
    // data << exampleStateVariable;

    // TODO:  Add verification if applicable.
    // return save was successful
    return true;
}

bool
EnetSlave::restoreData( MxIDataStream &data )
{
    // TODO:  Add your restore code here.
    // This shows how the example state variable is restored.
    // data >> exampleStateVariable;

    // TODO:  Add verification if applicable.
    // return restore was successful
	return true;
}


void
EnetSlave::initSignalPort(sc_mx_signal_p_base* signalIf)
{
    MxSignalProperties prop_sm1;
    memset(&prop_sm1,0,sizeof(prop_sm1));
    prop_sm1.isOptional=false;
    prop_sm1.bitwidth=32;
    signalIf->setProperties(&prop_sm1);
}

void
EnetSlave::initTransactionPort(sc_mx_transaction_p_base* transIf)
{
    MxTransactionProperties prop;
    memset(&prop,0,sizeof(prop));
    prop.mxsiVersion = MXSI_VERSION_6;     //the transaction version used for this transaction
    prop.addressBitwidth=32;               // address bitwidth used for addressing of resources
    prop.useMultiCycleInterface = false;    // using read/write interface methods
    prop.addressBitwidth = 32;      // address bitwidth used for addressing of resources 
    prop.mauBitwidth = 8;           // minimal addressable unit 
    prop.dataBitwidth = 32;         // maximum data bitwidth transferred in a single cycle 
    prop.isLittleEndian = true;     // alignment of MAUs 
    prop.isOptional = false;				// if true this port can be disabled 
    prop.supportsAddressRegions = true;	// M/S can negotiate address mapping
    prop.numCtrlFields = 1;         // # of ctrl elements used / entry is used for transfer size
    prop.protocolID = 0x00000001;           // magic number of the protocol (Vendor/Protocol-ID): 
				                             //The upper 16 bits = protocol implementation version
											//The lower 16 bits = actual protocol ID
    sprintf(prop.protocolName,"Mx");         // The name of the  protocol being used
    // MxSI 6.0: slave port address regions forwarding 
    prop.isAddrRegionForwarded = false; //true if addr region for this slave port is actually 
											//forwarded to a master port, false otherwise
    prop.forwardAddrRegionToMasterPort = NULL; //master port of the  same component to which this slave port's addr region is forwarded
    transIf->setProperties(&prop);
}

/************************
 * EnetSlave Factory class
 ***********************/

class EnetSlaveFactory : public MxFactory
{
public:
    EnetSlaveFactory() : MxFactory ( "EnetSlave" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
        return new EnetSlave(c, id); 
    }
};

/**
 *  Entry point into the memory components (from MaxSim)
 */
extern "C" void 
MxInit(void)
{
    new EnetSlaveFactory();
}
