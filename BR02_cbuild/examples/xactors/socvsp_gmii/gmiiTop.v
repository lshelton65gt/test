//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2006 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`timescale 1ns/1ns

module gmiiTop (
                sys_clk,
                sys_reset_l
               );

   input          sys_clk;
   input          sys_reset_l;

   wire           sys_clk; 
   wire           sys_reset_l;          

  // GMII signals - rx xactor
   wire [7:0]     TXD;
   wire           TX_ER;
   wire           TX_EN;
   wire           TX_CLK;
   wire           CRS;
   wire           COL;

  // GMII signals  -- tx xactor
   wire           RX_CLK;
   wire           RX_DV;
   wire           RX_ER;
   wire [7:0]     RXD;

// Customer Module
enet_xover crossover ( RXD,       
                       RX_DV,     
                       RX_ER,     
                       RX_CLK,    
                       TXD,      
                       TX_ER, 
                       TX_EN, 
                       CRS, 
                       COL, 
                       TX_CLK );

// Ethernet Transmit Transactor Module
carbonx_enetgmii_tx xmitter ( sys_clk,   
                              sys_reset_l,
                              RXD,
                              RX_DV,
                              RX_ER,
                              RX_CLK );

// Ethernet Receive Transactor Module
carbonx_enetgmii_rx receiver ( sys_clk,   
                               sys_reset_l,
                               TXD,
                               TX_ER,
                               TX_EN,
                               CRS,
                               COL,
                               TX_CLK );
endmodule
