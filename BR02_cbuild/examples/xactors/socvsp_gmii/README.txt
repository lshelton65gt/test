Ethernet GMII Carbon Transactor in SoC Designer Example

Project:
  gmiiSys.mxp -- This project connects three components into an ethernet loopback.

                 The EnetMaster component generates ethernet packets, builds the 
                 packets into transactions, and sends the transactions out its
                 transaction master port.  

                 The gmiiTop component is created from gmiiTop.v using Carbon's 
                 compiler and wizard.  This component contains RX and TX ethernet
                 transactors and a loopback module.  The loopback module wires 
                 the RX ethernet wires back to the TX to act like a loopback cable.

                 The EnetSlave component receives ethernet transactions on its
                 transaction slave port.  The packets are extracted from the 
                 transactions and are compared with expected packets.

Components:
  EnetMaster -- init() method
                The init() method begins by allocating space for 10 ethernet
                packets.  The build_descriptor_list functions fills the fields
                in each packet.  

                The next step is to build a transaction that configures the 
                transactor connected to the EnetMaster.  The example only sets 
                the configuration to GMII interface.  This is done by creating
                a transaction request object (CarbonXTransReqT) and making it
                a configuration write transaction (csWrite).  The transaction
                request object is now built into a new CarbonXTransInfo object.
                This object inherits from MxTransactionInfo and is the 
                transaction object used by the ports in the system.  

                Finally, the ethernet packet descriptors that were built earlier 
                are built into packet request transactions and then into
                CarbonXTransInfo objects.  
                
                communicate() method
                As the simulation runs, the tickCount counter increments once
                per clock.  The component waits until tick 120 and sends the
                configuration transaction.  Then, at 125, the ethernet packet
                transactions are sent.  The transactions are queued in the slave
                port for the transactor to get and send to the Carbon Model.

                reset method
                The example does not reset the transactions or permit re-running
                of the packets.  

  EnetSlave  -- The EnetSlave component just instantiates the ethernet slave
                transaction port for the example.  Receiving and comparing the
                ethernet packets is done in the port, ENET_TS
                ENET_TS Constructor
                The constructor sets the port properties and then builds a set of
                ethernet packet descriptors that match the EnetMaster's descriptors.
                These are used to compare with the received ethernet packets from 
                the Carbon Model.  
                driveTransaction Method
                The extraction and comparison of the ethernet packets from the 
                transactions are performed in the driveTransaction method.  The
                buildPacket method makes an ethernet packet to compare with the
                received packet.  

  gmiiTop    -- The Carbon Model that is connected to the EnetSlave and EnetMaster.


Files
  enet_xover.v
    Verilog source file for the Ethernet Crossover module.
  gmiiTop.v
    Top-level Verilog testbench for the example.
  gmiiSys.mxp
    SoC Designer project
  gmiiTop.ccfg
    Carbon configuration file to build the gmiiTop component
  maxlib.conf
    Component definition file for the example
  Makefile
    Build and run the entire example
      make run -- builds all the components and runs the simulation
  README.txt
    This file

  EnetMaster Directory
    EnetMaster.cpp, EnetMaster.h
      C++ source files for the Ethernet Master componenet
    EnetMaster.conf
      Component definition file for the Ethernet Master
    Makefile
      Builds the component

  EnetSlave Directory
    EnetSlave.cpp, EnetSlave.h
      C++ source files for the Ethernet Slave component
    ENET_TS.cpp, ENET_TS.h
      C++ source files for the Ethernet slave transaction port
    EnetSlave.conf
      Component definition file for the Ethernet Slave
    Makefile
      Builds the component

