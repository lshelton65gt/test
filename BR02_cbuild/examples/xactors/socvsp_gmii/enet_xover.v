//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2006 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module enet_xover(
     RXD,       
     RX_DV,     
     RX_ER,     
     RX_CLK,    
     TXD,      
     TX_ER, 
     TX_EN, 
     CRS, 
     COL, 
     TX_CLK 
   );

 // GMII signals - receiver (rx) xactor
   output  [7:0]      TXD;       // inputs to gmiiRX
   output             TX_ER;
   output             TX_EN;
   output             TX_CLK;

   input              CRS;       // outputs from gmiiRX
   input              COL;


  // outputs from Transmitter (named RX*) are INPUTS to crossover.
   input              RX_CLK;   // Receive clock - 125MHz
   input              RX_DV;    // Receive data valid
   input              RX_ER;    // Receive error
   input [7:0]        RXD;      // Receive dat 

  // Transmitter output (named RX*) are INPUTS here.
  // They are output as RX signals (named TX*)
  //  to become INPUTS to Receiver module

  assign  TXD = RXD;     
  assign  TX_ER = RX_ER;
  assign  TX_EN = RX_DV;

  assign  TX_CLK = RX_CLK;

endmodule
