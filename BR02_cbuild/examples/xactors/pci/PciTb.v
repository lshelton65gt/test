//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
`timescale 1ns/100ps

/**************************************************************************
 This module has the PCI master and slave modules instantiated inside.
 The is also a reset module, which drives the reset, clock and interrupt 
 signals.
 **************************************************************************/

module PciTb;
   wire          clk;   // carbon depositSignal
   wire 	 rst_l; // carbon depositSignal
   wire [7:0] 	 req_l;
   wire [7:0] 	 gnt_l;
   wire 	 frame_l;
   wire 	 devsel_l;
   wire 	 irdy_l;
   wire 	 trdy_l;
   wire [63:0] 	 ad;
   wire [7:0] 	 cbe_l;
   wire 	 stop_l;
   wire 	 par;
   wire 	 perr_l;
   wire 	 serr_l;
   wire [3:0] 	 int_l;
   wire 	 idsel_0;
   wire 	 idsel_1;

   pullup (frame_l);
   pullup (devsel_l);
   pullup (irdy_l);
   pullup (trdy_l);
   pullup (stop_l);
   pullup (perr_l);
   pullup (serr_l);
   pullup (ack64_l);
   pullup (req64_l);
   pullup (par64);

   // Note:
   // req_l, gnt_l, ad, cbe_l and int_l should also be pulled up according to specification
   
   assign idsel_0 = ad[16];
   assign idsel_1 = ad[17];

   assign gnt_l = req_l;

   // Assign to prevent warnings, if using more than one master, update this assign
   assign req_l[7:1] = 7'b1111111;
  
   // This is the PCI master device.
   carbonx_pci_initiator PciInitiator0
      (
       .frame_l(frame_l),
       .irdy_l(irdy_l),
       .trdy_l(trdy_l),
       .devsel_l(devsel_l),
       .stop_l(stop_l),
       .req_l(req_l[0]),
       .gnt_l(gnt_l[0]),
       .req64_l(req64_l),
       .ack64_l(ack64_l),
       .ad(ad),
       .cbe(cbe_l),
       .par(par),
       .par64(par64),
       .perr_l(perr_l),
       .serr_l(serr_l),
       .int_l(int_l),
       .clk(clk),
       .rst_l(rst_l)
       );

   // This is a PCI target device. It is device 0
   // on PCI bus, so ad[16] is asserted for 
   // configuration access.
   carbonx_pci_target PciSlave0
      ( 
	.frame_l(frame_l),
	.irdy_l(irdy_l),
	.trdy_l(trdy_l),
	.devsel_l(devsel_l),
	.stop_l(stop_l),
	.ad(ad),
	.cbe(cbe_l),
	.idsel(idsel_0),
	.par(par),
	.par64(par64),
	.perr_l(perr_l),
	.serr_l(serr_l),
	.req64_l(req64_l),
	.ack64_l(ack64_l),
	.clk(clk),
	.reset_l(rst_l)
	);

   //Instantiate the DUT here, and connect the appropriate address line
   //to its idsel pin.
   //Program the base address registers of the DUT and target transactor
   //with exclusive values.

   // DUT
   
   
endmodule 

