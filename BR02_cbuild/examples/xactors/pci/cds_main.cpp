/* Hand-written C testbench to drive a TBP example */

#include "libdesign.h"   /* generated header file */

#include "xactors/xactors.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <time.h>

//
// Transactor Library routine
//
extern "C" int  tbp_get_error_count(void);
extern "C" void tbp_print_error_status(void);

/*
 * Slave's address range and config register address
 */
// The target is wired with ad[16] to idsel
#define SLAVE_IDSEL_ADDR        0x00010000 

// COnfig space register addresses
#define SLAVE_BASE_ADDR_REG     0x0000001c 
#define SLAVE_LIMIT_ADDR_REG    0x00000020 

// Values written into the above registers
#define SLAVE_BASE_ADDR         0x00000000 
#define SLAVE_LIMIT_ADDR        0x00001000 
#define PCI_SLAVE_MEM_SIZE      4096 

/*
** These are some utility functions
*/
void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time);
void printHeader(char *);
bool DataCompare( CarbonUInt32, CarbonUInt32);
bool DataCompare( CarbonUInt32, CarbonUInt32, CarbonUInt32);
bool ReadExpected( CarbonUInt32, CarbonUInt32);
bool ReadCsExpected( CarbonUInt32, CarbonUInt32);
static void master_func(void);
static void slave_func(void);
int  passfail(void);

const int CLOCK_CYCLE = 20;
int myERROR = 0;
CarbonUInt32 ONE = 1;
CarbonUInt32 ZERO = 0;



// Main function
int main(int argc, char *argv[]) {
 
     printHeader("Carbon PCI transactor");

    // Create Sim Infrastructure.  Master, Clocks, and Reset

    // Handle to Carbon Model
     CarbonObjectID* model = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  
     printf("Start writing VCD file\n");
     CarbonWaveID* wave = carbonWaveInitVCD(model, "PciTb.vcd", e1ns);
     assert( carbonDumpVars(wave, 6, "PciTb") == eCarbon_OK );


    // get verilog side Clock & Reset signals
    
    CarbonNetID* clock  = carbonFindNet(model, "PciTb.clk");
    CarbonNetID* rst_l = carbonFindNet(model, "PciTb.rst_l");

    assert (clock != NULL);
    assert (rst_l != NULL);

    printf("cds_main.cpp: Assert RESET [PciTb.rst_l= 0] for a few cycles\n");
    carbonDeposit(model, rst_l, &ZERO, NULL);

    CarbonTime time = 0;
    for (int i=0;i<10;i++)  run(model, clock, time); 

    printf("cds_main.cpp: Deasserting Reset. [PciTb.rst_l = 1]\n");
    carbonDeposit(model, rst_l, &ONE, NULL);


    printf("\n****** Starting PCI Initiator 0  context  ******\n");

    // Connect an SVC control function to a specific SVC instance
    carbonXAttach("PciInitiator0", (CarbonXAttachFuncT)master_func, 0);
    carbonXAttach("PciSlave0",     (CarbonXAttachFuncT)slave_func,  0);
    tbp_set_max_error_count  (2);

    // Run simulation until the SVC control function says it is done
    carbonXSetWaitTest(TEST_COMPLETE, 100000LL);

    // printf("Main:  calling master->run\n");
    while (!carbonXCheckWait()) {
      run(model, clock, time);
      printf("Main:  cycle %d\n", time / CLOCK_CYCLE);
    }

    assert( carbonDumpFlush(wave) == eCarbon_OK );
    printf("Simulation complete at cycle: %d\n", time / CLOCK_CYCLE);

    carbonDestroy(&model);

    exit( passfail() );    
}


void printHeader(char *strInfo)
{
  time_t      curtime          = time (NULL);          // Get the current time.
  struct tm  *loctime          = localtime (&curtime); // Convert it to local time representation.  
  char       *dtString         = asctime(loctime);     // convert to ascii string
  dtString[strlen(dtString)-1] = '\0';                 // remove the \n at the end of dtString
  printf("======= %s -- Starting at %s =========\n\n", strInfo, dtString);
}



/************************************************************************ 
 * This function executes whenever the PCI master transactor 
 * makes a request for service. 
************************************************************************/ 
static void master_func() { 
 
    u_int32 i; 
    u_int8 byte_test_data, byte_rd_data; 
    u_int16 hw_test_data, hw_rd_data; 
    u_int32 test_address,test_data,read_data; 
    u_int32 burst_test_data[10], burst_rd_data[10]; 
    u_int64 u64_test_data, u64_rd_data;

    carbonXIdle(10);

    //**** Before we can use the slave, we need to map it u_int32o PCI memory space *****
    carbonXPciCfgWrite32( SLAVE_IDSEL_ADDR+SLAVE_BASE_ADDR_REG,SLAVE_BASE_ADDR); 
    carbonXPciCfgWrite32( SLAVE_IDSEL_ADDR+SLAVE_LIMIT_ADDR_REG,SLAVE_LIMIT_ADDR); 
 
 
    //******* Byte access ********** 
    test_address = 0x00000000;
    byte_test_data = 0x12;
    carbonXWrite8( test_address,byte_test_data); 

    byte_rd_data = carbonXRead8( test_address); 

    if(byte_rd_data!=byte_test_data) { 
	MSG_Error("Expected 0x%x, read back 0x%x from address 0x%08x\n", 
		  byte_test_data,byte_rd_data,test_address); 
    } 
    else { 
	MSG_Milestone("Read back 0x%x from address 0x%08x\n",byte_rd_data,test_address); 
    } 

    //******* 16 bit access ********** 
    test_address = 0x00000004;
    hw_test_data = 0x3456;
    carbonXWrite16( test_address,hw_test_data);
    hw_rd_data = carbonXRead16(test_address);
    if(hw_rd_data!=hw_test_data) {
	MSG_Error("Expected 0x%x, read back 0x%x from address 0x%08x\n",
		  hw_test_data,hw_rd_data,test_address);
    }
    else {
	MSG_Milestone("Read back 0x%x from address 0x%08x\n",hw_rd_data,test_address);
    }

		
    //******* 32 bit access **********
    test_address = 0x0000000c;
    test_data = 0x98765432;
    carbonXWrite32(test_address,test_data);
    read_data = carbonXRead32(test_address);
    if(read_data!=test_data) {
	MSG_Error("Expected 0x%x, read back 0x%x from address 0x%08x\n",
		  test_data,read_data,test_address);
    }
    else {
	MSG_Milestone("Read back 0x%x from address 0x%08x\n",read_data,test_address);
    }


    //**** Configuration read ***** 
    read_data = carbonXPciCfgRead32(SLAVE_IDSEL_ADDR+SLAVE_LIMIT_ADDR_REG);
    if(read_data!=SLAVE_LIMIT_ADDR) {
	MSG_Error("Expected 0x%x, read back 0x%x from address 0x%08x\n",
		  SLAVE_LIMIT_ADDR,read_data,test_address);
    }
    else {
	MSG_Milestone("Read back 0x%x from address 0x%08x\n",read_data,test_address);
    }


    //******* Burst accesses **********
    burst_test_data[0] = 0x12345678;
    burst_test_data[1] = 0x11223344;
    test_address = 0x00000000;

    carbonXWrite(test_address, burst_test_data, 8);
    carbonXRead(test_address, burst_rd_data, 8);
    for (i=0; i < 2; i++) {
	if (burst_rd_data[i] != burst_test_data[i]) {
	    MSG_Error("Expected 0x%x, read back 0x%x from address 0x%08x\n",
		      burst_test_data[i],burst_rd_data[i],test_address+i*4);
	}
	else {
	    MSG_Milestone("Read back 0x%x from address 0x%08x\n",burst_rd_data[i],test_address+i*4);
	}
    }

/*
 * Set the test termination signal
 */
    carbonXIdle(20);  // Wait a bit, just to make waveform file more readable at the end
    carbonXSetSignal(TEST_COMPLETE); 
} 
 

/****************************************************************************
 * This is the slave function. This under an infinite loop keeps checking if
 * there is a request from the simside.
 * carbonXPciSlv_ProcessWrite : Writes data to the memory  space
 * carbonXPciSlv_ProcessRead  : Reads data from the memory space
****************************************************************************/
 
static void slave_func(void) {

  PCI_OP_T   *transaction;
  int op;

  u_int32 slave = 0;  

  // Responses for carbonXPciSlv_resp_req could be
  // RETRY, TABRT, NORMAL, DISCNT

  // Define the response
  /*******************************************
	INSERT_WAIT            = 1;
	USE_32BIT
	CAUSE_PERR
	NUM_INIT_WAITS         = 4;
	NUM_DATA_PH_BEFORE_END = 1;
	RESPONSE               = NORMAL;
  *******************************************/

  // totally simple slave, with a fixed response to all transactions
  u_int32 status = 0x00108000;
  MSG_Milestone("Slave: Started \n");

  while(TRUE) {
    status += 0x100;  // Just a debug hack
    transaction = carbonXPciGetOperation(); // Wait for a request
    op = transaction->opcode & 0xf;
    MSG_Milestone("Slave:      A:%08llx  OP:%08x SZ:%3d  ST:%08x  W:%02x  D:%016llx\n",
		  transaction->address, transaction->opcode, transaction->size,
		  transaction->status, ((u_int64 *)transaction->read_data)[0]);

    switch(op) {
    case PCI_MEM_WRITE  : carbonXPciSlvProcessWrite(slave, transaction); break;
    case PCI_MEM_READ   : carbonXPciSlvProcessRead (slave, transaction); break;
    }

    transaction->status = status;
    MSG_Milestone("Slave resp: A:%08llx  OP:%08x SZ:%3d  ST:%08x  W:%02x  D:%016llx\n",
		  transaction->address, transaction->opcode, transaction->size,
		  transaction->status, ((u_int64 *)transaction->write_data)[0]);

    carbonXPciSendOperation(transaction);
  }
}

//  ******
//    Run       run clock one clock period.
//  ******
void run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time)
{
  carbonDeposit(model, clock, &ZERO, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
  carbonDeposit(model, clock, &ONE, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
}




/****  D A T A     C O M P A R E     function  ****/

bool DataCompare( CarbonUInt32 dataR, CarbonUInt32 dataX )
{
  bool dCmp = (dataX == dataR);

  if (dCmp) printf("Received data: 0x%08x EQUALS expected data: 0x%08x\n", dataR, dataX);
  else  
      MSG_Error("Received data: 0x%08x NOT equal to expected data: 0x%08x\n",dataR, dataX); 
    
  return (dCmp);
}


bool DataCompare( CarbonUInt32 dataR, CarbonUInt32 dataX, CarbonUInt32 Addr)
{
  bool dCmp = (dataX == dataR);

  if (dCmp) printf("Mem[%x]: 0x%08x EQUALS expected data: 0x%08x\n", 
                   Addr, dataR, dataX);
  else  
      MSG_Error("Mem[%x]: 0x%08x NOT equal to expected data: 0x%08x\n", Addr, dataR, dataX); 

  return (dCmp);
}


bool ReadExpected( CarbonUInt32 Addr, CarbonUInt32 dataX)
{
   return ( DataCompare( carbonXRead32(Addr), dataX, Addr)  );
}

bool ReadCsExpected( CarbonUInt32 Addr, CarbonUInt32 dataX)
{
   return ( DataCompare( carbonXCsRead(Addr), dataX, Addr)  );
}



// -------------------

int passfail(void)
{
  myERROR += tbp_get_error_count();
                    printf ("\n\n**********\n");
  if (myERROR == 0) printf     ("** PASS **\n");
  else              printf     ("** FAIL **   error count: %d\n", myERROR);
                    printf     ("**********\n");

  return myERROR;
}                        

//
// Alternate TBP style 
//
// int passfail(void)
// {
//   int errors;
//   tbp_print_error_status();
//   return tbp_get_error_count();
// }


