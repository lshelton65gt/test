// PCI test using CarbonSim library

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#include "libdesign.h"   /* generated header file */
#include "carbon/xactors.h"

#include "carbonsim/CarbonSimMaster.h"
#include "carbonsim/CarbonSimClock.h"
#include "carbonsim/CarbonSimNet.h"
#include "carbonsim/CarbonSimMemory.h"
#include "carbonsim/CarbonSimStep.h"
#include "carbonsim/CarbonSimObjectInstance.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <time.h>

/*
** These are some utility functions
*/
void createClock(CarbonSimMaster*, int, const char*, int initVal = 0);
void bindNet(CarbonSimMaster*, CarbonSimFunctionGen*, const char*);
void printHeader(char *);
bool DataCompare( uint32, uint32);
bool DataCompare( uint32, uint32, uint32);
bool ReadExpected( uint32, uint32);
bool ReadCsExpected( uint32, uint32);
void master_func(void);
void passfail(void);

const int CLOCK_CYCLE = 20;
int myERROR = 0;

// Main function
int main(int argc, char *argv[]) {
 	CarbonUInt32 ONE = 1;
        CarbonUInt32 ZERO = 0;

        printHeader("Carbonized PCI transactor testbench");

    // Create Sim Infrastructure.  Master, Clocks, and Reset
    // Handle to main simulation object
    CarbonSimMaster* master = CarbonSimMaster::create();
    assert(master);
  
    CarbonSimObjectType*   objType = master->addObjectType("PciTb", carbon_design_create);	
    CarbonSimObjectInstance* tbObj = master->addInstance(objType, "PciTb");

    printf("Start writing VCD file\n");
    CarbonWaveID* wave = carbonWaveInitVCD(tbObj->getCarbonObject(), "PciTb.vcd", e1ns);
    if(carbonDumpVars(wave, 6, "PciTb") != eCarbon_OK) {
      assert(0);
    }
	
    // Clocks
    //
     createClock(master, CLOCK_CYCLE, "PciTb.clk", 0);

    
    // reset
    // 
    printf("cds_main.cpp: Assert RESET [PciTb.rst_l= 0] for a few cycles\n");
    CarbonSimNet* rst_l = master->findNet("PciTb.rst_l");    
    assert(rst_l != NULL);
    rst_l->deposit(&ZERO);

    for (int i=0;i<10;i++)  master->run(CLOCK_CYCLE);

    printf("cds_main.cpp: Deasserting Reset. [PciTb.rst_l = 1]\n");
    rst_l->deposit(&ONE);

    printf("\n****** Starting PCI Initiator 0  thread  ******\n");

    // Connect an SVC control function to a specific SVC instance
    SIM_Attach("PciInitiator0", master_func, 0);
    tbp_set_max_error_count  (1);

    // Run simulation until the SVC control function says it is done
    SIM_SetWaitTest(TEST_COMPLETE, 100000LL);

    // printf("Main:  calling master->run\n");
    while (!SIM_CheckWait()) {
      master->run(CLOCK_CYCLE);
      printf("Main:  cycle %d\n", master->getTick() / CLOCK_CYCLE);
    }

    if(carbonDumpFlush(wave) != eCarbon_OK) {
      assert(0);
    }

    printf("Simulation complete at cycle: %d\n", master->getTick() / CLOCK_CYCLE);

    passfail();    
    delete master;
}

void printHeader(char *strInfo)
{
  time_t      curtime          = time (NULL);          // Get the current time.
  struct tm  *loctime          = localtime (&curtime); // Convert it to local time representation.  
  char       *dtString         = asctime(loctime);     // convert to ascii string
  dtString[strlen(dtString)-1] = '\0';                 // remove the \n at the end of dtString
  printf("======= %s -- Starting at %s =========\n\n", strInfo, dtString);
}


/*
** Create a clock, bind the net, add it to the master.
** Clocks will have duty cycle = per / 2
*/
void createClock(CarbonSimMaster* master, int cycle, const char* name, int initVal) {
    CarbonSimClock *clk = new CarbonSimClock(cycle, cycle / 2, initVal);
    master->addClock(clk);
    bindNet(master, clk, name);
}

/*
** bind a net name to a function generator (clock)
** looks up the net, checks the pointer is valid, and does the binding
**
** Should just be used once per bound signal at the begining of the test
*/
void bindNet(CarbonSimMaster* m, CarbonSimFunctionGen* ck, const char* name) {
    CarbonSimNet* n = m->findNet(name);
    assert (n);
    ck->bindSignal(n);
}

void master_func(void) {
  u_int32 foo;
  u_int32  addr, data;
  u_int32  addr2, data2;

  printf("Starting master_func\n");

  SIM_Idle(10);
  printf("master_func:  Idled for 10 cycles; start write\n");

 // MSG_Error("This is NOT AN ERROR -- just checking that MSG_Error works correctly");
 
  addr = 0x320;   
  data = 0x12345678;
  SIM_Write32(addr, data);
  printf("master_func: SIM_Write32(%6x, %8x)\n", addr, data);

  foo = SIM_CsRead(0);
  printf("master_func:  Back from a CS read; got data: %08x\n", foo);
  
  addr2 = 0;  data2 = 0x24;
  SIM_CsWrite(addr2, data2);
  foo = SIM_CsRead(addr2);
  DataCompare(foo, data2);


  foo = SIM_Read32 (addr);
  DataCompare(foo, data);

  u_int32 data3 = 0x87654321; 
  PCI_MemWrite32(addr, data3);
  foo = PCI_MemRead32(addr);
  DataCompare( foo, data3);
 

  SIM_Idle(10);

  SIM_SetSignal(TEST_COMPLETE);

  printf("master_func:  signaled test complete\n");
  printf("master_func:  end of function\n");

}






/****  D A T A     C O M P A R E     function  ****/

bool DataCompare( uint32 dataR, uint32 dataX )
{
  bool dCmp = (dataX == dataR);

  if (dCmp) printf("Received data: 0x%08x EQUALS expected data: 0x%08x\n", dataR, dataX);
  else  
    { myERROR++;
      MSG_Error("Received data: 0x%08x NOT equal to expected data: 0x%08x\n", dataR, dataX); 
    }
  return (dCmp);
}


bool DataCompare( uint32 dataR, uint32 dataX, uint32 Addr)
{
  bool dCmp = (dataX == dataR);

  if (dCmp) printf("Mem[%x]: 0x%08x EQUALS expected data: 0x%08x\n", 
                   Addr, dataR, dataX);
  else  
    { myERROR++;
      MSG_Error("Mem[%x]: 0x%08x NOT equal to expected data: 0x%08x\n",
                   Addr, dataR, dataX); 
    }

  return (dCmp);
}

bool ReadExpected( uint32 Addr, uint32 dataX)
{
   return ( DataCompare( SIM_Read32(Addr), dataX, Addr)  );
}

bool ReadCsExpected( uint32 Addr, uint32 dataX)
{
   return ( DataCompare( SIM_CsRead(Addr), dataX, Addr)  );
}

void passfail(void)
{

                   printf ("\n\n**********\n");
     if (myERROR == 0) printf ("** PASS **\n");
     else              printf ("** FAIL **   error count: %d\n", myERROR);
                       printf ("**********\n");
			
}
