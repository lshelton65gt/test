# VCS TCL script
# To run script:
#   simv -q -ucli -i vcs.tcl

# Add waveforms in Carbon
call {$carbon("tb.u1 dumpfile top.vcd")}
call {$carbon("tb.u1 dumpvars")}

# Run for 500 ns
run 500

# Goodbye
quit

