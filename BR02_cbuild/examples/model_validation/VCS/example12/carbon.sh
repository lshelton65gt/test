#!/bin/sh
$CARBON_HOME/bin/cbuild -q multia.v -o libmultia.a
$CARBON_HOME/bin/MVGenerate -s VCS libmultia.symtab.db
make -f Makefile.carbon.multia

$CARBON_HOME/bin/cbuild -q multib.v -directive multib.dir -o libmultib.a
$CARBON_HOME/bin/MVGenerate -s VCS libmultib.symtab.db
make -f Makefile.carbon.multib
