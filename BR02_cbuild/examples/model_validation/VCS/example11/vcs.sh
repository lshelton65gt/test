#!/bin/sh
vcs +vpi -debug -P carbon_top.tab -load ./carbon_top.so:top_RegisterTfs testmem.v carbon_top.v +define+TOP=reftop mem.v
simv -q -ucli -i vcs.tcl

# Diff the dump files from VCS and Carbon
diff write.mem1.hex dump.mem1.hex >& dump.mem1.diff
if [ $? -ne 0 ]; then
    echo "Memory dump difference for mem1"
fi
diff write.mem2.bin dump.mem2.bin >& dump.mem1.diff
if [ $? -ne 0 ]; then
    echo "Memory dump difference for mem2"
fi
