# VCS TCL script
# To run script:
#   simv -q -ucli -i vcs.tcl

# Load the Carbon memories
call {$carbon("tb.u1 readmemh top.MEM1.mem read.mem1.hex")}
call {$carbon("tb.u1 readmemb top.MEM2.mem read.mem2.bin")}

# Load the reference model memories in VCS
memory -read tb.u2.MEM1.mem -file read.mem3.mem1.hex -radix hex
memory -read tb.u2.MEM2.mem -file read.mem3.mem2.bin -radix bin

# Run for 500ns
run 500

# Dump the Carbon memories
call {$carbon("tb.u1 dumpmemh top.MEM1.mem dump.mem1.hex")}
call {$carbon("tb.u1 dumpmemb top.MEM2.mem dump.mem2.bin")}

# Dump the reference model memories in VCS
memory -write tb.u2.MEM1.mem -file write.mem3.mem1.hex -radix hex
memory -write tb.u2.MEM2.mem -file write.mem3.mem2.bin -radix bin

# Read and verify a couple of memory locations
set data [call {$carbon("tb.u1 memread top.MEM1.mem f")}]
puts "top.MEM1.mem\[f\] = $data"
if { $data == "1234abcd" } { echo "PASS" } else { echo "FAIL" }

# Write the same location and then re-read it
call {$carbon("tb.u1 memwrite top.MEM1.mem f 12345678")}
set data [call {$carbon("tb.u1 memread top.MEM1.mem f")}]
puts "top.MEM1.mem\[f\] = $data"
if { $data == "12345678" } { echo "PASS" } else { echo "FAIL" }

# Goodbye
quit
