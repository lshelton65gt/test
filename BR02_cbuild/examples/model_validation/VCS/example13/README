This example shows how to run a simulation with multiple instances of
the same Carbon Model. This is slightly different than a
multiple-Carbon-Model simulation because there is only one Carbon
Model to compile and load. But the control of the Carbon Model
waveform dumping and accessing Carbon Model memories is still the same
as a multiple-Carbon-Model simulation.

1. Compile the Carbon Model:

$ $CARBON_HOME/bin/cbuild -q multi.v -o libmulti.a

2. Generate the wrapper:

$ $CARBON_HOME/bin/MVGenerate -s ModelSim libmulti.symtab.db

3. Build the shared library:

$ make -f Makefile.carbon.multi
env -i LANG=C COMPILER_PATH=/carbon/product/latest/Linux/bin HOME=/home/carbon LD_LIBRARY_PATH=/carbon/product/latest/Linux/gcc/lib /carbon/product/latest/Linux/gcc345/bin/gcc -I. -I/carbon/product/latest/include -c -g -I/software/vcs/X-2005.06-SP1/include -I/carbon/product/latest/include carbon_multi.c
env -i LANG=C COMPILER_PATH=/carbon/product/latest/Linux/bin HOME=/home/carbon LD_LIBRARY_PATH=/carbon/product/latest/Linux/gcc/lib /carbon/product/latest/Linux/gcc345/bin/gcc -I. -I/carbon/product/latest/include -L/carbon/product/latest/Linux/gcc/lib -Wl,-rpath,/carbon/product/latest/Linux/gcc/lib -g -shared -o carbon_multi.so carbon_multi.o -L. -lmulti -lpliwrapper /carbon/product/latest/makefiles/LINK.script -L/carbon/product/latest/Linux/lib -L/carbon/product/latest/Linux/lib/ES3 -L/carbon/product/latest/Linux/lib/gcc3/shared -L/carbon/product/latest/Linux/lib/gcc3 -Wl,-rpath,/carbon/product/latest/Linux/lib/gcc3/shared -Wl,-dn -lcarbonvspx -lcarbonmem -Wl,-dy -lcarbon -Wl,-dn -lnffw -llmgr_nomt -lcrvs -lsb -lz -Wl,-dy
rm carbon_multi.o

4. Build the shared library:

$ vcs +vpi -debug -P carbon_multi.tab -load carbon_multi.so:multi_RegisterTfs testmulti.v carbon_multi.v +define+TOP=reftop multi.v

5. Simulate:

$ simv -q -ucli -i vcs.tcl

Note the following commands to load and dump the memories for the
multiple Carbon Model instances as well as the wave form dumping
commands:

# Load the u1 and u2 memories
call {$carbon("tb.u1 readmemh multi.MEM1.mem read.mem1.hex")}
call {$carbon("tb.u1 readmemb multi.MEM2.mem read.mem2.bin")}

...

# Load the u3 and u4 memories
call {$carbon("tb.u3 readmemh multi.MEM1.mem read.mem1.hex")}
call {$carbon("tb.u3 readmemb multi.MEM2.mem read.mem2.bin")}

...

# Dump the Carbon model wave files
call {$carbon("tb.u1 dumpfile multi.u1.vcd")}
call {$carbon("tb.u1 dumpvars 0 multi")}
call {$carbon("tb.u3 dumpfile multi.u3.vcd")}
call {$carbon("tb.u3 dumpvars 1 multi")}

...

# Dump the u1 and u2 memories
call {$carbon("tb.u1 dumpmemh multi.MEM1.mem dump.u1.mem1.hex")}
call {$carbon("tb.u1 dumpmemb multi.MEM2.mem dump.u1.mem2.bin")}

...

call {$carbon("tb.u3 dumpmemh multi.MEM1.mem dump.u3.mem1.hex")}
call {$carbon("tb.u3 dumpmemb multi.MEM2.mem dump.u3.mem2.bin")}

