# VCS TCL script
# To run script:
#   simv -q -ucli -i vcs.tcl

# Load the u1 and u2 memories
call {$carbon("tb.u1 readmemh multi.MEM1.mem read.mem1.hex")}
call {$carbon("tb.u1 readmemb multi.MEM2.mem read.mem2.bin")}
memory -read tb.u2.MEM1.mem -file read.mem1.hex -radix hex
memory -read tb.u2.MEM2.mem -file read.mem2.bin -radix bin

# Load the u3 and u4 memories
call {$carbon("tb.u3 readmemh multi.MEM1.mem read.mem1.hex")}
call {$carbon("tb.u3 readmemb multi.MEM2.mem read.mem2.bin")}
memory -read tb.u4.MEM1.mem -file read.mem1.hex -radix hex
memory -read tb.u4.MEM2.mem -file read.mem2.bin -radix bin

# Dump the Carbon model wave files
call {$carbon("tb.u1 dumpfile multi.u1.vcd")}
call {$carbon("tb.u1 dumpvars 0 multi")}
call {$carbon("tb.u3 dumpfile multi.u3.vcd")}
call {$carbon("tb.u3 dumpvars 1 multi")}

# Run the simulation for 500ns
run -timepoint 500

# Dump the u1 and u2 memories
call {$carbon("tb.u1 dumpmemh multi.MEM1.mem dump.u1.mem1.hex")}
call {$carbon("tb.u1 dumpmemb multi.MEM2.mem dump.u1.mem2.bin")}
memory -write tb.u2.MEM1.mem -file write.u2.mem1.hex -radix hex
memory -write tb.u2.MEM2.mem -file write.u2.mem2.bin -radix bin

call {$carbon("tb.u3 dumpmemh multi.MEM1.mem dump.u3.mem1.hex")}
call {$carbon("tb.u3 dumpmemb multi.MEM2.mem dump.u3.mem2.bin")}
memory -write tb.u4.MEM1.mem -file write.u4.mem1.hex -radix hex
memory -write tb.u4.MEM2.mem -file write.u4.mem2.bin -radix hex

# Read and verify a couple of memory locations
set data [call {$carbon("tb.u1 memread multi.MEM1.mem f")}]
puts "multi.u1.MEM1.mem\[f\] = $data"
if { $data == "1234abcd" } { echo "PASS" } else { echo "FAIL" }

# Write the same location and then re-read it
call {$carbon("tb.u1 memwrite multi.MEM1.mem f 12345678")}
set data [call {$carbon("tb.u1 memread multi.MEM1.mem f")}]
puts "multi.u1.MEM1.mem\[f\] = $data"
if { $data == "12345678" } { echo "PASS" } else { echo "FAIL" }

# Read and verify a couple of memory locations
set data [call {$carbon("tb.u3 memread multi.MEM1.mem f")}]
puts "multi.u3.MEM1.mem\[f\] = $data"
if { $data == "1234abcd" } { echo "PASS" } else { echo "FAIL" }

# Write the same location and then re-read it
call {$carbon("tb.u3 memwrite multi.MEM1.mem f 12345678")}
set data [call {$carbon("tb.u3 memread multi.MEM1.mem f")}]
puts "multi.u3.MEM1.mem\[f\] = $data"
if { $data == "12345678" } { echo "PASS" } else { echo "FAIL" }

# Goodbye
exit
