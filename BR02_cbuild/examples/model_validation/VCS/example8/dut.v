`define DUT dut

module `DUT (q, gclk, clk, en, d);
   output [7:0] q;
   output       gclk;
   input        clk, en;
   input [7:0]  d;

   assign       gclk = clk & en;

   reg [7:0]    q;
   initial q = 0;
   always @ (posedge gclk)
     q <= d;

endmodule
