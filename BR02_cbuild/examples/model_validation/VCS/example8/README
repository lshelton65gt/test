This example introduces delays added to the outputs of the Carbon
Model. In this case the Carbon Model computes both a clock and some
data and provides them at the exact same time. This can cause a race
in the other model. By adding a delay to the data portion, it
guarantees that the test behaves correctly.

The delays are added to the XML configuration file. Since it can be
painful to create an XML file that mirrors the shadow hierarchy, the
MVGenerate tool provides a boot strapping method to create an XML file
that can be edited to apply the delays.

1. Compile the Carbon Model:

$ $CARBON_HOME/bin/cbuild -q dut.v

2. Generate a configuration file and then edit it to apply the
   delays. This can then be used to generate the correct HDL and C
   Wrappers:

$ $CARBON_HOME/bin/MVGenerate -s VCS -w dut.xml libdesign.symtab.db
$ <Edit the dut.xml> -- See the Appendix for the final XML file.
$ $CARBON_HOME/bin/MVGenerate -c dut.xml libdesign.symtab.db

3. Build the shared library:

make -f Makefile.carbon.dut
env -i LANG=C COMPILER_PATH=/carbon/product/latest/Linux/bin HOME=/home/carbon LD_LIBRARY_PATH=/carbon/product/latest/Linux/gcc/lib /carbon/product/latest/Linux/gcc345/bin/gcc -I. -I/carbon/product/latest/include -c -g -I/software/vcs/X-2005.06-SP1/include -I/carbon/product/latest/include carbon_dut.c
env -i LANG=C COMPILER_PATH=/carbon/product/latest/Linux/bin HOME=/home/carbon LD_LIBRARY_PATH=/carbon/product/latest/Linux/gcc/lib /carbon/product/latest/Linux/gcc345/bin/gcc -I. -I/carbon/product/latest/include -L/carbon/product/latest/Linux/gcc/lib -Wl,-rpath,/carbon/product/latest/Linux/gcc/lib -g -shared -o carbon_dut.so carbon_dut.o -L. -ldesign -lpliwrapper /carbon/product/latest/makefiles/LINK.script -L/carbon/product/latest/Linux/lib -L/carbon/product/latest/Linux/lib/ES3 -L/carbon/product/latest/Linux/lib/gcc3/shared -L/carbon/product/latest/Linux/lib/gcc3 -Wl,-rpath,/carbon/product/latest/Linux/lib/gcc3/shared -Wl,-dn -lcarbonvspx -lcarbonmem -Wl,-dy -lcarbon -Wl,-dn -lnffw -llmgr_nomt -lcrvs -lsb -lz -Wl,-dy
rm carbon_dut.o

4. Compile the Carbon Model into the testbench:

$ vcs +vpi -debug -P carbon_dut.tab -load ./carbon_dut.so:dut_RegisterTfs test.v carbon_dut.v +define+DUT=refdut dut.v

5. Simulate:

$ simv -q -ucli -i vcs.tcl
