# VCS TCL script
# To run script:
#   simv -q -ucli -i vcs.tcl

# Add waveforms in Carbon
call {$carbon("tb.CarbonDut dumpfile dut.vcd")}
call {$carbon("tb.CarbonDut dumpvars")}

# Run for 500 ns
run 500

# Goodbye
quit

