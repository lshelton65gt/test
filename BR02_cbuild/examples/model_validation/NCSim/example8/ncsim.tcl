# NCSim TCL script
# To run script:
#   ncsim tb -input ncsim.tcl

# Add waveforms in Carbon
call carbon {"tb.CarbonDut dumpfile dut.vcd"}
call carbon {"tb.CarbonDut dumpvars"}

# Run for 500 ns
run -timepoint 500

# Goodbye
exit
