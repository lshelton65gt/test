# NCSim TCL script
# To run script:
#   ncsim tb -input ncsim.tcl

# Use a script to add readmem/dumpmem capabilities to NCSim
source readmem.tcl

# Load the Carbon memories
call carbon {"tb.u1 readmemh top.MEM1.mem read.mem1.hex"}
call carbon {"tb.u1 readmemb top.MEM2.mem read.mem2.bin"}

# Load the reference model memories in NCSim
readmemh read.mem1.hex tb.u2.MEM1.mem
readmemb read.mem2.bin tb.u2.MEM2.mem

# Run for 500ns
run -timepoint 500

# Dump the Carbon memories
call carbon {"tb.u1 dumpmemh top.MEM1.mem dump.mem1.hex"}
call carbon {"tb.u1 dumpmemb top.MEM2.mem dump.mem2.bin"}

# Dump the reference model memories in NCSim
memory -dump %h tb.u2.MEM1.mem write.mem1.hex
memory -dump %b tb.u2.MEM2.mem write.mem2.bin

# Read and verify a couple of memory locations
set data [call carbon {"tb.u1 memread top.MEM1.mem f"}]
puts "top.MEM1.mem\[f\] = $data"
if { $data == 32'b00010010001101001010101111001101 } { echo "PASS" } else { echo "FAIL" }

# Write the same location and then re-read it
call carbon {"tb.u1 memwrite top.MEM1.mem f 12345678"}
set data [call carbon {"tb.u1 memread top.MEM1.mem f"}]
puts "top.MEM1.mem\[f\] = $data"
if { $data == 32'b00010010001101000101011001111000 } { echo "PASS" } else { echo "FAIL" }

# Goodbye
exit
