proc readmemb {filename memname {startadd -1} {endadd -1}} {
    return [readmem $filename $memname $startadd $endadd 'b]
}
proc readmemh {filename memname {startadd -1} {endadd -1}} {
    return [readmem $filename $memname $startadd $endadd 'h]
}
proc readmemo {filename memname {startadd -1} {endadd -1}} {
    return [readmem $filename $memname $startadd $endadd 'o]
}
proc readmemd {filename memname {startadd -1} {endadd -1}} {
    return [readmem $filename $memname $startadd $endadd 'd]
}
proc readmem {filename memname startaddr endaddr format} {
    set fname [open $filename r]
    set range [lindex [split [describe $memname] " "] 2]
    set lbit  [lindex [split $range {[ ] :}] 1]
    set rbit  [lindex [split $range {[ ] :}] 2]
    set width [expr (($lbit<$rbit) ? $rbit - $lbit + 1 : $lbit - $rbit + 1)]

    set depth [lindex [split [describe $memname] " "] 4]
    set laddr [lindex [split $depth {[ ] :}] 1]
    set raddr [lindex [split $depth {[ ] :}] 2]
    if {$startaddr != -1} {
	set start [expr vdec($startaddr)]
	set startchar [string first d $start]
	set start [string range $start [expr ($startchar + 1)] end]
	if {$endaddr != -1} {
	    set end [expr vdec($endaddr)]
	    set startchar [string first d $end]
	    set end [string range $end [expr ($startchar + 1)] end]
	} else {
	    set end $raddr
	}
    } else {
	set start $laddr
	set end $raddr
    }

    set lowest [expr (($laddr<$raddr)?$laddr:$raddr)]
    set highest [expr (($laddr<$raddr)?$raddr:$laddr)]
    if {$start < $lowest} {
	puts "Error: start address is out of range (min address is $lowest)"
	return -1
    }
    if {$end > $highest} {
	puts "Error: end address is out of range (max address is $highest)"
	return -1
    }

    puts "*** Reading memfile $filename, address locations $start to $end"
    set addr $start
    set inc [expr (($start<$end)?1:-1)]
    set inblock 0
    while {[gets $fname vecin] >= 0} {
	if {$inblock == 1} {
	    if {[regex {\*\u2044} $vector]} {
		set inblock 0
	    }
	    continue
	} else {
	    set vector [string trim $vecin]
	    if {[regex {\u2044\*} $vector]} {
		if {[regex {\*\u2044} $vector]} {continue}
		set inblock 1
		continue
	    }
	}
	if {[regex {^\s*//} $vector]} {
	    continue
	}
	set vector [string trim $vector]
	if {[string length $vector] == 0} { continue }
	set vector [string trim $vecin]
	if {[regexp {\@[a-z,A-Z,0-9]+$} $vector]} {
	    set addr [expr vdec(\u2019h[string trim $vector @])]
	    set startchar [string first d $addr]
	    set addr [string range $addr [expr ($startchar + 1)] end]
	    continue
	}
	if {$addr == [expr ($end + $inc)]} {
	    puts "break"
	    break
	}
	puts "deposit $memname[$addr] $width$format$vector"
	deposit $memname[$addr] $width$format$vector
	set addr [expr ($addr + $inc)]
    }
    close $fname
    return 0
}

proc dumpmemb {filename memname {startadd -1} {endadd -1}} {
    return [dumpmem $filename $memname $startadd $endadd %b]
}
proc dumpmemh {filename memname {startadd -1} {endadd -1}} {
    return [dumpmem $filename $memname $startadd $endadd %h]
}
proc dumpmemo {filename memname {startadd -1} {endadd -1}} {
    return [dumpmem $filename $memname $startadd $endadd %o]
}
proc dumpmemd {filename memname {startadd -1} {endadd -1}} {
    return [dumpmem $filename $memname $startadd $endadd %d]
}
proc dumpmem {filename memname startaddr endaddr format} {
    set fname [open $filename w+]
    set depth [lindex [split [describe $memname] " "] 4]
    set laddr [lindex [split $depth {[ ] :}] 1]
    set raddr [lindex [split $depth {[ ] :}] 2]
    set lowest [expr (($laddr<$raddr)?$laddr:$raddr)]
    set highest [expr (($laddr<$raddr)?$raddr:$laddr)]
    if {$startaddr != -1} {
	set start [expr vdec($startaddr)]
	set startchar [string first d $start]
	set start [string range $start [expr ($startchar + 1)] end]
	if {$endaddr != -1} {
	    set end [expr vdec($endaddr)]
	    set startchar [string first d $end]
	    set end [string range $end [expr ($startchar + 1)] end]
	} else {
	    set end $highest
	}
    } else {
	set start $lowest
	set end $highest
    }

    if {$start < $lowest} {
	puts "Error: start address is out of range (min address is $lowest)"
	return -1
    }
    if {$end > $highest} {
	puts "Error: end address is out of range (max address is $highest)"
	return -1
    }

    puts "*** Dumping memfile $filename, address locations $start to $end"
    set addr $start
    set inc [expr (($start<$end)?1:-1)]

    while {$addr != [expr ($end + $inc)]} {
	set val [value $format $memname[$addr]]
	puts "value $format $memname[$addr] = $val"
	puts $fname $val
	set addr [expr ($addr + $inc)]
    }
    close $fname
    return 0
}
