#!/bin/sh
ncvlog testmem.v carbon_top.v -define TOP=reftop mem.v
ncelab -loadvpi carbon_top.so:top_RegisterTfs -access +rw tb
ncsim tb -input ncsim.tcl

# Diff the dump files from NCSim and Carbon
grep "/" write.mem1.hex | awk -F'/' '{ print $2 }' > ncsim-dump.mem1.hex
grep "/" write.mem2.bin | awk -F'/' '{ print $2 }' > ncsim-dump.mem2.bin
diff ncsim-dump.mem1.hex dump.mem1.hex >& dump.mem1.diff
if [ $? -ne 0 ]; then
    echo "Memory dump difference for mem1"
fi
diff ncsim-dump.mem2.bin dump.mem2.bin >& dump.mem1.diff
if [ $? -ne 0 ]; then
    echo "Memory dump difference for mem2"
fi
