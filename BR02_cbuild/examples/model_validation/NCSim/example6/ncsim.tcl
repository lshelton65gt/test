# NCSim TCL script
# To run script:
#   ncsim tb -input ncsim.tcl

# Add waveforms in Carbon
call carbon {"tb.u1 dumpfile top.vcd"}
call carbon {"tb.u1 dumpvars 1 top.F1"}
call carbon {"tb.u1 dumpvars 1 top.F2"}

# Run for 500 ns
run -timepoint 500

# Goodbye
exit
