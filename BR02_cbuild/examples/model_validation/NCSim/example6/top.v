`define TOP top
`define SIZE 8

module flop(q, clk, d);
  output [`SIZE-1:0] q;
  input clk;
  input [`SIZE-1:0] d;

  reg [`SIZE-1:0] q;

  initial q = 0;

  always @ (posedge clk)
    q <= d;

endmodule

// test
module `TOP(clk1, clk2, combo1, combo2, combo3, in1, in2, async1, async2);
  input clk1, clk2;
  output combo1, combo2, combo3;
  input in1, in2;
  output async1, async2;

  wire [`SIZE-1:0] count1, count2, count3, count4, count5, count6;

  flop F1 (count2, clk2, count1);
  flop F2 (count4, clk1, count3);
  flop F3 (count6, clk2, count5);

  assign combo1 = (count1 == `SIZE'b10);
  assign combo2 = (count3 != `SIZE'b100);
  assign combo3 = (count5 == `SIZE'b110);

  assign async1 = in1 & in2;
  assign async2 = in1 | in2;

endmodule

 
