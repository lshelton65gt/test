#!/bin/sh
if [ "$CARBON_TARGET_ARCH" == "Linux64" ]; then
$CARBON_HOME/bin/cbuild -q top.v -directive top.dir -Wc -fPIC
else
$CARBON_HOME/bin/cbuild -q top.v -directive top.dir
fi
$CARBON_HOME/bin/MVGenerate -s NCSim libdesign.symtab.db
make -f Makefile.carbon.top
