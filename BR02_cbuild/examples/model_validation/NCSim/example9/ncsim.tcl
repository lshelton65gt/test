# NCSim TCL script
# To run script:
#   ncsim tb -input ncsim.tcl

# Add waveforms in Carbon
call carbon {"tb.u1 dumpfile top.vcd"}
call carbon {"tb.u1 dumpvars"}

# Run for 500 ns
run -timepoint 500

# Goodbye
exit
