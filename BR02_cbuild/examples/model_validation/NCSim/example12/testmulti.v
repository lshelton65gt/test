`define SIZE 8
module tb;

   reg [`SIZE-1:0]  count1;
   wire [`SIZE-1:0] count2_u1;
   wire [`SIZE-1:0] count2_u2;
   reg [`SIZE-1:0]  count3_u2;
   wire [`SIZE-1:0] count4_u1;
   wire [`SIZE-1:0] count4_u2;
   reg [`SIZE-1:0]  count5_u2;
   wire [`SIZE-1:0] count6_u1;
   wire [`SIZE-1:0] count6_u2;

   wire [`SIZE-1:0] count2_u3;
   wire [`SIZE-1:0] count2_u4;
   reg [`SIZE-1:0]  count3_u4;
   wire [`SIZE-1:0] count4_u3;
   wire [`SIZE-1:0] count4_u4;
   reg [`SIZE-1:0]  count5_u4;
   wire [`SIZE-1:0] count6_u3;
   wire [`SIZE-1:0] count6_u4;

   wire       combo1_u1;
   wire       combo1_u2;
   wire       combo2_u1;
   wire       combo2_u2;
   wire       combo3_u1;
   wire       combo3_u2;

   wire       combo1_u3;
   wire       combo1_u4;
   wire       combo2_u3;
   wire       combo2_u4;
   wire       combo3_u3;
   wire       combo3_u4;

   wire       in1;
   wire       in2;

   wire       async1_u1;
   wire       async1_u2;
   wire       async2_u1;
   wire       async2_u2;

   wire       async1_u3;
   wire       async1_u4;
   wire       async2_u3;
   wire       async2_u4;
   
   reg 	      clk1;
   reg 	      clk2;

   wire       en1;
   wire       en2;

   wire [`SIZE-1:0] inout1_u1;
   wire [`SIZE-1:0] inout1_u2;
   wire [`SIZE-1:0] inout2_u1;
   wire [`SIZE-1:0] inout2_u2;
   wire [`SIZE-1:0] inout3_u1;
   wire [`SIZE-1:0] inout3_u2;

   reg [31:0] count;
   initial begin
      count1 = 0;
      count3_u2 = 0;
      count5_u2 = 0;
      count3_u4 = 0;
      count5_u4 = 0;
      count = 0;
      clk1 = 0;
      clk2 = 0;
   end

   // Clock generation
   always
     clk1 = #10 ~clk1;
   always
     clk2 = #20 ~clk2;

   // Generate inputs
   always @ (posedge clk1)
     begin
	count <= count + 1;
     end
   assign in1 = count[1];
   assign in2 = count[2];
   assign en1 = count[0] ^ count[3];
   assign en2 = count[1];


   // Counter seed
   always @ (posedge clk1)
     begin
	count1 <= count1 + 17;
     end

   // Add a multi clock flop sequence
   always @ (posedge clk2)
     begin
	count3_u2 <= count2_u2;
	count5_u2 <= count4_u2;
	count3_u4 <= count2_u4;
	count5_u4 <= count4_u4;
     end

   // a instance is the ios16 test case
   multia u1 (clk1, clk2, count1, count2_u1, count3_u2, count4_u1, count5_u2, count6_u1,
	   combo1_u1, combo2_u1, combo3_u1, in1, in2, async1_u1, async2_u1,
	   en1, en2, inout1_u1, inout2_u1, inout3_u1);
   
   reftopa u2 (clk1, clk2, count1, count2_u2, count3_u2, count4_u2, count5_u2, count6_u2,
	      combo1_u2, combo2_u2, combo3_u2, in1, in2, async1_u2, async2_u2,
	      en1, en2, inout1_u2, inout2_u2, inout3_u2);

   // Drive the bus from here sometimes
   assign inout1_u1 = !en1 ? count4_u1 : `SIZE'bz;
   assign inout1_u2 = !en1 ? count4_u2 : `SIZE'bz;
   assign inout2_u1 = !en2 ? count2_u1 : `SIZE'bz;
   assign inout2_u2 = !en2 ? count2_u2 : `SIZE'bz;
   assign inout3_u1 = en2 ? count1 : `SIZE'bz; // This should have conflicts
   assign inout3_u2 = en2 ? count1 : `SIZE'bz; // This should have conflicts

   // Hier refs to connect counters to multib
   assign u3.count1 = count1;
   assign u4.count1 = count1;
   assign u3.count3 = count3_u4;
   assign u4.count3 = count3_u4;
   assign u3.count5 = count5_u4;
   assign u4.count5 = count5_u4;
   assign count2_u3 = u3.F1.q;
   assign count2_u4 = u4.F1.q;
   assign count4_u3 = u3.F2.q;
   assign count4_u4 = u4.F2.q;
   assign count6_u3 = u3.F3.q;
   assign count6_u4 = u4.F3.q;

   // multib is the depobs10 test case
   multib u3 (clk1, clk2, 
	   combo1_u3, combo2_u3, combo3_u3, in1, in2, async1_u3, async2_u3);
   
   reftopb u4 (clk1, clk2, 
	      combo1_u4, combo2_u4, combo3_u4, in1, in2, async1_u4, async2_u4);

   always @ (posedge clk1 or negedge clk1)
     begin
	if ($time != 0)
	  begin
	     // test multia
	     $display("%t: multia: count2: Carbon: %x, Reference: %x",
		      $time, count2_u1, count2_u2);
	     if (count2_u1 != count2_u2)
	       $display("ERROR: %t: count2 mismatch", $time);
	     $display("%t: multia: count4: Carbon: %x, Reference: %x",
		      $time, count4_u1, count4_u2);
	     if (count4_u1 != count4_u2)
	       $display("ERROR: %t: count4 mismatch", $time);
	     $display("%t: multia: count6: Carbon: %x, Reference: %x",
		      $time, count6_u1, count6_u2);
	     if (count6_u1 != count6_u2)
	       $display("ERROR: %t: count6 mismatch", $time);
	     $display("%t: multia: combo1: Carbon: %x, Reference: %x",
		      $time, combo1_u1, combo1_u2);
	     if (combo1_u1 != combo1_u2)
	       $display("ERROR: %t: combo1 mismatch", $time);
	     $display("%t: multia: combo2: Carbon: %x, Reference: %x",
		      $time, combo2_u1, combo2_u2);
	     if (combo2_u1 != combo2_u2)
	       $display("ERROR: %t: combo2 mismatch", $time);
	     $display("%t: multia: combo3: Carbon: %x, Reference: %x",
		      $time, combo3_u1, combo3_u2);
	     if (combo3_u1 != combo3_u2)
	       $display("ERROR: %t: combo3 mismatch", $time);
	     $display("%t: multia: async1: Carbon: %x, Reference: %x",
		      $time, async1_u1, async1_u2);
	     if (async1_u1 != async1_u2)
	       $display("ERROR: %t: async1 mismatch", $time);
	     $display("%t: multia: async2: Carbon: %x, Reference: %x",
		      $time, async2_u1, async2_u2);
	     if (async2_u1 != async2_u2)
	       $display("ERROR: %t: async2 mismatch", $time);
	     $display("%t: multia: inout1: Carbon: %x, Reference: %x",
		      $time, inout1_u1, inout1_u2);
	     if (inout1_u1 !== inout1_u2)
	       $display("ERROR: %t: inout1 mismatch", $time);
	     $display("%t: multia: inout2: Carbon: %x, Reference: %x",
		      $time, inout2_u1, inout2_u2);
	     if (inout2_u1 !== inout2_u2)
	       $display("ERROR: %t: inout2 mismatch", $time);
	     $display("%t: multia: inout3: Carbon: %x, Reference: %x",
		      $time, inout3_u1, inout3_u2);
	     if (inout3_u1 !== inout3_u2)
	       $display("ERROR: %t: inout3 mismatch", $time);
	  end // always @ (posedge clk1 or negedge clk1)

	// test multib
	if (count2_u3 != count2_u4)
	  $display("ERROR: %t: count2 mismatch", $time);
	else
	  $display("%t: multib: count2: Carbon: %x, Reference: %x",
		   $time, count2_u3, count2_u4);
	if (count4_u3 != count4_u4)
	  $display("ERROR: %t: count4 mismatch", $time);
	else
	  $display("%t: multib: count4: Carbon: %x, Reference: %x",
		   $time, count4_u3, count4_u4);
	if (count6_u3 != count6_u4)
	  $display("ERROR: %t:%d: count6 mismatch, c: %x, m: %x", $time, $time, count6_u3, count6_u4);
	else
	  $display("%t: multib: count6: Carbon: %x, Reference: %x",
		   $time, count6_u3, count6_u4);
	if (combo1_u3 != combo1_u4)
	  $display("ERROR: %t: combo1 mismatch", $time);
	else
	  $display("%t: multib: combo1: Carbon: %x, Reference: %x",
		   $time, combo1_u3, combo1_u4);
	if (combo2_u3 != combo2_u4)
	  $display("ERROR: %t: combo2 mismatch", $time);
	else
	  $display("%t: multib: combo2: Carbon: %x, Reference: %x",
		   $time, combo2_u3, combo2_u4);
	if (combo3_u3 != combo3_u4)
	  $display("ERROR: %t: combo3 mismatch", $time);
	else
	  $display("%t: multib: combo3: Carbon: %x, Reference: %x",
		   $time, combo3_u3, combo3_u4);
	if (async1_u3 != async1_u4)
	  $display("ERROR: %t: async1 mismatch", $time);
	else
	  $display("%t: multib: async1: Carbon: %x, Reference: %x",
		   $time, async1_u3, async1_u4);
	if (async2_u3 != async2_u4)
	  $display("ERROR: %t: async2 mismatch", $time);
	else
	  $display("%t: multib: async2: Carbon: %x, Reference: %x",
		   $time, async2_u3, async2_u4);
     end // always @ (posedge clk1 or negedge clk1)
endmodule // tb


