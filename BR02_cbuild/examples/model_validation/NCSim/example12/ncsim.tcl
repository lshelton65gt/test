# NCSim TCL script
# To run script:
#   ncsim tb -input ncsim.tcl

# List the set of Carbon Models by using a non-existent model path.
call carbon {"models"}

# Create a dump file for all of the multia Carbon Models
call carbon {"tb.u1 dumpfile multia.vcd"}
call carbon {"tb.u1 dumpvars 0 multia"}

# Create a dump file for just the flop sub levels of multib
call carbon {"tb.u3 dumpfile multib.vcd"}
call carbon {"tb.u3 dumpvars 1 multib.F1"}
call carbon {"tb.u3 dumpvars 1 multib.F2"}
call carbon {"tb.u3 dumpvars 1 multib.F3"}

# Run the simulation for 100ns
run -timepoint 100

# Disable dumping and run for another 100ns
call carbon {"tb.u1 dumpoff"}
call carbon {"tb.u3 dumpoff"}
run -timepoint 200

# Turn it back on and run for another 300ns
call carbon {"tb.u1 dumpon"}
call carbon {"tb.u3 dumpon"}
run -timepoint 500

# Goodbye
exit
