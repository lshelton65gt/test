// Same as ios16 but with deposit/observable points
// This is used to show a multi-Carbon Model simulation
`define SIZE 8 
`define TOPB multib

module flop(q, clk, d);
  output [`SIZE-1:0] q;
  input clk;
  input [`SIZE-1:0] d;

  reg [`SIZE-1:0] q;

  initial q = 0;

  always @ (posedge clk)
    q <= d;

endmodule

// test
module `TOPB(clk1, clk2, combo1, combo2, combo3, in1, in2, async1, async2);
  input clk1, clk2;
  output combo1, combo2, combo3;
  input in1, in2;
  output async1, async2;

  wire [`SIZE-1:0] count1, count2, count3, count4, count5, count6;

  flop F1 (count2, clk2, count1);
  flop F2 (count4, clk1, count3);
  flop F3 (count6, clk2, count5);

  assign combo1 = (count1[0] == 1'b1);
  assign combo2 = (count3[1] != 1'b1);
  assign combo3 = (count5[2] == 1'b1);

  assign async1 = in1 & in2;
  assign async2 = in1 | in2;

endmodule
