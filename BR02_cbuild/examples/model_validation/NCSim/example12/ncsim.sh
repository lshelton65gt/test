#!/bin/sh
ncvlog testmulti.v carbon_multia.v carbon_multib.v -define TOPA=reftopa -define TOPB=reftopb multia.v multib.v
ncelab -loadvpi carbon_multia.so:multia_RegisterTfs -loadvpi carbon_multib.so:multib_RegisterTfs -access +rw tb
ncsim tb -input ncsim.tcl

