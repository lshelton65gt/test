# NCSim TCL script
# To run script:
#   ncsim tb -input ncsim.tcl

# Useful tcl file to read/dump NCSim memories
source readmem.tcl

# Load the u1 and u2 memories
call carbon {"tb.u1 readmemh multi.MEM1.mem read.mem1.hex"}
call carbon {"tb.u1 readmemb multi.MEM2.mem read.mem2.bin"}
readmemh read.mem1.hex tb.u2.MEM1.mem
readmemb read.mem2.bin tb.u2.MEM2.mem

# Load the u3 and u4 memories
call carbon {"tb.u3 readmemh multi.MEM1.mem read.mem1.hex"}
call carbon {"tb.u3 readmemb multi.MEM2.mem read.mem2.bin"}
readmemh read.mem1.hex tb.u4.MEM1.mem
readmemb read.mem2.bin tb.u4.MEM2.mem

# Dump the Carbon model wave files
call carbon {"tb.u1 dumpfile multi.u1.vcd"}
call carbon {"tb.u1 dumpvars 0 multi"}
call carbon {"tb.u3 dumpfile multi.u3.vcd"}
call carbon {"tb.u3 dumpvars 1 multi"}

# Run the simulation for 500ns
run -timepoint 500

# Dump the u1 and u2 memories
call carbon {"tb.u1 dumpmemh multi.MEM1.mem dump.u1.mem1.hex"}
call carbon {"tb.u1 dumpmemb multi.MEM2.mem dump.u1.mem2.bin"}
memory -dump %h tb.u2.MEM1.mem write.u2.mem1.hex
memory -dump %b tb.u2.MEM2.mem write.u2.mem2.bin

call carbon {"tb.u3 dumpmemh multi.MEM1.mem dump.u3.mem1.hex"}
call carbon {"tb.u3 dumpmemb multi.MEM2.mem dump.u3.mem2.bin"}
dumpmemh write.u4.mem1.hex tb.u4.MEM1.mem
dumpmemb write.u4.mem2.bin tb.u4.MEM2.mem

# Read and verify a couple of memory locations
set data [call carbon {"tb.u1 memread multi.MEM1.mem f"}]
puts "multi.u1.MEM1.mem\[f\] = $data"
if { $data == 32'b00010010001101001010101111001101 } { echo "PASS" } else { echo "FAIL" }

# Write the same location and then re-read it
call carbon {"tb.u1 memwrite multi.MEM1.mem f 12345678"}
set data [call carbon {"tb.u1 memread multi.MEM1.mem f"}]
puts "multi.u1.MEM1.mem\[f\] = $data"
if { $data == 32'b00010010001101000101011001111000 } { echo "PASS" } else { echo "FAIL" }

# Read and verify a couple of memory locations
set data [call carbon {"tb.u3 memread multi.MEM1.mem f"}]
puts "multi.u3.MEM1.mem\[f\] = $data"
if { $data == 32'b00010010001101001010101111001101 } { echo "PASS" } else { echo "FAIL" }

# Write the same location and then re-read it
call carbon {"tb.u3 memwrite multi.MEM1.mem f 12345678"}
set data [call carbon {"tb.u3 memread multi.MEM1.mem f"}]
puts "multi.u3.MEM1.mem\[f\] = $data"
if { $data == 32'b00010010001101000101011001111000 } { echo "PASS" } else { echo "FAIL" }

# Goodbye
exit
