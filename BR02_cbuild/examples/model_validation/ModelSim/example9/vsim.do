# ModelSim TCL script
# To run script:
#   vsim tb -do vsim.do -c -pli carbon_top.so

# Setup Error Handler
onerror {resume}

# Add waveforms in ModelSim
add wave -recursive *

# Add waveforms in Carbon
carbon tb.u1 dumpfile top.vcd
carbon tb.u1 dumpvars

# Run for 500 ns
run 500

# Goodbye
quit

