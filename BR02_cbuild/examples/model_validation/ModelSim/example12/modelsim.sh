#!/bin/sh
$MTI_HOME/bin/vlib work
$MTI_HOME/bin/vlog testmulti.v carbon_multia.v carbon_multib.v +define+TOPA=reftopa +define+TOPB=reftopb multia.v multib.v
$MTI_HOME/bin/vsim tb -do vsim.do -c -pli carbon_multia.so -pli carbon_multib.so
