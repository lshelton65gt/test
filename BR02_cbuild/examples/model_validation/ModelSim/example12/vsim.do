# ModelSim TCL script
# To run script:
#   vsim tb -do vsim.do -c -pli carbon_multia.so -pli carbon_multib.so

# Setup Error Handler
onerror {resume}

# List the set of Carbon Models by using a non-existent model path.
carbon models

# Create a dump file for all of the multia Carbon model
carbon tb.u1 dumpfile multia.vcd
carbon tb.u1 dumpvars 0 multia

# Create a dump file for just the flop sub levels of multib
carbon tb.u3 dumpfile multib.vcd
carbon tb.u3 dumpvars 1 multib.F1
carbon tb.u3 dumpvars 1 multib.F2
carbon tb.u3 dumpvars 1 multib.F3

# Run the simulation for 100ns
run 100

# Disable dumping and run for another 100ns
carbon tb.u1 dumpoff
carbon tb.u3 dumpoff
run 100

# Turn it back on and run for another 300ns
carbon tb.u1 dumpon
carbon tb.u3 dumpon
run 300

# Goodbye
quit

