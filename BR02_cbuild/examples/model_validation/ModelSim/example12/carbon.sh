#!/bin/sh
if [ "$CARBON_TARGET_ARCH" == "Linux64" ]; then
$CARBON_HOME/bin/cbuild -q multia.v -Wc -fPIC -o libmultia.a
else
$CARBON_HOME/bin/cbuild -q multia.v -o libmultia.a
fi
$CARBON_HOME/bin/MVGenerate -s ModelSim libmultia.symtab.db
make -f Makefile.carbon.multia

if [ "$CARBON_TARGET_ARCH" == "Linux64" ]; then
$CARBON_HOME/bin/cbuild -q multib.v -directive multib.dir -Wc -fPIC -o libmultib.a
else
$CARBON_HOME/bin/cbuild -q multib.v -directive multib.dir -o libmultib.a
fi
$CARBON_HOME/bin/MVGenerate -s ModelSim libmultib.symtab.db
make -f Makefile.carbon.multib
