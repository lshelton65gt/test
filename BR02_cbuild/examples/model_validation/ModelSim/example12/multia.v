// Simple I/O test for Verilog with counters
// This is used to show a multi-Carbon Model simulation
`define SIZE 8
`define TOPA multia

// test
module `TOPA(clk1, clk2, count1, count2, count3, count4, count5, count6,
	    combo1, combo2, combo3, in1, in2, async1, async2, en1, en2,
	    inout1, inout2, inout3);
   input	clk1, clk2;
   input [`SIZE-1:0] 	count1, count3, count5;
   output [`SIZE-1:0] count2, count4, count6;
   output 	combo1, combo2, combo3;
   input 	in1, in2;
   output 	async1, async2;
   input 	en1, en2;
   inout [`SIZE-1:0] 	inout1, inout2, inout3;

   reg [`SIZE-1:0] 	count2, count4, count6;
   initial begin
      count2 = 0;
      count4 = 0;
      count6 = 0;
   end

   always @ (posedge clk2)
     begin
	count2 <= count1;
	count6 <= count5;
     end
   always @ (posedge clk1)
     begin
	count4 <= count3;
     end

   assign combo1 = (count1[0] == 1'b1);
   assign combo2 = (count3[1] != 1'b1);
   assign combo3 = (count5[2] == 1'b1);

   assign async1 = in1 & in2;
   assign async2 = in1 | in2;

   assign inout1 = en1 ? count1 : `SIZE'bz;
   assign inout2 = en2 ? count3 : `SIZE'bz;
   assign inout3 = en1 & en2 ? count5: `SIZE'bz;

endmodule

