# ModelSim TCL script
# To run script:
#   vsim test -do vsim.do -c

# Setup Error Handler
onerror {resume}

# Add waveforms in ModelSim
add wave -recursive *

# Run for 500 ns
run 500

# Goodbye
quit

