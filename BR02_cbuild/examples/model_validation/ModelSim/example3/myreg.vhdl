library ieee;
use ieee.std_logic_1164.all;

entity flop is
  
  port (
    q   : out std_logic_vector (7 downto 0);
    clk : in  std_logic;
    d   : in  std_logic_vector (7 downto 0));

end flop;

architecture rtl of flop is

begin  -- rtl

  process (clk)
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge
      q <= d;
    end if;
  end process;

end rtl;

library ieee;
use ieee.std_logic_1164.all;

entity myreg is
  
  port (
    clk : in  std_logic;
    io1 : inout std_logic_vector (7 downto 0);
    io2 : inout std_logic_vector (7 downto 0);
    dir1 : in std_logic;
    dir2 : in std_logic);

end myreg;

architecture rtl of myreg is

  signal out1 : std_logic_vector (7 downto 0) := (others => '0');
  signal out2 : std_logic_vector (7 downto 0) := (others => '0');
  signal en1 : std_logic := '0';
  signal en2 : std_logic := '0';
  signal alt_dir2 : std_logic := '0';

  component flop
    port (
      q   : out std_logic_vector (7 downto 0);
      clk : in  std_logic;
      d   : in  std_logic_vector (7 downto 0));
  end component;

begin  -- arch

  process (clk)
  begin
    if clk'event and clk = '1' then
      en1 <= alt_dir2;
      en2 <= dir1;
    end if;
  end process;

  u1 : flop
    port map (
      q   => out1,
      clk => clk,
      d   => io2);
  u2 : flop
    port map (
      q   => out2,
      clk => clk,
      d   => io1);

  io1 <= out1 when en1 = '1' else (others => 'Z');
  io2 <= out2 when en2 = '1' else (others => 'Z');
  
end rtl;
