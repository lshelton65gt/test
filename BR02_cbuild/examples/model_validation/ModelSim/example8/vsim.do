# ModelSim TCL script
# To run script:
#   vsim tb -do vsim.do -c -pli carbon_dut.so

# Setup Error Handler
onerror {resume}

# Add waveforms in ModelSim
add wave -recursive *

# Add waveforms in Carbon
carbon tb.CarbonDut dumpfile dut.vcd
carbon tb.CarbonDut dumpvars

# Run for 500 ns
run 500

# Goodbye
quit

