#!/bin/sh
if [ "$CARBON_TARGET_ARCH" == "Linux64" ]; then
$CARBON_HOME/bin/cbuild -q -Wc -fPIC dut.v
else
$CARBON_HOME/bin/cbuild -q dut.v
fi
$CARBON_HOME/bin/MVGenerate -c dut.xml libdesign.symtab.db
make -f Makefile.carbon.dut
