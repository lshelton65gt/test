module tb;

   // Stimulus
   reg clk;
   wire en;
   wire [7:0] d;
   reg [9:0]  count;

   // Outputs
   wire       gclk1, gclk2;
   wire [7:0] q1, q2;
   reg [7:0]  q3, q4;

   initial begin
      clk = 0;
      count = 0;
      q3 = 0;
      q4 = 0;
   end

   // Create a clock
   always
     clk = #10 ~clk;

   // Create the stimulus
   always @ (posedge clk)
     begin
	count <= count + 1;
     end
   assign en = count[0] | count[1];
   assign d = count[9:2];

   // Instantiate the carbon version
   dut CarbonDut (q1, gclk1, clk, en, d);

   // Instantiate the reference version
   refdut ReferenceDut(q2, gclk2, clk, en, d);

   // Use the two generated clocks
   always @ (posedge gclk1)
     begin
	q3 <= q1;
     end
   always @ (posedge gclk2)
     begin
	q4 <= q2;
     end

   // Test the results
   always @ (posedge clk)
     begin
	if (gclk1 != gclk2)
	  $display("ERROR: diff: carbon gclk1 = %x, reference gclk2 = %x",
		   gclk1, gclk2);
	if (q1 !== q2)
	  $display("ERROR: diff: carbon q1 = %x, reference q2 = %x",
		   q1, q2);
	if (q3 !== q4)
	  $display("ERROR: diff: carbon q3 = %x, reference q4 = %x",
		   q3, q4);
     end // always @ (posedge clk)

endmodule // tb
