library ieee;
use ieee.std_logic_1164.all;

entity flop is
  
  port (
    q   : out bit_vector (7 downto 0);
    clk : in  bit;
    d   : in  std_logic_vector (7 downto 0));

end flop;

architecture rtl of flop is
  function to_bit_vector(arg: std_logic_vector) return bit_vector;

  function to_bit_vector (arg : std_logic_vector) return bit_vector is
    variable result : bit_vector(arg'range);
  begin
    for i in arg'range loop
      if arg(i) = '1' then
        result(i) := '1';
      else
        result(i) := '0';
      end if;
    end loop;
    return result;
  end;
      
begin  -- rtl

  process (clk)
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge
      q <= to_bit_vector(d);
    end if;
  end process;

end rtl;

library ieee;
use ieee.std_logic_1164.all;

entity myreg is
  
  port (
    clk : in  bit;
    io1 : inout std_logic_vector (7 downto 0);
    io2 : inout std_logic_vector (7 downto 0);
    dir1 : in bit;
    dir2 : in bit);

end myreg;

architecture rtl of myreg is

  function to_std_logic_vector(arg: bit_vector) return std_logic_vector;

  function to_std_logic_vector (arg : bit_vector) return std_logic_vector is
    variable result : std_logic_vector(arg'range);
  begin
    for i in arg'range loop
      if arg(i) = '1' then
        result(i) := '1';
      else
        result(i) := '0';
      end if;
    end loop;
    return result;
  end;

  signal out1 : bit_vector (7 downto 0) := (others => '0');
  signal out2 : bit_vector (7 downto 0) := (others => '0');
  signal en1 : bit := '0';
  signal en2 : bit := '0';
  signal alt_dir2 : bit := '0';

  component flop
    port (
      q   : out bit_vector (7 downto 0);
      clk : in  bit;
      d   : in  std_logic_vector (7 downto 0));
  end component;

begin  -- arch

  process (clk)
  begin
    if clk'event and clk = '1' then
      en1 <= alt_dir2;
      en2 <= dir1;
    end if;
  end process;

  u1 : flop
    port map (
      q   => out1,
      clk => clk,
      d   => io2);
  u2 : flop
    port map (
      q   => out2,
      clk => clk,
      d   => io1);

  io1 <= to_std_logic_vector(out1) when en1 = '1' else (others => 'Z');
  io2 <= to_std_logic_vector(out2) when en2 = '1' else (others => 'Z');
  
end rtl;
