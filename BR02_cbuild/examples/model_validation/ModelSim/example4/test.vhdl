library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
use std.textio.all;
library modelsim_lib;
use modelsim_lib.util.all;

entity test is
  
end test;

architecture arch of test is

  component myreg
    port (
      clk : in  bit;
      io1 : inout std_logic_vector (7 downto 0);
      io2 : inout std_logic_vector (7 downto 0);
      dir1 : in bit;
      dir2 : in bit);
  end component;

  for u1 : myreg use entity work.myreg(rtl);
--  for u2 : myreg use entity work.myreg(rtl);
  for u2 : myreg use entity work.myreg(carbon);
  
signal clk : bit := '0';
signal num : bit_vector (7 downto 0) := (others => '0');
signal io11  : std_logic_vector (7 downto 0) := (others => '0');
signal io21  : std_logic_vector (7 downto 0) := (others => '0');
signal io12  : std_logic_vector (7 downto 0) := (others => '0');
signal io22  : std_logic_vector (7 downto 0) := (others => '0');
signal dir1 : bit := '0';
signal dir2 : bit := '0';
  signal out1_u1 : bit_vector (7 downto 0) := (others => '0');
  signal out1_u2 : bit_vector (7 downto 0) := (others => '0');   
  
  function to_std_logic_vector(arg: bit_vector) return std_logic_vector;

  function to_std_logic_vector (arg : bit_vector) return std_logic_vector is
    variable result : std_logic_vector(arg'range);
  begin
    for i in arg'range loop
      if arg(i) = '1' then
        result(i) := '1';
      else
        result(i) := '0';
      end if;
    end loop;
    return result;
  end;

  function to_integer(arg: bit_vector) return integer;

  function to_integer (arg : bit_vector) return integer is
    variable result : integer;
  begin
    result := 0;
    for i in arg'range loop
      result := result + result;
      if arg(i) = '1' then
        result := result + 1;
      end if;
    end loop;
    return result;
  end;

  function to_bit_vector8(arg: integer) return bit_vector;

  function to_bit_vector8 (arg : integer) return bit_vector is
    variable result : bit_vector(7 downto 0);
    variable val : integer;
  begin
    result := (others => '0');
    val := arg;
    for i in result'range loop
      if (val mod 2) = 1 then
        result(i) := '1';
      else
        result(i) := '0';
      end if;
      val := val / 2;
    end loop;
    return result;
  end;

begin  -- arch

  dir1 <= not(num(0)) and not(num(1));
  dir2 <= not(num(0)) and num(1);
  io11 <= to_std_logic_vector(num) when dir1 = '1' else (others => 'Z');
  io12 <= to_std_logic_vector(num) when dir1 = '1' else (others => 'Z');
  io21 <= to_std_logic_vector(num) when dir2 = '1' else (others => 'Z');
  io22 <= to_std_logic_vector(num) when dir2 = '1' else (others => 'Z');

  -- Add signal spies for the out and dir2 signals
  process
  begin
    init_signal_spy("u1/u1/q", "out1_u1", 1);
    init_signal_spy("u2/u1/q", "out1_u2", 1);
    init_signal_driver("dir2", "u1/alt_dir2", open, open, 1);
    init_signal_driver("dir2", "u2/alt_dir2", open, open, 1);
    wait;
  end process;

  u1 : myreg
    port map (
      clk => clk,
      io1 => io11,
      io2 => io21,
      dir1 => dir1,
      dir2 => dir2);

  u2 : myreg
    port map (
      clk => clk,
      io1 => io12,
      io2 => io22,
      dir1 => dir1,
      dir2 => dir2);

  process
   begin
     wait for 1 ns;
     clk <= '1';
     wait for 1 ns;
     clk <= '0';
   end process;

   process (clk)
     variable myline : line;
     variable i : integer;
     variable val : integer;
   begin
     if clk'event and clk = '1' then
       val := to_integer(num);
       val := val + 1;
       num <= to_bit_vector8(val);
       write(myline, string'("io11 = "));
       write(myline, io11);
       write(myline, string'(", io12 = "));
       write(myline, io12);
       for i in io11'range loop
         if io11(i) /= io12(i) then
           write (myline, string'("  ***DIFF***"));
           exit;
         end if;
       end loop;
       writeline(OUTPUT, myline);

       write(myline, string'("io21 = "));
       write(myline, io21);
       write(myline, string'(", io22 = "));
       write(myline, io22);
       for i in io21'range loop
         if io21(i) /= io22(i) then
           write (myline, string'("  ***DIFF***"));
           exit;
         end if;
       end loop;
       writeline(OUTPUT, myline);

       write(myline, string'("out1_u1 = "));
       write(myline, out1_u1);
       write(myline, string'(", out1_u2 = "));
       write(myline, out1_u2);
       for i in out1_u1'range loop
         if (out1_u1(i) /= out1_u2(i))
         then
           write (myline, string'("  ***DIFF***"));
           exit;
         end if;
       end loop;
       writeline(OUTPUT, myline);

     end if;
   end process;
  
end arch;
