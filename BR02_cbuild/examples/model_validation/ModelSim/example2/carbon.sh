#!/bin/sh
if [ "$CARBON_TARGET_ARCH" == "Linux64" ]; then
$CARBON_HOME/bin/cbuild -q -vhdlTop myreg -Wc -fPIC myreg.vhdl
else
$CARBON_HOME/bin/cbuild -q -vhdlTop myreg myreg.vhdl
fi
$CARBON_HOME/bin/MVGenerate --config=myreg.xml libdesign.symtab.db
make -f Makefile.carbon.myreg
