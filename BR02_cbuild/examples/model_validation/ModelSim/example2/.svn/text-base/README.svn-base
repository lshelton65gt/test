This example shows a complete run of the Model Validation tool.

1. The first step is to compile the Carbon Model:

$ ${CARBON_HOME}/bin/cbuild -q -vhdlTop myreg myreg.vhdl

2. The next step is to generate the wrapper logic:

$(CARBON_HOME)/bin/MVGenerate --config=myreg.xml libdesign.symtab.db

The following files appear in that directory:

$ ls carbon* Makefile.carbon*
carbon_myreg.c carbon_myreg.vhdl Makefile.carbon.myreg

3. Compile the shareable object as follows:

$ make -f Makefile.carbon.myreg
gcc -c -g -I/software/modeltech6.0/include -I/carbon/product/latest/include carbon_myreg.c
env -i LANG=C COMPILER_PATH=/carbon/product/latest/Linux/bin HOME=/home/carbon LD_LIBRARY_PATH=/carbon/product/latest/Linux/gcc/lib /carbon/product/latest/Linux/gcc345/bin/g++ -I. -I/carbon/product/latest/include -g -shared -o carbon_myreg.so carbon_myreg.o -L. -ldesign /carbon/product/latest/makefiles/LINK.script -L/carbon/product/latest/Linux/lib -L/carbon/product/latest/Linux/lib/ES3 -L/carbon/product/latest/Linux/lib/gcc3 -Wl,-rpath,/carbon/product/latest/Linux/lib -Wl,-rpath,/carbon/product/latest/Linux/lib/ES3 -Wl,-rpath,/carbon/product/latest/Linux/gcc/lib -Wl,-dn -lcarbonvspx -lcarbon -lnffw -llmgr_nomt -lcrvs -lsb -Wl,-dy -lz -Wl,-dn -lcarbonmem -Wl,-dy

This creates the library:

$ ls *.so
carbon_myreg.so

4. Compile the wrapper HDL into the test bench. In this case, the test
   bench compiles both the original model RTL as well as the Carbon
   Model and compares the results:


$ vlib work
$ vcom myreg.vhdl carbon_myreg.vhdl test.vhdl

5. Run the simulation. The vsim.do file contains:

================================ vsim.do =============================
# ModelSim TCL script
# To run script:
#   vsim test -do vsim.do -c

# Setup Error Handler
onerror {resume}

# Add waveforms in ModelSim
add wave -recursive *

# Run for 500 ns
run 500

# Goodbye
quit
================================ vsim.do =============================

$ vsim test -do vsim.do -c
