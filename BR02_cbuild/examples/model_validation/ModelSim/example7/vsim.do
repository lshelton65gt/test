# ModelSim TCL script
# To run script:
#   vsim test -do vsim.do -c

# Setup Error Handler
onerror {resume}

# Add waveforms in ModelSim
add wave -recursive *

# Add waveforms in Carbon
carbon /test/carbon_dut dumpfile dut.vcd
carbon /test/carbon_dut dumpvars

# Run for 500 ns
run 500

# Goodbye
quit

