library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity test is
end test;

architecture cmp of test is

  component dut
    port(   clk1      : in bit;
            clk2      : in bit;
            count1    : in std_logic_vector(7 downto 0);
            count2    : out std_logic_vector(7 downto 0);
            count3    : in std_logic_vector(7 downto 0);
            count4    : out std_logic_vector(7 downto 0);
            count5    : in std_logic_vector(7 downto 0);
            count6    : out std_logic_vector(7 downto 0)
        );
  end component;

  signal clk1 : bit := '0';
  signal clk2 : bit := '0';
  signal count1 : std_logic_vector(7 downto 0) := "00000000";
  signal count2_ref : std_logic_vector(7 downto 0) := "00000000";
  signal count2_crbn : std_logic_vector(7 downto 0) := "00000000";
  signal count3 : std_logic_vector(7 downto 0) := "00000000";
  signal count4_ref : std_logic_vector(7 downto 0) := "00000000";
  signal count4_crbn : std_logic_vector(7 downto 0) := "00000000";
  signal count5 : std_logic_vector(7 downto 0) := "00000000";
  signal count6_ref : std_logic_vector(7 downto 0) := "00000000";
  signal count6_crbn : std_logic_vector(7 downto 0) := "00000000";

  for ref_dut : dut use entity work.dut(rtl);
  for carbon_dut : dut use entity work.dut(carbon);

  function to_std_ulogic_vector32(arg: integer) return std_ulogic_vector;

  function to_std_ulogic_vector32(arg : integer) return std_ulogic_vector is
    variable result : std_ulogic_vector(31 downto 0);
    variable val : integer;
  begin
    result := (others => '0');
    val := arg;
    for i in result'range loop
      if (val mod 2) = 1 then
        result(i) := '1';
      else
        result(i) := '0';
      end if;
      val := val / 2;
    end loop;
    return result;
  end;

begin

  ref_dut : dut
       port map ( clk1, clk2,
                  count1, count2_ref, count3, count4_ref, count5, count6_ref
                );

  carbon_dut : dut
       port map ( clk1, clk2,
                  count1, count2_crbn, count3, count4_crbn, count5, count6_crbn
                );


-- Stimulus

  -- purpose: clock generation
  -- type   : combinational
  -- inputs : 
  -- outputs: clk1, clk2
  process
  begin  -- process
    wait for 20 ns;
    clk1 <= not clk1;
    clk2 <= not clk2;
  end process;

    process (clk1)
      variable val : integer;
    begin  -- process
      if clk1'event and clk1 = '1' then  -- rising clock edge
        val := to_integer(unsigned(count1)) + 1;
        count1 <= std_logic_vector(to_unsigned(val, count1'length));
      end if;
    end process;

    process (clk2)
    begin  -- process
      if clk2'event and clk2 = '1' then  -- rising clock edge
        count3 <= count2_ref;
        count5 <= count4_ref;
      end if;
    end process;

    -- Comparison
    process (clk1)
    begin  -- process
      if (clk1'event and (clk1 = '1' or clk1 = '0'))  then  -- either edge of clock
        assert (count2_ref = count2_crbn) report "ERROR:count2 mismatch" severity ERROR;
        assert (count4_ref = count4_crbn) report "ERROR:count4 mismatch" severity ERROR;
        assert (count6_ref = count6_crbn) report "ERROR:count6 mismatch" severity ERROR;
      end if;
    end process;
    
end cmp;
