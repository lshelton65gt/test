library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity dut is
    port(   clk1         : in bit;
            clk2         : in bit;
            count1       : in std_logic_vector(7 downto 0) := "00000000";
            count2       : out std_logic_vector(7 downto 0) := "00000000";
            count3       : in std_logic_vector(7 downto 0) := "00000000";
            count4       : out std_logic_vector(7 downto 0) := "00000000";
            count5       : in std_logic_vector(7 downto 0) := "00000000";
            count6       : out std_logic_vector(7 downto 0) := "00000000"
        );
end;

architecture rtl of dut is
  
begin
  process (clk2)
  begin  -- process
    if clk2'event and clk2 = '1' then
      count2 <= count1;
      count6 <= count5;
    end if;
  end process;

  process (clk1)
  begin  -- process
    if clk1'event and clk1 = '1' then  -- rising clock edge
      count4 <= count3;
    end if;
  end process;

end;

