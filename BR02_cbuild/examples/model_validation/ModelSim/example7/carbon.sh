#!/bin/sh
if [ "$CARBON_TARGET_ARCH" == "Linux64" ]; then
$CARBON_HOME/bin/cbuild -q -vhdlTop dut -Wc -fPIC dut.vhdl
else
$CARBON_HOME/bin/cbuild -q -vhdlTop dut dut.vhdl
fi
$CARBON_HOME/bin/MVGenerate -c dut.xml libdesign.symtab.db
make -f Makefile.carbon.dut
