# ModelSim TCL script
# To run script:
#   vsim tb -do vsim.do -c -pli carbon_top.so

# Setup Error Handler
onerror {resume}

# Add waveforms in ModelSim
add wave -recursive *

# Load the Carbon memories
carbon tb.u1 readmemh top.MEM1.mem read.mem1.hex
carbon tb.u1 readmemb top.MEM2.mem read.mem2.bin

# Load the reference model memories in ModelSim
mem load -infile read.mem1.hex -format hex tb.u2.MEM1.mem
mem load -infile read.mem2.bin -format bin tb.u2.MEM2.mem

# Run for 500ns
run 500

# Dump the Carbon memories
carbon tb.u1 dumpmemh top.MEM1.mem dump.mem1.hex
carbon tb.u1 dumpmemb top.MEM2.mem dump.mem2.bin

# Dump the reference model memories in ModelSim
mem save -outfile write.mem1.hex -format hex -wordsperline 1 tb.u2.MEM1.mem
mem save -outfile write.mem2.bin -format bin -wordsperline 1 tb.u2.MEM2.mem

# Read and verify a couple of memory locations
set addr 0xf
set data [carbon tb.u1 memread top.MEM1.mem $addr]
puts "top.MEM1.mem\[$addr\] = $data"
if { $data == "1234abcd" } { echo "PASS" } else { echo "FAIL" }

# Write the same location and then re-read it
set data 0x12345678
carbon tb.u1 memwrite top.MEM1.mem $addr $data
set data [carbon tb.u1 memread top.MEM1.mem $addr]
puts "top.MEM1.mem\[$addr\] = $data"
if { $data == "12345678" } { echo "PASS" } else { echo "FAIL" }

# Goodbye
quit
