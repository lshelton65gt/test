#!/bin/sh
if [ "$CARBON_TARGET_ARCH" == "Linux64" ]; then
$CARBON_HOME/bin/cbuild -q mem.v -directive mem.dir -Wc -fPIC
else
$CARBON_HOME/bin/cbuild -q mem.v -directive mem.dir
fi
$CARBON_HOME/bin/MVGenerate -s ModelSim libdesign.symtab.db
make -f Makefile.carbon.top
