`define SIZE 32

module tb;

   reg [`SIZE-1:0]  count1;
   wire [`SIZE-1:0] count2_u1;
   wire [`SIZE-1:0] count2_u2;
   reg [`SIZE-1:0]  count3;
   wire [`SIZE-1:0] count4_u1;
   wire [`SIZE-1:0] count4_u2;
   reg [`SIZE-1:0]  count5;
   wire [`SIZE-1:0] count6_u1;
   wire [`SIZE-1:0] count6_u2;

   wire       combo1_u1;
   wire       combo1_u2;
   wire       combo2_u1;
   wire       combo2_u2;
   wire       combo3_u1;
   wire       combo3_u2;

   wire       in1;
   wire       in2;
   wire       async1_u1;
   wire       async1_u2;
   wire       async2_u1;
   wire       async2_u2;
   
   reg 	      clk1;
   reg 	      clk2;

   wire       en1;
   wire       en2;
   wire [`SIZE-1:0] inout1_u1;
   wire [`SIZE-1:0] inout1_u2;
   wire [`SIZE-1:0] inout2_u1;
   wire [`SIZE-1:0] inout2_u2;
   wire [`SIZE-1:0] inout3_u1;
   wire [`SIZE-1:0] inout3_u2;

   wire [3:0] 	    raddr, waddr;
   wire 	    ren, wen;
   wire [`SIZE-1:0] rdata1_u1, rdata1_u2, rdata2_u1, rdata2_u2, wdata;

   reg [31:0] count;
   initial begin
      count1 = 0;
      count3 = 0;
      count5 = 0;
      count = 0;
      clk1 = 0;
      clk2 = 0;
   end

   // Clock generation
   always
     clk1 = #10 ~clk1;
   always
     clk2 = #20 ~clk2;

   // Generate inputs
   always @ (posedge clk1)
     begin
	count <= count + 1;
     end
   assign in1 = count[1];
   assign in2 = count[2];
   assign en1 = count[0] ^ count[3];
   assign en2 = count[1];
   assign raddr = count[3:0];
   assign waddr = count[4:1];
   assign ren = 1;
   assign wen = count[1] | count[2];
   assign wdata = count;

   // Counter seed
   always @ (posedge clk1)
     begin
	count1 <= count1 + 17;
     end

   // Add a multi clock flop sequence
   always @ (posedge clk2)
     begin
	count3 <= count2_u2;
	count5 <= count4_u2;
     end

   top u1 (clk1, clk2, count1, count2_u1, count3, count4_u1, count5, count6_u1,
	   combo1_u1, combo2_u1, combo3_u1, in1, in2, async1_u1, async2_u1,
	   en1, en2, inout1_u1, inout2_u1, inout3_u1,
	   raddr, waddr, wdata, rdata1_u1, rdata2_u1, wen, ren);
   
   reftop u2 (clk1, clk2, count1, count2_u2, count3, count4_u2, count5, count6_u2,
	      combo1_u2, combo2_u2, combo3_u2, in1, in2, async1_u2, async2_u2,
	      en1, en2, inout1_u2, inout2_u2, inout3_u2,
	      raddr, waddr, wdata, rdata1_u2, rdata2_u2, wen, ren);

   // Drive the bus from here sometimes
   assign inout1_u1 = !en1 ? count4_u1 : `SIZE'bz;
   assign inout1_u2 = !en1 ? count4_u2 : `SIZE'bz;
   assign inout2_u1 = !en2 ? count2_u1 : `SIZE'bz;
   assign inout2_u2 = !en2 ? count2_u2 : `SIZE'bz;
   assign inout3_u1 = en2 ? count1 : `SIZE'bz; // This should have conflicts
   assign inout3_u2 = en2 ? count1 : `SIZE'bz; // This should have conflicts

   always @ (posedge clk1 or negedge clk1)
     begin
	if ($time != 0)
	  begin
	     $display("%t: count2: Carbon: %x, Reference: %x",
		      $time, count2_u1, count2_u2);
	     if (count2_u1 != count2_u2)
	       $display("ERROR: %t: count2 mismatch", $time);
	     $display("%t: count4: Carbon: %x, Reference: %x",
		      $time, count4_u1, count4_u2);
	     if (count4_u1 != count4_u2)
	       $display("ERROR: %t: count4 mismatch", $time);
	     $display("%t: count6: Carbon: %x, Reference: %x",
		      $time, count6_u1, count6_u2);
	     if (count6_u1 != count6_u2)
	       $display("ERROR: %t: count6 mismatch", $time);
	     $display("%t: combo1: Carbon: %x, Reference: %x",
		      $time, combo1_u1, combo1_u2);
	     if (combo1_u1 != combo1_u2)
	       $display("ERROR: %t: combo1 mismatch", $time);
	     $display("%t: combo2: Carbon: %x, Reference: %x",
		      $time, combo2_u1, combo2_u2);
	     if (combo2_u1 != combo2_u2)
	       $display("ERROR: %t: combo2 mismatch", $time);
	     $display("%t: combo3: Carbon: %x, Reference: %x",
		      $time, combo3_u1, combo3_u2);
	     if (combo3_u1 != combo3_u2)
	       $display("ERROR: %t: combo3 mismatch", $time);
	     $display("%t: async1: Carbon: %x, Reference: %x",
		      $time, async1_u1, async1_u2);
	     if (async1_u1 != async1_u2)
	       $display("ERROR: %t: async1 mismatch", $time);
	     $display("%t: async2: Carbon: %x, Reference: %x",
		      $time, async2_u1, async2_u2);
	     if (async2_u1 != async2_u2)
	       $display("ERROR: %t: async2 mismatch", $time);
	     $display("%t: inout1: Carbon: %x, Reference: %x",
		      $time, inout1_u1, inout1_u2);
	     if (inout1_u1 !== inout1_u2)
	       $display("ERROR: %t: inout1 mismatch", $time);
	     $display("%t: inout2: Carbon: %x, Reference: %x",
		      $time, inout2_u1, inout2_u2);
	     if (inout2_u1 !== inout2_u2)
	       $display("ERROR: %t: inout2 mismatch", $time);
	     $display("%t: inout3: Carbon: %x, Reference: %x",
		      $time, inout3_u1, inout3_u2);
	     if (inout3_u1 !== inout3_u2)
	       $display("ERROR: %t: inout3 mismatch", $time);
	     $display("%t: rdata1: Carbon: %x, Reference: %x",
		      $time, rdata1_u1, rdata1_u2);
	     if (rdata1_u1 !== rdata1_u2)
	       $display("ERROR: %t: rdata1 mismatch", $time);
	     $display("%t: rdata2: Carbon: %x, Reference: %x",
		      $time, rdata2_u1, rdata2_u2);
	     if (rdata2_u1 !== rdata2_u2)
	       $display("ERROR: %t: rdata2 mismatch", $time);
	  end // always @ (posedge clk1 or negedge clk1)
     end // always @ (posedge clk1 or negedge clk1)
endmodule // tb


