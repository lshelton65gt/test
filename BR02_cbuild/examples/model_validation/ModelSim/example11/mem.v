// A memory that support readmemh and dumpmem
`define TOP top
`define SIZE 32

// test
module `TOP(clk1, clk2, count1, count2, count3, count4, count5, count6,
	    combo1, combo2, combo3, in1, in2, async1, async2, en1, en2,
	    inout1, inout2, inout3, raddr, waddr, wdata, rdata1, rdata2,
	    wen, ren);
   input	clk1, clk2;
   input [`SIZE-1:0] 	count1, count3, count5;
   output [`SIZE-1:0] count2, count4, count6;
   output 	combo1, combo2, combo3;
   input 	in1, in2;
   output 	async1, async2;
   input 	en1, en2;
   inout [`SIZE-1:0] 	inout1, inout2, inout3;
   input [3:0] 		raddr, waddr;
   input [`SIZE-1:0] 	wdata;
   output [`SIZE-1:0] 	rdata1, rdata2;
   input 		wen, ren;

   reg [`SIZE-1:0] 	count2, count4, count6;
   initial begin
      count2 = 0;
      count4 = 0;
      count6 = 0;
   end

   always @ (posedge clk2)
     begin
	count2 <= count1;
	count6 <= count5;
     end
   always @ (posedge clk1)
     begin
	count4 <= count3;
     end

   assign combo1 = (count1[0] == 1'b1);
   assign combo2 = (count3[1] != 1'b1);
   assign combo3 = (count5[2] == 1'b1);

   assign async1 = in1 & in2;
   assign async2 = in1 | in2;

   assign inout1 = en1 ? count1 : `SIZE'bz;
   assign inout2 = en2 ? count3 : `SIZE'bz;
   assign inout3 = en1 & en2 ? count5: `SIZE'bz;

   memory MEM1 (raddr, waddr, wdata, rdata1, wen, ren, clk1);
   memory MEM2 (raddr, waddr, wdata, rdata2, wen, ren, clk2);

endmodule

module memory(raddr, waddr, wdata, rdata, wen, ren, clk);
   input [3:0] 		raddr, waddr;
   input [`SIZE-1:0] 	wdata;
   output [`SIZE-1:0] 	rdata;
   input 		wen, ren, clk;

   reg [`SIZE-1:0] 	mem [0:15];

   always @ (posedge clk)
     begin
	if (wen)
	  mem[waddr] <= wdata;
     end
   reg [`SIZE-1:0] rdata;
   initial rdata = 0;
   always @ (clk or ren or mem[raddr])
     if (~clk & ren)
       rdata <= mem[raddr];
   
endmodule // memory
