#!/bin/sh
$MTI_HOME/bin/vlib work
$MTI_HOME/bin/vlog testmem.v carbon_top.v +define+TOP=reftop mem.v
$MTI_HOME/bin/vsim tb -do vsim.do -c -pli carbon_top.so

# Diff the dump files from MTI and Carbon
grep "@" write.mem1.hex | awk '{ print $2 }' > mti-dump.mem1.hex
grep "@" write.mem2.bin | awk '{ print $2 }' > mti-dump.mem2.bin
diff mti-dump.mem1.hex dump.mem1.hex >& dump.mem1.diff
if [ $? -ne 0 ]; then
    echo "Memory dump difference for mem1"
fi
diff mti-dump.mem2.bin dump.mem2.bin >& dump.mem1.diff
if [ $? -ne 0 ]; then
    echo "Memory dump difference for mem2"
fi
