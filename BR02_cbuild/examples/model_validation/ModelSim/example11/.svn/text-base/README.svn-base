This example demonstrates how to use the Carbon command to load, dump,
read, and write memory locations using ModelSim/Verilog. You will note
below that the example TCL script is almost identical to the the VHDL
Example10 above. The only real difference is that the path to the
Carbon Model is specified using Verilog notation instead of VHDL
notation.

1. Compile the Carbon Model:

$ $CARBON_HOME/bin/cbuild -q mem.v -directive mem.dir

2. Generate the wrapper:

$ $CARBON_HOME/bin/MVGenerate -s ModelSim libdesign.symtab.db

3. Build the shared library:

$ make -f Makefile.carbon.top
env -i LANG=C COMPILER_PATH=/carbon/product/latest/Linux/bin HOME=/home/carbon LD_LIBRARY_PATH=/carbon/product/latest/Linux/gcc/lib /carbon/product/latest/Linux/gcc345/bin/gcc -I. -I/carbon/product/latest/include -c -g -I/software/modeltech6.0/include -I/carbon/product/latest/include carbon_top.c
env -i LANG=C COMPILER_PATH=/carbon/product/latest/Linux/bin HOME=/home/carbon LD_LIBRARY_PATH=/carbon/product/latest/Linux/gcc/lib /carbon/product/latest/Linux/gcc345/bin/gcc -I. -I/carbon/product/latest/include -L/carbon/product/latest/Linux/gcc/lib -Wl,-rpath,/carbon/product/latest/Linux/gcc/lib -g -shared -o carbon_top.so carbon_top.o -L. -ldesign -lpliwrapper /carbon/product/latest/makefiles/LINK.script -L/carbon/product/latest/Linux/lib -L/carbon/product/latest/Linux/lib/ES3 -L/carbon/product/latest/Linux/lib/gcc3/shared -L/carbon/product/latest/Linux/lib/gcc3 -Wl,-rpath,/carbon/product/latest/Linux/lib/gcc3/shared -Wl,-dn -lcarbonvspx -lcarbonmem -Wl,-dy -lcarbon -Wl,-dn -lnffw -llmgr_nomt -lcrvs -lsb -lz -Wl,-dy
rm carbon_top.o

4. Compile the Carbon Model into the testbench:

$ vlib work
$ vlog testmem.v carbon_top.v +define+TOP=reftop mem.v

5. Simulate:

$ vsim tb -do vsim.do -c -pli carbon_top.soc

As noted above the only difference between the VHDL Example10 and this
Verilog example is that the path to the Carbon Model is specified
using the '.' separator (e.g. 'tb.u1').

The following commands show how to use the readmemX and dumpmemX
sub-commands:

# Load the Carbon memories
carbon tb.u1 readmemh top.MEM1.mem read.mem1.hex
carbon tb.u1 readmemb top.MEM2.mem read.mem2.bin

# Dump the Carbon memories
carbon tb.u1 dumpmemh top.MEM1.mem dump.mem1.hex
carbon tb.u1 dumpmemb top.MEM2.mem dump.mem2.bin

The following commands show how to use the memread sub-command:

# Read and verify a couple of memory locations
set addr 0xf
set data [carbon tb.u1 memread top.MEM1.mem $addr]
puts "top.MEM1.mem\[$addr\] = $data"
if { $data == "1234abcd" } { echo "PASS" } else { echo "FAIL" }

The following commands show how to use the memwrite sub-command:

# Write the same location and then re-read it
set data 0x12345678
carbon tb.u1 memwrite top.MEM1.mem $addr $data
set data [carbon tb.u1 memread top.MEM1.mem $addr]
puts "top.MEM1.mem\[$addr\] = $data"
