#!/bin/sh

# Run the simulation
$MTI_HOME/bin/vlib work
$MTI_HOME/bin/vlog testmulti.v carbon_multi.v +define+TOP=reftop multi.v
$MTI_HOME/bin/vsim tb -do vsim.do -c -pli carbon_multi.so

# Diff the dump files from ModelSim and Carbon for u1 and u2
grep "@" write.u2.mem1.hex | awk '{ print $2 }' > mti-dump.u2.mem1.hex
diff mti-dump.u2.mem1.hex dump.u1.mem1.hex
if [ $? -ne 0 ]; then
    echo "Memory dump difference for mem1"
fi
grep "@" write.u2.mem2.bin | awk '{ print $2 }' > mti-dump.u2.mem2.bin
diff mti-dump.u2.mem2.bin dump.u1.mem2.bin
if [ $? -ne 0 ]; then
    echo "Memory dump difference for mem2"
fi

# Diff the dump files from ModelSim and Carbon for u3 and u4
grep "@" write.u4.mem1.hex | awk '{ print $2 }' > mti-dump.u4.mem1.hex
diff mti-dump.u4.mem1.hex dump.u3.mem1.hex
if [ $? -ne 0 ]; then
    echo "Memory dump difference for mem1"
fi
grep "@" write.u4.mem2.bin | awk '{ print $2 }' > mti-dump.u4.mem2.bin
diff mti-dump.u4.mem2.bin dump.u3.mem2.bin
if [ $? -ne 0 ]; then
    echo "Memory dump difference for mem2"
fi
