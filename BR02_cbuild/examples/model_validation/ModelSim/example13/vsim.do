# ModelSim TCL script
# To run script:
#  vsim tb -do vsim.do -c -pli carbon_multi.so

# Setup Error Handler
onerror {resume}

# Load the u1 and u2 memories
carbon tb.u1 readmemh multi.MEM1.mem read.mem1.hex
carbon tb.u1 readmemb multi.MEM2.mem read.mem2.bin
mem load -infile read.mem1.hex -format hex tb.u2.MEM1.mem
mem load -infile read.mem2.bin -format bin tb.u2.MEM2.mem 

# Load the u3 and u4 memories
carbon tb.u3 readmemh multi.MEM1.mem read.mem1.hex
carbon tb.u3 readmemb multi.MEM2.mem read.mem2.bin
mem load -infile read.mem1.hex -format hex tb.u4.MEM1.mem
mem load -infile read.mem2.bin -format bin tb.u4.MEM2.mem

# Dump the Carbon wave forms
carbon tb.u1 dumpfile multi.u1.vcd
carbon tb.u1 dumpvars
carbon tb.u3 dumpfile multi.u3.vcd
carbon tb.u3 dumpvars

# Run the simulation for 500ns
run 500

# Dump the u1 and u2 memories
carbon tb.u1 dumpmemh multi.MEM1.mem dump.u1.mem1.hex
carbon tb.u1 dumpmemb multi.MEM2.mem dump.u1.mem2.bin
mem save -outfile write.u2.mem1.hex -format hex -wordsperline 1 tb.u2.MEM1.mem
mem save -outfile write.u2.mem2.bin -format bin -wordsperline 1 tb.u2.MEM2.mem

# Dump the u3 and u4 memories
carbon tb.u3 dumpmemh multi.MEM1.mem dump.u3.mem1.hex
carbon tb.u3 dumpmemb multi.MEM2.mem dump.u3.mem2.bin
mem save -outfile write.u4.mem1.hex -format hex -wordsperline 1 tb.u4.MEM1.mem
mem save -outfile write.u4.mem2.bin -format bin -wordsperline 1 tb.u4.MEM2.mem

# Read and verify a couple of u1 memory locations
set data [carbon tb.u1 memread multi.MEM1.mem f]
puts "multi.u1.MEM1.mem\[f\] = $data"
if { $data == "1234abcd" } { echo "PASS" } else { echo "FAIL" }

# Write the same location and then re-read it
carbon tb.u1 memwrite multi.MEM1.mem f 12345678
set data [carbon tb.u1 memread multi.MEM1.mem f]
puts "multi.u1.MEM1.mem\[f\] = $data"
if { $data == "12345678" } { echo "PASS" } else { echo "FAIL" }

# Read and verify a couple of u3 memory locations
set data [carbon tb.u3 memread multi.MEM1.mem f]
puts "multi.u3.MEM1.mem\[f\] = $data"
if { $data == "1234abcd" } { echo "PASS" } else { echo "FAIL" }

# Write the same location and then re-read it
carbon tb.u3 memwrite multi.MEM1.mem f 12345678
set data [carbon tb.u3 memread multi.MEM1.mem f]
puts "multi.u3.MEM1.mem\[f\] = $data"
if { $data == "12345678" } { echo "PASS" } else { echo "FAIL" }

# Goodbye
quit
