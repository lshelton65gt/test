#!/bin/sh
if [ "$CARBON_TARGET_ARCH" == "Linux64" ]; then
$CARBON_HOME/bin/cbuild -q multi.v -Wc -fPIC -o libmulti.a
else
$CARBON_HOME/bin/cbuild -q multi.v -o libmulti.a
fi
$CARBON_HOME/bin/MVGenerate -s ModelSim libmulti.symtab.db
make -f Makefile.carbon.multi
