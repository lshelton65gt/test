#!/bin/sh
if [ "$CARBON_TARGET_ARCH" == "Linux64" ]; then
$CARBON_HOME/bin/cbuild -q -vhdlTop ramtop ramrw.vhd ramvrw.vhd -Wc -fPIC ramtop.vhd
else
$CARBON_HOME/bin/cbuild -q -vhdlTop ramtop ramrw.vhd ramvrw.vhd ramtop.vhd
fi
$CARBON_HOME/bin/MVGenerate --simulator=ModelSim libdesign.symtab.db
make -f Makefile.carbon.ramtop
