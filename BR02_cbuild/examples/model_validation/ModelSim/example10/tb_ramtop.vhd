
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;


entity TB_RAMTOP is 
end TB_RAMTOP;


architecture behav of  TB_RAMTOP is
	
   component RAMTOP is 
   generic(
   	databits	: 	INTEGER := 44; 
   	adrbits		:	INTEGER := 5);         
    port(clk	: in STD_ULOGIC; 				
	 adr	: in STD_LOGIC_VECTOR ((adrbits-1) downto 0); 	
	 din	: in STD_LOGIC_VECTOR ((databits-1) downto 0); 
 	 wen	: in STD_ULOGIC;				
	 dout	: out STD_LOGIC_VECTOR ((databits-1) downto 0);
	 doutv	: out STD_LOGIC_VECTOR ((databits-1) downto 0)); 
   end component;


    constant databits : INTEGER := 44; 
    constant adrbits : INTEGER := 5;         
    -- constant maxcnt : integer : 2**adrbits; 
    constant maxcnt : integer := 32; 

    signal clk	 : STD_ULOGIC := '0'; 				
    signal adr	 : STD_LOGIC_VECTOR ((adrbits-1) downto 0); 	
    signal din	 : STD_LOGIC_VECTOR ((databits-1) downto 0); 
    signal wen	 : STD_ULOGIC := '0';				
    signal dout	 : STD_LOGIC_VECTOR ((databits-1) downto 0);
    signal doutv : STD_LOGIC_VECTOR ((databits-1) downto 0); 

    signal cnt        : integer := 0; 
    signal loop_cnt   : integer := 0; 

begin

   process
   begin
     wait for 1 ns;
     clk <= '1';
     wait for 1 ns;
     clk <= '0';
   end process;

   process (clk)
     -- variable myline : line;
   begin
     if clk'event and clk = '1' then
         if (cnt <  maxcnt) then
            cnt <= cnt + 1;
         else 
            cnt <= 0;
            wen <= not(wen);
            loop_cnt <= loop_cnt + 1;
     end if;
     end if;
   end process;

  process (clk)
  begin
     if clk'event and clk = '1' then
       adr <= conv_std_logic_vector(cnt, adrbits); 
       din <= conv_std_logic_vector((cnt + loop_cnt), databits); 
     end if;
  end process;

 u_ramtop : RAMTOP
          GENERIC MAP(databits => databits,
	              adrbits => adrbits)
          PORT MAP   (clk  => clk,
                      adr  => adr,
                      din  => din,
                      wen  => wen,
                      dout => dout,
                      doutv => doutv );


end behav;

