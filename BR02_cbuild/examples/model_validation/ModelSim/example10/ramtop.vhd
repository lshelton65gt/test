
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;


entity RAMTOP is 
   generic(
   	databits	: 	INTEGER := 44; 
   	adrbits		:	INTEGER := 5);         
    port(clk	: in STD_ULOGIC; 				
	 adr	: in STD_LOGIC_VECTOR ((adrbits-1) downto 0); 	
	 din	: in STD_LOGIC_VECTOR ((databits-1) downto 0); 
 	 wen	: in STD_ULOGIC;				
	 dout	: out STD_LOGIC_VECTOR ((databits-1) downto 0);
	 doutv	: out STD_LOGIC_VECTOR ((databits-1) downto 0)); 
end RAMTOP;


architecture SYNTH of  RAMTOP is
	
   component RAMRW is 
      generic(
   	databits	: 	INTEGER := 44; 
   	adrbits		:	INTEGER := 5);         
       port(clk	: in STD_ULOGIC; 				
   	 adr	: in STD_LOGIC_VECTOR ((adrbits-1) downto 0); 	
   	 din	: in STD_LOGIC_VECTOR ((databits-1) downto 0); 
    	 wen	: in STD_ULOGIC;				
   	 dout	: out STD_LOGIC_VECTOR ((databits-1) downto 0)); 
   end component;

   component RAMVRW is 
      generic(
   	databits	: 	INTEGER := 44; 
   	adrbits		:	INTEGER := 5);         
       port(clk	: in STD_ULOGIC; 				
   	 adr	: in STD_LOGIC_VECTOR ((adrbits-1) downto 0); 	
   	 din	: in STD_LOGIC_VECTOR ((databits-1) downto 0); 
    	 wen	: in STD_ULOGIC;				
   	 dout	: out STD_LOGIC_VECTOR ((databits-1) downto 0)); 
   end component;

   signal adrN	: STD_LOGIC_VECTOR ((adrbits-1) downto 0); 	
   signal dinN	: STD_LOGIC_VECTOR ((databits-1) downto 0); 
   signal doutN	: STD_LOGIC_VECTOR ((databits-1) downto 0); 
   signal doutvN	: STD_LOGIC_VECTOR ((databits-1) downto 0); 

begin

 adrN <= not(adr);
 dinN <= not(din);
 dout <= not(doutN);
 doutv <= not(doutvN);

 u_ramrw : RAMRW
          GENERIC MAP(databits => databits,
	              adrbits => adrbits)
          PORT MAP   (clk  => clk,
                      adr  => adrN,
                      din  => dinN,
                      wen  => wen,
                      dout => doutN );

 u_ramvrw : RAMVRW
          GENERIC MAP(databits => databits,
	              adrbits => adrbits)
          PORT MAP   (clk  => clk,
                      adr  => adrN,
                      din  => dinN,
                      wen  => wen,
                      dout => doutvN );


end SYNTH;

