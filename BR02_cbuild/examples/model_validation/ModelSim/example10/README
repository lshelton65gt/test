This example demonstrates how to use the Carbon command to load, dump,
read, and write memory locations using ModelSim/VHDL.

1. Compile the Carbon Model:

$ $CARBON_HOME/bin/cbuild -q -vhdlTop ramtop ramrw.vhd ramvrw.vhd ramtop.vhd

2. Generate the wrapper:

$ $CARBON_HOME/bin/MVGenerate --simulator=ModelSim libdesign.symtab.db

3. Build the shared library:

$ make -f Makefile.carbon.ramtop
env -i LANG=C COMPILER_PATH=/carbon/product/latest/Linux/bin HOME=/home/carbon LD_LIBRARY_PATH=/carbon/product/latest/Linux/gcc/lib /carbon/product/latest/Linux/gcc345/bin/gcc -I. -I/carbon/product/latest/include -c -g -I/software/modeltech6.0/include -I/carbon/product/latest/include carbon_ramtop.c
env -i LANG=C COMPILER_PATH=/carbon/product/latest/Linux/bin HOME=/home/carbon LD_LIBRARY_PATH=/carbon/product/latest/Linux/gcc/lib /carbon/product/latest/Linux/gcc345/bin/gcc -I. -I/carbon/product/latest/include -L/carbon/product/latest/Linux/gcc/lib -Wl,-rpath,/carbon/product/latest/Linux/gcc/lib -g -shared -o carbon_ramtop.so carbon_ramtop.o -L. -ldesign -lpliwrapper /carbon/product/latest/makefiles/LINK.script -L/carbon/product/latest/Linux/lib -L/carbon/product/latest/Linux/lib/ES3 -L/carbon/product/latest/Linux/lib/gcc3/shared -L/carbon/product/latest/Linux/lib/gcc3 -Wl,-rpath,/carbon/product/latest/Linux/lib/gcc3/shared -Wl,-dn -lcarbonvspx -lcarbonmem -Wl,-dy -lcarbon -Wl,-dn -lnffw -llmgr_nomt -lcrvs -lsb -lz -Wl,-dy
rm carbon_ramtop.o

4. Compile the Carbon Model into the testbench:

$ vlib work
$ vcom ramrw.vhd ramvrw.vhd ramtop.vhd carbon_ramtop.vhdl tb_ramtop.vhd

5. Simulate:

vsim tb_ramtop -do vsim.do -c

The simulation is run with the following vsim.do TCL script. Note the
uses of readmemh, readmemb, dumpmemh, dumpmemb, memread, and
memwrite. The following commands show how to use the readmemX and
dumpmemX sub-commands:

# define memory instances
set mem1 ramtop.u_ramrw.mem_process.memcore   
set mem2 ramtop.u_ramvrw.mem_process.memcore  

# Now run carbon /tb_ramtop/u_ramtop load/dump routines.
echo [carbon /tb_ramtop/u_ramtop readmemh   $mem1  read.mem1.hex]
echo [carbon /tb_ramtop/u_ramtop readmemb   $mem2  read.mem2.bin]
echo [carbon /tb_ramtop/u_ramtop dumpmemh $mem1  dump.mem1.hex]
echo [carbon /tb_ramtop/u_ramtop dumpmemb $mem2  dump.mem2.bin]

The following commands show how to use the memread sub-command:

# Read & verify a couple of memory locations
set addr 0xf
set data [carbon /tb_ramtop/u_ramtop memread $mem1 $addr]
echo "addr= " $addr
echo "data= " $data
if { $data == "76543210000" } { echo "PASS" } else { echo "FAIL" }

The following commands show how to use the memwrite sub-command:

# Write & read back one memory location
set data 65432123456
echo [carbon /tb_ramtop/u_ramtop memwrite $mem1 $addr $data]
