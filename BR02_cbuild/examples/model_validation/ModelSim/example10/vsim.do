# ModelSim TCL script
# To run script:
#    vsim tb_ramtop -do vsim.do -c

# Setup Error Handler
onerror { resume }

# Capture Waveforms
add wave -recursive *

# define memory instances
set mem1 ramtop.u_ramrw.mem_process.memcore   
set mem2 ramtop.u_ramvrw.mem_process.memcore  

# Now run carbon /tb_ramtop/u_ramtop load/dump routines.
echo [carbon /tb_ramtop/u_ramtop readmemh   $mem1  read.mem1.hex]
echo [carbon /tb_ramtop/u_ramtop readmemb   $mem2  read.mem2.bin]
echo [carbon /tb_ramtop/u_ramtop dumpmemh $mem1  dump.mem1.hex]
echo [carbon /tb_ramtop/u_ramtop dumpmemb $mem2  dump.mem2.bin]

# Read & verify a couple of memory locations
set addr 0xf
set data [carbon /tb_ramtop/u_ramtop memread $mem1 $addr]
echo "addr= " $addr
echo "data= " $data
if { $data == "76543210000" } { echo "PASS" } else { echo "FAIL" }

set addr [format "%x" [expr $addr + 1]]
set data [carbon /tb_ramtop/u_ramtop memread $mem1 $addr]
echo "addr= " $addr
echo "data= " $data
if { $data == "000ffffffff" } { echo "PASS" } else { echo "FAIL" }

# Write & read back one memory location
set data 65432123456
echo [carbon /tb_ramtop/u_ramtop memwrite $mem1 $addr $data]

set data [carbon /tb_ramtop/u_ramtop memread $mem1 $addr]
echo "addr= " $addr
echo "data= " $data

if { $data == "65432123456" } { echo "PASS" } else { echo "FAIL" }

# Run simulation
run 500

# Goodbye
quit

