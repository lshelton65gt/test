
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;


entity RAMVRW is 
   generic(
	databits	: 	INTEGER := 44; 
	adrbits		:	INTEGER := 5);         

    port(clk	: in STD_ULOGIC; 				
	 adr	: in STD_LOGIC_VECTOR ((adrbits-1) downto 0); 	
	 din	: in STD_LOGIC_VECTOR ((databits-1) downto 0); 
 	 wen	: in STD_ULOGIC;				
	 dout	: out STD_LOGIC_VECTOR ((databits-1) downto 0)); 
end RAMVRW;


architecture CARBONARCH of  RAMVRW is
	
   TYPE t_memcore is ARRAY (((2**adrbits)-1) downto 0) of STD_LOGIC_VECTOR((databits-1) DOWNTO 0);

begin


       mem_process: process(clk)
         variable memcore  	 : t_memcore;
         VARIABLE i_adr              : integer;
       begin
         if(clk'event and clk='0')then
            if(wen ='0')then
                  -- memcore(conv_integer(unsigned(adr))) <= din; 
                  i_adr := conv_integer(unsigned(adr));
                  memcore(i_adr) := din;
		  dout <= din;
	    elsif (wen = '1')  then
                  -- dout  <= memcore(conv_integer(unsigned(adr))); 
                  i_adr := conv_integer(unsigned(adr));
                  dout <= memcore(i_adr);
            end if;
         end if;
      end process;

end CARBONARCH;

