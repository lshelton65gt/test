#!/bin/sh
if [ "$CARBON_TARGET_ARCH" == "Linux64" ]; then
$CARBON_HOME/bin/cbuild -q -Wc -fPIC top.v
else
$CARBON_HOME/bin/cbuild -q top.v
fi
$CARBON_HOME/bin/MVGenerate -s ModelSim libdesign.symtab.db
make -f Makefile.carbon.top
