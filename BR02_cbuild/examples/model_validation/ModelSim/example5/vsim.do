# ModelSim TCL script
# To run script:
#   vsim tb -do vsim.do -c -pli carbon_top.so

# Setup Error Handler
onerror {resume}

# Add waveforms in ModelSim
add wave -recursive *

# Run for 500 ns
run 500

# Goodbye
quit

