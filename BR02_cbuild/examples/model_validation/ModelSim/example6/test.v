`define SIZE 8
module tb;

   reg [`SIZE-1:0]  count1;
   wire [`SIZE-1:0] count2_u1;
   wire [`SIZE-1:0] count2_u2;
   reg [`SIZE-1:0]  count3;
   wire [`SIZE-1:0] count4_u1;
   wire [`SIZE-1:0] count4_u2;
   reg [`SIZE-1:0]  count5;
   wire [`SIZE-1:0] count6_u1;
   wire [`SIZE-1:0] count6_u2;

   wire       combo1_u1;
   wire       combo1_u2;
   wire       combo2_u1;
   wire       combo2_u2;
   wire       combo3_u1;
   wire       combo3_u2;

   wire	     in1;
   wire	     in2;
   wire      async1_u1;
   wire      async1_u2;
   wire      async2_u1;
   wire      async2_u2;
   
   reg 	     clk1;
   reg 	     clk2;

   reg [31:0] count;
   initial begin
     count1 = 0;
     count3 = 0;
     count5 = 0;
     count = 0;
     clk1 = 0;
     clk2 = 0;
   end

   // Clock generation
   always
     clk1 = #10 ~clk1;
   always
     clk2 = #20 ~clk2;

   // Generate inputs
   always @ (posedge clk1)
     begin
       count <= count + 1;
     end
   assign in1 = count[1];
   assign in2 = count[2];


   // Counter seed
   always @ (posedge clk1)
     begin
	count1 <= count1 + 1;
     end

   // Add a multi clock flop sequence
   always @ (posedge clk2)
     begin
	count3 <= count2_u2;
	count5 <= count4_u2;
     end

   // Hier refs to connect counters
   assign u1.count1 = count1;
   assign u2.count1 = count1;
   assign u1.count3 = count3;
   assign u2.count3 = count3;
   assign u1.count5 = count5;
   assign u2.count5 = count5;
   assign count2_u1 = u1.F1.q;
   assign count2_u2 = u2.F1.q;
   assign count4_u1 = u1.F2.q;
   assign count4_u2 = u2.F2.q;
   assign count6_u1 = u1.F3.q;
   assign count6_u2 = u2.F3.q;

   top u1 (clk1, clk2, 
	   combo1_u1, combo2_u1, combo3_u1, in1, in2, async1_u1, async2_u1);
   
   reftop u2 (clk1, clk2, 
	      combo1_u2, combo2_u2, combo3_u2, in1, in2, async1_u2, async2_u2);

   always @ (posedge clk1)
     begin
	if (count2_u1 != count2_u2)
	  $display("ERROR: %t: count2 mismatch", $time);
	else
	  $display("%t: count2: Carbon: %x, Reference: %x",
		   $time, count2_u1, count2_u2);
	if (count4_u1 != count4_u2)
	  $display("ERROR: %t: count4 mismatch", $time);
	else
	  $display("%t: count4: Carbon: %x, Reference: %x",
		   $time, count4_u1, count4_u2);
	if (count6_u1 != count6_u2)
	  $display("ERROR: %t:%d: count6 mismatch, c: %x, m: %x", $time, $time, count6_u1, count6_u2);
	else
	  $display("%t: count6: Carbon: %x, Reference: %x",
		   $time, count6_u1, count6_u2);
	if (combo1_u1 != combo1_u2)
	  $display("ERROR: %t: combo1 mismatch", $time);
	else
	  $display("%t: combo1: Carbon: %x, Reference: %x",
		   $time, combo1_u1, combo1_u2);
	if (combo2_u1 != combo2_u2)
	  $display("ERROR: %t: combo2 mismatch", $time);
	else
	  $display("%t: combo2: Carbon: %x, Reference: %x",
		   $time, combo2_u1, combo2_u2);
	if (combo3_u1 != combo3_u2)
	  $display("ERROR: %t: combo3 mismatch", $time);
	else
	  $display("%t: combo3: Carbon: %x, Reference: %x",
		   $time, combo3_u1, combo3_u2);
	if (async1_u1 != async1_u2)
	  $display("ERROR: %t: async1 mismatch", $time);
	else
	  $display("%t: async1: Carbon: %x, Reference: %x",
		   $time, async1_u1, async1_u2);
	if (async2_u1 != async2_u2)
	  $display("ERROR: %t: async2 mismatch", $time);
	else
	  $display("%t: async2: Carbon: %x, Reference: %x",
		   $time, async2_u1, async2_u2);
     end // always @ (posedge clk1)

endmodule // tb


