ifeq ($(CARBON_HOME),)
  $(error You must set CARBON_HOME to the Carbon installation directory)
endif

ifeq ($(MAXSIM_HOME),)
  $(error You must set MAXSIM_HOME to the SOC Designer installation directory)
endif

# This defines some Carbon Makefile macros to help with compiling
include $(CARBON_HOME)/makefiles/Makefile.common

# Parse SoCDesigner version
MAXSIM_VERSION       := $(shell awk '/MAXSIM_VERSION / {print $$3}' $(MAXSIM_HOME)/include/mxsim_revision.h)
MAXSIM_MAJOR_VERSION  = $(word 1, $(subst ., ,$(MAXSIM_VERSION)))
export MAXSIM_MAJOR_VERSION

CXX = $(CARBON_HOME)/bin/carbon_sdcxx
export CXX

CXXFLAGS = -I. -I$(MAXSIM_HOME)/include -I$(CARBON_HOME)/include -DSC_INCLUDE_FX 
LDFLAGS  = -shared -lcarbonxsocvsp$(MAXSIM_MAJOR_VERSION) $(CARBON_LIB_LIST)

run_mx: libpcieFlop.mx.so libTestComponent.so maxlib.conf
	$(MAXSIM_HOME)/bin/sdsim -s 5000 pcieTb.mxp

libdesign.a: pcieFlop.v
	$(CARBON_CBUILD) pcieFlop.v -o libdesign.a

mx.pcieFlop.cpp : libdesign.a pcieFlop.ccfg
	$(CARBON_HOME)/bin/carbon carmgr -q pcieFlop.ccfg

libpcieFlop.mx.so : mx.pcieFlop.cpp Makefile.pcieFlop.mx
	make -f Makefile.pcieFlop.mx

libTestComponent.so : TestComponent.cpp
	$(CXX) $(CXXFLAGS) TestComponent.cpp -o $@ $(LDFLAGS) 

maxlib.conf : maxlib.pcieFlop.conf maxlib-$(CARBON_HOST_ARCH).conf $(CARBON_HOME)/lib/MaxLib$(MAXSIM_MAJOR_VERSION)/$(CARBON_TARGET_ARCH)/etc/maxlib.conf
	cat $^ | sed -e 's@\.\./lib@$(CARBON_HOME)/lib/MaxLib$(MAXSIM_MAJOR_VERSION)/$(CARBON_TARGET_ARCH)/lib@' > maxlib.conf

.PHONY: clean 
clean: 
	rm -f *.o *.so
	rm -f carbon_make_complete
	rm -f libdesign* Makefile.pcieFlop.mx* mx.* maxlib.pcieFlop* *.log pcieTb.css pcieFlop.xpm pcieTb.~bc pcieTb.opt *.cse maxlib.conf
	rm -rf .carbon.libdesign
