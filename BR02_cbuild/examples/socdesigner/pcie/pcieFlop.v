// This module has two 1-lane PCI Express ports. There is a data is passed in both
// directions through a simple flip-flop.

module pcieFlop (
                 pcieRst,

                 pcieAClk,
                 pcieATx,
                 pcieARx,

                 pcieATxElecIdle,
                 pcieARxElecIdle,

                 pcieBClk,
                 pcieBTx,
                 pcieBRx,

                 pcieBTxElecIdle,
                 pcieBRxElecIdle
                 );

   input        pcieRst;

   input        pcieAClk;
   output [9:0] pcieATx;
   input [9:0] 	pcieARx;
   output       pcieATxElecIdle;
   input        pcieARxElecIdle;

   input 	pcieBClk;
   output [9:0] pcieBTx;
   input [9:0] 	pcieBRx;
   output       pcieBTxElecIdle;
   input        pcieBRxElecIdle;

   reg [9:0] 	pcieATx;
   reg 		pcieATxElecIdle;
   
   reg [9:0] 	pcieBTx;
   reg 		pcieBTxElecIdle;


   // Pass Data from Port B's Input to Port A's Output, using Port B's clock
   always @ (posedge pcieBClk or posedge pcieRst)
     begin
        if (pcieRst == 1)
          begin
             pcieATx         <= 10'b0;
             pcieATxElecIdle <= 1'b1;
          end
        else
          begin
             pcieATx         <= pcieBRx;
             pcieATxElecIdle <= pcieBRxElecIdle;
          end
     end
   
   // Pass Data from Port A's Input to Port B's Output, using Port A's clock
   always @ (posedge pcieAClk or posedge pcieRst)
     begin
        if (pcieRst == 1)
          begin
             pcieBTx         <= 10'b0;
             pcieBTxElecIdle <= 1'b1;
          end
        else
          begin
             pcieBTx         <= pcieARx;
             pcieBTxElecIdle <= pcieARxElecIdle;
          end
     end
   
endmodule


