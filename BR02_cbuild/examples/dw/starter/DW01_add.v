/****************************************************************************

   --- DUMMY DW01_add module ----


 ****************************************************************************

 -- ** Description : This module matches the port list of the DW01_add module.
 --                  It does NOT implement any of the DW01_add functionality.
 --                  It is needed in order to compile using the 
 --                  substituteModule directive (see file DW_all.dir)

 -- ** Input       :    A,B,CI
 -- ** Output      :   SUM , CO 
****************************************************************************/
module DW01_add(A, B, CI, SUM, CO);

   parameter width = 32;

   input  [(width - 1):0]   A;
   input  [(width - 1):0]   B;
   input 		   CI;
   output [(width - 1):0] SUM;
   output 		   CO;

   reg    [(width - 1):0] SUM;
   reg 		           CO;

 initial
  begin
  $display("\n*****************  E R R O R  *************************");
  $display(  "****** This module (DW01_add) is a DUMMY Module. ******");
  $display(  "*******************************************************");

  $display("\n            It should never be executing \n");

  $display("The substituteModule directive in directives file DW_all.dir ");
  $display("is supposed to replace this DW01_add module with the DW01_add_carbon module");
  $display("something may have gone wrong with the -directives command, or");
  $display("the DW_all.dir file may be missing\n\n");

  CO  = 1'b1;
  SUM = 'hffffffff;
  end

endmodule

