// **************************************************************
//	This "C" testbench demostrates an example usage of the 
//	            DW01_add    Designware component
// **************************************************************
//
//  the verilog code looks like this:
//
//        DW01_add #(32) u1 (A, B, CI, ADD_out, CO_ADD_out);
//

#include "libdesign.h"  // defines the carbon_design_create() call for this simulation
#include <cstdio>

int main()
{ 
  const CarbonUInt32 ONE = 0x1;
  const CarbonUInt32 ZERO = 0x0; 
  CarbonUInt32 edge;

  CarbonObjectID *obj = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  
  CarbonNetID *clk = carbonFindNet(obj, "top.clk");
  CarbonNetID *rst_n = carbonFindNet(obj, "top.rst_n");

  CarbonTime ctime = 0LL;  // CarbonTime is a 64 bit integer !

    printf("cds_main: Asserting reset_n at time %d\n", ctime);
    carbonDeposit(obj, clk, &ZERO, 0);
    carbonDeposit(obj, rst_n, &ZERO, 0);
    carbonSchedule(obj, ctime);

    ctime = 10;
    printf("cds_main: clearing reset_n at time %d\n", ctime);
    carbonDeposit(obj, clk, &ONE, 0);
    carbonDeposit(obj, rst_n, &ONE, 0);
    carbonSchedule(obj, ctime);

    edge = 0;
    for (ctime= 15; ctime < 100; ctime += 5)
    {
      carbonDeposit(obj, clk, &edge, 0);
      carbonSchedule(obj, ctime);
      edge = edge ^ 1;               // XOR: edge toggles between 0 and 1
    }
    
  carbonDestroy(&obj);
  
}

