//
// small testbench for DW01_add example
//
module top (clk, rst_n);
 
	input clk, rst_n;

	reg  [31:0] A, B;
	reg  CI;
	wire [31:0] ADD_out;
        wire CO_ADD_out;

        integer counter;

   DW01_add #(32) u1 (A, B, CI, ADD_out, CO_ADD_out);


 // ------------------------------------------------

 initial
  begin
  $display("\n\n********************************************");
  $display(    "Carbon designware DW01_add example testbench");
  $display(    "********************************************\n\n");

  counter = 0;
  A = 0;
  B = 30;
  end


 // ------------------------------------------------

 always @(posedge clk)
   begin
    if (counter == 0)
       begin
       $display("\n                 Time          A          B     CI      ADD_out      CO       counter");
       $display(  "                 ----         ---        ---    --      -------      --       -------");
       end

   counter = counter + 1;
   
   $display("%t  %d  %d     %d  %d        %d  %d", 
                 $time, A, B, CI, ADD_out, CO_ADD_out, counter);
   end 

 // ------------------------------------------------

 always @(posedge clk)
   begin

    if (counter == 2)  A = 204;
    if (counter == 4) CI = 1'b1;
   end

endmodule
