//
// minimal testbench for DW01_add example
//
//   this module 1. Instantiates a DW01_add
//               2. Sets the inputs to known values
//               3. Displays both inputs and outputs on each posedge clk
//               4. changes the CI input AFTER the first posedge clk
//
//   the cbuild command line -testdriver and -testdriverVectorCount 8
//   make sure that only 2 posedge clocks are generated for the sim.
//
module top(clk) ;

        input clk;

	reg  [31:0] A, B;
	reg  CI;
	wire [31:0] ADD_out;
        wire CO_ADD_out;


   DW01_add #(32) u1 (A, B, CI, ADD_out, CO_ADD_out);

 // ------------------------------------------------------------

 initial
  begin
  A  = 25;
  B  = 30;
  CI =  0;

  $display("\n\n********************************************");
  $display(    "Carbon designware DW01_add minimal example ");
  $display(    "********************************************\n\n");

  $display("CLK   [driven by -testdriver vectors]");
  $display("---");
  end

 // ------------------------------------------------------------

 always @(posedge clk)
   begin
   $display("         A: %0d    B: %0d    CI: %0d         A+B+CI = %0d     CO = %0d",
                      A, B, CI, ADD_out, CO_ADD_out);
   CI = 1;
   end

endmodule

// End-of-file
