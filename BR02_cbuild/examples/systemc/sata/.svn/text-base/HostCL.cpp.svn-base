// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "HostCL.h"
#include <iomanip>

// Test sequence
void HostCL :: test()
{
  // Perform a Identify Packet Command
  CarbonUInt16 info[256];
  bool res = identifyPacketDeviceCmd(info);

  // If the command was successful, check the Device Info returned
  if(res) {
    
    // Check Device Info
    cout << "Checking Device Info...." << endl;

    // Generate Expected Payload with same seed as in device
    vector<CarbonUInt16> expPayload(256);
    srand(1);
    for (int i = 0; i < 128; i++) {
      CarbonUInt32 val = random();
      expPayload[i*2]   = val;
      expPayload[i*2+1] = val >> 16;
    }
    
    // Now check the expected payload against the received payload
    int errors = 0;
    for (int i = 0; i < 256; i++) {
      if(expPayload[i] != info[i]) {
        cout << "Error: word " << dec << setw(3) << i
             << " Expected = 0x" << hex << setfill('0') << setw(5) << expPayload[i] << " Actual = 0x" << setw(5) << info[i] << endl;
        errors++;
      }
    }

    if(errors == 0) cout << "Test Passed!" << endl;
    else cout << "Test Failed!" << endl;
  }

  // Stop the simulation
  sc_stop();
}

bool HostCL :: identifyPacketDeviceCmd(CarbonUInt16* info)
{
  // Starting Register - Host to Device
  cout << "**** HOST: Starting 'IDENTIFY PACKET DEVICE' Command." << endl;
  
  // Write to command register to start the command
  tlPort->writeReg(SATA_REG_COMMAND, 0xA1);
  
  // Wait until we received the Data FIS. This will include receiving a PIO Setup FIS
  // followed by a Data FIS.
  if(!tlPort->pollReg(SATA_REG_STATUS, 0, 0x88, sc_time(80000, SC_NS))) {
    cout << "Timed out when polling status at time: " << sc_time_stamp() << endl;
    return false;
  }
  
  cout << "**** HOST: Received Device Information." << endl;
  
  // Copy Device Info
  for(int i = 0; i < 256; i++)
    info[i] = tlPort->readReg(SATA_REG_DATA_REG);
  
  return true;
}
