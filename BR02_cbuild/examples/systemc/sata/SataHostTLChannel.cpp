// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "SataHostTLChannel.h"
#include "xactors/sata/carbonXSataTLTrans.h"

SataHostTLChannel::SataHostTLChannel(sc_module_name _name) :
  sc_channel(_name),
  startTransPort("startTransPort"),
  mLbaHigh(0),
  mLbaMid(0),
  mLbaLow(0),
  mDevice(0),
  mSectorCnt(0),
  mCommand(0),
  mStatus(0),
  mFeatures(0),
  mErrors(0),
  mDeviceCtrl(0),
  mAltStatus(0),
  mEStatus(0),
  mTxTransCount(0),
  mRxTransCount(0)
{
  mXtorName = std::string(name());
}


// Transport Layer Interface
void SataHostTLChannel::writeReg(CarbonUInt8 addr, CarbonUInt16 value) {
  switch(addr) {
  case SATA_REG_LBA_HIGH :
    mLbaHigh = value;
    break;
  case SATA_REG_LBA_MID :
    mLbaMid = value;
    break;
  case SATA_REG_LBA_LOW :
    mLbaLow = value;
    break;
  case SATA_REG_DEVICE :
    mDevice = value;
    break;
  case SATA_REG_SECTOR_CNT :
    mSectorCnt = value;
    break;
  case SATA_REG_COMMAND :
    {
      mCommand = value;

      // Initialize a Register Host to Device FIS
      CarbonXSataTLTransRegHost2DevFis regHost2DevFis;
      regHost2DevFis.C = 1;
      regHost2DevFis.Command        = mCommand;
      regHost2DevFis.Device         = mDevice;
      regHost2DevFis.PmPort         = 0;
      regHost2DevFis.Command        = mCommand;
      regHost2DevFis.Control        = mDeviceCtrl;
      regHost2DevFis.LbaLow         = mLbaLow & 0xff;
      regHost2DevFis.LbaLowExp      = (mLbaLow >> 8) & 0xff;
      regHost2DevFis.LbaMid         = mLbaMid & 0xff;
      regHost2DevFis.LbaMidExp      = (mLbaMid >> 8) & 0xff;
      regHost2DevFis.LbaHigh        = mLbaHigh & 0xff;
      regHost2DevFis.LbaHighExp     = (mLbaHigh >> 8) & 0xff;
      regHost2DevFis.Device         = mDevice;
      regHost2DevFis.Features       = mFeatures;
      regHost2DevFis.FeaturesExp    = (mFeatures >> 8) & 0xff;
      regHost2DevFis.SectorCount    = mSectorCnt & 0xff;
      regHost2DevFis.SectorCountExp = (mSectorCnt >> 8) & 0xff;

      // Crate a Link Layer Transaction
      CarbonXSataLLTrans *trans =
        carbonXSataTLTransCreateRegHost2DevFisLLFrame(&regHost2DevFis);

      // Send it off over the port
      cout << "**** " << mXtorName << ": Sending Register Host2Device FIS." << endl;
      startTransPort->startNewTransaction(trans);
      wait(mTransactionDoneEv); // Wait for completion of transaction (this is optional)
    
      // Set BSY to one so we can start polling for it before the device had the chance
      // to update it.
      mStatus |= 0x80;
    }
    break;
  case SATA_REG_FEATURES :
    mFeatures = value;
    break;
  case SATA_REG_DATA_REG :

    // Save Data in transmitt queue and decrement the transfer count 
    mTxDataQueue.push_back(value);
    mTxTransCount -=2;
    
    // If all data for the transfer has been collected, send it off
    if(mTxTransCount == 0) {

      // If size is not an even number of dwords, add an extra 0 to the end
      if( (mTxDataQueue.size() % 2) != 0) mTxDataQueue.push_back(0);

      int size = mTxDataQueue.size();
      CarbonUInt32 data[size/2];
      for(CarbonUInt32 i = 0; i < size/2; i++) {
        data[i]   = mTxDataQueue.front();
        mTxDataQueue.pop_front();
        data[i] |= CarbonUInt32(mTxDataQueue.front()) << 16;;
        mTxDataQueue.pop_front();
      }
      
      // Send the data FIS
      CarbonXSataTLTransDataFis dataHost2DevFis;
      dataHost2DevFis.dataSize  = size/2;
      dataHost2DevFis.data      = data;
      dataHost2DevFis.PmPort    = 0;
      CarbonXSataLLTrans* trans = carbonXSataTLTransCreateDataFisLLFrame(&dataHost2DevFis);
      startTransPort->startNewTransaction(trans);
    }
    break;
  case SATA_REG_DEVICE_CTRL :
    mDeviceCtrl = value;
    // Setup a Host2Device FIS transmission here
    // ...
    break;
  default :
    break;
  }
}

CarbonUInt16 SataHostTLChannel::readReg(CarbonUInt8 addr)
{
  switch(addr) {
  case SATA_REG_LBA_HIGH :
    return mLbaHigh;
    break;
  case SATA_REG_LBA_MID :
    return mLbaMid;
    break;
  case SATA_REG_LBA_LOW :
    return mLbaLow;
    break;
  case SATA_REG_DEVICE :
    return mDevice;
    break;
  case SATA_REG_SECTOR_CNT :
    return mSectorCnt;
    break;
  case SATA_REG_STATUS :
    return mStatus;
    break;
  case SATA_REG_ERROR :
    return mErrors;
    break;
  case SATA_REG_DATA_REG :
    if(!mRxDataQueue.empty()) {
      CarbonUInt16 data = mRxDataQueue.front();
      mRxDataQueue.pop_front();
      return data;
    }
    else return 0;
    break;
  case SATA_REG_ALT_STATUS :
    return mAltStatus;
    break;
  default :
    return 0;
    break;
  }
}
  
bool SataHostTLChannel::pollReg(CarbonUInt8 addr, CarbonUInt16 exp_value, CarbonUInt16 mask, const sc_time& timeout)
{
  sc_time my_timeout(timeout);
  
  // Loop until the masked bits of read register matches the expected value 
  while( (readReg(addr) & mask) !=  (exp_value & mask) ) {
    sc_time then(sc_time_stamp()); 
    wait(timeout, mReceivedUpdateEv);

    // If a timeout was detected, report failure
    if(timed_out()) return false;
    else {
      sc_time now(sc_time_stamp());

      // Decrement the timeout value with the time it took for the last wait.
      my_timeout = my_timeout - (now - then);
    }
  }

  return true; // Success
}

sc_event& SataHostTLChannel::receivedUpdate()
{
  return mReceivedUpdateEv;
}

// Response Interface
void SataHostTLChannel::setDefaultResponse(CarbonXSataLLTrans* trans)
{
}

void SataHostTLChannel::setResponse(CarbonXSataLLTrans* trans)
{
  if (carbonXSataLLTransGetType(trans) == CarbonXSataLLTrans_Frame)
  {
    if (carbonXSataLLTransGetTransStatus(trans) ==
        CarbonXSataLLTransStatus_Ok)
    {
      switch ( carbonXSataTLTransGetFisType(trans) )
      {
      case CarbonXSataTLTrans_RegHost2DevFis:
      {
        cout << "**** " << mXtorName <<
          ": Register FIS - Host to Device received."
                        << endl;
        SC_REPORT_ERROR(mXtorName.c_str(), "Host received a Register Host To Device FIS");
        break;
      }
      case CarbonXSataTLTrans_RegDev2HostFis:
      {
        cout << "**** " << mXtorName <<
          ": Register FIS - Device to Host received."
                        << endl;
        break;
      }
      case CarbonXSataTLTrans_SetDevBitsFis:
      {
        cout << "**** " << mXtorName <<
          ": Set Device Bits FIS received." << endl;

        // Implement Functionality for this FIS type here
        // ...
        break;
      }
      case CarbonXSataTLTrans_DmaActFis:
      {
        cout << "**** " << mXtorName <<
          ": DMA Activate FIS received." << endl;

        // Implement Functionality for this FIS type here
        // ...
        break;
      }
      case CarbonXSataTLTrans_DmaSetupFis:
      {
        cout << "**** " << mXtorName <<
          ": DMA Setup FIS received." << endl;

        // Implement Functionality for this FIS type here
        // ...
        break;
      }
      case CarbonXSataTLTrans_PioSetupFis:
      {
        cout << "**** " << mXtorName <<
          ": PIO Setup FIS received." << endl;
        
        // Extract PioSetupStructure from Link Layer FIS
        CarbonXSataTLTransPioSetupFis* fis =
          carbonXSataTLTransGetPioSetupFis(trans);
                                                
        // Update Device Status Register and store away E_Status
        // so the status register can be updated upon reception of
        // Data FIS
        mDevice     = fis->Device;
        mLbaHigh    = fis->LbaHigh     | (CarbonUInt32(fis->LbaHighExp)      << 8); 
        mLbaMid     = fis->LbaMid      | (CarbonUInt32(fis->LbaMidExp)       << 8); 
        mLbaLow     = fis->LbaLow      | (CarbonUInt32(fis->LbaLowExp)       << 8); 
        mSectorCnt  = fis->SectorCount | (CarbonUInt32(fis->SectorCountExp)  << 8);
        mErrors     = fis->Error;
        mStatus     = fis->Status;
        mEStatus    = fis->E_Status;
        
        // Load Countdown Registers depending on Data direction
        if(fis->D == 1) mRxTransCount = fis->TransCount;
        else            mTxTransCount = fis->TransCount;

        // Delete Transaction Structure
        carbonmem_free(fis);

        break;
      }
      case CarbonXSataTLTrans_DataFis:
      {
        cout << "**** " << mXtorName <<
          ": Data FIS received." << endl;

        // Extract PioSetupStructure from Link Layer FIS
        CarbonXSataTLTransDataFis* fis =
          carbonXSataTLTransGetDataFis(trans);
        
        // Copy Data into the Data Queue (Should perhaps clear queue first, not sure)
        for(CarbonUInt32 i = 0; i < mRxTransCount; i++) {
          mRxDataQueue.push_back((fis->data[i/2] >> (16 * (i&1)) & 0xffff));
        }

        // Update Device Status Register with the E_Status saved when
        // PIO Setup FIS was received
        mStatus = mEStatus;

        // Delete Transaction Structure
        carbonmem_free(fis->data);
        carbonmem_free(fis);
        break;
      } 
      case CarbonXSataTLTrans_BistActFis:
      {
        SC_REPORT_WARNING(mXtorName.c_str(),
                          "Unsupported BIST Activate FIS received");
        break;
      }
      case CarbonXSataTLTrans_ReservedFis:
      {
        SC_REPORT_WARNING(mXtorName.c_str(),
                          "Unsupported Reserved FIS received");
        break;
      }
      case CarbonXSataTLTrans_VendorSpecFis:
      {
        SC_REPORT_WARNING(mXtorName.c_str(),
                          "Unsupported Vendor Specific FIS received");
        break;
      }
      default:
      {
        SC_REPORT_ERROR(mXtorName.c_str(),
                        "Invalid FIS received");
      }
      }
      mReceivedUpdateEv.notify(SC_ZERO_TIME);
    }
    else
    {
      SC_REPORT_ERROR(mXtorName.c_str(), "Error in FIS reception");
    }
  }
}

// Report Interface
void SataHostTLChannel::reportTransaction(CarbonXSataLLTrans* trans)
{
  if (carbonXSataLLTransGetType(trans) == CarbonXSataLLTrans_Frame)
  {
    if (carbonXSataLLTransGetTransStatus(trans) ==
        CarbonXSataLLTransStatus_Ok)
    {
      cout << "**** " << mXtorName << ": FIS transmission successful." << endl;
    }
    else
    {
      SC_REPORT_ERROR(mXtorName.c_str(), "Error in FIS transmission");
    }
  }
  carbonXSataLLTransDestroy(trans);
  mTransactionDoneEv.notify(SC_ZERO_TIME);
}
  
