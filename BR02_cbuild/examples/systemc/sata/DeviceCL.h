// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __DeviceCL_h__
#define __DeviceCL_h__

#include <string>
#include <queue>
#include "SataDeviceTLIF.h"

//! Device Command Layer
/*! Executes the commands given by the Device Transport Layer
 */
class DeviceCL : public sc_module
{
public:
  SC_HAS_PROCESS(DeviceCL);
  
  // Device transaction ports
  sc_port<SataDeviceTLIF>    tlPort;
  
  DeviceCL(sc_module_name name) : sc_module(name)
  {
    SC_THREAD(fsm);
  }
  
  //! Statemachine implementing the Sata Device
  void fsm();
};

#endif
