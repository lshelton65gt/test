// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "xactors/sata/carbonXSataTLTrans.h"
#include "DeviceCL.h"

using namespace std;

// Device transport layer FSM
void DeviceCL :: fsm()
{
  while(1) {
    // Wait for the next command
    wait(tlPort->receivedCmd());

    CarbonUInt8 command = tlPort->readReg(SATA_REG_COMMAND);
    
    switch (command)
    {
    case 0xA1: // IDENTFY PACKET DEVICE Command received
    {
      cout << "**** DEVICE: Received 'IDENTIFY PACKET DEVICE'"
           << " Command. Sending Device Information (random)."
           << endl;
      
      // 1. Transmit PIO Setup FIS
      tlPort->writeReg(SATA_REG_STATUS, 0x48); // Set BSY 1, and DRDY to 1
      tlPort->PIOSetup(true, true, 256 * 2, 0x40); 
      
      // 2. Transmit device info as Data FIS. Transport Layer will
      //    send the FIS as soon as enough data for the transfer has been
      //    provided.
      srand(1);
      for (int i = 0; i < 256/2; i++) {
        CarbonUInt32 deviceInfo = random();
        tlPort->writeReg(SATA_REG_DATA_REG, deviceInfo & 0xffff);
      tlPort->writeReg(SATA_REG_DATA_REG, (deviceInfo >> 16) & 0xffff);
      }
      
      break;
    }
    default:
    { 
      SC_REPORT_WARNING("DEVICE", "Command not handled.");
      break;
    }
    }
    
  }
}

