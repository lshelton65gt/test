// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef _SataDeviceTLChannel_
#define _SataDeviceTLChannel_

#include "systemc.h"
#include "xactors/systemc/CarbonXSCSataLLIfs.h"
#include "SataDeviceTLIF.h"
#include <deque>

/*!
  \brief SATA Device Transaction Layer Channel
  This module/channel abstracts out the device transaction layer by implementing
  a register access interface to the shadow register structure.
  The interface also has methods to trigger transmission of FIS'es to
  the host.

  The class is not complete and is just implemented enough to support the 
  'IDENTIFY PACKET DEVICE' command example, but it should be easily expandable
  to support all transaction level functionality.
*/
class SataDeviceTLChannel : public sc_channel, 
                      public virtual CarbonXSCSataResponseIF, 
                      public virtual CarbonXSCSataReportTransactionIF,
                      public virtual SataDeviceTLIF
{
public:

  // Transaction Port
  sc_port<CarbonXSCSataStartTransactionIF> startTransPort;

  // Constructor
  SataDeviceTLChannel(sc_module_name name);

  /*!
    \brief Write Register
    Write a value to the specified register. 
    If the data register is written and a PIO Setup FIS or a 
    DMA Setup FIS is previously sent, the data is added to a transmitt
    queue. When enough data is written to satisfy the requirement
    for the prevoius sent FIS a Data FIS will be sent to the host
    containing the data in the queue. The queue will be cleared at
    that time.
    \param addr Register Address to write
    \param value Value to be written 
  */
  void writeReg(CarbonUInt8 addr, CarbonUInt16 value);
  
  /*!
    \brief Read Shadow Register
    Reads the current value of the given register.
    \param addr Register Address to read
    \return value of read register.
  */
  CarbonUInt16 readReg(CarbonUInt8 addr);

  /*!
    \brief send a PIO Setup FIS
    \param I Interupt bit
    \param D Direction bit
    \param transCount Number of bytes to send/receive in the following FIS
    \param eStatus End Status field
  */
  bool PIOSetup(bool I, bool D, CarbonUInt32 transCount, CarbonUInt8 eStatus);

  /*!
    \brief send a DMA Setup FIS
    \param I Interupt bit
    \param D Direction bit
    \param transCount Number of bytes to send/receive in the following FIS
    \param dmaBufIdHi DMA Buffer ID High field
    \param dmaBufIdLo DMA Buffer ID Low field
    \param dmaBufOffset DMA Buffer Offset field
  */
  bool DMASetup(bool I, bool D, CarbonUInt32 transCount, CarbonUInt32 dmaBufIdHi, CarbonUInt32 dmaBufIdLo, CarbonUInt32 dmaBufOffset);

  //! Send an DMA Activate FIS
  bool DMActivate();

  /*!
    \brief Send a Set Device Bits FIS
    \param I Interupt bit
    \param N Notification bit
  */
  bool setDeviceBits(bool I, bool N);

  /*!
    \brief Send a Register Device to Host FIS
    Send an FIS with all the device shadow register values.
  */
  bool updateHostShadowReg();

  /*! 
    \brief Received Command Event
    \return event that will be notified whenever an Reg Host2Device FIS with C = 1 is received.
  */
  sc_event& receivedCmd();
  
  /*!
    \brief Received Data Event
    \return event that will be notified whenever a Data FIS with is received.
  */
  sc_event& receivedData();

private:

  //! Set Default Response Callback method
  //! \param trans pointer to the Link Layer transaction struture
  void setDefaultResponse(CarbonXSataLLTrans* trans);

  //! Set Response Callback method
  //! \param trans pointer to the Link Layer transaction struture
  void setResponse(CarbonXSataLLTrans* trans);

  //! Report Transaction Callback method
  //! \param trans pointer to the Link Layer transaction struture
  void reportTransaction(CarbonXSataLLTrans* trans);

  // Shadow Registers
  CarbonUInt16 mLbaHigh;
  CarbonUInt16 mLbaMid;
  CarbonUInt16 mLbaLow;
  CarbonUInt16 mDevice;
  CarbonUInt16 mSectorCnt;
  CarbonUInt16 mCommand;
  CarbonUInt32 mStatus;
  CarbonUInt16 mFeatures;
  CarbonUInt16 mErrors;
  CarbonUInt16 mDeviceCtrl;
  CarbonUInt16 mAltStatus;
  
  // Data Transmitt Queue
  std::deque<CarbonUInt16> mTxDataQueue;

  // Data Receive Queue
  std::deque<CarbonUInt16> mRxDataQueue;

  // Member Variables
  std::string  mXtorName;
  CarbonUInt32 mEStatus;
  CarbonUInt32 mTxTransCount;
  CarbonUInt32 mRxTransCount;
  bool         mLastTransSuccess;

  // Events
  sc_event mReceivedCmdEv;
  sc_event mReceivedDataEv;
  sc_event mTransactionDoneEv;
};

#endif
