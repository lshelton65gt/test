// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __HostCL_h__
#define __HostCL_h__

#include <string>
#include <queue>
using namespace std;

#include "SataHostTLIF.h"

//! Host Command Layer and test
/*! Implements the example command + a test
 */
class HostCL : public sc_module
{
public:
  SC_HAS_PROCESS(HostCL);
  
  // Host transaction ports
  sc_port<SataHostTLIF>    tlPort;
  
  HostCL(sc_module_name name) : sc_module(name)
  {
    SC_THREAD(test);
  }
  
  //! Test Thread
  /*!
    Executed the command and checks the result
  */
  void test();
  
  // Command Layer Methods

  //! Perform Identify Packet Device Command
  /*! 
    Performs the command end sets info with the returned
    device into.
    \retval true if command was successful
    \retval false if command was unsuccessful
  */
  bool identifyPacketDeviceCmd(CarbonUInt16* info);
};

#endif
