// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __TestBench_h__
#define __TestBench_h__

#include "HostCL.h"
#include "DeviceCL.h"
#include "xactors/systemc/CarbonXSCSataLL.h"
#include "SataHostTLChannel.h"
#include "SataDeviceTLChannel.h"
#ifndef _NO_CARBON_MODEL
#include "libTestGlue.systemc.h"
#else
#include "TestGlue.h"
#endif

// Top level testbench comprising of transactors and glue and connecting
// test layer
class TestBench : public sc_module
{
public:
  SC_HAS_PROCESS(TestBench);
  
  // Clock
  sc_clock            SystemClock;

  // Host Data Path
  HostCL              hostCL;
  SataHostTLChannel   hostTL;
  CarbonXSCSataLL     hostLL;
  
  // Device Data Path
  DeviceCL            deviceCL;
  SataDeviceTLChannel deviceTL;
  CarbonXSCSataLL     deviceLL;
  
  // Carbon Model or SystemC glue component
  TestGlue            glue;
  
  TestBench(sc_module_name name);
  ~TestBench();
};

#endif
