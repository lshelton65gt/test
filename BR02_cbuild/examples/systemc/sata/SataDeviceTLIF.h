// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef _SataDeviceTLIF_
#define _SataDeviceTLIF_

#include "systemc.h"
#include "carbon/carbon_shelltypes.h"

#define SATA_REG_LBA_HIGH     0x0D // RW
#define SATA_REG_LBA_MID      0x0C // RW
#define SATA_REG_LBA_LOW      0x0B // RW
#define SATA_REG_DEVICE       0x0E // RW
#define SATA_REG_SECTOR_CNT   0x0A // RW
#define SATA_REG_COMMAND      0x0F // WO
#define SATA_REG_STATUS       0x0F // RO
#define SATA_REG_FEATURES     0x09 // WO
#define SATA_REG_ERROR        0x09 // RO
#define SATA_REG_DATA_REG     0x08 // RW
#define SATA_REG_DATA_PORT    0x00 // RW
#define SATA_REG_DEVICE_CTRL  0x16 // WO
#define SATA_REG_ALT_STATUS   0x16 // RO

//! Sata Device Transaction Layer Interface Class
class SataDeviceTLIF : virtual public sc_interface {
public :
  //! Write Shadow Register
  virtual void writeReg(CarbonUInt8 addr, CarbonUInt16 value)=0;

  //! Write Read Shadow Register
  virtual CarbonUInt16 readReg(CarbonUInt8 addr)=0;

  //! Initiate PIO Setup FIS Transmission
  virtual bool PIOSetup(bool I, bool D, CarbonUInt32 transCount, CarbonUInt8 eStatus)=0;

  //! Initiate DMA Setup FIS Transmission
  virtual bool DMASetup(bool I, bool D, CarbonUInt32 transCount, CarbonUInt32 dmaBufIdHi, CarbonUInt32 dmaBufIdLo, CarbonUInt32 dmaBufOffset)=0;

  //! Initiate DMA Activate FIS Transmission
  virtual bool DMActivate()=0;

  //! Initiate Set Device Bits  FIS Transmission
  virtual bool setDeviceBits(bool I, bool N)=0;

  //! Initiate Register Device to Host FIS Transmission
  virtual bool updateHostShadowReg()=0;

  //! Received Cmd Event
  virtual sc_event& receivedCmd()=0;

  //! Received Data Event
  virtual sc_event& receivedData()=0;
};

#endif
