// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "SataDeviceTLChannel.h"
#include "xactors/sata/carbonXSataTLTrans.h"

SataDeviceTLChannel::SataDeviceTLChannel(sc_module_name _name) :
  sc_channel(_name),
  startTransPort("startTransPort"),
  mLbaHigh(0),
  mLbaMid(0),
  mLbaLow(0),
  mDevice(0),
  mSectorCnt(0),
  mCommand(0),
  mStatus(0),
  mFeatures(0),
  mErrors(0),
  mDeviceCtrl(0),
  mAltStatus(0),
  mEStatus(0),
  mTxTransCount(0),
  mRxTransCount(0),
  mLastTransSuccess(false)
{
  mXtorName = std::string(name());
}


// Transport Layer Interface
void SataDeviceTLChannel::writeReg(CarbonUInt8 addr, CarbonUInt16 value) {
  switch(addr) {
  case SATA_REG_LBA_HIGH :
    mLbaHigh = value;
    break;
  case SATA_REG_LBA_MID :
    mLbaMid = value;
    break;
  case SATA_REG_LBA_LOW :
    mLbaLow = value;
    break;
  case SATA_REG_DEVICE :
    mDevice = value;
    break;
  case SATA_REG_SECTOR_CNT :
    mSectorCnt = value;
    break;
  case SATA_REG_ERROR :
    mErrors = value;
    break;
  case SATA_REG_STATUS :
    mStatus = value;
    break;
  case SATA_REG_DATA_REG :

    // Save Data in transmitt queue and decrement the transfer count 
    mTxDataQueue.push_back(value);
    mTxTransCount -=2;
    
    // If all data for the transfer has been collected, send it off
    if(mTxTransCount == 0) {

      // If size is not an even number of dwords, add an extra 0 to the end
      if( (mTxDataQueue.size() % 2) != 0) mTxDataQueue.push_back(0);

      int size = mTxDataQueue.size();
      CarbonUInt32 data[size/2];
      for(CarbonUInt32 i = 0; i < size/2; i++) {
        data[i]   = mTxDataQueue.front();
        mTxDataQueue.pop_front();
        data[i] |= CarbonUInt32(mTxDataQueue.front()) << 16;;
        mTxDataQueue.pop_front();
      }
      
      // Send the data FIS
      CarbonXSataTLTransDataFis dataDev2HostFis;
      dataDev2HostFis.dataSize  = size/2;
      dataDev2HostFis.data      = data;
      dataDev2HostFis.PmPort    = 0;
      CarbonXSataLLTrans* trans = carbonXSataTLTransCreateDataFisLLFrame(&dataDev2HostFis);
      startTransPort->startNewTransaction(trans);
    }
    break;
  case SATA_REG_ALT_STATUS :
    mAltStatus = value;
    break;
  default :
    break;
  }
 }

CarbonUInt16 SataDeviceTLChannel::readReg(CarbonUInt8 addr)
{
  switch(addr) {
  case SATA_REG_LBA_HIGH :
    return mLbaHigh;
    break;
  case SATA_REG_LBA_MID :
    return mLbaMid;
    break;
  case SATA_REG_LBA_LOW :
    return mLbaLow;
    break;
  case SATA_REG_DEVICE :
    return mDevice;
    break;
  case SATA_REG_SECTOR_CNT :
    return mSectorCnt;
    break;
  case SATA_REG_COMMAND :
    return mCommand;
    break;
  case SATA_REG_FEATURES :
    return mFeatures;
    break;
  case SATA_REG_DATA_REG :
    if(!mRxDataQueue.empty()) {
      CarbonUInt16 data = mRxDataQueue.front();
      mRxDataQueue.pop_front();
      return data;
    }
    else return 0;
    break;
  case SATA_REG_DEVICE_CTRL :
    return mDeviceCtrl;
    break;
  default :
    return 0;
    break;
  }
}
  
bool SataDeviceTLChannel::PIOSetup(bool I, bool D, CarbonUInt32 transCount, CarbonUInt8 eStatus)
{
  CarbonXSataTLTransPioSetupFis pioSetupFis;
  pioSetupFis.I          = I;
  pioSetupFis.D          = D;
  pioSetupFis.Device     = mDevice;
  pioSetupFis.PmPort     = 0;
  pioSetupFis.Error      = mErrors;
  pioSetupFis.Status     = mStatus;
  pioSetupFis.E_Status   = eStatus;
  pioSetupFis.TransCount = transCount & 0xfffffffe; // Bit 0 has to be cleared
  CarbonXSataLLTrans* trans = 
    carbonXSataTLTransCreatePioSetupFisLLFrame(&pioSetupFis);
  startTransPort->startNewTransaction(trans);

  // Save number of bytes to transfer
  if(D) mTxTransCount = transCount & 0xfffffffe; // Bit 0 has to be cleared
  else  mRxTransCount = transCount & 0xfffffffe; // Bit 0 has to be cleared

  // Wait for transaction to finish
  wait(mTransactionDoneEv);

  // Return the success status of the transaction
  return mLastTransSuccess;

}

bool SataDeviceTLChannel::DMASetup(bool I, bool D, CarbonUInt32 transCount, 
                                   CarbonUInt32 dmaBufIdHi, CarbonUInt32 dmaBufIdLo, CarbonUInt32 dmaBufOffset)
{
  // Setup and send a DMA Setup FIS Here.
  // ...
  return false;
}

bool SataDeviceTLChannel::DMActivate(){
  // Setup and send a DMA Activate FIS Here.
  // ...  
  return false;
}

bool SataDeviceTLChannel::setDeviceBits(bool I, bool N)
{
  // Setup and send a Set Device Bits FIS Here.
  // ...  
  return false;
}

bool SataDeviceTLChannel::updateHostShadowReg()
{
  // Setup and send a Register Device to Host FIS Here.
  // ...  
  return false;
}

sc_event& SataDeviceTLChannel::receivedCmd()
{
  return mReceivedCmdEv;
}

sc_event& SataDeviceTLChannel::receivedData()
{
  return mReceivedDataEv;
}

// Response Interface
void SataDeviceTLChannel::setDefaultResponse(CarbonXSataLLTrans* trans)
{
}

void SataDeviceTLChannel::setResponse(CarbonXSataLLTrans* trans)
{
  if (carbonXSataLLTransGetType(trans) == CarbonXSataLLTrans_Frame)
  {
    if (carbonXSataLLTransGetTransStatus(trans) ==
        CarbonXSataLLTransStatus_Ok)
    {
      switch ( carbonXSataTLTransGetFisType(trans) )
      {
      case CarbonXSataTLTrans_RegHost2DevFis:
      {
        cout << "**** " << mXtorName <<
          ": Register FIS - Host to Device received."
                        << endl;

        // Extract PioSetupStructure from Link Layer FIS
        CarbonXSataTLTransRegHost2DevFis* fis =
          carbonXSataTLTransGetRegHost2DevFis(trans);
                                                
        // Update Device Status Register
        mCommand    = fis->Command;
        mDeviceCtrl = fis->Control;
        mDevice     = fis->Device;
        mLbaHigh    = fis->LbaHigh     | (CarbonUInt32(fis->LbaHighExp)      << 8); 
        mLbaMid     = fis->LbaMid      | (CarbonUInt32(fis->LbaMidExp)       << 8); 
        mLbaLow     = fis->LbaLow      | (CarbonUInt32(fis->LbaLowExp)       << 8); 
        mSectorCnt  = fis->SectorCount | (CarbonUInt32(fis->SectorCountExp)  << 8);
        mFeatures   = fis->Features;
        
        // If this is a new command, notify command layer
        if(fis->C) mReceivedCmdEv.notify(SC_ZERO_TIME);

        // Delete Transaction Structure
        carbonmem_free(fis);
        break;
      }
      case CarbonXSataTLTrans_RegDev2HostFis:
      {
        cout << "**** " << mXtorName <<
          ": Register FIS - Device to Host received."
                        << endl;
        SC_REPORT_ERROR(mXtorName.c_str(), "Device received a Register Device to Host FIS");
        break;
      }
      case CarbonXSataTLTrans_SetDevBitsFis:
      {
        cout << "**** " << mXtorName <<
          ": Set Device Bits FIS received." << endl;

        SC_REPORT_ERROR(mXtorName.c_str(), "Device received a Set Device Buts FIS");
        break;
      }
      case CarbonXSataTLTrans_DmaActFis:
      {
        cout << "**** " << mXtorName <<
          ": DMA Activate FIS received." << endl;
        
        SC_REPORT_ERROR(mXtorName.c_str(), "Device received a DMA Activate FIS");
        break;
      }
      case CarbonXSataTLTrans_DmaSetupFis:
      {
        cout << "**** " << mXtorName <<
          ": DMA Setup FIS received." << endl;

        // Implement Functionality for this FIS type here
        // ...
        break;
      }
      case CarbonXSataTLTrans_PioSetupFis:
      {
        cout << "**** " << mXtorName <<
          ": PIO Setup FIS received." << endl;
        
        SC_REPORT_ERROR(mXtorName.c_str(), "Device received a PIO Setup FIS");

        break;
      }
      case CarbonXSataTLTrans_DataFis:
      {
        cout << "**** " << mXtorName <<
          ": Data FIS received." << endl;

        // Extract PioSetupStructure from Link Layer FIS
        CarbonXSataTLTransDataFis* fis =
          carbonXSataTLTransGetDataFis(trans);
        
        // Copy Data into the Data Queue (Should perhaps clear queue first, not sure)
        for(CarbonUInt32 i = 0; i < mRxTransCount; i++) {
          mRxDataQueue.push_back((fis->data[i/2] >> (16 * (i&1)) & 0xffff));
        }
        
        mReceivedDataEv.notify(SC_ZERO_TIME);

        // Delete Transaction Structure
        carbonmem_free(fis->data);
        carbonmem_free(fis);
        break;
      } 
      case CarbonXSataTLTrans_BistActFis:
      {
        SC_REPORT_WARNING(mXtorName.c_str(),
                          "Unsupported BIST Activate FIS received");
        break;
      }
      case CarbonXSataTLTrans_ReservedFis:
      {
        SC_REPORT_WARNING(mXtorName.c_str(),
                          "Unsupported Reserved FIS received");
        break;
      }
      case CarbonXSataTLTrans_VendorSpecFis:
      {
        SC_REPORT_WARNING(mXtorName.c_str(),
                          "Unsupported Vendor Specific FIS received");
        break;
      }
      default:
      {
        SC_REPORT_ERROR(mXtorName.c_str(),
                        "Invalid FIS received");
      }
      }
    }
    else
    {
      SC_REPORT_ERROR(mXtorName.c_str(), "Error in FIS reception");
    }
  }
}

// Report Interface
void SataDeviceTLChannel::reportTransaction(CarbonXSataLLTrans* trans)
{
  if (carbonXSataLLTransGetType(trans) == CarbonXSataLLTrans_Frame)
  {
    if (carbonXSataLLTransGetTransStatus(trans) ==
        CarbonXSataLLTransStatus_Ok)
    {
      cout << "**** " << mXtorName << ": FIS transmission successful." << endl;
      mLastTransSuccess = true;
    }
    else
    {
      mLastTransSuccess = false;
      SC_REPORT_ERROR(mXtorName.c_str(), "Error in FIS transmission");
    }
  }
  carbonXSataLLTransDestroy(trans);
  mTransactionDoneEv.notify(SC_ZERO_TIME);
}
  
