// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "TestBench.h"

const int MAX_TRANS = 5;

TestBench :: TestBench(sc_module_name name) :
  sc_module(name), SystemClock("SystemClock", 100, SC_NS, 0.5),
  hostCL("HOST_CL"),
  hostTL("HOST_TL"),
  hostLL("HOST_LL", CarbonXSataLL_Host, MAX_TRANS),
  deviceCL("DEVICE_CL"),
  deviceTL("DEVICE_TL"),
  deviceLL("DEVICE_LL", CarbonXSataLL_Device, MAX_TRANS),
  glue("TestGlue")
{
  // Connect Host Command Layer to Host Transport Layer
  hostCL.tlPort(hostTL);
  
  // Connect Host Transport Layer to Link Layer transactor
  hostLL.reportTransPort(hostTL);
  hostLL.responsePort(hostTL);
  hostTL.startTransPort(hostLL.startTransExport);

  // Connect Device Command Layer to Device Transport Layer
  deviceCL.tlPort(deviceTL);

  // Connect Device Transport Layer to Link Layer transactor
  deviceLL.reportTransPort(deviceTL);
  deviceLL.responsePort(deviceTL);
  deviceTL.startTransPort(deviceLL.startTransExport);
  
  // Connect Clocks
  glue.SystemClock(SystemClock);
  hostLL.SystemClock(SystemClock);
  deviceLL.SystemClock(SystemClock);

  // Connect Link Layer transactors to glue module
  glue.Host_COMRESET(hostLL.COMRESET);
  glue.Host_COMWAKE(hostLL.COMWAKE);
  glue.Host_PhyRdy(hostLL.PhyRdy);
  glue.Host_DeviceDetect(hostLL.DeviceDetect);
  glue.Host_PhyInternalError(hostLL.PhyInternalError);
  glue.Host_COMMA(hostLL.COMMA);
  glue.Host_PARTIAL(hostLL.PARTIAL);
  glue.Host_SLUMBER(hostLL.SLUMBER);
  glue.Host_DATAOUT(hostLL.DATAOUT);
  glue.Host_DATAIN(hostLL.DATAIN);

  glue.Device_COMRESET(deviceLL.COMRESET);
  glue.Device_COMWAKE(deviceLL.COMWAKE);
  glue.Device_PhyRdy(deviceLL.PhyRdy);
  glue.Device_DeviceDetect(deviceLL.DeviceDetect);
  glue.Device_PhyInternalError(deviceLL.PhyInternalError);
  glue.Device_COMMA(deviceLL.COMMA);
  glue.Device_PARTIAL(deviceLL.PARTIAL);
  glue.Device_SLUMBER(deviceLL.SLUMBER);
  glue.Device_DATAOUT(deviceLL.DATAOUT);
  glue.Device_DATAIN(deviceLL.DATAIN);
}

TestBench :: ~TestBench()
{
}

