// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef _SataHostTLChannel_
#define _SataHostTLChannel_

#include "systemc.h"
#include "xactors/systemc/CarbonXSCSataLLIfs.h"
#include "SataHostTLIF.h"
#include <deque>

/*!
  \brief SATA Host Transaction Layer Channel
  This module/channel abstracts out the host transaction layer by implementing
  a register access interface to the shadow register structure.

  The class is not complete and is just implemented enough to support the 
  'IDENTIFY PACKET DEVICE' command example, but it should be easily expandable
  to support all transaction level functionality.
*/
class SataHostTLChannel : public sc_channel, 
                          public virtual CarbonXSCSataResponseIF, 
                          public virtual CarbonXSCSataReportTransactionIF,
                          public virtual SataHostTLIF
{
public:

  //! Start Transaction Port to the Link Layer transactor
  sc_port<CarbonXSCSataStartTransactionIF> startTransPort;

  //! Constructor
  SataHostTLChannel(sc_module_name name);

  // Transport Layer Interface

  /*!
    \brief Write Register
    Write a value to the specified register If the command register
    is written a Register Host2Device FIS will generated and send to
    the device prompting it to execute the given command.
    \param addr Register Address to write
    \param value Value to be written 
  */
  void writeReg(CarbonUInt8 addr, CarbonUInt16 value);
  
  /*!
    \brief Read Shadow Register
    Reads the current value of the given register.
    \param addr Register Address to read
    \return value of read register.
  */
  CarbonUInt16 readReg(CarbonUInt8 addr);
  
  /*
    \brief Polls a register until it contains the expected value
    Poll the given register until it contains the value given by \a exp_value 
    masked with \a mask or until the time given by timeout has passed.
    The register will be read every time the shadow registers has been
    updated by an FIS from the device. Note that since the \sa readReg method
    just reads the shadow register without any time being passed readReg can't
    be used to poll a register since that will cause an infinate loop.
    \param addr Address of register to be polled.
    \param exp_value Value to poll for
    \param mask Only look at the bits asserted in mask when comparing the exp_value \
    to the current content of the given register.
    \retval true if the expected value was found in the register before it had timed out
    \retval false when timeout occured
  */
  bool pollReg(CarbonUInt8 addr, CarbonUInt16 exp_value, CarbonUInt16 mask, const sc_time& timeout);

  /*!
    \brief Returns an event that will be notified every time an FIS from the device is returned
    \return reference to the event
  */
  sc_event& receivedUpdate();

private:

  //! Set Default Response Callback method
  //! \param trans pointer to the Link Layer transaction struture
  void setDefaultResponse(CarbonXSataLLTrans* trans);

  //! Set Response Callback method
  //! \param trans pointer to the Link Layer transaction struture
  void setResponse(CarbonXSataLLTrans* trans);

  //! Report Transaction Callback method
  //! \param trans pointer to the Link Layer transaction struture
  void reportTransaction(CarbonXSataLLTrans* trans);

  // Shadow Registers
  CarbonUInt16 mLbaHigh;
  CarbonUInt16 mLbaMid;
  CarbonUInt16 mLbaLow;
  CarbonUInt16 mDevice;
  CarbonUInt16 mSectorCnt;
  CarbonUInt16 mCommand;
  CarbonUInt32 mStatus;
  CarbonUInt16 mFeatures;
  CarbonUInt16 mErrors;
  CarbonUInt16 mDeviceCtrl;
  CarbonUInt16 mAltStatus;
  
  // Data Transmitt Queue
  std::deque<CarbonUInt16> mTxDataQueue;

  // Data Receive Queue
  std::deque<CarbonUInt16> mRxDataQueue;

  // Member Variables
  std::string  mXtorName;
  CarbonUInt32 mEStatus;
  CarbonUInt32 mTxTransCount;
  CarbonUInt32 mRxTransCount;

  // Events
  sc_event mTransactionDoneEv;
  sc_event mReceivedUpdateEv;
};

#endif
