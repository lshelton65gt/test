/**** Glue Logic connecting master and slave Transactors ****/
`ifndef ID_BUS_WIDTH
`define ID_BUS_WIDTH 4
`endif
`ifndef DATA_BUS_WIDTH
`define DATA_BUS_WIDTH 32
`endif
`define STRB_BUS_WIDTH (`DATA_BUS_WIDTH / 8 + (`DATA_BUS_WIDTH % 8 != 0))
module TestGlue(SystemClock,
        /* Master Ports */
        /* Write Address Channel */
        Master_AWID,
        Master_AWADDR,
        Master_AWLEN,
        Master_AWSIZE,
        Master_AWBURST,
        Master_AWLOCK,
        Master_AWCACHE,
        Master_AWPROT,
        Master_AWVALID,
        Master_AWREADY,
        /* Write Data Channel */
        Master_WID,
        Master_WDATA,
        Master_WSTRB,
        Master_WLAST,
        Master_WVALID,
        Master_WREADY,
        /* Write Response Channel */
        Master_BID,
        Master_BRESP,
        Master_BVALID,
        Master_BREADY,
        /* Read Address Channel */
        Master_ARID,
        Master_ARADDR,
        Master_ARLEN,
        Master_ARSIZE,
        Master_ARBURST,
        Master_ARLOCK,
        Master_ARCACHE,
        Master_ARPROT,
        Master_ARVALID,
        Master_ARREADY,
        /* Read Data Channel */
        Master_RID,
        Master_RDATA,
        Master_RRESP,
        Master_RLAST,
        Master_RVALID,
        Master_RREADY,

        /* Slave Connections */
        /* Write Address Channel */
        Slave_AWID,
        Slave_AWADDR,
        Slave_AWLEN,
        Slave_AWSIZE,
        Slave_AWBURST,
        Slave_AWLOCK,
        Slave_AWCACHE,
        Slave_AWPROT,
        Slave_AWVALID,
        Slave_AWREADY,
        /* Write Data Channel */
        Slave_WID,
        Slave_WDATA,
        Slave_WSTRB,
        Slave_WLAST,
        Slave_WVALID,
        Slave_WREADY,
        /* Write Response Channel */
        Slave_BID,
        Slave_BRESP,
        Slave_BVALID,
        Slave_BREADY,
        /* Read Address Channel */
        Slave_ARID,
        Slave_ARADDR,
        Slave_ARLEN,
        Slave_ARSIZE,
        Slave_ARBURST,
        Slave_ARLOCK,
        Slave_ARCACHE,
        Slave_ARPROT,
        Slave_ARVALID,
        Slave_ARREADY,
        /* Read Data Channel */
        Slave_RID,
        Slave_RDATA,
        Slave_RRESP,
        Slave_RLAST,
        Slave_RVALID,
        Slave_RREADY
        );
input SystemClock;
/* Master ports*/
/* Write Address Channel */
input  [`ID_BUS_WIDTH - 1 : 0] Master_AWID;
input  [31 : 0] Master_AWADDR;
input  [3 : 0] Master_AWLEN;
input  [2 : 0] Master_AWSIZE;
input  [1 : 0] Master_AWBURST;
input  [1 : 0] Master_AWLOCK;
input  [3 : 0] Master_AWCACHE;
input  [2 : 0] Master_AWPROT;
input  Master_AWVALID;
output Master_AWREADY;
/* Write Data Channel */
input  [`ID_BUS_WIDTH - 1 : 0] Master_WID;
input  [`DATA_BUS_WIDTH - 1 : 0] Master_WDATA;
input  [`STRB_BUS_WIDTH - 1 : 0] Master_WSTRB;
input  Master_WLAST;
input  Master_WVALID;
output Master_WREADY;
/* Write Response Channel */
output [`ID_BUS_WIDTH - 1 : 0] Master_BID;
output [1 : 0] Master_BRESP;
output Master_BVALID;
input  Master_BREADY;
/* Read Address Channel */
input  [`ID_BUS_WIDTH - 1 : 0] Master_ARID;
input  [31 : 0] Master_ARADDR;
input  [3 : 0] Master_ARLEN;
input  [2 : 0] Master_ARSIZE;
input  [1 : 0] Master_ARBURST;
input  [1 : 0] Master_ARLOCK;
input  [3 : 0] Master_ARCACHE;
input  [2 : 0] Master_ARPROT;
input  Master_ARVALID;
output Master_ARREADY;
/* Read Data Channel */
output [`ID_BUS_WIDTH - 1 : 0] Master_RID;
output [`DATA_BUS_WIDTH - 1 : 0] Master_RDATA;
output [1 : 0] Master_RRESP;
output Master_RLAST;
output Master_RVALID;
input  Master_RREADY;

/* Slave Ports */
/* Write Address Channel */
output [`ID_BUS_WIDTH - 1 : 0] Slave_AWID;
output [31 : 0] Slave_AWADDR;
output [3 : 0] Slave_AWLEN;
output [2 : 0] Slave_AWSIZE;
output [1 : 0] Slave_AWBURST;
output [1 : 0] Slave_AWLOCK;
output [3 : 0] Slave_AWCACHE;
output [2 : 0] Slave_AWPROT;
output Slave_AWVALID;
input  Slave_AWREADY;
/* Write Data Channel */
output [`ID_BUS_WIDTH - 1 : 0] Slave_WID;
output [`DATA_BUS_WIDTH - 1 : 0] Slave_WDATA;
output [`STRB_BUS_WIDTH - 1 : 0] Slave_WSTRB;
output Slave_WLAST;
output Slave_WVALID;
input  Slave_WREADY;
/* Write Response Channel */
input  [`ID_BUS_WIDTH - 1 : 0] Slave_BID;
input  [1 : 0] Slave_BRESP;
input  Slave_BVALID;
output Slave_BREADY;
/* Read Address Channel */
output [`ID_BUS_WIDTH - 1 : 0] Slave_ARID;
output [31 : 0] Slave_ARADDR;
output [3 : 0] Slave_ARLEN;
output [2 : 0] Slave_ARSIZE;
output [1 : 0] Slave_ARBURST;
output [1 : 0] Slave_ARLOCK;
output [3 : 0] Slave_ARCACHE;
output [2 : 0] Slave_ARPROT;
output Slave_ARVALID;
input  Slave_ARREADY;
/* Read Data Channel */
input  [`ID_BUS_WIDTH - 1 : 0] Slave_RID;
input  [`DATA_BUS_WIDTH - 1 : 0] Slave_RDATA;
input  [1 : 0] Slave_RRESP;
input  Slave_RLAST;
input  Slave_RVALID;
output Slave_RREADY;

/* Xtor connectivities */
/* Write Address Channel */
assign Slave_AWID = Master_AWID;
assign Slave_AWADDR = Master_AWADDR;
assign Slave_AWLEN = Master_AWLEN;
assign Slave_AWSIZE = Master_AWSIZE;
assign Slave_AWBURST = Master_AWBURST;
assign Slave_AWLOCK = Master_AWLOCK;
assign Slave_AWCACHE = Master_AWCACHE;
assign Slave_AWPROT = Master_AWPROT;
assign Slave_AWVALID = Master_AWVALID;
assign Master_AWREADY = Slave_AWREADY;
/* Write Data Channel */
assign Slave_WID = Master_WID;
assign Slave_WDATA = Master_WDATA;
assign Slave_WSTRB = Master_WSTRB;
assign Slave_WLAST = Master_WLAST;
assign Slave_WVALID = Master_WVALID;
assign Master_WREADY = Slave_WREADY;
/* Write Response Channel */
assign Master_BID = Slave_BID;
assign Master_BRESP = Slave_BRESP;
assign Master_BVALID = Slave_BVALID;
assign Slave_BREADY = Master_BREADY;
/* Read Address Channel */
assign Slave_ARID = Master_ARID;
assign Slave_ARADDR = Master_ARADDR;
assign Slave_ARLEN = Master_ARLEN;
assign Slave_ARSIZE = Master_ARSIZE;
assign Slave_ARBURST = Master_ARBURST;
assign Slave_ARLOCK = Master_ARLOCK;
assign Slave_ARCACHE = Master_ARCACHE;
assign Slave_ARPROT = Master_ARPROT;
assign Slave_ARVALID = Master_ARVALID;
assign Master_ARREADY = Slave_ARREADY;
/* Read Data Channel */
assign Master_RID = Slave_RID;
assign Master_RDATA = Slave_RDATA;
assign Master_RRESP = Slave_RRESP;
assign Master_RLAST = Slave_RLAST;
assign Master_RVALID = Slave_RVALID;
assign Slave_RREADY = Master_RREADY;

endmodule

