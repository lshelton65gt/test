// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "TestBench.h"

int sc_main (int argc , char *argv[])
{
    // Create the testbench
    sc_module *tb = new TestBench("TESTBENCH");

    // Run test
    sc_start();

    delete tb;

    return 0;
}
