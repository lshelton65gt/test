# Scalablity option
ifndef DataBusWidth
	DataBusWidth = 32
endif

# Set SystemC Home
SYSTEMC_HOME ?= $(CARBON_HOME)/systemc

# Compiler options
CFLAGS += -g -Wall -DDATA_BUS_WIDTH=$(DataBusWidth) \
		  -I. -I$(CARBON_HOME)/include -I$(SYSTEMC_HOME)/include
CBUILDFLAGS := -v2k -enableOutputSysTasks +define+DATA_BUS_WIDTH=$(DataBusWidth)
SYSTEMC_LIB_LIST ?= -L${SYSTEMC_HOME}/lib-linux -lsystemc -lstdc++ -lm

# Source files
SRCS := \
	libTestGlue.systemc.cpp \
	TestBench.cpp \
	Test.cpp \
	TestMain.cpp
OBJS := $(SRCS:%.cpp=%.o)

ifdef CARBON_HOME
	include $(CARBON_HOME)/makefiles/Makefile.common
endif

all: check_env check_log

ifdef CARBON_HOME
ifdef SYSTEMC_HOME
check_env:
else
check_env:
	@echo Environment variable SYSTEMC_HOME is not set ...
	@exit 1
endif
else
check_env:
	@echo Environment variable CARBON_HOME is not set ...
	@exit 1
endif

# Explicit dependencies
libTestGlue.systemc.cpp: libTestGlue.a
TestBench.cpp: libTestGlue.a
TestMain.cpp: libTestGlue.a

check_log: Test.log
	diff Test.gld $^

Test.log: Test.exe
	./$< > $@ 2> /dev/null

Test.exe: $(OBJS) libTestGlue.a
	$(CARBON_LINK) $(CFLAGS) -o $@ $^ $(CARBON_LIB_LIST) $(SYSTEMC_LIB_LIST)

%.o: %.cpp
	$(CARBON_CC) $(CFLAGS) -o $@ -c $<

libTestGlue.a: TestGlue.v
	$(CARBON_CBUILD) $(CBUILDFLAGS) -o $@ $<

libTestGlue.systemc.cpp libTestGlue.systemc.h : libTestGlue.a
	$(CARBON_HOME)/bin/carbon systemCWrapper libTestGlue.symtab.db
clean::
	rm -rf *.out *.exe lib*.* .carbon* *.o *.fsdb cds_*.h *.log *.vcd

