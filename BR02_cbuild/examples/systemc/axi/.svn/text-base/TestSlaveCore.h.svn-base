// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __TestSlaveCore_h__
#define __TestSlaveCore_h__

#include <string>
#include <queue>
using namespace std;

#include "xactors/axi/carbonXAxiTrans.h"
#include "xactors/systemc/CarbonXSCAxiIfs.h"

// Slave core module implementing memory. It implements slave transactors
// set default response and set response callbacks using exports.
// It also contains a memory (RAM) which can be accessed by master thru
// write and read transactions. The core also supports exclusive access
// implementing an exclusive access monitor.
template<int _ID_BUS_WIDTH = 4, int _DATA_BUS_WIDTH = 32, // config
    int _START_ADDRESS = 0, int _MEMORY_SPACE = 1024> // memory config
    // start address denotes the base address of the slave memory and
    // memory space specifies the memory size in bytes
class TestSlaveCore : public sc_module
{
public:
    SC_HAS_PROCESS(TestSlaveCore);

    // Slave transaction exports implementing slave transactor callbacks
    sc_export<CarbonXSCAxiReportTransactionIF> slaveReportTransExport;
    sc_export<CarbonXSCAxiResponseIF>          slaveResponseExport;

    TestSlaveCore(sc_module_name name) :
        sc_module(name), mSlaveReportTransExport(), mSlaveResponseExport()
    { 
        // Binding exports
        slaveReportTransExport(mSlaveReportTransExport);
        slaveResponseExport(mSlaveResponseExport);
    }

private:

    class ReportTransChannel : public CarbonXSCAxiReportTransactionIF
    {
    public:
        ReportTransChannel() {}
        void reportTransaction(CarbonXAxiTrans* trans) { }
    };

    // Response channel implementing callbacks
    class ResponseChannel : public CarbonXSCAxiResponseIF
    {
    public:
        ResponseChannel()
        {
            // initialize
            for (CarbonUInt32 i = 0; i < (1 << _ID_BUS_WIDTH); i++)
                mExIdAddrReg[i] = _MEMORY_SPACE + 1; // Reset to invalid address
        }

        // Set default response callback implemented providing data wait states 
        void setDefaultResponse(CarbonXAxiTrans* trans)
        {
            CarbonUInt32 wait[16];
            CarbonUInt32 waitSize = carbonXAxiTransGetBurstLength(trans);
            // Sets random waits
            for (CarbonUInt32 i = 0; i < waitSize; i++)
                wait[i] = rand() % 0xFF;
            carbonXAxiTransSetDataWaitStates(trans, waitSize, wait);
        }

        // Set response callback
        void setResponse(CarbonXAxiTrans* trans)
        {
            CarbonXAxiTransType transType = carbonXAxiTransGetType(trans);
            if (transType == CarbonXAxiTrans_WRITE)
            {
                cout << "SLAVE" << // "[" << sc_time_stamp() << "]" <<
                    ": ID = " << carbonXAxiTransGetId(trans) << ": " <<
                    "Write transaction received." << endl;
                CarbonUInt32 startAddress =
                    carbonXAxiTransGetStartAddress(trans);
                CarbonUInt32 dataSize = carbonXAxiTransGetDataSize(trans);
                // Check for page fault 
                if (startAddress < _START_ADDRESS || startAddress + dataSize >
                        _START_ADDRESS + _MEMORY_SPACE)
                {
                    cout << "**** PAGE FAULT ****" << endl;
                    // Respond decoder error for page fault
                    carbonXAxiTransSetWriteResponse(trans,
                            CarbonXAxiResponse_DECERR);
                    return;
                }

                // **** Exclusive Access Monitor ****
                if (carbonXAxiTransGetLockType(trans)
                       == CarbonXAxiAccess_EXCLUSIVE)
                {
                    // If address location has been changed by:
                    // 1. Some intermediate write
                    // 2. Some intermediate exclusive read
                    if (mExIdAddrReg[carbonXAxiTransGetId(trans)] !=
                            startAddress)
                    {
                        carbonXAxiTransSetWriteResponse(trans,
                                CarbonXAxiResponse_OKAY);
                        return;
                    }
                    else // Reset for success
                        mExIdAddrReg[carbonXAxiTransGetId(trans)] =
                            _MEMORY_SPACE + 1; // Reset
                }
                else
                {
                    // For normal write, if the write address is monitored
                    // for a particular ID, reset the address for that ID
                    for (CarbonUInt32 i = 0; i < (1 << _ID_BUS_WIDTH); i++)
                    {
                        if (mExIdAddrReg[i] == startAddress)
                            mExIdAddrReg[i] = _MEMORY_SPACE + 1; // Reset
                    }
                }

                // Data write to memory
                const CarbonUInt8 *data = carbonXAxiTransGetData(trans);
                const CarbonUInt1 *strobe =
                    carbonXAxiTransGetWriteStrobe(trans);
                for (CarbonUInt32 i = 0; i < dataSize; i++)
                {
                    if (strobe[i])
                        mSlaveMemory[startAddress + i - _START_ADDRESS] =
                            data[i];
#ifdef _DEBUG
                    cout << ">>>> DATA WRITE = " << int(data[i]) << endl;
#endif
                }
                if (carbonXAxiTransGetLockType(trans)
                        == CarbonXAxiAccess_EXCLUSIVE)
                    carbonXAxiTransSetWriteResponse(trans,
                            CarbonXAxiResponse_EXOKAY); // success
                else
                    carbonXAxiTransSetWriteResponse(trans,
                            CarbonXAxiResponse_OKAY); // exclusive error or
                                                      // normal success
            }
            else // if (transType == CarbonXAxiTrans_READ)
            {
                cout << "SLAVE" << // "[" << sc_time_stamp() << "]" <<
                    ": ID = " << carbonXAxiTransGetId(trans) << ": " <<
                    "Write transaction received." << endl;
                CarbonUInt32 startAddress =
                    carbonXAxiTransGetStartAddress(trans);
                // Aligned transfers only
                CarbonUInt32 dataSize =
                    (1 << carbonXAxiTransGetBurstSize(trans)) *
                    carbonXAxiTransGetBurstLength(trans);
                // Check for page fault
                if (startAddress < _START_ADDRESS || startAddress + dataSize >
                        _START_ADDRESS + _MEMORY_SPACE)
                {
                    cout << "**** PAGE FAULT ****" << endl;
                    // Respond with decoder error
                    CarbonXAxiResponse resp[16];
                    for (CarbonUInt32 i = 0; i < 16; i++)
                        resp[i] = CarbonXAxiResponse_DECERR;
                    carbonXAxiTransSetReadResponse(trans, 16, resp);
                    return;
                }
                // Data read from memory
                carbonXAxiTransSetData(trans, dataSize,
                        mSlaveMemory + startAddress - _START_ADDRESS);

                // **** Exclusive Access Monitor ****
                if (carbonXAxiTransGetLockType(trans)
                       == CarbonXAxiAccess_EXCLUSIVE)
                {
                    // Register address for ID provided and start monitoring
                    mExIdAddrReg[carbonXAxiTransGetId(trans)] = startAddress;
                }

            }
        }

    private:

        // Slave Memory
        CarbonUInt8        mSlaveMemory[_MEMORY_SPACE];
        // Exclusive Access Monitoring Array
        CarbonUInt32       mExIdAddrReg[1 << _ID_BUS_WIDTH];
    };

private:
    ReportTransChannel mSlaveReportTransExport;
    ResponseChannel    mSlaveResponseExport;

};

#endif
