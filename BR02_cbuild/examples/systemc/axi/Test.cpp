// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "Test.h"

// User specific test sequence
void Test :: test_func()
{
    // Starting a WRITE transaction
    CarbonXAxiTrans *trans = carbonXAxiTransCreate(1, CarbonXAxiTrans_WRITE);
    carbonXAxiTransSetStartAddress(trans, 0);
    carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
    CarbonUInt32 dataSize = 16;
    CarbonUInt8 data[128*16];
    for (CarbonUInt32 i = 0; i < dataSize; i++)
        data[i] = i;
    carbonXAxiTransSetData(trans, dataSize, data);
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Starting a WRITE transaction" << endl;
    masterStartTransPort->startNewTransaction(trans); // start transaction
    wait(); // Wait for completion of transaction
    // show the response
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Response received = " <<
        getResponseString(carbonXAxiTransGetWriteResponse(trans)) << endl;
    carbonXAxiTransDestroy(trans);

    // Starting a READ transaction
    trans = carbonXAxiTransCreate(1, CarbonXAxiTrans_READ);
    carbonXAxiTransSetStartAddress(trans, 4);
    carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
    carbonXAxiTransSetBurstLength(trans, 4);
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Starting a READ transaction" << endl;
    masterStartTransPort->startNewTransaction(trans);
    wait(); // Wait for completion of transaction
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Read data received." << endl;
    carbonXAxiTransDump(trans);
    carbonXAxiTransDestroy(trans);

    // **** Exclusive Access with EXOKAY ****
    cout << endl << "**** Exclusive Access with EXOKAY ****" << endl;
    // Starting a EXCLUSIVE READ transaction
    trans = carbonXAxiTransCreate(1, CarbonXAxiTrans_READ);
    carbonXAxiTransSetStartAddress(trans, 8);
    carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
    carbonXAxiTransSetBurstLength(trans, 2);
    carbonXAxiTransSetLockType(trans, CarbonXAxiAccess_EXCLUSIVE);
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Starting an EXCLUSIVE READ transaction" << endl;
    masterStartTransPort->startNewTransaction(trans);
    wait(); // Wait for completion of transaction
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Read data received." << endl;
    carbonXAxiTransDump(trans);
    carbonXAxiTransDestroy(trans);
    // Starting a EXCLUSIVE WRITE transaction
    trans = carbonXAxiTransCreate(1, CarbonXAxiTrans_WRITE);
    carbonXAxiTransSetStartAddress(trans, 8);
    carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
    carbonXAxiTransSetBurstLength(trans, 2);
    carbonXAxiTransSetData(trans, 4, data + 1);
    carbonXAxiTransSetLockType(trans, CarbonXAxiAccess_EXCLUSIVE);
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Starting an EXCLUSIVE WRITE transaction" << endl;
    masterStartTransPort->startNewTransaction(trans);
    wait(); // Wait for completion of transaction
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Response received = " <<
        getResponseString(carbonXAxiTransGetWriteResponse(trans)) << endl;
    carbonXAxiTransDestroy(trans);

    // **** Exclusive Access with OKAY error ****
    cout << endl << "**** Exclusive Access with OKAY error ****" << endl;
    // Starting a EXCLUSIVE READ transaction
    trans = carbonXAxiTransCreate(1, CarbonXAxiTrans_READ);
    carbonXAxiTransSetStartAddress(trans, 8);
    carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
    carbonXAxiTransSetBurstLength(trans, 2);
    carbonXAxiTransSetLockType(trans, CarbonXAxiAccess_EXCLUSIVE);
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Starting an EXCLUSIVE READ transaction" << endl;
    masterStartTransPort->startNewTransaction(trans);
    wait(); // Wait for completion of transaction
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Read data received." << endl;
    carbonXAxiTransDump(trans);
    carbonXAxiTransDestroy(trans);
    // Starting a WRITE transaction to same location with different ID
    trans = carbonXAxiTransCreate(2, CarbonXAxiTrans_WRITE);
    carbonXAxiTransSetStartAddress(trans, 8);
    carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
    carbonXAxiTransSetBurstLength(trans, 2);
    carbonXAxiTransSetData(trans, 4, data + 2);
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Starting an WRITE transaction to same location" << endl;
    masterStartTransPort->startNewTransaction(trans);
    wait(); // Wait for completion of transaction
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Response received = " <<
        getResponseString(carbonXAxiTransGetWriteResponse(trans)) << endl;
    carbonXAxiTransDestroy(trans);
    // Starting a EXCLUSIVE WRITE transaction
    trans = carbonXAxiTransCreate(1, CarbonXAxiTrans_WRITE);
    carbonXAxiTransSetStartAddress(trans, 8);
    carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
    carbonXAxiTransSetBurstLength(trans, 2);
    carbonXAxiTransSetData(trans, 4, data + 3);
    carbonXAxiTransSetLockType(trans, CarbonXAxiAccess_EXCLUSIVE);
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Starting an EXCLUSIVE WRITE transaction" << endl;
    masterStartTransPort->startNewTransaction(trans);
    wait(); // Wait for completion of transaction
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Response received = " <<
        getResponseString(carbonXAxiTransGetWriteResponse(trans)) << endl;
    carbonXAxiTransDestroy(trans);
    // Starting a READ transaction to see that earlier data write failed
    trans = carbonXAxiTransCreate(1, CarbonXAxiTrans_READ);
    carbonXAxiTransSetStartAddress(trans, 8);
    carbonXAxiTransSetBurstSize(trans, CarbonXAxiBurstSize_2);
    carbonXAxiTransSetBurstLength(trans, 2);
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Starting an READ transaction to check write failed" << endl;
    masterStartTransPort->startNewTransaction(trans);
    wait(); // Wait for completion of transaction
    cout << "**** MASTER" << // "[" << sc_time_stamp() << "]" <<
        ": ID = " << carbonXAxiTransGetId(trans) <<
        ": Read data received." << endl;
    carbonXAxiTransDump(trans);
    carbonXAxiTransDestroy(trans);

    // Stop the simulation
    sc_stop();
}

