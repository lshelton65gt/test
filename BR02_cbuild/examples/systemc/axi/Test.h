// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __Test_h__
#define __Test_h__

#include <string>
using namespace std;

#include "xactors/axi/carbonXAxiTrans.h"
#include "xactors/systemc/CarbonXSCAxiIfs.h"

// Configurations
#ifndef ID_BUS_WIDTH
#define ID_BUS_WIDTH 4
#endif
#ifndef DATA_BUS_WIDTH
#define DATA_BUS_WIDTH 32
#endif
#ifndef START_ADDRESS
#define START_ADDRESS 0
#endif
#ifndef MEMORY_SIZE
#define MEMORY_SIZE 1024
#endif

// Test layer for master
// User can generate transactions to access slave's memory
class Test : public sc_module
{
public:
    SC_HAS_PROCESS(Test);

    // Master transaction ports
    sc_port<CarbonXSCAxiStartTransactionIF>    masterStartTransPort;
    sc_export<CarbonXSCAxiReportTransactionIF> masterReportTransExport;
    sc_export<CarbonXSCAxiResponseIF>          masterResponseExport;

    // Test layer should wait for this event. This event denotes that
    // the transaction is completed and one can check the response.
    sc_event                                   transCompleteEvent;

    Test(sc_module_name name) :
        sc_module(name),
        mMasterReportTransExport("MASTER", transCompleteEvent),
        mMasterResponseExport("MASTER")
    { 
        // Binding exports
        masterReportTransExport(mMasterReportTransExport);
        masterResponseExport(mMasterResponseExport);

        // Start test thread
        SC_THREAD(test_func);
        sensitive << transCompleteEvent;
    }

    // Function thread to develop test scenarios
    void test_func();

private:

    // Resport transaction callback implemented at master side. It notifies 
    // the test thread - test_func, with transCompleteEvent event. On this
    // event user need to check response arraived from slave for the initiated
    // transaction at master.
    class ReportTransChannel : public CarbonXSCAxiReportTransactionIF
    {
    public:
        ReportTransChannel(string name, sc_event &reportTrans) :
            xtorName(name), transCompleteEvent(reportTrans) {}

        // Resport transaction callback
        void reportTransaction(CarbonXAxiTrans* trans)
        {
            // notify event to test thread
            transCompleteEvent.notify(SC_ZERO_TIME);
        }

    private:
        string xtorName;
        // Event to notify test thread that a transacton is complete
        sc_event &transCompleteEvent;

    };

    class ResponseChannel : public CarbonXSCAxiResponseIF
    {
    public:
        ResponseChannel(string name) : xtorName(name) {}

        void setDefaultResponse(CarbonXAxiTrans* trans) {}
        void setResponse(CarbonXAxiTrans* trans) {}

    private:
        string xtorName;

    };

    static const char *getResponseString(CarbonXAxiResponse resp)
    {
        const char *responseStr;
        switch (resp)
        {
            case CarbonXAxiResponse_OKAY:   responseStr = "OKAY"; break;
            case CarbonXAxiResponse_EXOKAY: responseStr = "EXOKAY"; break;
            case CarbonXAxiResponse_SLVERR: responseStr = "SLVERR"; break;
            case CarbonXAxiResponse_DECERR: responseStr = "DECERR"; break;

        }
        return responseStr;
    }

private:
    ReportTransChannel mMasterReportTransExport;
    ResponseChannel    mMasterResponseExport;

};

#endif
