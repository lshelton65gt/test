// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "TestBench.h"

TestBench :: TestBench(sc_module_name name) :
    sc_module(name), SystemClock("SystemClock", 100, SC_NS, 0.5)
{
    // instantiating test layer
    test = new Test("TEST");
    // instantiating transactors
    const int MAX_TRANS = 5;
    masterXtor = new CarbonXSCAxi<ID_BUS_WIDTH, CarbonXAxiAddressBusWidth_32,
               static_cast<CarbonXAxiDataBusWidth>(DATA_BUS_WIDTH)>
                   ("MASTER", CarbonXAxi_Master, MAX_TRANS);
    slaveXtor = new CarbonXSCAxi<ID_BUS_WIDTH, CarbonXAxiAddressBusWidth_32,
              static_cast<CarbonXAxiDataBusWidth>(DATA_BUS_WIDTH)>
                  ("SLAVE", CarbonXAxi_Slave, MAX_TRANS);
    // instantiating slave memory core
    slaveCore = new TestSlaveCore<ID_BUS_WIDTH, DATA_BUS_WIDTH>("SLAVE_CORE");
    // instantiating glue
    glue = new TestGlue("TestGlue");

    // connection between transactors and test
    test->masterStartTransPort(masterXtor->startTransExport);
    masterXtor->reportTransPort(test->masterReportTransExport);
    masterXtor->responsePort(test->masterResponseExport);

    slaveXtor->reportTransPort(slaveCore->slaveReportTransExport);
    slaveXtor->responsePort(slaveCore->slaveResponseExport);

    // connection between transactors and glue
    glue->SystemClock(SystemClock);
    masterXtor->SystemClock(SystemClock);
    slaveXtor->SystemClock(SystemClock);

    glue->Master_AWID(masterXtor->AWID);
    glue->Master_AWADDR(masterXtor->AWADDR);
    glue->Master_AWLEN(masterXtor->AWLEN);
    glue->Master_AWSIZE(masterXtor->AWSIZE);
    glue->Master_AWBURST(masterXtor->AWBURST);
    glue->Master_AWLOCK(masterXtor->AWLOCK);
    glue->Master_AWCACHE(masterXtor->AWCACHE);
    glue->Master_AWPROT(masterXtor->AWPROT);
    glue->Master_AWVALID(masterXtor->AWVALID);
    glue->Master_AWREADY(masterXtor->AWREADY);
    glue->Master_WID(masterXtor->WID);
    glue->Master_WDATA(masterXtor->WDATA);
    glue->Master_WSTRB(masterXtor->WSTRB);
    glue->Master_WLAST(masterXtor->WLAST);
    glue->Master_WVALID(masterXtor->WVALID);
    glue->Master_WREADY(masterXtor->WREADY);
    glue->Master_BID(masterXtor->BID);
    glue->Master_BRESP(masterXtor->BRESP);
    glue->Master_BVALID(masterXtor->BVALID);
    glue->Master_BREADY(masterXtor->BREADY);
    glue->Master_ARID(masterXtor->ARID);
    glue->Master_ARADDR(masterXtor->ARADDR);
    glue->Master_ARLEN(masterXtor->ARLEN);
    glue->Master_ARSIZE(masterXtor->ARSIZE);
    glue->Master_ARBURST(masterXtor->ARBURST);
    glue->Master_ARLOCK(masterXtor->ARLOCK);
    glue->Master_ARCACHE(masterXtor->ARCACHE);
    glue->Master_ARPROT(masterXtor->ARPROT);
    glue->Master_ARVALID(masterXtor->ARVALID);
    glue->Master_ARREADY(masterXtor->ARREADY);
    glue->Master_RID(masterXtor->RID);
    glue->Master_RDATA(masterXtor->RDATA);
    glue->Master_RRESP(masterXtor->RRESP);
    glue->Master_RLAST(masterXtor->RLAST);
    glue->Master_RVALID(masterXtor->RVALID);
    glue->Master_RREADY(masterXtor->RREADY);

    glue->Slave_AWID(slaveXtor->AWID);
    glue->Slave_AWADDR(slaveXtor->AWADDR);
    glue->Slave_AWLEN(slaveXtor->AWLEN);
    glue->Slave_AWSIZE(slaveXtor->AWSIZE);
    glue->Slave_AWBURST(slaveXtor->AWBURST);
    glue->Slave_AWLOCK(slaveXtor->AWLOCK);
    glue->Slave_AWCACHE(slaveXtor->AWCACHE);
    glue->Slave_AWPROT(slaveXtor->AWPROT);
    glue->Slave_AWVALID(slaveXtor->AWVALID);
    glue->Slave_AWREADY(slaveXtor->AWREADY);
    glue->Slave_WID(slaveXtor->WID);
    glue->Slave_WDATA(slaveXtor->WDATA);
    glue->Slave_WSTRB(slaveXtor->WSTRB);
    glue->Slave_WLAST(slaveXtor->WLAST);
    glue->Slave_WVALID(slaveXtor->WVALID);
    glue->Slave_WREADY(slaveXtor->WREADY);
    glue->Slave_BID(slaveXtor->BID);
    glue->Slave_BRESP(slaveXtor->BRESP);
    glue->Slave_BVALID(slaveXtor->BVALID);
    glue->Slave_BREADY(slaveXtor->BREADY);
    glue->Slave_ARID(slaveXtor->ARID);
    glue->Slave_ARADDR(slaveXtor->ARADDR);
    glue->Slave_ARLEN(slaveXtor->ARLEN);
    glue->Slave_ARSIZE(slaveXtor->ARSIZE);
    glue->Slave_ARBURST(slaveXtor->ARBURST);
    glue->Slave_ARLOCK(slaveXtor->ARLOCK);
    glue->Slave_ARCACHE(slaveXtor->ARCACHE);
    glue->Slave_ARPROT(slaveXtor->ARPROT);
    glue->Slave_ARVALID(slaveXtor->ARVALID);
    glue->Slave_ARREADY(slaveXtor->ARREADY);
    glue->Slave_RID(slaveXtor->RID);
    glue->Slave_RDATA(slaveXtor->RDATA);
    glue->Slave_RRESP(slaveXtor->RRESP);
    glue->Slave_RLAST(slaveXtor->RLAST);
    glue->Slave_RVALID(slaveXtor->RVALID);
    glue->Slave_RREADY(slaveXtor->RREADY);
}

TestBench :: ~TestBench()
{
    delete masterXtor;
    delete slaveXtor;
    delete glue;
    delete test;
}

