// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __TestBench_h__
#define __TestBench_h__

// Configurations 
#ifndef ID_BUS_WIDTH
#define ID_BUS_WIDTH 4
#endif
#ifndef DATA_BUS_WIDTH
#define DATA_BUS_WIDTH 32
#endif
#ifndef START_ADDRESS
#define START_ADDRESS 0
#endif
#ifndef MEMORY_SIZE
#define MEMORY_SIZE 1024
#endif

#include "Test.h"
#include "xactors/systemc/CarbonXSCAxi.h"
#include "TestSlaveCore.h"
#include "libTestGlue.systemc.h"

// Top level testbench comprising of transactors and glue and connecting
// test layer
class TestBench : public sc_module
{
public:
    SC_HAS_PROCESS(TestBench);

    sc_clock      SystemClock;

    // Module instantiations 
    Test         *test; // Master side test layer
    // Master and slave transactors
    CarbonXSCAxi<ID_BUS_WIDTH, CarbonXAxiAddressBusWidth_32,
        static_cast<CarbonXAxiDataBusWidth>(DATA_BUS_WIDTH)> *masterXtor;
    CarbonXSCAxi<ID_BUS_WIDTH, CarbonXAxiAddressBusWidth_32,
        static_cast<CarbonXAxiDataBusWidth>(DATA_BUS_WIDTH)> *slaveXtor;
    // Slave memory core
    TestSlaveCore<ID_BUS_WIDTH, DATA_BUS_WIDTH,
        START_ADDRESS, MEMORY_SIZE>                          *slaveCore;
    TestGlue     *glue;

    TestBench(sc_module_name name);
    ~TestBench();
};

#endif
