/*

This is not a working RTL one can use in real PCI design. User can ONLY write to this 
PCI slave. Also It's base address is NOT configurable. It has a fixed base address. 
It's only purpose is to demo some CMS features.

 */

module core (
  clk,
  rst_n,

  ad_in,
  ad_out,
  ad_en,

  cbe_n_in,
  cbe_n_out,
  cbe_n_en,

  par_in,
  par_out,
  par_en,

  perr_n_in,
  perr_n_out,
  perr_n_en,

  frame_n_in,
  frame_n_out,
  frame_n_en,

  trdy_n_in,
  trdy_n_out,
  trdy_n_en,

  irdy_n_in,
  irdy_n_out,
  irdy_n_en,

  stop_n_in,
  stop_n_out,
  stop_n_en,

  devsel_n_in,
  devsel_n_out,
  devsel_n_en,

  idsel_in,

  req_n_out,
  req_n_en,

  gnt_n_in,

  serr_n_out,

  inta_n_out
);

input          clk;
input          rst_n;

input  [31: 0] ad_in;
output [31: 0] ad_out;
output         ad_en;
input  [ 3: 0] cbe_n_in;
output [ 3: 0] cbe_n_out;
output         cbe_n_en;
input          par_in;
output         par_out;
output         par_en;
input          perr_n_in;
output         perr_n_out;
output         perr_n_en;
input          frame_n_in;
output         frame_n_out;
output         frame_n_en;
input          trdy_n_in;
output         trdy_n_out;
output         trdy_n_en;
input          irdy_n_in;
output         irdy_n_out;
output         irdy_n_en; 
input          stop_n_in;
output         stop_n_out;
output         stop_n_en;
input          devsel_n_in;
output         devsel_n_out;
output         devsel_n_en;
output         req_n_out;
output         req_n_en;
input          gnt_n_in;

input          idsel_in;
output         serr_n_out;


output         inta_n_out;


wire    [31:0] wrData;
wire    [31:0] rdData;
wire    [31:0] ad_in;
wire    [31:0] ad_out;
wire    [31:0] ad_en;
wire    [ 3:0] cbe_n_in;
wire    [ 3:0] cbe_n_out;
wire    [ 3:0] cbe_n_en;

wire           full;


assign stop_n_out = 1;
assign stop_n_en  = 0;

assign serr_n_out = 1;


write write0 (
  .reset_n ( rst_n ),
  .clock   ( clk ),
  .wr      ( wr ),
  .wrData  ( wrData ),
  .full    ( full ),
  .frame_n_in   ( frame_n_in ),
  .cbe_n_in     ( cbe_n_in ),
  .ad_in        ( ad_in ),
  .irdy_n_in    ( irdy_n_in ),
  .par_in       ( par_in ),
  .trdy_n_out   ( trdy_n_out ),
  .trdy_n_en    ( trdy_n_en ),
  .devsel_n_out ( devsel_n_out ),
  .devsel_n_en  ( devsel_n_en ),
  .perr_n_out   ( perr_n_out ),
  .perr_n_en    ( perr_n_en)
);


Fifo32x32 writeFifo(
  .reset_n ( rst_n ),
  .clock   ( clk ),
  .wr      ( wr ),
  .wData   ( wrData ),
  .full    ( full ),
  .rd      ( rd ),
  .rData   ( rdData ),
  .empty   ( empty )
);


read read0 (
  .reset_n ( rst_n ),
  .clock   ( clk ),
  .start   ( full ),
  .rd      ( rd ),
  .rdData  ( rdData ),
  .empty   ( empty ),
  .ad_out      ( ad_out ),
  .ad_en       ( ad_en ),
  .cbe_n_out   ( cbe_n_out ),
  .cbe_n_en    ( cbe_n_en ),
  .frame_n_out ( frame_n_out ),
  .frame_n_en  ( frame_n_en ),
  .irdy_n_out  ( irdy_n_out ),
  .irdy_n_en   ( irdy_n_en ),
  .par_out     ( par_out ),
  .par_en      ( par_en ),
  .req_n_out   ( req_n_out ),
  .req_n_en    ( req_n_en ),
  .trdy_n_in   ( trdy_n_in ),
  .gnt_n_in    ( gnt_n_in),
  .irq_n       ( inta_n_out )
);


endmodule
