module pci (
  rst_n,
  clk,

  ad,
  ad_out,
  cbe_n,
  par,
  perr_n,

  frame_n,
  trdy_n,
  irdy_n,
  stop_n,
  devsel_n,
  idsel,

  serr_n,

  req_n,
  gnt_n,

  inta_n
);

input          rst_n;
input          clk;

inout  [31: 0] ad;
output [31: 0] ad_out;
inout  [ 3: 0] cbe_n;
inout          par;
inout          perr_n;

inout          frame_n;
inout          trdy_n;
inout          irdy_n;
inout          stop_n;
inout          devsel_n;
input          idsel;

output         serr_n;
inout          req_n;
input          gnt_n;
output         inta_n;

wire   [31: 0] ad_in;
wire   [31: 0] ad_out;
wire   [31: 0] ad_en;

wire   [ 3: 0] cbe_n_in;
wire   [ 3: 0] cbe_n_out;
wire   [ 3: 0] cbe_n_en;

wire           par_in;
wire           par_out;
wire           par_en;

wire           perr_n_in;
wire           perr_n_out;
wire           perr_n_en;

pads pads0 (
  .clk          ( clk ),
  .clk_in       ( clk_in ),

  .rst_n        ( rst_n ),
  .rst_n_in     ( rst_n_in ),

  .ad           ( ad ),
  .ad_in        ( ad_in ),
  .ad_out       ( ad_out ),
  .ad_en        ( ad_en ),
  .cbe_n        ( cbe_n ),
  .cbe_n_in     ( cbe_n_in ),
  .cbe_n_out    ( cbe_n_out ),
  .cbe_n_en     ( cbe_n_en ),
  .par          ( par ),
  .par_in       ( par_in ),
  .par_out      ( par_out ),
  .par_en       ( par_en ),
  .perr_n       ( perr_n ),
  .perr_n_in    ( perr_n_in ),
  .perr_n_out   ( perr_n_out ),
  .perr_n_en    ( perr_n_en ),
  .frame_n      ( frame_n ),
  .frame_n_in   ( frame_n_in ),
  .frame_n_out  ( frame_n_out ),
  .frame_n_en   ( frame_n_en ),
  .trdy_n       ( trdy_n ),
  .trdy_n_in    ( trdy_n_in ),
  .trdy_n_out   ( trdy_n_out ),
  .trdy_n_en    ( trdy_n_en ),
  .irdy_n       ( irdy_n ),
  .irdy_n_in    ( irdy_n_in ),
  .irdy_n_out   ( irdy_n_out ),
  .irdy_n_en    ( irdy_n_en ),
  .stop_n       ( stop_n ),
  .stop_n_in    ( stop_n_in ),
  .stop_n_out   ( stop_n_out ),
  .stop_n_en    ( stop_n_en ),
  .devsel_n     ( devsel_n ),
  .devsel_n_in  ( devsel_n_in ),
  .devsel_n_out ( devsel_n_out ),
  .devsel_n_en  ( devsel_n_en ),
  .idsel        ( idsel ),
  .idsel_in     ( idsel_in ),
  .req_n        ( req_n ),
  .req_n_out    ( req_n_out ),
  .req_n_en     ( req_n_en ),
  .gnt_n        ( gnt_n ),
  .gnt_n_in     ( gnt_n_in ),
  .serr_n       ( serr_n ),
  .serr_n_out   ( serr_n_out ),
  .inta_n       ( inta_n ),
  .inta_n_out   ( inta_n_out )
);

core core0 (
  .clk          ( clk_in ),
  .rst_n        ( rst_n_in ),

  .ad_in        ( ad_in ),
  .ad_out       ( ad_out ),
  .ad_en        ( ad_en ),
  .cbe_n_in     ( cbe_n_in ),
  .cbe_n_out    ( cbe_n_out ),
  .cbe_n_en     ( cbe_n_en ),
  .par_in       ( par_in ),
  .par_out      ( par_out ),
  .par_en       ( par_en ),
  .perr_n_in    ( perr_n_in ),
  .perr_n_out   ( perr_n_out ),
  .perr_n_en    ( perr_n_en ),
  .frame_n_in   ( frame_n_in ),
  .frame_n_out  ( frame_n_out ),
  .frame_n_en   ( frame_n_en ),
  .trdy_n_in    ( trdy_n_in ),
  .trdy_n_out   ( trdy_n_out ),
  .trdy_n_en    ( trdy_n_en ),
  .irdy_n_in    ( irdy_n_in ),
  .irdy_n_out   ( irdy_n_out ),
  .irdy_n_en    ( irdy_n_en ),
  .stop_n_in    ( stop_n_in ),
  .stop_n_out   ( stop_n_out ),
  .stop_n_en    ( stop_n_en ),
  .devsel_n_in  ( devsel_n_in ),
  .devsel_n_out ( devsel_n_out ),
  .devsel_n_en  ( devsel_n_en ),
  .idsel_in     ( idsel_in ),
  .req_n_out    ( req_n_out ),
  .req_n_en     ( req_n_en ),
  .gnt_n_in     ( gnt_n_in ),
  .serr_n_out   ( serr_n_out ),
  .inta_n_out   ( inta_n_out )
);

endmodule
