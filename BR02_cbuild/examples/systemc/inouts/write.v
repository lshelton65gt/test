module write (
  reset_n,
  clock,

  wr,
  wrData,
  full,

  frame_n_in,
  cbe_n_in,
  ad_in,
  irdy_n_in,
  par_in,
  trdy_n_out,
  trdy_n_en,
  devsel_n_out,
  devsel_n_en,
  perr_n_out,
  perr_n_en
);

input          reset_n;
input          clock;

output         wr;
output [31: 0] wrData;

input          full;

input          frame_n_in;
input  [ 3: 0] cbe_n_in;
input  [31: 0] ad_in;
input          irdy_n_in;
input          par_in;
output         trdy_n_out;
output         trdy_n_en;
output         devsel_n_out;
output         devsel_n_en;
output         perr_n_out;
output         perr_n_en;


parameter  IDLE   = 0;
parameter  WAIT   = 1;
parameter  WRITE  = 2;

parameter  MEM_WRITE = 4'b0111;

parameter  MEM_BASE_ADDR = 32'h8000_0000;
parameter  MEM_BASE_MASK = 32'hFFFF_0000;


reg   [ 1: 0]  state_nxt;
reg   [ 1: 0]  state;

reg            trdy_n_out_nxt;
reg            trdy_n_out;
reg            trdy_n_en_nxt;
reg            trdy_n_en;

reg            devsel_n_out_nxt;
reg            devsel_n_out;
reg            devsel_n_en_nxt;
reg            devsel_n_en;

reg            perr_n_out_nxt;
reg            perr_n_out;
reg            perr_n_en_nxt;
reg            perr_n_en;

reg            wr_nxt;
reg            wr;

reg   [31: 0]  wrData_nxt;
reg   [31: 0]  wrData;


wire addr_sel = ((ad_in & MEM_BASE_MASK) == MEM_BASE_ADDR); // addr decoding
wire mem_write = (cbe_n_in == MEM_WRITE); // command decoding
wire perr = ((^ ad_in) ^ (^ cbe_n_in) ^ par_in) && ! irdy_n_in && ! trdy_n_out;

// only recognizes memory write to fixed address
always @ (frame_n_in or ad_in or cbe_n_in or irdy_n_in or trdy_n_out or state
          or addr_sel or mem_write or devsel_n_en or devsel_n_out or full or perr
	  or trdy_n_en)
begin
  if (perr) begin
    perr_n_out_nxt = 0;
    perr_n_en_nxt  = 1;
  end else begin
    perr_n_out_nxt = 1;
    perr_n_en_nxt  = 0;
  end

  state_nxt = state;

  trdy_n_out_nxt = trdy_n_out;
  trdy_n_en_nxt  = trdy_n_en;

  devsel_n_out_nxt = devsel_n_out;
  devsel_n_en_nxt  = devsel_n_en;

  wr_nxt     = 0;
  wrData_nxt = 0;

  case(state)
    IDLE:
      begin
        if(! frame_n_in && addr_sel && mem_write) begin
          state_nxt = WRITE;

          trdy_n_out_nxt = 0;
          trdy_n_en_nxt  = 1;

          devsel_n_out_nxt = 0;
          devsel_n_en_nxt  = 1;
        end else begin
          state_nxt = IDLE;
        end
      end

    WAIT:
      if(frame_n_in)
        state_nxt = IDLE;

    WRITE:
      begin
        if(frame_n_in && ! irdy_n_in && ! trdy_n_out) begin
          state_nxt = IDLE;

          trdy_n_out_nxt = 1;
          trdy_n_en_nxt  = 0;

          devsel_n_out_nxt = 1;
          devsel_n_en_nxt  = 0;
        end

        wr_nxt     = ! irdy_n_in && ! trdy_n_out && ! full;
        wrData_nxt = ad_in;
      end

    default:
      begin
        $display("@%d, PCI Slave state machine into error state!", $time);
        state_nxt = IDLE;
 
        trdy_n_out_nxt = 1'b1;
        trdy_n_en_nxt  = 1'b0;

        devsel_n_out_nxt = 1'b1;
        devsel_n_en_nxt  = 1'b0;

        wr_nxt     = 0;
        wrData_nxt = 0;
      end
  endcase
end

always @ (negedge reset_n or posedge clock)
begin
  if(! reset_n) begin
    state = IDLE;
    trdy_n_out <= 1'b1;
    trdy_n_en  <= 1'b0;

    devsel_n_out <= 1'b1;
    devsel_n_en  <= 1'b0;

    perr_n_out <= 1'b1;
    perr_n_en  <= 1'b0;

    wr     <= 0;
    wrData <= 0;
  end else begin
    state <= state_nxt;

    trdy_n_out <= trdy_n_out_nxt;
    trdy_n_en  <= trdy_n_en_nxt;

    devsel_n_out <= devsel_n_out_nxt;
    devsel_n_en  <= devsel_n_en_nxt;

    perr_n_out <= trdy_n_out_nxt;
    perr_n_en  <= trdy_n_en_nxt;

    wr     <= wr_nxt;
    wrData <= wrData_nxt;
  end
end


endmodule
