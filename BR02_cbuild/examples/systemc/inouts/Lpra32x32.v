module Lpra32x32 (
  wclk,
  wr,
  waddr,
  wdata,

  rclk,
  raddr,
  rdata
);

input          wclk;
input          wr;
input  [ 4:0]  waddr;
input  [31:0]  wdata;

input          rclk;
input  [ 4:0]  raddr;
output [31:0]  rdata;

reg    [31:0]  rdata;

reg    [31:0]  data[0:32];

always @ (posedge wclk)
  if (wr)
    data[waddr] <= wdata;

always @ (posedge rclk)
  rdata <= data[raddr];

endmodule
