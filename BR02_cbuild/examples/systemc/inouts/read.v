module read (
  reset_n,
  clock,

  start,

  rd,
  rdData,
  empty,

  irq_n,

  ad_out,
  ad_en,
  cbe_n_out,
  cbe_n_en,
  frame_n_out,
  frame_n_en,
  irdy_n_out,
  irdy_n_en,
  par_out,
  par_en,
  req_n_out,
  req_n_en,
  trdy_n_in,
  gnt_n_in
);

input          reset_n;
input          clock;

input          start;

output         rd;
input  [31: 0] rdData;
input          empty;

output         irq_n;

output [31: 0] ad_out;
output [31: 0] ad_en;
output [ 3: 0] cbe_n_out;
output [ 3: 0] cbe_n_en;
output         frame_n_out;
output         frame_n_en;
output         irdy_n_out;
output         irdy_n_en;
output         par_out;
output         par_en;
output         req_n_out;
output         req_n_en;
input          trdy_n_in;
input          gnt_n_in;


parameter  IDLE       = 0;
parameter  BUS_REQ    = 1;
parameter  ADDR_PHASE = 2;
parameter  DATA_PHASE = 3;


parameter  MEM_WRITE = 4'b0111;

parameter  DST_MEM_ADDR = 32'h9000_0000;

reg            par_out_nxt;
reg            par_out;

reg   [ 1: 0]  state_nxt;
reg   [ 1: 0]  state;

reg            irq_n_nxt;
reg            irq_n;

reg            frame_n_out_nxt;
reg            frame_n_out;
reg            frame_n_en_nxt;
reg            frame_n_en;
reg   [ 3: 0]  cbe_n_out_nxt;
reg   [ 3: 0]  cbe_n_out;
reg   [ 3: 0]  cbe_n_en_nxt;
reg   [ 3: 0]  cbe_n_en;
reg   [31: 0]  ad_out_nxt;
reg   [31: 0]  ad_out;
reg   [31: 0]  ad_en_nxt;
reg   [31: 0]  ad_en;
reg            par_en_nxt;
reg            par_en;
reg            req_n_out_nxt;
reg            req_n_out;
reg            req_n_en_nxt;
reg            req_n_en;
reg            irdy_n_out_nxt;
reg            irdy_n_out;
reg            irdy_n_en_nxt;
reg            irdy_n_en;

reg            rd_nxt;
reg            rd;


always @ (state or frame_n_out or frame_n_en or cbe_n_out or cbe_n_en
  	  or ad_out or ad_en or par_en or  req_n_out or req_n_en or
	  empty or gnt_n_in or irdy_n_en or irdy_n_out or irq_n or rdData or start or trdy_n_in) begin
  par_out_nxt     = (^ ad_out) ^ (^ cbe_n_out);

  state_nxt = state;

  irq_n_nxt = irq_n;

  frame_n_out_nxt = frame_n_out;
  frame_n_en_nxt  = frame_n_en;
  cbe_n_out_nxt   = cbe_n_out;
  cbe_n_en_nxt    = cbe_n_en;
  ad_out_nxt      = ad_out;
  ad_en_nxt       = ad_en;
  irdy_n_out_nxt  = irdy_n_out;
  irdy_n_en_nxt   = irdy_n_en;
  par_en_nxt      = par_en;
  req_n_out_nxt   = req_n_out;
  req_n_en_nxt    = req_n_en;

  rd_nxt = 0;

  case(state)
    IDLE:
      begin
        if (start && ! gnt_n_in) begin
          state_nxt = ADDR_PHASE;

          frame_n_out_nxt = 0;
          frame_n_en_nxt  = 1;
          cbe_n_out_nxt   = MEM_WRITE;
          cbe_n_en_nxt    = 4'hF;
          ad_out_nxt      = DST_MEM_ADDR;
          ad_en_nxt       = 32'hFFFF_FFFF;

          rd_nxt = 1;
        end else if (start) begin
          state_nxt = BUS_REQ;

          req_n_out_nxt = 0;
          req_n_en_nxt  = 1;
        end else begin
          ad_out_nxt    = 0;
          frame_n_out_nxt = 1;
          frame_n_en_nxt  = 0;
          cbe_n_out_nxt   = 4'hF;
          cbe_n_en_nxt    = 4'h0;
          ad_out_nxt      = 32'h0;
          ad_en_nxt       = 32'h0;
          irdy_n_out_nxt  = 1;
          irdy_n_en_nxt   = 0;
          par_en_nxt      = 0;

          rd_nxt = 0;
        end
      end

    BUS_REQ:
      begin
        if (! gnt_n_in) begin
          state_nxt = ADDR_PHASE;

          frame_n_out_nxt = 0;
          frame_n_en_nxt  = 1;
          cbe_n_out_nxt   = MEM_WRITE;
          cbe_n_en_nxt    = 4'hF;
          ad_out_nxt      = DST_MEM_ADDR;
          ad_en_nxt       = 32'hFFFF_FFFF;

          req_n_out_nxt = 1;
          req_n_en_nxt  = 0;

          rd_nxt = 1;
        end
      end

    ADDR_PHASE:
      begin
        state_nxt = DATA_PHASE;

        ad_out_nxt    = rdData;
        cbe_n_out_nxt = 4'h0;

	rd_nxt = 1;
      end

    DATA_PHASE:
      begin
        if (empty && ! trdy_n_in) begin
          state_nxt = IDLE;

          irq_n_nxt = 1;

          ad_out_nxt    = 0;
          frame_n_out_nxt = 1;
          frame_n_en_nxt  = 1;
          cbe_n_out_nxt   = 4'h0;
          cbe_n_en_nxt    = 4'hF;
          ad_out_nxt      = rdData;
          ad_en_nxt       = 32'hFFFF_FFFF;
          irdy_n_out_nxt  = 0;
          irdy_n_en_nxt   = 1;
          par_en_nxt      = 0;

          rd_nxt = 0;
        end else if (! trdy_n_in) begin
          irdy_n_out_nxt  = 0;
          irdy_n_en_nxt   = 1;

          ad_out_nxt    = rdData;
          rd_nxt = 1;
        end
      end
  endcase
end


always @ (negedge reset_n or posedge clock)
begin
  if(! reset_n) begin
    state <= IDLE;

    irq_n <= 0;

    frame_n_out <= 1;
    frame_n_en  <= 0;
    cbe_n_out   <= 4'hF;
    cbe_n_en    <= 4'h0;
    ad_out      <= 32'hFFFF_FFFF;
    ad_en       <= 32'h0;
    irdy_n_out  <= 1;
    irdy_n_en   <= 0;
    par_out     <= 0;
    par_en      <= 0;
    req_n_out   <= 1;
    req_n_en    <= 0;

    rd <= 0;
  end else begin
    par_out = par_out_nxt;

    state = state_nxt;

    irq_n <= irq_n_nxt;

    frame_n_out <= frame_n_out_nxt;
    frame_n_en  <= frame_n_en_nxt;
    cbe_n_out   <= cbe_n_out_nxt;
    cbe_n_en    <= cbe_n_en_nxt;
    ad_out      <= ad_out_nxt;
    ad_en       <= ad_en_nxt;
    irdy_n_out  <= irdy_n_out_nxt;
    irdy_n_en   <= irdy_n_en_nxt;
    par_en      <= par_en_nxt;
    req_n_out   <= req_n_out_nxt;
    req_n_en    <= req_n_en_nxt;

    rd <= rd_nxt;
  end
end


endmodule
