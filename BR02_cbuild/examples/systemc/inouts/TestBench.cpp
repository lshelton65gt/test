// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "TestBench.h"


#define ADDR_PHASE 13
#define DATA_PHASE 29
#define INIT_DATA 0xCAFE0000
#define Z32BITS "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"
#define Z4BITS "ZZZZ"

const char * MEM_BASE_ADDR = "0x80000000";


TestBench::TestBench(sc_module_name name) :
  sc_module(name)
{
  SC_METHOD(StimulusDrive);
  sensitive << rst_n.pos();
  sensitive << clk.pos();

  SC_METHOD(DataMonitor);
  sensitive << clk.pos();
}

TestBench::~TestBench()
{

}

void TestBench::StimulusDrive()
{
  idsel = 0;

  if (! rst_n)
    {
      frame_n  = SC_LOGIC_Z;
      devsel_n = SC_LOGIC_Z;
      irdy_n   = SC_LOGIC_Z;
      trdy_n   = SC_LOGIC_Z;
      stop_n   = SC_LOGIC_Z;
      ad       = Z32BITS;
      cbe_n    = Z4BITS;
      par      = SC_LOGIC_Z;

      perr_n = SC_LOGIC_Z;
      req_n  = SC_LOGIC_Z;
      gnt_n  = 0;

      count = 0;
      data = INIT_DATA;
      index = 0;
      monitor = 0;
    }
  else
    {
      if (count.read() < ADDR_PHASE)
	{
	  frame_n  = SC_LOGIC_Z;
	  devsel_n = SC_LOGIC_Z;
	  irdy_n   = SC_LOGIC_Z;
	  trdy_n   = SC_LOGIC_Z;
	  stop_n   = SC_LOGIC_Z;
	  ad       = Z32BITS;
	  cbe_n    = Z4BITS;
	  par      = SC_LOGIC_Z;

	  perr_n = SC_LOGIC_Z;
	  req_n  = SC_LOGIC_Z;
	  gnt_n  = 0;
	}
      else if (count.read() == ADDR_PHASE)
	{
          // drives addr
	  frame_n  = SC_LOGIC_0;
	  devsel_n = SC_LOGIC_Z;
	  irdy_n   = SC_LOGIC_1;
	  trdy_n   = SC_LOGIC_Z;
	  stop_n   = SC_LOGIC_Z;
	  ad       = MEM_BASE_ADDR;
	  cbe_n    = "0x7";
	  par      = SC_LOGIC_Z;

	  perr_n = SC_LOGIC_Z;
	  req_n  = SC_LOGIC_Z;
	  gnt_n  = 0;
	}
      else if (ADDR_PHASE < count.read() && count.read() < DATA_PHASE)
	{
          // drive data
	  frame_n  = SC_LOGIC_0;
	  devsel_n = SC_LOGIC_Z;
	  irdy_n   = SC_LOGIC_0;
	  trdy_n   = SC_LOGIC_Z;
	  stop_n   = SC_LOGIC_Z;
	  ad       = data + index;
	  cbe_n    = "0x0";
	  par      = SC_LOGIC_Z;

	  perr_n = SC_LOGIC_Z;
	  req_n  = SC_LOGIC_Z;
	  gnt_n  = 0;

          index += 1;
	}
      else if (count.read() == DATA_PHASE)
	{
	  frame_n  = SC_LOGIC_1;
	  devsel_n = SC_LOGIC_Z;
	  irdy_n   = SC_LOGIC_0;
	  trdy_n   = SC_LOGIC_1;
	  stop_n   = SC_LOGIC_Z;
	  ad       = data + index;
	  cbe_n    = "0x0";
	  par      = SC_LOGIC_Z;

	  perr_n = SC_LOGIC_Z;
	  req_n  = SC_LOGIC_Z;
	  gnt_n  = 0;
	}
      else if (count.read() < (DATA_PHASE + 2))
	{
	  frame_n  = SC_LOGIC_1;
	  devsel_n = SC_LOGIC_Z;
	  irdy_n   = SC_LOGIC_1;
	  trdy_n   = SC_LOGIC_1;
	  stop_n   = SC_LOGIC_Z;
	  ad       = "0x0";
	  cbe_n    = "0xF";
	  par      = SC_LOGIC_Z;

	  perr_n = SC_LOGIC_Z;
	  req_n  = SC_LOGIC_Z;
	  gnt_n  = 0;
	}
      else
	{
	  frame_n  = SC_LOGIC_Z;
	  devsel_n = frame_n;
	  irdy_n   = SC_LOGIC_Z;
	  trdy_n   = frame_n;
	  stop_n   = SC_LOGIC_Z;
	  ad       = Z32BITS;
	  cbe_n    = Z4BITS;
	  par      = SC_LOGIC_Z;

	  perr_n = SC_LOGIC_Z;
	  req_n  = SC_LOGIC_Z;
	}

      count = count.read() + 1;
    }
}

void TestBench::DataMonitor()
{
  if (ad.read() == "0x90000000" && cbe_n.read() == "0x7")
    {
      monitor = 1;
      index = 0;
    }

  if (monitor && irdy_n.read() == 0 && trdy_n.read() == 0)
    {
      //cout << "@" << sc_simulation_time() << ", data[" << index << "] = " << hex << ad.read() << dec << endl;
      outdata[index ++] = ad.read();
    }
}

bool TestBench::CheckData()
{
  bool check = true;

  for(int i = 0; i < 16; i ++)
    if (outdata[i] != 0xcafe0000 + i)
      check = false;

  return check;
}
