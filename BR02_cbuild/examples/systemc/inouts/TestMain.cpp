// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "TestBench.h"
#include "libpci.systemc.h"

int sc_main (int argc , char *argv[])
{
  TestBench tb("tb");
  pci dut("dut");

  sc_signal<bool > rst_n;
  sc_clock clk("clk", 100, SC_NS, 0.5);

#ifdef USE_PORT_OVERRIDE
  sc_signal<sc_int<32> > ad_out;
#else
  sc_signal<sc_uint<32> > ad_out;
#endif
  sc_signal<bool >        idsel;
  sc_signal<bool >        serr_n;
  sc_signal<bool >        gnt_n;
  sc_signal<bool >        inta_n;

  sc_signal_resolved  frame_n;
  sc_signal_resolved  devsel_n;
  sc_signal_resolved  irdy_n;
  sc_signal_resolved  trdy_n;
  sc_signal_resolved  stop_n;
  sc_signal_rv<32>    ad;
  sc_signal_rv<4>     cbe_n;
  sc_signal_resolved  par;
  sc_signal_resolved  perr_n;
  sc_signal_resolved  req_n;

  tb.rst_n    ( rst_n );
  tb.clk      ( clk );
  tb.ad       ( ad );
  tb.ad_out   ( ad_out );
  tb.cbe_n    ( cbe_n );
  tb.par      ( par );
  tb.frame_n  ( frame_n );
  tb.trdy_n   ( trdy_n );
  tb.irdy_n   ( irdy_n );
  tb.stop_n   ( stop_n );
  tb.devsel_n ( devsel_n );
  tb.idsel    ( idsel );
  tb.req_n    ( req_n );
  tb.gnt_n    ( gnt_n );
  tb.inta_n   ( inta_n );
  tb.perr_n   ( perr_n );
  tb.serr_n   ( serr_n );

  dut.rst_n    ( rst_n );
  dut.clk      ( clk );
  dut.ad       ( ad );
  dut.ad_out   ( ad_out );
  dut.cbe_n    ( cbe_n );
  dut.par      ( par );
  dut.perr_n   ( perr_n );
  dut.frame_n  ( frame_n );
  dut.trdy_n   ( trdy_n );
  dut.irdy_n   ( irdy_n );
  dut.stop_n   ( stop_n );
  dut.devsel_n ( devsel_n );
  dut.idsel    ( idsel );
  dut.req_n    ( req_n );
  dut.gnt_n    ( gnt_n );
  dut.inta_n   ( inta_n );
  dut.serr_n   ( serr_n );


  // Run test
  rst_n = 0;

  sc_start(330, SC_NS);
  rst_n = 1;

  sc_start(6000, SC_NS);

  cout << "At " << sc_time_stamp() << ", ";
  sc_stop();

  if (tb.CheckData())
    {
      cout << "Test Passed!" << endl;
      cout << "  TestBench detected correct data." << endl;
    }
  else
    {
      cout << "Test Failed!" << endl;
      cout << "  TestBench detected incorrect data." << endl;
    }

  return 0;
}
