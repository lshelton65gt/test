module pads (
  clk,
  clk_in,

  rst_n,
  rst_n_in,

  ad,
  ad_in,
  ad_out,
  ad_en,

  cbe_n,
  cbe_n_in,
  cbe_n_out,
  cbe_n_en,

  par,
  par_in,
  par_out,
  par_en,

  perr_n,
  perr_n_in,
  perr_n_out,
  perr_n_en,

  frame_n,
  frame_n_in,
  frame_n_out,
  frame_n_en,

  trdy_n,
  trdy_n_in,
  trdy_n_out,
  trdy_n_en,

  irdy_n,
  irdy_n_in,
  irdy_n_out,
  irdy_n_en,

  stop_n,
  stop_n_in,
  stop_n_out,
  stop_n_en,

  devsel_n,
  devsel_n_in,
  devsel_n_out,
  devsel_n_en,

  idsel,
  idsel_in,

  serr_n,
  serr_n_out,

  req_n,
  req_n_out,
  req_n_en,

  gnt_n,
  gnt_n_in,

  inta_n,
  inta_n_out
);

input          clk;
output         clk_in;

input          rst_n;
output         rst_n_in;

inout  [31: 0] ad;
output [31: 0] ad_in;
input  [31: 0] ad_out;
input  [31: 0] ad_en;

inout  [ 3: 0] cbe_n;
output [ 3: 0] cbe_n_in;
input  [ 3: 0] cbe_n_out;
input  [ 3: 0] cbe_n_en;

inout          par;
output         par_in;
input          par_out;
input          par_en;

inout          perr_n;
output         perr_n_in;
input          perr_n_out;
input          perr_n_en;

inout          frame_n;
output         frame_n_in;
input          frame_n_out;
input          frame_n_en;

inout          trdy_n;
output         trdy_n_in;
input          trdy_n_out;
input          trdy_n_en;

inout          irdy_n;
output         irdy_n_in;
input          irdy_n_out;
input          irdy_n_en;

inout          stop_n;
output         stop_n_in;
input          stop_n_out;
input          stop_n_en;

inout          devsel_n;
output         devsel_n_in;
input          devsel_n_out;
input          devsel_n_en;

input          idsel;
output         idsel_in;

inout          req_n;        // tri-state
input          req_n_out;
input          req_n_en;

input          gnt_n;
output         gnt_n_in;

output         serr_n;       // o/d
input          serr_n_out;

output         inta_n;       // o/d
input          inta_n_out;

//reg            req_n;


// behavioural port driving description
assign clk_in = clk;

assign rst_n_in = rst_n;

assign ad[0]  = ad_en[0] ?  ad_out[0] : 1'bz;
assign ad[1]  = ad_en[1] ?  ad_out[1] : 1'bz;
assign ad[2]  = ad_en[2] ?  ad_out[2] : 1'bz;
assign ad[3]  = ad_en[3] ?  ad_out[3] : 1'bz;
assign ad[4]  = ad_en[4] ?  ad_out[4] : 1'bz;
assign ad[5]  = ad_en[5] ?  ad_out[5] : 1'bz;
assign ad[6]  = ad_en[6] ?  ad_out[6] : 1'bz;
assign ad[7]  = ad_en[7] ?  ad_out[7] : 1'bz;
assign ad[8]  = ad_en[8] ?  ad_out[8] : 1'bz;
assign ad[9 ] = ad_en[9] ?  ad_out[9] : 1'bz;
assign ad[10] = ad_en[10] ? ad_out[10] : 1'bz;
assign ad[11] = ad_en[11] ? ad_out[11] : 1'bz;
assign ad[12] = ad_en[12] ? ad_out[12] : 1'bz;
assign ad[13] = ad_en[13] ? ad_out[13] : 1'bz;
assign ad[14] = ad_en[14] ? ad_out[14] : 1'bz;
assign ad[15] = ad_en[15] ? ad_out[15] : 1'bz;
assign ad[16] = ad_en[16] ? ad_out[16] : 1'bz;
assign ad[17] = ad_en[17] ? ad_out[17] : 1'bz;
assign ad[18] = ad_en[18] ? ad_out[18] : 1'bz;
assign ad[19] = ad_en[19] ? ad_out[19] : 1'bz;
assign ad[20] = ad_en[20] ? ad_out[20] : 1'bz;
assign ad[21] = ad_en[21] ? ad_out[21] : 1'bz;
assign ad[22] = ad_en[22] ? ad_out[22] : 1'bz;
assign ad[23] = ad_en[23] ? ad_out[23] : 1'bz;
assign ad[24] = ad_en[24] ? ad_out[24] : 1'bz;
assign ad[25] = ad_en[25] ? ad_out[25] : 1'bz;
assign ad[26] = ad_en[26] ? ad_out[26] : 1'bz;
assign ad[27] = ad_en[27] ? ad_out[27] : 1'bz;
assign ad[28] = ad_en[28] ? ad_out[28] : 1'bz;
assign ad[29] = ad_en[29] ? ad_out[29] : 1'bz;
assign ad[30] = ad_en[30] ? ad_out[30] : 1'bz;
assign ad[31] = ad_en[31] ? ad_out[31] : 1'bz;

assign ad_in = ad;

assign cbe_n = cbe_n_en ? cbe_n_out : 4'hz;
assign cbe_n_in = cbe_n;

assign par = par_en ? par_out : 1'hz;
assign par_in = par;

assign perr_n = perr_n_en ? perr_n_out : 1'hz;
assign perr_n_in = perr_n;

assign frame_n = frame_n_en ? frame_n_out : 1'hz;
assign frame_n_in = frame_n;

assign trdy_n = trdy_n_en ? trdy_n_out : 1'hz;
assign trdy_n_in = trdy_n;

assign irdy_n = irdy_n_en ? irdy_n_out : 1'hz;
assign irdy_n_in = irdy_n;

assign stop_n = stop_n_en ? stop_n_out : 1'hz;
assign stop_n_in = stop_n;

assign devsel_n = devsel_n_en ? devsel_n_out : 1'hz;
assign devsel_n_in = devsel_n;

assign idsel_in = idsel;

assign req_n = req_n_en ? req_n_out : 1'bz;

assign gnt_n_in = gnt_n;

assign serr_n = serr_n_out;

assign inta_n = inta_n_out;


endmodule
