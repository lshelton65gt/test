
# Set SystemC Home
SYSTEMC_HOME ?= $(CARBON_HOME)/systemc

# Compiler options
CFLAGS += -g -Wall -I. -I$(CARBON_HOME)/include -I$(SYSTEMC_HOME)/include
# To dump waveform, uncomment the following line
#CFLAGS += -DCARBON_DUMP_FSDB=1
# To enable carbon sc_trace, uncomment the following line
#CFLAGS += -DCARBON_SC_TRACE


CBUILDFLAGS := -v2k -enableOutputSysTasks
# To observe all the carbon module (RTL) signals, uncomment the following line
CBUILDFLAGS += -directive all.dir


SYSTEMC_LIB_LIST = -L${SYSTEMC_HOME}/lib-linux -lsystemc -lstdc++ -lm

# Source files
RTL_SRCS := pci.v Lpra32x32.v Fifo32x32.v read.v write.v core.v pads.v
SRCS := libpci.systemc.cpp TestBench.cpp TestMain.cpp

OBJS := $(SRCS:%.cpp=%.o)

ifdef CARBON_HOME
	include $(CARBON_HOME)/makefiles/Makefile.common
endif


all: check_env check_log

ifdef CARBON_HOME
ifdef SYSTEMC_HOME
check_env:
else
check_env:
	@echo Environment variable SYSTEMC_HOME is not set ...
	@exit 1
endif
else
check_env:
	@echo Environment variable CARBON_HOME is not set ...
	@exit 1
endif


check_log: Test.ccfg.log
	diff Test.gld $^

Test.ccfg.log: Test.exe
	./$< > $@ 2> /dev/null


# Explicit dependencies
libpci.systemc.cpp: libpci.a
TestBench.cpp: libpci.a
TestMain.cpp: libpci.a


Test.exe: $(OBJS) libpci.a
	$(CARBON_LINK) $(CFLAGS) -o $@ $^ $(CARBON_LIB_LIST) $(SYSTEMC_LIB_LIST)

libpci.systemc.cpp libpci.systemc.h : libpci.a
	$(CARBON_HOME)/bin/carbon systemCWrapper -ccfg pci.ccfg

libpci.a: $(RTL_SRCS)
	$(CARBON_CBUILD) $(CBUILDFLAGS) -o $@ $(RTL_SRCS)

%.o: %.cpp
	$(CARBON_CC) $(CFLAGS) -o $@ -c $<

clean::
	rm -rf *.out *.exe lib*.* .carbon* *.o *.fsdb cds_*.h *.log *.vcd
