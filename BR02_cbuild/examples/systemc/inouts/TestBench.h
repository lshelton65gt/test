// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __TestBench_h__
#define __TestBench_h__


#include "libpci.systemc.h"


class TestBench : public sc_module
{
public:
  SC_HAS_PROCESS(TestBench);

  TestBench(sc_module_name name);
  ~TestBench();

  // port declarations
  sc_in<bool >  rst_n;
  sc_in<bool >  clk;

  sc_out<bool >        idsel;
  sc_inout<sc_logic >  frame_n;
  sc_inout<sc_logic >  devsel_n;
  sc_inout<sc_logic >  irdy_n;
  sc_inout<sc_logic >  trdy_n;
  sc_inout<sc_logic >  stop_n;
  sc_inout<sc_lv<32> > ad;
  sc_inout<sc_lv<4> >  cbe_n;
  sc_inout<sc_logic >  par;

  sc_inout<sc_logic >  perr_n;
  sc_in<bool >         serr_n;
  sc_inout<sc_logic >  req_n;
  sc_out<bool >        gnt_n;
  sc_in<bool >         inta_n;

#ifdef USE_PORT_OVERRIDE
  sc_in<sc_int<32> > ad_out;
#else
  sc_in<sc_uint<32> > ad_out;
#endif

  sc_signal<sc_uint<8> > count;
  sc_uint<32> data;
  sc_uint<8> index;
  bool monitor;

  void StimulusDrive();
  void DataMonitor();

  bool CheckData();

  sc_uint<32> outdata[16];
};


#endif
