// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __TestMaster_h__
#define __TestMaster_h__

#include <systemc.h>
#include "tlm.h"
#include "xactors/systemc/CarbonXEnetIfs.h"

//! Transaction Master module for the XAUI transactor
/*!
  This example shows how to generate a list of XAUI transactions,
  send them over the interface and check them as they come back.
  Signal level loopback of the transmited data directly back
  to the receive data of the transactor is also shown in a
  commented out block of code that can uncommented by the user.
*/
SC_MODULE(TestMaster) {
  
  // Constructor
  SC_CTOR(TestMaster);
  
  // Destructor
  ~TestMaster();

  //! TX Port, used to send transactions to the transactor
  sc_port<tlm::tlm_master_if<CarbonXTransReqT, CarbonXTransRespT>  > tx_port;

  //! RX Port, used to get transactions from the transactor
  sc_port<tlm::tlm_master_if<CarbonXTransReqT, CarbonXTransRespT>  > rx_port;

  //! Tx Process
  /*!
    The tx Process sends transactions to the transactor.
  */
  void txProcess();

  //! Rx Process
  /*!
    The rx Process waits for transactions from the transactor
    and checks to see if they are as expected.
  */
  void rxProcess();
  
 private:

  // Member variables

  //! Total number of packets to send and to receive
  unsigned int   mPktCount;

  //! List of packet descriptors to send. Also used to check received packets 
  ENET_PKT_T   * mPktList;

  //! Number of packets received so far
  unsigned int   mPktsRecvd;

  //! Number of packets sent so far
  unsigned int   mPktsSent;
  
  //! Base Size of the first transaction,
  //! Every transacion sent adds one byte.
  static const UInt32 mTxPktLen = 65; 

  //! Event used to notify the TX process when the link is up
  sc_event       link_up;

  //! Build a list of descriptors to send
  void buildDescriptorList();

};
    
#endif
