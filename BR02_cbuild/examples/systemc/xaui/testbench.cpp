/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "systemc.h"
#include "libdesign.systemc.h"
#include "xactors/carbonX.h"
#include "TestMaster.h"
#include "Loopback.h"

#include "xactors/systemc/CarbonXTransLoopSysC.h"
#include "xactors/systemc/CarbonXTlmFifo.h"
#include "xactors/systemc/CarbonXTlmReqRspChannel.h"

#include <cassert>

using namespace std;
using namespace tlm;

// -------------------- TestBench --------------------

SC_MODULE(testbench)
{
  sc_clock                              clk;

  sc_clock         	                sys_clk;
  sc_signal<bool> 	                sys_reset_l;
  sc_signal<sc_uint<12> >               path_latency;

  // Test Module
  TestMaster                            test_master;
  
  // Loopback Module
  Loopback                              loopback_mod;

  // XAUI Transactor 0
  CarbonXTransLoopSysC tx0_enet_xtor;
  CarbonXTransLoopSysC rx0_enet_xtor;
  CarbonXTlmReqRspChannel<CarbonXTransReqT, CarbonXTransRespT> tx0_req_resp;
  CarbonXTlmReqRspChannel<CarbonXTransReqT, CarbonXTransRespT> rx0_req_resp;

  // XAUI Transactor 1
  CarbonXTransLoopSysC tx1_enet_xtor;
  CarbonXTransLoopSysC rx1_enet_xtor;
  CarbonXTlmReqRspChannel<CarbonXTransReqT, CarbonXTransRespT> tx1_req_resp;
  CarbonXTlmReqRspChannel<CarbonXTransReqT, CarbonXTransRespT> rx1_req_resp;
  
  // Carbon Model Object
  xauiTb *pEnetTb;
  
  // Timeout event
  sc_event ev_timeout;

  // Contructor
  testbench(sc_module_name name);

  // Initilization routine
  void init();

  // Timeout function
  void timeout();

  ~testbench();

  SC_HAS_PROCESS(testbench);
};

testbench::testbench(sc_module_name name)
: sc_module(name)
, sys_clk("sys_clk", 20, 0.5, 0, false)
, sys_reset_l("sys_reset_l")
, path_latency("path_latency")
, test_master("test_master")
, loopback_mod("loopback_mod")
, tx0_enet_xtor("tx0_enet_xtor", "xauiTb.XAUI0.TX_XTOR")
, rx0_enet_xtor("rx0_enet_xtor", "xauiTb.XAUI0.RX_XTOR")
, tx0_req_resp("tx0_req_resp")
, rx0_req_resp("rx0_req_resp")
, tx1_enet_xtor("tx1_enet_xtor", "xauiTb.XAUI1.TX_XTOR")
, rx1_enet_xtor("rx1_enet_xtor", "xauiTb.XAUI1.RX_XTOR")
, tx1_req_resp("tx1_req_resp")
, rx1_req_resp("rx1_req_resp")
{
  cout << name << ":XAUI SystemC Ethernet testbench starting" << endl;

  // Create Sim Infrastructure.  testBench, Clocks, and Reset
  // Handle to main simulation object
  cout << "\n============== create carbon testBench  ==============\n" << endl;	
  pEnetTb = new xauiTb("pEnetTb");
  assert(pEnetTb);

  // Hook up clk and reset to the Carbon Model
  pEnetTb->sys_clk(sys_clk);
  pEnetTb->sys_reset_l(sys_reset_l); 
  pEnetTb->path_latency(path_latency);

  // Connect Master Test to Transactor 0
  test_master.tx_port(tx0_req_resp.master_export);
  tx0_enet_xtor.trans_port(tx0_req_resp.slave_export);
  test_master.rx_port(rx0_req_resp.master_export);
  rx0_enet_xtor.trans_port(rx0_req_resp.slave_export);

  // Connect Loopback Test to Transactor 1
  loopback_mod.tx_port(tx1_req_resp.master_export);
  tx1_enet_xtor.trans_port(tx1_req_resp.slave_export);
  loopback_mod.rx_port(rx1_req_resp.master_export);
  rx1_enet_xtor.trans_port(rx1_req_resp.slave_export);
  
  cout << "=============== Start writing VCD file ===================\n" << endl;

  // wave dumping
  // ------------
  pEnetTb->carbonSCWaveInitVCD("carbon.vcd", SC_NS);
  pEnetTb->carbonSCDumpVars();

  SC_THREAD(init);
  SC_METHOD(timeout);
  sensitive << ev_timeout;
  dont_initialize();
}

// Initialize Testbench
void testbench::init() {
  // Set Path Latency in verilog testbench
  // This is the number of cycles the XAUI signals
  // take from the output of the transmitter to the
  // input of the receiver.
  path_latency.write(2);

  // Setup Timeout to 100000 ns
  ev_timeout.notify(100000, SC_NS);

  // Wait for a litte bit and then deassert reset
  wait(100, SC_NS);
  sys_reset_l.write(true);

}

// Timeout method
// This method will only be called if something has gone wrong and the test has hung
void testbench::timeout() {
  SC_REPORT_ERROR("/testbench/", "Test timed out!");
  sc_stop();
}
 
// Destructor
testbench::~testbench()
{
  pEnetTb->carbonSCDumpFlush();
  delete pEnetTb;
}

// *****************
//   SystemC  Main   
// *****************

int sc_main (int argc , char *argv[]) 
{
  testbench *tb;
  tb = new testbench("XAUI_Ethernet_Testbench");

  sc_start();

  delete tb;
  return 0;
}

