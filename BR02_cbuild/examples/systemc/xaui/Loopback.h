// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __Loopback_h__
#define __Loopback_h__

#include <systemc.h>
#include "tlm.h"
#include "xactors/systemc/CarbonXEnetIfs.h"

//! This is an example of how to implement transaction level loopback
//! using the XAUI Transactor. Signal level Loopback is also shown in
//! a commented out block of code that can be uncommented by the user.
SC_MODULE(Loopback) {
  
  // Constructor
  SC_CTOR(Loopback);
  
  // Destructor
  ~Loopback();

  //! TX Port, used to send transactions to the transactor
  sc_port<tlm::tlm_master_if<CarbonXTransReqT, CarbonXTransRespT>  > tx_port;

  //! RX Port, used to get transactions from the transactor
  sc_port<tlm::tlm_master_if<CarbonXTransReqT, CarbonXTransRespT>  > rx_port;
  
  // Processes
  void loopbackProcess();
  
 private:

};
    
#endif
