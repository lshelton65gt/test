/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "TestMaster.h"
using namespace std;
using namespace tlm;

TestMaster::TestMaster(sc_module_name inst) : 
  sc_module(inst),
  tx_port("tx_port"),
  rx_port("rx_port")
{
  
  SC_THREAD(txProcess);
  SC_THREAD(rxProcess);
  
  // Default values
  mPktsRecvd     = 0;
  mPktsSent      = 0;
  mPktCount      = 10;

  // Build Descriptor List
  mPktList = new ENET_PKT_T[mPktCount];
  assert(mPktList);
  buildDescriptorList();
}

void TestMaster::txProcess() {
  cout << "Starting txProcess" << endl;

  CarbonXEnetTransReqT trans;
  
  // Configure Transactors
  trans.csWrite(XGMII_FRTR_CONFIG, XGMII_FCS_ENABLE);
  tx_port->put(trans);
  
  // Reset Transactor
  trans.resetBFM();
  tx_port->put(trans);
  
  // Wait for response from reset transaction
  CarbonXTransRespT rsp;
  tx_port->get(rsp);

  // Wait for Link to be up
  wait(link_up);

  for (; mPktsSent < mPktCount; mPktsSent++) {
    // Build Packet from Descriptor
    trans.buildPacket(mPktList[mPktsSent]);
    trans.setTransId(mPktsSent);
    
    // Send Packet
    cout << "Sending Packet...." << endl;
    tx_port->put(trans);
  }

}

void TestMaster::rxProcess(){
  
  CarbonXEnetTransReqT   request;
  CarbonXEnetTransRespT  response;
  
  cout << "Starting rxProcess" << endl;
  
  // Mark Transactions and Config Transactions so we can throw the responses away
  request.setTransId(0x80000000);
  
  // Configure Transactors
  request.csWrite(XGMII_FRTR_CONFIG, XGMII_FCS_ENABLE);
  rx_port->nb_put(request);
  
  // To Loopback Transactor 0 back to itself, uncomment the next two lines
  /*
  request.csWrite(XAUI_RX_CONFIG, XAUI_RX_LOOPBACK_ENABLE);
  rx_port->nb_put(request);
  */

  CarbonUInt32 linkStatus = 0;
  while(linkStatus != 0xf) {
    // Send Read Request
    request.csReadReq(XAUI_LINK_STATUS);
    rx_port->put(request);
    
    // Get Link Status Response
    rx_port->get(response);
    linkStatus = response.read32();
    cout << "Waiting for Link to be ready: " << linkStatus << endl;
  }
  link_up.notify();
  
  // Tell Transactor to start receiving packets
  request.readArray32Req(0, 0); // Hdl side will return size
  request.setRepeatCnt(0);      // Repeat receiving Transactions until further notice
  
  // Mark Transaction so we recognize it
  request.setTransId(0x1234);
  
  // Send the request to receive transactions
  rx_port->nb_put(request);
  
  // Check all Packets currently available
  for(; mPktsRecvd < mPktCount;) {
    rx_port->get(response); 
    cout << "rxprocess: Got Response with ID: 0x" 
         << hex << response.getTransId() << endl;

    if(response.getTransId() == 0x1234) {
      cout << "rxprocess: Checking Packet number "
           << dec << mPktsRecvd << endl;
      response.checkPacket(mPktList[mPktsRecvd++]);
    }
  }

  // When all packets are received, end simulation
  cout << endl << "Pkts received = "   << dec << mPktsRecvd 
       << " Total Packets Expected = " << mPktCount << endl;

  if(mPktsRecvd >= mPktCount){

     u_int32 numErrs = tbp_get_error_count();
                       cout << "**********" << endl;
     if (numErrs == 0) cout << "** PASS **" << endl;
     else              cout << "** FAIL **" << endl;
                       cout << "**********" << endl;

     cout << "All packets received -- Ending simulation" << endl;
     sc_stop();
  }  
}

//----------------------------------------------------------------------
// This function builds a list of descriptors
// onto a named queue for use by a transmit function
//----------------------------------------------------------------------
void TestMaster::buildDescriptorList(){
  u_int32 i, j;
  ENET_PKT_T enet;
 
  cout << "build_descriptor_list(): creating " << dec << mPktCount << " packets." << endl;

  // Create descriptors, fill in fields and add them to the tx queue
  for(i=0; i < mPktCount; i++) {
    
    cout << "Building packet " << dec << i << " ... " << endl;

    // enable the ethernet header and fill in the fields
    enet.sa = 0xdeadfacefeedLL;
    enet.da = 0x112233445566LL;
    enet.typelength = mTxPktLen +i;
    enet.pl_size    = mTxPktLen + i;    

    // Disable the VLAN Header
    enet.vlan.enable = 0;

    // Allocate Memory for payload buffer
    enet.payload = new u_int8[mTxPktLen +i];
    assert(enet.payload);
 
    // There is only a pdu in this example, so just fill in the
    // pdu fields that interest us.
    for (j = 0; j < mTxPktLen +i; j+= 1) enet.payload[j] = j;
    enet.ipg = 5;
    mPktList[i] = enet;

  } //--end of for(i=0; i<pkt_count; i++)

}


TestMaster::~TestMaster() {
  delete mPktList;
  
  // Make sure that all expected transactions has been received, when test is destroyed
  if(mPktsRecvd < mPktCount) {
    MSG_Error("Didn't receive all packets. Received %d, Expected %d\n", mPktsRecvd, mPktCount);
  }
}
