/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
`timescale 1ns / 100 ps

// This module acts as a verilog testbench instantiating two
// XAUI transactors and connecting them up to each other with
// a latency fifo in between. The latency of the fifo can be
// controlled with the path_latency port that can be set by
// the SystemC testbench.
module xauiTb(sys_clk, sys_reset_l, path_latency);
   
   input          sys_clk;      // System Clock
   input          sys_reset_l;  // System Reset
   input [11:0]   path_latency; // Number of cycles of Latency between the 
   //                              transactors in the testbench

   wire [9:0] 	  x0SL0;
   wire [9:0] 	  x0SL1;
   wire [9:0] 	  x0SL2;
   wire [9:0] 	  x0SL3;
   
   wire [9:0] 	  x0DL0;
   wire [9:0] 	  x0DL1;
   wire [9:0] 	  x0DL2;
   wire [9:0] 	  x0DL3;
   
   wire [9:0] 	  x1SL0;
   wire [9:0] 	  x1SL1;
   wire [9:0] 	  x1SL2;
   wire [9:0] 	  x1SL3;
   
   wire [9:0] 	  x1DL0;
   wire [9:0] 	  x1DL1;
   wire [9:0] 	  x1DL2;
   wire [9:0] 	  x1DL3;
   
   carbonx_enetxaui XAUI0
     (.SCLK(sys_clk),
      .DCLK(sys_clk),
      .sys_reset_l(sys_reset_l),
      
      // XAUI Input Ports
      .SL0(x0SL0),
      .SL1(x0SL1),
      .SL2(x0SL2),
      .SL3(x0SL3),
      
      // XAUI Output Ports
      .DL0(x0DL0),
      .DL1(x0DL1),
      .DL2(x0DL2),
      .DL3(x0DL3));
   
   carbonx_enetxaui XAUI1
     (.SCLK(sys_clk),
      .DCLK(sys_clk),
      .sys_reset_l(sys_reset_l),
      
      // XAUI Input Ports
      .SL0(x1SL0),
      .SL1(x1SL1),
      .SL2(x1SL2),
      .SL3(x1SL3),
      
      // XAUI Output Ports
      .DL0(x1DL0),
      .DL1(x1DL1),
      .DL2(x1DL2),
      .DL3(x1DL3));

   lat_fifo f1
     (sys_clk, sys_reset_l, path_latency,
      x0DL0, x0DL1, x0DL2, x0DL3,
      x1SL0, x1SL1, x1SL2, x1SL3);

   lat_fifo f2
     (sys_clk, sys_reset_l, path_latency,
      x1DL0, x1DL1, x1DL2, x1DL3,
      x0SL0, x0SL1, x0SL2, x0SL3);
      
		         
endmodule   

// Latency Fifo, delays input with path_latency cyclse
// before outputing the data. If path_latency is changed
// the module will require a reset for the new latency
// to take affect.
module lat_fifo(clk, reset_l, path_latency,
		 in0, in1, in2, in3,
		 out0, out1, out2, out3);

   input        clk;
   input        reset_l;
   input [11:0] path_latency;
   input [9:0]  in0,  in1,  in2,  in3;
   output [9:0] out0, out1, out2, out3;

   reg [9:0] 	out0, out1, out2, out3;
   reg [11:0] 	rd_ptr;
   reg [11:0] 	wr_ptr;

   reg [40:0] 	fifo_mem[4095:0];
   
   always @(posedge clk or negedge reset_l) begin
      if(reset_l == 0) begin
	 wr_ptr <= path_latency;
	 rd_ptr <= 0;
	 out0   <= 0;
	 out1   <= 0;
	 out2   <= 0;
	 out3   <= 0;
      end
      else begin
	 // Update Read and Write Pointers
	 wr_ptr <= wr_ptr + 1;
	 rd_ptr <= rd_ptr + 1;

	 // Write Fifo
	 fifo_mem[wr_ptr] <= {in3, in2, in1, in0};

	 // Read Fifo
	 {out3, out2, out1, out0} <= fifo_mem[rd_ptr];
      end
   end
   
endmodule
