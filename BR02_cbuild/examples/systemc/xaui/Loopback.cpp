/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "Loopback.h"

Loopback::Loopback(sc_module_name inst) : 
  sc_module(inst),
  tx_port("tx_port"),
  rx_port("rx_port")
{  
  SC_THREAD(loopbackProcess);
}

// This thread receives transactions from the RX Transactor,
// converts them into requests and sends them on the TX Tramsactor
void Loopback::loopbackProcess(){
  
  CarbonXEnetTransReqT   request;
  CarbonXEnetTransRespT  response;

  cout << "Loopback Process" << endl;

  // Mark Transactions and Config Transactions so we can through them away
  request.setTransId(0x80000000);
  
  // Configure Transactors
  request.csWrite(XGMII_FRTR_CONFIG, XGMII_FCS_ENABLE);
  rx_port->nb_put(request);
  tx_port->nb_put(request);
    
  // To Loopback on the signal level instead of the transaction level, uncomment the next two lines
  /*
  request.csWrite(XAUI_TX_CONFIG, XAUI_TX_LOOPBACK_ENABLE);
  tx_port->nb_put(request);
  */

  // Tell RX Transactor to start receiving packets
  request.readArray32Req(0, 0); // Hdl side will return size
  request.setRepeatCnt(0);      // Repeat receiving Transactions until further notice
  
  // Mark Transaction so we recognize it
  request.setTransId(0x1234);
  rx_port->nb_put(request);

  // Just loop and send all packets receivied.
  while(1) {
    rx_port->get(response); 
    if(response.getTransId() == 0x1234) {
      cout << "Loopback: Got Packet with Size: " << dec << response.getSize() 
           << " Sending it back!" << endl;
      
      request.transaction(CARBONX_OP_WRITE,
                          response.getAddress(), response.getData(), 
                          response.getSize(), response.getStatus(), response.getWidth());
      tx_port->nb_put(request);
    }
  }
}

Loopback::~Loopback(){
}
