// -*- C++ -*-
/******************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "HostAppln.h"
#include "xactors/usb/carbonXUsbDescriptors.h"

// Configuration and test sequence
void HostAppln::test()
{
    // Wait for device attachment, and then enumerate attached device
    while (1)
    {
        wait(rxCommandEvent);
        CarbonXUsbCommand command = rxCommandQueue.front();
        rxCommandQueue.pop();
        if (command == CarbonXUsbCommand_DeviceAttach)
            break;
    }
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Device attach detected. Starting device enumeration ..." << endl;
    host->enumerate();

    // Configuration sequence
    cout << "**** Starting Device Configuration ****" << endl;
    CarbonXUsbTrans *trans = NULL;
    CarbonXUsbPipe *defaultControlPipe = NULL;

    // 1. Wait for SET_ADDRESS response
    while (1)
    {
        wait(rxTransEvent);
        trans = rxTransQueue.front();
        rxTransQueue.pop();
        defaultControlPipe = carbonXUsbTransGetPipe(trans);
        if (carbonXUsbPipeGetEpNumber(defaultControlPipe) == 0 &&
                (carbonXUsbTransGetRequestType(trans) << 8 |
                 carbonXUsbTransGetRequest(trans)) == 0x0005)
            break;
        carbonXUsbTransDestroy(trans);
    }
    CarbonUInt7 deviceAddress = carbonXUsbTransGetRequestValue(trans) & 0x7F;
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Device is enumerated to " << static_cast<int>(deviceAddress) <<
        " address." << endl;
    carbonXUsbTransDestroy(trans);

    // 2. Start GET_DESCRIPTOR(DeviceDescriptor) transfer
    trans = carbonXUsbTransCreate(defaultControlPipe);
    carbonXUsbTransSetRequestType(trans, 0x80);
    carbonXUsbTransSetRequest(trans, 6);
    carbonXUsbTransSetRequestValue(trans, CarbonXUsbDescriptor_DEVICE << 8);
    carbonXUsbTransSetRequestIndex(trans, 0);
    carbonXUsbTransSetRequestLength(trans, 18);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Starting GET_DESCRIPTOR(DeviceDescriptor) transfer ..." << endl;
    host->startTransExport->startNewTransaction(trans);

    // 3. Wait for GET_DESCRIPTOR(DeviceDescriptor) response
    while (1)
    {
        wait(rxTransEvent);
        trans = rxTransQueue.front();
        rxTransQueue.pop();
        defaultControlPipe = carbonXUsbTransGetPipe(trans);
        if (carbonXUsbPipeGetEpNumber(defaultControlPipe) == 0 &&
                (carbonXUsbTransGetRequestType(trans) << 8 |
                 carbonXUsbTransGetRequest(trans)) == 0x8006 &&
                ((carbonXUsbTransGetRequestValue(trans) >> 8) & 0xFF) ==
                CarbonXUsbDescriptor_DEVICE)
            break;
        carbonXUsbTransDestroy(trans);
    }
    CarbonXUsbDeviceDescriptor *deviceDescriptor =
        carbonXUsbGetDeviceDescriptorFromBytes(carbonXUsbTransGetData(trans));
    assert(deviceDescriptor);
    CarbonUInt8 numConfigs = deviceDescriptor->bNumConfigurations;
    assert(numConfigs > 0);
    carbonXUsbDestroyDeviceDescriptor(deviceDescriptor);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Device descriptor received with " << static_cast<int>(numConfigs) <<
        " configurations." << endl;
    carbonXUsbTransDestroy(trans);

    // 4. Start GET_DESCRIPTOR(ConfigurationDescriptor) transfers
    CarbonXUsbConfigurationDescriptor *configDescriptorList[256];
    for (CarbonUInt8 i = 0; i < numConfigs; i++)
    {
        trans = carbonXUsbTransCreate(defaultControlPipe);
        carbonXUsbTransSetRequestType(trans, 0x80);
        carbonXUsbTransSetRequest(trans, 6);
        CarbonUInt8 configIndex = i + 1;
        carbonXUsbTransSetRequestValue(trans,
                CarbonXUsbDescriptor_CONFIGURATION << 8 | configIndex);
        carbonXUsbTransSetRequestIndex(trans, 0);
        carbonXUsbTransSetRequestLength(trans, 0xFFFF);
        cout << name() << ": " << sc_time_stamp() << ": " <<
            "Starting GET_DESCRIPTOR(ConfigurationDescriptor, " <<
            static_cast<int>(configIndex) << ") transfer ..." << endl;
        host->startTransExport->startNewTransaction(trans);

        configDescriptorList[i] = NULL;
    }

    // 5. Wait for GET_DESCRIPTOR(ConfigurationDescriptor) responses
    CarbonUInt16 activeConfigNumber = 0;
    for (CarbonUInt8 i = 0; i < numConfigs; i++)
    {
        while (1)
        {
            wait(rxTransEvent);
            trans = rxTransQueue.front();
            rxTransQueue.pop();
            CarbonXUsbPipe *pipe = carbonXUsbTransGetPipe(trans);
            if (carbonXUsbPipeGetEpNumber(pipe) == 0 &&
                    (carbonXUsbTransGetRequestType(trans) << 8 |
                     carbonXUsbTransGetRequest(trans)) == 0x8006 &&
                    ((carbonXUsbTransGetRequestValue(trans) >> 8) & 0xFF) ==
                    CarbonXUsbDescriptor_CONFIGURATION)
                break;
            carbonXUsbTransDestroy(trans);
        }
        CarbonUInt8 configIndex = carbonXUsbTransGetRequestValue(trans) & 0xFF;
        cout << name() << ": " << sc_time_stamp() << ": " <<
            "Configuration descriptor received with " <<
            static_cast<int>(configIndex) << " index." << endl;
        assert(configDescriptorList[configIndex - 1] == NULL);
        configDescriptorList[configIndex - 1] =
            carbonXUsbGetConfigurationDescriptorFromBytes(
                    carbonXUsbTransGetData(trans));
        carbonXUsbTransDestroy(trans);

        if (activeConfigNumber == 0)
            activeConfigNumber = configIndex;
    }

    // 6. Select and start SET_CONFIGURATION(FirstConfigNumber) transfer
    trans = carbonXUsbTransCreate(defaultControlPipe);
    carbonXUsbTransSetRequestType(trans, 0x00);
    carbonXUsbTransSetRequest(trans, 9);
    carbonXUsbTransSetRequestValue(trans, activeConfigNumber & 0xFF);
    carbonXUsbTransSetRequestIndex(trans, 0);
    carbonXUsbTransSetRequestLength(trans, 0);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Starting SET_CONFIGURATION(" << static_cast<int>(activeConfigNumber) <<
        ") transfer ..." << endl;
    host->startTransExport->startNewTransaction(trans);

    // 7. Wait for SET_CONFIGURATION response
    while (1)
    {
        wait(rxTransEvent);
        trans = rxTransQueue.front();
        rxTransQueue.pop();
        defaultControlPipe = carbonXUsbTransGetPipe(trans);
        if (carbonXUsbPipeGetEpNumber(defaultControlPipe) == 0 &&
                (carbonXUsbTransGetRequestType(trans) << 8 |
                 carbonXUsbTransGetRequest(trans)) == 0x0009)
            break;
        carbonXUsbTransDestroy(trans);
    }
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "SET_CONFIGURATION complete. Creating pipes ..." << endl;
    carbonXUsbTransDestroy(trans);

    // 8. Create pipes of active configuration
    CarbonXUsbConfigurationDescriptor *activeConfig =
        configDescriptorList[activeConfigNumber - 1];
    CarbonXUsbPipe *bulkOutPipe = NULL, *bulkInPipe = NULL;
    for (CarbonUInt8 i = 0; i < activeConfig->bNumInterfaces; i++)
    {
        for (CarbonUInt8 j = 0;
                j < activeConfig->intfDescriptor[i]->bNumEndpoints; j++)
        {
            CarbonUInt8 *epDescriptor =
                carbonXUsbCreateBytesFromEndpointDescriptor(
                        activeConfig->intfDescriptor[i]
                        ->epDescriptor[j]);
            CarbonXUsbPipe *pipe =
                host->createPipe(deviceAddress, epDescriptor);
            carbonmem_free(epDescriptor);
            
            if (carbonXUsbPipeGetType(pipe) == CarbonXUsbPipe_BULK)
            {
                if (carbonXUsbPipeGetDirection(pipe) ==
                        CarbonXUsbPipeDirection_OUTPUT)
                {
                    cout << "Bulk OUT Pipe is created for EP " <<
                        static_cast<int>(carbonXUsbPipeGetEpNumber(pipe)) <<
                        "." << endl;
                    if (bulkOutPipe == NULL)
                        bulkOutPipe = pipe;
                }
                else if (carbonXUsbPipeGetDirection(pipe) ==
                        CarbonXUsbPipeDirection_INPUT)
                {
                    cout << "Bulk IN Pipe is created for EP " <<
                        static_cast<int>(carbonXUsbPipeGetEpNumber(pipe)) <<
                        "." << endl;
                    if (bulkInPipe == NULL)
                        bulkInPipe = pipe;
                }
            }
            else
                host->destroyPipe(pipe);
        }
    }
    cout << "**** Device Configuration Complete ****" << endl;

    // Test sequence
    cout << "**** Starting Test Sequence ****" << endl;

    // 1. Writing 100 data through Bulk OUT pipe
    CarbonUInt8 data[100];
    for (CarbonUInt32 i = 0; i < 100; i++)
        data[i] = i;
    trans = carbonXUsbTransCreate(bulkOutPipe);
    carbonXUsbTransSetData(trans, 100, data);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Starting Bulk OUT transfer for writing 100 bytes ..." << endl;
    host->startTransExport->startNewTransaction(trans);
    wait(rxTransEvent);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Transfer complete with " <<
        ((carbonXUsbTransGetStatus(trans) == CarbonXUsbStatus_SUCCESS) ?
         "SUCCESS" : "HALT") << " status." << endl;
    carbonXUsbTransDump(trans);
    carbonXUsbTransDestroy(trans);

    // 2. Reading all data through Bulk IN pipe
    trans = carbonXUsbTransCreate(bulkInPipe);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Starting Bulk IN transfer for reading ..." << endl;
    host->startTransExport->startNewTransaction(trans);
    wait(rxTransEvent);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Transfer complete with " <<
        ((carbonXUsbTransGetStatus(trans) == CarbonXUsbStatus_SUCCESS) ?
         "SUCCESS" : "HALT") << " status." << endl;
    carbonXUsbTransDump(trans);
    carbonXUsbTransDestroy(trans);

    // 3. Writing 50 data through Bulk OUT pipe
    for (CarbonUInt32 i = 0; i < 50; i++)
        data[i] = 50 - i;
    trans = carbonXUsbTransCreate(bulkOutPipe);
    carbonXUsbTransSetData(trans, 50, data);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Starting Bulk OUT transfer for writing 50 bytes ..." << endl;
    host->startTransExport->startNewTransaction(trans);
    wait(rxTransEvent);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Transfer complete with " <<
        ((carbonXUsbTransGetStatus(trans) == CarbonXUsbStatus_SUCCESS) ?
         "SUCCESS" : "HALT") << " status." << endl;
    carbonXUsbTransDump(trans);
    carbonXUsbTransDestroy(trans);

    // 4. Reading all data through Bulk IN pipe
    trans = carbonXUsbTransCreate(bulkInPipe);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Starting Bulk IN transfer for reading ..." << endl;
    host->startTransExport->startNewTransaction(trans);
    wait(rxTransEvent);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Transfer complete with " <<
        ((carbonXUsbTransGetStatus(trans) == CarbonXUsbStatus_SUCCESS) ?
         "SUCCESS" : "HALT") << " status." << endl;
    carbonXUsbTransDump(trans);
    carbonXUsbTransDestroy(trans);

    cout << "**** Test Sequence Complete ****" << endl;

    // Stop the simulation
    sc_stop();
}
