// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __HostAppln_h__
#define __HostAppln_h__

#include <string>
#include <queue>
using namespace std;

#include "carbon/c_memmanager.h"
#include "xactors/systemc/CarbonXSCUsb.h"
#include "xactors/usb/carbonXUsbTrans.h"

// Host Application Layer
class HostAppln : public sc_module
{
public:
    SC_HAS_PROCESS(HostAppln);

    // Host transaction ports
    sc_port<CarbonXSCUsbStartTransactionIF>     startTransPort;
    sc_export<CarbonXSCUsbReportTransactionIF>  reportTransExport;
    sc_export<CarbonXSCUsbResponseIF>           responseExport;

    // Host command notification queue and event
    queue<CarbonXUsbCommand>                    rxCommandQueue;
    sc_event                                    rxCommandEvent;

    // Host report transaction queue and event
    queue<CarbonXUsbTrans*>                     rxTransQueue;
    sc_event                                    rxTransEvent;

    HostAppln(sc_module_name name, CarbonXSCUsb *hostRef):
        sc_module(name), host(hostRef),
        mReportTransExport(rxTransQueue, rxTransEvent,
                rxCommandQueue, rxCommandEvent),
        mResponseExport(rxTransQueue, rxTransEvent)
    {
        // Binding exports
        reportTransExport(mReportTransExport);
        responseExport(mResponseExport);

        SC_THREAD(test);
    }

    // Test sequence
    void test();

private:
    // Interface channel definition
    class ReportTransChannel: public CarbonXSCUsbReportTransactionIF
    {
    public:
        ReportTransChannel(queue<CarbonXUsbTrans*> &rptTransQueue,
                sc_event &rptTransEvent,
                queue<CarbonXUsbCommand> &rptCommandQueue,
                sc_event &rptCommandEvent):
            reportTransQueue(rptTransQueue),
            reportTransEvent(rptTransEvent),
            reportCommandQueue(rptCommandQueue),
            reportCommandEvent(rptCommandEvent)
        {}
        void reportTransaction(CarbonXUsbTrans *trans)
        {
            reportTransQueue.push(trans);
            reportTransEvent.notify(SC_ZERO_TIME);
        }
        void reportCommand(CarbonXUsbCommand command)
        {
            reportCommandQueue.push(command);
            reportCommandEvent.notify(SC_ZERO_TIME);
        }
    private:
        queue<CarbonXUsbTrans*> &reportTransQueue;
        sc_event &reportTransEvent;
        queue<CarbonXUsbCommand> &reportCommandQueue;
        sc_event &reportCommandEvent;
    };
    class ResponseChannel: public CarbonXSCUsbResponseIF
    {
    public:
        ResponseChannel(queue<CarbonXUsbTrans*> &rptTransQueue,
                sc_event &rptTransEvent):
            reportTransQueue(rptTransQueue),
            reportTransEvent(rptTransEvent)
        {}
        void setDefaultResponse(CarbonXUsbTrans *trans) {}
        void setResponse(CarbonXUsbTrans *trans)
        {
            CarbonXUsbPipe *pipe = carbonXUsbTransGetPipe(trans);
            if (carbonXUsbPipeGetType(pipe) == CarbonXUsbPipe_CONTROL &&
                    carbonXUsbPipeGetEpNumber(pipe) == 0)
            {
                // For standard control transfer on default control pipe
                // copy the transfer and push to transaction queue
                CarbonXUsbTrans *stdMesgTrans = carbonXUsbTransCreate(pipe);
                carbonXUsbTransSetRequestType(stdMesgTrans,
                        carbonXUsbTransGetRequestType(trans));
                carbonXUsbTransSetRequest(stdMesgTrans,
                        carbonXUsbTransGetRequest(trans));
                carbonXUsbTransSetRequestValue(stdMesgTrans,
                        carbonXUsbTransGetRequestValue(trans));
                carbonXUsbTransSetRequestIndex(stdMesgTrans,
                        carbonXUsbTransGetRequestIndex(trans));
                carbonXUsbTransSetRequestLength(stdMesgTrans,
                        carbonXUsbTransGetRequestLength(trans));
                CarbonUInt32 dataSize = carbonXUsbTransGetDataSize(trans);
                if (dataSize > 0)
                {
                    CarbonUInt8 *data = static_cast<CarbonUInt8*>(
                            carbonmem_malloc(dataSize * sizeof(CarbonUInt8)));
                    const CarbonUInt8 *transData =
                        carbonXUsbTransGetData(trans);
                    for (CarbonUInt8 i = 0; i < dataSize; i++)
                        data[i] = transData[i];
                    carbonXUsbTransSetData(stdMesgTrans, dataSize, data);
					carbonmem_free(data);
                }
                reportTransQueue.push(stdMesgTrans);
                reportTransEvent.notify(SC_ZERO_TIME);
            }
        }
    private:
        queue<CarbonXUsbTrans*> &reportTransQueue;
        sc_event &reportTransEvent;
    };

    // Host reference
    CarbonXSCUsb *host;

    // Interface channel instantiation
    ReportTransChannel mReportTransExport;
    ResponseChannel mResponseExport;
};

#endif
