// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __BulkCore_h__
#define __BulkCore_h__

#include <string>
using namespace std;

#include "xactors/systemc/CarbonXSCUsb.h"
#include "xactors/usb/carbonXUsbTrans.h"

// Bulk Core Layer
class BulkCore : public sc_module
{
public:
    SC_HAS_PROCESS(BulkCore);

    // Device transaction ports
    sc_port<CarbonXSCUsbStartTransactionIF>     startTransPort;
    sc_export<CarbonXSCUsbReportTransactionIF>  reportTransExport;
    sc_export<CarbonXSCUsbResponseIF>           responseExport;

    BulkCore(sc_module_name name) :
        sc_module(name),
        mReportTransExport(mDataStorage),
        mResponseExport(mDataStorage)
    {
        // Binding exports
        reportTransExport(mReportTransExport);
        responseExport(mResponseExport);

        SC_THREAD(test);

        // Initialize data storage
        for (CarbonUInt32 i = 0; i < STORAGE_SIZE; i++)
            mDataStorage[i] = 0xFF;
    }

    // Test sequence
    void test();

    // Device configuration
    void setDeviceConfiguration(CarbonXUsbConfig *deviceConfig);

private:
    // Interface channel definition
    class ReportTransChannel: public CarbonXSCUsbReportTransactionIF
    {
    public:
        ReportTransChannel(CarbonUInt8 *dataStorage): mDataStorage(dataStorage) {}
        void reportTransaction(CarbonXUsbTrans *trans) {}
        void reportCommand(CarbonXUsbCommand command) {}
    private:
        CarbonUInt8 *mDataStorage;
    };
    class ResponseChannel: public CarbonXSCUsbResponseIF
    {
    public:
        ResponseChannel(CarbonUInt8 *dataStorage): mDataStorage(dataStorage)  {}
        void setDefaultResponse(CarbonXUsbTrans *trans)
        {
            // Provide data for bulk input transfer from data storage
            if ((carbonXUsbPipeGetType(carbonXUsbTransGetPipe(trans))
                        == CarbonXUsbPipe_BULK) &&
                    (carbonXUsbPipeGetDirection(carbonXUsbTransGetPipe(trans))
                     == CarbonXUsbPipeDirection_INPUT))
            {
                carbonXUsbTransSetData(trans, STORAGE_SIZE, mDataStorage);
            }
        }
        void setResponse(CarbonXUsbTrans *trans)
        {
            // Provide data for bulk input transfer from data storage
            if ((carbonXUsbPipeGetType(carbonXUsbTransGetPipe(trans))
                        == CarbonXUsbPipe_BULK) &&
                    (carbonXUsbPipeGetDirection(carbonXUsbTransGetPipe(trans))
                     == CarbonXUsbPipeDirection_OUTPUT))
            {
                CarbonUInt32 dataSize = carbonXUsbTransGetDataSize(trans);
                const CarbonUInt8 *data = carbonXUsbTransGetData(trans);
                for (CarbonUInt32 i = 0; i < dataSize && i < STORAGE_SIZE; i++)
                    mDataStorage[i] = data[i];
            }
        }
    private:
        CarbonUInt8 *mDataStorage;
    };

    // Interface channel instantiation
    ReportTransChannel mReportTransExport;
    ResponseChannel mResponseExport;

    // Data storage
    enum { STORAGE_SIZE = 256 };
    CarbonUInt8 mDataStorage[STORAGE_SIZE];
};

#endif
