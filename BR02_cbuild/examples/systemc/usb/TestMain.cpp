// -*- C++ -*-
/******************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "TestBench.h"

int sc_main (int argc , char *argv[])
{
#if SYSTEMC_VERSION > 20050714 // For SystemC version 2.2
    sc_report_handler::set_actions("/IEEE_Std_1666/deprecated", SC_DO_NOTHING);
#endif

    // Create the testbench
    sc_module *tb = new TestBench("TESTBENCH");

    // Run test
    sc_start(1000, SC_NS);

    delete tb;

    return 0;
}
