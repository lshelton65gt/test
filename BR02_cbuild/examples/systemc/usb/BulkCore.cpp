// -*- C++ -*-
/******************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "carbon/c_memmanager.h"
#include "xactors/usb/carbonXUsbDescriptors.h"
#include "BulkCore.h"

// Test sequenece
void BulkCore::test()
{
    /* Device attachment for PHY device */
    wait(500, SC_PS);
    cout << name() << ": " << sc_time_stamp() << ": " <<
        "Device attached ..." << endl;
    startTransPort->startCommand(CarbonXUsbCommand_DeviceAttach);
}

// Device configuration
void BulkCore::setDeviceConfiguration(CarbonXUsbConfig *deviceConfig)
{
    CarbonUInt8 *bytes = NULL;

    /* Creating Device Descriptor */
    CarbonXUsbDeviceDescriptor *deviceDescriptor =
        static_cast<CarbonXUsbDeviceDescriptor*>(
                carbonmem_malloc(sizeof(CarbonXUsbDeviceDescriptor)));
    deviceDescriptor->bcdUSB = 0x200;
    deviceDescriptor->bDeviceClass = 0x0;
    deviceDescriptor->bDeviceSubClass = 0x0;
    deviceDescriptor->bDeviceProtocol = 0x0;
    deviceDescriptor->bMaxPacketSize0 = 32;
    deviceDescriptor->idVendor = 0x58F;
    deviceDescriptor->idProduct = 0x6387;
    deviceDescriptor->bcdDevice = 0x141;
    deviceDescriptor->iManufacturer = 0x1;
    deviceDescriptor->iProduct = 0x2; 
    deviceDescriptor->iSerialNumber = 0x3; 
    deviceDescriptor->bNumConfigurations = 1;
    /* Creating byte array from device descriptor */
    bytes = carbonXUsbCreateBytesFromDeviceDescriptor(deviceDescriptor);
    /* Check if the descriptor is OK */
    assert(carbonXUsbCheckDescriptor(bytes, CarbonXUsbSpeed_FS));
    /* Registering device descriptor to the configuration */
    carbonXUsbConfigSetDeviceDescriptor(deviceConfig, bytes);
    /* Destroying device descriptor structure and byte array */
    carbonXUsbDestroyDeviceDescriptor(deviceDescriptor);
    carbonmem_free(bytes);

    /* Creating configuration descriptor */
    CarbonXUsbConfigurationDescriptor *configDescriptor =
        static_cast<CarbonXUsbConfigurationDescriptor*>(
                carbonmem_malloc(sizeof(CarbonXUsbConfigurationDescriptor)));
    configDescriptor->wTotalLength = 32; /* Two Bulk OUT and IN endpoints */
    configDescriptor->bNumInterfaces = 1;
    configDescriptor->bConfigurationValue = 1;
    configDescriptor->iConfiguration = 0;
    configDescriptor->bmAttributes = 0x80; /* Bus Powered */
    configDescriptor->bMaxPower = 0x32;
    configDescriptor->intfDescriptor =
        static_cast<CarbonXUsbInterfaceDescriptor**>(
                carbonmem_malloc(sizeof(CarbonXUsbInterfaceDescriptor*)));

    /* Creating interface descriptor */
    configDescriptor->intfDescriptor[0] =
        static_cast<CarbonXUsbInterfaceDescriptor*>(
                carbonmem_malloc(sizeof(CarbonXUsbInterfaceDescriptor)));
    CarbonXUsbInterfaceDescriptor *interfaceDescriptor =
        configDescriptor->intfDescriptor[0];
    interfaceDescriptor->bInterfaceNumber = 0;
    interfaceDescriptor->bAlternateSetting = 0;
    interfaceDescriptor->bNumEndpoints = 2;
    interfaceDescriptor->bInterfaceClass = 0x8; /* Mass Storage */
    interfaceDescriptor->bInterfaceSubClass = 0x6;
    interfaceDescriptor->bInterfaceProtocol = 0x50; /* Bulk-Only Transport */
    interfaceDescriptor->iInterface = 0x0;
    interfaceDescriptor->epDescriptor =
        static_cast<CarbonXUsbEndpointDescriptor**>(
                carbonmem_malloc(2 * sizeof(CarbonXUsbEndpointDescriptor*)));

    /* Creating endpoint descriptors */
    interfaceDescriptor->epDescriptor[0] =
        static_cast<CarbonXUsbEndpointDescriptor*>(
                carbonmem_malloc(sizeof(CarbonXUsbEndpointDescriptor)));
    interfaceDescriptor->epDescriptor[1] =
        static_cast<CarbonXUsbEndpointDescriptor*>(
                carbonmem_malloc(sizeof(CarbonXUsbEndpointDescriptor)));

    /* Bulk OUT endpoint */
    CarbonXUsbEndpointDescriptor *bulkOutEp =
        interfaceDescriptor->epDescriptor[0];
    bulkOutEp->bEndpointAddress = 0x1; /* OUT */
    bulkOutEp->bmAttributes = 0x2; /* USB_ENDPOINT_TYPE_BULK */
    bulkOutEp->wMaxPacketSize = 16;
    bulkOutEp->bInterval = 0;

    /* Bulk IN endpoint */
    CarbonXUsbEndpointDescriptor *bulkInEp =
        interfaceDescriptor->epDescriptor[1];
    bulkInEp->bEndpointAddress = 0x81; /* IN */
    bulkInEp->bmAttributes = 0x2; /* USB_ENDPOINT_TYPE_BULK */
    bulkInEp->wMaxPacketSize = 16;
    bulkInEp->bInterval = 0;

    /* Creating bytes from configuration descriptor */
    bytes = carbonXUsbCreateBytesFromConfigurationDescriptor(configDescriptor);
    /* Check if the descriptor is OK */
    assert(carbonXUsbCheckDescriptor(bytes, CarbonXUsbSpeed_FS));
    /* Adding configuration descriptor to the configuration */
    carbonXUsbConfigAddConfigDescriptor(deviceConfig, bytes);
    /* Destroying configuration descriptor structure and byte array */
    carbonXUsbDestroyConfigurationDescriptor(configDescriptor);
    carbonmem_free(bytes);
}

