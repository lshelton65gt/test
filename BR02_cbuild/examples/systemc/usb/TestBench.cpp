// -*- C++ -*-
/******************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "TestBench.h"

TestBench::TestBench(sc_module_name name):
    sc_module(name), SystemClock("SystemClock", 10, SC_PS, 0.5)
{
    // Instantiating host side components
    // 1. Host Application Layer
    // 2. Host Controller Transactor with UTMI LINK interface
    CarbonXUsbConfig *hostConfig = carbonXUsbConfigCreate();
    carbonXUsbConfigSetIntfSide(hostConfig, CarbonXUsbIntfSide_LINK);
    host = new CarbonXSCUsbUtmi("HOST_CONTROLLER", CarbonXUsb_Host, hostConfig);
    hostApplnLayer = new HostAppln("HOST_APPLICATION", host);
    carbonXUsbConfigDestroy(hostConfig); hostConfig = NULL;

    // Instantiating device side components
    // 1. Bulk Core which will configure the device
    // 2. Device Transactor with UTMI PHY interface
    CarbonXUsbConfig *deviceConfig = carbonXUsbConfigCreate();
    carbonXUsbConfigSetIntfSide(deviceConfig, CarbonXUsbIntfSide_PHY);
    bulkCore = new BulkCore("BULK_CORE");
    bulkCore->setDeviceConfiguration(deviceConfig);
    device = new CarbonXSCUsbUtmi("DEVICE", CarbonXUsb_Device, deviceConfig);
    carbonXUsbConfigDestroy(deviceConfig); deviceConfig = NULL;

    // Instantiating Utmi glue
    glue = new TestUtmiGlue("TestUtmiGlue");

    // Connection between host application layer and host controller
    hostApplnLayer->startTransPort(host->startTransExport);
    host->reportTransPort(hostApplnLayer->reportTransExport);
    host->responsePort(hostApplnLayer->responseExport);

    // Connection between bulk core and device transactor
    bulkCore->startTransPort(device->startTransExport);
    device->reportTransPort(bulkCore->reportTransExport);
    device->responsePort(bulkCore->responseExport);

    // Connection between transactors and glue
    glue->SystemClock(SystemClock);
    host->SystemClock(SystemClock);
    device->SystemClock(SystemClock);

    glue->LINK_Reset(host->Reset);
    glue->LINK_DataBus16_8(host->DataBus16_8);
    glue->LINK_SuspendM(host->SuspendM);
    glue->LINK_DataInLo(host->DataInLo);
    glue->LINK_DataInHi(host->DataInHi);
    glue->LINK_TXValid(host->TXValid);
    glue->LINK_TXValidH(host->TXValidH);
    glue->LINK_TXReady(host->TXReady);
    glue->LINK_DataOutLo(host->DataOutLo);
    glue->LINK_DataOutHi(host->DataOutHi);
    glue->LINK_RXValid(host->RXValid);
    glue->LINK_RXValidH(host->RXValidH);
    glue->LINK_RXActive(host->RXActive);
    glue->LINK_RXError(host->RXError);
    glue->LINK_XcvrSelect(host->XcvrSelect);
    glue->LINK_TermSelect(host->TermSelect);
    glue->LINK_OpMode(host->OpMode);
    glue->LINK_LineState(host->LineState);

    glue->PHY_Reset(device->Reset);
    glue->PHY_DataBus16_8(device->DataBus16_8);
    glue->PHY_SuspendM(device->SuspendM);
    glue->PHY_DataInLo(device->DataInLo);
    glue->PHY_DataInHi(device->DataInHi);
    glue->PHY_TXValid(device->TXValid);
    glue->PHY_TXValidH(device->TXValidH);
    glue->PHY_TXReady(device->TXReady);
    glue->PHY_DataOutLo(device->DataOutLo);
    glue->PHY_DataOutHi(device->DataOutHi);
    glue->PHY_RXValid(device->RXValid);
    glue->PHY_RXValidH(device->RXValidH);
    glue->PHY_RXActive(device->RXActive);
    glue->PHY_RXError(device->RXError);
    glue->PHY_XcvrSelect(device->XcvrSelect);
    glue->PHY_TermSelect(device->TermSelect);
    glue->PHY_OpMode(device->OpMode);
    glue->PHY_LineState(device->LineState);
}

TestBench::~TestBench()
{
    delete host;
    delete hostApplnLayer;
    delete bulkCore;
    delete device;
    delete glue;
}

