// -*- C++ -*-
/******************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __TestBench_h__
#define __TestBench_h__

#include "HostAppln.h"
#include "BulkCore.h"
#include "xactors/systemc/CarbonXSCUsb.h"
#ifndef _NO_CARBON_MODEL
#include "libTestUtmiGlue.systemc.h"
#else
#include "TestUtmiGlue.h"
#endif

// Top level testbench comprising of a host and device transactors. These
// transactors are connected via UTMI level 0 glue logic. The testbench also
// consists of a host application layer which sits on top of host transactor,
// and a bulk core on top of device transactor.
class TestBench : public sc_module
{
public:
    SC_HAS_PROCESS(TestBench);

    sc_clock            SystemClock;
    HostAppln          *hostApplnLayer;
    CarbonXSCUsbUtmi   *host;
    BulkCore           *bulkCore;
    CarbonXSCUsbUtmi   *device;
    TestUtmiGlue       *glue;

    TestBench(sc_module_name name);
    ~TestBench();
};

#endif
