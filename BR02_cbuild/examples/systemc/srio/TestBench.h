// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __TestBench_h__
#define __TestBench_h__

#include "TestLogicalIf.h"
#include "xactors/systemc/CarbonXSCSrio.h"
#include "SrioMemoryPe.h"
#include "libTestGlue.systemc.h"

// Top level testbench comprising of :
// 1. Logical Layer Test interface for initiating and receiving transactions
// 2. Memory Only Processing Element containing Local Memory Space, 
//    Configuration Register Space(CAR/CSR), SRIO transactor and a Queue for
//    storing response transaction
// 3. Glue model for connecting two Memory Only Processing Element
// 4. Event for maintaining communication between Memory Only Processing Element
//    and Logical Layer Test interface
// 5. Signals for making the connections between Memory Only Processing Element
//    and glue model.
class TestBench : public sc_module
{
public:
    
    SC_HAS_PROCESS(TestBench);

    // System clock
    sc_clock         SystemClock;
    
    // Logical Layer Test interface
    TestLogicalIf   *test;
    
    // Memory Only Processing Element
    SrioMemoryPe    *PE1;
    SrioMemoryPe    *PE2;

    // Glue model for connecting two PE   
    TestGlue        *glue;

    // Logical Layer Test interface should wait for this event. 
    // This event is notified by PE to denote completion of a transaction.
    sc_event  transEvent;

    // Constructor
    TestBench(sc_module_name name);

    // Destructor
    ~TestBench();

    // Signals for connecting PE1 with glue model
    sc_signal<sc_uint<10> > PE1_TLane0;
    sc_signal<sc_uint<10> > PE1_TLane1;
    sc_signal<sc_uint<10> > PE1_TLane2;
    sc_signal<sc_uint<10> > PE1_TLane3;
    sc_signal<sc_uint<10> > PE1_RLane0;
    sc_signal<sc_uint<10> > PE1_RLane1;
    sc_signal<sc_uint<10> > PE1_RLane2;
    sc_signal<sc_uint<10> > PE1_RLane3;
    
    // Signals for connecting PE2 with glue model
    sc_signal<sc_uint<10> > PE2_TLane0;
    sc_signal<sc_uint<10> > PE2_TLane1;
    sc_signal<sc_uint<10> > PE2_TLane2;
    sc_signal<sc_uint<10> > PE2_TLane3;
    sc_signal<sc_uint<10> > PE2_RLane0;
    sc_signal<sc_uint<10> > PE2_RLane1;
    sc_signal<sc_uint<10> > PE2_RLane2;
    sc_signal<sc_uint<10> > PE2_RLane3;
};

#endif
