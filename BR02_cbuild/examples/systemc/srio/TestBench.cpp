// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "TestBench.h"

// Creation and conection of the Memory Only Processing Elements, Logical Layer
// Test Interface and Glue Model
TestBench :: TestBench(sc_module_name name) :
    sc_module(name), SystemClock("SystemClock", 100, SC_NS, 0.5)
{
    // Set the maximum no of transaction that can be buffered to 5
    const int MAX_TRANS = 5;
    
    // Create configuration for 34 bit addressing support
    CarbonXSrioConfig *config = carbonXSrioConfigCreate();
    carbonXSrioConfigSetAddressing(config, CarbonXSrioAddressing_34);
    
    // Instantiating SrioMemoryPe PE1
    PE1 = new SrioMemoryPe("SrioPe1", config, MAX_TRANS, transEvent);
    
    // Connections between PE1's ports and TestBench signals for enabling
    // future connection of PE1 with glue model
    PE1->SystemClock(SystemClock);
    PE1->RLane0(PE1_RLane0);
    PE1->RLane1(PE1_RLane1);
    PE1->RLane2(PE1_RLane2);
    PE1->RLane3(PE1_RLane3);
    PE1->TLane0(PE1_TLane0);
    PE1->TLane1(PE1_TLane1);
    PE1->TLane2(PE1_TLane2);
    PE1->TLane3(PE1_TLane3);
   
    // Instantiating SrioMemoryPe PE2
    PE2 = new SrioMemoryPe("SrioPe2", config, MAX_TRANS, transEvent);
    
    // Connections between PE2's ports and TestBench signals for enabling
    // future connection of PE2 with glue model
    PE2->SystemClock(SystemClock);
    PE2->RLane0(PE2_RLane0);
    PE2->RLane1(PE2_RLane1);
    PE2->RLane2(PE2_RLane2);
    PE2->RLane3(PE2_RLane3);
    PE2->TLane0(PE2_TLane0);
    PE2->TLane1(PE2_TLane1);
    PE2->TLane2(PE2_TLane2);
    PE2->TLane3(PE2_TLane3);

    
    // Instantiating Logical layer Test Interface. This test layer will 
    // initiate transaction on PE1 via the port PE1->startTransPort and wait for
    // the transEvent to be notified by PE1 which denotes transaction completion
    // and retrieves the response transaction from the queue PE1->responseQueue
    test = new TestLogicalIf("TestLogical", transEvent, 
            PE1->startTransPort, PE1->responseQueue);
    
    // Instantiating glue model for connecting two PE
    glue = new TestGlue("TestGlue");

    // connection between PE1 and glue through TestBench signals
    glue->SystemClock(SystemClock);
    glue->PE1_TLane0(PE1_TLane0);
    glue->PE1_TLane1(PE1_TLane1);
    glue->PE1_TLane2(PE1_TLane2);
    glue->PE1_TLane3(PE1_TLane3);
    glue->PE1_RLane0(PE1_RLane0);
    glue->PE1_RLane1(PE1_RLane1);
    glue->PE1_RLane2(PE1_RLane2);
    glue->PE1_RLane3(PE1_RLane3);

    // connection between PE2 and glue through TestBench signals
    glue->PE2_TLane0(PE2_TLane0);
    glue->PE2_TLane1(PE2_TLane1);
    glue->PE2_TLane2(PE2_TLane2);
    glue->PE2_TLane3(PE2_TLane3);
    glue->PE2_RLane0(PE2_RLane0);
    glue->PE2_RLane1(PE2_RLane1);
    glue->PE2_RLane2(PE2_RLane2);
    glue->PE2_RLane3(PE2_RLane3);
}

TestBench :: ~TestBench()
{
    delete PE1;
    delete PE2;
    delete glue;
    delete test;
}

