// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __TestLogicalIf_h__
#define __TestLogicalIf_h__

#include <string>
#include <queue>
#include <iostream>
using namespace std;

#include "xactors/systemc/CarbonXSCSrioIfs.h"

// Logical layer Test Interface:
// This test layer initiate transactions on PE1 via the port _startTransPort
// and wait for the _transEvent to be notified by PE1 that denotes completion
// of initiated transaction and then it recover the response transaction from 
// _responseQueue
class TestLogicalIf : public sc_module
{
public:
    
    SC_HAS_PROCESS(TestLogicalIf);
    
    // Constructor
    TestLogicalIf(sc_module_name name, 
            // Reference of TestBench transEvent
            sc_event &transEvent,
            // Reference of PE1's startTransPort
            sc_port<CarbonXSCSrioStartTransactionIF> &startTransPort,
            // Reference of PE1's responseQueue 
            queue<CarbonXSrioResponse*> &responseQueue):
        sc_module(name),
        _transEvent(transEvent), _startTransPort(startTransPort), 
        _responseQueue(responseQueue)
    {
        // test_func initiate transaction and wait for _transEvent before
        // starting a new transaction
        SC_THREAD(test_func);
        sensitive << _transEvent;
    }

    // Initiate transactions on PE1
    void test_func();
    // Display response transaction
    void dumpRxTrans();
    // Display I/O request transaction
    void dumpTxTrans(const CarbonXSrioIoRequest *ioReq);
    // Display Message request transaction
    void dumpTxTrans(const CarbonXSrioMessage *msgReq);
    // Display Doorbell request transaction
    void dumpTxTrans(const CarbonXSrioDoorbell *dbReq);
    
private:
    
    // PE1 denotes transaction completion by notifying this event
    sc_event &_transEvent;
    // PE1 start transaction ports
    sc_port<CarbonXSCSrioStartTransactionIF> &_startTransPort;
    // PE1 response transaction queue
    queue<CarbonXSrioResponse*> &_responseQueue;

};

#endif
