/**** Glue Logic connecting PE1 and PE2 Transactors ****/
module TestGlue(SystemClock,
        /* PE1 Connections */
        PE1_TLane0,
        PE1_TLane1,
        PE1_TLane2,
        PE1_TLane3,
        PE1_RLane0,
        PE1_RLane1,
        PE1_RLane2,
        PE1_RLane3,
        /* PE2 Connections */
        PE2_TLane0,
        PE2_TLane1,
        PE2_TLane2,
        PE2_TLane3,
        PE2_RLane0,
        PE2_RLane1,
        PE2_RLane2,
        PE2_RLane3);
       
input SystemClock;

input [9:0] PE1_TLane0;
input [9:0] PE1_TLane1;
input [9:0] PE1_TLane2;
input [9:0] PE1_TLane3;
output [9:0] PE1_RLane0;
output [9:0] PE1_RLane1;
output [9:0] PE1_RLane2;
output [9:0] PE1_RLane3;


input [9:0] PE2_TLane0;
input [9:0] PE2_TLane1;
input [9:0] PE2_TLane2;
input [9:0] PE2_TLane3;
output [9:0] PE2_RLane0;
output [9:0] PE2_RLane1;
output [9:0] PE2_RLane2;
output [9:0] PE2_RLane3;

assign PE2_RLane0 = PE1_TLane0;
assign PE2_RLane1 = PE1_TLane1;
assign PE2_RLane2 = PE1_TLane2;
assign PE2_RLane3 = PE1_TLane3;
assign PE1_RLane0 = PE2_TLane0;
assign PE1_RLane1 = PE2_TLane1;
assign PE1_RLane2 = PE2_TLane2;
assign PE1_RLane3 = PE2_TLane3;

endmodule

