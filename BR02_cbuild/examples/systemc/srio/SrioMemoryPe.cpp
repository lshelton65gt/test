// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "SrioMemoryPe.h"

SrioMemoryPe::SrioMemoryPe(sc_module_name name, 
        CarbonXSrioConfig *config, CarbonUInt32 maxTransfers, 
        sc_event &transEvent): sc_module(name),
                               // Instantiating the report transaction channel
                               _reportTransChannel((const char*)name), 
                               // Instantiating the response transaction channel
                               _responseChannel((const char*)name, transEvent, 
                                       this, startTransPort, responseQueue)
{
    // Creating SRIO transactor
    xtor = new CarbonXSCSrio((const char*)name, config, maxTransfers);
    
    // Binding SystemClock
    xtor->SystemClock(SystemClock);

    // Binding transactor's RLane signals with input ports
    // getRLanes function is called whenever the value of any one input port
    // (RLane0 or RLane1 or RLane2 or RLane3) changes    
    SC_METHOD(getRLanes);
    sensitive << RLane0 << RLane1 << RLane2 << RLane3;

    // Binding transactor's TLane signals with output ports
    // setTLanes function is called whenever the value of any one transactor
    // signal (xtor->TLane0/xtor->TLane1/xtor->TLane2/xtor->TLane3) changes    
    SC_METHOD(setTLanes);
    sensitive << xtor->TLane0 << xtor->TLane1 << xtor->TLane2 << xtor->TLane3;

    // Binding transactor's Export 
    startTransPort(xtor->startTransExport);
    
    // Binding transactor's reportTransPort with report transaction channel
    xtor->reportTransPort(_reportTransChannel);
    // Binding transactor's responsePort with response transaction channel
    xtor->responsePort(_responseChannel);
    
    // Geting the current configuration
    CarbonXSrioConfig *currentConfig = carbonXSrioConfigCreate();
    xtor->getConfig(currentConfig);
    
    
    // Initializing the Configuration registers Cars and Csrs
    
    configRegSpace[ SrioCar_DeviceIdentityCar ] = 0;
    configRegSpace[ SrioCar_DeviceInformationCar ] = 0;
    configRegSpace[ SrioCar_AssemblyIdentityCar ] = 0;
    // SrioCar_AssemblyInformationCar: bit16-31:ExtendedFeaturesPtr = 0x0100
    configRegSpace[ SrioCar_AssemblyInformationCar ] = 0x00000100;
    const CarbonXSrioAddressingType addressing = 
        carbonXSrioConfigGetAddressing(currentConfig);
    // SrioCar_ProcessingElementFeaturesCar: 
    //              bit1:Memory = 1, bit28:ExtendedFeatures=1
    //              bit29-31:ExtendedAddressingSupport
    // SrioCsr_ProcessingElementLogicalLayerControlCsr:
    //              bit29-31:ExtendedAddressingControl
    if (addressing == CarbonXSrioAddressing_66)
    {
        // bit29-31 = 0b111
        configRegSpace[ SrioCar_ProcessingElementFeaturesCar ] = 0x4000000F;
        configRegSpace[ SrioCsr_ProcessingElementLogicalLayerControlCsr ] 
            = 0x00000004;
        configRegSpace[ SrioCsr_LocalConfigurationSpaceBaseAddress0Csr ] 
            = 0xFFFFFFFF;
        configRegSpace[ SrioCsr_LocalConfigurationSpaceBaseAddress1Csr ] 
            = 0xFFFFFFE0;
    }
    else if (addressing == CarbonXSrioAddressing_50)
    {
        // bit29-31 = 0b011
        configRegSpace[ SrioCar_ProcessingElementFeaturesCar ] = 0x4000000B;
        configRegSpace[ SrioCsr_ProcessingElementLogicalLayerControlCsr ] 
            = 0x00000002;
        configRegSpace[ SrioCsr_LocalConfigurationSpaceBaseAddress0Csr ] 
            = 0x0000FFFF;
        configRegSpace[ SrioCsr_LocalConfigurationSpaceBaseAddress1Csr ] 
            = 0xFFFFFFE0;
    }
    else
    {
        // bit29-31 = 0b001
        configRegSpace[ SrioCar_ProcessingElementFeaturesCar ] = 0x40000009;
        configRegSpace[ SrioCsr_ProcessingElementLogicalLayerControlCsr ] 
            = 0x00000001;
        // SrioCsr_LocalConfigurationSpaceBaseAddress0Csr: reserved 
        // for 34bit addressing
        configRegSpace[ SrioCsr_LocalConfigurationSpaceBaseAddress0Csr ] = 0;
        // SrioCsr_LocalConfigurationSpaceBaseAddress1Csr: bit0:reserved
        // bit1-31: bits0-30 of 34-bit local physical address
        // local physical add start from 0x0FFFFFF00 ranges upto0x0FFFFFFF8
        // Hence 34 bit Offset should be (0x0FFFFFF00>>3)=0x1FFFFFE0
        configRegSpace[ SrioCsr_LocalConfigurationSpaceBaseAddress1Csr ] 
            = 0x1FFFFFE0;
    }
    configRegSpace[ SrioCar_SwitchPortInformationCar ] = 0;
    // SrioCar_SourceOperationCar = 0x0000FFFC for supporting all I/O operations
    // Data Message and Doorbell transactions
    configRegSpace[ SrioCar_SourceOperationCar ] = 0x0000FFFC;
    // SrioCar_DestinationOperationCar = 0x0000FFFC for supporting all 
    // I/O operations, Data Message and Doorbell transactions
    configRegSpace[ SrioCar_DestinationOperationCar ] = 0x0000FFFC;

    // Initializing Memory space
    for (unsigned int i = 0; i < 500; i++)
    {
        localMemorySpace[i] = i;
    }

}

// Setting transactor's Receive Lane Signals values with the values from 
// RLane Input port
void SrioMemoryPe::getRLanes()
{
    xtor->RLane0.write(RLane0.read()); 
    xtor->RLane1.write(RLane1.read()); 
    xtor->RLane2.write(RLane2.read()); 
    xtor->RLane3.write(RLane3.read()); 
}

// Setting the Output port TLane's value with the values of transactor's
// TLane signals
void SrioMemoryPe::setTLanes()
{
    TLane0.write(xtor->TLane0.read());
    TLane1.write(xtor->TLane1.read());
    TLane2.write(xtor->TLane2.read());
    TLane3.write(xtor->TLane3.read());
}

int SrioMemoryPe::readMemory(CarbonUInt32 address,
        CarbonUInt32 extAddress, CarbonUInt2 xamsbs, CarbonUInt6 dataSize,
        CarbonXSrioDword* data)
{
    // Checking address range
    if (address < 0xFFFFFF00 || address > 0xFFFFFFF8 || xamsbs != 0)
        return 0;
    
    // local mamory address calculation from 32bit address and 
    // SrioCsr_LocalConfigurationSpaceBaseAddress1Csr
    // SrioCsr_LocalConfigurationSpaceBaseAddress1Csr bit1-31 contains Bits 0-30
    // of a 34 bit local physical address offset and xamsbs value is 0
    unsigned int memoryAddress = address - 
        ( getConfigReg(SrioCsr_LocalConfigurationSpaceBaseAddress1Csr) << 3);
    
    for (int i = 0; i < dataSize; i++)
    {
        data[i].lsWord = localMemorySpace[memoryAddress];
        memoryAddress++;
        data[i].msWord = localMemorySpace[memoryAddress];
        memoryAddress++;
    }
    return 1;
}

int SrioMemoryPe::writeMemory(CarbonUInt32 address, CarbonUInt32 extAddress,
        CarbonUInt2 xamsbs, CarbonXSrioDword* data, CarbonUInt6 dataSize)
{
    // Checking address range
    if (address < 0xFFFFFF00 || address > 0xFFFFFFF8 || xamsbs != 0)
        return 0;
    
    // local memory address calculation from 32bit address and 
    // SrioCsr_LocalConfigurationSpaceBaseAddress1Csr
    // SrioCsr_LocalConfigurationSpaceBaseAddress1Csr bit1-31 contains Bits 0-30
    // of a 34 bit local physical address offset and xamsbs value is 0
    unsigned int memoryAddress = address - 
        ( getConfigReg(SrioCsr_LocalConfigurationSpaceBaseAddress1Csr) << 3);
    
    for (int i = 0; i < dataSize; i++)
    {
        localMemorySpace[memoryAddress] = data[i].lsWord;
        memoryAddress++;
        localMemorySpace[memoryAddress] = data[i].msWord;
        memoryAddress++;
    }
    return 1;
}

// Create Response transaction for I/O request transaction
CarbonXSrioTrans* SrioMemoryPe::ResponseChannel::genResponse(
        const CarbonXSrioIoRequest *ioReq, CarbonUInt6 dataSize)
{
    CarbonXSrioDword data[32];
    CarbonXSrioResponse resp;
    resp.status = CarbonXResponseStatus_DONE;
    resp.transaction = 0x0;
    resp.dataSize = 0;
    resp.data = NULL;
    resp.transId = ioReq->transId;
    resp.srcId = ioReq->destId;
    resp.destId = ioReq->srcId;
    resp.prio = ioReq->prio;
    if (dataSize > 0)
    {
        // Reading values from the requested address of memory
        if (!_pe->readMemory(ioReq->address, ioReq->extAddress, ioReq->xamsbs,
                    dataSize, data))
        {
            cout << _name <<
                "address must be in between 0xFFFFFF00 and 0xFFFFFFF8 and " <<
                "xamsbs must be 0." << endl;
        }
        resp.dataSize = dataSize;
        resp.data = data;
        resp.transaction = 0x8;
    }
    CarbonXSrioTrans *trans =
        carbonXSrioCreateResponseTrans(&resp);
    return (trans);
}

// Create Response transaction for Message request transaction
CarbonXSrioTrans* SrioMemoryPe::ResponseChannel::genResponse(
        const CarbonXSrioMessage *msgReq)
{
    unsigned int respMessageTransId = (msgReq->msglen == 0) ?
        ((msgReq->letter << 6) | (msgReq->mbox << 4) | (msgReq->xmbox)) :
        ((msgReq->letter << 6) | (msgReq->mbox << 4) | (msgReq->msgseg));
    CarbonXSrioResponse resp;
    resp.status = CarbonXResponseStatus_DONE;
    resp.transaction = 0x1;
    resp.dataSize = 0;
    resp.data = NULL;
    resp.transId = respMessageTransId;
    resp.srcId = msgReq->destId;
    resp.destId = msgReq->srcId;
    resp.prio = msgReq->prio;
    CarbonXSrioTrans *trans =
        carbonXSrioCreateResponseTrans(&resp);
    return (trans);
}

// Create Response transaction for Doorbell request transaction
CarbonXSrioTrans* SrioMemoryPe::ResponseChannel::genResponse(
        const CarbonXSrioDoorbell *dbReq)
{
    CarbonXSrioResponse resp;
    resp.status = CarbonXResponseStatus_DONE;
    resp.transaction = 0x0;
    resp.dataSize = 0;
    resp.data = NULL;
    resp.transId = dbReq->transId;
    resp.srcId = dbReq->destId;
    resp.destId = dbReq->srcId;
    resp.prio = dbReq->prio;
    CarbonXSrioTrans *trans =
        carbonXSrioCreateResponseTrans(&resp);
    return (trans);
}

