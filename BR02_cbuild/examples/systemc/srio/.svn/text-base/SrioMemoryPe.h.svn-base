// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef _SrioMemoryPe_h_
#define _SrioMemoryPe_h_

#include <iostream>
#include <string>
#include <queue>
using namespace std;

#include "xactors/systemc/CarbonXSCSrio.h"

// The CAR's of SRIO Memory Only Processing Element 
typedef enum {
    SrioCar_DeviceIdentityCar = 0,
    SrioCar_DeviceInformationCar,
    SrioCar_AssemblyIdentityCar,
    SrioCar_AssemblyInformationCar,
    SrioCar_ProcessingElementFeaturesCar,
    SrioCar_SwitchPortInformationCar,
    SrioCar_SourceOperationCar,
    SrioCar_DestinationOperationCar
} SrioCar;

// The CSR's of SRIO Memory Only Processing Element 
typedef enum {
    SrioCsr_ProcessingElementLogicalLayerControlCsr = 8,
    SrioCsr_LocalConfigurationSpaceBaseAddress0Csr,
    SrioCsr_LocalConfigurationSpaceBaseAddress1Csr
} SrioCsr;

// SRIO Memory Only Processing Element comprises of SRIO transactor,
// configuration register space for SrioCar and SrioCsr, read/write 
// Local Memory Space, Queue for holding the response transaction. 
//
// When incomming request arrives from the link, it reads or writes the
// local memory accordingly. If test layer wants to push a tranaction,
// he can do that on the StartTrans port and expect a response from its
// response queue.
//
// It also defines the Response Channel and Report Transaction Channel for 
// connecting with the transactor's corresponding ports. 
class SrioMemoryPe : public sc_module
{
    public:

        SC_HAS_PROCESS(SrioMemoryPe);

        // Input SystemClock for transactor
        sc_in<bool> SystemClock;
        
        // Input ports for connecting transactor's RLanes with glue model
        sc_in<sc_uint<10> > RLane0;
        sc_in<sc_uint<10> > RLane1;
        sc_in<sc_uint<10> > RLane2;
        sc_in<sc_uint<10> > RLane3;
        // Output ports for connecting transactor's TLanes with glue model
        sc_out<sc_uint<10> > TLane0;
        sc_out<sc_uint<10> > TLane1;
        sc_out<sc_uint<10> > TLane2;
        sc_out<sc_uint<10> > TLane3;

        // startTransPort connected with transactor's startTransExport
        sc_port<CarbonXSCSrioStartTransactionIF> startTransPort;

        // Function for updating transactor's RLane signals
        void getRLanes();
        // Function for updating transactor's TLane signals
        void setTLanes();
        
        // Response transaction queue
        queue<CarbonXSrioResponse*> responseQueue;

    public:
   
        // Constructor
        SrioMemoryPe(sc_module_name name, 
                // Configuration object from TestBench for instantiating
                // SRIO transactor
                CarbonXSrioConfig *config,
                // Number of maximum transaction that can be started at a time
                // required for instantiating SRIO transactor
                CarbonUInt32 maxTransfers, 
                // Reference of TestBench transEvent
                sc_event &transEvent);

        // Destructor
        ~SrioMemoryPe(){ delete xtor;}

        // Functions for getting Configuration Register's value
        CarbonUInt32 getConfigReg(SrioCar reg){return configRegSpace[reg];}
        CarbonUInt32 getConfigReg(SrioCsr reg){return configRegSpace[reg];}

        // Functions for reading values from a particular address of 
        // Local memory Space. The address should be within 0xFFFFFF00 to
        // 0xFFFFFFF8 and xamsbs should be 0
        int readMemory(CarbonUInt32 address, 
                CarbonUInt32 extAddress, CarbonUInt2 xamsbs,
                CarbonUInt6 dataSize, CarbonXSrioDword* data);

        // Functions for writing values to a particular address of 
        // Local memory Space. The address should be within 0xFFFFFF00 to
        // 0xFFFFFFF8 and xamsbs should be 0
        int writeMemory(CarbonUInt32 address, CarbonUInt32 extAddress,
                CarbonUInt2 xamsbs, CarbonXSrioDword* data, 
                CarbonUInt6 dataSize);

    private:

        // ReportTransChannel: This class provides the channel definition that
        // can be connected with the report transaction port of 
        // SRIO transactor(CarbonXSCSrio *xtor), as every port should be 
        // connected with a channel or export that provide the interface method
        // definition of the port. Whenever a initiated transaction is started
        // SRIO transactor(CarbonXSCSrio *xtor) callback reportTransaction 
        // function defined in this class.
        class ReportTransChannel : public CarbonXSCSrioReportTransactionIF
        {
        public:
            ReportTransChannel(string name):_name(name){}
            void reportTransaction(CarbonXSrioTrans* trans)
            {
                switch(carbonXSrioGetTransType(trans))
                {
                    case CarbonXSrioTrans_IO_REQUEST :
                    {
                        CarbonXSrioIoRequest *ioReq =
                            carbonXSrioGetIoRequest(trans);
                        carbonXSrioDestroyIoRequest(ioReq);
                        ioReq = NULL;
                        break;
                    }
                    case CarbonXSrioTrans_MESSAGE:
                    {
                        CarbonXSrioMessage *msgReq = 
                            carbonXSrioGetMessage(trans);
                        carbonXSrioDestroyMessage(msgReq);
                        msgReq = NULL;
                        break;
                    }
                    case CarbonXSrioTrans_DOORBELL:
                    {
                        CarbonXSrioDoorbell *dbReq =
                            carbonXSrioGetDoorbell(trans);
                        carbonXSrioDestroyDoorbell(dbReq);
                        dbReq = NULL;
                        break;
                    }
                    case CarbonXSrioTrans_RESPONSE :
                    {
                        CarbonXSrioResponse *resp = 
                            carbonXSrioGetResponse(trans);
                        cout << _name << ": Transmitted RESPONSE." << endl;
                        carbonXSrioDestroyResponse(resp);
                        resp = NULL;
                        break;
                    }
                    default :
                    {
                        cout << _name << ": ERROR: Invalid Transmission :"
                            << endl;
                    }
                }
                carbonXSrioTransDestroy(trans);
            }
            
        private:

            string _name;
            
        };

        // ResponseChannel: This class provides the channel definition that
        // can be connected with the response transaction port of 
        // SRIO transactor(CarbonXSCSrio *xtor), as every port should be 
        // connected with a channel or export that provide the interface method
        // definition of the port. Whenever a transaction is received
        // SRIO transactor(CarbonXSCSrio *xtor) callback setResponse 
        // function defined in this class. setResponse function check the 
        // received transaction and initiate a response transaction if required.
        // If the received transaction do not require any response then it
        // notify the _transEvent to signal Logical Layer Test Interface the
        // completion of initiated transaction otherwise it notify _transEvent
        // after receiving the response transaction. It also stores the response
        // transaction in _responseQueue to make it available to the Logical
        // Layer Test Interface.
        class ResponseChannel : public CarbonXSCSrioResponseIF
        {
        public:
            ResponseChannel(string name, sc_event &transEvent, 
                SrioMemoryPe* const pe,
                sc_port<CarbonXSCSrioStartTransactionIF> &startTransPort,
                queue<CarbonXSrioResponse*> &responseQueue):
                _name(name), _transEvent(transEvent), _pe(pe), 
                _startTransPort(startTransPort),
                _responseQueue(responseQueue){}

            void setDefaultResponse(CarbonXSrioTrans* trans) {}
            void setResponse(CarbonXSrioTrans* rxTrans)
            {
                switch(carbonXSrioGetTransType(rxTrans))
                {
                    case CarbonXSrioTrans_IO_REQUEST :
                    {
                        CarbonXSrioIoRequest *ioReq = 
                            carbonXSrioGetIoRequest(rxTrans);
                        switch(ioReq->reqType)
                        {
                            case CarbonXSrioIoRequest_NREAD :
                            {
                                cout << _name << 
                                    ": Received NREAD IO Request no." 
                                   << (int)(ioReq->transId) << endl;
                                CarbonUInt6 dataSize = 
                                    carbonXSrioGetIoRequestDataSize(ioReq);
                                // send a response transaction back
                                CarbonXSrioTrans *trans = 
                                    genResponse(ioReq, dataSize);
                                assert(trans);
                                _startTransPort->startNewTransaction(trans);
                                break;
                            }
                            case CarbonXSrioIoRequest_NWRITE :
                            {
                                cout << _name << 
                                    ": Received NWRITE IO Request no."
                                    << (int)ioReq->transId << endl;
                                CarbonUInt6 dataSize = 
                                    carbonXSrioGetIoRequestDataSize(ioReq);
                                // Write the data to the requested address of
                                // Local Memory
                                if (!_pe->writeMemory(ioReq->address, 
                                            ioReq->extAddress, ioReq->xamsbs, 
                                            ioReq->data, dataSize))
                                {
                                    cout << _name << 
                                        "Address must be in between 0xFFFFFF00"
                                           << " and 0xFFFFFFF8" << endl;
                                }
                                // As NWRITE do not require a response, hence
                                // transaction is complete, therefore, notify
                                // _transEvent
                                _transEvent.notify(SC_ZERO_TIME);
                                break;
                            }
                            case CarbonXSrioIoRequest_SWRITE :
                            {
                                cout << _name << 
                                    ": Received SWRITE IO Request." << endl;
                                // Write the data to the requested address of
                                // Local Memory
                                if (!_pe->writeMemory(ioReq->address, 
                                            ioReq->extAddress, ioReq->xamsbs, 
                                            ioReq->data, ioReq->dataSize))
                                {
                                    cout << _name << 
                                        "address must be in between 0xFFFFFF00"
                                        << " and 0xFFFFFFF8 and xamsbs = 0" 
                                        << endl;
                                }
                                
                                // As SWRITE do not require a response, hence
                                // transaction is complete, therefore, notify
                                // _transEvent
                                _transEvent.notify(SC_ZERO_TIME);
                                break;
                            }
                            case CarbonXSrioIoRequest_NWRITE_R :
                            {
                                cout << _name << 
                                    ": Received NWRITE_R IO Request no."
                                    << (int)ioReq->transId << endl;
                                CarbonUInt6 dataSize = 
                                    carbonXSrioGetIoRequestDataSize(ioReq);
                                // Write the data to the requested address of
                                // Local Memory
                                if (!_pe->writeMemory(ioReq->address, 
                                            ioReq->extAddress, ioReq->xamsbs, 
                                            ioReq->data, dataSize))
                                {
                                    cout << _name << 
                                        "Address must be in between 0xFFFFFF00" 
                                        << " and 0xFFFFFFF8" << endl;
                                }
                                // send a response transaction back
                                 CarbonXSrioTrans *trans = 
                                    genResponse(ioReq, 0);
                                assert(trans);
                                _startTransPort->startNewTransaction(trans);
                                break;
                            }
                            case CarbonXSrioIoRequest_ATOMIC_INC:
                            {
                                cout << _name << 
                                    ": Received ATOMIC_INC IO Request no."
                                    << (int)ioReq->transId << endl;
                                // send a response transaction back 
                                CarbonXSrioTrans *trans = 
                                    genResponse (ioReq, 0);
                                assert(trans);
                                _startTransPort->startNewTransaction(trans);
                                break;
                            }
                            case CarbonXSrioIoRequest_ATOMIC_TSWP :
                            {
                                cout << _name << 
                                    ": Received ATOMIC_TSWP IO Request no."
                                    << (int)ioReq->transId << endl;
                                CarbonUInt6 dataSize = 
                                    carbonXSrioGetIoRequestDataSize(ioReq);
                                // send a response transaction back 
                                CarbonXSrioTrans *trans = 
                                    genResponse(ioReq, dataSize);
                                assert(trans);
                                _startTransPort->startNewTransaction(trans);
                                break;
                            }
                            case CarbonXSrioIoRequest_ATOMIC_SET :
                            {
                                cout << _name << 
                                    ": Received ATOMIC_SET IO Request no."
                                    << (int)ioReq->transId << endl;
                                // send a response transaction back 
                                CarbonXSrioTrans *trans = 
                                    genResponse(ioReq, 0);
                                assert(trans);
                                _startTransPort->startNewTransaction(trans);
                                break;
                            }
                            case CarbonXSrioIoRequest_ATOMIC_CLR :
                            {
                                cout << _name << 
                                    ": Received ATOMIC_CLR IO Request no."
                                    << (int)ioReq->transId << endl;
                                // send a response transaction back 
                                CarbonXSrioTrans *trans = 
                                    genResponse(ioReq, 0);
                                assert(trans);
                                _startTransPort->startNewTransaction(trans);
                                break;
                            }
                            case CarbonXSrioIoRequest_ATOMIC_SWAP :
                            {
                                cout << _name << 
                                    ": Received ATOMIC_SWAP IO Request no."
                                    << (int)ioReq->transId << endl;
                                CarbonUInt6 dataSize = 
                                    carbonXSrioGetIoRequestDataSize(ioReq);
                                // send a response transaction back 
                                CarbonXSrioTrans *trans = 
                                    genResponse(ioReq, dataSize);
                                assert(trans);
                                _startTransPort->startNewTransaction(trans);
                                break;
                            }
                            case CarbonXSrioIoRequest_ATOMIC_DEC:
                            {
                                cout << _name << 
                                    ": Received ATOMIC_DEC IO Request no."
                                    << (int)ioReq->transId << endl;
                                // send a response transaction back 
                                CarbonXSrioTrans *trans = genResponse(ioReq, 0);
                                assert(trans);
                                _startTransPort->startNewTransaction(trans);
                                break;
                            }
                            default:
                            {
                                cout << _name << 
                                    ": Received UNKNOWN type of IO Request." 
                                    << endl;
                                break;
                            }
                        }
                        carbonXSrioDestroyIoRequest(ioReq);
                        ioReq = NULL;
                        break;
                    }
                    case CarbonXSrioTrans_MESSAGE:
                    {
                        CarbonXSrioMessage *msgReq = 
                            carbonXSrioGetMessage(rxTrans);
                        cout << _name << ": Received MESSAGE." << endl;
                        // send a response transaction back 
                        CarbonXSrioTrans *trans = genResponse(msgReq);
                        assert(trans);
                        _startTransPort->startNewTransaction(trans);
                        break;
                        
                    }
                    case CarbonXSrioTrans_DOORBELL:
                    {
                        CarbonXSrioDoorbell *dbReq = 
                            carbonXSrioGetDoorbell(rxTrans);
                        cout << _name << ": Received DOORBELL." << endl;
                        // send a response transaction back
                        CarbonXSrioTrans *trans = genResponse(dbReq);
                        assert(trans);
                        _startTransPort->startNewTransaction(trans);
                        break;
                    }
                    case CarbonXSrioTrans_RESPONSE :
                    {
                        CarbonXSrioResponse *resp = 
                            carbonXSrioGetResponse(rxTrans);
                        cout << _name << ": Received RESPONSE." << endl;
                        // Store the response transaction in queue
                        _responseQueue.push(resp);
                        // Notify the event to denote transaction completion
                        _transEvent.notify(SC_ZERO_TIME);
                        break;
                    }
                    default:
                    {
                        cout << _name << ": Received UNKNOWN Type of " << 
                            "Request." << endl;
                    }
                }
            }

        private:

            string _name;
            // For notifying transaction comletion
            sc_event &_transEvent;
            // Handle for SRIO Memory Only Processing Element
            SrioMemoryPe* const _pe;
            // Port of the transactor for initiating response transaction
            sc_port<CarbonXSCSrioStartTransactionIF> &_startTransPort;
            // Queue for storing the response transaction
            queue<CarbonXSrioResponse*> &_responseQueue;
            // Function for generating response transaction of a I/O request
            // transaction
            CarbonXSrioTrans* genResponse(const CarbonXSrioIoRequest *ioReq, 
                    CarbonUInt6 dataSize); 
            // Function for generating response transaction of a Message request
            // transaction
            CarbonXSrioTrans* genResponse(const CarbonXSrioMessage *msgReq); 
            // Function for generating response transaction of a Doorbell
            // request transaction
            CarbonXSrioTrans* genResponse(const CarbonXSrioDoorbell *dbReq); 
        };
               
                        
    private:
   
        // SRIO Transactor
        CarbonXSCSrio* xtor;
        // Configuration Register Space for SrioCar and SrioCsr
        CarbonUInt32 configRegSpace[11];
        // Local Memory Space
        CarbonUInt32 localMemorySpace[500];
        // Report Transaction Channel for connecting with SRIO Transactor's
        // corresponding reportTransPort
        ReportTransChannel _reportTransChannel;
        // Response Channel for connecting with SRIO Transactor's
        // corresponding response port
        ResponseChannel _responseChannel;
};
#endif
