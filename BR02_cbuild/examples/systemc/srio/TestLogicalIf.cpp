// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include <iomanip>
using namespace std;

#include "TestLogicalIf.h"

// This function initiate transaction on PE1's startTransPort and wait for 
// _transEvent to be notified by PE1. Then it displays the response transaction
// taking from PE1's response Queue and delete the same. 
void TestLogicalIf :: test_func()
{
    static int transCount = 0;
    CarbonXSrioTrans *trans;
    CarbonXSrioIoRequest ioReq;
    
    // starting NREAD transaction 
    ioReq.reqType = CarbonXSrioIoRequest_NREAD;
    ioReq.wdptr = 1;
    ioReq.rdSize = 12; // 8 DWORDs
    ioReq.wrSize = 0;
    ioReq.address = 0xFFFFFF48;// must be within 0xFFFFFF00 to 0xFFFFFFF8
    ioReq.extAddress = 0;
    ioReq.xamsbs = 0;
    ioReq.data = NULL;
    ioReq.transId = transCount++;
    ioReq.srcId = 1;
    ioReq.destId = 2;
    ioReq.dataSize = 0;
    ioReq.prio = 0;
    cout << "SrioPe1: Transmitted NREAD IO Request no " << 
        (int)ioReq.transId << endl;
    // dump request
    dumpTxTrans(&ioReq);
    // create transaction
    assert( trans = carbonXSrioCreateIoRequestTrans(&ioReq));
    // start transmission
    _startTransPort->startNewTransaction(trans);
    // Wait for completion of transaction
    wait(); 
    // dump response 
    dumpRxTrans();
    
    // starting NWRITE transaction 
    CarbonXSrioDword data[32];
    for (int i = 0; i < 32; i++)
    {
        data[i].lsWord = i;
        data[i].msWord = i;
    }
    ioReq.reqType = CarbonXSrioIoRequest_NWRITE;
    ioReq.wdptr = 0;
    ioReq.rdSize = 0;
    ioReq.wrSize = 12; // 4 DWORDs
    ioReq.address = 0xFFFFFF48;// must be within 0xFFFFFF00 to 0xFFFFFFF8
    ioReq.extAddress = 0;
    ioReq.xamsbs = 0;
    ioReq.dataSize = 4;
    ioReq.data = data;
    ioReq.transId = transCount++;
    ioReq.srcId = 1;
    ioReq.destId = 2;
    ioReq.prio = 0;
    cout << "SrioPe1: Transmitted NWRITE IO Request no " << 
        (int)ioReq.transId << endl;
    // dump request
    dumpTxTrans(&ioReq);
    // create transaction
    assert( trans = carbonXSrioCreateIoRequestTrans(&ioReq));
    // start transmission
    _startTransPort->startNewTransaction(trans);
    // Wait for completion of transaction
    wait();

    // starting NREAD transaction 
    ioReq.reqType = CarbonXSrioIoRequest_NREAD;
    ioReq.wdptr = 1;
    ioReq.rdSize = 12; // 8 DWORDs
    ioReq.wrSize = 0;
    ioReq.address = 0xFFFFFF48;// must be within 0xFFFFFF00 to 0xFFFFFFF8
    ioReq.extAddress = 0;
    ioReq.xamsbs = 0;
    ioReq.data = NULL;
    ioReq.transId = transCount++;
    ioReq.srcId = 1;
    ioReq.destId = 2;
    ioReq.dataSize = 0;
    ioReq.prio = 0;
    cout << "SrioPe1: Transmitted NREAD IO Request no " << 
        (int)ioReq.transId << endl;
    // dump request
    dumpTxTrans(&ioReq);
    // create transaction
    assert( trans = carbonXSrioCreateIoRequestTrans(&ioReq));
    // start transmission
    _startTransPort->startNewTransaction(trans);
    // Wait for completion of transaction
    wait(); // Wait for completion of transaction
    // dump response 
    dumpRxTrans();
    
    // starting NREAD transaction 
    ioReq.reqType = CarbonXSrioIoRequest_NREAD;
    ioReq.wdptr = 1;
    ioReq.rdSize = 15; // 32 DWORDs
    ioReq.wrSize = 0;
    ioReq.address = 0xFFFFFFF8;// must be within 0xFFFFFF00 to 0xFFFFFFF8
    ioReq.extAddress = 0;
    ioReq.xamsbs = 0;
    ioReq.data = NULL;
    ioReq.transId = transCount++;
    ioReq.srcId = 1;
    ioReq.destId = 2;
    ioReq.dataSize = 0;
    ioReq.prio = 0;
    cout << "SrioPe1: Transmitted NREAD IO Request no " << 
        (int)ioReq.transId << endl;
    // dump request
    dumpTxTrans(&ioReq);
    assert( trans = carbonXSrioCreateIoRequestTrans(&ioReq));
    _startTransPort->startNewTransaction(trans);
    // Wait for completion of transaction
    wait(); 
    // dump response 
    dumpRxTrans();
    
    // starting SWRITE transaction 
    ioReq.reqType = CarbonXSrioIoRequest_SWRITE;
    ioReq.wdptr = 1;
    ioReq.rdSize = 0;
    ioReq.wrSize = 13; // 16 DWORDs
    ioReq.dataSize = 16;
    ioReq.address = 0xFFFFFFF8;// must be within 0xFFFFFF00 to 0xFFFFFFF8
    ioReq.extAddress = 0;
    ioReq.xamsbs = 0;
    ioReq.data = data;
    ioReq.transId = transCount++;
    ioReq.srcId = 1;
    ioReq.destId = 2;
    ioReq.prio = 0;
    cout << "SrioPe1: Transmitted SWRITE IO Request no " << 
        (int)ioReq.transId << endl;
    // dump request
    dumpTxTrans(&ioReq);
    assert( trans = carbonXSrioCreateIoRequestTrans(&ioReq));
    _startTransPort->startNewTransaction(trans);
    // Wait for completion of transaction
    wait(); 
    
    // starting NREAD transaction 
    ioReq.reqType = CarbonXSrioIoRequest_NREAD;
    ioReq.wdptr = 1;
    ioReq.rdSize = 15; // 32 DWORDs
    ioReq.wrSize = 0;
    ioReq.address = 0xFFFFFFF8;// must be within 0xFFFFFF00 to 0xFFFFFFF8
    ioReq.extAddress = 0;
    ioReq.xamsbs = 0;
    ioReq.data = NULL;
    ioReq.transId = transCount++;
    ioReq.srcId = 1;
    ioReq.destId = 2;
    ioReq.dataSize = 0;
    ioReq.prio = 0;
    cout << "SrioPe1: Transmitted NREAD IO Request no " << 
        (int)ioReq.transId << endl;
    // dump request
    dumpTxTrans(&ioReq);
    assert( trans = carbonXSrioCreateIoRequestTrans(&ioReq));
    _startTransPort->startNewTransaction(trans);
    // Wait for completion of transaction
    wait(); 
    // dump response 
    dumpRxTrans();

    // starting MESSAGE transaction
    CarbonXSrioMessage msgReq;
    msgReq.mbox = 2;
    msgReq.xmbox = 7;
    msgReq.letter = 3;
    msgReq.msglen = 0;
    msgReq.msgseg = 0;
    msgReq.ssize = 10;
    for (int i = 0; i < (int)(0x1 << (msgReq.ssize - 9)); i++)
    {
        data[i].msWord = i;
        data[i].lsWord = i;
    }
    msgReq.data = data;
    msgReq.srcId = 1;
    msgReq.destId = 2;
    msgReq.prio = 0;
    cout <<"SrioPe1: Transmitted MESSAGE." << endl;
    // dump request
    dumpTxTrans(&msgReq);
    assert(trans = carbonXSrioCreateMessageTrans(&msgReq));
    _startTransPort->startNewTransaction(trans);
    // Wait for completion of transaction
    wait(); 
    // dump response 
    dumpRxTrans();

    // starting DOORBELL transaction
    CarbonXSrioDoorbell dbReq;
    dbReq.info = 10;
    dbReq.transId = transCount++;
    dbReq.srcId = 1;
    dbReq.destId = 2;
    dbReq.prio = 0;
    cout << "SrioPe1: Transmitted DOORBELL." << endl;
    // dump request
    dumpTxTrans(&dbReq);
    assert(trans = carbonXSrioCreateDoorbellTrans(&dbReq));
    _startTransPort->startNewTransaction(trans);
    // Wait for completion of transaction
    wait(); 
    // dump response 
    dumpRxTrans();

    sc_stop();
}

// Display the I/O request transaction
void TestLogicalIf::dumpTxTrans(const CarbonXSrioIoRequest *ioReq)
{
    cout << "IO REQUEST:" << endl;
    cout << "    address = " << (int)ioReq->xamsbs << "." <<
        (unsigned int)ioReq->extAddress << "." ;
    cout << setbase(16) << ioReq->address << endl;
    cout << setbase(10);
    cout << "    wdptr = " << (int)ioReq->wdptr << ", rdSize / wrSize = " <<
        (int)((ioReq->rdSize > 0) ? ioReq->rdSize : ioReq->wrSize) << endl;
    cout << "    Trans ID = " << (int)ioReq->transId << ", Src ID = " <<
        (int)ioReq->srcId <<", Dest ID = " << (int)ioReq->destId  << endl;
    cout << "    Data size = " << (int)ioReq->dataSize << " DWORDs." << endl;
    if (ioReq->data != NULL)
    {
        for (int i = 0; i < (int)ioReq->dataSize; i++)
        {
            cout << "    DWORD[" << i << "] = " ;
            cout << setbase(16);
            cout << ioReq->data[i].msWord << "." << ioReq->data[i].lsWord
                << endl;
            cout << setbase(10);
        }
    }

}

// Display the Message request transaction
void TestLogicalIf::dumpTxTrans(const CarbonXSrioMessage *msgReq)
{
    cout <<"MESSAGE REQUEST:" << endl;
    cout << setbase(10);
    cout <<"    xmbox = " << (int)msgReq->xmbox <<", mbox = " <<
        (int)msgReq->mbox << ", letter = " << (int)msgReq->letter << endl;
    cout <<"    msglen = " << (int)msgReq->msglen << ", msgseg = " <<
        (int)msgReq->msgseg << ", ssize = " << (int)msgReq->ssize << endl;
    cout <<"    srcid = " << (int)msgReq->srcId << ", destid = " <<
        (int)msgReq->destId << ", prio = " << (int)msgReq->prio << endl;
    if ((int)msgReq->ssize >= 9 && (int)msgReq->ssize <= 14)
    {
        int dataSize = 0x1 << (msgReq->ssize - 9);
        for(int i = 0; i < dataSize; i++) 
        {
            cout << "    DWORD[" << i << "] = " << msgReq->data[i].msWord
                << "." << msgReq->data[i].lsWord << endl;
        }
    }

}

// Display the Doorbell request transaction
void TestLogicalIf::dumpTxTrans(const CarbonXSrioDoorbell *dbReq)
{
    cout << "DOORBELL REQUEST:" << endl;
    cout << "    info = " << (int)dbReq->info << endl;
    cout << "    srcid = " << (int)dbReq->srcId << ", destid = " <<
        (int)dbReq->destId << ", prio = " << (int)dbReq->prio << endl;

}

// Display the response transaction
void TestLogicalIf::dumpRxTrans()
{
    // Retrieve the response transaction from _responseQueue
    CarbonXSrioResponse *resp = _responseQueue.front();
    
    cout << "RESPONSE:" << endl;
    switch (resp->status)
    {
        case CarbonXResponseStatus_DONE:
            cout << "    Status = DONE" << endl;
            break;
        case CarbonXResponseStatus_RETRY:
            cout << "    Status = RETRY" << endl;
            break;
        case CarbonXResponseStatus_ERROR:
            cout << "    Status = ERROR" << endl;
            break;
        default: assert(0);
    }
    cout << setbase(10);
    cout << "    Trans ID = " << (int)resp->transId << ", Src ID = " <<
       (int)resp->srcId << ", Dest ID = " << (int)resp->destId << endl;
    cout << "    Data size = " << (int)resp->dataSize << " DWORDs." << endl;
    if (resp->data != NULL)
    {
        for(int i = 0; i < (int)resp->dataSize; i++)
        {
            cout << "    DWORD[" << i << "] = " ;
            cout << setbase(16);
            cout << resp->data[i].msWord << "." << resp->data[i].lsWord << endl;
            cout << setbase(10);
        }
    }

    // Delete the response transaction
    carbonXSrioDestroyResponse(resp);
    resp = NULL;
    _responseQueue.pop();
}
