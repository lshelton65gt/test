//  This module will iterate through the pixel locations to be read in the
//  output image.  It will fill in the appropriate pixels that correspond
//  to the new rotation of the original image within the virtual background.

module	scannewimage	(
			getpixel,
			pixel,
			rd_addr_pixel,
			pix_stall,
			start_scan,
			rd_pixel_data,
			bckgnd_pixel,
			brown,
			bcoln,
			o_ll_row,
			o_ll_col,
			o_up_row,
			o_up_col,
			cos,
			sin,
			clk,
			rst);

output		getpixel;
output	[23:0]	pixel;		// Valid the 2 cycles AFTER getpixel
output	[19:0]	rd_addr_pixel;
input		start_scan, pix_stall;
input	[23:0]	rd_pixel_data, bckgnd_pixel;
input	[15:0]	brown, bcoln, o_ll_row, o_ll_col, o_up_row, o_up_col;
input	[31:0]	cos, sin;
input		clk, rst;

wire	[15:0]	row_os, col_os;
wire	[16:0]	rot_rnd_row_x_pix, rot_rnd_row_y_pix;
wire	[47:0]	rotate_r_cos, 
		rotate_r_sin, 
		rotate_c_cos, 
		rotate_c_sin,
		rotate_pr_cos, 
		rotate_pr_sin,
		rotate_pc_cos,
		rotate_pc_sin,
		rotate_row_os,
		rotate_col_os,
		incr_cos,
		incr_sin,
		rotate_row_coord,
		rotate_col_coord,
		next_row_x_coord,
		next_row_y_coord,
		next_row_x_pix,
		next_row_y_pix;
wire		pix_inside, end_of_row, getpixel;
wire	[15:0]	orig_pix_row, orig_pix_col, next_pix_row, next_pix_col;
wire	[19:0]	next_rd_addr;
wire		next_getmore_pixels;
reg	[15:0]	pix_row, pix_col;
reg	[19:0]	rd_addr_pixel;
reg	[47:0]	rotate_row_x_coord,
		rotate_row_y_coord,
		rotate_row_x_pix,
		rotate_row_y_pix;
reg		getmore_pixels;

//
// Get the row and column os of the the background from the center of the 
// original image.  This will be a 17 bit value, both are negative numbers by
// definition, but we're carrying about absolute values.  BE CAREFUL OF THE
// SIGN!  Finally, the total offset is in half pixel increments to allow the 
// center of rotation calculation to allow for an even number of pixels.
//
offsetpixel	offsetpix (.total_roffset(row_os),
 		            .total_coffset(col_os),
		    	    .brown(brown),
			    .bcoln(bcoln));
//
// Given the cumalative COS and SIGN.  Calculate the coordinates of the new
// lower left corner.  The COS and SIN are carried around as whole numbers, and
// so they are absolute numbers with bit 31 representing the sign.  Remember
// that the math with the COS and SIN must be normalized.  The final addition
// of the below equations is done in two's compliment math.  I'm not
// accounting for the least significant bit properly, but that is so
// insignificant to the calculation that we won't lose any precision that we
// care about.
//
// The equation to rotate coordinates is as follows
//
// 		New X'	= (Old X * COS) + (Old Y * ~SIN)
// 		New Y'	= (Old X * SIN) + (Old Y *  COS)

// take 30 bits of fractional precision and
// 1 bit of integer precision (COS or SIN) and multiply it by 16
// bits of precision of .5 pixel precision to produce a 48 bit absolute
// magnitude with number, 31 bits of which are fractional.  We'll take care of
// the sign later.
//
//	The following equations result in 	Integer [47:31] : [30:0] Frac
//

assign	rotate_pr_cos	=	(row_os * cos[30:0]);
assign	rotate_pr_sin	=	(col_os * sin[30:0]);
assign	rotate_pc_sin	=	(row_os * sin[30:0]);
assign	rotate_pc_cos	=	(col_os	* cos[30:0]);

assign	rotate_r_cos[47]	=	~cos[31] & (|cos[30:0]);
assign	rotate_r_sin[47]	=	 sin[31] & (|sin[30:0]);
assign	rotate_c_cos[47]	=	~cos[31] & (|cos[30:0]);
assign	rotate_c_sin[47]	=	~sin[31] & (|sin[30:0]);

// Lose 1 bit of precision.  Not a big deal for this demo
//
// The following equations create signed subterms for the rotated coordinates.
// The values are 16 bits of integer [46:31] and 31 bits of fraction [30:0]:wq 
// 

assign	rotate_r_cos[46:0]	=	  (cos[31])
					? rotate_pr_cos[46:0]
					: ~rotate_pr_cos[46:0] + 1'b1;
assign	rotate_r_sin[46:0]	=	  (~sin[31])
					? rotate_pr_sin[46:0]
					: ~rotate_pr_sin[46:0] + 1'b1;
assign	rotate_c_cos[46:0]	=	  (cos[31])
					? rotate_pc_cos[46:0]
					: ~rotate_pc_cos[46:0] +1'b1;
assign	rotate_c_sin[46:0]	=	  (sin[31])
					? rotate_pc_sin[46:0]
					: ~rotate_pc_sin[46:0] +1'b1;

assign	rotate_row_os	=	rotate_r_cos + rotate_r_sin;
assign	rotate_col_os	=	rotate_c_sin + rotate_c_cos;

assign	incr_cos[47]	=	cos[31];
assign	incr_cos[46:0]	=	(cos[31])
				? {16'hffff, ~cos[30:0], 1'b1} + 48'h01
				: {16'h0000, cos[30:0], 1'b0};

assign	incr_sin[47]	=	sin[31];
assign	incr_sin[46:0]	=	(sin[31])
				? {16'hffff, ~sin[30:0], 1'b1} + 48'h01
				: {16'h0000, sin[30:0], 1'b0};

// Put the lower left corner of the rotated background in terms of the
// original background and image.  The signals "rotate_xxx_coord" are signed
// values, with the 48th bit being the sign value.  The "xxx_os" signals are
// absolute values for the Lower Left Corner offset, so they can be added to
// the rotated value to put the coordinates back into the original plane
// definition.  Remember that the following is 48 bit signed math, 47
// significant bits, of which 31 are fractional.  We will want to check the
// following result to see if it resides between the lower left or upper right
// corner of the original image, if not we will assign the background color.
// Bits 47:31 are the pixel address bits, but we will look at bit 30 to
// determine whether to round up or down.  We carry around the extra precision
// because the X & Y coordinates are being incremented by the COS and SIN of
// the current angle for the location of the next scan pixel.  

assign rotate_row_coord	=	rotate_row_os + {1'b0,row_os,30'b0};
assign rotate_col_coord	=	rotate_col_os + {1'b0,col_os,30'b0};

// The "rotate_xxx_coord" signals may be added to by the new COS and SIN
// values to grab the subseqent pixels to address in the original plane.
// With each pixel we need to see if they reside within the original image
// to see if we need to read pixel memory or just assign the background
// color.
//


assign end_of_row	=	((pix_col + 1'b1) == bcoln);

assign next_pix_row	=	  (start_scan)
				? 16'h0
				: (getpixel && end_of_row)
				? pix_row + 1'b1
				: pix_row;

wire   [15:0]	temp_pix_col0	=	pix_col + 16'h0001;
wire   [15:0]	temp_pix_col1	=    (~end_of_row) ? temp_pix_col0 : 16'h0000;
wire   [15:0]	temp_pix_col2	=    (getpixel)    ? temp_pix_col1 : pix_col;
assign 		next_pix_col	=    (start_scan)  ? 16'b0 : temp_pix_col2;
assign 		next_pix_col[0]	=    (start_scan)  ? 1'b0 : temp_pix_col2[0];


assign next_row_x_coord	=	  (start_scan)	
				? rotate_row_coord
				: (getpixel && end_of_row) 
				? rotate_row_x_coord + ~incr_sin + 1'b1
				: rotate_row_x_coord;
assign next_row_y_coord	=	  (start_scan)	
				? rotate_col_coord
				: (getpixel && end_of_row) 
				? rotate_row_y_coord + incr_cos
				: rotate_row_y_coord;

assign next_row_x_pix	=	  (start_scan)	
				? rotate_row_coord
				: (getpixel && end_of_row) 
				? rotate_row_x_coord + ~incr_sin + 1'b1
				: (getpixel && ~end_of_row)
				? rotate_row_x_pix + incr_cos
				: rotate_row_x_pix;

assign next_row_y_pix	=	  (start_scan)	
				? rotate_col_coord
				: (getpixel && end_of_row) 
				? rotate_row_y_coord + incr_cos
				: (getpixel && ~end_of_row)
				? rotate_row_y_pix + incr_sin
				: rotate_row_y_pix;




//
//  The following equation determines if the pixel of interest resides within 
//  the original image
//

//
// We need to round up or down to the appropriate pixel location based on
// bit 30
//

assign	rot_rnd_row_x_pix	=	  rotate_row_x_pix[47:31]
					+ rotate_row_x_pix[30]; 

assign	rot_rnd_row_y_pix	=	  rotate_row_y_pix[47:31]
					+ rotate_row_y_pix[30]; 

assign	pix_inside	=	  ~rot_rnd_row_x_pix[16]
				& ~rot_rnd_row_y_pix[16]
				& ( rot_rnd_row_x_pix[15:0] >= o_ll_col)
				& ( rot_rnd_row_x_pix[15:0] <=  o_up_col)
				& ( rot_rnd_row_y_pix[15:0] >= o_ll_row)
	 			& ( rot_rnd_row_y_pix[15:0] <=  o_up_row);

assign	pixel		=	  (pix_inside && getpixel) 
				? rd_pixel_data 
				: bckgnd_pixel;

/*

assign	orig_pix_col	=	rot_rnd_row_x_pix[15:0] + ~o_ll_col + 1'b1;
assign	orig_pix_row	=	rot_rnd_row_y_pix[15:0] + ~o_ll_row + 1'b1;

*/ 

assign	orig_pix_col	=	  next_row_x_pix[47:31]
				+ next_row_x_pix[30]
				+ ~o_ll_col
				+ 1'b1;
assign	orig_pix_row	=	  next_row_y_pix[47:31]
				+ next_row_y_pix[30]
				+ ~o_ll_row
				+ 1'b1;

assign	next_rd_addr	=	  (start_scan || getpixel)
				? {orig_pix_row[9:0], orig_pix_col[9:0]}
				: rd_addr_pixel;

//
//   Add controls to facilitate DMA fifo block.  This entails gathering up
//   4 bytes to write, not 3 for a single pixel.  This also means adding
//   a pipeline stage, pad bytes for the end of of each scan line of the BMP,
//   and factoring in fifo_full to hold everything.
//

assign	getpixel	=	getmore_pixels & ~pix_stall;

assign	next_getmore_pixels	=	  start_scan
					|  (getmore_pixels
					  & ~(((pix_col + 1'b1) == bcoln)
					     &((pix_row + 1'b1) == brown)
					     &~pix_stall));

always @(posedge clk)  begin

  if (rst) begin

	  pix_row	<=	16'h0;	// Row of output frame
	  pix_col	<=	16'h0;	// Col of output frame
	  rd_addr_pixel	<=	20'h0;
	  getmore_pixels<=	1'b0;


// 
// Coordinates of start of current rotated picture scan line
//
	  rotate_row_x_coord	<=	48'h0;	
	  rotate_row_y_coord	<=	48'h0;
//	  
// Coords of current pixel in the current scan line;
//
	  rotate_row_x_pix	<=	48'h0;
	  rotate_row_y_pix	<=	48'h0;

  end if(pix_stall) begin
	  pix_row		<=	pix_row;
	  pix_col		<=	pix_col;
	  rd_addr_pixel		<=	rd_addr_pixel;
	  rotate_row_x_coord	<=	rotate_row_x_coord;
	  rotate_row_y_coord	<=	rotate_row_y_coord;
	  rotate_row_x_pix	<=	rotate_row_x_pix;
	  rotate_row_y_pix	<=	rotate_row_y_pix;
	  getmore_pixels	<=	getmore_pixels;
  end else begin

	  pix_row		<=	next_pix_row;
	  pix_col		<=	next_pix_col;
	  rd_addr_pixel		<=	next_rd_addr;
	  rotate_row_x_coord	<=	next_row_x_coord;
	  rotate_row_y_coord	<=	next_row_y_coord;
	  rotate_row_x_pix	<=	next_row_x_pix;
	  rotate_row_y_pix	<=	next_row_y_pix;
	  getmore_pixels	<=	next_getmore_pixels;
    end
end

				
endmodule
