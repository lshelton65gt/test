module Lpra16x32 (
  wclk,
  wr,
  waddr,
  wdata,

  rclk,
  raddr,
  rdata
);

input          wclk;
input          wr;
input  [ 3:0]  waddr;
input  [31:0]  wdata;

input          rclk;
input  [ 3:0]  raddr;
output [31:0]  rdata;

wire	[31:0]	rdata;

reg    [31:0]  data[0:15];

always @ (posedge wclk) begin	
  if (wr) begin
	data[waddr] <= wdata;
    end
  end

assign	rdata	=	data[raddr];

endmodule
