module Fifo16x32 (
  reset_n,
  clock,

  // write interface
  wr,
  wData,
  full,

  // read interface
  rd,
  rData,
  empty
);

input          reset_n;
input          clock;

// write interface
input          wr;
input  [31:0]  wData;
output         full;

// read interface
input          rd;
output [31:0]  rData;
output         empty;

reg            full;
reg            empty;

reg    [3:0]   count_nxt;
reg    [3:0]   count;
reg    [3:0]   waddr_nxt;
reg    [3:0]   waddr;
reg    [3:0]   raddr_nxt;
reg    [3:0]   raddr;
reg            full_nxt;
reg            empty_nxt;


Lpra16x32 lpra16x320 (
  .wclk  ( clock ),
  .wr    ( wr ),
  .waddr ( waddr ),
  .wdata ( wData ),
  .rclk  ( clock ),
  .raddr ( raddr ),
  .rdata ( rData )
);


always @ (rd or wr or count or waddr or raddr or full or empty)
  begin
    count_nxt = count;
    waddr_nxt = waddr;
    raddr_nxt = raddr;
    full_nxt  = full;
    empty_nxt = empty;

    if(wr && rd) begin
      waddr_nxt = waddr + 1;
      raddr_nxt = raddr + 1;
    end else if(wr) begin
      count_nxt = count + 1;
      waddr_nxt = waddr + 1;
      full_nxt  = count > 4'hA;
      empty_nxt = 0;
    end else if(rd) begin
      count_nxt = count - 1;
      raddr_nxt = raddr + 1;
      full_nxt  = count > 4'hA;
      empty_nxt = (count == 1);
    end else begin
      // default, do nothing
    end
  end

always @ (negedge reset_n or posedge clock)
  if(! reset_n) begin
    count <= 0;
    waddr <= 0;
    raddr <= 0;
    full  <= 0;
    empty <= 1;
  end else begin
    count <= count_nxt;
    waddr <= waddr_nxt;
    raddr <= raddr_nxt;
    full  <= full_nxt;
    empty <= empty_nxt;
  end

endmodule
