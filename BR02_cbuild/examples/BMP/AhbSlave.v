`define  IDLE    2'b00
`define  BUSY    2'b01
`define  NONSEQ  2'b10
`define  SEQ     2'b11

module AhbSlave (
  // ahb interface
  hReset_n,
  hClk,
  hSel,
  hAddr,
  hWrite,
  hTrans,
  hSize,
  hBurst,
  hWData,
  hReadyIn,

  hReadyOut,
  hResp,
  hRData,

  // register file interface
  localRdAddr,
  addr,
  wEn,
  wData,
  rData
);

input          hReset_n;
input          hClk;
input          hSel;
input  [31:0]  hAddr;
input          hWrite;
input  [ 1:0]  hTrans;
input  [ 2:0]  hSize;
input  [ 2:0]  hBurst;
input  [31:0]  hWData;
input          hReadyIn;

output         hReadyOut;
output [ 1:0]  hResp;
output [31:0]  hRData;

// register file access interface
output [ 7:0]  localRdAddr;
output [ 7:0]  addr;
output         wEn;
output [31:0]  wData;

input  [31:0]  rData;


reg            addrPhase;
reg            dataPhase;
reg            write;
reg    [ 7:0]  latchedAddr;

reg    [ 7:0]  addr;
reg            wEn;
reg    [31:0]  wData;


always@(hTrans or hSel or hReadyIn)
  addrPhase = (hTrans == `NONSEQ || hTrans == `SEQ) && hSel && hReadyIn;


always@(negedge hReset_n or posedge hClk)
  begin
    if(! hReset_n)
      begin
        dataPhase <= 0;
        latchedAddr <= 0;
        addr <= 0;
        write <= 0;
        wEn <= 0;
        wData <= 0;
      end
    else
      begin
       if(addrPhase)
         begin
           dataPhase <= 1;
           latchedAddr <= hAddr[7:0];
           write <= hWrite;
         end
       else if(hReadyIn)
         begin
           dataPhase <= 0;
           latchedAddr <= 0;
           write <= 0;
         end
       else
         begin
           dataPhase <= dataPhase;
           latchedAddr <= latchedAddr;
           write <= write;
         end

        if(dataPhase && hReadyIn)
          begin
            addr <= latchedAddr;
            wEn <= write;
            wData <= hWData;
          end
        else
          begin
            addr <= 0;
            wEn <= 0;
            wData <= 0;
          end
      end

  end

assign hReadyOut = 1'b1;
assign hResp = 2'b00;
assign hRData = rData;
assign localRdAddr = latchedAddr;

endmodule
