module BmpProcessor (
  hReset_n,
  hClk,
  irq,

  // ahb slave bus
  hSel,
  hAddr,
  hWrite,
  hTrans,
  hSize,
  hBurst,
  hWData,
  // hReadyIn, --- this is the same as the master port one
  // ahb slave port response
  hReadyOutBp,
  hRespBp,
  hRDataBp,

  // ahb master bus
  hReady,
  hResp,
  hRData,
  // ahb master port request
  hBusReqBp,
  hGrantBp,
  hLockBp,
  hTransBp,
  hAddrBp,
  hWriteBp,
  hSizeBp,
  hBurstBp,
  hProtBp,
  hWDataBp
);

input          hReset_n;
input          hClk;

// slave port
input          hSel;
input  [31:0]  hAddr;
input          hWrite;
input  [ 1:0]  hTrans;
input  [ 2:0]  hSize;
input  [ 2:0]  hBurst;
input  [31:0]  hWData;
// input          hReadyIn;
// ahb slave port response
output         hReadyOutBp;
output [ 1:0]  hRespBp;
output [31:0]  hRDataBp;

output	       irq;

// ahb master bus
input          hReady;
input  [ 1:0]  hResp;
input  [31:0]  hRData;
// ahb master port request
input          hGrantBp;
output         hBusReqBp;
output         hLockBp;
output [ 1:0]  hTransBp;
output [31:0]  hAddrBp;
output         hWriteBp;
output [ 2:0]  hSizeBp;
output [ 2:0]  hBurstBp;
output [ 3:0]  hProtBp;
output [31:0]  hWDataBp;

wire           inFifoFull;
wire           inFifoWrite;
wire   [31:0]  inFifoWData;
wire           inFifoEmpty;
//wire           inFifoRead;
wire   [31:0]  inFifoRData;

wire           outFifoFull;
wire           outFifoWrite;
wire   [31:0]  outFifoWData;
wire           outFifoEmpty;
wire           outFifoRead;
wire   [31:0]  outFifoRData;

wire   [ 7:0]  localRdAddr;
wire   [ 7:0]  addr;
wire           wEn;
wire   [31:0]  wData;
wire   [31:0]  rData;

wire   [31:0]  cos;
wire   [31:0]  sin;
wire   [31:0]  dmaSrcAddr;
wire   [31:0]  dmaDstAddr;
wire   [31:0]  dmaLngth;

// pixel mem wires
wire   [19:0]  addr_pixel;
wire   [23:0]  wr_pixel_data;
wire   [23:0]  rd_pixel_data;

wire   [23:0]  backGrndColor;

wire   [9:0]   orown;
wire   [9:0]   ocoln;
wire   [15:0]  brown;
wire   [15:0]  bcoln;
wire   [19:0]  rd_addr_pixel;

wire   [15:0]  o_ll_row;
wire   [15:0]  o_ll_col;
wire   [15:0]  o_up_row;
wire   [15:0]  o_up_col;

//  scannewimage <-> PixelAsmblr wires

wire   [23:0]  pixel;
wire   	       getpixel;
wire           pix_stall;

// local mem block is accessed by DMA through master port
AhbMaster master0 (
  .hReset_n   ( hReset_n ),
  .hClk       ( hClk ),
  .hGrantX    ( hGrantBp ),
  .hReady     ( hReady ),
  .hResp      ( hResp ),
  .hRData     ( hRData ),
  .hBusReqX   ( hBusReqBp ),
  .hLockX     ( hLockBp ),
  .hTransX    ( hTransBp ),
  .hAddrX     ( hAddrBp ),
  .hWriteX    ( hWriteBp ),
  .hSizeX     ( hSizeBp ),
  .hBurstX    ( hBurstBp ),
  .hProtX     ( hProtBp ),
  .hWDataX    ( hWDataBp ),
  .wFifoFull  ( inFifoFull ),
  .wFifoWrite ( inFifoWrite ),
  .wFifoData  ( inFifoWData ),
  .rFifoEmpty ( outFifoEmpty ),
  .rFifoRead  ( outFifoRead ),
  .rFifoData  ( outFifoRData ),
  .start      ( start ),
  .dmaIn      ( dmaIn ),
  .dmaOut     ( dmaOut ),
  .dmaSrcAddr ( dmaSrcAddr ),
  .dmaDstAddr ( dmaDstAddr ),
  .dmaLngth   ( dmaLngth ),
  .dmaInDone  ( dmaInDone ),
  .dmaOutDone ( dmaOutDone )
);

// DMA in Fifo
Fifo16x32 dmaInFifo0 (
  .reset_n ( hReset_n ),
  .clock   ( hClk ),
  .wr      ( inFifoWrite ),
  .wData   ( inFifoWData ),
  .full    ( inFifoFull ),
  .rd      ( inFifoRd ),
  .rData   ( inFifoRData ),
  .empty   ( inFifoEmpty )
);

// DMA out Fifo
Fifo16x32 dmaOutFifo0 (
  .reset_n ( hReset_n ),
  .clock   ( hClk ),
  .wr      ( outFifoWrite ),        // to be connected from scannewimage
  .wData   ( outFifoWData ),       // to be connected from scannewimage
  .full    ( outFifoFull ), // to be connected to scannewimage
  .rd      ( outFifoRead ),
  .rData   ( outFifoRData ),
  .empty   ( outFifoEmpty )
);

// registers are accessed through slave port
AhbSlave slave0 (
  .hReset_n    ( hReset_n ),
  .hClk        ( hClk ),
  .hSel        ( hSel ),
  .hAddr       ( hAddr ),
  .hWrite      ( hWrite ),
  .hTrans      ( hTrans ),
  .hSize       ( hSize ),
  .hBurst      ( hBurst ),
  .hWData      ( hWData ),
  .hReadyIn    ( hReady ),
  .hReadyOut   ( hReadyOutBp ),
  .hResp       ( hRespBp ),
  .hRData      ( hRDataBp ),
  .localRdAddr ( localRdAddr ),
  .addr        ( addr ),
  .wEn         ( wEn ),
  .wData       ( wData ),
  .rData       ( rData )
);

RegBlock regs0 (
  .hReset_n    ( hReset_n ),
  .hClk        ( hClk ),
  .localRdAddr ( localRdAddr ),
  .addr        ( addr ),
  .wEn         ( wEn ),
  .wData       ( wData ),
  .rData       ( rData ),
  .cos         ( cos ),
  .sin         ( sin ),
  .orown       (orown),
  .ocoln       (ocoln),
  .backGrndColor ( backGrndColor ),
  .dmaSrcAddr  ( dmaSrcAddr ),
  .dmaDstAddr  ( dmaDstAddr ),
  .dmaLngth    ( dmaLngth ),
  .dmaIn       ( dmaIn ),
  .dmaOut      ( dmaOut ),
  .start       ( start ),
  .dmaInDone   ( dmaInDone ),
  .dmaOutDone  ( dmaOutDone ),
  .start_scan  ( start_scan ),
  .irq	       ( irq ),
  .brown       ( brown ),
  .bcoln       ( bcoln )
);

PixelAsmblr  pixassembler (
  .reset_n       ( hReset_n ),
  .clock         ( hClk ),
  .cols          ( bcoln[9:0] ),
  .valid         ( getpixel ),
  .pixel         ( pixel ),
  .stall         ( pix_stall ),
  .wr            ( outFifoWrite ),
  .wData         ( outFifoWData ),
  .full          ( outFifoFull )
);

scannewimage scannewimage0 (
  .getpixel	 ( getpixel ),
  .pixel	 ( pixel ),
  .rd_addr_pixel ( rd_addr_pixel ),
  .start_scan	 ( start_scan ),	//Must be supplied from RegBlock;
  .pix_stall	 (pix_stall ),
  .rd_pixel_data ( rd_pixel_data ),
  .bckgnd_pixel  ( backGrndColor ),	// Must be supplied from RegBlock
  .brown         ( brown ),
  .bcoln         ( bcoln ),
  .o_ll_row      ( o_ll_row ),
  .o_ll_col      ( o_ll_col ),
  .o_up_row      ( o_up_row ),
  .o_up_col      ( o_up_col ),
  .cos           ( cos ),
  .sin           ( sin ),
  .clk           ( hClk ),
  .rst           ( ~ hReset_n )
);

bckgrnsize    backGrndSize (
  .brown	 ( brown ),
  .bcoln	 ( bcoln ),
  .o_ll_row	 ( o_ll_row ),
  .o_ll_col	 ( o_ll_col ),
  .o_up_row	 ( o_up_row ),
  .o_up_col	 ( o_up_col ),
  .orown	 ( {6'b000000, orown} ),
  .ocoln	 ( {6'b000000, ocoln} )
);


MemController memController0 (
  .reset_n     ( hReset_n ),
  .clock       ( hClk ),
  .ocoln       ( ocoln ),
  .start       ( start ),
  .dmaIn       ( dmaIn ),
  .dmaOut      ( dmaOut ),
  .dmaInDone   ( dmaInDone ),
  .dmaInAddr   ( dmaDstAddr ),
  .rdAddrPixel ( rd_addr_pixel ), // to be connected from scannewimage rd_addr_pixel
  .rd          ( inFifoRd ),
  .rData       ( inFifoRData ),
  .empty       ( inFifoEmpty ),
  .memWData    ( wr_pixel_data ),
  .memWr       ( write_pixel_mem ),
  .memAddr     ( addr_pixel )
);

pixel_mem pixel_mem0 (
  .rd_pixel_data   ( rd_pixel_data ),
  .addr_pixel      ( addr_pixel ),
  .wr_pixel_data   ( wr_pixel_data ),
  .write_pixel_mem ( write_pixel_mem )
);

endmodule
