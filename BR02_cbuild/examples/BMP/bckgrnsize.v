//
//   This module calculates the size of the new background given the size of
//   the image to rotate
//

module bckgrnsize (brown, 
		   bcoln,
		   o_ll_row,
		   o_ll_col,
		   o_up_row,
		   o_up_col, 
		   orown, 
		   ocoln);

output	[15:0]	brown, bcoln;		//  New background limited to 32K X 32K
output	[15:0]	o_ll_row, o_ll_col;	//  lower left coordinates for orig
output	[15:0]	o_up_row, o_up_col;	//  upper right coordinates for orig
input	[15:0]	orown, ocoln;		//  Size of the original image

wire		orown_max;		//  True if there are more rows than
					//  columns
wire	[15:0]	bmaxn, omaxn;		//  Temp values to hold the count for 
					//  the larger edge
					//

wire	[15:0]	olow_left_row, olow_left_col;
wire	[15:0]	oup_right_row, oup_right_col;




assign	orown_max	=	(orown > ocoln);
assign	omaxn		=	(orown_max) ? orown : ocoln;

					//
					//  The following statement is to
					//  account for the sizing the
					//  background appropriately per the
					//  following table
					//
					//  [1:0] = 2'b00  add 0 extra pixels
					//  [1:0] = 2'b01  add 2 extra pixels
					//  [1:0] = 2'b10  add 1 extra pixel
					//  [1:0] = 2'b11  add 1 extra pixel
					//
					//  These extra pixels extend the
					//  background at least 1.5 x the
					//  original size.  It will also keep
					//  the same number of pixels of
					//  background on either side of the
					//  original image.  
					//
assign	bmaxn		=	  omaxn 
				+ (omaxn >> 1) 
				+ ((~omaxn[1] & omaxn[0])||omaxn[1]);

assign	brown		=	  (orown_max||(orown[0]==ocoln[0])) 
				? bmaxn 
				: (bmaxn - 1'b1);

assign	bcoln		=	  (~orown_max ||(orown[0]==ocoln[0])) 
				? bmaxn 
				: (bmaxn - 1'b1);

assign	olow_left_row	=	(brown - orown) >> 1;
assign	olow_left_col	=	(bcoln - ocoln) >> 1;
assign	oup_right_row	=	(olow_left_row + orown -1'b1);
assign	oup_right_col	=	(olow_left_col + ocoln -1'b1);

assign	o_ll_row	=	olow_left_row;
assign	o_ll_col	=	olow_left_col;

assign	o_up_row	=	oup_right_row;
assign	o_up_col	=	oup_right_col;
endmodule			  
