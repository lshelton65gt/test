module PixelAsmblr (
  reset_n,
  clock,

  // columns per row
  cols,

  // 24-bit pixel collect port
  valid,
  pixel,
  stall,

  // 32-bit write port to DMA out fifo
  wr,
  wData,
  full
);

input          reset_n;
input          clock;

input  [ 9:0]  cols;

input          valid;
input  [23:0]  pixel;
output         stall; 

output         wr;
output [31:0]  wData;
input          full;

reg            wr;
reg    [31:0]  wData;

reg    [23:0]  pixel0;
reg            empty0;
reg    [23:0]  pixel1;
reg            empty1;
reg    [23:0]  pixel2;
reg            empty2;
reg    [23:0]  pixel3;
reg            empty3;

reg    [ 1:0]  wrPtr;
reg    [ 1:0]  rdPtr;

reg    [ 9:0]  colCount;

wire [31:0] wData0 = {pixel1[ 7:0], pixel0};
wire [31:0] wData1 = {pixel2[15:0], pixel1[23:8]};
wire [31:0] wData2 = {pixel3,       pixel2[23:16]};

wire endOfRow = (colCount >= cols);

wire ready = ! full && ((! empty3 && ! empty2) || (! empty2 && ! empty1) ||
                        (! empty1 && ! empty0));


assign stall = ((! empty0) && (! empty1) && (! empty2) && (! empty3)) || endOfRow;

//   reg [31:0] ARL_COUNT;
   
always @ (negedge reset_n or posedge clock)
  if(! reset_n) begin
//    ARL_COUNT <= 31'h0;
    pixel0 <= 0;
    empty0 <= 1;
    pixel1 <= 0;
    empty1 <= 1;
    pixel2 <= 0;
    empty2 <= 1;
    pixel3 <= 0;
    empty3 <= 1;

    wrPtr <=0;
    rdPtr <=0;

    wr    <= 0;
    wData <= 0;

    colCount <= 0;
  end else begin
    // whenever there is valid pixel, it is stored in pixel0-3
    if(valid) begin
      case(wrPtr)
        0:
          begin
            pixel0 <= pixel;
//            pixel0 <= ARL_COUNT;
//	    ARL_COUNT <= ARL_COUNT+1;
            empty0 <= 0;
          end
        1:
          begin
            pixel1 <= pixel;
//            pixel1 <= ARL_COUNT;
//	    ARL_COUNT <= ARL_COUNT+1;
            empty1 <= 0;
          end
        2:
          begin
            pixel2 <= pixel;
//            pixel2 <= ARL_COUNT;
//	    ARL_COUNT <= ARL_COUNT+1;
            empty2 <= 0;
          end
        3:
          begin
            pixel3 <= pixel;
//            pixel3 <= ARL_COUNT;
//	    ARL_COUNT <= ARL_COUNT+1;
            empty3 <= 0;
          end
      endcase

      wrPtr    <= wrPtr + 1;
      colCount <= colCount + 1;
    end

    if(ready) begin
      wr <= 1;

      case(rdPtr)
        0:
          begin
            wData <= wData0;

            empty0      <= 1;
            pixel1[7:0] <= 0;
            pixel0      <= 0;
          end
        1:
          begin
            wData <= wData1;

            empty1       <= 1;
            pixel2[15:0] <= 0;
            pixel1[23:8] <= 0;
          end
        2:
          begin
            wData <= wData2;

            empty2        <= 1;
            empty3        <= 1;
            pixel3        <= 0;
            pixel2[23:16] <= 0;
          end
        3:
          $display("@%d, PixelAsmblr ready error!", $time);
      endcase

      rdPtr <= (rdPtr == 2) ? 0 : (rdPtr + 1);
    end else if(endOfRow) begin
// Fix:  Multiple entries can be non-empty, need to move this down below
//      pixel0 <= 0;
//      empty0 <= 1;
//      pixel1 <= 0;
//      empty1 <= 1;
//      pixel2 <= 0;
//      empty2 <= 1;
//      pixel3 <= 0;
//      empty3 <= 1;

//      wrPtr <=0;
//      rdPtr <=0;

//      colCount <= 0;

      if(! empty0) begin
        wr    <= 1;
        wData <= wData0;
        empty0      <= 1;
        pixel1[7:0] <= 0;
        pixel0      <= 0;
      end else if(! empty1) begin
        wr    <= 1;
        wData <= wData1;
        empty1       <= 1;
        pixel2[15:0] <= 0;
        pixel1[23:8] <= 0;
      end else if(! empty2) begin
        wr    <= 1;
        wData <= wData2;
        empty2        <= 1;
        empty3        <= 1;
        pixel3        <= 0;
        pixel2[23:16] <= 0;
      end else begin
        wr    <= 0;
        wData <= 0;
	wrPtr <=0;
	rdPtr <=0;

	colCount <= 0;
      end
     end else begin
      wr    <= 0;
      wData <= 0;
    end

  end

endmodule
