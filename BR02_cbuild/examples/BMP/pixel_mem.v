//
// This module instantiates memory pixels.  The memory array will be modeled in
// a 24 bit width (bits/pixel)
//

module	pixel_mem      (rd_pixel_data,
			addr_pixel,
			wr_pixel_data,
			write_pixel_mem);

output	[23:0]	rd_pixel_data;
input	[19:0]	addr_pixel;
input	[23:0]	wr_pixel_data;
input		write_pixel_mem;

reg	[23:0]	pixmem	[0:1023][0:1023];	// pixel memory
wire	[9:0]	pixrow;
wire	[9:0]	pixcol;

assign		pixrow 		=	addr_pixel[19:10];
assign		pixcol		=	addr_pixel[9:0];
assign		rd_pixel_data	=	pixmem [pixrow][pixcol];

always @(write_pixel_mem or pixrow or pixcol or wr_pixel_data)
  if(write_pixel_mem)
    pixmem [pixrow][pixcol] <=	wr_pixel_data;

endmodule
		  
