`define cmndsttsAddr       8'h00
`define srcImageAddr       8'h04
`define storeImageAddr     8'h08
`define backGrndColorAddr  8'h0C
`define cosAddr            8'h10
`define sinAddr            8'h14
`define orownAddr          8'h18
`define lngthAddr          8'h1C
`define brownAddr          8'h20


module RegBlock (
  hReset_n,
  hClk,

  // read/write interface from ahb slave interface
  localRdAddr,
  addr,
  wEn,
  wData,
  rData,

  cos,
  sin,

  orown,
  ocoln,
  backGrndColor,

  // dma control registers
  dmaSrcAddr,
  dmaDstAddr,
  dmaLngth,

  // control signals
  dmaIn,
  dmaOut,
  start,
  dmaInDone,
  dmaOutDone,
  start_scan,
  irq,
  brown,
  bcoln
);

input          hReset_n;
input          hClk;

input  [ 7:0]  localRdAddr;
input  [ 7:0]  addr;
input          wEn;
input  [31:0]  wData;

output [31:0]  rData;

output [31:0]  cos;
output [31:0]  sin;
output [ 9:0]  orown;
output [ 9:0]  ocoln;
output [23:0]  backGrndColor;
output [31:0]  dmaSrcAddr;
output [31:0]  dmaDstAddr;
output [31:0]  dmaLngth;

// control signals
output         dmaIn;
output         dmaOut;
output         start;
output         start_scan;
output	       irq;	

input          dmaInDone;
input          dmaOutDone;

input  [15:0]  brown;
input  [15:0]  bcoln;


reg    [ 7:0]  cmnd;
reg    [ 7:0]  stts;
reg    [31:0]  srcImage;
reg    [31:0]  storeImage;
reg    [23:0]  backGrndColor;
reg    [ 9:0]  orown;
reg    [ 9:0]  ocoln;
reg    [31:0]  cos;
reg    [31:0]  sin;
reg    [31:0]  dmaLngth;

reg    [31:0]  rData;

reg            dmaInDelay;
reg            dmaOutDelay;
reg            cmndCmplt;

reg            start;
reg            dmaIn;
reg            dmaOut;


parameter CMD_FETCH   = 8'h1;
parameter CMD_STORE   = 8'h2;
parameter CMD_RESET   = 8'hFF;


always@(negedge hReset_n or posedge hClk)
  if(! hReset_n) begin
    dmaInDelay  <= 0;
    dmaOutDelay <= 0;
    start       <= 0;
  end else begin
    dmaInDelay  <= dmaIn;
    dmaOutDelay <= dmaOut;
    start       <= (dmaIn && ~ dmaInDelay) || (dmaOut && ~ dmaOutDelay);
  end

assign start_scan 	=	dmaOut && ~dmaOutDelay;
// cmnd write
always@(negedge hReset_n or posedge hClk)
  if(! hReset_n) begin
    cmnd   <= 0;
    dmaIn  <= 0;
    dmaOut <= 0;
  end else if(wEn && (addr == `cmndsttsAddr)) begin
    cmnd <= wData[7:0];
    stts <= wData[15:8];

    if( wData[7:0] == CMD_FETCH ) begin
      dmaIn  <= 1;
      dmaOut <= 0;
    end else if( wData[7:0] == CMD_STORE ) begin
      dmaIn  <= 0;
      dmaOut <= 1;
    end else if ( wData[7:0] == CMD_RESET ) begin
      dmaIn  <= 0;
      dmaOut <= 0;
      stts[7] <= 0;
    end else begin
      dmaIn  <= dmaIn;
      dmaOut <= dmaOut;
    end
  end else begin
    cmnd   <= cmnd;
    stts[6:0]   <= stts[6:0];

    if (dmaInDone || dmaOutDone )
      stts[7]	<= 1'b1;

    if( dmaInDone )
      dmaIn  <= 0;

    if( dmaOutDone )
      dmaOut <= 0;
  end

assign irq	=	stts[7];


// srcImage write
always@(negedge hReset_n or posedge hClk)
  if(! hReset_n)
    srcImage <= 0;
  else if(wEn && (addr == `srcImageAddr))
    srcImage <= wData;
  else
    srcImage <= srcImage;

// storeImage write
always@(negedge hReset_n or posedge hClk)
  if(! hReset_n)
    storeImage <= 0;
  else if(wEn && (addr == `storeImageAddr))
    storeImage <= wData;
  else
    storeImage <= storeImage;

// backGrndColor write
always@(negedge hReset_n or posedge hClk)
  if(! hReset_n)
    backGrndColor <= 0;
  else if(wEn && (addr == `backGrndColorAddr))
    backGrndColor <= wData[23:0];
  else
    backGrndColor <= backGrndColor;

// cos write
always@(negedge hReset_n or posedge hClk)
  if(! hReset_n)
    cos <= 0;
  else if(wEn && (addr == `cosAddr))
    cos <= wData;
  else
    cos <= cos;

// sin write
always@(negedge hReset_n or posedge hClk)
  if(! hReset_n)
    sin <= 0;
  else if(wEn && (addr == `sinAddr))
    sin <= wData;
  else
    sin <= sin;

// original size write write
always@(negedge hReset_n or posedge hClk)
	if(! hReset_n) begin
    orown <= 0;
    ocoln <= 0;
    end
  else if(wEn && (addr == `orownAddr)) begin
    orown <= wData[9:0];
    ocoln <= wData[25:16];
    end
  else begin
    orown <= orown;
    ocoln <= ocoln;
    end

// dmaSrcAddr write
assign dmaSrcAddr = srcImage;

// dmaDstAddr write
assign dmaDstAddr = storeImage;

// dmaLngth write
always@(negedge hReset_n or posedge hClk)
  if(! hReset_n)
    dmaLngth <= 0;
  else if(wEn && (addr == `lngthAddr))
    dmaLngth <= wData;
  else
    dmaLngth <= dmaLngth;


wire busy = dmaIn || dmaOut;

always@(negedge hReset_n or posedge hClk)
  if(! hReset_n)
    cmndCmplt <= 1;
  else if(start)
    cmndCmplt <= 0;
  else if(dmaInDone || dmaOutDone)
    cmndCmplt <= 1;

// register read data path
always@(localRdAddr or cmnd or cmndCmplt or stts or busy
             or srcImage or storeImage or backGrndColor
             or cos or sin or dmaLngth or bcoln or brown or ocoln or orown)
  case (localRdAddr)
    `cmndsttsAddr: rData = {16'h0, stts, cmnd};
    `srcImageAddr: rData = srcImage;
    `storeImageAddr: rData = storeImage;
    `backGrndColorAddr: rData = {8'h0, backGrndColor};
    `cosAddr: rData = cos;
    `sinAddr: rData = sin;
    `orownAddr: rData = {orown, ocoln};
    `lngthAddr: rData = dmaLngth;
    `brownAddr: rData = {brown, bcoln};
    default: rData = 32'h0;
  endcase

endmodule
