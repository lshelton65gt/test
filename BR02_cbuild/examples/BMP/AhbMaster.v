/*

Feb 24, 2010 � TR � Qualify the write fifo signal (wFifoWrite) with hReady to avoid grabbing the wrong data when hReady isn�t active.  This means that we write the Last cycle of the SEQ state, and not the first.

*/

module AhbMaster (
  // ahb interface
  hReset_n,
  hClk,
  hGrantX,
  hReady,
  hResp,
  hRData,

  hBusReqX,
  hLockX,
  hTransX,
  hAddrX,
  hWriteX,
  hSizeX,
  hBurstX,
  hProtX,
  hWDataX,

  // local interface
  wFifoFull,
  wFifoWrite,
  wFifoData,

  rFifoEmpty,
  rFifoRead,
  rFifoData,

  // control information: src, dst, length
  start,
  dmaIn,
  dmaOut,
  dmaSrcAddr,
  dmaDstAddr,
  dmaLngth,
  dmaInDone,
  dmaOutDone
);

// ahb interface
input          hReset_n;
input          hClk;
input          hGrantX;
input          hReady;
input  [ 1:0]  hResp;
input  [31:0]  hRData;

output         hBusReqX;
output         hLockX;
output [ 1:0]  hTransX;
output [31:0]  hAddrX;
output         hWriteX;
output [ 2:0]  hSizeX;
output [ 2:0]  hBurstX;
output [ 3:0]  hProtX;
output [31:0]  hWDataX;

input          wFifoFull;
output         wFifoWrite;
output [31:0]  wFifoData;

input          rFifoEmpty;
output         rFifoRead;
input  [31:0]  rFifoData;

input          start; // needs to hold true during transfer, gets cleared after done
input          dmaIn;
input          dmaOut;
input  [31:0]  dmaSrcAddr;
input  [31:0]  dmaDstAddr;
input  [31:0]  dmaLngth;
output         dmaInDone;
output         dmaOutDone;


reg    [1:0]   addr_state;
reg	       data_state_val;
reg	       dmaLast, dmaInBufVal;
reg    [31:0]  dmaDataBuf;

reg    [31:0]  count;

reg    [31:0]  hAddrX;
reg            hWriteX;

reg            dmaInBusLast;

wire   [31:0]  count_start, count_nxt, dmaDataBuf_nxt, addr_nxt;
wire   [1:0]   addr_state_nxt;
wire	       addr_state_hold, data_state_hold, data_state_nxt;
wire	       dmaInBufVal_nxt, dmaInStart, dmaOutStart;
wire	       addr_last, addr_cross_boundry;


// addr state machine variable
parameter ADDR_IDLE	= 2'b00;
parameter ADDR_NONSEQ 	= 2'b10;
parameter ADDR_SEQ	= 2'b11;

//hTrans definitions
parameter TRANS_IDLE	= 2'b00;
parameter TRANS_BUSY	= 2'b01;
parameter TRANS_NONSEQ	= 2'b10;
parameter TRANS_SEQ	= 2'b11;


assign hBusReqX = addr_state[1];	//  Valid request active in Addr State
assign hBurstX 	= 1;  			// only support INCR transfer
assign hLockX 	= 0;   			// no locked transaction
assign hSizeX 	= 2;			// only 32-bit transfer

//
//  Tom's code starts here
// 


always@(negedge hReset_n or posedge hClk) begin
  if(~hReset_n)
    begin
      addr_state	<=	ADDR_IDLE;
      hAddrX		<=	32'h0;
      data_state_val	<=	1'b0;
      dmaLast		<=	1'b0;
      dmaInBufVal	<=	1'b0;
      dmaDataBuf	<=	32'h0;		
      count		<=	32'h0;
      hWriteX		<=	1'b0;
      dmaInBusLast	<=	1'b0;
    end
  else
    begin
      addr_state	<=	addr_state_nxt;
      hAddrX		<=	addr_nxt;
      data_state_val	<=	data_state_nxt;
      dmaLast		<=	dmaIn | dmaOut;		// only one may be active
      dmaInBufVal	<=	dmaInBufVal_nxt;
      dmaDataBuf	<=	dmaDataBuf_nxt;
      count 		<=	count_nxt;
      hWriteX		<=	dmaOut;    
      dmaInBusLast	<=	  (dmaInBusLast & ~hReady)
      				| (dmaIn & ~addr_state_hold & addr_last);
    end
end


assign      dmaInStart	=	dmaIn & ~dmaLast;
assign      dmaOutStart	=	dmaOut & ~dmaLast;
				//
				// The data phase of any request will
				// automatically complete with a hReady unless the write
				// fifo is full on a read.  Writes will
				// complete because Data is guarenteed to be
				// present in the Data Phase.
				//
				// The addr phase will advance to the
				// dataphase provided there is a Grant, that
				// the fifo isn't full on a read nor empty on
				// a write, and that the prior BUS cycle isn't
				// holding in the Data Phase.
				//
				// The First request will always be a NONSEQ
				// reguest.  subsequent requests will be
				// sequential unless we are crossing a line
				// boundry, which will be a NONSEQ state.
				//
				// The actual hTrans signal is calculated in
				// the ADDR PHASE.  For SEQ requests where
				// there is no room for a read or no data
				// present for a write, we will be in a BUSY
				// phase.  For NONSEQ states, TRANS will be
				// set to an IDLE phase while hBusReqX remains
				// high.

assign data_state_nxt	=	  (data_state_hold)		?  data_state_val
								:  ( ( dmaIn & ~wFifoFull )
								   | ( dmaOut & ~rFifoEmpty ) )
								&  addr_state[1] 
								&  ( hGrantX & hReady); 

assign data_state_hold	=	data_state_val & ~hReady;

assign addr_state_nxt	=	  (addr_state_hold)		?  addr_state
				: ( ( dmaInStart | dmaOutStart)  
				  | (  addr_cross_boundry 
				    & ~addr_last 
				    &  addr_state[1]) )	 	?  ADDR_NONSEQ
				: ( ~addr_cross_boundry 
				  & ~addr_last & addr_state[1])	?  ADDR_SEQ
								:  ADDR_IDLE;

assign addr_state_hold	=	  addr_state[1] 
				& ( ( dmaIn 
				    & (~( hGrantX & hReady) | wFifoFull | data_state_hold))
				  | ( dmaOut
				    & (~( hGrantX & hReady) | rFifoEmpty | data_state_hold)));

assign addr_nxt		=	  (addr_state_hold)		?  hAddrX
				: (dmaInStart)			?  dmaSrcAddr
				: (dmaOutStart)			?  dmaDstAddr
				: (~addr_last)			?  hAddrX + 32'H4
								:  32'H0;

				//  The dma length is in bytes, and every
				//  request is guaranteed to be of modulo
				//  4 bytes.  Hence, shift two bits to the
				//  right to get the absolute count of qord
				//  transfers.  Transfer counts are maintained
				//  in the ADDR Phase.
				//
				//  We will initiate NONSEQ reguests when we
				//  are about to cross line boundrys.  This
				//  requires that the current ADDR be checked
				//  for an address of 5'h1C in the least
				//  significant bits of the address.
				//

assign count_start	=	dmaLngth >> 2'h2;

assign addr_cross_boundry =	( hAddrX[4:0] == 5'h1c );
assign addr_last	=	( count == 32'h1);

assign count_nxt	=	  ( dmaInStart | dmaOutStart )	?  count_start
				: ( addr_state_hold)		?  count
				: ( ~addr_last)			?  count - 32'h1
								:  32'h0;

assign dmaInBufVal_nxt	=	dmaIn & wFifoFull & ( dmaInBufVal 
						    | (data_state_val & hReady));

assign dmaDataBuf_nxt	=	  (dmaIn & data_state_val & wFifoFull & hReady)
								?  hRData
				: (dmaOut & ~data_state_hold)
								?  rFifoData
							        :  dmaDataBuf;


assign rFifoRead 	= 	dmaOut & ~rFifoEmpty & hGrantX & hReady & ~data_state_hold;

assign wFifoWrite	=	dmaIn & ~wFifoFull & ( dmaInBufVal 
						     | (data_state_val & hReady));

assign wFifoData	=	(dmaInBufVal)	?	dmaDataBuf
						:	hRData;

assign hTransX		=	  (dmaIn)
				? ( (( addr_state == ADDR_NONSEQ ) 
				    & ~wFifoFull)		?  TRANS_NONSEQ
		  		  : (( addr_state == ADDR_SEQ ) 
				    & ~wFifoFull)		?  TRANS_SEQ
				  : (( addr_state == ADDR_SEQ ) 
				    & wFifoFull)		?  TRANS_BUSY
				 				:  TRANS_IDLE
				  )				
				: (dmaOut)
			        ? ( (( addr_state == ADDR_NONSEQ ) 
				    & ~rFifoEmpty)		?  TRANS_NONSEQ
		  		  : (( addr_state == ADDR_SEQ ) 
				    & ~rFifoEmpty)		?  TRANS_SEQ
				  : (( addr_state == ADDR_SEQ ) 
				    & rFifoEmpty)		?  TRANS_BUSY
				 				:  TRANS_IDLE
				  )				:  TRANS_IDLE;

assign hWDataX		=	dmaDataBuf;

assign dmaInDone	=	dmaInBusLast & hReady;
assign dmaOutDone	=	dmaOut & ~addr_state_hold & addr_last;

endmodule
