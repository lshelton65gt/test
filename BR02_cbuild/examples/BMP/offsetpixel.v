//
//	This module calculates the location of the offset pixel
//	The offset pixel is defined to be the first full pixel on the 
//	lower left side of the point of rotation.  Remember that the point of 
//	rotation may reside between two pixels when the axis of interest has
//	an even number of pixels, or it will reside on a center line of 
//	pixels when the axis of interest contains an odd number of pixels
//

module offsetpixel     (total_roffset,
       			total_coffset, 
       			brown,
		       	bcoln);

output	[15:0]	total_roffset, 
		total_coffset;

		      		//  coordinate pairs for offset pixel
				//  The abs_  pair is the absolute coordinates
				//  While the rel_ pair is relative to the 
				//  center of rotation.  This is needed to
				//  calculate scan rows
				//
input	[15:0]	brown, bcoln;		//  New background limited to 32K X 32K

wire	[15:0]	rel_roffset, rel_coffset;

assign	rel_roffset	=	 (brown[0]) ? 16'h0000 : 16'hffff;
assign	rel_coffset	=	 (bcoln[0]) ? 16'h0000 : 16'hffff;
assign	total_roffset	=	{brown[15:1], 1'b0} + rel_roffset;
assign	total_coffset	=	{bcoln[15:1], 1'b0} + rel_coffset;

endmodule			  
