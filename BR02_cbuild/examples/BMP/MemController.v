/*  16 February 2010
    Fixed a bug with the Memory Controller State Machine locking up in READ2 state.  Need to get a rd/Valid combo to break deadlock.

  Attempt #2.  Hopefully this works.

*/

module MemController (
  reset_n,
  clock,

  ocoln,

  start,
  dmaIn,
  dmaOut,
  dmaInDone,
  rdAddrPixel,

  rd,
  rData,
  empty,

  dmaInAddr,
  memWData,
  memWr,
  memAddr
);

input          reset_n;
input          clock;

// BMP column size
input  [9:0]   ocoln;

// mem addr control
input          start;
input          dmaIn;
input          dmaOut;
input          dmaInDone;

// pixel read addr from scannewimage
input  [19:0]  rdAddrPixel;

// pixel dma in buffer interface
output         rd;
input  [31:0]  rData;
input          empty;

input  [31:0]  dmaInAddr;
output         memWr;
output [19:0]  memAddr;
output [23:0]  memWData;

parameter      IDLE    = 3'h0;
parameter      READ1   = 3'h1;
parameter      READ2   = 3'h2;
parameter      WRITE0  = 3'h3;
parameter      WRITE1  = 3'h4;
parameter      WRITE2  = 3'h5;
parameter      WRITE3  = 3'h6;



//reg     [ 2:0] mState_nxt;
//reg     [ 2:0] mState;

//reg     [31:0] rdData_nxt;
//reg     [31:0] rdData;

//reg            memWr_nxt;
//reg            memWr;
//reg     [23:0] memWData_nxt;
//reg     [23:0] memWData;

reg     [ 9:0] colAddr_nxt;
reg     [ 9:0] colAddr;
reg     [ 9:0] rowAddr_nxt;
reg     [ 9:0] rowAddr;

//reg     [19:0] localWrAddr;
//reg     [19:0] localWrAddr_nxt;

//reg            endOfRow_nxt;

reg            dmaInDelay;


reg     [31:0] data0_nxt;
reg     [31:0] data1_nxt;
reg     [31:0] data2_nxt;
reg     [31:0] data0;
reg     [31:0] data1;
reg     [31:0] data2;

reg     [ 2:0] state_nxt;
reg     [ 2:0] state;

reg	[31:0] data;
reg            valid;

//reg     [ 9:0] words_nxt;
reg     [ 9:0] words;

reg            stopRead_nxt;
reg            stopRead;

reg            wr;
reg            wr_nxt;
reg     [ 9:0] wrAddr;
reg     [23:0] wrData;
reg     [23:0] wrData_nxt;


wire [9:0]     wordsPerRow = ((ocoln * 3) & 10'h3) ? (((ocoln * 3) >> 2) + 1) : ((ocoln * 3) >> 2);
wire           rdDone = (words == wordsPerRow);
wire           rd = ! empty && ! rdDone && ! stopRead;
wire           endOfRow = (colAddr == ocoln);


always @ (empty or data or state or valid or data0 or data1 or data2 or rdDone or colAddr or endOfRow or rowAddr or stopRead or rd)
begin
  state_nxt = state;

  data0_nxt = data0;
  data1_nxt = data1;
  data2_nxt = data2;

  wr_nxt     = 0;
  wrData_nxt = 0;

  stopRead_nxt = stopRead;

  rowAddr_nxt = rowAddr;
  colAddr_nxt = colAddr;

  case(state)
    IDLE:
      begin
        if(valid) begin
          state_nxt = READ1;

          data0_nxt = data;
        end

//        if(endOfRow)
//          rowAddr_nxt = rowAddr + 1;
      end

    READ1:
      begin
        if(rdDone) begin
          state_nxt = WRITE0;

          if(valid)
            data1_nxt = data;

        end else if(valid) begin
          state_nxt = READ2;

          data1_nxt = data;
          stopRead_nxt = rd;
        end
      end

    READ2:
      begin
        if(rdDone) begin
          state_nxt = WRITE0;

          if(valid)
            data2_nxt = data;

        end else if(valid) begin
          state_nxt = WRITE0;

          data2_nxt = data;
        end else
	  stopRead_nxt = rd;
      end

    WRITE0:
      begin
        colAddr_nxt = colAddr + 1;
        wr_nxt      = 1;
        wrData_nxt  = data0[23:0];

        state_nxt = WRITE1;
      end

    WRITE1:
      begin
        if(endOfRow) begin
          state_nxt = IDLE;
          stopRead_nxt= 0;

          data0_nxt =0;
          data1_nxt =0;
          data2_nxt =0;

          colAddr_nxt = 0;
          //rowAddr_nxt = rowAddr + 1;
          wr_nxt      = 0;
          wrData_nxt  = 0;
        end else begin
          state_nxt = WRITE2;

          colAddr_nxt = colAddr + 1;
          wr_nxt      = 1;
          wrData_nxt  = {data1[15:0], data0[31:24]};
        end
      end

    WRITE2:
      begin
        if(endOfRow) begin
          state_nxt = IDLE;
          stopRead_nxt = 0;

          data0_nxt =0;
          data1_nxt =0;
          data2_nxt =0;

          colAddr_nxt = 0;
          //rowAddr_nxt = rowAddr + 1;
          wr_nxt      = 0;
          wrData_nxt  = 0;
        end else begin
          state_nxt = WRITE3;

          colAddr_nxt = colAddr + 1;
          wr_nxt      = 1;
          wrData_nxt  = {data2[7:0], data1[31:16]};
        end
      end

    WRITE3:
      begin
        state_nxt = IDLE;
        stopRead_nxt= 0;

        if(endOfRow) begin
          data0_nxt =0;
          data1_nxt =0;
          data2_nxt =0;

          colAddr_nxt = 0;
          //rowAddr_nxt = rowAddr + 1;
          wr_nxt      = 0;
          wrData_nxt  = 0;
        end else begin
          data0_nxt =0;
          data1_nxt =0;
          data2_nxt =0;

          colAddr_nxt = colAddr + 1;
          wr_nxt      = 1;
          wrData_nxt  = data2[31:8];
        end
      end

  endcase
end


always @(negedge reset_n or posedge clock) begin
  if (! reset_n) begin
    dmaInDelay <= 0;

    state <= IDLE;

    words <= 0;
    valid <= 1'b0;
    data0 <= 0;
    data1 <= 0;
    data2 <= 0;

    rowAddr <= 0;
    colAddr <= 0;

    wrAddr <= 0;
    wr     <= 0;
    wrData <= 0;

    stopRead <= 0;
    data <= 0;
  end else begin
    if(dmaInDone) begin
      dmaInDelay <= 1;
    end else begin
	    if ( empty & endOfRow )
        dmaInDelay <= 0;
    end

    state  <= state_nxt;

    if(endOfRow)
      words <= 0;
    else if(rd)
      words <= words + 1;
    else
      words <= words;

    if(endOfRow)
      wrAddr <= 0;
    else if(wr)
      wrAddr <= wrAddr + 1;
    else
      wrAddr <= wrAddr;

    if(start && dmaIn) begin
      stopRead <= 0;

      rowAddr <= 0;
      colAddr <= 0;
    end else if(endOfRow && (dmaIn || dmaInDelay)) begin
      stopRead <= 0;

      rowAddr <= rowAddr + 1;
      colAddr <= 0;
    end else begin
      stopRead <= stopRead_nxt;

      colAddr <= colAddr_nxt;
      rowAddr <= rowAddr;
    end

    //rowAddr <= rowAddr_nxt;
    //colAddr <= colAddr_nxt;

    valid <= rd;
    data  <= rData;
    data0 <= data0_nxt;
    data1 <= data1_nxt;
    data2 <= data2_nxt;

    wr      <= wr_nxt;
    wrData  <= wrData_nxt;
  end
end


wire [19:0] localWrAddr = {rowAddr, wrAddr};

assign memWr = wr;
assign memAddr  = (dmaIn || dmaInDelay) ? localWrAddr : rdAddrPixel;
assign memWData = wrData;

endmodule
