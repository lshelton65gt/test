/* Hand-written SystemC testbench to drive count example */

#define CARBON_NO_UINT_TYPES 1  /* avoid typedefing for UInt32, etc */
#include "systemc.h"
#include "SystemC/libcount.systemc.h"
#include <vector>
#include <string>
#include <iostream>

SC_MODULE(count_test) {
  SC_HAS_PROCESS(count_test);
  
  // Ports
  sc_in<bool>  clk;
  sc_out<sc_uint<32> > out;
  sc_out<sc_uint<32> > out2;

  // Signal Declarations
  sc_signal<bool> reset;
  sc_signal<bool> hold;
  sc_signal<bool> ena_debug;
  
  // Carbon Model COUNT Instance, which is created by cbuild and
  // declared in libcount.systemc.h
  count count0;
  
  // Constructor
  count_test(sc_module_name inst, int offset, int interval, bool doPrint,
             bool ena_debug, CarbonSys*);
  
  // Test function
  void test_func();

  // callback to tell the carbon system how many cycles have been run
  static CarbonUInt64 getCycleCount(void* clientData);

  // callback to tell the carbon system the current sim time
  static double getSimTime(void* clientData);

  // Member Variables
  bool         mDoPrint;
  int          mCycleCount;
  int          mOffset;
  int          mInterval;
  CarbonSys*   mSystem;
};

count_test::count_test(sc_module_name inst, int offset, int interval,
                       bool doPrint, bool enaDebug, CarbonSys* system) :
  sc_module(inst),
  count0("count0"),
  mDoPrint(doPrint),
  mOffset(offset),
  mInterval(interval),
  mCycleCount(0),
  mSystem(system)
{
  // setup test thread
  SC_THREAD(test_func);
  
  // Connect signals
  count0.clk(clk);
  count0.reset(reset);
  count0.hold(hold);
  count0.out(out);
  count0.out2(out2);
  count0.ena_debug(ena_debug);
  ena_debug = enaDebug;
}

// Provide the cycle count for the carbon system to update the GUI
// with accurate CPS numbers
CarbonUInt64 count_test::getCycleCount(void* clientData) {
  count_test* at = (count_test*) clientData;
  return at->mCycleCount;
}

// Provide the cycle count for the carbon system to update the GUI
// with accurate CPS numbers
double count_test::getSimTime(void* /* clientData */) {
  // count_test* at = (count_test*) clientData;
  return sc_simulation_time();
}

void count_test::test_func() {
  bool do_hold = true;
  int interval = mInterval;

  for (int i = 0; true; ++i) {
    reset.write(i < 10);       // reset first 10 cycles
    if (i > mOffset) {
      if (--interval == 0) {
        do_hold = !do_hold;
        carbonSystemUpdateGUI(mSystem);
        interval = mInterval;
        //std::cerr << "updating...\n";
        hold.write(do_hold);
      }
    }

    // synchronize with the model
    wait(clk.posedge_event());
    
    // Update cycle count
    mCycleCount++;

    // Wait a little extra so the output signals have time to update
    wait(1, SC_NS);

    CarbonUInt32 outval = out.read();
    CarbonUInt32 outval2 = out2.read();
    if (mDoPrint) {
      cout << outval << " " << outval2 << " ";
    }

    if ((i % 5000) == 0) {
      carbonSystemUpdateGUI(mSystem);
    }
  }
} // void count_test::test_func

SC_MODULE(top) {
  
  sc_clock        clk;
  sc_signal<sc_uint<32> > out0, out1, out2, out02, out12, out22;
  count_test        count_test0;
  count_test        count_test1;
  count_test        count_test2;
  CarbonSys*        mSystem;
  
  SC_HAS_PROCESS(top);
  top(sc_module_name inst, bool doPrint, bool ena_debug,
      CarbonSys* system) : 
    clk("clk", 10, SC_NS),
    out0("out0"),
    out1("out1"),
    out2("out2"),
    out02("out02"),
    out12("out12"),
    out22("out22"),
    count_test0("count_test0", 500, 30000, doPrint, ena_debug, system),
    count_test1("count_test1", 500, 30000, doPrint, ena_debug, system),
    count_test2("count_test2", 500, 30000, doPrint, ena_debug, system),
    mSystem(system)
  {
    count_test0.clk(clk);
    count_test0.out(out0);
    count_test0.out2(out02);

    count_test1.clk(clk);
    count_test1.out(out1);
    count_test1.out2(out12);

    count_test2.clk(clk);
    count_test2.out(out2);
    count_test2.out2(out22);
  }

};

int sc_main (int argc , char *argv[]) 
{
  int          cycles = 200000;
  int          divert = 0;
  bool         doPrint = false;
  bool         ena_debug = false;

  // Setup carbon System
  CarbonSys* system = carbonGetSystem(NULL);
  carbonSystemPutName(system, argv[0]);

  sc_set_time_resolution(1, SC_PS);
  carbonSystemPutSimTimeUnits(system, "ps");


  for(int i = 0; i < argc; i ++) {
    if (std::string(argv[i]) == "-cycles") {
      cycles = atoi(argv[++i]);
    }
    else if (std::string(argv[i]) == "-divert") {
      divert = atoi(argv[++i]);
    }
    else if (std::string(argv[i]) == "-verbose") {
      doPrint = true;
    }
    else if (std::string(argv[i]) == "-enaDebug") {
      ena_debug = true;
    }
  }
   
  top top0("top", doPrint, ena_debug, system);
  //carbonSystemPutCycleCountCB(system, count_test::getCycleCount,
  //                            (void*) &top0.count_test0);
  carbonSystemPutSimTimeCB(system, count_test::getSimTime,
                           (void*) &top0.count_test0);
	
  // Setup Reporting
  sc_report_handler::set_actions(SC_ERROR, SC_LOG | SC_DISPLAY | SC_CACHE_REPORT);

  cout << "Running " << cycles << " cycles." << endl;
  sc_start(cycles*10);

  // Shut down system system
  carbonSystemShutdown(system);

  // And Report Errors
  int sc_errors = sc_report_handler::get_count(SC_ERROR) + sc_report_handler::get_count(SC_FATAL);

  cout << "---------------------------------------" << endl
       << "- Number of SystemC Errors: " << sc_errors << endl
       << "-" << endl;

  if (sc_errors == 0)
    cout << "- Test PASSED" << endl;
  else
    cout << "- Test FAILED" << endl;

  cout << "---------------------------------------" << endl;

  return sc_errors;
  
}
