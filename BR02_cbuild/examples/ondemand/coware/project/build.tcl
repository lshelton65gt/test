# Include files are in the parent directory
cwr_append_simbld_opts preprocessor -I..
# disable the use of ~/.ccache directory
cwr_append_simbld_opts cacheObjects 0

puts "Sourcing Carbon Command File"
source libcount.coware.cmd

puts "Compiling CoWare Design"
load count.cpp libcount.systemc.cpp
