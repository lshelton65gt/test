/* Hand-written C testbench to drive count example */

#define CARBON_NO_UINT_TYPES 1  /* avoid typedefing for UInt32, etc */
#include "libcount.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "carbon/carbon_system.h"

/* Wrapper struct for an instance of the Carbon model for 'count'. */
typedef struct {
  CarbonNetID    * m_clk;
  CarbonNetID    * m_reset;
  CarbonNetID    * m_hold;
  CarbonNetID    * m_ena_debug;
  CarbonNetID    * m_out;
  CarbonNetID    * m_out2;
  CarbonObjectID * m_hdl;
  CarbonUInt32     mClockVal;
  CarbonUInt32     mHoldVal;
  int              mDoPrint;
  int              mOffset;
  int              mInterval;
  int              mStartInterval;
  CarbonSys*       mSystem;
} Count;

Count* Count_init(int doPrint, CarbonUInt32 enaDebugCounter,
                  int offset, int interval,
                  CarbonSys* system, const char* name)
{
  CarbonUInt32 one = 1;
  CarbonInitFlags flags = (CarbonInitFlags)(eCarbon_OnDemand | eCarbon_NoInit);
  Count* count = (Count*) malloc(sizeof(Count));
  count->mDoPrint = doPrint;
  count->mOffset = offset;
  count->mInterval = interval;
  count->mStartInterval = interval;
  count->mSystem = system;

  count->m_hdl = carbon_count_create(eCarbonFullDB, flags);
  carbonChangeMsgSeverity(count->m_hdl, 5065, eCarbonMsgSuppress);
  carbonInitialize(count->m_hdl, NULL, NULL, NULL);
  count->m_clk = carbonFindNet(count->m_hdl, "count.clk");
  assert(count->m_clk);
  count->m_hold = carbonFindNet(count->m_hdl, "count.hold");
  assert(count->m_hold);
  count->m_reset = carbonFindNet(count->m_hdl, "count.reset");
  assert(count->m_reset);
  count->m_ena_debug = carbonFindNet(count->m_hdl, "count.ena_debug");
  assert(count->m_ena_debug);
  count->m_out = carbonFindNet(count->m_hdl, "count.out");
  assert(count->m_out);
  count->m_out2 = carbonFindNet(count->m_hdl, "count.out2");
  assert(count->m_out2);

  /*
   * ena_debug is passed on the command line and held constant for
   * the entire simulation.  Start out in reset
   */
  carbonDeposit(count->m_hdl, count->m_ena_debug, &enaDebugCounter, 0);
  fprintf(stdout, "Enabling Debug: %d\n", (int) enaDebugCounter);
  carbonDeposit(count->m_hdl, count->m_reset, &one, 0);

  count->mClockVal = 0;
  count->mHoldVal = 1;
  carbonSystemAddComponent(system, name, &count->m_hdl);
  return count;
}


void Count_destroy(Count* count) {
  carbonDestroy(&count->m_hdl);
  free(count);
}

void Count_run(Count* count, CarbonTime ticks) {
  CarbonUInt32 zero = 0;
  /* after 10 cycles, de-assert reset */
  if (ticks == 10) {
    fprintf(stdout, "Deasserting reset\n");
    fflush(stdout);
    carbonDeposit(count->m_hdl, count->m_reset, &zero, 0);
  }
    
  /* after 'offset', toggle hold every 'interval' cycles */
  if ((ticks > count->mOffset) && (--count->mInterval == 0)) {
    count->mHoldVal = !count->mHoldVal;
    fprintf(stdout, "Toggling Hold, new value = %d\n", (int) count->mHoldVal);
    fflush(stdout);
    carbonSystemUpdateGUI(count->mSystem);
    count->mInterval = count->mStartInterval;
    carbonDeposit(count->m_hdl, count->m_hold, &count->mHoldVal, 0);
  }

  count->mClockVal = !count->mClockVal;
  carbonDeposit(count->m_hdl, count->m_clk, &count->mClockVal, 0);
  carbonSchedule(count->m_hdl, ticks);

  if (count->mDoPrint) {
    CarbonUInt32 outval, outval2;
    carbonExamine(count->m_hdl, count->m_out, &outval, 0);
    carbonExamine(count->m_hdl, count->m_out2, &outval, 0);
    fprintf(stdout, "Hold=%d\tOut=%d\tOut2=%d\n",
            (int) count->mHoldVal, (int) count->m_out, (int) count->m_out2);
  }
}


/* callback to tell the carbon system how many cycles have been run */
static CarbonUInt64 sGetCycleCount(void* clientData) {
  int* cycle = (int*) clientData;
  return *cycle;
}

int main(int argc, char **argv) {
  int doPrint = 0;
  int cycles = 200000;
  int arg = 1;
  CarbonUInt32 ena_debug = 0;
  int cycle = 0;
  int offset = 500;
  int interval = 30000;
  const char* fsdb_name = NULL;

  CarbonSys* system = carbonGetSystem(NULL);
  carbonSystemPutName(system, argv[0]);
  carbonSystemPutCycleCountCB(system, sGetCycleCount, &cycle);

  while (argc > arg) {
    if (strcmp(argv[arg], "-print") == 0) {
      /* Use -print if you want to see the i/o vectors on stdout */
      doPrint = 1;
      ++arg;
    }
    else if (strcmp(argv[arg], "-cycles") == 0) {
      char* endp;
      ++arg;
      cycles = strtol(argv[arg], &endp, 10);
      ++arg;
      if ((cycles == 0) || (*endp != '\0')) {
        fprintf(stderr, "Invalid args\n");
        exit(1);
      }
    }
    else if (strcmp(argv[arg], "-enaDebug") == 0) {
      /* Use -enaDebug if you want to enable the free-running debug counter */
      ena_debug = 1;
      ++arg;
    }
    else if (strcmp(argv[arg], "-fsdb") == 0) {
      fsdb_name = argv[++arg];
      ++arg;
    }
    else {
      fprintf(stderr, "Invalid argument: %s\n", argv[arg]);
      exit(1);
    }
  }


  {
    Count* count1 = Count_init(doPrint, ena_debug, offset, interval,
                               system, "count1");

#define MULTI_COMP 1
#if MULTI_COMP 
    Count* count2 = Count_init(doPrint, ena_debug, offset, interval,
                               system, "count2");
    Count* count3 = Count_init(doPrint, ena_debug, offset, interval,
                               system, "count3");
#endif

    /* Add waveforms for just one of the models if requested */
    if (fsdb_name != NULL) {
      CarbonWaveID* wave = carbonWaveInitFSDB(count1->m_hdl, fsdb_name, e1ns);
      carbonDumpVars(wave, 0, "count");
    }

    carbonSystemUpdateGUI(system);
    if (carbonSystemReadFromGUI(system, 1) == eCarbonSystemCmdError) {
      fprintf(stdout, "%s\n", carbonSystemGetErrmsg(system));
      carbonSystemClearCycleCountCB(system);
      return 1;
    }

    fprintf(stdout, "Executing %d cycles\n", cycles);

    for (; cycle < cycles; ++cycle) {
      if ((cycle % 1000) == 0) {
        carbonSystemUpdateGUI(system);
      }
      Count_run(count1, cycle);
#if MULTI_COMP 
      Count_run(count2, cycle);
      Count_run(count3, cycle);
#endif
    }
    carbonSystemClearCycleCountCB(system);
    carbonSystemShutdown(system);

    Count_destroy(count1);
#if MULTI_COMP 
    Count_destroy(count2);
    Count_destroy(count3);
#endif
  }
  
  fprintf(stdout, "Simulation Complete\n");  

  return 0;
}
