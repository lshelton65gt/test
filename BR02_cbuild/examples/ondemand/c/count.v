module count(clk, reset, hold, ena_debug, out, out2);
  input clk, reset, hold, ena_debug;
  output [31:0] out, out2;
  reg [31:0]    out, out2;
  reg [31:0]    debug_counter;  // carbon observeSignal
  reg [2:0]     state;

  initial begin
    debug_counter = 0;
    state = 3'h0;
  end

  always @(posedge clk) begin : scope
    // This loop is meant just to slow down the model without introducing
    // any new state that OnDemand would detect as acyclic.  The loop
    // can't be (easily) unrolled and accelerated because the loop bounds
    // depend on primary-input "hold".
    integer i;
    out2 = 0;
    for (i = 0; i < 32'hffff - hold; i = i + 1)
      out2 = out2 + 32'd1;

    // compute output value based on reset and current state
    if (reset)
      out <= 32'b0;
    else if (!hold && (state == 3'h2))
      out <= out + 32'd1;

    // Simple state-machine next-state logic.  3-bit state wraps around.
    state <= state + 3'h1;
  end

  // this debug counter auto-increments and will prevent automatic OnDemand
  // idle detection unless
  always @(posedge clk)
    if (ena_debug)
      debug_counter = debug_counter + 1;
endmodule
