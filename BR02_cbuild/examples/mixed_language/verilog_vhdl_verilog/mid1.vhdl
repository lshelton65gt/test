-- Description:  This file contains a mid level entity used in a mixed language
-- testcase.


library ieee ; 
use ieee.std_logic_1164.all ;
use work.all ;

entity mid1 is
  port (clock : in  std_logic ;
        reset : in  std_logic ;
        output : out std_logic_vector(31 downto 0)) ;

end;

architecture arch of mid1 is

  component counter1 is
                       port(clock: in std_logic;
                            reset: in std_logic;
                            out1: out std_logic_vector(31 downto 0));
  end component;

begin
  U1: counter1 port map(clock,reset,output);

end;

