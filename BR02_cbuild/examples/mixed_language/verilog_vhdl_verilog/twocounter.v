// filename: examples/mixed_language/verilog_vhdl_verilog/twocounter
// 
// This testcase demonstrates compiling and running a simple design that is a
// mixed language design. In this case the top level is verilog, the middle
// layer is VHDL and the bottom layer is verilog again.

// the functionality is the same as the testcase in examples/twocounter

module twocounter (clk1, clk2, reset1, reset2, out1, out2) ;
   input clk1, clk2, reset1, reset2;
   output [31:0] out1, out2;

   mid1 i1(clk1, reset1, out1);
   MID3 i2(clk2, reset2, out2);	// note module name is uppercase, actual vhdl
				// name is mixed case, but the match is found

endmodule

