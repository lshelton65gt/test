module Counter3(clock, reset, out1);
   input clock;
   input reset;
   output [31:0] out1;
   reg [31:0] 	 out1;

   always @(posedge clock)
     begin: main
	if (reset )
	  out1 = 32'b0;
	else
	  out1 = out1 + 32'd3;
     end
endmodule
