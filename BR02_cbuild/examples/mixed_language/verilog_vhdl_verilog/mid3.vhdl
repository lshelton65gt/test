-- Description:  This file contains a mid level entity used in a mixed language
-- testcase.


library ieee ; 
use ieee.std_logic_1164.all ;
use work.all ;

entity Mid3 is
  port (mid_clk3 : in  std_logic ;
        mid_reset3 : in  std_logic ;
        mid_out3 : out std_logic_vector(31 downto 0)) ;

end Mid3 ;

architecture arch_mid3 of mid3 is

  component Counter3 is
                       port( clock: in std_logic;
                             reset: in std_logic;
                             out1: out std_logic_vector(31 downto 0));
  end component;

begin
  U1: counter3 port map(clock => mid_clk3,
                        reset => mid_reset3,
                        out1 => mid_out3);

end;

