-- This demonstrates compiling and running a simple design with
-- two counters driven by two clocks

library ieee;
use ieee.std_logic_1164.all;
-- use ieee.std_logic_unsigned.all;

entity incr is
  generic ( step : integer := 1);
  port (clk, rst: in std_logic;
        outp : out std_logic_vector(31 downto 0));
end;

architecture arch of incr is 
 begin
 end;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
 entity twocounter is
   port (clk1, clk2, reset1, reset2 : in std_logic;
         out1, out2 : out std_logic_vector(31 downto 0));
 end twocounter;

 architecture arch_twocounter of twocounter is

   component incr
     generic ( step : integer );
     port ( clk, rst: in std_logic; outp : out std_logic_vector(31 downto 0));
   end component;

 begin
   incr1: incr
     generic map (1) port map(clk1, reset1, out1);
   incr2: incr
     generic map (3) port map(clk2, reset2, out2);
 end;



