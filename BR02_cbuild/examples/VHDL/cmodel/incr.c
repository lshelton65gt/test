/* C-model implementation of increment module */

#include "cds_libtwocounter_INCR.h"   /* generated header file */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* struct to keep state between calls */
typedef struct
{
    CarbonUInt32 step;	/* step amount for increment */
    CarbonUInt32 out;		/* output register */	
} incr_state;

void* cds_INCR_create(int numParams,
                      CModelParam* cmodelParams,
                      const char* inst)
{
    /* allocate state struct */
    incr_state *state = (incr_state*)malloc(sizeof(incr_state));

    /* determine the increment step from parameter */
    assert(numParams == 1);
    state->step = atoi(cmodelParams[0].paramValue);

    /* init output state */
    state->out = 0;

    return (void*)state;
}

void cds_INCR_misc(void* hndl,
                   CarbonCModelReason reason,
                   void* cmodelData)
{
}

void cds_INCR_run(void* hndl,
                  CDSINCRContext context,
                  const CarbonUInt32* clk, // Input, size = 1 word(s)
                  const CarbonUInt32* rst, // Input, size = 1 word(s)
                  CarbonUInt32* out // Output, size = 1 word(s)
  )
{
    /* retrieve state */
    incr_state *state = (incr_state*)hndl;

    /* implement behavior */
    assert (context == eCDSINCRRiseCLK);
    if (*rst)
	state->out = 0;
    else
	state->out += state->step;

    /* drive output pin */
    *out = state->out;
}

void cds_INCR_destroy(void* hndl)
{
    /* deallocate */
    incr_state *state = (incr_state*)hndl;
    free(state);
}
