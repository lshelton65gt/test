/* Hand-written C testbench to drive twocounter example */

#include "libtwocounter.h"   /* generated header file */
#include <stdio.h>
#include <assert.h>

/* Convenience routine to examine one 32 bit value */
static unsigned long examine1(CarbonObjectID *model,
                              CarbonNetID* net)
{
  CarbonUInt32 val;
  (void) carbonExamine(model, net, &val, 0);
  return val;
}

static unsigned long examine1Input(CarbonObjectID *model,
                                   CarbonNetID* net)
{
  CarbonUInt32 val;
  (void) carbonExamine(model, net, &val, 0);
  return val;
}

int main()
{
  /* Instantiate a two counter model */
  CarbonObjectID *twocounter =
    carbon_twocounter_create(eCarbonIODB, eCarbon_NoFlags);

  /* Get handles to all the I/O nets */
  CarbonNetID* clk1 = carbonFindNet(twocounter, "TWOCOUNTER.CLK1");
  CarbonNetID* clk2 = carbonFindNet(twocounter, "TWOCOUNTER.CLK2");
  CarbonNetID* reset1 = carbonFindNet(twocounter, "TWOCOUNTER.RESET1");
  CarbonNetID* reset2 = carbonFindNet(twocounter, "TWOCOUNTER.RESET2");
  CarbonNetID* out1 = carbonFindNet(twocounter, "TWOCOUNTER.OUT1");
  CarbonNetID* out2 = carbonFindNet(twocounter, "TWOCOUNTER.OUT2");

  CarbonTime t = 0;
  int clk1tick = 0;
  int clk2tick = 0;
  CarbonUInt32 one = 1;
  CarbonUInt32 zero = 0;

  /* Start out in reset for both domains */
  carbonDeposit(twocounter, reset1, &one /* active high */, 0);
  carbonDeposit(twocounter, reset2, &one /* active high */, 0);

  for (t = 0; t < 100000; t += 100)
  {
    /*
     * Advance clk1 every 700 ticks
     */
    if ((t % 700) == 0)
    {
      CarbonUInt32 val;
      ++clk1tick;
      val = clk1tick & 1;
      carbonDeposit(twocounter, clk1, &val, NULL);

      /* After 5 edges de-assert reset */
      if (clk1tick == 5)
        carbonDeposit(twocounter, reset1, &zero, NULL);
    }

    /*
     * Advance clk2 every 800 ticks.  There will be an occasional
     * coincident edge with clk1
     */
    if ((t % 800) == 0)
    {
      CarbonUInt32 val;
      ++clk2tick;
      val = clk2tick & 1;
      carbonDeposit(twocounter, clk2, &val, NULL);

      /* After 5 edges de-assert reset */
      if (clk2tick == 5)
        carbonDeposit(twocounter, reset2, &zero, NULL);
    }

    /*
     * Run a schedule, print out results
     */
    carbonSchedule(twocounter, t);
    fprintf(stdout,
            "%lu:\tclk1=%ld reset1=%ld clk2=%ld reset2=%ld out1=%ld out2=%ld\n",
            (unsigned long) t,
            examine1Input(twocounter, clk1),
            examine1Input(twocounter, reset1),
            examine1Input(twocounter, clk2),
            examine1Input(twocounter, reset2),
            examine1(twocounter, out1),
            examine1(twocounter, out2));
  }

  carbonDestroy(&twocounter);
  return 0;
}
