#
#  Makefile for   test/cexplore, combined from test/replay and test/wizard
#

include $(CARBON_HOME)/makefiles/Makefile.common

CFILES = alu.cpp libalu.systemc.cpp

SYSTEMC_HOME ?= $(CARBON_HOME)/systemc
SYSTEMC_INCLUDES = -I$(SYSTEMC_HOME)/include

UNAME = $(shell uname)
LIBDIR = unknown-arch
ifeq ($(UNAME),Linux)
ifeq ($(CARBON_TARGET_ARCH),Linux64)
  LIBDIR = lib-linux64
else
  LIBDIR = lib-linux
endif
endif
ifeq ($(UNAME),SunOS)
  LIBDIR = lib-gccsparcOS5
endif

SYSTEMC_LIB = -L${SYSTEMC_HOME}/$(LIBDIR) -lsystemc

CFLAGS = -O3 $(SYSTEMC_INCLUDES)

STATEXT ?= a

MY_CBUILD_FLAGS ?= -g -2001 -enableReplay

CBUILD_FLAGS= -q $(MY_CBUILD_FLAGS)

.PHONY : record playback diverge run normal gui

CYCLE_COUNT = 100000
EXTRA_CYCLE_COUNT = 150000

all : normal record playback diverge

run : alu.exe

record : alu.exe
	./alu.exe -verboseReplay -record -cycles $(CYCLE_COUNT)

normal : alu.exe
	./alu.exe -cycles $(CYCLE_COUNT)

playback : alu.exe
	./alu.exe -verboseReplay -playback -cycles $(EXTRA_CYCLE_COUNT)

diverge : alu.exe
	./alu.exe -playback -divert 75728 -verboseReplay -cycles $(CYCLE_COUNT)

# in order to run the gui we need some alu.exe.css to have been created,
# so make a small one by default
gui : alu.exe.css
	$(CARBON_HOME)/bin/ccontrol alu.exe.css

alu.exe.css : alu.exe
	./alu.exe -cycles 100

%.o: %.cpp libalu.systemc.h
	$(CARBON_CXX) -c $(CFLAGS) $<


alu.exe: $(CFILES:.cpp=.o) libalu.$(STATEXT)
	$(CARBON_LINK) -o $@ $^ libalu.$(STATEXT) $(CARBON_LIB_LIST) $(SYSTEMC_LIB)

libalu.a libalu.io.db : alu.v
	$(CARBON_CBUILD) $(CBUILD_FLAGS) $< -o libalu.$(STATEXT)

libalu.systemc.cpp libalu.systemc.h : libalu.io.db
	$(CARBON_HOME)/bin/carbon systemCWrapper libalu.io.db
clean::
	$(RM) *.bld *.a *.obj *.so *.o *.css *.csu
	$(RM) -rf libalu*.* .carbon.* *.rdb* *.log
