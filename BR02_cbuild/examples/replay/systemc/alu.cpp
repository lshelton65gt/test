/* Hand-written SystemC testbench to drive alu example */

#define CARBON_NO_UINT_TYPES 1  /* avoid typedefing for UInt32, etc */
#include "systemc.h"
#include "libalu.systemc.h"
#include <vector>
#include <string>
#include <iostream>




// Integer generator for stimuli.  It is not important for this test
// that the integers be random, just that they vary.  It's good enough
// that they vary by incrementing. 
class IntGen {
public:
  IntGen(CarbonSys* system) : mSystem(system), mIndex(10000), mState(0) {
  }

  CarbonUInt32 next() {
    // Every once in a while, wake up the carbon system to let it update
    // the GUI by writing alu.exe.css.  This allows the GUI to plot the
    // CPU performance graph
    if (--mIndex == 0) {
      carbonSystemUpdateGUI(mSystem);
      mIndex = 10000;
    }
    mState += 1610612741;
    return mState;
  }

  void reshuffle(CarbonUInt32 val) {
    mState = val;
  }

  CarbonSys* mSystem;
  CarbonUInt32 mState;
  CarbonUInt32 mIndex;
};


SC_MODULE(alu_test) {
  SC_HAS_PROCESS(alu_test);
  
  // Ports
  sc_in<bool>  clk;
  sc_out<bool> done;

  // Signal Declarations
  sc_signal<sc_uint<32> > in;
  sc_signal<sc_uint<32> > out;
  sc_signal<sc_uint<32> > out2;
  sc_signal<sc_uint<4> > op;
  
  // Carbon Model ALU Instance, which is created by cbuild and
  // declared in libalu.systemc.h
  alu alu0;
  
  // Constructor
  alu_test(sc_module_name inst, int seed, int divert, bool doPrint,
           IntGen* intGen);
  
  // Test function
  void test_func();

  // callback to tell the carbon system how many cycles have been run
  static CarbonUInt64 getCycleCount(void* clientData);

  // callback to tell the carbon system the current sim time
  static double getSimTime(void* clientData);

  // Member Variables
  bool         mDoPrint;
  int          mCycleCount;
  int          mDivert;
  IntGen       *mIntGen;
};

alu_test::alu_test(sc_module_name inst, int seed, int divert, bool doPrint,
                   IntGen* rand) : 
  sc_module(inst),
  alu0("alu0"),
  mDoPrint(doPrint),
  mCycleCount(0),
  mDivert(divert),
  mIntGen(rand)
{
  // setup test thread
  SC_THREAD(test_func);
  
  // Connect signals
  alu0.clk(clk);
  alu0.in(in);
  alu0.out(out);
  alu0.out2(out2);
  alu0.op(op);
}

// Provide the cycle count for the carbon system to update the GUI
// with accurate CPS numbers
CarbonUInt64 alu_test::getCycleCount(void* clientData) {
  alu_test* at = (alu_test*) clientData;
  return at->mCycleCount;
}

// Provide the cycle count for the carbon system to update the GUI
// with accurate CPS numbers
double alu_test::getSimTime(void* /* clientData */) {
  // alu_test* at = (alu_test*) clientData;
  return sc_simulation_time();
}

void alu_test::test_func()
{
  // Make sure that done deasserted
  done->write(false);

  while(1) {
    // Check if we should create a divergence
    // If we should, we do that be reseding the intGen generator
    if (mDivert != 0 && mDivert == mCycleCount) {
      mIntGen->reshuffle(mDivert);
    }

    // op-codes defined in alu.v range from 0 to 11
    CarbonUInt32 val = mIntGen->next() % 12; 
    op.write(val);

    // input data can be any 32-bit value
    val = mIntGen->next();
    in.write(val);
    
    // synchronize with the model
    wait(clk.posedge_event());
    
    // Update cycle count
    mCycleCount++;

    // Wait a little extra so the output signals have time to update
    wait(1, SC_NS);

    CarbonUInt32 outval = out.read();
    if (mDoPrint) {
      cout << outval << " ";
    }
  }
}

SC_MODULE(top) {
  
  sc_clock        clk;
  sc_signal<bool> done0, done1;
  alu_test        alu_test0;
  alu_test        alu_test1;
  
  SC_HAS_PROCESS(top);
  top(sc_module_name inst, int divert0, int divert1, bool doPrint,
      IntGen* intGen) : 
    clk("clk", 10, SC_NS),
    done0("done0"),
    done1("done1"),
    alu_test0("alu_test0", 2, divert0, doPrint, intGen),
    alu_test1("alu_test1", 3, divert1, doPrint, intGen)
  {
    alu_test0.clk(clk);
    alu_test0.done(done0);

    alu_test1.clk(clk);
    alu_test1.done(done1);
  }

};

int sc_main (int argc , char *argv[]) 
{
  int          cycles = 100;
  int          divert = 0;
  bool         doPrint = false;
  bool         record = false;
  bool         playback = false;

  // Setup carbon System
  CarbonSys* system = carbonGetSystem(NULL);
  carbonSystemPutName(system, argv[0]);

  sc_set_time_resolution(1, SC_PS);
  carbonSystemPutSimTimeUnits(system, "ps");

  for(int i = 0; i < argc; i ++) {
    if (std::string(argv[i]) == "-cycles") {
      cycles = atoi(argv[++i]);
    }
    else if (std::string(argv[i]) == "-divert") {
      divert = atoi(argv[++i]);
    }
    else if (std::string(argv[i]) == "-verbose") {
      doPrint = true;
    }
    else if (std::string(argv[i]) == "-record") {
      record = true;
    }
    else if (std::string(argv[i]) == "-playback") {
      playback = true;
    }
  }
   
  IntGen intGen(system);

  top top0("top", divert, divert, doPrint, &intGen);
  //carbonSystemPutCycleCountCB(system, alu_test::getCycleCount,
  //                            (void*) &top0.alu_test0);
  carbonSystemPutSimTimeCB(system, alu_test::getSimTime,
                           (void*) &top0.alu_test0);

  if (record) {
    carbonReplayRecordStart(top0.alu_test0.alu0.carbonModelHandle);
    carbonReplayRecordStart(top0.alu_test1.alu0.carbonModelHandle);
  }
  if (playback) {
    carbonReplayPlaybackStart(top0.alu_test0.alu0.carbonModelHandle);
    carbonReplayPlaybackStart(top0.alu_test1.alu0.carbonModelHandle);
  }
	
  // Setup Reporting
  sc_report_handler::set_actions(SC_ERROR, SC_LOG | SC_DISPLAY | SC_CACHE_REPORT);

  cout << "Running " << cycles << " cycles." << endl;
  sc_start(cycles*10);

  // Shut down system system
  carbonSystemShutdown(system);

  // And Report Errors
  int sc_errors = sc_report_handler::get_count(SC_ERROR) + sc_report_handler::get_count(SC_FATAL);

  cout << "---------------------------------------" << endl
       << "- Number of SystemC Errors: " << sc_errors << endl
       << "-" << endl;

  if (sc_errors == 0)
    cout << "- Test PASSED" << endl;
  else
    cout << "- Test FAILED" << endl;

  cout << "---------------------------------------" << endl;

  return sc_errors;
  
}
