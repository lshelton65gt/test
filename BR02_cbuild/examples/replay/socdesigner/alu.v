module alu(clk, in, out0, out1, op);
  input clk;
  input [31:0] in;
  input [3:0]  op;
  output [31:0] out0;
  output [31:0] out1;


  parameter RST = 0;
  parameter ADD = 1;
  parameter SUB = 2;
  parameter MUL = 3;
  parameter POW = 4;
  parameter LSHIFT = 5;
  parameter RSHIFT = 6;
  parameter XOR = 7;
  parameter AND = 8;
  parameter OR = 9;
  parameter NOT = 10;
  parameter NEGATE = 11;

  reg [31:0]    out0, out1;
  integer       i;

  always @(posedge clk) begin
    out1 = 0;
    for (i = 0; i < in[15:0]; i = i + 1)
      out1 = out1 + in;

    case (op)
      RST: 	out0 <= 0;
      ADD: 	out0 <= out0 + in;
      SUB: 	out0 <= out0 - in;
      MUL:   	out0 <= out0 * in;
      POW: 	out0 <= out0 ** in;
      LSHIFT:   out0 <= out0 << in;
      RSHIFT:   out0 <= out0 >> in;
      XOR: 	out0 <= out0 ^ in;
      AND:	out0 <= out0 & in;
      OR: 	out0 <= out0 | in;
      NOT: 	out0 <= ~in;
      NEGATE: 	out0 <= -in;
    endcase
  end

endmodule
