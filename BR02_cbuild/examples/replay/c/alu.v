module alu(clk, in, out, out2, op);
  input clk;
  input [31:0] in;
  input [3:0]  op;
  output [31:0] out;
  output [31:0] out2;

  parameter RST = 0;
  parameter ADD = 1;
  parameter SUB = 2;
  parameter MUL = 3;
  parameter POW = 4;
  parameter LSHIFT = 5;
  parameter RSHIFT = 6;
  parameter XOR = 7;
  parameter AND = 8;
  parameter OR = 9;
  parameter NOT = 10;
  parameter NEGATE = 11;

  reg [31:0]    out, out2;
  integer       i;

  always @(posedge clk) begin
    out2 = 0;

    // Slow loop to force this carbon model to run in the 10kz
    // range, rather than the 1mhz range.
    for (i = 0; i < in[15:0]; i = i + 1)
      out2 = out2 + in;

    case (op)
      RST: 	out <= 0;
      ADD: 	out <= out + in;
      SUB: 	out <= out - in;
      MUL:   	out <= out * in;
      POW: 	out <= out ** in;
      LSHIFT:   out <= out << in;
      RSHIFT:   out <= out >> in;
      XOR: 	out <= out ^ in;
      AND:	out <= out & in;
      OR: 	out <= out | in;
      NOT: 	out <= ~in;
      NEGATE: 	out <= -in;
    endcase
  end
endmodule
