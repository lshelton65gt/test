#include "libalu.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "carbon/carbon_system.h"

/*
 * Integer generator for stimuli.  It is not important for this test
 * that the integers be random, just that they vary.  We can get a
 * reasonably decent distribution by incrementing by a large prime.
 */
CarbonUInt32 generate_integer(CarbonUInt32* state) {
  *state += 1610612741;
  return *state;
}

void generate_reset(CarbonUInt32* state, CarbonUInt32 val) {
  *state = val;
}

/* Wrapper struct for an instance of the Carbon model for an ALU. */
typedef struct {
  CarbonNetID * m_clk;
  CarbonNetID * m_in;
  CarbonNetID * m_op;
  CarbonNetID * m_out;
  CarbonObjectID *m_hdl;
  CarbonUInt32 mClockVal;
  int mDoPrint;
  CarbonUInt32* mIntGen;
} Alu;

/*
 * In the constructor, get handles for the I/O nets, and register
 * the component with the Carbon system.
 */
Alu* Alu_init(int doPrint, CarbonSys* system,
              const char* name, CarbonUInt32* intGen)
{
  CarbonInitFlags flags = (CarbonInitFlags)(eCarbon_Replay | eCarbon_NoInit);
  Alu* alu = (Alu*) malloc(sizeof(Alu));
  alu->mDoPrint = doPrint;
  alu->mIntGen = intGen;
  alu->m_hdl = carbon_alu_create(eCarbonIODB, flags);
  carbonChangeMsgSeverity(alu->m_hdl, 5065, eCarbonMsgSuppress);
  carbonInitialize(alu->m_hdl, NULL, NULL, NULL);
  alu->m_clk = carbonFindNet(alu->m_hdl, "alu.clk");
  assert(alu->m_clk);
  alu->m_in = carbonFindNet(alu->m_hdl, "alu.in");
  assert(alu->m_in);
  alu->m_op = carbonFindNet(alu->m_hdl, "alu.op");
  assert(alu->m_op);
  alu->m_out = carbonFindNet(alu->m_hdl, "alu.out");
  assert(alu->m_out);

  alu->mClockVal = 0;
  carbonSystemAddComponent(system, name, &alu->m_hdl);
  return alu;
}

void Alu_destroy(Alu* alu) {
  carbonDestroy(&alu->m_hdl);
  free(alu);
}

void Alu_run(Alu* alu, CarbonTime ticks) {
  CarbonUInt32 out;
  UInt32 val = generate_integer(alu->mIntGen) % 11;
  alu->mClockVal = !alu->mClockVal;
  carbonDeposit(alu->m_hdl, alu->m_clk, &alu->mClockVal, 0);
  carbonDeposit(alu->m_hdl, alu->m_op, &val, 0);
  val = generate_integer(alu->mIntGen);
  carbonDeposit(alu->m_hdl, alu->m_in, &val, 0);
  carbonSchedule(alu->m_hdl, ticks);

  carbonExamine(alu->m_hdl, alu->m_out, &out, 0);
  if (alu->mDoPrint) {
    fprintf(stdout, "%lu\n", (unsigned long) out);
  }
}

/*
 * Callback function so that the Replay system can determine how
 * many cycles have been run
 */
static CarbonUInt64 sGetCycleCount(void* clientData) {
  int* count = (int*) clientData;
  return (CarbonUInt64) (*count);
}

int main(int argc, char **argv) {
  int doPrint = 0;
  int cycles = 100;
  int arg = 1;
  int diverge = -1;
  CarbonUInt32 intGen = 0;
  int cycle = 0;
  int record = 0;
  int playback = 0;
  int verboseReplay = 0;

  CarbonSys* system = carbonGetSystem(NULL);
  carbonSystemPutName(system, argv[0]);
  carbonSystemPutCycleCountCB(system, sGetCycleCount, &cycle);

  while (argc > arg) {
    if (strcmp(argv[arg], "-print") == 0) {
      /* Use -print if you want to see the i/o vectors on stdout */
      doPrint = 1;
      ++arg;
    }
    else if (strcmp(argv[arg], "-cycles") == 0) {
      char* endp;
      ++arg;
      cycles = strtol(argv[arg], &endp, 10);
      ++arg;
      if ((cycles == 0) || (*endp != '\0')) {
        fprintf(stderr, "Invalid args\n");
        exit(1);
      }
    }
    else if (strcmp(argv[arg], "-divert") == 0) {
      char* endp;
      ++arg;
      diverge = strtol(argv[arg], &endp, 10);
      ++arg;
      if ((diverge == 0) || (*endp != '\0')) {
        fprintf(stderr, "Invalid args\n");
        exit(1);
      }
    }
    else if (strcmp(argv[arg], "-record") == 0) {
      record = 1;
      ++arg;
    }
    else if (strcmp(argv[arg], "-playback") == 0) {
      playback = 1;
      ++arg;
    }
    else if (strcmp(argv[arg], "-verboseReplay") == 0) {
      verboseReplay = 1;
      ++arg;
    }
    else {
      fprintf(stderr, "Invalid argument: %s\n", argv[arg]);
      exit(1);
    }
  }


  {
    Alu* alu1 = Alu_init(doPrint, system, "alu1", &intGen);
    Alu* alu2 = Alu_init(doPrint, system, "alu2", &intGen);
    Alu* alu3 = Alu_init(doPrint, system, "alu3", &intGen);
    if (verboseReplay) {
      carbonSystemSetVerboseReplay(system, 1);
    }
    if (record) {
      carbonReplayRecordStart(alu1->m_hdl);
      carbonReplayRecordStart(alu2->m_hdl);
      carbonReplayRecordStart(alu3->m_hdl);
    }
    if (playback) {
      carbonReplayPlaybackStart(alu1->m_hdl);
      carbonReplayPlaybackStart(alu2->m_hdl);
      carbonReplayPlaybackStart(alu3->m_hdl);
    }

    carbonSystemUpdateGUI(system);
    if (carbonSystemReadFromGUI(system, 1) == eCarbonSystemCmdError) {
      fprintf(stdout, "%s\n", carbonSystemGetErrmsg(system));
      carbonSystemClearCycleCountCB(system);
      return 1;
    }

    for (; cycle < cycles; ++cycle) {
      if (cycle == diverge) {
        generate_reset(&intGen, diverge);
      }
      if ((cycle % 5000) == 0) {
        carbonSystemUpdateGUI(system);
      }
      Alu_run(alu1, cycle);
      Alu_run(alu2, cycle);
      Alu_run(alu3, cycle);
    }
    carbonSystemClearCycleCountCB(system);
    carbonSystemShutdown(system);

    Alu_destroy(alu1);
    Alu_destroy(alu2);
    Alu_destroy(alu3);
  }
  
  return 0;
}
