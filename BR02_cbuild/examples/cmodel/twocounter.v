// This demonstrates compiling and running a simple design with
// two counters driven by two clocks

module twocounter(clk1, clk2, reset1, reset2, out1, out2);
  input clk1, clk2, reset1, reset2;
  output [31:0] out1, out2;

  incr #(1) incr1(.clk(clk1), .rst(reset1), .out(out1));
  incr #(3) incr2(.clk(clk2), .rst(reset2), .out(out2));
   
endmodule


module incr(clk, rst, out);
   parameter STEP = 1;

   input     clk, rst;
   output    [31:0] out;

endmodule

