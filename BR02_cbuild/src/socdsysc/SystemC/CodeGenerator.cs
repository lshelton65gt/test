﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using CPPStuff;
using Antlr.StringTemplate;

namespace systemc
{
  public class VisualStudio
  {
    public enum VisualStudioVersion { VS2003, VS2005 };

    public VisualStudio(string name, VisualStudioVersion version)
    {
      Name = name;
      Version = version;
    }
    public override string ToString() { return Name; }
    public string Name { get; set; }
    public string VcprojVersion { get; set; }
    public string SolutionVersion { get; set; }
    public string EnvVarName { get; set; }
    public string VisualCPPGuid { get; set; }
    public VisualStudioVersion Version { get; set; }

    public static VisualStudio getVs(VisualStudio.VisualStudioVersion v)
    {
      VisualStudio vs = null;

      switch (v)
      {
        case VisualStudio.VisualStudioVersion.VS2003:

          vs = new VisualStudio("VS C++ .NET 2003", VisualStudio.VisualStudioVersion.VS2003);
          vs.EnvVarName = "VS71COMNTOOLS";
          vs.SolutionVersion = "8.00";
          vs.VcprojVersion = "7.10";
          vs.VisualCPPGuid = "{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}";
          break;

        case VisualStudio.VisualStudioVersion.VS2005:
          vs = new VisualStudio("VS C++ 2005", VisualStudio.VisualStudioVersion.VS2005);
          vs.EnvVarName = "VS80COMNTOOLS";
          vs.VcprojVersion = "8.00";
          vs.SolutionVersion = "9.00";
          vs.VisualCPPGuid = "{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}";
          break;
      }

      return vs;
    }

  }

  public class ModuleItem
  {
    public CPPClassOrStruct ClassOrStruct { get; set; }
    public SystemCModule SysCModule { get; set; }
  }

  // Represents the users's choices from the GUI
  // Associated with each treeNode
  public class ModuleParameter
  {
    public enum ParameterType { Boolean, String };
    public enum ParameterKind { InitTime, RunTime };

    public string Name { get; set; }
    public ParameterType Type { get; set; }
    public ParameterKind Kind { get; set; }
    public string DefaultValue { get; set; }

    public string CasiType
    {
      get
      {
        switch (Type)
        {
          case ParameterType.Boolean: return "eslapi::CASI_PARAM_BOOL";
          case ParameterType.String: return "eslapi::CASI_PARAM_STRING";
        }
        return "eslapi::CASI_PARAM_STRING";
      }
    }
    public string CasiKind
    {
      get
      {
        switch (Kind)
        {
          case ParameterKind.InitTime: return "false";
          case ParameterKind.RunTime: return "true";
        }
        return "true";
      }
    }
  }


  [Serializable]
  public class ModuleConfiguration
  {
    public ModuleConfiguration()
    {
      Module = null;
      Clocks = new List<DataMember>();
      Parameters = new List<ModuleParameter>();
    }

    public DataMember findClock(string portName)
    {
      foreach (DataMember mi in Clocks)
      {
        if (mi.Name == portName)
          return mi;
      }
      return null;
    }

    [XmlIgnoreAttribute]
    public ModuleItem Module { get; set; }

    public string ClassName{ get; set; }
    public string ClassTemplatizedName { get; set; }
    public List<DataMember> Clocks { get; set; }
    public SystemCConstructor Constructor { get; set; }
    public string ParameterCallbackSource { get; set; }
    public List<ModuleParameter> Parameters { get; set; }
  }

  [Serializable]
  public class ModuleConfigurations
  {
    public ModuleConfigurations()
    {
      Configurations = new List<ModuleConfiguration>();
      SourceFiles = new List<string>();
      IncludeFiles = new List<string>();
    }
    [XmlArrayItemAttribute("SourceFile", typeof(string))]
    public List<string> SourceFiles { get; set; }

    [XmlArrayItemAttribute("IncludeFile", typeof(string))]
    public List<string> IncludeFiles { get; set; }

    public string GccXmlFile { get; set; }
    public string DesignFile { get; set; }
    public string HeaderGuard { get; set; }
    public string IncludePaths { get; set; }
    public string Definitions { get; set; }
    public string LinkerFlags { get; set; }
    public string LinkerPaths { get; set; }

    [XmlArrayItemAttribute("ModuleConfiguration", typeof(ModuleConfiguration))]
    public List<ModuleConfiguration> Configurations { get; set; }

    public ModuleConfiguration findModule(string moduleName)
    {
      foreach (ModuleConfiguration mc in Configurations)
      {
        if (mc.ClassTemplatizedName == moduleName)
          return mc;
      }
      return null;
    }
  }

  [Serializable]
  public class CodeGeneratorArguments
  {
    public int Version { get; set; }
    public string TemplateDirectory { get; set; }
    public string HeaderGuard { get; set; }
    public SystemCModule SystemCModule { get; set; }
    public ModuleConfiguration ModConfig { get; set; }
    public string OutputDirectory { get; set; }
    public string DesignFile { get; set; }
    public string ArgFile { get; set; }

    public List<string> SourceFiles { get; set; }
    public List<string> LinkFlags { get; set; }
    public List<string> LinkPaths { get; set; }
    public List<string> CxxFlags { get; set; }
    public List<string> IncludePaths { get; set; }
    public List<string> IncludeFiles { get; set; }
    public List<string> Defines { get; set; }
    public List<string> GccxmlArgs { get; set; }
    NameValueCollection AnalyzeArgs { get; set; }
    public VisualStudio.VisualStudioVersion VSVersion { get; set; }
    public bool ReAnalyze { get; set; }

    public CodeGeneratorArguments()
    {
      Version = 1;
      ReAnalyze = false;
      SourceFiles = new List<string>();
      LinkFlags = new List<string>();
      LinkPaths = new List<string>();
      CxxFlags = new List<string>();
      IncludePaths = new List<string>();
      IncludeFiles = new List<string>();
      Defines = new List<string>();
      GccxmlArgs = new List<string>();
      AnalyzeArgs = new NameValueCollection();
      VSVersion = VisualStudio.VisualStudioVersion.VS2005;
    }
  }

  public class VcProject
  {
    public string ProjectName { get; set; }
    public string ProjectFile { get; set; }
    public string ProjectGUID { get; set; }
    public string SolutionGUID { get; set; }
    public string VisualCPPGUID { get; set; }
  }

  public class CodeGenerator
  {
    private SystemCModule _m = null;
    private StringTemplate _pchTemplate = null;
    private StringTemplate _hfTemplate = null;
    private StringTemplate _cppTemplate = null;
    private StringTemplate _makeTemplate = null;
    private StringTemplate _confTemplate = null;
    private StringTemplate _confTemplateTop = null;
    private StringTemplate _makefileTop = null;
    private StringTemplate _vcprojTemplate = null;
    private StringTemplate _slnTemplate = null;
    private StringTemplate _carbonBatTemplate = null;
    private StringTemplate _carbonAllTemplate = null;

    private CodeGeneratorArguments _args { get; set; }

    public CodeGenerator(CodeGeneratorArguments args)
    {
      _args = args;

      StringTemplateGroup group = null;

      string templateDir = args.TemplateDirectory;

      _m = args.SystemCModule;

      loadTemplate(templateDir, out group, "syscWrappers.stg", typeof(Antlr.StringTemplate.Language.DefaultTemplateLexer));

      StringTemplateGroup makeGroup;
      loadTemplate(templateDir, out makeGroup, "Makefile.stg", typeof(Antlr.StringTemplate.Language.AngleBracketTemplateLexer));

      StringTemplateGroup confGroup;
      loadTemplate(templateDir, out confGroup, "MaxlibConf.stg", typeof(Antlr.StringTemplate.Language.AngleBracketTemplateLexer));

      _confTemplate = confGroup.GetInstanceOf("confFile");
      if (_confTemplate == null)
        throw new Exception("Unable to load template confFile: MaxlibConf.stg");

      _confTemplateTop = confGroup.GetInstanceOf("confFileIncludes");
      if (_confTemplateTop == null)
        throw new Exception("Unable to load template confFileIncludes: MaxlibConf.stg");

      _makefileTop = makeGroup.GetInstanceOf("makeScript");
      if (_makefileTop == null)
        throw new Exception("Unable to load template makeScript: Makefile.stg");

      _hfTemplate = group.GetInstanceOf("headerFile");
      if (_hfTemplate == null)
        throw new Exception("Unable to load template headerFile: syscWrappers.stg");

      _vcprojTemplate = group.GetInstanceOf("vcprojFile");
      if (_vcprojTemplate == null)
        throw new Exception("Unable to load template vcprojFile: syscWrappers.stg");

      _slnTemplate = group.GetInstanceOf("slnFile");
      if (_slnTemplate == null)
        throw new Exception("Unable to load template slnFile: syscWrappers.stg");
      
      _carbonBatTemplate = group.GetInstanceOf("carbonBat");
      if (_carbonBatTemplate == null)
        throw new Exception("Unable to load template carbonBat: syscWrappers.stg");

      _carbonAllTemplate = group.GetInstanceOf("carbonAll");
      if (_carbonAllTemplate == null)
        throw new Exception("Unable to load template carbonAll: syscWrappers.stg");
      
      _pchTemplate = group.GetInstanceOf("paramCallbackHeader");
      if (_pchTemplate == null)
        throw new Exception("Unable to load template parameterCallback: syscWrappers.stg");

      _cppTemplate = group.GetInstanceOf("cppFile");
      if (_cppTemplate == null)
        throw new Exception("Unable to load template cppFile: syscWrappers.stg");

      _makeTemplate = makeGroup.GetInstanceOf("makeFile");
      if (_makeTemplate == null)
        throw new Exception("Unable to load template makeFile: Makefile.stg");
    }

    private static void loadTemplate(string templateDir, out StringTemplateGroup group, string templateName, Type delim)
    {
      group = null;

      string templatePath = Path.Combine(templateDir, templateName);

      //Console.WriteLine("Reading Template: " + templatePath);
      try
      {
        using (TextReader tr = new StreamReader(templatePath))
        {

          //Console.WriteLine("Constructing Template Group: " + templatePath);
          group = new StringTemplateGroup(tr, delim);
          tr.Close();
          //Console.WriteLine("Finished Reading Template: " + templatePath);
        }
      }
      catch (Exception)
      {
        throw new Exception("Unable to load template group: " + templatePath);
      }
    }

    public string generateParamCallbackHeader()
    {
      string outputDir = _args.OutputDirectory;
      string headerName = Path.Combine(outputDir, "MxCarbonParameterCallback.h");
      Console.WriteLine("Generate Parameter Callback Header File " + headerName);
      FileInfo hfi = new FileInfo(headerName);
      string fileName = hfi.Name;

      using (TextWriter streamWriter = new StreamWriter("Carbon/" + fileName))
      {
        streamWriter.Write(_pchTemplate.ToString());
        streamWriter.Close();
      }
      return headerName;
    }

    public string generateHeaderFile(CPPClassOrStruct c)
    {
      
      string outputDir = _args.OutputDirectory;
      string headerName = Path.Combine(outputDir, _m.SourceFilePath);

      Console.WriteLine("Generate Header File " + headerName);

      _hfTemplate.SetAttribute("scTemplatedClassName", _m.ClassTemplatedName);


      if (_args.ModConfig != null && _args.ModConfig.Parameters.Count > 0)
      {
        _hfTemplate.SetAttribute("scParametersDeclare", _args.ModConfig.ParameterCallbackSource);
      }


      foreach(string incFile in _args.IncludeFiles)
        _hfTemplate.SetAttribute("scIncludeFiles", incFile);

      // Set the class name
      _hfTemplate.SetAttribute("scClassName", _m.ClassName);


      string hdrFileName = null;
      if (_m.SourceFilePath.StartsWith(".") || Path.IsPathRooted(_m.SourceFilePath))
        hdrFileName = _m.SourceFilePath;
      else
        hdrFileName = Path.Combine("..", _m.SourceFilePath);

     _hfTemplate.SetAttribute("scHeaderFile", hdrFileName);

      foreach (DataMember dm in _m.Inputs)
        _hfTemplate.SetAttribute("scInputPorts", dm);
      
      foreach (DataMember dm in _m.Outputs)
        _hfTemplate.SetAttribute("scClassMembers", dm);
      foreach (DataMember dm in _m.InOuts)
        _hfTemplate.SetAttribute("scClassMembers", dm);
      foreach (DataMember dm in _m.Exports)
        _hfTemplate.SetAttribute("scClassMembers", dm);

      foreach (DataMember dm in _m.Ports)
        _hfTemplate.SetAttribute("scClassMembers", dm);

      FileInfo hfi = new FileInfo(headerName);
      string fileName = hfi.Name;

      using (TextWriter streamWriter = new StreamWriter("Carbon/" + fileName))
      {
        streamWriter.Write(_hfTemplate.ToString());
        streamWriter.Close();
      }

      return headerName;
    }
    public string generateConfFile()
    {
      string outputDir = _args.OutputDirectory;

      string path = Path.Combine(outputDir, String.Format("{0}.maxlib.conf", _m.ClassName));

      Console.WriteLine("Generate Maxlib conf File " + path);

      _confTemplate.SetAttribute("scClassName", _m.ClassName);

      using (TextWriter streamWriter = new StreamWriter(path))
      {
        streamWriter.Write(_confTemplate.ToString());
        streamWriter.Close();
      }

      return String.Format("{0}.maxlib.conf", _m.ClassName);
    }

    public string generateConfFileTop(string fname, List<string> files)
    {
      string outputDir = _args.OutputDirectory;

      string path = Path.Combine(outputDir, String.Format("{0}.maxlib.conf", fname));

      Console.WriteLine("Generate MaxlibConf File " + path);

      _confTemplateTop.SetAttribute("confFiles", files);

      using (TextWriter streamWriter = new StreamWriter(path))
      {
        streamWriter.Write(_confTemplateTop.ToString());
        streamWriter.Close();
      }

      return String.Format("{0}.maxlib.conf", fname);
    }

    public string generateSolutionFile(List<VcProject> projects)
    {
      string outputDir = _args.OutputDirectory;
      string path = Path.Combine(outputDir, "carbon.sln");

      Guid slnGuid = Guid.NewGuid();

      _slnTemplate.SetAttribute("slnGuid", "{" + slnGuid.ToString() + "}");

      VisualStudio vs = VisualStudio.getVs(_args.VSVersion);
      _slnTemplate.SetAttribute("slnVersion", vs.SolutionVersion);

      foreach (VcProject p in projects)
      {
        p.SolutionGUID = "{" + slnGuid.ToString() + "}";
        p.VisualCPPGUID = vs.VisualCPPGuid;
        _slnTemplate.SetAttribute("vcProjects", p);
      }

      using (TextWriter streamWriter = new StreamWriter(path))
      {
        streamWriter.Write(_slnTemplate.ToString());
        streamWriter.Close();
      }

      _carbonBatTemplate.SetAttribute("vsEnvVar", vs.EnvVarName);
      _carbonBatTemplate.SetAttribute("vsTarget", "Debug");

      string path1 = Path.Combine(outputDir, "carbonDebug.bat");
      using (TextWriter streamWriter = new StreamWriter(path1))
      {
        streamWriter.Write(_carbonBatTemplate.ToString());
        streamWriter.Close();
      }

      _carbonBatTemplate.Reset();
      _carbonBatTemplate.SetAttribute("vsEnvVar", vs.EnvVarName);
      _carbonBatTemplate.SetAttribute("vsTarget", "Release");

      string path2 = Path.Combine(outputDir, "carbonRelease.bat");
      using (TextWriter streamWriter = new StreamWriter(path2))
      {
        streamWriter.Write(_carbonBatTemplate.ToString());
        streamWriter.Close();
      }

      string path3 = Path.Combine(outputDir, "carbon.bat");
      using (TextWriter streamWriter = new StreamWriter(path3))
      {
        streamWriter.Write(_carbonAllTemplate.ToString());
        streamWriter.Close();
      }

      return path;
    }

    public string generateMakeScript(string fname, List<string> files)
    {
      string outputDir = _args.OutputDirectory;

      string path = Path.Combine(outputDir, fname);

      Console.WriteLine("Generate Makefile " + path);


      foreach(string file in files)
        Console.WriteLine("Referencing " + file);

      _makefileTop.SetAttribute("makeFiles", files);

      using (TextWriter streamWriter = new StreamWriter(path))
      {
        streamWriter.Write(_makefileTop.ToString());
        streamWriter.Close();
      }
      return path;

    }


    private void addSourceFile(string srcFile, StringTemplate templ)
    {
      FileInfo fi = new FileInfo(srcFile);
      string fileName = fi.Name;
      if (srcFile.StartsWith(".") || Path.IsPathRooted(srcFile))
        fileName = srcFile;
      else
        fileName = Path.Combine("..", srcFile);

      templ.SetAttribute("scSourceFiles", fileName);
    }

    
    public string generateVcprojFile(out string projectGuid)
    {
      string outputDir = _args.OutputDirectory;

      string makePath = Path.Combine(outputDir, String.Format("{0}.vcproj", _m.ClassName));

      Console.WriteLine("Generate VCProj " + makePath);

      VisualStudio vs = VisualStudio.getVs(_args.VSVersion);

      _vcprojTemplate.SetAttribute("projVersion", vs.VcprojVersion);

      Guid projGuid = Guid.NewGuid();
      projectGuid = "{" + projGuid.ToString() + "}";

      _vcprojTemplate.SetAttribute("projGuid", projGuid);
      _vcprojTemplate.SetAttribute("hdrGuid", Guid.NewGuid());
      _vcprojTemplate.SetAttribute("srcGuid", Guid.NewGuid());
      _vcprojTemplate.SetAttribute("resGuid", Guid.NewGuid());

      _vcprojTemplate.SetAttribute("scClassName", _m.ClassName);

      foreach (string arg in _args.Defines)
        _vcprojTemplate.SetAttribute("scCPPFlags", arg);
     
      foreach (string cxxFlag in _args.CxxFlags)
        _vcprojTemplate.SetAttribute("scCPPFlags", cxxFlag);

      if (_args.IncludePaths != null)
      {
        foreach (string arg in _args.IncludePaths)
          _vcprojTemplate.SetAttribute("scIncludePaths", arg);
      }


      if (_args.LinkFlags != null)
      {
        foreach (string arg in _args.LinkFlags)
        {
          string a = arg;
          if (!a.ToLower().EndsWith(".lib"))
            a += ".lib";
          _vcprojTemplate.SetAttribute("scLinkFlags", a);
        }
      }

      if (_args.LinkPaths != null)
      {
        foreach (string arg in _args.LinkPaths)
          _vcprojTemplate.SetAttribute("scLinkPaths", arg);
      }

      addSourceFile(string.Format(".\\{0}_wrapper.cpp", _args.SystemCModule.ClassName), _vcprojTemplate);

      foreach (string file in _args.SourceFiles)
        addSourceFile(file, _vcprojTemplate);

      using (TextWriter streamWriter = new StreamWriter(makePath))
      {
        streamWriter.Write(_vcprojTemplate.ToString());
        streamWriter.Close();
      }

      return String.Format("{0}.vcproj", _m.ClassName);
    }


    public string generateMakefile()
    {
      string outputDir = _args.OutputDirectory;

      string makePath = Path.Combine(outputDir, String.Format("Makefile.carbon.{0}", _m.ClassName));

      Console.WriteLine("Generate Makefile " + makePath);

      _makeTemplate.SetAttribute("scClassName", _m.ClassName);
      _makeTemplate.SetAttribute("scSourceFiles", String.Format("{0}_wrapper.cpp", _m.ClassName));

      if (!_m.SourceFilePath.ToLower().EndsWith(".h"))
        addSourceFile(_m.SourceFilePath, _makeTemplate);

      foreach (string cxxFlag in _args.CxxFlags)
        _makeTemplate.SetAttribute("scCPPFlags", cxxFlag);

      if (_args.IncludePaths != null)
      {
        foreach (string cxxFlag in _args.IncludePaths)
          _makeTemplate.SetAttribute("scCPPFlags", string.Format("-I{0}", cxxFlag));
      }

      foreach (string linkFlag in _args.LinkPaths)
        _makeTemplate.SetAttribute("scLinkerFlags", string.Format("-L {0}", linkFlag));

      foreach (string linkFlag in _args.LinkFlags)
        _makeTemplate.SetAttribute("scLinkerFlags", string.Format("-l {0}", linkFlag));

      if (_args.ModConfig != null && !string.IsNullOrEmpty(_args.ModConfig.ParameterCallbackSource))
        addSourceFile(_args.ModConfig.ParameterCallbackSource, _makeTemplate);

      foreach (string file in _args.SourceFiles)
        addSourceFile(file, _makeTemplate);

      using (TextWriter streamWriter = new StreamWriter(makePath))
      {
        streamWriter.Write(_makeTemplate.ToString());
        streamWriter.Close();
      }

      return String.Format("Makefile.carbon.{0}", _m.ClassName);
    }


    public string generateCppFile(CPPClassOrStruct c)
    {
      string outputDir = _args.OutputDirectory;

      foreach (CPPConstructor cs in c.Constructors)
      {
        if (cs.ParseData.Argument != null)
        {
          foreach (GCCXML.Argument arg in cs.ParseData.Argument)
          {
            CPPClassOrStruct argType = null;

            if (c.Top.ClassMap.ContainsKey(arg.type))
              argType = c.Top.ClassMap[arg.type];
          }
        }
      }
      string srcPath = Path.Combine(outputDir, String.Format("{0}_wrapper.cpp", _m.ClassName));
      Console.WriteLine("Generate CPP " + srcPath);

      if (_args.ModConfig != null && _args.ModConfig.Parameters.Count > 0)
      {
        _cppTemplate.SetAttribute("scParametersInit", _args.ModConfig.ParameterCallbackSource);

        // Set the class name
        foreach(ModuleParameter param in _args.ModConfig.Parameters)
          _cppTemplate.SetAttribute("scParameters", param);
      }


      _cppTemplate.SetAttribute("scClassName", _m.ClassName);
      _cppTemplate.SetAttribute("scTemplatedClassName", _m.ClassTemplatedName);

      if (!String.IsNullOrEmpty(_args.HeaderGuard))
        _cppTemplate.SetAttribute("scHeaderGuard", _args.HeaderGuard);

      FileInfo fi = new FileInfo(_m.SourceFilePath);

      _cppTemplate.SetAttribute("scHeaderFile", fi.Name);

      foreach (DataMember dm in _m.Clocks)
      {
        _cppTemplate.SetAttribute("scClockPorts", dm);
      }

      foreach (DataMember dm in _m.Inputs)
      {
        _cppTemplate.SetAttribute("scInputPorts", dm);
      }

      foreach (DataMember dm in _m.Exports)
      {
        _cppTemplate.SetAttribute("scSlavePorts", dm);
        _cppTemplate.SetAttribute("scClassMembers", dm);
      }

      foreach (DataMember dm in _m.Outputs)
      {
        _cppTemplate.SetAttribute("scMasterPorts", dm);
        _cppTemplate.SetAttribute("scClassMembers", dm);
      }


      foreach (DataMember dm in _m.TlmMasterPorts)
      {
        _cppTemplate.SetAttribute("scTlmMasterPorts", dm);
      }

      foreach (DataMember dm in _m.TlmSlavePorts)
      {
        _cppTemplate.SetAttribute("scTlmSlavePorts", dm);
      }

      foreach (DataMember dm in _m.InOuts)
      {
        _cppTemplate.SetAttribute("scInOutPorts", dm);
        _cppTemplate.SetAttribute("scMasterPorts", dm);
        _cppTemplate.SetAttribute("scClassMembers", dm);
      }

      foreach (DataMember dm in _m.Ports)
      {
        _cppTemplate.SetAttribute("scMasterPorts", dm);
        _cppTemplate.SetAttribute("scClassMembers", dm);
      }

      using (TextWriter streamWriter = new StreamWriter(srcPath))
      {
        streamWriter.Write(_cppTemplate.ToString());
        streamWriter.Close();
      }

      return srcPath;
    }
  }
}
