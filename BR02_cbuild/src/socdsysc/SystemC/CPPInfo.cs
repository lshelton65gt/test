﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GCCXML;

namespace CPPStuff
{
  public class CPPInfo
  {
    public GCC_XML parseData;
    public CPPInfo(GCCXML.GCC_XML _parseData)
    {
      parseData = _parseData;

      ConstructorMap = new Dictionary<string, CPPConstructor>();
      ClassMap = new Dictionary<string, CPPClassOrStruct>();

      if (parseData.Class != null)
      {
        // Turn all the id references into actual classes

        Constructors = new List<CPPConstructor>();
        parseData.Constructor.ToList().ForEach(delegate(GCC_XMLConstructor t)
        {
          CPPConstructor cs = new CPPConstructor(t, this);
          ConstructorMap[cs.Id] = cs;
          Constructors.Add(cs);
        });

        Typedefs = new List<CPPTypedef>();
        parseData.Typedef.ToList().ForEach(delegate(GCC_XMLTypedef t) { Typedefs.Add(new CPPTypedef(t, this)); });

        Namespaces = new List<CPPNamespace>();
        parseData.Namespace.ToList().ForEach(delegate(GCC_XMLNamespace t) { Namespaces.Add(new CPPNamespace(t, this)); });

        FundamentalTypes = new List<CPPFundamentalType>();
        parseData.FundamentalType.ToList().ForEach(delegate(GCC_XMLFundamentalType t) { FundamentalTypes.Add(new CPPFundamentalType(t, this)); });

        PointerTypes = new List<CPPPointerType>();
        parseData.PointerType.ToList().ForEach(delegate(GCC_XMLPointerType t) { PointerTypes.Add(new CPPPointerType(t, this)); });
        
        // ---------- Files
        Files = new List<CPPFile>();
        foreach (GCC_XMLFile f in parseData.File)
        {
          Files.Add(new CPPFile(f, this));
        }

        Classes = new List<CPPClassOrStruct>();
        foreach (GCC_XMLClass c in parseData.Class)
        {
          CPPClassOrStruct cs = new CPPClassOrStruct(c, this);
          ClassMap[cs.Id] = cs;
          Classes.Add(cs);
        }
        // -------- structs
        Structs = new List<CPPClassOrStruct>();
        foreach (GCC_XMLStruct c in parseData.Struct)
        {
          Structs.Add(new CPPClassOrStruct(c, this));
        }
        // -------- fields
        Fields = new List<CPPField>();
        foreach (GCC_XMLField f in parseData.Field)
        {
          Fields.Add(new CPPField(f, this));
        }
        // --------- Methods
        Methods = new List<CPPMethod>();
        foreach (GCC_XMLMethod m in parseData.Method)
        {
          Methods.Add(new CPPMethod(m, this));
        }

      }
    }

    public List<CPPNamespace> Namespaces { get; set; }
    public List<CPPFile> Files { get; set; }
    public List<CPPClassOrStruct> Classes { get; set; }
    public List<CPPClassOrStruct> Structs { get; set; }
    public List<CPPField> Fields { get; set; }
    public List<CPPClassOrStruct> ClassesAndStructs
    {
      get
      {
        return Classes.Concat(Structs).ToList();
      }
      set { }
    }
    public List<CPPMethod> Methods { get; set; }
    public List<CPPTypedef> Typedefs { get; set; }
    public List<CPPConstructor> Constructors { get; set; }
    public Dictionary<string, CPPConstructor> ConstructorMap { get; set; }
    public Dictionary<string, CPPClassOrStruct> ClassMap { get; set; }
    public List<CPPFundamentalType> FundamentalTypes { get; set; }
    public List<CPPPointerType> PointerTypes { get; set; }

    public CPPFile FindFileById(string id)
    {
      CPPFile file = Files.Find(delegate(CPPFile f) { return f.Id == id; });
      return file;
    }

    public CPPNamespace FindNamespaceById(string id)
    {
      CPPNamespace file = Namespaces.Find(delegate(CPPNamespace f) { return f.Id == id; });
      return file;
    }

    public CPPConstructor FindConstructorById(string id)
    {
      CPPConstructor file = Constructors.Find(delegate(CPPConstructor f) { return f.Id == id; });
      return file;
    }

    public CPPPointer FindPointerType(string id)
    {
      CPPPointer type = PointerTypes.Find(delegate(CPPPointerType t) { return t.Id == id; });
      return type;
    }


    public CPPType FindTypeById(string id)
    {
      if (Classes != null)
      {
        CPPType type = ClassesAndStructs.Find(delegate(CPPClassOrStruct c) { return c.Id == id; });
        if (type != null) return type;
      }

      if (Typedefs != null)
      {
        CPPType type = Typedefs.Find(delegate(CPPTypedef t) { return t.Id == id; });
        if (type != null) return type;
      }

      if (PointerTypes != null)
      {
        CPPPointer type = PointerTypes.Find(delegate(CPPPointerType t) { return t.Id == id; });
        if (type != null) return type.Type;
      }

      if (FundamentalTypes != null)
      {
        CPPFundamentalType ftype = FundamentalTypes.Find(delegate(CPPFundamentalType t) { return t.Id == id; });
        if (ftype != null)
        {
          FundamentalType ft = new FundamentalType(ftype.Name, ftype.Id);
          return ft;
        }
      }

      return new UnknownType();
    }
  }

  public class CPPType
  {
    public CPPType(IGCC_XMLItem data, CPPInfo i)
    {
      //   if (data != null && data.name != null && data.name.StartsWith("sc"))
      //     System.Diagnostics.Debug.WriteLine(data.name);

      parseData = data;
      infoTop = i;
    }

    public CPPInfo Top
    {
      get
      {
        return infoTop;
      }
    }
    protected IGCC_XMLItem parseData;
    protected CPPInfo infoTop;

    public virtual string Name
    {
      get
      {
        return parseData.name;
      }
      protected set { }
    }
    public virtual string Id
    {
      get
      {
        return parseData.id;
      }
      private set { }

    }
  }

  public class CPPPointer
  {
    public CPPPointer(IGCC_XMLPointerTypeItem data, CPPInfo i)
    {
      parseData = data;
      infoTop = i;
    }

    protected IGCC_XMLPointerTypeItem parseData;
    protected CPPInfo infoTop;
   
    public string Id
    {
      get
      {
        return parseData.id;
      }
      private set { }
    }

    public CPPType Type
    {
      get
      {
        return infoTop.FindTypeById(parseData.type);
      }
      private set { }
    }

  }

  public class CPPFundamental
  {
    public CPPFundamental(IGCC_XMLTypeItem data, CPPInfo i)
    {
      parseData = data;
      infoTop = i;
    }

    protected IGCC_XMLTypeItem parseData;
    protected CPPInfo infoTop;

    public virtual string Name
    {
      get
      {
        return parseData.name;
      }
      protected set { }
    }
    public string Id
    {
      get
      {
        return parseData.id;
      }
      private set { }

    }
  }


  public class CPPTypedef : CPPType
  {
    public CPPTypedef(GCC_XMLTypedef t, CPPInfo i)
      : base(t, i)
    {

    }

    public CPPType Type
    {
      get
      {
        return infoTop.FindTypeById(Id);
      }
      private set { }
    }
  }

  public class CPPPointerType : CPPPointer
  {
    public CPPPointerType(GCC_XMLPointerType t, CPPInfo i)
      : base(t, i)
    {
    }
  }

  public class CPPFundamentalType : CPPFundamental
  {
    public CPPFundamentalType(GCC_XMLFundamentalType t, CPPInfo i)
      : base(t, i)
    {

    }

    public CPPType Type
    {
      get
      {
        return infoTop.FindTypeById(Id);
      }
      private set { }
    }
  }

  /// <summary>
  /// This class is a placeholder for data type info that has not been implemented
  /// </summary>
  public class UnknownType : CPPType
  {
    public UnknownType()
      : base(null, null)
    {

    }
    public override string Name
    {
      get
      {
        return "<Unknown Type>";
      }
      protected set { }
    }
  }


  public class FundamentalType : CPPType
  {
    private string _name;
    private string _id;
    public FundamentalType(string name, string id)
      : base(null, null)
    {
      _name = name;
      _id = id;
    }
    public string ID { get { return _id; } }
    public override string Name
    {
      get
      {
        return _name;
      }
      protected set { }
    }
  }
  public class CPPMethod
  {
    public CPPMethod(GCC_XMLMethod m, CPPInfo i)
    {

    }
  }

  public class CPPConstructor
  {
    public CPPConstructor(GCC_XMLConstructor ns, CPPInfo i)
    {
      infoTop = i;
      parseData = ns;
      Id = ns.id;
      Name = ns.name;
    }
    public string Name { get; set; }
    public string Id { get; set; }
    private GCC_XMLConstructor parseData;
    private CPPInfo infoTop;

    public GCC_XMLConstructor ParseData { get { return parseData; } }
    public CPPInfo InfoTop { get { return infoTop; } }
  }

  public class CPPNamespace
  {
    public CPPNamespace(GCC_XMLNamespace ns, CPPInfo i)
    {
      infoTop = i;
      parseData = ns;
      Id = ns.id;
      Name = ns.name;
    }
    public string Name { get; set; }
    public string Id { get; set; }
    private GCC_XMLNamespace parseData;
    private CPPInfo infoTop;

    public GCC_XMLNamespace ParseData { get { return parseData; } }
    public CPPInfo InfoTop { get { return infoTop; } }
  }

  public class CPPFile
  {
    public CPPFile(GCC_XMLFile f, CPPInfo i)
    {
      infoTop = i;
      parseData = f;
      Id = f.id;
      Name = f.name;
    }
    public string Name { get; set; }
    public string Id { get; set; }
    private GCC_XMLFile parseData;
    private CPPInfo infoTop;
    public GCC_XMLFile ParseData { get { return parseData; } }
    public CPPInfo InfoTop { get { return infoTop; } }
  }


  public class CPPField
  {
    public CPPField(GCC_XMLField f, CPPInfo i)
    {
      //System.Diagnostics.Debug.WriteLine("CPP Field: " + f.name);

      infoTop = i;
      parseData = f;
      Name = f.name;
      Id = f.id;
    }

    public string Context { get { return parseData.context; } private set { } }
    public string Id { get; set; }
    public string Name { get; set; }

    public string Access
    {
      get
      {
        return parseData.access;
      }
      private set
      {
        throw new NotImplementedException();
      }
    }

    public CPPType Type
    {
      get
      {
        return infoTop.FindTypeById(parseData.type);
      }

      private set
      {
        throw new NotImplementedException();
      }
    }

    private GCC_XMLField parseData;
    private CPPInfo infoTop;

  }

  public class CPPClassOrStruct : CPPType
  {
    protected IGCC_XMLClassOrStruct classParseData;
    public bool IsClass;

    public string Filename { get; set; }

    public CPPNamespace Namespace { get; set; }
    public List<CPPConstructor> Constructors { get; set; }

    public override string ToString()
    {
      return Name;
    }
    /// <summary>
    /// Type name without templatization info
    /// </summary>
    public string NameNoTemplate
    {
      get { return Name.Split('<').First(); }
      private set { }
    }

    public CPPClassOrStruct(GCC_XMLStruct s, CPPInfo i)
      : base(s, i)
    {
      Constructors = new List<CPPConstructor>();
      IsClass = false;
      CPPFile file = i.FindFileById(s.file);
      if (file != null)
        Filename = file.Name;

      CPPNamespace ns = i.FindNamespaceById(s.context);
      if (ns != null)
        Namespace = ns;

      if (s.members != null)
      {
        foreach (string mid in s.members.Split(' '))
        {
          if (i.ConstructorMap.ContainsKey(mid))
            Constructors.Add(i.ConstructorMap[mid]);
        }
      }

      classParseData = s;
    }
    public CPPClassOrStruct(GCC_XMLClass c, CPPInfo i)
      : base(c, i)
    {
      Constructors = new List<CPPConstructor>();

      IsClass = true;
      CPPFile file = i.FindFileById(c.file);
      if (file != null)
        Filename = file.Name;

      CPPNamespace ns = i.FindNamespaceById(c.context);
      if (ns != null)
        Namespace = ns;

      if (c.members != null)
      {
        foreach (string mid in c.members.Split(' '))
        {
          if (i.ConstructorMap.ContainsKey(mid))
            Constructors.Add(i.ConstructorMap[mid]);
        }
      }

      classParseData = c;

    }
    /// <summary>
    /// Check to see if a parent class derives from the given class.  Requires
    /// templitization to match exactly
    /// </summary>
    /// <param name="baseName"></param>
    /// <returns></returns>
    public bool DerivesFrom(string baseName)
    {
      // Is it possible to have a class hierarchy where this gets stuck in a loop?
      foreach (CPPClassOrStruct c in BaseClasses)
      {
        if (c.Name == baseName || c.DerivesFrom(baseName))
          return true;
      }
      return false;
    }
    /// <summary>
    /// Checks to see if a class derives from the provided class name, optional ignoring
    /// templatization
    /// </summary>
    /// <param name="baseName">Name of parent class to search for</param>
    /// <param name="stripTemplate">If true, ignore templatization</param>
    /// <returns></returns>
    public bool DerivesFrom(string baseName, bool stripTemplate)
    {
      if (stripTemplate == false) return DerivesFrom(baseName);

      foreach (CPPClassOrStruct c in BaseClasses)
      {
        string strippedName = c.Name.Split('<').First();
        if (strippedName == baseName || c.DerivesFrom(baseName, true))
          return true;
      }

      return false;
    }

    protected List<CPPClassOrStruct> baseClasses;
    public List<CPPClassOrStruct> BaseClasses
    {
      get
      {
        if (baseClasses == null) LoadBases();
        return baseClasses;
      }
      protected set { }
    }

    protected List<CPPField> members;
    public List<CPPField> Members
    {
      get
      {
        if (members == null)
        {
          LoadMembers();
        }
        return members;
      }
      set { }

    }
    // Find all the members for this class
    protected void LoadMembers()
    {
      members = infoTop.Fields.FindAll(FieldByContext);
    }

    Base[] getBases()
    {
      //return IsClass ? classParseData.Base : structParseData.Base;
      return classParseData.Base;
    }
    protected void LoadBases()
    {
      baseClasses = new List<CPPClassOrStruct>();
      if (getBases() == null)
      {
        return; // No base classes -just create an empty list
      }
      else
      {
        foreach (CPPClassOrStruct c in infoTop.Classes.FindAll(IdMatchesBase))
        {
          baseClasses.Add(c);
        }
        foreach (CPPClassOrStruct s in infoTop.Structs.FindAll(IdMatchesBase))
        {
          baseClasses.Add(s);
        }
      }
    }
    #region Predicates
    private bool FieldByContext(CPPField f)
    {
      if (f.Context == this.Id) return true;
      else return false;
    }

    private bool IdMatchesBase(CPPClassOrStruct c)
    {
      foreach (Base b in getBases())
      {
        if (c.Id == b.type) return true;
      }
      return false;
    }
    #endregion

    #region Operators
    /*
      public static bool operator ==(CPPClassOrStruct lhs, CPPClassOrStruct rhs)
      {
         if (lhs != rhs) return false;
         return true;
      }
      public static bool operator !=(CPPClassOrStruct lhs, CPPClassOrStruct rhs)
      {
         // TODO this should be checking a lot more than just the class names!
         //        probably need to makes sure all the members match as well?
         if (lhs.Name != rhs.Name) return true;
         return false;
      }
       */
    #endregion
  }

}
