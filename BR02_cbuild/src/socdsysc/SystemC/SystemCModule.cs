﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using CPPStuff;

namespace systemc
{
  [Serializable]
  public class DataMember
  {
    public string Name { get; set; }
    public string DataType { get; set; }
    public string SignalType { get; set; }
    public string Namespace { get; set; }
  }

  [Serializable]
  public class SystemCConstructorArg
  {
    public string Name { get; set; }
    public string Type { get; set; }
    public string DefaultValue { get; set; }
  }

  [Serializable]
  public class SystemCConstructor
  {
    public string Name { get; set; }

    [XmlArrayItemAttribute("Argument", typeof(SystemCConstructorArg))]
    public List<SystemCConstructorArg> Arguments { get; set; }

    public SystemCConstructor()
    {
      Arguments = new List<SystemCConstructorArg>();
    }
  }


  [Serializable]
  public class SystemCModule
  {
    public static SystemCModule CreateFromModule(CPPClassOrStruct c, ModuleConfiguration mc)
    {
      SystemCModule sysc = new SystemCModule();
      sysc.ClassName = c.NameNoTemplate;
      if (c.Namespace.Name == "::")
        sysc.ClassTemplatedName = c.Name;
      else
        sysc.ClassTemplatedName = String.Format("{0}::{1}", c.Namespace.Name, c.Name);

      sysc.Namespace = c.Namespace.Name;
      sysc.SourceFilePath = c.Filename;


      foreach (CPPConstructor cs in c.Constructors)
      {
        SystemCConstructor scc = new SystemCConstructor();
        scc.Name = cs.Name;
        sysc.Constructors.Add(scc);
        
        if (cs.ParseData.Argument != null)
        {
          foreach (GCCXML.Argument arg in cs.ParseData.Argument)
          {
            CPPClassOrStruct argType = null;
            if (c.Top.ClassMap.ContainsKey(arg.type))
            {
              argType = c.Top.ClassMap[arg.type];
              SystemCConstructorArg carg = new SystemCConstructorArg();
              carg.Name = arg.name;
              carg.Type = argType.Name;
              carg.DefaultValue = arg.@default;
              scc.Arguments.Add(carg);
            }
            else
            {
              SystemCConstructorArg carg = new SystemCConstructorArg();
              carg.Name = arg.name;
              carg.Type = arg.type;

              CPPType t = c.Top.FindTypeById(arg.type);
              if (t != null)
              {
                CPPPointer pointerType = c.Top.FindPointerType(arg.type);

                if (pointerType != null)
                  carg.Type = String.Format("{0}*", t.Name);
                else
                  carg.Type = t.Name;
              }

              carg.DefaultValue = arg.@default;
              scc.Arguments.Add(carg);
            }
          }
        }
      }

      constructModuleFromClass(c, mc, sysc);

      foreach (CPPClassOrStruct bc in c.BaseClasses)
        constructModuleFromClass(bc, mc, sysc);

      return sysc;
    }

    private static void constructModuleFromClass(CPPClassOrStruct c, ModuleConfiguration mc, SystemCModule sysc)
    {
      foreach (CPPField f in c.Members)
      {
        if (f.Access.ToLower() != "public")
          continue;

        // A clock?
        // sc_in_clk is definitely a clock
        // sc_in<bool> might be configured by the user as a clock
        bool isClockPort = false;
        if (mc != null && mc.findClock(f.Name) != null)
          isClockPort = true;
        else if (mc == null && (f.Type.Name == "sc_in_clk" || f.Type.Name == "sc_in<bool>"))
          isClockPort = true;

        if (isClockPort)
        {
          DataMember dm = new DataMember();
          dm.Name = f.Name;
          dm.DataType = f.Type.Name;
          sysc.Clocks.Add(dm);
          // If mc==null, the gui needs the possible clocks from sc_in<bool>s
          if (mc != null || (mc == null && f.Type.Name == "sc_in_clk"))
            continue;
        }

        if (f.Type is CPPClassOrStruct)
        {
          CPPClassOrStruct memberClass = (CPPClassOrStruct)f.Type;

          if (memberClass.NameNoTemplate == "tlm_initiator_socket")
          {
            DataMember dm = new DataMember();
            dm.Name = f.Name;
            dm.DataType = f.Type.Name;
            if (memberClass.Namespace.Name != "::")
              dm.Namespace = String.Format("{0}::", memberClass.Namespace.Name);
            sysc.TlmMasterPorts.Add(dm);

          }
          else if (memberClass.NameNoTemplate == "tlm_target_socket")
          {
            DataMember dm = new DataMember();
            dm.Name = f.Name;
            dm.DataType = f.Type.Name;
            if (memberClass.Namespace.Name != "::")
              dm.Namespace = String.Format("{0}::", memberClass.Namespace.Name);
            sysc.TlmSlavePorts.Add(dm);
          }
          else
            if (memberClass.NameNoTemplate == "sc_in" || memberClass.DerivesFrom("sc_in", true))
            {
              DataMember dm = new DataMember();
              dm.Name = f.Name;
              dm.DataType = f.Type.Name;
              dm.SignalType = f.Type.Name.Replace("sc_in<", "sc_signal<");
              if (memberClass.Namespace.Name != "::")
                dm.Namespace = String.Format("{0}::", memberClass.Namespace.Name);

              sysc.Inputs.Add(dm);
            }
            else if (memberClass.NameNoTemplate == "sc_out" || memberClass.DerivesFrom("sc_out", true))
            {
              DataMember dm = new DataMember();
              dm.Name = f.Name;
              dm.DataType = f.Type.Name;
              if (memberClass.Namespace.Name != "::")
                dm.Namespace = String.Format("{0}::", memberClass.Namespace.Name);
              sysc.Outputs.Add(dm);
            }
            else if (memberClass.NameNoTemplate == "sc_inout" || memberClass.DerivesFrom("sc_inout", true))
            {
              DataMember dm = new DataMember();
              dm.Name = f.Name;
              dm.DataType = f.Type.Name;
              if (memberClass.Namespace.Name != "::")
                dm.Namespace = String.Format("{0}::", memberClass.Namespace.Name);
              sysc.InOuts.Add(dm);
            }
            else if (memberClass.NameNoTemplate == "sc_export" || memberClass.DerivesFrom("sc_export", true))
            {
              DataMember dm = new DataMember();
              dm.Name = f.Name;
              dm.DataType = f.Type.Name;
              if (memberClass.Namespace.Name != "::")
                dm.Namespace = String.Format("{0}::", memberClass.Namespace.Name);
              sysc.Exports.Add(dm);
            }

            else if (memberClass.NameNoTemplate == "sc_port" || memberClass.DerivesFrom("sc_port", true))
            {
              DataMember dm = new DataMember();
              dm.Name = f.Name;
              dm.DataType = f.Type.Name;
              if (memberClass.Namespace.Name != "::")
                dm.Namespace = String.Format("{0}::", memberClass.Namespace.Name);
              sysc.Ports.Add(dm);
            }
        }
      }
    }


    public SystemCModule()
    {
      Ports = new List<DataMember>();
      Inputs = new List<DataMember>();
      Outputs = new List<DataMember>();
      InOuts = new List<DataMember>();
      Exports = new List<DataMember>();
      Clocks = new List<DataMember>();
      TlmMasterPorts = new List<DataMember>();
      TlmSlavePorts = new List<DataMember>();
      Constructors = new List<SystemCConstructor>();
    }

    public string ClassName { get; set; }
    public string ClassTemplatedName { get; set; }
    public string Namespace { get; set; }
    public string SourceFilePath { get; set; }


    [XmlArrayItemAttribute("Constructor", typeof(SystemCConstructor))]
    public List<SystemCConstructor> Constructors { get; set; }

    [XmlArrayItemAttribute("Clocks", typeof(DataMember))]
    public List<DataMember> Clocks { get; set; }

    [XmlArrayItemAttribute("Port", typeof(DataMember))]
    public List<DataMember> Ports { get; set; }

    [XmlArrayItemAttribute("Input", typeof(DataMember))]
    public List<DataMember> Inputs { get; set; }

    [XmlArrayItemAttribute("Output", typeof(DataMember))]
    public List<DataMember> Outputs { get; set; }

    [XmlArrayItemAttribute("InOut", typeof(DataMember))]
    public List<DataMember> InOuts { get; set; }

    [XmlArrayItemAttribute("Export", typeof(DataMember))]
    public List<DataMember> Exports { get; set; }

    [XmlArrayItemAttribute("TlmMasterPort", typeof(DataMember))]
    public List<DataMember> TlmMasterPorts { get; set; }

    [XmlArrayItemAttribute("TlmSlavePort", typeof(DataMember))]
    public List<DataMember> TlmSlavePorts { get; set; }
  }
}
