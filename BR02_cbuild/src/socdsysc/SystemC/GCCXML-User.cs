﻿namespace GCCXML
{
   public interface IGCC_XMLItem
   {
      string id { get; set; }
      string name { get; set; }
      string context { get; set; }
   }

   public interface IGCC_XMLTypeItem
   {
     string id { get; set; }
     string name { get; set; }
   }

   public interface IGCC_XMLPointerTypeItem
   {
     string id { get; set; }
     string type { get; set; }
   }

   public class UnimplementedType : IGCC_XMLItem
   {
      public UnimplementedType(string _id)
      {
         id = _id;
      }

      #region IGCC_XMLItem Members
      protected string _id;
      public string id
      {
         get
         {
            return _id;
         }
         set
         {
            _id = value;
         }
      }

      public string name
      {
         get
         {
            return "type not implemented";
         }
         set
         {
            throw new System.NotImplementedException();
         }
      }

      public string context
      {
         get
         {
            throw new System.NotImplementedException();
         }
         set
         {
            throw new System.NotImplementedException();
         }
      }

      #endregion
   }
   public interface IGCC_XMLClassOrStruct : IGCC_XMLItem
   {
      Base[] Base { get; set; }
   }

   public partial class GCC_XMLClass : IGCC_XMLClassOrStruct
   {
   }

   public partial class GCC_XMLStruct : IGCC_XMLClassOrStruct
   {
   }

   public partial class GCC_XMLMethod : IGCC_XMLItem
   { 
   }

   public partial class GCC_XMLTypedef : IGCC_XMLItem
   {
   }

   public partial class GCC_XMLFundamentalType : IGCC_XMLTypeItem
   {
   }

   public partial class GCC_XMLPointerType : IGCC_XMLPointerTypeItem
   {
   }
}