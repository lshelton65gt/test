﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.IO;

namespace sys2socd
{
  public class Utilities
  {
    public static string Join<T>(string separator, IEnumerable<T> list)
    {
      return Join<T>(separator, list, delegate(T o) { return o.ToString(); });
    }

    // Join that takes an IEnumerable list that uses a converter to convert the type to a string
    public static string Join<T>(string separator, IEnumerable<T> list, Converter<T, string> converter)
    {
      StringBuilder sb = new StringBuilder();
      foreach (T t in list)
      {
        if (sb.Length != 0) sb.Append(separator);
        sb.Append(converter(t));
      }
      return sb.ToString();
    }

     static public bool runningWithMono()
    {
      Type mt = Type.GetType("Mono.Runtime");
      return mt != null;
    }
    // absolutePathIn = Working directory
    // relativeToIn = Filename being converted to relative
    static public string RelativePath(string absolutePathIn, string relativeToIn)
    {
      string absolutePath = absolutePathIn.Replace('/', '\\');
      string relativeTo = relativeToIn.Replace('/', '\\');

      string[] absoluteDirectories = absolutePath.Split('\\');
      string[] relativeDirectories = relativeTo.Split('\\');

      //Get the shortest of the two paths
      int length = absoluteDirectories.Length < relativeDirectories.Length ? absoluteDirectories.Length : relativeDirectories.Length;

      //Use to determine where in the loop we exited
      int lastCommonRoot = -1;
      int index;

      //Find common root
      for (index = 0; index < length; index++)
        if (absoluteDirectories[index] == relativeDirectories[index])
          lastCommonRoot = index;
        else
          break;

      //If we didn't find a common prefix then throw
      if (lastCommonRoot == -1)
        return absolutePathIn;

      //Build up the relative path
      StringBuilder relativePath = new StringBuilder();

      string dirUpString = "..\\";
      string sepString = "\\";
      if (runningWithMono())
      {
        dirUpString = "../";
        sepString = "/";
      }

      if (lastCommonRoot == 0)
        return relativeToIn;

      //Add on the ..
      for (index = lastCommonRoot + 1; index < absoluteDirectories.Length; index++)
        if (absoluteDirectories[index].Length > 0)
          relativePath.Append(dirUpString);

      //Add on the folders
      for (index = lastCommonRoot + 1; index < relativeDirectories.Length - 1; index++)
        relativePath.Append(relativeDirectories[index] + sepString);
      relativePath.Append(relativeDirectories[relativeDirectories.Length - 1]);

      return relativePath.ToString();
    }

    static public string ExpandString(string inStr)
    {
      string temp = inStr;

      if (inStr.Contains('$'))
      {
        Regex exp1 = new Regex(@"\$\((\w+)\)");
        MatchCollection mc = exp1.Matches(temp);
        if (mc.Count > 0)
        {
          foreach (Match m in mc)
          {
            Capture c = m.Groups[1].Captures[0];
            string evar = Environment.GetEnvironmentVariable(c.Value);
            if (evar != null)
            {
              string newtemp = temp.Replace(m.Value, evar);
              temp = newtemp;
            }
          }
        }

        Regex exp2 = new Regex(@"\$\{(\w+)\}");
        mc = exp2.Matches(temp);
        if (mc.Count > 0)
        {
          foreach (Match m in mc)
          {
            Capture c = m.Groups[1].Captures[0];
            string evar = Environment.GetEnvironmentVariable(c.Value);
            if (evar != null)
            {
              string newtemp = temp.Replace(m.Value, evar);
              temp = newtemp;
            }
          }

          inStr = Environment.ExpandEnvironmentVariables(temp);
        }
      }

      if (temp.Contains(" ") && !temp.StartsWith("\""))
        temp = "\"" + temp + "\"";

      if (runningWithMono())
        return temp.Replace("\\", "/");
      else
        return temp.Replace("/", "\\");
      
    }
  }
}
