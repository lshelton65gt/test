﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace sys2socd
{
  public partial class DialogAddParameter : Form
  {
    public enum ParameterType { String, Boolean };
    public enum ParameterKind { InitTime, RunTime };
    public ParameterType Type { get; set; }
    public ParameterKind Kind { get; set; }
    public string ParameterName { get; set; }
    public string DefaultValue { get; set; }

    public DialogAddParameter()
    {
      Kind = ParameterKind.InitTime;
      Type = ParameterType.String;
      InitializeComponent();
      initComboBoxes();
      buttonOK.Enabled = false;
    }


    public DialogAddParameter(systemc.ModuleParameter mp)
    {
      Type = ParameterType.String;
      Kind = ParameterKind.InitTime;
      switch (mp.Type)
      {
        case systemc.ModuleParameter.ParameterType.Boolean:
          Type = ParameterType.Boolean; break;
        case systemc.ModuleParameter.ParameterType.String:
          Type = ParameterType.String; break;
      }

      switch (mp.Kind)
      {
        case systemc.ModuleParameter.ParameterKind.InitTime:
          Kind = ParameterKind.InitTime; break;
        case systemc.ModuleParameter.ParameterKind.RunTime:
          Kind = ParameterKind.RunTime; break;
      }


      InitializeComponent();

      initComboBoxes();

      textBoxParamName.Text = mp.Name;

      switch (mp.Type)
      {
        case systemc.ModuleParameter.ParameterType.Boolean:
          comboBoxParamType.SelectedItem = "Boolean";
          if (mp.DefaultValue.ToLower() == "true")
            comboBoxBoolDefault.SelectedItem = "true";
          else
            comboBoxBoolDefault.SelectedItem = "false";
          break;
        case systemc.ModuleParameter.ParameterType.String:
          comboBoxParamType.SelectedItem = "String";
          textBoxStringDefault.Text = mp.DefaultValue;
          break;
      }

      switch (mp.Kind)
      {
        case systemc.ModuleParameter.ParameterKind.InitTime:
          comboBoxParamKind.SelectedItem = "Init-Time"; break;
        case systemc.ModuleParameter.ParameterKind.RunTime: 
          comboBoxParamKind.SelectedItem = "Run-Time"; break;
      }
     
      comboBoxParamType_SelectedIndexChanged(null, null);

      buttonOK.Enabled = validParamName(textBoxParamName.Text);
    }


    private void buttonOK_Click(object sender, EventArgs e)
    {
      ParameterName = textBoxParamName.Text;
      Type = getType();
      switch (Type)
      {
        case ParameterType.String: DefaultValue = textBoxStringDefault.Text; break;
        case ParameterType.Boolean: DefaultValue = comboBoxBoolDefault.SelectedItem as string; break;
      }

      switch (comboBoxParamKind.SelectedItem as string)
      {
        case "Init-Time": Kind = ParameterKind.InitTime; break;
        case "Run-Time": Kind = ParameterKind.RunTime; break;
      }
    }

    private bool validParamName(string text)
    {
      if (!String.IsNullOrEmpty(text))
        return true;
      else
        return false;
    }

    private void DialogAddParameter_Load(object sender, EventArgs e)
    {
    }

    private void initComboBoxes()
    {
      comboBoxParamType.Items.Add("String");
      comboBoxParamType.Items.Add("Boolean");
      comboBoxParamType.SelectedItem = "String";
      comboBoxBoolDefault.Items.Add("false");
      comboBoxBoolDefault.Items.Add("true");
      comboBoxBoolDefault.SelectedItem = "false";

      comboBoxParamKind.Items.Add("Init-Time");
      comboBoxParamKind.Items.Add("Run-Time");
      comboBoxParamKind.SelectedItem = "Init-Time";
    }

    private ParameterType getType()
    {
      string choice = comboBoxParamType.SelectedItem as string;
      switch (choice)
      {
        case "String": return ParameterType.String;
        case "Boolean": return ParameterType.Boolean;
      }

      return ParameterType.String;
    }

    private void comboBoxParamType_SelectedIndexChanged(object sender, EventArgs e)
    {
      textBoxStringDefault.Visible = false;
      comboBoxBoolDefault.Visible = false;

      switch (getType())
      {
        case ParameterType.String: textBoxStringDefault.Visible = true; break;
        case ParameterType.Boolean: comboBoxBoolDefault.Visible = true; break;
      }
    }

    private void textBoxParamName_TextChanged(object sender, EventArgs e)
    {
      buttonOK.Enabled = validParamName(textBoxParamName.Text);
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
