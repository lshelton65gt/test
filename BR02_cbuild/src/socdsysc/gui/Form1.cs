﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using GCCXML;
using CPPStuff;
using systemc;

namespace sys2socd
{
  public partial class Form1 : Form
  {
    string workingDir;
    Process _proc = null;
    public NameValueCollection Arguments { get; set; }
    bool _processRunning = false;
    GCC_XML cppStructure = null;
    CPPInfo cppinfo;


    List<CPPClassOrStruct> _modules = new List<CPPClassOrStruct>();

    VisualStudio.VisualStudioVersion getVersion()
    {
      VisualStudio vs = comboBoxVC.SelectedItem as VisualStudio;
      if (vs != null)
        return vs.Version;
      else
        return VisualStudio.VisualStudioVersion.VS2005;
    }

    public Form1()
    {
      workingDir = Environment.CurrentDirectory;

      InitializeComponent();

      buttonRemoveSource.Enabled = false;

      openFileDialogDesignFile.InitialDirectory = workingDir;

      // restore GUI values
      restoreValues();

      textBoxOutputDir.Text = workingDir;

      Arguments = new NameValueCollection();

      if (Utilities.runningWithMono())
      {
        labelVC.Visible = false;
        comboBoxVC.Visible = false;
        labelTarget.Visible = false;
        comboBoxTarget.Visible = false;
      }
      else
        populateVisualStudioChoices();
    }

    private void populateVisualStudioChoices()
    {
      string vc71 = Environment.GetEnvironmentVariable("VS71COMNTOOLS");
      string vc80 = Environment.GetEnvironmentVariable("VS80COMNTOOLS");
      string vsInstallDir = Environment.GetEnvironmentVariable("VSINSTALLDIR");

      VisualStudio vs2003 = VisualStudio.getVs(VisualStudio.VisualStudioVersion.VS2003);
      VisualStudio vs2005 = VisualStudio.getVs(VisualStudio.VisualStudioVersion.VS2005);


      comboBoxTarget.SelectedItem = "Release";
      comboBoxVC.Items.Clear();

      if (!String.IsNullOrEmpty(vc71))
        comboBoxVC.Items.Add(vs2003);

      if (!String.IsNullOrEmpty(vc80))
        comboBoxVC.Items.Add(vs2005);

      // choose default
      if (!String.IsNullOrEmpty(vsInstallDir))
      {
        if (!String.IsNullOrEmpty(vc71) && vc71.StartsWith(vsInstallDir))
          comboBoxVC.SelectedItem = vs2003;
        else if (!String.IsNullOrEmpty(vc80) && vc80.StartsWith(vsInstallDir))
          comboBoxVC.SelectedItem = vs2005;
      }
      else // No default
      {
        if (!String.IsNullOrEmpty(vc80))
          comboBoxVC.SelectedItem = vs2005;
        else
          comboBoxVC.SelectedItem = vs2003;
      }
     }

    private void restoreValues()
    {
      textBoxDefinitions.Text = Settings1.Default.Defines;
      textBoxIncludePaths.Text = Settings1.Default.IncludePaths;
      textBoxLinkerDirs.Text = Settings1.Default.LinkerPaths;
      textBoxLinkerLibs.Text = Settings1.Default.LinkerLibs;
    }

    private void saveValues()
    {
      Settings1.Default.IncludePaths = textBoxIncludePaths.Text;
      Settings1.Default.Defines = textBoxDefinitions.Text;
      Settings1.Default.LinkerLibs = textBoxLinkerLibs.Text;
      Settings1.Default.LinkerPaths = textBoxLinkerDirs.Text;
      Settings1.Default.Save();
    }

    private void buttonBrowseDesignFile_Click(object sender, EventArgs e)
    {
      if (!checkOutputDir())
      {
        openFileDialogDesignFile.Multiselect = false;
        if (openFileDialogDesignFile.ShowDialog() == DialogResult.OK)
        {
          string relPath = Utilities.RelativePath(workingDir, openFileDialogDesignFile.FileName);
          if (relPath == workingDir)
            relPath = openFileDialogDesignFile.FileName;

          textBoxDesignFile.Text = relPath;
        }
      }
    }

    private void buttonBrowseSourceFiles_Click(object sender, EventArgs e)
    {
      if (!checkOutputDir())
      {
        openFileDialogDesignFile.Multiselect = true;
        if (openFileDialogDesignFile.ShowDialog() == DialogResult.OK)
        {
          foreach (string filename in openFileDialogDesignFile.FileNames)
          {
            string relPath = Utilities.RelativePath(workingDir, filename);
            if (relPath == workingDir)
              relPath = filename;

            listBoxSourceFiles.Items.Add(relPath);
          }
        }
      }

      updateOutputDirControls();
    }

    private void buttonRemoveSource_Click(object sender, EventArgs e)
    {
      while (listBoxSourceFiles.SelectedIndices.Count > 0)
        listBoxSourceFiles.Items.RemoveAt(listBoxSourceFiles.SelectedIndices[0]);

      buttonRemoveSource.Enabled = listBoxSourceFiles.SelectedItems.Count > 0;
    }

    private void buttonBrowseIncludePaths_Click(object sender, EventArgs e)
    {
      if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
      {
        string relPath = Utilities.RelativePath(workingDir, folderBrowserDialog1.SelectedPath);
        appendItem(textBoxIncludePaths, relPath);
      }
    }

    void appendItem(TextBox tb, string item)
    {
      if (tb.Text.Length == 0)
        tb.Text = item;
      else
        tb.Text += String.Format(";{0}", item);
    }

    private void textBoxDesignFile_TextChanged(object sender, EventArgs e)
    {
      if (textBoxDesignFile.Text.Length > 0)
        errorProvider1.SetError(textBoxDesignFile, "");

      updateOutputDirControls();
    }

    private void updateOutputDirControls()
    {
      textBoxOutputDir.Enabled = String.IsNullOrEmpty(textBoxDesignFile.Text);
      buttonBrowseOutputDir.Enabled = textBoxOutputDir.Enabled;
    }

    private void wizardPageAnalyze_CloseFromNext(object sender, Gui.Wizard.PageEventArgs e)
    {
     
    }

    private void wizardPageAnalyze_CloseFromBack(object sender, Gui.Wizard.PageEventArgs e)
    {
      System.Diagnostics.Debug.WriteLine("Close from back");
    }

    private void wizardPageInit_CloseFromNext(object sender, Gui.Wizard.PageEventArgs e)
    {
      errorProvider1.Clear();

      bool hasErrors = false;

      if (String.IsNullOrEmpty(textBoxDesignFile.Text))
      {
        errorProvider1.SetError(textBoxDesignFile, "Required");
        hasErrors = true;
      }

      if (checkOutputDir())
      {
        errorProvider1.SetError(textBoxOutputDir, "Directory Must Exist");
        hasErrors = true;
      }

      if (hasErrors)
        e.Page = wizardPageInit;
    }

    private void wizardPageAnalyze_ShowFromNext(object sender, EventArgs e)
    {
      setCursor(Cursors.WaitCursor);

      richTextBox1.Clear();

      saveValues();

      wizard1.NextEnabled = false;
      wizard1.BackEnabled = false;

      _processRunning = false;

      Arguments.Clear();

      Arguments["includePaths"] = textBoxIncludePaths.Text;
      Arguments["designFile"] = textBoxDesignFile.Text;
      Arguments["defines"] = textBoxDefinitions.Text;
      Arguments["linkerDirs"] = textBoxLinkerDirs.Text;
      Arguments["linkerLibs"] = textBoxLinkerLibs.Text;

      progressBar1.Value = 0;

      _proc = new Process();

      string carbonHome = Environment.GetEnvironmentVariable("CARBON_HOME");
      string program = String.Format("{0}/bin/socdsyscimport", carbonHome);
      List<string> args = new List<string>();

      if (Utilities.runningWithMono())
      {
        _proc.StartInfo.FileName = program.Replace("\\", "/");
      }
      else
      {
        program += ".bat";

        if (!program.StartsWith("\""))
          program = String.Format("\"{0}\"", program);

        _proc.StartInfo.FileName = "cmd.exe";
        args.Add("/c");
        args.Add(program.Replace("/", "\\"));
      }

      string ip = Arguments["includePaths"];
      char[] seps = { ';' };

      foreach (string ipath in ip.Split(seps))
      {
        if (String.IsNullOrEmpty(ipath))
          continue;

        args.Add("-I");
        string includeDir = Utilities.ExpandString(ipath).Replace("\\", "/");
        args.Add(includeDir);
      }

      ip = Arguments["defines"];
      foreach (string ipath in ip.Split(seps))
      {
        if (String.IsNullOrEmpty(ipath))
          continue;

        args.Add("-D");
        string defineValue = Utilities.ExpandString(ipath).Replace("\\", "/");
        args.Add(defineValue);
      }

      ip = Arguments["linkerDirs"];
      foreach (string ipath in ip.Split(seps))
      {
        if (String.IsNullOrEmpty(ipath))
          continue;

        args.Add("-L");
        string defineValue = Utilities.ExpandString(ipath).Replace("\\", "/");
        args.Add(defineValue);
      }

      ip = Arguments["linkerLibs"];
      foreach (string ipath in ip.Split(seps))
      {
        if (String.IsNullOrEmpty(ipath))
          continue;

        args.Add("-l");
        string defineValue = Utilities.ExpandString(ipath).Replace("\\", "/");
        args.Add(defineValue);
      }


      string df = Arguments["designFile"].Replace("\\", "/");

      args.Add("-design");
      args.Add(df);
      args.Add("-analyze");

      _proc.StartInfo.Arguments = Utilities.Join(" ", args);
      _proc.EnableRaisingEvents = true;
      _proc.StartInfo.CreateNoWindow = true;
      _proc.StartInfo.UseShellExecute = false;
      _proc.StartInfo.WorkingDirectory = workingDir;
      _proc.StartInfo.RedirectStandardError = true;
      _proc.StartInfo.RedirectStandardOutput = true;

      _proc.OutputDataReceived += new DataReceivedEventHandler(_proc_OutputDataReceived);
      _proc.ErrorDataReceived += new DataReceivedEventHandler(_proc_OutputDataReceived);
      _proc.Exited += new EventHandler(_proc_Exited);

      //Wizard.SetWizardButtons(SMS.Windows.Forms.WizardButton.DisabledNext);

      _processRunning = true;
      if (_proc.Start())
      {
        _proc.BeginOutputReadLine();
        _proc.BeginErrorReadLine();
      }
    }

    void _proc_Exited(object sender, EventArgs e)
    {
      setCursor(Cursors.Default);

      _processRunning = false;

      if (InvokeRequired)
      {
        BeginInvoke(new MethodInvoker(delegate()
        {
          wizard1.NextEnabled = true;
          wizard1.BackEnabled = true;
          progressBar1.Value = 0;
          if (_proc.ExitCode == 0)
            wizard1.Next();
          else
            wizard1.NextEnabled = false;
        }));
      }
      else
      {
        wizard1.NextEnabled = true;
        wizard1.BackEnabled = true;
        progressBar1.Value = 0;
        if (_proc.ExitCode == 0)
          wizard1.Next();
        else
          wizard1.NextEnabled = false;
      }
    }

    void updateProgress(ProgressBar pb)
    {
      if (_processRunning)
      {
        if (pb.Value == 10)
          pb.Value = 1;
        else
          pb.Value = pb.Value + 1;
      }
    }

    void appendText(ProgressBar pb, RichTextBox rtb, string output)
    {
      if (rtb.InvokeRequired)
      {
        BeginInvoke(new MethodInvoker(delegate()
        {
          if (!String.IsNullOrEmpty(output) && output.Trim() == "." && _processRunning)
            updateProgress(pb);
          else
          {
            if (!String.IsNullOrEmpty(output))
              rtb.AppendText(output + System.Environment.NewLine);
            else
              rtb.AppendText(System.Environment.NewLine);

            rtb.ScrollToCaret();
          }
        }));
      }
      else
      {
        if (!String.IsNullOrEmpty(output) && output.Trim() == "." && _processRunning)
          updateProgress(pb);
        else
        {
          if (!String.IsNullOrEmpty(output))
            rtb.AppendText(output + System.Environment.NewLine);
          else
            rtb.AppendText(System.Environment.NewLine);

          rtb.ScrollToCaret();
        }

      }
    }

    void _proc_OutputDataReceived(object sender, DataReceivedEventArgs e)
    {
      Debug.WriteLine(e.Data);
      appendText(progressBar1, richTextBox1, e.Data);
    }

    private void wizardPageModuleSelect_ShowFromNext(object sender, EventArgs e)
    {
      this.Cursor = Cursors.WaitCursor;

      checkedListBoxModules.Items.Clear();

      if (loadGccXmlFile() != null)
        populateModules();

      this.Cursor = Cursors.Default;
    }

    GCCXML.GCC_XML loadGccXmlFile()
    {
      XmlSerializer serializer = new XmlSerializer(typeof(GCCXML.GCC_XML));

      string xmlFile = Path.Combine(workingDir, Path.Combine("Carbon", "out.xml"));

      cppStructure = null;
      cppinfo = null;

      using (TextReader reader = new StreamReader(xmlFile))
      {
        cppStructure = (GCCXML.GCC_XML)serializer.Deserialize(reader);
        reader.Close();
        cppinfo = new CPPInfo(cppStructure);
      }
     
      return cppStructure;
    }


    void populateModules()
    {
      foreach (CPPClassOrStruct c in cppinfo.ClassesAndStructs)
      {
        if (c.Name != "sc_event_queue" && c.DerivesFrom("sc_module"))
        {
          checkedListBoxModules.Items.Add(c, true);
        }
      }

      wizard1.NextEnabled = checkedListBoxModules.CheckedIndices.Count > 0;
    }

    private void buttonCheckAll_Click(object sender, EventArgs e)
    {
      for (int i = 0; i < checkedListBoxModules.Items.Count; i++)
      {
        checkedListBoxModules.SetItemChecked(i, true);
      }
      wizard1.NextEnabled = checkedListBoxModules.CheckedIndices.Count > 0;
    }

    private void buttonUncheckAll_Click(object sender, EventArgs e)
    {
      for (int i = 0; i < checkedListBoxModules.Items.Count; i++)
      {
        checkedListBoxModules.SetItemChecked(i, false);
      }
      wizard1.NextEnabled = checkedListBoxModules.CheckedIndices.Count > 0;
    }

    private void wizardPageModuleSelect_CloseFromNext(object sender, Gui.Wizard.PageEventArgs e)
    {
      _modules.Clear();
      foreach (int i in checkedListBoxModules.CheckedIndices)
      {
        _modules.Add(checkedListBoxModules.Items[i] as CPPClassOrStruct);
      }
    }

    private void checkedListBoxModules_SelectedIndexChanged(object sender, EventArgs e)
    {
      wizard1.NextEnabled = checkedListBoxModules.CheckedIndices.Count > 0;
    }

    private void wizardPageModules_ShowFromNext(object sender, EventArgs e)
    {
      populateModuleTree();

    }

    void populateModuleTree()
    {
      treeViewModules.Nodes.Clear();
      foreach (CPPClassOrStruct c in _modules)
      {
        ModuleItem item = new ModuleItem();
        item.ClassOrStruct = c;
        item.SysCModule = SystemCModule.CreateFromModule(c, null);

        ModuleConfiguration mc = createModuleConfiguration(item);
        
        TreeNode tn = new TreeNode(c.Name);
        tn.Tag = mc;
        treeViewModules.Nodes.Add(tn);
      }

      if (_modules.Count > 0)
      {
        treeViewModules.SelectedNode = treeViewModules.Nodes[0];
        treeViewModules.Select();
      }
    }

    private static ModuleConfiguration createModuleConfiguration(ModuleItem item)
    {
      ModuleConfiguration mc = new ModuleConfiguration();
      mc.Module = item;
      mc.ClassName = item.SysCModule.ClassName;
      mc.ClassTemplatizedName = item.SysCModule.ClassTemplatedName;

      foreach (DataMember clockDm in item.SysCModule.Clocks)
      {
        if (clockDm.DataType == "sc_in_clk")
          mc.Clocks.Add(clockDm);
      }


      foreach (SystemCConstructor ctor in item.SysCModule.Constructors)
      {
        if (ctor.Arguments.Count == 1 && ctor.Arguments[0].Type == "sc_module_name")
        {
          mc.Constructor = ctor;
          break;
        }
      }

      return mc;
    }

    private void populateTabs(TreeNode tn)
    {
      ModuleConfiguration mc = tn.Tag as ModuleConfiguration;
      ModuleItem c = mc.Module;
      populateClockTab(mc);
      populateCtors(c);
      populateParameters(mc);
    }

    private void populatePorts(List<DataMember> ports, ModuleConfiguration mc)
    {
      ports.Clear();

      foreach (DataMember dm in mc.Module.SysCModule.Inputs)
        ports.Add(dm);

      foreach (DataMember dm in mc.Module.SysCModule.Outputs)
        ports.Add(dm);

      foreach (DataMember dm in mc.Module.SysCModule.InOuts)
        ports.Add(dm);

      foreach (DataMember dm in mc.Module.SysCModule.Exports)
        ports.Add(dm);

      foreach (DataMember dm in mc.Module.SysCModule.TlmMasterPorts)
        ports.Add(dm);

      foreach (DataMember dm in mc.Module.SysCModule.TlmSlavePorts)
        ports.Add(dm);

      foreach (DataMember dm in mc.Module.SysCModule.Clocks)
        ports.Add(dm);
    }

   
    private void populateParameters(ModuleConfiguration mc)
    {
      listViewParameters.Items.Clear();

      textBoxParamCallbackSource.Text = mc.ParameterCallbackSource;

      foreach (ModuleParameter mp in mc.Parameters)
        addParameter(mp);
    }

    private void populateCtors(ModuleItem mi)
    {
      comboBoxCtors.Items.Clear();
   
      foreach (SystemCConstructor ctor in mi.SysCModule.Constructors)
      {
        string args = "";
        string argType = "";
        foreach (SystemCConstructorArg carg in ctor.Arguments)
        {
          argType = carg.Type;
          if (String.IsNullOrEmpty(args))
            args = String.Format("{0} {1}", carg.Type, carg.Name);
          else
            args +=  String.Format(", {0} {1}", carg.Type, carg.Name);
        }
        string ctorValue = String.Format("{0}({1})", ctor.Name, args);
        //Console.WriteLine(ctorValue);
        comboBoxCtors.Items.Add(ctorValue);
        if (argType == "sc_module_name")
          comboBoxCtors.SelectedIndex = comboBoxCtors.FindString(ctorValue);
      }
    }


    private void populateClockTab(ModuleConfiguration mc)
    {
      List<DataMember> ports = new List<DataMember>();

      populatePorts(ports, mc);

      dataGridView1.AutoGenerateColumns = false;
      dataGridView1.DataSource = ports;

      for (int i = 0; i < dataGridView1.Rows.Count; i++)
      {
        DataMember dm = dataGridView1.Rows[i].DataBoundItem as DataMember;
        DataGridViewCheckBoxCell cell = dataGridView1.Rows[i].Cells[2] as DataGridViewCheckBoxCell;
        cell.Tag = dm;

        // What is this currently set to?
        DataMember cm = mc.findClock(dm.Name);
        if (cm != null)
          cell.Value = true;
        else
          cell.Value = false;
      }
    }

    private void treeViewModules_BeforeSelect(object sender, TreeViewCancelEventArgs e)
    {
      saveSelectedNodeValues();
    }

    private void saveSelectedNodeValues()
    {
      TreeNode selNode = treeViewModules.SelectedNode;

      #region MonoWorkaround1
      // This Code, is to workaround a Mono Bug
      // If a control has focus, it won't "Accept" the change
      // Unless we change the selection to the first column
      if (dataGridView1.SelectedCells.Count > 0)
      {
        int rowIndex = dataGridView1.SelectedCells[0].RowIndex;
        dataGridView1.CurrentCell = dataGridView1[0, rowIndex];
      }
      #endregion

      if (selNode != null) // Node is being de-selected, save it's values
        saveModuleChoices(selNode.Tag as ModuleConfiguration);
    }

    // called whenever selection changes to save
    // current state of the GUI
    void saveModuleChoices(ModuleConfiguration mc)
    {
      saveClockChoices(mc);
      saveParameters(mc);
    }

    private void saveParameters(ModuleConfiguration mc)
    {
      mc.ParameterCallbackSource = textBoxParamCallbackSource.Text;
      mc.Parameters.Clear();
      foreach (ListViewItem lvi in listViewParameters.Items)
      {
        ModuleParameter mp = lvi.Tag as ModuleParameter;
        mc.Parameters.Add(mp);
      }
    }

    // Wipe out the clock list, re-build it from current-state
    private void saveClockChoices(ModuleConfiguration mc)
    {
      mc.Clocks.Clear();
      for (int i = 0; i < dataGridView1.Rows.Count; i++)
      {
        DataGridViewCheckBoxCell cell = dataGridView1.Rows[i].Cells[2] as DataGridViewCheckBoxCell;
        DataMember dm = cell.Tag as DataMember;
        bool cellValue = (bool)cell.Value;
        if (cellValue)
          mc.Clocks.Add(dm);
      }
    }

    private void treeViewModules_AfterSelect(object sender, TreeViewEventArgs e)
    {
      TreeNode tn = e.Node;
      if (tn != null)
      {
        populateTabs(tn);
      }
    }

    private void buttonBrowseIncludeFiles_Click(object sender, EventArgs e)
    {
      openFileDialogDesignFile.Multiselect = true;
      if (openFileDialogDesignFile.ShowDialog() == DialogResult.OK)
      {
        foreach (string filename in openFileDialogDesignFile.FileNames)
        {
          string relPath = Utilities.RelativePath(workingDir, filename);
          listBoxIncludeFiles.Items.Add(relPath);
        }
      }
    }

    private void buttonRemoveIncludeFile_Click(object sender, EventArgs e)
    {
      while (listBoxIncludeFiles.SelectedIndices.Count > 0)
        listBoxIncludeFiles.Items.RemoveAt(listBoxIncludeFiles.SelectedIndices[0]);

      buttonRemoveIncludeFile.Enabled = listBoxIncludeFiles.SelectedItems.Count > 0;

    }

    private void textBoxDesignFile_Validating(object sender, CancelEventArgs e)
    {
    }

    private void wizardPageGenerate_ShowFromNext(object sender, EventArgs e)
    {
      ModuleConfigurations configs = new ModuleConfigurations();

      foreach (TreeNode tn in treeViewModules.Nodes)
      {
        ModuleConfiguration mc = tn.Tag as ModuleConfiguration;
        configs.Configurations.Add(mc);
      }

      configs.GccXmlFile = "out.xml";
      configs.HeaderGuard = textBoxHeaderGuard.Text;
      configs.DesignFile = textBoxDesignFile.Text;
      configs.IncludePaths = textBoxIncludePaths.Text;
      configs.Definitions = textBoxDefinitions.Text;
      configs.LinkerFlags = textBoxLinkerLibs.Text;
      configs.LinkerPaths = textBoxLinkerDirs.Text;

      for (int i = 0; i < listBoxIncludeFiles.Items.Count; i++)
      {
        string includeFile = listBoxIncludeFiles.Items[i] as string;
        configs.IncludeFiles.Add(includeFile);
      }

      for (int i = 0; i < listBoxSourceFiles.Items.Count; i++)
      {
        string sourceFile = listBoxSourceFiles.Items[i] as string;

        string relativeTo = Path.Combine(workingDir, "Carbon");
        string srcFile = Path.Combine(workingDir, sourceFile);
        string relPath = Utilities.RelativePath(relativeTo, srcFile);
        if (relPath == relativeTo)
          relPath = srcFile;

        configs.SourceFiles.Add(relPath);
      }
   
      XmlSerializer xs = new XmlSerializer(typeof(ModuleConfigurations));

      string cfgFile = Path.Combine(workingDir, Path.Combine("Carbon", "configs.xml"));

      using (Stream sw = new FileStream(cfgFile, FileMode.Create, FileAccess.Write))
      {
        xs.Serialize(sw, configs);
      }

      generateCode();

      Console.WriteLine("");
    }


    void setCursor(Cursor c)
    {
      if (InvokeRequired)
      {
        BeginInvoke(new MethodInvoker(delegate()
        {
          this.Cursor = c;
        }));
      }
      else
      {
        this.Cursor = c;
      }
    }

    private void generateCode()
    {
      setCursor(Cursors.WaitCursor);

      _proc = new Process();

      _proc.EnableRaisingEvents = true;

      _proc.Exited += new EventHandler(pCodeGen_Exited);

      string carbonHome = System.Environment.GetEnvironmentVariable("CARBON_HOME");
      string carbonBin = Path.Combine(carbonHome, "bin");
      string socdsyscimport;
      if (Utilities.runningWithMono())
        socdsyscimport = Path.Combine(carbonBin, "socdsyscimport");
      else
        socdsyscimport = Path.Combine(carbonBin, "socdsyscimport.bat");

      _processRunning = false;

      _proc.StartInfo.WorkingDirectory = workingDir;
      _proc.StartInfo.FileName = socdsyscimport;
      _proc.StartInfo.CreateNoWindow = true;
      _proc.StartInfo.UseShellExecute = false;

      _proc.StartInfo.RedirectStandardError = true;
      _proc.StartInfo.RedirectStandardOutput = true;

      progressBar2.Value = 0;

      _proc.OutputDataReceived += new DataReceivedEventHandler(p_OutputDataReceived);
      _proc.ErrorDataReceived += new DataReceivedEventHandler(p_OutputDataReceived);

      _proc.StartInfo.Arguments = "-argFile Carbon/configs.xml";

      if (!Utilities.runningWithMono())
      {
        VisualStudio.VisualStudioVersion vs = getVersion();
        _proc.StartInfo.Arguments += " -msvc " + vs.ToString();
      }

      appendText(progressBar2, richTextBox2, String.Format("{0} {1}", _proc.StartInfo.FileName, _proc.StartInfo.Arguments));

      try
      {
        if (_proc.Start())
        {
          wizard1.NextEnabled = false;
          wizard1.BackEnabled = false;
          _processRunning = true;
          _proc.BeginErrorReadLine();
          _proc.BeginOutputReadLine();
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Error Launching Program");
      }
    }

    private void compileGeneratedCode()
    {
      setCursor(Cursors.WaitCursor);

      _proc = new Process();

      _proc.EnableRaisingEvents = true;

      _proc.Exited += new EventHandler(pCompile_Exited);

      string carbonHome = System.Environment.GetEnvironmentVariable("CARBON_HOME");
      string carbonBin = Path.Combine(carbonHome, "bin");
      string programtorun;
      if (Utilities.runningWithMono())
        programtorun = Path.Combine(carbonBin, "make");
      else
        programtorun = "cmd.exe";

      _processRunning = false;

      _proc.StartInfo.WorkingDirectory = Path.Combine(workingDir, "Carbon");
      _proc.StartInfo.FileName = programtorun;
      _proc.StartInfo.CreateNoWindow = true;
      _proc.StartInfo.UseShellExecute = false;

      _proc.StartInfo.RedirectStandardError = true;
      _proc.StartInfo.RedirectStandardOutput = true;

      progressBar2.Value = 0;

      _proc.OutputDataReceived += new DataReceivedEventHandler(p_OutputDataReceived);
      _proc.ErrorDataReceived += new DataReceivedEventHandler(p_OutputDataReceived);

      if (Utilities.runningWithMono())
        _proc.StartInfo.Arguments = "-f Makefile";
      else
      {
        string targetName = "carbon.bat";
        switch (comboBoxTarget.SelectedItem.ToString())
        {
          case "Debug": targetName = "carbonDebug.bat"; break;
          case "Release": targetName = "carbonRelease.bat"; break;
          case "All": targetName = "carbon.bat"; break;
        }

        _proc.StartInfo.Arguments = String.Format("/c {0}", targetName);
      }

      appendText(progressBar2, richTextBox2, String.Format("{0} {1}", _proc.StartInfo.FileName, _proc.StartInfo.Arguments));

      try
      {
        _processRunning = true;
        if (_proc.Start())
        {
          wizard1.NextEnabled = false;
          wizard1.BackEnabled = false;
          _proc.BeginErrorReadLine();
          _proc.BeginOutputReadLine();
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Error Launching Program");
      }
    }


    void p_OutputDataReceived(object sender, DataReceivedEventArgs e)
    {
      Debug.WriteLine(e.Data);
      appendText(progressBar2, richTextBox2, ".");
      appendText(progressBar2, richTextBox2, e.Data);
    }

    void pCodeGen_Exited(object sender, EventArgs e)
    {
      setCursor(Cursors.Default);

      _processRunning = false;

      if (InvokeRequired)
      {
        BeginInvoke(new MethodInvoker(delegate()
        {
          compileGeneratedCode();
          progressBar2.Value = 0;
        }));
      }
      else
      {
        progressBar2.Value = 0;
        compileGeneratedCode();
      }
    }

    void pCompile_Exited(object sender, EventArgs e)
    {
      setCursor(Cursors.Default);

      _processRunning = false;

      if (InvokeRequired)
      {
        BeginInvoke(new MethodInvoker(delegate()
        {
          wizard1.NextEnabled = true;
          wizard1.BackEnabled = true;
          progressBar2.Value = 0;

          //if (_proc.ExitCode == 0)
          //  wizard1.Next();
          //else
          //  wizard1.NextEnabled = false;
        }));
      }
      else
      {
        wizard1.NextEnabled = true;
        wizard1.BackEnabled = true;
        progressBar2.Value = 0;
        //if (_proc.ExitCode == 0)
        //  wizard1.Next();
        //else
        //  wizard1.NextEnabled = false;
      }
    }
   

    private void wizardPageModules_CloseFromNext(object sender, Gui.Wizard.PageEventArgs e)
    {
      saveSelectedNodeValues();
      Debug.Write("Next!");
    }

    private void textBoxParamCallbackSource_TextChanged(object sender, EventArgs e)
    {
      buttonAddParameter.Enabled = !String.IsNullOrEmpty(textBoxParamCallbackSource.Text);
      buttonRemoveParameter.Enabled = listViewParameters.Items.Count > 0 && listViewParameters.SelectedItems.Count > 0;
      buttonEditParameter.Enabled = buttonRemoveParameter.Enabled;
    }

    private void buttonBrowseParamCallbackSource_Click(object sender, EventArgs e)
    {
      openFileDialogDesignFile.Multiselect = false;
      openFileDialogDesignFile.FileName = textBoxParamCallbackSource.Text;
      if (openFileDialogDesignFile.ShowDialog() == DialogResult.OK)
      {
        string rp = Utilities.RelativePath(openFileDialogDesignFile.FileName, workingDir);
        string relPath = Utilities.RelativePath(workingDir, openFileDialogDesignFile.FileName);
        textBoxParamCallbackSource.Text = relPath;
      }
    }

    ModuleConfiguration getConfig()
    {
      ModuleConfiguration mc = treeViewModules.SelectedNode.Tag as ModuleConfiguration;
      return mc;
    }

    private void buttonAddParameter_Click(object sender, EventArgs e)
    {
      DialogAddParameter dlg = new DialogAddParameter();
      if (dlg.ShowDialog() == DialogResult.OK)
      {
        ModuleParameter mp = new ModuleParameter();
        applyParamValues(dlg, mp);

        ModuleConfiguration mc = getConfig();
        mc.Parameters.Add(mp);

        addParameter(mp);
      }
    }

    private static void applyParamValues(DialogAddParameter dlg, ModuleParameter mp)
    {
      mp.Name = dlg.ParameterName;
      mp.DefaultValue = dlg.DefaultValue;
      switch (dlg.Type)
      {
        case DialogAddParameter.ParameterType.Boolean:
          mp.Type = ModuleParameter.ParameterType.Boolean; break;
        case DialogAddParameter.ParameterType.String:
          mp.Type = ModuleParameter.ParameterType.String; break;
      }

      switch (dlg.Kind)
      {
        case DialogAddParameter.ParameterKind.InitTime:
          mp.Kind = ModuleParameter.ParameterKind.InitTime; break;
        case DialogAddParameter.ParameterKind.RunTime:
          mp.Kind = ModuleParameter.ParameterKind.RunTime; break;
      }
    }

    private void addParameter(ModuleParameter mp)
    {
      ListViewItem lvi = new ListViewItem(mp.Name);
      lvi.Tag = mp;
      listViewParameters.Items.Add(lvi);
      lvi.SubItems.Add(mp.Type.ToString());
      lvi.SubItems.Add(mp.Kind.ToString());
      lvi.SubItems.Add(mp.DefaultValue);
    }

    private void buttonRemoveParameter_Click(object sender, EventArgs e)
    {
      if (MessageBox.Show("Are you sure you want to remove these parameter(s)?", "Confirm Removal", MessageBoxButtons.YesNo) == DialogResult.Yes)
      {
        ModuleConfiguration mc = getConfig();
        foreach (ListViewItem lvi in listViewParameters.SelectedItems)
        {
          ModuleParameter mp = lvi.Tag as ModuleParameter;
          mc.Parameters.Remove(mp);
        }

        while (listViewParameters.SelectedItems.Count > 0)
          listViewParameters.Items.Remove(listViewParameters.SelectedItems[0]);

        updateParameterButtons();
      }
    }

    private void updateParameterButtons()
    {
      buttonRemoveParameter.Enabled = listViewParameters.SelectedIndices.Count > 0;
      buttonEditParameter.Enabled = buttonRemoveParameter.Enabled;
    }

    private void listViewParameters_SelectedIndexChanged(object sender, EventArgs e)
    {
      updateParameterButtons();
    }

    private void listViewParameters_DoubleClick(object sender, EventArgs e)
    {
      ModuleConfiguration mc = getConfig();
      if (mc != null)
      {
        if (listViewParameters.SelectedItems.Count > 0)
        {
          ListViewItem lvi = listViewParameters.SelectedItems[0];
          ModuleParameter mp = lvi.Tag as ModuleParameter;
          DialogAddParameter dlg = new DialogAddParameter(mp);
          if (dlg.ShowDialog() == DialogResult.OK)
          {
            applyParamValues(dlg, mp);
            lvi.Text = mp.Name;
            lvi.SubItems[1].Text = mp.Type.ToString();
            lvi.SubItems[2].Text = mp.Kind.ToString();
            lvi.SubItems[3].Text = mp.DefaultValue;
          }
        }
      }
    }

    private void buttonEditParameter_Click(object sender, EventArgs e)
    {
      listViewParameters_DoubleClick(sender, e);
    }


    private void listBoxSourceFiles_SelectedIndexChanged(object sender, EventArgs e)
    {
      buttonRemoveSource.Enabled = listBoxSourceFiles.SelectedItems.Count > 0;
    }

    private bool checkOutputDir()
    {
      if (!Directory.Exists(textBoxOutputDir.Text))
      {
        DialogResult reply = MessageBox.Show(String.Format("Directory: {0} does not exist, create it?", textBoxOutputDir.Text),
          "Create Directory", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (reply == DialogResult.Yes)
          Directory.CreateDirectory(textBoxOutputDir.Text);
        else
          textBoxOutputDir.Text = Environment.CurrentDirectory;
      }

      bool errorsFound = !Directory.Exists(textBoxOutputDir.Text);
      if (!errorsFound)
        changeOutputDir();

      return errorsFound;
    }

    private void changeOutputDir()
    {
      workingDir = textBoxOutputDir.Text;
      openFileDialogDesignFile.InitialDirectory = workingDir;
      Environment.CurrentDirectory = workingDir;
    }


    bool hasFiles()
    {
      return listBoxSourceFiles.Items.Count > 0 || textBoxDesignFile.Text.Length > 0;
    }


    private void buttonBrowseOutputDir_Click(object sender, EventArgs e)
    {
      if (hasFiles() && MessageBox.Show("Changing the output directory requires re-entering all referenced files\n.Are you sure you want to do this?", "Confirm Directory Change", MessageBoxButtons.YesNo) == DialogResult.No)
        return;

      if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
      {
        string relPath = Utilities.RelativePath(workingDir, folderBrowserDialog1.SelectedPath);
        textBoxOutputDir.Text = folderBrowserDialog1.SelectedPath;
        changeOutputDir();
      }
    }

  }
}
